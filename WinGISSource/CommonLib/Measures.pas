{*******************************************************************************
| Unit Measures
|-------------------------------------------------------------------------------
| Autor: Martin Forst
|-------------------------------------------------------------------------------
| Definiert eine Klasse zum Arbeiten mit Ma�angaben:
| - speichern von Wert und Ma�einheit
| - Umrechnung zwischen verschiedenen Ma�einheiten
| - Liefert die Namen der Ma�einheiten (aus String-File)                       
+******************************************************************************}
Unit Measures;

Interface

Uses Classes;

Type TMeasureUnits      = (muNone,muDrawingUnits,muPixels,muPoints,muPicas,
                           muMillimeters,muCentimeters,muMeters,muKilometers,
                           muInches,muFeet,muYards,muMiles,muLines,muPercent,
                           muRad,muGrad,muDegrees);

     TMeasureUnitsSet   = Set of TMeasureUnits;

     TMeasureType       = (mtLinear,mtSquare,mtCubic);

Type TMeasure      = Class(TPersistent)
      Private
       FOnChange   : TNotifyEvent;
       FValue      : Double;
       FUnits      : TMeasureUnits;
       Procedure   DoChange;
       Function    GetValue:Double;
       Function    GetValues(AUnits:TMeasureUnits):Double;
       Function    GetShortUnitsName:String;
       Function    GetShortUnitsNames(AUnits:TMeasureUnits):String;
       Procedure   SetUnits(AUnits:TMeasureUnits);
       Function    GetUnitsName:String;
       Function    GetUnitsNames(AUnits:TMeasureUnits):String;
       Procedure   SetValue(Const AValue:Double);
       Procedure   SetValues(AUnits:TMeasureUnits;Const AValue:Double);
      Protected
       Procedure   AssignTo(Dest:TPersistent); override;
      Public
       Constructor Create(Const AValue:Double;AUnits:TMeasureUnits);
       Function    IsEqual(AMeasure:TMeasure):Boolean;
       Property    UnitsName:String read GetUnitsName;
       Property    UnitsNames[AUnit:TMeasureUnits]:String read GetUnitsNames;
       Property    UnitsShortName:String read GetShortUnitsName;
       Property    UnitsShortNames[AUnit:TMeasureUnits]:String read GetShortUnitsNames;
       Property    Values[Units:TMeasureUnits]:Double read GetValues write SetValues; default;
      Published
       Property    OnChange:TNotifyEvent read FOnChange write FOnChange;
       Property    Units:TMeasureUnits read FUnits write SetUnits default muMillimeters;
       Property    Value:Double read GetValue write SetValue;
     end;

Function UnitsToName(AUnits:TMeasureUnits):String;

Function UnitsToShortName(AUnits:TMeasureUnits):String;

Function NameToUnits(Const AName:String):TMeasureUnits;

Function ShortNameToUnits(Const AName:String):TMeasureUnits;

Function UnitsToBaseUnits(AUnits:TMeasureUnits):Double;

Function UnitsToIdent(AUnits:TMeasureUnits):String;

Function IdentToUnits(Const Ident:String):TMeasureUnits;

Function ConvertibleUnits(AUnits:TMeasureUnits):TMeasureUnitsSet;

Function ConvertMeasure(const Value:Double;FromUnits,ToUnits:TMeasureUnits):Double;

Implementation

Uses NumTools,MultiLng;

var SectionNumber  : Integer;

Constructor TMeasure.Create
   (
   Const AValue    : Double;
   AUnits          : TMeasureUnits
   );
  begin
    inherited Create;
    FUnits:=AUnits;
    Value:=AValue;
  end;   

Function TMeasure.GetValue
   : Double;
  begin
    Result:=GetValues(FUnits);
  end;

Procedure TMeasure.SetValue
   (
   Const AValue    : Double
   );
  begin
    SetValues(FUnits,AValue);
  end;

Function UnitsToBaseUnits
   (
   AUnits          : TMeasureUnits
   )
   : Double;
  begin
    case AUnits of
      muNone         : Result:=1;
      muPoints       : Result:=0.667*2.54;
      muPicas        : Result:=0.667*2.45;
      muCentimeters  : Result:=10;
      muMeters       : Result:=1000;
      muKilometers   : Result:=1E6;
      muInches       : Result:=25.4;
      muFeet         : Result:=304.8;
      muYards        : Result:=914.4;
      muMiles        : Result:=1609000;
      muLines        : Result:=0.667*2.54;
      muMillimeters  : Result:=1;
      muDegrees      : Result:=1;
      muRad          : Result:=180.0/Pi;
      muGrad         : Result:=200.0/Pi;
      muPercent      : Result:=1;
      muDrawingUnits : Result:=1;
      else Result:=1;
    end;
  end;
   
Function TMeasure.GetValues(AUnits:TMeasureUnits):Double;
begin
  case AUnits of
    muPixels       : Result:=FValue;
    muPicas        : Result:=FValue/0.667/2.45;
    muPoints       : Result:=FValue/0.667/2.54;
    muCentimeters  : Result:=FValue/10;
    muMeters       : Result:=FValue/1000;
    muKilometers   : Result:=FValue/1E6;
    muInches       : Result:=FValue/25.4;
    muFeet         : Result:=FValue/304.8;
    muYards        : Result:=FValue/914.4;
    muMiles        : Result:=FValue/1609000;
    muLines        : Result:=FValue/0.667/2.45;
    muDrawingUnits,
    muNone,
    muMillimeters,
    muRad,
    muPercent      : Result:=FValue;
    muGrad         : Result:=FValue*200.0/Pi;
    muDegrees      : Result:=FValue*180.0/Pi;
    else Result:=0;
  end;
end;

Procedure TMeasure.SetValues(AUnits:TMeasureUnits;Const AValue:Double);
var NewValue     : Double;
begin
  case AUnits of
    muPixels       : NewValue:=AValue;
    muPoints       : NewValue:=AValue*0.667*2.54;
    muPicas        : NewValue:=AValue*0.667*2.45;
    muCentimeters  : NewValue:=AValue*10;
    muMeters       : NewValue:=AValue*1000;
    muKilometers   : NewValue:=AValue*1E6;
    muInches       : NewValue:=AValue*25.4;
    muFeet         : NewValue:=AValue*304.8;
    muYards        : NewValue:=AValue*914.4;
    muMiles        : NewValue:=AValue*1609000;
    muLines        : NewValue:=AValue*0.667*2.54;
    muNone,
    muDrawingUnits,
    muMillimeters,
    muRad,
    muPercent      : NewValue:=AValue;
    muGrad         : NewValue:=AValue*Pi/200.0;
    muDegrees      : NewValue:=AValue*Pi/180.0;
    else NewValue:=AValue;
  end;
  if Abs(NewValue-FValue)>1E-200 then begin
    FValue:=NewValue;
    DoChange;
  end;
end;

Function UnitsToName
   (
   AUnits          : TMeasureUnits
   )
   : String;
  begin
    if SectionNumber=-1 then SectionNumber:=MlgStringList.Sections['UnitsNames'];
    Result:=MlgStringList.SectionStrings[SectionNumber,Byte(AUnits)];
  end;

Function TMeasure.GetUnitsNames
   (
   AUnits          : TMeasureUnits
   )
   : String;
  begin
    Result:=GetUnitsNames(AUnits);
  end;

Function UnitsToShortName
   (
   AUnits          : TMeasureUnits
   )
   : String;
  begin
    if SectionNumber=-1 then SectionNumber:=MlgStringList.Sections['UnitsNames'];
    Result:=MlgStringList.SectionStrings[SectionNumber,Byte(AUnits)+50];
  end;

Function TMeasure.GetShortUnitsNames
   (
   AUnits          : TMeasureUnits
   )
   : String;
  begin
    Result:=UnitsToShortName(AUnits);
  end;

Function TMeasure.GetUnitsName
   : String;
  begin
    Result:=GetUnitsNames(FUnits);
  end;

Function TMeasure.GetShortUnitsName
   : String;
  begin
    Result:=GetShortUnitsNames(FUnits);
  end;

Procedure TMeasure.DoChange;
  begin
    if Assigned(FOnChange) then OnChange(Self);
  end;

Procedure TMeasure.SetUnits
   (
   AUnits          : TMeasureUnits
   );
  begin
    if AUnits<>FUnits then begin
      FUnits:=AUnits;
      DoChange;
    end;
  end;

Function TMeasure.IsEqual
   (
   AMeasure        : TMeasure
   )
   : Boolean;
  begin
    Result:=DblCompare(Values[muMillimeters],AMeasure.Values[muMillimeters])=0;
  end;

Procedure TMeasure.AssignTo
   (
   Dest            : TPersistent
   );
  begin
    with Dest as TMeasure do begin
      FUnits:=Self.FUnits;
      FValue:=Self.FValue;
      FOnChange:=Self.FOnChange;
    end;
  end;
    
Function UnitsToIdent
   (
   AUnits          : TMeasureUnits
   )
   : String;
  begin
    case AUnits of
      muNone         : Result:='None';
      muDrawingUnits : Result:='DrawingUnits';
      muPixels       : Result:='Pixels';
      muPoints       : Result:='Points';
      muPicas        : Result:='Picas';
      muMillimeters  : Result:='Millimeters';
      muCentimeters  : Result:='Centimeters';
      muMeters       : Result:='Meters';
      muKilometers   : Result:='Kilometers';
      muInches       : Result:='Inches';
      muFeet         : Result:='Feet';
      muYards        : Result:='Yards';
      muMiles        : Result:='Miles';
      muLines        : Result:='Lines';
      muPercent      : Result:='Percent';
      muRad          : Result:='Rad';
      muGrad         : Result:='Grad';
      muDegrees      : Result:='Degrees';
      else Result:='';
    end;
  end;

Function IdentToUnits
   (
   Const Ident     : String
   )
   : TMeasureUnits;
  begin
    if Ident='DrawingUnits' then Result:=muDrawingUnits
    else if Ident='Pixels' then Result:=muPixels
    else if Ident='Points' then Result:=muPoints
    else if Ident='Picas' then Result:=muPicas
    else if Ident='Millimeters' then Result:=muMillimeters
    else if Ident='Centimeters' then Result:=muCentimeters
    else if Ident='Meters' then Result:=muMeters
    else if Ident='Kilometers' then Result:=muKilometers
    else if Ident='Inches' then Result:=muInches
    else if Ident='Feet' then Result:=muFeet
    else if Ident='Yards' then Result:=muYards
    else if Ident='Miles' then Result:=muMiles
    else if Ident='Lines' then Result:=muLines
    else if Ident='Percent' then Result:=muPercent
    else if Ident='Rad' then Result:=muRad
    else if Ident='Grad' then Result:=muGrad
    else if Ident='Degrees' then Result:=muDegrees
    else Result:=muNone;
  end;

Function NameToUnits
   (
   Const AName     : String
   )
   : TMeasureUnits;
  begin
    for Result:=Low(TMeasureUnits) to High(TMeasureUnits) do
        if UnitsToName(Result)=AName then Exit;
    Result:=muNone;    
  end; 

Function ShortNameToUnits
   (
   Const AName     : String
   )
   : TMeasureUnits;
  begin
    for Result:=Low(TMeasureUnits) to High(TMeasureUnits) do
        if UnitsToShortName(Result)=AName then Exit;
    Result:=muNone;
  end;

{*******************************************************************************
| Function ConvertibleUnits
|-------------------------------------------------------------------------------
| Berechnet, in welche Ma�einheiten die angegebene Ma�einheit umrechenbar ist.
+******************************************************************************}
Function ConvertibleUnits
   (
   AUnits          : TMeasureUnits
   )
   : TMeasureUnitsSet;
  begin
    Result:=[];
    if AUnits in [muNone,muPercent,muDrawingUnits,muPixels] then Result:=[]
    else if AUnits in [muRad,muGrad,muDegrees] then Result:=[muRad,muGrad,muDegrees]-[AUnits]
    else Result:=[muPoints,muPicas,muMillimeters,muCentimeters,muMeters,muKilometers,
        muInches,muFeet,muYards,muMiles,muLines]-[AUnits];
  end;
   
Function ConvertMeasure(const Value:Double;FromUnits,ToUnits:TMeasureUnits):Double;
begin
  Result:=Value*UnitsToBaseUnits(FromUnits)/UnitsToBaseUnits(ToUnits);
end;

Initialization
  begin
    RegisterClass(TMeasure);
    SectionNumber:=-1;
  end;

end.
