{**********************************************}
{   TBar3DSeries Component                     }
{   Copyright (c) 1996-2000 by David Berneda   }
{**********************************************}
{$I teedefs.inc}
unit Bar3D;

{ This Series component is derived from TBarSeries.
  It has a new property:  OffsetValues

  This new property allows to specify a different ORIGIN value
  for EACH bar point.
  This can be used with standard TBarSeries components to
  make a "Stacked-3D" chart type.
}
interface

uses
  Windows, SysUtils, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, Teengine, Series;

type
  TBar3DSeries = class(TBarSeries)
  private
    { Private declarations }
    FOffsetValues : TChartValueList;
  protected
    { Protected declarations }
    Procedure AddSampleValues(NumValues:Integer); override;
    Procedure SetOffsetValues(Value:TChartValueList);
  public
    { Public declarations }
    Constructor Create(AOwner: TComponent); override;
    Function AddBar( Const AX,AY,AOffset:Double;
                     Const AXLabel:String{$IFDEF D4}=''{$ENDIF};
                     AColor:TColor{$IFDEF D4}=clTeeColor{$ENDIF}):Integer;
    Function GetOriginValue(ValueIndex:Integer):Double; override;
    Function MaxYValue:Double; override;
    Function MinYValue:Double; override;
    Function PointOrigin(ValueIndex:Integer; SumAll:Boolean):Double; override;
  published
    { Published declarations }
    property OffsetValues:TChartValueList read FOffsetValues
                                          write SetOffsetValues;
  end;

implementation

Uses Chart, TeeProcs, TeCanvas, TeeProco;

{ TBar3DSeries }
Constructor TBar3DSeries.Create(AOwner: TComponent);
begin
  inherited;
  FOffsetValues:=TChartValueList.Create(Self,TeeMsg_ValuesOffset); { <-- "offset" storage }
end;

Procedure TBar3DSeries.SetOffsetValues(Value:TChartValueList);
begin
  SetChartValueList(FOffsetValues,Value); { standard method }
end;

{ calculate maximum Y value }
Function TBar3DSeries.MaxYValue:Double;
begin
  result:=inherited MaxYValue;
  if (MultiBar=mbNone) or (MultiBar=mbSide) then
     result:=MaxDouble(result,FOffsetValues.MaxValue);
end;

{ calculate minimum Y value ( YValues and negative Offsets supported ) }
Function TBar3DSeries.MinYValue:Double;
var t : Integer;
begin
  result:=inherited MinYValue;
  if (MultiBar=mbNone) or (MultiBar=mbSide) then
     for t:=0 to Count-1 do
         if FOffsetValues.Value[t]<0 then result:=MinDouble(result,YValues.Value[t]+FOffsetValues.Value[t]);
end;

Function TBar3DSeries.AddBar( Const AX,AY,AOffset:Double;
                              Const AXLabel:String{$IFDEF D4}=''{$ENDIF};
                              AColor:TColor{$IFDEF D4}=clTeeColor{$ENDIF}):Integer;
begin
  FOffsetValues.TempValue:=AOffset;
  result:=AddXY(AX,AY,AXLabel,AColor);
//  AddValue(result);
end;

Procedure TBar3DSeries.AddSampleValues(NumValues:Integer);
Var t : Integer;
Begin
  With RandomBounds(NumValues) do
  for t:=1 to NumValues do { some sample values to see something in design mode }
  Begin
    tmpY:=System.Random(Round(DifY));
    AddBar( tmpX,
            10+Abs(tmpY),
            Abs(DifY/(1+System.Random(5)))
            {$IFNDEF D4},'',clTeeColor{$ENDIF});
    tmpX:=tmpX+StepX;
  end;
end;

{ this overrides default bottom origin calculation }
Function TBar3DSeries.PointOrigin(ValueIndex:Integer; SumAll:Boolean):Double;
begin
  result:=FOffsetValues.Value[ValueIndex];
end;

{ this makes this bar heigth to be: "offset" + "height" }
Function TBar3DSeries.GetOriginValue(ValueIndex:Integer):Double;
begin
  result:=inherited GetOriginValue(ValueIndex)+FOffsetValues.Value[ValueIndex];
end;

{ Register the Series at Chart gallery }
initialization
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
{
   RegisterTeeSeries( TBar3DSeries, TeeMsg_GalleryBar3D, TeeMsg_GallerySamples, 1 );
}
   RegisterTeeSeries( TBar3DSeries, TeeMsg_GalleryBar3D, TeeMsg_GallerySamples, 1, stBar3D );
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
finalization
   UnRegisterTeeSeries([TBar3DSeries]);
end.
