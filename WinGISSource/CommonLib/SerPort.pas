Unit SerPort;
     
Interface

Uses Classes,Controls,SysUtils,WinProcs,WinTypes;

Type ESerialPort   = Class(Exception);
                            
     TDTRControl        = (dtrDisable,dtrEnable,dtrHandshake);
     TParity            = (parNone,parOdd,parEven,parMark,parSpace);
     TRTSControl        = (rtsDisable,rtsEnable,rtsHandshake,rtsToggle);
     TStopBits          = (stopOne,stopOnePointFive,stopTwo);
     TXOnXOffHandshake  = (xonNone,xonInput,xonOutput,xonInputOutput);
     TCommEvent         = (ceBreak,ceCTS,ceDSR,ceError,ceRing,ceRLSD,
                           ceReceive,ceEventCharReceived,ceTXEmpty);
     TCommEvents        = Set of TCommEvent;
                                               
     TReadTimeouts      = Class(TPersistent)
      Private
       FConstant        : Integer;
       FInterval        : Integer;                   
       FMultiplier      : Integer;
      Published
       Property    Constant:Integer read FConstant write FConstant;
       Property    Interval:Integer read FInterval write FInterval;
       Property    Multiplier:Integer read FMultiplier write FMultiplier;
     end;

     TWriteTimeouts     = Class(TPersistent)
      Private
       FConstant        : Integer;
       FMultiplier      : Integer;
      Published
       Property    Constant:Integer read FConstant write FConstant;
       Property    Multiplier:Integer read FMultiplier write FMultiplier;
     end;

     TSerialPort        = Class(TComponent)
      Private
       FBaudRate        : Integer;
       FByteSize        : Integer;
       FCommEvents      : TCommEvents;
       FCTSHandshake    : Boolean;
       FDSRHandshake    : Boolean;
       FDTRControl      : TDTRControl;
       FDSRSensitifity  : Boolean;
       FEOFChar         : Char;
       FErrorChar       : Char;
       FEventChar       : Char;
       FEventMasks      : TCommEvents;
       FLogWindow       : Boolean;
       FInBufferSize    : Integer;
       FNullDiscard     : Boolean;
       {$IFDEF WIN32}
       FHandle          : THandle;
       FOnComm          : TThreadMethod;
       {$ELSE}
       FHandle          : Integer;
       FOnComm          : TNotifyEvent;
       {$ENDIF}
       FOutBufferSize   : Integer;
       FParity          : TParity;
       FReadTimeouts    : TReadTimeouts;
       FRTSControl      : TRTSControl;
       FStopBits        : TStopBits;
       FPortNumber      : Integer;
       FWriteTimeouts   : TWriteTimeouts;
       FXOffChar        : Char;
       FXOffLimit       : Word;
       FXOnChar         : Char;
       FXOnLimit        : Word;
       FXOnXOff         : TXOnXOffHandshake;
       {$IFDEF WIN32}
       FThread          : TThread;
       {$ELSE}
       FWindow          : TWinControl;
      {$ENDIF}
       Function    GetActive:Boolean;
       Function    GetBufferCount(AIndex:Integer):Integer;
       Function    GetDCB:TDCB;
       Function    GetLine(AIndex:Integer):Boolean;
       Procedure   SetActive(AActive:Boolean);
       Procedure   SetBaudRate(ABaudRate:Integer);
       Procedure   SetDCB(var DCB:TDCB);
       Procedure   SetEventMask(const NewMask:TCommEvents);
       Procedure   SetLine(AIndex:Integer;ASet:Boolean);
       Procedure   SetLogWindow(ALogWindow:Boolean);
       Procedure   SetParity(AParity:TParity);
       Procedure   SetPort(APort:Integer);
       Procedure   SetupPort;
       Procedure   CheckCommError;
      Public
       Constructor Create(AOwner:TComponent); override;
       Destructor  Destroy; override;
       Procedure   Clear;
       Procedure   Close;
       Property    CommEvents:TCommEvents read FCommEvents;
       Procedure   FlushInBuffer;
       Procedure   FlushOutBuffer;
       Procedure   GetBlock(var Buffer;BufferSize:Integer);
       {$IFDEF WIN32}
       Property    Handle:THandle read FHandle;
       {$ELSE}
       Property    Handle:Integer read FHandle;
       {$ENDIF}
       Property    InBufferCount:Integer index 0 read GetBufferCount;
       Property    OutBufferCount:Integer index 1 read GetBufferCount;
       Procedure   Open;
       Procedure   PutBlock(var Buffer;BufferSize:Integer);
       Function    Read(var Buffer;BufferSize:Integer):Integer;
       Function    ReadBlocking(var Buffer;BufferSize:Integer):Integer;
       Function    Write(var Buffer;BufferSize:Integer):Integer; overload;
       Function    Write(const Text:String):Integer; overload;
       Property    DTR:Boolean index 0 write SetLine;
       Property    RTS:Boolean index 1 write SetLine;
       Property    CTS:Boolean index 0 read GetLine;
       Property    DSR:Boolean index 1 read GetLine;
       Property    RING:Boolean index 2 read GetLine;
       Property    RLSD:Boolean index 3 read GetLine;
      Published
       Property    Active:Boolean read GetActive write SetActive default FALSE;
       Property    BaudRate:Integer read FBaudRate write SetBaudRate default 9600;
       Property    ByteSize:Integer read FByteSize write FByteSize default 8;
       Property    EventMask:TCommEvents read FEventMasks write SetEventMask;
       Property    CTSHandshake:Boolean read FCTSHandshake write FCTSHandshake default FALSE;
       Property    DSRHandshake:Boolean read FDSRHandshake write FDSRHandshake default FALSE;
       Property    DTRControl:TDTRControl read FDTRControl write FDTRControl default dtrEnable;
       Property    DSRSensitifity:Boolean read FDSRSensitifity write FDSRSensitifity default FALSE;
       Property    EOFChar:Char read FEOFChar write FEOFChar default ^Z;
       Property    EventChar:Char read FEventChar write FEventChar default #0;
       Property    ErrorChar:Char read FErrorChar write FErrorChar default #0;
       Property    LogWindow:Boolean read FLogWindow write SetLogWindow;
       Property    NullDiscard:Boolean read FNullDiscard write FNullDiscard default FALSE;
       {$IFDEF WIN32}
       Property    OnComm:TThreadMethod read FOnComm write FOnComm;
       {$ELSE}
       Property    OnComm:TNotifyEvent read FOnComm write FOnComm;
       {$ENDIF}
       Property    OutBufferSize:Integer read FOutBufferSize write FOutBufferSize default 8192;
       Property    Parity:TParity read FParity write SetParity default parNone;
       Property    Port:Integer read FPortNumber write SetPort default 1;
       Property    InBufferSize:Integer read FInBufferSize write FInBufferSize default 8192;
       Property    ReadTimeouts:TReadTimeouts read FReadTimeouts;
       Property    RTSControl:TRTSControl read FRTSControl write FRTSControl default rtsEnable;
       Function    Setup:Boolean;
       Property    StopBits:TStopBits read FStopBits write FStopBits default stopOne;
       Property    WriteTimeouts:TWriteTimeouts read FWriteTimeouts;
       Property    XOffLimit:Word read FXOffLimit write FXOffLimit default $FFFF;
       Property    XOffChar:Char read FXOffChar write FXOffChar default #17;
       Property    XOnChar:Char read FXOnChar write FXOnChar default #19;
       Property    XOnXOffHandshake:TXOnXOffHandshake read FXOnXOff write FXOnXOff default xonNone;
       Property    XOnLimit:Word read FXOnLimit write FXOnLimit default $FFFF;
     end;

Implementation

Uses Forms,NumTools,Messages,MultiLng;

{$IFDEF WIN32}
Const DefaultOverlapped : TOverlapped = (Offset : 0; OffsetHigh : 0; hEvent : 0);

Type TCommThread   = Class(TThread)
      Private
       FSerialPort : TSerialPort;
      Public
       Constructor Create(AOwner:TSerialPort);
       Destructor  Destroy; override;
       Procedure   Execute; override;
     end;
{$ELSE}
Const INVALID_HANDLE_VALUE = -1;

Type TCommWindow   = Class(TWinControl)
      Private
       FSerialPort : TSerialPort;
       Procedure   CreateParams(var Params:TCreateParams); override;
       Procedure   WMCommNotify(var Msg:TWMCommNotify); message wm_CommNotify;
      Public
       Property    SerialPort:TSerialPort read FSerialPort write FSerialPort;
     end;
{$ENDIF}

Procedure Check(PortNumber:Integer;Status:Boolean;ErrorNumber:Integer);
var MsgText        : PChar;
    Exception      : ESerialPort;
begin
  if not Status then begin
    {$IFDEF WIN32}
    FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER or FORMAT_MESSAGE_FROM_SYSTEM, NIL,
	      GetLastError(),SUBLANG_DEFAULT Shl 10 or LANG_NEUTRAL,@MsgText,0,NIL);
    Exception:=ESerialPort.Create(Format(MlgStringList['SerialPort',ErrorNumber],
        [PortNumber,MsgText]));
    LocalFree(Integer(MsgText));
    {$ELSE}
    Exception:=ESerialPort.CreateFmt(MlgStringList['SerialPort',ErrorNumber],[PortNumber,'']);
    {$ENDIF}
    Raise Exception;
  end;
end;

Constructor TSerialPort.Create(AOwner:TComponent);
begin
  inherited Create(AOwner);
  FHandle:=INVALID_HANDLE_VALUE;
  FBaudRate:=9600;
  FByteSize:=8;
  FDTRControl:=dtrEnable;
  FEOFChar:=^Z;
  FInBufferSize:=8192;
  FOutBufferSize:=8192;
  FPortNumber:=1;
  FReadTimeouts:=TReadTimeouts.Create;
  FRTSControl:=rtsEnable;
  FXOnLimit:=$FFFF;
  FXOffLimit:=$FFFF;
  FWriteTimeouts:=TWriteTimeouts.Create;
  FXOnChar:=#17;
  FXOffChar:=#19;
  {$IFNDEF WIN32}
  FWindow:=TCommWindow.Create(NIL);
  TCommWindow(FWindow).SerialPort:=Self;
  {$ENDIF}
end;

Destructor TSerialPort.Destroy;
begin
  Close;
  FReadTimeouts.Free;
  FWriteTimeouts.Free;
  {$IFNDEF WIN32}
  FWindow.Free;
  {$ENDIF}
  inherited Destroy;
end;

Procedure TSerialPort.Open;
{$IFDEF WIN32}
var Errors         : LongWord;
{$ELSE}
var Errors         : LongInt;
    AText          : Array[0..10] of Char;
    ErrorNumber    : Integer;
{$ENDIF}
    Status         : TComStat;
begin
  if FHandle=INVALID_HANDLE_VALUE then begin
    { create file-handle }
    {$IFDEF WIN32}
    FHandle:=CreateFile(PChar('COM'+IntToStr(FPortNumber)),
        GENERIC_READ or GENERIC_WRITE,0,NIL,
        OPEN_EXISTING,FILE_FLAG_OVERLAPPED,0);
    { throw error if not successfull }
    Check(FPortNumber,FHandle<>INVALID_HANDLE_VALUE,1);
    {$ELSE}
    FHandle:=OpenComm(StrPCopy(AText,'COM'+IntToStr(FPortNumber)),FInBufferSize,
        FOutBufferSize);
    if FHandle<0 then begin
      case FHandle of
        ie_BadID    : ErrorNumber:=10;
        ie_BaudRate : ErrorNumber:=11;
        ie_ByteSize : ErrorNumber:=12;
        ie_Default  : ErrorNumber:=13;
        ie_Hardware : ErrorNumber:=14;
        ie_Memory   : ErrorNumber:=15;
        ie_NOPen    : ErrorNumber:=16;
        ie_Open     : ErrorNumber:=17;              
        else ErrorNumber:=1;
      end;
      FHandle:=INVALID_HANDLE_VALUE;
      Raise ESerialPort.CreateFmt(MlgStringList['SerialPort',1],
          [FPortNumber,MlgStringList['SerialPort',ErrorNumber]]);
    end;
    {$ENDIF}
    { setup comm-parameters }
    SetupPort;
    {$IFDEF WIN32}
    { clear all buffers and events }
    PurgeComm(FHandle,PURGE_RXABORT or PURGE_TXABORT);
    GetCommMask(FHandle,Errors);
    ClearCommError(FHandle,Errors,@Status);
    { create a thread for waiting for events of the port }
    FThread:=TCommThread.Create(Self);              
    {$ELSE}                                              
    FlushComm(FHandle,0);    
    FlushComm(FHandle,1);
    GetCommEventMask(FHandle,-1);             
    GetCommError(FHandle,Status);                                       
    Check(FPortNumber,EnableCommNotification(FHandle,FWindow.Handle,-1,-1),1);
    {$ENDIF}
  end;
end;

{******************************************************************************+
  Procedure TSerialPort.SetDCB
--------------------------------------------------------------------------------
  Setups the object-properties from a dcb-structure. The comm-settings are
  changed if the port is currently opened.
+******************************************************************************}
Procedure TSerialPort.SetDCB(var DCB:TDCB);
begin
  with DCB do begin
    FBaudRate:=BaudRate;
    { set binary-mode and enable parity }
    FCTSHandshake:=Flags and $000004<>0;
    FDSRHandshake:=Flags and $000008<>0;
    case Flags and $000020 of
      $000010 : FDTRControl:=dtrEnable;
      $000020 : FDTRControl:=dtrHandshake;
      else FDTRControl:=dtrDisable;
    end;
    FDSRSensitifity:=Flags and $000040<>0;
    { !?!?fTXContinueOnXoff }
    case Flags and $000100 of
      $000100 : FXOnXOff:=xonOutput;
      $000200 : FXOnXOff:=xonInput;
      $000300 : FXOnXOff:=xonInputOutput;
      else FXOnXOff:=xonNone;
    end;  
    FNullDiscard:=Flags and $000800<>0;
    case Flags and $003000 of
      $001000 : FRTSControl:=rtsEnable;
      $002000 : FRTSControl:=rtsHandshake;
      $003000 : FRTSControl:=rtsToggle;
      else FRTSControl:=rtsDisable;
    end;
    FXonLimit:=XOnLim;
    FXOffLimit:=XOffLimit;
    FByteSize:=ByteSize;
    FParity:=TParity(Parity);
    if StopBits=ONESTOPBIT then FStopBits:=stopOne
    else if StopBits=ONE5STOPBITS then FStopBits:=stopOnePointFive
    else FStopBits:=stopTwo;
    FXonChar:=XOnChar;
    FXoffChar:=XOffChar;
    FErrorChar:=ErrorChar;
    FEOFChar:=EOFChar;
    FEventChar:=EvtChar;
  end;
  {$IFDEF WIN32}
  if FHandle<>INVALID_HANDLE_VALUE then Check(FPortNumber,SetCommState(FHandle,DCB),1);
  {$ELSE}
  if FHandle<>INVALID_HANDLE_VALUE then Check(FPortNumber,SetCommState(DCB)=0,1);
  {$ENDIF}
end;

Function TSerialPort.GetDCB:TDCB;
begin
  { query default settings for the comm-port }
  {$IFDEF WIN32}
  Result.DCBLength:=SizeOf(Result);
  Check(FPortNumber,GetCommState(FHandle,Result),1);
  {$ELSE}
  Check(FPortNumber,GetCommState(FHandle,Result)=0,1);
  {$ENDIF}
  with Result do begin
    BaudRate:=FBaudRate;
    {$IFDEF WIN32}
    { set binary-mode and enable parity }
    if FParity=parNone then Flags:=$0001
    else Flags:=$00003;
    if FCTSHandshake then Flags:=Flags or $000004;
    if FDSRHandshake then Flags:=Flags or $000008
    else if FDTRControl=dtrEnable then Flags:=Flags or $000010
    else if FDTRControl=dtrHandshake then Flags:=Flags or $000020;
    if FDSRSensitifity then Flags:=Flags or $000040;
    { !?!?fTXContinueOnXoff }
    if FXOnXOff=xonOutput then Flags:=Flags or $000100
    else if FXOnXOff=xonInput then Flags:=Flags or $000200
    else if FXOnXOff=xonInputOutput then Flags:=Flags or $000300;
    if FErrorChar<>#0 then Flags:=Flags or $000400;
    if FNullDiscard then Flags:=Flags or $000800;
    if FRTSControl=rtsEnable then Flags:=Flags or $001000
    else if FRTSControl=rtsHandshake then Flags:=Flags or $002000
    else if FRTSControl=rtsToggle then Flags:=Flags or $003000;
    {$ELSE}
    Flags:=$0001;
    if FRTSControl=rtsHandshake then Flags:=Flags or $004000
    else if FRTSControl<>rtsEnable then Flags:=Flags or $000002;
    if FParity<>parNone then Flags:=Flags or $000004;
    if FCTSHandshake then Flags:=Flags or $000008;
    if FDSRHandshake then Flags:=Flags or $000010;
    if FDTRControl=dtrHandshake then Flags:=Flags or $002000
    else if FDTRControl<>dtrEnable then Flags:=Flags or $000080;
    if FXOnXOff=xonOutput then Flags:=Flags or $000100
    else if FXOnXOff=xonInput then Flags:=Flags or $000200
    else if FXOnXOff=xonInputOutput then Flags:=Flags or $000300;
    if FErrorChar<>#0 then Flags:=Flags or $000400;
    if FNullDiscard then Flags:=Flags or $000800;
    if ceEventCharReceived in FEventMasks then Flags:=Flags or $001000; 
    {$ENDIF}
    XonLim:=FXOnLimit;
    XOffLim:=FXOffLimit;
    ByteSize:=FByteSize;
    Parity:=Integer(FParity);
    if FStopBits=stopOne then StopBits:=ONESTOPBIT
    else if FStopBits=stopOnePointFive then StopBits:=ONE5STOPBITS
    else StopBits:=TWOSTOPBITS;
    XOnChar:=FXOnChar;
    XOffChar:=FXOffChar;
    ErrorChar:=FErrorChar;
    EOFChar:=FEOFChar;
    EvtChar:=FEventChar;
  end;
end;

Procedure TSerialPort.SetupPort;
var DCB            : TDCB;
    {$IFDEF WIN32}
    Timeouts       : TCommTimeouts;
    {$ENDIF}
    EventMask      : Integer;
begin
  if FHandle<>INVALID_HANDLE_VALUE then try
    {$IFDEF WIN32}
    { setup buffer-sizes }
    Check(FPortNumber,SetupComm(FHandle,FInBufferSize,FOutBufferSize),1);
    DCB:=GetDCB;
    Check(FPortNumber,SetCommState(FHandle,DCB),1);
    Timeouts.ReadIntervalTimeout:=FReadTimeouts.Interval;
    Timeouts.ReadTotalTimeoutConstant:=FReadTimeouts.Constant;
    Timeouts.ReadTotalTimeoutMultiplier:=FReadTimeouts.Multiplier;
    Timeouts.WriteTotalTimeoutConstant:=FWriteTimeouts.Constant;
    Timeouts.WriteTotalTimeoutMultiplier:=FWriteTimeouts.Multiplier;
    Check(FPortNumber,SetCommTimeouts(FHandle,Timeouts),1);
    {$ELSE}
    DCB:=GetDCB;
    Check(FPortNumber,SetCommState(DCB)=0,1);
    {$ENDIF}
    EventMask:=0;
    if ceBreak in FEventMasks then EventMask:=EventMask or EV_BREAK;
    if ceCTS in FEventMasks then EventMask:=EventMask or EV_CTS;
    if ceDSR in FEventMasks then EventMask:=EventMask or EV_DSR;
    if ceError in FEventMasks then EventMask:=EventMask or EV_ERR;
    if ceRing in FEventMasks then EventMask:=EventMask or EV_RING;
    if ceRLSD in FEventMasks then EventMask:=EventMask or EV_RLSD;
    if ceReceive in FEventMasks then EventMask:=EventMask or EV_RXChar;
    if ceEventCharReceived in FEventMasks then EventMask:=EventMask or EV_RXFLAG;
    if ceTXEmpty in FEventMasks then EventMask:=EventMask or EV_TXEMPTY;
    {$IFDEF WIN32}
    Check(FPortNumber,SetCommMask(FHandle,EventMask),1);
    {$ELSE}
    EventMask:=EventMask or EV_RLSDS or EV_CTSS or EV_DSRS;
    Check(FPortNumber,SetCommEventMask(FHandle,EventMask)<>NIL,1);
    {$ENDIF}
  except
    if FHandle<>INVALID_HANDLE_VALUE then begin
      {$IFDEF WIN32}
      CloseHandle(FHandle);
      {$ELSE}
      CloseComm(FHandle);
      {$ENDIF}
      FHandle:=INVALID_HANDLE_VALUE;
    end;
    Raise;
  end;
end;

Procedure TSerialPort.Close;
begin
  if FHandle<>INVALID_HANDLE_VALUE then begin
    {$IFDEF WIN32}
    { free the thread }
    FThread.Free;
    { close the port }
    CloseHandle(FHandle);
    {$ELSE}
    EnableCommNotification(FHandle,0,0,0);
    CloseComm(FHandle);
    {$ENDIF}
    FHandle:=INVALID_HANDLE_VALUE;
  end;
end;

Function TSerialPort.GetActive:Boolean;
begin
  Result:=FHandle<>INVALID_HANDLE_VALUE;
end;

Procedure TSerialPort.SetActive(AActive:Boolean);
begin
  if AActive then Open
  else Close;
end;

Procedure TSerialPort.SetPort(APort:Integer);
begin
  if APort<>FPortNumber then begin
    Close;
    FPortNumber:=APort;
  end;
end;

Procedure TSerialPort.SetBaudRate(ABaudRate:Integer);
begin
  if ABaudRate<>FBaudRate then begin
    Close;
    FBaudRate:=ABaudRate;
  end;
end;

Procedure TSerialPort.SetParity(AParity:TParity);
begin
  if AParity<>FParity then begin
    Close;
    FParity:=AParity;
  end;
end;

Procedure TSerialPort.CheckCommError;
{$IFNDEF WIN32}
var Status         : TComStat;
    CommError      : Word;
    ErrorNumber    : Integer;
{$ENDIF}
begin
  {$IFDEF WIN32}
 // Check(FPortNumber,FHandle=INVALID_HANDLE_VALUE,2);
  {$ELSE}
  if FHandle<>INVALID_HANDLE_VALUE then begin
    CommError:=GetCommError(FHandle,Status);
    if CommError<>0 then Raise ESerialPort.CreateFmt(MlgStringList['SerialPort',
        2],[FPortNumber,IntToHex(CommError,4)]);
  end;
  {$ENDIF}  
end;

{$IFDEF WIN32}
Function TSerialPort.Read(var Buffer;BufferSize:Integer):Integer;
var BytesRead      : LongWord;
    Overlapped     : TOverlapped;
begin
  BufferSize:=Min(BufferSize,InBufferCount);
  if BufferSize=0 then Result:=0
  else begin
    Overlapped:=DefaultOverlapped;
    ReadFile(FHandle,Buffer,BufferSize,BytesRead,@Overlapped);
    GetOverlappedResult(FHandle,Overlapped,BytesRead,TRUE);
    Result:=BytesRead;
    CheckCommError;
  end;
end;

Function TSerialPort.ReadBlocking(var Buffer;BufferSize:Integer):Integer;
var BytesRead      : LongWord;
    Overlapped     : TOverlapped;
begin
  if BufferSize=0 then Result:=0
  else begin
    Overlapped:=DefaultOverlapped;
    ReadFile(FHandle,Buffer,BufferSize,BytesRead,@Overlapped);
    GetOverlappedResult(FHandle,Overlapped,BytesRead,TRUE);
    Result:=BytesRead;
    CheckCommError;
  end;
end;

Procedure TSerialPort.GetBlock(var Buffer;BufferSize:Integer);
var BytesRead      : LongWord;
    Overlapped     : TOverlapped;
begin
  if BufferSize>0 then begin
    Overlapped:=DefaultOverlapped;
    ReadFile(FHandle,Buffer,BufferSize,BytesRead,@Overlapped);
    GetOverlappedResult(FHandle,Overlapped,BytesRead,TRUE);
    CheckCommError;
  end;
end;

Function TSerialPort.Write(var Buffer;BufferSize:Integer):Integer;
var BytesWritten   : LongWord;
    Overlapped     : TOverlapped;
begin
  Overlapped:=DefaultOverlapped;
  WriteFile(FHandle,Buffer,BufferSize,BytesWritten,@Overlapped);
  GetOverlappedResult(FHandle,Overlapped,BytesWritten,TRUE);
  Result:=BytesWritten;
  CheckCommError;
end;

procedure TSerialPort.PutBlock(var Buffer; BufferSize: Integer);
var BytesWritten   : LongWord;
    Overlapped     : TOverlapped;
begin
  Overlapped:=DefaultOverlapped;
  WriteFile(FHandle,Buffer,BufferSize,BytesWritten,@Overlapped);
  GetOverlappedResult(FHandle,Overlapped,BytesWritten,TRUE);
  CheckCommError;
end;

Function TSerialPort.Write(const Text:String):Integer;
begin
  Result:=Write(PChar(AnsiString(Text))^,Length(Text));
end;

Constructor TCommThread.Create(AOwner:TSerialPort);
begin
  inherited Create(AOwner.EventMask=[]);
  FSerialPort:=AOwner;
end;
                                      
Destructor TCommThread.Destroy;
begin
  // clear event-mask to force WaitCommEvent to return
  SetCommMask(FSerialPort.FHandle,0);
  inherited Destroy;
end;

Procedure TCommThread.Execute;
var Events         : DWORD;
begin
  while not Terminated do if FSerialPort.FHandle<>INVALID_HANDLE_VALUE then begin
    // wait for comm-event
    if WaitCommEvent(FSerialPort.FHandle,Events,NIL) and not Terminated then begin
      // determine events that occurred
      FSerialPort.FCommEvents:=[];
      if Events and EV_BREAK<>0 then Include(FSerialPort.FCommEvents,ceBreak);
      if Events and EV_CTS<>0 then Include(FSerialPort.FCommEvents,ceCTS);
      if Events and EV_DSR<>0 then Include(FSerialPort.FCommEvents,ceDSR);
      if Events and EV_ERR<>0 then Include(FSerialPort.FCommEvents,ceError);
      if Events and EV_RING<>0 then Include(FSerialPort.FCommEvents,ceRing);
      if Events and EV_RLSD<>0 then Include(FSerialPort.FCommEvents,ceRLSD);
      if Events and EV_RXChar<>0 then Include(FSerialPort.FCommEvents,ceReceive);
      if Events and EV_RXFLAG<>0 then Include(FSerialPort.FCommEvents,ceEventCharReceived);
      if Events and EV_TXEMPTY<>0 then Include(FSerialPort.FCommEvents,ceTXEmpty);
      // call event-handler-function
      if Assigned(FSerialPort.FOnComm) then FSerialPort.OnComm;
    end;
  end;
end;

{$ELSE}

Function TSerialPort.Read(var Buffer;BufferSize:Integer):Integer;
begin
  if BufferSize=0 then Result:=0  
  else Result:=ReadComm(FHandle,@Buffer,BufferSize);
  CheckCommError;
end;

Function TSerialPort.ReadBlocking(var Buffer;BufferSize:Integer):Integer;
begin
  if BufferSize=0 then Result:=0
  else Result:=ReadComm(FHandle,@Buffer,BufferSize);
  CheckCommError;
end;                                

Function TSerialPort.Write(var Buffer;BufferSize:Integer):Integer;
begin
  Result:=WriteComm(FHandle,@Buffer,BufferSize);
  CheckCommError;                    
end;
{$ENDIF}                

Function TSerialPort.GetBufferCount(AIndex:Integer):Integer;
var Status         : TComStat;
{$IFDEF WIN32}
    Errors         : DWORD;
begin
  ClearCommError(FHandle,Errors,@Status);
{$ELSE}
    Errors         : Word;
begin
  Errors:=GetCommError(FHandle,Status);
{$ENDIF}
  if AIndex=0 then Result:=Status.cbInQue
  else Result:=Status.cbOutQue;
end;

Procedure TSerialPort.SetLine(AIndex:Integer;ASet:Boolean);
begin
  case AIndex of
    0: if ASet then EscapeCommFunction(FHandle,SETDTR)
       else EscapeCommFunction(FHandle,CLRDTR);
    1: if ASet then EscapeCommFunction(FHandle,SETRTS)
       else EscapeCommFunction(FHandle,CLRRTS);
  end;                    
end;

Function TSerialPort.GetLine(AIndex:Integer):Boolean;
{$IFDEF WIN32}
var ModemStatus    : DWORD;
begin
  GetCommModemStatus(FHandle,ModemStatus);
  Case AIndex of
    0: Result:=ModemStatus and MS_CTS_ON<>0;
    1: Result:=ModemStatus and MS_DSR_ON<>0;
    2: Result:=ModemStatus and MS_RING_ON<>0;
    3: Result:=ModemStatus and MS_RLSD_ON<>0;
    else Result:=FALSE;
  end;
{$ELSE}
var ModemStatus    : Word;
begin
  ModemStatus:=GetCommEventMask(FHandle,0);
  Case AIndex of
    0: Result:=ModemStatus and EV_CTSS<>0;
    1: Result:=ModemStatus and EV_DSRS<>0;
    2: Result:=ModemStatus and EV_RING<>0;
    3: Result:=ModemStatus and EV_RLSDS<>0;
    else Result:=FALSE;
  end;
{$ENDIF}
end;

Procedure TSerialPort.FlushInBuffer;
begin
  {$IFDEF WIN32}
  PurgeComm(FHandle,PURGE_RXABORT or PURGE_RXCLEAR);
  {$ELSE}
  FlushComm(FHandle,1);
  {$ENDIF}
end;

Procedure TSerialPort.FlushOutBuffer;
begin
  {$IFDEF WIN32}
  PurgeComm(FHandle,PURGE_TXABORT or PURGE_TXCLEAR);
  {$ELSE}
  FlushComm(FHandle,0);
  {$ENDIF}
end;

Function TSerialPort.Setup;
{$IFDEF WIN32}
var Config         : TCommConfig;
    Size           : LongWord;
    WasOpened      : Boolean;
begin
  // close the
  WasOpened:=FHandle<>INVALID_HANDLE_VALUE;
  Close;
  // setup fields of
  Size:=SizeOf(Config);
  GetDefaultCommConfig(PChar('Com'+IntToStr(FPortNumber)),Config,Size);
  // set current configuration
  Config.DCB:=GetDCB;
  // show the comm-configuration-dialog
  Result:=CommConfigDialog(PChar('Com'+IntToStr(FPortNumber)),Application.MainForm.Handle,Config);
  // activate the new settings
  if Result then begin
    SetDCB(Config.dcb);
    if WasOpened then Open;
  end;
{$ELSE}
begin
{$ENDIF}
end;

Procedure TSerialPort.SetLogWindow(ALogWindow:Boolean);
begin
  if ALogWindow<>FLogWindow then begin
    FLogWindow:=ALogWindow;
{    if FLogWindow then begin
      if FLoggingWindow=NIL then begin
        FLoggingWindow:=TSerialLogForm.Create(Application);
        FLoggingWindow.Show;
      end;                                              tbi
    end;}       
  end;
end;

{******************************************************************************+
  Procedure TSerialPort.Clear
--------------------------------------------------------------------------------
  Clears all buffers (send and receive) and clears the event-mask. 
+******************************************************************************}
Procedure TSerialPort.Clear;
{$IFDEF WIN32}
var Errors         : LongWord;
begin
  { clear all buffers and events }
  PurgeComm(FHandle,PURGE_RXABORT or PURGE_RXCLEAR or PURGE_TXABORT or PURGE_TXCLEAR);
  ClearCommError(FHandle,Errors,NIL);
{$ELSE}
var Status         : TComStat;
begin
  { clear all buffers and events }
  FlushComm(FHandle,0);
  FlushComm(FHandle,1);
  GetCommError(FHandle,Status);
{$ENDIF}
end;

Procedure TSerialPort.SetEventMask(const NewMask:TCommEvents);
begin
  if FEventMasks<>NewMask then begin
    FEventMasks:=NewMask;
    SetupPort;
  end;
end;

{$IFNDEF WIN32}
Procedure TCommWindow.WMCommNotify(var Msg:TWMCommNotify);
var Events         : Word;
begin
  if Msg.Device=FSerialPort.Handle then begin
    if Msg.NotifyStatus and CN_EVENT<>0 then begin
      Events:=GetCommEventMask(FSerialPort.Handle,EV_RXFLAG);
      FSerialPort.FCommEvents:=[];
      if Events and EV_BREAK<>0 then Include(FSerialPort.FCommEvents,ceBreak);
      if Events and EV_CTS<>0 then Include(FSerialPort.FCommEvents,ceCTS);
      if Events and EV_DSR<>0 then Include(FSerialPort.FCommEvents,ceDSR);
      if Events and EV_ERR<>0 then Include(FSerialPort.FCommEvents,ceError);
      if Events and EV_RING<>0 then Include(FSerialPort.FCommEvents,ceRing);
      if Events and EV_RLSD<>0 then Include(FSerialPort.FCommEvents,ceRLSD);
      if Events and EV_RXChar<>0 then Include(FSerialPort.FCommEvents,ceReceive);
      if Events and EV_RXFLAG<>0 then Include(FSerialPort.FCommEvents,ceEventCharReceived);
      if Events and EV_TXEMPTY<>0 then Include(FSerialPort.FCommEvents,ceTXEmpty);
      if Assigned(FSerialPort.FOnComm) then FSerialPort.OnComm(FSerialPort);
      Msg.Result:=0;
    end;
  end;
end;

Procedure TCommWindow.CreateParams(var Params:TCreateParams);
begin
  inherited CreateParams(Params);
  Params.Style:=0;
end;
{$ENDIF}

end.

