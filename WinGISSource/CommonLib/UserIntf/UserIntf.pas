{******************************************************************************+
  Unit UserIntf
--------------------------------------------------------------------------------
  Autor: Martin Forst
--------------------------------------------------------------------------------
  Interface zum String-File.
  - Einlesen des String-Files
  - Verwalten der Men�s und Toolbars
  - Verwalten der String-Liste
+******************************************************************************}
unit UserIntf;

interface

uses
  Classes, Controls, Dialogs, ExtCtrls, Forms, Hash, MenuList, MenuFn, Menus,
  Messages, MultiLng, Rollup, WCtrls, Windows, ComCtrls, HTMLHelp, MultiLngFile,
  TB97, UIntfCtrls;

const
  uiMenus = 0;
  uiCursor = 1;
  uiStatusBar = 2;
  uiContextToolbars = 3;
  uiPredefined = [0, 1, 2, 3];

type
  TUpdateInfo = set of Byte;
  TUpdateProc = procedure(const AWhat: TUpdateInfo) of object;

  TUpdateCustomEvent = procedure(Sender: TObject; const PendingUpdates: TUpdateInfo) of object;

  TUIInitialize = class(TComponent)
  private
    FBoundLines: Boolean;
       // true if a client-edge shall be added to the MDI-client for
       // MDI-applications or to the main-form for SDI-applications
    FClientEdge: Boolean;
    FFileName: string;
    FFileHistory: TMenuHistoryList;
       // true if the application has context-toolbars
    FHasContextToolbar: Boolean;
    FHTMLHelp: Boolean;
    FIniFileName: string;
    FLanguageIni: string;
    FLanguageIniFile: string;
    FOnAppActivate: TNotifyEvent;
    FOnFormChange: TNotifyEvent;
    FOnMessages: TMessageEvent;
    FOnSetup: TNotifyEvent;
    FSaveWindowSize: Boolean;
    FShowShortcuts: Boolean;
    FStatusBar: TWStatusBar;
  protected
    procedure Loaded; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property AlwaysShowClientEdge: Boolean read FClientEdge write FClientEdge default FALSE;
    property BoundLines: Boolean read FBoundLines write FBoundLines;
    property ContextToolbar: Boolean read FHasContextToolbar write FHasContextToolbar default FALSE;
    property FileHistoryList: TMenuHistoryList read FFileHistory write FFileHistory;
    property HTMLHelp: Boolean read FHTMLHelp write FHTMLHelp;
    property IniFileName: string read FIniFileName write FIniFileName;
    property LanguageIniLocation: string read FLanguageIni write FLanguageIni;
    property LanguageIniFileLocation: string read FLanguageIniFile write FLanguageIniFile;
    property OnActiveFormChange: TNotifyEvent read FOnFormChange write FOnFormChange;
    property OnApplicationActivate: TNotifyEvent read FOnAppActivate write FOnAppActivate;
    procedure OnLanguageChanged(Sender: TObject);
    property OnMessages: TMessageEvent read FOnMessages write FOnMessages;
    property OnSetup: TNotifyEvent read FOnSetup write FOnSetup;
    property SaveMainWindowSize: Boolean read FSaveWindowSize write FSaveWindowSize default FALSE;
    property ShowShortcutsInTooltips: Boolean read FShowShortcuts write FShowShortcuts default FALSE;
    property StatusBar: TWStatusBar read FStatusBar write FStatusBar;
    property StrFileName: string read FFileName write FFileName;
  end;

  TUIExtension = class(TComponent)
  private
    FContextToolbar: string;
    FDefaultContextToolbar: string;
    FMenuName: string;
    FOnMDIActivate: TNotifyEvent;
    FOnMDIDeactivate: TNotifyEvent;
    FOnHidden: TNotifyEvent;
    FOnShown: TNotifyEvent;
    FOnUpdateContextBars: TNotifyEvent;
    FOnUpdateCursor: TNotifyEvent;
    FOnUpdateCustom: TUpdateCustomEvent;
    FOnUpdateMenus: TNotifyEvent;
    FOnUpdateStatus: TNotifyEvent;
    FPopupMenuName: string;
    function GetMainMenu: TUIMainMenu;
    function GetPopupMenu: TUIPopupMenu;
    procedure DoPopup(Sender: TObject);
    procedure DoUpdateContextToolbars;
    procedure DoUpdateCursor;
    procedure DoUpdateCustom(const PendingUpdates: TUpdateInfo);
    procedure DoUpdateMenus;
    function DoUpdateStatusbar: Boolean;
  protected
    procedure Loaded; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property MainMenu: TUIMainMenu read GetMainMenu;
    property PopupMenu: TUIPopupMenu read GetPopupMenu;
  published
    property ContextToolbar: string read FContextToolbar write FContextToolbar;
    property DefaultContextToolbar: string read FDefaultContextToolbar write FDefaultContextToolbar;
    property MainMenuName: string read FMenuName write FMenuName;
    property OnHidden: TNotifyEvent read FOnHidden write FOnHidden;
    procedure OnLanguageChanged(Sender: TObject);
    property OnMDIActivate: TNotifyEvent read FOnMDIActivate write FOnMDIActivate;
    property OnMDIDeactivate: TNotifyEvent read FOnMDIDeactivate write FOnMDIDeactivate;
    property OnShown: TNotifyEvent read FOnShown write FOnShown;
    property OnUpdateContextToolbars: TNotifyEvent read FOnUpdateContextBars write FOnUpdateContextBars;
    property OnUpdateCursor: TNotifyEvent read FOnUpdateCursor write FOnUpdateCursor;
    property OnUpdateCustom: TUpdateCustomEvent read FOnUpdateCustom write FOnUpdateCustom;
    property OnUpdateMenus: TNotifyEvent read FOnUpdateMenus write FOnUpdateMenus;
    property OnUpdateStatusbar: TNotifyEvent read FOnUpdateStatus write FOnUpdateStatus;
    property PopupMenuName: string read FPopupMenuName write FPopupMenuName;
  end;

     {*************************************************************************+
       Class TUserInterface
     ---------------------------------------------------------------------------
       Verwaltet die Men�s, PopupMen�s und Toolbars des Projekts. Enth�lt
       Routinen zum Einlesen des String-Files und zum Updaten des Userinterfaces.
     ---------------------------------------------------------------------------
       FileName = Name des String-Files
       MainMenus = Liste der im String-File definierten String-Files
       PopupMenus = Liste der im String-File definierten Popup-Men�s
       Toolbars = Liste der im String-File definierten Toolbars
       ToolbarCount = Anzahl der definierten Toolbars
       ToolbarDialog = Dialog zum Ein/Ausschalten der Toolbars
       ToolbarLists = Liste aller auf Toolbars enthaltenen Comboboxen
       ToolbarListCount = Anzahl der auf Toolbars enthaltenen Comboboxen
       UpdateProc = Methode die von Update aufgerufen wird
     ---------------------------------------------------------------------------
       ReadFile = Routine zum Einlesen des String-Files
       Update = L�st ein Update des User-Interfaces (Menus, Statuszeile) aus
     +*************************************************************************}
  TUserInterface = class(TComponent)
  private
    FClientHandle: THandle;
       // pointer to the context-toolbar-component
    FContextToolbar: TUIToolbar;
       // list of all toolbars added to the context-toolbar
    FContextToolbars: TStringList;
    FEscPressed: Boolean;
       // name of the language-file
    FFileName: string;
       // pointer to the file-history-list component
    FFileHistory: TMenuHistoryList;
       // true if the focus is currently in a menu or popup-menu
    FInMenuLoop: Boolean;
       // name of the ini-file to read application-settings from
    FIniFileName: string;
       // currently selected user-interface-language
    FLanguage: LCID;
       // list of all languages a language-file exists for
    FLanguages: TStringList;
       // list of available main- and popup.menus
    FMenus: TList;
       // main-form of the application
    FMainForm: TForm;
       // handle of the main-window of the application
    FMainHandle: THandle;
       // pointer to the new main-window window-function
    FNewMainWndProc: Pointer;
       // pointer to the original main-window window-function
    FOldMainWndProc: Pointer;
       // pointer to the windows default MDI-client window-function
    FDefClientWndProc: Pointer;
       // pointer to the original window-function of the MDI-client
    FOldClientWndProc: Pointer;
       // pointer to the new window-function of the MDI-client
    FNewClientWndProc: Pointer;
       // updates currently waiting for processing
    FPendingUpdates: TUpdateInfo;
       // list of all rollups of the application or the classes of the rollups
    FRollups: TList;
       // true if the rollups have been created, other wise FRollups contains
       // a list of rollup-classes
    FRollupsCreated: Boolean;
    FStatusText: string;
    FToolbars: TList;
    FUpdateProc: TUpdateProc;
    FWaitCursorCount: Integer;
    FTopControlBar: TDock97;
    FPopupMenu: TPopupMenu;
    FUIInitialize: TUIInitialize;
    FHelpHandler: THelpHandler;
    procedure ClientWindowHook(var Message: TMessage);
    procedure DoShowHint(Sender: TObject);
    function GetLanguageCount: Integer;
    function GetLanguages(AIndex: Integer): LCID;
    function GetMainMenu(const MenuName: string): TUIMainMenu;
    function GetPopupMenu(const MenuName: string): TUIPopupMenu;
    function GetRollupCount: Integer;
    function GetRollup(AIndex: Integer): TRollupForm;
    function GetRollupNamed(const AName: string): TRollupForm;
    function GetToolbar(AIndex: Integer): TUIToolbar;
    function GetToolbarNamed(const AName: string): TUIToolbar;
    function GetToolbarCount: Integer;
    procedure MainWindowHook(var Message: TMessage);
    procedure RehookRollupValidators;
    procedure RemoveContextToolbars;
    procedure SetLanguage(ALanguage: LCID);
    procedure SetLanguageFile(AFile: string);
    procedure SetMainForm(AForm: TForm);
    procedure SetStatusText(const AText: string);
    procedure UpdateTimerEvent;
  private
    procedure DoActiveFormChanged(Sender: TObject);
    procedure DoMessages(var Msg: tagMsg; var Handled: Boolean);
    procedure DoPopup(Sender: TObject);
    procedure DoShowHideToolbar(Sender: TObject);
    procedure DoUpdateWindowLists;
    property MainForm: TForm read FMainForm write SetMainForm;
    function ReadFile(const FileName: string; var Language: LCID): Boolean;
    procedure SetUIInitialize(AUIInitialize: TUIInitialize);
    procedure UpdateRollupMenus;
    procedure WMMenuSelect(var Msg: TWMMenuSelect);
  public
    F3DChild: TForm;
    F3DActive: Boolean;
        {+++Brovak}
    TopB, LeftB, RightB: TDock97;
       {---Brovak}

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure AddToolbars(AddContextToolbar: Boolean);
    procedure AddContextToolbar(const AName: string);
    procedure AddRollup(ARollup: TRollupFormClass);
    procedure AddRollups;
    procedure BeginWaitCursor;
    property ContextToolbar: TUIToolbar read FContextToolbar write FContextToolbar;
    procedure EndWaitCursor;
    property EscPressed: Boolean read FEscPressed write FEscPressed;
    property FileHistoryList: TMenuHistoryList read FFileHistory write FFileHistory;
    property IniFileName: string read FIniFileName write FIniFileName;
    property LanguageCount: Integer read GetLanguageCount;
    property Languages[AIndex: Integer]: LCID read GetLanguages;
    property Language: LCID read FLanguage write SetLanguage;
    property LanguageFile: string read FFileName write SetLanguageFile;
    property MainMenus[const MenuName: string]: TUIMainMenu read GetMainMenu;
    property PopupMenus[const MenuName: string]: TUIPopupMenu read GetPopupMenu;
    property RollupCount: Integer read GetRollupCount;
    procedure RollupDialog;
    property RollupNamed[const AName: string]: TRollupForm read GetRollupNamed;
    property Rollups[AIndex: Integer]: TRollupForm read GetRollup;
    property StatusText: string read FStatusText write SetStatusText;
    property StrFileName: string read FFileName write FFileName;
    property ToolbarCount: Integer read GetToolbarCount;
    property Toolbars[AIndex: Integer]: TUIToolbar read GetToolbar;
    property ToolbarNamed[const AName: string]: TUIToolbar read GetToolbarNamed;
    procedure ToolbarDialog(Toolbars: Boolean = TRUE; Rollups: Boolean = TRUE);
    procedure Update(AWhat: TUpdateInfo; Immediately: Boolean = FALSE);
    property UpdateProc: TUpdateProc read FUpdateProc write FUpdateProc;
    property UIInitialize: TUIInitialize read FUIInitialize write SetUIInitialize;

             {++brovak - for user's interface}
    procedure ChangeToolBarSettings(ToolBarsName: string; ADockedTo: TDock97; ADockRow, ADockPos: integer; AVisible: boolean);
    procedure ChangeRollupSettings(RollUpName: string; ADockedTo: TDock97; ADockRow, ADockPos, AareaHeight, AareaWidth: integer; AVisible: boolean);
    procedure SetDefUserInterface(Sender: TObject);
       {--brovak}
  end;

var // alias of the MultiLng-string-list
  MlgStringList: TMlgStringList absolute MultiLng.MlgStringList;
    // the user-interface-object itself
  UserInterface: TUserInterface;
    // pointer to the status-bar of the application
  StatusBar: TWStatusBar;

implementation

uses Graphics, IniFiles, NumTools, SysUtils, ToolSel, TypInfo, WinProcs, Tb97TlWn,
  TB97Tlbr, Validate, WinOSInfo, ErrHndl;

const
  UPDATE_TIMER_ID = $7DEF;

function FindUIExtension(Component: TComponent): TUIExtension;
var
  Cnt: Integer;
begin
  if (Component <> nil) and not (csDestroying in Component.ComponentState) then
    for Cnt := 0 to Component.ComponentCount - 1 do
      if Component.Components[Cnt] is TUIExtension then
      begin
        Result := TUIExtension(Component.Components[Cnt]);
        Exit;
      end;
  Result := nil;
end;

{==============================================================================+
  TUserInterface
+==============================================================================}

constructor TUserInterface.Create;
begin
  inherited Create(nil);
  F3DChild := nil;
  F3DActive := FALSE;
  FMenus := TList.Create;
  FToolbars := TList.Create;
  FRollups := TList.Create;
    // setup popup-menu for toolbars
  FPopupMenu := TPopupMenu.Create(Self);
  FPopupMenu.OnPopup := DoPopup;
    // create context-toolbar
  FContextToolbar := TUIToolbar.Create(Self);
  FContextToolbar.ToolbarType := ttUser;
  FContextToolbar.Name := 'Context';
  FContextToolbar.Visible := True;
    // create context-toolbar-list
  FContextToolbars := TStringList.Create;
    // create list for supported languages
  FLanguages := TStringList.Create;
  FUIInitialize := TUIInitialize.Create(Self);
end;

destructor TUserInterface.Destroy;
var
  Cnt: Integer;
begin
  // free all lists (objects in the list are freed by the owner-component)
  FMenus.Free;
  FContextToolbars.Free;
  FLanguages.Free;
  // free rollups
  if FRollupsCreated then
    for Cnt := 0 to FRollups.Count - 1 do
      TRollupForm(FRollups[Cnt]).Free;
  FRollups.Free;
  FToolbars.Free;
  // free the window-hooks
  if FNewMainWndProc <> nil then
    FreeObjectInstance(FNewMainWndProc);
  if FNewClientWndProc <> nil then
    FreeObjectInstance(FNewClientWndProc);
  FHelpHandler.Free;
  inherited Destroy;
end;

procedure TUserInterface.SetUIInitialize(AUIInitialize: TUIInitialize);
begin
  if (FUIInitialize <> nil) and (FUIInitialize.Owner = Self) then
    FUIInitialize.Free;
  FUIInitialize := AUIInitialize;
end;

{******************************************************************************+
  Procedure TUserInterface.UpdateRollupMenus
--------------------------------------------------------------------------------
  Updates the checked-status of the show/hide-menus of the rollups.
+******************************************************************************}

procedure TUserInterface.UpdateRollupMenus;
var
  Cnt: Integer;
  Rollup: TRollupForm;
begin
  if FRollupsCreated then
    for Cnt := 0 to FRollups.Count - 1 do
    begin
      Rollup := TRollupForm(FRollups[Cnt]);
      if Rollup.Rollup.ShowHideMenuFunction <> '' then
        MenuFunctions[
          Rollup.Rollup.ShowHideMenuFunction].Checked :=
          Rollup.RollupToolWindow.Visible;
    end;
  UserInterface.Update([uiMenus]);
end;

function TUserInterface.GetLanguageCount: Integer;
begin
  Result := FLanguages.Count;
end;

function TUserInterface.GetLanguages(AIndex: Integer): LCID;
begin
  Result := MakeLong(Integer(FLanguages.Objects[AIndex]), SORT_DEFAULT);
end;

{******************************************************************************+
  Procedure TUserInterface.SetLanguage
--------------------------------------------------------------------------------
  Called to set the user-interface-language. Loads an appropriate language-file
  and updates all language-dependend controls.
+******************************************************************************}

procedure TUserInterface.SetLanguage(ALanguage: LCID);
  {****************************************************************************+
    Function IndexOfLCID
  ------------------------------------------------------------------------------
    Searches the languages-list for the specified LCID. Returns the index
    of the list-entry or -1 if not found.
  +****************************************************************************}

  function IndexOfLCID(ALanguage: LCID): Integer;
  begin
    for Result := 0 to FLanguages.Count - 1 do
      if LCID(FLanguages.Objects[Result]) = ALanguage then
        Exit;
    Result := -1;
  end;
  {****************************************************************************+
    Function IndexOfLangID
  ------------------------------------------------------------------------------
    Searches the languages-list for the specified Language-ID (which are the
    lower 10 bits of the LCID. Returns the index of the list-entry or
    -1 if not found.
  +****************************************************************************}

  function IndexOfLangID(ALanguage: LangID): Integer;
  begin
    for Result := 0 to FLanguages.Count - 1 do
      if Integer(FLanguages.Objects[Result]) and $3FF = ALanguage then
        Exit;
    Result := -1;
  end;

  procedure RaiseError;
  begin
    raise Exception.Create(Format('The file %s was not found or has an invalid'
      + ' format. The application can not run without this file. Copy the file'
      + ' from the installation disks or ask your dealer for help.',
      [FFileName]));
  end;
var
  Index: Integer;
  Cnt: Integer;
begin
  if ALanguage <> FLanguage then
  begin
    // first search for the LCID
    Index := IndexOfLCID(ALanguage);
    // if not found search for the lang-id
    if Index = -1 then
      Index := IndexOfLangID(ALanguage and $3FF);
    // if not found search for British-English
    if Index = -1 then
      Index := IndexOfLCID($0809);
    // if not found search for any English
    if Index = -1 then
      Index := IndexOfLangID($09);
    // if not found take the first in the list
    if Index = -1 then
      Index := Min(0, FLanguages.Count - 1);
    // raise an exception if no language-file found
    if Index = -1 then
      RaiseError;
    // do nothing if there is no change in the language
    if ALanguage <> FLanguage then
    begin
      // if the application already has a main-form disable control-alignment
      // and redraw to avoid "flickering"
      if FMainForm <> nil then
      begin
        FMainForm.DisableAlign;
        SendMessage(FMainForm.Handle, wm_SetRedraw, 0, 0);
      end;
      if not ReadFile(FLanguages[Index], FLanguage) then
        RaiseError;
      // set object-variables specifiying language and language-file-name
      FFileName := FLanguages[Index];
      FLanguage := LCID(FLanguages.Objects[Index]);
      // set process-locale to the new language
      SetThreadLocale(FLanguage);
      // inform objects about language-change
      for Cnt := 0 to LanguageDependend.Count - 1 do
        CallMethodByName(LanguageDependend[Cnt], 'OnLanguageChanged');
      if FMainForm <> nil then
      begin
        // add toolbars to main-form (don't add context-toolbar and rollups
        // because this has already been done by SetMainForm)
        AddToolbars(FALSE);
        // re-enable the control-alignment
        FMainForm.EnableAlign;
        // menus, statusbar and context-toolbar must be updated
        Update([uiMenus, uiStatusBar, uiContextToolbars]);
        // enable and force the redraw of the window
        SendMessage(FMainForm.Handle, wm_SetRedraw, 1, 0);
        FMainForm.Invalidate;
      end;
    end;
  end;
end;

procedure TUserInterface.SetLanguageFile(AFile: string);
var
  Cnt: Integer;

  procedure RaiseError;
  begin
    raise Exception.Create(Format('The file %s was not found or has an invalid'
      + ' format. The application can not run without this file. Copy the file'
      + ' from the installation disks or ask your dealer for help.',
      [AFile]));
  end;
begin
  // if the application already has a main-form disable control-alignment
  // and redraw to avoid "flickering"
  if FMainForm <> nil then
  begin
    FMainForm.DisableAlign;
    SendMessage(FMainForm.Handle, wm_SetRedraw, 0, 0);
  end;
  // if filename does not contain a drive specifier add the application-path
  if ExtractFileDrive(AFile) = '' then
    AFile := ExtractFilePath(Application.ExeName) + AFile;
  // try to read the file
  if not ReadFile(AFile, FLanguage) then
    RaiseError;
  // set object-variables specifiying language and language-file-name
  FFileName := AFile;
  // set process-locale to the new language
  SetThreadLocale(FLanguage);
  // inform objects about language-change
  for Cnt := 0 to LanguageDependend.Count - 1 do
    CallMethodByName(LanguageDependend[Cnt], 'OnLanguageChanged');
  if FMainForm <> nil then
  begin
    // add toolbars to main-form (don't add context-toolbar and rollups
    // because this has already been done by SetMainForm)
    AddToolbars(FALSE);
    // re-enable the control-alignment
    FMainForm.EnableAlign;
    // menus, statusbar and context-toolbar must be updated
    Update([uiMenus, uiStatusBar, uiContextToolbars]);
    // enable and force the redraw of the window
    SendMessage(FMainForm.Handle, wm_SetRedraw, 1, 0);
    FMainForm.Invalidate;
  end;
end;

function TUserInterface.GetMainMenu(const MenuName: string): TUIMainMenu;
var
  Cnt: Integer;
begin
  for Cnt := 0 to FMenus.Count - 1 do
    if CompareText(RemoveNumbers(
      TUIMainMenu(FMenus[Cnt]).Name), MenuName) = 0 then
    begin
      Result := TUIMainMenu.Create(Self);
      Result.Assign(TUIMainMenu(FMenus[Cnt]));
      Exit;
    end;
  Result := nil;
end;

function TUserInterface.GetPopupMenu(const MenuName: string): TUIPopupMenu;
var
  Cnt: Integer;
begin
  for Cnt := 0 to FMenus.Count - 1 do
    if CompareText(RemoveNumbers(
      TUIPopupMenu(FMenus[Cnt]).Name), MenuName) = 0 then
    begin
      Result := TUIPopupMenu(FMenus[Cnt]);
      Exit;
    end;
  Result := nil;
end;

function TUserInterface.ReadFile(const FileName: string; var Language: LCID): Boolean;
var
  MlgFile: TMultiLngFile;
  Cnt: Integer;
begin
  Result := FALSE;
  MlgFile := TMultiLngFile.Create;
  try
    try
      // open the multi-language-file
      MlgFile.Open(FileName, Self);
      // free old menus
      for Cnt := 0 to FMenus.Count - 1 do
        TObject(FMenus[Cnt]).Free;
      // copy new menus to list
      FMenus.Clear;
      for Cnt := 0 to MlgFile.Menus.Count - 1 do
        FMenus.Add(MlgFile.Menus[Cnt]);
      // free old toolbars and also remove them from the context-toolbar
      RemoveContextToolbars;
      for Cnt := 0 to FToolbars.Count - 1 do
        TObject(FToolbars[Cnt]).Free;
      // copy new toolbars to list
      FToolbars.Clear;
      for Cnt := 0 to MlgFile.Toolbars.Count - 1 do
        FToolbars.Add(MlgFile.Toolbars[Cnt]);
      // copy multilanguage-string-list
      MlgStringList.Assign(MlgFile.MlgStringList);
      // remove old-shortcuts, create new shortcuts
      MenuFunctions.ClearShortCuts;
      for Cnt := 0 to MlgFile.ShortCuts.Count - 1 do
        MenuFunctions.AddShortCut(
          MlgFile.ShortCuts[Cnt], LoWord(Integer(MlgFile.ShortCuts.Objects[Cnt])),
          Boolean(HiWord(Integer(MlgFile.ShortCuts.Objects[Cnt]))));
      Result := TRUE;
    except
    end;
  finally
    MlgFile.Free;
  end;
end;

function TUserInterface.GetToolbarCount: Integer;
begin
  Result := FToolbars.Count;
end;

function TUserInterface.GetToolbar(AIndex: Integer): TUIToolbar;
begin
  Result := FToolbars[AIndex];
end;

procedure TUserInterface.ToolbarDialog(Toolbars: Boolean; Rollups: Boolean);
var
  ToolbarSelectDialog: TToolbarSelectDialog;
begin
  ToolbarSelectDialog := TToolbarSelectDialog.Create(Application);
  try
    if Toolbars and Rollups then
      ToolbarSelectDialog.SelectType := tsBoth
    else
      if Toolbars then
        ToolbarSelectDialog.SelectType := tsToolbars
      else
        ToolbarSelectDialog.SelectType := tsRollups;
    ToolbarSelectDialog.ShowModal;
  finally
    ToolbarSelectDialog.Free;
  end;
end;

procedure TUserInterface.RollupDialog;
var
  ToolbarSelectDialog: TToolbarSelectDialog;
begin
  ToolbarSelectDialog := TToolbarSelectDialog.Create(Application);
  try
    ToolbarSelectDialog.SelectType := tsRollups;
    ToolbarSelectDialog.ShowModal;
  finally
    ToolbarSelectDialog.Free;
  end;
end;

procedure TUserInterface.Update(AWhat: TUpdateInfo; Immediately: Boolean);
var
  UIExtension: TUIExtension;
  MainUIExtension: TUIExtension;
  Updated: Boolean;
  Cnt: Integer;
begin
  // exit if designing or application is being destroyed (application is exited)
  if csDestroying in Application.ComponentState then
    Exit;
  // call user-defined procedure if set
  if Assigned(FUpdateProc) then
    UpdateProc(AWhat)
  else
  begin
    MainUIExtension := FindUIExtension(FMainForm);
    if F3DActive and (F3DChild <> nil) then
      UIExtension := FindUIExtension(F3DChild)
    else
      UIExtension := FindUIExtension(FMainForm.ActiveMDIChild);
    if (uiStatusBar in AWhat) and (StatusBar <> nil)
      and not StatusBar.ProgressPanel then
    begin
      if UserInterface.StatusText <> '' then
      begin
        StatusBar.SimpleText := UserInterface.StatusText;
        StatusBar.SimplePanel := TRUE;
      end
      else
      begin
        if StatusBar.SimplePanel then
          StatusBar.SimplePanel := FALSE;
        if UIExtension <> nil then
          Updated := UIExtension.DoUpdateStatusbar
        else
          if MainUIExtension <> nil then
            Updated := MainUIExtension.DoUpdateStatusbar
          else
            Updated := FALSE;
        if not Updated then
          for Cnt := 0 to StatusBar.Panels.Count - 1 do
            StatusBar[Cnt] := '';
      end;
      StatusBar.Update;
    end;
    if (uiCursor in AWhat) and (FWaitCursorCount = 0) then
    begin
      if MainUIExtension <> nil then
        MainUIExtension.DoUpdateCursor;
      if UIExtension <> nil then
        UIExtension.DoUpdateCursor;
    end;
  end;
  AWhat := AWhat - [uiStatusBar, uiCursor];
  if AWhat <> [] then
  begin
    FPendingUpdates := FPendingUpdates + AWhat;
    if Immediately then
      UpdateTimerEvent
    else
      SetTimer(FMainHandle, UPDATE_TIMER_ID, 50, nil);
  end;
end;

procedure TUserInterface.UpdateTimerEvent;
var
  UIExtension: TUIExtension;
  MainUIExtension: TUIExtension;
  Updates: TUpdateInfo;
begin
  Updates := FPendingUpdates;
  FPendingUpdates := [];
  // stop the update-timer
  KillTimer(FMainHandle, UPDATE_TIMER_ID);
  // get pointers to ui-extensions
  MainUIExtension := FindUIExtension(FMainForm);
  if F3DActive and (F3DChild <> nil) then
    UIExtension := FindUIExtension(F3DChild)
  else
    UIExtension := FindUIExtension(FMainForm.ActiveMDIChild);

  ///// update context-toolbars
  if (uiContextToolbars in Updates) then
  begin

    //FContextToolbar.Perform(WM_SETREDRAW,0,0);
    FContextToolbar.BeginUpdate;

    RemoveContextToolbars;
    if MainUIExtension <> nil then
      MainUIExtension.DoUpdateContextToolbars;
    if UIExtension <> nil then
      UIExtension.DoUpdateContextToolbars;
    MenuFunctions.UpdateContextToolbars;

    //FContextToolbar.Perform(WM_SETREDRAW,1,0);
    FContextToolbar.Visible := FContextToolbars.Count > 0;
    FContextToolbar.EndUpdate;
    FContextToolbar.Invalidate;

    if FContextToolbars.Count = 0 then
    begin
       //FContextToolbar.Perform(WM_SETREDRAW,0,0);
      FContextToolbar.BeginUpdate;
      RemoveContextToolbars;
      FContextToolbar.Visible := FALSE;
      FContextToolbar.EndUpdate;
      FContextToolbar.Invalidate;
    end;
  end;

  // update menus
  if uiMenus in Updates then
  begin
    // begin the menu-update, disables all menus
    MenuFunctions.BeginUpdate;
    // group 1 is always enabled
    MenuFunctions.GroupEnabled[1] := TRUE;
{++ Moskaliov BUG#148 BUILD#100 10.01.00}
    // group 1001 is disabled only if Project.SymbolMode=sym_SymbolMode
    MenuFunctions.GroupEnabled[1001] := TRUE;
{-- Moskaliov BUG#148 BUILD#100 10.01.00}
    if MainUIExtension <> nil then
      MainUIExtension.DoUpdateMenus;
    if UIExtension <> nil then
      UIExtension.DoUpdateMenus;
    // Update the status of all controls
    UpdateRollupMenus;
    MenuFunctions.EndUpdate;
  end;

  // update custom-updates if any set
  if Updates - uiPredefined <> [] then
  begin
    if MainUIExtension <> nil then
      MainUIExtension.DoUpdateCustom(Updates - uiPredefined);
    if UIExtension <> nil then
      UIExtension.DoUpdateCustom(Updates - uiPredefined);
  end;
end;

procedure TUserInterface.DoShowHideToolbar(Sender: TObject);
var
  Cnt: Integer;
  Index: Integer;
  Toolbar: TUIToolbar;
begin
  Index := FPopupMenu.Items.IndexOf(TMenuItem(Sender));
  for Cnt := 0 to ToolbarCount - 1 do
  begin
    Toolbar := Toolbars[Cnt];
    if Toolbar.ToolbarType = ttUser then
    begin
      Dec(Index);
      if Index < 0 then
      begin
        Toolbar.Visible := not Toolbar.Visible;
        Exit;
      end;
    end;
  end;
  Dec(Index);
  for Cnt := 0 to RollupCount - 1 do
  begin
    Dec(Index);
    if Index < 0 then
    begin
      Rollups[Cnt].RollupToolWindow.Visible := not Rollups[Cnt].RollupToolWindow.Visible;
      Exit;
    end;
  end;
end;

procedure TUserInterface.DoPopup(Sender: TObject);
var
  Cnt: Integer;
  Toolbar: TUIToolbar;
begin
  for Cnt := FPopupMenu.Items.Count - 1 downto 0 do
    FPopupMenu.Items.Delete(Cnt);
  for Cnt := 0 to ToolbarCount - 1 do
  begin
    Toolbar := Toolbars[Cnt];
    if Toolbar.ToolbarType = ttUser then
      FPopupMenu.Items.Add(NewItem(Toolbar.Caption,
        0, Toolbar.Visible, TRUE, DoShowHideToolbar, 0, ''));
  end;
  if RollupCount > 0 then
    FPopupMenu.Items.Add(NewLine);
  for Cnt := 0 to RollupCount - 1 do
  begin
    FPopupMenu.Items.Add(NewItem(Rollups[Cnt].Caption,
      0, Rollups[Cnt].RollupToolWindow.Visible, TRUE, DoShowHideToolbar, 0, ''));
  end;

{$IFNDEF CL}
  {++brovak}
  if (RollupCount > 0) or (ToolBarCount > 0) then
    FPopupMenu.Items.Add(NewLine);
  FPopupMenu.Items.Add(NewItem(MlgStringList['Hint', 41], 0, false, true, SetDefUserInterface, 0, ''));
  {--brovak}
{$ENDIF}
end;

procedure TUserInterface.AddToolbars(AddContextToolbar: Boolean);
var
  Cnt: Integer;
  Toolbar: TUIToolbar;
begin
  for Cnt := 0 to FToolbars.Count - 1 do
  begin
    Toolbar := FToolbars[Cnt];
    if Toolbar.ToolbarType <> ttContext then
    begin
      RemoveComponent(Toolbar);
      FMainForm.InsertComponent(Toolbar);
      Toolbar.Parent := FTopControlBar;
    end;
  end;
    // add the context-toolbar
  if AddContextToolbar and UIInitialize.FHasContextToolbar then
  begin
    RemoveComponent(FContextToolbar);
    FMainForm.InsertComponent(FContextToolbar);
    FContextToolbar.Parent := FTopControlBar;
       {++ brovak}
       {Set Off CloseButton on ContextToolBar}
    FContextToolBar.CloseButton := false;
       {-- brovak}
  end;
    // load toolbar-positions from the ini-file
  IniLoadToolbarPositions(FMainForm, FIniFileName);
  FPopupMenu.Images := MenuFunctions.Images;
    // ensure program-toolbars are hidden
  for Cnt := 0 to UserInterface.ToolbarCount - 1 do
  begin
    Toolbar := UserInterface.Toolbars[Cnt];
    if Toolbar.ToolbarType <> ttUser then
      Toolbar.Visible := FALSE;
  end;
    // hide context-toolbar by default
  FContextToolbar.Visible := FALSE;
end;

function TUserInterface.GetToolbarNamed(const AName: string): TUIToolbar;
var
  Cnt: Integer;
begin
  for Cnt := 0 to ToolbarCount - 1 do
    if CompareText(RemoveNumbers(
      Toolbars[Cnt].Name), AName) = 0 then
    begin
      Result := Toolbars[Cnt];
      Exit;
    end;
  Result := nil;
end;

procedure TUserInterface.SetStatusText(const AText: string);
begin
  FStatusText := AText;
  Update([uiStatusBar]);
end;

procedure TUserInterface.AddRollup(ARollup: TRollupFormClass);
begin
  FRollups.Add(ARollup);
end;

function TUserInterface.GetRollupCount: Integer;
begin
  if FRollupsCreated then
    Result := FRollups.Count
  else
    Result := 0;
end;

function TUserInterface.GetRollup(AIndex: Integer): TRollupForm;
begin
  Result := FRollups[AIndex];
end;

function TUserInterface.GetRollupNamed(const AName: string): TRollupForm;
var
  Cnt: Integer;
begin
  for Cnt := 0 to FRollups.Count - 1 do
    if CompareText(TRollupForm(FRollups[Cnt]).Name, AName) = 0 then
    begin
      Result := TRollupForm(FRollups[Cnt]);
      Exit;
    end;
  Result := nil;
end;

{******************************************************************************+
  Procedure TUserInterface.DoMessages
--------------------------------------------------------------------------------
  Called by Application before a message is handled. Calls original handler,
  if message was not handled then checks if the message is a shortcut-message.
  Sets EscPressed to true if the ESC-key was pressed.
+******************************************************************************}

procedure TUserInterface.DoMessages(var Msg: TMsg; var Handled: Boolean);
const
  AltMask = $20000000;
var
  ShortCut: Word;
begin
  if Assigned(UIInitialize.FOnMessages) then
  begin
    UIInitialize.FOnMessages(Msg, Handled);
  end;
  if not Handled and (Msg.Message = WM_KEYDOWN) and FMainForm.Enabled then
  begin
    { Pr�ft, ob die Key-Message einem ShortCut entspricht und ruft
    + die entsprechende Behandlungsroutine auf }
    ShortCut := Byte(Msg.wParam);
    if GetKeyState(VK_SHIFT) < 0 then
      Inc(ShortCut, scShift);
    if GetKeyState(VK_CONTROL) < 0 then
      Inc(ShortCut, scCtrl);
    if Msg.lParam and AltMask <> 0 then
      Inc(ShortCut, scAlt);
    Handled := MenuFunctions.DispatchShortCut(ShortCut);
    if Msg.wParam = vk_Escape then
      EscPressed := TRUE;
  end;
end;

{******************************************************************************+
  Procedure TUserInterface.SetMainForm
--------------------------------------------------------------------------------
  Sets the pointer to the main-form and hooks the main- and client-window and
  the application. MainForm must be set before AddRollups or AddToolbars
  is called. Does not use Application.MainForm, because this is set after
  the MainForm is created, too late for this purpose.
+******************************************************************************}

procedure TUserInterface.SetMainForm
  (
  AForm: TForm
  );
{$IFDEF WIN32}

  function CreateControlBar(Align: TDockPosition; const Name: string; BoundLines: Boolean): TDock97;
  begin
    Result := TDock97.Create(AForm);
    Result.Position := Align;
    Result.Name := Name;
    Result.Parent := AForm;
    Result.PopupMenu := FPopupMenu;
    if BoundLines then
      case Align of
        dpBottom: Result.BoundLines := [blTop];
        dpTop: Result.BoundLines := [blBottom];
        dpLeft: Result.BoundLines := [blRight];
        dpRight: Result.BoundLines := [blLeft];
      end;
  end;
{$ENDIF}
var
  WndClass: TWndClass;
  IniFile: TIniFile;
begin
  // setup application-events and screen-events
  Application.OnMessage := DoMessages;
  Application.OnHint := DoShowHint;
  Application.OnActivate := UIInitialize.FOnAppActivate;
  Screen.OnActiveFormChange := DoActiveFormChanged;
  FMainForm := AForm;
  // hook window-function of then main-window
  FMainHandle := FMainForm.Handle;
  FOldMainWndProc := Pointer(GetWindowLong(FMainHandle, gwl_WndProc));
  FNewMainWndProc := MakeObjectInstance(MainWindowHook);
  SetWindowLong(FMainHandle, gwl_WndProc, LongInt(FNewMainWndProc));
  { get the windows default mdi-client window-function }
  GetClassInfo(HInstance, 'MDICLIENT', WndClass);
  FDefClientWndProc := WndClass.lpfnWndProc;
  { hook window-function of the mdi-client-window }
  FClientHandle := FMainForm.ClientHandle;
  if FClientHandle <> 0 then
  begin
    FOldClientWndProc := Pointer(GetWindowLong(FClientHandle, gwl_WndProc));
    FNewClientWndProc := MakeObjectInstance(ClientWindowHook);
    SetWindowLong(FClientHandle, gwl_WndProc, LongInt(FNewClientWndProc));
  end;
  // create docking-bars
  FTopControlBar := CreateControlBar(dpTop, 'TopDockbar', UIInitialize.FBoundLines);
  FTopControlBar.BoundLines := [blTop];
  TopB := FTopControlBar;
  CreateControlBar(dpBottom, 'BottomDockbar', UIInitialize.FBoundLines);
  LeftB := CreateControlBar(dpLeft, 'LeftDockbar', UIInitialize.FBoundLines);

  RightB := CreateControlBar(dpRight, 'RightDockbar', UIInitialize.FBoundLines);
  // add all toolbars and rollups to the application
  AddRollups;
  AddToolbars(TRUE);
  RehookRollupValidators;
  if UIInitialize.FSaveWindowSize then
    with FMainForm do
    begin
      IniFile := TIniFile.Create(FIniFileName);
      try
        WindowState := TWindowState(IniFile.ReadInteger('Preferences', 'MainWindowState', Integer(wsNormal)));
        if WindowState = wsNormal then
        begin
          Left := IniFile.ReadInteger('Preferences', 'MainWindowLeft', Left);
          Top := IniFile.ReadInteger('Preferences', 'MainWindowTop', Top);
          Width := IniFile.ReadInteger('Preferences', 'MainWindowWidth', Width);
          Height := IniFile.ReadInteger('Preferences', 'MainWindowHeight', Height);
        end;
      finally
        IniFile.Free;
      end;
    end;
  // link to HTML-help-function
  FHelpHandler := THelpHandler.Create;
  FHelpHandler.HTMLHelp := UIInitialize.FHTMLHelp;
  Application.OnHelp := FHelpHandler.OnHelp;
  // force update of the user-interface
  Update([uiMenus, uiStatusBar, uiCursor, uiContextToolbars]);
end;

{******************************************************************************+
  Procedure TUserInterface.AddRollups
--------------------------------------------------------------------------------
  Adds all the registered rollups to the mainwindow of the application. IniFile
  specifies the name of the ini-file the positions of the rollups are to be
  loaded from.
+******************************************************************************}

procedure TUserInterface.AddRollups;
var
  Cnt: Integer;
  Rollup: TRollupForm;
  ToolWindow: TToolWindow97;
begin
  { create rollups from the rollup-classes in the list }
{$IFNDEF WMLT}
  for Cnt := 0 to FRollups.Count - 1 do
  begin
    Rollup := TRollupFormClass(FRollups[Cnt]).Create(nil);
    FRollups[Cnt] := Rollup;
    ToolWindow := Rollup.RollupToolWindow;
    FMainForm.InsertComponent(ToolWindow);
    ToolWindow.Parent := FTopControlBar;
{$IFDEF WIN32}
    Rollup.ParentWindow := FMainHandle;
{$ENDIF}
    { load rollup-settings from the ini-file }
//      if Rollup.Rollup<>NIL then Rollup.Rollup.IniFile:=FIniFileName;
  end;
  FRollupsCreated := TRUE;
{$ENDIF}
end;

{******************************************************************************+
  Procedure TUserInterface.ClientWindowHook
--------------------------------------------------------------------------------
  Hook for the window-function of the mdi-client-window. Ensures the correct
  alignment and visibility of the rollups.
+******************************************************************************}

procedure TUserInterface.ClientWindowHook(var Message: TMessage);
begin
  with Message do
  begin
    case Msg of
      $3F:
        if UIInitialize.FClientEdge then
        begin
          Result := CallWindowProc(FDefClientWndProc, FClientHandle, Msg, wParam, lParam);
          Exit;
        end;
    end;
    if Msg <> WM_MDIRestore then
      Result := CallWindowProc(FOldClientWndProc, FClientHandle, Msg, wParam, lParam);
    case Msg of
      WM_MDIDestroy: DoActiveFormChanged(Screen);
    end;
  end;
end;

{******************************************************************************+
  Procedure TUserInterface.MainWindowHook
--------------------------------------------------------------------------------
  Hook for the window-function of the mainwindow. Ensures the correct alignment
  and visibility of the rollups.
+******************************************************************************}

procedure TUserInterface.MainWindowHook(var Message: TMessage);
var
  IniFile: TIniFile;
begin
  with Message do
  begin
    case Msg of
      wm_Timer:
        if wParam = UPDATE_TIMER_ID then
        begin
          UpdateTimerEvent;
          Exit;
        end;
      WM_EnterMenuLoop:
        begin
          FInMenuLoop := True;
          if StatusBar <> nil then
          begin
            StatusBar.SimpleText := '';
            StatusBar.SimplePanel := TRUE;
          end;
        end;
      WM_ExitMenuLoop:
        begin
          FInMenuLoop := False;
          if StatusBar <> nil then
            StatusBar.SimplePanel := FALSE;
        end;
      WM_DESTROY:
        begin
          // store toolbar-positions to the ini-file
          IniSaveToolbarPositions(FMainForm, FIniFileName);
          // store current main-window-settings to the ini-file
          if UIInitialize.FSaveWindowSize and (FMainForm <> nil) then
            with FMainForm do
            begin
              IniFile := TIniFile.Create(FIniFileName);
              try
                IniFile.WriteInteger('Preferences', 'MainWindowState', Integer(WindowState));
                if WindowState = wsNormal then
                begin
                  IniFile.WriteInteger('Preferences', 'MainWindowLeft', Left);
                  IniFile.WriteInteger('Preferences', 'MainWindowTop', Top);
                  IniFile.WriteInteger('Preferences', 'MainWindowWidth', Width);
                  IniFile.WriteInteger('Preferences', 'MainWindowHeight', Height);
                end;
              finally
                IniFile.Free;
              end;
            end;
        end;
    end;
    Result := CallWindowProc(FOldMainWndProc, FMainHandle, Msg, wParam, lParam);
    case Msg of
      wm_MenuSelect: WMMenuSelect(TWMMenuSelect(Message));
    end;
  end;
end;

{******************************************************************************+
  Procedure TUIExtension.WMMenuSelect
--------------------------------------------------------------------------------
  Wird von Windows aufgerufen, wenn ein Men�punkt ausgew�hlt wird. Wenn es
  sich um das Systemmen� des Hauptfensters oder um das MDI-Men� eines
  maximierten Child-Fensters handlet, dann wird der entsprechende Hinweistext
  in der Statuszeile eingeblendet.
+******************************************************************************}

procedure TUserInterface.WMMenuSelect(var Msg: TWMMenuSelect);
var
  AHint: Integer;
begin
  inherited;
{  AHint:=-1;
  if Msg.MenuFlag and MF_SYSMENU<>0 then begin
    if Msg.MenuFlag and MF_POPUP<>0 then AHint:=10
    else case Msg.IDItem of
      sc_Size       : AHint:=13;
      sc_Move       : AHint:=12;
      sc_Minimize   : AHint:=14;
      sc_Maximize   : AHint:=15;
      sc_Close      : AHint:=16;
      sc_Restore    : AHint:=11;
      else AHint:=0;
    end;
  end
  else if (Msg.Menu=GetSubMenu(FMainForm.Menu.Handle,0))
      and (GetMenuState(FMainForm.Menu.Handle,0,MF_BYPOSITION) and MF_BITMAP<>0) then begin
    if Msg.MenuFlag and MF_POPUP<>0 then AHint:=1
    else case Msg.IDItem of
      sc_Size       : AHint:=5;
      sc_Move       : AHint:=4;
      sc_Minimize   : AHint:=6;
      sc_Maximize   : AHint:=7;
      sc_NextWindow : AHint:=8;
      sc_PrevWindow : AHint:=9;
      sc_Close      : AHint:=2;
      sc_Restore    : AHint:=3;
      else AHint:=0;
    end;
  end;
  if AHint>=0 then begin
    if AHint=0 then Application.Hint:=''
    else Application.Hint:=GetLongHint(MlgStringList['UserInterface',AHint]);
    Msg.Result:=0;
  end;}
end;

{******************************************************************************+
  Procedure TUserInterface.BeginWaitCursor
--------------------------------------------------------------------------------
  Activates the hourglass-cursor. Always call EndWaitCursor to remove the
  hourglass-cursor. Userinterface manages an internal counter of how often
  BeginWaitCursor/EndWaitCursor was called. Keeps the wait-cursor until this
  counter reaches zero, calls UpdateCursor in this case to restore the right
  cursor.
+******************************************************************************}

procedure TUserInterface.BeginWaitCursor;
begin
  { increment the BeginWaitCursor-counter }
  Inc(FWaitCursorCount);
  { set the wait-cursor if the counter is 1 }
  if FWaitCursorCount = 1 then
    Screen.Cursor := crHourGlass;
end;

{******************************************************************************+
  Procedure TUserInterface.EndWaitCursor
--------------------------------------------------------------------------------
  Must be called after BeginWaitCursor to restore the current cursor. For
  more details see BeginWaitCursor.
+******************************************************************************}

procedure TUserInterface.EndWaitCursor;
begin
  { decrement the BeginWaitCursor-counter }
  if FWaitCursorCount > 0 then
    Dec(FWaitCursorCount);
  { restore the current cursor if counter reaches zero }
  if FWaitCursorCount = 0 then
  begin
    { reset cursor to default to ensure hourglass-cursor is removed even if
    + Update doesn't do this }
    Screen.Cursor := crDefault;
    { restore the current cursor }
    Update([uiCursor]);
  end;
end;

{******************************************************************************+
  Procedure TUserInterface.DoActiveFormChanged
--------------------------------------------------------------------------------
  Called by Application if the active form changes. Determines if the
  active form has an UI-menu and sets the FileHistory and the Window-menu
  in this case. Updates the menus-enabled states.
+******************************************************************************}

procedure TUserInterface.DoActiveFormChanged(Sender: TObject);
begin
  DoUpdateWindowLists;
  // Men� Enable/Disable updaten
  UserInterface.Update([uiMenus]);
  // call user-defined function
  if Assigned(UIInitialize.FOnFormChange) then
    UIInitialize.FOnFormChange(Self);
end;

procedure TUserInterface.DoUpdateWindowLists;
var
  UIExtension: TUIExtension;
begin
  try
    if (FMainForm <> nil) and not (csDestroying in FMainForm.ComponentState) then
    begin
      UIExtension := nil;
      if F3DActive and (F3DChild <> nil) then
        UIExtension := FindUIExtension(F3DChild)
      else
        if FMainForm.ActiveMDIChild <> nil then
          UIExtension := FindUIExtension(FMainForm.ActiveMDIChild);
      if UIExtension = nil then
        UIExtension := FindUIExtension(FMainForm);
      if UIExtension <> nil then
      begin
        FMainForm.WindowMenu := TUIMainMenu(TForm(UIExtension.Owner).Menu).SpecialMenus['WindowList'];
        FMainForm.ObjectMenuItem := TUIMainMenu(TForm(UIExtension.Owner).Menu).SpecialMenus['ObjectMenu'];
      { determine if ui-extension has a file-history }
        if FFileHistory <> nil then
          FFileHistory.Position :=
            TUIMainMenu(TForm(UIExtension.Owner).Menu).SpecialMenus['FileHistory'];
      end;
    end;
  except
    on E: Exception do
    begin
      DebugMsg('TUserInterface.DoUpdateWindowLists', E.Message);
    end;
  end;
end;

procedure TUserInterface.DoShowHint(Sender: TObject);
begin
  if StatusBar <> nil then
  begin
    if UserInterface.FInMenuLoop then
      StatusBar.SimpleText := Application.Hint
    else
    begin
      if Application.Hint = '' then
        StatusBar.SimplePanel := FALSE
      else
      begin
        StatusBar.SimplePanel := TRUE;
        StatusBar.SimpleText := GetLongHint(Application.Hint);
      end;
    end;
  end;
end;

{******************************************************************************+
  Procedure TUserInterface.AddContextToolbar
--------------------------------------------------------------------------------
  Adds a context-specific toolbar to the current toolbar-list. Recreates the
  context-toolbar.
+******************************************************************************}

procedure TUserInterface.AddContextToolbar(const AName: string);
var
  Cnt: Integer;
  Toolbar: TUIToolbar;
  Separator: TToolbarSep97;
begin
  if not (csDestroying in FContextToolbar.ComponentState) then
  begin
    Toolbar := ToolbarNamed[AName];
    if (Toolbar <> nil) and (FContextToolbars.IndexOf(AName) = -1) then
    begin
      if FContextToolbars.Count > 0 then
      begin
        Separator := TToolbarSep97.Create(FContextToolbar);
        Separator.Width := 8;
        Separator.Parent := FContextToolbar;
      end;
      FContextToolbars.AddObject(AName, Toolbar);
      for Cnt := 0 to Toolbar.ComponentCount - 1 do
        TCustomControl(Toolbar.Components[Cnt]).Parent := FContextToolbar;
    end;
  end;
end;

{******************************************************************************+
  Procedure TUserInterface.RemoveContextToolbars
--------------------------------------------------------------------------------
  Removes a context-specific toolbar from the current toolbar-list. Recreates
  the context-toolbar.
+******************************************************************************}

procedure TUserInterface.RemoveContextToolbars;
var
  Cnt: Integer;
  Cnt1: Integer;
  Toolbar: TUIToolbar;
begin
  for Cnt := 0 to FContextToolbars.Count - 1 do
  begin
    Toolbar := TUIToolbar(FContextToolbars.Objects[Cnt]);
    for Cnt1 := 0 to Toolbar.ComponentCount - 1 do
      TCustomControl(Toolbar.Components[Cnt1]).Parent := Toolbar;
  end;
  for Cnt := 0 to FContextToolbar.ComponentCount - 1 do
    TCustomControl(FContextToolbar.Components[Cnt]).Free;
  FContextToolbars.Clear;
end;

{==============================================================================+
  TUIExtension
+==============================================================================}

constructor TUIExtension.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  LanguageDependend.Add(Self);
end;

destructor TUIExtension.Destroy;
begin
  UserInterface.DoUpdateWindowLists;
  if not (csDesigning in ComponentState) then
    Userinterface.Update([uiContextToolbars]);
  LanguageDependend.Remove(Self);
  inherited Destroy;
end;

{******************************************************************************+
  TUIExtension.OnLanguageChanged
--------------------------------------------------------------------------------
  Called by the user-interface if the user-interface-language has been changed.
  Updates the menu of the form.
+******************************************************************************}

procedure TUIExtension.OnLanguageChanged;
var
  PopupMenu: TPopupMenu;
begin
  if Owner is TForm then
  begin
    // set new main- and popup-menu to the owner-form
    PopupMenu := UserInterface.PopupMenus[FPopupMenuName];
    if PopupMenu <> nil then
      PopupMenu.OnPopup := DoPopup;
    TForm(Owner).PopupMenu := PopupMenu;
    if UserInterface.MainMenus[FMenuName] <> nil then
      TForm(Owner).Menu := UserInterface.MainMenus[FMenuName];
    if TForm(Owner).Menu is TUIMainMenu then
    begin
      if (UserInterface.FMainForm = nil) or (Owner <> UserInterface.FMainForm) then
      begin
        TForm(Owner).WindowMenu := TUIMainMenu(TForm(Owner).Menu).SpecialMenus['WindowList'];
        TForm(Owner).ObjectMenuItem := TUIMainMenu(TForm(Owner).Menu).SpecialMenus['ObjectMenu'];
      end;
      // if owner-form is MainForm or active MDI-child then file-history and
      // window-list must be blended in
      if (UserInterface.FMainForm <> nil) and ((Owner = UserInterface.FMainForm)
        or (Owner = UserInterface.FMainForm.ActiveMDIChild)) then
      begin
        // set file-history-position
        if UserInterface.FFileHistory <> nil then
          UserInterface.FFileHistory.Position :=
            TUIMainMenu(TForm(Owner).Menu).SpecialMenus['FileHistory'];
        // set window-list-position
        UserInterface.FMainForm.WindowMenu := TForm(Owner).WindowMenu;
        UserInterface.FMainForm.ObjectMenuItem := TForm(Owner).ObjectMenuItem;
      end;
    end;
  end;
end;

procedure TUIExtension.DoUpdateContextToolbars;
begin
  try
    UserInterface.AddContextToolbar(FContextToolbar);
    if Assigned(FOnUpdateContextBars) then
      FOnUpdateContextBars(Self);
  except
    on E: Exception do
    begin
      DebugMsg('TUIExtension.DoUpdateContextToolbars', E.Message);
    end;
  end;
end;

procedure TUIExtension.DoUpdateMenus;
begin
  try
    if Assigned(FOnUpdateMenus) then
      FOnUpdateMenus(Self);
  except
    on E: Exception do
    begin
      DebugMsg('TUIExtension.DoUpdateMenus', E.Message);
    end;
  end;
end;

procedure TUIExtension.DoUpdateCursor;
begin
  try
    if Assigned(FOnUpdateCursor) then
      FOnUpdateCursor(Self);
  except
    on E: Exception do
    begin
      DebugMsg('TUIExtension.DoUpdateCursor', E.Message);
    end;
  end;
end;

procedure TUIExtension.DoUpdateCustom(const PendingUpdates: TUpdateInfo);
begin
  try
    if Assigned(FOnUpdateCustom) then
      FOnUpdateCustom(Self, PendingUpdates);
  except
    on E: Exception do
    begin
      DebugMsg('TUIExtension.DoUpdateCustom', E.Message);
    end;
  end;
end;

function TUIExtension.DoUpdateStatusbar: Boolean;
begin
  try
    Result := Assigned(FOnUpdateStatus);
    if Result then
      FOnUpdateStatus(Self);
  except
    on E: Exception do
    begin
      DebugMsg('TUIExtension.DoUpdateStatusbar', E.Message);
    end;
  end;
end;

procedure TUIExtension.Loaded;
var
  PopupMenu: TPopupMenu;
begin
  inherited Loaded;
  if not (csDesigning in ComponentState) and (Owner is TForm) then
  begin
    // set additional events for extended forms
    if Owner is TWForm then
      with Owner as TWForm do
      begin
        OnHidden := FOnHidden;
        OnMDIActivate := FOnMDIActivate;
        OnMDIDeactivate := FOnMDIDeactivate;
        OnShown := FOnShown;
      end;
    PopupMenu := UserInterface.PopupMenus[FPopupMenuName];
    if PopupMenu <> nil then
      PopupMenu.OnPopup := DoPopup;
    TForm(Owner).PopupMenu := PopupMenu;
    if UserInterface.MainMenus[FMenuName] <> nil then
      TForm(Owner).Menu := UserInterface.MainMenus[FMenuName];
    if TForm(Owner).Menu is TUIMainMenu then
    begin
      TForm(Owner).WindowMenu := TUIMainMenu(TForm(Owner).Menu).SpecialMenus['WindowList'];
      TForm(Owner).ObjectMenuItem := TUIMainMenu(TForm(Owner).Menu).SpecialMenus['ObjectMenu'];
    end;
  end;
end;

function TUIExtension.GetMainMenu: TUIMainMenu;
begin
  if (Owner <> nil) and (Owner is TForm) and (TForm(Owner).Menu is TUIMainMenu) then
    Result := TUIMainMenu(TForm(Owner).Menu)
  else
    Result := nil;
end;

function TUIExtension.GetPopupMenu: TUIPopupMenu;
begin
  if (Owner <> nil) and (Owner is TForm) and (TForm(Owner).PopupMenu is TUIPopupMenu) then
    Result := TUIPopupMenu(TForm(Owner).PopupMenu)
  else
    Result := nil;
end;

{===============================================================================
  TUIInitialize
+==============================================================================}

constructor TUIInitialize.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  LanguageDependend.Add(Self);
end;

destructor TUIInitialize.Destroy;
begin
  LanguageDependend.Remove(Self);
  inherited Destroy;
end;

procedure TUIInitialize.Loaded;
var
  IniFile: TIniFile;
  LanguageLCID: LCID;
  LanguageID: string;
begin
  inherited Loaded;
  if not (csDesigning in ComponentState) then
  begin
    UserInterface.UIInitialize := Self;
    // call setup-function to enable application to set lng and ini-file-name
    if Assigned(FOnSetup) then
      OnSetup(Self);
    // use exe-name with .lng-extension if no lng-file specified
    if FFileName = '' then
      FFileName := ChangeFileExt(Application.ExeName, '.044');
    // set properties of user-interface-object
    UserInterface.StrFileName := FFileName;
    // set file-history to user-interface-object
    UserInterface.FileHistoryList := FFileHistory;
    // set ini-file-name to user-interface-object
    if FIniFileName = '' then
      FIniFileName := OSInfo.WingisIniFileName;
    UserInterface.IniFileName := FIniFileName;
    // determine language to startup with, use user-default-language or entry
    // from ini-file if specified
    LanguageLCID := GetUserDefaultLCID;
    if (FLanguageIni <> '') or (FLanguageIniFile <> '') then
    begin
      IniFile := TIniFile.Create(FIniFileName);
      try
        if FLanguageIniFile <> '' then
        begin
          LanguageID := IniFile.ReadString(GetShortHint(FLanguageIniFile),
            GetLongHint(FLanguageIniFile), FFileName);
          if LanguageID = '' then
            LanguageID := FFileName;
        end
        else
        begin
          LanguageID := IniFile.ReadString(GetShortHint(FLanguageIni), GetLongHint(FLanguageIni), '');
          try
            LanguageLCID := StrToInt('$' + LanguageID);
          except
          end;
        end;
      finally
        IniFile.Free;
      end;
    end;
    // try to read lng-file, halt if an error occurs during load
    try
      if FLanguageIniFile <> '' then
        UserInterface.LanguageFile := LanguageID
      else
        UserInterface.Language := LanguageLCID;
    except
      Application.HandleException(Self);
      Halt;
    end;
    UserIntf.StatusBar := StatusBar;
    // set user-interface mainform if not already set
    if UserInterface.MainForm = nil then
      UserInterface.MainForm := TForm(Owner)
  end;
end;

procedure TUIInitialize.OnLanguageChanged(Sender: TObject);
var
  IniFile: TIniFile;
begin
  if FLanguageIni <> '' then
  begin
    IniFile := TIniFile.Create(FIniFileName);
    try
      IniFile.WriteString(GetShortHint(FLanguageIni), GetLongHint(FLanguageIni),
        IntToHex(UserInterface.Language, 4));
    finally
      IniFile.Free;
    end;
  end;
end;

{==============================================================================+
  Initialisation- and termination-code
+==============================================================================}

procedure TUIExtension.DoPopup(Sender: TObject);
begin
  UserInterface.Update([uiMenus], TRUE);
end;

procedure TUserInterface.RehookRollupValidators;
var
  Cnt: Integer;
begin
  if FRollupsCreated then
    for Cnt := 0 to FRollups.Count - 1 do
      RehookValidators(FRollups[Cnt]);
end;

{++brovak}
{ Operation for manipulation with Toolbars and Rollups }
{----------------------------------------------------- }

procedure TUSerInterFace.ChangeToolBarSettings(ToolBarsName: string; ADockedTo: TDock97; ADockRow, ADockPos: integer; AVisible: boolean);
var
  Toolbar: TUIToolbar;
begin
  Toolbar := UserInterface.ToolbarNamed[ToolBarsName];
  ToolBar.DockedTo := ADockedTo;
  ToolBar.DockRow := ADockRow;
  ToolBar.DockPos := ADockPos;
  ToolBar.LastDock := ADockedTo;
  ToolBar.Visible := AVisible;
end;

procedure TUSerInterFace.ChangeRollupSettings(RollUpName: string; ADockedTo: TDock97; ADockRow, ADockPos, AareaHeight, AareaWidth: integer; AVisible: boolean);
var
  Rollup: TRollupForm;
begin;
  Rollup := UserInterface.RollupNamed[RollUpName];
  Rollup.RollupToolWindow.DockedTo := ADockedTo;
  Rollup.RollupToolWindow.LastDock := ADockedTo;
  RollUp.RollupToolWindow.ClientAreaWidth := AareaWidth;
  RollUp.RollupToolWindow.ClientAreaHeight := AareaHeight;
  Rollup.RollupToolWindow.DockPos := ADockPos;
  Rollup.RollupToolWindow.DockRow := ADockRow;
  Rollup.RollupToolWindow.Visible := AVisible;
end;

{ Reset User's Interface for Default settings }
{ ------------------------------------------- }

procedure TUSerInterFace.SetDefUserInterface(Sender: TObject);
const
  NVT = 1; //count of Toolbars for Hide
  NVR = 1; //count of RollUps for Hide
var

{Put to this set Toolbars & Rollups for hide it from Default User's
    Interface}

  NonVisibleToolbars: array[1..NVT] of string;
  NonVisibleRollups: array[1..NVR] of string;
  Rollup: TRollupForm;
  Toolbar: TUIToolbar;
  i, j: integer;

begin
//identify Toolbars & RollUps for hide from user default's interface
  NonVisibleToolBars[1] := 'ThreeDModule';
  NonVisibleRollups[1] := 'LegendManagerRollupForm';

{Hide All Rollups@Toolbars }

  with UserInterface do
  begin;
    for i := 0 to ToolbarCount - 1 do
    begin;
      for j := 1 to NVT do
        if ToolBars[i].Name = NonVisibleToolbars[j] then
        begin;
          ChangeToolBarSettings(ToolBars[i].Name, nil, 3 * (TopB.Top + TopB.Height), Trunc((RightB.Left - LeftB.Left - LeftB.Width) / 2), false);
          Break;
        end;
      Toolbars[i].Visible := false;
    end;

    for i := 0 to RollupCount - 1 do
    begin;
      for j := 1 to NVR do
        if Rollups[i].Name = NonVisibleRollups[j] then
        begin;
          ChangeRollupSettings(Rollups[i].Name, nil, 3 * (TopB.Top + TopB.Height), Trunc((RightB.Left - LeftB.Left - LeftB.Width) / 2), 200, 200, false);
          Break;
        end;
      Rollups[i].Visible := false;
    end;

    {Show Default's ToolBars & RollUps}

    ChangeToolBarSettings('Standard', TopB, 0, 0, true);
    ChangeToolBarSettings('View', TopB, 1, 0, true);
    ChangeToolBarSettings('Drawing', LeftB, 0, 0, true);
    ChangeToolBarSettings('Selection', TopB, 1, 226, true);
    ChangeToolBarSettings('Database', LeftB, 0, 285, true);

    ChangeRollupSettings('ViewManagerRollupForm', RightB, 0, 0, 170, 170, true);
    ChangeRollupSettings('SymbolRollupForm', RightB, 0, 170, 290, 270, true);
    ChangeRollupSettings('LegendManagerRollupForm', RightB, 0, 460, 155, 170, true);

  end; {with}

end;
{--brovak}

initialization
  StatusBar := nil;
  UserInterface := TUserInterface.Create(nil);

finalization
  UserInterface.Free;

end.

