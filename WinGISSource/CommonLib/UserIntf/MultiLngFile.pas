{******************************************************************************+
  Module MultiLngFile
--------------------------------------------------------------------------------
  Author: Martin Forst
--------------------------------------------------------------------------------
  Implements the TMultiLngFile-object which can read multilanguage-files.
+******************************************************************************}
{$H+}

Unit MultiLngFile;

Interface

Uses Classes,MultiLng,Menus,UintfCtrls,Windows;

Type TMultiLngFile      = Class
      Private
       FMenus           : TList;
       FMlgStringList   : TMlgStringList;
       FOwner           : TComponent;
       FParser          : TParser;
       FShortcuts       : TStringList;
       FToolbars        : TList;
       Procedure   CheckNextToken(AToken:Char);
       Function    CheckNextTokenString(AToken:Char):String;
       Procedure   ReadMenu;
       Procedure   RenameComponent(AComponent:TComponent;Const AName:String);
       Procedure   ReadShortCuts;
       Procedure   ReadStrings;
       Procedure   ReadToolbar;
       Procedure   ReadFileInfo;
      Public
       Constructor Create;
       Destructor  Destroy; override;
       // menus defined in the file
       Property    Menus:TList read FMenus;
       // multi-language string-list defined in the file
       Property    MlgStringList:TMlgStringList read FMlgStringList;
       Procedure   Open(const FileName:String;ComponentOwner:TComponent);
       // toolbars defined in the string-file
       Property    Toolbars:TList read FToolbars;
       //  shortcuts defined in the file
       Property    ShortCuts:TStringList read FShortCuts;
     end;

{++ IDB_UNDO}
Var
   AnUndoButton: TUIToolbarButton;
   ARedoButton: TUIToolbarButton;
{-- IDB_UNDO}
{++ IDB_INFO}
   AnIDBInfoButton: TUIToolbarButton;
{-- IDB_INFO}

Implementation

Uses Controls,MenuFn,StdCtrls,SysUtils,TB97,TB97Ctls,TB97Tlbr;

{==============================================================================+
  TMultiLngFile
+==============================================================================}

Constructor TMultiLngFile.Create;
begin
  inherited Create;
  FMenus:=TList.Create;
  FMlgStringList:=TMlgStringList.Create(NIL);
  FShortcuts:=TStringList.Create;
  FToolbars:=TList.Create;
end;

Destructor TMultiLngFile.Destroy;
begin
  FMenus.Free;
  FToolbars.Free;
  FMlgStringList.Free;
  FShortcuts.Free;
  inherited Destroy;
end;


Procedure TMultiLngFile.CheckNextToken(AToken:Char);
begin
  FParser.NextToken;
  FParser.CheckToken(AToken);
end;

Function TMultiLngFile.CheckNextTokenString(AToken:Char):String;
begin
  FParser.NextToken;
  FParser.CheckToken(AToken);
  Result:=FParser.TokenString;
end;

Procedure TMultiLngFile.ReadMenu;
var Menu         : TMenu;
    IsPopup      : Boolean;

  Procedure ReadMenu(Owner:TComponent);
  var MenuItem   : TMenuItem;
      SpecialName: String;
      FuncName   : String;
      {$IFDEF WIN32}
      {$IFOPT C+}
      Cnt        : Integer;
      Cnt1       : Integer;
      APos       : Integer;
      BPos       : Integer;
      {$ENDIF}
      {$ENDIF}
  begin
    with FParser do begin
      NextToken;
      while Token<>'}' do begin
        if Token='<' then begin
          SpecialName:=CheckNextTokenString(toSymbol);
          if CompareText(SpecialName,'Separator')=0 then begin
            MenuItem:=TMenuItem.Create(Menu);
            MenuItem.Caption:='-';
            MenuItem.Hint:='| ';
            RenameComponent(MenuItem,'Separator');
            if Owner=Menu then Menu.Items.Add(MenuItem)
            else TMenuItem(Owner).Add(MenuItem);
            NextToken;
            if Token=',' then begin
              NextToken;
              CheckToken(toInteger);
              MenuFunctions.SetAction(MenuItem,MenuFunctions.VisibleGroupFunction[TokenInt]);
              NextToken;
            end;
          end
          else if CompareText(SpecialName,'WindowList')=0 then begin
            if IsPopup then TUIPopupMenu(Menu).AddSpecialMenu('WindowList',TMenuItem(Owner))
            else TUIMainMenu(Menu).AddSpecialMenu('WindowList',TMenuItem(Owner));
            NextToken;
          end
          else if CompareText(SpecialName,'FileHistory')=0 then begin
            if IsPopup then TUIPopupMenu(Menu).AddSpecialMenu('FileHistory',TMenuItem(MenuItem))
            else TUIMainMenu(Menu).AddSpecialMenu('FileHistory',TMenuItem(MenuItem));
            NextToken;
          end
          else NextToken;
        end
        else begin
          MenuItem:=TMenuItem.Create(Menu);
          CheckToken(toSymbol);
          FuncName:=TokenString;
          MenuItem.Action:=MenuFunctions[FuncName];
          CheckNextToken('=');
          MenuItem.Caption:=CheckNextTokenString(toString);
          NextToken;
          if Token='{' then ReadMenu(MenuItem);
               if Copy(FuncName,1,1)='_' then MenuItem.Free
               else if (Owner=Menu) then Menu.Items.Add(MenuItem)
               else TMenuItem(Owner).Add(MenuItem);
        end;
      end;
      NextToken;
    end;
    { check for duplicate accelerator-keys }
    {$IFDEF WIN32}
    {$IFOPT C+}
    if Owner is TMenuItem then MenuItem:=TMenuItem(Owner)
    else MenuItem:=TMenuItem(TMenu(Owner).Items);
    with MenuItem do for Cnt:=0 to Count-1 do begin
      APos:=Pos('&',Items[Cnt].Caption);
      if APos>0 then for Cnt1:=Cnt+1 to Count-1 do begin
        BPos:=Pos('&',Items[Cnt1].Caption);
        if BPos>0 then Assert(UpCase(Items[Cnt].Caption[APos+1])<>UpCase(Items[Cnt1].Caption[BPos+1]),
            Format('Duplicate Short-Cuts in Menu %s: %s, %s',[Name,Items[Cnt].Caption,Items[Cnt1].Caption]));
      end;
    end;
    {$ENDIF}
    {$ENDIF}
  end;
begin
  with FParser do begin
    if TokenSymbolIs('MenuBar') then begin
      Menu:=TUIMainMenu.Create(FOwner);
      IsPopup:=FALSE;
    end
    else begin
      Menu:=TUIPopupMenu.Create(FOwner);
      IsPopup:=TRUE;
    end;
    FMenus.Add(Menu);
    CheckNextToken(',');
    RenameComponent(Menu,CheckNextTokenString(toSymbol));
    CheckNextToken(']');
    NextToken;
    CheckTokenSymbol('Menu');
    CheckNextToken('=');
    CheckNextToken('{');
    ReadMenu(Menu);
  end;
end;

Function TextToShortCut(Text:String):TShortCut;
  Function CompareFront(var Text:String;const Front:String):Boolean;
  begin
    Result := False;
    if CompareText(Copy(Text,1,Length(Front)),Front)=0 then begin
      Result:=True;
      Delete(Text,1,Length(Front)+1);
    end;
  end;
var Key          : TShortCut;
    Shift        : TShortCut;
    Offset       : Word;
begin
  Result:=0;
  Shift:=0;
  Offset:=0;
  while True do begin
    if CompareFront(Text,'Shift') then Shift:=Shift or scShift
    else if CompareFront(Text,'Ctrl') then Shift:=Shift or scCtrl
    else if CompareFront(Text,'Alt') then Shift:=Shift or scAlt
    else if CompareFront(Text,'Num') then Offset:=$60
    else Break;
  end;
  if Text='' then Exit;
  if CompareText(Text,'Back')=0 then Key:=vk_Back
  else if CompareText(Text,'Tab')=0 then Key:=vk_Tab
  else if CompareText(Text,'Enter')=0 then Key:=vk_Tab
  else if CompareText(Text,'Esc')=0 then Key:=vk_Escape
  else if CompareText(Text,'Space')=0 then Key:=vk_Space
  else if CompareText(Text,'Ins')=0 then Key:=vk_Insert
  else if CompareText(Text,'Del')=0 then Key:=vk_Delete
  else if CompareText(Text,'Up')=0 then Key:=vk_Up
  else if CompareText(Text,'Down')=0 then Key:=vk_Down
  else if CompareText(Text,'Left')=0 then Key:=vk_Left
  else if CompareText(Text,'Right')=0 then Key:=vk_Right
  else if (UpCase(Text[1])='F')
      and (Length(Text)>1) then begin
    Delete(Text,1,1);
    Key:=StrToInt(Text)+$6F;
    if Key>$87 then Exit;
  end
  else begin                             
    // determine vortual key-code of the character
    Key:=VKKeyScan(AnsiLowerCase(Text)[1]);
    // convert windows shift-keys to delphi shift-keys
    Key:=((Key and $FF00) Shl 5) or (Key and $FF);
  end;  
  Result:=Shift or  Key or Offset;
end;

Procedure TMultiLngFile.RenameComponent(AComponent:TComponent;Const AName:String);
var Cnt          : Integer;
    NewName      : String;
begin
  Cnt:=0;
  NewName:=AName;
  while AComponent.Owner.FindComponent(NewName)<>NIL do begin
    Inc(Cnt);
    NewName:=AName+IntToStr(Cnt);
  end;
  AComponent.Name:=NewName;
end;

Procedure TMultiLngFile.ReadShortCuts;
var FunctionName : String;
    ShortCutText : String;
    ShortCut     : TShortCut;
    Default      : Boolean;
begin
  with FParser do begin
    CheckNextToken(']');
    NextToken;
    while (Token<>'[')
        and (Token<>toEOF) do begin
      CheckToken(toSymbol);
      FunctionName:=TokenString;
      CheckNextToken('=');
      Default:=TRUE;
      repeat
        ShortCutText:=CheckNextTokenString(toString);
        ShortCut:=TextToShortCut(ShortCutText);
        if ShortCut<>0 then begin
           FShortcuts.AddObject(FunctionName,TObject(MakeLong(ShortCut,Word(Default))));
        end;
        Default:=FALSE;
        NextToken;
      until Token<>',';
    end;
  end;
end;

Procedure TMultiLngFile.ReadToolbar;
var Toolbar      : TUIToolbar;
    TBVisible    : Boolean;
  Procedure ReadToolbarItems;
    Procedure AddItem(AItem:TControl);
    begin
      AItem.Parent:=Toolbar;
      Toolbar.OrderIndex[AItem]:=Toolbar.ComponentCount-1;
    end;
  var FuncName        : String;
      Button          : TToolbarButton97;
      Separator       : TToolbarSep97;
      SpecialName     : String;
      MenuFunction    : TMenuFunction;
      Combobox        : TCombobox;
  begin
    with FParser do begin
      CheckNextToken('=');
      CheckNextToken('{');
      NextToken;
      while Token<>'}' do begin
        if Token='<' then begin
          SpecialName:=CheckNextTokenString(toSymbol);
          if CompareText(SpecialName,'Separator')=0 then begin
            // create a separator-button
            Separator:=TToolbarSep97.Create(Toolbar);
            Separator.Width:=8;
            AddItem(Separator);
          end;
          NextToken;
        end
        else begin
          CheckToken(toSymbol);
          FuncName:=TokenString;
          MenuFunction:=MenuFunctions[FuncName];
          NextToken;
          if Token='=' then begin
            SpecialName:=CheckNextTokenString(toString);
            NextToken;
          end
          else SpecialName:='';
          if FuncName[1]<>'_' then case MenuFunction.ToolbarControlType of
            tcButton: begin
                Button:=TUIToolbarButton.Create(Toolbar);
{++ IDB_UNDO Interface}
                If FuncName = 'IDB_UndoFunction' Then
                   AnUndoButton := TUIToolbarButton(Button);
                If Funcname = 'IDB_RedoFunction' Then
                   ARedoButton := TUIToolbarButton(Button);
{-- IDB_UNDO Interface}
{++ IDB_INFO}
                If FuncName = 'IDB_Info' Then
                   AnIDBInfoButton := TUIToolbarButton(Button);
{-- IDB_INFO}
                AddItem(Button);
                Button.Action:=MenuFunction;
                with TMenuFunction(Button.Action) do if (CheckGroup>0)
                    or (ToggleCheckState) then Button.GroupIndex:=Toolbar.ComponentCount;
                Button.AllowAllUp:=TRUE;
                Button.Images:=MenuFunctions.Images;
                Button.Caption:=SpecialName;
              end;
            tcCombobox: begin
                Combobox:=TCombobox(MenuFunction.CreateToolbarControl(Toolbar));
                MenuFunction.AddControl(Combobox);
                AddItem(Combobox);
                // create button to show if vertically docked
                Button:=TUIToolbarButton.Create(Toolbar);
                AddItem(Button);
                Button.Action:=MenuFunctions[MenuFunction.VerticalDockedFunction];
                with TMenuFunction(Button.Action) do if (CheckGroup>0)
                    or (ToggleCheckState) then Button.GroupIndex:=Toolbar.ComponentCount;
                Button.AllowAllUp:=TRUE;
                Button.Images:=MenuFunctions.Images;
                Button.Caption:=SpecialName;
                Toolbar.SetSlaveControl(Combobox,Button);
              end;
          end;
        end;
      end;
    end;
  end;
begin
  Toolbar:=TUIToolbar.Create(FOwner);
  with Toolbar do begin
    BeginUpdate;
    with FParser do begin
      CheckNextToken(',');
      RenameComponent(Toolbar,CheckNextTokenString(toSymbol));
      CheckNextToken(']');
      NextToken;
      while (Token<>'[') and (Token<>toEOF) do begin
        CheckToken(toSymbol);
        if CompareText(TokenString,'Items')=0 then ReadToolbarItems
        else if CompareText(TokenString,'Caption')=0 then begin
          CheckNextToken('=');
          Caption:=CheckNextTokenString(toString);
        end
        else if CompareText(TokenString,'Type')=0 then begin
          CheckNextToken('=');
          NextToken;
          FParser.CheckToken(toSymbol);
          if CompareText(TokenString,'Program')=0 then Toolbar.ToolbarType:=ttProgram
          else if CompareText(TokenString,'Context')=0 then Toolbar.ToolbarType:=ttContext
          else if CompareText(TokenString,'User')=0 then Toolbar.ToolbarType:=ttUser;
        end
        else if CompareText(TokenString,'CloseButton')=0 then begin
          CheckNextToken('=');
          NextToken;
          FParser.CheckToken(toInteger);
          if FParser.TokenInt=0 then Toolbar.CloseButton:=FALSE;
        end;
        NextToken;
      end;
    end;
    EndUpdate;
  end;
  TBVisible:=True;
  {$IFNDEF CL}
  if (Uppercase(Toolbar.Name) = 'WINMAPLT') then TBVisible:=False;
  {$ENDIF}
  if TBVisible then FToolbars.Add(Toolbar);
end;

Procedure TMultiLngFile.ReadStrings;
var SectionName  : String;
    StringNumber : Integer;
    SectionNumber: Integer;
    Text         : String;
begin
 try
  with FParser do begin
    CheckNextToken(',');
    SectionName:=CheckNextTokenString(toSymbol);
    SectionNumber:=FMlgStringList.AddSection(SectionName);
    CheckNextToken(']');
    NextToken;
    while (Token<>'[') and (Token<>toEOF) do begin
      CheckToken(toInteger);
      StringNumber:=TokenInt;
      CheckNextToken('=');
      Text:=CheckNextTokenString(toString);
      FMlgStringList.AddString(SectionNumber,StringNumber,Text);
      NextToken;
    end;
  end;
 except
       on E: Exception do begin
          MessageBox(0,PChar(E.Message),nil,MB_ICONERROR + MB_SYSTEMMODAL);
       end;
 end;
end;

Procedure TMultiLngFile.Open(const FileName:String;ComponentOwner:TComponent);
var FStream      : TStream;
begin
  FOwner:=ComponentOwner;
  FStream:=TFileStream.Create(FileName,fmOpenRead);
  FParser:=TParser.Create(FStream);
  try
    while FParser.Token<>toEOF do begin
      FParser.CheckToken('[');
      FParser.NextToken;
      if FParser.TokenSymbolIs('MenuBar') then ReadMenu
      else if FParser.TokenSymbolIs('PopupMenu') then ReadMenu
      else if FParser.TokenSymbolIs('Strings') then ReadStrings
      else if FParser.TokenSymbolIs('ShortCuts') then ReadShortCuts
      else if FParser.TokenSymbolIs('Toolbar') then ReadToolbar
      else if FParser.TokenSymbolIs('FileInfo') then ReadFileInfo
    end;
  finally
    FParser.Free;
    FStream.Free;
  end;
end;

{******************************************************************************+
  Procedure TMultiLngFile.ReadFileInfo
--------------------------------------------------------------------------------
  Function to read the FileInfo section. Currently overreads the whole section.
+******************************************************************************}
Procedure TMultiLngFile.ReadFileInfo;
begin
  with FParser do begin
    NextToken;
    while (Token<>'[') and (Token<>toEOF) do NextToken;
  end;
end;

end.
