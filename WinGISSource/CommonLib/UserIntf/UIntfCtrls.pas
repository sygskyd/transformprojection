Unit UintfCtrls;

Interface

Uses Classes,Controls,Menus,Messages,TB97,TB97Tlbr,TB97Ctls,TB97TlWn;

Type TToolbarType       = (ttProgram,ttUser,ttContext);

     {*************************************************************************+
      Class TUIMainMenu
     ---------------------------------------------------------------------------
       Verwaltet ein Men� und seine Zusatzinformationen. UserInterface.FMenus
       speichert eine Liste von Objekten dieses Typs.
     ---------------------------------------------------------------------------
       SpecialMenus = Liste mit Men�punkten an denen Listen (wie z.B. die Liste
           der ge�ffneten Fenster oder die Liste der zuletzt ge�ffneten Dateien
           angeh�ngt wird
     +*************************************************************************}
     TUIMainMenu        = Class(TMainMenu)
      Private                     
       FSpecialMenus    : TStringList;
       Function    GetSpecialMenu(Const Name:String):TMenuItem;
      Protected
       Procedure   AssignTo(Dest:TPersistent); override;
      Public
       Constructor Create(AParent:TComponent); override;
       Destructor  Destroy; override;
       Procedure   AddSpecialMenu(Const Name:String;AMenu:TMenuItem);
       Property    SpecialMenus[Const Name:String]:TMenuItem read GetSpecialMenu; default;
     end;

     {*************************************************************************+
       Class TUIPopupMenu
     ---------------------------------------------------------------------------
       Verwaltet ein Men� und seine Zusatzinformationen. UserInterface.FMenus
       speichert eine Liste von Objekten dieses Types.
     ---------------------------------------------------------------------------
       SpecialMenus = Liste mit Men�punkten an denen Listen (wie z.B. die Liste
           der ge�ffneten Fenster oder die Liste der zuletzt ge�ffneten Dateien
           angeh�ngt wird
     +*************************************************************************}
     TUIPopupMenu        = Class(TPopupMenu)
      Private
       FSpecialMenus    : TStringList;
       Function    GetSpecialMenu(Const Name:String):TMenuItem;
      Public
       Constructor Create(AParent:TComponent); override;
       Destructor  Destroy; override;
       Procedure   AddSpecialMenu(Const Name:String;AMenu:TMenuItem);
       Procedure   Popup(X,Y:Integer); override;
       Property    SpecialMenus[Const Name:String]:TMenuItem read GetSpecialMenu; default;
     end;

     TUIToolbar         = Class(TToolbar97)
      Private
       FType            : TToolbarType;
      Protected
       Procedure   ChangeScale(M,D:Integer); override;
      Public
       Constructor Create(AOwner:TComponent); override;
       Property    ToolbarType:TToolbarType read FType write FType;
     end;

     TUIToolWindow = Class(TToolWindow97)
      Private
       FOnShow     : TNotifyEvent;
       FOnHide     : TNotifyEvent;
       Procedure   CMShowingChanged(var Msg:TMessage); message cm_ShowingChanged;
      Published
       Property    OnCanResize;
       Property    OnHide:TNotifyEvent read FOnHide write FOnHide;
       Property    OnShow:TNotifyEvent read FOnShow write FOnShow;
      end;

     TUIToolbarButton = Class(TToolbarButton97)
     end;
     
Implementation

Uses MenuFn,UserIntf;

{==============================================================================+
  Common
+==============================================================================}


Procedure CopyMenu(SourceSpecial,DestSpecial:TStringList;
   Source:TMenuItem;Dest:TMenuItem);
var MenuItem       : TMenuItem;
    DestItem       : TMenuItem;
    Cnt            : Integer;
    Index          : Integer;
begin
  for Cnt:=0 to Source.Count-1 do begin
    MenuItem:=Source.Items[Cnt];
    DestItem:=TMenuItem.Create(Dest);
    DestItem.Name:=MenuItem.Name;
    DestItem.Action:=MenuItem.Action;
    DestItem.Caption:=MenuItem.Caption;
    Dest.Add(DestItem);
    Index:=SourceSpecial.IndexOfObject(MenuItem);
    if Index>=0 then DestSpecial.AddObject(SourceSpecial[Index],DestItem);
    CopyMenu(SourceSpecial,DestSpecial,MenuItem,DestItem);
  end;
end;

{==============================================================================+
  TUIMainMenu
+==============================================================================}

Constructor TUIMainMenu.Create
   (
   AParent         : TComponent
   );
  begin
    inherited Create(AParent);
    FSpecialMenus:=TStringList.Create;
    Images:=MenuFunctions.Images;
  end;

Destructor TUIMainMenu.Destroy;
begin
  FSpecialMenus.Free;
  inherited Destroy;
end;

Function TUIMainMenu.GetSpecialMenu
   (
   Const Name      : String
   )
   : TMenuItem;
  var AIndex       : Integer;
  begin
    if FSpecialMenus.Find(Name,AIndex) then Result:=TMenuItem(FSpecialMenus.Objects[AIndex])
    else Result:=NIL;
  end;

Procedure TUIMainMenu.AddSpecialMenu
   (
   Const Name      : String;
   AMenu           : TMenuItem
   );
  begin
    FSpecialMenus.AddObject(Name,AMenu);
  end;

Procedure TUIMainMenu.AssignTo(Dest:TPersistent);
begin
  if Dest is TUIMainMenu then with Dest as TUIMainMenu do begin
    AutoMerge:=Self.AutoMerge;
    BiDiMode:=Self.BiDiMode;
    OwnerDraw:=Self.OwnerDraw;
    ParentBiDiMode:=Self.ParentBiDiMode;
    Images:=Self.Images;
    Tag:=Self.Tag;
    OnChange:=Self.OnChange;
    CopyMenu(Self.FSpecialMenus,FSpecialMenus,Self.Items,Items);
  end
  else inherited AssignTo(Dest);
end;

{===============================================================================
| TUIPopupMenu
+==============================================================================}

Constructor TUIPopupMenu.Create
   (
   AParent         : TComponent
   );
  begin
    inherited Create(AParent);
    FSpecialMenus:=TStringList.Create;
    FSpecialMenus.Sorted:=TRUE;
    Images:=MenuFunctions.Images;
  end;

Destructor TUIPopupMenu.Destroy;
  begin                         
    FSpecialMenus.Free;
    inherited Destroy;
  end;

Function TUIPopupMenu.GetSpecialMenu
   (
   Const Name      : String
   )
   : TMenuItem;
  var AIndex       : Integer;
  begin
    if FSpecialMenus.Find(Name,AIndex) then Result:=TMenuItem(FSpecialMenus.Objects[AIndex])
    else Result:=NIL;
  end;

Procedure TUIPopupMenu.AddSpecialMenu
   (
   Const Name      : String;
   AMenu           : TMenuItem
   );
  begin
    FSpecialMenus.AddObject(Name,AMenu);
  end;

{==============================================================================+
  TUIToolbar
+==============================================================================}

Constructor TUIToolbar.Create(AOwner:TComponent);
begin
  inherited Create(AOwner);
  ActivateParent:=FALSE;
  Hint:='| ';
  HideWhenInactive:=FALSE;
end;

Procedure TUIToolbar.ChangeScale
   (
   M               : Integer;
   D               : Integer
   );
  begin
    inherited ChangeScale(M,D);
{    ButtonWidth:=MulDiv(ButtonWidth,M,D);
    ButtonHeight:=MulDiv(ButtonHeight,M,D);}
  end;

procedure TUIPopupMenu.Popup(X, Y: Integer);
begin
  UserInterface.Update([uiMenus],TRUE);
  inherited Popup(X,Y); 
end;

{ TUIToolWindow }

procedure TUIToolWindow.CMShowingChanged(var Msg: TMessage);
begin
  inherited;
  if Showing then if Assigned(FOnShow) then OnShow(Self) else
  else if Assigned(FonHide) then OnHide(Self);
end;

end.
