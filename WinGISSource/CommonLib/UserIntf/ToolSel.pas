Unit Toolsel;

Interface

Uses WinTypes,WinProcs,Chklist,Classes,Controls,Forms,Graphics,
     StdCtrls,UserIntf,MultiLng,WCtrls;

Type TToolbarSelectType = (tsToolbars,tsRollups,tsBoth);

     TToolbarSelectDialog    = Class(TWForm)
       CancelBtn        : TButton;
       MlgSection       : TMlgSection;
       OkBtn            : TButton;
       ToolbarList      : TCheckedListbox;
       Procedure FormShow(Sender: TObject);
       Procedure FormHide(Sender: TObject);
      Private
       FType       : TToolbarSelectType;
      Public
       Property    SelectType:TToolbarSelectType read FType write FType;
     end;

Implementation

{$R *.DFM}

Uses Rollup,SysUtils,UintfCtrls;

Procedure TToolbarSelectDialog.FormShow(Sender: TObject);
  var Cnt          : Integer;
      Toolbar      : TUIToolbar;
  begin
    if FType in [tsToolbars,tsBoth] then Caption:=MlgSection[1]
    else Caption:=MlgSection[4];
    with ToolbarList do begin
      Items.BeginUpdate;
      if FType in [tsToolbars,tsBoth] then for Cnt:=0 to UserInterface.ToolbarCount-1 do begin
        Toolbar:=UserInterface.Toolbars[Cnt];
        if Toolbar.ToolbarType=ttUser then begin
          Items.Add(Toolbar.Caption);
          Checked[Items.Count-1]:=Toolbar.Visible;
        end;
      end;
      if FType in [tsRollups,tsBoth] then for Cnt:=0 to UserInterface.RollupCount-1 do begin
        Items.Add(UserInterface.Rollups[Cnt].Caption);
        Checked[Items.Count-1]:=UserInterface.Rollups[Cnt].RollupToolWindow.Visible;
      end;
      Items.EndUpdate;
    end;
  end;

Procedure TToolbarSelectDialog.FormHide(Sender: TObject);
var Cnt          : Integer;
    Toolbar      : TUIToolbar;
    Toolbars     : Integer;
begin
  if ModalResult=mrOK then with ToolbarList do begin
    Toolbars:=0;
    if FType in [tsToolbars,tsBoth] then for Cnt:=0 to UserInterface.ToolbarCount-1 do begin
      Toolbar:=UserInterface.Toolbars[Cnt];
      if Toolbar.ToolbarType=ttUser then begin
        Toolbar.Visible:=Checked[Toolbars];
        Inc(Toolbars);
      end;
    end;  
    if FType in [tsRollups,tsBoth] then for Cnt:=0 to UserInterface.RollupCount-1 do
        UserInterface.Rollups[Cnt].RollupToolWindow.Visible:=Checked[Cnt+Toolbars]; 
  end;
end;                               

end.

