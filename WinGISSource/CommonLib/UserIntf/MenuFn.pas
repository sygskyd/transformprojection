{******************************************************************************+
  Unit MenuFn
--------------------------------------------------------------------------------
  Autor: Martin Forst
--------------------------------------------------------------------------------
  Klassen:
  - TMenuFunction    Definition eines Men�punkts
  - TShortcutItem    Definition einer Tastenkombination
  - TMenuFunctions   Liste aller registrierten Men�punkte
  - TMenuFnButton    Button f�r die Toolbar-Leiste
  - TMenuFnMenu      Men�punkt f�r ein Haupt- oder Popup-Men�
--------------------------------------------------------------------------------
  ToDo
  - replace ShortCutToText by a multilanguage-version
+******************************************************************************}
Unit MenuFn;

Interface

Uses Controls,Classes,Lists,WinProcs,WinTypes,Graphics,Hash,Menus,Messages,WCtrls,
     ActnList,ComCtrls;

Const CurrentVersion    = 6;

Type TToolbarControlType= (tcButton,tcEdit,tcListbox,tcCombobox,tcCustom);

     TMenuFunction      = Class(TCustomAction)
      Private
       FBitmap          : TBitmap;
       FCheckGroup      : Integer;
       FCheckMenus      : Boolean;
       FContextToolbar  : AnsiString;
       FControls        : TList;
       FEnableGroup     : Integer;
       FHintNumber      : Integer;
       FHintSection     : AnsiString;
       FItemIndex       : Integer;
       FItems           : TStringList;
       FReinvoke        : Boolean;
       FToolbarControl  : TToolbarControlType;
       FToolbarControlWidth : Integer;
       FToggle          : Boolean;
       FVerticalDocked  : AnsiString;
       FVisibleGroup    : Integer;
       Procedure   DoItemsChange(Sender:TObject);
       Procedure   ReadPrivateData(Reader:TReader);
       Procedure   SetItemIndex(AIndex:Integer);
       Procedure   SetItems(AItems:TStringList);
       Procedure   SetToolbarControl(AControl:TToolbarControlType);
       Procedure   UpdateChecked;
       Procedure   WritePrivateData(Writer:TWriter);
      Protected
       Procedure   AssignTo(Dest:TPersistent); override;
       Procedure   DefineProperties(Filer:TFiler); override;
       Procedure   DoListChange(Sender:TObject);
       Procedure   DoListClick(Sender:TObject);
       Function    GetChecked:Boolean; virtual;
       Function    GetEnabled:Boolean; virtual;
       Function    GetVisible:Boolean; virtual;
       Procedure   Loaded; override;
       Procedure   Notification(AComponent:TComponent;Operation:TOperation); override;
       Procedure   SetChecked(AChecked:Boolean); virtual;
       Procedure   SetEnabled(AEnabled:Boolean); virtual;
       Procedure   SetVisible(AVisible:Boolean); virtual;
      Public
       Constructor Create(AOwner:TComponent); override;
       Destructor  Destroy; override;
       Procedure   AddControl(AControl:TComponent);
       Procedure   Call(Sender:TObject);
       Property    Checked:Boolean read GetChecked write SetChecked;
       Property    CheckGroup:Integer read FCheckGroup write FCheckGroup;
       Property    CheckMenus:Boolean read FCheckMenus write FCheckMenus;
       Function    CreateToolbarControl(Owner:TComponent):TControl; virtual;
       Property    ContextToolbar:AnsiString read FContextToolbar write FContextToolbar;
       Procedure   DeleteControl(AControl:TComponent);
       Property    Enabled:Boolean read GetEnabled write SetEnabled;
       Property    EnableGroup:Integer read FEnableGroup write FEnableGroup;
       Function    Execute:Boolean; override;
       Function    Handle(Handler:TObject):Boolean;
       Property    HintNumber:Integer read FHintNumber write FHintNumber;
       Property    HintSection:AnsiString read FHintSection write FHintSection;
       Property    ItemIndex:Integer read FItemIndex write SetItemIndex;
       Property    Items:TStringList read FItems write SetItems;
       Property    Reinvokeable:Boolean read FReinvoke write FReinvoke;
       Property    ToolbarControlType:TToolbarControlType read FToolbarControl write SetToolbarControl;
       Property    ToolbarControlWidth:Integer read FToolbarControlWidth write FToolbarControlWidth;
       Property    ToggleCheckState:Boolean read FToggle write FToggle;
       Property    VerticalDockedFunction:AnsiString read FVerticalDocked write FVerticalDocked;
       Property    Visible:Boolean read GetVisible write SetVisible default True;
       Property    VisibleGroup:Integer read FVisibleGroup write FVisibleGroup;
      Published
       Property    Bitmap:TBitmap read FBitmap write FBitmap;  
     end;

     TMenuFunctionClass = Class of TMenuFunction;

     TDefaultMenuNotification= Procedure(Sender:TObject;MenuFunction:TMenuFunction) of Object;

     TShortCutItem      = Class(TObject)
      Private
       FFunction   : TMenuFunction;
       FDefault    : Boolean;
       ShortCut    : TShortCut;
       Function    GetFunctionName:String;
       Function    GetShortCutText:String;
       Procedure   SetFunctionName(Const AName:String);
      Public
       Constructor Create(Const AFunctionName:String;AShortCut:TShortCut);
       Property    DefaultShortCut:Boolean read FDefault write FDefault;
       Property    FunctionName:String read GetFunctionName write SetFunctionName;
       Property    MenuFunction:TMenuFunction read FFunction;
       Property    ShortCutText:String read GetShortCutText;
     end;

     TMenuFunctions     = Class(TComponent)
      Private
       FCurrentChecked  : TStringList;
       FGroupEnables    : TBitList;
       FGroupVisibles   : TBitList;
       FHandlers        : TStringList;
       FImages          : TImageList;
       FLastGroupEnables: TBitList;
       FLastGroupVisibles : TBitList;
       FOnCheck         : TDefaultMenuNotification;
       FOnUncheck       : TDefaultMenuNotification;
       FShortCuts       : TAutoHashTable;
       FVersion         : Integer;
       Procedure   DoCall(Sender:TObject;MenuFunction:TMenuFunction);
       Function    GetChecked(Const AFunctionName:String):Boolean;
       Function    GetCheckedInGroup(AGroup:Integer):TMenuFunction;
       Function    GetEnabled(Const AFunctionName:String):Boolean;
       Function    GetGroupEnabled(AGroup:Integer):Boolean;
       Function    GetGroupVisible(AGroup:Integer):Boolean;
       Function    GetMenuFunction(Const AFunctionName:String):TMenuFunction;
       Function    GetShortCutName(Const AFunctionName:String):String;
       Function    GetShortCuts(Const AFunctionName:String):TList;
       Function    GetVisibleGroupFunction(AGroup:Integer):TMenuFunction; 
       Procedure   DoCheck(MenuFunction:TMenuFunction);
       Procedure   DoUnCheck(MenuFunction:TMenuFunction);
       Procedure   ReadPrivateData(Reader:TReader);
       Procedure   SetChecked(Const AFunctionName:String;ASet:Boolean);
       Procedure   SetCurrentChecked(ACurrentChecked:TStringList);
       Procedure   SetEnabled(Const AFunctionName:String;ASet:Boolean);
       Procedure   SetGroupEnabled(AGroup:Integer;ASet:Boolean);
       Procedure   SetGroupVisible(AGroup:Integer;ASet:Boolean);
       Procedure   SetHintSection(Const AHintSection:String);
       Procedure   WritePrivateData(Writer:TWriter);
      Protected
       Procedure   DefineProperties(Filer:TFiler); override;
       Procedure   GetChildren(Proc:TGetChildProc;Root:TComponent); override;
       Procedure   Notification(AComponent:TComponent;Operation:TOperation); override;
      Public
       Constructor Create(AOwner:TComponent); override;
       Destructor  Destroy; override;
       Procedure   AddShortCut(Const AFunctionName:String;AShortCut:TShortCut;ADefault:Boolean);
       Procedure   BeginUpdate;
       Procedure   Call(Const AFunctionName:String);
       Property    Checked[Const AFunctionName:String]:Boolean read GetChecked write SetChecked;
       Property    CheckedMenuFunctions:TStringList read FCurrentChecked write SetCurrentChecked;
       Property    CheckedInGroup[AGroup:Integer]:TMenuFunction read GetCheckedInGroup;
       Procedure   ClearShortCuts;
       Function    DispatchShortCut(ShortCut:Word):Boolean;
       Property    Enabled[Const AFunctionName:String]:Boolean read GetEnabled write SetEnabled;
       Procedure   EndUpdate;
       Property    Functions[Const AFunctionName:String]:TMenuFunction read GetMenuFunction; default;
       Property    GroupEnabled[AGroup:Integer]:Boolean read GetGroupEnabled write SetGroupEnabled;
       Property    GroupVisible[AGroup:Integer]:Boolean read GetGroupVisible write SetGroupVisible;
       Property    HintSection:String write SetHintSection;
       Property    Images:TImageList read FImages write FImages;
       Property    OnCheck:TDefaultMenuNotification read FOnCheck write FOnCheck;
       Property    OnUncheck:TDefaultMenuNotification read FOnUnCheck write FOnUnCheck;
       Procedure   RegisterMenuFunctions(AMenuFunctions:TMenuFunctions);
       Procedure   RegisterFromResource(HInst:Integer;Const ResourceName:String;Const HintSection:String);
       Procedure   RegisterAlternativeHandler(const FunctionName:String;HandlerClass:TMenuFunctionClass);
       Procedure   SetAction(Control:TControl;Const AFunctionName:String); overload;
       Procedure   SetAction(Control:TMenuItem;Const AFunctionName:String); overload;
       Procedure   SetAction(Control:TMenuItem;Action:TCustomAction); overload;
       Property    ShortCuts[Const AFunctionName:String]:TList read GetShortCuts;
       Property    ShortCutName[Const AFunctionName:String]:String read GetShortCutName;
       Procedure   UpdateContextToolbars;
       Property    VisibleGroupFunction[AGroup:Integer]:TMenuFunction read GetVisibleGroupFunction;
      Published
       Procedure   OnLanguageChanged;
     end;

Function CallMethodByName(Handler:TComponent;Const Name:String):Boolean;

Function RemoveNumbers(Const AName:String):String;

var MenuFunctions               : TMenuFunctions;
    MenuFunctionImages          : TImageList;

Implementation

Uses ExtCtrls,Forms,MultiLng,NumTools,StdCtrls,SysUtils,TypInfo,UserIntf;

Const MaxEnableGroup    = 10000;

var DefMenuFunction     : TMenuFunction;    { menufunction assigned to unknown menus }

Type {**************************************************************************
     | TShortCutHash
     |--------------------------------------------------------------------------
     | Hashtable for the keyboard-shortcuts.
     +*************************************************************************}
     TShortCutHash      = Class(TAutoHashTable)
      Protected
       Function    KeyOf(Item:Pointer):Pointer; override;
       Function    OnCompareItems(Item1,Item2:Pointer):Boolean; override;
       Function    OnGetHashValue(Item:Pointer):LongInt; override;
     end;

Function CallMethodByName
   (
   Handler       : TComponent;
   Const Name    : String
   )
   : Boolean;
  var Method          : TMethod;
  begin
    Result:=FALSE;
    if Assigned(Handler) and not (csDestroying in Handler.ComponentState) then begin
      Method.Data:=Handler;
      Method.Code:=Handler.MethodAddress(Name);
      if Assigned(Method.Code) then begin
        TNotifyEvent(Method)(NIL);
        Result:=TRUE;
      end;
    end;
  end;

Function RemoveNumbers(Const AName:String):String;
begin
  Result:=AName;
  {$IFDEF WIN32}
  while Result[Length(Result)] in ['0'..'9'] do SetLength(Result,Length(Result)-1);
  {$ELSE}
  while Result[Length(Result)] in ['0'..'9'] do Dec(Byte(Result[0]));
  {$ENDIF}
end;

{==============================================================================+
  TMenuFunction                                                                
+==============================================================================}

Constructor TMenuFunction.Create
   (
   AOwner          : TComponent
   );
  begin
    inherited Create(AOwner);
    FBitmap:=TBitmap.Create;
    DisableIfNoHandler:=FALSE;
  end;

Destructor TMenuFunction.Destroy;
  var Cnt          : Integer;
  begin
    if FControls<>NIL then begin
      for Cnt:=0 to FControls.Count-1 do TControl(FControls[Cnt]).RemoveComponent(Self);
      FControls.Free;
    end;
    FBitmap.Free;
    if ImageIndex>-1 then MenuFunctions.Images.Delete(ImageIndex);
    FItems.Free;
    inherited Destroy;
  end;

Procedure TMenuFunction.AssignTo(Dest:TPersistent);
begin
  inherited AssignTo(Dest);
  with Dest as TMenuFunction do begin
    FBitmap.Assign(Self.FBitmap);
    FCheckGroup:=Self.FCheckGroup;
    FCheckMenus:=Self.FCheckMenus;
    FContextToolbar:=Self.FContextToolbar;
    FEnableGroup:=Self.FEnableGroup;
    FHintNumber:=Self.FHintNumber;
    FHintSection:=Self.FHintSection;
    FItemIndex:=Self.FItemIndex;
    SetToolbarControl(Self.FToolbarControl);
    FToggle:=Self.FToggle;
    FVisibleGroup:=Self.FVisibleGroup;
    FVerticalDocked:=Self.FVerticalDocked;
  end;
end;

Function TMenuFunction.GetEnabled:Boolean;
begin
  Result:=inherited Enabled;
end;  

Procedure TMenuFunction.SetChecked
   (
   AChecked        : Boolean
   );
  var Cnt          : Integer;
      AMenuFunction: TMenuFunction;
  begin
    if AChecked<>Checked then begin
      inherited Checked:=AChecked;
      if FContextToolbar<>'' then UserInterface.Update([uiContextToolbars]);
      // set linked controls checked or unchecked
      if Checked then MenuFunctions.DoCheck(Self)
      else MenuFunctions.DoUnCheck(Self);
      // uncheck all menu-functions with the same check-group
      if (FCheckGroup>0) and Checked then for Cnt:=0 to Owner.ComponentCount-1 do begin
        AMenuFunction:=TMenuFunction(Owner.Components[Cnt]);
        if (AMenuFunction<>Self) and (AMenuFunction.CheckGroup=CheckGroup)
            and AMenuFunction.Checked then begin
          if AMenuFunction.ToggleCheckState then AMenuFunction.Call(Self)
          else AMenuFunction.Checked:=FALSE;
        end;
      end;
    end;
  end;

Function TMenuFunction.GetChecked:Boolean;
begin
  Result:=inherited Checked;
end;

Procedure TMenuFunction.SetEnabled(AEnabled:Boolean);
var Cnt          : Integer;
begin
  if AEnabled<>Enabled then begin
    inherited Enabled:=AEnabled;
    if FControls<>NIL then for Cnt:=0 to FControls.Count-1 do
        TControl(FControls[Cnt]).Enabled:=AEnabled;
  end;
end;

Function TMenuFunction.GetVisible:Boolean;
begin                                                            
  Result:=inherited Visible;
end;  

Procedure TMenuFunction.SetVisible(AVisible:Boolean);
var Cnt          : Integer;
begin
  if AVisible<>Visible then begin
    inherited Visible:=AVisible;
    if FControls<>NIL then for Cnt:=0 to FControls.Count-1 do
        TControl(FControls[Cnt]).Visible:=AVisible;
  end;
end;

Procedure TMenuFunction.AddControl(AControl:TComponent);
begin
  if AControl is TControl then begin
    if FControls=NIL then FControls:=TList.Create;
    // add the control to the list
    FControls.Add(AControl);
    // menu-function must be informed when the control is freed
    AControl.FreeNotification(Self);
    TControl(AControl).Enabled:=Enabled;
    TControl(AControl).Visible:=Visible;
  end;
end;

Procedure TMenuFunction.Notification(AComponent:TComponent;Operation:TOperation);
begin
  inherited Notification(AComponent,Operation);
  if Operation=opRemove then if FControls<>NIL then FControls.Remove(AComponent);
end;

Procedure TMenuFunction.DeleteControl(AControl:TComponent);
begin
  if FControls<>NIL then begin
    FControls.Remove(AControl);                   
    AControl.RemoveComponent(Self);
  end;
end;

Procedure TMenuFunction.Call(Sender:TObject);
begin
  TMenuFunctions(Owner).DoCall(Sender,Self);
end;

Function TMenuFunction.Handle
   (
   Handler         : TObject
   )
   : Boolean;
  var Method       : TMethod;
  begin
    Result:=FALSE;
    if Assigned(Handler) then begin
      Method.Data:=Handler;
      Method.Code:=TObject(Handler).MethodAddress(Name);
      if Assigned(Method.Code) then begin
        TNotifyEvent(Method)(Self);
        Result:=TRUE;
      end
      else begin
        Method.Code:=TObject(Handler).MethodAddress('DefaultMenuHandler');
        if Assigned(Method.Code) then begin
          TDefaultMenuNotification(Method)(Self,Self);
          Result:=TRUE;
        end;
      end;
    end;
  end;

Procedure TMenuFunction.DefineProperties
   (
   Filer           : TFiler
   );
  begin   
    Filer.DefineProperty('Data',ReadPrivateData,WritePrivateData,TRUE);
  end;

Procedure TMenuFunction.ReadPrivateData
   (
   Reader          : TReader
   );
  begin
    FEnableGroup:=Reader.ReadInteger;
    FCheckGroup:=Reader.ReadInteger;
    FHintNumber:=Reader.ReadInteger;
    FCheckMenus:=Reader.ReadBoolean;
    FToggle:=Reader.ReadBoolean;
    if TMenuFunctions(Owner).FVersion>0 then
      FContextToolbar:=Reader.ReadString;
    if TMenuFunctions(Owner).FVersion>1 then
      HelpContext:=Reader.ReadInteger;
    if TMenuFunctions(Owner).FVersion>2 then
      FVisibleGroup:=Reader.ReadInteger;
    if TMenuFunctions(Owner).FVersion>3 then begin
      ToolbarControlType:=TToolbarControlType(Reader.ReadInteger);
      VerticalDockedFunction:=Reader.ReadString;
    end;
    if TMenuFunctions(Owner).FVersion>4 then FToolbarControlWidth:=Reader.ReadInteger;
    if TMenuFunctions(Owner).FVersion>5 then FReinvoke:=Reader.ReadBoolean;
  end;

Procedure TMenuFunction.WritePrivateData
   (
   Writer          : TWriter
   );
  begin
    Writer.WriteInteger(FEnableGroup);
    Writer.WriteInteger(FCheckGroup);
    Writer.WriteInteger(FHintNumber);
    Writer.WriteBoolean(FCheckMenus);
    Writer.WriteBoolean(FToggle);
    Writer.WriteString(FContextToolbar);
    Writer.WriteInteger(HelpContext);
    Writer.WriteInteger(FVisibleGroup);
    Writer.WriteInteger(Integer(FToolbarControl));
    Writer.WriteString(VerticalDockedFunction);
    Writer.WriteInteger(FToolbarControlWidth);
    Writer.WriteBoolean(FReinvoke);
  end;

Procedure TMenuFunction.Loaded;
var ABitmap        : TBitmap;
begin
  inherited Loaded;
  Enabled:=MenuFunctions.GroupEnabled[FEnableGroup];
  { add image to global image-list }
  if not FBitmap.Empty then begin
    ABitmap:=TBitmap.Create;
    try
      ABitmap.Width:=16;
      ABitmap.Height:=16;
      ABitmap.Canvas.Draw(0,0,FBitmap);
      ImageIndex:=MenuFunctionImages.AddMasked(ABitmap,FBitmap.TransparentColor);
    finally
      ABitmap.Free;
    end;
    FBitmap.Dormant;  
  end;
end;

Function TMenuFunction.Execute:Boolean;
begin
  if Enabled then Call(Self);
  Result:=TRUE;
end;

{******************************************************************************+
  Procedure TMenuFunction.UpdateChecked;
--------------------------------------------------------------------------------
  Updates the checked status of the linked controls. This must be done because
  linked controls normally change their checked-state without controlling
  the action if the state has already been checked or unchecked. 
+******************************************************************************}
Procedure TMenuFunction.UpdateChecked;
begin
  inherited Checked:=not Checked;
  inherited Checked:=not Checked;
end;

{******************************************************************************+
  Procedure TMenuFunction.SetItemIndex
--------------------------------------------------------------------------------
  Sets the item-index of linked listboxes to the new number.
+******************************************************************************}
Procedure TMenuFunction.SetItemIndex(AIndex:Integer);
var Cnt            : Integer;
begin
  if AIndex<>FItemIndex then if FControls<>NIL then begin
    FItemIndex:=AIndex;
    for Cnt:=0 to FControls.Count-1 do if TObject(FControls[Cnt]) is TCustomListbox then
      TListbox(FControls[Cnt]).ItemIndex:=AIndex;
    for Cnt:=0 to FControls.Count-1 do if TObject(FControls[Cnt]) is TCustomCombobox then
      TCombobox(FControls[Cnt]).ItemIndex:=AIndex;
  end;
end;

{******************************************************************************+
  Procedure TMenuFunction.SetItems
--------------------------------------------------------------------------------
  Sets the items of the 
+******************************************************************************}
Procedure TMenuFunction.SetItems(AItems:TStringList);
begin
  FItems.Assign(AItems);
end;

Procedure TMenuFunction.DoItemsChange(Sender:TObject);
var Cnt            : Integer;
begin
  if FControls<>NIL then begin
    for Cnt:=0 to FControls.Count-1 do if TObject(FControls[Cnt]) is TCustomListbox then begin
      TListbox(FControls[Cnt]).Items:=FItems;
      TListbox(FControls[Cnt]).ItemIndex:=FItemIndex;
    end;
    for Cnt:=0 to FControls.Count-1 do begin
        if TObject(FControls[Cnt]) is TCustomCombobox then begin
           TCombobox(FControls[Cnt]).DropDownCount:=16;
           TCombobox(FControls[Cnt]).Items:=FItems;
           TCombobox(FControls[Cnt]).ItemIndex:=FItemIndex;
        end;
    end;
  end;
end;

Procedure TMenuFunction.SetToolbarControl(AControl:TToolbarControlType);
begin
  if AControl<>FToolbarControl then begin
    FToolbarControl:=AControl;
    case FToolbarControl of
      tcListbox,
      tcCombobox: if FItems=NIL then begin
          FItems:=TStringList.Create;
          FItems.OnChange:=DoItemsChange;
        end;                          
    end;
  end;
end;

Function TMenuFunction.CreateToolbarControl(Owner:TComponent):TControl;
var Combobox       : TCombobox;
begin
  case FToolbarControl of
    tcCombobox: begin
        Combobox:=TCombobox.Create(Owner);
        if FToolbarControlWidth>0 then ComboBox.Width:=FToolbarControlWidth;
        Combobox.Style:=csDropDownList;
        Combobox.OnChange:=DoListChange;
        Combobox.OnClick:=DoListClick;
        Result:=Combobox;
      end;
    else Result:=NIL;
  end;
end;

Procedure TMenuFunction.DoListChange(Sender:TObject);
begin
  if Sender is TCustomCombobox then ItemIndex:=TComboBox(Sender).ItemIndex
  else if Sender is TCustomListbox then ItemIndex:=TListbox(Sender).ItemIndex;
end;

Procedure TMenuFunction.DoListClick(Sender:TObject);
begin
  if Sender is TCustomCombobox then ItemIndex:=TComboBox(Sender).ItemIndex
  else if Sender is TCustomListbox then ItemIndex:=TListbox(Sender).ItemIndex;
  Call(Self);
end;

{==============================================================================}
{ TMenuFunctions                                                               }
{==============================================================================}

Constructor TMenuFunctions.Create
   (
   AOwner          : TComponent
   );
  begin
    inherited Create(AOwner);
    FShortCuts:=TShortCutHash.Create(13);
    FGroupEnables:=TBitList.Create(MaxEnableGroup);
    FGroupEnables[1]:=TRUE;
    FGroupVisibles:=TBitList.Create(MaxEnableGroup+1);
    FGroupVisibles.SetAll;
    FGroupVisibles[MaxEnableGroup]:=FALSE;
    FLastGroupEnables:=TBitList.Create(MaxEnableGroup);
    FLastGroupVisibles:=TBitList.Create(MaxEnableGroup+1);
    FCurrentChecked:=TStringList.Create;
    FCurrentChecked.Sorted:=TRUE;
    FCurrentChecked.Duplicates:=dupIgnore;
    Images:=MenuFunctionImages;
    // add to list of language-depenend components
    LanguageDependend.Add(Self);
    // create list for alternative handlers
    FHandlers:=TStringList.Create;
  end;

Destructor TMenuFunctions.Destroy;
begin
  FShortCuts.Free;
  FGroupEnables.Free;
  FGroupVisibles.Free;
  FLastGroupEnables.Free;
  FLastGroupVisibles.Free;
  FCurrentChecked.Free;
  // remove from list of language-depenend components
  LanguageDependend.Remove(Self);
  FHandlers.Free;
  //inherited Destroy;
end;

Function TMenuFunctions.GetChecked(Const AFunctionName:String):Boolean;
begin
  Result:=Functions[AFunctionName].Checked;
end;

Procedure TMenuFunctions.DoCheck(MenuFunction:TMenuFunction);
begin
  MenuFunctions.FCurrentChecked.AddObject(MenuFunction.Name,MenuFunction);
  if Assigned(FOnCheck) then OnCheck(Self,MenuFunction);
end;

Procedure TMenuFunctions.DoUnCheck(MenuFunction:TMenuFunction);
var AIndex       : Integer;
begin
  AIndex:=MenuFunctions.FCurrentChecked.IndexOf(MenuFunction.Name);
  if AIndex>-1 then FCurrentChecked.Delete(AIndex);
  if Assigned(FOnUnCheck) then OnUnCheck(Self,MenuFunction);
end;

Function TMenuFunctions.GetEnabled(Const AFunctionName:String):Boolean;
begin
  Result:=Functions[AFunctionName].Enabled;
end;

Function TMenuFunctions.GetMenuFunction(Const AFunctionName:String):TMenuFunction;
begin
  Result:=TMenuFunction(FindComponent(AFunctionName));
  if not Assigned(Result) then Result:=DefMenuFunction;
end;               

Procedure TMenuFunctions.SetChecked(Const AFunctionName:String;ASet:Boolean);
begin
  Functions[AFunctionName].Checked:=ASet;
end;

Procedure TMenuFunctions.SetEnabled(Const AFunctionName:String;ASet:Boolean);
begin
  Functions[AFunctionName].Enabled:=ASet;
end;

Procedure TMenuFunctions.Call(Const AFunctionName:String);
begin
  DoCall(Self,Functions[AFunctionName]);
end;

Procedure TMenuFunctions.DoCall(Sender:TObject;MenuFunction:TMenuFunction);

  Function CallMethod(Handler:TComponent;Const Name:String;Default:Boolean):Boolean;
  var Method          : TMethod;
  begin
    Result:=FALSE;
    if Assigned(Handler) then begin
      Method.Data:=Handler;
      Method.Code:=Handler.MethodAddress(Name);
      if Assigned(Method.Code) then begin
        if Default then TDefaultMenuNotification(Method)(Sender,MenuFunction)
        else TNotifyEvent(Method)(Sender);
        Result:=TRUE;
      end;                                      
    end;
  end;

begin
  OutputDebugString(PChar(MenuFunction.Name));
  if MenuFunction<>DefMenuFunction then begin
    if MenuFunction.CheckGroup<>0 then begin
      if MenuFunction.ToggleCheckState then
        MenuFunction.Checked:=not MenuFunction.Checked
      else if MenuFunction.Checked then begin
        MenuFunction.UpdateChecked;
        if not Menufunction.Reinvokeable then Exit;
      end
      else MenuFunction.Checked:=TRUE;
    end;
    if not CallMethod(Application.MainForm.ActiveMDIChild,MenuFunction.Name,FALSE) then
    if not CallMethod(Application.MainForm,MenuFunction.Name,FALSE) then
    if not CallMethod(Application.MainForm.ActiveMDIChild,'DefaultMenuHandler',TRUE) then
        CallMethod(Application.MainForm,'DefaultMenuHandler',TRUE);
    UserInterface.Update([uiMenus]);
  end;
end;

Function TMenuFunctions.GetShortCutName(Const AFunctionName:String):String;
var Cnt            : Integer;
begin
  Result:='';
  with FShortCuts do for Cnt:=0 to Capacity-1 do begin
    if Used(Items[Cnt])
        and (TShortCutItem(Items[Cnt]).FunctionName=AFunctionName)
        and (TShortCutItem(Items[Cnt]).DefaultShortCut) then begin
      Result:=TShortCutItem(Items[Cnt]).ShortCutText;
      break;
    end;
  end;
end;

Procedure TMenuFunctions.ClearShortCuts;
begin
  FShortCuts.Clear(TRUE);
end;

Function TMenuFunctions.GetShortCuts(Const AFunctionName:String):TList;
var Cnt          : Integer;
begin
  Result:=TList.Create;
  with FShortCuts do for Cnt:=0 to Capacity-1 do begin
    if Used(Items[Cnt])
        and (TShortCutItem(Items[Cnt]).FunctionName=AFunctionName) then
      Result.Add(Items[Cnt]);
  end;
end;

Procedure TMenuFunctions.AddShortCut(Const AFunctionName:String;
    AShortCut:TShortCut;ADefault:Boolean);
var ShortCutItem       : TShortCutItem;
begin
 ShortCutItem:=TShortCutItem.Create(AFunctionName,AShortCut);
 ShortCutItem.DefaultShortCut:=ADefault;
 FShortCuts.Add(ShortCutItem);
end;

Function TMenuFunctions.DispatchShortCut(ShortCut:Word):Boolean;
var ShortCutItem : TShortCutItem;
begin
  Result:=FALSE;
  try
    if (Screen.ActiveForm<>NIL)
    and (Screen.ActiveForm.Name<>'IDB_ViewForm')
    and (Screen.ActiveForm.Name<>'IDB_MonitorForm')
    and (Screen.ActiveForm.Name<>'WKTForm')
    and (Screen.ActiveForm.Name<>'ThematicMainForm')
    and (Screen.ActiveForm.BorderStyle<>bsDialog) then begin
      ShortCutItem:=FShortCuts.Find(@ShortCut);
      if Assigned(ShortCutItem)
          and ShortCutItem.MenuFunction.Enabled then begin
        Result:=TRUE;
        DoCall(Self,ShortCutItem.MenuFunction);
      end;
    end;
  except
    Application.HandleException(Self);
  end;
end;

Procedure TMenuFunctions.BeginUpdate;
begin
  FLastGroupEnables.Assign(FGroupEnables);
  FGroupEnables.ClearAll;
  FGroupEnables[1]:=TRUE;
  FLastGroupVisibles.Assign(FGroupVisibles);
  FGroupVisibles.ClearAll;
  FGroupVisibles[0]:=TRUE;
end;

Procedure TMenuFunctions.EndUpdate;
  var Cnt          : Integer;
      MenuFn       : TMenuFunction;
  begin
    for Cnt:=0 to ComponentCount-1 do begin
      MenuFn:=TMenuFunction(Components[Cnt]);
      if MenuFn.Name <> '' then begin
         if FLastGroupEnables[MenuFn.EnableGroup]<>FGroupEnables[MenuFn.EnableGroup] then begin
            MenuFn.Enabled:=FGroupEnables[MenuFn.EnableGroup];
         end;
         if FLastGroupVisibles[MenuFn.VisibleGroup]<>FGroupVisibles[MenuFn.VisibleGroup] then begin
            MenuFn.Visible:=FGroupVisibles[MenuFn.VisibleGroup];
         end;
      end;
    end;
  end;

Procedure TMenuFunctions.SetGroupEnabled
   (
   AGroup          : Integer;
   ASet            : Boolean
   );
  begin
    FGroupEnables[AGroup]:=ASet;
  end;

Procedure TMenuFunctions.SetGroupVisible
   (
   AGroup          : Integer;
   ASet            : Boolean
   );
  begin
    FGroupVisibles[AGroup]:=ASet;
  end;

Function TMenuFunctions.GetCheckedInGroup
   (
   AGroup          : Integer
   )
   : TMenuFunction;
  var Cnt          : Integer;
  begin
    for Cnt:=0 to FCurrentchecked.Count-1 do
        if TMenuFunction(FCurrentChecked.Objects[Cnt]).Checked then begin
      Result:=TMenuFunction(FCurrentChecked.Objects[Cnt]);
      Exit;
    end;
    Result:=NIL;
  end;

Procedure TMenuFunctions.SetCurrentChecked
   (
   ACurrentChecked : TStringList
   );
  var AIndex       : Integer;
      Cnt          : Integer;
  begin
    for Cnt:=FCurrentChecked.Count-1 downto 0 do begin
      AIndex:=ACurrentChecked.IndexOf(FCurrentChecked[Cnt]);
      if AIndex>-1 then FCurrentChecked.Delete(Cnt);
    end;
    for Cnt:=FCurrentChecked.Count-1 downto 0 do
      Checked[FCurrentChecked[Cnt]]:=FALSE;
    FCurrentChecked.Assign(ACurrentChecked);
    for Cnt:=0 to FCurrentChecked.Count-1 do
      Checked[FCurrentChecked[Cnt]]:=TRUE;
  end;

Function TMenuFunctions.GetGroupEnabled(AGroup:Integer):Boolean;
begin
  Result:=FGroupEnables[AGroup]
end;

Function TMenuFunctions.GetGroupVisible
   (
   AGroup          : Integer
   )
   : Boolean;
  begin
    Result:=FGroupVisibles[AGroup]
  end;

Procedure TMenuFunctions.RegisterMenuFunctions(AMenuFunctions:TMenuFunctions);
var AMenuFunction  : TComponent;
    BMenuFunction  : TComponent;
    Index          : Integer;
begin
  while AMenuFunctions.ComponentCount>0 do begin
    AMenuFunction:=AMenuFunctions.Components[0];
    AMenuFunctions.RemoveComponent(AMenuFunction);
    // look if there is an alternative handler registered for this menu-function
    Index:=FHandlers.IndexOf(AMenuFunction.Name);
    if Index>-1 then begin
      BMenuFunction:=TMenuFunctionClass(FHandlers.Objects[Index]).Create(Self);
      BMenuFunction.Assign(AMenuFunction);
      BMenuFunction.Name:=AMenuFunction.Name;
      AMenuFunction.Free;
    end
    else InsertComponent(AMenuFunction);
  end;
end;

Procedure TMenuFunctions.RegisterFromResource(HInst:Integer;Const ResourceName:String;
   Const HintSection    : String);
var AMenuFunctions    : TMenuFunctions;
begin
  try
    {$IFDEF WIN32}
    AMenuFunctions:=TMenuFunctions(ReadComponentResEx(HInst,ResourceName));
    try
    {$ELSE}
    AMenuFunctions:=TMenuFunctions.Create(NIL);
    try
      ReadComponentRes(ResourceName,AMenuFunctions);
    {$ENDIF}
      AMenuFunctions.HintSection:=HintSection;
      RegisterMenuFunctions(AMenuFunctions);
    finally
      AMenuFunctions.Free;         
    end;
  except
    on EResNotFound do
    else Raise;
  end;
end;                 

Procedure TMenuFunctions.SetHintSection(Const AHintSection:String);
var Cnt               : Integer;
begin
  for Cnt:=0 to ComponentCount-1 do TMenuFunction(Components[Cnt]).HintSection:=AHintSection;
end;
                                                        
Procedure TMenuFunctions.OnLanguageChanged;
var Cnt            : Integer;
    MenuFunction   : TMenuFunction;
begin
  for Cnt:=0 to ComponentCount-1 do begin
    MenuFunction:=TMenuFunction(Components[Cnt]);
    // update hint-text
    if (MenuFunction.HintSection<>'') and (MenuFunction.HintNumber>0) then
        MenuFunction.Hint:=MlgStringList[MenuFunction.HintSection,MenuFunction.HintNumber]
    else MenuFunction.Hint:='';
  end;
end;

Procedure TMenuFunctions.SetAction(Control:TControl;Const AFunctionName:String);
var OldCaption     : String;
    Info           : PPropInfo;
begin
  { read old caption using RTTI }
  Info:=GetPropInfo(Control.ClassInfo,'Caption');
  if Info<>NIL then OldCaption:=GetStrProp(Control,Info)
  else OldCaption:='';
  Control.Action:=Functions[AFunctionName];
  { assigning an action resets the caption, set old caption again if it was not blank }
  if OldCaption<>'' then SetStrProp(Control,Info,OldCaption);
end;

Procedure TMenuFunctions.SetAction(Control:TMenuItem;Action:TCustomAction);
begin
  SetAction(Control,Action.Name);
end;

Procedure TMenuFunctions.SetAction(Control:TMenuItem;Const AFunctionName:String);
var OldCaption     : String;
    Info           : PPropInfo;
begin
  { read old caption using RTTI }
  Info:=GetPropInfo(Control.ClassInfo,'Caption');
  if Info<>NIL then OldCaption:=GetStrProp(Control,Info)
  else OldCaption:='';
  Control.Action:=Functions[AFunctionName];
  { assigning an action resets the caption, set old caption again if it was not blank }
  if OldCaption<>'' then SetStrProp(Control,Info,OldCaption);
end;

Procedure TMenuFunctions.ReadPrivateData(Reader:TReader);
begin
  FVersion:=Reader.ReadInteger;
end;

Procedure TMenuFunctions.WritePrivateData(Writer:TWriter);
begin
  Writer.WriteInteger(CurrentVersion);
end;

Procedure TMenuFunctions.DefineProperties(Filer:TFiler);
begin                                      
  Filer.DefineProperty('Data',ReadPrivateData,WritePrivateData,TRUE);
end;

Procedure TMenuFunctions.GetChildren(Proc:TGetChildProc;Root:TComponent);
var
  I: Integer;
  OwnedComponent: TComponent;
begin
  inherited GetChildren(Proc, Root);
  if Root = Self then
    for I := 0 to ComponentCount - 1 do
    begin
      OwnedComponent := Components[I];
      if not OwnedComponent.HasParent then Proc(OwnedComponent);
    end;
end;

Procedure TMenuFunctions.Notification(AComponent:TComponent;Operation:TOperation);
begin
  inherited Notification(AComponent,Operation);
{  if (Operation=opInsert) and (AComponent is TMenuFunction) then
      with AComponent as TMenuFunction do begin
    Enabled:=FGroupEnables[EnableGroup];
    Visible:=FGroupVisibles[VisibleGroup];
  end;}
end;

Function TMenuFunctions.GetVisibleGroupFunction(AGroup:Integer):TMenuFunction;
var Cnt            : Integer;
    MenuFunction   : TMenuFunction;
    SearchFor      : String;
begin
  SearchFor:='TempVisibleGroup'+IntToStr(AGroup);
  for Cnt:=0 to ComponentCount-1 do begin
    MenuFunction:=TMenuFunction(Components[Cnt]);
    if MenuFunction.Name=SearchFor then begin
      Result:=MenuFunction;
      Exit;
    end;
  end;
  Result:=TMenuFunction.Create(Self);
  Result.Name:=SearchFor;
  Result.VisibleGroup:=AGroup;
end;

Procedure TMenuFunctions.UpdateContextToolbars;
var Cnt            : Integer;
begin
  for Cnt:=0 to FCurrentChecked.Count-1 do UserInterface.AddContextToolbar(
      TMenuFunction(FCurrentChecked.Objects[Cnt]).ContextToolbar);
end;

Procedure TMenuFunctions.RegisterAlternativeHandler(const FunctionName:String;
  HandlerClass: TMenuFunctionClass);
var Index          : Integer;
begin
  Index:=FHandlers.IndexOf(FunctionName);
  if Index>-1 then FHandlers.Delete(Index);
  FHandlers.AddObject(FunctionName,Pointer(HandlerClass));  
end;

{==============================================================================+
  TShortCutHash                                                                
+==============================================================================}

Function TShortCutHash.KeyOf
   (
   Item            : Pointer
   )
   : Pointer;
  begin
    Result:=@TShortCutItem(Item).ShortCut;
  end;

Function TShortCutHash.OnCompareItems
   (
   Item1           : Pointer;
   Item2           : Pointer
   )
   : Boolean;
  begin
    Result:=PWord(Item1)^=PWord(Item2)^;
  end;

Function TShortCutHash.OnGetHashValue
   (
   Item            : Pointer
   )
   : LongInt;
  begin
    Result:=PWord(Item)^;
  end;

{==============================================================================}
{ TShortCutItem                                                                }
{==============================================================================}

Constructor TShortCutItem.Create
   (
   Const AFunctionName  : String;
   AShortCut            : TShortCut
   );
  begin
    inherited Create;
    ShortCut:=AShortCut;
    FunctionName:=AFunctionName;
  end;

Function TShortCutItem.GetFunctionName
   : String;
  begin
    Result:=FFunction.Name;
  end;

Function TShortCutItem.GetShortCutText
   : String;
  begin
    Result:=ShortCutToText(ShortCut);
  end;

Procedure TShortCutItem.SetFunctionName
   (
   Const AName     : String
   );
  begin
    FFunction:=MenuFunctions[AName];
    //FFunction.ShortCut:=ShortCut;
  end;

{==============================================================================+
  Initialisierungs- und Terminierungscode
+==============================================================================}

Initialization
  Classes.RegisterClass(TMenuFunctions);
  Classes.RegisterClass(TMenuFunction);
  MenuFunctionImages:=TImageList.CreateSize(16,16);
  MenuFunctions:=TMenuFunctions.Create(NIL);
  DefMenuFunction:=TMenuFunction.Create(MenuFunctions);
  DefMenuFunction.VisibleGroup:=MaxEnableGroup;

Finalization
  MenuFunctions.Free;
  MenuFunctionImages.Free;

end.
