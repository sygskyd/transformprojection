Unit RollUp;

Interface

Uses WinProcs,WinTypes,Classes,Controls,ExtCtrls,Forms,Graphics,Messages,WCtrls,
     UIntfCtrls;

Type TCaptionStyle = (csDefault,csFont,csNone);

     TCaptionIcon  = (ciNone,ciClose,ciMinMax,ciHelp);
     TCaptionIcons = Set of TCaptionIcon;

     TRollup       = Class(TComponent)
      Private
       FActive          : Boolean;
       FCaptionIcons    : TCaptionIcons;
       FCaptionStyle    : TCaptionStyle;
       FFont            : TFont;
       FMenuFunction    : String;
       Procedure   DoFontChange(Sender:TObject);
       Function    GetFrameWidth:Integer;
       Procedure   SetActive(AActive:Boolean);
       Function    StoreFont:Boolean;
      Public
       Property    FrameWidth:Integer read GetFrameWidth;
      Published
       Constructor Create(AOwner:TComponent); override;
       Destructor  Destroy; override;
       Property    Active:Boolean read FActive write SetActive default TRUE;
       Property    ShowHideMenuFunction:String read FMenuFunction write FMenuFunction;
       Property    CaptionStyle:TCaptionStyle read FCaptionStyle write FCaptionStyle default csDefault;
       Property    CaptionIcons:TCaptionIcons read FCaptionIcons write FCaptionIcons;
       Property    Font:TFont read FFont write FFont stored StoreFont;
     end;

     TRollupFormClass    = Class of TRollupForm;

     TRollupForm         = Class(TWForm)
      Private
       FRollup          : TRollup;
       FRollupWindow    : TUIToolWindow;
       Procedure   CMShowingChanged(var Msg:TMessage); message cm_ShowingChanged;
       Function    GetRollup:TRollup;
       Function    GetRollupWindow:TUIToolWindow;
      Public
       Property    Rollup:TRollup read GetRollup;
       Property    RollupToolWindow:TUIToolWindow read GetRollupWindow;
     end;

Implementation

{$IFDEF WIN32}
{$R *.R32}
{$ELSE}
{$R *.RES}
{$ENDIF}

Uses SysUtils,UserIntf;                

{===============================================================================
| TRollup
+==============================================================================}

Constructor TRollup.Create
   (
   AOwner          : TComponent
   );
  begin
    inherited Create(AOwner);
    FActive:=TRUE;
    FFont:=TFont.Create;
    with FFont do OnChange:=DoFontChange;
  end;

Destructor TRollup.Destroy;
begin
  FFont.Free;
  inherited Destroy;
end;

Procedure TRollup.DoFontChange
   (
   Sender          : TObject
   );
  begin
    CaptionStyle:=csFont;
  end;

Function TRollup.StoreFont
   : Boolean;
  begin
    Result:=FCaptionStyle<>csDefault;
  end;

Function TRollup.GetFrameWidth
   : Integer;
  begin
    Result:=4;
  end;

Procedure TRollup.SetActive
   (
   AActive         : Boolean
   );
  begin
    if AActive<>FActive then begin
      FActive:=AActive;
    end;
  end;

Function TRollupForm.GetRollup
   : TRollup;
  var Cnt          : Integer;
  begin
    if FRollup=NIL then for Cnt:=0 to ComponentCount-1 do if Components[Cnt] is TRollup then begin
      FRollup:=TRollup(Components[Cnt]);
      Break;
    end;
    Result:=FRollup;
  end;

Function TRollupForm.GetRollupWindow:TUIToolWindow;
var Cnt            : Integer;
begin
  if FRollupWindow<>NIL then Result:=FRollupWindow
  else begin
    FRollupWindow:=TUIToolWindow.Create(Owner);
    FRollupWindow.ClientAreaWidth:=ClientWidth;
    FRollupWindow.ClientAreaHeight:=ClientHeight;
    FRollupWindow.ShowHint:=TRUE;
    FRollupWindow.BeginUpdate;
    for Cnt:=ComponentCount-1 downto 0 do if Components[Cnt] is TControl
        and (TControl(Components[Cnt]).Parent=Self) then
      TControl(Components[Cnt]).Parent:=FRollupWindow;
    FRollupWindow.Caption:=Caption;
    FRollupWindow.Name:=Name;
    FRollupWindow.EndUpdate;
    FRollupWindow.OnResize:=OnResize;
    FRollupWindow.OnCanResize:=OnCanResize;
    FRollupWindow.OnShow:=OnShow;
    FRollupWindow.OnHide:=OnHide;
    FRollupWindow.HelpContext:=HelpContext;
    FRollupWindow.CloseButtonWhenDocked:=TRUE;
    Result:=FRollupWindow;
  end;
end;

Procedure TRollupForm.CMShowingChanged(var Msg:TMessage);
begin
  inherited;
  if FRollupWindow<>NIL then FRollupWindow.Visible:=Visible;
  UserInterface.Update([uiMenus]);
end;

end.
