Unit InplEdit;

Interface

Uses Classes,Messages,StdCtrls,Validate,WCtrls,WinProcs,WinTypes;

Type TOnEditingEvent    = Procedure(Sender:TObject;Index:Integer;var AllowEdit:Boolean;var Text:String) of Object;
     TOnEditedEvent     = Procedure(Sender:TObject;Index:Integer;Const Text:String) of Object;
     TOnValidate        = Procedure(Sender:TObject;Index:Integer;Const Text:String;var Valid:Boolean) of Object;
                                           
     TInplaceEditor = Class(TComponent)
      Private
       FActive          : Boolean;
       FAutoEdit        : Boolean;
       FEdit            : TEdit;
       FEditLeft        : Integer;
       FEditRight       : Integer;
       FHooked          : Boolean;
       FHookProc        : Pointer;
       FIgnoreCursor    : Boolean;
       FItemIndex       : Integer;
       FOnEditing       : TOnEditingEvent;
       FOnEdited        : TOnEditedEvent;
       FOnValidate      : TOnValidate;
       FOrgWndProc      : Pointer;
       FListbox         : TWCustomListbox;
       FValidator       : TCustomValidator;
       Function    GetCount:Integer;
       Function    GetInplaceEdit:TEdit;
       Function    GetItemAtPos(Const APos:TPoint;AExists:Boolean):Integer;
       Function    GetItemIndex:Integer;
       Function    GetSelCount:Integer;
      Protected
       Procedure   Loaded; override;
       Procedure   HookListbox;
       Procedure   HookProc(var Msg:TMessage);
       Procedure   UnHookListbox;
      Public
       Constructor Create(AOwner:TComponent); override;
       Destructor  Destroy; override;
       Property    Active:Boolean read FActive write FActive;
       Function    EditRect:TRect;
       Property    InplaceEdit:TEdit read GetInplaceEdit;
       Procedure   Rehook;
      Published
       Property    AutoEdit:Boolean read FAutoEdit write FAutoEdit default FALSE;
       Procedure   BeginEdit(Const AText:String);
       Property    EditLeft:Integer read FEditLeft write FEditLeft default 0;
       Property    EditRight:Integer read FEditRight write FEditRight default 0;
       Function    EndEdit(ASave:Boolean):Boolean;
       Property    IgnoreCursorKeys:Boolean read FIgnoreCursor write FIgnoreCursor;
       Property    Listbox:TWCustomListbox read FListbox write FListbox;
       Property    OnEditing:TOnEditingEvent read FOnEditing write FOnEditing;
       Property    OnEdited:TOnEditedEvent read FOnEdited write FOnEdited;
       Property    OnValidate:TOnValidate read FOnValidate write FOnValidate;
       Property    Validator:TCustomValidator read FValidator write FValidator;
     end;

Implementation

Uses Controls,Forms,NumTools,TypInfo;

Type TInplaceEdit  = Class(TEdit)
      Protected
       Procedure   CMWantSpecialKey(var Msg:TMessage); message cm_WantSpecialKey;
       Procedure   DoExit; override;
       Procedure   KeyDown(var Key:Word;Shift:TShiftState); override;
       Procedure   KeyPress(var Key:Char); override;
       Procedure   MouseDown(Button:TMouseButton;Shift:TShiftState;X,Y:Integer); override;
      Public
       Constructor Create(AOwner:TComponent); override;
     end;

{===============================================================================
| TInplaceEdit
+==============================================================================}

Constructor TInplaceEdit.Create(AOwner:TComponent);
begin
  inherited Create(AOwner);
  ControlStyle:=ControlStyle-[csCaptureMouse];
  BorderStyle:=bsSingle;
  ParentCtl3D:=FALSE;
  Ctl3D:=FALSE;
  Visible:=FALSE;
end;

Procedure TInplaceEdit.DoExit;
begin
  TInplaceEditor(Owner).EndEdit(TRUE);
end;

Procedure TInplaceEdit.KeyDown(var Key:Word;Shift:TShiftState);
var ClearKey     : Boolean;
begin
  with TInplaceEditor(Owner) do if FActive then begin
    ClearKey:=TRUE;
    if FIgnoreCursor then ClearKey:=FALSE
    else case Byte(Key) of
      vk_Up   : if EndEdit(True) then if GetItemIndex>0 then FListbox.ItemIndex:=GetItemIndex-1;
      vk_Down : if EndEdit(True) then if GetItemIndex<GetCount-1 then FListbox.ItemIndex:=GetItemIndex+1;
      else ClearKey:=FALSE;
    end;
    if not ClearKey then inherited KeyDown(Key,Shift);
  end;
end;

Procedure TInplaceEdit.KeyPress(var Key:Char);
var ClearKey     : Boolean;
begin
  with TInplaceEditor(Owner) do if FActive then begin
    ClearKey:=TRUE;
    case Byte(Key) of
      vk_Return : EndEdit(True);
      vk_Escape : EndEdit(False);
      else ClearKey:=FALSE;
    end;
    if ClearKey then Key:=#0;
  end;
end;

Procedure TInplaceEdit.MouseDown(Button:TMouseButton;Shift:TShiftState;
   X,Y:Integer);
var APoint       : TPoint;
begin
  APoint:=Point(X,Y);
  if (Button<>mbLeft) or not ptInRect(ClientRect,APoint) then begin
    if TInplaceEditor(Owner).EndEdit(TRUE) then begin
      APoint:=ClientToScreen(APoint);
      with TInplaceEditor(Owner).Listbox do
        ItemIndex:=ItemAtPos(ScreenToClient(APoint),TRUE);
    end;
  end;
end;

Procedure TInplaceEdit.CMWantSpecialKey(var Msg:TMessage);
begin
  Msg.Result:=1;
end;

{===============================================================================
| TInplaceEditor
+==============================================================================}

Constructor TInplaceEditor.Create(AOwner:TComponent);
begin
  inherited Create(AOwner);
  FHookProc:=MakeObjectInstance(HookProc);
end;

Destructor TInplaceEditor.Destroy;
begin
  UnHookListbox;
  FreeObjectInstance(FHookProc);
  inherited Destroy;
end;

Procedure TInplaceEditor.UnHookListbox;
begin
  if FHooked and (FListbox.HandleAllocated) then begin
    SetWindowLong(FListbox.Handle,gwl_WndProc,LongInt(FOrgWndProc));
    FHooked:=FALSE;
  end;
end;

Procedure TInplaceEditor.HookListbox;
begin
  if not (csDesigning in ComponentState) and not (csDestroying in FListbox.ComponentState)
      and Assigned(FListbox) and not FHooked then begin
    LongInt(FOrgWndProc):=GetWindowLong(FListbox.Handle,gwl_WndProc);
    SetWindowLong(FListbox.Handle,gwl_WndProc,LongInt(FHookProc));
    FHooked:=TRUE;
  end;
end;

Procedure TInplaceEditor.HookProc(var Msg:TMessage);
var DoInherited  : Boolean;
begin
  with Msg do begin
    DoInherited:=TRUE;
    case Msg of
      wm_LButtonDown : begin
          KillTimer(FListbox.Handle,$ABCD);
          if FActive then EndEdit(True)
          else if (GetKeyState(vk_Shift)>=0) and (GetKeyState(vk_Control)>=0)
                and (GetSelCount=1) and ptInRect(EditRect,Point(lParamLo,lParamHi))
                and (GetItemAtPos(Point(lParamLo,lParamHi),FALSE)=GetItemIndex) then
              SetTimer(FListbox.Handle,$ABCD,GetDoubleClickTime+1,NIL);
        end;
      cm_Drag,  
      wm_LButtonDblClk: KillTimer(FListbox.Handle,$ABCD);
      wm_Timer: if wParam=$ABCD then begin
          KillTimer(FListbox.Handle,$ABCD);
          if not FListBox.Dragging then BeginEdit('');
          DoInherited:=FALSE;
        end;
      cn_Char        : if (FAutoEdit) and (GetSelCount=1) then begin
          if Char(wParam)<' ' then BeginEdit('')
          else BeginEdit(Char(wParam));
        end;
      cn_KeyDown :  if (GetSelCount=1) and (wParam=vk_F2) then BeginEdit('');
      cm_RecreateWnd,
      wm_Destroy : UnhookListbox;
    end;
    if DoInherited then Result:=CallWindowProc(FOrgWndProc,
        FListbox.Handle,Msg,wParam,lParam);
    case Msg of
      cm_RecreateWnd : HookListbox;
    end;
  end;
end;

Procedure TInplaceEditor.Rehook;
begin
  UnhookListbox;
  HookListbox;
end;

Procedure TInplaceEditor.Loaded;
begin
  inherited Loaded;
  HookListbox;
end;

Function TInplaceEditor.EditRect:TRect;
var AHeight      : Integer;
begin
  with FListbox,Result do begin
    Left:=FEditLeft;
    if FEditRight=0 then Right:=ClientWidth
    else Right:=FEditRight;
    AHeight:=Canvas.TextHeight('Ay');
    AHeight:=(AHeight-ItemHeight) Div 2+2;
    Top:=(ItemIndex-TopIndex)*ItemHeight-AHeight+1;
    Bottom:=Top+ItemHeight+AHeight;
  end;  
end;

Procedure TInplaceEditor.BeginEdit(Const AText:String);
var Allowed      : Boolean;
    BText        : String;
    ARect        : TRect;
    BRect        : TRect;
begin
  if Assigned(FOnEditing) then begin
    Allowed:=TRUE;
    BText:='';
    FItemIndex:=GetItemIndex;
    OnEditing(Self,FItemIndex,Allowed,BText);
    if AText<>'' then BText:=AText;
    if Allowed then begin
      { setup and show edit-control }
      with InplaceEdit do begin
        { calculate dimensions of edit-control }
        ARect:=EditRect;
        InflateRect(ARect,-3,0);
        with ARect do BRect:=Rect(0,0,Right-Left,Bottom-Top);
        SendMessage(Handle,EM_SETRECTNP,0,LongInt(@BRect));
        BoundsRect:=ARect;
        Text:=BText;
        Show;
        SetFocus;
        if AText<>'' then SelLength:=0;
        SelStart:=Length(BText);
        Update;
        SetCapture(Handle);
      end;
      FActive:=TRUE;
    end;
  end;
end;

Function TInplaceEditor.GetInplaceEdit:TEdit;
begin
  if FEdit=NIL then begin
    FEdit:=TInplaceEdit.Create(Self);
    FEdit.Parent:=FListbox;
    if FValidator<>NIL then FValidator.Edit:=FEdit;
  end;
  Result:=FEdit;
end;

Function TInplaceEditor.EndEdit(ASave:Boolean):Boolean;
var DoFocus      : Boolean;
    IsValid      : Boolean;
begin
  if FActive then begin
    if ASave then begin
      if FValidator<>NIL then IsValid:=FValidator.ValidateInput
      else IsValid:=TRUE;
      if IsValid and Assigned(FOnValidate) then
          OnValidate(Self,FItemIndex,InplaceEdit.Text,IsValid);
    end
    else IsValid:=TRUE;
    if IsValid then begin
      FActive:=False;
      DoFocus:=InplaceEdit.Focused;
      InplaceEdit.Hide;
      if GetCapture=FEdit.Handle then ReleaseCapture;
      if DoFocus then FListbox.SetFocus;
      if ASave and Assigned(FOnEdited) then OnEdited(Self,FItemIndex,InplaceEdit.Text);
    end;
    Result:=IsValid;
  end
  else Result:=TRUE;
end;

Function TInplaceEditor.GetCount:Integer;
begin
  Result:=FListbox.Count;
end;

Function TInplaceEditor.GetItemIndex:Integer;
begin
  Result:=FListbox.ItemIndex;
end;

Function TInplaceEditor.GetSelCount:Integer;
begin
  if GetWindowLong(FListbox.Handle,GWL_STYLE) and lbs_MultipleSel<>0 then
      Result:=FListbox.SelCount
  else if FListbox.ItemIndex>=0 then Result:=1
  else Result:=-1;
end;

Function TInplaceEditor.GetItemAtPos(Const APos:TPoint;AExists:Boolean):Integer;
begin
  Result:=FListbox.ItemAtPos(APos,AExists);
end;

end.

