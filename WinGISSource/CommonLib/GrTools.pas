{******************************************************************************+
  Module GrTools  
--------------------------------------------------------------------------------
  Contains some geometric functions like line-intersections, containment
  in rectangles, circle-intersections etc. The functions al work with
  points of type TGrPoint or rectangles of type TGrRect. GrRect and all it's
  functions are based on a carthesian coordinate-system with +x to the
  right and +y up. In difference to the windows-rectagle GrRect stores the
  point with the lower coordinates in LeftBottom and not in LeftTop.
--------------------------------------------------------------------------------
  Function GrPoint
  Function GrRect
  Function GrBounds
  Function RectWidth
  Function RectHeight
  Procedure InflateRect
  Function ScaleGrRect
  Function LineAngle
  Function LineIntersection
  Function VectorIntersection
  Function LineVectorIntersection
  Function LineNormalDistance
  Function LineNormalIntersection
  Function VectorNormalIntersection
  Function GrPointDistance
  Procedure MoveGrPoint
  Procedure MoveGrPointAngle
  Procedure MoveGrRect
  Function RectsEqual
  Function GrPointsEqual
  Function GrPointInRect
  Function PointInsidePolygon
  Procedure InsertPointInRect
  Function CircleLineIntersection
  Function ClipLineToRect
  Function ClipVectorToRect
  Function RotateGrPoint
  Function RotateGrRect
  Function RotRect
  Function ISOFrame
  Function ISOWidth
  Function ISOHeight
  Function FitRectangle
  Procedure Normalize
  Function Normalized
--------------------------------------------------------------------------------
  Functions marked with NYT are Not Yet Tested
+******************************************************************************}
Unit GrTools;

Interface

Uses Windows;                         

Type PGrPoint      = ^TGrPoint;
     TGrPoint      = Record
       X           : Double;
       Y           : Double;
     end;
     
     PGrPointArray = ^TGrPointArray;
     TGrPointArray = Array[0..0] of TGrPoint;

     PGrRect       = ^TGrRect;
     TGrRect       = Record
       case Integer of
         0: (Left, Bottom, Right, Top : Double);
         1: (BottomLeft, TopRight: TGrPoint);
     end;

     PRotRect      = ^TRotRect;
     TRotRect      = Record
       Width       : Double;
       Height      : Double;
       Rotation    : Double;
       case Integer of
         0: (X,  Y : Double);
         1: (Origin: TGrPoint);
     end;

     TGrRectCorners = Array[0..3] of TGrPoint;

     TGrPointList  = Class
      Private
       FCapacity        : LongInt;
       FCount           : LongInt;
       FItemPtr         : PGrPointArray;
       Procedure   SetCapacity(ACapacity:LongInt);
       Function    GetItem(AIndex:LongInt):TGrPoint;
       Function    GetItemPtr(AIndex:LongInt):PGrPoint;
       Procedure   SetItem(AIndex:LongInt;const Item:TGrPoint);
      Public
       Destructor  Destroy; override;
       Procedure   Add(const Item:TGrPoint);
       Procedure   Assign(const Source:TGrPointList);
       Property    Capacity:LongInt read FCapacity write SetCapacity;
       Procedure   Clear;
       Property    Count:LongInt read FCount;
       Function    Extent:TGrRect;
       Property    Points[AIndex:LongInt]:TGrPoint read GetItem write SetItem; default;
       Property    PointPtrs[AIndex:LongInt]:PGrPoint read GetItemPtr;
       Property    List:PGrPointArray read FItemPtr;
     end;

{******************************************************************************+
  Function GrPoint
--------------------------------------------------------------------------------
  constructs a TGRPoint from x- and y-coordinate.
+******************************************************************************}
Function GrPoint(const X,Y:Double):TGrPoint;

{******************************************************************************+
  Function GrRect
--------------------------------------------------------------------------------
  constructs a TGrRect from x- and y-coordinates of the upperleft and lowerright
  corner of the rectange
+******************************************************************************}
Function GrRect(const Left,Bottom,Right,Top:Double):TGrRect; overload;
Function GrRect(const Rect:TRect):TGrRect; overload;

{******************************************************************************+
  Function RectWidth
--------------------------------------------------------------------------------
  Calculates the width of the rectangle, the result may be negative if right
  is smaller than left.
+******************************************************************************}
Function RectWidth(const ARect:TGrRect):Double; overload;
Function RectWidth(const ARect:TRotRect):Double; overload;

{******************************************************************************+
  Function RectHeight
--------------------------------------------------------------------------------
  Calculates the height of the rectangle, the result may be negative if top is
  smaller than bottom.
+******************************************************************************}
Function RectHeight(const ARect:TGrRect):Double; overload;
Function RectHeight(const ARect:TRotRect):Double; overload;

{******************************************************************************+
  Procedure InflateRect
--------------------------------------------------------------------------------
  Grows or shrinks a rectangle symmetrically. X and Y are subtracted from left
  and bottom and are added to right and top.
+******************************************************************************}
Procedure InflateGrRect(var ARect:TGrRect;const DX,DY:Double);
Procedure InflateRotRect(var ARect:TRotRect;const DX,DY:Double);
Function InflatedRect(Const ARect:TGrRect;const DX,DY:Double):TGrRect; overload;
Function InflatedRect(Const ARect:TRotRect;const DX,DY:Double):TRotRect; overload;

{******************************************************************************+
  Function RectCenter
--------------------------------------------------------------------------------
  Returnes the coordinates of the center-point of the rectangle.
+******************************************************************************}
Function RectCenter(const Rect:TGrRect):TGrPoint; overload;
Function RectCenter(const Rect:TRotRect):TGrPoint; overload;

{******************************************************************************+
  Function ScaleGrRect
--------------------------------------------------------------------------------
  Scales the specified rectangle.
+******************************************************************************}
Procedure ScaleRect(var ARect:TGrRect;const XScale,YScale:Double); overload;
Procedure ScaleRect(var ARect:TRotRect;const XScale,YScale:Double); overload;

{******************************************************************************+
  Function ScaledGrRect
--------------------------------------------------------------------------------
  Returnes a scaled version of ARect.
+******************************************************************************}
Function ScaledRect(const ARect:TGrRect;const XScale,YScale:Double):TGrRect; overload;
Function ScaledRect(const ARect:TRotRect;const XScale,YScale:Double):TRotRect; overload;

{******************************************************************************+
  Function GrBounds
--------------------------------------------------------------------------------
  constructs a TGrRect from x- and y-coordinates of the upperleft corner and
  the width and height of the rectangle
+******************************************************************************}
Function GrBounds(const Left,Bottom,Width,Height:Double):TGrRect;

{******************************************************************************+
  Function LineAngle
--------------------------------------------------------------------------------
  Calculates the angle between the line and the x-axis.
+******************************************************************************}
Function LineAngle(const Point1,Point2:TGrPoint):Double;

{******************************************************************************+
  Function LineIntersection
--------------------------------------------------------------------------------
  Calculates the intersection-points between the two lines.
+******************************************************************************}
Function LineIntersection(const L1P1,L1P2,L2P1,L2P2:TGrPoint;var Cut1,Cut2:TGrPoint):Integer;

{******************************************************************************+
  Function VectorIntersection
--------------------------------------------------------------------------------
  Calculates the intersection-points between the two vectors. The vector needs
  not to be normalized.
+******************************************************************************}
Function VectorIntersection(const L1P1,Vect1,L2P1,Vect2:TGrPoint;var Cut1:TGrPoint):Integer;

{******************************************************************************+
  Function LineVectorIntersection
--------------------------------------------------------------------------------
  Calculates the intersection-points between the two lines. The vector needs
  not to be normalized.
+******************************************************************************}
Function LineVectorIntersection(const LP1,LP2,Point,Vector:TGrPoint;var Cut1,Cut2:TGrPoint):Integer;

{******************************************************************************+
  Function LineNormalDistance
--------------------------------------------------------------------------------
  Calculates the normal-distance of the point to the line.
+******************************************************************************}
Function LineNormalDistance(const Point1,Point2,Point:TGrPoint):Double;

{******************************************************************************+
  Function VectorNormalDistance
--------------------------------------------------------------------------------
  Calculates the normal-distance of the point to the vector.
+******************************************************************************}
Function VectorNormalDistance(const Point1,Vector:TGrPoint;const Point:TGrPoint):Double;

{******************************************************************************+
  Function LineNormalIntersection
--------------------------------------------------------------------------------
  Calculates the cutting-point of the line and the normal to the line through
  the specified point. The function-result gives the normal distance of the
  point to the line.
+******************************************************************************}
Function LineNormalIntersection(const Point1,Point2,Point:TGrPoint;var CutPoint:TGrPoint):Double; overload;

{******************************************************************************+
  Function LineNormalIntersection
--------------------------------------------------------------------------------
  Calculates the cutting-point of the line and the normal to the line through
  the specified point. The function-result gives the normal distance of the
  point to the line. PointOnLine returns true if the intersection-point
  lies on the line.
+******************************************************************************}
Function LineNormalIntersection(const Point1,Point2,Point:TGrPoint;var CutPoint:TGrPoint;var PointOnLine:Boolean):Double; overload;
                 
{******************************************************************************+
  Function VectorNormalIntersection
--------------------------------------------------------------------------------
  Calculates the cutting-point of the line and the normal to the line through
  the specified point. The function-result gives the normal distance of the
  point to the line.
+******************************************************************************}
Function VectorNormalIntersection(const Point1,Vector:TGrPoint;const Point:TGrPoint;var CutPoint:TGrPoint):Double;

{******************************************************************************+
  Function GrPointDistance
--------------------------------------------------------------------------------
  Calculates the distance between the two points.
+******************************************************************************}
Function GrPointDistance(const Point1,Point2:TGrPoint):Double;

{******************************************************************************+
  Function MovePoint
--------------------------------------------------------------------------------
  Moves the point by the specified distance in x- and y-direction.
+******************************************************************************}
Procedure MovePoint(var Point:TGrPoint;const DeltaX,DeltaY:Double); overload;

{******************************************************************************+
  Function MovedPoint
--------------------------------------------------------------------------------
  Returns the specified point moved by the offsets
+******************************************************************************}
Function MovedPoint(const Point:TGrPoint;const DeltaX,DeltaY:Double):TGrPoint; overload;

{******************************************************************************+
  Function RotatedPoint
--------------------------------------------------------------------------------
  Rotates the point around the coordinate-system-origin by the specified
  angle.
+******************************************************************************}
Function RotatedPoint(const Point:TGrPoint;const Angle:Double):TGrPoint; overload;
Function RotatedPoint(const Point:TGrPoint;const SinAngle,CosAngle:Double):TGrPoint; overload;

{******************************************************************************+
  Function RotatedPoint
--------------------------------------------------------------------------------
  Rotates the point around the coordinate-system-origin by the specified
  angle.
+******************************************************************************}
Procedure RotatePoint(var Point:TGrPoint;const Angle:Double); overload;
Procedure RotatePoint(var Point:TGrPoint;const SinAngle,CosAngle:Double); overload;

{******************************************************************************+
  Function RotatedRect
--------------------------------------------------------------------------------
  Rotates the rect around the coordinate-system-origin by the sprcified
  angle.
+******************************************************************************}
Function RotatedRect(const Rect:TGrRect;const Angle:Double):TGrRect; overload;
Function RotatedRect(const Rect:TGrRect;const SinAngle,CosAngle:Double):TGrRect; overload;
Function RotatedRect(const Rect:TRotRect;const Angle:Double):TRotRect; overload;

{******************************************************************************+
  Function RotatedRect
--------------------------------------------------------------------------------
  Rotates the rect around the coordinate-system-origin by the sprcified
  angle.
+******************************************************************************}
Procedure RotateRect(var Rect:TGrRect;const Angle:Double); overload;
Procedure RotateRect(var Rect:TGrRect;const SinAngle,CosAngle:Double); overload;
Procedure RotateRect(var Rect:TRotRect;const Angle:Double); overload;

{******************************************************************************+
  Function MovePointAngle
--------------------------------------------------------------------------------
  Moves the point by the specified distance and the specified angle.
+******************************************************************************}
Procedure MovePointAngle(var Point:TGrPoint;const Delta,Angle:Double);
Function MovedPointAngle(const Point:TGrPoint;const Delta,Angle:Double):TGrPoint;

{******************************************************************************+
  Function MoveRect
--------------------------------------------------------------------------------
  Moves the point by the specified distance in x- and y-direction.
+******************************************************************************}
Procedure MoveRect(var Rect:TGrRect;const DeltaX,DeltaY:Double); overload;
Procedure MoveRect(var Rect:TRotRect;const DeltaX,DeltaY:Double); overload;

{******************************************************************************+
  Function RectsEqual
--------------------------------------------------------------------------------
  Determines, if the two rectangles are equal.
+******************************************************************************}
Function RectsEqual(const ARect,BRect:TGrRect):Boolean; overload;
Function RectsEqual(const ARect,BRect:TRotRect):Boolean; overload;

{******************************************************************************+
  Function RectEmpty
--------------------------------------------------------------------------------
  Determines if the specified rectangle is empty.
+******************************************************************************}
Function RectEmpty(const Rect:TGrRect):Boolean; overload;
Function RectEmpty(const Rect:TRotRect):Boolean; overload;

{******************************************************************************+
  Function GrPointsEqual
--------------------------------------------------------------------------------
  Determines, if the two points are equal.
+******************************************************************************}
Function GrPointsEqual(const APoint,BPoint:TGrPoint):Boolean;

{******************************************************************************+
  Function GrPointInRect
--------------------------------------------------------------------------------
  Determines if a point lies inside a rectangle.
+******************************************************************************}
Function GrPointInRect(const Point:TGrPoint;const Rect:TGrRect):Boolean;

{******************************************************************************+
  Function PointInsidePolygon
--------------------------------------------------------------------------------
  Determines if a point lies inside a polygon.
+******************************************************************************}
Function PointInsidePolygon(const Point:TGrPoint;const Polygon:Array of TGrPoint;
    PointCount:Integer):Boolean;

{******************************************************************************+
  Procedure InsertPointInRect
--------------------------------------------------------------------------------
  Increases the size of the rectangle so that the point lies in it.
+******************************************************************************}
Procedure InsertPointInRect(APoint:TGrPoint;var ARect:TGrRect); overload;

{******************************************************************************+
  Function CircleLineIntersection
--------------------------------------------------------------------------------
  Calculates the intersection between a circle and a line.
+******************************************************************************}
Function CircleLineIntersection(const ACenter:TGrPoint;
    const ARadius:Double;const P1,P2:TGrPoint;var Cut1,Cut2:TGrPoint):Integer;

{******************************************************************************+
  Function CircleVectorIntersection
--------------------------------------------------------------------------------
  Calculates the intersection between a circle and a vector.
+******************************************************************************}
Function CircleVectorIntersection(const ACenter:TGrPoint;
    const ARadius:Double;const P1,P2:TGrPoint;var Cut1,Cut2:TGrPoint):Integer;

{******************************************************************************+
  Function ClipLineToRect
--------------------------------------------------------------------------------
  Clips the line specified by Point1 and Point2 to the rectangle. Returns
  false if the line lies completely outside the rectangle.
+******************************************************************************}
Function ClipLineToRect(var Point1:TGrPoint;var Point2:TGrPoint;const Rect:TGrRect):Boolean;

{******************************************************************************+
  Function ClipVectorToRect                                     
--------------------------------------------------------------------------------
  Clips the vector specified by Point and Angle to the rectangle. Returns
  false if the vector lies completely outside the rectangle. The vector needs
  not to be normalized.
+******************************************************************************}
Function ClipVectorToRect(const Point,Vector:TGrPoint;const Rect:TGrRect;
    var Point1:TGrPoint;var Point2:TGrPoint):Boolean;

{******************************************************************************+
  Function RotRect
--------------------------------------------------------------------------------
  constructs a TRotRect from X, Y, Width, Height and Angle.
+******************************************************************************}
Function RotRect(const AX,AY,AWidth,AHeight,ARotation:Double):TRotRect; overload;
Function RotRect(Const Rect:TGrRect;ARotation:Double):TRotRect; overload;

{******************************************************************************+
  Function RotRect
--------------------------------------------------------------------------------
  constructs a RotRect from two corners and the rotation of the rectangle.
+******************************************************************************}
Function RotRect(const Point1,Point2:TGrPoint;const Angle:Double):TRotRect; overload;
Function RotRect(const Position:TGrPoint;AWidth,AHeight,ARotation:Double):TRotRect; overload;

{******************************************************************************+
  Procedure RotRectCorners
--------------------------------------------------------------------------------
  Calculates the four corners of the rectangle.
+******************************************************************************}
Procedure RectCorners(const GrRect:TGrRect;var Corners:TGrRectCorners); overload;
Procedure RectCorners(const RotRect:TRotRect;var Corners:TGrRectCorners); overload;

{******************************************************************************+
  Function ISOFrame
--------------------------------------------------------------------------------
  Calculates the smallest ISO-oriented rectangle that contains the given
  rotated rectangle.
+******************************************************************************}
Function ISOFrame(const Rect:TRotRect):TGrRect;

{******************************************************************************+
  Function ISOFrameWidth
--------------------------------------------------------------------------------
  Calculates the width of the smallest ISO-oriented rectangle that contains
  the given rotated rectangle.
+******************************************************************************}
Function ISOFrameWidth(const Rect:TRotRect):Double;

{******************************************************************************+
  Function ISOFrameHeight
--------------------------------------------------------------------------------
  Calculates the height of the smallest ISO-oriented rectangle that contains
  the given rotated rectangle.
+******************************************************************************}
Function ISOFrameHeight(const Rect:TRotRect):Double;

{******************************************************************************+
  Function FitRectangle
--------------------------------------------------------------------------------
  Calculates a rotated rectangle with the specified angle that fits into the
  ISO-oriented rectangle.
+******************************************************************************}
Function FitRectangle(const Rect:TGrRect;const Angle:Double):TRotRect;

{******************************************************************************+
  Function FitISORectangle
--------------------------------------------------------------------------------
  Calculates an ISO-oriented rectangle that fits into the rotated rectangle.
+******************************************************************************}
Function FitISORectangle(const Rect:TRotRect):TGrRect;

{******************************************************************************+
  Procedure Normalize
--------------------------------------------------------------------------------
  Normalizes the specified vector. If the vector has zero-length nothing is done.
+******************************************************************************}
Procedure Normalize(var Vector:TGrPoint);
Function Normalized(const Vector:TGrPoint):TGrPoint;

Function Norm(const Vector:TGrPoint):Double;

Function ScalarProduct(const Vect1,Vect2:TGrPoint):Double;
Function CrossProduct(const Vect1,Vect2:TGrPoint):Double;

Function RectInside(Rect:TGrRect;InRect:TGrRect):Boolean; overload;

Function PointOnArc(const Point,Center,Start,Stop:TGrPoint):Boolean; overload;
Function PointOnArc(const Point,Center:TGrPoint;Start,Stop:Double):Boolean; overload;

Function ArcClipRect(const Center:TGrPoint;Const Radius:Double;Start,Stop:TGrPoint):TGrRect; overload;
Function ArcClipRect(const Center:TGrPoint;Radius,Start,Stop:Double):TGrRect; overload;
Function CircleClipRect(const Center:TGrPoint;Const Radius:Double):TGrRect; overload;

Implementation

Uses NumTools;

{******************************************************************************+
  Function GrPoint
--------------------------------------------------------------------------------
  constructs a TGRPoint from x- and y-coordinate.
+******************************************************************************}
Function GrPoint(const X,Y:Double):TGrPoint;
begin
  Result.X:=X; Result.Y:=Y;
end;

{******************************************************************************+
  Function GrRect
--------------------------------------------------------------------------------
  constructs a TGrRect from x- and y-coordinates of the upperleft and lowerright
  corner of the rectange
--------------------------------------------------------------------------------
  in Left, Top = upper-left corner of the rectangle
  in Right, Bottom = lower-right corner of the rectangle
+******************************************************************************}
Function GrRect(const Left,Bottom,Right,Top:Double):TGrRect;
begin
  Result.Left:=Left; Result.Top:=Top;
  Result.Right:=Right; Result.Bottom:=Bottom;
end;

{******************************************************************************+
  Function GrBounds
--------------------------------------------------------------------------------
  constructs a TGrRect from x- and y-coordinates of the lower-left corner and
  the width and height of the rectangle
--------------------------------------------------------------------------------
  in Left, Bottom = lower-left corner of the rectangle
  in Width, Height = width and height of the rectangle
+******************************************************************************}
Function GrBounds(const Left,Bottom,Width,Height:Double):TGrRect;
begin
  Result.Left:=Left;  Result.Bottom:=Bottom;
  Result.Right:=Result.Left+Width;
  Result.Top:=Result.Bottom+Height;
end;

{******************************************************************************+
  Function LineIntersection
--------------------------------------------------------------------------------
  Ermittelt die Schnittpunkte der Linien L1P1->L1P2 und L2P1->L2P2. Der
  Algorithmus stammt aus Hill Computer Graphics (Seite 131)
--------------------------------------------------------------------------------
  Parameter
  in L1P1 = Anfangspunkt erste Linie
  in L1P2 = Endpunkt erste Linie
  in L2P1 = Anfangspunkt zweite Linie
  in L2P2 = Endpunkt zweite Linie
  out Cut1 = Koordinaten des 1. Schnittpunkts
  out Cut2 = Koordinaten des 2. Schnittpunkts
--------------------------------------------------------------------------------
  Funktionswert
  Gibt die Anzahl der Schnittpunkte an (0 bis 2).
+******************************************************************************}
Function LineIntersection
   (
   const L1P1      : TGrPoint;
   const L1P2      : TGrPoint;
   const L2P1      : TGrPoint;
   const L2P2      : TGrPoint;
   var Cut1        : TGrPoint;
   var Cut2        : TGrPoint
   )
   : Integer;
  var D            : Double;
      DX1          : Double;
      DY1          : Double;
      DX2          : Double;
      DY2          : Double;
      t0           : Double;
      u0           : Double;
      tc           : Double;
      td           : Double;
  begin
    Result:=0;
    DX1:=L1P2.X-L1P1.X;
    DX2:=L2P2.X-L2P1.X;
    DY1:=L1P2.Y-L1P1.Y;
    DY2:=L2P2.Y-L2P1.Y;
    D:=DX1*DY2-DY1*DX2;
    if Abs(D)>=Tolerance then begin
      t0:=((L2P1.X-L1P1.X)*DY2-(L2P1.Y-L1P1.Y)*DX2)/D;
      if (t0>-Tolerance) and (t0<1.0+Tolerance) then begin
        if Abs(DX2)>Abs(DY2) then u0:=((L1P1.X-L2P1.X)+DX1*t0)/DX2
        else u0:=((L1P1.Y-L2P1.Y)+DY1*t0)/DY2;
        if (u0>-Tolerance) and (u0<1.0+Tolerance) then begin
          Cut1:=GrPoint(L1P1.X+DX1*t0,L1P1.Y+DY1*t0);
          Result:=1;
        end;
      end;
    end
    else if Abs(DX1*(L2P1.Y-L1P1.Y)-DY1*(L2P1.X-L1P1.X))<Tolerance then begin
      if Abs(DX1)>Abs(DY1) then begin
        tc:=(L2P1.X-L1P1.X)/DX1;
        td:=(L2P2.X-L1P1.X)/DX1;
      end
      else if Abs(DY1)<Tolerance then Exit
      else begin
        tc:=(L2P1.Y-L1P1.Y)/DY1;
        td:=(L2P2.Y-L1P1.Y)/DY1;
      end;
      if ((tc>-Tolerance) and (td<1.0+Tolerance))
          or ((tc<1.0+Tolerance) and (td>-Tolerance)) then begin
        if tc<0.0 then tc:=0.0
        else if tc>1.0 then tc:=1.0;
        if td<0.0 then td:=0.0
        else if td>1.0 then td:=1.0;
        Cut1:=GrPoint(L1P1.X+DX1*tc,L1P1.Y+DY1*tc);
        if Abs(tc-td)<Tolerance then Result:=1
        else begin
          Cut2:=GrPoint(L1P1.X+DX1*td,L1P1.Y+DY1*td);
          Result:=2;
        end;
      end;
    end;
  end;

Function LineVectorIntersection(Const LP1,LP2,Point,Vector:TGrPoint;
  var Cut1,Cut2:TGrPoint):Integer;
var DX1          : Double;
    DY1          : Double;
    D            : Double;
    t0           : Double;
begin
  Assert(DblCompare(Norm(Vector),1)=0);
  Result:=0;
  DX1:=LP2.X-LP1.X;
  DY1:=LP2.Y-LP1.Y;
  D:=DX1*Vector.Y-DY1*Vector.X;
  if Abs(D)>=Tolerance then begin
    t0:=((Point.X-LP1.X)*Vector.Y-(Point.Y-LP1.Y)*Vector.X)/D;
    if (t0>-Tolerance) and (t0<1.0+Tolerance) then begin
      Cut1:=GrPoint(LP1.X+DX1*t0,LP1.Y+DY1*t0);
      Result:=1;
    end;
  end
  else if Abs(DX1*(Point.Y-LP1.Y)-DY1*(Point.X-LP1.X))<Tolerance then begin
    Cut1:=LP1;
    Cut2:=LP2;
    Result:=2;
  end;
end;

{******************************************************************************+
  Function VectorIntersection NYT
--------------------------------------------------------------------------------
  Ermittelt die Schnittpunkte der Linien L1P1->L1P2 und L2P1->L2P2. Der
  Algorithmus stammt aus Hill Computer Graphics (Seite 131)
--------------------------------------------------------------------------------
  in L1P1 = Anfangspunkt erste Linie
  in Vect1 = Vector1 (DX1, DY1)
  in L2P1 = Anfangspunkt zweite Linie
  in Vect2 = Vector2 (DX2, DY2)
  out Cut1 = Koordinaten des Schnittpunkts
--------------------------------------------------------------------------------
  Gibt die Anzahl der Schnittpunkte zur�ck (0 wenn Vektoren parallel, 1 wenn
  sich die Vektoren schneiden, 2 wenn Vektoren aufeinander liegen).
+******************************************************************************}
Function VectorIntersection
   (
   const L1P1      : TGrPoint;
   const Vect1     : TGrPoint;
   const L2P1      : TGrPoint;
   const Vect2     : TGrPoint;
   var Cut1        : TGrPoint
   )
   : Integer;
  var D            : Double;
      t0           : Double;
  begin
    Assert(DblCompare(Norm(Vect1),1)=0);
    Assert(DblCompare(Norm(Vect2),1)=0);
    Result:=0;
    D:=Vect1.X*Vect2.Y-Vect1.Y*Vect2.X;
    if Abs(D)>=Tolerance then begin
      t0:=((L2P1.X-L1P1.X)*Vect2.Y-(L2P1.Y-L1P1.Y)*Vect2.X)/D;
      Cut1:=GrPoint(L1P1.X+Vect1.X*t0,L1P1.Y+Vect1.Y*t0);
      Result:=1;
    end
    else if Abs(Vect1.X*(L2P1.Y-L1P1.Y)-Vect1.Y*(L2P1.X-L1P1.X))<Tolerance then
      Result:=2;
  end;

{******************************************************************************+
  Function LineNormalDistance
--------------------------------------------------------------------------------
  Calculates the normal-distance of the point to the line. The result if
  positive if the point lies counter-clockwise to the line, negative else.
+******************************************************************************}
Function LineNormalDistance
   (
   const Point1    : TGrPoint;
   const Point2    : TGrPoint;
   const Point     : TGrPoint
   )
   : Double;
  var DX1          : Double;
      DY1          : Double;
      Root         : Double;
  begin
    DX1:=Point2.X-Point1.X;
    DY1:=Point2.Y-Point1.Y;
    Root:=Sqrt(Sqr(DX1)+Sqr(DY1));
    if Root<Tolerance then Result:=1.7e+308
    else Result:=(-DY1*(Point.X-Point1.X)+DX1*(Point.Y-Point1.Y))/Root;
  end;

{******************************************************************************+
  Function VectorNormalDistance
--------------------------------------------------------------------------------
  Calculates the normal-distance of the point to the line. The result if
  positive if the point lies counter-clockwise to the line, negative else.
+******************************************************************************}
Function VectorNormalDistance
   (
   const Point1    : TGrPoint;
   const Vector    : TGrPoint;
   const Point     : TGrPoint
   )
   : Double;
  begin
    Assert(DblCompare(Norm(Vector),1)=0);
    Result:=(-Vector.Y*(Point.X-Point1.X)+Vector.X*(Point.Y-Point1.Y))
  end;

{******************************************************************************+
  Function LineNormalIntersection
--------------------------------------------------------------------------------
  Calculates the cutting-point of the line and the normal to the line through
  the specified point. The function-result gives the normal distance of the
  point to the line. PointOnLine returns true if the intersection-point
  lies on the line.
+******************************************************************************}
Function LineNormalIntersection(const Point1,Point2,Point:TGrPoint;
    var CutPoint:TGrPoint;var PointOnLine:Boolean):Double;
var DX1          : Double;
    DY1          : Double;
    Norm         : Double;
    T            : Double;
begin
  // calculate line-vector and normalize it
  DX1:=Point2.X-Point1.X;
  DY1:=Point2.Y-Point1.Y;
  Norm:=Sqrt(Sqr(DX1)+Sqr(DY1));
  if Norm<Tolerance then Result:=1.7E308
  else begin
    DX1:=DX1/Norm;
    DY1:=DY1/Norm;
    // calculate normal-distance
    Result:=(-DY1*(Point.X-Point1.X)+DX1*(Point.Y-Point1.Y));
    // calculate intersection-point
    CutPoint:=GrPoint(Point.X+Result*DY1,Point.Y-Result*DX1);
    // determine if intersection-point lies on the line
    if Abs(DX1)>Abs(DY1) then T:=(CutPoint.X-Point1.X)/DX1/Norm
    else T:=(CutPoint.Y-Point1.Y)/DY1/Norm;
    PointOnLine:=(T>-Tolerance) and (T<1.0+Tolerance);
  end;
end;

{******************************************************************************+
  Function LineNormalIntersection
--------------------------------------------------------------------------------
  Calculates the cutting-point of the line and the normal to the line through
  the specified point. The function-result gives the normal distance of the
  point to the line. PointOnLine returns true if the intersection-point
  lies on the line.
+******************************************************************************}
Function LineNormalIntersection(const Point1,Point2,Point:TGrPoint;
    var CutPoint:TGrPoint):Double;
var DX1          : Double;
    DY1          : Double;
    Norm         : Double;
begin
  // calculate line-vector and normalize it
  DX1:=Point2.X-Point1.X;
  DY1:=Point2.Y-Point1.Y;
  Norm:=Sqrt(Sqr(DX1)+Sqr(DY1));
  if Norm<Tolerance then Result:=1.7E308
  else begin
    DX1:=DX1/Norm;
    DY1:=DY1/Norm;
    // calculate normal-distance
    Result:=(-DY1*(Point.X-Point1.X)+DX1*(Point.Y-Point1.Y));
    // calculate intersection-point
    CutPoint:=GrPoint(Point.X+Result*DY1,Point.Y-Result*DX1);
  end;
end;

{******************************************************************************+
  Function VectorNormalIntersection
--------------------------------------------------------------------------------
  Calculates the cutting-point of the line and the normal to the line through
  the specified point. The function-result gives the normal distance of the
  point to the line. The vector-component must be normalized.
+******************************************************************************}
Function VectorNormalIntersection
   (
   const Point1    : TGrPoint;
   const Vector    : TGrPoint;
   const Point     : TGrPoint;
   var CutPoint    : TGrPoint
   )
   : Double;
  begin
    Assert(DblCompare(Norm(Vector),1)=0);
    { calculate normal-distance }
    Result:=(-Vector.Y*(Point.X-Point1.X)+Vector.X*(Point.Y-Point1.Y));
    { calculate intersection-point }
    CutPoint:=GrPoint(Point.X+Result*Vector.Y,Point.Y-Result*Vector.X);
  end;
   
{******************************************************************************+
  Function GrPointDistance
--------------------------------------------------------------------------------
  Calculates the distance between the two points.
+******************************************************************************}
Function GrPointDistance
   (
   const Point1    : TGrPoint;
   const Point2    : TGrPoint
   )
   : Double;
  begin
    Result:=Sqrt(Sqr(Point1.X-Point2.X)+Sqr(Point1.Y-Point2.Y));
  end;

{******************************************************************************+
  Procedure MovePoint
--------------------------------------------------------------------------------
  Moves the point by the specified distance in x- and y-direction.
+******************************************************************************}
Procedure MovePoint(var Point:TGrPoint;const DeltaX,DeltaY:Double);
begin
  Point.X:=Point.X+DeltaX;
  Point.Y:=Point.Y+DeltaY;
end;
     
{******************************************************************************+
  Function MovedPoint
--------------------------------------------------------------------------------
  Moves the point by the specified distance in x- and y-direction.
+******************************************************************************}
Function MovedPoint(const Point:TGrPoint;const DeltaX,DeltaY:Double):TGrPoint;
begin
  Result.X:=Point.X+DeltaX;
  Result.Y:=Point.Y+DeltaY;
end;

{******************************************************************************+
  Function LineAngle
--------------------------------------------------------------------------------
  in Point1, Point2 = points defining the line
  Result = angle in rad
+******************************************************************************}
Function LineAngle
   (
   const Point1    : TGrPoint;
   const Point2    : TGrPoint
   )
   : Double;
  var DX           : Double;
      DY           : Double;
  begin
    DX:=Point2.X-Point1.X;
    DY:=Point2.Y-Point1.Y;
    if Abs(DX)<Tolerance then begin
      if DY>=0 then Result:=Pi/2
      else Result:=-Pi/2;
    end
    else if Abs(DY)<Tolerance then begin
      if DX>=0 then Result:=0
      else Result:=Pi;
    end
    else begin
      Result:=ArcTan(DY/DX);
      if DX<0 then Result:=Pi+Result;
    end;
  end;

{******************************************************************************+
  Function RectsEqual
--------------------------------------------------------------------------------
  Determines, if the two rectangles are equal.
+******************************************************************************}
Function RectsEqual(const ARect,BRect:TGrRect):Boolean;
begin
  if Abs(ARect.Left-BRect.Left)<Tolerance then
  if Abs(ARect.Right-BRect.Right)<Tolerance then
  if Abs(ARect.Top-BRect.Top)<Tolerance then
  if Abs(ARect.Bottom-BRect.Bottom)<Tolerance then begin
    Result:=TRUE;
    Exit;
  end;
  Result:=FALSE;
end;

Function RectsEqual(const ARect,BRect:TRotRect):Boolean;
begin
  if Abs(ARect.X-BRect.X)<Tolerance then
  if Abs(ARect.Y-BRect.Y)<Tolerance then
  if Abs(ARect.Width-BRect.Width)<Tolerance then
  if Abs(ARect.Height-BRect.Height)<Tolerance then
  if Abs(ARect.Rotation-BRect.Rotation)<Tolerance then begin
    Result:=TRUE;
    Exit;
  end;
  Result:=FALSE;
end;

Function RectEmpty(const Rect:TGrRect):Boolean;
begin
  Result:=(Rect.Right-Rect.Left<Tolerance) or (Rect.Top-Rect.Bottom<Tolerance);
end;

Function RectEmpty(const Rect:TRotRect):Boolean;
begin
  Result:=(Rect.Width<Tolerance) or (Rect.Height<Tolerance);
end;

Function RectWidth(const ARect:TGrRect):Double;
begin
  Result:=ARect.Right-ARect.Left;
end;

Function RectWidth(const ARect:TRotRect):Double;
begin
  Result:=ARect.Width;
end;

Function RectHeight(const ARect:TRotRect):Double;
begin
  Result:=ARect.Height;
end;

Function RectHeight(const ARect:TGrRect):Double;
begin
  Result:=ARect.Top-ARect.Bottom;
end;

{******************************************************************************+
  Function ScaledGrRect
--------------------------------------------------------------------------------
  Returnes a scaled version of ARect.
+******************************************************************************}
Function ScaledRect(const ARect:TGrRect;const XScale,YScale:Double):TGrRect;
begin
  with ARect do begin
    Result.Left:=Left*XScale;
    Result.Right:=Right*XScale;
    Result.Top:=Top*YScale;
    Result.Bottom:=Bottom*YScale;
  end;
end;

{******************************************************************************+
  Function ScaledGrRect
--------------------------------------------------------------------------------
  Returnes a scaled version of ARect.
+******************************************************************************}
Function ScaledRect(const ARect:TRotRect;const XScale,YScale:Double):TRotRect;
begin
  with ARect do begin
    Result.X:=X*XScale;
    Result.Y:=Y*XScale;
    Result.Width:=Width*YScale;
    Result.Height:=Height*YScale;
  end;
end;

Procedure ScaleRect(var ARect:TGrRect;const XScale,YScale:Double);
begin
  with ARect do begin
    Left:=Left*XScale;
    Right:=Right*XScale;
    Top:=Top*YScale;
    Bottom:=Bottom*YScale;
  end;
end;

Procedure ScaleRect(var ARect:TRotRect;const XScale,YScale:Double);
begin
  with ARect do begin
    X:=X*XScale;
    Y:=Y*XScale;
    Width:=Width*XScale;
    Height:=Height*YScale;
  end;
end;

Procedure InflateGrRect(var ARect:TGrRect;const DX,DY:Double);
begin
  with ARect do begin
    Left:=Left-DX;
    Right:=Right+DX;
    Top:=Top+DY;
    Bottom:=Bottom-DY;
  end;
end;

Procedure InflateRotRect(var ARect:TRotRect;const DX,DY:Double);
begin
  with ARect do begin
    X:=X-DX;
    Y:=Y-DY;
    Width:=Width+2*DX;
    Height:=Height+2*DY;
  end;
end;

Function InflatedRect(Const ARect:TGrRect;const DX,DY:Double):TGrRect;
begin
  Result:=ARect;
  with Result do begin
    Left:=Left-DX;
    Right:=Right+DX;
    Top:=Top+DY;
    Bottom:=Bottom-DY;
  end;
end;


Function InflatedRect(Const ARect:TRotRect;const DX,DY:Double):TRotRect;
begin
  Result:=ARect;
  with Result do begin
    X:=X-DX;
    Y:=Y-DY;
    Width:=Width+2*DX;
    Height:=Height+2*DY;
  end;
end;

{******************************************************************************+
  Function GrPointsEqual
--------------------------------------------------------------------------------
  Determines, if the two points are equal.
+******************************************************************************}
Function GrPointsEqual
   (
   const APoint    : TGrPoint;
   const BPoint    : TGrPoint
   )
   : Boolean;
  begin
    if Abs(APoint.X-BPoint.X)<Tolerance then
    if Abs(APoint.Y-BPoint.Y)<Tolerance then begin
      Result:=TRUE;
      Exit;
    end;
    Result:=FALSE;
  end;

Function PointInsidePolygon
   (
   const Point     : TGrPoint;
   const Polygon   : Array of TGrPoint;
   PointCount      : Integer
   )
   : Boolean;
  var LeftPoint    : TGrPoint;
      Cnt          : Integer;
      Cut1         : TGrPoint;
      Cut2         : TGrPoint;
      CutCount     : Integer;
  begin
    LeftPoint:=GrPoint(-1E10,Point.Y);
    CutCount:=0;
    for Cnt:=Low(Polygon) to Low(Polygon)+PointCount-2 do if LineIntersection(LeftPoint,
        Point,Polygon[Cnt],Polygon[Cnt+1],Cut1,Cut2)>0 then Inc(CutCount);
    if not GrPointsEqual(Polygon[Low(Polygon)],Polygon[Low(Polygon)+PointCount-1])
        and (LineIntersection(LeftPoint,Point,Polygon[Low(Polygon)+PointCount-1],
            Polygon[Low(Polygon)],Cut1,Cut2)>0) then Inc(CutCount);
    Result:=CutCount and 1=1;
  end;

Procedure InsertPointInRect(APoint:TGrPoint;var ARect:TGrRect);
begin
  with ARect,APoint do begin
    if X<Left then Left:=X;
    if X>Right then Right:=X;
    if Y>Top then Top:=Y;
    if Y<Bottom then Bottom:=Y;
  end;
end;

{******************************************************************************+
  Function MoveRect
--------------------------------------------------------------------------------
  Moves the point by the specified distance in x- and y-direction.
+******************************************************************************}
Procedure MoveRect(var Rect:TGrRect;const DeltaX,DeltaY:Double);
begin
  with Rect do begin
    Left:=Left+DeltaX;
    Right:=Right+DeltaX;
    Top:=Top+DeltaY;
    Bottom:=Bottom+DeltaY;
  end;
end;

Procedure MoveRect(var Rect:TRotRect;const DeltaX,DeltaY:Double);
begin
  with Rect do begin
    X:=X+DeltaX;
    Y:=Y+DeltaY;
  end;
end;

Function CircleLineIntersection
   (
   const ACenter   : TGrPoint;
   const ARadius   : Double;
   const P1        : TGrPoint;
   const P2        : TGrPoint;
   var Cut1        : TGrPoint;
   var Cut2        : TGrPoint
   )
   : Integer;
  var D            : Double;
      DX           : Double;
      DY           : Double;
      DXK          : Double;
      DYK          : Double;
      N            : Double;
      P            : Double;
      T            : Double;
  begin
    Result:=0;
    DX:=P2.X-P1.X;
    DY:=P2.Y-P1.Y;
    DXK:=P1.X-ACenter.X;
    DYK:=P1.Y-ACenter.Y;
    N:=DX*DX+DY*DY;
    P:=(DX*DXK+DY*DYK)/N;
    D:=P*P+(1.0*ARadius*ARadius-DXK*DXK-DYK*DYK)/N;
    if D>=0 then begin
      D:=Sqrt(D);
      T:=-P-D;
      if (T>-Tolerance) and (T<1.0+Tolerance) then begin
        Cut1:=GrPoint(P1.X+T*DX,P1.Y+T*DY);
        Inc(Result);
      end;
      T:=-P+D;
      if (T>-Tolerance) and (T<1.0+Tolerance) then begin
        Cut2:=GrPoint(P1.X+T*DX,P1.Y+T*DY);
        Inc(Result);
      end;
    end;
  end;

Function CircleVectorIntersection
   (
   const ACenter   : TGrPoint;
   const ARadius   : Double;
   const P1        : TGrPoint;
   const P2        : TGrPoint;
   var Cut1        : TGrPoint;
   var Cut2        : TGrPoint
   )
   : Integer;
  var D            : Double;
      DX           : Double;
      DY           : Double;
      DXK          : Double;
      DYK          : Double;
      N            : Double;
      P            : Double;
      T            : Double;
  begin
    Result:=0;
    DX:=P2.X-P1.X;
    DY:=P2.Y-P1.Y;
    DXK:=P1.X-ACenter.X;
    DYK:=P1.Y-ACenter.Y;
    N:=DX*DX+DY*DY;
    P:=(DX*DXK+DY*DYK)/N;
    D:=P*P+(ARadius*ARadius-DXK*DXK-DYK*DYK)/N;
    if D>=0 then begin
      D:=Sqrt(D);
      T:=-P-D;
      Cut1:=GrPoint(P1.X+T*DX,P1.Y+T*DY);
      Inc(Result);
      T:=-P+D;
      Cut2:=GrPoint(P1.X+T*DX,P1.Y+T*DY);
      if not GrPointsEqual(Cut1,Cut2) then Inc(Result);
    end;
  end;

{******************************************************************************+
  Function MovePointAngle
--------------------------------------------------------------------------------
  Moves the point by the specified distance and the specified angle.
+******************************************************************************}
Procedure MovePointAngle
   (
   var Point       : TGrPoint;
   const Delta     : Double;
   const Angle     : Double
   );
  begin
    Point.X:=Point.X+Delta*Cos(Angle);
    Point.Y:=Point.Y+Delta*Sin(Angle);
  end;

{******************************************************************************+
  Function MovedPointAngle
--------------------------------------------------------------------------------
  Moves the point by the specified distance and the specified angle.
+******************************************************************************}
Function MovedPointAngle(const Point:TGrPoint;const Delta:Double;
    const Angle:Double):TGrPoint;
begin
  Result.X:=Point.X+Delta*Cos(Angle);
  Result.Y:=Point.Y+Delta*Sin(Angle);
end;

{******************************************************************************+
  Procedure ClipLineToRect
--------------------------------------------------------------------------------
  Clips the line specified by Point1 and Point2 to the rectangle. Returns
  false if the line lies completely outside the rectangle.
+******************************************************************************}
Function ClipLineToRect
    (
    var Point1     : TGrPoint;
    var Point2     : TGrPoint;
    const Rect     : TGrRect
    )
    : Boolean;
  var Point3       : TGrPoint;
      Point4       : TGrPoint;
  begin
    Result:=ClipVectorToRect(Point1,Point2,Rect,Point3,Point4) and
        (LineIntersection(Point3,Point4,Point1,Point2,Point1,Point2)=2);
  end;

{******************************************************************************+
  Function ClipVectorToRect
--------------------------------------------------------------------------------
  Clips the vector specified by Point and Angle to the rectangle. Returns
  false if the vector lies completely outside the rectangle.
+******************************************************************************}
Function ClipVectorToRect
   (
   const Point     : TGrPoint;
   const Vector    : TGrPoint;
   const Rect      : TGrRect;
   var Point1      : TGrPoint;
   var Point2      : TGrPoint
   )
   : Boolean;
  var PointCount   : Integer;
      CutPos       : TGrPoint;
      Points       : Array[0..3] of TGrPoint;
  Procedure AddPoint;
    begin
      if (PointCount=0) or not GrPointsEqual(Points[PointCount-1],CutPos) then begin
        Points[PointCount]:=CutPos;
        Inc(PointCount);
      end;
    end;
  begin
    PointCount:=0;
    with Rect do begin
      { calculate intersection with left-border }
      if VectorIntersection(Point,Vector,GrPoint(Left,Top),GrPoint(0,-1),
          CutPos)=1 then if CutPos.Y>=Rect.Bottom-Tolerance then
          if CutPos.Y<=Rect.Top+Tolerance then AddPoint;
      { calculate intersection with bottom-border }
      if VectorIntersection(Point,Vector,GrPoint(Left,Bottom),GrPoint(1,0),
          CutPos)=1 then if CutPos.X>=Rect.Left-Tolerance then
          if CutPos.X<=Rect.Right+Tolerance then AddPoint;
      { calculate intersection with right-border }
      if VectorIntersection(Point,Vector,GrPoint(Right,Bottom),GrPoint(0,1),
          CutPos)=1 then if CutPos.Y>=Rect.Bottom-Tolerance then
          if CutPos.Y<=Rect.Top+Tolerance then AddPoint;
      { calculate intersection with top-border }
      if VectorIntersection(Point,Vector,GrPoint(Right,Top),GrPoint(-1,0),
          CutPos)=1 then if CutPos.X>=Rect.Left-Tolerance then
          if CutPos.X<=Rect.Right+Tolerance then AddPoint;
    end;
    if PointCount=2 then begin
      Point1:=Points[0];
      Point2:=Points[1];
    end;  
    Result:=PointCount=2;
  end; 

{******************************************************************************+
  Function GrPointInRect
--------------------------------------------------------------------------------
  Determines if a point lies inside the rectangle.
+******************************************************************************}
Function GrPointInRect
   (
   const Point     : TGrPoint;
   const Rect      : TGrRect
   )
   : Boolean;
  begin
    Result:=FALSE;
    if (Point.X>=Rect.Left-Tolerance) then if (Point.Y>=Rect.Bottom-Tolerance) then
    if (Point.X<=Rect.Right+Tolerance) then if (Point.Y<=Rect.Top+Tolerance) then
      Result:=TRUE;
  end;

{==============================================================================+
  TGrPointList
+==============================================================================}

Destructor TGrPointList.Destroy;
  begin
    if FCapacity>0 then FreeMem(FItemPtr,FCapacity*SizeOf(TGrPoint));
    FCount:=0;
    FCapacity:=0;
  end;

Procedure TGrPointList.Assign(const Source:TGrPointList);
begin
  Capacity:=Source.Count;
  FCount:=Source.Count;
  Move(Source.FItemPtr^,FItemPtr^,FCount*SizeOf(TGrPoint));
end;

Procedure TGrPointList.Clear;
begin
  FCount:=0;
end;

Function TGrPointList.Extent:TGrRect;
var Cnt            : Integer;
begin
  with Result do begin
    if Count>0 then begin
      Result:=GrRect(1E300,1E300,-1E300,-1E300);
      for Cnt:=0 to Count-1 do with Points[Cnt] do begin
        if X<Left then Left:=X;
        if X>Right then Right:=X;
        if Y>Top then Top:=Y;
        if Y<Bottom then Bottom:=Y;
      end;
    end
    else Result:=GrRect(0,0,0,0);
  end;
end;

Function TGrPointList.GetItem(AIndex:LongInt):TGrPoint;
begin
  Result:=FItemPtr^[AIndex];
end;

Function TGrPointList.GetItemPtr(AIndex:LongInt):PGrPoint;
begin
  Result:=@FItemPtr^[AIndex];
end;

Procedure TGrPointList.SetItem(AIndex:LongInt;const Item:TGrPoint);
begin
  Move(Item,FItemPtr^[AIndex],SizeOf(TGrPoint));
end;

Procedure TGrPointList.SetCapacity(ACapacity:LongInt);
var FTempPtr     : PGrPointArray;
begin
  if FCapacity<FCount then FCapacity:=FCount;
  if ACapacity>FCapacity then begin
    if FCapacity=0 then GetMem(FItemPtr,ACapacity*SizeOf(TGrPoint))
    else begin
      GetMem(FTempPtr,ACapacity*SizeOf(TGrPoint));
      Move(FItemPtr^,FTempPtr^,FCapacity*SizeOf(TGrPoint));
      FreeMem(FItemPtr,FCapacity*SizeOf(TGrPoint));
      FItemPtr:=FTempPtr;
    end;
    FCapacity:=ACapacity;
  end;
end;

Procedure TGrPointList.Add(const Item:TGrPoint);
begin
  if FCount=FCapacity then Capacity:=Capacity+1024;
  Move(Item,FItemPtr^[FCount],SizeOf(TGrPoint));
  Inc(FCount);
end;

{******************************************************************************+
  Function RotRect
--------------------------------------------------------------------------------
  constructs a TRotRect from X, Y, Width, Height and Angle.
+******************************************************************************}
Function RotRect(const AX,AY,AWidth,AHeight,ARotation:Double):TRotRect; 
begin
  with Result do begin
    X:=AX;
    Y:=AY;
    Width:=AWidth;
    Height:=AHeight;
    Rotation:=ARotation;
  end;
end;

Function RotRect(Const Rect:TGrRect;ARotation:Double):TRotRect;
begin
  with Rect do begin
    Result.X:=Left;
    Result.Y:=Bottom;
    Result.Width:=Right-Left;
    Result.Height:=Top-Bottom;
    Result.Rotation:=ARotation;
  end;
end;

Function RotRect(const Position:TGrPoint;AWidth,AHeight,ARotation:Double):TRotRect;
begin
  with Result do begin
    Origin:=Position;
    Width:=AWidth;
    Height:=AHeight;
    Rotation:=ARotation;
  end;
end;

{******************************************************************************+
  Function RotRect
--------------------------------------------------------------------------------
  constructs a RotRect from two corners and the rotation of the rectangle.
+******************************************************************************}
Function RotRect(const Point1,Point2:TGrPoint;const Angle:Double):TRotRect;
var DX             : Double;
    DY             : Double;
    CosAngle       : Double;
    SinAngle       : Double;
begin
  with Result do begin
    DX:=Point2.X-Point1.X;                      
    DY:=Point2.Y-Point1.Y;
    SinCos(Angle,SinAngle,CosAngle);
    Width:=DX*CosAngle+DY*SinAngle;
    Height:=DY*CosAngle-DX*SinAngle;
    Origin:=Point1;
    // correct origin if width or height less than zero
    if Width<0 then begin
      Origin.X:=Origin.X+Width*CosAngle;
      Origin.Y:=Origin.Y+Width*SinAngle;
      Width:=Abs(Width);
    end;
    if Height<0 then begin
      Origin.X:=Origin.X-Height*SinAngle;
      Origin.Y:=Origin.Y+Height*CosAngle;
      Height:=Abs(Height);
    end;
    Rotation:=Angle;
  end;
end;

Procedure RectCorners(const RotRect:TRotRect;var Corners:TGrRectCorners);
var CosAngle       : Double;
    SinAngle       : Double;
begin
  with RotRect do begin
    SinCos(Rotation,SinAngle,CosAngle);
    Corners[0]:=RotRect.Origin;
    Corners[1].X:=Corners[0].X+Width*CosAngle;
    Corners[1].Y:=Corners[0].Y+Width*SinAngle;
    Corners[2].X:=Corners[1].X-Height*SinAngle;
    Corners[2].Y:=Corners[1].Y+Height*CosAngle;
    Corners[3].X:=Corners[2].X-Width*CosAngle;
    Corners[3].Y:=Corners[2].Y-Width*SinAngle;
  end;
end;

Procedure RectCorners(const GrRect:TGrRect;var Corners:TGrRectCorners); 
begin
  with GrRect do begin
    Corners[0]:=BottomLeft;
    Corners[1].X:=Right;
    Corners[1].Y:=Bottom;
    Corners[2]:=TopRight;
    Corners[3].X:=Left;
    Corners[3].Y:=Top;
  end;
end;                               

Function ISOFrame(const Rect:TRotRect):TGrRect;
var CosAngle       : Double;
    SinAngle       : Double;
begin
  with Result,Rect do begin
    SinCos(Rotation,SinAngle,CosAngle);
    Left:=X+0.5*(Width*(CosAngle-Abs(CosAngle))-Height*(SinAngle+Abs(SinAngle)));
    Bottom:=Y+0.5*(Width*(SinAngle-Abs(SinAngle))+Height*(CosAngle-Abs(CosAngle)));
    CosAngle:=Abs(CosAngle);
    SinAngle:=Abs(SinAngle);
    Right:=Left+Width*CosAngle+Height*SinAngle;
    Top:=Bottom+Height*CosAngle+Width*SinAngle;
  end;
end;

{******************************************************************************+
  Function ISOFrameWidth  NYT
--------------------------------------------------------------------------------
  Calculates the width of the ISO-oriented frame of the rectangle.
+******************************************************************************}
Function ISOFrameWidth(const Rect:TRotRect):Double;
begin
  with Rect do Result:=Width*Abs(Cos(Rotation))+Height*Abs(Sin(Rotation));
end;

{******************************************************************************+
  Function ISOFrameWidth  NYT
--------------------------------------------------------------------------------
  Calculates the height of the ISO-oriented frame of the rectangle.
+******************************************************************************}
Function ISOFrameHeight(const Rect:TRotRect):Double;
begin
  with Rect do Result:=Width*Abs(Sin(Rotation))+Height*Abs(Cos(Rotation));
end;

Function FitRectangle(const Rect:TGrRect;const Angle:Double):TRotRect;
var AWidth       : Double;
    AHeight      : Double;
    CosAngle     : Double;
    SinAngle     : Double;
begin
  with Result do begin
    SinCos(Angle,SinAngle,CosAngle);
    AWidth:=RectWidth(Rect);
    AHeight:=RectHeight(Rect);
    Width:=(AWidth*Abs(CosAngle)-AHeight*Abs(SinAngle))/Cos(2*Angle);
    Height:=(AHeight*Abs(CosAngle)-AWidth*Abs(SinAngle))/Cos(2*Angle);
    X:=Rect.Left-0.5*(Width*(CosAngle-Abs(CosAngle))-Height*(SinAngle+Abs(SinAngle)));
    Y:=Rect.Bottom-0.5*(Width*(SinAngle-Abs(SinAngle))+Height*(CosAngle-Abs(CosAngle)));
    Rotation:=Angle;
  end;
end;

Function FitISORectangle(const Rect:TRotRect):TGrRect;
var CosAngle     : Double;
    SinAngle     : Double;
    AWidth       : Double;
    AHeight      : Double;
begin
  with Result,Rect do begin
    SinCos(Rotation,SinAngle,CosAngle);
    AWidth:=(Width*Abs(CosAngle)-Height*Abs(SinAngle))/Cos(2*Rotation);
    AHeight:=(Height*Abs(CosAngle)-Width*Abs(SinAngle))/Cos(2*Rotation);
    Left:=X+0.5*(Width*CosAngle-Height*SinAngle-AWidth);
    Bottom:=Y+0.5*(Width*SinAngle+Height*CosAngle-AHeight);
    Right:=Left+AWidth;
    Top:=Bottom+AHeight;
  end;
end;

{******************************************************************************+
  Procedure Normalize
--------------------------------------------------------------------------------
  Normalizes the specified vector. If the vector has zero-length nothing is done.
+******************************************************************************}
Procedure Normalize(var Vector:TGrPoint);
var Norm           : Double;
begin
  { calculate lenght of vector }
  Norm:=Sqrt(Sqr(Vector.X)+Sqr(Vector.Y));
  { avoid division by zero }
  if Abs(Norm)>Tolerance then begin
    Vector.X:=Vector.X/Norm;
    Vector.Y:=Vector.Y/Norm;
  end;
end;

Function Normalized(const Vector:TGrPoint):TGrPoint;
var Norm           : Double;
begin
  { calculate lenght of vector }
  Norm:=Sqrt(Sqr(Vector.X)+Sqr(Vector.Y));
  { avoid division by zero }
  if Abs(Norm)>Tolerance then begin
    Result.X:=Vector.X/Norm;
    Result.Y:=Vector.Y/Norm;
  end;
end;

{******************************************************************************+
  Function RotatedPoint
--------------------------------------------------------------------------------
  Rotates the point around the coordinate-system-origin by the specified
  angle.
+******************************************************************************}
Function RotatedPoint(const Point:TGrPoint;const Angle:Double):TGrPoint;
begin
  Result.X:=Point.X*Cos(Angle)-Point.Y*Sin(Angle);
  Result.Y:=Point.X*Sin(Angle)+Point.Y*Cos(Angle);
end;

Function RotatedPoint(const Point:TGrPoint;const SinAngle,CosAngle:Double):TGrPoint;
begin
  Result.X:=Point.X*CosAngle-Point.Y*SinAngle;
  Result.Y:=Point.X*SinAngle+Point.Y*CosAngle;
end;

{******************************************************************************+
  Function RotatedPoint
--------------------------------------------------------------------------------
  Rotates the point around the coordinate-system-origin by the specified
  angle.
+******************************************************************************}
Procedure RotatePoint(var Point:TGrPoint;const Angle:Double);
var NewX           : Double;
    NewY           : Double;
    CosAngle       : Double;
    SinAngle       : Double;
begin
  SinCos(Angle,SinAngle,CosAngle);
  NewX:=Point.X*Cos(Angle)-Point.Y*Sin(Angle);
  NewY:=Point.X*Sin(Angle)+Point.Y*Cos(Angle);
  Point.X:=NewX;
  Point.Y:=NewY;
end;

Procedure RotatePoint(var Point:TGrPoint;const SinAngle,CosAngle:Double);
var NewX           : Double;
    NewY           : Double;
begin
  NewX:=Point.X*CosAngle-Point.Y*SinAngle;
  NewY:=Point.X*SinAngle+Point.Y*CosAngle;
  Point.X:=NewX;
  Point.Y:=NewY;
end;

Function RotatedRect(const Rect:TGrRect;const Angle:Double):TGrRect;
var CosAngle       : Double;
    SinAngle       : Double;
begin
  with Rect do begin
    SinCos(Angle,SinAngle,CosAngle);
    Result.Left:=Left*CosAngle-Bottom*SinAngle;
    Result.Bottom:=Left*SinAngle+Bottom*CosAngle;
    Result.Right:=Right*CosAngle-Top*SinAngle;
    Result.Top:=Right*SinAngle+Top*CosAngle;
  end;
end;

Function RotatedRect(const Rect:TGrRect;const SinAngle,CosAngle:Double):TGrRect;
begin
  with Rect do begin
    Result.Left:=Left*CosAngle-Bottom*SinAngle;
    Result.Bottom:=Left*SinAngle+Bottom*CosAngle;
    Result.Right:=Right*CosAngle-Top*SinAngle;
    Result.Top:=Right*SinAngle+Top*CosAngle;
  end;
end;

Procedure RotateRect(var Rect:TGrRect;const Angle:Double);
var NewX           : Double;
    NewY           : Double;
    CosAngle       : Double;
    SinAngle       : Double;
begin
  with Rect do begin
    SinCos(Angle,SinAngle,CosAngle);
    NewX:=Left*CosAngle-Bottom*SinAngle;
    NewY:=Left*SinAngle+Bottom*CosAngle;
    Left:=NewX;
    Bottom:=NewY;
    NewX:=Right*CosAngle-Top*SinAngle;
    NewY:=Right*SinAngle+Top*CosAngle;
    Right:=NewX;
    Top:=NewY;
  end;
end;

Procedure RotateRect(var Rect:TGrRect;const SinAngle,CosAngle:Double);
var NewX           : Double;
    NewY           : Double;
begin
  with Rect do begin
    NewX:=Left*CosAngle-Bottom*SinAngle;
    NewY:=Left*SinAngle+Bottom*CosAngle;
    Left:=NewX;
    Bottom:=NewY;
    NewX:=Right*CosAngle-Top*SinAngle;
    NewY:=Right*SinAngle+Top*CosAngle;
    Right:=NewX;
    Top:=NewY;
  end;
end;

{******************************************************************************+
  Function RectCenter
--------------------------------------------------------------------------------
  Returnes the coordinates of the center-point of the rectangle.
+******************************************************************************}
Function RectCenter(const Rect:TGrRect):TGrPoint;
begin
  with Rect do begin
    Result.X:=(Left+Right)/2;
    Result.Y:=(Bottom+Top)/2;
  end;
end;

Function RectCenter(const Rect:TRotRect):TGrPoint;
var SinAngle       : Double;
    CosAngle       : Double;
begin
  with Rect do begin
    SinCos(Rotation,SinAngle,CosAngle);
    Result.X:=Origin.X+(Width*CosAngle-Height*SinAngle)/2;
    Result.Y:=Origin.Y+(Height*CosAngle+Width*SinAngle)/2;
  end;
end;

Procedure RotateRect(var Rect:TRotRect;const Angle:Double);
begin
  with Rect do begin
    RotatePoint(Origin,Angle);
    Rotation:=Rotation+Angle;
  end;
end;

Function RotatedRect(const Rect:TRotRect;const Angle:Double):TRotRect;
begin
  with Rect do begin
    Result.Origin:=RotatedPoint(Origin,Angle);
    Result.Width:=Width;
    Result.Height:=Height;
    Result.Rotation:=Rotation+Angle;
  end;
end;

Function Norm(const Vector:TGrPoint):Double;
begin
  Result:=Sqrt(Sqr(Vector.X)+Sqr(Vector.Y));
end;

Function ScalarProduct(const Vect1,Vect2:TGrPoint):Double;
begin
  Result:=Vect1.X*Vect2.X+Vect1.Y*Vect2.Y;
end;

Function CrossProduct(const Vect1,Vect2:TGrPoint):Double;
begin
  Result:=Vect1.X*Vect2.Y-Vect1.Y*Vect2.X;
end;

Function RectInside(Rect:TGrRect;InRect:TGrRect):Boolean;
begin
  Result:=FALSE;
  with Rect do  if Left>=InRect.Left then if Right<=InRect.Right then
      if Top<=InRect.Top then if Bottom>=InRect.Bottom then Result:=TRUE;
end;

Function GrRect(const Rect:TRect):TGrRect;
begin
  with Result do begin
    Left:=Rect.Left;
    Right:=Rect.Right;
    Top:=Rect.Bottom;
    Bottom:=Rect.Top;
  end;
end;

Function PointOnArc(const Point,Center,Start,Stop:TGrPoint):Boolean;
begin
  Result:=PointOnArc(Point,Center,LineAngle(Center,Start),LineAngle(Center,Stop));
end;

Function PointOnArc(const Point,Center:TGrPoint;Start,Stop:Double):Boolean;
var Angle          : Double;
begin
  if Start<0 then Start:=Start+2*Pi;
  if Stop<0 then Stop:=Stop+2*Pi;
  if Stop<Start then Stop:=Stop+2*Pi;
  Angle:=LineAngle(Center,Point);
  if Angle<0 then Angle:=Angle+2*Pi;
  Result:=(Angle>=Start) and (Angle<=Stop);
end;

Function ArcClipRect(const Center:TGrPoint;Const Radius:Double;Start,Stop:TGrPoint):TGrRect;
begin
  Result:=ArcClipRect(Center,Radius,LineAngle(Center,Start),LineAngle(Center,Stop));
end;

Function ArcClipRect(const Center:TGrPoint;Radius,Start,Stop:Double):TGrRect;
  Function PointOnArc(Angle:Double):Boolean;
  begin
    if Angle>Stop then Angle:=Angle-2*Pi;
    Result:=(Angle>=Start) and (Angle<=Stop);
  end;
begin
  Result:=GrRect(1E301,1E301,-1E301,-1E301);
  InsertPointInRect(GrPoint(Center.X+Radius*Cos(Start),Center.Y
      +Radius*Sin(Start)),Result);
  InsertPointInRect(GrPoint(Center.X+Radius*Cos(Stop),Center.Y
      +Radius*Sin(Stop)),Result);
  if Start<0 then Start:=Start+2*Pi;
  if Stop<0 then Stop:=Stop+2*Pi;
  if Start>Stop then Start:=Start-2*Pi;
  if PointOnArc(0) then InsertPointInRect(GrPoint(Center.X+Radius,Center.Y),Result);
  if PointOnArc(Pi/2) then InsertPointInRect(GrPoint(Center.X,Center.Y+Radius),Result);
  if PointOnArc(Pi) then InsertPointInRect(GrPoint(Center.X-Radius,Center.Y),Result);
  if PointOnArc(3*Pi/2) then InsertPointInRect(GrPoint(Center.X,Center.Y-Radius),Result);
end;

Function CircleClipRect(const Center:TGrPoint;Const Radius:Double):TGrRect;
begin
  Result:=GrRect(Center.X-Radius,Center.Y-Radius,Center.X+Radius,Center.Y+Radius);
end;

end.
