unit Regwco32;

interface

procedure Register;

implementation

uses DesignIntf, Classes, MenuList, Validate, DipGrid, MultiLng,
  ComboBtn, SpinBtn, ChkList, WCtrls, EditHndl, Rollup, MeasProp, FileTree, XYSelect,
  TreeView, InplEdit, StdCtrls, ListDlg, ImagEdit, Scroller, InpHndl,
  SelectFolderDlg, MultiDlg, IntervalList, ColorSelector, UserIntf, WDBCtrls,
  SerPort, MenuFn, MSConvert, InputBox, ColumnList, Modem, ProgressDlg,
  PolyEdit;

procedure Register;
begin
  RegisterComponents('CommonLib', [TSpeedBtn, TMenuHistoryList, TWTabbedNotebook,
    TWListBox, TValidator, TIntValidator, TFloatValidator, TPXValidator, TScaleValidator,
      TTimeValidator, TInputBox, TFileValidator, TPathValidator,
      TDipGrid, TDipList, TComboBtn, TDropDownBtn, TPopupMenuBtn, TSpinBtn, TCheckedListbox,
      TWGroupBox, TMeasureValidator, TWindow, TRollup, TMeasureLookupValidator,
      TWLabel, TMlgSection, TFileTreeView, TXYSelector, TTreeListView, TInplaceEditor,
      TSelectionDialog, TImageEditor, TScroller, TSelectFolderDialog,
      TWStatusBar, TWListView, TWButton, TUIInitialize, TUIExtension, TSerialPort,
      TTextConverter, TWRadioGroup, TColumnListbox, TColumnTreeListView,
      TWDBRadioGroup, TColorSelectBtn,
      TColumnComboBox, TModem, TWOpenDialog, TWSaveDialog, TProgressDialog]);
  RegisterComponents('InputHandlers', [TEditHandler, TRubberEllipse, TRubberBox, TRubberLine,
    TRubberPolygon, TGuideLine, TRubberArc, TRubberGrid, TPolyEditor]);

  RegisterPropertyEditor(TypeInfo(LongInt), TMeasureValidator, 'UseableUnits', TMeasureUnitsSetProperty);
end;

end.

