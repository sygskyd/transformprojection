Unit Chklist;

Interface

Uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, StdCtrls,Menus;

Const MaxSmallInt       = 32767;  

Type TOnCheckItem       = Procedure(Sender:TObject;AIndex:Integer) of Object;

     PIntArray          = ^TIntArray;
     TIntArray          = Array[0..MaxSmallInt Div 2-1] of Integer;

     TCheckedListbox    = Class(TCustomListBox)
      Private
       FOnCheckItem     : TOnCheckItem;
       FCheckedCount    : Integer;
       FChecked         : PIntArray;
       FClickedIndex    : Integer;
       FDisabled        : PIntArray;
       FDisabledCount   : Integer;
       Function    BlockSize(ACount:Integer):Integer;
       Procedure   CNDrawItem(var Message: TWMDrawItem); message CN_DrawItem;
       Function    GetCheckedItem(AIndex:Integer):Boolean;
       function    GetDisabledItem(AIndex: Integer): Boolean;
       Procedure   SetCheckedItem(AIndex:Integer;AChecked:Boolean);
       Procedure   SetCheckedCount(ACount:Integer);
       Procedure   SetDisabledCount(ACount:Integer);
       procedure   SetDisabledItem(AIndex: Integer; const Value: Boolean);
       Procedure   WMLButtonDown(var Message:TWMMouse); message wm_LButtonDown;
       Procedure   WMLButtonUp(var Message:TWMMouse); message wm_LButtonUp;
      Protected
       Property    DisabledCount:Integer read FDisabledCount write SetDisabledCount;
       Procedure   DrawItem(Index:Integer;BRect:TRect;State:TOwnerDrawState); override;
      Public
       Constructor Create(AParent:TComponent); override;
       Destructor  Destroy; override;
       Property    Checked[AIndex:Integer]:Boolean read GetCheckedItem write SetCheckedItem;
       Property    CheckedCount:Integer read FCheckedCount write SetCheckedCount;
       Property    Disabled[AIndex:Integer]:Boolean read GetDisabledItem write SetDisabledItem;
      Published
       property    Align;
       Property    BorderStyle;
       Property    Color;
       Property    Columns;
       Property    Ctl3D;
       Property    DragCursor;
       Property    DragMode;
       Property    Enabled;
       Property    ExtendedSelect;
       Property    Font;
       Property    IntegralHeight;
       Property    ItemHeight;
       Property    Items;
       Property    MultiSelect;
       Property    ParentColor;
       Property    ParentCtl3D;
       Property    ParentFont;
       Property    ParentShowHint;
       Property    PopupMenu;
       Property    ShowHint;
       Property    Sorted;
       Property    TabOrder;
       Property    TabStop;
       Property    Visible;
       Property    OnCheckItem:TOnCheckItem read FOnCheckItem write FOnCheckItem;
       Property    OnClick;
       Property    OnDblClick;
       Property    OnDragDrop;
       Property    OnDragOver;
       Property    OnDrawItem;
       Property    OnEndDrag;
       Property    OnEnter;
       Property    OnExit;
       Property    OnKeyDown;
       Property    OnKeyPress;
       Property    OnKeyUp;
       Property    OnMeasureItem;
       Property    OnMouseDown;
       Property    OnMouseMove;
       Property    OnMouseUp;
     end;

Implementation

{$IFDEF WIN32}
{$R *.R32}
{$ELSE}
{$R *.RES}
{$ENDIF}

var CheckedBitmap         : TBitmap;
    CheckedDisabledBitmap : TBitmap;
    UncheckedBitmap       : TBitmap;
    BitmapRect            : TRect;

Constructor TCheckedListbox.Create(AParent:TComponent);
begin
  inherited Create(AParent);
  Style:=lbOwnerDrawFixed;
  GetMem(FChecked,BlockSize(1)*SizeOf(Integer));
  GetMem(FDisabled,BlockSize(1)*SizeOf(Integer));
end;

Destructor TCheckedListbox.Destroy;
begin
  FreeMem(FChecked,BlockSize(FCheckedCount)*SizeOf(Integer));
  FreeMem(FDisabled,BlockSize(FDisabledCount)*SizeOf(Integer));
  inherited Destroy;
end;

Procedure TCheckedListbox.CNDrawItem(var Message:TWMDrawItem);
var State        : TOwnerDrawState;
begin
  with Message.DrawItemStruct^ do begin
    {$IFDEF WIN32}
    State := TOwnerDrawState(LongRec(itemState).Lo);
//    State:=TOwnerDrawState(WordRec(LongRec(itemState).Lo).Lo);
    {$ELSE}
    State:=TOwnerDrawState(WordRec(itemState).Lo);
    {$ENDIF}
    Canvas.Handle:=hDC;
    Canvas.Font:=Font;
    Canvas.Brush:=Brush;
    if Integer(itemID)>=0 then DrawItem(itemID,rcItem,State)
    else Canvas.FillRect(rcItem);
    Canvas.Handle:=0;
  end;
end;

Procedure TCheckedListbox.WMLButtonDown(var Message:TWMMouse);
begin
  with Message do begin
    inherited;
    if XPos<BitmapRect.Right then
      FClickedIndex:=ItemAtPos(Point(XPos,YPos),TRUE);
  end;
end;

Procedure TCheckedListbox.WMLButtonUp(var Message:TWMMouse);
var AIndex       : Integer;
begin
  with Message do begin
    AIndex:=ItemAtPos(Point(XPos,YPos),TRUE);
    if (XPos<BitmapRect.Right) and (AIndex=FClickedIndex)
        and not Disabled[AIndex] then begin
      Checked[AIndex]:=not Checked[AIndex];
      if Assigned(FOnCheckItem) then OnCheckItem(Self,AIndex);
    end;
    inherited;
  end;
end;

Function TCheckedListbox.GetCheckedItem(AIndex:Integer):Boolean;
var Cnt          : Integer;
begin
  for Cnt:=0 to FCheckedCount-1 do if FChecked^[Cnt]=AIndex then begin
    Result:=TRUE;
    Exit;
  end;
  Result:=FALSE;
end;

Procedure TCheckedListbox.SetCheckedItem(AIndex:Integer;AChecked:Boolean);
var Cnt          : Integer;
    Index        : Integer;
    ARect        : TRect;
begin
  Index:=-1;
  for Cnt:=0 to FCheckedCount-1 do if FChecked^[Cnt]=AIndex then begin
    Index:=Cnt;
    Break;
  end;
  if (Index<>-1)
      and not AChecked then begin
    for Cnt:=Index to FCheckedCount-2 do
        FChecked^[Cnt]:=FChecked^[Cnt+1];
    CheckedCount:=CheckedCount-1;
    ARect:=Rect(0,(AIndex-TopIndex)*ItemHeight,
        BitmapRect.Right+2,(AIndex-TopIndex+1)*ItemHeight);
    InvalidateRect(Handle,@ARect,FALSE);
  end
  else if (Index=-1) and
      AChecked then begin
    CheckedCount:=CheckedCount+1;
    FChecked^[FCheckedCount-1]:=AIndex;
    ARect:=Rect(0,(AIndex-TopIndex)*ItemHeight,
        BitmapRect.Right+2,(AIndex-TopIndex+1)*ItemHeight);
    InvalidateRect(Handle,@ARect,FALSE);
  end;
end;

Procedure TCheckedListbox.DrawItem(Index:Integer;BRect:TRect;State:TOwnerDrawState);
var ARect        : TRect;
    Disable      : Boolean;
begin
  with Canvas do begin
    ARect:=Rect(BRect.Left+2,BRect.Top+(BRect.Bottom-BRect.Top-BitmapRect.Bottom) Div 2,
      BRect.Left+BitmapRect.Right+2,0);
    ARect.Bottom:=ARect.Top+BitmapRect.Bottom;
    Disable:=Disabled[Index];
    if Checked[Index] then begin
      if Disable then BrushCopy(ARect,CheckedDisabledBitmap,BitmapRect,clWhite)
      else BrushCopy(ARect,CheckedBitmap,BitmapRect,clWhite);
    end  
    else BrushCopy(ARect,UnCheckedBitmap,BitmapRect,clWhite);
    if Disable then Font.Color:=clBtnShadow;
    if odSelected in State then begin
      Brush.Color:=clHighlight;
      if not Disable then Font.Color:=clHighlightText
    end;
    Inc(BRect.Left,BitmapRect.Right+6);
    FillRect(BRect);
    TextOut(BRect.Left+2,BRect.Top+1,Items[Index]);
    if odFocused in State then DrawFocusRect(BRect);
  end;
end;

Function TCheckedListbox.BlockSize(ACount:Integer):Integer;
begin
  Result:=(ACount Div 8+1)*8;
end;

Procedure TCheckedListbox.SetCheckedCount(ACount:Integer);
var HelpPtr      : PIntArray;
begin
  if BlockSize(ACount)<>BlockSize(FCheckedCount) then begin
    GetMem(HelpPtr,BlockSize(ACount)*SizeOf(Integer));
    FillChar(HelpPtr^,BlockSize(ACount)*SizeOf(Integer),#0);
    if ACount>FCheckedCount then Move(FChecked^,HelpPtr^,BlockSize(FCheckedCount))
    else Move(FChecked^,HelpPtr^,BlockSize(ACount));
    FreeMem(FChecked,BlockSize(FCheckedCount));
    FChecked:=HelpPtr;
  end;
  FCheckedCount:=ACount;
end;

Procedure TCheckedListbox.SetDisabledCount(ACount:Integer);
var HelpPtr      : PIntArray;
begin
  if BlockSize(ACount)<>BlockSize(FDisabledCount) then begin
    GetMem(HelpPtr,BlockSize(ACount)*SizeOf(Integer));
    FillChar(HelpPtr^,BlockSize(ACount)*SizeOf(Integer),#0);
    if ACount>FDisabledCount then Move(FDisabled^,HelpPtr^,BlockSize(FDisabledCount))
    else Move(FDisabled^,HelpPtr^,BlockSize(ACount));
    FreeMem(FDisabled,BlockSize(FDisabledCount));
    FDisabled:=HelpPtr;
  end;
  FDisabledCount:=ACount;
end;

{$IFNDEF WIN32}
Procedure ExitUnit; Far;
begin
  CheckedBitmap.Free;
  UnCheckedBitmap.Free;
  CheckedDisabledBitmap.Free;
end;
{$ENDIF}

function TCheckedListbox.GetDisabledItem(AIndex: Integer): Boolean;
var Cnt          : Integer;
begin
  for Cnt:=0 to FDisabledCount-1 do if FDisabled^[Cnt]=AIndex then begin
    Result:=TRUE;
    Exit;
  end;
  Result:=FALSE;
end;

procedure TCheckedListbox.SetDisabledItem(AIndex: Integer; const Value: Boolean);
var Cnt          : Integer;
    Index        : Integer;
    ARect        : TRect;
begin
  Index:=-1;
  for Cnt:=0 to FDisabledCount-1 do if FDisabled^[Cnt]=AIndex then begin
    Index:=Cnt;
    Break;
  end;
  if (Index<>-1) and not Value then begin
    for Cnt:=Index to FDisabledCount-2 do FDisabled^[Cnt]:=FDisabled^[Cnt+1];
    DisabledCount:=DisabledCount-1;
    ARect:=Rect(0,(AIndex-TopIndex)*ItemHeight,ClientWidth,(AIndex-TopIndex+1)*ItemHeight);
    InvalidateRect(Handle,@ARect,FALSE);
  end
  else if (Index=-1) and Value then begin
    DisabledCount:=DisabledCount+1;
    FDisabled^[FDisabledCount-1]:=AIndex;
    ARect:=Rect(0,(AIndex-TopIndex)*ItemHeight,ClientWidth,(AIndex-TopIndex+1)*ItemHeight);
    InvalidateRect(Handle,@ARect,FALSE);
  end;
end;

Initialization
  CheckedBitmap:=TBitmap.Create;
  UnCheckedBitmap:=TBitmap.Create;
  CheckedDisabledBitmap:=TBitmap.Create;
  {$IFNDEF WIN32}
  AddExitProc(ExitUnit);
  {$ENDIF}
  CheckedBitmap.Handle:=LoadBitmap(hInstance,'CHECKEDBMP');
  CheckedDisabledBitmap.Handle:=LoadBitmap(hInstance,'CHECKEDDISABLEDBMP');
  UnCheckedBitmap.Handle:=LoadBitmap(hInstance,'UNCHECKEDBMP');
  with UncheckedBitmap do BitmapRect:=Rect(0,0,Width,Height);

{$IFDEF WIN32}
Finalization
  CheckedBitmap.Free;
  CheckedDisabledBitmap.Free;
  UnCheckedBitmap.Free;
{$ENDIF}

end.
