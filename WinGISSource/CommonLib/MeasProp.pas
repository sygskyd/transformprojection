unit MeasProp;

interface

{$IFDEF WIN32}
{$H+}
{$ENDIF}

uses Classes, DesignIntf, DesignEditors, Measures, WinProcs;

type
  PLongInt = ^LongInt;

  TMeasureUnitsSetProperty = class(TPropertyEditor)
  public
    function GetAttributes: TPropertyAttributes; override;
    procedure GetProperties(Proc: TGetPropProc); override;
    function GetValue: string; override;
  end;

  TMeasureUnitsProperty = class(TPropertyEditor)
  private
    FElement: TMeasureUnits;
    FParent: TMeasureUnitsSetProperty;
    constructor Create(AParent: TMeasureUnitsSetProperty; AElement: TMeasureUnits);
  public
    destructor Destroy; override;
    function AllEqual: Boolean; override;
    function GetAttributes: TPropertyAttributes; override;
    function GetName: string; override;
    function GetValue: string; override;
    procedure GetValues(Proc: TGetStrProc); override;
    procedure SetValue(const Value: string); override;
  end;

implementation

uses SysUtils, TypInfo, Validate;

function TMeasureUnitsSetProperty.GetAttributes
  : TPropertyAttributes;
begin
  Result := [paMultiSelect, paSubProperties, paReadOnly];
end;

procedure TMeasureUnitsSetProperty.GetProperties(Proc: TGetPropProc);
var
  I: TMeasureUnits;
begin
  for I := Low(TMeasureUnits) to High(TMeasureUnits) do
    Proc(TMeasureUnitsProperty.Create(Self, I));
end;

function TMeasureUnitsSetProperty.GetValue: string;
var
  W: TMeasureUnitsSet;
  I: TMeasureUnits;
begin
  FillChar(W, SizeOf(W), #0);
  PLongInt(@W)^ := GetOrdValue;
  Result := '[';
  for I := Low(TMeasureUnits) to High(TMeasureUnits) do
    if I in TMeasureUnitsSet(W) then
    begin
      if Length(Result) <> 1 then
        Result := Result + ',';
      Result := Result + UnitsToIdent(I);
    end;
  Result := Result + ']';
end;

constructor TMeasureUnitsProperty.Create
  (
  AParent: TMeasureUnitsSetProperty;
  AElement: TMeasureUnits
  );
begin
  FParent := AParent;
  FElement := AElement;
end;

destructor TMeasureUnitsProperty.Destroy;
begin
end;

function TMeasureUnitsProperty.AllEqual
  : Boolean;
var
  I: Integer;
  W: TMeasureUnitsSet;
  V: Boolean;
begin
  Result := FALSE;
  if FParent.PropCount > 1 then
  begin
    FillChar(W, SizeOf(W), #0);
    PLongInt(@W)^ := FParent.GetOrdValue;
    V := FElement in TMeasureUnitsSet(W);
    for I := 1 to PropCount - 1 do
    begin
      FillChar(W, SizeOf(W), #0);
      PLongInt(@W)^ := FParent.GetOrdValueAt(I);
      if (FElement in W) <> V then
        Exit;
    end;
  end;
  Result := True;
end;

function TMeasureUnitsProperty.GetAttributes: TPropertyAttributes;
begin
  Result := [paMultiSelect, paValueList, paSortList];
end;

function TMeasureUnitsProperty.GetName: string;
begin
  Result := UnitsToIdent(FElement);
end;

function TMeasureUnitsProperty.GetValue: string;
var
  W: TMeasureUnitsSet;
begin
  FillChar(W, SizeOf(W), #0);
  PLongInt(@W)^ := FParent.GetOrdValue;
  if FElement in W then
    Result := 'True'
  else
    Result := 'False';
end;

procedure TMeasureUnitsProperty.GetValues
  (
  Proc: TGetStrProc
  );
begin
  Proc('False');
  Proc('True');
end;

procedure TMeasureUnitsProperty.SetValue
  (
  const Value: string
  );
var
  W: TMeasureUnitsSet;
begin
  FillChar(W, SizeOf(W), #0);
  PLongInt(@W)^ := FParent.GetOrdValue;
  if CompareText(Value, 'True') = 0 then
    Include(W, FElement)
  else
    Exclude(W, FElement);
  FParent.SetOrdValue(PLongInt(@W)^);
end;

end.

