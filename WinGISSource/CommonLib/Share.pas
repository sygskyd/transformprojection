Unit Share;

Interface

Function IsFileWriteLocked(const FileName:String):Boolean;

Function LockFileWrites(const FileName:String):Integer;

Procedure UnlockFileWrites(FileHandle:Integer);

Implementation

Uses SysUtils,Windows;

Function IsFileWriteLocked(const FileName:String):Boolean;
var Handle         : Integer;
    Error          : Integer;    
begin
  Result:=FALSE;
  Handle:=FileOpen(FileName,fmOpenWrite or fmShareDenyNone);
  if Handle>=0 then FileClose(Handle)
  else begin
    Error:=GetLastError;           
    Result:=(Error=ERROR_SHARING_VIOLATION) or (Error=ERROR_ACCESS_DENIED);
  end;  
end;

Function LockFileWrites(const FileName:String):Integer;
begin
  Result:=FileOpen(FileName,fmOpenRead or fmShareDenyWrite);
end;

Procedure UnlockFileWrites(FileHandle:Integer);
begin
  if FileHandle>=0 then FileClose(FileHandle);
end;

end.
 