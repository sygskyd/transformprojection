Unit PaletteDialog;

Interface

Uses WinProcs,WinTypes,Classes,Combobtn,Controls,Dialogs,DipGrid,ExtCtrls,Forms,
     Graphics,Menus,Palette,Spinbtn,StdCtrls,Validate,XYSelect, WCtrls,
  MultiLng;

Type TPaletteDialog = class(TWForm)
       AddColorMenu     : TMenuItem;
       BEdit            : TEdit;
       BLabel           : TLabel;
       BSpin            : TSpinBtn;
       BVal             : TIntValidator;
       CancelBtn        : TButton;
       ColorGrid        : TDipList;
       ColorModelCombo  : TComboBox;
       ColorNameEdit    : TEdit;
       CurrentColorWindow: TWindow;
       DeleteColorMenu  : TMenuItem;
       GEdit            : TEdit;
       GLabel           : TLabel;
       Group1           : TGroupBox;
       GSpin            : TSpinBtn;
       GVal             : TIntValidator;
       HSSelector       : TXYSelector;
       KEdit            : TEdit;
       InsertMenu       : TMenuItem;
       KLabel           : TLabel;
       KSpin            : TSpinBtn;
       KVal             : TIntValidator;
       Label1           : TLabel;
       MlgSection       : TMlgSection;
       N1               : TMenuItem;
       NewPalette       : TMenuItem;
       OkBtn            : TButton;
       OpenDialog       : TOpenDialog;
       OpenPalette      : TMenuItem;
       OriginalColorWindow: TWindow;
       PaletteMenu      : TPopupMenu;
       PaletteMenuBtn   : TPopupMenuBtn;
       Panel            : TPanel;
       ReplaceColorMenu : TMenuItem;
       REdit            : TEdit;
       RenameMenu       : TMenuItem;
       RLabel           : TLabel;
       RSpin            : TSpinBtn;
       RVal             : TIntValidator;
       SaveDialog       : TSaveDialog;
       SavePalette      : TMenuItem;
       ToggleBtn        : TButton;
       VSelector        : TXYSelector;
       Procedure   AddColorMenuClick(Sender: TObject);
       Procedure   ChangeScale(M,D:Integer); override;
       Procedure   ColorGridClick(Sender: TObject);
       Procedure   ColorGridDblClick(Sender: TObject);
       Procedure   ColorGridDragOver(Sender, Source: TObject; X, Y: Integer;
                       State: TDragState; var Accept: Boolean);
       Procedure   ColorGridDrawCell(Sender: TObject; Canvas: TCanvas; Index: Integer;
                       Rect: TRect);
       Procedure   ColorGridDragDrop(Sender, Source: TObject; X, Y: Integer);
       Procedure   ColorGridMouseDown(Sender: TObject; Button: TMouseButton;
                     Shift: TShiftState; X, Y: Integer);
       Procedure   ColorGridMouseMove(Sender: TObject; Shift: TShiftState; X,
                       Y: Integer);
       Procedure   ColorInputChange(Sender: TObject);
       Procedure   ColorModelComboClick(Sender: TObject);
       Procedure   CurrentColorWindowPaint(Sender: TObject);
       Procedure   DeleteColorMenuClick(Sender: TObject);
       Procedure   FormCreate(Sender: TObject);
       Procedure   FormDestroy(Sender: TObject);
       Procedure   FormShow(Sender: TObject);
       Procedure   FormHide(Sender: TObject);
       Procedure   HSSelectorChange(Sender: TObject);
       Procedure   HSSelectorDblClick(Sender: TObject);
       Procedure   HSSelectorPaint(Sender: TObject);
       Procedure   InsertMenuClick(Sender: TObject);
       Procedure   NewPaletteClick(Sender: TObject);
       Procedure   OpenPaletteClick(Sender: TObject);
       Procedure   OriginalColorWindowPaint(Sender: TObject);
       Procedure   OriginalColorWindowClick(Sender: TObject);
       Procedure   RenameMenuClick(Sender: TObject);
       Procedure   ReplaceColorMenuClick(Sender: TObject);
       Procedure   SavePaletteClick(Sender: TObject);
       Procedure   SelectorChange(Sender: TObject);
       Procedure   ToggleBtnClick(Sender: TObject);
       Procedure   VSelectorPaint(Sender: TObject);
      Private
       FColorModel      : TPaletteType;
       FCurrentColor    : TColor;
       FDragIndex       : Integer;
       FLastIndex       : Integer;
       FOriginalColor   : TColor;
       FPalette         : TPalette;
       FShowAsList      : Boolean;
       Function    ContainsColor(AColor:Integer;Const AName:String):Boolean;
       Procedure   SetPaletteType(AModel:TPaletteType);
       Procedure   SetPalette(APalette:TPalette);
       Procedure   SetSelectedColor(AColor:TColor);
       Procedure   SetShowAsList(AList:Boolean);
       Procedure   UpdateCaption;
       Procedure   UpdateColorEdits;
       Procedure   UpdateEnabled;
      Public
       Property    ColorModel:TPaletteType read FColorModel write SetPaletteType;
       Property    OriginalColor:TColor read FOriginalColor write FOriginalColor;
       Property    Palette:TPalette read FPalette write SetPalette;
       Property    SelectedColor:TColor read FCurrentColor write SetSelectedColor;
       Property    ShowAsList:Boolean read FShowAsList write SetShowAsList default False;
     end;

Implementation

{$R *.DFM}

Uses NumTools,SysUtils;

Const FLastColorModel : TPaletteType = ptRGB;      { last color model selected }

var FBitmap           : TBitmap;

{===============================================================================
| TColorDialog
+==============================================================================}

Procedure TPaletteDialog.SetPalette
   (
   APalette        : TPalette
   );
  begin
    FPalette.Assign(APalette);
    ColorGrid.Count:=FPalette.Count;
  end;

Procedure TPaletteDialog.ColorGridDrawCell(Sender: TObject; Canvas: TCanvas; Index: Integer;
    Rect: TRect);
  var ARect        : TRect;
  begin
    with Canvas do begin
      if FShowAsList then begin
        Brush.Color:=clWindow;
        FillRect(Rect);
        ARect:=Rect;
        ARect.Left:=Rect.Left+2*ColorGrid.RowHeight+2;
        TextRect(ARect,ARect.Left,CenterHeightInRect(TextHeight('A'),ARect),
            Palette.ColorNames[Index]);
        ARect:=Rect;
        ARect.Right:=ARect.Left+2*ColorGrid.RowHeight;
        InflateRect(ARect,-1,-1);
        Brush.Color:=Palette[Index];
        FillRect(ARect);
      end
      else begin
        Brush.Color:=Palette[Index];
        FillRect(Rect);
      end;
    end;
  end;

Procedure TPaletteDialog.ColorModelComboClick(Sender: TObject);
  var AVisible     : Boolean;
      AMaxValue    : Integer;
  begin
    FColorModel:=TPalette.NameToPaletteType(ColorModelCombo.Items[ColorModelCombo.ItemIndex]);
    case FColorModel of
      ptRGB : begin
          RLabel.Caption:='R:'; RVal.MaxValue:=255; RSpin.Max:=255;
          GLabel.Caption:='G:'; GVal.MaxValue:=255; GSpin.Max:=255;
          BLabel.Caption:='B:'; BVal.MaxValue:=255; BSpin.Max:=255;
        end;
      ptCMY,
      ptCMYK255,
      ptCMYK : begin
          if FColorModel=ptCMYK then AMaxValue:=100
          else AMaxValue:=255;
          RLabel.Caption:='C:'; RVal.MaxValue:=AMaxValue; RSpin.Max:=AMaxValue;
          GLabel.Caption:='M:'; GVal.MaxValue:=AMaxValue; GSpin.Max:=AMaxValue;
          BLabel.Caption:='Y:'; BVal.MaxValue:=AMaxValue; BSpin.Max:=AMaxValue;
          KLabel.Caption:='K:'; KVal.MaxValue:=AMaxValue; KSpin.Max:=AMaxValue;
        end;
      ptGrayScale : begin
          RLabel.Caption:='G:';
          RVal.MaxValue:=255; RSpin.Max:=255;
        end;
      ptHSV  : begin
          RLabel.Caption:='H:'; RVal.MaxValue:=360; RSpin.Max:=360;
          GLabel.Caption:='S:'; GVal.MaxValue:=100; GSpin.Max:=100;
          BLabel.Caption:='V:'; BVal.MaxValue:=100; BSpin.Max:=100;
        end;
    end;
    AVisible:=ColorModel in [ptRGB,ptCMY,ptCMYK,ptCMYK255,ptHSV];
    SetControlVisible(GEdit,AVisible);
    SetControlVisible(BEdit,AVisible);
    AVisible:=ColorModel in [ptCMYK,ptCMYK255];
    KLabel.Visible:=AVisible;
    SetControlVisible(KEdit,AVisible);
    UpdateColorEdits;
  end;

Procedure TPaletteDialog.ColorGridClick(Sender: TObject);
  begin
    if ColorGrid.ItemIndex>=0 then begin
      SetSelectedColor(FPalette[ColorGrid.ItemIndex]);
      if ColorGrid.ItemIndex<0 then ColorNameEdit.Text:=''
      else ColorNameEdit.Text:=FPalette.ColorNames[ColorGrid.ItemIndex];
    end;
    UpdateEnabled;
  end;

Procedure TPaletteDialog.SetPaletteType
   (
   AModel          : TPaletteType
   );
  begin
    with ColorModelCombo do ItemIndex:=Items.IndexOf(TPalette.PaletteTypeToName(AModel));
  end;

Procedure TPaletteDialog.OpenPaletteClick(Sender: TObject);
  var APalette     : TPalette;
  begin
    OpenDialog.FileName:='';
    if OpenDialog.Execute then begin
      APalette:=TPalette.Create; 
      try
        APalette.ReadFromFile(OpenDialog.FileName);
        FPalette.Assign(APalette);
        ColorGrid.Count:=FPalette.Count;
        ColorGrid.Invalidate;
        UpdateCaption;
      finally
        APalette.Free;
      end;  
    end;
    UpdateEnabled;
  end;

Procedure TPaletteDialog.SavePaletteClick(Sender: TObject);
  begin
    SaveDialog.FileName:='';
    if SaveDialog.Execute then begin
      Update;
      Screen.Cursor:=crHourGlass;
      try
        FPalette.WriteToFile(SaveDialog.FileName,ptRGB);
        UpdateCaption;
      finally
        Screen.Cursor:=crDefault;
      end;
    end;
  end;

Procedure TPaletteDialog.NewPaletteClick(Sender: TObject);
  begin
    ColorGrid.Count:=0;
    FPalette.Clear;
    FPalette.FileName:=MlgSection[40];
    UpdateCaption;
    UpdateEnabled;
  end;

Procedure TPaletteDialog.FormCreate(Sender: TObject);
  var Cnt          : TPaletteType;
  begin
    FPalette:=TPalette.Create;
    with ColorModelCombo do for Cnt:=Low(Cnt) to High(Cnt) do
        if Cnt<>ptGrayscale then Items.Add(TPalette.PaletteTypeToName(Cnt));
    ColorModel:=FLastColorModel;
  end;

Procedure TPaletteDialog.FormDestroy(Sender: TObject);
  begin
    FPalette.Free;
  end;

Procedure TPaletteDialog.HSSelectorPaint(Sender: TObject);
  var ARect        : TRect;
  begin
    with HSSelector do begin
      ARect:=ClientRect;
      InflateRect(ARect,1,1);
      Frame3D(Canvas,ARect,clBtnShadow,clBtnHighlight,1);
      Canvas.Draw(ARect.Left,ARect.Top,FBitmap);
    end;  
  end;

Procedure TPaletteDialog.SetSelectedColor
   (
   AColor          : TColor
   );
  var H            : Integer;
      S            : Integer;
      V            : Integer;
  begin
    FCurrentColor:=AColor;
    TPalette.ColorToHSV(FCurrentColor,H,S,V);
    HSSelector.XPosition:=H;
    HSSelector.YPosition:=100-S;
    VSelector.YPosition:=100-V;
    UpdateColorEdits;
    CurrentColorWindow.Invalidate;
  end;

Procedure TPaletteDialog.FormShow(Sender: TObject);
  Const Resolution = 5;
  var H            : Integer;
      S            : Integer;
      ARect        : TRect;
      XSize        : Double;
      YSize        : Double;
  begin
    S:=HSSelector.Width-2*HSSelector.SelectorSize;
    H:=HSSelector.Height-2*HSSelector.SelectorSize;
    { recreate color-selector-bitmap if size has changed }
    if (FBitmap.Width<>S) or (FBitmap.Height<>H) then begin
      Screen.Cursor:=crHourGlass;
      try
        FBitmap.Width:=S;
        FBitmap.Height:=H;
        with FBitmap do begin
          XSize:=Width*Resolution/360;
          YSize:=Height*Resolution/100;
          for S:=0 to 100 Div Resolution-1 do begin
            for H:=0 to 360 Div Resolution-1 do begin
              ARect:=Rect(Round(H*XSize),Round(Height-S*YSize),
                  Round((H+1)*XSize),Round(Height-(S+1)*YSize));
              Canvas.Brush.Color:=TPalette.HSVToColor(H*Resolution,S*Resolution,100);
              Canvas.FillRect(ARect);
            end;
          end;
        end;
      finally
        Screen.Cursor:=crDefault;
      end;
    end;
    ColorModelComboClick(ColorModelCombo);
    FCurrentColor:=FOriginalColor;
    ColorGrid.ItemIndex:=FPalette.IndexOf(FCurrentColor);
    ColorGridClick(ColorGrid);
    UpdateCaption;
    UpdateEnabled;
  end;

Procedure TPaletteDialog.UpdateCaption;
  begin
    Caption:=MlgSection[1]+' - '+ExtractFileName(FPalette.FileName);
  end;

Procedure TPaletteDialog.UpdateColorEdits;
  var Values       : Array[0..3] of Integer;
  begin
    FillChar(Values,SizeOf(Values),#0);
    case FColorModel of
      ptRGB       : TPalette.ColorToRGB(FCurrentColor,Values[0],Values[1],Values[2]);
      ptCMY       : TPalette.ColorToCMY(FCurrentColor,Values[0],Values[1],Values[2]);
      ptCMYK255   : TPalette.ColorToCMYK255(FCurrentColor,Values[0],Values[1],Values[2],Values[3]);
      ptCMYK      : TPalette.ColorToCMYK(FCurrentColor,Values[0],Values[1],Values[2],Values[3]);
      ptGrayScale : TPalette.ColorToGrayScale(FCurrentColor,Values[0]);
      ptHSV       : TPalette.ColorToHSV(FCurrentColor,Values[0],Values[1],Values[2]);
    end;
    RVal.AsInteger:=Values[0];
    GVal.AsInteger:=Values[1];
    BVal.AsInteger:=Values[2];
    KVal.AsInteger:=Values[3];
  end;

Procedure TPaletteDialog.SelectorChange(Sender: TObject);
  var H            : Integer;
      S            : Integer;
      V            : Integer;
  begin
    H:=Round(HSSelector.XPosition);
    S:=100-Round(HSSelector.YPosition);
    V:=100-Round(VSelector.YPosition);
    FCurrentColor:=TPalette.HSVToColor(H,S,V);
    ColorNameEdit.Text:='';
    UpdateColorEdits;
    CurrentColorWindow.Invalidate;
  end;

Procedure TPaletteDialog.CurrentColorWindowPaint(Sender: TObject);
  var ARect        : TRect;
  begin
    with CurrentColorWindow do begin
      ARect:=ClientRect;
      Frame3D(Canvas,ARect,clBtnShadow,clBtnHighlight,1);
      Canvas.Brush.Color:=FCurrentColor;
      Canvas.FillRect(ARect);
    end;
  end;

Procedure TPaletteDialog.ColorInputChange(Sender: TObject);
  var Color        : TColor;
  begin
    case FColorModel of
      ptRGB       : Color:=TPalette.RGBToColor(RVal.AsInteger,GVal.AsInteger,BVal.AsInteger);
      ptCMY       : Color:=TPalette.CMYToColor(RVal.AsInteger,GVal.AsInteger,BVal.AsInteger);
      ptCMYK255   : Color:=TPalette.CMYK255ToColor(RVal.AsInteger,GVal.AsInteger,BVal.AsInteger,KVal.AsInteger);
      ptCMYK      : Color:=TPalette.CMYKToColor(RVal.AsInteger,GVal.AsInteger,BVal.AsInteger,KVal.AsInteger);
      ptGrayScale : Color:=TPalette.GrayScaleToColor(RVal.AsInteger);
      ptHSV       : Color:=TPalette.HSVToColor(RVal.AsInteger,GVal.AsInteger,BVal.AsInteger);
      else Color:=clBlack;
    end;
    SetSelectedColor(Color);
  end;

Procedure TPaletteDialog.OriginalColorWindowPaint(Sender: TObject);
  var ARect        : TRect;
  begin
    with OriginalColorWindow do begin
      ARect:=ClientRect;
      Frame3D(Canvas,ARect,clBtnShadow,clBtnHighlight,1);
      Canvas.Brush.Color:=FOriginalColor;
      Canvas.FillRect(ARect);
    end;
  end;

Procedure TPaletteDialog.OriginalColorWindowClick(Sender: TObject);
  begin
    SetSelectedColor(FOriginalColor);
  end;

Procedure TPaletteDialog.VSelectorPaint(Sender: TObject);
  Const Resolution = 4;
  var ARect        : TRect;
      BRect        : TRect;
      H            : Integer;
      S            : Integer;
      V            : Integer;
      YSize        : Double;
  begin
    with VSelector,Canvas do begin
      ARect:=ClientRect;
      InflateRect(ARect,-2,1);
      Frame3D(Canvas,ARect,clBtnShadow,clBtnHighlight,1);
      BRect:=ARect;
      H:=Round(HSSelector.XPosition);
      S:=100-Round(HSSelector.YPosition);
      YSize:=(ARect.Bottom-ARect.Top)*Resolution/100;
      for V:=0 to 100 Div Resolution-1 do begin
        BRect.Top:=ARect.Top+Round(V*YSize);
        BRect.Bottom:=ARect.Top+Round((V+1)*YSize);
        Brush.Color:=TPalette.HSVToColor(H,S,100-V*Resolution);
        FillRect(BRect);
      end;
    end;
  end;

Procedure TPaletteDialog.HSSelectorChange(Sender: TObject);
  begin
    SelectorChange(HSSelector);
    VSelector.Invalidate;
  end;

Procedure TPaletteDialog.AddColorMenuClick(Sender: TObject);
  begin
    if not ContainsColor(FCurrentColor,ColorNameEdit.Text) then
        with ColorGrid do begin
      FPalette.Add(ColorNameEdit.Text,FCurrentColor);
      Count:=FPalette.Count;
      ItemIndex:=FPalette.Count-1;
      InvalidateDip(ItemIndex);
    end;
    UpdateEnabled;
  end;

Procedure TPaletteDialog.ReplaceColorMenuClick(Sender: TObject);
  var AIndex       : Integer;
      BIndex       : Integer;
  begin
    AIndex:=FPalette.IndexOf(FCurrentColor);
    BIndex:=FPalette.IndexOfName(ColorNameEdit.Text);
    with ColorGrid do if (AIndex>=0) and (AIndex<>ItemIndex) then
        MessageDlg(MlgSection[41],mtInformation,[mbOK],0)
    else if (BIndex>=0) and (BIndex<>ItemIndex) then
        MessageDialog(MlgSection[42],mtInformation,[mbOK],0)
    else begin
      FPalette[ColorGrid.ItemIndex]:=FCurrentColor;
      ColorGrid.InvalidateDip(ColorGrid.ItemIndex);
    end;
  end;

Procedure TPaletteDialog.UpdateEnabled;
  var AEnable      : Boolean;
  begin
    AEnable:=(FPalette.Count>0) and (ColorGrid.ItemIndex>=0);
    DeleteColorMenu.Enabled:=AEnable;
    ReplaceColorMenu.Enabled:=AEnable;
    RenameMenu.Enabled:=AEnable;
  end;

Procedure TPaletteDialog.HSSelectorDblClick(Sender: TObject);
  begin
    InsertMenuClick(HSSelector);
  end;

Procedure TPaletteDialog.ColorGridDragOver(Sender, Source: TObject; X,
    Y: Integer; State: TDragState; var Accept: Boolean);
  begin
    Accept:=(Source=ColorGrid) and (ColorGrid.ItemAtPos(Point(X,Y),TRUE)>=0);
  end;

Procedure TPaletteDialog.ColorGridMouseDown(Sender: TObject;
   Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
  begin
    if Button=mbLeft then begin
      FDragIndex:=ColorGrid.ItemAtPos(Point(X,Y),TRUE);
      if FDragIndex>=0 then ColorGrid.BeginDrag(FALSE);
    end;  
  end;

Procedure TPaletteDialog.ColorGridDragDrop(Sender, Source: TObject; X,
    Y: Integer);
  var FDestIndex   : Integer;  
  begin
    FDestIndex:=ColorGrid.ItemAtPos(Point(X,Y),TRUE);
    if FDragIndex*FDestIndex>=0 then begin
      if FDragIndex<FDestIndex then begin
        FPalette.Insert(FDestIndex+1,FPalette.ColorNames[FDragIndex],FPalette[FDragIndex]);
        FPalette.Delete(FDragIndex);
        ColorGrid.ItemIndex:=FDestIndex;
      end
      else begin
        FPalette.Insert(FDestIndex,FPalette.ColorNames[FDragIndex],FPalette[FDragIndex]);
        FPalette.Delete(FDragIndex+1);
        ColorGrid.ItemIndex:=FDestIndex;
      end;
      ColorGrid.Invalidate;
    end;
  end;

Procedure TPaletteDialog.DeleteColorMenuClick(Sender: TObject);
  begin
    FPalette.Delete(ColorGrid.ItemIndex);
    Colorgrid.Count:=FPalette.Count;
    ColorGrid.Invalidate;
    UpdateEnabled;
  end;

Procedure TPaletteDialog.FormHide(Sender: TObject);
  begin
    if ModalResult=mrOK then begin
      FLastColorModel:=ColorModel;
      if FPalette.FileName<>'' then try
        Screen.Cursor:=crHourGlass;
        if FPalette.Modified then FPalette.WriteToFile(FPalette.FileName,ptRGB);
      finally
        Screen.Cursor:=crDefault;  
      end;  
    end;  
  end;

{*******************************************************************************
| Procedure TColorDialog.ChangeScale
|-------------------------------------------------------------------------------
| Called when scaling dialogs because of font or resolution-change. Adapts
| size of group-box so that palette fits in.
+******************************************************************************}
Procedure TPaletteDialog.ChangeScale
   (
   M               : Integer;
   D               : Integer
   );
  begin
    inherited ChangeScale(M,D);
    M:=ColorGrid.Height+16+ToggleBtn.Height+PaletteMenuBtn.Height;
    if Group1.Height<M then Group1.Height:=M;
    M:=ColorGrid.Width+16;
    if Group1.Width<M then Group1.Width:=M;
    ColorGrid.SetBounds((Group1.Width-ColorGrid.Width) Div 2,
      (Group1.Height-ColorGrid.Height+Group1.Font.Size-
      ToggleBtn.Height-PaletteMenuBtn.Height-8) Div 2,
      ColorGrid.Width,ColorGrid.Height);
  end;

Procedure TPaletteDialog.SetShowAsList
   (
   AList           : Boolean
   );
  var NewColCount  : Integer;
  begin
    if FShowAsList<>AList then with ColorGrid do begin
      FShowAsList:=AList;
      if FShowAsList then NewColCount:=1
      else NewColCount:=8;
      CellWidth:=(ClientWidth-1) Div NewColCount;
      ColCount:=NewColCount;
      ShowHint:=not FShowAsList;
    end;
  end;

Procedure TPaletteDialog.ToggleBtnClick(Sender: TObject);
  begin
    SetShowAsList(not FShowAsList);
  end;

Procedure TPaletteDialog.ColorGridDblClick(Sender: TObject);
  begin
    ModalResult:=mrOK;
  end;

Procedure TPaletteDialog.ColorGridMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
  var Index        : Integer;
  begin
    Index:=ColorGrid.ItemAtPos(Point(X,Y),TRUE);
    if Index<>FLastIndex then begin
      Application.CancelHint;
      FLastIndex:=Index;
      if Index>=0 then ColorGrid.Hint:=Palette.ColorNames[Index]
      else ColorGrid.Hint:='';
    end;
  end;

Procedure TPaletteDialog.InsertMenuClick(Sender: TObject);
  var AIndex       : Integer;
  begin
    if not ContainsColor(FCurrentColor,ColorNameEdit.Text) then
        with ColorGrid do begin
      AIndex:=ItemIndex;
      if AIndex<0 then AIndex:=0;
      FPalette.Insert(AIndex,ColorNameEdit.Text,FCurrentColor);
      Count:=FPalette.Count;
      ItemIndex:=AIndex;
      Invalidate;
    end;
    UpdateEnabled;
  end;

Procedure TPaletteDialog.RenameMenuClick(Sender: TObject);
  var AIndex       : Integer;
  begin
    AIndex:=FPalette.IndexOfName(ColorNameEdit.Text);
    with ColorGrid do if ItemIndex>=0 then begin
      if (AIndex>=0) and (AIndex<>ItemIndex) then
          MessageDialog(MlgSection[42],mtInformation,[mbOK],0)
      else begin
        FPalette.ColorNames[ItemIndex]:=ColorNameEdit.Text;
        InvalidateDip(ItemIndex);
      end;
    end;
  end;

Function TPaletteDialog.ContainsColor
   (
   AColor          : Integer;
   Const AName     : String
   )
   : Boolean;
  var AIndex       : Integer;
      BIndex       : Integer;
  begin
    Result:=TRUE;
    AIndex:=FPalette.IndexOf(AColor);
    BIndex:=FPalette.IndexOfName(AName);
    if AIndex>=0 then MessageDialog(MlgSection[41],mtInformation,[mbOK],0)
    else if BIndex>=0 then MessageDialog(MlgSection[42],mtInformation,[mbOK],0)
    else Result:=FALSE;
  end;

{===============================================================================
| initialization and exit-code of the unit
+==============================================================================}

Initialization
  begin
    FBitmap:=TBitmap.Create;
  end;

Finalization
  begin
    FBitmap.Free;
  end;

end.
