{==============================================================================+
  Unit Palette
--------------------------------------------------------------------------------
  Autor: Martin Forst
--------------------------------------------------------------------------------
  - TPalette-Class
  - color conversion routines
+==============================================================================}
unit Palette;

interface

uses WinProcs, WinTypes, Classes, Controls, Graphics, RegDB, SysUtils;

const
  erOpenFile = 1;
  erFileType = 2;
  erFileFormat = 3;
  erPaletteType = 4;

type
  EPaletteException = class(Exception)
    ErrorNumber: Integer;
    constructor Create(const ErrorText: string; AErrorNumber: Integer);
  end;

  TPaletteType = (ptRGB, ptHSV, ptCMY, ptCMYK, ptCMYK255, ptGrayScale);

type
  TPalette = class(TPersistent)
  private
    FFileName: string;
    FList: TStringList;
    FModified: Boolean;
    function GetCount: Integer;
    function GetColor(AIndex: Integer): TColor;
    function GetColorName(AIndex: Integer): string;
    procedure SetColor(AIndex: Integer; AColor: TColor);
    procedure SetColorName(AIndex: Integer; const Name: string);
  protected
    procedure AssignTo(Dest: TPersistent); override;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Add(const Name: string; AColor: TColor);
    procedure Clear;
    property Count: Integer read GetCount;
    property Colors[AIndex: Integer]: TColor read GetColor write SetColor; default;
    property ColorNames[AIndex: Integer]: string read GetColorName write SetColorName;
    procedure Delete(AIndex: Integer);
    property FileName: string read FFileName write FFileName;
    function IndexOf(AColor: TColor): Integer;
    function IndexOfName(const AName: string): Integer;
    procedure Insert(AIndex: Integer; const Name: string; AColor: TColor);
    property Modified: Boolean read FModified write FModified;
    procedure ReadFromFile(const FileName: string);
    function ReadFromRegistry(Registry: TRegistryDatabase; const KeyName: string): Boolean;
    procedure WriteToFile(const FileName: string; AType: TPaletteType);
    procedure WriteToRegistry(Registry: TRegistryDatabase; const KeyName: string);
       { color conversion functions }
    class procedure ColorToRGB(AColor: TColor; var R, G, B: Integer);
    class procedure ColorToCMY(AColor: TColor; var C, M, Y: Integer);
    class procedure ColorToCMYK(AColor: TColor; var C, M, Y, K: Integer);
    class procedure ColorToCMYK255(AColor: TColor; var C, M, Y, K: Integer);
    class procedure ColorToHSV(AColor: TColor; var H, S, V: Integer);
    class procedure ColorToGrayScale(AColor: TColor; var GrayLevel: Integer);
    class function RGBToColor(R, G, B: Integer): TColor;
    class function CMYToColor(C, M, Y: Integer): TColor;
    class function CMYKToColor(C, M, Y, K: Integer): TColor;
    class function CMYK255ToColor(C, M, Y, K: Integer): TColor;
    class function HSVToColor(H, S, V: Integer): TColor;
    class function GrayScaleToColor(GrayLevel: Integer): TColor;
    class function PaletteTypeToName(AType: TPaletteType): string;
    class function NameToPaletteType(const AName: string): TPaletteType;
    class function PaletteTypeToIdent(AType: TPaletteType): string;
    class function IdentToPaletteType(const AIdent: string; var AType: TPaletteType): Boolean;
  end;

implementation

uses IniFiles, MultiLng, NumTools, StrTools;

const
  RGBColors: array[0..15] of TColor = (
    $000000FF, $00000080, $00FF00FF, $00800080, $00FF0000,
    $00800000, $00FFFF00, $00808000, $0000FF00, $00008000,
    $0000FFFF, $00008080, $00FFFFFF, $00C0C0C0, $00808080,
    $00000000);

  StdPalette: array[0..255] of TColor = (
    $00000000, $00000C3F, $0000183F, $0000243F, $0000303F, $00003F3F,
    $00003F30, $00003F24, $00003F18, $00003F0C, $00003F00, $000C3F00,
    $00183F00, $00243F00, $00303F00, $003F3F00, $003F3000, $003F2400,
    $003F1800, $003F0C00, $003F0000, $003F000A, $003F0014, $003F001E,
    $003F0028, $003F0032, $003F003F, $0030003F, $0024003F, $0018003F,
    $000C003F, $0000003F,
    $0000007E, $0000197E, $0000327E, $00004B7E, $0000647E, $00007E7E,
    $00007E64, $00007E4B, $00007E32, $00007E19, $00007E00, $00197E00,
    $00327E00, $004B7E00, $00647E00, $007E7E00, $007E6400, $007E4B00,
    $007E3200, $007E1900, $007E0000, $007E0015, $007E002A, $007E003F,
    $007E0054, $007E0069, $007E007E, $0064007E, $004B007E, $0032007E,
    $0019007E, $0000007E,
    $000000BD, $000025BD, $00004ABD, $00006FBD, $000094BD, $0000BDBD,
    $0000BD94, $0000BD6F, $0000BD4A, $0000BD25, $0000BD00, $0025BD00,
    $004ABD00, $006FBD00, $0094BD00, $00BDBD00, $00BD9400, $00BD6F00,
    $00BD4A00, $00BD2500, $00BD0000, $00BD001F, $00BD003E, $00BD005D,
    $00BD007C, $00BD009B, $00BD00BD, $009400BD, $006F00BD, $004A00BD,
    $002500BD, $000000BD,
    $000000FF, $000033FF, $000066FF, $000099FF, $0000CCFF, $0000FFFF,
    $0000FFCC, $0000FF99, $0000FF66, $0000FF33, $0000FF00, $0033FF00,
    $0066FF00, $0099FF00, $00CCFF00, $00FFFF00, $00FFCC00, $00FF9900,
    $00FF6600, $00FF3300, $00FF0000, $00FF002A, $00FF0054, $00FF007E,
    $00FF00A8, $00FF00D2, $00FF00FF, $00CC00FF, $009900FF, $006600FF,
    $003300FF, $000000FF,
    $003333FF, $00335BFF, $003383FF, $0033ABFF, $0033D3FF, $0033FFFF,
    $0033FFD3, $0033FFAB, $0033FF83, $0033FF5B, $0033FF33, $005BFF33,
    $0083FF33, $00ABFF33, $00D3FF33, $00FFFF33, $00FFD333, $00FFAB33,
    $00FF8333, $00FF5B33, $00FF3333, $00FF3355, $00FF3377, $00FF3399,
    $00FF33BB, $00FF33DD, $00FF33FF, $00D333FF, $00AB33FF, $008333FF,
    $005B33FF, $003333FF,
    $006666FF, $006684FF, $0066A2FF, $0066C0FF, $0066DEFF, $0066FFFF,
    $0066FFDE, $0066FFC0, $0066FFA2, $0066FF84, $0066FF66, $0084FF66,
    $00A2FF66, $00C0FF66, $00DEFF66, $00FFFF66, $00FFDE66, $00FFC066,
    $00FFA266, $00FF8466, $00FF6666, $00FF667F, $00FF6698, $00FF66B1,
    $00FF66CA, $00FF66E3, $00FF66FF, $00DE66FF, $00C066FF, $00A266FF,
    $008466FF, $006666FF,
    $009999FF, $0099ADFF, $0099C1FF, $0099D5FF, $0099E9FF, $0099FFFF,
    $0099FFE9, $0099FFD5, $0099FFC1, $0099FFAD, $0099FF99, $00ADFF99,
    $00C1FF99, $00D5FF99, $00E9FF99, $00FFFF99, $00FFE999, $00FFD599,
    $00FFC199, $00FFAD99, $00FF9999, $00FF99AA, $00FF99BB, $00FF99CC,
    $00FF99DD, $00FF99EE, $00FF99FF, $00E999FF, $00D599FF, $00C199FF,
    $00AD99FF, $009999FF,
    $00CCCCFF, $00CCD6FF, $00CCE0FF, $00CCEAFF, $00CCF4FF, $00CCFFFF,
    $00CCFFF4, $00CCFFEA, $00CCFFE0, $00CCFFD6, $00CCFFCC, $00D6FFCC,
    $00E0FFCC, $00EAFFCC, $00F4FFCC, $00FFFFCC, $00FFF4CC, $00FFEACC,
    $00FFE0CC, $00FFD6CC, $00FFCCCC, $00FFCCD4, $00FFCCDC, $00FFCCE4,
    $00FFCCEC, $00FFCCF4, $00FFCCFF, $00F4CCFF, $00EACCFF, $00E0CCFF,
    $00D6CCFF, $00FFFFFF);

{===============================================================================
| TPalette
+==============================================================================}

constructor TPalette.Create;
var
  Cnt: Integer;
  R: Integer;
  G: Integer;
  B: Integer;
begin
  inherited Create;
  FList := TStringList.Create;
  for Cnt := 0 to 15 do
  begin
    ColorTORGB(RGBColors[Cnt], R, G, B);
    Add('R' + IntToStr(R) + 'G' + IntToStr(G) + 'B' + IntToStr(B), RGBColors[Cnt]);
  end;
  for Cnt := 0 to 255 do
    if IndexOf(StdPalette[Cnt]) = -1 then
    begin
      ColorTORGB(StdPalette[Cnt], R, G, B);
      Add('R' + IntToStr(R) + 'G' + IntToStr(G) + 'B' + IntToStr(B), StdPalette[Cnt]);
    end;
end;

destructor TPalette.Destroy;
begin
  FList.Free;
  inherited Destroy;
end;

function TPalette.GetCount
  : Integer;
begin
  Result := FList.Count;
end;

function TPalette.GetColor(AIndex: Integer): TColor;
begin
  Result := TColor(FList.Objects[AIndex]);
end;

procedure TPalette.SetColor(AIndex: Integer; AColor: TColor);
begin
  if FList.Objects[AIndex] <> Pointer(AColor) then
    FModified := TRUE;
  FList.Objects[AIndex] := Pointer(AColor);
end;

procedure TPalette.Add(const Name: string; AColor: TColor);
begin
  FList.AddObject(Name, Pointer(AColor));
  FModified := TRUE;
end;

procedure TPalette.Insert(AIndex: Integer; const Name: string; AColor: TColor);
begin
  FList.InsertObject(AIndex, Name, Pointer(AColor));
  FModified := TRUE;
end;

function TPalette.IndexOf(AColor: TColor): Integer;
begin
  Result := FList.IndexOfObject(Pointer(AColor));
end;

function TPalette.IndexOfName(const AName: string): Integer;
begin
  Result := FList.IndexOf(AName);
end;

procedure TPalette.ReadFromFile(const FileName: string);
var
  IniFile: TIniFile;
  APalette: TPalette;
  AType: TPaletteType;
  Cnt: Integer;
  ColorCnt: Integer;
  Color: TColor;
  ReadStr: string;
  Name: string;
  Values: array[0..3] of Integer;
  Entries: Integer;
  NeededEntries: Integer;

  function ParseString: Integer;
  var
    Strings: array[0..4] of string;
    Cnt: Integer;
    APos: Integer;
  begin
    Result := SplitStringList(ReadStr, '''', ',', Strings);
    Name := Strings[0];
    for Cnt := 1 to Result - 1 do
    begin
      APos := Pos('0x', Strings[Cnt]);
      try
        if APos <> 0 then
        begin
          System.Delete(Strings[Cnt], APos, 2);
          Values[Cnt - 1] := StrToInt('$' + Strings[Cnt]);
        end
        else
          Values[Cnt - 1] := StrToInt(Strings[Cnt]);
      except
        on E: EConvertError do
          raise EPaletteException.Create(
            Format(MlgStringList['Palette', 22], [FileName, 'Color' + IntToStr(Cnt)]),
            erFileFormat);
      else
        raise;
      end;
    end;
  end;
begin
  if not FileExists(ExpandFileName(FileName)) then
    raise EPaletteException.Create(
      Format(MlgStringList['Palette', 24], [FileName]), erOpenFile);
  IniFile := TIniFile.Create(ExpandFileName(FileName));
  try
    APalette := TPalette.Create;
    try
      APalette.Clear;
      { check file-type entry }
      if CompareText(IniFile.ReadString('FileInfo', 'FileType', ''),
        'Named-Palette-File') <> 0 then
        raise EPaletteException.Create(Format(
          MlgStringList['Palette', 20], [FileName]), erFileType);
      { check palette-type-entry}
      ReadStr := IniFile.ReadString('PaletteInfo', 'PaletteType', '');
      if ReadStr = '' then
        raise EPaletteException.Create(Format(
          MlgStringList['Palette', 20], [FileName]), erFileType);
      if not IdentToPaletteType(ReadStr, AType) then
        raise EPaletteException.Create(
          Format(MlgStringList['Palette', 21], [FileName, ReadStr]), erPaletteType);
      { read number of color-entries }
      ColorCnt := IniFile.ReadInteger('PaletteInfo', 'Colors', -1);
      { determine number of color-parts for current palette-type }
      if AType = ptGrayScale then
        NeededEntries := 1
      else
        if AType = ptCMYK then
          NeededEntries := 4
        else
          NeededEntries := 3;
      { read all colors }
      for Cnt := 0 to ColorCnt - 1 do
      begin
        ReadStr := IniFile.ReadString('PaletteColors', 'Color' + IntToStr(Cnt), '');
        if ReadStr = '' then
          raise EPaletteException.Create(
            Format(MlgStringList['Palette', 23], [FileName, 'Color' + IntToStr(Cnt)]), erFileFormat);
        Entries := ParseString;
        if Entries < NeededEntries then
          raise EPaletteException.Create(
            Format(MlgStringList['Palette', 22], [FileName, 'Color' + IntToStr(Cnt)]), erFileFormat);
        case AType of
          ptRGB: Color := RGBToColor(Values[0], Values[1], Values[2]);
          ptCMYK: Color := CMYKToColor(Values[0], Values[1], Values[2], Values[3]);
          ptCMYK255: Color := CMYK255ToColor(Values[0], Values[1], Values[2], Values[3]);
          ptGrayScale: Color := GrayScaleToColor(Values[0]);
          ptHSV: Color := HSVToColor(Values[0], Values[1], Values[2]);
          ptCMY: Color := CMYToColor(Values[0], Values[1], Values[2]);
        else
          Color := clBlack;
        end;
        APalette.Add(Name, Color);
      end;
      { file was read successfully, copy new palette to self and set filename }
      Assign(APalette);
      FFileName := FileName;
      FModified := FALSE;
    finally
      APalette.Free;
    end;
  finally
    IniFile.Free;
  end;
end;

procedure TPalette.WriteToFile(const FileName: string; AType: TPaletteType);
var
  IniFile: TIniFile;
  Cnt: Integer;
  Value1: Integer;
  Value2: Integer;
  Value3: Integer;
  Value4: Integer;
begin
  IniFile := TIniFile.Create(ExpandFileName(FileName));
  try
    IniFile.WriteString('FileInfo', 'FileType', 'Named-Palette-File');
    IniFile.WriteInteger('FileInfo', 'FileVersion', 1);
    IniFile.WriteInteger('PaletteInfo', 'Colors', Count);
    IniFile.WriteString('PaletteInfo', 'PaletteType', PaletteTypeToIdent(AType));
    IniFile.EraseSection('PaletteColors');
    case AType of
      ptHSV: for Cnt := 0 to Count - 1 do
        begin
          ColorToHSV(Colors[Cnt], Value1, Value2, Value3);
          IniFile.WriteString('PaletteColors', Format('Color%d', [Cnt]),
            Format('%s,0x%.2x,0x%.2x,0x%.2x', [FList[Cnt], Value1, Value2, Value3]));
        end;
      ptCMY: for Cnt := 0 to Count - 1 do
        begin
          ColorToCMY(Colors[Cnt], Value1, Value2, Value3);
          IniFile.WriteString('PaletteColors', Format('Color%d', [Cnt]),
            Format('%s,0x%.2x,0x%.2x,0x%.2x', [FList[Cnt], Value1, Value2, Value3]));
        end;
      ptCMYK: for Cnt := 0 to Count - 1 do
        begin
          ColorToCMYK(Colors[Cnt], Value1, Value2, Value3, Value4);
          IniFile.WriteString('PaletteColors', Format('Color%d', [Cnt]),
            Format('%s,0x%.2x,0x%.2x,0x%.2x,0x%.2x', [FList[Cnt], Value1, Value2, Value3, Value4]));
        end;
      ptCMYK255: for Cnt := 0 to Count - 1 do
        begin
          ColorToCMYK255(Colors[Cnt], Value1, Value2, Value3, Value4);
          IniFile.WriteString('PaletteColors', Format('Color%d', [Cnt]),
            Format('%s,0x%.2x,0x%.2x,0x%.2x,0x%.2x', [FList[Cnt], Value1, Value2, Value3, Value4]));
        end;
      ptGrayScale: for Cnt := 0 to Count - 1 do
        begin
          ColorToGrayScale(Colors[Cnt], Value1);
          IniFile.WriteString('PaletteColors', Format('Color%d', [Cnt]),
            Format('%s,0x%.2x', [FList[Cnt], Value1]));
        end;
    else
      for Cnt := 0 to Count - 1 do
      begin
        ColorToRGB(Colors[Cnt], Value1, Value2, Value3);
        IniFile.WriteString('PaletteColors', Format('Color%d', [Cnt]),
          Format('%s,0x%.2x,0x%.2x,0x%.2x', [FList[Cnt], Value1, Value2, Value3]));
      end;
    end;
    FFileName := FileName;
    FModified := FALSE;
  finally
    IniFile.Free;
  end;
end;

procedure TPalette.Clear;
begin
  FList.Clear;
  FFileName := '';
  FModified := FALSE;
end;

type
  TLongArray = array[0..MaxInt div 4 - 1] of LongInt;
  PLongArray = ^TLongArray;

procedure TPalette.AssignTo(Dest: TPersistent);
var
  Cnt: Integer;
begin
  if not (Dest is TPalette) then
    inherited AssignTo(Dest)
  else
    with Dest as TPalette do
    begin
      Clear;
      for Cnt := 0 to Self.Count - 1 do
        Add(Self.ColorNames[Cnt], Self[Cnt]);
      FFileName := Self.FFileName;
      FModified := Self.FModified;
    end;
end;

procedure TPalette.Delete(AIndex: Integer);
begin
  FList.Delete(AIndex);
  FModified := TRUE;
end;

function TPalette.GetColorName(AIndex: Integer): string;
begin
  Result := FList[AIndex];
end;

procedure TPalette.SetColorName(AIndex: Integer; const Name: string);
begin
  if FList[AIndex] <> Name then
    FModified := TRUE;
  FList[AIndex] := Name;
end;

{===============================================================================
| Color conversion functions
+==============================================================================}

class procedure TPalette.ColorToRGB
  (
  AColor: TColor;
  var R: Integer;
  var G: Integer;
  var B: Integer
  );
begin
  R := GetRValue(LongInt(AColor));
  G := GetGValue(LongInt(AColor));
  B := GetBValue(LongInt(AColor));
end;

class procedure TPalette.ColorToHSV
  (
  AColor: TColor;
  var H: Integer;
  var S: Integer;
  var V: Integer
  );
var
  R: Double;
  G: Double;
  B: Double;
  MaxValue: Double;
  MinValue: Double;
  Diff: Double;
  RDist: Double;
  GDist: Double;
  BDist: Double;
begin
  R := GetRValue(LongInt(AColor)) / 255.0;
  G := GetGValue(LongInt(AColor)) / 255.0;
  B := GetBValue(LongInt(AColor)) / 255.0;
  MaxValue := Max(R, Max(G, B));
  MinValue := Min(R, Min(G, B));
  Diff := MaxValue - MinValue;
  V := Round(MaxValue * 100.0);
  if MaxValue <> 0 then
    S := Round(100 * Diff / MaxValue)
  else
    S := 0;
  if S = 0 then
    H := 0
  else
  begin
    RDist := (MaxValue - R) / Diff;
    GDist := (MaxValue - G) / Diff;
    BDist := (MaxValue - B) / Diff;
    if R = MaxValue then
      H := Round(60 * (BDist - GDist))
    else
      if G = MaxValue then
        H := Round(60 * (2 + RDist - BDist))
      else
        if B = MaxValue then
          H := Round(60 * (4 + GDist - RDist));
    if H < 0 then
      H := H + 360;
  end;
end;

class function TPalette.HSVToColor
  (
  H: Integer;
  S: Integer;
  V: Integer
  )
  : TColor;
var
  i: Integer;
  f: Double;
  p: Integer;
  q: Integer;
  t: Integer;
  SInt: Double;
begin
  if S = 0 then
    Result := RGBToColor(Round(V * 2.55), Round(V * 2.55), Round(V * 2.55))
  else
  begin
    if H = 360 then
      H := 0;
    i := H div 60;
    f := H / 60.0 - i;
    SInt := S / 100.0;
    p := Round(V * (1 - SInt) * 2.55);
    q := Round(V * (1 - (SInt * f)) * 2.55);
    t := Round(V * (1 - (SInt * (1 - f))) * 2.55);
    case i of
      0: Result := RGBToColor(Round(V * 2.55), t, p);
      1: Result := RGBToColor(q, Round(V * 2.55), p);
      2: Result := RGBToColor(p, Round(V * 2.55), t);
      3: Result := RGBToColor(p, q, Round(V * 2.55));
      4: Result := RGBToColor(t, p, Round(V * 2.55));
    else
      Result := RGBToColor(Round(V * 2.55), p, q);
    end;
  end;
end;

class procedure TPalette.ColorToCMYK
  (
  AColor: TColor;
  var C: Integer;
  var M: Integer;
  var Y: Integer;
  var K: Integer
  );
begin
  C := Round(100 - GetRValue(LongInt(AColor)) / 2.55);
  M := Round(100 - GetGValue(LongInt(AColor)) / 2.55);
  Y := Round(100 - GetBValue(LongInt(AColor)) / 2.55);
  K := Min(C, Min(M, Y));
  C := C - K;
  M := M - K;
  Y := Y - K;
end;

class procedure TPalette.ColorToCMY
  (
  AColor: TColor;
  var C: Integer;
  var M: Integer;
  var Y: Integer
  );
begin
  C := 255 - GetRValue(LongInt(AColor));
  M := 255 - GetGValue(LongInt(AColor));
  Y := 255 - GetBValue(LongInt(AColor));
end;

class procedure TPalette.ColorToCMYK255
  (
  AColor: TColor;
  var C: Integer;
  var M: Integer;
  var Y: Integer;
  var K: Integer
  );
begin
  C := 255 - GetRValue(LongInt(AColor));
  M := 255 - GetGValue(LongInt(AColor));
  Y := 255 - GetBValue(LongInt(AColor));
  K := Min(C, Min(M, Y));
  C := C - K;
  M := M - K;
  Y := Y - K;
end;

class procedure TPalette.ColorToGrayScale
  (
  AColor: TColor;
  var GrayLevel: Integer
  );
begin
end;

class function TPalette.RGBToColor
  (
  R: Integer;
  G: Integer;
  B: Integer
  )
  : TColor;
begin
  LongInt(Result) := RGB(R, G, B);
end;

class function TPalette.CMYToColor
  (
  C: Integer;
  M: Integer;
  Y: Integer
  )
  : TColor;
begin
  Result := RGBToColor(255 - C, 255 - M, 255 - Y);
end;

class function TPalette.CMYKToColor
  (
  C: Integer;
  M: Integer;
  Y: Integer;
  K: Integer
  )
  : TColor;
begin
  Result := RGBToColor(Max(0, Round(2.55 * (100 - C - K))), Max(0, Round(2.55 * (100 - M - K))),
    Max(0, Round(2.55 * (100 - Y - K))));
end;

class function TPalette.CMYK255ToColor
  (
  C: Integer;
  M: Integer;
  Y: Integer;
  K: Integer
  )
  : TColor;
begin
  Result := RGBToColor(Max(0, 255 - C - K), Max(0, 255 - M - K), Max(0, 255 - Y - K));
end;

class function TPalette.GrayScaleToColor
  (
  GrayLevel: Integer
  )
  : TColor;
begin
  Result := RGBToColor(GrayLevel, GrayLevel, GrayLevel);
end;

class function TPalette.PaletteTypeToIdent
  (
  AType: TPaletteType
  )
  : string;
begin
  case AType of
    ptRGB: Result := 'RGB';
    ptCMYK: Result := 'CMYK';
    ptCMYK255: Result := 'CMYK255';
    ptGrayScale: Result := 'GrayScale';
    ptHSV: Result := 'HSV';
    ptCMY: Result := 'CMY';
  else
    Result := '';
  end;
end;

class function TPalette.IdentToPaletteType
  (
  const AIdent: string;
  var AType: TPaletteType
  )
  : Boolean;
begin
  Result := TRUE;
  if CompareText(AIdent, 'RGB') = 0 then
    AType := ptRGB
  else
    if CompareText(AIdent, 'CMYK') = 0 then
      AType := ptCMYK
    else
      if CompareText(AIdent, 'CMYK255') = 0 then
        AType := ptCMYK255
      else
        if CompareText(AIdent, 'GrayScale') = 0 then
          AType := ptGrayScale
        else
          if CompareText(AIdent, 'HSV') = 0 then
            AType := ptHSV
          else
            if CompareText(AIdent, 'CMY') = 0 then
              AType := ptCMY
            else
              Result := FALSE;
end;

class function TPalette.PaletteTypeToName
  (
  AType: TPaletteType
  )
  : string;
begin
  Result := MlgStringList['Palette', Integer(AType) + 1];
end;

class function TPalette.NameToPaletteType
  (
  const AName: string
  )
  : TPaletteType;
begin
  for Result := Low(TPaletteType) to High(TPaletteType) do
    if AnsiCompareText(AName, PaletteTypeToName(Result)) = 0 then
      Exit;
  Result := ptRGB;
end;

function TPalette.ReadFromRegistry
  (
  Registry: TRegistryDatabase;
  const KeyName: string
  )
  : Boolean;
var
  Entries: Integer;
  Cnt: Integer;
  ColorStream: TMemoryStream;
  ColorReader: TReader;
  NameStream: TMemoryStream;
  NameReader: TReader;
begin
  Result := FALSE;
  with Registry do
  begin
    Openkey(KeyName, FALSE);
    Entries := ReadInteger('Count');
    if LastError = rdbeNoError then
    begin
      try
        ColorStream := TMemoryStream.Create;
        ColorStream.SetSize(DataSize['Colors']);
        NameStream := TMemoryStream.Create;
        NameStream.SetSize(DataSize['Names']);
        ReadBinary('Colors', ColorStream.Memory^, ColorStream.Size);
        ReadBinary('Names', NameStream.Memory^, NameStream.Size);
        ColorReader := TReader.Create(ColorStream, 128);
        NameReader := TReader.Create(NameStream, 128);
        Clear;
        for Cnt := 0 to Entries - 1 do
          Add(NameReader.ReadString, ColorReader.ReadInteger);
      finally
        ColorReader.Free;
        ColorStream.Free;
        NameReader.Free;
        NameStream.Free;
      end;
    end;
  end;
end;

procedure TPalette.WriteToRegistry(Registry: TRegistryDatabase; const KeyName: string);
var
  Cnt: Integer;
  MemStream: TMemoryStream;
  Writer: TWriter;
begin
  with Registry do
  begin
    Openkey(KeyName, TRUE);
    WriteInteger('Count', Count);
    MemStream := TMemoryStream.Create;
    Writer := TWriter.Create(MemStream, 128);
    try
      for Cnt := 0 to Count - 1 do
        Writer.WriteInteger(Colors[Cnt]);
      Writer.FlushBuffer;
      WriteBinary('Colors', MemStream.Memory^, MemStream.Size);
      MemStream.Clear;
      for Cnt := 0 to Count - 1 do
        Writer.WriteString(ColorNames[Cnt]);
      Writer.FlushBuffer;
      WriteBinary('Names', MemStream.Memory^, MemStream.Size);
    finally
      Writer.Free;
      MemStream.Free;
    end;
  end;
end;

{===============================================================================
| EPaletteException
+==============================================================================}

constructor EPaletteException.Create
  (
  const ErrorText: string;
  AErrorNumber: Integer
  );
begin
  inherited Create(ErrorText);
  ErrorNumber := AErrorNumber;
end;

end.

