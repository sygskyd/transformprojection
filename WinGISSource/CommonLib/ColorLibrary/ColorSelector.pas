 Unit ColorSelector;

Interface

Uses SysUtils,WinTypes,WinProcs,Classes,Combobtn,Controls,DipGrid,Forms,Graphics,
     Messages,MultiLng,Palette,StdCtrls,PropCollect,WCtrls;

Type TColorSelectForm = Class(TDropDownForm)
       ColorGrid   : TDipList;              
       MlgSection  : TMlgSection;
       OtherBtn    : TButton;
       ToggleBtn   : TButton;
       Procedure   ColorGridDrawCell(Sender: TObject; Canvas: TCanvas; Index: Integer;
                       Rect: TRect);
       Procedure   ColorGridKeyDown(Sender: TObject; var Key: Word;
                       Shift: TShiftState);
       Procedure   ColorGridMouseMove(Sender: TObject; Shift: TShiftState; X,
                       Y: Integer);
       Procedure   ColorGridMouseUp(Sender: TObject; Button: TMouseButton;
                       Shift: TShiftState; X, Y: Integer);
       Procedure   FormShow(Sender: TObject);
       Procedure   OtherBtnClick(Sender: TObject);
       Procedure   ToggleBtnClick(Sender: TObject);
      Private
       FColor           : TColor;
       FLastIndex       : Integer;
       FFlatGrid        : Boolean;
       FOnColSelect     : TNotifyEvent;
       FOptions         : TSelectOptionList;
       FPalette         : TPalette;
       FScrollbars      : TScrollbars;
       FShowAsList      : Boolean;
       FShowMore        : Boolean;
       FShowNames       : Boolean;
       FShowToggle      : Boolean;
       Procedure   DoColorSelect;
       Procedure   SetShowAsList;
      Public
       Constructor Create(AOwner:TComponent); override;
       Destructor  Destroy; override;
       Property    OnSelectColor:TNotifyEvent read FOnColSelect write FOnColSelect;
       Property    Palette:TPalette read FPalette write FPalette;
       Property    SelectedColor:TColor read FColor write FColor;
     end;

     TColorSelectBtn  = Class(TCustomDropDownBtn)
      Private
       FBitmap          : TBitmap;
       FColorDropDown   : TColorSelectForm;
       FOnSelectColor   : TNotifyEvent;
       Procedure   DoPaintButton(Sender:TObject;ARect:TRect);
       Procedure   DoColorSelect(Sender:TObject);
       Function    GetColor:TColor;
       Function    GetColorDropDown:TColorSelectForm;
       Function    GetCurrentPalette:TPalette;
       Function    GetOptions:TSelectOptionList;
       Function    GetScrollbars:TScrollbars;
       Function    GetValue(AIndex:Integer):Boolean;
       Procedure   SetBitmap(ABitmap:TBitmap);
       Procedure   SetColor(AColor:TColor);
       Procedure   SetPalette(APalette:TPalette);
       Procedure   SetScrollbars(AScrollbars:TScrollbars);
       Procedure   SetOptions(AOptions:TSelectOptionList);
       Procedure   SetValue(AIndex:Integer;AValue:Boolean);
      Public
       Constructor Create(AOwner:TComponent); override;
       Destructor  Destroy; override;
       Property    ColorDropDown:TColorSelectForm read GetColorDropDown;
       Property    Palette:TPalette read GetCurrentPalette write SetPalette;
       Property    Options:TSelectOptionList read GetOptions write SetOptions;
      Published
       Property    Alignment;
       Property    Bitmap:TBitmap read FBitmap write SetBitmap;
       Property    Caption;
       Property    Flat;
       Property    FlatGrid:Boolean index 4 read GetValue write SetValue default FALSE;
       Property    Font;
       Property    Hint;
       Property    OnSelectColor:TNotifyEvent read FOnSelectColor write FOnSelectColor;
       Property    ParentFont;
       Property    ParentShowHint;
       Property    Scrollbars:TScrollbars read GetScrollbars write SetScrollbars default DipGrid.sbVertical;
       Property    SelectedColor:TColor read GetColor write SetColor default clBlack;
       Property    ShowHint;
       Property    ShowAsList:Boolean index 2 read GetValue write SetValue default FALSE;
       Property    ShowColorNames:Boolean index 0 read GetValue write SetValue default TRUE;
       Property    ShowMoreButton:Boolean index 3 read GetValue write SetValue default TRUE;
       Property    ShowToggleButton:Boolean index 1 read GetValue write SetValue default TRUE;
       Property    TabOrder;
       Property    TabStop;
     end;

Implementation

{$R *.DFM}

Uses PaletteDialog,NumTools;

{===============================================================================
  TColorSelectForm
 ** Changed by Brovak 29.09.2000
+==============================================================================}

Constructor TColorSelectForm.Create(AOwner:TComponent);
  begin
    inherited Create(AOwner);
    FShowNames:=TRUE;
    FShowToggle:=TRUE;
    FShowMore:=TRUE;
    FScrollbars:=sbAutomatic;
    FOptions:=TSelectOptionList.Create;
  end;

Destructor TColorSelectForm.Destroy;
  begin
    FOptions.Free;
    inherited Destroy;
  end;    

Procedure TColorSelectForm.OtherBtnClick(Sender: TObject);
  var ColorDialog  : TPaletteDialog;
  begin
    Hide;
    ColorDialog:=TPaletteDialog.Create(Application.MainForm);
    try
      ColorDialog.Palette:=FPalette;
      ColorDialog.OriginalColor:=FColor;
      ColorDialog.ShowAsList:=FShowAsList;
      if ColorDialog.ShowModal=mrOK then begin
        SelectedColor:=ColorDialog.SelectedColor;
        Palette.Assign(ColorDialog.Palette);
        if Assigned(FOnColSelect) then OnSelectColor(Self);
      end;
    finally
      ColorDialog.Free;
    end;
  end;

Procedure TColorSelectForm.ColorGridMouseUp(Sender: TObject;
    Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
  var Pos          : TPoint;
  begin
    Pos:=ColorGrid.DipAtPos(Point(X,Y),TRUE);
    if Pos.X>-1 then DoColorSelect;
    Hide;
  end;

Procedure TColorSelectForm.ColorGridDrawCell(Sender: TObject; Canvas: TCanvas;
    Index: Integer; Rect: TRect);
  var ARect        : TRect;
      X            : Integer;
  begin
    if FPalette<>NIL then with Canvas do begin
      if Index<0 then begin
        X:=Rect.Left+(Rect.Right-Rect.Left-TextWidth(FOptions.LongNames[Abs(Index)-1])) Div 2;
        TextRect(Rect,X,Rect.Top-2,FOptions.LongNames[Abs(Index)-1]);
                        //Rect.Top -> Rect.Top-2 Brovak
      end
      else if FShowAsList then begin
        Brush.Color:=clWindow;
        FillRect(Rect);
        ARect:=Rect;
        if FShowNames then begin
          ARect.Left:=Rect.Left+2*ColorGrid.RowHeight+2;
          TextRect(ARect,ARect.Left,CenterHeightInRect(TextHeight('A'),ARect),
              Palette.ColorNames[Index]);
          ARect:=Rect;
          ARect.Right:=ARect.Left+2*ColorGrid.RowHeight;
          InflateRect(ARect,-1,-1);
        end;
        Brush.Color:=Palette[Index];
        FillRect(ARect);
      end
      else begin
        Brush.Color:=Palette[Index];
        FillRect(Rect);
      end;
    end;
  end;

Procedure TColorSelectForm.DoColorSelect;
  begin
    if Assigned(FOnColSelect) then with ColorGrid do begin
      if ItemIndex>=-FOptions.Count then begin
        if ItemIndex>=0 then FColor:=Palette[ItemIndex]
        else FColor:=FOptions.Values[Abs(ItemIndex)-1];
      end;
      OnSelectColor(Self);
    end;
  end;

Procedure TColorSelectForm.ColorGridKeyDown(Sender: TObject;
    var Key: Word; Shift: TShiftState);
  begin
    case Key of
      vk_Return : DoColorSelect;
      vk_Escape : Hide;
    end;
  end;

Procedure TColorSelectForm.FormShow(Sender: TObject);
  var AHeight      : Integer;
      Index        : Integer;
  begin
    ToggleBtn.Visible:=FShowToggle;
    OtherBtn.Visible:=FShowMore;
    ColorGrid.Scrollbars:=FScrollbars;
    ColorGrid.Flat:=FFlatGrid;
    with ColorGrid do begin
      SetShowAsList;
      if Assigned(FPalette) then Count:=FPalette.Count
      else Count:=0;
      HeaderRows:=FOptions.Count;
      AHeight:=Count Div ColCount+HeaderRows;
      if Count Mod ColCount<>0 then Inc(AHeight);
      if AHeight>15 then AHeight:=15; //8->15 Brovak
      Height:=AHeight*CellHeight;
      Index:=Top+Height+2;
      if FShowToggle then begin
        ToggleBtn.Top:=Index;
        Index:=Index+ToggleBtn.Height;
      end;
      OtherBtn.Top:=Index;
    end;
    if FShowMore then ClientHeight:=OtherBtn.Top+OtherBtn.Height+1
    else ClientHeight:=OtherBtn.Top-1;
    ClientWidth:=ColorGrid.Width+2;
    ToggleBtn.Width:=ColorGrid.Width;
    OtherBtn.Width:=ColorGrid.Width;
    if FPalette<>NIL then begin
      Index:=-FOptions.IndexOfValue(FColor)-1;
      if Index=0 then begin
        Index:=FPalette.IndexOf(FColor);
        if Index<0 then Index:=-FOptions.Count-1;
      end;
      ColorGrid.ItemIndex:=Index;
    end;

    {Brovak - Correct pos at screen}
    if Left+Width > Screen.Width
     then Left:=(Screen.Width-10)-Width;
    if Left<0 then Left:=10;

    if Top+Height > Screen.Height
     then Left:=(Screen.Width-10)-Width;
    if Top<0 then Top:=10;
    {Brovak}

  end;

Procedure TColorSelectForm.SetShowAsList;
var NewColCount  : Integer;
begin
  with ColorGrid do begin
    if FShowAsList then NewColCount:=1
    else NewColCount:=24; //8->24 Brovak
    CellWidth:=(ClientWidth-1) Div NewColCount;
    ColCount:=NewColCount;
    ShowHint:=not FShowAsList;
  end;
end;

Procedure TColorSelectForm.ToggleBtnClick(Sender: TObject);
  begin
    FShowAsList:=not FShowAsList;
    SetShowAsList;
  end;

Procedure TColorSelectForm.ColorGridMouseMove(Sender: TObject;
    Shift: TShiftState; X, Y: Integer);
  var Index        : Integer;
  begin
    Index:=ColorGrid.ItemAtPos(Point(X,Y),TRUE);
    if Index<>FLastIndex then begin
      Application.CancelHint;
      FLastIndex:=Index;
      if Index>=0 then ColorGrid.Hint:=Palette.ColorNames[Index]
      else ColorGrid.Hint:='';
    end;
  end;

{===============================================================================
  TColorSelectBtn
+==============================================================================}

Constructor TColorSelectBtn.Create
   (
   AOwner          : TComponent
   );
  begin
    inherited Create(AOwner);
    FBitmap:=TBitmap.Create;
    FColorDropDown:=GetColorDropDown;
    Scrollbars:=DipGrid.sbVertical;
  end;

Destructor TColorSelectBtn.Destroy;
  begin
    FBitmap.Free;
    inherited Destroy;
  end;

Function TColorSelectBtn.GetColorDropDown
   : TColorSelectForm;
  begin
    if FColorDropDown=NIL then begin
      FColorDropDown:=TColorSelectForm.Create(Self);
      FColorDropDown.OnSelectColor:=DoColorSelect;
      OnPaintBtn:=DoPaintButton;
      DropDown:=FColorDropDown;
    end;
    Result:=FColorDropDown;
  end;

Function TColorSelectBtn.GetCurrentPalette
   : TPalette;
  begin
    Result:=ColorDropDown.Palette;
  end;

Procedure TColorSelectBtn.SetPalette
   (
   APalette        : TPalette
   );
  begin
    ColorDropDown.Palette:=APalette;
  end;

Procedure TColorSelectBtn.DoColorSelect(Sender:TObject);
  begin
    if SelectedColor>=0 then Caption:=''
    else Caption:=Options.LongNames[Options.IndexOfValue(SelectedColor)];
    if Assigned(FOnSelectColor) then OnSelectColor(Self);
    Invalidate;
  end;

Function TColorSelectBtn.GetColor
   : TColor;
  begin
    Result:=ColorDropDown.SelectedColor;
  end;

Procedure TColorSelectBtn.SetColor
   (
   AColor          : TColor
   );
  begin
    if AColor<>ColorDropDown.Color then begin
      ColorDropDown.SelectedColor:=AColor;
      if SelectedColor>=0 then Caption:=''
      else Caption:=Options.LongNames[Options.IndexOfValue(SelectedColor)];
      Invalidate;
    end;
  end;

Procedure TColorSelectBtn.DoPaintButton
   (
   Sender          : TObject;
   ARect           : TRect
   );
  Const ROP_DSPDxax     = $00E20746;
  var BRect        : TRect;
      MonoBmp      : TBitmap;
  begin

    if Caption<>'' then inherited DoPaintBtn(ARect)
    else with Canvas do if FBitmap.Width>0 then begin
      with FBitmap do if Width>Height then BRect:=Rect(0,0,Width Div 2,Height)
      else BRect:=Rect(0,0,Width,Height);
      MonoBmp:=TBitmap.Create;
      try
        if not Enabled then begin
          CreateDisabledBitmap(FBitmap,MonoBmp);
          BrushCopy(CenterRectInRect(BRect,ARect),MonoBmp,BRect,MonoBmp.TransparentColor);
        end
        else if Self.SelectedColor>=0 then begin
          BrushCopy(CenterRectInRect(BRect,ARect),FBitmap,BRect,FBitmap.TransparentColor);
          with MonoBmp do begin
            Assign(FBitmap);
            {$IFDEF WIN32}
            HandleType:=bmDDB;
            {$ENDIF}
            Canvas.Brush.Color:=clRed;
            if Monochrome then begin
              Canvas.Font.Color:=clWhite;
              Monochrome:=FALSE;
              Canvas.Brush.Color:=clWhite;
            end;
            Monochrome:=TRUE;
          end;
          Brush.Color:=Self.SelectedColor;
          SetTextColor(Handle,clBlack);
          SetBkColor(Handle,clWhite);
          with CenterRectInRect(BRect,ARect) do BitBlt(Handle,Left,Top,
              RectWidth(BRect),RectHeight(BRect),MonoBmp.Canvas.Handle,0,0,ROP_DSPDxax);
        end;
      finally
        MonoBmp.Free;
      end;
    end
    else begin
      if Enabled then Brush.Color:=ColorDropDown.SelectedColor
      else Brush.Color:=clBtnFace;
      FillRect(ARect);
    end;
  end;

Procedure TColorSelectBtn.SetBitmap
   (
   ABitmap         : TBitmap
   );
  begin
    FBitmap.Assign(ABitmap);
    Invalidate;
  end;

Function TColorselectBtn.GetValue(AIndex:Integer):Boolean;
begin
  case AIndex of
    0: Result:=FColorDropDown.FShowNames;
    1: Result:=FColorDropDown.FShowToggle;
    2: Result:=FColorDropDown.FShowAsList;
    3: Result:=FColorDropDown.FShowMore;
    4: Result:=FColorDropDown.FFlatGrid;
    else Result:=FALSE;
  end;
end;

Procedure TColorSelectBtn.SetValue(AIndex:Integer;AValue:Boolean);
begin
  case AIndex of
    0: FColorDropDown.FShowNames:=AValue;
    1: FColorDropDown.FShowToggle:=AValue;
    2: FColorDropDown.FShowAsList:=AValue;
    3: FColorDropDown.FShowMore:=AValue;
    4: FColorDropDown.FFlatGrid:=AValue;
  end;
end;

Function TColorSelectBtn.GetScrollbars:TScrollbars;
  begin
    Result:=FColorDropDown.FScrollbars;
  end;

Procedure TColorSelectBtn.SetScrollbars(AScrollbars:TScrollbars);
  begin
    FColorDropDown.FScrollbars:=AScrollbars;
  end;

Function TColorSelectBtn.GetOptions
   : TSelectOptionList;
  begin
    Result:=FColorDropDown.FOptions;
  end;

Procedure TColorSelectBtn.SetOptions(AOptions:TSelectOptionList);
  begin
    FColorDropDown.FOptions.Assign(AOptions);
  end;

end.
