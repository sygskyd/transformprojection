�
 TPALETTEDIALOG 0�  TPF0TPaletteDialogPaletteDialogLeftjTop� BorderStylebsDialogCaption!1ClientHeightMClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrder	PositionpoScreenCenterOnCreate
FormCreate	OnDestroyFormDestroyOnHideFormHideOnShowFormShowPixelsPerInch`
TextHeight TLabelKLabelLeft� Top,Width
HeightCaptionK:FocusControlKEdit  TLabelBLabelLeft� Top,Width
HeightCaptionB:FocusControlBEdit  TLabelGLabelLeftHTop,WidthHeightCaptionG:FocusControlGEdit  TLabelRLabelLeftTop,WidthHeightCaptionR:FocusControlREdit  TLabelLabel1LeftTopWidthIHeightAutoSizeCaption!2FocusControlColorModelCombo  TWindowOriginalColorWindowLeftTopWidth1Height!Ctl3DEraseBackgroundOnClickOriginalColorWindowClickOnPaintOriginalColorWindowPaintParentCtl3D  TButton	CancelBtnLeft`Top0WidthiHeightCancel	Caption!6ModalResultTabOrder  TButtonOkBtnLeft� Top0WidthiHeightCaption!5Default	ModalResultTabOrder  	TGroupBoxGroup1LeftTopCWidth� Height� Caption!3TabOrderTabStop	 TDipList	ColorGridLeftTopWidth� Height� ColCountColWidth
DragCursorcrCrossOnClickColorGridClick
OnDragDropColorGridDragDrop
OnDragOverColorGridDragOver
OnDrawCellColorGridDrawCellOnMouseDownColorGridMouseDownOnMouseMoveColorGridMouseMoveParentShowHint	PopupMenuPaletteMenu	RowHeight
Scrollbars
sbVerticalShowHint	TabOrder TabStop	  TButton	ToggleBtnLeft
Top� Width� HeightCaption!7TabOrderOnClickToggleBtnClick  TPopupMenuBtnPaletteMenuBtnLeft
Top� Width� Caption!4	PopupMenuPaletteMenuTabOrder   TPanelPanelLeftTopHWidth� Height� 
BevelInnerbvRaised
BevelOuter	bvLoweredTabOrder TXYSelector	VSelectorLeft� TopWidthHeight� OnChangeSelectorChangeOnPaintVSelectorPaintSelectorTypestYSelectorSizeYMax       �@  TXYSelector
HSSelectorLeftTopWidth� Height� 
OnDblClickHSSelectorDblClickOnChangeHSSelectorChangeOnPaintHSSelectorPaintXMax       �@YMax       �@   TSpinBtnKSpinLeft� Top(WidthHeight	Arrowkeys	AssociateKEditCtrlIncrement       �@	Increment       ��?Max       �@OnClickColorInputChangeShiftIncrement       �@  TEditKEditLeft� Top(WidthHeightTabOrderOnExitColorInputChange  TSpinBtnBSpinLeft� Top(Height	Arrowkeys	AssociateBEditCtrlIncrement       �@	Increment       ��?Max       �@OnClickColorInputChangeShiftIncrement       �@  TEditBEditLeft� Top(WidthHeightTabOrderOnExitColorInputChange  TSpinBtnGSpinLeftuTop(Height	Arrowkeys	AssociateGEditCtrlIncrement       �@	Increment       ��?Max       �@OnClickColorInputChangeShiftIncrement       �@  TEditGEditLeftXTop(WidthHeightTabOrderOnExitColorInputChange  TSpinBtnRSpinLeft5Top(Height	Arrowkeys	AssociateREditCtrlIncrement       �@	Increment       ��?Max       �@OnClickColorInputChangeShiftIncrement       �@  TEditREditLeftTop(WidthHeightTabOrderOnExitColorInputChange  TWindowCurrentColorWindowLeft TopWidth1Height!Ctl3DEraseBackgroundOnPaintCurrentColorWindowPaintParentCtl3D  	TComboBoxColorModelComboLeftXTopWidth� HeightStylecsDropDownList
ItemHeightSorted	TabOrder OnClickColorModelComboClick  TEditColorNameEditLeftXTop$WidthqHeightTabOrder  
TPopupMenuPaletteMenuLeftHTop0 	TMenuItemAddColorMenuCaption!23OnClickAddColorMenuClick  	TMenuItem
InsertMenuCaption!29OnClickInsertMenuClick  	TMenuItemDeleteColorMenuCaption!24OnClickDeleteColorMenuClick  	TMenuItem
RenameMenuCaption!30OnClickRenameMenuClick  	TMenuItemReplaceColorMenuCaption!28OnClickReplaceColorMenuClick  	TMenuItemN1Caption-  	TMenuItem
NewPaletteCaption!25OnClickNewPaletteClick  	TMenuItemOpenPaletteCaption!26OnClickOpenPaletteClick  	TMenuItemSavePaletteCaption!27OnClickSavePaletteClick   TIntValidatorRValEditREdit	AllowSignMinValue� 	MaxLengthLeft Top   TIntValidatorGValEditGEdit	AllowSignMinValue� 	MaxLengthLeft`Top   TIntValidatorBValEditBEdit	AllowSignMinValue� 	MaxLengthLeft� Top   TIntValidatorKValEditKEdit	AllowSignMinValue� 	MaxLengthLeft� Top   TOpenDialog
OpenDialog
DefaultExtpalFilter!20OptionsofHideReadOnlyofFileMustExist Title!21Left(Top0  TSaveDialog
SaveDialog
DefaultExtpalFilter!20OptionsofOverwritePromptofHideReadOnlyofExtensionDifferent Title!22LeftTop0  TMlgSection
MlgSectionSectionPaletteDialogLefthTop0   