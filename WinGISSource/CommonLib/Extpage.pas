unit ExtPage;

//---------------------------------------------------------------------------//
//                                                                           //
//   Components Extended PageControl & Extended TabControl                   //
//                                                                           //
//   Date:    Nov, 01 1999                                                   //
//   Version: 2.3q                                                           //
//   Author:  Jan - M. Strube                                                //
//   Systems: Delphi 2.01, 3, 4, 5, C++Builder 1, 3, 4                       //
//                                                                           //
//   http://ourworld.compuserve.com/homepages/praxisservice/                 //
//                                                                           //
//---------------------------------------------------------------------------//

{$DEFINE NODEBUG} // DEBUG / NODEBUG
{$DEFINE EXTPAGE_WARNING} // EXTPAGE_WARNING / EXTPAGE_NOWARNING
{$DEFINE EXTPAGE_FULL} // EXTPAGE_FULL / EXTPAGE_TRIAL

{$BOOLEVAL OFF}
{$IFDEF DEBUG}
{$RANGECHECKS ON}
{$ENDIF}

interface

{$H+}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, ComCtrls, CommCtrl
{$IFNDEF EXTPAGE_FULL}, Buttons{$ENDIF};

{$IFDEF VER100} // Delphi 3
{$DEFINE IS_DELPHI3_OR_HIGHER}
{$DEFINE IS_DELPHI4_OR_LOWER}
{$ENDIF}

{$IFDEF VER110} // C++Builder 3
{$OBJEXPORTALL On}
{$DEFINE IS_DELPHI3_OR_HIGHER}
{$DEFINE IS_DELPHI4_OR_LOWER}
{$ENDIF}

{$IFDEF VER120} // Delphi 4
{$DEFINE IS_DELPHI3_OR_HIGHER}
{$DEFINE IS_DELPHI4_OR_HIGHER}
{$DEFINE IS_DELPHI4_OR_LOWER}
{$ENDIF}

{$IFDEF VER125} // C++Builder 4
{$OBJEXPORTALL On}
{$DEFINE IS_DELPHI3_OR_HIGHER}
{$DEFINE IS_DELPHI4_OR_HIGHER}
{$DEFINE IS_DELPHI4_OR_LOWER}
{$ENDIF}

{$IFDEF VER130} // Delphi 5
{$DEFINE IS_DELPHI3_OR_HIGHER}
{$DEFINE IS_DELPHI4_OR_HIGHER}
{$DEFINE IS_DELPHI5_OR_HIGHER}
{$ENDIF}

{$IFNDEF IS_DELPHI3_OR_HIGHER}
const
  TCS_BOTTOM = $0002;
  TCS_RIGHT = $0002;
  TCS_VERTICAL = $0080;
{$ENDIF}

{$IFNDEF IS_DELPHI4_OR_HIGHER}
const
  TCS_FLATBUTTONS = $0008;
{$ENDIF}

type
  TOwnerDrawState = StdCtrls.TOwnerDrawState;
  TExtGlyphPosition = (glPosLeft, glPosTop);

  TExtGlyphs = class(TPersistent)
  private
    FGlyph: TBitmap;
    FGlyphPaint: TBitmap;
    FGlpyhOwner: TControl;
    FImageList: TImageList;
    FNumGlyphs: Word;
    FOnChange: TNotifyEvent;
    FOnChangeGlyph: TNotifyEvent;
    FPosition: TExtGlyphPosition;
    FSpacing: Integer;
    FTransparentColor: TColor;
    procedure SetGlyph(Value: TBitmap);
    procedure SetImageList(Value: TImageList);
    procedure SetNumGlyphs(Value: Word);
    procedure SetPosition(Value: TExtGlyphPosition);
    procedure SetSpacing(Value: Integer);
    procedure SetTransparentColor(Value: TColor);
  protected
    procedure Changed(Sender: TObject);
    procedure ChangedGlyph(Sender: TObject);
  public
    constructor Create(AOwner: TControl);
    destructor Destroy; override;
    property ImageList: TImageList
      read FImageList
      write SetImageList;

    property OnChange: TNotifyEvent
      read FOnChange
      write FOnChange;

    property OnChangeGlyph: TNotifyEvent
      read FOnChangeGlyph
      write FOnChangeGlyph;

  published
    property Glyph: TBitmap
      read FGlyph
      write SetGlyph;

    property NumGlyphs: Word
      read FNumGlyphs
      write SetNumGlyphs
      default 0;

    property Position: TExtGlyphPosition
      read FPosition
      write SetPosition
      default glPosLeft;

    property Spacing: Integer
      read FSpacing
      write SetSpacing
      default 5;

    property TransparentColor: TColor
      read FTransparentColor
      write SetTransparentColor
      default clFuchsia;
  end;

  TExtHottrack = class(TPersistent)
  private
    FEnabled: Boolean;
    FHottrackOwner: TControl;
    FTabActiveColor: TColor;
    FTabInactiveColor: TColor;
    procedure SetEnabled(Value: Boolean);
    procedure SetTabActiveColor(Value: TColor);
    procedure SetTabInactiveColor(Value: TColor);
  protected
  public
    TabTracked: Integer;
    constructor Create(AOwner: TControl);
  published
    property Enabled: Boolean
      read FEnabled
      write SetEnabled
      default False;

    property TabActiveColor: TColor
      read FTabActiveColor
      write SetTabActiveColor
      default clHighlight;

    property TabInactiveColor: TColor
      read FTabInactiveColor
      write SetTabInactiveColor
      default clHighlight;
  end;

  EExtPageInvalidCall = class(Exception);

  TExtNbDrawTabEvent = procedure(Control: TWinControl; Index: Integer;
    ActiveTab: Boolean; const RectFg, RectBg: TRect;
    State: TOwnerDrawState) of object;

  TExtDrawBkgndInside = procedure(Control: TWinControl; DC: HDC;
    CurrPage: Integer; const RectBg: TRect) of object;

  TExtDrawBkgndOutside = procedure(Control: TWinControl; DC: HDC;
    RgnBg: HRgn) of object;

  TExtTabStyle = (tabStDefault, tabStButton);
  TExtTabPosition = (tabPosLeft, tabPosRight, tabPosTop, tabPosBottom);

  TExtPageControl = class(TPageControl)
  private
    FCanvas: TCanvas;
    FHottrack: TExtHottrack;
    FHintOrg: string;
    FInitialPage: Integer;
    FirstPaint: Boolean;
    FLastTabHintIndex: Integer;
    FMouseInControl: Boolean;
    FOnDrawBkgndInside: TExtDrawBkgndInside;
    FOnDrawBkgndOutside: TExtDrawBkgndOutside;
    FOnDrawTab: TExtNbDrawTabEvent;
    FTabActiveColor: TColor;
    FTabActiveFont: TFont;
    FTabGlyphs: TExtGlyphs;
    FTabHeight: Word;
    FTabHints: TStringList;
    FTabInactiveColor: TColor;
    FTabInactiveFont: TFont;
    FTabPosition: TExtTabPosition;
    FTabStyle: TExtTabStyle;
    FTabWidth: Word;
    FTabWordWrap: Boolean;
    StopBGPainting: Integer;
    StopBGPaintingTabs: Boolean;
    StopFGPainting: Integer;
    procedure CheckAndSetHWndFont;
    function GetActivePage: TTabSheet;
    procedure SetActivePage(Value: TTabSheet);
    procedure SetHottrack(Value: TExtHottrack);
    procedure SetTabActiveColor(Value: TColor);
    procedure SetTabActiveFont(Value: TFont);
    procedure SetTabGlyphs(Value: TExtGlyphs);
    procedure SetTabHeight(Value: Word);
    procedure SetTabHints(Value: TStringList);
    procedure SetTabInactiveColor(Value: TColor);
    procedure SetTabInactiveFont(Value: TFont);
    procedure SetTabPosition(Value: TExtTabPosition);
    procedure SetTabStyle(Value: TExtTabStyle);
    procedure SetTabWidth(Value: Word);
    procedure SetTabWordWrap(Value: Boolean);
    procedure CMDialogChar(var Msg: TCMDialogChar); message CM_DIALOGCHAR;
    procedure CMFontChanged(var Msg: TMessage); message CM_FONTCHANGED;
    procedure CMHintShow(var Msg: TMessage); message CM_HINTSHOW;
    procedure CMMouseEnter(var Msg: TMessage); message CM_MOUSEENTER;
    procedure CMMouseLeave(var Msg: TMessage); message CM_MOUSELEAVE;
    procedure CMParentColorChanged(var Msg: TMessage); message
      CM_PARENTCOLORCHANGED;
    procedure CNDrawTab(var Msg: TWMDrawItem); message CN_DRAWITEM;
    procedure TCMInsertItem(var Msg: TMessage); message TCM_INSERTITEM;
    procedure WMEraseBkgnd(var Msg: TWMEraseBkgnd); message WM_ERASEBKGND;
    procedure WMHelp(var Msg: TWMHelp); message WM_HELP;
    procedure WMLButtonDown(var Msg: TWMLButtonDown); message WM_LBUTTONDOWN;
    procedure WMNCHitTest(var Msg: TWMNCHitTest); message WM_NCHITTEST;
    procedure WMPaint(var Msg: TWMPaint); message WM_PAINT;
  protected
    procedure ChangeScale(M, D: Integer); override;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure CreateWnd; override;
    procedure FillTabBG(DC: HDC; ActiveTab: Boolean; const TabRect: TRect);
      virtual;
    procedure GlyphHasChanged(Sender: TObject);
    procedure GlyphHasChangedPicture(Sender: TObject);
    procedure Loaded; override;
    procedure TabFontChanged(Sender: TObject); virtual;
    function TabTextOut(Rect: TRect; Margin: TPoint; Text: string;
      Align: TAlignment; Disabled: Boolean): TPoint; virtual;
  public
    PaintFlickerFree: Boolean;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure BeginUpdate; virtual;
    procedure DefaultDrawTab(Index: Integer; const RectFg: TRect;
      State: TOwnerDrawState); virtual;
    procedure DestroyAllOtherSheetHandles; virtual;
    procedure DrawTab(Index: Integer; ActiveTab: Boolean;
      const RectFg, RectBg: TRect; State: TOwnerDrawState);
{$IFDEF IS_DELPHI4_OR_HIGHER} reintroduce;
{$ENDIF} virtual;
    procedure EndUpdate; virtual;
    function GetGlyphIndex(PgIndex: Integer): Integer; virtual;
    function GetPageIndexByTabIndex(CurrentTabIndex: Integer): Integer;
    procedure GetSheetRect(var SheetRect: TRect); virtual;
    function GetTabNumber(CurrentPos: TPoint): Integer; virtual;
    function GetTabRect(TabIndex: Integer): TRect; virtual;
    procedure HottrackChange(Cold, Hot: Integer); virtual;
    function IsNewComCtl: Boolean;
    procedure ReAlignTabSheets(Adamant: Boolean); virtual;
    procedure SelectNextEnabledPage(GoForward: Boolean; FullCycle: Boolean);
    property Canvas: TCanvas read FCanvas;
    property Tabs;
  published
    property ActivePage: TTabSheet
      read GetActivePage
      write SetActivePage;

    property Color;

    property Hottrack: TExtHottrack
      read FHottrack
      write SetHottrack;

    property InitialPage: Integer
      read FInitialPage
      write FInitialPage
      default -1;

    property ParentColor;

    property TabActiveColor: TColor
      read FTabActiveColor
      write SetTabActiveColor;

    property TabActiveFont: TFont
      read FTabActiveFont
      write SetTabActiveFont;

    property TabGlyphs: TExtGlyphs
      read FTabGlyphs
      write SetTabGlyphs;

    property TabHeight: Word
      read FTabHeight
      write SetTabHeight;

    property TabHints: TStringList
      read FTabHints
      write SetTabHints;

    property TabInactiveColor: TColor
      read FTabInactiveColor
      write SetTabInactiveColor;

    property TabInactiveFont: TFont
      read FTabInactiveFont
      write SetTabInactiveFont;

    property TabPosition: TExtTabPosition
      read FTabPosition
      write SetTabPosition
      default tabPosTop;

    property TabStyle: TExtTabStyle
      read FTabStyle
      write SetTabStyle
      default tabStDefault;

    property TabWidth: Word
      read FTabWidth
      write SetTabWidth;

    property TabWordWrap: Boolean
      read FTabWordWrap
      write SetTabWordWrap
      default False;

    property OnDrawBkgndInside: TExtDrawBkgndInside
      read FOnDrawBkgndInside
      write FOnDrawBkgndInside;

    property OnDrawBkgndOutside: TExtDrawBkgndOutside
      read FOnDrawBkgndOutside
      write FOnDrawBkgndOutside;

    property OnDrawTab: TExtNbDrawTabEvent
      read FOnDrawTab
      write FOnDrawTab;
  end;

  TExtPageControlTag = class(TExtPageControl)
  public
    function GetGlyphIndex(PgIndex: Integer): Integer; override;
  end;

  TExtTabControl = class(TTabControl)
  private
    FCanvas: TCanvas;
    FHottrack: TExtHottrack;
    FHintOrg: string;
    FInitialTab: Integer;
    FirstPaint: Boolean;
    FLastTabHintIndex: Integer;
    FMouseInControl: Boolean;
    FOnDrawBkgndInside: TExtDrawBkgndInside;
    FOnDrawBkgndOutside: TExtDrawBkgndOutside;
    FOnDrawTab: TExtNbDrawTabEvent;
    FTabActiveColor: TColor;
    FTabActiveFont: TFont;
    FTabDisabledList: TList;
    FTabGlyphs: TExtGlyphs;
    FTabHeight: Word;
    FTabHints: TStringList;
    FTabInactiveColor: TColor;
    FTabInactiveFont: TFont;
    FTabPosition: TExtTabPosition;
    FTabStyle: TExtTabStyle;
    FTabWidth: Word;
    FTabWordWrap: Boolean;
    StopBGPainting: Integer;
    StopBGPaintingTabs: Boolean;
    StopFGPainting: Integer;
    procedure CheckAndSetHWndFont;
    function GetTabEnabled(Index: Integer): Boolean;
    function GetTabIndex: Integer;
    procedure SetHottrack(Value: TExtHottrack);
    procedure SetTabActiveColor(Value: TColor);
    procedure SetTabActiveFont(Value: TFont);
    procedure SetTabEnabled(Index: Integer; Value: Boolean);
    procedure SetTabGlyphs(Value: TExtGlyphs);
    procedure SetTabHeight(Value: Word);
    procedure SetTabHints(Value: TStringList);
    procedure SetTabInactiveColor(Value: TColor);
    procedure SetTabInactiveFont(Value: TFont);
    procedure SetTabIndex(Value: Integer);
    procedure SetTabPosition(Value: TExtTabPosition);
    procedure SetTabStyle(Value: TExtTabStyle);
    procedure SetTabWidth(Value: Word);
    procedure SetTabWordWrap(Value: Boolean);
    procedure CMDialogChar(var Msg: TCMDialogChar); message CM_DIALOGCHAR;
    procedure CMFontChanged(var Msg: TMessage); message CM_FONTCHANGED;
    procedure CMHintShow(var Msg: TMessage); message CM_HINTSHOW;
    procedure CMMouseEnter(var Msg: TMessage); message CM_MOUSEENTER;
    procedure CMMouseLeave(var Msg: TMessage); message CM_MOUSELEAVE;
    procedure CMParentColorChanged(var Msg: TMessage); message
      CM_PARENTCOLORCHANGED;
    procedure CNDrawTab(var Msg: TWMDrawItem); message CN_DRAWITEM;
    procedure TCMInsertItem(var Msg: TMessage); message TCM_INSERTITEM;
    procedure WMEraseBkgnd(var Msg: TWMEraseBkgnd); message WM_ERASEBKGND;
    procedure WMLButtonDown(var Msg: TWMLButtonDown); message WM_LBUTTONDOWN;
    procedure WMNCHitTest(var Msg: TWMNCHitTest); message WM_NCHITTEST;
    procedure WMPaint(var Msg: TWMPaint); message WM_PAINT;
  protected
    procedure ChangeScale(M, D: Integer); override;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure CreateWnd; override;
    procedure FillTabBG(DC: HDC; ActiveTab: Boolean; const TabRect: TRect);
      virtual;
    procedure GlyphHasChanged(Sender: TObject);
    procedure GlyphHasChangedPicture(Sender: TObject);
    procedure Loaded; override;
    procedure TabFontChanged(Sender: TObject); virtual;
    function TabTextOut(Rect: TRect; Margin: TPoint; Text: string;
      Align: TAlignment; Disabled: Boolean): TPoint; virtual;
  public
    PaintFlickerFree: Boolean;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure BeginUpdate; virtual;
    procedure DefaultDrawTab(Index: Integer; const RectFg: TRect;
      State: TOwnerDrawState); virtual;
    procedure DrawTab(Index: Integer; ActiveTab: Boolean;
      const RectFg, RectBg: TRect; State: TOwnerDrawState);
{$IFDEF IS_DELPHI4_OR_HIGHER} reintroduce;
{$ENDIF} virtual;
    procedure EndUpdate; virtual;
    function GetGlyphIndex(TabIndex: Integer): Integer; virtual;
    procedure GetSheetRect(var SheetRect: TRect); virtual;
    function GetTabNumber(CurrentPos: TPoint): Integer; virtual;
    function GetTabRect(TabIndex: Integer): TRect; virtual;
    procedure HottrackChange(Cold, Hot: Integer); virtual;
    function IsNewComCtl: Boolean;
    property Canvas: TCanvas
      read FCanvas;

    property TabEnabled[Index: Integer]: Boolean
    read GetTabEnabled
      write SetTabEnabled;

    property Tabs;
  published
    property Color;

    property Hottrack: TExtHottrack
      read FHottrack
      write SetHottrack;

    property InitialTab: Integer
      read FInitialTab
      write FInitialTab
      default -1;

    property ParentColor;

    property TabActiveColor: TColor
      read FTabActiveColor
      write SetTabActiveColor;

    property TabActiveFont: TFont
      read FTabActiveFont
      write SetTabActiveFont;

    property TabGlyphs: TExtGlyphs
      read FTabGlyphs
      write SetTabGlyphs;

    property TabHeight: Word
      read FTabHeight
      write SetTabHeight;

    property TabHints: TStringList
      read FTabHints
      write SetTabHints;

    property TabInactiveColor: TColor
      read FTabInactiveColor
      write SetTabInactiveColor;

    property TabInactiveFont: TFont
      read FTabInactiveFont
      write SetTabInactiveFont;

    property TabIndex: Integer
      read GetTabIndex
      write SetTabIndex;

    property TabPosition: TExtTabPosition
      read FTabPosition
      write SetTabPosition
      default tabPosTop;

    property TabStyle: TExtTabStyle
      read FTabStyle
      write SetTabStyle
      default tabStDefault;

    property TabWidth: Word
      read FTabWidth
      write SetTabWidth;

    property TabWordWrap: Boolean
      read FTabWordWrap
      write SetTabWordWrap
      default False;

    property OnDrawBkgndInside: TExtDrawBkgndInside
      read FOnDrawBkgndInside
      write FOnDrawBkgndInside;

    property OnDrawBkgndOutside: TExtDrawBkgndOutside
      read FOnDrawBkgndOutside
      write FOnDrawBkgndOutside;

    property OnDrawTab: TExtNbDrawTabEvent
      read FOnDrawTab
      write FOnDrawTab;
  end;

{$IFNDEF EXTPAGE_FULL}{$INCLUDE EXTPAG1.INC}{$ENDIF}

procedure ExtDrawBkgndImage(Control: TWinControl; DC: HDC; const RectBg: TRect;
  ImageBg: TImage);

procedure ExtDrawBkgndImageRgn(Control: TWinControl; DC: HDC; RgnBg: HRgn;
  ImageBg: TImage);

procedure ExtDrawTabGraphic(Canvas: TCanvas; Graphic: TGraphic;
  const RectBg: TRect; Stretch: Boolean);

procedure ExtStretchBlt(DC: HDC; const Dest: TRect; Graphic: TGraphic; SrcLeft,
  SrcTop, SrcWidth, SrcHeight: Integer; CopyMode: DWORD; StretchWidth,
  StretchHeight: Integer);

procedure Register;

implementation

{$IFNDEF EXTPAGE_FULL}
uses
  Dialogs, ShellApi;
{$ENDIF}

var
  IsNewComCtl32Version: Boolean;

{ Support for painting tab / background image }

procedure ExtDrawBkgndImage(Control: TWinControl; DC: HDC; const RectBg: TRect;
  ImageBg: TImage);
var
  NewRect: TRect;
  ControlR: TRect;
  StretchX, StretchY: Integer;
begin
  if (ImageBg = nil) or (ImageBg.Picture = nil) then
    Exit;
  NewRect := RectBg;
  ControlR := Control.ClientRect;
  MapWindowPoints(Control.Handle, ImageBg.Parent.Handle, ControlR, 2);
  OffsetRect(NewRect, ControlR.Left - ImageBg.Left, ControlR.Top - ImageBg.Top);
  if ImageBg.Stretch then
  begin
    StretchX := ImageBg.Width;
    StretchY := ImageBg.Height;
  end
  else
  begin
    StretchX := 0;
    StretchY := 0;
  end;
  ExtStretchBlt(DC, RectBg, ImageBg.Picture.Graphic,
    NewRect.Left, NewRect.Top, NewRect.Right - NewRect.Left,
    NewRect.Bottom - NewRect.Top, SRCCOPY, StretchX, StretchY)
end;

procedure ExtDrawBkgndImageRgn(Control: TWinControl; DC: HDC; RgnBg: HRgn;
  ImageBg: TImage);
var
  RectBkgndOutside: TRect;
  RgnBkgndDataSize: Integer;
  RgnBkgndData: PRgnData;
  TempCount: Integer;
begin
  RgnBkgndDataSize := GetRegionData(RgnBg, 0, nil);
  if RgnBkgndDataSize > 0 then
  begin
    GetMem(RgnBkgndData, RgnBkgndDataSize);
    try
      FillChar(RgnBkgndData^, RgnBkgndDataSize, 0);
      GetRegionData(RgnBg, RgnBkgndDataSize, RgnBkgndData);
      for TempCount := 0 to RgnBkgndData^.rdh.nCount - 1 do
      begin
{$RANGECHECKS OFF}
        Move(RgnBkgndData^.Buffer[TempCount * SizeOf(RectBkgndOutside)],
          RectBkgndOutside, SizeOf(RectBkgndOutside));
{$IFDEF DEBUG}{$RANGECHECKS ON}{$ENDIF}
        ExtDrawBkgndImage(Control, DC, RectBkgndOutside, ImageBg);
      end;
    finally
      FreeMem(RgnBkgndData);
    end;
  end;
end;

procedure ExtDrawTabGraphic(Canvas: TCanvas; Graphic: TGraphic;
  const RectBg: TRect; Stretch: Boolean);
var
  MaxX, MaxY: Integer;
begin
  if (Graphic <> nil) and (not Graphic.Empty) then
  begin
    if Stretch then
      Canvas.StretchDraw(RectBg, Graphic)
    else
    begin
      MaxX := Graphic.Width;
      if MaxX > (RectBg.Right - RectBg.Left) then
        MaxX := RectBg.Right - RectBg.Left;

      MaxY := Graphic.Height;
      if MaxY > (RectBg.Bottom - RectBg.Top) then
        MaxY := RectBg.Bottom - RectBg.Top;

      ExtStretchBlt(Canvas.Handle, RectBg, Graphic,
        0, 0, MaxX, MaxY, CMSRCCOPY, 0, 0);
    end;
  end;
end;

procedure ExtStretchBlt(DC: HDC; const Dest: TRect; Graphic: TGraphic; SrcLeft,
  SrcTop, SrcWidth, SrcHeight: Integer; CopyMode: DWORD; StretchWidth,
  StretchHeight: Integer);
var
  MemDC: HDC;
  MemBitmap: HBitmap;
  OldMemBitmap: HBitmap;
begin
  MemDC := CreateCompatibleDC(DC);
  try
    MemBitmap := CreateCompatibleBitmap(DC, SrcWidth, SrcHeight);
    try
      OldMemBitmap := SelectObject(MemDC, MemBitmap);
      try
        with TCanvas.Create do
        try
          Handle := MemDC;
          Brush.Color := GetBkColor(DC);
          FillRect(Rect(0, 0, SrcWidth, SrcHeight));
          if (StretchWidth > 0) and (StretchHeight > 0) then
            StretchDraw(Rect(-SrcLeft, -SrcTop, -SrcLeft + StretchWidth,
              -SrcTop + StretchHeight), Graphic)
          else
            Draw(-SrcLeft, -SrcTop, Graphic);
        finally
          Free;
        end;
        StretchBlt(DC, Dest.Left, Dest.Top, Dest.Right - Dest.Left,
          Dest.Bottom - Dest.Top, MemDC, 0, 0, SrcWidth, SrcHeight, CopyMode);
      finally
        SelectObject(MemDC, OldMemBitmap);
      end;
    finally
      DeleteObject(MemBitmap);
    end;
  finally
    DeleteDC(MemDC);
  end;
end;

// Retrieve rectangle of the small buttons in a Win-TabControl

procedure GetTabControl_BtnRect(TabCHndle: HWnd; var ABtnRect: TRect);
var
  BtnHndle: HWnd;
begin
  BtnHndle := FindWindowEx(TabCHndle, 0, 'msctls_updown32', nil);
  if BtnHndle <> 0 then
  begin
    GetWindowRect(BtnHndle, ABtnRect);
    MapWindowPoints(0, TabCHndle, ABtnRect, 2);
  end
  else
  begin
    ABtnRect.Left := 0;
    ABtnRect.Top := 0;
    ABtnRect.Right := 0;
    ABtnRect.Bottom := 0;
  end;
end;

{ TExtGlyphs }

constructor TExtGlyphs.Create(AOwner: TControl);
begin
  FGlpyhOwner := AOwner;
  FNumGlyphs := 0;
  FImageList := TImageList.Create(AOwner);
  FGlyph := TBitmap.Create;
  FGlyph.OnChange := ChangedGlyph;
  FGlyphPaint := TBitmap.Create;
  FPosition := glPosLeft;
  FSpacing := 5;
  FTransparentColor := clFuchsia;
end;

destructor TExtGlyphs.Destroy;
begin
  FGlyphPaint.Free;
  FImageList.Free;
  FGlyph.Free;
  inherited;
end;

procedure TExtGlyphs.Changed(Sender: TObject);
begin
  if Assigned(FOnChange) then
    FOnChange(Self);
end;

procedure TExtGlyphs.ChangedGlyph(Sender: TObject);
begin
  FGlyphPaint.Assign(FGlyph);
  FImageList.Clear;
  if (FNumGlyphs > 0) and (not FGlyphPaint.Empty) then
  begin
    FImageList.Height := FGlyphPaint.Height;
    FImageList.Width := FGlyphPaint.Width div FNumGlyphs;
    FImageList.AddMasked(FGlyphPaint, FTransparentColor);
  end;
  if Assigned(FOnChangeGlyph) then
    FOnChangeGlyph(Self);
end;

procedure TExtGlyphs.SetGlyph(Value: TBitmap);
begin
  FGlyph.Assign(Value);
end;

procedure TExtGlyphs.SetImageList(Value: TImageList);
var
  i: Integer;
begin
  if Value <> nil then
  begin
    FGlyph.ReleaseHandle;
    FGlyph.Width := Value.Width * Value.Count;
    FGlyph.Height := Value.Height;
    FGlyph.Canvas.Brush.Color := FTransparentColor;
    FGlyph.Canvas.FillRect(Rect(0, 0, FGlyph.Width, FGlyph.Height));
    for i := 0 to Value.Count - 1 do
      Value.Draw(FGlyph.Canvas, i * Value.Width, 0, i);
    FNumGlyphs := Value.Count;
    ChangedGlyph(Self);
  end;
end;

procedure TExtGlyphs.SetNumGlyphs(Value: Word);
begin
  if Value <> FNumGlyphs then
  begin
    FNumGlyphs := Value;
    ChangedGlyph(Self);
  end;
end;

procedure TExtGlyphs.SetPosition(Value: TExtGlyphPosition);
begin
  if Value <> FPosition then
  begin
    FPosition := Value;
    Changed(Self);
  end;
end;

procedure TExtGlyphs.SetSpacing(Value: Integer);
begin
  if Value <> FSpacing then
  begin
    FSpacing := Value;
    Changed(Self);
  end;
end;

procedure TExtGlyphs.SetTransparentColor(Value: TColor);
begin
  if Value <> FTransparentColor then
  begin
    FTransparentColor := Value;
    ChangedGlyph(Self);
  end;
end;

{ TExtHottrack }

constructor TExtHottrack.Create(AOwner: TControl);
begin
  FHottrackOwner := AOwner;
  FEnabled := False;
  FTabActiveColor := clHighlight;
  FTabInactiveColor := clHighlight;
  TabTracked := -1;
end;

procedure TExtHottrack.SetEnabled(Value: Boolean);
begin
  if Value <> FEnabled then
  begin
    FEnabled := Value;
    FHottrackOwner.Invalidate;
  end;
end;

procedure TExtHottrack.SetTabActiveColor(Value: TColor);
begin
  if FTabActiveColor <> Value then
  begin
    FTabActiveColor := Value;
    FHottrackOwner.Invalidate;
  end;
end;

procedure TExtHottrack.SetTabInactiveColor(Value: TColor);
begin
  if FTabInactiveColor <> Value then
  begin
    FTabInactiveColor := Value;
    FHottrackOwner.Invalidate;
  end;
end;

{ TExtPageControl }

constructor TExtPageControl.Create(AOwner: TComponent);
begin
  inherited;
  FCanvas := TControlCanvas.Create;
  FHottrack := TExtHottrack.Create(Self);
  FInitialPage := -1;
  FirstPaint := True;
  FLastTabHintIndex := -1;
  FMouseInControl := False;
  FTabHints := TStringList.Create;
  FTabHints.Sorted := False;
  FTabActiveColor := clBtnFace;
  FTabActiveFont := TFont.Create;
  FTabActiveFont.OnChange := TabFontChanged;
  FTabGlyphs := TExtGlyphs.Create(Self);
  FTabGlyphs.OnChange := GlyphHasChanged;
  FTabGlyphs.OnChangeGlyph := GlyphHasChangedPicture;
  FTabInactiveColor := RGB(160, 160, 164);
  FTabInactiveFont := TFont.Create;
  FTabInactiveFont.OnChange := TabFontChanged;
  FTabHeight := inherited TabHeight;
  FTabWidth := inherited TabWidth;
  FTabPosition := tabPosTop;
  FTabStyle := tabStDefault;
  FTabWordWrap := False;
  PaintFlickerFree := False;
  StopBGPainting := 0;
  StopBGPaintingTabs := False;
  StopFGPainting := 0;
end;

destructor TExtPageControl.Destroy;
begin
  FTabActiveFont.Free;
  FTabGlyphs.Free;
  FTabInactiveFont.Free;
  FHottrack.Free;
  FCanvas.Free;
  FTabHints.Free;
  inherited;
end;

procedure TExtPageControl.CreateParams(var Params: TCreateParams);
begin
  inherited;
  Params.Style := Params.Style or TCS_OWNERDRAWFIXED;
  case FTabPosition of
    tabPosLeft: Params.Style := Params.Style or TCS_VERTICAL and (not
        TCS_RIGHT);
    tabPosRight: Params.Style := Params.Style or TCS_VERTICAL or TCS_RIGHT;
    tabPosTop: Params.Style := Params.Style and (not TCS_VERTICAL) and (not
        TCS_BOTTOM);
    tabPosBottom: Params.Style := Params.Style and (not TCS_VERTICAL) or
      TCS_BOTTOM;
  end;
  if FTabStyle = tabStButton then
    Params.Style := Params.Style or TCS_BUTTONS;
end;

procedure TExtPageControl.CreateWnd;
begin
  inherited;
  CheckAndSetHWndFont;
  if not FTabGlyphs.FGlyph.Empty then
    Perform(TCM_SETIMAGELIST, 0, LPARAM(TabGlyphs.ImageList.Handle));
end;

procedure TExtPageControl.TCMInsertItem(var Msg: TMessage);
var
  TabItemStruct: TTCItem;
begin
  inherited;
  TabItemStruct.Mask := TCIF_IMAGE;
  TabItemStruct.iImage := 0;
  Perform(TCM_SETITEM, Msg.WParam, LPARAM(@TabItemStruct));
end;

procedure TExtPageControl.ChangeScale(M, D: Integer);
begin
  inherited;
  TabHeight := MulDiv(FTabHeight, M, D);
  TabWidth := MulDiv(FTabWidth, M, D);
  TabActiveFont.Height := MulDiv(FTabActiveFont.Height, M, D);
  TabInactiveFont.Height := MulDiv(FTabInactiveFont.Height, M, D);
end;

function TExtPageControl.GetTabNumber(CurrentPos: TPoint): Integer;
var
  HitTestInfo: TTCHitTestInfo;
begin
  if HandleAllocated then
  begin
    HitTestInfo.pt := CurrentPos;
    Result := Perform(TCM_HITTEST, 0, LParam(@HitTestInfo));
  end
  else
    Result := -1;
end;

procedure TExtPageControl.Loaded;
begin
  inherited;
  FHintOrg := Hint;
  if (FInitialPage >= 0) and (not (csDesigning in componentState)) then
    inherited ActivePage := Pages[FInitialPage];
end;

procedure TExtPageControl.TabFontChanged(Sender: TObject);
begin
  if HandleAllocated then
  begin
    CheckAndSetHWndFont;
    Invalidate;
  end;
end;

procedure TExtPageControl.CheckAndSetHWndFont;
var
  TempDC: HDC;
  OldFont: HFont;
  WidthActive, WidthInactive: Integer;
begin
  // We let Windows PageControl know what's the font of the tabs.
  // With this information it calculates height and width
  // itself, if TabHeight or TabWidth = 0
  TempDC := GetDC(0);
  try
    OldFont := SelectObject(TempDC, FTabActiveFont.Handle);
    try
      WidthActive := 0;
      WidthInActive := 0;
      // function GetCharWidth32 is not implemented in Win95!
      GetCharWidth(TempDC, Ord('W'), Ord('W'), WidthActive);
      SelectObject(TempDC, FTabInActiveFont.Handle);
      GetCharWidth(TempDC, Ord('W'), Ord('W'), WidthInactive);
    finally
      SelectObject(TempDC, OldFont);
    end;
  finally
    ReleaseDC(0, TempDC);
  end;

  if WidthInactive > WidthActive then
    Perform(WM_SETFONT, WPARAM(FTabInActiveFont.Handle), 0)
  else
    Perform(WM_SETFONT, WPARAM(FTabActiveFont.Handle), 0);
end;

function TExtPageControl.GetActivePage: TTabSheet;
begin
  GetActivePage := inherited ActivePage;
end;

procedure TExtPageControl.SetActivePage(Value: TTabSheet);
begin
  if Value = nil then
    inherited ActivePage := Value
  else
    if Value <> inherited ActivePage then
    begin
      if Value.Enabled or (csDesigning in ComponentState) then
      begin
        if csLoading in ComponentState then
          inherited ActivePage := Value
        else
          if CanChange then
          begin
            inherited ActivePage := Value;
            RealignTabSheets(False);
            if Assigned(OnChange) then
              OnChange(Self);
          end;
      end;
    end;
end;

procedure TExtPageControl.SetHottrack(Value: TExtHottrack);
begin
  // no action
end;

procedure TExtPageControl.SetTabActiveColor(Value: TColor);
begin
  if FTabActiveColor <> Value then
  begin
    FTabActiveColor := Value;
    if HandleAllocated then
      InvalidateRect(WindowHandle, nil, False);
  end;
end;

procedure TExtPageControl.SetTabHints(Value: TStringList);
begin
  FTabHints.Assign(Value);
end;

procedure TExtPageControl.SetTabInactiveColor(Value: TColor);
begin
  if FTabInactiveColor <> Value then
  begin
    FTabInactiveColor := Value;
    if HandleAllocated then
      InvalidateRect(WindowHandle, nil, False);
  end;
end;

procedure TExtPageControl.SetTabActiveFont(Value: TFont);
begin
  FTabActiveFont.Assign(Value);
end;

procedure TExtPageControl.SetTabGlyphs(Value: TExtGlyphs);
begin
  // no action
end;

procedure TExtPageControl.SetTabHeight(Value: Word);
begin
  if FTabHeight <> Value then
  begin
    FTabHeight := Value;
    case FTabPosition of
      tabPosLeft, tabPosRight: inherited TabWidth := Value;
    else
      inherited TabHeight := Value;
    end;
    ReAlignTabSheets(False);
  end;
end;

procedure TExtPageControl.SetTabInactiveFont(Value: TFont);
begin
  FTabInactiveFont.Assign(Value);
end;

procedure TExtPageControl.SetTabPosition(Value: TExtTabPosition);

  procedure UpdateInherTabSizeNormal;
  begin
    inherited TabWidth := FTabWidth;
    inherited TabHeight := FTabHeight;
  end;

  procedure UpdateInherTabSizeOpposite;
  begin
    if (Value in [tabPosLeft, tabPosRight]) then
    begin
      if FTabHeight = 0 then
        FTabHeight := 35;
      if FTabWidth = 0 then
        FTabWidth := 70;
    end;
    inherited TabWidth := FTabHeight;
    inherited TabHeight := FTabWidth;
  end;

begin
  if FTabPosition <> Value then
  begin
    BeginUpdate;
    try
      // update inherited tab-height and tab-with if necessary
      case FTabPosition of
        tabPosLeft:
          if Value <> tabPosRight then
            UpdateInherTabSizeNormal;
        tabPosRight:
          if Value <> tabPosLeft then
            UpdateInherTabSizeNormal;
        tabPosTop:
          if Value <> tabPosBottom then
            UpdateInherTabSizeOpposite;
        tabPosBottom:
          if Value <> tabPosTop then
            UpdateInherTabSizeOpposite;
      end;
      FTabPosition := Value;

      if (Value <> tabPosTop) and (FTabStyle = tabStButton) then
        TabStyle := tabStDefault;
      if HandleAllocated then
      begin
        ReCreateWnd;
        ReAlignTabSheets(False);
      end;
    finally
      EndUpdate;
    end;
  end;
end;

procedure TExtPageControl.SetTabStyle(Value: TExtTabStyle);
var
  OldWStyle: Longint;
begin
  if FTabStyle <> Value then
  begin
    FTabStyle := Value;
    if FTabStyle = tabStButton then
      TabPosition := tabPosTop;
    if HandleAllocated then
    begin
      OldWStyle := GetWindowLong(WindowHandle, GWL_STYLE);
      if Value = tabStButton then
        SetWindowLong(WindowHandle, GWL_STYLE, OldWStyle or TCS_BUTTONS)
      else
        SetWindowLong(WindowHandle, GWL_STYLE, OldWStyle and (not TCS_BUTTONS));
    end;
  end;
end;

procedure TExtPageControl.SetTabWidth(Value: Word);
begin
  if FTabWidth <> Value then
  begin
    FTabWidth := Value;
    case FTabPosition of
      tabPosLeft, tabPosRight: inherited TabHeight := Value;
    else
      inherited TabWidth := Value;
    end;
    ReAlignTabSheets(False);
  end;
end;

procedure TExtPageControl.SetTabWordWrap(Value: Boolean);
begin
  if FTabWordWrap <> Value then
  begin
    FTabWordWrap := Value;
    if HandleAllocated then
      InvalidateRect(WindowHandle, nil, False);
  end;
end;

procedure TExtPageControl.CMDialogChar(var Msg: TCMDialogChar);
var
  TabCounter: Integer;
begin
  if CanFocus and ((Msg.KeyData and $20000000) <> 0) then
  begin // ALT-key pressed
    for TabCounter := 0 to Tabs.Count - 1 do
      if IsAccel(Msg.CharCode, Tabs[TabCounter]) then
      begin
        SetActivePage(TTabSheet(Tabs.Objects[TabCounter]));
        Msg.Result := 1;
        Exit;
      end;
  end;
  inherited;
end;

procedure TExtPageControl.CMFontChanged(var Msg: TMessage);
begin
  Invalidate;
  NotifyControls(CM_PARENTFONTCHANGED);
end;

procedure TExtPageControl.CMHintShow(var Msg: TMessage);
var
  InfHint: ^THintInfo;
  TabIndex: Integer;
  CurrCurPos: TPoint;
  NewHintPos: TPoint;
  CurrTabRect: TRect;
begin
  if GetCursorPos(CurrCurPos) then
  begin
    Windows.ScreenToClient(Handle, CurrCurPos);
    TabIndex := GetTabNumber(CurrCurPos);
    if TabIndex >= 0 then
    begin
      // Hint should appear at Tab
      if Perform(TCM_GETITEMRECT, TabIndex, LParam(@CurrTabRect)) <> 0 then
      begin
        InfHint := Pointer(Msg.LParam);
        NewHintPos := Point(CurrTabRect.Left + 3, CurrTabRect.Bottom + 1);
        Windows.ClientToScreen(Handle, NewHintPos);
        InfHint^.HintPos := NewHintPos;
      end;
    end;
  end;
end;

procedure TExtPageControl.CMParentColorChanged(var Msg: TMessage);
begin
  inherited;
  Invalidate;
end;

{++ Moskaliov BUG#268 BUILD#115 17.05.00}
{$RANGECHECKS OFF}
{-- Moskaliov BUG#268 BUILD#115 17.05.00}

procedure TExtPageControl.WMPaint(var Msg: TWMPaint);
var
  IsOurDC: Boolean;
  OldMsgDC, PaintDC: HDC;
  PaintStr: TPaintStruct;
  OldBitmap, MemBitmap: HBitmap;
  MemDC: HDC;
  PaintInRect: TRect;
  Form: {$IFDEF IS_DELPHI3_OR_HIGHER}TCustomForm{$ELSE}TForm{$ENDIF};
begin
  if StopFGPainting > 0 then
  begin
    if HandleAllocated then
      ValidateRect(WindowHandle, nil);
    Msg.Result := 1;
    Exit;
  end;

  OldMsgDC := Msg.DC;
  if Msg.DC = 0 then
  begin
    Inc(StopBGPainting);
    PaintDC := BeginPaint(Handle, PaintStr);
    PaintInRect := PaintStr.rcPaint;
    Dec(StopBGPainting);
    IsOurDC := True;
  end
  else
  begin
    PaintDC := Msg.DC;
    PaintInRect := ClientRect;
    IsOurDC := False;
  end;

  try
    Form := TForm(GetParentForm(Self));
    if (not PaintFlickerFree) and (FirstPaint or
      ((Form <> nil) and (not Form.Active))) then
    begin
      FirstPaint := False;
      // faster direct painting if first time or inactive
      Msg.DC := PaintDC;
      Perform(WM_ERASEBKGND, Msg.DC, 0);
      inherited;
    end
    else
    begin
      // slower indirect (flickerfree) painting otherwise
      MemBitmap := CreateCompatibleBitmap(PaintDC, ClientWidth, ClientHeight);
      try
        MemDC := CreateCompatibleDC(PaintDC);
        try
          OldBitmap := SelectObject(MemDC, MemBitmap);
          StopBGPaintingTabs := True;
          try
            Perform(WM_ERASEBKGND, MemDC, 0);
          finally
            StopBGPaintingTabs := False;
          end;

          try
            Msg.DC := MemDC;
            // paint into Offscreen-DC
            inherited;
            BitBlt(PaintDC, PaintInRect.Left, PaintInRect.Top,
              PaintInRect.Right - PaintInRect.Left,
              PaintInRect.Bottom - PaintInRect.Top,
              MemDC, PaintInRect.Left, PaintInRect.Top, SRCCOPY);
          finally
            SelectObject(MemDC, OldBitmap);
          end;
        finally
          DeleteDC(MemDC);
        end;
      finally
        DeleteObject(MemBitmap);
      end;
    end;

  finally
    if IsOurDC then
      EndPaint(Handle, PaintStr);
    Msg.DC := OldMsgDC;
    PaintFlickerFree := False;
  end;
end;
{++ Moskaliov BUG#268 BUILD#115 17.05.00}
{$IFDEF DEBUG}{$RANGECHECKS ON}{$ENDIF}
{-- Moskaliov BUG#268 BUILD#115 17.05.00}

procedure TExtPageControl.CNDrawTab(var Msg: TWMDrawItem);
var
  FgRect, BgRect: TRect;
  State: TOwnerDrawState;
  CurrPageIdx: Integer;
begin
  with Msg.DrawItemStruct^ do
  begin
    if Integer(itemID) < 0 then
      Exit;
    CurrPageIdx := GetPageIndexByTabIndex(itemID);
    State := TOwnerDrawState(
{$IFDEF IS_DELPHI4_OR_LOWER}
      WordRec(LongRec(itemState).Lo).Lo
{$ELSE}
      LongRec(itemState).Lo
{$ENDIF}
      );
    if not Pages[CurrPageIdx].Enabled then
      Include(State, odDisabled);

    // retrieve Foreground Rect
    Perform(TCM_GETITEMRECT, itemID, LParam(@FgRect));
    if (odSelected in State) and (FTabStyle = tabStDefault) then
    begin
      // active Tab
      BgRect := rcItem;
      case FTabPosition of
        tabPosLeft: OffsetRect(FgRect, -2, 0);
        tabPosRight: OffsetRect(FgRect, 2, 0);
        tabPosTop: OffsetRect(FgRect, 0, -1);
      end;
    end
    else
    begin
      // inactive Tab
      BgRect := FgRect;
      if FTabStyle = tabStDefault then
        case FTabPosition of
          tabPosTop: OffsetRect(FgRect, 0, 1);
          tabPosBottom: OffsetRect(FgRect, 0, -2);
        end;
    end;

    FCanvas.Handle := hDC;
    FCanvas.Brush := Brush;
    try
      if odSelected in State then
      begin
        // paint the active tab
        FCanvas.Brush.Color := FTabActiveColor;
        FCanvas.Font := FTabActiveFont;
        if FHottrack.Enabled and (FHottrack.FTabActiveColor <> clNone) and
          (FHottrack.TabTracked = Integer(itemID)) then
          FCanvas.Font.Color := FHottrack.FTabActiveColor;
        DrawTab(CurrPageIdx, True, FgRect, BgRect, State);
      end
      else
      begin
        // paint an inactive tab
        FCanvas.Brush.Color := FTabInactiveColor;
        FCanvas.Font := FTabInactiveFont;
        if FHottrack.Enabled and (FHottrack.FTabInactiveColor <> clNone) and
          (FHottrack.TabTracked = Integer(itemID)) then
          FCanvas.Font.Color := FHottrack.FTabInactiveColor;
        DrawTab(CurrPageIdx, False, FgRect, BgRect, State);
      end;
    finally
      FCanvas.Handle := 0;
    end;
  end;
end;

procedure TExtPageControl.DefaultDrawTab(Index: Integer; const RectFg: TRect;
  State: TOwnerDrawState);
const
  DistanceMargin = 8;
var
  ImagePosLeft: Integer;
  ImagePosTop: Integer;
begin
  if FTabGlyphs.FGlyph.Empty then
  begin
    // draw only text
    case FTabPosition of
      tabPosLeft, tabPosRight:
        TabTextOut(RectFg, Point(DistanceMargin, 0), Pages[Index].Caption,
          taLeftJustify, odDisabled in State);
      tabPosTop, tabPosBottom:
        TabTextOut(RectFg, Point(0, 0), Pages[Index].Caption,
          taCenter, odDisabled in State);
    end;
  end
  else
  begin
    // draw bitmap if Tab enabled
    if odDisabled in State then
      TabTextOut(RectFg, Point(0, 0), Pages[Index].Caption, taCenter, True)
    else
      case FTabGlyphs.FPosition of
        glPosLeft:
          begin // Tab image is left beside Text
            case FTabPosition of
            // Tabs on Left or Right -> Alignment Left
              tabPosLeft, tabPosRight:
                begin
                  ImagePosLeft := RectFg.Left + DistanceMargin;
                  TabTextOut(RectFg, Point(DistanceMargin +
                    TabGlyphs.ImageList.Width +
                    FTabGlyphs.FSpacing, 0), Pages[Index].Caption,
                    taLeftJustify, False);
                end;
            else
            // Tabs on Top or Bottom -> Alignment Center
              ImagePosLeft := TabTextOut(RectFg, Point(TabGlyphs.ImageList.Width
                +
                FTabGlyphs.FSpacing, 0), Pages[Index].Caption, taCenter,
                False).X;
            end;
            TabGlyphs.ImageList.Draw(FCanvas, ImagePosLeft, RectFg.Top +
              (RectFg.Bottom -
              RectFg.Top - TabGlyphs.ImageList.Height) div 2,
              GetGlyphIndex(Index));
          end;
        glPosTop:
          begin // Tab image is above Text
            ImagePosTop := TabTextOut(RectFg, Point(0, TabGlyphs.ImageList.Height
              +
              FTabGlyphs.FSpacing), Pages[Index].Caption, taCenter, False).Y;
            TabGlyphs.ImageList.Draw(FCanvas, RectFg.Left + (RectFg.Right -
              RectFg.Left -
              TabGlyphs.ImageList.Width) div 2, ImagePosTop,
              GetGlyphIndex(Index));
          end;
      end;
  end;
end;

procedure TExtPageControl.DrawTab(Index: Integer; ActiveTab: Boolean;
  const RectFg, RectBg: TRect; State: TOwnerDrawState);
begin
  FillRect(FCanvas.Handle, RectBg, FCanvas.Brush.Handle);
  if Assigned(FOnDrawTab) then
    FOnDrawTab(Self, Index, ActiveTab, RectFg, RectBg, State)
  else
    DefaultDrawTab(Index, RectFg, State);
end;

type
  TCrackerTabSheet = class(TTabSheet);

procedure TExtPageControl.DestroyAllOtherSheetHandles;
var
  TempPageCounter: Integer;
begin
  for TempPageCounter := 0 to PageCount - 1 do
  begin
    if Pages[TempPageCounter] <> ActivePage then
      TCrackerTabSheet(Pages[TempPageCounter]).DestroyHandle;
  end;
end;

procedure TExtPageControl.BeginUpdate;
begin
  // BackGround painting is stopped, if StopBGPainting > 0
  Inc(StopBGPainting);
  // ForeGround painting is stopped, if StopFGPainting > 0
  Inc(StopFGPainting);
end;

procedure TExtPageControl.EndUpdate;
begin
  // BackGround painting goes on if StopBGPainting is 0
  if StopBGPainting > 0 then
    Dec(StopBGPainting);
  // ForeGround painting goes on if StopFGPainting is 0
  if StopFGPainting > 0 then
    Dec(StopFGPainting);
  if (StopBGPainting = 0) and (StopFGPainting = 0) then
    Invalidate;
end;

procedure TExtPageControl.FillTabBG(DC: HDC; ActiveTab: Boolean; const TabRect:
  TRect);
// paint the background of one tab
var
  TabsBrush, OldBrush: HBrush;
begin
  if ActiveTab then
    TabsBrush := CreateSolidBrush(ColorToRGB(FTabActiveColor))
  else
    TabsBrush := CreateSolidBrush(ColorToRGB(FTabInactiveColor));
  try
    OldBrush := SelectObject(DC, TabsBrush);
    try
      FillRect(DC, TabRect, TabsBrush);
    finally
      SelectObject(DC, OldBrush);
    end;
  finally
    DeleteObject(TabsBrush);
  end;
end;

procedure TExtPageControl.WMEraseBkgnd(var Msg: TWMEraseBkgnd);
var
  ActiveTabIdx: Integer;
  SingleTabRect: TRect;
  SheetAreaRect: TRect;
  RgnTemp, RgnBkgnd: HRgn;
  TabCounter: Integer;
  BtnRect: TRect;
  IntersSect: TRect;
begin
  if StopBGPainting > 0 then
  begin
    Msg.Result := 1;
    Exit;
  end;
  SetBrushOrgEx(Msg.DC, Left mod 8, Top mod 8, nil);
//{$IFDEF IS_DELPHI3_OR_HIGHER}nil{$ELSE}PPoint(nil)^{$ENDIF});

  // retrieve Sheet-Area
  GetSheetRect(SheetAreaRect);
  FillRect(Msg.DC, SheetAreaRect, Brush.Handle);
  if Assigned(FOnDrawBkgndInside) then
    FOnDrawBkgndInside(Self, Msg.DC, GetPageIndexByTabIndex(TabIndex),
      SheetAreaRect);

  // take out Sheet-Area from Background Area
  RgnBkgnd := CreateRectRgnIndirect(ClientRect);
  try
    RgnTemp := CreateRectRgnIndirect(SheetAreaRect);
    try
      CombineRgn(RgnBkgnd, RgnBkgnd, RgnTemp, RGN_DIFF);
    finally
      DeleteObject(RgnTemp);
    end;

   // retrieve and fill Tabs if necessary
    if (Tabs.Count > 0) and (not StopBGPaintingTabs) then
    begin
      ActiveTabIdx := Perform(TCM_GETCURSEL, 0, 0);
      GetTabControl_BtnRect(Handle, BtnRect);
      for TabCounter := 0 to Tabs.Count - 1 do
      begin
        Perform(TCM_GETITEMRECT, TabCounter, LPARAM(@SingleTabRect));
        FillTabBG(Msg.DC, ActiveTabIdx = TabCounter, SingleTabRect);
        if not InterSectRect(IntersSect, SingleTabRect, BtnRect) then
        begin
          // take out this Tab from Background Area
          RgnTemp := CreateRectRgnIndirect(SingleTabRect);
          try
            CombineRgn(RgnBkgnd, RgnBkgnd, RgnTemp, RGN_DIFF);
          finally
            DeleteObject(RgnTemp);
          end;
        end;
      end;
    end;
    FillRgn(Msg.DC, RgnBkgnd, Parent.Brush.Handle);
    if Assigned(FOnDrawBkgndOutside) then
    begin
      SetBkColor(Msg.DC, ColorToRGB(Parent.Brush.Color));
      FOnDrawBkgndOutside(Self, Msg.DC, RgnBkgnd);
    end;
  finally
    DeleteObject(RgnBkgnd);
  end;
  Msg.Result := 1;
end;

procedure TExtPageControl.WMHelp(var Msg: TWMHelp);
var
  ClickedAtTab: Integer;
begin
  ClickedAtTab := GetTabNumber(ScreenToClient(Msg.HelpInfo^.MousePos));
  if ClickedAtTab >= 0 then
    Msg.HelpInfo^.hItemHandle :=
      Pages[GetPageIndexByTabIndex(ClickedAtTab)].Handle;
  inherited;
end;

procedure TExtPageControl.WMLButtonDown(var Msg: TWMLButtonDown);
var
  HitIndex: Integer;
begin
  if csDesigning in ComponentState then
    inherited
  else
  begin
    // filter mouse clicks at disabled pages
    HitIndex := GetTabNumber(SmallPointToPoint(Msg.Pos));
    if HitIndex >= 0 then
    begin
      if Pages[GetPageIndexByTabIndex(HitIndex)].Enabled then
        inherited;
    end
    else
      inherited;
  end;
end;

procedure TExtPageControl.WMNCHitTest(var Msg: TWMNCHitTest);
var
  TabIndexNew: Integer;
  OldHottrackTab: Integer;
  NowAtPos: TPoint;
begin
  inherited;
  if (not FMouseInControl) or (Msg.Result <> HTCLIENT) or (GetCapture <> 0) then
    Exit;

  NowAtPos := SmallPointToPoint(Msg.Pos);
  Windows.ScreenToClient(Handle, NowAtPos);
  TabIndexNew := GetTabNumber(NowAtPos);

  // check if another Hottrack color necessary
  if FHottrack.Enabled and (TabIndexNew <> FHottrack.TabTracked) then
  begin
    OldHottrackTab := FHottrack.TabTracked;
    FHottrack.TabTracked := TabIndexNew;
    HottrackChange(OldHottrackTab, FHottrack.TabTracked);
  end;

  // check if another Hint string necessary
  if ShowHint and (not (csDesigning in ComponentState)) and
    (TabIndexNew <> FLastTabHintIndex) then
  begin
    FLastTabHintIndex := TabIndexNew;
    if Application <> nil then
    begin
      Application.CancelHint;
      if (TabIndexNew >= 0) and (FTabHints.Count >= TabIndexNew + 1) then
        Hint := FTabHints[GetPageIndexByTabIndex(TabIndexNew)]
      else
        Hint := '';
    end;
  end;
end;

procedure TExtPageControl.CMMouseEnter(var Msg: TMessage);
begin
  FMouseInControl := True;
  inherited;
end;

procedure TExtPageControl.CMMouseLeave(var Msg: TMessage);
var
  OldHottrackTab: Integer;
begin
  FMouseInControl := False;
  // reset Hottrack
  if FHottrack.Enabled and (FHottrack.TabTracked <> -1) then
  begin
    OldHottrackTab := FHottrack.TabTracked;
    FHottrack.TabTracked := -1;
    HottrackChange(OldHottrackTab, -1);
  end;
  // reset TabHint
  if ShowHint and (FLastTabHintIndex <> -1) then
  begin
    FLastTabHintIndex := -1;
    Hint := FHintOrg;
  end;
  inherited;
end;

function TExtPageControl.GetGlyphIndex(PgIndex: Integer): Integer;
begin
  Result := PgIndex;
end;

function TExtPageControl.GetPageIndexByTabIndex(CurrentTabIndex: Integer):
  Integer;
var
  LokZaehl: Integer;
begin
  // search the correct page within the Pages-Array
  for LokZaehl := CurrentTabIndex to PageCount - 1 do
  begin
    // Pages[LokZaehl].TabIndex is -1 if the Tab of the page
    // is invisible (TabVisible = False)
    if Pages[LokZaehl].TabIndex = CurrentTabIndex then
    begin
      GetPageIndexByTabIndex := LokZaehl;
      Exit;
    end;
  end;
  raise EExtPageInvalidCall.CreateFmt('Function GetPageIndexByTabIndex ' +
    'of component %s of class %s was called with an invalid parameter ' +
    '(CurrentTabIndex = %d).', [Self.Name, Self.Classname, CurrentTabIndex]);
end;

procedure TExtPageControl.GetSheetRect(var SheetRect: TRect);
begin
  SheetRect := ClientRect;
  Perform(TCM_ADJUSTRECT, 0, LPARAM(@SheetRect));
  case FTabPosition of
    tabPosLeft: Inc(SheetRect.Left, 2);
    tabPosRight: Dec(SheetRect.Right, 2);
    tabPosTop: Inc(SheetRect.Top, 2);
    tabPosBottom: Dec(SheetRect.Bottom, 2);
  end;
  InflateRect(SheetRect, 4, 4);
end;

function TExtPageControl.GetTabRect(TabIndex: Integer): TRect;
begin
  if Perform(TCM_GETITEMRECT, TabIndex, LPARAM(@Result)) = 0 then
    Result := Rect(0, 0, 0, 0);
end;

procedure TExtPageControl.HottrackChange(Cold, Hot: Integer);
var
  UpdR: TRect;
begin
  if HandleAllocated then
  begin
    PaintFlickerFree := True;
    UpdR := GetTabRect(Cold);
    InvalidateRect(WindowHandle, @UpdR, False);
    UpdR := GetTabRect(Hot);
    InvalidateRect(WindowHandle, @UpdR, False);
    UpdateWindow(WindowHandle);
  end;
end;

procedure TExtPageControl.GlyphHasChanged(Sender: TObject);
begin
  if HandleAllocated then
    InvalidateRect(WindowHandle, nil, False);
end;

procedure TExtPageControl.GlyphHasChangedPicture(Sender: TObject);
begin
  if HandleAllocated then
  begin
    if (TabGlyphs.NumGlyphs <= 0) or TabGlyphs.FGlyphPaint.Empty then
      Perform(TCM_SETIMAGELIST, 0, 0)
    else
      Perform(TCM_SETIMAGELIST, 0, LPARAM(TabGlyphs.ImageList.Handle));
    InvalidateRect(WindowHandle, nil, False);
  end;
end;

function TExtPageControl.IsNewComCtl: Boolean;
begin
  IsNewComCtl := IsNewComCtl32Version;
end;

procedure TExtPageControl.ReAlignTabSheets(Adamant: Boolean);
var
  TabCounter: Integer;
  TempRect: TRect;
begin
  if HandleAllocated and (Adamant or IsWindowVisible(WindowHandle)) then
  begin
    TempRect := DisplayRect;
    for TabCounter := 0 to PageCount - 1 do
      Pages[TabCounter].BoundsRect := TempRect;
    InvalidateRect(WindowHandle, nil, False);
  end;
end;

procedure TExtPageControl.SelectNextEnabledPage(GoForward: Boolean;
  FullCycle: Boolean);
var
  LokZaehl: Integer;
begin
  if GoForward then
  begin
    // forward
    for LokZaehl := ActivePage.PageIndex + 1 to PageCount - 1 do
      if Pages[LokZaehl].Enabled then
      begin
        ActivePage := Pages[LokZaehl];
        Exit;
      end;
    if FullCycle then
    begin
      for LokZaehl := 0 to ActivePage.PageIndex - 1 do
        if Pages[LokZaehl].Enabled then
        begin
          ActivePage := Pages[LokZaehl];
          Exit;
        end;
    end;

  end
  else
  begin
    // backward
    for LokZaehl := ActivePage.PageIndex - 1 downto 0 do
      if Pages[LokZaehl].Enabled then
      begin
        ActivePage := Pages[LokZaehl];
        Exit;
      end;
    if FullCycle then
    begin
      for LokZaehl := PageCount - 1 downto ActivePage.PageIndex + 1 do
        if Pages[LokZaehl].Enabled then
        begin
          ActivePage := Pages[LokZaehl];
          Break;
        end;
    end;
  end;
end;

function TExtPageControl.TabTextOut(Rect: TRect; Margin: TPoint; Text: string;
  Align: TAlignment; Disabled: Boolean): TPoint;
var
  DrawInRect, TempRect: TRect;
  TextPosY: Integer;
  DrawStyle: UInt;
  OldColor: COLORREF;
begin
  if FTabWordWrap then
    DrawStyle := DT_WORDBREAK
  else
    DrawStyle := DT_SINGLELINE;
  case Align of
    taLeftJustify: DrawStyle := DrawStyle or DT_LEFT;
    taRightJustify: DrawStyle := DrawStyle or DT_RIGHT;
    taCenter: DrawStyle := DrawStyle or DT_CENTER;
  end;
  SetBkMode(FCanvas.Handle, Windows.TRANSPARENT);

  InflateRect(Rect, -2, -2);
  DrawInRect := Rect;
  Inc(DrawInRect.Left, Margin.X);
  Inc(DrawInRect.Top, Margin.Y);

  // calculate position for multiline text
  TempRect := DrawInRect;
  OffsetRect(TempRect, -TempRect.Left, -TempRect.Top);
  DrawText(FCanvas.Handle, PChar(Text), -1, TempRect, DrawStyle or DT_CALCRECT);

  if TempRect.Bottom > (DrawInRect.Bottom - DrawInRect.Top) then
    TempRect.Bottom := DrawInRect.Bottom - DrawInRect.Top;
  TextPosY := ((DrawInRect.Bottom - DrawInRect.Top) -
    (TempRect.Bottom - TempRect.Top)) div 2;
  if TextPosY < 0 then
    TextPosY := 0;
  Inc(DrawInRect.Top, TextPosY);

  if TempRect.Right > (DrawInRect.Right - DrawInRect.Left) then
    TempRect.Right := DrawInRect.Right - DrawInRect.Left;
  if Align = taCenter then
  begin
    DrawInRect.Left := ((Rect.Right - Rect.Left) - TempRect.Right - Margin.X)
      div
      2
      + Rect.Left + Margin.X;
    DrawInRect.Right := DrawInRect.Left + TempRect.Right;
  end;

  if Disabled then
  begin
    // draw text (disabled)
    OldColor := SetTextColor(FCanvas.Handle, ColorToRGB(clBtnHighlight));
    try
      OffsetRect(DrawInRect, 1, 1);
      DrawText(FCanvas.Handle, PChar(Text), -1, DrawInRect, DrawStyle);
      OffsetRect(DrawInRect, -1, -1);
      SetTextColor(FCanvas.Handle, ColorToRGB(clGrayText));
      DrawText(FCanvas.Handle, PChar(Text), -1, DrawInRect, DrawStyle);
    finally
      SetTextColor(FCanvas.Handle, OldColor);
    end;
  end
  else
    // draw text (enabled)
    DrawText(FCanvas.Handle, PChar(Text), -1, DrawInRect, DrawStyle);

  Result.X := DrawInRect.Left - Margin.X;
  Result.Y := DrawInRect.Top - Margin.Y;
end;

{ TExtPageControlTag }

function TExtPageControlTag.GetGlyphIndex(PgIndex: Integer): Integer;
begin
  Result := Pages[PgIndex].Tag;
end;

{ TExtTabControl }

constructor TExtTabControl.Create(AOwner: TComponent);
begin
  inherited;
  FCanvas := TControlCanvas.Create;
  FHottrack := TExtHottrack.Create(Self);
  FInitialTab := -1;
  FirstPaint := True;
  FLastTabHintIndex := -1;
  FMouseInControl := False;
  FTabHints := TStringList.Create;
  FTabHints.Sorted := False;
  FTabDisabledList := TList.Create;
  FTabActiveColor := clBtnFace;
  FTabActiveFont := TFont.Create;
  FTabActiveFont.OnChange := TabFontChanged;
  FTabGlyphs := TExtGlyphs.Create(Self);
  FTabGlyphs.OnChange := GlyphHasChanged;
  FTabGlyphs.OnChangeGlyph := GlyphHasChangedPicture;
  FTabInactiveColor := RGB(160, 160, 164);
  FTabInactiveFont := TFont.Create;
  FTabInactiveFont.OnChange := TabFontChanged;
  FTabHeight := inherited TabHeight;
  FTabWidth := inherited TabWidth;
  FTabPosition := tabPosTop;
  FTabStyle := tabStDefault;
  FTabWordWrap := False;
  PaintFlickerFree := False;
  StopBGPainting := 0;
  StopBGPaintingTabs := False;
  StopFGPainting := 0;
end;

destructor TExtTabControl.Destroy;
begin
  FTabActiveFont.Free;
  FTabGlyphs.Free;
  FTabInactiveFont.Free;
  FHottrack.Free;
  FCanvas.Free;
  FTabDisabledList.Free;
  FTabHints.Free;
  inherited;
end;

procedure TExtTabControl.CreateParams(var Params: TCreateParams);
begin
  inherited;
  Params.Style := Params.Style or TCS_OWNERDRAWFIXED;
  case FTabPosition of
    tabPosLeft: Params.Style := Params.Style or TCS_VERTICAL and (not
        TCS_RIGHT);
    tabPosRight: Params.Style := Params.Style or TCS_VERTICAL or TCS_RIGHT;
    tabPosTop: Params.Style := Params.Style and (not TCS_VERTICAL) and (not
        TCS_BOTTOM);
    tabPosBottom: Params.Style := Params.Style and (not TCS_VERTICAL) or
      TCS_BOTTOM;
  end;
  if FTabStyle = tabStButton then
    Params.Style := Params.Style or TCS_BUTTONS;
end;

procedure TExtTabControl.CreateWnd;
begin
  inherited;
  CheckAndSetHWndFont;
  if not FTabGlyphs.FGlyph.Empty then
    Perform(TCM_SETIMAGELIST, 0, LPARAM(TabGlyphs.ImageList.Handle));
end;

procedure TExtTabControl.TCMInsertItem(var Msg: TMessage);
var
  TabItemStruct: TTCItem;
begin
  inherited;
  TabItemStruct.Mask := TCIF_IMAGE;
  TabItemStruct.iImage := 0;
  Perform(TCM_SETITEM, Msg.WParam, LPARAM(@TabItemStruct));
end;

procedure TExtTabControl.ChangeScale(M, D: Integer);
begin
  inherited;
  TabHeight := MulDiv(FTabHeight, M, D);
  TabWidth := MulDiv(FTabWidth, M, D);
  TabActiveFont.Height := MulDiv(FTabActiveFont.Height, M, D);
  TabInactiveFont.Height := MulDiv(FTabInactiveFont.Height, M, D);
end;

function TExtTabControl.GetTabNumber(CurrentPos: TPoint): Integer;
var
  HitTestInfo: TTCHitTestInfo;
begin
  if HandleAllocated then
  begin
    HitTestInfo.pt := CurrentPos;
    Result := Perform(TCM_HITTEST, 0, LParam(@HitTestInfo));
  end
  else
    Result := -1;
end;

procedure TExtTabControl.Loaded;
begin
  inherited;
  FHintOrg := Hint;
  if (FInitialTab >= 0) and (not (csDesigning in componentState)) then
    inherited TabIndex := FInitialTab;
end;

procedure TExtTabControl.TabFontChanged(Sender: TObject);
begin
  if HandleAllocated then
  begin
    CheckAndSetHWndFont;
    Invalidate;
  end;
end;

procedure TExtTabControl.CheckAndSetHWndFont;
var
  TempDC: HDC;
  OldFont: HFont;
  WidthActive, WidthInactive: Integer;
begin
  // We let Windows TabControl know what's the font of the tabs.
  // With this information it calculates height and width
  // itself, if TabHeight or TabWidth = 0
  TempDC := GetDC(0);
  try
    OldFont := SelectObject(TempDC, FTabActiveFont.Handle);
    try
      WidthActive := 0;
      WidthInActive := 0;
      // function GetCharWidth32 is not implemented in Win95!
      GetCharWidth(TempDC, Ord('W'), Ord('W'), WidthActive);
      SelectObject(TempDC, FTabInActiveFont.Handle);
      GetCharWidth(TempDC, Ord('W'), Ord('W'), WidthInactive);
    finally
      SelectObject(TempDC, OldFont);
    end;
  finally
    ReleaseDC(0, TempDC);
  end;

  if WidthInactive > WidthActive then
    Perform(WM_SETFONT, FTabInActiveFont.Handle, 0)
  else
    Perform(WM_SETFONT, FTabActiveFont.Handle, 0);
end;

function TExtTabControl.GetTabEnabled(Index: Integer): Boolean;
begin
  GetTabEnabled := FTabDisabledList.IndexOf(Pointer(Index)) = -1;
end;

procedure TExtTabControl.SetTabEnabled(Index: Integer; Value: Boolean);
begin
  if Value then
  begin // enable Tab
    if FTabDisabledList.IndexOf(Pointer(Index)) <> -1 then
    begin
      FTabDisabledList.Remove(Pointer(Index));
      if HandleAllocated then
        InvalidateRect(WindowHandle, nil, False);
    end;
  end
  else
  begin // disable Tab
    if FTabDisabledList.IndexOf(Pointer(Index)) = -1 then
    begin
      FTabDisabledList.Add(Pointer(Index));
      if HandleAllocated then
        InvalidateRect(WindowHandle, nil, False);
    end;
  end;
end;

function TExtTabControl.GetTabIndex: Integer;
begin
  GetTabIndex := inherited TabIndex;
end;

procedure TExtTabControl.SetTabIndex(Value: Integer);
begin
  if (Value <> inherited TabIndex) and (Value < Tabs.Count) then
  begin
    if TabEnabled[Value] or (csDesigning in ComponentState) then
    begin
      if csLoading in ComponentState then
        inherited TabIndex := Value
      else
        if CanChange then
        begin
          inherited TabIndex := Value;
          Change;
        end;
    end;
  end;
end;

procedure TExtTabControl.SetHottrack(Value: TExtHottrack);
begin
  // no action
end;

procedure TExtTabControl.SetTabActiveColor(Value: TColor);
begin
  if FTabActiveColor <> Value then
  begin
    FTabActiveColor := Value;
    if HandleAllocated then
      InvalidateRect(WindowHandle, nil, False);
  end;
end;

procedure TExtTabControl.SetTabHints(Value: TStringList);
begin
  FTabHints.Assign(Value);
end;

procedure TExtTabControl.SetTabInactiveColor(Value: TColor);
begin
  if FTabInactiveColor <> Value then
  begin
    FTabInactiveColor := Value;
    if HandleAllocated then
      InvalidateRect(WindowHandle, nil, False);
  end;
end;

procedure TExtTabControl.SetTabActiveFont(Value: TFont);
begin
  FTabActiveFont.Assign(Value);
end;

procedure TExtTabControl.SetTabGlyphs(Value: TExtGlyphs);
begin
  // no action
end;

procedure TExtTabControl.SetTabHeight(Value: Word);
begin
  if FTabHeight <> Value then
  begin
    FTabHeight := Value;
    case FTabPosition of
      tabPosLeft, tabPosRight: inherited TabWidth := Value;
    else
      inherited TabHeight := Value;
    end;
  end;
end;

procedure TExtTabControl.SetTabInactiveFont(Value: TFont);
begin
  FTabInactiveFont.Assign(Value);
end;

procedure TExtTabControl.SetTabPosition(Value: TExtTabPosition);

  procedure UpdateInherTabSizeNormal;
  begin
    inherited TabWidth := FTabWidth;
    inherited TabHeight := FTabHeight;
  end;

  procedure UpdateInherTabSizeOpposite;
  begin
    if (Value in [tabPosLeft, tabPosRight]) then
    begin
      if FTabHeight = 0 then
        FTabHeight := 35;
      if FTabWidth = 0 then
        FTabWidth := 70;
    end;
    inherited TabWidth := FTabHeight;
    inherited TabHeight := FTabWidth;
  end;

begin
  if FTabPosition <> Value then
  begin
    BeginUpdate;
    try
      // update inherited tab-height and tab-with if necessary
      case FTabPosition of
        tabPosLeft:
          if Value <> tabPosRight then
            UpdateInherTabSizeNormal;
        tabPosRight:
          if Value <> tabPosLeft then
            UpdateInherTabSizeNormal;
        tabPosTop:
          if Value <> tabPosBottom then
            UpdateInherTabSizeOpposite;
        tabPosBottom:
          if Value <> tabPosTop then
            UpdateInherTabSizeOpposite;
      end;
      FTabPosition := Value;

      if (Value <> tabPosTop) and (FTabStyle = tabStButton) then
        TabStyle := tabStDefault;
      if HandleAllocated then
        ReCreateWnd;
    finally
      EndUpdate;
    end;
  end;
end;

procedure TExtTabControl.SetTabStyle(Value: TExtTabStyle);
var
  OldWStyle: Longint;
begin
  if FTabStyle <> Value then
  begin
    FTabStyle := Value;
    if FTabStyle = tabStButton then
      TabPosition := tabPosTop;
    if HandleAllocated then
    begin
      OldWStyle := GetWindowLong(WindowHandle, GWL_STYLE);
      if Value = tabStButton then
        SetWindowLong(WindowHandle, GWL_STYLE, OldWStyle or TCS_BUTTONS)
      else
        SetWindowLong(WindowHandle, GWL_STYLE, OldWStyle and (not TCS_BUTTONS));
    end;
  end;
end;

procedure TExtTabControl.SetTabWidth(Value: Word);
begin
  if FTabWidth <> Value then
  begin
    FTabWidth := Value;
    case FTabPosition of
      tabPosLeft, tabPosRight: inherited TabHeight := Value;
    else
      inherited TabWidth := Value;
    end;
  end;
end;

procedure TExtTabControl.SetTabWordWrap(Value: Boolean);
begin
  if FTabWordWrap <> Value then
  begin
    FTabWordWrap := Value;
    if HandleAllocated then
      InvalidateRect(WindowHandle, nil, False);
  end;
end;

procedure TExtTabControl.CMDialogChar(var Msg: TCMDialogChar);
var
  TabCounter: Integer;
begin
  if CanFocus and ((Msg.KeyData and $20000000) <> 0) then
  begin // ALT-key pressed
    for TabCounter := 0 to Tabs.Count - 1 do
      if IsAccel(Msg.CharCode, Tabs[TabCounter]) then
      begin
        TabIndex := TabCounter;
        Msg.Result := 1;
        Exit;
      end;
  end;
  inherited;
end;

procedure TExtTabControl.CMFontChanged(var Msg: TMessage);
begin
  Invalidate;
  NotifyControls(CM_PARENTFONTCHANGED);
end;

procedure TExtTabControl.CMHintShow(var Msg: TMessage);
var
  InfHint: ^THintInfo;
  TabIndex: Integer;
  CurrCurPos: TPoint;
  NewHintPos: TPoint;
  CurrTabRect: TRect;
begin
  if GetCursorPos(CurrCurPos) then
  begin
    Windows.ScreenToClient(Handle, CurrCurPos);
    TabIndex := GetTabNumber(CurrCurPos);
    if TabIndex >= 0 then
    begin
      // Hint should appear at Tab
      if Perform(TCM_GETITEMRECT, TabIndex, LParam(@CurrTabRect)) <> 0 then
      begin
        InfHint := Pointer(Msg.LParam);
        NewHintPos := Point(CurrTabRect.Left + 3, CurrTabRect.Bottom + 1);
        Windows.ClientToScreen(Handle, NewHintPos);
        InfHint^.HintPos := NewHintPos;
      end;
    end;
  end;
end;

procedure TExtTabControl.CMParentColorChanged(var Msg: TMessage);
begin
  inherited;
  Invalidate;
end;

procedure TExtTabControl.WMPaint(var Msg: TWMPaint);
var
  IsOurDC: Boolean;
  OldMsgDC, PaintDC: HDC;
  PaintStr: TPaintStruct;
  OldBitmap, MemBitmap: HBitmap;
  MemDC: HDC;
  PaintInRect: TRect;
  Form: {$IFDEF IS_DELPHI3_OR_HIGHER}TCustomForm{$ELSE}TForm{$ENDIF};
begin
  if StopFGPainting > 0 then
  begin
    if HandleAllocated then
      ValidateRect(WindowHandle, nil);
    Msg.Result := 1;
    Exit;
  end;

  OldMsgDC := Msg.DC;
  if Msg.DC = 0 then
  begin
    Inc(StopBGPainting);
    PaintDC := BeginPaint(Handle, PaintStr);
    PaintInRect := PaintStr.rcPaint;
    Dec(StopBGPainting);
    IsOurDC := True;
  end
  else
  begin
    PaintDC := Msg.DC;
    PaintInRect := ClientRect;
    IsOurDC := False;
  end;

  try
    Form := TForm(GetParentForm(Self));
    if (not PaintFlickerFree) and (FirstPaint or
      ((Form <> nil) and (not Form.Active))) then
    begin
      FirstPaint := False;
      // faster direct painting if first time or inactive
      Msg.DC := PaintDC;
      Perform(WM_ERASEBKGND, Msg.DC, 0);
      inherited;
    end
    else
    begin
      // slower indirect (flickerfree) painting otherwise
      MemBitmap := CreateCompatibleBitmap(PaintDC, ClientWidth, ClientHeight);
      try
        MemDC := CreateCompatibleDC(PaintDC);
        try
          OldBitmap := SelectObject(MemDC, MemBitmap);
          StopBGPaintingTabs := True;
          try
            Perform(WM_ERASEBKGND, MemDC, 0);
          finally
            StopBGPaintingTabs := False;
          end;

          try
            Msg.DC := MemDC;
            // paint into Offscreen-DC
            inherited;
            BitBlt(PaintDC, PaintInRect.Left, PaintInRect.Top,
              PaintInRect.Right - PaintInRect.Left,
              PaintInRect.Bottom - PaintInRect.Top,
              MemDC, PaintInRect.Left, PaintInRect.Top, SRCCOPY);
          finally
            SelectObject(MemDC, OldBitmap);
          end;
        finally
          DeleteDC(MemDC);
        end;
      finally
        DeleteObject(MemBitmap);
      end;
    end;

  finally
    if IsOurDC then
      EndPaint(Handle, PaintStr);
    Msg.DC := OldMsgDC;
    PaintFlickerFree := False;
  end;
end;

procedure TExtTabControl.CNDrawTab(var Msg: TWMDrawItem);
var
  FgRect, BgRect: TRect;
  State: TOwnerDrawState;
  CurrPageIdx: Integer;
begin
  with Msg.DrawItemStruct^ do
  begin
    if Integer(itemID) < 0 then
      Exit;
    CurrPageIdx := itemID;
    State := TOwnerDrawState(
{$IFDEF IS_DELPHI4_OR_LOWER}
      WordRec(LongRec(itemState).Lo).Lo
{$ELSE}
      LongRec(itemState).Lo
{$ENDIF}
      );
    if not TabEnabled[CurrPageIdx] then
      Include(State, odDisabled);

    // retrieve Foreground Rect
    Perform(TCM_GETITEMRECT, itemID, LParam(@FgRect));
    if (odSelected in State) and (FTabStyle = tabStDefault) then
    begin
      // active Tab
      BgRect := rcItem;
      case FTabPosition of
        tabPosLeft: OffsetRect(FgRect, -2, 0);
        tabPosRight: OffsetRect(FgRect, 2, 0);
        tabPosTop: OffsetRect(FgRect, 0, -1);
      end;
    end
    else
    begin
      // inactive Tab
      BgRect := FgRect;
      if FTabStyle = tabStDefault then
        case FTabPosition of
          tabPosTop: OffsetRect(FgRect, 0, 1);
          tabPosBottom: OffsetRect(FgRect, 0, -2);
        end;
    end;

    FCanvas.Handle := hDC;
    FCanvas.Brush := Brush;
    try
      if odSelected in State then
      begin
        // paint the active tab
        FCanvas.Brush.Color := FTabActiveColor;
        FCanvas.Font := FTabActiveFont;
        if FHottrack.Enabled and (FHottrack.FTabActiveColor <> clNone) and
          (FHottrack.TabTracked = Integer(itemID)) then
          FCanvas.Font.Color := FHottrack.FTabActiveColor;
        DrawTab(CurrPageIdx, True, FgRect, BgRect, State);
      end
      else
      begin
        // paint an inactive tab
        FCanvas.Brush.Color := FTabInactiveColor;
        FCanvas.Font := FTabInactiveFont;
        if FHottrack.Enabled and (FHottrack.FTabInactiveColor <> clNone) and
          (FHottrack.TabTracked = Integer(itemID)) then
          FCanvas.Font.Color := FHottrack.FTabInactiveColor;
        DrawTab(CurrPageIdx, False, FgRect, BgRect, State);
      end;
    finally
      FCanvas.Handle := 0;
    end;
  end;
end;

procedure TExtTabControl.DefaultDrawTab(Index: Integer; const RectFg: TRect;
  State: TOwnerDrawState);
const
  DistanceMargin = 8;
var
  ImagePosLeft: Integer;
  ImagePosTop: Integer;
begin
  if FTabGlyphs.FGlyph.Empty then
  begin
    // draw only text
    case FTabPosition of
      tabPosLeft, tabPosRight:
        TabTextOut(RectFg, Point(DistanceMargin, 0), Tabs[Index],
          taLeftJustify, odDisabled in State);
      tabPosTop, tabPosBottom:
        TabTextOut(RectFg, Point(0, 0), Tabs[Index],
          taCenter, odDisabled in State);
    end;
  end
  else
  begin
    // draw bitmap if Tab enabled
    if odDisabled in State then
      TabTextOut(RectFg, Point(0, 0), Tabs[Index], taCenter, True)
    else
      case FTabGlyphs.FPosition of
        glPosLeft:
          begin // Tab image is left beside Text
            case FTabPosition of
            // Tabs on Left or Right -> Alignment Left
              tabPosLeft, tabPosRight:
                begin
                  ImagePosLeft := RectFg.Left + DistanceMargin;
                  TabTextOut(RectFg, Point(DistanceMargin +
                    TabGlyphs.ImageList.Width +
                    FTabGlyphs.FSpacing, 0), Tabs[Index], taLeftJustify, False);
                end;
            else
            // Tabs on Top or Bottom -> Alignment Center
              ImagePosLeft := TabTextOut(RectFg, Point(TabGlyphs.ImageList.Width
                +
                FTabGlyphs.FSpacing, 0), Tabs[Index], taCenter, False).X;
            end;
            TabGlyphs.ImageList.Draw(FCanvas, ImagePosLeft, RectFg.Top +
              (RectFg.Bottom -
              RectFg.Top - TabGlyphs.ImageList.Height) div 2,
              GetGlyphIndex(Index));
          end;
        glPosTop:
          begin // Tab image is above Text
            ImagePosTop := TabTextOut(RectFg, Point(0, TabGlyphs.ImageList.Height
              +
              FTabGlyphs.FSpacing), Tabs[Index], taCenter, False).Y;
            TabGlyphs.ImageList.Draw(FCanvas, RectFg.Left + (RectFg.Right -
              RectFg.Left -
              TabGlyphs.ImageList.Width) div 2, ImagePosTop,
              GetGlyphIndex(Index));
          end;
      end;
  end;
end;

procedure TExtTabControl.DrawTab(Index: Integer; ActiveTab: Boolean;
  const RectFg, RectBg: TRect; State: TOwnerDrawState);
begin
  FillRect(FCanvas.Handle, RectBg, FCanvas.Brush.Handle);
  if Assigned(FOnDrawTab) then
    FOnDrawTab(Self, Index, ActiveTab, RectFg, RectBg, State)
  else
    DefaultDrawTab(Index, RectFg, State);
end;

procedure TExtTabControl.BeginUpdate;
begin
  // BackGround painting is stopped, if StopBGPainting > 0
  Inc(StopBGPainting);
  // ForeGround painting is stopped, if StopFGPainting > 0
  Inc(StopFGPainting);
end;

procedure TExtTabControl.EndUpdate;
begin
  // BackGround painting goes on if StopBGPainting is 0
  if StopBGPainting > 0 then
    Dec(StopBGPainting);
  // ForeGround painting goes on if StopFGPainting is 0
  if StopFGPainting > 0 then
    Dec(StopFGPainting);
  if (StopBGPainting = 0) and (StopFGPainting = 0) then
    Invalidate;
end;

procedure TExtTabControl.FillTabBG(DC: HDC; ActiveTab: Boolean; const TabRect:
  TRect);
// paint the background of one tab
var
  TabsBrush, OldBrush: HBrush;
begin
  if ActiveTab then
    TabsBrush := CreateSolidBrush(ColorToRGB(FTabActiveColor))
  else
    TabsBrush := CreateSolidBrush(ColorToRGB(FTabInactiveColor));
  try
    OldBrush := SelectObject(DC, TabsBrush);
    try
      FillRect(DC, TabRect, TabsBrush);
    finally
      SelectObject(DC, OldBrush);
    end;
  finally
    DeleteObject(TabsBrush);
  end;
end;

procedure TExtTabControl.WMEraseBkgnd(var Msg: TWMEraseBkgnd);
var
  ActiveTabIdx: Integer;
  SingleTabRect: TRect;
  SheetAreaRect: TRect;
  RgnTemp, RgnBkgnd: HRgn;
  TabCounter: Integer;
  BtnRect: TRect;
  IntersSect: TRect;
begin
  if StopBGPainting > 0 then
  begin
    Msg.Result := 1;
    Exit;
  end;
  SetBrushOrgEx(Msg.DC, Left mod 8, Top mod 8, nil);
//{$IFDEF IS_DELPHI3_OR_HIGHER}nil{$ELSE}PPoint(nil)^{$ENDIF});

  // retrieve Sheet-Area
  GetSheetRect(SheetAreaRect);
  FillRect(Msg.DC, SheetAreaRect, Brush.Handle);
  if Assigned(FOnDrawBkgndInside) then
    FOnDrawBkgndInside(Self, Msg.DC, TabIndex, SheetAreaRect);

  // take out Sheet-Area from Background Area
  RgnBkgnd := CreateRectRgnIndirect(ClientRect);
  try
    RgnTemp := CreateRectRgnIndirect(SheetAreaRect);
    try
      CombineRgn(RgnBkgnd, RgnBkgnd, RgnTemp, RGN_DIFF);
    finally
      DeleteObject(RgnTemp);
    end;

    // retrieve and fill Tabs if necessary
    if (Tabs.Count > 0) and (not StopBGPaintingTabs) then
    begin
      ActiveTabIdx := Perform(TCM_GETCURSEL, 0, 0);
      GetTabControl_BtnRect(Handle, BtnRect);
      for TabCounter := 0 to Tabs.Count - 1 do
      begin
        Perform(TCM_GETITEMRECT, TabCounter, LPARAM(@SingleTabRect));
        FillTabBG(Msg.DC, ActiveTabIdx = TabCounter, SingleTabRect);
        if not InterSectRect(IntersSect, SingleTabRect, BtnRect) then
        begin
          // take out this Tab from Background Area
          RgnTemp := CreateRectRgnIndirect(SingleTabRect);
          try
            CombineRgn(RgnBkgnd, RgnBkgnd, RgnTemp, RGN_DIFF);
          finally
            DeleteObject(RgnTemp);
          end;
        end;
      end;
    end;
    FillRgn(Msg.DC, RgnBkgnd, Parent.Brush.Handle);
    if Assigned(FOnDrawBkgndOutside) then
    begin
      SetBkColor(Msg.DC, ColorToRGB(Parent.Brush.Color));
      FOnDrawBkgndOutside(Self, Msg.DC, RgnBkgnd);
    end;
  finally
    DeleteObject(RgnBkgnd);
  end;
  Msg.Result := 1;
end;

procedure TExtTabControl.WMLButtonDown(var Msg: TWMLButtonDown);
var
  HitIndex: Integer;
begin
  if csDesigning in ComponentState then
    inherited
  else
  begin
    // filter mouse clicks at disabled pages
    HitIndex := GetTabNumber(SmallPointToPoint(Msg.Pos));
    if HitIndex >= 0 then
    begin
      if TabEnabled[HitIndex] then
        inherited;
    end
    else
      inherited;
  end;
end;

procedure TExtTabControl.WMNCHitTest(var Msg: TWMNCHitTest);
var
  TabIndexNew: Integer;
  OldHottrackTab: Integer;
  NowAtPos: TPoint;
begin
  inherited;
  if (not FMouseInControl) or (Msg.Result <> HTCLIENT) or (GetCapture <> 0) then
    Exit;
  NowAtPos := SmallPointToPoint(Msg.Pos);
  Windows.ScreenToClient(Handle, NowAtPos);
  TabIndexNew := GetTabNumber(NowAtPos);

  // check if another Hottrack color necessary
  if FHottrack.Enabled and (TabIndexNew <> FHottrack.TabTracked) then
  begin
    OldHottrackTab := FHottrack.TabTracked;
    FHottrack.TabTracked := TabIndexNew;
    HottrackChange(OldHottrackTab, FHottrack.TabTracked);
  end;

  // check if another Hint string necessary
  if ShowHint and (not (csDesigning in ComponentState)) and
    (TabIndexNew <> FLastTabHintIndex) then
  begin
    FLastTabHintIndex := TabIndexNew;
    if Application <> nil then
    begin
      Application.CancelHint;
      if (TabIndexNew >= 0) and (FTabHints.Count >= TabIndexNew + 1) then
        Hint := FTabHints[TabIndexNew]
      else
        Hint := '';
    end;
  end;
end;

procedure TExtTabControl.CMMouseEnter(var Msg: TMessage);
begin
  FMouseInControl := True;
  inherited;
end;

procedure TExtTabControl.CMMouseLeave(var Msg: TMessage);
var
  OldHottrackTab: Integer;
begin
  FMouseInControl := False;
  // reset Hottrack
  if FHottrack.Enabled and (FHottrack.TabTracked <> -1) then
  begin
    OldHottrackTab := FHottrack.TabTracked;
    FHottrack.TabTracked := -1;
    HottrackChange(OldHottrackTab, -1);
  end;
  // reset TabHint
  if ShowHint and (FLastTabHintIndex <> -1) then
  begin
    FLastTabHintIndex := -1;
    Hint := FHintOrg;
  end;
  inherited;
end;

function TExtTabControl.GetGlyphIndex(TabIndex: Integer): Integer;
begin
  Result := TabIndex;
end;

procedure TExtTabControl.GetSheetRect(var SheetRect: TRect);
begin
  SheetRect := ClientRect;
  Perform(TCM_ADJUSTRECT, 0, LPARAM(@SheetRect));
  case FTabPosition of
    tabPosLeft: Inc(SheetRect.Left, 2);
    tabPosRight: Dec(SheetRect.Right, 2);
    tabPosTop: Inc(SheetRect.Top, 2);
    tabPosBottom: Dec(SheetRect.Bottom, 2);
  end;
  InflateRect(SheetRect, 4, 4);
end;

function TExtTabControl.GetTabRect(TabIndex: Integer): TRect;
begin
  if Perform(TCM_GETITEMRECT, TabIndex, LPARAM(@Result)) = 0 then
    Result := Rect(0, 0, 0, 0);
end;

procedure TExtTabControl.HottrackChange(Cold, Hot: Integer);
var
  UpdR: TRect;
begin
  if HandleAllocated then
  begin
    PaintFlickerFree := True;
    UpdR := GetTabRect(Cold);
    InvalidateRect(WindowHandle, @UpdR, False);
    UpdR := GetTabRect(Hot);
    InvalidateRect(WindowHandle, @UpdR, False);
    UpdateWindow(WindowHandle);
  end;
end;

procedure TExtTabControl.GlyphHasChanged(Sender: TObject);
begin
  if HandleAllocated then
    InvalidateRect(WindowHandle, nil, False);
end;

procedure TExtTabControl.GlyphHasChangedPicture(Sender: TObject);
begin
  if HandleAllocated then
  begin
    if (TabGlyphs.NumGlyphs <= 0) or TabGlyphs.FGlyphPaint.Empty then
      Perform(TCM_SETIMAGELIST, 0, 0)
    else
      Perform(TCM_SETIMAGELIST, 0, LPARAM(TabGlyphs.ImageList.Handle));
    InvalidateRect(WindowHandle, nil, False);
  end;
end;

function TExtTabControl.IsNewComCtl: Boolean;
begin
  IsNewComCtl := IsNewComCtl32Version;
end;

function TExtTabControl.TabTextOut(Rect: TRect; Margin: TPoint; Text: string;
  Align: TAlignment; Disabled: Boolean): TPoint;
var
  DrawInRect, TempRect: TRect;
  TextPosY: Integer;
  DrawStyle: UInt;
  OldColor: COLORREF;
begin
  if FTabWordWrap then
    DrawStyle := DT_WORDBREAK
  else
    DrawStyle := DT_SINGLELINE;
  case Align of
    taLeftJustify: DrawStyle := DrawStyle or DT_LEFT;
    taRightJustify: DrawStyle := DrawStyle or DT_RIGHT;
    taCenter: DrawStyle := DrawStyle or DT_CENTER;
  end;
  SetBkMode(FCanvas.Handle, Windows.TRANSPARENT);

  InflateRect(Rect, -2, -2);
  DrawInRect := Rect;
  Inc(DrawInRect.Left, Margin.X);
  Inc(DrawInRect.Top, Margin.Y);

  // calculate position for multiline text
  TempRect := DrawInRect;
  OffsetRect(TempRect, -TempRect.Left, -TempRect.Top);
  DrawText(FCanvas.Handle, PChar(Text), -1, TempRect, DrawStyle or DT_CALCRECT);

  if TempRect.Bottom > (DrawInRect.Bottom - DrawInRect.Top) then
    TempRect.Bottom := DrawInRect.Bottom - DrawInRect.Top;
  TextPosY := ((DrawInRect.Bottom - DrawInRect.Top) -
    (TempRect.Bottom - TempRect.Top)) div 2;
  if TextPosY < 0 then
    TextPosY := 0;
  Inc(DrawInRect.Top, TextPosY);

  if TempRect.Right > (DrawInRect.Right - DrawInRect.Left) then
    TempRect.Right := DrawInRect.Right - DrawInRect.Left;
  if Align = taCenter then
  begin
    DrawInRect.Left := ((Rect.Right - Rect.Left) - TempRect.Right - Margin.X)
      div 2 + Rect.Left + Margin.X;
    DrawInRect.Right := DrawInRect.Left + TempRect.Right;
  end;

  if Disabled then
  begin
    // draw text (disabled)
    OldColor := SetTextColor(FCanvas.Handle, ColorToRGB(clBtnHighlight));
    try
      OffsetRect(DrawInRect, 1, 1);
      DrawText(FCanvas.Handle, PChar(Text), -1, DrawInRect, DrawStyle);
      OffsetRect(DrawInRect, -1, -1);
      SetTextColor(FCanvas.Handle, ColorToRGB(clGrayText));
      DrawText(FCanvas.Handle, PChar(Text), -1, DrawInRect, DrawStyle);
    finally
      SetTextColor(FCanvas.Handle, OldColor);
    end;
  end
  else
    // draw text (enabled)
    DrawText(FCanvas.Handle, PChar(Text), -1, DrawInRect, DrawStyle);

  Result.X := DrawInRect.Left - Margin.X;
  Result.Y := DrawInRect.Top - Margin.Y;
end;

procedure Register;
begin
  RegisterComponents({$IFDEF IS_DELPHI3_OR_HIGHER} 'CommonLib'{$ELSE}
    'Win95'{$ENDIF}, [TExtPageControl, TExtPageControlTag, TExtTabControl]);
end;

function GetIsNewComCtl32Version: Boolean;
var
  VSize, VHandle: DWORD;
  Buffer: PChar;
  VersResult: PVSFixedFileInfo;
  VersResultLength: UINT;
  FileComCtl32: array[0..MAX_PATH + 50] of Char;
begin
  Result := False;
  GetSystemDirectory(FileComCtl32, SizeOf(FileComCtl32));
  StrCat(FileComCtl32, '\ComCtl32.DLL');
  VSize := GetFileVersionInfoSize(FileComCtl32, VHandle);
  GetMem(Buffer, VSize + 1);
  try
    if GetFileVersionInfo(FileComCtl32, VHandle, VSize, Buffer) then
    begin
      VerQueryValue(Buffer, '\', Pointer(VersResult), VersResultLength);
      Result := VersResult^.dwFileVersionMS > $40030;
{$IFDEF EXTPAGE_WARNING}
      if not Result then
        Application.MessageBox(PChar(Format('Windows library Common Controls (file %s) ' +
          'is too old. To choose left, right or bottom tab positions in ' +
          'ExtPage/TabControl you need a newer version.'#10#10'You can download an ' +
          'updated version (v4.72) for Windows 95 and Windows NT 4 from ' +
          'http://ourworld.compuserve.com/homepages/praxisservice/40comupd.exe',
          [FileComCtl32])), 'Update of ComCtl32.DLL recommended',
          MB_OK or MB_ICONINFORMATION);
{$ENDIF}
    end;
  finally
    FreeMem(Buffer);
  end;
end;

{$IFNDEF EXTPAGE_FULL}{$INCLUDE EXTPAG2.INC}{$ENDIF}

initialization
{$IFNDEF EXTPAGE_FULL}ExtendedPageControl;
{$ENDIF}
  IsNewComCtl32Version := GetIsNewComCtl32Version;
end.

