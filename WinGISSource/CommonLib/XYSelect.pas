Unit XYSelect;

Interface

Uses WinProcs,WinTypes,Messages,Classes,Controls,Graphics;

Type TXYSelectorType    = (stXY,stX,stY);

     TXYSelector   = Class(TGraphicControl)
      Private
       FOnChange        : TNotifyEvent;
       FOnPaint         : TNotifyEvent;
       FSelectorType    : TXYSelectorType;
       FSelectorSize    : Integer;
       FXMax            : Double;
       FXMin            : Double;
       FXPosition       : Double;
       FYMax            : Double;
       FYMin            : Double;
       FYPosition       : Double;
       FSelectorSave    : TBitmap;
       FNotFirstPaint   : Boolean;
       Procedure   MouseDown(Button:TMouseButton;Shift:TShiftState;X,Y:Integer); override;
       Procedure   MouseMove(Shift:TShiftState;X,Y:Integer); override;
       Procedure   MouseUp(Button:TMouseButton;Shift:TShiftState;X,Y:Integer); override;
       Procedure   PaintSelector;
       Procedure   PositionToWindow(Const X,Y:Double;var WX,WY:Integer);
       Procedure   RestoreSelector;
       Procedure   SaveSelector;
       Procedure   SetPosition(Const XPos,YPos:Double);
       Procedure   SetSelectorType(AType:TXYSelectorType);
       Procedure   SetSelectorSize(ASize:Integer);
       Procedure   SetXMax(AValue:Double);
       Procedure   SetXMin(AValue:Double);
       Procedure   SetXPosition(APosition:Double);
       Procedure   SetYMax(AValue:Double);
       Procedure   SetYMin(AValue:Double);
       Procedure   SetYPosition(APosition:Double);
       Function    SelectorRect:TRect;
       Procedure   WindowToPosition(WX,WY:Integer;var X,Y:Double);
      Protected
       Function    GetClientRect:TRect; override;
       Procedure   Paint; override;
      Public
       Constructor Create(AOwner:TComponent); override;
       Destructor  Destroy; override;
       Property    Canvas;
       Procedure   SetBounds(ALeft,ATop,AWidth,AHeight:Integer); override;
      Published
       Property    Height default 32;
       Property    OnClick;
       Property    OnDblClick;
       Property    OnChange:TNotifyEvent read FOnChange write FOnChange;
       Property    OnPaint:TNotifyEvent read FOnPaint write FOnPaint;
       Property    SelectorType:TXYSelectorType read FSelectorType write SetSelectorType default stXY;
       Property    SelectorSize:Integer read FSelectorSize write SetSelectorSize default 4;
       Property    Visible;
       Property    XMax:Double read FXMax write SetXMax;
       Property    XMin:Double read FXMin write SetXMin;
       Property    XPosition:Double read FXPosition write SetXPosition;
       Property    YMax:Double read FYMax write SetYMax;
       Property    YMin:Double read FYMin write SetYMin;
       Property    YPosition:Double read FYPosition write SetYPosition;
       Property    Width default 32;
     end;

Implementation

Uses Forms,NumTools;

Constructor TXYSelector.Create
   (
   AOwner          : TComponent
   );
  begin
    inherited Create(AOwner);
    ControlStyle:=ControlStyle+[csOpaque];
    FSelectorSave:=TBitmap.Create;
    FSelectorSize:=4;
    Width:=32;
    Height:=32;
  end;

Destructor TXYSelector.Destroy;
  begin
    FSelectorSave.Free;
    inherited Destroy;
  end;

Procedure TXYSelector.Paint;
  begin
    if FNotFirstPaint then RestoreSelector;
    if Assigned(FOnPaint) then OnPaint(Self);
    SaveSelector;
    PaintSelector;
    FNotFirstPaint:=TRUE;
  end;

Procedure TXYSelector.SetSelectorType
   (
   AType           : TXYSelectorType
   );
  begin
    if AType<>FSelectorType then begin
      FSelectorType:=AType;
      with SelectorRect do begin
        FSelectorSave.Width:=Right-Left;
        FSelectorSave.Height:=Bottom-Top;
      end;
      Invalidate;
    end;
  end;

Procedure TXYSelector.SetXMax
   (
   AValue          : Double
   );
  begin
    if AValue<>FXMax then begin
      FXMax:=AValue;
      Invalidate;
    end;
  end;

Procedure TXYSelector.SetXMin
   (
   AValue          : Double
   );
  begin
    if AValue<>FXMin then begin
      FXMin:=AValue;
      Invalidate;
    end;
  end;

Procedure TXYSelector.SetYMax
   (
   AValue          : Double
   );
  begin
    if AValue<>FYMax then begin
      FYMax:=AValue;
      Invalidate;
    end;
  end;

Procedure TXYSelector.SetYMin
   (
   AValue          : Double
   );
  begin
    if AValue<>FYMin then begin
      FYMin:=AValue;
      Invalidate;
    end;
  end;

Procedure TXYSelector.SetXPosition
   (
   APosition          : Double
   );
  begin
    SetPosition(APosition,FYPosition);
  end;

Procedure TXYSelector.SetYPosition
   (
   APosition          : Double
   );
  begin
    SetPosition(FXPosition,APosition);
  end;

Function TXYSelector.SelectorRect
   : TRect;
  var X            : Integer;
      Y            : Integer;
  begin
    PositionToWindow(FXPosition,FYPosition,X,Y);
    case FSelectorType of
      stXY : Result:=Rect(X-FSelectorSize,Y-FSelectorSize,X+FSelectorSize,Y+FSelectorSize);
      stX  : Result:=Rect(X-FSelectorSize,0,Y+FSelectorSize,Height);
      stY  : Result:=Rect(0,Y-FSelectorSize,Width,Y+FSelectorSize);
    end;
  end;

Procedure TXYSelector.PaintSelector;
  var ARect        : TRect;
  begin
    with Canvas do begin
      Brush.Style:=bsClear;
      Pen.Style:=psSolid;
      Pen.Color:=clBlack;
      ARect:=SelectorRect;
      with ARect do Rectangle(Left,Top,Right,Bottom);
    end;
  end;

Procedure TXYSelector.MouseMove
   (
   Shift           : TShiftState;
   X               : Integer;
   Y               : Integer
   );
  var XPos         : Double;
      YPos         : Double;
  begin
    if ssLeft in Shift then begin
      WindowToPosition(X,Y,XPos,YPos);
      SetPosition(XPos,YPos);
    end;  
  end;

Procedure TXYSelector.SetPosition
   (
   Const XPos      : Double;
   Const YPos      : Double
   );
  var PosChanged   : Boolean;
  begin
    case FSelectorType of
      stXY : PosChanged:=(DblCompare(XPos,FXPosition)<>0)
                   or (DblCompare(YPos,FYPosition)<>0);
      stX  : PosChanged:=DblCompare(XPos,FXPosition)<>0;
      stY  : PosChanged:=DblCompare(YPos,FYPosition)<>0;
      else PosChanged:=FALSE;
    end;
    if PosChanged then begin
      RestoreSelector;
      FXPosition:=XPos;
      FYPosition:=YPos;
      SaveSelector;
      PaintSelector;
      if Assigned(FOnChange) then OnChange(Self);
    end;
  end;

Procedure TXYSelector.PositionToWindow
   (
   Const X         : Double;
   Const Y         : Double;
   var WX          : Integer;
   var WY          : Integer
   );
  begin
    if DblCompare(FXMax-FXMin,0)=0 then WX:=0
    else WX:=Max(FSelectorSize,Min(Width-FSelectorSize,
       Round((Width-2*FSelectorSize)*(X-FXMin)/(FXMax-FXMin))+FSelectorSize));
    if DblCompare(FYMax-FYMin,0)=0 then WY:=0
    else WY:=Max(FSelectorSize,Min(Height-FSelectorSize,
       Round((Height-2*FSelectorSize)*(Y-FYMin)/(FYMax-FYMin))+FSelectorSize));
  end;

Procedure TXYSelector.WindowToPosition
   (
   WX              : Integer;
   WY              : Integer;
   var X           : Double;
   var Y           : Double
   );
  begin
    X:=Max(FXMin,Min(FXMax,(WX-FSelectorSize)*(FXMax-FXMin)/(Width-2*FSelectorSize)));
    Y:=Max(FYMin,Min(FYMax,(WY-FSelectorSize)*(FYMax-FYMin)/(Height-2*FSelectorSize)));
  end;

Procedure TXYSelector.MouseDown
   (
   Button          : TMouseButton;
   Shift           : TShiftState;
   X               : Integer;
   Y               : Integer
   );
  begin
    if Button=mbLeft then begin
      Cursor:=crCross;
      MouseMove(Shift,X,Y);
    end;
  end;

Procedure TXYSelector.MouseUp
   (
   Button          : TMouseButton;
   Shift           : TShiftState;
   X               : Integer;
   Y               : Integer
   );
  begin
    if Button=mbLeft then Cursor:=crDefault;
  end;

Procedure TXYSelector.SetSelectorSize
   (
   ASize           : Integer
   );
  begin
    if ASize<>FSelectorSize then begin
      RestoreSelector;
      FSelectorSize:=ASize;
      with SelectorRect do begin
        FSelectorSave.Width:=Right-Left;
        FSelectorSave.Height:=Bottom-Top;
      end;  
      SaveSelector;
      PaintSelector;
    end;
  end;

Procedure TXYSelector.SaveSelector;
  var DestRect     : TRect;
  begin
    DestRect:=SelectorRect;
    OffsetRect(DestRect,-DestRect.Left,-DestRect.Top);
    FSelectorSave.Canvas.CopyRect(DestRect,Canvas,SelectorRect);
  end;

Procedure TXYSelector.RestoreSelector;
  begin
    Canvas.Draw(SelectorRect.Left,SelectorRect.Top,FSelectorSave);
  end;

Function TXYSelector.GetClientRect
   : TRect;
  begin
    Result:=inherited GetClientRect;
    case SelectorType of
      stXY : InflateRect(Result,-SelectorSize,-SelectorSize);
      stX  : InflateRect(Result,-SelectorSize,0);
      stY  : InflateRect(Result,0,-SelectorSize);
    end;
  end;

procedure TXYSelector.SetBounds
   (
   ALeft           : Integer;
   ATop            : Integer;
   AWidth          : Integer;
   AHeight         : Integer
   );
  begin
    inherited SetBounds(ALeft,ATop,AWidth,AHeight);
    with SelectorRect do begin
      FSelectorSave.Width:=Right-Left;
      FSelectorSave.Height:=Bottom-Top;
    end;
  end;

end.
