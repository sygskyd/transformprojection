{******************************************************************************+
  Unit RegDB
--------------------------------------------------------------------------------
  Autor: Martin Forst
--------------------------------------------------------------------------------
  Registierungsdatenbank
+******************************************************************************}
unit RegDB;

interface

{$H+}

uses
  Classes, WinTypes, Objects;

const
  MaxKeyLength = 30;

  rdbeNoError = 0;
  rdbeIllegalType = 1;
  rdbeUnknownEntry = 2;

type
  TRegistryHeap = class
  private
    FFreeList: LongInt;
    FGlobalSize: LongInt;
    FGlobalHandle: THandle;
    FGlobalPtr: PChar;
    FOnChangeSize: TNotifyEvent;
    function GetBlockPointer(BlockNumber: LongInt): Pointer;
    function GetNumber(Ptr: Pointer): LongInt;
    procedure SetGlobalSize(ASize: LongInt);
  protected
  public
    constructor Create;
    destructor Destroy; override;
    function Allocate(Size: LongInt): LongInt;
    procedure DeAllocate(BlockNumber: LongInt);
    procedure LoadFromStream(S: TReader); overload;
    property Numbers[Ptr: Pointer]: LongInt read GetNumber;
    property OnChangeSize: TNotifyEvent read FOnChangeSize write FOnChangeSize;
    property Ptrs[BlockNumber: LongInt]: Pointer read GetBlockPointer; default;
    function ReAllocate(BlockNumber: LongInt; Size: LongInt): LongInt;

    procedure StoreToStream(S: TWriter); overload;

//    procedure LoadFromStream(S: POldStream); overload;
    procedure LoadFromStream(S: TOldStream); overload;

    procedure StoreToStream(S: TOldStream); overload;
//    procedure StoreToStream(S: TOldMemoryStream); overload;
  end;

  TRegDataType = (rtUnknown, rtKey, rtInteger, rtBool, rtFloat, rtText, rtBinary, rtDateTime);

  TRegistryData = packed record
    case Integer of
      1: (Ptr: Pointer; );
      2: (Long: LongInt; );
      3: (Float: Double; );
      4: (Binary: array[0..7] of Char; );
  end;

  TRegistryEntryInfo = class
  private
    FName: string;
    FDataType: TRegDataType;
    FDataSize: Integer;
  public
    property Name: string read FName;
    property DataType: TRegDataType read FDataType;
    property DataSize: Integer read FDataSize;
  end;

  PRegistryEntry = ^TRegistryEntry;
  TRegistryEntry = packed record
    Name: LongInt;
    DataType: TRegDataType;
    DataSize: Word;
    DataPtr: TRegistryData;
    Next: LongInt;
    Children: LongInt;
  end;

  TCustomRegistry = class
  private
    FAliasList: TStringList;
    FCurrentPath: string;
    FLastError: Integer;
  protected
    function GetData(const EntryName: string; var Buffer; Offset,
      BufSize: Integer; var Copied: Integer): TRegDataType; virtual; abstract;
    function GetDataSize(const ValueName: string): Integer; virtual; abstract;
    function GetDataType(const ValueName: string): TRegDataType; virtual; abstract;
    procedure SetCurrentPath(const APath: string); virtual;
    procedure SetData(const Name: string; const Buffer; Size: Integer; ADataType: TRegDataType); virtual; abstract;
  public
    constructor Create;
    destructor Destroy; override;
    function AddAlias(const KeyName, AliasName: string): Boolean;
    property CurrentPath: string read FCurrentPath write SetCurrentPath;
    property DataSize[const ValueName: string]: Integer read GetDataSize;
    property DataType[const ValueName: string]: TRegDataType read GetDataType;
    procedure GetSubKeys(const KeyName: string; SubKeys: TStrings); virtual;
    procedure GetValues(const KeyName: string; Values: TStrings); virtual;
    function GetEntryInfo(const Name: string): TregistryEntryInfo; virtual;
    property LastError: Integer read FLastError;
    function OpenKey(const KeyName: string; CanCreate: Boolean): Boolean; virtual; abstract;
    function ReadBinary(const ValueName: string; var Buffer; BufSize: Integer): Integer;
    function ReadBinaryOffset(const ValueName: string; var Buffer; Offset, BufSize: Integer): Integer;
    function ReadBool(const ValueName: string): Boolean;
    function ReadDateTime(const ValueName: string): TDateTime;
    function ReadFloat(const ValueName: string): Double;
    function ReadInteger(const ValueName: string): LongInt;
    function ReadString(const ValueName: string): string;
    function ReadText(const ValueName: string; Value: PChar; ValueLen: Integer): Integer;
    procedure WriteBinary(const ValueName: string; const Buffer; BufSize: Integer);
    procedure WriteBool(const ValueName: string; Value: Boolean);
    procedure WriteDateTime(const ValueName: string; Value: TDateTime);
    procedure WriteFloat(const ValueName: string; Value: Double);
    procedure WriteInteger(const ValueName: string; Value: LongInt);
    procedure WriteString(const ValueName: string; Value: string);
    procedure WriteText(const ValueName: string; Value: PChar);
  end;

  TRegistryDataBase = class(TCustomRegistry)
  private
    FHeap: TRegistryHeap;
    FCurrentEntry: PRegistryEntry;
    FNameTable: LongInt;
    FNameTableSize: LongInt;
    FRootEntry: LongInt;
    function AddSubEntry(Entry: PRegistryEntry; const KeyName: string): PRegistryEntry;
    procedure DeleteSubEntry(Entry: PRegistryEntry; const KeyName: string);
    procedure DoChangeSize(Sender: TObject);
    function FindCreateEntry(const EntryName: string; ADataType: TRegDataType): PRegistryEntry;
    function FindEntry(const KeyName: string): PRegistryEntry;
    function FindCreateName(const Name: ShortString): LongInt;
    function FindSubEntry(Entry: PRegistryEntry; const KeyName: string): PRegistryEntry;
    procedure ParseKeyName(KeyName: string; Strings: TStringList);
  protected
    function GetData(const EntryName: string; var Buffer; Offset, BufSize: Integer; var Copied: Integer): TRegDataType; override;
    function GetDataSize(const ValueName: string): Integer; override;
    function GetDataType(const ValueName: string): TRegDataType; override;
    procedure SetCurrentPath(const APath: string); override;
    procedure SetData(const Name: string; const Buffer; Size: Integer; ADataType: TRegDataType); override;
  public
    constructor Create;
    destructor Destroy; override;
    function CreateKey(const KeyName: string): Boolean;
    function DeleteKey(const KeyName: string): Boolean;
    function DeleteValue(const ValueName: string): Boolean;
    function GetEntryInfo(const Name: string): TRegistryEntryInfo; override;
    procedure GetSubKeys(const KeyName: string; SubKeys: TStrings); override;
    procedure GetValues(const KeyName: string; Values: TStrings); override;
    function HasSubKeys: Boolean;
    function KeyExists(const KeyName: string): Boolean;
    procedure LoadFromStream(Reader: TReader); overload;
    function OpenKey(const KeyName: string; CanCreate: Boolean): Boolean; override;
    function RenameValue(const OldName, NewName: string): Boolean;
    procedure StoreToStream(Writer: TWriter); overload;
    function ValueExists(const ValueName: string): Boolean;

//    procedure LoadFromStream(S: POldStream); overload;
    procedure LoadFromStream(S: TOldStream); overload;

    procedure StoreToStream(S: TOldStream); overload;
//    procedure StoreToStream(S: TOldMemoryStream); overload;
  end;

  TWindowsRegistry = class(TCustomRegistry)
  end;

  TIniFileRegistry = class(TCustomRegistry)
  end;

implementation

uses SysUtils, WinProcs;

const
  PageSize = 1024;
  NameTablePageSize = 128;

type
  PFreeBlock = ^TFreeBlock;
  TFreeBlock = record
    Size: LongInt;
    Next: LongInt;
  end;

  PNameEntry = ^TNameEntry;
  TNameEntry = record
    UseCount: Word;
    case Integer of
      0: (Name: ShortString; );
      1: (Length: Byte; );
  end;

{==============================================================================}
{ TCustomRegistry                                                              }
{==============================================================================}

constructor TCustomRegistry.Create;
begin
  inherited Create;
  FLastError := rdbeNoError;
  FAliasList := TStringList.Create;
  FAliasList.Sorted := TRUE;
  FAliasList.Duplicates := dupIgnore;
end;

destructor TCustomRegistry.Destroy;
var
  Cnt: Integer;
begin
  for Cnt := 0 to FAliasList.Count - 1 do
    DisposeStr(PString(FAliasList.Objects[Cnt]));
  FAliasList.Free;
  inherited Destroy;
end;

function TCustomRegistry.ReadBinary(const ValueName: string; var Buffer;
  BufSize: Integer): Integer;
begin
  FLastError := rdbeNoError;
  GetData(ValueName, Buffer, 0, BufSize, Result);
end;

function TCustomRegistry.ReadBinaryOffset(const ValueName: string; var Buffer;
  Offset: Integer; BufSize: Integer): Integer;
begin
  FLastError := rdbeNoError;
  GetData(ValueName, Buffer, Offset, BufSize, Result);
end;

function TCustomRegistry.ReadBool(const ValueName: string): Boolean;
var
  Copied: Integer;
begin
  FLastError := rdbeNoError;
  if GetData(ValueName, Result, 0, SizeOf(Result), Copied) <> rtBool then
  begin
    if FLastError = rdbeNoError then
      FLastError := rdbeIllegalType;
    Result := FALSE;
  end;
end;

function TCustomRegistry.ReadFloat(const ValueName: string): Double;
var
  Copied: Integer;
  EntryType: TRegDataType;
begin
  FLastError := rdbeNoError;
  EntryType := GetData(ValueName, Result, 0, SizeOf(Result), Copied);
  if EntryType = rtInteger then
    Result := PLongInt(@Result)^
  else
    if EntryType <> rtFloat then
    begin
      if FLastError = rdbeNoError then
        FLastError := rdbeIllegalType;
      Result := 0;
    end;
end;

function TCustomRegistry.ReadInteger(const ValueName: string): LongInt;
var
  Copied: Integer;
  EntryType: TRegDataType;
  Value: Double;
begin
  FLastError := rdbeNoError;
  EntryType := GetData(ValueName, Value, 0, SizeOf(Value), Copied);
  if EntryType = rtFloat then
    Result := Round(Value)
  else
    if EntryType = rtInteger then
      Result := PLongInt(@Value)^
    else
    begin
      if FLastError = rdbeNoError then
        FLastError := rdbeIllegalType;
      Result := 0;
    end;
end;

function TCustomRegistry.ReadString(const ValueName: string): string;
var
  Copied: Integer;
begin
  FLastError := rdbeNoError;
  SetLength(Result, 512);
  if GetData(ValueName, PChar(Result)^, 0, 512, Copied) <> rtText then
    Result := ''
  else
    SetLength(Result, Copied);
end;

function TCustomRegistry.ReadText(const ValueName: string; Value: PChar;
  ValueLen: Integer): Integer;
begin
  FLastError := rdbeNoError;
  if (GetData(ValueName, Value, 0, ValueLen, Result) <> rtText)
    or (Result = 0) then
  begin
    if FLastError = rdbeNoError then
      FLastError := rdbeIllegalType;
    StrCopy(Value, '');
  end
  else
    Value[Result] := #0;
end;

function TCustomRegistry.ReadDateTime(const ValueName: string): TDateTime;
var
  Copied: Integer;
begin
  FLastError := rdbeNoError;
  if GetData(ValueName, Result, 0, SizeOf(Result), Copied) <> rtDateTime then
  begin
    if FLastError = rdbeNoError then
      FLastError := rdbeIllegalType;
    Result := Now;
  end;
end;

procedure TCustomRegistry.WriteBinary(const ValueName: string; const Buffer;
  BufSize: Integer);
begin
  FLastError := rdbeNoError;
  SetData(ValueName, Buffer, BufSize, rtBinary);
end;

procedure TCustomRegistry.WriteBool(const ValueName: string; Value: Boolean);
begin
  FLastError := rdbeNoError;
  SetData(ValueName, Value, SizeOf(Value), rtBool);
end;

procedure TCustomRegistry.WriteFloat(const ValueName: string; Value: Double);
begin
  FLastError := rdbeNoError;
  SetData(ValueName, Value, SizeOf(Value), rtFloat);
end;

procedure TCustomRegistry.WriteInteger(const ValueName: string; Value: LongInt);
begin
  FLastError := rdbeNoError;
  SetData(ValueName, Value, SizeOf(Value), rtInteger);
end;

procedure TCustomRegistry.WriteString(const ValueName: string; Value: string);
begin
  FLastError := rdbeNoError;
  SetData(ValueName, PChar(Value)^, Length(Value), rtText);
end;

procedure TCustomRegistry.WriteDateTime(const ValueName: string; Value: TDateTime);
begin
  FLastError := rdbeNoError;
  SetData(ValueName, Value, SizeOf(Value), rtDateTime);
end;

procedure TCustomRegistry.WriteText(const ValueName: string; Value: PChar);
begin
  FLastError := rdbeNoError;
  SetData(ValueName, Value^, StrLen(Value), rtText);
end;

procedure TCustomRegistry.SetCurrentPath(const APath: string);
begin
  if APath <> '' then
  begin
    if APath[1] = '\' then
      FCurrentPath := APath
    else
      FCurrentPath := FCurrentPath + APath;
  end;
end;

function TCustomRegistry.AddAlias(const KeyName, AliasName: string): Boolean;
begin
  if FAliasList.IndexOf(KeyName) >= 0 then
    Result := FALSE
  else
  begin
    FAliasList.AddObject(KeyName, Pointer(NewStr(AliasName)));
    Result := TRUE;
  end;
end;

{==============================================================================+
  TRegistryDataBase
+==============================================================================}

constructor TRegistryDataBase.Create;
begin
  inherited Create;
  FHeap := TRegistryHeap.Create;
  FHeap.OnChangeSize := DoChangeSize;
  FNameTable := FHeap.Allocate(NameTablePageSize);
  FNameTableSize := NameTablePageSize;
  FRootEntry := FHeap.Allocate(SizeOf(TRegistryEntry));
  FCurrentEntry := FHeap[FRootEntry];
  with FCurrentEntry^ do
    DataType := rtKey;
  AddAlias('\Users\.Default', '\User');
end;

destructor TRegistryDataBase.Destroy;
begin
  FHeap.Free;
  inherited Destroy;
end;

procedure TRegistryDataBase.DoChangeSize;
begin
  FCurrentEntry := FindEntry(CurrentPath);
end;

function TRegistryDataBase.FindCreateName(const Name: ShortString): LongInt;
var
  Source: PNameEntry;
  Position: LongInt;
  PossiblyFree: LongInt;
  CurLength: Integer;

  procedure GetPosition;
  begin
    Source := FHeap[Position];
    CurLength := Source^.Length;
    if CurLength and $80 <> 0 then
      CurLength := -(CurLength and $7F);
  end;
begin
  Position := FNameTable;
  GetPosition;
  PossiblyFree := -1;
  while CurLength <> 0 do
  begin
    if (CurLength > 0)
      and (CompareText(Name, Source^.Name) = 0) then
    begin
      Inc(Source^.UseCount);
      Result := Position - FNameTable;
      Exit;
    end
    else
      if (CurLength < 0)
        and (Abs(CurLength) >= Length(Name) + 3) then
        PossiblyFree := Position;
    Position := Position + Abs(CurLength) + 3;
    GetPosition;
  end;
  if PossiblyFree >= 0 then
  begin
    Position := PossiblyFree;
    GetPosition;
    Move(Name, Source^, Length(Name) + 1);
    if Length(Name) < Abs(CurLength) then
    begin
      Position := Position + Length(Name);
      GetPosition;
      Source^.Length := (Abs(CurLength) - Length(Name) - 1) or $80;
    end;
    Source^.UseCount := 1;
    Result := PossiblyFree - FNameTable;
  end
  else
  begin
    // need palace for name, use-count, name-length and a blank entry at the end
    if FNameTableSize - Position + FNameTable <= Length(Name) + 6 then
    begin
      Dec(Position, FNameTable);
      FNameTable := FHeap.ReAllocate(FNameTable, FNameTableSize + NameTablePageSize);
      Inc(FNameTableSize, NameTablePageSize);
      Inc(Position, FNameTable);
      Source := FHeap[Position];
    end;
    Move(Name, Source^.Name, Length(Name) + 1);
    Source^.UseCount := 1;
    Result := Position - FNameTable;
  end;
end;

procedure TRegistryDataBase.ParseKeyName
  (
  KeyName: string;
  Strings: TStringList
  );
var
  APos: Integer;
begin
  APos := Pos('\', KeyName);
  while APos <> 0 do
  begin
    if APos = 1 then
      Delete(KeyName, 1, 1)
    else
    begin
      if APos > MaxKeyLength then
        Strings.Add(Copy(KeyName, 1, MaxKeyLength))
      else
        Strings.Add(Copy(KeyName, 1, APos - 1));
      Delete(KeyName, 1, APos);
    end;
    APos := Pos('\', KeyName);
  end;
  if KeyName <> '' then
    Strings.Add(KeyName);
end;

function TRegistryDataBase.CreateKey
  (
  const KeyName: string
  )
  : Boolean;
begin
  FLastError := rdbeNoError;
  FindCreateEntry(KeyName, rtKey);
  Result := TRUE;
end;

function TRegistryDataBase.FindEntry
  (
  const KeyName: string
  )
  : PRegistryEntry;
var
  KeyList: TStringList;
  Cnt: Integer;
begin
  if KeyName = '' then
    Result := FCurrentEntry
  else
  begin
    KeyList := TStringList.Create;
    try
      ParseKeyName(KeyName, KeyList);
      if KeyName[1] = '\' then
        Result := FHeap[FRootEntry]
      else
        Result := FCurrentEntry;
      for Cnt := 0 to KeyList.Count - 1 do
      begin
        Result := FindSubEntry(Result, KeyList[Cnt]);
        if Result = nil then
          Exit;
      end;
    finally
      KeyList.Free;
    end;
  end;
end;

function TRegistryDataBase.FindCreateEntry
  (
  const EntryName: string;
  ADataType: TRegDataType
  )
  : PRegistryEntry;
var
  KeyList: TStringList;
  Entry: PRegistryEntry;
  Cnt: Integer;
  Cnt1: Integer;
begin
  if EntryName = '' then
    Result := FCurrentEntry
  else
  begin
    KeyList := TStringList.Create;
    try
      ParseKeyName(EntryName, KeyList);
      if EntryName[1] = '\' then
        Result := FHeap[FRootEntry]
      else
      begin
        if FCurrentEntry = nil then
          FCurrentEntry := FindCreateEntry(CurrentPath, rtKey);
        Result := FCurrentEntry;
      end;
      for Cnt := 0 to KeyList.Count - 1 do
      begin
        Entry := FindSubEntry(Result, KeyList[Cnt]);
        if Entry = nil then
        begin
          for Cnt1 := Cnt to KeyList.Count - 1 do
          begin
            Result := AddSubEntry(Result, KeyList[Cnt1]);
            Result^.DataType := rtKey;
          end;
          Result^.DataType := ADataType;
          Break;
        end
        else
          Result := Entry;
      end;
    finally
      KeyList.Free;
    end;
  end;
end;

function TRegistryDataBase.DeleteKey
  (
  const KeyName: string
  )
  : Boolean;
var
  Entry: PRegistryEntry;
begin
  FLastError := rdbeNoError;
  Result := FALSE;
  Entry := FindEntry(KeyName);
  if Entry = nil then
    FLastError := rdbeUnknownEntry
  else
    if Entry^.DataType <> rtKey then
      FLastError := rdbeIllegalType
    else
    begin
{      DeleteSubEntry(FHeap[Entry^.Parent],Entry^.Name);}
      Result := TRUE;
    end;
end;

function TRegistryDataBase.DeleteValue
  (
  const ValueName: string
  )
  : Boolean;
var
  Entry: PRegistryEntry;
begin
  Result := FALSE;
  FLastError := rdbeNoError;
  Entry := FindEntry(ValueName);
  if Entry = nil then
    FLastError := rdbeUnknownEntry
  else
    if Entry^.DataType in [rtUnknown, rtKey] then
      FLastError := rdbeIllegalType
    else
    begin
{      DeleteSubEntry(FHeap[Entry^.Parent],Entry^.Name);}
      Result := TRUE;
    end;
end;

function TRegistryDataBase.GetDataSize
  (
  const ValueName: string
  )
  : Integer;
var
  Entry: PRegistryEntry;
begin
  FLastError := rdbeNoError;
  Result := 0;
  Entry := FindEntry(ValueName);
  if Entry = nil then
    FLastError := rdbeUnknownEntry
  else
    if Entry^.DataType in [rtUnknown, rtKey] then
      FLastError := rdbeIllegalType
    else
      Result := Entry^.DataSize;
end;

function TRegistryDataBase.GetDataType
  (
  const ValueName: string
  )
  : TRegDataType;
var
  Entry: PRegistryEntry;
begin
  FLastError := rdbeNoError;
  Result := rtUnknown;
  Entry := FindEntry(ValueName);
  if Entry = nil then
    FLastError := rdbeUnknownEntry
  else
    Result := Entry^.DataType;
end;

function TRegistryDataBase.HasSubKeys
  : Boolean;
begin
  FLastError := rdbeNoError;
  Result := (FCurrentEntry <> nil) and (FCurrentEntry^.Children <> 0);
end;

function TRegistryDataBase.KeyExists
  (
  const KeyName: string
  )
  : Boolean;
var
  Entry: PRegistryEntry;
begin
  FLastError := rdbeNoError;
  Result := FALSE;
  Entry := FindEntry(KeyName);
  if Entry = nil then
    FLastError := rdbeUnknownEntry
  else
    if Entry^.DataType <> rtKey then
      FLastError := rdbeIllegalType
    else
      Result := TRUE;
end;

function TRegistryDataBase.OpenKey
  (
  const KeyName: string;
  CanCreate: Boolean
  )
  : Boolean;
var
  Entry: PRegistryEntry;
begin
  FLastError := rdbeNoError;
  if KeyName = '' then
    Result := TRUE
  else
  begin
    FCurrentEntry := nil;
    Result := FALSE;
    if KeyName[1] = '\' then
      FCurrentPath := KeyName
    else
      FCurrentPath := FCurrentPath + '\';
    if CanCreate then
      Entry := FindCreateEntry(KeyName, rtKey)
    else
      Entry := FindEntry(KeyName);
    if Entry = nil then
      FLastError := rdbeUnknownEntry
    else
      if Entry^.DataType <> rtKey then
        FLastError := rdbeIllegalType
      else
      begin
        FCurrentEntry := Entry;
        Result := TRUE;
      end;
  end;
end;

function TRegistryDataBase.GetData
  (
  const EntryName: string;
  var Buffer;
  Offset: Integer;
  BufSize: Integer;
  var Copied: Integer
  )
  : TRegDataType;
var
  Source: PChar;
  Entry: PRegistryEntry;
begin
  Copied := 0;
  Entry := FindEntry(EntryName);
  if Entry = nil then
  begin
    FLastError := rdbeUnknownEntry;
    Result := rtUnknown;
  end
  else
    if Entry^.DataType = rtKey then
      Result := rtKey
    else
    begin
      if Entry^.DataSize <= 8 then
        Source := @Entry^.DataPtr
      else
        Source := FHeap[Entry^.DataPtr.Long];
      if BufSize + Offset < Entry^.DataSize then
        Copied := BufSize
      else
        if Offset >= BufSize then
          Copied := 0
        else
          Copied := Entry^.DataSize - Offset;
      Move(Source[Offset], Buffer, Copied);
      Result := Entry^.DataType;
    end;
end;

function TRegistryDataBase.RenameValue
  (
  const OldName: string;
  const NewName: string
  )
  : Boolean;
begin
  Result := FALSE;
end;

function TRegistryDataBase.ValueExists
  (
  const ValueName: string
  )
  : Boolean;
var
  Entry: PRegistryEntry;
begin
  FLastError := rdbeNoError;
  Result := FALSE;
  Entry := FindEntry(ValueName);
  if Entry = nil then
    FLastError := rdbeUnknownEntry
  else
    if Entry^.DataType in [rtKey, rtUnknown] then
      FLastError := rdbeIllegalType
    else
      Result := TRUE;
end;

procedure TRegistryDataBase.SetData
  (
  const Name: string;
  const Buffer;
  Size: Integer;
  ADataType: TRegDataType
  );
var
  Entry: PRegistryEntry;
  Dest: PChar;
  EntryNumber: LongInt;
  Data: LongInt;
begin
  Entry := FindCreateEntry(Name, rtUnknown);
  if Entry^.DataType = rtKey then
    FLastError := rdbeIllegalType
  else
  begin
    Entry^.DataType := ADataType;
    if Size <= 8 then
    begin
      if Entry^.DataSize > 8 then
        FHeap.DeAllocate(Entry^.DataPtr.Long);
      Dest := @Entry^.DataPtr;
    end
    else
    begin
      if Entry^.DataSize <= 8 then
        Entry^.DataPtr.Long := 0;
      EntryNumber := FHeap.Numbers[Entry];
      if Entry^.DataPtr.Long = 0 then
        Data := FHeap.Allocate(Size)
      else
        Data := FHeap.ReAllocate(Entry^.DataPtr.Long, Size);
      Entry := FHeap[EntryNumber];
      Entry^.DataPtr.Long := Data;
      Dest := FHeap[Data];
    end;
    Entry^.DataSize := Size;
    if Size > 0 then
      Move(Buffer, Dest^, Size);
  end;
end;

procedure TRegistryDataBase.SetCurrentPath
  (
  const APath: string
  );
begin
  inherited SetCurrentPath(APath);
  FCurrentEntry := FindEntry(APath);
end;

function TRegistryDataBase.FindSubEntry(Entry: PRegistryEntry; const KeyName: string): PRegistryEntry;
var
  SubNumber: LongInt;
begin
  if Entry <> nil then
  begin
    SubNumber := Entry^.Children;
    while SubNumber <> 0 do
    begin
      Result := FHeap[SubNumber];
      if CompareText(PNameEntry(FHeap[FNameTable + Result^.Name])^.Name, KeyName) = 0 then
        Exit;
      SubNumber := Result^.Next;
    end;
  end;
  Result := nil;
end;

function TRegistryDataBase.AddSubEntry
  (
  Entry: PRegistryEntry;
  const KeyName: string
  )
  : PRegistryEntry;
var
  ANumber: LongInt;
  EntryNumber: LongInt;
begin
  if Entry^.Children = 0 then
  begin
    ANumber := FHeap.Allocate(SizeOf(TRegistryEntry));
    Entry^.Children := ANumber;
  end
  else
  begin
    Entry := FHeap[Entry^.Children];
    while Entry^.Next <> 0 do
      Entry := FHeap[Entry^.Next];
    ANumber := FHeap.Allocate(SizeOf(TRegistryEntry));
    Entry^.Next := ANumber
  end;
  EntryNumber := FindCreateName(KeyName);
  Result := FHeap[ANumber];
  Result^.Name := EntryNumber;
end;

procedure TRegistryDataBase.DeleteSubEntry
  (
  Entry: PRegistryEntry;
  const KeyName: string
  );
begin
end;

{procedure TRegistryDataBase.LoadFromStream(S: POldStream);
begin
  FHeap.LoadFromStream(S);
  S.Read(FNameTable, SizeOf(FNameTable));
  S.Read(FNameTableSize, SizeOf(FNameTableSize));
  S.Read(FRootEntry, SizeOf(FRootEntry));
  FCurrentEntry := FHeap[FRootEntry];
  FCurrentPath := '';
end;}

procedure TRegistryDataBase.LoadFromStream(S: TOldStream);
begin
  SELF.FHeap.LoadFromStream(S);
  S.Read(FNameTable, SizeOf(FNameTable));
  S.Read(FNameTableSize, SizeOf(FNameTableSize));
  S.Read(FRootEntry, SizeOf(FRootEntry));
  FCurrentEntry := FHeap[FRootEntry];
  FCurrentPath := '';
end;

procedure TRegistryDataBase.StoreToStream(S: TOldStream);
begin
  FHeap.StoreToStream(S);
  S.Write(FNameTable, SizeOf(FNameTable));
  S.Write(FNameTableSize, SizeOf(FNameTableSize));
  S.Write(FRootEntry, SizeOf(FRootEntry));
end;

{procedure TRegistryDataBase.StoreToStream(S: TOldMemoryStream);
begin
  SELF.FHeap.StoreToStream(S);
  S.Write(FNameTable, SizeOf(FNameTable));
  S.Write(FNameTableSize, SizeOf(FNameTableSize));
  S.Write(FRootEntry, SizeOf(FRootEntry));
end;}

procedure TRegistryDataBase.LoadFromStream(Reader: TReader);
begin
  FHeap.LoadFromStream(Reader);
  Reader.Read(FNameTable, SizeOf(FNameTable));
  Reader.Read(FNameTableSize, SizeOf(FNameTableSize));
  Reader.Read(FRootEntry, SizeOf(FRootEntry));
  FCurrentEntry := FHeap[FRootEntry];
  FCurrentPath := '';
end;

procedure TRegistryDataBase.StoreToStream(Writer: TWriter);
begin
  FHeap.StoreToStream(Writer);
  Writer.Write(FNameTable, SizeOf(FNameTable));
  Writer.Write(FNameTableSize, SizeOf(FNameTableSize));
  Writer.Write(FRootEntry, SizeOf(FRootEntry));
end;

{==============================================================================+
  TRegistryHeap
+==============================================================================}

constructor TRegistryHeap.Create;
begin
  inherited Create;
  FFreeList := -1;
end;

destructor TRegistryHeap.Destroy;
begin
  if FGlobalHandle <> 0 then
  begin
    GlobalUnlock(FGlobalHandle);
    GlobalFree(FGlobalHandle);
  end;
  inherited Destroy;
end;

function TRegistryHeap.GetBlockPointer
  (
  BlockNumber: LongInt
  )
  : Pointer;
begin
  Result := FGlobalPtr + BlockNumber;
end;

function TRegistryHeap.GetNumber
  (
  Ptr: Pointer
  )
  : LongInt;
begin
  Result := PChar(Ptr) - FGlobalPtr;
end;

function TRegistryHeap.Allocate
  (
  Size: LongInt
  )
  : LongInt;
var
  BlockNumber: LongInt;
  BlockPtr: PFreeBlock;
  NewBlockPtr: PFreeBlock;
  PrevBlockPtr: PFreeBlock;

  procedure DoAllocate;
  begin
    if PrevBlockPtr = nil then
      FFreeList := BlockPtr^.Next
    else
      PrevBlockPtr^.Next := BlockPtr^.Next;
    FillChar(BlockPtr^, Size, #0);
    BlockPtr^.Size := Size;
    Result := BlockNumber + 4;
  end;
begin
  Inc(Size, 4);
  if Size < 8 then
    Size := 8;
  while TRUE do
  begin
    PrevBlockPtr := nil;
    BlockNumber := FFreeList;
    while BlockNumber > -1 do
    begin
      BlockPtr := Ptrs[BlockNumber];
      if BlockPtr^.Size = Size then
      begin
        DoAllocate;
        Exit;
      end
      else
        if BlockPtr^.Size - Size >= 8 then
        begin
          NewBlockPtr := Ptrs[BlockNumber + Size];
          NewBlockPtr^.Size := BlockPtr^.Size - Size;
          NewBlockPtr^.Next := BlockPtr^.Next;
          BlockPtr^.Next := BlockNumber + Size;
          DoAllocate;
          Exit;
        end
        else
          BlockNumber := BlockPtr^.Next;
      PrevBlockPtr := BlockPtr;
    end;
    SetGlobalSize(FGlobalSize + Size);
  end;
end;

procedure TRegistryHeap.SetGlobalSize
  (
  ASize: LongInt
  );
var
  BlockPtr: PFreeBlock;
begin
  if ASize mod PageSize <> 0 then
    ASize := (ASize div PageSize + 1) * PageSize;
  if ASize <> FGlobalSize then
  begin
    if FGlobalHandle <> 0 then
    begin
      GlobalUnlock(FGlobalHandle);
      FGlobalHandle := GlobalRealloc(FGlobalHandle, ASize, GMEM_MOVEABLE or GMEM_ZEROINIT);
    end
    else
      FGlobalHandle := GlobalAlloc(GMEM_MOVEABLE or GMEM_ZEROINIT, ASize);
    FGlobalPtr := GlobalLock(FGlobalHandle);
    BlockPtr := Ptrs[FGlobalSize];
    BlockPtr^.Size := ASize - FGlobalSize;
    BlockPtr^.Next := FFreeList;
    FFreeList := FGlobalSize;
    FGlobalSize := ASize;
    if Assigned(FOnChangeSize) then
      OnChangeSize(Self);
  end;
end;

procedure TRegistryHeap.DeAllocate
  (
  BlockNumber: LongInt
  );
var
  BlockPtr: PFreeBlock;
begin
  BlockPtr := Ptrs[BlockNumber - 4];
  FillChar(Ptrs[BlockNumber]^, BlockPtr^.Size - 4, #0);
  BlockPtr^.Next := FFreeList;
  FFreeList := BlockNumber - 4;
end;

function TRegistryHeap.ReAllocate
  (
  BlockNumber: LongInt;
  Size: LongInt
  )
  : LongInt;
var
  BlockPtr: PFreeBlock;
  NewPtr: PFreeBlock;
begin
  if Size = PFreeBlock(Ptrs[BlockNumber - 4])^.Size - 4 then
    Result := BlockNumber
  else
  begin
    Result := Allocate(Size);
    BlockPtr := Ptrs[BlockNumber - 4];
    NewPtr := Ptrs[Result - 4];
    if NewPtr^.Size < BlockPtr^.Size then
      Move(Ptrs[BlockNumber]^, Ptrs[Result]^, NewPtr^.Size - 4)
    else
      Move(Ptrs[BlockNumber]^, Ptrs[Result]^, BlockPtr^.Size - 4);
    DeAllocate(BlockNumber);
  end;
end;

{procedure TRegistryHeap.LoadFromStream(S: POldStream);
var
  ASize: LongInt;
  Position: LongInt;
  Count: LongInt;
begin
  S.Read(ASize, SizeOf(ASize));
  SetGlobalSize(ASize);
  S.Read(FFreeList, SizeOf(FFreeList));
  Position := 0;
  for Count := 1 to FGlobalSize div PageSize do
  begin
    S.Read(Ptrs[Position]^, PageSize);
    Inc(Position, PageSize);
  end;
end;}

procedure TRegistryHeap.LoadFromStream(S: TOldStream);
var
  ASize: LongInt;
  Position: LongInt;
  Count: LongInt;
begin
  S.Read(ASize, SizeOf(ASize));
  SetGlobalSize(ASize);
  S.Read(FFreeList, SizeOf(FFreeList));
  Position := 0;
  for Count := 1 to FGlobalSize div PageSize do
  begin
    S.Read(Ptrs[Position]^, PageSize);
    Inc(Position, PageSize);
  end;
end;

procedure TRegistryHeap.StoreToStream(S: TOldStream);
var
  Position: LongInt;
  Count: LongInt;
begin
  S.Write(FGlobalSize, SizeOf(FGlobalSize));
  S.Write(FFreeList, SizeOf(FFreeList));
  Position := 0;
  for Count := 1 to FGlobalSize div PageSize do
  begin
    S.Write(Ptrs[Position]^, PageSize);
    Inc(Position, PageSize);
  end;
end;

{procedure TRegistryHeap.StoreToStream(S: TOldMemoryStream);
var
  Position: LongInt;
  Count: LongInt;
begin
  S.Write(FGlobalSize, SizeOf(FGlobalSize));
  S.Write(FFreeList, SizeOf(FFreeList));
  Position := 0;
  for Count := 1 to FGlobalSize div PageSize do
  begin
    S.Write(Ptrs[Position]^, PageSize);
    Inc(Position, PageSize);
  end;
end;}

procedure TRegistryHeap.LoadFromStream(S: TReader);
var
  ASize: LongInt;
  Position: LongInt;
  Count: LongInt;
begin
  S.Read(ASize, SizeOf(ASize));
  SetGlobalSize(ASize);
  S.Read(FFreeList, SizeOf(FFreeList));
  Position := 0;
  for Count := 1 to FGlobalSize div PageSize do
  begin
    S.Read(Ptrs[Position]^, PageSize);
    Inc(Position, PageSize);
  end;
end;

procedure TRegistryHeap.StoreToStream(S: TWriter);
var
  Position: LongInt;
  Count: LongInt;
begin
  S.Write(FGlobalSize, SizeOf(FGlobalSize));
  S.Write(FFreeList, SizeOf(FFreeList));
  Position := 0;
  for Count := 1 to FGlobalSize div PageSize do
  begin
    S.Write(Ptrs[Position]^, PageSize);
    Inc(Position, PageSize);
  end;
end;

procedure TCustomRegistry.GetSubKeys(const KeyName: string; SubKeys: TStrings);
begin
end;

procedure TCustomRegistry.GetValues(const KeyName: string; Values: TStrings);
begin
end;

procedure TRegistryDataBase.GetSubKeys(const KeyName: string; SubKeys: TStrings);
var
  Entry: PRegistryEntry;
  SubNumber: LongInt;
begin
  Entry := FindEntry(KeyName);
  if Entry <> nil then
  begin
    SubNumber := Entry^.Children;
    while SubNumber <> 0 do
    begin
      Entry := FHeap[SubNumber];
      if Entry^.DataType = rtKey then
        SubKeys.Add(PNameEntry(
          FHeap[FNameTable + Entry^.Name])^.Name);
      SubNumber := Entry^.Next;
    end;
  end;
end;

procedure TRegistryDataBase.GetValues(const KeyName: string; Values: TStrings);
var
  Entry: PRegistryEntry;
  SubNumber: LongInt;
begin
  Entry := FindEntry(KeyName);
  if Entry <> nil then
  begin
    SubNumber := Entry^.Children;
    while SubNumber <> 0 do
    begin
      Entry := FHeap[SubNumber];
      if Entry^.DataType <> rtKey then
        Values.Add(PNameEntry(
          FHeap[FNameTable + Entry^.Name])^.Name);
      SubNumber := Entry^.Next;
    end;
  end;
end;

function TCustomRegistry.GetEntryInfo(const Name: string): TRegistryEntryInfo;
begin
  Result := TRegistryEntryInfo.Create;
  Result.FName := Name;
  Result.FDataType := GetDataType(Name);
  Result.FDataSize := GetDataSize(Name);
end;

function TRegistryDataBase.GetEntryInfo(const Name: string): TRegistryEntryInfo;
var
  Entry: PRegistryEntry;
begin
  Entry := FindEntry(Name);
  if Entry = nil then
    Result := nil
  else
  begin
    Result := TRegistryEntryInfo.Create;
    Result.FName := Name;
    Result.FDataType := Entry^.DataType;
    Result.FDataSize := Entry^.DataSize;
  end;
end;

end.

