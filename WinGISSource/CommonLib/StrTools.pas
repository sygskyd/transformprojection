Unit StrTools;

Interface

{*******************************************************************************
| Function ScanText
|-------------------------------------------------------------------------------
| Reads values from a formatted string. Variables are specified by a % char
| and the type-specifier. The output-variables are given in an open array.
| %s = pchar or pointer to string (short-string for 32-bit programs)
| %S = huge-string
| %d = pointer to double
| %i = pointer to integer
| %l = pointer to long-int
| The return-value is the number of variables successfully read.
*******************************************************************************}
Function ScanText(Format:PChar;Text:PChar;Const Parameters:Array of Const):Integer;

Function ScanString(Const Format:String;Const Text:String;Const Parameters:Array of Const):Integer;

Function SplitStringList(Text:String;Const QuoteChar:Char;Const SplitChars:String;
    Strings:Array of String):Integer;

{******************************************************************************+
  CalculateEqualLen
--------------------------------------------------------------------------------
  Determines the number of equal charactes at the beginning of the two string.
  The function is case-insensitive and can handle DBCS-strings.
+******************************************************************************}
Function CalculateEqualLen(SearchText:String;Text:String):Integer;

{******************************************************************************+
  Function IsPrefix
--------------------------------------------------------------------------------
  Determines if Text starts with Prefix. The compare may be made case-
  sensitive of case-insensitive. The function can handle DBCS-strings.
+******************************************************************************}
Function IsPrefix(Prefix,Text:String;CaseSensitive:Boolean=FALSE):Boolean;

{******************************************************************************+
  Function IsSuffix
--------------------------------------------------------------------------------
  Determines if Text ends with Suffix. The compare may be made case-
  sensitive of case-insensitive. The function can handle DBCS-strings.
+******************************************************************************}
Function IsSuffix(Suffix,Text:String;CaseSensitive:Boolean=FALSE):Boolean;

{******************************************************************************+
  Function StrLPas
--------------------------------------------------------------------------------
  Copies MaxLength characters from Text to the pascal-string.  
+******************************************************************************}
Function StrLPas(Text:PChar;MaxLength:Integer):String;

Implementation

{$IFDEF WIN32}
Uses Classes,NumTools,SysUtils;
{$ELSE}
Uses AnsiStr,Classes,NumTools,SysUtils;
{$ENDIF}

Type TTokenType    = (ttText,ttString,ttDouble,ttInteger,ttLongInt,ttLString);

     PToken        = ^TToken;
     TToken        = Record
       TokenType   : TTokenType;
       TokenText   : PChar;
     end;

Function ScanText
   (
   Format           : PChar;
   Text             : PChar;
   Const Parameters : Array of Const
   )
   : Integer;
  var TokenList    : TList;
      CurPos       : PChar;
      LastPos      : PChar;
      EndPos       : PChar;
      CurToken     : PToken;
      NextToken    : PToken;
      Cnt          : Integer;
      CurParameter : Integer;
  Procedure AddToken
     (
     AType         : TTokenType;
     AText         : PChar;
     ALength       : Integer
     );
    var Token      : PToken;
    begin
      New(Token);
      with Token^ do begin
        TokenType:=AType;
        if ALength>0 then begin
          TokenText:=StrAlloc(ALength+1);
          StrLCopy(TokenText,AText,ALength);
        end
        else TokenText:=NIL;
      end;
      TokenList.Add(Token);
    end;
  Function GetString
     : Boolean;
    begin
      Result:=TRUE;
      if Parameters[CurParameter].VType=vtString then begin
        StrLCopy(@(Parameters[CurParameter].VString^[1]),CurPos,LastPos-CurPos);
        Parameters[CurParameter].VString^[0]:=Char(LastPos-CurPos);
      end
      else if Parameters[CurParameter].VType=vtPChar then
        StrLCopy(Parameters[CurParameter].VPChar,CurPos,LastPos-CurPos)
      else Result:=FALSE;
    end;
  Function GetLongString
     : Boolean;
    begin
{      if Parameters[CurParameter].VType<>vtPointer then Result:=FALSE
      else begin
        Result:=TRUE;
        try
          StrLCopy(PChar(Parameters[CurParameter].VPointer),CurPos,LastPos-CurPos);
        except
          Result:=FALSE;
        end;
      end;}
      Result:=FALSE;
    end;
  Function GetFloat
     : Boolean;
    var AText      : Array[0..255] of Char;
    Type PDouble   = ^Double;
    begin
      if Parameters[CurParameter].VType<>vtPointer then Result:=FALSE
      else begin
        Result:=TRUE;
        try
          StrLCopy(AText,CurPos,LastPos-CurPos);
          PDouble(Parameters[CurParameter].VPointer)^:=StrToFloat(StrPas(AText));
        except
          Result:=FALSE;
        end;
      end;
    end;
  Function GetInteger
     : Boolean;
    var AText      : Array[0..255] of Char;
        BText      : String;
        APos       : Integer;
    Type PInteger  = ^Integer;
    begin
      if Parameters[CurParameter].VType<>vtPointer then Result:=FALSE
      else begin
        Result:=TRUE;
        try
          StrLCopy(AText,CurPos,LastPos-CurPos);
          BText:=StrPas(AText);
          APos:=Pos('0x',BText);
          if APos<>0 then begin
            Delete(BText,APos,2);
            Insert('$',BText,APos);
          end;
          PInteger(Parameters[CurParameter].VPointer)^:=StrToInt(BText);
        except
          Result:=FALSE;
        end;
      end;
    end;
  Function GetLongInt
     : Boolean;
    var AText      : Array[0..255] of Char;
        BText      : String;
        APos       : Integer;
    Type PLongInt  = ^LongInt;
    begin
      if Parameters[CurParameter].VType<>vtPointer then Result:=FALSE
      else begin
        Result:=TRUE;
        try
          StrLCopy(AText,CurPos,LastPos-CurPos);
          BText:=StrPas(AText);
          APos:=Pos('0x',BText);
          if APos<>0 then begin
            Delete(BText,APos,2);
            Insert('$',BText,APos);
          end;
          PLongInt(Parameters[CurParameter].VPointer)^:=StrToInt(Trim(BText));
        except
          Result:=FALSE;
        end;
      end;
    end;
  begin
    Result:=0;
    TokenList:=TList.Create;
    try
      LastPos:=Format;
      CurPos:=AnsiStrScan(Format,'%');
      EndPos:=StrEnd(Format);
      while CurPos<>NIL do begin
        if LastPos<>CurPos then AddToken(ttText,LastPos,CurPos-LastPos);
        if CurPos<EndPos then begin
          case (CurPos+1)^ of
            's' : AddToken(ttString,NIL,0);
            'S' : AddToken(ttLString,NIL,0);
            'd',
            'D' : AddToken(ttDouble,NIL,0);
            'i',
            'I' : AddToken(ttInteger,NIL,0);
            'l',
            'L' : AddToken(ttLongInt,NIL,0);
          end;
        end;
        LastPos:=CurPos+2;
        CurPos:=AnsiStrScan(CurPos+1,'%');
      end;
      if LastPos<EndPos then AddToken(ttText,LastPos,EndPos-LastPos);
      CurPos:=Text;
      CurParameter:=0;
      for Cnt:=0 to TokenList.Count-1 do begin
        CurToken:=TokenList[Cnt];
        if CurToken^.TokenType=ttText then begin
          if StrLen(CurPos)<StrLen(CurToken^.TokenText) then Exit;
          if StrLComp(CurPos,CurToken^.TokenText,StrLen(CurToken^.TokenText))<>0 then Exit;
          CurPos:=CurPos+StrLen(CurToken^.TokenText);
        end
        else begin
          if Cnt=TokenList.Count-1 then LastPos:=StrEnd(Text)
          else begin
            NextToken:=TokenList[Cnt+1];
            if NextToken^.TokenType<>ttText then LastPos:=StrEnd(Text)
            else begin
              LastPos:=AnsiStrPos(CurPos,NextToken^.TokenText);
              if LastPos=NIL then LastPos:=StrEnd(Text);
            end;  
          end;
          if CurParameter>High(Parameters) then Exit;
          case CurToken^.TokenType of
            ttString   : if not GetString then Exit;
            ttLString  : if not GetLongString then Exit;
            ttDouble   : if not GetFloat then Exit;
            ttInteger  : if not GetInteger then Exit;
            ttLongInt  : if not GetLongInt then Exit;
          end;
          CurPos:=LastPos;
          Inc(CurParameter);
        end;
      end;
    finally
      Result:=CurParameter;
      for Cnt:=0 to TokenList.Count-1 do begin
        StrDispose(PToken(TokenList[Cnt])^.TokenText);
        Dispose(PToken(TokenList[Cnt]));
      end;
      TokenList.Free;
    end;
  end;

Function ScanString
   (
   Const Format     : String;
   Const Text       : String;
   Const Parameters : Array of Const
   )
   : Integer;
  var AFormat       : PChar;
      AText         : PChar;
  begin
    if Text='' then Result:=0
    else begin
      {$IFDEF WIN32}
      {$IFOPT H+}
      Result:=ScanText(PChar(Format),PChar(Text),Parameters);
      {$ELSE}
      AFormat:=StrAlloc(Length(Format)+1);
      AText:=StrAlloc(Length(Text)+1);
      Result:=ScanText(StrPCopy(AFormat,Format),StrPCopy(AText,Text),Parameters);
      StrDispose(AFormat);
      StrDispose(AText);
      {$ENDIF}
      {$ELSE}
      AFormat:=StrAlloc(Length(Format)+1);
      AText:=StrAlloc(Length(Text)+1);
      Result:=ScanText(StrPCopy(AFormat,Format),StrPCopy(AText,Text),Parameters);
      StrDispose(AFormat);
      StrDispose(AText);
      {$ENDIF}
    end;
  end;

Function SplitStringList
   (
   Text            : String;
   Const QuoteChar : Char;
   Const SplitChars: String;
   Strings     : Array of String
   )
   : Integer;
  var Cnt          : Integer;
      InQuotes     : Boolean;
      CurParam     : String;
      ParamCount   : Integer;
  Function AddParameter
     : Boolean;
    begin
      if ParamCount<=High(Strings) then begin
        Strings[ParamCount]:=CurParam;
        Inc(ParamCount);
        CurParam:='';
        Result:=TRUE;
      end
      else Result:=FALSE;  
    end;
  begin
    ParamCount:=0;
    try
      if Text<>'' then begin
        InQuotes:=FALSE;
        CurParam:='';
        Cnt:=1;
        while Cnt<=Length(Text) do begin
          if Text[Cnt] = QuoteChar then begin
            if (Cnt<Length(Text)) and (Text[Cnt+1]=QuoteChar) then begin
              CurParam:=Concat(CurParam,QuoteChar);
              Inc(Cnt);
            end
            else if not InQuotes then InQuotes:=TRUE
            else if not AddParameter then begin
              Result:=ParamCount;
              Exit;
            end;
          end
          else if InQuotes then CurParam:=Concat(CurParam,Text[Cnt])
          else if Pos(Text[Cnt],SplitChars)<>0 then AddParameter
          else CurParam:=Concat(CurParam,Text[Cnt]);
          Inc(Cnt);
        end;
        AddParameter;
      end;  
    finally
      Result:=ParamCount;
    end;
  end;

{******************************************************************************+
  Function CalculateEqualLen
--------------------------------------------------------------------------------
  Determines the number of equal charactes at the beginning of the two string.
  The function is case-insensitive and can handle DBCS-strings.
+******************************************************************************}
Function CalculateEqualLen
   (
   SearchText    : String;
   Text          : String
   )
   : Integer;
  var Cnt        : Integer;
  begin
    SearchText:=AnsiUpperCase(SearchText);
    Text:=AnsiUpperCase(Text);
    Result:=0;
    for Cnt:=1 to Min(Length(SearchText),Length(Text)) do begin
      if SearchText[Cnt]<>Text[Cnt] then begin
        Result:=ByteToCharIndex(SearchText,Cnt-1);
        if (Result>0) and (ByteType(SearchText,Cnt)=mbTrailByte) then Dec(Result);
        Exit;
      end;
      Result:=Min(Length(SearchText),Length(Text));
    end;
  end;
  
{******************************************************************************+
  Function IsPrefix
--------------------------------------------------------------------------------
  Determines if Text starts with Prefix. The compare may be made case-
  sensitive of case-insensitive. The function can handle DBCS-strings.
+******************************************************************************}
Function IsPrefix(Prefix,Text:String;CaseSensitive:Boolean):Boolean;
begin
  if not CaseSensitive then begin
    Prefix:=AnsiUpperCase(Prefix);
    Text:=AnsiUpperCase(Text);
  end;
  Text:=Copy(Text,1,Min(Length(Text),Length(Prefix)));
  Result:=CompareText(Text,Prefix)=0;
end;

{******************************************************************************+
  Function IsSuffix
--------------------------------------------------------------------------------
  Determines if Text ends with Suffix. The compare may be made case-
  sensitive of case-insensitive. The function can handle DBCS-strings.
+******************************************************************************}
Function IsSuffix(Suffix,Text:String;CaseSensitive:Boolean):Boolean;
begin
  if not CaseSensitive then begin
    Suffix:=AnsiUpperCase(Suffix);
    Text:=AnsiUpperCase(Text);
  end;
  Text:=Copy(Text,Max(0,Length(Text)-Length(Suffix)+1),Min(Length(Text),Length(Suffix)));
  Result:=CompareText(Text,Suffix)=0;
end;

{******************************************************************************+
  Function StrLPas
--------------------------------------------------------------------------------
  Copies MaxLength characters from Text to the pascal-string.  
+******************************************************************************}
Function StrLPas(Text:PChar;MaxLength:Integer):String;
begin
  if MaxLength<=0 then Result:=''
  else begin
    SetLength(Result,MaxLength);
    StrPLCopy(@Result[1],Text,MaxLength);
  end;  
end;

end.
