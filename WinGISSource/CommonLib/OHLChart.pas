{**********************************************}
{   TOHLCSeries (derived from TCustomSeries)   }
{   Copyright (c) 1995-2000 by David Berneda   }
{**********************************************}
{$I TeeDefs.inc}
unit OHLChart;

interface

Uses Classes, Graphics, Chart, Series, Teengine;

{ WARNING: NOTE FOR TeeChart Pro 4 users.

  The logic in OHLC series has been changed.
  Now the default "Y" values correspond to the Close values,
  before they were referred to Open values.
  This change should be transparent to your code unless you're
  accessing directly the "Y" values.
}

{ Used in financial applications, OHLC stands for Open,High,Low & Close.
  These are the prices a particular financial product has in a given time
  period.

  TOHLCSeries extends the basic TCustomSeries adding new lists for
  High, Low & Open prices, and preserving the default Y values for Close
  prices.

  Overrides the basic list functions (Add, Clear & Delete) plus the
  FillSampleValues method, used in design mode to show some fictional
  values to the user.

  Publishes the High, Low & Open values lists and "overrides" the XValues
  property to be DateValues and the YValues to be CloseValues.
}
Type TOHLCSeries=class(TCustomSeries)
     private { assumed YValues = CloseValues }
       FHighValues  : TChartValueList;
       FLowValues   : TChartValueList;
       FOpenValues  : TChartValueList;

       Function GetCloseValues:TChartValueList;
       Function GetDateValues:TChartValueList;
       Procedure SetCloseValues(Value:TChartValueList);
       Procedure SetDateValues(Value:TChartValueList);
       Procedure SetHighValues(Value:TChartValueList);
       Procedure SetLowValues(Value:TChartValueList);
       Procedure SetOpenValues(Value:TChartValueList);
     protected
       Procedure AddSampleValues(NumValues:Integer); override;
     public
       Constructor Create(AOwner: TComponent); override;

       Function AddOHLC( Const ADate:TDateTime;
                         Const AOpen,AHigh,ALow,AClose:Double):Integer; {$IFDEF D5}overload;{$ENDIF}

       {$IFDEF D5}
       Function AddOHLC(Const AOpen,AHigh,ALow,AClose:Double):Integer; overload;
       {$ENDIF}

       Function IsValidSourceOf(Value:TChartSeries):Boolean; override;
       Function MaxYValue:Double; override;
       Function MinYValue:Double; override;
       Function NumSampleValues:Integer; override;
     published
       property CloseValues:TChartValueList read GetCloseValues write SetCloseValues;
       property DateValues:TChartValueList read GetDateValues write SetDateValues;
       property HighValues:TChartValueList read FHighValues write SetHighValues;
       property LowValues:TChartValueList read FLowValues write SetLowValues;
       property OpenValues:TChartValueList read FOpenValues write SetOpenValues;
     end;

{ returns random values for Open,Close,High and Low prices. Used for demos }
Procedure GetRandomOHLC(Var AOpen,AClose,AHigh,ALow:Double; Const YRange:Double);

implementation

Uses SysUtils, TeCanvas ,TeeProco;

Constructor TOHLCSeries.Create(AOwner: TComponent);
Begin
  inherited;
  XValues.DateTime:=True;
  XValues.Name:=TeeMsg_ValuesDate;
  YValues.Name:=TeeMsg_ValuesClose;
  FHighValues :=TChartValueList.Create(Self,TeeMsg_ValuesHigh);
  FLowValues  :=TChartValueList.Create(Self,TeeMsg_ValuesLow);
  FOpenValues :=TChartValueList.Create(Self,TeeMsg_ValuesOpen);
end;

Function TOHLCSeries.GetDateValues:TChartValueList;
Begin
  result:=XValues; { overrides the default XValues }
end;

Procedure TOHLCSeries.SetDateValues(Value:TChartValueList);
begin
  SetXValues(Value); { overrides the default XValues }
end;

Function TOHLCSeries.GetCloseValues:TChartValueList;
Begin
  result:=YValues; { overrides the default YValues }
end;

Procedure TOHLCSeries.SetCloseValues(Value:TChartValueList);
begin
  SetYValues(Value); { overrides the default YValues }
end;

Procedure TOHLCSeries.SetHighValues(Value:TChartValueList);
Begin
  SetChartValueList(FHighValues,Value); { standard method }
end;

Procedure TOHLCSeries.SetLowValues(Value:TChartValueList);
Begin
  SetChartValueList(FLowValues,Value); { standard method }
end;

Procedure TOHLCSeries.SetOpenValues(Value:TChartValueList);
Begin
  SetChartValueList(FOpenValues,Value); { standard method }
end;

Function TOHLCSeries.AddOHLC( Const ADate:TDateTime;
                              Const AOpen,AHigh,ALow,AClose:Double):Integer;
Begin
  HighValues.TempValue:=AHigh;
  LowValues.TempValue:=ALow;
  OpenValues.TempValue:=AOpen;
  result:=AddXY(ADate,AClose{$IFNDEF D4},'', clTeeColor{$ENDIF}); { standard add X,Y }
end;

{$IFDEF D5}
Function TOHLCSeries.AddOHLC(Const AOpen,AHigh,ALow,AClose:Double):Integer;
begin
  DateValues.DateTime:=False;
  HighValues.TempValue:=AHigh;
  LowValues.TempValue:=ALow;
  OpenValues.TempValue:=AOpen;
  result:=Add(AClose{$IFNDEF D4},'', clTeeColor{$ENDIF}); { standard Add }
end;
{$ENDIF}

Function TOHLCSeries.MaxYValue:Double;
Begin
  result:=MaxDouble(CloseValues.MaxValue,HighValues.MaxValue);
  result:=MaxDouble(result,LowValues.MaxValue);
  result:=MaxDouble(result,OpenValues.MaxValue);
End;

Function TOHLCSeries.MinYValue:Double;
Begin
  result:=MinDouble(CloseValues.MinValue,HighValues.MinValue);
  result:=MinDouble(result,LowValues.MinValue);
  result:=MinDouble(result,OpenValues.MinValue);
End;

Procedure GetRandomOHLC(Var AOpen,AClose,AHigh,ALow:Double; Const YRange:Double);
var tmpY     : Integer;
    tmpFixed : Double;
Begin
  tmpY:=Abs(Round(YRange/400.0));
  AClose:=AOpen+Random(Round(YRange/25.0))-(YRange/50.0); { imagine a close price... }
  { and imagine the high and low session price }
  tmpFixed:=3*Round(Abs(AClose-AOpen)/10.0);
  if AClose>AOpen then
  Begin
    AHigh:=AClose+tmpFixed+Random(tmpY);
    ALow:=AOpen-tmpFixed-Random(tmpY);
  end
  else
  begin
    AHigh:=AOpen+tmpFixed+Random(tmpY);
    ALow:=AClose-tmpFixed-Random(tmpY);
  end;
end;

Procedure TOHLCSeries.AddSampleValues(NumValues:Integer);
Var AOpen  : Double;
    AHigh  : Double;
    ALow   : Double;
    AClose : Double;
    t      : Integer;
Begin
  With RandomBounds(NumValues) do
  begin
    AOpen:=MinY+Random(Round(DifY)); { starting open price }
    for t:=1 to NumValues do
    Begin
      { Generate random figures }
      GetRandomOHLC(AOpen,AClose,AHigh,ALow,DifY);
      { call the standard add method }
      AddOHLC(tmpX,AOpen,AHigh,ALow,AClose);
      tmpX:=tmpX+StepX;  { <-- next point X value }
      { tomorrow, the market will open at today's close plus/minus something }
      AOpen:=AClose+Random(10)-5;
    end;
  end;
end;

Function TOHLCSeries.NumSampleValues:Integer;
begin
  result:=40;
end;

Function TOHLCSeries.IsValidSourceOf(Value:TChartSeries):Boolean;
begin
  result:=Value is TOHLCSeries;
end;

end.
