{********************************************}
{     TeeChart Pro Charting Library          }
{ Copyright (c) 1995-2000 by David Berneda   }
{         All Rights Reserved                }
{********************************************}
unit DBEditCh;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DB, TeeEdiSeri, StdCtrls, ComCtrls, TeEngine, TeeDBEdit, ExtCtrls,
  TeeSourceEdit;

Const MaxValueSources=16;

type
  TDBChartEditor = class(TBaseDBChartEditor)
    GroupFields: TScrollBox;
    LLabels: TLabel;
    CBLabelsField: TComboBox;
    procedure CBLabelsFieldChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BApplyClick(Sender: TObject);
    procedure CBSourcesChange(Sender: TObject);
  private
    { Private declarations }
    LabelValues : Array[0..MaxValueSources-1] of TLabel;
    CBDateTime  : Array[0..MaxValueSources-1] of TCheckBox;
    procedure CBValuesChange(Sender: TObject);
    Procedure SetFields;
    Procedure SetTextItemIndex(Combo:TComboBox);
  protected
    Function IsValid(AComponent:TComponent):Boolean; override;
  public
    { Public declarations }
    CBValues    : Array[0..MaxValueSources-1] of TComboBox;
  end;

Var InternalOnGetDesignerNames : TOnGetDesignerNamesEvent=nil;

implementation

{$R *.DFM}

uses TeeConst, Chart, DBChart, TeeProcs, TeeDBSumEdit, TeeDBSourceEditor;

Function OnDBChartGetSourceStyle(ASeries:TChartSeries):TFormClass;
begin
  result:=nil;
  if Assigned(ASeries.DataSource) then
     if ASeries.DataSource is TDataSet then
        if Copy(ASeries.MandatoryValueList.ValueSource,1,1)='#' then
           result:=TDBChartSumEditor
        else
           result:=TDBChartEditor
     else
     if ASeries.DataSource is TDataSource then
        result:=TDBChartSourceEditor;
end;

Procedure OnCreateEditDBChart(AForm:TFormTeeSeries; AChart:TCustomChart);

  Procedure AddDataStyle(Const AStyle:String; AFormClass:TFormClass);
  begin
    With AForm.CBDataSourceStyle.Items do
         if IndexOf(AStyle)=-1 then AddObject(AStyle,TObject(AFormClass));
  end;

  Procedure RemoveDataStyle(Const AStyle:String);
  var tmp : Integer;
  begin
    With AForm.CBDataSourceStyle.Items do
    begin
      tmp:=IndexOf(AStyle);
      if tmp<>-1 then Delete(tmp);
    end;
  end;

begin
  With AForm do
  if AChart is TCustomDBChart then
  begin
    AddDataStyle(TeeMsg_Dataset,TDBChartEditor);
    AddDataStyle(TeeMsg_SingleRecord,TDBChartSourceEditor);
    AddDataStyle(TeeMsg_Summary,TDBChartSumEditor);
    OnChartGetSourceStyle:=OnDBChartGetSourceStyle;
    if Assigned(InternalOnGetDesignerNames) then
       OnGetDesignerNames:=InternalOnGetDesignerNames;
  end
  else
  begin
    RemoveDataStyle(TeeMsg_DataSet);
    RemoveDataStyle(TeeMsg_SingleRecord);
    RemoveDataStyle(TeeMsg_Summary);
    OnChartGetSourceStyle:=nil;
    OnGetDesignerNames:=nil;
  end;
end;

Function TDBChartEditor.IsValid(AComponent:TComponent):Boolean;
begin
  result:=AComponent is TDataSet;
end;

procedure TDBChartEditor.CBLabelsFieldChange(Sender: TObject);
begin
  BApply.Enabled:=True;
end;

procedure TDBChartEditor.CBValuesChange(Sender: TObject);
var tmpField : TField;
begin
  With TComboBox(Sender) do
  if ItemIndex<>-1 then
  begin
    tmpField:=DataSet.FindField(Items[ItemIndex]);
    CBDateTime[Tag].Checked:=Assigned(tmpField) and (TeeFieldType(tmpField.DataType)=tftDateTime);
  end;
  BApply.Enabled:=True;
end;

Procedure TDBChartEditor.SetTextItemIndex(Combo:TComboBox);
var tmp : Integer;
begin
  With Combo do
  begin
    tmp:=Items.IndexOf(Text);
    if tmp<>ItemIndex then ItemIndex:=tmp;
  end;
end;

procedure TDBChartEditor.FormShow(Sender: TObject);
var t       : Integer;
    tmpName : String;
begin
  inherited;
  TheSeries:=TChartSeries(Tag);
  With TheSeries do
  for t:=0 to ValuesList.Count-1 do
  begin
    tmpName:=ValuesList[t].Name;
    CBValues[t]:=TComboBox.Create(Self);
    With CBValues[t] do
    begin
      Parent:=GroupFields;
      Left:=CBLabelsField.Left;
      Style:=csDropDown;
      HelpContext:=178;
      Width:=CBLabelsField.Width;
      Top:=2+CBLabelsField.Top+CBLabelsField.Height+((CBValues[t].Height+4)*t+1);
      OnChange:=CBValuesChange;
      Tag:=t;
      Visible:=tmpName<>'';
    end;
    LabelValues[t]:=TLabel.Create(Self);
    With LabelValues[t] do
    begin
      Alignment:=taRightJustify;
      Parent:=GroupFields;
      Top:=CBValues[t].Top+4;
      AutoSize:=False;
      Left:=LLabels.Left;
      Width:=LLabels.Width;
      Caption:=tmpName+':';
      Visible:=tmpName<>'';
    end;
    CBDateTime[t]:=TCheckBox.Create(Self);
    With CBDateTime[t] do
    begin
      Parent:=GroupFields;
      Left:=CBLabelsField.Left+CBLabelsField.Width+6;
      Top:=CBValues[t].Top;
      HelpContext:=178;
      Caption:=TeeMsg_DateTime;
      Width:=Canvas.TextWidth(Caption + 'www');
      Tag:=t;
      Visible:=tmpName<>'';
      OnClick:=CBLabelsFieldChange;
    end;
  end;
  SetFields;
end;

Procedure TDBChartEditor.SetFields;

  Procedure FillFields;

    Procedure AddField(Const tmpSt:String; tmpType:TFieldType);
    Var t : Integer;
    begin
      Case TeeFieldType(tmpType) of
      tftNumber,
     tftDateTime: begin
                    CBLabelsField.Items.Add(tmpSt);
                    for t:=0 to TheSeries.ValuesList.Count-1 do
                        CBValues[t].Items.Add(tmpSt);
                  end;
         tftText: CBLabelsField.Items.Add(tmpSt);
      end;
    end;

  Var t : Integer;
  Begin
    With DataSet do
    if FieldCount>0 then
       for t:=0 to FieldCount-1 do
           AddField(Fields[t].FieldName,Fields[t].DataType)
    else
    begin
      FieldDefs.Update;
      for t:=0 to FieldDefs.Count-1 do
          AddField(FieldDefs[t].Name,FieldDefs[t].DataType);
    end;
  end;

var t : Integer;
begin
  CBLabelsField.Clear;
  CBLabelsField.Enabled:=CBSources.ItemIndex<>-1;
  for t:=0 to TheSeries.ValuesList.Count-1 do
  With CBValues[t] do
  begin
    Clear;
    Enabled:=CBLabelsField.Enabled;
  end;
  if CBSources.ItemIndex<>-1 then FillFields;
  With CBLabelsField do ItemIndex:=Items.IndexOf(TheSeries.XLabelsSource);
  With TheSeries.ValuesList do
  for t:=0 to Count-1 do
  begin
    With CBValues[t] do ItemIndex:=Items.IndexOf(ValueList[t].ValueSource);
    CBDateTime[t].Checked:=ValueList[t].DateTime;
  end;
end;

procedure TDBChartEditor.BApplyClick(Sender: TObject);

  Procedure CheckFieldIsBlank(Const AFieldName:String);
  begin
    if AFieldName<>'' then
       Raise ChartException.CreateFmt(TeeMsg_FieldNotFound,[AFieldName]);
  end;

  Procedure CheckValidFields;
  var t : Integer;
  begin
    for t:=0 to TheSeries.ValuesList.Count-1 do
    With CBValues[t] do
    begin
      SetTextItemIndex(CBValues[t]);
      if ItemIndex=-1 then CheckFieldIsBlank(Text);
    end;
    SetTextItemIndex(CBLabelsField);
    With CBLabelsField do
    if ItemIndex=-1 then CheckFieldIsBlank(Text);
  end;

var t : Integer;
begin
  inherited;
  With TheSeries do
  begin
    DataSource:=nil;
    try
      CheckValidFields;
      for t:=0 to ValuesList.Count-1 do
      With ValuesList[t] do
      begin
        ValueSource:=CBValues[t].Text;
        DateTime:=CBDateTime[t].Checked;
      end;
      XLabelsSource:=CBLabelsField.Text;
    finally
      DataSource:=DataSet;
    end;
  end;
  BApply.Enabled:=False;
end;

procedure TDBChartEditor.CBSourcesChange(Sender: TObject);
begin
  inherited;
  SetFields;
end;

{$IFNDEF TEEOCX}
initialization
  InternalOnCreateEditSeries:=OnCreateEditDBChart;
finalization
  InternalOnCreateEditSeries:=nil;
{$ENDIF}
end.
