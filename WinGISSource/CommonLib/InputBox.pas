Unit InputBox;

Interface

Uses Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,Validate,
  StdCtrls, WCtrls, ExtCtrls, MultiLng;

Type TInputBoxDialog = Class(TWForm)
        Bevel2: TBevel;
        CancelBtn: TButton;
        Edit: TEdit;
        Label1: TWLabel;              
        MlgSection: TMlgSection;
        OkBtn: TButton;
       Private
       Public
     end;

     TInputBox       = Class(TComponent)
      Private
       FCaption      : String;
       FLabel        : String;
       FText         : String;
       FValidator    : TCustomValidator;
      Protected
       Procedure   Notification(AComponent:TComponent;Operation:TOperation); override;
      Public
       Function    Execute:Boolean;
      Published
       Property    Caption:String read FCaption write FCaption;
       Property    LabelText:String read FLabel write FLabel;
       Property    Text:String read FText write FText;
       Property    Validator:TCustomValidator read FValidator write FValidator;
     end;

Implementation

{$R *.DFM}

Function TInputBox.Execute:Boolean;
var Dialog         : TInputBoxDialog;
begin
  Dialog:=TInputBoxDialog.Create(Self);
  try
    Dialog.Caption:=FCaption;
    Dialog.Label1.Caption:=FLabel;
    if FValidator<>NIL then begin
      FValidator.Edit:=Dialog.Edit;
      FValidator.AsText:=FText;
    end;
    if Dialog.ShowModal=mrOK then begin
      if FValidator<>NIL then begin
        FText:=FValidator.AsText;
        FValidator.Edit:=NIL;
      end;
      Result:=TRUE;
    end
    else Result:=FALSE;
  finally
    Dialog.Free;
  end;
end;

Procedure TInputBox.Notification(AComponent:TComponent;Operation:TOperation);
begin
  inherited Notification(AComponent,Operation);
  if (Operation=opRemove) and (AComponent=FValidator) then FValidator:=NIL;
end;

end.
