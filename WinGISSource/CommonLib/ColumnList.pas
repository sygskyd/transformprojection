Unit ColumnList;

Interface

Uses Classes,Controls,Graphics,StdCtrls,TreeView,WCtrls,Windows;

Type TColumn       = Class(TCollectionItem)
      Private
       FAutoSize        : Boolean;
       FBounds          : TRect;
       FCurrentRect     : TRect;
       FItemIndex       : Integer;
       FLineBreak       : Boolean;
       FHint            : String;
       FMinWidth        : Integer;
       FOnMouseDown     : TMouseEvent;
       FOnMouseMove     : TMouseMoveEvent;
       FOnMouseUp       : TMouseEvent;
       FOnPaint         : TDrawItemEvent;
       FSeparator       : Boolean;
       FVisible         : Boolean;
       FWidth           : Integer;
       Procedure   SetVisible(const Value: Boolean);
      Protected
       Procedure   MouseDown(Button:TMouseButton;Shift:TShiftState;X,Y:Integer); virtual;
       Procedure   MouseMove(Shift:TShiftState;X,Y:Integer); virtual;
       Procedure   MouseUp(Button:TMouseButton;Shift:TShiftState;X,Y:Integer); virtual;
       Procedure   Paint(Control:TWinControl;State:TOwnerDrawState);
       Procedure   SetItem(Index,Y:Integer);
      Public
       Constructor Create(ACollection:TCollection); override;
       Property    Bounds:TRect read FBounds write FBounds;
       Property    ItemIndex:Integer read FItemIndex;
      Published
       Property    AutoSize:Boolean read FAutoSize write FAutoSize default False;
       Property    Separator:Boolean read FSeparator write FSeparator default False;
       Property    LineBreak:Boolean read FLineBreak write FLineBreak;
       // hint shown when the mouse is in the colums
       Property    Hint:String read FHint write FHint;
       Property    MinWidth:Integer read FMinWidth write FMinWidth default 0;
       Property    OnMouseDown:TMouseEvent read FOnMouseDown write FOnMouseDown;
       Property    OnMouseMove:TMouseMoveEvent read FOnMouseMove write FOnMouseMove;
       Property    OnMouseUp:TMouseEvent read FOnMouseUp write FOnMouseUp;
       Property    OnPaint:TDrawItemEvent read FOnPaint write FOnPaint;
       Property    Visible:Boolean read FVisible write SetVisible default True;
       Property    Width:Integer read FWidth write FWidth;
     end;

     TColumnListbox = Class;

     TColumns      = Class(TCollection)
      Private
       FListbox    : TWCustomListbox;
       FComboBox   : TWCustomComboBox;
       Function    ColumnAtPosition(X,Y:Integer):TColumn;
       Function    GetColumn(AIndex:Integer):TColumn;
       Function    GetItemHeight:Integer;
       Function    ReAlignColumns(NewWidth:Integer):Integer;
      Protected
       Procedure   ChangeScale(M,D:Integer);
       Function    GetOwner:TPersistent; override;
      Public
       Constructor Create(AOwner:TWCustomListbox); overload;
       Constructor Create(AOwner:TWCustomCombobox); overload;
       Property    Columns[AIndex:Integer]:TColumn read GetColumn; default;
     end;

     TColumnListbox     = Class(TWCustomListbox)
      Private
       FColumns         : TColumns;
       FLastColumn      : TColumn;
       FSaveBrush       : TBrush;
       FSavePen         : TPen;
       FSaveFont        : TFont;
      Protected
       Procedure   ChangeScale(M,D:Integer); override;
       Procedure   DrawItem(Index:Integer;Rect:TRect;State:TOwnerDrawState); override;
       Procedure   Loaded; override;
       Procedure   MouseDown(Button:TMouseButton;Shift:TShiftState;X,Y:Integer); override;
       Procedure   MouseMove(Shift:TShiftState;X,Y:Integer); override;
       Procedure   MouseUp(Button:TMouseButton;Shift:TShiftState;X,Y:Integer); override;
       Procedure   Resize; override;
      Public
       Constructor Create(AOwner:TComponent); override;
       Destructor  Destroy; override;
       Function    ColumnAtPosition(var X,Y:Integer;var Item:Integer;Existing:Boolean=FALSE):TColumn;
      Published
       Property    Align;
       Property    Anchors;
       Property    BorderStyle;
       Property    Color;
       Property    Columns:TColumns read FColumns write FColumns;
       Property    Ctl3D;
       Property    DragCursor;
       Property    DragMode;
       Property    Enabled;
       Property    ExtendedSelect;
       Property    Font;
       Property    ImeMode;
       Property    ImeName;
       Property    IntegralHeight;
       Property    ItemHeight;
       Property    MultiSelect;
       Property    ParentColor;
       Property    ParentCtl3D;
       Property    ParentFont;
       Property    ParentShowHint;
       Property    PopupMenu;
       Property    Scrollbars;
       Property    ShowHint;
       Property    TabOrder;
       Property    TabStop;
       Property    TabWidth;
       Property    Visible;
       Property    OnClick;
       Property    OnDblClick;
       Property    OnDragDrop;
       Property    OnDragOver;
       Property    OnDrawItem;
       Property    OnEndDrag;
       Property    OnEnter;
       Property    OnExit;
       Property    OnKeyDown;
       Property    OnKeyPress;
       Property    OnKeyUp;
       Property    OnMeasureItem;
       Property    OnMouseDown;
       Property    OnMouseMove;
       Property    OnMouseUp;
       Property    OnPaint;
       Property    OnStartDrag;
     end;

     TColumnTreeListView = Class(TTreeListView)
      Private
       FColumns    : TColumns;
      Protected
       Procedure   ChangeScale(M,D:Integer); override;
      Public
       Constructor Create(AOwner:TComponent); override;
       Destructor  Destroy; override;
      Published 
       Property    Columns:TColumns read FColumns write FColumns;
     end;

     TColumnComboBox = Class(TWCustomComboBox)
      Private
       FColumns    : TColumns;
      Public
       Constructor Create(AOwner:TComponent); override;
       Destructor  Destroy; override;
      Protected
       Procedure   ChangeScale(M,D:Integer); override;
       Procedure   DrawItem(Index:Integer;Rect:TRect;State:TOwnerDrawState); override;
       Procedure   Loaded; override;
       Procedure   Resize; override;
      Published
       Property    Align;
       Property    Anchors;
       Property    Color;
       Property    Columns:TColumns read FColumns write FColumns;
       Property    Ctl3D;
       Property    DragCursor;
       Property    DragMode;
       Property    Enabled;
       Property    Font;
       Property    ImeMode;
       Property    ImeName;
       Property    ItemHeight;
       Property    ParentColor;
       Property    ParentCtl3D;
       Property    ParentFont;
       Property    ParentShowHint;
       Property    PopupMenu;
       Property    ShowHint;
       Property    TabOrder;
       Property    TabStop;
       Property    Visible;
       Property    OnClick;
       Property    OnDblClick;
       Property    OnDragDrop;
       Property    OnDragOver;
       Property    OnDrawItem;
       Property    OnEndDrag;
       Property    OnEnter;
       Property    OnExit;
       Property    OnKeyDown;
       Property    OnKeyPress;
       Property    OnKeyUp;
       Property    OnMeasureItem;
       Property    OnMouseDown;
       Property    OnMouseMove;
       Property    OnMouseUp;
       Property    OnStartDrag;
     end;

Function NewColumn(ACollection:TCollection;AWidth:Integer;AMinWidth:Integer=0;
    AAutoSize:Boolean=FALSE;ASeparator:Boolean=FALSE;ALineBreak:Boolean=FALSE;
    Const AHint:String=''):TColumn;
    
Implementation

Uses Forms;

{==============================================================================+
  TColumn
+==============================================================================}

Constructor TColumn.Create(ACollection:TCollection);
begin
  inherited Create(ACollection);
  FVisible:=TRUE;
end;

Procedure TColumn.MouseDown(Button:TMouseButton;Shift:TShiftState;X,Y:Integer);
begin
  if Assigned(FOnMouseDown) then OnMouseDown(Self,Button,Shift,X,Y);
end;

Procedure TColumn.MouseMove(Shift:TShiftState;X,Y:Integer);
begin
  if Assigned(FOnMouseMove) then OnMouseMove(Self,Shift,X,Y);
end;

Procedure TColumn.MouseUp(Button:TMouseButton;Shift:TShiftState;X,Y:Integer);
begin
  if Assigned(FOnMouseUp) then OnMouseUp(Self,Button,Shift,X,Y);
end;

Procedure TColumn.SetItem(Index:Integer;Y:Integer);
begin
  FItemIndex:=Index;
  FBounds:=Rect(FCurrentRect.Left,Y,FCurrentRect.Right,Y+FCurrentRect.Bottom);
end;

Procedure TColumn.Paint(Control:TWinControl;State:TOwnerDrawState);
var Rect           : TRect;
begin
  if Assigned(FOnPaint) then begin
    Rect:=FBounds;
    if FSeparator then Dec(Rect.Right);
    OnPaint(Control,ItemIndex,Rect,State);
  end;  
end;

{==============================================================================+
  TColumns
+==============================================================================}

Constructor TColumns.Create(AOwner:TWCustomListbox);
begin
  inherited Create(TColumn);
  FListbox:=AOwner;
end;

Constructor TColumns.Create(AOwner:TWCustomComboBox);
begin
  inherited Create(TColumn);
  FComboBox:=AOwner;
end;

Function TColumns.GetOwner:TPersistent;
begin
  if FListbox<>NIL then Result:=FListbox
  else Result:=FComboBox;
end;

Function TColumns.GetItemHeight:Integer;
begin
  if FListbox<>NIL then Result:=FListbox.ItemHeight
  else if FCombobox<>NIL then Result:=FComboBox.ItemHeight
  else Result:=0;
end;

Function TColumns.GetColumn(AIndex:Integer):TColumn;
begin
  Result:=TColumn(Items[AIndex]);
end;
 
Function TColumns.ColumnAtPosition(X,Y:Integer):TColumn;
var Cnt            : Integer;
begin
  for Cnt:=0 to Count-1 do if PtInRect(TColumn(Items[Cnt]).FCurrentRect,
      Point(X,Y)) then begin
    Result:=TColumn(Items[Cnt]);
    Exit;
  end;
  Result:=NIL;
end;

Function TColumns.ReAlignColumns(NewWidth:Integer):Integer;
Const MAX_LINES    = 5;
var CurrentX       : Integer;
    Cnt            : Integer;
    Cnt1           : Integer;
    Column         : TColumn;
    AutoColumn     : TColumn;
    AutoSize       : Array[0..MAX_LINES] of Integer;
    LineBreaks     : Array[0..MAX_LINES] of Integer;
    LineStart      : Integer;
  Procedure BreakLine;
  begin
    LineBreaks[Result]:=Cnt;
    CurrentX:=0;
    Inc(Result);
    LineBreaks[Result]:=Count-1;
    AutoSize[Result]:=-1;
  end;
begin
  Result:=0;
  CurrentX:=0;
  AutoSize[0]:=-1;
  LineBreaks[0]:=Count-1;
  // calculate line-layout
  for Cnt:=0 to Count-1 do begin
    Column:=TColumn(Items[Cnt]);
    if Column.Visible then begin
      // column has line-break property set, create a new line
      if Column.LineBreak then BreakLine;
      // column has autosize-property, store index
      if Column.AutoSize then AutoSize[Result]:=Cnt;
      // increment current x-xoordinate
      Inc(CurrentX,Column.Width);
      // if column does not fit and line contains an auto-break field
      // set size of auto-size field to min-width
      if (CurrentX>NewWidth) and (AutoSize[Result]<>-1) then begin
        AutoColumn:=Columns[AutoSize[Result]];
        CurrentX:=CurrentX+AutoColumn.MinWidth-AutoColumn.Width;
      end;
      // if column does not fit create a new line
      if CurrentX>NewWidth then begin
        BreakLine;
        CurrentX:=Column.Width;
      end;
    end
    else Column.FCurrentRect:=Rect(-Column.Width,0,0,0);  
  end;
  // calculate positions of fields
  LineStart:=0;
  for Cnt:=0 to Result do begin
    if AutoSize[Cnt]>-1 then begin
      CurrentX:=0;
      for Cnt1:=LineStart to AutoSize[Cnt]-1 do begin
        if not Columns[Cnt1].Visible then Continue;
        Columns[Cnt1].FCurrentRect:=Bounds(CurrentX,Cnt*GetItemHeight,
            Columns[Cnt1].Width,GetItemHeight);
        Inc(CurrentX,Columns[Cnt1].Width);
      end;
      Columns[AutoSize[Cnt]].FCurrentRect.Left:=CurrentX;
      CurrentX:=NewWidth;
      for Cnt1:=LineBreaks[Cnt] downto AutoSize[Cnt]+1 do begin
        if not Columns[Cnt1].Visible then Continue;
        Dec(CurrentX,Columns[Cnt1].Width);
        Columns[Cnt1].FCurrentRect:=Bounds(CurrentX,Cnt*GetItemHeight,
            Columns[Cnt1].Width,GetItemHeight);
      end;
      Columns[AutoSize[Cnt]].FCurrentRect:=Rect(Columns[AutoSize[Cnt]].FCurrentRect.Left,
          Cnt*GetItemHeight,CurrentX,(Cnt+1)*GetItemHeight);
    end
    else begin
      CurrentX:=0;
      for Cnt1:=LineStart to LineBreaks[Cnt] do begin
        if not Columns[Cnt1].Visible then Continue;
        Columns[Cnt1].FCurrentRect:=Bounds(CurrentX,Cnt*GetItemHeight,
            Columns[Cnt1].Width,GetItemHeight);     
        Inc(CurrentX,Columns[Cnt1].Width);
      end;
    end;
    LineStart:=LineBreaks[Cnt];         
  end;
  Inc(Result);  
end;

{==============================================================================+
  TColumnListbox
+==============================================================================}

Constructor TColumnListbox.Create(AOwner:TComponent);
begin
  inherited Create(AOwner);
  FColumns:=TColumns.Create(Self);     
  Style:=lbOwnerDrawFixed;
  FSavePen:=TPen.Create;
  FSaveBrush:=TBrush.Create;
  FSaveFont:=TFont.Create;
end;

Destructor TColumnListbox.Destroy;
begin
  FColumns.Free;
  FSavePen.Free;
  FSaveBrush.Free;
  FSaveFont.Free;
  inherited Destroy;
end;

Function TColumnListbox.ColumnAtPosition(var X,Y:Integer;var Item:Integer;
    Existing:Boolean):TColumn;
begin
  // search for a column and call it's mous-handlers
  Item:=ItemAtPos(Point(X,Y),Existing);
  // convert y-position to column-coordinates
  if Item<0 then Result:=NIL
  else begin
    Y:=Y-(Item-TopIndex)*ItemHeight;
    Result:=FColumns.ColumnAtPosition(X,Y);
  end;  
end;

Procedure TColumnListbox.MouseDown(Button:TMouseButton;Shift:TShiftState;X,Y:Integer);
var Column         : TColumn;
    Item           : Integer;
begin
  inherited MouseDown(Button,Shift,X,Y);
  Column:=ColumnAtPosition(X,Y,Item);
  if Column<>NIL then begin
    Column.SetItem(Item,(Item-TopIndex)*ItemHeight);
    Column.MouseDown(Button,Shift,X,Y);
  end;  
end;

Procedure TColumnListbox.MouseMove(Shift:TShiftState;X,Y:Integer);
var Column         : TColumn;
    Item           : Integer;
begin
  inherited MouseMove(Shift,X,Y);
  Column:=ColumnAtPosition(X,Y,Item);
  if Column<>NIL then begin
    Column.SetItem(Item,(Item-TopIndex)*ItemHeight);
    Column.MouseMove(Shift,X,Y);
  end;
  if Column<>FLastColumn then begin
    Application.CancelHint;
    FLastColumn:=Column;
    if Column<>NIL then Hint:=Column.Hint;
  end;
end;

Procedure TColumnListbox.MouseUp(Button:TMouseButton;Shift:TShiftState;X,Y:Integer);
var Column         : TColumn;
    Item           : Integer;
begin
  inherited MouseUp(Button,Shift,X,Y);
  Column:=ColumnAtPosition(X,Y,Item);
  if Column<>NIL then begin
    Column.SetItem(Item,(Item-TopIndex)*ItemHeight);
    Column.MouseUp(Button,Shift,X,Y);
  end;  
end;

Procedure TColumnListbox.DrawItem(Index:Integer;Rect:TRect;State:TOwnerDrawState);
var Column         : TColumn;
    Cnt            : Integer;
begin
  FSavePen.Assign(Canvas.Pen);
  FSaveBrush.Assign(Canvas.Brush);
  FSaveFont.Assign(Canvas.Font);
  for Cnt:=0 to FColumns.Count-1 do begin
    Column:=TColumn(FColumns.Items[Cnt]);
    Column.SetItem(Index,(Index-TopIndex)*ItemHeight);
    Column.Paint(Self,State);
    if Column.Separator then begin
      Canvas.Pen.Color:=clBtnFace;
      Canvas.Pen.Style:=psSolid;
      Canvas.MoveTo(Column.Bounds.Right-1,Column.Bounds.Top);
      Canvas.LineTo(Column.Bounds.Right-1,Column.Bounds.Bottom);
    end;
    Canvas.Pen:=FSavePen;
    Canvas.Brush:=FSaveBrush;
    Canvas.Font:=FSaveFont;
  end;
end;

Procedure TColumnListbox.Loaded;
begin
  inherited Loaded;
  FColumns.ReAlignColumns(ClientWidth);
end;

Procedure TColumnListbox.Resize;
begin
  inherited Resize;
  FColumns.ReAlignColumns(ClientWidth);
end;

{ TColumnTreeListView }

procedure TColumnTreeListView.ChangeScale(M, D: Integer);
begin
  inherited ChangeScale(M,D);
  FColumns.ChangeScale(M,D);
  FColumns.RealignColumns(ClientWidth);
end;

Constructor TColumnTreeListView.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FColumns:=TColumns.Create(Self);
end;

Destructor TColumnTreeListView.Destroy;
begin
  FColumns.Free;
  inherited Destroy;
end;

{==============================================================================+
  TColumnComboBox
+==============================================================================}

procedure TColumnComboBox.ChangeScale(M, D: Integer);
begin
  inherited ChangeScale(M,D);
  FColumns.ChangeScale(M,D);
  FColumns.RealignColumns(ClientWidth);
end;

Constructor TColumnComboBox.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FColumns:=TColumns.Create(Self);
  Style:=csOwnerDrawFixed;
end;

Destructor TColumnComboBox.Destroy;
begin
  FColumns.Free;
  inherited Destroy;
end;

Procedure TColumnCombobox.DrawItem(Index:Integer;Rect:TRect;State:TOwnerDrawState);
var Column         : TColumn;
    Cnt            : Integer;
begin
  for Cnt:=0 to FColumns.Count-1 do begin
    Column:=TColumn(FColumns.Items[Cnt]);
    Column.SetItem(Index,Rect.Top);
    Column.Paint(Self,State);
    if Column.Separator then begin
      Canvas.Pen.Color:=clBtnFace;
      Canvas.Pen.Style:=psSolid;
      Canvas.MoveTo(Column.Bounds.Right-1,Column.Bounds.Top);
      Canvas.LineTo(Column.Bounds.Right-1,Column.Bounds.Bottom);
    end;
  end;
end;

Procedure TColumnCombobox.Loaded;
begin
  inherited Loaded;
  FColumns.ReAlignColumns(ClientWidth);
end;

Procedure TColumnCombobox.Resize;
begin
  inherited Resize;
  FColumns.ReAlignColumns(ClientWidth);
end;

{==============================================================================+
  Functions
+==============================================================================}

Function NewColumn(ACollection:TCollection;AWidth,AMinWidth:Integer;AAutoSize,
    ASeparator,ALineBreak:Boolean;Const AHint:String):TColumn;
begin
  Result:=TColumn.Create(ACollection);
  with Result do begin
    Width:=AWidth;
    MinWidth:=AMinWidth;
    AutoSize:=AAutoSize;
    Separator:=ASeparator;
    LineBreak:=ALineBreak;
    Hint:=AHint;
  end;
end;

procedure TColumn.SetVisible(const Value: Boolean);
begin
  if FVisible<>Value then begin
    FVisible:=Value;
    with TColumns(Collection) do begin
      if FListBox<>NIL then RealignColumns(FListbox.ClientWidth)
      else if FComboBox<>NIL then RealignColumns(FComboBox.ClientWidth);
    end;  
  end;
end;

procedure TColumnListbox.ChangeScale(M, D: Integer);
begin
  inherited ChangeScale(M,D);
  FColumns.ChangeScale(M,D);
  FColumns.RealignColumns(ClientWidth);
end;

procedure TColumns.ChangeScale(M, D: Integer);
var Column         : TColumn;
    Cnt            : Integer;
begin
  for Cnt:=0 to Count-1 do begin
    Column:=TColumn(Items[Cnt]);
    Column.Width:=MulDiv(Column.Width,M,D);
    Column.MinWidth:=MulDiv(Column.MinWidth,M,D);
  end;
end;

end.
