
{*******************************************************}
{                                                       }
{       Turbo Pascal Version 7.0                        }
{       Standard Objects Unit                           }
{                                                       }
{       Copyright (c) 1992 Borland International        }
{                                                       }
{*******************************************************}

unit Objects;

{$H-}

interface

const

{ TOldStream access modes }

  stCreate    = $3C00;           { Create new file }
  stOpenRead  = $3D00;           { Read access only }
  stOpenWrite = $3D01;           { Write access only }
  stOpen      = $3D02;           { Read and write access }

{ TOldStream error codes }

  stOk         =  0;              { No error }
  stError      = -1;              { Access error }
  stInitError  = -2;              { Cannot initialize stream }
  stReadError  = -3;              { Read beyond end of stream }
  stWriteError = -4;              { Cannot expand stream }
  stGetError   = -5;              { Get of unregistered object type }
  stPutError   = -6;              { Put of unregistered object type }

{ Maximum TCollection size }

  MaxCollectionSize = MaxLongInt Div SizeOf(Pointer);  

{ TCollection error codes }

  coIndexError = -1;              { Index out of range }
  coOverflow   = -2;              { Overflow }

{ VMT header size }

  vmtHeaderSize = 8;
  
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
const co_Bit0            : Byte = $01;   { Mask for 0-bit    |7|6|5|4|3|2|1|0| }
      co_Bit1            : Byte = $02;   { Mask for 1-bit    |7|6|5|4|3|2|1|0| }
      co_Bit2            : Byte = $04;   { Mask for 2-bit    |7|6|5|4|3|2|1|0| }
      co_Bit3            : Byte = $08;   { Mask for 3-bit    |7|6|5|4|3|2|1|0| }
      co_Bit4            : Byte = $10;   { Mask for 4-bit    |7|6|5|4|3|2|1|0| }
      co_Bit5            : Byte = $20;   { Mask for 5-bit    |7|6|5|4|3|2|1|0| }
      co_Bit6            : Byte = $40;   { Mask for 6-bit    |7|6|5|4|3|2|1|0| }
      co_Bit7            : Byte = $80;   { Mask for 7-bit    |7|6|5|4|3|2|1|0| }

      co_BaseChartSize   : Integer = 1000; { Base Chart Size                   }
      co_HalfChartSize   : Integer = 500 ; { Half of the Base Chart Size       }
      co_RelChartPercent : Integer = 50 ;  { Template Percent                  }
      co_FontWidthPrec   : Integer = 10;   { Chart Font Width Precision        }
      co_TextVisible     : Integer = 3;    { Chart Font Height Text Visibility }
      co_PermisPercent   : Double = 0.01;  { Permissible Percent (coefficient) }
                                           {            of the Size Difference }

      co_DelphiExeName   : String = 'delphi32.exe';

var   ChartStoreVersion  : Byte;           { Chart Kind load-store version     }

  { WinGIS runing state type}
Type TWinGIS_RunState = ( runNotDef, runWinGIS, runComponent);

  function WinGIS_runing: Boolean;         { WinGIS is runing ?                }
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

type

{ Type conversion records }

  WordRec = record
    Lo, Hi: Byte;
  end;

  LongRec = record
    Lo, Hi: Word;
  end;

  PtrRec = record
    Ofs, Seg: Word;
  end;

{ String pointers }

{ Character set type }

  PCharSet = ^TCharSet;
  TCharSet = set of Char;

{ General arrays }

  PByteArray = ^TByteArray;
  TByteArray = array[0..32767] of Byte;

  PWordArray = ^TWordArray;
  TWordArray = array[0..16383] of Word;

{ TObject base object }

  POldObject = ^TOldObject;
  TOldObject = object
    constructor Init;
    procedure Free;
    destructor Done; virtual;
  end;

{ TOldStreamRec }

  PStreamRec = ^TStreamRec;
  TStreamRec = record
    ObjType: Word;
    VmtLink: Pointer;
    Load: Pointer;
    Store: Pointer;
    Next: PStreamRec;
  end;

{ TOldStream }

  POldStream = ^TOldStream;
  TOldStream = object(TOldObject)
    Status: Integer;
    ErrorInfo: Integer;
    constructor Init;
    procedure CopyFrom(var S: TOldStream; Count: Longint);
    procedure Error(Code, Info: Integer); virtual;
    procedure Flush; virtual;
    function Get: POldObject;
    function GetPos: Longint; virtual;
    function GetSize: Longint; virtual;
    procedure Put(P: POldObject);
    procedure Read(var Buf; Count: Integer); virtual;
    function ReadString:String;
    function ReadStr: PString;
    procedure Reset;
    procedure Seek(Pos: Longint); virtual;
    function StrRead: PChar;
    procedure StrWrite(P: PChar);
    procedure Truncate; virtual;
    procedure Write(const Buf; Count: Integer); virtual;
    procedure WriteStr(P: PString);
    procedure WriteString(Const S: String);
  end;

{ DOS file name string }

  FNameStr = PChar;

{ TBufStream }

  PBufStream = ^TBufStream;
  TBufStream = object(TOldStream)
    DataFile    : File;
    Mode        : Integer;
    Buffer      : Pointer;
    BufSize     : Integer;
    BufPtr      : Integer;
    BufEnd      : Integer;
    constructor Init(FileName: FNameStr; AMode, Size: Integer);
    destructor Done; virtual;
    procedure Flush; virtual;
    function GetPos: Longint; virtual;
    function GetSize: Longint; virtual;
    procedure Read(var Buf; Count: Integer); virtual;
    procedure Seek(Pos: Longint); virtual;
    procedure Write(const Buf; Count: Integer); virtual;
  end;

{ TOldMemoryStream }

  POldMemoryStream = ^TOldMemoryStream;
  TOldMemoryStream = object(TOldStream)
  private
    FCapacity: Longint;
//    FMemory: Pointer;
    FSize, FPosition: Longint;
  protected
{++ IDB It was moved from Private declarations into Protected ones by me. Ivanoff.}
    FMemory: Pointer;
{-- IDB}
    procedure SetPointer(Ptr: Pointer; Size: Longint);
    procedure SetCapacity(NewCapacity: Longint);
    function Realloc(var NewCapacity: Longint): Pointer; virtual;
  public
    constructor Init(ALimit: Longint; ABlockSize: Integer);
    destructor Done; virtual;
    function GetPos: Longint; virtual;
    function GetSize: Longint; virtual;
    procedure Read(var Buf; Count: Integer); virtual;
    procedure Seek(Pos: Longint); virtual;
    procedure Truncate; virtual;
    procedure Write(const Buf; Count: Integer); virtual;
  end;
        
{ TCollection types }

  PItemList = ^TItemList;
  TItemList = array[0..MaxCollectionSize - 1] of Pointer;

{ TCollection object }

  PCollection = ^TCollection;
  TCollection = object(TOldObject)
    Items: PItemList;
    Count: Integer;
    Limit: Integer;
    Delta: SmallInt;
    constructor Init(ALimit, ADelta: Integer);
    constructor Load(var S: TOldStream);
    destructor Done; virtual;
    function At(Index: Integer): Pointer;
    procedure AtDelete(Index: Integer);
    procedure AtFree(Index: Integer);
    procedure AtInsert(Index: Integer; Item: Pointer);
    procedure AtPut(Index: Integer; Item: Pointer);
    procedure Delete(Item: Pointer);
    procedure DeleteAll;
    procedure Error(Code, Info: Integer); virtual;
    function FirstThat(Test: Pointer): Pointer;
    procedure ForEach(Action: Pointer);
    procedure Free(Item: Pointer);
    procedure FreeAll;
    procedure FreeItem(Item: Pointer); virtual;
    function GetItem(var S: TOldStream): Pointer; virtual;
    function IndexOf(Item: Pointer): Integer; virtual;
    procedure Insert(Item: Pointer); virtual;
    function LastThat(Test: Pointer): Pointer; 
    procedure Pack;
    procedure PutItem(var S: TOldStream; Item: Pointer); virtual;
    procedure SetLimit(ALimit: Integer); virtual;
    procedure Store(var S: TOldStream);
  end;

{ TSortedCollection object }

  PSortedCollection = ^TSortedCollection;
  TSortedCollection = object(TCollection)
    Duplicates: Boolean;
    constructor Init(ALimit, ADelta: Integer);
    constructor Load(var S: TOldStream);
    function Compare(Key1, Key2: Pointer): Integer; virtual;
    function IndexOf(Item: Pointer): Integer; virtual;
    procedure Insert(Item: Pointer); virtual;
    function KeyOf(Item: Pointer): Pointer; virtual;
    function Search(Key: Pointer; var Index: Integer): Boolean; virtual;
    procedure Store(var S: TOldStream);
  end;

{ TStringCollection object }

  PStringCollection = ^TStringCollection;
  TStringCollection = object(TSortedCollection)
    function Compare(Key1, Key2: Pointer): Integer; virtual;
    procedure FreeItem(Item: Pointer); virtual;
    function GetItem(var S: TOldStream): Pointer; virtual;
    procedure PutItem(var S: TOldStream; Item: Pointer); virtual;
  end;

{ TStrCollection object }

  PStrCollection = ^TStrCollection;
  TStrCollection = object(TSortedCollection)
    function Compare(Key1, Key2: Pointer): Integer; virtual;
    procedure FreeItem(Item: Pointer); virtual;
    function GetItem(var S: TOldStream): Pointer; virtual;
    procedure PutItem(var S: TOldStream; Item: Pointer); virtual;
  end;

{ Stream routines }

function NewOfs(Const Ptr:Pointer):Pointer;

procedure RegisterType(var S: TStreamRec);

{ Abstract notification procedure }

procedure Abstract;

{ Objects registration procedure }

procedure RegisterObjects;

const

{ Stream error procedure }

  StreamError: Pointer = nil;

{ Stream registration records }

const
  RCollection: TStreamRec = (
    ObjType: 50;
    VmtLink: TypeOf(TCollection);
    Load: @TCollection.Load;
    Store: @TCollection.Store);

const
  RStringCollection: TStreamRec = (
    ObjType: 51;
    VmtLink: TypeOf(TStringCollection);
    Load: @TStringCollection.Load;
    Store: @TStringCollection.Store);

const
  RStrCollection: TStreamRec = (
    ObjType: 69;
    VmtLink: TypeOf(TStrCollection);
    Load:    @TStrCollection.Load;
    Store:   @TStrCollection.Store);

implementation

uses WinProcs, SysUtils,
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
     Forms;
var   WinGIS_RunState    : TWinGIS_RunState = runNotDef; { WinGIS runing state }
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

  {$DEFINE NewExeFormat}

procedure Abstract;
begin
//  RunError(211);
end;

function NewOfs(Const Ptr:Pointer):Pointer;
  begin
    Result:=Ptr;
  end;

{ TObject }

constructor TOldObject.Init;
type
  PImage=^Image;
  Image = record
    Link: Word;
    Data: Word;
  end;
begin
{  FillChar(PImage(@Self)^.Data, SizeOf(Self) - 4, 0);}
end;

{ Shorthand procedure for a done/dispose }

procedure TOldObject.Free;
begin
  Dispose(POldObject(@Self), Done);
end;

destructor TOldObject.Done;
begin
end;

{ TOldStream type registration routines }

const
  StreamTypes : PStreamRec = NIL;

Function TypeToStreamRec
   (
   ObjType         : Pointer
   )
   : PStreamRec;
  begin
    Result:=StreamTypes;
    while (Result<>NIL)
        and (Result^.VMTLink<>ObjType) do
      Result:=Result^.Next;
  end;

Function NumberToStreamRec
   (
   ObjType         : Word
   )
   : PStreamRec;
  begin
    Result:=StreamTypes;
    while (Result<>NIL)
        and (Result^.ObjType<>ObjType) do
      Result:=Result^.Next;
  end;

procedure RegisterError;
begin
//  RunError(212);
end;

procedure RegisterType(var S: TStreamRec);
begin
  if NumberToStreamRec(S.ObjType)<>NIL then RegisterError
  else begin
    S.Next:=StreamTypes;
    StreamTypes:=@S;
  end;  
end;

{ TOldStream support routines }

{ TOldStream }

constructor TOldStream.Init;
begin
  TOldObject.Init;
  Status := 0;
  ErrorInfo := 0;
end;

procedure TOldStream.CopyFrom(var S: TOldStream; Count: Longint);
var
  N: Word;
  Buffer: array[0..1023] of Byte;
begin
  while Count > 0 do
  begin
    if Count > SizeOf(Buffer) then N := SizeOf(Buffer) else N := Count;
    S.Read(Buffer, N);
    Write(Buffer, N);
    Dec(Count, N);
  end;
end;

procedure TOldStream.Error(Code, Info: Integer);
type
  TErrorProc = procedure(var S: TOldStream);
begin
  Status := Code;
  ErrorInfo := Info;
  if StreamError <> nil then TErrorProc(StreamError)(Self);
end;

procedure TOldStream.Flush;
begin
end;

var Last:Word;
function TOldStream.Get: POldObject;
var Info           : PStreamRec;
    ObjNumber      : Word;
    Cons           : Pointer;
    VMT            : Pointer;
begin
  Read(ObjNumber,SizeOf(ObjNumber));
  if Status<>stOK then Result:=NIL
  else if ObjNumber=0 then Result:=NIL
  else begin
    Info:=NumberToStreamRec(ObjNumber);
    if Info=NIL then begin
      Error(stGetError,ObjNumber);
      Result:=NIL;
    end
    else begin
      Cons:=Info^.Load;
      VMT:=Info^.VMTLink;
      ASM
        MOV ECX,Self
        MOV EDX,VMT
        XOR EAX,EAX
        CALL DWORD PTR Cons
        MOV @Result,EAX
      end;
    end;
  end;
  Last:=ObjNumber;
end;

function TOldStream.GetPos: Longint;
begin
  Abstract;
end;

function TOldStream.GetSize: Longint;
begin
  Abstract;
end;

procedure TOldStream.Put(P: POldObject);
var Info           : PStreamRec;
    Cons           : Pointer;
begin
  if P=NIL then Write(P,SizeOf(Word))
  else begin
    Info:=TypeToStreamRec(TypeOf(P^));
    if Info=NIL then Error(stPutError,0)
    else begin
      Write(Info^.ObjType,SizeOf(Info^.ObjType));
      Cons:=Info^.Store;               
      ASM
        MOV EDX,Self
        MOV EAX,P
        CALL DWORD PTR Cons
      end;
    end;
  end;
end;

procedure TOldStream.Read(var Buf; Count: Integer);
begin
  Abstract;
end;

function TOldStream.ReadStr: PString;
var
  L: Byte;
  P: Array[0..255] of Char;
begin
  Read(L, 1);                  
  if L > 0 then begin
    Read(P,L);
    P[L]:=#0;
    Result:=NewStr(StrPas(P));
  end
  else ReadStr := nil;
end;

procedure TOldStream.Reset;
begin
  Status := 0;
  ErrorInfo := 0;
end;

procedure TOldStream.Seek(Pos: Longint);
begin
  Abstract;
end;

function TOldStream.StrRead: PChar;
var
  L: Word;
  P: PChar;
begin
  Read(L, SizeOf(Word));
  if L = 0 then StrRead := nil else
  begin
    P:=StrAlloc(L+1);
    Read(P[0], L);
    P[L] := #0;
    StrRead := P;
  end;
end;

procedure TOldStream.StrWrite(P: PChar);
var
  L: Word;
begin
  if P = nil then L := 0 else L := StrLen(P);
  Write(L, SizeOf(Word));
  if P <> nil then Write(P[0], L);
end;

procedure TOldStream.Truncate;
begin
  Abstract;
end;

procedure TOldStream.Write(const Buf; Count: Integer);
begin
  Abstract;
end;

procedure TOldStream.WriteStr(P: PString);
var Str            : String;
begin
  if P<>NIL then begin
    Str:=P^;
    Write(Str,Length(Str)+1);
  end
  else begin
    Str:='';
    Write(Str,1);
  end;  
end;

procedure TOldStream.WriteString(Const S: String);
begin
  Write(S,Length(S)+1);
end;

function TOldStream.ReadString:String;
begin
  Read(Result[0],1);
  Read(Result[1],Byte(Result[0]));
end;

{ TBufStream }

Constructor TBufStream.Init
   (
   FileName        : FNameStr;
   AMode           : Integer;
   Size            : Integer
   );
  begin
    Mode:=AMode;
    BufSize:=Size;
    Status:=stOK;
    GetMem(Buffer,Size);
    // read IOResult to reset it and prevent from wrong results
    ErrorInfo:=IOResult;
    Assign(DataFile,FileName);
    {$I-}
    case Mode of
      stCreate   : Rewrite(DataFile,1);
      stOpenRead : begin
          FileMode:=0;
          System.Reset(DataFile,1);
          FileMode:=2;     
        end;
    end;
    {$I+}
    ErrorInfo:=IOResult;
    if ErrorInfo<>0 then Status:=stInitError;
    BufPtr:=0;
    BufEnd:=0;
  end;

Destructor TBufStream.Done;
  begin
    Flush;
    if Buffer<>NIL then FreeMem(Buffer,BufSize);
    if Status<>stInitError then Close(DataFile);
  end;

Procedure TBufStream.Flush;
  var Written      : LongInt;
  begin
    if Mode<>stOpenRead then begin
      BlockWrite(DataFile,Buffer^,BufEnd,Written);
      ErrorInfo:=IOResult;
      if Written<BufEnd then Status:=stReadError;
    end;
    BufPtr:=0;
    BufEnd:=0;
  end;

Function TBufStream.GetPos
   : LongInt;
  begin
    Result:=FilePos(DataFile)-BufEnd+BufPtr;
  end;

Function TBufStream.GetSize
   : LongInt;
  begin
    GetSize:=FileSize(DataFile);
  end;

Type PBA           = ^TByteArray;  

Procedure TBufStream.Read
   (
   var Buf         ;
   Count           : Integer
   );
  var Cnt          : Integer;
      Addr         : Integer;
      Data         : TByteArray Absolute Buf;
      BytesRead    : LongInt;
  begin
    if Status<>stOK then Exit;
    Addr:=0;
    while BufPtr+Count>BufEnd do begin
      Cnt:=BufEnd-BufPtr;
      Move(PBA(Buffer)^[BufPtr],Data[Addr],Cnt);
      BlockRead(DataFile,Buffer^,BufSize,BytesRead);
      ErrorInfo:=IOResult;
      if IOResult<>0 then Status:=stReadError;
      BufEnd:=BytesRead;
      BufPtr:=0;
      if BytesRead=0 then Status:=stReadError;
      if (Status<>stOK) then Exit;
      Dec(Count,Cnt);
      Inc(Addr,Cnt);
    end;
    Move(PBA(Buffer)^[BufPtr],Data[Addr],Count);
    Inc(BufPtr,Count);
  end;

Procedure TBufStream.Seek
   (
   Pos             : LongInt
   );
  begin
    if Status<>stOK then Exit;
    if Mode<>stOpenRead then Flush;
    System.Seek(DataFile,Pos);
    BufPtr:=0;
    BufEnd:=0;
  end;

Procedure TBufStream.Write
   (
   Const Buf         ;
   Count           : Integer
   );
  var Cnt          : Integer;
      Addr         : Integer;
      Data         : TByteArray Absolute Buf;
  begin
    if Status<>stOK then Exit;
    Addr:=0;
    while BufPtr+Count>BufSize do begin
      Cnt:=BufSize-BufPtr;
      Move(Data[Addr],PBA(Buffer)^[BufPtr],Cnt);
      Inc(BufPtr,Cnt);
      BufEnd:=BufPtr;
      Flush;
      Dec(Count,Cnt);
      Inc(Addr,Cnt);
    end;
    Move(Data[Addr],PBA(Buffer)^[BufPtr],Count);
    Inc(BufPtr,Count);
    BufEnd:=BufPtr;
  end;

{ TOldMemoryStream }

const
  MemoryDelta = $2000; { Must be a power of 2 }

constructor TOldMemoryStream.Init(ALimit: Longint; ABlockSize: Integer);
begin
  inherited Init;
  FCapacity:=0;
  FMemory:=NIL;
  FSize:=0;
  FPosition:=0;
  SetCapacity(ALimit);
end;

destructor TOldMemoryStream.Done;
begin
  SetCapacity(0);
  inherited Done;
end;

procedure TOldMemoryStream.SetCapacity(NewCapacity: Longint);
begin
  SetPointer(Realloc(NewCapacity), FSize);
  FCapacity := NewCapacity;
end;

function TOldMemoryStream.Realloc(var NewCapacity: Longint): Pointer;
begin
  if NewCapacity > 0 then
    NewCapacity := (NewCapacity + (MemoryDelta - 1)) and not (MemoryDelta - 1);
  Result := FMemory;
  if NewCapacity <> FCapacity then
  begin
    if NewCapacity = 0 then
    begin
      GlobalFreePtr(FMemory);
      Result := nil;
    end else
    begin
      if FCapacity = 0 then
        Result := GlobalAllocPtr(HeapAllocFlags, NewCapacity)
      else
        Result := GlobalReallocPtr(FMemory, NewCapacity, HeapAllocFlags);
      if Result = nil then Error(stError,0);
    end;
  end;
end;

function TOldMemoryStream.GetPos: Longint;
begin
  if Status<>0 then Result:=-1
  else Result:=FPosition;
end;

function TOldMemoryStream.GetSize: Longint;
begin
  if Status<>stOK then Result:=-1
  else Result:=FSize;
end;

procedure TOldMemoryStream.Read(var Buf; Count: Integer);
var ACount : LongInt;
begin
  if (FPosition >= 0) and (Count >= 0) then
  begin
    ACount := FSize - FPosition;
    if ACount<0 then Error(stReadError,0)
    else begin
      if ACount > Count then ACount := Count;
      Move(Pointer(Longint(FMemory) + FPosition)^, Buf, ACount);
      Inc(FPosition, ACount);
    end;
  end;
end;

procedure TOldMemoryStream.Seek(Pos: Longint);
begin
  FPosition:=Pos;
end;

procedure TOldMemoryStream.SetPointer(Ptr: Pointer; Size: Longint);
begin
  FMemory := Ptr;
  FSize := Size;
end;

procedure TOldMemoryStream.Truncate;
begin
end;

procedure TOldMemoryStream.Write(const Buf; Count: Integer);
var
  Pos: Longint;
begin
  if (FPosition >= 0) and (Count >= 0) then
  begin
    Pos := FPosition + Count;
    if Pos > 0 then
    begin
      if Pos > FSize then
      begin
        if Pos > FCapacity then
          SetCapacity(Pos);
        FSize := Pos;
      end;
      System.Move(Buf, Pointer(Longint(FMemory) + FPosition)^, Count);
      FPosition := Pos;
      Exit;
    end;
  end;
end;

{ TCollection }

constructor TCollection.Init(ALimit, ADelta: Integer);
begin
  TOldObject.Init;
  Items := nil;
  Count := 0;
  Limit := 0;
  Delta := ADelta;
  SetLimit(ALimit);
end;

constructor TCollection.Load(var S: TOldStream);
var C, I: Integer;
    Cnt : SmallInt;
    OldData : Array[0..1] of SmallInt;
begin
  S.Read(Cnt,SizeOf(Cnt));
  if Cnt<>-1 then begin
    Count:=Cnt;
    S.Read(OldData,SizeOf(SmallInt)*2);
    Limit:=OldData[0];
    Delta:=OldData[1];
  end
  else S.Read(Count,SizeOf(Integer)*2+SizeOf(SmallInt));
  Items := nil;
  C := Count;
  I := Limit;
  Count := 0;
  Limit := 0;
  SetLimit(I);
  Count := C;
  for I := 0 to C - 1 do AtPut(I, GetItem(S));
end;

destructor TCollection.Done;
begin
  FreeAll;
  SetLimit(0);
end;

function TCollection.At(Index: Integer): Pointer;
begin
  if Index>=Count then Error(coIndexError,0)
  else Result:=Items^[Index];
end;

procedure TCollection.AtDelete(Index: Integer);
begin
  if Index>=Count then Error(coIndexError,0)
  else begin
    Move(Items^[Index+1],Items^[Index],(Count-Index-1)*SizeOf(Pointer));
    Dec(Count);
  end;
end;

procedure TCollection.AtFree(Index: Integer);
var
  Item: Pointer;
begin
  Item := At(Index);
  AtDelete(Index);
  FreeItem(Item);
end;

procedure TCollection.AtInsert(Index: Integer; Item: Pointer);
begin
  if Index>Count then Error(coIndexError,0)
  else begin
    if Count>=Limit then SetLimit(Limit+Delta);
    Move(Items^[Index],Items^[Index+1],(Count-Index)*SizeOf(Pointer));
     Items^[Index]:=Item;
    Inc(Count);
  end;
end;

procedure TCollection.AtPut(Index: Integer; Item: Pointer);
begin
  if Index>=Count then Error(coIndexError,0)
  else Items^[Index]:=Item;
end;

procedure TCollection.Delete(Item: Pointer);
begin
  AtDelete(IndexOf(Item));
end;

procedure TCollection.DeleteAll;
begin
  Count := 0;
end;

procedure TCollection.Error(Code, Info: Integer);
begin
//  RunError(212 - Code);
end;

function TCollection.FirstThat(Test: Pointer): Pointer;
var Cnt       : Integer;
    Item      : Pointer;
begin
  Result:=NIL; 
  Cnt:=0;
  while (Cnt<Count) do begin
    Item:=At(Cnt);
    ASM
      MOV     EAX,Item
      PUSH    DWORD PTR [EBP]
      CALL    DWORD PTR Test
      ADD     ESP,4
      CMP AL,0
      JE @@1
      MOV EAX,Item
      MOV @Result,EAX
      MOV Cnt,$7FFE
    @@1: INC Cnt
    end;
  end;
end;

procedure TCollection.ForEach(Action: Pointer);
var Cnt       : Integer;             
    Item      : Pointer;
begin
  for Cnt:=0 to Count-1 do begin
    Item:=At(Cnt);
    ASM
      MOV     EAX,Item
      PUSH    DWORD PTR [EBP]
      CALL    DWORD PTR Action
      ADD     ESP,4
    end;
  end;                  
end;

procedure TCollection.Free(Item: Pointer);
begin
  Delete(Item);
  FreeItem(Item);
end;

procedure TCollection.FreeAll;
var
  I: Integer;
begin
  for I := 0 to Count - 1 do FreeItem(At(I));
  Count := 0;
end;

procedure TCollection.FreeItem(Item: Pointer);
begin
  if Item <> nil then Dispose(POldObject(Item), Done);
end;

function TCollection.GetItem(var S: TOldStream): Pointer;
begin
  GetItem := S.Get;
end;

function TCollection.IndexOf(Item: Pointer): Integer;
var Cnt       : Integer;
begin
  for Cnt:=0 to Count-1 do if Items^[Cnt]=Item then begin
    Result:=Cnt;
    Exit;
  end;
  Result:=-1;
end;

procedure TCollection.Insert(Item: Pointer);
begin
  AtInsert(Count, Item);
end;

function TCollection.LastThat(Test: Pointer): Pointer; 
var Cnt       : Integer;
    Item      : Pointer;
begin
  Cnt:=Count-1;
  while (Cnt>=0) do begin
    Item:=At(Cnt);
    ASM
      MOV EAX,Item
      PUSH DWORD PTR [EBP]
      CALL DWORD PTR Test
      ADD  ESP,4
      CMP AL,0
      JE @@1
      MOV Cnt,0
      MOV EAX,Item
      MOV @Result,EAX
    @@1: DEC Cnt
    end;
  end;
end;

procedure TCollection.Pack;
begin
end;

procedure TCollection.PutItem(var S: TOldStream; Item: Pointer);
begin
  S.Put(Item);
end;

procedure TCollection.SetLimit(ALimit: Integer);
var
  AItems: PItemList;
begin
  if ALimit < Count then ALimit := Count;
  if ALimit > MaxCollectionSize then ALimit := MaxCollectionSize;
  if ALimit <> Limit then begin
    if ALimit = 0 then AItems := nil else
    begin
      GetMem(AItems, ALimit * SizeOf(Pointer));
      if (Count <> 0) and (Items <> nil) then
        Move(Items^, AItems^, Count * SizeOf(Pointer));
    end;
    if Limit <> 0 then FreeMem(Items, Limit * SizeOf(Pointer));
    Items := AItems;
    Limit := ALimit;
  end;
end;

procedure TCollection.Store(var S: TOldStream);

procedure DoPutItem(P: Pointer); far;
begin
  PutItem(S, P);
end;
var Cnt : SmallInt;
begin
  Cnt:=-1;
  S.Write(Cnt, SizeOf(SmallInt));
  S.Write(Count,SizeOf(Integer)*2+SizeOf(SmallInt));
  ForEach(@DoPutItem);
end;

{ TSortedCollection }

constructor TSortedCollection.Init(ALimit, ADelta: Integer);
begin
  TCollection.Init(ALimit, ADelta);
  Duplicates := False;
end;

constructor TSortedCollection.Load(var S: TOldStream);
begin
  TCollection.Load(S);
  S.Read(Duplicates, SizeOf(Boolean));
end;

function TSortedCollection.Compare(Key1, Key2: Pointer): Integer;
begin
  Abstract;
end;

function TSortedCollection.IndexOf(Item: Pointer): Integer;
var
  I: Integer;
begin
  IndexOf := -1;
  if Search(KeyOf(Item), I) then
  begin
    if Duplicates then
      while (I < Count) and (Item <> Items^[I]) do Inc(I);
    if I < Count then IndexOf := I;
  end;
end;

procedure TSortedCollection.Insert(Item: Pointer);
var
  I: Integer;
begin
  if not Search(KeyOf(Item), I) or Duplicates then AtInsert(I, Item);
end;

function TSortedCollection.KeyOf(Item: Pointer): Pointer;
begin
  KeyOf := Item;
end;

function TSortedCollection.Search(Key: Pointer; var Index: Integer): Boolean;
var
  L, H, I, C: Integer;
begin
  Search := False;
  L := 0;
  H := Count - 1;
  while L <= H do
  begin
    I := (L + H) shr 1;
    C := Compare(KeyOf(Items^[I]), Key);
    if C < 0 then L := I + 1 else
    begin
      H := I - 1;
      if C = 0 then
      begin
        Search := True;
        if not Duplicates then L := I;
      end;
    end;
  end;
  Index := L;
end;

procedure TSortedCollection.Store(var S: TOldStream);
begin
  TCollection.Store(S);
  S.Write(Duplicates, SizeOf(Boolean));
end;

{ TStringCollection }

function TStringCollection.Compare(Key1, Key2: Pointer): Integer;
begin
  Result:=CompareText(PString(Key1)^,PString(Key2)^);
end;

procedure TStringCollection.FreeItem(Item: Pointer);
begin
  DisposeStr(Item);
end;

function TStringCollection.GetItem(var S: TOldStream): Pointer;
begin
  GetItem := S.ReadStr;
end;

procedure TStringCollection.PutItem(var S: TOldStream; Item: Pointer);
begin
  S.WriteStr(Item);
end;

{ TStrCollection }

function TStrCollection.Compare(Key1, Key2: Pointer): Integer;
begin
  Compare := StrComp(Key1, Key2);
end;

procedure TStrCollection.FreeItem(Item: Pointer);
begin
  StrDispose(Item);
end;

function TStrCollection.GetItem(var S: TOldStream): Pointer;
begin
  GetItem := S.StrRead;
end;

procedure TStrCollection.PutItem(var S: TOldStream; Item: Pointer);
begin
  S.StrWrite(Item);
end;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}
function WinGIS_runing: Boolean;         { WinGIS is runing ?                }
var strName   : String;
    intLength : Integer;
begin
   Result:=FALSE;
   if WinGIS_RunState = runNotDef then
      if Assigned(Application) then
         begin
            intLength:=Length(Application.ExeName);
            if intLength >= 12 then
               begin
                  strName:=Copy(Application.ExeName,intLength-11,12);
                  if strName = co_DelphiExeName then
                     WinGIS_RunState:=runComponent
                  else
                     WinGIS_RunState:=runWinGIS;
               end;
         end;
   Result:=WinGIS_RunState = runWinGis;
end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

{ Objects registration procedure }

procedure RegisterObjects;
begin
  RegisterType(RCollection);
  RegisterType(RStringCollection);
  RegisterType(RStrCollection);
end;

end.
