{******************************************************************************+
  Unit InpHndl
--------------------------------------------------------------------------------
  Autor: Martin Forst
--------------------------------------------------------------------------------
  Enth�lt diverse Mauseingabe-Objekte.
   - TRubberBox
   - TRubberLine
   - TRubberArc
   - TGuideLine
--------------------------------------------------------------------------------
  Version 1.0 10-04-1997
----------------------------------------------------------
  Some little changes and correction May 2001, Brovak
  Added support for 
+******************************************************************************}
Unit InpHndl;

Interface

Uses Classes,Controls,EditHndl,Graphics,GrTools,MenuHndl;

Const inpstWaitForFirstPoint      = $0001;
      inpstWaitForSecondPoint     = $0002;
      inpstWaitForThirdPoint      = $0003;
      inpstValid                  = $0100;

Type TGridAlignment     = (gaLeft, gaCentered, gaRight);
     TGridCellType      = (ctConstantSize,ctConstantCount);
     
     TCalculateCellEvent= Procedure (Sender:TObject;Index:Integer;var Size:Double) of Object;

     TRubberGrid        = Class(TEditHandler)
      Private
       FColAlign        : TGridAlignment;
       FColCount        : Integer;
       FColType         : TGridCellType;
       FColWidth        : Double;
       FOnCalcCol       : TCalculateCellEvent;
       FOnCalcRow       : TCalculateCellEvent;
       FRowAlign        : TGridAlignment;
       FRowCount        : Integer;
       FRowHeight       : Double;
       FRowType         : TGridCellType;
       Procedure   SetColAlign(AAlign:TGridAlignment);
       Procedure   SetColCount(ACount:Integer);
       Procedure   SetColWidth(const AWidth:Double);
       Procedure   SetRowAlign(AAlign:TGridAlignment);
       Procedure   SetRowCount(ACount:Integer);
       Procedure   SetRowHeight(const AHeight:Double);
      Protected
       Procedure   DoPaintInterior; override;
       Procedure   RecalculateGrid;
      Public
       Function    OnMouseDown(Const X,Y:Double;Button:TMouseButton;Shift:TShiftState):Boolean; override;
       Function    OnMouseMove(Const X,Y:Double;Shift:TShiftState):Boolean; override;
       Function    OnMouseUp(Const X,Y:Double;Button:TMouseButton;Shift:TShiftState):Boolean; override;
      Public
       Constructor Create(AOwner:TComponent); override;
       Destructor  Destroy; override;
       { event to calculate the width of a column, if not set ColWidth is used }
       Property    OnCalculateColWidth:TCalculateCellEvent read FOnCalcCol write FOnCalcCol;
       { event to calculate the height of a row, if not set RowHeight is used }
       Property    OnCalculateRowHeight:TCalculateCellEvent read FOnCalcRow write FOnCalcRow;
       { aligment of the grid-columns within the frame-width }
       Property    ColAlignment:TGridAlignment read FColAlign write SetColAlign;
       { number of columns }
       Property    ColCount:Integer read FColCount write SetColCount;
       { width of the column }
       Property    ColWidth:Double read FColWidth write SetColWidth;
       Property    ColType:TGridCellType read FColType write FColType;
       { aligment of the grid-rows within the frame-width }
       Property    RowAlignment:TGridAlignment read FRowAlign write SetRowAlign;
       { number of rows }
       Property    RowCount:Integer read FRowCount write SetRowCount;
       { height of the rows }
       Property    RowHeight:Double read FRowHeight write SetRowHeight;
       Property    RowType:TGridCellType read FRowType write FRowType;
     end;

     TGuideLineOption   = (goMove,goRotate,goMovePosition);
     TGuideLineOptions  = Set of TGuideLineOption;

     TGuideLine         = Class(TMenuHandler)
      Private
       FAngle           : Double;
       FControlPen      : TPen;
       FControlSize     : Integer;
       FDrawIfSelected  : Boolean;
       FDrawIfNotSelected : Boolean;
       FLastPos         : TGrPoint;
       FMaxDistance     : Integer;
       FMoving          : Boolean;
       FPositionMoving  : Boolean;
       FOnMove          : TNotifyEvent;
       FOnMovePosition  : TNotifyEvent;
       FOnRotate        : TNotifyEvent;
       FOptions         : TGuideLineOptions;
       FPosition        : TGrPoint;
       FRotating        : Boolean;
       FSelected        : Boolean;
       FStartPos        : TGrPoint;
       Function    FindControlPoint(Const Point:TGrPoint):Integer;
       Procedure   GetCurrentPosition(var Position:TGrPoint;var Angle:Double);
       Function    GetControlPoints(var Points:Array of TGrPoint;All:Boolean):Boolean;
       Function    IntMaxDistance:Double;
       Procedure   SetAngle(Const AAngle:Double);
       Procedure   SetPosition(Const APosition:TGrPoint);
       Procedure   SetSelected(ASelected:Boolean);
       Function    VirtControlSize:Double;
      Protected
       Procedure   DoPaint; override;
      Public
       Function    OnMouseDown(Const X,Y:Double;Button:TMouseButton;Shift:TShiftState):Boolean; override;
       Function    OnMouseMove(Const X,Y:Double;Shift:TShiftState):Boolean; override;
       Function    OnMouseUp(Const X,Y:Double;Button:TMouseButton;Shift:TShiftState):Boolean; override;
       Function    LineEndPoints(var Point1,Point2:TGrPoint):Boolean;
      Public
       Constructor Create(AParent:TComponent); override;
       Destructor  Destroy; override;
       // angle of the guideline in rad
       Property    Angle:Double read FAngle write SetAngle;
       // current angle of the guideline, differs from angle if in rotating-mode
       Function    CurrentAngle:Double;
       // current position of the guide-line, differs from Position if in move-mode 
       Function    CurrentPosition:TGrPoint;
       // true if the guide-line is in the editing.state
       Function    Editing:Boolean;
       // determines if the specified point is near the guideline, uses MaxDistance
       Function    IsPointNearLine(Const Point:TGrPoint):Boolean;
       // true if the user moves the guideline
       Property    Moving:Boolean read FMoving;
       // reference-position of the guideline
       Property    Position:TGrPoint read FPosition write SetPosition;
       // true if the user moves the reference-point along the guideline
       Property    PositionMoving:Boolean read FPositionMoving;
       // true if the user rotates the guideline }
       Property    Rotating:Boolean read FRotating;
       // true if the guideline is selected (drawn in red in this case)
       Property    Selected:Boolean read FSelected write SetSelected;
      Published
       // size of the control-points in window-units
       Property    ControlSize:Integer read FControlSize write FControlSize default 3;
       // draw the guide-line if selected/not selected
       Property    DrawIfNotSelected:Boolean read FDrawIfSelected write FDrawIfNotSelected default TRUE;
       Property    DrawIfSelected:Boolean read FDrawIfSelected write FDrawIfSelected default TRUE;
       // maximum distance a point may have to be recognized "near the guideline" (window-units)
       Property    MaxDistance:Integer read FMaxDistance write FMaxDistance default 3;
       // notification send when the user finishes moving the guideline
       Property    OnMove:TNotifyEvent read FOnMove write FOnMove;
       // notification send when the user finishes moving the reference-position
       Property    OnMovePosition:TNotifyEvent read FOnMovePosition write FOnMovePosition;
       // notification send when the user finishes rotating the guideline
       Property    OnRotate:TNotifyEvent read FOnRotate write FOnRotate;
       // editing-options for the guide-line (combination of goXXXX)
       Property    Options:TGuideLineOptions read FOptions write FOptions;
       // pen to draw the guide-line if not selected
       Property    Pen;
     end;

     TRubberBox         = Class(TMenuHandler)
      Private
       FActiveCursor    : TCursor;
       FBoxVisible      : Boolean;
       FInactiveCursor  : TCursor;
       FLastX           : Double;
       FLastY           : Double;
       FMinSize         : Double;
       FMinSizeCursor   : TCursor;
       FOnFinishedInput : TNotifyEvent;
       FOnMinSizeReached: TNotifyEvent;
       FOnStartInput    : TNotifyEvent;
       FPositives       : Boolean;
       FSizing          : Boolean;
       FSquare          : Boolean;
       FStartX          : Double;
       FStartY          : Double;
       FStatus          : Word;
       FTwoClicks       : Boolean;
       Procedure   DrawFrame;
       Function    GetMinSizeReached:Boolean;
       Procedure   SetActiveCursor(ACursor:TCursor);
       Procedure   SetInActiveCursor(ACursor:TCursor);
       Procedure   SetMinSizeCursor(ACursor:TCursor);
      Protected
       Procedure   DoPaint; override;
      Public
       Function    OnMouseDown(Const X,Y:Double;Button:TMouseButton;Shift:TShiftState):Boolean; override;
       Function    OnMouseMove(Const X,Y:Double;Shift:TShiftState):Boolean; override;
       Function    OnMouseUp(Const X,Y:Double;Button:TMouseButton;Shift:TShiftState):Boolean; override;
      Public
       Constructor Create(AParent:TComponent); override;
       Function    Box:TGrRect;
       Function    Height:Double;
       Property    MinSizeReached:Boolean read GetMinSizeReached;
       Property    Status:Word read FStatus;
       Function    Width:Double;
       Function    Position:TGrPoint;
      Published
       Property    ActiveCursor:TCursor read FActiveCursor write SetActiveCursor;
       Property    InactiveCursor:TCursor read FInactiveCursor write SetInactiveCursor;
       Property    MinSize:Double read FMinSize write FMinSize;
       Property    MinSizeReachedCursor:TCursor read FMinSizeCursor write SetMinSizeCursor;
       Property    OnFinishedInput:TNotifyEvent read FOnFinishedInput write FOnFinishedInput;
       Property    OnMinSizeReached:TNotifyEvent read FOnMinSizeReached write FOnMinSizeReached;
       Property    OnStartInput:TNotifyEvent read FOnStartInput write FOnStartInput;
       Property    PositiveDimensions:Boolean read FPositives write FPositives;
       Property    Square:Boolean read FSquare write FSquare;
       Property    TwoClicks:Boolean read FTwoClicks write FTwoClicks default false;
     end;

     TRubberLine   = Class(TMenuHandler)
      Private
       FActiveCursor    : TCursor;
       FActive          : Boolean;
       FInActiveCursor  : TCursor;
       FLastX           : Double;
       FLastY           : Double;
       FOnFinishedInput : TNotifyEvent;
       FOnStartInput    : TNotifyEvent;
       FStartX          : Double;
       FStartY          : Double;
       FTwoClicks       : Boolean;
       {++Brovak}
       FForRotate       : Boolean;
       {--Brovak}
       Procedure   SetActiveCursor(ACursor:TCursor);
       Procedure   SetInActiveCursor(ACursor:TCursor);
      Protected
       Procedure   DrawLine;
      Public
       Function    OnMouseDown(Const X,Y:Double;Button:TMouseButton;Shift:TShiftState):Boolean; override;
       Function    OnMouseMove(Const X,Y:Double;Shift:TShiftState):Boolean; override;
       Function    OnMouseUp(Const X,Y:Double;Button:TMouseButton;Shift:TShiftState):Boolean; override;
      Public

       Property    Active:Boolean read FActive;
       Property    XStart:Double read FStartX;
       Property    XEnd:Double read FLastX;
       Property    YStart:Double read FStartY;
       Property    YEnd:Double read FLastY;

      Published
       Property    ActiveCursor:TCursor read FActiveCursor write SetActiveCursor;
       Property    InActiveCursor:TCursor read FInactiveCursor write SetInactiveCursor;
       Property    OnFinishedInput:TNotifyEvent read FOnFinishedInput write FOnFinishedInput;
       Property    OnStartInput:TNotifyEvent read FOnStartInput write FOnStartInput;
       Property    TwoClicks:Boolean read FTwoClicks write FTwoClicks default false;
       {++Brovak}
       Property    ForRotate: boolean  read FForRotate write FForRotate default false;
       {--Brovak}
     end;

     TRubberEllipseType = (retCircleCenterRadius,retCircle2Points,retCircle3Points,
                           retCircle2PointsCenter,retCircleInRectangle,retEllipseCenter,
                           retEllipseAxes,retEllipseInRectangle);

     {++brovak - for edit Ellipse}
     TVladType = Array[0..3] of TGrPoint;
     TVladType1 = Array[0..5] of TGrPoint;
     {--}

     TRubberEllipse     = Class(TMenuHandler)
      private
      Protected
       FMiddle          : TGrPoint;
       FOnFinishedInput : TNotifyEvent;
       FOnStartInput    : TNotifyEvent;
       FPrimaryAxis     : Double;
       FPoints          : TVladType;
       FPointCnt        : Integer;
       FStatus          : Word;
       FType            : TRubberEllipseType;
       Procedure   CalculateEllipse;
       Procedure   DrawEllipse(DrawHandles:Boolean);
       Procedure   DoPaint; override;
      Public
       Function    OnMouseDown(Const X,Y:Double;Button:TMouseButton;Shift:TShiftState):Boolean; override;
       Function    OnMouseMove(Const X,Y:Double;Shift:TShiftState):Boolean; override;
       Function    OnMouseUp(Const X,Y:Double;Button:TMouseButton;Shift:TShiftState):Boolean; override;
       Procedure   OnUndo(Steps:Integer); override;
       {+++brovak for edit circle}
       Procedure   ImportNewCoords(PointCntImport:integer);
       procedure   SetArray(ArrayOP:TVladType);
       Property    ArrayOfPoints: TVladType read FPoints;
       Property    PointsCount:Integer   read FPointCnt;
       {---brovak}

      Public
       Constructor Create(AParent:TComponent); override;
       Property    Center:TGRPoint read FMiddle;
       Property    PrimaryAxis:Double read FPrimaryAxis;
       Property    SecondaryAxis:Double read FPrimaryAxis;
      Published
       Property    InputType:TRubberEllipseType read FType write FType;
       Property    OnFinishedInput:TNotifyEvent read FOnFinishedInput write FOnFinishedInput;
       Property    OnStartInput:TNotifyEvent read FOnStartInput write FOnStartInput;
     end;

     TRubberArcType     = (ratArcCenter,ratArc3Points,ratArc2PointsCenter,
                           ratArcInRectangle,ratEllipseArcCenter,ratEllipseArcAxes,
                           ratEllipseArcInRectangle);

     TRubberArc         = Class(TMenuHandler)
      Private
       FBeginAngle      : Double;
       FChordMode       : Boolean;
       FEndAngle        : Double;
       FInCalculateArc  : Boolean;
       FMiddle          : TGrPoint;
       FOnFinishedInput : TNotifyEvent;
       FOnStartInput    : TNotifyEvent;
       FPrimaryAxis     : Double;
       FPoints          : TVladType1;
       FPointCnt        : Integer;
       FStatus          : Word;
       FType            : TRubberArcType;
       X1,Y1            : double;
       Procedure   CalculateArc;
       Procedure   DrawArc(DrawHandles:Boolean;FromPaint:Boolean);
       Procedure   CorrectCoordWithShiftPressed;
      Protected
       Procedure   DoPaint; override;
      Public
       Function    OnMouseDown(Const X,Y:Double;Button:TMouseButton;Shift:TShiftState):Boolean; override;
       Function    OnMouseMove(Const X,Y:Double;Shift:TShiftState):Boolean; override;
       Function    OnMouseUp(Const X,Y:Double;Button:TMouseButton;Shift:TShiftState):Boolean; override;
       Procedure   OnUndo(Steps:Integer); override;
        {+++brovak for edit circle}
       Procedure   ImportNewCoords(PointCntImport:integer);
       procedure   SetArray(ArrayOP:TVladType1);
       Property    ArrayOfPoints: TVladType1 read FPoints;
       Property    PointsCount:Integer   read FPointCnt;
       {---brovak}
      Public
       Constructor Create(AParent:TComponent); override;
       Property    BeginAngle:Double read FBeginAngle;
       Property    EndAngle:Double read FEndAngle;
       Property    Center:TGRPoint read FMiddle;
       Property    PrimaryAxis:Double read FPrimaryAxis;
       Property    SecondaryAxis:Double read FPrimaryAxis;
      Published
       Property    ChordMode:Boolean read FChordMode write FChordMode default False;
       Property    InputType:TRubberArcType read FType write FType;
       Property    OnFinishedInput:TNotifyEvent read FOnFinishedInput write FOnFinishedInput;
       Property    OnStartInput:TNotifyEvent read FOnStartInput write FOnStartInput;
     end;

     TRubberPolygon     = Class(TMenuHandler)
      Private
       FCapacity        : Integer;
       FDrawClosed      : Boolean;
       FLastX           : Double;
       FLastY           : Double;
       FMaximumPoints   : Integer;
       FOnFinishedInput : TNotifyEvent;
       FOnStartInput    : TNotifyEvent;
       FPointCount      : Integer;
       FPoints          : PGrPointArray;
       Function    GetPoint(AIndex:Integer):TGrPoint;
       Procedure   SetCapacity(ACapacity:Integer);
      Protected
       Procedure   DrawPolyline(DrawAll:Boolean);
       Procedure   DoPaint; override;
      Public
       Function    OnMouseDown(Const X,Y:Double;Button:TMouseButton;Shift:TShiftState):Boolean; override;
       Function    OnMouseMove(Const X,Y:Double;Shift:TShiftState):Boolean; override;
       Function    OnMouseUp(Const X,Y:Double;Button:TMouseButton;Shift:TShiftState):Boolean; override;
       Procedure   OnUndo(Steps:Integer); override;
      Public 
       Destructor  Destroy; override;
       Property    PointCount:Integer read FPointCount;
       Property    Points[AIndex:Integer]:TGrPoint read GetPoint; default;
      Published
       Property    DrawClosed:Boolean read FDrawClosed write FDrawClosed;
       Property    MaximumPoints:Integer read FMaximumPoints write FMaximumPoints;
       Property    OnFinishedInput:TNotifyEvent read FOnFinishedInput write FOnFinishedInput;
       Property    OnStartInput:TNotifyEvent read FOnStartInput write FOnStartInput;
     end;

Implementation

Uses CommRes,NumTools,WinProcs,WinTypes,SysUtils,MultiLng{,Math}; {++ Sygsky: Math added }

{==============================================================================+
  TRubberBox
+==============================================================================}

Constructor TRubberBox.Create(AParent:TComponent);
begin
  inherited Create(AParent);
  FStatus:=inpstWaitForFirstPoint;
end;

Function TRubberBox.OnMouseDown(Const X,Y:Double;Button:TMouseButton;
    Shift:TShiftState):Boolean;
begin
  Result:=FALSE;
  if FSizing then begin
    if FBoxVisible then DrawFrame;
    FLastX:=X;                                           
    FLastY:=Y;
    FSizing:=FALSE;
    Cursor:=FInactiveCursor;
    FStatus:=inpstValid or inpstWaitForFirstPoint;
    if Assigned(FOnFinishedInput) then OnFinishedInput(Self);
    FBoxVisible:=FALSE;
    Result:=TRUE;
  end
  else if Button=mbLeft then begin
    FStartX:=X;
    FStartY:=Y;
    FLastX:=X;
    FLastY:=Y;
    FSizing:=TRUE;
    FBoxVisible:=FALSE;
    Cursor:=FActiveCursor;
    FStatus:=inpstWaitForSecondPoint;
    if Assigned(FOnStartInput) then OnStartInput(Self);
    Result:=TRUE;
  end;
end;

Function TRubberBox.OnMouseMove(Const X,Y:Double;Shift:TShiftState):Boolean;
begin
  Result:=FALSE;
  if FSizing then begin
    if FBoxVisible then DrawFrame;
    FBoxVisible:=(Abs(Width)>WindowToInternalDist(FMinSize))
        and (Abs(Height)>WindowToInternalDist(FMinSize));
    FLastX:=X;
    FLastY:=Y;
    if FBoxVisible then begin
      DrawFrame;
      Cursor:=FMinSizeCursor;  
    end;
    Result:=TRUE;
  end;
end;

Function TRubberBox.OnMouseUp(Const X,Y:Double;Button:TMouseButton;Shift:TShiftState):Boolean;
begin
  Result:=FALSE;
  if not FTwoClicks and (Button=mbLeft) and FSizing then begin
    if FBoxVisible then DrawFrame;
    FLastX:=X;
    FLastY:=Y;
    FSizing:=FALSE;
    Cursor:=FInactiveCursor;
    FStatus:=inpstValid or inpstWaitForFirstPoint;
    if Assigned(FOnFinishedInput) then OnFinishedInput(Self);
    FBoxVisible:=FALSE;
    Result:=TRUE;
  end;
end;

Function TRubberBox.GetMinSizeReached:Boolean;
begin
  Result:=(Abs(Width)>WindowToInternalDist(FMinSize))
      and (Abs(Height)>WindowToInternalDist(FMinSize));
end;

Procedure TRubberBox.DrawFrame;
var DX           : Double;
    DY           : Double;
    Rect         : TRotRect;
    Corners      : TGrRectCorners;
    Cnt          : Integer;
begin
  if FBoxVisible then begin
    Rect:=RotRect(GrPoint(FStartX,FStartY),GrPoint(FLastX,FLastY),-FViewRotation);
    RectCorners(Rect,Corners);
    if Canvas<>NIL then with Canvas do begin
      ActivateDrawingTools;
      InternalToWindowPoint(Corners[0].X,Corners[0].Y,DX,DY);
      MoveTo(Round(DX),Round(DY));
      for Cnt:=1 to 3 do begin
        InternalToWindowPoint(Corners[Cnt].X,Corners[Cnt].Y,DX,DY);
        LineTo(Round(DX),Round(DY));
      end;
      InternalToWindowPoint(Corners[0].X,Corners[0].Y,DX,DY);
      LineTo(Round(DX),Round(DY));
      RestoreDrawingTools;
    end;
  end;
end;

Function TRubberBox.Width:Double;
var DX             : Double;
    DY             : Double;
begin
  DX:=FLastX-FStartX;
  DY:=FLastY-FStartY;
  Result:=DX*FCosViewRotation-DY*FSinViewRotation;
  if FPositives then Result:=Abs(Result);
end;

Function TRubberBox.Height:Double;
var DX             : Double;
    DY             : Double;
begin
  DX:=FLastX-FStartX;
  DY:=FLastY-FStartY;
  Result:=DY*FCosViewRotation+DX*FSinViewRotation;
  if FPositives then Result:=Abs(Result);
end;

Function TRubberBox.Position:TGrPoint;
var Rect           : TRotRect;
begin
  if FPositives then begin
    Rect:=RotRect(GrPoint(FStartX,FStartY),GrPoint(FLastX,FLastY),-FViewRotation);
    Result:=Rect.Origin;
  end
  else Result:=GrPoint(FStartX,FStartY);
end;

Procedure TRubberBox.SetActiveCursor(ACursor:TCursor);
begin
  FActiveCursor:=ACursor;
  if FSizing and not FBoxVisible then Cursor:=FActiveCursor;
end;

Procedure TRubberBox.SetInActiveCursor(ACursor:TCursor);
begin
  FInActiveCursor:=ACursor;
  if not FSizing and not FBoxVisible then Cursor:=FInActiveCursor;
end;

Procedure TRubberBox.SetMinSizeCursor(ACursor:TCursor);
begin
  FMinSizeCursor:=ACursor;
  if FBoxVisible then Cursor:=FMinSizeCursor;
end;

Procedure TRubberBox.DoPaint;
begin
  if FBoxVisible then DrawFrame;
end;

Function TRubberBox.Box:TGrRect;
begin
  Result:=GrBounds(Position.X,Position.Y,Width,Height);
end;

{===============================================================================
| TRubberLine
+==============================================================================}

Function TRubberLine.OnMouseDown
   (
   Const X         : Double;
   Const Y         : Double;
   Button          : TMouseButton;
   Shift           : TShiftState
   )
   : Boolean;
  begin
    Result:=FALSE;
    if Button=mbLeft then begin
      if not FActive then begin
        FStartX:=X;
        FStartY:=Y;
        FLastX:=X;
        FLastY:=Y;
        FActive:=TRUE;
        Cursor:=FActiveCursor;
        if Assigned(FOnStartInput) then OnStartInput(Self);
        {++Brovak}
        if ForRotate then
         StatusText:=MlgStringList['Main', 79001];
        {--Brovak}
      end
      else begin
        DrawLine;
        FLastX:=X;
        FLastY:=Y;
        FActive:=FALSE;
        Cursor:=FInactiveCursor;
        if Assigned(FOnFinishedInput) then OnFinishedInput(Self);
        {--Brovak}
        if ForRotate then
            StatusText:='';
        {++Brovak}
      end;
      Result:=TRUE;
    end;
  end;

Function TRubberLine.OnMouseMove
   (
   Const X         : Double;
   Const Y         : Double;
   Shift           : TShiftState
   )
   : Boolean;
  begin
    Result:=FALSE;
    if FActive then begin
      DrawLine;
      FLastX:=X;
      FLastY:=Y;
      DrawLine;
      Result:=TRUE;
    end;
  end;

Function TRubberLine.OnMouseUp
   (
   Const X         : Double;
   Const Y         : Double;
   Button          : TMouseButton;
   Shift           : TShiftState
   )
   : Boolean;
  begin
    Result:=FALSE;
    if Button=mbLeft then begin
      if FActive and not FTwoClicks then begin
        DrawLine;
        FLastX:=X;
        FLastY:=Y;
        FActive:=FALSE;
        Cursor:=FInactiveCursor;
        if Assigned(FOnFinishedInput) then OnFinishedInput(Self);
        Result:=TRUE;
      end;
    end;
  end;

Procedure TRubberLine.SetActiveCursor
   (
   ACursor         : TCursor
   );
  begin
    FActiveCursor:=ACursor;
    if FActive then Cursor:=FActiveCursor;
  end;

Procedure TRubberLine.SetInActiveCursor
   (
   ACursor         : TCursor
   );
  begin
    FInActiveCursor:=ACursor;
    if not FActive then Cursor:=FInActiveCursor;
  end;

Procedure TRubberLine.DrawLine;
  var DX           :Double;
      DY           :Double;
  begin
    with Canvas do begin
      ActivateDrawingTools;
      InternalToWindowPoint(FStartX,FStartY,DX,DY);
      MoveTo(Round(DX),Round(DY));
      InternalToWindowPoint(FLastX,FLastY,DX,DY);
      LineTo(Round(DX),Round(DY));
      RestoreDrawingTools;
    end;
  end;

{===============================================================================
| TRubberEllipse
+==============================================================================}

Constructor TRubberEllipse.Create
   (
   AParent         : TComponent
   );
  begin
    inherited Create(AParent);
    FStatus:=inpstWaitForFirstPoint;
    Brush.Style:=bsClear;
  end;

Function TRubberEllipse.OnMouseDown
   (
   Const X         : Double;
   Const Y         : Double;
   Button          : TMouseButton;
   Shift           : TShiftState
   )
   : Boolean;
  begin
    Result:=inherited OnMouseDown(X,Y,Button,Shift);
    if not Result and (Button=mbLeft) then begin
      DrawEllipse(True);
      FPoints[FPointCnt].X:=X;
      FPoints[FPointCnt].Y:=Y;
      Inc(FPointCnt);
      FPoints[FPointCnt].X:=X;
      FPoints[FPointCnt].Y:=Y;
      CalculateEllipse;
      DrawEllipse(True);
      Result:=TRUE;
    end;
  end;
//some modifications by Brovak for BUG 539,524
Function TRubberEllipse.OnMouseMove
   (
   Const X         : Double;
   Const Y         : Double;
   Shift           : TShiftState
   )
   : Boolean;
  var
     Sq              : Double;
  begin
    Result:=inherited OnMouseMove(X,Y,Shift);
    Result:=inherited OnMouseMove(X,Y,Shift);
    if not Result then begin
      DrawEllipse(False);
     IF  NeedZoom=FALSE
      then begin;
      FPoints[FPointCnt].X:=X;
      FPoints[FPointCnt].Y:=Y;
      end;
      CalculateEllipse;
       if NeedZoom=false
       then
    begin;
      DrawEllipse(False);
{++ Sygsky 24-SEP-1999 BUG#188 BUILD#93 }
      if FPointCnt>0 then begin  // If center defined
        if DblCompare(FPrimaryAxis,0) <> 0 then begin // If Radious defined
          Sq := Pi * FPrimaryAxis / 10000. *FPrimaryAxis;
          StatusText := Format(MlgStringList['Main',11732],[FPrimaryAxis / 100., Sq]);
        end;
      end;
  {-- Sygsky 24-SEP-1999 BUG#188 BUILD#93 }
      Result:=TRUE;
    end
    else NeedZoom:=false;
    end;
  end;

Function TRubberEllipse.OnMouseUp
   (
   Const X         : Double;
   Const Y         : Double;
   Button          : TMouseButton;
   Shift           : TShiftState
   )
   : Boolean;
  begin
    Result:=inherited OnMouseUp(X,Y,Button,Shift);
    if not Result and (Button=mbLeft) then Result:=TRUE;
    if NeedZoom=true then OnMouseMove(x,y,[]);
  end;

Procedure TRubberEllipse.CalculateEllipse;
var DX           : Double;
    DY           : Double;
    Vector1      : TGrPoint;
    Vector2      : TGrPoint;
    P1           : TGrPoint;
    P2           : TGrPoint;
    AWidth       : Double;
    AHeight      : Double;
    ASize        : Double;
begin
  FPrimaryAxis:=0;
  case FType of
    retCircleCenterRadius : if FPointCnt>0 then begin
        FMiddle:=FPoints[0];
        FPrimaryAxis:=GrPointDistance(FPoints[0],FPoints[1]);
        if FPointCnt>=2 then begin
          FPointCnt:=0;
          if Assigned(FOnFinishedInput) then OnFinishedInput(Self);
          FPrimaryAxis:=0;
        end;                          
      end;
    retCircle2Points : if FPointCnt>0 then begin
        DX:=FPoints[1].X-FPoints[0].X;
        DY:=FPoints[1].Y-FPoints[0].Y;
        FPrimaryAxis:=Sqrt(DX*DX+DY*DY)/2.0;
        FMiddle.X:=FPoints[0].X+DX/2.0;
        FMiddle.Y:=FPoints[0].Y+DY/2.0;
        if FPointCnt>=2 then begin
          FPointCnt:=0;
          if Assigned(FOnFinishedInput) then OnFinishedInput(Self);
          FPrimaryAxis:=0;
        end;
      end;
    retCircle3Points : if FPointCnt>1 then begin
        Vector1:=GrPoint(-FPoints[1].Y+FPoints[0].Y,FPoints[1].X-FPoints[0].X);
        Vector2:=GrPoint(-FPoints[2].Y+FPoints[1].Y,FPoints[2].X-FPoints[1].X);
        P1:=GrPoint(FPoints[0].X+Vector1.Y/2.0,FPoints[0].Y-Vector1.X/2.0);
        P2:=GrPoint(FPoints[1].X+Vector2.Y/2.0,FPoints[1].Y-Vector2.X/2.0);
        if VectorIntersection(P1,Vector1,P2,Vector2,FMiddle)<>1 then
            FPrimaryAxis:=0.0
        else FPrimaryAxis:=GrPointDistance(FPoints[0],FMiddle);
        if FPointCnt>=3 then begin
          FPointCnt:=0;
          if Assigned(FOnFinishedInput) then OnFinishedInput(Self);
          FPrimaryAxis:=0;
        end;
      end;
    retCircle2PointsCenter : if FPointCnt>1 then begin
        Vector1:=GrPoint(-FPoints[1].Y+FPoints[0].Y,FPoints[1].X-FPoints[0].X);
        P1:=GrPoint(FPoints[0].X+Vector1.Y/2.0,FPoints[0].Y-Vector1.X/2.0);
        MovePoint(Vector1,P1.X,P1.Y);
        LineNormalIntersection(P1,Vector1,FPoints[2],FMiddle);
        FPrimaryAxis:=GrPointDistance(FPoints[0],FMiddle);
        if FPointCnt>=3 then begin
          FPointCnt:=0;
          if Assigned(FOnFinishedInput) then OnFinishedInput(Self);
          FPrimaryAxis:=0;
        end;
      end;
    retCircleInRectangle : if FPointCnt>0 then begin
        DX:=FPoints[1].X-FPoints[0].X;
        DY:=FPoints[1].Y-FPoints[0].Y;
        // calculate width and height of the rectangle
        AWidth:=DX*FCosViewRotation+DY*FSinViewRotation;
        AHeight:=DY*FCosViewRotation-DX*FSinViewRotation;
        ASize:=Max(Abs(AWidth),Abs(AHeight));
        // calculate the center-point of the circle
        if AWidth<0 then AWidth:=-ASize else AWidth:=ASize;
        if AHeight<0 then AHeight:=-ASize else AHeight:=ASize;
        FMiddle.X:=Round(FPoints[0].X+AWidth*FCosViewRotation/2.0-AHeight*FSinViewRotation/2.0);
        FMiddle.Y:=Round(FPoints[0].Y+AWidth*FSinViewRotation/2.0+AHeight*FCosViewRotation/2.0);
        // calculate the radius  
        FPrimaryAxis:=ASize/2.0;
        // calculate the second rectangle-corner
        FPoints[FPointCnt].X:=FPoints[0].X+AWidth*FCosViewRotation-AHeight*FSinViewRotation;
        FPoints[FPointCnt].Y:=FPoints[0].Y+AWidth*FSinViewRotation+AHeight*FCosViewRotation;
        if FPointCnt>=2 then begin
          FPointCnt:=0;
          if Assigned(FOnFinishedInput) then OnFinishedInput(Self);
          FPrimaryAxis:=0;
        end;
      end;
  end;
end;

Procedure TRubberEllipse.DrawEllipse(DrawHandles:Boolean);
var APrimaryAxis      : Double;
    AX                : Double;
    AY                : Double;
    Cnt               : Integer;
begin
  if FPointCnt>0 then with Canvas do begin
    ActivateDrawingTools;
    if DblCompare(FPrimaryAxis,0)<>0 then begin
      APrimaryAxis:=InternalToWindowDist(FPrimaryAxis);
      InternalToWindowPoint(FMiddle.X,FMiddle.Y,AX,AY);
      if (Abs(APrimaryAxis)+Round(Abs(AX))<MaxInt)
          and (Abs(APrimaryAxis)+Round(Abs(AY))<MaxInt) then
        Ellipse(Round(AX-APrimaryAxis),Round(AY-APrimaryAxis),
            Round(AX+APrimaryAxis),Round(AY+APrimaryAxis));
    end;
    Pen.Color:=clRed;
    Pen.Style:=psSolid;
    if (FType in [retCircle2Points,retCircle3Points,retCircle2PointsCenter,
        retCircleInRectangle]) and (DblCompare(FPrimaryAxis,0)<>0) then
      PaintControlCross(AX,AY,3);
    if (FType=retCircleInRectangle) and (FPointCnt>0) then begin
      InternalToWindowPoint(FPoints[FPointCnt].X,FPoints[FPointCnt].Y,AX,AY);
      PaintControlCross(AX,AY,3);
    end;
    if DrawHandles then begin
      for Cnt:=0 to FPointCnt-1 do begin
        InternalToWindowPoint(FPoints[Cnt].X,FPoints[Cnt].Y,AX,AY);
        PaintControlCross(AX,AY,3);
      end;
    end;
    RestoreDrawingTools;
  end;
end;

Procedure TRubberEllipse.DoPaint;
begin
  DrawEllipse(TRUE);
end;

Procedure TRubberEllipse.OnUndo(Steps:Integer);
begin
  DrawEllipse(True);
  if Steps=0 then FPointCnt:=0
  else FPointCnt:=Max(0,FPointCnt-Steps);
  CalculateEllipse;
  DrawEllipse(True);
  StatusText := '';
end;
{++brovak for create/edit circle}
Procedure TRubberEllipse.ImportNewCoords(PointCntImport:integer);
 var i:integer;
 begin;
  FPointCnt:=PointCntImport;
 // for i:=0 to FPointCnt do
 // FPoints[i]:=ArrayOfPoints[i];
  CalculateEllipse;
  DrawEllipse(true);
 end;

 procedure TRubberEllipse.SetArray(ArrayOP:TVladType);
  begin;
   FPoints:=ArrayOP;
  end;
{--brovak}

{==============================================================================+
  TRubberArc
+==============================================================================}

Constructor TRubberArc.Create
   (
   AParent         : TComponent
   );
  begin
    inherited Create(AParent);
    FStatus:=inpstWaitForFirstPoint;
    Brush.Style:=bsClear;
  end;

Function TRubberArc.OnMouseDown
   (
   Const X         : Double;
   Const Y         : Double;
   Button          : TMouseButton;
   Shift           : TShiftState
   )
   : Boolean;
  begin
    Result:=inherited OnMouseDown(X,Y,Button,Shift);
    if Button=mbLeft then begin
      DrawArc(True,False);
      X1:=X; Y1:=Y;
    if (ssShift in Shift) and (FPointCnt>=1) then
      CorrectCoordWithShiftPressed;
      FPoints[FPointCnt].X:=X1;
      FPoints[FPointCnt].Y:=Y1;
      Inc(FPointCnt);
      FPoints[FPointCnt].X:=X1;
      FPoints[FPointCnt].Y:=Y1;
      CalculateArc;
      DrawArc(True,False);
      Result:=TRUE;
    end;
  end;

 Procedure TRubberArc.CorrectCoordWithShiftPressed;
   Procedure Calculate(BasePoint:TGrPoint);
    var
    S_XDist, S_YDist: double;
    XKoeff, YKoeff: integer;
     begin;
      if (BasePoint.X = X1) or (BasePoint.Y = Y1) or (BasePoint.Y = X1) then Exit; //correction not needed
      S_XDist := X1 - BasePoint.X; S_YDist := Y1 - BasePOint.Y;
    //Detect Dekart Quarter
       if (S_XDist > 0) and (S_YDist > 0) then
      begin; XKoeff := 1; YKoeff := 1;
      end; //1st Quarter
      if (S_XDist < 0) and (S_YDist > 0) then
      begin; XKoeff := -1; YKoeff := 1;
      end; //2nd Quarter
      if (S_XDist < 0) and (S_YDist < 0) then
      begin; XKoeff := -1; YKoeff := -1;
      end; //3rd Quarter
      if (S_XDist > 0) and (S_YDist < 0) then
      begin; XKoeff := 1; YKoeff := -1;
      end; //4th Quarter
    S_XDist := abs(S_XDist); S_YDist := abs(S_YDist);
     if ((S_Xdist / S_Ydist > 0.6) and (S_Xdist / S_Ydist < 1)) then begin; //  dy>dx, near 45 grd
     Y1 := BasePoint.Y + YKoeff * S_XDist; Exit;
     end;
     if ((S_Ydist / S_Xdist > 0.6) and (S_Ydist / S_Xdist < 1)) then begin; //  dx>dy, near 45 grd
     X1 := BasePoint.X + XKoeff * S_YDist; Exit;
     end;
     if S_Xdist / S_Ydist > 1 then
      Y1 := BasePoint.Y
      else X1 := BasePoint.X

     end;
  begin;
   case FType of
     ratArcCenter:
       if FPointCnt>0 then
         Calculate(FMiddle);
    ratArc2PointsCenter,ratArc3Points:
     if FPointCnt=1 then
       Calculate(FPoints[0])
   // ratArcInRectangle
    end;
 end;


//modified by Brovak BUG 524,539 BUILD 167
Function TRubberArc.OnMouseMove
   (
   Const X         : Double;
   Const Y         : Double;
   Shift           : TShiftState
   )
   : Boolean;
  begin
    DrawArc(true,false);
    if  NeedZoom=false
     then begin;
     X1:=X; Y1:=Y;
    if (ssShift in Shift) and (FPointCnt>=1) then
    CorrectCoordWithShiftPressed;
    FPoints[FPointCnt].X:=X1;
    FPoints[FPointCnt].Y:=Y1;
          end;
    CalculateArc;
    if NeedZoom=false
       then begin;
       DrawArc(true,false);
       Result:=TRUE;
            end
      else NeedZoom:=false;
  end;

Function TRubberArc.OnMouseUp
   (
   Const X         : Double;
   Const Y         : Double;
   Button          : TMouseButton;
   Shift           : TShiftState
   )
   : Boolean;
  begin
    Result:=inherited OnMouseUp(X,Y,Button,Shift);
    if Button=mbLeft then Result:=TRUE;
    if NeedZoom=true
     then OnMouseMove(x,y,[]);
  end;

Procedure TRubberArc.CalculateArc;
  var Vector1      : TGrPoint;
      Vector2      : TGrPoint;
      P1           : TGrPoint;
      P2           : TGrPoint;
      DX           : Double;
      DY           : Double;
      AWidth       : Double;
      AHeight      : Double;
      ASize        : Double;
  begin
    FInCalculateArc:=TRUE;
    try
      FPrimaryAxis:=0;
      case FType of
        ratArcCenter : begin
            if FPointCnt=0 then FMiddle:=FPoints[0];
            if FPointCnt>=1 then FPrimaryAxis:=GrPointDistance(FPoints[0],FPoints[1]);
            if FPointCnt>=2 then FBeginAngle:=LineAngle(FPoints[0],FPoints[2]);
            if FPointCnt>=3 then FEndAngle:=LineAngle(FPoints[0],FPoints[3]);
            if FPointCnt>=4 then begin
              FEndAngle:=LineAngle(FPoints[0],FPoints[3]);
              if Assigned(FOnFinishedInput) then OnFinishedInput(Self);
              FPointCnt:=0;
              FPrimaryAxis:=0;
            end;
            {+++Brovak BUG #373}
            case FpointCnt of
            0:StatusText:=MlgStringList['Main', 80000];
            1:StatusText:=MlgStringList['Main', 80001];
            2:StatusText:=MlgStringList['Main', 80002];
            3:StatusText:=MlgStringList['Main', 80003];
              end;
            {--Brovak BUG #373}
          end;
        ratArc3Points : if FPointCnt>1 then begin
            Vector1:=GrPoint(-FPoints[1].Y+FPoints[0].Y,FPoints[1].X-FPoints[0].X);
            Vector2:=GrPoint(-FPoints[2].Y+FPoints[1].Y,FPoints[2].X-FPoints[1].X);
            P1:=GrPoint(FPoints[0].X+Vector1.Y/2.0,FPoints[0].Y-Vector1.X/2.0);
            P2:=GrPoint(FPoints[1].X+Vector2.Y/2.0,FPoints[1].Y-Vector2.X/2.0);
            if VectorIntersection(P1,Vector1,P2,Vector2,FMiddle)<>1 then
                FPrimaryAxis:=0.0
            else begin
              FPrimaryAxis:=GrPointDistance(FPoints[0],FMiddle);
              if LineNormalDistance(FPoints[0],FPoints[1],FPoints[2])<0 then begin
                FBeginAngle:=LineAngle(FMiddle,FPoints[0]);
                FEndAngle:=LineAngle(FMiddle,FPoints[1]);
              end
              else begin
                FBeginAngle:=LineAngle(FMiddle,FPoints[1]);
                FEndAngle:=LineAngle(FMiddle,FPoints[0]);
              end;  
            end;
            if FPointCnt>=3 then begin
              if Assigned(FOnFinishedInput) then OnFinishedInput(Self);
              FPointCnt:=0;
              FPrimaryAxis:=0;
            end;
          end;
        ratArc2PointsCenter : if FPointCnt>1 then begin
            Vector1:=GrPoint(-FPoints[1].Y+FPoints[0].Y,FPoints[1].X-FPoints[0].X);
            P1:=GrPoint(FPoints[0].X+Vector1.Y/2.0,FPoints[0].Y-Vector1.X/2.0);
            MovePoint(Vector1,P1.X,P1.Y);
            LineNormalIntersection(P1,Vector1,FPoints[2],FMiddle);
            FPrimaryAxis:=GrPointDistance(FPoints[0],FMiddle);
            if LineNormalDistance(FPoints[0],FPoints[1],FPoints[2])<0 then begin
              FBeginAngle:=LineAngle(FMiddle,FPoints[0]);
              FEndAngle:=LineAngle(FMiddle,FPoints[1]);
            end
            else begin
              FBeginAngle:=LineAngle(FMiddle,FPoints[1]);
              FEndAngle:=LineAngle(FMiddle,FPoints[0]);
            end;
            if FPointCnt>=3 then begin
              if Assigned(FOnFinishedInput) then OnFinishedInput(Self);
              FPointCnt:=0;
              FPrimaryAxis:=0;
            end;
          end;
        ratArcInRectangle : if FPointCnt>0 then begin
            DX:=FPoints[1].X-FPoints[0].X;
            DY:=FPoints[1].Y-FPoints[0].Y;
            // calculate width and height of the rectangle
            AWidth:=DX*FCosViewRotation+DY*FSinViewRotation;
            AHeight:=DY*FCosViewRotation-DX*FSinViewRotation;
            ASize:=Max(Abs(AWidth),Abs(AHeight));
            // calculate the center-point of the circle
            if AWidth<0 then AWidth:=-ASize else AWidth:=ASize;
            if AHeight<0 then AHeight:=-ASize else AHeight:=ASize;
            FMiddle.X:=Round(FPoints[0].X+AWidth*FCosViewRotation/2.0-AHeight*FSinViewRotation/2.0);
            FMiddle.Y:=Round(FPoints[0].Y+AWidth*FSinViewRotation/2.0+AHeight*FCosViewRotation/2.0);
            // calculate the radius
            FPrimaryAxis:=ASize/2.0;
            // calculate the second rectangle-corner
            FPoints[1].X:=FPoints[0].X+AWidth*FCosViewRotation-AHeight*FSinViewRotation;
            FPoints[1].Y:=FPoints[0].Y+AWidth*FSinViewRotation+AHeight*FCosViewRotation;
            if FPointCnt>=2 then FBeginAngle:=LineAngle(FMiddle,FPoints[2]);
            if FPointCnt>=3 then FEndAngle:=LineAngle(FMiddle,FPoints[3]);
            if FPointCnt>=4 then begin
              FEndAngle:=LineAngle(FMiddle,FPoints[3]);
              if Assigned(FOnFinishedInput) then OnFinishedInput(Self);
              FPointCnt:=0;
              FPrimaryAxis:=0;
            end;
          end;
      end;
    finally
      FInCalculateArc:=FALSE;
    end;
  end;

Procedure TRubberArc.DrawArc
   (
   DrawHandles          : Boolean;
   FromPaint            : Boolean
   );
  var APrimaryAxis      : Double;
      AX                : Double;
      AY                : Double;
      BX                : Double;
      BY                : Double;
      Cnt               : Integer;
	  DrawEllipse       : Boolean;
      DrawArc           : Boolean;
      DrawLine          : Boolean;
  begin
    if not FInCalculateArc then with Canvas do begin
      ActivateDrawingTools;
      DrawEllipse:=False;
      DrawArc:=False;
      DrawLine:=False;
      case FType of
        ratArcInRectangle,
        ratArcCenter : begin
            DrawEllipse:=(FPointCnt>0) and (FPointCnt<3);
            DrawArc:=FPointCnt>2;
            if Ftype=ratArcCenter then
            DrawLine:=FPointCnt>0
            else
            DrawLine:=FPointCnt>1;
            {+++Brovak BUG #373}
            if FType=ratArcCenter then FMiddle:=FPoints[0];
            {--Brovak BUG #373}
          end;
        ratArc2PointsCenter,
        ratArc3Points : begin;
                        DrawArc:=FPointCnt>0;
                        DrawLine:=FPointCnt=1;
                       if FpointCnt=1 then FMiddle:=FPoints[0];
                        end;

      end;


      if DrawEllipse and (DblCompare(FPrimaryAxis,0)<>0) then begin
        APrimaryAxis:=InternalToWindowDist(FPrimaryAxis);
        InternalToWindowPoint(FMiddle.X,FMiddle.Y,AX,AY);
        if (Abs(APrimaryAxis)+Round(Abs(AX))<MaxInt)
            and (Abs(APrimaryAxis)+Round(Abs(AY))<MaxInt) then
          Ellipse(Round(AX-APrimaryAxis),Round(AY-APrimaryAxis),
              Round(AX+APrimaryAxis),Round(AY+APrimaryAxis));
      end;
      if DrawArc and (DblCompare(FPrimaryAxis,0)<>0) then begin
        APrimaryAxis:=InternalToWindowDist(FPrimaryAxis);
        InternalToWindowPoint(FMiddle.X,FMiddle.Y,AX,AY);
        if (Abs(APrimaryAxis)+Round(Abs(AX))<MaxInt)
            and (Abs(APrimaryAxis)+Round(Abs(AY))<MaxInt) then
          if FChordMode then Chord(Round(AX-APrimaryAxis),Round(AY-APrimaryAxis),
              Round(AX+APrimaryAxis),Round(AY+APrimaryAxis),
              Round(AX+1000.0*Cos(FBeginAngle+FViewRotation)),Round(AY+1000.0*Sin(FBeginAngle+FViewRotation)),
              Round(AX+1000.0*Cos(FEndAngle+FViewRotation)),Round(AY+1000.0*Sin(FEndAngle+FViewRotation)))
          else
            Arc(Round(AX-APrimaryAxis),Round(AY-APrimaryAxis),
              Round(AX+APrimaryAxis),Round(AY+APrimaryAxis),
              Round(AX+1000.0*Cos(FBeginAngle+FViewRotation)),Round(AY+1000.0*Sin(FBeginAngle+FViewRotation)),
              Round(AX+1000.0*Cos(FEndAngle+FViewRotation)),Round(AY+1000.0*Sin(FEndAngle+FViewRotation)));
      end;
      if DrawLine then begin
        InternalToWindowPoint(FMiddle.X,FMiddle.Y,AX,AY);
        InternalToWindowPoint(FPoints[FPointCnt].X,FPoints[FPointCnt].Y,BX,BY);
        MoveTo(Round(AX),Round(AY));
        LineTo(Round(BX),Round(BY));
      end;
      Pen.Color:=clRed;
      Pen.Style:=psSolid;
      if (FType in [ratArc3Points,ratArc2PointsCenter,
          ratArcInRectangle]) and (DblCompare(FPrimaryAxis,0)<>0) then
        PaintControlCross(AX,AY,3);
      if (FType=ratArcInRectangle) and (FPointCnt=1) then begin
        InternalToWindowPoint(FPoints[FPointCnt].X,FPoints[FPointCnt].Y,AX,AY);
        PaintControlCross(AX,AY,3);
      end;
      if DrawHandles then for Cnt:=0 to FPointCnt-1 do begin
        InternalToWindowPoint(FPoints[Cnt].X,FPoints[Cnt].Y,AX,AY);
        PaintControlCross(AX,AY,3);
      end;
      RestoreDrawingTools;
    end;
  end;

Procedure TRubberArc.DoPaint;
begin
  DrawArc(True,True);
end;

Procedure TRubberArc.OnUndo(Steps:Integer);
begin
  DrawArc(True,False);
  if Steps=0 then begin; FPointCnt:=0; {+++Brovak #373} StatusText := '';end {--Brovak #373}
  else FPointCnt:=Max(0,FPointCnt-Steps);
  CalculateArc;
  DrawArc(True,False);
   StatusText := '';
end;


{++brovak for create/edit arcs}
Procedure TRubberArc.ImportNewCoords(PointCntImport:integer);
 var i:integer;
 begin;
  FPointCnt:=PointCntImport;
 // for i:=0 to FPointCnt do
 // FPoints[i]:=ArrayOfPoints[i];
  CalculateArc;
  DrawArc(true,true);
 end;

 procedure TRubberArc.SetArray(ArrayOP:TVladType1);
  begin;
   FPoints:=ArrayOP;
  end;
{--brovak}

{===============================================================================
| TRubberPolygon
+==============================================================================}

Destructor TRubberPolygon.Destroy;
begin
  SetCapacity(0);
  inherited Destroy;
end;

Function TRubberPolygon.GetPoint(AIndex:Integer):TGrPoint;
begin
  Result:=FPoints^[AIndex];
end;

Procedure TRubberPolygon.SetCapacity(ACapacity:Integer);
var HelpPtr      : PGrPointArray;
begin
  if ACapacity<>FCapacity then begin
    if (FCapacity>0) and (ACapacity>0) then begin
      GetMem(HelpPtr,ACapacity*SizeOf(TGrPoint));
      Move(FPoints^,HelpPtr^,Min(ACapacity,FCapacity)*SizeOf(TGrPoint));
      FreeMem(FPoints,FCapacity*SizeOf(TGrPoint));
      FPoints:=HelpPtr;
    end
    else if ACapacity>0 then GetMem(FPoints,ACapacity*SizeOf(TGrPoint))
    else FPoints:=NIL;
    FCapacity:=ACapacity;
  end;
end;

Procedure TRubberPolygon.DoPaint;
begin
  DrawPolyline(TRUE);
end;

Procedure TRubberPolygon.DrawPolyline(DrawAll:Boolean);
var DX           : Double;
    DY           : Double;
    Cnt          : Integer;
begin
  with Canvas do if FPointCount>0 then begin
    ActivateDrawingTools;
    if DrawAll then begin
      InternalToWindowPoint(FPoints^[0].X,FPoints^[0].Y,DX,DY);
      MoveTo(Round(DX),Round(DY));
      for Cnt:=0 to FPointCount-1 do begin
        InternalToWindowPoint(FPoints^[Cnt].X,FPoints^[Cnt].Y,DX,DY);
        LineTo(Round(DX),Round(DY));
      end;
    end;
    InternalToWindowPoint(FPoints^[PointCount-1].X,FPoints^[PointCount-1].Y,DX,DY);
    MoveTo(Round(DX),Round(DY));
    InternalToWindowPoint(FLastX,FLastY,DX,DY);
    LineTo(Round(DX),Round(DY));
    if FDrawClosed and (FPointCount>1) then begin
      InternalToWindowPoint(FPoints^[0].X,FPoints^[0].Y,DX,DY);
      LineTo(Round(DX),Round(DY));
    end;
    RestoreDrawingTools;
  end;
end;

Function TRubberPolygon.OnMouseDown(Const X,Y:Double;Button:TMouseButton;
    Shift:TShiftState):Boolean;
begin
  Result:=inherited OnMouseDown(X,Y,Button,Shift);
  if Button=mbLeft then begin
    if (FPointCount=0) or (DblCompare(X,FPoints^[FPointCount-1].X)<>0)
        or (DblCompare(Y,FPoints^[FPointCount-1].Y)<>0)
        or (ssDouble in Shift) then begin
      DrawPolyline(TRUE);
      if not (ssDouble in Shift) then begin
        if FPointCount=FCapacity then SetCapacity(FCapacity+8);
        FPoints^[FPointCount].X:=X;
        FPoints^[FPointCount].Y:=Y;
        Inc(FPointCount);
      end;
      if ((FMaximumPoints>1) and (FPointCount=FMaximumPoints))
          or (ssDouble in Shift) then begin
        if ssDouble in Shift then Dec(FPointCount);
        if Assigned(FOnFinishedInput) then OnFinishedInput(Self);
        FPointCount:=0;
        SetCapacity(0);
      end;
      FLastX:=X;
      FLastY:=Y;
      DrawPolyline(TRUE);
    end;
    Result:=TRUE;
  end;  
end;

Function TRubberPolygon.OnMouseMove(Const X,Y:Double;Shift:TShiftState):Boolean;
begin
  DrawPolyline(FALSE);
  FLastX:=X;
  FLastY:=Y;
  DrawPolyline(FALSE);
  Result:=TRUE;
end;

Function TRubberPolygon.OnMouseUp(Const X,Y:Double;Button:TMouseButton;
    Shift:TShiftState):Boolean;
begin
  Result:=inherited OnMouseUp(X,Y,Button,Shift);
  if Button=mbLeft then Result:=TRUE;
end;

Procedure TRubberPolygon.OnUndo(Steps:Integer);
begin
  DrawPolyline(True);
  if Steps=0 then FPointCount:=0
  else FPointCount:=Max(0,FPointCount-Steps);
  DrawPolyline(True);
end;

{==============================================================================+
  TGuideLine
+==============================================================================}

Constructor TGuideLine.Create(AParent:TComponent);
begin
  inherited Create(AParent);
  FControlSize:=3;
  FMaxDistance:=3;
  FControlPen:=TPen.Create;
  with FControlPen do Mode:=pmNotXOr;
  FOptions:=[goMove,goRotate,goMovePosition];
  FDrawIfSelected:=TRUE;
  FDrawIfNotSelected:=TRUE;
end;

Destructor TGuideLine.Destroy;
begin
  inherited Destroy;
  FControlPen.Free;
end;

Procedure TGuideLine.SetAngle(Const AAngle:Double);
begin
  if DblCompare(FAngle,AAngle)<>0 then begin
    Paint;
    FAngle:=AAngle;
    Paint;
  end;
end;

Function TGuideline.Editing:Boolean;
begin
  Result:=FMoving or FRotating or FPositionMoving;
end;

Procedure TGuideLine.SetSelected(ASelected:Boolean);
begin
  if ASelected<>FSelected then begin
    Paint;
    FSelected:=ASelected;
    Paint;
  end;
end;

Function TGuideLine.OnMouseDown(const X,Y:Double;Button:TMouseButton;Shift:TShiftState):Boolean;
var ControlPoint : Integer;
begin
  Result:=inherited OnMouseDown(X,Y,Button,Shift);
  if not Result and Visible and (Button=mbLeft) then begin
    FLastPos:=GrPoint(X,Y);
    ControlPoint:=FindControlPoint(FLastPos);
    if (goRotate in FOptions) and IntCheckRange(ControlPoint,0,1) then begin
      Paint;
      FStartPos:=FLastPos;
      FRotating:=TRUE;
      Paint;
      Result:=TRUE;
    end
    else if (goMovePosition in FOptions) and (ControlPoint=2) then begin
      Paint;
      FStartPos:=FLastPos;
      FPositionMoving:=TRUE;
      Paint;
      Result:=TRUE;
    end
    else if (goMove in FOptions) and IsPointNearLine(FLastPos) then begin
      Cursor:=MoveCursor1;
      Paint;
      FStartPos:=FLastPos;
      FMoving:=TRUE;
      Paint;
      Result:=TRUE;
    end;
  end;
end;

Function TGuideLine.FindControlPoint(Const Point:TGrPoint):Integer;
var Controls     : Array[0..5] of TGrPoint;
    Dist         : Double;
    WinX         : Double;
    WinY         : Double;
    Cnt          : Integer;
begin
  if GetControlPoints(Controls,TRUE) then begin
    InternalToWindowPoint(Point.X,Point.Y,WinX,WinY);
    Dist:=VirtControlSize;
    for Cnt:=2 to 4 do if (Abs(Controls[Cnt].X-WinX)<=Dist)
        and (Abs(Controls[Cnt].Y-WinY)<=Dist) then begin
      Result:=Cnt-2;
      Exit;
    end;
  end;
  Result:=-1;
end;

Function TGuideLine.OnMouseMove(const X,Y:Double;Shift:TShiftState):Boolean;
var CursorNumber : Integer;
    Controls     : Array[0..5] of TGrPoint; 
begin
  Result:=inherited OnMouseMove(X,Y,Shift);
  if not Result and Visible then begin
    if FMoving or FRotating or FPositionMoving then begin
      Paint;
      FLastPos:=GrPoint(X,Y);
      Paint;
      Result:=TRUE;
    end
    else begin
      FLastPos:=GrPoint(X,Y);
      CursorNumber:=FindControlPoint(FLastPos);
      if (goRotate in FOptions) and IntCheckRange(CursorNumber,0,1) then begin
        GetControlPoints(Controls,FALSE);
        CursorNumber:=Round(CursorNumber*4+8+(LineAngle(Controls[1],Controls[0])
            +FViewRotation)*4/Pi) Mod 8;
        case CursorNumber of
          0   : Cursor:=RotECursor;           
          1   : Cursor:=RotNECursor;
          2   : Cursor:=RotNCursor;
          3   : Cursor:=RotNWCursor;
          4   : Cursor:=RotWCursor;
          5   : Cursor:=RotSWCursor;
          6   : Cursor:=RotSCursor;
          7   : Cursor:=RotSECursor;
          else Cursor:=crDefault;
        end;
      end
      else if (goMovePosition in FOptions) and (CursorNumber=2) then Cursor:=MoveCursor1
      else if (goMove in Options) and IsPointNearLine(FLastPos) then begin
        CursorNumber:=Round(8+(FAngle+FViewRotation)*4/Pi) Mod 8;
        case CursorNumber of
          0,4 : Cursor:=SizeNSCursor;
          1,5 : if true {Height<0} then Cursor:=SizeNESWCursor
                else Cursor:=SizeSENWCursor;
          2,6 : Cursor:=SizeEWCursor;
          3,7 : if true {Height<0} then Cursor:=SizeSENWCursor
                else Cursor:=SizeNESWCursor;
        end;
      end
      else Cursor:=crDefault;
    end;
  end;  
end;

Function TGuideLine.OnMouseUp(const X,Y:Double;Button:TMouseButton;Shift:TShiftState):Boolean;
var Position     : TGrPoint;
    Angle        : Double;
begin
  Result:=inherited OnMouseUp(X,Y,Button,Shift);
  if not Result and Visible and (Button=mbLeft) then begin
    if FMoving then begin
      Paint;
      GetCurrentPosition(Position,Angle);
      FMoving:=FALSE;
      FPosition:=Position;
      if Assigned(FOnMove) then OnMove(Self);
      Paint;
      Result:=TRUE;
    end
    else if FPositionMoving then begin
      Paint;
      GetCurrentPosition(Position,Angle);
      FPositionMoving:=FALSE;
      FPosition:=Position;
      if Assigned(FOnMovePosition) then OnMovePosition(Self);
      Paint;
      Result:=TRUE;
    end
    else if FRotating then begin
      Paint;
      GetCurrentPosition(Position,Angle);
      FRotating:=FALSE;
      FAngle:=Angle;
      if Assigned(FOnRotate) then OnRotate(Self);
      Paint;
      Result:=TRUE;
    end;
  end;  
end;

Procedure TGuideLine.DoPaint;
var Controls     : Array[0..5] of TGrPoint;
    DrawIt       : Boolean;
begin
  if FSelected then DrawIt:=FDrawIfSelected
  else DrawIt:=FDrawIfNotSelected;
  if DrawIt and GetControlPoints(Controls,TRUE) then with Canvas do begin
    ActivateDrawingTools;
    // paint line
    if FSelected then Pen.Color:=clRed
    else Pen.Color:=clBlue;
    MoveTo(Round(Controls[0].X),Round(Controls[0].Y));
    LineTo(Round(Controls[1].X),Round(Controls[1].Y));
    Pen.Assign(FControlPen);
    Brush.Style:=bsClear;
    if FSelected then begin
      // paint control-markers if not moving or rotating
      if not FMoving and not FRotating and not FPositionMoving then begin
        // paint rotating-circles
        if goRotate in FOptions then begin
          PaintControlCircle(Controls[2].X,Controls[2].Y,FControlSize);
          PaintControlCircle(Controls[3].X,Controls[3].Y,FControlSize);
        end;
      end;
      if goMovePosition in FOptions then PaintControlRect(Controls[4].X,Controls[4].Y,FControlSize,FALSE);
    end;
    RestoreDrawingTools;
  end;
end;

Procedure TGuideLine.SetPosition(Const APosition:TGrPoint);
begin
  if not GrPointsEqual(FPosition,APosition) then begin
    Paint;
    FPosition:=APosition;
    Paint;
  end;
end;

Function TGuideLine.VirtControlSize:Double;
var APoint       : TPoint;
begin
  APoint:=Point(FControlSize,FControlSize);
  DPToLP(Canvas.Handle,APoint,1);
  Result:=APoint.X;
end;

Function TGuideLine.IntMaxDistance:Double;
var APoint       : TPoint;
begin
  APoint:=Point(FMaxDistance,FMaxDistance);
  DPToLP(Canvas.Handle,APoint,1);
  Result:=WindowToInternalDist(APoint.X);
end;

Function TGuideLine.IsPointNearLine(Const Point:TGrPoint):Boolean;
begin
  Result:=Abs(VectorNormalDistance(FPosition,GrPoint(Cos(FAngle+FViewRotation),
      Sin(Angle+FViewRotation)),Point))<IntMaxDistance;
end;

{******************************************************************************+
  Procedure TGuideLine.GetLinePoints
--------------------------------------------------------------------------------
  Calculates the intersection-points of the guideline with the window-border.
  the points are given in window-coordinates. The function returns false
  if the guideline is outside the window-rect.
*******************************************************************************}
Function TGuideLine.GetControlPoints(var Points:Array of TGrPoint;All:Boolean):Boolean;
var WinPoint     : TGrPoint;
    CurPosition  : TGrPoint;
    WinRect      : TRect;
    Distance     : Double;
    Angle        : Double;
    Length       : Double;
begin              
  GetCurrentPosition(CurPosition,Angle);
  { calculate position in window-coordinates }
  InternalToWindowPoint(CurPosition.X,CurPosition.Y,WinPoint.X,WinPoint.Y);
  { calculate a line surely intersecting the window-rectangle }
  WinRect:=Parent.ClientRect;
  DPToLP(Canvas.Handle,WinRect,2);
  Result:=ClipVectorToRect(GrPoint(WinPoint.X,WinPoint.Y),GrPoint(Cos(Angle),
      Sin(Angle)),GrRect(WinRect.Left,WinRect.Bottom,WinRect.Right,WinRect.Top),
      Points[0],Points[1]);
  if Result and All then begin
    { calculate rotation-points }
    Angle:=LineAngle(Points[0],Points[1]);                
    Distance:=VirtControlSize*3;
    if DblCompare(Points[0].X,WinRect.left)
        *DblCompare(Points[0].X,WinRect.Right)=0 then
      Length:=Abs(Distance/Cos(Angle))
    else Length:=Abs(Distance/Sin(Angle));
    Points[2]:=Points[0];
    MovePointAngle(Points[2],Length,Angle);
    if DblCompare(Points[1].X,WinRect.left)
        *DblCompare(Points[1].X,WinRect.Right)=0 then
      Length:=Abs(Distance/Cos(Angle))
    else Length:=Abs(Distance/Sin(Angle));
    Points[3]:=Points[1];
    MovePointAngle(Points[3],-Length,Angle);
    // calculate position-marker-position
    InternalToWindowPoint(CurPosition.X,CurPosition.Y,WinPoint.X,WinPoint.Y);
    Points[4]:=WinPoint;
  end;
end;

Procedure TGuideLine.GetCurrentPosition(var Position:TGrPoint;var Angle:Double);
begin
  Position:=FPosition;
  Angle:=FAngle+FViewRotation;
  if FMoving then begin
    { move the position along a normal to the line }
    MovePointAngle(Position,VectorNormalDistance(FPosition,GrPoint(Cos(Angle),
        Sin(Angle)),FLastPos)-VectorNormalDistance(FPosition,GrPoint(Cos(Angle),
        Sin(Angle)),FStartPos),Angle+Pi/2);
  end;
  if FPositionMoving then VectorNormalIntersection(FPosition,GrPoint(Cos(Angle),
      Sin(Angle)),FLastPos,Position);
  if FRotating then Angle:=Angle+LineAngle(Position,FLastPos)-LineAngle(Position,FStartPos);
end;

{******************************************************************************+
  Function TGuideLine.CurrentAngle
--------------------------------------------------------------------------------
  Returns the current alngle of the guideline. If the guideline is currently
  being rotated this value differs from the angle-property.
+******************************************************************************}
Function TGuideLine.CurrentAngle:Double;
var Position     : TGrPoint;
begin
  GetCurrentPosition(Position,Result);
end;

{******************************************************************************+
  Function TGuideLine.CurrentPosition
--------------------------------------------------------------------------------
  Returns the current position. If the guideline is currently moved this
  value differs from the position-property
+******************************************************************************}
Function TGuideLine.CurrentPosition:TGrPoint;
var Angle        : Double;
begin
  GetCurrentPosition(Result,Angle);
end;

Function TGuideLine.LineEndPoints(var Point1,Point2:TGrPoint):Boolean;
var Controls       : Array[0..5] of TGrPoint;
begin
  if GetControlPoints(Controls,FALSE) then begin
    Point1:=Controls[0];
    Point2:=Controls[1];
    Result:=TRUE;
  end
  else Result:=FALSE;
end;

{===============================================================================
  TRubberGrid
+==============================================================================}

Constructor TRubberGrid.Create
   (
   AOwner          : TComponent
   );
  begin
    inherited Create(AOwner);
  end;

Destructor TRubberGrid.Destroy;
  begin
    inherited Destroy;
  end;    
   
Function TRubberGrid.OnMouseDown
   (
   Const X         : Double;
   Const Y         : Double;
   Button          : TMouseButton;
   Shift           : TShiftState
   )
   : Boolean;
  begin
    Result:=inherited OnMouseDown(X,Y,Button,Shift);
  end;  

Function TRubberGrid.OnMouseMove
   (
   Const X         : Double;
   Const Y         : Double;
   Shift           : TShiftState
   )
   : Boolean;
  begin
    Result:=inherited OnMouseMove(X,Y,Shift);
  end;

Function TRubberGrid.OnMouseUp
   (
   Const X         : Double;
   Const Y         : Double;
   Button          : TMouseButton;
   Shift           : TShiftState
   )
   : Boolean;
  begin
    Result:=inherited OnMouseUp(X,Y,Button,Shift);
  end;

Procedure TRubberGrid.SetColAlign
   (
   AAlign          : TGridAlignment
   );
  begin
    if FColAlign<>AAlign then begin
      FrameVisible:=FALSE;
      FColAlign:=AAlign;
      RecalculateGrid;
      FrameVisible:=TRUE;
    end;
  end;

Procedure TRubberGrid.SetColCount
   (
   ACount          : Integer
   );
  begin
    if FColCount<>ACount then begin
      FrameVisible:=FALSE;
      FColCount:=ACount;
      RecalculateGrid;
      FrameVisible:=TRUE;
    end;
  end;

Procedure TRubberGrid.SetColWidth
   (
   const AWidth    : Double
   );
  begin
    if DblCompare(FColWidth,AWidth)<>0 then begin
      FrameVisible:=FALSE;
      FColWidth:=AWidth;
      RecalculateGrid;
      FrameVisible:=TRUE;
    end;
  end;
   
Procedure TRubberGrid.SetRowAlign
   (
   AAlign          : TGridAlignment
   );
  begin
    if FRowAlign<>AAlign then begin
      FrameVisible:=FALSE;
      FRowAlign:=AAlign;
      RecalculateGrid;
      FrameVisible:=TRUE;
    end;
  end;

Procedure TRubberGrid.SetRowCount
   (
   ACount          : Integer
   );
  begin
    if FRowCount<>ACount then begin
      FrameVisible:=FALSE;
      FRowCount:=ACount;
      RecalculateGrid;
      FrameVisible:=TRUE;
    end;  
  end;
   
Procedure TRubberGrid.SetRowHeight
   (
   const AHeight   : Double
   );
  begin
    if DblCompare(FRowHeight,AHeight)<>0 then begin
      FrameVisible:=FALSE;
      FRowHeight:=AHeight;
      RecalculateGrid;
      FrameVisible:=TRUE;
    end;
  end;

{******************************************************************************+
  Procedure TRubberGrid.DoPaintInterior
--------------------------------------------------------------------------------
  Called from TEditHandler.PaintFrame to draw the additional interior of
  the input-handler.
+******************************************************************************}
Procedure TRubberGrid.DoPaintInterior;
  begin
  end;

{******************************************************************************+
  Procedure TRubberGrid.RecalculateGrid
--------------------------------------------------------------------------------
  Recalculates the parameters for drawing the grid.
+******************************************************************************}
Procedure TRubberGrid.RecalculateGrid;
  begin
  end;

end.
