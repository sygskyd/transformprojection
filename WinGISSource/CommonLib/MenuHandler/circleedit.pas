Unit CircleEdit;
//Edit objects mode for Circles
//by Brovak - used PolyEdit logic
//in test mode now - please son't change it without notify of me brovak@progis.ru

Interface

Uses Classes,Controls,EditHndl1,Graphics,GrTools,MenuFn,Dialogs;

Type  PCircleEditor     = ^TCircleEditor;
      TCircleEditor   = Class(TEditHandler1)
      Private
       FClosed          : Boolean;
       FHandleSize      : Integer;
       FLastPoint       : Integer;
       FLastX           : Double;
       FLastY           : Double;
       FLastX1          : Double;
       FLastY1          : Double;
       FPoly            : TGrPointList;
       FTempPoly        : TGrPointList;
       FMousePoint      : Integer;
       FMoving          : Boolean;
       FMovingPoints    : Boolean;
       FSelected        : TList;
       OldSin,OLdCos:double;
       Procedure   DeSelectAll;
       Procedure   DeSelectPoint(AIndex:Integer);
       Function    FindPoint(Const X,Y:Double):LongInt;
       Procedure   MoveSelectedPoints(Const X,Y:Double);
       Function    PointSelected(AIndex:LongInt):Boolean;
       Procedure   RefreshSize;
       Procedure   SelectPoint(AIndex:Integer);
       Function    VirtualHandleSize:Integer;
      Protected
       Procedure   DoMove(Const AXMove,AYMove:Double); override;
       Procedure   DoPaint; override;
       Procedure   DoRotate(Const AX,AY,AAngle:Double); override;
       Procedure   DoResize(Const AX,AY,AWidth,AHeight:double; var AOldWidth,AOldHeight:Double); override;
       Procedure   SetItem(const AItem:TGrPointList);
      Public
       Middle     : TGrPoint;
       Radius     : LongInt;
       Constructor Create(AParent:TComponent); override;
       Destructor  Destroy; override;
       Property    Closed:Boolean read FClosed write FClosed;
       Property    HandleSize:Integer read FHandleSize write FHandleSize;
       Procedure   OnEnableMenus; override;
       Function    OnMouseDown(Const X,Y:Double;Button:TMouseButton;Shift:TShiftState):Boolean; override;
       Function    OnMouseMove(Const X,Y:Double;Shift:TShiftState):Boolean; override;
       Function    OnMouseUp(Const X,Y:Double;Button:TMouseButton;Shift:TShiftState):Boolean; override;
       Property    Polygon:TGrPointList read FPoly write SetItem;
    
     end;

Implementation

Uses NumTools,UserIntf,Windows;

Constructor TCircleEditor.Create(AParent:TComponent);
begin
  inherited Create(AParent);
  HandleSize:=3;
  FSelected:=TList.Create;
  FTempPoly:=TGrPointList.Create;
  FPoly:=TGrPointList.Create;
  Extend:=0;
  MinimalMove:=0;
//  TypeOfObject:=1; //1-Polygon or Polyline;
end;

Destructor TCircleEditor.Destroy;
begin
  FPoly.Free;
  FTempPoly.Free;
  FSelected.Free;
  inherited Destroy;

end;

Procedure TCircleEditor.SetItem(const AItem:TGrPointList);
begin
  Visible:=FALSE;
  FPoly.Free;
  FPoly:=AItem;
  RefreshSize;
  FLastPoint:=-1;
  FMousePoint:=-1;
  Visible:=TRUE;
end;

Procedure TCircleEditor.DoPaint;
var Cnt            : Integer;
    Point          : TGrPoint;
    Radius2        :Double;
    AX,AY :Double;
begin
  with Canvas do begin
    ActivateDrawingTools;
    try
    if DblCompare(Radius,0)<>0 then begin
      Radius2:=InternalToWindowDist(Radius);
      InternalToWindowPoint(Middle.X,Middle.Y,AX,AY);
       if (Abs(Radius2)+Round(Abs(AX))<MaxInt)
          and (Abs(Radius2)+Round(Abs(AY))<MaxInt) then
        Ellipse(Round(AX-Radius2),Round(AY-Radius2),
            Round(AX+Radius2),Round(AY+Radius2));
    end;
        Pen.Style:=psSolid;
        Pen.Color:=clBlack;
{        if Count>0 then PaintControlRect(Round(Points[0].X),Round(Points[0].Y),
            FHandleSize+1,PointSelected(0));
        for Cnt:=1 to Count-1 do PaintControlRect(Round(Points[Cnt].X),Round(Points[Cnt].Y),
            FHandleSize,PointSelected(Cnt));}

    finally
      RestoreDrawingTools;



{


    try
      FTempPoly.Assign(FPoly);
      with FTempPoly do begin
        for Cnt:=0 to Count-1 do begin
          InternalToWindowPoint(Points[Cnt].X,Points[Cnt].Y,Point.X,Point.Y);
          Points[Cnt]:=Point;
        end;
        if Count>0 then begin
          MoveTo(Round(Points[0].X),Round(Points[0].Y));
          for Cnt:=1 to Count-1 do LineTo(Round(Points[Cnt].X),Round(Points[Cnt].Y));
          if FClosed then LineTo(Round(Points[0].X),Round(Points[0].Y));
        end;
        Pen.Style:=psSolid;
        Pen.Color:=clBlack;
        if Count>0 then PaintControlRect(Round(Points[0].X),Round(Points[0].Y),
            FHandleSize+1,PointSelected(0));
        for Cnt:=1 to Count-1 do PaintControlRect(Round(Points[Cnt].X),Round(Points[Cnt].Y),
            FHandleSize,PointSelected(Cnt));
      end;
    finally
      RestoreDrawingTools;}
    end;
  end;
end;

Function TCircleEditor.OnMouseDown(Const X,Y:Double;Button:TMouseButton;
    Shift:TShiftState):Boolean;
var APoint       : LongInt;
    Cnt          : Integer;
begin
  Result:=inherited OnMouseDown(X,Y,Button,Shift);
end;


Function TCircleEditor.OnMouseUp(Const X,Y:Double;Button:TMouseButton;
    Shift:TShiftState):Boolean;
begin
//bro
Result:=inherited OnMouseUp(X,Y,Button,Shift);
end;

Procedure TCircleEditor.MoveSelectedPoints(Const X,Y:Double);
var Cnt          : Integer;
    XMove        : Double;
    YMove        : Double;
begin
  XMove:=X-FLastX;
  YMove:=Y-FLastY;
  for Cnt:=0 to FSelected.Count-1 do MovePoint(FPoly.PointPtrs[
      LongInt(FSelected[Cnt])]^,XMove,YMove);
  FLastX:=X;
  FLastY:=Y;
end;

Function TCircleEditor.OnMouseMove(Const X,Y:Double;Shift:TShiftState):Boolean;
var AMinMove     : Double;
    APoint       : Integer;
    Point        : TGrPoint;
    Cnt:integer;
begin
Result:= inherited OnMouseMove(X,Y,Shift);
if ssleft in Shift
then
 begin;
FlastX:=X;
FlastY:=Y;
end;
end;

Function TCircleEditor.VirtualHandleSize:Integer;
var APoint       : TPoint;
begin
  APoint:=Point(FHandleSize,FHandleSize);
  DPToLP(Canvas.Handle,APoint,1);
  Result:=APoint.X;
end;

Function TCircleEditor.PointSelected(AIndex:LongInt):Boolean;
var Cnt          : Integer;
begin
  for Cnt:=0 to FSelected.Count-1 do if LongInt(FSelected[Cnt])=AIndex then begin
    Result:=TRUE;
    Exit;
  end;
  Result:=FALSE;

end;

Function TCircleEditor.FindPoint(Const X,Y:Double):LongInt;
var ASize        : Double;
    APoint       : TGrPoint;
begin
  ASize:=WindowToInternalDist(VirtualHandleSize);
  for Result:=0 to FPoly.Count-1 do begin
    APoint:=FPoly[Result];
    if (Abs(APoint.X-X)<=ASize) and (Abs(APoint.Y-Y)<=ASize) then Exit;
  end;
  Result:=-1;
end;

Procedure TCircleEditor.DoResize(Const AX,AY,AWidth,AHeight:double; var AOldWidth,AOldHeight:Double);
var
    Scale      : Double;
    XMove        : Double;
    YMove        : Double;
    Cnt          : Integer;
    ClipRect     : TGrRect;
    APoint       : PGrPoint;
    TmpPOint:TGrPoint;
begin
 Visible:=FALSE;

  inherited DoResize(AX,AY,AWidth,AHeight,AOldWidth,AOldHeight);

    Scale:=AWidth/AOldWidth;

     //APoint^.X:=(APoint^.X-XMove)*X1Scale+TmpPOint.X;
     // APoint^.Y:=(APoint^.Y-YMove)*Y1Scale+TmpPoint.Y;
//    XMove:=Middle.X-Radius;
 //   YMove:=Middle.Y-Radius;


    Radius:=round(Radius*Scale);
    Middle.X:=XPosition+Radius;
    Middle.Y:=YPOsition+Radius;
    {Y1Scale:=AHeight/AOldHeight;



     for Cnt:=0 to Count-1 do begin
     APoint:=PointPtrs[Cnt];
     RotatePoint(Apoint^,-Fangle);
     end;

       TmpPOint.X:=AX;
       TmpPOint.Y:=AY;

      TmpPOint:=RotatedPoint(TmpPOint,-Fangle);

      ClipRect:=Extent;
      XMove:=ClipRect.Left;
      YMove:=ClipRect.Bottom;

      for Cnt:=0 to Count-1 do begin
      APoint:=PointPtrs[Cnt];
      APoint^.X:=(APoint^.X-XMove)*X1Scale+TmpPOint.X;
      APoint^.Y:=(APoint^.Y-YMove)*Y1Scale+TmpPoint.Y;
                               end;
     for Cnt:=0 to Count-1 do begin
     APoint:=PointPtrs[Cnt];
     RotatePoint(Apoint^,Fangle);
     end;
 end;}
Visible:=true;
end;



Procedure TCircleEditor.DoMove(Const AXMove,AYMove:Double);
var Cnt          : Integer;
begin

  Visible:=FALSE;
  inherited DoMove(AXMove,AYMove);

    Middle.X:=Middle.X+AXMove;
    Middle.Y:=Middle.Y+AYMove;

  Visible:=TRUE;
end;

Procedure TCircleEditor.OnEnableMenus;
begin
(*  MenuFunctions.GroupEnabled[1100]:=FSelected.Count>0;
  MenuFunctions.GroupEnabled[1101]:=FSelected.Count=1;
  MenuFunctions.GroupEnabled[1102]:=(FSelected.Count=1)
      and not PointSelected(0);
      *)
end;

Procedure TCircleEditor.DeSelectAll;
var APoint       : TGrPoint;
    Cnt          : Integer;
begin

  if (FSelected.Count>0) and Visible then with Canvas,FPoly do begin
    Pen:=Self.Pen;
    Brush:=Self.Brush;
    Pen.Style:=psSolid;
    Pen.Color:=clBlack;
    for Cnt:=0 to FSelected.Count-1 do begin
      APoint:=FPoly[LongInt(FSelected[Cnt])];
      InternalToWindowPoint(APoint.X,APoint.Y,APoint.X,APoint.Y);
      PaintControlRect(APoint.X,APoint.Y,FHandleSize+Byte(LongInt(FSelected[Cnt])=0),TRUE);
      PaintControlRect(APoint.X,APoint.Y,FHandleSize+Byte(LongInt(FSelected[Cnt])=0),FALSE);
    end;
    FSelected.Clear;
  end;

end;

Procedure TCircleEditor.DeSelectPoint(AIndex:Integer);
var APoint       : TGrPoint;
begin
(*
  if PointSelected(AIndex) and Visible then with Canvas do begin
    Pen:=Self.Pen;
    Brush:=Self.Brush;
    Pen.Style:=psSolid;
    Pen.Color:=clBlack;
    APoint:=FPoly[AIndex];
    InternalToWindowPoint(APoint.X,APoint.Y,APoint.X,APoint.Y);
    PaintControlRect(APoint.X,APoint.Y,FHandleSize+Byte(AIndex=0),TRUE);
    PaintControlRect(APoint.X,APoint.Y,FHandleSize+Byte(AIndex=0),FALSE);
    FSelected.Remove(Pointer(AIndex));
  end;
 *)
  end;

Procedure TCircleEditor.SelectPoint(AIndex:Integer);
var APoint       : TGrPoint;
begin
 (* if not PointSelected(AIndex) and Visible then with Canvas do begin
    Pen:=Self.Pen;
    Brush:=Self.Brush;
    Pen.Style:=psSolid;
    Pen.Color:=clBlack;
    APoint:=FPoly[AIndex];
    InternalToWindowPoint(APoint.X,APoint.Y,APoint.X,APoint.Y);
    PaintControlRect(APoint.X,APoint.Y,FHandleSize+Byte(AIndex=0),FALSE);
    PaintControlRect(APoint.X,APoint.Y,FHandleSize+Byte(AIndex=0),TRUE);
    FSelected.Add(Pointer(AIndex));
  end;
   *)
end;



Procedure TCircleEditor.RefreshSize;
var Rect           : TGrRect;
    Cnt            : Integer;
begin
  Rect:=FPoly.Extent;
  Width:=RectWidth(Rect);
  Height:=RectHeight(Rect);
  XPosition:=Rect.Left;
  YPosition:=Rect.Bottom;
end;

Procedure TCircleEditor.DoRotate(Const AX,AY,AAngle:Double);
var Center         : TGrPoint;
    Cnt            : Integer;
    SinAngle       : Double;
    CosAngle       : Double;
    Point          : TGrPoint;
    NewPoint       : TGrPoint;
begin
{  Visible:=FALSE;
  inherited DoRotate(AX,AY,AAngle);

  with FPoly do begin
    SinCos(AAngle,SinAngle,CosAngle);
     Center:=RectCenter(Extent);
     Center.X:= FFrameRectCenterX;
     Center.Y:=FFrameRectCenterY;
     for Cnt:=0 to Count-1 do with Point do begin
      Point:=MovedPoint(Points[Cnt],-Center.X,-Center.Y);
      NewPoint.X:=Center.X+X*CosAngle-Y*SinAngle;
      NewPoint.Y:=Center.Y+X*SinAngle+Y*CosAngle;
      Points[Cnt]:=NewPoint;
    end;
  end;
  Visible:=TRUE;}
 end;
end.

