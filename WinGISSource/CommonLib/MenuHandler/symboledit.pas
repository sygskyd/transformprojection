Unit Symboledit;
//Edit objects mode for Arc
//by Brovak - used PolyEdit logic
//in test mode now - please son't change it without notify of me brovak@progis.ru
Interface

Uses Classes,Controls,EditHndl1,Graphics,GrTools,MenuFn,Dialogs;

Type  PSymboleditor     = ^TSymboleditor;
      TSymboleditor   = Class(TEditHandler1)
      Private
       FClosed          : Boolean;
       FHandleSize      : Integer;
       FLastPoint       : Integer;
       FLastX           : Double;
       FLastY           : Double;
       FLastX1          : Double;
       FLastY1          : Double;
       FPoly            : TGrPointList;
       FTempPoly        : TGrPointList;
       FMousePoint      : Integer;
       FMoving          : Boolean;
       FMovingPoints    : Boolean;
       FSelected        : TList;
       OldSin,OLdCos:double;
       Procedure   DeSelectAll;
       Procedure   DeSelectPoint(AIndex:Integer);
       Function    FindPoint(Const X,Y:Double):LongInt;
       Procedure   MoveSelectedPoints(Const X,Y:Double);
       Function    PointSelected(AIndex:LongInt):Boolean;
       Procedure   RefreshSize;
       Procedure   SelectPoint(AIndex:Integer);
       Function    VirtualHandleSize:Integer;
      Protected
       Procedure   DoMove(Const AXMove,AYMove:Double); override;
       Procedure   DoPaint; override;
       Procedure   DoRotate(Const AX,AY,AAngle:Double); override;
       Procedure   DoResize(Const AX,AY,AWidth,AHeight:double; var AOldWidth,AOldHeight:Double); override;
       Procedure   SetItem(const AItem:TGrPointList);
      Public
       ObjectTypeForEdit : integer;
       SRefposition: TGrPoint;
       Center:  TGrPoint;
       Radius     : LongInt;
       BegAng     : Real48;
       EndAng      : Real48;
       AChordMode  : boolean;
       FViewRotation : Double;
       Constructor Create(AParent:TComponent); override;
       Destructor  Destroy; override;
       Property    Closed:Boolean read FClosed write FClosed;
       Property    HandleSize:Integer read FHandleSize write FHandleSize;
       Procedure   OnEnableMenus; override;
       Function    OnMouseDown(Const X,Y:Double;Button:TMouseButton;Shift:TShiftState):Boolean; override;
       Function    OnMouseMove(Const X,Y:Double;Shift:TShiftState):Boolean; override;
       Function    OnMouseUp(Const X,Y:Double;Button:TMouseButton;Shift:TShiftState):Boolean; override;
       Property    Polygon:TGrPointList read FPoly write SetItem;
    
     end;

Implementation

Uses NumTools,UserIntf,Windows;

Constructor TSymboleditor.Create(AParent:TComponent);
begin
  inherited Create(AParent);
  HandleSize:=3;
  FSelected:=TList.Create;
  FTempPoly:=TGrPointList.Create;
  FPoly:=TGrPointList.Create;
  Extend:=0;
  MinimalMove:=0;
//  TypeOfObject:=1; //1-Polygon or Polyline;
end;

Destructor TSymboleditor.Destroy;
begin
  FPoly.Free;
  FTempPoly.Free;
  FSelected.Free;
  inherited Destroy;

end;

Procedure TSymboleditor.SetItem(const AItem:TGrPointList);
begin
  Visible:=FALSE;
  FPoly.Free;
  FPoly:=AItem;
  RefreshSize;
  FLastPoint:=-1;
  FMousePoint:=-1;
  Visible:=TRUE;
end;

Procedure TSymboleditor.DoPaint;
var Cnt            : Integer;
    Point          : TGrPoint;
begin

  with Canvas do begin
    ActivateDrawingTools;
    try
      FTempPoly.Assign(FPoly);
//      Rect:=Parent.ClientRect;
//      DPToLP(Handle,Rect,2);
//      ClipPolygon(FTempPoly,Rect);
      with FTempPoly do begin
        for Cnt:=0 to Count-1 do begin
          InternalToWindowPoint(Points[Cnt].X,Points[Cnt].Y,Point.X,Point.Y);
          Points[Cnt]:=Point;
        end;
        if Count>0 then begin
          MoveTo(Round(Points[0].X),Round(Points[0].Y));
          for Cnt:=1 to Count-1 do LineTo(Round(Points[Cnt].X),Round(Points[Cnt].Y));
          if FClosed then LineTo(Round(Points[0].X),Round(Points[0].Y));
        end;
        Pen.Style:=psSolid;
        Pen.Color:=clBlack;
//        if Count>0 then PaintControlRect(Round(Points[0].X),Round(Points[0].Y),
  //          FHandleSize+1,PointSelected(0));
    //    for Cnt:=1 to Count-1 do PaintControlRect(Round(Points[Cnt].X),Round(Points[Cnt].Y),
      //      FHandleSize,PointSelected(Cnt));
      end;
    finally
      RestoreDrawingTools;
    end;
  end;
end;

Function TSymboleditor.OnMouseDown(Const X,Y:Double;Button:TMouseButton;
    Shift:TShiftState):Boolean;
var APoint       : LongInt;
    Cnt          : Integer;
begin
  Result:=inherited OnMouseDown(X,Y,Button,Shift);
end;


Function TSymboleditor.OnMouseUp(Const X,Y:Double;Button:TMouseButton;
    Shift:TShiftState):Boolean;
begin
//bro
Result:=inherited OnMouseUp(X,Y,Button,Shift);
end;

Procedure TSymboleditor.MoveSelectedPoints(Const X,Y:Double);
var Cnt          : Integer;
    XMove        : Double;
    YMove        : Double;
begin
  XMove:=X-FLastX;
  YMove:=Y-FLastY;
  for Cnt:=0 to FSelected.Count-1 do MovePoint(FPoly.PointPtrs[
      LongInt(FSelected[Cnt])]^,XMove,YMove);
  FLastX:=X;
  FLastY:=Y;
end;

Function TSymboleditor.OnMouseMove(Const X,Y:Double;Shift:TShiftState):Boolean;
var AMinMove     : Double;
    APoint       : Integer;
    Point        : TGrPoint;
    Cnt:integer;
begin
Result:= inherited OnMouseMove(X,Y,Shift);
if ssleft in Shift
then
 begin;
FlastX:=X;
FlastY:=Y;
end;
end;

Function TSymboleditor.VirtualHandleSize:Integer;
var APoint       : TPoint;
begin
  APoint:=Point(FHandleSize,FHandleSize);
  DPToLP(Canvas.Handle,APoint,1);
  Result:=APoint.X;
end;

Function TSymboleditor.PointSelected(AIndex:LongInt):Boolean;
var Cnt          : Integer;
begin
  for Cnt:=0 to FSelected.Count-1 do if LongInt(FSelected[Cnt])=AIndex then begin
    Result:=TRUE;
    Exit;
  end;
  Result:=FALSE;

end;

Function TSymboleditor.FindPoint(Const X,Y:Double):LongInt;
var ASize        : Double;
    APoint       : TGrPoint;
begin
  ASize:=WindowToInternalDist(VirtualHandleSize);
  for Result:=0 to FPoly.Count-1 do begin
    APoint:=FPoly[Result];
    if (Abs(APoint.X-X)<=ASize) and (Abs(APoint.Y-Y)<=ASize) then Exit;
  end;
  Result:=-1;
end;

Procedure TSymboleditor.DoResize(Const AX,AY,AWidth,AHeight:double; var AOldWidth,AOldHeight:Double);
var
    Scale,Scale1: Double;
    XMove        : Double;
    YMove        : Double;
    Cnt          : Integer;
    ClipRect     : TGrRect;
    APoint       : PGrPoint;
    TmpPOint:TGrPoint;
begin
 Visible:=FALSE;

  inherited DoResize(AX,AY,AWidth,AHeight,AOldWidth,AOldHeight);

 with Fpoly  do begin;

    Scale:=AWidth/AOldWidth;
    Scale1:=AHeight/AOldHeight;

    for Cnt:=0 to Count-1 do begin;
     APoint:=PointPtrs[Cnt];
     RotatePoint(APoint^,-Fangle)
                             end;
    RotatePoint(SRefPosition,-Fangle);

//   ClipRect:=Extent;
  //    XMove:=ClipRect.Left;
    //  YMove:=ClipRect.Bottom;


    TmpPoint.X:=AX;
    TmpPoint.Y:=AY;

    TmpPoint:= RotatedPoint(TmpPoint,-Fangle);



   //  Center.X:= XPosition-Width/2;
   //  Center.Y:= YPosition-Height/2;

 //RotatePoint(Center,-Fangle);

    ClipRect:=Extent;
    XMove:=Cliprect.Left;
//   if ObjectTypeForEdit=0 then
 //   YMove:= Cliprect.Top ;
  //  else
   YMove:= Cliprect.Bottom;
  

    for Cnt:=0 to Count-1 do begin;
      APoint:=PointPtrs[Cnt];
      APoint.X:=(APoint^.X-XMove)*Scale+TmpPoint.X;
      APoint.Y:=(APoint^.Y-YMove)*Scale1+TmpPoint.Y;
                             end;




    for Cnt:=0 to Count-1 do begin;
     APoint:=PointPtrs[Cnt];
     RotatePoint(APoint^,Fangle)
                             end;

   SRefPosition.X:=(SRefPosition.X-XMove)*Scale+TmpPOint.X;
   SRefPosition.Y:=(SRefPosition.Y-YMove)*Scale+TmpPoint.Y;
   RotatePoint(SRefPosition,Fangle);
end;
Visible:=true;
//SRefPosition.X:=CurrentX;//AX;//(*;//(SRefPosition.X-XMove)*Scale+*)TmpPOint.X;
//SRefPosition.Y:=CurrentY;//AY;//(*;//(SRefPosition.Y-YMove)*Scale+*)TmpPoint.Y;


end;



Procedure TSymboleditor.DoMove(Const AXMove,AYMove:Double);
var Cnt          : Integer;
begin

  Visible:=FALSE;
  inherited DoMove(AXMove,AYMove);

   SRefPosition.X:=SRefPosition.X+AXMove;
   SRefPosition.Y:=SRefPosition.Y+AYMove;
   with FPoly do begin
    for Cnt:=0 to Count-1 do with PointPtrs[Cnt]^ do begin
      X:=X+AXMove;
      Y:=Y+AYMove;
    end;
         end;
  Visible:=TRUE;
end;

Procedure TSymboleditor.OnEnableMenus;
begin
(*  MenuFunctions.GroupEnabled[1100]:=FSelected.Count>0;
  MenuFunctions.GroupEnabled[1101]:=FSelected.Count=1;
  MenuFunctions.GroupEnabled[1102]:=(FSelected.Count=1)
      and not PointSelected(0);
      *)
end;

Procedure TSymboleditor.DeSelectAll;
var APoint       : TGrPoint;
    Cnt          : Integer;
begin

  if (FSelected.Count>0) and Visible then with Canvas,FPoly do begin
    Pen:=Self.Pen;
    Brush:=Self.Brush;
    Pen.Style:=psSolid;
    Pen.Color:=clBlack;
    for Cnt:=0 to FSelected.Count-1 do begin
      APoint:=FPoly[LongInt(FSelected[Cnt])];
      InternalToWindowPoint(APoint.X,APoint.Y,APoint.X,APoint.Y);
      PaintControlRect(APoint.X,APoint.Y,FHandleSize+Byte(LongInt(FSelected[Cnt])=0),TRUE);
      PaintControlRect(APoint.X,APoint.Y,FHandleSize+Byte(LongInt(FSelected[Cnt])=0),FALSE);
    end;
    FSelected.Clear;
  end;

end;

Procedure TSymboleditor.DeSelectPoint(AIndex:Integer);
var APoint       : TGrPoint;
begin
(*
  if PointSelected(AIndex) and Visible then with Canvas do begin
    Pen:=Self.Pen;
    Brush:=Self.Brush;
    Pen.Style:=psSolid;
    Pen.Color:=clBlack;
    APoint:=FPoly[AIndex];
    InternalToWindowPoint(APoint.X,APoint.Y,APoint.X,APoint.Y);
    PaintControlRect(APoint.X,APoint.Y,FHandleSize+Byte(AIndex=0),TRUE);
    PaintControlRect(APoint.X,APoint.Y,FHandleSize+Byte(AIndex=0),FALSE);
    FSelected.Remove(Pointer(AIndex));
  end;
 *)
  end;

Procedure TSymboleditor.SelectPoint(AIndex:Integer);
var APoint       : TGrPoint;
begin
 (* if not PointSelected(AIndex) and Visible then with Canvas do begin
    Pen:=Self.Pen;
    Brush:=Self.Brush;
    Pen.Style:=psSolid;
    Pen.Color:=clBlack;
    APoint:=FPoly[AIndex];
    InternalToWindowPoint(APoint.X,APoint.Y,APoint.X,APoint.Y);
    PaintControlRect(APoint.X,APoint.Y,FHandleSize+Byte(AIndex=0),FALSE);
    PaintControlRect(APoint.X,APoint.Y,FHandleSize+Byte(AIndex=0),TRUE);
    FSelected.Add(Pointer(AIndex));
  end;
   *)
end;



Procedure TSymboleditor.RefreshSize;
var Rect           : TGrRect;
    Cnt            : Integer;
begin
Exit;
 Rect:=FPoly.Extent;
  Width:=RectWidth(Rect);
  Height:=RectHeight(Rect);
  XPosition:=Rect.Left;
  YPosition:=Rect.Bottom;
end;

Procedure TSymboleditor.DoRotate(Const AX,AY,AAngle:Double);
var Cnt            : Integer;
    SinAngle       : Double;
    CosAngle       : Double;
    Point          : TGrPoint;
    NewPoint       : TGrPoint;
begin
  Visible:=FALSE;
  inherited DoRotate(AX,AY,AAngle);


     SinCos(AAngle,SinAngle,CosAngle);

     Center.X:= FFrameRectCenterX;
     Center.Y:=FFrameRectCenterY;


   (*  BegAng:=BegAng+AAngle;
     if BegAng>PI*2 then BegAng:=BegAng-2*PI
     else if BegAng<-2*PI then BegAng:=BegAng+2*PI;

     EndAng:=EndAng+AAngle;
      if EndAng>PI*2 then EndAng:=EndAng-2*PI
     else if EndAng<-2*PI then EndAng:=EndAng+2*PI;

     {

     for Cnt:=0 to Count-1 do} *)

      with FPoly do begin
    SinCos(AAngle,SinAngle,CosAngle);
     Center:=RectCenter(Extent);
     Center.X:= FFrameRectCenterX;
     Center.Y:=FFrameRectCenterY;

     for Cnt:=0 to Count-1 do with Point do begin
      Point:=MovedPoint(Points[Cnt],-Center.X,-Center.Y);
      NewPoint.X:=Center.X+X*CosAngle-Y*SinAngle;
      NewPoint.Y:=Center.Y+X*SinAngle+Y*CosAngle;
      Points[Cnt]:=NewPoint;
    end;
      with SRefPosition do begin
      SRefPosition:=MovedPoint(SRefPosition,-Center.X,-Center.Y);
      NewPoint.X:=Center.X+X*CosAngle-Y*SinAngle;
      NewPoint.Y:=Center.Y+X*SinAngle+Y*CosAngle;
      SrefPosition:=NewPoint;
     end;
  end;


  Visible:=TRUE;
 end;
end.




