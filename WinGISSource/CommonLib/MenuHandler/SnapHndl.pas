{******************************************************************************+
  Module SnapHndl
--------------------------------------------------------------------------------
  Author: Martin Forst
--------------------------------------------------------------------------------

+******************************************************************************}
Unit SnapHndl;

Interface

Uses Classes,Graphics,GrTools,Scroller;

Type TSnapHandlers   = Class;

     TSnapResultType = (snrPoint,snrVector);

     TSnapResult   = Class(TPersistent)
      Private
       FDirection  : TGrPoint;
       FDistance   : Double;
       FPosition   : TGrPoint;
       FType       : TSnapResultType;
       Procedure   SetDirection(const ADirection:TGrPoint);
      Protected 
       Procedure   AssignTo(Dest:TPersistent); override;
      Public
       // direction of the found snap-vector
       Property    Direction:TGrPoint read FDirection write SetDirection;
       // distance between to object to snap and this snap-result
       Property    Distance:Double read FDistance write FDistance;
       // position of the found snap-point
       Property    Position:TGrPoint read FPosition write FPosition;
       //
       Property    ResultType:TSnapResultType read FType write FType;
     end;

     TSnapResultList    = Class(TList)
      Private
       Function    GetItems(const AIndex:Integer):TSnapResult;
      Public
       Procedure   Assign(AList:TSnapResultList);      
       Procedure   Clear; override;
       Property    Items[const AIndex:Integer]:TSnapResult read GetItems; default;
     end;

     TCommonSnapHandler = Class
      Private
       FEnabled    : Boolean;
       FParent     : TSnapHandlers;
      Public
       Constructor Create(AParent:TSnapHandlers);
       Destructor  Destroy; override;
       // enables/disables the snapping to this handler
       Property    Enabled:Boolean read FEnabled write FEnabled default TRUE;
       // snaps a point to the snap-object
       Procedure   SnapPoint(const Point:TGrPoint;const MaxDistance:Double;
                       Result:TSnapResultList); virtual; abstract;
       Function    Terminated:Boolean;
     end;

     TSnapGrid     = Class(TCommonSnapHandler)
      Private
       FCosRotation: Double;
       FOrigin     : TGrPoint;
       FRotation   : Double;
       FSinRotation: Double;
       FXDistance  : Double;
       FYDistance  : Double;
       Procedure   SetRotation(Const ARotation:Double);
      Public
       Constructor Create(AParent:TSnapHandlers);
       // origin of the grid
       Property    Origin:TGrPoint read FOrigin write FOrigin;
       // distance between the grid-points in x-directon
       Property    XDistance:Double read FXDistance write FXDistance;
       // distance between the grid-points in y-directon
       Property    YDistance:Double read FYDistance write FYDistance;
       // rotation of the grid in radians counter-clockwise
       Property    Rotation:Double read FRotation write SetRotation;
       // snaps a point to the snap-object
       Procedure   SnapPoint(const Point:TGrPoint;const MaxDistance:Double;
                       Result:TSnapResultList); override;
     end;

     TSnapVector   = Class(TCommonSnapHandler)
      Private
       FDirection  : TGrPoint;
       FIsVector   : Boolean;
       FOrigin     : TGrPoint;
       Procedure   SetDirection(const ADirection:TGrPoint);
      Public
       Constructor Create(AParent:TSnapHandlers);
       Property    Direction:TGrPoint read FDirection write SetDirection;
       // false->defines a line (Origin + t * Direction, 0 <= t <= 1)
       Property    IsVector:Boolean read FIsVector write FIsVector default TRUE;
       // start-point of the vector or line
       Property    Origin:TGrPoint read FOrigin write FOrigin;
       // snaps a point to the snap-object
       Procedure   SnapPoint(const Point:TGrPoint;const MaxDistance:Double;
                       Result:TSnapResultList); override;
     end;

     TSnapType          = (snpPoint);
     
     TSnapThread        = Class(TThread)
      Private
       FMaxDistance     : Double;
       FSnapSystem      : TSnapHandlers;
       FSnapType        : TSnapType;
       FSnapPoint       : TGrPoint;
      Public
       Constructor Create(AParent:TSnapHandlers);
       Procedure   Execute; override;
       Procedure   SnapPoint(const Point:TGrPoint;const MaxDistance:Double); 
     end;

     TSnapHandlers      = Class
      Private
       FScroller        : TScroller;
       FOnFinished      : TThreadMethod;
       FPrevSnapResults : TSnapResultList;
       FSavePen         : TPen;
       FSaveBrush       : TBrush;
       FSnapHandlers    : TList;
       FSnappedPoint    : TGrPoint;
       FSnapResults     : TSnapResultList;
       FSnapType        : TSnapType;
       FThread          : TSnapThread;
       Procedure   AddHandler(AHandler:TCommonSnapHandler);
       Procedure   ClearHandlerList;
       Procedure   DrawSnapResults(Results:TSnapResultList);
       Procedure   DoFinishedSnapping;
       Function    GetRunning:Boolean;
       Function    GetSnapResults:TSnapResultList;
       Procedure   RemoveHandler(AHandler:TCommonSnapHandler);
      Public
       Constructor Create;
       Destructor  Destroy; override;
       Property    OnFinished:TThreadMethod read FOnFinished write FOnFinished;
       Procedure   Paint;
       Property    Scroller:TScroller read FScroller write FScroller;
       Property    SnappedPoint:TGrPoint read FSnappedPoint;
       Procedure   SnapPoint(const Point:TGrPoint;const MaxDistance:Double);
       Property    SnapResults:TSnapResultList read GetSnapResults;
       Procedure   Stop;
       Property    Running:Boolean read GetRunning;
     end;

Implementation

Uses Windows,MenuHndl,NumTools;

{ TSnapHandlers }

Constructor TSnapHandlers.Create;
begin
  inherited Create;
  FSnapHandlers:=TList.Create;
  FSnapResults:=TSnapResultList.Create;
  FPrevSnapResults:=TSnapResultList.Create;
  FSavePen:=TPen.Create;
  FSaveBrush:=TBrush.Create;
end;

Destructor TSnapHandlers.Destroy;
begin
  FThread.Free;
  ClearHandlerList;
  FSnapHandlers.Free;
  FSnapResults.Free;
  FPrevSnapResults.Free;
  FSavePen.Free;
  FSaveBrush.Free;
  inherited Destroy;
end;

Procedure TSnapHandlers.ClearHandlerList;
begin
  while FSnapHandlers.Count>0 do TObject(FSnapHandlers[FSnapHandlers.Count-1]).Free;
  FSnapHandlers.Clear;
end;

Procedure TSnapHandlers.AddHandler(AHandler: TCommonSnapHandler);
begin
  FSnapHandlers.Add(AHandler);
end;

Procedure TSnapHandlers.RemoveHandler(AHandler: TCommonSnapHandler);
begin
  FSnapHandlers.Remove(AHandler);
end;

Function TSnapHandlers.GetRunning: Boolean;
begin
  Result:=(FThread<>NIL) and (not FThread.Suspended);
end;

Function TSnapHandlers.GetSnapResults: TSnapResultList;
begin
  Result:=FSnapResults;
end;

Procedure TSnapHandlers.SnapPoint(const Point:TGrPoint;const MaxDistance:Double);
begin
  // stop the thread
  FThread.Free;
  // clear the current thread-list
  FSnapResults.Clear;
  FSnapType:=snpPoint;
  // start the new snapping
  FThread:=TSnapThread.Create(Self);
  FThread.SnapPoint(Point,MaxDistance);
end;

Procedure TSnapHandlers.Stop;
begin
  FThread.Free;
  FThread:=NIL;
end;

Function SortByDistance(Item1,Item2:Pointer):Integer;
begin
  Result:=DblCompare(TSnapResult(Item1).Distance,TSnapResult(Item2).Distance);
  if Result=0 then Result:=LongCompare(Integer(TSnapResult(Item1).ResultType),
      Integer(TSnapResult(Item2).ResultType));
end;

Procedure TSnapHandlers.DoFinishedSnapping;
var Cnt            : Integer;
begin
  // remove the old results from the display
  DrawSnapResults(FPrevSnapResults);
  if FSnapResults.Count>0 then case FSnapType of
    snpPoint: begin
        // sort the results-list by the distance
        FSnapResults.Sort(SortByDistance);
        if FSnapResults[0].ResultType=snrPoint then begin
          // if a point is the nearest snap-result remove all other items
          FSnappedPoint:=FSnapResults[0].Position;
          for Cnt:=FSnapResults.Count-1 downto 1 do begin
            FSnapResults[Cnt].Free;
            FSnapResults.Delete(Cnt);
          end;
          Assert(FSnapResults.Count=1);
        end
        else begin
          // search for a second line-snap
          Cnt:=1;
          while Cnt<FSnapResults.Count do begin
            if (FSnapResults[Cnt].ResultType=snrPoint)
                or (Cnt>=2) then begin
              FSnapResults[Cnt].Free;
              FSnapResults.Delete(Cnt);
            end
            else Inc(Cnt);  
          end;
          Assert(FSnapResults.Count<=2);
          Assert(FSnapResults[0].ResultType=snrVector);
          Assert((FSnapResults.Count=1) or (FSnapResults[1].ResultType=snrVector));
          // calculate cut of lines as snap-point
          FSnappedPoint:=FSnapResults[0].Position;
          if FSnapResults.Count>1 then VectorIntersection(FSnapResults[0].Position,
              FSnapResults[0].Direction,FSnapResults[1].Position,FSnapResults[1].Direction,
              FSnappedPoint);
        end;
      end;
  end;
  // store results
  FPrevSnapResults.Assign(FSnapResults);
  // draw current snap-results
  DrawSnapResults(FSnapResults);
  if Assigned(OnFinished) then FOnFinished;  
end;

Procedure TSnapHandlers.DrawSnapResults(Results:TSnapResultList);
var Cnt            : Integer;
    SnapResult     : TSnapResult;
    XPos           : Double;
    YPos           : Double;
begin
  if Results.Count>0 then with FScroller.Canvas do begin
    FSavePen.Assign(Pen);
    FSaveBrush.Assign(Brush);
    try
      Brush.Style:=bsClear;
      Pen.Style:=psSolid;
      Pen.Color:=clBlack;
      Pen.Width:=0;
      Pen.Mode:=pmNotXOr;
      for Cnt:=0 to Results.Count-1 do begin
        SnapResult:=Results[Cnt];
        FScroller.InternalToWindow(SnapResult.Position.X,SnapResult.Position.Y,XPos,YPos);
        if SnapResult.ResultType=snrPoint then //PaintControlRect(XPos,YPos,3,FALSE)
        else begin
          Pen.Color:=clBlue;
          Pen.Width:=2*GetDeviceCaps(Handle,ASPECTX);
          MoveTo(Round(XPos-SnapResult.Direction.X*500),Round(YPos-SnapResult.Direction.Y*500));
          LineTo(Round(XPos+SnapResult.Direction.X*500),Round(YPos+SnapResult.Direction.Y*500));
          Pen.Color:=clBlack;
          Pen.Width:=0;
        end;
      end;
      FScroller.InternalToWindow(FSnappedPoint.X,FSnappedPoint.Y,XPos,YPos);
      DrawControlRect(FScroller.Canvas,XPos,YPos,3,FALSE);
    finally
      Pen.Assign(FSavePen);
      Brush.Assign(FSaveBrush);
    end;
  end;
end;

{ TCommonSnapHandler }

Constructor TCommonSnapHandler.Create(AParent:TSnapHandlers);
begin
  inherited Create;
  FParent:=AParent;
  FParent.AddHandler(Self);
end;

Destructor TCommonSnapHandler.Destroy;
begin
  FParent.RemoveHandler(Self);
  inherited Destroy;
end;

Function TCommonSnapHandler.Terminated:Boolean;
begin
  Result:=FParent.FThread.Terminated;
end;

{ TSnapThread }

Constructor TSnapThread.Create(AParent: TSnapHandlers);
begin
  inherited Create(TRUE);
  FSnapSystem:=AParent;
end;

Procedure TSnapThread.Execute;
var Cnt            : Integer;
begin
  case FSnapType of
    snpPoint: begin
        for Cnt:=0 to FSnapSystem.FSnapHandlers.Count-1 do begin
          TCommonSnapHandler(FSnapSystem.FSnapHandlers[Cnt]).
              SnapPoint(FSnapPoint,FMaxDistance,FSnapSystem.FSnapResults);
          if Terminated then Break;
        end;
      end;
  end;
  if not Terminated then Synchronize(FSnapSystem.DoFinishedSnapping);
end;

Procedure TSnapThread.SnapPoint(const Point:TGrPoint;const MaxDistance:Double);
begin   
  // setup the new data
  FMaxDistance:=MaxDistance;
  FSnapType:=snpPoint;
  FSnapPoint:=Point;
  // start snapping
  Resume;
end;

{ TSnapVector }

Constructor TSnapVector.Create(AParent:TSnapHandlers);
begin
  inherited Create(AParent);
  FIsVector:=TRUE;
end;

Procedure TSnapVector.SetDirection(const ADirection: TGrPoint);
begin
  FDirection:=Normalized(ADirection);
end;

Procedure TSnapVector.SnapPoint(const Point:TGrPoint;const MaxDistance:Double;
    Result:TSnapResultList);
var Distance       : Double;
    Cut1           : TGrPoint;
    Cut2           : TGrPoint;
    SnapResult     : TSnapResult;
begin
  if FIsVector then Distance:=Abs(VectorNormalIntersection(FOrigin,FDirection,Point,Cut1))
  else if LineVectorIntersection(FOrigin,GrPoint(FOrigin.X+FDirection.X,
        FOrigin.Y+FDirection.Y),Point,GrPoint(FDirection.X,-FDirection.Y),
        Cut1,Cut2)>0 then Distance:=GrPointDistance(Point,Cut1)
  else Distance:=MaxDistance+1.0;
  if Distance<MaxDistance then begin
    SnapResult:=TSnapResult.Create;
    SnapResult.Distance:=Distance;
    SnapResult.Position:=Cut1;
    SnapResult.Direction:=FDirection;
    SnapResult.ResultType:=snrVector;
    Result.Add(SnapResult);
  end;
end;

{ TSnapResultList }

Procedure TSnapResultList.Clear;
var Cnt            : Integer;
begin
  for Cnt:=0 to Count-1 do TObject(Items[Cnt]).Free;
  inherited Clear;
end;

Function TSnapResultList.GetItems(const AIndex: Integer): TSnapResult;
begin
  Result:=TSnapResult(inherited Items[AIndex]);
end;

Procedure TSnapResultList.Assign(AList:TSnapResultList);
var Cnt            : Integer;
    Item           : TSnapResult;
begin
  Clear;
  for Cnt:=0 to AList.Count-1 do begin
    Item:=TSnapResult.Create;
    Item.Assign(TSnapResult(AList[Cnt]));
    Add(Item);
  end;
end;

{ TSnapResult }

procedure TSnapResult.AssignTo(Dest: TPersistent);
begin
  with Dest as TSnapResult do begin
    FDirection:=Self.FDirection;
    FDistance:=Self.FDistance;
    FPosition:=Self.FPosition;
    FType:=Self.FType;
  end;
end;

Procedure TSnapResult.SetDirection(const ADirection:TGrPoint);
begin
  FDirection:=Normalized(ADirection);
end;

{ TSnapGrid }

Constructor TSnapGrid.Create(AParent:TSnapHandlers);
begin
  inherited Create(AParent);
  FCosRotation:=1.0;
end;

Procedure TSnapGrid.SetRotation(const ARotation: Double);
begin
  FRotation:=ARotation;
  SinCos(FRotation,FSinRotation,FCosRotation);
end;

Procedure TSnapGrid.SnapPoint(const Point: TGrPoint;
  const MaxDistance: Double; Result: TSnapResultList);
var GridCoords     : TGrPoint;
    GridPoint      : TGrPoint;
    Distance       : Double;
    SnapResult     : TSnapResult;
begin
  //!?!? calculate point-coordinates in grid-coordinates
  GridCoords:=MovedPoint(Point,-FOrigin.X,-FOrigin.Y);
  GridPoint.X:=GridCoords.X*FCosRotation+GridCoords.Y*FSinRotation;
  GridPoint.Y:=-GridCoords.X*FSinRotation+GridCoords.Y*FCosRotation;
  // calculate nearest grid-point in grid-coordinates
  GridPoint.X:=Round(GridPoint.X/FXDistance)*FXDistance;
  GridPoint.Y:=Round(GridPoint.Y/FYDistance)*FYDistance;
  // convert grid-point to real coordinates
  GridCoords.X:=GridPoint.X*FCosRotation-GridPoint.Y*FSinRotation;
  GridCoords.Y:=GridPoint.X*FSinRotation+GridPoint.Y*FCosRotation;
  GridCoords:=MovedPoint(GridPoint,FOrigin.X,FOrigin.Y);
  // check if it's nearer than MaxDistance
  Distance:=GrPointDistance(Point,GridPoint);
  if Distance<MaxDistance then begin
    SnapResult:=TSnapResult.Create;
    SnapResult.Distance:=Distance;
    SnapResult.Position:=GridCoords;
    SnapResult.ResultType:=snrPoint;
    Result.Add(SnapResult);
  end;
end;

Procedure TSnapHandlers.Paint;
begin
  DrawSnapResults(FPrevSnapResults);
end;

end.
