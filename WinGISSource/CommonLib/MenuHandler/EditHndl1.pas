Unit EditHndl1;

Interface

Uses Classes,Controls,Graphics,GrTools,MenuHndl;

Type {*************************************************************************+
       Type TEditHandler1Options
     ---------------------------------------------------------------------------
       ehoSize = Rahmengr��e kann ver�ndert werden
       ehoSizeMultiple = durch Dr�cken der Ctrl-Taste kann der Rahmen um
         vielfache der Objektgr��e vergr��ert werden.
       ehoSizeSymmetric = Die Rahmengr��e kann durch Dr�cken der Shift-Taste
         symmetrisch in seiner Gr��e ge�ndert werden.
       ehoOnlySymmetric = Rahmen kann nur symmetrisch in der Gr��e ge�ndert
         werden.
       ehoKeepAspect = Verh�ltnis H�he/Breite kann nicht ver�ndert werden
       ehoCornersKeepAspect = Bei Gr��en�nderung durch die vier Eckpunkte
         bleibt das Verh�ltnis H�he/Breite gleich
       ehoRotate = Rahmen kann rotiert werden
  	   ehoMoveBorder = Rahmen kann durch Anclicken des Rahmensverschoben werden
       ehoMoveInside = Rahmen kann durch Anclicken innerhalb verschoben werden
       ehoReference = Rahmen hat einen Referenzpunkt um den Rotiert und
         der als Referenz f�r symmetrisches Skalieren verwendet wird.
       ehoIsoOrientated = Rahmen wird immer parallel zu den Fensterkanten
         dargestellt, auch wenn die Ansicht gedreht ist
       ehoAllowMirroring = Allows a change of the sign of Width or Height.
         If not specified then a change of the sign is prevented by moving the
         position to the opposite corner.
     +*************************************************************************}
     TEditHandler1Option = (ehoSize,ehoSizeMultiple,ehoSizeSymmetric,
         ehoOnlySymmetric,ehoKeepAspect,ehoCornersKeepAspect,ehoRotate,
         ehoMoveBorder,ehoMoveInside,ehoReference,ehoIsoOrientated,
         ehoAllowMirroring);

     TEditHandler1Options = Set of TEditHandler1Option;

     TEditHandler1Class  = Class of TEditHandler1;

     TControlPoints     = Array[0..11] of TGrPoint;

     {**************************************************************************
      Class TEditHandler1
     ---------------------------------------------------------------------------
     ---------------------------------------------------------------------------
       FAngle = Drehwinkel des Begrenzungs-Rechtecks
       FControl = gew�hlter Kontrollpunkt
       FFrameVisible = wenn Null->Rahmen ist sichtbar
       FControlSize = Gr��e der Handles in Pixeln
       FHeight = H�he des Rahmens
       FLastX = X-Position der letzen Mausmeldung
       FLastY = Y-Position der letzen Mausmeldung
       FMinimalMove = minimale Verschiebung der Handles in Pixel
       FMoving = Rahmen wird gerade verschoben
       FMovingFrame = Rahmen wurde um die minimale Pixelanzahl verschoben
       FOptions = Optionen f�r den Rahmen
       FRotating = Rahmen wird gerade gedreht
       FShiftState = aktueller Zustand der Shift und Ctrl-Tasten
       FSizing = Rahmengr��e wird ge�ndert
       FSizingFrame = Kontrollpunkt wurde um die minimale Pixelanzahl versch.
       FStartX = Anfangsposition eines MouseMove
       FStartY = Anfangsposition eines MouseMove
       FWidth = Breite des Rahmens
       FXPos = X-Koordinate des rechten, unteren Eckpunkts
       FXRef = X-Koordinate des Punktes, um den rotiert wird
       FYPos = Y-Koordinate des rechten, unteren Eckpunkts
       FYRef = Y-Koordinate des Punktes, um den rotiert wird
       FExtend = Vergr��erung des Rahmens in Fenster-Einheiten
     +*************************************************************************}
     TEditHandler1  = Class(TMenuHandler)
      Private
       FControl         : Integer;
       FControlPen      : TPen;
       FFrameVisible    : Integer;
       FControlSize     : Integer;
       FLastX           : Double;
       FLastY           : Double;
       FMinimalMove     : Integer;
       FMoving          : Boolean;
       FMovingFrame     : Boolean;
       FOnChange        : TNotifyEvent;
       FOnMove          : TNotifyEvent;
       FOnResize        : TNotifyEvent;
       FOnRotate        : TNotifyEvent;
       FReferencePoint  : Integer;
       FRotating        : Boolean;
       FRotatingFrame   : Boolean;
       FShiftState      : TShiftState;
       FSizing          : Boolean;
       FSizingFrame     : Boolean;
       FStartX          : Double;
       FStartY          : Double;
       FWindowCoords    : Boolean;
       Procedure   CalculateRotating(var AX,AY,AAngle:Double);
       Procedure   CalculateSizing(var AX,AY,AWidth,AHeight:Double);
       Procedure   GetControlPoints(var APoints:TControlPoints;AWindowCoords:Boolean=FALSE);
       Function    GetFrameActive:Boolean;
       Function    GetFrameVisible:Boolean;
       Procedure   GetRectPoints(Const AXPos,AYPos,AWidth,AHeight,AAngle:Double;
                       Extend:Double;AAll:Boolean;var APoints:TControlPoints;
                       AWindowCoords:Boolean);
       Procedure   PaintFrame;
       Procedure   SetFrameVisible(ASet:Boolean);
       Procedure   SetHeight(Const AHeight:Double);
       Procedure   SetOptions(AOptions:TEditHandler1Options);
       Procedure   SetRotation(Const AAngle:Double);
       Procedure   SetWidth(Const AWidth:Double);
       Procedure   SetXPosition(Const AXPos:Double);
       Procedure   SetYPosition(Const AYPos:Double);
      Protected
       FAngle      : Double;
       FExtend     : Double;
       FHeight     : Double;
       FFrameRectCenterX : double;
       FFrameRectCenterY : double;
       FOptions    : TEditHandler1Options;
       FWidth      : Double;
       FXPos       : Double;
       FXRef       : Double;
       FYPos       : Double;
       FYRef       : Double;
       Procedure   DoChange; virtual;
       Procedure   DoHide; override;
       Procedure   DoMove(Const AXMove,AYMove:Double); virtual;
       Procedure   DoPaintInterior; virtual;
       Procedure   DoResize(Const AX,AY,AWidth,AHeight:double; var AoldWidth,AOldHeight:double); virtual;
       Procedure   DoRotate(Const AX,AY,ADeltaAngle:Double); virtual;
       Procedure   DoShow; override;
       Function    FindControlPoint(Const XPos,YPos:Double;var ControlX,ControlY:Double):Integer;
       Function    GetCurrentAngle:Double;
       Function    GetCurrentHeight:Double;
       Function    GetCurrentWidth:Double;
       Function    GetCurrentX:Double;
       Function    GetCurrentY:Double;
       Function    IsPointInside(Const X,Y:Double):Boolean;
       Function    IsPointNearBorder(Const X,Y:Double):Boolean;

       Procedure HiHi;
      Public

       Function    OnMouseDown(Const X,Y:Double;Button:TMouseButton;Shift:TShiftState):Boolean; override;
       Function    OnMouseMove(Const X,Y:Double;Shift:TShiftState):Boolean; override;
       Function    OnMouseUp(Const X,Y:Double;Button:TMouseButton;Shift:TShiftState):Boolean; override;

      Public

       Constructor Create(AParent:TComponent); override;
       Destructor  Destroy; override;
       Property    ControlSize:Integer read FControlSize write FControlSize;
       Property    CurrentHeight:Double read GetCurrentHeight;
       Property    CurrentRotation:Double read GetCurrentAngle;
       Property    CurrentWidth:Double read GetCurrentWidth;
       Property    CurrentX:Double read GetCurrentX;
       Property    CurrentY:Double read GetCurrentY;
       Procedure   Paint; override;

       Property    FrameVisible:Boolean read GetFrameVisible write SetFrameVisible;
       Property    FrameActive:Boolean read GetFrameActive;
       Property    Height:Double read FHeight write SetHeight;
       Function    IntControlSize:Double;
       Function    IntMinimalMove:Double;
       Property    MinimalMove:Integer read FMinimalMove write FMinimalMove;
       Property    Moving:Boolean read FMoving;
       Procedure   OnKeyDown(var Key:Word;Shift:TShiftState); override;
       Procedure   OnKeyUp(var Key:Word;Shift:TShiftState); override;
       Property    Rotating:Boolean read FRotating;
       Property    Rotation:Double read FAngle write SetRotation;
       Property    Sizing:Boolean read FSizing;
       Property    Width:Double read FWidth write SetWidth;
       Property    XPosition:Double read FXPos write SetXPosition;
       Property    XReference:Double read FXRef write FXRef;
       Property    YPosition:Double read FYPos write SetYPosition;
       Property    YReference:Double read FYRef write FYRef;

       property    FrameRectCenterX:Double read FFrameRectCenterX write FFrameRectCenterX;
       property    FrameRectCenterY:Double read FFrameRectCenterY write FFrameRectCenterY;

      Published
       Property    Extend:Double read FExtend write FExtend;
       Property    OnChange:TNotifyEvent read FOnChange write FOnChange;
       Property    OnMove:TNotifyEvent read FOnMove write FOnMove;
       Property    Options:TEditHandler1Options read FOptions write SetOptions;
       Property    OnResize:TNotifyEvent read FOnResize write FOnResize;
       Property    OnRotate:TNotifyEvent read FOnRotate write FOnRotate;
       { index of the reference-point, if > -1 then corresponding point will be drawn in black }
       Property    ReferencePointIndex:Integer read FReferencePoint write FReferencePoint default -1;
       Property    UseWindowCoordinates:Boolean read FWindowCoords write FWindowCoords default False;
      end;

Implementation

Uses CommRes,Forms,NumTools,SysUtils,WCtrls,WinTypes,WinProcs;

Const Tolerance    = 1E-10;

{==============================================================================+
  TEditHandler1
+==============================================================================}

Constructor TEditHandler1.Create
   (
   AParent         : TComponent
   );
  begin
    inherited Create(AParent);
    ControlSize:=3;               { Defaultgr��e f�r Control-Rechtecke         }
    MinimalMove:=0;               { Defaultgr��e f�r minimale Verschiebung     }
    FFrameVisible:=1;             { Rahmen verstecken                          }
    FControlPen:=TPen.Create;
    with FControlPen do Mode:=pmNotXOr;
    FReferencePoint:=-1;
  end;

Destructor TEditHandler1.Destroy;
  begin
    FControlPen.Free;
    inherited Destroy;
  end;

Function TEditHandler1.FindControlPoint
   (
   Const XPos      : Double;
   Const YPos      : Double;
   var ControlX    : Double;
   var ControlY    : Double
   )
   : Integer;
  var APoints      : TControlPoints;
      AWidth       : Double;
      Cnt          : Integer;
      First        : Integer;
      Last         : Integer;
  begin
    GetControlPoints(APoints,FWindowCoords);
    if FWindowCoords then AWidth:=FControlSize
    else AWidth:=IntControlSize;
    if ehoRotate in Options then Last:=11
    else Last:=7;
    if ehoSize in Options then First:=0
    else First:=8;
    for Cnt:=First to Last do begin
      if (Abs(APoints[Cnt].X-XPos)<=AWidth)
          and (Abs(APoints[Cnt].Y-YPos)<=AWidth) then begin
        Result:=Cnt;
        ControlX:=APoints[Cnt].X;
        ControlY:=APoints[Cnt].Y;
        if (ehoKeepAspect in FOptions) and (Result and 1=1) and (Cnt<8) then
            Result:=-1;
        Exit;
      end;
    end;
    Result:=-1;
  end;

Function TEditHandler1.IsPointInside
   (
   Const X         : Double;
   Const Y         : Double
   )
   : Boolean;
  var APoints      : TControlPoints;
  begin
    GetRectPoints(FXPos,FYPos,FWidth,FHeight,FAngle,FExtend,FALSE,APoints,
        FWindowCoords);
    Result:=PointInsidePolygon(GrPoint(X,Y),APoints,4);
  end;

Function TEditHandler1.IsPointNearBorder
   (
   Const X         : Double;
   Const Y         : Double
   )
   : Boolean;
  var APoints      : TControlPoints;
      AWidth       : Double;
      Point1       : TGrPoint;
      Cnt          : Integer;
  begin
    Point1:=GrPoint(X,Y);
    GetControlPoints(APoints,FWindowCoords);
    if FWindowCoords then AWidth:=FControlSize
    else AWidth:=IntControlSize;
    for Cnt:=0 to 3 do if Abs(LineNormalDistance(APoints[Cnt*2],APoints[Cnt*2+2],
        Point1))<AWidth then begin
      Result:=TRUE;
      Exit;
    end;
    Result:=FALSE;
  end;

Function TEditHandler1.OnMouseMove
   (
   Const X         : Double;
   Const Y         : Double;
   Shift           : TShiftState
   )
   : Boolean;
  var ControlPoint : Integer;
      AMinMove     : Double;
      Angle        : Double;
      AX           : Double;
      AY           : Double;
      AWidth       : Double;
      AHeight      : Double;
      AAngle       : Double;
  begin


    Result:=FALSE;
    if Visible then begin
      if FSizing
          or FMoving
          or FRotating then begin
        if FSizingFrame
            or FMovingFrame
            or FRotatingFrame then begin

       FrameVisible:=FALSE;
       FStartX:=FlastX;
       FStartY:=FlastY;
       FLastX:=X;
       FLastY:=Y;
       FShiftState:=Shift;
       Hihi;
       FrameVisible:=TRUE;
        end
        else begin
          if FWindowCoords then AMinMove:=Sqr(FMinimalMove)
          else AMinMove:=Sqr(IntMinimalMove);
          FLastX:=X;
          FLastY:=Y;
          FShiftState:=Shift;
          if Sqr(FLastX-FStartX)+Sqr(FLastY-FStartY)>AMinMove then begin
            if FRotating then FRotatingFrame:=TRUE
            else if FSizing then FSizingFrame:=TRUE
            else begin
              FMovingFrame:=TRUE;
              Cursor:=MoveCursor1;
            end;
            FrameVisible:=TRUE;
          end;
        end;
        Result:=TRUE;
      end
      else begin
        ControlPoint:=FindControlPoint(X,Y,AX,AY);
        if ControlPoint=-1 then begin
          if ehoMoveInside in FOptions then begin
            if IsPointInside(X,Y) then Cursor:=MoveCursor2
            else Cursor:=crDefault;
          end
          else if ehoMoveBorder in FOptions then begin
            if IsPointNearBorder(X,Y) then Cursor:=MoveCursor2
            else Cursor:= crDefault;
          end
          else Cursor:=crDefault;
        end
        else begin
          if FAngle<0 then Angle:=FAngle+2*Pi
          else Angle:=FAngle;
          Angle:=Angle+FViewRotation;
          if FHeight<0 then begin
            if ControlPoint<8 then ControlPoint:=(8+6-ControlPoint) Mod 8
            else ControlPoint:=19-ControlPoint;
          end;
          if ControlPoint<8 then ControlPoint:=(ControlPoint+Round(Angle*4/Pi)) Mod 8
          else ControlPoint:=((ControlPoint*2-16+Round(Angle*4/Pi)) Mod 8)+8;
          case ControlPoint of
            0,4 : Cursor:=SizeSENWCursor;
            1,5 : Cursor:=SizeNSCursor;
            2,6 : Cursor:=SizeNESWCursor;
            3,7 : Cursor:=SizeEWCursor;
            8   : Cursor:=RotSWCursor;
            9   : Cursor:=RotSCursor;
            10  : Cursor:=RotSECursor;
            11  : Cursor:=RotECursor;
            12  : Cursor:=RotNECursor;
            13  : Cursor:=RotNCursor;
            14  : Cursor:=RotNWCursor;
            15  : Cursor:=RotWCursor;
          end;
        end;
      end;
    end;
  end;

Function TEditHandler1.OnMouseDown
   (
   Const X         : Double;
   Const Y         : Double;
   Button          : TMouseButton;
   Shift           : TShiftState
   )
   : Boolean;
  var AX           : Double;
      AY           : Double;
  begin
    Result:=FALSE;
    if (Button=mbLeft) and FrameVisible and not FSizing and
        not FMoving and not FRotating then begin
      FStartX:=X;
      FStartY:=Y;
      FControl:=FindControlPoint(X,Y,AX,AY);
      if FControl<>-1 then begin
        FrameVisible:=FALSE;
        if FControl<8 then FSizing:=TRUE
        else FRotating:=TRUE;
        Result:=TRUE;
      end
      else if ehoMoveBorder in FOptions then begin
        if IsPointNearBorder(X,Y) then begin
         FrameVisible:=FALSE;
          FMoving:=TRUE;
          Result:=TRUE;
        end;
      end
      else if ehoMoveInside in FOptions then begin
        if IsPointInside(X,Y) then begin
          FrameVisible:=FALSE;
          FMoving:=TRUE;
          Result:=TRUE;
        end;
      end;
    end;
  end;

Function TEditHandler1.OnMouseUp
   (
   Const X         : Double;
   Const Y         : Double;
   Button          : TMouseButton;
   Shift           : TShiftState
   )
   :Boolean;
  var AX           : Double;
      AY           : Double;
      AWidth       : Double;
      AHeight      : Double;
      AAngle       : Double;
      AoldHeight   : Double;
      AoldWidth    : Double;
  begin
    Result:=FALSE;

    if Button=mbLeft then begin
      if FSizing or FMoving or FRotating then begin
        if FSizingFrame then begin
          FrameVisible:=FALSE;
          FSizingFrame:=FALSE;
          FSizing:=FALSE;
          FLastX:=X;
          FLastY:=Y;
          CalculateSizing(AX,AY,AWidth,AHeight);

          if not (ehoAllowMirroring in Options) then begin
            if (AWidth*Abs(AWidth)<0) XOr (Width*Abs(Width)<0) then begin
              AX:=AX+AWidth;
              AWidth:=-AWidth;
            end;
            if (AHeight*Abs(AHeight)<0) XOr (Height*Abs(Height)<0) then begin
              AY:=AY+AHeight;
              AHeight:=-AHeight;
            end;
          end;
          DoResize(AX,AY,AWidth,AHeight,AoldWidth,AoldHeight);
          FrameVisible:=TRUE;
        end
        else if FMovingFrame then begin
          FrameVisible:=FALSE;
          FMovingFrame:=FALSE;
          FMoving:=FALSE;
          FLastX:=X;
          FLastY:=Y;
          DoMove(FLastX-FStartX,FLastY-FStartY);
          FrameVisible:=TRUE;
        end
        else if FRotatingFrame then begin
          FrameVisible:=FALSE;
          FRotatingFrame:=FALSE;
          FRotating:=FALSE;
          FLastX:=X;
          FLastY:=Y;
          CalculateRotating(AX,AY,AAngle);
          DoRotate(AX,AY,AAngle-FAngle);
          FrameVisible:=TRUE;
        end
        else begin
          FSizing:=FALSE;
          FMoving:=FALSE;
          FRotating:=FALSE;
          FrameVisible:=TRUE;
        end;
        Result:=TRUE;
      end;
    end;
  end;

{******************************************************************************+
  Procedure TEditHandler1.CalculateSizing
--------------------------------------------------------------------------------
  Berechnet den Ursprung und die Breite und H�he des Begrenzungsrechtecks bei
  der Gr��en�nderung aufgrund der aktuellen Mausposition.
+******************************************************************************}
Procedure TEditHandler1.CalculateSizing
   (
   var AX          : Double;           { X-Position des neuen Ursprungs        }
   var AY          : Double;           { Y-Position des neuen Ursprungs        }
   var AWidth      : Double;           { neue Breite des Begrenzungsrechtecks  }
   var AHeight     : Double            { neue H�he des Begrenzungsrechtecks    }
   );
  Type TSizeItem   = Record
         XMove     : Boolean;
         YMove     : Boolean;
         WidthMult : Integer;
         HeightMult: Integer;
       end;
  Const SizeTable  : Array[0..7] of TSizeItem = (
    (XMove:TRUE;  YMove:TRUE;  WidthMult:-1; HeightMult:-1),
    (XMove:FALSE; YMove:TRUE;  WidthMult:0;  HeightMult:-1),
    (XMove:FALSE; YMove:TRUE;  WidthMult:1;  HeightMult:-1),
    (XMove:FALSE; YMove:FALSE; WidthMult:1;  HeightMult:0),
    (XMove:FALSE; YMove:FALSE; WidthMult:1;  HeightMult:1),
    (XMove:FALSE; YMove:FALSE; WidthMult:0;  HeightMult:1),
    (XMove:TRUE;  YMove:FALSE; WidthMult:-1; HeightMult:1),
    (XMove:TRUE;  YMove:FALSE; WidthMult:-1; HeightMult:0));
  var SizeItem     : TSizeItem;        { Tabelleneintrag f�r den Kontrollpunkt }
      DWidth       : Double;           { �nderung der Breite des Rechtecks     }
      DHeight      : Double;           { �nderung der H�he des Rechtecks       }
      OrgAspect    : Double;           { Breiten/H�henverh�ltnis des Originals }
  begin
    { Mausverschiebung und Gr��en�nderung berechnen. Das Vorzeichen wird       }
    { je nach "gezogenem" Kontrollpunkt korrigiert.                            }
    SizeItem:=SizeTable[FControl];
    AX:=FLastX-FStartX;
    AY:=FLastY-FStartY;
    DWidth:=SizeItem.WidthMult*(AX*Cos(FAngle)+AY*Sin(FAngle));
    DHeight:=SizeItem.HeightMult*(-AX*Sin(FAngle)+AY*Cos(FAngle));
    { Abfrage, ob nur ein Mehrfaches der Objektgr��e zul�ssig ist (Aspect-     }
    { Beibehaltung wird unten berechnet                                        }
    if (ehoSizeMultiple in FOptions)
        and (ssCtrl in FShiftState) then begin
      { wenn symmetrische Gr��en�nderung aktiv, dann �nderung auf ein          }
      { Vielfaches der halben Objektgr��e setzen->beidseitig hinzugef�gt       }
      if (ehoOnlySymmetric in FOptions)
          or ((ehoSizeSymmetric in FOptions)
            and (ssShift in FShiftState)) then begin
        DWidth:=Int(2.0*DWidth/FWidth)*FWidth/2.0;
        DHeight:=Int(2.0*DHeight/FHeight)*FHeight/2.0;
      end
      else begin
        { Gr��en�nderung auf Vielfaches der Objektgr��e setzen                 }
        DWidth:=Int(DWidth/FWidth)*FWidth;
        DHeight:=Int(DHeight/FHeight)*FHeight;
      end;
      { Verhindern, da� das Objekt auf Gr��e Null verkleinert wird             }
      if Abs(FWidth+DWidth)<Tolerance then DWidth:=0.0;
      if Abs(FHeight+DHeight)<Tolerance then DHeight:=0.0;
    end;
    { wenn das Breiten/H�hen Verh�ltnis beibehalten werden mu�, so m�ssen      }
    { die Breiten- und H�hen�nderung korrigiert werden                         }
    if (ehoKeepAspect in FOptions)
        or (ssAlt in FShiftState)
        or ((ehoCornersKeepAspect in FOptions)
          and (FControl Mod 2=0)) then begin
      OrgAspect:=FWidth/FHeight;
      AWidth:=FWidth+DWidth;
      AHeight:=FHeight+DHeight;
      if OrgAspect<1.0 then begin
        if AHeight<AWidth then AWidth:=AHeight*OrgAspect
        else AHeight:=AWidth/OrgAspect;
      end
      else begin
        if AHeight>AWidth then AWidth:=AHeight*OrgAspect
        else AHeight:=AWidth/OrgAspect;
      end;
      DWidth:=AWidth-FWidth;
      DHeight:=AHeight-FHeight;
    end;
    { neue Gr��e berechnen                                                     }
    AWidth:=FWidth+DWidth;
    AHeight:=FHeight+DHeight;
    { wenn symmetrische Gr��en�nderung aktiv, so mu� die neue Gr��e            }
    { angepa�t werden. Der Ursprungspunkt des Rechtecks wird verschoben,       }
    { wenn H�hen- bzw. BreitenMultiplikator ungleich null sind.                }
    if (ehoOnlySymmetric in FOptions)
        or ((ehoSizeSymmetric in FOptions)
          and (ssShift in FShiftState)) then begin
      AWidth:=AWidth+DWidth;
      AHeight:=AHeight+DHeight;
    end
    else begin
      { keine symmetrische Gr��enanpassung->Ursprung darf nur dann             }
      { verschoben werden, wenn in der Tabelle angegeben.                      }
      if not SizeItem.XMove then DWidth:=0.0;
      if not SizeItem.YMove then DHeight:=0.0;
    end;
    { move origin of the rectangle }
    AX:=FXPos-DWidth*Cos(FAngle)+DHeight*Sin(FAngle);
    AY:=FYPos-DWidth*Sin(FAngle)-DHeight*Cos(FAngle);
  end;

{*******************************************************************************
| Procedure TEditHandler1.CalculateRotating
|-------------------------------------------------------------------------------
| Calculates the new frame-position and angle when in rotating-mode from the
| current mouse-position.
+******************************************************************************}
Procedure TEditHandler1.CalculateRotating
   (
   var AX          : Double;
   var AY          : Double;
   var AAngle      : Double
   );
  var CenterX      : Double;
      CenterY      : Double;
      Angle1       : Double;
      Angle2       : Double;
  begin
    if ehoReference in FOptions then begin
      CenterX:=FXRef;
      CenterY:=FYRef;
    end
    else begin
      CenterX:=FXPos+Width*Cos(FAngle)/2-Height*Sin(FAngle)/2;
      CenterY:=FYPos+Width*Sin(FAngle)/2+Height*Cos(FAngle)/2;
    end;
    Angle1:=LineAngle(GrPoint(CenterX,CenterY),GrPoint(FStartX,FStartY));
    Angle2:=LineAngle(GrPoint(CenterX,CenterY),GrPoint(FLastX,FLastY));
    AAngle:=FAngle+Angle2-Angle1;
    AX:=CenterX-Width*Cos(AAngle)/2+Height*Sin(AAngle)/2;
    AY:=CenterY-Width*Sin(AAngle)/2-Height*Cos(AAngle)/2;

    FFrameRectCenterX:=  CenterX ;
    FFrameRectCenterY:= Centery;
  end;


{******************************************************************************+
  Procedure TEditHandler1.PaintFrame
-------------------------------------------------------------------------------
  Zeichnet den Rahmen um das Objekt. Je nach Optionen werden die entsprechenden
  Bedienelemente gezeichnet.
+******************************************************************************}
Procedure TEditHandler1.PaintFrame;
  var APoints      : TControlPoints;
      Cnt          : Integer;
      DX           : Double;
      DY           : Double;
      AWidth       : Double;           { neue H�he des Rechtecks               }
      AHeight      : Double;           { neue Breite des Rechtecks             }
      AAngle       : Double;
  begin
    with Canvas do begin
      ActivateDrawingTools;
      if FSizing then begin
        { neue Parameter f�r das Rechteck berechnen                            }
        CalculateSizing(DX,DY,AWidth,AHeight);
        { Eckpunkte des neuen Rechtecks berechnen und in Bildschirmkoordinaten }
        { umwandeln.                                                           }
        GetRectPoints(DX,DY,AWidth,AHeight,FAngle,0,FALSE,APoints,True);
        { Rechteck und Kontrollpunkte zeichnen                                 }
        MoveTo(Round(APoints[3].X),Round(APoints[3].Y));
        for Cnt:=0 to 3 do LineTo(Round(APoints[Cnt].X),Round(APoints[Cnt].Y));
      end
      else if FMoving then begin
        GetRectPoints(FXPos+FLastX-FStartX,FYPos+FLastY-FStartY,FWidth,FHeight,
            FAngle,0,FALSE,APoints,True);
        { Rechteck und Kontrollpunkte zeichnen                                 }
        MoveTo(Round(APoints[3].X),Round(APoints[3].Y));
        for Cnt:=0 to 3 do LineTo(Round(APoints[Cnt].X),Round(APoints[Cnt].Y));
      end
      else if FRotating then begin
        { calculate new parameters }
        CalculateRotating(DX,DY,AAngle);
        { calculate rectangle-points and convert them to display-coordinates }
        GetRectPoints(DX,DY,FWidth,FHeight,AAngle,0,FALSE,APoints,True);
        { draw rectangle and control-points }
        MoveTo(Round(APoints[3].X),Round(APoints[3].Y));
        for Cnt:=0 to 3 do LineTo(Round(APoints[Cnt].X),Round(APoints[Cnt].Y));
      end
      else begin
        GetControlPoints(APoints,True);
        MoveTo(Round(APoints[6].X),Round(APoints[6].Y));
        for Cnt:=0 to 3 do LineTo(Round(APoints[Cnt*2].X),Round(APoints[Cnt*2].Y));
        Pen.Assign(FControlPen);
        Brush.Style:=bsClear;
        if ehoSize in FOptions then begin
          if ehoKeepAspect in FOptions then for Cnt:=0 to 3 do
              PaintControlRect(APoints[Cnt*2].X,APoints[Cnt*2].Y,FControlSize,
              FReferencePoint=Cnt*2)
          else for Cnt:=0 to 7 do PaintControlRect(APoints[Cnt].X,APoints[Cnt].Y,
              FControlSize,FReferencePoint=Cnt);
        end;
        if ehoRotate in FOptions then for Cnt:=8 to 11 do
          PaintControlCircle(APoints[Cnt].X,APoints[Cnt].Y,FControlSize);
        DoPaintInterior;
      end;
     RestoreDrawingTools;
    end;
   end;

{******************************************************************************+
  Procedure TEditHandler1.GetRectPoints
--------------------------------------------------------------------------------
  Calculates the control-points for a frame with the specified parameters.
--------------------------------------------------------------------------------
  AXPos, AYPos = lower left position of the frame
  AWidth, AHeight = size of the frame
  AAngle = rotation of the frame in rad
  Extend = amount the frame shall be made bigger
  AAll = if true all points are calculated, if false only the edge-points are
      returned
  APoints = the points are stored in here
+******************************************************************************}
Procedure TEditHandler1.GetRectPoints
   (
   Const AXPos     : Double;
   Const AYPos     : Double;
   Const AWidth    : Double;
   Const AHeight   : Double;
   Const AAngle    : Double;
   Extend          : Double;
   AAll            : Boolean;
   var APoints     : TControlPoints;
   AWindowCoords   : Boolean
   );
  var Cnt          : Integer;
      AExtend      : Double;
      BExtend      : Double;
      BXPos        : Double;
      BYPos        : Double;
      BHeight      : Double;
      BWidth       : Double;
      APoint       : TPoint;
      ASinAngle    : Double;
      ACosAngle    : Double;
      BPoint       : TGrPoint;
      TmpPOint     : TGrPoint;
  begin
    ACosAngle:=Cos(AAngle);
    ASinAngle:=Sin(AAngle);
    AExtend:=WindowToInternalDist(Extend);
    BExtend:=AExtend;
    if AWidth<0 then AExtend:=-AExtend;
    if AHeight<0 then BExtend:=-BExtend;
    if AAll then begin
      BWidth:=AWidth/2.0+AExtend;
      BHeight:=AHeight/2.0+BExtend;
    end
    else begin
      BWidth:=AWidth+2.0*AExtend;
      BHeight:=AHeight+2.0*BExtend;
    end;
    BXPos:=AXPos-AExtend*ACosAngle+BExtend*ASinAngle;
    BYPos:=AYPos-AExtend*ASinAngle-BExtend*ACosAngle;
    Cnt:=0;
    APoints[Cnt].X:=BXPos;
    APoints[Cnt].Y:=BYPos;
    Inc(Cnt);
    BXPos:=BXPos+ACosAngle*BWidth; BYPos:=BYPos+ASinAngle*BWidth;
    APoints[Cnt].X:=BXPos;
    APoints[Cnt].Y:=BYPos;
    Inc(Cnt);
    if AAll then begin
      BXPos:=BXPos+ACosAngle*BWidth; BYPos:=BYPos+ASinAngle*BWidth;
      APoints[Cnt].X:=BXPos;
      APoints[Cnt].Y:=BYPos;
      Inc(Cnt);
    end;
    BXPos:=BXPos-ASinAngle*BHeight; BYPos:=BYPos+ACosAngle*BHeight;
    APoints[Cnt].X:=BXPos;
    APoints[Cnt].Y:=BYPos;
    Inc(Cnt);
    if AAll then begin
      BXPos:=BXPos-ASinAngle*BHeight; BYPos:=BYPos+ACosAngle*BHeight;
      APoints[Cnt].X:=BXPos;
      APoints[Cnt].Y:=BYPos;
      Inc(Cnt);
    end;
    BXPos:=BXPos-ACosAngle*BWidth; BYPos:=BYPos-ASinAngle*BWidth;
    APoints[Cnt].X:=BXPos;
    APoints[Cnt].Y:=BYPos;
    Inc(Cnt);
    if AAll then begin
      BXPos:=BXPos-ACosAngle*BWidth; BYPos:=BYPos-ASinAngle*BWidth;
      APoints[Cnt].X:=BXPos;
      APoints[Cnt].Y:=BYPos;
      Inc(Cnt);
      BXPos:=BXPos+ASinAngle*BHeight; BYPos:=BYPos-ACosAngle*BHeight;
      APoints[Cnt].X:=BXPos;
      APoints[Cnt].Y:=BYPos;
    end;
    APoint:=Point(Round(FControlSize*2.5),0);
    DPToLP(Canvas.Handle,APoint,1);
    AExtend:=WindowToInternalDist(Extend+Abs(APoint.X));
    BExtend:=AExtend;
    if AWidth<0 then AExtend:=-AExtend;
    if AHeight<0 then BExtend:=-BExtend;
    BWidth:=AWidth+2.0*AExtend;
    BHeight:=AHeight+2.0*BExtend;
    BXPos:=AXPos-AExtend*ACosAngle+BExtend*ASinAngle;
    BYPos:=AYPos-AExtend*ASinAngle-BExtend*ACosAngle;
    APoints[8].X:=BXPos;
    APoints[8].Y:=BYPos;
    BXPos:=BXPos+ACosAngle*BWidth; BYPos:=BYPos+ASinAngle*BWidth;
    APoints[9].X:=BXPos;
    APoints[9].Y:=BYPos;
    BXPos:=BXPos-ASinAngle*BHeight; BYPos:=BYPos+ACosAngle*BHeight;
    APoints[10].X:=BXPos;
    APoints[10].Y:=BYPos;
    BXPos:=BXPos-ACosAngle*BWidth; BYPos:=BYPos-ASinAngle*BWidth;
    APoints[11].X:=BXPos;
    APoints[11].Y:=BYPos;

    if AWindowCoords then begin
      for Cnt:=0 to 3 do begin
        InternalToWindowPoint(APoints[Cnt].X,APoints[Cnt].Y,BPoint.X,BPoint.Y);
        APoints[Cnt]:=BPoint;
      end;
      if AAll then for Cnt:=4 to 7 do begin
        InternalToWindowPoint(APoints[Cnt].X,APoints[Cnt].Y,BPoint.X,BPoint.Y);
        APoints[Cnt]:=BPoint;
      end;
      for Cnt:=8 to 11 do begin
        InternalToWindowPoint(APoints[Cnt].X,APoints[Cnt].Y,BPoint.X,BPoint.Y);
        APoints[Cnt]:=BPoint;
      end;
    end;

  end;

Procedure TEditHandler1.GetControlPoints
   (
   var APoints     : TControlPoints;
   AWindowCoords   : Boolean
   );
  begin
    GetRectPoints(FXPos,FYPos,FWidth,FHeight,FAngle,FExtend,TRUE,APoints,
        AWindowCoords);
  end;

Procedure TEditHandler1.SetXPosition
   (
   Const AXPos     : Double
   );
  begin
    if Abs(FXPos-AXPos)>Tolerance then begin
      FrameVisible:=FALSE;
      FXPos:=AXPos;
      FrameVisible:=TRUE;
    end;
  end;

Procedure TEditHandler1.SetWidth
   (
   Const AWidth    : Double
   );
  begin
    if Abs(FWidth-AWidth)>Tolerance then begin
      FrameVisible:=FALSE;
      FWidth:=AWidth;
      FrameVisible:=TRUE;
    end;
  end;

Procedure TEditHandler1.SetHeight
   (
   Const AHeight    : Double
   );
  begin
    if Abs(FHeight-AHeight)>Tolerance then begin
      FrameVisible:=FALSE;
      FHeight:=AHeight;
      FrameVisible:=TRUE;
    end;
  end;

Procedure TEditHandler1.SetYPosition
   (
   Const AYPos     : Double
   );
  begin
    if Abs(FYPos-AYPos)>Tolerance then begin
      FrameVisible:=FALSE;
      FYPos:=AYPos;
      FrameVisible:=TRUE;
    end;
  end;

Procedure TEditHandler1.SetRotation
   (
   Const AAngle    : Double
   );
  begin
    if DblCompare(AAngle,FAngle)<>0 then begin
      FrameVisible:=FALSE;
      FAngle:=AAngle;
      FrameVisible:=TRUE;
    end;
  end;

Function TEditHandler1.IntControlSize
   : Double;
  var APoint       : TPoint;
  begin
    APoint:=Point(FControlSize,FControlSize);
    DPToLP(Canvas.Handle,APoint,1);
    Result:=WindowToInternalDist(APoint.X);
  end;

Function TEditHandler1.IntMinimalMove
   : Double;
  var APoint       : TPoint;
  begin
    APoint:=Point(FMinimalMove,FMinimalMove);
    DPToLP(Canvas.Handle,APoint,1);
    Result:=WindowToInternalDist(APoint.X);
  end;

Procedure TEditHandler1.OnKeyDown
   (
   var Key         : Word;
   Shift           : TShiftState
   );
  begin
    if Shift<>FShiftState then begin
      if FSizing then begin
        FrameVisible:=FALSE;
        FShiftState:=Shift;
        FrameVisible:=TRUE;
      end
      else FShiftState:=Shift;
    end;
  end;

Procedure TEditHandler1.Paint;
begin
  if not FrameActive then DoPaint;
  if FrameVisible then PaintFrame;
end;

Procedure TEditHandler1.OnKeyUp
   (
   var Key         : Word;
   Shift           : TShiftState
   );
  begin
    if Shift<>FShiftState then begin
      if FSizing then begin
        FrameVisible:=FALSE;
        FShiftState:=Shift;
        FrameVisible:=TRUE;
      end
      else FShiftState:=Shift;
    end;
  end;

Function TEditHandler1.GetFrameActive
   : Boolean;
  begin
    Result:=FSizing or FMoving;
  end;

Function TEditHandler1.GetFrameVisible
   : Boolean;
  begin
    Result:=FFrameVisible=0;
  end;

Procedure TEditHandler1.SetFrameVisible
   (
   ASet            : Boolean
   );
  begin
    if ASet then begin
      if FFrameVisible>0 then begin
        Dec(FFrameVisible);
        if (FFrameVisible=0) and ParentValid then PaintFrame;
      end;
    end
    else begin
      Inc(FFrameVisible);
      if (FFrameVisible=1) and ParentValid then PaintFrame;
    end;
  end;

{******************************************************************************+
  Procedure TEditHandler1.DoResize
--------------------------------------------------------------------------------
  Wird aufgerufen, wenn eine Gr��en�nderung durchzuf�hren ist.
--------------------------------------------------------------------------------
  AX,AY   = neue Position des Rahmenurspungs
  AWidth  = neue Breite des Rahmens
  AHeight = neue H�he des Rahmens
+******************************************************************************}
Procedure TEditHandler1.DoResize
   (
   Const AX        : Double;
   Const AY        : Double;
   Const AWidth    : Double;
   Const AHeight   : Double;
   var AoldWidth : Double;
   var AOldHeight: Double);
  begin
    Visible:=FALSE;
    XPosition:=AX;
    YPosition:=AY;
    AoldWidth:=Width;
    Width:=AWidth;
    AoldHeight:=Height;
    Height:=AHeight;
    if Assigned(FOnResize) then OnResize(Self);
    Visible:=TRUE;
  end;

Procedure TEditHandler1.DoRotate(Const AX,AY,ADeltaAngle:Double);
begin
  Visible:=FALSE;
  XPosition:=AX;
  YPosition:=AY;
  Rotation:=FAngle+ADeltaAngle;
  if Assigned(FOnRotate) then OnRotate(Self);
  Visible:=TRUE;
end;

{******************************************************************************+
  Procedure TEditHandler1.DoHide
--------------------------------------------------------------------------------
  Wird aufgerufen, wenn der Handler unsichtbar wird. Es wird zud�tzlich der
  Rahmen versteckt.
+******************************************************************************}
Procedure TEditHandler1.DoHide;
  begin
    FrameVisible:=FALSE;
    Cursor:=crDefault;
    inherited DoHide;
  end;

{******************************************************************************+
  Procedure TEditHandler1.DoShow
--------------------------------------------------------------------------------
  Wird aufgerufen, wenn der Handler sichtbar wird. Es wird zud�tzlich der
  Rahmen sichtbar gemacht.
+******************************************************************************}
Procedure TEditHandler1.DoShow;
  begin
    inherited DoShow;
    FrameVisible:=TRUE;
  end;

{******************************************************************************+
  Procedure TEditHandler1.DoMove
--------------------------------------------------------------------------------
  Wird aufgerufen, wenn der Rahmen verschoben wurde,
--------------------------------------------------------------------------------
  XMove, YMove = In X bzw. Y-Richtung durchzuf�hrende Verschiebung
+******************************************************************************}
Procedure TEditHandler1.DoMove
   (
   Const AXMove    : Double;
   Const AYMove    : Double
   );
  begin
    Visible:=FALSE;
    XPosition:=XPosition+AXMove;
    YPosition:=YPosition+AYMove;
    if Assigned(FOnMove) then OnMove(Self);
    Visible:=TRUE;
  end;

Function TEditHandler1.GetCurrentAngle:Double;
  var NewValue1    : Double;
      NewValue2    : Double;
  begin
    if FRotating then CalculateRotating(NewValue1,NewValue2,Result)
    else Result:=FAngle;
  end;

Function TEditHandler1.GetCurrentHeight:Double;
  var NewValue1    : Double;
      NewValue2    : Double;
      NewValue3    : Double;
  begin
    if FSizing then CalculateSizing(NewValue1,NewValue2,NewValue3,Result)
    else Result:=FHeight;
  end;

Function TEditHandler1.GetCurrentWidth:Double;
  var NewValue1    : Double;
      NewValue2    : Double;
      NewValue3    : Double;
  begin
    if FSizing then CalculateSizing(NewValue1,NewValue2,Result,NewValue3)
    else Result:=FWidth;
  end;

Function TEditHandler1.GetCurrentX
   : Double;
  var NewValue1    : Double;
      NewValue2    : Double;
      NewValue3    : Double;
  begin
    if FSizing then CalculateSizing(Result,NewValue1,NewValue2,NewValue3)
    else if FMoving then Result:=FXPos+FLastX-FStartX
    else Result:=FXPos;
  end;

Function TEditHandler1.GetCurrentY
   : Double;
  var NewValue1    : Double;
      NewValue2    : Double;
      NewValue3    : Double;
  begin
    if FSizing then CalculateSizing(NewValue1,Result,NewValue2,NewValue3)
    else if FMoving then Result:=FYPos+FLastY-FStartY
    else Result:=FYPos;
  end;

Procedure TEditHandler1.DoChange;
  begin
	if Assigned(FOnChange) then OnChange(Self);
  end;

Procedure TEditHandler1.DoPaintInterior;
  begin
  end;

procedure TEditHandler1.SetOptions(AOptions: TEditHandler1Options);
begin
  if AOptions<>FOptions then begin
    FrameVisible:=FALSE;
    FOptions:=AOptions;
    FrameVisible:=TRUE;
  end;
end;


procedure TEditHandler1.Hihi;
  var ControlPoint : Integer;
      AMinMove     : Double;
      Angle        : Double;
      AX           : Double;
      AY           : Double;
      AWidth       : Double;
      AOldWidth,
      AOLdHeight:Double;
      AHeight      : Double;
      AAngle       : Double;
 begin;
  if FSizingFrame
   then
    begin;
       CalculateSizing(AX,AY,AWidth,AHeight);
       AoldWidth:=AWidth; AoldHeight:=AHeight;
        if not (ehoAllowMirroring in Options) then begin
            if (AWidth*Abs(AWidth)<0) XOr (Width*Abs(Width)<0) then begin
              AX:=AX+AWidth;
              AWidth:=-AWidth;
            end;
            if (AHeight*Abs(AHeight)<0) XOr (Height*Abs(Height)<0) then begin
              AY:=AY+AHeight;
              AHeight:=-AHeight;
            end;
          end;
          DoResize(AX,AY,AWidth,AHeight,AoldWidth,AoldHeight);
    end
     else if FMovingFrame then begin
          DoMove(FLastX-FStartX,FLastY-FStartY);
        end
        else if FRotatingFrame then begin
          CalculateRotating(AX,AY,AAngle);
          DoRotate(AX,AY,AAngle-FAngle);
           end;


 end;


end.
