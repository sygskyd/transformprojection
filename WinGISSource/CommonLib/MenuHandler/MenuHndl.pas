{******************************************************************************+
  Unit MenuHndl
--------------------------------------------------------------------------------
  Autor: Martin Forst
--------------------------------------------------------------------------------
  - Basisklasse f�r die Men�handler
  - Registrierungsprozedur f�r Men�handler
  - Liste aller registrierten Men�handler
--------------------------------------------------------------------------------
  Version 1.0, 10-04-1997
+******************************************************************************}
Unit MenuHndl;

Interface

Uses Classes,Controls,Graphics,MenuFn,Windows;

Const mhtSingleAction        = $0001;
      mhtPushActive          = $0002;
      mhtReturnToPushed      = $0003;
      mhtReturnAndReset      = $0004;
      mhtReturnOrClear       = $0005;

      mhtDontPush            = $0100;

Type TMenuHandlerStatus      = (mhsNotFinished,mhsReturnToPrevious,mhsReturnToDefault);

     TMenuHandlerClass       = Class of TMenuHandler;

     TConvertPointEvent      = Procedure(Const FromX,FromY:Double;var ToX,ToY:Double) of Object;
     TConvertDistanceEvent   = Function(Const FromDist:Double):Double of Object;

     TMenuHandler  = Class(TComponent)
      Private
       FBrush           : TBrush;
       FCursor          : Integer;
       FOnIntToWinPoint : TConvertPointEvent;
       FOnWinToIntPoint : TConvertPointEvent;
       FOnIntToWinDist  : TConvertDistanceEvent;
       FOnWinToIntDist  : TConvertDistanceEvent;
       FParent          : TWinControl;
       FPopupMenu       : AnsiString;
       FPen             : TPen;
       FVisible         : Integer;
       FCanvas          : TCanvas;
       FSubHandler      : TMenuHandler;
       FSavePen         : TPen;
       FSaveBrush       : TBrush;
       FStatus          : TMenuHandlerStatus;
       FStatusText      : String;
       FToolsActive     : Integer;
       Function    GetCursor:Integer;
       Function    GetMenuName:String;
       Function    GetVisible:Boolean;
       Procedure   SetCursor(ACursor:Integer);
       Procedure   SetStatusText(Const AText:String);
       Procedure   SetSubHandler(AHandler:TMenuHandler);
      Protected
       FViewRotation    : Double;
       FCosViewRotation : Double;
       FSinViewRotation : Double;
       {+++ Brovak BUG 539 BUILD 167}
       FAllowDblclick : Boolean; //Handler have reaction with right doubleclick
       {-- Brovak }
       Procedure   ActivateDrawingTools;
       Procedure   DoHide; virtual;
       Procedure   DoPaint; virtual;
       Procedure   DoPopup(Const X,Y:Double;Const AMenu:String);
       Procedure   DoShow; virtual;
       Function    GetStatusText:String; virtual;
       Procedure   Notification(AComponent:TComponent;Operation:TOperation); override;
       Function    ParentValid:Boolean;
       Procedure   RestoreDrawingTools;
       Procedure   SetCanvas(ACanvas:TCanvas); virtual;
       Procedure   SetParent(AParent:TWinControl); virtual;
       Procedure   SetViewRotation(const Rotation:Double); virtual;
       Procedure   SetVisible(ASet:Boolean); virtual;
      Public
       {+++ Brovak BUG 539 BUILD 167}
       NeedZoom :boolean; //active handler zooming now
       {-- Brovak }
       Constructor Create(AParent:TComponent); override;
       Destructor  Destroy; override;
       Property    Brush:TBrush read FBrush;
       Property    Canvas:TCanvas read FCanvas write SetCanvas;
       Property    Cursor:Integer read GetCursor write SetCursor;
       Class Function HandlerType:Word; virtual;
       Function    InternalToWindowDist(Const FromDist:Double):Double; virtual;
       Procedure   InternalToWindowPoint(Const IntX,IntY:Double;var WinX,WinY:Double); virtual;
       Property    MenuName:String read GetMenuName;
       Procedure   OnActivate; virtual;
       Procedure   OnDeActivate; virtual;
       Procedure   OnEnableMenus; virtual;
       Procedure   OnEnd; virtual;
       Procedure   OnKeyDown(var Key:Word;Shift:TShiftState); virtual;
       Procedure   OnKeyUp(var Key:Word;Shift:TShiftState); virtual;
       Function    OnMouseDown(Const X,Y:Double;Button:TMouseButton;Shift:TShiftState):Boolean; virtual;
       Function    OnMouseMove(Const X,Y:Double;Shift:TShiftState):Boolean; virtual;
       Function    OnMouseUp(Const X,Y:Double;Button:TMouseButton;Shift:TShiftState):Boolean; virtual;
       Procedure   OnReinvoke; virtual;
       Procedure   OnStart; virtual;
       Procedure   OnUndo(Steps:Integer); virtual;
       Procedure   Paint; virtual;
       Procedure   PaintControlCircle(Const XPos,YPos:Double;RectSize:Integer);
       Procedure   PaintControlCross(Const XPos,YPos:Double;RectSize:Integer);
       Procedure   PaintControlRect(Const XPos,YPos:Double;RectSize:Integer;Filled:Boolean);
       Procedure   PaintControlTriangle(Const XPos,YPos:Double;RectSize:Integer;Const Angle:Double);
       Property    PopupMenu:AnsiString read FPopupMenu write FPopupMenu;
       Property    Pen:TPen read FPen write FPen;
       Class Procedure Registrate(Const AMenuFnName:String);
       Procedure   ShowDialog; virtual;
       Property    Status:TMenuHandlerStatus read FStatus write FStatus;
       Property    StatusText:String read GetStatusText write SetStatusText;
       Property    SubHandler:TMenuHandler read FSubHandler write SetSubHandler;
       Property    Visible:Boolean read GetVisible write SetVisible;
       { rotation between internal- and window-coordinate-system }
       Property    ViewRotation:Double read FViewRotation write SetViewRotation;
       Function    WindowToInternalDist(Const FromDist:Double):Double; virtual;
       Procedure   WindowToInternalPoint(Const WinX,WinY:Double;var IntX,IntY:Double); virtual;
       Procedure   WindowToScreenPoint(Const WinX,WinY:Integer;var ScrX,ScrY:Integer); virtual;
       {+++ Brovak BUG 539 BUILD 167}
       Property    AllowDblclick : Boolean  read FAllowDblclick;
       {-- Brovak }
      Published
       { event to convert a distance from internal to window coordinates }
       Property    OnIntToWinDistance:TConvertDistanceEvent read FOnIntToWinDist write FOnIntToWinDist;
       { event to convert a point from internal to window coordinates }
       Property    OnIntToWinPoint:TConvertPointEvent read FOnIntToWinPoint write FOnIntToWinPoint;
       Property    Parent:TWinControl read FParent write SetParent;
       { event to convert a distance from window to internal coordinates }
       Property    OnWinToIntDistance:TConvertDistanceEvent read FOnWinToIntDist write FOnWinToIntDist;
       { event to convert a point from window to internal coordinates }
       Property    OnWinToIntPoint:TConvertPointEvent read FOnWinToIntPoint write FOnWinToIntPoint;
     end;

     TMenuHandlers = Class
      Private
       FHandlers   : TStringList;
       Function    GetMenuHandler(Const AFunctionName:String):TMenuHandlerClass;
       Function    GetMenuHandlerName(Const AHandler:TMenuHandlerClass):String;
      Public
       Constructor Create;
       Destructor  Destroy; override;
       Property    MenuHandlers[Const AFunctionName:String]:TMenuHandlerClass read GetMenuHandler; default;
       Property    MenuHandlerNames[Const AHandler:TMenuHandlerClass]:String read GetMenuHandlerName;
       Procedure   RegisterMenuHandler(Const FuncName:String;AHandler:TMenuHandlerClass);
     end;

Procedure DrawControlCircle(Canvas:TCanvas;Const XPos,YPos:Double;RectSize:Integer);
Procedure DrawControlCross(Canvas:TCanvas;Const XPos,YPos:Double;RectSize:Integer);
Procedure DrawControlRect(Canvas:TCanvas;Const XPos,YPos:Double;RectSize:Integer;Filled:Boolean);
Procedure DrawControlTriangle(Canvas:TCanvas;Const XPos,YPos:Double;RectSize:Integer;Const Angle:Double);
Function  RectWithPixelSize(Canvas:TCanvas;X,Y:Double;PixSize:Integer):TRect;

var MenuHandlers   : TMenuHandlers;

Implementation

Uses Forms,NumTools,SysUtils,TypInfo,UIntfCtrls,UserIntf;

{==============================================================================+
  TMenuHandler                                                                 
+==============================================================================}

Constructor TMenuHandler.Create(AParent:TComponent);
begin
  inherited Create(AParent);
  FVisible:=1;
  FPen:=TPen.Create;
  FPen.Color:=clBlue;
  FPen.Style:=psDot;
  FPen.Mode:=pmNotXor;
  FBrush:=TBrush.Create;
  FBrush.Color:=clWhite;
  FBrush.Style:=bsSolid;
  FCursor:=crDefault;
  FSavePen:=TPen.Create;
  FSaveBrush:=TBrush.Create;
  FStatus:=mhsReturnToPrevious;
  FCosViewRotation:=1.0;
  { +++ Brovak BUG 539 BUILD 167}
  FAllowDblclick:=false;
  NeedZoom:=false;
  {-- Brovak }
end;

Destructor TMenuHandler.Destroy;
begin
  while Visible do Visible:=FALSE;
  if FSubHandler<>NIL then FSubHandler.Free;
  FPen.Free;
  FBrush.Free;
  FSavePen.Free;
  FSaveBrush.Free;
  inherited Destroy;
end;

Function TMenuHandler.GetStatusText
   : String;
  begin
    if FSubHandler<>NIL then Result:=FSubHandler.StatusText
    else Result:=FStatusText;
  end;

Procedure TMenuHandler.ShowDialog;
begin
  if SubHandler<>NIL then SubHandler.ShowDialog;
end;

Procedure TMenuHandler.SetStatusText
   (
   Const AText     : String
   );
  begin
    if FSubHandler<>NIL then FSubHandler.StatusText:=AText
    else if AText<>FStatusText then begin
      FStatusText:=AText;
      UserInterface.Update([uiStatusBar]);
    end;  
  end;

{******************************************************************************+
  Function TMenuHandler.GetVisible
--------------------------------------------------------------------------------
  Ermittelt, ob das Objekt sichtbar ist. Dies ist dann der Fall, wenn FVisbile
  Null ist.
{******************************************************************************}
Function TMenuHandler.GetVisible
   : Boolean;
  begin
    Result:=FVisible=0;
  end;

{******************************************************************************+
  Procedure TMenuHandler.SetVisible                                            
--------------------------------------------------------------------------------
  Erh�ht oder erniedrigt den Sichtbarkeitsz�hler und zeichnet das Objekt,
  wenn sich der Sichtbarkeitsstatus ver�ndert.
{******************************************************************************}
Procedure TMenuHandler.SetVisible
   (
   ASet            : Boolean
   );
  begin                         
    if FSubHandler<>NIL then FSubHandler.Visible:=ASet;
    if ASet then begin
      if FVisible>0 then begin
        Dec(FVisible);
        if FVisible=0 then DoShow;
      end;
    end
    else begin
      Inc(FVisible);
      if FVisible=1 then DoHide;
    end;
  end;

Procedure TMenuHandler.DoPaint;
begin
end;

Procedure TMenuHandler.OnKeyDown
   (
   var Key         : Word;
   Shift           : TShiftState
   );
  begin
    if FSubHandler<>NIL then FSubHandler.OnKeyDown(Key,Shift);
  end;

Procedure TMenuHandler.OnKeyUp
   (
   var Key         : Word;
   Shift           : TShiftState
   );
  begin
    if FSubHandler<>NIL then FSubHandler.OnKeyUp(Key,Shift);
  end;

Function TMenuHandler.OnMouseMove
   (
   Const X         : Double;
   Const Y         : Double;
   Shift           : TShiftState
   )
   : Boolean;
  begin
    if FSubHandler<>NIL then Result:=FSubHandler.OnMouseMove(X,Y,Shift)
    else Result:=FALSE;
  end;

Function TMenuHandler.OnMouseUp
   (
   Const X         : Double;
   Const Y         : Double;
   Button          : TMouseButton;
   Shift           : TShiftState
   )
   : Boolean;
  begin
    if FSubHandler<>NIL then Result:=FSubHandler.OnMouseUp(X,Y,Button,Shift)
    else Result:=FALSE;
    if not Result and (Button=mbRight) then begin
      if PopupMenu<>'' then begin
        DoPopup(X,Y,PopupMenu);        
        Result:=TRUE;
      end
      else Result:=FALSE;
    end
    else Result:=FALSE;
  end;

Function TMenuHandler.OnMouseDown
   (
   Const X         : Double;
   Const Y         : Double;
   Button          : TMouseButton;
   Shift           : TShiftState
   )
   : Boolean;
  begin
    if FSubHandler<>NIL then Result:=FSubHandler.OnMouseDown(X,Y,Button,Shift)
    else Result:=FALSE;
  end;

Procedure TMenuHandler.Paint;
begin
  if ParentValid and (Canvas<>NIL) then begin
    if FSubHandler<>NIL then FSubHandler.Paint;
    if Visible then DoPaint;
  end;  
end;

Procedure TMenuHandler.DoShow;
begin
  DoPaint;
end;

Procedure TMenuHandler.DoHide;
begin
  if ParentValid then DoPaint;
end;

Procedure TMenuHandler.OnEnableMenus;
begin
  if FSubHandler<>NIL then FSubHandler.OnEnableMenus;
end;

Procedure TMenuHandler.SetCursor(ACursor:Integer);
begin
  if FSubHandler<>NIL then FSubHandler.Cursor:=ACursor
  else if ACursor<>FCursor then begin
    FCursor:=ACursor;
    FParent.Cursor:=FCursor;
    if GetCapture<>0 then Windows.SetCursor(Screen.Cursors[FParent.Cursor]);
  end;
end;

{******************************************************************************+
  Procedure TMenuHandler.InternalToWindowPoint
--------------------------------------------------------------------------------
  Wandelt die Koordinaten eines Punktes vom internen Koordinatensystem in
  das Koordinatensystem des Fensters um. Wenn der Methodenpointer FIntToWin
  gesetzt ist, so wird dieser aufgerufen, ansonsten gibt die Routine die
  Koordinaten unver�ndert zur�ck.
{******************************************************************************}
Procedure TMenuHandler.InternalToWindowPoint
   (
   Const IntX      : Double;
   Const IntY      : Double;
   var WinX        : Double;
   var WinY        : Double
   );
  begin
    if Assigned(FOnIntToWinPoint) then OnIntToWinPoint(IntX,IntY,WinX,WinY)
    else if Owner is TMenuHandler then TMenuHandler(Owner).InternalToWindowPoint(IntX,IntY,WinX,WinY)
    else begin
      WinX:=IntX;
      WinY:=IntY;
    end;
  end;

Procedure TMenuHandler.WindowToInternalPoint
   (
   Const WinX      : Double;
   Const WinY      : Double;
   var IntX        : Double;
   var IntY        : Double
   );
  begin
    if Assigned(FOnWinToIntPoint) then OnWinToIntPoint(WinX,WinY,IntX,IntY)
    else if Owner is TMenuHandler then TMenuHandler(Owner).WindowToInternalPoint(WinX,WinY,IntX,IntY)
    else begin
      IntX:=WinX;
      IntY:=WinY;
    end;
  end;

Function TMenuHandler.InternalToWindowDist
   (
   Const FromDist  : Double
   )
   : Double;
  begin
    if Assigned(FOnIntToWinDist) then Result:=OnIntToWinDistance(FromDist)
    else if Owner is TMenuHandler then Result:=TMenuHandler(Owner).InternalToWindowDist(FromDist)
    else Result:=FromDist;
  end;

Function TMenuHandler.WindowToInternalDist
   (
   Const FromDist  : Double
   )
   : Double;
  begin
    if Assigned(FOnWinToIntDist) then Result:=OnWinToIntDistance(FromDist)
    else if Owner is TMenuHandler then Result:=TMenuHandler(Owner).WindowToInternalDist(FromDist)
    else Result:=FromDist;
  end;

Procedure TMenuHandler.WindowToScreenPoint
   (
   Const WinX      : Integer;
   Const WinY      : Integer;
   var ScrX        : Integer;
   var ScrY        : Integer
   );
  var Point        : TPoint; 
  begin
    Point.X:=WinX;
    Point.Y:=WinY;
    LPToDP(Canvas.Handle,Point,1);
    ClientToScreen(FParent.Handle,Point);
    ScrX:=Point.X;
    ScrY:=Point.Y;
  end;
   
Procedure TMenuHandler.OnStart;
  begin
    if FSubHandler<>NIL then FSubHandler.OnStart;
  end;

Procedure TMenuHandler.OnEnd;
begin
  Visible:=FALSE;
  if FSubHandler<>NIL then FSubHandler.OnEnd;
end;

Procedure TMenuHandler.SetSubHandler(AHandler:TMenuhandler);
begin
  if FSubHandler<>NIL then begin
    FSubHandler.Visible:=FALSE;
    FSubHandler.Free;
  end;
  FSubHandler:=AHandler;
  if FSubHandler<>NIL then begin
    FSubHandler.Parent:=FParent;
    FSubHandler.Canvas:=FCanvas;
    FSubHandler.FOnIntToWinPoint:=FOnIntToWinPoint;
    FSubHandler.FOnWinToIntPoint:=FOnWinToIntPoint;
    FSubHandler.FOnIntToWinDist:=FOnIntToWinDist;
    FSubHandler.FOnWinToIntDist:=FOnWInToIntDist;
  end;
end;

Function TMenuHandler.GetCursor:Integer;
begin
  if FSubHandler<>NIL then Result:=FSubHandler.Cursor
  else Result:=FCursor;
end;

Procedure TMenuHandler.SetParent(AParent:TWinControl);
begin
  FParent:=AParent;
  if FSubHandler<>NIL then FSubHandler.Parent:=AParent;
end;

Procedure TMenuHandler.SetCanvas
   (
   ACanvas         : TCanvas
   );
  begin
    FCanvas:=ACanvas;
    if FSubHandler<>NIL then FSubHandler.Canvas:=ACanvas;
  end;

Procedure TMenuHandler.DoPopup(Const X,Y:Double;Const AMenu:String);
var APopup       : TUIPopupMenu;
    MX           : Double;
    MY           : Double;
    IX           : Integer;
    IY           : Integer;
begin
  APopup:=UserInterface.PopupMenus[AMenu];
  if APopup<>NIL then begin
    UserInterface.Update([uiMenus]);
    InternalToWindowPoint(X,Y,MX,MY);
    WindowToScreenPoint(Round(MX),Round(MY),IX,IY);
    APopup.Popup(IX,IY);
  end;
end;

Function TMenuHandler.GetMenuName
   : String;
  begin
    Result:=MenuHandlers.MenuHandlerNames[TMenuHandlerClass(ClassType)];
  end;

Class Function TMenuHandler.HandlerType:Word;
begin
  Result:=mhtSingleAction;
end;

Class Procedure TMenuHandler.Registrate(Const AMenuFnName:String);
begin
  MenuHandlers.RegisterMenuHandler(AMenuFnName,TMenuHandlerClass(GetTypeData(ClassInfo)^.ClassType));
end;

Procedure TMenuHandler.OnActivate;
begin
end;

Procedure TMenuHandler.OnDeActivate;
begin
end;
  
Procedure TMenuHandler.RestoreDrawingTools;
begin
  if FToolsActive>0 then Dec(FToolsActive);
  if (Canvas<>NIL) and (FToolsActive=0) then with Canvas do begin
    { reset brush and pen }
    Pen:=FSavePen;
    Brush:=FSaveBrush;
    { query handle to activate brush and pen }
    Handle;
  end;
end;

Procedure TMenuHandler.ActivateDrawingTools;
begin
  Inc(FToolsActive);
  if (Canvas<>NIL) and (FToolsActive=1) then with Canvas do begin
    FSavePen.Assign(Pen);
    FSaveBrush.Assign(Brush);
    Pen:=FPen;
    Brush:=FBrush;
  end;                            
end;
  
Function RectWithPixelSize(Canvas:TCanvas;X,Y:Double;PixSize:Integer):TRect;
begin
  with Result do begin
    Left:=Round(X);
    Top:=Round(Y);
    LpToDp(Canvas.Handle,Result,1);
    Right:=Left+PixSize+1;
    Bottom:=Top+PixSize+1;
    Left:=Left-PixSize;
    Top:=Top-PixSize;
    DpToLp(Canvas.Handle,Result,2);
  end;
end;

Procedure TMenuHandler.PaintControlRect(Const XPos,YPos:Double;RectSize:Integer;
    Filled:Boolean);
begin
  DrawControlRect(Canvas,XPos,YPos,RectSize,Filled);
end;

Procedure TMenuHandler.PaintControlCircle(Const XPos,YPos:Double;RectSize:Integer);
begin
  DrawControlCircle(Canvas,XPos,YPos,RectSize);
end;

Procedure TMenuHandler.PaintControlCross(Const XPos,YPos:Double;RectSize:Integer);
begin
  DrawControlCross(Canvas,XPos,YPos,RectSize);
end;

Procedure TMenuHandler.PaintControlTriangle(Const XPos,YPos:Double;RectSize:Integer;
    Const Angle:Double);
begin
  DrawControlTriangle(Canvas,XPos,YPos,RectSize,Angle);
end;

Procedure DrawControlRect(Canvas:TCanvas;Const XPos,YPos:Double;RectSize:Integer;
    Filled:Boolean);
var ARect        : TRect;
begin
  ARect:=RectWithPixelSize(Canvas,XPos,YPos,RectSize);
  with ARect do if Filled then PatBlt(Canvas.Handle,Left,Top,Right-Left,Bottom-Top,dstInvert)
  else Canvas.Rectangle(Left,Top,Right,Bottom);
end;

Procedure DrawControlCircle(Canvas:TCanvas;Const XPos,YPos:Double;RectSize:Integer);
var ARect        : TRect;
begin
  ARect:=RectWithPixelSize(Canvas,XPos,YPos,RectSize);
  with ARect do Canvas.Ellipse(Left,Bottom,Right,Top);
end;

Procedure DrawControlCross(Canvas:TCanvas;Const XPos,YPos:Double;RectSize:Integer);
var Point        : TPoint;
    OldMapMode   : Integer;
begin
  Point.X:=Round(XPos);
  Point.Y:=Round(YPos);
  with Canvas,Point do begin
    LPToDP(Handle,Point,1);
    OldMapMode:=SetMapMode(Handle,mm_Text);
    MoveTo(X-RectSize,Y-RectSize);
    LineTo(X+RectSize+1,Y+RectSize+1);
    MoveTo(X-RectSize,Y-RectSize+1);
    LineTo(X+RectSize,Y+RectSize+1);
    MoveTo(X-RectSize+1,Y-RectSize);
    LineTo(X+RectSize+1,Y+RectSize);

    MoveTo(X-RectSize,Y+RectSize);
    LineTo(X+RectSize+1,Y-RectSize-1);
    MoveTo(X-RectSize,Y+RectSize-1);
    LineTo(X+RectSize,Y-RectSize-1);
    MoveTo(X-RectSize+1,Y+RectSize);
    LineTo(X+RectSize+1,Y-RectSize);
  end;
  SetMapMode(Canvas.Handle, OldMapMode);
end;
                                              
Procedure DrawControlTriangle(Canvas:TCanvas;Const XPos:Double;Const YPos:Double;
    RectSize:Integer;Const Angle:Double);
var ARect        : TRect;
    AXPos        : Double;
    AYPos        : Double;
begin
  with Canvas do begin
    ARect:=RectWithPixelSize(Canvas,XPos,YPos,RectSize);
    MoveTo(Round(XPos),Round(YPos));
    AXPos:=XPos-RectWidth(ARect)*Cos(Angle)/2+RectHeight(ARect)*Sin(Angle);
    AYPos:=YPos-RectWidth(ARect)*Sin(Angle)/2-RectHeight(ARect)*Cos(Angle);
    LineTo(Round(AXPos),Round(AYPos));
    AXPos:=AXPos+RectWidth(ARect)*Cos(Angle);
    AYPos:=AYPos+RectWidth(ARect)*Sin(Angle);
    LineTo(Round(AXPos),Round(AYPos));
    LineTo(Round(XPos),Round(YPos));
  end;
end;

Procedure TMenuHandler.SetViewRotation(const Rotation:Double);
begin
  if DblCompare(FViewRotation,Rotation)<>0 then begin
    SetVisible(FALSE);
    FViewRotation:=Rotation;
    FSinViewRotation:=Sin(FViewRotation);
    FCosViewRotation:=Cos(FViewRotation);
    if FSubHandler<>NIL then FSubHandler.ViewRotation:=FViewRotation;
    SetVisible(TRUE);
  end;
end;

{==============================================================================+
  TMenuHandlers                                                                
+==============================================================================}

Constructor TMenuHandlers.Create;
  begin
    inherited Create;
    FHandlers:=TStringList.Create;
    FHandlers.Sorted:=TRUE;
  end;

Destructor TMenuHandlers.Destroy;
  begin
    FHandlers.Free;
    inherited Destroy;
  end;

Procedure TMenuHandlers.RegisterMenuHandler
   (
   Const FuncName  : String;
   AHandler        : TMenuHandlerClass
   );
  begin
    FHandlers.AddObject(FuncName,Pointer(AHandler));
  end;

Function TMenuHandlers.GetMenuHandler
   (
   Const AFunctionName  : String
   )
   : TMenuHandlerClass;
  var AIndex            : Integer;
  begin
    if not FHandlers.Find(AFunctionName,AIndex) then Result:=NIL
    else Result:=TMenuHandlerClass(FHandlers.Objects[AIndex]);
  end;

Function TMenuHandlers.GetMenuHandlerName
   (
   Const AHandler  : TMenuHandlerClass
   )
   : String;
  var AIndex       : Integer;
  begin
    AIndex:=FHandlers.IndexOfObject(Pointer(AHandler));
    if AIndex<0 then Result:=''
    else Result:=FHandlers[AIndex];
  end;
   
Procedure TMenuHandler.Notification
   (
   AComponent      : TComponent;
   Operation       : TOperation
   );
  begin
    inherited Notification(AComponent,Operation);
    if (Operation=opRemove) and (AComponent=FParent) then FParent:=NIL;
  end;

Procedure TMenuHandler.OnReinvoke;
  begin
    if SubHandler<>NIL then SubHandler.OnReinvoke;
  end;  

Procedure TMenuHandler.OnUndo(Steps:Integer);
  begin
    if SubHandler<>NIL then SubHandler.OnUndo(Steps);
  end;

Function TMenuHandler.ParentValid:Boolean;
begin
  Result:=(FParent<>NIL) and not (csDestroying in FParent.ComponentState);
end;

{==============================================================================+
  Initialisuerungs- und Terminierungscode
+==============================================================================}

Initialization
  begin
    MenuHandlers:=TMenuHandlers.Create;
    {$IFNDEF WIN32}
    AddExitProc(ExitUnit);
    {$ENDIF}
  end;

{$IFDEF WIN32}
Finalization
  begin
    MenuHandlers.Free;
  end;
{$ENDIF}

end.