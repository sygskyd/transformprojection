{*************************************}
{    TeeChart Functions               }
{ Copyright (c) 2000 by David Berneda }
{    All Rights Reserved              }
{*************************************}
{$I teedefs.inc}
unit StatChar;

interface

Uses Windows, Classes, TeEngine, Series, TeCanvas;

Type
   { Moving Average, Weigted, WeightedIndex }
   TMovingAverageFunction=class(TTeeMovingFunction)
   private
     FWeighted      : Boolean;
     FWeightedIndex : Boolean;
     Procedure SetWeighted(Value:Boolean);
     procedure SetWeightedIndex(const Value: Boolean);
   public
     Function Calculate( Series:TChartSeries;
                         FirstIndex,LastIndex:Integer):Double; override;
   published
     property Weighted:Boolean read FWeighted write SetWeighted default False;
     property WeightedIndex:Boolean read FWeightedIndex write SetWeightedIndex default False;
   end;

   { Exponential Moving Average }
   TExpMovAveFunction=class(TTeeFunction)
   public
     Constructor Create(AOwner:TComponent); override;
     procedure AddPoints(Source:TChartSeries); override;
   end;

   { RSI, Relative Strentgh Index }
   TRSIStyle=(rsiOpenClose,rsiClose);

   TRSIFunction = class(TTeeMovingFunction)
   private
     FStyle : TRSIStyle;
     procedure SetStyle(Const Value:TRSIStyle);
   public
     Constructor Create(AOwner:TComponent); override;

     Function Calculate( Series:TChartSeries;
                         FirstIndex,LastIndex:Integer):Double; override;
   published
     property Style:TRSIStyle read FStyle write SetStyle default rsiOpenClose;
   end;

   { Exponential Average }
   TExpAverageFunction = class(TTeeMovingFunction)
   private
     FWeight:Double;
     Procedure SetWeight(Const Value:Double);
   public
     Constructor Create(AOwner:TComponent); override;
     Function Calculate( Series:TChartSeries;
                         FirstIndex,LastIndex:Integer):Double; override;
   published
     property Weight:Double read FWeight write SetWeight;
   end;

   { Momemtum }
   TMomentumFunction=class(TTeeMovingFunction)
   public
     Function Calculate( Series:TChartSeries;
                         FirstIndex,LastIndex:Integer):Double; override;
   end;

   { Momemtum Divisor }
   TMomentumDivFunction=class(TTeeMovingFunction)
   public
     Function Calculate( Series:TChartSeries;
                         FirstIndex,LastIndex:Integer):Double; override;
   end;

   { RMS, Root Mean Square }
   TRMSFunction = class(TTeeFunction)
   private
     FComplete  : Boolean;

     INumPoints : Integer;
     ISum2      : Double;
     Procedure Accumulate(Const Value: Double);
     Function  CalculateRMS: Double;
     Procedure SetComplete(const Value: Boolean);
   public
     Function Calculate(SourceSeries:TChartSeries; FirstIndex,LastIndex:Integer):Double; override;
     Function CalculateMany(SourceSeriesList:TList; ValueIndex:Integer):Double;  override;
   published
     property Complete: Boolean read FComplete write SetComplete;
   end;

   { Standard Deviation, Complete }
   TStdDeviationFunction=class(TTeeFunction)
   private
     FComplete : Boolean;
     ISum      : Double;
     ISum2     : Double;
     INumPoints: Integer;
     Procedure Accumulate(Const Value:Double);
     Function CalculateDeviation:Double;
     Procedure SetComplete(Value:Boolean);
   public
     Function Calculate(SourceSeries:TChartSeries; FirstIndex,LastIndex:Integer):Double; override;
     Function CalculateMany(SourceSeriesList:TList; ValueIndex:Integer):Double;  override;
   published
     property Complete:Boolean read FComplete write SetComplete default False;
   end;

   { MACD, Moving Average Convergence }
   TMACDFunction=class(TTeeMovingFunction)
   private
     IMoving1 : TExpMovAveFunction;
     IMoving2 : TExpMovAveFunction;
     ISeries1 : TChartSeries;
     ISeries2 : TChartSeries;
     function GetPeriod2: Double;
     procedure SetPeriod2(const Value: Double);
   public
     Constructor Create(AOwner: TComponent); override;
     Destructor Destroy; override;
     procedure AddPoints(Source: TChartSeries); override;
   published
     property Period2:Double read GetPeriod2 write SetPeriod2;
   end;

   { Stochastic }
   TStochasticFunction=class(TTeeMovingFunction)
   protected
     FNums : Array{$IFNDEF D4}[0..10000]{$ENDIF} of Double;
     FDens : Array{$IFNDEF D4}[0..10000]{$ENDIF} of Double;
   public
     {$IFDEF D4}
     Destructor Destroy; override;

     procedure AddPoints(Source: TChartSeries); override;
     {$ENDIF}
     Function Calculate( Series:TChartSeries;
                         FirstIndex,LastIndex:Integer):Double; override;
   end;

   { Histogram Series }
   THistogramSeries=class(TCustomLineSeries)
   private
     FLinesPen : TChartHiddenPen;

     IPrevious : Integer;
     procedure SetLinesPen(const Value: TChartHiddenPen);
     Function VisiblePoints:Integer;
   protected
     class Procedure CreateSubGallery(AddSubChart:TChartSubGalleryProc); override;
     Procedure CalcHorizMargins(Var LeftMargin,RightMargin:Integer); override;
     Procedure CalcVerticalMargins(Var TopMargin,BottomMargin:Integer); override;
     class Function GetEditorClass:String; override;
     class Procedure SetSubGallery(ASeries:TChartSeries; Index:Integer); override;
   public
     Constructor Create(AOwner: TComponent); override;
     Destructor Destroy; override;

     Procedure Assign(Source:TPersistent); override;
     procedure DrawValue(ValueIndex:Integer); override;
   published
     property Active;
     property ColorEachPoint;
     property ColorSource;
     property Cursor;
     property HorizAxis;
     property LinePen;
     property Marks;
     property ParentChart;
     property DataSource;
     property PercentFormat;
     property SeriesColor;
     property ShowInLegend;
     property Title;
     property ValueFormat;
     property VertAxis;
     property XLabelsSource;
       { events }
     property AfterDrawValues;
     property BeforeDrawValues;
     property OnAfterAdd;
     property OnBeforeAdd;
     property OnClearValues;
     property OnClick;
     property OnDblClick;
     property OnGetMarkText;

     property Brush;
     property LinesPen:TChartHiddenPen read FLinesPen write SetLinesPen;
     property Pen;
     property XValues;
     property YValues;
   end;

   { Financial Bollinger Bands }
   TBollingerFunction=class(TTeeFunction)
   private
     FExponential : Boolean;
     FDeviation   : Integer;
     IOther       : TChartSeries;
     procedure SetDeviation(const Value: Integer);
     procedure SetExponential(const Value: Boolean);
   public
     Constructor Create(AOwner:TComponent); override;
     Destructor Destroy; override;
     procedure AddPoints(Source:TChartSeries); override;
     property LowBand:TChartSeries read IOther;
   published
     property Deviation:Integer read FDeviation write SetDeviation default 1;
     property Exponential:Boolean read FExponential write SetExponential default True;
   end;

implementation

Uses SysUtils, TeeProcs, TeeConst, TeeProCo, Chart, Graphics;

{ TMovingAverageFunction }
Procedure TMovingAverageFunction.SetWeighted(Value:Boolean);
begin
  if FWeighted<>Value then
  begin
    FWeighted:=Value;
    Recalculate;
  end;
end;

Function TMovingAverageFunction.Calculate( Series:TChartSeries;
                                           FirstIndex,LastIndex:Integer):Double;
var t         : Integer;
    tmpSumX   : Double;
    tmpYValue : Double;
    tmpXValue : Double;
    tmpVList  : TChartValueList;
begin
  result:=0;
  tmpSumX:=0;
  tmpVList:=ValueList(Series);
  for t:=FirstIndex to LastIndex do
  begin
    tmpYValue:=tmpVList.Value[t];
    if FWeighted then
    Begin
      tmpXValue:=Series.XValues.Value[t];
      result:=result+tmpYValue*tmpXValue;
      tmpSumX:=tmpSumX+tmpXValue;
    end
    else
    if FWeightedIndex then
    begin
      tmpXValue:=t-FirstIndex+1;
      result:=result+tmpYValue*tmpXValue;
      tmpSumX:=tmpSumX+tmpXValue;
    end
    else
      result:=result+tmpYValue;
  end;
  if FWeighted or FWeightedIndex then
  begin
    if tmpSumX<>0 then result:=result/tmpSumX else result:=0;
  end
  else result:=result/(LastIndex-FirstIndex+1);
end;

procedure TMovingAverageFunction.SetWeightedIndex(const Value: Boolean);
begin
  if FWeightedIndex<>Value then
  begin
    FWeightedIndex:=Value;
    Recalculate;
  end;
end;

{ R.S.I. }
Constructor TRSIFunction.Create(AOwner: TComponent);
begin
  inherited;
  FStyle:=rsiOpenClose;
end;

Function TRSIFunction.Calculate( Series:TChartSeries;
                                 FirstIndex,LastIndex:Integer):Double;
var NumPoints : Integer;
    t         : Integer;
    tmpClose  : Double;
    Ups       : Double;
    Downs     : Double;
    Opens     : TChartValueList;
    Closes    : TChartValueList;
Begin
  With Series do
  Begin
    Closes:=GetYValueList('CLOSE');
    Ups:=0;
    Downs:=0;
    if Self.Style=rsiOpenClose then
    begin
      Opens :=GetYValueList('OPEN');
      for t:=FirstIndex to LastIndex do
      Begin
        tmpClose:=Closes.Value[t];
        if Opens.Value[t]>tmpClose then Downs:=Downs+tmpClose
                                   else Ups  :=Ups  +tmpClose;
      end;
      NumPoints:=(LastIndex-FirstIndex)+1;
      Downs:=Downs/NumPoints;
      Ups  :=Ups  /NumPoints;
      if Downs<>0 then
      Begin
        result:=100.0 - ( 100.0 / ( 1.0+Abs(Ups/Downs) ) );
        if result<0   then result:=0 else
        if result>100 then result:=100;
      end
      else result:=100;
    end
    else
    begin
      for t:=FirstIndex+1 to LastIndex do
      Begin
        tmpClose:=Closes.Value[t]-Closes.Value[t-1];
        if tmpClose<0 then Downs:=Downs-tmpClose
                      else Ups  :=Ups  +tmpClose;
      end;
      NumPoints:=(LastIndex-FirstIndex);  // +1 ?
      Downs:=Downs/NumPoints;
      Ups  :=Ups  /NumPoints;
      if Downs=0 then Downs:=1;
      result:=100.0 - ( 100.0 / ( 1.0+(Ups/Downs) ) )
    end;
  end;
end;

procedure TRSIFunction.SetStyle(const Value: TRSIStyle);
begin
  if Style<>Value then
  begin
    FStyle:=Value;
    ReCalculate;
  end;
end;

{ TExpAverageFunction }
Constructor TExpAverageFunction.Create(AOwner: TComponent);
Begin
  inherited;
  FWeight:=0.2;
End;

Procedure TExpAverageFunction.SetWeight(Const Value:Double);
Begin
  if (Value<0) or (Value>1) then
     raise Exception.Create(TeeMsg_ExpAverageWeight);
  if FWeight<>Value then
  begin
    FWeight:=Value;
    Recalculate;
  end;
End;

Function TExpAverageFunction.Calculate( Series:TChartSeries;
                                        FirstIndex,LastIndex:Integer):Double;
Begin
  With ValueList(Series) do
  begin
    result:=Value[LastIndex];
    if LastIndex>0 then result:=Value[LastIndex-1]*(1.0-FWeight)+result*FWeight;
  end;
end;

{ Momentum }
Function TMomentumFunction.Calculate( Series:TChartSeries;
                                      FirstIndex,LastIndex:Integer):Double;
Begin
  if FirstIndex=TeeAllValues then
  begin
    FirstIndex:=0;
    LastIndex:=Series.Count-1;
  end;
  With ValueList(Series) do result:=Value[LastIndex]-Value[FirstIndex];
End;

{ MomentumDivision }
Function TMomentumDivFunction.Calculate( Series:TChartSeries;
                                      FirstIndex,LastIndex:Integer):Double;
Begin
  if FirstIndex=TeeAllValues then
  begin
    FirstIndex:=0;
    LastIndex:=Series.Count-1;
  end;
  With ValueList(Series) do
  if Value[FirstIndex]=0 then result:=0
                         else result:=100.0*Value[LastIndex]/Value[FirstIndex];
End;

{ StdDeviation }
Function TStdDeviationFunction.CalculateDeviation:Double;
Var Divisor : Double;
begin
  if Complete then Divisor:=Sqr(INumPoints)
              else Divisor:=INumPoints*(INumPoints-1);
  result:=Sqrt( ((INumPoints*ISum2) - Sqr(ISum)) / Divisor );
end;

Procedure TStdDeviationFunction.Accumulate(Const Value:Double);
begin
  ISum:=ISum+Value;
  ISum2:=ISum2+Sqr(Value);
end;

Function TStdDeviationFunction.Calculate(SourceSeries:TChartSeries; FirstIndex,LastIndex:Integer):Double;
var t : Integer;
begin
  if FirstIndex=TeeAllValues then
  begin
    FirstIndex:=0;
    INumPoints:=SourceSeries.Count;
    LastIndex:=INumPoints-1;
  end
  else INumPoints:=LastIndex-FirstIndex+1;
  if INumPoints>1 then
  begin
    ISum2:=0;
    ISum:=0;
    With ValueList(SourceSeries) do
    for t:=FirstIndex to LastIndex do Accumulate(Value[t]);
    result:=CalculateDeviation;
  end
  else result:=0;
end;

Function TStdDeviationFunction.CalculateMany(SourceSeriesList:TList; ValueIndex:Integer):Double;
var t:Integer;
begin
  if SourceSeriesList.Count>0 then
  begin
    INumPoints:=0;
    ISum2:=0;
    ISum:=0;
    for t:=0 to SourceSeriesList.Count-1 do
    begin
      With ValueList(TChartSeries(SourceSeriesList[t])) do
      if Count>ValueIndex then
      begin
        Accumulate(Value[ValueIndex]);
        Inc(INumPoints);
      end;
    end;
    if INumPoints>1 then result:=CalculateDeviation
                    else result:=0;
  end
  else result:=0;
end;

Procedure TStdDeviationFunction.SetComplete(Value:Boolean);
begin
  if FComplete<>Value then
  begin
    FComplete:=Value;
    Recalculate;
  end;
end;

{ THistogramSeries }
Constructor THistogramSeries.Create(AOwner: TComponent);
begin
  inherited;
  FLinesPen:=TChartHiddenPen.Create(CanvasChanged);
  CalcVisiblePoints:=False;
end;

Destructor THistogramSeries.Destroy;
begin
  FLinesPen.Free;
  inherited;
end;

procedure THistogramSeries.SetLinesPen(const Value: TChartHiddenPen);
begin
  FLinesPen.Assign(Value);
end;

class Function THistogramSeries.GetEditorClass:String;
begin
  result:='THistogramSeriesEditor';
end;

Function THistogramSeries.VisiblePoints:Integer;
begin
  result:=ParentChart.MaxPointsPerPage;
  if result=0 then result:=Count;
end;

Procedure THistogramSeries.CalcVerticalMargins(Var TopMargin,BottomMargin:Integer);
begin
  inherited;
  if Pen.Visible then Inc(TopMargin,Pen.Width);
end;

Procedure THistogramSeries.CalcHorizMargins(Var LeftMargin,RightMargin:Integer);
var tmp : Integer;
begin
  inherited;
  tmp:=VisiblePoints;
  if tmp>0 then tmp:=(GetHorizAxis.IAxisSize div VisiblePoints) div 2;
  Inc(LeftMargin,tmp);
  Inc(RightMargin,tmp);
  if Pen.Visible then Inc(RightMargin,Pen.Width);
end;

Procedure THistogramSeries.Assign(Source:TPersistent);
begin
  if Source is THistogramSeries then
  With THistogramSeries(Source) do
  begin
    Self.Pen:=Pen;
    Self.LinesPen:=LinesPen;
    Self.Brush:=Brush;
  end;
  inherited;
end;

procedure THistogramSeries.DrawValue(ValueIndex:Integer);

  Procedure VerticalLine(X,Y0,Y1:Integer);
  begin
    if ParentChart.View3D then
       ParentChart.Canvas.VertLine3D(X,Y0,Y1,MiddleZ)
    else
       ParentChart.Canvas.DoVertLine(X,Y0,Y1);
  end;

  Procedure HorizLine(X0,X1,Y:Integer);
  begin
    if ParentChart.View3D then
       ParentChart.Canvas.HorizLine3D(X0,X1,Y,MiddleZ)
    else
       ParentChart.Canvas.DoHorizLine(X0,X1,Y);
  end;

var R   : TRect;
    tmp : Integer;
begin
  With ParentChart.Canvas do
  begin
    tmp:=(GetHorizAxis.IAxisSize div VisiblePoints) div 2;
    With R do
    begin
      if ValueIndex=FirstValueIndex then
      begin
        Left:=CalcXPos(ValueIndex)-tmp+1;
        Right:=Left+2*tmp;
      end
      else
      begin
        Left:=IPrevious;
        Right:=CalcXPos(ValueIndex)+tmp+1;
      end;
      IPrevious:=Right-1;
      Top:=CalcYPos(ValueIndex);
      With GetVertAxis do
      if Inverted then Bottom:=IStartPos
                  else Bottom:=IEndPos;
    end;
    Pen.Style:=psClear;

    if Self.Brush.Style<>bsClear then
    begin
      AssignBrush(Self.Brush,ValueColor[ValueIndex]);
      if GetVertAxis.Inverted then Inc(R.Top);
      if ParentChart.View3D then RectangleWithZ(Rect(R.Left,R.Top,R.Right-1,R.Bottom),MiddleZ)
                            else DoRectangle(R);
      if GetVertAxis.Inverted then Dec(R.Top);
    end;

    if Self.Pen.Visible then
    begin
      AssignVisiblePen(Self.Pen);
      With R do
      begin
        if ValueIndex=FirstValueIndex then VerticalLine(Left,Bottom,Top)
                                      else VerticalLine(Left,Top,CalcYPos(ValueIndex-1));
        HorizLine(Left,Right,Top);
        if ValueIndex=LastValueIndex then VerticalLine(Right-1,Top,Bottom);
      end;
    end;
    if (ValueIndex>FirstValueIndex) and LinesPen.Visible then
    begin
      tmp:=CalcYPos(ValueIndex-1);
      if GetVertAxis.Inverted then tmp:=MinLong(R.Top,tmp)
                              else tmp:=MaxLong(R.Top,tmp);
      if not Self.Pen.Visible then Dec(tmp);
      AssignVisiblePen(LinesPen);
      VerticalLine(R.Left,R.Bottom,tmp);
    end;
  end;
end;

class procedure THistogramSeries.CreateSubGallery(
  AddSubChart: TChartSubGalleryProc);
begin
  inherited;
  AddSubChart(TeeMsg_Hollow);
  AddSubChart(TeeMsg_NoBorder);
  AddSubChart(TeeMsg_Lines);
end;

class procedure THistogramSeries.SetSubGallery(ASeries: TChartSeries;
  Index: Integer);
begin
  with THistogramSeries(ASeries) do
  Case Index of
    1: Brush.Style:=bsClear;
    2: Pen.Visible:=False;
    3: LinesPen.Visible:=True;
  else inherited;
  end;
end;

{ TStochasticFunction }
{$IFDEF D4}
Destructor TStochasticFunction.Destroy;
begin
  FNums:=nil;
  FDens:=nil;
  inherited;
end;
{$ENDIF}

{$IFDEF D4}
procedure TStochasticFunction.AddPoints(Source: TChartSeries);
begin
  FNums:=nil;
  FDens:=nil;
  SetLength(FNums,Source.Count);
  SetLength(FDens,Source.Count);
  inherited;
end;
{$ENDIF}

function TStochasticFunction.Calculate(Series: TChartSeries; FirstIndex,
  LastIndex: Integer): Double;
var Lows    : TChartValueList;
    Highs   : TChartValueList;
    tmpLow  : Double;
    tmpHigh : Double;
    t       : Integer;
begin
  result:=0;
  With Series do
  Begin
    Lows   :=GetYValueList('LOW');
    Highs  :=GetYValueList('HIGH');
    tmpLow :=Lows.Value[FirstIndex];
    tmpHigh:=Highs.Value[FirstIndex];
    for t:=FirstIndex to LastIndex do
    begin
      if Lows.Value[t] <tmpLow  then tmpLow :=Lows.Value[t];
      if Highs.Value[t]>tmpHigh then tmpHigh:=Highs.Value[t];
    end;
    FNums[LastIndex]:=ValueList(Series).Value[LastIndex]-tmpLow;
    FDens[LastIndex]:=tmpHigh-tmpLow;
    if tmpHigh<>tmpLow then result:=100.0*(FNums[LastIndex]/FDens[LastIndex]);
  end;
end;

{ TRMSFunction }
procedure TRMSFunction.Accumulate(const Value: Double);
begin
  ISum2:=ISum2+Sqr(Value);
end;

function TRMSFunction.Calculate(SourceSeries: TChartSeries; FirstIndex,
  LastIndex: Integer): Double;
var t : Integer;
begin
  if FirstIndex=TeeAllValues then
  begin
    FirstIndex:=0;
    INumPoints:=SourceSeries.Count;
    LastIndex:=INumPoints-1;
  end
  else INumPoints:=LastIndex-FirstIndex+1;
  if INumPoints>1 then
  begin
    ISum2:=0;
    With ValueList(SourceSeries) do
    for t:=FirstIndex to LastIndex do Accumulate(Value[t]);
    result:=CalculateRMS;
  end
  else result:=0;
end;

function TRMSFunction.CalculateMany(SourceSeriesList: TList;
  ValueIndex: Integer): Double;
var t:Integer;
begin
  if SourceSeriesList.Count>0 then
  begin
    INumPoints:=0;
    ISum2:=0;
    for t:=0 to SourceSeriesList.Count-1 do
    begin
      With ValueList(TChartSeries(SourceSeriesList[t])) do
      if Count>ValueIndex then
      begin
        Accumulate(Value[ValueIndex]);
        Inc(INumPoints);
      end;
    end;
    if INumPoints>1 then result:=CalculateRMS
                    else result:=0;
  end
  else result:=0;
end;

function TRMSFunction.CalculateRMS: Double;
Var Divisor : Double;
begin
  if Complete then Divisor:=INumPoints
              else Divisor:=INumPoints-1;
  { safeguard against only one point }
  Result:=Sqrt(ISum2 / Divisor );
end;

procedure TRMSFunction.SetComplete(const Value: Boolean);
begin
  if FComplete<>Value then
  begin
    FComplete:=Value;
    Recalculate;
  end;
end;

{ TMACDFunction }
constructor TMACDFunction.Create(AOwner: TComponent);
begin
  inherited;
  IMoving1:=TExpMovAveFunction.Create(nil);
  IMoving1.Period:=12;
  ISeries1:=TChartSeries.Create(nil);
  ISeries1.SetFunction(IMoving1);
  IMoving2:=TExpMovAveFunction.Create(nil);
  IMoving2.Period:=26;
  ISeries2:=TChartSeries.Create(nil);
  ISeries2.SetFunction(IMoving2);
  Period:=IMoving2.Period;
end;

destructor TMACDFunction.Destroy;
begin
  ISeries1.Free;
  ISeries2.Free;
  inherited;
end;

procedure TMACDFunction.AddPoints(Source: TChartSeries);
var t : Integer;
begin
  ParentSeries.Clear;
  With Source do
  if Count>1 then
  begin
    IMoving1.AddPoints(Source);
    IMoving2.Period:=Self.Period;
    IMoving2.AddPoints(Source);
    for t:=0 to Count-1 do
        ParentSeries.AddXY( XValues.Value[t],ISeries1.YValues.Value[t]-ISeries2.YValues.Value[t]
           {$IFNDEF D4},'',clTeeColor{$ENDIF});
    ISeries1.Clear;
    ISeries2.Clear;
  end;
end;

function TMACDFunction.GetPeriod2: Double;
begin
  result:=IMoving1.Period
end;

procedure TMACDFunction.SetPeriod2(const Value: Double);
begin
  if IMoving1.Period<>Value then
  begin
    IMoving1.Period:=Value;
    if IMoving1.Period<1 then IMoving1.Period:=1;
    Recalculate;
  end;
end;

{ TExpMovAveFunction }
constructor TExpMovAveFunction.Create(AOwner: TComponent);
begin
  inherited;
  CanUsePeriod:=False;
  InternalSetPeriod(10);
end;

procedure TExpMovAveFunction.AddPoints(Source: TChartSeries);
var tmpV : TChartValueList;
    Old  : Double;
    t    : Integer;
    tmp  : Double;
begin
  ParentSeries.Clear;
  if Period>0 then
  With Source do
  if Count>1 then
  begin
    tmpV:=ValueList(Source);
    tmp:=2/(Self.Period+1);
    Old:=0;
    for t:=0 to Count-1 do
    begin
      if t=0 then Old:=tmpV.Value[0]
             else Old:=(tmpV.Value[t]*tmp)+(Old*(1-tmp));
      ParentSeries.AddXY( XValues.Value[t],Old {$IFNDEF D4},'',clTeeColor{$ENDIF});
    end;
  end;
end;

type TChartSeriesAccess=class(TChartSeries);

{ TBollingerFunction }
constructor TBollingerFunction.Create(AOwner: TComponent);
begin
  inherited;
  Exponential:=True;
  Deviation:=2;
  IOther:=TFastLineSeries.Create(Self);
  IOther.ShowInLegend:=False;
  TChartSeriesAccess(IOther).InternalUse:=True;
  InternalSetPeriod(10);
end;

procedure TBollingerFunction.AddPoints(Source:TChartSeries);
Var AList : TChartValueList;

  Function StdDev(First,Last:Integer):Double;
  var ISum     : Double;
      ISum2    : Double;
      t        : Integer;
  begin
    ISum:=0;
    ISum2:=0;
    for t:=First to Last do
    begin
      ISum:=ISum+AList.Value[t];
      ISum2:=ISum2+Sqr(AList.Value[t]);
    end;
    result:=Sqrt( ((Period*ISum2) - Sqr(ISum)) / Sqr(Period) );
  end;

  Procedure InternalAddPoints(ASeries:TChartSeries; ADeviation:Integer);
  var Mov       : TTeeFunction;
      tmp       : TChartSeries;
      tmpValue  : Double;
      tmpPeriod : Integer;
      t         : Integer;
      tmpSource : String;
  begin
    if Exponential then Mov:=TExpMovAveFunction.Create(nil)
                   else Mov:=TMovingAverageFunction.Create(nil);
    Mov.Period:=Period;

    tmp:=TChartSeries.Create(nil);
    try
      tmp.ParentChart:=Source.ParentChart;
      tmp.DataSource:=Source;
      tmpSource:=ParentSeries.MandatoryValueList.ValueSource;
      if tmpSource='' then tmpSource:=Source.MandatoryValueList.Name;
      tmp.MandatoryValueList.ValueSource:=tmpSource;
      tmp.SetFunction(Mov);

      ASeries.Clear;
      AList:=Source.GetYValueList(tmpSource);
      tmpPeriod:=Round(Period);
      for t:=tmpPeriod to Source.Count do
      begin
        tmpValue:=(ADeviation*StdDev(t-tmpPeriod,t-1));
        if Exponential then
           tmpValue:=tmp.YValues.Value[t-1]+tmpValue
        else
           tmpValue:=tmp.YValues.Value[t-tmpPeriod]+tmpValue;
        ASeries.AddXY(Source.XValues.Value[t-1],tmpValue{$IFNDEF D4},'',clTeeColor{$ENDIF});
      end;
      tmp.DataSource:=nil;
    finally
      tmp.Free;
    end;
  end;

begin
  With IOther do
  begin
    ParentChart:=ParentSeries.ParentChart;
    CustomVertAxis:=ParentSeries.CustomVertAxis;
    VertAxis:=ParentSeries.VertAxis;
    SeriesColor:=ParentSeries.SeriesColor;
    if ClassType=ParentSeries.ClassType then
       Pen.Assign(ParentSeries.Pen);
    XValues.DateTime:=ParentSeries.XValues.DateTime;
    AfterDrawValues:=ParentSeries.AfterDrawValues;
    BeforeDrawValues:=ParentSeries.BeforeDrawValues;
  end;
  InternalAddPoints(ParentSeries,Deviation);
  InternalAddPoints(IOther,-Deviation);
end;

procedure TBollingerFunction.SetDeviation(const Value: Integer);
begin
  if FDeviation<>Value then
  begin
    FDeviation:=Value;
    ReCalculate;
  end;
end;

procedure TBollingerFunction.SetExponential(const Value: Boolean);
begin
  if FExponential<>Value then
  begin
    FExponential:=Value;
    ReCalculate;
  end;
end;

destructor TBollingerFunction.Destroy;
begin
  IOther.Free;
  inherited;
end;

initialization
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
{
   RegisterTeeFunction( TMovingAverageFunction, TeeMsg_FunctionMovingAverage, TeeMsg_GalleryFinancial, 1 );
   RegisterTeeFunction( TExpMovAveFunction,     TeeMsg_FunctionExpMovAve, TeeMsg_GalleryFinancial, 1 );
   RegisterTeeFunction( TExpAverageFunction,    TeeMsg_FunctionExpAverage, TeeMsg_GalleryExtended,1 );
   RegisterTeeFunction( TRSIFunction,           TeeMsg_FunctionRSI, TeeMsg_GalleryFinancial,1  );
   RegisterTeeFunction( TMomentumFunction,      TeeMsg_FunctionMomentum, TeeMsg_GalleryFinancial,1  );
   RegisterTeeFunction( TMomentumDivFunction,   TeeMsg_FunctionMomentumDiv, TeeMsg_GalleryFinancial,1  );
   RegisterTeeFunction( TStdDeviationFunction,  TeeMsg_FunctionStdDeviation, TeeMsg_GalleryExtended,1  );
   RegisterTeeFunction( TRMSFunction,           TeeMsg_FunctionRMS, TeeMsg_GalleryExtended,1 );
   RegisterTeeFunction( TMACDFunction,          TeeMsg_FunctionMACD, TeeMsg_GalleryFinancial,1  );
   RegisterTeeFunction( TStochasticFunction,    TeeMsg_FunctionStochastic, TeeMsg_GalleryFinancial,1  );
   RegisterTeeFunction( TBollingerFunction,     TeeMsg_FunctionBollinger, TeeMsg_GalleryFinancial,1 );
   RegisterTeeSeries( THistogramSeries,TeeMsg_GalleryHistogram,TeeMsg_GalleryStats,1);
}
   RegisterTeeFunction( TMovingAverageFunction,TeeMsg_FunctionMovingAverage,TeeMsg_GalleryFinancial, 1, ftMovingAvrg );
   RegisterTeeFunction( TExpMovAveFunction,    TeeMsg_FunctionExpMovAve,    TeeMsg_GalleryFinancial, 1, ftExpMovAvrg );
   RegisterTeeFunction( TExpAverageFunction,   TeeMsg_FunctionExpAverage,   TeeMsg_GalleryExtended,  1, ftExpAverage );
   RegisterTeeFunction( TRSIFunction,          TeeMsg_FunctionRSI,          TeeMsg_GalleryFinancial, 1, ftRSI );
   RegisterTeeFunction( TMomentumFunction,     TeeMsg_FunctionMomentum,     TeeMsg_GalleryFinancial, 1, ftMomentum );
   RegisterTeeFunction( TMomentumDivFunction,  TeeMsg_FunctionMomentumDiv,  TeeMsg_GalleryFinancial, 1, ftMomentumDiv );
   RegisterTeeFunction( TStdDeviationFunction, TeeMsg_FunctionStdDeviation, TeeMsg_GalleryExtended,  1, ftStdDeviation );
   RegisterTeeFunction( TRMSFunction,          TeeMsg_FunctionRMS,          TeeMsg_GalleryExtended,  1, ftRootMeanSq );
   RegisterTeeFunction( TMACDFunction,         TeeMsg_FunctionMACD,         TeeMsg_GalleryFinancial, 1, ftMACD );
   RegisterTeeFunction( TStochasticFunction,   TeeMsg_FunctionStochastic,   TeeMsg_GalleryFinancial, 1, ftStochastic );
   RegisterTeeFunction( TBollingerFunction,    TeeMsg_FunctionBollinger,    TeeMsg_GalleryFinancial, 1, ftBollinger );
   RegisterTeeSeries  ( THistogramSeries,      TeeMsg_GalleryHistogram,     TeeMsg_GalleryStats,     1, stHistogram );
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
finalization
   UnRegisterTeeFunctions([ TMovingAverageFunction,
                            TExpMovAveFunction,
                            TExpAverageFunction,
                            TRSIFunction,
                            TMomentumFunction,
                            TMomentumDivFunction,
                            TStdDeviationFunction,
                            TRMSFunction,
                            TMACDFunction,
                            TStochasticFunction,
                            TBollingerFunction ]);
   UnRegisterTeeSeries([THistogramSeries]);
end.
