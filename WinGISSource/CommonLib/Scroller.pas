Unit Scroller;

Interface

Uses Classes,Controls,Forms,Graphics,Messages,Windows,GrTools;

Type TGraphicSite       = Class(TComponent)
      Protected
       FCanvas          : TCanvas;
       FCosRotation     : Double;
       FRotation        : Double;
       FXOffset         : Double;
       FYOffset         : Double;
       FScale           : Double;
       FSinRotation     : Double;
       FYDown           : Boolean;
       Procedure   SetCanvas(ACanvas:TCanvas);
       Procedure   SetRotation(Const ARotation:Double); virtual;
       Procedure   SetScale(Const AScale:Double); virtual;
      Public
       Constructor Create(AOwner:TComponent); override;
       Property    Canvas:TCanvas read FCanvas write SetCanvas;
       Procedure   InternalToWindow(Const FromX,FromY:Double;var ToX,ToY:Double); overload;
       Procedure   InternalToWindow(var Point:TGrPoint); overload;
       Function    InternalToWindow(const WinDist:Double):Double; overload;
       Procedure   InternalToWindow(var Rect:TGrRect); overload;
       Procedure   InternalToWindow(Const Point:TGrPoint;var Result:TPoint); overload;
       Procedure   InternalToWindow(Const Rect:TGrRect;var Result:TRect); overload;
       Property    Rotation:Double read FRotation write SetRotation;
       Property    Scale:Double read FScale write SetScale;
       Procedure   WindowToInternal(Const FromX,FromY:Double;var ToX,ToY:Double); overload;
       Procedure   WindowToInternal(var Point:TGrPoint); overload;
       Function    WindowToInternal(const WinDist:Double):Double; overload;
       Procedure   WindowToInternal(var Rect:TGrRect); overload;
       Property    XOffset:Double read FXOffset write FXOffset;
       Property    YOffset:Double read FYOffset write FYOffset;
     end;

     TScroller          = Class(TGraphicSite)
      Private
       FAutoHideScroll  : Boolean;
       FCurrentView     : TRotRect;
       FOrgWndProc      : Pointer;
       FHookWndProc     : Pointer;
       FFlatScrollBars  : Boolean;
       FFrame           : Integer;
       FMaxScale        : Double;
       FMinScale        : Double;
       FOnScroll        : TNotifyEvent;
       FOnZoom          : TNotifyEvent;
       FScrollSteps     : Integer;
       FScrollXOffset   : Double;
       FScrollXStep     : Double;
       FScrollYOffset   : Double;
       FScrollYStep     : Double;
       FSize            : TRotRect;
       FStepsPerPage    : Integer;
       FUpdateLocks     : Integer;
       FScalePending    : Boolean;
       FScrollerPending : Boolean;
       FWindow          : TWinControl;
       FUpdating        : Boolean;
       Function    ClientRect(SubHorzScroll,SubVertScroll:Boolean):TRect;
       Function    GetScrollXOffset:Double;
       Function    GetScrollYOffset:Double;
       Procedure   HookWindow;
       Procedure   SetCurrentView(Const ARect:TRotRect);
       Procedure   SetSize(Const ASize:TRotRect);
       Procedure   SetWindow(AWindow:TWinControl);
       Procedure   UnhookWindow;
       Procedure   UpdateOffsets;
       Procedure   UpdateScale;
       Procedure   UpdateScrollbars;
       Procedure   WindowHookProc(var Message:TMessage);
       Procedure   DoScroll(ScrollBar:Integer;ScrollCode:Integer);
      Protected
       Procedure   Notification(AComponent:TComponent;Operation:TOperation); override;
       Procedure   SetRotation(Const ARotation:Double); override;
       Procedure   SetScale(Const AScale:Double); override;
      Public
       Property    CurrentView:TRotRect read FCurrentView write SetCurrentView;
       Procedure   InvalidateRect(const Rect:TGrRect); overload;
       Procedure   InvalidateRect(const Rect:TRect); overload;
       Procedure   LockUpdate;
       Property    ScrollXOffset:Double read GetScrollXOffset;
       Property    ScrollYOffset:Double read GetScrollYOffset;
       Property    ScrollXStep:Double read FScrollXStep;
       Property    ScrollYStep:Double read FScrollYStep;
       Property    Size:TRotRect read FSize write SetSize;
       Procedure   UnlockUpdate;
       Procedure   ZoomAll;
       Procedure   ZoomByFactor(Const Factor:Double);
       Procedure   ZoomByFactorWithCenter(Const Center:TGrPoint;Const Factor:Double);
      Published
       Constructor Create(AOwner:TComponent); override;
       Destructor  Destroy; override;
       Property    OnScroll:TNotifyEvent read FOnScroll write FOnScroll;
       Property    OnZoom:TNotifyEvent read FOnZoom write FOnZoom;
       Property    AutoHideScrollers:Boolean read FAutoHideScroll write FAutoHideScroll;
       Property    Frame:Integer read FFrame write FFrame;
       Property    FlatScrollbars:Boolean read FFlatScrollbars write FFlatScrollbars default FALSE;
       Property    MaxScale:Double read FMaxScale write FMaxScale;
       Property    MinScale:Double read FMinScale write FMinScale;
       Property    ScrollSteps:Integer read FScrollSteps write FScrollSteps;
       Property    StepsPerPage:Integer read FStepsPerPage write FStepsPerPage default 20;
       Property    Window:TWinControl read FWindow write SetWindow;
     end;

Implementation

Uses FlatSB,NumTools,SysUtils;

Constructor TScroller.Create(AOwner:TComponent);
begin
  inherited Create(AOwner);
  FCurrentView:=RotRect(0,0,1,1,0);
  FSize:=FCurrentView;
  FMaxScale:=100;
  FMinScale:=1E-20;
  FStepsPerPage:=20;
  FHookWndProc:=MakeObjectInstance(WindowHookProc);
end;

Destructor TScroller.Destroy;
begin
  FreeObjectInstance(FHookWndProc);
  inherited Destroy;
end;

Procedure TScroller.SetCurrentView(Const ARect:TRotRect);
begin
  if not RectsEqual(ARect,FCurrentView) then begin
    FCurrentView:=ARect;
    UpdateScale;
    UpdateScrollbars;
  end;                  
end;

Procedure TScroller.SetSize(Const ASize:TRotRect);
begin
  if not RectsEqual(ASize,FSize) then begin
    FSize:=ASize;
    UpdateScale;
    UpdateScrollbars;
  end;
end;
                                                  
Procedure TScroller.UpdateScale;
var WorldSize    : TGrRect;
    WorldRect    : TGrRect;
    Current      : TRotRect;
    WindowRect   : TRect;
    Zoom1        : Double;
    Zoom2        : Double;
begin
  if FUpdateLocks>0 then FScalePending:=TRUE
  else begin
    FUpdating:=TRUE;
    { Begrenzungsrechtecke für Projektgröße und Zoom-Rechteck im World-
    + Koordinatensystem berechen }
    WorldSize:=ISOFrame(Size);
    Current:=FCurrentView;
    Current.Rotation:=Current.Rotation+FRotation;
    WorldRect:=ISOFrame(Current);
    { Fenstergröße in virtuellen Einheiten berechnen und Breite der Scroller
    + abziehen }
    WindowRect:=ClientRect(RectWidth(WorldSize)>RectWidth(WorldRect)+Tolerance,
        RectHeight(WorldSize)>RectHeight(WorldRect));
    { Zoom-Faktoren berechnen }
    if RectWidth(WorldSize)>=RectWidth(WorldRect) then
         Zoom1:=Abs((RectWidth(WindowRect)-2*FFrame)/RectWidth(WorldRect))
    else Zoom1:=Abs((RectWidth(WindowRect))/RectWidth(WorldRect));
    if RectHeight(WorldSize)>=RectHeight(WorldRect) then
        Zoom2:=Abs((RectHeight(WindowRect)-2*FFrame)/RectHeight(WorldRect))
    else Zoom2:=Abs((RectHeight(WindowRect))/RectHeight(WorldRect));
    { take the smaller of the two scales }
    FScale:=Max(FMinScale,Min(FMaxScale,Min(Zoom1,Zoom2)));
    if Assigned(FOnZoom) then FOnZoom(Self);
    FScalePending:=FALSE;
    FUpdating:=FALSE;                   
  end;
end;

Procedure TScroller.UpdateScrollbars;
var XScrollInfo  : TScrollInfo;
    YScrollInfo  : TScrollInfo;
    XSteps       : Double;
    YSteps       : Double;
    XPage        : Integer;
    YPage        : Integer;
    WorldSize    : TGrRect;
    WorldView    : TGrRect;
    WindowRect   : TRect;
    WindowWidth  : Integer;
    WindowHeight : Integer;
Const Steps      = 20; 
begin
  if FUpdateLocks>0 then FScrollerPending:=TRUE
  else begin
    FUpdating:=TRUE;
    // Projektgröße in World-Koordinaten berechen
    WorldSize:=ISOFrame(RotatedRect(Size,FRotation));
    WorldView:=ISOFrame(RotatedRect(FCurrentView,FRotation));
    // calculate project and view-size in virtual units
    ScaleRect(WorldSize,FScale,FScale);
    InflateGrRect(WorldSize,FFrame,FFrame); 
    ScaleRect(WorldView,FScale,FScale);
    // calculate window-width in virtual units, subtract scroller-size if
    // necessary
    WindowRect:=ClientRect(FALSE,FALSE);
    WindowRect:=ClientRect(DblCompare(RectWidth(WorldSize),RectWidth(WindowRect))=1,
        DblCompare(RectHeight(WorldSize),RectHeight(WindowRect))=1);
    WindowWidth:=RectWidth(WindowRect);
    WindowHeight:=RectHeight(WindowRect);
    { Anzahl und Schrittweite der Scroller-Schritte berechnen }
    XSteps:=RectWidth(WorldSize)/Max(1,WindowWidth);
    XPage:=Min(Steps,Trunc(MaxInt/XSteps));
    XSteps:=XSteps*XPage;
    YSteps:=RectHeight(WorldSize)/Max(1,WindowHeight);
    YPage:=Min(Steps,Trunc(MaxInt/YSteps));
    YSteps:=YSteps*YPage;
    if XSteps=0 then FScrollXStep:=0   
    else FScrollXStep:=RectWidth(WorldSize)/XSteps;
    if YSteps=0 then FScrollYStep:=0
    else FScrollYStep:=RectHeight(WorldSize)/YSteps;
    // Scrollbar-Ranges updaten
    if DblCompare(XSteps,Steps)<>1 then begin
      XScrollInfo.nMax:=0;
      FScrollXOffset:=WorldSize.Left+(RectWidth(WorldSize)-WindowWidth)/2.0;
    end
    else begin
      XScrollInfo.nMax:=Round(XSteps);
      FScrollXOffset:=WorldSize.Left;
    end;
    if DblCompare(YSteps,Steps)<>1 then begin
      YScrollInfo.nMax:=0;
      FScrollYOffset:=WorldSize.Top-(RectHeight(WorldSize)-WindowHeight)/2.0;
    end
    else begin
      YScrollInfo.nMax:=Round(YSteps);
      FScrollYOffset:=WorldSize.Top;
    end;
    // Scrollbar-Positionen berechnen
    XScrollInfo.nPos:=Max(0,Round((XSteps/RectWidth(WorldSize))
        *((WorldView.Right+WorldView.Left-WindowWidth)/2.0-WorldSize.Left)));
    YScrollInfo.nPos:=Max(0,Round((YSteps/RectHeight(WorldSize))
        *(WorldSize.Top-(WorldView.Top+WorldView.Bottom+WindowHeight)/2.0)));
    XScrollInfo.cbSize:=SizeOf(XScrollInfo);
    XScrollInfo.fMask:=SIF_RANGE or SIF_PAGE or SIF_POS;
    XScrollInfo.nMin:=0;
    XScrollInfo.nPage:=XPage;
    YScrollInfo.cbSize:=SizeOf(YScrollInfo);
    YScrollInfo.fMask:=SIF_RANGE or SIF_PAGE or SIF_POS;
    YScrollInfo.nMin:=0;
    YScrollInfo.nPage:=YPage;
    if FFlatScrollBars then begin
      FlatSB_SetScrollInfo(Window.Handle,sb_Horz,XScrollInfo,TRUE);
      FlatSB_SetScrollInfo(Window.Handle,sb_Vert,YScrollInfo,TRUE);
    end
    else begin
      SetScrollInfo(Window.Handle,sb_Horz,XScrollInfo,TRUE);
      SetScrollInfo(Window.Handle,sb_Vert,YScrollInfo,TRUE);
    end;
    UpdateOffsets;
    Window.Invalidate;
    FScrollerPending:=FALSE;
    FUpdating:=FALSE;
  end;
end;

Function TScroller.ClientRect(SubHorzScroll:Boolean;SubVertScroll:Boolean):TRect;
var MinRange     : Integer;
    MaxRange     : Integer;
begin
  GetClientRect(FWindow.Handle,Result);
  if FAutoHideScroll then begin
    if FFlatScrollBars then FlatSB_GetScrollRange(FWindow.Handle,sb_Horz,MinRange,MaxRange)
    else GetScrollRange(FWindow.Handle,sb_Horz,MinRange,MaxRange);
    if GetWindowLong(FWindow.Handle,GWL_STYLE) and ws_HScroll=0 then MinRange:=MaxRange;
    if (MinRange=MaxRange) XOr not SubHorzScroll then begin
       if SubHorzScroll then Dec(Result.Bottom,GetSystemMetrics(sm_CYHScroll))
       else Inc(Result.Bottom,GetSystemMetrics(sm_CYHScroll));
    end;
    if FFlatScrollBars then FlatSB_GetScrollRange(FWindow.Handle,sb_Vert,MinRange,MaxRange)
    else GetScrollRange(FWindow.Handle,sb_Vert,MinRange,MaxRange);
    if GetWindowLong(FWindow.Handle,GWL_STYLE) and ws_VScroll=0 then MinRange:=MaxRange;
    if (MinRange=MaxRange) XOr not SubVertScroll then begin
       if SubVertScroll then Dec(Result.Right,GetSystemMetrics(sm_CXVScroll))
       else Inc(Result.Right,GetSystemMetrics(sm_CXVScroll));
    end;
  end;  
  if Assigned(FCanvas) then DPToLP(Canvas.Handle,Result,2);
end;

Function TScroller.GetScrollXOffset:Double;
begin
  if FFlatScrollBars then Result:=Round(FScrollXOffset
      +FlatSB_GetScrollPos(FWindow.Handle,sb_Horz)*FScrollXStep)
  else Result:=Round(FScrollXOffset
      +GetScrollPos(FWindow.Handle,sb_Horz)*FScrollXStep);
end;

Function TScroller.GetScrollYOffset:Double;
begin
  if FFlatScrollBars then Result:=Round(FScrollYOffset
      -FlatSB_GetScrollPos(FWindow.Handle,sb_Vert)*FScrollYStep)
  else Result:=Round(FScrollYOffset
      -GetScrollPos(FWindow.Handle,sb_Vert)*FScrollYStep);
end;

Procedure TScroller.WindowHookProc(var Message:TMessage);
var DoInherited  : Boolean;
begin
  with Message do begin
    DoInherited:=FALSE;
    case Msg of
      wm_HScroll     : DoScroll(sb_Horz,TWMHScroll(Message).ScrollCode);
      wm_VScroll     : DoScroll(sb_Vert,TWMHScroll(Message).ScrollCode);
      {$IFDEF WIN32}
      cm_RecreateWnd,
      {$ENDIF}
      wm_Destroy     : UnhookWindow;
      else DoInherited:=TRUE;              
    end;
    if DoInherited then Result:=CallWindowProc(FOrgWndProc,FWindow.Handle,Msg,wParam,lParam);
    case Msg of
    {$IFDEF WIN32}
      cm_RecreateWnd : HookWindow;
    {$ENDIF}
      wm_Size        : if not FUpdating then begin
          UpdateScale;
          UpdateScrollbars;
        end;  
    end;
  end;   
end;

Procedure TScroller.SetWindow(AWindow:TWinControl);
begin
  UnhookWindow;
  FWindow:=AWindow;
  HookWindow;
end;

Procedure TScroller.HookWindow;
begin
  if FWindow<>NIL then begin
    FOrgWndProc:=Pointer(GetWindowLong(FWindow.Handle,gwl_WndProc));
    SetWindowLong(FWindow.Handle,gwl_WndProc,LongInt(FHookWndProc));
  end;
end;

Procedure TScroller.UnhookWindow;
begin
  if (FWindow<>NIL) and (FOrgWndProc<>NIL) then
      SetWindowLong(FWindow.Handle,gwl_WndProc,LongInt(FOrgWndProc));
end;

{******************************************************************************+
 Procedure TScroller.DoScroll
--------------------------------------------------------------------------------
  Behandelt die Scroller-Meldungen damit Delphi diese nicht bekommt und die
  Scroller durcheinanderbringt. Wird die Scroller-Position verändert, so
  wird das Projekt davon informiert und das gesamte Fenster neu gezeichnet.
+******************************************************************************}
Procedure TScroller.DoScroll(ScrollBar:Integer;ScrollCode:Integer);
var ScrollInfo     : TScrollInfo;
    Position       : Integer;
begin
  with ScrollInfo do begin
    { neue Scroller-Position abhängig vom Ereignis neu berechnen }
    cbSize:=SizeOf(ScrollInfo);
    fMask:=SIF_ALL;
    if FFlatScrollBars then FlatSB_GetScrollInfo(FWindow.Handle,ScrollBar,ScrollInfo)
    else GetScrollInfo(FWIndow.Handle,ScrollBar,ScrollInfo);
    case ScrollCode of
      SB_BOTTOM        : Position:=nMax;
      SB_LINEDOWN      : Position:=nPos+1;
      SB_LINEUP        : Position:=nPos-1;
      SB_PAGEDOWN      : Position:=nPos+FStepsPerPage;
      SB_PAGEUP        : Position:=nPos-FStepsPerPage;
      SB_THUMBPOSITION : Position:=nTrackPos;
      SB_TOP           : Position:=nMin;
      else Position:=nPos;
    end;
    { Scroll-Position auf den Scroll-Bereich einschränken }
    if Position>nMax then Position:=nMax;
    if Position<nMin then Position:=nMin;
    if Position<>nPos then begin
      { Position wurde verändert, Projekt informieren und Fenster neu zeichnen }
      if FFlatScrollBars then FlatSB_SetScrollPos(FWindow.Handle,ScrollBar,Position,True)
      else SetScrollPos(FWindow.Handle,ScrollBar,Position,True);
      UpdateOffsets;
      if Assigned(FOnScroll) then OnScroll(FWindow);
      FWindow.Invalidate;
    end;
  end;
end;

Procedure TScroller.LockUpdate;
  begin
    Inc(FUpdateLocks);
  end;

Procedure TScroller.UnlockUpdate;
  begin
    if FUpdateLocks>0 then begin
      Dec(FUpdateLocks);
      if FUpdateLocks=0 then begin
        if FScalePending then UpdateScale;
        if FScrollerPending then UpdateScrollbars;
      end;
    end;
  end;
                                       
Procedure TScroller.ZoomByFactor(Const Factor:Double);
begin
  InflateRotRect(FCurrentView,(1.0/Factor-1.0)*RectWidth(FCurrentView)/2.0,
      (1.0/Factor-1.0)*RectHeight(FCurrentView)/2.0);
  FScale:=Max(FMinScale,Min(FMaxScale,FScale*Factor));
  if Assigned(FOnZoom) then FOnZoom(Self);
  UpdateScrollbars;
end;

Procedure TScroller.ZoomByFactorWithCenter(Const Center:TGrPoint;Const Factor:Double);
var CenterRect     : TGrPoint;
begin
  CenterRect:=RectCenter(FCurrentView);
  MoveRect(FCurrentView,Center.X-CenterRect.X,Center.Y-CenterRect.Y);
  InflateRotRect(FCurrentView,(1.0/Factor-1.0)*FCurrentView.Width/2.0,
      (1.0/Factor-1.0)*FCurrentView.Height/2.0);
  FScale:=Max(FMinScale,Min(FMaxScale,FScale*Factor));
  if Assigned(FOnZoom) then FOnZoom(Self);
  UpdateScrollbars;
end;
                                
Procedure TScroller.Notification(AComponent:TComponent;Operation:TOperation);
begin
  inherited Notification(AComponent,Operation);
  if (Operation=opRemove) and (AComponent=FWindow) then FWindow:=NIL;
end;

Procedure TScroller.ZoomAll;
begin
  CurrentView:=Size;
end;

procedure TScroller.UpdateOffsets;
begin
  if FFlatScrollBars then begin                    
    FXOffset:=FScrollXOffset+FlatSB_GetScrollPos(Window.Handle,sb_Horz)*FScrollXStep;
    FYOffset:=FScrollYOffset-FlatSB_GetScrollPos(Window.Handle,sb_Vert)*FScrollYStep;
  end
  else begin
    FXOffset:=FScrollXOffset+GetScrollPos(Window.Handle,sb_Horz)*FScrollXStep;
    FYOffset:=FScrollYOffset-GetScrollPos(Window.Handle,sb_Vert)*FScrollYStep;
  end;
end;

procedure TScroller.InvalidateRect(const Rect: TGrRect);
var WindowRect     : TRect;
begin
  InternalToWindow(Rect,WindowRect);
  LPToDP(Canvas.Handle,WindowRect,2);
  Inc(WindowRect.Right); Inc(WindowRect.Bottom);
  Windows.InvalidateRect(FWindow.Handle,@WindowRect,TRUE);
end;

{ TGraphicSite }

constructor TGraphicSite.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FScale:=1;
  FCosRotation:=1;
  FYDown:=FALSE;
end;

Procedure TScroller.SetScale(Const AScale:Double);
begin
  if DblCompare(AScale,FScale)<>0 then ZoomByFactor(AScale/FScale);
end;

Procedure TScroller.SetRotation(Const ARotation:Double);
begin
  if DblCompare(ARotation,FRotation)<>0 then begin
    inherited SetRotation(ARotation);
    UpdateScale;
    UpdateScrollbars;
  end;
end;

procedure TGraphicSite.InternalToWindow(var Point: TGrPoint);
var Win            : TGrPoint;
begin
  Win.X:=(Point.X*FCosRotation-Point.Y*FSinRotation)*FScale-FXOffset;
  if FYDown then Win.Y:=FYOffset-(Point.X*FSinRotation+Point.Y*FCosRotation)*FScale
  else Win.Y:=(Point.X*FSinRotation+Point.Y*FCosRotation)*FScale-FYOffset;
  Point:=Win;
end;

function TGraphicSite.InternalToWindow(const WinDist: Double): Double;
begin
  Result:=WinDist*FScale;
end;

procedure TGraphicSite.InternalToWindow(var Rect: TGrRect);
var Win            : TGrRect;
begin
  Win.Left:=(Rect.Left*FCosRotation-Rect.Top*FSinRotation)*FScale-FXOffset;
  if FYDown then Win.Top:=FYOffset-(Rect.Left*FSinRotation+Rect.Top*FCosRotation)*FScale
  else Win.Top:=(Rect.Left*FSinRotation+Rect.Top*FCosRotation)*FScale-FYOffset;
  Win.Right:=(Rect.Right*FCosRotation-Rect.Bottom*FSinRotation)*FScale-FXOffset;
  if FYDown then Win.Bottom:=FYOffset-(Rect.Right*FSinRotation+Rect.Bottom*FCosRotation)*FScale
  else Win.Bottom:=(Rect.Right*FSinRotation+Rect.Bottom*FCosRotation)*FScale-FYOffset;
  Rect:=Win;
end;

procedure TGraphicSite.InternalToWindow(const Point: TGrPoint;
  var Result: TPoint);
begin
  Result.X:=Round((Point.X*FCosRotation-Point.Y*FSinRotation)*FScale-FXOffset);
  if FYDown then Result.Y:=Round(FYOffset-(Point.X*FSinRotation+Point.Y*FCosRotation)*FScale)
  else Result.Y:=Round((Point.X*FSinRotation+Point.Y*FCosRotation)*FScale-FYOffset);
end;

procedure TGraphicSite.InternalToWindow(const Rect: TGrRect;
  var Result: TRect);
begin
  Result.Left:=Round((Rect.Left*FCosRotation-Rect.Top*FSinRotation)*FScale-FXOffset);
  if FYDown then Result.Top:=Round(FYOffset-(Rect.Left*FSinRotation+Rect.Top*FCosRotation)*FScale)
  else Result.Top:=Round((Rect.Left*FSinRotation+Rect.Top*FCosRotation)*FScale-FYOffset);
  Result.Right:=Round((Rect.Right*FCosRotation-Rect.Bottom*FSinRotation)*FScale-FXOffset);
  if FYDown then Result.Bottom:=Round(FYOffset-(Rect.Right*FSinRotation+Rect.Bottom*FCosRotation)*FScale)
  else Result.Bottom:=Round((Rect.Right*FSinRotation+Rect.Bottom*FCosRotation)*FScale-FYOffset);
end;

procedure TGraphicSite.InternalToWindow(const FromX, FromY: Double; var ToX,
  ToY: Double);
begin     
  ToX:=Round((FromX*FCosRotation-FromY*FSinRotation)*FScale-FXOffset);
  if FYDown then ToY:=Round(FYOffset-(FromX*FSinRotation+FromY*FCosRotation)*FScale)
  else ToY:=Round((FromX*FSinRotation+FromY*FCosRotation)*FScale-FYOffset);
end;

procedure TGraphicSite.WindowToInternal(const FromX, FromY: Double; var ToX,
  ToY: Double);
var AXPos          : Double;
    AYPos          : Double;
begin
  AXPos:=(FromX+FXOffset)/FScale;
  if FYDown then AYPos:=(FYOffset-FromY)/FScale
  else AYPos:=(FromY+FYOffset)/FScale;
  ToX:=AXPos*FCosRotation+AYPos*FSinRotation;
  ToY:=-AXPos*FSinRotation+AYPos*FCosRotation;
end;

procedure TGraphicSite.SetCanvas(ACanvas:TCanvas);
begin
  FCanvas:=ACanvas;
  FYDown:=GetMapMode(FCanvas.Handle)=mm_Text;
end;

procedure TGraphicSite.SetRotation(const ARotation: Double);
begin
  FRotation:=ARotation;
  FCosRotation:=Cos(FRotation);
  FSinRotation:=Sin(FRotation);
end;

procedure TGraphicSite.SetScale(const AScale: Double);
begin
  FScale:=AScale;
end;

Procedure TGraphicSite.WindowToInternal(var Point: TGrPoint);
var AXPos          : Double;
    AYPos          : Double;
begin
  AXPos:=(Point.X+FXOffset)/FScale;
  if FYDown then AYPos:=(FYOffset-Point.Y)/FScale
  else AYPos:=(Point.Y+FYOffset)/FScale;
  Point.X:=AXPos*FCosRotation+AYPos*FSinRotation;
  Point.Y:=-AXPos*FSinRotation+AYPos*FCosRotation;
end;

function TGraphicSite.WindowToInternal(const WinDist: Double): Double;
begin
  Result:=WinDist/FScale;
end;

Procedure TGraphicSite.WindowToInternal(var Rect: TGrRect);
var AXPos          : Double;
    AYPos          : Double;
begin
  AXPos:=(Rect.Left+FXOffset)/FScale;
  if FYDown then AYPos:=(FYOffset-Rect.Top)/FScale
  else AYPos:=(Rect.Top+FYOffset)/FScale;
  Rect.Left:=AXPos*FCosRotation+AYPos*FSinRotation;
  Rect.Top:=-AXPos*FSinRotation+AYPos*FCosRotation;
  AXPos:=(Rect.Right+FXOffset)/FScale;
  AYPos:=(Rect.Bottom+FYOffset)/FScale;
  Rect.Right:=AXPos*FCosRotation+AYPos*FSinRotation;
  Rect.Bottom:=-AXPos*FSinRotation+AYPos*FCosRotation;
end;

procedure TScroller.InvalidateRect(const Rect: TRect);
var WindowRect     : TRect;
begin
  InternalToWindow(GrRect(Rect),WindowRect);
  LPToDP(Canvas.Handle,WindowRect,2);
  Inc(WindowRect.Right); Inc(WindowRect.Bottom);
  Windows.InvalidateRect(FWindow.Handle,@WindowRect,TRUE);
end;

end.
