Unit HTMLHelp;

Interface

Uses Windows;

Const HH_DISPLAY_TOPIC        = $0000;
      HH_HELP_FINDER          = $0000;  // WinHelp equivalent
      HH_DISPLAY_TOC          = $0001;  // not currently implemented
      HH_DISPLAY_INDEX        = $0002;  // not currently implemented
      HH_DISPLAY_SEARCH       = $0003;  // not currently implemented
      HH_SET_WIN_TYPE         = $0004;
      HH_GET_WIN_TYPE         = $0005;
      HH_GET_WIN_HANDLE       = $0006;
      HH_ENUM_INFO_TYPE       = $0007;  // Get Info type name, call repeatedly to enumerate, -1 at end
      HH_SET_INFO_TYPE        = $0008;  // Add Info type to filter.
      HH_SYNC                 = $0009;
      HH_ADD_NAV_UI           = $000A;  // not currently implemented
      HH_ADD_BUTTON           = $000B;  // not currently implemented
      HH_GETBROWSER_APP       = $000C;  // not currently implemented
      HH_KEYWORD_LOOKUP       = $000D;
      HH_DISPLAY_TEXT_POPUP   = $000E;  // display string resource id or text in a popup window
      HH_HELP_CONTEXT         = $000F;  // display mapped numeric value in dwData
      HH_TP_HELP_CONTEXTMENU  = $0010;  // text popup help, same as WinHelp HELP_CONTEXTMENU
      HH_TP_HELP_WM_HELP      = $0011;  // text popup help, same as WinHelp HELP_WM_HELP
      HH_CLOSE_ALL            = $0012;  // close all windows opened directly or indirectly by the caller
      HH_ALINK_LOOKUP         = $0013;  // ALink version of HH_KEYWORD_LOOKUP
      HH_GET_LAST_ERROR       = $0014;  // not currently implemented // See HHERROR.h
      HH_ENUM_CATEGORY        = $0015;	// Get category name, call repeatedly to enumerate, -1 at end
      HH_ENUM_CATEGORY_IT     = $0016;  // Get category info type members, call repeatedly to enumerate, -1 at end
      HH_RESET_IT_FILTER      = $0017;  // Clear the info type filter of all info types.
      HH_SET_INCLUSIVE_FILTER = $0018;  // set inclusive filtering method for untyped topics to be included in display
      HH_SET_EXCLUSIVE_FILTER = $0019;  // set exclusive filtering method for untyped topics to be excluded from display
      HH_SET_GUID             = $001A;  // For Microsoft Installer -- dwData is a pointer to the GUID string

      HH_INTERNAL             = $00FF;  // Used internally.

      HHWIN_PROP_ONTOP            = (1 shl 1);    // Top-most window (not currently implemented);
      HHWIN_PROP_NOTITLEBAR       = (1 shl 2);    // no title bar
      HHWIN_PROP_NODEF_STYLES     = (1 shl 3);    // no default window styles (only HH_WINTYPE.dwStyles);
      HHWIN_PROP_NODEF_EXSTYLES   = (1 shl 4);    // no default extended window styles (only HH_WINTYPE.dwExStyles);
      HHWIN_PROP_TRI_PANE         = (1 shl 5);    // use a tri-pane window
      HHWIN_PROP_NOTB_TEXT        = (1 shl 6);    // no text on toolbar buttons
      HHWIN_PROP_POST_QUIT        = (1 shl 7);    // post WM_QUIT message when window closes
      HHWIN_PROP_AUTO_SYNC        = (1 shl 8);    // automatically ssync contents and index
      HHWIN_PROP_TRACKING         = (1 shl 9);    // send tracking notification messages
      HHWIN_PROP_TAB_SEARCH       = (1 shl 10);   // include search tab in navigation pane
      HHWIN_PROP_TAB_HISTORY      = (1 shl 11);   // include history tab in navigation pane
      HHWIN_PROP_TAB_BOOKMARKS    = (1 shl 12);   // include bookmark tab in navigation pane
      HHWIN_PROP_CHANGE_TITLE     = (1 shl 13);   // Put current HTML title in title bar
      HHWIN_PROP_NAV_ONLY_WIN     = (1 shl 14);   // Only display the navigation window
      HHWIN_PROP_NO_TOOLBAR       = (1 shl 15);   // Don't display a toolbar
      HHWIN_PROP_MENU             = (1 shl 16);   // Menu
      HHWIN_PROP_TAB_ADVSEARCH    = (1 shl 17);   // Advanced FTS UI.
      HHWIN_PROP_USER_POS         = (1 shl 18);   // After initial creation, user controls window size/position

      HHWIN_PARAM_PROPERTIES      = (1 shl 1);    // valid fsWinProperties
      HHWIN_PARAM_STYLES          = (1 shl 2);    // valid dwStyles
      HHWIN_PARAM_EXSTYLES        = (1 shl 3);    // valid dwExStyles
      HHWIN_PARAM_RECT            = (1 shl 4);    // valid rcWindowPos
      HHWIN_PARAM_NAV_WIDTH       = (1 shl 5);    // valid iNavWidth
      HHWIN_PARAM_SHOWSTATE       = (1 shl 6);    // valid nShowState
      HHWIN_PARAM_INFOTYPES       = (1 shl 7);    // valid apInfoTypes
      HHWIN_PARAM_TB_FLAGS        = (1 shl 8);    // valid fsToolBarFlags
      HHWIN_PARAM_EXPANSION       = (1 shl 9);    // valid fNotExpanded
      HHWIN_PARAM_TABPOS          = (1 shl 10);   // valid tabpos
      HHWIN_PARAM_TABORDER        = (1 shl 11);   // valid taborder
      HHWIN_PARAM_HISTORY_COUNT   = (1 shl 12);   // valid cHistory
      HHWIN_PARAM_CUR_TAB         = (1 shl 13);   // valid curNavType

      HHWIN_BUTTON_EXPAND         = (1 shl 1);    // Expand/contract button
      HHWIN_BUTTON_BACK           = (1 shl 2);    // Back button
      HHWIN_BUTTON_FORWARD        = (1 shl 3);    // Forward button
      HHWIN_BUTTON_STOP           = (1 shl 4);    // Stop button
      HHWIN_BUTTON_REFRESH        = (1 shl 5);    // Refresh button
      HHWIN_BUTTON_HOME           = (1 shl 6);    // Home button
      HHWIN_BUTTON_BROWSE_FWD     = (1 shl 7);    // not implemented
      HHWIN_BUTTON_BROWSE_BCK     = (1 shl 8);    // not implemented
      HHWIN_BUTTON_NOTES          = (1 shl 9);    // not implemented
      HHWIN_BUTTON_CONTENTS       = (1 shl 10);   // not implemented
      HHWIN_BUTTON_SYNC           = (1 shl 11);   // Sync button
      HHWIN_BUTTON_OPTIONS        = (1 shl 12);   // Options button
      HHWIN_BUTTON_PRINT          = (1 shl 13);   // Print button
      HHWIN_BUTTON_INDEX          = (1 shl 14);   // not implemented
      HHWIN_BUTTON_SEARCH         = (1 shl 15);   // not implemented
      HHWIN_BUTTON_HISTORY        = (1 shl 16);   // not implemented
      HHWIN_BUTTON_BOOKMARKS      = (1 shl 17);   // not implemented
      HHWIN_BUTTON_JUMP1          = (1 shl 18);
      HHWIN_BUTTON_JUMP2          = (1 shl 19);
      HHWIN_BUTTON_ZOOM           = (1 shl 20);
      HHWIN_BUTTON_TOC_NEXT       = (1 shl 21);
      HHWIN_BUTTON_TOC_PREV       = (1 shl 22);

      HHWIN_DEF_BUTTONS           =
            (HHWIN_BUTTON_EXPAND  or
             HHWIN_BUTTON_BACK    or
             HHWIN_BUTTON_OPTIONS or
             HHWIN_BUTTON_PRINT);

// Button IDs

      IDTB_EXPAND           = 200;
      IDTB_CONTRACT         = 201;
      IDTB_STOP             = 202;
      IDTB_REFRESH          = 203;
      IDTB_BACK             = 204;
      IDTB_HOME             = 205;
      IDTB_SYNC             = 206;
      IDTB_PRINT            = 207;
      IDTB_OPTIONS          = 208;
      IDTB_FORWARD          = 209;
      IDTB_NOTES            = 210; // not implemented
      IDTB_BROWSE_FWD       = 211;
      IDTB_BROWSE_BACK      = 212;
      IDTB_CONTENTS         = 213; // not implemented
      IDTB_INDEX            = 214; // not implemented
      IDTB_SEARCH           = 215; // not implemented
      IDTB_HISTORY          = 216; // not implemented
      IDTB_BOOKMARKS        = 217; // not implemented
      IDTB_JUMP1            = 218;
      IDTB_JUMP2            = 219;
      IDTB_CUSTOMIZE        = 221;
      IDTB_ZOOM             = 222;
      IDTB_TOC_NEXT         = 223;
      IDTB_TOC_PREV         = 224;

// Notification codes

      HHN_FIRST            = UINT(-860);
      HHN_LAST             = UINT(-879);

      HHN_NAVCOMPLETE      = (HHN_FIRST-0);
      HHN_TRACK            = (HHN_FIRST-1);
      HHN_WINDOW_CREATE    = (HHN_FIRST-2);

type tagHHN_NOTIFY = Record
    hdr : NMHDR;
    pszUrl : PChar; // Multi-byte, null-terminated string
end;

     tagHH_POPUP = Record
    cbStruct : Integer;      // sizeof this structure
    hinst : Integer;         // instance handle for string resource
    idString : UINT;      // string resource id, or text id if pszFile is specified in HtmlHelp call
    pszText : PChar;       // used if idString is zero
    pt : TPoint;            // top center of popup window
    clrForeground : TColorRef; // use -1 for default
    clrBackground : TColorRef; // use -1 for default
    rcMargins : TRect;     // amount of space between edges of window and text, -1 for each member to ignore
    pszFont : PChar;       // facename, point size, char set, BOLD ITALIC UNDERLINE
end;

HH_POPUP = tagHH_POPUP;

     tagHH_AKLINK = Record
    cbStruct : Integer;     // sizeof this structure
    fReserved : Bool;    // must be FALSE (really!)
    pszKeywords : PChar;  // semi-colon separated keywords
    pszUrl : PChar;       // URL to jump to if no keywords found (may be NULL)
    pszMsgText : PChar;   // Message text to display in MessageBox if pszUrl is NULL and no keyword match
    pszMsgTitle : PChar;  // Message text to display in MessageBox if pszUrl is NULL and no keyword match
    pszWindow : PChar;    // Window to display URL in
    fIndexOnFail : Bool; // Displays index if keyword lookup fails.
end;

HH_AKLINK = tagHH_AKLINK;

tagNavType = (
    HHWIN_NAVTYPE_TOC,
    HHWIN_NAVTYPE_INDEX,
    HHWIN_NAVTYPE_SEARCH,
    HHWIN_NAVTYPE_BOOKMARKS,
    HHWIN_NAVTYPE_HISTORY   // not implemented
    );

tagIT = (
    IT_INCLUSIVE,
    IT_EXCLUSIVE,
    IT_HIDDEN
    );

     tagHH_ENUM_IT = Record
    cbStruct : Integer;          // size of this structure
    iType : Integer;             // the type of the information type ie. Inclusive, Exclusive, or Hidden
    pszCatName : PChar;        // Set to the name of the Category to enumerate the info types in a category; else NULL
    pszITName : PChar;         // volitile pointer to the name of the infotype. Allocated by call. Caller responsible for freeing
    pszITDescription : PChar;  // volitile pointer to the description of the infotype.
end;

HH_ENUM_IT = tagHH_ENUM_IT;
PHH_ENUM_IT = ^HH_ENUM_IT;

     tagHH_ENUM_CAT = Record
    cbStruct : Integer;          // size of this structure
    pszCatName : PChar;        // volitile pointer to the category name
    pszCatDescription : PChar; // volitile pointer to the category description
end;

HH_ENUM_CAT = tagHH_ENUM_CAT;
PHH_ENUM_CAT = ^HH_ENUM_CAT;

     tagHH_SET_INFOTYPE = Record
    cbStruct : Integer;          // the size of this structure
    pszCatName : PChar;        // the name of the category, if any, the InfoType is a member of.
    pszInfoTypeName : PChar;   // the name of the info type to add to the filter
end;

HH_SET_INFOTYPE = tagHH_SET_INFOTYPE;
PHH_SET_INFOTYPE = ^HH_SET_INFOTYPE;

HH_INFOTYPE = DWORD;
PHH_INFOTYPE = ^HH_INFOTYPE;

tagNAVTAB = (
    HHWIN_NAVTAB_TOP,
    HHWIN_NAVTAB_LEFT,
    HHWIN_NAVTAB_BOTTOM
    );

const HH_MAX_TABS  = 19;  // maximum number of tabs

type

tagHH_TAB = (
    HH_TAB_CONTENTS,
        HH_TAB_INDEX,
    HH_TAB_SEARCH,
    HH_TAB_BOOKMARKS,
    HH_TAB_HISTORY
    );

// HH_DISPLAY_SEARCH Command Related Structures and Constants

const HH_FTS_DEFAULT_PROXIMITY = -1;

type

     tagHH_FTS_QUERY = Record
    cbStruct : Integer;            // Sizeof structure in bytes.
    fUniCodeStrings : Bool;    // TRUE if all strings are unicode.
    pszSearchQuery : PChar;  // String containing the search query.
    iProximity : LongInt;         // Word proximity.
    fStemmedSearch : Bool;     // TRUE for StemmedSearch only.
    fTitleOnly : Bool;         // TRUE for Title search only.
    fExecute : Bool;           // TRUE to initiate the search.
    pszWindow : PChar;       // Window to display in
end;

HH_FTS_QUERY = tagHH_FTS_QUERY;

// HH_WINTYPE Structure

     tagHH_WINTYPE  = Record
    cbStruct : Integer;        // IN: size of this structure including all Information Types
    fUniCodeStrings : Bool; // IN/OUT: TRUE if all strings are in UNICODE
    pszType : PChar;         // IN/OUT: Name of a type of window
    fsValidMembers : DWORD;  // IN: Bit flag of valid members (HHWIN_PARAM_)
    fsWinProperties : DWORD; // IN/OUT: Properties/attributes of the window (HHWIN_)

    pszCaption : PChar;      // IN/OUT: Window title
    dwStyles : DWORD;        // IN/OUT: Window styles
    dwExStyles : DWORD;      // IN/OUT: Extended Window styles
    rcWindowPos : TRect;     // IN: Starting position, OUT: current position
    nShowState : Integer;      // IN: show state (e.g., SW_SHOW)

    hwndHelp : HWND;          // OUT: window handle
    hwndCaller : HWND;        // OUT: who called this window

    paInfoTypes : PHH_INFOTYPE;  // IN: Pointer to an array of Information Types

    // The following members are only valid if HHWIN_PROP_TRI_PANE is set

    hwndToolBar : HWND;      // OUT: toolbar window in tri-pane window
    hwndNavigation : HWND;   // OUT: navigation window in tri-pane window
    hwndHTML : HWND;         // OUT: window displaying HTML in tri-pane window
    iNavWidth : Integer;        // IN/OUT: width of navigation window
    rcHTML : TRect;           // OUT: HTML window coordinates

    pszToc : PChar;         // IN: Location of the table of contents file
    pszIndex : PChar;       // IN: Location of the index file
    pszFile : PChar;        // IN: Default location of the html file
    pszHome : PChar;        // IN/OUT: html file to display when Home button is clicked
    fsToolBarFlags : DWORD; // IN: flags controling the appearance of the toolbar
    fNotExpanded : Bool;   // IN: TRUE/FALSE to contract or expand, OUT: current state
    curNavType : Integer;     // IN/OUT: UI to display in the navigational pane
    tabpos : Integer;         // IN/OUT: HHWIN_NAVTAB_TOP, HHWIN_NAVTAB_LEFT, or HHWIN_NAVTAB_BOTTOM
    idNotify : Integer;       // IN: ID to use for WM_NOTIFY messages
    tabOrder : Array[0..HH_MAX_TABS] of Char;    // IN/OUT: tab order: Contents, Index, Search, History, Favorites, Reserved 1-5, Custom tabs
    cHistory : Integer;       // IN/OUT: number of history items to keep (default is 30)
    pszJump1 : PChar;       // Text for HHWIN_BUTTON_JUMP1
    pszJump2 : PChar;       // Text for HHWIN_BUTTON_JUMP2
    pszUrlJump1 : PChar;    // URL for HHWIN_BUTTON_JUMP1
    pszUrlJump2 : PChar;    // URL for HHWIN_BUTTON_JUMP2
    rcMinSize : TRect;      // Minimum size for window (ignored in version 1)
    cbInfoTypes : Integer;    // size of paInfoTypes;
end;

 HH_WINTYPE = tagHH_WINTYPE;
 PHH_WINTYPE = ^HH_WINTYPE;

tagHHACT_TAB = (
    HHACT_TAB_CONTENTS,
    HHACT_TAB_INDEX,
    HHACT_TAB_SEARCH,
    HHACT_TAB_HISTORY,
    HHACT_TAB_FAVORITES,

    HHACT_EXPAND,
    HHACT_CONTRACT,
    HHACT_BACK,
    HHACT_FORWARD,
    HHACT_STOP,
    HHACT_REFRESH,
    HHACT_HOME,
    HHACT_SYNC,
    HHACT_OPTIONS,
    HHACT_PRINT,
    HHACT_HIGHLIGHT,
    HHACT_CUSTOMIZE,
    HHACT_JUMP1,
    HHACT_JUMP2,
    HHACT_ZOOM,
    HHACT_TOC_NEXT,
    HHACT_TOC_PREV,
    HHACT_NOTES,

    HHACT_LAST_ENUM
    );

     tagHHNTRACK = Record
    hdr : TNMHDR;
    pszCurUrl : PChar;      // Multi-byte, null-terminated string
    idAction : Integer;       // HHACT_ value
    phhWinType : PHH_WINTYPE; // Current window type structure
end;

HHNTRACK = tagHHNTRACK;

     THTMLHelpFunctionA  = Function(Handle:HWND;FileName:PChar;Command:UINT;
                              Data:DWORD):HWND; stdcall;
     THTMLHelpFunctionW  = Function(Handle:HWND;FileName:PWideString;Command:UINT;
                              Data:DWORD):HWND; stdcall;
     THTMLHelpFunction   = THTMLHelpFunctionA;

     THelpHandler       = Class
      Private
       FHTMLHelp             : Boolean;
       FHTMLHelpHandle       : Integer;
       FHTMLHelpFunction     : THTMLHelpFunction;
      Public
       Destructor  Destroy; override;
       Property    HTMLHelp:Boolean read FHTMLHelp write FHTMLHelp;
       Function    OnHelp(Command:Word;Data:LongInt;var CallHelp:Boolean):Boolean;
     end;

Implementation

Uses Forms;

{==============================================================================+
  THelpHandler
+==============================================================================}

Destructor THelpHandler.Destroy;
begin
  Application.OnHelp:=NIL;
  if FHTMLHelpHandle<>0 then FreeLibrary(FHTMLHelpHandle);
  inherited Destroy;
end;

Function THelpHandler.OnHelp(Command:Word;Data:Longint;var CallHelp:Boolean):Boolean;
const HH_HELP_CONTEXT = $f;
begin
  if FHTMLHelp then begin
    Result:=FALSE;
    CallHelp:=FALSE;
    // load help-library and get function-pointer (if not already done and
    // Command is not HELP_QUIT)
    if (FHTMLHelpHandle=0) and (Command<>HELP_QUIT) then begin
      FHTMLHelpHandle:=LoadLibrary('HHCTRL.OCX');
      if FHTMLHelpHandle<>0 then FHTMLHelpFunction:=GetProcAddress(FHTMLHelpHandle,
          'HtmlHelpA');
    end;
    if @FHTMLHelpFunction<>NIL then begin
      // map help-command-numbers to html-commands
      case Command of
        HELP_CONTEXT     : Command:=HH_HELP_CONTEXT;
        HELP_CONTENTS    : begin
            Command:=HH_DISPLAY_TOPIC;
            Data:=0;
          end;
        HELP_CONTEXTMENU : Command:=HH_TP_HELP_CONTEXTMENU;
        HELP_CONTEXTPOPUP: Command:=HH_DISPLAY_TEXT_POPUP;
        HELP_QUIT        : Command:=HH_CLOSE_ALL;
        HELP_KEY         : Command:=HH_KEYWORD_LOOKUP;
        HELP_MULTIKEY    : Command:=HH_ALINK_LOOKUP;
        HELP_WM_HELP     : Command:=HH_TP_HELP_WM_HELP;
        else Exit;
      end;
      Result:=FHTMLHelpFunction(0,PChar(Application.HelpFile),
          Command,Data)<>0;
    end                                      
    else if Command=HELP_QUIT then Command:=HH_CLOSE_ALL;
    // free the object if help shall be quit
    if Command=HH_CLOSE_ALL then Application.OnHelp:=NIL;
  end
  else begin
    Result:=TRUE;
    CallHelp:=TRUE;
  end;
end;

end.
