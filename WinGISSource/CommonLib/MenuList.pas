{******************************************************************************+
  Unit MenuList
--------------------------------------------------------------------------------
  Autor: Martin Forst
--------------------------------------------------------------------------------
  Dient zum Einblenden von Listen in ein Men�. TMenuList kann f�r beliebige
  Listen verwendet werden, TMenuHistroyList ist f�r Listen wie z.B. die
  Liste der vorher ge�ffneten Dateien verwendbar. TMenuHistoryList kann seine
  Eintr�ge aus einem .INI-File lesen und auch dort speichern. Weiters kann die
  Anzahl der Eintr�ge in der Liste begrenzt werden. Wird ein Eintrag ein
  zweites mal eingef�gt, so wird dieser Eintrag an den Beginn der Liste
  gesetzt. TMenuHistoryList kann f�r Delphi-Palette registiert werden.
+******************************************************************************}
Unit Menulist;

Interface

Uses Classes,Menus;

Type {**************************************************************************
     | Type TMenuListNotify
     |--------------------------------------------------------------------------
     | Typ der Funktion, die bei Auswahl eines Listeneintrag aufgerufen wird.
     +*************************************************************************}
     TMenuListNotify    = Procedure(Item:String) of object;

     {*************************************************************************+
      Class TMenuList
     ---------------------------------------------------------------------------
       Definiert eine Liste in einem Men�. Wenn die Liste leer ist, so wird
       unter dem in Position angegebenen Eintrag ein Separator angezeit.
       enth�lt die Liste Eintr�ge, so werden diese in Separatoren
       eingeschlossen. NumerItems gibt an, ob die Eintr�ge im Men� nummeriert
       werden sollen.
     ---------------------------------------------------------------------------
       FHint        = Hinweistext der bei Anwahl eines Men�punkts erscheint
       FItems       = Liste mit den zur Liste geh�renden Men�punkten
       FNumberItems = True, wenn die Eintr�ge durchnummeriert werden sollen
       FOnClick     = Ereignis, das bei Auswahl eines Eintrags aufzurufen ist
       FPosition    = Men�punkt nach dem die Liste eingeblendet werden soll
       FSeparator1  = Separator oberhalb der Liste
       FSepatator2  = Separator unterhalb der Liste
     +*************************************************************************}
     TMenuList     = Class(TComponent)
      Private
     {+++ brovak  BUILD#114   enhancement}
       NoAddHistFlag :boolean;
     {- brovak}
       FHint       : PString;
       FItems      : TStringList;
       FNumberItems: Boolean;
       FOnClick    : TMenuListNotify;
       FPosition   : TMenuItem;
       FSeparator1 : TMenuItem;
       FSeparator2 : TMenuItem;
       Procedure   AddToMenu;
       Procedure   CreateMenuItem(AIndex:Integer);
       Procedure   DoClick(Sender:TObject);
       Function    GetCount:Integer;
       Function    GetCaption(AIndex:Integer):String;
       Function    GetHint:String;
       Procedure   RemoveFromMenu;
       Procedure   RenumberItems;
       Procedure   SetCaption(AIndex:Integer;Const ACaption:String);
       Procedure   SetHint(Const AHint:String);
       Procedure   SetNumberItems(ASet:Boolean);
       Procedure   SetPosition(APosition:TMenuItem);

      Public
       Constructor Create(AOwner:TComponent); override;
       Destructor  Destroy; override;
       Procedure   Add(Const ACaption:String); virtual;
       Property    Captions[AIndex:Integer]:String read GetCaption write SetCaption;
       Procedure   Clear;
       Property    Count:Integer read GetCount;
       Procedure   Delete(AIndex:Integer);
       Procedure   Insert(AIndex:Integer;Const ACaption:String);
       Procedure   MoveItem(FromIndex,ToIndex:Integer);
       Procedure   Remove(Const ACaption:String);
      Published
       Property    Hint:String read GetHint write SetHint;
       Property    NumberItems:Boolean read FNumberItems write SetNumberItems;
       Property    OnClick:TMenuListNotify read FOnClick write FOnClick;
       Property    Position:TMenuItem read FPosition write SetPosition;
       Property    NoAddHistoryFlag:Boolean read NoAddHistFlag write NoAddHistFlag default false;

     end;

     {*************************************************************************+
       Class TMenuList
     ---------------------------------------------------------------------------
       Definiert eine History-Liste in einem Men�. Die Anzahl der Eintr�ge
       kann mittels MaxEntryCount begrenzt werden. IniFileName und
       IniSectionName bestimmen, wo die History-Liste gelesen und gespeichert
       werden soll.
     ---------------------------------------------------------------------------
       FMaxEntries      = maximale Anzahl von Eintr�gen.
       FIniFileName     = Name der Ini-Datei zum Lesen uns Speichern der Liste
       FIniSectionName  = Abschnitts-Name in der Ini-Datei
     +*************************************************************************}
     TMenuHistoryList   = Class(TMenuList)
      Private
       FMaxEntries : Integer;
       FIniFileName: PString;
       FIniSection : PString;
       Function    GetIniFileName:String;
       Function    GetIniSectionName:String;
       Procedure   ReadFromIni;
       Procedure   SetIniFileName(Const AName:String);
       Procedure   SetIniSectionName(Const AName:String);
       Procedure   SetMaxEntryCount(AEntries:Integer);
       Procedure   WriteToIni;
      Public
       Constructor Create(AOwner:TComponent); override;
       Destructor  Destroy; override;
       Procedure   Add(Const ACaption:String); override;
       Procedure   Replace(Const OldCaption,NewCaption:String;MoveToFirst:Boolean);
      Published
       Property    MaxEntryCount:Integer read FMaxEntries write SetMaxEntryCount default 4;
       Property    IniFileName:String read GetIniFileName write SetIniFileName;
       Property    IniSectionName:String read GetIniSectionName write SetIniSectionName;
     end;

Implementation

Uses IniFiles,SysUtils,WinOSInfo;

{==============================================================================+
  TMenuList
+==============================================================================}

Constructor TMenuList.Create(AOwner:TComponent);
begin
  inherited Create(AOwner);
  FItems:=TStringList.Create;
  FHint:=NullStr;
end;

Destructor TMenuList.Destroy;
begin
  DisposeStr(FHint);
  FItems.Free;
  inherited Destroy;
end;

{******************************************************************************+
  Procedure TMenuList.AddToMenu
--------------------------------------------------------------------------------
  F�gt die gesamte Liste neu ins Men� ein, allerdings nur dann, wenn die
  Liste einem Menu zugewiesen ist (in diesem Fall existiert FSeparator1).
+******************************************************************************}
Procedure TMenuList.AddToMenu;
var Cnt          : Integer;
begin
  { wenn Liste einem Men� zugewiesen ist                                     }
  if Assigned(FSeparator1) then begin
    { alle Eintr�ge einf�gen oder updaten, falls bereits vorhanden           }
    for Cnt:=0 to FItems.Count-1 do
      CreateMenuItem(Cnt);
    { Separator unter der Liste einblenden, falls mindestens ein Eintrag     }
    FSeparator2.Visible:=FItems.Count>0;
  end;
end;

{******************************************************************************+
  Function TMenuList.GetCount
--------------------------------------------------------------------------------
  Ermittelt die Anzahl der Eintr�ge in der Liste.
+******************************************************************************}
Function TMenuList.GetCount:Integer;
begin
  Result:=FItems.Count;
end;

{******************************************************************************+
  Function TMenuList.GetCount
--------------------------------------------------------------------------------
  Ermittelt den Namen des Eintrags mit der Nummer AIndex.
+******************************************************************************}
Function TMenuList.GetCaption(AIndex:Integer):String;
begin
  Result:=FItems[AIndex];
end;

{******************************************************************************+
  Function TMenuList.GetHint
--------------------------------------------------------------------------------
  Liefert den Hinweistext f�r die Men�punkte (lediglich Typumwandlung).
+******************************************************************************}
Function TMenuList.GetHint:String;
begin
  Result:=FHint^;
end;

{******************************************************************************+
  Procedure TMenuList.RemoveFromMenu
--------------------------------------------------------------------------------
  Entfernt alle Men�punkte aus dem Men� (inklusive der Separatoren).
+******************************************************************************}
Procedure TMenuList.RemoveFromMenu;
var Cnt          : Integer;
begin
  { wenn bereits einem Men� zugewiesen                                       }
  if Assigned(FSeparator1) and not (csDestroying in FSeparator1.ComponentState) then begin
    { entferne alle Men�punkte aus dem Men� (Zeiger auf die Men�items stehen }
    { in der Objektliste                                                     }
    for Cnt:=0 to FItems.Count-1 do begin
      FItems.Objects[Cnt].Free;
      FItems.Objects[Cnt]:=NIL;
    end;
    { Separatoren entfernen und Speicher freigeben                           }
    FSeparator1.Free;
    FSeparator1:=NIL;
    FSeparator2.Free;
    FSeparator2:=NIL;
  end;
end;

{******************************************************************************+
  Procedure TMenuList.SetCaption
--------------------------------------------------------------------------------
  �ndert den Namen des Eintrags mit der Nummer AIndex und �ndert den Namen
  im Men�. Schreibmethode f�r die Property Captions.
+******************************************************************************}
Procedure TMenuList.SetCaption(AIndex:Integer;Const ACaption:String);
begin
  TMenuItem(FItems.Objects[AIndex]).Caption:=ACaption;
end;

{******************************************************************************+
  Procedure TMenuList.SetHint
--------------------------------------------------------------------------------
  Schreibmethode f�r die Property Hint. Weist FHint den neuen Text zu.
+******************************************************************************}
Procedure TMenuList.SetHint(Const AHint:String);
begin
  AssignStr(FHint,AHint);
end;

{******************************************************************************+
  Procedure TMenuList.RenumberItems
--------------------------------------------------------------------------------
  Nummeriert die Eintr�ge in der Men�liste neu durch oder entfernt die Nummern.
+******************************************************************************}
Procedure TMenuList.RenumberItems;
var Cnt          : Integer;
begin
  if Assigned(FSeparator1) then for Cnt:=0 to FItems.Count-1 do
    if not FNumberItems then TMenuItem(FItems.Objects[Cnt]).Caption:=FItems[Cnt]
    else TMenuItem(FItems.Objects[Cnt]).Caption:='&'+IntToStr(Cnt+1)+' '+FItems[Cnt];
end;

{******************************************************************************+
  Procedure TMenuList.SetNumberItems
--------------------------------------------------------------------------------
  Schreibmethode f�r die Property NumberItems.
+******************************************************************************}
Procedure TMenuList.SetNumberItems(ASet:Boolean);
begin
  if FNumberItems<>ASet then begin                  { �nderung im Status     }
    FNumberItems:=ASet;                             { neuen Status speichern }
    { wenn einem Men� zugewiesen, dann Men�punkte updaten                    }
    if Assigned(FSeparator1) then RenumberItems;
  end;
end;

{******************************************************************************+
  Procedure TMenuList.SetPosition
--------------------------------------------------------------------------------
  Schreibmethode f�r die Property Position. Entfernt die Liste aus dem
  derzeitigen Men� und blendet sie an der neuen Position ein.
+******************************************************************************}
Procedure TMenuList.SetPosition(APosition:TMenuItem);
var AIndex       : Integer;
begin
  RemoveFromMenu;
  FPosition:=APosition;
  if Assigned(FPosition) then begin
    AIndex:=APosition.Parent.IndexOf(APosition);
    FSeparator1:=TMenuItem.Create(APosition.Parent);
    FSeparator1.Caption:='-';
    APosition.Parent.Insert(AIndex+1,FSeparator1);
    FSeparator2:=TMenuItem.Create(APosition.Parent);
    FSeparator2.Caption:='-';
    FSeparator2.Visible:=FALSE;
    APosition.Parent.Insert(AIndex+2,FSeparator2);
    AddToMenu;
  end;
end;

Procedure TMenuList.DoClick(Sender:TObject);
var AIndex       : Integer;
begin
  AIndex:=FItems.IndexOfObject(Sender);
  if (AIndex>-1)
      and Assigned(FOnClick) then
    OnClick(FItems[AIndex]);
end;

Procedure TMenuList.CreateMenuItem
   (
   AIndex          : Integer
   );
  var MenuItem     : TMenuItem;
      BIndex       : Integer;
  begin
    if Assigned(FSeparator1) then begin
      BIndex:=FSeparator1.Parent.IndexOf(FSeparator1)+AIndex+1;
      MenuItem:=TMenuItem.Create(FSeparator1.Parent);
      if not FNumberItems then MenuItem.Caption:=FItems[AIndex]
      else MenuItem.Caption:='&'+IntToStr(AIndex+1)+' '+FItems[AIndex];
      MenuItem.Hint:=Hint;
      MenuItem.OnClick:=DoClick;
      FSeparator1.Parent.Insert(BIndex,MenuItem);
      FItems.Objects[AIndex]:=MenuItem;
    end;
  end;

Procedure TMenuList.Add
   (
   Const ACaption  : String
   );
  var AIndex       : Integer;
  begin
    AIndex:=FItems.Add(ACaption);
    if Assigned(FSeparator1) then begin
      CreateMenuItem(AIndex);
      FSeparator2.Visible:=TRUE;
    end;
  end;

Procedure TMenuList.Delete
   (
   AIndex          : Integer
   );
  begin
    FItems.Objects[AIndex].Free;
    FItems.Delete(AIndex);
  end;

Procedure TMenuList.Remove
   (
   Const ACaption  : String
   );
  var AIndex       : Integer;
  begin
    if FItems.Find(ACaption,AIndex) then Delete(AIndex);
  end;

Procedure TMenuList.MoveItem
   (
   FromIndex       : Integer;
   ToIndex         : Integer
   );
  var SaveCaption  : String;
  begin
    FItems.Objects[FromIndex].Free;
    SaveCaption:=FItems[FromIndex];
    FItems.Delete(FromIndex);
    if ToIndex<=FromIndex then FItems.Insert(ToIndex,SaveCaption)
    else FItems.Insert(ToIndex-1,SaveCaption);
    CreateMenuItem(ToIndex);
    RenumberItems;
  end;

Procedure TMenuList.Clear;
  var Cnt          : Integer;
  begin
    for Cnt:=0 to FItems.Count-1 do
      FItems.Objects[Cnt].Free;
    FItems.Clear;
    if Assigned(FSeparator2) then
      FSeparator2.Visible:=FALSE;
  end;

Procedure TMenuList.Insert
   (
   AIndex          : Integer;
   Const ACaption  : String
   );
  begin
    FItems.Insert(AIndex,ACaption);
    CreateMenuItem(AIndex);
    if Assigned(FSeparator2) then
      FSeparator2.Visible:=TRUE;
    RenumberItems;
  end;

{==============================================================================+
  TMenuHistoryList
+==============================================================================}

Constructor TMenuHistoryList.Create
   (
   AOwner          : TComponent
   );
  begin
    inherited Create(AOwner);
    FIniFilename:=NullStr;
    FIniSection:=NullStr;
    FMaxEntries:=4;
  end;

Destructor TMenuHistoryList.Destroy;
  begin
    DisposeStr(FIniFileName);
    DisposeStr(FIniSection);
    inherited Destroy;
  end;

Function TMenuHistoryList.GetIniFileName
   : String;
  begin
    Result:=FIniFilename^;
  end;

Procedure TMenuHistoryList.SetIniFileName
   (
   Const AName     : String
   );
  begin
    AssignStr(FIniFileName,AName);
    ReadFromIni;
  end;

Function TMenuHistoryList.GetIniSectionName
   : String;
  begin
    Result:=FIniSection^;
  end;

Procedure TMenuHistoryList.SetIniSectionName
   (
   Const AName     : String
   );
  begin
    AssignStr(FIniSection,AName);
    ReadFromIni;
  end;

Procedure TMenuHistoryList.SetMaxEntryCount
   (
   AEntries        : Integer
   );
  var Cnt          : Integer;
  begin
    if AEntries>FMaxEntries then
      for Cnt:=AEntries to FItems.Count-1 do Delete(AEntries);
    FMaxEntries:=AEntries;
    WriteToIni;
  end;

Procedure TMenuHistoryList.Add
   (
   Const ACaption  : String
   );
  var AIndex       : Integer;
  begin
  {+++ brovak  BUILD#114   enhancement
  Procedure was changed. Added additional condition for it-
   NoAddHistFlag. It's necessary for correctly work MenuHistoryList- to MenuHistorylist
   will be added only projects, which was created and saved or opened and
   closed exists
  --- brovak}
    if NoAddHistFlag=false then begin;
    AIndex:=FItems.IndexOf(ACaption);
    if AIndex>-1 then MoveItem(AIndex,0)
    else Insert(0,ACaption);
    if FItems.Count>FMaxEntries then Delete(FItems.Count-1);
    WriteToIni;
    end;
  end;

Procedure TMenuHistoryList.ReadFromIni;
  var Count        : Integer;
      Cnt          : Integer;
      IniFile      : TIniFile;
      s: String;
  begin
    if (FIniFilename^<>'') and (FIniSection^<>'') then begin
      Clear;
      IniFile:=TIniFile.Create(FIniFileName^);
      try
        Count:=IniFile.ReadInteger(FIniSection^,'Count',0);
        if Count>FMaxEntries then Count:=FMaxEntries;
        for Cnt:=0 to Count-1 do begin
          s:=IniFile.ReadString(FIniSection^,'Entry'+IntToStr(Cnt),'');
          if (Pos('\\',s) = 0) and (Pos(Lowercase(OSInfo.CommonAppDataDir),Lowercase(s)) = 0) then begin
             if FileExists(s) then begin
                inherited Add(s);
             end;
          end;
        end;
      finally
        IniFile.Free;
      end;
    end;
  end;

Procedure TMenuHistoryList.WriteToIni;
  var Cnt          : Integer;
      IniFile      : TIniFile;

  begin
    if (FIniFilename^<>'') and (FIniSection^<>'') then begin
      IniFile:=TIniFile.Create(FIniFileName^);
      try
        IniFile.EraseSection(FIniSection^);
        IniFile.WriteInteger(FIniSection^,'Count',FItems.Count);
        for Cnt:=0 to FItems.Count-1 do begin
          if (Pos('\\',Captions[Cnt]) = 0) and (Pos(Lowercase(OSInfo.CommonAppDataDir),Lowercase(Captions[Cnt])) = 0) then begin
             if FileExists(Captions[Cnt]) then begin
                IniFile.WriteString(FIniSection^,'Entry'+IntToStr(Cnt),Captions[Cnt]);
             end;
          end;
        end;
      finally
        IniFile.Free;
      end;
    end;
                
  end;

Procedure TMenuHistoryList.Replace
   (
   Const OldCaption     : String;
   Const NewCaption     : String;
   MoveToFirst          : Boolean
   );
  var AIndex       : Integer;
  begin
    AIndex:=FItems.IndexOf(OldCaption);
    if AIndex>-1 then begin
      FItems[AIndex]:=NewCaption;
      if MoveToFirst then MoveItem(AIndex,0);
      WriteToIni;
    end;
  end;

begin
  Classes.RegisterClass(TMenuHistoryList);
end.
