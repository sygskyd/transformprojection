unit TextPosPanel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls;

type TTextAlign = (taUpperLeft,taCenterLeft,taLowerLeft,taLowerCenter,taLowerRight,taCenterRight,taUpperRight,taUpperCenter,taCenter);

type
  TTextPosPanel = class(TCustomPanel)
  private
    FText        : String;
    FTextAlign   : TTextAlign;
    FTextWidth   : Integer;
    FTextHeight  : Integer;
    FTextRect    : TRect;
    FCenter      : TPoint;
    FDragging    : Boolean;
    FDistance    : Integer;
    FTextPoint   : TPoint;
    XDiff        : Integer;
    YDiff        : Integer;
    procedure    SetTextAlign(AAlign: TTextAlign);
    procedure    SetText(AText: String);
    procedure    SetDistance(ADistance: Integer);
    function     IsInsideRect(X,Y: Integer; ARect: TRect): Boolean;
    procedure    CalcTextRect;
    procedure    CalcTextAlign;
    procedure    CalcTextPos;
    procedure    MsgMouseDown(var Message: TMessage); message WM_LBUTTONDOWN;
    procedure    MsgMouseUp(var Message: TMessage); message WM_LBUTTONUP;
    procedure    MsgMouseMove(var Message: TMessage); message WM_MOUSEMOVE;
    procedure    MsgPosChanged(var Message: TMessage); message WM_WINDOWPOSCHANGED;
  protected
    procedure    Paint; override;
  public
    Constructor Create(AParent:TComponent); override;
  published
    Property    Align;
    Property    Enabled;
    Property    Font;
    Property    BorderStyle;
    Property    Distance: Integer read FDistance write SetDistance;
    Property    TextAlign:TTextAlign read FTextAlign write SetTextAlign;
    Property    Text: String read FText write SetText;
    Property    Visible;
    Property    OnClick;
    Property    OnDblClick;
    Property    OnMouseDown;
    Property    OnMouseMove;
    Property    OnMouseUp;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('InputHandlers', [TTextPosPanel]);
end;

constructor TTextPosPanel.Create(AParent:TComponent);
begin
     inherited Create(AParent);
     Width:=80;
     Height:=60;
     FCenter.X:=Width div 2;
     FCenter.Y:=Height div 2;
     BorderStyle:=bsSingle;
     FText:='Text';
     FTextAlign:=taUpperLeft;
     FDragging:=False;
     FDistance:=15;
end;

procedure TTextPosPanel.Paint;
begin
     FCenter.X:=Width div 2;
     FCenter.Y:=Height div 2;

     Canvas.Font.Assign(Font);

     FTextWidth:=Canvas.TextWidth(FText)+4;
     FTextHeight:=Canvas.TextHeight(FText)+4;

     CalcTextRect;
     CalcTextPos;

     Canvas.Brush.Color:=clWhite;
     Canvas.FillRect(Rect(0,0,Width,Height));

     Canvas.Brush.Color:=clGreen;
     Canvas.Brush.Style:=bsSolid;
     Canvas.FrameRect(FTextRect);

     Canvas.Font.Color:=clBlue;
     Canvas.Brush.Style:=bsClear;
     Canvas.TextOut(FTextRect.Left+2,FTextRect.Top+2,FText);

     Canvas.Pen.Color:=clBlack;
     Canvas.MoveTo(FCenter.X-10,FCenter.Y);
     Canvas.LineTo(FCenter.X+10,FCenter.Y);
     Canvas.MoveTo(FCenter.X,FCenter.Y-10);
     Canvas.LineTo(FCenter.X,FCenter.Y+10);

     Canvas.Pen.Color:=clRed;
     Canvas.MoveTo(FCenter.X,FCenter.Y);
     Canvas.LineTo(FTextPoint.X,FTextPoint.Y);

     Canvas.Brush.Style:=bsSolid;
     Canvas.Brush.Color:=clRed;
     Canvas.Ellipse(FTextPoint.X-4,FTextPoint.Y-4,FTextPoint.X+4,FTextPoint.Y+4);
end;

procedure TTextPosPanel.SetText(AText: String);
begin
     if FText <> AText then begin
        FText:=AText;
        FTextWidth:=Canvas.TextWidth(FText)+10;
        FTextHeight:=Canvas.TextHeight(FText)+10;
        CalcTextRect;
        Invalidate;
     end;
end;

procedure TTextPosPanel.SetTextAlign(AAlign: TTextAlign);
begin
     if FTextAlign <> AAlign then begin
        FTextAlign:=AAlign;
        CalcTextRect;
        CalcTextPos;
        Invalidate;
     end;
end;

procedure TTextPosPanel.SetDistance(ADistance: Integer);
begin
     if FDistance <> ADistance then begin
        FDistance:=ADistance;
        CalcTextRect;
        CalcTextPos;
        Invalidate;
     end;
end;

function TTextPosPanel.IsInsideRect(X,Y: Integer; ARect: TRect): Boolean;
begin
     Result:=(X >= ARect.Left) and (X <= ARect.Right) and (Y >= ARect.Top) and (Y <= ARect.Bottom);
end;

procedure TTextPosPanel.MsgMouseDown(var Message: TMessage);
var
X,Y: Integer;
begin
     X:=LOWORD(Message.lParam);
     Y:=HIWORD(Message.lParam);
     FDragging:=IsInsideRect(X,Y,FTextRect);
     if FDragging then begin
        Cursor:=crDrag;
        XDiff:=X-FTextRect.Left;
        YDiff:=Y-FTextRect.Top;
     end;
end;

procedure TTextPosPanel.MsgMouseMove(var Message: TMessage);
var
X,Y: Integer;
AWidth: Integer;
AHeight: Integer;
ATextAlign: TTextAlign;
begin
     if FDragging then begin
        X:=LOWORD(Message.lParam);
        Y:=HIWORD(Message.lParam);
        AWidth:=FTextRect.Right-FTextRect.Left;
        AHeight:=FTextRect.Bottom-FTextRect.Top;
        FTextRect.Left:=X-XDiff;
        FTextRect.Top:=Y-YDiff;
        FTextRect.Right:=FTextRect.Left+AWidth;
        FTextRect.Bottom:=FTextRect.Top+AHeight;
        ATextAlign:=FTextAlign;
        CalcTextAlign;
        CalcTextPos;
        if FTextAlign <> ATextAlign then begin
           Invalidate;
        end;
     end;
end;

procedure TTextPosPanel.MsgMouseUp(var Message: TMessage);
begin
     inherited;
     Cursor:=crDefault;
     if FDragging then begin
        FDragging:=False;
        CalcTextAlign;
        CalcTextRect;
        CalcTextPos;
        Invalidate;
     end;
end;

procedure TTextPosPanel.CalcTextAlign;
var
VAlign,HAlign: Integer;
begin
     if (FTextRect.Left <= FCenter.X) and (FTextRect.Right <= FCenter.X) then begin
        HAlign:=0;
     end
     else if (FTextRect.Left <= FCenter.X) and (FTextRect.Right >= FCenter.X) then begin
        HAlign:=1;
     end
     else begin
        HALign:=2;
     end;

     if (FTextRect.Top <= FCenter.Y) and (FTextRect.Bottom <= FCenter.Y) then begin
        VAlign:=0;
     end
     else if (FTextRect.Top <= FCenter.Y) and (FTextRect.Bottom >= FCenter.Y) then begin
        VAlign:=1;
     end
     else begin
        VALign:=2;
     end;

     if (HAlign = 0) and (VAlign = 0) then begin
        FTextAlign:=taLowerRight;
     end
     else if (HAlign = 0) and (VAlign = 1) then begin
        FTextAlign:=taCenterRight;
     end
     else if (HAlign = 0) and (VAlign = 2) then begin
        FTextAlign:=taUpperRight;
     end
     else if (HAlign = 1) and (VAlign = 0) then begin
        FTextAlign:=taLowerCenter;
     end
     else if (HAlign = 1) and (VAlign = 1) then begin
        FTextAlign:=taCenter;
     end
     else if (HAlign = 1) and (VAlign = 2) then begin
        FTextAlign:=taUpperCenter;
     end
     else if (HAlign = 2) and (VAlign = 0) then begin
        FTextAlign:=taLowerLeft;
     end
     else if (HAlign = 2) and (VAlign = 1) then begin
        FTextAlign:=taCenterLeft;
     end
     else begin
        FTextAlign:=taUpperLeft;
     end;
end;

procedure TTextPosPanel.CalcTextRect;
begin
     case FTextAlign of
        taUpperLeft:
           begin
                FTextRect.Left:=FCenter.X+FDistance;
                FTextRect.Top:=FCenter.Y+FDistance;
                FTextRect.Right:=FCenter.X+FTextWidth+FDistance;
                FTextRect.Bottom:=FCenter.Y+FTextHeight+FDistance;
           end;
        taCenterLeft:
           begin
                FTextRect.Left:=FCenter.X+FDistance;
                FTextRect.Top:=FCenter.Y-FTextHeight div 2;
                FTextRect.Right:=FCenter.X+FTextWidth+FDistance;
                FTextRect.Bottom:=FCenter.Y+FTextHeight div 2;
           end;
        taLowerLeft:
           begin
                FTextRect.Left:=FCenter.X+FDistance;
                FTextRect.Top:=FCenter.Y-FTextHeight-FDistance;
                FTextRect.Right:=FCenter.X+FTextWidth+FDistance;
                FTextRect.Bottom:=FCenter.Y-FDistance;
           end;
        taLowerCenter:
           begin
                FTextRect.Left:=FCenter.X-FTextWidth div 2;
                FTextRect.Top:=FCenter.Y-FTextHeight-FDistance;
                FTextRect.Right:=FCenter.X+FTextWidth div 2;
                FTextRect.Bottom:=FCenter.Y-FDistance;
           end;
        taLowerRight:
           begin
                FTextRect.Left:=FCenter.X-FTextWidth-FDistance;
                FTextRect.Top:=FCenter.Y-FTextHeight-FDistance;
                FTextRect.Right:=FCenter.X-FDistance;
                FTextRect.Bottom:=FCenter.Y-FDistance;
           end;
        taCenterRight:
           begin
                FTextRect.Left:=FCenter.X-FTextWidth-FDistance;
                FTextRect.Top:=FCenter.Y-FTextHeight div 2;
                FTextRect.Right:=FCenter.X-FDistance;
                FTextRect.Bottom:=FCenter.Y+FTextHeight div 2;
           end;
        taUpperRight:
           begin
                FTextRect.Left:=FCenter.X-FTextWidth-FDistance;
                FTextRect.Top:=FCenter.Y+FDistance;
                FTextRect.Right:=FCenter.X-FDistance;
                FTextRect.Bottom:=FCenter.Y+FTextHeight+FDistance;
           end;
        taUpperCenter:
           begin
                FTextRect.Left:=FCenter.X-FTextWidth div 2;
                FTextRect.Top:=FCenter.Y+FDistance;
                FTextRect.Right:=FCenter.X+FTextWidth div 2;
                FTextRect.Bottom:=FCenter.Y+FTextHeight+FDistance;
           end;
        taCenter:
           begin
                FTextRect.Left:=FCenter.X-FTextWidth div 2;
                FTextRect.Top:=FCenter.Y-FTextHeight div 2;
                FTextRect.Right:=FCenter.X+FTextWidth div 2;
                FTextRect.Bottom:=FCenter.Y+FTextHeight div 2;
           end;
     end;
end;

procedure TTextPosPanel.CalcTextPos;
begin
     case FTextAlign of
        taUpperLeft:
           begin
                FTextPoint.X:=FTextRect.Left;
                FTextPoint.Y:=FTextRect.Top;
           end;
        taCenterLeft:
           begin
                FTextPoint.X:=FTextRect.Left;
                FTextPoint.Y:=(FTextRect.Bottom+FTextRect.Top) div 2;
           end;
        taLowerLeft:
           begin
                FTextPoint.X:=FTextRect.Left;
                FTextPoint.Y:=FTextRect.Bottom;
           end;
        taLowerCenter:
           begin
                FTextPoint.X:=(FTextRect.Right+FTextRect.Left) div 2;
                FTextPoint.Y:=FTextRect.Bottom;
           end;
        taLowerRight:
           begin
                FTextPoint.X:=FTextRect.Right;
                FTextPoint.Y:=FTextRect.Bottom;
           end;
        taCenterRight:
           begin
                FTextPoint.X:=FTextRect.Right;
                FTextPoint.Y:=(FTextRect.Bottom+FTextRect.Top) div 2;
           end;
        taUpperRight:
           begin
                FTextPoint.X:=FTextRect.Right;
                FTextPoint.Y:=FTextRect.Top;
           end;
        taUpperCenter:
           begin
                FTextPoint.X:=(FTextRect.Right+FTextRect.Left) div 2;
                FTextPoint.Y:=FTextRect.Top;
           end;
        taCenter:
           begin
                FTextPoint.X:=(FTextRect.Right+FTextRect.Left) div 2;
                FTextPoint.Y:=(FTextRect.Bottom+FTextRect.Top) div 2;
           end;
     end;
end;

procedure TTextPosPanel.MsgPosChanged(var Message: TMessage);
begin
     inherited;
     CalcTextRect;
     CalcTextPos;
end;

end.
