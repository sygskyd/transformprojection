Unit ProgressDlg;

Interface

Uses WinProcs,WinTypes,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     StdCtrls,ComCtrls,WCtrls, ExtCtrls;

Type TProgressForm     = Class(TWForm)
       Panel1      : TPanel;
       ProgressBar : TProgressBar;
       WLabel      : TWLabel;
       Procedure   FormHide(Sender:TObject);
       Procedure   FormShow(Sender:TObject);
      Private
       FDisabled   : Pointer;
     end;

     TProgressDialog   = Class(TComponent)
      Private
       FCaption         : String;
       FDialog          : TProgressForm;
       Function    GetDialog:TProgressForm;
       Function    GetProgress:Double;
       Procedure   SetProgress(const Value:Double);
      Protected
      Public
       Destructor  Destroy; override;
       Property    Dialog:TProgressForm read GetDialog;
      Published
       Property    Caption:String read FCaption write FCaption;
       Property    Progress:Double read GetProgress write SetProgress;
     end;

Implementation

{$R *.DFM}

Function TProgressDialog.GetDialog:TProgressForm;
begin
  if FDialog=NIL then begin
    FDialog:=TProgressForm.Create(Application);
    FDialog.WLabel.Caption:=FCaption;
    FDialog.Update;
  end;
  Result:=FDialog;
end;

procedure TProgressForm.FormShow(Sender: TObject);
begin
  FDisabled:=DisableTaskWindows(Handle);
end;

procedure TProgressForm.FormHide(Sender: TObject);
begin
  if FDisabled<>NIL then EnableTaskWindows(FDisabled);
end;

function TProgressDialog.GetProgress: Double;
begin
  Result:=Dialog.ProgressBar.Position;
end;

procedure TProgressDialog.SetProgress(const Value: Double);
begin
  Dialog.ProgressBar.Position:=Round(Value);
end;

destructor TProgressDialog.Destroy;
begin
  FDialog.Free;
  inherited Destroy;
end;

end.
