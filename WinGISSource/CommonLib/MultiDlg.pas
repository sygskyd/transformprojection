Unit MultiDlg;

Interface

Uses Classes,Controls,Forms,Lists,Messages,TabNotBk,WCtrls;

Type PDialogInfo   = ^TDialogInfo;
     TDialogInfo   = Record
       Dialog        : TForm;
       ControlParent : TWinControl;
       PageIndex     : Integer;
     end;

     TMultiFormDialog   = Class(TWForm)
      Private
       FCallShowHide    : Boolean;
       FDialogInfos     : TPackedRecList;
       FNotebook        : TTabbedNotebook;
       FRemoveFirstPage : Boolean;
       FOrgOnChange     : TPageChangeEvent;
       Function    GetActiveDialog:TForm;
       Function    GetDialogCount:Integer;
       Function    GetDialog(AIndex:Integer):TForm;
       Function    GetDialogFromTabIndex(AIndex:Integer):TForm;
       Function    GetDialogInfo(AIndex:Integer):PDialogInfo;
       Procedure   SetNotebook(ANotebook:TTabbedNotebook);
       Procedure   DoNotebookChange(Sender:TObject;NewTab:Integer;var AllowChange:Boolean);
      Protected
       Procedure   Activate; override;
       Procedure   Deactivate; override;
       Procedure   DoHide; override;
       Procedure   DoShow; override;
       Procedure   Resize; override;
       Property    DialogInfos[Index:Integer]:PDialogInfo read GetDialogInfo;
      Public
       Constructor Create(AParent:TComponent); override;
       Destructor  Destroy; override;
       Property    ActiveDialog:TForm read GetActiveDialog;
       Function    AddDialog(ADialog:TForm;APage:TWinControl;Const ACaption:String;
                       AddPage:Boolean=FALSE):Integer;
       Property    CallShowHide:Boolean read FCallShowHide write FCallShowHide;
       Function    CloseQuery:Boolean; override;
       Procedure   DeleteDialog(ADialog:TForm);
       Property    DialogCount:Integer read GetDialogCount;
       Property    DialogNotebook:TTabbedNotebook read FNotebook write SetNotebook;
       Property    Dialogs[AIndex:Integer]:TForm read GetDialog;
       Property    RemoveFirstPage:Boolean read FRemoveFirstPage write FRemoveFirstPage;
     end;

Implementation

Uses ExtCtrls,Validate,UserIntf,WinProcs;

Constructor TMultiFormDialog.Create
   (
   AParent         : TComponent
   );
  begin
    FDialogInfos:=TPackedRecList.Create(SizeOf(TDialogInfo));
    FRemoveFirstPage:=TRUE;
    inherited Create(AParent);
  end;

Destructor TMultiFormDialog.Destroy;
begin
  FDialogInfos.Free;
  inherited Destroy;
end;

Function TMultiFormDialog.GetDialogCount:Integer;
begin
  Result:=FDialogInfos.Count;
end;

Function TMultiFormDialog.GetDialog(AIndex:Integer):TForm;
begin
  Result:=DialogInfos[AIndex].Dialog;
end;

Function TMultiFormDialog.AddDialog(ADialog:TForm;APage:TWinControl;
    Const ACaption:String;AddPage:Boolean):Integer;
var Cnt          : Integer;
    TabPage      : TTabPage;
    DialogInfo   : TDialogInfo;
  Procedure SetControlsParent(ParentControl:TWinControl;NewParent:TWinControl);
  var TabList     : TList;
      Cnt         : Integer;
  begin
    TabList:=TList.Create;
    with ParentControl do begin
      GetTabOrderList(TabList);
      while ControlCount>0 do Controls[0].Parent:=NewParent;
      for Cnt:=0 to TabList.Count-1 do TWinControl(TabList[Cnt]).TabOrder:=Cnt;
    end;
    TabList.Free;
  end;
begin
  Result:=FDialogInfos.Count;
  DialogInfo.Dialog:=ADialog;
  DialogInfo.ControlParent:=APage;
  DialogInfo.PageIndex:=FNotebook.Pages.Count;
  FDialogInfos.Add(DialogInfo);
  if APage is TTabbedNotebook then with APage as TTabbedNotebook do begin
    for Cnt:=0 to Pages.Count-1 do begin
      TabPage:=TTabPage(Pages.Objects[Cnt]);
      FNotebook.Pages.Add(TabPage.Caption);
      SetControlsParent(TabPage,TTabPage(FNotebook.Pages.Objects[FNotebook.Pages.Count-1]));
    end;
  end
  else if APage is TPanel and AddPage then begin
    FNotebook.Pages.Add(ACaption);
    APage.Parent:=TTabPage(FNotebook.Pages.Objects[FNotebook.Pages.Count-1])
  end  
  else begin
    FNotebook.Pages.Add(ACaption);
    SetControlsParent(APage,TTabPage(FNotebook.Pages.Objects[FNotebook.Pages.Count-1]));
  end;
  RehookValidators(ADialog);
end;

Procedure TMultiFormDialog.SetNotebook(ANotebook:TTabbedNotebook);
begin
  FNotebook:=ANotebook;
  FOrgOnChange:=FNotebook.OnChange;
  FNotebook.OnChange:=DoNotebookChange;
  if not(csDesigning in ComponentState) and FRemoveFirstPage then FNotebook.Pages.Delete(0);
end;

Procedure TMultiFormDialog.DoHide;
var Cnt          : Integer;
    Dialog       : TForm;
    DoHide       : TNotifyEvent;
    ActiveForm   : TForm;
begin
  // call the hide-method of the currently active page if Show/Hide on
  // page-change is set
  if FCallShowHide then begin
    ActiveForm:=GetDialogFromTabIndex(FNotebook.PageIndex);
    if (ActiveForm<>NIL) and (ActiveForm<>Self) then begin
      DoHide:=ActiveForm.OnHide;
      if Assigned(DoHide) then ActiveForm.OnHide(ActiveForm);
    end;
  end
  // call OnHide of all sub-dialogs
  else for Cnt:=0 to FDialogInfos.Count-1 do begin
    Dialog:=DialogInfos[Cnt].Dialog;
    Dialog.ModalResult:=ModalResult;
    DoHide:=Dialog.OnHide;
    if Assigned(DoHide) then Dialog.OnHide(Dialog);
  end;
  inherited DoHide;
end;

Procedure TMultiFormDialog.DoShow;
var Cnt          : Integer;
    Dialog       : TForm;
    DoShow       : TNotifyEvent;
begin
  UserInterface.BeginWaitCursor;
  try
    inherited DoShow;
    // set the modal-results to mrOK if Show/Hide is called when the active
    // notebook-page is called
    if FCallShowHide then for Cnt:=0 to FDialogInfos.Count-1 do begin
      Dialog:=DialogInfos[Cnt].Dialog;
      Dialog.ModalResult:=mrOK;
    end
    // call OnShow of all sub-dialogs
    else for Cnt:=0 to FDialogInfos.Count-1 do begin
      Dialog:=DialogInfos[Cnt].Dialog;
      DoShow:=Dialog.OnShow;    
      if Assigned(DoShow) then Dialog.OnShow(Dialog);
    end;
  finally
    UserInterface.EndWaitCursor;
  end;
end;

Procedure TMultiFormDialog.Resize;
var Cnt          : Integer;
    Dialog       : TForm;
begin
  inherited;
  // resize all sub-dialogs to realign their controls 
  if FNotebook<>NIL then for Cnt:=0 to FDialogInfos.Count-1 do begin
    Dialog:=DialogInfos[Cnt].Dialog;
    Dialog.BoundsRect:=TWinControl(FNoteBook.Pages.Objects[0]).ClientRect;
  end;
end;

Procedure TMultiFormDialog.Activate;
var ActiveForm   : TForm;
    DoActivate   : TNotifyEvent;
begin
  inherited Activate;
  ActiveForm:=ActiveDialog;
  if ActiveForm<>NIL then begin
    DoActivate:=ActiveForm.OnActivate;
    if Assigned(DoActivate) then ActiveForm.OnActivate(ActiveForm);
  end;
end;

Procedure TMultiFormDialog.Deactivate;
var ActiveForm   : TForm;
    DoActivate   : TNotifyEvent;
begin
  inherited Deactivate;
  ActiveForm:=ActiveDialog;
  if ActiveForm<>NIL then begin
    DoActivate:=ActiveForm.OnDeactivate;
    if Assigned(DoActivate) then ActiveForm.OnDeactivate(ActiveForm);
  end;
end;

Function TMultiFormDialog.GetActiveDialog:TForm;
begin
  if FNotebook=NIL then Result:=NIL
  else Result:=GetDialogFromTabIndex(FNotebook.PageIndex);
end;
                               
Function TMultiFormDialog.GetDialogFromTabIndex(AIndex:Integer):TForm;
var Cnt          : Integer;
begin
  Cnt:=0;
  while (Cnt<FDialogInfos.Count) and (DialogInfos[Cnt].PageIndex<=AIndex) do
      Inc(Cnt);
  Dec(Cnt);
  if Cnt<0 then Result:=NIL
  else Result:=DialogInfos[Cnt].Dialog;
end;

Procedure TMultiFormDialog.DoNotebookChange(Sender:TObject;NewTab:Integer;
    var AllowChange:Boolean);
var ActiveForm   : TForm;
    NewActiveForm: TForm;
    DoEvent      : TNotifyEvent;
begin
  if Assigned(FOrgOnChange) then FOrgOnChange(Sender,NewTab,AllowChange)
  else AllowChange:=TRUE;        
  if AllowChange then begin
    ActiveForm:=ActiveDialog;        
    NewActiveForm:=GetDialogFromTabIndex(NewTab);
    if NewActiveForm<>ActiveForm then begin
        if ActiveForm<>NIL then begin
          DoEvent:=ActiveForm.OnDeactivate;
          if Assigned(DoEvent) then ActiveForm.OnDeactivate(ActiveForm);
          { call hide of form im ShowHide is set }
          if FCallShowHide then begin
            DoEvent:=ActiveForm.OnHide;
            if Assigned(DoEvent) then ActiveForm.OnHide(ActiveForm);
          end;
        end;
        if NewActiveForm<>NIL then begin
          { call show of form im ShowHide is set }
          if FCallShowHide then begin
            DoEvent:=NewActiveForm.OnShow;
            if Assigned(DoEvent) then NewActiveForm.OnShow(NewActiveForm);
          end;
          DoEvent:=NewActiveForm.OnActivate;
          if Assigned(DoEvent) then NewActiveForm.OnActivate(NewActiveForm);
        end;
    end;
  end;
end;

Function TMultiFormDialog.CloseQuery:Boolean;
var Cnt            : Integer;
    Dialog         : TCustomForm;
begin
  Result:=FALSE;
  for Cnt:=0 to FDialogInfos.Count-1 do begin
    Dialog:=DialogInfos[Cnt].Dialog;
    Dialog.ModalResult:=ModalResult;
    if not Dialog.CloseQuery then Exit;
  end;
  Result:=inherited CloseQuery;
end;

procedure TMultiFormDialog.DeleteDialog(ADialog: TForm);
var Index          : Integer;
    PageCount      : Integer;
    PageIndex      : Integer;
  Procedure SetControlsParent(ParentControl:TWinControl;NewParent:TWinControl);
  var TabList     : TList;
      Cnt         : Integer;
  begin
    TabList:=TList.Create;
    with ParentControl do begin
      GetTabOrderList(TabList);
      while ControlCount>0 do Controls[0].Parent:=NewParent;
      for Cnt:=0 to TabList.Count-1 do TWinControl(TabList[Cnt]).TabOrder:=Cnt;
    end;
    TabList.Free;
  end;
begin
  Index:=0;
  while (Index<FDialogInfos.Count) and (DialogInfos[Index].Dialog<>ADialog) do
    Inc(Index);
  if Index<FDialogInfos.Count then begin
    FNotebook.DisableAlign;
    try
      PageIndex:=FNoteBook.PageIndex;
      // calculate number of pages to remove
      if DialogInfos[Index].ControlParent is TTabbedNotebook then begin
        if Index<FDialogInfos.Count-1 then PageCount:=DialogInfos[Index+1].PageIndex
        else PageCount:=FNotebook.Pages.Count;
        PageCount:=PageCount-DialogInfos[Index].PageIndex;
      end
      else PageCount:=1;               
      // set a new active page
      if DialogInfos[Index].PageIndex>=PageIndex then PageIndex:=PageIndex-PageCount;
      FNotebook.PageIndex:=PageIndex;
      if DialogInfos[Index].ControlParent is TTabbedNotebook then begin
      end
      else begin
        SetControlsParent(TTabPage(FNotebook.Pages.Objects[DialogInfos[Index].PageIndex]),
            DialogInfos[Index].ControlParent);
        FNotebook.Pages.Delete(DialogInfos[Index].PageIndex);
      end;
      FDialogInfos.Delete(Index);
      for Index:=Index to FDialogInfos.Count-1 do DialogInfos[Index].PageIndex:=
          DialogInfos[Index].PageIndex-PageCount;
    finally
      FNotebook.EnableAlign;
    end;
  end;
end;

function TMultiFormDialog.GetDialogInfo(AIndex: Integer): PDialogInfo;
begin
  Result:=PDialogInfo(FDialogInfos[AIndex]);
end;

end.
