Unit ListDlg;

Interface

Uses WinProcs,WinTypes,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     StdCtrls,WCtrls;

Type TSelectionForm     = Class(TWForm)
       CancelBtn   : TButton;
       ListBox     : TListBox;
       OkBtn       : TButton;
       Procedure   ListBoxDblClick(Sender: TObject);
     end;

     TSelectionDialog   = Class(TComponent)
      Private
       FCaption         : String;
       FDialog          : TSelectionForm;
       FMultiSelect     : Boolean;
       FSorted          : Boolean;
       Function    GetDialog:TSelectionForm;
       Function    GetItemIndex: Integer;
       Function    GetItems:TStrings;
       Function    GetSelected(AIndex:Integer):Boolean;
       Procedure   SetItems(AItems:TStrings);
      Protected
       Property    Dialog:TSelectionForm read GetDialog;
      Public
       Property    Items:TStrings read GetItems write SetItems;
       Property    ItemIndex:Integer read GetItemIndex;
       Property    Selected[AIndex:Integer]:Boolean read GetSelected;
       Function    SelCount:Integer;
       Function    Execute:Boolean;
      Published
       Property    Caption:String read FCaption write FCaption;
       Property    MultiSelect:Boolean read FMultiSelect write FMultiSelect;
       Property    Sorted:Boolean read FSorted write FSorted;
     end;

Implementation

{$R *.DFM}

Function TSelectionDialog.GetSelected
   (
   AIndex          : Integer
   )
   : Boolean;
  begin
    Result:=Dialog.Listbox.Selected[AIndex];
  end;

Procedure TSelectionDialog.SetItems
   (
   AItems          : TStrings
   );
  begin
    if not(csDesigning in ComponentState) then Dialog.Listbox.Items:=AItems;
  end;

Function TSelectionDialog.GetDialog
   : TSelectionForm;
  begin
    if FDialog=NIL then begin
      FDialog:=TSelectionForm.Create(Application);
      FDialog.Listbox.MultiSelect:=FMultiselect;
      FDialog.Caption:=FCaption;
      FDialog.Listbox.Sorted:=FSorted;
    end;  
    Result:=FDialog;
  end;

Function TSelectionDialog.GetItems
   : TStrings;
  begin
    if csDesigning in ComponentState then Result:=NIL
    else Result:=Dialog.Listbox.Items;
  end;

Function TSelectionDialog.Execute
   : Boolean;
  begin
    Result:=Dialog.ShowModal=mrOK;
  end;

Procedure TSelectionForm.ListBoxDblClick(Sender: TObject);
  begin
    ModalResult:=mrOK;
  end;

Function TSelectionDialog.SelCount
   : Integer;
  var Cnt          : Integer; 
  begin
    Result:=0;
    for Cnt:=0 to Items.Count-1 do if Selected[Cnt] then Inc(Result);
  end;   

Function TSelectionDialog.GetItemIndex: Integer;
begin
  Result:=Dialog.Listbox.ItemIndex;
end;

end.
