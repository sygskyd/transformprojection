{*******************************************************************************
| Unit DipGrid
|-------------------------------------------------------------------------------
| Author: Martin Forst, 1998-04-08
|-------------------------------------------------------------------------------
| Modifications
*******************************************************************************}
Unit DipGrid;

Interface                                                              

Uses WinProcs,WinTypes,Classes,Controls,Forms,Graphics,Menus,Messages;

Type TDrawDipGridEvent  = Procedure (Sender:TObject;Canvas:TCanvas;Col,Row:Integer;Rect:TRect) of object;
     TDrawDipListEvent  = Procedure (Sender:TObject;Canvas:TCanvas;Index:Integer;Rect:TRect) of object;

     TScrollbars        = (sbNone,sbAutomatic,sbVertical,sbHorizontal,sbsBoth);

     TCustomDipGrid     = Class(TCustomControl)
      Private
       FAutoScrollDirX  : Integer;
       FAutoScrollDirY  : Integer;
       FBorderStyle     : TBorderStyle;
       FColumns         : Integer;
       FColumnWidth     : Integer;
       FDipDepth        : Integer;
       FDrawUnusedCells : Boolean;
       FFlat            : Boolean;
       FFrameWidth      : Integer;
       FGridBitmap      : TBitmap;
       FGridBitmapValid : Boolean;
       FHeaderRows      : Integer;
       FIntegralHeight  : Boolean;
       FIntegralWidth   : Boolean;
       FLeftColumn      : Integer;
       FLastMouse       : TPoint;
       FLastRowColumns  : Integer;
       FOnDrawCell      : TDrawDipGridEvent;
       FRows            : Integer;
       FRowHeight       : Integer;
       FScrollBars      : TScrollbars;
       FSelColumn       : Integer;
       FSelRow          : Integer;
       FTopRow          : Integer;
       FUpdateScroll    : Boolean;
       Function    CellRect(ACol,ARow:Integer):TRect;
       Procedure   CMEnabledChanged(var Msg:TMessage); message cm_EnabledChanged;
       Procedure   CMMouseEnter(var Message: TMessage); message CM_MOUSEENTER;
       Procedure   CMMouseLeave(var Message: TMessage); message CM_MOUSELEAVE;
       Procedure   CMWantSpecialKey(var Msg:TCMWantSpecialKey); message cm_WantSpecialKey;
       Procedure   DoPaint;
       Procedure   DrawDip(Pos:TPoint;Selected,MouseOver,MouseDown,Transparent:Boolean);
       Procedure   DrawSelection(State:Boolean);
       Function    FullyVisibleCols:Integer;
       Function    FullyVisibleRange:TRect;
       Function    FullyVisibleRows:Integer;
       Function    GetCellWidth:Integer;
       Procedure   SetAutoScrollTimer(Shift:TShiftState);
       Procedure   SetColWidth(AWidth:Integer);
       Procedure   SetCellWidth(AWidth:Integer);
       Procedure   SetDipDepth(ADepth:Integer);
       Procedure   SetDrawUnusedCells(ADraw:Boolean);
       Procedure   SetFlat(AFlat:Boolean);
       Procedure   SetFrameWidth(AWidth:Integer);
       Procedure   SetHeaderRows(ARows:Integer);
       Procedure   SetIntegralHeight(AIntegral:Boolean);
       Procedure   SetIntegralWidth(AIntegral:Boolean);
       Procedure   SetLeftColumn(ACol:Integer);
       Procedure   SetMouseDip(Pos:TPoint;Pressed:Boolean);
       Procedure   SetRows(ARows:Integer);
       Procedure   SetRowHeight(AHeight:Integer);
       Procedure   SetScrollStyle(AStyle:TScrollbars);
       Procedure   SetSelColumn(ACol:Integer);
       Procedure   SetSelection(ACol,ARow:Integer);
       Procedure   SetSelRow(ARow:Integer);
       Procedure   SetTopRow(ARow:Integer);
       Procedure   UpdateScrollRange;
       Function    VisibleCols:Integer;
       Function    VisibleRange:TRect;
       Function    VisibleRows:Integer;
       Procedure   WMEraseBkgnd(var Msg:TWMEraseBkgnd); message wm_EraseBkgnd;
       Procedure   WMGetDlgCode(var Msg:TWMGetDlgCode); message wm_GetDlgCode;
       Procedure   WMHScroll(var Message:TWMHScroll); message wm_HScroll;
       Procedure   WMSize(var Message:TWMSize); message wm_Size;
       Procedure   WMTimer(var Msg:TWMTimer); message wm_Timer;
       Procedure   WMVScroll(var Message:TWMVScroll); message wm_VScroll;
      Protected
       Procedure   CreateParams(var Params:TCreateParams); override;
       Procedure   KeyDown(var Key:Word;Shift:TShiftState); override;
       Procedure   Loaded; override;
       Procedure   MouseDown(Button:TMouseButton;Shift:TShiftState;X,Y:Integer); override;
       Procedure   MouseMove(Shift:TShiftState;X,Y:Integer); override;
       Procedure   MouseUp(Button:TMouseButton;Shift:TShiftState;X,Y:Integer); override;
       Procedure   SetColumns(AColumns:Integer); virtual;
      Protected
       Procedure   ChangeScale(M,D:Integer); override;
       Property    ColCount:Integer read FColumns write SetColumns default 3;
       Property    ColWidth:Integer read FColumnWidth write SetColWidth;
       Property    Color default clWindow;
       Property    Ctl3D default False;
       Property    DipDepth:Integer read FDipDepth write SetDipDepth default 1;
       Procedure   DrawCell(Canvas:TCanvas;ACol,ARow:Longint;ARect:TRect); virtual;
       Property    DrawUnusedCells:Boolean read FDrawUnusedCells write SetDrawUnusedCells default TRUE;
       Property    Flat:Boolean read FFlat write SetFlat default FALSE;
       Property    FrameWidth:Integer read FFrameWidth write SetFrameWidth default 1;
       Property    HeaderRows:Integer read FHeaderRows write SetHeaderRows default 0;
       Property    IntegralHeight:Boolean read FIntegralHeight write SetIntegralHeight default True;
       Property    IntegralWidth:Boolean read FIntegralWidth write SetIntegralWidth default True;
       Property    LastRowColumns:Integer read FLastRowColumns write FLastRowColumns default -1;
       Property    OnDrawCell:TDrawDipGridEvent read FOnDrawCell write FOnDrawCell;
       Property    ParentColor default False;
       Property    ParentCtl3D default False;
       Procedure   Paint; override;
       Property    RowCount:Integer read FRows write SetRows default 5;
       Property    RowHeight:Integer read FRowHeight write SetRowHeight;
       Property    Scrollbars:TScrollbars read FScrollbars write SetScrollStyle default sbAutomatic;
      Public
       Constructor Create(AOwner:TComponent); override;
       Destructor  Destroy; override;
       Property    Canvas;
       Function    CellHeight:Integer;
       Property    CellWidth:Integer read GetCellWidth write SetCellWidth;
       Function    DipAtPos(Pos:TPoint;AExists:Boolean):TPoint;
       Procedure   InvalidateCell(ACol,ARow:Integer);
       Property    LeftColumn:Integer read FLeftColumn write SetLeftColumn;
       Function    NeededSize(ACols,ARows:Integer):TRect; virtual;
       Property    SelectedColumn:Integer read FSelColumn write SetSelColumn;
       Property    SelectedRow:Integer read FSelRow write SetSelRow;
       Procedure   SetBounds(ALeft,ATop,ARight,ABottom:Integer); override;
       Property    TopRow:Integer read FTopRow write SetTopRow;
     end;

     TDipGrid      = Class(TCustomDipGrid)
      Published
       Property    Align;
       Property    Anchors;
       Property    ColCount;
       Property    ColWidth;
       Property    Color;
       Property    DipDepth;
       Property    DragCursor;
       Property    DragMode;
       Property    DrawUnusedCells;
       Property    Font;
       Property    Flat;
       Property    FrameWidth;
       Property    HeaderRows;
       Property    Hint;
       Property    IntegralHeight;
       Property    IntegralWidth;
       Property    LastRowColumns;
       Property    OnClick;
       Property    OnDblClick;
       Property    OnDragDrop;
       Property    OnDragOver;
       Property    OnDrawCell;
       Property    OnEndDrag;
       Property    OnEnter;
       Property    OnExit;
       Property    OnKeyDown;
       Property    OnKeyPress;
       Property    OnKeyUp;
       Property    OnMouseDown;
       Property    OnMouseMove;
       Property    OnMouseUp;
       {$IFDEF WIN32}
       Property    OnStartDrag;
       {$ENDIF}
       Property    ParentColor;
       Property    ParentFont;
       Property    ParentShowHint;
       Property    PopupMenu;
       Property    RowCount;
       Property    RowHeight;
       Property    ShowHint;
       Property    TabOrder;
       Property    TabStop;
       Property    ScrollBars;
     end;

     TDipList      = Class(TCustomDipGrid)
      Private
       FOnDrawCell : TDrawDipListEvent;
       FCount      : Integer;
       Function    GetItemIndex:Integer;
       Procedure   SetCount(ACount:Integer);
       Procedure   SetItemIndex(AIndex:Integer);
      Protected
       Procedure   DrawCell(Canvas:TCanvas;ACol,ARow:LongInt;ARect:TRect); override;
       Procedure   SetColumns(AColumns:Integer); override;
      Public
       Property    Count:Integer read FCount write SetCount;
       Procedure   InvalidateDip(AIndex:Integer);
       Procedure   InvalidateItems(FromItem,ToItem:Integer);
       Property    ItemIndex:Integer read GetItemIndex write SetItemIndex;
      Published
       Property    Align;
       Property    Anchors;
       Property    ColCount;
       Property    Color;
       Property    ColWidth;
       Property    DipDepth;
       Property    DragCursor;
       Property    DragMode;
       Property    DrawUnusedCells;
       Property    Flat;
       Property    Font;
       Property    FrameWidth;
       Property    HeaderRows;
       Property    Hint;
       Function    ItemAtPos(Const Pos:TPoint;AExists:Boolean):Integer;
       Property    IntegralHeight;
       Property    IntegralWidth;
       Property    OnClick;
       Property    OnDblClick;
       Property    OnDragDrop;
       Property    OnDragOver;
       Property    OnEndDrag;
       Property    OnEnter;
       Property    OnExit;
       {$IFDEF WIN32}
       Property    OnStartDrag;
       {$ENDIF}
       Property    OnDrawCell:TDrawDipListEvent read FOnDrawCell write FOnDrawCell;
       Property    OnKeyDown;
       Property    OnKeyPress;
       Property    OnKeyUp;
       Property    OnMouseDown;
       Property    OnMouseMove;
       Property    OnMouseUp;
       Property    ParentColor;
       Property    ParentFont;
       Property    ParentShowHint;
       Property    PopupMenu;
       Property    RowHeight;
       Property    Scrollbars;
       Property    ShowHint;
       Property    TabOrder;
       Property    TabStop;
     end;

Implementation

Uses ExtCtrls,NumTools;

Constructor TCustomDipGrid.Create
   (
   AOwner          : TComponent
   );
  begin
    inherited Create(AOwner);
    ControlStyle:=[csCaptureMouse,csFramed,csOpaque,csDoubleClicks];
    ParentColor:=FALSE;
    Color:=clWindow;
    FFrameWidth:=1;
    FColumns:=3;
    FRows:=5;
    FColumnWidth:=20;                  
    FRowHeight:=20;
    FScrollbars:=sbAutomatic;
    FDipDepth:=1;
    ParentCtl3D:=False;
    Ctl3D:=False;
    FIntegralHeight:=True;
    FIntegralWidth:=True;
    FLastRowColumns:=-1;
    FSelColumn:=-1;
    FSelRow:=-1;
    FGridBitmap:=TBitmap.Create;
    FDrawUnusedCells:=TRUE;
  end;

Destructor TCustomDipGrid.Destroy;
  begin
    FGridBitmap.Free;
    inherited Destroy;
  end;

Procedure TCustomDipGrid.SetDrawUnusedCells
   (
   ADraw           : Boolean
   );
  begin
    if ADraw<>FDrawUnusedCells then begin
      FDrawUnusedCells:=ADraw;
      Invalidate;
    end;  
  end;

Procedure TCustomDipGrid.SetColumns
   (
   AColumns        : Integer
   );
  begin
    if AColumns<>FColumns then begin
      FColumns:=AColumns;
      if FLastRowColumns>AColumns then FLastRowColumns:=AColumns;
      UpdateScrollRange;
      Invalidate;
    end;
  end;

Procedure TCustomDipGrid.SetColWidth(AWidth:Integer);
begin
  if AWidth<>FColumnWidth then begin
    FColumnWidth:=AWidth;
    FGridBitmapValid:=FALSE;
    UpdateScrollRange;
    Invalidate;
  end;
end;

Procedure TCustomDipGrid.SetCellWidth(AWidth:Integer);
begin
  SetColWidth(AWidth-FFrameWidth-FDipDepth*2);
end;

Procedure TCustomDipGrid.SetRows
   (
   ARows           : Integer
   );
  begin
    if ARows<>FRows then begin
      FRows:=ARows;
      UpdateScrollRange;
      Invalidate;
    end;
  end;

Procedure TCustomDipGrid.SetRowHeight
   (
   AHeight         : Integer
   );
  begin
    if AHeight<>FRowHeight then begin
      FRowHeight:=AHeight;
      FGridBitmapValid:=FALSE;
      UpdateScrollRange;
      Invalidate;
    end;
  end;

Procedure TCustomDipGrid.Paint;
begin
  DoPaint;
end;

Procedure TCustomDipGrid.SetScrollStyle
   (
   AStyle          : TScrollbars
   );
  begin
    if AStyle<>FScrollBars then begin
      FScrollBars:=AStyle;
      UpdateScrollRange;
      RecreateWnd;
    end;
  end;

Procedure TCustomDipGrid.DoPaint;
  var Col          : Integer;
      Row          : Integer;
      ARect        : TRect;
      BRect        : TRect;
      CRect        : TRect;
      DRect        : TRect;
      ACellWidth   : Integer;
      ACellHeight  : Integer;
      Range        : TRect;
      Cols         : Integer;
      ABitmap      : TBitmap;
      FrameOffset  : Integer;
  begin
    ACellWidth:=CellWidth;
    ACellHeight:=CellHeight;       
    Cols:=VisibleCols;
    FrameOffset:=FFrameWidth+FDipDepth;
    if not FGridBitmapValid then with FGridBitmap,Canvas do begin
      Width:=Self.Width;
      Height:=3*ACellHeight;
      Brush.Style:=bsSolid;
      Brush.Color:=clBtnFace;
      FillRect(ClientRect);
      ARect:=Rect(0,0,Cols*ACellWidth-2*FDipDepth-FFrameWidth,FRowHeight);
      OffsetRect(ARect,FrameOffset,FrameOffset);
      InflateRect(ARect,FDipDepth,FDipDepth);
      if FDipDepth=1 then begin
        Frame3D(Canvas,ARect,clBtnShadow,clBtnHighlight,1);
        Frame3D(Canvas,ARect,cl3DLight,cl3DLight,1);
      end
      else if FDipDepth>1 then begin
        Frame3D(Canvas,ARect,clBtnShadow,clBtnHighlight,FDipDepth-1);
        Frame3D(Canvas,ARect,cl3DDKShadow,cl3DLight,1);
      end;
      ARect:=Rect(0,0,FColumnWidth,FRowHeight);
      OffsetRect(ARect,FrameOffset,FrameOffset+ACellHeight);
      InflateRect(ARect,FDipDepth,FDipDepth);
      for Col:=0 to Cols-1 do begin
        BRect:=ARect;
        if FDipDepth=1 then begin
          Frame3D(Canvas,BRect,clBtnShadow,clBtnHighlight,1);
          Frame3D(Canvas,BRect,clBtnFace,cl3DLight,1);
        end
        else if FDipDepth>1 then begin
          Frame3D(Canvas,BRect,clBtnShadow,clBtnHighlight,FDipDepth-1);
          Frame3D(Canvas,BRect,cl3DDKShadow,cl3DLight,1);
        end;
        OffsetRect(ARect,ACellWidth,0);
      end;
      FGridBitmapValid:=TRUE;
    end;
    ABitmap:=TBitmap.Create;
    ABitmap.Width:=ClientWidth;
    ABitmap.Height:=ClientHeight;
    with ABitmap,Canvas do begin
      Range:=VisibleRange;
      { Headerrows zeichnen }
      CRect:=Rect(0,0,Width,ACellHeight);
      DRect:=CRect;
      ARect:=Rect(0,0,Cols*ACellWidth-2*FrameOffset,FRowHeight);
      OffsetRect(ARect,FrameOffset,FrameOffset);
      if FDipDepth=1 then Dec(ARect.Bottom);
      for Row:=Range.Top to -1 do begin
        CopyRect(CRect,FGridBitmap.Canvas,DRect);
        OffsetRect(CRect,0,ACellHeight);
        DrawCell(Canvas,0,Row,ARect);
        OffsetRect(ARect,0,ACellHeight);
      end;
      { andere Rows zeichnen }
      OffsetRect(DRect,0,ACellHeight);
      if Range.Top<0 then Row:=0
      else Row:=Range.Top;
      for Row:=Row to Range.Bottom-1 do begin
        CopyRect(CRect,FGridBitmap.Canvas,DRect);
        OffsetRect(CRect,0,ACellHeight);
        ARect:=Rect(0,0,FColumnWidth,FRowHeight);
        if FDipDepth=1 then begin Dec(ARect.Right); Dec(ARect.Bottom); end;
        OffsetRect(ARect,FrameOffset,FrameOffset+(Row-Range.Top)*ACellHeight);
        for Col:=Range.Left to Range.Right do begin
          DrawCell(Canvas,Col,Row,ARect);
          OffsetRect(ARect,ACellWidth,0);
        end;
      end;                                          
      { letzte Zeile zeichnen }
      if Range.Top<=Range.Bottom then begin
        if Range.Bottom<FRows-1 then Cols:=Range.Right
        else if FLastRowColumns=-1 then Cols:=Range.Right
        else Cols:=FLastRowColumns-1;
        CopyRect(CRect,FGridBitmap.Canvas,DRect);
        OffsetRect(CRect,0,ACellHeight);
        ARect:=Rect(0,0,FColumnWidth,FRowHeight);
        if FDipDepth=1 then begin Dec(ARect.Right); Dec(ARect.Bottom); end;
        OffsetRect(ARect,FrameOffset,FrameOffset+(Range.Bottom-Range.Top)*ACellHeight);
        for Col:=Range.Left to Cols do begin
          DrawCell(Canvas,Col,Range.Bottom,ARect);
          OffsetRect(ARect,ACellWidth,0);
        end;
        if not FDrawUnusedCells then for Col:=Cols to VisibleCols do begin
          Brush.Color:=clBtnFace;
          FillRect(ARect);
          OffsetRect(ARect,ACellWidth,0);
        end;
      end;
      { nicht verwendete Zeilen zeichnen }
      if not FDrawUnusedCells then OffsetRect(DRect,0,ACellHeight);
      for Row:=Range.Bottom-Range.Top to VisibleRows-1 do begin
        CopyRect(CRect,FGridBitmap.Canvas,DRect);
        OffsetRect(CRect,0,ACellHeight);
      end;
    end;
    Canvas.Draw(0,0,ABitmap);
    ABitmap.Free;
    DrawSelection(TRUE);
  end;

Function TCustomDipGrid.GetCellWidth
   : Integer;
  begin
    Result:=FColumnWidth+FFrameWidth+FDipDepth*2;
  end;                           

Procedure TCustomDipGrid.KeyDown(var Key:Word;Shift:TShiftState);
begin
  case Key of
    vk_Up   : SetSelection(FSelColumn,Max(FSelRow-1,-FHeaderRows));
    vk_Down : SetSelection(FSelColumn,Min(FSelRow+1,FRows-1));
    vk_Left : SetSelection(Max(FSelColumn-1,0),FSelRow);
    vk_Right: SetSelection(Min(FSelColumn+1,FColumns-1),FSelRow);
    vk_Prior: SetSelection(FSelColumn,Max(FSelRow-FullyVisibleRows,-FHeaderRows));
    vk_Next : SetSelection(FSelColumn,Min(FSelRow+FullyVisibleRows,FRows-1));
  end;
  inherited KeyDown(Key,Shift);
end;

Function TCustomDipGrid.CellHeight:Integer;
begin
  Result:=FRowHeight+FFrameWidth+FDipDepth*2;
end;

Function TCustomDipGrid.CellRect(ACol,ARow:Integer):TRect;
begin
  if ARow<0 then Result:=Rect(0,0,CellWidth*FColumns-2*FDipDepth-FFrameWidth,FRowHeight)
  else Result:=Rect(0,0,FColumnWidth,FRowHeight);
  OffsetRect(Result,FrameWidth+FDipDepth+(ACol-FLeftColumn)*CellWidth,
      FrameWidth+FDipDepth+(ARow-FTopRow)*CellHeight);
end;

Procedure TCustomDipGrid.DrawDip(Pos:TPoint;Selected,MouseOver,MouseDown,
    Transparent:Boolean);
Const FrameStyles  : Array[Boolean] of Integer = (BDR_RAISEDINNER,BDR_SUNKENOUTER);
var Rect           : TRect;
begin
  Rect:=CellRect(Pos.X,Pos.Y);
  with Canvas do if FFlat then begin
    InflateRect(Rect,FrameWidth,FrameWidth);
    if not Selected and not MouseOver then begin
      Brush.Color:=clBtnFace;
      FrameRect(Rect);
    end                           
    else DrawEdge(Canvas.Handle,Rect,FrameStyles[Selected or
        (MouseOver and MouseDown)],BF_RECT);  
{    InflateRect(Rect,-1,-1);
    if Selected and not MouseOver then begin
      Brush.Bitmap:=AllocPatternBitmap(clBtnFace,clBtnHighlight);
      FillRect(Rect);
    end
    else if not Transparent then begin
      Brush.Color:=clBtnFace;
      FillRect(Rect);
    end;}  
  end
  else if FDipDepth>=1 then begin
    InflateRect(Rect,2,2);
    if FDipDepth=1 then begin Dec(Rect.Right); Dec(Rect.Bottom); end;
    if Selected then begin 
      Brush.Color:=clBlack;
      FrameRect(Rect);                  
      InflateRect(Rect,-1,-1);
      FrameRect(Rect);
    end
    else if FDipDepth=1 then begin
      Frame3D(Canvas,Rect,clBtnFace,clBtnHighlight,1);
      Frame3D(Canvas,Rect,clBtnShadow,cl3DLight,1);
    end
    else begin
      Frame3D(Canvas,Rect,clBtnShadow,clBtnHighlight,1);
      Frame3D(Canvas,Rect,clBlack,cl3DLight,1);
    end;
  end;
end;

Procedure TCustomDipGrid.DrawSelection(State:Boolean);
var Rect         : TRect;
begin
  Rect:=VisibleRange;
  Inc(Rect.Right); Inc(Rect.Bottom);
  if ptInRect(Rect,Point(FSelColumn,FSelRow)) then
      DrawDip(Point(FSelColumn,FSelRow),State,FALSE,FALSE,FALSE);
end;

Procedure TCustomDipGrid.Loaded;
begin
  inherited Loaded;
  TopRow:=-FHeaderRows;
  UpdateScrollRange;
end;

Procedure TCustomDipGrid.CreateParams(var Params:TCreateParams);
begin
  inherited CreateParams(Params);
  with Params do begin
    if FBorderStyle=bsSingle then Style:=Style or WS_BORDER;
    if FScrollBars in [sbAutomatic,sbVertical,sbsBoth] then Style:=Style or WS_VSCROLL;
    if FScrollBars in [sbAutomatic,sbHorizontal,sbsBoth] then Style:=Style or WS_HSCROLL;
    WindowClass.Style:=CS_DBLCLKS;
  end;
end;

Procedure TCustomDipGrid.WMTimer(var Msg:TWMTimer);
begin
  SetSelection(Max(0,FSelColumn+FAutoScrollDirX),Max(0,FSelRow+FAutoScrollDirY));
end;

Procedure TCustomDipGrid.UpdateScrollRange;
  Function ScrollbarVisible(ABar:Integer):Boolean;
  var Min          : Integer;
      Max          : Integer;
  begin
    GetScrollRange(Handle,ABar,Min,Max);
    Result:=Min<>Max;
  end;
var Value          : Integer;
begin
  if (csReading in ComponentState) or FUpdateScroll then Exit;
  FUpdateScroll:=TRUE;
  try
    if not (FScrollBars in [sbVertical,sbsBoth,sbAutomatic]) then Value:=-FHeaderRows
    else Value:=Max(-FHeaderRows,FRows-FullyVisibleRows);
    if FScrollbars in [sbVertical,sbsBoth] then begin
      if Value<=-FHeaderRows then EnableScrollBar(Handle,sb_Vert,esb_Disable_Both)
      else EnableScrollBar(Handle,sb_Vert,esb_Enable_Both);
      Value:=Max(-FHeaderRows+1,Value);
    end;                         
    SetScrollRange(Handle,SB_VERT,-FHeaderRows,Value,True);
    if IntegralWidth then ClientWidth:=Max(1,Round(ClientWidth/CellWidth))*CellWidth+1;
    if not (FScrollBars in [sbHorizontal,sbsBoth,sbAutomatic]) then Value:=0
    else Value:=Max(0,FColumns-FullyVisibleCols);
    if FScrollbars in [sbHorizontal,sbsBoth] then begin
      if Value<=0 then EnableScrollBar(Handle,sb_Horz,esb_Disable_Both)
      else EnableScrollBar(Handle,sb_Horz,esb_Enable_Both);
      Value:=Max(1,Value);
    end;
    SetScrollRange(Handle,SB_HORZ,0,Value,True);
    if IntegralHeight then ClientHeight:=Max(1,Round(ClientHeight/CellHeight))*CellHeight;
  finally
    FUpdateScroll:=FALSE;
  end;
end;

Procedure TCustomDipGrid.WMVScroll(var Message:TWMVScroll);
var NewTop         : Integer;
begin
  case Message.ScrollCode of
    SB_BOTTOM        : NewTop:=FRows;
    SB_LINEDOWN      : NewTop:=FTopRow+1;
    SB_LINEUP        : NewTop:=FTopRow-1;
    SB_PAGEDOWN      : NewTop:=FTopRow+FullyVisibleRows;
    SB_PAGEUP        : NewTop:=FTopRow-FullyVisibleRows;
    SB_THUMBPOSITION : NewTop:=Message.Pos;
    SB_THUMBTRACK    : NewTop:=Message.Pos;
    SB_TOP           : NewTop:=-FHeaderRows;
    else NewTop:=FTopRow;
  end;
  if NewTop>FRows-FullyVisibleRows then NewTop:=FRows-FullyVisibleRows;
  if NewTop<-FHeaderRows then NewTop:=-FHeaderRows;
  if NewTop<>FTopRow then begin
    FTopRow:=NewTop;
    SetScrollPos(Handle,SB_VERT,NewTop,True);
    DoPaint;
  end;
end;

Procedure TCustomDipGrid.WMHScroll(var Message:TWMHScroll);
var NewLeft        : Integer;
begin
  case Message.ScrollCode of
    SB_BOTTOM        : NewLeft:=FColumns;
    SB_LINEDOWN      : NewLeft:=FLeftColumn+1;
    SB_LINEUP        : NewLeft:=FLeftColumn-1;
    SB_PAGEDOWN      : NewLeft:=FLeftColumn+FullyVisibleCols;
    SB_PAGEUP        : NewLeft:=FLeftColumn-FullyVisibleCols;
    SB_THUMBPOSITION : NewLeft:=Message.Pos;
    SB_THUMBTRACK    : NewLeft:=Message.Pos;
    SB_TOP           : NewLeft:=0;
    else NewLeft:=FLeftColumn;
  end;
  if NewLeft>FColumns-FullyVisibleCols then NewLeft:=FColumns-FullyVisibleCols;
  if NewLeft<0 then NewLeft:=0;
  if NewLeft<>FLeftColumn then begin
    FLeftColumn:=NewLeft;
    SetScrollPos(Handle,SB_HORZ,NewLeft,True);
    DoPaint;
  end;
end;
 
Function TCustomDipGrid.VisibleRange:TRect;
begin
  Result:=Rect(FLeftColumn,FTopRow,FLeftColumn+VisibleCols,FTopRow+VisibleRows);
  if Result.Right>=FColumns then Result.Right:=FColumns-1;
  if Result.Bottom>=FRows then Result.Bottom:=FRows-1;    
end;                                   

Function TCustomDipGrid.FullyVisibleRange:TRect;
begin
  Result:=Rect(FLeftColumn,FTopRow,FLeftColumn+FullyVisibleCols,FTopRow+FullyVisibleRows);
  if Result.Right>=FColumns then Result.Right:=FColumns-1;
  if Result.Bottom>=FRows then Result.Bottom:=FRows-1;
end;

Function TCustomDipGrid.FullyVisibleCols:Integer;
begin
  Result:=Min(FColumns,ClientWidth Div CellWidth);
end;

Function TCustomDipGrid.FullyVisibleRows:Integer;
begin
  Result:=ClientHeight Div CellHeight;
end;

Function TCustomDipGrid.VisibleCols:Integer;
begin
  Result:=Min(FColumns,Round(ClientWidth/CellWidth));
end;

Function TCustomDipGrid.VisibleRows:Integer;
begin
  Result:=Round(ClientHeight/CellHeight);
end;

Procedure TCustomDipGrid.DrawCell(Canvas:TCanvas;ACol,ARow:LongInt;ARect:TRect);
begin
  Canvas.Font:=Font;
  Canvas.Brush.Color:=Color;
  Canvas.Brush.Style:=bsSolid;
  if Assigned(FOnDrawCell) then begin
    with ARect do IntersectClipRect(Canvas.Handle,Left,Top,Right,Bottom);
    OnDrawCell(Self,Canvas,ACol,ARow,ARect);
    Canvas.Pen.Style:=psSolid;
    Canvas.Brush.Style:=bsSolid;
    SelectClipRgn(Canvas.Handle,0);
  end
  else Canvas.FillRect(ARect);
end;

Procedure TCustomDipGrid.SetFrameWidth(AWidth:Integer);
begin
  if AWidth<>FFrameWidth then begin
    FFrameWidth:=AWidth;
    FGridBitmapValid:=FALSE;
    if FIntegralWidth or FIntegralHeight then UpdateScrollRange;
    Invalidate;
  end;
end;

Procedure TCustomDipGrid.SetDipDepth(ADepth:Integer);
begin
  if FFlat then ADepth:=0;
  if ADepth<>FDipDepth then begin
    FDipDepth:=ADepth;
    FGridBitmapValid:=FALSE;
    if FIntegralWidth or FIntegralHeight then UpdateScrollRange;
    Invalidate;
  end;
end;

Procedure TCustomDipGrid.WMSize(var Message:TWMSize);
begin
  inherited;
  UpdateScrollRange;
end;
                                    
Procedure TCustomDipGrid.SetTopRow(ARow:Integer);
begin
  if ARow<>FTopRow then Perform(wm_VScroll,MakeLong(sb_ThumbPosition,Word(ARow)),0);
end;

Procedure TCustomDipGrid.SetLeftColumn(ACol:Integer);
begin
  if ACol<>FLeftColumn then Perform(wm_HScroll,MakeLong(sb_ThumbPosition,ACol),0);
end;

Procedure TCustomDipGrid.SetSelRow(ARow:Integer);
begin
  SetSelection(FSelColumn,ARow);
end;

Procedure TCustomDipGrid.SetSelColumn(ACol:Integer);
begin
  SetSelection(ACol,FSelRow);
end;

Procedure TCustomDipGrid.SetSelection(ACol,ARow:Integer);
var Rect         : TRect;
begin
  ACol:=Max(-1,Min(ACol,FColumns-1));
  ARow:=Max(-FHeaderRows-1,Min(ARow,FRows-1));
  if (ARow=FRows-1) and (FLastRowColumns<>-1) then
    ACol:=Min(ACol,FLastRowColumns-1);
  if ARow<0 then ACol:=0;
  if (ACol<>FSelColumn) or (ARow<>FSelRow) then begin
    DrawSelection(False);
    FSelColumn:=ACol;
    FSelRow:=ARow;
    Rect:=FullyVisibleRange;
    if FSelColumn<Rect.Left then LeftColumn:=FSelColumn;
    if FSelColumn>=Rect.Right then LeftColumn:=FSelColumn-FullyVisibleCols+1;
    if FSelRow<Rect.Top then TopRow:=FSelRow;
    if FSelRow>=Rect.Bottom then TopRow:=FSelRow-FullyVisibleRows+1;
    if FSelRow*FSelColumn>=0 then DrawSelection(True);
    Click;
  end;
end;

Procedure TCustomDipGrid.MouseDown(Button:TMouseButton;Shift:TShiftState;X,Y:Integer);
var Pos          : TPoint;
begin
  if ssDouble in Shift then DblClick; // Sygsky's answer : why not to run SymbolEditor in DblClick?
  inherited MouseDown(Button,Shift,X,Y);
{++ Deleted by Sygsky at 19-AUG-1999 Bug #311 Build #86
  if Button=mbLeft then begin
--}
    Pos:=DipAtPos(Point(X,Y),TRUE);
    if Pos.X>-1 then SetSelection(Pos.X,Pos.Y);
    if Parent.Visible and TabStop and not Focused then SetFocus;
{++ Deleted by Sygsky at 19-AUG-1999 Bug #311 Build #86
  end;
--}
  SetAutoScrollTimer(Shift);
end;

Procedure TCustomDipGrid.MouseUp
   (
   Button          : TMouseButton;
   Shift           : TShiftState;
   X               : Integer;
   Y               : Integer
   );
  var Pos          : TPoint;
  begin
    inherited MouseUp(Button,Shift,X,Y);
{++ Deleted by Sygsky at 19-AUG-1999 Bug #311 Build #86
    if Button=mbLeft then begin
--}
      Pos:=DipAtPos(Point(X,Y),TRUE);
      if Pos.X>-1 then SetSelection(Pos.X,Pos.Y);
      if Parent.Visible
          and TabStop
          and not Focused then
        SetFocus;
{++ Deleted by Sygsky at 19-AUG-1999  Bug #311 Build #86
    end;
--}
    SetAutoScrollTimer(Shift);
  end;

Procedure TCustomDipGrid.SetMouseDip(Pos:TPoint;Pressed:Boolean);
begin
  if not PointsEqual(Pos,FLastMouse) then begin
    if FLastMouse.X>=0 then DrawDip(FLastMouse,PointsEqual(FLastMouse,
        Point(FSelColumn,FSelRow)),FALSE,Pressed,TRUE);
    FLastMouse:=Pos;
    if FLastMouse.X>=0 then DrawDip(FLastMouse,PointsEqual(FLastMouse,
        Point(FSelColumn,FSelRow)),TRUE,Pressed,TRUE);
  end;   
end;

Procedure TCustomDipGrid.MouseMove
   (
   Shift           : TShiftState;
   X               : Integer;
   Y               : Integer
   );
  var Pos          : TPoint;
  begin
    inherited MouseMove(Shift,X,Y);
    Pos:=DipAtPos(Point(X,Y),TRUE);
    if FFlat then SetMouseDip(Pos,ssLeft in Shift);
    if (ssLeft in Shift) and (Pos.X>-1) then SetSelection(Pos.X,Pos.Y);
    SetAutoScrollTimer(Shift);
  end;

Procedure TCustomDipGrid.SetAutoScrollTimer
   (
   Shift           : TShiftState
   );
  var Mouse        : TPoint;                        
  begin
    if not (ssLeft in Shift) then KillTimer(Handle,1)
    else begin
      GetCursorPos(Mouse);
      Mouse:=ScreenToClient(Mouse);
      if Mouse.Y<0 then FAutoScrollDirY:=-(Abs(Mouse.Y-5) Div 5)
      else if Mouse.Y>=Height then FAutoScrollDirY:=(Mouse.Y-Height+5) Div 5
      else FAutoScrollDirY:=0;
      if Mouse.X<0 then FAutoScrollDirX:=-(Abs(Mouse.X-5) Div 5)
      else if Mouse.X>=Width then FAutoScrollDirX:=(Mouse.X-Width+5) Div 5
      else FAutoScrollDirX:=0;
      if (FAutoScrollDirY<>0)
          or (FAutoScrollDirX<>0) then
        SetTimer(Handle,1,100,NIL)
      else KillTimer(Handle,1);
    end;  
  end;

Procedure TCustomDipGrid.SetHeaderRows
   (
   ARows           : Integer
   );
  begin
    if ARows<>FHeaderRows then begin
      FHeaderRows:=ARows;
      UpdateScrollRange;
      TopRow:=-ARows;
    end;  
  end;

Function TCustomDipGrid.DipAtPos(Pos:TPoint;AExists:Boolean):TPoint;
var ARect        : TRect;
begin
  Result.Y:=Pos.Y Div CellHeight+FTopRow;
  if Result.Y<0 then Result.X:=0
  else Result.X:=Pos.X Div CellWidth+FLeftColumn;
  if AExists then begin
    ARect:=Rect(0,-FHeaderRows,FColumns,FRows);
    if not ptInRect(ARect,Result)
        or ((Result.Y=FRows-1)
          and (FLastRowColumns<>-1)
          and (Result.X>=FLastRowColumns)) then
    Result:=Point(-1,-FHeaderRows-1);
  end;
end;

Procedure TCustomDipGrid.SetIntegralHeight(AIntegral:Boolean);
begin
  if AIntegral<>FIntegralHeight then begin
    FIntegralHeight:=AIntegral;
    if FIntegralHeight then UpdateScrollRange;
  end;
end;

Procedure TCustomDipGrid.SetIntegralWidth(AIntegral:Boolean);
begin
  if AIntegral<>FIntegralWidth then begin
    FIntegralWidth:=AIntegral;
    if FIntegralWidth then UpdateScrollRange;
  end;
end;

Procedure TCustomDipGrid.CMWantSpecialKey(var Msg:TCMWantSpecialKey);
begin
  inherited;
  if Msg.CharCode in [vk_Up,vk_Down,vk_Left,vk_Right,vk_Prior,vk_Next] then
      Msg.Result:=1;
end;

Procedure TCustomDipGrid.WMEraseBkgnd(var Msg:TWMEraseBkgnd);
begin
  Msg.Result:=1;
end;

Procedure TCustomDipGrid.WMGetDlgCode(var Msg:TWMGetDlgCode);
begin
  inherited;
  Msg.Result:=Msg.Result or DLGC_WANTARROWS;
end;

Procedure TCustomDipGrid.ChangeScale(M,D:Integer);
begin
  inherited ChangeScale(M,D);
  ColWidth:=MulDiv(FColumnWidth,M,D);
  RowHeight:=MulDiv(FRowHeight,M,D);
end;

Procedure TCustomDipGrid.InvalidateCell(ACol,ARow:Integer);
var ARect        : TRect;
begin
  ARect:=CellRect(ACol,ARow);
  InvalidateRect(Handle,@ARect,FALSE);
end;

Procedure TCustomDipGrid.SetBounds(ALeft,ATop,ARight,ABottom:Integer);
begin
  FGridBitmapValid:=FALSE;
  inherited SetBounds(ALeft,ATop,ARight,ABottom);
end;

Procedure TCustomDipGrid.CMEnabledChanged(var Msg:TMessage);
begin
  inherited;
  if Enabled then EnableScrollBar(Handle,sb_Both,esb_Enable_Both)
  else EnableScrollBar(Handle,sb_Both,esb_Disable_Both);
end;

Procedure TCustomDipGrid.SetFlat(AFlat:Boolean);
begin
  if FFlat<>AFlat then begin
    FFlat:=AFlat;
    FDipDepth:=0;
    FGridBitmapValid:=FALSE;
    if FIntegralWidth or FIntegralHeight then UpdateScrollRange;
    Invalidate;
  end;
end;
     
Procedure TCustomDipGrid.CMMouseEnter(var Message: TMessage);
begin
end;

Procedure TCustomDipGrid.CMMouseLeave(var Message: TMessage);
begin
  SetMouseDip(Point(-1,0),FALSE);
end;

Function TCustomDipGrid.NeededSize(ACols,ARows:Integer):TRect;
begin
end;

{===============================================================================
| TDipList
+==============================================================================}

Procedure TDipList.DrawCell
   (
   Canvas          : TCanvas;
   ACol            : LongInt;
   ARow            : LongInt;
   ARect           : TRect
   );                                                  
  begin                              
    Canvas.Font:=Font;
    Canvas.Brush.Color:=Color;
    Canvas.Brush.Style:=bsSolid;      
    if Assigned(FOnDrawCell) then begin
      with ARect do IntersectClipRect(Canvas.Handle,Left,Top,Right,Bottom);
      if ARow<0 then OnDrawCell(Self,Canvas,ARow,ARect)
      else OnDrawCell(Self,Canvas,ACol+ARow*ColCount,ARect);
      Canvas.Pen.Style:=psSolid;
      Canvas.Brush.Style:=bsSolid;
      SelectClipRgn(Canvas.Handle,0);
    end
    else Canvas.FillRect(ARect);
  end;

Procedure TDipList.SetCount
   (
   ACount          : Integer
   );
  begin
    if not (csDesigning in ComponentState) then begin
      DrawSelection(FALSE);
      FCount:=ACount;
      RowCount:=FCount Div ColCount;
      if FCount Mod ColCount=0 then FLastRowColumns:=-1
      else begin
        RowCount:=RowCount+1;
        FLastRowColumns:=FCount Mod ColCount;
      end;
      SetSelection(FSelColumn,FSelRow);
    end;
  end;

Function TDipList.ItemAtPos
   (
   Const Pos       : TPoint;
   AExists         : Boolean
   )
   : Integer;
  var APos         : TPoint;
  begin
    APos:=DipAtPos(Pos,AExists);
    if APos.X<0 then Result:=-1
    else if APos.Y<0 then Result:=APos.Y
    else Result:=APos.Y*FColumns+APos.X
  end;

Function TDipList.GetItemIndex:Integer;
begin
  if SelectedColumn<0 then Result:=-1
  else if SelectedRow<0 then Result:=SelectedRow
  else Result:=SelectedRow*FColumns+SelectedColumn
end;

Procedure TDipList.SetItemIndex
   (
   AIndex          : Integer
   );
  begin
    if csReading in ComponentState then Exit;
    if AIndex<0 then SetSelection(0,AIndex)
    else SetSelection(AIndex Mod FColumns,AIndex Div FColumns);
  end;

Procedure TDipList.InvalidateDip
   (
   AIndex          : Integer
   );
  begin
    InvalidateCell(AIndex Mod FColumns, AIndex Div FColumns);
  end;

Procedure TDipList.InvalidateItems
   (
   FromItem        : Integer;
   ToItem          : Integer
   );
  begin
    for FromItem:=FromItem to ToItem do InvalidateDip(FromItem);
  end;

Procedure TDipList.SetColumns(AColumns:Integer);
  var SaveSelected : Integer;
  begin
    { save current selected item and remove selection }
    SaveSelected:=GetItemIndex;
    FSelColumn:=-1;
    { update column-count }
    inherited SetColumns(AColumns);
    { set count again to update rowcount ... }
    SetCount(FCount);
    { select previously selected item }
    SetItemIndex(SaveSelected);
  end;
                                   
end.
