Unit WDbCtrls;

Interface

Uses Controls,DBCtrls,WCtrls,WinProcs,WinTypes;

Type TWDBRadioGroup = Class(TDBRadioGroup)
      Private
       FBoxStyle : TBoxStyle;
       Procedure   SetBoxStyle(AStyle:TBoxStyle);
      Protected
       Procedure   Paint; override;
      Published
       Property    BoxStyle:TBoxStyle read FBoxStyle write SetBoxStyle default bsBox;
     end;

Implementation

Uses Classes,Graphics,SysUtils;

Procedure TWDBRadioGroup.Paint;
  var X            : Integer;
      Y            : Integer;
      R            : TRect;
      S            : Array[0..255] of Char;
  begin
    with Canvas do if FBoxStyle=bsBox then inherited Paint
    else if FBoxStyle<>bsNone then begin
      Brush.Color:=Color;
      Font:=Self.Font;
      R:=Rect(0,0,0,0);
      StrPCopy(S,Caption);
      DrawText(Handle,S,Length(Caption),R,DT_LEFT or DT_SINGLELINE or DT_CALCRECT);
      if Enabled then DrawText(Handle,S,Length(Caption),R,DT_LEFT or DT_SINGLELINE)
      else begin
        Brush.Style:=bsClear;
        Font.Color:=clBtnHighlight;
        OffsetRect(R,1,1);
        DrawText(Handle,S,Length(Caption),R,DT_LEFT or DT_SINGLELINE);
        Font.Color:=clBtnShadow;
        OffsetRect(R,-1,-1);
        DrawText(Handle,S,Length(Caption),R,DT_LEFT or DT_SINGLELINE);
      end;
      X:=R.Right+3;
      Y:=Abs(Font.Height) Div 2+1;
      Pen.Color:=clBtnShadow;
      MoveTo(X,Y);
      LineTo(ClientWidth,Y);
      Pen.Color:=clBtnHighLight;
      MoveTo(X,Y+1);
      LineTo(ClientWidth,Y+1);
    end;
  end;

Procedure TWDBRadioGroup.SetBoxStyle
   (
   AStyle          : TBoxStyle
   );
  begin
    if AStyle<>FBoxStyle then begin
      FBoxStyle:=AStyle;
      Invalidate;
    end;
  end;

end.
