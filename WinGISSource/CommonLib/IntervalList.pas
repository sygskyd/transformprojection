Unit IntervalList;

Interface

Uses Classes;

Type PInterval          = ^TInterval;
     TInterval          = Record
       Left             : LongInt;
       Size             : LongInt;
     end;

     PIntervalItems     = ^TIntervalItems;
     TIntervalItems     = Array[0..32000 Div 4-1] of TInterval;

     TIntervalList      = Class(TPersistent)
      Private
       FCapacity        : Integer;
       FCount           : Integer;
       FIntervals       : PIntervalItems;
       Function    GetInterval(AIndex:Integer):PInterval;
       Function    GetText:String;
       Procedure   SetText(Const Text:String);
       Procedure   SetCapacity(ACapacity:Integer);
      Protected
       Procedure   InsertInterval(Index:Integer;Left:Integer;Size:Integer);
       Procedure   AssignTo(Dest:TPersistent); override; 
      Public
       Constructor Create(ACapacity:Integer);
       Destructor  Destroy; override;
       Procedure   Add(ALeft,ASize:LongInt); overload;
       Procedure   Add(const Intervals:TIntervalList); overload;
       Function    Add(Const Text:String):Boolean; overload;
       Function    FindWithSize(ASize:LongInt):LongInt;
       Property    Capacity:Integer read FCapacity write SetCapacity;
       Procedure   Clear;
       Function    Contains(Const Interval:TInterval):Boolean; overload;
       Function    Contains(Const Intervals:TIntervalList):Boolean; overload;
       Property    Count:Integer read FCount;
       Procedure   Delete(ALeft,ASize:LongInt); overload;
       Function    Delete(Const Text:String):Boolean; overload;
       Procedure   Expand;
       Function    First:LongInt;
       Function    IsEmpty:Boolean;
       Procedure   Intersect(Const Interval:TInterval); overload;
       Procedure   Intersect(Const Intervals:TIntervalList); overload;
       Property    Intervals[AIndex:Integer]:PInterval read GetInterval; default;
       Property    Items:PIntervalItems read FIntervals;
       Function    Next(Previous:Integer):Integer;
       Function    Overlaps(Const Interval:TInterval):Boolean; overload;
       Function    Overlaps(Const Intervals:TIntervalList):Boolean; overload;
       Function    Size:Integer;
       Procedure   Subtract(Const Interval:TInterval); overload;
       Procedure   Subtract(Const Intervals:TIntervalList); overload;
       Property    Text:String read GetText write SetText;
       Procedure   Union(Const Interval:TInterval); overload;
       Procedure   Union(Const Intervals:TIntervalList); overload;
{$IFDEF DEBUG}
Procedure Print
   (
   var F           : TextFile
   );
{$ENDIF}
     end;

Function ContainsInterval(Const Interval1,Interval2:TInterval):Boolean;
Function Interval(Left,Size:Integer):TInterval;     
Function IntersectIntervals(Const Interval1:TInterval;Const Interval2:TInterval):TInterval;
Function OverlapIntervals(Const Interval1,Interval2:TInterval):Boolean;

Implementation

Uses NumTools,SysUtils,StrTools;

Function Interval(Left,Size:Integer):TInterval;
begin
  Result.Left:=Left;
  Result.Size:=Size;
end;

Function OverlapIntervals(Const Interval1,Interval2:TInterval):Boolean;
begin
  Result:=(Interval1.Left<Interval2.Left+Interval2.Size)
      and (Interval1.Left+Interval1.Size>Interval2.Left);
end;

Function ContainsInterval(Const Interval1,Interval2:TInterval):Boolean;
begin
  Result:=(Interval1.Left<=Interval2.Left)
      and (Interval1.Left+Interval1.Size>=Interval2.Left+Interval2.Size);
end;

Function IntersectIntervals(Const Interval1,Interval2:TInterval):TInterval;
begin
  if OverlapIntervals(Interval1,Interval2) then begin
    Result.Left:=Max(Interval1.Left,Interval2.Left);
    Result.Size:=Min(Interval1.Left+Interval1.Size,Interval2.Left+Interval2.Size)-Result.Left;
  end
  else begin
    Result.Left:=0;
    Result.Size:=0;
  end;  
end;

{==============================================================================}
{ TIntervalList                                                                }
{==============================================================================}

{******************************************************************************}
{ Constructor TIntervalList.Create                                             }
{------------------------------------------------------------------------------}
{ Erzeugt eine neue Intervall-Liste. Die Listengr��e wird auf ACapacity        }
{ gesetzt.                                                                     }
{******************************************************************************}
Constructor TIntervalList.Create(ACapacity:Integer);
begin
  inherited Create;
  Capacity:=ACapacity;
end;

{******************************************************************************}
{ Destructor TIntervalList.Destroy                                             }
{------------------------------------------------------------------------------}
{ Gibt den Speicher des Objekts frei.                                          }
{******************************************************************************}
Destructor TIntervalList.Destroy;
begin
  if Capacity>0 then FreeMem(FIntervals,FCapacity*SizeOf(TInterval));
  inherited Destroy;
end;

{******************************************************************************}
{ Function TIntervalList.Getinterval                                           }
{------------------------------------------------------------------------------}
{ Liefert einen Zeiger auf das Intervall mit dem angegebenen Index.            }
{******************************************************************************}
Function TIntervalList.GetInterval(AIndex:Integer):PInterval;
begin
  {$IFDEF DEBUG}
  if AIndex>Count then Raise Exception.Create('IntervalList.GetInterval: Index out of Range');
  {$ENDIF}
  Result:=@FIntervals^[AIndex];
end;

{******************************************************************************}
{ Procedure TIntervalList.Add                                                  }
{------------------------------------------------------------------------------}
{ F�gt ein Intervall in die Liste ein. �berlappende Intervalle werden          }
{ zusammengezogen.                                                             }
{******************************************************************************}
Procedure TIntervalList.Add(ALeft,ASize:LongInt);
var Cnt          : Integer;
    BSize        : LongInt;
begin
  if FCount=0 then InsertInterval(0,ALeft,ASize)
  else begin
    // Einf�geposition suchen (Intervall, wo Left>=ALeft
    Cnt:=0;
    while (Cnt<FCount) and (FIntervals^[Cnt].Left<=ALeft) do Inc(Cnt);
    // Intervalle, die sich mit dem einzuf�genden �berlappen l�schen          
    while (Cnt<FCount) and (FIntervals^[Cnt].Left<=ALeft+ASize) do begin
      BSize:=FIntervals^[Cnt].Left+FIntervals^[Cnt].Size-ALeft;
      if BSize>ASize then ASize:=BSize;
      if Cnt+1<FCount then Move(FIntervals^[Cnt+1],FIntervals^[Cnt],
          (FCount-Cnt-1)*SizeOf(TInterval));
      Dec(FCount);
    end;
    { wenn �berlappung mit vorherigem Intervall->vorheriges Intervall updaten}
    if (Cnt>0) and (FIntervals^[Cnt-1].Left+FIntervals^[Cnt-1].Size>=ALeft) then begin
      BSize:=ALeft+ASize-FIntervals^[Cnt-1].Left;
      if BSize>FIntervals^[Cnt-1].Size then FIntervals^[Cnt-1].Size:=BSize;
    end
    else InsertInterval(Cnt,ALeft,ASize);
  end;
end;

Procedure TIntervalList.InsertInterval(Index,Left,Size:Integer);
begin
  if FCount=FCapacity then Expand;
  if Index<FCount then Move(FIntervals^[Index],FIntervals^[Index+1],
      (FCount-Index)*SizeOf(TInterval));
  FIntervals^[Index].Left:=Left;
  FIntervals^[Index].Size:=Size;
  Inc(FCount);
end;
   
{******************************************************************************}
{ Procedure TIntervalList.Delete                                               }
{------------------------------------------------------------------------------}
{ L�scht ein Intervall aus der Liste.                                          }
{******************************************************************************}
Procedure TIntervalList.Delete(ALeft,ASize:LongInt);
var Cnt          : Integer;
    Right        : LongInt;
    ARight       : LongInt;
begin
  Cnt:=0;
  ARight:=ALeft+ASize-1;
  while Cnt<FCount do begin
    Right:=FIntervals^[Cnt].Left+FIntervals^[Cnt].Size-1;
    if FIntervals^[Cnt].Left<ALeft then begin
      if Right<ALeft then begin
        Inc(Cnt);
        Continue;
      end
      else if Right<=ARight then FIntervals^[Cnt].Size:=ALeft-FIntervals^[Cnt].Left
      else if Right>ARight then begin
        if FCount=FCapacity then Expand;
        if Cnt<FCount then Move(FIntervals^[Cnt],FIntervals^[Cnt+1],(FCount-Cnt)*SizeOf(TInterval));
        FIntervals^[Cnt+1].Left:=ARight+1;
        FIntervals^[Cnt+1].Size:=Right-ARight;
        FIntervals^[Cnt].Size:=ALeft-FIntervals^[Cnt].Left;
        Inc(FCount);
        Break;
      end;
    end
    else if FIntervals^[Cnt].Left<=ARight then begin
      if Right<=ARight then begin
        if Cnt+1<FCount then Move(FIntervals^[Cnt+1],FIntervals^[Cnt],(FCount-Cnt-1)*SizeOf(TInterval));
        Dec(FCount);
        Dec(Cnt);
      end
      else begin
        FIntervals^[Cnt].Left:=ARight+1;
        FIntervals^[Cnt].Size:=Right-ARight;
      end;
    end
    else Break;
    Inc(Cnt);
  end;
end;

{******************************************************************************}
{ Procedure TIntervalList.Setcapacity                                          }
{------------------------------------------------------------------------------}
{ Vergr��ert die Speicherkapazit�t der Liste auf den angegebenen Wert. Sind    }
{ bereits Eintr�ge in der Liste enthalten, so werden diese in den neuen        }
{ Speicherbereich kopiert.                                                     }
{******************************************************************************}
Procedure TIntervalList.SetCapacity(ACapacity:Integer);
var HelpPtr      : PIntervalItems;
begin
  {$IFDEF DEBUG}
  if ACapacity*SizeOf(TInterval)>MaxInt then
      Raise Exception.Create('TIntervalList.SetCapacity: Structure too large');
  {$ENDIF}
  if ACapacity>FCapacity then begin
    if FCapacity=0 then GetMem(FIntervals,ACapacity*SizeOf(TInterval))
    else begin
      GetMem(HelpPtr,ACapacity*SizeOf(TInterval));
      Move(FIntervals^[0],HelpPtr^[0],FCapacity*SizeOf(TInterval));
      FreeMem(FIntervals,FCapacity*SizeOf(TInterval));
      FIntervals:=HelpPtr;
    end;
    FCapacity:=ACapacity;
  end;
end;

{******************************************************************************}
{ Procedure TIntervalList.Expand                                               }
{------------------------------------------------------------------------------}
{ Vergr��ert die Speicherkapazit�t der Liste. Die Anzahl der hinzugef�gten     }
{ Eintr�ge ist abh�ngig von der aktuellen Gr��e der Liste.                     }
{******************************************************************************}
Procedure TIntervalList.Expand;
begin
  if Capacity<4 then Capacity:=Capacity+4
  else if Capacity<9 then Capacity:=Capacity+8
  else Capacity:=Capacity+16;
end;

{******************************************************************************}
{ Function TIntervalList.FindWithSize                                          }
{------------------------------------------------------------------------------}
{ Sucht ein Intervall, das gleich gro� oder gr��er wie die angegebene Gr��e    }
{ ist. FindWithSize gibt Left dieses Intervalls zur�ck, oder -1, wenn kein     }
{ passendes Intervall gefunden wird.                                           }
{******************************************************************************}
Function TIntervalList.FindWithSize(ASize:LongInt):LongInt;
var Cnt          : Integer;
begin
  Cnt:=0;
  while Cnt<FCount do begin
    if FIntervals^[Cnt].Size>=ASize then begin
      Result:=FIntervals^[Cnt].Left;
      Exit;
    end;
    Inc(Cnt);
  end;
  Result:=-1;
end;

Procedure TIntervalList.Clear;
begin
  FCount:=0;
end;

{$IFDEF DEBUG}
Procedure TIntervalList.Print(var F:TextFile);
var Cnt          : Integer;
    Interval     : PInterval;
begin
  Write(F,'Intervallist: ');
  for Cnt:=0 to Count-1 do begin
    Interval:=Intervals[Cnt];
    Write(F,'(',Interval^.Left,',',Interval^.Size,') ');
  end;
  Writeln(F);
end;
{$ENDIF}

Function TIntervalList.GetText:String;
var Cnt          : Integer; 
begin
  Result:='';
  for Cnt:=0 to FCount-1 do begin
    if Cnt>0 then Result:=Result+', ';
    if FIntervals^[Cnt].Size=1 then Result:=Result+IntToStr(FIntervals^[Cnt].Left)
    else Result:=Result+IntToStr(FIntervals[Cnt].Left)+' - '
        +IntToStr(FIntervals^[Cnt].Left+FIntervals^[Cnt].Size-1);
  end;
end;

Function TIntervalList.Add(Const Text:String):Boolean;
var Items        : Integer;
    Strings      : Array of String;//Array[0..255] of String;
    Left         : LongInt;
    Size         : LongInt;
    Cnt          : Integer;
    Elements     : Integer;
begin
  Result:=TRUE;
  Items:=SplitStringList(Text,'''',',',Strings);
  for Cnt:=0 to Items-1 do begin
    Elements:=ScanString('%l-%l',Strings[Cnt],[@Left,@Size]);
    if Elements=1 then Add(Left,1)
    else if Elements=2 then Add(Left,Size-Left+1)
    else begin
      Result:=FALSE;
      Exit;
    end;  
  end;
end;

Function TIntervalList.Delete(Const Text:String):Boolean;
var Items        : Integer;
    Strings      : Array of String;
    Left         : LongInt;
    Size         : LongInt;
    Cnt          : Integer;
    Elements     : Integer;
begin
  Result:=TRUE;
  Items:=SplitStringList(Text,'''',',',Strings);
  for Cnt:=0 to Items-1 do begin
    Elements:=ScanString('%l-%l',Strings[Cnt],[@Left,@Size]);
    if Elements=1 then Delete(Left,1)
    else if Elements=2 then Delete(Left,Size-Left+1)
    else begin
      Result:=FALSE;
      Exit;
    end;
  end;
end;

Procedure TIntervalList.SetText(Const Text:String);
begin
  Clear;
  Add(Text);
end;
   
Procedure TIntervalList.Union(Const Interval:TInterval);
begin
  Add(Interval.Left,Interval.Size);
end;

Procedure TIntervalList.Union(Const Intervals:TIntervalList);
var Cnt          : Integer;
begin
  for Cnt:=0 to Intervals.Count-1 do
      Add(Intervals[Cnt].Left,Intervals[Cnt].Size);
end;

Procedure TIntervalList.Subtract(Const Interval:TInterval);
begin
  Delete(Interval.Left,Interval.Size);
end;

Procedure TIntervalList.Subtract(Const Intervals:TIntervalList);
var Cnt          : Integer;
begin
  for Cnt:=0 to Intervals.Count-1 do
      Delete(Intervals[Cnt].Left,Intervals[Cnt].Size);
end;

Procedure TIntervalList.Add(Const Intervals:TIntervalList);
var Cnt          : Integer;
begin
  for Cnt:=0 to Intervals.Count-1 do
      Add(Intervals[Cnt].Left,Intervals[Cnt].Size);
end;

Function TIntervalList.IsEmpty:Boolean;
begin
  Result:=FCount=0;
end;

Function TIntervalList.Contains(Const Interval:TInterval):Boolean;
var Cnt          : Integer;
begin
  for Cnt:=0 to FCount-1 do if ContainsInterval(FIntervals[Cnt],Interval) then begin
    Result:=TRUE;
    Exit;
  end;
  Result:=FALSE;
end;

Function TIntervalList.Contains(Const Intervals:TIntervalList):Boolean;
var Cnt          : Integer;
begin
  for Cnt:=0 to Intervals.Count-1 do if not Contains(Intervals[Cnt]^) then begin
    Result:=FALSE;
    Exit;
  end;
  Result:=TRUE;
end;

Function TIntervalList.Overlaps(Const Interval:TInterval):Boolean;
var Cnt          : Integer;
begin
  for Cnt:=0 to FCount-1 do if OverlapIntervals(FIntervals[Cnt],Interval) then begin
    Result:=TRUE;
    Exit;
  end;
  Result:=FALSE;
end;

Function TIntervalList.Overlaps(Const Intervals:TIntervalList):Boolean;
var Cnt          : Integer;
begin
  for Cnt:=0 to Intervals.Count-1 do if not Overlaps(Intervals[Cnt]^) then begin
    Result:=FALSE;
    Exit;
  end;
  Result:=TRUE;
end;

Procedure TIntervalList.Intersect(Const Interval:TInterval);
var Cnt          : Integer;
    NewList      : TIntervalList;
    NewInterval  : TInterval;
begin
  NewList:=TIntervalList.Create(FCapacity);
  for Cnt:=0 to FCount-1 do if OverlapIntervals(FIntervals[Cnt],Interval) then begin
    NewInterval.Left:=Max(FIntervals[Cnt].Left,Interval.Left);
    NewInterval.Size:=Min(FIntervals[Cnt].Left+FIntervals[Cnt].Size,
        Interval.Left+Interval.Size)-NewInterval.Left;
    NewList.InsertInterval(NewList.Count,NewInterval.Left,NewInterval.Size);
  end;
  Assign(NewList);
  NewList.Free;
end;

Procedure TIntervalList.Intersect(Const Intervals:TIntervalList);
var Cnt          : Integer;
    Cnt1         : Integer;
    NewList      : TIntervalList;
    NewInterval  : TInterval;
begin
  NewList:=TIntervalList.Create(FCapacity);
  for Cnt:=0 to FCount-1 do for Cnt1:=0 to Intervals.Count-1 do
      if OverlapIntervals(FIntervals[Cnt],Intervals[Cnt1]^) then begin
    NewInterval.Left:=Max(FIntervals[Cnt].Left,Intervals[Cnt1]^.Left);
    NewInterval.Size:=Min(FIntervals[Cnt].Left+FIntervals[Cnt].Size,
        Intervals[Cnt1]^.Left+Intervals[Cnt1]^.Size)-NewInterval.Left;
    NewList.InsertInterval(NewList.Count,NewInterval.Left,NewInterval.Size);
  end;
  Assign(NewList);
  NewList.Free;
end;

Procedure TIntervalList.AssignTo(Dest:TPersistent);
begin
  if not (Dest is TIntervalList) then inherited AssignTo(Dest)
  else with Dest as TIntervalList do begin
    SetCapacity(Self.Capacity);
    FCount:=Self.FCount;
    Move(Self.FIntervals^,FIntervals^,FCount*SizeOf(TInterval));
  end;
end;

Function TIntervalList.Size:Integer;
var Cnt          : Integer;
begin
  Result:=0;
  for Cnt:=0 to FCount-1 do Inc(Result, FIntervals[Cnt].Size);
end;

Function TIntervalList.First:LongInt;
begin
  if FCount=0 then Result:=-1
  else Result:=FIntervals[0].Left;
end;

function TIntervalList.Next(Previous: Integer): Integer;
var Cnt            : Integer;
begin
  Result:=-MaxInt;
  Cnt:=0;
  while (Cnt<FCount) and (FIntervals^[Cnt].Left<=Previous) do Inc(Cnt);
  if (Cnt<FCount) and (Previous>=FIntervals^[Cnt].Left+FIntervals^[Cnt].Size-1) then
      Inc(Cnt);
  if (Cnt<FCount) then begin
    if FIntervals^[Cnt].Left<=Previous then Result:=Previous+1
    else Result:=FIntervals^[Cnt].Left;
  end;
end;

end.
