unit MSConvert;

interface

uses Classes, DesignIntf, DesignEditors, SysUtils, Windows;

const
  fRegAppPreview = $0004;
  fRegAppSupportNonOem = $0008;

  fceNoErr = 0;
  fceOpenInFileErr = -1;
  fceReadErr = -2;
  fceOpenConvErr = -3;
  fceWriteErr = -4;
  fceInvalidFile = -5;
  fceOpenExceptErr = -6;
  fceWriteExceptErr = -7;
  fceNoMemory = -8;
  fceInvalidDoc = -9;
  fceDiskFull = -10;
  fceDocTooLarge = -11;
  fceOpenOutFileErr = -12;
  fceUserCancel = -13;
  fceWrongFileType = -14;

type
  ETextConverter = class(Exception)
  end;

  TOutputCallback = function(Count: Integer; PercentComplete: Integer): Integer; stdcall;
  TInputCallback = function(Flags: Integer; PercentComplete: Integer): Integer; stdcall;
  TForeignToRTF = function(FileName: THandle; Param: Pointer; Buffer, Description,
    SubSet: THandle; OutputCallback: TOutputCallback): Integer; stdcall;
  TRTFToForeign = function(FileName: THandle; Param: Pointer; Buffer,
    ClassName: THandle; InputCallback: TInputCallback): Integer; stdcall;
  TInitConverter = function(Window: THandle; ApplicationName: PChar): Integer; stdcall;
  TUninitConverter = procedure; stdcall;
  TIsFormatCorrect = function(FileName: THandle; ClassName: THandle): Integer; stdcall;
  TRegisterApp = function(Flags: LongInt; Params: Pointer): HGLOBAL;
  TCompletionNotify = procedure(Sender: TObject; Percentage: Integer; var Continue: Boolean) of object;
  TFetchErrorText = function(ErrorCode: Integer; Buffer: PChar; BufferSize: Integer): Integer; stdcall;

  TTextConverter = class(TComponent)
  private
    FBufferPtr: PChar;
    FDLLHandle: THandle;
    FExportConverter: string;
    FFetchErrorText: TFetchErrorText;
    FFileName: string;
    FForeignToRTF: TForeignToRTF;
    FImportConverter: string;
    FInitConverter: TInitConverter;
    FIsFormatCorrect: TIsFormatCorrect;
    FNotify: TCompletionNotify;
    FRegisterApp: TRegisterApp;
    FRTFToForeign: TRTFToForeign;
    FStream: TStream;
    FTextOnly: Boolean;
    FUninitConverter: TUninitConverter;
    function DoRead(Count: Integer; PercentComplete: Integer): Integer;
    function GetExportConverter: string;
    class function GetExportConverters: TStringList;
    function GetImportConverter: string;
    class function GetImportConverters: TStringList;
    procedure LoadConverter(const DLLName: string; Import: Boolean);
    procedure SetExportConverter(const Name: string);
    procedure SetImportConverter(const Name: string);
    procedure ThrowConverterError(ErrorCode: Integer);
    procedure UnloadCOnverter;
  public
    property ExportConverters: TStringList read GetExportConverters;
    property ImportConverters: TStringList read GetImportConverters;
    function IsFormatCorrect(const FileName: string): Boolean;
    procedure Read(OutputTo: TStream);
    procedure Write(OutputFrom: TStream);
  published
    property CompletionNotify: TCompletionNotify read FNotify write FNotify;
    property ImportConverter: string read GetImportConverter write SetImportConverter;
    property ExportConverter: string read GetExportConverter write SetExportConverter;
    property FileName: string read FFileName write FFileName;
    property TextOnly: Boolean read FTextOnly write FTextOnly default FALSE;
  end;

  TExportConverterProperty = class(TStringProperty)
    function GetAttributes: TPropertyAttributes; override;
    procedure GetValues(Proc: TGetStrProc); override;
  end;

  TImportConverterProperty = class(TStringProperty)
    function GetAttributes: TPropertyAttributes; override;
    procedure GetValues(Proc: TGetStrProc); override;
  end;

procedure Register;

implementation

uses Controls, Forms, ActiveX, MultiLng, Registry;

const
  BufferSize = 8192;

var
  ActiveConverter: TTextConverter;
  ImportConverters: TStringList;
  ExportConverters: TStringList;

type
  PConverterInfo = ^TConverterInfo;
  TConverterInfo = record
    ClassName: string;
    DLLName: string;
    Name: string;
  end;

function StringToHGLOBAL(const Str: string): HGLOBAL;
var
  Dest: PChar;
begin
  // allocate global memory-block
  Result := GlobalAlloc(GHND, Length(Str) * 2 + 1);
  // copy characters
  Dest := GlobalLock(Result);
  StrCopy(Dest, PChar(Str));
  GlobalUnlock(Result);
end;

procedure ThrowError(const Text: string);
var
  MsgText: PChar;
  Exception: ETextConverter;
begin
  FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER or FORMAT_MESSAGE_FROM_SYSTEM, nil,
    GetLastError(), SUBLANG_DEFAULT shl 10 or LANG_NEUTRAL, @MsgText, 0, nil);
  Exception := ETextConverter.Create(Text + ' ' + MsgText);
  LocalFree(Integer(MsgText));
  raise Exception;
end;

procedure ClearConverterList(AList: TStringList);
var
  Cnt: Integer;
begin
  for Cnt := 0 to AList.Count - 1 do
    Dispose(PConverterInfo(AList.Objects[Cnt]));
  AList.Clear;
end;

function InfoFromClass(AList: TStringList; const ClassName: string): PConverterInfo;
var
  Cnt: Integer;
begin
  for Cnt := 0 to AList.Count - 1 do
    if AnsiCompareText(ClassName,
      PConverterInfo(AList.Objects[Cnt])^.ClassName) = 0 then
    begin
      Result := PConverterInfo(AList.Objects[Cnt]);
      Exit;
    end;
  Result := nil;
end;

procedure CreateConverterList;
var
  Registry: TRegistry;

  procedure AddConverters(const RegName: string);
  var
    Keys: TStringList;
    Info: PConverterInfo;
    Cnt: Integer;
  begin
    Keys := TStringList.Create;
    try
      // read import-filters
      if Registry.OpenKeyReadOnly(RegName + '\Import') then
      begin
        Registry.GetKeyNames(Keys);
        // create a new converter-info if there does not already exist one
        for Cnt := 0 to Keys.Count - 1 do
          if InfoFromClass(ImportConverters,
            Keys[Cnt]) = nil then
            if Registry.OpenKeyReadOnly(RegName + '\Import\'
              + Keys[Cnt]) then
            begin
              Info := New(PConverterInfo);
              Info^.ClassName := Keys[Cnt];
              Info^.DLLName := Registry.ReadString('Path');
              Info^.Name := Registry.ReadString('Name');
              ImportConverters.AddObject(Info^.Name, Pointer(Info));
            end;
      end;
      // read export-filters
      Keys.Clear;
      if Registry.OpenKeyReadOnly(RegName + '\Export') then
      begin
        Registry.GetKeyNames(Keys);
        // create a new converter-info if there does not already exist one
        for Cnt := 0 to Keys.Count - 1 do
          if InfoFromClass(ExportConverters,
            Keys[Cnt]) = nil then
            if Registry.OpenKeyReadOnly(RegName + '\Export\'
              + Keys[Cnt]) then
            begin
              Info := New(PConverterInfo);
              Info^.ClassName := Keys[Cnt];
              Info^.DLLName := Registry.ReadString('Path');
              Info^.Name := Registry.ReadString('Name');
              ExportConverters.AddObject(Info^.Name, Pointer(Info));
            end;
      end;
    finally
      Keys.Free;
    end;
  end;
begin
  if ImportConverters = nil then
  begin
    ImportConverters := TStringList.Create;
    ExportConverters := TStringList.Create;
    Registry := TRegistry.Create;
    try
      Registry.RootKey := HKEY_LOCAL_MACHINE;
      AddConverters('\Software\Microsoft\Shared Tools\Text Converters');
      Registry.RootKey := HKEY_CURRENT_USER;
      AddConverters('\Software\Microsoft\Office\8.0\Word\Text Converters');
      AddConverters('\Software\Microsoft\Word\7.0\Text Converters');
    finally
      Registry.Free;
    end;
  end;
end;

{==============================================================================+
  TTextConverter
+==============================================================================}

{******************************************************************************+
  Procedure TTextConverter.LoadConverter
--------------------------------------------------------------------------------
  Loads the text-converter-dll and initializes the function-pointers. Checks
  if all necessary functions are exported by the dll.
+******************************************************************************}

procedure TTextConverter.LoadConverter(const DLLName: string; Import: Boolean);
var
  OwnerHandle: THandle;
begin
  FDLLHandle := LoadLibrary(PChar(DLLName));
  try
    if FDLLHandle = 0 then
      ThrowError(MlgStringList['TextConverter', 1]);
    FInitConverter := GetProcAddress(FDLLHandle, 'InitConverter32');
    FUninitConverter := GetProcAddress(FDLLHandle, 'UninitConverter');
    FForeignToRTF := GetProcAddress(FDLLHandle, 'ForeignToRtf32');
    FRTFToForeign := GetProcAddress(FDLLHandle, 'RtfToForeign32');
    FIsFormatCorrect := GetProcAddress(FDLLHandle, 'IsFormatCorrect32');
    FRegisterApp := GetProcAddress(FDLLHandle, 'RegisterApp');
    FFetchErrorText := GetProcAddress(FDLLHandle, 'CchFetchLpszError');
    if @FInitConverter = nil then
      ThrowError(MlgStringList['TextConverter', 2]);
    if Import and (@FForeignToRTF = nil) then
      ThrowError(MlgStringList['TextConverter', 2]);
    if not Import and (@FRTFToForeign = nil) then
      ThrowError(MlgStringList['TextConverter', 2]);
    if Owner is TWinControl then
      OwnerHandle := TWinControl(Owner).Handle
    else
      OwnerHandle := Application.Handle;
    if FInitConverter(OwnerHandle, PChar(ExtractFileName(Application.ExeName))) <> 1 then
      ThrowError(MlgStringList['TextConverter', 3]);
//    if @FRegisterApp<>NIL then FRegisterApp(fRegAppSupportNonOem,NIL);
  except
    UnloadConverter;
  end;
end;

{******************************************************************************+
  Procedure TTextConverter.UnloadConverter
--------------------------------------------------------------------------------
  Unloads the converter-dll and resets all internal variables.
+******************************************************************************}

procedure TTextConverter.UnloadConverter;
begin
  if @FUninitConverter <> nil then
    FUninitConverter;
  if FDLLHandle <> 0 then
  begin
    FreeLibrary(FDLLHandle);
    FDLLHandle := 0;
  end;
  FUninitConverter := nil;
  OleUninitialize;
end;

function ReadCallback(Count: Integer; PercentComplete: Integer): Integer; stdcall;
begin
  if ActiveConverter <> nil then
    Result := ActiveConverter.DoRead(Count, PercentComplete);
  Result := 0;
end;

function TTextConverter.DoRead(Count: Integer; PercentComplete: Integer): Integer;
var
  Continue: Boolean;
begin
  FStream.Write(FBufferPtr^, Count);
  if Assigned(FNotify) then
  begin
    Continue := TRUE;
    FNotify(Self, PercentComplete, Continue);
    if Continue then
      Result := 0
    else
      Result := -1;
  end
  else
    Result := 0;
end;

procedure TTextConverter.ThrowConverterError(ErrorCode: Integer);
var
  Text: array[0..255] of Char;
begin
  // did an error occur?
  if ErrorCode >= 0 then
    Exit;
  Text := '';
  // does the converter supply error-messages?
  if @FFetchErrorText <> nil then
    FFetchErrorText(ErrorCode, Text, SizeOf(Text));
  // use standard error-messages if the converter procides none
  if (Text = '') and (ErrorCode >= -14) then
    StrPCopy(Text, MlgStringList['TextConverter', 20 + Abs(ErrorCode)]);
  raise ETextConverter.CreateFmt(MlgStringList['TextConverter', 6] + ' ' + Text, [FFileName]);
end;

procedure TTextConverter.Read(OutputTo: TStream);
var
  BufferHandle: HGLOBAL;
  NameHandle: HGLOBAL;
  DescHandle: HGLOBAL;
  SubHandle: HGLOBAL;
  Info: PConverterInfo;
  ErrorCode: Integer;
begin
  if FImportConverter = '' then
    ThrowError(MlgStringList['TextConverter', 4]);
  CreateConverterList;
  Info := InfoFromClass(ImportConverters, FImportConverter);
  if Info = nil then
    ThrowError(MlgStringList['TextConverter', 5]);
  LoadConverter(Info^.DLLName, TRUE);
  try
    BufferHandle := GlobalAlloc(GHND, BufferSize);
    if BufferHandle <> 0 then
    begin
      FBufferPtr := GlobalLock(BufferHandle);
      FStream := OutputTo;
      CharToOEM(PChar(FileName), PChar(FileName));
      NameHandle := StringToHGLOBAL(FileName);
      DescHandle := StringToHGLOBAL('');
      SubHandle := StringToHGLOBAL('');
      if NameHandle * DescHandle * SubHandle <> 0 then
      begin
        ActiveConverter := Self;
        ThrowConverterError(FForeignToRTF(NameHandle, nil, BufferHandle, DescHandle,
          SubHandle, @ReadCallback));
      end;
      if NameHandle <> 0 then
        GlobalFree(NameHandle);
      if DescHandle <> 0 then
        GlobalFree(DescHandle);
      if SubHandle <> 0 then
        GlobalFree(SubHandle);
      GlobalUnlock(BufferHandle);
      GlobalFree(BufferHandle);
      ActiveConverter := nil;
    end;
  finally
    UnloadConverter;
  end;
end;

procedure TTextConverter.Write(OutputFrom: TStream);
begin
end;

function TTextConverter.IsFormatCorrect(const FileName: string): Boolean;
var
  NameHandle: HGLOBAL;
  DescHandle: HGLOBAL;
  a: INteger;
begin
  Result := FALSE;
  if @FIsFormatCorrect <> nil then
  begin
    NameHandle := StringToHGLOBAL(FileName);
    DescHandle := GlobalAlloc(GHND, 256);
    if NameHandle * DescHandle <> 0 then
      a := FIsFormatCorrect(NameHandle, DescHandle);
    if NameHandle <> 0 then
      GlobalFree(NameHandle);
    if DescHandle <> 0 then
      GlobalFree(DescHandle);
  end;
end;

function TTextConverter.GetExportConverter: string;
var
  Info: PConverterInfo;
begin
  if csWriting in ComponentState then
    Result := FExportConverter
  else
  begin
    Info := InfoFromClass(ExportConverters, FExportConverter);
    if Info <> nil then
      Result := Info^.Name
    else
      Result := '';
  end;
end;

function TTextConverter.GetImportConverter: string;
var
  Info: PConverterInfo;
begin
  if csWriting in ComponentState then
    Result := FImportConverter
  else
  begin
    Info := InfoFromClass(ImportConverters, FImportConverter);
    if Info <> nil then
      Result := Info^.Name
    else
      Result := '';
  end;
end;

procedure TTextConverter.SetExportConverter(const Name: string);
var
  Index: Integer;
begin
  if csReading in ComponentState then
    FExportConverter := Name
  else
  begin
    Index := ExportConverters.IndexOf(Name);
    if Index >= 0 then
      FExportConverter := PConverterInfo(ExportConverters.
        Objects[Index])^.ClassName
    else
      FExportConverter := '';
  end;
end;

procedure TTextConverter.SetImportConverter(const Name: string);
var
  Index: Integer;
begin
  if csReading in ComponentState then
    FImportConverter := Name
  else
  begin
    Index := ImportConverters.IndexOf(Name);
    if Index >= 0 then
      FImportConverter := PConverterInfo(ImportConverters.
        Objects[Index])^.ClassName
    else
      FImportConverter := '';
  end;
end;

class function TTextConverter.GetImportConverters: TStringList;
begin
  CreateConverterList;
  Result := MSConvert.ImportConverters;
end;

class function TTextConverter.GetExportConverters: TStringList;
begin
  CreateConverterList;
  Result := MSConvert.ExportConverters;
end;

{==============================================================================+

+==============================================================================}

{ TImportConverterProperty }

function TImportConverterProperty.GetAttributes: TPropertyAttributes;
begin
  Result := [paValueList, paSortList, paMultiSelect];
end;

procedure TImportConverterProperty.GetValues(Proc: TGetStrProc);
var
  Cnt: Integer;
begin
  CreateConverterList;
  if ImportConverters <> nil then
    for Cnt := 0 to ImportConverters.Count - 1 do
      Proc(ImportConverters[Cnt]);
end;

function TExportConverterProperty.GetAttributes: TPropertyAttributes;
begin
  Result := [paValueList, paSortList, paMultiSelect];
end;

procedure TExportConverterProperty.GetValues(Proc: TGetStrProc);
var
  Cnt: Integer;
begin
  CreateConverterList;
  if ExportConverters <> nil then
    for Cnt := 0 to ExportConverters.Count - 1 do
      Proc(ExportConverters[Cnt]);
end;

procedure Register;
begin
  RegisterComponents('Text', [TTextConverter]);
  RegisterPropertyEditor(TypeInfo(string), TTextConverter, 'ExportConverter', TExportConverterProperty);
  RegisterPropertyEditor(TypeInfo(string), TTextConverter, 'ImportConverter', TImportConverterProperty);
end;

initialization
  ActiveConverter := nil;
  ImportConverters := nil;
  ExportConverters := nil;

finalization
  if ImportConverters <> nil then
  begin
    ClearConverterList(ImportConverters);
    ImportConverters.Free;
  end;
  if ExportConverters <> nil then
  begin
    ClearConverterList(ExportConverters);
    ExportConverters.Free;
  end;

end.

