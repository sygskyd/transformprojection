unit WinOSInfo;

interface

type

TOSInfo = record
        WingisDir: AnsiString;
        WingisIniFileName: AnsiString;
        CommonAppDataDir: AnsiString;
        LocalAppDataDir: AnsiString;
        DocDataDir: AnsiString;
        TempDataDir: AnsiString;
        OSVersion: Double;
end;

var

OSInfo: TOSInfo;

function GetDirSize(ADir: AnsiString; ASubDir: Boolean; var AFilesCount: Integer): Int64;

implementation

uses Windows, SysUtils, SHFolder, FileCtrl;

function GetDirSize(ADir: AnsiString; ASubDir: Boolean; var AFilesCount: Integer): Int64;
var
Rec: TSearchRec;
Found: Integer;
begin
     Result:=0;
     if ADir[Length(ADir)] <> '\' then begin
        ADir:=ADir + '\';
     end;
     Found:=FindFirst(ADir + '*.*',faAnyFile,Rec);
     while Found = 0 do begin
           Inc(Result,Rec.Size);
           if (Rec.Name[1] <> '.') and (Rec.Attr and faDirectory = 0) then begin
              Inc(AFilesCount);
           end;
           if (Rec.Attr and faDirectory > 0) and (Rec.Name[1] <> '.') and (ASubDir) then begin
              Inc(Result,GetDirSize(ADir + Rec.Name,True,AFilesCount));
           end;
           Found:=FindNext(Rec);
     end;
     FindClose(Rec);
end;

function CheckPath(s: AnsiString): AnsiString;
begin
     if s[Length(s)] <> '\' then begin
        s:=s + '\';
     end;
     Result:=s;
end;

procedure GetOSInfo;
var
OSVI: TOSVersionInfo;
Buffer: Array[0..MAX_PATH] of Char;
begin
     FillChar(OSVI, SizeOf(OSVI), 0);
     OSVI.dwOSVersionInfoSize := SizeOf(OSVI);
     GetVersionEx(OSVI);
     OSInfo.OSVersion:=StrToFloat(IntToStr(OSVI.dwMajorVersion) + DecimalSeparator + IntToStr(OSVI.dwMinorVersion));

     FillChar(Buffer, SizeOf(Buffer), 0);
     GetModuleFileName(hInstance,Buffer,Sizeof(Buffer));
     OSInfo.WingisDir:=CheckPath(ExtractFilePath(StrPas(Buffer)));
     OSInfo.WingisIniFileName:='Wingis.ini';

     FillChar(Buffer, SizeOf(Buffer), 0);
     SHGetFolderPath(0,CSIDL_PERSONAL,0,0,@Buffer);
     OSInfo.DocDataDir:=CheckPath(StrPas(Buffer)) + 'WinGIS\';
     if not DirectoryExists(OSInfo.DocDataDir) then begin
        CreateDir(OSInfo.DocDataDir);
     end;

     FillChar(Buffer, SizeOf(Buffer), 0);
     GetTempPath(MAX_PATH, @Buffer);
     OSInfo.TempDataDir:=CheckPath(StrPas(Buffer)) + 'WinGIS\';
     if not DirectoryExists(OSInfo.TempDataDir) then begin
        CreateDir(OSInfo.TempDataDir);
     end;

     FillChar(Buffer, SizeOf(Buffer), 0);
     SHGetFolderPath(0,CSIDL_COMMON_APPDATA,0,0,@Buffer);
     OSInfo.CommonAppDataDir:=CheckPath(StrPas(Buffer)) + 'WinGIS\';
     if not DirectoryExists(OSInfo.CommonAppDataDir) then begin
        CreateDir(OSInfo.CommonAppDataDir);
     end;

     FillChar(Buffer, SizeOf(Buffer), 0);
     SHGetFolderPath(0,CSIDL_LOCAL_APPDATA,0,0,@Buffer);
     OSInfo.LocalAppDataDir:=CheckPath(StrPas(Buffer)) + 'WinGIS\';
     if not DirectoryExists(OSInfo.LocalAppDataDir) then begin
        CreateDir(OSInfo.LocalAppDataDir);
     end;

     OSInfo.WingisIniFileName:=OSInfo.LocalAppDataDir + OSInfo.WingisIniFileName;
end;

procedure CopyIniFiles;
begin
     CopyFile(PChar(OSInfo.WingisDir + 'WinGIS.ini'),PChar(OSInfo.WingisIniFileName),True);
     CopyFile(PChar(OSInfo.WingisDir + 'DKMComb.ini'),PChar(OSInfo.LocalAppDataDir + 'DKMComb.ini'),True);
end;

initialization

GetOSInfo;
CopyIniFiles;

end.
