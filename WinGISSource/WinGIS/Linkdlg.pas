{******************************************************************************+
  Unit LnkDlg
--------------------------------------------------------------------------------
  Autor:Martin Forst
--------------------------------------------------------------------------------
  Dialog to show all linked picture- and OLE-files. The files are shown in
  a directory-tree. The dialog allows to delete files and to change path- and
  filenames.


------------------------------
November 2001, Brovak
Edit properties of Symbol : Images . Now it can be from Main menu and from Edit Properties,
for current symbol (In Symbol's Rollup) and for Edit Symol at Symbol's Editor.
Corrected work with Attached files
Corrected Path show for relative
+******************************************************************************}
unit LinkDlg;

interface
{$IFNDEF AXDLL} // <----------------- AXDLL
uses WinProcs, WinTypes, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, WCtrls, AM_Proj, ExtCtrls, TreeList, FileTree, UserIntf, CommDlg,
  TreeView, MultiLng, Menus, SelectFolderDlg, am_group;

type
  TLinkDialog = class(TWForm)
    CancelBtn: TButton;
    ChangeSourceBtn: TButton;
    DeleteBtn: TButton;
    EmbeddBtn: TButton;
    Group1: TWGroupBox;
    LinkList: TFileTreeView;
    MlgSection: TMlgSection;
    MultimediaCheck: TCheckBox;
    OkBtn: TButton;
    OLEObjectsCheck: TCheckBox;
    PicturesCheck: TCheckBox;
    PopupMenu: TPopupMenu;
    DeleteMenu: TMenuItem;
    ChangeSourceMenu: TMenuItem;
    N1: TMenuItem;
    ModeMenu: TMenuItem;
    AbsolutePathsMenu: TMenuItem;
    RelativePathsMenu: TMenuItem;
    OpenDialog: TWOpenDialog;
    DirectorySelectDialog: TSelectFolderDialog;
    ImagesInSymbolsCheck: TCheckBox;
    procedure ChangeSourceBtnClick(Sender: TObject);
    procedure DeleteBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure LinkListClick(Sender: TObject);
    function LinkListFilterItems(AInfo: TFileInfo): Boolean;
    procedure ViewCheckClick(Sender: TObject);
    procedure AbsolutePathsMenuClick(Sender: TObject);
    procedure RelativePathsMenuClick(Sender: TObject);
    procedure LinkListMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
  private
//       LastDirOpened    : string;
    FItemWidth: Integer;
    FProject: PProj;
    FPictures: Integer;
    FOLEObjects: Integer;
    FMultimedia: Integer;
    //brovak
    FSymbolsPictures: integer;
    //--brovak
    FDirectories: Integer;
    FFiles: Integer;
    FToDelete: TWList;
    FChanged: Boolean;
    LibShortName:string;
    LibRelativePath:string;
    LibAbsolutePath:string;

    procedure CreateLinkList;
    procedure UpdateEnabled;
    procedure UpdateSelectionInfo(SelectedOnly: Boolean = True);
    procedure UpdateRealCounts;
    procedure UpdateSymLibInfo;
    procedure UpdateSymInfoForExtend(CurrentSymbol: PSGroup);
    procedure FolderForLibImageSymbolsInTree;
  public
  {++Brovak}
   //CallFromMenu - call from MenuProject (true) or from SymbolProperties (false)
    CallFromMenu: boolean;
    SingleSymbol: PSGroup;
     {Brovak}
       // pointer to the project
    property Project: PProj read FProject write FProject;
   // property MasterProject: PProj read MasterFProject write MasterFProject;
  end;

{$ENDIF} // <----------------- AXDLL
implementation
{$IFNDEF AXDLL} // <----------------- AXDLL
{$R *.DFM}

uses AM_Def, AM_Index, AM_Layer, AM_MMedi, AM_Ole, BMPImage, Ole, Lists, RegDb, StrTools,
  AM_View, Img_Info
  , TrLib32, AttachHndl, Objects, UserUtils, symlib, Rollup, symrol, Am_sym, extLib,am_obj,symedit;

type
  TShows = set of (PicShow, OLEShow, MMShow, PicInSymbolsShow, RelPathShow);
var
  StoredShows: TShows;
{==============================================================================+
  TLinkDialog
+==============================================================================}

procedure TLinkDialog.FormShow(Sender: TObject);

begin
  // load dialog-settings from the registry

  with FProject^.Registry do
  begin
    CurrentPath := '\User\WinGISUI\LinkDialog';
//    Store original setting for options
    StoredShows := [];

    LinkList.RelativePaths := ReadBool('ShowRelativePaths');
    if LastError <> rdbeNoError then
      LinkList.RelativePaths := TRUE;
    if LinkList.RelativePaths then
    begin
      RelativePathsMenu.Checked := TRUE;
      StoredShows := StoredShows + [RelPathShow];
    end
    else
      AbsolutePathsMenu.Checked := TRUE;


      PicturesCheck.Checked := ReadBool('ShowPictures');
      if LastError <> rdbeNoError then
        PicturesCheck.Checked := True;
      if PicturesCheck.Checked then
        StoredShows := StoredShows + [PicShow];


      OLEObjectsCheck.Checked := ReadBool('ShowOLEObjects');
      if LastError <> rdbeNoError then
        OLEObjectsCheck.Checked := True;
      if OLEObjectsCheck.Checked then
        StoredShows := StoredShows + [OLEShow];

      MultimediaCheck.Checked := ReadBool('ShowMultimedia');
      if LastError <> rdbeNoError then
        MultimediaCheck.Checked := True;
      if MultimediaCheck.Checked then
        StoredShows := StoredShows + [MMShow];

 {Brovak}
    ImagesInSymbolsCheck.Checked := ReadBool('ShowSymbolsImages');
    if LastError <> rdbeNoError then
      ImagesInSymbolsCheck.Checked := True;
    if ImagesInSymbolsCheck.Checked then
      StoredShows := StoredShows + [PicInSymbolsShow];

            if CallFromMenu = false then
     begin;
      PicturesCheck.Checked := false;
      PicturesCheck.Enabled := false;
      OLEObjectsCheck.Checked := false;
      OLEObjectsCheck.Enabled := false;
      MultimediaCheck.Checked := false;
      MultimediaCheck.Enabled := false;
     end


 {--Brovak}


  end;
  // create the list of all links in the project
  Screen.Cursor := crHourGlass;
  try
   if Project^.SymbolMode=Sym_Project then
    begin;
    if CallFromMenu=true then
     LinkList.RelativeToPath := ExtractFilePath(Project^.FName)
                         else
     FolderForLibImageSymbolsInTree;
    end
    else
     FolderForLibImageSymbolsInTree;

    CreateLinkList;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TLinkDialog.FormDestroy(Sender: TObject);
var
  Cnt: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    for Cnt := 0 to FToDelete.Count - 1 do
      TFileInfo(FToDelete[Cnt]).Free;
    FToDelete.Free;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TLinkDialog.FormCreate(Sender: TObject);
begin
  FItemWidth := 20;
  FToDelete := TWList.Create;
  FChanged := False;
end;

function SortObjectInfo(Item1, Item2: TTreeList): Integer; far;
begin
  Result := AnsiCompareText(TFileTree(Item1).Caption,
    TFileTree(Item2).Caption);
end;

{******************************************************************************+
  Procedure TInfoDialog.CreateLinkList
--------------------------------------------------------------------------------
  Creates a list of all linked files.
+******************************************************************************}

procedure TLinkDialog.CreateLinkList;
var
  AIndex: TIndex;
  Index1: LongInt;
  Index2: LongInt;
  Cnt, CntMain: LongInt;
  AImage: PImage;
  AOleObject: POleClientObject;
  MMEntry: PMMEntry;
  ASymbol: PSGroup;
  a, a1, a2: string;
//  LayerPtr: PLayer;

  procedure FillMyObjects(AItem: PIndex);
  var
    LayerPtr: PLayer;
    AIndex: TIndex;
    AImage: PImage;
    AOleObject: POleClientObject;
    Cnt: Integer;
    Index1: LongInt;
    Index2: LongInt;
    dd: Ansistring;
  begin
    LayerPtr := PLayer(AItem);
    with LayerPtr^ do
    begin
      if SortIndex = 0 then { Select layer not prosessible }
        Exit;
      // search for indixes of images and OLE-objects in the object-list
      AIndex.Index := mi_Image;
      Data^.Search(@AIndex, Index1); { Start index}
      AIndex.Index := mi_OleObj;
      Data^.Search(@AIndex, Index2); { Stop-1 index}
      // add pictures to the list
      for Cnt := Index1 to Index2 - 1 do
      begin
        AImage := PImage(IndexObject(Project.PInfo, Data^.At(Cnt)));
        if Assigned(AImage) then { TView found }
          with AImage^ do
            LinkList.Add(PToStr(AbsolutePath), '',
             ExtractRelativePath(LinkList.RelativeToPath+'\',(PtoStr(AImage.FileName))), '', AImage^.Index, LayerPtr, '');
      end;
      // add OLE-objects to the list
      for Cnt := Index2 to Data^.GetCount - 1 do
      begin
        AOleObject := POleClientObject(IndexObject(Project.PInfo, Data^.At(Cnt)));
        if AOleObject^.GetType = ot_Link then
        begin;
           setstring(dd,AOleObject^.Name,strlen(AOleObject^.Name));
           LinkList.Add(StrPas(AOleObject^.Name), StrPas(AOleObject^.Name),
           ExtractRelativePath(LinkList.RelativeToPath+'\',dd), StrPas(AOleObject^.Name), AOleObject^.Index, LayerPtr, 'OLE');
        end;
      end;
    end;
  end;

  procedure FillMyMM(AItem: PIndex);
  var
    MMEntry: PMMEntry;
    dd: Ansistring;
  begin
    MMEntry := PMMEntry(AItem);
    Inc(Cnt);
    setstring(dd,MMEntry^.FileName,strlen(MMEntry^.FileName));
    LinkList.Add(StrPas(MMEntry^.FileName), '', ExtractRelativePath(LinkList.RelativeToPath+'\',dd),'', 2000000000 + Cnt, nil, intToStr(MMEntry.ID));
  end;

  procedure AddSymbolsPictures;
  var i, cnt1: integer;
    BItem: PIndex;
    HintString: AnsiString;
  begin;
    with Project^ do
      if CallFromMenu = false //only Single symbol view/edit now
        then
      begin;
        for i := 0 to SingleSymbol.Data.Count - 1 do
        begin;
          BItem := SingleSymbol^.Data^.At(i);
          if Bitem.GetObjType = ot_image then begin;
            AImage := PImage(BItem);
            if SingleSymbol.GetLibName = ''
              then
              HintString := SingleSymbol.GetName
            else // %^%  - it is special symbol for detect existing library
              HintString := SingleSymbol.GetName + '%^%' + SingleSymbol.GetLibName;
            if Assigned(AImage) then { TView found }
              LinkList.Add(PToStr(PImage(BItem)^.AbsolutePath), '',
                ExtractRelativePath(LinkList.RelativeToPath+'\', PToStr(PImage(BItem)^.FileName)), '', i + mi_Group, SingleSymbol, HintString);

          end;
        end; //for i

      end

      else

        for Cnt1 := 0 to PLayer(PInfo^.Symbols)^.Data^.GetCount - 1 do
        begin
          ASymbol := PSGroup(PLayer(PInfo^.Symbols)^.Data^.At(Cnt1));
          if ASymbol^.UseCount > 0 then
          begin
            for i := 0 to ASymbol.Data.Count - 1 do
            begin;
              BItem := Asymbol^.Data^.At(i);
              if Bitem.GetObjType = ot_image then begin;
                AImage := PImage(BItem);
                if ASymbol.GetLibName = ''
                  then
                  HintString := ASymbol.GetName
                else
          // %^%  - it is special symbol for detect existing library
                  HintString := ASymbol.GetName + '%^%' + ASymbol.GetLibName;

                if Assigned(AImage) then { TView found }
                  LinkList.Add(PToStr(PImage(BItem)^.AbsolutePath), '',
                    ExtractRelativePath(LinkList.RelativeToPath+'\', PToStr(PImage(BItem)^.FileName)), '', i + mi_Group, ASymbol, HintString);

              end;
            end; //for i
          end; //UseCount>0
        end; //for cnt1
  end;

  procedure AddSPicturesForSymbolProject;
  var i, j, cnt, z: integer;
    LayerPtr: PLayer;
   AItem: Pindex;
    dz:string;
  begin;
    z := -1;
    for i := 1 to Project.Layers.LData.Count - 1 do
    begin;
      LayerPtr := Project.Layers.LData.At(i);
      Cnt := LayerPtr.Data.GetCount - 1;
      for j := 0 to Cnt do
      begin;
      //  AItem := LayerPtr^.Data^.At(j);
         AItem := LayerPtr^.IndexObject(Project.PInfo,LayerPtr^.Data^.At(j));
        if AItem.GetObjType = ot_Image
          then begin;
          AImage:=PImage(AItem);
           if Assigned(AImage) then { TView found }
                  with AImage^ do
   //         LinkList.Add(PToStr(RealFileName{AbsolutePath}), '',
     //         PToStr(LibRelativePathRelativePath), '', AImage^.Index, LayerPtr, '');

       //          LinkList.Add(LinkList.RelativeToPath, '',
       //            ExtractRelativePath(ReProject^.FName, PToStr(PImage(BItem)^.FileName)), '', i + mi_Group, ASymbol, HintString);

            with AImage^ do
              LinkList.Add(PToStr(AImage.FileName), '',
               ExtractRelativePath(LinkList.RelativeToPath+'\',PToStr(AImage.FileName)), '',AItem.Index ,LayerPtr,'');


      //                                AImage^.Index, LayerPtr, '');
        end;

      end; //for j

    end; //for i


  end;


begin
  Screen.Cursor := crHourGlass;
  try
    // prevent list form updating on every change
    LinkList.BeginUpdate;
    LinkList.Clear;
    if Project.SymbolMode = sym_Project then
    begin;
      Project.Layers.LData.ForEach(@FillMyObjects);
      Cnt := -1;
      Project.Multimedia.MediaList.ForEach(@FillMyMM);
      AddSymbolsPictures;
    end;
    if Project.SymbolMode = sym_SymbolMode then
     begin;
      AddSPicturesForSymbolProject;
     end;
    // update the tree-control

  //  if Project^.SymbolMode=sym_SymbolMode
   //  then
    //  begin;
      // LinkList.RelativeCaption:=Format(Mlgsection[34],[Project.ActSymLib]);
  //     FolderForLibImageSymbolsInTree
   //   end;


    LinkList.EndUpdate;
    // update enabled-state of controls
    UpdateEnabled;
  finally
    Screen.Cursor := crDefault;
  end;
end;



procedure TLinkDialog.ViewCheckClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    UpdateRealCounts; { Count all the objects existing in list }

    if Sender = OleObjectsCheck then { Ole clicked }
    begin
      if FOleObjects = 0 then
        Exit;
    end;
    if Sender = MultiMediaCheck then { MMedia clicked }
    begin
      if FMultimedia = 0 then
        Exit;
    end;

    if Sender = ImagesInSymbolsCheck then
    begin;
      if FSymbolsPictures = 0 then
        Exit;
    end;

    { Pictures clicked }
    if Sender = PicturesCheck then
    begin;
      if FPictures = 0 then
        Exit;
    end;

    if Sender = OleObjectsCheck then { Ole clicked }
    begin
      if FOleObjects = 0 then
        Exit;
    end;

    LinkList.UpdateView;
  finally
    Screen.Cursor := crDefault;
  end;
end;

{******************************************************************************+
  Procedure TLinkDialog.UpdateSelectionInfo
--------------------------------------------------------------------------------
  Updates the information about the selected items. Counts the different
  object-types.
  SelectedOnly parameter stands for what to count: if True - only selected
  ones will be counted, else all will do.
+******************************************************************************}

procedure TLinkDialog.UpdateSelectionInfo(SelectedOnly: Boolean);
var
  Cnt: Integer;
  AItem: TFileTree;
  Layer: PLayer;
begin
  with LinkList do
  begin
    FDirectories := 0;
    FFiles := 0;
    FPictures := 0;
    FOLEObjects := 0;
    FMultimedia := 0;
    FSymbolsPictures := 0;
    // step through the list of items
    for Cnt := 0 to Tree.ExpandedCount - 1 do
      if SelectedOnly and (not Selected[Cnt]) then
        Continue { Skip Item }
      else
      begin { Count item to }
        AItem := TFileTree(Tree.ExpandedItems[Cnt]);
        // is the item a directory- or a file-entry?
        if AItem.IsDirectory then
          Inc(FDirectories)
        else
          Inc(FFiles);
        // gather information about the object
        if AItem.FileInfo <> nil then
        begin
          if AItem.FileInfo.Reference < mi_OLEObj then
          begin
{//++Sygsky to avoid fixed layer objects to be selected
            // First check that owner layer is not fixed
            Layer := AItem.FileInfo.SomePtr;
            if Assigned(Layer) then
              if Layer.GetState(sf_Fixed) then // Layer is fixed, not select object!
              begin
                LinkList.Selected[Cnt] := False;
                Continue; // Bump to the next item...
              end;
//--Sygsky}
           //brovak
            if AItem.FileInfo.Reference < mi_Image then
              Inc(FSymbolsPictures)
            else
              Inc(FPictures);
            //brovak
          end
          else
            if AItem.FileInfo.Reference < 2000000000 then
              Inc(FOLEObjects)
            else
              Inc(FMultimedia);
        end;
      end;
  end;
end;

{******************************************************************************+
  Procedure TLinkDialog.UpdateEnabled
--------------------------------------------------------------------------------
  Updates the enabled-state of the buttons and menu-entries. Calls
  UpdateSelectionInfo to update the selection-info.
+******************************************************************************}

procedure TLinkDialog.UpdateEnabled;
var
  AEnable: Boolean;
begin
  with LinkList do
  begin
    // update the information about selected objects
    UpdateSelectionInfo;
    // change source and delete allowed if only files or only one directory
    // selected
    AEnable := (Tree.AllCount > 0) and (SelCount > 0) and ((FDirectories = 0) or (FFiles = 0));
    ChangeSourceBtn.Enabled := AEnable;
    DeleteBtn.Enabled := AEnable;
    // embedding allowed if only OLE-objects are selected
    EmbeddBtn.Enabled := (FPictures = 0) and (FOLEObjects > 0) and (FMultimedia = 0) and (FSymbolsPictures = 0);
  end;
end;

{******************************************************************************+
  Procedure TLinkDialog.LinkListClick
--------------------------------------------------------------------------------
  Called if the user clicks on the files-list. Selection-state may have changed,
  update enabled-state of controls.
+******************************************************************************}

procedure TLinkDialog.LinkListClick(Sender: TObject);
begin
  UpdateEnabled;
end;

{******************************************************************************+
  Function TLinkDialog.LinkListFilterItems
--------------------------------------------------------------------------------
  Filter-function for the files-tree. Used to hide or show items depending
  on the show-objects-flags.
+******************************************************************************}

function TLinkDialog.LinkListFilterItems(AInfo: TFileInfo): Boolean;
begin
  if Ainfo.Reference < mi_ChartKind then
    Result := ImagesInSymbolsCheck.Checked
  else
    if AInfo.Reference < mi_OLEObj then
      Result := PicturesCheck.Checked
    else
      if AInfo.Reference < 2000000000 then
        Result := OLEObjectsCheck.Checked
      else
        Result := MultimediaCheck.Checked;
end;

{******************************************************************************+
  Procedure TLinkDialog.DeleteBtnClick
--------------------------------------------------------------------------------
  Called if the user wants to delete the selected items.
+******************************************************************************}

procedure TLinkDialog.DeleteBtnClick(Sender: TObject);
var
  MsgNumber: Integer;
  Cnt: Integer;
  procedure CopyTree(ATree: TFileTree);
  var
    Cnt: Integer;
    Layer: PLayer;
  begin
    if ATree.IsDirectory then
      for Cnt := 0 to ATree.Count - 1 do
        CopyTree(TFileTree(ATree.Items[Cnt]))
    else
    begin
//++Sygsky: avoid fixed layer obj to be added to the delete list
      Layer := PLayer(ATree.FileInfo.SomePtr);
      if Assigned(Layer) then
        if Layer.GetState(sf_Fixed) then // not count fixed layer object!
          Exit;
//--Sygsky
      FToDelete.Add(ATree.FileInfo); // Move object to the executing list
      LinkList.Files.Remove(ATree.FileInfo);
    end;
  end;
begin
  // update selection-information
  UpdateSelectionInfo;
  // determine right message to display for the user
  if FFiles = 1 then
    MsgNumber := 11
  else
    if FFiles > 1 then
      MsgNumber := 12
    else
      if FDirectories = 1 then
        MsgNumber := 13
      else
        MsgNumber := 14;
  // ask if user really wants to delete the selected files
  if MessageDialog(MlgSection[MsgNumber], mtConfirmation, [mbYes, mbNo, mbCancel], 0) = mrYes then
  begin
    // prevent list form updating control at every change
    LinkList.BeginUpdate;
    // delete all selected files from the tree and copy them to the
    // deleted-list
    for Cnt := 0 to LinkList.Tree.ExpandedCount - 1 do
      if LinkList.Selected[Cnt] then
        CopyTree(TFileTree(LinkList.Tree.ExpandedItems[Cnt]));
    LinkList.EndUpdate;
  end;
end;

function SortReference(Item1, Item2: Pointer): Integer; far;
begin
  if TFileInfo(Item1).Reference < TFileInfo(Item2).Reference then
    Result := -1
  else
    if TFileInfo(Item1).Reference > TFileInfo(Item2).Reference then
      Result := 1
    else
      Result := 0;

end;

{******************************************************************************+
  Procedure TLinkDialog.FormHide
--------------------------------------------------------------------------------
  Called if the form is hidden. If the modal-result is mrOK the changes made
  are processed.
+******************************************************************************}

procedure TLinkDialog.FormHide(Sender: TObject);
var
  AItem: TFileInfo;
  Cnt: Integer;
//  NeedRedraw: Boolean;
  ObjectsToDelete: TIntList;
  AIndex: TIndex;
  PInd: PIndex;
  AObject: PView;
  Status: Word;
  Changed: Boolean;
  DoRedraw: Boolean;
  AlreadyDeselected: Boolean;
  NewShows: TShows;
  MMEntry: PMMEntry;
  ASymbol: PSGroup;
  AImage: PImage;
  SymbolWasChanged: boolean;
  { Auto function}
  procedure DelRef(P: Pointer);
  begin
    Project.Layers.DeleteIndex(Project.PInfo, PIndex(P));
  end;

begin
//  Changed := FALSE;
  DoRedraw := FALSE;
  SymbolWasChanged := false;
  if ModalResult = mrOK then
    with Project^ do
    begin

//+++++++++++++++++++++++++++++++++++++++++++++++ Delete marked images

      if FToDelete.Count > 0 then
      begin
      // sort the list of deleted items by the reference-index
        FToDelete.Sort(SortReference);
        ObjectsToDelete := TIntList.Create;
        Screen.Cursor := crHourGlass;
        try
      // step through the list of items to delete
          AlreadyDeselected := False; // Mark deselection function not called
          for Cnt := FToDelete.Count - 1 downto 0 do
          begin
            AItem := FToDelete[Cnt];
        // delete multimedia-links
            if AItem.Reference >= 2000000000 then { Media attachment }
              Multimedia^.MediaList^.AtFree(AItem.Reference - 2000000000)
            else
            //images  from symbols
              if AItem.Reference <= mi_ChartKind then
              begin;
                PSGroup(Aitem.SomePtr)^.Data.AtFree(AItem.Reference - mi_Group);

                if pos('%^%', AItem.Comment) > 0 then
                  UpdateSymInfoForExtend(PSGroup(Aitem.SomePtr));


//              PSGroup(Aitem.SomePtr)^.Data.AtFree(AItem.Reference-mi_Group);
                FChanged := True;
                SymbolWasChanged := true;
              end

              else
            // delete images
                if InRange(mi_Image - 1, AItem.Reference, mi_OLEObj, False) then { Image! }
                begin
                  if Assigned(AItem.SomePtr) then // Owner layer is stored
                    with PLayer(AItem.SomePtr)^ do
                    begin
                      PInd := HasIndexObject(AItem.Reference); { Find onject on the layer }
                      if Assigned(PInd) then { Found, simply select it! }
                        if not AlreadyDeSelected then
                        begin
                    { Deselect all objects now as it is 1st object to select for deleting
                      them later}
                          Project.DeSelectAll(True);
                          AlreadyDeSelected := True; // Mark the action
                        end;
                      Select(Project.PInfo, PInd);
                      FChanged := True;
                    end;
                end
                else
                  if InRange(mi_OLEObj, AItem.Reference, MaxInt, True) then { OLEObject! }
                  begin
              { For you, future and power programmer of WinGIS, I leave it here for your FUN :}
                  end;
          end;
{$IFNDEF WMLT}
          Project.DeleteSelected(FALSE);
{$ENDIF}
//        Project^.DeleteObjectList(ObjectsToDelete); { for future generation of WinGIS programmers :}
        finally
          ObjectsToDelete.Free;
          Screen.Cursor := crDefault;
          if DoRedraw then Project^.PInfo^.RedrawScreen(FALSE);
        end;
      end;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++ update changed paths
      for Cnt := 0 to LinkList.Tree.AllCount - 1 do
      begin
        AItem := TFileTree(LinkList.Tree.AllItems[Cnt]).FileInfo;
        if (AItem <> nil) {and (AItem.Reference < 2000000000)} then
        begin
          AIndex.Init(AItem.Reference);
          AObject := Project^.Layers^.IndexObject(Project^.PInfo, @AIndex);

          if AObject = nil
            then
          begin;
            if AItem.Reference >= 2000000000 //650000000
              then
            begin;
                // MyIndex:=AItem.Reference-2000000000;
              MMEntry := PMMEntry(Multimedia^.MediaList^.At(AItem.Reference - 2000000000));
              with MMEntry^ do
              begin;
                if (AnsiCompareText(Filename, AItem.LongName) <> 0)
                  or (AnsiCompareText(RelativeFileName, AItem.RelativeLongName) <> 0)
                  then begin;
                  StrPCopy(FileName, AItem.LongName);
          //              AssignStr(AbsolutePath, AItem.LongName);
                  StrPCopy(RelativeFileName, AItem.RelativeLongName);
                end; //if
              end; //with
            end //Attache
            else begin; //Image in symbols
              ASymbol := PSGroup(AItem.SomePtr);
              AImage := Pimage(PSGroup(AItem.SomePtr)^.Data.At(AItem.Reference - mi_Group));
              with AImage^ do begin;
                if (AnsiCompareText(PToStr(AbsolutePath), AItem.LongName) <> 0)
                  or (AnsiCompareText(PToStr(RelativePath), AItem.RelativeLongName) <> 0) then
                begin
                  Unload;
                  AssignStr(FileName, AItem.LongName);
                  AssignStr(AbsolutePath, AItem.LongName);
                  AssignStr(RelativePath, AItem.RelativeLongName);
                  FChanged := true;
                  SymbolWasChanged := true;
                end;
              end;

              if pos('%^%', AItem.Comment) > 0 then
                UpdateSymInfoForExtend(PSGroup(Aitem.SomePtr));
            end; //Image in symbol
          end //Aobject=nil
          else
//for image
            if AObject^.GetObjType = ot_Image then
              with PImage(AObject)^ do
                if (AnsiCompareText(PToStr(AbsolutePath), AItem.LongName) <> 0)
                  or (AnsiCompareText(PToStr(RelativePath), AItem.RelativeLongName) <> 0) then
                begin
                  Unload;
                  AssignStr(FileName, AItem.LongName);
                  AssignStr(AbsolutePath, AItem.LongName);
                  AssignStr(RelativePath, AItem.RelativeLongName);
{++ Ivanoff BUG#441 BUILD#95
After change of file path, the image was reloaded only after reloading of the project.
Now to see the image it is necessary click 'Redraw' button.}
                  FChanged := TRUE;
                  Loaded := st_Error;
                  InitFileEditLinks;
                  if (Loaded and st_Delete) = 0 then
                  begin
                    if (Loaded and st_NoLoad) = 0 then
                    begin
                      OpenInfo(Status);
                      if (Status <> err_OK) then
                      begin
                        if (Status = err_WrongFormat) then
                          Loaded := st_WrongType
                        else
                          if (Status = err_ReadErr) then Loaded := st_ReadErr;
                      end
                      else
                      begin
                        Loaded := UINT(Loaded) or st_AllOK;
//                        LoadImage(Project^.PInfo, bm_Display);
                        DoRedraw := TRUE;
                      end;
                    end;
                  end;
{-- Ivanoff}
                end;
        end;
      end;
//   if  UpdateSymbols= true then
  // begin;

  //   DoRedraw:=true;
  // end;
//++++++++++++++++++++++++++++++++++++++++++++++++++++++ Change options setting

      NewShows := [];
      if LinkList.RelativePaths then
        NewShows := NewShows + [RelPathShow];
      if PicturesCheck.Checked then
        NewShows := NewShows + [PicShow];
      if OLEObjectsCheck.Checked then
        NewShows := NewShows + [OLEShow];
      if MultimediaCheck.Checked then
        NewShows := NewShows + [MMShow];
//      if NewShows <> StoredShows then // Something changed
      // write dialog-settings to the registry
        with FProject^.Registry do
        begin
          CurrentPath := '\User\WinGISUI\LinkDialog';
          WriteBool('ShowRelativePaths', LinkList.RelativePaths);
          if CallFromMenu = true then begin;
            WriteBool('ShowPictures', PicturesCheck.Checked);
            WriteBool('ShowOLEObjects', OLEObjectsCheck.Checked);
            WriteBool('ShowMultimedia', MultimediaCheck.Checked);
            WriteBool('ShowSymbolsImages',ImagesInSymbolsCheck.Checked);
          end;
          FChanged := True;
        end;

//+++++++++++++++++++++++++++++++ Final changes check point

      if FChanged then Project^.SetModified;
    end;
  if SymbolWasChanged = true then UpdateSymLibInfo;
end;

procedure TLinkDialog.ChangeSourceBtnClick(Sender: TObject);
  {****************************************************************************+
    ChangePathNames
  ------------------------------------------------------------------------------
    Replaces the path-names of all sub-entries of Tree with the newly selected
    path-name. The function is recursively called for all sub-directories of
    the tree.
  +****************************************************************************}
  procedure ChangePathNames(Replace: Integer; const ReplaceBy: string; Tree: TFileTree);
  var
    Cnt: Integer;
    TreeItem: TFileTree;
  begin
    for Cnt := 0 to Tree.Count - 1 do
    begin
      TreeItem := TFileTree(Tree[Cnt]);
      if TreeItem.IsDirectory then
        ChangePathNames(Replace, ReplaceBy, TreeItem)
      else
      begin
{        TreeItem.FileInfo.LongName := ReplaceBy + Copy(TreeItem.FileInfo.LongName,
          Replace + 1, Length(TreeItem.FileInfo.LongName) - Replace);}
        TreeItem.FileInfo.LongName := ReplaceBy + ExtractFileName(TreeItem.FileInfo.LongName);
        TreeItem.FileInfo.RelativeLongName := ExtractRelativePath(LinkList.RelativeToPath + '\',
          TreeItem.FileInfo.LongName);
      end;
    end;
  end;
var
  LinkItem: TFileTree;
  FileInfo: TFileInfo;
  ReplaceWith: string;
  ReplaceLength: Integer;
begin
  with LinkList do
    if SelCount = 1 then
    begin
      LinkItem := TFileTree(Tree.ExpandedItems[ItemIndex]);
      if LinkItem.IsDirectory then
      begin
        ReplaceWith := PathOfTree(LinkItem);
        DirectorySelectDialog.FileName := '';
        DirectorySelectDialog.InitialDir := ReplaceWith;
        if DirectorySelectDialog.Execute then
        begin
          BeginUpdate;
          ReplaceLength := Length(ReplaceWith);
          if not IsSuffix('\', ReplaceWith, False) then Inc(ReplaceLength);
          ReplaceWith := DirectorySelectDialog.FileName;
          if not IsSuffix('\', ReplaceWith, False) then ReplaceWith := ReplaceWith + '\';
          ChangePathNames(ReplaceLength, ReplaceWith, LinkItem);
        // update the list-view
          FChanged := True; // Mark changes
          EndUpdate;
        end;
      end
      else
      begin
        FileInfo := LinkItem.FileInfo;
        OpenDialog.FileName := FileInfo.RelativeLongName {Name(FALSE,FALSE)};
//      OpenDialog.InitialDir:=LastDirOpened;
        if OpenDialog.Execute then
        begin
          BeginUpdate;
          FileInfo.LongName := OpenDialog.FileName;
          FileInfo.RelativeLongName := ExtractRelativePath(RelativeToPath + '\',
            OpenDialog.FileName);
          FChanged := True; // mark changes
        // update the list-view
          EndUpdate;
        // select the updated item
          LinkItem := FileInfoToTree(FileInfo);
          ItemIndex := Tree.ExpandedIndexOf(LinkItem);
          OpenDialog.InitialDir := ExtractFilePath(FIleInfo.LongName);
        end;
      end;
    end;
end;

{******************************************************************************+
  Procedure TLinkDialog.AbsolutePathsMenuClick
--------------------------------------------------------------------------------
  Called by the absolute-paths-menu. Switches file-list to the absolute-path-
  mode.
+******************************************************************************}

procedure TLinkDialog.AbsolutePathsMenuClick(Sender: TObject);
begin
  LinkList.RelativePaths := FALSE;
  AbsolutePathsMenu.Checked := TRUE;
end;

{******************************************************************************+
  Procedure TLinkDialog.RelativePathsMenuClick
--------------------------------------------------------------------------------
  Called by the relative-paths-menu. Switches file-list to the relative-path-
  mode.
+******************************************************************************}

procedure TLinkDialog.RelativePathsMenuClick(Sender: TObject);
begin
  LinkList.RelativePaths := TRUE;
  RelativePathsMenu.Checked := TRUE;
end;

procedure TLinkDialog.UpdateRealCounts;
var
  AIndex: TIndex;
  Index1: LongInt;
  Index2: LongInt;
  Cnt, i, j: LongInt;
  AOleObject: POleClientObject;
  ASymbol: PSGroup;
  LayerPtr : Player;
  AItem : Pindex;
 begin


if Project^.SymbolMode=sym_SymbolMode then
     begin;
     FPictures:=0;
       for i := 0 to Project.Layers.LData.Count - 1 do
         begin;
           LayerPtr := Project.Layers.LData.At(i);
           Cnt := LayerPtr.Data.GetCount - 1;
             for j := 0 to Cnt do
               begin;
                AItem := LayerPtr.Data.At(j);
                if AItem.GetObjType = ot_Image
                  then Inc(FPictures);
               end;
         end;
        Exit;
     end;

  with PLayer(Project^.PInfo^.Objects)^ do
  begin
    // search for indexes of images and OLE-objects in the object-list
    AIndex.Index := mi_Image;
    Data^.Search(@AIndex, Index1);
    AIndex.Index := mi_OleObj;
    Data^.Search(@AIndex, Index2);
    // Count pictures to the list
    FPictures := Index2 - Index1; { Numberof pictures with links }
      // add OLE-objects to the list
    FOLEObjects := 0;
    for Cnt := Index2 to Data^.GetCount - 1 do
    begin
      { Note that OLE objects are last objects in WinGIS array on index base}
      AOleObject := POleClientObject(Data^.At(Cnt));
      if AOleObject^.GetType = ot_Link then
        Inc(FOLEObjects);
    end;
  end;
  // Count multimedia-files in the list
  FMultimedia := 0;
  if (Project.Multimedia <> nil) and (Project.Multimedia.MediaList <> nil) then
    FMultimedia := Project.Multimedia.MediaList.Count;
//                  Project^.Pinfo.Symbols.

  FSymbolsPictures := 0;
  with Project^ do
    for i := 0 to PLayer(PInfo^.Symbols)^.Data^.GetCount - 1 do
    begin;
      ASymbol := PSGroup(PLayer(PInfo^.Symbols)^.Data^.At(i));
      if ASymbol^.UseCount > 0 then
      begin
        for j := 0 to ASymbol.Data.Count - 1 do
        begin;
          if PIndex(Asymbol^.Data^.At(j)).GetObjType = ot_image
            then
            Inc(FSymbolsPictures);
        end;
      end;
    end;


end;

procedure TLinkDialog.LinkListMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var
  i, iSection: Integer;
  Layer: PLayer;
  APos: TPoint;
  AItem: TFileTree;
  LName: AnsiString;
  iLType: Integer;
  AdvancedType: boolean;
  APosition: integer;
  Left, Right: AnsiString;
begin
//  AdvancedType:=false;
  APos.X := X;
  APos.Y := Y;
  i := LinkList.ItemAtPos(APos, True);
  if i >= 0 then
  begin
    LinkList.Hint := EmptyStr;
    AItem := TFileTree(LinkList.Tree.ExpandedItems[i]);
    // is the item a directory- or a file-entry?
    iSection := MlgStringList.Sections[MlgSection.Section];
    if AItem.IsDirectory then // Directory, out of score
    begin
      LinkList.Hint := SmartString(MlgStringList.SectionStrings[iSection, 22], []);
      Exit;
    end
    else
    begin;
      Layer := PLayer(AItem.FIleInfo.SomePtr); // Layer is preset
      if Assigned(AItem.FileInfo) then // File Info defined
        if Layer = nil
          then
        begin

          if AItem.FileInfo.Reference >= 2000000000 //Attache
            then
          begin;
            LinkList.Hint := MlgSection[32] + AItem.FIleInfo.Comment;
            Exit;

          end;
        end //nil
        else
        begin;
          if AItem.FileInfo.Reference < mi_Image //Image in Symbol
            then
          begin
            LinkList.Hint := MlgSection[29] + ' "';
            APosition := pos('%^%', AItem.FIleInfo.Comment);

            if Aposition = 0 //symbol  from Project Lybrary
              then
              LinkList.Hint := LinkList.Hint + AItem.FIleInfo.Comment + '" ' + Mlgsection[33] + ' ' + MlgSection[31]
            else
            begin;
              Left := Copy(AItem.FIleInfo.Comment, 1, Aposition - 1);
              Right := Copy(AItem.FIleInfo.Comment, length(Left) + 4, Length(AItem.FIleInfo.Comment) - Length(Left) - 3);
              LinkList.Hint := LinkList.Hint + Left + '" ' + Mlgsection[33] + ' "' + Right + '" ' + MlgSection[30];
            end;
            Exit;
          end;

          if Assigned(Layer) then
          begin
        //  if Layer^.Text<>nil then
            LName := Layer^.Text^;
          //  else
            //begin;
            {It Is Attache or Symbol's Image}
//           if AItem.FileInfo.Reference>=2000000000
  //          then
    //          LName:=AItem.FIleInfo.Comment;
  //          AdvancedType:=true;
      //      end;
//            AItem.

            if Layer.GetState(sf_Fixed) then // Lie on fixed layer
              iLType := 23
            else
              if Layer.GetState(sf_LayerOff) then // Lie on fixed layer
                iLType := 24
              else
                if Layer.GetState(sf_LayerOffAtGen) then // Lie on fixed layer
                  iLType := 25
                else
                  iLType := 27;
          // Lie on normal layer
        //  if AdvancedType=false then
            LinkList.Hint := SmartString(MlgStringList.SectionStrings[iSection, 26],
              [MlgStringList.SectionStrings[iSection, iLType],
              LName]);
            Exit;
          end;
        end;
    end;
  end;
  LinkList.Hint := EmptyStr;
end;



//used modified procedure from IDB_MAN by Ivanoff

procedure TLinkDialog.UpdateSymLibInfo;
var
  iI: Integer;
  RollUp: TRollupForm;
  Tmp: integer;
  APExtLib: PExtLib;
  i: integer;
begin
        // Find RollupForm with symbols.
  for iI := 0 to UserInterface.RollupCount - 1 do begin
    Rollup := UserInterface.Rollups[iI];

    TSymbolRollupForm(RollUp).UpdateSymbols;

    if TSymbolRollupForm(RollUp).Visible = true
      then begin;
      TSymbolRollupForm(RollUp).Invalidate;
      TSymbolRollupForm(RollUp).SymbolList.Repaint;
      TSymbolRollupForm(RollUp).SymbolList.InvalidateItems(0,TSymbolRollupForm(RollUp).SymbolList.Count-1);
      EXIT;
    end;

  end; // For iI end
end;


procedure TLinkDialog.UpdateSymInfoForExtend(CurrentSymbol: PSGroup);
var
  iI: Integer;
  RollUp: TRollupForm;
  Tmp: integer;
  APExtLib: PExtLib;
  i: integer;
  ProjSym: PSGroup;
  ExtLib: PExtLib;
begin

        // Find RollupForm with symbols.
  for iI := 0 to UserInterface.RollupCount - 1 do begin
    Rollup := UserInterface.Rollups[iI];

    if Rollup is TSymbolRollupForm then begin
      ExtLib := ExtLibAlreadyOpened(CurrentSymbol^.GetLibName);
      ProJSym := ExtLib.GetSymbol(NewStr(CurrentSymbol^.GetName));
      if ProjSym <> nil then begin;
        ProjSym.Data.DeleteAll;
        ProJSym.Data := MakeObjectCopy(CurrentSymbol^.Data);
        ExtLib.SetModified(true);

      end; // <>nil
      Exit;
    end; //rollup
  end; //for

end;


procedure TLinkDialog.FolderForLibImageSymbolsInTree;
var
  iI: Integer;
  RollUp: TRollupForm;
  Tmp: integer;
  APExtLib: PExtLib;
  i: integer;
  ProjSym: PSGroup;
  ExtLib: PExtLib;
  ProjTmp:PProj;
begin
 Rollup:= TSymbolRollupForm(UserInterface.RollupNamed['SymbolRollupForm']);
 ProJSym := TSymbolRollupForm(RollUp).FSymInfo.GetSym(TSymbolRollupForm(RollUp).FSymInfo.SymIndex);
 if TSymEditor(Project^.SymEditInfo.SymEditor)<>nil then
 ProjTmp:=TSymEditor(Project^.SymEditInfo.SymEditor).BossProject
 else
 ProjTmp:=Project;
 if ProjSym.GetLibName=''
 then
  begin;
   LibRelativePath:=ExtractRelativePath(ProjTmp.FName{LibAbsolutePath},ExtractFilePath(ProjTmp.FName));
   LinkList.RelativeToPath:=ExtractFilepath(ProjTmp.FName);
   LinkList.RelativeCaption:=Format(MlgSection[35],[LinkList.RelativeToPath]);
  end
 else
  begin;
  ExtLib:=ExtLibAlreadyOpened(ProjSym.GetLibName);
  LibRelativePath:=ExtractRelativePath(PToStr(ExtLib.LibFileName),ExtractFilePath(PToStr(ExtLib.LibFileName))) ;
  LinkList.RelativeToPath:=ExtractFilepath(PtoStr(ExtLib.LibFileName));
  LinkList.RelativeCaption:=Format(MlgSection[34],[LinkList.RelativeToPath]);
  end;


end;
{$ENDIF} // <----------------- AXDLL

end.

