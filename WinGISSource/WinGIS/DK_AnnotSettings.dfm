�
 TDK_ANNOTSETTINGSFORM 00  TPF0TDK_AnnotSettingsFormDK_AnnotSettingsFormLeftTop� BorderStylebsDialogCaption!430ClientHeight|ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoDesktopCenterScaledOnCloseQueryFormCloseQueryOnCreate
FormCreateOnHideFormHideOnShowFormShowPixelsPerInch`
TextHeight TPanelTopPanelLeft Top Width�Height=AlignalTop
BevelOuterbvNoneTabOrder  TLabelLabel1Left>TopWidth� HeightAutoSizeCaption!431Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  	TComboBoxLayerNamesComboBoxLeft� TopWidth� HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ItemHeight
ParentFontTabOrder    TPanelBottomPanelLeft Top\Width�Height AlignalBottom
BevelOuterbvNoneTabOrder TButtonOKButtonLeftQTopWidthKHeightAnchorsakRightakBottom Caption!0Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style ModalResult
ParentFontTabOrder   TButtonCancelButtonLeft�TopWidthKHeightAnchorsakRightakBottom Caption!1Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style ModalResult
ParentFontTabOrder   TPageControlPageControl1Left Top=Width�Height
ActivePage	TabSheet1AlignalClientFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder 	TTabSheet	TabSheet1Caption!6000Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont TSpeedButtonUpSBLeft
Top� WidthHeightFlat	
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 333 333333��333333	333333��333333	333333��333333	333333��333333	333333��333333	33333�����330 	  3338�����330����3338?3383333	��3333�33�3333	��3333��3�33330��333338�8�33330��333338?8333333	333333��333333	333333��333333033333338�3333330333333383333	NumGlyphsOnClick	UpSBClick  TSpeedButtonDownSBLeft(Top� WidthHeightFlat	
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 333033333338�333333033333338?333333	333333��333333	333333���33330��333338�8�33330��33333838?3333	��3333�33�3333	��3333�33��330����3338��?��330 	  3338����33333	333333��333333	333333��333333	333333��333333	333333��333333	333333��333333 333333��333	NumGlyphsOnClick	UpSBClick  	TGroupBoxFieldNamesGBLeftTop#Width� Height� Caption!6003Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TCheckListBoxFieldNamesListBoxLeftTopWidth� Height� AlignalClient
ItemHeightTabOrder OnClickFieldNamesListBoxClickOnEnterFieldNamesListBoxClick   	TGroupBoxDataGBLeftTop#Width� Height� Caption!6005Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder TMemoDataMemoLeftTopWidth� Height� AlignalClientFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont
ScrollBarsssBothTabOrder OnChangeDataMemoChange  TStringGridPrePostGridLeftTopWidth� Height� BorderStylebsNoneColCountDefaultRowHeight	FixedCols 	FixedRows OptionsgoFixedVertLinegoFixedHorzLine
goVertLine
goHorzLine	goEditinggoTabs 
ScrollBars
ssVerticalTabOrderOnClickPrePostGridClickOnEnterPrePostGridClick	ColWidthsmc    TRadioButtonRadioButton1LeftTop
Width� HeightCaption!6001Checked	Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderTabStop	OnClickRadioButton1Click  TRadioButtonRadioButton2LeftTop
Width� HeightCaption!6002Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderOnClickRadioButton2Click   	TTabSheet	TabSheet2Caption	TabSheet2Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ImageIndex
ParentFont  	TTabSheet	TabSheet3CaptionAdvanced
ImageIndex TLabelLabel2LeftTop*Width� HeightAutoSizeCaption!6102  	TCheckBoxOffSetAdvCheckBoxLeftTop&WidthqHeightCaption!6111TabOrder OnClickOffSetAdvCheckBoxClick  	TComboBoxPolyLineAdvDirectionComboBoxLeftTopFWidth� HeightStylecsDropDownList
ItemHeightItems.Strings!6104!6105!6106 TabOrder  	TCheckBoxArcAdvCheckBoxLeftToprWidth� HeightCaption!6107TabOrderOnClickArcAdvCheckBoxClick  	TComboBoxArcAdvDirectionComboBoxLeftTop� Width� HeightStylecsDropDownList
ItemHeightItems.Strings!6108!6109!6110 TabOrder  
TWGroupBox
WGroupBox1LeftTopWidth� HeightCaption!6100TabOrderBoxStyle	bsTopLine  
TWGroupBox
WGroupBox2LeftTopWidth� HeightCaption!6101TabOrderBoxStyle	bsTopLine  TWindowWindow2Left
TopHWidth� HeightOnMouseDownWindow2MouseDownOnMouseMoveWindow2MouseMove	OnMouseUpWindow2MouseUpOnPaintWindow2Paint    TMlgSectionMlgSection1SectionMainLeftETop   