Unit SelectHndl;

Interface

Uses Classes,Controls,MenuFn,ProjHndl;

Type TSelectionHandler = Class(TProjMenuHandler)
       Class Function HandlerType:Word; override;
       Procedure   OnDeActivate; override;
       Procedure   OnStart; override;
     end;

     TCustomSelectHandler = Class(TProjMenuHandler)
      Protected
       Function    GetStatusText:String; override;
      Public
       Class Function HandlerType:Word; override;
       Procedure   OnActivate; override;
       Procedure   OnDeActivate; override;
       Procedure   OnKeyDown(var Key:Word;Shift:TShiftState); override;
       Procedure   OnStart; override;
     end;

     TSelectByRectangleHandler = Class(TCustomSelectHandler)
      Private
       Procedure   DoFinishedInput(Sender:TObject);
       Procedure   DoStartInput(Sender:TObject);
      Public
       Class Function HandlerType:Word; override;
       Procedure   OnStart; override;
     end;

     TSelectByCircleHandler = Class(TCustomSelectHandler)
      Private
       Procedure   DoFinishedInput(Sender:TObject);
       Procedure   DoStartInput(Sender:TObject);
      Public
       Procedure   OnStart; override;
       Procedure   OnActivate; override;
     end;

     TSelectByPolygonHandler = Class(TCustomSelectHandler)
      Private
       Procedure   DoFinishedInput(Sender:TObject);
       Procedure   DoStartInput(Sender:TObject);
      Public
       Procedure   OnStart; override;
       Procedure   OnActivate; override;
     end;

     TDeselectAllHandler = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;

     TSelectAdditiveHandler = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;

     TSelectXOrHandler = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;

     TSelectSubtractiveHandler = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;

     TSelectInsideHandler = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;

     TSelectIntersectingHandler = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;

{++ IDB}
     TUndoHandler = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;

     TRedoHandler = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;
{-- IDB}

Implementation

{$R *.mfn}

Uses AM_CPoly
     {$IFNDEF WMLT}
     ,AM_DDE
     {$ENDIF}
     ,AM_Def,AM_Index,AM_Ini,AM_Paint,AM_ProjO,AM_ProjP,GrTools,
     InpHndl,MenuHndl,RegDb,Windows
{++ IDB}
    , Am_Main
{-- IDB}
     ;

{==============================================================================+
  TSelectionHandler
+==============================================================================}

Class Function TSelectionHandler.HandlerType:Word;
begin
  Result:=mhtReturnAndReset;
end;
 
procedure TSelectionHandler.OnDeActivate;
var CurrentFn    : String;
begin
  CurrentFn:=Project.Registry.ReadString('\User\WinGISUI\DrawingTools\Selection\InputType');
  if Project.Registry.LastError<>rdbeNoError then CurrentFn:='SelectByRectangle';
  MenuFunctions[CurrentFn].Checked:=FALSE;
end;

Procedure TSelectionHandler.OnStart;
var CurrentFn    : String;
begin
  with Project^ do begin
    // determine the function that was previously selected
    CurrentFn:=Registry.ReadString('\User\WinGISUI\DrawingTools\Selection\InputType');
    if Registry.LastError<>rdbeNoError then CurrentFn:='SelectByRectangle';
    MenuFunctions[CurrentFn].Execute;
    // read the previously selected mode from the project-registry
    PInfo^.SelectionMode:=TSelectionMode(
        Registry.ReadInteger('\User\WinGISUI\DrawingTools\Selection\Mode'));
    if Registry.LastError<>rdbeNoError then PInfo^.SelectionMode:=smAdditive;
    // check the right menu-function
    case PInfo^.SelectionMode of
      smAdditive   : MenuFunctions['SelectAdditive'].Execute;
      smSubtractive: MenuFunctions['SelectSubtractive'].Execute;
      smXOr        : MenuFunctions['SelectXOr'].Execute;
    end;
    // read the previously selected inside/intersecting mode from the project-registry
    PInfo^.SelectInside:=not Registry.ReadBool('\User\WinGISUI\DrawingTools\Selection\Intersecting');
    if Registry.LastError<>rdbeNoError then PInfo^.SelectInside:=TRUE;
    // check the right menu-function
    if PInfo^.SelectInside then MenuFunctions['SelectInside'].Execute
    else MenuFunctions['SelectIntersecting'].Execute;
    PopupMenu:='ProjectWindowPopup';
    Visible:=TRUE;
    //brovak for move/resize/rotate
    if OldMenu=-1000 then Pinfo.EditPoly:=false;
    SetActualMenu(mn_Select);
  end;
end;

{==============================================================================+
  TCustomSelectHandler
+==============================================================================}

Class Function TCustomSelectHandler.HandlerType:Word;
begin
  Result:=mhtReturnAndReset;
end;

Procedure TCustomSelectHandler.OnStart;
begin
  // store the current input-type in the registry
  Project^.Registry.WriteString('\User\WinGISUI\DrawingTools\Selection\InputType',
      MenuName);
  PopupMenu:='ProjectWindowPopup';
  Status:=mhsNotFinished;
end;

Function TCustomSelectHandler.GetStatusText:String;
begin
  Result:=Project^.Layers^.SelLayer^.SelInfo.StatusText;
end;

Procedure TCustomSelectHandler.OnActivate;
begin
  MenuFunctions['EditSelect'].Checked:=TRUE;
end;

Procedure TCustomSelectHandler.OnDeActivate;
begin
  MenuFunctions['EditSelect'].Checked:=FALSE;
end;

Procedure TCustomSelectHandler.OnKeyDown(var Key:Word;Shift:TShiftState);
begin
  if Key=vk_Escape then OnUndo(0);
end;

{==============================================================================+
  TSelectByRectangleHandler
+==============================================================================}

Procedure TSelectByRectangleHandler.OnStart;
begin
  inherited OnStart;
{!?!?!
  SubHandler:=TRubberBox.Create(Self);
  with SubHandler as TRubberBox do begin
    PositiveDimensions:=TRUE;
    OnStartInput:=DoStartInput;
    OnFinishedInput:=DoFinishedInput;
  end;
  Visible:=TRUE;}
  Project^.SetActualMenu(mn_Select);
  Status:=mhsReturnToPrevious;
end;

Procedure TSelectByRectangleHandler.DoStartInput(Sender:TObject);
begin
{!?!?!  with Project^,SubHandler as TRubberBox do begin
    ViewRotation:=PInfo^.ViewRotation;
    MinSize:=10;
  end;}
end;

Procedure TSelectByRectangleHandler.DoFinishedInput(Sender:TObject);
var SelectRect        : TDRect;
    Point             : TDPoint;
    SnapObject        : PIndex;
    DrawPos           : TDPoint;
    Corners           : TGrRectCorners;
    Poly              : PCPoly;
    Cnt               : Integer;
begin
{!?!?!  with SubHandler as TRubberBox,Project^ do begin
    if MinSizeReached then begin
      if PInfo^.SelectInside and (Abs(PInfo^.ViewRotation)<1E-4) then begin
        if IniFile^.FrameSelect then begin
          with Box do SelectRect.Assign(LimitToLong(Left),LimitToLong(Bottom),
              LimitToLong(Right),LimitToLong(Top));
          SelectByRect(SelectRect,FALSE);
        end;
      end
      else begin
        with Box do RectCorners(RotRect(Position.X,Position.Y,Width,Height,
            -PInfo^.ViewRotation),Corners);
        Poly:=New(PCPoly,Init);
        try
          for Cnt:=0 to 3 do Poly^.Data^.Insert(New(PDPoint,Init(
              LimitToLong(Corners[Cnt].X),LimitToLong(Corners[Cnt].Y))));
          Poly^.ClosePoly;
          Poly^.CalculateClipRect;
          SelectByPoly(Poly,ot_Any,PInfo^.SelectInside);
        finally
          Dispose(Poly,Done);
        end;
      end;
    end
    else begin
      if not(SnapState) then begin
        DrawPos.Init(LimitToLong(Position.X),LimitToLong(Position.Y));
        if IniFile^.DoOneSelect then DeselectAll(FALSE);
        SelectByPoint(DrawPos,ot_Any);
        if IniFile^.DoOneSelect and DDEHandler^.FOneSelect then begin
          DDEHandler^.SetDBSendStrings(DDECommOSel,NIL);
          if Layers^.SelLayer^.Data^.GetCount > 0 then ProcDBMonitoring(@Self);
          DDEHandler^.SetDBSendStrings(DDECommAll,NIL);
        end;
      end
      else begin
        Point.Init(0,0);
        if ProcSnapObjects(@Self,DrawPos,0,ot_Any,Point,SnapObject) then
          if SnapObject <> NIL then ProcSelDeselIndex(@Self,SnapObject);
      end;
    end;
  end;}
end;

{==============================================================================+
  TSelectByCircleHandler
+==============================================================================}

Procedure TSelectByCircleHandler.OnStart;
begin
  inherited OnStart;                            
  SubHandler:=TRubberEllipse.Create(Self);
  with SubHandler as TRubberEllipse do begin
    InputType:=retCircleCenterRadius;
    OnStartInput:=DoStartInput;
    OnFinishedInput:=DoFinishedInput;
  end;
  Visible:=TRUE;
end;

Procedure TSelectByCircleHandler.DoStartInput(Sender:TObject);
begin
  with Project^,SubHandler as TRubberEllipse do begin
    ViewRotation:=PInfo^.ViewRotation;
  end;
end;

Procedure TSelectByCircleHandler.DoFinishedInput(Sender:TObject);
var Middle         : TDPoint;
begin
  {$IFNDEF WMLT}
  with SubHandler as TRubberEllipse,Project^ do begin
    Middle.Init(LimitToLong(Center.X),LimitToLong(Center.Y));
    if SelectByCircle(Middle,LimitToLong(PrimaryAxis),ot_Any,PInfo^.SelectInside) then begin
       ProcDBSendUserSelection;
    end;
  end;
  {$ENDIF}
end;

Procedure TSelectByCircleHandler.OnActivate;
begin
  inherited OnActivate;
  MenuFunctions['SelectByCircle'].Checked:=TRUE;
end;

{==============================================================================+
  TSelectByPolygonHandler
+==============================================================================}

Procedure TSelectByPolygonHandler.OnStart;
begin
  inherited OnStart;
  SubHandler:=TRubberPolygon.Create(Self);
  with SubHandler as TRubberPolygon do begin
    OnStartInput:=DoStartInput;
    OnFinishedInput:=DoFinishedInput;
  end;
  Visible:=TRUE;
end;

Procedure TSelectByPolygonHandler.DoStartInput(Sender:TObject);
begin
  with Project^,SubHandler as TRubberPolygon do begin
    ViewRotation:=PInfo^.ViewRotation;
  end;
end;

Procedure TSelectByPolygonHandler.DoFinishedInput(Sender:TObject);
var Cnt            : Integer;
    Poly           : PCPoly;
begin
  {$IFNDEF WMLT}
  with SubHandler as TRubberPolygon,Project^ do begin
    Poly:=New(PCPoly,Init);
    try
      if PointCount > 2 then begin
        for Cnt:=0 to PointCount-1 do Poly^.Data^.Insert(New(PDPoint,Init(
            LimitToLong(Points[Cnt].X),LimitToLong(Points[Cnt].Y))));
        Poly^.ClosePoly;
        Poly^.CalculateClipRect;
        if SelectByPoly(Poly,ot_Any,PInfo^.SelectInside) then begin
           ProcDBSendUserSelection;
        end;
      end;
    finally
      Dispose(Poly,Done);
    end;
  end;
  {$ENDIF}
end;

Procedure TSelectByPolygonHandler.OnActivate;
begin
  inherited OnActivate;
  MenuFunctions['SelectByPolygon'].Checked:=TRUE;
end;

{==============================================================================+
  TDeselectAllHandler
+==============================================================================}

Procedure TDeselectAllHandler.OnStart;
begin
  Project^.DeSelectAll(TRUE);
  ProcDBSendUserSelection;
end;

{==============================================================================+
  TSelectAdditiveHandler
+==============================================================================}

Procedure TSelectAdditiveHandler.OnStart;
begin
  inherited OnStart;
  // Save the current selection-mode
  Project^.Registry.WriteInteger('\User\WinGISUI\DrawingTools\Selection\Mode',Integer(smAdditive));
  // activate the selection-mode
  Project^.PInfo^.SelectionMode:=smAdditive;
end;

{==============================================================================+
  TSelectSubtractiveHandler
+==============================================================================}

Procedure TSelectSubtractiveHandler.OnStart;
begin
  inherited OnStart;
  // Save the current selection-mode
  Project^.Registry.WriteInteger('\User\WinGISUI\DrawingTools\Selection\Mode',Integer(smSubtractive));
  // activate the selection-mode
  Project^.PInfo^.SelectionMode:=smSubtractive;
end;

{==============================================================================+
  TSelectXOrHandler
+==============================================================================}

Procedure TSelectXOrHandler.OnStart;
begin
  inherited OnStart;
  // Save the current selection-mode
  Project^.Registry.WriteInteger('\User\WinGISUI\DrawingTools\Selection\Mode',Integer(smXOr));
  // activate the selection-mode
  Project^.PInfo^.SelectionMode:=smXOr;
end;

{==============================================================================+
  TSelectInsideHandler
+==============================================================================}

Procedure TSelectInsideHandler.OnStart;
begin
  inherited OnStart;
  Project^.PInfo^.SelectInside:=TRUE;
  Project^.Registry.WriteBool('\User\WinGISUI\DrawingTools\Selection\Intersecting',FALSE);
end;

{==============================================================================+
  TSelectIntersectingHandler
+==============================================================================}

Procedure TSelectIntersectingHandler.OnStart;
begin
  inherited OnStart;
  Project^.PInfo^.SelectInside:=FALSE;
  Project^.Registry.WriteBool('\User\WinGISUI\DrawingTools\Selection\Intersecting',TRUE);
end;

{++ IDB}
// The UNDO functuion handler.
Procedure TUndoHandler.OnStart;
Begin
  {$IFNDEF AXDLL} // <----------------- AXDLL
    {$IFNDEF WMLT}
     If WinGisMainForm.WeAreUsingUndoFunction[Project]  AND (WinGISMainForm.ActualChild.Data.ActualMen = mn_Select) Then
        WinGisMainForm.UndoRedo_Man.Undo(Project);
    {$ENDIF}
  {$ENDIF} // <----------------- AXDLL
End;

Procedure TRedoHandler.OnStart;
Begin
  {$IFNDEF AXDLL} // <----------------- AXDLL
    {$IFNDEF WMLT}
     If WinGisMainForm.WeAreUsingUndoFunction[Project]  AND (WinGISMainForm.ActualChild.Data.ActualMen = mn_Select) Then
        WinGisMainForm.UndoRedo_Man.Redo(Project);
    {$ENDIF}
  {$ENDIF} // <----------------- AXDLL
End;
{-- IDB}

{==============================================================================+
  Initalization- and termination-code
+==============================================================================}

class function TSelectByRectangleHandler.HandlerType: Word;
begin
  Result:=mhtSingleAction;
end;

Initialization
  MenuFunctions.RegisterFromResource(HInstance,'SelectHndl','SelectHndl');
  TSelectionHandler.Registrate('EditSelect');
  TSelectByRectangleHandler.Registrate('SelectByRectangle');
  TSelectByCircleHandler.Registrate('SelectByCircle');
  TSelectByPolygonHandler.Registrate('SelectByPolygon');
  TDeselectAllHandler.Registrate('SelectDeselectAll');
  TSelectInsideHandler.Registrate('SelectInside');
  TSelectIntersectingHandler.Registrate('SelectIntersecting');
  TSelectAdditiveHandler.Registrate('SelectAdditive');  
  TSelectSubtractiveHandler.Registrate('SelectSubtractive');
  TSelectXOrHandler.Registrate('SelectXOr');
{++ IDB}
  TUndoHandler.Registrate('IDB_UndoFunction');
  TRedoHandler.Registrate('IDB_RedoFunction');  
{-- IDB}
end.

