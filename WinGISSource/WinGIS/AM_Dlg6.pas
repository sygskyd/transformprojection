{****************************************************************************}
{ Unit                                                                       }
{----------------------------------------------------------------------------}
{                                                                            }
{----------------------------------------------------------------------------}
{ Modifikationen                                                             }
{ 31.7.94                                                                    }
{****************************************************************************}
Unit AM_Dlg6;

Interface

Uses Messages,SysUtils,WinTypes,WinProcs,AM_Def,AM_Text,AM_Font,AM_Paint,
     AM_Ini,AM_Admin,AM_Layer,AM_Dlg1,AM_Dlg3,Objects,AM_StdDl,AM_BuGra,
     ResDlg,Classes,Controls,UserIntf,ProjStyle;

Const id_ButtonPlus    = 50;
      id_ButtonMinus   = 51;

Const id_LayListBox     = 101;
      id_BeamDiag       = 105;
      id_CircleDiag     = 106;
      id_LineDiag       = 107;
      id_AreaDiag       = 108;
      id_PointDiag      = 109;
      id_Legend         = 110;
      id_AnnotFont      = 125;
      id_NewPos         = 392;
      id_CircAbs        = 395;
      id_CircRel        = 396;
      id_WidthVal       = 397;
      id_HeightVal      = 398;
      id_WidthText      = 399;
      id_HeightText     = 400;
      id_Abs            = 393;
      id_Rel            = 394;
      id_ButtonFont     = 507;
      id_CDNot          = 510;
      id_CDsNever       = 511;
      id_CDAsBD         = 512;
      id_CDsAsBDs       = 513;
      id_ADAsLD         = 514;
      id_ADsAsLDs       = 515;
      id_ReadLine1      = 516;
      id_ReadLine2      = 517;
      id_TopObjTopLay   = 100;
      id_AllObjTopLay   = 101;
      id_AllObjAllLay   = 102;
      id_FirstCopy      = 103;
      id_AllCopies      = 104;
      id_KGData         = 100;
      id_GSTData        = 101;
      id_HData          = 100;

Type TDSelFontLayer = Class(TDFont)
      Public
       CopyTo         : PPLayer;
       Fixed          : Boolean;
       CollInfo       : PCollection;
       LayerScroller  : TRScrollBar;
       FirstLine      : Integer;
       EditFocus      : Integer;
       NewPos         : PBool;
       ShowNewPos     : Boolean;
       Constructor Init(AParent:TComponent;ALayers:PLayers;ACopy:PPLayer;ACollInfo:PCollection;
                        APInfo:PPaint;AData:PFontData;
                        ANewPos:PBool;AName:PChar;AShowNewPos:Boolean);
       Procedure   SetupWindow; override;
       Procedure   IDButtonPlus(var Msg : TMessage); message id_First + id_ButtonPlus;
       Procedure   IDButtonMinus(var Msg : TMessage); message id_First + id_ButtonMinus;
       Procedure   VScroll;
       Procedure   WMVScroll(var Msg : TMessage); message wm_First + wm_VScroll;
       Procedure   WMCommand(var Msg : TWMCommand); message wm_First + wm_Command;
       Function    CanClose:Boolean; override;
     end;

     TDColumn      = Class(TLayerDlg)
      Public
       Info           : PCollection;
       LayerScroller  : TRScrollBar;
       FirstLine      : Integer;
       EditFocus      : Integer;
       DiagType       : PByte;
       BeamHeight     : PLongInt;
       BeamWidth      : PLongInt;
       CircleDiameter : PLongInt;
       TempBHeight    : LongInt;
       TempBWidth     : LongInt;
       TempCDiameter  : LongInt;
       DiamAbsRel     : PBool;
       CopyTo         : PPLayer;
       ShowValues     : PBool;
       ValsAbsRel     : PBool;
       PDCInfo        : PPaint;
       PDCFontData    : PFontData;
       LastType       : Byte;
       PLAPColor      : PLongInt;
       PLAPPen        : PWord;
       PLAPPattern    : PWord;
       PLADColor      : LongInt;
       PLADPen        : Word;
       PLADPattern    : Word;
       Constructor Init(AParent:TComponent;ALayers:PLayers;ACopy:PPLayer;AInfo:PCollection;
                      APInfo:PPaint;AFontData:PFontData;ADType:PByte;ABeamHeight,ABeamWidth,
                      ACircleDiameter:PLongInt;ACDiamAbsRel:PBool;AShowValues:PBool;AAbsRel:PBool;APLAColor:PLongInt;
                      APLAPen,APLAPattern:PWord;AName:PChar);
       Procedure   SetupWindow; override;
       Procedure   IDButtonPlus(var Msg:TMessage); message id_First + id_ButtonPlus;
       Procedure   IDButtonMinus(var Msg:TMessage); message id_First + id_ButtonMinus;
       Procedure   IDButtonFont(var Msg:TMessage); message id_First + id_ButtonFont;
       Procedure   WMDrawObject(var Msg:TMessage); message wm_First+wm_DrawObject;
       Procedure   UpdateItem(Item:Integer);
       Procedure   WMDrawItem(var Msg:TMessage); message wm_First + wm_DrawItem;
       Procedure   VScroll;
       Procedure   WMVScroll(var Msg:TMessage); message wm_First + wm_VScroll;
       Procedure   WMCommand(var Msg:TMessage); message wm_First + wm_Command;
       Function    CanClose:Boolean; override;
     end;

     TDColumnChange = Class(TLayerDlg)
      Public
       Info           : PCollection;
       LayerScroller  : TRScrollBar;
       FirstLine      : Integer;
       EditFocus      : Integer;
       DiagType       : PByte;
       BeamHeight     : PLongInt;
       BeamWidth      : PLongInt;
       CircleDiameter : PLongInt;
       TempBHeight    : LongInt;
       TempBWidth     : LongInt;
       TempCDiameter  : LongInt;
       DiamAbsRel     : PBool;
       CopyTo         : PPLayer;
       ShowValues     : PBool;           
       ValsAbsRel     : PBool;
       PDCInfo        : PPaint;
       PDCFontData    : PFontData;
       LastType       : Byte;
       PLAPColor      : PLongInt;
       PLAPPen        : PWord;
       PLAPPattern    : PWord;
       PLADColor      : LongInt;
       PLADPen        : Word;
       PLADPattern    : Word;
       NewPos         : PBool;
       Constructor Init(AParent:TComponent;ALayers:PLayers;ACopy:PPLayer;var AInfo:PCollection;
                      APInfo:PPaint;AFontData:PFontData;ADType:PByte;ABeamHeight,ABeamWidth,
                      ACircleDiameter:PLongInt;ACDiamAbsRel:PBool;AShowValues:PBool;AAbsRel:PBool;APLAColor:PLongInt;
                      APLAPen,APLAPattern:PWord;ANewPos:PBool;AName:PChar);
       Procedure   SetupWindow; override;
       Procedure   UpdateItem(Item:Integer);
       Procedure   WMDrawItem(var Msg : TMessage); message wm_First + wm_DrawItem;
       Procedure   WMDrawObject(var Msg:TMessage); message wm_First+wm_DrawObject;
       Procedure   IDButtonPlus(var Msg : TMessage); message id_First + id_ButtonPlus;
       Procedure   IDButtonMinus(var Msg : TMessage); message id_First + id_ButtonMinus;
       Procedure   VScroll;
       Procedure   WMVScroll(var Msg : TMessage); message wm_First + wm_VScroll;
       Procedure   WMCommand(var Msg : TMessage); message wm_First + wm_Command;
       Procedure   IDButtonFont(var Msg:TMessage); message id_First + id_ButtonFont;
       Function    CanClose:Boolean; override;
     end;

     TDColFontDlg  = Class(TDFont)
      Public
       ValAbsRel   : PBool;
       Constructor Init(AParent:TComponent;APInfo:PPaint;AFontData:PFontData;AVAbsRel:PBool;AName:PChar);
       Procedure   SetupWindow; override;
       Function    CanClose:Boolean; override;
     end;

     TDColTypeDlg  = Class(TGrayDialog)
      Public
       OldType     : Byte;
       NewType     : PByte;
       Constructor Init(AParent:TComponent;AType:Byte;ANewType:PByte);
       Procedure   SetupWindow; override;
       Function    CanClose:Boolean; override;
     end;

     TDCTSizeDlg   = Class(TGrayDialog)
      Public
       BeamHeight     : PLongInt;
       BeamWidth      : PLongInt;
       Constructor Init(AParent:TComponent;ABHeight:PLongInt;ABWidth:PLongInt);
       Procedure   SetupWindow; override;
       Function    CanClose:Boolean; override;
     end;

     TDZoomOpt     = Class(TGrayDialog)
      Public
       Value       : PLongInt;
       Constructor Init(AParent:TComponent;AValue:PLongInt);
       Function    CanClose:Boolean; override;
       Procedure   SetupWindow; override;
     end;

Implementation

Uses AM_DBGra,Win32Def;

{*****************************************************************************************************************************}
{**************************************** TDColumn ***************************************************************************}
{*****************************************************************************************************************************}

Constructor TDColumn.Init
   (
   AParent         : TComponent;
   ALayers         : PLayers;
   ACopy           : PPLayer;
   AInfo           : PCollection;
   APInfo          : PPaint;
   AFontData       : PFontData;
   ADType          : PByte;
   ABeamHeight     : PLongInt;
   ABeamWidth      : PLongInt;
   ACircleDiameter : PLongInt;
   ACDiamAbsRel    : PBool;
   AShowValues     : PBool;
   AAbsRel         : PBool;
   APLAColor       : PLongInt;
   APLAPen         : PWord;
   APLAPattern     : PWord;
   AName           : PChar
   );
  begin
    inherited Init(AParent,AName,ALayers,APInfo^.ProjStyles);
    PDCInfo:=APInfo;
    PDCFontData:=AFontData;
    Info:=AInfo;
    EditFocus:=1;
    LayerScroller:=TRScrollBar.InitResource(Self,171);
    DiagType:=ADType;
    CopyTo:=ACopy;
    BeamHeight:=ABeamHeight;
    BeamWidth:=ABeamWidth;
    CircleDiameter:=ACircleDiameter;
    TempBHeight:=BeamHeight^;
    TempBWidth:=BeamWidth^;
    TempCDiameter:=CircleDiameter^;
    DiamAbsRel:=ACDiamAbsRel;
    ShowValues:=AShowValues;
    ValsAbsRel:=AAbsRel;
    PLAPColor:=APLAColor;
    PLAPPen:=APLAPen;
    PLAPPattern:=APLAPattern;
    PLADColor:=PLAPColor^;
    PLADPen:=PLAPPen^;
    PLADPattern:=PLAPPattern^;
  end;

{*****************************************************************************************************************************}

Procedure TDColumn.SetupWindow;
  var Temp         : Array [0..200] of Char;
      Count        : Word;
      BText        : String;
      LayerName    : Array[0..255] of Char;
      i            : Integer;
  begin
    inherited SetupWindow;
    FillLayerList(id_LayListBox,FALSE,TRUE);
    SendDlgItemMsg(id_LayListBox,cb_SetCurSel,0,0);
    StrPCopy(LayerName,(CopyTo^)^.Text^);
    SendDlgItemMsg(id_LayListBox,cb_SelectString,-1,LongInt(@LayerName));
    if Info^.Count<10 then LayerScroller.SetRange(1,1)
    else LayerScroller.SetRange(1,Info^.Count-9);
    LayerScroller.SetPosition(1);
    Count:=1;
    FirstLine:=1;
    while (Count<=10) and (Count<=Info^.Count) do begin
      StrPCopy(Temp,PNameColl(Info^.At(Count-1))^.Name^);
      SetDlgItemText(Handle,110+Count,@Temp);
      Inc(Count);
    end;
    for Count:=Info^.Count+1 to 10 do begin
      EnableWindow(GetItemHandle(110+Count),FALSE);
      EnableWindow(GetItemHandle(140+Count),FALSE);
      EnableWindow(GetItemHandle(150+Count),FALSE);
    end;
    if Info^.Count < 2 then begin
      EnableWindow(GetItemHandle(id_AreaDiag),FALSE);
      EnableWindow(GetItemHandle(id_LineDiag),FALSE);
      EnableWindow(GetItemHandle(id_PointDiag),FALSE);
      if (DiagType^ = dtLineDiag) or (DiagType^ = dtAreaDiag) or (DiagType^ = dtPointDiag) then
        DiagType^:=dtBeamDiag;
      EnableWindow(GetItemHandle(id_ButtonPlus),FALSE);
      EnableWindow(GetItemHandle(id_ButtonMinus),FALSE);
    end;
    if Info^.Count < 1 then begin
      EnableWindow(GetItemHandle(id_BeamDiag),FALSE);
      EnableWindow(GetItemHandle(id_CircleDiag),FALSE);
      DiagType^:=255;
      EnableWindow(GetItemHandle(id_OK),FALSE);
      EnableWindow(GetItemHandle(id_LayListBox),FALSE);
      EnableWindow(GetItemHandle(id_Legend),FALSE);
      EnableWindow(GetItemHandle(id_WidthVal),FALSE);
      EnableWindow(GetItemHandle(id_HeightVal),FALSE);
      EnableWindow(GetItemHandle(id_CircAbs),FALSE);
      EnableWindow(GetItemHandle(id_CircRel),FALSE);
      EnableWindow(GetItemHandle(id_ButtonFont),FALSE);
    end;
    if DiamAbsRel^ then CheckDlgButton(Handle,id_CircAbs,1)
    else CheckDlgButton(Handle,id_CircRel,1);
    case DiagType^ of
      dtCircleDiag : begin
                       CheckDlgButton(Handle,id_CircleDiag,1);
                       if DiamAbsRel^ then BText:=GetLangText(484)
                       else BText:=GetLangText(485);
                       StrPCopy(Temp,BText);
                       SetDlgItemText(Handle,id_WidthText,Temp);
                       SetDlgItemText(Handle,id_HeightText,'');
                       SetFieldLongZero(id_WidthVal,TempCDiameter);
                       EnableWindow(GetItemHandle(id_HeightVal),FALSE);
                       LastType:=dtCircleDiag;
                     end;
      dtBeamDiag   : begin
                       CheckDlgButton(Handle,id_BeamDiag,1);
                       BText:=GetLangText(482);
                       StrPCopy(Temp,BText);
                       SetDlgItemText(Handle,id_WidthText,Temp);
                       BText:=GetLangText(483);
                       StrPCopy(Temp,BText);
                       SetDlgItemText(Handle,id_HeightText,Temp);
                       SetFieldLongZero(id_WidthVal,TempBWidth);
                       SetFieldLongZero(id_HeightVal,TempBHeight);
                       EnableWindow(GetItemHandle(id_CircAbs),FALSE);
                       EnableWindow(GetItemHandle(id_CircRel),FALSE);
                       LastType:=dtBeamDiag;
                     end;
      dtLineDiag   : begin
                       CheckDlgButton(Handle,id_LineDiag,1);
                       BText:=GetLangText(481);
                       StrPCopy(Temp,BText);
                       SetDlgItemText(Handle,id_WidthText,Temp);
                       BText:=GetLangText(483);
                       StrPCopy(Temp,BText);
                       SetDlgItemText(Handle,id_HeightText,Temp);
                       SetFieldLongZero(id_WidthVal,TempBWidth);
                       SetFieldLongZero(id_HeightVal,TempBHeight);
                       EnableWindow(GetItemHandle(id_CircAbs),FALSE);
                       EnableWindow(GetItemHandle(id_CircRel),FALSE);
                       LastType:=dtLineDiag;
                     end;
      dtAreaDiag   : begin
                       CheckDlgButton(Handle,id_AreaDiag,1);
                       BText:=GetLangText(481);
                       StrPCopy(Temp,BText);
                       SetDlgItemText(Handle,id_WidthText,Temp);
                       BText:=GetLangText(483);
                       StrPCopy(Temp,BText);
                       SetDlgItemText(Handle,id_HeightText,Temp);
                       SetFieldLongZero(id_WidthVal,TempBWidth);
                       SetFieldLongZero(id_HeightVal,TempBHeight);
                       EnableWindow(GetItemHandle(id_CircAbs),FALSE);
                       EnableWindow(GetItemHandle(id_CircRel),FALSE);
                       LastType:=dtAreaDiag;
                     end;
      dtPointDiag  : begin
                       CheckDlgButton(Handle,id_PointDiag,1);
                       BText:=GetLangText(481);
                       StrPCopy(Temp,BText);
                       SetDlgItemText(Handle,id_WidthText,Temp);
                       BText:=GetLangText(483);
                       StrPCopy(Temp,BText);
                       SetDlgItemText(Handle,id_HeightText,Temp);
                       SetFieldLongZero(id_WidthVal,TempBWidth);
                       SetFieldLongZero(id_HeightVal,TempBHeight);
                       EnableWindow(GetItemHandle(id_CircAbs),FALSE);
                       EnableWindow(GetItemHandle(id_CircRel),FALSE);
                       LastType:=dtPointDiag;
                     end;
    end;
    if ShowValues^ then CheckDlgButton(Handle,id_Legend,1)
    else CheckDlgButton(Handle,id_Legend,0);
    SetDlgItemText(Handle,510,'&%');
    EnableWindow(GetItemHandle(105),FALSE);
  end;

{*****************************************************************************************************************************}

Procedure TDColumn.UpdateItem
   (
   Item            : Integer
   );
  var WHandle      : THandle;
      Rect         : TRect;
  begin
    WHandle:=GetItemHandle(Item);
    GetClientRect(WHandle,Rect);
    InvalidateRect(WHandle,@Rect,TRUE);
    UpdateWindow(WHandle);
  end;

{*****************************************************************************************************************************}

Procedure TDColumn.IDButtonPlus
   (
   var Msg         : TMessage
   );
  var Count        : Integer;
      Temp         : Array [0..200] of Char;
  begin
    if EditFocus<>0 then begin
      if EditFocus>1 then begin
        Info^.AtInsert((FirstLine+EditFocus-1) ,Info^.At(FirstLine+EditFocus-3));
        Info^.AtDelete(FirstLine+EditFocus-3);
        for Count:=EditFocus-1 to EditFocus do begin
          StrPCopy(Temp,PNameColl(Info^.At(FirstLine+Count-2))^.Name^);
          SetDlgItemText(Handle,110+Count,@Temp);
          UpdateItem(140+Count);
          UpdateItem(150+Count);
        end;
        Dec(EditFocus);
      end
      else begin
        if EditFocus+FirstLine-1>1 then begin
          Info^.AtInsert((FirstLine+EditFocus-1) ,Info^.At(FirstLine+EditFocus-3));
          Info^.AtDelete(FirstLine+EditFocus-3);
          LayerScroller.SetPosition(FirstLine-1);
          VScroll;
        end;
      end;
    end;
  end;

{*****************************************************************************************************************************}

Procedure TDColumn.IDButtonMinus
   (
   var Msg         : TMessage
   );
  var Count        : Integer;
      Temp         : Array [0..200] of Char;
  begin
    if EditFocus<>0 then begin
      if EditFocus<10 then begin
        if FirstLine+EditFocus-1<Info^.Count then begin
          Info^.AtInsert((FirstLine+EditFocus-2) ,Info^.At(FirstLine+EditFocus-1));
          Info^.AtDelete(FirstLine+EditFocus);
          for Count:=EditFocus to EditFocus+1 do begin
            StrPCopy(Temp,PNameColl(Info^.At(FirstLine+Count-2))^.Name^);
            SetDlgItemText(Handle,110+Count,@Temp);
            UpdateItem(140+Count);
            UpdateItem(150+Count);
          end;
          Inc(EditFocus);
        end;
      end
      else begin
        if EditFocus+FirstLine-1<Info^.Count then begin
          Info^.AtInsert((FirstLine+EditFocus-2) ,Info^.At(FirstLine+EditFocus-1));
          Info^.AtDelete(FirstLine+EditFocus);
          LayerScroller.SetPosition(FirstLine+1);
          VScroll;
        end;
      end;
    end;
  end;

{*****************************************************************************************************************************}

Procedure TDColumn.WMVScroll
   (
   var Msg         : TMessage
   );
  begin
    inherited ;
    VScroll;
  end;

{*****************************************************************************************************************************}

Procedure TDColumn.VScroll;
  var Pos          : Integer;
      Count        : Integer;
      Temp         : Array [0..200] of Char;
  begin
    Pos:=LayerScroller.GetPosition;
    if FirstLine<>Pos then begin
      FirstLine:=Pos;
      Count:=Pos;
      while (Count<=Pos+9) do begin
        if Count<=Info^.Count then begin
          if PNameColl(Info^.At(Count-1))^.Name<>NIL then
            StrPCopy(@Temp,PNameColl(Info^.At(Count-1))^.Name^)
          else
            Temp[0]:=#0;
          SetDlgItemText(Handle,110+Count-Pos+1,@Temp);
          UpdateItem(140+Count-Pos+1);
          UpdateItem(150+Count-Pos+1);
        end
        else begin
          SetDlgItemText(Handle,110+Count-Pos+1,'');
          EnableWindow(GetItemHandle(110+Count-Pos+1),FALSE);
          UpdateItem(140+Count-Pos+1);
          EnableWindow(GetItemHandle(140+Count-Pos+1),FALSE);
          UpdateItem(150+Count-Pos+1);
          EnableWindow(GetItemHandle(150+Count-Pos+1),FALSE);
        end;
        Inc(Count);
      end;
    end;
  end;

{*****************************************************************************************************************************}

Procedure TDColumn.WMDrawItem
   (
   var Msg         : TMessage
   );
  var Brush        : HBrush;
      OldBrush     : HBrush;
      Pen          : HPen;
      OldPen       : HPen;
      Rect         : TRect;
  begin
    with PDrawItemStruct(Msg.lParam)^ do begin
      if (ItemAction and oda_DrawEntire<>0) then begin
        if (CtlId<=150) and (CtlId>=141) then begin
          if Info^.Count>=CtlId-140 then begin
            if (IsDlgButtonChecked(Handle,id_CircleDiag) <> 0)
                or (IsDlgButtonChecked(Handle,id_BeamDiag) <> 0) then begin
              Pen:=GetPen(PNameColl(Info^.At(CtlId+FirstLine-142))^.Color,
                  PNameColl(Info^.At(CtlId+FirstLine-142))^.Pen,0);
              Brush:=GetBrush(PNameColl(Info^.At(CtlId+FirstLine-142))^.Color,
                  pt_Solid, ProjectStyles.XFillStyles);
            end
            else begin
              Pen:=GetPen(PLADColor,lt_Solid,0);
              Brush:=GetBrush(PLADColor,pt_Solid,ProjectStyles.XFillStyles);
            end;                                                                         
            SetBkColor(hDC,RGBColors[c_LGray]);
            OldPen:=SelectObject(hDC,Pen);
            OldBrush:=SelectObject(hDC,Brush);
            Rectangle(hDC,1,1,62,19);
            SelectObject(hDC,OldBrush);
            DeleteObject(Brush);
            SelectObject(hDC,OldPen);
            DeleteObject(Pen);
          end;
        end;
        if (CtlId<=160) and (CtlId>=151) then begin
          if Info^.Count>=CtlId-150 then begin
            if (IsDlgButtonChecked(Handle,id_CircleDiag) <> 0)
                or (IsDlgButtonChecked(Handle,id_BeamDiag) <> 0) then begin
              if PNameColl(Info^.At(CtlId+FirstLine-152))^.Pattern<>pt_NoPattern then begin
                Brush:=GetBrush(PNameColl(Info^.At(CtlId+FirstLine-152))^.Color,
                    PNameColl(Info^.At(CtlId+FirstLine-152))^.Pattern,ProjectStyles.XFillStyles);
                SetBkColor(hDC,RGBColors[c_LGray]);
                OldBrush:=SelectObject(hDC,Brush);
                PatBlt(hDC,1,1,61,18,PatCopy);
                SelectObject(hDC,OldBrush);
                DeleteObject(Brush);
              end;
            end
            else if (IsDlgButtonChecked(Handle,id_AreaDiag) <> 0) then begin
              if PLADPattern<>pt_NoPattern then begin
                Brush:=GetBrush(PLADColor,PLADPattern,ProjectStyles.XFillStyles);
                SetBkColor(hDC,RGBColors[c_LGray]);
                OldBrush:=SelectObject(hDC,Brush);
                PatBlt(hDC,1,1,61,18,PatCopy);
                SelectObject(hDC,OldBrush);
                DeleteObject(Brush);
              end;
            end
            else if (IsDlgButtonChecked(Handle,id_LineDiag) <> 0) then begin
              Pen:=GetPen(PLADColor,PLADPen,0);
              SetBkColor(hDC,RGBColors[c_LGray]);
              OldPen:=SelectObject(hDC,Pen);
              MoveTo(hDC,1,10);
              LineTo(hDC,61,10);
              SelectObject(hDC,OldPen);
              DeleteObject(OldPen);
            end
            else begin
              Brush:=GetBrush(PLADColor,pt_Solid,ProjectStyles.XFillStyles);
              SetBkColor(hDC,RGBColors[c_LGray]);
              OldBrush:=SelectObject(hDC,Brush);
              PatBlt(hDC,1,1,61,18,PatCopy);
              SelectObject(hDC,OldBrush);
              DeleteObject(Brush);
            end;                                                                    
          end;
        end;
      end;
      if (ItemAction and oda_Select=0) and ((ItemAction and oda_Focus<>0)
          or (hWndItem=GetFocus)) and (CtlId<=160) and (CtlId>=151) then begin
        GetClientRect(hwndItem,Rect);
        DrawFocusRect(hDC,Rect); 
      end;
    end;
  end;

{*****************************************************************************************************************************}

Procedure TDColumn.WMDrawObject
   (
   var Msg         : TMessage
   );
  var Font         : HFont;
      OldFont      : HFont;
      Rect         : TRect;
  begin
    if Msg.wParam=510 then with Msg do begin
      GetClientRect(GetItemHandle(wParam),Rect);
      SelectObject(lParamlo,GetStockObject(ltGray_Brush));
      PatBlt(lParamlo,0,0,Rect.Right,Rect.Bottom,PatCopy);
      if PDCFontData^.Font <> 0 then Font:=PDCInfo^.GetFont(PDCFontData^,30,0)
      else Font:=0;
      if Font<>0 then begin
        OldFont:=SelectObject(lParamlo,Font);
        SetBkColor(lParamlo,RGBColors[c_LGray]);
        DrawText(lParamlo,'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPp',32,Rect,dt_Left or dt_VCenter);
        SelectObject(lParamlo,OldFont);
        DeleteObject(Font);
      end;
    end;
  end;

{*****************************************************************************************************************************}

Procedure TDColumn.IDButtonFont
   (
   var Msg         : TMessage
   );
  begin
    if ExecDialog(TDColFontDlg.Init(Self,PDCInfo,PDCFontData,ValsAbsRel,'COLUMNFONT'))=id_OK then
      InvalidateRect(GetItemHandle(510),NIL,FALSE);
  end;

{*****************************************************************************************************************************}

Procedure TDColumn.WMCommand
   (
   var Msg         : TMessage
   );
  var Text         : Array [0..200] of Char;
      PText        : String;
      Return       : Integer;
      ColorType    : Integer;
      NewColor     : TColorRef;
      NewType      : Word;
      NewTransparent : Boolean;
      TempVal      : LongInt;
  begin
(*!?!?    Inherited ;
    case Msg.lParamHi of
      en_Change :  begin
        if (Msg.wParam<=120) and (Msg.wParam>=111) then begin
          if Info^.Count>=Msg.wParam-110 then begin
            Return:=GetDlgItemText(Handle,Msg.wParam,@Text,200);
            PText:=StrPas(@Text);
            DisposeStr(PNameColl(Info^.At(Msg.wParam+FirstLine-112))^.Name);
            PNameColl(Info^.At(Msg.wParam+FirstLine-112))^.Name:=NewStr(PText);
          end;
        end;
      end;
      en_SetFocus :
        if (Msg.wParam<=120) and (Msg.wParam>=111) then EditFocus:=Msg.wParam-110;
      bn_Clicked : begin
        if Msg.wParam = id_CircleDiag then begin
          if (LastType = dtCircleDiag) then begin
            if GetFieldLong(id_WidthVal,TempVal) then
              if TempVal >= 0 then TempCDiameter:=TempVal;
          end
          else if (LastType = dtBeamDiag) or (LastType = dtLineDiag)
              or (LastType = dtAreaDiag) or (LastType = dtPointDiag) then begin
            if GetFieldLong(id_WidthVal,TempVal) then
              if TempVal >= 0 then TempBWidth:=TempVal;
            if GetFieldLong(id_HeightVal,TempVal) then
             if TempVal >= 0 then TempBHeight:=TempVal;
          end;
          EnableWindow(GetItemHandle(id_CircAbs),TRUE);
          EnableWindow(GetItemHandle(id_CircRel),TRUE);
          if IsDlgButtonChecked(Handle,id_CircAbs) = 1 then PText:=GetLangText(484)
          else PText:=GetLangText(485);
          StrPCopy(Text,PText);
          SetDlgItemText(Handle,id_WidthText,Text);
          SetDlgItemText(Handle,id_HeightText,'');
          SetFieldLongZero(id_WidthVal,TempCDiameter);
          SetFieldLong(id_HeightVal,0);
          EnableWindow(GetItemHandle(id_HeightVal),FALSE);
          for Return:=141 to 160 do UpdateItem(Return);           
          LastType:=dtCircleDiag;
        end;
        if Msg.wParam = id_BeamDiag then begin
          if (LastType = dtCircleDiag) then begin
            if GetFieldLong(id_WidthVal,TempVal) then
              if TempVal >= 0 then TempCDiameter:=TempVal;
          end
          else if (LastType = dtBeamDiag) or (LastType = dtLineDiag)
              or (LastType = dtAreaDiag) or (LastType = dtPointDiag) then begin
            if GetFieldLong(id_WidthVal,TempVal) then
              if TempVal >= 0 then TempBWidth:=TempVal;
            if GetFieldLong(id_HeightVal,TempVal) then
             if TempVal >= 0 then TempBHeight:=TempVal;
          end;
          EnableWindow(GetItemHandle(id_CircAbs),FALSE);
          EnableWindow(GetItemHandle(id_CircRel),FALSE);
          EnableWindow(GetItemHandle(id_HeightVal),TRUE);
          PText:=GetLangText(482);
          StrPCopy(Text,PText);
          SetDlgItemText(Handle,id_WidthText,Text);
          PText:=GetLangText(483);
          StrPCopy(Text,PText);
          SetDlgItemText(Handle,id_HeightText,Text);
          SetFieldLongZero(id_WidthVal,TempBWidth);
          SetFieldLongZero(id_HeightVal,TempBHeight);
          for Return:=141 to 160 do UpdateItem(Return);                
          LastType:=dtBeamDiag;
        end;
        if Msg.wParam = id_LineDiag then begin
          if (LastType = dtCircleDiag) then begin
            if GetFieldLong(id_WidthVal,TempVal) then
              if TempVal >= 0 then TempCDiameter:=TempVal;
          end
          else if (LastType = dtBeamDiag) or (LastType = dtLineDiag)
              or (LastType = dtAreaDiag) or (LastType = dtPointDiag) then begin
            if GetFieldLong(id_WidthVal,TempVal) then
              if TempVal >= 0 then TempBWidth:=TempVal;
            if GetFieldLong(id_HeightVal,TempVal) then
             if TempVal >= 0 then TempBHeight:=TempVal;
          end;
          EnableWindow(GetItemHandle(id_CircAbs),FALSE);
          EnableWindow(GetItemHandle(id_CircRel),FALSE);
          EnableWindow(GetItemHandle(id_HeightVal),TRUE);
          PText:=GetLangText(481);
          StrPCopy(Text,PText);
          SetDlgItemText(Handle,id_WidthText,Text);
          PText:=GetLangText(483);
          StrPCopy(Text,PText);
          SetDlgItemText(Handle,id_HeightText,Text);
          SetFieldLongZero(id_WidthVal,TempBWidth);
          SetFieldLongZero(id_HeightVal,TempBHeight);
          for Return:=141 to 160 do UpdateItem(Return);          
          LastType:=dtLineDiag;
        end;
        if Msg.wParam = id_AreaDiag then begin
          if (LastType = dtCircleDiag) then begin
            if GetFieldLong(id_WidthVal,TempVal) then
              if TempVal >= 0 then TempCDiameter:=TempVal;
          end
          else if (LastType = dtBeamDiag) or (LastType = dtLineDiag)
              or (LastType = dtAreaDiag) or (LastType = dtPointDiag) then begin
            if GetFieldLong(id_WidthVal,TempVal) then
              if TempVal >= 0 then TempBWidth:=TempVal;
            if GetFieldLong(id_HeightVal,TempVal) then
             if TempVal >= 0 then TempBHeight:=TempVal;
          end;
          EnableWindow(GetItemHandle(id_CircAbs),FALSE);
          EnableWindow(GetItemHandle(id_CircRel),FALSE);
          EnableWindow(GetItemHandle(id_HeightVal),TRUE);
          PText:=GetLangText(481);
          StrPCopy(Text,PText);
          SetDlgItemText(Handle,id_WidthText,Text);
          PText:=GetLangText(483);
          StrPCopy(Text,PText);
          SetDlgItemText(Handle,id_HeightText,Text);
          SetFieldLongZero(id_WidthVal,TempBWidth);
          SetFieldLongZero(id_HeightVal,TempBHeight);
          for Return:=141 to 160 do UpdateItem(Return);             
          LastType:=dtAreaDiag;
        end;
        if Msg.wParam = id_PointDiag then begin
          if (LastType = dtCircleDiag) then begin
            if GetFieldLong(id_WidthVal,TempVal) then
              if TempVal >= 0 then TempCDiameter:=TempVal;
          end
          else if (LastType = dtBeamDiag) or (LastType = dtLineDiag)
              or (LastType = dtAreaDiag) or (LastType = dtPointDiag) then begin
            if GetFieldLong(id_WidthVal,TempVal) then
              if TempVal >= 0 then TempBWidth:=TempVal;
            if GetFieldLong(id_HeightVal,TempVal) then
             if TempVal >= 0 then TempBHeight:=TempVal;
          end;
          EnableWindow(GetItemHandle(id_CircAbs),FALSE);
          EnableWindow(GetItemHandle(id_CircRel),FALSE);
          EnableWindow(GetItemHandle(id_HeightVal),TRUE);
          PText:=GetLangText(481);
          StrPCopy(Text,PText);
          SetDlgItemText(Handle,id_WidthText,Text);
          PText:=GetLangText(483);
          StrPCopy(Text,PText);
          SetDlgItemText(Handle,id_HeightText,Text);
          SetFieldLongZero(id_WidthVal,TempBWidth);
          SetFieldLongZero(id_HeightVal,TempBHeight);
          for Return:=141 to 160 do UpdateItem(Return);
          LastType:=dtPointDiag;
        end;
        if Msg.wParam = id_CircAbs then begin
          PText:=GetLangText(484);
          StrPCopy(Text,PText);
          SetDlgItemText(Handle,id_WidthText,Text);
        end;
        if Msg.wParam = id_CircRel then begin
          PText:=GetLangText(485);
          StrPCopy(Text,PText);
          SetDlgItemText(Handle,id_WidthText,Text);
        end;
        if (Msg.wParam<=160) and (Msg.wParam>=151) then EditFocus:=Msg.wParam-150;
      end;
      bn_DoubleClicked : begin
        if (Msg.wParam<=160) and (Msg.wParam>=151) and (Info^.Count>=Msg.wParam-150) then begin
          if (IsDlgButtonChecked(Handle,id_CircleDiag) <> 0)                                 
              or (IsDlgButtonChecked(Handle,id_BeamDiag) <> 0) then begin
            NewColor :=PNameColl(Info^. At(Msg.wParam+FirstLine-152))^.Color;
            NewType := PNameColl(Info^.At(Msg.wParam+FirstLine- 152))^.Pattern;
            NewTransparent:=FALSE;
            ExecDialog(TFarben.Init(Self,'COLORMuster',@NewColor,
                                    @NewType, Pattern, Palette,@NewTransparent,0));
            PNameColl(Info^.At(Msg.wParam+FirstLine-152))^.Color:= NewColor;
            PNameColl(Info^.At(Msg.wParam+FirstLine-152))^.Pattern:= Byte(NewType);
            UpdateItem(Msg.wParam);
            UpdateItem(Msg.wParam-10);
          end
          else if (IsDlgButtonChecked(Handle,id_AreaDiag) <> 0) then begin
            NewColor:=PLADColor;
            NewType:=PLADPattern;
            NewTransparent:=FALSE;
            ExecDialog(TFarben.Init(Self,'COLORMUSTER',@NewColor,
                                    @NewType, Pattern, Palette, @NewTransparent, 0));
            PLADColor:=NewColor;
            PLADPattern:=Byte(NewType);
            for Return:=141 to 160 do UpdateItem(Return);
          end
          else if (IsDlgButtonChecked(Handle,id_LineDiag) <> 0) then begin
            NewColor:=PLADColor;
            NewType:=PLADPen;
            NewTransparent:=FALSE;
            ExecDialog(TFarben.Init(Self,'COLORLINIE',@NewColor,
                                    @NewType, NIL, Palette, @NewTransparent, so_NoLineWidth));
            PLADColor:=NewColor;
            PLADPen:=Byte(NewType);
            for Return:=141 to 160 do UpdateItem(Return);
          end
          else begin
            NewColor:=PLADColor;
            NewType:=pt_Solid;
            NewTransparent:=FALSE;
            ExecDialog(TFarben.Init(Self,'COLOR',@NewColor,
                                    @NewType, NIL, Palette, @NewTransparent, 0));
            PLADColor:=NewColor;
            for Return:=141 to 160 do UpdateItem(Return);
          end;
        end;
      end;
    end;*)
  end;

{*****************************************************************************************************************************}

Function TDColumn.CanClose
   : Boolean;
  var Count         : Integer;
      ALayer        : LongInt;
      AllParams     : Boolean;
      TempVal       : LongInt;
      TempVal1      : LongInt;
  begin
    CanClose:=FALSE;
    AllParams:=FALSE;
    TempVal:=-1;
    TempVal1:=-1;
    if IsDlgButtonChecked(Handle,id_CircleDiag) <> 0 then begin
      if GetFieldLong(id_WidthVal,TempVal) then begin
        if TempVal > 0 then begin
          TempCDiameter:=TempVal;
          AllParams:=TRUE;
        end
        else begin
          MsgBox(Handle,491,mb_IconExclamation or mb_Ok,'');
          SetFocus(GetItemHandle(id_WidthVal));
        end;
      end
      else begin
        MsgBox(Handle,210,mb_IconExclamation or mb_Ok,'');
        SetFocus(GetItemHandle(id_WidthVal));
      end;
    end
    else begin
      if GetFieldLong(id_WidthVal,TempVal) and GetFieldLong(id_HeightVal,TempVal1) then begin
        if (TempVal > 0) and (TempVal1 > 0) then begin
          TempBWidth:=TempVal;
          TempBHeight:=TempVal1;
          AllParams:=TRUE;
        end
        else begin
          MsgBox(Handle,491,mb_IconExclamation or mb_Ok,'');
          if TempVal <= 0 then SetFocus(GetItemHandle(id_WidthVal))
          else SetFocus(GetItemHandle(id_HeightVal));
        end;
      end
      else begin
        MsgBox(Handle,210,mb_IconExclamation or mb_Ok,'');
        if GetFieldLong(id_WidthVal,TempVal) then SetFocus(GetItemHandle(id_HeightVal))
        else SetFocus(GetItemHandle(id_WidthVal));
      end;
    end;
    if AllParams then begin
      Count:=0;
      while (Count<Info^.Count) and (PLayerInfo(Info^.At(Count))^.Name<>NIL) do Inc(Count);
      if Count<Info^.Count then begin
        LayerScroller.SetPosition(Count);
        VScroll;
        SetFocus(GetItemHandle(110+Count-FirstLine+2));
        MsgBox(Handle,492,mb_Ok or mb_IconExclamation,'');
      end
      else if GetLayer(id_LayListBox,ALayer,946,1025,10128,ilTop) and inherited CanClose then begin
{!?!?!?        if not Layers^.TranspLayer then Layers^.DetermineTopLayer;}
        CopyTo^:=Layers^.IndexToLayer(ALayer);
        DiagType^:=LastType;
        if LastType = dtCircleDiag then begin
          CircleDiameter^:=TempCDiameter;
          if IsDlgButtonChecked(Handle,id_CircAbs) = 1 then DiamAbsRel^:=TRUE
          else DiamAbsRel^:=FALSE;
        end
        else begin
          BeamWidth^:=TempBWidth;
          BeamHeight^:=TempBHeight;
        end;
        PLAPColor^:=PLADColor;
        PLAPPen^:=PLADPen;
        PLAPPattern^:=PLADPattern;
        if IsDlgButtonChecked(Handle,id_Legend) = 1 then ShowValues^:=TRUE
        else ShowValues^:=FALSE;
        CanClose:=TRUE;
      end;
    end;
  end;

{*****************************************************************************************************************************}
{**************************************** TDColumnChange *********************************************************************}
{*****************************************************************************************************************************}

Constructor TDColumnChange.Init
   (
   AParent         : TComponent;
   ALayers         : PLayers;
   ACopy           : PPLayer;
   var AInfo       : PCollection;
   APInfo          : PPaint;
   AFontData       : PFontData;
   ADType          : PByte;
   ABeamHeight     : PLongInt;
   ABeamWidth      : PLongInt;
   ACircleDiameter : PLongInt;
   ACDiamAbsRel    : PBool;
   AShowValues     : PBool;
   AAbsRel         : PBool;
   APLAColor       : PLongInt;
   APLAPen         : PWord;
   APLAPattern     : PWord;
   ANewPos         : PBool;
   AName           : PChar
   );
  begin
    inherited Init(AParent,AName,ALayers,APInfo^.ProjStyles);
    PDCInfo:=APInfo;
    PDCFontData:=AFontData;
    Info:=AInfo;
    EditFocus:=1;
    LayerScroller:=TRScrollBar.InitResource(Self,171);
    DiagType:=ADType;
    CopyTo:=ACopy;
    BeamHeight:=ABeamHeight;
    BeamWidth:=ABeamWidth;
    CircleDiameter:=ACircleDiameter;
    TempBHeight:=BeamHeight^;
    TempBWidth:=BeamWidth^;
    TempCDiameter:=CircleDiameter^;
    DiamAbsRel:=ACDiamAbsRel;
    ShowValues:=AShowValues;
    ValsAbsRel:=AAbsRel;
    PLAPColor:=APLAColor;
    PLAPPen:=APLAPen;
    PLAPPattern:=APLAPattern;
    PLADColor:=PLAPColor^;
    PLADPen:=PLAPPen^;
    PLADPattern:=PLAPPattern^;
    NewPos:=ANewPos;
  end;

{*****************************************************************************************************************************}

Procedure TDColumnChange.SetupWindow;
  var Temp         : Array [0..200] of Char;
      Count        : Word;
      AIndex       : Integer;
      AStyle       : LongInt;
      DoDisable    : Boolean;
      BText        : String;
      LayerName    : Array[0..255] of Char;
      HasPlusMinus : Boolean;
      GesSum       : Real;

  Procedure DoAll
     (
     Item          : PDGraph
     ); Far;
    begin
      GesSum:=GesSum+Item^.Value;
    end;

  begin
    inherited SetupWindow;
    HasPlusMinus:=FALSE;
    GesSum:=0;
    Info^.ForEach(@DoAll);
    HasPlusMinus:=PlusMinus(Info);
    FillLayerList(id_LayListBox,FALSE,TRUE);
    SendDlgItemMsg(id_LayListBox,cb_SetCurSel,0,0);
    StrPCopy(LayerName,(CopyTo^)^.Text^);
    SendDlgItemMsg(id_LayListBox,cb_SelectString,-1,LongInt(@LayerName));
    if Info^.Count<10 then LayerScroller.SetRange(1,1)
    else LayerScroller.SetRange(1,Info^.Count-9);
    LayerScroller.SetPosition(1);
    Count:=1;
    FirstLine:=1;
    while (Count<=10) and (Count<=Info^.Count) do begin
      StrCopy(Temp,PDGraph(Info^.At(Count-1))^.Name);
      SetDlgItemText(Handle,110+Count,@Temp);
      Str(PDGraph(Info^.At(Count-1))^.Value:12:2,BText);
      StrPCopy(Temp,BText);
      SetDlgItemText(Handle,160+Count,@Temp);
      Inc(Count);
    end;
    for Count:=Info^.Count+1 to 10 do begin
      EnableWindow(GetItemHandle(110+Count),FALSE);
      EnableWindow(GetItemHandle(140+Count),FALSE);
      EnableWindow(GetItemHandle(150+Count),FALSE);
      EnableWindow(GetItemHandle(160+Count),FALSE);
    end;
    if Info^.Count < 2 then begin
      EnableWindow(GetItemHandle(id_AreaDiag),FALSE);
      EnableWindow(GetItemHandle(id_LineDiag),FALSE);
      EnableWindow(GetItemHandle(id_PointDiag),FALSE);
      if (DiagType^ = dtLineDiag) or (DiagType^ = dtAreaDiag) or (DiagType^ = dtPointDiag) then
        DiagType^:=dtBeamDiag;
      EnableWindow(GetItemHandle(id_ButtonPlus),FALSE);
      EnableWindow(GetItemHandle(id_ButtonMinus),FALSE);
    end;
    if Info^.Count < 1 then begin
      EnableWindow(GetItemHandle(id_BeamDiag),FALSE);
      EnableWindow(GetItemHandle(id_CircleDiag),FALSE);
      DiagType^:=255;
      EnableWindow(GetItemHandle(id_OK),FALSE);
      EnableWindow(GetItemHandle(id_LayListBox),FALSE);
      EnableWindow(GetItemHandle(id_Legend),FALSE);
      EnableWindow(GetItemHandle(id_WidthVal),FALSE);
      EnableWindow(GetItemHandle(id_HeightVal),FALSE);
      EnableWindow(GetItemHandle(id_CircAbs),FALSE);
      EnableWindow(GetItemHandle(id_CircRel),FALSE);
      EnableWindow(GetItemHandle(id_ButtonFont),FALSE);
      EnableWindow(GetItemHandle(id_NewPos),FALSE);
    end;
    if DiamAbsRel^ then CheckDlgButton(Handle,id_CircAbs,1)
    else CheckDlgButton(Handle,id_CircRel,1);
    case DiagType^ of
      dtCircleDiag : begin
                       CheckDlgButton(Handle,id_CircleDiag,1);
                       if DiamAbsRel^ then BText:=GetLangText(484)
                       else BText:=GetLangText(485);
                       StrPCopy(Temp,BText);
                       SetDlgItemText(Handle,id_WidthText,Temp);
                       SetDlgItemText(Handle,id_HeightText,'');
                       SetFieldLongZero(id_WidthVal,TempCDiameter);
                       EnableWindow(GetItemHandle(id_HeightVal),FALSE);
                       LastType:=dtCircleDiag;
                     end;
      dtBeamDiag   : begin
                       CheckDlgButton(Handle,id_BeamDiag,1);
                       BText:=GetLangText(482);
                       StrPCopy(Temp,BText);
                       SetDlgItemText(Handle,id_WidthText,Temp);
                       BText:=GetLangText(483);
                       StrPCopy(Temp,BText);
                       SetDlgItemText(Handle,id_HeightText,Temp);
                       SetFieldLongZero(id_WidthVal,TempBWidth);
                       SetFieldLongZero(id_HeightVal,TempBHeight);
                       EnableWindow(GetItemHandle(id_CircAbs),FALSE);
                       EnableWindow(GetItemHandle(id_CircRel),FALSE);
                       LastType:=dtBeamDiag;
                     end;
      dtLineDiag   : begin
                       CheckDlgButton(Handle,id_LineDiag,1);
                       BText:=GetLangText(481);
                       StrPCopy(Temp,BText);
                       SetDlgItemText(Handle,id_WidthText,Temp);
                       BText:=GetLangText(483);
                       StrPCopy(Temp,BText);
                       SetDlgItemText(Handle,id_HeightText,Temp);
                       SetFieldLongZero(id_WidthVal,TempBWidth);
                       SetFieldLongZero(id_HeightVal,TempBHeight);
                       EnableWindow(GetItemHandle(id_CircAbs),FALSE);
                       EnableWindow(GetItemHandle(id_CircRel),FALSE);
                       LastType:=dtLineDiag;
                     end;
      dtAreaDiag   : begin
                       CheckDlgButton(Handle,id_AreaDiag,1);
                       BText:=GetLangText(481);
                       StrPCopy(Temp,BText);
                       SetDlgItemText(Handle,id_WidthText,Temp);
                       BText:=GetLangText(483);
                       StrPCopy(Temp,BText);
                       SetDlgItemText(Handle,id_HeightText,Temp);
                       SetFieldLongZero(id_WidthVal,TempBWidth);
                       SetFieldLongZero(id_HeightVal,TempBHeight);
                       EnableWindow(GetItemHandle(id_CircAbs),FALSE);
                       EnableWindow(GetItemHandle(id_CircRel),FALSE);
                       LastType:=dtAreaDiag;
                     end;
      dtPointDiag  : begin
                       CheckDlgButton(Handle,id_PointDiag,1);
                       BText:=GetLangText(481);
                       StrPCopy(Temp,BText);
                       SetDlgItemText(Handle,id_WidthText,Temp);
                       BText:=GetLangText(483);
                       StrPCopy(Temp,BText);
                       SetDlgItemText(Handle,id_HeightText,Temp);
                       SetFieldLongZero(id_WidthVal,TempBWidth);
                       SetFieldLongZero(id_HeightVal,TempBHeight);
                       EnableWindow(GetItemHandle(id_CircAbs),FALSE);
                       EnableWindow(GetItemHandle(id_CircRel),FALSE);
                       LastType:=dtPointDiag;
                     end;
      else           begin
                       CheckDlgButton(Handle,id_BeamDiag,1);
                       BText:=GetLangText(482);
                       StrPCopy(Temp,BText);
                       SetDlgItemText(Handle,id_WidthText,Temp);
                       BText:=GetLangText(483);
                       StrPCopy(Temp,BText);
                       SetDlgItemText(Handle,id_HeightText,Temp);
                       SetFieldLongZero(id_WidthVal,TempBWidth);
                       SetFieldLongZero(id_HeightVal,TempBHeight);
                       EnableWindow(GetItemHandle(id_CircAbs),FALSE);
                       EnableWindow(GetItemHandle(id_CircRel),FALSE);
                       LastType:=dtBeamDiag;
                     end;
    end;
    if ShowValues^ then CheckDlgButton(Handle,id_Legend,1)
    else CheckDlgButton(Handle,id_Legend,0);
    if HasPlusMinus then begin
      EnableWindow(GetItemHandle(id_CircleDiag),FALSE);
      EnableWindow(GetItemHandle(id_AreaDiag),FALSE);
    end;
    if GesSum = 0 then EnableWindow(GetItemHandle(id_CircleDiag),FALSE);
    SetDlgItemText(Handle,510,'&%');
  end;

{*****************************************************************************************************************************}

Procedure TDColumnChange.UpdateItem
   (
   Item            : Integer
   );
  var WHandle      : THandle;
      Rect         : TRect;
  begin
    WHandle:=GetItemHandle(Item);
    GetClientRect(WHandle,Rect);
    InvalidateRect(WHandle,@Rect,TRUE);
    UpdateWindow(WHandle);
  end;

{*****************************************************************************************************************************}

Procedure TDColumnChange.WMDrawItem
   (
   var Msg         : TMessage
   );
  var Brush        : HBrush;
      OldBrush     : HBrush;
      Pen          : HPen;
      OldPen       : HPen;
      Rect         : TRect;
  begin
    with PDrawItemStruct(Msg.lParam)^ do begin
      if (ItemAction and oda_DrawEntire<>0) then begin
        if (CtlId<=150) and (CtlId>=141) then begin
          if Info^.Count>=CtlId-140 then begin
            if (IsDlgButtonChecked(Handle,id_CircleDiag) <> 0)
                or (IsDlgButtonChecked(Handle,id_BeamDiag) <> 0) then begin
              Pen:=GetPen(PDGraph(Info^.At(CtlId+FirstLine-142))^.Color,
                  PDGraph(Info^.At(CtlId+FirstLine-142))^.Pen,0);
              Brush:=GetBrush(PDGraph(Info^.At(CtlId+FirstLine-142))^.Color,
                  pt_Solid, ProjectStyles.XFillStyles);
            end
            else begin
              Pen:=GetPen(PLADColor,lt_Solid,0);
              Brush:=GetBrush(PLADColor,pt_Solid,ProjectStyles.XFillStyles);
            end;                                                                            
            SetBkColor(hDC,RGBColors[c_LGray]);
            OldPen:=SelectObject(hDC,Pen);
            OldBrush:=SelectObject(hDC,Brush);
            Rectangle(hDC,1,1,62,19);
            SelectObject(hDC,OldBrush);
            DeleteObject(Brush);
            SelectObject(hDC,OldPen);
            DeleteObject(Pen);
          end;
        end;
        if (CtlId<=160) and (CtlId>=151) then begin
          if Info^.Count>=CtlId-150 then begin
            if (IsDlgButtonChecked(Handle,id_CircleDiag) <> 0)                      
                or (IsDlgButtonChecked(Handle,id_BeamDiag) <> 0) then begin
              if PDGraph(Info^.At(CtlId+FirstLine-152))^.Pattern<>pt_NoPattern then begin
                Brush:=GetBrush(PDGraph(Info^.At(CtlId+FirstLine-152))^.Color,
                    PDGraph(Info^.At(CtlId+FirstLine-152))^.Pattern, ProjectStyles.XFillStyles);
                SetBkColor(hDC,RGBColors[c_LGray]);
                OldBrush:=SelectObject(hDC,Brush);
                PatBlt(hDC,1,1,61,18,PatCopy);
                SelectObject(hDC,OldBrush);
                DeleteObject(Brush);
              end;
            end
            else if (IsDlgButtonChecked(Handle,id_AreaDiag) <> 0) then begin
              if PLADPattern<>pt_NoPattern then begin
                Brush:=GetBrush(PLADColor,PLADPattern,ProjectStyles.XFillStyles);
                SetBkColor(hDC,RGBColors[c_LGray]);
                OldBrush:=SelectObject(hDC,Brush);
                PatBlt(hDC,1,1,61,18,PatCopy);
                SelectObject(hDC,OldBrush);
                DeleteObject(Brush);
              end;
            end
            else if (IsDlgButtonChecked(Handle,id_LineDiag) <> 0) then begin
              Pen:=GetPen(PLADColor,PLADPen,0);
              SetBkColor(hDC,RGBColors[c_LGray]);
              OldPen:=SelectObject(hDC,Pen);
              MoveTo(hDC,1,10);
              LineTo(hDC,61,10);
              SelectObject(hDC,OldBrush);
              DeleteObject(Brush);
            end
            else begin
              Brush:=GetBrush(PLADColor,pt_Solid,ProjectStyles.XFillStyles);
              SetBkColor(hDC,RGBColors[c_LGray]);
              OldBrush:=SelectObject(hDC,Brush);
              PatBlt(hDC,1,1,61,18,PatCopy);
              SelectObject(hDC,OldBrush);
              DeleteObject(Brush);
            end;                                                                        
          end;
        end;
      end;
      if (ItemAction and oda_Select=0) and ((ItemAction and oda_Focus<>0)
          or (hWndItem=GetFocus)) and (CtlId<=160) and (CtlId>=151) then begin
        GetClientRect(hwndItem,Rect);
        DrawFocusRect(hDC,Rect);
      end;
    end;
  end;
                    
{*****************************************************************************************************************************}

Procedure TDColumnChange.WMDrawObject
   (
   var Msg         : TMessage
   );
  var Font         : HFont;
      OldFont      : HFont;
      Rect         : TRect;
  begin
    if Msg.wParam=510 then with Msg do begin
      GetClientRect(GetItemHandle(wParam),Rect);
      SelectObject(lParamlo,GetStockObject(ltGray_Brush));
      PatBlt(lParamlo,0,0,Rect.Right,Rect.Bottom,PatCopy);
      if PDCFontData^.Font <> 0 then Font:=PDCInfo^.GetFont(PDCFontData^,30,0);
      if Font<>0 then begin
        OldFont:=SelectObject(lParamlo,Font);
        SetBkColor(lParamlo,RGBColors[c_LGray]);
        DrawText(lParamlo,'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPp',32,Rect,dt_Left or dt_VCenter);
        SelectObject(lParamlo,OldFont);
        DeleteObject(Font);
      end;
    end;
  end;

{*****************************************************************************************************************************}

Procedure TDColumnChange.IDButtonPlus
   (
   var Msg : TMessage
   );
  var Count : Integer;
      Temp  : Array [0..200] of char;
      BText : String;
  begin
    if EditFocus<>0 then begin
      if EditFocus>1 then begin
        Info^.AtInsert((FirstLine+EditFocus-1) ,Info^.At(FirstLine+EditFocus-3));
        Info^.AtDelete(FirstLine+EditFocus-3);
        for Count:=EditFocus-1 to EditFocus do begin
          StrCopy(Temp,PDGraph(Info^.At(FirstLine+Count-2))^.Name);
          SetDlgItemText(Handle,110+Count,@Temp);
          Str(PDGraph(Info^.At(FirstLine+Count-2))^.Value:12:2,BText);
          StrPCopy(Temp,BText);
          SetDlgItemText(Handle,160+Count,@Temp);
          UpdateItem(140+Count);
          UpdateItem(150+Count);
        end;
        Dec(EditFocus);
      end
      else begin
        if EditFocus+FirstLine-1>1 then begin
          Info^.AtInsert((FirstLine+EditFocus-1) ,Info^.At(FirstLine+EditFocus-3));
          Info^.AtDelete(FirstLine+EditFocus-3);
          LayerScroller.SetPosition(FirstLine-1);
          VScroll;
        end;
      end;
    end;
  end;

{*****************************************************************************************************************************}

Procedure TDColumnChange.IDButtonMinus
   (
   var Msg : TMessage
   );
  var Count : Integer;
      Temp  : Array [0..200] of Char;
      BText : String;
  begin
    if EditFocus<>0 then begin
      if EditFocus<10 then begin
        if FirstLine+EditFocus-1<Info^.Count then begin
          Info^.AtInsert((FirstLine+EditFocus-2) ,Info^.At(FirstLine+EditFocus-1));
          Info^.AtDelete(FirstLine+EditFocus);
          for Count:=EditFocus to EditFocus+1 do begin
            StrCopy(Temp,PDGraph(Info^.At(FirstLine+Count-2))^.Name);
            SetDlgItemText(Handle,110+Count,@Temp);
            Str(PDGraph(Info^.At(FirstLine+Count-2))^.Value:12:2,BText);
            StrPCopy(Temp,BText);
            SetDlgItemText(Handle,160+Count,@Temp);
            UpdateItem(140+Count);
            UpdateItem(150+Count);
          end;
          Inc(EditFocus);
        end;
      end
      else begin
        if EditFocus+FirstLine-1<Info^.Count then begin
          Info^.AtInsert((FirstLine+EditFocus-2) ,Info^.At(FirstLine+EditFocus-1));
          Info^.AtDelete(FirstLine+EditFocus);
          LayerScroller.SetPosition(FirstLine+1);
          VScroll;
        end;
      end;
    end;
  end;

{*****************************************************************************************************************************}

Procedure TDColumnChange.WMVScroll
   (
   var Msg : TMessage
   );

  begin
    inherited ;
    VScroll;
  end;

{*****************************************************************************************************************************}

Procedure TDColumnChange.VScroll;
  var Pos   : Integer;
      Count : Integer;
      Temp1 : Array [0..200] of Char;
      Temp2 : Array [0..200] of Char;
      BText : String;
  begin
    Pos:=LayerScroller.GetPosition;
    if FirstLine<>Pos then begin
      FirstLine:=Pos;
      Count:=Pos;
      while (Count<=Pos+9) do begin
        if Count<=Info^.Count then begin
          if PDGraph(Info^.At(Count-1))^.Name<>NIL then begin
            StrCopy(Temp1,PDGraph(Info^.At(Count-1))^.Name);
            Str(PDGraph(Info^.At(Count-1))^.Value:12:2,BText);
            StrPCopy(Temp2,BText);
          end
          else begin
            Temp1[0]:=#0;
            Temp2[0]:=#0;
          end;
          SetDlgItemText(Handle,110+Count-Pos+1,@Temp1);
          SetDlgItemText(Handle,160+Count-Pos+1,@Temp2);
          UpdateItem(140+Count-Pos+1);
          UpdateItem(150+Count-Pos+1);
        end
        else begin
          SetDlgItemText(Handle,110+Count-Pos+1,'');
          SetDlgItemText(Handle,160+Count-Pos+1,'');
          EnableWindow(GetItemHandle(110+Count-Pos+1),FALSE);
          EnableWindow(GetItemHandle(160+Count-Pos+1),FALSE);
          UpdateItem(140+Count-Pos+1);
          EnableWindow(GetItemHandle(140+Count-Pos+1),FALSE);
          UpdateItem(150+Count-Pos+1);
          EnableWindow(GetItemHandle(150+Count-Pos+1),FALSE);
        end;
        Inc(Count);
      end;
    end;
  end;

{*****************************************************************************************************************************}

Procedure TDColumnChange.WMCommand
   (
   var Msg            : TMessage
   );
  var Text             : Array [0..200] of Char;
      Return           : Integer;
      Value            : Real;
      Error            : Integer;
      TempVal          : LongInt;
      PText            : String;
      NewColor         : TColorRef;
      NewType          : Word;
      NewTransparent   : Boolean;
      ColorType        : Integer;
  begin
(*!?!?    Inherited ;
    case Msg.lParamHi of
      en_Change :  begin
        if (Msg.wParam<=120) and (Msg.wParam>=111) then begin
          if Info^.Count>=Msg.wParam-110 then begin
            Return:=GetDlgItemText(Handle,Msg.wParam,@Text,200);
            StrDispose(PDGraph(Info^.At(Msg.wParam+FirstLine-112))^.Name);
            PDGraph(Info^.At(Msg.wParam+FirstLine-112))^.Name:=StrNew(Text);
          end;
        end;
        if (Msg.wParam<=170) and (Msg.wParam>=161) then begin
          if Info^.Count>=Msg.wParam-160 then begin
            Return:=GetDlgItemText(Handle,Msg.wParam,@Text,200);
            Val(Text,Value,Error);
            PDGraph(Info^.At(Msg.wParam+FirstLine-162))^.Value:=Value;
          end;
        end;
      end;
      en_SetFocus : begin
        if (Msg.wParam<=120) and (Msg.wParam>=111) then EditFocus:=Msg.wParam-110
        else if (Msg.wParam<=170) and (Msg.wParam>=161) then EditFocus:=Msg.wParam-160;
      end;
      bn_Clicked : begin
        if Msg.wParam = id_CircleDiag then begin
          if (LastType = dtCircleDiag) then begin
            if GetFieldLong(id_WidthVal,TempVal) then
              if TempVal >= 0 then TempCDiameter:=TempVal;
          end
          else if (LastType = dtBeamDiag) or (LastType = dtLineDiag)
              or (LastType = dtAreaDiag) or (LastType = dtPointDiag) then begin
            if GetFieldLong(id_WidthVal,TempVal) then
              if TempVal >= 0 then TempBWidth:=TempVal;
            if GetFieldLong(id_HeightVal,TempVal) then
             if TempVal >= 0 then TempBHeight:=TempVal;
          end;
          EnableWindow(GetItemHandle(id_CircAbs),TRUE);
          EnableWindow(GetItemHandle(id_CircRel),TRUE);
          if IsDlgButtonChecked(Handle,id_CircAbs) = 1 then PText:=GetLangText(484)
          else PText:=GetLangText(485);
          StrPCopy(Text,PText);
          SetDlgItemText(Handle,id_WidthText,Text);
          SetDlgItemText(Handle,id_HeightText,'');
          SetFieldLongZero(id_WidthVal,TempCDiameter);
          SetFieldLong(id_HeightVal,0);
          EnableWindow(GetItemHandle(id_HeightVal),FALSE);
          for Return:=141 to 160 do UpdateItem(Return);         
          LastType:=dtCircleDiag;
        end;
        if Msg.wParam = id_BeamDiag then begin
          if (LastType = dtCircleDiag) then begin
            if GetFieldLong(id_WidthVal,TempVal) then
              if TempVal >= 0 then TempCDiameter:=TempVal;
          end
          else if (LastType = dtBeamDiag) or (LastType = dtLineDiag)
              or (LastType = dtAreaDiag) or (LastType = dtPointDiag) then begin
            if GetFieldLong(id_WidthVal,TempVal) then
              if TempVal >= 0 then TempBWidth:=TempVal;
            if GetFieldLong(id_HeightVal,TempVal) then
             if TempVal >= 0 then TempBHeight:=TempVal;
          end;
          EnableWindow(GetItemHandle(id_CircAbs),FALSE);
          EnableWindow(GetItemHandle(id_CircRel),FALSE);
          EnableWindow(GetItemHandle(id_HeightVal),TRUE);
          PText:=GetLangText(482);
          StrPCopy(Text,PText);
          SetDlgItemText(Handle,id_WidthText,Text);
          PText:=GetLangText(483);
          StrPCopy(Text,PText);
          SetDlgItemText(Handle,id_HeightText,Text);
          SetFieldLongZero(id_WidthVal,TempBWidth);
          SetFieldLongZero(id_HeightVal,TempBHeight);
          for Return:=141 to 160 do UpdateItem(Return);            
          LastType:=dtBeamDiag;
        end;
        if Msg.wParam = id_LineDiag then begin
          if (LastType = dtCircleDiag) then begin
            if GetFieldLong(id_WidthVal,TempVal) then
              if TempVal >= 0 then TempCDiameter:=TempVal;
          end
          else if (LastType = dtBeamDiag) or (LastType = dtLineDiag)
              or (LastType = dtAreaDiag) or (LastType = dtPointDiag) then begin
            if GetFieldLong(id_WidthVal,TempVal) then
              if TempVal >= 0 then TempBWidth:=TempVal;
            if GetFieldLong(id_HeightVal,TempVal) then
             if TempVal >= 0 then TempBHeight:=TempVal;
          end;
          EnableWindow(GetItemHandle(id_CircAbs),FALSE);
          EnableWindow(GetItemHandle(id_CircRel),FALSE);
          EnableWindow(GetItemHandle(id_HeightVal),TRUE);
          PText:=GetLangText(481);
          StrPCopy(Text,PText);
          SetDlgItemText(Handle,id_WidthText,Text);
          PText:=GetLangText(483);
          StrPCopy(Text,PText);
          SetDlgItemText(Handle,id_HeightText,Text);
          SetFieldLongZero(id_WidthVal,TempBWidth);
          SetFieldLongZero(id_HeightVal,TempBHeight);
          for Return:=141 to 160 do UpdateItem(Return);      
          LastType:=dtLineDiag;
        end;
        if Msg.wParam = id_AreaDiag then begin
          if (LastType = dtCircleDiag) then begin
            if GetFieldLong(id_WidthVal,TempVal) then
              if TempVal >= 0 then TempCDiameter:=TempVal;
          end
          else if (LastType = dtBeamDiag) or (LastType = dtLineDiag)
              or (LastType = dtAreaDiag) or (LastType = dtPointDiag) then begin
            if GetFieldLong(id_WidthVal,TempVal) then
              if TempVal >= 0 then TempBWidth:=TempVal;
            if GetFieldLong(id_HeightVal,TempVal) then
             if TempVal >= 0 then TempBHeight:=TempVal;
          end;
          EnableWindow(GetItemHandle(id_CircAbs),FALSE);
          EnableWindow(GetItemHandle(id_CircRel),FALSE);
          EnableWindow(GetItemHandle(id_HeightVal),TRUE);
          PText:=GetLangText(481);
          StrPCopy(Text,PText);
          SetDlgItemText(Handle,id_WidthText,Text);
          PText:=GetLangText(483);
          StrPCopy(Text,PText);
          SetDlgItemText(Handle,id_HeightText,Text);
          SetFieldLongZero(id_WidthVal,TempBWidth);
          SetFieldLongZero(id_HeightVal,TempBHeight);
          for Return:=141 to 160 do UpdateItem(Return);           
          LastType:=dtAreaDiag;
        end;
        if Msg.wParam = id_PointDiag then begin
          if (LastType = dtCircleDiag) then begin
            if GetFieldLong(id_WidthVal,TempVal) then
              if TempVal >= 0 then TempCDiameter:=TempVal;
          end
          else if (LastType = dtBeamDiag) or (LastType = dtLineDiag)
              or (LastType = dtAreaDiag) or (LastType = dtPointDiag) then begin
            if GetFieldLong(id_WidthVal,TempVal) then
              if TempVal >= 0 then TempBWidth:=TempVal;
            if GetFieldLong(id_HeightVal,TempVal) then
             if TempVal >= 0 then TempBHeight:=TempVal;
          end;
          EnableWindow(GetItemHandle(id_CircAbs),FALSE);
          EnableWindow(GetItemHandle(id_CircRel),FALSE);
          EnableWindow(GetItemHandle(id_HeightVal),TRUE);
          PText:=GetLangText(481);
          StrPCopy(Text,PText);
          SetDlgItemText(Handle,id_WidthText,Text);
          PText:=GetLangText(483);
          StrPCopy(Text,PText);
          SetDlgItemText(Handle,id_HeightText,Text);
          SetFieldLongZero(id_WidthVal,TempBWidth);
          SetFieldLongZero(id_HeightVal,TempBHeight);
          for Return:=141 to 160 do UpdateItem(Return);
          LastType:=dtPointDiag;
        end;
        if Msg.wParam = id_CircAbs then begin
          PText:=GetLangText(484);
          StrPCopy(Text,PText);
          SetDlgItemText(Handle,id_WidthText,Text);
        end;
        if Msg.wParam = id_CircRel then begin
          PText:=GetLangText(485);
          StrPCopy(Text,PText);
          SetDlgItemText(Handle,id_WidthText,Text);
        end;
        if (Msg.wParam<=160) and (Msg.wParam>=151) then EditFocus:=Msg.wParam-150;
      end;
      bn_DoubleClicked : begin
        if (Msg.wParam<=160) and (Msg.wParam>=151) and (Info^.Count>=Msg.wParam-150) then begin
          if (IsDlgButtonChecked(Handle,id_CircleDiag) <> 0)                     
              or (IsDlgButtonChecked(Handle,id_BeamDiag) <> 0) then begin
            NewColor :=PDGraph(Info^. At(Msg.wParam+FirstLine-152))^.Color;
            NewType := PDGraph(Info^.At(Msg.wParam+FirstLine- 152))^.Pattern;
            NewTransparent:=FALSE;
            ExecDialog(TFarben.Init(Self,'COLORMuster',@NewColor,
                                    @NewType, Pattern, Palette,@NewTransparent,0));
            PDGraph(Info^.At(Msg.wParam+FirstLine-152))^.Color:= NewColor;
            PDGraph(Info^.At(Msg.wParam+FirstLine-152))^.Pattern:= Byte(NewType);
            UpdateItem(Msg.wParam);
            UpdateItem(Msg.wParam-10);
          end
          else if (IsDlgButtonChecked(Handle,id_AreaDiag) <> 0) then begin
            NewColor:=PLADColor;
            NewType:=PLADPattern;
            NewTransparent:=FALSE;
            ExecDialog(TFarben.Init(Self,'COLORMUSTER',@NewColor,
                                    @NewType, Pattern, Palette, @NewTransparent, 0));
            PLADColor:=NewColor;
            PLADPattern:=Byte(NewType);
            for Return:=141 to 160 do UpdateItem(Return);
          end
          else if (IsDlgButtonChecked(Handle,id_LineDiag) <> 0) then begin
            NewColor:=PLADColor;
            NewType:=PLADPen;
            NewTransparent:=FALSE;
            ExecDialog(TFarben.Init(Self,'COLORLINIE',@NewColor,
                                    @NewType, NIL, Palette, @NewTransparent, so_NoLineWidth));
            PLADColor:=NewColor;
            PLADPen:=Byte(NewType);
            for Return:=141 to 160 do UpdateItem(Return);
          end
          else begin
            NewColor:=PLADColor;
            NewType:=pt_Solid;
            NewTransparent:=FALSE;
            ExecDialog(TFarben.Init(Self,'COLOR',@NewColor,
                                    @NewType, NIL, Palette, @NewTransparent, 0));
            PLADColor:=NewColor;
            for Return:=141 to 160 do UpdateItem(Return);
          end;                                                                             
        end;
      end;
    end;*)
  end;

{*****************************************************************************************************************************}

Procedure TDColumnChange.IDButtonFont
   (
   var Msg         : TMessage
   );
  begin
    if ExecDialog(TDColFontDlg.Init(Self,PDCInfo,PDCFontData,ValsAbsRel,'COLUMNFONT'))=id_OK then
      InvalidateRect(GetItemHandle(510),NIL,FALSE);
  end;

{*****************************************************************************************************************************}

Function TDColumnChange.CanClose
   : Boolean;
  var Count        : Integer;
      ALayer       : LongInt;
      AllParams    : Boolean;
      TempVal      : LongInt;
      TempVal1     : LongInt;
  begin
    CanClose:=FALSE;
    AllParams:=FALSE;
    TempVal:=-1;
    TempVal1:=-1;
    Count:=0;
    if IsDlgButtonChecked(Handle,id_CircleDiag) <> 0 then begin
      if GetFieldLong(id_WidthVal,TempVal) then begin
        if TempVal > 0 then begin
          TempCDiameter:=TempVal;
          AllParams:=TRUE;
        end
        else begin
          MsgBox(Handle,491,mb_IconExclamation or mb_Ok,'');
          SetFocus(GetItemHandle(id_WidthVal));
        end;
      end
      else begin
        MsgBox(Handle,210,mb_IconExclamation or mb_Ok,'');
        SetFocus(GetItemHandle(id_WidthVal));
      end;
    end
    else begin
      if GetFieldLong(id_WidthVal,TempVal) and GetFieldLong(id_HeightVal,TempVal1) then begin
        if (TempVal > 0) and (TempVal1 > 0) then begin
          TempBWidth:=TempVal;
          TempBHeight:=TempVal1;
          AllParams:=TRUE;
        end
        else begin
          MsgBox(Handle,491,mb_IconExclamation or mb_Ok,'');
          if TempVal <= 0 then SetFocus(GetItemHandle(id_WidthVal))
          else SetFocus(GetItemHandle(id_HeightVal));
        end;
      end
      else begin
        MsgBox(Handle,210,mb_IconExclamation or mb_Ok,'');
        if GetFieldLong(id_WidthVal,TempVal) then SetFocus(GetItemHandle(id_HeightVal))
        else SetFocus(GetItemHandle(id_WidthVal));
      end;
    end;
    if AllParams then begin
      Count:=0;
      while (Count<Info^.Count) and (PLayerInfo(Info^.At(Count))^.Name<>NIL) do Inc(Count);
      if Count<Info^.Count then begin
        LayerScroller.SetPosition(Count);
        VScroll;
        SetFocus(GetItemHandle(110+Count-FirstLine+2));
        MsgBox(Handle,492,mb_Ok or mb_IconExclamation,'');
      end
      else if GetLayer(id_LayListBox,ALayer,946,1025,10128,ilTop) and inherited CanClose then begin
        CopyTo^:=Layers^.IndexToLayer(ALayer);
        DiagType^:=LastType;
        if LastType = dtCircleDiag then begin
          CircleDiameter^:=TempCDiameter;
          if IsDlgButtonChecked(Handle,id_CircAbs) = 1 then DiamAbsRel^:=TRUE
          else DiamAbsRel^:=FALSE;
        end
        else begin
          BeamWidth^:=TempBWidth;
          BeamHeight^:=TempBHeight;
        end;
        PLAPColor^:=PLADColor;
        PLAPPen^:=PLADPen;
        PLAPPattern^:=PLADPattern;
        if IsDlgButtonChecked(Handle,id_Legend) = 1 then ShowValues^:=TRUE
        else ShowValues^:=FALSE;
        if IsDlgButtonChecked(Handle,id_NewPos) = 1 then NewPos^:=TRUE
        else NewPos^:=FALSE;
        CanClose:=TRUE;
      end;
    end;
  end;

{*****************************************************************************************************************************}
{**************************************** TDColFontDlg ***********************************************************************}
{*****************************************************************************************************************************}

Constructor TDColFontDlg.Init
   (
   AParent         : TComponent;
   APInfo          : PPaint;
   AFontData       : PFontData;
   AVAbsRel        : PBool;
   AName           : PChar
   );
  begin
    inherited Init(AParent,APInfo,AFontData,AName,TRUE);
    ValAbsRel:=AVAbsRel;
  end;

{*****************************************************************************************************************************}

Procedure TDColFontDlg.SetupWindow;
  begin
    inherited SetupWindow;
    if ValAbsRel^ then CheckDlgButton(Handle,id_Abs,1)
    else CheckDlgButton(Handle,id_Rel,1);
  end;

{*****************************************************************************************************************************}

Function TDColFontDlg.CanClose
   : Boolean;
  var Cnt          : Integer;
      AFName       : Array[0..255] of Char;
  begin
    with Data^ do begin
      Style:=0;
      if IsDlgButtonChecked(Handle,id_Bold)=1 then Style:=Style or ts_Bold;
      if IsDlgButtonChecked(Handle,id_Underl)=1 then Style:=Style or ts_Underl;
      if IsDlgButtonChecked(Handle,id_Italic)=1 then Style:=Style or ts_Italic;
      if IsDlgButtonChecked(Handle,id_Transparent)=1 then Style:=Style or ts_Transparent;
      Style:=Style and not ts_FixHeight;
      Style:=Style or ts_Left;
      Cnt:=SendDlgItemMsg(id_FontList,cb_GetCurSel,0,0);
      if Cnt>=0 then begin
        SendDlgItemMsg(id_FontList,cb_GetLBText,Cnt,LongInt(@AFName));
        Font:=PInfo^.Fonts^.IDFromName(StrPas(AFName));
      end
      else Font:=0;
    end;
    if IsDlgButtonChecked(Handle,id_Abs) = 1 then ValAbsRel^:=TRUE
    else ValAbsRel^:=FALSE;
    CanClose:=TRUE;
  end;

{*****************************************************************************************************************************}
{**************************************** TDColTypeDlg ***********************************************************************}
{*****************************************************************************************************************************}

Constructor TDColTypeDlg.Init
   (
   AParent         : TComponent;
   AType           : Byte;
   ANewType        : PByte
   );
  begin
    inherited Init(AParent,'COLUMNTYPECHANGE');
    OldType:=AType;
    NewType:=ANewType;
  end;

{*****************************************************************************************************************************}

Procedure TDColTypeDlg.SetupWindow;
  var Temp         : Array [0..200] of Char;
      Count        : Word;
      AIndex       : Integer;
      AStyle       : LongInt;
      DoDisable    : Boolean;
      BText        : String;
  begin
    inherited SetupWindow;
    if OldType = dtCircleDiag then begin
      BText:=GetLangText(493);
      StrPCopy(Temp,BText);
      SetDlgItemText(Handle,id_ReadLine1,Temp);
      BText:=GetLangText(494);
      StrPCopy(Temp,BText);
      SetDlgItemText(Handle,id_ReadLine2,Temp);
      ShowWindow(GetItemHandle(id_ADAsLD),SW_Hide);
      ShowWindow(GetItemHandle(id_ADsAsLDs),SW_Hide);
    end
    else begin
      BText:=GetLangText(475);
      StrPCopy(Temp,BText);
      SetDlgItemText(Handle,id_ReadLine1,Temp);
      BText:=GetLangText(476);
      StrPCopy(Temp,BText);
      SetDlgItemText(Handle,id_ReadLine2,Temp);
      ShowWindow(GetItemHandle(id_CDAsBD),SW_Hide);
      ShowWindow(GetItemHandle(id_CDsAsBDs),SW_Hide);
    end;
    if NewType^ = ntCDNot then CheckDlgButton(Handle,id_CDNot,1)
    else if NewType^ = ntCDAsBD then begin
      if OldType = dtCircleDiag then CheckDlgButton(Handle,id_CDAsBD,1)
      else CheckDlgButton(Handle,id_ADAsLD,1);
    end
    else CheckDlgButton(Handle,id_CDNot,1);
  end;

{*****************************************************************************************************************************}

Function TDColTypeDlg.CanClose
   : Boolean;
  begin
    if IsDlgButtonChecked(Handle,id_CDsAsBDs) = 1 then NewType^:=ntCDsAsBDs
    else if IsDlgButtonChecked(Handle,id_ADsAsLDs) = 1 then NewType^:=ntCDsAsBDs
    else if IsDlgButtonChecked(Handle,id_CDAsBD) = 1 then NewType^:=ntCDAsBD
    else if IsDlgButtonChecked(Handle,id_ADAsLD) = 1 then NewType^:=ntCDAsBD
    else if IsDlgButtonChecked(Handle,id_CDsNever) = 1 then NewType^:=ntCDsNever
    else NewType^:=ntCDNot;
    CanClose:=TRUE;
  end;

{*****************************************************************************************************************************}
{**************************************** TDCTSizeDlg ************************************************************************}
{*****************************************************************************************************************************}

Constructor TDCTSizeDlg.Init
   (
   AParent         : TComponent;
   ABHeight        : PLongInt;
   ABWidth         : PLongInt
   );
  begin
    inherited Init(AParent,'COLUMNTCSIZE');
    BeamHeight:=ABHeight;
    BeamWidth:=ABWidth;
  end;

{*****************************************************************************************************************************}

Procedure TDCTSizeDlg.SetupWindow;
  begin
    inherited SetupWindow;
    SetFieldLongZero(id_WidthVal,BeamWidth^);
    SetFieldLongZero(id_HeightVal,BeamHeight^);
  end;

{*****************************************************************************************************************************}

Function TDCTSizeDlg.CanClose
   : Boolean;
  var TempVal       : LongInt;
      TempVal1      : LongInt;
  begin
    CanClose:=FALSE;
    TempVal:=-1;
    TempVal1:=-1;
    if GetFieldLong(id_WidthVal,TempVal) and GetFieldLong(id_HeightVal,TempVal1) then begin
      if (TempVal > 0) and (TempVal1 > 0) then begin
        BeamWidth^:=TempVal;
        BeamHeight^:=TempVal1;
        CanClose:=TRUE;
      end
      else begin
        MsgBox(Handle,491,mb_IconExclamation or mb_Ok,'');
        if TempVal <= 0 then SetFocus(GetItemHandle(id_WidthVal))
        else SetFocus(GetItemHandle(id_HeightVal));
      end;
    end
    else begin
      MsgBox(Handle,210,mb_IconExclamation or mb_Ok,'');
      if GetFieldLong(id_WidthVal,TempVal) then SetFocus(GetItemHandle(id_HeightVal))
      else SetFocus(GetItemHandle(id_WidthVal));
    end;
  end;

{*****************************************************************************************************************************}
{**************************************** TDSelFontLayer *********************************************************************}
{*****************************************************************************************************************************}


Constructor TDSelFontLayer.Init
   (
   AParent         : TComponent;
   ALayers         : PLayers;
   ACopy           : PPLayer;
   ACollInfo       : PCollection;
   APInfo          : PPaint;
   AData           : PFontData;
   ANewPos         : PBool;
   AName           : PChar;
   AShowNewPos     : Boolean
   );
  begin
    inherited Init(AParent,APInfo,AData,AName,TRUE);
    CopyTo:=ACopy;
    Data:=AData;
    CollInfo:=ACollInfo;
    EditFocus:=1;
    LayerScroller:=TRScrollBar.InitResource(Self,171);
    PInfo:=APInfo;
    Layers:=ALayers;
    NewPos:=ANewPos;
    ShowNewPos:=AShowNewPos;
  end;

{*****************************************************************************************************************************}

Procedure TDSelFontLayer.SetupWindow;
  var
      AIndex       : Integer;
      LayerName    : Array[0..255] of Char;
      Count        : Word;
      Temp         : Array[0..200] of Char;
  begin
    inherited SetupWindow;
    FillLayerList(id_LayListBox,FALSE,TRUE);
    SendDlgItemMsg(id_LayListBox,cb_SetCurSel,0,0);
    StrPCopy(LayerName,(CopyTo^)^.Text^);
    SendDlgItemMsg(id_LayListBox,cb_SelectString,-1,LongInt(@LayerName));
    if CollInfo^.Count<10 then
      LayerScroller.SetRange(1,1)
    else
      LayerScroller.SetRange(1,CollInfo^.Count-9);
    LayerScroller.SetPosition(1);
    Count:=1;
    FirstLine:=1;
    while (Count<=10) and (Count<=CollInfo^.Count) do begin
      SetDlgItemText(Handle,110+Count,PAnnot(CollInfo^.At(Count-1))^.LineName);
      SetDlgItemText(Handle,160+Count,PAnnot(CollInfo^.At(Count-1))^.LineValue);
      Inc(Count);
    end;
    for Count:=CollInfo^.Count+1 to 10 do begin
      EnableWindow(GetItemHandle(110+Count),FALSE);
      EnableWindow(GetItemHandle(160+Count),FALSE);
    end;
    if not ShowNewPos then EnableWindow(GetItemHandle(id_NewPos),FALSE);
  end;

{*****************************************************************************************************************************}

Procedure TDSelFontLayer.IDButtonPlus
   (
   var Msg : TMessage
   );
  var Count : integer;
      Temp : array [0..200] of char;
  begin
    if EditFocus<>0 then begin
      if EditFocus>1 then begin
        CollInfo^.AtInsert((FirstLine+EditFocus-1) ,CollInfo^.At(FirstLine+EditFocus-3));
        CollInfo^.AtDelete(FirstLine+EditFocus-3);
        for Count:=EditFocus-1 to EditFocus do SetDlgItemText(Handle,110+Count,
                                                              PAnnot(CollInfo^.At(FirstLine+Count-2))^.LineName);
        for Count:=EditFocus-1 to EditFocus do SetDlgItemText(Handle,160+Count,
                                                              PAnnot(CollInfo^.At(FirstLine+Count-2))^.LineValue);
        Dec(EditFocus);
      end
      else begin
        if EditFocus+FirstLine-1>1 then begin
          CollInfo^.AtInsert((FirstLine+EditFocus-1) ,CollInfo^.At(FirstLine+EditFocus-3));
          CollInfo^.AtDelete(FirstLine+EditFocus-3);
          LayerScroller.SetPosition(FirstLine-1);
          VScroll;
        end;
      end;
    end;
  end;

{*****************************************************************************************************************************}

Procedure TDSelFontLayer.IDButtonMinus
   (
   var Msg : TMessage
   );
  var Count : integer;
      Temp : array [0..200] of char;
  begin
    if EditFocus<>0 then begin
      if EditFocus<10 then begin
        if FirstLine+EditFocus-1<CollInfo^.Count then begin
          CollInfo^.AtInsert((FirstLine+EditFocus-2) ,CollInfo^.At(FirstLine+EditFocus-1));
          CollInfo^.AtDelete(FirstLine+EditFocus);
          for Count:=EditFocus to EditFocus+1 do SetDlgItemText(Handle,110+Count,
                                                                PAnnot(CollInfo^.At(FirstLine+Count-2))^.LineName);
          for Count:=EditFocus to EditFocus+1 do SetDlgItemText(Handle,160+Count,
                                                                PAnnot(CollInfo^.At(FirstLine+Count-2))^.LineValue);
          Inc(EditFocus);
        end;
      end
      else begin
        if EditFocus+FirstLine-1<CollInfo^.Count then begin
          CollInfo^.AtInsert((FirstLine+EditFocus-2) ,CollInfo^.At(FirstLine+EditFocus-1));
          CollInfo^.AtDelete(FirstLine+EditFocus);
          LayerScroller.SetPosition(FirstLine+1);
          VScroll;
        end;
      end;
    end;
  end;

{*****************************************************************************************************************************}

Procedure TDSelFontLayer.WMVScroll
   (
   var Msg : TMessage
   );
  begin
    inherited ;
    VScroll;
  end;

{*****************************************************************************************************************************}

Procedure TDSelFontLayer.VScroll;
  var Pos   : Integer;
      Count : Integer;
      Temp  : Array [0..200] of Char;
      Temp1 : Array [0..200] of Char;
  begin
    Pos:=LayerScroller.GetPosition;
    if FirstLine<>Pos then begin
      FirstLine:=Pos;
      Count:=Pos;
      while (Count<=Pos+9) do begin
        if Count<=CollInfo^.Count then begin
          if CollInfo^.At(Count-1)<>NIL then begin
            StrCopy(Temp,PAnnot(CollInfo^.At(Count-1))^.LineName);
            if PAnnot(CollInfo^.At(Count-1))^.LineValue <> NIL then
              StrCopy(Temp1,PAnnot(CollInfo^.At(Count-1))^.LineValue)
            else Temp1[0]:=#0;
          end
          else begin
            Temp[0]:=#0;
            Temp1[0]:=#0;
          end;
          SetDlgItemText(Handle,110+Count-Pos+1,@Temp);
          SetDlgItemText(Handle,160+Count-Pos+1,@Temp1);
        end
        else begin
          SetDlgItemText(Handle,110+Count-Pos+1,'');
          EnableWindow(GetItemHandle(110+Count-Pos+1),FALSE);
          SetDlgItemText(Handle,160+Count-Pos+1,'');
          EnableWindow(GetItemHandle(160+Count-Pos+1),FALSE);
        end;
        Inc(Count);
      end;
    end;
  end;

{*****************************************************************************************************************************}

Procedure TDSelFontLayer.WMCommand
   (
   var Msg : TWMCommand
   );
  var Text           : Array [0..200] of Char;
      PText          : String;
      Return         : Integer;
      ColorType      : Integer;
      NewColor       : TColorRef;
      NewType        : Word;
      NewTransparent : Boolean;
  begin
    Inherited WMCommand(Msg);
    case Msg.NotifyCode of
      en_Change :  begin
        if (Msg.ItemID<=120) and (Msg.ItemID>=111) then begin
          if CollInfo^.Count>=Msg.ItemID-110 then begin
            Return:=GetDlgItemText(Handle,Msg.ItemID,@Text,200);
            StrDispose(PAnnot(CollInfo^.At(Msg.ItemID+FirstLine-112))^.LineName);
            PAnnot(CollInfo^.At(Msg.ItemID+FirstLine-112))^.LineName:=StrNew(Text);
          end;
        end;
        if (Msg.ItemID<=170) and (Msg.ItemID>=161)
                             and (PAnnot(CollInfo^.At(Msg.ItemID+FirstLine-162))^.LineValue <> NIL) then begin
          if CollInfo^.Count>=Msg.ItemID-160 then begin
            Return:=GetDlgItemText(Handle,Msg.ItemID,@Text,200);
            StrDispose(PAnnot(CollInfo^.At(Msg.ItemID+FirstLine-162))^.LineValue);
            PAnnot(CollInfo^.At(Msg.ItemID+FirstLine-162))^.LineValue:=StrNew(Text);
          end;
        end;
      end;
      en_SetFocus : begin
        if (Msg.ItemID<=120) and (Msg.ItemID>=111) then EditFocus:=Msg.ItemID-110
        else if (Msg.ItemID<=170) and (Msg.ItemID>=161) then EditFocus:=Msg.ItemID-160;
      end;
    end;
  end;

{*****************************************************************************************************************************}

Function TDSelFontLayer.CanClose
   : Boolean;
  var Count         : Integer;
      ALayer        : LongInt;
  begin
    CanClose:=FALSE;
    Count:=0;
    while (Count<CollInfo^.Count) and (CollInfo^.At(Count)<>NIL) do Inc(Count);
    if Count<CollInfo^.Count then begin
      LayerScroller.SetPosition(Count);
      VScroll;
      SetFocus(GetItemHandle(110+Count-FirstLine+2));
      MsgBox(Handle,3100,mb_Ok or mb_IconExclamation,'');
    end
    else if GetLayer(id_LayListBox,ALayer,946,1025,10128,ilTop)
       and inherited CanClose then begin
{      if not Layers^.TranspLayer then Layers^.DetermineTopLayer;}
      CopyTo^:=Layers^.IndexToLayer(ALayer);
      if IsDlgButtonChecked(Handle,id_NewPos) = 1 then NewPos^:=TRUE;
      CanClose:=TRUE;
    end;
  end;

{*****************************************************************************************************************************}
{**************************************** TDZoomOpt **************************************************************************}
{*****************************************************************************************************************************}

Constructor TDZoomOpt.Init
   (
   AParent         : TComponent;
   AValue          : PLongInt
   );
  begin
    inherited Init(AParent,'ZOOMFACT');
    Value:=AValue;
  end;

{*****************************************************************************************************************************}

Procedure TDZoomOpt.SetupWindow;
  begin
    inherited SetupWindow;
    SetFieldLongZero(100,Value^);
  end;

{*****************************************************************************************************************************}

Function TDZoomOpt.CanClose
   : Boolean;
  var
    NewVal  : LongInt;
  begin
    CanClose:=FALSE;
    if GetFieldLong(100,NewVal) then begin
      if NewVal >= 0 then begin
        Value^:=NewVal;
        CanClose:=TRUE;
      end
      else MsgBox(Handle,10412,mb_Ok or mb_IconExclamation,'');
    end
    else MsgBox(Handle,10413,mb_Ok or mb_IconExclamation,'');
  end;

{*****************************************************************************************************************************}
end.
