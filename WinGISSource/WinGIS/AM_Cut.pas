{****************************************************************************}
{ Unit AM_Cut4                                                               }
{----------------------------------------------------------------------------}
{ - Dialog zur Auswahl der Layer f�r das Fl�chenverschneiden                 }
{ - Routinen zum Fl�chenverschneiden                                         }
{----------------------------------------------------------------------------}
{ Literatur: [1] Computer Graphics, Foley-van Dam-Feiner-Hughes              }
{            [2] Computer Graphics, F.S. Hill Jr.                            }
{----------------------------------------------------------------------------}
{ Verwendet wird der Weiler-Polygonclipping Algorithmus ([1] S. 937)         }
{----------------------------------------------------------------------------}
{ Bedingte Kompilierung                                                      }
{  TESTING         = Pr�froutinen werden aktiviert.                          }
{----------------------------------------------------------------------------}
{ Modifikationen                                                             }
{  16.02.1994 Martin Forst                                                   }
{  07.04.1994 Martin Forst   Error-Layer, Polygonpr�fung hinzugef�gt         }
{  21.06.1994 Martin Forst   Double-Koordinaten f�r Punkte, Schnittpunkte    }
{   vorher berechnen, Schnittpunktberechnung in Parameterform                }
{  29.09.2005 Mariacher      Improve CheckInside method.
{----------------------------------------------------------------------------}
{ m�gliche Verbesserungen                                                    }
{  1) schnellerer Algorithmus f�r Schnittpunktberechnung in CutContours      }
{  2) CheckPoly in 1) inkludieren                                            }
{  3) nicht gesamtes Poly in Kontour umwandeln, sondern nur bei den Schnitt- }
{     punkten (-> weniger Speicher, schnelleres MergeContour)                }
{****************************************************************************}
unit AM_Cut;

interface

uses Classes, SysUtils, Messages, WinProcs, WinTypes, AM_Admin, AM_CPoly, AM_Def, AM_Dlg1,
  AM_Index, AM_Layer, AM_Paint, AM_Poly, AM_StdDl,
{$IFNDEF WMLT}
  AM_DDE,
{$ENDIF}
  Controls, Objects, ProjStyle;

const { IDs der Dialogelemente                                               }
  id_Layer1 = 100; { Combobox 1. Layer                   }
  id_Layer2 = 101; { Combobox 2. Layer                   }
  id_Additive = 102; { Radiobutton additiv                 }
  id_Subtractive = 103; { Radiobutton subtraktiv              }
  id_CheckPolys = 104; { CheckBox Polygonpr�fung             }
  id_Destination = 105; { Combobox Ziellayer                  }
  id_Database = 106; { Datenbankverschneidung              }
  id_MergeParts = 107; { Ergebnissfl�chen zusammenfassen     }

      { Konstanten f�r TAreaCut.Action                                       }
  doAdditive = 0; { additive Verschneidung              }
  doSubtract = 1; { subtraktive Verschneidung           }

      { Konstanten f�r TAreaCutDat.CutOptions                                }
  cutCheckPolys = $01; { Polygone �berpr�fen                 }
  cutDatabase = $02; { Datenbankverschneidung durchf�hren  }
  cutMergeParts = $04; { Ergebnissfl�chen zusammenfassen     }

type TSide = Boolean; { zwei Seiten je Kante                }
  TDirection = Boolean; { zwei Richtungen je Kante            }

const X = $01;
  A = $02;
  B = $04;
  C = $08;
  D = $10;

type PEdge = ^TEdge;
  PCutEdge = ^TCutEdge;

  TMergeResult = (mrNoError, mrNoEdge, mrLoopDetected);

     {***********************************************************************}
     { Object TCPoint                                                        }
     {-----------------------------------------------------------------------}
     { Fl�chenverschneiden verwendet Punkte mit Double-Koordinaten           }
     {-----------------------------------------------------------------------}
     {   X              = X-Koordinate                                       }
     {   Y              = Y-Koordinate                                       }
     {***********************************************************************}
  PCPoint = ^TCPoint;
  TCPoint = record
    X: Double;
    Y: Double;
    UseCount: Integer;
  end;

     {***********************************************************************}
     { Object TCutEdgeList                                                   }
     {-----------------------------------------------------------------------}
     { Liste von TCutEdge, sortiert nach TCutEdge.Edge                       }
     {***********************************************************************}
  PCutEdgeList = ^TCutEdgeList;
  TCutEdgeList = object(TSortedCollection)
    constructor Init;
    function Compare(Key1, Key2: Pointer): Integer; virtual;
    function GetCutEdge(AEdge: PEdge): PCutEdge;
    function KeyOf(Item: Pointer): Pointer; virtual;
  end;

  PEdgeList = ^TEdgeList;
  TEdgeList = object(TSortedCollection)
    constructor Init;
    function Compare(Key1, Key2: Pointer): Integer; virtual;
  end;

  PCutVertex = ^TCutVertex;
  TCutVertex = object(TOldObject)
    Position: PCPoint;
    Edges: TEdgeList;
    constructor Init(APosition: PCPoint);
    destructor Done; virtual;
  end;

  PCutVertexList = ^TCutVertexList;
  TCutVertexList = object(TSortedCollection)
    function Compare(Key1, Key2: Pointer): Integer; virtual;
    function KeyOf(Item: Pointer): Pointer; virtual;
    function Add(Position: PCPoint): PCutVertex;
  end;

     {***********************************************************************}
     { Object TCutItem                                                       }
     {-----------------------------------------------------------------------}
     { Speichert einen Schnittpunkt auf einer Kante und den Verweis auf      }
     { die Kante des anderen Polys.                                          }
     {-----------------------------------------------------------------------}
     {   CutEdge        = Kante des anderen Polys                            }
     {   Cut1           = 1. Schnittpunkt mit CutEdge                        }
     {   Cut2           = 2. Schnittpunkt mit CutEdge oder NIL               }
     {   Distance       = Abstand vom Anfangspunkt der Kante (bei 2 Punkten  }
     {                    der kleinere Abstand)                              }
     {***********************************************************************}
  PCutItem = ^TCutItem;
  TCutItem = object(TOldObject)
    CutVertex: PCutVertex;
    Distance: Double;
    constructor Init(ACut: PCutVertex; const ADistance: Double);
  end;

     {***********************************************************************}
     { Object TCutList                                                       }
     {-----------------------------------------------------------------------}
     { Liste von PCutItem, sortiert nach TCutItem.Distance.                  }
     {***********************************************************************}
  PCutList = ^TCutList;
  TCutList = object(TSortedCollection)
    constructor Init;
    function Compare(A, B: Pointer): Integer; virtual;
  end;

     {***********************************************************************}
     { Object TCutEdge                                                       }
     {-----------------------------------------------------------------------}
     { Speichert alle Schnittpunkte einer Kante mit dem anderen Poly.        }
     {-----------------------------------------------------------------------}
     {   Edge           = Zugeh�rige Kante des Polys                         }
     {   Cuts           = Liste aller Schnittpunkte mit dem anderen Poly     }
     {***********************************************************************}
  TCutEdge = object(TOldObject)
    Edge: PEdge;
    CutList: TCutList;
    constructor Init(AEdge: PEdge);
    destructor Done; virtual;
  end;

  PEdgeSort = ^TEdgeSort;
  TEdgeSort = object(TOldObject)
    Edge: PEdge;
    Angle: Double;
    Direction: TDirection;
    constructor Init(AEdge: PEdge; const AAngle: Double; ADirection: TDirection);
  end;

  TEdgeSortList = object(TSortedCollection)
    constructor Init;
    function Compare(Key1, Key2: Pointer): Integer; virtual;
  end;

     {***********************************************************************}
     { Record TContour                                                       }
     {-----------------------------------------------------------------------}
     {   BelongsTo      = zu welchen Polys geh�rt die Kontour                }
     {   EntryEdge      = Anfangskante der Kontour                           }
     {   EntrySide      = Anfangskante ist rechts oder links                 }
     {***********************************************************************}
  PContour = ^TContour;
  TContour = record
    BelongsTo: Byte;
    EntryEdge: PEdge;
    EntrySide: TSide;
    PolyEdge: PEdge;
  end;

     {***********************************************************************}
     { Record TEdge                                                          }
     {-----------------------------------------------------------------------}
     {   Vertices       = Zeiger auf Anfangs- und Endpunkt der Kante         }
     {   NextPolyEdge   = naechste Kante im Originalpoly                     }
     {   EdgeLinks      = Verbindung zu den Kontour-Kanten                   }
     {   EdgeOwners     = Polygon zu dem die Kante gehoert                   }
     {   ContourPtr     = Zeiger auf die Kontour, wenn die Kante der         }
     {                    Beginn einer solchen ist, sonst NIL                }
     {***********************************************************************}
  TEdge = record
    Vertices: array[TDirection] of PCPoint;
    NextPolyEdge: PEdge;
    EdgeLinks: array[TSide, TDirection] of PEdge;
    EdgeOwners: array[TSide] of Byte;
    ContourPtr: array[TSide] of PContour;
{$IFDEF TESTING}
    number: integer;
{$ENDIF}
  end;

      {**********************************************************************}
      { Record TAreaCutDat                                                   }
      {----------------------------------------------------------------------}
      { Datenstruktur f�r den Dialog TAreaCutDlg.                            }
      {----------------------------------------------------------------------}
      {   Action             = auszuf�hrende Aktion (doXXXX)                 }
      {   Layer1             = Nummer des 1. Layers                          }
      {   Layer2             = Nummer des 2. Layers                          }
      {   CheckPolys         = Polygonpr�fung durchf�hren                    }
      {**********************************************************************}
  PAreaCutDat = ^TAreaCutDat;
  TAreaCutDat = record
    Action: Integer16;
    Layer1: LongInt;
    Layer2: LongInt;
    DestLayer: LongInt;
    CutOptions: Byte;
  end;

      {**********************************************************************}
      { Dialog TAreaCutDlg                                                   }
      {----------------------------------------------------------------------}
      { Dialog zur Auswahl der Layer f�r das Fl�chenverschneiden.            }
      {**********************************************************************}
  PAreaCutDlg = ^TAreaCutDlg;
  TAreaCutDlg = class(TLayerDlg)
  public
    Data: PAreaCutDat;
    constructor Init(AParent: TWinControl; ALayers: PLayers; AData: PAreaCutDat; AProjStyle: TProjectStyles);
    function CanClose: Boolean; override;
    procedure SetupWindow; override;
  end;

procedure FreePolyContour(ACont: PContour);

procedure ObjectCut(AProj: Pointer);
procedure AreaCut4(AProj: Pointer);
procedure AreaCut5(AProj: Pointer; L1, L2, L3: string);
procedure AreaCut6(AProj: Pointer; L1, L2, L3: string);
procedure AreaCut7(AProj: Pointer; ASourceLayer1, ASourceLayer2, ATargetLayer: LongInt;
  AAddSub: Integer; ACheckAreas, ADBOverlay, AMergeAreas: Boolean);
procedure AreaCutSelected(AProj: Pointer; DestLayerName: string; AAddSub: Integer);

function AreaCut2Polies(AProj: Pointer; APoly1, APoly2: PCPoly; DoAdditive, DoMerge: Boolean; AColl: PCollection): Integer;

function AngleDifference(Angle1: Double; Angle2: Double): Double;

type {***********************************************************************}
     { Collection TDisposeColl                                               }
     {-----------------------------------------------------------------------}
     { Collection, die ihre Eintr�ge mittels Dispose freigibt.               }
     {***********************************************************************}
  PDisposeColl = ^TDisposeColl;
  TDisposeColl = object(TCollection)
    procedure FreeItem(AItem: Pointer); virtual;
  end;

     {***********************************************************************}
     { Object TAreaCut                                                       }
     {-----------------------------------------------------------------------}
     {   Contours       = Liste aller Kontouren                              }
     {   Vertices       = Liste der Schnittpunkte (f�r Speicherfreigabe)     }
     {***********************************************************************}
  PAreaCut = ^TAreaCut;
  TAreaCut = object(TOldObject)
    Contours: TDisposeColl;
    Layers: PLayers;
    PInfo: PPaint;
    ErrorLayer: PLayer;
    CutEdges: TCutEdgeList;
    AObject: PCPoly;
    BObject: PCPoly;
    CutVertexList: TCutVertexList;
    AllObjTypes: Boolean;
    constructor Init(ALayers: PLayers; APInfo: PPaint);
    destructor Done; virtual;
    function AreaDivide(ACPoly: PCPoly; APoly: PPoly; var Result1, Result2: PCPoly): Boolean;
    function CalculateCuts(Cont1: PEdge; Cont2: PEdge): Integer;
    function CPolyToContour(APoly: PCPoly; InsideOwner, OutsideOwner: Byte): TPoint;
    procedure CutContours;
    procedure DoCut(const Data: TAreaCutDat);
    function DoCut2Polies(Poly1, Poly2: PCPoly; DoCutAdditive, DoMergeParts: Boolean; ResultColl: PCollection): Integer;
    procedure InsertContour(AEdge, APolyEdge: PEdge);
    function InsertOnLayer (ALayer: PLayer; AItem, Item1, Item2: PCPoly): Boolean; {sl}
    function InsertOnLayer2 (ALayer: PLayer; AItem, Item1, Item2: PIndex): Boolean; {sl}
    function MergeContours(AIndex, EndLoop: Integer; var APoly: PCPoly; var Owners: Byte): TMergeResult;
    function PolyToContour(APoly: PPoly; LeftOwner, RightOwner: Byte): TPoint;
  end;

     {++Brovak - for convert old island to new}
  TConvert2000 = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    Apolygon: PCollection;
    APolygons: PCollection;
    BasePolygon: Pcollection;
    TmpCollection: PCollection;
    AData: PDpoint;
    procedure ExtractPolyGons1(CurrentPoly: PCollection);
    function ConvertOldIslandAreaToVersion2000(APoly: PCPoly): PCpoly;
    function CreateIslandPolygonForOLd: pcpoly;
    constructor Create;
    destructor Destroy;
     {--Brovak --}

  end;

function CheckIfOldCpoly(CurrentPoly: PCPoly): boolean;

implementation

uses AM_Proj, AM_Point, Islands, ResDlg, UserIntf, ListHndl, AM_Main,
AM_Sym, AM_Text, AM_Circl, StyleDef, LayerHndl, BMPImage, ObjCutDlg, AM_View
{++ IDB_UNDO}
  , IDB_CallBacksDef
{-- IDB_UNDO}
  ;

{$IFDEF TESTING}
var outfile: Text;
  anumber: integer;
{$ENDIF}

const Right = TRUE; { rechtsseitige Kontour               }
  Left = FALSE; { linksseitige Kontour                }

  CW = TRUE; { clockwise                           }
  CCW = FALSE; { counter-clockwise                   }

  Tolerance = 1E-8; { Toleranz f�r Real-Vergleiche        }

 {+++Brovak For convert old to new island}
 Function CheckIfOldCpoly(CurrentPoly:PCPoly):boolean;
  var i,j:integer;
  begin;
  Result:=false;
  for i:=1 to CurrentPoly.Data.Count-1 do
   for j:=1 to CurrentPoly.Data.Count-1 do
     if i<>j then
       if  ((Pdpoint(CurrentPoly.Data.At(i))^.X=Pdpoint(CurrentPoly.Data.At(j))^.X )
        and (Pdpoint(CurrentPoly.Data.At(i))^.Y= Pdpoint(CurrentPoly.Data.At(j))^.Y))
         then
          if (Pdpoint(CurrentPoly.Data.At(i+1))^.X=Pdpoint(CurrentPoly.Data.At(j-1))^.X )
           and (Pdpoint(CurrentPoly.Data.At(i+1))^.Y= Pdpoint(CurrentPoly.Data.At(j-1))^.Y)
            then
             begin;
              Result:=true;
              Exit;
             end;

  end;
  {--Brovak}




{****************************************************************************}
{ Function VerticesEqual                                                     }
{----------------------------------------------------------------------------}
{ Bestimmt, ob die X- und Y-Koordinaten der zwei Punkte gleich sind.         }
{----------------------------------------------------------------------------}
{ Ergebnis: TRUE             = Punkte haben die gleiche Position             }
{           FALSE            = Punkte ungleich                               }
{****************************************************************************}

function VerticesEqual
  (
  Vertex1: PCPoint;
  Vertex2: PCPoint
  )
  : Boolean;
begin
  VerticesEqual := (Abs(Vertex1^.X - Vertex2^.X) < Tolerance)
    and (Abs(Vertex1^.Y - Vertex2^.Y) < Tolerance);
end;

function AddVertex
  (
  X: Double;
  Y: Double
  )
  : PCPoint;
var Point: PCPoint;
begin
  New(Point);
  Point^.X := X;
  Point^.Y := Y;
  Point^.UseCount := 0;
  Result := Point;
end;

{****************************************************************************}
{ Function VerticesEqual                                                     }
{----------------------------------------------------------------------------}
{ Bestimmt, ob die X- und Y-Koordinaten der zwei Punkte gleich sind.         }
{----------------------------------------------------------------------------}
{ Ergebnis: TRUE             = Punkte haben die gleiche Position             }
{           FALSE            = Punkte ungleich                               }
{****************************************************************************}

function VerticesEqual2
  (
  Vertex1: PDPoint;
  Vertex2: PDPoint
  )
  : Boolean;
begin
  VerticesEqual2 := (Vertex1^.X = Vertex2^.X)
    and (Vertex1^.Y = Vertex2^.Y);
end;

{****************************************************************************}
{ Function VertexIsEdgeEnd                                                   }
{----------------------------------------------------------------------------}
{ Ermittelt, ob APoint einer der Eckpunkte der Kante ist. Es werden die      }
{ Koordinaten verglichen.                                                    }
{----------------------------------------------------------------------------}
{ Ergebnis: 0                = APoint ist kein Eckpunkt von AEdge            }
{           1                = APoint ist 1. Eckpunkt (Vertices[CCW])        }
{           2                = APoint ist 2. Eckpunkt (Vertices[CW])         }
{****************************************************************************}

function VertexIsEdgeEnd
  (
  AEdge: PEdge;
  APoint: PCPoint
  )
  : Integer;
begin
  if VerticesEqual(AEdge^.Vertices[CCW], APoint) then
    VertexIsEdgeEnd := 1
  else if VerticesEqual(AEdge^.Vertices[CW], APoint) then
    VertexIsEdgeEnd := 2
  else VertexIsEdgeEnd := 0;
end;

{****************************************************************************}
{ Function SquareDistance                                                    }
{----------------------------------------------------------------------------}
{ Berechnet das Quadrat der Distanz der beiden Punkte.                       }
{----------------------------------------------------------------------------}
{ Ergebnis: DeltaX^2+DeltaY^2                                                }
{****************************************************************************}

function SquareDistance
  (
  AVertex: PCPoint;
  BVertex: PCPoint
  )
  : Double;
var DX: Double;
  DY: Double;
begin
  DX := AVertex^.X - BVertex^.X;
  DY := AVertex^.Y - BVertex^.Y;
  SquareDistance := DX * DX + DY * DY;
end;

{****************************************************************************}
{ Procedure InsertContour                                                    }
{----------------------------------------------------------------------------}
{ Fuegt fuer die Kante AEdge zwei Kontouren (Left, Right) hinzu, sofern      }
{ diese noch nicht existieren. Die Kontur-Zeiger der Kante werden auf die    }
{ entsprechenden Kontouren gesetzt.                                          }
{****************************************************************************}

procedure TAreaCut.InsertContour
  (
  AEdge: PEdge;
  APolyEdge: PEdge
  );
var AContour: PContour;
begin
  if AEdge^.ContourPtr[Right] = nil then begin
    New(AContour); { Rechte Kontour                 }
    Contours.Insert(AContour);
    AContour^.EntryEdge := AEdge;
    AContour^.EntrySide := Right;
    AContour^.PolyEdge := APolyEdge;
    AEdge^.ContourPtr[Right] := AContour;
  end;
  if AEdge^.ContourPtr[Left] = nil then begin
    New(AContour); { Linke Kontour                  }
    Contours.Insert(AContour);
    AContour^.EntryEdge := AEdge;
    AContour^.EntrySide := Left;
    AContour^.PolyEdge := nil;
    AEdge^.ContourPtr[Left] := AContour;
  end;
end;

{****************************************************************************}
{ Procedure FreePolyContour                                                  }
{----------------------------------------------------------------------------}
{ Gibt alle Edges eines Polygons frei. Es werden alle Edges in der           }
{ NextPolyEdge-Kette freigegeben.                                            }
{****************************************************************************}

procedure FreePolyContour
  (
  ACont: PContour
  );
var AEdge: PEdge;
  BEdge: PEdge;
begin
  if ACont^.PolyEdge <> nil then begin
    AEdge := ACont^.PolyEdge^.NextPolyEdge;
    ACont^.PolyEdge^.NextPolyEdge := nil;
    ACont^.PolyEdge := nil;
    repeat
      BEdge := AEdge;
      AEdge := AEdge^.NextPolyEdge;
      with BEdge^ do begin
        Dec(Vertices[CCW]^.UseCount);
        if Vertices[CCW]^.UseCount = 0 then Dispose(Vertices[CCW]);
        Dec(Vertices[CW]^.UseCount);
        if Vertices[CW]^.UseCount = 0 then Dispose(Vertices[CW]);
      end;
      Dispose(BEdge);
    until AEdge = nil;
  end;
end;

{****************************************************************************}
{ Function CalculateAngle                                                    }
{----------------------------------------------------------------------------}
{ Berechnet der Winkel des Vektors DX,DY zur X-Achse.                        }
{----------------------------------------------------------------------------}
{ Parameter:                                                                 }
{  DX              i = X-Komponente des Vektors                              }
{  DY              i = Y-Komponente des Vektors                              }
{----------------------------------------------------------------------------}
{ Ergebnis: Winkel in rad (0<=CalculateAngle<=2*Pi)                          }
{****************************************************************************}

function CalculateAngle
  (
  DX: Double;
  DY: Double
  )
  : Double;
var Beta: Double;
const Gr90 = Pi / 2;
  Gr180 = Pi;
  Gr270 = 3 * Pi / 2;
  Gr360 = 2 * Pi;
begin
  if DX = 0 then begin
    if DY > 0 then Beta := Gr90
    else Beta := Gr270;
  end
  else if DY = 0 then begin
    if DX >= 0 then Beta := 0
    else Beta := Gr180;
  end
  else begin
    Beta := ArcTan(DY / DX);
    if (DX >= 0) then Beta := Beta + Gr360
    else Beta := Beta + Gr180;
    if Beta >= Gr360 then Beta := Beta - Gr360;
  end;
  CalculateAngle := Beta;
end;

{****************************************************************************}
{ Function Angle                                                             }
{----------------------------------------------------------------------------}
{ Berechnet den Winkel der Vektors Vertex1->Vertex2 zur X-Achse.             }
{----------------------------------------------------------------------------}
{ Ergebnis: Winkel zur X-Achse, 0<=Angle<=2*Pi                               }
{****************************************************************************}

function Angle
  (
  Vertex1: PCPoint;
  Vertex2: PCPoint
  )
  : Double;
var DX: Double;
  DY: Double;
begin
  DX := Vertex2^.X - Vertex1^.X;
  DY := -Vertex2^.Y + Vertex1^.Y;
  Angle := CalculateAngle(DX, DY);
end;

{$IFDEF TESTING}

procedure writepoint
  (point: pCpoint
  );
begin
  with point^ do begin
    write(outfile, ' X: ', X: 0: 7, ' Y: ', Y: 0: 7);
  end;
end;

procedure writeedge
  (
  edge: PEdge
  );
  procedure writee
      (atext: string;
      aedge: pedge;
      writepoints: boolean
      );
  begin
    if aedge <> nil then with aedge^ do begin
        write(outfile, atext, ' - ', number);
        if writepoints then begin
          write(outfile, ' - CCW:');
          writepoint(vertices[ccw]);
          write(outfile, ' - CW:');
          writepoint(vertices[cw]);
        end;
        writeln(outfile);
      end
  end;
begin
  writee('Edge: ', edge, true);
  writee('L CW: ', edge^.EdgeLinks[Right, CW], false);
  writee('LCCW: ', edge^.EdgeLinks[Right, CCW], false);
  writee('R CW: ', edge^.EdgeLinks[Left, CW], false);
  writee('RCCW: ', edge^.EdgeLinks[Left, CCW], false);
  writeln(outfile);
  flush(outfile);
end;
{$ENDIF}

function CheckInside
  (
  PInfo: PPaint;
  AObject: PCPoly;
  BObject: PCPoly
  )
  : Boolean;
var CutCnt: Integer;
    CornerIdx:integer;
    MaxIdx:integer;
begin
  // Mariacher 28-09-2005 Check all corners to increase occurency
  CutCnt := 0;
  MaxIdx:=BObject^.Data^.Count;
  for CornerIdx:=0 to MaxIdx - 1 do
  begin
     if AObject^.PointInsideWithoutBorder(PInfo, PDPoint(BObject^.Data^.At(CornerIdx))^) then Inc(CutCnt);
  end;
  // CornerIdx:=0;
  // if AObject^.PointInside(PInfo, PDPoint(BObject^.Data^.At(CornerIdx))^) then Inc(CutCnt);
  // CornerIdx:=MaxIdx div 3;
  // if AObject^.PointInside(PInfo, PDPoint(BObject^.Data^.At(CornerIdx))^) then Inc(CutCnt);
  // CornerIdx:=2 * (MaxIdx div 3);
  // if AObject^.PointInside(PInfo, PDPoint(BObject^.Data^.At(CornerIdx))^) then Inc(CutCnt);
  CheckInside := CutCnt >= 2;
end;


{$IFDEF TESTING}

procedure PrintIslandStruct
  (
  Level: Integer;
  Island: PIslandStruct
  );
  procedure WriteLevel;
  var Cnt: Integer;
  begin
    for Cnt := 1 to Level do Write(OutFile, '  ');
  end;
var Cnt: Integer;
begin
  Writeln(outfile, '-------------');
  if Island^.Poly <> nil then for Cnt := 0 to Island^.Poly^.Data^.Count - 1 do with PDPoint(Island^.Poly^.Data^.At(Cnt))^ do begin
        writelevel;
        writeln(Outfile, X, ' ', Y);
      end;
  writeln(OutFile, 'BeginIslands');
  if Island^.Islands <> nil then PrintIslandStruct(Level + 1, Island^.Islands);
  writeln(Outfile, 'EndIslands');
  if Island^.Next <> nil then PrintIslandStruct(Level, Island^.Next);
  flush(outfile);
end;
{$ENDIF}

{****************************************************************************}
{ Function TAreaCut.PolyToContour                                            }
{----------------------------------------------------------------------------}
{ Erzeugt f�r das Polygon die ringverkettete Liste von PEdge. Der Weiler-    }
{ Algorithmus funktioniert nur mit Polygonen richtig, die im Uhrzeigersinn   }
{ (Wingis gegen den Uhrzeigersinn, da Bildschirmausgabe in Y-Richtung        }
{ gekippt ist) definiert sind. Andernfalls erh�lt man statt additiver die    }
{ subtraktive Verkn�pfung. Die Ermittlung des Umlaufsinns erfolgt durch      }
{ Berechnung der Fl�che. Esprechend dem Umlaufsinn werden die Labels         }
{ f�r die linke und rechte Kontour gesetzt.                                  }
{----------------------------------------------------------------------------}
{****************************************************************************}

function TAreaCut.CPolyToContour
  (
  APoly: PCPoly;
  InsideOwner: Byte;
  OutsideOwner: Byte
  )
  : TPoint;
  procedure DoPolyToContour
      (
      APoly: PCPoly;
      LabelInside: Byte;
      LabelOutside: Byte
      );
  var AEdge: PEdge;
    LastEdge: PEdge;
    Cnt: Integer;
    EntryEdge: PEdge;
    ALeft: Byte;
    ARight: Byte;
    LastVertex: PCPoint;
    FirstVertex: PCPoint;
  begin
    if APoly^.Direction then begin
      ALeft := LabelInside;
      ARight := LabelOutside;
    end
    else begin
      ALeft := LabelOutside;
      ARight := LabelInside;
    end;
    LastEdge := nil;
    LastVertex := AddVertex(PDPoint(APoly^.Data^.At(0))^.X, PDPoint(APoly^.Data^.At(0))^.Y);
    FirstVertex := LastVertex;
    with APoly^ do for Cnt := Data^.Count - 2 downto 0 do begin
        if not VerticesEqual2(Data^.At(Cnt), Data^.At(Cnt + 1)) then begin
          New(AEdge);
          with AEdge^ do begin
{$IFDEF TESTING}
            number := anumber;
            inc(anumber);
{$ENDIF}
            Vertices[CCW] := LastVertex;
            Inc(LastVertex^.UseCount);
            if Cnt = 0 then LastVertex := FirstVertex
            else LastVertex := AddVertex(PDPoint(Data^.At(Cnt))^.X, PDPoint(Data^.At(Cnt))^.Y);
            Vertices[CW] := LastVertex;
            Inc(LastVertex^.UseCount);
            EdgeLinks[Right, CCW] := LastEdge;
            EdgeLinks[Left, CCW] := LastEdge;
            if LastEdge <> nil then begin
              LastEdge^.EdgeLinks[Right, CW] := AEdge;
              LastEdge^.EdgeLinks[Left, CW] := AEdge;
              LastEdge^.NextPolyEdge := AEdge;
            end
            else EntryEdge := AEdge;
            EdgeOwners[Right] := ARight;
            EdgeOwners[Left] := ALeft;
            ContourPtr[Right] := nil;
            ContourPtr[Left] := nil;
          end;
          LastEdge := AEdge;
        end;
      end;
    if LastEdge <> nil then begin

    LastEdge^.EdgeLinks[Right, CW] := EntryEdge;
    LastEdge^.EdgeLinks[Left, CW] := EntryEdge;
    LastEdge^.NextPolyEdge := EntryEdge;
    EntryEdge^.EdgeLinks[Right, CCW] := LastEdge;
    EntryEdge^.EdgeLinks[Left, CCW] := LastEdge;
    InsertContour(EntryEdge, EntryEdge);

    end;
  end;
var IslandStruct: PIslandStruct;
  procedure IslandStructToContour
      (
      Island: PIslandStruct;
      IsIsland: Boolean
      );
  begin
    if Island <> nil then begin
      if not IsIsland then DoPolyToContour(Island^.Poly, InsideOwner, OutsideOwner)
      else DoPolyToContour(Island^.Poly, OutsideOwner, InsideOwner);
      IslandStructToContour(Island^.Next, IsIsland);
      IslandStructToContour(Island^.Islands, not IsIsland);
    end;
  end;
begin
  Result.X := Contours.Count div 2;
  if not APoly^.GetState(sf_IslandArea) then DoPolyToContour(APoly, InsideOwner, OutsideOwner)
  else begin
    IslandStruct := PolyToIslandStruct(PInfo, APoly);
    IslandStructToContour(IslandStruct, FALSE);
  end;
  Result.Y := Contours.Count div 2 - 1;
end;

function TAreaCut.PolyToContour
  (
  APoly: PPoly;
  LeftOwner: Byte;
  RightOwner: Byte
  )
  : TPoint;
var AEdge: PEdge;
  EntryEdge: PEdge;
  LastEdge: PEdge;
  LastVertex: PCPoint;
  Cnt: Integer;
begin
  Result.X := Contours.Count div 2;
  Cnt := 0;
  LastEdge := nil;
  LastVertex := AddVertex(PDPoint(APoly^.Data^.At(0))^.X, PDPoint(APoly^.Data^.At(0))^.Y); ;
  with APoly^ do while Cnt < Data^.Count - 1 do begin
      if VerticesEqual2(Data^.At(Cnt), Data^.At(Cnt + 1)) then Data^.AtFree(Cnt + 1)
      else begin
        New(AEdge);
        with AEdge^ do begin
{$IFDEF TESTING}
          number := anumber;
          inc(anumber);
{$ENDIF}
          Vertices[CCW] := LastVertex;
          Inc(LastVertex^.UseCount);
          LastVertex := AddVertex(PDPoint(Data^.At(Cnt + 1))^.X, PDPoint(Data^.At(Cnt + 1))^.Y);
          Vertices[CW] := LastVertex;
          Inc(LastVertex^.UseCount);
          EdgeLinks[Right, CCW] := LastEdge;
          EdgeLinks[Left, CCW] := LastEdge;
          if LastEdge <> nil then begin
            LastEdge^.EdgeLinks[Right, CW] := AEdge;
            LastEdge^.EdgeLinks[Left, CW] := AEdge;
            LastEdge^.NextPolyEdge := AEdge;
          end
          else EntryEdge := AEdge;
          EdgeOwners[Right] := RightOwner;
          EdgeOwners[Left] := LeftOwner;
          ContourPtr[Right] := nil;
          ContourPtr[Left] := nil;
        end;
        LastEdge := AEdge;
        Inc(Cnt);
      end;
    end;
  LastEdge^.EdgeLinks[Right, CW] := nil;
  LastEdge^.EdgeLinks[Left, CW] := nil;
  LastEdge^.NextPolyEdge := nil;
  InsertContour(EntryEdge, EntryEdge);
  Result.Y := Contours.Count div 2 - 1;
end;

{****************************************************************************}
{ Function CutLines                                                          }
{----------------------------------------------------------------------------}
{ Ermittelt die Schnittpunkte der Linien L1P1->L1P2 und L2P1->L2P2.          }
{ Algorithmus aus [2] Seite 131. Die Schnittpunkte werden in die             }
{ VertexList eingef�gt.                                                      }
{----------------------------------------------------------------------------}
{ L1P1             i = Anfangspunkt erste Linie                              }
{ L1P2             i = Endpunkt erste Linie                                  }
{ L2P1             i = Anfangspunkt zweite Linie                             }
{ L2P2             i = Endpunkt zweite Linie                                 }
{ Cut1             o = Zeiger auf 1. Schnittpunkt, ung�ltig wenn 0 Schnittp. }
{ Cut2             o = Zeiger auf 2. Schnittpunkt, ung�ltig wenn weniger     }
{                      als zwei Schnittpunkte                                }
{----------------------------------------------------------------------------}
{ Ergebnis: Anzahl der Schnittpunkte.                                        }
{****************************************************************************}

function CutLines
  (
  L1P1: PCPoint;
  L1P2: PCPoint;
  L2P1: PCPoint;
  L2P2: PCPoint;
  var Cut1: PCPoint;
  var Cut2: PCPoint
  )
  : Integer;
var D: Double;
  DX1: Double;
  DY1: Double;
  DX2: Double;
  DY2: Double;
  t0: Double;
  u0: Double;
  tc: Double;
  td: Double;
begin
  CutLines := 0;
  DX1 := L1P2^.X - L1P1^.X;
  DX2 := L2P2^.X - L2P1^.X;
  DY1 := L1P2^.Y - L1P1^.Y;
  DY2 := L2P2^.Y - L2P1^.Y;
  D := DX1 * DY2 - DY1 * DX2;
  if Abs(D) >= Tolerance then begin
    t0 := ((L2P1^.X - L1P1^.X) * DY2 - (L2P1^.Y - L1P1^.Y) * DX2) / D;
    if (t0 > -Tolerance) and (t0 < 1.0 + Tolerance) then begin
      if Abs(DX2) > Abs(DY2) then u0 := ((L1P1^.X - L2P1^.X) + DX1 * t0) / DX2
      else u0 := ((L1P1^.Y - L2P1^.Y) + DY1 * t0) / DY2;
      if (u0 > -Tolerance) and (u0 < 1.0 + Tolerance) then begin
        Cut1 := AddVertex(L1P1^.X + DX1 * t0, L1P1^.Y + DY1 * t0);
        CutLines := 1;
      end;
    end;
  end
  else if Abs(DX1 * (L2P1^.Y - L1P1^.Y) - DY1 * (L2P1^.X - L1P1^.X)) < Tolerance then begin
    if Abs(DX1) > Abs(DY1) then begin
      tc := (L2P1^.X - L1P1^.X) / DX1;
      td := (L2P2^.X - L1P1^.X) / DX1;
    end
    else if Abs(DY1) < Tolerance then Exit
    else begin
      tc := (L2P1^.Y - L1P1^.Y) / DY1;
      td := (L2P2^.Y - L1P1^.Y) / DY1;
    end;
    if ((tc > -Tolerance) and (td < 1.0 + Tolerance))
      or ((tc < 1.0 + Tolerance) and (td > -Tolerance)) then begin
      if tc < 0.0 then tc := 0.0
      else if tc > 1.0 then tc := 1.0;
      if td < 0.0 then td := 0.0
      else if td > 1.0 then td := 1.0;
      Cut1 := AddVertex(L1P1^.X + DX1 * tc, L1P1^.Y + DY1 * tc);
      if Abs(tc - td) < Tolerance then CutLines := 1
      else begin
        Cut2 := AddVertex(L1P1^.X + DX1 * td, L1P1^.Y + DY1 * td);
        CutLines := 2;
      end;
    end;
  end;
end;

{****************************************************************************}
{ Procedure SwitchLinks                                                      }
{----------------------------------------------------------------------------}
{ Ersetzt alle Links in CEdge mit FromEdge durch ToEdge.                     }
{****************************************************************************}

procedure SwitchLinks
  (
  CEdge: PEdge;
  FromEdge: PEdge;
  ToEdge: PEdge
  );
begin
  if CEdge <> nil then begin
    if CEdge^.EdgeLinks[Right, CCW] = FromEdge then
      CEdge^.EdgeLinks[Right, CCW] := ToEdge
    else if CEdge^.EdgeLinks[Right, CW] = FromEdge then
      CEdge^.EdgeLinks[Right, CW] := ToEdge;
    if CEdge^.EdgeLinks[Left, CCW] = FromEdge then
      CEdge^.EdgeLinks[Left, CCW] := ToEdge
    else if CEdge^.EdgeLinks[Left, CW] = FromEdge then
      CEdge^.EdgeLinks[Left, CW] := ToEdge;
  end;
end;

{$IFDEF TESTING}

procedure PrintCutEdges(Items: PCollection);
  procedure printcutedge
      (
      item: pcutedge
      ); far;
    procedure printitems
        (
        item: pcutitem
        ); far;
    begin
      with item^ do begin
        writeln(outfile, '-----------------------');
{          write(outfile,CutEdge^.Edge^.number);}
        writeln(outfile);
        writepoint(CutVertex^.Position);
        writeln(outfile);
        writeln(outfile, Distance: 0: 2);
      end;
    end;
  begin
    write(outfile, Item^.Edge^.number);
    writeln(outfile);
    item^.cutlist.ForEach(@printitems);
    writeln(outfile, '=======================');
  end;
begin
  Items^.ForEach(@printcutedge);
  flush(outfile);
end;
{$ENDIF}
{****************************************************************************}
{ Function InsertVertex                                                      }
{----------------------------------------------------------------------------}
{ Fuegt einen Punkt in die Kante Edge ein und stellt die entsprechenden      }
{ Links neu her.                                                             }
{****************************************************************************}

procedure InsertVertex
  (
  Edge: PEdge;
  Point: PCPoint
  );
var AEdge: PEdge;
begin
  New(AEdge);
  AEdge^ := Edge^;
  AEdge^.NextPolyEdge := Edge^.NextPolyEdge;
  Edge^.NextPolyEdge := AEdge;
  AEdge^.Vertices[CCW] := Point;
  Inc(Point^.UseCount);
  AEdge^.EdgeLinks[Right, CCW] := Edge;
  AEdge^.EdgeLinks[Left, CCW] := Edge;
  AEdge^.ContourPtr[Left] := nil;
  AEdge^.ContourPtr[Right] := nil;
{$IFDEF TESTING}
  AEdge^.number := anumber;
  inc(anumber);
{$ENDIF}
  SwitchLinks(Edge^.EdgeLinks[Left, CW], Edge, AEdge);
  SwitchLinks(Edge^.EdgeLinks[Right, CW], Edge, AEdge);
  Edge^.Vertices[CW] := Point;
  Inc(Point^.UseCount);
  Edge^.EdgeLinks[Right, CW] := AEdge;
  Edge^.EdgeLinks[Left, CW] := AEdge;
end;

{****************************************************************************}
{ Function AngleDifference                                                   }
{----------------------------------------------------------------------------}
{ Berechnet Angle1-CalculateAngle und korrigiert das Ergebnis auf            }
{ 0<=Angle<=2*Pi.                                                            }
{----------------------------------------------------------------------------}
{ Ergebnis: 0<=Angle1-Angle2<=2*Pi                                           }
{****************************************************************************}

function AngleDifference
  (
  Angle1: Double;
  Angle2: Double
  )
  : Double;
begin
  Angle1 := Angle1 - Angle2;
  if Angle1 < 0 then AngleDifference := Angle1 + 2 * Pi
  else AngleDifference := Angle1;
end;

{$IFDEF Testing}

procedure printcutlist(alist: PCollection);
  procedure doprint
      (
      aitem: PCutVertex
      ); far;
  var cnt: integer;
  begin
    with aitem^ do begin
      writeln(outfile, 'X=', position^.x: 0: 4, '  Y=', position^.y: 0: 4);
      for cnt := 0 to Edges.Count - 1 do begin
        write(outfile, PEdge(Edges.at(cnt))^.number, ' ');
      end;
      writeln(outfile);
      writeln(outfile, '-----------------');
    end;
  end;
begin
  alist^.foreach(@doprint);
  writeln(outfile, '===================');
  flush(outfile);
end;

{$ENDIF}

{****************************************************************************}
{ Procedure TAreaCut.CalculateContours                                       }
{----------------------------------------------------------------------------}
{ Berechnet alle Schnittpunkte der Polygone Cont1 und Cont2 und berechnet    }
{ die BResultierenden Kontouren.                                             }
{****************************************************************************}

function TAreaCut.CalculateCuts
  (
  Cont1: PEdge;
  Cont2: PEdge
  )
  : Integer;
var ACut: PCPoint;
  BCut: PCPoint;
  Cuts: Integer;
  AEdge: PEdge;
  BEdge: PEdge;
  ACutEdge: PCutEdge;
  BCutEdge: PCutEdge;
  ACutVertex: PCutVertex;
  procedure InsertCut
      (
      ACutEdge: PCutEdge;
      ACut: PCutVertex
      );
  var ADistance: Double;
  begin
    with ACutEdge^ do begin
      ADistance := SquareDistance(Edge^.Vertices[CCW], ACut^.Position);
      CutList.Insert(New(PCutItem, Init(ACut, ADistance)));
      ACut^.Edges.Insert(Edge);
    end;
  end;
begin
  Result := 0;
  if Cont1 = Cont2 then begin
    AEdge := Cont1;
    repeat
      ACutEdge := nil;
      BEdge := AEdge^.NextPolyEdge;
      if (BEdge <> Cont1) and (BEdge <> nil) then begin
        BEdge := BEdge^.NextPolyEdge;
        if (BEdge <> Cont1) and (BEdge <> nil) then repeat
            Cuts := CutLines(AEdge^.Vertices[CCW], AEdge^.Vertices[CW],
              BEdge^.Vertices[CCW], BEdge^.Vertices[CW], ACut, BCut);
            if Cuts <> 0 then begin
              Inc(Result, Cuts);
              if ACutEdge = nil then ACutEdge := CutEdges.GetCutEdge(AEdge);
              BCutEdge := CutEdges.GetCutEdge(BEdge);
              ACutVertex := CutVertexList.Add(ACut);
              InsertCut(ACutEdge, ACutVertex);
              InsertCut(BCutEdge, ACutVertex);
              if Cuts = 2 then begin
                ACutVertex := CutVertexList.Add(BCut);
                InsertCut(ACutEdge, ACutVertex);
                InsertCut(BCutEdge, ACutVertex);
              end;
            end;
            BEdge := BEdge^.NextPolyEdge;
          until (BEdge = Cont1) or (BEdge = nil);
      end;
      AEdge := AEdge^.NextPolyEdge;
    until (AEdge = Cont1) or (AEdge = nil);
  end
  else begin
    AEdge := Cont1;
    repeat
      ACutEdge := nil;
      BEdge := Cont2;
      repeat
        Cuts := CutLines(AEdge^.Vertices[CCW], AEdge^.Vertices[CW],
          BEdge^.Vertices[CCW], BEdge^.Vertices[CW], ACut, BCut);
        if Cuts <> 0 then begin
          Inc(Result, Cuts);
          if ACutEdge = nil then ACutEdge := CutEdges.GetCutEdge(AEdge);
          BCutEdge := CutEdges.GetCutEdge(BEdge);
          ACutVertex := CutVertexList.Add(ACut);
          InsertCut(ACutEdge, ACutVertex);
          InsertCut(BCutEdge, ACutVertex);
          if Cuts = 2 then begin
            ACutVertex := CutVertexList.Add(BCut);
            InsertCut(ACutEdge, ACutVertex);
            InsertCut(BCutEdge, ACutVertex);
          end;
        end;
        BEdge := BEdge^.NextPolyEdge;
      until (BEdge = Cont2) or (BEdge = nil);
      AEdge := AEdge^.NextPolyEdge;
    until (AEdge = Cont1) or (AEdge = nil);
  end;
end;

procedure TAreaCut.CutContours;
var ACutVertex: PCutVertex;
  ACutEdge: PCutEdge;
  ACutItem: PCutItem;
  AEdge: PEdge;
  Cnt: Integer;
  Cnt1: Integer;
  EdgeSort: TEdgeSortList;
  AEdgeSort: PEdgeSort;
  BEdgeSort: PEdgeSort;
  CEdgeSort: PEdgeSort;
  RemovedEdges: TEdgeList;
begin
  RemovedEdges.Init;
  for Cnt := 0 to CutEdges.Count - 1 do begin
    ACutEdge := CutEdges.At(Cnt);
    AEdge := ACutEdge^.Edge;
    with ACutEdge^ do for Cnt1 := 0 to CutList.Count - 1 do begin
        ACutItem := CutList.At(Cnt1);
        with ACutItem^ do case VertexIsEdgeEnd(AEdge, CutVertex^.Position) of
            0: with CutVertex^ do begin
                if AEdge <> Edge then begin
                  Edges.Delete(Edge);
                  Edges.Insert(AEdge);
                end;
                InsertVertex(AEdge, Position);
                AEdge := AEdge^.NextPolyEdge;
                Edges.Insert(AEdge);
              end;
            2: with CutVertex^ do begin
                if AEdge <> Edge then begin
                  Edges.Delete(Edge);
                  Edges.Insert(AEdge);
                end;
                Break;
              end;
          end;
      end;
  end;
  EdgeSort.Init;
  for Cnt := 0 to CutVertexList.Count - 1 do begin
    ACutVertex := CutVertexList.At(Cnt);
    with ACutVertex^ do for Cnt1 := 0 to Edges.Count - 1 do begin
        AEdge := Edges.At(Cnt1);
        case VertexIsEdgeEnd(AEdge, Position) of
          1: EdgeSort.Insert(New(PEdgeSort, Init(AEdge,
              Angle(AEdge^.Vertices[CCW], AEdge^.Vertices[CW]), CCW)));
          2: EdgeSort.Insert(New(PEdgeSort, Init(AEdge,
              Angle(AEdge^.Vertices[CW], AEdge^.Vertices[CCW]), CW)));
{$IFDEF TESTING}
        else writeln('Fehler 17');
{$ENDIF}
        end;
      end;
    Cnt1 := 0;
    while Cnt1 < EdgeSort.Count - 1 do begin
      AEdgeSort := EdgeSort.At(Cnt1);
      if RemovedEdges.IndexOf(AEdgeSort^.Edge) > -1 then EdgeSort.AtFree(Cnt1)
      else begin
        BEdgeSort := EdgeSort.At(Cnt1 + 1);
        if RemovedEdges.IndexOf(BEdgeSort^.Edge) > -1 then EdgeSort.AtFree(Cnt1 + 1)
        else if Abs(AEdgeSort^.Angle - BEdgeSort^.Angle) < Tolerance then begin
          with AEdgeSort^, Edge^ do begin
            EdgeOwners[not Direction] := EdgeOwners[not Direction] or
              BEdgeSort^.Edge^.EdgeOwners[not BEdgeSort^.Direction];
            EdgeOwners[Direction] := EdgeOwners[Direction] or
              BEdgeSort^.Edge^.EdgeOwners[BEdgeSort^.Direction];
          end;
          if BEdgeSort^.Edge^.ContourPtr[Left] <> nil then
            BEdgeSort^.Edge^.ContourPtr[Left]^.EntryEdge := nil;
          if BEdgeSort^.Edge^.ContourPtr[Right] <> nil then
            BEdgeSort^.Edge^.ContourPtr[Right]^.EntryEdge := nil;
          RemovedEdges.Insert(BEdgeSort^.Edge);
          EdgeSort.AtFree(Cnt1 + 1);
        end
        else Inc(Cnt1);
      end;
    end;
    CEdgeSort := EdgeSort.At(EdgeSort.Count - 1);
    AEdgeSort := EdgeSort.At(0);
    for Cnt1 := 1 to EdgeSort.Count do begin
      if Cnt1 = EdgeSort.Count then BEdgeSort := EdgeSort.At(0)
      else BEdgeSort := EdgeSort.At(Cnt1);
      with AEdgeSort^, Edge^ do begin
        InsertContour(Edge, nil);
        EdgeLinks[Right xor Direction, Direction] := CEdgeSort^.Edge;
        EdgeLinks[Left xor Direction, Direction] := BEdgeSort^.Edge;
      end;
      CEdgeSort := AEdgeSort;
      AEdgeSort := BEdgeSort;
    end;
    EdgeSort.FreeAll;
  end;
  EdgeSort.Done;
  CutEdges.Done;
  CutVertexList.FreeAll;
  RemovedEdges.DeleteAll;
  RemovedEdges.Done;
end;

{****************************************************************************}
{ Function TAreaCut.MergeContours                                            }
{----------------------------------------------------------------------------}
{ Erzeugt aus den Kontouren wieder Polygone. Die Ben�tigten Polys (je nach   }
{ Data.Action) werden in BResult eingef�gt, die restlichen wieder gel�scht.  }
{****************************************************************************}

function TAreaCut.MergeContours
  (
  AIndex: Integer;
  EndLoop: Integer;
  var APoly: PCPoly;
  var Owners: Byte
  )
  : TMergeResult;
var ACont: PContour;
  AEdge: PEdge;
  Direction: TDirection;
  Side: TSide;
  LastVertex: PCPoint;
  VertexCnt: Integer;
// ++ Cadmensky
  APoint: TDPoint;
// -- Cadmensky
begin
  ACont := Contours.At(AIndex);
  Owners := 0;
  AEdge := ACont^.EntryEdge;
  Result := mrNoEdge;
  if AEdge <> nil then begin
    APoly := New(PCPoly, Init);
    Side := ACont^.EntrySide;
    Direction := CW;
    LastVertex := AEdge^.Vertices[CCW];
    VertexCnt := 0;
    repeat
      if Direction = CW then begin
        if not VerticesEqual(LastVertex, AEdge^.Vertices[CCW]) then begin
          Direction := not Direction;
          Side := not Side;
          LastVertex := AEdge^.Vertices[CCW];
        end
        else LastVertex := AEdge^.Vertices[CW];
      end
      else begin
        if not VerticesEqual(LastVertex, AEdge^.Vertices[CW]) then begin
          Direction := not Direction;
          Side := not Side;
          LastVertex := AEdge^.Vertices[CW];
        end
        else LastVertex := AEdge^.Vertices[CCW];
      end;
      Owners := Owners or AEdge^.EdgeOwners[Side];
      if (AEdge^.ContourPtr[Side] <> nil) and (AEdge^.ContourPtr[Side] <> ACont) then
        AEdge^.ContourPtr[Side]^.EntryEdge := nil;
      AEdge := AEdge^.EdgeLinks[Side, Direction];
      Inc(VertexCnt);
      APoint.Init (Round(LastVertex^.X), Round(LastVertex^.Y));
      APoly^.Data^.Insert(@APoint);
      APoint.Done;
    until (AEdge = ACont^.EntryEdge) or (AEdge = nil) or (VertexCnt > EndLoop);
    if VertexCnt > EndLoop then Result := mrLoopDetected
    else Result := mrNoError;
  end;
end;

constructor TAreaCut.Init
  (
  ALayers: PLayers;
  APInfo: PPaint
  );
begin
  inherited Init;
  Layers := ALayers;
  PInfo := APInfo;
  Contours.Init(100, 10);
  CutEdges.Init;
  CutVertexList.Init(1000, 1000);
  AllObjTypes:=False;
end;

destructor TAreaCut.Done;
begin
  Contours.Done;
  CutEdges.Done;
  CutVertexList.Done;
  inherited Done;
end;

{*************************************************************************}
{ Procedure InsertOnLayer                                                 }
{-------------------------------------------------------------------------}
{ Ist das Objekt am Layer nocht nicht vorhanden, so wird ein neues        }
{ PIndex in den Layer eingef�gt.                                          }
{*************************************************************************}

function TAreaCut.InsertOnLayer (ALayer: PLayer; AItem, Item1, Item2: PCPoly): Boolean;
{++ IDB_UNDO}
var AProject: Pointer;
{-- IDB_UNDO}
begin
   Result := TRUE;
   if AItem^.Index = 0 then
   begin
      try
         Result := PLayer(PInfo^.Objects)^.InsertObject(PInfo, AItem, TRUE);
      except
         Result := FALSE;
      end;
   end;
   if Result then
      ALayer^.InsertObject(PInfo, New(PIndex, Init(AItem^.Index)), TRUE);
{++ IDB_UNDO}
  {$IFNDEF WMLT}
  {$IFNDEF AXDLL} // <----------------- AXDLL
   AProject := Layers.Parent;
   if WinGisMainForm.WeAreUsingTheIDB[AProject] then
   begin
      if (Item1 = nil) and (Item2 = nil) then
         WinGisMainForm.IDB_Man.InsertItem(AProject, ALayer, AItem)
      else
         WinGisMainForm.IDB_Man.OverlayInsertItem(AProject, ALayer, AItem.Index, Item1.Index, Item2.Index);
   end;
   if WinGISMainForm.WeAreUsingUndoFunction[AProject] then
      WinGISMainForm.UndoRedo_Man.AddObjectInUndoData(AProject, ALayer, AItem.Index);
  {$ENDIF} // <----------------- AXDLL
  {$ENDIF}
{-- IDB_UNDO}
end;

function TAreaCut.InsertOnLayer2 (ALayer: PLayer; AItem, Item1, Item2: PIndex): Boolean;
{++ IDB_UNDO}
var AProject: Pointer;
{-- IDB_UNDO}
begin
   Result := TRUE;
   if AItem^.Index = 0 then
   begin
      try
         Result := PLayer(PInfo^.Objects)^.InsertObject(PInfo, AItem, TRUE);
      except
         Result := FALSE;
      end;
   end;
   if Result then
      ALayer^.InsertObject(PInfo, New(PIndex, Init(AItem^.Index)), TRUE);
{++ IDB_UNDO}
  {$IFNDEF WMLT}
  {$IFNDEF AXDLL} // <----------------- AXDLL
   AProject := Layers.Parent;
   if WinGisMainForm.WeAreUsingTheIDB[AProject] then
   begin
      if (Item1 = nil) and (Item2 = nil) then
         WinGisMainForm.IDB_Man.InsertItem(AProject, ALayer, AItem)
      else
         WinGisMainForm.IDB_Man.OverlayInsertItem(AProject, ALayer, AItem.Index, Item1.Index, Item2.Index);
   end;
   if WinGISMainForm.WeAreUsingUndoFunction[AProject] then
      WinGISMainForm.UndoRedo_Man.AddObjectInUndoData(AProject, ALayer, AItem.Index);
  {$ENDIF} // <----------------- AXDLL
  {$ENDIF}
{-- IDB_UNDO}
end;

function CreateIslandStruct
  (
  APoly: PCPoly
  )
  : PIslandStruct;
begin
  New(Result);
  with Result^ do begin
    Poly := APoly;
    Islands := nil;
    Next := nil;
  end;
end;

{***************************************************************************}
{ Procedure TAreaCut.DoCut                                                  }
{---------------------------------------------------------------------------}
{ F�hrt die Fl�chenverschneidung aus. Bei additiver Verschneidung wird      }
{ jedes Poly der ersten mit jedem Poly des zweiten Layers verschnitten      }
{ und das Ergebnis jeder Verschneidung eingef�gt. Bei subtraktiver          }
{ Verschneidung werden die Ergebnispolys der Verschneidung mit dem          }
{ n�chsten Poly des zweiten Layers verschnitten usw..                       }
{ Ist der DestLayer<>Toplayer so wird das Begrenzungsrechteck aller         }
{ eingef�gten Ergebnispolys neu gezeichnet.                                 }
{***************************************************************************}

procedure TAreaCut.DoCut
  (
  const Data: TAreaCutDat
  );
{$IFNDEF WMLT}
var Layer1: PLayer; { 1. Layer                           }
  Layer2: PLayer; { 2. Layer                           }
  DestLayer: PLayer; { Ergebnislayer                      }
  Redraw: TDRect; { Begrenzung aller Ergebnispolys     }
  Search: Integer;
  Count: Integer;
  ObjCount: LongInt;
  AllObjects: LongInt;
  DDEString: string;
  TempString: string;
  {*************************************************************************}
  { Callback Procedure DoAll                                                }
  {-------------------------------------------------------------------------}
  { Wird f�r jedes Poly des 1. Layers aufgerufen.                           }
  {*************************************************************************}
  function DoAll
      (
      Item: PIndex
      )
      : Boolean; far;
    {***********************************************************************}
    { Procedure InsertPolys                                                 }
    {-----------------------------------------------------------------------}
    { F�gt die Polys aus der BResult-Liste in den DestLayer ein. Neue Polys }
    { werden auch in die Objektliste eingef�gt, f�r bereits existierende    }
    { werden die Statusflags f�r die Hirarchie neu berechnet.               }
    {***********************************************************************}
    function InsertCPoly
        (
        Item: PCPoly;
        DataBase: Boolean
        )
        : Boolean;
    var AText: string;
      BText: string;
      CText: string[10]; {2806F}
      DText: string[10]; {2806F}
    begin
      Result := FALSE;
      if Item^.Index <> 0 then begin
        Item := MakeObjectCopy(Item);
        Item^.Index := 0;
      end;
      if InsertOnLayer(DestLayer, Item, AObject, BObject) then
      begin
        Redraw.CorrectByRect(Item^.ClipRect);
        if Database then
        begin
// ++ Cadmensky
// -- Cadmensky
        try
          if Item^.GetObjType = ot_CPoly then
            Str(PCPoly(Item)^.Flaeche: 0: 2, BText)
          else
            BText := '0';

          if (AObject^.Flaeche > 0) then
             Str(100 * Item^.Flaeche / AObject^.Flaeche: 0: 2, CText) {2806F}
          else
             Str(0, CText); {2806F}

          if (BObject.Flaeche > 0) then
             Str(100 * Item^.Flaeche / BObject^.Flaeche: 0: 2, DText) {2806F}
          else
             Str(0, DText); {2806F}

        finally
          AText := Format('[CUT][%10d][%10d][%10d][%15s][%6s][%6s]',
            [AObject^.Index, BObject^.Index, Item^.Index, BText, CText, DText]);
  {$IFNDEF AXDLL} // <----------------- AXDLL
          DDEHandler.SendString(AText);
  {$ENDIF} // <----------------- AXDLL
        end;
       end;
        Result := TRUE;
      end;
    end;
    {***********************************************************************}
    { Callback Procedure CutAdditive                                        }
    {-----------------------------------------------------------------------}
    { Wird f�r jedes Poly des 2. Layers aufgerufen.                         }
    {***********************************************************************}
    function CutAdditive
        (
        Item: PIndex
        )
        : Boolean; far;
    var AOwners: Byte;
      APoly: TPoint; { Kontour zu AObject                 }
      BPoly: TPoint; { Kontour zu BObject                 }
      CPoly: PCPoly;
      Cnt: Integer;
      Cnt1: Integer;
      CutCnt: Integer;
      InsertPoly: Boolean;
      IslandList: PIslandStruct;
      TempList: PIslandStruct;

      //***************************
      procedure CutNonAreas;
      var
      APoint: TDPoint;
      APixel,NewPixel: PPixel;
      ASymbol,NewSymbol: PSymbol;
      AText,NewText: PText;
      ACircle,NewCircle: PEllipse;
      begin
           if Item^.GetObjType = ot_Pixel then begin
              APixel:=Pointer(Layer2^.IndexObject(PInfo,Item));
              APoint.Init(APixel^.Position.X,APixel^.Position.Y);
              if AObject^.PointInside(PInfo,APoint) then begin
                 NewPixel:=MakeObjectCopy(APixel);
                 NewPixel^.Index:=0;
                 NewPixel^.MPtr:=nil;
                 if not InsertOnLayer2(DestLayer,NewPixel,AObject,APixel) then begin
                    Dispose(NewPixel,Done);
                 end;
              end;
           end
           else if Item^.GetObjType = ot_Symbol then begin
              ASymbol:=Pointer(Layer2^.IndexObject(PInfo,Item));
              APoint.Init(ASymbol^.Position.X,ASymbol^.Position.Y);
              if AObject^.PointInside(PInfo,APoint) then begin
                 NewSymbol:=MakeObjectCopy(ASymbol);
                 NewSymbol^.Index:=0;
                 NewSymbol^.MPtr:=nil;
                 if not InsertOnLayer2(DestLayer,NewSymbol,AObject,ASymbol) then begin
                    Dispose(NewSymbol,Done);
                 end;
              end;
           end
           else if Item^.GetObjType = ot_Text then begin
              AText:=Pointer(Layer2^.IndexObject(PInfo,Item));
              APoint.Init(AText^.Pos.X,AText^.Pos.Y);
              if AObject^.PointInside(PInfo,APoint) then begin
                 NewText:=MakeObjectCopy(AText);
                 NewText^.Index:=0;
                 NewText^.MPtr:=nil;
                 if not InsertOnLayer2(DestLayer,NewText,AObject,AText) then begin
                    Dispose(NewText,Done);
                 end;
              end;
           end
           else if Item^.GetObjType = ot_Circle then begin
              ACircle:=Pointer(Layer2^.IndexObject(PInfo,Item));
              APoint.Init(ACircle^.Position.X,ACircle^.Position.Y);
              if AObject^.PointInside(PInfo,APoint) then begin
                 NewCircle:=MakeObjectCopy(ACircle);
                 NewCircle^.Index:=0;
                 NewCircle^.MPtr:=nil;
                 if not InsertOnLayer2(DestLayer,NewCircle,AObject,ACircle) then begin
                    Dispose(NewCircle,Done);
                 end;
              end;
           end;
      end;
      //***************************
    begin
      Result := FALSE;
      if ((Item^.GetObjType = ot_CPoly) or (Item^.GetObjType = ot_Image)) and (ErrorLayer^.HasObject(Item) = nil) then
      begin
        BObject := Pointer(Layer2^.IndexObject(PInfo, Item));

        if BObject^.GetObjType = ot_Image then begin
           PImage(BObject)^.UpdateBorder(PInfo);
           BObject:=PImage(BObject)^.Border;
        end;

        if (AObject^.Index = BObject^.Index) and (Layer1 <> Layer2) then
          InsertCPoly(AObject, Data.CutOptions and cutDatabase <> 0)
        else
          if AObject^.ClipRect.IsVisible(BObject^.ClipRect) then
          begin
            APoly := CPolyToContour(AObject, A, X);
            BPoly := CPolyToContour(BObject, B, X);
            { alle Teilkontouren miteinander verschneiden                      }

            CutCnt := AObject^.Data^.Count + BObject^.Data^.Count;
            //aig
            for Cnt := APoly.X to APoly.Y do begin
                for Cnt1 := BPoly.X to BPoly.Y do begin
                    Inc(CutCnt,CalculateCuts(PContour(Contours.At(Cnt * 2))^.EntryEdge,PContour(Contours.At(Cnt1 * 2))^.EntryEdge));
                end;
            end;
  {$IFDEF TESTING}
            printcutedges(@cutedges);
            printcutlist(@cutvertexList);
  {$ENDIF}
            CutContours;
            { Teilkontouren miteinander verschmelzen                           }
            IslandList := CreateIslandStruct(nil);
            for Cnt := 0 to Contours.Count - 1 do
            begin
              InsertPoly := FALSE;
              case MergeContours(Cnt, CutCnt, CPoly, AOwners) of
                mrLoopDetected: begin
                   if ((Data.CutOptions and cutCheckPolys) <> 0) then begin
                      InsertOnLayer(ErrorLayer, AObject, nil, nil);
                      InsertOnLayer(ErrorLayer, BObject, nil, nil);
                   end;
                  end;
                mrNoEdge: Continue;
                mrNoError: if AOwners and X = 0 then begin
                    CPoly^.ClosePoly;
                    CPoly^.CalculateClipRect;
                      //if CPoly^.Flaeche>=0.5 then begin
                    if CPoly^.Data^.Count > 2 then begin
                      if AOwners = A + B then InsertPoly := TRUE
                      else if AOwners = A then InsertPoly := CheckInside(PInfo, BObject, CPoly)
                      else if AOwners = B then InsertPoly := CheckInside(PInfo, AObject, CPoly);
                    end;
                  end;
              end;
              if InsertPoly then IslandList := InsertIsland(PInfo, IslandList,
                  CreateIslandStruct(CPoly))
              else Dispose(CPoly, Done);
            end;
            if Data.CutOptions and cutMergeParts = 0 then
            begin
              TempList := IslandList^.Islands;
//++ Glukhov Bug#162 BUILD#134 08.10.00
              TurnIslandOut(PInfo, TempList);
//-- Glukhov Bug#162 BUILD#134 08.10.00
              while TempList <> nil do
              begin
                if TempList^.Islands = nil then
                begin
                  if not InsertCPoly(TempList^.Poly, Data.CutOptions and cutDatabase <> 0) then
                  begin
                    Result := TRUE;
                    Break;
                  end;
                  TempList^.Poly := nil;
                end
                else
                  if not InsertCPoly(IslandStructToPoly(TempList, FALSE), Data.CutOptions and cutDatabase <> 0) then
                  begin
                    Result := TRUE;
                    Break;
                  end;
                TempList := TempList^.Next;
              end;
            end
            else
              if (IslandList^.Islands <> nil)
                and not InsertCPoly(IslandStructToPoly(IslandList, TRUE), Data.CutOptions and cutDatabase <> 0) then
                Result := TRUE;
            DestroyIslandStruct(IslandList, TRUE);
            for Cnt := 0 to Contours.Count - 1 do
              FreePolyContour(Contours.At(Cnt));
            Contours.FreeAll;
          end;
        end
        else if ((AllObjTypes) and (ErrorLayer^.HasObject(Item) = nil)) then begin
             CutNonAreas;
        end;

        Inc(ObjCount);
        StatusBar.Progress := 100 * ObjCount / AllObjects;
        Result := Result or StatusBar.QueryAbort;
      end;
    {***********************************************************************}
    { Callback Procedure CutSubtractive                                     }
    {-----------------------------------------------------------------------}
    { Wird f�r jedes Poly des 2. Layers bei subtraktiver Verschneidung      }
    { aufgerufen.                                                           }
    {***********************************************************************}
    function CutSubtractive
        : Boolean;
    var Count: LongInt;
      Cnt: Integer;
      Cnt1: Integer;
      Cnt2: Integer;
      CutCnt: Integer;
      IslandList: PIslandStruct;
      CPoly: PCPoly;
      AOwners: Byte;
      InsertPoly: Boolean;
      TempList: PIslandStruct;
      Results: PCollection;
      ResultsNew: PCollection;
      Temp: Pointer;
      CObject: PCPoly;
    begin
      Result := FALSE;
      Results := New(PCollection, Init(1000, 1000));
      ResultsNew := New(PCollection, Init(1000, 1000));
      Results^.Insert(AObject);
      for Count := 0 to Layer2^.Data^.GetCount - 1 do begin
        Item := Layer2^.Data^.At(Count);
        if (Item^.GetObjType = ot_CPoly)
          and (ErrorLayer^.HasObject(Item) = nil) then begin
          BObject := Pointer(Layer2^.IndexObject(PInfo, Item));
          if AObject^.ClipRect.IsVisible(BObject^.ClipRect) then begin
            for Cnt2 := 0 to Results^.Count - 1 do begin
              CObject := Results^.At(Cnt2);
                { Kontouren f�r die aktuellen Polys erzeugen                     }
              CPolyToContour(CObject, A, X);
              CPolyToContour(BObject, X, B);
              CutCnt := BObject^.Data^.Count + CObject^.Data^.Count;
                { Schnittpunkte berechnen                                        }
              for Cnt := 0 to Contours.Count div 2 - 2 do for Cnt1 := Cnt + 1 to Contours.Count div 2 - 1 do
                  Inc(CutCnt, CalculateCuts(PContour(Contours.At(Cnt * 2))^.EntryEdge,
                    PContour(Contours.At(Cnt1 * 2))^.EntryEdge));
                { Schnittpunkte einf�gen und Contour-Links updaten               }
              CutContours;
                { Teilkontouren miteinander verschmelzen                         }
              IslandList := CreateIslandStruct(nil);
              for Cnt := 0 to Contours.Count - 1 do begin
                InsertPoly := FALSE;
                case MergeContours(Cnt, CutCnt, CPoly, AOwners) of
                  mrLoopDetected: begin
                      if ((Data.CutOptions and cutCheckPolys) <> 0) then begin
                         InsertOnLayer(ErrorLayer, AObject, nil, nil);
                         InsertOnLayer(ErrorLayer, BObject, nil, nil);
                      end;
                    end;
                  mrNoEdge: Continue;
                  mrNoError: if AOwners and X = 0 then begin
                      CPoly^.ClosePoly;
                      CPoly^.CalculateClipRect;
                      //if CPoly^.Flaeche>=0.5 then begin
                      if CPoly^.Data^.Count > 2 then begin
                        if AOwners = A + B then InsertPoly := TRUE
                        else if AOwners = B then InsertPoly := CheckInside(PInfo, CObject, CPoly)
                        else if AOwners = A then InsertPoly := not CheckInside(PInfo, BObject, CPoly)
                      end;
                    end;
                end;
                if InsertPoly then IslandList := InsertIsland(PInfo, IslandList,
                    CreateIslandStruct(CPoly))
                else Dispose(CPoly, Done);
              end;
                { Ergebnispolygone erzeugen                                      }
              if Data.CutOptions and cutMergeParts = 0 then begin
                TempList := IslandList^.Islands;
//++ Glukhov Bug#162 BUILD#134 08.10.00
                TurnIslandOut(PInfo, TempList);
//-- Glukhov Bug#162 BUILD#134 08.10.00
                while TempList <> nil do begin
                  if TempList^.Islands = nil then begin
                    ResultsNew^.Insert(TempList^.Poly);
                    TempList^.Poly := nil;
                  end
                  else begin
                    ResultsNew^.Insert(IslandStructToPoly(TempList, FALSE));
                  end;
                  TempList := TempList^.Next;
                end;
              end
              else if (IslandList^.Islands <> nil) then
                ResultsNew^.Insert(IslandStructToPoly(IslandList, TRUE));
              DestroyIslandStruct(IslandList, TRUE);
                { Kontourenliste freigeben                                       }
              for Cnt := 0 to Contours.Count - 1 do
                FreePolyContour(Contours.At(Cnt));
              Contours.FreeAll;
            end;
              { Zwischenergebnis freigeben                                       }
            for Cnt := 0 to Results^.Count - 1 do begin
              CObject := Results^.At(Cnt);
              if CObject^.Index = 0 then Dispose(CObject, Done);
            end;
            Results^.DeleteAll;
            Temp := Results;
            Results := ResultsNew;
            ResultsNew := Temp;
          end;
        end;
        Inc(ObjCount);
        StatusBar.Progress := 100 * ObjCount / AllObjects;
        if StatusBar.QueryAbort then begin
          Result := TRUE;
          Break;
        end;
      end;
      for Cnt := 0 to Results^.Count - 1 do begin
        BObject := Results^.At(Cnt);
        if not InsertCPoly(BObject, Data.CutOptions and cutDatabase <> 0) then begin
          for Cnt1 := Cnt + 1 to Results^.Count - 1 do begin
            BObject := Results^.At(Cnt1);
            if BObject^.Index = 0 then Dispose(BObject, Done);
          end;
          Results^.DeleteAll;
          Result := TRUE;
          Break;
        end;
      end;
      Results^.DeleteAll;
      Dispose(Results, Done);
      Dispose(ResultsNew, Done);
    end;
  var Count1: Integer;
  begin
    DoAll := FALSE;
    if ((Item^.GetObjType = ot_CPoly) or (Item^.GetObjType = ot_Image)) and (ErrorLayer^.HasObject(Item) = nil) then begin
      AObject := Pointer(Layer1^.IndexObject(PInfo, Item));

      /////
      if AObject^.GetObjType = ot_Image then begin
           PImage(AObject)^.UpdateBorder(PInfo);
           AObject:=PImage(AObject)^.Border;
      end;
      /////

      if Data.Action = doAdditive then begin
        if Layer1 <> Layer2 then begin
          AllObjects := Layer1^.Data^.GetCount * Layer2^.Data^.GetCount;
          DoAll := Layer2^.Data^.FirstThat(@CutAdditive) <> nil;
        end
        else begin
          AllObjects := (Layer1^.Data^.GetCount * (Layer1^.Data^.GetCount - 1)) div 2;
          for Count1 := Count + 1 to Layer1^.Data^.GetCount - 1 do
            if CutAdditive(Layer1^.Data^.At(Count1)) then break;
          DoAll := Count1 < Layer1^.Data^.GetCount - 1;
        end;
      end
      else begin
        AllObjects := Layer1^.Data^.GetCount * Layer2^.Data^.GetCount;
        DoAll := CutSubtractive;
      end;
    end;
  end;
  function I2Str
      (
      I: Integer
      )
      : string;
  var OutStr: string;
  begin
    Str(I, OutStr);
    I2Str := OutStr;
  end;
  {*************************************************************************}
  { Procedure CheckPoly                                                     }
  {-------------------------------------------------------------------------}
  { Wenn Item ein CPoly ist, so pr�ft die Prozedur, ob sich Kanten des      }
  { Polygons �berschneiden. Existiert eine �berschneidung, dann wird das    }
  { Poly in den Errorlayer eingef�gt.                                       }
  {*************************************************************************}
  procedure CheckPoly
      (
      Item: PIndex
      ); far;
  var AObject: PCPoly;
    procedure DoCheckPoly
        (
        Start: Integer;
        Stop: Integer
        );
    var Cnt: Integer;
      Cnt2: Integer;
      C1: TDPoint;
    begin
      with AObject^.Data^ do begin
        for Cnt2 := Start + 2 to Stop - 3 do
          if PInfo^.LineCut(TRUE, PDPoint(At(0))^, PDPoint(At(1))^, PDPoint(At(Cnt2))^, PDPoint(At(Cnt2 + 1))^, C1) then
          begin
            InsertOnLayer(ErrorLayer, AObject, nil, nil);
            Exit;
          end;
        for Cnt := Start + 1 to Stop - 2 do
          for Cnt2 := Cnt + 2 to Stop - 2 do
            if PInfo^.LineCut(TRUE, PDPoint(At(Cnt))^, PDPoint(At(Cnt + 1))^,
              PDPoint(At(Cnt2))^, PDPoint(At(Cnt2 + 1))^, C1) then begin
              InsertOnLayer(ErrorLayer, AObject, nil, nil);
              Exit;
            end;          
      end;
    end;
  var Start: Integer;
    Stop: Integer;
    Island: Integer;
  begin
    if Item^.GetObjType = ot_CPoly then
    begin
      AObject := Pointer(Layer1^.IndexObject(PInfo, Item));
      AObject^.EliminatePoints(1);
      if AObject^.GetState(sf_IslandArea) then
        with AObject^ do
        begin
          Start := 0;
          for Island := 0 to IslandCount - 1 do
          begin
            Stop := Start + IslandInfo^[Island];
            DoCheckPoly(Start, Stop);
            Start := Stop;
          end;
        end
      else
        DoCheckPoly(0, AObject^.Data^.Count);
    end;
  end;
  {*************************************************************************}
  { Callback Procedure CopyToError                                          }
  {-------------------------------------------------------------------------}
  { Wird f�r alle Eintr�ge in Islands aufgerufen. Das Poly wird in des      }
  { Errorlayer eingef�gt.                                                   }
  {*************************************************************************}
{++ IDB_UNDO}
var
  bWeWereUsingTheIDBUndoFunction: Boolean;
{-- IDB_UNDO}
{$ENDIF}
begin
{$IFNDEF WMLT}
  ObjCount := 0;
  Layer1 := Layers^.IndexToLayer(Data.Layer1);
  Layer2 := Layers^.IndexToLayer(Data.Layer2);
  DestLayer := Layers^.IndexToLayer(Data.DestLayer);
  if Data.CutOptions and cutDatabase <> 0 then begin
{$IFNDEF AXDLL} // <----------------- AXDLL
    DDEString := '[POV][';
    if DDEHandler.FLayerInfo <> 2 then begin
      DDEString := DDEString + Layer1^.Text^ + '][' + Layer2^.Text^ + '][' + DestLayer^.Text^ + ']';
    end
    else begin
      Str(Layer1^.Index: 10, TempString);
      DDEString := DDEString + TempString + '][';
      Str(Layer2^.Index: 10, TempString);
      DDEString := DDEString + TempString + '][';
      Str(DestLayer^.Index: 10, TempString);
      DDEString := DDEString + TempString + ']';
    end;
    if Data.Action = doAdditive then DDEString := DDEString + '[0]'
    else DDEString := DDEString + '[1]';
    DDEHandler.SendString(DDEString);
{$ENDIF} // <----------------- AXDLL
  end;
  Redraw.Init;
  Search := 1;
  while Layers^.NameToLayer(GetLangText(10153) + ' ' + I2Str(Search)) <> nil do Inc(Search);
  ErrorLayer := Layers^.InsertLayer(GetLangText(10153) + ' ' + I2Str(Search),
             RGBColors[c_LRed], lt_Solid, RGBColors[c_LRed], pt_Solid, ilTop, DefaultSymbolFill);
  if (Data.CutOptions and cutCheckPolys) <> 0 then
  begin
    Layer1^.Data^.ForEach(@CheckPoly);
    if Layer1 <> Layer2 then
      Layer2^.Data^.ForEach(@CheckPoly);
  end;
{++ IDB_UNDO}
//(* // Commented by Cadmensky
    {$IFNDEF AXDLL} // <----------------- AXDLL
    If (WinGISMainForm.WeAreUsingTheIDB[Layers.Parent]) AND (Data.CutOptions AND cutDatabase <> 0) Then
       WinGISMainForm.IDB_Man.GetReadyToPolygonOverlaying(Layers.Parent, Layer1, Layer2, DestLayer);
    {$ENDIF} // <----------------- AXDLL
//*) // Commented by Cadmensky
{-- IDB_UNDO}
  for Count := 0 to Layer1^.Data^.GetCount - 1 do
    if DoAll(Layer1^.Data^.At(Count)) then
       Break;

{++ IDB_UNDO}
  {$IFNDEF AXDLL} // <----------------- AXDLL
  bWeWereUsingTheIDBUndoFunction := WinGISMainForm.WeAreUsingUndoFunction[Layers.Parent];
  if bWeWereUsingTheIDBUndoFunction then
  begin
    WinGISMainForm.UndoRedo_Man.SaveUndoData(Layers.Parent, utUnInsert);
    WinGISMainForm.WeAreUsingUndoFunction[Layers.Parent] := FALSE;
  end;

  if (WinGISMainForm.WeAreUsingTheIDB[Layers.Parent]) and (Data.CutOptions and cutDatabase <> 0) then
    WinGISMainForm.IDB_Man.EndOfPolygonOverlaying (Layers.Parent);

  {$ENDIF} // <----------------- AXDLL
{-- IDB_UNDO}
  if (ErrorLayer^.Data^.GetCount = 0) then Layers^.DeleteLayer(ErrorLayer);
{++ IDB_UNDO}
  {$IFNDEF AXDLL} // <----------------- AXDLL
  if bWeWereUsingTheIDBUndoFunction then
    WinGISMainForm.WeAreUsingUndoFunction[Layers.Parent] := TRUE;
  {$ENDIF} // <----------------- AXDLL
{-- IDB_UNDO}
  UpdateLayersViewsLegendsLists(WingisMainForm.ActualChild.Data);
  if DestLayer <> Layers^.TopLayer then begin
    Redraw.Grow(PInfo^.CalculateDraw(Round(DestLayer^.LineStyle.Width))); {!?!?!?}
    PInfo^.RedrawRect(Redraw);
  end;
  Redraw.Done;
{$IFNDEF AXDLL} // <----------------- AXDLL
  if Data.CutOptions and cutDatabase <> 0 then DDEHandler.SendString('[END][]');
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
end;

function TAreaCut.DoCut2Polies
  (
  Poly1: PCPoly;
  Poly2: PCPoly;
  DoCutAdditive: Boolean;
  DoMergeParts: Boolean;
  ResultColl: PCollection
  )
  : Integer;
var ResError: Integer;
  ResCol: PCollection;
  Cnt: Integer;
  {*************************************************************************}
  { Procedure CheckPoly                                                     }
  {-------------------------------------------------------------------------}
  { Pr�ft die Prozedur, ob sich Kanten des Polygons �berschneiden.          }
  { Existiert eine �berschneidung, dann wird FALSE zur�ckgegeben.           }
  {*************************************************************************}
  function CheckPoly
      (
      Item: PCPoly
      )
      : Boolean;
    function DoCheckPoly
        (
        Start: Integer;
        Stop: Integer
        )
        : Boolean;
    var Cnt: Integer;
      Cnt2: Integer;
      C1: TDPoint;
    begin
      Result := TRUE;
      with Item^.Data^ do begin
        for Cnt2 := Start + 2 to Stop - 3 do
          if PInfo^.LineCut(TRUE, PDPoint(At(0))^, PDPoint(At(1))^,
            PDPoint(At(Cnt2))^, PDPoint(At(Cnt2 + 1))^, C1) then begin
//            Result := FALSE;
            Exit;
          end;
        for Cnt := Start + 1 to Stop - 2 do
          for Cnt2 := Cnt + 2 to Stop - 2 do
            if PInfo^.LineCut(TRUE, PDPoint(At(Cnt))^, PDPoint(At(Cnt + 1))^,
              PDPoint(At(Cnt2))^, PDPoint(At(Cnt2 + 1))^, C1) then begin
//              Result := FALSE;
              Exit;
            end;
      end;
    end;
  var Start: Integer;
    Stop: Integer;
    Island: Integer;
  begin
    if Item^.GetObjType = ot_CPoly then begin
      Item^.EliminatePoints(1);
      if Item^.GetState(sf_IslandArea) then with Item^ do begin
          Start := 0;
          for Island := 0 to IslandCount - 1 do begin
            Stop := Start + IslandInfo^[Island];
            Result := DoCheckPoly(Start, Stop);
            Start := Stop;
          end;
        end
      else Result := DoCheckPoly(0, Item^.Data^.Count);
    end;
  end;
  {*************************************************************************}
  { Function DoAll                                                          }
  {-------------------------------------------------------------------------}
  { Wird f�r das 1. Poly aufgerufen.                                        }
  {*************************************************************************}
  function DoAll
      (
      Item: PCPoly
      )
      : Boolean;
    {***********************************************************************}
    { Function CutAdditive                                                  }
    {-----------------------------------------------------------------------}
    { Wird f�r das 2. Poly aufgerufen.                                      }
    {***********************************************************************}
    function CutAdditive
        (
        Item: PCPoly
        )
        : Boolean;
    var AOwners: Byte;
      APoly: TPoint; { Kontour zu AObject                 }
      BPoly: TPoint; { Kontour zu BObject                 }
      CPoly: PCPoly;
      Cnt: Integer;
      Cnt1: Integer;
      CutCnt: Integer;
      InsertPoly: Boolean;
      IslandList: PIslandStruct;
      TempList: PIslandStruct;
    begin
      Result := FALSE;
      BObject := Item;
      if AObject^.Index = BObject^.Index then begin
        ResCol^.Insert(AObject);
        Result := TRUE;
      end
      else if AObject^.ClipRect.IsVisible(BObject^.ClipRect) then begin
        APoly := CPolyToContour(AObject, A, X);
        BPoly := CPolyToContour(BObject, B, X);
          { alle Teilkontouren miteinander verschneiden                      }
        CutCnt := AObject^.Data^.Count + BObject^.Data^.Count;
        for Cnt := APoly.X to APoly.Y do for Cnt1 := BPoly.X to BPoly.Y do
            Inc(CutCnt, CalculateCuts(PContour(Contours.At(Cnt * 2))^.EntryEdge,
              PContour(Contours.At(Cnt1 * 2))^.EntryEdge));
        CutContours;
          { Teilkontouren miteinander verschmelzen                           }
        IslandList := CreateIslandStruct(nil);
        for Cnt := 0 to Contours.Count - 1 do begin
          InsertPoly := FALSE;
          case MergeContours(Cnt, CutCnt, CPoly, AOwners) of
            mrLoopDetected: ResError := 3;
            mrNoEdge: Continue;
            mrNoError: if AOwners and X = 0 then begin
                CPoly^.ClosePoly;
                CPoly^.CalculateClipRect;
                  //if CPoly^.Flaeche>=0.5 then begin
                if CPoly^.Data^.Count > 2 then begin
                  if AOwners = A + B then InsertPoly := TRUE
                  else if AOwners = A then InsertPoly := CheckInside(PInfo, BObject, CPoly)
                  else if AOwners = B then InsertPoly := CheckInside(PInfo, AObject, CPoly);
                end;
              end;
          end;
          if InsertPoly then IslandList := InsertIsland(PInfo, IslandList, CreateIslandStruct(CPoly))
          else Dispose(CPoly, Done);
        end;
        if not DoMergeParts then begin
          TempList := IslandList^.Islands;
          while TempList <> nil do begin
            if TempList^.Islands = nil then begin
              ResCol^.Insert(TempList^.Poly);
              TempList^.Poly := nil;
              Result := TRUE;
            end
            else begin
              ResCol^.Insert(IslandStructToPoly(TempList, FALSE));
              Result := TRUE;
            end;
            TempList := TempList^.Next;
          end;
        end
        else if (IslandList^.Islands <> nil) then begin
          ResCol^.Insert(IslandStructToPoly(IslandList, TRUE));
          Result := TRUE;
        end;
        DestroyIslandStruct(IslandList, TRUE);
        for Cnt := 0 to Contours.Count - 1 do
          FreePolyContour(Contours.At(Cnt));
        Contours.FreeAll;
      end;
    end;
    {***********************************************************************}
    { Callback Procedure CutSubtractive                                     }
    {-----------------------------------------------------------------------}
    { Wird f�r das 2. Poly bei subtraktiver Verschneidung aufgerufen.       }
    {***********************************************************************}
    function CutSubtractive
        (
        Item: PCPoly
        )
        : Boolean;
    var Count: LongInt;
      Cnt: Integer;
      Cnt1: Integer;
      Cnt2: Integer;
      CutCnt: Integer;
      IslandList: PIslandStruct;
      CPoly: PCPoly;
      AOwners: Byte;
      InsertPoly: Boolean;
      TempList: PIslandStruct;
      Results: PCollection;
      ResultsNew: PCollection;
      Temp: Pointer;
      CObject: PCPoly;
    begin
      Result := FALSE;
      Results := New(PCollection, Init(5, 5));
      ResultsNew := New(PCollection, Init(5, 5));
      Results^.Insert(AObject);
      BObject := Item;
      if AObject^.ClipRect.IsVisible(BObject^.ClipRect) then begin
        for Cnt2 := 0 to Results^.Count - 1 do begin
          CObject := Results^.At(Cnt2);
            { Kontouren f�r die aktuellen Polys erzeugen                     }
          CPolyToContour(CObject, A, X);
          CPolyToContour(BObject, X, B);
          CutCnt := BObject^.Data^.Count + CObject^.Data^.Count;
            { Schnittpunkte berechnen                                        }
          for Cnt := 0 to Contours.Count div 2 - 2 do for Cnt1 := Cnt + 1 to Contours.Count div 2 - 1 do
              Inc(CutCnt, CalculateCuts(PContour(Contours.At(Cnt * 2))^.EntryEdge,
                PContour(Contours.At(Cnt1 * 2))^.EntryEdge));
            { Schnittpunkte einf�gen und Contour-Links updaten               }
          CutContours;
            { Teilkontouren miteinander verschmelzen                         }
          IslandList := CreateIslandStruct(nil);
          for Cnt := 0 to Contours.Count - 1 do begin
            InsertPoly := FALSE;
            case MergeContours(Cnt, CutCnt, CPoly, AOwners) of
              mrLoopDetected: ResError := 3;
              mrNoEdge: Continue;
              mrNoError: if AOwners and X = 0 then begin
                  CPoly^.ClosePoly;
                  CPoly^.CalculateClipRect;
                  //if CPoly^.Flaeche>=0.5 then begin
                  if CPoly^.Data^.Count > 2 then begin
                    if AOwners = A + B then InsertPoly := TRUE
                    else if AOwners = B then InsertPoly := CheckInside(PInfo, CObject, CPoly)
                    else if AOwners = A then InsertPoly := not CheckInside(PInfo, BObject, CPoly)
                  end;
                end;
            end;
            if InsertPoly then IslandList := InsertIsland(PInfo, IslandList, CreateIslandStruct(CPoly))
            else Dispose(CPoly, Done);
          end;
            { Ergebnispolygone erzeugen                                      }
          if not DoMergeParts then begin
            TempList := IslandList^.Islands;
            while TempList <> nil do begin
              if TempList^.Islands = nil then begin
                ResultsNew^.Insert(TempList^.Poly);
                TempList^.Poly := nil;
              end
              else ResultsNew^.Insert(IslandStructToPoly(TempList, FALSE));
              TempList := TempList^.Next;
            end;
          end
          else if (IslandList^.Islands <> nil) then
            ResultsNew^.Insert(IslandStructToPoly(IslandList, TRUE));
          DestroyIslandStruct(IslandList, TRUE);
            { Kontourenliste freigeben                                       }
          for Cnt := 0 to Contours.Count - 1 do
            FreePolyContour(Contours.At(Cnt));
          Contours.FreeAll;
        end;
          { Zwischenergebnis freigeben                                       }
        for Cnt := 0 to Results^.Count - 1 do begin
          CObject := Results^.At(Cnt);
          if CObject^.Index = 0 then Dispose(CObject, Done);
        end;
        Results^.DeleteAll;
        Temp := Results;
        Results := ResultsNew;
        ResultsNew := Temp;
      end;
      for Cnt := 0 to Results^.Count - 1 do begin
        BObject := Results^.At(Cnt);
        ResCol^.Insert(BObject);
      end;
      Results^.DeleteAll;
      Dispose(Results, Done);
      Dispose(ResultsNew, Done);
    end;
  begin
    Result := FALSE;
    AObject := Item;
    if DoCutAdditive then Result := CutAdditive(Poly2)
    else Result := CutSubtractive(Poly2);
  end;
begin
  ResError := 0;
  Result := 0;
  ResCol := New(PCollection, Init(5, 5));
  if CheckPoly(Poly1) and CheckPoly(Poly2) then begin
    if not DoAll(Poly1) then ResError := 1;
  end
  else ResError := 2;
  for Cnt := 0 to ResCol^.Count - 1 do begin
    BObject := ResCol^.At(Cnt);
    if BObject^.Index <> 0 then begin
      BObject := MakeObjectCopy(BObject);
      BObject^.Index := 0;
    end;
    ResultColl^.Insert(BObject);
  end;
  Result := ResError;
end;

function TAreaCut.AreaDivide
  (
  ACPoly: PCPoly;
  APoly: PPoly;
  var Result1: PCPoly;
  var Result2: PCPoly
  )
  : Boolean;
var AContours: TPoint;
  BContours: TPoint;
  IslandList1: PIslandStruct;
  IslandList2: PIslandStruct;
  Cnt: Integer;
  Cnt1: Integer;
  CutCnt: Integer;
  CPoly: PCPoly;
  AOwners: Byte;
begin
  Result := FALSE;
  AContours := CPolyToContour(ACPoly, A, X);
  BContours := PolyToContour(APoly, B, C);
    { alle Teilkontouren miteinander verschneiden                              }
  CutCnt := ACPoly^.Data^.Count + APoly^.Data^.Count;
  for Cnt := AContours.X to AContours.Y do for Cnt1 := BContours.X to BContours.Y do
      Inc(CutCnt, CalculateCuts(PContour(Contours.At(Cnt * 2))^.EntryEdge,
        PContour(Contours.At(Cnt1 * 2))^.EntryEdge));
  if CutCnt > 0 then begin
    CutContours;
      { Teilkontouren miteinander verschmelzen                                 }
    IslandList1 := CreateIslandStruct(nil);
    IslandList2 := CreateIslandStruct(nil);
    for Cnt := 0 to Contours.Count - 1 do begin
      if MergeContours(Cnt, CutCnt, CPoly, AOwners) = mrNoError then begin
        if AOwners and (A + X) = A then begin
          CPoly^.ClosePoly;
          CPoly^.CalculateClipRect;
          if CPoly^.Flaeche < 0.5 then Dispose(CPoly, Done)
          else if AOwners = A + B then IslandList1 := InsertIsland(PInfo, IslandList1,
              CreateIslandStruct(CPoly))
          else if AOwners = A + C then IslandList2 := InsertIsland(PInfo, IslandList2,
              CreateIslandStruct(CPoly))
          else Dispose(CPoly, Done);
        end;
      end;
    end;
    if (IslandList1^.Islands <> nil)
      and (IslandList2^.Islands <> nil) then begin
      Result1 := IslandStructToPoly(IslandList1, TRUE);
      Result2 := IslandStructToPoly(IslandList2, TRUE);
      Result := TRUE;
    end;
    DestroyIslandStruct(IslandList1, TRUE);
    DestroyIslandStruct(IslandList2, TRUE);
  end;
  for Cnt := 0 to Contours.Count - 1 do
    FreePolyContour(Contours.At(Cnt));
end;

function AreaCut2Polies
  (
  AProj: Pointer;
  APoly1: PCPoly;
  APoly2: PCPoly;
  DoAdditive: Boolean;
  DoMerge: Boolean;
  AColl: PCollection
  )
  : Integer;
var CutObj: TAreaCut;
begin
  CutObj.Init(PProj(AProj)^.Layers, PProj(AProj)^.PInfo);
  Result := CutObj.DoCut2Polies(APoly1, APoly2, DoAdditive, DoMerge, AColl);
  CutObj.Done;
end;

procedure AreaCut4
  (
  AProj: Pointer
  );
var Dialog: TAreaCutDlg;
  CutObj: TAreaCut;
  OldTopLayer: PLayer;
begin
  PProj(AProj)^.SetActualMenu(mn_Select);
  PProj(AProj)^.DeselectAll(TRUE);
  Dialog := TAreaCutDlg.Init(PProj(AProj)^.Parent, PProj(AProj)^.Layers, @PProj(AProj)^.AreaCutOptions,
    PProj(AProj)^.PInfo^.ProjStyles);
  if ExecDialog(Dialog) = id_OK then begin
    PProj(AProj)^.PInfo^.RedrawInvalidate;
    StatusBar.ProgressPanel := TRUE;
    OldTopLayer:=PProj(AProj)^.Layers^.TopLayer;
    try
      StatusBar.ProgressText := GetLangText(10126);
      StatusBar.AbortText := GetLangText(3581);
      CutObj.Init(PProj(AProj)^.Layers, PProj(AProj)^.PInfo);
      CutObj.DoCut(PProj(AProj)^.AreaCutOptions);
      CutObj.Done;
    finally
      StatusBar.ProgressPanel := FALSE;
      PProj(AProj)^.Layers^.SetTopLayer(OldTopLayer);
    end;
    PProj(AProj)^.SetModified;
  end;
end;

{***************************************************************************}
{ Procedure AreaCut5                                                        }
{---------------------------------------------------------------------------}
{ F�hrt die Fl�chenverschneidung mit �bergebenen Layern ohne Dialog         }
{ substraktiv aus.                                                          }
{***************************************************************************}

procedure AreaCut5
  (
  AProj: Pointer;
  L1: string;
  L2: string;
  L3: string
  );
var CutObj: TAreaCut;
begin
  PProj(AProj)^.SetActualMenu(mn_Select);
  PProj(AProj)^.DeselectAll(TRUE);
  PProj(AProj)^.AreaCutOptions.Action := doSubtract;
  PProj(AProj)^.AreaCutOptions.Layer1 := PProj(AProj)^.Layers^.NameToLayer(L1)^.Index;
  PProj(AProj)^.AreaCutOptions.Layer2 := PProj(AProj)^.Layers^.NameToLayer(L2)^.Index;
  PProj(AProj)^.AreaCutOptions.DestLayer := PProj(AProj)^.Layers^.NameToLayer(L3)^.Index;
  PProj(AProj)^.AreaCutOptions.CutOptions := CutMergeParts;
  PProj(AProj)^.PInfo^.RedrawInvalidate;
  StatusBar.ProgressPanel := TRUE;
  try
    StatusBar.ProgressText := GetLangText(10126);
    StatusBar.AbortText := GetLangText(3581);
    CutObj.Init(PProj(AProj)^.Layers, PProj(AProj)^.PInfo);
    CutObj.DoCut(PProj(AProj)^.AreaCutOptions);
    CutObj.Done;
  finally
    StatusBar.ProgressPanel := FALSE;
  end;
end;

{***************************************************************************}
{ Procedure AreaCut6                                                        }
{---------------------------------------------------------------------------}
{ F�hrt die Fl�chenverschneidung mit �bergebenen Layern ohne Dialog         }
{ additiv aus.                                                              }
{***************************************************************************}

procedure AreaCut6
  (
  AProj: Pointer;
  L1: string;
  L2: string;
  L3: string
  );
var CutObj: TAreaCut;
begin
  PProj(AProj)^.SetActualMenu(mn_Select);
  PProj(AProj)^.DeselectAll(TRUE);
  PProj(AProj)^.AreaCutOptions.Action := doAdditive;
  PProj(AProj)^.AreaCutOptions.Layer1 := PProj(AProj)^.Layers^.NameToLayer(L1)^.Index;
  PProj(AProj)^.AreaCutOptions.Layer2 := PProj(AProj)^.Layers^.NameToLayer(L2)^.Index;
  PProj(AProj)^.AreaCutOptions.DestLayer := PProj(AProj)^.Layers^.NameToLayer(L3)^.Index;
  PProj(AProj)^.AreaCutOptions.CutOptions := CutMergeParts;
  PProj(AProj)^.PInfo^.RedrawInvalidate;
  StatusBar.ProgressPanel := TRUE;
  try
    StatusBar.ProgressText := GetLangText(10126);
    StatusBar.AbortText := GetLangText(3581);
    CutObj.Init(PProj(AProj)^.Layers, PProj(AProj)^.PInfo);
    CutObj.DoCut(PProj(AProj)^.AreaCutOptions);
    CutObj.Done;
  finally
    StatusBar.ProgressPanel := FALSE;
  end;
end;

{***************************************************************************}
{ Procedure AreaCut7                                                        }
{---------------------------------------------------------------------------}
{ Runs der polygon overlay without dialog and with all the options          }
{ in the parameters.                                                        }
{***************************************************************************}

procedure AreaCut7
  (
  AProj: Pointer;
  ASourceLayer1: LongInt;
  ASourceLayer2: LongInt;
  ATargetLayer: LongInt;
  AAddSub: Integer;
  ACheckAreas: Boolean;
  ADBOverlay: Boolean;
  AMergeAreas: Boolean
  );
var CutObj: TAreaCut;
begin
  PProj(AProj)^.SetActualMenu(mn_Select);
  PProj(AProj)^.DeselectAll(TRUE);
  with PProj(AProj)^.AreaCutOptions do begin
    if AAddSub = 0 then Action := doAdditive
    else Action := doSubtract;
    Layer1 := ASourceLayer1;
    Layer2 := ASourceLayer2;
    DestLayer := ATargetLayer;
    CutOptions := 0;
    if ACheckAreas then CutOptions := CutOptions or cutCheckPolys;
    if ADBOverlay then CutOptions := CutOptions or cutDatabase;
    if AMergeAreas then CutOptions := CutOptions or CutMergeParts;
  end;
  PProj(AProj)^.PInfo^.RedrawInvalidate;
  StatusBar.ProgressPanel := TRUE;
  try
    StatusBar.ProgressText := GetLangText(10126);
    StatusBar.AbortText := GetLangText(3581);
    CutObj.Init(PProj(AProj)^.Layers, PProj(AProj)^.PInfo);
    CutObj.DoCut(PProj(AProj)^.AreaCutOptions);
    CutObj.Done;
  finally
    StatusBar.ProgressPanel := FALSE;
  end;
end;

////////////////////////////////////////////////////////////////////////////////

procedure ObjectCut(AProj: Pointer);
var
CutObj: TAreaCut;
OldTopLayer: PLayer;
ALayer: PLayer;
s: AnsiString;
Res: Integer;
i,n: Integer;
L1,L2,DL: AnsiString;
begin
     Res:=mrNone;

     L1:='';
     L2:='';
     DL:='';

     ObjCutDialog:=TObjCutDialog.Create(nil);
     try
        n:=PProj(AProj)^.Layers^.LData^.Count;

        for i:=1 to n-1 do begin
            ALayer:=PProj(AProj)^.Layers^.LData^.At(i);
            s:=ALayer^.Text^;

            if not ALayer^.GetState(sf_LayerOff) then begin
               ObjCutDialog.ListBoxS1.Items.Add(s);
               ObjCutDialog.ListBoxS2.Items.Add(s);

               if not ALayer^.GetState(sf_Fixed) then begin
                  ObjCutDialog.ListBoxD.Items.Add(s);
               end;
            end;
        end;

        Res:=ObjCutDialog.ShowModal;

        if Res = mrOK then begin
           if ObjCutDialog.ListBoxS1.ItemIndex > -1 then begin
              L1:=ObjCutDialog.ListBoxS1.Items[ObjCutDialog.ListBoxS1.ItemIndex];
           end;

           if ObjCutDialog.ListBoxS2.ItemIndex > -1 then begin
              L2:=ObjCutDialog.ListBoxS2.Items[ObjCutDialog.ListBoxS2.ItemIndex];
           end;

           if ObjCutDialog.ListBoxD.ItemIndex > -1 then begin
              DL:=ObjCutDialog.ListBoxD.Items[ObjCutDialog.ListBoxD.ItemIndex];
           end;
        end;

     finally
        ObjCutDialog.Free;
     end;

     if (Res <> mrOK) or (L1 = '') or (L2 = '') or (DL = '') then exit;

     PProj(AProj)^.SetActualMenu(mn_Select);
     PProj(AProj)^.DeselectAll(True);

     with PProj(AProj)^.AreaCutOptions do begin
          Action:=doAdditive;

          Layer1:=PProj(AProj)^.Layers^.NameToLayer(L1)^.Index;
          Layer2:=PProj(AProj)^.Layers^.NameToLayer(L2)^.Index;
          DestLayer:=PProj(AProj)^.Layers^.NameToLayer(DL)^.Index;

          CutOptions:=cutDatabase;
     end;

     PProj(AProj)^.PInfo^.RedrawInvalidate;

     StatusBar.ProgressPanel:=True;
     StatusBar.ProgressText:=GetLangText(10126);
     StatusBar.AbortText:=GetLangText(3581);

     CutObj.Init(PProj(AProj)^.Layers, PProj(AProj)^.PInfo);
     OldTopLayer:=PProj(AProj)^.Layers^.TopLayer;
     try
        CutObj.AllObjTypes:=True;
        CutObj.DoCut(PProj(AProj)^.AreaCutOptions);
     finally
        CutObj.Done;
        StatusBar.ProgressPanel:=False;
        PProj(AProj)^.Layers^.SetTopLayer(OldTopLayer);
        PProj(AProj)^.SetModified;
     end;
end;

////////////////////////////////////////////////////////////////////////////////

procedure AreaCutSelected(AProj: Pointer; DestLayerName: string; AAddSub: Integer);
var
  CutObj: TAreaCut;
  OldTop: PLayer;
  DestLayer: PLayer;
begin
  DestLayer := PProj(AProj)^.Layers^.NameToLayer(DestLayerName);
  if DestLayer = nil then
    DestLayer := SelectLayerDialog(AProj, [sloNewLayer], 2010);
  if DestLayer <> nil then begin
    OldTop := PProj(AProj)^.Layers^.TopLayer;
    PProj(AProj)^.AreaCutOptions.Layer1 := 0;
    PProj(AProj)^.AreaCutOptions.Layer2 := 0;
    PProj(AProj)^.AreaCutOptions.DestLayer := DestLayer.Index;
    PProj(AProj)^.AreaCutOptions.Action := AAddSub;
    PProj(AProj)^.AreaCutOptions.CutOptions := cutMergeParts;
    StatusBar.ProgressPanel := TRUE;
    try
      StatusBar.ProgressText := GetLangText(10126);
      StatusBar.AbortText := GetLangText(3581);
      CutObj.Init(PProj(AProj)^.Layers, PProj(AProj)^.PInfo);
      CutObj.DoCut(PProj(AProj)^.AreaCutOptions);
      CutObj.Done;
    finally
      StatusBar.ProgressPanel := FALSE;
    end;
    PProj(AProj)^.Layers^.TopLayer := OldTop;
    PProj(AProj)^.DeselectAll(False);
    PProj(AProj)^.SetModified;
  end;
end;


{============================================================================}
{ TAreaCutDlg                                                                }
{============================================================================}

constructor TAreaCutDlg.Init
  (
  AParent: TWinControl;
  ALayers: PLayers;
  AData: PAreaCutDat;
  AProjStyle: TProjectStyles
  );
begin
  inherited Init(AParent, 'AreaCut', ALayers, AProjStyle);
  Data := AData;
  HelpContext := 4030;
end;

{****************************************************************************}
{ Procedure TAreaCutDlg.SetupWindow                                          }
{----------------------------------------------------------------------------}
{ Initialisiert alle Dialogelemente. Die Listboxen werden mit den Layer-     }
{ namen gef�llt und die in Data angegebenen Layer selektiert. Weiters        }
{ werden die Radiobuttons und die Checkbox entsprechend der Angaben in Data  }
{ gesetzt.                                                                   }
{****************************************************************************}

procedure TAreaCutDlg.SetupWindow;
begin
  inherited SetupWindow;
  FillLayerList(id_Layer1, TRUE, TRUE);
  FillLayerList(id_Layer2, TRUE, TRUE);
  FillLayerList(id_Destination, FALSE, TRUE);
  SendDlgItemMsg(id_Layer1, cb_SetCurSel, IndexOfLayer(Data^.Layer1, FALSE), 0);
  SendDlgItemMsg(id_Layer2, cb_SetCurSel, IndexOfLayer(Data^.Layer2, FALSE), 0);
  CheckRadioButton(Handle, id_Additive, id_Subtractive, Data^.Action + id_Additive);
  if (Data^.CutOptions and cutCheckPolys) <> 0 then CheckDlgButton(Handle, id_CheckPolys, 1);
  if (Data^.CutOptions and cutDatabase) <> 0 then CheckDlgButton(Handle, id_Database, 1);
  if (Data^.CutOptions and cutMergeParts) <> 0 then CheckDlgButton(Handle, id_MergeParts, 1);
end;

{***************************************************************************}
{ Function TAreaCutDlg.CanClose                                             }
{---------------------------------------------------------------------------}
{ Pr�ft alle Eingaben:                                                      }
{  - Ziellayer mittels GetLayer                                             }
{  - Ziellayer ungleich 1. und 2. Layer                                     }
{ Setzt den Datenrecord auf die eingegebenen Werte.                         }
{---------------------------------------------------------------------------}
{ Ergebnis: TRUE   = Eingabe OK, Dialog kann geschlossen werden             }
{           FALSE  = Eingaben fehlerhaft                                    }
{***************************************************************************}

function TAreaCutDlg.CanClose
  : Boolean;
var AName: array[0..255] of Char;
begin
  CanClose := FALSE;
  with Data^ do begin
    GetDlgItemText(Handle, id_Layer1, AName, SizeOf(AName));
    if StrComp(AName, '') = 0 then begin
      MsgBox(Handle, 11290, mb_IconInformation or mb_Ok, '');
      SetFocus(GetItemHandle(id_Layer1));
    end
    else begin
      Layer1 := Layers^.NameToLayer(StrPas(AName))^.Index;
      GetDlgItemText(Handle, id_Layer2, AName, SizeOf(AName));
      if StrComp(AName, '') = 0 then begin
        MsgBox(Handle, 11291, mb_IconInformation or mb_Ok, '');
        SetFocus(GetItemHandle(id_Layer2));
      end
      else begin
        Layer2 := Layers^.NameToLayer(StrPas(AName))^.Index;
//          if (Layer1<>Layer2) or (MsgBox(Handle,11288,mb_IconQuestion or mb_YesNo,'')=id_Yes) then begin
        if GetLayer(id_Destination, DestLayer, 959, 10125, 10128, ilTop) then begin
          if (Layer1 = DestLayer) or (Layer2 = DestLayer) then begin
            MsgBox(Handle, 10124, mb_IconInformation or mb_Ok, '');
            SetFocus(GetItemHandle(id_Destination));
          end
          else begin
            if IsDlgButtonChecked(Handle, id_Additive) = 1 then Action := doAdditive
            else if IsDlgButtonChecked(Handle, id_Subtractive) = 1 then Action := doSubtract;
            CutOptions := 0;
            if IsDlgButtonChecked(Handle, id_CheckPolys) = 1 then
              CutOptions := CutOptions or cutCheckPolys;
            if IsDlgButtonChecked(Handle, id_Database) = 1 then
              CutOptions := CutOptions or cutDatabase;
            if IsDlgButtonChecked(Handle, id_MergeParts) = 1 then
              CutOptions := CutOptions or cutMergeParts;
            CanClose := TRUE
          end;
//            end;
        end;
      end;
    end;
  end;
end;

{===========================================================================}
{ TDisposeColl                                                              }
{===========================================================================}

procedure TDisposeColl.FreeItem
  (
  AItem: Pointer
  );
begin
  Dispose(AItem);
end;

{============================================================================}
{ TCutItem                                                                   }
{============================================================================}

constructor TCutItem.Init
  (
  ACut: PCutVertex;
  const ADistance: Double
  );
begin
  inherited Init;
  CutVertex := ACut;
  Distance := ADistance;
end;

{============================================================================}
{ TCutList                                                                   }
{============================================================================}

constructor TCutList.Init;
begin
  inherited Init(2, 2);
  Duplicates := TRUE;
end;

function TCutList.Compare
  (
  A: Pointer;
  B: Pointer
  )
  : Integer;
var Difference: Double;
begin
  Difference := PCutItem(A)^.Distance - PCutItem(B)^.Distance;
  if Abs(Difference) < Tolerance then Result := 0
  else if Difference < 0 then Result := -1
  else Result := 1;
end;

{============================================================================}
{ TCutEdge                                                                   }
{============================================================================}

constructor TCutEdge.Init
  (
  AEdge: PEdge
  );
begin
  inherited Init;
  Edge := AEdge;
  CutList.Init;
end;

destructor TCutEdge.Done;
begin
  CutList.DeleteAll;
  CutList.Done;
  inherited Done;
end;


{============================================================================}
{ TCutEdgeList                                                                       }
{============================================================================}

constructor TCutEdgeList.Init;
begin
  inherited Init(100, 10);
end;

function TCutEdgeList.Compare
  (
  Key1: Pointer;
  Key2: Pointer
  )
  : Integer;
begin
  if LongInt(Key1) < LongInt(Key2) then Compare := -1
  else if LongInt(Key1) > LongInt(Key2) then Compare := 1
  else Compare := 0;
end;

function TCutEdgeList.KeyOf
  (
  Item: Pointer
  )
  : Pointer;
begin
  KeyOf := PCutEdge(Item)^.Edge;
end;

function TCutEdgeList.GetCutEdge
  (
  AEdge: PEdge
  )
  : PCutEdge;
var AIndex: Integer;
  ACutEdge: PCutEdge;
begin
  if Search(AEdge, AIndex) then GetCutEdge := At(AIndex)
  else begin
    ACutEdge := New(PCutEdge, Init(AEdge));
    Insert(ACutEdge);
    GetCutEdge := ACutEdge;
  end;
end;

function TCutVertexList.Compare
  (
  Key1: Pointer;
  Key2: Pointer
  )
  : Integer;
var Diff: Double;
begin
  Diff := PCPoint(Key1)^.X - PCPoint(Key2)^.X;
  if Abs(Diff) < Tolerance then begin
    Diff := PCPoint(Key1)^.Y - PCPoint(Key2)^.Y;
    if Abs(Diff) < Tolerance then Result := 0
    else if Diff < 0 then Result := -1
    else Result := 1;
  end
  else if Diff < 0 then Result := -1
  else Result := 1;
end;

function TCutVertexList.KeyOf
  (
  Item: Pointer
  )
  : Pointer;
begin
  Result := PCutVertex(Item)^.Position;
end;

function TCutVertexList.Add
  (
  Position: PCPoint
  )
  : PCutVertex;
var AIndex: Integer;
begin
  if Search(Position, AIndex) then begin
    Result := At(AIndex);
    Dispose(Position);
  end
  else begin
    Result := New(PCutVertex, Init(Position));
    Insert(Result);
  end;
end;

constructor TCutVertex.Init
  (
  APosition: PCPoint
  );
begin
  inherited Init;
  Edges.Init;
  Position := APosition;
end;

destructor TCutVertex.Done;
begin
  Edges.DeleteAll;
  Edges.Done;
  inherited Done;
end;

constructor TEdgeSort.Init
  (
  AEdge: PEdge;
  const AAngle: Double;
  ADirection: TDirection
  );
begin
  inherited Init;
  Edge := AEdge;
  Angle := AAngle;
  Direction := ADirection;
end;

constructor TEdgeSortList.Init;
begin
  inherited Init(100, 10);
  Duplicates := TRUE;
end;

function TEdgeSortList.Compare
  (
  Key1: Pointer;
  Key2: Pointer
  )
  : Integer;
var Diff: Double;
begin
  Diff := PEdgeSort(Key1)^.Angle - PEdgeSort(Key2)^.Angle;
  if Abs(Diff) < Tolerance then Result := 0
  else if Diff < 0 then Result := -1
  else Result := 1;
end;

constructor TEdgeList.Init;
begin
  inherited Init(100, 10);
end;

function TEdgeList.Compare
  (
  Key1: Pointer;
  Key2: Pointer
  )
  : Integer;
begin
  if LongInt(Key1) = LongInt(Key2) then Result := 0
  else if LongInt(Key1) < LongInt(Key2) then Result := -1
  else Result := 1;
end;

  {++Brovak}

constructor TConvert2000.Create;
begin;
  inherited Create;
  APolygons := New(PCollection, Init(10, 10));
  Basepolygon := New(PCollection, Init(10, 10));
  Apolygon := New(PCollection, Init(10, 10));
end;

destructor TConvert2000.Destroy;
var i: integer;
begin;
  inherited Destroy;
  Basepolygon.FreeAll;
  for I := 0 to APolygons.Count - 1 do
  begin;
    Basepolygon := Pcollection(APolygons.At(i));
    if BasePolygon <> nil then
      BasePolygon.FreeAll;
  end;
  APolygons.FreeAll;
  APolygon.FreeAll;
end;

{This function converting Area with old Island format to Wingis 2000 (4.xx) ver}
function TConvert2000.ConvertOldIslandAreaToVersion2000(APoly: PCPoly): PCpoly;

var i, j: integer;
  A_Poly: PCPoly;
  P: Tdpoint;
begin;
  P.Init(0, 0);
  A_Poly := New(PCPoly, Init);
  A_Poly^.Data := MakeObjectCopy(APoly^.Data);
  for i := 0 to A_poly.Data.Count - 1 do
  begin;
    P := PdpOint(A_poly.Data.At(i))^;
    Adata := New(Pdpoint, Init(P.X, P.Y));
    BasePolygon.Insert(Adata);
  end;
  ExtractPolygons1(BasePolygon);
  Result := CreateIslandPolygonForOLd;
end;

function TConvert2000.CreateIslandPolygonForOLd: PCPoly;

  procedure CollectionToCpoly(CurrentCollection: PCollection; CurrentPcpoly: PCpoly);
  var i: integer;
  begin;
    for i := 0 to CurrentCollection.Count - 1 do
      CurrentPCPoly.InsertPoint(PDPoint(CurrentCollection.At(i))^);
  end;

var A0, A1, A2, A3: PCPoly;
  Island1, Island2, Island3: PIslandStruct;
  i, j, PointCnt: Longint;
  D: string;
  TmpLayer: PLayer;
  TopL: Player;
  P1: Pindex;
begin;

  TopL := PProj(WingisMainform.ActualProj).Layers.TopLayer;
  TmpLayer := PProj(WingisMainform.ActualProj).Layers^.InsertLayer('tmp_layer_for_old_polygon_transform',
    RGBColors[nl_Color], nl_Line, RGBColors[nl_PatCol], nl_Pat, ilTop, DefaultSymbolFill);
  TmpLayer^.SetState(sf_Layeroff, true);

  for i := 0 to APolygons.Count - 1 do begin;
    A0 := New(PCPoly, Init);
    BasePolygon := Pcollection(APolygons.At(i));
    CollectionToCpoly(BasePolygon, A0);
    WingisMainForm.ActualChild.Data.InsertObjectOnLayer(A0, Tmplayer.Index);
  end;
  P1 := Pindex(TmpLayer.Data.At(0));
  A1 := PCpoly(TmpLayer.IndexObject(WingisMainForm.ActualChild.Data.Pinfo, P1));
  PointCnt := A1.Data.Count + 1;
  Island1 := PolyToIslandStruct(WingisMainForm.ActualChild.Data.PInfo, A1);
  for i := 1 to TmpLayer.Data.GetCount - 1 do
  begin;
    P1 := Pindex(TmpLayer.Data.At(i));
    A2 := PCpoly(TmpLayer.IndexObject(WingisMainForm.ActualChild.Data.Pinfo, P1));
    PointCnt := PointCnt + A2^.Data^.Count + 1;
    if PointCnt >= MaxCollectionSize then Break;
    Island2 := PolyToIslandStruct(WingisMainForm.ActualChild.Data.PInfo, A2);
    Island1 := InsertIsland(WingisMainForm.ActualChild.Data.PInfo, Island1, Island2);
  end;
  Result := IslandStructTopoly(Island1, true);
  for i := TmpLayer.Data.GetCount - 1 downto 0 do
    Tmplayer.Data.AtDelete(i);
  PProj(WingisMainform.ActualProj).Layers^.DeleteLayer(TmpLayer);
  DestroyIslandStruct(Island1, true);
end;

procedure TConvert2000.ExtractPolyGons1(CurrentPoly: PCollection);
var i, j, k, i1, j1: integer;
  ExternalPoint, InternalPoint, P: Tdpoint;
  Bdata: PCollection;
  Rect: TRect;
begin;
  ExternalPoint.Init(0, 0);
  InternalPoint.Init(0, 0);
  P.INit(0, 0);
  i := 0; j := 0;
  while i <= CurrentPoly.Count - 1 do
  begin;
    J := i + 1;
    ExternalPoint := Pdpoint(CurrentPoly.At(i))^;
    while j < CurrentPoly.Count do
    begin;
      InternalPoint := Pdpoint(CurrentPoly.At(j))^;
      if (Internalpoint.X = Externalpoint.X)
        and (Internalpoint.Y = Externalpoint.Y)
        and (i <> j)
        then begin;
        if j = CurrentPoly.Count
          then
          Break //    Continue
        else
        begin;
          if (Pdpoint(CurrentPoly.At(i + 1))^.X = Pdpoint(CurrentPoly.At(j - 1))^.X)
            and (Pdpoint(CurrentPoly.At(i + 1))^.Y = Pdpoint(CurrentPoly.At(j - 1))^.Y)
            and (i <> j - 1)
            then
          begin;
            i1 := i + 1; j1 := j - 1;
            TmpCollection := New(PCollection, Init(i1 - j1 + 1, 10));
            for k := i1 to j1 do
            begin;
              P := Pdpoint(CurrentPoly.At(k))^;
              Adata := New(Pdpoint, Init(P.X, P.Y));
              TmpCollection.Insert(Adata);
            end; //for k
            for k := i to j - 1 do
              CurrentPoly.AtDelete(i);
            ExtractPolyGons1(TmpCollection);
            j := i + 1;
            continue;
          end //if old
        end; //else
      end;
      Inc(j);
    end; //while j
    Inc(i);
  end; //while i
  APolyGons.Insert(CurrentPoly);
end;
  {--Brovak}


{$IFDEF TESTING}
begin
  assign(outfile, 't.o');
  rewrite(outfile);
  anumber := 0;
{$ENDIF}
end.

