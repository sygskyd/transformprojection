Unit Xstyles;

{
 The behavior of XStyle was corrected by Ivanoff on 25-01-2001.

 The parameters of an user line style for each component of the style were
 storaged in different records (TXlineDef). And there is a record that contains
 this information (TXLineStyleDef). This record storage XLStyle's array and length
 of this array.
 Now it works the following way:
 1. The parameters of the style (cycle, clipping mode) are common for all items of style
    and they are storage in appropriate fields of first items.
 2. There was only one clipping mode - clip or not clip style edges if the style exeeds
    line section. Now we have 2 modes:
    1. turning the style by line or not;
    2. clipping the style or not.
    These modes are mutually exclusive.
    To storage this information I use the very ClipEdges: Boolean field of first item of the array.
    To detect turning mode I use XLineStyle.Items[0].ClipEdges AND ci_XLine_TurnIt and to detect
    clipping mode I use XLineStyle.Items[0].ClipEdges AND ci_XLine_ClipIt.
 Ivanoff.
}

Interface

Uses Windows,Graphics;

{$ALIGN ON}

Const itcircle     = 0;
      itArc        = 1;
      itChord      = 2;
      itRectangle  = 5;
      itPolygon    = 6;
      itPolyline   = 7;

      PolyPoints   = 32;

{++ New XLine BUG#251}
     ci_XLine_ClipIt = 1; // Clipping mode of the user line style.
     ci_XLine_TurnIt = 2; // Turning mode of the user line style.
{-- New XLine BUG#251}


Type PXLineDef   = ^TXLineDef;
     TXLineDef   = Record
       PointCount  : Integer;
       Points      : Array[1..PolyPoints] of TPoint;
       Cycle       : Integer;
       ClipEdges   : Boolean;
       ItemType    : Byte;
       LineWidth   : Integer;
       FillStyle   : Integer;
       LineColor   : TColor;
       FillColor   : TColor;
     end;

     PXLineDefs    = ^TXLineDefs;
     TXLineDefs    = Array[0..0] of TXLineDef;

     PXLineStyleDef = ^TXLineStyleDef;
     TXLineStyleDef = Record
       Count        : Integer;
       XLineDefs    : TXLineDefs;
     end;

     PPointArray    = ^TPointArray;
     TPointArray    = Array[0..0] of TPoint;

     {**************************************************************************
     | Record TXFillDef
     |--------------------------------------------------------------------------
     | Style = windows line-style or lt_UserDef if x-line-style
     | XStyleCount = number of xlinestyle-defs in XLineStyle
     | XLineStyle = pointer to x-line definition or NIL if windows-style
     | XOffset = offset of the start-point of the line
     | Cycle = distance of the lines
     | Phase = offset between two subsequent lines
     | Angle = angle the lines are drawn with
     | Width = width of the line, scale for user-defined, 1/100mm for windows-lines
     | LineColor = color of lines, Parameter if -2
     | FillColor = color of filled elements, transparent if -1, Parameter if -2
     **************************************************************************}
     PXFillDef     = ^TXFillDef;
     TXFillDef     = Record
       Style       : Integer;
       XLineStyle  : PXLineStyleDef;
       Cycle       : Integer;
       XOffset     : Integer;
       YOffset     : Integer;
       Phase       : Integer;
       Angle       : Double;
       LineWidth   : Double;
       LineColor   : TColor;
       FillColor   : TColor;
     end;

     PXFillDefs    = ^TXFillDefs;
     TXFillDefs    = Array[0..0] of TXFillDef;

     PXFillStyleDef = ^TXFillStyleDef;
     TXFillStyleDef = Record
       Count        : Integer;
       XFillDefs    : TXFillDefs;
     end;

     PBitmapXFillDef = ^TBitmapXFillDef;
     TBitmapXFillDef = Record
       BitmapHandle : HBitmap;
     end;

Procedure XLineTo(DC: HDC; X2,Y2: Integer;
                  Const XLN: Array of TXLineDef; XNum: Integer;
                  LCol,FCol: TColor; Switch: Boolean; Zoom: Double);

Procedure XPolyLine(DC: HDC; var PL: Array of TPoint; PNum: Integer;
                    Const XLN: Array of TXLineDef; XNum: Integer;
                    LCol,FCol: TColor; Switch: Boolean; Zoom: Double);

Procedure XPolygon(DC: HDC; var PL: Array of TPoint; PNum: Integer;
                   Const XLN: Array of TXLineDef; XNum: Integer;
                   LCol,FCol: TColor; Switch: Boolean; Zoom: Double);

Procedure XEllipse(DC: HDC; X,Y,A,B: Integer; R: Double;
                   Const XLN: Array of TXLineDef; XNum: Integer;
                   LCol,FCol: TColor; Switch: Boolean; Zoom: Double);

Procedure XArc(DC: HDC; X,Y,A,B:Integer; P,Q,R: Double;
               Const XLN: Array of TXLineDef; XNum: Integer;
               LCol,FCol: TColor; Switch: Boolean; Zoom: Double);

Procedure XLFillEllipse(DC: HDC; X1,Y1,X2,Y2: Integer;
                        Const XFL: TXFillDefs; FNum: Integer;
                        LineColor,FillColor: TColor; Const Zoom, Rotation: Double);

Procedure XLFillPolygon(DC: HDC; var PL: Array of TPoint; PNum: Integer;
                        Const XFL: TXFillDefs; FNum: Integer;
                        LineColor,FillColor: TColor; Const Zoom, Rotation: Double);

Procedure BMFillEllipse(DC: HDC; X1,Y1,X2,Y2: Integer;
                        BM: HBitmap; Mode: Integer; Zoom: Double);

Procedure BMFillPolygon(DC: HDC; Var PL: Array of TPoint; PNum: Integer;
                        BM: HBitmap; Mode: Integer; Zoom: Double);

Function XLineStyleDefSize(Count:Integer):Integer; overload;
Function XLineStyleDefSize(XStyle:PXLineStyleDef):Integer; overload;
Function XFillStyleDefSize(Count:Integer):Integer; overload;
Function XFillStyleDefSize(XStyle:PXFillStyleDef):Integer; overload;
Procedure FreeXLineStyle(XStyle:PXLineStyleDef);
Function CopyXLineStyle(XStyle:PXLineStyleDef):PXLineStyleDef;

Implementation

{==============================================================================+
  Common
+==============================================================================}

Function XLineStyleDefSize(XStyle:PXLineStyleDef):Integer;
begin
  Result:=XStyle^.Count*SizeOf(TXLineDef)+SizeOf(Integer);
end;

Function XFillStyleDefSize(XStyle:PXFillStyleDef):Integer;
begin
  Result:=XStyle^.Count*SizeOf(TXFillDef)+SizeOf(Integer);
end;

Function XLineStyleDefSize(Count:Integer):Integer;
begin
  Result:=Count*SizeOf(TXLineDef)+SizeOf(Integer);
end;

Function XFillStyleDefSize(Count:Integer):Integer;
begin
  Result:=Count*SizeOf(TXFillDef)+SizeOf(Integer);
end;

Procedure FreeXLineStyle(XStyle:PXLineStyleDef);
begin
  FreeMem(XStyle,XLineStyleDefSize(XStyle));
end;

Function CopyXLineStyle(XStyle:PXLineStyleDef):PXLineStyleDef;
begin
  GetMem(Result,XLineStyleDefSize(XStyle));
  Move(XStyle^,Result^,XLineStyleDefSize(XStyle));
end;

{==============================================================================+
  XLines
+==============================================================================}

{ Datenstruktur des Linienstils:
  Points:   Linienelement in Form eines 4-wertigen Polygons
          d.h. beliebiges Viereck (kann auch zu Dreieck oder Linie entarten)
          oder Kreissegment (siehe Circle)
  Cycle:  L�nge des Wiederholzyklus des Elements
         (falls 0 -> durchgezogene Linie)
  Circle: Wenn TRUE => Kreissegment statt Viereck
          x1=Radius, (x2,y2)=Mittelpunkt, P3-P4=Schnittgerade
  ClipEdges:   Wenn TRUE => Elemente werden an Polygonkanten geschnitten,
          andernfalls als Ganzes �ber Polygonzug approximiert
  Width:  Dicke des Randes des Elements
          Wenn <0 => kein Rand
  FillStyle:   Typ der F�llung des Elements
          Wenn <0 => keine F�llung
          Wenn  0 => volle F�llung
          F�r durchgezogene Linientypen (Cycle=0) -> Spezialf�llung
          mit dem Typ als niedrigstem Byte und Liniendicke und
          Gitterabstand als weitere Bytes; folgende Typen:
            1 ||||||
            2 ------
            3 ++++++
            4 //////
            5 \\\\\\
            6 XXXXXX
          Beispiel: FillStyle=$090301 => senkrechtes Liniengitter mit
                                    Liniendicke 3 und Gitterabstand 9
  LineColor: Farbe der Randlinie des Elementes
          Wenn -1 => kein Rand
          Wenn -2 => Farbe der aufrufenden Prozedur wird �bernommen
  FillColor: Farbe der Elementf�llung
          Wenn -1 => keine F�llung
          Wenn -2 => Farbe der aufrufenden Prozedur wird �bernommen   }

Type Vector = record X,Y: Double end;
     PP = Array[1..PolyPoints] of TPoint;

procedure direction(p1,p2: TPoint; Var dl: Double; Var d: Vector);
begin
  dl:=sqrt(sqr(p2.x-p1.x)+sqr(p2.y-p1.y));
  if dl>0 then begin
    d.x:=(p2.x-p1.x)/dl;
    d.y:=(p2.y-p1.y)/dl;
  end;
end;

procedure turn(Var P: PP);
begin
  P[2].x:=P[2].x+P[1].x;
  P[2].y:=P[2].y-P[1].x;
  P[1].y:=P[2].y+2*P[1].x;
  P[1].x:=P[2].x-2*P[1].x;
end;

Procedure XLine(DC: HDC; var PL: Array of TPoint; PNum: Integer;
                Const XLN: Array of TXLineDef; XNum: Integer;
                LCol,FCol: TColor; Switch: Boolean; Zoom: Double; Close: Integer);
{ Hauptprozedur, auf der alle exportierten Prozeduren basieren,
  welche entlang eines Polygonzuges die Linienelemente zeichnet.
  Parameter sind der Devicekontext(DC), ein Polygonzug(PL) mit
  Anzahl PNum, ein Array von Linienelementen, die sich beliebig
  kombinieren lassen (XLN) mit Anzahl XNum, die Farben des Randes
  (LCol) und der F�llung (FCol) von Linienelementen mit variabler
  Farbe (-2), ein Zoomfaktor und ein Entscheidungsparameter
  (Close=0 => Polygonzug wird geschlossen)    }

{++ New XLine BUG#251}
{The parameters of the function:
 DC: HDC;                       - sevice context
 var PL: Array of TPoint;       - the array of the line points
 PNum: Integer;                 - number of hte line points
 Const XLN: Array of TXLineDef; - the array of the user line style items
 XNum: Integer;                 - number of the user line style items
 LCol,                          - the line color of the user line style items
 FCol: TColor;                  - the fill color of the user line style items
 Switch: Boolean;               - I don't know what it meant and why it was required.
 Zoom: Double;                  - a scale
 Close: Integer);               - I think this parameters wasn't used because But for polygon
                                  objects we already have the first and the last point are the same.
                                  I think this parameters doesn't mean anything for a long time.
 Ivanoff.}
{-- New XLine BUG#251}

   Procedure RotateAndMovePoint(Xst, Yst: Integer; SinF, CosF: Double; var APoint: TPoint);
   Var
      X, Y: Integer;
   Begin
        X := APoint.X;
        Y := APoint.Y;
        APoint.X := Xst + Round(X * CosF - Y * SinF);
        APoint.Y := Yst + Round(X * SinF + Y * CosF);
   End;


Var
   iI, iJ, iK, iOffsetX, iLP: Integer;
   iDeltaX, iDeltaY: Integer;
   dblDiagLength, dblCosF, dblSinF: Double;
   Xmin, Xmax, Ymin, Ymax, XminRgn, XmaxRgn, YminRgn, YmaxRgn, X, Y, iMaxLineWidth: Integer;
   iItWasDrawn, iXst, iXend, iYst, iYend: Integer;
   bInterruptIt: Boolean;
   AnOldRegion, ARegion: HRGN;
   ARegionPoints: Array [0..3] Of TPoint;
   AnItemPoints: PP; // Array[1..PolyPoints] of TPoint;
   APen, AnOldPen: HPen;
   ABrush, AnOldBrush: HBrush;
   dblRadius: Double;
   iXcenter, iYcenter: Integer;
   clLineColor, clFillColor: TColor;
   iLineWidth: Integer;
   bTurnStyleByDirection,
   bClipExceed: Boolean;
Begin
     If (XNum<1) OR (PNum<2) Then EXIT;
{
  XminRgn, YminRgn +----------------+
                   !                !
                   +----------------+ XmaxRgn, YmaxRgn


        Xmin, Ymin           (Xmin + iOffestX), Ymin
             +---------------+---------------+
             !               !               !
             +---------------+---------------+
                        Xmax, Ymax           (Xmax + iOffestX), Ymax
}
     iItWasDrawn := 0;
     bTurnStyleByDirection := Boolean(Byte(XLN[0].ClipEdges) AND ci_XLine_TurnIt);
     bClipExceed := Boolean(Byte(XLN[0].ClipEdges) AND ci_XLine_ClipIt); 
     // Define alredy exist clip region.
     AnOldRegion := CreateRectRgn(0, 0, 5, 5);
     If GetClipRgn(DC, AnOldRegion) < 1 Then Begin
        Deleteobject(AnOldRegion);
        AnOldRegion := 0;
        End;
     TRY
        // Determine the boundaries of the region around all items of XLine style.
        XminRgn := 0;
        YminRgn := 0;
        XmaxRgn := 0;
        YmaxRgn := 0;
        iMaxLineWidth := 0;
        For iI:=0 To XNum-1 Do Begin
            Case XLN[iI].ItemType Of
            itCircle, itArc, itChord: Begin
                 // For circle, arc and chord items.
                 X := XLN[iI].Points[2].X - XLN[iI].Points[1].X;
                 Y := XLN[iI].Points[2].Y - XLN[iI].Points[1].X;
                 If X < XminRgn Then XminRgn := X;
                 If Y < YminRgn Then YminRgn := Y;
                 X := XLN[iI].Points[2].X + XLN[iI].Points[1].X;
                 Y := XLN[iI].Points[2].Y + XLN[iI].Points[1].X;
                 If X > XmaxRgn Then XmaxRgn := X;
                 If Y > YmaxRgn Then YmaxRgn := Y;
                 End;
            Else Begin
                 // For polygon and polyline items.
                 For iJ:=1 To XLN[iI].PointCount Do Begin
                     If XLN[iI].Points[iJ].X < XminRgn Then XminRgn := XLN[iI].Points[iJ].X;
                     If XLN[iI].Points[iJ].X > XmaxRgn Then XmaxRgn := XLN[iI].Points[iJ].X;
                     If XLN[iI].Points[iJ].Y < YminRgn Then YminRgn := XLN[iI].Points[iJ].Y;
                     If XLN[iI].Points[iJ].Y > YmaxRgn Then YmaxRgn := XLN[iI].Points[iJ].Y;
                     End; // For iJ end
                 End;
            End; // Case end
            // Determine also maximal line width (for correct clip rect boundaries calculating).
            If XLN[iI].LineWidth > iMaxLineWidth Then iMaxLineWidth := XLN[iI].LineWidth;
            End; // For iI end
        Ymin := - Round((YminRgn - iMaxLineWidth) * Zoom) + 5;
        Ymax := - Round((YmaxRgn + iMaxLineWidth) * Zoom) - 5;
        // This cycle by all segments of the line.
        For iLP:=0 To PNum-2 Do Begin
            // Get the points of the segment.
            iXst := PL[iLP].X;
            iYst := PL[iLP].Y;
            iXend := PL[iLP+1].X;
            iYend := PL[iLP+1].Y;

            If (iXst = iXend) AND (iYst = iYend) Then CONTINUE; // To avoid drawing between two points with the same coordinates.

            iDeltaX := iXend - iXst;
            iDeltaY := iYend - iYst;
            dblDiagLength := SqRt(Sqr(iDeltaX) + Sqr(iDeltaY));
            dblSinF := iDeltaY / dblDiagLength;
            dblCosF := iDeltaX / dblDiagLength;

            iI := 0;
            bInterruptIt := FALSE;
            While NOT bInterruptIt Do Begin
                  iOffsetX := Round(iI * XLN[0].Cycle) - iItWasDrawn;

                  Xmin := Round((XminRgn + iOffsetX) * Zoom);
                  If Xmin < -1 Then Xmin := -1;
                  Xmin := Xmin - Round(iMaxLineWidth * Zoom);

                  Xmax := Round((XLN[0].Cycle + iOffsetX) * Zoom);
                  If Xmax > dblDiagLength Then Begin
                     If bTurnStyleByDirection Then Begin
                        // If the line style demand turning mode, the style segment have to be clipped by the boundary of the section.
                        // The remains of the style segment have to be dranw at the start of the next line section.
                        iItWasDrawn := XLN[0].Cycle - Round((Xmax - dblDiagLength) / Zoom);
                        Xmax := Round(dblDiagLength);
                        End
                     Else Begin
                        // If the line style doesn't demand turning mode, the next line section will be start from zero.
                        // If the line style demand clipping mode, the line section have to be clipped by the boundary of the section.
                        // If the line style doesn't demand clipping mode, the line section have to be drawn to the end of the style segment.
                        iItWasDrawn := 0;
                        If bClipExceed Then
                           Xmax := Round(dblDiagLength);
                        End;
                     bInterruptIt := TRUE;
                     End;
                  // Determine the clip region unrotated and unmoved coordinates.
                  // We need some growing of clip region (but not in all cases).
                  If iI > 0 Then Xmin := Xmin - 5;
                  If NOT bInterruptIt Then Xmax := Xmax + 5; 

                  ARegionPoints[0].X := Xmin; ARegionPoints[0].Y :=  Ymin;
                  ARegionPoints[1].X := Xmax; ARegionPoints[1].Y :=  Ymin;
                  ARegionPoints[2].X := Xmax; ARegionPoints[2].Y :=  Ymax;
                  ARegionPoints[3].X := Xmin; ARegionPoints[3].Y :=  Ymax;
                  // Rotate clip region
                  RotateAndMovePoint(iXst, iYst, dblSinF, dblCosF, ARegionPoints[0]);
                  RotateAndMovePoint(iXst, iYst, dblSinF, dblCosF, ARegionPoints[1]);
                  RotateAndMovePoint(iXst, iYst, dblSinF, dblCosF, ARegionPoints[2]);
                  RotateAndMovePoint(iXst, iYst, dblSinF, dblCosF, ARegionPoints[3]);

// This is for debug only. It draws the clip region boundaries.
//                  PolyLine(DC, ARegionPoints, 4);

                  // Create new clip region and select it.
                  ARegion := CreatePolygonRgn(ARegionPoints, 4, 1);
{++ Ivanoff Bug#398}
                  // In the old clip region exists, combine just created region with it.
                  If AnOldRegion <> 0 Then
                     CombineRgn(ARegion, ARegion, AnOldRegion, RGN_AND);
{-- Ivanoff Bug#398}
                  SelectClipRgn(DC, ARegion);
                  TRY
                     For iJ:=0 To XNum-1 Do Begin
                         Case XLN[iJ].ItemType Of
                         itCircle,
                         itArc,
                         itChord: Begin
                              X := Round((XLN[iJ].Points[2].X + iOffsetX) * Zoom);
                              Y := - Round(XLN[iJ].Points[2].Y * Zoom);
                              iXcenter := iXst + Round(X * dblCosF - Y * dblSinF);
                              iYcenter := iYst + Round(X * dblSinF + Y * dblCosF);
                              dblRadius := XLN[iJ].Points[1].X * Zoom;

                              AnItemPoints[1].X := Round(iXcenter - dblRadius);
                              AnItemPoints[1].Y := Round(iYcenter - dblRadius);
                              AnItemPoints[2].X := Round(iXcenter + dblRadius);
                              AnItemPoints[2].Y := Round(iYcenter + dblRadius);
                              If XLN[iJ].ItemType = itCircle Then Begin
                                 AnItemPoints[3].X := AnItemPoints[1].X;
                                 AnItemPoints[3].Y := AnItemPoints[1].Y;
                                 AnItemPoints[4].X := AnItemPoints[3].X;
                                 AnItemPoints[4].Y := AnItemPoints[3].Y;
                                 End
                              Else Begin
                                 AnItemPoints[3].X := Round((XLN[iJ].Points[3].X + iOffsetX) * Zoom);
                                 AnItemPoints[3].Y := - Round(XLN[iJ].Points[3].Y * Zoom);
                                 RotateAndMovePoint(iXst, iYst, dblSinF, dblCosF, AnItemPoints[3]);

                                 AnItemPoints[4].X := Round((XLN[iJ].Points[4].X + iOffsetX) * Zoom);
                                 AnItemPoints[4].Y := - Round(XLN[iJ].Points[4].Y * Zoom);
                                 RotateAndMovePoint(iXst, iYst, dblSinF, dblCosF, AnItemPoints[4]);
                                 End;
                              End;
                         Else Begin // Case Else
                              For iK:=1 To XLN[iJ].PointCount Do Begin
                                  AnItemPoints[iK].X := Round((XLN[iJ].Points[iK].X + iOffsetX) * Zoom);
                                  AnItemPoints[iK].Y := - Round(XLN[iJ].Points[iK].Y * Zoom);
                                  RotateAndMovePoint(iXst, iYst, dblSinF, dblCosF, AnItemPoints[iK]);
                                  End; // For iK end
                              End;
                         End; // Case end
                         // Create required pen and brush and select them.
                         // Detect line width.
                         If XLN[iJ].LineWidth < 0 Then iLineWidth := -1
                         Else iLineWidth := Round(XLN[iJ].LineWidth * Zoom);
                         // Detect line color.
                         If XLN[iJ].LineColor = -1 Then iLineWidth := -1
                         Else
                            If XLN[iJ].LineColor < -1 Then clLineColor := LCol // Parameter of this function
                            Else clLineColor := XLN[iJ].LineColor;
                         // Detect fill color.
                         // The color > -1 - palette color.
                         // The color = -1 - no fill color.
                         // The color < -1 - a layer color? I understood so. Ivanoff.
                         If XLN[iJ].FillColor < -1 Then clFillColor := FCol // Parameter of this function
                         Else clFillColor := XLN[iJ].FillColor;
                         // Create the pen.
                         If iLineWidth < 0 Then APen := GetStockObject(NULL_PEN)
                         Else APen := CreatePen(PS_SOLID, iLineWidth, clLineColor);
                         // Create the brush.
                         If clFillColor <> -1 Then
                            ABrush := CreateSolidBrush(clFillColor)
                         Else
                            ABrush := GetStockObject(NULL_BRUSH);
                         // Storage the old pen and brush.
                         AnOldPen := SelectObject(DC, APen);
                         AnOldBrush := SelectObject(DC, ABrush);
                         // Draw the style segment item.
                         TRY
                            Case XLN[iJ].ItemType Of
                            itArc: Arc(DC, AnItemPoints[1].X, AnItemPoints[1].Y,
                                           AnItemPoints[2].X, AnItemPoints[2].Y,
                                           AnItemPoints[3].X, AnItemPoints[3].Y,
                                           AnItemPoints[4].X, AnItemPoints[4].Y);
                            itCircle,
                            itChord: Chord(DC, AnItemPoints[1].X, AnItemPoints[1].Y,
                                               AnItemPoints[2].X, AnItemPoints[2].Y,
                                               AnItemPoints[3].X, AnItemPoints[3].Y,
                                               AnItemPoints[4].X, AnItemPoints[4].Y);
                            itRectangle,
                            itPolygon: Polygon(DC, AnItemPoints, XLN[iJ].PointCount);
                            Else Polyline(DC, AnItemPoints, XLN[iJ].PointCount);
                            End; // Case end
                         FINALLY
                            // Restore old pen and brush and delete once that was just used.
                            SelectObject(DC, AnOldPen);
                            SelectObject(DC, AnOldBrush);
                            DeleteObject(APen);
                            DeleteObject(ABrush);
                         END;
                         End; // For iJ end
                  FINALLY
                     // Delete clip region and restore the old one.
                     DeleteObject(ARegion);
                     SelectClipRgn(DC, AnOldRegion);
                  END;
                  Inc(iI); // Make offset parameter.
                  End; // While NOT bInterruptIt end
            End; // For iLP end
     FINALLY
        // Delete the handle of old clip region.
        If AnOldRegion <> 0 Then
           DeleteObject(AnOldRegion);
     END;
End;

(*************************************************************************)
(*************************************************************************)
(*************************************************************************)

Procedure XLineTo(DC: HDC; X2,Y2: Integer;
                  Const XLN: Array of TXLineDef; XNum: Integer;
                  LCol,FCol: TColor; Switch: Boolean; Zoom: Double);

Var Poly: Array[1..2] of TPoint;
    P:                   TPoint;
Begin
  MovetoEx(DC,X2,Y2,@P);
  Poly[1]:=P;
  Poly[2].x:=X2;
  Poly[2].y:=Y2;
  XLine(DC,Poly,2,XLN,XNum,LCol,FCol,Switch,Zoom,1);
End;

(*************************************************************************)

Procedure XPolyLine(DC: HDC; var PL: Array of TPoint; PNum: Integer;
                    Const XLN: Array of TXLineDef; XNum: Integer;
                    LCol,FCol: TColor; Switch: Boolean; Zoom: Double);
Begin
  XLine(DC,PL,PNum,XLN,XNum,LCol,FCol,Switch,Zoom,1);
End;

(*************************************************************************)

Procedure XPolygon(DC: HDC; var PL: Array of TPoint; PNum: Integer;
                   Const XLN: Array of TXLineDef; XNum: Integer;
                   LCol,FCol: TColor; Switch: Boolean; Zoom: Double);
Begin
  XLine(DC,PL,PNum,XLN,XNum,LCol,FCol,Switch,Zoom,0);
End;

(*************************************************************************)
Procedure XBow(DC: HDC; X,Y,A,B: Integer; P,Q,R: Double; PNum: Integer;
               Const XLN: Array of TXLineDef; XNum: Integer;
               LCol,FCol: TColor; Switch: Boolean; Zoom: Double; close: Integer);
{ Basisprozedur f�r Ellipsen und Kreisb�gen.
  (X,Y) - Mittelpunkt der Ellipse
   A,B  - Hauptachsen
   P    - Anfangswinkel
   Q    - Endwinkel
   R    - Drehwinkel
}
Var poly:  Array[0..199] of TPoint;
    i:     Integer;
    d,e,f: Double;
Begin
  if PNum>200 then PNum:=200;
  d:=(p-q)/(PNum-1);
  e:=cos(r);
  f:=-sin(r);
  p:=-p;
  for i:=0 to PNum-1 do begin
    poly[i].x:=round(X+A*cos(p)*e-B*sin(p)*f);
    poly[i].y:=round(Y+A*cos(p)*f+B*sin(p)*e);
    p:=p+d;
  end;
{++ New XLine BUG#251}
// In the circle the last point must be the same as the first.
  Poly[PNum-1].X := Poly[0].X;
  Poly[PNum-1].Y := Poly[0].Y;
{-- New XLine BUG#251}
  XLine(DC,poly,PNum,XLN,XNum,LCol,FCol,not Switch,Zoom,close);
End;

(*************************************************************************)

Procedure XEllipse(DC: HDC; X,Y,A,B: Integer; R: Double;
                   Const XLN: Array of TXLineDef; XNum: Integer;
                   LCol,FCol: TColor; Switch: Boolean; Zoom: Double);
Var n: Integer;
Begin
  n:=20+round(sqrt(a+b));
  XBow(DC,x,y,a,b,0,pi*(2-1/n),r,n,XLN,XNum,LCol,FCol,Switch,Zoom,0);
End;

(*************************************************************************)

Procedure XArc(DC: HDC; X,Y,A,B:Integer; P,Q,R: Double;
               Const XLN: Array of TXLineDef; XNum: Integer;
               LCol,FCol: TColor; Switch: Boolean; Zoom: Double);
Var v,w: Double;
    n:   Integer;

  function arctan2(y,x: Double): Double;
  var a: Double;
  begin
    if x=0 then a:=pi/2 else a:=arctan(y/x);
    if (y=0) and (x<0) then a:=pi;
    if (a>0) and (y<0) then a:=a-pi;
    if (a<0) and (y>0) then a:=a+pi;
    arctan2:=a;
  end;

Begin
  v:=arctan2(a*sin(p),b*cos(p));
  w:=arctan2(a*sin(q),b*cos(q));
  if w<v then w:=w+2*pi;
  n:=round(10+(sqrt(a+b)*(w-v)/(2*pi)));
  XBow(DC,x,y,a,b,v,w,r,n,XLN,XNum,LCol,FCol,Switch,Zoom,1);
End;

Procedure XLFill(DC: HDC; X1,Y1,X2,Y2: Integer; ClRgn: HRgn;
                 Const XFL: TXFillDefs; FNum: Integer;
                 LC,FC: TColor; Zoom, Rotation: Double);

Type TXLineStyle = Record
       p:            array [1..PolyPoints] of Vector;
       cyc:          Double;
       typ,pnum:     Integer;
       co1,co2:      TColor;
       wid,ft,fw,fd: Integer;
       xmin,xmax:    Double;
       ymin,ymax:    Double;
     End;
     PXLineStyleArray = ^XLineStyleArray;
     XLineStyleArray = Array[0..0] of TXLineStyle;

  Procedure XLine(DC: HDC; X1,Y1: Integer; d: Vector;
                  dl,ext: Double; xlt: PXLineStyleArray; xnum:Integer);

  var n:              Double;
      mx,my,nx,ny,j   :Integer;
      Poly:           PP;
      Pen,OldPen:     HPen;
      Brush,OldBrush: HBrush;

    Procedure FillPoly(P:PP; var t,w,d: Integer; var col: TColor);
    Var x1,y1,x2,y2: Integer;
        xl,yl,i,l:   Integer;
        rgn:         HRgn;
        pen,oldpen:  HPen;
        p1,p2:       TPoint;
    Begin
      for i:=1 to 4 do begin
        if (i=1) or (P[i].x<x1) then x1:=P[i].x;
        if (i=1) or (P[i].x>x2) then x2:=P[i].x;
        if (i=1) or (P[i].y<y1) then y1:=P[i].y;
        if (i=1) or (P[i].y>y2) then y2:=P[i].y;
      end;
      l:=2*(abs(p[1].x-p[2].x)+abs(p[1].y-p[2].y));
      if l<200 then l:=200;
      if (x2-x1>l) and (y2-y1>l) then begin
        p1:=p[3];
        p2:=p[4];
        p[3].x:=(p[2].x+p1.x) div 2;
        p[3].y:=(p[2].y+p1.y) div 2;
        p[4].x:=(p[1].x+p2.x) div 2;
        p[4].y:=(p[1].y+p2.y) div 2;
        fillpoly(p,t,w,d,col);
        p[1]:=p2;
        p[2]:=p1;
        fillpoly(p,t,w,d,col);
      end else begin
        pen:=CreatePen(ps_Solid,w,col);
        oldpen:=SelectObject(DC,pen);
        rgn:=CreatePolygonRgn(P,4,1);
        CombineRgn(rgn,rgn,ClRgn,rgn_and);
        SelectClipRgn(DC,rgn);
        x1:=x1-(x1 mod d);
        y1:=y1-(y1 mod d);
        x2:=x2-(x2 mod d)+d;
        y2:=y2-(y2 mod d)+d;
        xl:=x2-x1;
        yl:=y2-y1;
        if (t=1) or (t=3) then
        for i:=0 to xl div d do begin
          movetoex(DC,x1+i*d,y2,@p1);
          lineto(DC,x1+i*d,y1);
        end;
        if (t=2) or (t=3) then
        for i:=0 to yl div d do begin
          movetoex(DC,x1,y2-i*d,@p1);
          lineto(DC,x2,y2-i*d);
        end;
        if (t=4) or (t=6) then if xl>yl then
        for i:=0 to (xl+yl) div d do begin
            movetoex(DC,x1+i*d,y2,@p1);
            lineto(DC,x1+i*d-yl,y1);
        end else
        for i:=0 to (xl+yl) div d do begin
          movetoex(DC,x2,y2-i*d+xl,@p1);
          lineto(DC,x1,y2-i*d);
        end;
        if (t=5) or (t=6) then if xl>yl then
        for i:=0 to (xl+yl) div d do begin
            movetoex(DC,x1+i*d-yl,y2,@p1);
            lineto(DC,x1+i*d,y1);
        end else
        for i:=0 to (xl+yl) div d do begin
          movetoex(DC,x1,y2-i*d+xl,@p1);
          lineto(DC,x2,y2-i*d);
        end;
        SelectObject(DC,oldpen);
        DeleteObject(pen);
        DeleteObject(rgn);
        SelectClipRgn(DC,ClRgn)
      end;
    end;

  var Cnt : Integer;
  Begin
    for Cnt:=0 to XNum-1 do with xlt^[Cnt] do begin
     if (ymax-ymin>=1) or (ymax-ymin=0) then begin

      if wid<0 then Pen:=GetStockObject(null_pen)
               else Pen:=CreatePen(ps_Solid,wid,co1);
      if ft=0 then Brush:=CreateSolidBrush(co2)
              else Brush:=GetStockObject(null_brush);
      OldPen:=SelectObject(DC,Pen);
      OldBrush:=SelectObject(DC,Brush);
      if cyc>0 then begin
        for j:=1 to pnum do begin
          Poly[j].x:=X1+trunc(P[j].x)-round(d.x*ext);
          Poly[j].y:=Y1+trunc(P[j].y)-round(d.y*ext);
        end;
        n:=0; mx:=0; my:=0;
        while (n+xmin<dl+ext) do begin
          case typ of
            itArc: Arc(DC,Poly[1].x,Poly[1].y,Poly[2].x,Poly[2].y,
                               Poly[3].x,Poly[3].y,Poly[4].x,Poly[4].y);
            itCircle,itChord: Chord(DC,Poly[1].x,Poly[1].y,Poly[2].x,Poly[2].y,
                               Poly[3].x,Poly[3].y,Poly[4].x,Poly[4].y);
            itRectangle,itPolygon: Polygon(DC,Poly,pnum);
            else Polyline(DC,Poly,pnum);
          end;
          n:=n+cyc;
          nx:=round(d.x*n);
          ny:=round(d.y*n);
          for j:=1 to pnum do begin
            Poly[j].x:=Poly[j].x+nx-mx;
            Poly[j].y:=Poly[j].y+ny-my;
          end;
          mx:=nx;
          my:=ny;
        end;
      end else begin
        Poly[1].x:=X1-round(d.y*ymin);
        Poly[1].y:=Y1+round(d.x*ymin);
        Poly[2].x:=X1-round(d.y*ymax);
        Poly[2].y:=Y1+round(d.x*ymax);
        Poly[3].x:=Poly[2].x+round(d.x*dl);
        Poly[3].y:=Poly[2].y+round(d.y*dl);
        Poly[4].x:=Poly[1].x+round(d.x*dl);
        Poly[4].y:=Poly[1].y+round(d.y*dl);
        if ft>0 then FillPoly(Poly,ft,fw,fd,co2);
        Polygon(DC,Poly,4);
      end;

      SelectObject(DC,OldPen);
      SelectObject(DC,OldBrush);
      DeleteObject(Brush);
      DeleteObject(Pen);
     end;
    end;
  End;

  function rmod(const x,r: Double): Double;
  var y: Double;
  begin
    y:=x/r;
    y:=(y-int(y))*r;
    if y<0 then y:=y+r;
    rmod:=y;
  end;

  function rdiv(const x,r: Double): Integer;
  var y: Double;
  begin
    y:=x/r;
    if y>0 then y:=int(y) else y:=int(y)-1;
    rdiv:=trunc(y);
  end;

(****************************************************************************)

Var i,j,k,n:Integer;
    xlt: PXLineStyleArray;
    d:      Vector;
    dx,dy:  Double;
    dl,ds:  Double;
    r,ex,z: Double;
    pn,opn: HPen;
    p:      TPoint;
    a1,a2,b1,b2: Integer;
    m,c,ph,q,xo,yo: Double;
    u1,v1,u2,v2: Double;
Begin
  //Zoom:=0.037;
  if Zoom>0 then for k:=0 to FNum-1 do with XFL[k] do
  if (LineWidth>0) or (style<>1000) then begin
    r:=1.0;
    z:=zoom*LineWidth;
    d.x:=cos(angle+Rotation);
    d.y:=-sin(angle+Rotation);
    m:=0;
    n:=0;
    if style=1000 then GetMem(XLT,XLineStyle^.Count*SizeOf(TXLineStyle))
    else XLT:=NIL;
    if (style=1000) then for j:=0 to XLineStyle^.Count-1 do with XLineStyle^.XLineDefs[j] do begin
      with XLT^[j] do begin
        pnum:=PointCount;
        for i:=1 to pnum do begin
          P[i].x:=Points[i].x*z;
          P[i].y:=-Points[i].y*z;
          if (i=1) or (P[i].x<xmin) then xmin:=P[i].x;
          if (i=1) or (P[i].x>xmax) then xmax:=P[i].x;
          if (i=1) or (P[i].y<ymin) then ymin:=P[i].y;
          if (i=1) or (P[i].y>ymax) then ymax:=P[i].y;
        end;
        typ:=ItemType;
        if typ<itRectangle then begin
          xmin:=P[2].x-P[1].x;
          xmax:=P[2].x+P[1].x;
          ymin:=P[2].y-P[1].x;
          ymax:=P[2].y+P[1].x;
        end;
        for i:=1 to pnum do if (i>1) or (typ>=itRectangle) then begin
          ex:=round(d.y*P[i].x+d.x*P[i].y);
          P[i].x:=round(d.x*P[i].x-d.y*P[i].y);
          P[i].y:=ex;
        end;
        if typ<itRectangle then begin
          P[2].x:=P[2].x+P[1].x;
          P[2].y:=P[2].y-P[1].x;
          P[1].y:=P[2].y+2*P[1].x;
          P[1].x:=P[2].x-2*P[1].x;
        end else if (Points[1].x-Points[2].x=Points[4].x-Points[3].x)
        and (Points[1].y-Points[2].y=Points[4].y-Points[3].y) then begin
          P[1].x:=P[2].x-P[3].x+P[4].x;
          P[1].y:=P[2].y-P[3].y+P[4].y;
        end;
        if ymax>n then n:=round(ymax);
        if ymin<m then m:=round(ymin);
        cyc:=cycle*z;
        if cyc>r then r:=cyc;
        if LineWidth<0 then wid:=-1 else wid:=round(LineWidth*zoom);
        if LineColor=-1 then wid:=-1 else
        if LineColor<-1 then co1:=XFL[k].LineColor else co1:=LineColor;
        if FillColor=-1 then FillStyle:=-1 else
        if FillColor<-1 then co2:=XFL[k].FillColor else co2:=FillColor;
        if co1=-1 then wid:=-1 else if co1<-1 then co1:=lc;
        if co2=-1 then FillStyle:=-1 else if co2<-1 then co2:=fc;
        if (FillStyle>0) then if (cyc<=0) then begin
          fd:=FillStyle;
          ft:=fd and $FF;
          fd:=fd shr 8;
          fw:=fd and $FF;
          fw:=round(fw*z);
          fd:=fd shr 8;
          fd:=fd and $FF;
          fd:=round(fd*z);
          if fd=0 then ft:=0;
        end else ft:=0 else ft:=FillStyle;
      end;
    end;
    q:=round(r);
    if (q=1) and (n-m>1) then q:=n-m;

    u1:=x1;
    v1:=y1;
    u2:=x2;
    v2:=y2;
    xo:=rmod(XOffset*zoom,r);
    ph:=rmod(phase*zoom,r);
    if abs(d.y)<abs(d.x) then begin
      c:=abs(cycle*zoom/d.x);
      if c<1 then c:=1;
      yo:=rmod(YOffset*zoom/d.x,c);
      dy:=u1*d.y/d.x;
      dl:=abs((u2-u1)/d.x);
      ds:=dl*d.y;
      if d.y<0 then v1:=v1-ds else v2:=v2-ds;
      v1:=dy+c*(rdiv(v1-dy,c)+1);
      v2:=dy+c*rdiv(v2-dy,c);
      ex:=rmod(-xo+yo*d.y,r);
      n:=rdiv(v1-v2,c);
      v1:=v1+yo;
      i:=n;
      if style<>1000 then begin
        if LineColor<0 then pn:=CreatePen(style,round(LineWidth*zoom),LC)
        else pn:=CreatePen(style,round(LineWidth*zoom),LineColor);
        opn:=SelectObject(DC,pn);
        if d.x<0 then u1:=u2;
        u2:=u1+dl*d.x;
        v2:=v1+dl*d.y;
        a1:=round(u1);
        a2:=round(u2);
        while i>=0 do begin
          movetoex(DC,a1,round(v1),@p);
          lineto(DC,a2,round(v2));
          dec(i);
          v1:=v1-c;
          v2:=v2-c;
        end;
        SelectObject(DC,opn);
        DeleteObject(pn);
      end else if xlt<>Nil then begin
        a1:=round(u1);
        a2:=round(u2);
        while i>=0 do begin        
          if d.x<0 then XLine(DC,a2,round(v1),d,dl,ex,xlt, XLineStyle^.Count)
                   else XLine(DC,a1,round(v1),d,dl,ex,xlt, XLineStyle^.Count);
          dec(i);
          v1:=v1-c;
          ex:=rmod(ex-c*d.y-ph,r);      
        end;
      end;
    end else begin
      c:=abs(cycle*zoom/d.y);
      if c<1 then c:=1;
      yo:=rmod(YOffset*zoom/d.y,c);
      dx:=v2*d.x/d.y;
      dl:=abs((v1-v2)/d.y);
      ds:=dl*d.x;
      if d.x<0 then u2:=u2-ds else u1:=u1-ds;
      u2:=dx+c*(rdiv(u2-dx,c)+1);
      u1:=dx+c*rdiv(u1-dx,c);
      ex:=rmod(-xo+yo*d.x,r);
      n:=rdiv(u2-u1,c);
      i:=0;
      u2:=u1+yo;
      if style<>1000 then begin
        if LineColor<0 then pn:=CreatePen(style,round(LineWidth*zoom),LC)
        else pn:=CreatePen(style,round(LineWidth*zoom),LineColor);
        opn:=SelectObject(DC,pn);
        if d.y<0 then v2:=v1;
        u1:=u2+dl*d.x;
        v1:=v2+dl*d.y;
        b1:=round(v1);
        b2:=round(v2);
        while i<=n do begin
          movetoex(DC,round(u2),b2,@p);
          lineto(DC,round(u1),b1);
          inc(i);
          u1:=u1+c;
          u2:=u2+c;
        end;
        SelectObject(DC,opn);
        DeleteObject(pn);
      end else if xlt<>Nil then begin
        b1:=round(v1);
        b2:=round(v2);
        while i<=n do begin
          if d.y<0 then XLine(DC,round(u2),b1,d,dl,ex,xlt, XLineStyle^.Count)
                   else XLine(DC,round(u2),b2,d,dl,ex,xlt, XLineStyle^.Count);
          inc(i);
          u2:=u2+c;
          ex:=rmod(ex+c*d.x+ph,r);
        end;
      end;
    end;
    if XLT<>NIL then FreeMem(XLT,XLineStyle^.Count*SizeOf(TXLineStyle));
  end;
End;


(*************************************************************************)


Procedure BMFill(DC: HDC; X1,Y1,X2,Y2: Integer;
                 BM: HBitmap; Mode: Integer; Zoom: Double);
Var map:   Windows.TBitmap;
    mapDC: HDC;
    oldmp: HBitmap;
    dx,dy: Integer;
    x,y:   Integer;
Begin
  if GetObject(BM,sizeof(Windows.TBitmap),@map)=0 then exit;
  dx:=round(map.bmWidth*Zoom);
  dy:=round(map.bmHeight*Zoom);
  if (dx<=1) or (dy<=1) then exit;
  x1:=x1-(x1 mod dx);
  y1:=y1-(y1 mod dy);
  x2:=x2-(x2 mod dx)+dx;
  y2:=y2-(y2 mod dy)-dy;

  mapDC:=CreateCompatibleDC(DC);
  oldmp:=SelectObject(mapDC,BM);

  y:=y1;
  while y>y2 do begin
    x:=x1; 
    while x<x2 do begin
      StretchBlt(DC,x,y,dx,dy,mapDC,0,0,map.bmWidth,map.bmHeight,mode);
      x:=x+dx;
    end;
    y:=y-dy;
  end;

  SelectObject(mapDC,oldmp);
  DeleteObject(mapDC);
End;


(*************************************************************************)
(*************************************************************************)

 Function PolytoRgn(DC: HDC; var PL: Array of TPoint; PNum: Integer): HRgn;
 Var Rgn,R2: HRgn;
     Pt:     TPoint;
     C,i:    Integer;
 Begin
   C:=16000;
   i:=0;
   repeat
     if PNum-i<C then C:=PNum-i;
     if i=0 then begin
       Rgn:=CreatePolygonRgn(PL,C,1);
       i:=C-2;
     end else begin
       Pt:=PL[i];
       PL[i]:=PL[0];
       R2:=CreatePolygonRgn(PL[i],C,1);
       CombineRgn(Rgn,Rgn,R2,Rgn_xor);
       PL[i]:=Pt;
       i:=i+C-2;
     end;
   until C<16000;
   if PNum>=16000 then DeleteObject(R2);
   PolytoRgn:=Rgn;
 End;


Procedure XLFillEllipse(DC: HDC; X1,Y1,X2,Y2: Integer;
                        Const XFL: TXFillDefs; FNum: Integer;
                        LineColor,FillColor: TColor; Const Zoom, Rotation: Double); 
Var Rgn,OldRgn: HRgn;
Begin
  Rgn:=CreateEllipticRgn(X1,Y1,X2,Y2);
  if Rgn<>0 then begin
    OldRgn:=CreateRectRgn(0,0,5,5);
    if GetClipRgn(DC,OldRgn)<1 then begin
      DeleteObject(OldRgn);
      OldRgn:=0;
    end
    else CombineRgn(Rgn,Rgn,OldRgn,rgn_and);
    SelectClipRgn(DC,Rgn);
    XLFill(DC,X1,Y1,X2,Y2,Rgn,XFL,FNum,LineColor,FillColor,Zoom,Rotation);
    SelectClipRgn(DC,OldRgn);
    if OldRgn<>0 then DeleteObject(OldRgn);
    DeleteObject(Rgn);
  end;  
End;

Procedure XLFillPolygon(DC: HDC; var PL: Array of TPoint; PNum: Integer;
                        Const XFL: TXFillDefs; FNum: Integer;
                        LineColor,FillColor: TColor; Const Zoom, Rotation: Double);
Var Rgn,OldRgn: HRgn;
    Rec:        TRect;
Begin
  Rgn:=PolytoRgn(DC,PL,PNum);
  if Rgn<>0 then begin
    OldRgn:=CreateRectRgn(0,0,5,5);
    if GetClipRgn(DC,OldRgn)<1 then begin
      DeleteObject(OldRgn);
      OldRgn:=0;
    end
    else CombineRgn(Rgn,Rgn,OldRgn,rgn_and);
    SelectClipRgn(DC,Rgn);
    GetClipBox(DC,Rec);
    with Rec do XLFill(DC,left,bottom,right,top,Rgn,XFL,FNum,LineColor,FillColor,
        Zoom,Rotation);
    SelectClipRgn(DC,OldRgn);
    if OldRgn<>0 then DeleteObject(OldRgn);
    DeleteObject(Rgn);
  end;  
End;

Procedure BMFillEllipse(DC: HDC; X1,Y1,X2,Y2: Integer;
                        BM: HBitmap; Mode: Integer; Zoom: Double); 
Var Rgn,OldRgn: HRgn;
Begin
  Rgn:=CreateEllipticRgn(X1,Y1,X2,Y2);
  if Rgn<>0 then begin
    OldRgn:=CreateRectRgn(0,0,5,5);
    if GetClipRgn(DC,OldRgn)<1 then begin
      DeleteObject(OldRgn);
      OldRgn:=0;
    end
    else CombineRgn(Rgn,Rgn,OldRgn,rgn_and);
    SelectClipRgn(DC,Rgn);
    BMFill(DC,X1,Y1,X2,Y2,BM,Mode,Zoom);
    SelectClipRgn(DC,OldRgn);
    if OldRgn<>0 then DeleteObject(OldRgn);
    DeleteObject(Rgn);
  end;  
End;


Procedure BMFillPolygon(DC: HDC; var PL: Array of TPoint; PNum: Integer;
                        BM: HBitmap; Mode: Integer; Zoom: Double);
Var Rgn,OldRgn: HRgn;
    Rec:        TRect;
Begin
  Rgn:=PolytoRgn(DC,PL,PNum);
  if Rgn<>0 then begin
    OldRgn:=CreateRectRgn(0,0,5,5);
    if GetClipRgn(DC,OldRgn)<1 then begin
      DeleteObject(OldRgn);
      OldRgn:=0;
    end
    else CombineRgn(Rgn,Rgn,OldRgn,rgn_and);
    SelectClipRgn(DC,Rgn);
    GetClipBox(DC,Rec);
    with Rec do BMFill(DC,left,bottom,right,top,BM,Mode,Zoom);
    SelectClipRgn(DC,OldRgn);
    if OldRgn<>0 then DeleteObject(OldRgn);
    DeleteObject(Rgn);
  end;  
End;

end.

