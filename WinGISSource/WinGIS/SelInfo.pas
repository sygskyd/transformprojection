{******************************************************************************+
  Unit SelInfo                                                                 
--------------------------------------------------------------------------------
  Autor: Martin Forst
--------------------------------------------------------------------------------
  Verwaltet Informationen über die selektierten Objekte eines Projekts.
   - Gesamtanzahl der selektierten Objekte
   - Anzahl der selektierten Objekte eines bestimmten Typs
   - Abfrage, ob nur Objekte des selben Types selektiert sind
   - Abfrage des Statuszeilen-Textes
  Für jeden Objekttyp muß ein eigenes Objekt von TSelectInfoItem abgeleitet
  und zumindest die Methode GetStatusText überschrieben werden.
+******************************************************************************}
Unit SelInfo;

Interface

Uses AM_Def,AM_View,Classes,Hash,uview, am_dde;

Type TSelectInfoItem    = Class;

     {*************************************************************************+
       Class TSelectInfo
     ---------------------------------------------------------------------------
       Author: Martin Forst
     ---------------------------------------------------------------------------
     +*************************************************************************}
     TSelectInfo        = Class
      Private
       FItems           : Array[0..LastObjectType] of TSelectInfoItem;
       FCount           : LongInt;
       FHspCount        : Longint;
       FHsdCount        : Longint;
       FDefaultInfo     : TSelectInfoItem;
       Function    GetInfoToType(AType:LongInt):TSelectInfoItem;
       Function    GetSelCount(ObjectType:LongInt):LongInt;
       Function    GetSelectedType:LongInt;
      Protected
       Function    GetStatusText:String; virtual;
      Public
       Constructor Create;
       Destructor  Destroy; override;
       Procedure   Add(AItem:PView);
       Procedure   AddHsd(AItem:CView);
       Procedure   Clear;
       Property    Count:LongInt read FCount;
       Property    HspCount:Longint read FHspCount;
       Property    HsdCount:Longint read FHsdCount;
       Procedure   Delete(AItem:PView;ALast:PView;ALastHsd:CView);
       Procedure   DeleteHsd(AItem:CView;ALast:PView;AlastHsd:CView);
       Property    Infos[AType:LongInt]:TSelectInfoItem read GetInfoToType; default;
       Property    StatusText:String read GetStatusText;
       Property    SelectCount[ObjectType:LongInt]:LongInt read GetSelCount;
       Property    SelectedType:LongInt read GetSelectedType;
     end;

     {**************************************************************************
     | Class TSelectInfoItem
     |--------------------------------------------------------------------------
     | Author: Martin Forst
     |--------------------------------------------------------------------------
     +*************************************************************************}
     TSelectInfoItem    = Class
      Protected
       FCount      : LongInt;
       FHspCount   : Longint;
       FHsdCount   : Longint;
       FObjectType : LongInt;
       FOwner      : TSelectInfo;
       FIndex      : Integer;
       FGUID       : TGUID;
       Function    GetStatusText:String; virtual;
      Public
       Constructor Create(AOwner:TSelectInfo;AType:LongInt);
       Destructor  Destroy; override;
       Procedure   Add(AItem:PView); virtual;
       Procedure   AddHsd(AItem:CView); virtual;
       Procedure   Clear;
       Property    Count:LongInt read FCount write FCount;
       Procedure   Delete(AItem:PView); virtual;
       Procedure   DeleteHsd(AItem:CView); virtual;
       Property    ObjectType:LongInt read FObjectType write FObjectType;
       Property    StatusText:String read GetStatusText;
       Property    HspCount:Longint read FHspCount;
       Property    HsdCount:Longint read FHsdCount;
     end;

{==============================================================================+
  Objekttypen-spezifische Infos
+==============================================================================}

Type TPointInfo    = Class(TSelectInfoItem)
      Private
       FXPosition  : Double;
       FYPosition  : Double;
      Protected
       Function    GetStatusText:String; override;
      Public
       Procedure   Add(AItem:PView); override;
       Procedure   AddHsd(AItem:CView); override;
     end;

     TAreaInfo     = Class(TSelectInfoItem)
      Private
       FArea       : Double;
       FLength     : Double;
       FPoints     : LongInt;
      Protected
       Function    GetStatusText:String; override;
      Public
       Procedure   Add(AItem:PView); override;
       Procedure   AddHsd(AItem:CView); override;
       Property    Area:Double read FArea;
       Procedure   Delete(AItem:PView); override;
       Procedure   DeleteHsd(AItem:CView); override;
       Property    Length:Double read FLength;
       Property    NodeCount:Integer read FPoints;
     end;

     TLineInfo     = Class(TSelectInfoItem)
      Private
       FLength     : Double;
       FPoints     : LongInt;
      Protected
       Function    GetStatusText:String; override;
      Public
       Procedure   Add(AItem:PView); override;
       Procedure   AddHsd(AItem:CView); override;
       Procedure   Delete(AItem:PView); override;
       Procedure   DeleteHsd(AItem:CView); override;
       Property    Length:Double read FLength;
       Property    NodeCount:Integer read FPoints;
     end;

     TEllipseInfo  = Class(TSelectInfoItem)
      Private
       FXPosition       : Double;
       FYPosition       : Double;
       FPrimaryAxis     : Double;
       FArea            : Double;
       FPerimeter       : Double;
      Protected
       Function    GetStatusText:String; override;
      Public
       Procedure   Add(AItem:PView); override;
       Procedure   AddHsd(AItem:CView); override;
       Property    Area:Double read FArea;
       Procedure   Delete(AItem:PView); override;
       Procedure   DeleteHsd(AItem:CView); override;
       Property    Perimeter:Double read FPerimeter;
     end;

     TEllipseArcInfo    = Class(TSelectInfoItem)
      Private
       FXPosition       : Double;
       FYPosition       : Double;
       FPrimaryAxis     : Double;
       FBeginAngle      : Double;
       FEndAngle        : Double;
       FLength          : Double;
      Protected
       Function    GetStatusText:String; override;
      Public
       Procedure   Add(AItem:PView); override;
       Procedure   AddHsd(AItem:CView); override;
       Procedure   Delete(AItem:PView); override;
       Procedure   DeleteHsd(AItem:CView); override;
       Property    Length:Double read FLength;
     end;

     TImageInfo         = Class(TSelectInfoItem)
      Private
       FWidth           : LongInt;
       FHeight          : LongInt;
       FFileName        : AnsiString;
      Protected
       Function    GetStatusText:String; override;
      Public
       Procedure   Add(AItem:PView); override;
       Property    Width:LongInt read FWidth;
       Property    Height:LongInt read FHeight;
       Property    FileName:AnsiString read FFileName;
     end;

     TSplineInfo        = Class(TSelectInfoItem)
      Protected
       Function    GetStatusText:String; override;
      Public
       Procedure   Add(AItem:PView); override;
       Procedure   AddHsd(AItem:CView); override;
     end;

     TSymbolInfo        = Class(TSelectInfoItem)
      Protected
 {+++ Added and Changed by Brovak}
       FXPosition  : Double;
       FYPosition  : Double;
       FName       : String;
       Function    GetStatusText:String; override;
      Public
       Procedure   Add(AItem:PView); override;
       Procedure   AddHsd(AItem:CView); override;
 {--Brovak}
     end;

     TMeasurementInfo   = Class(TSelectInfoItem)
      Protected
       Function    GetStatusText:String; override;
      Public
       Procedure   Add(AItem:PView); override;
       //Procedure   AddHsd(AItem:CView); override;
     end;

     TDiagramInfo      = Class(TSelectInfoItem)
      Protected
       Function    GetStatusText:String; override;
      Public
       Procedure   Add(AItem:PView); override;
     end;

     TTextInfo          = Class(TSelectInfoItem)
      Private
       FText       : AnsiString;
       FXPosition  : Double;
       FYPosition  : Double;
       FAngle      : Double;
       FFontName   : AnsiString;
       FFontHeight : Double;
       FIsItAnnot  : boolean;
      Protected
       Function    GetStatusText:String; override;
      Public
       Procedure   Add(AItem:PView); override;
       Procedure   AddHsd(AItem:CView); override;
       Property    Text:AnsiString read FText;
       Property    XPosition:Double read FXPosition;
       Property    YPosition:Double read FYPosition;
       Property    Rotation:Double read FAngle;
       Property    FontName:AnsiString read FFontName;
       Property    FontHeight:Double read FFontHeight;
       Property    IsItAnnot:boolean read FIsItAnnot;
     end;

     TRTextInfo         = Class(TSelectInfoItem)
      Private
       FXPosition  : Double;
       FYPosition  : Double;
       FFontName   : AnsiString;
       FFontHeight : Double;
      Protected
       Function    GetStatusText:String; override;
      Public
       Procedure   Add(AItem:PView); override;
       Property    XPosition:Double read FXPosition;
       Property    YPosition:Double read FYPosition;
       Property    FontName:AnsiString read FFontName;
       Property    FontHeight:Double read FFontHeight;
     end;

     TOLEInfo           = Class(TSelectInfoItem)
      Protected
       Function    GetStatusText:String; override;
      Public
       Procedure   Add(AItem:PView); override;
     end;

     PSelOrderEntry     = ^TSelOrderEntry;
     TSelOrderEntry     = Packed Record
       Index            : Integer;
       Previous         : Integer;
       Next             : Integer;
     end;

     TSelOrderList      = Class
      Private
       FFirst           : Integer;
       FItems           : TAutoRecHashTable;
       FLast            : Integer;
       FCurrent         : Integer;
       Function    Find(Item:Integer):PSelOrderEntry;
       Function    GetCount:Integer;
      Public
       Constructor Create;
       Destructor  Destroy; override;
       Procedure   Append(Item:Integer);
       Procedure   Clear;
       Property    Count:Integer read GetCount;
       Property    Current:Integer read FCurrent;
       Procedure   Delete(Item:Integer);
       Procedure   Next;
     end;

Function CreateSelectInfo:TSelectInfo;

Implementation

Uses AM_BuGra,AM_Circl,AM_CPoly,BMPImage,AM_Meas,AM_OLE,AM_Point,AM_Poly,
     AM_Splin,AM_Sym,AM_Text,SysUtils,WinTypes,UserIntf,AM_RText,
     upixel,upoly,ucpoly,utext,usymbol,uptlst,ucirc,am_main,coordinatesystem,AM_Ini,
     ComObj,GPSConn;

Const StringSection  : Integer = -1;

{==============================================================================+
  TSelectInfo                                                                   
+==============================================================================}

Constructor TSelectInfo.Create;
  begin
    inherited Create;
    FillChar(FItems,SizeOf(FItems),#0);
    FDefaultInfo:=TSelectInfoItem.Create(NIL,0);
  end;

Destructor TSelectInfo.Destroy;
  var Cnt          : Integer;
  begin
    for Cnt:=0 to LastObjectType do FItems[Cnt].Free;
    FDefaultInfo.Free;
    inherited Destroy;
  end;

Function TSelectInfo.GetStatusText:String;
var AInfo        : TSelectInfoItem;
    TypeSelected : LongInt;
Procedure DefaultInfo;
  begin
    if FCount=1 then Result:=Format(MlgStringList.SectionStrings[StringSection,24],[FCount])
    else Result:=Format(MlgStringList.SectionStrings[StringSection,1],[FCount])
  end;
begin
  if FCount=0 then Result:=''
  else begin
    TypeSelected:=SelectedType;
    if TypeSelected=-1 then DefaultInfo
    else begin
      AInfo:=GetInfoToType(TypeSelected);
      if AInfo<>FDefaultInfo then begin
         Result:=AInfo.StatusText;
         if FCount = 1 then begin
            Result:=Result; // + ' | ID=' + IntToStr(AInfo.FIndex);
         end;
      end
      else DefaultInfo;
    end;
  end;
  {$IFNDEF AXDLL} // <----------------- AXDLL
  if Result <> sLastSelInfo then
    begin
    DDEHandler.SendString('[GSS][' + Result + ']');  //WAIDACHER 17th July 2001 - Invokes AX-Event OnSelectionStatus
    sLastSelInfo:=Result;
    end;
  {$ENDIF} // <----------------- AXDLL
end;

Procedure TSelectInfo.Add(AItem:PView);
begin
  GetInfoToType(AItem^.GetObjType).Add(AItem);
  Inc(FCount);
  Inc(FHspCount);
end;

Procedure TSelectInfo.AddHsd(AItem:CView);
begin
  GetInfoToType(AItem.GetObjType).AddHsd(AItem);
  Inc(FCount);
  Inc(FHsdCount);
end;

Procedure TSelectInfo.Delete(AItem:PView;ALast:PView;ALastHsd:CView);
begin
  GetInfoToType(AItem^.GetObjType).Delete(AItem);
  Dec(FCount);
  Dec(FHspCount);
  if FCount=1 then begin
    Clear;
    if ALast<>NIL then Add(ALast);
    if ALastHsd<>NIL then AddHsd(ALastHsd);
  end;
end;

Procedure TSelectInfo.DeleteHsd(AItem:CView;ALast:PView;ALastHsd:CView);
begin

  GetInfoToType(AItem.GetObjType).DeleteHsd(AItem);
  Dec(FCount);
  Dec(FHsdCount);
  if FCount=1 then begin
    Clear;
    if ALast<>NIL then Add(ALast);
    if ALastHsd<>NIL then AddHsd(ALastHsd);
  end;

end;

Function TSelectInfo.GetInfoToType(AType:LongInt):TSelectInfoItem;
begin
  Result:=FItems[AType];
  if Result=NIL then Result:=FDefaultInfo;
end;

Procedure TSelectInfo.Clear;
var Cnt          : Integer;
    AInfo        : TSelectInfoItem;
begin
  for Cnt:=0 to LastObjectType do begin
    AInfo:=FItems[Cnt];
    if AInfo<>NIL then AInfo.Clear;
  end;
  FCount:=0;
  FHSDCount:=0;
  FHSPCount:=0;
end;

Function TSelectInfo.GetSelCount(ObjectType:LongInt):LongInt;
var AInfo        : TSelectInfoItem;
begin
  AInfo:=GetInfoToType(ObjectType);
  if AInfo=FDefaultInfo then Result:=0
  else Result:=AInfo.Count;
end;

Function TSelectInfo.GetSelectedType:LongInt;
var AInfo        : TSelectInfoItem;
    Cnt          : Integer;
begin
  Result:=-1;
  if FDefaultInfo.Count=0 then for Cnt:=0 to LastObjectType do begin
    AInfo:=FItems[Cnt];
    if (AInfo<>NIL)
        and (AInfo.Count>0) then begin
      if Result=-1 then Result:=AInfo.ObjectType
      else if AInfo.ObjectType<>Result then begin
        Result:=-1;
        Break;
      end;
    end;
  end;
end;

{==============================================================================+
  TSelectInfoItem
+==============================================================================}

Constructor TSelectInfoItem.Create(AOwner:TSelectInfo;AType:LongInt);
begin
  inherited Create;
  FObjectType:=AType;
  FOwner:=AOwner;
  if Assigned(FOwner) then FOwner.FItems[ObjectType]:=Self;
end;

Destructor TSelectInfoItem.Destroy;
begin
  if Assigned(FOwner) then FOwner.FItems[ObjectType]:=NIL;
  inherited Destroy;

end;

Procedure TSelectInfoItem.Add(AItem:PView);
begin
  Inc(FCount);
  Inc(FHspCount);
  FIndex:=AItem^.Index;
  FGUID:=AItem^.GUID;
end;

Procedure TSelectInfoItem.AddHsd(AItem:CView);
begin
  Inc(FCount);
  Inc(FHsdCount);
  FIndex:=AItem.Index;
end;

Function TSelectInfoItem.GetStatusText:String;
begin
  Result:='';
end;

Procedure TSelectInfoItem.Delete(AItem:PView);
begin
  Dec(FCount);
  Dec(FHspCount);
end;

Procedure TSelectInfoItem.DeleteHsd(AItem:CView);
begin
  Dec(FCount);
  Dec(FHsdCount);
end;

Procedure TSelectInfoItem.Clear;
begin
  FCount:=0;
  FHspCount:=0;
  FHsdCount:=0;
end;

{==============================================================================+
  TPointInfo
+==============================================================================}

Procedure TPointInfo.Add(AItem:PView);
begin
  inherited Add(AItem);
  if Count=1 then with PPixel(AItem)^ do begin
    FXPosition:=Position.X/100.0;
    FYPosition:=Position.Y/100.0;
  end;
end;

Procedure TPointInfo.AddHsd(AItem:CView);
begin
  inherited AddHsd(AItem);
  if Count=1 then with CPixel(AItem) do begin
    FXPosition:=Position.X/100.0;
    FYPosition:=Position.Y/100.0;
  end;
end;

Function TPointInfo.GetStatusText
   : String;
  begin
    if Count=1 then begin
       if WingisMainForm.ActualChild.Data^.PInfo^.CoordinateSystem.CoordinateType = ctCarthesian then
          Result:=MlgStringList.SectionStrings[StringSection,2]
       else
          Result:=MlgStringList.SectionStrings[StringSection,29];
    end
    else Result:=MlgStringList.SectionStrings[StringSection,3];
    Result:=Format(Result,[Count,FormatFloat('#,##0.00',FXPosition),FormatFloat('#,##0.00',FYPosition)]);
  end;

{==============================================================================+
  TAreaInfo
+==============================================================================}

Procedure TAreaInfo.Add(AItem:PView);
begin
  inherited Add(AItem);
  if Count=1 then begin
    FArea:=PCPoly(AItem)^.Flaeche;
    FLength:=PCPoly(AItem)^.Laenge;
    FPoints:=PCPoly(AItem)^.Data^.Count;   {!?!?!? Inselflächen berücksichtigen }
  end
  else begin
    FArea:=FArea+PCPoly(AItem)^.Flaeche;
    FLength:=FLength+PCPoly(AItem)^.Laenge;
    FPoints:=FPoints+PCPoly(AItem)^.Data^.Count;
  end;
end;

Procedure TAreaInfo.AddHsd(AItem:CView);
begin
  inherited AddHsd(AItem);
  if Count=1 then begin
    FArea:=CCPoly(AItem).Area;
    FLength:=CCPoly(AItem).Length;
    FPoints:=CCPoly(AItem).Data.Count;   {!?!?!? Inselflächen berücksichtigen }
  end
  else begin
    FArea:=FArea+CCPoly(AItem).Area;
    FLength:=FLength+CCPoly(AItem).Length;
    FPoints:=FPoints+CCPoly(AItem).Data.Count;
  end;
end;

Procedure TAreaInfo.Delete(AItem:PView);
begin
  inherited Delete(AItem);
  FArea:=FArea-PCPoly(AItem)^.Flaeche;
  FLength:=FLength-PCPoly(AItem)^.Laenge;
  FPoints:=FPoints-PCPoly(AItem)^.Data^.Count;
end;


Procedure TAreaInfo.DeleteHsd(AItem:CView);
begin
  inherited DeleteHsd(AItem);
  FArea:=FArea-CCPoly(AItem).Area;
  FLength:=FLength-CCPoly(AItem).Length;
  FPoints:=FPoints-CCPoly(AItem).Data.Count;
end;


Function TAreaInfo.GetStatusText:String;
begin
  if Count=1 then Result:=MlgStringList.SectionStrings[StringSection,4]
  else Result:=MlgStringList.SectionStrings[StringSection,5];
  if IniFile^.AreaFact > 1 then begin
     Result:=Format(Result,[Count,FormatStr(FArea*10000.0,4)+IniFile^.GetAreaUnit]);
  end
  else begin
     Result:=Format(Result,[Count,FormatStr(FArea*100.0,2)+IniFile^.GetAreaUnit]);
  end;
end;

{==============================================================================+
  TLineInfo                                                                    
+==============================================================================}

Procedure TLineInfo.Add(AItem:PView);
begin
  inherited Add(AItem);
  with PPoly(AItem)^ do if Count=1 then begin
    FLength:=Laenge;
    FPoints:=Data^.Count;
  end
  else begin
    FLength:=FLength+Laenge;
    FPoints:=FPoints+Data^.Count;
  end;
end;

Procedure TLineInfo.Delete(AItem:PView);
begin
  inherited Delete(AItem);
  with PPoly(AItem)^ do begin
    FLength:=FLength-Laenge;
    FPoints:=FPoints-Data^.Count;
  end;
end;

Procedure TLineInfo.AddHsd(AItem:CView);
begin
  inherited AddHsd(AItem);
  if Count=1 then begin
    FLength:=CPoly(AItem).Length;
    FPoints:=CPoly(AItem).Data.Count;
  end
  else begin
    FLength:=FLength+CPoly(AItem).Length;
    FPoints:=FPoints+CPoly(AItem).Data.Count;
  end;
end;

Procedure TLineInfo.DeleteHsd(AItem:CView);
begin
  inherited DeleteHsd(AItem);
  FLength:=FLength-CPoly(AItem).Length;
  FPoints:=FPoints-CPoly(AItem).Data.Count;
end;


Function TLineInfo.GetStatusText:String;
begin
  if Count=1 then Result:=MlgStringList.SectionStrings[StringSection,6]
  else Result:=MlgStringList.SectionStrings[StringSection,7];
  Result:=Format(Result,[Count,FormatStr(FLength*100,2)]);
end;

{==============================================================================+
  TEllipseInfo
+==============================================================================}

Procedure TEllipseInfo.Add(AItem:PView);
begin
  inherited Add(AItem);
  with PEllipse(AItem)^ do if Count=1 then begin
    FXPosition:=Position.X;
    FYPosition:=Position.Y;
    FPrimaryAxis:=PrimaryAxis;
    FArea:=Flaeche;
    FPerimeter:=Laenge;
  end
  else begin
    FArea:=FArea+Flaeche;
    FPerimeter:=FPerimeter+Laenge;
  end;
end;

Procedure TEllipseInfo.Delete(AItem:PView);
begin
  inherited Delete(AItem);
  with PEllipse(AItem)^ do begin
    FArea:=FArea-Flaeche;
    FPerimeter:=FPerimeter-Laenge;
  end;
end;

Procedure TEllipseInfo.AddHsd(AItem:CView);
begin
  inherited AddHsd(AItem);
  with CEllipse(AItem) do if Count=1 then begin
    FXPosition:=Position.X;
    FYPosition:=Position.Y;
    FPrimaryAxis:=PrimaryAxis;
    FArea:=Flaeche;
    FPerimeter:=Laenge;
  end
  else begin
    FArea:=FArea+Flaeche;
    FPerimeter:=FPerimeter+Laenge;
  end;
end;

Procedure TEllipseInfo.DeleteHsd(AItem:CView);
begin
  inherited DeleteHsd(AItem);
  with CEllipse(AItem) do begin
    FArea:=FArea-Flaeche;
    FPerimeter:=FPerimeter-Laenge;
  end;
end;


Function TEllipseInfo.GetStatusText:String;
begin
  if Count=1 then begin
     if WingisMainForm.ActualChild.Data^.PInfo^.CoordinateSystem.CoordinateType = ctCarthesian then
        Result:=MlgStringList.SectionStrings[StringSection,8]
     else
        Result:=MlgStringList.SectionStrings[StringSection,30];
  end
  else Result:=MlgStringList.SectionStrings[StringSection,9];
  Result:=Format(Result,[Count,FormatStr(FXPosition,2),
    FormatStr(FYPosition,2),FormatStr(FPrimaryAxis,2)]);
end;

{==============================================================================+
  TEllipseArcInfo
+==============================================================================}

Procedure TEllipseArcInfo.Add(AItem:PView);
begin
  inherited Add(AItem);
  with PEllipseArc(AItem)^ do if Count=1 then begin
    FXPosition:=Position.X;
    FYPosition:=Position.Y;
    FPrimaryAxis:=PrimaryAxis;
    FBeginAngle:=BeginAngle;
    FEndAngle:=EndAngle;
    FLength:=Laenge;
  end
  else FLength:=FLength+Laenge;
end;

Procedure TEllipseArcInfo.Delete(AItem:PView);
begin
  inherited Delete(AItem);
  with PEllipseArc(AItem)^ do FLength:=FLength-Laenge;
end;

Procedure TEllipseArcInfo.AddHsd(AItem:CView);
begin
  inherited AddHsd(AItem);
  with CArc(AItem) do if Count=1 then begin
    FXPosition:=Position.X;
    FYPosition:=Position.Y;
    FPrimaryAxis:=PrimaryAxis;
    FBeginAngle:=BeginAngle;
    FEndAngle:=EndAngle;
    FLength:=Laenge;
  end
  else FLength:=FLength+Laenge;
end;

Procedure TEllipseArcInfo.DeleteHsd(AItem:CView);
begin
  inherited DeleteHsd(AItem);
  with CArc(AItem) do FLength:=FLength-Laenge;
end;


Function TEllipseArcInfo.GetStatusText:String;
begin
  Result:=FormatStr(FBeginAngle*1800.0/Pi,1);
  if Count=1 then begin
     if WingisMainForm.ActualChild.Data^.PInfo^.CoordinateSystem.CoordinateType = ctCarthesian then
        Result:=MlgStringList.SectionStrings[StringSection,10]
     else
        Result:=MlgStringList.SectionStrings[StringSection,31];
  end
  else Result:=MlgStringList.SectionStrings[StringSection,11];
  Result:=Format(Result,[Count,FormatStr(FXPosition,2),
    FormatStr(FYPosition,2),FormatStr(FPrimaryAxis,2),
    FormatStr(FBeginAngle*1800.0/Pi,1),FormatStr(FEndAngle*1800.0/Pi,1)]);
end;

{==============================================================================+
  TImageInfo
+==============================================================================}

Procedure TImageInfo.Add(AItem:PView);
begin
  inherited Add(AItem);
  if Count=1 then with PImage(AItem)^ do begin
    if Loaded and st_NoLoad=0 then begin
      FWidth:=Info.Width;
      FHeight:=Info.Height;
    end;
    FFileName:=PToStr(FileName);
  end;
end;

Function TImageInfo.GetStatusText:String;
begin
  if Count=1 then Result:=MlgStringList.SectionStrings[StringSection,12]
  else Result:=MlgStringList.SectionStrings[StringSection,13];
  Result:=Format(Result,[Count,FFileName,IntToStr(FWidth),IntToStr(FHeight)]);
end;

{==============================================================================+
  TSplineInfo
+==============================================================================}

Procedure TSplineInfo.Add(AItem:PView);
begin
  inherited Add(AItem);
  if Count=1 then with PSpline(AItem)^ do begin
  end;
end;

Procedure TSplineInfo.AddHsd(AItem:CView);
begin
  inherited AddHsd(AItem);
end;

Function TSplineInfo.GetStatusText:String;
begin
  if Count=1 then Result:=MlgStringList.SectionStrings[StringSection,14]
  else Result:=MlgStringList.SectionStrings[StringSection,15];
  Result:=Format(Result,[Count]);
end;

{==============================================================================+
  TSymbolInfo

 Changed and Added by Brovak
+==============================================================================}

Procedure TSymbolInfo.Add(AItem:PView);
begin
  inherited Add(AItem);
  if Count=1 then with PSymbol(AItem)^ do begin
    FXPosition:= Position.X/100.0;
    FYPosition:= Position.Y/100.0;
    if (SymPtr <> nil) and (SymPtr^.Name <> nil) then begin
       FName:=SymPtr^.Name^;
    end;
  end;
end;

Procedure TSymbolInfo.AddHsd(AItem:CView);
begin
  inherited AddHsd(AItem);
  if Count=1 then with CSymbol(AItem) do begin
    FXPosition:= Position.X/100.0;
    FYPosition:= Position.Y/100.0;
    if (SymPtr <> nil) and (SymPtr^.Name <> nil) then begin
       StrCopy(PChar(AnsiString(FName)),PChar(AnsiString(SymPtr^.Name^)));
    end;
  end;
end;

Function TSymbolInfo.GetStatusText:String;
begin
     if Count=1 then begin
        if WingisMainForm.ActualChild.Data <> nil then begin
           if WingisMainForm.ActualChild.Data^.PInfo^.CoordinateSystem.CoordinateType = ctCarthesian then begin
              Result:=MlgStringList.SectionStrings[StringSection,16];
              if GPSConnection <> nil then begin
                 GPSConnection.CheckComPorts;
              end;
           end
           else begin
              Result:=MlgStringList.SectionStrings[StringSection,32];
              if GPSConnection <> nil then begin
                 GPSConnection.CheckComPorts;
              end;
           end;
        end;
     end
     else begin
        Result:=MlgStringList.SectionStrings[StringSection,17];
     end;
     Result:=Format(Result,[Count,FName,FormatFloat('#,##0.00',FXPosition),FormatFloat('#,##0.00',FYPosition)]);
end;

{==============================================================================+
  TMeasurementInfo
+==============================================================================}

Procedure TMeasurementInfo.Add(AItem:PView);
begin
  inherited Add(AItem);
  if Count=1 then with PMeasureLine(AItem)^ do begin
  end;
end;

Function TMeasurementInfo.GetStatusText:String;
begin
  if Count=1 then Result:=MlgStringList.SectionStrings[StringSection,18]
  else Result:=MlgStringList.SectionStrings[StringSection,19];
  Result:=Format(Result,[Count]);
end;

{==============================================================================+
  TDiagramInfo
+==============================================================================}

Procedure TDiagramInfo.Add(AItem:PView);
begin
  inherited Add(AItem);
  if Count=1 then begin
  end;
end;

Function TDiagramInfo.GetStatusText:String;
begin
  if Count=1 then Result:=MlgStringList.SectionStrings[StringSection,20]
  else Result:=MlgStringList.SectionStrings[StringSection,21];
  Result:=Format(Result,[Count]);
end;

{==============================================================================+
  TTextInfo
+==============================================================================}

Procedure TTextInfo.Add(AItem:PView);
begin
  inherited Add(AItem);
  if Count=1 then with PText(AItem)^ do begin
    FText:=PToStr(Text);
    FXPosition:=Pos.X/100.0;
    FYPosition:=Pos.Y/100.0;
    FAngle:=Angle*1800/Pi;
//      FFontName:=
    FFontHeight:=Font.Height/100.0;
    FIsItAnnot:=IsItAnnot;
  end;
end;

Procedure TTextInfo.AddHsd(AItem:CView);
begin
  inherited AddHsd(AItem);
  if Count=1 then with CText(AItem) do begin
    FText:=GetText;
    FXPosition:=Position.X/100.0;
    FYPosition:=Position.Y/100.0;
    FAngle:=Angle*1800/Pi;
    FFontHeight:=Font.Height/100.0;
    FIsItAnnot:=IsItAnnot;
  end;
end;

Function TTextInfo.GetStatusText:String;
begin
  if Count=1 then
  begin;
   if IsItAnnot = false
    then //Text
     Result:=MlgStringList.SectionStrings[StringSection,22]
    else //Annot
     Result:=MlgStringList.SectionStrings[StringSection,27]
  end
  else Result:=MlgStringList.SectionStrings[StringSection,23];
  Result:=Format(Result,[Count]);
end;

{==============================================================================+
  TRTextInfo
+==============================================================================}

Procedure TRTextInfo.Add(AItem:PView);
begin
  inherited Add(AItem);
  if Count=1 then with PRText(AItem)^ do begin
//    FXPosition:=Pos.X/100.0;
//    FYPosition:=Pos.Y/100.0;
//      FFontName:=
    FFontHeight:=Font.Height/100.0;
  end;
end;

Function TRTextInfo.GetStatusText:String;
begin
  if Count=1 then Result:=MlgStringList.SectionStrings[StringSection,27]
  else Result:=MlgStringList.SectionStrings[StringSection,28];
  Result:=Format(Result,[Count]);
end;

{==============================================================================+
  TOLEInfo
+==============================================================================}

Procedure TOLEInfo.Add(AItem:PView);
begin
  inherited Add(AItem);
  if Count=1 then with POLEClientObject(AItem)^ do begin
  end;
end;

Function TOLEInfo.GetStatusText:String;
begin
  if Count=1 then Result:=MlgStringList.SectionStrings[StringSection,25]
  else Result:=MlgStringList.SectionStrings[StringSection,26];
  Result:=Format(Result,[Count]);
end;

{==============================================================================+
  TSelOrderList
+==============================================================================}

Constructor TSelOrderList.Create;
begin
  inherited Create;
  FItems:=TAutoRecHashTable.Create(1024,SizeOf(TSelOrderEntry));
  FCurrent:=-1;
  FFirst:=-1;
  FLast:=-1;
end;

Destructor TSelOrderList.Destroy;
begin
  Clear;
  FItems.Free;
  inherited Destroy;
end;

Procedure TSelOrderList.Append(Item:Integer);
var LastIndex      : Integer;
    Entry          : TSelOrderEntry;
    LastEntry      : PSelOrderEntry;
begin
  // search for the position the item is to be inserted to
  if Find(Item)=NIL then begin
    // set entry-data
    Entry.Index:=Item;
    Entry.Previous:=FLast;
    Entry.Next:=-1;
    FItems.Add(Entry);
    // update next-pointer of the currently last in the list
    LastEntry:=Find(LastIndex);
    if LastEntry<>NIL then LastEntry^.Next:=Item;
    FLast:=Item;
    if FFirst<0 then FFirst:=Item;
    if FCurrent<0 then FCurrent:=Item;
  end;
end;

Procedure TSelOrderList.Clear;
begin
  FItems.Clear;
end;

Procedure TSelOrderList.Delete(Item: Integer);
var Entry          : PSelOrderEntry;
    PrevEntry      : PSelOrderEntry;
    NextEntry      : PSelOrderEntry;
begin
  Entry:=Find(Item);
  if Entry<>NIL then begin
    // remove the item from the double-linked list
    PrevEntry:=Find(Entry^.Previous);
    if PrevEntry<>NIL then PrevEntry^.Next:=Entry^.Next;
    NextEntry:=Find(Entry^.Next);
    if NextEntry<>NIL then NextEntry^.Previous:=Entry^.Previous;
    FItems.Delete(Item);
    // update last-, first- and current item 
    if Item=FLast then FLast:=Entry^.Previous;
    if Item=FFirst then FFirst:=Entry^.Next;
    if Item=FCurrent then FCurrent:=Entry^.Previous;
  end;
end;

Procedure TSelOrderList.Next;
var Entry          : PSelOrderEntry;
begin
  Entry:=Find(FCurrent);
  if Entry<>NIL then FCurrent:=Entry^.Next
  else FCurrent:=FFirst;
  if FCurrent=FLast then FCurrent:=FFirst;
end;

Function TSelOrderList.GetCount: Integer;
begin
  Result:=FItems.Count;
end;

Function TSelOrderList.Find(Item:Integer):PSelOrderEntry;
begin
  if Item>=0 then Result:=FItems.Find(Item)
  else Result:=NIL;
end;

{==============================================================================+
  Diverses
+==============================================================================}

Function CreateSelectInfo:TSelectInfo;
begin
  if StringSection=-1 then StringSection:=MlgStringList.Sections['SelectInfo'];
  Result:=TSelectInfo.Create;
  TPointInfo.Create(Result,ot_Pixel);
  TAreaInfo.Create(Result,ot_CPoly);
  TLineInfo.Create(Result,ot_Poly);
  TEllipseInfo.Create(Result,ot_Circle);
  TEllipseArcInfo.Create(Result,ot_Arc);
  TImageInfo.Create(Result,ot_Image);
  TSplineInfo.Create(Result,ot_Spline);
  TSymbolInfo.Create(Result,ot_Symbol);
  TMeasurementInfo.Create(Result,ot_MesLine);
  TDiagramInfo.Create(Result,ot_BusGraph);
  TTextInfo.Create(Result,ot_Text);
  TRTextInfo.Create(Result,ot_RText);
  TOLEInfo.Create(Result,ot_OLEObj);
end;

{==============================================================================+
  Initialisierung
+==============================================================================}

Initialization
  StringSection:=-1;
  
end.
