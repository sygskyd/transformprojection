Unit OrganizeDlg;

Interface

Uses Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,ExtLib,
     ComCtrls,StdCtrls,WCtrls,DipGrid,ImgList,ActnList,Buttons,MultiLng,XLines,
     XFills,ProjStyle,ExtCtrls,AM_Obj,AM_Paint,SymLib,AM_Font,AM_Group;

Type TOrganizePage      = (opLineStyles,opFillStyles,opSymbols);
     TOrganizePages     = Set of TOrganizePage;

     TSide              = (siLeft,siRight);            

     TLibraryType       = (ltNone,ltNew,ltFile,ltProject);

     TOrganizeDialog    = Class;
     
     TLibrary           = Class
      Private
       // filename of the library or "Project Library" if project-library is opened
       FFileName        : String;
       FList            : TDipList;
       FLabel           : TWLabel;
       FModified        : Boolean;
       FParent          : TOrganizeDialog;
       FType            : TLibraryType;
      Protected
       Procedure   DrawCell(Sender:TObject;Canvas:TCanvas;Index:Integer;Rect:TRect); virtual; abstract;
       Function    GetCount:Integer; virtual; abstract;
       Function    GetName(AIndex:Integer):String; virtual; abstract;
       Function    GetNamedItem(const AName:String):Pointer; virtual; abstract;
       Function    GetItem(AIndex:Integer):Pointer; virtual; abstract;
       Function    GetReferenceCount(AItem:Pointer):Integer; virtual; abstract;
      Public
       Constructor Create(AParent:TOrganizeDialog;AList:TDipList;ALabel:TWLabel);
       Procedure   AddCopy(AItem:Pointer); virtual; abstract;
       Procedure   AddCopyNamed(AItem:Pointer;const NewName:String); virtual; abstract;
       Function    CloseQuery:Boolean;
       Function    Close:Boolean;
       Property    Caption:TWLabel read FLabel;
       Procedure   Delete(const AName:String); virtual; abstract;
       Property    Count:Integer read GetCount;
       Property    FileName:String read FFileName;
       Property    Items[AIndex:Integer]:Pointer read GetItem; default;
       Property    LibraryType:TLibraryType read FType;
       Property    List:TDipList read FList;
       Property    Modified:Boolean read FModified write FModified;
       Property    Names[AIndex:Integer]:String read GetName;
       Property    NamedItems[const Name:String]:Pointer read GetNamedItem;
       Procedure   New; virtual;
       Procedure   Open(const AFileName:String); virtual;
       Procedure   OpenProjectLibrary(Param:Pointer;Param1:Pointer=NIL); virtual;
       Property    Parent:TOrganizeDialog read FParent write FParent;
       Procedure   Replace(AItem:Pointer); virtual; abstract;
       Property    ReferenceCount[AItem:Pointer]:Integer read GetReferenceCount;
       Function    Save:Boolean; virtual;
       Function    SaveAs:Boolean; virtual;
     end;

     TLineLibrary       = Class(TLibrary)
      Private
       FLineList        : TStringList;
       FLineStyles      : TXLineStyleList;
       FOrgLineStyles   : TXLineStyleList;
      Protected
       Procedure   DrawCell(Sender:TObject;Canvas:TCanvas;Index:Integer;Rect:TRect); override;
       Function    GetCount:Integer; override;
       Function    GetItem(AIndex:Integer):Pointer; override;
       Function    GetName(AIndex:Integer):String; override;
       Function    GetNamedItem(const AName:String):Pointer; override;
       Function    GetReferenceCount(AItem:Pointer):Integer; override;
       Procedure   UpdateStylesList;
      Public
       Constructor Create(AParent:TOrganizeDialog;AList:TDipList;ALabel:TWLabel);
       Destructor  Destroy; override;
       Procedure   AddCopy(AItem:Pointer); override;
       Procedure   AddCopyNamed(AItem:Pointer;const NewName:String); override;
       Procedure   Delete(const AName:String); override;
       Procedure   New; override;
       Procedure   Open(const AFileName:String); override;
       Procedure   OpenProjectLibrary(Param:Pointer;Param1:Pointer=NIL); override;
       Procedure   Replace(AItem:Pointer); override;
       Function    Save:Boolean; override;
       Function    SaveAs:Boolean; override;
       Property    Styles:TXLineStyleList read FLineStyles write FLineStyles;
     end;

     TFillLibrary       = Class(TLibrary)
      Private
       FFillList        : TStringList;
       FFillStyles      : TXFillStyleList;
       FOrgFillStyles   : TXFillStyleList;
      Protected
       Procedure   DrawCell(Sender:TObject;Canvas:TCanvas;Index:Integer;Rect:TRect); override;
       Function    GetCount:Integer; override;
       Function    GetItem(AIndex:Integer):Pointer; override;
       Function    GetName(AIndex:Integer):String; override;
       Function    GetNamedItem(const AName:String):Pointer; override;
       Function    GetReferenceCount(AItem:Pointer):Integer; override;
       Procedure   UpdateStylesList;
      Public
       Constructor Create(AParent:TOrganizeDialog;AList:TDipList;ALabel:TWLabel);
       Destructor  Destroy; override;
       Procedure   AddCopy(AItem:Pointer); override;
       Procedure   AddCopyNamed(AItem:Pointer;const NewName:String); override;
       Procedure   Delete(const AName:String); override;
       Procedure   New; override;
       Procedure   Open(const AFileName:String); override;
       Procedure   OpenProjectLibrary(Param:Pointer;Param1:Pointer=NIL); override;
       Procedure   Replace(AItem:Pointer); override;
       Function    Save:Boolean; override;
       Function    SaveAs:Boolean; override;
       Property    Styles:TXFillStyleList read FFillStyles write FFillStyles;
     end;

     TSymbolLibrary     = Class(TLibrary)
      Private
       FOrgSymbols : PSymbols;
       FExtLib     : PExtLib;
       FOrgFonts   : PFonts;
       FFonts      : PFonts;
       FPInfo      : PPaint;
       FSymbols    : PSymbols;
       FSymInfo    : TSymInfo;
       Procedure   FreeCurrent;
       Function    SymbolFromName(const Name:String):PSGroup;
      Protected
       Procedure   DrawCell(Sender:TObject;Canvas:TCanvas;Index:Integer;Rect:TRect); override;
       Function    GetCount:Integer; override;
       Function    GetItem(AIndex:Integer):Pointer; override;
       Function    GetName(AIndex:Integer):String; override;
       Function    GetNamedItem(const AName:String):Pointer; override;
       Function    GetReferenceCount(AItem:Pointer):Integer; override;
      Public
       Constructor Create(AParent:TOrganizeDialog;AList:TDipList;ALabel:TWLabel);
       Destructor  Destroy; override;
       Procedure   AddCopy(AItem:Pointer); override;
       Procedure   AddCopyNamed(AItem:Pointer;const NewName:String); override;
       Procedure   Delete(const AName:String); override;
       Procedure   New; override;
       Procedure   Open(const AFileName:String); override;
       Procedure   OpenProjectLibrary(Param:Pointer;Param1:Pointer=NIL); override;
       Procedure   Replace(AItem:Pointer); override;
       Function    Save:Boolean; override;
       Function    SaveAs:Boolean; override;
       Property    Symbols:PSymbols read FSymbols write FSymbols;
       Property    SymbolInfo:TSymInfo read FSymInfo;
     end;

     TOrganizeDialog    = Class(TWForm)
       ActionList       : TActionList;
       ButtonPanel      : TPanel;
       CopyBtn          : TBitBtn;
       CopyAction       : TAction;
       CopyAllAction    : TAction;
       CopyAllBtn       : TBitBtn;
       DeleteAction     : TAction;
       DeleteBtn        : TBitBtn;
       FillLeftLabel    : TWLabel;
       FillRightLabel   : TWLabel;
       ImageList        : TImageList;
       LeftButtonPanel  : TPanel;
       LeftFillList     : TDipList;
       LeftNewAction    : TAction;
       LeftNewBtn       : TSpeedButton;
       LeftOpenBtn      : TSpeedButton;
       LeftOpenAction   : TAction;
       LineRightLabel   : TWLabel;
       LeftProjectBtn   : TSpeedButton;
       LeftSaveAsBtn    : TSpeedButton;
       LeftSaveAction   : TAction;
       LeftSaveBtn      : TSpeedButton;
       LeftProjectAction: TAction;
       LeftLineList     : TDipList;
       LineLeftLabel    : TWLabel;
       LeftSaveAsAction : TAction;
       LeftSymList      : TDipList;
       LeftSymbolMosaic : TAction;
       LeftSymbolList   : TAction;
       MlgSection       : TMlgSection;
       MoveAction       : TAction;
       MoveAllAction    : TAction;
       MoveAllBtn       : TBitBtn;
       MoveBtn          : TBitBtn;
       Notebook         : TPageControl;
       OKBtn            : TButton;
       OpenStylesDialog : TOpenDialog;
       OpenSymbolsDialog: TWOpenDialog;
       ReplaceAction    : TAction;
       ReplaceBtn       : TBitBtn;
       RightProjectAction: TAction;
       RightLineList    : TDipList;
       RightNewAction   : TAction;
       RightOpenAction  : TAction;
       RightSaveAction  : TAction;
       RightSaveAsAction: TAction;
       RightSymList     : TDipList;
       RightFillList    : TDipList;
       RightBtnPanel    : TPanel;
       RigthNewBtn      : TSpeedButton;
       RightOpenBtn     : TSpeedButton;
       RightProjectBtn  : TSpeedButton;
       RightSaveBtn     : TSpeedButton;
       RightSaveAsBtn   : TSpeedButton;
       RightSymbolMosaic: TAction;
       RightSymbolList  : TAction;
       SaveStylesDialog : TSaveDialog;
       SaveSymbolsDialog: TWSaveDialog;
       SymbolSheet      : TTabSheet;
       SymLeftMosaicBtn : TSpeedButton;
       SymLeftListBtn   : TSpeedButton;
       SymRightMosaicBtn: TSpeedButton;
       SymRightListBtn  : TSpeedButton;
       SymLeftLabel     : TWLabel;
       SymRightLabel    : TWLabel;
       XLineSheet       : TTabSheet;
       XFillSheet       : TTabSheet;
    CBUsed: TCheckBox;
       Procedure   CopyActionExecute(Sender: TObject);
       Procedure   CopyAllActionExecute(Sender: TObject);
       Procedure   DeleteActionExecute(Sender: TObject);
       Procedure   FormCloseQuery(Sender: TObject; var CanClose: Boolean);
       Procedure   FormCreate(Sender: TObject);
       Procedure   FormDestroy(Sender: TObject);
       Procedure   FormHide(Sender: TObject);
       Procedure   FormShow(Sender: TObject);
       Procedure   ListEnter(Sender: TObject);
       Procedure   LeftNewActionExecute(Sender: TObject);
       Procedure   LeftOpenActionExecute(Sender: TObject);
       Procedure   LeftProjectActionExecute(Sender: TObject);
       Procedure   LeftSaveActionExecute(Sender: TObject);
       Procedure   LeftSaveAsActionExecute(Sender: TObject);
       Procedure   LeftSymListMouseMove(Sender:TObject;Shift:TShiftState;X,Y:Integer);
       Procedure   LeftSymbolListExecute(Sender: TObject);
       Procedure   LeftSymbolMosaicExecute(Sender: TObject);
       Procedure   MoveActionExecute(Sender: TObject);
       Procedure   MoveAllActionExecute(Sender: TObject);
       Procedure   NotebookChange(Sender: TObject);
       Procedure   RightNewActionExecute(Sender: TObject);
       Procedure   RightOpenActionExecute(Sender: TObject);
       Procedure   RightProjectActionExecute(Sender: TObject);
       Procedure   RightSaveAsActionExecute(Sender: TObject);
       Procedure   RightSaveActionExecute(Sender: TObject);
       Procedure   RightSymListMouseMove(Sender:TObject;Shift:TShiftState;X,Y:Integer);
       Procedure   RightSymbolListExecute(Sender: TObject);
       Procedure   RightSymbolMosaicExecute(Sender: TObject);
       Procedure   UpdateControls(Sender:TObject);
    procedure CBUsedClick(Sender: TObject);
      Private
       FLeftLastIndex   : Integer;
       FRightLastIndex  : Integer;
       // pages currently shitched on
       FPages           : TOrganizePages;
       FProjModified    : Boolean;
       FStartupPage     : TOrganizePage;
       // styles of the project, if called for project
       FOrgLineStyles   : TXLineStyleList;
       FLineStyles      : TXLineStyleList;
       FOrgFillStyles   : TXFillStyleList;
       FFillStyles      : TXFillStyleList;
       // symbol pointers
       FOrgSymbols      : PSymbols;
       // fonts pointers
       FOrgFonts        : PFonts;
       // currently opened libraries
       FLibraries       : Array[TSide,TOrganizePage] of TLibrary;
       // currently active libraries (dependend on active page)
       FLeftLibrary     : TLibrary;
       FRightLibrary    : TLibrary;
       // last library-list focused, is source for copy and move
       FLastLibrary     : TLibrary;
       // true if organize-dialog is called from project
       FFromProject     : Boolean;
       Function    CheckDuplicateNames(ItemList:TStringList;Dest:TLibrary):Integer;
       Function    CheckReferenceCount(ItemList:TStringList;Source:TLibrary):Boolean;
       Function    CopyEntries(ItemList:TStringList;Source,Dest:TLibrary):Boolean;
       Procedure   MoveEntries(ItemList:TStringList;Source,Dest:TLibrary);
       Function    GetCurrentPage:TOrganizePage;
       Function    GetFocusedLibrary(var Page:TOrganizePage;var Side:TSide):Boolean;
       Procedure   GetLibraryPosition(ALibrary:TLibrary;var Page:TOrganizePage;var Side:TSide);
       Procedure   SetLeftLibrary(ALibrary:TLibrary);
       Procedure   SetProjectLibrary(Page:TOrganizePage;Side:TSide);
       Procedure   SetRightLibrary(ALibrary:TLibrary);
       Procedure   SetSymMosaic(ASide:TSide;AMosaic:Boolean);
       Procedure   SetLastLibrary(APage:TOrganizePage;ASide:TSide);
       Procedure   UpdateLibraryControls(ALibrary:TLibrary);
      Public
       Property    Fonts:PFonts read FOrgFonts write FOrgFonts;
       Property    Pages:TOrganizePages read FPages write FPages;
       Property    ProjectModified:Boolean read FProjModified write FProjModified;
       Property    StartupPage:TOrganizePage read FStartupPage write FStartupPage;
       Property    Symbols:PSymbols read FOrgSymbols write FOrgSymbols;
       Property    XFillStyles:TXFillStyleList read FFillStyles write FOrgFillStyles;
       Property    XLineStyles:TXLineStyleList read FLineStyles write FOrgLineStyles;
     end;

var ShowUsedSym: Boolean;

Implementation

{$R *.DFM}

Uses AM_Def,NumTools,OrganizeCopyDlg,UserIntf,XStyles;

{==============================================================================+
  Other
+==============================================================================}

Function OtherSide(Side:TSide):TSide;
begin
  if Side=siLeft then Result:=siRight
  else Result:=siLeft;
end;

{==============================================================================+
  TOrganizeDialog
+==============================================================================}

Procedure TOrganizeDialog.LeftSymbolListExecute(Sender: TObject);
begin
  SetSymMosaic(siLeft,FALSE);
end;

Procedure TOrganizeDialog.LeftSymbolMosaicExecute(Sender: TObject);
begin
  SetSymMosaic(siLeft,TRUE);
end;

Procedure TOrganizeDialog.RightSymbolListExecute(Sender: TObject);
begin
  SetSymMosaic(siRight,FALSE);
end;

Procedure TOrganizeDialog.RightSymbolMosaicExecute(Sender: TObject);
begin
  SetSymMosaic(siRight,TRUE);
end;

Function TOrganizeDialog.GetCurrentPage: TOrganizePage;
begin
  if Notebook.ActivePage=XLineSheet then Result:=opLineStyles
  else if Notebook.ActivePage=XFillSheet then Result:=opFillStyles
  else Result:=opSymbols;
end;

Procedure TOrganizeDialog.SetLeftLibrary(ALibrary: TLibrary);
begin
  FLeftLibrary:=ALibrary;
  UpdateLibraryControls(FLeftLibrary);
  UpdateControls(Self);
end;

Procedure TOrganizeDialog.SetRightLibrary(ALibrary: TLibrary);
begin
  FRightLibrary:=ALibrary;
  UpdateLibraryControls(FRightLibrary);
  UpdateControls(Self);
end;

Procedure TOrganizeDialog.UpdateControls(Sender:TObject);
var AEnabled       : Boolean;
begin
  // enable/disable save-buttons, dependend if file or project-library is opened
  AEnabled:=(FLeftLibrary.Count>0) and (FLeftLibrary.LibraryType<>ltProject);
  LeftSaveAction.Enabled:=AEnabled;
  LeftSaveAsAction.Enabled:=AEnabled;
  AEnabled:=(FRightLibrary.Count>0) and (FRightLibrary.LibraryType<>ltProject);
  RightSaveAction.Enabled:=AEnabled;
  RightSaveAsAction.Enabled:=AEnabled;
  LeftNewAction.Enabled:=FRightLibrary.LibraryType<>ltNone;
  // update enable/disable of project-library-buttons to avoid opening the
  // project-library on both sides, disable them also if no project set
  LeftProjectAction.Enabled:=FFromProject and (FRightLibrary.LibraryType<>ltProject);
  RightProjectAction.Enabled:=FFromProject and (FLeftLibrary.LibraryType<>ltProject);
  // enable/disable copy-, move- and delete-buttons
  AEnabled:=(FLeftLibrary.LibraryType<>ltNone) and (FRightLibrary.LibraryType<>ltNone)
      and (FLastLibrary<>NIL) and (FLastLibrary.Count>0);
  CopyAllAction.Enabled:=AEnabled;
  MoveAllAction.Enabled:=AEnabled;
  AEnabled:=AEnabled and (FLastLibrary<>NIL) and (FLastLibrary.List.ItemIndex>=0);
  CopyAction.Enabled:=AEnabled;
  MoveAction.Enabled:=AEnabled;
  DeleteAction.Enabled:=(FLastLibrary<>NIL) and (FLastLibrary.Count>0);
  CBUsed.Visible:=(FLeftLibrary.LibraryType = ltProject);
end;

Procedure TOrganizeDialog.NotebookChange(Sender: TObject);
var ASide          : TSide;
    APage          : TOrganizePage;
begin
  // update last-library
  if FLastLibrary<>NIL then begin
    GetLibraryPosition(FLastLibrary,APage,ASide);
    SetLastLibrary(GetCurrentPage,ASide);
  end;
  // update pointers to left and right library
  SetLeftLibrary(FLibraries[siLeft,GetCurrentPage]);
  SetRightLibrary(FLibraries[siRight,GetCurrentPage]);
end;

Procedure TOrganizeDialog.LeftOpenActionExecute(Sender: TObject);
var DoOpen         : Boolean;
begin
  if GetCurrentPage=opSymbols then DoOpen:=OpenSymbolsDialog.Execute
  else DoOpen:=OpenStylesDialog.Execute;
  if DoOpen and FLibraries[siLeft,GetCurrentPage].Close then begin
    if GetCurrentPage=opSymbols then FLibraries[siLeft,opSymbols].
        Open(OpenSymbolsDialog.FileName)
    else FLibraries[siLeft,GetCurrentPage].Open(OpenStylesDialog.FileName);
    SetLeftLibrary(FLibraries[siLeft,GetCurrentPage]);
  end;
end;

Procedure TOrganizeDialog.RightOpenActionExecute(Sender: TObject);
var DoOpen         : Boolean;
begin
  if FLibraries[siRight,GetCurrentPage].Close then begin
    DoOpen:=FALSE;
    if GetCurrentPage=opSymbols then begin
      if OpenSymbolsDialog.Execute then begin
        FLibraries[siRight,opSymbols].Open(OpenSymbolsDialog.FileName);
{++ Ivanoff BUG#555 BUILD#100}
{The symbol library and the ExtLibrary may have different fonts.
As libraries use number of fonts, but not font name, the same fonts may have different
numbers. These statements syncronized these fonts.}
        TSymbolLibrary(FLibraries[siRight,opSymbols]).FExtLib^.Fonts := Fonts;
        TSymbolLibrary(FLibraries[siRight,opSymbols]).FOrgFonts := Fonts;
        TSymbolLibrary(FLibraries[siRight,opSymbols]).FFonts.Assign(TSymbolLibrary(FLibraries[siRight,opSymbols]).FOrgFonts);
{-- Ivanoff}
        DoOpen:=TRUE;
      end;
    end
    // open a wingis styles-library
    else if OpenStylesDialog.Execute then begin
      FLibraries[siRight,GetCurrentPage].Open(OpenStylesDialog.FileName);
      DoOpen:=TRUE;
    end;
    if DoOpen then SetRightLibrary(FLibraries[siRight,GetCurrentPage]);
  end;
end;

Procedure TOrganizeDialog.LeftNewActionExecute(Sender: TObject);
begin
  // close the currently opened library - saves changes if any
  if (FLeftLibrary=NIL) or FLeftLibrary.Close then begin
    // initialise the library
    FLibraries[siLeft,GetCurrentPage].New;
    // display the library
    SetLeftLibrary(FLibraries[siLeft,GetCurrentPage]);
  end;
end;

Procedure TOrganizeDialog.RightNewActionExecute(Sender: TObject);
begin
  // close the currently opened library - saves changes if any
  if (FRightLibrary=NIL) or FRightLibrary.Close then begin
    // initialise the library
    FLibraries[siRight,GetCurrentPage].New;
    // display the library
    SetRightLibrary(FLibraries[siRight,GetCurrentPage]);
  end;
end;

Procedure TOrganizeDialog.FormHide(Sender:TObject);
begin
  if ModalResult=mrOK then begin
    if FOrgLineStyles<>NIL then FOrgLineStyles.Assign(FLineStyles);
    if FOrgFillStyles<>NIL then FOrgFillStyles.Assign(FFillStyles);
  end;                                  
end;

Procedure TOrganizeDialog.FormCloseQuery(Sender:TObject;
  var CanClose:Boolean);
begin
  CanClose:=FLeftLibrary.CloseQuery and FRightLibrary.CloseQuery;
end;

Procedure TOrganizeDialog.FormCreate(Sender: TObject);
begin
  FPages:=[opLineStyles,opFillStyles,opSymbols];
  //
  FLineStyles:=TXLineStyleList.Create;
  FFillStyles:=TXFillStyleList.Create;
  // create library-interfaces
  FLibraries[siLeft,opLineStyles]:=TLineLibrary.Create(Self,LeftLineList,LineLeftLabel);
  FLibraries[siLeft,opFillStyles]:=TFillLibrary.Create(Self,LeftFillList,FillLeftLabel);
  FLibraries[siLeft,opSymbols]:=TSymbolLibrary.Create(Self,LeftSymList,SymLeftLabel);
  FLibraries[siRight,opLineStyles]:=TLineLibrary.Create(Self,RightLineList,LineRightLabel);
  FLibraries[siRight,opFillStyles]:=TFillLibrary.Create(Self,RightFillList,FillRightLabel);
  FLibraries[siRight,opSymbols]:=TSymbolLibrary.Create(Self,RightSymList,SymRightLabel);
end;

Procedure TOrganizeDialog.FormDestroy(Sender: TObject);
var Page           : TOrganizePage;
begin
  for Page:=Low(TOrganizePage) to High(TOrganizePage) do begin
    FLibraries[siLeft,Page].Free;
    FLibraries[siRight,Page].Free;
  end;
  FLineStyles.Free;
  FFillStyles.Free;
end;

{******************************************************************************+
  Procedure TOrganizeDialog.UpdateLibraryControls
--------------------------------------------------------------------------------
  Updates the controls that depend on the contents of the library.
+******************************************************************************}
Procedure TOrganizeDialog.UpdateLibraryControls(ALibrary: TLibrary);
begin
  with ALibrary do begin
    List.Count:=Count;
    List.Invalidate;
    case LibraryType of
      ltNone    : Caption.Caption:=MlgSection[19];
      ltNew     : Caption.Caption:=MlgSection[20];
      ltFile    : Caption.Caption:=Format(MlgSection[21],[ExtractFileName(FileName)]);
      ltProject : Caption.Caption:=MlgSection[22];
    end;
  end;
end;

Procedure TOrganizeDialog.SetProjectLibrary(Page:TOrganizePage;Side:TSide);
begin
  if FLibraries[Side,Page].Close then begin
    if Page=opSymbols then FLibraries[Side,Page].OpenProjectLibrary(FOrgSymbols,FOrgFonts)
    else if Page=opLineStyles then FLibraries[Side,Page].OpenProjectLibrary(FLineStyles)
    else FLibraries[Side,Page].OpenProjectLibrary(FFillStyles);
    if Side=siLeft then SetLeftLibrary(FLibraries[Side,Page])
    else SetRightLibrary(FLibraries[Side,Page])
  end;
end;                                    

Procedure TOrganizeDialog.LeftProjectActionExecute(Sender: TObject);
begin
  SetProjectLibrary(GetCurrentPage,siLeft);
end;

Procedure TOrganizeDialog.RightProjectActionExecute(Sender: TObject);
begin
  SetProjectLibrary(GetCurrentPage,siRight);
end;

Procedure TOrganizeDialog.FormShow(Sender: TObject);
begin
  ShowUsedSym:=False;
  // show or hide pages selected by the caller
  if not (opLineStyles in FPages) then XLineSheet.Hide;
  if not (opFillStyles in FPages) then XFillSheet.Hide;
  if not (opSymbols in FPages) then SymbolSheet.Hide;
  FLeftLibrary:=FLibraries[siLeft,FStartupPage];
  FRightLibrary:=FLibraries[siRight,FStartupPage];
  // copy the original fill- and line-styles
  if FOrgLineStyles<>NIL then begin
    FLineStyles.Assign(FOrgLineStyles);
    SetProjectLibrary(opLineStyles,siLeft);
    FFromProject:=TRUE;
  end;
  if FOrgFillStyles<>NIL then begin
    FFillStyles.Assign(FOrgFillStyles);
    SetProjectLibrary(opFillStyles,siLeft);
    FFromProject:=TRUE;
  end;
  // create a copy of the original symbols
  if FOrgSymbols<>NIL then begin
    SetProjectLibrary(opSymbols,siLeft);
    FFromProject:=TRUE;
  end;
  // activate the startup-page
  FLeftLibrary:=FLibraries[siLeft,FStartupPage];
  FRightLibrary:=FLibraries[siRight,FStartupPage];
  case FStartupPage of
    opLineStyles : Notebook.ActivePage:=XLineSheet;
    opFillStyles : Notebook.ActivePage:=XFillSheet;
    opSymbols    : Notebook.ActivePage:=SymbolSheet;
  end;
  UpdateControls(Self);
end;

Procedure TOrganizeDialog.LeftSaveAsActionExecute(Sender: TObject);
begin
  FLeftLibrary.SaveAs;
end;

Procedure TOrganizeDialog.RightSaveAsActionExecute(Sender: TObject);
begin
  FRightLibrary.SaveAs;
end;

Procedure TOrganizeDialog.LeftSaveActionExecute(Sender: TObject);
begin
  if FLeftLibrary.LibraryType=ltNew then FLeftLibrary.SaveAs
  else FLeftLibrary.Save;
end;

Procedure TOrganizeDialog.RightSaveActionExecute(Sender:TObject);
begin
  if FRightLibrary.LibraryType=ltNew then FRightLibrary.SaveAs
  else FRightLibrary.Save;
end;

Function TOrganizeDialog.GetFocusedLibrary(var Page:TOrganizePage;var Side:TSide):Boolean;
var ASide          : TSide;
    APage          : TOrganizePage;
begin
  for ASide:=Low(TSide) to High(TSide) do for APage:=Low(TOrganizePage) to
      High(TOrganizePage) do if FLibraries[ASide,APage].List.Focused then begin
    Side:=ASide;
    Page:=APage;
    Result:=TRUE;
    Exit;
  end;
  Result:=FALSE;
end;

Procedure TOrganizeDialog.GetLibraryPosition(ALibrary:TLibrary;var Page:TOrganizePage;var Side:TSide);
var ASide          : TSide;
    APage          : TOrganizePage;
begin
  for ASide:=Low(TSide) to High(TSide) do for APage:=Low(TOrganizePage) to
      High(TOrganizePage) do if FLibraries[ASide,APage]=ALibrary then begin
    Side:=ASide;
    Page:=APage;
    Exit;
  end;
end;

Procedure TOrganizeDialog.CopyActionExecute(Sender: TObject);
var ASide          : TSide;
    APage          : TOrganizePage;
    Items          : TStringList;
begin
  if FLastLibrary<>NIL then begin
    Items:=TStringList.Create;
    try
      GetLibraryPosition(FLastLibrary,APage,ASide);
      Items.AddObject(FLastLibrary.Names[FLastLibrary.List.ItemIndex],
          FLastLibrary[FLastLibrary.List.ItemIndex]);
      CopyEntries(Items,FLastLibrary,FLibraries[OtherSide(ASide),APage]);
    finally
      Items.Free;
    end;
  end;
end;

Procedure TOrganizeDialog.CopyAllActionExecute(Sender: TObject);
var ASide          : TSide;
    APage          : TOrganizePage;
    Items          : TStringList;
    Cnt            : Integer;
begin
  if FLastLibrary<>NIL then begin
    Items:=TStringList.Create;
    try
      for Cnt:=0 to FLastLibrary.Count-1 do Items.AddObject(FLastLibrary.Names[Cnt],
          FLastLibrary[Cnt]);
      GetLibraryPosition(FLastLibrary,APage,ASide);
      CopyEntries(Items,FLastLibrary,FLibraries[OtherSide(ASide),APage]);
    finally
      Items.Free;
    end;
  end;
end;

Procedure TOrganizeDialog.ListEnter(Sender: TObject);
var ASide          : TSide;
    APage          : TOrganizePage;
begin
  if GetFocusedLibrary(APage,ASide) then SetLastLibrary(APage,ASide)
  else FLastLibrary:=NIL;
  UpdateControls(Self);
end;

Procedure TOrganizeDialog.MoveActionExecute(Sender: TObject);
var ASide          : TSide;
    APage          : TOrganizePage;
    Items          : TStringList;
begin
  if FLastLibrary<>NIL then begin
    Items:=TStringList.Create;
    try
      GetLibraryPosition(FLastLibrary,APage,ASide);
      Items.AddObject(FLastLibrary.Names[FLastLibrary.List.ItemIndex],
          FLastLibrary[FLastLibrary.List.ItemIndex]);
      MoveEntries(Items,FLastLibrary,FLibraries[OtherSide(ASide),APage]);
    finally
      Items.Free;                          
    end;
  end;
end;

Procedure TOrganizeDialog.MoveAllActionExecute(Sender: TObject);
var ASide          : TSide;
    APage          : TOrganizePage;
    Items          : TStringList;
    Cnt            : Integer;
begin
  if FLastLibrary<>NIL then begin
    Items:=TStringList.Create;
    try
      for Cnt:=0 to FLastLibrary.Count-1 do Items.AddObject(FLastLibrary.Names[Cnt],
          FLastLibrary[Cnt]);
      GetLibraryPosition(FLastLibrary,APage,ASide);
      MoveEntries(Items,FLastLibrary,FLibraries[OtherSide(ASide),APage]);
    finally
      Items.Free;
    end;                                                   
  end;
end;

{******************************************************************************+
  Procedure TOrganizeDialog.SetLastLibrary
--------------------------------------------------------------------------------
  Sets the last library the user clicked on. This library will be the
  source for copy and move Functions. Sets the appropriate bitmaps to
  the copy and move buttons.
+******************************************************************************}
Procedure TOrganizeDialog.SetLastLibrary(APage:TOrganizePage;ASide:TSide);
var Layout         : TButtonLayout;
    Bitmap         : TBitmap;
    Bitmap1        : TBitmap;
begin
  FLastLibrary:=FLibraries[ASide,APage];
  Bitmap:=TBitmap.Create;
  Bitmap1:=TBitmap.Create;
  if ASide=siLeft then begin
    ImageList.GetBitmap(8,Bitmap);
    ImageList.GetBitmap(12,Bitmap1);
    Layout:=blGlyphRight;
  end
  else begin
    ImageList.GetBitmap(7,Bitmap);
    ImageList.GetBitmap(11,Bitmap1);
    Layout:=blGlyphLeft;
  end;
  CopyBtn.Glyph:=Bitmap1;
  CopyBtn.Layout:=Layout;
  CopyAllBtn.Glyph:=Bitmap;
  CopyAllBtn.Layout:=Layout;
  MoveBtn.Glyph:=Bitmap1;
  MoveBtn.Layout:=Layout;
  MoveAllBtn.Glyph:=Bitmap;
  MoveAllBtn.Layout:=Layout;
  ReplaceBtn.Glyph:=Bitmap1;
  ReplaceBtn.Layout:=Layout;
  Bitmap.Free;
  Bitmap1.Free;
end;

{******************************************************************************+
  Function TOrganizeDialog.CheckDuplicateNames
--------------------------------------------------------------------------------
  Checks if items to copy or to move already exist in the destination-library,
  displays a message-box in this case. Returns 0 if there are no duplicates,
  1 if the user presses cancel, 2 if the user presses overwrite and 3 if the
  user presses add.
+******************************************************************************}
Function TOrganizeDialog.CheckDuplicateNames(ItemList: TStringList;
  Dest: TLibrary):Integer;
var Duplicates     : TStringList;
    Dialog         : TOrganizeCopyDialog;
    Cnt            : Integer;
begin
  Duplicates:=TStringList.Create;
  try                             
    for Cnt:=0 to ItemList.Count-1 do if Dest.NamedItems[ItemList[Cnt]]<>NIL then
        Duplicates.Add(ItemList[Cnt]);
    if Duplicates.Count=0 then Result:=0
    else begin
      Dialog:=TOrganizeCopyDialog.Create(Self);
      try
        Dialog.ItemList.Items:=Duplicates;
        Dialog.ShowModal;
        case Dialog.ModalResult of
          mrYes : Result:=2;
          mrNo  : Result:=3;
          else Result:=1;
        end;
      finally
        Dialog.Free;
      end;
    end;
  finally
    Duplicates.Free;
  end;
end;

Function TOrganizeDialog.CheckReferenceCount(ItemList:TStringList;
  Source:TLibrary): Boolean;
var Referenced     : TStringList;
    Dialog         : TOrganizeCopyDialog;
    Cnt            : Integer;
    ModalResult    : TModalResult;
begin
  Referenced:=TStringList.Create;
  try
    for Cnt:=0 to ItemList.Count-1 do
        if Source.ReferenceCount[ItemList.Objects[Cnt]]>0 then
            Referenced.Add(ItemList[Cnt]);
    if Referenced.Count=0 then Result:=TRUE
    else begin
      Dialog:=TOrganizeCopyDialog.Create(Self);
      Dialog.References:=TRUE;
      try
        Dialog.ItemList.Items:=Referenced;
        ModalResult:=Dialog.ShowModal;
        // if user selected don't copy remove the referenced items from the list
        if ModalResult=mrNo then for Cnt:=ItemList.Count-1 downto 0 do
            if Source.ReferenceCount[ItemList.Objects[Cnt]]>0 then ItemList.Delete(Cnt);
        Result:=ModalResult<>mrCancel;
      finally
        Dialog.Free;
      end;
    end;
  finally
    Referenced.Free;
  end;
end;

Function TOrganizeDialog.CopyEntries(ItemList: TStringList; Source,
  Dest: TLibrary):Boolean;
var Action         : Integer;
    Cnt            : Integer;
    Cnt1           : Integer;
begin
  Action:=CheckDuplicateNames(ItemList,Dest);
  if Action<>1 then begin
    for Cnt:=0 to ItemList.Count-1 do begin
      if Dest.NamedItems[ItemList[Cnt]]<>NIL then begin
        if Action=2 then Dest.Replace(ItemList.Objects[Cnt])
        else begin
          Cnt1:=1;
          while Dest.NamedItems[ItemList[Cnt]+' '+IntToStr(Cnt1)]<>NIL do Inc(Cnt1);
          Dest.AddCopyNamed(ItemList.Objects[Cnt],ItemList[Cnt]+' '+IntToStr(Cnt1));
        end;
      end
      else Dest.AddCopy(ItemList.Objects[Cnt]);
    end;
    UpdateLibraryControls(Dest);
    UpdateControls(Self);
  end;
  Result:=Action<>1;
end;

Procedure TOrganizeDialog.MoveEntries(ItemList: TStringList; Source,
  Dest: TLibrary);
var Cnt            : Integer;
begin
  if CheckReferenceCount(ItemList,Source) and CopyEntries(ItemList,Source,Dest) then begin
    for Cnt:=0 to ItemList.Count-1 do if Source.ReferenceCount[ItemList.Objects[Cnt]]=0 then
        Source.Delete(ItemList[Cnt]);
    UpdateLibraryControls(Source); 
    UpdateControls(Self);
  end;
end;

Procedure TOrganizeDialog.SetSymMosaic(ASide:TSide;AMosaic:Boolean);
var NewColCount  : Integer;
begin
  with FLibraries[ASide,opSymbols].List do begin
    if AMosaic then NewColCount:=4
    else NewColCount:=1;
    CellWidth:=(ClientWidth-1) Div NewColCount;
    ColCount:=NewColCount;
    ShowHint:=AMosaic;
  end;
end;

Procedure TOrganizeDialog.LeftSymListMouseMove(Sender:TObject;Shift:TShiftState;
    X,Y:Integer);
var Index        : Integer;
begin
  Index:=LeftSymList.ItemAtPos(Point(X,Y),True);
  if Index<>FLeftLastIndex then begin
    Application.CancelHint;
    FLeftLastIndex:=Index;
    if Index<0 then LeftSymList.Hint:=''
    else LeftSymList.Hint:=PToStr(TSymbolLibrary(FLibraries[siLeft,opSymbols]).SymbolInfo.GetSym(Index)^.Name);
  end;
end;

Procedure TOrganizeDialog.RightSymListMouseMove(Sender:TObject;Shift:TShiftState;
    X,Y:Integer);
var Index        : Integer;
begin
  Index:=RightSymList.ItemAtPos(Point(X,Y),True);
  if Index<>FRightLastIndex then begin
    Application.CancelHint;
    FRightLastIndex:=Index;
    if Index<0 then RightSymList.Hint:=''
    else RightSymList.Hint:=PToStr(TSymbolLibrary(FLibraries[siRight,opSymbols]).
        SymbolInfo.GetSym(Index)^.Name);
  end;
end;

{==============================================================================+
  TLibrary
+==============================================================================}

Function TLibrary.Close:Boolean;
begin
  if CloseQuery then begin
    FType:=ltNone;
    FFileName:='';
    FModified:=FALSE;
    Result:=TRUE;
  end
  else Result:=FALSE;
end;

Function TLibrary.CloseQuery:Boolean;
var MessageResult  : TModalResult;
begin
  if FModified then begin
    MessageResult:=MessageDialog(MlgStringList['OrganizeDialog',25],
        mtConfirmation,mbYesNoCancel,0);
    if MessageResult=mrYes then begin
      if LibraryType=ltNew then Result:=SaveAs
      else Result:=Save;
    end
    else if MessageResult=mrNo then Result:=TRUE
    else Result:=FALSE;
  end
  else Result:=TRUE;
end;

Constructor TLibrary.Create(AParent:TOrganizeDialog;AList:TDipList;ALabel:TWLabel);
begin
  inherited Create;
  FList:=AList;
  FList.OnDrawCell:=DrawCell;
  FLabel:=ALabel;
  FParent:=AParent;
  FType:=ltNone;
end;
                         
Procedure TLibrary.New;
begin
  FFileName:='';
  FType:=ltNew;
  FModified:=FALSE;
end;

Procedure TLibrary.Open(const AFileName:String);
begin
  FFileName:=AFileName;
  FType:=ltFile;
  FModified:=FALSE;
end;

Procedure TLibrary.OpenProjectLibrary(Param:Pointer;Param1:Pointer);
begin
  FType:=ltProject;
  FModified:=FALSE;
end;

Function TLibrary.Save:Boolean;
begin
  if (FType=ltProject) and FModified then FParent.ProjectModified:=TRUE; 
  if FType<>ltProject then FType:=ltFile;
  Result:=TRUE;
end;

Function TLibrary.SaveAs:Boolean;
begin
  Result:=FALSE;
end;

{==============================================================================+
  TLineLibrary
+==============================================================================}

Procedure TLineLibrary.AddCopy(AItem: Pointer);
begin
  AddCopyNamed(AItem,TXLineStyle(AItem).Name);
end;

Procedure TLineLibrary.AddCopyNamed(AItem: Pointer; const NewName: String);
var NewItem        : TXLineStyle;
begin
  NewItem:=TXLineStyle.Create;
  NewItem.Assign(AItem);
  NewItem.Number:=0;
  NewItem.ReferenceCount:=0;
  NewItem.Name:=NewName;
  FLineStyles.Add(NewItem);
  UpdateStylesList;
  FModified:=TRUE;
end;

Constructor TLineLibrary.Create(AParent:TOrganizeDialog;AList:TDipList;ALabel:TWLabel);
begin
  inherited Create(AParent,AList,ALabel);
  FLineStyles:=TXLineStyleList.Create;
  FLineList:=TStringList.Create;
  FLineList.Sorted:=TRUE;
  FLineList.Duplicates:=dupAccept;
end;

Procedure TLineLibrary.Delete(const AName:String);
var Item           : TXLineStyle;
    AIndex         : Integer;
begin
  AIndex:=FLineList.IndexOf(AName);
  if AIndex>=0 then begin
    Item:=TXLineStyle(FLineList.Objects[AIndex]);
    FLineList.Delete(AIndex);
    FLineStyles.Delete(Item);
    FModified:=TRUE;
    UpdateStylesList;
  end;
end;

Destructor TLineLibrary.Destroy;
begin
  FLineStyles.Free;
  FLineList.Free;
  inherited Destroy; 
end;

Procedure TLineLibrary.DrawCell(Sender: TObject; Canvas: TCanvas;
  Index: Integer; Rect: TRect);
var ARect        : TRect;
    AText        : String;
    Y            : Integer;
    Item         : TXLineStyle;
    Points       : Array[0..1] of TPoint;
    AZoom        : Double;
begin
  with Canvas do begin
    FillRect(Rect);
    ARect:=Classes.Rect(Rect.Left+2,Rect.Top,Rect.Left+70,Rect.Bottom);
    Y:=CenterHeightInRect(0,Rect);
    Item:=TXLineStyle(FLineList.Objects[Index]);
    with ARect do begin
      Points[0]:=Point(Left+1,Y);
      Points[1]:=Point(Right-1,Y);

      if RectHeight(Item.ClipRect) = 0 then begin
         AZoom := FList.CellWidth / Item.Items[0].Cycle / 10;
      end
      else begin
         AZoom:=(FList.RowHeight-2)/RectHeight(Item.ClipRect)/5;
      end;

      XPolyline(Canvas.Handle,Points,2,Item.StyleDef^.XLineDefs,
          Item.StyleDef^.Count,clBlack,clBlack,True,AZoom);
    end;
    AText:=Item.Name;
    ARect:=InflatedWidth(Rect,-75,0);
    TextRect(ARect,ARect.Left,CenterHeightInRect(TextHeight(AText),ARect),AText);
  end;
end;

Function TLineLibrary.GetCount:Integer;
begin
  Result:=FLineStyles.Count;
end;

Function TLineLibrary.GetItem(AIndex:Integer):Pointer;
begin
  Result:=Pointer(FLineList.Objects[AIndex]);
end;

Function TLineLibrary.GetName(AIndex:Integer): String;
begin
  Result:=TXLineStyle(FLineList.Objects[AIndex]).Name;
end;

Function TLineLibrary.GetNamedItem(const AName: String): Pointer;
var Index          : Integer;
begin
  Index:=FLineList.IndexOf(AName);
  if Index<0 then Result:=NIL
  else Result:=Pointer(FLineList.Objects[Index]);
end;

Function TLineLibrary.GetReferenceCount(AItem:Pointer): Integer;
begin
  Result:=TXLineStyle(AItem).ReferenceCount;
end;

Procedure TLineLibrary.New;
begin
{++ Ivanoff BUG#419 BUILD#95
New LineStyle library was not opened when one was created.}
{  inherited New;
  FLineStyles.Clear;
  UpdateStylesList;}
     If Parent.SaveStylesDialog.Execute Then Begin
        inherited New;
        inherited Open(Parent.SaveStylesDialog.FileName);
        FLineStyles.Clear;
        UpdateStylesList;
        End;
{-- Ivanoff}
end;

Procedure TLineLibrary.Open(const AFileName:String);
begin
  inherited Open(AFileName);
  FLineStyles.Clear;
  FLineStyles.ReadFromFile(FFileName);
  UpdateStylesList;
end;
                                         
Procedure TLineLibrary.OpenProjectLibrary(Param:Pointer;Param1:Pointer);
begin
  inherited OpenProjectLibrary(Param);
  FOrgLineStyles:=Param;
  FLineStyles.Assign(FOrgLineStyles);
  UpdateStylesList;
end;

Procedure TLineLibrary.Replace(AItem:Pointer);
var Item           : TXLineStyle;
    AIndex         : Integer;
    OldNumber      : Integer;
begin
  AIndex:=FLineList.IndexOf(TXLineStyle(AItem).Name);
  if AIndex>0 then begin
    Item:=TXLineStyle(FLineList.Objects[AIndex]);
    OldNumber:=Item.Number;
    Item.Assign(AItem);
    Item.Number:=OldNumber;
    FModified:=TRUE;
    UpdateStylesList;
  end;
end;

Function TLineLibrary.Save:Boolean;
begin                      
  if inherited Save then begin
    if FType=ltProject then FOrgLineStyles.Assign(FLineStyles)
    else FLineStyles.WriteToFile(FFileName);
    FModified:=FALSE;
    Result:=TRUE;
  end  
  else Result:=FALSE;
end;

Function TLineLibrary.SaveAs: Boolean;
var OldName        : String;
begin
  Result:=FALSE;
  if Parent.SaveStylesDialog.Execute then begin
    OldName:=FFileName;
    FFileName:=Parent.SaveStylesDialog.FileName;
    Result:=Save;
    if Result then FFileName:=OldName;
  end;
end;

Procedure TLineLibrary.UpdateStylesList;
var XLineStyle   : TXLineStyle;
    Cnt          : Integer;
begin
  FLineList.Clear;
  for Cnt:=0 to FLineStyles.Capacity-1 do begin
    XLineStyle:=FLineStyles.Items[Cnt];
    if XLineStyle<>NIL then FLineList.AddObject(XLineStyle.Name,XLineStyle);
  end;
end;

{==============================================================================+
  TFillLibrary
+==============================================================================}

Procedure TFillLibrary.AddCopy(AItem:Pointer);
begin
  AddCopyNamed(AItem,TCustomXFill(AItem).Name);
end;

Procedure TFillLibrary.AddCopyNamed(AItem: Pointer; const NewName: String);
var NewItem        : TCustomXFill;
begin
  NewItem:=TCustomXFillClass(TObject(AItem).ClassType).Create;
  NewItem.Assign(AItem);
  NewItem.Number:=0;
  NewItem.ReferenceCount:=0;
  NewItem.Name:=NewName;
  FFillStyles.Add(NewItem);
  FModified:=TRUE;
  UpdateStylesList;
end;

Constructor TFillLibrary.Create(AParent:TOrganizeDialog;AList:TDipList;ALabel:TWLabel);
begin
  inherited Create(AParent,AList,ALabel);
  FFillStyles:=TXFillStyleList.Create;
  FFillList:=TStringList.Create;
  FFillList.Sorted:=TRUE;
  FFillList.Duplicates:=dupAccept;
end;

Procedure TFillLibrary.Delete(const AName:String);
var Item           : TCustomXFill;
    Aindex         : Integer;
begin
  AIndex:=FFillList.IndexOf(AName);
  if AIndex>=0 then begin
    Item:=TCustomXFill(FFillList.Objects[AIndex]);
    FFillList.Delete(AIndex);
    FFillStyles.Delete(Item);
    FModified:=TRUE;
  end;
end;

Destructor TFillLibrary.Destroy;
begin
  FFillStyles.Free;
  FFillList.Free;
  inherited Destroy;
end;

Procedure TFillLibrary.DrawCell(Sender: TObject; Canvas: TCanvas;
  Index: Integer; Rect: TRect);
var ARect        : TRect;
    Item         : TCustomXFill;
    Points       : TRectCorners;
begin
 with Canvas do begin
    FillRect(Rect);
    ARect:=Classes.Rect(Rect.Left+2,Rect.Top,Rect.Left+70,Rect.Bottom);
    Item:=TCustomXFill(FFillList.Objects[Index]);
    RectCorners(InflatedRect(ARect,0,-2),Points);
    if Item is TVectorXFill then with Item as TVectorXFill do
      XLFillPolygon(Handle,Points,4,StyleDef^.XFillDefs,StyleDef^.Count,
          0,0,5/MinDistance,0)
    else if Item is TBitmapXFill then with Item as TBitmapXFill do
        BMFillPolygon(Handle,Points,4,StyleDef(Handle)^.BitmapHandle,srcCopy,1);
    ARect:=InflatedWidth(Rect,-75,0);
    TextRect(ARect,ARect.Left,CenterHeightInRect(TextHeight(Item.Name),ARect),Item.Name);
  end;
end;

Function TFillLibrary.GetCount: Integer;
begin
  Result:=FFillStyles.Count;
end;

Function TFillLibrary.GetItem(AIndex:Integer):Pointer;
begin
  Result:=Pointer(FFillList.Objects[AIndex]);
end;

Function TFillLibrary.GetName(AIndex: Integer): String;
begin
  Result:=TCustomXFill(FFillList.Objects[AIndex]).Name;
end;

Function TFillLibrary.GetNamedItem(const AName: String): Pointer;
var AIndex         : Integer;
begin
  AIndex:=FFillList.IndexOf(AName);
  if AIndex<0 then Result:=NIL
  else Result:=FFillList.Objects[AIndex];
end;

Function TFillLibrary.GetReferenceCount(AItem:Pointer): Integer;
begin
  Result:=TCustomXFill(AItem).ReferenceCount;
end;

Procedure TFillLibrary.New;                     
begin
{++ Ivanoff BUG#419 BUILD#95
New FillStyle library was not opened when one was created.}
{  inherited New;
  FFillStyles.Clear;
  UpdateStylesList;}
     If Parent.SaveStylesDialog.Execute Then Begin
        inherited New;
        inherited Open(Parent.SaveStylesDialog.FileName);
        FFillStyles.Clear;
        UpdateStylesList;
        End;
{-- Ivanoff}
end;

Procedure TFillLibrary.Open(const AFileName:String);
begin
  inherited Open(AFileName);
  FFillStyles.Clear;
  FFillStyles.ReadFromFile(FFileName);
  UpdateStylesList;
end;

Procedure TFillLibrary.OpenProjectLibrary(Param:Pointer;Param1:Pointer);
begin
  inherited OpenProjectLibrary(Param);
  FOrgFillStyles:=Param;
  FFillStyles.Assign(FOrgFillStyles);
  UpdateStylesList;
end;

Procedure TFillLibrary.Replace(AItem:Pointer);
var Item           : TCustomXFill;
    AIndex         : Integer;
    OldNumber      : Integer;
begin
  AIndex:=FFillList.IndexOf(TCustomXFill(AItem).Name);
  if AIndex>0 then begin
    Item:=TCustomXFill(FFillList.Objects[AIndex]);
    OldNumber:=Item.Number;
    Item.Assign(AItem);
    Item.Number:=OldNumber;
    FModified:=TRUE;
    UpdateStylesList;
  end;
end;

Function TFillLibrary.Save:Boolean;
begin
  if inherited Save then begin
    if FType=ltProject then FOrgFillStyles.Assign(FFillStyles)
    else FFillStyles.WriteToFile(FFileName);
    FModified:=FALSE; 
    Result:=TRUE;
  end
  else Result:=FALSE;
end;

Function TFillLibrary.SaveAs: Boolean;
var OldName        : String;
begin
  Result:=FALSE;
  if Parent.SaveStylesDialog.Execute then begin
    OldName:=FFileName;
    FFileName:=Parent.SaveStylesDialog.FileName;
    Result:=Save;
    if Result then FFileName:=OldName;
  end;
end;

Procedure TFillLibrary.UpdateStylesList;
var XFillStyle   : TCustomXFill;
    Cnt          : Integer;
begin
  FFillList.Clear;
  for Cnt:=0 to FFillStyles.Capacity-1 do begin
    XFillStyle:=FFillStyles.Items[Cnt];
    if XFillStyle<>NIL then FFillList.AddObject(XFillStyle.Name,XFillStyle);
  end;
end;

{==============================================================================+
  TSymbolLibrary
+==============================================================================}

Procedure TSymbolLibrary.AddCopy(AItem:Pointer);
begin
  AddCopyNamed(AItem,PSGroup(AItem)^.GetName);
end;

Procedure TSymbolLibrary.AddCopyNamed(AItem:Pointer;const NewName:String);
var NewSym         : PSGroup;
begin
//  NewSym:=MakeObjectCopy(PSGroup(AItem));
  NewSym:=PSGroup(AItem);
//  NewSym^.UseCount:=0;
  NewSym^.SetName(NewName);
//  if FType=ltProject then
  NewSym^.SetLibName('');
//  else NewSym^.SetLibName(FExtLib.LibName^);
//  FSymbols^.InsertSymbol(NIL,NewSym,FALSE);

  FSymbols.CopySymIntoProject(FPInfo,NewSym);
  FSymInfo.AddSym(NewSym);
  FModified:=TRUE;
end;

Constructor TSymbolLibrary.Create(AParent:TOrganizeDialog;AList:TDipList;ALabel:TWLabel);
begin
  inherited Create(AParent,AList,ALabel);
  FPInfo:=System.New(PPaint,Init);
  FPInfo^.ExtCanvas.Cached:=FALSE;
  Dispose(FPInfo^.Fonts,Done);
  FSymbols:=System.New(PSymbols,Init(NIL));
  FFonts:=System.New(PFonts,Init);
  FPInfo.Fonts:=FFonts;
end;

Destructor TSymbolLibrary.Destroy;
begin
  FPInfo^.ExtCanvas.Handle:=0;
  FPInfo^.Fonts:=NIL;
  Dispose(FPInfo,Done);
  Dispose(FSymbols,Done);
  Dispose(FFonts,Done);
  if FSymInfo<>NIL then FSymInfo.Free;
  inherited Destroy;
end;

Procedure TSymbolLibrary.Delete(const AName:String);
var Symbol         : PSGroup;
begin
  Symbol:=SymbolFromName(AName);
  Assert(Symbol<>NIL);
  if Symbol<>NIL then begin
    FSymInfo.Delete(Symbol);
    FSymbols.DeleteSymbol(Symbol);
    FModified:=TRUE;
  end;
end;

Procedure TSymbolLibrary.DrawCell(Sender:TObject; Canvas:TCanvas;
  Index:Integer; Rect:TRect);
var aSym           : PSGroup;
    Y              : Integer;
begin
  if FSymInfo=NIL then Exit;
  if FSymInfo.SymCount<Index then Exit;
  aSym:=FSymInfo.GetSym(Index);

  Canvas.FillRect(Rect);

  if aSym<>nil then begin

//    if (ShowUsedSym) and (aSym^.UseCount = 0) then exit;

    Rect.Right:=Rect.Left+RectHeight(Rect);
    FPInfo^.ExtCanvas.Handle:=Canvas.Handle;
    aSym^.DrawSymbolInRect(FPInfo,Rect);
    if FList.ColCount=1 then begin
      // force VCL to recreate the drawing-objects
      Canvas.Refresh;
      // draw the symbol-name
      Y:=CenterHeightInRect(Abs(Canvas.Font.Height),Rect);
      Canvas.TextOut(Rect.Right+2,Y,PToStr(aSym^.Name));
    end;
  end;
end;

Function TSymbolLibrary.GetCount:Integer;
begin
  if FSymInfo=NIL then Result:=0
  else Result:=FSymInfo.SymCount
end;

Function TSymbolLibrary.GetItem(AIndex:Integer):Pointer;
begin
  Result:=FSymInfo.GetSym(AIndex);
end;

Function TSymbolLibrary.GetName(AIndex:Integer):String;
begin
  Result:=PToStr(FSymInfo.GetSym(AIndex)^.Name);
end;

Function TSymbolLibrary.GetNamedItem(const AName:String):Pointer;
var Cnt            : Integer;
begin
  for Cnt:=0 to FSymInfo.SymCount-1 do if CompareText(
      AName,PToStr(FSymInfo.GetSym(Cnt)^.Name))=0 then begin
    Result:=FSymInfo.GetSym(Cnt);
    Exit;
  end;
  Result:=NIL;
end;

Function TSymbolLibrary.GetReferenceCount(AItem:Pointer):Integer;
begin
  Result:=PSGroup(AItem)^.UseCount;
end;

Procedure TSymbolLibrary.New;
begin
  if Parent.SaveSymbolsDialog.Execute then begin
    inherited Open(Parent.SaveSymbolsDialog.FileName);
    FreeCurrent;
    FExtLib:=ExtLibAlreadyOpened(FFileName);
    if FExtLib=NIL then FExtLib:=System.New(PExtLib,Init(NIL,FFileName));
    if not FExtLib^.Loaded then FExtLib^.NewExtLib(FALSE);
    FOrgFonts:=FExtLib^.Fonts;
    FFonts.Assign(FOrgFonts);
    FPInfo.Fonts:=FFonts;
    FOrgSymbols:=FExtLib^.Symbols;
    FSymbols.Assign(FOrgSymbols);
    FSymInfo:=TSymInfo.Create(FSymbols,NIL,@NullStr);
  end;
end;

Procedure TSymbolLibrary.Open(const AFileName:String);
begin
  inherited Open(AFileName);
  FreeCurrent;
  FExtLib:=ExtLibAlreadyOpened(AFileName);
  if FExtLib=NIL then FExtLib:=System.New(PExtLib,Init(NIL,AFileName));
  if not FExtLib^.Loaded then FExtLib^.ReLoad;
  FOrgFonts:=FExtLib^.Fonts;
  FFonts.Assign(FOrgFonts);
  FOrgSymbols:=FExtLib^.Symbols;
  FSymbols.Assign(FOrgSymbols);
  FSymInfo:=TSymInfo.Create(FSymbols,NIL,@NullStr);
end;

Procedure TSymbolLibrary.FreeCurrent;
begin
//!?!?!  if FExtLib<>NIL then Dispose(FExtLib,Done);
  FExtLib:=NIL;
end;

Procedure TSymbolLibrary.OpenProjectLibrary(Param:Pointer;Param1:Pointer);
//++ Glukhov Bug#73 BUILD#128 06.09.00
var   TmpExtLibs  : TList;
//-- Glukhov Bug#73 BUILD#128 06.09.00
begin
  inherited OpenProjectLibrary(Param);
  FreeCurrent;
  FOrgSymbols:=Param;
  FOrgFonts:=Param1;
//++ Glukhov Bug#73 BUILD#128 06.09.00
// Save and Hide the extern libraries
  TmpExtLibs:= FOrgSymbols.ExternLibs;
  FOrgSymbols.ExternLibs:= TList.Create;
//-- Glukhov Bug#73 BUILD#128 06.09.00
  FSymbols.Assign(FOrgSymbols);
//++ Glukhov Bug#73 BUILD#128 06.09.00
// Restore the Saved and Hidden extern libraries
//  FOrgSymbols.ExternLibs.Clear;
  FOrgSymbols.ExternLibs:= TmpExtLibs;
//-- Glukhov Bug#73 BUILD#128 06.09.00
  FFonts.Assign(FOrgFonts);
  FSymInfo:=TSymInfo.Create(FSymbols,NIL,@NullStr,ShowUsedSym);
end;

Procedure TSymbolLibrary.Replace(AItem:Pointer);
var OldSymbol      : PSGroup;
    NewSymbol      : PSGroup;
begin
  OldSymbol:=SymbolFromName(PToStr(PSGroup(AItem)^.Name));
  if OldSymbol<>NIL then begin
    // create a copy of the new symbol
//    NewSymbol:=MakeObjectCopy(AItem);
    NewSymbol:=PSGroup(AItem);
    // set index and use-count to valus of old symbol
    NewSymbol^.UseCount:=OldSymbol^.UseCount;
    NewSymbol^.Index:=OldSymbol^.Index;
    NewSymbol^.SetLibName('');
    Fsymbols.CopySymIntoProject(FPInfo,NewSymbol);
//    FSymbols.DeleteSymbol(OldSymbol);
//    FSymbols.InsertSymbol(NIL,NewSymbol,FALSE);
    FSymInfo.Delete(OldSymbol);
    FSymInfo.AddSym(NewSymbol);
    FModified:=TRUE;
  end;
end;

Function TSymbolLibrary.Save:Boolean;
{++ Ivanoff BUG#419 BUILD#95}
Var sS, sS1, sS2: String;
{-- Ivanoff}
begin
  if inherited Save then begin
    FOrgSymbols.Assign(FSymbols);
    FOrgFonts.Assign(FFonts);
{++ Ivanoff BUG#419 BUILD#95
Symbol library did not do saving "SaveAs".}
//    if LibraryType<>ltProject then FExtLib.SaveExtLib;
    if LibraryType<>ltProject then Begin
       sS := FExtLib.LibName^; sS1 := FExtLib.LibFileName^; sS2 := FExtLib.LibRelName^;
       FExtLib.LibName^ := ExtractFileName(FFileName); FExtLib.LibFileName^ := FFileName; FExtLib.LibRelName^ := ExtractFileName(FFileName);
       FExtLib.SaveExtLib;
       FExtLib.LibName^ := sS; FExtLib.LibFileName^ := sS1; FExtLib.LibRelName^ := sS2;
       End;
{-- Ivanoff}
    UserInterface.Update([uiSymbols]);
    FModified:=FALSE;
    Result:=TRUE;
  end
  else Result:=FALSE;
end;

Procedure TOrganizeDialog.DeleteActionExecute(Sender: TObject);
var ASide          : TSide;
    APage          : TOrganizePage;
    Item           : Pointer;
begin
  if FLastLibrary<>NIL then begin
    GetLibraryPosition(FLastLibrary,APage,ASide);
    Item:=FLastLibrary[FLastLibrary.List.ItemIndex];
    if FLastLibrary.ReferenceCount[Item]>0 then MessageDialog(MlgSection[28],
        mtError,[mbOK],0)
    else if MessageDialog(MlgSection[29],mtConfirmation,[mbYes,mbNo],0,mbNo)=mrYes then begin
      FLastLibrary.Delete(FLastLibrary.Names[FLastLibrary.List.ItemIndex]);
      UpdateLibraryControls(FLastLibrary);
      UpdateControls(Self);
    end;
  end;
end;

function TSymbolLibrary.SymbolFromName(const Name: String): PSGroup;
var Cnt            : Integer;
begin
  for Cnt:=0 to FSymInfo.SymCount-1 do if AnsiCompareText(Name,
      PToStr(FSymInfo.GetSym(Cnt)^.Name))=0 then begin
    Result:=FSymInfo.GetSym(Cnt);
    Exit;
  end;
  Result:=NIL;
end;

Function TSymbolLibrary.SaveAs: Boolean;
var OldName        : String;
begin
  Result:=FALSE;
  if Parent.SaveSymbolsDialog.Execute then begin
    OldName:=FFileName;
{++ Ivanoff BUG#419 BUILD#95
Symbol library did not do saving "SaveAs".}
//     FFileName:=Parent.SaveStylesDialog.FileName;
    FFileName:=Parent.SaveSymbolsDialog.FileName;
{-- Ivanoff}
    Result:=Save;
    if Result then FFileName:=OldName;
  end;
end;

procedure TOrganizeDialog.CBUsedClick(Sender: TObject);
var
i: Integer;
begin
  ShowUsedSym:=not ShowUsedSym;
  if FOrgSymbols<>NIL then begin
     SetProjectLibrary(GetCurrentPage,siLeft);
  end;
  LeftSymList.Repaint;
end;

end.
