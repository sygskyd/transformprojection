�
 TVIEWPROPERTIESDIALOG 0_  TPF0TViewPropertiesDialogViewPropertiesDialogLeft� TopvBorderStylebsDialogCaption!1ClientHeight� ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderShowHint	OnCloseQueryFormCloseQuery	OnDestroyFormDestroyOnHideFormHideOnShowFormShowPixelsPerInch`
TextHeight TButtonOKBtnLeft� Top� WidthYHeightCaption!4Default	ModalResultTabOrder  TButton	CancelBtnLeft*Top� WidthYHeightCancel	Caption!5ModalResultTabOrder  TPageControlNotebookLeftTopWidth}Height� 
ActivePageGeneralSheetTabOrder  	TTabSheetGeneralSheetCaption!2 TWLabelWLabel1LeftTopWidth9HeightAutoSizeCaption!12FocusControlNameEditAlignToControl	alcCenter  TWLabelWLabel7LeftTop� Width9HeightAutoSizeCaption!17FocusControlDescriptionEditAlignToControl	alcCenter  
TWGroupBoxGroup1LeftTop0WidthaHeightICaption!13TabOrderBoxStyle	bsTopLine 	TCheckBoxStartupCheckLeftTop4Width� HeightCaption!16TabOrder   	TCheckBoxSaveRangeCheckLeftTopWidth� HeightCaption!14TabOrderOnClickSaveRangeCheckClick  	TCheckBoxSaveLayersCheckLeftTop"Width� HeightCaption!15TabOrderOnClickSaveLayersCheckClick   TEditNameEditLeftHTopWidth� HeightTabOrder OnExitNameEditExit  TEditDescriptionEditLeftHTop� Width!Height	MaxLength� TabOrderOnExitNameEditExit   	TTabSheet
RangeSheetCaption!11
ImageIndex TWLabelWLabel2LeftTopWidthIHeightAutoSizeCaption!6FocusControlXEditAlignToControl	alcCenter  TWLabelWLabel3LeftTop,WidthIHeightAutoSizeCaption!7FocusControlYEditAlignToControl	alcCenter  TWLabelWLabel4LeftTopDWidthIHeightAutoSizeCaption!8FocusControl	WidthEditAlignToControl	alcCenter  TWLabelWLabel5LeftTop\WidthIHeightAutoSizeCaption!9FocusControl
HeightEditAlignToControl	alcCenter  TWLabelWLabel6LeftToptWidthIHeightAutoSizeCaption!10FocusControlRotationEditAlignToControl	alcCenter  TEditXEditLeftXTopWidthyHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   TEditYEditLeftXTop(WidthyHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TEdit	WidthEditLeftXTop@WidthyHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TEdit
HeightEditLeftXTopXWidthyHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TEditRotationEditLeftXToppWidthyHeightTabOrder  TSpinBtnXSpinLeft� TopHeight	Arrowkeys	Increment       ��?Max �P]���CMin �P]����	ValidatorXPosVal  TSpinBtnYSpinLeft� Top(Height	Arrowkeys	Increment       ��?Max �P]���CMin �P]����	ValidatorYPosVal  TSpinBtn	WidthSpinLeft� Top@Height	Arrowkeys	Increment       ��?Max �P]���CMin       ��?	ValidatorWidthVal  TSpinBtn
HeightSpinLeft� TopXHeight	Arrowkeys	Increment       ��?Max �P]���CMin       ��?	Validator	HeightVal  TSpinBtnRotationSpinLeft� ToppHeight	Arrowkeys	Increment       ��?Max       �@Min       ��	ValidatorRotationVal   	TTabSheetLayersSheetCaption!3
ImageIndex TColumnListboxLayerListboxLeftTopWidthcHeight� Columns	LineBreakOnPaintLayerListboxColumns0PaintWidth AutoSize		Separator		LineBreakHint!18MinWidth2OnPaintLayerListboxColumns1PaintWidth2 	Separator		LineBreakHint!19OnPaintLayerListboxColumns2PaintWidth 	LineBreakHint!20OnPaintLayerListboxColumns3PaintWidth  
ItemHeightMultiSelect	
Scrollbars
ssVerticalTabOrder     
TValidatorNameValEditNameEdit	MaxLength� 
ValidChars	' '..#255LeftTop�   TMlgSection
MlgSectionSectionViewPropertiesDialogLeft(Top�   TMeasureLookupValidatorXPosValEditXEditUseableUnits� MinValue.UnitsmuMetersMinValue.Value hbL�B����MaxValue.UnitsmuMetersMaxValue.Value hbL�B���CUnitsmuMetersLeftHTop�   TMeasureLookupValidatorYPosValEditYEditUseableUnits� MinValue.UnitsmuMetersMinValue.Value hbL�B����MaxValue.UnitsmuMetersMaxValue.Value hbL�B���CUnitsmuMetersLeftXTop�   TMeasureLookupValidatorWidthValEdit	WidthEditUseableUnits� MinValue.UnitsmuMetersMinValue.Value �O��n��?MaxValue.UnitsmuMetersMaxValue.Value hbL�B���CUnitsmuMetersLefthTop�   TMeasureLookupValidator	HeightValEdit
HeightEditUseableUnits� MinValue.UnitsmuMetersMinValue.Value �O��n��?MaxValue.UnitsmuMetersMaxValue.Value hbL�B���CUnitsmuMetersLeftxTop�   TMeasureLookupValidatorRotationValEditRotationEditUseableUnits � DefaultValue.Units	muDegreesMinValue.Units	muDegreesMinValue.Value       ��MaxValue.Units	muDegreesMaxValue.Value       �@Units	muDegreesLeft� Top�    