Unit SymbolStyleDlg;

Interface

Uses Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     ExtCtrls,StdCtrls,Validate,Spinbtn,WCtrls,MultiLng,StyleDef,Measures,
     AM_Group,AM_Proj;

Type TSymbolStyleDialog = class(TWForm)
       AngleEdit        : TEdit;
       AngleSpin        : TSpinBtn;
       AngleVal         : TMeasureLookupValidator;
       Bevel1           : TBevel;
       CancelBtn        : TButton;
       ControlPanel     : TPanel;
       MlgSection       : TMlgSection;
       OKBtn            : TButton;
       ScaleIndependCheck: TCheckBox;
       SizeEdit         : TEdit;
       SizeSpin         : TSpinBtn;
       SizeVal          : TMeasureLookupValidator;
       SymbolSelectBtn  : TButton;
       SymbolWindow     : TWindow;
       WLabel1          : TWLabel;
       WLabel2          : TWLabel;
       Procedure   FormCloseQuery(Sender: TObject; var CanClose: Boolean);
       Procedure   FormCreate(Sender: TObject);
       Procedure   FormDestroy(Sender: TObject);
       Procedure   FormShow(Sender: TObject);
       Procedure   FormHide(Sender: TObject);
       Procedure   SymbolSelectBtnClick(Sender: TObject);
       Procedure   SymbolWindowPaint(Sender: TObject);
       procedure ScaleIndependCheckClick(Sender: TObject);
      Private
       FProject         : PProj;
       FOrgSymbolStyle  : TSymbolProperties;
       FSymbol          : PSGroup;
       FSymbolStyle     : TSymbolProperties;
       FFormShown       : Boolean;
       Procedure   CheckSize;
      Public
       Property    Project:PProj read FProject write FProject;
       Property    SymbolStyle:TSymbolProperties read FSymbolStyle write FOrgSymbolStyle;
     end;

Implementation

{$R *.DFM}

Uses AM_Def,AM_Layer,PropCollect,SymbolSelDlg;

Procedure TSymbolStyleDialog.FormCreate(Sender: TObject);
begin
  FSymbolStyle:=TSymbolProperties.Create;
end;

Procedure TSymbolStyleDialog.FormDestroy(Sender: TObject);
begin
  FSymbolStyle.Free;
end;

Procedure TSymbolStyleDialog.FormShow(Sender: TObject);
begin
  FFormShown:=False;
  if FOrgSymbolStyle<>NIL then FSymbolStyle.Assign(FOrgSymbolStyle);
  with FSymbolStyle do begin
    if Symbol>=0 then begin
       FSymbol:=PSGroup(PLayer(FProject^.PInfo^.Symbols)^.HasIndexObject(Symbol));
    end;
    SizeVal.LookupList.Clear;
    SetSizeField(Size,SizeType,SizeVal,SizeOptions,ScaleIndependCheck);
    CheckSize;
    AngleVal.LookupList.Clear;
    SetDoubleField(Angle,1,AngleVal,muRad,AngleOptions);
    if not UpdatesEnabled then SetControlGroupEnabled(ControlPanel,FALSE);
  end;
  FFormShown:=True;
end;

procedure TSymbolStyleDialog.ScaleIndependCheckClick(Sender: TObject);
begin
     CheckSize;
     if (FFormShown) then begin
        try
           SizeEdit.SetFocus;
           if Sender <> nil then
              if TWinControl(Sender).Enabled then begin
                 TWinControl(Sender).SetFocus;
              end;
        except
        end;
     end;
end;

procedure TSymbolStyleDialog.CheckSize;
begin
     if ScaleIndependCheck.Checked then begin
        if SizeVal.Value <> 0 then SizeVal.Units:=muMillimeters;
        SizeVal.UseableUnits:=32;
        SizeVal.MinValue.Value:=1;
        SizeVal.MinValue.Units:=muMillimeters;
        SizeVal.MaxValue.Units:=muMillimeters;
     end
     else begin
        if SizeVal.Value <> 0 then SizeVal.Units:=muDrawingUnits;
        SizeVal.UseableUnits:=2;
        SizeVal.MinValue.Value:=0.1;
        SizeVal.MinValue.Units:=muDrawingUnits;
        SizeVal.MaxValue.Units:=muDrawingUnits;
     end;
     if SizeVal.Value = 0 then begin
        //ScaleIndependCheck.Checked:=False;
        //ScaleIndependCheck.Enabled:=False;
     end
     else begin
        //ScaleIndependCheck.Enabled:=True;
     end;
end;

Procedure TSymbolStyleDialog.FormHide(Sender: TObject);
begin
  if ModalResult=mrOK then begin
    with FSymbolStyle do begin
      PropertyMode:=pmSet;
      if FSymbol<>NIL then Symbol:=FSymbol^.Index;
      Size:=GetSizeField(SizeVal,SizeOptions,ScaleIndependCheck);
      SizeType:=GetSizeTypeField(SizeVal,SizeOptions,ScaleIndependCheck);
      Angle:=GetDoubleField(1,AngleVal,muRad,AngleOptions);
    end;
    if FOrgSymbolStyle<>NIL then FOrgSymbolStyle.Assign(FSymbolStyle);
  end;
end;

Procedure TSymbolStyleDialog.FormCloseQuery(Sender:TObject;var CanClose:Boolean);
begin
  CanClose:=CheckValidators(Self);
end;

Procedure TSymbolStyleDialog.SymbolWindowPaint(Sender: TObject);
var ARect          : TRect;
begin
  if FSymbol<>NIL then begin
    SymbolWindow.Canvas.TextOut(SymbolWindow.Height,23,PToStr(FSymbol^.Name));
    SymbolWindow.Canvas.TextOut(SymbolWindow.Height,25+Abs(SymbolWindow.Font.Height),
        PToStr(FSymbol^.LibName));
    ARect:=Rect(2,2,SymbolWindow.Height-4,SymbolWindow.Height-4);
    FSymbol^.DrawSymbolInRect(SymbolWindow.Canvas.Handle,ARect,Project^.PInfo^.Fonts);
  end;
end;

Procedure TSymbolStyleDialog.SymbolSelectBtnClick(Sender: TObject);
var SelectedSymbol : PSGroup;
begin
  SelectedSymbol:=GetSymbol(Self,FProject);
  if SelectedSymbol<>NIL then begin
    FSymbol:=SelectedSymbol;
    SymbolWindow.Repaint;
  end;
end;

end.
