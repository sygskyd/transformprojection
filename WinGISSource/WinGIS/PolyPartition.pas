Unit PolyPartition;

Interface

Uses Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,StdCtrls,ExtCtrls,
     AM_Proj,AM_Def,AM_Poly,AM_CPoly,AM_Input;

Const pmParallel      = 0;
      pmPuffer        = 1;
      pmIsland        = 2;
      pmFree          = 3;
      smDist          = 0;
      smArea          = 1;
      mdRight         = 0;
      mdRightUp       = 1;
      mdUp            = 2;
      mdLeftUp        = 3;
      mdLeft          = 4;
      mdLeftDown      = 5;
      mdDown          = 6;
      mdRightDown     = 7;
      csLeft          = 1;
      csRight         = 2;
      csBottom        = 3;
      csTop           = 4;

Type TPolyPart        = Class(TObject)
      protected
       Function    ReadCutLineOK:Boolean;
       Function    ReadCalcOK:Boolean;
      private
       {DLLOK          : Boolean;}
       ShortLineDir   : Boolean;
       LastPoint      : TDPoint;
       PartLineEnds   : PPoly;
       PartLineAngle  : Extended;
       LastPartLine   : PPoly;
       MoveDistX      : LongInt;
       MoveDistY      : LongInt;
       FCutLineOK     : Boolean;
       FCalcOK        : Boolean;
       MainMoveDir    : Integer;
       Procedure   SendPolyData;
      public
       AProj          : PProj;
       PolyPartProps  : TPolyPartProps;
       PartPoly       : PCPoly;
       PartLine       : PPoly;
       FCheckedMenus  : TStringList;
       ResultPoly     : PCPoly;
       RestLine       : PNewPoly;
       CutPoly        : PCPoly;
       HideDlg        : Boolean;
       bDeleteSourcePoly: Boolean;
       AutoPartLine   : Boolean;
       Constructor Create(AData:PProj);
       Destructor  Destroy; override;
       Procedure   CalcStartMovePoly;
       Procedure   CalcStateMoveDist(XM,YM:LongInt);
       Function    CheckPoint(APoly:PNewPoly;APoint:TDPoint;AFirstPoint:Boolean):Boolean;
       Procedure   CreatePartLine(APartPoly:PNewPoly;APoint:TDPoint);
       Procedure   DoPartition(ApMode,AsMode:Integer;ADist:LongInt;AAngle,AArea:Double);
       Procedure   InsertPolyToProject(ALayer:String);
       Procedure   SetMoveDist(XM,YM:LongInt);
       Procedure   SetPartLine(APoly:PNewPoly);
       Procedure   SetupPartDlg;
       Procedure   CalculateCutPolyWithArea(AArea,AAngle:Double;AMainMDir:Integer);
       Procedure   CalculateCutPolyWithDist(ADist:LongInt;AAngle:Double;AMainMDir:Integer);
       Procedure   CalculateRestPoly(FirstPoly:PCPoly);
       Procedure   MergeCutPolyWithClipRect(AMainMDir:Integer);
      published
       Property    CutLineOK:Boolean read ReadCutLineOK;
       Property    CalcOK:Boolean read ReadCalcOK;
     end;

var PolyPart       : TPolyPart;

Function  DoPartPoly(AData:PProj;DisableDDE:Boolean):Integer;
Function  DoPartPolyFree(AData:PProj;DisableDDE:Boolean):Integer;
Procedure ShowPartDlg(AData:PProj);
Procedure UpdatePartDlg(AData:PPRoj);
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
Procedure ProcStartPolyPartitionFromDB(AData:PProj);
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
procedure ProcPolyPartWithLine(AData: PProj; DLayerName: AnsiString; PolyId: Integer; LineId: Integer; ASimpleMode: Boolean);

procedure ProcPolyPartWithLineSelected(AData: PProj; ASimpleMode: Boolean);

Implementation

Uses AM_Main,Objects
     {$IFNDEF AXDLL} // <----------------- AXDLL
     ,PolyPartDlg
     {$ENDIF} // <----------------- AXDLL
     ,AM_Index,AM_View,AM_Ini,AM_Child,AM_Layer,MultiLng,MenuFn,AM_Cut,LayerHndl
     {$IFNDEF AXDLL} // <----------------- AXDLL
       {$IFNDEF WMLT}
     ,AM_DDE
       {$ENDIF}
     {$ENDIF} // <----------------- AXDLL
     ,DDEDef,AM_ProjO{,UserIntf,Rollup,ProjHndl};



procedure ProcPolyPartWithLineSelected(AData: PProj; ASimpleMode: Boolean);
var
ALayer: PLayer;
LineId: Integer;

     function GetLineId(AIndex: PIndex): Boolean;
     begin
          Result:=False;
          if AIndex^.GetObjType = ot_Poly then begin
             LineId:=AIndex^.Index;
             Result:=True;
          end;
     end;

     procedure DoAll(AIndex: PIndex);
     begin
          if AIndex^.GetObjType = ot_CPoly then begin
             ProcPolyPartWithLine(AData,ALayer^.Text^,AIndex^.Index,LineId,ASimpleMode);
          end;
     end;

begin
     LineId:=-1;
     AData^.Layers^.SelLayer^.Data^.FirstThat(@GetLineId);
     if LineId <> -1 then begin
        if ASimpleMode then begin
           ALayer:=AData^.Layers^.TopLayer;
        end
        else begin
           ALayer:=SelectLayerDialog(AData,[sloNewLayer]);
        end;
        if ALayer <> nil then begin
           AData^.Layers^.SelLayer^.Data^.ForEach(@DoAll);
        end;
     end;
end;

procedure ProcPolyPartWithLine(AData: PProj; DLayerName: AnsiString; PolyId: Integer; LineId: Integer; ASimpleMode: Boolean);
var
DLayer: PLayer;
APolyPart: TPolyPart;
AView: PView;
AIndex: PIndex;
ACPoly: PCPoly;
APoly: PPoly;
i: Integer;
ActMen: Integer;
begin
     ACPoly:=nil;
     APoly:=nil;
     if AData <> nil then begin

        if ASimpleMode then begin
           DLayer:=AData^.Layers^.TopLayer;
        end
        else begin
           DLayerName:=Trim(DLayerName);
           DLayer:=AData^.Layers^.NameToLayer(DLayerName);
        end;

        if DLayer = nil then begin
           DLayer:=NewLayerDialog1(AData,GetLangText(10154),DLayerName);
        end;
        if (DLayer <> nil) then begin
           AIndex:=New(PIndex,Init(PolyId));
           AView:=PLayer(AData^.PInfo^.Objects)^.IndexObject(AData^.PInfo,AIndex);
           if (AView <> nil) and (AView^.GetObjType = ot_CPoly) then begin
              ACPoly:=PCPoly(AView);
           end;
           AIndex^.Index:=LineId;
           AView:=PLayer(AData^.PInfo^.Objects)^.IndexObject(AData^.PInfo,AIndex);
           if (AView <> nil) and (AView^.GetObjType = ot_Poly) then begin
              APoly:=PPoly(AView);
           end;
           if (ACPoly <> nil) and (APoly <> nil) then begin
              APolyPart:=TPolyPart.Create(AData);
              ActMen:=AData^.ActualMen;
              try
                 APolyPart.bDeleteSourcePoly:=ASimpleMode;

                 APolyPart.AProj:=AData;
                 APolyPart.PartPoly:=ACPoly;
                 APolyPart.PolyPartProps.DLayerName:=DLayerName;
                 APolyPart.PolyPartProps.PartDist:=0;
                 APolyPart.PolyPartProps.PartAngle:=0;
                 APolyPart.PolyPartProps.PartMode:=pmFree;
                 APolyPart.PolyPartProps.SizeMode:=smDist;
                 APolyPart.PolyPartProps.FillArea:=True;
                 for i:=0 to APoly^.Data^.Count-1 do begin
                     AData^.NewPoly^.PointInsert(AData^.PInfo,APoly^.Data^.At(i)^,False);
                 end;
                 APolyPart.SetPartLine(AData^.NewPoly);
                 AData^.MovePoly^.SetPoly(AData^.NewPoly);
                 APolyPart.DoPartition(pmFree,smDist,0,0,0);
                 AData^.NewPoly^.EndIt(AData^.PInfo);
                 AData^.InfoPoly^.EndIt(AData^.PInfo);
                 APolyPart.InsertPolyToProject(DlayerName);
              finally
                 AData^.ActualMen:=ActMen;
                 APolyPart.Destroy;
              end;
           end;
        end;
     end;
end;

Function DoPartPoly
   (
   AData           : PProj;
   DisableDDE      : Boolean
   )
   : Integer;
  var APoly        : PCPoly;
      PolyPoint    : PDPoint;
      SendStr      : Array[0..4095] of Char;
      TempStr      : Array[0..255] of Char;
      MaxPoints    : Integer;
      DlgResult    : Integer;
  begin
    Result:=0;

    {if PolyPart.DLLOK then begin}
      if (AData^.Layers^.SelLayer^.Data^.GetCount = 1) and
         ((AData^.Layers^.SelLayer^.Data^.At(0)^.GetObjType) = ot_CPoly) then begin
        {if DisableDDE then DDEHandler.EnableDDEReception(FALSE);}
        Result:=2;
        PolyPart:=TPolyPart.Create(AData);
        PolyPart.HideDlg:=False;
        APoly:=PCPoly(AData^.Layers^.IndexObject(AData^.PInfo,AData^.Layers^.SelLayer^.Data^.At(0)));
        PolyPart.PolyPartProps.SAreaName:='';
        PolyPart.PolyPartProps.SArea:=APoly^.Flaeche*IniFile^.AreaFact;
        PolyPart.PolyPartProps.DArea1:=0;
        PolyPart.PolyPartProps.DArea2:=APoly^.Flaeche*IniFile^.AreaFact;
        PolyPart.PolyPartProps.SLayerName:=AData^.Layers^.TopLayer^.Text^;
        PolyPart.PolyPartProps.SLayerIndex:=AData^.Layers^.TopLayer^.Index;
        PolyPart.PolyPartProps.DLayerName:=AData^.Layers^.TopLayer^.Text^;
        PolyPart.PolyPartProps.DLayerIndex:=AData^.Layers^.TopLayer^.Index;
        PolyPart.PolyPartProps.SID:=APoly^.Index;
        PolyPart.PolyPartProps.FillArea:=True;
        PolyPart.PolyPartProps.PartMode:=pmParallel;
        PolyPart.PolyPartProps.SizeMode:=smDist;
        PolyPart.SendPolyData;
        {$IFNDEF AXDLL} // <----------------- AXDLL
        PolyPartitionDlg:=TPolyPartitionDlg.Create(AData^.Parent);
        PolyPart.SetupPartDlg;
        PolyPartitionDlg.FDDEMode:=FALSE;
        DlgResult:=PolyPartitionDlg.ShowModal;
        if DlgResult = mrCancel then begin
          PolyPart.Destroy;
          AData^.SetActualMenu(mn_Select);
          {DDEHandler.EnableDDEReception(TRUE);}
          Result:=1;
        end;
        {$ENDIF} // <----------------- AXDLL
      end;
    {end
    else begin
      PolyPart.Destroy;
      DDEHandler.EnableDDEReception(TRUE);
    end;}
  end;

Function DoPartPolyFree
   (
   AData           : PProj;
   DisableDDE      : Boolean
   )
   : Integer;
  var APoly        : PCPoly;
      PolyPoint    : PDPoint;
      SendStr      : Array[0..4095] of Char;
      TempStr      : Array[0..255] of Char;
      MaxPoints    : Integer;
      DlgResult    : Integer;
  begin
    Result:=0;

    {if PolyPart.DLLOK then begin}
      if (AData^.Layers^.SelLayer^.Data^.GetCount = 1) and
         ((AData^.Layers^.SelLayer^.Data^.At(0)^.GetObjType) = ot_CPoly) then begin
        {if DisableDDE then DDEHandler.EnableDDEReception(FALSE);}
        Result:=2;
        PolyPart:=TPolyPart.Create(AData);
        PolyPart.HideDlg:=True;
        POlyPart.bDeleteSourcePoly:=True;

        AData^.SnapState:=True;
        MenuFunctions['DrawSnapModeOnOff'].Checked:=True;

        APoly:=PCPoly(AData^.Layers^.IndexObject(AData^.PInfo,AData^.Layers^.SelLayer^.Data^.At(0)));
        PolyPart.PolyPartProps.SAreaName:='';
        PolyPart.PolyPartProps.SArea:=APoly^.Flaeche*IniFile^.AreaFact;
        PolyPart.PolyPartProps.DArea1:=0;
        PolyPart.PolyPartProps.DArea2:=APoly^.Flaeche*IniFile^.AreaFact;
        PolyPart.PolyPartProps.SLayerName:=AData^.Layers^.TopLayer^.Text^;
        PolyPart.PolyPartProps.SLayerIndex:=AData^.Layers^.TopLayer^.Index;
        PolyPart.PolyPartProps.DLayerName:=AData^.Layers^.TopLayer^.Text^;
        PolyPart.PolyPartProps.DLayerIndex:=AData^.Layers^.TopLayer^.Index;
        PolyPart.PolyPartProps.SID:=APoly^.Index;
        PolyPart.PolyPartProps.FillArea:=True;
        PolyPart.PolyPartProps.PartMode:=pmFree;
        PolyPart.PolyPartProps.SizeMode:=smDist;
        PolyPart.SendPolyData;
        {$IFNDEF AXDLL} // <----------------- AXDLL
        PolyPartitionDlg:=TPolyPartitionDlg.Create(AData^.Parent);
        PolyPart.SetupPartDlg;
        PolyPartitionDlg.FDDEMode:=FALSE;
        PolyPartitionDlg.FormShow(nil);
        PolyPartitionDlg.InputButtonClick(nil);
        {$ENDIF} // <----------------- AXDLL
      end;
  end;

Procedure ShowPartDlg
   (
   AData           : PProj
   );
  var AIndex       : PIndex;
      APoly        : PView;
      TmpString    : String;
      ALayerList   : TStringList;
      DlgResult    : Integer;
  Procedure ReadLayers
     (
     Item          : PLayer
     )
     ; Far;
    begin
      if Item^.SortIndex > 0 then  ALayerList.Add(Item^.Text^);
    end;
  begin
    AData^.SetActualMenu(mn_Select);
//    PolyPartitionDlg:=TPolyPartitionDlg.Create(AData^.Parent);
    AIndex:=New(PIndex,Init(PolyPart.PolyPartProps.SID));
    APoly:=AData^.Layers^.IndexObject(AData^.PInfo,AIndex);
    if APoly <> NIL then begin
      {$IFNDEF AXDLL} // <----------------- AXDLL
      with PolyPartitionDlg do begin
        Proj:=AData;
        PropAreaName:=PolyPart.PolyPartProps.SAreaName;
        PropSLayer:=PolyPart.PolyPartProps.SLayerName;
        PropTLayer:=PolyPart.PolyPartProps.DLayerName;
        SourceArea:=PolyPart.PolyPartProps.SArea;
        TargetArea1:=PolyPart.PolyPartProps.DArea1;
        TargetArea2:=PolyPart.PolyPartProps.DArea2;
        ALayerList:=TStringList.Create;
        AData^.Layers^.LData^.ForEach(@ReadLayers);
        LayerList:=ALayerList;
        ALayerList.Destroy;
        PartDist:=PolyPart.PolyPartProps.PartDist;
        PartAngle:=PolyPart.PolyPartProps.PartAngle;
        PartArea:=PolyPart.PolyPartProps.PartArea;
        FillArea:=PolyPart.PolyPartProps.FillArea;
        PartMode:=PolyPart.PolyPartProps.PartMode;
        SizeMode:=PolyPart.PolyPartProps.SizeMode;
        FDDEMode:=PolyPart.PolyPartProps.DDEMode;
      end;
      {$ENDIF} // <----------------- AXDLL
    end;
    Dispose(AIndex,Done);
    {$IFNDEF AXDLL} // <----------------- AXDLL

    if PolyPart.HideDlg then begin
       PolyPartitionDlg.FormShow(PolyPart);
       if PolyPartitionDlg.OKButton.Enabled then begin
          PolyPartitionDlg.OKButtonClick(nil);
          DlgResult:=mrOK;
       end
       else begin
          DlgResult:=mrCancel;
       end;
       AData^.InfoPoly^.EndIt(AData^.PInfo);
       AData^.NewPoly^.EndIt(AData^.PInfo);
    end
    else begin
       DlgResult:=PolyPartitionDlg.ShowModal;
    end;
    if (DlgResult = mrOK) or (DlgResult = mrCancel) then begin
      PolyPart.Destroy;
      AData^.SetActualMenu(mn_Select);
      {$IFNDEF WMLT}
      if DlgResult = mrCancel then begin
        DDEHandler.SendString('[PPR][]');
        DDEHandler.SendString('[END][]');
      end;
      DDEHandler.EnableDDEReception(TRUE);
      {$ENDIF}
    end;
    {$ENDIF} // <----------------- AXDLL
  end;

Procedure UpdatePartDlg
   (
   AData           : PProj
   );
  var AIndex       : PIndex;
      APoly        : PView;
      TmpString    : String;
      ALayerList   : TStringList;
      DlgResult    : Integer;
  Procedure ReadLayers
     (
     Item          : PLayer
     )
     ; Far;
    begin
      if Item^.SortIndex > 0 then  ALayerList.Add(Item^.Text^);
    end;
  begin
    AData^.SetActualMenu(mn_Select);
    {$IFNDEF AXDLL} // <----------------- AXDLL
    PolyPartitionDlg.FormCreate(nil);
    {$ENDIF} // <----------------- AXDLL
    AIndex:=New(PIndex,Init(PolyPart.PolyPartProps.SID));
    APoly:=AData^.Layers^.IndexObject(AData^.PInfo,AIndex);
    if APoly <> NIL then begin
      {$IFNDEF AXDLL} // <----------------- AXDLL
      with PolyPartitionDlg do begin
        Proj:=AData;
        PropAreaName:=PolyPart.PolyPartProps.SAreaName;
        PropSLayer:=PolyPart.PolyPartProps.SLayerName;
        PropTLayer:=PolyPart.PolyPartProps.DLayerName;
        SourceArea:=PolyPart.PolyPartProps.SArea;
        TargetArea1:=PolyPart.PolyPartProps.DArea1;
        TargetArea2:=PolyPart.PolyPartProps.DArea2;
        ALayerList:=TStringList.Create;
        AData^.Layers^.LData^.ForEach(@ReadLayers);
        LayerList:=ALayerList;
        ALayerList.Destroy;
        PartDist:=PolyPart.PolyPartProps.PartDist;
        PartAngle:=PolyPart.PolyPartProps.PartAngle;
        PartArea:=PolyPart.PolyPartProps.PartArea;
        FillArea:=PolyPart.PolyPartProps.FillArea;
        PartMode:=PolyPart.PolyPartProps.PartMode;
        SizeMode:=PolyPart.PolyPartProps.SizeMode;
        FDDEMode:=PolyPart.PolyPartProps.DDEMode;
      end;
      {$ENDIF} // <----------------- AXDLL
    end;
    Dispose(AIndex,Done);
    {$IFNDEF AXDLL} // <----------------- AXDLL
    PolyPartitionDlg.FormShow(nil);
    {$ENDIF} // <----------------- AXDLL
  end;

{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}
  Procedure ProcStartPolyPartitionFromDB
   (
   AData           : PProj
   );
  var DDELine      : Array[0..512] of Char;
      DDEStrSect   : Integer;
      ResStr       : String;
      ResLInt      : Integer;
      ResReal      : Real;
      StrOK        : Boolean;
      StrToLong    : Boolean;
      MStrs        : Boolean;
      SFound       : Boolean;
      OldTop       : PLayer;
      SearchOnAllL : Boolean;
      ObjLayer     : PLayer;
      DestLayer    : PLayer;
      SItem        : PIndex;
      ExistObject  : PIndex;
      ExistPoly    : PCPoly;
      i            : Integer;
      DlgResult    : Integer;
      PartPoint1   : TDPoint;
      PartPoint2   : TDPoint;

  Procedure ReadDDELine
     (
     Item          : PChar
     ); Far;
    begin
      if StrLComp(Item,'[DPP]',5) = 0 then begin
        StrCopy(DDELine,Item);
        Item[0]:=DDEStrUsed;
      end;
    end;
  begin
    DDEHandler.DDEData^.ForEach(@ReadDDELine);
    if StrComp(DDELine,'') <> 0 then begin
      // LayerInfo
      DDEStrSect:=1;
      OldTop:=AData^.Layers^.TopLayer;
      SearchOnAllL:=True;
      if DDEHandler.FGotLInfo = 1 then begin
        StrOK:=ReadDDECommandString(DDELine,DDEStrSect,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ResStr,StrToLong,MStrs);
        if StrOK and (ResStr <> '') then begin
          ObjLayer:=AData^.Layers^.NameToLayer(ResStr);
        end;
        Inc(DDEStrSect);
      end
      else if DDEHandler.FGotLInfo = 2 then begin
        if ReadDDECommandLongInt(DDELine,DDEStrSect,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ResLInt,SFound,MStrs) then begin
          ObjLayer:=AData^.Layers^.IndexToLayer(ResLInt);
        end;
        Inc(DDEStrSect);
      end;
      // ID, search object
      if ReadDDECommandLongInt(DDELine,DDEStrSect,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ResLInt,SFound,MStrs) then begin
        SItem:=New(PIndex);
        SItem^.Index:=ResLInt;
        if not SearchOnAllL then AData^.Layers^.TopLayer:=ObjLayer;
        if (AData^.Layers^.TopLayer <> NIL)
           and (not AData^.Layers^.TopLayer^.GetState(sf_LayerOff)) then
          ExistObject:=AData^.Layers^.TopLayer^.HasObject(SItem)
        else ExistObject:=NIL;
        if (ExistObject = NIL) and SearchOnAllL then begin
          i:=0;
          while (ExistObject = NIL) and (i < AData^.Layers^.LData^.Count) do begin
            AData^.Layers^.TopLayer:=AData^.Layers^.LData^.At(i);
            if (not AData^.Layers^.TopLayer^.GetState(sf_LayerOff)) then
              ExistObject:=AData^.Layers^.TopLayer^.HasObject(SItem);
            i:=i+1;
          end;
        end;
        // if SItem <> NIL then Dispose(SItem,Done);
      end
      else ExistObject:=NIL;

      if ExistObject <> NIL then begin
        ExistPoly:=PCPoly(AData^.Layers^.IndexObject(AData^.PInfo,ExistObject));

        AData^.DeSelectAll(FALSE);

        PolyPart:=TPolyPart.Create(AData);
        {APoly:=PCPoly(AData^.Layers^.IndexObject(AData^.PInfo,AData^.Layers^.SelLayer^.Data^.At(0)));}

        Inc(DDEStrSect);
        if ReadDDECommandLongInt(DDELine,DDEStrSect,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ResLInt,SFound,MStrs) then begin
          PolyPart.HideDlg:=ResLInt >= 128;
          ResLInt:=ResLInt and $F;
          case ResLInt of
            0 : PolyPart.PolyPartProps.PartMode:=pmParallel;
            1 : PolyPart.PolyPartProps.PartMode:=pmPuffer;
            2 : PolyPart.PolyPartProps.PartMode:=pmIsland;
            3 : PolyPart.PolyPartProps.PartMode:=pmFree;
            4 : begin
                PolyPart.PolyPartProps.PartMode:=pmFree;
                PolyPart.AutoPartLine:=True;
                end;
            else PolyPart.PolyPartProps.PartMode:=pmParallel;
          end;
        end
        else PolyPart.PolyPartProps.PartMode:=pmParallel;

        if not PolyPart.AutoPartLine then begin
           AData^.Layers^.TopLayer^.Select(AData^.PInfo,ExistObject);
        end;

        Inc(DDEStrSect);
        if ReadDDECommandLongInt(DDELine,DDEStrSect,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ResLInt,SFound,MStrs) then begin
          case ResLInt of
            0 : PolyPart.PolyPartProps.SizeMode:=smDist;
            1 : PolyPart.PolyPartProps.SizeMode:=smArea;
            else PolyPart.PolyPartProps.SizeMode:=smArea;
          end;
        end
        else PolyPart.PolyPartProps.SizeMode:=smArea;

        PolyPart.PolyPartProps.SArea:=ExistPoly^.Flaeche;//*IniFile^.AreaFact;
        PolyPart.PolyPartProps.DArea1:=0;
        PolyPart.PolyPartProps.DArea2:=ExistPoly^.Flaeche;//*IniFile^.AreaFact;

        Inc(DDEStrSect);
        if ReadDDECommandReal(DDELine,DDEStrSect,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ResReal,SFound,MStrs) then begin
          PolyPart.PolyPartProps.PartArea:=ResReal;
        end
        else PolyPart.PolyPartProps.PartArea:=0;

        Inc(DDEStrSect);
        if ReadDDECommandReal(DDELine,DDEStrSect,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ResReal,SFound,MStrs) then begin
          PolyPart.PolyPartProps.PartDist:=ResReal;
        end
        else PolyPart.PolyPartProps.PartDist:=0;

        Inc(DDEStrSect);
        if ReadDDECommandReal(DDELine,DDEStrSect,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ResReal,SFound,MStrs) then begin
          PolyPart.PolyPartProps.PartAngle:=ResReal;
        end
        else PolyPart.PolyPartProps.PartAngle:=0;

        Inc(DDEStrSect);
        if ReadDDECommandLongInt(DDELine,DDEStrSect,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ResLInt,SFound,MStrs) then begin
          case ResLInt of
            0 : PolyPart.PolyPartProps.FillArea:=FALSE;
            1 : PolyPart.PolyPartProps.FillArea:=TRUE;
            else PolyPart.PolyPartProps.FillArea:=True;
          end;
        end
        else PolyPart.PolyPartProps.FillArea:=True;

        PolyPart.PolyPartProps.SLayerName:=AData^.Layers^.TopLayer^.Text^;
        PolyPart.PolyPartProps.SLayerIndex:=AData^.Layers^.TopLayer^.Index;

        Inc(DDEStrSect);
        if (DDEHandler.FGotLInfo = 0) or (DDEHandler.FGotLInfo = 1) then begin
          StrOK:=ReadDDECommandString(DDELine,DDEStrSect,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ResStr,StrToLong,MStrs);
          if StrOK and (ResStr <> '') then begin
            DestLayer:=AData^.Layers^.NameToLayer(ResStr);
            if DestLayer <> NIL then begin
              PolyPart.PolyPartProps.DLayerName:=DestLayer^.Text^;
              PolyPart.PolyPartProps.DLayerIndex:=DestLayer^.Index;
            end
            else begin
              PolyPart.PolyPartProps.DLayerName:=AData^.Layers^.TopLayer^.Text^;
              PolyPart.PolyPartProps.DLayerIndex:=AData^.Layers^.TopLayer^.Index;
            end;
          end
          else begin
            PolyPart.PolyPartProps.DLayerName:=AData^.Layers^.TopLayer^.Text^;
            PolyPart.PolyPartProps.DLayerIndex:=AData^.Layers^.TopLayer^.Index;
          end;
        end
        else if DDEHandler.FGotLInfo = 2 then begin
          if ReadDDECommandLongInt(DDELine,DDEStrSect,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ResLInt,SFound,MStrs) then begin
            DestLayer:=AData^.Layers^.IndexToLayer(ResLInt);
            if DestLayer <> NIL then begin
              PolyPart.PolyPartProps.DLayerName:=DestLayer^.Text^;
              PolyPart.PolyPartProps.DLayerIndex:=DestLayer^.Index;
            end
            else begin
              PolyPart.PolyPartProps.DLayerName:=AData^.Layers^.TopLayer^.Text^;
              PolyPart.PolyPartProps.DLayerIndex:=AData^.Layers^.TopLayer^.Index;
            end;
          end
          else begin
            PolyPart.PolyPartProps.DLayerName:=AData^.Layers^.TopLayer^.Text^;
            PolyPart.PolyPartProps.DLayerIndex:=AData^.Layers^.TopLayer^.Index;
          end;
        end;

        Inc(DDEStrSect);
        StrOK:=ReadDDECommandString(DDELine,DDEStrSect,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ResStr,StrToLong,MStrs);
        if StrOK and (ResStr <> '') then PolyPart.PolyPartProps.SAreaName:=ResStr;

        PolyPart.PolyPartProps.SID:=ExistPoly^.Index;

        PolyPart.SendPolyData;

        AData^.Layers^.TopLayer:=OldTop;

        if PolyPart.PolyPartProps.PartArea >= PolyPart.PolyPartProps.SArea then begin
           DDEHandler.SendString('[PPR][]');
           DDEHandler.SendString('[END][]');
           DDEHandler.EnableDDEReception(TRUE);
        end
        else begin
           PolyPartitionDlg:=TPolyPartitionDlg.Create(AData^.Parent);
           PolyPart.SetupPartDlg;
           PolyPartitionDlg.FDDEMode:=TRUE;

           PolyPartitionDlg.FormShow(nil);
           PolyPartitionDlg.InputButtonClick(nil);

           if (PolyPart.AutoPartLine) and (ExistPoly^.Data <> nil) and (ExistPoly^.Data^.Count > 3) then begin

              PartPoint1.Init(0,0);
              PartPoint2.Init(0,0);

              if ExistPoly.ClipRect.XSize < ExistPoly.ClipRect.YSize then begin
                 PartPoint1.X:=ExistPoly^.ClipRect.A.X;
                 PartPoint1.Y:=ExistPoly^.ClipRect.A.Y + 100;
                 PartPoint2.X:=ExistPoly^.ClipRect.B.X;
                 PartPoint2.Y:=ExistPoly^.ClipRect.A.Y - 100;
              end
              else begin
                 PartPoint1.X:=ExistPoly^.ClipRect.B.X - 100;
                 PartPoint1.Y:=ExistPoly^.ClipRect.B.Y;
                 PartPoint2.X:=ExistPoly^.ClipRect.B.X + 100;
                 PartPoint2.Y:=ExistPoly^.ClipRect.A.Y;
              end;

              AData^.MsgMouseMove(PartPoint1);
              AData^.MsgLeftClick(PartPoint1);
              AData^.MsgLeftUp(PartPoint1);

              AData^.MsgMouseMove(PartPoint2);
              AData^.MsgLeftClick(PartPoint2);
              AData^.MsgLeftUp(PartPoint2);

              AData^.MsgRightClick(PartPoint2);
              AData^.MsgRightUp(PartPoint2);

              AData^.MsgLeftClick(PartPoint2);
              AData^.MsgLeftUp(PartPoint2);

              PartPoint1.Done;
              PartPoint2.Done;
          end;
        end;

      end
      else begin
        DlgResult:=DoPartPoly(AData,FALSE);
        if (DlgResult = 0) or (DlgResult = 1) then begin
          DDEHandler.SendString('[PPR][]');
          DDEHandler.SendString('[END][]');
          DDEHandler.EnableDDEReception(TRUE)
        end;
      end;
    end
    else begin
      DDEHandler.SendString('[PPR][]');
      DDEHandler.SendString('[END][]');
      DDEHandler.EnableDDEReception(TRUE);
    end;
    DDEHandler.DeleteDDEData;
  end;
{$ENDIF} // <----------------- AXDLL
{$ENDIF}

Constructor TPolyPart.Create
   (
   AData           : PProj
   );
  {var Count        : Integer;
      Rollup       : TRollupForm;}
  begin
    Inherited Create;
    AProj:=AData;
    {if StartPolyPart then DLLOK:=TRUE
    else DLLOK:=FALSE;}
    PartLine:=NIL;
    PartLineEnds:=NIL;
    PartLineAngle:=0;
    RestLine:=New(PNewPoly,Init(0,0,FALSE));
    CutPoly:=NIL;
    FCutLineOK:=FALSE;
    FCalcOK:=FALSE;
    FCheckedMenus:=TStringList.Create;
    bDeleteSourcePoly:=False;
    AutoPartLine:=False;
    (*
    { aktuell ausgew�hlte Men�punkte wiederherstellen }
    MenuFunctions.CheckedMenuFunctions:=FCheckedMenus;
    { Rollups updaten }
    for Count:=0 to UserInterface.RollupCount-1 do begin
      Rollup:=UserInterface.Rollups[Count];
      if Rollup is TProjRollupForm then TProjRollupForm(Rollup).Project:=NIL;
    end;
    UserInterface.Update([uiMenus,uiCursor,uiStatusBar,uiLayers,uiWorkingLayer,
        uiContextToolbars]);
    *)
  end;

Destructor TPolyPart.Destroy;
  begin
    {
    FCheckedMenus.Assign(MenuFunctions.CheckedMenuFunctions);
    }
    FCheckedMenus.Free;
    if ResultPoly <> NIL then Dispose(ResultPoly,Done);
    if CutPoly <> NIL then Dispose(CutPoly,Done);
    if PartLine <> NIL then Dispose(PartLine,Done);
    if PartLineEnds <> NIL then Dispose(PartLineEnds,Done);
    if RestLine <> NIL then Dispose(RestLine,Done);
    if LastPartLine <> NIL then Dispose(LastPartLine);

    MenuFunctions['DrawSnapModeOnOff'].Checked:=FALSE;
    TMDIChild(AProj^.Parent).DrawSnapModeOnOff;
    {if DLLOK then EndPolyPart;}
    {$IFNDEF AXDLL} // <----------------- AXDLL
    if PolyPartitionDlg <> nil then begin
       PolyPartitionDlg.Destroy;
       PolyPartitionDlg:=nil;
    end;
    {$ENDIF} // <----------------- AXDLL
    Inherited Destroy;
  end;

Procedure TPolyPart.SendPolyData;
  var AIndex       : PIndex;
      APoly        : PView;
  Procedure DoAll
     (
     AItem         : PDPoint
     )
     ; Far;
    begin
      {with AItem^ do SetPolyPoints(X,Y);}
    end;
  begin
    AIndex:=New(PIndex,Init(PolyPartProps.SID));
    APoly:=AProj^.Layers^.IndexObject(AProj^.PInfo,AIndex);
    if APoly <> NIL then begin
      PCPoly(APoly)^.Data^.ForEach(@DoAll);
      PartPoly:=PCPoly(APoly);
    end;
    Dispose(AIndex,Done);
  end;

Procedure TPolyPart.SetupPartDlg;
  var AIndex       : PIndex;
      APoly        : PView;
      TmpString    : String;
      ALayerList   : TStringList;
  Procedure ReadLayers
     (
     Item          : PLayer
     )
     ; Far;
    begin
      if Item^.SortIndex > 0 then
         if not Item^.GetState(sf_Fixed) then
            ALayerList.Add(Item^.Text^);
    end;
  begin
    AIndex:=New(PIndex,Init(PolyPartProps.SID));
    APoly:=AProj^.Layers^.IndexObject(AProj^.PInfo,AIndex);
    if APoly <> NIL then begin
      {$IFNDEF AXDLL} // <----------------- AXDLL
      with PolyPartitionDlg do begin
        Proj:=AProj;
        PropAreaName:=PolyPartProps.SAreaName;
        PropSLayer:=PolyPartProps.SLayerName;
        PropTLayer:=PolyPartProps.DLayerName;
        SourceArea:=PolyPartProps.SArea;
        TargetArea1:=PolyPartProps.DArea1;
        TargetArea2:=PolyPartProps.DArea2;
        ALayerList:=TStringList.Create;
        AProj^.Layers^.LData^.ForEach(@ReadLayers);
        LayerList:=ALayerList;
        ALayerList.Destroy;
        PartDist:=PolyPart.PolyPartProps.PartDist;
        PartAngle:=PolyPart.PolyPartProps.PartAngle;
        PartArea:=PolyPart.PolyPartProps.PartArea;
        FillArea:=PolyPart.PolyPartProps.FillArea;
        PartMode:=PolyPart.PolyPartProps.PartMode;
        SizeMode:=PolyPart.PolyPartProps.SizeMode;
      end;
      {$ENDIF} // <----------------- AXDLL
    end;
    Dispose(AIndex,Done);
  end;

Function TPolyPart.ReadCutLineOK:Boolean;
  begin
    Result:=(PartLine <> NIL) and (PartLine^.Data^.Count > 0);
  end;

Function TPolyPart.ReadCalcOK:Boolean;
  begin
    Result:=(CutPoly <> NIL) and (CutPoly^.Data^.Count > 0);
  end;

Function TPolyPart.CheckPoint
   (
   APoly           : PNewPoly;
   APoint          : TDPoint;
   AFirstPoint     : Boolean
   )
   : Boolean;
  Function SearchPoint
     (
     Item          : PDPoint
     )
     : Boolean; Far;
    begin
      Result:=not Item^.IsDiff(APoint);
    end;
  begin
    Result:=FALSE;
    if AFirstPoint then Result:=PartPoly^.Data^.FirstThat(@SearchPoint) <> NIL
    else begin
      if (APoly^.Data^.Count > 0) and PDPoint(APoly^.Data^.At(0))^.IsDiff(APoint) then
        Result:=PartPoly^.Data^.FirstThat(@SearchPoint) <> NIL;
    end;
  end;

Procedure TPolyPart.CreatePartLine(APartPoly:PNewPoly;APoint:TDPoint);
  var TestPoly1    : PNewPoly;
      TestPoly2    : PNewPoly;
      CheckPoint   : PDPoint;
      Dist1        : Double;
      Dist2        : Double;
      PointCnt1    : Integer;
      PointCnt2    : Integer;
      Cnt          : Integer;
      AllCnt       : Integer;
  Function SearchPoint
     (
     Item          : PDPoint
     )
     : Boolean; Far;
    begin
      if (Item^.X = CheckPoint^.X) and (Item^.Y = CheckPoint^.Y) then Result:=TRUE
      else begin
        Result:=FALSE;
        Inc(PointCnt2);
      end;
    end;
  Procedure CopyPoints
     (
     Item          : PDPoint
     )
     ; Far;
    var ANewPoint  : TDPoint;
    begin
      ANewPoint.Init(Item^.X,Item^.Y);
      APartPoly^.PointInsert(AProj^.PInfo,ANewPoint,TRUE);
    end;
  Procedure CopyRestPoints
     (
     Item          : PDPoint
     )
     ; Far;
    var ANewPoint  : TDPoint;
    begin
      ANewPoint.Init(Item^.X,Item^.Y);
      RestLine^.PointInsert(AProj^.PInfo,ANewPoint,TRUE);
    end;
  begin
    {MessageBeep(0);}
    if APartPoly^.Data^.Count = 1 then ShortLineDir:=TRUE
    else if (APoint.X = LastPoint.X) and (APoint.Y = LastPoint.Y) then
         ShortLineDir:=not ShortLineDir;
    LastPoint.Init(APoint.X,APoint.Y);
    CheckPoint:=New(PDPoint,Init(PDPoint(APartPoly^.Data^.At(0))^.X,PDPoint(APartPoly^.Data^.At(0))^.Y));
    PointCnt2:=0;
    PartPoly^.Data^.FirstThat(@SearchPoint);
    PointCnt1:=PointCnt2;
    CheckPoint.X:=APoint.X;
    CheckPoint.Y:=APoint.Y;
    PointCnt2:=0;
    PartPoly^.Data^.FirstThat(@SearchPoint);
    TestPoly1:=New(PNewPoly,Init(0,0,FALSE));
    TestPoly2:=New(PNewPoly,Init(0,0,FALSE));
    AllCnt:=0;
    Cnt:=PointCnt1;
    repeat
      TestPoly1^.PointInsert(AProj^.PInfo,PDPoint(PartPoly^.Data^.At(Cnt))^,TRUE);
      Inc(Cnt);
      Inc(AllCnt);
      if Cnt = PartPoly^.Data^.Count then Cnt:=1;
    until (Cnt = PointCnt2+1) or (AllCnt = PartPoly^.Data^.Count);
    AllCnt:=0;
    Cnt:=PointCnt1;
    repeat
      TestPoly2^.PointInsert(AProj^.PInfo,PDPoint(PartPoly^.Data^.At(Cnt))^,TRUE);
      Dec(Cnt);
      Inc(AllCnt);
      if (Cnt = -1) and (PointCnt2 > 0)  then Cnt:=PartPoly^.Data^.Count-2;
    until (Cnt = PointCnt2-1) or (AllCnt = PartPoly^.Data^.Count);
    APartPoly^.Draw(AProj^.PInfo,TRUE);
    APartPoly^.Data^.FreeAll;
    if (TestPoly1^.Laenge < TestPoly2^.Laenge) then begin
      if ShortLineDir then begin
        TestPoly1^.Data^.ForEach(@CopyPoints);
        TestPoly2^.Data^.ForEach(@CopyRestPoints);
      end
      else begin
        TestPoly2^.Data^.ForEach(@CopyPoints);
        TestPoly1^.Data^.ForEach(@CopyRestPoints);
      end;
    end
    else begin
      if ShortLineDir then begin
        TestPoly2^.Data^.ForEach(@CopyPoints);
        TestPoly1^.Data^.ForEach(@CopyRestPoints);
      end
      else begin
        TestPoly1^.Data^.ForEach(@CopyPoints);
        TestPoly2^.Data^.ForEach(@CopyRestPoints);
      end
    end;
    Dispose(TestPoly1,Done);
    Dispose(TestPoly2,Done);
    Dispose(CheckPoint,Done);
    AProj^.PInfo^.RedrawRect(PartPoly^.ClipRect);
  end;

Procedure TPolyPart.SetPartLine(APoly:PNewPoly);
  begin
    if PartLine <> NIL then Dispose(PartLine,Done);
    PartLine:=New(PPoly,Init);
    APoly^.CheckCopyData(AProj^.PInfo,PartLine,FALSE);
  end;

Procedure TPolyPart.SetMoveDist(XM,YM:LongInt);
  var DiffX        : LongInt;
      DiffY        : LongInt;
      MoveAngle    : Extended;
  begin
    MoveDistX:=XM;
    MoveDistY:=YM;
    {PolyPartProps.PartMode:=pmParallel;}
    DiffX:=PDPoint(PartLine^.Data^.At(PartLine^.Data^.Count-1))^.X-PDPoint(PartLine^.Data^.At(0))^.X;
    DiffY:=PDPoint(PartLine^.Data^.At(PartLine^.Data^.Count-1))^.Y-PDPoint(PartLine^.Data^.At(0))^.Y;
    if Abs(DiffX) < 1E-30 then PartLineAngle:=90
    else PartLineAngle:=ArcTan(DiffY/DiffX)/Pi*180;
    if Abs(MoveDistX) < 1E-30 then MoveAngle:=90
    else MoveAngle:=ArcTan(MoveDistY/MoveDistX)/Pi*180;
    with PolyPartProps do begin
      PartDist:=Sqrt(Sqr(MoveDistX/1)+Sqr(MoveDistY/1));
      PartAngle:=-PartLineAngle+MoveAngle-90;
      if PartAngle < -90 then PartAngle:=PartAngle+180
      else if PartAngle > 90 then PartAngle:=PartAngle-180;
    end;
  end;

Procedure TPolyPart.CalcStartMovePoly;
  var DiffX        : LongInt;
      DiffY        : LongInt;
  begin
    if PartLine^.Data^.Count > 0 then begin
       DiffX:=PDPoint(PartLine^.Data^.At(PartLine^.Data^.Count-1))^.X-PDPoint(PartLine^.Data^.At(0))^.X;
       DiffY:=PDPoint(PartLine^.Data^.At(PartLine^.Data^.Count-1))^.Y-PDPoint(PartLine^.Data^.At(0))^.Y;
       if Abs(DiffX) < 1E-30 then PartLineAngle:=90
       else PartLineAngle:=ArcTan(DiffY/DiffX)/Pi*180;
     end;
  end;

Procedure TPolyPart.CalcStateMoveDist(XM,YM:LongInt);
  var DiffX        : LongInt;
      DiffY        : LongInt;
      MoveAngle    : Extended;
      StateDist    : Extended;
      StateAngle   : Extended;
      StateText    : String;
  begin
    if Abs(XM) < 1E-30 then MoveAngle:=90
    else MoveAngle:=ArcTan(YM/XM)/Pi*180;
    StateDist:=Sqrt(Sqr(XM/100)+Sqr(YM/100));
    StateAngle:=-PartLineAngle+MoveAngle-90;
    if StateAngle < -90 then StateAngle:=StateAngle+180
    else if StateAngle > 90 then StateAngle:=StateAngle-180;
    StateText:=GetLangText(11755);
    StateText:=StateText+FloatToStrF(StateDist,ffNumber,18,2);
    StateText:=StateText+GetLangText(11756);
    StateText:=StateText+GetLangText(11757);
    StateText:=StateText+FloatToStrF(StateAngle,ffNumber,18,2);
    StateText:=StateText+GetLangText(11758);
    AProj^.ClearStatusText;
    AProj^.SetStatusText(StateText,True);
  end;

Procedure TPolyPart.InsertPolyToProject
   (
   ALayer          : String
   );
  var BLayer       : PLayer;
      SendEnd      : Boolean;
      InsCount     : Integer;
  begin
    InsCount:=0;

    if ResultPoly <> NIL then begin
      if PolyPartProps.FillArea then SendEnd:=FALSE
      else SendEnd:=TRUE;
      BLayer:=AProj^.Layers^.NameToLayer(ALayer);
//      if BLayer = nil then begin
//         BLayer:=NewLayerDialog(AProj,GetLangText(10154),ALayer);
//      end;
      if BLayer <> NIL then begin
         if AProj^.InsertObjectOnLayer(ResultPoly,BLayer^.Index) then begin
            Inc(InsCount);
         end;
      end;
      {$IFNDEF AXDLL} // <----------------- AXDLL
      {$IFNDEF WMLT}
      with PolyPartProps do begin
           if WinGisMainForm.WeAreUsingTheIDB[AProj] then begin
              WingisMainForm.IDB_Man.CopyAttributeInfo (AProj,AProj^.Layers^.TopLayer,BLayer,PartPoly^.Index,ResultPoly^.Index);
           end;
           ProcSendNewPartPolyToDB(AProj,SLayerName,SLayerIndex,DLayerName,DLayerIndex,SID,ResultPoly,FALSE);
      end;
      {$ENDIF}
      {$ENDIF} // <----------------- AXDLL
      ResultPoly:=NIL;
    end;

    if (PolyPartProps.FillArea) and (PolyPartProps.SArea <> PolyPartProps.DArea1) then begin
      CalculateRestPoly(ResultPoly);

     if ResultPoly <> nil then begin

      if BLayer <> NIL then begin
         if AProj^.InsertObjectOnLayer(ResultPoly,BLayer^.Index) then begin
            Inc(InsCount);
         end;
      end;
      {$IFNDEF AXDLL} // <----------------- AXDLL
      {$IFNDEF WMLT}
      with PolyPartProps do begin
           if WinGisMainForm.WeAreUsingTheIDB[AProj] then begin
              WingisMainForm.IDB_Man.CopyAttributeInfo (AProj,AProj^.Layers^.TopLayer,BLayer,PartPoly^.Index,ResultPoly^.Index);
           end;
           ProcSendNewPartPolyToDB(AProj,SLayerName,SLayerIndex,DLayerName,DLayerIndex,SID,ResultPoly,FALSE);
      end;
      {$ENDIF}
      {$ENDIF} // <----------------- AXDLL

     end;
     {$IFNDEF AXDLL}
     if (bDeleteSourcePoly) and (InsCount >= 2) then begin
         AProj^.DeleteSelected(True);
         AProj^.PInfo^.RedrawScreen(False);
     end;
     {$ENDIF}
     ResultPoly:=NIL;
    end;
    {$IFNDEF AXDLL}
    DDEHandler.SendString('[END][]');
    {$ENDIF}
  end;

Procedure TPolyPart.DoPartition
   (
   ApMode          : Integer;
   AsMode          : Integer;
   ADist           : LongInt;
   AAngle          : Double;
   AArea           : Double
   );
  var ANewPoly     : PNewPoly;
      MoveDirection: Double;
      MainMDir     : Integer;
      APoint       : TDPoint;
      BPoint       : TDPoint;
      CAngle       : Double;
  Procedure CopyPoints
     (
     Item          : PDPoint
     )
     ; Far;
    begin
      ANewPoly^.PointInsert(AProj^.PInfo,Item^,TRUE);
    end;
  begin
    ANewPoly:=New(PNewPoly,Init(0,0,FALSE));
    PartLine^.Data^.ForEach(@CopyPoints);
    LastPartLine:=New(PPoly,Init);
    ANewPoly^.CheckCopyData(AProj^.PInfo,LastPartLine,FALSE);
    Dispose(ANewPoly,Done);

    if (MoveDistX = 0) and (MoveDistY = 0) then begin
       APoint.Init(0,0);
       BPoint.Init(0,0);
       APoint:=PartLine^.GetCenter;
       BPoint:=PartPoly^.GetCenter;
       CAngle:=(APoint.CalculateAngle(BPoint)*180)/PI;
       MoveDirection:=CAngle;
    end
    else begin
       MoveDirection:=PartLineAngle+90;
    end;

    if Abs(MoveDistX) > Abs(MoveDistY) then begin
      if (MoveDistX > 0) and (MoveDirection > 90) then MoveDirection:=MoveDirection+180;
      if (MoveDistX < 0) and (MoveDirection < 90) then MoveDirection:=MoveDirection+180;
    end
    else begin
      if (MoveDistY > 0) and (MoveDirection < 0) then MoveDirection:=MoveDirection+180;
      if (MoveDistY < 0) and (MoveDirection > 0) then MoveDirection:=MoveDirection-180;
    end;

    MoveDirection:=MoveDirection+AAngle;
    while MoveDirection < 0 do MoveDirection:=MoveDirection+360;
    while MoveDirection >=360 do MoveDirection:=MoveDirection-360;

    if MoveDirection = 0 then MainMDir:=mdRight
    else if (MoveDirection > 0) and (MoveDirection < 90) then MainMDir:=mdRightUp
    else if MoveDirection = 90 then MainMDir:=mdUp
    else if (MoveDirection > 90) and (MoveDirection < 180) then MainMDir:=mdLeftUp
    else if MoveDirection = 180 then MainMDir:=mdLeft
    else if (MoveDirection > 180) and (MoveDirection < 270) then MainMDir:=mdLeftDown
    else if MoveDirection = 270 then MainMDir:=mdDown
    else if (MoveDirection > 270) and (MoveDirection < 360) then MainMDir:=mdRightDown;
    MainMoveDir:=MainMDir;

    MoveDirection:=MoveDirection/180*Pi;

    if AsMode = smDist then CalculateCutPolyWithDist(ADist,MoveDirection,MainMDir)
    else CalculateCutPolyWithArea(AArea,MoveDirection,MainMDir);
  end;

Procedure TPolyPart.CalculateCutPolyWithDist
   (
   ADist           : LongInt;
   AAngle          : Double;
   AMainMDir       : Integer
   );
  var ARes         : PCollection;
      AErr         : Integer;
      ADrawPos     : TDPoint;
  Procedure MovePoints
     (
     Item          : PDPoint
     )
     ; Far;
    begin
      Item^.X:=Item^.X+Round(ADist*cos(AAngle));
      Item^.Y:=Item^.Y+Round(ADist*sin(AAngle));
    end;
  Procedure InsertPoly
     (
     Item          : PCPoly
     )
     ; Far;
    begin
      if ResultPoly <> NIL then Dispose(ResultPoly,Done);
      ResultPoly:=Item;
    end;
  {var i : Integer;
      pt : PDPoint;}
  begin
    LastPartLine^.Data^.ForEach(@MovePoints);
    MergeCutPolyWithClipRect(AMainMDir);
    {for i:=0 to CutPoly^.Data^.Count-1 do begin
      pt:=CutPoly^.Data^.At(i);
    end;}
    ARes:=New(PCollection,Init(3,3));
    if (PartPoly <> NIL) and (CutPoly <> NIL) then begin
      AErr:=AreaCut2Polies(AProj,PartPoly,CutPoly,TRUE,TRUE,ARes);
      if ARes^.Count > 0 then begin
        ARes^.ForEach(@InsertPoly);
        ARes^.DeleteAll;
      end;
    end;
    Dispose(ARes,Done);
    if ResultPoly <> NIL then begin
      with PolyPartProps do begin
        DArea1:=ResultPoly^.Flaeche*IniFile^.AreaFact;
        DArea2:=SArea-DArea1;
      end;
      ADrawPos.Init(0,0);
      {AProj^.TestPoly^.SetPoly(CutPoly);
      AProj^.TestPoly^.StartIt(AProj^.PInfo,ADrawPos,FALSE);}
      AProj^.InfoPoly^.SetPoly(ResultPoly);
      AProj^.InfoPoly^.StartIt(AProj^.PInfo,ADrawPos,FALSE);
      AProj^.SetActualMenu(mn_InfoPoly);
    end;
  end;

Procedure TPolyPart.CalculateCutPolyWithArea
   (
   AArea           : Double;
   AAngle          : Double;
   AMainMDir       : Integer
   );
  var LastMoveDist : LongInt;
      MaxDist      : Extended;
      PolyToSmall  : Boolean;
      ARes         : PCollection;
      AErr         : Integer;
      AktArea      : Double;
      ADrawPos     : TDPoint;
  Procedure MovePoints
     (
     Item          : PDPoint
     )
     ; Far;
    begin
      Item^.X:=Item^.X+Round(LastMoveDist*cos(AAngle));
      Item^.Y:=Item^.Y+Round(LastMoveDist*sin(AAngle));
    end;
  Procedure InsertPoly
     (
     Item          : PCPoly
     )
     ; Far;
    begin
      if ResultPoly <> NIL then Dispose(ResultPoly,Done);
      ResultPoly:=Item;
    end;
  Procedure AddArea
     (
     Item          : PCPoly
     )
     ; Far;
    begin
      AktArea:=AktArea+(Item^.Flaeche*IniFile^.AreaFact);
    end;
  begin
    PolyToSmall:=TRUE;
    MaxDist:=Sqrt(Sqr(PartPoly^.ClipRect.XSize/1)+Sqr(PartPoly^.ClipRect.YSize/1));
    LastMoveDist:=Round(MaxDist/2);
    ARes:=New(PCollection,Init(3,3));

    repeat
      AktArea:=0;
      LastPartLine^.Data^.ForEach(@MovePoints);
      MergeCutPolyWithClipRect(AMainMDir);
      if (PartPoly <> NIL) and (CutPoly <> NIL) then begin
        if ARes^.Count > 0 then ARes^.FreeAll;
        AErr:=AreaCut2Polies(AProj,PartPoly,CutPoly,TRUE,TRUE,ARes);
        if ARes^.Count > 0 then ARes^.ForEach(@AddArea);
      end;
      if AktArea = 0 then LastMoveDist:=-(LastMoveDist div 2)
      else begin
        if AktArea < AArea then begin
           LastMoveDist:=Abs(LastMoveDist div 2);
        end
        else begin
           LastMoveDist:=-(Abs(LastMoveDist div 2));
        end;
      end;
    until (AktArea = AArea) or (LastMoveDist = 0);

    if ARes^.Count > 0 then begin
      ARes^.ForEach(@InsertPoly);
      ARes^.DeleteAll;
    end;
    Dispose(ARes,Done);
    if ResultPoly <> NIL then begin
      with PolyPartProps do begin
        DArea1:=ResultPoly^.Flaeche*IniFile^.AreaFact;
        DArea2:=SArea-DArea1;
      end;
      ADrawPos.Init(0,0);
      AProj^.InfoPoly^.SetPoly(ResultPoly);
      AProj^.InfoPoly^.StartIt(AProj^.PInfo,ADrawPos,FALSE);
      {AProj^.TestPoly^.SetPoly(CutPoly);
      AProj^.TestPoly^.StartIt(AProj^.PInfo,ADrawPos,FALSE);}
      AProj^.SetActualMenu(mn_InfoPoly);
    end;
  end;

Procedure TPolyPart.CalculateRestPoly
   (
   FirstPoly       : PCPoly
   );
  var ARes         : PCollection;
      AErr         : Integer;
  Procedure InsertPoly
     (
     Item          : PCPoly
     )
     ; Far;
    begin
      if ResultPoly <> NIL then Dispose(ResultPoly,Done);
      ResultPoly:=Item;
    end;
  begin
    ARes:=New(PCollection,Init(3,3));
    if (PartPoly <> NIL) and (CutPoly <> NIL) then begin
      AErr:=AreaCut2Polies(AProj,PartPoly,CutPoly,FALSE,TRUE,ARes);
      if ARes^.Count > 0 then begin
        ARes^.ForEach(@InsertPoly);
        ARes^.DeleteAll;
      end;
    end;
    Dispose(ARes,Done);
  end;

Procedure TPolyPart.MergeCutPolyWithClipRect
   (
   AMainMDir       : Integer
   );
  var Line1P1      : TDPoint;
      Line1P2      : TDPoint;
      Line2P1      : TDPoint;
      Line2P2      : TDPoint;
      CutPoint1    : TDPoint;
      CutPoint2    : TDPoint;
      CutSide1     : Integer;
      CutSide2     : Integer;
      CutIndex1    : Integer;
      CutIndex2    : Integer;
      Cnt          : Integer;
      TmpPoint     : TDPoint;
      CutInside    : Boolean;
      SearchCut    : Boolean;

      i            : Integer;
      AClipRect    : TDRect;
      p1,p2        : PDPoint;
      idx1,idx2    : Integer;
  begin
    //

    if CutPoly <> NIL then Dispose(CutPoly,Done);
    CutPoly:=New(PCPoly,Init);

    if (bDeleteSourcePoly) then begin
       p1:=PartLine^.Data^.At(0);
       p2:=PartLine^.Data^.At(PartLine^.Data^.Count-1);

       idx1:=PartPoly^.FindIndexOfPoint(p1^);
       idx2:=PartPoly^.FindIndexOfPoint(p2^);

       if (PartLine^.Data^.Count > 1) and (idx2 = -1) then begin
          p2:=PartLine^.Data^.At(PartLine^.Data^.Count-2);
          idx2:=PartPoly^.FindIndexOfPoint(p2^);
       end;

       if (idx1 <> -1) and (idx2 <> -1) then begin

          for i:=0 to PartLine^.Data^.Count-1 do begin
              CutPoly^.InsertPoint(PartLine^.Data^.At(i)^);
          end;

          if idx1 < idx2 then begin
             for i:=idx2-1 downto idx1+1 do begin
                 CutPoly^.InsertPoint(PartPoly^.Data^.At(i)^);
             end;
          end
          else if idx1 > idx2 then begin
             for i:=idx2+1 to idx1-1 do begin
                 CutPoly^.InsertPoint(PartPoly^.Data^.At(i)^);
             end;
          end;

          CutPoly^.ClosePoly;
          exit;
       end;
    end;


    AClipRect.InitByRect(PartPoly^.ClipRect);
    //AClipRect.InitByRect(PartLine^.ClipRect);

    Cnt:=0;

    while not AClipRect.PointInside(PDPoint(LastPartLine^.Data^.At(Cnt))^) and
       (Cnt < LastPartLine^.Data^.Count-1) do Inc(Cnt);


    CutIndex1:=Cnt;
    if Cnt = 0 then begin
      Line2P1.Init(PDPoint(LastPartLine^.Data^.At(Cnt))^.X,PDPoint(LastPartLine^.Data^.At(Cnt))^.Y);
      Line2P2.Init(PDPoint(LastPartLine^.Data^.At(Cnt+1))^.X,PDPoint(LastPartLine^.Data^.At(Cnt+1))^.Y);
      CutInside:=FALSE;
    end
    else begin
      Line2P1.Init(PDPoint(LastPartLine^.Data^.At(Cnt-1))^.X,PDPoint(LastPartLine^.Data^.At(Cnt-1))^.Y);
      Line2P2.Init(PDPoint(LastPartLine^.Data^.At(Cnt))^.X,PDPoint(LastPartLine^.Data^.At(Cnt))^.Y);
      CutInside:=TRUE;
    end;
    if Line2P1.X <> Line2P2.X then begin
      SearchCut:=TRUE;
      if Line2P1.X < Line2P2.X then begin
        Line1P1.Init(AClipRect.A.X,AClipRect.A.Y);
        Line1P2.Init(AClipRect.A.X,AClipRect.B.Y);
        CutSide1:=csLeft;
      end
      else begin
        Line1P1.Init(AClipRect.B.X,AClipRect.A.Y);
        Line1P2.Init(AClipRect.B.X,AClipRect.B.Y);
        CutSide1:=csRight;
      end;
    end;
    if not SearchCut or
       not AProj^.PInfo^.LineCut(CutInside,Line1P1,Line1P2,Line2P1,Line2P2,CutPoint1) then begin
      if Line2P1.Y <> Line2P2.Y then begin
        if Line2P1.Y < Line2P2.Y then begin
          Line1P1.Init(AClipRect.A.X,AClipRect.A.Y);
          Line1P2.Init(AClipRect.B.X,AClipRect.A.Y);
          CutSide1:=csBottom;
        end
        else begin
          Line1P1.Init(AClipRect.A.X,AClipRect.B.Y);
          Line1P2.Init(AClipRect.B.X,AClipRect.B.Y);
          CutSide1:=csTop;
        end;
      end;
      AProj^.PInfo^.LineCut(CutInside,Line1P1,Line1P2,Line2P1,Line2P2,CutPoint1);
    end;

    Cnt:=LastPartLine^.Data^.Count-1;

    while not AClipRect.PointInside(PDPoint(LastPartLine^.Data^.At(Cnt))^) and
       (Cnt > 0) do Dec(Cnt);

    CutIndex2:=Cnt;
    if Cnt = LastPartLine^.Data^.Count-1 then begin
      Line2P1.Init(PDPoint(LastPartLine^.Data^.At(Cnt))^.X,PDPoint(LastPartLine^.Data^.At(Cnt))^.Y);
      Line2P2.Init(PDPoint(LastPartLine^.Data^.At(Cnt-1))^.X,PDPoint(LastPartLine^.Data^.At(Cnt-1))^.Y);
      CutInside:=FALSE;
    end
    else begin
      Line2P1.Init(PDPoint(LastPartLine^.Data^.At(Cnt+1))^.X,PDPoint(LastPartLine^.Data^.At(Cnt+1))^.Y);
      Line2P2.Init(PDPoint(LastPartLine^.Data^.At(Cnt))^.X,PDPoint(LastPartLine^.Data^.At(Cnt))^.Y);
      CutInside:=TRUE;
    end;
    if Line2P1.X <> Line2P2.X then begin
      SearchCut:=TRUE;
      if Line2P1.X < Line2P2.X then begin
        Line1P1.Init(AClipRect.A.X,AClipRect.A.Y);
        Line1P2.Init(AClipRect.A.X,AClipRect.B.Y);
        CutSide2:=csLeft;
      end
      else begin
        Line1P1.Init(AClipRect.B.X,AClipRect.A.Y);
        Line1P2.Init(AClipRect.B.X,AClipRect.B.Y);
        CutSide2:=csRight;
      end;
    end;
    if not SearchCut or
       not AProj^.PInfo^.LineCut(CutInside,Line1P1,Line1P2,Line2P1,Line2P2,CutPoint2) then begin
      if Line2P1.Y <> Line2P2.Y then begin
        if Line2P1.Y < Line2P2.Y then begin
          Line1P1.Init(AClipRect.A.X,AClipRect.A.Y);
          Line1P2.Init(AClipRect.B.X,AClipRect.A.Y);
          CutSide2:=csBottom;
        end
        else begin
          Line1P1.Init(AClipRect.A.X,AClipRect.B.Y);
          Line1P2.Init(AClipRect.B.X,AClipRect.B.Y);
          CutSide2:=csTop;
        end;
      end;
      AProj^.PInfo^.LineCut(CutInside,Line1P1,Line1P2,Line2P1,Line2P2,CutPoint2);
    end;

    CutPoly^.InsertPoint(CutPoint1);
    for Cnt:=CutIndex1 to CutIndex2 do CutPoly^.InsertPoint(PDPoint(LastPartLine^.Data^.At(Cnt))^);
    CutPoly^.InsertPoint(CutPoint2);

    {for i:=0 to LastPartLine^.Data^.Count-1 do begin
      pt:=LastPartLine^.Data^.At(i);
    end;}

    {for i:=0 to CutPoly^.Data^.Count-1 do begin
      pt:=CutPoly^.Data^.At(i);
    end;}

    if CutSide1 = CutSide2 then begin
      if CutSide1 = csLeft then begin
        case AMainMDir of
          mdRight,
          mdRightUp,
          mdRightDown : CutPoly^.ClosePoly;
          else begin
                 if CutPoint2.Y > CutPoint1.Y then begin
                   TmpPoint.Init(AClipRect.A.X,AClipRect.B.Y);
                   CutPoly^.InsertPoint(TmpPoint);
                   TmpPoint.Init(AClipRect.B.X,AClipRect.B.Y);
                   CutPoly^.InsertPoint(TmpPoint);
                   TmpPoint.Init(AClipRect.B.X,AClipRect.A.Y);
                   CutPoly^.InsertPoint(TmpPoint);
                   TmpPoint.Init(AClipRect.A.X,AClipRect.A.Y);
                   CutPoly^.InsertPoint(TmpPoint);
                 end
                 else begin
                   TmpPoint.Init(AClipRect.A.X,AClipRect.A.Y);
                   CutPoly^.InsertPoint(TmpPoint);
                   TmpPoint.Init(AClipRect.B.X,AClipRect.A.Y);
                   CutPoly^.InsertPoint(TmpPoint);
                   TmpPoint.Init(AClipRect.B.X,AClipRect.B.Y);
                   CutPoly^.InsertPoint(TmpPoint);
                   TmpPoint.Init(AClipRect.A.X,AClipRect.B.Y);
                   CutPoly^.InsertPoint(TmpPoint);
                 end;
                 CutPoly^.ClosePoly;
               end;
        end;
      end
      else if CutSide1 = csRight then begin
        case AMainMDir of
          mdLeft,
          mdLeftUp,
          mdLeftDown  : CutPoly^.ClosePoly;
          else begin
                 if CutPoint2.Y > CutPoint1.Y then begin
                   TmpPoint.Init(AClipRect.B.X,AClipRect.B.Y);
                   CutPoly^.InsertPoint(TmpPoint);
                   TmpPoint.Init(AClipRect.A.X,AClipRect.B.Y);
                   CutPoly^.InsertPoint(TmpPoint);
                   TmpPoint.Init(AClipRect.A.X,AClipRect.A.Y);
                   CutPoly^.InsertPoint(TmpPoint);
                   TmpPoint.Init(AClipRect.B.X,AClipRect.A.Y);
                   CutPoly^.InsertPoint(TmpPoint);
                 end
                 else begin
                   TmpPoint.Init(AClipRect.B.X,AClipRect.A.Y);
                   CutPoly^.InsertPoint(TmpPoint);
                   TmpPoint.Init(AClipRect.A.X,AClipRect.A.Y);
                   CutPoly^.InsertPoint(TmpPoint);
                   TmpPoint.Init(AClipRect.A.X,AClipRect.B.Y);
                   CutPoly^.InsertPoint(TmpPoint);
                   TmpPoint.Init(AClipRect.B.X,AClipRect.B.Y);
                   CutPoly^.InsertPoint(TmpPoint);
                 end;
                 CutPoly^.ClosePoly;
               end;
        end;
      end
      else if CutSide1 = csBottom then begin
        case AMainMDir of
          mdUp,
          mdLeftUp,
          mdRightUp   : CutPoly^.ClosePoly;
          else begin
                 if CutPoint2.X > CutPoint1.X then begin
                   TmpPoint.Init(AClipRect.B.X,AClipRect.A.Y);
                   CutPoly^.InsertPoint(TmpPoint);
                   TmpPoint.Init(AClipRect.B.X,AClipRect.B.Y);
                   CutPoly^.InsertPoint(TmpPoint);
                   TmpPoint.Init(AClipRect.A.X,AClipRect.B.Y);
                   CutPoly^.InsertPoint(TmpPoint);
                   TmpPoint.Init(AClipRect.A.X,AClipRect.A.Y);
                   CutPoly^.InsertPoint(TmpPoint);
                 end
                 else begin
                   TmpPoint.Init(AClipRect.A.X,AClipRect.A.Y);
                   CutPoly^.InsertPoint(TmpPoint);
                   TmpPoint.Init(AClipRect.A.X,AClipRect.B.Y);
                   CutPoly^.InsertPoint(TmpPoint);
                   TmpPoint.Init(AClipRect.B.X,AClipRect.B.Y);
                   CutPoly^.InsertPoint(TmpPoint);
                   TmpPoint.Init(AClipRect.B.X,AClipRect.A.Y);
                   CutPoly^.InsertPoint(TmpPoint);
                 end;
                 CutPoly^.ClosePoly;
               end;
        end;
      end
      else if CutSide1 = csTop then begin
        case AMainMDir of
          mdDown,
          mdLeftDown,
          mdRightDown : CutPoly^.ClosePoly;
          else begin
                 if CutPoint2.X > CutPoint1.X then begin
                   TmpPoint.Init(AClipRect.B.X,AClipRect.B.Y);
                   CutPoly^.InsertPoint(TmpPoint);
                   TmpPoint.Init(AClipRect.B.X,AClipRect.A.Y);
                   CutPoly^.InsertPoint(TmpPoint);
                   TmpPoint.Init(AClipRect.A.X,AClipRect.A.Y);
                   CutPoly^.InsertPoint(TmpPoint);
                   TmpPoint.Init(AClipRect.A.X,AClipRect.B.Y);
                   CutPoly^.InsertPoint(TmpPoint);
                 end
                 else begin
                   TmpPoint.Init(AClipRect.A.X,AClipRect.B.Y);
                   CutPoly^.InsertPoint(TmpPoint);
                   TmpPoint.Init(AClipRect.A.X,AClipRect.A.Y);
                   CutPoly^.InsertPoint(TmpPoint);
                   TmpPoint.Init(AClipRect.B.X,AClipRect.A.Y);
                   CutPoly^.InsertPoint(TmpPoint);
                   TmpPoint.Init(AClipRect.B.X,AClipRect.B.Y);
                   CutPoly^.InsertPoint(TmpPoint);
                 end;
                 CutPoly^.ClosePoly;
               end;
        end;
      end;
    end
    else if CutSide1 = csLeft then begin
      if CutSide2 = csBottom then begin
        case AMainMDir of
          mdLeft,
          mdLeftDown,
          mdDown      : begin
                          TmpPoint.Init(AClipRect.B.X,AClipRect.A.Y);
                          CutPoly^.InsertPoint(TmpPoint);
                          TmpPoint.Init(AClipRect.B.X,AClipRect.B.Y);
                          CutPoly^.InsertPoint(TmpPoint);
                          TmpPoint.Init(AClipRect.A.X,AClipRect.B.Y);
                          CutPoly^.InsertPoint(TmpPoint);
                        end;
          else begin
                 TmpPoint.Init(AClipRect.A.X,AClipRect.A.Y);
                 CutPoly^.InsertPoint(TmpPoint);
               end;
        end;
        CutPoly^.ClosePoly;
      end
      else if CutSide2 = csTop then begin
        case AMainMDir of
          mdLeft,
          mdLeftUp,
          mdUp        : begin
                          TmpPoint.Init(AClipRect.B.X,AClipRect.B.Y);
                          CutPoly^.InsertPoint(TmpPoint);
                          TmpPoint.Init(AClipRect.B.X,AClipRect.A.Y);
                          CutPoly^.InsertPoint(TmpPoint);
                          TmpPoint.Init(AClipRect.A.X,AClipRect.A.Y);
                          CutPoly^.InsertPoint(TmpPoint);
                        end;
          else begin
                 TmpPoint.Init(AClipRect.A.X,AClipRect.B.Y);
                 CutPoly^.InsertPoint(TmpPoint);
               end;
        end;
        CutPoly^.ClosePoly;
      end
      else if CutSide2 = csRight then begin
        case AMainMDir of
          mdLeftUp,
          mdUp,
          mdRightUp,
          mdRight     : begin
                          TmpPoint.Init(AClipRect.B.X,AClipRect.A.Y);
                          CutPoly^.InsertPoint(TmpPoint);
                          TmpPoint.Init(AClipRect.A.X,AClipRect.A.Y);
                          CutPoly^.InsertPoint(TmpPoint);
                        end;
          else begin
                 TmpPoint.Init(AClipRect.B.X,AClipRect.B.Y);
                 CutPoly^.InsertPoint(TmpPoint);
                 TmpPoint.Init(AClipRect.A.X,AClipRect.B.Y);
                 CutPoly^.InsertPoint(TmpPoint);
               end;
        end;
        CutPoly^.ClosePoly;
      end;
    end
    else if CutSide1 = csRight then begin
      if CutSide2 = csBottom then begin
        case AMainMDir of
          mdRight,
          mdRightDown,
          mdDown      : begin
                          TmpPoint.Init(AClipRect.A.X,AClipRect.A.Y);
                          CutPoly^.InsertPoint(TmpPoint);
                          TmpPoint.Init(AClipRect.A.X,AClipRect.B.Y);
                          CutPoly^.InsertPoint(TmpPoint);
                          TmpPoint.Init(AClipRect.B.X,AClipRect.B.Y);
                          CutPoly^.InsertPoint(TmpPoint);
                        end;
          else begin
                 TmpPoint.Init(AClipRect.B.X,AClipRect.A.Y);
                 CutPoly^.InsertPoint(TmpPoint);
               end;
        end;
        CutPoly^.ClosePoly;
      end
      else if CutSide2 = csTop then begin
        case AMainMDir of
          mdRight,
          mdRightUp,
          mdUp        : begin
                          TmpPoint.Init(AClipRect.A.X,AClipRect.B.Y);
                          CutPoly^.InsertPoint(TmpPoint);
                          TmpPoint.Init(AClipRect.A.X,AClipRect.A.Y);
                          CutPoly^.InsertPoint(TmpPoint);
                          TmpPoint.Init(AClipRect.B.X,AClipRect.A.Y);
                          CutPoly^.InsertPoint(TmpPoint);
                        end;
          else begin
                 TmpPoint.Init(AClipRect.B.X,AClipRect.B.Y);
                 CutPoly^.InsertPoint(TmpPoint);
               end;
        end;
        CutPoly^.ClosePoly;
      end
      else if CutSide2 = csLeft then begin
        case AMainMDir of
          mdRightDown,
          mdDown,
          mdLeftDown,
          mdLeft      : begin
                          TmpPoint.Init(AClipRect.A.X,AClipRect.B.Y);
                          CutPoly^.InsertPoint(TmpPoint);
                          TmpPoint.Init(AClipRect.B.X,AClipRect.B.Y);
                          CutPoly^.InsertPoint(TmpPoint);
                        end;
          else begin
                 TmpPoint.Init(AClipRect.A.X,AClipRect.A.Y);
                 CutPoly^.InsertPoint(TmpPoint);
                 TmpPoint.Init(AClipRect.B.X,AClipRect.A.Y);
                 CutPoly^.InsertPoint(TmpPoint);
               end;
        end;
        CutPoly^.ClosePoly;
      end;
    end
    else if CutSide1 = csBottom then begin
      if CutSide2 = csLeft then begin
        case AMainMDir of
          mdLeft,
          mdLeftDown,
          mdDown      : begin
                          TmpPoint.Init(AClipRect.A.X,AClipRect.B.Y);
                          CutPoly^.InsertPoint(TmpPoint);
                          TmpPoint.Init(AClipRect.B.X,AClipRect.B.Y);
                          CutPoly^.InsertPoint(TmpPoint);
                          TmpPoint.Init(AClipRect.B.X,AClipRect.A.Y);
                          CutPoly^.InsertPoint(TmpPoint);
                        end;
          else begin
                 TmpPoint.Init(AClipRect.A.X,AClipRect.A.Y);
                 CutPoly^.InsertPoint(TmpPoint);
               end;
        end;
        CutPoly^.ClosePoly;
      end
      else if CutSide2 = csRight then begin
        case AMainMDir of
          mdRight,
          mdRightDown,
          mdDown      : begin
                          TmpPoint.Init(AClipRect.B.X,AClipRect.B.Y);
                          CutPoly^.InsertPoint(TmpPoint);
                          TmpPoint.Init(AClipRect.A.X,AClipRect.B.Y);
                          CutPoly^.InsertPoint(TmpPoint);
                          TmpPoint.Init(AClipRect.A.X,AClipRect.A.Y);
                          CutPoly^.InsertPoint(TmpPoint);
                        end;
          else begin
                 TmpPoint.Init(AClipRect.B.X,AClipRect.A.Y);
                 CutPoly^.InsertPoint(TmpPoint);
               end;
        end;
        CutPoly^.ClosePoly;
      end
      else if CutSide2 = csTop then begin
        case AMainMDir of
          mdLeftDown,
          mdLeft,
          mdLeftUp,
          mdUp        : begin
                          TmpPoint.Init(AClipRect.B.X,AClipRect.B.Y);
                          CutPoly^.InsertPoint(TmpPoint);
                          TmpPoint.Init(AClipRect.B.X,AClipRect.A.Y);
                          CutPoly^.InsertPoint(TmpPoint);
                        end;
          else begin
                 TmpPoint.Init(AClipRect.A.X,AClipRect.B.Y);
                 CutPoly^.InsertPoint(TmpPoint);
                 TmpPoint.Init(AClipRect.A.X,AClipRect.A.Y);
                 CutPoly^.InsertPoint(TmpPoint);
               end;
        end;
        CutPoly^.ClosePoly;
      end;
    end
    else if CutSide1 = csTop then begin
      if CutSide2 = csLeft then begin
        case AMainMDir of
          mdLeft,
          mdLeftUp,
          mdUp        : begin
                          TmpPoint.Init(AClipRect.A.X,AClipRect.A.Y);
                          CutPoly^.InsertPoint(TmpPoint);
                          TmpPoint.Init(AClipRect.B.X,AClipRect.A.Y);
                          CutPoly^.InsertPoint(TmpPoint);
                          TmpPoint.Init(AClipRect.B.X,AClipRect.B.Y);
                          CutPoly^.InsertPoint(TmpPoint);
                        end;
          else begin
                 TmpPoint.Init(AClipRect.A.X,AClipRect.B.Y);
                 CutPoly^.InsertPoint(TmpPoint);
               end;
        end;
        CutPoly^.ClosePoly;
      end
      else if CutSide2 = csRight then begin
        case AMainMDir of
          mdRight,
          mdRightUp,
          mdUp        : begin
                          TmpPoint.Init(AClipRect.B.X,AClipRect.A.Y);
                          CutPoly^.InsertPoint(TmpPoint);
                          TmpPoint.Init(AClipRect.A.X,AClipRect.A.Y);
                          CutPoly^.InsertPoint(TmpPoint);
                          TmpPoint.Init(AClipRect.A.X,AClipRect.B.Y);
                          CutPoly^.InsertPoint(TmpPoint);
                        end;
          else begin
                 TmpPoint.Init(AClipRect.B.X,AClipRect.B.Y);
                 CutPoly^.InsertPoint(TmpPoint);
               end;
        end;
        CutPoly^.ClosePoly;
      end
      else if CutSide2 = csBottom then begin
        case AMainMDir of
          mdRightUp,
          mdRight,
          mdRightDown,
          mdDown      : begin
                          TmpPoint.Init(AClipRect.A.X,AClipRect.A.Y);
                          CutPoly^.InsertPoint(TmpPoint);
                          TmpPoint.Init(AClipRect.A.X,AClipRect.B.Y);
                          CutPoly^.InsertPoint(TmpPoint);
                        end;
          else begin
                 TmpPoint.Init(AClipRect.B.X,AClipRect.A.Y);
                 CutPoly^.InsertPoint(TmpPoint);
                 TmpPoint.Init(AClipRect.B.X,AClipRect.B.Y);
                 CutPoly^.InsertPoint(TmpPoint);
               end;
        end;
        CutPoly^.ClosePoly;
      end;
    end;
  end;
end.
