{********************************************************************************}
{ Unit AM_SelDB                                                                  }
{--------------------------------------------------------------------------------}
{ Methoden f�r Selektion und Nachbarsuche aus der Datenbank                      }
{--------------------------------------------------------------------------------}
{ Modifikationen                                                                 }
{  23.04.1999 Heinz Ofner                                                        }
{********************************************************************************}
Unit AM_SelDB;

Interface
{$IFNDEF AXDLL} // <----------------- AXDLL
Uses Messages,Objects,WinTypes,WinProcs,SysUtils,AM_Def,AM_Layer,AM_Index,AM_Paint,
     AM_Coll,AM_Poly;

Const SelTypeNoType          =     0;
      SelTypePoint           =     1;
      SelTypeArea            =     2;
      SelTypeCircle          =     3;
      SelTypeRect            =     4;
      SelTypePoly            =     5;
      SelTypeOffset          =     6;
      SelTypeWithArea        =     7;
      DrawSelNoRange         =     0;
      DrawSelDrawRange       =     1;
      DrawSelSelRange        =     2;
      DrawSelGetRange        =     3;
      DrawSelGetObjOnLayer   =     4;
      DrawSelGetObjAllLayers =     5;
      SelFirstObjectNo       = FALSE;
      SelFirstObjectYes      =  TRUE;
      SelObjTypeAllObjects   = 28647;
      SelObjTypePoints       =     1;
      SelObjTypePolies       =     2;
      SelObjTypeAreas        =     4;
      SelObjTypeCircles      =     8;
      SelObjTypeArcs         =    16;
      SelObjTypeTexts        =    32;
      SelObjTypeSymbols      =    64;
      SelObjTypePictures     =   128;
      SelObjTypeSplines      =   256;
      SelObjTypeAnnotations  =   512;
      SelObjTypeCharts       =  1024;
      SelObjTypeMeasures     =  2048;
      CutInsideCut           = FALSE;
      CutInsideInside        =  TRUE;
      EdgesRounded           =     1;
      EdgesAngular           =     2;
      LineEndsNoEndsOneSide  =     1;
      LineEndsNoEndsTwoSides =     2;
      LineEndsStraigthNoDist =     3;
      LineEndsStraigthDist   =     4;
      LineEndsRounded        =     5;
      DrawSelLineNo          = FALSE;
      DrawSelLineYes         =  TRUE;
      ZoomNo                 = FALSE;
      ZoomYes                =  TRUE;
      SendIDsNo              = FALSE;
      SendIDsYes             =  TRUE;
      DeselectNo             = FALSE;
      DeselectYes            =  TRUE;
      SelAddSubAdd           =     0;
      SelAddSubSub           =     1;
      SelAddSubChange        =     2;

Type PDBSelection       = ^TDBSelection;
     TDBSelection       = Object(TOldObject)
       AData            : Pointer;
       SType            : Word;
       SDrawSel         : Word;
       SObjLayer        : PLayer;
       SSelFirstObj     : Boolean;
       SSelObjLayer     : PCollection;
       SSelBorder       : PBigUStrColl;
       SSelObjType      : TObjectTypes;
       SOffset          : LongInt;
       SDrawSelLine     : Boolean;
       SSelLineLayer    : PLayer;
       SZoom            : Boolean;
       SSendIDs         : Boolean;
       OldTSLayers      : PCollection;
       OldMen           : Word;
       OldTransp        : Boolean;
       OldSelState      : TSelectionMode;
       SelItem          : PIndex;
       SelOffsetLine    : PPoly;
       OffsetPoly       : PPoly;
       OffsetPoly1      : PPoly;
       CircleCenter     : TDPoint;
       CircleRadius     : LongInt;
       Selecting        : Boolean;
       ResetTM          : Boolean;
       ResetL           : Boolean;
       CorrectDir       : Boolean;
       EndingSelection  : Boolean;
       Constructor Init(AProj:Pointer);
       Destructor  Done; virtual;
       Procedure   ClearSSelBorder;
       Procedure   ClearSSelObjLayer;
       Procedure   ClearOldTSLayers;
       Function    CreateSelCircle:Boolean;
       Function    CreateSelPoly(MinCoords,MaxCoords:Integer):Boolean;
       Procedure   ContinueDBSelection;
       Procedure   EndDBSelection(SendErr,SendIDs:Boolean);
       Procedure   SaveOffsetPoly(PolyNr:Integer);
       Procedure   SetOffset(AOffset:LongInt);
       Procedure   StartDBSelection;
     end;
{$ENDIF} // <----------------- AXDLL
Implementation
{$IFNDEF AXDLL} // <----------------- AXDLL

Uses AM_View,AM_Obj,AM_Admin,AM_Dlg1,{AM_Dlg4,}AM_Pass,AM_Gener,AM_Selec,
     AM_Offse,AM_Sight,AM_Point,AM_CPoly,AM_Circl,AM_Sym,AM_Group,
     {$IFNDEF WMLT}
     AM_DDE,
     {$ENDIF}
     AM_Ini,AM_Proj,AM_ProjO,AM_ProjP,Islands,UserIntf,DDEDef;

Constructor TDBSelection.Init
   (
   AProj           : Pointer
   );
  begin
    inherited Init;
    AData:=AProj;
    SSelObjLayer:=New(PCollection,Init(3,3));
    SSelBorder:=New(PBigUStrColl,Init);
    OldTSLayers:=New(PCollection,Init(5,5));
    SType:=SelTypeNoType;
    SDrawSel:=DrawSelNoRange;
    SSelObjType:=ot_Any;
    SOffset:=0;
    SDrawSelLine:=DrawSelLineNo;
    SSelLineLayer:=NIL;
    SZoom:=ZoomNo;
    SSendIDs:=SendIDsNo;
    OldMen:=mn_Select;
    OldTransp:=FALSE;
    OldSelState:=smAdditive;
    SelItem:=NIL;
    OffsetPoly:=NIL;
    OffsetPoly1:=NIL;
    CircleCenter.Init(0,0);
    CircleRadius:=0;
    Selecting:=FALSE;
    ResetTM:=FALSE;
    ResetL:=FALSE;
    EndingSelection:=FALSE;
  end;

Destructor TDBSelection.Done;
  begin
    ClearOldTSLayers;
    Dispose(OldTSLayers,Done);
    ClearSSelBorder;
    Dispose(SSelBorder,Done);
    ClearSSelObjLayer;
    Dispose(SSelObjLayer,Done);
    inherited Done;
  end;

Procedure TDBSelection.ClearSSelObjLayer;
  begin
    SSelObjLayer^.DeleteAll;
  end;

Procedure TDBSelection.ClearSSelBorder;
  Procedure DelCoords
     (
     Item          : PChar
     ); Far;
    begin
      StrDispose(Item);
    end;
  begin
    SSelBorder^.ForEach(@DelCoords);
    SSelBorder^.DeleteAll;
  end;

Procedure TDBSelection.ClearOldTSLayers;
  begin
    OldTSLayers^.DeleteAll;
  end;

Procedure TDBSelection.SaveOffsetPoly
   (
   PolyNr          : Integer
   );
  var Proj         : PProj;
  begin
    Proj:=(PProj(AData));
    if PolyNr = 0 then begin
      if OffsetPoly <> NIL then Dispose(OffsetPoly,Done);
      OffsetPoly:=NIL;
      if (Proj^.SelectOffsetData.OffsetType = otSingle) or
         (Proj^.SelectOffsetData.OffsetType = otDouble) then OffsetPoly:=New(PCPoly,Init)
      else OffsetPoly:=New(PPoly,Init);
      Proj^.NewPoly^.GetPoly(Proj^.PInfo,OffsetPoly,FALSE)
    end
    else if PolyNr = 1 then begin
      if OffsetPoly1 <> NIL then Dispose(OffsetPoly1,Done);
      OffsetPoly1:=NIL;
      if (Proj^.SelectOffsetData.OffsetType = otSingle) or
         (Proj^.SelectOffsetData.OffsetType = otDouble) then OffsetPoly1:=New(PCPoly,Init)
      else OffsetPoly1:=New(PPoly,Init);
      Proj^.NewPoly1^.GetPoly(Proj^.PInfo,OffsetPoly1,FALSE)
    end;
  end;

Procedure TDBSelection.SetOffset
   (
   AOffset         : LongInt
   );
  begin
    SOffset:=AOffset;
  end;

Function TDBSelection.CreateSelCircle
   : Boolean;
  var TmpVal       : LongInt;
      Error        : Integer;
  begin
    if SSelBorder^.GetCount >= 3 then begin
      Val(SSelBorder^.At(0),TmpVal,Error);
      CircleCenter.X:=TmpVal;
      Val(SSelBorder^.At(1),TmpVal,Error);
      CircleCenter.Y:=TmpVal;
      Val(SSelBorder^.At(2),TmpVal,Error);
      CircleRadius:=TmpVal;
      Result:=TRUE;
    end
    else Result:=FALSE;
  end;

Function TDBSelection.CreateSelPoly
   (
   MinCoords       : Integer;
   MaxCoords       : Integer
   )
   : Boolean;
  var Proj         : PProj;
      IsXCoord     : Boolean;
      NewPoint     : TDPoint;
  Procedure CreatePoints
     (
     Item          : PChar
     ); Far;
    var TmpVal     : LongInt;
        Error      : Integer;
    begin
      Val(Item,TmpVal,Error);
      if Error = 0 then begin
        if IsXCoord then begin
          NewPoint.X:=TmpVal;
          IsXCoord:=FALSE;
        end
        else begin
          NewPoint.Y:=TmpVal;
          IsXCoord:=TRUE;
          Proj^.NewPoly^.PointInsert(Proj^.PInfo,NewPoint,FALSE);
          Proj^.NewPoly^.LastIns:=True;
          Proj^.NewPoly^.LineOn:=True;
        end;
      end;
    end;
  begin
    Proj:=(PProj(AData));
    Result:=FALSE;
    NewPoint.Init(0,0);
    IsXCoord:=TRUE;
    SSelBorder^.ForEach(@CreatePoints);
    if (Proj^.NewPoly^.Data^.Count >= MinCoords) and (Proj^.NewPoly^.Data^.Count <= MaxCoords) then Result:=TRUE
    else Proj^.NewPoly^.EndIt(Proj^.PInfo);
  end;

Procedure TDBSelection.StartDBSelection;
{$IFNDEF WMLT}
  var Proj         : PProj;
      SObjID       : LongInt;
      SCutInside   : Boolean;
      SEdges       : Word;
      SLineEnds    : Word;
      SDeselect    : Boolean;
      SAddSubXOr   : TSelectionMode;
      ParamsOK     : Boolean;
      ErrorCode    : Word;
      SSelTmpLayer : PLayer;
      Count        : Integer;
      MinCoords    : Integer;
      TempString   : String;
      TmpStr       : Array[0..20] of Char;
      OldTop       : PLayer;
      AItem        : PIndex;
      SItem        : PIndex;
      AView        : PView;
      CoordX       : LongInt;
      CoordY       : LongInt;
      MCoords      : Integer;
      Error        : Integer;
      DrawPos      : TDPoint;
      SelObjType   : Integer;
      DoSelect     : Boolean;
      ResLInt      : LongInt;
      ResString    : String;
      ResReal      : Real;
      StrOK        : Boolean;
      StrFound     : Boolean;
      StrToLong    : Boolean;
      MoreStrs     : Boolean;
  Procedure ReadOldTSLayers
     (
     Item          : PLayer
     ); Far;
    begin
      if Item^.GetState(sf_DoSelect) then OldTSLayers^.Insert(Item);
    end;
  Procedure WriteAllTSLayers
     (
     Item          : PLayer
     ); Far;
    begin
      Item^.SetState(sf_DoSelect,FALSE);
    end;
  Procedure SetTSLayers
     (
     Item          : PLayer
     ); Far;
    begin
      Item^.SetState(sf_DoSelect,TRUE);
    end;
  Procedure CopyPolyPoints
     (
     Item          : PDPoint
     ); Far;
    begin
      SelOffsetLine^.InsertPoint(Item^);
    end;
{$ENDIF}
  begin
{$IFNDEF WMLT}
    Proj:=(PProj(AData));
    ClearOldTSLayers;
    ClearSSelBorder;
    ClearSSelObjLayer;
    ParamsOK:=TRUE;
    ErrorCode:=0;
    ResetTM:=FALSE;
    ResetL:=FALSE;
    SSelObjLayer:=New(PCollection,Init(3,3));
    SSelBorder:=New(PBigUStrColl,Init);
    {**************************************** Selektionsart ****************************************}
    SType:=SelTypeNoType;
    if ReadDDECommandLongInt(PChar(DDEHandler.DDEData^.At(0)),1,0,WgBlockStart,
                                         DDEHandler.FDDESepSign,WgBlockEnd,ResLInt,StrFound,MoreStrs) then begin
      if StrFound then begin
        case ResLInt of
          0 : SType:=SelTypePoint;
          1 : SType:=SelTypeArea;
          2 : SType:=SelTypeCircle;
          3 : SType:=SelTypeRect;
          4 : SType:=SelTypePoly;
          5 : SType:=SelTypeOffset;
          6 : SType:=SelTypeWithArea;
          else SType:=SelTypeNoType;
        end;
      end
      else begin
        ParamsOK:=FALSE;
        ErrorCode:=1;
      end;
    end
    else begin
      ParamsOK:=FALSE;
      ErrorCode:=1;
    end;
    {**************************************** Zeichnen oder selektieren ****************************************}
    if ParamsOK then begin
      SDrawSel:=DrawSelNoRange;
      if ReadDDECommandLongInt(PChar(DDEHandler.DDEData^.At(0)),1,1,WgBlockStart,
                                           DDEHandler.FDDESepSign,WgBlockEnd,ResLInt,StrFound,MoreStrs) then begin
        if StrFound then begin
          case ResLInt of
            0 : begin
                  SDrawSel:=DrawSelDrawRange;
                  if SType = SelTypeWithArea then begin
                    ParamsOK:=FALSE;
                    ErrorCode:=2;
                  end;
                end;
            1 : begin
                  SDrawSel:=DrawSelSelRange;
                  if SType = SelTypeRect then begin
                    ParamsOK:=FALSE;
                    ErrorCode:=2;
                  end;
                end;
            2 : begin
                  SDrawSel:=DrawSelGetRange;
                  if SType = SelTypeWithArea then begin
                    ParamsOK:=FALSE;
                    ErrorCode:=2;
                  end;
                end;
            3 : begin
                  SDrawSel:=DrawSelGetObjOnLayer;
                  if SType = SelTypeRect then begin
                    ParamsOK:=FALSE;
                    ErrorCode:=2;
                  end;
                end;
            4 : begin
                  SDrawSel:=DrawSelGetObjAllLayers;
                  if SType = SelTypeRect then begin
                    ParamsOK:=FALSE;
                    ErrorCode:=2;
                  end;
                end;
            else begin
                     SDrawSel:=DrawSelNoRange;
                     ParamsOK:=FALSE;
                     ErrorCode:=2;
                   end;
          end;
        end
        else begin
          ParamsOK:=FALSE;
          ErrorCode:=2;
        end;
      end
      else begin
        ParamsOK:=FALSE;
        ErrorCode:=2;
      end;
    end;
    {**************************************** ID des Ausgangsobjektes ****************************************}
    if ParamsOK then begin
      SObjID:=0;
      if (SDrawSel = DrawSelGetObjOnLayer) or (SDrawSel = DrawSelGetObjAllLayers) then begin
        if ReadDDECommandLongInt(PChar(DDEHandler.DDEData^.At(0)),1,2,WgBlockStart,
                                             DDEHandler.FDDESepSign,WgBlockEnd,ResLInt,StrFound,MoreStrs) then begin
          if StrFound then SObjID:=ResLInt
          else begin
            ParamsOK:=FALSE;
            ErrorCode:=3;
          end;
        end
        else begin
          ParamsOK:=FALSE;
          ErrorCode:=3;
        end;
        if SObjID = 0 then begin
          ParamsOK:=FALSE;
          ErrorCode:=3;
        end;
      end;
    end;
    {**************************************** Layer des Ausgangsobjektes ****************************************}
    if ParamsOK then begin
      SObjLayer:=NIL;
      if (SDrawSel = DrawSelSelRange) or (SDrawSel = DrawSelGetObjOnLayer) then begin
        if (DDEHandler.FGotLInfo = 0) or (DDEHandler.FGotLInfo = 1) then begin
          StrOK:=ReadDDECommandString(PChar(DDEHandler.DDEData^.At(0)),1,3,WgBlockStart,
                                                  DDEHandler.FDDESepSign,WgBlockEnd,ResString,StrToLong,MoreStrs);
          if StrOK then begin
            if ResString = '' then SObjLayer:=Proj^.Layers^.TopLayer
            else begin
              SObjLayer:=Proj^.Layers^.NameToLayer(ResString);
              if SObjLayer = NIL then begin
                ParamsOK:=FALSE;
                ErrorCode:=4;
              end;
            end;
          end
          else SObjLayer:=Proj^.Layers^.TopLayer;
        end
        else if DDEHandler.FGotLInfo = 2 then begin
          if ReadDDECommandLongInt(PChar(DDEHandler.DDEData^.At(0)),1,3,WgBlockStart,
                                               DDEHandler.FDDESepSign,WgBlockEnd,ResLInt,StrFound,MoreStrs) then begin
            if StrFound then begin
              SObjLayer:=Proj^.Layers^.IndexToLayer(ResLInt);
              if SObjLayer = NIL then begin
                ParamsOK:=FALSE;
                ErrorCode:=4;
              end;
            end
            else SObjLayer:=Proj^.Layers^.TopLayer;
          end
          else begin
            StrOK:=ReadDDECommandString(PChar(DDEHandler.DDEData^.At(0)),1,3,WgBlockStart,
                                                    DDEHandler.FDDESepSign,WgBlockEnd,ResString,StrToLong,MoreStrs);
            if StrOK then begin
              if ResString = '' then SObjLayer:=Proj^.Layers^.TopLayer
              else begin
                ParamsOK:=FALSE;
                ErrorCode:=4;
              end;
            end;
          end;
        end;
        if SObjLayer = NIL then begin
          ParamsOK:=FALSE;
          ErrorCode:=4;
        end;
      end
      else if (SDrawSel = DrawSelDrawRange) or (SDrawSel = DrawSelGetRange) then
        SObjLayer:=Proj^.Layers^.TopLayer;
    end;
    {**************************************** Ausgangsobjekt selektieren ****************************************}
    if ParamsOK then begin
      SSelFirstObj:=SelFirstObjectNo;
      if (SDrawSel = DrawSelSelRange) or (SDrawSel = DrawSelGetObjOnLayer) or (SDrawSel = DrawSelGetObjAllLayers) then begin
        if ReadDDECommandLongInt(PChar(DDEHandler.DDEData^.At(0)),1,4,WgBlockStart,
                                             DDEHandler.FDDESepSign,WgBlockEnd,ResLInt,StrFound,MoreStrs) then begin
          if StrFound then begin
            case ResLInt of
              0 : SSelFirstObj:=SelFirstObjectNo;
              1 : SSelFirstObj:=SelFirstObjectYes;
            end;
          end;
        end;
      end;
    end;
    {**************************************** zu selektierende Objekttypen ****************************************}
    if ParamsOK then begin
      SSelObjType:=ot_Any;
      if ReadDDECommandLongInt(PChar(DDEHandler.DDEData^.At(0)),1,5,WgBlockStart,
                                           DDEHandler.FDDESepSign,WgBlockEnd,ResLInt,StrFound,MoreStrs) then begin
        if StrFound then begin
          SSelObjType:=[];
          if (ResLInt and SelObjTypePoints) > 0 then Include(SSelObjType,ot_Pixel);
          if (ResLInt and SelObjTypePolies) > 0 then Include(SSelObjType,ot_Poly);
          if (ResLInt and SelObjTypeAreas) > 0 then Include(SSelObjType,ot_CPoly);
          if (ResLInt and SelObjTypeCircles) > 0 then Include(SSelObjType,ot_Circle);
          if (ResLInt and SelObjTypeArcs) > 0 then Include(SSelObjType,ot_Arc);
          if (ResLInt and SelObjTypeTexts) > 0 then Include(SSelObjType,ot_Text);
          if (ResLInt and SelObjTypeSymbols) > 0 then Include(SSelObjType,ot_Symbol);
          if (ResLInt and SelObjTypePictures) > 0 then Include(SSelObjType,ot_Image);
          if (ResLInt and SelObjTypeSplines) > 0 then Include(SSelObjType,ot_Spline);
          if (ResLInt and SelObjTypeAnnotations) > 0 then Include(SSelObjType,ot_RText);
          if (ResLInt and SelObjTypeCharts) > 0 then Include(SSelObjType,ot_BusGraph);
          if (ResLInt and SelObjTypeMeasures) > 0 then Include(SSelObjType,ot_MesLine);
          if (ResLInt = 0) or (ResLInt = SelObjTypeAllObjects) then SSelObjType:=ot_Any;
        end;
      end;
      if SType = SelTypeWithArea then SSelObjType:=[ot_CPoly];
    end;
    {**************************************** Selektionslayer ****************************************}
    if ParamsOK then begin
      Count:=0;
      repeat
        SSelTmpLayer:=NIL;
        if (DDEHandler.FGotLInfo = 0) or (DDEHandler.FGotLInfo = 1) then begin
          StrOK:=ReadDDECommandString(PChar(DDEHandler.DDEData^.At(0)),2,Count,WgBlockStart,
                                                  DDEHandler.FDDESepSign,WgBlockEnd,ResString,StrToLong,MoreStrs);
          if StrOK then SSelTmpLayer:=Proj^.Layers^.NameToLayer(ResString);
        end
        else if DDEHandler.FGotLInfo = 2 then begin
          if ReadDDECommandLongInt(PChar(DDEHandler.DDEData^.At(0)),2,Count,WgBlockStart,
                                               DDEHandler.FDDESepSign,WgBlockEnd,ResLInt,StrFound,MoreStrs) then begin
            if StrFound then SSelTmpLayer:=Proj^.Layers^.IndexToLayer(ResLInt);
          end;
        end;
        if SSelTmpLayer <> NIL then SSelObjLayer^.Insert(SSelTmpLayer);
        Inc(Count);
      until not MoreStrs;
      if SSelObjLayer^.Count = 0 then SSelObjLayer^.Insert(Proj^.Layers^.TopLayer);
    end;
    {**************************************** Geschnittene oder innenliegende Objekte ****************************************}
    if ParamsOK then begin
      SCutInside:=CutInsideCut;
      if (SType = SelTypeArea) or (SType = SelTypeCircle) or (SType = SelTypeRect) or (SType = SelTypeOffset) or ((SType = SelTypeWithArea)) then begin
        if ReadDDECommandLongInt(PChar(DDEHandler.DDEData^.At(0)),3,0,WgBlockStart,
                                             DDEHandler.FDDESepSign,WgBlockEnd,ResLInt,StrFound,MoreStrs) then begin
          if StrFound then begin
            case ResLInt of
              0 : SCutInside:=CutInsideCut;
              1 : SCutInside:=CutInsideInside;
            end;
          end;
        end;
      end;
    end;
    {**************************************** Offset ****************************************}
    if ParamsOK then begin
      SOffset:=0;
      if SType = SelTypeOffset then begin
        if ReadDDECommandReal(PChar(DDEHandler.DDEData^.At(0)),3,1,WgBlockStart,
                                          DDEHandler.FDDESepSign,WgBlockEnd,ResReal,StrFound,MoreStrs) then begin
          if StrFound then begin
            if (ResReal >= -MaxLongInt/100) and (ResReal <= MaxLongInt/100) then
              SOffset:=Round(ResReal*100);
          end;
        end;
      end;
    end;
    {**************************************** Ecken ****************************************}
    if ParamsOK then begin
      SEdges:=EdgesAngular;
      if SType = SelTypeOffset then begin
        if ReadDDECommandLongInt(PChar(DDEHandler.DDEData^.At(0)),3,2,WgBlockStart,
                                             DDEHandler.FDDESepSign,WgBlockEnd,ResLInt,StrFound,MoreStrs) then begin
          if StrFound then begin
            case ResLInt of
              0 : SEdges:=EdgesRounded;
              1 : SEdges:=EdgesAngular;
            end;
          end;
        end;
      end;
    end;
    {**************************************** Linienenden ****************************************}
    if ParamsOK then begin
      SLineEnds:=LineEndsStraigthNoDist;
      if SType = SelTypeOffset then begin
        if ReadDDECommandLongInt(PChar(DDEHandler.DDEData^.At(0)),3,3,WgBlockStart,
                                             DDEHandler.FDDESepSign,WgBlockEnd,ResLInt,StrFound,MoreStrs) then begin
          if StrFound then begin
            case ResLInt of
              0 : SLineEnds:=LineEndsNoEndsOneSide;
              1 : SLineEnds:=LineEndsNoEndsTwoSides;
              2 : SLineEnds:=LineEndsStraigthNoDist;
              3 : SLineEnds:=LineEndsStraigthDist;
              4 : SLineEnds:=LineEndsRounded;
            end;
          end;
        end;
      end;
    end;
    {**************************************** Selektionsgrenzen ****************************************}
    if ParamsOK then begin
      if SDrawSel = DrawSelGetRange then begin
        case SType of
          SelTypeNoType   : MinCoords:=0;
          SelTypePoint    : MinCoords:=2;
          SelTypeArea     : MinCoords:=6;
          SelTypeCircle   : MinCoords:=3;
          SelTypeRect     : MinCoords:=4;
          SelTypePoly     : MinCoords:=2;
          SelTypeOffset   : begin
                              case SLineEnds of
                                LineEndsNoEndsOneSide  : MinCoords:=6;
                                LineEndsNoEndsTwoSides : MinCoords:=6;
                                LineEndsStraigthNoDist : MinCoords:=4;
                                LineEndsStraigthDist   : MinCoords:=4;
                                LineEndsRounded        : MinCoords:=4;
                              end;
                            end;
          SelTypeWithArea : MinCoords:=6;
        end;
        Count:=0;
        repeat
          if ReadDDECommandReal(PChar(DDEHandler.DDEData^.At(0)),4,Count,WgBlockStart,
                                            DDEHandler.FDDESepSign,WgBlockEnd,ResReal,StrFound,MoreStrs) then begin
            if StrFound then begin
              if (ResReal >= -MaxLongInt/100) and (ResReal <= MaxLongInt/100) then begin
                Str(Round(ResReal*100):0,TempString);
                StrPCopy(TmpStr,TempString);
                SSelBorder^.Insert(StrNew(@TmpStr));
                Inc(Count);
              end
              else begin
                ParamsOK:=FALSE;
                ErrorCode:=6;
              end;
            end
            else begin
              ParamsOK:=FALSE;
              ErrorCode:=6;
            end;
          end
          else begin
            ParamsOK:=FALSE;
            ErrorCode:=6;
          end;
        until (not MoreStrs) or (not ParamsOK);
        if Count < MinCoords then begin
          ParamsOK:=FALSE;
          ErrorCode:=6;
        end;
      end;
    end;
    {**************************************** Selektionslinie zeichnen ****************************************}
    if ParamsOK then begin
      SDrawSelLine:=DrawSelLineNo;
      if ReadDDECommandLongInt(PChar(DDEHandler.DDEData^.At(0)),5,0,WgBlockStart,
                                           DDEHandler.FDDESepSign,WgBlockEnd,ResLInt,StrFound,MoreStrs) then begin
        if StrFound then begin
          case ResLInt of
            0 : SDrawSelLine:=DrawSelLineNo;
            1 : SDrawSelLine:=DrawSelLineYes;
          end;
        end;
      end;
    end;
    {**************************************** Selektionslinienlayer ****************************************}
    if ParamsOK then begin
      SSelLineLayer:=NIL;
      if SDrawSelLine = DrawSelLineYes then begin
        if (DDEHandler.FGotLInfo = 0) or (DDEHandler.FGotLInfo = 1) then begin
          StrOK:=ReadDDECommandString(PChar(DDEHandler.DDEData^.At(0)),5,1,WgBlockStart,
                                                  DDEHandler.FDDESepSign,WgBlockEnd,ResString,StrToLong,MoreStrs);
          if StrOK then SSelLineLayer:=Proj^.Layers^.NameToLayer(ResString)
          else SDrawSelLine:=DrawSelLineNo;
        end
        else if DDEHandler.FGotLInfo = 2 then begin
          if ReadDDECommandLongInt(PChar(DDEHandler.DDEData^.At(0)),5,1,WgBlockStart,
                                               DDEHandler.FDDESepSign,WgBlockEnd,ResLInt,StrFound,MoreStrs) then begin
            if StrFound then SSelLineLayer:=Proj^.Layers^.IndexToLayer(ResLInt)
            else SDrawSelLine:=DrawSelLineNo;
          end
          else SDrawSelLine:=DrawSelLineNo;
        end;
        if SSelLineLayer = NIL then SDrawSelLine:=DrawSelLineNo;
      end;
    end;
    {**************************************** Zu selektierten Objekten zoomen ****************************************}
    if ParamsOK then begin
      SZoom:=ZoomNo;
      if ReadDDECommandLongInt(PChar(DDEHandler.DDEData^.At(0)),6,0,WgBlockStart,
                                           DDEHandler.FDDESepSign,WgBlockEnd,ResLInt,StrFound,MoreStrs) then begin
        if StrFound then begin
          case ResLInt of
            0 : SZoom:=ZoomNo;
            1 : SZoom:=ZoomYes;
          end;
        end;
      end;
    end;
    {**************************************** IDs selektierter Objekte an DB senden ****************************************}
    if ParamsOK then begin
      SSendIDs:=SendIDsYes;
      if ReadDDECommandLongInt(PChar(DDEHandler.DDEData^.At(0)),6,1,WgBlockStart,
                                           DDEHandler.FDDESepSign,WgBlockEnd,ResLInt,StrFound,MoreStrs) then begin
        if StrFound then begin
          case ResLInt of
            0 : SSendIDs:=SendIDsNo;
            1 : SSendIDs:=SendIDsYes;
          end;
        end;
      end;
    end;
    {**************************************** Vor der Selektion alles deselektieren ****************************************}
    if ParamsOK then begin
      SDeselect:=DeselectYes;
      if ReadDDECommandLongInt(PChar(DDEHandler.DDEData^.At(0)),7,0,WgBlockStart,
                                           DDEHandler.FDDESepSign,WgBlockEnd,ResLInt,StrFound,MoreStrs) then begin
        if StrFound then begin
          case ResLInt of
            0 : SDeselect:=DeselectNo;
            1 : SDeselect:=DeselectYes;
          end;
        end;
      end;
    end;
    {**************************************** Selektieren, deselektieren oder Status wechseln ****************************************}
    if ParamsOK then begin
      SAddSubXOr:=smAdditive;
      if ReadDDECommandLongInt(PChar(DDEHandler.DDEData^.At(0)),7,1,WgBlockStart,
                                           DDEHandler.FDDESepSign,WgBlockEnd,ResLInt,StrFound,MoreStrs) then begin
        if StrFound then begin
          case ResLInt of
            SelAddSubAdd : SAddSubXOr:=smAdditive;
            SelAddSubSub : SAddSubXOr:=smSubtractive;
            SelAddSubChange : SAddSubXOr:=smXOr;
          end;
        end;
      end;
    end;
    {**************************************** Selektion und Nachbarsuche ****************************************}
    if DDEHandler.DDEData^.GetCount > 0 then PChar(DDEHandler.DDEData^.At(0))[0]:=DDEStrUsed;
    DDEHandler.DeleteDDEData;
    if ParamsOK then begin
      if SDeselect then Proj^.DeSelectAll(TRUE);
      with Proj^.SelectData do begin
        case SType of
          SelTypePoint    : SelectWith:=swPixel;
          SelTypeArea     : SelectWith:=swPoly;
          SelTypeCircle   : SelectWith:=swCircle;
          SelTypeRect     : SelectWith:=swRect;
          SelTypePoly     : SelectWith:=swLine;
          SelTypeOffset   : SelectWith:=swOffset;
          SelTypeWithArea : SelectWith:=swPoint;
          else SelectWith:=swPixel;
        end;
        case SDrawSel of
          DrawSelDrawRange       : SelectObject:=FALSE;
          DrawSelSelRange        : SelectObject:=TRUE;
          DrawSelGetRange        : SelectObject:=FALSE;
          DrawSelGetObjOnLayer   : SelectObject:=TRUE;
          DrawSelGetObjAllLayers : SelectObject:=TRUE;
          else SelectObject:=TRUE;
        end;
        if SCutInside then SelectInside:=TRUE
        else SelectInside:=FALSE;
        SelectTypes:=SSelObjType;
        if (SDrawSel = DrawSelSelRange) or (SDrawSel = DrawSelGetObjOnLayer) or
           (SDrawSel = DrawSelDrawRange) or (SDrawSel = DrawSelGetRange) then begin
          if SObjLayer <> NIL then SelectLayer:=SObjLayer^.Index
          else SelectLayer:=Proj^.Layers^.TopLayer^.Index;
        end
        else if SDrawSel = DrawSelGetObjAllLayers then begin
          OldTop:=Proj^.Layers^.TopLayer;
          AItem:=New(PIndex,Init(SObjID));
          SItem:=Proj^.Layers^.TopLayer^.HasObject(AItem);
          if (SItem = NIL) then begin
            Count:=0;
            while (SItem = NIL) and (Count < Proj^.Layers^.LData^.Count) do begin
              Proj^.Layers^.TopLayer:=Proj^.Layers^.LData^.At(Count);
              if not Proj^.Layers^.TopLayer^.GetState(sf_LayerOff) then
                SItem:=Proj^.Layers^.TopLayer^.HasObject(AItem);
              Inc(Count);
            end;
          end;
          Dispose(AItem,Done);
          if SItem <> NIL then begin
            SObjLayer:=Proj^.Layers^.TopLayer;
            SelectLayer:=Proj^.Layers^.TopLayer^.Index;
          end;
          Proj^.Layers^.TopLayer:=OldTop;
        end;
      end;
      with Proj^.SelectOffsetData do begin
        case SEdges of
          EdgesAngular : RoundEdges:=FALSE;
          EdgesRounded : RoundEdges:=TRUE;
        end;
        case SLineEnds of
          LineEndsNoEndsOneSide  : OffsetType:=otSingle;
          LineEndsNoEndsTwoSides : OffsetType:=otDouble;
          LineEndsStraigthNoDist : OffsetType:=otStreight;
          LineEndsStraigthDist   : OffsetType:=otStreightDistance;
          LineEndsRounded        : OffsetType:=otRounded;
        end;
      end;
      OldSelState:=Proj^.PInfo^.SelectionMode;
      Proj^.PInfo^.SelectionMode:=SAddSubXOr;
      Selecting:=TRUE;
      ResetTM:=FALSE;
      ResetL:=FALSE;
      case SType of
        SelTypePoint    : begin
            Proj^.Layers^.LData^.ForEach(@ReadOldTSLayers);
            Proj^.Layers^.LData^.ForEach(@WriteAllTSLayers);
            SSelObjLayer^.ForEach(@SetTSLayers);
            ResetL:=TRUE;
            case SDrawSel of
                DrawSelDrawRange       : begin
                    OldTransp:=Proj^.Layers^.TranspLayer;
                    Proj^.Layers^.TranspLayer:=TRUE;
                    OldMen:=Proj^.ActualMen;
                    ResetTM:=TRUE;
                    Proj^.SetActualMenu(mn_DrawForSelect);
                  end;
                DrawSelSelRange        : begin
                    OldTransp:=Proj^.Layers^.TranspLayer;
                    Proj^.Layers^.TranspLayer:=TRUE;
                    OldMen:=Proj^.ActualMen;
                    ResetTM:=TRUE;
                    Proj^.SelectMask:=[ot_Pixel];
                    Proj^.SetActualMenu(mn_SelForSelect);
                  end;
                DrawSelGetRange        : begin
                    OldTransp:=Proj^.Layers^.TranspLayer;
                    Proj^.Layers^.TranspLayer:=TRUE;
                    OldMen:=Proj^.ActualMen;
                    ResetTM:=TRUE;
                    Val(PChar(SSelBorder^.At(0)),CoordX,Error);
                    Val(PChar(SSelBorder^.At(1)),CoordY,Error);
                    Proj^.SelPoint.Init(CoordX,CoordY);
                    ContinueDBSelection;
                  end;
                DrawSelGetObjOnLayer   : begin
                    AItem:=New(PIndex,Init(SObjID));
                    SItem:=SObjLayer^.HasObject(AItem);
                    Dispose(AItem,Done);
                    if (SItem <> NIL) and (SItem^.GetObjType = ot_Pixel) then begin
                      SelItem:=SItem;
                      OldTransp:=Proj^.Layers^.TranspLayer;
                      Proj^.Layers^.TranspLayer:=TRUE;
                      OldMen:=Proj^.ActualMen;
                      ResetTM:=TRUE;
                      AView:=Proj^.Layers^.IndexObject(Proj^.PInfo,SItem);
                      Proj^.SelPoint.Init(PPixel(AView)^.Position.X,PPixel(AView)^.Position.Y);
                      ContinueDBSelection;
                    end
                    else ErrorCode:=7;
                  end;
                DrawSelGetObjAllLayers : begin
                    if SObjLayer <> NIL then begin
                      AItem:=New(PIndex,Init(SObjID));
                      SItem:=SObjLayer^.HasObject(AItem);
                      Dispose(AItem,Done);
                      if SItem^.GetObjType = ot_Pixel then begin
                        SelItem:=SItem;
                        OldTransp:=Proj^.Layers^.TranspLayer;
                        Proj^.Layers^.TranspLayer:=TRUE;
                        OldMen:=Proj^.ActualMen;
                        ResetTM:=TRUE;
                        AView:=Proj^.Layers^.IndexObject(Proj^.PInfo,SItem);
                        Proj^.SelPoint.Init(PPixel(AView)^.Position.X,PPixel(AView)^.Position.Y);
                        ContinueDBSelection;
                      end
                      else ErrorCode:=8;
                    end
                    else ErrorCode:=8;
                  end;
              end;
          end;
        SelTypeArea     : begin
            case SDrawSel of
                DrawSelDrawRange       : begin
                    OldTransp:=Proj^.Layers^.TranspLayer;
                    Proj^.Layers^.TranspLayer:=TRUE;
                    OldMen:=Proj^.ActualMen;
                    ResetTM:=TRUE;
                    Proj^.SetActualMenu(mn_DrawForSelect);
                  end;
                DrawSelSelRange        : begin
                    OldTransp:=Proj^.Layers^.TranspLayer;
                    Proj^.Layers^.TranspLayer:=TRUE;
                    OldMen:=Proj^.ActualMen;
                    ResetTM:=TRUE;
                    Proj^.SelectMask:=[ot_CPoly];
                    Proj^.SetActualMenu(mn_SelForSelect);
                  end;
                DrawSelGetRange        : begin
                    OldTransp:=Proj^.Layers^.TranspLayer;
                    Proj^.Layers^.TranspLayer:=TRUE;
                    OldMen:=Proj^.ActualMen;
                    ResetTM:=TRUE;
                    if CreateSelPoly(3,MaxInt) then ContinueDBSelection
                    else ErrorCode:=11;
                  end;
                DrawSelGetObjOnLayer   : begin
                    AItem:=New(PIndex,Init(SObjID));
                    SItem:=SObjLayer^.HasObject(AItem);
                    Dispose(AItem,Done);
                    if (SItem <> NIL) and (SItem^.GetObjType = ot_CPoly) then begin
                      AView:=Proj^.Layers^.IndexObject(Proj^.PInfo,SItem);
                      SelItem:=AView;
                      OldTransp:=Proj^.Layers^.TranspLayer;
                      Proj^.Layers^.TranspLayer:=TRUE;
                      OldMen:=Proj^.ActualMen;
                      ResetTM:=TRUE;
                      ContinueDBSelection;
                    end
                    else ErrorCode:=7;
                  end;
                DrawSelGetObjAllLayers : begin
                    if SObjLayer <> NIL then begin
                      AItem:=New(PIndex,Init(SObjID));
                      SItem:=SObjLayer^.HasObject(AItem);
                      Dispose(AItem,Done);
                      if (SItem <> NIL) and (SItem^.GetObjType = ot_CPoly) then begin
                        AView:=Proj^.Layers^.IndexObject(Proj^.PInfo,SItem);
                        SelItem:=AView;
                        OldTransp:=Proj^.Layers^.TranspLayer;
                        Proj^.Layers^.TranspLayer:=TRUE;
                        OldMen:=Proj^.ActualMen;
                        ResetTM:=TRUE;
                        ContinueDBSelection;
                      end
                      else ErrorCode:=8;
                    end
                    else ErrorCode:=8;
                  end;
              end;
          end;
        SelTypeCircle   : begin
            case SDrawSel of
                DrawSelDrawRange       : begin
                    OldTransp:=Proj^.Layers^.TranspLayer;
                    Proj^.Layers^.TranspLayer:=TRUE;
                    OldMen:=Proj^.ActualMen;
                    ResetTM:=TRUE;
                    Proj^.SetActualMenu(mn_DrawForSelect);
                  end;
                DrawSelSelRange        : begin
                    OldTransp:=Proj^.Layers^.TranspLayer;
                    Proj^.Layers^.TranspLayer:=TRUE;
                    OldMen:=Proj^.ActualMen;
                    ResetTM:=TRUE;
                    Proj^.SelectMask:=[ot_Circle];
                    Proj^.SetActualMenu(mn_SelForSelect);
                  end;
                DrawSelGetRange        : begin
                    OldTransp:=Proj^.Layers^.TranspLayer;
                    Proj^.Layers^.TranspLayer:=TRUE;
                    OldMen:=Proj^.ActualMen;
                    ResetTM:=TRUE;
                    if CreateSelCircle then ContinueDBSelection
                    else ErrorCode:=11;
                  end;
                DrawSelGetObjOnLayer   : begin
                    AItem:=New(PIndex,Init(SObjID));
                    SItem:=SObjLayer^.HasObject(AItem);
                    Dispose(AItem,Done);
                    if (SItem <> NIL) and (SItem^.GetObjType = ot_Circle) then begin
                      AView:=Proj^.Layers^.IndexObject(Proj^.PInfo,SItem);
                      SelItem:=AView;
                      OldTransp:=Proj^.Layers^.TranspLayer;
                      Proj^.Layers^.TranspLayer:=TRUE;
                      OldMen:=Proj^.ActualMen;
                      ResetTM:=TRUE;
                      ContinueDBSelection;
                    end
                    else ErrorCode:=7;
                  end;
                DrawSelGetObjAllLayers : begin
                    if SObjLayer <> NIL then begin
                      AItem:=New(PIndex,Init(SObjID));
                      SItem:=SObjLayer^.HasObject(AItem);
                      Dispose(AItem,Done);
                      if (SItem <> NIL) and (SItem^.GetObjType = ot_Circle) then begin
                        AView:=Proj^.Layers^.IndexObject(Proj^.PInfo,SItem);
                        SelItem:=AView;
                        OldTransp:=Proj^.Layers^.TranspLayer;
                        Proj^.Layers^.TranspLayer:=TRUE;
                        OldMen:=Proj^.ActualMen;
                        ResetTM:=TRUE;
                        ContinueDBSelection;
                      end
                      else ErrorCode:=8;
                    end
                    else ErrorCode:=8;
                  end;
              end;
          end;
        SelTypeRect     : begin
            case SDrawSel of
                DrawSelDrawRange       : begin
                    OldTransp:=Proj^.Layers^.TranspLayer;
                    Proj^.Layers^.TranspLayer:=TRUE;
                    OldMen:=Proj^.ActualMen;
                    ResetTM:=TRUE;
                    Proj^.SetActualMenu(mn_DrawForSelect);
                  end;
                DrawSelSelRange        : ErrorCode:=10;
                DrawSelGetRange        : begin
                    OldTransp:=Proj^.Layers^.TranspLayer;
                    Proj^.Layers^.TranspLayer:=TRUE;
                    OldMen:=Proj^.ActualMen;
                    ResetTM:=TRUE;
                    if CreateSelPoly(2,2) then ContinueDBSelection
                    else ErrorCode:=11;
                  end;
                DrawSelGetObjOnLayer   : ErrorCode:=10;
                DrawSelGetObjAllLayers : ErrorCode:=10;
              end;
          end;
        SelTypePoly     : begin
            case SDrawSel of
                DrawSelDrawRange       : begin
                    OldTransp:=Proj^.Layers^.TranspLayer;
                    Proj^.Layers^.TranspLayer:=TRUE;
                    OldMen:=Proj^.ActualMen;
                    ResetTM:=TRUE;
                    Proj^.SetActualMenu(mn_DrawForSelect);
                  end;
                DrawSelSelRange        : begin
                    OldTransp:=Proj^.Layers^.TranspLayer;
                    Proj^.Layers^.TranspLayer:=TRUE;
                    OldMen:=Proj^.ActualMen;
                    ResetTM:=TRUE;
                    Proj^.SelectMask:=[ot_Poly];
                    Proj^.SetActualMenu(mn_SelForSelect);
                  end;
                DrawSelGetRange        : begin
                    OldTransp:=Proj^.Layers^.TranspLayer;
                    Proj^.Layers^.TranspLayer:=TRUE;
                    OldMen:=Proj^.ActualMen;
                    ResetTM:=TRUE;
                    if CreateSelPoly(2,MaxInt) then ContinueDBSelection
                    else ErrorCode:=11;
                  end;
                DrawSelGetObjOnLayer   : begin
                    AItem:=New(PIndex,Init(SObjID));
                    SItem:=SObjLayer^.HasObject(AItem);
                    Dispose(AItem,Done);
                    if (SItem <> NIL) and (SItem^.GetObjType = ot_Poly) then begin
                      AView:=Proj^.Layers^.IndexObject(Proj^.PInfo,SItem);
                      SelItem:=AView;
                      OldTransp:=Proj^.Layers^.TranspLayer;
                      Proj^.Layers^.TranspLayer:=TRUE;
                      OldMen:=Proj^.ActualMen;
                      ResetTM:=TRUE;
                      ContinueDBSelection;
                    end
                    else ErrorCode:=7;
                  end;
                DrawSelGetObjAllLayers : begin
                    if SObjLayer <> NIL then begin
                      AItem:=New(PIndex,Init(SObjID));
                      SItem:=SObjLayer^.HasObject(AItem);
                      Dispose(AItem,Done);
                      if (SItem <> NIL) and (SItem^.GetObjType = ot_Poly) then begin
                        AView:=Proj^.Layers^.IndexObject(Proj^.PInfo,SItem);
                        SelItem:=AView;
                        OldTransp:=Proj^.Layers^.TranspLayer;
                        Proj^.Layers^.TranspLayer:=TRUE;
                        OldMen:=Proj^.ActualMen;
                        ResetTM:=TRUE;
                        ContinueDBSelection;
                      end
                      else ErrorCode:=8;
                    end
                    else ErrorCode:=8;
                  end;
              end;
          end;
        SelTypeOffset   : begin
            case SDrawSel of
                DrawSelDrawRange       : begin
                    OldTransp:=Proj^.Layers^.TranspLayer;
                    Proj^.Layers^.TranspLayer:=TRUE;
                    OldMen:=Proj^.ActualMen;
                    ResetTM:=TRUE;
                    Proj^.SetActualMenu(mn_DrawForSelect);
                  end;
                DrawSelSelRange        : begin
                    OldTransp:=Proj^.Layers^.TranspLayer;
                    Proj^.Layers^.TranspLayer:=TRUE;
                    OldMen:=Proj^.ActualMen;
                    ResetTM:=TRUE;
                    if Proj^.SelectOffsetData.OffsetType = otSingle then Proj^.SelectMask:=[ot_CPoly]
                    else if Proj^.SelectOffsetData.OffsetType = otDouble then Proj^.SelectMask:=[ot_Poly,ot_CPoly]
                    else Proj^.SelectMask:=[ot_Poly];
                    Proj^.SetActualMenu(mn_SelForSelect);
                  end;
                DrawSelGetRange        : begin
                    OldTransp:=Proj^.Layers^.TranspLayer;
                    Proj^.Layers^.TranspLayer:=TRUE;
                    OldMen:=Proj^.ActualMen;
                    ResetTM:=TRUE;
                    if (Proj^.SelectOffsetData.OffsetType = otSingle) or
                       (Proj^.SelectOffsetData.OffsetType = otDouble) then MCoords:=3
                    else MCoords:=2;
                    if CreateSelPoly(MCoords,MaxInt) then begin
                      if (Proj^.SelectOffsetData.OffsetType = otSingle) or
                         (Proj^.SelectOffsetData.OffsetType = otDouble) then SelOffsetLine:=New(PCPoly,Init)
                      else SelOffsetLine:=New(PPoly,Init);
                      Proj^.NewPoly^.GetPoly(Proj^.PInfo,SelOffsetLine,FALSE);
                      Proj^.OffsetItem:=SelOffsetLine;
                      if SOffset <> 0 then begin
                        CorrectDir:=TRUE;
                        ContinueDBSelection;
                      end
                      else begin
                        CorrectDir:=FALSE;
                        Proj^.NewPoly^.LineOn:=TRUE;
                        Proj^.NewPoly^.DrawReally:=TRUE;
                        Proj^.NewPoly^.Draw(Proj^.PInfo,TRUE);
                        Proj^.NewPoly^.DrawReally:=FALSE;
                        DrawPos.Init(0,0);
                        Proj^.InpOffset^.SetPoly(PPoly(Proj^.OffsetItem));
                        Proj^.InpOffset^.StartIt(Proj^.PInfo,DrawPos,FALSE);
                        Proj^.InpOffset^.MouseMove(Proj^.PInfo,DrawPos);
                        Proj^.SetActualMenu(mn_OffsetForSel);
                      end;
                    end
                    else ErrorCode:=11;
                  end;
                DrawSelGetObjOnLayer   : begin
                    AItem:=New(PIndex,Init(SObjID));
                    SItem:=SObjLayer^.HasObject(AItem);
                    Dispose(AItem,Done);
                    if (SItem <> NIL) then begin
                      DoSelect:=FALSE;
                      SelObjType:=SItem^.GetObjType;
                      case Proj^.SelectOffsetData.OffsetType of
                        otSingle           : DoSelect:=(SelObjType = ot_CPoly);
                        otDouble           : DoSelect:=((SelObjType = ot_CPoly) or (SelObjType = ot_Poly));
                        otStreight         : DoSelect:=(SelObjType = ot_Poly);
                        otStreightDistance : DoSelect:=(SelObjType = ot_Poly);
                        otRounded          : DoSelect:=(SelObjType = ot_Poly);
                      end;
                      if DoSelect then begin
                        OldTransp:=Proj^.Layers^.TranspLayer;
                        Proj^.Layers^.TranspLayer:=TRUE;
                        OldMen:=Proj^.ActualMen;
                        ResetTM:=TRUE;
                        AView:=Proj^.Layers^.IndexObject(Proj^.PInfo,SItem);
                        SelItem:=AView;
                        Proj^.OffsetItem:=Pointer(AView);
                        if SOffset <> 0 then begin
                          CorrectDir:=TRUE;
                          ContinueDBSelection;
                        end
                        else begin
                          CorrectDir:=FALSE;
                          DrawPos.Init(0,0);
                          Proj^.InpOffset^.SetPoly(PPoly(Proj^.OffsetItem));
                          Proj^.InpOffset^.StartIt(Proj^.PInfo,DrawPos,FALSE);
                          Proj^.InpOffset^.MouseMove(Proj^.PInfo,DrawPos);
                          Proj^.SetActualMenu(mn_OffsetForSel);
                        end;
                      end
                      else ErrorCode:=7;
                    end
                    else ErrorCode:=7;
                  end;
                DrawSelGetObjAllLayers : begin
                    if SObjLayer <> NIL then begin
                      AItem:=New(PIndex,Init(SObjID));
                      SItem:=SObjLayer^.HasObject(AItem);
                      Dispose(AItem,Done);
                      if (SItem <> NIL) then begin
                        DoSelect:=FALSE;
                        SelObjType:=SItem^.GetObjType;
                        case Proj^.SelectOffsetData.OffsetType of
                          otSingle           : DoSelect:=(SelObjType = ot_CPoly);
                          otDouble           : DoSelect:=((SelObjType = ot_CPoly) or (SelObjType = ot_Poly));
                          otStreight         : DoSelect:=(SelObjType = ot_Poly);
                          otStreightDistance : DoSelect:=(SelObjType = ot_Poly);
                          otRounded          : DoSelect:=(SelObjType = ot_Poly);
                        end;
                        if DoSelect then begin
                          OldTransp:=Proj^.Layers^.TranspLayer;
                          Proj^.Layers^.TranspLayer:=TRUE;
                          OldMen:=Proj^.ActualMen;
                          ResetTM:=TRUE;
                          AView:=Proj^.Layers^.IndexObject(Proj^.PInfo,SItem);
                          SelItem:=AView;
                          Proj^.OffsetItem:=Pointer(AView);
                          if SOffset <> 0 then begin
                            CorrectDir:=TRUE;
                            ContinueDBSelection;
                          end
                          else begin
                            CorrectDir:=FALSE;
                            DrawPos.Init(0,0);
                            Proj^.InpOffset^.SetPoly(PPoly(Proj^.OffsetItem));
                            Proj^.InpOffset^.StartIt(Proj^.PInfo,DrawPos,FALSE);
                            Proj^.InpOffset^.MouseMove(Proj^.PInfo,DrawPos);
                            Proj^.SetActualMenu(mn_OffsetForSel);
                          end;
                        end
                        else ErrorCode:=8;
                      end
                      else ErrorCode:=8;
                    end
                    else ErrorCode:=8;
                  end;
              end;
          end;
        SelTypeWithArea : begin
            case SDrawSel of
                DrawSelDrawRange       : ErrorCode:=10;
                DrawSelSelRange        : begin
                    OldTransp:=Proj^.Layers^.TranspLayer;
                    Proj^.Layers^.TranspLayer:=TRUE;
                    OldMen:=Proj^.ActualMen;
                    ResetTM:=TRUE;
                    Proj^.SelectMask:=[ot_CPoly];
                    Proj^.SetActualMenu(mn_SelForSelect);
                  end;
                DrawSelGetRange        : ErrorCode:=10;
                DrawSelGetObjOnLayer   : begin
                    AItem:=New(PIndex,Init(SObjID));
                    SItem:=SObjLayer^.HasObject(AItem);
                    Dispose(AItem,Done);
                    if (SItem <> NIL) and (SItem^.GetObjType = ot_CPoly) then begin
                      AView:=Proj^.Layers^.IndexObject(Proj^.PInfo,SItem);
                      SelItem:=AView;
                      OldTransp:=Proj^.Layers^.TranspLayer;
                      Proj^.Layers^.TranspLayer:=TRUE;
                      OldMen:=Proj^.ActualMen;
                      ResetTM:=TRUE;
                      ContinueDBSelection;
                    end
                    else ErrorCode:=7;
                  end;
                DrawSelGetObjAllLayers : begin
                    if SObjLayer <> NIL then begin
                      AItem:=New(PIndex,Init(SObjID));
                      SItem:=SObjLayer^.HasObject(AItem);
                      Dispose(AItem,Done);
                      if (SItem <> NIL) and (SItem^.GetObjType = ot_CPoly) then begin
                        AView:=Proj^.Layers^.IndexObject(Proj^.PInfo,SItem);
                        SelItem:=AView;
                        OldTransp:=Proj^.Layers^.TranspLayer;
                        Proj^.Layers^.TranspLayer:=TRUE;
                        OldMen:=Proj^.ActualMen;
                        ResetTM:=TRUE;
                        ContinueDBSelection;
                      end
                      else ErrorCode:=8;
                    end
                    else ErrorCode:=8;
                  end;
              end;
          end;
      end;
    end;
    if ErrorCode > 0 then begin
      EndDBSelection(FALSE,FALSE);
      Str(ErrorCode:0,TempString);
      DDEHandler.SendString('[SER]['+TempString+']');
    end;
{$ENDIF}
  end;

Procedure TDBSelection.ContinueDBSelection;
  var Proj         : PProj;
      AView        : PView;
      AItem        : PIndex;
      ObjIndex     : LongInt;
      NewPoint     : PPixel;
      SelPoly      : PCPoly;
      SelCircle    : PEllipse;
      Center       : TDPoint;
      Radius       : LongInt;
      Rect         : TDRect;
      SelLine      : PPoly;
      AOffset      : LongInt;
      OffPoly      : PCPoly;
      OArea        : Double;
      Cnt          : Integer;
      LCount       : Integer;
      LastLayer    : Boolean;
      Island1      : PIslandStruct;
      Island2      : PIslandStruct;
  Procedure SelByPoly
     (
     Item          : PLayer
     ); Far;
    begin
      Item^.SelectByPoly(Proj^.PInfo,SelPoly,Proj^.SelectData.SelectTypes,Proj^.SelectData.SelectInside);
    end;
  Procedure SelByCircle
     (
     Item          : PLayer
     ); Far;
    begin
      Item^.SelectByCircle(Proj^.PInfo,Center,Radius,Proj^.SelectData.SelectTypes,Proj^.SelectData.SelectInside);
    end;
  Procedure SelByPolyLine
     (
     Item          : PLayer
     ); Far;
    begin
      Item^.SelectByPolyLine(Proj^.PInfo,SelLine,Proj^.SelectData.SelectTypes);
    end;
  Procedure SelByOffset
     (
     Item          : PLayer
     ); Far;
    begin
      Inc(Cnt);
      if Cnt = LCount then LastLayer:=TRUE;
      DoCreateOffsetAndSelectOnLayer(Proj,AOffset,Item,LastLayer);
    end;
  Procedure CopyPolyPoints
     (
     Item          : PDPoint
     ); Far;
    begin
      SelPoly^.InsertPoint(Item^);
    end;
  Procedure CopyLinePoints
     (
     Item          : PDPoint
     ); Far;
    begin
      SelLine^.InsertPoint(Item^);
    end;
  begin
    {$IFNDEF WMLT}
    Proj:=PProj(AData);
    SelPoly:=NIL;
    SelLine:=NIL;
    LCount:=0;
    LastLayer:=FALSE;
    Proj^.PInfo^.SelectionRect.Init;
    case SType of
      SelTypePoint    : begin
          case SDrawSel of
              DrawSelDrawRange,
              DrawSelGetRange        : Proj^.SelectByPoint(Proj^.SelPoint,SSelObjType);
              DrawSelSelRange        : begin
                  Proj^.SelectMask:=ot_Any;
                  Proj^.SelectByPoint(Proj^.SelPoint,SSelObjType);
                  if SelItem <> NIL then begin
                    if SObjLayer^.Data^.Search(SelItem,ObjIndex) then begin
                      AItem:=SObjLayer^.Data^.At(ObjIndex);
                      if SSelFirstObj then begin
                        if not AItem^.GetState(sf_Selected) then SObjLayer^.Select(Proj^.PInfo,AItem);
                      end
                      else begin
                        if AItem^.GetState(sf_Selected) then SObjLayer^.Deselect(Proj^.PInfo,AItem);
                      end
                    end;
                  end;
                end;
              DrawSelGetObjOnLayer,
              DrawSelGetObjAllLayers : begin
                  Proj^.SelectByPoint(Proj^.SelPoint,SSelObjType);
                  if SelItem <> NIL then begin
                    if SObjLayer^.Data^.Search(SelItem,ObjIndex) then begin
                      AItem:=SObjLayer^.Data^.At(ObjIndex);
                      if SSelFirstObj then begin
                        if not AItem^.GetState(sf_Selected) then SObjLayer^.Select(Proj^.PInfo,AItem);
                      end
                      else begin
                        if AItem^.GetState(sf_Selected) then SObjLayer^.Deselect(Proj^.PInfo,AItem);
                      end
                    end;
                  end;
                end;
          end;
        end;
      SelTypeArea     : begin
          case SDrawSel of
              DrawSelDrawRange       : begin
                  SelPoly:=New(PCPoly,Init);
                  Proj^.NewPoly^.GetPoly(Proj^.PInfo,SelPoly,TRUE);
                  SSelObjLayer^.ForEach(@SelByPoly);
                end;
              DrawSelSelRange        : begin
                  Proj^.SelectMask:=ot_Any;
                  SelPoly:=New(PCPoly,Init);
                  PCPoly(SelItem)^.Data^.ForEach(@CopyPolyPoints);
                  SSelObjLayer^.ForEach(@SelByPoly);
                  if SelItem <> NIL then begin
                    if SObjLayer^.Data^.Search(SelItem,ObjIndex) then begin
                      AItem:=SObjLayer^.Data^.At(ObjIndex);
                      if SSelFirstObj then begin
                        if not AItem^.GetState(sf_Selected) then SObjLayer^.Select(Proj^.PInfo,AItem);
                      end
                      else begin
                        if AItem^.GetState(sf_Selected) then SObjLayer^.Deselect(Proj^.PInfo,AItem);
                      end
                    end;
                  end;
                end;
              DrawSelGetRange        : begin
                  SelPoly:=New(PCPoly,Init);
                  Proj^.NewPoly^.CheckCopyData(Proj^.PInfo,SelPoly,TRUE);
                  SSelObjLayer^.ForEach(@SelByPoly);
                end;
              DrawSelGetObjOnLayer,
              DrawSelGetObjAllLayers : begin
                  SelPoly:=New(PCPoly,Init);
                  AView:=Proj^.Layers^.IndexObject(Proj^.PInfo,SelItem);
                  PCPoly(AView)^.Data^.ForEach(@CopyPolyPoints);
                  SSelObjLayer^.ForEach(@SelByPoly);
                  if SelItem <> NIL then begin
                    if SObjLayer^.Data^.Search(SelItem,ObjIndex) then begin
                      AItem:=SObjLayer^.Data^.At(ObjIndex);
                      if SSelFirstObj then begin
                        if not AItem^.GetState(sf_Selected) then SObjLayer^.Select(Proj^.PInfo,AItem);
                      end
                      else begin
                        if AItem^.GetState(sf_Selected) then SObjLayer^.Deselect(Proj^.PInfo,AItem);
                      end
                    end;
                  end;
                end;
            end;
        end;
      SelTypeCircle   : begin
          case SDrawSel of
              DrawSelDrawRange       : begin
                  Proj^.SelCircle^.GetCircle(Center,Radius);
                  SSelObjLayer^.ForEach(@SelByCircle);
                end;
              DrawSelSelRange        : begin
                  Proj^.SelectMask:=ot_Any;
                  Center.Init(PEllipse(SelItem)^.Position.X,PEllipse(SelItem)^.Position.Y);
                  Radius:=PEllipse(SelItem)^.PrimaryAxis;
                  SSelObjLayer^.ForEach(@SelByCircle);
                  if SelItem <> NIL then begin
                    if SObjLayer^.Data^.Search(SelItem,ObjIndex) then begin
                      AItem:=SObjLayer^.Data^.At(ObjIndex);
                      if SSelFirstObj then begin
                        if not AItem^.GetState(sf_Selected) then SObjLayer^.Select(Proj^.PInfo,AItem);
                      end
                      else begin
                        if AItem^.GetState(sf_Selected) then SObjLayer^.Deselect(Proj^.PInfo,AItem);
                      end
                    end;
                  end;
                end;
              DrawSelGetRange        : begin
                  Center.Init(CircleCenter.X,CircleCenter.Y);
                  Radius:=CircleRadius;
                  SSelObjLayer^.ForEach(@SelByCircle);
                end;
              DrawSelGetObjOnLayer,
              DrawSelGetObjAllLayers : begin
                  Center.Init(PEllipse(SelItem)^.Position.X,PEllipse(SelItem)^.Position.Y);
                  Radius:=PEllipse(SelItem)^.PrimaryAxis;
                  SSelObjLayer^.ForEach(@SelByCircle);
                  if SelItem <> NIL then begin
                    if SObjLayer^.Data^.Search(SelItem,ObjIndex) then begin
                      AItem:=SObjLayer^.Data^.At(ObjIndex);
                      if SSelFirstObj then begin
                        if not AItem^.GetState(sf_Selected) then SObjLayer^.Select(Proj^.PInfo,AItem);
                      end
                      else begin
                        if AItem^.GetState(sf_Selected) then SObjLayer^.Deselect(Proj^.PInfo,AItem);
                      end
                    end;
                  end;
                end;
            end;
        end;
      SelTypeRect     : begin
          case SDrawSel of
              DrawSelDrawRange       : begin
                  Proj^.SelRect^.GetRect(Rect);
                  SelPoly:=New(PCPoly,Init);
                  Center.X:=Rect.A.X;
                  Center.Y:=Rect.A.Y;
                  SelPoly^.InsertPoint(Center);
                  Center.Y:=Rect.B.Y;
                  SelPoly^.InsertPoint(Center);
                  Center.X:=Rect.B.X;
                  SelPoly^.InsertPoint(Center);
                  Center.Y:=Rect.A.Y;
                  SelPoly^.InsertPoint(Center);
                  Center.X:=Rect.A.X;
                  SelPoly^.InsertPoint(Center);
                  SSelObjLayer^.ForEach(@SelByPoly);
                end;
              DrawSelSelRange        : {nicht erlaubt};
              DrawSelGetRange        : begin
                  SelLine:=New(PPoly,Init);
                  Proj^.NewPoly^.CheckCopyData(Proj^.PInfo,SelLine,False);
                  SelPoly:=New(PCPoly,Init);
                  Center.X:=PDPoint(SelLine^.Data^.At(0))^.X;
                  Center.Y:=PDPoint(SelLine^.Data^.At(0))^.Y;
                  SelPoly^.InsertPoint(Center);
                  Center.Y:=PDPoint(SelLine^.Data^.At(1))^.Y;
                  SelPoly^.InsertPoint(Center);
                  Center.X:=PDPoint(SelLine^.Data^.At(1))^.X;
                  SelPoly^.InsertPoint(Center);
                  Center.Y:=PDPoint(SelLine^.Data^.At(0))^.Y;
                  SelPoly^.InsertPoint(Center);
                  Center.X:=PDPoint(SelLine^.Data^.At(0))^.X;
                  SelPoly^.InsertPoint(Center);
                  Dispose(SelLine,Done);
                  SelLine:=nil;
                  SSelObjLayer^.ForEach(@SelByPoly);
                end;
              DrawSelGetObjOnLayer   : {nicht erlaubt};
              DrawSelGetObjAllLayers : {nicht erlaubt};
            end;
        end;
      SelTypePoly     : begin
          case SDrawSel of
              DrawSelDrawRange       : begin
                  SelLine:=New(PPoly,Init);
                  Proj^.NewPoly^.GetPoly(Proj^.PInfo,SelLine,TRUE);
                  SSelObjLayer^.ForEach(@SelByPolyLine);
                end;
              DrawSelSelRange        : begin
                  Proj^.SelectMask:=ot_Any;
                  SelLine:=New(PPoly,Init);
                  PPoly(SelItem)^.Data^.ForEach(@CopyLinePoints);
                  SSelObjLayer^.ForEach(@SelByPolyLine);
                  if SelItem <> NIL then begin
                    if SObjLayer^.Data^.Search(SelItem,ObjIndex) then begin
                      AItem:=SObjLayer^.Data^.At(ObjIndex);
                      if SSelFirstObj then begin
                        if not AItem^.GetState(sf_Selected) then SObjLayer^.Select(Proj^.PInfo,AItem);
                      end
                      else begin
                        if AItem^.GetState(sf_Selected) then SObjLayer^.Deselect(Proj^.PInfo,AItem);
                      end
                    end;
                  end;
                end;
              DrawSelGetRange        : begin
                  SelLine:=New(PPoly,Init);
                  Proj^.NewPoly^.CheckCopyData(Proj^.PInfo,SelLine,TRUE);
                  SSelObjLayer^.ForEach(@SelByPolyLine);
                end;
              DrawSelGetObjOnLayer,
              DrawSelGetObjAllLayers : begin
                  SelLine:=New(PPoly,Init);
                  PPoly(SelItem)^.Data^.ForEach(@CopyLinePoints);
                  SSelObjLayer^.ForEach(@SelByPolyLine);
                  if SelItem <> NIL then begin
                    if SObjLayer^.Data^.Search(SelItem,ObjIndex) then begin
                      AItem:=SObjLayer^.Data^.At(ObjIndex);
                      if SSelFirstObj then begin
                        if not AItem^.GetState(sf_Selected) then SObjLayer^.Select(Proj^.PInfo,AItem);
                      end
                      else begin
                        if AItem^.GetState(sf_Selected) then SObjLayer^.Deselect(Proj^.PInfo,AItem);
                      end
                    end;
                  end;
                end;
            end;
        end;
      SelTypeOffset   : begin
          case SDrawSel of
              DrawSelDrawRange       : begin
                  AOffset:=SOffset;
                  if CorrectDir and (Proj^.OffsetItem^.GetObjType = ot_CPoly) then begin
                    OArea:=0.0;
                    with Proj^.OffsetItem^ do for Cnt:=1 to Data^.Count-1 do
                      OArea:=OArea+((0.0+PDPoint(Data^.At(Cnt))^.X+PDPoint(Data^.At(Cnt-1))^.X)*
                             (0.0+PDPoint(Data^.At(Cnt))^.Y-PDPoint(Data^.At(Cnt-1))^.Y));
                    if OArea < 0 then AOffset:=-AOffset;
                  end;
                  Cnt:=0;
                  LCount:=SSelObjLayer^.Count;
                  SSelObjLayer^.ForEach(@SelByOffset);
                end;
              DrawSelSelRange        : begin
                  Proj^.SelectMask:=ot_Any;
                  AOffset:=SOffset;
                  if CorrectDir and (Proj^.OffsetItem^.GetObjType = ot_CPoly) then begin
                    OArea:=0.0;
                    with Proj^.OffsetItem^ do for Cnt:=1 to Data^.Count-1 do
                      OArea:=OArea+((0.0+PDPoint(Data^.At(Cnt))^.X+PDPoint(Data^.At(Cnt-1))^.X)*
                             (0.0+PDPoint(Data^.At(Cnt))^.Y-PDPoint(Data^.At(Cnt-1))^.Y));
                    if OArea < 0 then AOffset:=-AOffset;
                  end;
                  Cnt:=0;
                  LCount:=SSelObjLayer^.Count;
                  SSelObjLayer^.ForEach(@SelByOffset);
                  if SelItem <> NIL then begin
                    if SObjLayer^.Data^.Search(SelItem,ObjIndex) then begin
                      AItem:=SObjLayer^.Data^.At(ObjIndex);
                      if SSelFirstObj then begin
                        if not AItem^.GetState(sf_Selected) then SObjLayer^.Select(Proj^.PInfo,AItem);
                      end
                      else begin
                        if AItem^.GetState(sf_Selected) then SObjLayer^.Deselect(Proj^.PInfo,AItem);
                      end
                    end;
                  end;
                end;
              DrawSelGetRange        : begin
                  AOffset:=SOffset;
                  if CorrectDir and (SelOffsetLine^.GetObjType = ot_CPoly) then begin
                    OArea:=0.0;
                    with Proj^.OffsetItem^ do for Cnt:=1 to Data^.Count-1 do
                      OArea:=OArea+((0.0+PDPoint(Data^.At(Cnt))^.X+PDPoint(Data^.At(Cnt-1))^.X)*
                             (0.0+PDPoint(Data^.At(Cnt))^.Y-PDPoint(Data^.At(Cnt-1))^.Y));
                    if OArea < 0 then AOffset:=-AOffset;
                  end;
                  Cnt:=0;
                  LCount:=SSelObjLayer^.Count;
                  SSelObjLayer^.ForEach(@SelByOffset);
                end;
              DrawSelGetObjOnLayer,
              DrawSelGetObjAllLayers : begin
                  AOffset:=SOffset;
                  if CorrectDir and (Proj^.OffsetItem^.GetObjType = ot_CPoly) then begin
                    OArea:=0.0;
                    with Proj^.OffsetItem^ do for Cnt:=1 to Data^.Count-1 do
                      OArea:=OArea+((0.0+PDPoint(Data^.At(Cnt))^.X+PDPoint(Data^.At(Cnt-1))^.X)*
                             (0.0+PDPoint(Data^.At(Cnt))^.Y-PDPoint(Data^.At(Cnt-1))^.Y));
                    if OArea < 0 then AOffset:=-AOffset;
                  end;
                  Cnt:=0;
                  LCount:=SSelObjLayer^.Count;
                  SSelObjLayer^.ForEach(@SelByOffset);
                  if SelItem <> NIL then begin
                    if SObjLayer^.Data^.Search(SelItem,ObjIndex) then begin
                      AItem:=SObjLayer^.Data^.At(ObjIndex);
                      if SSelFirstObj then begin
                        if not AItem^.GetState(sf_Selected) then SObjLayer^.Select(Proj^.PInfo,AItem);
                      end
                      else begin
                        if AItem^.GetState(sf_Selected) then SObjLayer^.Deselect(Proj^.PInfo,AItem);
                      end
                    end;
                  end;
                end;
            end;
        end;
      SelTypeWithArea : begin
          case SDrawSel of
              DrawSelDrawRange       : {nicht erlaubt};
              DrawSelSelRange        : begin
                  Proj^.SelectMask:=ot_Any;
                  SelPoly:=New(PCPoly,Init);
                  PCPoly(SelItem)^.Data^.ForEach(@CopyPolyPoints);
                  SSelObjLayer^.ForEach(@SelByPoly);
                  if SelItem <> NIL then begin
                    if SObjLayer^.Data^.Search(SelItem,ObjIndex) then begin
                      AItem:=SObjLayer^.Data^.At(ObjIndex);
                      if SSelFirstObj then begin
                        if not AItem^.GetState(sf_Selected) then SObjLayer^.Select(Proj^.PInfo,AItem);
                      end
                      else begin
                        if AItem^.GetState(sf_Selected) then SObjLayer^.Deselect(Proj^.PInfo,AItem);
                      end
                    end;
                  end;
                end;
              DrawSelGetRange        : {nicht erlaubt};
              DrawSelGetObjOnLayer,
              DrawSelGetObjAllLayers : begin
                  SelPoly:=New(PCPoly,Init);
                  PCPoly(SelItem)^.Data^.ForEach(@CopyPolyPoints);
                  SSelObjLayer^.ForEach(@SelByPoly);
                  if SelItem <> NIL then begin
                    if SObjLayer^.Data^.Search(SelItem,ObjIndex) then begin
                      AItem:=SObjLayer^.Data^.At(ObjIndex);
                      if SSelFirstObj then begin
                        if not AItem^.GetState(sf_Selected) then SObjLayer^.Select(Proj^.PInfo,AItem);
                      end
                      else begin
                        if AItem^.GetState(sf_Selected) then SObjLayer^.Deselect(Proj^.PInfo,AItem);
                      end
                    end;
                  end;
                end;
            end;
        end;
    end;
    if SDrawSelLine and (SSelLineLayer <> NIL) then begin
      case SType of
        SelTypePoint    : begin
                            NewPoint:=New(PPixel,Init(Proj^.SelPoint));
                            Proj^.InsertObjectOnLayer(NewPoint,SSelLineLayer^.Index);
                          end;
        SelTypeArea     : Proj^.InsertObjectOnLayer(SelPoly,SSelLineLayer^.Index);
        SelTypeCircle   : begin
                            SelCircle:=New(PEllipse,Init(Center,Radius,Radius,0));
                            Proj^.InsertObjectOnLayer(SelCircle,SSelLineLayer^.Index);
                          end;
        SelTypeRect     : Proj^.InsertObjectOnLayer(SelPoly,SSelLineLayer^.Index);
        SelTypePoly     : Proj^.InsertObjectOnLayer(SelLine,SSelLineLayer^.Index);
        SelTypeOffset   : begin
                            if OffsetPoly1 <> NIL then begin
                              Island1:=PolyToIslandStruct(Proj^.PInfo,PCPoly(OffsetPoly));
                              Island2:=PolyToIslandStruct(Proj^.PInfo,PCPoly(OffsetPoly1));
                              Island1:=InsertIsland(Proj^.PInfo,Island1,Island2);
                              OffPoly:=IslandStructToPoly(Island1,TRUE);
                              if OffPoly <> NIL then Proj^.InsertObjectOnLayer(OffPoly,SSelLineLayer^.Index);
                              DestroyIslandStruct(Island1,TRUE);
                              Dispose(OffsetPoly,Done);
                              OffsetPoly:=NIL;
                              Dispose(OffsetPoly1,Done);
                              OffsetPoly1:=NIL;
                            end
                            else if OffsetPoly <> NIL then begin
                              Proj^.InsertObjectOnLayer(OffsetPoly,SSelLineLayer^.Index);
                              OffsetPoly:=NIL;
                            end;
                          end;
        SelTypeWithArea : Proj^.InsertObjectOnLayer(SelPoly,SSelLineLayer^.Index);
      end;
    end
    else begin
      if SelPoly <> NIL then Dispose(SelPoly,Done);
      if SelLine <> NIL then Dispose(SelLine,Done);
      if OffsetPoly <> NIL then Dispose(OffsetPoly,Done);
      OffsetPoly:=NIL;
      if OffsetPoly1 <> NIL then Dispose(OffsetPoly1,Done);
      OffsetPoly1:=NIL;
    end;
    if SZoom then ProcShowAllSel(AData);
    EndDBSelection(FALSE,TRUE);
    {$ENDIF}
  end;

Procedure TDBSelection.EndDBSelection
   (
   SendErr         : Boolean;
   SendIDs         : Boolean
   );
{$IFNDEF WMLT}
  var Proj         : PProj;
  Procedure WriteAllTSLayers
     (
     Item          : PLayer
     ); Far;
    begin
      Item^.SetState(sf_DoSelect,FALSE);
    end;
  Procedure ResetTSLayers
     (
     Item          : PLayer
     ); Far;
    begin
      Item^.SetState(sf_DoSelect,TRUE);
    end;
{$ENDIF}
  begin
{$IFNDEF WMLT}
    Proj:=PProj(AData);
    EndingSelection:=TRUE;
    Proj^.NewPoly^.EndIt(Proj^.PInfo);
    if ResetL then begin
      Proj^.Layers^.LData^.ForEach(@WriteAllTSLayers);
      OldTSLayers^.ForEach(@ResetTSLayers);
    end;
    ClearOldTSLayers;
    ClearSSelBorder;
    ClearSSelObjLayer;
    Selecting:=FALSE;
    if ResetTM then begin
      Proj^.Layers^.TranspLayer:=OldTransp;
      Proj^.SetActualMenu(OldMen);
    end;
    Proj^.PInfo^.SelectionMode:=OldSelState;
    EndingSelection:=FALSE;
    Proj^.CorrectSelAllRect(Proj^.PInfo^.SelectionRect);
    Proj^.PInfo^.RedrawRect(Proj^.PInfo^.SelectionRect);
    UserInterface.Update([uiMenus,uiStatusBar]);
    if SendErr then DDEHandler.SendString('[SER][99]');
    if SendIDs then begin
      Proj^.Layers^.TranspLayer:=TRUE;
      if SSendIDs then begin
         ProcDBSendCommand(AData,'OBS');
      end;
      Proj^.Layers^.TranspLayer:=OldTransp;
    end;
{$ENDIF}
  end;

{$ENDIF} // <----------------- AXDLL
end.

