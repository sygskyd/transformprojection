{*******************************************************************************
I Unit Legende                                                                 I
+------------------------------------------------------------------------------+
I Autor : Raimund Leitner                                                      I
+------------------------------------------------------------------------------+
I Legendenverwaltung und -erstellung in WinGIS                                 I
*******************************************************************************}
unit Legend;

{$H+ use dynamic strings by Delphi}

interface

uses Classes, Windows, Graphics, AM_Layer, SysUtils,
  AM_Def, AM_Paint, NumTools, GrTools, Objects, ExtCanvas, AM_Admin, AM_Group,
  AM_Obj, AM_Sym, StyleDef, RegDB,
{++ Moskaliov BUG#268 BUILD#108 29.03.00}
  Messages, Controls, Forms, Dialogs, MultiLng, StdCtrls;
{-- Moskaliov BUG#268 BUILD#108 29.03.00}

const
  LegendListVersion = 1;
  LegendVersion = 1;
  LegendEntryVersion = 1;
//++ Glukhov Bug#572 Build#166 17.10.01
  // The legend font should be True Type (not Roman)
//  LegendFont         = 'Roman';
//  LegendFont         = 'Arial';
  LegendFont = 'Times New Roman';
//-- Glukhov Bug#572 Build#166 17.10.01
//++ Glukhov Bug#293(PrintFrame) Build#175 09.02.02
const
  // This name is used to distinguish the frames from the normal legends
  // in the print dialog. The normal legend should not have such name
  csLegName4Frame = 'csLegName4FrameInPrnDlg';
  csOleFrame = '{!OleFrame}';
//-- Glukhov Bug#293(PrintFrame) Build#175 09.02.02

      // legend shape constants (relative to Line Height)
      // Width of layer sample
  SampleWidth = 1.3;
      // Height of layer sample
  SampleHeight = 0.8;
      // Space to Text
  TextSpace = 0.2;

type
  // different types of legend entries
//  TLegendTypeSet = (leNone,leHorzLine,leLine,leFilledRect,leRectangle,leSymbol);
  TLegendTypeSet = (leNone, leLine, leFilledRect, leSymbol);

  TBitmapNr = (biVisible, biRectangle, biFilledRect, biHorzLine,
    biLine, biNone, biSymbol, biQuestion);

  // class for one legend entry
  TLegendEntry = class
    FLayerNumber: LongInt;
    FText: string;
    FType: TLegendTypeSet;
    FSymName: string;
    FLibName: string;
    FNameFromLayer: Boolean;
    constructor Create;
    constructor Load(S: TOldStream);
    constructor ReadFromRegistry(aRegDB: TRegistryDataBase; Where: string);
    procedure Store(S: TOldStream);
    procedure WriteToRegistry(aRegDB: TRegistryDataBase; Where: string);
    procedure SetToDefaults;
    function Copy: TLegendEntry;
    procedure ToggleType;
  end;

  // one legend itself
  TLegend = class
  private
    FName: string;
    FTitle: string;
    FList: TList; // of TLegendEntry
    FSorted: Boolean; // alphabetical sort order
    FTitleStyle: TTextProperties;
    FEntryStyle: TTextProperties;
//++ Glukhov Bug#458 Build#166 10.11.01
    dScaleVal: Double;
    FScBarStyle: TTextProperties;
//-- Glukhov Bug#458 Build#166 10.11.01
    function GetLegendEntry(Pos: Integer): TLegendEntry;
    procedure SetLegendEntry(Pos: Integer; Entry: TLegendEntry);
    function GetCount: Integer;
    procedure DrawLine(PInfo: pPaint; X: Double; Y: Double; HLine: Double;
      Horizontal: Boolean);
    procedure DrawSymbol(PInfo: pPaint; X: Double; Y: Double; HLine: Double;
      Symbol: pSGroup);
    procedure DrawArea(PInfo: pPaint; X: Double; Y: Double; HLine: Double;
      Filled: Boolean);
    procedure DrawEntry(PInfo: pPaint; X: Double; Y: Double; HLine: Double;
      WLine: Double; Entry: TLegendEntry; aLayer: pLayer);
    procedure DrawBackground(pInfo: pPaint; Rect: TDRect);
    procedure DrawTitle(pInfo: pPaint; Rect: TDRect; Title: string);
//++ Glukhov Bug#458 Build#166 10.11.01
    procedure DrawScBar(pInfo: pPaint; Rect: TDRect);
//-- Glukhov Bug#458 Build#166 10.11.01
    procedure SetSorted(aSorted: Boolean);
  public
//++ Glukhov Bug#458 Build#166 15.11.01
    bScaleBar: Boolean;
//-- Glukhov Bug#458 Build#166 15.11.01
    bTransparent: Boolean;
    constructor Create;
    destructor Destroy; override;
    constructor Load(S: TOldStream);
    constructor ReadFromRegistry(aRegDB: TRegistryDataBase; Where: string);
    procedure Store(S: TOldStream);
    procedure WriteToRegistry(aRegDB: TRegistryDataBase; Where: string);
    procedure WriteTextProperties(aRegDB: TRegistryDataBase; Where: string;
      TextProp: TTextProperties);
    procedure ReadTextProperties(aRegDB: TRegistryDataBase; Where: string;
      var TextProp: TTextProperties);
    procedure Add(Entry: TLegendEntry);
    procedure Insert(Index: Integer; Entry: TLegendEntry);
    procedure Delete(Index: Integer);
    function IndexOf(Entry: TLegendEntry): Integer;
    // draw legend inside the given Rect into given PInfo
    procedure Draw(PInfo: pPaint; Layers: pLayers; Rect: TDRect);
//++ Glukhov Bug#458 Build#166 15.11.01
    // Set the scale bar data to draw it, if necessary
    procedure SetScaleVal(dVal: Double);
//-- Glukhov Bug#458 Build#166 15.11.01
    // legend properties
    property Name: string read FName write FName;
    property Title: string read FTitle write FTitle;
    property Count: Integer read GetCount;
    property Items[Pos: Integer]: TLegendEntry read GetLegendEntry
    write SetLegendEntry; default;
    property Sorted: Boolean read FSorted write SetSorted;
    property TitleStyle: TTextProperties read FTitleStyle write FTitleStyle;
    property EntryStyle: TTextProperties read FEntryStyle write FEntryStyle;
//++ Glukhov Bug#458 Build#166 10.11.01
    property ScBarStyle: TTextProperties read FScBarStyle write FScBarStyle;
//-- Glukhov Bug#458 Build#166 10.11.01
  end;

  // list of legends in project
  TLegendList = class
  private
    FList: TList; // of TLegend
    FCurrent: Integer; // index of current legend
    function GetLegend(Pos: Integer): TLegend;
    procedure SetLegend(Pos: Integer; Legend: TLegend);
    function GetLegendByName(Name: string): TLegend;
    procedure SetLegendByName(Name: string; Legend: TLegend);
    function GetCount: Integer;
  public
    constructor Create;
    destructor Destroy; override;
    constructor Load(S: TOldStream);
    constructor ReadFromRegistry(aRegDB: TRegistryDataBase; Where: string);
    procedure Store(S: TOldStream);
    procedure WriteToRegistry(aRegDB: TRegistryDataBase; Where: string);
    function NewLegend(Name: string): TLegend;
    function AddLegend(aLegend: TLegend): Boolean;
    procedure CopyLegend(Dest: TLegend; Source: TLegend);
    function Clone: TLegendList;
    function DeleteLegend(Name: string): Boolean;
    property Count: Integer read GetCount;
    property Legends[Pos: Integer]: TLegend read GetLegend
    write SetLegend; default;
    property LegendByName[Name: string]: TLegend read GetLegendByName
    write SetLegendByName;
    property CurrentLegendIndex: Integer read FCurrent write FCurrent;
//    Property    CurrentLegendByName[Name:String]:TLegend read GetCurrentLegend
//                                                         write SetCurrentLegend;
  end;

{++ Moskaliov BUG#268 BUILD#108 29.03.00}
type
  TLegendPrintDlg = class(TForm)
    MlgSection: TMlgSection;
    LegendCombo: TComboBox;
    OkBtn: TButton;
    CancelBtn: TButton;
    constructor Create(AOwner: TComponent; Legends: TLegendList; TextEditEnable: Boolean); reintroduce;
    procedure LegendComboChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    FName: string;
  public
    property Name: string read FName;
  end;

{-- Moskaliov BUG#268 BUILD#108 29.03.00}

implementation

{$R *.DFM}

{*******************************************************************************
I Class TLegendEntry                                                                I
*******************************************************************************}

constructor TLegendEntry.Create;
begin
  inherited Create;
  SetToDefaults;
end;

procedure TLegendEntry.SetToDefaults;
begin
  FText := '';
  FType := leFilledRect;
  FSymName := '';
  FLibName := '';
  FNameFromLayer := False;
  FLayerNumber := -1;
end;

constructor TLegendEntry.Load
  (
  S: TOldStream
  );
var
  Version: Byte;
begin
  inherited Create;
  S.Read(Version, sizeof(Version));
  S.Read(FLayerNumber, sizeof(FLayerNumber));
  FText := S.ReadString;
  S.Read(FType, sizeof(FType));
  FSymName := S.ReadString;
end;

constructor TLegendEntry.ReadFromRegistry
  (
  aRegDB: TRegistryDataBase;
  Where: string
  );
begin
  inherited Create;
  SetToDefaults;
  with aRegDB do
  begin
    // if Where exists...
    if OpenKey(Where, False) then
    begin
      FNameFromLayer := ReadBool('NameFromLayer');
      FText := ReadString('EntryText');
      FType := TLegendTypeSet(ReadInteger('EntryType'));
      FLayerNumber := ReadInteger('LayerNumber');
      FSymName := ReadString('SymName');
      FLibName := ReadString('LibName');
    end;
  end;
end;

procedure TLegendEntry.Store
  (
  S: TOldStream
  );
var
  Version: Byte;
begin
  Version := LegendEntryVersion;
  S.Write(Version, sizeof(Version));
  S.Write(FLayerNumber, sizeof(FLayerNumber));
  S.WriteString(FText);
  S.Write(FType, sizeof(FType));
  S.WriteString(FSymName);
end;

procedure TLegendEntry.WriteToRegistry
  (
  aRegDB: TRegistryDatabase;
  Where: string
  );
begin
  with aRegDB do
  begin
    // write where function caller says...
    OpenKey(Where, True);
    WriteInteger('LayerNumber', FLayerNumber);
    WriteBool('NameFromLayer', FNameFromLayer);
    if FNameFromLayer then
      WriteString('EntryText', '')
    else
      WriteString('EntryText', FText);
    WriteInteger('EntryType', Ord(FType));
    WriteInteger('LayerNumber', FLayerNumber);
    WriteString('SymName', FSymName);
    WriteString('LibName', FLibName);
  end;
end;

function TLegendEntry.Copy: TLegendEntry;
begin
  Result := TLegendEntry.Create;
  Result.FlayerNumber := FLayerNumber;
  Result.FText := FText;
  Result.FType := FType;
  Result.FSymName := FSymName;
  Result.FLibName := FLibName;
  Result.FNameFromLayer := FNameFromLayer;
end;

procedure TLegendEntry.ToggleType;
var
  Nr: TLegendTypeSet;
begin
  Nr := FType;
  if Nr = leSymbol then
    Nr := leNone
  else
    Nr := Succ(Nr);
  FType := Nr;
end;

{*******************************************************************************
I Class TLegend                                                                I
*******************************************************************************}

function SortAlphabetic(Item1: Pointer; Item2: Pointer): Integer; far;
begin
  Result := AnsiCompareText(TLegendEntry(Item1).FText, TLegendEntry(Item2).FText);
end;

procedure InitStyle(aStyle: TTextProperties);
begin
  with aStyle do
  begin
    Height := 1;
    FixedHeight := 0;
//++ Glukhov Bug#572 Build#166 17.10.01
//    FontName:='Roman';
    FontName := LegendFont;
//-- Glukhov Bug#572 Build#166 17.10.01
    Bold := 0;
    Underlined := 0;
    Italic := 0;
    Transparent := 0;
    Alignment := taLeft;
  end;
end;

constructor TLegend.Create;
begin
  inherited Create;
  FList := TList.Create;
  FSorted := False;
  FTitleStyle := TTextProperties.Create;
  FEntryStyle := TTextProperties.Create;
  InitStyle(FTitleStyle);
  InitStyle(FEntryStyle);
//++ Glukhov Bug#458 Build#166 10.11.01
  bScaleBar := False;
  bTransparent := True;
  dScaleVal := 0;
  FScBarStyle := TTextProperties.Create;
  InitStyle(FScBarStyle);
//-- Glukhov Bug#458 Build#166 10.11.01
end;

destructor TLegend.Destroy;
var
  Cnt: Integer;
begin
  for Cnt := 0 to FList.Count - 1 do
    TLegendEntry(FList[Cnt]).Free;
  FList.Free;
  FTitleStyle.Free;
  FEntryStyle.Free;
//++ Glukhov Bug#458 Build#166 10.11.01
  FScBarStyle.Free;
//-- Glukhov Bug#458 Build#166 10.11.01
  inherited Destroy;
end;

procedure TLegend.Add
  (
  Entry: TLegendEntry
  );
{++ Moskaliov BUG#268 BUILD#108 29.03.00}
//var Index : Integer;
{-- Moskaliov BUG#268 BUILD#108 29.03.00}
begin
  FList.Add(Entry);
  if FSorted then
    FList.Sort(SortAlphabetic);
end;

procedure TLegend.Insert
  (
  Index: Integer;
  Entry: TLegendEntry
  );
begin
  FList.Insert(Index, Entry);
  if FSorted then
    FList.Sort(SortAlphabetic);
end;

procedure TLegend.Delete
  (
  Index: Integer
  );
begin
  FList.Delete(Index);
end;

function TLegend.IndexOf
  (
  Entry: TLegendEntry
  ): Integer;
begin
  Result := FList.IndexOf(Entry);
end;

function TLegend.GetLegendEntry
  (
  Pos: Integer
  ): TLegendEntry;
begin
  Result := TLegendEntry(FList.Items[Pos]);
end;

procedure TLegend.SetLegendEntry
  (
  Pos: Integer;
  Entry: TLegendEntry
  );
begin
  FList.Items[Pos] := Entry;
  if FSorted then
    FList.Sort(SortAlphabetic);
end;

function TLegend.GetCount: Integer;
begin
  Result := FList.Count;
end;

procedure TLegend.SetSorted(aSorted: Boolean);
begin
  if aSorted then
  begin
    FSorted := True;
    FList.Sort(SortAlphabetic);
  end
  else
    FSorted := False;
end;

procedure TLegend.Store
  (
  S: TOldStream
  );
var
  i: Integer;
  Version: Byte;
  Count: Integer;
begin
  Version := LegendVersion;
  S.Write(Version, sizeof(Version));
  S.WriteString(FName);
  S.WriteString(FTitle);
  Count := FList.Count;
  S.Write(Count, sizeof(Count));
  for i := 0 to Count - 1 do
    Items[i].Store(S);
end;

procedure TLegend.WriteTextProperties
  (
  aRegDB: TRegistryDataBase;
  Where: string;
  TextProp: TTextProperties
  );
begin
  with aRegDB, TextProp do
  begin
    OpenKey(Where, True);
    WriteString('FontName', FontName);
    WriteInteger('Alignment', Alignment);
    WriteFloat('Height', Height);
    WriteBool('FixedHeight', FixedHeight = 1);
    WriteBool('Italic', Italic = 1);
    WriteBool('Bold', Bold = 1);
    WriteBool('Underlined', Underlined = 1);
    WriteBool('Transparent', Transparent = 1);
  end;
end;

procedure TLegend.ReadTextProperties
  (
  aRegDB: TRegistryDataBase;
  Where: string;
  var TextProp: TTextProperties
  );
begin
  with aRegDB, TextProp do
  begin
    if OpenKey(Where, False) then
    begin
      FontName := ReadString('FontName');
//++ Glukhov Bug#572 Build#166 17.10.01
      if FontName = 'Roman' then
        FontName := LegendFont;
//-- Glukhov Bug#572 Build#166 17.10.01
      Alignment := ReadInteger('Alignment');
      Height := ReadFloat('Height');
      FixedHeight := ShortInt(ReadBool('FixedHeight'));
      Italic := ShortInt(ReadBool('Italic'));
      Bold := ShortInt(ReadBool('Bold'));
      Underlined := ShortInt(ReadBool('Underlined'));
      Transparent := ShortInt(ReadBool('Transparent'));
    end;
  end;
end;

procedure TLegend.WriteToRegistry
  (
  aRegDB: TRegistryDatabase;
  Where: string
  );
var
  i: Integer;
  iStr: string;
begin
  with aRegDB do
  begin
    OpenKey(Where, True);
    WriteString('LegendName', FName);
    WriteString('LegendTitle', FTitle);
    WriteBool('Sorted', FSorted);
    WriteTextProperties(aRegDB, Where + '\TitleStyle', FTitleStyle);
    WriteTextProperties(aRegDB, Where + '\EntryStyle', FEntryStyle);
//++ Glukhov Bug#458 Build#166 10.11.01
    WriteBool('bScaleBar', bScaleBar);
    WriteTextProperties(aRegDB, Where + '\ScBarStyle', FScBarStyle);
//-- Glukhov Bug#458 Build#166 10.11.01
    WriteBool('bTransparent', bTransparent);
    WriteInteger('EntryCount', FList.Count);
    // write each entry into a key like EntryXXX, where XXX is an ascending integer
    for i := 0 to Count - 1 do
    begin
      Str(i, iStr);
      Items[i].WriteToRegistry(aRegDB, Where + '\Entry' + iStr);
    end;
  end;
end;

constructor TLegend.Load
  (
  S: TOldStream
  );
var
  Version: Byte;
  i, Count: Integer;
begin
  inherited Create;
  FList := TList.Create;
  S.Read(Version, sizeof(Version));
  FName := S.ReadString;
  FTitle := S.ReadString;
  S.Read(Count, sizeof(Count));
  for i := 0 to Count - 1 do
    Add(TLegendEntry.Load(S));
end;

constructor TLegend.ReadFromRegistry
  (
  aRegDB: TRegistryDataBase;
  Where: string
  );
var
  i,
    Cnt: Integer;
  iStr: string;
begin
  inherited Create;
  FList := TList.Create;
  FSorted := False;
  FTitleStyle := TTextProperties.Create;
  FEntryStyle := TTextProperties.Create;
  InitStyle(FTitleStyle);
  InitStyle(FEntryStyle);
//++ Glukhov Bug#458 Build#166 10.11.01
  bScaleBar := False;
  dScaleVal := 0;
  FScBarStyle := TTextProperties.Create;
  InitStyle(FScBarStyle);
//-- Glukhov Bug#458 Build#166 10.11.01
  bTransparent := True;
  with aRegDB do
  begin
    // if entry exists...
    if OpenKey(Where, False) then
    begin
      FName := ReadString('LegendName');
      FTitle := ReadString('LegendTitle');
      FSorted := ReadBool('Sorted');
      ReadTextProperties(aRegDB, Where + '\TitleStyle', FTitleStyle);
      ReadTextProperties(aRegDB, Where + '\EntryStyle', FEntryStyle);
//++ Glukhov Bug#458 Build#166 10.11.01
      if aRegDB.ValueExists('bScaleBar') then
        bScaleBar := ReadBool('bScaleBar');
      ReadTextProperties(aRegDB, Where + '\ScBarStyle', FScBarStyle);
//-- Glukhov Bug#458 Build#166 10.11.01
      if aRegDB.ValueExists('bTransparent') then
        bTransparent := ReadBool('bTransparent');
      Cnt := ReadInteger('EntryCount');
      // read each entry (scheme EntryXXX, where XXX is an ascending integer)
      for i := 0 to Cnt - 1 do
      begin
        Str(i, iStr);
        FList.Add(TLegendEntry.ReadFromRegistry(aRegDB, Where + '\Entry' + iStr));
      end;
    end;
  end;
end;

procedure TLegend.DrawSymbol
  (
  PInfo: pPaint;
  X: Double;
  Y: Double;
  HLine: Double;
  Symbol: pSGroup
  );
{++ Moskaliov BUG#268 BUILD#108 29.03.00}
//var aRect     : TRect;
//    aSym      : pSymbol;
var
  aSym: pSymbol;
{-- Moskaliov BUG#268 BUILD#108 29.03.00}
  DrawPos: TDPoint;
  Ofx, Ofy: Integer;
  SymSize: Double;
{++ Moskaliov BUG#268 BUILD#108 29.03.00}
//    dummy     : Real;
{-- Moskaliov BUG#268 BUILD#108 29.03.00}
begin
  if Symbol = nil then
    Exit;
  // get middle of place for symbol
  X := X - SampleWidth / 2 * HLine;
  // recalculate drawing position for the symbol drawing routines
  PInfo^.ConvertToDraw(Point(LimitToInt(X), LimitToInt(Y)), DrawPos);
  // calculate symbol size;
  SymSize := HLine / PInfo^.Zoom;
    // make new symbol
  aSym := New(pSymbol, InitName(PInfo, DrawPos, Symbol^.GetName,
    Symbol^.GetLibName, SymSize, stDrawingUnits, 0));
  try
    with aSym^ do
    begin
//++ Glukhov Bug#400 Build#161 28.05.01
// Commented the incorrect operations
      // correct the size exactly...
      //Size:=SymSize*HLine/100/Max(ClipRect.XSize,ClipRect.YSize)*100000;
//-- Glukhov Bug#400 Build#161 28.05.01
      // align the symbol right in the middle (not bothering any displacement of original symbol)
      CalculateClipRect(PInfo);
      Ofx := Round(DrawPos.X - (ClipRect.A.X + ClipRect.B.X) / 2);
      Ofy := Round(DrawPos.Y - (ClipRect.A.Y + ClipRect.B.Y) / 2);
      MoveRel(Ofx, Ofy);
    end;
    aSym^.Draw(PInfo, true);
  finally
    aSym.Free;
  end;
end;

procedure TLegend.DrawLine
  (
  PInfo: pPaint;
  X: Double;
  Y: Double;
  HLine: Double;
  Horizontal: Boolean
  );
var
  Points: array[0..1] of TPoint;
begin
  if Horizontal then
  begin
    Points[0].X := LimitToLong(X);
    Points[0].Y := LimitToLong(Y);
    Points[1].X := LimitToLong(X - SampleWidth * HLine);
    Points[1].Y := LimitToLong(Y);
  end
  else
  begin
    Points[0].X := LimitToLong(X);
    Points[0].Y := LimitToLong(Y + SampleHeight / 2 * HLine);
    Points[1].X := LimitToLong(X - SampleWidth * HLine);
    Points[1].Y := LimitToLong(Y - SampleHeight / 2 * HLine);
  end;
  with PInfo^.ExtCanvas do
    PolyLine(Points, 2, False);
end;

procedure TLegend.DrawArea
  (
  PInfo: pPaint;
  X: Double;
  Y: Double;
  HLine: Double;
  Filled: Boolean
  );
var
  Points: array[0..4] of TPoint;
begin
  Points[0].X := LimitToLong(X - SampleWidth * HLine);
  Points[0].Y := LimitToLong(Y + SampleHeight / 2 * HLine);
  Points[1].X := LimitToLong(X - SampleWidth * HLine);
  Points[1].Y := LimitToLong(Y - SampleHeight / 2 * HLine);
  Points[2].X := LimitToLong(X);
  Points[2].Y := LimitToLong(Y - SampleHeight / 2 * HLine);
  Points[3].X := LimitToLong(X);
  Points[3].Y := LimitToLong(Y + SampleHeight / 2 * HLine);
  Points[4].X := LimitToLong(X - SampleWidth * HLine);
  Points[4].Y := LimitToLong(Y + SampleHeight / 2 * HLine);
  with PInfo^.ExtCanvas do
    Polygon(Points, 5, False);
end;

procedure TLegend.DrawEntry
  (
  PInfo: pPaint;
  X: Double;
  Y: Double;
  HLine: Double;
  WLine: Double;
  Entry: TLegendEntry;
  aLayer: pLayer
  );
var
  TextPos: TGrPoint;
  hSymbols: pSymbols;

  procedure SetTextStyle;
  begin
    // setup font
    with PInfo^, ExtCanvas, FEntryStyle do
    begin
      Font.Angle := 0;
      Font.Name := FEntryStyle.FontName;
      Font.Size := LimitToInt(HLine);
      Font.Color := clBlack;
      Font.Style := Italic + (Bold shl 1) + (Underlined shl 2);
      Font.Alignment := TTextAlignment(Alignment);
      Brush.Style := ptSolid;
      // Text transparent ?     TBrushStyle
      if Transparent = 1 then
        Brush.BackColor := clNone
      else
        Brush.BackColor := clWhite;
      Pen.Color := clBlack;
    end;
  end;

begin
  with PInfo^, ExtCanvas do
  begin
    TextPos.X := X;
    TextPos.Y := Y;
    Push;
    try
    // setup font style
      SetTextStyle;

      case Ord(Font.Alignment) of
        taLeft: TextPos.X := X;
        taCentered: TextPos.X := X + WLine / 2;
        taRight: TextPos.X := X + WLine;
      end;

    // draw legend text
      TextOut(TextPos, @Entry.FText[1], Length(Entry.FText));

      PInfo^.SetLineStyle(aLayer^.LineStyle);
      PInfo^.SetFillStyle(aLayer^.FillStyle);

    // little bit spaced from text
      X := X - TextSpace * HLine;
    // middle of line
      Y := Y - 0.5 * HLine;

//    Entry.SetStyle(PInfo);
      case Entry.FType of
        leSymbol:
          begin
            hSymbols := pSymbols(Symbols);
            if hSymbols <> nil then
              DrawSymbol(PInfo, X, Y, HLine, hSymbols^.
                GetSymbol(Entry.FSymName, Entry.FLibName));
          end;
        leLine: DrawLine(PInfo, X, Y, HLine, False);
//      leHorzLine    : DrawLine(PInfo,X,Y,HLine,True);
        leFilledRect: DrawArea(PInfo, X, Y, HLine, True);
//      leRectangle   : DrawArea(PInfo,X,Y,HLine,False);
      end; { case }
    finally
      Pop;
    end;

  end; // with

end;

//++ Glukhov Bug#458 Build#166 01.11.01

procedure TLegend.Draw(PInfo: pPaint; Layers: pLayers; Rect: TDRect);

  function GetLayerForEntry(Entry: TLegendEntry): PLayer;
  begin
    Result := Layers^.IndexToLayer(Entry.FLayerNumber);
  end;

  procedure SetTextStyle;
  begin
    // setup font
    with PInfo^, ExtCanvas, FEntryStyle do
    begin
      Font.Angle := 0;
      Font.Name := FEntryStyle.FontName;
      Font.Size := 45;
      Font.Color := clBlack;
      Font.Style := Italic + (Bold shl 1) + (Underlined shl 2);
      Font.Alignment := TTextAlignment(Alignment);
      Brush.Style := ptSolid;
      // Text transparent ?
      if Transparent = 1 then
        Brush.BackColor := clNone
      else
        Brush.BackColor := clWhite;
      Pen.Color := clBlack;
    end;
  end;

const
  dc_GapWeight = 0.02;
  dc_TitWeight = 1.0;
  dc_ScBWeight = 2.5;
  dc_TitMaxRat = 7.0;
  dc_ScBMaxRat = 12.0;
  dc_EntSymRat = SampleWidth + 1.0;
var
  i, j, nItems: Integer;
  bTitle: Boolean;
  rctAll: TDRect;
  rctPrt: TDRect;
  rctPhi: TRect;
  dVal, dRat: Double;
  dX, dY: Double;
  dH, dW: Double;
  dField: Double;
  dTmp: Double;
  ptrLayer: pLayer;
  Entry: TLegendEntry;
  sTmp: string;
  MaxWidth: Integer;
  nCols, nRows: Integer;
  dXPos, dYPos: Double;

begin
  nItems := FList.Count;
  bTitle := (Length(FTitle) > 0);

  // Check if there is something to draw
  if (nItems = 0) and (not bTitle) and (not bScaleBar) then
    Exit;

  // Convert to display coordinates
  rctAll.Init;
  PInfo.ConvertToDispDouble(Rect.A, dX, dY);
  rctAll.A.X := LimitToLong(dX);
  rctAll.A.Y := LimitToLong(dY);
  PInfo.ConvertToDispDouble(Rect.B, dX, dY);
  rctAll.B.X := LimitToLong(dX);
  rctAll.B.Y := LimitToLong(dY);

  if bScaleBar and (dScaleVal = 0) then
  begin
    // Width, in millimeters, of the physical screen
    i := GetDeviceCaps(PInfo^.ExtCanvas.Handle, HORZSIZE);
    // Width, in pixels, of the screen
    j := GetDeviceCaps(PInfo^.ExtCanvas.Handle, HORZRES);
    // Convert pixels to logical units
    rctPhi.Left := 0;
    rctPhi.Bottom := 0;
    rctPhi.Right := j - 1;
    rctPhi.Top := 100;
    DPToLP(Pinfo.ExtCanvas.Handle, rctPhi, 2);
    // Width, in project units (project centimeters)
    dVal := PInfo.CalculateDrawDouble(Abs(rctPhi.Right - rctPhi.Left) + 1);
    // Calculate the scale
    dScaleVal := i / (10 * dVal);
  end;

//  bScBar:= False;

  // Prepare settings to draw
  PInfo.ExtCanvas.Push;
  PInfo.ExtCanvas.BeginPaint;
  PInfo.ExtCanvas.Pen.Color := clBlack;
  PInfo.ExtCanvas.Pen.Style := ptSolid;

  // Deflate legend rectangle to move its content from border
  dVal := Min(rctAll.YSize, rctAll.XSize) * dc_GapWeight;
  rctAll.A.X := LimitToLong(rctAll.A.X + dVal);
  rctAll.A.Y := LimitToLong(rctAll.A.Y + dVal);
  rctAll.B.X := LimitToLong(rctAll.B.X - dVal);
  rctAll.B.Y := LimitToLong(rctAll.B.Y - dVal);

  if not bTransparent then
  begin
    DrawBackground(PInfo, rctAll);
  end;

  // Draw a title, if nevessary
  if bTitle then
  begin
    rctPrt.InitByRect(rctAll);
    if (nItems <> 0) or (bScaleBar) then
    begin
    // Calculate the title rectangle
      dRat := nItems + dc_TitWeight;
      if bScaleBar then
        dRat := dRat + dc_ScBWeight;
      dVal := dc_TitWeight * rctAll.YSize / Min(dRat, dc_TitMaxRat);

      rctAll.B.Y := LimitToLong(rctAll.B.Y - dVal);
      rctPrt.A.Y := LimitToLong(rctAll.B.Y);
    end;
    DrawTitle(PInfo, rctPrt, FTitle);
  end;

  // Draw a scale bar, if nevessary
  if bScaleBar then
  begin
    rctPrt.InitByRect(rctAll);
    if (nItems <> 0) then
    begin
    // Calculate the scale bar rectangle
      dRat := nItems + dc_ScBWeight;
      dVal := dc_ScBWeight * rctAll.YSize / Min(dRat, dc_ScBMaxRat);

      rctAll.A.Y := LimitToLong(rctAll.A.Y + dVal);
      rctPrt.B.Y := LimitToLong(rctAll.A.Y);
    end;
    DrawScBar(PInfo, rctPrt)
  end;

  // Check if there is something to draw
  if (nItems = 0) then
    Exit;

  // setup font
  SetTextStyle;

  // Calculate a maximal width for the legend entries
  MaxWidth := 0;
  for i := 0 to nItems - 1 do
  begin
    Entry := Items[i];
    sTmp := Entry.FText;
    // sync ev. layer name change
    if Entry.FNameFromLayer then
    begin
      ptrLayer := GetLayerForEntry(Items[i]);
      if Assigned(ptrLayer) then
        sTmp := ptrLayer^.Text^;
    end;
    Entry.FText := sTmp;
    MaxWidth := Max(MaxWidth, PInfo.ExtCanvas.TextWidth(@sTmp[1], Length(sTmp)));
  end;

  // get the entry text ratio: width to height
  dTmp := MaxWidth / PInfo.ExtCanvas.TextHeight('A', 1);
  // get the whole (symbol+text) entry ratio: width to height
  dTmp := dTmp + dc_EntSymRat;
  // get the inverted ratio: height to width
  dRat := 1 / dTmp;

  dY := rctAll.YSize;
  dX := rctAll.XSize;

  // try to find an ideal row/col ration
  nCols := 1;
  nRows := nItems;
  dVal := 0;
  for i := 1 to nItems do
  begin
    // Get a trying number of columns
    j := Trunc((1.0 * nItems / (1.0 * i)));
    if (j * i < nItems) then
      j := j + 1;

    dH := dY / i;
    dW := dX / j;
    // Get the ratio: height to width
    dTmp := dH / dW;
    if dTmp > dRat then
    begin
    // Text is too high
      // Get the height for the perfect ratio
      dH := dW * dRat;
    end;
    if dVal < dH then
    begin
    // The current text ratio is better than the saved one
      // Save the current data
      nCols := j;
      nRows := i;
      dVal := dH;
    end;
  end;

  // Calculate row/col size
  dY := dY / nRows;
  dX := dX / nCols;

  // Calculate text size
  dTmp := dY / dX;
  if dTmp > dRat then
  begin
  // Text is too high
    dH := dX * dRat;
  end
  else
  begin
  // Text is too wide
    dH := dY;
  end;

  // Set the entry text width
  dVal := dH * (SampleWidth + 2 * TextSpace);
  dW := dX - dVal;
  // Set the beginning position
  dXPos := rctAll.A.X + dVal;
  dYPos := rctAll.B.Y - (dY - dH) / 2.0;

  for i := 0 to nItems - 1 do
  begin

    ptrLayer := GetLayerForEntry(Items[i]);
    if Assigned(ptrLayer) then
      DrawEntry(PInfo, dXPos, dYPos, dH, dW, Items[i], ptrLayer);

    // Calculate next entry position
    if ((i + 1) mod nRows) > 0 then
      dYPos := dYPos - dY // the same column, next row
    else
    begin
    // the new column, first row
      dYPos := rctAll.B.Y - (dY - dH) / 2.0;
      dXPos := dXPos + dX;
    end;
  end;

  // finish painting...
  PInfo.ExtCanvas.EndPaint;
  PInfo.ExtCanvas.Pop;
end;
//-- Glukhov Bug#458 Build#166 01.11.01

procedure TLegend.DrawBackground(PInfo: pPaint; Rect: TDRect);
var
  TextPos: TGrPoint;
  Width: Integer;
  Size: TSize;
begin
  with PInfo^.ExtCanvas do
  begin
    Brush.Style := ptSolid;
    Brush.ForeColor := clWhite;
    Pen.Color := clWhite;
    PInfo^.ExtCanvas.Rectangle(Rect.A.X, Rect.A.Y, Rect.B.X, Rect.B.Y);
  end;
end;

procedure TLegend.DrawTitle
  (
  PInfo: pPaint;
  Rect: TDRect;
  Title: string
  );
var
  TextPos: TGrPoint;
  Width: Integer;
  Size: TSize;
begin
  with PInfo^.ExtCanvas, FTitleStyle do
  begin
    // setup font
    Font.Angle := 0;
    Font.Name := FTitleStyle.FontName;
    Font.Size := LimitToInt(Rect.YSize);
    Font.Color := clBlack;
    Font.Style := Italic + (Bold shl 1) + (Underlined shl 2);
    Font.Alignment := TTextAlignment(Alignment);
    Brush.Style := ptSolid;
    // Text transparent ?
    if Transparent = 1 then
      Brush.BackColor := clNone
    else
      Brush.BackColor := clWhite;
    Pen.Color := clBlack;
    Width := TextWidth(@Title[1], Length(Title));
    // if text is too long, adjust
    if Width > Rect.XSize then
      Font.Size := LimitToInt(Rect.XSize / Width * Rect.YSize);
    Size := TextExtent(@Title[1], Length(Title));
    case Font.Alignment of
      ExtCanvas.taLeft: TextPos.X := Rect.A.X;
      ExtCanvas.taCentered: TextPos.X := Rect.A.X + Rect.XSize / 2;
      ExtCanvas.taRight: TextPos.X := Rect.A.X + Rect.XSize;
    end;
    TextPos.Y := Rect.B.Y - (Rect.YSize - Size.cy) / 2;
    TextOut(TextPos, @Title[1], Length(Title));
  end; // with
end;

//++ Glukhov Bug#458 Build#166 10.11.01

procedure TLegend.SetScaleVal(dVal: Double);
begin
  dScaleVal := dVal;
end;

procedure TLegend.DrawScBar(PInfo: pPaint; Rect: TDRect);
var
  Width: Integer;
  sTmp, sUn: string;
  TextPos: TGrPoint;
  dTmp: Double;
  i, j: Integer;
  iBarWProj: Integer;
  iBRctWP: Integer;
  iBRctWD: Integer;
  iBRctsNum: Integer;
  iBarWDisp: Integer;

  procedure FormScaleFraction(dValue: Double; var iNum, iDen: Integer);
  const
    dc_MinScale = 1E-9;
    dc_MaxScale = 10.0;
  begin
    iNum := 1;
    iDen := 1;
    if dValue < dc_MinScale then
    begin
      iDen := Round(1 / dc_MinScale);
      Exit;
    end;
    if dValue > dc_MaxScale then
    begin
      iNum := Round(dc_MaxScale);
      Exit;
    end;
    if Abs(dValue - 1) < 0.005 then
      Exit;

    // Calculate the numerator
    while dValue > 0.01005 do
    begin
      iNum := 10 * iNum;
      dValue := dValue / 10.0;
    end;

    // Calculate the denominator
    iDen := Round(1.0 / dValue);

    // Optimize, reducing the extra zeros
    while (iNum mod 10 = 0) and (iDen mod 10 = 0) do
    begin
      iNum := iNum div 10;
      iDen := iDen div 10;
    end;
  end;

  procedure FormScaleFractionStr(dValue: Double; var sScale: string);
  const
    dc_MinScale = 1E-9;
    dc_MaxScale = 10.0;
  var
    iNum, iDen: Integer;
  begin
    sScale := '';
    if dValue < dc_MinScale then
      Exit;
    if dValue > dc_MaxScale then
      Exit;
    FormScaleFraction(dValue, iNum, iDen);
    sScale := IntToStr(iNum) + ' : ' + IntToStr(iDen);
  end;

const
  dc_YTxt1Offs = 0.05;
  dc_YTxt1Weight = 0.4;
  dc_YTxt2Offs = 0.5;
  dc_YTxt2Weight = 0.3;
  dc_YFigOffs = 0.85;
  dc_YFigWeight = 0.1;
  dc_XGapWeight = 0.15;
  ic_UNumMin = 3;
  ic_MRatio = 100;
  ic_KMRatio = 100000;

begin
  with PInfo.ExtCanvas do
  begin
    with FScBarStyle do
    begin
      // Set up the font
      Font.Angle := 0;
      Font.Name := FScBarStyle.FontName;
      Font.Size := LimitToInt(Rect.YSize * dc_YTxt1Weight);
      Font.Color := clBlack;
      Font.Style := Italic + (Bold shl 1) + (Underlined shl 2);
//      Font.Alignment:= TTextAlignment( Alignment );
      Font.Alignment := ExtCanvas.taCentered;
      Brush.Style := ptSolid;
      if Transparent = 1 then
        Brush.BackColor := clNone
      else
        Brush.BackColor := clWhite;
      Pen.Color := clBlack;
    end; // with FScBarStyle

    FormScaleFractionStr(dScaleVal, sTmp);

    Width := TextWidth(@sTmp[1], Length(sTmp));
    // if text is too long, adjust
    if Width > Rect.XSize then
      Font.Size := LimitToInt(Rect.XSize / Width * Font.Size);
    case Font.Alignment of
      ExtCanvas.taLeft: TextPos.X := Rect.A.X;
      ExtCanvas.taCentered: TextPos.X := Rect.A.X + Rect.XSize / 2;
      ExtCanvas.taRight: TextPos.X := Rect.A.X + Rect.XSize;
    end;
    TextPos.Y := Rect.B.Y - Rect.YSize * dc_YTxt1Offs;
    TextOut(TextPos, @sTmp[1], Length(sTmp));

    // Calculate the project length of a scale bar
    dTmp := PInfo.CalculateDrawDouble(Rect.XSize);
    iBarWProj := LimitToLong(dTmp * (1 - 2 * dc_XGapWeight));
    // It would be good to make something smarter for the number of rects
    iBRctsNum := ic_UNumMin;
    // Calculate size of a color bar rectangle
    iBRctWP := LimitToLong(iBarWProj / iBRctsNum);
    // Round it up to 1 significant digit
    i := 1;
    while iBRctWP > 10 do
    begin
      iBRctWP := iBRctWP div 10;
      i := i * 10;
    end;
    iBRctWP := iBRctWP * i;
    // Calculate the logical size of a scale bar rectangle
    iBRctWD := PInfo.CalculateDisp(iBRctWP);

    // Calculate the unit and rectangle size in these units
    if iBRctWP > ic_KMRatio then
    begin
      sUn := MlgStringList.Strings['LegendPrintDlg', 12];
      iBRctWP := iBRctWP div ic_KMRatio;
    end
    else
      if iBRctWP > ic_MRatio then
      begin
        sUn := MlgStringList.Strings['LegendPrintDlg', 11];
        iBRctWP := iBRctWP div ic_MRatio;
      end
      else
      begin
        sUn := MlgStringList.Strings['LegendPrintDlg', 10];
      end;
    // Draw the scale bar rectangles
    i := Round(Rect.A.X + (Rect.XSize - iBRctsNum * iBRctWD) / 2);
    j := Round(Rect.B.Y - Rect.YSize * dc_YFigOffs);
    Width := Round(Rect.YSize * dc_YFigWeight);
    Brush.ForeColor := clBlack;
    Rectangle(i, j, i + iBRctWD, j - Width);
    Brush.ForeColor := clWhite;
    Rectangle(i + iBRctWD, j, i + 2 * iBRctWD, j - Width);
    Brush.ForeColor := clBlack;
    Rectangle(i + 2 * iBRctWD, j, i + 3 * iBRctWD, j - Width);

    // Draw the scale values
    TextPos.Y := Rect.B.Y - Rect.YSize * dc_YTxt2Offs;
    Font.Size := LimitToInt(dc_YTxt2Weight * Rect.YSize);
    sTmp := IntToStr(2 * iBRctWP);
    Width := TextWidth(@sTmp[1], Length(sTmp));
    if Width > iBRctWD then
      Font.Size := LimitToInt(iBRctWD / Width * Font.Size);
    TextPos.X := i + 3 * iBRctWD;
    TextOut(TextPos, @sTmp[1], Length(sTmp));
    sTmp := IntToStr(iBRctWP);
    TextPos.X := i;
    TextOut(TextPos, @sTmp[1], Length(sTmp));
    TextPos.X := i + 2 * iBRctWD;
    TextOut(TextPos, @sTmp[1], Length(sTmp));
    sTmp := '0';
    TextPos.X := i + iBRctWD;
    TextOut(TextPos, @sTmp[1], Length(sTmp));
    TextPos.X := Round(i + 3.52 * iBRctWD);
    Font.Alignment := ExtCanvas.taLeft;
    TextOut(TextPos, @sUn[1], Length(sUn));
  end; // with
end;
//-- Glukhov Bug#458 Build#166 10.11.01

{*******************************************************************************
I Class TLegendList                                                            I
*******************************************************************************}

constructor TLegendList.Create;
begin
  inherited Create;
  FList := TList.Create;
  FCurrent := -1;
end;

destructor TLegendList.Destroy;
var
  Cnt: Integer;
begin
  for Cnt := 0 to FList.Count - 1 do
    TLegend(FList[Cnt]).Free;
  FList.Free;
  inherited Destroy;
end;

function TLegendList.GetLegend
  (
  Pos: Integer
  ): TLegend;
begin
  Result := FList.Items[Pos];
end;

procedure TLegendList.SetLegend
  (
  Pos: Integer;
  Legend: TLegend
  );
begin
  FList.Items[Pos] := Legend;
end;

function TLegendList.GetLegendByName
  (
  Name: string
  ): TLegend;
var
  i: Integer;
begin
  for i := 0 to FList.Count - 1 do
  begin
    Result := FList.Items[i];
    if Result.Name = Name then
      Exit;
  end;
  Result := nil;
end;

procedure TLegendList.SetLegendByName
  (
  Name: string;
  Legend: TLegend
  );
var
  i: Integer;
  L: TLegend;
begin
  for i := 0 to FList.Count - 1 do
  begin
    L := FList.Items[i];
    if L.Name = Name then
    begin
      FList.Items[i] := L;
      Exit;
    end;
  end;
end;

//++ Glukhov Bug#293(PrintFrame) Build#175 09.02.02

function TLegendList.NewLegend(Name: string): TLegend;
begin
  Result := nil;
  // Check that the name is distinguished from the reserved for frames in the print dialog
  if Name = csLegName4Frame then
    Exit;
  if LegendByName[Name] <> nil then
    Exit;
  Result := TLegend.Create;
  Result.Name := Name;
  FList.Add(Result);
end;
//-- Glukhov Bug#293(PrintFrame) Build#175 09.02.02

function TLegendList.AddLegend(aLegend: TLegend): Boolean;
begin
  Result := LegendByName[aLegend.Name] = nil;
  if Result then
    FList.Add(aLegend);
end;

procedure TLegendList.CopyLegend(Dest: TLegend; Source: TLegend);
var
  i: Integer;
  SrcEntry: TLegendEntry;
begin
  // copy entries
  for i := 0 to Source.Count - 1 do
  begin
    SrcEntry := Source.Items[i];
    Dest.Add(SrcEntry.Copy);
  end;
  // copy title
  Dest.Name := Source.Name;
  Dest.Title := Source.Title;
//++ Glukhov Bug#458 Build#166 15.11.01
  Dest.bScaleBar := Source.bScaleBar;
//-- Glukhov Bug#458 Build#166 15.11.01
  Dest.bTransparent := Source.bTransparent;

  Dest.FTitleStyle.Assign(Source.FTitleStyle);
  Dest.FScBarStyle.Assign(Source.FScBarStyle);
  Dest.FEntryStyle.Assign(Source.FEntryStyle);
end;

function TLegendList.Clone: TLegendList;
var
  i: Integer;
  aClone: TLegendList;
  aLegend: TLegend;
begin
  aClone := TLegendList.Create;
  for i := 0 to Count - 1 do
  begin
    aLegend := TLegend.Create;
    CopyLegend(aLegend, Legends[i]);
    aClone.AddLegend(aLegend);
  end;
  aClone.CurrentLegendIndex := CurrentLegendIndex;
  Result := aClone;
end;

function TLegendList.DeleteLegend
  (
  Name: string
  ): Boolean;
var
  L: TLegend;
  i: Integer;
begin
  Result := False;
  L := LegendByName[Name];
  if L <> nil then
  begin
    i := FList.IndexOf(L);
    if i >= 0 then
    begin
      FList.Delete(i);
      Result := True;
    end;
  end;
end;

function TLegendList.GetCount: Integer;
begin
  Result := FList.Count;
end;

procedure TLegendList.Store
  (
  S: TOldStream
  );
var
  i: Integer;
  Version: Byte;
begin
  Version := LegendListVersion;
  S.Write(Version, sizeof(Version));
  S.Write(FList.Count, sizeof(FList.Count));
  for i := 0 to FList.Count - 1 do
    Legends[i].Store(S);
end;

procedure TLegendList.WriteToRegistry
  (
  aRegDB: TRegistryDatabase;
  Where: string
  );
var
  i: Integer;
  iStr: string;
begin
  with aRegDB do
  begin
    OpenKey(Where, True);
    WriteInteger('CurrentLegend', FCurrent);
    WriteInteger('LegendCount', FList.Count);
    // write each entry into a key like EntryXXX, where XXX is an ascending integer
    for i := 0 to Count - 1 do
    begin
      Str(i, iStr);
      Legends[i].WriteToRegistry(aRegDB, Where + '\Legend' + iStr);
    end;
  end;
end;

constructor TLegendList.Load
  (
  S: TOldStream
  );
var
  Version: Byte;
  i, Count: Integer;
begin
  inherited Create;
  FList := TList.Create;
  S.Read(Version, sizeof(Version));
  S.Read(Count, sizeof(Count));
  for i := 0 to Count - 1 do
    FList.Add(TLegend.Load(S));
end;

constructor TLegendList.ReadFromRegistry
  (
  aRegDB: TRegistryDataBase;
  Where: string
  );
var
  i,
    Cnt: Integer;
  iStr: string;
begin
  inherited Create;
  FList := TList.Create;
  with aRegDB do
  begin
    if OpenKey(Where, False) then
    begin
      FCurrent := -1;
      FCurrent := ReadInteger('CurrentLegend');
      Cnt := ReadInteger('LegendCount');
      // read each entry (scheme LegendXXX, where XXX is an ascending integer)
      for i := 0 to Cnt - 1 do
      begin
        Str(i, iStr);
        FList.Add(TLegend.ReadFromRegistry(aRegDB, Where + '\Legend' + iStr));
      end;
    end;
  end;
end;

{++ Moskaliov BUG#268 BUILD#108 29.03.00}

constructor TLegendPrintDlg.Create(AOwner: TComponent; Legends: TLegendList; TextEditEnable: Boolean);
var
  Cnt: Integer;
begin
  inherited Create(AOwner);
  if TextEditEnable then
    LegendCombo.Style := csDropDown
  else
    LegendCombo.Style := csDropDownList;
   // fill the legends-combo
  for Cnt := 0 to Legends.Count - 1 do
    LegendCombo.Items.Add(Legends.Legends[Cnt].Name);
  FName := '';
  if LegendCombo.Items.Count > 0 then
  begin
    LegendCombo.ItemIndex := 0;
    FName := LegendCombo.Items[LegendCombo.ItemIndex];
  end;
end;

procedure TLegendPrintDlg.LegendComboChange(Sender: TObject);
begin
  FName := LegendCombo.Text;
end;
{-- Moskaliov BUG#268 BUILD#108 29.03.00}

procedure TLegendPrintDlg.FormCreate(Sender: TObject);
begin
     //Scaleby(Trunc(9600000/(PixelsPerInch)),100000);
end;

end.

