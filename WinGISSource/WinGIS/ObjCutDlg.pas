unit ObjCutDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  MultiLng, StdCtrls;

type
  TObjCutDialog = class(TForm)
    MlgSection: TMlgSection;
    ButtonOK: TButton;
    ButtonCancel: TButton;
    GroupBoxS1: TGroupBox;
    GroupBoxS2: TGroupBox;
    GroupBoxD: TGroupBox;
    ListBoxS1: TListBox;
    ListBoxS2: TListBox;
    ListBoxD: TListBox;
    procedure ListBoxClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ObjCutDialog: TObjCutDialog;

implementation

{$R *.DFM}

procedure TObjCutDialog.ListBoxClick(Sender: TObject);
begin
     ButtonOK.Enabled:=(ListBoxS1.ItemIndex > -1)
                   and (ListBoxS2.ItemIndex > -1)
                   and (ListBoxD.ItemIndex > -1)
                   and (ListBoxS1.ItemIndex <> ListBoxS2.ItemIndex)
                   and (ListBoxS1.ItemIndex <> ListBoxD.ItemIndex)
                   and (ListBoxS2.ItemIndex <> ListBoxD.ItemIndex);

end;

end.
