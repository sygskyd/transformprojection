�
 TOPTIONSDIALOG 0�  TPF0TOptionsDialogOptionsDialogLeft>Top� HelpContext�BorderStylebsDialogCaption!1ClientHeight6ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrder	PositionpoScreenCenterOnCloseQueryFormCloseQueryOnCreate
FormCreateOnHideFormHideOnShowFormShowPixelsPerInch`
TextHeight TLabelLabelTemplateLeftTopWidthHeightCaption!25  TLabelLabelIniLeft�TopWidthHeightCursorcrHandPointCaptionINIFont.CharsetDEFAULT_CHARSET
Font.ColorclBlueFont.Height�	Font.NameMS Sans Serif
Font.StylefsUnderline 
ParentFontOnClickLabelIniClick  TButton	CancelBtnLeft@TopWidthYHeightCancel	Caption!11ModalResultTabOrder  TButtonOkBtnLeft� TopWidthYHeightCaption!10Default	ModalResultTabOrderOnClick
OkBtnClick  TWTabbedNoteBookNoteBookLeftTopWidth�Height� TabFont.CharsetDEFAULT_CHARSETTabFont.Color	clBtnTextTabFont.Height�TabFont.NameMS Sans SerifTabFont.Style TabOrder  TTabPage LeftTopCaption!2 
TWGroupBoxGroup2LeftTopRWidthyHeight.Caption!7TabOrderBoxStyle	bsTopLine TLabelLabel4LeftTopWidth1HeightAutoSizeCaption!8FocusControlDigitizerCombo  TButtonDigitizerSetupBtnLeft� TopWidthYHeightCaption!9TabOrderOnClickDigitizerSetupBtnClick  	TComboBoxDigitizerComboLeft8TopWidth� Height
ItemHeightSorted	TabOrder OnChangeDigitizerComboChange   
TWGroupBoxGroup1LeftTopWidthyHeight<Caption!3TabOrder BoxStyle	bsTopLine TLabelLabel1Left� Top&Width	HeightCaption!6  TLabelLabel7LeftTopWidthHeight	AlignmenttaRightJustifyCaption!26  TSpinBtnAutoSaveSpinLeft� Top"Height	Arrowkeys	AssociateAutoSaveEditCtrlIncrement       �@	Increment       ��?Max      ��@ShiftIncrement       �@  TEditAutoSaveEditLeft� Top"Width9HeightTabOrder  	TCheckBoxAutoSaveCheckLeftTop$Width� HeightCaption!5TabOrderOnClickAutoSaveCheckClick  	TCheckBoxBackupFileCheckLeftTopWidth� HeightCaption!4TabOrder   TEditEditHistSizeLeft(Top
WidthHeightTabOrder  TSpinBtnSpinBtnHistSizeLeftBTop
Height	Arrowkeys	Increment       ��?Max       �@Min       �@	ValidatorHistSizeValidator   
TWGroupBox
WGroupBox3LeftTop� WidthyHeight)Caption!20TabOrderBoxStyle	bsTopLine TLabelLabel2LeftTopWidthAHeightAutoSizeCaption!21EnabledFocusControl
UnitsComboVisible  TLabelLabel3Left� TopWidthAHeightAutoSizeCaption!22EnabledFocusControl	ScaleEditVisible  TLabelLabel8LeftTopWidthQHeightAutoSizeCaption!27  	TComboBox
UnitsComboLeftHTopWidthtHeightEnabled
ItemHeightTabOrder Visible  TEdit	ScaleEditLeft	TopWidthbHeightEnabledTabOrderVisible  TSpinBtn	ScaleSpinLeftlTopHeight	ArrowkeysEnabled	Increment       ��?Max �P]���CMin �P]����Visible  	TComboBox	AreaComboLeft`TopWidthIHeightStylecsDropDownList
ItemHeightItems.Stringsm�hakm� TabOrderOnChangeAreaComboChange     TEditEditTemplateLeftTopWidth� HeightTabOrderTextEditTemplate  TButtonButton1Left� TopWidthHeightCaption...TabOrderOnClickButton1Click  TIntValidatorAutoSaveValEditAutoSaveEdit	MaxLengthMaxValue�Left:  TMlgSection
MlgSectionSectionOptionsDialogLeftX  TOpenDialogDlgTemplateFilter	AMP|*.ampLeftx  TIntValidatorHistSizeValidatorEditEditHistSizeValidateOnExitMinValue	MaxLengthMaxValueLeft�    