Unit USelect;

Interface

Uses Messages,Objects,AM_Admin,AM_Def,AM_Index,AM_Point
     {$IFNDEF WMLT}
     ,AM_DDE
     {$ENDIF}
     ,Controls,
     AM_Layer,AM_Poly,AM_CPoly,AM_StdDl,AM_Dlg1,AM_Coll,WinProcs,ResDlg,
     AM_Sym,AM_Text,AVLTree,AM_Paint,WinTypes,am_punkt,ProjStyle;

Const maxType            = 6;
      Id_Invers          = 102;
      vot_SpecificSymbol = 255;
      ObjType          : Array[0..maxType-1] of Word = (ot_Poly,ot_CPoly,ot_Pixel,ot_Symbol,ot_Text,vot_SpecificSymbol);
Type
     {***********************************************************************}
     { Object TTree                                                          }
     {-----------------------------------------------------------------------}
     { Felder:                                                               }
     {***********************************************************************}
     PSelTree              = ^TSelTree;
     TSelTree              = Object(TAvlTree)
       Function    Compare(Key1,Key2:Pointer):Integer; virtual;
       Procedure   FreeItem(Item:Pointer); virtual;
     end;
     PSelectDlg           = ^TSelectDlg;
     TSelectDlg           = Class(TLayerDlg)
      Public
       Data             : PPointData;
       TypesList        : TRListBox;
       Constructor Init(AParent:TWinControl;AData:PPointData;
           ALayers:PLayers;AProjStyle:TProjectStyles);
       Function    CanClose:Boolean; override;
       Procedure   SetupWindow; override;
     end;

Procedure SelectEmpty(Proj:Pointer;PInfo:PPaint);

Implementation

Uses AM_Proj,UserIntf,SymbolSelDlg,AM_Group;

Procedure SelectEmpty
   (
   Proj            : Pointer;
   PInfo           : PPaint
   );
  var Data             : TPointData;
      AvlTree          : PSelTree;
      Cnt              : Integer;
      ALayer           : PLayer;
      AllCount         : LongInt;
      CurObject        : LongInt;
      AbortDlg         : TAbortDlg;
      Aborted          : Boolean;
      ASGroup          : PSGroup;
  Function InsertPoints
     (
     Item              : PIndex
     )
     : Boolean; Far;
    var Node           : PNode;
        AItem          : PPixel;
        APercent       : double;
    begin
      New(Node);
      AItem:=Pointer(PLayer(PInfo^.Objects)^.IndexObject(PInfo,Item));
      with Node^ do begin
        Position:=@AItem^.Position;
        ObjNumber:=AItem^.Index;
      end;
      if not AvlTree^.Insert(Node) then Dispose(Node);
      Inc(CurObject);
      APercent:=100*(CurObject/AllCount);
      if APercent>100 then APercent:=100;
      AbortDlg.SetPercent(APercent);
      InsertPoints:=AbortDlg.CheckAskForAbort(3803);
    end;

    Function InsertPoly
     (
     Item              : PIndex
     )
     : Boolean; Far;
    var Node           : PNode;
        AItem          : PPoly;
        APoint         : PDPoint;
        cnt            : Integer;
        APercent       : Double;
    begin
      AItem:=Pointer(PLayer(PInfo^.Objects)^.IndexObject(PInfo,Item));
      for Cnt:=0 to AItem^.Data^.Count-1 do begin
        APoint := AItem^.Data^.at(Cnt);
        New(Node);
        with Node^ do begin
          Position:=APoint;
          ObjNumber:=0;
        end;
        if not AvlTree^.Insert(Node) then Dispose(Node);
        Inc(CurObject);
      end;
      APercent:=100*(CurObject/AllCount);
      if APercent>100 then APercent:=100;
      AbortDlg.SetPercent(APercent);
      InsertPoly:=AbortDlg.CheckAskForAbort(3803);
    end;

  Function InsertSymbols
     (
     Item              : PIndex
     )
     : Boolean; Far;
    var Node           : PNode;
        AItem          : PSymbol;
        APercent       : Double;
    begin
      AItem:=Pointer(PLayer(PInfo^.Objects)^.IndexObject(PInfo,Item));
      if (ASGroup = nil) or (AItem^.SymIndex = ASGroup^.Index) then begin
         New(Node);
         with Node^ do begin
              Position:=@AItem^.Position;
              ObjNumber:=AItem^.Index;
         end;
         if not AvlTree^.Insert(Node) then Dispose(Node);
         Inc(CurObject);
         APercent:=100*(CurObject/AllCount);
         if APercent>100 then APercent:=100;
         AbortDlg.SetPercent(APercent);
         InsertSymbols:=AbortDlg.CheckAskForAbort(3803);
      end
      else begin
         InsertSymbols:=False;                                   
      end;
    end;

  Function InsertText
     (
     Item              : PIndex
     )
     : Boolean; Far;
    var Node           : PNode;
        AItem          : PText;
        APercent       : Double;
    begin
      New(Node);
      AItem:=Pointer(PLayer(PInfo^.Objects)^.IndexObject(PInfo,Item));
      with Node^ do begin
        Position:=@AItem^.Pos;
        ObjNumber:=AItem^.Index;
      end;
      if not AvlTree^.Insert(Node) then Dispose(Node);
      Inc(CurObject);
      APercent:=100*(CurObject/AllCount);
      if APercent>100 then APercent:=100;
      AbortDlg.SetPercent(APercent);
      InsertText:=AbortDlg.CheckAskForAbort(3803);
    end;
  Function SearchNode
     (
     SData           : Pointer;
     SelTree            : PAVLNode
     )
     : PAVLNode;
    begin
      if SelTree=NIL then SearchNode:=NIL
      else begin
        repeat
          case AvlTree^.Compare(SData,SelTree^.Data) of
            CPEQUAL   : begin
              SearchNode:=SelTree;
              Exit;
            end;
            CPSMALLER : if SelTree^.Left=NIL then begin
                  SearchNode:=SelTree;
                  Exit;
                end
                else SelTree:=SelTree^.Left;
            CPBIGGER  : if SelTree^.Right=NIL then begin
                  SearchNode:=SelTree;
                  Exit;
                end
                else SelTree:=SelTree^.Right;
          end;
        until SelTree=NIL;
        SearchNode:=NIL;
      end;
    end;
  Function DoInversSelection
     (
     Item          : PCPoly
     )
     : Boolean; Far;
    var Search     : TNode;
        SearchPos  : TDPoint;
        Node1      : PAVLNode;
        Node2      : PAvlNode;
        List       : PListNode;
        NewNode    : PListNode;
        AStr       : String;
        Params     : Array[0..1] of LongInt;
        AItem      : PCPoly;
        found      : Boolean;
        APercent   : Double;
    begin
      found:=false;
      AItem:=Pointer(PLayer(PInfo^.Objects)^.IndexObject(PInfo,Item));
      Search.Position:=@SearchPos;
      SearchPos.Init(AItem^.ClipRect.A.X,-MaxLongInt);
      Node1:=SearchNode(@Search,AvlTree^.AvlRoot);
      SearchPos.Init(AItem^.ClipRect.B.X,MaxLongInt);
      Node2:=SearchNode(@Search,AvlTree^.AvlRoot);
      List:=NIL;
      if (PNode(Node1^.Data)^.Position^.Y>=AItem^.ClipRect.A.Y)
          and (PNode(Node1^.Data)^.Position^.Y<=AItem^.ClipRect.B.Y)
          and (AItem^.PointInside(PInfo,PNode(Node1^.Data)^.Position^)) then begin
        New(NewNode);
        NewNode^.ObjNumber:=PNode(Node1^.Data)^.ObjNumber;
        NewNode^.Next:=List;
        List:=NewNode;
        found:=True;
      end;
      if Node1<>Node2 then repeat
        Node1:=AvlTree^.NextNode(Node1);
        if (PNode(Node1^.Data)^.Position^.Y>=AItem^.ClipRect.A.Y)
            and (PNode(Node1^.Data)^.Position^.Y<=AItem^.ClipRect.B.Y)
            and (AItem^.PointInside(PInfo,PNode(Node1^.Data)^.Position^)) then begin
          New(NewNode);
          NewNode^.ObjNumber:=PNode(Node1^.Data)^.ObjNumber;
          NewNode^.Next:=List;
          List:=NewNode;
          found:=True;
        end;
      until Node1=Node2;
      if not found then ALayer^.Select(PProj(Proj)^.PInfo,Item);
      while List<>NIL do begin
        NewNode:=List;
        List:=List^.Next;
        Dispose(NewNode);
      end;
      Inc(CurObject,100);
      APercent:=100*(CurObject/AllCount);
      if APercent>100 then APercent:=100;
      AbortDlg.SetPercent(APercent);
      DoInversSelection:=AbortDlg.CheckAskForAbort(3803);
    end;

  Function DoSelection
     (
     Item          : PCPoly
     )
     : Boolean; Far;
    var Search     : TNode;
        SearchPos  : TDPoint;
        Node1      : PAVLNode;
        Node2      : PAvlNode;
        List       : PListNode;
        NewNode    : PListNode;
        AStr       : String;
        Params     : Array[0..1] of LongInt;
        AItem      : PCPoly;
        found      : Boolean;
        APercent   : Double;
    begin
      found:=false;
      AItem:=Pointer(PLayer(PInfo^.Objects)^.IndexObject(PInfo,Item));
      Search.Position:=@SearchPos;
      SearchPos.Init(AItem^.ClipRect.A.X,-MaxLongInt);
      Node1:=SearchNode(@Search,AvlTree^.AvlRoot);
      SearchPos.Init(AItem^.ClipRect.B.X,MaxLongInt);
      Node2:=SearchNode(@Search,AvlTree^.AvlRoot);
      List:=NIL;
      if (PNode(Node1^.Data)^.Position^.Y>=AItem^.ClipRect.A.Y)
          and (PNode(Node1^.Data)^.Position^.Y<=AItem^.ClipRect.B.Y)
          and (AItem^.PointInside(PInfo,PNode(Node1^.Data)^.Position^)) then begin
        New(NewNode);
        NewNode^.ObjNumber:=PNode(Node1^.Data)^.ObjNumber;
        NewNode^.Next:=List;
        List:=NewNode;
        found:=True;
      end;
      if Node1<>Node2 then repeat
        Node1:=AvlTree^.NextNode(Node1);
        if (PNode(Node1^.Data)^.Position^.Y>=AItem^.ClipRect.A.Y)
            and (PNode(Node1^.Data)^.Position^.Y<=AItem^.ClipRect.B.Y)
            and (AItem^.PointInside(PInfo,PNode(Node1^.Data)^.Position^)) then begin
          New(NewNode);
          NewNode^.ObjNumber:=PNode(Node1^.Data)^.ObjNumber;
          NewNode^.Next:=List;
          List:=NewNode;
          found:=True;
        end;
      until Node1=Node2;
      if found then ALayer^.Select(PProj(Proj)^.PInfo,Item);
      while List<>NIL do begin
        NewNode:=List;
        List:=List^.Next;
        Dispose(NewNode);
      end;
      Inc(CurObject,100);
      APercent:=100*(CurObject/AllCount);
      if APercent>100 then APercent:=100;
      AbortDlg.SetPercent(APercent);
      DoSelection:=AbortDlg.CheckAskForAbort(3803);
    end;

  Function DoCount
     (
     Item          : Pointer
     )
     : Boolean; Far;
    begin
      Inc(AllCount);
      DoCount:=FALSE;
    end;
  begin
    if ExecDialog(TSelectDlg.Init(PProj(Proj)^.Parent,
        @PProj(Proj)^.CombPointAreaData,PProj(Proj)^.Layers,PProj(Proj)^.PInfo^.ProjStyles))=id_OK then begin
      Data:=PProj(Proj)^.CombPointAreaData;

      ASGroup:=nil;
      if vot_SpecificSymbol in Data.ObjectTypes then begin
         ASGroup:=GetSymbol(nil,PProj(Proj));
         while ASGroup = nil do begin
             if ExecDialog(TSelectDlg.Init(PProj(Proj)^.Parent,@PProj(Proj)^.CombPointAreaData,PProj(Proj)^.Layers,PProj(Proj)^.PInfo^.ProjStyles)) = id_OK then begin
                Data:=PProj(Proj)^.CombPointAreaData;
                ASGroup:=GetSymbol(nil,PProj(Proj));
             end
             else begin
                exit;  
             end;
         end;
         Include(Data.ObjectTypes,ot_Symbol);
         Exclude(Data.ObjectTypes,vot_SpecificSymbol);
      end;

      AbortDlg:=CreateAbortDlg(PProj(Proj)^.Parent);
      PProj(Proj)^.SetStatusText(GetLangText(11082),TRUE);
      AllCount:=0;
      for Cnt:=0 to Data.LayersAreas^.Count-1 do begin
        ALayer:=PProj(Proj)^.Layers^.IndexToLayer(LongInt(Data.LayersAreas^.At(Cnt)));
        ALayer^.FirstObjectType(ot_CPoly,@DoCount);
      end;
      AllCount:=AllCount*100;
      for Cnt:=0 to Data.LayersPoints^.Count-1 do begin
        ALayer:=PProj(Proj)^.Layers^.IndexToLayer(LongInt(Data.LayersPoints^.At(Cnt)));
        if ot_Pixel in Data.ObjectTypes then
           ALayer^.FirstObjectType(ot_Pixel,@DoCount);
        if ot_Symbol in Data.ObjectTypes then
           ALayer^.FirstObjectType(ot_Symbol,@DoCount);
        if ot_Text in Data.ObjectTypes then
           ALayer^.FirstObjectType(ot_Text,@DoCount);
      end;
      AvlTree:=New(PSelTree,Init);
      Aborted:=FALSE;
      CurObject:=0;
      for Cnt:=0 to Data.LayersPoints^.Count-1 do begin
        ALayer:=PProj(Proj)^.Layers^.IndexToLayer(LongInt(Data.LayersPoints^.At(Cnt)));
        if ot_Pixel in Data.ObjectTypes then
           Aborted:=ALayer^.FirstObjectType(ot_Pixel,@InsertPoints)<>NIL;
        if ot_Symbol in Data.ObjectTypes then
           Aborted:=Aborted or (ALayer^.FirstObjectType(ot_Symbol,@InsertSymbols)<>NIL);
        if ot_Text in Data.ObjectTypes then
           Aborted:=Aborted or (ALayer^.FirstObjectType(ot_Text,@InsertText)<>NIL);
        if ot_CPoly in Data.ObjectTypes then
           Aborted:=Aborted or (ALayer^.FirstObjectType(ot_CPoly,@InsertPoly)<>NIL);
        if ot_Poly in Data.ObjectTypes then
           Aborted:=Aborted or (ALayer^.FirstObjectType(ot_Poly,@InsertPoly)<>NIL);
      end;
      if (AvlTree^.AvlRoot<>NIL)
          and not Aborted then for Cnt:=0 to Data.LayersAreas^.Count-1 do begin
        ALayer:=PProj(Proj)^.Layers^.IndexToLayer(LongInt(Data.LayersAreas^.At(Cnt)));
        if Data.Invers then ALayer^.FirstObjectType(ot_CPoly,@DoInversSelection) else
          ALayer^.FirstObjectType(ot_CPoly,@DoSelection);
      end;
      Dispose(AvlTree,Done);
      PProj(Proj)^.ClearStatusText;
      AbortDlg.Free;
    end;
  end;


{******************** Dialog *******************************}
Constructor TSelectDlg.Init
   (
   AParent         : TWinControl;
   AData           : PPointData;
   ALayers         : PLayers;
   AProjStyle      : TProjectStyles
   );
  begin
    inherited Init(AParent,'ArAnalyse',ALayers,AProjStyle);
    Data:=AData;
    Typeslist:=TRListBox.InitResource(Self,id_ObjectTypes);
    HelpContext:=4020;
  end;

Procedure TSelectDlg.SetupWindow;
  var Cnt          : Integer;
      AText        : Array[0..255] of Char;
  begin
    inherited SetupWindow;
    FillLayerList(id_SourceList,TRUE,FALSE);
    FillLayerList(id_SourcePoint,TRUE,FALSE);
    SetMultiLayerSelection(id_SourceList,Data^.LayersAreas,TRUE);
    SetMultiLayerSelection(id_SourcePoint,Data^.LayersPoints,TRUE);
    for Cnt:=0 to maxType-1 do begin
      GetLangPChar(11155+Cnt,AText,SizeOf(AText));
      TypesList.AddString(AText);
      if ObjType[Cnt] in Data^.ObjectTypes then
          SendDlgItemMsg(id_ObjectTypes,lb_SetCurSel,Cnt,0);
    end;
    CheckDlgButton(Handle,id_Invers,0);
  end;

Function TSelectDlg.CanClose
   : Boolean;
  var Cnt          : Integer;
  begin
    CanClose:=FALSE;
    Data^.Invers:=False;
    if SendDlgItemMsg(id_SourceList,lb_GetSelCount,0,0)=0 then begin
      MsgBox(Handle,841,mb_IconExclamation or mb_OK,'');
      SetFocus(GetItemHandle(id_SourceList));
    end
    else if SendDlgItemMsg(id_SourcePoint,lb_GetSelCount,0,0)=0 then begin
      MsgBox(Handle,841,mb_IconExclamation or mb_OK,'');
      SetFocus(GetItemHandle(id_SourcePoint));
    end
    else with Data^ do begin
      if IsDlgButtonChecked(Handle,id_Invers)=1 then Invers:=True;
      if LayersAreas<>NIL then Dispose(LayersAreas,Done);
      if LayersPoints<>NIL then Dispose(LayersPoints,Done);
      LayersAreas:=New(PLongColl,Init(5,5));
      LayersPoints:=New(PLongColl,Init(5,5));
      GetMultiLayerSelection(id_SourceList,TRUE,LayersAreas);
      GetMultiLayerSelection(id_SourcePoint,TRUE,LayersPoints);
      ObjectTypes:=[];
      for Cnt:=0 to maxType-1 do
          if SendDlgItemMsg(id_ObjectTypes,lb_GetSel,Cnt,0)<>0 then
        ObjectTypes:=ObjectTypes+[ObjType[Cnt]];
      CanClose:=TRUE;
    end;
  end;

Function TSelTree.Compare
   (
   Key1            : Pointer;
   Key2            : Pointer
   )
   : Integer;
  begin
    if PNode(Key1)^.Position^.X>PNode(Key2)^.Position^.X then Compare:=cpBigger
    else if PNode(Key1)^.Position^.X<PNode(Key2)^.Position^.X then Compare:=cpSmaller
    else begin
      if PNode(Key1)^.Position^.Y>PNode(Key2)^.Position^.Y then Compare:=cpBigger
      else if PNode(Key1)^.Position^.Y<PNode(Key2)^.Position^.Y then Compare:=cpSmaller
      else Compare:=cpEqual;
    end;
  end;

Procedure TSelTree.FreeItem
   (
   Item            : Pointer
   );
  begin
    Dispose(PNode(Item));
  end;


end.
