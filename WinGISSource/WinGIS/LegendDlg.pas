unit LegendDlg;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, WCtrls, ColumnList, AM_Proj, Legend, StyleDef,
  Menus, AM_Def, ExtCtrls, InplEdit
{$IFNDEF WMLT}
  , LegendAddDlg
{$ENDIF}
  , SymbolSelDlg, AM_Group,
  FontStyleDlg, XLines, XFills, XStyles, NumTools, AM_Paint, AM_Obj, StrTools,
  MultiLng, ResDlg, UserIntf;

const
  minDragMove = 2; { minimale Mausbewegung, um Drag zu starten  }
  LegDlgSection = 'LegendDialogs';

type
//   TBitmapNr    = (biVisible,biRectangle,biFilledRect,biHorzLine,
//                   biLine,biNone,biSymbol,biQuestion);
// defined in Legend.Pas
  TFieldIndex = (fiNone, fiType, fiText, fiLayer);

  TLegendDialog = class(TWForm)
    WLabel2: TWLabel;
    WLabel1: TWLabel;
    LegendListbox: TColumnListbox;
    RemoveLegendBtn: TSpeedButton;
    CopyLegendBtn: TSpeedButton;
    AddLegendBtn: TSpeedButton;
    LegendsCombo: TComboBox;
    TitleEdit: TEdit;
    RenameLegendBtn: TSpeedButton;
    PopupMenu1: TPopupMenu;
    AddEntry1: TMenuItem;
    DeleteEntry1: TMenuItem;
    LegendeSymbols: TImage;
    Up1: TMenuItem;
    Down1: TMenuItem;
    MoveToTop1: TMenuItem;
    MoveToBottom1: TMenuItem;
    N1: TMenuItem;
    ChooseSymbol1: TMenuItem;
    N2: TMenuItem;
    ChangeText1: TMenuItem;
    MlgSection1: TMlgSection;
    HierarchicBtn: TSpeedBtn;
    AlphabeticBtn: TSpeedBtn;
    Label2: TLabel;
    SearchEdit: TEdit;
    SearchBtn: TSpeedBtn;
    EntryStyleBtn: TSpeedBtn;
    TitleStyleBtn: TSpeedBtn;
    InplaceEditor: TInplaceEditor;
    Panel1: TPanel;
    Bevel1: TBevel;
    OkBtn: TButton;
    CancelBtn: TButton;
    chkScBar: TCheckBox;
    chkTransp: TCheckBox;
    procedure FormActivate(Sender: TObject);
    procedure AddLegendBtnClick(Sender: TObject);
    procedure LegendsComboChange(Sender: TObject);
    procedure RenameLegendBtnClick(Sender: TObject);
    procedure TitleEditChange(Sender: TObject);
    procedure RemoveLegendBtnClick(Sender: TObject);
    procedure CopyLegendBtnClick(Sender: TObject);
    procedure LegendListboxColumns1Paint(Control: TWinControl;
      Index: Integer; Rect: TRect; State: TOwnerDrawState);
    procedure LegendListboxColumns0Paint(Control: TWinControl;
      Index: Integer; Rect: TRect; State: TOwnerDrawState);
    procedure DeleteEntry1Click(Sender: TObject);
    procedure AddEntry1Click(Sender: TObject);
   //    procedure Button1Click(Sender: TObject);
    procedure LegendListboxMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Up1Click(Sender: TObject);
    procedure Down1Click(Sender: TObject);
    procedure LegendListboxClick(Sender: TObject);
    procedure MoveToTop1Click(Sender: TObject);
    procedure MoveToBottom1Click(Sender: TObject);
    procedure LegendListboxDblClick(Sender: TObject);
    procedure LegendListboxMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure LegendListboxDragDrop(Sender, Source: TObject; X,
      Y: Integer);
    procedure LegendListboxDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure LegendListboxMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure LegendListboxDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure ChangeText1Click(Sender: TObject);
    procedure LegendListboxColumns2Paint(Control: TWinControl;
      Index: Integer; Rect: TRect; State: TOwnerDrawState);
    procedure ChooseSymbol1Click(Sender: TObject);
    procedure TitleStyleBtnClick(Sender: TObject);
    procedure EntryStyleBtnClick(Sender: TObject);
    procedure SearchEditChange(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure AlphabeticBtnClick(Sender: TObject);
    procedure HierarchicBtnClick(Sender: TObject);
    procedure HierarchicBtnMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure InplaceEditorEdited(Sender: TObject; Index: Integer;
      const Text: string);
    procedure InplaceEditorEditing(Sender: TObject; Index: Integer;
      var AllowEdit: Boolean; var Text: string);
    procedure FormShow(Sender: TObject);
    procedure chkScBarClick(Sender: TObject);
    procedure chkTranspClick(Sender: TObject);
  private
       // list of layers in the project, contains TLayerProperties objects
    FLayerList: TList;
    FProject: PProj;
    FLegend: TLegend;
    FLegendList: TLegendList;
    FFirstAct: Boolean;
    FImages: TWImageList;
       // for symbol drawing...
    FPInfo: PPaint;

    FClickField: TFieldIndex;
    FCurSorting: Boolean;
    FMouseDownPos: TPoint;
    FLastIndex: TFieldIndex;
    FLastDrag: Integer;
    FLastSelected: Integer;
    FModified: Boolean;
    FPendingUps: Integer;

    procedure EnableButtons;
    procedure FillLegendFromLayerList(Legend: TLegend; Where: Integer; aLayerList: TList);
    procedure DoChangeEntryText(Index: Integer; const NewText: string);
    procedure DrawColumnText(Rect: TRect; State: TOwnerDrawState; Text: string);
    procedure DrawSymbol(AIndex: TBitmapNr; ARect: TRect);
    procedure DrawDlgLine(Rect: TRect; Layer: TLayerProperties);
    procedure DrawDlgFilledRect(Rect: TRect; Layer: TLayerProperties);
    procedure DrawDlgSymbol(Symbol: pSGroup; Rect: TRect; Layer: TLayerProperties);
    function FieldAtPos(Listbox: TWCustomListbox; Pos: TPoint): TFieldIndex;
    function FieldRect(Listbox: TWCustomListbox; AIndex: TFieldIndex; ATop: Integer): TRect;
    function FieldHint(Field: TFieldIndex): string;
    procedure EstablishHierarchicOrder;
    procedure MoveSelection(Index: Integer);
    function GetLayerWithNumber(Number: Integer): TLayerProperties;
    procedure UpdateEnabled;
  public
    constructor Create(aOwner: TComponent); override;
    property LayerList: TList read FLayerList write FLayerList;
    property Project: PProj read FProject write FProject;
    destructor Destroy; override;
    procedure SetNewCaption(ANewCaption: Integer);
    function AddLegend: Boolean;
    function RenameLegend: Boolean;
    function RemoveLegend: Boolean;
    function CopyLegend: Boolean;
  end;

procedure CopyLineStyle(FLineStyle: TLineStyle; LineStyle: TLineProperties);
procedure CopyFillStyle(FFillStyle: TFillStyle; FillStyle: TFillProperties);
procedure CopySymbolFill(FSymbolFill: TSymbolFill; SymbolFill: TSymbolFillProperties);

implementation

{$R *.DFM}

constructor TLegendDialog.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  FLegend := nil;
  FLegendList := nil;
  FFirstAct := True;
  FImages := TWImageList.CreateSize(16, 16);
  FImages.AddBitmaps(LegendeSymbols.Picture.Bitmap);
  // setup pinfo
  FPInfo := New(PPaint, Init);
  FPInfo^.ExtCanvas.Cached := FALSE;
  Dispose(FPInfo^.Fonts, Done);
  FPinfo^.Fonts := nil;
{$IFDEF WMLT}
  EntryStyleBtn.Visible := False;
  TitleStyleBtn.Visible := False;
  PopupMenu1.AutoPopup := False;
{$ENDIF}
end;

destructor TLegendDialog.Destroy;
begin
  FImages.Free;
   // free the clone
  if (FLegendList <> nil) then
    FLegendList.Free;
  Dispose(FPInfo, Done);
  inherited Destroy;
end;

procedure TLegendDialog.FormActivate(Sender: TObject);
var
  i: Integer;
begin
  if FFirstAct and (FProject <> nil) then
  begin
    // create a clone for modifications and if user press ok
    // execute them for the project
    FLegendList := FProject^.Legends.Clone;
    if FLegendList.CurrentLegendIndex >= 0 then
    begin
       //FLegendList.Legends[FLegendList.CurrentLegendIndex].EntryStyle.Assign(FProject^.Legends[FProject^.Legends.CurrentLegendIndex].EntryStyle);
       //FLegendList.Legends[FLegendList.CurrentLegendIndex].TitleStyle.Assign(FProject^.Legends[FProject^.Legends.CurrentLegendIndex].TitleStyle);
    end;
    with FLegendList do
    begin
      LegendsCombo.Items.Clear;
      for i := 0 to Count - 1 do
      begin
        LegendsCombo.Items.Add(Legends[i].Name);
      end; // for
      // latter select current legend
      FLegend := nil;
      if (Count > 0) and (CurrentLegendIndex <> -1) then
      begin
        LegendsCombo.ItemIndex := CurrentLegendIndex;
        LegendsCombo.Invalidate;
        FLegend := Legends[CurrentLegendIndex];
      end;
    end; // with
    FFirstAct := False;
  end;
  EnableButtons;
end;

procedure TLegendDialog.EnableButtons;
var
  OneSelected: Boolean;
begin
  OneSelected := FLegend <> nil;
{$IFNDEF WMLT}
  AddLegendBtn.Enabled := True;
  RenameLegendBtn.Enabled := OneSelected;
  CopyLegendBtn.Enabled := OneSelected;
  RemoveLegendBtn.Enabled := OneSelected;
{$ENDIF}
  TitleEdit.Enabled := OneSelected;
//++ Glukhov Bug#458 Build#166 15.11.01
  chkScBar.Enabled := OneSelected;
//-- Glukhov Bug#458 Build#166 15.11.01
  chkTransp.Enabled := OneSelected;
  LegendListBox.Enabled := OneSelected;
  WLabel1.Enabled := OneSelected;
  TitleStyleBtn.Enabled := OneSelected;
  EntryStyleBtn.Enabled := OneSelected;
  SearchBtn.Enabled := False;
  SetControlEnabled(SearchEdit, OneSelected);
  HierarchicBtn.Enabled := OneSelected;
  AlphabeticBtn.Enabled := OneSelected;
  if OneSelected then
  begin
    TitleEdit.Text := FLegend.Title;
//++ Glukhov Bug#458 Build#166 15.11.01
    chkScBar.Checked := FLegend.bScaleBar;
//-- Glukhov Bug#458 Build#166 15.11.01
    chkTransp.Checked := FLegend.bTransparent;
    AlphabeticBtn.Down := FLegend.Sorted;
    HierarchicBtn.Down := not FLegend.Sorted;
    LegendListBox.Count := FLegend.Count;
  end;
  UpdateEnabled;
end;

procedure TLegendDialog.AddLegendBtnClick(Sender: TObject);
var
  Legend: TLegend;
  aStr: AnsiString;
begin
  with FLegendList do
  begin
    aStr := MlgStringList[LegDlgSection, 19];
    if InputQuery(MlgStringList[LegDlgSection, 17],
      MlgStringList[LegDlgSection, 18], aStr) then
    begin
      Legend := NewLegend(aStr);
      if Assigned(Legend) then
      begin
        FillLegendFromLayerList(Legend, 0, FLayerList);
        with LegendsCombo do
        begin
          Items.Add(Legend.Name);
          ItemIndex := Items.IndexOf(Legend.Name);
        end;
        FLegend := Legend;
        LegendListBox.Invalidate;
        EnableButtons;
      end
      else
        MessageDialog(MlgStringList[LegDlgSection, 20], mtInformation, [mbOk], 0);
    end; // if
  end; // with
end;

function TLegendDialog.AddLegend: Boolean;
var
  Legend: TLegend;
  aStr: AnsiString;
begin
  Result := FALSE;
  with FLegendList do
  begin
    aStr := MlgStringList[LegDlgSection, 19];
    if InputQuery(MlgStringList[LegDlgSection, 17], MlgStringList[LegDlgSection, 18], aStr) then
    begin
      Legend := NewLegend(aStr);
      if Assigned(Legend) then
      begin
        Legend.Title := Legend.Name;
        FillLegendFromLayerList(Legend, 0, FLayerList);
        with LegendsCombo do
        begin
          Items.Add(Legend.Name);
          ItemIndex := Items.IndexOf(Legend.Name);
        end;
        FLegend := Legend;
        LegendListBox.Invalidate;
        EnableButtons;
        Result := TRUE;
      end
      else
        MessageDialog(MlgStringList[LegDlgSection, 20], mtInformation, [mbOk], 0);
    end;
  end;
end;

procedure TLegendDialog.FillLegendFromLayerList
  (
  Legend: TLegend;
  Where: Integer;
  aLayerList: TList
  );

var
  LayerProp: TLayerProperties;
  Entry: TLegendEntry;
  i: Integer;
begin
  for i := 0 to aLayerList.Count - 1 do
  begin
    LayerProp := TLayerProperties(aLayerList.Items[i]);
    Entry := TLegendEntry.Create;
    with Entry do
    begin
      FText := LayerProp.Name; //LayerProp.Name;
      FLayerNumber := LayerProp.Number; // unique for layer
      FType := leFilledRect;
      FSymName := '';
      FNameFromLayer := True;
      // copy layer styles
{      CopyLineStyle(FLineStyle,LayerProp.LineStyle);
      CopyFillStyle(FFillStyle,LayerProp.FillStyle);
      CopySymbolFill(FSymbolFill,LayerProp.SymbolFill);}
    end;
    Legend.Insert(Where + i, Entry);
  end; // for
end;

procedure TLegendDialog.LegendsComboChange(Sender: TObject);
var
  i: Integer;
begin
  i := LegendsCombo.ItemIndex;
  if i <> -1 then
  begin
    FLegend := FLegendList.LegendByName[LegendsCombo.Items[i]];
    FLegendList.CurrentLegendIndex := i;
  end
  else
  begin
    FLegend := nil;
    FLegendList.CurrentLegendIndex := -1;
  end;
  EnableButtons;
  LegendListBox.Invalidate;
end;

procedure TLegendDialog.RenameLegendBtnClick(Sender: TObject);
var
  Legend: TLegend;
  aStr: AnsiString;
  i: Integer;
begin
  with FLegendList do
  begin
    aStr := FLegend.Name;
    if InputQuery(MlgStringList[LegDlgSection, 21],
      MlgStringList[LegDlgSection, 22], aStr) then
    begin
      Legend := LegendByName[aStr];
      // look if name exists already
      if not Assigned(Legend) then
      begin
        // change legend name
        FLegend.Name := aStr;
        // change text in combobox
        with LegendsCombo do
        begin
          i := ItemIndex; // funny bug workaround
          Items[i] := aStr;
          ItemIndex := i;
        end;
      end
      else
        MessageDialog(MlgStringList[LegDlgSection, 20], mtInformation, [mbOk], 0);
    end; // if
  end; // with
end;

function TLegendDialog.RenameLegend: Boolean;
var
  Legend: TLegend;
  aStr: AnsiString;
  i: Integer;
begin
  Result := FALSE;
  with FLegendList do
  begin
    aStr := FLegend.Name;
    if InputQuery(MlgStringList[LegDlgSection, 21], MlgStringList[LegDlgSection, 22], aStr) then
    begin
      Legend := LegendByName[aStr];
      // look if name exists already
      if not Assigned(Legend) then
      begin
        // change legend name
        FLegend.Name := aStr;
        // change text in combobox
        with LegendsCombo do
        begin
          i := ItemIndex; // funny bug workaround
          Items[i] := aStr;
          ItemIndex := i;
        end;
        Result := TRUE;
      end
      else
        MessageDialog(MlgStringList[LegDlgSection, 20], mtInformation, [mbOk], 0);
    end;
  end;
end;

procedure TLegendDialog.TitleEditChange(Sender: TObject);
begin
  FLegend.Title := TitleEdit.Text;
end;

procedure TLegendDialog.RemoveLegendBtnClick(Sender: TObject);
var
  i: Integer;
begin
  with FLegendList do
  begin
    if MessageDialog(MlgStringList[LegDlgSection, 23],
      mtInformation, [mbOk, mbCancel], 0) = mrOk then
    begin
      DeleteLegend(FLegend.Name);
      FLegend := nil;
      with LegendsCombo do
      begin
        i := ItemIndex;
        ItemIndex := -1;
        Items.Delete(i);
      end;
      LegendListBox.Count := 0;
      EnableButtons;
    end; // if
  end; // with
end;

function TLegendDialog.RemoveLegend: Boolean;
var
  i: Integer;
begin
  Result := FALSE;
  with FLegendList do
  begin
    if MessageDialog(MlgStringList[LegDlgSection, 23], mtInformation, [mbOk, mbCancel], 0) = mrOk then
    begin
      DeleteLegend(FLegend.Name);
      FLegend := nil;
      with LegendsCombo do
      begin
        i := ItemIndex;
        ItemIndex := -1;
        Items.Delete(i);
      end;
      LegendListBox.Count := 0;
      EnableButtons;
      Result := TRUE;
    end;
  end;
end;

procedure TLegendDialog.CopyLegendBtnClick(Sender: TObject);
var
  Legend: TLegend;
  aStr: AnsiString;
begin
  with FLegendList do
  begin
    aStr := FLegend.Name;
    if InputQuery(MlgStringList[LegDlgSection, 24],
      MlgStringList[LegDlgSection, 25], aStr) then
    begin
      Legend := NewLegend(aStr);
      if Assigned(Legend) then
      begin
        CopyLegend(Legend, FLegend);
        // CopyLegend copies legend name too
        Legend.Name := aStr;
        with LegendsCombo do
        begin
          Items.Add(Legend.Name);
          ItemIndex := Items.IndexOf(Legend.Name);
        end;
        FLegend := Legend;
        EnableButtons;
        LegendListBox.Invalidate;
      end
      else
        MessageDialog(MlgStringList[LegDlgSection, 20], mtInformation, [mbOk], 0);
    end; // if
  end; // with
end;

function TLegendDialog.CopyLegend: Boolean;
var
  Legend: TLegend;
  aStr: AnsiString;
begin
  Result := FALSE;
  with FLegendList do
  begin
    aStr := FLegend.Name;
    if InputQuery(MlgStringList[LegDlgSection, 24], MlgStringList[LegDlgSection, 25], aStr) then
    begin
      Legend := NewLegend(aStr);
      if Assigned(Legend) then
      begin
        CopyLegend(Legend, FLegend);
        // CopyLegend copies legend name too
        Legend.Name := aStr;
        with LegendsCombo do
        begin
          Items.Add(Legend.Name);
          ItemIndex := Items.IndexOf(Legend.Name);
        end;
        FLegend := Legend;
        EnableButtons;
        LegendListBox.Invalidate;
        Result := TRUE;
      end
      else
        MessageDialog(MlgStringList[LegDlgSection, 20], mtInformation, [mbOk], 0);
    end;
  end;
end;

function TLegendDialog.GetLayerWithNumber
  (
  Number: Integer
  )
  : TLayerProperties;
var
  i: Integer;
  LayerProp: TLayerProperties;
begin
  Result := nil;
  for i := 0 to FLayerList.Count - 1 do
  begin
    LayerProp := TLayerProperties(FLayerList.Items[i]);
    // find layer with right number
    if Assigned(LayerProp) then
      if LayerProp.Number = Number then
      begin
        Result := LayerProp;
        Exit;
      end;
  end;
end;

procedure TLegendDialog.LegendListboxColumns1Paint(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
  Entry: TLegendEntry;
  LayerProp: TLayerProperties;
begin
  if FLegend = nil then
    Exit;
  if (FLegend.Count > Index) and (Index > -1) then
  begin
    Entry := FLegend.Items[Index];
    if Assigned(Entry) then
      with Entry do
      begin
        // sync layername change
        if FNameFromLayer then
        begin
          LayerProp := GetLayerWithNumber(FLayerNumber);
          if Assigned(LayerProp) then
          begin
            FText := LayerProp.Name;
            FLegend.Items[Index] := Entry;
          end;
        end;
        DrawColumnText(Rect, State, FText)
      end;
  end;
end;

procedure TLegendDialog.DrawColumnText
  (
  Rect: TRect;
  State: TOwnerDrawState;
  Text: string
  );
begin
  with LegendListBox, Canvas do
  begin
    Brush.Color := clWindow;
    Font.Color := clWindowText;
    if odSelected in State then
    begin
      Brush.Color := clHighlight;
      Font.Color := clHighlightText
    end
    else
      if odGrayed in State then
      begin
        Brush.Color := clBtnFace;
        Font.Color := clBtnText
      end;
    InflateRect(Rect, -3, 0);
    TextRect(Rect, Rect.Left + 2, CenterHeightInRect(TextHeight('A'), Rect), Text);
    if odFocused in State then
      DrawFocusRect(Rect);
  end;
end;

  {*****************************************************************************
  | Procedure DrawSymbol
  |-----------------------------------------------------------------------------
  | Zeichnet das einem Feld zugeordnete Symbol in das angegebene Rechteck.
  |-----------------------------------------------------------------------------
  | in AIndex = Nummer des Feldes (fiXXXX)
  | in ARect = Rechteck in das das Symbol gezeichnet werden soll
  +****************************************************************************}

procedure TLegendDialog.DrawSymbol
  (
  AIndex: TBitmapNr;
  ARect: TRect
  );
var
  X: Integer;
  Y: Integer;
begin
  X := ARect.Left + (ARect.Right - ARect.Left - 16) div 2;
  Y := ARect.Top + (ARect.Bottom - ARect.Top - 16) div 2;
  FImages.Draw(LegendListbox.Canvas, X, Y, Ord(AIndex));
end;

procedure TLegendDialog.LegendListboxColumns0Paint(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
  Entry: TLegendEntry;
  LayerProp: TLayerProperties;
  Symbol: pSGroup;
begin
  if FLegend = nil then
    Exit;
  if (FLegend.Count > Index) and (Index > -1) then
  begin
    Entry := FLegend.Items[Index];
    if Assigned(Entry) then
    begin
      LayerProp := GetLayerWithNumber(Entry.FLayerNumber);
      case Entry.FType of
        leLine: DrawDlgLine(Rect, LayerProp);
        leFilledRect: DrawDlgFilledRect(Rect, LayerProp);
        leSymbol:
          begin
            Symbol := pSymbols(FProject^.PInfo^.Symbols)^.GetSymbol(Entry.FSymName,
              Entry.FLibName);
            DrawDlgSymbol(Symbol, Rect, LayerProp);
          end;
      end;
    end;
  end;
end;

{******************************************************************************+
I Procedure DrawDlgLine                                                        I
I draws a diagonal line inside Rect in the style of Layer                      I
+******************************************************************************}

procedure TLegendDialog.DrawDlgLine
  (
  Rect: TRect;
  Layer: TLayerProperties
  );
var
  ClipBox: TRect;
  Region: THandle;
  XLineStyle: TXLineStyle;
  Points: array[0..1] of TPoint;
begin
  if Layer = nil then
    Exit;
  InflateRect(Rect, -3, -3);
  with LegendListBox, Canvas, Layer do
  begin
    with LineStyle do
      if ValidValues then
      begin
        Brush.Color := clWindow;
        if (Style >= lt_UserDefined) and
          (Style < FProject^.PInfo^.ProjStyles.XLineStyles.Count) then
        begin
        { draw user-defined type, set scale that line covers 4/5 of the item-height }
        // [!!!]
        // if userdefined style doesn't exists already draw symbolic line
        // just new created user-defines styles are only known in layerdialog (cannot
        // be accessed here -> later correction ray
          XLineStyle := FProject^.PInfo^.ProjStyles.XLineStyles[Style];
          with Rect do
          begin
            Points[0] := Point(Left, Bottom);
            Points[1] := Point(Right, Top);
            XPolyline(Handle, Points, 2, XLineStyle.StyleDef^.XLineDefs,
              XLineStyle.StyleDef^.Count, LineStyle.Color,
              LineStyle.FillColor, FALSE,
              (ItemHeight - 2) / RectHeight(XLineStyle.ClipRect) / 5);
          end;
        end
        else
        begin
        { draw windows line-style }
          Pen.Handle := GetPen(Color, Style, Round(Width));
          GetClipBox(Handle, ClipBox);
          with Rect do
            IntersectClipRect(Handle, Left, Top, Right, Bottom);
          MoveTo(Rect.Left, Rect.Bottom);
          LineTo(Rect.Right, Rect.Top + 1);
          MoveTo(Rect.Left, Rect.Bottom - 1);
          LineTo(Rect.Right, Rect.Top);
          with ClipBox do
            Region := CreateRectRgn(Left, Top, Right, Bottom);
          SelectClipRgn(Handle, Region);
          DeleteObject(Region);
        end;
      end
  end;
end;

{******************************************************************************+
I Procedure DrawDlgFilledRect                                                  I
I draws a filled rect inside Rect in the style of Layer                        I
+******************************************************************************}

procedure TLegendDialog.DrawDlgFilledRect
  (
  Rect: TRect;
  Layer: TLayerProperties
  );
var
  XLineStyle: TXLineStyle;
  XFillStyle: TCustomXFill;
  Points: array[0..4] of TPoint;
begin
  if (Layer = nil) or (FProject = nil) then
    Exit;
  InflateRect(Rect, -3, -1);
  with LegendListBox, Canvas, Layer do
  begin
    with FillStyle do
      if ValidValues then
      begin
        with LineStyle do
          if ValidValues then
          begin
            Pen.Handle := 0;
            Brush.Handle := 0;
      // build drawing rectangle
            InflateRect(Rect, -1, -1);
            with Rect do
            begin
              Points[0] := Point(Left, Top);
              Points[1] := Point(Right, Top);
              Points[2] := Point(Right, Bottom);
              Points[3] := Point(Left, Bottom);
              Points[4] := Point(Left, Top);
            end;
      // draw windows or draw userdefined fill styles
            if Pattern >= pt_UserDefined then
            begin
        // style present ?(if layer style is changed, style isn't saved immediately)
              if Pattern < FProject^.PInfo^.ProjStyles.XFillStyles.Count then
              begin
            { draw user-defined fill-style }
                XFillStyle := FProject^.PInfo^.ProjStyles.XFillStyles[Pattern];
                if XFillStyle is TVectorXFill then
                  with XFillStyle as TVectorXFill do
                  begin
                    XLFillPolygon(Handle, Points, 4, StyleDef^.XFillDefs, StyleDef^.Count,
                      0, 0, 5 / MinDistance, 0);
                  end
                else
                  if XFillStyle is TBitmapXFill then
                    with XFillStyle as TBitmapXFill do
                      BMFillPolygon(Handle, Points, 4, StyleDef(Handle)^.BitmapHandle, srcCopy, 1);
              end;
              Brush.Handle := 0;
            end
            else
            begin
              if Pattern <> 0 then
              begin
          // setup windows-fill-type
                Pen.Style := psClear;
                Brush.Handle := GetBrush(ForeColor, Pattern, FProject^.PInfo^.ProjStyles.XFillStyles);
                if BackColor = clNone then
                  SetBKMode(Handle, Transparent)
                else
                  SetBKColor(Handle, BackColor);
                with Rect do
                  Rectangle(Left, Top, Right + 1, Bottom + 1);
              end
            end;
            Pen.Style := psClear;
      // setup pen style
            if (Style >= lt_UserDefined) and
              (Project^.PInfo^.ProjStyles.XLineStyles.Count <= Style) then
            begin
              XLineStyle := Project^.PInfo^.ProjStyles.XLineStyles[Style];
              InflateRect(Rect, -1, -1);
              with Rect do
              begin
                Points[0] := Point(Left, Top);
                Points[1] := Point(Right, Top);
                Points[2] := Point(Right, Bottom);
                Points[3] := Point(Left, Bottom);
                Points[4] := Point(Left, Top);
                XPolygon(Handle, Points, 4, XLineStyle.StyleDef^.XLineDefs,
                  XLineStyle.StyleDef^.Count, LineStyle.Color,
                  LineStyle.FillColor, FALSE,
                  (ItemHeight - 2) / RectHeight(XLineStyle.ClipRect) / 5);
              end;
            end
            else
            begin
              Pen.Handle := GetPen(Color, Style, Round(Width));
              with Rect do
                Rectangle(Left, Top, Right, Bottom);
              Brush.Handle := 0;
              Pen.Handle := 0;
            end;
          end;
      end; { with linestyle }
  end; { with fillstyle }
end;

{******************************************************************************+
I Procedure DrawDlgSymbol                                                      I
I draws a symbol inside Rect in the style of Layer                             I
I (changes should also be done in ViewManager.Pas)                             I
+******************************************************************************}

procedure TLegendDialog.DrawDlgSymbol
  (
  Symbol: pSGroup;
  Rect: TRect;
  Layer: TLayerProperties
  );

  procedure IDrawSymbol;
  begin
    with LegendListBox, Canvas do
    begin
      if Symbol = nil then
        DrawSymbol(biSymbol, Rect)
      else
      begin
        FPInfo^.ExtCanvas.Handle := Canvas.Handle;
        Symbol^.DrawSymbolInRect(FPInfo, Rect);
      end;
    end;
  end;

begin
  if (Layer = nil) or (Project = nil) then
    Exit;
  InflateRect(Rect, -3, 0);
  with LegendListBox, Canvas, Layer do
  begin
    with FillStyle do
      if ValidValues then
      begin
        with LineStyle do
          if ValidValues then
          begin
      // clear background
            Pen.Style := psClear;
            Pen.Color := clWindow;
            Brush.Color := clWindow;
            Canvas.FillRect(Rect);
      // "default style"
            Pen.Handle := GetPen(Color, Style, Round(Width));
            Brush.Handle := 0;
            if Pattern >= pt_UserDefined then
            begin
        { draw user-defined fill-style }
        // only if style is present
        // setup user defined style [!!!]
{        if Project^.PInfo^.ProjStyles.XFillStyles.Count>Pattern then begin
          XFillStyle:=FProject^.PInfo^.ProjStyles.XFillStyles[Pattern];
          if XFillStyle.FillType=xftBitmap then begin
            end else begin
            end;
          end else begin}
            end
            else
            begin
              if Pattern <> 0 then
              begin
          // setup windows-fill-type and linetype
                Brush.Handle := GetBrush(ForeColor, Pattern, FProject^.PInfo^.ProjStyles.XFillStyles);
                if BackColor = clNone then
                  SetBKMode(Handle, Transparent)
                else
                  SetBKColor(Handle, BackColor);
              end;
            end;
// draw symbol (or if impossible a symbol bitmap)
            IDrawSymbol;
// reset brush'n pen
            Brush.Handle := 0;
            Pen.Handle := 0;
          end;
      end; { with linestyle }
  end; { with fillstyle }
end;

procedure TLegendDialog.DeleteEntry1Click(Sender: TObject);
var
  i: Integer;
begin
  with LegendListBox do
  begin
    if SelCount > 1 then
    begin
      if MessageDialog(MlgStringList[LegDlgSection, 26],
        mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        ItemIndex := -1;
        for i := Count - 1 downto 0 do
          if Selected[i] then
          begin
            FLegend.Delete(i);
            Items.Delete(i);
          end;
        UpdateEnabled;
      end
    end
    else
    begin
      if ItemIndex <> -1 then
        if MessageDialog(MlgStringList[LegDlgSection, 27],
          mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          i := ItemIndex;
          ItemIndex := -1;
          FLegend.Delete(i);
          Items.Delete(i);
          UpdateEnabled;
        end;
    end;
  end; // with
end;

procedure TLegendDialog.AddEntry1Click(Sender: TObject);
{$IFNDEF WMLT}
var
  AddDlg: TLegendAddDialog;
  aList: TList;
  Where: Integer;
{$ENDIF}
begin
{$IFNDEF WMLT}
// open add dialog...
  AddDlg := TLegendAddDialog.Create(self);
  try
    // choose from all available layers
    AddDlg.LayerList := FLayerList;
    if AddDlg.ShowModal = mrOk then
    begin
      // get selected layers
      aList := AddDlg.Selected;
      try
        Where := 0;
        with LegendListBox do
          if ItemIndex <> -1 then
            Where := ItemIndex;
        // insert selected layers before selected entry
        FillLegendFromLayerList(FLegend, Where, aList);
        // adjust list box
        LegendListBox.Count := FLegend.Count;
        LegendListBox.Invalidate;
      finally
        aList.Free;
      end;
    end;
  finally
    AddDlg.Free;
  end;
{$ENDIF}
end;

function TLegendDialog.FieldAtPos
  (
  Listbox: TWCustomListbox;
  Pos: TPoint
  )
  : TFieldIndex;
begin
  if (Pos.X >= 0) and (Pos.X <= 24) then
    Result := fiType
  else
    if (Pos.X > 24) and (Pos.X <= 270) then
      Result := fiText
    else
      if (Pos.X > 270) and (Pos.X <= 469) then
        Result := fiLayer
      else
        Result := fiNone;
end;

function TLegendDialog.FieldHint(Field: TFieldIndex): string;
begin
  case Field of
    fiType: Result := MlgStringList[LegDlgSection, 28];
    fiText: Result := MlgStringList[LegDlgSection, 29];
    fiLayer: Result := MlgStringList[LegDlgSection, 30];
  else
    Result := '';
  end;
end;

function TLegendDialog.FieldRect(Listbox: TWCustomListbox; AIndex: TFieldIndex;
  ATop: Integer): TRect;
begin
  Result := LegendListBox.ItemRect(ATop);
  case AIndex of
    fiType:
      begin
        Result.Left := 1;
        Result.Right := 25;
      end;
    fiText:
      begin
        Result.Left := 26;
        Result.Right := 242;
      end;
    fiLayer:
      begin
        Result.Left := 243;
        Result.Right := 393;
      end;
  end;
end;

procedure TLegendDialog.LegendListboxMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  Field: TFieldIndex;
  Index: Integer;
begin
  if Button = mbLeft then
  begin
    Index := LegendListBox.ItemAtPos(Point(X, Y), TRUE);
    with LegendListBox do
      if FPendingUps > 0 then
        Dec(FPendingUps)
      else
        if Index >= 0 then
        begin
          Field := FieldAtPos(LegendListBox, Point(X, Y));
          if (FLastSelected = -1) and (Field in [fiText]) then
            ItemIndex := Index;
          if (SelCount = 1)
            and (Field = FClickField)
            and (Field in [fiType])
            and Selected[Index] then
          begin
            FLegend.Items[Index].ToggleType;
            LegendListBox.Invalidate;
            UpdateEnabled;
            FModified := TRUE;
          end;
        end;
  end;
end;

procedure TLegendDialog.LegendListboxMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
var
  Index: TFieldIndex;
begin
  with LegendListBox do
  begin
    if not Dragging and not FCurSorting and (FClickField = fiText)
      and (ssLeft in Shift) and ((Abs(FMouseDownPos.X - X) >= minDragMove)
      or (Abs(FMouseDownPos.Y - Y) >= minDragMove)) then
    begin
      FPendingUps := 2;
      FLastDrag := -1;
      BeginDrag(TRUE);
    end;
    Index := FieldAtPos(LegendListBox, Point(X, Y));
    if Index <> FLastIndex then
    begin
      Application.CancelHint;
      FLastIndex := Index;
      LegendListBox.Hint := FieldHint(Index);
    end
    else
      LegendListBox.Hint := FieldHint(Index);
    DoInherited := FALSE;
  end;
end;

procedure TLegendDialog.LegendListboxMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  Index: Integer;
begin
  if Button = mbLeft then
    with TWCustomListbox(Sender) do
    begin
      FMouseDownPos := Point(X, Y);
      Index := ItemAtPos(FMouseDownPos, TRUE);
      FLastSelected := -1;
      if Index < 0 then
        FClickField := fiNone
      else
      begin
        FClickField := FieldAtPos(TWCustomListbox(Sender), FMouseDownPos);
        if (Shift * [ssShift, ssCtrl] <> []) then
          FLastSelected := Index
        else
        begin
          DoInherited := SelCount <= 1;
          if not Selected[Index] then
          begin
            FLastSelected := Index;
            DoInherited := DoInherited or (FClickField in [fiText]);
          end
          else
            if FClickField in [fiText] then
              FocusedIndex := Index;
        end;
      end;
    end;
end;

procedure TLegendDialog.LegendListBoxDragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
var
  Index: Integer;
begin
  with LegendListBox do
  begin
    if State = dsDragLeave then
      Index := -1
    else
      Index := ItemAtPos(Point(X, Y), TRUE);
    Accept := (Y >= 0)
      and (Source = LegendListBox)
      and ((Index = -1) or not Selected[Index]);
    if Index <> FLastDrag then
    begin
      if (FLastDrag > -1)
        and not Selected[FLastDrag] then
        LegendListBoxDrawItem(LegendListBox,
          FLastDrag, FieldRect(LegendListBox, fiText, FLastDrag), []);
      FLastDrag := Index;
      if (FLastDrag > -1)
        and not Selected[FLastDrag] then
        LegendListBoxDrawItem(LegendListBox,
          FLastDrag, FieldRect(LegendListBox, fiText, FLastDrag), [odGrayed]);
    end;
  end;
end;

procedure TLegendDialog.LegendListBoxDragDrop(Sender, Source: TObject; X, Y: Integer);
var
  Index: Integer;
begin
  with LegendListBox do
  begin
    Index := ItemAtPos(Point(X, Y), TRUE);
    if Index = -1 then
      Index := Count - 1;
    SELF.MoveSelection(Index);
  end;
end;

procedure TLegendDialog.MoveSelection(Index: Integer);
var
  Cnt: Integer;
  Inserted: Integer;
  FSelected: TList;
begin
  with LegendListbox do
  begin
    FSelected := TList.Create;
    try
      Inserted := 0;
      for Cnt := Count - 1 downto 0 do
        if Selected[Cnt] then
        begin
          FSelected.Add(FLegend.Items[Cnt]);
          FLegend.Delete(Cnt);
          if Cnt <= Index then
            Inc(Inserted);
        end;
      if Inserted > 0 then
        Index := Index - Inserted + 1;
      for Cnt := 0 to FSelected.Count - 1 do
        FLegend.Insert(Index, FSelected[Cnt]);
      Selected[-1] := FALSE;
      RangeSelected[Index, Index + FSelected.Count - 1] := TRUE;
      SendMessage(Handle, lb_SetSel, 1, Index);
      LegendListBox.Invalidate;
      UpdateEnabled;
    finally
      FSelected.Free;
    end;
  end;
end;

// for testing legend.draw
{procedure TLegendDialog.Button1Click(Sender: TObject);
begin
  FLegend.Draw(FProject^.PInfo,FProject^.Layers,FProject^.Size);
end;}

// move selected entry up

procedure TLegendDialog.Up1Click(Sender: TObject);
var
  Entry: TLegendEntry;
begin
  with LegendListBox do
  begin
    if ItemIndex > 0 then
    begin
      Entry := FLegend.Items[ItemIndex - 1];
      FLegend.Items[ItemIndex - 1] := FLegend.Items[ItemIndex];
      FLegend.Items[ItemIndex] := Entry;
      ItemIndex := ItemIndex - 1;
      LegendListBoxClick(Sender);
      Invalidate;
    end;
  end; // with
end;

procedure TLegendDialog.Down1Click(Sender: TObject);
var
  Entry: TLegendEntry;
begin
  with LegendListBox do
  begin
    if (ItemIndex >= 0) and (ItemIndex < Items.Count - 1) then
    begin
      Entry := FLegend.Items[ItemIndex + 1];
      FLegend.Items[ItemIndex + 1] := FLegend.Items[ItemIndex];
      FLegend.Items[ItemIndex] := Entry;
      ItemIndex := ItemIndex + 1;
      LegendListBoxClick(Sender);
      Invalidate;
    end;
  end; // with
end;

procedure TLegendDialog.LegendListboxClick(Sender: TObject);
begin
  UpdateEnabled;
end;

procedure TLegendDialog.UpdateEnabled;
begin
  with LegendListBox do
  begin
    Up1.Enabled := (SelCount > 0) and (ItemIndex > 0);
    MoveToTop1.Enabled := (SelCount > 0) and (ItemIndex > 0);
    Down1.Enabled := (SelCount > 0) and (ItemIndex < Items.Count - 1);
    MoveToBottom1.Enabled := (SelCount > 0) and (ItemIndex < Items.Count - 1);
    DeleteEntry1.Enabled := ItemIndex <> -1;
    ChangeText1.Enabled := SelCount = 1;
    if (ItemIndex <> -1) and (FLegend <> nil) then
      ChooseSymbol1.Enabled := FLegend.Items[ItemIndex].FType = leSymbol
    else
      ChooseSymbol1.Enabled := False;
  end;
end;

procedure TLegendDialog.MoveToTop1Click(Sender: TObject);
var
  Entry: TLegendEntry;
  i: Integer;
begin
  with LegendListBox do
  begin
    if ItemIndex > 0 then
    begin
      Entry := FLegend.Items[ItemIndex];
      for i := ItemIndex - 1 downto 0 do
      begin
        FLegend.Items[i + 1] := FLegend.Items[i];
      end;
      FLegend.Items[0] := Entry;
      ItemIndex := 0;
      LegendListBoxClick(Sender);
      Invalidate;
    end;
  end; // with
end;

procedure TLegendDialog.MoveToBottom1Click(Sender: TObject);
var
  Entry: TLegendEntry;
  i: Integer;
begin
  with LegendListBox do
  begin
    if (ItemIndex >= 0) and (ItemIndex < Items.Count - 1) then
    begin
      Entry := FLegend.Items[ItemIndex];
      for i := ItemIndex to Items.Count - 2 do
      begin
        FLegend.Items[i] := FLegend.Items[i + 1];
      end;
      FLegend.Items[Items.Count - 1] := Entry;
      ItemIndex := Items.Count - 1;
      LegendListBoxClick(Sender);
      Invalidate;
    end;
  end; // with
end;

{*******************************************************************************
I style copy functions                                                         I
*******************************************************************************}

procedure CopyLineStyle(FLineStyle: TLineStyle; LineStyle: TLineProperties);
begin
  FLineStyle.Style := LineStyle.Style;
  FLineStyle.Width := LineStyle.Width;
  FLineStyle.WidthType := LineStyle.WidthType;
  FLineStyle.Scale := LineStyle.Scale;
  FLineStyle.Color := LineStyle.Color;
  FLineStyle.FillColor := LineStyle.FillColor;
end;

procedure CopyFillStyle(FFillStyle: TFillStyle; FillStyle: TFillProperties);
begin
  FFillStyle.Pattern := FillStyle.Pattern;
  FFillStyle.ForeColor := FillStyle.ForeColor;
  FFillStyle.BackColor := FillStyle.BackColor;
  FFillStyle.Scale := FillStyle.Scale;
  FFillStyle.ScaleType := FillStyle.ScaleType;
end;

procedure CopySymbolFill(FSymbolFill: TSymbolFill; SymbolFill: TSymbolFillProperties);
begin
  FSymbolFill.FillType := SymbolFill.FillType;
  FSymbolFill.Symbol := SymbolFill.Symbol;
  FSymbolFill.Size := SymbolFill.Size;
  FSymbolFill.SizeType := SymbolFill.SizeType;
  FSymbolFill.SymbolAngle := SymbolFill.SymbolAngle;
  FSymbolFill.DistanceType := SymbolFill.DistanceType;
  FSymbolFill.Distance := SymbolFill.Distance;
  FSymbolFill.LineDistance := SymbolFill.LineDistance;
  FSymbolFill.Offset := SymbolFill.Offset;
  FSymbolFill.OffsetType := SymbolFill.OffsetType;
  FSymbolFill.Angle := SymbolFill.Angle;
  CopyLineStyle(FSymbolFill.LineStyle, SymbolFill.LineStyle);
  CopyFillStyle(FSymbolFill.FillStyle, SymbolFill.FillStyle);
end;

procedure TLegendDialog.LegendListBoxDblClick(Sender: TObject);
var
  Cnt: Integer;
  Field: TFieldIndex;
  Index: Integer;
  ToEdit: TList;
begin
  with LegendListBox do
  begin
    Field := FieldAtPos(LegendListBox, FMouseDownPos);
    Index := ItemAtPos(FMouseDownPos, TRUE);
    if (Field <> fiNone)
      and (Index >= 0)
      and ((SelCount > 1)
      or (Field in [fiType])) then
    begin
      ToEdit := TList.Create;
      try
        for Cnt := 0 to Count - 1 do
          if Selected[Cnt] then
            ToEdit.Add(FLegend.Items[Cnt]);
        FLegend.Items[Index].ToggleType;
        for Cnt := 0 to Count - 1 do
          if Selected[Cnt] then
          begin
            FLegend.Items[Cnt].FType := FLegend.Items[Index].FType;
            FModified := TRUE;
          end;
        LegendListBox.Invalidate;
      finally
        ToEdit.Free;
      end;
    end;
  end;
end;

procedure TLegendDialog.LegendListboxDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  LegendListboxColumns0Paint(Control, Index, Rect, State);
  LegendListboxColumns1Paint(Control, Index, Rect, State);
  LegendListboxColumns2Paint(Control, Index, Rect, State);
end;

procedure TLegendDialog.ChangeText1Click(Sender: TObject);
var
  aStr, bStr: string;
  Entry: TLegendEntry;
begin
  if FLegend = nil then
    Exit;
  with LegendListBox do
    if ItemIndex <> -1 then
    begin
      Entry := FLegend.Items[ItemIndex];
      aStr := Entry.FText;
      bStr := InputBox(MlgStringList[LegDlgSection, 31],
        MlgStringList[LegDlgSection, 32], aStr);
      if aStr <> bStr then
        DoChangeEntryText(ItemIndex, bStr);
    end;
end;

procedure TLegendDialog.DoChangeEntryText(Index: Integer; const NewText: string);
var
  Entry: TLegendEntry;
begin
  Entry := FLegend.Items[Index];
  // change entry text
  Entry.FText := NewText;
  Entry.FNameFromLayer := False;
  // no text -> option for use layer name again
  if NewText = '' then
    if MessageDialog(MlgStringList[LegDlgSection, 33],
      mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      Entry.FNameFromLayer := True;
  // if Sorted then Sort again for consistency
  if FCurSorting then
    FLegend.Sorted := True;
  // select right entry
  LegendListBox.ItemIndex := FLegend.IndexOf(Entry);
  // show changes
  Invalidate;
//++ Glukhov Bug#384 Build#157 26.04.01
  LegendListBox.Refresh;
//-- Glukhov Bug#384 Build#157 26.04.01
end;

procedure TLegendDialog.LegendListboxColumns2Paint(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
  Entry: TLegendEntry;
  LayerProp: TLayerProperties;
  i: Integer;
begin
  if (FLegend.Count > Index) and (Index > -1) then
  begin
    Entry := FLegend.Items[Index];
    if Assigned(Entry) then
      with Entry do
        for i := 0 to FLayerList.Count - 1 do
        begin
          LayerProp := TLayerProperties(FLayerList.Items[i]);
          // find layer with right number
          if Assigned(LayerProp) then
            if LayerProp.Number = FLayerNumber then
              DrawColumnText(Rect, State, LayerProp.Name)
        end;
  end;
end;

procedure TLegendDialog.ChooseSymbol1Click(Sender: TObject);
var
  SelectedSymbol: psGroup;
begin
  with LegendListBox do
    if ItemIndex <> -1 then
    begin
      SelectedSymbol := GetSymbol(Self, FProject);
      if SelectedSymbol <> nil then
      begin
        with FLegend.Items[ItemIndex] do
        begin
          FSymName := SelectedSymbol^.GetName;
          FLibName := SelectedSymbol^.GetLibName;
        end;
        Invalidate;
      end;
    end;
end;

procedure TLegendDialog.TitleStyleBtnClick(Sender: TObject);
{$IFNDEF AXDLL} // <----------------- AXDLL
var
  TextStyleDlg: TFontStyleDialog;
{$ENDIF}
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  TextStyleDlg := TFontStyleDialog.Create(self);
  try
    with TextStyleDlg do
    begin
      TextStyleDlg.SetNewCaption(18);
      // disable height elements
      AllowHeightOptions := False;
      Fonts := FProject^.PInfo^.Fonts;
      TextStyle.Assign(FLegend.TitleStyle);
      if ShowModal = mrOk then
      begin
        FLegend.TitleStyle.Assign(TextStyle);
      end;
    end;
  finally
    TextStyleDlg.Free;
  end;
{$ENDIF}
end;

procedure TLegendDialog.EntryStyleBtnClick(Sender: TObject);
{$IFNDEF AXDLL} // <----------------- AXDLL
var
  TextStyleDlg: TFontStyleDialog;
{$ENDIF}
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  TextStyleDlg := TFontStyleDialog.Create(self);
  try
    with TextStyleDlg do
    begin
      TextStyleDlg.SetNewCaption(18);
      // disable height elements
      AllowHeightOptions := False;
      Fonts := FProject^.PInfo^.Fonts;
      TextStyle.Assign(FLegend.EntryStyle);
      if ShowModal = mrOk then
      begin
        FLegend.EntryStyle.Assign(TextStyle);
      end;
    end;
  finally
    TextStyleDlg.Free;
  end;
{$ENDIF}  
end;

procedure TLegendDialog.SearchEditChange(Sender: TObject);
var
  MaxEqualLen: Integer;
  MaxEqualIndex: Integer;
  MaxEqualAllLen: Integer;
  EqualLen: Integer;
  Cnt: Integer;
  Entry: TLegendEntry;
begin
  MaxEqualLen := 0;
  MaxEqualAllLen := 0;
  MaxEqualIndex := -1;
  for Cnt := 0 to FLegend.Count - 1 do
  begin
    Entry := FLegend[Cnt];
    EqualLen := CalculateEqualLen(SearchEdit.Text, Entry.FText);
    if (EqualLen > MaxEqualLen) or ((EqualLen = MaxEqualLen)
      and (MaxEqualAllLen > ByteToCharIndex(Entry.FText, Length(Entry.FText)))) then
    begin
      MaxEqualLen := EqualLen;
      MaxEqualAllLen := ByteToCharIndex(Entry.FText, Length(Entry.FText));
      MaxEqualIndex := Cnt;
    end;
  end;
  if MaxEqualIndex >= 0 then
    LegendListbox.ItemIndex := MaxEqualIndex;
  SearchEdit.Text := Copy(SearchEdit.Text, 1, CharToByteIndex(SearchEdit.Text, MaxEqualLen));
  SearchEdit.SelStart := Length(SearchEdit.Text);
  SearchBtn.Enabled := Length(SearchEdit.Text) > 0;
end;

procedure TLegendDialog.SearchBtnClick(Sender: TObject);
var
  Cnt: Integer;
  Entry: TLegendEntry;
begin
  if FLegend = nil then
    Exit;
  Cnt := (LegendListbox.ItemIndex + 1) mod FLegend.Count;
  while Cnt <> LegendListbox.ItemIndex do
  begin
    Entry := FLegend[Cnt];
    if CalculateEqualLen(SearchEdit.Text, Entry.FText) = Length(SearchEdit.Text) then
    begin
      LegendListbox.ItemIndex := Cnt;
      Break;
    end;
    Cnt := (Cnt + 1) mod FLegend.Count;
  end;
end;

procedure TLegendDialog.AlphabeticBtnClick(Sender: TObject);
var
  Entry: TLegendEntry;
begin
  if FLegend = nil then
    Exit;
  Entry := nil;
  with LegendListBox do
  begin
    if ItemIndex <> -1 then
      Entry := FLegend.Items[ItemIndex];
    FCurSorting := AlphabeticBtn.Down;
    FLegend.Sorted := FCurSorting;
    if Entry <> nil then
      ItemIndex := FLegend.IndexOf(Entry);
    LegendListBox.Invalidate;
  end;
end;

procedure TLegendDialog.HierarchicBtnClick(Sender: TObject);
begin
  if FLegend = nil then
    Exit;
  if HierarchicBtn.Down then
  begin
    FCurSorting := False;
    FLegend.Sorted := False;
    EstablishHierarchicOrder;
    LegendListBox.Invalidate;
  end;
end;

procedure TLegendDialog.EstablishHierarchicOrder;
var
  i, j: Integer;
  Entry1,
    Entry2: TLegendEntry;
  Layer: TLayerProperties;
  Pos: Integer;
begin
  Pos := 0;
  for i := 0 to FLayerList.Count - 1 do
  begin
    Layer := FLayerList[i];
    Entry2 := nil;
    for j := Pos to FLegend.Count - 1 do
    begin
      Entry1 := FLegend[j];
      if Entry1.FLayerNumber = Layer.Number then
      begin
        Entry2 := Entry1;
        break;
      end;
    end;
    // "quasi" insertion sort
    if Entry2 <> nil then
    begin
      // swap entries (Entry2 is thr right for FLegend[Pos])
      Entry1 := FLegend[Pos];
      FLegend[Pos] := Entry2;
      FLegend[j] := Entry1;
      // search next pos
      Inc(Pos);
    end;
    if Pos >= FLegend.Count then
      break; // finished...
  end;
end;

procedure TLegendDialog.HierarchicBtnMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if FLegend = nil then
    Exit;
  if HierarchicBtn.Down then
  begin
    FCurSorting := False;
    FLegend.Sorted := False;
    EstablishHierarchicOrder;
    LegendListBox.Invalidate;
  end;
end;

procedure TLegendDialog.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  if ((ModalResult = mrOK) or (ModalResult = mrRetry)) and (FLegendList <> nil) then
  begin
    // execute changes to project
    // free former legends
    Project^.Legends.Free;
    // setup new legend (and modifications)
    Project^.Legends := FLegendList;
    Project^.SetModified;
    FLegendList := nil;
  end;
  CanClose := True;
end;

procedure TLegendDialog.InplaceEditorEdited(Sender: TObject;
  Index: Integer; const Text: string);
begin
  DoChangeEntryText(Index, Text);
end;

procedure TLegendDialog.InplaceEditorEditing(Sender: TObject;
  Index: Integer; var AllowEdit: Boolean; var Text: string);
begin
  InplaceEditor.EditLeft := LegendListbox.Columns[1].Bounds.Left;
  InplaceEditor.EditRight := LegendListbox.Columns[1].Bounds.Right;
  AllowEdit := TRUE;
  Text := TLegendEntry(FLegend.Items[LegendListbox.ItemIndex]).FText;
end;

procedure TLegendDialog.FormShow(Sender: TObject);
begin
  InplaceEditor.Rehook;
end;

procedure TLegendDialog.SetNewCaption(ANewCaption: Integer);
begin
  Caption := MlgSection1[ANewCaption];
end;

//++ Glukhov Bug#458 Build#166 15.11.01

procedure TLegendDialog.chkScBarClick(Sender: TObject);
begin
  FLegend.bScaleBar := chkScBar.Checked;
end;
//-- Glukhov Bug#458 Build#166 15.11.01

procedure TLegendDialog.chkTranspClick(Sender: TObject);
begin
  FLegend.bTransparent := chkTransp.Checked;
end;

end.

