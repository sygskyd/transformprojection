�
 TPOINTSCOORDDIALOG 0�'  TPF0TPointsCoordDialogPointsCoordDialogLeft TopBorderIcons BorderStylebsDialogCaption!1ClientHeight2ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrder	PositionpoScreenCenterOnCloseQueryFormCloseQuery	OnDestroyFormDestroyOnShowFormShow
DesignSize�2 PixelsPerInch`
TextHeight TBevelBevel1Left
TopWidthtHeightAnchorsakLeftakTopakRight ShapebsBottomLine  TPanelControlsPanelLeft Top Width�Height� AlignalTop
BevelOuterbvNoneTabOrder  TLabelLabel1Left� TopWidthLHeightAutoSizeCaption!7FocusControl	TypeComboVisible  TLabelLabel5Left
TopWidth	HeightCaption!8  TLabel	AltLabel1Left� TopWidth,HeightCaption	AltLabel1FocusControlAltEdit1Visible  TLabel	AltLabel2LeftTopWidth,HeightCaption	AltLabel2FocusControlAltEdit2Visible  TLabelLabel9LeftTopWidth,HeightCaption	AltLabel3Visible  TLabel	AltLabel3Left� TopWidthHeightVisible  TLabel
ALtlabel3aLeftTopWidth2HeightCaption
ALtlabel3a  TSpeedButton	DelAllBtnLeftQTopWidthHeightHint!84Flat	
Glyph.Data
�   �   BM�       v   (               �                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� ������������������������ ������  ������� ������ ������ � �����  ������ ������  ����� � ���� ����  ��� �� ������������ParentShowHintShowHint	OnClickDelAllBtnClick  TSpeedButton
FileButtonLeft6TopWidthHeightHint!83Flat	
Glyph.Data
�   �   BM�       v   (               �                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� ����������������     �� 33330��3333��33330��3333��     �������������   ��� ���� ����������������� ����������ParentShowHintShowHint	OnClickFileButtonClick  TSpeedButtonOrtoBtnLeft*Top� WidthHeightHint!87
AllowAllUp	
GroupIndex
Flat	
Glyph.Data
�   �   BM�       v   (               �                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 333333333333313333331333333133333333333330 333333  3333333333333333333333333333333333333 13  333 133333333ParentShowHintShowHint	VisibleOnClickOrtoBtnClick  TSpeedButtonCoordTypeBtnLeftlTopWidthHeightHint!90Flat	
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� UUUUUUUUUUU���UUUUT��UUUUU�ww_�UUTD��DUUUWu�Uw_UUDNtNDEUUuWUU�TDN��DwUW�W�Uw�T��wN�wUWUWw�Uw_N�NwNN�EU�w�UU�D�wD�N�Www��UDD����DEwww��UDN�w~�DEWwwwUD��Dw��Eu�u_w�u^D�ww�N�W��www��TG~��wDUWWwwww�UUw�D~�EUUwwUuwuUU^D��GUUUWu��wUUUUT��UUUUUWwwUUU	NumGlyphsParentShowHintShowHint	OnClickCoordTypeBtnClick  	TComboBox	TypeComboLeft@TopiWidthIHeightStylecsOwnerDrawFixed
ItemHeightTabOrderVisibleOnChangeTypeComboChangeOnClickTypeComboClick  TPanelPanel1LeftPTop,WidthHeight5
BevelOuter	bvLoweredTabOrder	 TLabelLabel2LeftTop!WidthHeightCaption!11FocusControl	ModeCombo  TLabelLabel3LeftTop7Width?HeightAutoSizeCaptionLabel3FocusControlEdit1  TLabelLabel4LeftTopNWidth=HeightAutoSizeCaptionLabel4FocusControlEdit2  TLabelLabel8LeftTopfWidth HeightCaptionLabel8FocusControlEdit3  TRadioGroupRadioGroup1LeftTopWidth� Height(Caption!88Columns	ItemIndex Items.Strings90-90 TabOrderVisibleOnClickRadioGroup1Click  	TCheckBox	CheckBox1LeftTopWidthcHeightCaption!10TabOrder OnClickCheckBox1Click  	TComboBox
PointComboLefthTopWidthBHeightStylecsOwnerDrawFixedEnabled
ItemHeightParentShowHintShowHint	TabOrderOnChangePointComboChangeOnClickPointComboClick  	TComboBox	ModeComboLeftATopWidthjHeightStylecsOwnerDrawFixed
ItemHeightTabOrderOnChangeModeComboChangeOnClickModeComboClickItems.StringsLength and AngleIncrement X and Increment Y   TButtonApplyBtnLeftpTopcWidth:HeightCaption!17TabOrderOnClickApplyBtnClick  TEditEdit1LeftATop4WidthjHeightTabOrderOnClick
Edit1ClickOnExitEditExit  TEditEdit2LeftATopKWidthjHeightParentShowHintShowHint	TabOrderTextEdit2OnClick
Edit2ClickOnExit	Edit2Exit  TEditEdit3Left4TopcWidth:HeightTabOrderTextEdit3OnClick
Edit3Click   	TCheckBoxSpecificCheckBoxLeft� TopWidth� HeightCaption!9TabOrderVisibleOnClickSpecificCheckBoxClick  TButtonInsBtnLeft
Top� WidthKHeightCaption!14TabOrderTabStopOnClickInsBtnClick  TButtonAddBtnLeftdTop� WidthKHeightCaption!15TabOrderTabStopOnClickAddBtnClick  TButtonDelBtnLeft� Top� WidthKHeightCaption!16TabOrderTabStopOnClickDelBtnClick  THeaderControlHeaderControl1Left
TopWidthtHeightAlignalNoneEnabledSections	AlignmenttaCenter
ImageIndex�Text!25Width2 	AlignmenttaCenter
ImageIndex�Text!20Width2 	AlignmenttaCenter
ImageIndex�Text!21Width2    TPanelPanel2Left
Top(WidthtHeighta
BevelOuterbvNoneTabOrder TLabelLabel10Left8Top,WidthHeightCaption!86Visible  TStringGridPointsStringGridLeft Top WidthtHeightaAlignalClientColCountDefaultColWidthPDefaultRowHeightRowCount	FixedRows Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OptionsgoFixedVertLinegoFixedHorzLine
goVertLine
goHorzLinegoDrawFocusSelected	goEditing 
ParentFont	PopupMenu	PopupMenu
ScrollBars
ssVerticalTabOrder OnClickPointsStringGridClickOnColumnMovedPointsStringGridColumnMoved
OnDrawCellPointsStringGridDrawCellOnExitPointsStringGridExitOnGetEditTextPointsStringGridGetEditText	OnMouseUpPointsStringGridMouseUp
OnRowMovedPointsStringGridRowMovedOnSelectCellPointsStringGridSelectCellOnTopLeftChangedPointsStringGridTopLeftChanged
RowHeights    TEditAltEdit1Left
TopmWidthHeightTabOrderText0.00Visible  TEditAltEdit2LeftToplWidthHeightTabOrderText0.00Visible  	TComboBox	ComboBox1LeftTophWidth!HeightStylecsOwnerDrawFixed
ItemHeightTabOrderVisible  TEditAltEdit3Left3ToppWidthHeightTabOrderText0.00Visible  TEdit
StringEditLeft
TopVWidthHeightCtl3D	Font.CharsetRUSSIAN_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Serif
Font.Style ParentCtl3D
ParentFontTabOrder VisibleOnExitStringEditExit  TEdit
GradusEditLeftTop<WidthHeightHint!92Ctl3D		MaxLengthParentCtl3DParentShowHintShowHint	TabOrderVisibleOnExitGradusEditExit	OnKeyDownGradusEditKeyDown  TEdit
SecondEditLeftHTop<WidthHeightHint!94Ctl3D	ParentCtl3DParentShowHintShowHint	TabOrderVisibleOnExitSecondEditExit	OnKeyDownSecondEditKeyDown  TEdit
MinuteEditLeft&Top<Width!HeightHint!93Ctl3D	ParentCtl3DParentShowHintShowHint	TabOrderVisibleOnExitMinuteEditExit	OnKeyDownMinuteEditKeyDown   TButton	CancelBtnLeft2TopWidthKHeightAnchorsakTopakRight Caption!4ModalResultTabOrderTabStop  TButtonOKBtnLeft� TopWidthKHeightAnchorsakTopakRight Caption!3Default	ModalResultTabOrderTabStopOnClick
OKBtnClick  TButton	SwitchBtnLeftTopWidthKHeightCaption!6TabOrderTabStopOnClickSwitchBtnClick  TPanelControlsPanel1Left Top� Width�Height]AlignalTop
BevelOuterbvNoneTabOrder 	TGroupBox	GroupBox1Left
TopWidth_HeightQTabOrder TabStop	 TLabelLabel6Left
TopWidthCHeight	AlignmenttaCenterAutoSizeCaption!43FocusControlEdit4  TLabelLabel7Left
Top2WidthCHeight	AlignmenttaCenterAutoSizeCaption!44FocusControlEdit5  TEditEdit4LeftSTopWidth� HeightTabOrder OnClick
Edit4ClickOnExit	EditExit1OnMouseDownEdit4MouseDown  TEditEdit5LeftSTop/Width� HeightTabOrderOnClick
Edit5ClickOnExit	EditExit1OnMouseDownEdit5MouseDown   TEditGradusEdit1LeftTopFWidthUHeight	MaxLengthTabOrderVisibleOnExitGradusEdit1Exit	OnKeyDownGradusEdit1KeyDown  TEditMinuteEdit1Left� TopFWidthAHeightTabOrderVisibleOnExitMinuteEdit1Exit	OnKeyDownMinuteEdit1KeyDown  TEditSecondEdit1Left\TopFWidthAHeightTabOrderVisibleOnExitSecondEdit1Exit	OnKeyDownSecondEdit1KeyDown   TButton	CoordBtn2LeftbTopWidthKHeightHint!90Caption!95ParentShowHintShowHint	TabOrderTabStopVisibleOnClickCoordBtn2Click  TStringGrid
IgnoreGridLeft� Top Width Height 	FixedCols 	FixedRows TabOrderVisible  TStringGridReplaceGridLeft� Top Width Height TabOrderVisible  TMlgSection
MlgSectionSectionPointsCoordDialogLeft� Top8  TFloatValidatorAngleValidatorEditEdit2CommasMinValue       ��	MaxLengthMaxValue       �@Left� TopH  TFloatValidatorFloatValidator1EditAltEdit1OnErrorFloatValidator1ErrorCommas	MaxLength
MaxValue    �'k�@LeftZTopF  TFloatValidatorFloatValidator2EditAltEdit2OnErrorFloatValidator1ErrorCommas	MaxLength
MaxValue    �'k�@LeftTopH  TFloatValidatorAngleValidator2OnErrorFloatValidator1ErrorCommasMinValue       ��	MaxLengthMaxValue       �@Left<TopF  TFloatValidatorAngleValidator3OnErrorFloatValidator1ErrorCommasMinValue       ��	MaxLengthMaxValue       �@Left� TopD  TFloatValidatorStringEditValid
LeaveBlank	FormatWhenExitCommasMinValue   ���� @	MaxLengthMaxValue   ���� @Left� TopH  
TPopupMenu	PopupMenuLeftRTop` 	TMenuItemPMSaveCaption!97OnClickPMSaveClick   TSaveDialog
SaveDialog
DefaultExt*.txtFilter*.txt|*.txtOptionsofOverwritePromptofHideReadOnlyofEnableSizing LeftPTop�    