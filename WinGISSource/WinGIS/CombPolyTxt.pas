unit CombPolyTxt;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, MultiLng, OleCtrls, AXImpExpDbcXControl_TLB, AxGisCmb_TLB;

type
  TCombPolyTxtForm = class(TForm)
    MlgSection: TMlgSection;
    AXImpExpDbc: TAXImpExpDbc;
    AxGisCombine: TAxGisCombine;
    GroupBox1: TGroupBox;
    ListBoxPoly: TListBox;
    GroupBox2: TGroupBox;
    ListBoxText: TListBox;
    GroupBox3: TGroupBox;
    ButtonCancel: TButton;
    ButtonOk: TButton;
    ButtonDB: TButton;
    GroupBox4: TGroupBox;
    CheckBoxErrorLayers: TCheckBox;
    CheckBoxLinkTexts: TCheckBox;
    CheckBoxRemoveCombined: TCheckBox;
    EditDBField: TEdit;
    LabelDBField: TLabel;
    procedure ButtonDBClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure AxGisCombineDisplayPercent(Sender: TObject; aValue: Integer);
    procedure AxGisCombineDisplayPass(Sender: TObject;
      const aPass: WideString);
    procedure AXImpExpDbcRequestExpProjectName(Sender: TObject;
      var ProjectName: WideString);
    procedure AXImpExpDbcRequestImpProjectName(Sender: TObject;
      var ProjectName: WideString);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CombPolyTxtForm: TCombPolyTxtForm;

implementation

uses UserIntf, am_main;

{$R *.DFM}

procedure TCombPolyTxtForm.FormShow(Sender: TObject);
begin
   AXImpExpDbc.Visible:=False;
   AXGisCombine.Visible:=False;
   AXImpExpDbc.ShowMode:=2;   // Shw-Targer
end;

procedure TCombPolyTxtForm.ButtonDBClick(Sender: TObject);
begin
   AXImpExpDbc.SetupByModalDialog;
   CheckBoxRemoveCombined.Enabled:=(AXImpExpDbc.ImpDbMode <> 0);
   if not CheckBoxRemoveCombined.Enabled then begin
      CheckBoxRemoveCombined.Checked:=False;
   end;
   EditDBField.Enabled:=(AXImpExpDbc.ImpDbMode <> 0);
end;

procedure TCombPolyTxtForm.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
     CanClose:=True;
     if ModalResult = mrOK then begin
        if ListBoxPoly.SelCount = 0 then begin
           CanClose:=False;
           MessageBox(0,PChar(MlgSection[6]),PChar(MlgSection[8]),MB_OK or MB_ICONERROR or MB_APPLMODAL);
        end
        else if ListBoxText.SelCount = 0 then begin
           CanClose:=False;
           MessageBox(0,PChar(MlgSection[7]),PChar(MlgSection[8]),MB_OK or MB_ICONERROR or MB_APPLMODAL);
        end;
     end;
end;

procedure TCombPolyTxtForm.AxGisCombineDisplayPercent(Sender: TObject;
  aValue: Integer);
begin
     StatusBar.Progress:=aValue;
     Application.ProcessMessages;
end;

procedure TCombPolyTxtForm.AxGisCombineDisplayPass(Sender: TObject;
  const aPass: WideString);
begin
     StatusBar.ProgressText:=aPass;
end;

procedure TCombPolyTxtForm.AXImpExpDbcRequestExpProjectName(
  Sender: TObject; var ProjectName: WideString);
var Projname:string;
begin
    {$IFNDEF WMLT}
    {$IFNDEF AXDLL} // <----------------- AXDLL
    // get current project name
    WinGISMainForm.CheckTDocumentIsCorrect;
    Projname:=WinGISMainForm.GetCurrentTDocumentName;
    ProjectName:=Projname;
    {$ENDIF} // <----------------- AXDLL
    {$ENDIF}
end;

procedure TCombPolyTxtForm.AXImpExpDbcRequestImpProjectName(
  Sender: TObject; var ProjectName: WideString);
var Projname:string;
begin
    {$IFNDEF WMLT}
    {$IFNDEF AXDLL} // <----------------- AXDLL
    // get current project name
    WinGISMainForm.CheckTDocumentIsCorrect;
    Projname:=WinGISMainForm.GetCurrentTDocumentName;
    ProjectName:=Projname;
    {$ENDIF} // <----------------- AXDLL
    {$ENDIF}
end;

end.
