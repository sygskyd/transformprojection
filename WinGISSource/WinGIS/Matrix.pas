{******************************************************************************}
{ Unit Matrix                                                                  }
{------------------------------------------------------------------------------}
{ Datenstuktur f�r Matrizenrechnung und einige Rechenoperationen f�r Matrizen. }
{  - Erzeugen der Einheitsmatrix                                               }
{  - Addition von zwei Matizen                                                 }
{  - Subtraktion von zwei Matrizen                                             }
{  - Multiplikation mit einer Zahl                                             }
{  - Multiplikation von zwei Matrizen                                          }
{  - Gau�-Algorithmu� zum L�sen von Gleichungssystemen                         }
{  - Inverse einer Matrix bilden (mit Gau�)                                    }
{------------------------------------------------------------------------------}
{ Autor: Martin Forst, Februar 1996                                            }
{------------------------------------------------------------------------------}
{ ToDo: Alle Fehlermeldungen ins String-File auslagern.                        }
{******************************************************************************}
Unit Matrix;

Interface

Uses Classes,SysUtils;

Type {*************************************************************************}
     { Exception EMatrix                                                       }
     {-------------------------------------------------------------------------}
     { Exception, die von den Matrizenfunktionen im Falle eines Fehlers        }
     { ausgel�st wird.                                                         }
     {*************************************************************************}
     EMatrix            = Class(Exception);

Type TMatrix            = Class(TPersistent)
      Private
       FColumns         : Integer;
       FRows            : Integer;
       FData            : Pointer;
       Procedure   ChangeSize(ARows,AColumns:Integer);
       Procedure   SetColumns(AColumns:Integer);
       Procedure   SetRows(ARows:Integer);
       Function    GetElement(ARow,AColumn:Integer):Double;
       Procedure   SetElement(ARow,AColumn:Integer;Const AElement:Double);
      Protected
       Procedure   AssignTo(Dest:TPersistent); override;
      Public
       Constructor Create(ARows,AColumns:Integer);
       Constructor Identity(ASize:Integer);
       Destructor  Destroy; override;
       Procedure   AddMatrix(AMatrix:TMatrix);
       Function    AddMatrixToNew(AMatrix:TMatrix):TMatrix;
       Property    Columns:Integer read FColumns write SetColumns;
       Property    Elements[ARow,AColumn:Integer]:Double read GetElement write SetElement; default;
       Procedure   ExchangeRows(Row1,Row2:Integer);
       Procedure   Invers;
       Function    InversToNew:TMatrix;
       Function    Maximum:Double;
       Function    Minimum:Double;
       Procedure   MultByValue(Const AValue:Double);
       Function    MultByValueToNew(Const AValue:Double):TMatrix;
       Procedure   MultByMatrix(AMatrix:TMatrix);
       Function    MultByMatrixToNew(AMatrix:TMatrix):TMatrix;
       Property    Rows:Integer read FRows write SetRows;
       Function    SolveEquations(AResults:TMatrix;var XSpez,XHom:TMatrix;var DET:Double):Boolean;
       Procedure   SubMatrix(AMatrix:TMatrix);
       Function    SubMatrixToNew(AMatrix:TMatrix):TMatrix; 
       Procedure   Transpose;
       Function    TransposeToNew:TMatrix;
       {$IFDEF DEBUG}
       Procedure   Print;
       {$ENDIF}
     end;

Implementation

Const MaxShortInt  = 32756;

Type PMatrixData   = ^TMatrixData;
     TMatrixData   = Array[0..MaxShortInt Div 4-1] of Double;

     PIntArray     = ^TIntArray;
     TIntArray     = Array[0.. MaxShortInt div 2-1] of Integer;

{******************************************************************************}
{ Constructor TMatrix.Create                                                   }
{------------------------------------------------------------------------------}
{ Erzeugt eine neue Matrix mit den angegebenen Dimensionen. Die Eintr�ge in    }
{ den Matizenfeldern werden Null gesetzt.                                      }
{******************************************************************************}
Constructor TMatrix.Create
   (
   ARows           : Integer;
   AColumns        : Integer
   );
  begin
    inherited Create;
    ChangeSize(ARows,AColumns);
  end;

{******************************************************************************}
{ Constructor TMatrix.Identity                                                 }
{------------------------------------------------------------------------------}
{ Erzeugt eine Einheitsmatrix mit den angegebenen Dimensionen.                 }
{******************************************************************************}
Constructor TMatrix.Identity
   (
   ASize           : Integer
   );
  var Cnt          : Integer;
  begin
    Create(ASize,ASize);
    for Cnt:=0 to ASize-1 do PMatrixData(FData)^[Cnt*FColumns+Cnt]:=1;
  end;

{******************************************************************************}
{ Destructor TMatrix.Destroy                                                   }
{------------------------------------------------------------------------------}
{ Gibt den von der Matrix belegten Speicher frei.                              }
{******************************************************************************}
Destructor TMatrix.Destroy;
  begin
    FreeMem(FData,FRows*FColumns*SizeOf(Double));
    inherited Destroy;
  end;

{******************************************************************************}
{ Procedure TMatrix.SetColumns                                                 }
{------------------------------------------------------------------------------}
{ Setzt die Spaltenanzahl der Matrix auf einen neuen Wert.                     }
{******************************************************************************}
Procedure TMatrix.SetColumns
   (
   AColumns        : Integer
   );
  begin
    ChangeSize(FRows,AColumns);
  end;

{******************************************************************************}
{ Procedure TMatrix.SetRows                                                    }
{------------------------------------------------------------------------------}
{ Setzt die Zeilenanzahl der Matrix auf einen neuen Wert.                      }
{******************************************************************************}
Procedure TMatrix.SetRows
   (
   ARows           : Integer
   );
  begin
    ChangeSize(ARows,FColumns);
  end;

{******************************************************************************}
{ Procedure TMatrix.ChangeSize                                                 }
{------------------------------------------------------------------------------}
{ �ndert die Gr��e der Matrix auf die angegebenen Werte. Enth�lt die Matrix    }
{ bereits Werte, so werden diese gel�scht.                                     }
{------------------------------------------------------------------------------}
{ Exceptions: EOutOfMemory, wenn nicht gen�gend Speicher zur Verf�gung steht.  }
{               In diesem Fall beh�lt die Matrix alle ihre alten Werte.        }
{******************************************************************************}
Procedure TMatrix.ChangeSize
   (
   ARows           : Integer;
   AColumns        : Integer
   );
  var HelpPtr      : PMatrixData;
  begin
    if (ARows<>FRows)
       or (AColumns<>FColumns) then begin
      { neuen Speicher reservieren                                             }
      HelpPtr:=AllocMem(ARows*AColumns*SizeOf(Double));
      { alten Speicher freigeben                                               }
      FreeMem(FData,FRows*FColumns*SizeOf(Double));
      { Objektfelder updaten                                                   }
      FData:=HelpPtr;
      FRows:=ARows;
      FColumns:=AColumns;
    end;
  end;

{******************************************************************************}
{ Function TMatrix.GetElement                                                  }
{------------------------------------------------------------------------------}
{ Liefert das Element der Matrix an der angegebenen Position.                  }
{------------------------------------------------------------------------------}
{ Exceptions: EMatrix, wenn die angegebene Zeile oder Spalte ung�ltig sind.    }
{******************************************************************************}
Function TMatrix.GetElement
   (
   ARow            : Integer;
   AColumn         : Integer
   )
   : Double;
  begin
    if (ARow>FRows)
        or (ARow<0) then Raise EMatrix.Create('TMatrix.GetElement: Rownumber out of range');
    if (AColumn>FColumns)
        or (AColumn<0) then Raise EMatrix.Create('TMatrix.GetElement: Columnnumber out of range');
    Result:=PMatrixData(FData)^[ARow*FColumns+AColumn];
  end;

{******************************************************************************}
{ Procedure TMatrix.GetElement                                                 }
{------------------------------------------------------------------------------}
{ Setzt das Element der Matrix an der angegebenen Position auf einen neuen     }
{ Wert.                                                                        }
{------------------------------------------------------------------------------}
{ Exceptions: EMatrix, wenn die angegebene Zeile oder Spalte ung�ltig sind.    }
{******************************************************************************}
Procedure TMatrix.SetElement
   (
   ARow            : Integer;
   AColumn         : Integer;
   Const AElement  : Double
   );
  begin
    if (ARow>FRows)
        or (ARow<0) then Raise EMatrix.Create('TMatrix.SetElement: Rownumber out of range');
    if (AColumn>FColumns)
        or (AColumn<0) then Raise EMatrix.Create('TMatrix.SetElement: Columnnumber out of range');
    PMatrixData(FData)^[ARow*FColumns+AColumn]:=AElement;
  end;

{******************************************************************************}
{ Function TMatrix.TransposeToNew                                              }
{------------------------------------------------------------------------------}
{ Erzeut die transponierte Matrix. Das Ergebnis wird in eine neuen Matrix      }
{ gespeichert und als Funktionswert zur�ckgegeben.                             }
{------------------------------------------------------------------------------}
{ Exceptions: EOutOfMemory, wenn nicht gen�gend Speicher zur Verf�gung steht.  }
{******************************************************************************}
Function TMatrix.TransposeToNew
   : TMatrix;
  var Row          : Integer;
      Column       : Integer;
  begin
    Result:=TMatrix.Create(FColumns,FRows);
    for Row:=0 to FRows-1 do
        for Column:=0 to FColumns-1 do begin
      PMatrixData(Result.FData)^[Column*FRows+Row]:=
         PMatrixData(FData)^[Row*FColumns+Column];
    end;
  end;

{******************************************************************************}
{ Procedure TMatrix.Transpose                                                  }
{------------------------------------------------------------------------------}
{ Transponiert die aktuelle Matrix.                                            }
{------------------------------------------------------------------------------}
{ Exceptions: EOutOfMemory, wenn nicht gen�gend Speicher zur Verf�gung steht.  }
{               In diesem Fall beh�lt die Matrix alle ihre alten Werte.        }
{******************************************************************************}
Procedure TMatrix.Transpose;
  var Transposed   : TMatrix;
  begin
    Transposed:=TransposeToNew;
    Assign(Transposed);
    Transposed.Free;
  end;

{******************************************************************************}
{ Procedure TMatrix.AssignTo                                                   }
{------------------------------------------------------------------------------}
{ Kopiert alle Werte der Matrix (Gr��e und Felder) nach Dest.                  }
{------------------------------------------------------------------------------}
{ Exceptions: EConvertError, wenn Dest nicht vom Typ TMatrix ist.              }
{               In diesem Fall beh�lt die Matrix Dest alle ihre alten Werte.   }
{             EOutOfMemory, wenn nicht gen�gend Speicher zur Verf�gung steht.  }
{               In diesem Fall beh�lt die Matrix Dest alle ihre alten Werte.   }
{******************************************************************************}
Procedure TMatrix.AssignTo
   (
   Dest            : TPersistent
   );
  begin
    if not (Dest is TMatrix) then Raise EConvertError.Create('Matrix');
    TMatrix(Dest).ChangeSize(FRows,FColumns);
    Move(FData^,TMatrix(Dest).FData^,FRows*FColumns*SizeOf(Double));
  end;

{******************************************************************************}
{ Procedure TMatrix.MultByValue                                                }
{------------------------------------------------------------------------------}
{ Multipliziert alle Matrixelemente mit einer Zahl.                            }
{******************************************************************************}
Procedure TMatrix.MultByValue
   (
   Const AValue    : Double
   );
  var Cnt          : Integer;
  begin
    for Cnt:=0 to Rows*Columns-1 do
      PMatrixData(FData)^[Cnt]:=PMatrixData(FData)^[Cnt]*AValue;
  end;

{******************************************************************************}
{ Function TMatrix.MultByValueToNew                                            }
{------------------------------------------------------------------------------}
{ Multipliziert alle Matrixelemente mit einer Zahl und liefert die Ergebnis-   }
{ matrix als Funktionsergebnis zur�ck.                                         }
{------------------------------------------------------------------------------}
{ Exceptions: EOutOfMemory, wenn nicht gen�gend Speicher zur Verf�gung steht.  }
{******************************************************************************}
Function TMatrix.MultByValueToNew
   (
   Const AValue    : Double
   )
   : TMatrix;
  begin
    Result:=TMatrix.Create(FRows,FColumns);
    Result.Assign(Self);
    Result.MultByValue(AValue);
  end;

{******************************************************************************}
{ Procedure TMatrix.MultByMatrix                                               }
{------------------------------------------------------------------------------}
{ Multipliziert die Matrix mit AMatrix. Das Ergebnis wird im Objekt abgelegt.  }
{------------------------------------------------------------------------------}
{ Exceptions: EMatrix, wenn die Matrixdimensionen nicht �bereinstimmen.        }
{               In diesem Fall beh�lt die Matrix alle ihre alten Werte.        }
{             EOutOfMemory, wenn nicht gen�gend Speicher zur Verf�gung steht.  }
{               In diesem Fall beh�lt die Matrix alle ihre alten Werte.        }
{******************************************************************************}
Procedure TMatrix.MultByMatrix
   (
   AMatrix         : TMatrix
   );
  var Multiplied   : TMatrix;
  begin
    Multiplied:=MultByMatrixToNew(AMatrix);
    Assign(Multiplied);
    Multiplied.Free;
  end;

{******************************************************************************}
{ Procedure TMatrix.MultByMatrix                                               }
{------------------------------------------------------------------------------}
{ Multipliziert die Matrix mit AMatrix. Das Ergebnis wird als Funktionswert    }
{ zur�ckgegeben.                                                               }
{------------------------------------------------------------------------------}
{ Exceptions: EMatrix, wenn die Matrixdimensionen nicht �bereinstimmen.        }
{             EOutOfMemory, wenn nicht gen�gend Speicher zur Verf�gung steht.  }
{******************************************************************************}
Function TMatrix.MultByMatrixToNew
   (
   AMatrix         : TMatrix
   )
   : TMatrix;
  var Row          : Integer;
      Column       : Integer;
      k            : Integer;
      Sum          : Double;
  begin
    if FColumns<>AMatrix.FRows then
       Raise EMatrix.Create('TMatrix.MultByMatrixToNew: Matrix dimensions do not match');
    Result:=TMatrix.Create(FRows,AMatrix.FColumns);
    for Row:=0 to FRows-1 do
        for Column:=0 to AMatrix.FColumns-1 do begin
      Sum:=0;
      for k:=0 to FColumns-1 do
        Sum:=Sum+Elements[Row,k]*AMatrix[k,Column];
      Result[Row,Column]:=Sum;
    end;
  end;

{******************************************************************************}
{ Procedure TMatrix.ExchangeRows                                               }
{------------------------------------------------------------------------------}
{ Vertauscht zwei Zeilen.                                                      }
{------------------------------------------------------------------------------}
{ Exceptions: EMatrix, wenn die Zeilennummern ung�ltig sind.                   }
{             EOutOfMemory, wenn nicht gen�gend Speicher zur Verf�gung steht.  }
{******************************************************************************}
Procedure TMatrix.ExchangeRows
   (
   Row1            : Integer;
   Row2            : Integer
   );
  var Save         : Pointer;
  begin
    if (Row1<0)
        or (Row1>=Rows)
        or (Row2<0)
        or (Row2>=Rows) then Raise EMatrix.Create('TMatrix.ExchangeRows: Linenumber(s) out of range');
    Save:=AllocMem(FColumns*SizeOf(Double));
    Move(PMatrixData(FData)^[Row1*Fcolumns],Save^,FColumns*SizeOf(Double));
    Move(PMatrixData(FData)^[Row2*Fcolumns],PMatrixData(FData)^[Row2*Fcolumns],FColumns*SizeOf(Double));
    Move(Save^,PMatrixData(FData)^[Row2*Fcolumns],FColumns*SizeOf(Double));
    FreeMem(Save,FColumns*SizeOf(Double));
  end;

{******************************************************************************}
{ Procedure TMatrix.Insvers                                                    }
{------------------------------------------------------------------------------}
{ Berechnet die Inverse der Matrix.                                            }
{------------------------------------------------------------------------------}
{ Exceptions: EMatrix, wenn die Matrix keine eindeutige Inverse besitzt.       }
{               In diesem Fall wird die Matrix nicht ver�ndert.                }
{             EOutOfMemory, wenn nicht gen�gend Speicher zur Verf�gung steht.  }
{******************************************************************************}
Procedure TMatrix.Invers;
  var AMatrix      : TMatrix;
  begin
    AMatrix:=InversToNew;
    Assign(AMatrix);
    AMatrix.Free;
  end;

{******************************************************************************}
{ Procedure TMatrix.Insvers                                                    }
{------------------------------------------------------------------------------}
{ Berechnet die Inverse der Matrix und liefert das Ergebnis als neue Matrix    }
{ �ber den Funktionswert zur�ck.                                               }
{------------------------------------------------------------------------------}
{ Exceptions: EMatrix, wenn die Matrix keine eindeutige Inverse besitzt.       }
{             EOutOfMemory, wenn nicht gen�gend Speicher zur Verf�gung steht.  }
{******************************************************************************}
Function TMatrix.InversToNew
   : TMatrix;
  var AMatrix      : TMatrix;
      XHom         : TMatrix;
      Det          : Double;
  begin
    XHom:=NIL;
    AMatrix:=TMatrix.Identity(Rows);
    try
      if not SolveEquations(AMatrix,Result,XHom,Det) then
          Raise EMatrix.Create('TMatrix.InversToNew: Invers matrix does not exist');
    finally
      XHom.Free;
      AMatrix.Free;
    end;
  end;

{******************************************************************************}
{ Procedure TMatrix.SubMatrix                                                  }
{------------------------------------------------------------------------------}
{ Subtrahiert die zwei Matrizen voneinander.                                   }
{------------------------------------------------------------------------------}
{ Exceptions: EMatrix, wenn die Matrixgr��en nicht �bereinstimmen.             }
{******************************************************************************}
Procedure TMatrix.SubMatrix
   (
   AMatrix         : TMatrix
   );
  var Cnt          : Integer;
  begin
    if (Rows<>AMatrix.Rows)
        or (Columns<>AMatrix.Columns) then
      Raise EMatrix.Create('TMatrix.SubMatrix: Matrix dimensions do not match');
    for Cnt:=0 to Rows*Columns-1 do
      PMatrixData(FData)^[Cnt]:=PMatrixData(FData)^[Cnt]-PMatrixData(AMatrix.FData)^[Cnt];
  end;

{******************************************************************************}
{ Procedure TMatrix.SubMatrixToNew                                             }
{------------------------------------------------------------------------------}
{ Subtrahiert die zwei Matrizen voneinander und liefert das Ergebnis als neue  }
{ Matrix.                                                                      }
{------------------------------------------------------------------------------}
{ Exceptions: EMatrix, wenn die Matrixgr��en nicht �bereinstimmen.             }
{******************************************************************************}
Function TMatrix.SubMatrixToNew
   (
   AMatrix         : TMatrix
   )
   : TMatrix;
  begin
    Result:=TMatrix.Create(Rows,Columns);
    Result.Assign(Self);
    try
      Result.SubMatrix(AMatrix);
    except
      Result.Free;
    end;
  end;

Procedure TMatrix.AddMatrix
   (
   AMatrix         : TMatrix
   );
  var Cnt          : Integer;      
  begin
    if (Rows<>AMatrix.Rows)
        or (Columns<>AMatrix.Columns) then
      Raise EMatrix.Create('TMatrix.AddMatrix: Matrix dimensions do not match');
    for Cnt:=0 to Rows*Columns-1 do 
      PMatrixData(FData)^[Cnt]:=PMatrixData(FData)^[Cnt]+PMatrixData(AMatrix.FData)^[Cnt];
  end;

Function TMatrix.AddMatrixToNew
   (
   AMatrix         : TMatrix
   )
   : TMatrix;
  begin
    Result:=TMatrix.Create(Rows,Columns);
    Result.Assign(Self);
    try
      Result.SubMatrix(AMatrix);
    except
      Result.Free;
    end;
  end;

Function TMatrix.Maximum
   : Double;
  var Cnt          : Integer;
  begin
    Result:=-1.7E308;	
    for Cnt:=0 to Rows*Columns-1 do
      if PMatrixData(FData)^[Cnt]>Result then Result:=PMatrixData(FData)^[Cnt];
  end;

Function TMatrix.Minimum
   : Double;
  var Cnt          : Integer;
  begin
    Result:=1.7E308;
    for Cnt:=0 to Rows*Columns-1 do
      if PMatrixData(FData)^[Cnt]<Result then Result:=PMatrixData(FData)^[Cnt];
  end;

Function TMatrix.SolveEquations
   (
   AResults        : TMatrix;
   var XSpez       : TMatrix;
   var XHom        : TMatrix;
   var Det         : Double
   )
   : Boolean;
  var hilf         : Double;
      genau        : Double;
      h1           : integer;
      A            : TMatrix;
      B            : TMatrix;
      FEHLST       : Integer;
      IMERKER      : Integer;
      IRG          : Integer;
      i            : Integer;
      j            : Integer;
      l            : Integer;
      IZLNR        : TMatrix;
      IPS          : PIntArray;
      IPZ          : PIntArray;
      IFrei        : PIntArray;
      IWIDZL       : PIntArray;
  procedure maximum;
    var max        : Integer;
        hilf       : Integer;
        j          : Integer;
    begin
      max:=i;
      for j:=i to Columns-1 do if abs(B[IPZ^[j],IPS^[i]])>abs(B[IPZ^[max],IPS^[i]]) then max:=j;
      if max<>i then begin
        hilf:=IPZ^[i];
        IPZ^[i]:=IPZ^[max];
        IPZ^[max]:=hilf;
        inc(FEHLST);
      end;
    end;
  begin
    Result:=TRUE;
    A:=TMatrix.Create(Rows,Columns+AResults.Columns);
    for i:=0 to Rows-1 do for j:=0 to Columns-1 do
      A[i,j]:=Elements[i,j];
    for i:=0 to Columns-1 do for j:=0 to AResults.Columns-1 do
      A[i,j+Columns]:=AResults[i,j];
    B:=TMatrix.Create(Rows,A.Columns);
    B.Assign(A);
    GetMem(IPZ,Columns*SizeOf(Integer));
    GetMem(IFrei,Columns*SizeOf(Integer));
    for i:=0 to Columns-1 do begin
      IPZ^[i]:=i;
      IFrei^[i]:=1;
    end;
    GetMem(IWIDZL,AResults.Columns*SizeOf(Integer));
    IZLNR:=TMatrix.Create(Rows,AResults.Columns);
    for i:=0 to AResults.Columns-1 do begin
     IWIDZL^[i]:=0;
     for j:=0 to Rows-1 do IZLNR[j,i]:=0;
    end;
    GetMem(IPS,A.Columns*SizeOf(Integer));
    for i:=0 to A.Columns-1 do IPS^[i]:=i;
    IRG:=-1;
    FEHLST:=0;
    IMERKER:=Columns-1;
    DET:=1.0;
    XSpez:=TMatrix.Create(Rows,AResults.Columns);
    XHom:=TMatrix.Create(Rows,Columns);
    genau:=A.Maximum*1E-10;
    i:=0;
    while i<=IMERKER do begin
      for j:=i to Columns-1 do begin
        hilf:=0;
        for L:=0 to i-1 do Hilf:=hilf+B[IPZ^[j],IPS^[L]]*B[IPZ^[L],IPS^[i]];
        B[IPZ^[j],IPS^[i]]:=A[IPZ^[j],IPS^[i]]+hilf;
      end;
      maximum;
      if abs(B[IPZ^[i],IPS^[i]])>=genau then begin
        DET:=DET*B[IPZ^[i],IPS^[i]];
        Inc(IRG);
        IFREI^[IPS^[i]]:=-1;
        for j:=i+1 to Columns-1 do B[IPZ^[j],IPS^[i]]:=B[IPZ^[j],IPS^[i]]/(-B[IPZ^[i],IPS^[i]]);
        for j:=i+1 to A.Columns-1 do begin
          hilf:=0;
          for L:=0 to i-1 do Hilf:=hilf+B[IPZ^[i],IPS^[L]]*B[IPZ^[L],IPS^[j]];
          B[IPZ^[i],IPS^[j]]:=A[IPZ^[i],IPS^[j]]+hilf;
        end;
      end
      else if i<IMERKER then begin
        DET:=0;
        h1:=IPS^[i];
        IPS^[i]:=IPS^[IMERKER];
        IPS^[IMERKER]:=h1;
        DEC(IMERKER);
        Dec(i);
      end
      else begin
        for i:=IRG+1 to Columns-1 do
          for j:=Columns to A.Columns-1 do begin
            hilf:=0;
            for L:=0 to IRG do Hilf:=hilf+B[IPZ^[i],IPS^[L]]*B[IPZ^[L],IPS^[j]];
            B[IPZ^[i],IPS^[j]]:=A[IPZ^[i],IPS^[j]]+hilf;
            if abs(B[IPZ^[i],IPS^[j]]) >= genau then begin
              inc(IWIDZL^[j-Columns]);
              IZLNR[IWIDZL^[j-Columns],J-Columns]:=IPZ^[i];
            end;
          end;
      end;
      Inc(i);
    end; { for i..IMERKER }  { ENDE GAUSS Zerlegung }
    if IRG=Columns-1 then begin            { R�ckw�rtseinsetzen ANFANG }
      for j:=0 to AResults.Columns-1 do
        for i:=Columns-1 downto 0 do begin
          hilf:=0;
          for L:=i+1 to Columns-1 do Hilf:=hilf+B[IPZ^[i],IPS^[L]]*XSPEZ[L,j];
          XSPEZ[i,j]:=(B[IPZ^[i],IPS^[j+Columns]]-hilf) / B[IPZ^[i],IPS^[i]];
        end;
    end
    else begin
      for i:=0 to Columns-1 do begin
        for j:=0 to AResults.Columns-1 do XSPEZ[i,j]:=1.7E308;
        for j:=0 to Columns-1 do XHOM[i,j]:=0;
      end;
      for j:=0 to AResults.Columns-1 do if IWIDZL^[j]=0 then begin
        for i:=Columns-1 downto IRG+1 do XSPEZ[IPS^[i],j]:=0;
        for i:=IRG downto 0 do begin
          hilf:=0;
          for L:=i+1 to IRG do Hilf:=hilf+B[IPZ^[i],IPS^[L]]*XSPEZ[IPS^[L],j];
          XSPEZ[IPS^[i],j]:=(B[IPZ^[i],IPS^[j+Columns]]-hilf) / B[IPZ^[i],IPS^[i]];
        end;
      end
      else Result:=FALSE; { Widerspr�che ausgeben? }
      for j:=IRG+1 to Columns-1 do begin
        XHOM[IPS^[j],j-IRG]:=1;
        for i:=IRG downto 0 do begin
          hilf:=0;
          for L:=IPS^[i]+1 to Columns-1 do Hilf:=hilf+B[IPZ^[i],IPS^[L]]*XHOM[IPS^[L],j-IRG];
          XHOM[IPS^[i],j-IRG]:=(-hilf) / B[IPZ^[i],IPS^[i]];
        end;
      end;
    end;                           { R�ckw�rtseinsetzen ENDE }
    if Fehlst mod 2<>0 then DET:=-DET;
    FreeMem(IPZ,Columns*SizeOf(Integer));
    FreeMem(IFrei,Columns*SizeOf(Integer));
    FreeMem(IWIDZL,AResults.Columns*SizeOf(Integer));
    FreeMem(IPS,A.Columns*SizeOf(Integer));
    A.Free;
    B.Free;
    IZLNR.Free;
  end;

{$IFDEF DEBUG}
Procedure TMatrix.Print;
  var Row,Col      : Integer;
  begin
    for Row:=0 to Rows-1 do begin
      for Col:=0 to Columns-1 do
        Write(Elements[Row,Col]:0:2);
      writeln;
    end;
    writeln;
  end;
{$ENDIF}

end.

