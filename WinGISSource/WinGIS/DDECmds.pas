Unit DDECmds;

Interface

Const maxCommandNameLength   = 5;      { maximal length of dde-command-string }
      maxDDECommands         = 1;      { number of defined dde-commands }

Type PDDECommand   = ^TDDECommand;
     TDDECommand   = Record
       { fields defined in the DDECommands-Table }
       Name        : Array[0..maxCommandNameLength] of Char;
       ClosedByEnd : Boolean;
       Command     : Word;
       { fields filled while unit-initilization }
       NameLength  : Byte;             { lenght of the name-string, used for
                                       + speed-up of the search-function } 
     end;

Function FindDDECommandByName(CmdName:PChar):PDDECommand;

Implementation

Uses Am_Def,SysUtils;

Const DDECommands  : Array[0..maxDDECommands-1] of TDDECommand = (
    (Name:'[SHW]'; ClosedByEnd:FALSE; Command:0)
    );

{*******************************************************************************
| Function FindDDECommandByName
|-------------------------------------------------------------------------------
| Searches the list of dde-commands for the specified command. FindDDDECommand
| performs a binary search, therefore the list must be sorted by name.
| The length of CmdName must be equal to or larger than maxCommandNameLength,
| otherwide StrLComp could result in an exception.
+******************************************************************************}
Function FindDDECommandByName
   (
   CmdName         : PChar
   )
   : PDDECommand;
  var L            : Integer;
      H            : Integer;
      I            : Integer;
      C            : Integer;
  begin
    Result:=NIL;
    L:=0;
    H:=maxDDECommands-1;
    while L<=H do begin
      I:=(L+H) Shr 1;
      C:=StrLComp(DDECommands[I].Name,CmdName,DDECommands[I].NameLength);
      if C<0 then L:=I+1
      else begin
        H:=I-1;
        if C=0 then begin
          Result:=@DDECommands[I];
          L:=I;
        end;
      end;
    end;
  end;

Procedure Initialize;
  var Cnt          : Integer;
  begin
    { calculate NameLength for all dde-commands in the list }
    for Cnt:=0 to maxDDECommands-1 do
        DDECommands[Cnt].NameLength:=StrLen(DDECommands[Cnt].Name);
  end;

begin
  Initialize;
end.
 