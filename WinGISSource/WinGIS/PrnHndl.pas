Unit PrnHndl;

Interface
{$IFNDEF AXDLL} // <----------------- AXDLL
Uses Classes,Controls,GrTools,InpHndl,MenuHndl,PageSetup,PrintDlg,PrintOptions,
{++ Moskaliov Pictures Location Control and AXWinGIS Printing Methods BUILD#133 25.09.00}
     LicenseHndl,StyleDef
     {$IFNDEF WMLT}
     ,AM_DDE
     {$ENDIF}
     ,DDEDef,AM_Proj,
{-- Moskaliov Pictures Location Control and AXWinGIS Printing Methods BUILD#133 25.09.00}
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 25.10.00}
     AM_Font,
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 25.10.00}
{++ Moskaliov BUG#90 BUILD#150 05.01.01}
     PrnSelRange,
{-- Moskaliov BUG#90 BUILD#150 05.01.01}
     ProjHndl,PrintTemplates,Scroller,WPrinter,Windows,AM_Paint,Graphics;

Type TPrintHandler      = Class(TProjMenuHandler)
      Private
       ClippingRect     : TRect;
       FDialog          : TPrintingDialog;
       FPInfo           : TPaint;
       FPageSetup       : TPageSetup;
       PageRect         : TRect;
       ProjPrintRect    : TGrRect;
       FWidth           : Integer;
       FHeight          : Integer;
{++ Moskaliov AXWinGIS Printing Methods BUILD#127 05.09.00}
       FPrintFromDB     : Boolean;
       FMode            : Integer;
       FFileName        : String;
       FShowProgress    : Boolean;
       FPrintMonitoring : Boolean;
{-- Moskaliov AXWinGIS Printing Methods BUILD#127 05.09.00}
{++ Ivanoff Bug#443}
       iOldActualMenu: Integer;
{-- Ivanoff Bug#443}
       Procedure   DoPrint(PrintOptions: TPrintOptions);
       Function    DoPrintProject(Scroller: TGraphicSite): Boolean;
       Function    DoPrintLegend(Graphics: TLegendPlaceholder): Boolean;
       Procedure   DoPrintWMF(PrintOptions: TPrintOptions);
       Function    DoPrintWMFLegend(Graphics: TLegendPlaceholder): Boolean;
       Function    DoPrintWMFProject(Scroller: TGraphicSite): Boolean;
{++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
       Function    CreateInvisibleObjects(var FileName: String): Boolean;
       Function    DeleteInvisibleObjects(var FileName: String): Boolean;
       {-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
       Procedure   SendPrnMonDDECommand(Page: Integer; OfPages: Integer; ResultCode: Integer);
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
{++ Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
       Procedure   DrawCropMarks(Scroller: TGraphicSite; ARect: TRect);
       Procedure   DrawPageNum(Scroller: TGraphicSite; ARect: TRect; i,n: Integer);
{-- Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
      Public
       Constructor Create(AParent: TComponent); override; //++ Moskaliov AXWinGIS Printing Methods BUILD#127 05.09.00
       Destructor  Destroy; override;
       Function    OnMouseDown(Const X,Y: Double; Button: TMouseButton; Shift: TShiftState): Boolean; override;
       Function    OnMouseUp(Const X,Y: Double; Button: TMouseButton; Shift: TShiftState): Boolean; override;
       Procedure   OnReinvoke; override;
       Procedure   OnStart; override;
{++ Ivanoff Bug#443}
       Procedure OnActivate; override;
       Procedure OnEnd; override;
       Class Function HandlerType:Word; override;
{-- Ivanoff Bug#443}
{++ Moskaliov AXWinGIS Printing Methods BUILD#127 05.09.00}
       Function    PrintFromDB(Mode: Integer; FileName: String; Copies: Integer;
                         CollateCopies: Boolean; ShowProgress: Boolean;
                         PrintMonitoring: Boolean): Integer;
{-- Moskaliov AXWinGIS Printing Methods BUILD#127 05.09.00}
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 02.10.00}
       Function    FuncGetPaperFormat(APrinter: TWPrinter): Integer;
       Function    SetPrnParamFromDB(APrinter: TWPrinter; APaperFormatIndex: Integer;
                         AOrientation: Integer): Integer;
       Function    SetPgsParamFromDB(AModel: Integer; ARepaginate: Boolean;
                         HorizPgs: Integer; VertPgs: Integer; PageFrames: TGrRect;
                         InnerFrames: TGrRect; OverlapFrames: TGrRect;
                         NumbOrder: Boolean; NumbFrom: Integer): Integer;
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 02.10.00}
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 09.10.00}
       Function    SetMapParamFromDB(MapID: Integer; PrintingRegion: Integer;
                         ViewName: String; RegionPoint1: TGrPoint; RegionPoint2: TGrPoint;
                         bFitToPage: Boolean; PrintScale: Double; bKeepScale: Boolean;
                         dFrameWidth: Double; dFrameHeight: Double;
                         FrameCentering: Integer; FrameLeft: Double; FrameTop: Double): Integer;
       Function    SetLegParamFromDB(LegendID: Integer; FrameWidth: Double; FrameHeight: Double;
                         FrameCentering: Integer; FrameOridgin: Integer;
                         FrameLeft: Double; FrameTopOrBot: Double): Integer;
       Function    IsertLegendFromDB(LegendName: String; FrameWidth: Double; FrameHeight: Double;
                         FrameCentering: Integer; FrameOridgin: Integer;
                         FrameLeft: Double; FrameTopOrBot: Double): Integer;
       Function    GetPrnParamFromDB(var APrinterName: String; var APaperFormat: String; var AOrientation: Integer): Integer;
       Function    GetPgsParamFromDB(var AModel: Integer; var ARepaginate: Integer;
                         var HorizPgs: Integer; var VertPgs: Integer; var APageFrames: TGrRect;
                         var InnerFrames: TGrRect; var OverlapFrames: TGrRect;
                         var NumbOrder: Integer; var NumbFrom: Integer): Integer;
       Function    GetMapParamFromDB(MapID: Integer; var ARegion: Integer; var AViewName : String;
                         var ARegionBorders: TGrRect; var AFitToPage: Integer; var APrintScale: Double;
                         var AKeepScale: Integer; var AFrameWidth, AFrameHeight: Double;
                         var AFrameCentering: Integer; var AFrameLeft, AFrameTop: Double): Integer;
       Function    GetLegParamFromDB(LegendID: Integer; var ALegendName: String;
                         var AFrameWidth: Double; var AFrameHeight: Double;
                         var AFrameCentering: Integer; var AFrameOridgin: Integer;
                         var AFrameLeft: Double; var AFrameTopOrBot: Double): Integer;
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 09.10.00}
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 25.10.00}
       Function    SetPicParamFromDB(PictureID: Integer; PictureLocation: Integer;
                         FrameSizeInterpret: Integer; FrameWidth: Double; FrameHeight: Double;
                         FrameCentering: Integer; FrameOridgin: Integer;
                         FrameLeft: Double; FrameTopOrBot: Double): Integer;
       Function    IsertPictureFromDB(PictureFile: String; PictureLocation: Integer;
                         FrameSizeInterpret: Integer; FrameWidth: Double; FrameHeight: Double;
                         FrameCentering: Integer; FrameOridgin: Integer;
                         FrameLeft: Double; FrameTopOrBot: Double): Integer;
       Function    GetPicParamFromDB(PictureID: Integer; var APictureFile: String;
                         var APictureLocation: Integer; var AKeepAspect: Boolean;
                         var AFrameWidth: Double; var AFrameHeight: Double;
                         var AFrameCentering: Integer; var AFrameOridgin: Integer;
                         var AFrameLeft: Double; var AFrameTopOrBot: Double): Integer;
       Function    SetSigParamFromDB(SignatureID: Integer; Signature: String;
                         FontName: String; Italic: Boolean;
                         Bold: Boolean; Underlined: Boolean; Transparent: Boolean;
                         FontSize: Integer; FontColor: Integer; FontBackColor: Integer;
                         TextAlignment: Integer; OnPagesLocation: Boolean;
                         OnFirstPage: Boolean; OnInternalPages: Boolean; OnLastPage: Boolean;
                         FrameCentering: Integer; FrameOridgin: Integer;
                         FrameLeft: Double; FrameTopOrBot: Double): Integer;
       Function    InsertSignatureFromDB(Signature: String; FontName: String; Italic: Boolean;
                         Bold: Boolean; Underlined: Boolean; Transparent: Boolean;
                         FontSize: Integer; FontColor: Integer; FontBackColor: Integer;
                         TextAlignment: Integer; OnPagesLocation: Boolean;
                         OnFirstPage: Boolean; OnInternalPages: Boolean; OnLastPage: Boolean;
                         FrameCentering: Integer; FrameOridgin: Integer;
                         FrameLeft: Double; FrameTopOrBot: Double): Integer;
       Function    GetSigParamFromDB(SignatureID: Integer; var ASignature: String;
                         var AFontName: String; var AItalic: Boolean;
                         var ABold: Boolean; var AUnderlined: Boolean; var ATransparent: Boolean;
                         var AFontSize: Integer; var AFontColor: Integer; var AFontBackColor: Integer;
                         var ATextAlignment: Integer; var AOnPagesLocation: Boolean;
                         var AOnFirstPage: Boolean; var AOnInternalPages: Boolean; var AOnLastPage: Boolean;
                         var AFrameCentering: Integer; var AFrameOridgin: Integer;
                         var AFrameLeft: Double; var AFrameTopOrBot: Double): Integer;
       Function    StoreTemplateFromDB(FileName: String; Options: Integer): Integer;
       Function    LoadTemplateFromDB(FileName: String): Integer;
       Function    GetObjInformFromDB(var ATypesList: TStringList): Integer;
       Function    DeleteObjectFromDB(ObjectType: Integer; ObjectID: Integer): Integer;
       Function    MoveObjectFromDB(ObjectType: Integer; ObjectID: Integer; Options: Integer): Integer;
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 25.10.00}
     end;
{$ENDIF} // <----------------- AXDLL
Implementation

{$IFNDEF AXDLL} // <----------------- AXDLL
Uses AM_Def,Dialogs,ExtCanvas,Forms,Measures,MultiLng,NumTools,
     PrnStDlg,ProjStyle,SysUtils,UserIntf,Tooltips,Legend,PrnRep,OleCtnrs2, AM_ProjO
{++ Ivanoff BUG#91}
     , AM_Index, AM_Main
{-- Ivanoff BUG#91}
 ,AM_Layer    ;

const
vrFrame       = 500;
vrBorder      = False;

{===============================================================================
  TPrintHandler
+==============================================================================}

{++ Moskaliov AXWinGIS Printing Methods BUILD#127 05.09.00}
Constructor TPrintHandler.Create(AParent:TComponent);
begin
   inherited Create(AParent);
   FPrintFromDB:=FALSE;
   FMode:=1;
   FFileName:='';
   FShowProgress:=FALSE;
   FPrintMonitoring:=FALSE;
end;
{-- Moskaliov AXWinGIS Printing Methods BUILD#127 05.09.00}

//++ Glukhov PrinterProblemInfo Build#175 25.01.02
Procedure TPrintHandler.OnStart;
var Handler      : TSelectPrintRangeHandler;
TmpLayer:PLayer;
begin
{++ Ivanoff Bug#443}
  Inherited OnActivate;
{-- Ivanoff Bug#443}
  if WPrinters.Printers.Count<1 then begin
    MessageDialog(MlgStringList['PrnHndl',7],mtError,[mbOK],0);
    Exit;
  end;
  FDialog:=TPrintingDialog.Create(Parent);
  FDialog.Project:=Project;
  iOldActualMenu := Project.ActualMen;
  FDialog.RedrawSpecPrintFrame;
  case FDialog.ShowModal of
  mrOK:
    begin
      // Check the printer
      if (not Assigned(FDialog.PrintOptions.PageSetup.Printer)) then  begin
         MessageDialog(MlgStringList['PrnHndl',17],mtError,[mbOK],0)
      end
      else begin
        if FDialog.PrintOptions.PageSetup.PrintToWMF then begin
           PrintReportGetHeader;
           PrintReportGetPrinter(FDialog.PrintOptions);
           PrintReportShow;
           DoPrintWMF(FDialog.PrintOptions);
        end
        else begin
          if (not FDialog.PrintOptions.PageSetup.Printer.GotDocProperties) then begin
             MessageDialog(MlgStringList['PrnHndl',18],mtError,[mbOK],0);
          end
          else begin
             PrintReportGetHeader;
             PrintReportGetPrinter(FDialog.PrintOptions);
             PrintReportShow;
             DoPrint(FDialog.PrintOptions);
           end;
        end;
      end;
    end;
  mrYes:
    begin
      Handler:=TSelectPrintRangeHandler.Create(Self);
      Handler.OnMove:=Handler.DispFrameStatusText;
      Handler.OnResize:=Handler.DispFrameStatusText;
      Handler.OnRotate:=Handler.DispFrameStatusText;
      // calculate position and size
      Handler.PrintDialog:=FDialog;
      // set view-rotation to handler
      Handler.ViewRotation:=FDialog.Project^.PInfo^.ViewRotation;
      // create menu-handler to select part to print
      SubHandler:=Handler;
      CoordinateInput:=TRUE;
      Handler.DispFrameStatusText(Self);
      Visible:=TRUE;
      Status:=mhsNotFinished;
      with Handler do begin
           Project^.ZoomByRect(RotRect(XPosition,YPosition,Width,Height,0),vrFrame,vrBorder);
      end;
    end;
  end;   // End of CASE
     TmpLayer:=Project.Layers.NameToLayer(PrintFrameLayerName);
     if Tmplayer<>nil then begin;
        Screen.Cursor:=crHourGlass;
        ProcDeleteLayer(Project,TmpLayer);

        project.UpdatePaintOffset;
        Screen.Cursor:=crDefault;
     end;

end;
//-- Glukhov PrinterProblemInfo Build#175 25.01.02

{++ Ivanoff Bug#443}
Procedure TPrintHandler.OnActivate;
Begin
     Parent.Invalidate;
     Inherited OnActivate;
End;

Procedure TPrintHandler.OnEnd;
Begin
     Project.ActualMen := iOldActualMenu;
     Inherited OnEnd;
End;

Class Function TPrintHandler.HandlerType: Word;
Begin
     RESULT := mhtReturnToPushed;
End;
{-- Ivanoff Bug#443}

Function TPrintHandler.OnMouseDown(Const X: Double; Const Y: Double;
                                   Button: TMouseButton; Shift: TShiftState): Boolean;
begin
   if (ssDouble in Shift) and (Not (ssRight in Shift)) then begin
      OnReinvoke;
      Result:=TRUE;
   end
   else begin
      Result:=inherited OnMouseDown(X,Y,Button,Shift);
   end;
end;

Function TPrintHandler.OnMouseUp(Const X,Y: Double; Button: TMouseButton; Shift: TShiftState): Boolean;
var
APoint: TDPoint;
ASnapObject: PIndex;
begin
        Result:=inherited OnMouseUp(X,Y,Button,Shift);
        if FDialog.Project^.SnapState then begin
           APoint.Init(Round(TSelectPrintRangeHandler(SubHandler).XPosition),Round(TSelectPrintRangeHandler(SubHandler).YPosition));
           ASnapObject := NIL;
           if (FDialog.Project.SnapPoint(APoint, 0, ot_Any, APoint, ASnapObject)) and (ASnapObject <> NIL) then begin
              with TSelectPrintRangeHandler(SubHandler)do begin
                   XPosition := APoint.X;
                   YPosition := APoint.Y;
              end;
           end;
        end;
end;

Destructor TPrintHandler.Destroy;
begin
   FDialog.Free;
   inherited Destroy;
end;

Procedure TPrintHandler.DoPrintWMF(PrintOptions:TPrintOptions);
var WorldPrintRect : TGrRect;
    XPage          : Integer;
    YPage          : Integer;
    OverlapL       : Integer;     // left page-overlap in virtual units
    OverlapT       : Integer;     // top page-overlap in virtual units
    OverlapR       : Integer;     // right page-overlap in virtual units
    OverlapB       : Integer;     // bottom page-overlap in virtual units
    ProjOverlapL   : Double;      // left pageoverlap in project-units
    ProjOverlapT   : Double;      // top pageoverlap in project-units
    ProjOverlapR   : Double;      // right pageoverlap in project-units
    ProjOverlapB   : Double;      // bottom pageoverlap in project-units
    ProjHorizStep  : Double;
    ProjVertStep   : Double;
    Cnt            : Integer;
    ProgFile       : Array[0..255] of Char;
    VersionInfo    : PChar;
    FileOffset     : LongWord;
    StructSize     : LongInt;
    Info           : Pointer;
    InfoSize       : LongWord;
    Version        : String;
    MetaFile       : TMetafile;
    MetaFileCanvas : TMetafileCanvas;
    MFCreatedBy    : String;
    MFDescription  : String;
    Graphic        : TProjectPlaceholder;
    GraphicSite    : TGraphicSite;
//  Disabled       : Pointer;     // pointer to disabled windows-information
    Dialog         : TPrintStatusDialog;
    FileName       : String;
    MainName       : String;
    intFileNumber  : Integer;
    strFileNumber  : String;
    FileExtension  : String;
    Pages          : Integer;
    intPageNumber  : Integer;
    Search         : TSearchRec;
    DosError       : Integer;
{++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
    InvisObjExist  : Boolean;
    ObjFileName    : String;
{-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
    boolValue      : Boolean;
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}

begin
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
   FDialog.SaveWMFDialog.FileName:=FFileName;
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
    with PrintOptions.PageSetup do
       begin
          Pages:=VerticalPages*HorizontalPages;
          with FDialog.SaveWMFDialog do
             if (PrintLayoutModel = plmBook) and (Pages > 1) then
                Options:=Options - [ofOverwritePrompt]
             else
                Options:=Options + [ofOverwritePrompt];
       end;
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
    if FDialog.SaveWMFDialog.FileName = '' then
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
       if Win32Platform = VER_PLATFORM_WIN32_NT then
          FDialog.SaveWMFDialog.FilterIndex:=1    // *.WMF as default
       else
          FDialog.SaveWMFDialog.FilterIndex:=2    // *.EMF as default
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
    else
       if ExtractFileExt(FFileName) = '.wmf' then
          FDialog.SaveWMFDialog.FilterIndex:=1    // *.WMF will be created
       else
          FDialog.SaveWMFDialog.FilterIndex:=2;    // *.EMF will be created
   if FPrintFromDB and (FMode = 4) then
      boolValue:=TRUE
   else
      begin
         boolValue:=FDialog.SaveWMFDialog.Execute;
         if (not boolValue) and FPrintFromDB and FPrintMonitoring then
            // User pressed Cancel button on Save WMF Dialog
            SendPrnMonDDECommand(0,Pages,1);
      end;

// if FDialog.SaveWMFDialog.Execute then
   if boolValue then
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
   begin
{++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
      InvisObjExist:=CreateInvisibleObjects(ObjFileName);
{-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
      boolValue:= FPrintFromDB and (FMode = 4);
      if boolValue then
         boolValue:=FShowProgress
      else
         boolValue:=TRUE;
      if boolValue then
         begin
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
            Application.CreateForm(TPrintStatusDialog,Dialog);
//          Disabled:=DisableTaskWindows(0); //-- Moskaliov AXWinGIS Printing Methods
            Dialog.Show;
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
            {$IFDEF DEBUG}
            SetWindowPos(Dialog.Handle,HWND_TOPMOST,Screen.Width-Dialog.Width,Dialog.Top,Dialog.Width,Dialog.Height,SWP_SHOWWINDOW);
            {$ELSE}
            SetWindowPos(Dialog.Handle,HWND_TOPMOST,Dialog.Left,Dialog.Top,Dialog.Width,Dialog.Height,SWP_SHOWWINDOW);
            {$ENDIF}
            Dialog.Update;
         end;
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
      try
         // create Metafile object
         MetaFile:=TMetafile.Create;

         FileName:=FDialog.SaveWMFDialog.FileName;
         if ExtractFileExt(FileName) = '.wmf' then
            MetaFile.Enhanced:=FALSE
         else
            MetaFile.Enhanced:=TRUE;

{++ Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
       if boolValue then
          begin
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
               // setup status-dialog-labels
               Dialog.FileLabel.Caption:=ExtractFileName(StrPas(Project^.FName));
               Dialog.Label1.Caption:=MlgStringList['PrnHndl',8];
               if MetaFile.Enhanced then
                  if PrintOptions.PageSetup.PrintLayoutModel = plmBook then
                     Dialog.PrinterLabel.Caption:=MlgStringList['PrnHndl',12]
                  else   // PrintLayoutModel =  plmUniquePage or plmUniformLayout
                     Dialog.PrinterLabel.Caption:=MlgStringList['PrnHndl',10]
               else   // MetaFile.Enhanced = FALSE
                  if PrintOptions.PageSetup.PrintLayoutModel = plmBook then
                     Dialog.PrinterLabel.Caption:=MlgStringList['PrnHndl',11]
                  else   // PrintLayoutModel =  plmUniquePage or plmUniformLayout
                     Dialog.PrinterLabel.Caption:=MlgStringList['PrnHndl',9];
          end;   //++ Moskaliov AXWinGIS Printing Methods

         FileName:=FDialog.SaveWMFDialog.FileName;
         with PrintOptions.PageSetup do
            if (PrintLayoutModel = plmBook) and (Pages > 1) then
               begin
                  // Metafile size setting
                  MetaFile.MMWidth:=Round((RectWidth(PageExtents[0,0])+OverlapPageFrames.Right)*100);
                  MetaFile.MMHeight:=Round((RectHeight(PageExtents[0,0])+OverlapPageFrames.Bottom)*100);
                  // Metafile files name template creation
                  Cnt:=Length(FileName);
                  MainName:=Copy(FileName,1,Cnt-4);
                  FileExtension:=Copy(FileName,Cnt-3,4);

                  // common Metafile name creation
                  Str(StartPageNum+Pages-1,strFileNumber);
                  intFileNumber:=Length(strFileNumber);
                  strFileNumber:='';
                  for Cnt:=1 to intFileNumber do
                      strFileNumber:=strFileNumber+'0';
                  FileName:=MainName+strFileNumber+FileExtension;

                  // calculate page-overlap in virutal-units
                  OverlapL:=Round(OverlapPageFrames.Left*100);
                  OverlapT:=Round(OverlapPageFrames.Top*100);
                  OverlapR:=Round(OverlapPageFrames.Right*100);
                  OverlapB:=Round(OverlapPageFrames.Bottom*100);
                  XPage:=0;
                  YPage:=0;
               end
            else   // plmUniquePage or plmUniformLayout
               begin
                  // Metafile size setting
                  MetaFile.MMWidth:=Round(RectWidth(PageExtent)*100);
                  MetaFile.MMHeight:=Round(RectHeight(PageExtent)*100);
                  Pages:=1;
               end;

{++ Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
         if boolValue then
            begin
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
               Dialog.PortLabel.Caption:=ExtractFileName(FileName)+':';

               // update the page-number-display in the printing-status-dialog
               intPageNumber:=1;
               Dialog.PageLabel.Caption:=Format(MlgStringList['PrnHndl',6],[intPageNumber,Pages]);
               Dialog.Update;
            end;   //++ Moskaliov AXWinGIS Printing Methods

         // Clipping rectangle setting
         ClippingRect.Left:=0;
         ClippingRect.Right:=MetaFile.MMWidth;
         ClippingRect.Top:=0;
         ClippingRect.Bottom:=-MetaFile.MMHeight;

         with PrintOptions do
            begin
               // set current print page value
               Graphics.CurrPrintPage:=Graphics.PageSetup.StartPageNum;
               for Cnt:=0 to Graphics.ItemCount-1 do
                   if Graphics[Cnt].Initialised then
                      if Graphics[Cnt] is TLegendPlaceholder then
                         // set legends starting positions
                         begin
                            TLegendPlaceholder(Graphics[Cnt]).LegendPos.X:=
                               Round(TLegendPlaceholder(Graphics[Cnt]).BoundsRect.Left*100);
                            TLegendPlaceholder(Graphics[Cnt]).LegendPos.Y:=
                               -Round((RectHeight(PageSetup.PageExtent)-TLegendPlaceholder(Graphics[Cnt]).BoundsRect.Top)*100);
                         end
                      else
                         // detect the project placeholder
                         if Graphics[Cnt] is TProjectPlaceholder then
                               Graphic:=TProjectPlaceholder(Graphics[Cnt]);
            end;

         // Metafile resolution setting
         with PrintOptions.PageSetup.Printer.PixelsPerInch do
            if X > Y then
               MetaFile.Inch:=X
            else
               MetaFile.Inch:=Y;

         // WinGIS version getting
         Version:='';
         GetModuleFileName(HInstance,ProgFile,SizeOf(ProgFile));
         StructSize:=GetFileVersionInfoSize(ProgFile,FileOffset);
         if StructSize>0 then
            begin
               GetMem(VersionInfo,StructSize);
               if VersionInfo<>NIL then
                  if GetFileVersionInfo(ProgFile,FileOffset,StructSize,VersionInfo) then
                     if VerQueryValue(VersionInfo,'\StringFileInfo\080904E4\FileVersion',Info,InfoSize) then
                        SetString(Version,PChar(Info),Integer(InfoSize));
               FreeMem(VersionInfo,StructSize);
            end;

         // Metafile creation
         MFCreatedBy:=Format(MlgStringList['PrnHndl',13],[Version]);
         MFDescription:=Format(MlgStringList['PrnHndl',14],[ExtractFileName(StrPas(Project^.FName))]);
         for intPageNumber:=1 to Pages do
             begin
                MetaFileCanvas:=TMetafileCanvas.CreateWithComment(MetaFile,PrintOptions.PageSetup.Printer.Handle,MFCreatedBy,MFDescription);
                with MetaFileCanvas do
                   try
                      with PrintOptions do
                         begin
                            // for the first time
                            if intPageNumber = 1 then
                               begin
                                  // create PInfo for Metafile drawing and set options
                                  FPInfo.InitCopy(Project^.PInfo);
                                  FPInfo.Printing:=TRUE;   // TRUE value allows to interrupt the process
                                  FPInfo.WMPaint:=TRUE;
                                  FPInfo.AbortPrinting:=FALSE;
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
                                  if boolValue then
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
                                     Dialog.CancelPressed:=@FPInfo.AbortPrinting;
                                  // setup extended-canvas-object for Metafile drawing
                                  FPInfo.ExtCanvas.Cached:=FALSE;
                                  FPInfo.ExtCanvas.Handle:=MetaFileCanvas.Handle;
                                  FPInfo.ExtCanvas.MapMode:=mmHiMetric;
                                  FPInfo.Zoom:=FPInfo.ScaleToZoom(Graphic.PrintScale)*10;
                                  // setup draw-rotation
                                  FPInfo.SetViewRotation(-PrintRect.Rotation);
                                  WorldPrintRect:=ISOFrame(RotatedRect(PrintRect,-PrintRect.Rotation));
                                  FHeight:=Round(FPInfo.CalculateDispDouble(RectHeight(WorldPrintRect)));
                                  FWidth:=Round(FPInfo.CalculateDispDouble(RectWidth(WorldPrintRect)));
                                  // setup GraphicSite
                                  GraphicSite:=TGraphicSite.Create(NIL);
                                  GraphicSite.Scale:=100;
                                  GraphicSite.Canvas:=MetaFileCanvas;
                                  GraphicSite.XOffset:=0;
                                  GraphicSite.YOffset:=Round(RectHeight(PageSetup.PageExtent)*100);
                                  ProjPrintRect:=WorldPrintRect;
                                  // print page size setting
                                  PageRect.Top:=-Round((RectHeight(PageSetup.PageExtent)-Graphic.BoundsRect.Top)*100);
                                  PageRect.Bottom:=-Round(RectHeight(PageSetup.PageExtent)*100);
                                  if RectHeight(PageRect)>FHeight then
                                     PageRect.Bottom:=PageRect.Top-FHeight;
                                  ProjPrintRect.Bottom:=ProjPrintRect.Top-FPInfo.CalculateDrawDouble(RectHeight(PageRect));
                                  PageRect.Left:=Round(Graphic.BoundsRect.Left*100);
                                  PageRect.Right:=Round(RectWidth(PageSetup.PageExtent)*100);
                                  if RectWidth(PageRect)>FWidth then
                                     PageRect.Right:=PageRect.Left+FWidth;
                                  ProjPrintRect.Right:=ProjPrintRect.Left+FPInfo.CalculateDrawDouble(RectWidth(PageRect));
                               end
                            else
                               begin
                                  FPInfo.ExtCanvas.Handle:=MetaFileCanvas.Handle;
                                  GraphicSite.Canvas:=MetaFileCanvas;
                               end;

                            if Pages > 1 then   // at the same time PageSetup.PrintLayoutModel = plmBook
                               begin
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
                                  if boolValue then
                                     begin
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
                                        // update the page-number-display in the printing-status-dialog
                                        Dialog.PageLabel.Caption:=Format(MlgStringList['PrnHndl',6],[intPageNumber,Pages]);
                                        Dialog.Update;
                                     end;   // ++ Moskaliov AXWinGIS Printing Methods
                                  with PrintOptions.PageSetup do
                                     begin
                                        // calculate page-overlap in project-units
                                        if intPageNumber = 1 then
                                           begin
                                              ProjOverlapR:=FPInfo.CalculateDrawDouble(OverlapR);
                                              ProjOverlapB:=FPInfo.CalculateDrawDouble(OverlapB);
                                              ProjOverlapL:=FPInfo.CalculateDrawDouble(OverlapL);
                                              ProjOverlapT:=FPInfo.CalculateDrawDouble(OverlapT);
                                              ProjHorizStep:=FPInfo.CalculateDrawDouble(MetaFile.MMWidth);
                                              ProjVertStep:=FPInfo.CalculateDrawDouble(MetaFile.MMHeight);
                                           end;
                                        // current Metafile name creation
                                        Str(StartPageNum+Pages-1,strFileNumber);
                                        intFileNumber:=Length(strFileNumber);
                                        Str(StartPageNum+intPageNumber-1,strFileNumber);
                                     end;
                                  // current Metafile name creation (continuation)
                                  Cnt:=Length(strFileNumber);
                                  while Cnt < intFileNumber do
                                     begin
                                        strFileNumber:='0'+strFileNumber;
                                        Inc(Cnt);
                                     end;
                                  FileName:=MainName+strFileNumber+FileExtension;

                                  try
                                    FPInfo.ExtCanvas.PushClipping;
                                    // set cliping to the current page-frames
                                    Graphics.CurrPageClipRect:=ClippingRect;
                                    // draw graphic
                                    for Cnt:=0 to Graphics.ItemCount-1 do
                                    begin
                                      FPInfo.ClipRect:=ClippingRect;
                                      FPInfo.Extcanvas.ClipRect:=ClippingRect;
                                      FPInfo.Extcanvas.SetClippingRect(ClippingRect);
                                      if FPInfo.AbortPrinting then
                                      begin
                                        DosError:=intPageNumber;
                                        break;
                                      end;
                                      if Graphics[Cnt].Initialised then
                                        if Graphics[Cnt] is TPrintSignature
                                        then  Graphics[Cnt].Paint(GraphicSite)
                                        else if Graphics[Cnt] is TWindowsGraphic
                                        then  Graphics[Cnt].Paint(GraphicSite)
                                        else if Graphics[Cnt] is TLegendPlaceholder
                                        then  DoPrintWMFLegend(TLegendPlaceholder(Graphics[Cnt]))
                                        else if Graphics[Cnt] is TProjectPlaceholder then
{++ Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
                                        begin
{-- Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
                                          DoPrintWMFProject(NIL);
{++ Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
                                          TProjectPlaceholder(Graphics[Cnt]).DrawFrame(GraphicSite);
                                        end;
{-- Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
                                        if FPInfo.AbortPrinting then
                                        begin
                                          DosError:=intPageNumber;
                                          break;
                                        end;
                                    end;
                                  finally
                                     FPInfo.ExtCanvas.PopClipping;
                                  end;

                                  // set values of the parameters for next step ot the loop
                                  if Graphics.PageSetup.PagesOrder then   // From left to right and down
                                     begin
                                        if XPage < PageSetup.HorizontalPages-1 then
                                           begin
                                              XPage:=XPage+1;
                                              if PageRect.Left > 0 then
                                                 if PageRect.Left < MetaFile.MMWidth then
                                                    begin
                                                       FWidth:=FWidth+PageRect.Left-MetaFile.MMWidth+OverlapR;
                                                       if FWidth>0 then
                                                          FWidth:=FWidth+OverlapL;
                                                       PageRect.Left:=MetaFile.MMWidth-PageRect.Left;
                                                       ProjPrintRect.Left:=ProjPrintRect.Left+FPInfo.CalculateDrawDouble(PageRect.Left)-ProjOverlapL-ProjOverlapR;
                                                       PageRect.Left:=0;
                                                    end
                                                 else
                                                    PageRect.Left:=PageRect.Left-MetaFile.MMWidth
                                              else
                                                 begin
                                                    FWidth:=FWidth-MetaFile.MMWidth+OverlapR;
                                                    if FWidth>0 then
                                                       FWidth:=FWidth+OverlapL;
                                                    ProjPrintRect.Left:=ProjPrintRect.Left+ProjHorizStep-ProjOverlapL-ProjOverlapR;
                                                 end;
                                              for Cnt:=0 to Graphics.ItemCount-1 do
                                                  if Graphics[Cnt].Initialised then
                                                     if Graphics[Cnt] is TLegendPlaceholder then
                                                        TLegendPlaceholder(Graphics[Cnt]).LegendPos.X:=TLegendPlaceholder(Graphics[Cnt]).LegendPos.X-
                                                                   MetaFile.MMWidth+OverlapL+OverlapR;
                                              GraphicSite.XOffset:=GraphicSite.XOffset+MetaFile.MMWidth-OverlapL-OverlapR;
                                           end
                                        else
                                           begin
                                              XPage:=0;
                                              FWidth:=Round(FPInfo.CalculateDispDouble(RectWidth(WorldPrintRect)));
                                              PageRect.Left:=Round(Graphic.BoundsRect.Left*100);
                                              PageRect.Right:=Round(RectWidth(PageSetup.PageExtent)*100);
                                              if RectWidth(PageRect)>FWidth then
                                                 PageRect.Right:=PageRect.Left+FWidth;
                                              GraphicSite.XOffset:=0;
                                              ProjPrintRect.Left:=WorldPrintRect.Left;
                                              YPage:=YPage+1;
                                              if PageRect.Top < 0 then
                                                 if -PageRect.Top < MetaFile.MMHeight then
                                                    begin
                                                       FHeight:=FHeight-PageRect.Top-MetaFile.MMHeight+OverlapB;
                                                       if FHeight>0 then
                                                          FHeight:=FHeight+OverlapT;
                                                       PageRect.Top:=PageRect.Top+MetaFile.MMHeight;
                                                       ProjPrintRect.Top:=ProjPrintRect.Top-FPInfo.CalculateDrawDouble(PageRect.Top)+ProjOverlapT+ProjOverlapB;
                                                       PageRect.Top:=0;
                                                    end
                                                 else
                                                    begin
                                                       PageRect.Top:=PageRect.Top+MetaFile.MMHeight;
                                                       PageRect.Bottom:=PageRect.Bottom+MetaFile.MMHeight;
                                                    end
                                              else
                                                 begin
                                                    FHeight:=FHeight-MetaFile.MMHeight+OverlapB;
                                                    if FHeight>0 then
                                                       FHeight:=FHeight+OverlapT;
                                                    ProjPrintRect.Top:=ProjPrintRect.Top-ProjVertStep+ProjOverlapT+ProjOverlapB;
                                                 end;
                                              for Cnt:=0 to Graphics.ItemCount-1 do
                                                  if Graphics[Cnt].Initialised then
                                                     if Graphics[Cnt] is TLegendPlaceholder then
                                                        begin
                                                           TLegendPlaceholder(Graphics[Cnt]).LegendPos.X:=Round(TLegendPlaceholder(Graphics[Cnt]).BoundsRect.Left*100);
                                                           TLegendPlaceholder(Graphics[Cnt]).LegendPos.Y:=TLegendPlaceholder(Graphics[Cnt]).LegendPos.Y+
                                                                   MetaFile.MMHeight-OverlapT-OverlapB;
                                                        end;
                                              GraphicSite.YOffset:=GraphicSite.YOffset-MetaFile.MMHeight+OverlapT+OverlapB;
                                           end;
                                     end
                                  else
                                     begin
                                        if YPage < PageSetup.VerticalPages-1 then
                                           begin
                                              YPage:=YPage+1;
                                              if PageRect.Top < 0 then
                                                 if -PageRect.Top < MetaFile.MMHeight then
                                                    begin
                                                       FHeight:=FHeight-PageRect.Top-MetaFile.MMHeight+OverlapB;
                                                       if FHeight>0 then
                                                          FHeight:=FHeight+OverlapT;
                                                       PageRect.Top:=PageRect.Top+MetaFile.MMHeight;
                                                       ProjPrintRect.Top:=ProjPrintRect.Top-FPInfo.CalculateDrawDouble(PageRect.Top)+ProjOverlapT+ProjOverlapB;
                                                       PageRect.Top:=0;
                                                    end
                                                 else
                                                    begin
                                                       PageRect.Top:=PageRect.Top+MetaFile.MMHeight;
                                                       PageRect.Bottom:=PageRect.Bottom+MetaFile.MMHeight;
                                                    end
                                              else
                                                 begin
                                                    FHeight:=FHeight-MetaFile.MMHeight+OverlapB;
                                                    if FHeight>0 then
                                                       FHeight:=FHeight+OverlapT;
                                                    ProjPrintRect.Top:=ProjPrintRect.Top-ProjVertStep+ProjOverlapT+ProjOverlapB;
                                                 end;
                                              for Cnt:=0 to Graphics.ItemCount-1 do
                                                  if Graphics[Cnt].Initialised then
                                                     if Graphics[Cnt] is TLegendPlaceholder then
                                                           TLegendPlaceholder(Graphics[Cnt]).LegendPos.Y:=TLegendPlaceholder(Graphics[Cnt]).LegendPos.Y+
                                                                   MetaFile.MMHeight-OverlapT-OverlapB;
                                              GraphicSite.YOffset:=GraphicSite.YOffset-MetaFile.MMHeight+OverlapT+OverlapB;
                                           end
                                        else
                                           begin
                                              YPage:=0;
                                              // print page size setting
                                              FHeight:=Round(FPInfo.CalculateDispDouble(RectHeight(WorldPrintRect)));
                                              PageRect.Top:=-Round((RectHeight(PageSetup.PageExtent)-Graphic.BoundsRect.Top)*100);
                                              PageRect.Bottom:=-Round(RectHeight(PageSetup.PageExtent)*100);
                                              if RectHeight(PageRect)>FHeight then
                                                 PageRect.Bottom:=PageRect.Top-FHeight;
                                              GraphicSite.YOffset:=Round(RectHeight(PageSetup.PageExtent)*100);
                                              ProjPrintRect.Top:=WorldPrintRect.Top;
                                              XPage:=XPage+1;
                                              if PageRect.Left > 0 then
                                                 if PageRect.Left < MetaFile.MMWidth then
                                                    begin
                                                       FWidth:=FWidth+PageRect.Left-MetaFile.MMWidth+OverlapR;
                                                       if FWidth>0 then
                                                          FWidth:=FWidth+OverlapL;
                                                       PageRect.Left:=PageRect.Left-MetaFile.MMWidth;
                                                       ProjPrintRect.Left:=ProjPrintRect.Left+FPInfo.CalculateDrawDouble(PageRect.Left)-ProjOverlapL-ProjOverlapR;
                                                       PageRect.Left:=0;
                                                    end
                                                 else
                                                    PageRect.Left:=PageRect.Left-MetaFile.MMWidth
                                              else
                                                 begin
                                                    FWidth:=FWidth-MetaFile.MMWidth+OverlapR;
                                                    if FWidth>0 then
                                                       FWidth:=FWidth+OverlapL;
                                                    ProjPrintRect.Left:=ProjPrintRect.Left+ProjHorizStep-ProjOverlapL-ProjOverlapR;
                                                 end;
                                              for Cnt:=0 to Graphics.ItemCount-1 do
                                                  if Graphics[Cnt].Initialised then
                                                     if Graphics[Cnt] is TLegendPlaceholder then
                                                        begin
                                                           TLegendPlaceholder(Graphics[Cnt]).LegendPos.X:=TLegendPlaceholder(Graphics[Cnt]).LegendPos.X-
                                                                   MetaFile.MMWidth+OverlapL+OverlapR;
                                                           TLegendPlaceholder(Graphics[Cnt]).LegendPos.Y:=
                                                                   -Round((RectHeight(PageSetup.PageExtent)-TLegendPlaceholder(Graphics[Cnt]).BoundsRect.Top)*100);
                                                        end;
                                              GraphicSite.XOffset:=GraphicSite.XOffset+MetaFile.MMWidth-OverlapL-OverlapR;
                                           end;
                                     end;
                                  // set print page value
                                  Graphics.CurrPrintPage:=Graphics.CurrPrintPage+1;
                               end
                            else       // plmUniquePage or plmUniformLayout
                               begin   // or (PageSetup.PrintLayoutModel = plmBook and Pages = 1)
                                  try
                                     FPInfo.ExtCanvas.PushClipping;
                                     // set cliping to the current page-frames
                                     Graphics.CurrPageClipRect:=ClippingRect;
                                     // draw graphic
                                     for Cnt:=0 to Graphics.ItemCount-1 do
                                         begin
                                            FPInfo.Extcanvas.ClipRect:=ClippingRect;
                                            FPInfo.ClipRect:=ClippingRect;
                                            FPInfo.Extcanvas.SetClippingRect(ClippingRect);
                                            if FPInfo.AbortPrinting then
                                               begin
                                                  DosError:=intPageNumber;
                                                  break;
                                               end;
                                            if Graphics[Cnt].Initialised then
                                               if Graphics[Cnt] is TPrintSignature then
                                                  Graphics[Cnt].Paint(GraphicSite)
                                               else
                                                  if Graphics[Cnt] is TWindowsGraphic then
                                                     Graphics[Cnt].Paint(GraphicSite)
                                                  else
                                                     if Graphics[Cnt] is TLegendPlaceholder then
                                                        DoPrintWMFLegend(TLegendPlaceholder(Graphics[Cnt]))
                                                     else
                                                        if Graphics[Cnt] is TProjectPlaceholder then
{++ Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
                                                           begin
{-- Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
                                                              DoPrintWMFProject(NIL);
{++ Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
                                                              TProjectPlaceholder(Graphics[Cnt]).DrawFrame(GraphicSite);
                                                           end;
{-- Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
                                            if FPInfo.AbortPrinting then
                                               begin
                                                  DosError:=intPageNumber;
                                                  break;
                                               end;
                                         end;   // end of "for Cnt:=0 to Graphics.ItemCount-1 do" statement
                                  finally
                                     FPInfo.ExtCanvas.PopClipping;
                                  end;
                               end;   // end of ELSE statement for PrintLayoutModel = plmUniquePage or plmUniformLayout
                         end;   // end of "with PrintOptions do" statement
                   finally
                      Free;   // MetaFileCanvas.Free
                   end;
                if FPInfo.AbortPrinting then
                   begin
                      DosError:=intPageNumber;
                      break;
                   end;
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
//              if Pages > 1 then
                if (Pages > 1) and (not(FPrintFromDB and (FMode = 4))) then
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
                   begin
                      DosError:=FindFirst(FileName,faAnyFile,Search);
                      if DosError = 0 then
                         // file FileName already exist
                         begin
                            if Application.MessageBox(PChar(Format(MlgStringList['PrnHndl',15],[ExtractFileName(FileName)])),
                                                      PChar(MlgStringList['PrnHndl',16]),
                                                      MB_YESNO+MB_ICONQUESTION+MB_DEFBUTTON1) = IDYES then
                               MetaFile.SaveToFile(FileName);
                         end
                      else
                         MetaFile.SaveToFile(FileName);
                   end
                else
                   MetaFile.SaveToFile(FileName);
                if FPInfo.AbortPrinting then
                   begin
                      DosError:=intPageNumber;
                      break;
                   end
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
                else
                   if FPrintFromDB and FPrintMonitoring then
                      SendPrnMonDDECommand(intPageNumber,Pages,0);
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
             end;  // end of the "for intPageNumber:=1 to Pages do" statement
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
         if FPInfo.AbortPrinting then
            // User pressed Cancel button on Printing Progress Dialog
            if FPrintFromDB and FPrintMonitoring then
               SendPrnMonDDECommand(DosError,Pages,1);
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
         GraphicSite.Free;
         FPInfo.DoneCopy;
         MetaFile.Free;
      finally
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
//       EnableTaskWindows(Disabled);
         if boolValue then
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
            Dialog.Free;
{++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
         if InvisObjExist then
            InvisObjExist:=DeleteInvisibleObjects(ObjFileName);
{-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
      end;
   end;   // End of statment if FDialog.SaveWMFDialog.Execute then
end;

//++ Glukhov Bug#293(PrintFrame) Build#175 15.02.02
Function TPrintHandler.DoPrintWMFLegend(Graphics: TLegendPlaceholder):Boolean;
var
  ProjRect      : TDRect;
  Legend        : TLegend;
  longX         : LongInt;
  longY         : LongInt;
  XOffset       : Double;
  YOffset       : Double;
  Points        : array[0..3] of TPoint;
  OldMapMode    : TMapMode;
  ARect         : TRect;
begin
  if ((Graphics.LegendIndex >= 0) and not RectEmpty(Graphics.BoundsRect))
  or (Graphics.LegendName = csLegName4Frame) or (Graphics.LegendName = csOleFrame)
  then begin
    XOffset:=Graphics.LegendPos.X+Round(RectWidth(Graphics.BoundsRect)*100);
    YOffset:=Graphics.LegendPos.Y-Round(RectHeight(Graphics.BoundsRect)*100);
    if (XOffset > ClippingRect.Left) and (Graphics.LegendPos.X < ClippingRect.Right)
    and (YOffset < ClippingRect.Top) and (Graphics.LegendPos.Y > ClippingRect.Bottom)
    then begin
      FPInfo.ExtCanvas.Push;
      FPInfo.ExtCanvas.PushClipping;
      try
        FPInfo.SetViewRotation(0);
        FPInfo.XOffset:=-Graphics.LegendPos.X;
        FPInfo.YOffset:=-YOffset;

        longX:=FPInfo.CalculateDraw(Round(RectWidth(Graphics.BoundsRect)*100));
        longY:=FPInfo.CalculateDraw(Round(RectHeight(Graphics.BoundsRect)*100));
        ProjRect.Assign(0,0,Abs(longX),Abs(longY));
//        Legend:=Project^.Legends[Graphics.LegendIndex];

        FPInfo.Extcanvas.ClipRect:=ClippingRect;
        FPInfo.ClipRect:=ClippingRect;
        with FPInfo.ClipRect do
          SetRect(FPInfo.ClipLimits,Left,Bottom,Right,Top);
        FPInfo.Extcanvas.SetClippingRect(ClippingRect);

        if (Graphics.LegendName <> csLegName4Frame)
        and (Graphics.LegendName <> csOleFrame)
        then begin
          Legend:=Project^.Legends[Graphics.LegendIndex];
          Legend.Draw(@FPInfo,Project^.Layers,ProjRect);
        end;

        FPInfo.ConvertToDisp( ProjRect.A, Points[0] );
        FPInfo.ConvertToDisp( ProjRect.B, Points[2] );
        longY:= ProjRect.A.Y;
        ProjRect.A.Y:= ProjRect.B.Y;
        FPInfo.ConvertToDisp( ProjRect.A, Points[1] );
        ProjRect.B.Y:= longY;
        FPInfo.ConvertToDisp( ProjRect.B, Points[3] );

        FPInfo.ExtCanvas.BeginPaint;
        if Graphics.LegendName = csOleFrame then begin
           if Graphics.OleObj.State <> osEmpty then begin
              OldMapMode:=FPInfo.ExtCanvas.MapMode;
              try
                 ARect:=Rect(Points[1].x,Points[1].y,Points[3].x,Points[3].y);
                 LPToDP(FPInfo.ExtCanvas.Handle,ARect,2);
                 FPInfo.ExtCanvas.MapMode:=mmText;
                 Graphics.OleObj.Width:=Abs(ARect.Right-ARect.Left)+1;
                 Graphics.OleObj.Height:=Abs(ARect.Bottom-ARect.Top)+1;
                 Graphics.OleObj.PaintTo(FPInfo.ExtCanvas.Handle,ARect.Left,ARect.Top);
              finally
                 FPInfo.ExtCanvas.MapMode:=OldMapMode;
              end;
           end;
        end;
//        else begin
           FPInfo.ExtCanvas.Pen.Color:=clBlack;
           FPInfo.ExtCanvas.Pen.Style:=ptSolid;
           if (Graphics.BitFlags and cwPrnTempFrmBrdLeft)<>0 then begin
              FPInfo.ExtCanvas.MoveTo( Points[0].X, Points[0].Y );
              FPInfo.ExtCanvas.LineTo( Points[1].X, Points[1].Y );
           end;
           if (Graphics.BitFlags and cwPrnTempFrmBrdTop)<>0 then begin
              FPInfo.ExtCanvas.MoveTo( Points[1].X, Points[1].Y );
              FPInfo.ExtCanvas.LineTo( Points[2].X, Points[2].Y );
           end;
           if (Graphics.BitFlags and cwPrnTempFrmBrdRight)<>0 then begin
              FPInfo.ExtCanvas.MoveTo( Points[2].X, Points[2].Y );
              FPInfo.ExtCanvas.LineTo( Points[3].X, Points[3].Y );
           end;
           if (Graphics.BitFlags and cwPrnTempFrmBrdBottom)<>0 then begin
              FPInfo.ExtCanvas.MoveTo( Points[3].X, Points[3].Y );
              FPInfo.ExtCanvas.LineTo( Points[0].X, Points[0].Y );
           end;
//        end;
        FPInfo.ExtCanvas.EndPaint;

      finally
        FPInfo.ExtCanvas.PopClipping;
        FPInfo.ExtCanvas.Pop;
      end;
    end;
  end;
  Result:=TRUE;
end;
//-- Glukhov Bug#293(PrintFrame) Build#175 15.02.02

Function TPrintHandler.DoPrintWMFProject(Scroller: TGraphicSite):Boolean;
var ARect          : TRect;
begin
  if (FWidth>0) and (FHeight>0) then
     begin
        FPInfo.ExtCanvas.Push;
        FPInfo.ExtCanvas.PushClipping;
        try
           ARect:=PageRect;
           FPInfo.SetViewRotation(-FDialog.PrintOptions.PrintRect.Rotation);
           // calculate print-area, offsets and wingis clipping area
           FPInfo.XOffset:=Round(FPInfo.CalculateDispDouble(ProjPrintRect.Left)-ARect.Left);
           FPInfo.YOffset:=Round(FPInfo.CalculateDispDouble(ProjPrintRect.Top)-ARect.Top);

           if ARect.Left < ClippingRect.Left then
              ARect.Left:=ClippingRect.Left;
           if ARect.Top > ClippingRect.Top then
              ARect.Top:=ClippingRect.Top;
           if ARect.Right > ClippingRect.Right then
              ARect.Right:=ClippingRect.Right;
           if ARect.Bottom < ClippingRect.Bottom then
              ARect.Bottom:=ClippingRect.Bottom;

           FPInfo.ExtCanvas.SetClippingRect(ARect);
//           GetClipBox(FPInfo.ExtCanvas.Handle,FPInfo.ClipRect);
           FPInfo.ClipRect:=ARect;
//           InflateRect(FPInfo.ClipRect,1000,-1000);
           with FPInfo.ClipRect do
              SetRect(FPInfo.ClipLimits,Left,Bottom,Right,Top);
           FPInfo.ExtCanvas.ClipRect:=FPInfo.ClipRect;
           Project^.DrawGraphic(@FPInfo);                   
           TooltipThread.DrawFixedFloatingTexts(@FPInfo);
        finally
           FPInfo.ExtCanvas.PopClipping;
           FPInfo.ExtCanvas.Pop;
        end;
     end;
  Result:=TRUE;
end;

{++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
Function TPrintHandler.CreateInvisibleObjects(var FileName: String): Boolean;
var ansiFilePath   : AnsiString;
    ansiFileName   : AnsiString;
    ansiFileExt    : AnsiString;
    intValue       : Integer;
    Search         : TSearchRec;
    DosError       : Integer;
    WindowsGraphic : TWindowsGraphic;
    PrintSignature : TPrintSignature;
    Width          : Double;
    Height         : Double;
    LeftPos        : Double;
    PercentConst   : Double;
    ProgFile       : Array[0..255] of Char;
    VersionInfo    : PChar;
    FileOffset     : LongWord;
    StructSize     : LongInt;
    Info           : Pointer;
    InfoSize       : LongWord;
    Version        : String;
    SigText        : String;   //++ Unknown Person from Progis-Austria 19.02.01
begin
   Result:=FALSE;
{++ Unknown Person from Progis-Austria 19.02.01}
   SigText:=GetLangText(19000);
   if (SigText <> 'Unknown19000') and (SigText <> '') then
{-- Unknown Person from Progis-Austria 19.02.01}
      begin
         SetLength(ansiFilePath,SizeOf(FileName));
         intValue:=GetTempPath(SizeOf(FileName),PChar(ansiFilePath));
         SetLength(ansiFilePath,intValue);
         SetLength(ansiFileExt,4);
         ansiFileExt:='.tmp';
         intValue:=0;
         ansiFileName:=ansiFilePath+'WGprint'+IntToStr(intValue)+ansiFileExt;
         DosError:=FindFirst(ansiFileName,faAnyFile,Search);
         while DosError = 0 do
            // file FileName already exist
            begin
               intValue:=intValue+1;
               ansiFileName:=ansiFilePath+'WGprint'+IntToStr(intValue)+ansiFileExt;
               DosError:=FindFirst(ansiFileName,faAnyFile,Search);
            end;
         FileName:=ansiFileName;
//         FDialog.ProgisLogo.Picture.SaveToFile(FileName);
         //WindowsGraphic:=TWindowsGraphic.Create(FDialog.PrintOptions.Graphics);
         Result:=TRUE;
         {
         WindowsGraphic.BoundsRect:=GrRect(0,0,8.33,1.94);
         WindowsGraphic.CreateFromFile(ExtractFileName(FileName),ExtractFilePath(FileName),TRUE);
         WindowsGraphic.KeepAspect:=TRUE;
         WindowsGraphic.PrintLocation:=plOnPages;
         WindowsGraphic.OnFirstPage:=TRUE;
         WindowsGraphic.OnInternalPages:=TRUE;
         WindowsGraphic.OnLastPage:=TRUE;
         WindowsGraphic.PrintOrigin:=poBottom;
         }
         // PercentConst is the value of the WinGIS Logotype's picture width
         // in percents from the width of current paper format (printable area)
         // You can changhe PercentConst value for achievement of the necessary
         // size of the WinGIS Logotype
         {
         PercentConst:=0.10;
         Width:=PercentConst*RectWidth(FDialog.PrintOptions.PageSetup.PageExtents[0,0]);
         if Width > RectWidth(WindowsGraphic.OriginalBoundsRect) then
            // It's necessary, if you want to limit the sizes of Logotype's picture
            // to the sizes real picture
            Width:=RectWidth(WindowsGraphic.OriginalBoundsRect);
         Height:=(RectHeight(WindowsGraphic.OriginalBoundsRect)*Width)/RectWidth(WindowsGraphic.OriginalBoundsRect);
         LeftPos:=RectWidth(FDialog.PrintOptions.PageSetup.PageExtents[0,0])-Width;
         WindowsGraphic.BoundsRect:=GrRect(LeftPos,0,LeftPos+Width,Height);
         }
         // You can create special Signature(s) if it is necessary
         PrintSignature:=TPrintSignature.Create(FDialog.PrintOptions.Graphics);
         PrintSignature.BoundsRect:=GrRect(0,0,12,2);
{++ Unknown Person from Progis-Austria 19.02.01}
//       PrintSignature.Signature:='Printed &Date. with using WinGIS '+Version+'from project &File.';
//       PrintSignature.Signature:='WinMAP LT Hofkarte ('+DateToStr(Date)+') - Massstab &Scale. - DKM und Orthobilder (C)BEV';
         PrintSignature.Signature:=Format(GetLangText(19000),[DateToStr(Now)]);
{-- Unknown Person from Progis-Austria 19.02.01}
         PrintSignature.PrintLocation:=plOnPages;
         PrintSignature.OnFirstPage:=TRUE;
         PrintSignature.OnInternalPages:=TRUE;
         PrintSignature.OnLastPage:=TRUE;
         PrintSignature.PrintOrigin:=poBottom;
         PrintSignature.PrintPosition:=ppCenterHoriz;
         PrintSignature.TextStyle.FontName:='Arial';
         PrintSignature.TextStyle.Alignment:=0;
         PrintSignature.TextStyle.Bold:=0;
         PrintSignature.TextStyle.Italic:=0;
         PrintSignature.TextStyle.Underlined:=0;
         PrintSignature.TextStyle.Transparent:=0;
         PrintSignature.TextStyle.ForeColor:=clBlack;
         PrintSignature.TextStyle.BackColor:=clWhite;
         PrintSignature.TextStyle.Height:=12;   // 8pt
         PrintSignature.ForeColor:=clBlack;
      end;
end;

Function TPrintHandler.DeleteInvisibleObjects(var FileName: String): Boolean;
var intValue       : Integer;
    strValue       : String;
    WindowsGraphic : TWindowsGraphic;
    PrintSignature : TPrintSignature;
begin
   Result:=TRUE;
   intValue:=FDialog.PrintOptions.Graphics.ItemCount-1;
   if FDialog.PrintOptions.Graphics.Items[intValue].ClassName = 'TPrintSignature' then
      begin
         PrintSignature:=TPrintSignature(FDialog.PrintOptions.Graphics.Items[intValue]);
         FDialog.PrintOptions.Graphics.Delete(intValue);
         PrintSignature.Free;
         intValue:=intValue-1;
      end;
   if FDialog.PrintOptions.Graphics.Items[intValue].ClassName = 'TWindowsGraphic' then
      begin
         WindowsGraphic:=TWindowsGraphic(FDialog.PrintOptions.Graphics.Items[intValue]);
         strValue:=WindowsGraphic.RelativePath+WindowsGraphic.FileName;
         if FileName =  strValue then
            begin
               FDialog.PrintOptions.Graphics.Delete(intValue);
               WindowsGraphic.Free;
               DeleteFile(FileName);
               Result:=FALSE;
           end;
      end;
end;
{-- Moskaliov Pictures Location Control BUILD#131 18.09.00}

Procedure TPrintHandler.DoPrint(PrintOptions: TPrintOptions);
var WorldPrintRect : TGrRect;
    XPage          : Integer;
    YPage          : Integer;
    OverlapL       : Integer;     // left page-overlap in virtual units
    OverlapT       : Integer;     // top page-overlap in virtual units
    OverlapR       : Integer;     // right page-overlap in virtual units
    OverlapB       : Integer;     // bottom page-overlap in virtual units
    ProjOverlapL   : LongInt;     // left pageoverlap in project-units
    ProjOverlapT   : LongInt;     // top pageoverlap in project-units
    ProjOverlapR   : LongInt;     // right pageoverlap in project-units
    ProjOverlapB   : LongInt;     // bottom pageoverlap in project-units
    Dialog         : TPrintStatusDialog;
//  Disabled       : Pointer;     // pointer to disabled windows-information
    GraphicSite    : TGraphicSite;
    Cnt            : Integer;
    Graphic        : TProjectPlaceholder;
{++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
    InvisObjExist  : Boolean;
    ObjFileName    : String;
{-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
    Pages          : Integer;
    boolValue      : Boolean;
    OldClippingRect: TRect;
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
begin
{++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
  InvisObjExist:=CreateInvisibleObjects(ObjFileName);
{-- Moskaliov Pictures Location Control BUILD#131 18.09.00}

{++ Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
  with PrintOptions.PageSetup do
     Pages:=VerticalPages*HorizontalPages;

  boolValue:= FPrintFromDB and ((FMode mod 2) = 0);
  if boolValue then
     boolValue:=FShowProgress
  else
     boolValue:=TRUE;

  if boolValue then
     begin
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
        Application.CreateForm(TPrintStatusDialog,Dialog);
//      Disabled:=DisableTaskWindows(0);
        Dialog.Show;
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
//      Dialog.Update;
        SetWindowPos(Dialog.Handle,HWND_TOPMOST,Dialog.Left,Dialog.Top,Dialog.Width,Dialog.Height,SWP_SHOWWINDOW);
        Dialog.Update;
     end;
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
  try
    FPageSetup:=PrintOptions.PageSetup;
    with Project^,PrintOptions,FPageSetup,Printer do
    begin
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
      if boolValue then
         begin
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
            // setup document-title and status-dialog-labels
            Title:='WinGIS - '+ExtractFileName(StrPas(FName));
            Dialog.FileLabel.Caption:=ExtractFileName(StrPas(FName));
            Dialog.PrinterLabel.Caption:=Name;
            if PrintOptions.PrintToFile then begin
               if FDialog.DialogSaveToFile.Execute then begin
                  FFileName:=FDialog.DialogSaveToFile.FileName;
               end;
               Dialog.PortLabel.Caption:=Format(MlgStringList['PrnHndl',5],['FILE: ' + FFileName])
            end
            else begin
               Dialog.PortLabel.Caption:=Format(MlgStringList['PrnHndl',5],[Port]);
            end;
            Dialog.Update;
         end;   //++ Moskaliov AXWinGIS Printing Methods
      // setup the printer
      SetOptionsToPrinter;

      Printer.PrintToFile:=PrintOptions.PrintToFile;
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
      if PrintOptions.PrintToFile {and FPrintFromDB and ((FMode mod 2) = 0)} then
         Printer.FileName:=FFileName
      else
         Printer.FileName:='';
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
      // start printing the document
      BeginDoc;
      // create PInfo for printing and set options
      FPInfo.InitCopy(PInfo);
      FPInfo.Printing:=TRUE;
      FPInfo.WMPaint:=TRUE;
      // setup extended-canvas-object for printing
      FPInfo.ExtCanvas.Cached:=FALSE;
      FPInfo.ExtCanvas.Handle:=Handle;
      FPInfo.ExtCanvas.MapMode:=mmLoMetric;
      // detect the project placeholder
      for Cnt:=0 to Graphics.ItemCount-1 do
//          if Graphics[Cnt].Initialised then
          if Graphics[Cnt] is TProjectPlaceholder then
             begin
                Graphic:=TProjectPlaceholder(Graphics[Cnt]);
                FPInfo.Zoom:=FPInfo.ScaleToZoom(Graphic.PrintScale);
                Break;
             end;
      OutputToPlotter:=GetDeviceCaps(Handle,TECHNOLOGY)=dt_Plotter;
      FPInfo.AbortPrinting:=FALSE;
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
      if boolValue then
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
         Dialog.CancelPressed:=@FPInfo.AbortPrinting;
      // setup GraphicSite
      GraphicSite:=TGraphicSite.Create(NIL);
      GraphicSite.Scale:=10;
      GraphicSite.Canvas:=Canvas;
      GraphicSite.XOffset:=PrintableArea.Left;
      GraphicSite.YOffset:=RectHeight(PageSetup.PageExtent)*10+PrintableArea.Top;
      // setup print-rotation
      FPInfo.SetViewRotation(-PrintRect.Rotation);
      WorldPrintRect:=ISOFrame(RotatedRect(PrintRect,-PrintRect.Rotation));
      FHeight:=Round(FPInfo.CalculateDispDouble(RectHeight(WorldPrintRect)));
      FWidth:=Round(FPInfo.CalculateDispDouble(RectWidth(WorldPrintRect)));
      // calculate page-overlap in virutal and project-units
      OverlapL:=Round(PageSetup.OverlapPageFrames.Left*10);
      OverlapT:=Round(PageSetup.OverlapPageFrames.Top*10);
      OverlapR:=Round(PageSetup.OverlapPageFrames.Right*10);
      OverlapB:=Round(PageSetup.OverlapPageFrames.Bottom*10);
      ProjOverlapR:=FPInfo.CalculateDraw(OverlapR);
      ProjOverlapB:=FPInfo.CalculateDraw(OverlapB);
      ProjOverlapL:=FPInfo.CalculateDraw(OverlapL);
      ProjOverlapT:=FPInfo.CalculateDraw(OverlapT);
      ProjPrintRect:=WorldPrintRect;
      // set current print page value
      Graphics.CurrPrintPage:=Graphics.PageSetup.StartPageNum;

      if Graphics.PageSetup.PagesOrder then   // From left to right and down
         // for all pages in x- and y-direction
         for YPage:=0 to PageSetup.VerticalPages-1 do
             begin
                // calculate top-position of current rectangle
                if YPage=0 then
                   begin
                      PageRect.Top:=-Round((RectHeight(PageSetup.PageExtent)-Graphic.BoundsRect.Top+
                                     PageSetup.PageFrames.Top)*10);
                      for Cnt:=0 to Graphics.ItemCount-1 do
                          if Graphics[Cnt].Initialised then
                             if Graphics[Cnt] is TLegendPlaceholder then
                                TLegendPlaceholder(Graphics[Cnt]).LegendPos.Y:=-Round((RectHeight(PageSetup.PageExtent)-
                                                                                TLegendPlaceholder(Graphics[Cnt]).BoundsRect.Top)*10);
                      ClippingRect.Top:=-Round(PageSetup.PageFrames.Top*10);
                   end
                else
                   begin
                      if PrintLayoutModel = plmUniformLayout then
                         PageRect.Top:=-Round(PageSetup.InterPageFrames.Top*10)
                      else   // PrintLayoutModel = plmBook
                         PageRect.Top:=-Round(PageSetup.PageFrames.Top*10);
                         ClippingRect.Top:=PageRect.Top;
                   end;
                // calculate bottom of current rectangle
                if PrintLayoutModel = plmUniformLayout then
                   begin
                      if YPage=PageSetup.VerticalPages-1 then
                         PageRect.Bottom:=PageSize.Bottom+Round(PageSetup.PageFrames.Bottom*10)
                      else
                         PageRect.Bottom:=PageSize.Bottom+Round(PageSetup.InterPageFrames.Bottom*10);
                   end
                else   // PrintLayoutModel = plmBook
                   PageRect.Bottom:=PageSize.Bottom+Round(PageSetup.PageFrames.Bottom*10);
                ClippingRect.Bottom:=PageRect.Bottom;
                // correct bottom if print-range is smaller
                if RectHeight(PageRect)>FHeight then
                   PageRect.Bottom:=PageRect.Top-FHeight;
                ProjPrintRect.Left:=WorldPrintRect.Left;
                FWidth:=Round(FPInfo.CalculateDispDouble(RectWidth(WorldPrintRect)));
                ProjPrintRect.Bottom:=ProjPrintRect.Top-FPInfo.CalculateDrawDouble(RectHeight(PageRect));
                GraphicSite.XOffset:=PrintableArea.Left;
                GraphicSite.YOffset:=GraphicSite.YOffset-ClippingRect.Top;
                for XPage:=0 to PageSetup.HorizontalPages-1 do
                    begin
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
                       if boolValue then
                          begin
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
                             // update the page-number-display in the printing-status-dialog
                             Dialog.PageLabel.Caption:=Format(MlgStringList['PrnHndl',6],[PageNumber,PageSetup.VerticalPages*PageSetup.HorizontalPages]);
                             Dialog.Update;
                          end;   // ++ Moskaliov AXWinGIS Printing Methods
                       // calculate left-position of current rectangle
                       if XPage=0 then
                          begin
                             PageRect.Left:=Round((Graphic.BoundsRect.Left+PageSetup.PageFrames.Left)*10);
                             for Cnt:=0 to Graphics.ItemCount-1 do
                                 if Graphics[Cnt].Initialised then
                                    if Graphics[Cnt] is TLegendPlaceholder then
                                       TLegendPlaceholder(Graphics[Cnt]).LegendPos.X:=Round(TLegendPlaceholder(Graphics[Cnt]).BoundsRect.Left*10);
                             ClippingRect.Left:=Round(PageSetup.PageFrames.Left*10);
                          end
                       else
                          begin
                             if PrintLayoutModel = plmUniformLayout then
                                PageRect.Left:=Round(PageSetup.InterPageFrames.Left*10)
                             else   // PrintLayoutModel = plmBook
                                PageRect.Left:=Round(PageSetup.PageFrames.Left*10);
                             ClippingRect.Left:=PageRect.Left;
                          end;
                       // calculate right-position of current rectangle
                       if PrintLayoutModel = plmUniformLayout then
                          if XPage=PageSetup.HorizontalPages-1 then
                             PageRect.Right:=PageSize.Right-Round(PageSetup.PageFrames.Right*10)
                          else
                             PageRect.Right:=PageSize.Right-Round(PageSetup.InterPageFrames.Right*10)
                       else   // PrintLayoutModel = plmBook
                          PageRect.Right:=PageSize.Right-Round(PageSetup.PageFrames.Right*10);
                       ClippingRect.Right:=PageRect.Right;
                       if RectWidth(PageRect)>FWidth then
                          PageRect.Right:=PageRect.Left+FWidth;
                       ProjPrintRect.Right:=ProjPrintRect.Left+FPInfo.CalculateDrawDouble(RectWidth(PageRect));
                       try
                          FPInfo.ExtCanvas.PushClipping;
                          // set cliping to the current page-frames
                          Graphics.CurrPageClipRect:=MovedRect(ClippingRect,-PrintableArea.Left,-PrintableArea.Top);
                          FPInfo.Extcanvas.SetClippingRect(Graphics.CurrPageClipRect);
                          // draw graphic
                          GraphicSite.XOffset:=GraphicSite.XOffset-ClippingRect.Left;

                          for Cnt:=0 to Graphics.ItemCount-1 do
                              if Graphics[Cnt].Initialised then
                                 if Graphics[Cnt] is TPrintSignature then
                                    Graphics[Cnt].Paint(GraphicSite)
                                 else
                                    if Graphics[Cnt] is TWindowsGraphic then
                                       Graphics[Cnt].Paint(GraphicSite)
                                    else
                                       if Graphics[Cnt] is TLegendPlaceholder then
                                          DoPrintLegend(TLegendPlaceholder(Graphics[Cnt]))
                                       else
                                          if Graphics[Cnt] is TProjectPlaceholder then
{++ Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
                                             begin
{-- Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
                                                DoPrintProject(NIL);
{++ Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
                                                TProjectPlaceholder(Graphics[Cnt]).DrawFrame(GraphicSite);
                                             end;
{-- Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
                          GraphicSite.XOffset:=GraphicSite.XOffset+ClippingRect.Left+
                                               RectWidth(ClippingRect)-OverlapL-OverlapR;
                       finally
                          FPInfo.ExtCanvas.PopClipping;
                       end;
{++ Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
                       if PrintLayoutModel = plmUniformLayout then
                          if FPageSetup.PrintCropMarks then
                             begin
                                if XPage > 0 then
                                   with Graphics.CurrPageClipRect do
                                      Left:=Left+OverlapL;
                                if XPage < PageSetup.HorizontalPages-1 then
                                   with Graphics.CurrPageClipRect do
                                      Right:=Right-OverlapL;
                                if YPage > 0 then
                                   with Graphics.CurrPageClipRect do
                                      Top:=Top-OverlapT;
                                if YPage < PageSetup.VerticalPages-1 then
                                   with Graphics.CurrPageClipRect do
                                      Bottom:=Bottom+OverlapB;
                                DrawCropMarks(GraphicSite,Graphics.CurrPageClipRect);

                                OldClippingRect:=ClippingRect;
                                ClippingRect.Left:=PageSize.Left;
                                ClippingRect.Bottom:=PageSize.Bottom;
                                try
                                   FPInfo.ExtCanvas.PushClipping;
                                   Graphics.CurrPageClipRect:=MovedRect(ClippingRect,0,-PrintableArea.Top);
                                   FPInfo.Extcanvas.SetClippingRect(Graphics.CurrPageClipRect);
                                   DrawPageNum(GraphicSite,Graphics.CurrPageClipRect,Graphics.CurrPrintPage,Graphics.PageSetup.HorizontalPages*Graphics.PageSetup.VerticalPages);
                                finally
                                   ClippingRect:=OldClippingRect;
                                   FPInfo.ExtCanvas.PopClipping;
                                end;
                             end;
{-- Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
                       if FPInfo.AbortPrinting then
                          break;
                       FWidth:=FWidth-RectWidth(PageRect)+OverlapR;
                       if FWidth>0 then
                          FWidth:=FWidth+OverlapL;
                       for Cnt:=0 to Graphics.ItemCount-1 do
                           if Graphics[Cnt].Initialised then
                              if Graphics[Cnt] is TLegendPlaceholder then
                                 TLegendPlaceholder(Graphics[Cnt]).LegendPos.X:=TLegendPlaceholder(Graphics[Cnt]).LegendPos.X-
                                                                                RectWidth(ClippingRect)+OverlapL+OverlapR;
                       ProjPrintRect.Left:=ProjPrintRect.Left+RectWidth(ProjPrintRect)-ProjOverlapL-ProjOverlapR;
                       if (XPage<PageSetup.HorizontalPages-1) or (YPage<PageSetup.VerticalPages-1) then
                          begin
                             NewPage;
                             // set map-mode to lo-metric because some printer-drivers
                             // reset the map-mode if a new page is started
                             FPInfo.ExtCanvas.MapMode:=mmLoMetric;
                          end;
                       // set current print page value
                       Graphics.CurrPrintPage:=Graphics.CurrPrintPage+1;
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
                       if FPrintFromDB and FPrintMonitoring then
                          SendPrnMonDDECommand(PageNumber,Pages,0);
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
                    end;
                if FPInfo.AbortPrinting then
                   break;
                FHeight:=FHeight-RectHeight(PageRect)+OverlapB;
                if FHeight>0 then
                   FHeight:=FHeight+OverlapT;
                for Cnt:=0 to Graphics.ItemCount-1 do
                    if Graphics[Cnt].Initialised then
                       if Graphics[Cnt] is TLegendPlaceholder then
                          TLegendPlaceholder(Graphics[Cnt]).LegendPos.Y:=TLegendPlaceholder(Graphics[Cnt]).LegendPos.Y+
                                                                         RectHeight(ClippingRect)-OverlapT-OverlapB;
                ProjPrintRect.Top:=ProjPrintRect.Top-RectHeight(ProjPrintRect)+ProjOverlapT+ProjOverlapB;
                GraphicSite.YOffset:=GraphicSite.YOffset+ClippingRect.Top-RectHeight(ClippingRect)+OverlapT+OverlapB;
             end
      else
         // for all pages in y- and x-direction
         for XPage:=0 to PageSetup.HorizontalPages-1 do
             begin
                // calculate left-position of current rectangle
                if XPage=0 then
                   begin
                      PageRect.Left:=Round((Graphic.BoundsRect.Left+PageSetup.PageFrames.Left)*10);
                      for Cnt:=0 to Graphics.ItemCount-1 do
                          if Graphics[Cnt].Initialised then
                             if Graphics[Cnt] is TLegendPlaceholder then
                                TLegendPlaceholder(Graphics[Cnt]).LegendPos.X:=Round(TLegendPlaceholder(Graphics[Cnt]).BoundsRect.Left*10);
                      ClippingRect.Left:=Round(PageSetup.PageFrames.Left*10);
                   end
                else
                   begin
                      if PrintLayoutModel = plmUniformLayout then
                         PageRect.Left:=Round(PageSetup.InterPageFrames.Left*10)
                      else   // PrintLayoutModel = plmBook
                         PageRect.Left:=Round(PageSetup.PageFrames.Left*10);
                      ClippingRect.Left:=PageRect.Left;
                   end;
                // calculate right-position of current rectangle
                if PrintLayoutModel = plmUniformLayout then
                   if XPage=PageSetup.HorizontalPages-1 then
                      PageRect.Right:=PageSize.Right-Round(PageSetup.PageFrames.Right*10)
                   else
                      PageRect.Right:=PageSize.Right-Round(PageSetup.InterPageFrames.Right*10)
                else   // PrintLayoutModel = plmBook
                   PageRect.Right:=PageSize.Right-Round(PageSetup.PageFrames.Right*10);
                ClippingRect.Right:=PageRect.Right;

                // correct right if print-range is smaller
                if RectWidth(PageRect)>FWidth then
                   PageRect.Right:=PageRect.Left+FWidth;
                ProjPrintRect.Right:=ProjPrintRect.Left+FPInfo.CalculateDrawDouble(RectWidth(PageRect));

                ProjPrintRect.Top:=WorldPrintRect.Top;
                FHeight:=Round(FPInfo.CalculateDispDouble(RectHeight(WorldPrintRect)));
                GraphicSite.XOffset:=GraphicSite.XOffset-ClippingRect.Left;
                GraphicSite.YOffset:=RectHeight(PageSetup.PageExtent)*10+PrintableArea.Top;
                for YPage:=0 to PageSetup.VerticalPages-1 do
                    begin
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
                       if boolValue then
                          begin
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
                             // update the page-number-display in the printing-status-dialog
                             Dialog.PageLabel.Caption:=Format(MlgStringList['PrnHndl',6],[PageNumber,PageSetup.VerticalPages*PageSetup.HorizontalPages]);
                             Dialog.Update;
                          end;   //++ Moskaliov AXWinGIS Printing Methods
                       // calculate top-position of current rectangle
                       if YPage=0 then
                          begin
                             PageRect.Top:=-Round((RectHeight(PageSetup.PageExtent)-Graphic.BoundsRect.Top+
                                            PageSetup.PageFrames.Top)*10);
                             for Cnt:=0 to Graphics.ItemCount-1 do
                                 if Graphics[Cnt].Initialised then
                                    if Graphics[Cnt] is TLegendPlaceholder then
                                       TLegendPlaceholder(Graphics[Cnt]).LegendPos.Y:=-Round((RectHeight(PageSetup.PageExtent)-
                                                                                       TLegendPlaceholder(Graphics[Cnt]).BoundsRect.Top)*10);
                             ClippingRect.Top:=-Round(PageSetup.PageFrames.Top*10);
                          end
                       else
                          begin
                             if PrintLayoutModel = plmUniformLayout then
                                PageRect.Top:=-Round(PageSetup.InterPageFrames.Top*10)
                             else   // PrintLayoutModel = plmBook
                                PageRect.Top:=-Round(PageSetup.PageFrames.Top*10);
                             ClippingRect.Top:=PageRect.Top;
                          end;
                       // calculate bottom of current rectangle
                       if PrintLayoutModel = plmUniformLayout then
                          begin
                             if YPage=PageSetup.VerticalPages-1 then
                                PageRect.Bottom:=PageSize.Bottom+Round(PageSetup.PageFrames.Bottom*10)
                             else
                                PageRect.Bottom:=PageSize.Bottom+Round(PageSetup.InterPageFrames.Bottom*10);
                          end
                       else   // PrintLayoutModel = plmBook
                          PageRect.Bottom:=PageSize.Bottom+Round(PageSetup.PageFrames.Bottom*10);
                       ClippingRect.Bottom:=PageRect.Bottom;
                       // correct bottom if print-range is smaller
                       if RectHeight(PageRect)>FHeight then
                          PageRect.Bottom:=PageRect.Top-FHeight;
                       ProjPrintRect.Bottom:=ProjPrintRect.Top-FPInfo.CalculateDrawDouble(RectHeight(PageRect));
                       try
                          FPInfo.ExtCanvas.PushClipping;
                          // set cliping to the current page-frames
                          Graphics.CurrPageClipRect:=MovedRect(ClippingRect,-PrintableArea.Left,-PrintableArea.Top);
                          FPInfo.Extcanvas.SetClippingRect(Graphics.CurrPageClipRect);
                          // draw graphic
                          GraphicSite.YOffset:=GraphicSite.YOffset-ClippingRect.Top;
                          for Cnt:=0 to Graphics.ItemCount-1 do
                              if Graphics[Cnt].Initialised then
                                 if Graphics[Cnt] is TLegendPlaceholder then
                                    DoPrintLegend(TLegendPlaceholder(Graphics[Cnt]))
                                 else
                                    if Graphics[Cnt] is TPrintSignature then
                                       Graphics[Cnt].Paint(GraphicSite)
                                    else
                                       if Graphics[Cnt] is TWindowsGraphic then
                                          Graphics[Cnt].Paint(GraphicSite)
                                       else
                                          if Graphics[Cnt] is TProjectPlaceholder then
{++ Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
                                             begin
{-- Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
                                                DoPrintProject(NIL);
{++ Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
                                                TProjectPlaceholder(Graphics[Cnt]).DrawFrame(GraphicSite);
                                             end;
{-- Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
                          GraphicSite.YOffset:=GraphicSite.YOffset+ClippingRect.Top-
                                               RectHeight(ClippingRect)+OverlapT+OverlapB;
                       finally
                          FPInfo.ExtCanvas.PopClipping;
                       end;
{++ Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
                       if PrintLayoutModel = plmUniformLayout then
                          if FPageSetup.PrintCropMarks then
                             begin
                                if XPage > 0 then
                                   with Graphics.CurrPageClipRect do
                                      Left:=Left+OverlapL;
                                if XPage < PageSetup.HorizontalPages-1 then
                                   with Graphics.CurrPageClipRect do
                                      Right:=Right-OverlapL;
                                if YPage > 0 then
                                   with Graphics.CurrPageClipRect do
                                      Top:=Top-OverlapT;
                                if YPage < PageSetup.VerticalPages-1 then
                                   with Graphics.CurrPageClipRect do
                                      Bottom:=Bottom+OverlapB;
                                DrawCropMarks(GraphicSite,Graphics.CurrPageClipRect);
                             end;
{-- Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
                       if FPInfo.AbortPrinting then
                          break;
                       FHeight:=FHeight-RectHeight(PageRect)+OverlapB;
                       if FHeight>0 then
                          FHeight:=FHeight+OverlapT;
                       for Cnt:=0 to Graphics.ItemCount-1 do
                           if Graphics[Cnt].Initialised then
                              if Graphics[Cnt] is TLegendPlaceholder then
                                 TLegendPlaceholder(Graphics[Cnt]).LegendPos.Y:=TLegendPlaceholder(Graphics[Cnt]).LegendPos.Y+
                                                                                RectHeight(ClippingRect)-OverlapT-OverlapB;
                       ProjPrintRect.Top:=ProjPrintRect.Top-RectHeight(ProjPrintRect)+ProjOverlapT+ProjOverlapB;
                       if (XPage<PageSetup.HorizontalPages-1) or (YPage<PageSetup.VerticalPages-1) then
                          begin
                             NewPage;
                             // set map-mode to lo-metric because some printer-drivers
                             // reset the map-mode if a new page is started
                             FPInfo.ExtCanvas.MapMode:=mmLoMetric;
                          end;
                       // set current print page value
                       Graphics.CurrPrintPage:=Graphics.CurrPrintPage+1;
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
                       if FPrintFromDB and FPrintMonitoring then
                          SendPrnMonDDECommand(PageNumber,Pages,0);
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
                    end;
                if FPInfo.AbortPrinting then
                   break;
                FWidth:=FWidth-RectWidth(PageRect)+OverlapR;
                if FWidth>0 then
                   FWidth:=FWidth+OverlapL;
                for Cnt:=0 to Graphics.ItemCount-1 do
                    if Graphics[Cnt].Initialised then
                       if Graphics[Cnt] is TLegendPlaceholder then
                          TLegendPlaceholder(Graphics[Cnt]).LegendPos.X:=TLegendPlaceholder(Graphics[Cnt]).LegendPos.X-
                                                                         RectWidth(ClippingRect)+OverlapL+OverlapR;
                ProjPrintRect.Left:=ProjPrintRect.Left+RectWidth(ProjPrintRect)-ProjOverlapL-ProjOverlapR;
                GraphicSite.XOffset:=GraphicSite.XOffset+ClippingRect.Left+RectWidth(ClippingRect)-OverlapL-OverlapR;
             end;
      if FPInfo.AbortPrinting then
         begin
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
            if FPrintFromDB and FPrintMonitoring then
               // User pressed Cancel button on Printing Progress Dialog
               SendPrnMonDDECommand(PageNumber,Pages,1);
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
            Abort;
         end
      else
         EndDoc;
      GraphicSite.Free;
      FPInfo.DoneCopy;
    end;
  finally
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
//  EnableTaskWindows(Disabled);
    if boolValue then
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
       Dialog.Free;
{++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
    if InvisObjExist then
       InvisObjExist:=DeleteInvisibleObjects(ObjFileName);
{-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
  end;
end;

Function TPrintHandler.DoPrintProject(Scroller: TGraphicSite):Boolean;
var ARect          : TRect;
begin
  if (FWidth>0) and (FHeight>0) then
     begin
        FPInfo.ExtCanvas.Push;
        FPInfo.ExtCanvas.PushClipping;
        try
           ARect:=PageRect;
           OffsetRect(ARect,-FPageSetup.Printer.PrintableArea.Left,
                      -FPageSetup.Printer.PrintableArea.Top);
           FPInfo.SetViewRotation(-FDialog.PrintOptions.PrintRect.Rotation);
      // calculate print-area, offsets and wingis clipping area
      //!?!?! set repaintarea to speed-up printing and to reduce spooler-data
      // this version does not work
  //    with IsoFrame(RotRect(ProjPrintRect,FDialog.PrintOptions.PrintRect.Rotation)) do
  //        FPInfo.RepaintArea.Assign(LimitToLong(Left),LimitToLong(Bottom),
  //        LimitToLong(Right),LimitToLong(Top));
//++ Glukhov PrintOptimization BUILD#152 21.02.01
// The undefined RepaintArea filtered out the objects randomly, let set it at least so
           FPInfo.RepaintArea:= Project.Size;
//-- Glukhov PrintOptimization BUILD#152 21.02.01
           FPInfo.XOffset:=Round(FPInfo.CalculateDispDouble(ProjPrintRect.Left)-ARect.Left);
           FPInfo.YOffset:=Round(FPInfo.CalculateDispDouble(ProjPrintRect.Bottom)-ARect.Bottom);
           FPInfo.ExtCanvas.SetClippingRect(ARect);
           GetClipBox(FPInfo.ExtCanvas.Handle,FPInfo.ClipRect);
           InflateRect(FPInfo.ClipRect,1000,-1000);
           with FPInfo.ClipRect do
              SetRect(FPInfo.ClipLimits,Left,Bottom,Right,Top);
           FPInfo.ExtCanvas.ClipRect:=FPInfo.ClipRect;
           Project^.DrawGraphic(@FPInfo);
           TooltipThread.DrawFixedFloatingTexts(@FPInfo);
        finally
           FPInfo.ExtCanvas.PopClipping;
           FPInfo.ExtCanvas.Pop;
        end;
     end;
  Result:=TRUE;
end;

//++ Glukhov Bug#293(PrintFrame) Build#175 15.02.02
Function TPrintHandler.DoPrintLegend(Graphics: TLegendPlaceholder):Boolean;
var
  ProjRect      : TDRect;
  Legend        : TLegend;
  LegendPos     : TPoint;
  longX         : LongInt;
  longY         : LongInt;
  Points        : array[0..3] of TPoint;
  ARect         : TRect;
  OldMapMode    : TMapMode;
begin
  if ((Graphics.LegendIndex >= 0) and not RectEmpty(Graphics.BoundsRect))
  or (Graphics.LegendName = csLegName4Frame) or (Graphics.LegendName = csOleFrame)
  then begin
    FPInfo.ExtCanvas.Push;
    FPInfo.ExtCanvas.PushClipping;
    try
      FPInfo.SetViewRotation(0);
      LegendPos:=MovedPoint(Graphics.LegendPos,0,-Round(RectHeight(Graphics.BoundsRect)*10));
      LegendPos:=MovedPoint(LegendPos,ClippingRect.Left,ClippingRect.Top);
      LegendPos:=MovedPoint(LegendPos,-FPageSetup.Printer.PrintableArea.Left,-FPageSetup.Printer.PrintableArea.Top);
      FPInfo.XOffset:=-LegendPos.X;
      FPInfo.YOffset:=-LegendPos.Y;
      longX:=FPInfo.CalculateDraw(Round(RectWidth(Graphics.BoundsRect)*10));
      longY:=FPInfo.CalculateDraw(Round(RectHeight(Graphics.BoundsRect)*10));
      ProjRect.Assign( 0, 0, Abs(longX), Abs(longY) );
      if (Graphics.LegendName <> csLegName4Frame)
      and (Graphics.LegendName <> csOleFrame)
      then begin
        Legend:= Project^.Legends[Graphics.LegendIndex];
        Legend.Draw( @FPInfo, Project^.Layers, ProjRect );
      end;

      FPInfo.ConvertToDisp( ProjRect.A, Points[0] );
      FPInfo.ConvertToDisp( ProjRect.B, Points[2] );
      longY:= ProjRect.A.Y;
      ProjRect.A.Y:= ProjRect.B.Y;
      FPInfo.ConvertToDisp( ProjRect.A, Points[1] );
      ProjRect.B.Y:= longY;
      FPInfo.ConvertToDisp( ProjRect.B, Points[3] );

      FPInfo.ExtCanvas.BeginPaint;
      if Graphics.LegendName = csOleFrame then begin
         if Graphics.OleObj.State <> osEmpty then begin
              OldMapMode:=FPInfo.ExtCanvas.MapMode;
              try
                 ARect:=Rect(Points[1].x,Points[1].y,Points[3].x,Points[3].y);
                 LPToDP(FPInfo.ExtCanvas.Handle,ARect,2);
                 FPInfo.ExtCanvas.MapMode:=mmText;
                 Graphics.OleObj.Width:=Abs(ARect.Right-ARect.Left);
                 Graphics.OleObj.Height:=Abs(ARect.Bottom-ARect.Top);
                 Graphics.OleObj.PaintTo(FPInfo.ExtCanvas.Handle,ARect.Left,ARect.Top);
              finally
                 FPInfo.ExtCanvas.MapMode:=OldMapMode;
              end;
         end
      end
      else begin
         FPInfo.ExtCanvas.Pen.Color:=clBlack;
         FPInfo.ExtCanvas.Pen.Style:=ptSolid;
         if (Graphics.BitFlags and cwPrnTempFrmBrdLeft)<>0 then begin
            FPInfo.ExtCanvas.MoveTo( Points[0].X, Points[0].Y );
            FPInfo.ExtCanvas.LineTo( Points[1].X, Points[1].Y );
         end;
         if (Graphics.BitFlags and cwPrnTempFrmBrdTop)<>0 then begin
            FPInfo.ExtCanvas.MoveTo( Points[1].X, Points[1].Y );
            FPInfo.ExtCanvas.LineTo( Points[2].X, Points[2].Y );
         end;
         if (Graphics.BitFlags and cwPrnTempFrmBrdRight)<>0 then begin
            FPInfo.ExtCanvas.MoveTo( Points[2].X, Points[2].Y );
            FPInfo.ExtCanvas.LineTo( Points[3].X, Points[3].Y );
         end;
         if (Graphics.BitFlags and cwPrnTempFrmBrdBottom)<>0 then begin
            FPInfo.ExtCanvas.MoveTo( Points[3].X, Points[3].Y );
            FPInfo.ExtCanvas.LineTo( Points[0].X, Points[0].Y );
         end;
      end;
      FPInfo.ExtCanvas.EndPaint;
    finally
      FPInfo.ExtCanvas.PopClipping;
      FPInfo.ExtCanvas.Pop;
    end;
  end;
  Result:=TRUE;
end;
//-- Glukhov Bug#293(PrintFrame) Build#175 15.02.02

procedure TPrintHandler.DrawPageNum(Scroller: TGraphicSite; ARect: TRect; i,n: Integer);
var
ADistance: Integer;
PosPoint: TPoint;
OldFont: TFont;
dblHeight: Double;
begin
   if FPageSetup.PrintPageNum then
      with Scroller.Canvas do
         begin

            OldFont:=TFont.Create;
            OldFont.Assign(Font);

            Font.Color:=clBlack;
            Font.Size:=8;
            Font.Style:=[];
            Font.Name:='Arial';

            ADistance:=Round(Scroller.InternalToWindow(10));

            PosPoint.x:=ARect.Left;
            PosPoint.y:=ARect.Bottom + ADistance;

            TextOut(PosPoint.X,PosPoint.Y,IntToStr(i) + '/' + IntToStr(n));

            Font.Assign(OldFont);
            OldFont.Free;
         end;
end;

{++ Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
Procedure TPrintHandler.DrawCropMarks(Scroller: TGraphicSite; ARect: TRect);
var dblDistance    : Double;
    intDistance    : Integer;
    intLength      : Integer;
    PosPoint       : TPoint;
    OldPen         : TPen;
begin
   if FPageSetup.PrintCropMarks then
      with Scroller.Canvas do
         begin
            OldPen:=TPen.Create;
            OldPen.Assign(Pen);
            Pen.Style:=psSolid;
            Pen.Color:=clBlack;
            Pen.Mode:=pmNotXor;
            dblDistance:=1.0;   // Distance in mm
            intDistance:=Round(Scroller.InternalToWindow(dblDistance));
            intLength:=intDistance*5;
            // Draw Left-Top Corner
            PosPoint:=ARect.TopLeft;
            PosPoint.X:=PosPoint.X-intDistance;
            MoveTo(PosPoint.X,PosPoint.Y);
            PosPoint.X:=PosPoint.X-intLength;
            LineTo(PosPoint.X,PosPoint.Y);
            PosPoint:=ARect.TopLeft;
            PosPoint.Y:=PosPoint.Y+intDistance;
            MoveTo(PosPoint.X,PosPoint.Y);
            PosPoint.Y:=PosPoint.Y+intLength;
            LineTo(PosPoint.X,PosPoint.Y);
            // Draw Left-Bottom Corner
            PosPoint.Y:=ARect.Bottom;
            PosPoint.Y:=PosPoint.Y-intDistance;
            MoveTo(PosPoint.X,PosPoint.Y);
            PosPoint.Y:=PosPoint.Y-intLength;
            LineTo(PosPoint.X,PosPoint.Y);
            PosPoint.Y:=ARect.Bottom;
            PosPoint.X:=PosPoint.X-intDistance;
            MoveTo(PosPoint.X,PosPoint.Y);
            PosPoint.X:=PosPoint.X-intLength;
            LineTo(PosPoint.X,PosPoint.Y);
            // Draw Right-Bottom Corner
            PosPoint:=ARect.BottomRight;
            PosPoint.X:=PosPoint.X+intDistance;
            MoveTo(PosPoint.X,PosPoint.Y);
            PosPoint.X:=PosPoint.X+intLength;
            LineTo(PosPoint.X,PosPoint.Y);
            PosPoint:=ARect.BottomRight;
            PosPoint.Y:=PosPoint.Y-intDistance;
            MoveTo(PosPoint.X,PosPoint.Y);
            PosPoint.Y:=PosPoint.Y-intLength;
            LineTo(PosPoint.X,PosPoint.Y);
            // Draw Right-Top Corner
            PosPoint.Y:=ARect.Top;
            PosPoint.Y:=PosPoint.Y+intDistance;
            MoveTo(PosPoint.X,PosPoint.Y);
            PosPoint.Y:=PosPoint.Y+intLength;
            LineTo(PosPoint.X,PosPoint.Y);
            PosPoint.Y:=ARect.Top;
            PosPoint.X:=PosPoint.X+intDistance;
            MoveTo(PosPoint.X,PosPoint.Y);
            PosPoint.X:=PosPoint.X+intLength;
            LineTo(PosPoint.X,PosPoint.Y);
            Pen.Assign(OldPen);
            OldPen.Free;
         end;
end;
{-- Moskaliov BUGS##202b,293 BUILD#152 19.01.01}

Procedure TPrintHandler.OnReinvoke;
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
var intPages : Integer;
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
{++ Ivanoff BUG#91}
    APoint: TDPoint;
    ASnapObject: PIndex;
{-- Ivanoff BUG#91}
begin
   Visible:=FALSE;
{++ Moskaliov BUG#90 BUILD#150 05.01.01}
   StatusText:='';
   CoordinateInput:=FALSE;
{-- Moskaliov BUG#90 BUILD#150 05.01.01}
{++ Ivanoff BUG#91}
   // Snap it!  If it requires, of cource...
   If FDialog.Project.SnapState Then Begin
      APoint.Init(Round(TSelectPrintRangeHandler(SubHandler).XPosition),
                  Round(TSelectPrintRangeHandler(SubHandler).YPosition));
      ASnapObject := NIL;
      If (FDialog.Project.SnapPoint(APoint, 0, ot_Any, APoint, ASnapObject)) AND (ASnapObject <> NIL) Then Begin
         TSelectPrintRangeHandler(SubHandler).XPosition := APoint.X;
         TSelectPrintRangeHandler(SubHandler).YPosition := APoint.Y;
         End;
   End;
{-- Ivanoff BUG#91}
   with TSelectPrintRangeHandler(SubHandler) do
      FDialog.PrintOptions.PrintRect:=RotRect(XPosition,YPosition,Width,Height,Rotation);
   FDialog.Reinvoke:=TRUE;
   case FDialog.ShowModal of
      mrOK :    begin
                   if FDialog.PrintOptions.PageSetup.PrintToWMF then
                      DoPrintWMF(FDialog.PrintOptions)
                   else
                      DoPrint(FDialog.PrintOptions);
                   Status:=mhsReturnToPrevious;
                end;
      mrYes:    begin
                   TSelectPrintRangeHandler(SubHandler).PrintDialog:=FDialog;
{++ Moskaliov BUG#90 BUILD#150 05.01.01}
                   CoordinateInput:=TRUE;
                   TSelectPrintRangeHandler(SubHandler).DispFrameStatusText(Self);
{-- Moskaliov BUG#90 BUILD#150 05.01.01}
                   Visible:=TRUE;
                   with TSelectPrintRangeHandler(SubHandler) do begin
                      Project^.ZoomByRect(RotRect(XPosition,YPosition,Width,Height,0),vrFrame,vrBorder);
                   end;
                end;
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
      mrNo :    begin   // User pressed Close button on Print Dialog
                   if FPrintFromDB and FPrintMonitoring then
                      begin
                         with FDialog.PrintOptions.PageSetup do
                            intPages:=VerticalPages*HorizontalPages;
                         SendPrnMonDDECommand(0,intPages,2);
                      end;
                   Status:=mhsReturnToPrevious;
                end;
      mrCancel: begin   // User pressed Cancel button on Print Dialog
                   if FPrintFromDB and FPrintMonitoring then
                      begin
                         with FDialog.PrintOptions.PageSetup do
                            intPages:=VerticalPages*HorizontalPages;
                         SendPrnMonDDECommand(0,intPages,1);
                      end;
                   Status:=mhsReturnToPrevious;
                end;
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
   else
      Status:=mhsReturnToPrevious;
   end;
end;

{++ Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
Function TPrintHandler.PrintFromDB(Mode: Integer; FileName: String;
         Copies: Integer; CollateCopies: Boolean;
         ShowProgress: Boolean; PrintMonitoring: Boolean): Integer;
var Handler  : TSelectPrintRangeHandler;
    intPages : Integer;
begin
   Result:=0;
   FPrintFromDB:=TRUE;
   FMode:=Mode;
   FFileName:=FileName;
   FDialog:=TPrintingDialog.Create(Parent);
   FDialog.Project:=Project;
   FDialog.FormLoad(Parent);
{   FDialog.PrinterCombo.ItemIndex:=FDialog.PrinterCombo.Items.IndexOf(Printer.Name);
   if FDialog.PrinterCombo.ItemIndex < 0 then
      begin
         Result:=18;   // WinGIS internal ERROR
         Exit;
      end;   }
   FDialog.PrintOptions.PageSetup.Copies:=Copies;
   FDialog.PrintOptions.PageSetup.SortCopies:=CollateCopies;
   FShowProgress:=ShowProgress;
   FPrintMonitoring:=PrintMonitoring;

   case Mode of
      0,1 : begin
               FDialog.PrintOptions.PrintToFile:=FALSE;
               FDialog.PrintOptions.PageSetup.PrintToWMF:=FALSE;
            end;
      2,3 : begin
               FDialog.PrintOptions.PrintToFile:=TRUE;
               FDialog.PrintOptions.PageSetup.PrintToWMF:=FALSE;
            end;
      4,5 : begin
               FDialog.PrintOptions.PageSetup.PrintToWMF:=TRUE;
               FDialog.PrintOptions.PrintToFile:=FALSE;
            end;
   else
      begin
         Result:=1;   // value of the Mode parameter is invalid
         Exit;
      end;
   end;   // End of CASE

   if (Mode mod 2) = 0 then
      begin   // Print without Dialog
         FDialog.FormStore(Parent);
         if Mode < 4 then
            DoPrint(FDialog.PrintOptions)
         else
            DoPrintWMF(FDialog.PrintOptions);
      end
   else
      begin   // Print with Dialog showing
         FDialog.Reinvoke:=TRUE;
         case FDialog.ShowModal of
            mrOK :    if FDialog.PrintOptions.PageSetup.PrintToWMF then
                         DoPrintWMF(FDialog.PrintOptions)
                      else
                         DoPrint(FDialog.PrintOptions);
            mrYes:    begin
                         Handler:=TSelectPrintRangeHandler.Create(Self);
                         // calculate position and size
                         Handler.PrintDialog:=FDialog;
                         // set view-rotation to handler
                         Handler.ViewRotation:=FDialog.Project^.PInfo^.ViewRotation;
                         // create menu-handler to select part to print
                         SubHandler:=Handler;
                         Visible:=TRUE;
                         Status:=mhsNotFinished;
                      end;
            mrNo :    if PrintMonitoring then   // User pressed Close button on Print Dialog
                         begin
                            with FDialog.PrintOptions.PageSetup do
                               intPages:=VerticalPages*HorizontalPages;
                            SendPrnMonDDECommand(0,intPages,2);
                         end;
            mrCancel: if PrintMonitoring then   // User pressed Cancel button on Print Dialog
                         begin
                            with FDialog.PrintOptions.PageSetup do
                               intPages:=VerticalPages*HorizontalPages;
                            SendPrnMonDDECommand(0,intPages,1);
                         end;
         end;   // End of CASE FDialog.ShowModal
      end;
end;

Procedure TPrintHandler.SendPrnMonDDECommand(Page: Integer; OfPages: Integer; ResultCode: Integer);
var ResultString  : String;
begin
   {$IFNDEF WMLT}
   if FPrintFromDB and FPrintMonitoring then
      begin
         DDEHandler.SetAnswerMode(TRUE);
         ResultString:=WgBlockStart+'PRNMON'+WgBlockEnd;
         ResultString:=ResultString+WgBlockStart+IntToStr(Page)+WgBlockEnd;
         ResultString:=ResultString+WgBlockStart+IntToStr(OfPages)+WgBlockEnd;
         ResultString:=ResultString+WgBlockStart+IntToStr(ResultCode)+WgBlockEnd;
         DDEHandler.SendString(ResultString);
//       DDEHandler.SendString('[END][]');
         DDEHandler.SetAnswerMode(FALSE);
      end;
      {$ENDIF}
end;
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}

{++ Moskaliov AXWinGIS Printing Methods BUILD#133 02.10.00}
Function TPrintHandler.FuncGetPaperFormat(APrinter: TWPrinter): Integer;
begin
   Result:=-1;

   FDialog:=TPrintingDialog.Create(Parent);
   FDialog.Project:=Project;
   FDialog.FormLoad(Parent);

{++ Moskaliov BUG#326 BUILD#152 13.02.01}
{   if FDialog.PrintOptions.PageSetup.Printer = APrinter then
      Result:=FDialog.PrintOptions.PageSetup.PaperFormatIndex
   else }
{-- Moskaliov BUG#326 BUILD#152 13.02.01}
   Result:=APrinter.IndexOfPaperFormat(FDialog.PrintOptions.PageSetup.WindowsID);

   FDialog.FormDestroy(Parent);
end;

Function  TPrintHandler.SetPrnParamFromDB(APrinter: TWPrinter;
                        APaperFormatIndex: Integer; AOrientation: Integer): Integer;
{++ Moskaliov BUG#326 BUILD#152 13.02.01}
var strFormatName : String;
{-- Moskaliov BUG#326 BUILD#152 13.02.01}
begin
   Result:=0;

   FDialog:=TPrintingDialog.Create(Parent);
   FDialog.Project:=Project;
   FDialog.FormLoad(Parent);

   if WPrinters.Printers.IndexOf(APrinter.Name) > -1 then
      begin
         FDialog.PrintOptions.PageSetup.Printer:=APrinter;
         if (APaperFormatIndex >= 0) and (APaperFormatIndex < APrinter.PaperFormatCount) then
            begin
{++ Moskaliov BUG#326 BUILD#152 13.02.01}
//             FDialog.PrintOptions.PageSetup.PaperFormatIndex:=APaperFormatIndex;
               strFormatName:=APrinter.PaperFormatNames[APaperFormatIndex];
               with FDialog.PrintOptions.PageSetup do
                  PaperFormatIndex:=PaperFormatNames.IndexOf(strFormatName);
{-- Moskaliov BUG#326 BUILD#152 13.02.01}
               case AOrientation of
                  0 : FDialog.PrintOptions.PageSetup.Orientation := wpoPortrait;
                  1 : FDialog.PrintOptions.PageSetup.Orientation := wpoLandscape;
               else
                  Result:=3;
               end;
            end
         else
            Result:=2;
      end
   else
      Result:=1;

   if Result = 0 then
      FDialog.FormStore(Parent);
end;

Function TPrintHandler.SetPgsParamFromDB(AModel: Integer; ARepaginate: Boolean; HorizPgs: Integer;
                       VertPgs: Integer; PageFrames: TGrRect; InnerFrames: TGrRect;
                       OverlapFrames: TGrRect; NumbOrder: Boolean; NumbFrom: Integer): Integer;
begin
   Result:=0;

   FDialog:=TPrintingDialog.Create(Parent);
   FDialog.Project:=Project;
   FDialog.FormLoad(Parent);

   case AModel of
      0 : FDialog.PrintOptions.PageSetup.PrintLayoutModel := plmUniquePage;
      1 : FDialog.PrintOptions.PageSetup.PrintLayoutModel := plmUniformLayout;
      2 : FDialog.PrintOptions.PageSetup.PrintLayoutModel := plmBook;
   else
      Result:=1;   // value of the LayoutModel parameter is invalid
   end;

   if Result = 0 then
      begin
         FDialog.PrintOptions.PageSetup.AutoRepagenate:=ARepaginate;
         if HorizPgs > 0 then
            begin
               FDialog.PrintOptions.PageSetup.HorizontalPages:=HorizPgs;
               if VertPgs > 0 then
                  begin
                     FDialog.PrintOptions.PageSetup.VerticalPages:=VertPgs;
                     if (PageFrames.Left >= 0) and (PageFrames.Bottom >= 0) and
                        (PageFrames.Right >= 0) and (PageFrames.Top >= 0) then
                        begin
                           FDialog.PrintOptions.PageSetup.PageFrames:=PageFrames;
                           if (InnerFrames.Left >= 0) and (InnerFrames.Bottom >= 0) and
                              (InnerFrames.Right >= 0) and (InnerFrames.Top >= 0) then
                              begin
                                 FDialog.PrintOptions.PageSetup.InterPageFrames:=InnerFrames;
                                 if (OverlapFrames.Left >= 0) and (OverlapFrames.Bottom >= 0) and
                                    (OverlapFrames.Right >= 0) and (OverlapFrames.Top >= 0) then
                                    begin
                                       FDialog.PrintOptions.PageSetup.OverlapPageFrames:=OverlapFrames;
                                       FDialog.PrintOptions.PageSetup.PagesOrder:=NumbOrder;
                                       if NumbFrom > 0 then
                                          FDialog.PrintOptions.PageSetup.StartPageNum:=NumbFrom
                                       else
                                          Result:=9;   // value of the NumberingFrom parameter is invalid
                                    end
                                 else
                                    Result:=7;   // value of the Overlap Margins parameters is invalid
                              end
                           else
                              Result:=6;   // value of the Inner Margins parameters is invalid
                        end
                     else
                        Result:=5;   // value of the Margins parameters is invalid
                  end
               else
                  Result:=4;   // value of the VerticalPages parameter is invalid
            end
         else
            Result:=3;   // value of the HorizontalPages parameter is invalid
      end;

   if Result = 0 then
      FDialog.FormStore(Parent);
end;
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 02.10.00}

{++ Moskaliov AXWinGIS Printing Methods BUILD#133 09.10.00}
Function TPrintHandler.SetMapParamFromDB(MapID: Integer; PrintingRegion: Integer;
                       ViewName: String; RegionPoint1: TGrPoint; RegionPoint2: TGrPoint;
                       bFitToPage: Boolean; PrintScale: Double; bKeepScale: Boolean;
                       dFrameWidth: Double; dFrameHeight: Double; FrameCentering: Integer;
                       FrameLeft: Double; FrameTop: Double): Integer;
var ProjGraphics : TProjectPlaceholder;
    ARect        : TRotRect;
    intIndex     : Integer;
    tmpRect      : TGrRect;
begin
   if MapID = 0 then
      Result:=0
   else
      begin
         Result:=1;   // value of the MapID parameter is invalid
         Exit;
      end;

   FDialog:=TPrintingDialog.Create(Parent);
   FDialog.Project:=Project;
   FDialog.FormLoad(Parent);

   // detect the project placeholder
   ProjGraphics:=FDialog.GetProjGraph;
   if ProjGraphics = NIL then
      begin
         Result:=18;   // WinGIS internal ERROR
         Exit;
      end;

   case PrintingRegion of
      0 : begin
             ProjGraphics.PrintRange := prProject;
             FDialog.PrintAllBtn.Checked := TRUE;
          end;
      1 : if ViewName <> '' then
             begin
                intIndex:=FDialog.ViewsCombo.Items.IndexOf(ViewName);
                if intIndex >= 0 then
                   begin
                      ProjGraphics.PrintRange := prSavedView;
                      ProjGraphics.ViewName:=ViewName;
                      FDialog.ViewsCombo.ItemIndex:=intIndex;
                      FDialog.PrintSavedViewBtn.Checked := TRUE;
                   end
                else
                   Result:=3;   // value of the ViewName parameter is invalid
             end
          else
            Result:=3;   // value of the ViewName parameter is invalid
      2 : begin
             ProjGraphics.PrintRange := prCurrentView;
             FDialog.PrintCurrentViewBtn.Checked := TRUE;
          end;
      3 : begin
             RegionPoint1.X:=RegionPoint1.X*100.0;
             RegionPoint1.Y:=RegionPoint1.Y*100.0;
             RegionPoint2.X:=RegionPoint2.X*100.0;
             RegionPoint2.Y:=RegionPoint2.Y*100.0;
             ARect:=RotRect(RegionPoint1,RegionPoint2,-Project.PInfo.ViewRotation);
             if (ARect.Width > 0) and (ARect.Height > 0) then
                begin
                   FDialog.PrintOptions.PrintRect:=ARect;
                   ProjGraphics.PrintRange := prWindow;
                   FDialog.PrintWindowBtn.Checked := TRUE;
                end
             else
                Result:=4;   // value of the RegionBorders parameter is invalid
          end;
   else
      Result:=2;   // value of the PrintingRegion parameter is invalid
   end;

   if Result = 0 then
      if bFitToPage then
      begin
        ProjGraphics.PrintSize:= psFitToPage;           
        FDialog.chkKeepScale.Checked:= False;
        FDialog.chkFitToPage.Checked:= True;
        // all other parameters are ignored
      end
      else begin
        if ProjGraphics.PrintSize = psFitToPage then
        begin
          ProjGraphics.PrintSize:= psWidthAndHeight;          
          FDialog.chkKeepScale.Checked:= False;
          FDialog.chkFitToPage.Checked:= False;
        end
        else begin
          ProjGraphics.PrintSize:= psScale;          
          FDialog.chkFitToPage.Checked:= False;
        end;
        if (PrintScale > 0) and (PrintScale < 1) then
        begin
          ProjGraphics.PrintScale:= PrintScale;
          FDialog.ScaleVal.AsFloat:= PrintScale;
          FDialog.PrintOptions.KeepScale:= bKeepScale;
          if bKeepScale
          then  FDialog.btnSetFrameInPrj.Checked:= True
          else  FDialog.btnSetFrameInDlg.Checked:= True;
                  if (dFrameWidth >= 0) and (dFrameHeight >= 0) then
                     begin
                        FDialog.WidthVal[muMillimeters]:=dFrameWidth;
                        tmpRect:=ProjGraphics.BoundsRect;
                        tmpRect.Right:=tmpRect.Left+dFrameWidth;
                        ProjGraphics.BoundsRect:=tmpRect;
                        FDialog.PrintRangeOrScaleUpdated(FDialog.WidthEdit);
                        FDialog.HeightVal[muMillimeters]:=dFrameHeight;
                        tmpRect.Bottom:=tmpRect.Top-dFrameHeight;
                        ProjGraphics.BoundsRect:=tmpRect;
                        FDialog.PrintRangeOrScaleUpdated(FDialog.HeightEdit);
                        case FrameCentering of
                           0     : begin
                                      ProjGraphics.PrintPosition:=ppCustom;
                                      if FDialog.CenterPageBtn.Checked then
                                         FDialog.CenterPageBtn.Checked:=FALSE;
                                      if (FrameLeft > -dFrameWidth) and (FrameTop > -dFrameHeight) then
                                         begin
                                            FDialog.LeftPosVal[muMillimeters]:=FrameLeft;
                                            tmpRect.Left:=FrameLeft;
                                            tmpRect.Right:=tmpRect.Left+dFrameWidth;
                                            FDialog.TopPosVal[muMillimeters]:=FrameTop;
                                            tmpRect.Top:=RectHeight(FDialog.PrintOptions.PageSetup.PageExtent)-FrameTop;
                                            tmpRect.Bottom:=tmpRect.Top-dFrameHeight;
                                            ProjGraphics.BoundsRect:=tmpRect;
                                            FDialog.LeftOrTopPosUpdated(FDialog.LeftPosEdit);
                                         end
                                      else
                                         Result:=10;   // value of the FramePosition parameters are invalid
                                   end;
                           1,2,3 : begin
                                      ProjGraphics.PrintPosition:=ppCenter;
                                      FDialog.CenterPageBtn.Checked:=TRUE;
                                   end;
                        else
                           Result:=9;   // value of the FrameCentering parameter is invalid
                        end;
                     end
                  else
                     Result:=8;   // value of the FrameSize parameters are invalid
               end
            else
               Result:=6;   // value of the PrintScale parameter is invalid
         end;

   if Result = 0 then
      FDialog.FormStore(Parent);
end;

Function TPrintHandler.SetLegParamFromDB(LegendID: Integer; FrameWidth: Double; FrameHeight: Double;
                                   FrameCentering: Integer; FrameOridgin: Integer;
                                   FrameLeft: Double; FrameTopOrBot: Double): Integer;
begin
   FDialog:=TPrintingDialog.Create(Parent);
   FDialog.Project:=Project;
   FDialog.FormLoad(Parent);

   if (LegendID >= 0) and (LegendID < FDialog.LegendCombo.Items.Count) then
      begin
         Result:=0;
         FDialog.LegendCombo.ItemIndex:=LegendID;
         FDialog.LegendComboClick(FDialog.LegendCombo);
         if (FrameOridgin >= 0) and (FrameOridgin < 2) then
            begin
               FDialog.LegOriginCombo.ItemIndex:=FrameOridgin;
               FDialog.LegOriginComboClick(FDialog.LegOriginCombo);
               if (FrameWidth > 0) and (FrameHeight > 0) then
                  begin
                     FDialog.LegWidthVal[muMillimeters]:=FrameWidth;
                     FDialog.LegSizeOrPositionUpdated(FDialog.LegWidthEdit);
                     FDialog.LegHeightVal[muMillimeters]:=FrameHeight;
                     FDialog.LegSizeOrPositionUpdated(FDialog.LegHeightEdit);
                     if (FrameLeft > -FrameWidth) and (FrameTopOrBot > -FrameHeight) then
                        case FrameCentering of
                           0 : begin
                                  FDialog.LegCustomBtn.Checked:=TRUE;
                                  FDialog.LegLeftPosVal[muMillimeters]:=FrameLeft;
                                  FDialog.LegSizeOrPositionUpdated(FDialog.LegLeftPosEdit);
                                  FDialog.LegTopPosVal[muMillimeters]:=FrameTopOrBot;
                                  FDialog.LegSizeOrPositionUpdated(FDialog.LegTopPosEdit);
                               end;
                           1 : begin
                                  FDialog.LegCenterHorBtn.Checked:=TRUE;
                                  FDialog.LegTopPosVal[muMillimeters]:=FrameTopOrBot;
                                  FDialog.LegSizeOrPositionUpdated(FDialog.LegTopPosEdit);
                               end;
                           2 : begin
                                  FDialog.LegCenterVertBtn.Checked:=TRUE;
                                  FDialog.LegLeftPosVal[muMillimeters]:=FrameLeft;
                                  FDialog.LegSizeOrPositionUpdated(FDialog.LegLeftPosEdit);
                               end;
                           3 : begin
                                  FDialog.LegCenterBtn.Checked:=TRUE;
                                  FDialog.LegPositionBtnClick(FDialog.LegCenterBtn);
                               end;
                        else
                           Result:=3;   // value of the FrameCentering parameter is invalid
                        end   // End of the CASE statement
                     else
                        Result:=5;   // value of the FramePosition parameters are invalid
                  end
               else
                  Result:=2;   // values of the FrameWidth and FrameHeight parameters are invalid
            end
         else
            Result:=4;   // values of the FrameOridgin and FrameHeight parameters are invalid
      end
   else
      Result:=1;   // value of the LegendID parameter is invalid

   if Result = 0 then
      FDialog.FormStore(Parent);
end;

Function TPrintHandler.IsertLegendFromDB(LegendName: String; FrameWidth: Double; FrameHeight: Double;
                                   FrameCentering: Integer; FrameOridgin: Integer;
                                   FrameLeft: Double; FrameTopOrBot: Double): Integer;
var LegGraphic   : TLegendPlaceholder;
    Cnt          : Integer;
    tmpRect      : TGrRect;
begin
   if Project^.Legends.Count > 0 then
      begin
         Result:=1;   // value of the LegendName parameter is invalid
         if LegendName <> '' then
            with Project^.Legends do
               for Cnt:=0 to Count-1 do
                   if Legends[Cnt].Name = LegendName then
                      begin
                         Result:=0;
                         FDialog:=TPrintingDialog.Create(Parent);
                         FDialog.Project:=Project;
                         FDialog.FormLoad(Parent);
                         Break;
                      end;
      end
   else
      Result:=6;   // there are not Legends created in project

   if Result = 0 then
      if (FrameWidth > 0) and (FrameHeight > 0) then
         if (FrameCentering >= 0) and (FrameCentering < 4) then
            if (FrameOridgin >= 0) and (FrameOridgin < 2) then
               if (FrameLeft > -FrameWidth) and (FrameTopOrBot > -FrameHeight) then
                  begin
                     LegGraphic:=TLegendPlaceholder.Create(FDialog.PrintOptions.Graphics);
                     LegGraphic.LegendName:=LegendName;
                     tmpRect.Left:=FrameLeft;
                     tmpRect.Right:=FrameLeft+FrameWidth;
                     if FrameOridgin = Ord(poTop) then
                        begin
                           LegGraphic.PrintOrigin:=poTop;
                           tmpRect.Top:=RectHeight(FDialog.PrintOptions.PageSetup.PageExtent)-FrameTopOrBot;
                           tmpRect.Bottom:=tmpRect.Top-FrameHeight;
                        end
                     else
                        begin
                           LegGraphic.PrintOrigin:=poBottom;
                           tmpRect.Bottom:=FrameTopOrBot;
                           tmpRect.Top:=tmpRect.Bottom+FrameHeight;
                        end;
                     LegGraphic.BoundsRect:=tmpRect;
                     LegGraphic.OriginalBoundsRect:=tmpRect;
                     case FrameCentering of
                        0 : LegGraphic.PrintPosition:=ppCustom;
                        1 : LegGraphic.PrintPosition:=ppCenterHoriz;
                        2 : LegGraphic.PrintPosition:=ppCenterVert;
                        3 : LegGraphic.PrintPosition:=ppCenter;
                     end;   // End of the CASE statement
                     with FDialog.LegendCombo do
                        ItemIndex:=Items.AddObject(LegendName,TObject(LegGraphic));

                     FDialog.FormStore(Parent);
                  end
               else
                  Result:=5   // value of the FramePosition parameters are invalid
            else
               Result:=4   // values of the FrameOridgin and FrameHeight parameters are invalid
         else
            Result:=3   // value of the FrameCentering parameter is invalid
      else
         Result:=2   // values of the FrameWidth and FrameHeight parameters are invalid
   else
      Result:=1;   // value of the LegendName parameter is invalid
end;

Function TPrintHandler.GetPrnParamFromDB(var APrinterName: String; var  APaperFormat: String; var  AOrientation: Integer): Integer;
begin
   Result:=0;

   FDialog:=TPrintingDialog.Create(Parent);
   FDialog.Project:=Project;
   FDialog.FormLoad(Parent);

   with FDialog.PrintOptions.PageSetup do
      begin
         APrinterName:=Printer.Name;
         APaperFormat:=PaperFormats[PaperFormatIndex].Name;
         AOrientation:=Ord(Orientation);
      end;
end;

Function TPrintHandler.GetPgsParamFromDB(var AModel: Integer; var ARepaginate: Integer;
                       var HorizPgs: Integer; var VertPgs: Integer; var APageFrames: TGrRect;
                       var InnerFrames: TGrRect; var OverlapFrames: TGrRect;
                       var NumbOrder: Integer; var NumbFrom: Integer): Integer;
begin
   Result:=0;

   FDialog:=TPrintingDialog.Create(Parent);
   FDialog.Project:=Project;
   FDialog.FormLoad(Parent);

   with FDialog.PrintOptions.PageSetup do
      begin
         AModel:=Ord(PrintLayoutModel);
         if AutoRepagenate then
            ARepaginate:=1
         else
            ARepaginate:=0;
         HorizPgs:=HorizontalPages;
         VertPgs:=VerticalPages;
         APageFrames:=PageFrames;
         InnerFrames:=InterPageFrames;
         OverlapFrames:=OverlapPageFrames;
         if PagesOrder then
            NumbOrder:=0
         else
            NumbOrder:=1;
         NumbFrom:=StartPageNum;
      end;
end;

Function TPrintHandler.GetMapParamFromDB(MapID: Integer; var ARegion: Integer; var AViewName : String;
                      var ARegionBorders: TGrRect; var AFitToPage: Integer; var APrintScale: Double;
                      var AKeepScale: Integer; var AFrameWidth, AFrameHeight: Double;
                      var AFrameCentering: Integer; var AFrameLeft, AFrameTop: Double): Integer;
var ProjGraphics : TProjectPlaceholder;
begin
   if MapID = 0 then
      begin
         FDialog:=TPrintingDialog.Create(Parent);
         FDialog.Project:=Project;
         FDialog.FormLoad(Parent);

         // detect the project placeholder
         ProjGraphics:=FDialog.GetProjGraph;
         if ProjGraphics <> NIL then
            begin
               Result:=0;
               ARegion:=Ord(ProjGraphics.PrintRange);
               if ProjGraphics.PrintRange = prSavedView then
                  AViewName:=FDialog.ViewsCombo.Text
               else
                  AViewName:='';
               with FDialog.PrintOptions.PrintRect do
                  begin
                     ARegionBorders.Left:=X/100.0;
                     ARegionBorders.Bottom:=Y/100.0;
                     ARegionBorders.Right:=(X+Width)/100.0;
                     ARegionBorders.Top:=(Y+Height)/100.0;
                  end;
               if FDialog.chkFitToPage.Checked
               then  AFitToPage:=1
               else  AFitToPage:=0;
               APrintScale:=ProjGraphics.PrintScale;
               if FDialog.PrintOptions.KeepScale then
                  AKeepScale:=1
               else
                  AKeepScale:=0;
               AFrameWidth:=RectWidth(ProjGraphics.BoundsRect);
               AFrameHeight:=RectHeight(ProjGraphics.BoundsRect);
               AFrameCentering:=Ord(ProjGraphics.PrintPosition);
               AFrameLeft:=ProjGraphics.BoundsRect.Left;
               AFrameTop:=RectHeight(FDialog.PrintOptions.PageSetup.PageExtent)-ProjGraphics.BoundsRect.Top;
            end
         else
            Result:=18;   // WinGIS internal ERROR
      end
   else
      Result:=1;   // value of the MapID parameter is invalid
end;

Function TPrintHandler.GetLegParamFromDB(LegendID: Integer; var ALegendName: String;
                                   var AFrameWidth: Double; var AFrameHeight: Double;
                                   var AFrameCentering: Integer; var AFrameOridgin: Integer;
                                   var AFrameLeft: Double; var AFrameTopOrBot: Double): Integer;
var LegGraphic   : TLegendPlaceholder;
begin
   FDialog:=TPrintingDialog.Create(Parent);
   FDialog.Project:=Project;
   FDialog.FormLoad(Parent);

   if (LegendID >= 0) and (LegendID < FDialog.LegendCombo.Items.Count) then
      begin
         Result:=0;
         with FDialog.LegendCombo do
            LegGraphic:=TLegendPlaceholder(Items.Objects[LegendID]);
         ALegendName:=LegGraphic.LegendName;
         AFrameWidth:=RectWidth(LegGraphic.BoundsRect);
         AFrameHeight:=RectHeight(LegGraphic.BoundsRect);
         case Ord(LegGraphic.PrintPosition) of
            Ord(ppCenter)      : AFrameCentering:=3;
            Ord(ppCenterHoriz) : AFrameCentering:=1;
            Ord(ppCenterVert)  : AFrameCentering:=2;
         else
            AFrameCentering:=0;
         end;
         AFrameOridgin:=Ord(LegGraphic.PrintOrigin);
         AFrameLeft:=LegGraphic.BoundsRect.Left;
         if AFrameOridgin = 0 then
            AFrameTopOrBot:=RectHeight(FDialog.PrintOptions.PageSetup.PageExtent)-LegGraphic.BoundsRect.Top
         else
            AFrameTopOrBot:=LegGraphic.BoundsRect.Bottom;
      end
   else
      Result:=1;   // value of the LegendID parameter is invalid
end;
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 09.10.00}

{++ Moskaliov AXWinGIS Printing Methods BUILD#133 25.10.00}
Function TPrintHandler.SetPicParamFromDB(PictureID: Integer; PictureLocation: Integer;
                         FrameSizeInterpret: Integer; FrameWidth: Double; FrameHeight: Double;
                         FrameCentering: Integer; FrameOridgin: Integer;
                         FrameLeft: Double; FrameTopOrBot: Double): Integer;
var intLocation  : Integer;
    boolOneTime  : Boolean;
    dblWidth     : Double;
    dblHeight    : Double;
begin
   FDialog:=TPrintingDialog.Create(Parent);
   FDialog.Project:=Project;
   FDialog.FormLoad(Parent);

   if (PictureID >= 0) and (PictureID < FDialog.PicturesCombo.Items.Count) then
      begin
         Result:=0;
         FDialog.PicturesCombo.ItemIndex:=PictureID;
         FDialog.PicturesComboClick(FDialog.PicturesCombo);
         if FDialog.PrintOptions.PageSetup.PrintLayoutModel = plmBook then
            if (PictureLocation >= 0) and (PictureLocation <= 1111) then
               begin
                  if PictureLocation >= 1000 then
                     begin
                        boolOneTime:=FALSE;
                        intLocation:=PictureLocation-1000;
                     end
                  else
                     begin
                        boolOneTime:=TRUE;
                        intLocation:=PictureLocation;
                     end;
                  FDialog.PicLocOnPagesBtn.Checked:=TRUE;
                  FDialog.PicLocationBtnClick(FDialog.PicLocOnPagesBtn);
                  case intLocation of
                     0   : begin
                              FDialog.PicFirstCheck.Checked:=FALSE;
                              FDialog.PicInterCheck.Checked:=FALSE;
                              FDialog.PicLastCheck.Checked:=FALSE;
                              FDialog.PicLocationBtnClick(FDialog.PicFirstCheck);
                           end;
                     1   : begin
                              FDialog.PicFirstCheck.Checked:=FALSE;
                              FDialog.PicInterCheck.Checked:=FALSE;
                              FDialog.PicLastCheck.Checked:=TRUE;
                              FDialog.PicLocationBtnClick(FDialog.PicLastCheck);
                           end;
                     10  : begin
                              FDialog.PicFirstCheck.Checked:=FALSE;
                              FDialog.PicInterCheck.Checked:=TRUE;
                              FDialog.PicLastCheck.Checked:=FALSE;
                              FDialog.PicLocationBtnClick(FDialog.PicInterCheck);
                           end;
                     11  : begin
                              FDialog.PicFirstCheck.Checked:=FALSE;
                              FDialog.PicInterCheck.Checked:=TRUE;
                              FDialog.PicLastCheck.Checked:=TRUE;
                              FDialog.PicLocationBtnClick(FDialog.PicLastCheck);
                           end;
                     100 : begin
                              FDialog.PicFirstCheck.Checked:=TRUE;
                              FDialog.PicInterCheck.Checked:=FALSE;
                              FDialog.PicLastCheck.Checked:=FALSE;
                              FDialog.PicLocationBtnClick(FDialog.PicFirstCheck);
                           end;
                     101 : begin
                              FDialog.PicFirstCheck.Checked:=TRUE;
                              FDialog.PicInterCheck.Checked:=FALSE;
                              FDialog.PicLastCheck.Checked:=TRUE;
                              FDialog.PicLocationBtnClick(FDialog.PicFirstCheck);
                           end;
                     111 : begin
                              FDialog.PicFirstCheck.Checked:=TRUE;
                              FDialog.PicInterCheck.Checked:=TRUE;
                              FDialog.PicLastCheck.Checked:=TRUE;
                              FDialog.PicLocationBtnClick(FDialog.PicLastCheck);
                           end;
                  else
                     Result:=2;   // values of the PictureLocation parameter is invalid
                  end;   // End of CASE statement
                  if (Result = 0) and boolOneTime then
                     begin
                        FDialog.PicLocOneTimeBtn.Checked:=TRUE;
                        FDialog.PicLocationBtnClick(FDialog.PicLocOneTimeBtn);
                     end;

               end
            else
               Result:=2;   // values of the PictureLocation parameter is invalid
      end
   else
      Result:=1;   // value of the PictureID parameter is invalid

   if Result = 0 then
      if (FrameSizeInterpret >= 0) and (FrameSizeInterpret < 4) then
         if (FrameOridgin >= 0) and (FrameOridgin < 2) then
            begin
               FDialog.PicOriginCombo.ItemIndex:=FrameOridgin;
               FDialog.PicOriginComboClick(FDialog.PicOriginCombo);
               if (FrameWidth > 0) and (FrameHeight > 0) then
                  begin
                     case FrameSizeInterpret of
                        1 : begin
                               FDialog.KeepAspectCheck.Checked:=FALSE;
                               FDialog.PicKeepAspectCheckClick(FDialog.KeepAspectCheck);
                               FDialog.PicWidthVal[muMillimeters]:=FrameWidth;
                               FDialog.PicSizeOrPositionUpdated(FDialog.PicWidthEdit);
                               FDialog.PicHeightVal[muMillimeters]:=FrameHeight;
                               FDialog.PicSizeOrPositionUpdated(FDialog.PicHeightEdit);
                               FDialog.KeepAspectCheck.Checked:=TRUE;
                               FDialog.PicKeepAspectCheckClick(FDialog.KeepAspectCheck);
                               dblWidth:=FDialog.PicWidthVal[muMillimeters];
                               dblHeight:=FDialog.PicHeightVal[muMillimeters];
                            end;
                        2 : begin
                               FDialog.KeepAspectCheck.Checked:=FALSE;
                               FDialog.PicKeepAspectCheckClick(FDialog.KeepAspectCheck);
                               FDialog.PicWScaleVal[muPercent]:=FrameWidth;
                               FDialog.PicSizeOrPositionUpdated(FDialog.PicWScaleEdit);
                               FDialog.PicHScaleVal[muPercent]:=FrameHeight;
                               FDialog.PicSizeOrPositionUpdated(FDialog.PicHScaleEdit);
                               dblWidth:=FDialog.PicWidthVal[muMillimeters];
                               dblHeight:=FDialog.PicHeightVal[muMillimeters];
                            end;
                        3 : begin
                               FDialog.KeepAspectCheck.Checked:=FALSE;
                               FDialog.PicKeepAspectCheckClick(FDialog.KeepAspectCheck);
                               FDialog.PicWScaleVal[muPercent]:=FrameWidth;
                               FDialog.PicSizeOrPositionUpdated(FDialog.PicWScaleEdit);
                               FDialog.PicHScaleVal[muPercent]:=FrameHeight;
                               FDialog.PicSizeOrPositionUpdated(FDialog.PicHScaleEdit);
                               FDialog.KeepAspectCheck.Checked:=TRUE;
                               FDialog.PicKeepAspectCheckClick(FDialog.KeepAspectCheck);
                               dblWidth:=FDialog.PicWidthVal[muMillimeters];
                               dblHeight:=FDialog.PicHeightVal[muMillimeters];
                            end;
                     else
                        begin
                           dblWidth:=FrameWidth;
                           dblHeight:=FrameHeight;
                           FDialog.KeepAspectCheck.Checked:=FALSE;
                           FDialog.PicKeepAspectCheckClick(FDialog.KeepAspectCheck);
                           FDialog.PicWidthVal[muMillimeters]:=FrameWidth;
                           FDialog.PicSizeOrPositionUpdated(FDialog.PicWidthEdit);
                           FDialog.PicHeightVal[muMillimeters]:=FrameHeight;
                           FDialog.PicSizeOrPositionUpdated(FDialog.PicHeightEdit);
                        end;
                     end;   // End of CASE statement
                     if (FrameLeft > -dblWidth) and (FrameTopOrBot > -dblHeight) then
                        case FrameCentering of
                           0 : begin
                                  FDialog.PicCustomBtn.Checked:=TRUE;
                                  FDialog.PicLeftPosVal[muMillimeters]:=FrameLeft;
                                  FDialog.PicSizeOrPositionUpdated(FDialog.PicLeftPosEdit);
                                  FDialog.PicTopPosVal[muMillimeters]:=FrameTopOrBot;
                                  FDialog.PicSizeOrPositionUpdated(FDialog.PicTopPosEdit);
                               end;
                           1 : begin
                                  FDialog.PicCenterHorBtn.Checked:=TRUE;
                                  FDialog.PicTopPosVal[muMillimeters]:=FrameTopOrBot;
                                  FDialog.PicSizeOrPositionUpdated(FDialog.PicTopPosEdit);
                               end;
                           2 : begin
                                  FDialog.PicCenterVertBtn.Checked:=TRUE;
                                  FDialog.PicLeftPosVal[muMillimeters]:=FrameLeft;
                                  FDialog.PicSizeOrPositionUpdated(FDialog.PicLeftPosEdit);
                               end;
                           3 : begin
                                  FDialog.PicCenterBtn.Checked:=TRUE;
                                  FDialog.PicPositionBtnClick(FDialog.PicCenterBtn);
                               end;
                        else
                           Result:=5;   // value of the FrameCentering parameter is invalid
                        end   // End of the CASE statement
                     else
                        Result:=7;   // value of the FramePosition parameters are invalid
                  end
               else
                  Result:=4;   // values of the FrameWidth and FrameHeight parameters are invalid
            end
         else
            Result:=6   // values of the FrameOridgin parameter is invalid
      else
         Result:=3;   // values of the FrameSizeInterpretation parameter is invalid
         
   if Result = 0 then
      FDialog.FormStore(Parent);
end;

Function TPrintHandler.IsertPictureFromDB(PictureFile: String; PictureLocation: Integer;
                         FrameSizeInterpret: Integer; FrameWidth: Double; FrameHeight: Double;
                         FrameCentering: Integer; FrameOridgin: Integer;
                         FrameLeft: Double; FrameTopOrBot: Double): Integer;
var PicGraphic       : TWindowsGraphic;
    intLocation      : Integer;
    boolInPercent    : Boolean;
    dblWidth         : Double;
    dblHeight        : Double;
    dblScale         : Double;
    FileName         : String;
    FilePath         : String;
    tmpRect          : TGrRect;
    tmpLocation      : TPrintLocation;
    bOnFirstPage     : Boolean;
    bOnInternalPages : Boolean;
    bOnLastPage      : Boolean;
begin
   if PictureFile <> '' then
      if FileExists(PictureFile) then
         if (FrameSizeInterpret >= 0) and (FrameSizeInterpret < 4) then
            if (FrameWidth > 0) and (FrameHeight > 0) then
               if (FrameCentering >= 0) and (FrameCentering < 4) then
                  if (FrameOridgin >= 0) and (FrameOridgin < 2) then
                     begin
                        Result:=0;
                        tmpLocation:=plOneTime;
                        bOnFirstPage:=TRUE;
                        bOnInternalPages:=TRUE;
                        bOnLastPage:=TRUE;

                        FDialog:=TPrintingDialog.Create(Parent);
                        FDialog.Project:=Project;
                        FDialog.FormLoad(Parent);

                        if FDialog.PrintOptions.PageSetup.PrintLayoutModel = plmBook then
                           if (PictureLocation >= 0) and (PictureLocation <= 1111) then
                              begin
                                 if PictureLocation >= 1000 then
                                    begin
                                       tmpLocation:=plOnPages;
                                       intLocation:=PictureLocation-1000;
                                    end
                                 else
                                    intLocation:=PictureLocation;
                                 case intLocation of
                                    0   : begin
                                             bOnFirstPage:=FALSE;
                                             bOnInternalPages:=FALSE;
                                             bOnLastPage:=FALSE;
                                          end;
                                    1   : begin
                                             bOnFirstPage:=FALSE;
                                             bOnInternalPages:=FALSE;
                                          end;
                                    10  : begin
                                             bOnFirstPage:=FALSE;
                                             bOnLastPage:=FALSE;
                                          end;
                                    11  : bOnFirstPage:=FALSE;
                                    100 : begin
                                             bOnInternalPages:=FALSE;
                                             bOnLastPage:=FALSE;
                                          end;
                                    101 : bOnInternalPages:=FALSE;
                                    111 : begin
                                          end;
                                 else
                                    Result:=2;   // values of the PictureLocation parameter is invalid
                                 end;   // End of CASE statement
                              end
                           else
                              Result:=2;   // values of the PictureLocation parameter is invalid

                        if Result = 0 then
                           begin
                              // Create Picture object
                              PicGraphic:=TWindowsGraphic.Create(FDialog.PrintOptions.Graphics);

                              PicGraphic.KeepAspect:=((FrameSizeInterpret mod 2) > 0);
                              boolInPercent:=(FrameSizeInterpret > 1);

                              // preliminary Frame parameters definition
                              if boolInPercent then
                                 begin
                                    dblWidth:=100;
                                    dblHeight:=(dblWidth*FrameHeight)/FrameWidth;
                                 end
                              else
                                 begin
                                    dblWidth:=FrameWidth;
                                    dblHeight:=FrameHeight;
                                 end;
                              tmpRect.Left:=FrameLeft;
                              tmpRect.Right:=FrameLeft+dblWidth;
                              if FrameOridgin = Ord(poTop) then
                                 begin
                                    PicGraphic.PrintOrigin:=poTop;
                                    tmpRect.Top:=RectHeight(FDialog.PrintOptions.PageSetup.PageExtent)-FrameTopOrBot;
                                    tmpRect.Bottom:=tmpRect.Top-dblHeight;
                                 end
                              else
                                 begin
                                    PicGraphic.PrintOrigin:=poBottom;
                                    tmpRect.Bottom:=FrameTopOrBot;
                                    tmpRect.Top:=tmpRect.Bottom+dblHeight;
                                 end;
                              PicGraphic.BoundsRect:=tmpRect;
                              PicGraphic.OriginalBoundsRect:=tmpRect;

                              // Picture loading
                              FileName:=ExtractFileName(PictureFile);
                              FilePath:=ExtractFilePath(PictureFile);
                              PicGraphic.CreateFromFile(FileName,FilePath,TRUE);

                              if FDialog.PrintOptions.PageSetup.PrintLayoutModel = plmBook then
                                 begin
                                    PicGraphic.PrintLocation:=tmpLocation;
                                    PicGraphic.OnFirstPage:=bOnFirstPage;
                                    PicGraphic.OnInternalPages:=bOnInternalPages;
                                    PicGraphic.OnLastPage:=bOnLastPage;
                                 end;

                              // final Frame parameters definition
                              if boolInPercent then
                                 if PicGraphic.KeepAspect then
                                    begin
                                       dblScale:=(FrameWidth+FrameHeight)*0.5;
                                       dblWidth:=(RectWidth(PicGraphic.OriginalBoundsRect)*dblScale)/100.0;
                                       dblHeight:=(RectHeight(PicGraphic.OriginalBoundsRect)*dblScale)/100.0;
                                    end
                                 else
                                    begin
                                       dblWidth:=(RectWidth(PicGraphic.OriginalBoundsRect)*FrameWidth)/100.0;
                                       dblHeight:=(RectHeight(PicGraphic.OriginalBoundsRect)*FrameHeight)/100.0;
                                    end
                              else   // In mm
                                 if PicGraphic.KeepAspect then
                                    begin
                                       dblWidth:=FrameWidth/RectWidth(PicGraphic.OriginalBoundsRect);
                                       dblHeight:=FrameHeight/RectHeight(PicGraphic.OriginalBoundsRect);
                                       dblScale:=(dblWidth+dblHeight)*0.5;
                                       dblWidth:=RectWidth(PicGraphic.OriginalBoundsRect)*dblScale;
                                       dblHeight:=RectHeight(PicGraphic.OriginalBoundsRect)*dblScale;
                                    end
                                 else
                                    begin
                                       dblWidth:=FrameWidth;
                                       dblHeight:=FrameHeight;
                                    end;
                              tmpRect.Left:=FrameLeft;
                              tmpRect.Right:=FrameLeft+dblWidth;
                              if PicGraphic.PrintOrigin = poTop then
                                 begin
                                    tmpRect.Top:=RectHeight(FDialog.PrintOptions.PageSetup.PageExtent)-FrameTopOrBot;
                                    tmpRect.Bottom:=tmpRect.Top-dblHeight;
                                 end
                              else
                                 begin
                                    tmpRect.Bottom:=FrameTopOrBot;
                                    tmpRect.Top:=tmpRect.Bottom+dblHeight;
                                 end;
                              PicGraphic.BoundsRect:=tmpRect;

                              case FrameCentering of
                                 0 : PicGraphic.PrintPosition:=ppCustom;
                                 1 : PicGraphic.PrintPosition:=ppCenterHoriz;
                                 2 : PicGraphic.PrintPosition:=ppCenterVert;
                                 3 : PicGraphic.PrintPosition:=ppCenter;
                              end;   // End of the CASE statement

                              with FDialog.PicturesCombo do
                                 ItemIndex:=Items.AddObject(FileName,TObject(PicGraphic));

                              FDialog.FormStore(Parent);
                           end;
                     end
                  else
                     Result:=6   // values of the FrameOridgin and FrameHeight parameters are invalid
               else
                  Result:=5   // value of the FrameCentering parameter is invalid
            else
               Result:=4   // values of the FrameWidth and FrameHeight parameters are invalid
         else
            Result:=3   // values of the FrameSizeInterpretation parameter is invalid
      else
         Result:=1   // values of the PictureFile parameter is invalid
   else
      Result:=1;   // values of the PictureFile parameter is invalid
end;

Function TPrintHandler.GetPicParamFromDB(PictureID: Integer; var APictureFile: String;
                         var APictureLocation: Integer; var AKeepAspect: Boolean;
                         var AFrameWidth: Double; var AFrameHeight: Double;
                         var AFrameCentering: Integer; var AFrameOridgin: Integer;
                         var AFrameLeft: Double; var AFrameTopOrBot: Double): Integer;
var PicGraphic   : TWindowsGraphic;
begin
   FDialog:=TPrintingDialog.Create(Parent);
   FDialog.Project:=Project;
   FDialog.FormLoad(Parent);

   if (PictureID >= 0) and (PictureID < FDialog.PicturesCombo.Items.Count) then
      begin
         Result:=0;
         with FDialog.PicturesCombo do
            PicGraphic:=TWindowsGraphic(Items.Objects[PictureID]);
         APictureFile:=PicGraphic.RelativePath+PicGraphic.FileName;
         if PicGraphic.PrintLocation = plOnPages then
            APictureLocation:=1000
         else
            APictureLocation:=0;
         if PicGraphic.OnFirstPage then
            APictureLocation:=APictureLocation+100;
         if PicGraphic.OnInternalPages then
            APictureLocation:=APictureLocation+10;
         if PicGraphic.OnLastPage then
            APictureLocation:=APictureLocation+1;
         AKeepAspect:=PicGraphic.KeepAspect;
         AFrameWidth:=RectWidth(PicGraphic.BoundsRect);
         AFrameHeight:=RectHeight(PicGraphic.BoundsRect);
         case Ord(PicGraphic.PrintPosition) of
            Ord(ppCenter)      : AFrameCentering:=3;
            Ord(ppCenterHoriz) : AFrameCentering:=1;
            Ord(ppCenterVert)  : AFrameCentering:=2;
         else
            AFrameCentering:=0;
         end;
         AFrameOridgin:=Ord(PicGraphic.PrintOrigin);
         AFrameLeft:=PicGraphic.BoundsRect.Left;
         if AFrameOridgin = 0 then
            AFrameTopOrBot:=RectHeight(FDialog.PrintOptions.PageSetup.PageExtent)-PicGraphic.BoundsRect.Top
         else
            AFrameTopOrBot:=PicGraphic.BoundsRect.Bottom;
      end
   else
      Result:=1;   // value of the PictureID parameter is invalid
end;

Function TPrintHandler.SetSigParamFromDB(SignatureID: Integer; Signature: String;
                         FontName: String; Italic: Boolean; Bold: Boolean;
                         Underlined: Boolean; Transparent: Boolean;
                         FontSize: Integer; FontColor: Integer; FontBackColor: Integer;
                         TextAlignment: Integer; OnPagesLocation: Boolean;
                         OnFirstPage: Boolean; OnInternalPages: Boolean; OnLastPage: Boolean;
                         FrameCentering: Integer; FrameOridgin: Integer;
                         FrameLeft: Double; FrameTopOrBot: Double): Integer;
var SigGraphic   : TPrintSignature;
    Fonts        : PFonts;
    FontDes      : PFontDes;
    intValue     : Integer;
begin
   if SignatureID >= 0 then
      if Signature <> '' then
         if FontName <> '' then
            begin
               Fonts:=Project^.PInfo^.Fonts;
               intValue:=Fonts^.IDFromName(FontName);
               FontDes:=Fonts^.GetFont(intValue);
               if FontDes <> NIL then
                  if FontSize > 0 then
                     if (FontColor >= 0) and (FontBackColor >= 0) then
                        if (TextAlignment >= 0) and (TextAlignment < 3) then
                           if (FrameCentering >= 0) and (FrameCentering < 4) then
                              if (FrameOridgin >= 0) and (FrameOridgin < 2) then
                                 begin
                                    Result:=0;
                                    FDialog:=TPrintingDialog.Create(Parent);
                                    FDialog.Project:=Project;
                                    FDialog.FormLoad(Parent);
                                 end
                              else
                                 Result:=11 // value of the FrameOridgin parameter is invalid
                           else
                              Result:=10  // value of the FrameCentering parameter is invalid
                        else
                           Result:=7   // value of the TextAlignment parameter is invalid
                     else
                        Result:=6   // values of the FontColor parameters are invalid
                  else
                     Result:=5   // value of the FontSize parameter is invalid
               else
                  Result:=3;   // value of the FontName parameter is invalid
            end
         else
            Result:=3   // value of the FontName parameter is invalid
      else
         Result:=2   // value of the Signature parameter is invalid
   else
      Result:=1;   // value of the SignatureID parameter is invalid

   if Result = 0 then
      if SignatureID < FDialog.SignaturesCombo.Items.Count then
         begin
            with FDialog.SignaturesCombo do
               SigGraphic:=TPrintSignature(Items.Objects[SignatureID]);
            FDialog.SignaturesCombo.ItemIndex:=SignatureID;
            FDialog.SignaturesComboClick(FDialog.SignaturesCombo);
            FDialog.SigOriginCombo.ItemIndex:=FrameOridgin;
            FDialog.SigOriginComboClick(FDialog.SigOriginCombo);
            case FrameCentering of
               0 : begin
                      FDialog.SigCustomBtn.Checked:=TRUE;
                      FDialog.SigLeftPosVal[muMillimeters]:=FrameLeft;
                      FDialog.SigSizeOrPositionUpdated(FDialog.SigLeftPosEdit);
                      FDialog.SigTopPosVal[muMillimeters]:=FrameTopOrBot;
                      FDialog.SigSizeOrPositionUpdated(FDialog.SigTopPosEdit);
                   end;
               1 : begin
                      FDialog.SigCenterHorBtn.Checked:=TRUE;
                      FDialog.SigTopPosVal[muMillimeters]:=FrameTopOrBot;
                      FDialog.SigSizeOrPositionUpdated(FDialog.SigTopPosEdit);
                   end;
               2 : begin
                      FDialog.SigCenterVertBtn.Checked:=TRUE;
                      FDialog.SigLeftPosVal[muMillimeters]:=FrameLeft;
                      FDialog.SigSizeOrPositionUpdated(FDialog.SigLeftPosEdit);
                   end;
               3 : begin
                      FDialog.SigCenterBtn.Checked:=TRUE;
                      FDialog.SigPositionBtnClick(FDialog.SigCenterBtn);
                   end;
            end;   // End of the CASE statement

            SigGraphic.Signature:=Signature;
            SigGraphic.TextStyle.FontName:=FontDes^.Name^;
            SigGraphic.TextStyle.Alignment:=TextAlignment;
            if Italic then
               SigGraphic.TextStyle.Italic:=1
            else
               SigGraphic.TextStyle.Italic:=0;
            if Bold then
               SigGraphic.TextStyle.Bold:=1
            else
               SigGraphic.TextStyle.Bold:=0;
            if Underlined then
               SigGraphic.TextStyle.Underlined:=1
            else
               SigGraphic.TextStyle.Underlined:=0;
            if Transparent then
               SigGraphic.TextStyle.Transparent:=1
            else
               SigGraphic.TextStyle.Transparent:=0;
            SigGraphic.TextStyle.Height:=FontSize;
            SigGraphic.ForeColor:=TColor(FontColor);
            SigGraphic.BackColor:=TColor(FontBackColor);
            FDialog.SigSizeOrPositionUpdated(FDialog.FontDlgBtn);

            if FDialog.PrintOptions.PageSetup.PrintLayoutModel = plmBook then
               begin
                  FDialog.SigLocOnPagesBtn.Checked:=TRUE;
                  FDialog.SigLocationBtnClick(FDialog.SigLocOnPagesBtn);
                  FDialog.SigFirstCheck.Checked:=OnFirstPage;
                  FDialog.SigInterCheck.Checked:=OnInternalPages;
                  FDialog.SigLastCheck.Checked:=OnLastPage;
                  FDialog.SigLocationBtnClick(FDialog.SigFirstCheck);
                  if not OnPagesLocation then
                     begin
                        FDialog.SigLocOneTimeBtn.Checked:=TRUE;
                        FDialog.SigLocationBtnClick(FDialog.SigLocOneTimeBtn);
                     end;
               end;
            FDialog.FormStore(Parent);
         end
      else
         Result:=1;   // value of the SignatureID parameter is invalid
end;

Function TPrintHandler.InsertSignatureFromDB(Signature: String; FontName: String; Italic: Boolean;
                         Bold: Boolean; Underlined: Boolean; Transparent: Boolean;
                         FontSize: Integer; FontColor: Integer; FontBackColor: Integer;
                         TextAlignment: Integer; OnPagesLocation: Boolean;
                         OnFirstPage: Boolean; OnInternalPages: Boolean; OnLastPage: Boolean;
                         FrameCentering: Integer; FrameOridgin: Integer;
                         FrameLeft: Double; FrameTopOrBot: Double): Integer;
var SigGraphic   : TPrintSignature;
    Fonts        : PFonts;
    FontDes      : PFontDes;
    intValue     : Integer;
    dblWidth     : Double;
    dblHeight    : Double;
    tmpRect      : TGrRect;
begin
   if Signature <> '' then
      if FontName <> '' then
         begin
            Fonts:=Project^.PInfo^.Fonts;
            intValue:=Fonts^.IDFromName(FontName);
            FontDes:=Fonts^.GetFont(intValue);
            if FontDes <> NIL then
               if FontSize > 0 then
                  if (FontColor >= 0) and (FontBackColor >= 0) then
                     if (TextAlignment >= 0) and (TextAlignment < 3) then
                        if (FrameCentering >= 0) and (FrameCentering < 4) then
                           if (FrameOridgin >= 0) and (FrameOridgin < 2) then
                              begin
                                 Result:=0;
                                 FDialog:=TPrintingDialog.Create(Parent);
                                 FDialog.Project:=Project;
                                 FDialog.FormLoad(Parent);

                                 // Create Signature object
                                 SigGraphic:=TPrintSignature.Create(FDialog.PrintOptions.Graphics);

                                 SigGraphic.Signature:=Signature;
                                 SigGraphic.TextStyle.FontName:=FontName;
                                 if Italic then
                                    SigGraphic.TextStyle.Italic:=1
                                 else
                                    SigGraphic.TextStyle.Italic:=0;
                                 if Bold then
                                    SigGraphic.TextStyle.Bold:=1
                                 else
                                    SigGraphic.TextStyle.Bold:=0;
                                 if Underlined then
                                    SigGraphic.TextStyle.Underlined:=1
                                 else
                                    SigGraphic.TextStyle.Underlined:=0;
                                 if Transparent then
                                    SigGraphic.TextStyle.Transparent:=1
                                 else
                                    SigGraphic.TextStyle.Transparent:=0;
                                 SigGraphic.TextStyle.Height:=FontSize;
                                 SigGraphic.TextStyle.Alignment:=TextAlignment;
                                 SigGraphic.ForeColor:=FontColor;
                                 SigGraphic.BackColor:=FontBackColor;
                                 if FDialog.PrintOptions.PageSetup.PrintLayoutModel = plmBook then
                                    begin
                                       if OnPagesLocation then
                                          SigGraphic.PrintLocation:=plOnPages
                                       else
                                          SigGraphic.PrintLocation:=plOneTime;
                                       SigGraphic.OnFirstPage:=OnFirstPage;
                                       SigGraphic.OnInternalPages:=OnInternalPages;
                                       SigGraphic.OnLastPage:=OnLastPage;
                                    end;
                                 case FrameCentering of
                                    0 : SigGraphic.PrintPosition:=ppCustom;
                                    1 : SigGraphic.PrintPosition:=ppCenterHoriz;
                                    2 : SigGraphic.PrintPosition:=ppCenterVert;
                                    3 : SigGraphic.PrintPosition:=ppCenter;
                                 end;
                                 // preliminary signature Width and Height parameters definition
                                 dblHeight:=(FontSize*25.4)/72;   // Font height in mm;
                                 dblWidth:=dblHeight*Length(SigGraphic.SignatureValue)*0.75;
                                 // preliminary Frame parameters definition
                                 case TextAlignment of
                                    0 : begin
                                           tmpRect.Left:=FrameLeft;
                                           tmpRect.Right:=tmpRect.Left+dblWidth;
                                        end;
                                    1 : begin
                                           tmpRect.Left:=FrameLeft-dblWidth*0.5;
                                           tmpRect.Right:=tmpRect.Left+dblWidth;
                                        end;
                                    2 : begin
                                           tmpRect.Right:=FrameLeft;
                                           tmpRect.Left:=tmpRect.Right-dblWidth;
                                        end;
                                 end;
                                 case FrameOridgin of
                                    0 : begin
                                           SigGraphic.PrintOrigin:=poTop;
                                           tmpRect.Top:=RectHeight(FDialog.PrintOptions.PageSetup.PageExtent)-FrameTopOrBot;
                                           tmpRect.Bottom:=tmpRect.Top-dblHeight;
                                        end;
                                    1 : begin
                                           SigGraphic.PrintOrigin:=poBottom;
                                           tmpRect.Bottom:=FrameTopOrBot;
                                           tmpRect.Top:=tmpRect.Bottom+dblHeight;
                                        end;
                                 end;
                                 SigGraphic.BoundsRect:=tmpRect;
                                 SigGraphic.OriginalBoundsRect:=tmpRect;

                                 with FDialog.SignaturesCombo do
                                    ItemIndex:=Items.AddObject(Signature,TObject(SigGraphic));
                                 // final Frame parameters definition
                                 FDialog.SigSizeOrPositionUpdated(FDialog.FontDlgBtn);

                                 FDialog.FormStore(Parent);
                              end
                           else
                              Result:=10 // value of the FrameOridgin parameter is invalid
                        else
                           Result:=9  // value of the FrameCentering parameter is invalid
                     else
                        Result:=6   // value of the TextAlignment parameter is invalid
                  else
                     Result:=5   // values of the FontColor parameters are invalid
               else
                  Result:=4   // value of the FontSize parameter is invalid
            else
               Result:=2;   // value of the FontName parameter is invalid
         end
      else
         Result:=2   // value of the FontName parameter is invalid
   else
      Result:=1   // value of the Signature parameter is invalid
end;

Function TPrintHandler.GetSigParamFromDB(SignatureID: Integer; var ASignature: String;
                         var AFontName: String; var AItalic: Boolean;
                         var ABold: Boolean; var AUnderlined: Boolean; var ATransparent: Boolean;
                         var AFontSize: Integer; var AFontColor: Integer; var AFontBackColor: Integer;
                         var ATextAlignment: Integer; var AOnPagesLocation: Boolean;
                         var AOnFirstPage: Boolean; var AOnInternalPages: Boolean; var AOnLastPage: Boolean;
                         var AFrameCentering: Integer; var AFrameOridgin: Integer;
                         var AFrameLeft: Double; var AFrameTopOrBot: Double): Integer;
var SigGraphic   : TPrintSignature;
begin
   FDialog:=TPrintingDialog.Create(Parent);
   FDialog.Project:=Project;
   FDialog.FormLoad(Parent);

   if (SignatureID >= 0) and (SignatureID < FDialog.SignaturesCombo.Items.Count) then
      begin
         Result:=0;
         with FDialog.SignaturesCombo do
            SigGraphic:=TPrintSignature(Items.Objects[SignatureID]);

         ASignature:=SigGraphic.Signature;

         AFontName:=SigGraphic.TextStyle.FontName;
         AItalic:=(SigGraphic.TextStyle.Italic = 1);
         ABold:=(SigGraphic.TextStyle.Bold = 1);
         AUnderlined:=(SigGraphic.TextStyle.Underlined = 1);
         ATransparent:=(SigGraphic.TextStyle.Transparent = 1);
         AFontSize:=Round(SigGraphic.TextStyle.Height);
         ATextAlignment:=SigGraphic.TextStyle.Alignment;
         AFontColor:=SigGraphic.ForeColor;
         AFontBackColor:=SigGraphic.BackColor;

         AOnPagesLocation:=(SigGraphic.PrintLocation = plOnPages);
         AOnFirstPage:=SigGraphic.OnFirstPage;
         AOnInternalPages:=SigGraphic.OnInternalPages;
         AOnLastPage:=SigGraphic.OnLastPage;

         case Ord(SigGraphic.PrintPosition) of
            Ord(ppCenter)      : AFrameCentering:=3;
            Ord(ppCenterHoriz) : AFrameCentering:=1;
            Ord(ppCenterVert)  : AFrameCentering:=2;
         else
            AFrameCentering:=0;
         end;
         case ATextAlignment of
            1 : AFrameLeft:=(SigGraphic.BoundsRect.Left+SigGraphic.BoundsRect.Right)*0.5;
            2 : AFrameLeft:=SigGraphic.BoundsRect.Right;
         else
            AFrameLeft:=SigGraphic.BoundsRect.Left;
         end;
         AFrameOridgin:=Ord(SigGraphic.PrintOrigin);
         if AFrameOridgin = 0 then
            AFrameTopOrBot:=RectHeight(FDialog.PrintOptions.PageSetup.PageExtent)-SigGraphic.BoundsRect.Top
         else
            AFrameTopOrBot:=SigGraphic.BoundsRect.Bottom;
      end
   else
      Result:=1;   // value of the SignatureID parameter is invalid
end;

Function TPrintHandler.StoreTemplateFromDB(FileName: String; Options: Integer): Integer;
begin
   if FileName <> '' then
      case Options of
         0 : if FileExists(FileName) then
                Result:=3   // mentioned file already exist and value of the parameter Options is equal 0
             else
                Result:=0;
         1 : Result:=0;
      else
         Result:=2;   // value of the Options parameter is invalid
      end   // End of the CASE-statement
   else
      Result:=1;   // value of the FileName parameter is invalid

   if Result = 0 then
      begin
         FDialog:=TPrintingDialog.Create(Parent);
         FDialog.Project:=Project;
         FDialog.FormLoad(Parent);

         if not FDialog.PrintOptions.Graphics.SaveToFile(FileName) then
            Result:=4;   // Can't create file because of any reason
      end;
end;

Function TPrintHandler.LoadTemplateFromDB(FileName: String): Integer;
begin
   if FileName <> '' then
      if FileExists(FileName) then
         begin
            FDialog:=TPrintingDialog.Create(Parent);
            FDialog.Project:=Project;
            FDialog.FormLoad(Parent);

            FDialog.LegendCombo.Clear;
            FDialog.PicturesCombo.Clear;
            FDialog.SignaturesCombo.Clear;
            FDialog.PrintOptions.Graphics.Clear;
            if FDialog.PrintOptions.Graphics.LoadFromFile(FileName) then
               begin
                  FDialog.FillDialogComboBoxes;
                  FDialog.LegendCombo.ItemIndex:=FDialog.LegendCombo.Items.Count-1;
                  FDialog.PicturesCombo.ItemIndex:=FDialog.PicturesCombo.Items.Count-1;
                  FDialog.SignaturesCombo.ItemIndex:=FDialog.SignaturesCombo.Items.Count-1;
//                  FDialog.SetPrintOptions(FDialog.PrintOptions);

                  FDialog.FormStore(Parent);
                  Result:=0;
               end
            else
               Result:=3;   // Can't load file or mentioned file is not a template
         end
      else
         Result:=2   // no such file or directory
   else
      Result:=1;   // value of the FileName parameter is invalid
end;

Function TPrintHandler.GetObjInformFromDB(var ATypesList: TStringList): Integer;
var Cnt       : Integer;
begin
   FDialog:=TPrintingDialog.Create(Parent);
   FDialog.Project:=Project;
   FDialog.FormLoad(Parent);
   Result:=0;
   if ATypesList = NIL then
      ATypesList:=TStringList.Create
   else
      ATypesList.Clear;
   with FDialog.PrintOptions.Graphics do
      for Cnt:=0 to ItemCount-1 do
          if Items[Cnt].ClassName='TProjectPlaceholder' then
             ATypesList.Add('0')
          else
             if Items[Cnt].ClassName='TLegendPlaceholder' then
                ATypesList.Add('1')
             else
                if Items[Cnt].ClassName='TWindowsGraphic' then
                   ATypesList.Add('2')
                else
                   if Items[Cnt].ClassName='TPrintSignature' then
                      ATypesList.Add('3')
                   else
                      begin
                         Result:=18;   // WinGIS internal ERROR
                         Break;
                      end;
end;

Function TPrintHandler.DeleteObjectFromDB(ObjectType: Integer; ObjectID: Integer): Integer;
var Graphic   : TCustomGraphic;
    intIndex  : Integer;
begin
   Graphic:=NIL;
   intIndex:=0;
   if ObjectID >= 0 then
      begin
         FDialog:=TPrintingDialog.Create(Parent);
         FDialog.Project:=Project;
         FDialog.FormLoad(Parent);
         Result:=0;
         case ObjectType of
            1 : with FDialog.LegendCombo do    // Legend
                   if ObjectID < Items.Count then
                      begin
                         Graphic:=TCustomGraphic(Items.Objects[ObjectID]);
                         if ItemIndex > ObjectID then
                            intIndex:=ItemIndex-1
                         else
                            if (ItemIndex = ObjectID) and (ItemIndex = Items.Count-1) then
                               intIndex:=ItemIndex-1
                            else
                               intIndex:=ItemIndex;
                         Items.Delete(ObjectID);
                         ItemIndex:=intIndex;
                      end
                   else
                      Result:=2;   // value of the ObjectID parameter is invalid
            2 : with FDialog.PicturesCombo do    // Picture
                   if ObjectID < Items.Count then
                      begin
                         Graphic:=TCustomGraphic(Items.Objects[ObjectID]);
                         if ItemIndex > ObjectID then
                            intIndex:=ItemIndex-1
                         else
                            if (ItemIndex = ObjectID) and (ItemIndex = Items.Count-1) then
                               intIndex:=ItemIndex-1
                            else
                               intIndex:=ItemIndex;
                         Items.Delete(ObjectID);
                         ItemIndex:=intIndex;
                      end
                   else
                      Result:=2;   // value of the ObjectID parameter is invalid
            3 : with FDialog.SignaturesCombo do    // Signature
                   if ObjectID < Items.Count then
                      begin
                         Graphic:=TCustomGraphic(Items.Objects[ObjectID]);
                         if ItemIndex > ObjectID then
                            intIndex:=ItemIndex-1
                         else
                            if (ItemIndex = ObjectID) and (ItemIndex = Items.Count-1) then
                               intIndex:=ItemIndex-1
                            else
                               intIndex:=ItemIndex;
                         Items.Delete(ObjectID);
                         ItemIndex:=intIndex;
                      end
                   else
                      Result:=2;   // value of the ObjectID parameter is invalid
         else
            Result:=1;   // value of the ObjectType parameter is invalid
         end;   // End of the CASE-statement
         if Result = 0 then
            begin
               Graphic.Free;
               FDialog.FormStore(Parent);
            end;
      end
   else
      Result:=2;   // value of the ObjectID parameter is invalid
end;

Function TPrintHandler.MoveObjectFromDB(ObjectType: Integer; ObjectID: Integer; Options: Integer): Integer;
var Graphic   : TCustomGraphic;
    intIndex  : Integer;
begin
   Graphic:=NIL;
   intIndex:=0;
   if (ObjectType >= 0) and (ObjectType < 4) then
      if (Options >= 0) and (Options < 4) then
         if ObjectID >= 0 then
            begin
               FDialog:=TPrintingDialog.Create(Parent);
               FDialog.Project:=Project;
               FDialog.FormLoad(Parent);
               Result:=0;
               case ObjectType of
                  0 : if ObjectID = 0 then   // Project
                         begin
                            // detect the project placeholder
                            Graphic:=TCustomGraphic(FDialog.GetProjGraph);
                            if Graphic = NIL then
                               Result:=18;   // WinGIS internal ERROR
                         end
                      else
                         Result:=2;   // value of the ObjectID parameter is invalid
                  1 : with FDialog.LegendCombo do    // Legend
                         if ObjectID < Items.Count then
                            Graphic:=TCustomGraphic(Items.Objects[ObjectID])
                         else
                            Result:=2;   // value of the ObjectID parameter is invalid
                  2 : with FDialog.PicturesCombo do    // Picture
                         if ObjectID < Items.Count then
                            Graphic:=TCustomGraphic(Items.Objects[ObjectID])
                         else
                            Result:=2;   // value of the ObjectID parameter is invalid
                  3 : with FDialog.SignaturesCombo do    // Signature
                         if ObjectID < Items.Count then
                            Graphic:=TCustomGraphic(Items.Objects[ObjectID])
                         else
                            Result:=2;   // value of the ObjectID parameter is invalid
               end;   // End of the CASE ObjectType statement
               if Result = 0 then
                  begin
                     intIndex:=FDialog.PrintOptions.Graphics.IndexOf(Graphic);
                     case Options of
                        0 : if intIndex > 0 then   // Send to Back
                               begin
                                  FDialog.PrintOptions.Graphics.Delete(intIndex);
                                  FDialog.PrintOptions.Graphics.Insert(0,Graphic);
                               end
                            else
                               // the operation defined by the Options parameter is impossible for the mentioned object
                               Result:=4;
                        1 : if intIndex > 0 then   // Send Backward
                               begin
                                  FDialog.PrintOptions.Graphics.Delete(intIndex);
                                  FDialog.PrintOptions.Graphics.Insert(intIndex-1,Graphic);
                               end
                            else
                               // the operation defined by the Options parameter is impossible for the mentioned object
                               Result:=4;
                        2 : if intIndex < FDialog.PrintOptions.Graphics.ItemCount-1 then   // Bring Forward
                               begin
                                  FDialog.PrintOptions.Graphics.Delete(intIndex);
                                  FDialog.PrintOptions.Graphics.Insert(intIndex+1,Graphic);
                               end
                            else
                               // the operation defined by the Options parameter is impossible for the mentioned object
                               Result:=4;
                        3 : if intIndex < FDialog.PrintOptions.Graphics.ItemCount-1 then   // Bring to Front
                               with FDialog.PrintOptions.Graphics do
                                  begin
                                     Delete(intIndex);
                                     Insert(ItemCount,Graphic);
                                  end
                            else
                               // the operation defined by the Options parameter is impossible for the mentioned object
                               Result:=4;
                     end;   // End of the CASE Options statement
                     
                     if Result = 0 then
                        FDialog.FormStore(Parent);
                  end;
            end
         else
            Result:=2   // value of the ObjectID parameter is invalid
      else
         Result:=3   // value of the Options parameter is invalid
   else
      Result:=1;   // value of the ObjectType parameter is invalid
end;
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 25.10.00}

Initialization
  MenuHandlers.RegisterMenuHandler('FilePrint',TPrintHandler);
{$ENDIF} // <----------------- AXDLL
end.
