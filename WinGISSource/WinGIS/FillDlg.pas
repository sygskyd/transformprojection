{******************************************************************************+
  Unit FillDlg
--------------------------------------------------------------------------------
  Dialog to select a fill-style.
--------------------------------------------------------------------------------
  Autor: Martin Forst
--------------------------------------------------------------------------------
Correction: 27.07.2000 by Dennis Ivanoff, Progis-Moscow, Russia
+******************************************************************************}
unit FillDlg;

interface
{$IFNDEF AXDLL} // <----------------- AXDLL
uses
  SysUtils, WinTypes, WinProcs, Classes, Combobtn, Controls, DipGrid, Forms, Palette,
  Graphics, StdCtrls, TabNotBk, Objects, ExtCtrls, Dialogs, Validate,
  AM_Group, ColorSelector, Measures, Spinbtn, WCtrls, AM_Def, StyleDef, XFills,
  ProjStyle, MultiLng, PropCollect, AM_Proj,
  ComCtrls, Menus;

type
  TPatternDialog = class(TWForm)
    AddBitmapXFillMenu: TMenuItem;
    AddVectorXFillMenu: TMenuItem;
    BackColorBtn: TColorSelectBtn;
    Bevel1: TBevel;
    CancelBtn: TButton;
    ControlsPanel: TPanel;
    DeleteMenu: TMenuItem;
    ForeColorBtn: TColorSelectBtn;
    MlgSection: TMlgSection;
    N1: TMenuItem;
    OkBtn: TButton;
    PatternList: TDipList;
    PropertiesMenu: TMenuItem;
    ForeColorLabel: TWLabel;
    BackColorLabel: TWLabel;
    ScaleLabel: TWLabel;
    ScaleEdit: TEdit;
    ScaleIndependCheck: TCheckBox;
    ScaleVal: TMeasureLookupValidator;
    ScaleSpin: TSpinBtn;
    N2: TMenuItem;
    PopupMenu: TPopupMenu;
    procedure AddBitmapXFillMenuClick(Sender: TObject);
    procedure AddVectorXFillMenuClick(Sender: TObject);
    procedure DeleteMenuClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OrganizeMenuClick(Sender: TObject);
    procedure PatternListClick(Sender: TObject);
    procedure PatternListDblClick(Sender: TObject);
    procedure PatternListDrawCell(Sender: TObject; Canvas: TCanvas; Index: Integer;
      Rect: TRect);
    procedure PropertiesMenuClick(Sender: TObject);
    procedure PatternListMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
  private
    FAllowXFillStyles: Boolean;
    FFillStyle: TFillProperties;
    FOrgFillStyle: TFillProperties;
    FOrgProjStyles: TProjectStyles;
    FProjStyles: TProjectStyles;
    FProj: PProj;
    FXFillList: TStringList;
    procedure UpdateEnabled;
    procedure UpdateXFillList;
  public
    property AllowXFillStyles: Boolean read FAllowXFillStyles write FAllowXFillStyles default TRUE;
    property FillStyle: TFillProperties read FFillStyle write FOrgFillStyle;
    property Project: PProj read FProj write FProj;
    property ProjectStyles: TProjectStyles read FProjStyles write FOrgProjStyles;
  end;

{$ENDIF} // <----------------- AXDLL
implementation
{$IFNDEF AXDLL} // <----------------- AXDLL

{$R *.DFM}

uses AM_Index, AM_Paint, CommonResources, LineDlg, NumTools
{$IFNDEF WMLT}
  , XFillDlg, BitmapXFillDlg, OrganizeDlg
{$ENDIF}
  , SymLib, Win32Def
  , XStyles, AM_Obj
{++ Ivanoff Bug#320}
{$IFNDEF WMLT}
  , DK_ReplaceDlg
{$ENDIF}
  , AM_Layer, AM_Main
{-- Ivanoff Bug#320}
  ;

const
  SystemFillStyles = 8;

procedure TPatternDialog.FormCreate(Sender: TObject);
begin
  FAllowXFillStyles := TRUE;
  FFillStyle := TFillProperties.Create;
  PatternList.Count := 7;
  FProjStyles := TProjectStyles.Create;
  BackColorBtn.Palette := FProjStyles.Palette;
  ForeColorBtn.Palette := FProjStyles.Palette;
  FXFillList := TStringList.Create;
//  FXFillList.Sorted:=TRUE;
  FXFillList.Duplicates := dupAccept;
{$IFDEF WMLT}
  Popupmenu.AutoPopup := False;
{$ENDIF}
end;

procedure TPatternDialog.FormDestroy(Sender: TObject);
begin
  FFillStyle.Free;
  FXFillList.Free;
  FProjStyles.Free;
end;

procedure TPatternDialog.PatternListDrawCell(Sender: TObject; Canvas: TCanvas; Index: Integer;
  Rect: TRect);
var
  AText: string;
  ARect: TRect;
  Item: TCustomXFill;
  Points: TRectCorners;
begin
  with Canvas do
  begin
    if not FFillStyle.UpdatesEnabled then
    begin
      Font.Color := clBtnShadow;
      Pen.Color := clBtnShadow;
    end;
    if Index < 0 then
    begin
      AText := FFillStyle.PatternOptions.LongNames[Abs(Index) - 1];
      TextRect(Rect, CenterWidthInRect(TextWidth(AText), Rect),
        CenterHeightInRect(TextHeight(AText), Rect), AText);
    end
    else
    begin
      FillRect(Rect);
      ARect := Classes.Rect(Rect.Left + PatternList.RowHeight + 3,
        Rect.Top, Rect.Left + 80, Rect.Bottom);
      if Index < SystemFillStyles - 1 then
      begin
        CommonListBitmaps.Draw(Canvas, Rect.Left + 2, CenterHeightInRect(14, Rect), clbSystem);
        case Index + 1 of
          pt_FDiagonal: Brush.Style := bsFDiagonal;
          pt_Cross: Brush.Style := bsCross;
          pt_DiagCross: Brush.Style := bsDiagCross;
          pt_BDiagonal: Brush.Style := bsBDiagonal;
          pt_Horizontal: Brush.Style := bsHorizontal;
          pt_Vertical: Brush.Style := bsVertical;
          pt_Solid: Brush.Style := bsSolid;
        end;
        if not FFillStyle.UpdatesEnabled then
          Brush.Color := clBtnShadow
        else
          Brush.Color := clBlack;
        SetBKColor(Handle, ColorToRGB(clWindow));
        SetBKMode(Handle, Opaque);
        Pen.Style := psClear;
        with ARect do
          Rectangle(Left, Top + 1, Right + 1, Bottom);
        AText := MlgSection[30 + Index];
      end
      else
      begin
        CommonListBitmaps.Draw(Canvas, Rect.Left + 2, CenterHeightInRect(14, Rect), clbUser);
        Item := TCustomXFill(FXFillList.Objects[Index - SystemFillStyles + 1]);
        RectCorners(InflatedRect(ARect, 0, -1), Points);
        if Item is TVectorXFill then
          with Item as TVectorXFill do
            XLFillPolygon(Handle, Points, 4, StyleDef^.XFillDefs, StyleDef^.Count,
              0, 0, 5 / MinDistance, 0)
        else
          if Item is TBitmapXFill then
            with Item as TBitmapXFill do
              BMFillPolygon(Handle, Points, 4, StyleDef(Handle)^.BitmapHandle, srcCopy, 1);
        AText := Item.Name;
      end;
      ARect := InflatedWidth(Rect, -85, 0);
      TextRect(ARect, ARect.Left, CenterHeightInRect(TextHeight(AText), ARect), AText);
    end;
  end;
end;

procedure TPatternDialog.UpdateEnabled;
var
  AEnable: Boolean;
  AColor: TColor;
begin
  if not FFillStyle.UpdatesEnabled then
    SetControlGroupEnabled(ControlsPanel, FALSE)
  else
    with PatternList do
    begin
      AddBitmapXFillMenu.Enabled := FAllowXFillStyles;
      AddVectorXFillMenu.Enabled := FAllowXFillStyles;
      AEnable := ItemIndex <> -1;
      if AEnable then
        AColor := clWindowText
      else
        AColor := clActiveCaption;
      ForeColorLabel.Font.Color := AColor;
      AEnable := (ItemIndex <> -1) and (ItemIndex <> SystemFillStyles - 2);
      if AEnable then
        AColor := clWindowText
      else
        AColor := clActiveCaption;
      BackColorLabel.Font.Color := AColor;
      AEnable := ItemIndex >= SystemFillStyles - 1;
      DeleteMenu.Enabled := AEnable;
      PropertiesMenu.Enabled := AEnable;
//    if AEnable then AColor:=clWindowText
//    else AColor:=clActiveCaption;
//    ScaleLabel.Font.Color:=clActiveCaption;
      ScaleIndependCheck.Enabled := AEnable;
      ScaleLabel.Enabled := AEnable;
      ScaleEdit.Enabled := AEnable;
    // enable/disable all controls depending on the updates-enabled flag
      if not FillStyle.UpdatesEnabled then
        SetControlGroupEnabled(ControlsPanel, FALSE);
    end;
end;

procedure TPatternDialog.PatternListClick(Sender: TObject);
begin
  UpdateEnabled;
end;

procedure TPatternDialog.PatternListDblClick(Sender: TObject);
begin
  ModalResult := mrOK;
end;

procedure TPatternDialog.FormShow(Sender: TObject);
var
  XFillStyle: TCustomXFill;
begin
  if FOrgFillStyle <> nil then
    FFillStyle.Assign(FOrgFillStyle);
  if FOrgProjStyles <> nil then
    FProjStyles.Assign(FOrgProjStyles);
  UpdateXFillList;
  with FFillStyle do
  begin
    PropertyMode := pmSet;
    ForeColorBtn.Options := ForeColorOptions;
    ForeColorBtn.SelectedColor := ForeColor;
    BackColorBtn.Options := BackColorOptions;
    BackColorBtn.SelectedColor := BackColor;
    with PatternList do
    begin
      if AllowXFillStyles then
        Count := FProjStyles.XFillStyles.Count + SystemFillStyles - 1
      else
        Count := SystemFillStyles - 1;
      HeaderRows := PatternOptions.Count;
      if Pattern <= 0 then
        ItemIndex := -PatternOptions.IndexOfValue(Pattern) - 1
      else
        if Pattern < SystemFillStyles then
          ItemIndex := Pattern - 1
        else
        begin
          XFillStyle := FProjStyles.XFillStyles.NumberedStyles[Pattern];
          if XFillStyle = nil then
            ItemIndex := 0
          else
            ItemIndex := FXFillList.IndexOfObject(XFillStyle) + SystemFillStyles - 1;
        end;
    end;
    ScaleVal.LookupList.Clear;
    SetDoubleField(Scale, 100, ScaleVal, muPercent, ScaleOptions);
    ScaleIndependCheck.Checked := ScaleType = stScaleIndependend;
    UpdateEnabled;
  end;
end;

procedure TPatternDialog.FormHide(Sender: TObject);
begin
  if ModalResult = mrOK then
  begin
    with FFillStyle do
    begin
      PropertyMode := pmSet;
      BackColor := BackColorBtn.SelectedColor;
      ForeColor := ForeColorBtn.SelectedColor;
      with PatternList do
        if ItemIndex < 0 then
          Pattern := PatternOptions.Values[Abs(ItemIndex) - 1]
        else
          if ItemIndex < SystemFillStyles - 1 then
            Pattern := ItemIndex + 1
          else
            Pattern := TCustomXFill(FXFillList.Objects[ItemIndex - SystemFillStyles + 1]).Number;
      Scale := GetDoubleField(1 / 100, ScaleVal, muPercent, ScaleOptions);
      if ScaleIndependCheck.Checked then
        ScaleType := stScaleIndependend
      else
        ScaleType := stProjectScale;
    end;
    if FOrgFillStyle <> nil then
      FOrgFillStyle.Assign(FFillStyle);
    if FOrgProjStyles <> nil then
      FOrgProjStyles.Assign(FProjStyles);
  end;
end;

procedure TPatternDialog.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  CanClose := FALSE;
  if ModalResult <> mrOK then
    CanClose := TRUE
  else
    if CheckValidators(Self) then
    begin
      CanClose := TRUE;
    end;
end;

procedure TPatternDialog.AddBitmapXFillMenuClick(Sender: TObject);
{$IFNDEF WMLT}
var
  Dialog: TBitmapXFillDialog;
  BitmapXFill: TBitmapXFill;
{$ENDIF}
begin
{$IFNDEF WMLT}
  Dialog := TBitmapXFillDialog.Create(Self);
  try
    BitmapXFill := TBitmapXFill.Create;
    Dialog.BitmapXFill := BitmapXFill;
    Dialog.ProjectStyles := FProjStyles;
    if Dialog.ShowModal = mrOK then
    begin
      FProjStyles.XFillStyles.Add(BitmapXFill);
      UpdateXFillList;
      PatternList.Count := FProjStyles.XFillStyles.Count + SystemFillStyles - 1;
      PatternList.ItemIndex := FXFillList.IndexOfObject(BitmapXFill) + SystemFillStyles - 1;
      PatternList.Invalidate;
    end
    else
      BitmapXFill.Free;
  finally
    Dialog.Free;
  end;
{$ENDIF}
end;

procedure TPatternDialog.PropertiesMenuClick(Sender: TObject);
{$IFNDEF WMLT}
var
  Dialog: TForm;
  CustomXFill: TCustomXFill;
{$ENDIF}
begin
{$IFNDEF WMLT}
  with PatternList do
  begin
    CustomXFill := TCustomXFill(FXFillList.Objects[ItemIndex - SystemFillStyles + 1]);
    if CustomXFill.FillType = xftBitmap then
    begin
      Dialog := TBitmapXFillDialog.Create(Self);
      try
        TBitmapXFillDialog(Dialog).BitmapXFill := TBitmapXFill(CustomXFill);
        TBitmapXFillDialog(Dialog).ProjectStyles := FProjStyles;
        if Dialog.ShowModal = mrOK then
        begin
          UpdateXFillList;
          ItemIndex := FXFillList.IndexOf(TBitmapXFillDialog(Dialog).BitmapXFill.Name)
            + SystemFillStyles - 1;
          Invalidate;
        end;
      finally
        Dialog.Free;
      end;
    end
    else
    begin
      Dialog := TXFillStyleDialog.Create(Self);
      try
        TXFillStyleDialog(Dialog).ProjectStyles := FProjStyles;
        TXFillStyleDialog(Dialog).XFillStyle := TVectorXFill(CustomXFill);
        if Dialog.ShowModal = mrOK then
        begin
          UpdateXFillList;
          ItemIndex := FXFillList.IndexOf(TXFillStyleDialog(Dialog).XFillStyle.Name)
            + SystemFillStyles - 1;
          Invalidate;
        end;
      finally
        Dialog.Free;
      end;
    end;
  end;
{$ENDIF}
end;

procedure TPatternDialog.DeleteMenuClick(Sender: TObject);
{$IFNDEF WMLT}
var
  Item: TCustomXFill;
{++ Ivanoff Bug#320}
  RD: TDKReplaceDialog;
  iI, iJ, iRefCount, iUserChoice: Integer;
  AProject: PProj;
  ALayer: PLayer;
  ALayerItem: PIndex;
{-- Ivanoff Bug#320}
{$ENDIF}
begin
{$IFNDEF WMLT}
  with PatternList do
  begin
    Item := TCustomXFill(FXFillList.Objects[ItemIndex - SystemFillStyles + 1]);
{++ Ivanoff Bug#320}
    AProject := WinGisMainForm.ActualProj;
    iRefCount := 0;
    for iI := 1 to AProject.Layers.LData.Count - 1 do
    begin
      ALayer := AProject.Layers.LData.At(iI);
      if ALayer.LineStyle.Style = Item.Number then
        Inc(iRefCount);
    end; // For iI end
    if iRefCount > 0 then
    begin
      MessageBox(Handle, PChar(Format(MlgSection[38], [iRefCount])), 'WinGIS', MB_OK + MB_DEFBUTTON1 + MB_ICONWARNING);
      EXIT;
    end;
    if Item.ReferenceCount > 1 then
    begin
      if MessageBox(Handle, PChar(Format(MlgSection[37], [Item.ReferenceCount])), 'WinGIS', MB_YESNO + MB_DEFBUTTON1 + MB_ICONQUESTION) = mrNO then
        EXIT;
      RD := TDKReplaceDialog.Create(SELF);
      RD.SetPatternStyles(Item.Name, FFillStyle, FXFillList, MlgSection);
      if RD.ShowModal <> mrOK then
        EXIT;
      iUserChoice := RD.GetUserSelect; // With what replace
      for iI := 1 to AProject.Layers.LData.Count - 1 do
      begin
        ALayer := AProject.Layers.LData.At(iI);
           // Check the layer line style.
        if ALayer.FillStyle.Pattern = Item.Number then
          ALayer.FillStyle.Pattern := iUserChoice;
        for iJ := 0 to ALayer.Data.GetCount - 1 do
        begin
          ALayerItem := ALayer.Data.At(iJ);
               // Check the layer's object line style.
          if ALAyerItem.ObjectStyle = nil then
            CONTINUE;
          if ALayerItem.ObjectStyle.FillStyle.Pattern = Item.Number then
            ALayerItem.ObjectStyle.FillStyle.Pattern := iUserChoice;
        end; // For iJ end
      end; // For iI end
      RD.Free;
    end;
{-- Ivanoff Bug#320}
    FXFillList.Delete(ItemIndex - SystemFillStyles + 1);
    FProjStyles.XFillStyles.Delete(Item);
    Item.Free;
    ItemIndex := Min(ItemIndex, Count - 2);
    Count := FProjStyles.XFillStyles.Count + SystemFillStyles - 1;
{++ Ivanoff}
    AProject.SetModified;
    AProject.PInfo.RedrawScreen(TRUE);
{-- Ivanoff}
  end;
{$ENDIF}
end;

procedure TPatternDialog.AddVectorXFillMenuClick(Sender: TObject);
{$IFNDEF WMLT}
var
  Dialog: TXFillStyleDialog;
  VectorXFill: TVectorXFill;
{$ENDIF}
begin
{$IFNDEF WMLT}
  Dialog := TXFillStyleDialog.Create(Self);
  try
    VectorXFill := TVectorXFill.Create;
    Dialog.ProjectStyles := FProjStyles;
    Dialog.XFillStyle := VectorXFill;
    if Dialog.ShowModal = mrOK then
    begin
      FProjStyles.XFillStyles.Add(VectorXFill);
      UpdateXFillList;
      PatternList.Count := FProjStyles.XFillStyles.Count + SystemFillStyles - 1;
      PatternList.ItemIndex := FXFillList.IndexOfObject(VectorXFill) + SystemFillStyles - 1;
      PatternList.Invalidate;
    end
    else
      VectorXFill.Free;
  finally
    Dialog.Free;
  end;
{$ENDIF}
end;

procedure TPatternDialog.OrganizeMenuClick(Sender: TObject);
{$IFNDEF WMLT}
var
  Dialog: TOrganizeDialog;
{$ENDIF}
begin
{$IFNDEF WMLT}
  Dialog := TOrganizeDialog.Create(Self);
  with Dialog do
  try
    XLineStyles := FProjStyles.XLineStyles;
    XFillStyles := FProjStyles.XFillStyles;
    Symbols := PSymbols(FProj^.PInfo^.Symbols);
    Fonts := FProj^.PInfo^.Fonts;
    StartupPage := opFillStyles;
    if ShowModal = mrOK then
    begin
      UpdateXFillList;
      PatternList.Count := FProjStyles.XFillStyles.Count + SystemFillStyles - 1;
      PatternList.Invalidate;
      if ProjectModified then
        FProj^.SetModified;
    end;
  finally
    Dialog.Free;
  end;
{$ENDIF}
end;

procedure TPatternDialog.UpdateXFillList;
var
  Cnt: Integer;
  XFillStyle: TCustomXFill;
begin
  FXFillList.Clear;
  for Cnt := 0 to FProjStyles.XFillStyles.Capacity - 1 do
  begin
    XFillStyle := FProjStyles.XFillStyles.Items[Cnt];
    if XFillStyle <> nil then
      FXFillList.AddObject(XFillStyle.Name, XFillStyle);
  end;
end;

procedure TPatternDialog.PatternListMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var
  Pt: TPoint;
  Index: Integer;
begin
  Pt.x := x;
  Pt.y := y;
  Index := PatternList.ItemAtPos(Pt, True);
  if (Index + 1) >= SystemFillStyles then
  begin
    PatternList.Hint := IntToStr(TCustomXFill(FXFillList.Objects[Index - SystemFillStyles + 1]).Number);
  end
  else
  begin
    PatternList.Hint := '';
  end;
end;
{$ENDIF} // <----------------- AXDLL

end.

