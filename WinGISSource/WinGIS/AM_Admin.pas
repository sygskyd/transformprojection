{****************************************************************************}
{ Unit AM_Admin                                                              }
{----------------------------------------------------------------------------}
{ Verwaltung der Layer                                                       }
{----------------------------------------------------------------------------}
{ Modifikationen                                                             }
{  16.02.1994 Martin Forst                                                   }
{  30.03.1994 Martin Forst   SetMenu                                         }
{  07.04.1994 Martin Forst   DeleteLayer hinzugef�gt                         }
{  12.04.1994 Martin Forst   Selektieren mit Kreis und Poly                  }
{  18.09.1994 Raimund Leitner Transparenter Layer Modus                      }
{****************************************************************************}
unit AM_Admin;

interface

uses WinTypes, WinProcs, AM_Def, AM_Paint, AM_Layer, AM_Obj, AM_Index, AM_Sel, AM_View,
  Objects, AM_MMedi
{$IFNDEF AXDLL} // <----------------- AXDLL
  , ResDlg
{$ENDIF} // <----------------- AXDLL
  , PropCollect
{$IFNDEF AXDLL} // <----------------- AXDLL
  , OmegaLayer {++Sygsky: Omega add-in }
{$IFNDEF WMLT}
  , AM_DDE
{$ENDIF}
{$ENDIF} // <----------------- AXDLL
  ;

type
  PPLayers = ^PLayers;
  PLayers = ^TLayers;
  TLayers = object(TLayer)
    LData: PCollection;
    TopLayer: PLayer;
    SelLayer: PSelect;
    LastIndex: LongInt;
    UsrPattern: PCollection;
    TranspLayer: Boolean;
    ObjsDeleted: LongInt;
    DrawNow: Boolean;
      // pointer to the project
    Parent: Pointer;
    constructor Init;
    constructor Load(S: TOldStream);
    constructor LoadOld(S: TOldStream);
    destructor Done; virtual;
    procedure ClearSelectLayer(PInfo: PPaint; ARect: TDREct; frame: Boolean);
    function DeleteIndex(PInfo: PPaint; AItem: PIndex): Boolean; virtual;
    procedure DeleteLayer(ALayer: PLayer);
    procedure DeSelect(PInfo: PPaint; Item: PIndex); virtual;
    procedure DeSelectAllLayers(PInfo: PPaint);
    procedure DeselectAllLayersWithoutDlg(PInfo: PPaint);
    procedure DetermineTopLayer;
    function Draw(PInfo: PPaint): Boolean; virtual;
    function DrawTransform(PInfo: PPaint): Boolean; virtual;                    //Drawing project with Transformation Coords
    function Draw2(Proj: Pointer; PInfo: PPaint): Boolean;
    function ExistsObject(AItem: PIndex): Integer;
    function IsObjectFixed(AItem: PIndex): Boolean;
    procedure ForObject(AItem: PIndex; Action: Pointer);
    function FirstObject(AItem: PIndex; Action: Pointer): PLayer;
    procedure GetSize(var ABorder: TDRect); virtual;
    function GetTopLayer: PLayer;
    function GetTopmostInsertLayer: PLayer;
    function IndexOfLayer(ALayer: PLayer; Fixed: Boolean): Integer;
    function IndexToLayer(AIndex: LongInt): PLayer;
    function InsertLayer(AName: string; ACol: TColorRef; ALine: Word; APatCol: TColorRef; APat: Word; AWhere: Integer;
      ASymFill: TSymbolFill): PLayer;
{$IFNDEF AXDLL} // <----------------- AXDLL
    function InsertOmegaLayer(AName: string; ACol: TColorRef; ALine: Word; APatCol: TColorRef; APat: Word; AWhere: Integer;
      ASymFill: TSymbolFill): POmegaLayer; {++Sygsky: Omega }
{$ENDIF} // <----------------- AXDLL
    function InsertLayerByReference(BLayer: PLayer; AWhere: Integer): PLayer;
    function InsertZLayer(AName: string; ACol: TColorRef; ALine: Word; APatCol: TColorRef; APat: Word; AWhere: Integer;
      ASymFill: TSymbolFill): PLayer;
    function InsertObject(PInfo: PPaint; Item: PIndex; Paint: Boolean): Boolean; virtual;
    function NameToLayer(AName: string): PLayer;
    procedure NumberLayers;
    function SelectBetweenPolys(PInfo: PPaint; Poly1, Poly2: Pointer; const ObjType: TObjectTypes;
      Inside: Boolean): Boolean; virtual;
    function SearchByPoint(PInfo: PPaint; Point: TDPoint; const ObjType: TObjectTypes): PIndex; virtual;
    function SearchForPoint(PInfo: PPaint; InCircle: Pointer; Exclude: LongInt; const ObjType: TObjectTypes;
      var Point: TDPoint; var SnObje: PIndex): Boolean; virtual;
    procedure Select(PInfo: PPaint; Item: PIndex); virtual;
    procedure SelectAllItems(PInfo: PPaint; Rect: TDRect);
    procedure FinishImpExpRoutine(ExitMode: boolean; ModuleType: string);
    procedure FinishModule(DllIdx: integer);
    function GetIdbProcessMask: integer;
    procedure SetIdbProcessMask(value: integer);
    procedure DoAmpDkmImport(StartDir: string; AmpFileName: string; DkmDBDir: string; DestDBMode: Integer; DestDatabase: string; DestTable: string; LngCode: string);
    function SetLayerPriority(AProj: Pointer; DDEString: AnsiString = ''): Boolean;
    procedure SetState(AState: LongInt; ASet: Boolean); virtual;
    procedure SetTopLayer(ALayer: PLayer; SetSelectMode: boolean = false);
    procedure Store(S: TOldStream); virtual;
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
    procedure DeleteSelected(AProj: Pointer; PInfo: PPaint; DDEHandler: TDDEHandler;
      DelAll: Boolean; var BmpCount: Integer; MM: PMultiMedia; SilentMode: boolean);
    procedure DBSendLayerInfo(AProj: Pointer);
    function SelectByCircle(PInfo: PPaint; Middle: TDPoint; Radius: LongInt;
      const ObjType: TObjectTypes; Inside: Boolean): Boolean; virtual;
    function SelectByNeighbour(PInfo: PPaint; Item: PIndex): Boolean; virtual;
    function SelectByPoint(PInfo: PPaint; Point: TDPoint; const ObjType: TObjectTypes): PIndex; virtual;
    function SelectByPoly(PInfo: PPaint; Poly: Pointer; const ObjType: TObjectTypes; Inside: Boolean): Boolean; virtual;
    function SelectByPolyLine(PInfo: PPaint; Poly: Pointer; const ObjType: TObjectTypes): Boolean; virtual;
    function SelectByRect(PInfo: PPaint; Rect: TDRect; Inside: Boolean): Boolean; virtual;
    function SelectByRectFast(PInfo: PPaint; Rect: TDRect): Boolean; virtual;
{$ENDIF} // <----------------- AXDLL
    function SelectByRectFastWithoutDlg(PInfo: PPaint; Rect: TDRect; Inside: boolean): Boolean; virtual;
{$ENDIF}
      //++ Glukhov Bug#236 Build#144 25.12.00
      //       Procedure   LoadBmps(AData:Pointer;CurrScreen:TDRect;ScreenPix:TRect;C256:Boolean);
      //       Procedure   ReLoadBmps(AData:Pointer;CurrScreen:TDRect);
      //       Procedure   UnLoadReLoadBmps(AData:Pointer;CurrScreen:TDRect;ScreenPix:TRect;C256:Boolean);
      //-- Glukhov Bug#236 Build#144 25.12.00
  end;

implementation

uses AM_Proj, AM_StdDl, AM_Ini, Var_Cont, Palette,
{$IFNDEF AXDLL} // <----------------- AXDLL
  LayerDlg,
{$ENDIF} // <----------------- AXDLL
  Controls, StateBar,
  SysUtils, Classes, StyleDef, UserIntf, AM_ProjO, ULayer, UClass, UAdmin, UHeap, UGlob,
  ualock, am_ZLayer, DDEDef
  , AttachHndl //++ Glukhov ObjAttachment 18.10.00
{$IFNDEF AXDLL} // <----------------- AXDLL
   {++ IDB}
  , Am_Main
   {-- IDB}
{$ENDIF} // <----------------- AXDLL
  ;

type
  TItemFunc = function(Item, BItem: Pointer; BP: Word): Boolean;
  TItemProc = procedure(Item, BItem: Pointer; BP: Word);

constructor TLayers.Init;
begin
  TLayer.Init(nil);
  Dispose(Data, Done);
  Data := nil;
  LastIndex := 0;
  LData := Pointer(New(PCollection, Init(co_InitSize, co_Expand)));
  SelLayer := New(PSelect, Init(@Self));
  LData^.Insert(SelLayer);
{$IFNDEF AXDLL} // <----------------- AXDLL
  InsertLayer(GetLangText(1500), RGBColors[nl_Color], nl_Line, RGBColors[nl_PatCol],
    nl_Pat, ilTop, DefaultSymbolFill);
{$ELSE}
  InsertLayer('Active Layer', RGBColors[nl_Color], nl_Line, RGBColors[nl_PatCol],
    nl_Pat, ilTop, DefaultSymbolFill);
{$ENDIF} // <----------------- AXDLL

  NumberLayers;
  DetermineTopLayer;
  TranspLayer := FALSE;
  DrawNow := TRUE;
end;

constructor TLayers.LoadOld(S: TOldStream);

  procedure DoAll
      (
      Item: PLayer
      ); far;
  begin
    Item^.Owner := @Self;
    Inc(LastIndex);
    Item^.Index := LastIndex;
  end;
begin
  TLayer.Load(S);
  LData := Pointer(S.Get);
  LastIndex := -1;
  LData^.ForEach(@DoAll);
  SelLayer := LData^.At(0);
  NumberLayers;
  TopLayer := GetTopLayer;
  TranspLayer := FALSE;
  DrawNow := TRUE;
end;

constructor TLayers.Load(S: TOldStream);

  procedure DoAll(Item: PLayer); far;
  begin
    Item^.Owner := @Self;
  end;
begin
  TLayer.Load(S);
  LData := Pointer(S.Get);
  S.Read(LastIndex, SizeOf(LastIndex));
  LData^.ForEach(@DoAll);
  SelLayer := LData^.At(0);
  NumberLayers;
  TopLayer := GetTopLayer;
  TranspLayer := FALSE;
  DrawNow := TRUE;
end;

procedure TLayers.Store
  (
  S: TOldStream
  );
begin
  TLayer.Store(S);
  S.Put(LData);
  S.Write(LastIndex, SizeOf(LastIndex));
end;

destructor TLayers.Done;
begin
  Dispose(LData, Done);
  TLayer.Done;
end;

function TLayers.Draw
  (
  PInfo: PPaint
  )
  : Boolean;
var
  Cnt: Integer;
begin
   { draw all layers in reverse oder, except the selection-layer }

  if DrawNow then
    for Cnt := LData^.Count - 1 downto 1 do
    begin
      if PLayer(LData^.At(Cnt))^.Draw(PInfo) then
      begin
        Result := TRUE;
        Exit;
      end;
    end;
  Result := FALSE;
end;
{******************************************************************************

Draw and transform coords

******************************************************************************}

function TLayers.DrawTransform
  (
  PInfo: PPaint
  )
  : Boolean;
var
  Cnt: Integer;
begin
   { draw all layers in reverse oder, except the selection-layer }

  if DrawNow then
    for Cnt := LData^.Count - 1 downto 1 do
    begin
      if PLayer(LData^.At(Cnt))^.DrawTransform(PInfo) then
      begin
        Result := TRUE;
        Exit;
      end;
    end;
  Result := FALSE;
end;

function TLayers.Draw2
  (
  Proj: Pointer;
  PInfo: PPaint
  )
  : Boolean;
var
  i: longint;
  ALayer: PLayer;
  p: PIndex;
  b: Boolean;
  BLayer: CLayer;
  APtr: TExtPtr;
  SCount: longint;
  pt2: TTraversePtr;

  HLAbort: Boolean;
  lstr: string;
  hstr: string;
   //MLock : THardlock;
  cnt: integer;
begin
  HLAbort := false;
  if PProj(Proj)^.Document.JustCreated then
    HlABort := false;

  SCount := GetHeapMan.Swapincount;
  GetHeapMan.Swapincount := 0;
  SCount := GetHeapMan.Swapincount;
  Draw2 := false;
  GetHeapMan.Abortdraw := false;
  if PProj(Proj)^.Document.Loaded then
  begin
    TGlobal(PProj(Proj)^.AHeapManager.GlobalObj).DRawLst.ResetAll;
    if PProj(Proj)^.BaseMap then
    begin
      PProj(Proj)^.Document.Draw;
      for i := LData^.count - 1 downto 0 do
      begin
        ALayer := LData^.at(i);
        if ALayer <> nil then
        begin
          if ALayer^.Draw(PInfo) then
          begin
            Draw2 := true;
            break
          end;
        end;
      end;
    end
    else
      if PProj(Proj)^.ExtMap then
      begin
        for i := LData^.count - 1 downto 1 do
        begin
          ALayer := LData^.at(i);
          if ALayer <> nil then
          begin
            cnt := ALayer^.Data^.getcount;
            if ALayer^.GetState(sf_HSDVisible) then
            begin
              APtr := PProj(Proj)^.Document.GetLayers.GetLayerExtPtr(ALayer^.Text^);
              BLayer := APtr.Get;
              if (BLayer <> nil) then
              begin
                BLayer.LockPage(BLayer);
                Pt2 := TTraversePtr.create;
                Pt2.Item := BLayer.pTree;
                if not HLAbort then
                  if (BLayer.pTree <> nil) and (not GetHeapMan.AbortDraw) then
                    BLayer.DrawPByQuery(PInfo, ALayer, pt2 {BLayer.pTree});
                pt2.free;
                BLayer.UnlockPage(BLayer);
              end;
            end;

            if ALayer^.Draw(PInfo) then
            begin
              Draw2 := true;
              break
            end;

          end;
          if GetHeapMan.Abortdraw then
            break;
        end;
      end;
  end;
  SelLayer^.Draw(PInfo);
end;

function TLayers.InsertObject
  (
  PInfo: PPaint;
  Item: PIndex;
  Paint: Boolean
  )
  : Boolean;
begin
  if not Toplayer^.GetState(sf_LayerOff) then
  begin
    if IndexObject(PInfo, Item) <> nil then
    begin
      TopLayer^.InsertObject(PInfo, New(PIndex, Init(Item^.Index)), TRUE);
      InsertObject := TRUE;
    end
    else
      InsertObject := FALSE;
  end
  else
    InsertObject := FALSE;
end;

procedure TLayers.Select(PInfo: PPaint; Item: PIndex);
var
  NewIndex: PIndex;
begin
  NewIndex := New(PIndex, Init(Item^.Index));
  NewIndex^.Cliprect.InitByRect(Item^.Cliprect);
  NewIndex^.Mptr := Item^.Mptr;
  Item.CopyObjectStyleTo(NewIndex); {++Sygsky}
  SelLayer^.InsertObject(PInfo, NewIndex, True);
end;

procedure TLayers.SelectAllItems
  (
  PInfo: PPaint;
  Rect: TDRect
  );
var
  i: Integer;
  ALayer: PLayer;
begin
  for i := 0 to LData^.Count - 1 do
  begin
    ALayer := LData^.At(i);
    ALayer^.SelectLinesInside(Pinfo, Rect, TRUE);
  end;
end;

procedure TLayers.ClearSelectLayer
  (
  PInfo: PPaint;
  ARect: TDRect;
  Frame: Boolean
  );
var
  i: integer;
  ALayer: PLayer;
begin
   {for i:=0 to SelLayer^.Data^.GetCount-1 do begin
     AItem:=SelLayer^.Data^.at(i);
     AView:=IndexObject(PInfo,AItem);
     BIndex.Index:=AItem^.Index;
     PLayer(PInfo^.Objects)^.DeleteIndex(PInfo,@BIndex);
     DeleteIndex(PInfo,@BIndex);
   end;
   SelLayer^.Data^.FreeAll;}
  for i := 0 to LData^.Count - 1 do
  begin
    ALayer := LData^.At(i);
    ALayer^.DeleteSelectedRect(PInfo, ARect, Frame);
  end;
end;

procedure TLayers.DeSelect
  (
  PInfo: PPaint;
  Item: PIndex
  );
begin
  SelLayer^.DeleteIndex(PInfo, Item);
end;

{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL

function TLayers.SelectByPoint {~Ray}
  (
  PInfo: PPaint;
  Point: TDPoint;
  const ObjType: TObjectTypes
  )
  : PIndex;
begin
  if not TopLayer^.GetState(sf_LayerOff) then
    Result := TopLayer^.SelectByPoint(PInfo, Point, ObjType)
  else
    Result := nil;
end;
{$ENDIF} // <----------------- AXDLL
{$ENDIF}

function TLayers.SearchByPoint
  (
  PInfo: PPaint;
  Point: TDPoint;
  const ObjType: TObjectTypes
  )
  : PIndex;
begin
  if not TopLayer^.GetState(sf_LayerOff) then
    SearchByPoint := TopLayer^.SearchByPoint(PInfo, Point, ObjType)
  else
    Result := nil;
end;

{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL

function TLayers.SelectByRect
  (
  PInfo: PPaint;
  Rect: TDRect;
  Inside: Boolean
  )
  : Boolean;
begin
  if not TopLayer^.GetState(sf_LayerOff) then
    SelectByRect := TopLayer^.SelectByRect(PInfo, Rect, Inside)
  else
    Result := False;
end;

function TLayers.SelectByRectFast
  (
  PInfo: PPaint;
  Rect: TDRect
  )
  : Boolean;
begin
  if not TopLayer^.GetState(sf_LayerOff) then
    SelectByRectFast := TopLayer^.SelectByRectFast(PInfo, Rect)
  else
    Result := FALSE;
end;
{$ENDIF} // <----------------- AXDLL
{$ENDIF}

// MAR: function is used to finish import/export routine

procedure TLayers.FinishImpExpRoutine(ExitMode: boolean; ModuleType: string);
begin
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
  WinGISMainForm.FinishImpExpRoutine(ExitMode, ModuleType);
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
end;

// MAR: function is used to finish a Module application

procedure TLayers.FinishModule(DllIdx: integer);
begin
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
  WinGISMainForm.FinishModule(DllIdx);
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
end;

// MAR: function is used to get the current Idb-setup

function TLayers.GetIdbProcessMask: integer;
begin
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
  result := WinGISMainForm.GetIdbProcessMask;
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
end;

// MAR: function is used to setup Idb

procedure TLayers.SetIdbProcessMask(value: integer);
begin
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
  WinGISMainForm.SetIdbProcessMask(value);
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
end;

// MAR: function is used to start an amp-dkm-import

procedure TLayers.DoAmpDkmImport(StartDir: string; AmpFileName: string; DkmDBDir: string; DestDBMode: Integer; DestDatabase: string; DestTable: string; LngCode: string);
begin
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
  WinGISMainForm.DoAmpDkmImport(StartDir, AmpFileName, DkmDBDir, DestDBMode, DestDatabase, DestTable, LngCode);
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
end;

// MAR: function is used to make selection that is not shown on screen
{$IFNDEF WMLT}

function TLayers.SelectByRectFastWithoutDlg
  (
  PInfo: PPaint;
  Rect: TDRect;
  Inside: boolean
  )
  : Boolean;
begin
  if not TopLayer^.GetState(sf_LayerOff) then
    SelectByRectFastWithoutDlg := TopLayer^.SelectByRectFastWithoutDlg(PInfo, Rect, Inside)
  else
    Result := FALSE;
end;

{$IFNDEF AXDLL} // <----------------- AXDLL

function TLayers.SelectByNeighbour
  (
  PInfo: PPaint;
  Item: PIndex
  )
  : Boolean;
begin
  if not TopLayer^.GetState(sf_LayerOff) then
    SelectByNeighbour := TopLayer^.SelectByNeighbour(PInfo, Item)
  else
    Result := FALSE;
end;

function TLayers.SelectByCircle
  (
  PInfo: PPaint;
  Middle: TDPoint;
  Radius: LongInt;
  const ObjType: TObjectTypes;
  Inside: Boolean
  )
  : Boolean;
begin
  if not TopLayer^.GetState(sf_LayerOff) then
    SelectByCircle := TopLayer^.SelectByCircle(PInfo, Middle, Radius, ObjType, Inside)
  else
    Result := FALSE;
end;

function TLayers.SelectByPoly
  (
  PInfo: PPaint;
  Poly: Pointer;
  const ObjType: TObjectTypes;
  Inside: Boolean
  )
  : Boolean;
begin
  if not TopLayer^.GetState(sf_LayerOff) then
    SelectByPoly := TopLayer^.SelectByPoly(PInfo, Poly, ObjType, Inside)
  else
    Result := FALSE;
end;

function TLayers.SelectByPolyLine
  (
  PInfo: PPaint;
  Poly: Pointer;
  const ObjType: TObjectTypes
  )
  : Boolean;
begin
  if not TopLayer^.GetState(sf_LayerOff) then
    SelectByPolyLine := TopLayer^.SelectByPolyLine(PInfo, Poly, ObjType)
  else
    Result := FALSE;
end;
{$ENDIF} // <----------------- AXDLL
{$ENDIF}

procedure TLayers.SetState
  (
  AState: LongInt;
  ASet: Boolean
  );

  procedure DoAll
      (
      Item: PLayer
      ); far;
  begin
    Item^.SetState(AState, ASet);
  end;

begin
  LData^.ForEach(@DoAll);
end;

function TLayers.SetLayerPriority
  (
  AProj: Pointer;
  DDEString: AnsiString = ''
  )
  : Boolean;
var
  NewOrder: PCollection;
  ObjToDel: PLayer;
  NeedRedraw: Boolean;
  NextIndex: Integer;
  OldTopLayer: PLayer;
  TopLayerDel: Boolean;
  AllCount: LongInt;
  CurCount: LongInt;
  LayerList: TList;
  Cnt: Integer;
  FirstDelLayer: Boolean;
  NameLength: Integer;
  OldTopIndex: LongInt;
  TmpStr: string;

  procedure DoAll
      (
      Item: PLayer
      ); far;
  begin
    if Item^.SortIndex > 0 then
      with Item^ do
      begin
        LayerList.Add(Item^.GetLayerStyle);
      end;
  end;

  procedure CopyLayer
      (
      Item: TLayerProperties
      ); far;
  var
    ALayer: PLayer;
  begin
    with Item do
    begin
      if Index = 0 then
      begin
        ALayer := New(PLayer, Init(@Self));
        Inc(LastIndex);
        ALayer^.Index := LastIndex;
        NeedRedraw := TRUE;
      end
      else
      begin
        ALayer := LData^.At(Index);
        LData^.AtPut(Index, New(PLayer, Init(nil)));
        if NextIndex <> Index then
          NeedRedraw := TRUE;
        Inc(NextIndex);
      end;
      if PToStr(ALayer^.Text) <> Item.Name then
        PProj(AProj)^.SetModified;
      if ALayer^.SetLayerStyle(PProj(AProj)^.PInfo, Item) then
        NeedRedraw := TRUE;
      NewOrder^.Insert(ALayer);
    end;
  end;

  procedure CopyObjects {kopiert alle zu l�schenden Objekte in eine L�schliste}
      (
      Item: PLayer
      ); far;
  var
    IndexStr: string;

    procedure DoAll
        (
        AItem: PIndex
        ); far;
    begin
      ObjToDel^.InsertObject(PProj(AProj)^.PInfo, AItem, FALSE);
  {$IFNDEF WMLT}
  {$IFNDEF AXDLL} // <----------------- AXDLL
      if DDEHandler.FLayerInfo <> 0 then
        DDEHandler.AppendList1(AItem^.Index, NameLength);
  {$ENDIF} // <----------------- AXDLL
  {$ENDIF}
      Inc(CurCount);
      StatusBar.Progress := 100 * CurCount / AllCount;
    end;
  begin
    if Item^.SortIndex > 0 then
    begin
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
      if DDEHandler.FLayerInfo = 1 then
      begin
        if not FirstDelLayer then
          DDEHandler.EndList1(FALSE, NameLength);
        NameLength := Length(Item^.Text^);
        DDEHandler.StartList('[DEL][' + Item^.Text^ + '][   ]');
      end
      else
        if DDEHandler.FLayerInfo = 2 then
        begin
          if not FirstDelLayer then
            DDEHandler.EndList1(FALSE, NameLength);
          Str(Item^.Index: 10, IndexStr);
          NameLength := 10;
          DDEHandler.StartList('[DEL][' + IndexStr + '][   ]');
        end;
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
      Item^.Data^.ForEach(@DoAll);
      if Item = OldTopLayer then
        TopLayerDel := TRUE;
      NeedRedraw := TRUE;
      Item^.DeSelectAll(PProj(AProj)^.PInfo);
      FirstDelLayer := FALSE;
    end;
  end;

  procedure CountObjects
      (
      Item: PLayer
      ); far;

    procedure DoAll
        (
        AItem: PIndex
        ); far;
    begin
      Inc(AllCount);
    end;
  begin
    if Item^.SortIndex > 0 then
         {++ IDB}
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
    begin
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
         {-- IDB}
      Item^.Data^.ForEach(@DoAll);
         {++ IDB}// Delete the attribute table for deleting layer.
{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}
      if WinGisMainForm.WeAreUsingTheIDB[SELF.Parent] then
        WinGisMainForm.UndoRedo_Man.DeleteLayerTables(SELF.Parent, Item);
    end;
{$ENDIF}
{$ENDIF} // <----------------- AXDLL
      {-- IDB}
  end;

  procedure DeleteObjects
      (
      Item: PIndex
      ); far;
  begin
    if ExistsObject(Item) = 0 then
      with PProj(AProj)^ do
      begin
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
        if DDEHandler.FLayerInfo = 0 then
          DDEHandler.AppendList(Item^.Index);
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
        PObjects(PInfo^.Objects)^.DeleteIndex(PInfo, Item);
        Inc(CurCount, 4);
        StatusBar.Progress := 100 * CurCount / AllCount;
      end;
  end;

   // deselektiert transparent selektierte Objekte auf gerade ausgeschalteten Layern

  procedure DeselOffLayers(Item: PLayer); far;
  begin
    if Item^.GetState(sf_LayerOff) then
      Item^.DeSelectAll(PProj(AProj)^.PInfo);
  end;

  function CheckTopLayer(Item: PLayer): Boolean; far;
  begin
    if Item^.Index = OldTopIndex then
    begin
      TopLayerDel := FALSE;
      Result := TRUE;
    end
    else
      Result := FALSE;
  end;

   // releases all xstyles referenced by the layer

  procedure RemoveUseCounts(Item: PLayer); far;
  begin
    with Item^ do
      UpdateUseCounts(PProj(AProj)^.PInfo, LineStyle, FillStyle, SymbolFill, FALSE);
  end;

  procedure ShowLayers(Item: PLayer); far; {����������>>>}
  var
    a: Integer;
    b: string;
  begin
    a := Item^.Index;
    if Item^.Text <> nil then
      b := Item^.Text^
    else
      b := '';
  end; {<<<����������}

var
  Swapp: PCollection;
  DRes: Integer;
begin
  repeat
    NeedRedraw := FALSE;
    with PProj(AProj)^ do
    begin
      LayerList := TList.Create;
      LData^.ForEach(@ShowLayers); {����������}
      LData^.ForEach(@DoAll);
      OldTopLayer := TopLayer;
      TopLayerDel := FALSE;
{$IFNDEF AXDLL} // <----------------- AXDLL
      LayerDialog := TLayerDialog.Create(Parent);
      LayerDialog.DDEString := DDEString;
      LayerDialog.LayerList := LayerList;
      LayerDialog.TopLayer := PToStr(Layers^.TopLayer^.Text);
      LayerDialog.Project := AProj;
      LayerDialog.HSDAttributesLocked := PProj(AProj)^.AttribFix;
{$ENDIF} // <----------------- AXDLL
      LData^.ForEach(@ShowLayers); {����������}
{$IFNDEF AXDLL} // <----------------- AXDLL
      Dres := LayerDialog.ShowModal;
      if (DRes = mrOK) or (DRes = mrRetry) then
{$ENDIF} // <----------------- AXDLL
      begin
        OldTopIndex := TopLayer^.Index;
        LData^.ForEach(@ShowLayers); {����������}
        NewOrder := New(PCollection, Init(co_InitSize, co_Expand));
        NewOrder^.Insert(LData^.At(0));
        NewOrder^.ForEach(@ShowLayers); {����������}
        LData^.ForEach(@ShowLayers); {����������}
        NextIndex := 1;
        for Cnt := 0 to LayerList.Count - 1 do
          CopyLayer(LayerList[Cnt]);
        NewOrder^.ForEach(@ShowLayers); {����������}
        LData^.ForEach(@ShowLayers); {����������}
        LData^.AtDelete(0);
        LData^.ForEach(@ShowLayers); {����������}
        Swapp := LData;
        Swapp^.ForEach(@ShowLayers); {����������}
        LData := NewOrder;
        LData^.ForEach(@ShowLayers); {����������}
        TopLayerDel := TRUE;
        LData^.FirstThat(@CheckTopLayer);
        LData^.ForEach(@DeselOffLayers);
        PProj(AProj)^.SetStatusText(GetLangText(4011), TRUE);
        SetCursor(crHourGlass);
        FirstDelLayer := TRUE;
        ObjToDel := New(PLayer, Init(nil));
        if Swapp^.Count > 0 then
        begin
          AllCount := 0;
          Swapp^.ForEach(@CountObjects);
          if AllCount > 0 then
          begin
            StatusBar.CancelText := FALSE;
            StatusBar.ProgressPanel := TRUE;
            try
              StatusBar.ProgressText := GetLangText(11741);
              AllCount := AllCount * 5;
              CurCount := 0;
              Swapp^.ForEach(@CopyObjects);
              if ObjToDel^.Data^.GetCount > 0 then
              begin
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
                if DDEHandler.FLayerInfo = 0 then
                  DDEHandler.StartList('[DEL][   ]');
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
                ObjToDel^.Data^.ForEach(@DeleteObjects);
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
                if DDEHandler.FLayerInfo = 0 then
                  DDEHandler.EndList(TRUE)
                else
                  DDEHandler.EndList1(TRUE, NameLength);
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
                ObjToDel^.Data^.DeleteAll;
              end;
            finally
              StatusBar.CancelText := TRUE;
              StatusBar.ProgressPanel := FALSE;
            end;
          end;
        end;
        Dispose(ObjToDel, Done);
        Swapp^.ForEach(@RemoveUseCounts);
            {++ IDB}// If any layer was deleted, all data windows for this project should be refreshed.
{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}
        if (WinGisMainForm.WeAreUsingTheIDB[SELF.Parent]) and (Swapp.Count > 0) then
          WinGisMainForm.IDB_Man.RefreshDataWindows(SELF.Parent);
{$ENDIF}
{$ENDIF} // <----------------- AXDLL
            {-- IDB}
        Dispose(Swapp, Done);
        PProj(AProj)^.ClearStatusText;
        SetCursor(crDefault);
{$IFNDEF AXDLL} // <----------------- AXDLL
        NumberLayers;
        TopLayer := NameToLayer(LayerDialog.Toplayer);
        if TopLayer <> OldTopLayer then
        begin
          if TopLayerDel then
          begin
            SelLayer^.Data^.FreeAll;
            ShowNum := 0;
            SelList^.DeleteAll;
          end
          else
            DeSelectAll(TRUE);
        end;
{$IFNDEF WMLT}
        if (DDEHandler.FSendLMSets) or (CoreConnected) then
        begin
          DDEHandler.SetDBSendStrings(DDECommLMI, nil);
          ProcRequestLayerInfos(AProj, 'LMS');
          if DDEHandler.FLayerInfo <> 2 then
            DDEHandler.SendString('[ALA][' + TopLayer^.Text^ + ']')
          else
          begin
            Str(TopLayer^.Index: 10, TmpStr);
            DDEHandler.SendString('[ALA][' + TmpStr + ']');
          end;
          DDEHandler.SetDBSendStrings(DDECommAll, nil);
        end;
{$ENDIF}
{$ENDIF} // <----------------- AXDLL
        if PProj(AProj)^.Document.Loaded then
          PProj(AProj)^.Document.ActualLayer := TopLayer^.Text^;
        UserInterface.Update([uiMenus, uiStatusBar, uiLayers, uiWorkingLayer, uiViews]);
        PProj(AProj)^.SetModified;
      end;
      for Cnt := 0 to LayerList.Count - 1 do
        TLayerProperties(LayerList[Cnt]).Free;
      LayerList.Free;
{$IFNDEF AXDLL} // <----------------- AXDLL
      LayerDialog.Free;
{$ENDIF} // <----------------- AXDLL
      UserINterface.Update([uiLegends]);
    end;

    Result := False;
    for Cnt := 1 to LData^.Count - 1 do
    begin
      if PLayer(LData^.At(Cnt)) = PProj(AProj)^.TooltipLayer then
      begin
        Result := True;
        break;
      end;
    end;
    if not Result then
    begin
      PProj(AProj)^.TooltipLayer := nil;
    end;
    Result := NeedRedraw;
    if (DRes = mrRetry) then
      PProj(AProj)^.PInfo^.RedrawScreen(False);
  until DRes <> mrRetry;
end;

procedure TLayers.NumberLayers;
var
  AktIndex: LongInt;

  procedure DoAll
      (
      Item: PLayer
      ); far;
  begin
    Item^.SortIndex := AktIndex;
    Inc(AktIndex);
  end;
begin
  AktIndex := 0;
  LData^.ForEach(@DoAll);
end;

function TLayers.InsertLayer
  (
  AName: string;
  ACol: TColorRef;
  ALine: Word;
  APatCol: TColorRef;
  APat: Word;
  AWhere: Integer;
  ASymFill: TSymbolFill
  )
  : PLayer;
var
  ALayer: PLayer;
begin
  AName := ValidateLayerName(AName);
  ALayer := NameToLayer(AName);
  if ALayer <> nil then
  begin
    InsertLayer := ALayer;
    exit;
  end;
  ALayer := New(PLayer, Init(@Self));
  Inc(LastIndex);
  ALayer^.Index := LastIndex;
  ALayer^.SetStyle(ACol, ALine, APatCol, clNone, APat, UsrPattern, ASymFill);
  ALayer^.SetText(AName);
  if AWhere = ilTop then
    LData^.AtInsert(1, ALayer)
  else
    if AWhere = ilSecond then
      LData^.AtInsert(LData^.IndexOf(TopLayer) + 1, ALayer) {1104F}
    else
      LData^.Insert(ALayer);
  NumberLayers;
  InsertLayer := ALayer;
{$IFNDEF AXDLL} // <----------------- AXDLL
  UserInterface.Update([uiLayers]);
{$ENDIF} // <----------------- AXDLL
end;

{++Sygsky: Omega add-in }
{$IFNDEF AXDLL} // <----------------- AXDLL

function TLayers.InsertOmegaLayer
  (
  AName: string;
  ACol: TColorRef;
  ALine: Word;
  APatCol: TColorRef;
  APat: Word;
  AWhere: Integer;
  ASymFill: TSymbolFill
  )
  : POmegaLayer;
var
  ALayer: POmegaLayer;
begin
  ALayer := New(POmegaLayer, Init(@Self));
  Inc(LastIndex);
  ALayer^.Index := LastIndex;
  ALayer^.SetStyle(ACol, ALine, APatCol, clNone, APat, UsrPattern, ASymFill);
  ALayer^.SetText(AName);
  if AWhere = ilTop then
    LData^.AtInsert(1, ALayer)
  else
    if AWhere = ilSecond then
      LData^.AtInsert(LData^.IndexOf(TopLayer) + 1, ALayer) {1104F}
    else
      LData^.Insert(ALayer);
  NumberLayers;
  Result := ALayer;
  UserInterface.Update([uiLayers]);
end;
{$ENDIF} // <----------------- AXDLL
{--Sygsky: Omega add-in }

function TLayers.InsertLayerByReference
  (
  BLayer: PLayer;
  AWhere: Integer
  )
  : PLayer;
var
  ALayer: PLayer;
begin
  ALayer := New(PLayer, Init(@Self));
  Inc(LastIndex);
  ALayer^.SetText(BLayer^.Text^);
  ALayer^.LIneStyle := BLayer^.LineStyle;
  ALayer^.FillStyle := BLayer^.FillStyle;
  ALayer^.SymbolFill := BLayer^.SymbolFill;
  ALayer^.Index := LastIndex;
  ALayer^.ClipRect.InitbyREct(BLayer^.Cliprect);
  ALayer^.State := BLayer^.State;
  if not BLayer^.GetState(sf_Layeroff) then
    ALayer^.SetState(sf_HSDVisible, true);
  ALayer^.GeneralMin := BLayer^.GeneralMin;
  ALayer^.GeneralMax := BLayer^.GeneralMax;
  ALayer^.Printable := BLayer^.Printable;
  ALayer^.UseForSnap := BLayer^.UseForSnap;
  ALayer^.TooltipInfo.Assign(BLayer^.TooltipInfo);
  if AWhere = ilTop then
    LData^.AtInsert(1, ALayer)
  else
    if AWhere = ilSecond then
      LData^.AtInsert(LData^.IndexOf(TopLayer) + 1, ALayer) {1104F}
    else
      LData^.Insert(ALayer);
  NumberLayers;
  InsertLayerByReference := ALayer;
  UserInterface.Update([uiLayers]);
end;

function TLayers.GetTopLayer
  : PLayer;

  function DoAll
      (
      Item: PLayer
      )
      : Boolean; far;
  begin
    DoAll := (Item^.SortIndex <> 0) and (not (Item^.GetState(sf_LayerOff))
      or Item^.GetState(sf_HSDVisible));
  end;
begin
  GetTopLayer := LData^.FirstThat(@DoAll);
  if Result = nil then
    Result := LData^.At(1);
end;

{************************************************************************************}
{ Function TLayers.GetTopmostInsertLayer                                         }
{  Liefert einen Pointer auf den Layer mit der Nummer AIndex oder NIL, wenn kein      }
{  Layer mit der Nummer AIndex existiert.                                            }
{************************************************************************************}

function TLayers.GetTopMostInsertLayer
  : PLayer;

  function DoAll
      (
      Item: PLayer
      )
      : Boolean; far;
  begin
    DoAll := (Item^.SortIndex <> 0) and
      (not (Item^.GetState(sf_LayerOff)) or Item^.GetState(sf_HSDVisible))
      and not (Item^.GetState(sf_Fixed));
  end;
begin
  Result := LData^.FirstThat(@DoAll);
end;

{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL

procedure TLayers.DBSendLayerInfo
  (
  AProj: Pointer
  );

  procedure DoAll
      (
      Item: PLayer
      ); far;
  var
    TmpStr: string;
  begin
    if Item^.Text <> nil then
    begin
      if DDEHandler.FLayerInfo = 1 then
      begin
        DDEHandler.AppendString(Item^.Text^);
      end
      else
        if DDEHandler.FLayerInfo = 2 then
        begin
          Str(Item^.Index: 0, TmpStr);
          TmpStr := Item^.Text^ + DDEHandler.FOutSepSign + TmpStr;
          DDEHandler.AppendString(TmpStr);
        end;
    end;
  end;
begin
  PProj(AProj)^.SetCursor(crHourGlass);
  DDEHandler.StartList('[LIF][   ]');
  LData^.ForEach(@DoAll);
  DDEHandler.EndList(TRUE);
  PProj(AProj)^.SetCursor(crDefault);
end;

procedure TLayers.DeleteSelected
  (
  AProj: Pointer;
  PInfo: PPaint;
  DDEHandler: TDDEHandler;
  DelAll: Boolean;
  var BmpCount: Integer;
  MM: PMultiMedia;
  SilentMode: boolean
  );
var
  UpdateRect: TDRect;
  DoDelete: Boolean;
  DoDeleteFixed: Boolean;
  BResult: Integer;
  CResult: Integer;
  ObjCount: LongInt;
  AllObjects: LongInt;
  DoBreak: Boolean;
  FirstDelObj: Boolean;
  DelLayerName: string;
  DelLNLength: Integer;
  IndexStr: string;
  DelLayerIndex: LongInt;
  AEventString: AnsiString;

  function CheckExist
      (
      Item: PIndex
      )
      : Boolean; far;
  begin
    CheckExist := ExistsObject(Item) <= 1;
  end;

  function CheckFixed
      (
      Item: PIndex
      )
      : Boolean; far;
  begin
    CheckFixed := IsObjectFixed(Item);
  end;

  procedure DoAll
      (
      Item: PIndex
      ); far;
  var
    DelObj: PView;
    Exists: Boolean;
    OldTop: PLayer;
    i: Integer;
    DidDelete: Boolean;
    NotAllDel: Boolean;
      {++ IDB}
    j: Integer;
    ATempLayer: PLayer;
    ATempItem: PIndex;
      {-- IDB}

    procedure AddDDEIndex(DelLayer: PLayer; DelIndex: PIndex);
    begin
      if FirstDelObj then
      begin
        if DDEHandler.FLayerInfo = 1 then
        begin
          DelLayerName := DelLayer^.Text^;
          DelLNLength := Length(DelLayerName);
          DDEHandler.StartList('[DEL][' + DelLayerName + '][   ]');
        end
        else
          if DDEHandler.FLayerInfo = 2 then
          begin
            DelLayerIndex := DelLayer^.Index;
            Str(DelLayerIndex: 10, IndexStr);
            DelLNLength := 10;
            DDEHandler.StartList('[DEL][' + IndexStr + '][   ]');
          end;
        FirstDelObj := FALSE;
      end
      else
      begin
        if DDEHandler.FLayerInfo = 1 then
        begin
          if DelLayerName <> DelLayer^.Text^ then
          begin
            DDEHandler.EndList1(FALSE, DelLNLength);
            DelLayerName := DelLayer^.Text^;
            DelLNLength := Length(DelLayerName);
            DDEHandler.StartList('[DEL][' + DelLayerName + '][   ]');
          end;
        end
        else
          if DDEHandler.FLayerInfo = 2 then
          begin
            if DelLayerIndex <> DelLayer^.Index then
            begin
              DDEHandler.EndList1(FALSE, DelLNLength);
              DelLayerIndex := DelLayer^.Index;
              Str(DelLayerIndex: 10, IndexStr);
              DelLNLength := 10;
            end;
          end;
      end;
      DDEHandler.AppendList1(DelIndex^.Index, DelLNLength);
    end;

    procedure DeleteIndexWithDDE(ADelIndex: PIndex);

      procedure DoAll(Item: PLayer); far;
      begin
        if Item^.DeleteIndex(PInfo, ADelIndex) then
        begin
          Inc(ObjsDeleted);
          if Item^.Index <> 0 then
            AddDDEIndex(Item, ADelIndex);
        end;
      end;
    begin
      ObjsDeleted := 0;
      LData^.ForEach(@DoAll);
    end;
  begin
    DelObj := IndexObject(PInfo, Item);
    UpdateRect.CorrectByRect(DelObj^.ClipRect);
    if DelAll then
    begin
      if TranspLayer then
      begin {wenn im Transparentmodus ein Objekt}
        DidDelete := FALSE; {auf dem obersten oder auf allen}
        NotAllDel := FALSE; {Layern gel�scht werden soll}
        OldTop := TopLayer;
        i := 1; {SelLayer �bergehen}
        repeat
          TopLayer := LData^.At(i);
          Inc(i);
          if not TopLayer^.GetState(sf_LayerOff) then
          begin
            if TopLayer^.HasObject(DelObj) <> nil then
            begin
              if DoDelete or not (TopLayer^.GetState(sf_Fixed)) then
              begin
                if DelObj^.GetObjType = ot_Image then
                  Dec(BmpCount);
                TopLayer^.DeleteIndex(PInfo, DelObj);
                if DDEHandler.FLayerInfo <> 0 then
                  AddDDEIndex(TopLayer, PIndex(DelObj));
                Inc(ObjCount);
                DidDelete := TRUE
              end
              else
                NotAllDel := TRUE;
            end;
          end;
        until (i = LData^.Count);
        SelLayer^.DeleteIndex(PInfo, DelObj);
        if DidDelete then
        begin
          if not NotAllDel then
            MM^.DelMediasAtDel(DelObj^.Index);
          if DDEHandler.FLayerInfo = 0 then
            DDEHandler.AppendList(DelObj^.Index);
        end;
        TopLayer := OldTop;
      end
      else
      begin
        if DoDelete or not (IsObjectFixed(DelObj)) then
        begin
          if DelObj^.GetObjType = ot_Image then
            Dec(BmpCount);
          if DDEHandler.FLayerInfo = 0 then
            DeleteIndex(PInfo, DelObj)
          else
            DeleteIndexWithDDE(PIndex(DelObj));
          Inc(ObjCount, ObjsDeleted);
          MM^.DelMediasAtDel(DelObj^.Index);
          if DDEHandler.FLayerInfo = 0 then
            DDEHandler.AppendList(DelObj^.Index);
          PLayer(PInfo^.Objects)^.DeleteIndex(PInfo, DelObj);
        end
        else
          TopLayer^.DeSelect(PInfo, TopLayer^.HasObject(Item));
      end;
    end
    else
    begin
      Exists := ExistsObject(DelObj) > 1; {Objekt ist �fter als einmal im Projekt vorhanden}
      if TranspLayer then
      begin {Objekte k�nnen auf verschiedenen Layer liegen}
        OldTop := TopLayer;
        i := 1; {SelLayer �bergehen}
        repeat
          TopLayer := LData^.At(i);
          Inc(i);
        until (not TopLayer^.GetState(sf_LayerOff) and (TopLayer^.HasObject(DelObj) <> nil)) or (i = LData^.Count);
        DidDelete := FALSE;
        if (DoDelete or Exists)
          and (not TopLayer^.GetState(sf_Fixed) or (DoDeleteFixed and TopLayer^.GetState(sf_Fixed))) then
        begin
          if DelObj^.GetObjType = ot_Image then
            Dec(BmpCount);
               {++ IDB}
               {Problem linked with the IDB.
                We can have a situation when we have selected objects and these objects are placed on different
                layers. This stuation can be obtained via a database. Therefore at first I try to delete object
                from the top layer and if I faled I find objects on the other layers. Ivanoff.}
          if not TopLayer^.DeleteIndex(PInfo, DelObj) then
            for i := 1 to SELF.LData.Count - 1 do
            begin
              ATempLayer := LData.At(i);
              j := 0;
              while j < ATempLayer.Data.GetCount do
              begin
                ATempItem := ATempLayer.Data.At(j);
                if (ATempItem.Index = DelObj.Index)
                  and
                  (Boolean(ATempItem.State and sf_Selected)) then
                  ATempLayer.DeleteIndex(PInfo, DelObj);
                        //else
                Inc(j);
              end;
            end;
               { This is the old way to do it.
                           TopLayer^.DeleteIndex(PInfo,DelObj);}
               {-- IDB}
          SelLayer^.DeleteIndex(PInfo, DelObj);
          Inc(ObjCount);
          DidDelete := TRUE
        end
        else
          TopLayer^.DeSelect(PInfo, TopLayer^.HasObject(Item));
        TopLayer := OldTop;
      end
      else
      begin
        if DoDelete or Exists then
        begin
          if DelObj^.GetObjType = ot_Image then
            Dec(BmpCount);
               {++ IDB}
               {Problem linked with the IDB.
                We can have a situation when we have selected objects and these objects are placed on different
                layers. This stuation can be obtained via a database. Therefore at first I try to delete object
                from the top layer and if I faled I find objects on the other layers. Ivanoff.}

          if not TopLayer^.DeleteIndex(PInfo, DelObj) then
            for i := 1 to SELF.LData.Count - 1 do
            begin
              ATempLayer := LData.At(i);
              j := 0;
              while j < ATempLayer.Data.GetCount do
              begin
                ATempItem := ATempLayer.Data.At(j);
                if (ATempItem.Index = DelObj.Index)
                  and
                  (Boolean(ATempItem.State and sf_Selected)) then
                  ATempLayer.DeleteIndex(PInfo, DelObj)
                else
                  Inc(j);
              end;
            end;
               { This is the old way to do it.
                           TopLayer^.DeleteIndex(PInfo,DelObj);}
               {-- IDB}
          SelLayer^.DeleteIndex(PInfo, DelObj);
          if DDEHandler.FLayerInfo <> 0 then
            AddDDEIndex(TopLayer, PIndex(DelObj));
          Inc(ObjCount);
          DidDelete := TRUE
        end
        else
          TopLayer^.DeSelect(PInfo, TopLayer^.HasObject(Item));
      end;
      if DoDelete and not (Exists) and DidDelete then
      begin
        MM^.DelMediasAtDel(DelObj^.Index);
        if DDEHandler.FLayerInfo = 0 then
          DDEHandler.AppendList(DelObj^.Index);
        if CoreConnected then
          AEventString := AEventString + '[' + IntToStr(DelObj^.Index) + ']';
        PLayer(PInfo^.Objects)^.DeleteIndex(PInfo, DelObj);
      end;
    end;
    StatusBar.Progress := 100 * ObjCount / AllObjects;
    DoBreak := StatusBar.QueryAbort;
  end;
   //++ Glukhov ObjAttachment 18.10.00
var
  AddSelList: PCollection;
   //-- Glukhov ObjAttachment 18.10.00
begin
  BResult := id_OK;
  CResult := id_OK;
  if DelAll then
  begin
    DoDelete := TRUE;
    if SelLayer^.Data^.FirstThat(@CheckFixed) <> nil then
    begin
      BResult := MsgBox(PInfo^.HWindow, 1019, mb_YesNoCancel or mb_IconQuestion, '');
      DoDelete := BResult = id_Yes;
    end;
  end
  else
  begin
    DoDelete := FALSE;
    if SelLayer^.Data^.FirstThat(@CheckExist) <> nil then
    begin
         {++ Moskaliov BUG#469 BUILD#100 11.01.00}
      if PProj(AProj)^.SymbolMode = sym_SymbolMode then
        DoDelete := TRUE
      else
      begin
            {-- Moskaliov BUG#469 BUILD#100 11.01.00}
            {++ Ivanoff BUG#516}
            //          BResult:=MsgBox(PInfo^.HWindow,1008,mb_YesNoCancel or mb_IconQuestion,'');
                      // 80005='Some of the objects (%d selected) to delete only exist once! These objects will be deleted from the table too! Delete them anyway?'

            { mb: added silentmode }
        if not SilentMode then
          BResult := MessageBox(WingisMainForm.Handle, PChar(Format(MlgStringList['Main', 80005], [SELF.SelLayer.SelInfo.Count])), PChar(WingisMainForm.Caption), mb_YesNoCancel or mb_IconQuestion or MB_APPLMODAL)
        else
          BResult := id_Yes;
            {-- Ivanoff BUG#516}
        DoDelete := BResult = id_Yes;
            {++ Moskaliov BUG#469 BUILD#100 11.01.00}
      end;
         {-- Moskaliov BUG#469 BUILD#100 11.01.00}
    end;
    DoDeleteFixed := TRUE;
    if (BResult <> id_Cancel) and (TranspLayer) then
    begin
      if SelLayer^.Data^.FirstThat(@CheckFixed) <> nil then
      begin
        CResult := MsgBox(PInfo^.HWindow, 1019, mb_YesNoCancel or mb_IconQuestion, '');
        DoDeleteFixed := CResult = id_Yes;
      end;
    end;
  end;
  if (BResult <> id_Cancel) and (CResult <> id_Cancel) then
  begin
      //++ Glukhov ObjAttachment 18.10.00
    if PProj(AProj).PInfo.VariousSettings.bDelAttached then
    begin
         // Check for the attached objects that are not selected
      AddSelList := New(PCollection, Init(16, 16));
      FillUpSelectionByAttached(AProj, AddSelList, True);
      if PProj(AProj).PInfo.VariousSettings.bDelAttachedAsk
        and (AddSelList.Count > 0) then
      begin
            // Ask about the attached objects
        if (PInfo^.AnnotSettings.ShowDialog) and (MsgBox(PInfo.HWindow, 11776, mb_YesNo or mb_IconQuestion or mb_DefButton2, '') <> id_Yes) then
        begin
               // Deselect the additional objects
          ClearSelectionFromAdded(AProj, AddSelList);
        end;
      end;
      AddSelList.DeleteAll;
      Dispose(AddSelList);
    end;
      //-- Glukhov ObjAttachment 18.10.00
    UpdateRect.Init;
    if DDEHandler.FLayerInfo = 0 then
      DDEHandler.StartList('[DEL][   ]');
    if CoreConnected then
      AEventString := '[DEL]';

    FirstDelObj := TRUE;
      //if not SilentMode then
    StatusBar.ProgressPanel := TRUE;
    try
         //if not SilentMode then
         //begin
      StatusBar.ProgressText := GetLangText(11741);
      StatusBar.AbortText := GetLangText(11802);
         //end;
      ObjCount := 0;
      AllObjects := SelLayer^.Data^.GetCount;
      DoBreak := FALSE;
         {++ IDB}// This procedure makes Intenal database to receive deleting object's IDs.
                  // The IDs are transmitted to Internal database in the next cycle (While Do).
{$IFNDEF WMLT}
      if WinGisMainForm.WeAreUsingTheIDB[SELF.Parent] then
        WingisMainForm.IDB_Man.GetReadyToReceiveDeletedObjectID(SELF.Parent);
// ++ Cadmensky
      if WinGisMainForm.WeAreUsingUndoFunction[SELF.Parent] then
        WingisMainForm.UndoRedo_Man.GetReadyToReceiveDeletedObjectID(SELF.Parent);
// -- Cadmensky
{$ENDIF}
         {-- IDB}
      while (SelLayer^.Data^.GetCount > 0) and (not DoBreak) do
        DoAll(SelLayer^.Data^.At(0));
         {++ IDB}// This procedure delete information about deleting objects from an attributive table.
{$IFNDEF WMLT}
      if WinGisMainForm.WeAreUsingTheIDB[SELF.Parent] then
        WingisMainForm.IDB_Man.ExecuteDeleting(SELF.Parent);
// ++ Cadmensky
      if WinGisMainForm.WeAreUsingUndoFunction[SELF.Parent] then
        WingisMainForm.UndoRedo_Man.ExecuteDeleting(SELF.Parent);
// -- Cadmensky
{$ENDIF}
         {-- IDB}
      UpdateRect.Grow(PInfo^.CalculateDraw(Round(TopLayer^.LineStyle.Width))); {!?!?!?!}
      PInfo^.FromGraphic := TRUE;
         //if not SilentMode then
      PInfo^.RedrawRect(UpdateRect);
      if DDEHandler.FLayerInfo = 0 then
        DDEHandler.EndList(TRUE)
      else
        DDEHandler.EndList1(TRUE, DelLNLength);
    finally
         //if not SilentMode then
      StatusBar.ProgressPanel := FALSE;

      if CoreConnected then
        WingisMainForm.DXAppTCore.FireDDEEvent(AEventString);
    end;
  end;
end;
{$ENDIF} // <----------------- AXDLL
{$ENDIF}

function TLayers.SearchForPoint
  (
  PInfo: PPaint;
  InCircle: Pointer;
  Exclude: LongInt;
  const ObjType: TObjectTypes;
  var Point: TDPoint;
  var SnObje: PIndex
  )
  : Boolean;

  function DoAll
      (
      Item: PLayer
      )
      : Boolean; far;
  begin
    DoAll := Item^.SearchForPoint(PInfo, InCircle, Exclude, ObjType, Point, SnObje);
  end;
begin
  SearchForPoint := LData^.FirstThat(@DoAll) <> nil;
end;

function TLayers.DeleteIndex
  (
  PInfo: PPaint;
  AItem: PIndex
  )
  : Boolean;

  procedure DoAll
      (
      Item: PLayer
      ); far;
  begin
    if Item^.DeleteIndex(PInfo, AItem) then
      Inc(ObjsDeleted);
  end;
begin
  ObjsDeleted := 0;
  LData^.ForEach(@DoAll);
  DeleteIndex := TRUE;
end;

function TLayers.FirstObject
  (
  AItem: PIndex;
  Action: Pointer
  )
  : PLayer;
var
  OldBP: Integer;
  Cnt: Integer;
  BItem: PIndex;
begin
  for Cnt := 0 to LData^.Count - 1 do
  begin
    Result := LData^.At(Cnt);
    BItem := Result^.HasObject(AItem);
    if BItem <> nil then
    begin
      asm MOV EAX,@Result
            MOV EDX,BItem
            PUSH DWORD PTR [EBP]
            Call DWORD PTR Action
            ADD ESP,4
            CMP AL,0
            JE @@1
            MOV Cnt,-1
          @@1:
      end;
      if Cnt = -1 then
        Exit;
    end;
  end;
  Result := nil;
end;

procedure TLayers.ForObject
  (
  AItem: PIndex;
  Action: Pointer
  );
var
  OldBP: Integer;
  Cnt: Integer;
  BItem: PIndex;
  ALayer: PLayer;
begin
  for Cnt := 0 to LData^.Count - 1 do
  begin
    ALayer := LData^.At(Cnt);
    BItem := ALayer^.HasObject(AItem);
    if BItem <> nil then
    begin
      asm MOV EAX,ALayer
            MOV EDX,BItem
            PUSH DWORD PTR [EBP]
            Call DWORD PTR Action
            ADD ESP,4
      end;
    end;
  end;
end;

function TLayers.ExistsObject
  (
  AItem: PIndex
  )
  : Integer;

  procedure DoAll
      (
      AItem: PLayer;
      BItem: PIndex
      ); far;
  begin
    if AItem^.SortIndex > 0 then
      Inc(Result);
  end;
begin
  Result := 0;
  ForObject(AItem, @DoAll);
end;

procedure TLayers.GetSize
  (
  var ABorder: TDRect
  );

  procedure DoAll
      (
      Item: PLayer
      ); far;
  begin
    Item^.GetSize(ABorder);
  end;
begin
  LData^.ForEach(@DoAll);
end;

{************************************************************************************}
{ Function TLayer.IndexToLayer                                                       }
{  Liefert eine Pointer auf den Layer mit der Nummer AIndex oder NIL, wenn kein      }
{  Layer mit der Nummer AIndex existiert.                                            }
{************************************************************************************}

function TLayers.IndexToLayer
  (
  AIndex: LongInt
  )
  : PLayer;

  function DoAll
      (
      Item: PIndex
      )
      : Boolean; far;
  begin
    DoAll := Item^.Index = AIndex;
  end;
begin
  IndexToLayer := LData^.FirstThat(@DoAll);
end;

function TLayers.NameToLayer
  (
  AName: string
  )
  : PLayer;

  function DoAll
      (
      Item: PLayer
      )
      : Boolean; far;
  begin
    if Item^.Index = 0 then
      DoAll := FALSE
    else
      DoAll := AnsiCompareText(PToStr(Item^.Text), AName) = 0;
  end;
begin
  if AName = '' then
    NameToLayer := nil
  else
    NameToLayer := LData^.FirstThat(@DoAll);
end;

function TLayers.IsObjectFixed
  (
  AItem: PIndex
  )
  : Boolean;

  function DoAll
      (
      Item: PLayer;
      BItem: PIndex
      )
      : Boolean; far;
  begin
    DoAll := Item^.GetState(sf_Fixed);
  end;
begin
  IsObjectFixed := FirstObject(AItem, @DoAll) <> nil;
end;

{******************************************************************************+
  Procedure TLayers.DeleteLayer
--------------------------------------------------------------------------------
  L�scht einen Layer aus der Layerliste und gibt den belegten Speicher frei.
  Die Prozedur sollte nur f�r leere Layer verwendet werden, da die
  Objekthierarchie f�r Objekte auf dem  Layer nicht aktualisiert wird.
  Der TopLayer wird neu ermittelt und die Layer neu nummeriert.
+******************************************************************************}

procedure TLayers.DeleteLayer(ALayer: PLayer);
begin
   {++ IDB}
{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}
   // Additonally to deleting a layer table, I'll put this layer in an Undo table.
  if WinGisMainForm.WeAreUsingTheIDB[SELF.Parent] then
    WinGisMainForm.UndoRedo_Man.DeleteLayerTables(PProj(Parent), ALayer);
{$ENDIF}
{$ENDIF} // <----------------- AXDLL
   {-- IDB}
  with ALayer^ do
    UpdateUseCounts(PProj(Parent)^.PInfo, LineStyle, FillStyle,
      SymbolFill, FALSE);
  LData^.Free(ALayer);
  NumberLayers;
  DetermineTopLayer;
end;

procedure TLayers.DetermineTopLayer;
begin
  SetTopLayer(GetTopLayer);
end;

procedure TLayers.SetTopLayer(ALayer: PLayer; SetSelectMode: boolean = false);
begin
  if (Parent <> nil) and (TopLayer <> nil) and (TopLayer <> ALayer) then
  begin
    PProj(Parent)^.SetModified;
  end;
  TopLayer := ALayer;
{$IFNDEF AXDLL} // <----------------- AXDLL
  UserInterface.Update([uiStatusBar, uiMenus, uiWorkingLayer]);
{$ENDIF} // <----------------- AXDLL
  if (TopLayer^.GetState(sf_Fixed)) and (SetSelectMode = false) then
    PProj(Parent)^.SetActualMenu(mn_Select);
end;

function TLayers.SelectBetweenPolys
  (
  PInfo: PPaint;
  Poly1: Pointer;
  Poly2: Pointer;
  const ObjType: TObjectTypes;
  Inside: Boolean
  )
  : Boolean;
begin
  SelectBetweenPolys := TopLayer^.SelectBetweenPolys(PInfo, Poly1, Poly2, ObjType, Inside);
end;

procedure TLayers.DeselectAllLayers
  (
  PInfo: PPaint
  );
var
  Cnt2: LongInt;
  Item2: PIndex;
  AllCount: LongInt;

  procedure DoAll
      (
      Layer: PLayer
      ); far;
  var
    Cnt: LongInt;
    Item: PIndex;
  begin
    for Cnt := 0 to Layer^.Data^.GetCount - 1 do
    try
      Item := Layer^.Data^.At(Cnt);
      Item^.SetState(sf_Selected, FALSE);
    except
      break;
    end;
  end;
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  StatusBar.CancelText := FALSE;
  StatusBar.ProgressPanel := TRUE;
{$ENDIF} // <----------------- AXDLL
  try
{$IFNDEF AXDLL} // <----------------- AXDLL
    StatusBar.ProgressText := GetLangText(11744);
{$ENDIF} // <----------------- AXDLL
    LData^.ForEach(@DoAll);
    AllCount := SelLayer^.Data^.GetCount;
      {++ Ivanoff BUG#331 BUILD#95}
      //          SelLayer.DeSelectAll(PInfo);
      {-- Ivanoff}
    Sellayer^.DeleteAll;
  finally
{$IFNDEF AXDLL} // <----------------- AXDLL
    StatusBar.ProgressPanel := FALSE;
    StatusBar.CancelText := TRUE;
{$ENDIF} // <----------------- AXDLL
  end;
{$IFNDEF AXDLL} // <----------------- AXDLL
  UserInterface.Update([uiMenus]);
{$ENDIF} // <----------------- AXDLL
end;

// MAR: function is used to deselect without showing on screen

procedure TLayers.DeselectAllLayersWithoutDlg
  (
  PInfo: PPaint
  );
var
  Cnt2: LongInt;
  Item2: PIndex;
  AllCount: LongInt;

  procedure DoAll
      (
      Layer: PLayer
      ); far;
  var
    Cnt: LongInt;
    Item: PIndex;
  begin
    for Cnt := 0 to Layer^.Data^.GetCount - 1 do
    begin
      Item := Layer^.Data^.At(Cnt);
      Item^.SetState(sf_Selected, FALSE);
    end;
  end;
begin
   // StatusBar.CancelText:=FALSE;
   // StatusBar.ProgressPanel:=TRUE;
  try
      // StatusBar.ProgressText:=GetLangText(11744);
    LData^.ForEach(@DoAll);
    AllCount := SelLayer^.Data^.GetCount;
      {++ Ivanoff BUG#331 BUILD#95}
      //          SelLayer.DeSelectAll(PInfo);
      {-- Ivanoff}
    Sellayer^.DeleteAll;
  finally
      // StatusBar.ProgressPanel:=FALSE;
      // StatusBar.CancelText:=TRUE;
  end;
   // UserInterface.Update([uiMenus]);
end;

{****************************************************************************************}
{ Function TLayerDlg.IndexOfLayer                                                        }
{----------------------------------------------------------------------------------------}
{ Ermittelt die Position des Layers in der Layerliste.                                   }
{----------------------------------------------------------------------------------------}
{ Layer            i = gew�nschter Layer                                                 }
{ Fixed            i = fixierte Layer ber�cksichtigen                                    }
{----------------------------------------------------------------------------------------}
{ Ergebnis: Position des Layers in der Liste oder lb_Err, wenn Layer nicht existiert.    }
{****************************************************************************************}

function TLayers.IndexOfLayer
  (
  ALayer: PLayer;
  Fixed: Boolean
  )
  : Integer;
var
  Cnt: Integer;

  function DoAll
      (
      Item: PLayer
      )
      : Boolean; far;
  begin
    if Fixed
      or not (Item^.GetState(sf_Fixed)) then
    begin
      DoAll := Item = ALayer;
      if not Item^.GetState(sf_LayerOff) then
        Inc(Cnt);
    end
    else
      DoAll := FALSE;
  end;
begin
  Cnt := -2;
  if LData^.FirstThat(@DoAll) <> nil then
    Result := Cnt
  else
    Result := lb_Err;
end;

function TLayers.InsertZLayer
  (
  AName: string;
  ACol: TColorRef;
  ALine: Word;
  APatCol: TColorRef;
  APat: Word;
  AWhere: Integer;
  ASymFill: TSymbolFill
  )
  : PLayer;
var
  ALayer: PLayer;
begin
  ALayer := New(PZLayer, Init(@Self));
  Inc(LastIndex);
  ALayer^.Index := LastIndex;
  ALayer^.SetStyle(ACol, ALine, APatCol, clNone, APat, UsrPattern, ASymFill);
  ALayer^.SetText(AName);
  if AWhere = ilTop then
    LData^.AtInsert(1, ALayer)
  else
    if AWhere = ilSecond then
      LData^.AtInsert(LData^.IndexOf(TopLayer) + 1, ALayer) {1104F}
    else
      LData^.Insert(ALayer);
  NumberLayers;
  InsertZLayer := ALayer;
  UserInterface.Update([uiLayers]);
end;

const
  RLayersOld: TStreamRec = (
    ObjType: rn_LayersOld;
    VmtLink: TypeOf(TLayers);
    Load: @TLayers.LoadOld;
    Store: @TLayers.Store);

  RLayers: TStreamRec = (
    ObjType: rn_Layers;
    VmtLink: TypeOf(TLayers);
    Load: @TLayers.Load;
    Store: @TLayers.Store);
begin
  RegisterType(RLayersOld);
  RegisterType(RLayers);
end.

