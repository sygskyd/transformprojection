unit MUOpenDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  MultiLng, StdCtrls, WinInet, ComCtrls;

type
  TMUOpenForm = class(TForm)
    MlgSection: TMlgSection;
    GroupBoxServerURL: TGroupBox;
    EditServerAddress: TEdit;
    ButtonConnect: TButton;
    GroupBoxServerProjects: TGroupBox;
    ListBoxServerProjects: TListBox;
    GroupBoxButtons: TGroupBox;
    ButtonCancel: TButton;
    ButtonOK: TButton;
    GroupBoxLocalProjects: TGroupBox;
    ListBoxLocalProjects: TListBox;
    procedure FormShow(Sender: TObject);
    procedure ButtonConnectClick(Sender: TObject);
    procedure EditServerAddressChange(Sender: TObject);
    procedure ButtonOKClick(Sender: TObject);
    procedure ListBoxServerProjectsMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure ListBoxServerProjectsDblClick(Sender: TObject);
    procedure ButtonCancelClick(Sender: TObject);
    procedure EditServerAddressKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ListBoxServerProjectsKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ListBoxLocalProjectsDblClick(Sender: TObject);
    procedure ListBoxLocalProjectsKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ListBoxLocalProjectsMouseDown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
  private
    procedure GetServerProjectList;
    procedure GetLocalProjectList;
    procedure EnableControls;
    procedure ShowError(s: AnsiString);
  public
    ProjectUrl: AnsiString;
  end;

var
  MUOpenForm: TMUOpenForm;

implementation

uses MUModule, ToolsLib, WinOSInfo;

{$R *.DFM}

procedure TMUOpenForm.FormShow(Sender: TObject);
begin
     EditServerAddress.Text:=MU.LastServerAddress;
     GetLocalProjectList;
     EnableControls;
end;

procedure TMUOpenForm.ButtonConnectClick(Sender: TObject);
begin
     ButtonConnect.Enabled:=False;
     try
        EditServerAddress.Text:=Trim(EditServerAddress.Text);
        if Pos('://',EditServerAddress.Text) <= 0 then begin
           EditServerAddress.Text:='http://' + EditServerAddress.Text;
        end;
        MU.LastServerAddress:=EditServerAddress.Text;
        GetServerProjectList;
     finally
        ButtonConnect.Enabled:=True;
     end;
end;

procedure TMUOpenForm.EditServerAddressKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
     if (Key = VK_RETURN) and (ButtonConnect.Enabled) then begin
        ButtonConnectClick(nil);
     end;
end;

procedure TMUOpenForm.ButtonOKClick(Sender: TObject);
begin
     if ListBoxServerProjects.ItemIndex > -1 then begin
        ProjectUrl:=ListBoxServerProjects.Items[ListBoxServerProjects.ItemIndex];
        ModalResult:=mrOK;
     end
     else if ListBoxLocalProjects.ItemIndex > -1 then begin
        ProjectUrl:=ListBoxLocalProjects.Items[ListBoxLocalProjects.ItemIndex];
        ModalResult:=mrOK;
     end;
end;

procedure TMUOpenForm.ButtonCancelClick(Sender: TObject);
begin
     ModalResult:=mrCancel;
end;

procedure TMUOpenForm.EnableControls;
begin
     ButtonConnect.Enabled:=(Trim(EditServerAddress.Text) <> '');
     ButtonOK.Enabled:=(ListBoxServerProjects.ItemIndex > -1) or (ListBoxLocalProjects.ItemIndex > -1);
end;

procedure TMUOpenForm.EditServerAddressChange(Sender: TObject);
begin
     EnableControls;
end;

procedure TMUOpenForm.ListBoxServerProjectsMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
     ListBoxLocalProjects.ItemIndex:=-1;
     EnableControls;
end;

procedure TMUOpenForm.ListBoxLocalProjectsMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
     ListBoxServerProjects.ItemIndex:=-1;
     EnableControls;
end;

procedure TMUOpenForm.ListBoxServerProjectsKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
     ListBoxLocalProjects.ItemIndex:=-1;
     EnableControls;
end;

procedure TMUOpenForm.ListBoxLocalProjectsKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
     ListBoxServerProjects.ItemIndex:=-1;
     EnableControls;
end;

procedure TMUOpenForm.ListBoxServerProjectsDblClick(Sender: TObject);
begin
     if ButtonOK.Enabled then begin
        ButtonOKClick(nil);
     end;
end;

procedure TMUOpenForm.ListBoxLocalProjectsDblClick(Sender: TObject);
begin
     if ButtonOK.Enabled then begin
        ButtonOKClick(nil);
     end;
end;

procedure TMUOpenForm.ShowError(s: AnsiString);
begin
     MessageBox(Handle,PChar(s),PChar(Application.Title),MB_ICONERROR or MB_APPLMODAL or MB_OK);
end;

procedure TMUOpenForm.GetServerProjectList;
var
Inet,hURL: HInternet;
s,AUrl: AnsiString;
AList: TStringList;
i: Integer;
Buffer: Array[0..4095] of Char;
BufferSize: DWORD;
begin
     AUrl:=MU.LastServerAddress;
     if AUrl = '' then begin
        exit;
     end;
     AUrl:=AUrl + '?op=getlist';
     ListBoxServerProjects.Items.Clear;
     Inet:=InternetOpen(nil,INTERNET_OPEN_TYPE_PRECONFIG,nil,nil,0);
     if Inet <> nil then begin
        hURL:=InternetOpenURL(Inet,PChar(AUrl),nil,0,INTERNET_FLAG_RELOAD,0);
        if hURL <> nil then begin
           InternetReadFile(hURL,@Buffer,Sizeof(Buffer),BufferSize);
           s:=Copy(Buffer,0,BufferSize);
           if s <> '' then begin
              AList:=StringToList(s,'|');
              try
                 if AList[0] = 'RES_OK' then begin
                    for i:=1 to AList.Count-1 do begin
                        ListBoxServerProjects.Items.Add(Trim(AList[i]));
                    end;
                 end
                 else begin
                    if Pos('RES_',AList[0]) = 1 then begin
                       ShowError(MlgSection[7] + AList[0]);
                    end
                    else begin
                       ShowError(MlgSection[7] + 'RES_ERR_SERVER_ADDRESS');
                    end;
                 end;
              finally
                 AList.Free;
              end;
           end;
           InternetCloseHandle(hURL);
        end
        else begin
           ShowError(MlgSection[7] + IntToStr(GetLastError));
        end;
        InternetCloseHandle(Inet);
     end
     else begin
        ShowError(MlgSection[7] + IntToStr(GetLastError));
     end;
end;

procedure TMUOpenForm.GetLocalProjectList;
var
ASearch: TSearchRec;
ADir: AnsiString;
ALogFileName: AnsiString;
begin
     ADir:=OSInfo.CommonAppDataDir + 'MU\';
     if FindFirst(ADir + '*.amp',faAnyFile,ASearch) = 0 then begin
        ALogFileName:=ChangeFileExt((ADir + ASearch.Name),LogFileExt);
        if FileExists(ALogFileName) then begin
           ListBoxLocalProjects.Items.Add(ADir + ASearch.Name);
        end;
        while FindNext(ASearch) = 0 do begin
           ALogFileName:=ChangeFileExt((ADir + ASearch.Name),LogFileExt);
           if FileExists(ALogFileName) then begin
              ListBoxLocalProjects.Items.Add(ADir + ASearch.Name);
           end;
        end;
        FindClose(ASearch);
     end;
end;

end.
