//****************************************************************************
// Unit AttPropertyDlg
//----------------------------------------------------------------------------
// Provides a dialog that allows to view and edit the attachment data.
// It needs that the certain Project and Object (PView) were set.
//----------------------------------------------------------------------------
// Modifications
//  26.10.2000, Y.Glukhov, creation
//****************************************************************************
Unit AttPropertyDlg;

Interface

Uses
    Windows, Messages, SysUtils,
    AM_View, AM_Def, AM_Proj, MultiLng, Objects, ExtCtrls, StdCtrls,
    Spinbtn, WCtrls, Controls, Classes, Validate;

Type
  TAttPropertyDialog   = Class(TWForm)
    Bevel1           : TBevel;
    CancelBtn        : TButton;
    MlgSection       : TMlgSection;
    OKBtn            : TButton;
    ControlPanel1: TPanel;
    ControlPanel: TPanel;
    WLabel4: TWLabel;
    WLabel3: TWLabel;
    WLabel1: TWLabel;
    WLblAttNum: TWLabel;
    WLabel2: TWLabel;
    WLabel5: TWLabel;
    WLabel6: TWLabel;
    Group1: TWGroupBox;
    ChkSelAttached: TCheckBox;
    ChkDeselObject: TCheckBox;
    ChkSelMaster: TCheckBox;
    ChkMove: TCheckBox;
    VerOffSpin: TSpinBtn;
    VerOffEdit: TEdit;
    HorOffEdit: TEdit;
    HorOffSpin: TSpinBtn;
    RadioGrpAliMaster: TRadioGroup;
    RadioGrpAliObject: TRadioGroup;
    BtnHor0: TButton;
    BtnVer0: TButton;
    valHShift: TMeasureLookupValidator;
    valVShift: TMeasureLookupValidator;

    Procedure   FormShow(Sender: TObject);
    Procedure   FormHide(Sender: TObject);
    Procedure   FormCreate(Sender: TObject);
    Procedure   FormDestroy(Sender: TObject);

    Procedure   HorOffEditExit( Sender: TObject );
    Procedure   HorOffEditChange( Sender: TObject );
    Procedure   VerOffEditExit( Sender: TObject );
    Procedure   VerOffEditChange( Sender: TObject );
    Procedure   RadioGrpAliMasterClick( Sender: TObject );
    Procedure   RadioGrpAliObjectClick( Sender: TObject );
    procedure   ChkMoveClick(Sender: TObject);
    procedure   BtnVer0Click(Sender: TObject);
    procedure   BtnHor0Click(Sender: TObject);

    procedure HideAll;

  Private
    bInitialized    : Boolean;
    HorOffset       : LongInt;
    VerOffset       : LongInt;
    NumOfAttached   : LongInt;

    Procedure   InitFormData;
    Procedure   SetFormData;
    Procedure   CalculateOffset;
    Procedure   CheckMove;

  Public
    Project         : PProj;
    CurObject       : PView;
    MstObject       : PView;
    HorMove         : LongInt;
    VerMove         : LongInt;
    HideAllFlag     : boolean;
  end;

Implementation

{$R *.DFM}

Uses Measures;
//Uses Measures, NumTools, StrTools;

Procedure TAttPropertyDialog.InitFormData;
  begin
    bInitialized:= False;
    HorMove:= 0;
    VerMove:= 0;
    RadioGrpAliMaster.ItemIndex:= -1;
    RadioGrpAliObject.ItemIndex:= -1;
    NumOfAttached:= 0;
    HorOffset:= 0;
    VerOffset:= 0;
  end;

Procedure TAttPropertyDialog.SetFormData;
  var
    Attached    : PCollection;
  begin
    InitFormData;
    Group1.Visible:= True;

  // Set the master
    MstObject:= CurObject.GetObjMaster( Project.PInfo );
    if Assigned( MstObject ) then begin
      RadioGrpAliMaster.Enabled:= True;
      RadioGrpAliObject.Enabled:= True;
      ChkSelMaster.Enabled:= True;
      HorOffEdit.Enabled:= True;
      VerOffEdit.Enabled:= True;
    // Set the alignment positions
      RadioGrpAliMaster.ItemIndex:= CurObject.AttData.AliPosOfMaster;
      RadioGrpAliObject.ItemIndex:= CurObject.AttData.AliPosOfObject;
    // Set the offset
      CalculateOffset;
    end
    else begin
      RadioGrpAliMaster.Enabled:= False;
      RadioGrpAliObject.Enabled:= False;
      ChkSelMaster.Enabled:= False;
      HorOffEdit.Enabled:= False;
      VerOffEdit.Enabled:= False;
    end;
    HorOffSpin.Enabled:= HorOffEdit.Enabled;
    VerOffSpin.Enabled:= VerOffEdit.Enabled;

  // Set the number of attached objects     
    Attached:= CurObject.GetAttachedObjs;
    if Assigned( Attached ) then NumOfAttached:= Attached.Count;
    WLblAttNum.Caption:= Format(
              MlgStringList.Strings['AttPropertyDialog',7], [NumOfAttached] );
    if NumOfAttached > 0
    then  ChkSelAttached.Enabled:= True
    else  ChkSelAttached.Enabled:= False;

  // A movement is not set yet
    ChkMove.Enabled:= False;
  end;

Procedure TAttPropertyDialog.CalculateOffset;
  begin
    HorOffset:= CurObject.ClipRect.A.X - MstObject.ClipRect.A.X + HorMove;
    VerOffset:= CurObject.ClipRect.B.Y - MstObject.ClipRect.B.Y + VerMove;
  // Correct for the master alignment postion
    case (RadioGrpAliMaster.ItemIndex div 3) of
    1:  HorOffset:= HorOffset - Trunc( MstObject.ClipRect.XSize ) div 2;
    2:  HorOffset:= HorOffset - Trunc( MstObject.ClipRect.XSize );
    end;
    case (RadioGrpAliMaster.ItemIndex mod 3) of
    1:  VerOffset:= VerOffset + Trunc( MstObject.ClipRect.YSize ) div 2;
    2:  VerOffset:= VerOffset + Trunc( MstObject.ClipRect.YSize );
    end;
  // Correct for the object alignment postion
    case (RadioGrpAliObject.ItemIndex div 3) of
    1:  HorOffset:= HorOffset + Trunc( CurObject.ClipRect.XSize ) div 2;
    2:  HorOffset:= HorOffset + Trunc( CurObject.ClipRect.XSize );
    end;
    case (RadioGrpAliObject.ItemIndex mod 3) of
    1:  VerOffset:= VerOffset - Trunc( CurObject.ClipRect.YSize ) div 2;
    2:  VerOffset:= VerOffset - Trunc( CurObject.ClipRect.YSize );
    end;
    valHShift[muDrawingUnits]:= HorOffset/100;
    valVShift[muDrawingUnits]:= VerOffset/100;
  end;

Procedure TAttPropertyDialog.CheckMove;
  begin
    if not bInitialized then Exit;
    if (HorMove <> 0) or (VerMove <> 0) then begin
      ChkMove.Enabled:= True;
      ChkMove.Checked:= True;
    end
    else begin
      ChkMove.Checked:= False;
      ChkMove.Enabled:= False;
    end;
  end;

Procedure TAttPropertyDialog.FormShow(Sender: TObject);
  begin
    if bInitialized then Exit;
    if not Assigned( Project )
    or not Assigned( CurObject ) or not Assigned( CurObject.AttData ) then begin
    // No source data
      MsgBox( Project.PInfo.HWindow, 11777, Mb_IconExclamation or Mb_OK, '' );
      ModalResult := mrCancel;
      Exit;
    end;
    SetFormData;
  // Enable check of movement etc.
    bInitialized:= True;
    if HideAllFlag=true
     then
      begin;
       HideAll;
       HideAllFlag:=true;
      end;
  end;

Procedure TAttPropertyDialog.FormHide(Sender: TObject);
  begin
    if ModalResult=mrOK then begin
      if bInitialized and Assigned( MstObject ) then begin
        if (CurObject.AttData.AliPosOfMaster <> RadioGrpAliMaster.ItemIndex)
        or (CurObject.AttData.AliPosOfObject <> RadioGrpAliObject.ItemIndex)
        then begin
        // Save changes for alignment position,
        // the ClipRect movement and other actions
        // should be processed in the calling module,
        // using the procedures of module AttachHndl
          CurObject.AttData.AliPosOfMaster:= RadioGrpAliMaster.ItemIndex;
          CurObject.AttData.AliPosOfObject:= RadioGrpAliObject.ItemIndex;
          Project.SetModified;
        end;
      end;
    end;
  end;

Procedure TAttPropertyDialog.FormCreate(Sender: TObject);
  begin
    bInitialized:= False;
    Project:= nil;
    CurObject:= nil;
    MstObject:= nil;
    HideAllFlag:=false;
  end;

Procedure TAttPropertyDialog.FormDestroy(Sender: TObject);
  begin
    bInitialized:= False;
    Project:= nil;
    CurObject:= nil;
    MstObject:= nil;
  end;

Procedure TAttPropertyDialog.HorOffEditChange(Sender: TObject);
  var
    iTest   : Integer;
  begin
    if not bInitialized then Exit;
    iTest:= Round( valHShift[muDrawingUnits] )*100;
    HorMove:= HorMove + iTest - HorOffset;
    HorOffset:= iTest;
    CheckMove;
  end;

Procedure TAttPropertyDialog.HorOffEditExit(Sender: TObject);
  begin
    if not bInitialized then Exit;
    CalculateOffset;
  end;

Procedure TAttPropertyDialog.VerOffEditChange(Sender: TObject);
  var
    iTest   : Integer;
  begin
    if not bInitialized then Exit;
    iTest:= Round( valVShift[muDrawingUnits] )*100;
    VerMove:= VerMove + iTest - VerOffset;
    VerOffset:= iTest;
    CheckMove;
  end;

Procedure TAttPropertyDialog.VerOffEditExit(Sender: TObject);
  begin
    if not bInitialized then Exit;
    CalculateOffset;
  end;

Procedure TAttPropertyDialog.RadioGrpAliMasterClick(Sender: TObject);
  begin
  // Set the offset
    CalculateOffset;
  end;

Procedure TAttPropertyDialog.RadioGrpAliObjectClick(Sender: TObject);
  begin
  // Set the offset
    CalculateOffset;
  end;

procedure TAttPropertyDialog.ChkMoveClick(Sender: TObject);
  begin
    if not bInitialized then Exit;
    if not ChkMove.Checked then begin
      HorMove:= 0;
      VerMove:= 0;
      CalculateOffset;
    end;
  end;

procedure TAttPropertyDialog.BtnHor0Click(Sender: TObject);
  begin
    valHShift[muDrawingUnits]:= 0;
  end;

procedure TAttPropertyDialog.BtnVer0Click(Sender: TObject);
  begin
    valVShift[muDrawingUnits]:= 0;
  end;

procedure  TAttPropertyDialog.HideAll;
 var i:integer;
 begin;
for i:=0 to Self.Componentcount-1 do
 if Self.components[i] is TControl then
   TControl(Self.components[i]).enabled := false;
 end;  
 end.
