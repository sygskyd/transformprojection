Unit LayerPropertyDlg;

Interface
{$IFNDEF AXDLL} // <----------------- AXDLL
Uses Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     StdCtrls,ComCtrls,Tabnotbk,WCtrls,MultiLng,MultiDlg,StyleDef,
     LineDlg,FillDlg
     {$IFNDEF WMLT}
     ,SymbolFillDlg
     {$ENDIF}
     ,AM_Proj,ProjStyle,Validate,PropCollect;

Type TLayerPropertyDialog = Class(TMultiFormDialog)
       CancelBtn        : TButton;
       Group1           : TWGroupBox;
       HSDVisibleCheck  : TCheckBox;
       MlgSection       : TMlgSection;
       NameEdit         : TEdit;
       NameVal          : TValidator;
       NoteBook         : TWTabbedNoteBook;
       OkBtn            : TButton;
       PrintCheck       : TCheckBox;
       SnapCheck        : TCheckBox;
       VisibleCheck     : TCheckBox;
       WLabel1          : TWLabel;
       Procedure   FormCloseQuery(Sender: TObject; var CanClose: Boolean);
       Procedure   FormCreate(Sender: TObject);
       Procedure   FormDestroy(Sender: TObject);
       Procedure   FormHide(Sender: TObject);
       Procedure   FormShow(Sender: TObject);
      Private
       FFillDialog      : TPatternDialog;
       FLineDialog      : TLineStyleDialog;
       FLayerStyle      : TLayerProperties;
       FOrgLayerStyle   : TLayerProperties;
       FOrgProjStyles   : TProjectStyles;
       FProjectStyles   : TProjectStyles;
       FProject         : PProj;
       {$IFNDEF WMLT}
       FSymbolFillDialog: TSymbolFillDialog;
       {$ENDIF}
      Public
       LayerList   : TList;
       Property    LayerStyle:TLayerProperties read FLayerStyle write FOrgLayerStyle;
       Property    Project:PProj read FProject write FProject;
       Property    ProjectStyles:TProjectStyles read FProjectStyles write FOrgProjStyles;
     end;
{$ENDIF} // <----------------- AXDLL
Implementation
{$IFNDEF AXDLL} // <----------------- AXDLL

{$R *.DFM}

Procedure TLayerPropertyDialog.FormCreate(Sender: TObject);
begin
  // create local objects
  FLayerStyle:=TLayerProperties.Create;
  FProjectStyles:=TProjectStyles.Create;
  // setup notebook for multi-pages
  RemoveFirstPage:=FALSE;
  DialogNotebook:=Notebook;
  // create style-dialogs and add them to the notebook
  // linestyle-dialog
  FLineDialog:=TLineStyleDialog.Create(Self);
  AddDialog(FLineDialog,FLineDialog.ControlsPanel,FLineDialog.Caption);
  // fill-dialog
  FFillDialog:=TPatternDialog.Create(Self);
  AddDialog(FFillDialog,FFillDialog.ControlsPanel,FFillDialog.Caption);
  // symbolfill-dialog
  {$IFNDEF WMLT}
  FSymbolFillDialog:=TSymbolFillDialog.Create(Self);
  AddDialog(FSymbolFillDialog,FSymbolFillDialog.ControlsPanel,FSymbolFillDialog.Caption);
  {$ENDIF}
  Notebook.PageIndex:=0;
end;

Procedure TLayerPropertyDialog.FormShow(Sender: TObject);
var AEnable        : Boolean;
begin
  if FOrgLayerStyle<>NIL then FLayerStyle.Assign(FOrgLayerStyle);
  if FOrgProjStyles<>NIL then FProjectStyles.Assign(FOrgProjStyles);
  with FLayerStyle do begin
    // setup common-page
    NameEdit.Text:=Name;
    SetChecked(VisibleCheck,Visible);
    SetChecked(SnapCheck,UseForSnap);
    SetChecked(PrintCheck,Printable);
    HSDVisibleCheck.Visible:=FProject^.TurboVector;
    SetChecked(HSDVisibleCheck,HSDVisible);
    // setup sub-pages
    AEnable:=not FProject^.TurboVector or not FProject^.AttribFix;
    FLineDialog.ProjectStyles:=FProjectStyles;
    FLineDialog.LineStyle:=LineStyle;
    LineStyle.UpdatesEnabled:=AEnable;
    FFillDialog.ProjectStyles:=FProjectStyles;
    FFillDialog.FillStyle:=FillStyle;
    FillStyle.UpdatesEnabled:=AEnable;
    {$IFNDEF WMLT}
    FSymbolFillDialog.Project:=FProject;
    FSymbolFillDialog.SymbolFill:=SymbolFill;
    {$ENDIF}
    SymbolFill.UpdatesEnabled:=AEnable;
  end;
  ActiveControl:=NameEdit;
end;

Procedure TLayerPropertyDialog.FormDestroy(Sender: TObject);
begin
  // destroy local objects
  FProjectStyles.Free;
  FLayerStyle.Free;
end;

Procedure TLayerPropertyDialog.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  CanClose:=CheckValidators(Self);
end;

procedure TLayerPropertyDialog.FormHide(Sender: TObject);

  function ExistsLayer
      (
      const AName: string
      )
      : Boolean;
  var
    Cnt: Integer;
  begin
    if LayerList <> nil then begin
       for Cnt := 0 to LayerList.Count - 1 do begin
           if AnsiCompareText(TLayerProperties(LayerList[Cnt]).Name, AName) = 0 then begin
              Result := TRUE;
              Exit;
           end;
       end;
    end;
    Result := FALSE;
  end;

begin
 {++brovak BUG N77 BUGREPORT 2000/07/08}
 if Self.ModalResult=mrOk then
 {--brovak}
  begin;

  with FLayerStyle do begin
    // setup common-page
    //if Project^.Layers^.NameToLayer(NameEdit.Text) = nil then begin
    if not ExistsLayer(NameEdit.Text) then begin
       Name:=NameEdit.Text;
    end;
    Visible:=GetChecked(VisibleCheck);
    HSDVisible:=GetChecked(HSDVisibleCheck);
    UseForSnap:=GetChecked(SnapCheck);
    Printable:=GetChecked(PrintCheck);
  end;

{++ Ivanoff Bug#92 14.08.2000}
  If SELF.FLineDialog <> NIL Then
     FProjectStyles.XLineStyles.Assign(FLineDialog.ProjectStyles.XLineStyles);
  If SELF.FFillDialog <> NIL Then
     FProjectStyles.XFillStyles.Assign(FFillDialog.ProjectStyles.XFillStyles);
{-- Ivanoff}

  if FOrgLayerStyle<>NIL then FOrgLayerStyle.Assign(FLayerStyle);
  if FOrgProjStyles<>NIL then FOrgProjStyles.Assign(FProjectStyles);
{++brovak}
  end;
{--brovak}
end;
{$ENDIF} // <----------------- AXDLL

end.
