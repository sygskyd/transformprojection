{
  Created 08-FEB-2002 by Sygsky. mailto: Sygsky@progis.ru for any questions

  Dear friends! Please put here all routines you need to work with PALETTES
  and surrounding objects. It can help to future generations of WinGIS
  programmers. Long live PROGIS :o)

   08-FEB-2002. Added SetRGBQuad and SetGrayRGBQuad functions. Sygsky.
}

unit PALUtils;

interface
uses
   Windows, SysUtils;

type
   T1BitSysPal = array[0..1] of TRGBQuad;
   P1BitSysPal = ^T1BitSysPal;

   T4BitSysPal = array[0..15] of TRGBQuad;
   P4BitSysPal = ^T4BitSysPal;

   T8BitSysPal = array[Byte] of TRGBQuad;
   P8BitSysPal = ^T8BitSysPal;

   TPALARRAY = array[0..MAxInt div SizeOf (DWORD) - 1] of DWORD;
   PPALARRAY = ^TPALARRAY;

const
   C1BitSysPal: T1BitSysPal =
      (
      (rgbBlue: 000; rgbGreen: 000; rgbRed: 000; rgbReserved: 000),
      (rgbBlue: 255; rgbGreen: 255; rgbRed: 255; rgbReserved: 000)
      );

   C4BitSysPal: T4BitSysPal =
      (
      (rgbBlue: 000; rgbGreen: 000; rgbRed: 000; rgbReserved: 000),
      (rgbBlue: 000; rgbGreen: 000; rgbRed: 128; rgbReserved: 000),
      (rgbBlue: 000; rgbGreen: 128; rgbRed: 000; rgbReserved: 000),
      (rgbBlue: 000; rgbGreen: 128; rgbRed: 128; rgbReserved: 000),
      (rgbBlue: 128; rgbGreen: 000; rgbRed: 000; rgbReserved: 000),
      (rgbBlue: 128; rgbGreen: 000; rgbRed: 128; rgbReserved: 000),
      (rgbBlue: 128; rgbGreen: 128; rgbRed: 000; rgbReserved: 000),
      (rgbBlue: 128; rgbGreen: 128; rgbRed: 128; rgbReserved: 000),
      (rgbBlue: 192; rgbGreen: 192; rgbRed: 192; rgbReserved: 000),
      (rgbBlue: 000; rgbGreen: 000; rgbRed: 255; rgbReserved: 000),
      (rgbBlue: 000; rgbGreen: 255; rgbRed: 000; rgbReserved: 000),
      (rgbBlue: 000; rgbGreen: 255; rgbRed: 255; rgbReserved: 000),
      (rgbBlue: 255; rgbGreen: 000; rgbRed: 000; rgbReserved: 000),
      (rgbBlue: 255; rgbGreen: 000; rgbRed: 255; rgbReserved: 000),
      (rgbBlue: 255; rgbGreen: 255; rgbRed: 000; rgbReserved: 000),
      (rgbBlue: 255; rgbGreen: 255; rgbRed: 255; rgbReserved: 000)
      );

function SetRGBQuad (var RGBQuad: TRGBQuad; Val: DWORD): TRGBQuad;
function SetGrayRGBQuad (var RGBQuad: TRGBQuad; Val: Byte): TRGBQuad;

procedure Set1BitSysPal (pPal: P1BitSysPal);
procedure Set4BitSysPal (pPal: P4BitSysPal);
procedure Set8BitGrayPal (pPal: P8BitSysPal);

function PalsAreEqual (PInfo1: PBitMapInfo; PInfo2: PBitMapInfo): Boolean;
function BuildPalette (PSrc: PBitMapInfo): HPALETTE;
function BuildPaletteA (PSrc: PBitMapInfo): PMaxLogPalette;

implementation

function SetRGBQuad (var RGBQuad: TRGBQuad; Val: DWORD): TRGBQuad;
{
  EAX = RGBQuad address
  EDX = TRGBQuad value
}
asm
  MOV    [EAX], EDX;
  MOV    EAX, EDX
end;

function SetGrayRGBQuad (var RGBQuad: TRGBQuad; Val: Byte): TRGBQuad;
{
  EAX = RGBQuad address
  DL  = color value for R, G and B components
}
asm
  MOV   CL, DL
  MOV   DH, DL
  SUB   DL, DL      // EDX = [?,?,Val,0]
  BSWAP EDX         // Change to Big Endian EDX = [0,Val,?,?]
  MOV   DL, CL      // EDX = [0,Val,?,Val]
  MOV   DH, CL      // EDX = [0,Val,Val,Val]
  JMP   SetRGBQuad
end;

procedure Set1BitSysPal (pPal: P1BitSysPal);
begin
   CopyMemory (pPal, @C1BitSysPal, SizeOf (C1BitSysPal));
end;

procedure Set4BitSysPal (pPal: P4BitSysPal);
begin
   CopyMemory (pPal, @C1BitSysPal, SizeOf (C4BitSysPal));
end;

procedure Set8BitGrayPal (pPal: P8BitSysPal);
//
// EAX = 1st pal array entry address
//
asm
   MOV  ECX, 255        // Counter  of loop                 Res  R  G   B
   MOV  EDX, $00FFFFFF  // Higher RGB value (RGB_WHITE) = [000,255,255,000]
@@Loop:
   MOV  [EAX + ECX*4], EDX
   SUB  EDX, $00010101  //  Decrement by 1 each byte except MSB
   LOOP @@Loop
   MOV  [EAX], EDX      // And at last fill the first entry
end;

function PalsAreEqual (PInfo1: PBitMapInfo; PInfo2: PBitMapInfo): Boolean;
var
   I: Integer;
   Cnt: Integer;
   PalArray1, PalArray2: PPALARRAY;
begin
   Result := False;
   if PInfo1.bmiHeader.biBitCount <> PInfo2.bmiHeader.biBitCount then
      Exit;
   case PInfo1.bmiHeader.biBitCount of
      1, 4, 8:
         Cnt := 1 shl PInfo1.bmiHeader.biBitCount;
   else
      begin
         Result := True; // Always equal as no palettes at ALL
         Exit;
      end;
   end;
   PalArray1 := @PInfo1.bmiColors;
   PalArray2 := @PInfo2.bmiColors;
   for I := 0 to Pred (Cnt) do
      if (PalArray1[I] and $00FFFFFF) <> (PalArray2[I] and $00FFFFFF) then
         Exit;
   Result := True;
end;

function BuildPaletteA (PSrc: PBitMapInfo): PMaxLogPalette;
begin
   Result := nil;
   if not Assigned (PSrc) then
      Exit;
   if PSrc.bmiHeader.biBitCount > 8 then
      Exit;
   Result := AllocMem (SizeOf (TMaxLogPalette));
   if not Assigned (Result) then
      Exit;
   Result.palVersion := $300;
   Result.palNumEntries := WORD (1 shl PSrc.bmiHeader.biBitCount);
   CopyMemory (@Result.palPalEntry, @PSrc.bmiColors, Result.palNumEntries * SizeOf (TRGBQuad));
end;

function BuildPalette (PSrc: PBitMapInfo): HPALETTE;
var
   pPal: PMaxLogPalette;
begin
   Result := 0;
   pPal := BuildPaletteA (PSrc);
   if Assigned (pPal) then
      try
         Result := CreatePalette (PLogPalette (pPal)^);
      finally
         FreeMem (pPal);
      end;
end;

end.

