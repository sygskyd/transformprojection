Unit FillDropDown;

Interface

Uses Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     DipGrid,ComboBtn,ProjStyle,PropCollect,StyleDef,MultiLng;

Type TFillDropDownForm = Class(TDropDownForm)
       DipList     : TDipList;
       MlgSection  : TMlgSection;
       Procedure   DipListDrawCell(Sender:TObject;Canvas:TCanvas;Index:Integer;Rect:TRect);
       Procedure   DipListMouseUp(Sender: TObject; Button: TMouseButton;
                       Shift: TShiftState; X, Y: Integer);
       Procedure   FormCreate(Sender: TObject);
       Procedure   FormDestroy(Sender: TObject);
       Procedure   FormShow(Sender: TObject);
      Private
       FOnSelectStyle   : TNotifyEvent;
       FProjStyles      : TProjectStyles;
       FStyle           : Integer;
       FStyleOptions    : TSelectOptionList;
       FXFillList       : TStringList;
       Procedure   DoSelectStyle;
       Procedure   SetStyleOptions(const Value:TSelectOptionList);
       Procedure   UpdateStylesList;
      Public
       Property    Style:Integer read FStyle write FStyle;
       Property    StyleOptions:TSelectOptionList read FStyleOptions write SetStyleOptions;
       Property    OnSelectStyle:TNotifyEvent read FOnSelectStyle write FOnSelectStyle;
       Property    ProjStyles:TProjectStyles read FProjStyles write FProjStyles;
     end;

Implementation

{$R *.DFM}

Uses AM_Def,CommonResources,NumTools,XFills,XStyles;

Const SystemFillStyles  = 8;

Procedure TFillDropDownForm.DipListDrawCell(Sender: TObject;
  Canvas: TCanvas; Index: Integer; Rect: TRect);
var AText        : String;
    ARect        : TRect;
    Item         : TCustomXFill;
    Points       : TRectCorners;
begin
  with Canvas do begin
    if Index<0 then begin
      AText:=FStyleOptions.LongNames[Abs(Index)-1];
      TextRect(Rect,CenterWidthInRect(TextWidth(AText),Rect),
          CenterHeightInRect(TextHeight(AText),Rect),AText);
    end
    else begin
      FillRect(Rect);
      ARect:=Classes.Rect(Rect.Left+DipList.RowHeight+3,
          Rect.Top,Rect.Left+80,Rect.Bottom);
      if Index<SystemFillStyles-1 then begin
        CommonListBitmaps.Draw(Canvas,Rect.Left+2,CenterHeightInRect(14,Rect),clbSystem);
        case Index+1 of
          pt_FDiagonal  : Brush.Style:=bsFDiagonal;
          pt_Cross      : Brush.Style:=bsCross;
          pt_DiagCross  : Brush.Style:=bsDiagCross;
          pt_BDiagonal  : Brush.Style:=bsBDiagonal;
          pt_Horizontal : Brush.Style:=bsHorizontal;
          pt_Vertical   : Brush.Style:=bsVertical;
          pt_Solid      : Brush.Style:=bsSolid;
        end;
        Brush.Color:=clBlack;
        SetBKColor(Handle,ColorToRGB(clWindow));
        SetBKMode(Handle,Opaque);
        Pen.Style:=psClear;
        with ARect do Rectangle(Left,Top+1,Right+1,Bottom);
        AText:=MlgSection[30+Index];
      end
      else begin                                    
        CommonListBitmaps.Draw(Canvas,Rect.Left+2,CenterHeightInRect(14,Rect),clbUser);
        Item:=TCustomXFill(FXFillList.Objects[Index-SystemFillStyles+1]);
        RectCorners(InflatedRect(ARect,0,-1),Points);
        if Item is TVectorXFill then with Item as TVectorXFill do
            XLFillPolygon(Handle,Points,4,StyleDef^.XFillDefs,StyleDef^.Count,
              0,0,5/MinDistance,0)
        else if Item is TBitmapXFill then with Item as TBitmapXFill do
            BMFillPolygon(Handle,Points,4,StyleDef(Handle)^.BitmapHandle,srcCopy,1);
        AText:=Item.Name;
      end;
      ARect:=InflatedWidth(Rect,-85,0);
      TextRect(ARect,ARect.Left,CenterHeightInRect(TextHeight(AText),ARect),AText);
    end;
  end;
end;

Procedure TFillDropDownForm.SetStyleOptions(const Value:TSelectOptionList);
begin
  FStyleOptions.Assign(Value);
end;

Procedure TFillDropDownForm.FormCreate(Sender: TObject);
begin
  FStyleOptions:=TSelectOptionList.Create();
  SetupSelectOptions(FStyleOptions,sotFillStyle,[soNone]);
  FXFillList:=TStringList.Create;
  FXFillList.Sorted:=TRUE;
  FXFillList.Duplicates:=dupAccept;
end;

Procedure TFillDropDownForm.FormDestroy(Sender: TObject);
begin
  FXFillList.Free;
end;

Procedure TFillDropDownForm.UpdateStylesList;
var Cnt            : Integer;
    XFillStyle   : TCustomXFill;
begin
  FXFillList.Clear;
  if FProjStyles<>NIL then for Cnt:=0 to FProjStyles.XFillStyles.Capacity-1 do begin
    XFillStyle:=FProjStyles.XFillStyles.Items[Cnt];
    if XFillStyle<>NIL then FXFillList.AddObject(XFillStyle.Name,XFillStyle);
  end;
end;

Procedure TFillDropDownForm.FormShow(Sender: TObject);
var AHeight        : Integer;
    XFillStyle     : TCustomXFill;
begin
  UpdateStylesList;
  with DipList do begin
    Count:=FXFillList.Count+SystemFillStyles-1;
    HeaderRows:=FStyleOptions.Count;
    AHeight:=Count Div ColCount+HeaderRows;
    if Count Mod ColCount<>0 then Inc(AHeight);
    if AHeight>8 then AHeight:=8;
    Height:=AHeight*CellHeight;
    if FStyle<0 then ItemIndex:=-FStyleOptions.IndexOfValue(FStyle)-1
    else if FStyle<lt_UserDefined then ItemIndex:=FStyle-1
    else begin
      XFillStyle:=FProjStyles.XFillStyles.NumberedStyles[FStyle];
      if XFillStyle=NIL then ItemIndex:=0
      else ItemIndex:=FXFillList.IndexOfObject(XFillStyle)+SystemFillStyles-1;
    end;
  end;
  ClientHeight:=DipList.Height+2;
end;

Procedure TFillDropDownForm.DoSelectStyle;
begin
  with DipList do if ItemIndex>=-FStyleOptions.Count then begin
    if ItemIndex<0 then FStyle:=FStyleOptions.Values[Abs(ItemIndex)-1]
    else if ItemIndex<SystemFillStyles-1 then FStyle:=ItemIndex+1
    else FStyle:=TCustomXFill(FXFillList.Objects[ItemIndex-SystemFillStyles+1]).Number;
  end;
  if Assigned(FOnSelectStyle) then OnSelectStyle(Self);
end;

Procedure TFillDropDownForm.DipListMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var Index          : Integer;
begin
  Index:=DipList.ItemAtPos(Point(X,Y),TRUE);
  if Index>=-DipList.HeaderRows then DoSelectStyle;
  Hide;
end;

end.
