{****************************************************************************}
{ Unit TOmegaPhoto                                                               }
{----------------------------------------------------------------------------}
{ Interface to handle with OmegaPhoto.DLL                                    }
{----------------------------------------------------------------------------}
{ Date of creation:02-FEB-2000.                                              }
{ Full adaptation: ??-???-2000.                                              }
{----------------------------------------------------------------------------}
{ Author: Sigolaeff Victor, PROGIS-Moscow, Russia, mailto: sygsky@poboxes.com}
{----------------------------------------------------------------------------}
{ Comments: Still under constraction !         ! Before usage, ask author!   }
{****************************************************************************}

unit TOmegaPhoto; 

interface

uses Windows, SysUtils, Classes, Forms, Registry;

const
  DLLName = 'OmegaPhoto.DLL'; { Magic name to connect }

type
  TOmegaPhotoNotifyMsg =
    (
    omInit, // Start message - reset all the data in DLL (call it only once!)
    omShow, // Command to show window (create in real!)
    omHide, // To hide window of DLL (destroy in real!)
    omSrcRasterName, // Force to open image file
    omDstRasterName, // Output file name
    omNewControlPoint, // Insert new control point (CP)
    omDeleteControlPoint, // Delete existing CP
    omChangeControlPoint, // Change position of CP
    omSelectControlPoint, // Set active CP
    omTransPars, // Transmeet of transformation parameters ?
    omTransDone // Make transformation itself
    );
  TRelPointStatus = (rsNotSet, rsPreSet, rsSet, rsTest);

const
  MsgNames: array[omInit..omTransDone] of string = (
    'omInit',
    'omShow',
    'omHide',
    'omSrcRasterName',
    'omDstRasterName',
    'omNewControlPoint',
    'omDeleteControlPoint',
    'omChangeControlPoint',
    'omSelectControlPoint',
    'omTransPars',
    'omTransDone'
    );
const
  RelStatNames: array[rsNotSet..rsTest] of string = (
    'rsNotSet',
    'rsPreSet',
    'rsSet',
    'rsTest'
    );
type
  PPoint2 = ^TPoint2;
  TPoint2 = record
    X: Double;
    Y: Double;
  end;
  PPoint2Array = ^TPoint2Array;
  TPoint2Array = array[0..1] of TPoint2;
  TRectArray = array of double;
type
  PRelPoint = ^TRelPoint;
  TRelPoint = record
    Project: TPoint2; // ���������� �����
    Image: TPoint2; // ���������� �������� ����������
    ProjectStatus: TRelPointStatus;
    ImageStatus: TRelPointStatus;
    Shift: TPoint2; // ������ p2c - c
    NameSize: Integer; // ??? Ask MLevin@chat.ru what it is:-)
    UpdateDelayed: Boolean; // Check if point has delayed update from Omega
  end;
type
  TOmegaParams = record
    Msg: Integer;
    Param1: Integer;
    Param2: Integer;
  end;
type
  EOmegaPhoto = class(Exception);

  TNotifyOmegaPhoto = function(Msg, Param1, Param2: Integer): Integer; pascal;
  TArrayOfIndex = array of Integer;
  {--------------------- MAIN CLASS DECRARATION ---------------------------------}
type
  TOmegaPhotoDLL = class(TObject)
  private
    FLibraryHandle: THandle; { handle to loaded OmegaPhoto.DLL }
    FCallBack: TNotifyOmegaPhoto; { Set by user }
    FReceivedParams: TOmegaParams; { Last received  from FCallBack }
    FCallToOmega: TNotifyOmegaPhoto; { Get from Omega }
    FSentParams: TOmegaParams; { Last sent to Omega }
    FPoints: TList;
    FImageCorners: TPoint2Array;
    FTransformId: Integer;
    FIsVisible: Boolean; { Is Omega visible on the screen }
    FInCallback: Boolean; { Is callback function still not completed }
    FSelected: Integer;
    FAlreadyInUse: Boolean;
    FSrcFileName: string;
    FDstFileName: string;

    procedure FSetTransformId(Id: Integer);
    function FGetCount: Integer;
    function FGetPointAddr(Indx: Integer): PRelPoint;
    function SendToOmega(Msg_, Param1_, Param2_: Integer): Integer;
    procedure ProjToReal(var ProjP: TRelPoint; var RealP: TRelPoint);
    procedure RealToProj(var RealP: TRelPoint; var ProjP: TRelPoint);
  public
    {    Constructor Create;}
    constructor Create;
    destructor Destroy; override;

    function IsLoaded: Boolean;
    function SetCallBack(CallBackFn: TNotifyOmegaPhoto): TNotifyOmegaPhoto;
    function Init: Integer; // Reset all parameters
    function Show: Integer;
    function Hide: Integer;
    function SetSrcImage(SrcFile: AnsiString): Integer;
    function SetDstImage(DstFile: AnsiString): Integer;
    function AddControlPoint(var RelPoint: TRelPoint): Integer;
    function UpdatePoint(Index: Integer): Boolean;
    function DeleteControlPoint(PointIndex: Integer): Integer;
    function ChangeControlPoint(PointIndex: Integer; XMove, YMove: Integer): Integer;
    function SelectControlPoint(PointIndex: Integer): Integer;
    function MakeTransformation: PPoint2Array;
    function MsgName(MsgId: TOmegaPhotoNotifyMsg): string;
    function GenUpdateDelayedArray: TArrayOfIndex;
    procedure MarkUpdated;
    procedure Stop;

    property SentParams: TOmegaParams read FSentParams;
    property ReceivedParams: TOmegaParams read FReceivedParams;
    property Count: Integer read FGetCount;
    property InCallBack: Boolean read FInCallBack;
    property TransformType: Integer read FTransformId write FSetTransformId;
    property PointAddress[Indx: Integer]: PRelPoint read FGetPointAddr;
    property SelectedIndex: Integer read FSelected;
    property ImageCorners: TPoint2Array read FImageCorners;
    property SourceImageName: string read FSrcFileName;
    property DestinationImageName: string read FDstFileName;
  published
  end;

function AlreadyInUse: Boolean; { Return counter}
implementation

uses AM_Main;

var
  ObjCounter: Integer = 0;
  MyOmega: TOmegaPhotoDLL = nil;
  { TOmegaPhotoDLL }
  { To call from OmegaPhoto}

procedure RealCornersToProj(ImageCorners: TPoint2Array);
var
  I: Integer;
begin
  if MyOmega <> nil then
    with MyOmega do
      for I := 0 to 1 do
      begin
        MyOmega.FImageCorners[I].X := FImageCorners[I].X * 100.0;
        MyOmega.FImageCorners[I].Y := FImageCorners[I].Y * 100.0;
      end;
end;

function MyCallBack(Msg, Param1, Param2: Integer): Integer; pascal;
var
  PNew: PRelPoint;
  PPoint: PRelPoint;
begin
  MyOmega.FInCallBack := True;
  try
    with MyOmega do
    begin
      FReceivedParams.Msg := Msg;
      FReceivedParams.Param1 := Param1;
      FReceivedParams.Param2 := Param2;
{---------------------------------------- omNewControlPoint -------------}
      case TOmegaPhotoNotifyMsg(Msg) of
        omNewControlPoint: { New point from Omega. Omega is the foreground window! }
          with FPoints do
          begin
            New(PPoint); { Create new pool }
            MoveMemory(PPoint, PRelPoint(Param2), SizeOf(TRelPoint)); { Copy data }
            RealToProj(PPoint^, PPoint^);
            FPoints.Add(PPoint); { Store value internally }
            Param2 := Integer(PPoint);
            PPoint.UpdateDelayed := True; { Mark to update later}
          end;
{---------------------------------------- omDeleteControlPoint ----------}
        omDeleteControlPoint:
          begin
            with FPoints do
              if Param1 <= Count then
              begin
                Dispose(Items[Param1 - 1]);
                Delete(Param1 - 1); { Delete the item from List }
              end
              else
              begin
                Result := 0;
                Exit;
              end;
          end;
{---------------------------------- omChangeControlPoint event processing -----}
        omChangeControlPoint:
          begin
            with FPoints do
              if Param1 <= Count then
              begin { Exists in list of created objs }
                PPoint := PRelPoint(Items[Param1 - 1]); { Addr. to WinGIS point }
                PNew := PRelPoint(Param2); { Addr. to Omega one }
                MoveMemory(Pointer(PPoint), Pointer(PNew), SizeOf(TRelPoint)); { Replace data }
                RealToProj(PPoint^, PPoint^);     // Convert coordinates to ppoj units
                Param2 := Integer(PPoint);
                PPoint.UpdateDelayed := True;
              end;
          end;
{--------------------------------------- omHide -------------------------------}
        omHide:
          FIsVisible := False; // Yess, Omega is going to be hidden
{--------------------------------------- omTransPars  -------------------------}
        omTransPars:
          FTransformId := Param1; // Yess, Omega set transformation type
{--------------------------------------- omTransDone  -------------------------}
        omTransDone:
          begin
            if Param1 = 0 then
            begin
              FillChar(FImageCorners, 0, SizeOf(TPoint2Array));
            end
            else
            begin
              MoveMemory(Addr(FImageCorners), Pointer(Param1), SizeOf(TPoint2Array));
              RealCornersToProj(FImageCorners);
              Param1 := Integer(Addr(FImageCorners));
            end;
          end;
{------------------------------------ omSelectControlPoint --------------------}
        omSelectControlPoint:
          FSelected := Param1 - 1; { Mark selected }
      end;

      if Assigned(FCallBack) then
        Result := FCallBack(Msg, Param1, Param2)
      else
        Result := 1; { All is OK: Die-ass-hole-oho-ho-ho }
    end;
  finally
    MyOmega.FInCallBack := False; { It MUST be a last line of this function }
  end;
end;

{----------------------------------}

function TOmegaPhotoDLL.AddControlPoint(var RelPoint: TRelPoint): Integer;
var
  P: PRelPoint;
  WP: TRelPoint;
begin
  Result := -1;
  with RelPoint do
  begin
    ProjectStatus := rsSet; { Set it always }
    ImageStatus := rsPreSet; { Let it to be preset while user  not set them manually }
  end;
  ProjToReal(RelPoint, WP);
  if SendToOmega(Integer(omNewControlPoint), 0, Integer(@WP)) <> 0 then
    with FPoints do
    begin
      New(P);
      MoveMemory(P, @RelPoint, SizeOf(TRelPoint));
      FPoints.Add(P);
      Result := Count;
    end;
end;
{----------------------------------}
//
// This function tries to update point in Omega from WinGIS. The image point state
// after it should be rsSet and in Project also rsSet accordingly.
//
function TOmegaPhotoDLL.UpdatePoint(Index: Integer): Boolean;
var
  P: PRelPoint;
  WP: TRelPoint;
begin
  Result := False;
  with FPoints do
    if (Index > Count) then
      Exit;
  P := PRelPoint(FPoints[Index]);
  ProjToReal(P^, WP);
  WP.ImageStatus := rsSet;
  if SendToOmega(Integer(omChangeControlPoint), Index+1, Integer(@WP)) <> 0 then
  begin
    P.ImageStatus := rsSet; // Update chenges in our copy as they were set succesfully in Omega also
    Result := True;
  end;
end;

{------------------------------------------------------------------------------}
{ Called from WinGIS side when user deletes point }

function TOmegaPhotoDLL.DeleteControlPoint(PointIndex: Integer): Integer;
begin
  Result := -1;
  SendToOmega(Integer(omDeleteControlPoint), PointIndex + 1, 0);
  with FPoints do
    if (PointIndex < Count) then
    begin
      Dispose(FPoints[PointIndex]); { Free memory }
      Delete(PointIndex); { Free pointer }
      Result := Count;
    end;
end;

{-----------------------------------}
{ User calls it to change WinGIS position ONLY and inform Omega about }

function TOmegaPhotoDLL.ChangeControlPoint(PointIndex: Integer; XMove, YMove: Integer): Integer;
var
  ARelPoint: PRelPoint;
  WP: TRelPoint;
begin
  Result := 0;
  if PointIndex >= Count then { Out of range }
    Exit;
  ARelPoint := PRelPoint(FPoints[PointIndex]); { Get old point value }
  with ARelPoint^ do { Store updated coordinates }
  begin
    ProjectStatus := rsSet; { From now is will be SETTTTT :-)}
    Project.X := Project.X + XMove;
    Project.Y := Project.Y + YMove;
{    ImageStatus := rsSet;}
  end;
  ProjToReal(ARelPoint^, WP);
  Result := SendToOmega(Integer(omChangeControlPoint), PointIndex + 1, Integer(@WP));
end;
{------------------------------------------------------------------------------}

constructor TOmegaPhotoDLL.Create;
var
  OmegaReg: TRegistry;
begin
  if ObjCounter <> 0 then { Already  created one time before }
  begin
    raise EOmegaPhoto.Create('TOmegaPhotoDLL.Create: The unique object already exists!');
    Exit;
  end;
  inherited Create;
  FLibraryHandle := LoadLibrary(DLLName);
  {$IFNDEF AXDLL}
  WingisMainForm.CheckLibStatus(FLibraryHandle,ExtractFileName(DLLName),'Omega Photo');
  {$ENDIF}
  if FLibraryHandle = 0 then { Not loaded }
  begin
    raise EOmegaPhoto.Create('TOmegaPhotoDLL.Create: Can''t load library "' + DLLName + '"'#13#10
      + 'Error code:' + IntToStr(GetLastError));
    Exit;
  end;
  @FCallToOmega := GetProcAddress(FLibraryHandle, 'NOTIFYOMEGAPHOTO'); { I only know it :-) }
  if @FCallToOmega = nil then
  begin
    raise EOmegaPhoto.Create('TOmegaPhotoDLL.Create: Can''t extract callback address'#13#10
      + 'Error code:' + IntToStr(GetLastError));
    Exit;
  end;
  {  SetCallBack(UserCallBack);
    FCallToOmega(Integer(omInit), Integer(@MyCallBack), 0);}
  FPoints := TList.Create;
  ObjCounter := 1;
  MyOmega := Self;
  OmegaReg := TRegistry.Create;
  if OmegaReg <> nil then
    with OmegaReg do
    try
      RootKey := HKEY_CURRENT_USER;
      if OpenKey('\Software\Omega+\OmegaPhoto4WinGIS\VS', True) then
      begin
        LazyWrite := False; { Work baddy, work }
        WriteInteger('InsertPointMenu', 0);
      end;
    finally
      CloseKey;
      Free;
    end;
end;

{--------------------------------------------------}

function TOmegaPhotoDLL.Init: Integer;
begin
  Result := SendToOmega(Integer(omInit), Integer(@MyCallBack), 0);
  FAlreadyInUse := Result <> 0;
  if Result <> 0 then
  begin
    FAlreadyInUse := TRUE;
    FSrcFileName := '';
    FDstFileName := '';
  end
  else
    FAlreadyInUse := FALSE;
end;

function TOmegaPhotoDLL.IsLoaded: Boolean; register;
asm
  MOV    EAX, FLibraryHandle; { Zero in value ?}
  JNZ    @@True;          { No, loaded}
@@True:
  MOV    AL, 1;
  RET
@@False:
  SUB   AL, AL
end;

{---------------------------------------------
  Function
  Sets:
  Returns:
----------------------------------------------}

function TOmegaPhotoDLL.MakeTransformation: PPoint2Array;
begin
  Result := PPoint2Array(Pointer(SendToOmega(Integer(omTransDone), 0, 0)));
  if Result <> nil then
  begin
    CopyMemory(@FImageCorners, Result, SizeOf(TPoint2Array));
   { Convert to project coordinates }
    RealCornersToProj(FImageCorners);
    Result := Addr(FImageCorners);
  end;
end;
{-------------------------------------------------------------------}

function TOmegaPhotoDLL.MsgName(MsgId: TOmegaPhotoNotifyMsg): string;
begin
  if (MsgId <= omTransDone) and (MsgId >= omInit) then
    Result := MsgNames[MsgId]
  else
    Result := '??? name ID:' + IntToStr(Integer(MsgId));
end;
{-----------------------------------------------------------------------}

function TOmegaPhotoDLL.SelectControlPoint(PointIndex: Integer): Integer;
begin
  Result := SendToOmega(Integer(omSelectControlPoint), PointIndex + 1, 0);
  if Result <> 0 then
    FSelected := PointIndex;
end;
{----------------------------------------------------------------------------}

function TOmegaPhotoDLL.SendToOmega(Msg_, Param1_, Param2_: Integer): Integer;
begin
  Result := 0;
  with SentParams do
  begin
    Msg := Msg_;
    Param1 := Param1_;
    Param2 := Param2_;
  end;
  if Assigned(FCallToOmega) then
  begin
    Result := FCallToOmega(Msg_, Param1_, Param2_);
  end;
end;
{------------------------------------------------------------------------------------}

function TOmegaPhotoDLL.SetCallBack(CallBackFn: TNotifyOmegaPhoto): TNotifyOmegaPhoto;
begin
  Result := FCallBack;
  FCallBack := CallBackFn;
end;
{--------------}

function TOmegaPhotoDLL.Show: Integer;
begin
  Result := SendToOmega(Integer(omShow), {Integer(WinGISMainForm)} 0, 0);
  if Result <> 0 then
    FISVisible := True;
end;
{--------------}

function TOmegaPhotoDLL.Hide: Integer;
begin
  if FIsVisible then { We can hide it! }
  begin
    Result := SendToOmega(Integer(omHide), 0, 0); { Commnad to hide }
    if Result <> 0 then
      FIsVisible := False; { Hidden now }
  end
  else
    Result := 1; { Alredy hiddenn }
end;
{-----------------------------------------------}

{function TOmegaPhotoDLL.TransmeetParams: Integer;
begin
  Result := SendToOmega(Integer(omTransPars), 0, 0);
end;}
{----------------------------------------------------------------}

function TOmegaPhotoDLL.SetDstImage(DstFile: AnsiString): Integer;
begin
  if FileExists(DstFile) then
    if not DeleteFile(DstFile) then { File exists and can't be deleted }
    begin
      Result := 0;
      Exit;
    end;
  Result := SendToOmega(Integer(omDstRasterName), Integer(PChar(DstFile)), 0);
  if Result <> 0 then
    FDstFileName := DstFile;
end;
{----------------------------------------------------------------}

function TOmegaPhotoDLL.SetSrcImage(SrcFile: AnsiString): Integer;
begin
  Result := SendToOmega(Integer(omSrcRasterName), Integer(PChar(SrcFile)), 0);
  if Result <> 0 then
    FSrcFileName := SrcFile;
end;
{-----------------------------------------}

function TOmegaPhotoDLL.FGetCount: Integer;
begin
  Result := FPoints.Count;
end;
{-----------------------------}

function AlreadyInUse: Boolean;
begin
  Result := False; { Default is not in use }
  if MyOmega = nil then Exit;
  Result := MyOmega.FAlreadyInUse; { Some OmegaPhoto is already running }
end;
{------------------------------------------------}

function TOmegaPhotoDLL.GenUpdateDelayedArray: TArrayOfIndex;
var
  I, J: Integer;
begin
  Setlength(Result, Count);
  J := 0; // Start the index
  for I := 0 to Count - 1 do // Search index
    if PRelPoint(FPoints.Items[I]).UpdateDelayed then
    begin
      Result[J] := I;
      Inc(J);
    end;
  if J = 0 then
    Result := nil
  else
    Result := Copy(Result, 0, J - 1);
end;

procedure TOmegaPhotoDLL.MarkUpdated;
var
  I: Integer;
begin
  for I := 0 to Count - 1 do
    PRelPoint(FPoints.Items[I]).UpdateDelayed := False;
end;

{-------------------------------------
  Returns new transformation type set
  or 0 if illegal number is used
 ------------------------------------}

procedure TOmegaPhotoDLL.FSetTransformId(Id: Integer);
begin
  if SendToOmega(Integer(omTransPars), Id, 0) = 1 then { Omega accepted it .-}
    FTransformId := Id;
end;

function TOmegaPhotoDLL.FGetPointAddr(Indx: Integer): PRelPoint;
begin
  if (Indx >= 0) and (Indx < FPoints.Count) then
  begin
    Result := PRelPoint(FPoints.Items[Indx]);
  end
  else
    Result := nil;
end;

procedure TOmegaPhotoDLL.Stop;
var
  I: Integer;
begin
  Hide; { Clear from screen }
  Init; { Total reset to Omega }
  { Clear all the points }
  for I := 0 to FPoints.Count - 1 do
    Dispose(FPoints[i]);
  FPoints.Clear; { Delete all the entries }
  FCallBack := nil;
  ZeroMemory(Addr(FImageCorners), SizeOf(TPoint2Array));
  FSrcFileName := '';
  FDstFileName := '';
  FAlreadyInUse := False;
end;

procedure TOmegaPhotoDLL.ProjToReal(var ProjP, RealP: TRelPoint);
begin
  MoveMemory(Addr(RealP), Addr(ProjP), SizeOf(TRelPoint));
  RealP.Project.X := ProjP.Project.X / 100.0;
  RealP.Project.Y := ProjP.Project.Y / 100.0;
end;

procedure TOmegaPhotoDLL.RealToProj(var RealP, ProjP: TRelPoint);
begin
  MoveMemory(Addr(ProjP), Addr(RealP), SizeOf(TRelPoint));
  ProjP.Project.X := RealP.Project.X * 100.0;
  ProjP.Project.Y := RealP.Project.Y * 100.0;
end;

destructor TOmegaPhotoDLL.Destroy;
begin
  if FLibraryHandle <> 0 then
  begin
    Stop; { Sygsky }
//    Application.ProcessMessages;
    FreeLibrary(FLibraryHandle);
    FLibraryHandle := 0;
    FPoints.Free;
  end;
  ObjCounter := 0;
  inherited Destroy;
end;

initialization

finalization
  if ObjCounter > 0 then
    if MyOmega <> nil then
    begin
      MyOmega.Free;
      MyOmega := nil;
    end;
end.

