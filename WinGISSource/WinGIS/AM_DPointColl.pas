{****************************************************************************}
{ Unit AM_DPointColl                                                         }
{----------------------------------------------------------------------------}
{ New collection object for point's store                                    }
{----------------------------------------------------------------------------}
{ Modifikationen                                                             }
{  07.08.2002 Fedor Cadmensky                                                }
{****************************************************************************}

unit AM_DPointColl;

interface

uses Classes, Objects, AM_Def;

type
   { TDPointCollection types }

//   PItemList = ^TItemList;
  TPointList = array of TDPoint;

   { TDPointCollection object }

  PDPointCollection = ^TDPointCollection;
  TDPointCollection = object(TOldObject)
    Items: TPointList;
    Count: Integer;
    Limit: Integer;
    Delta: SmallInt;
    constructor Init(ALimit, ADelta: Integer);
    constructor Load(S: TOldStream);
    destructor Done; virtual;
    function At(Index: Integer): PDPoint;
    procedure AtDelete(Index: Integer);
    procedure AtFree(Index: Integer);
    procedure AtInsert(Index: Integer; Item: Pointer);
    procedure AtPut(Index: Integer; Item: PDPoint);
    procedure Delete(Item: TDPoint);
    procedure DeleteAll;
    procedure Error(Code, Info: Integer); virtual;
    function FirstThat(Test: Pointer): Pointer;
    procedure ForEach(Action: Pointer);
    procedure Free(Item: TDPoint);
    procedure FreeAll;
    procedure FreeItem(Item: TDPoint); virtual;
    function GetItem(S: TOldStream): Pointer; virtual;
    function IndexOf(Item: PDPoint): Integer; virtual;
    procedure Insert(Item: PDPoint); virtual;
    function LastThat(Test: Pointer): Pointer;
    procedure Pack;
    procedure PutItem(S: TOldStream; Item: PDPoint); virtual;
    procedure SetLimit(ALimit: Integer); virtual;
    procedure Store(S: TOldStream);
  end;

implementation

{ TDPointCollection }

constructor TDPointCollection.Init(ALimit, ADelta: Integer);
begin
  TOldObject.Init;
  Items := nil;
  Count := 0;
  Limit := 0;
  Delta := 16; //ADelta;
  SetLimit(ALimit);
end;

constructor TDPointCollection.Load(S: TOldStream);
var
  C, I: Integer;
  Cnt: SmallInt;
  OldData: array[0..1] of SmallInt;
  Item: PDPoint;
begin
  S.Read(Cnt, SizeOf(Cnt));
  if Cnt <> -1 then
  begin
    Count := Cnt;
    S.Read(OldData, SizeOf(SmallInt) * 2);
    Limit := OldData[0];
    Delta := 16; //OldData[1];
  end
  else
    S.Read(Count, SizeOf(Integer) * 2 + SizeOf(SmallInt));

  if Limit = 0 then
  begin
    Limit := Count;
  end;

  Items := nil;
  C := Count;
  I := Limit;
  Count := 0;
  Limit := 0;
  SetLimit(I);
  Count := C;
//   for I := 0 to C - 1 do
  i := 0;

  while i < C do
  begin
    Item := GetItem(S);
    if Item = nil then
    begin
      Count := Count - 1;
      C := Count;
      Continue;
    end;
    AtPut(i, Item);
    Dispose(Item, Done);
    Inc(i);
  end;
end;

destructor TDPointCollection.Done;
begin
  FreeAll;
  SetLimit(0);
end;

function TDPointCollection.At(Index: Integer): PDPoint;
begin
  if Index >= Count then
    Error(coIndexError, 0)
  else
    Result := PDPoint(@Items[Index]);
end;

procedure TDPointCollection.AtDelete(Index: Integer);
begin
  if Index = -1 then
    exit;
  if Index >= Count then
    Error(coIndexError, 0)
  else
  begin
    Move(Items[Index + 1], Items[Index], (Count - Index - 1) * SizeOf(TDPoint));
    Dec(Count);
    if Count <= Limit - 16 then
      SetLimit(Count);
  end;
end;

procedure TDPointCollection.AtFree(Index: Integer);
begin
  AtDelete(Index);
end;

procedure TDPointCollection.AtInsert(Index: Integer; Item: Pointer);
begin
  if Index > Count then
    Error(coIndexError, 0)
  else
  begin
    if Count >= Limit then
      SetLimit(Limit + Delta);
    Move(Items[Index], Items[Index + 1], (Count - Index) * SizeOf(TDPoint));
    Items[Index].Init(PDPoint(Item).X, PDPoint(Item).Y);
    Inc(Count);
  end;
end;

procedure TDPointCollection.AtPut(Index: Integer; Item: PDPoint);
begin
  if Index >= Count then
    Error(coIndexError, 0)
  else
    Items[Index].Init(Item^.X, Item^.Y); //Item;
end;

procedure TDPointCollection.Delete(Item: TDPoint);
begin
  AtDelete(IndexOf(@Item));
end;

procedure TDPointCollection.DeleteAll;
begin
  Count := 0;
end;

procedure TDPointCollection.Error(Code, Info: Integer);
begin
     //RunError(212 - Code);
end;

function TDPointCollection.FirstThat(Test: Pointer): Pointer;
var
  Cnt: Integer;
  Item: Pointer;
begin
  Result := nil;
  Cnt := 0;
  while (Cnt < Count) do
  begin
    Item := Pointer(At(Cnt));
    asm
      MOV     EAX,Item
      PUSH    DWORD PTR [EBP]
      CALL    DWORD PTR Test
      ADD     ESP,4
      CMP AL,0
      JE @@1
      MOV EAX,Item
      MOV @Result,EAX
      MOV Cnt,$7FFE
    @@1: INC Cnt
    end;
  end;
end;

procedure TDPointCollection.ForEach(Action: Pointer);
var
  Cnt: Integer;
  Item: PDPoint;
  AItem: TDPoint;
begin
  for Cnt := 0 to Count - 1 do
  begin
    Item := At(Cnt);
    asm
      MOV     EAX,Item
      PUSH    DWORD PTR [EBP]
      CALL    DWORD PTR Action
      ADD     ESP,4
    end;
  end;
end;

procedure TDPointCollection.Free(Item: TDPoint);
begin
//   Delete (Item);
  FreeItem(Item);
end;

procedure TDPointCollection.FreeAll;
var
  I: Integer;
begin
  SetLimit(0);
  Count := 0;
end;

procedure TDPointCollection.FreeItem(Item: TDPoint);
begin
  if Pointer(@Item) <> nil then
    AtDelete(IndexOf(@Item));
end;

function TDPointCollection.GetItem(S: TOldStream): Pointer;
var
  Info: PStreamRec;
  ObjNumber: Word;
  Cons: Pointer;
  VMT: Pointer;
begin
{$IFDEF NEWPC}
  if ProjectVersion < 29 then
{$ENDIF}
    S.Read(ObjNumber, SizeOf(ObjNumber));
  ObjNumber := rn_DPoint;

  if S.Status <> stOK then
    Result := nil
  else
    if ObjNumber = 0 then
      Result := nil
    else
    begin
      Cons := @TDPoint.Load;
      VMT := TypeOf(TDPoint);
      asm
        MOV ECX,S
        MOV EDX,VMT
        XOR EAX,EAX
        CALL DWORD PTR Cons
        MOV @Result,EAX
      end;
//         end;
    end;
//   Last := ObjNumber;
end;

function TDPointCollection.IndexOf(Item: PDPoint): Integer;
var
  Cnt: Integer;
begin
  for Cnt := 0 to Count - 1 do
    if @Items[Cnt] = Item then
    begin
      Result := Cnt;
      Exit;
    end;
  Result := -1;
end;

procedure TDPointCollection.Insert(Item: PDPoint);
begin
  AtInsert(Count, Item);
end;

function TDPointCollection.LastThat(Test: Pointer): Pointer;
var
  Cnt: Integer;
  Item: Pointer;
begin
  Cnt := Count - 1;
  while (Cnt >= 0) do
  begin
    Item := At(Cnt);
    asm
      MOV EAX,Item
      PUSH DWORD PTR [EBP]
      CALL DWORD PTR Test
      ADD  ESP,4
      CMP AL,0
      JE @@1
      MOV Cnt,0
      MOV EAX,Item
      MOV @Result,EAX
    @@1: DEC Cnt
    end;
  end;
end;

procedure TDPointCollection.Pack;
begin
end;

procedure TDPointCollection.PutItem(S: TOldStream; Item: PDPoint);
var
  Info: PStreamRec;
  Cons: Pointer;
  ObjType: Word;
begin
{$IFDEF NEWPC}
  if ProjectVersion < 29 then
{$ENDIF}
  begin
    ObjType := rn_DPoint;
    S.Write(ObjType, SizeOf(ObjType));
  end;

  if Item <> nil then
  begin
    Cons := @TDPoint.Store;
    asm
         MOV EDX,S
         MOV EAX,Item
         CALL DWORD PTR Cons
    end;
  end;
end;

procedure TDPointCollection.SetLimit(ALimit: Integer);
var
  AItems: TPointList;
begin
  if ALimit < Count then
    ALimit := Count;
  if ALimit > MaxCollectionSize then
    ALimit := MaxCollectionSize;
{   if ALimit <> Limit then
   begin
      if ALimit = 0 then
         AItems := nil
      else
      begin}
{         GetMem (AItems, ALimit * SizeOf (Pointer));
         if (Count <> 0) and (Items <> nil) then
            Move (Items, AItems, Count * SizeOf (Pointer));}
//      end;
{      if Limit <> 0 then
         SetLength (Items, Limit);}

//         FreeMem (Items, Limit * SizeOf (Pointer));
  SetLength(Items, ALimit);
//      Items := AItems;
  Limit := ALimit;
//   end;
end;

procedure TDPointCollection.Store(S: TOldStream);

  procedure DoPutItem(P: PDPoint); far;
  begin
    PutItem(S, P);
  end;
var
  Cnt: SmallInt;
begin
  Cnt := -1;
  S.Write(Cnt, SizeOf(SmallInt));
  S.Write(Count, SizeOf(Integer) * 2 + SizeOf(SmallInt));
  ForEach(@DoPutItem);
end;

const
  RDPointCollection: TStreamRec = (
    ObjType: rn_DPointColl;
    VmtLink: TypeOf(TDPointCollection);
    Load: @TDPointCollection.Load;
    Store: @TDPointCollection.Store);

begin
  RegisterType(RDPointCollection);
end.

