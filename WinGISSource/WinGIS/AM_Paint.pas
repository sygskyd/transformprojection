{******************************************************************************+
  Unit
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
 Modifikationen
+******************************************************************************}
unit AM_Paint;

interface

uses Messages, WinTypes, WinProcs, AM_Def, SysUtils, AM_Font, Objects, Controls,
  Measures, ProjStyle, ExtCanvas, GrTools, CoordinateSystem, Dialogs;

type
  TSelectionMode = (smAdditive, smSubtractive, smXOr);

type {***********************************************************************}
     { Printing    = TRUE, wenn gerade gedruckt wird                         }
     {***********************************************************************}
  PPaint = ^TPaint;
  TPaint = object(TOldObject)
//++ Glukhov Bug#236 Build#144 14.12.00
//       bMustLoad: Boolean;
//-- Glukhov Bug#236 Build#144 14.12.00
    DrawMode: Word;
    HWindow: HWnd;
    Parent: TWinControl;
    RepaintArea: TDRect;
    WMPaint: Boolean;
    XOffset: Double;
    YOffset: Double;
    Zoom: Double;
    SysBrush: HBrush;
    SysPen: HPen;
    SysColor: LongInt;
    ActBrush: HBrush;
    ActPen: HPen;
    ArrowLen: LongInt;
    Scale: Double;
       // angle symbols are drawn with
    Angle: Double;
       // cos of symbol-angle
    CosSymAngle: Double;
       // sin of symbol-angle
    SinSymAngle: Double;
       // position symbols are drawn at
    SymbolPos: TDPoint;
       // list of symbols stored in the project
    Symbols: POldObject;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
       // list of chart view kinds stored in the project
    ChartViewKinds: POldObject;
       // list of the old business graphic object stored in the project
       // for compatibility with old project version during loading
    OldBusGraphList: POldObject;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
       // list of objects stored in the project
    Objects: POldObject;
       // list of fonts used in the project
    Fonts: PFonts;
       // pointer to the project the object belongs to
    AProj: Pointer;
       // set to true if in one of the edit-poly-modes
    EditPoly: Boolean;
    LineWidth: Integer;
       // set to true if the project is being pritnerd
    Printing: Boolean;
       // rotation of the view (counterclockwise)
    ViewRotation: Double;
       // cos of the view-rotation-angle
    CosViewRotation: Double;
       // sin f the view-rotation-angle
    SinViewRotation: Double;
       // styles of the current layer
    ActLineStyle: TLineStyle;
    ActFillStyle: TFillStyle;
    ActSymbolFill: TSymbolFill;
       // current line-width in virtual units
    ActLineWidth: Integer;
       // current scale for userdefined line-styles
    ActLineScale: Double;
       // current scale for userdefined fill-styles
    ActFillScale: Double;
    Colors256: Boolean;
    ScreenWidth: Integer;
    ScreenHeight: Integer;
    WinWidth: Integer;
       // set to true while drawing the printing-preview
    PreViewing: Boolean;
    Cursor: TCursor; { currently selected cursor }
       // default-scale of the project
    ProjectScale: Double;
       // units of the project
    ProjectUnits: TMeasureUnits;
       // factor for converting project-units to millimeters
    UnitsToMM: Double;
    SymbolDrawError: Integer; { wird von TSymbol.Draw gesetzt, falls }
                                              { das zug. Symbol nicht mehr existiert 030497Ray}
       // aborts printing if set to true
    AbortPrinting: Boolean;
       // coordinate-system of the project (0=geodatic, 1=carthesian)
    CoordinateSystem: TCoordinateSystem;
       // projection of the project (projection, date)
    ProjectionSettings: TProjectionSettings;
       // set to true if a wmpaint must redraw from the graphic, not from the cache
    FromGraphic: Boolean;
       // settings for selection
    SelectionSettings: TSelectionSettings;
       // various project settings
    VariousSettings: TVariousSettings;
       // default bitmap-settings
    BitmapSettings: TBitmapSettings;
       // default Font-settings
    FontSettings: TFontSettings;
       // default 3D-Module-settings
    ThreeDSettings: TThreeDSettings;
       // default annotation-settings
    AnnotSettings: TAnnotSettings;
       // list of xlines and xstyles
    ProjStyles: TProjectStyles;
       // rectangle objects are clipped to
    ClipRect: TRect;
       // rectangle objects must tower above to be clipped
    ClipLimits: TRect;
       // canvas for drawing objects
    ExtCanvas: TExtCanvas;
       // current selection-mode
    SelectionMode: TSelectionMode;
    SelectInside: Boolean;
       // rectangle enclosing results from last selection
    SelectionRect: TDRect;
       // scale between tru-units and display-units, normally set to 1,
       // used for print-preview to achieve correct generalisation
    DisplayScale: Double;
    DoDrawObject: Boolean;
    Projectsize: TDREct;
//++ Glukhov TextVersion Build#151 14.02.01
       // bitmap used to calculate the text size
    textBM: HBITMAP;
       // DC used to calculate the text size
    textDC: HDC;
       // Font used to calculate the text size
    textFont: HFont;
    textFontData: TLogFont;
//-- Glukhov TextVersion Build#151 14.02.01
       //Brovak Bug 164
    NowDrawingLayer: Pointer;
    IsMU: Boolean;
    MUServerAddress: AnsiString;
    MUProjectAddress: AnsiString;
    constructor Init;
    constructor InitCopy(APInfo: PPaint);
    procedure InitObject;
    constructor Load(S: TOldStream);
    destructor Done; virtual;
    destructor DoneCopy;
    procedure BeginPaint(Brush, Pen: THandle; AMode: Word; ALineWidth: Integer);
    function CalculateDispDouble(Draw: Double): Double;
    function CalculateDisp(Draw: Double): LongInt;
    function CalculateDraw(Disp: LongInt): LongInt;
    function CalculateDrawDouble(Disp: Double): Double;
    function CheckNormalDistance(Point1, Point2, Point: PDPoint; MaxDist: LongInt): Boolean;
    procedure ConvertToDisp(Pos: TDPoint; var DPos: TPoint);
    procedure ConvertToDispDouble(Pos: TDPoint; var XPos, YPos: Double); overload;
    procedure ConvertToDispDouble(var XPos, YPos: Double); overload;
    procedure ConvertToPhysRect(ARect: TDRect; var Rect: TRect);
    procedure ConvertToDraw(Pos: TPoint; var DPos: TDPoint);
    procedure ConvertToDrawDouble(Pos: TPoint; var XPos, YPos: Double); overload;
    procedure ConvertToDrawDouble(var XPos, YPos: Double); overload;
    procedure ConvertToDrawDouble(var XPos, YPos: Double; AZoom: Double); overload;
    procedure DispToVirtualRect(var Rect: TRect);
    procedure EndPaint;
    function FontsInstalled: Boolean;
    procedure FrameForRectCorners(const LowerLeft, UpperRight: TDPoint; var ARect: TDRect);
    function BorderForRectCorners(const LowerLeft, UpperRight: TDPoint): Pointer;
    function GetCut(Point1, Point2, Point: PDPoint): Boolean;
    procedure GetCurrentScreen(var Rect: TDRect); overload;
    function GetCurrentScreen: TRotRect; overload;
{$DEFINE Sygsky } {++ Sygsky 10-DEC-1999: added to support new TrLib32 functionality }
{$DEFINE SygNew } {++ Sygsky 06-JAN-2000: added to process all image loading mode }
{$IFDEF Sygsky }
    procedure GetVirtScreen(var Rect: TRect);
    procedure GetProjScreen(var Rect: TDRect);
    procedure ProjRectToWorldRect(var DrawRect: TDRect; var WorldFrame: TRect);
    procedure ProjRectToDispRect(var DrawRect: TDRect; var DispFrame: TRect); overload;
    procedure ProjRectToDispRect(var DrawRect, DispRect: TRect); overload;
    procedure GetDispScreen(var DispRect: TRect);
{$ENDIF}
    function GetDrawMode(AMode: Word): Boolean;
    function GetFont(Font: TFontData; AHeight: Integer; AAngle: Integer): THandle;
    function GetTextRatio(Font: TFontData; AText: string): Real;
//++ Glukhov Bug#114 Build#125 15.11.00
    procedure GetTextSize(Font: TFontData; Text: string; var Width, Height: LongInt);
//-- Glukhov Bug#114 Build#125 15.11.00
//++ Glukhov TextVersion Build#152 28.02.01
      // Returns the font handle, which is cashed.
      // Do NOT delete this object after its use, it is deleted automatically
    function GetFontViaCash(Font: TFontData; AHeight: Integer; AAngle: {in radians} Double): THandle;
//-- Glukhov TextVersion Build#152 28.02.01
    function LineCut(InSecond: Boolean; Line1P1, Line1P2, Line2P1, Line2P2: TDPoint; var Cut: TDPoint): Boolean;
    function LineCut2(L1P1, L1P2, L2P1, L2P2: TDPoint; var Cut: TDPoint): Boolean;
    procedure MsgToDrawPos(const X, Y: Integer; var Pos: TDPoint);
    function NormalDistance(Point1, Point2, Point: PDPoint; var NDist: Double; var CutPoint: TDPoint): Boolean;
    procedure RedrawAfterNoRedraw(ARect: TDRect);
    procedure RedrawInvalidate;
    procedure RedrawRect(ARect: TDRect);
    procedure RedrawScreen(EraseBkg: Boolean);
    procedure ResetTools;
    function ScaleDisp(Draw: LongInt): LongInt;
    function ScaleDispDouble(Draw: LongInt): Double;
    function ScaleDraw(Draw: LongInt): LongInt;
    procedure ScaleToDisp(Pos: TDPoint; var DPos: TPoint);
    procedure ScaleToDispDouble(Pos: TDPoint; var XPosition: Double; var YPosition: Double);
    procedure ScaleToDraw(var Pos: TDPoint);
    function SetTheCursor(CursorId: Integer): Integer;
    procedure SetTools(ABrush, APen: THandle);
    procedure SetScaling(AScale: Double; AAngle: Real; Pos: TDPoint);
    procedure SetupPaint(AArrow: LongInt);
    procedure SetupPaintForSymb(AArrow: LongInt);
    procedure SetupWindow(AParent: TWinControl);
    procedure SetViewRotation(const AAngle: Double);
    procedure SetZoom(AZoom: Double);
    procedure StartPaint(ADC: HDC; Area: TRect; XOfs, YOfs: Double);
    procedure StopPaint;
    procedure Store(S: TOldStream);
    function ZoomToScale(AZoom: Double): Double;
    function ScaleToZoom(AScale: Double): Double;
    procedure SetProjectUnits(AUnits: TMeasureUnits);

    procedure CurrentVirtRect(var VirtRect: TRect);

    procedure PhysRectToVirtRect(const PhysRect: TRect; var VirtRect: TRect);
    function ProjRectToWorldBorder(const DrawRect: TDRect): Pointer;
    procedure ProjPointToPhysPoint(var DrawPoint: TDPoint; var DispPoint: TPoint);
    procedure ProjPointToScreenPoint(var DrawPoint: TDPoint; var DispPoint: TPoint);
    procedure ProjPointToVirtPoint(var DrawPoint: TDPoint; var DispPoint: TPoint);
    procedure ProjPointToWorldPoint(var DrawPoint, RotPoint: TDPoint);
    procedure VirtFrameForProjRect(const DrawRect: TDRect; var DispFrame: TDRect);
    procedure VirtFrameToProjRect(const VirtFrame: TRect; var ProjRect: TDRect);
    procedure VirtPointToProjPoint(const VirtPoint: TPoint; var ProjPoint: TDPoint);
    procedure WorldFrameForProjRect(const DrawRect: TDRect; var WorldFrame: TDRect);

       // sets the current drawing-line-style
    procedure SetLineStyle(const AStyle: TLineStyle; SetActual: Boolean = TRUE);
       // sets the current drawing-fill-style
    procedure SetFillStyle(const AStyle: TFillStyle; SetActual: Boolean = TRUE);
       // sets the current drawing-symbolfill-style
    procedure SetSymbolFill(const AStyle: TSymbolFill);
       // converts a size to 1/10 millimeters
    function CalculateSize(SizeType: Integer; const Size: Double): Double;
       // function to process messages while drawing
    function KeepWindowsAlive: Boolean;
       // method to export user defined line and fill-styles to a wlf-File
    procedure ExportUserDefStylesToWlf(const Filename: string);
       // method to import user defined line and fill-styles from a wlf-File
    procedure ImportUserDefStylesFromWlf(const Filename: string);
       // method to get the number of a user defined FillStyle
    function GetUserFillStyleNumberByName(const StyleName: string): integer;
       // method to get the name of a user defined FillStyle
    function GetUserFillStyleNameByNumber(StyleNumber: integer): string;
       // method to get the number of a user defined LineStyle
    function GetUserLineStyleNumberByName(const StyleName: string): integer;
       // method to get the name of a user defined LineStyle
    function GetUserLineStyleNameByNumber(StyleNumber: integer): string;
    function ConvAngle(AAngle: Double; AMode: Boolean): Double;
    procedure SetProjectionByDialog;
    function GetObjCount: Integer;
  private
    procedure CalculateFillScale(const AStyle: TFillStyle);
    procedure CalculateLineWidth(const AStyle: TLineStyle);
  end;

implementation

uses ResDlg, AM_CPoly, AM_Text, AM_RText, AM_BuGra, AM_Proj, AM_Layer, AM_Child, Win32Def, AM_Ini, Classes,
  UserIntf, Forms, XFills, NumTools, FlatSB, XLines, StyleDef, WinOSInfo,
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
  AM_Main, AM_Obj, AxGisPro_TLB, VerMgr;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

constructor TPaint.Init;
begin
  inherited Init;
  AProj := nil;
  RepaintArea.Init;
  Symbols := nil;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
  ChartViewKinds := nil;
  OldBusGraphList := nil;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
  Objects := nil;
  Fonts := New(PFonts, Init);
  InitObject;
  Cursor := crArrow;
  with FontSettings do
  begin
    UseGlobals := TRUE;
    AllFonts := Fonts;
    ChosenFont := nil;
  end;
  NowDrawingLayer := nil;
end;

{******************************************************************************+
  Constructor TPaint.InitCopy
--------------------------------------------------------------------------------
  Initializes new PPaint and copies the the settings of
+******************************************************************************}

constructor TPaint.InitCopy(APInfo: PPaint);
begin
  Init;
  // free objects that are common to both PInfos
{++ Ivanoff BUG#555 BUILD#100}
{Fonts must be "dispose" only if AProj exists and AProj is not symbol-editor-project.}
  AProj := APInfo^.AProj;
  if (AProj <> nil) and (PProj(AProj)^.SymbolMode <> sym_SymbolMode) then
{-- Ivanoff}
    Dispose(Fonts, Done);
  ExtCanvas.Free;
  CoordinateSystem.Free;
  ProjStyles.Free;
  // set self to APInfo
  Self := APInfo^;
  // create a new ExtCanvas
  ExtCanvas := TExtCanvas.Create(nil);
  CoordinateSystem := TCoordinateSystem.Create;
  FontSettings.AllFonts := Fonts;
//++ Glukhov TextVersion Build#151 14.02.01
  textBM := 0;
  textDC := 0;
  textFont := 0;
//-- Glukhov TextVersion Build#151 14.02.01
end;

destructor TPaint.DoneCopy;
begin
  // set pointers to shared objects nil
  Symbols := nil;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
  ChartViewKinds := nil;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
  Objects := nil;
  Fonts := nil;
  FontSettings.AllFonts := nil;
  ProjStyles := nil;
//++ Glukhov TextVersion Build#151 14.02.01
  if textBM <> 0 then
    DeleteObject(textBM);
  textBM := 0;
  if textDC <> 0 then
    DeleteDC(textDC);
  textDC := 0;
  if textFont <> 0 then
    DeleteObject(textFont);
  textFont := 0;
//-- Glukhov TextVersion Build#151 14.02.01
  inherited Done;
end;

procedure TPaint.InitObject;
begin
  SymbolPos.Init(0, 0);
  SysBrush := 0;
  SysPen := 0;
  SysColor := 0;
  HWindow := 0;
  XOffset := 0;
  YOffset := 0;
  Zoom := 1.0;
  WMPaint := FALSE;
  EditPoly := FALSE;
  Printing := FALSE;
  PreViewing := FALSE;
  DrawMode := 0;
  IsMU := False;

  ViewRotation := 0.0;
  SinCos(ViewRotation, SinViewRotation, CosViewRotation);
  ActLineStyle := DefaultLineStyle;
  ActFillStyle := DefaultFillStyle;
  ActSymbolFill := DefaultSymbolFill;
  SymbolDrawError := 0; { beim Zeichnen von Symbolen trat kein Fehler auf 030497Ray }
  FromGraphic := TRUE;
  ProjStyles := TProjectStyles.Create;
  SetRect(ClipRect, -5000, 5000, 5000, -5000);
  SetRect(ClipLimits, -5000, -5000, 5000, 5000);
  ExtCanvas := TExtCanvas.Create(nil);
  ExtCanvas.MapMode := mmLoMetric;
  ExtCanvas.Cached := TRUE;
  ExtCanvas.CachedDraw := TRUE;
  ExtCanvas.DrawVertices := TRUE;
  SelectionMode := smAdditive;
  DisplayScale := 1;
  DoDrawObject := TRUE;
  ProjectScale := 1;
  ProjectUnits := muMillimeters;
  UnitsToMM := 1;
  CoordinateSystem := TCoordinateSystem.Create;
//++ Glukhov TextVersion Build#151 14.02.01
  textBM := 0;
  textDC := 0;
  textFont := 0;
//-- Glukhov TextVersion Build#151 14.02.01
  with ProjectionSettings do
  begin
    ProjectProjection := 'NONE';
    ProjectDate := 'NONE';
    ProjectionXOffset := 0;
    ProjectionYOffset := 0;
    ProjectionScale := 1;
  end;
end;

procedure TPaint.SetViewRotation(const AAngle: Double);
begin
  ViewRotation := AAngle;
  SinCos(ViewRotation, SinViewRotation, CosViewRotation);
  // set the rotation of the canvas-object, is needed for user-defined fills
  ExtCanvas.ViewRotation := ViewRotation;
//++ Glukhov Bug#236 Build#144 14.12.00
//  bMustLoad := TRUE;
//-- Glukhov Bug#236 Build#144 14.12.00
end;

destructor TPaint.Done;
begin
  if Symbols <> nil then
    Dispose(Symbols, Done);
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
  if ChartViewKinds <> nil then
    Dispose(ChartViewKinds, Done);
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
  if Objects <> nil then
    Dispose(Objects, Done);
{++ Ivanoff BUG#555 BUILD#100}
{Fonts must be "dispose" only if AProj exists and AProj is not symbol-editor-project.}
  if (AProj <> nil) and (PProj(AProj).SymbolMode <> sym_SymbolMode) then
{-- Ivanoff}
    if Fonts <> nil then
      Dispose(Fonts, Done);
  FontSettings.AllFonts := nil;
  ProjStyles.Free;
  ExtCanvas.Free;
  CoordinateSystem.Free;
//++ Glukhov TextVersion Build#151 14.02.01
  if textBM <> 0 then
    DeleteObject(textBM);
  textBM := 0;
  if textDC <> 0 then
    DeleteDC(textDC);
  textDC := 0;
  if textFont <> 0 then
    DeleteObject(textFont);
  textFont := 0;
//-- Glukhov TextVersion Build#151 14.02.01
  inherited Done;
end;

constructor TPaint.Load
  (
  S: TOldStream
  );
var
  NextObject: Word;
  Dummy: Real48;
  NotUsed: LongInt;
begin
  InitObject;
  S.Read(NotUsed, SizeOf(NotUsed));
  S.Read(NotUsed, SizeOf(NotUsed));
  S.Read(Dummy, SizeOf(Dummy));
  Symbols := Pointer(S.Get);
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
{$IFNDEF AXDLL} // <----------------- AXDLL
  PProj(WinGISMainForm.LoadingProj)^.PInfo := @Self;
{$ENDIF} // <----------------- AXDLL
  if ProjectVersion > 26 then
  begin
    ChartViewKinds := Pointer(S.Get); // Chart View Kinds loading
    OldBusGraphList := nil;
    RepaintArea.Init;
    WMPaint := FALSE;
    Fonts := Pointer(S.Get);
    FontSettings.AllFonts := Fonts;
    Objects := Pointer(S.Get);
  end
  else
  begin
    ChartViewKinds := New(PChartsLib, Init(nil));
    OldBusGraphList := New(POldBuGraList, Init);
    Fonts := nil;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
    Objects := Pointer(S.Get);
    RepaintArea.Init;
    WMPaint := FALSE;
    S.Read(NextObject, SizeOf(NextObject));
    S.Seek(S.GetPos - 2, soFromBeginning);
    if NextObject = rn_Fonts then
      Fonts := Pointer(S.Get)
    else
      Fonts := New(PFonts, Init);
    FontSettings.AllFonts := Fonts;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
  end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
end;

procedure TPaint.Store
  (
  S: TOldStream
  );
var
  Dummy: Real48;
  NotUsed: LongInt;
begin
  S.Write(NotUsed, SizeOf(NotUsed));
  S.Write(NotUsed, SizeOf(NotUsed));
  S.Write(Dummy, SizeOf(Dummy));
  S.Put(Symbols);
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
  S.Put(ChartViewKinds);
  S.Put(Fonts);
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
 {+++ Brovak Bug 541 build 168 - show progress bar while saving project}
{$IFNDEF AXDLL} // <----------------- AXDLL
  ShowProgressPut := true;
{$ELSE} // <----------------- AXDLL
  ShowProgressPut := false;
{$ENDIF} // <----------------- AXDLL
 {-- Brovak}
  S.Put(Objects);
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
//-- moved up before Objects loading
// S.Put(Fonts);
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
{$IFNDEF AXDLL} // <----------------- AXDLL
  StatusBar.ProgressPanel := false;
{$ENDIF} // <----------------- AXDLL
end;

procedure TPaint.ConvertToDispDouble
  (
  Pos: TDPoint;
  var XPos: Double;
  var YPos: Double
  );
begin
  XPos := (Pos.X * CosViewRotation - Pos.Y * SinViewRotation) * Zoom - XOffset;
  YPos := (Pos.X * SinViewRotation + Pos.Y * CosViewRotation) * Zoom - YOffset;
end;

procedure TPaint.ConvertToDispDouble(var XPos, YPos: Double);
var
  NewX: Double;
  NewY: Double;
begin
  NewX := (XPos * CosViewRotation - YPos * SinViewRotation) * Zoom - XOffset;
  NewY := (XPos * SinViewRotation + YPos * CosViewRotation) * Zoom - YOffset;
  XPos := NewX;
  YPos := NewY;
end;

procedure TPaint.ConvertToDisp
  (
  Pos: TDPoint;
  var DPos: TPoint
  );
var
  XPos: Double;
  YPos: Double;
begin
  XPos := (Pos.X * CosViewRotation - Pos.Y * SinViewRotation) * Zoom - XOffset;
  YPos := (Pos.X * SinViewRotation + Pos.Y * CosViewRotation) * Zoom - YOffset;
  DPos.X := Round(XPos); {Sygsky 20-SEP-1999 BUG#299 BUILD#91}
  DPos.Y := Round(YPos); {Sygsky 20-SEP-1999 BUG#299 BUILD#91}
end;

procedure TPaint.ScaleToDispDouble
  (
  Pos: TDPoint;
  var XPosition: Double;
  var YPosition: Double
  );
var
  AXPos: Double;
  AYPos: Double;
begin
  AXPos := Pos.X * CosSymAngle - Pos.Y * SinSymAngle;
  AYPos := Pos.X * SinSymAngle + Pos.Y * CosSymAngle;
  AXPos := Scale * AXPos + SymbolPos.X;
  AYPos := Scale * AYPos + SymbolPos.Y;
  XPosition := (AXPos * CosViewRotation - AYPos * SinViewRotation) * Zoom - XOffset;
  YPosition := (AXPos * SinViewRotation + AYPos * CosViewRotation) * Zoom - YOffset;
end;

procedure TPaint.ScaleToDisp
  (
  Pos: TDPoint;
  var DPos: TPoint
  );
var
  XPos: Double;
  YPos: Double;
  AXPos: Double;
  AYPos: Double;
begin
  AXPos := Pos.X * CosSymAngle - Pos.Y * SinSymAngle;
  AYPos := Pos.X * SinSymAngle + Pos.Y * CosSymAngle;
  AXPos := Scale * AXPos + SymbolPos.X;
  AYPos := Scale * AYPos + SymbolPos.Y;
  XPos := (AXPos * CosViewRotation - AYPos * SinViewRotation) * Zoom - XOffset;
  YPos := (AXPos * SinViewRotation + AYPos * CosViewRotation) * Zoom - YOffset;
  DPos.X := Round(XPos); {Sygsky 20-SEP-1999 BUG#299 BUILD#91}
  DPos.Y := Round(YPos); {Sygsky 20-SEP-1999 BUG#299 BUILD#91}
end;

procedure TPaint.ScaleToDraw
  (
  var Pos: TDPoint
  );
begin
  Pos.Rotate(Angle);
  Pos.X := LimitToLong(Scale * Pos.X + SymbolPos.X);
  Pos.Y := LimitToLong(Scale * Pos.Y + SymbolPos.Y);
end;

function TPaint.ScaleDisp(Draw: LongInt): LongInt;
begin
  Result := LimitToLong(Scale * Draw * Zoom);
end;

function TPaint.ScaleDispDouble(Draw: LongInt): Double;
begin
  Result := Scale * Draw * Zoom;
end;

function TPaint.ScaleDraw
  (
  Draw: LongInt
  )
  : LongInt;
begin
  ScaleDraw := LimitToLong(Scale * Draw);
end;

function TPaint.CalculateDisp
  (
  Draw: Double
  )
  : LongInt;
begin
  CalculateDisp := LimitToLong(Draw * Zoom);
end;

function TPaint.CalculateDispDouble
  (
  Draw: Double
  )
  : Double;
begin
  CalculateDispDouble := Draw * Zoom;
end;

procedure TPaint.ConvertToDraw
  (
  Pos: TPoint;
  var DPos: TDPoint
  );
var
  AXPos: Double;
  AYPos: Double;
begin
  AXPos := (Pos.X + XOffset) / Zoom;
  AYPos := (Pos.Y + YOffset) / Zoom;
  DPos.X := LimitToLong(AXPos * CosViewRotation + AYPos * SinViewRotation);
  DPos.Y := LimitToLong(-AXPos * SinViewRotation + AYPos * CosViewRotation);
end;

procedure TPaint.ConvertToDrawDouble(var XPos, YPos: Double; AZoom: Double);
var
  AXPos: Double;
  AYPos: Double;
begin
  AXPos := (XPos + XOffset) / AZoom;
  AYPos := (YPos + YOffset) / AZoom;
  XPos := AXPos * CosViewRotation + AYPos * SinViewRotation;
  YPos := -AXPos * SinViewRotation + AYPos * CosViewRotation;
end;

procedure TPaint.ConvertToDrawDouble(Pos: TPoint; var XPos, YPos: Double);
var
  AXPos: Double;
  AYPos: Double;
begin
  AXPos := (Pos.X + XOffset) / Zoom;
  AYPos := (Pos.Y + YOffset) / Zoom;
  XPos := AXPos * CosViewRotation + AYPos * SinViewRotation;
  YPos := -AXPos * SinViewRotation + AYPos * CosViewRotation;
end;

procedure TPaint.ConvertToDrawDouble(var XPos, YPos: Double);
var
  AXPos: Double;
  AYPos: Double;
begin
  AXPos := (XPos + XOffset) / Zoom;
  AYPos := (YPos + YOffset) / Zoom;
  XPos := AXPos * CosViewRotation + AYPos * SinViewRotation;
  YPos := -AXPos * SinViewRotation + AYPos * CosViewRotation;
end;

function TPaint.CalculateDraw(Disp: LongInt): LongInt;
begin
  try
    Result := Round(Disp / Zoom);
  except
//++ Glukhov SmallBug Build#125 24.11.00
    if Disp < 0 then
      Result := Low(Integer)
    else
      Result := High(Integer);
//-- Glukhov SmallBug Build#125 24.11.00
  end;
end;

function TPaint.CalculateDrawDouble(Disp: Double): Double;
begin
  Result := Disp / Zoom;
end;

{**************************************************************************************}
{ Function TPaint.GetCut                                                               }
{  Ermittelt, ob eine waagrechte Linie durch den Punkt Point mit der Linie durch die   }
{  Punkte Point1 und point2 einen Schnittpunkt besitzen. Der Schnittpunkt mu�          }
{  zwischen diesen Punkten liegen.                                                     }
{**************************************************************************************}

function TPaint.GetCut
  (
  Point1: PDPoint;
  Point2: PDPoint;
  Point: PDPoint
  )
  : Boolean;
var
  DX: Double;
  DY: Double;
  XCut: Double;
  Y1: Double;
  Y2: Double;
begin
  GetCut := FALSE;
  DX := Point2^.XDist(Point1^);
  DY := Point2^.YDist(Point1^);
  if DY <> 0 then
  begin
    if DX = 0 then
      XCut := Point1^.X
    else
      XCut := (Point^.Y - Point1^.Y * 1.0) * DX / DY + Point1^.X;
    if XCut < Point^.X then
    begin
      if Point1^.Y < Point2^.Y then
      begin
        Y1 := Point2^.Y;
        Y2 := Point1^.Y;
      end
      else
      begin
        Y1 := Point1^.Y;
        Y2 := Point2^.Y;
      end;
      if (Point^.Y <= Y1) and (Point^.Y > Y2) then
        GetCut := TRUE;
    end;
  end;
end;

{***********************************************************************************}
{ Function TPaint.NormalDistance                                                    }
{  Berechnet den Normalabstand des Punktes Point von der Linie Point1-Point2.       }
{  NDist liefert den Normalabstand, CutPoint den Schnittpunkt der Normalen mit der  }
{  Linie. Normaldistance liefert TRUE, wenn CutPoint auf der Linie liegt.           }
{***********************************************************************************}

function TPaint.NormalDistance
  (
  Point1: PDPoint;
  Point2: PDPoint;
  Point: PDPoint;
  var NDist: Double;
  var CutPoint: TDPoint
  )
  : Boolean;
var
  DX: Double;
  DY: Double;
  CutX: Double;
  CutY: Double;
  X1: Double;
  Y1: Double;
  X2: Double;
  Y2: Double;
  XPos: Double;
  YPos: Double;
  Gradient: Double;
begin
  DX := Point2^.XDist(Point1^);
  DY := Point2^.YDist(Point1^);
  XPos := Point^.XDist(Point1^);
  YPos := Point^.YDist(Point1^);
  if (DX = 0) and (DY = 0) then
  begin
    CutX := XPos;
    CutY := YPos;
  end
  else
    if DX = 0 then
    begin
      CutX := 0;
      CutY := YPos;
    end
    else
      if DY = 0 then
      begin
        CutX := XPos;
        CutY := 0;
      end
      else
      begin
        Gradient := DY / DX;
        CutX := YPos * 1 / (Gradient + 1 / Gradient) + XPos * 1 / (1 + Sqr(Gradient));
        CutY := Gradient * CutX;
      end;
  if DX < 0 then
  begin
    X1 := DX;
    X2 := 0;
  end
  else
  begin
    X1 := 0;
    X2 := DX;
  end;
  if DY < 0 then
  begin
    Y1 := DY;
    Y2 := 0;
  end
  else
  begin
    Y1 := 0;
    Y2 := DY;
  end;
  NormalDistance := FALSE;
  if CutX >= X1 then
    if CutY >= Y1 then
      if CutX <= X2 then
        if CutY <= Y2 then
          NormalDistance := TRUE;
  CutPoint.Init(LimitToLong(CutX + Point1^.X), LimitToLong(CutY + Point1^.Y));
  NDist := CutPoint.Dist(Point^);
end;

{**************************************************************************************}
{ Function TPaint.LineCut2                                                             }
{--------------------------------------------------------------------------------------}
{ Berechnet den Schnittpunkt der Linien Line1P1-Line1P2 und Line2P1-Line2P2.           }
{ Liefert FALSE, wenn die Linien parallel liegen                                       }
{**************************************************************************************}

function TPaint.LineCut2
  (
  L1P1: TDPoint;
  L1P2: TDPoint;
  L2P1: TDPoint;
  L2P2: TDPoint;
  var Cut: TDPoint
  )
  : Boolean;
var
  Grad1: Double;
  Grad2: Double;
  XCut: Double;
  YCut: Double;
  DX1: Double;
  DY1: Double;
  DX2: Double;
  DY2: Double;
  LCut: Boolean;

  procedure HandleParallel;
  begin
    if (L1P1.Dist(L2P1) = 0) or
      (L1P1.Dist(L2P2) = 0) then
      Cut := L1P1
    else
      if (L1P2.Dist(L2P1) = 0) or
        (L1P2.Dist(L2P2) = 0) then
        Cut := L1P2
      else
        LCut := FALSE;
    XCut := Cut.X;
    YCut := Cut.Y;
  end;
begin
  DX1 := L1P2.XDist(L1P1);
  DY1 := L1P2.YDist(L1P1);
  DX2 := L2P2.XDist(L2P1);
  DY2 := L2P2.YDist(L2P1);
  Cut.Init(0, 0);
  XCut := 0;
  YCut := 0;
  LCut := TRUE;
  if ((DY1 = 0) and (DY2 = 0)) or
    ((DX1 = 0) and (DX2 = 0)) then
    HandleParallel
  else
    if DX1 = 0 then
    begin
      XCut := L1P1.X;
      Grad2 := DY2 / DX2;
      YCut := Grad2 * (XCut - L2P1.X) + L2P1.Y;
    end
    else
      if DX2 = 0 then
      begin
        XCut := L2P1.X;
        Grad1 := DY1 / DX1;
        YCut := Grad1 * (XCut - L1P1.X) + L1P1.Y;
      end
      else
      begin
        Grad1 := DY1 / DX1;
        Grad2 := DY2 / DX2;
        if Grad1 = Grad2 then
          HandleParallel
        else
        begin
          XCut := (L2P1.Y - L1P1.Y - Grad2 * L2P1.X + Grad1 * L1P1.X) / (Grad1 - Grad2);
          YCut := Grad1 * (XCut - L1P1.X) + L1P1.Y;
        end;
      end;
  Cut.Init(LimitToLong(XCut), LimitToLong(YCut));
  LineCut2 := LCut;
end;

{**************************************************************************************}
{ Function TPaint.LineCut
{  Der Funktionswert ist abh�ngig von InSecond. Ist InSecond FALSE, so mu� der         }
{  Schnittpunkt nur zwischen den Punkte Line1P1 und Line1P2 liegen. Ist InSecond TRUE, }
{  so mu� der Schnittpunkt auch zwischen Line2P1 und Line2P2 liegen                    }
{**************************************************************************************}

function TPaint.LineCut
  (
  InSecond: Boolean;
  Line1P1: TDPoint;
  Line1P2: TDPoint;
  Line2P1: TDPoint;
  Line2P2: TDPoint;
  var Cut: TDPoint
  )
  : Boolean;
var
  LRect: TDRect;
  LCut: Boolean;
begin
  LCut := LineCut2(Line1P1, Line1P2, Line2P1, Line2P2, Cut);
  if LCut then
  begin
    LRect.Init;
    LRect.CorrectByPoint(Line1P1);
    LRect.CorrectByPoint(Line1P2);
    LCut := LRect.PointInside(Cut);
    if LCut and InSecond then
    begin
      LRect.Init;
      LRect.CorrectByPoint(Line2P1);
      LRect.CorrectByPoint(Line2P2);
      LCut := LRect.PointInside(Cut);
    end;
  end;
  LineCut := LCut;
end;

function TPaint.CheckNormalDistance
  (
  Point1: PDPoint;
  Point2: PDPoint;
  Point: PDPoint;
  MaxDist: LongInt
  )
  : Boolean;
var
  Dummy: TDPoint;
  Dist: Double;
begin
  CheckNormalDistance := NormalDistance(Point1, Point2, Point, Dist, Dummy) and (Dist <= MaxDist);
end;

{******************************************************************************+
  Procedure TPaint.BeginPaint
--------------------------------------------------------------------------------
  Must be called before drawing. ALineWidth gives the current line-width, set
  it to -1 if the line-width of the ext-canvas shall be used.
+******************************************************************************}

procedure TPaint.BeginPaint
  (
  Brush: THandle;
  Pen: THandle;
  AMode: Word;
  ALineWidth: Integer
  );
begin
  if not WMPaint then
    RepaintArea.Assign(-MaxLongInt, -MaxLongInt, MaxLongInt, MaxLongInt);
  SetTools(Brush, Pen);
  DrawMode := AMode;
  if ALineWidth < 0 then
    LineWidth := ActLineWidth
  else
    LineWidth := ALineWidth;
end;

procedure TPaint.StartPaint
  (
  ADC: HDC;
  Area: TRect;
  XOfs: Double;
  YOfs: Double
  );
var
  Point: TPoint;
  APoint: TGrPoint;
  BPoint: TGrPoint;
begin
  WMPaint := TRUE;
  XOffset := XOfs;
  YOffset := YOfs;
  ExtCanvas.Handle := ADC;
    // calculate repaint-area
  with Area do
  begin
    Point.X := Left;
    Point.Y := Bottom;
    ConvertToDrawDouble(Point, APoint.X, APoint.Y);
    Point.X := Right;
    Point.Y := Top;
    ConvertToDrawDouble(Point, BPoint.X, BPoint.Y);
    with ISOFrame(RotRect(APoint, BPoint, -ViewRotation)) do
    begin
      RepaintArea.A.X := LimitToLong(Left);
      RepaintArea.A.Y := LimitToLong(Bottom);
      RepaintArea.B.X := LimitToLong(Right);
      RepaintArea.B.Y := LimitToLong(Top);
    end;
  end;
end;

procedure TPaint.RedrawAfterNoRedraw
  (
  ARect: TDRect
  );
var
  Rect: TRect;
  ScreenRect: TDRect;
  PaintRect: TDRect;
begin
  GetCurrentScreen(ScreenRect);
  ARect.Intersect(ScreenRect, PaintRect);
  ConvertToPhysRect(PaintRect, Rect);
  if DoDrawObject then
  begin
    if TMDIChild(Parent).DoRedraw then
      InvalidateRect(HWindow, @Rect, TRUE)
    else
      InvalidateRect(HWindow, @Rect, FALSE);

    UpdateWindow(HWindow);
  end;
end;

procedure TPaint.StopPaint;
begin
  ResetTools;
//!?!?!? wof�r ist das gut    WMPaint:=FALSE;
end;

procedure TPaint.EndPaint;
begin
  ResetTools;
end;

procedure TPaint.SetTools
  (
  ABrush: THandle;
  APen: THandle
  );
var
  OldBrush: HBrush;
  OldPen: HPen;
  PenInfo: TLogPen;
begin
  if ABrush <> 0 then
  begin
    OldBrush := SelectObject(ExtCanvas.WindowDC, ABrush);
    if SysBrush = 0 then
      SysBrush := OldBrush;
  end;
  if APen <> 0 then
  begin
    OldPen := SelectObject(ExtCanvas.WindowDC, APen);
    GetObject(APen, SizeOf(TLogPen), @PenInfo);
    SetTextColor(ExtCanvas.Handle, PenInfo.lopnColor);
    if SysPen = 0 then
      SysPen := OldPen;
  end;
  ActPen := APen;
  ActBrush := ABrush;
end;

procedure TPaint.ResetTools;
begin
  if SysBrush <> 0 then
  begin
    SelectObject(ExtCanvas.Handle, SysBrush);
    ActBrush := SysBrush;
  end;
  if SysPen <> 0 then
  begin
    SelectObject(ExtCanvas.Handle, SysPen);
    ActPen := SysPen;
  end;
  SysBrush := 0;
  SysPen := 0;
end;

procedure TPaint.GetCurrentScreen(var Rect: TDRect);
begin
  GetProjScreen(Rect);
end;

function TPaint.GetCurrentScreen: TRotRect;
var
  Rect: TGrRect;
  DRect: TRect;
begin
  // get the screen-size in virtual units
  GetClientRect(HWindow, DRect);
  DPToLP(ExtCanvas.Handle, DRect, 2);
  Rect := GrRect(DRect.Left, DRect.Bottom, DRect.Right, DRect.Top);
  // convert the screen-corners to project coordinates
  ConvertToDrawDouble(Rect.Left, Rect.Bottom);
  ConvertToDrawDouble(Rect.Right, Rect.Top);
  // calculate a rectangle with the screen-corners and the current view rotation
  Result := RotRect(Rect.BottomLeft, Rect.TopRight, -ViewRotation);
end;

(*
// this way of calculation returns wrong results e.g. for views!  (99/04/13 Heinz)
var
    DRect          : TRect;
    DrawPoint      : TGrPoint;
begin
  GetClientRect(HWindow,DRect);             { Physical rect }
  DPToLP(ExtCanvas.Handle,DRect,2);         { Virtual rect. }
  ConvertToDrawDouble(DRect.TopLeft,DrawPoint.X,DrawPoint.Y); { Left bottom point to Draw }
  Result:=RotRect(DrawPoint,
                  CalculateDrawDouble(DRect.BottomRight.x-DRect.BottomRight.x+1),
                  CalculateDrawDouble(DRect.BottomRight.y-DRect.BottomRight.y+1),
                  -ViewRotation);
end;
*)

procedure TPaint.RedrawScreen
  (
  EraseBkg: Boolean
  );
begin
  if HWindow <> 0 then
  begin
    InvalidateRect(HWindow, nil, EraseBkg);
  end;
  FromGraphic := TRUE;
end;

procedure TPaint.RedrawInvalidate;
begin
  if HWindow <> 0 then
  begin
    UpdateWindow(HWindow);
  end;
end;

procedure TPaint.ConvertToPhysRect
  (
  ARect: TDRect;
  var Rect: TRect
  );
var
  BRect: TDRect;

  procedure Validate(var R: TRect);
  begin
    with R do
    begin
      if (Left < 0) or (Left > ExtCanvas.PhysicalSize.Right) then
        Left := 0;
      if (Top < 0) or (Top > ExtCanvas.PhysicalSize.Bottom) then
        Top := 0;
      if (Right < 0) or (Right > ExtCanvas.PhysicalSize.Right) then
        Right := ExtCanvas.PhysicalSize.Right;
      if (Bottom < 0) or (Bottom > ExtCanvas.PhysicalSize.Bottom) then
        Bottom := ExtCanvas.PhysicalSize.Bottom;
    end;
  end;

begin
  VirtFrameForProjRect(ARect, BRect);
  Rect.Left := BRect.A.X;
  Rect.Bottom := BRect.A.Y;
  Rect.Right := BRect.B.X;
  Rect.Top := BRect.B.Y;
  LPToDP(ExtCanvas.Handle, Rect, 2);
  Validate(Rect);
end;

function TPaint.SetTheCursor(CursorId: Integer): Integer;
begin
  Result := Cursor;
  if (CursorID > 0) and (Screen.Cursors[CursorID] = Screen.Cursors[crDefault]) then
    Screen.Cursors[CursorID] := LoadCursor(hInstance, MakeIntResource(CursorID));
  if CursorID = crHourGlass then
    Screen.Cursor := crHourGlass
  else
  begin
    Screen.Cursor := crDefault;
    if CursorID = crDefault then
      CursorID := Cursor;
    Cursor := CursorID;
  end;
  UserInterface.Update([uiCursor]);
end;

function TPaint.GetFont(Font: TFontData; AHeight: Integer; AAngle: Integer): THandle;
var
  FStyle: Integer;
  BFont: PFontDes;
  FName: string;
  Quality: Byte;
begin
  BFont := Fonts^.GetFont(Font.Font);
  if BFont <> nil then
    with Font do
    begin
      if BFont^.Installed then
      begin
        FName := BFont^.Name^ + #0;
        if (Style and ts_Bold) > 0 then
          FStyle := fw_Bold
        else
          FStyle := fw_Normal;
        Quality := Out_Default_Precis;
        Result := CreateFont(AHeight, 0, AAngle, AAngle, FStyle, Byte((Style and ts_Italic) > 0),
          Byte((Style and ts_Underl) > 0), 0, BFont^.CharSet, Quality,
          Clip_Default_Precis or Clip_LH_Angles, Proof_Quality, BFont^.Family, @FName[1]);
      end
      else
        Result := 0;
    end
  else
  begin
    Result := 0;
  end;
end;

//++ Glukhov TextVersion Build#152 28.02.01
// Returns the font handle, which can be cashed.
// Do NOT delete this object after its use, it is deleted here or in destructor

function TPaint.GetFontViaCash(Font: TFontData; AHeight: Integer; AAngle: Double // in radians
  ): THandle;
var
  dscFont: PFontDes;
  newFontData: TLogFont;
  newFont: HFont;
begin
    // Set initial value for Result
  Result := 0;
  dscFont := Fonts^.GetFont(Font.Font);
  if not Assigned(dscFont) then
    Exit;
//++ Glukhov Bug#581 Build#166 23.11.01
// Commented to enable the uninstalled font, it is necessary for W95/98
//    if not dscFont^.Installed  then Exit;
//-- Glukhov Bug#581 Build#166 23.11.01
    // Prepare font data
  newFontData.lfHeight := AHeight;
  newFontData.lfWidth := 0;
  newFontData.lfEscapement := -Round(AAngle * 1800.0 / Pi);
  newFontData.lfOrientation := newFontData.lfEscapement;
  if (Font.Style and ts_Bold) <> 0 then
    newFontData.lfWeight := FW_BOLD
  else
    newFontData.lfWeight := FW_NORMAL;
  newFontData.lfItalic := Byte(Font.Style and ts_Italic <> 0);
  newFontData.lfUnderline := Byte(Font.Style and ts_Underl <> 0);
    // Some default values
  newFontData.lfStrikeOut := 0;
  newFontData.lfCharSet := DEFAULT_CHARSET;
  newFontData.lfOutPrecision := OUT_DEFAULT_PRECIS;
  newFontData.lfClipPrecision := CLIP_DEFAULT_PRECIS or CLIP_LH_ANGLES;
  newFontData.lfQuality := DEFAULT_QUALITY;
  newFontData.lfPitchAndFamily := DEFAULT_PITCH;
    // Font name
  StrPCopy(newFontData.lfFaceName, dscFont.Name^);

  if textFont = 0 then
  begin
      // Create the cash font
    textFont := CreateFontIndirect(newFontData);
  end
  else
  begin
      // Compare the cashed font and new one
    if (textFontData.lfHeight <> newFontData.lfHeight)
      or (textFontData.lfWidth <> newFontData.lfWidth)
      or (textFontData.lfEscapement <> newFontData.lfEscapement)
      or (textFontData.lfOrientation <> newFontData.lfOrientation)
      or (textFontData.lfWeight <> newFontData.lfWeight)
      or (textFontData.lfItalic <> newFontData.lfItalic)
      or (textFontData.lfUnderline <> newFontData.lfUnderline)
      or (StrComp(textFontData.lfFaceName, newFontData.lfFaceName) <> 0) then
    begin
        // Create the new font
      newFont := CreateFontIndirect(newFontData);
      if newFont = 0 then
        Exit; // incorrect new font

        // Delete the old cash font
      DeleteObject(textFont);
        // Set the new cash font
      textFont := newFont;
    end;
  end;
    // Save data for the cash font
  textFontData := newFontData;
  Result := textFont;
end;
//-- Glukhov TextVersion Build#152 28.02.01

procedure TPaint.SetScaling
  (
  AScale: Double;
  AAngle: Real;
  Pos: TDPoint
  );
begin
  Scale := AScale;
  Angle := AAngle;
  SinCos(Angle, SinSymAngle, CosSymAngle);
  SymbolPos := Pos;
end;

procedure TPaint.DispToVirtualRect
  (
  var Rect: TRect
  );
var
  Swap: Integer;
begin
  DpToLP(ExtCanvas.Handle, Rect, 2);
  Swap := Rect.Top;
  Rect.Top := Rect.Bottom;
  Rect.Bottom := Swap;
end;

procedure TPaint.MsgToDrawPos
  (
  const X: Integer;
  const Y: Integer;
  var Pos: TDPoint
  );
var
  APoint: TPoint;
begin
  APoint.X := X;
  APoint.Y := Y;
  DPToLP(ExtCanvas.Handle, APoint, 1);
  ConvertToDraw(APoint, Pos);
end;

function TPaint.GetDrawMode
  (
  AMode: Word
  )
  : Boolean;
begin
  GetDrawMode := DrawMode and AMode <> 0;
end;

function TPaint.GetTextRatio
  (
  Font: TFontData;
  AText: string
  )
  : Real;
var
  AFont: HFont;
  OldFont: HFont;
  PText: array[0..255] of Char;
{++ Moskaliov BUG#544 BUILD#95 05.11.99 *1}
  TemporaryDC: HDC;
{-- Moskaliov BUG#544 BUILD#95 05.11.99 *1}
begin
  AFont := GetFont(Font, 100, 0);
{++ Moskaliov BUG#544 BUILD#95 05.11.99 *2}
{   Comments: Error BUG#544 existed because of the object ExtCanvas }
{             was not initialised yet in a moment of the project }
{             opening and attempt of replacement of fonts.}
  if ExtCanvas.Handle <> 0 then
  begin
{-- Moskaliov BUG#544 BUILD#95 05.11.99 *2}
    OldFont := SelectObject(ExtCanvas.Handle, AFont);
    GetTextRatio := LoWord(GetTextExtent(ExtCanvas.Handle, StrPCopy(PText, AText), Length(AText))) / 100;
    SelectObject(ExtCanvas.Handle, OldFont);
{++ Moskaliov BUG#544 BUILD#95 05.11.99 *3}
  end
  else
  begin
    TemporaryDC := CreateCompatibleDC(0);
    SelectObject(TemporaryDC, AFont);
    GetTextRatio := LoWord(GetTextExtent(TemporaryDC, StrPCopy(PText, AText), Length(AText))) / 100;
    DeleteDC(TemporaryDC);
  end;
{-- Moskaliov BUG#544 BUILD#95 05.11.99 *3}
  DeleteObject(AFont);
end;

//++ Glukhov TextVersion Build#151 05.03.01

procedure TPaint.GetTextSize(Font: TFontData; Text: string; var Width, Height: LongInt);
var
  newFont: HFont;
//  tmpDC     : HDC;
  tmpRgn: HRGN;
  Rect: TRect;
  dScale: Double;
  curWidth: LongInt;
  i, n, nl: Integer;
const
  cFntHt = 60;
begin
//  newFont:= GetFont( Font, cFntHt, 0 );
  newFont := GetFontViaCash(Font, cFntHt, 0.0);
  dScale := Height / cFntHt;

  Rect.Left := 0;
  Rect.Top := 0;
  Rect.Right := 0;
  Rect.Bottom := 0;

  if textDC = 0 then
    textDC := CreateCompatibleDC(ExtCanvas.Handle);
  if textBM = 0 then
  begin
    textBM := CreateCompatibleBitmap(textDC, cMaxTxtLen * cFntHt, cFntHt + 1);
    SelectObject(textDC, textBM);
  end;
  if newFont <> 0 then
    SelectObject(textDC, newFont);

// Calculate the width and number of lines
  Width := 0;
  nl := 1;
  if Text <> '' then
  begin
    i := 1;
    n := 1;
    while i <= (Length(Text) + 1) do
    begin
      if (i <= Length(Text)) and (Text[i] <> #13) and (Text[i] <> #10) then
        Inc(i)
      else
      begin
        if (i <= Length(Text)) and (Text[i] = #13) then
          Inc(nl);
        if i > n then
        begin
        // Take in account the line width
          BeginPath(textDC);
          TextOut(textDC, 0, 0, @(Text[n]), i - n);
          EndPath(textDC);
          tmpRgn := PathToRegion(textDC);
          GetRgnBox(tmpRgn, Rect);
          DeleteObject(tmpRgn);

          curWidth := LimitToLong(dScale * ((Abs(Rect.Right - Rect.Left) + (i - n) / 2)));
          if curWidth > Width then
            Width := curWidth;
        end;
        n := i + 1;
        Inc(i);
      end;
    end;
  end;
// Calculate the height
  Height := LimitToLong(nl * Height);

//  if newFont <> 0 then DeleteObject( newFont );
end;
//-- Glukhov TextVersion Build#151 05.03.01

procedure TPaint.SetupWindow
  (
  AParent: TWinControl
  );
begin
  Parent := AParent;
  HWindow := Parent.Handle;
  ExtCanvas.Handle := GetDC(HWindow);
end;

procedure TPaint.SetupPaint
  (
  AArrow: LongInt
  );

  procedure DoFont
      (
      Item: PFontDes
      ); far;
  begin
    Item^.UseCount := 0;
  end;

  procedure IncUseCount
      (
      AFont: PFontDes
      );
  begin
    if AFont <> nil then
      Inc(AFont^.UseCount);
  end;

  function CountFontText
      (
      Item: PText
      )
      : Boolean; far;
  begin
    IncUseCount(Fonts^.GetFont(Item^.Font.Font));
    Result := FALSE;
  end;

  function CountFontRText
      (
      Item: PRText
      )
      : Boolean; far;
  begin
    IncUseCount(Fonts^.GetFont(Item^.Font.Font));
    Result := FALSE;
  end;

  function CountFontBusGraph
      (
      Item: PBusGraph
      )
      : Boolean; far;
  begin
    IncUseCount(Fonts^.GetFont(Item^.SVFont.Font));
    Result := FALSE;
  end;
var
  NewFont: Integer;
  NewFontDes: PFontDes;

  function ReplaceFontText
      (
      Item: PText
      )
      : Boolean; far;
  var
    AFont: PFontDes;
  begin
    with Item^ do
    begin
      AFont := Fonts^.GetFont(Font.Font);
      if (AFont = nil)
        or (not AFont^.Installed) then
      begin
        if AFont <> nil then
          Dec(AFont^.UseCount);
        Font.Font := NewFont;
        Inc(NewFontDes^.UseCount);
        Width := LimitToLong(Font.Height * GetTextRatio(Font, PToStr(Text)));
        CalculateClipRect(@Self);
      end;
    end;
    Result := FALSE;
  end;

  function ReplaceFontRText
      (
      Item: PRText
      )
      : Boolean; far;
  var
    AFont: PFontDes;
  begin
    with Item^ do
    begin
      AFont := Fonts^.GetFont(Font.Font);
      if (AFont = nil)
        or (not AFont^.Installed) then
      begin
        if AFont <> nil then
          Dec(AFont^.UseCount);
        Font.Font := NewFont;
        Inc(NewFontDes^.UseCount);
        CalculateClipRect(@Self);
      end;
    end;
    Result := FALSE;
  end;

  function ReplaceFontBusGraph
      (
      Item: PBusGraph
      )
      : Boolean; far;
  var
    AFont: PFontDes;
  begin
    with Item^ do
    begin
      AFont := Fonts^.GetFont(SVFont.Font);
      if (AFont = nil)
        or (not AFont^.Installed) then
      begin
        if AFont <> nil then
          Dec(AFont^.UseCount);
        SVFont.Font := NewFont;
        Inc(NewFontDes^.UseCount);
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
//++ Temporarily for DEBUg
//--              CalculateClipRect(@Self);
        CalculateClipRect;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
      end;
    end;
    Result := FALSE;
  end;
begin
  PProj(AProj)^.NotUsedFonts := FALSE;
  if not Fonts^.CheckInsertFonts(HWindow) then
  begin
    Fonts^.ForEach(@DoFont); { verhindert die Anzeige von 'Texten mit nicht }
    PLayer(Objects)^.FirstObjectType(ot_Text, @CountFontText); { installierten Fonts' bei falsche Abspeicherung }
    PLayer(Objects)^.FirstObjectType(ot_RText, @CountFontRText); { durch Neuz�hlung der Verwendungen }
    PLayer(Objects)^.FirstObjectType(ot_BusGraph, @CountFontBusGraph); { durch Neuz�hlung der Verwendungen }
    if not Fonts^.CheckInsertFonts(HWindow) then
    begin
      NewFont := Fonts^.IDFromName('Arial');
      if (NewFont = 0)
        or not (Fonts^.GetFont(NewFont)^.Installed) then
        NewFont := Fonts^.IDFromName('Times New Roman');
      if (NewFont = 0)
        or not (Fonts^.GetFont(NewFont)^.Installed) then
        NewFont := 1;
      NewFontDes := Fonts^.GetFont(NewFont);
      if not IniFile^.AutoReplaceFonts
        and (MsgBox(HWindow, 2019, mb_YesNo or mb_DefButton2 or mb_IconQuestion, PToStr(NewFontDes^.Name)) <> id_Yes) then
        NewFont := 0;
      if NewFont <> 0 then
      begin
        PLayer(Objects)^.FirstObjectType(ot_Text, @ReplaceFontText);
        PLayer(Objects)^.FirstObjectType(ot_RText, @ReplaceFontRText);
        PLayer(Objects)^.FirstObjectType(ot_BusGraph, @ReplaceFontBusGraph);
      end;
    end
    else
    begin
        //if MsgBox(HWindow,2018,mb_Ok or mb_IconExclamation,'') = id_Ok then
        //PProj(AProj)^.NotUsedFonts:=TRUE;                            { f�r Frage '�nderung speichern' }
    end;
  end;
  ArrowLen := AArrow;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
  if OldBusGraphList <> nil then
    PChartsLib(ChartViewKinds)^.InitChartKinds(@Self);
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
end;

procedure TPaint.SetupPaintForSymb
  (
  AArrow: LongInt
  );
begin
  if not Fonts^.CheckInsertFonts(HWindow) then
    MsgBox(HWindow, 2013, mb_OK or mb_IconExclamation, '');
  ArrowLen := AArrow;
end;

procedure TPaint.RedrawRect
  (
  ARect: TDRect
  );
var
  Rect: TRect;
begin
//++ Glukhov ObjAttachment 22.11.00
  ARect.Grow(CalculateDraw(LineWidth));
//-- Glukhov ObjAttachment 22.11.00
  ConvertToPhysRect(ARect, Rect);
  InflateRect(Rect, 10, 10); {interessant beim Deselektieren}
  if DoDrawObject then
  begin
    if TMDIChild(Parent).DoRedraw then
      InvalidateRect(HWindow, @Rect, TRUE)
    else
      InvalidateRect(HWindow, @Rect, FALSE);
      //UpdateWindow(HWindow);
  end;
end;

function TPaint.FontsInstalled
  : Boolean;
begin
  if Fonts^.Count = 0 then
  begin
    MsgBox(HWindow, 1010, mb_Ok or mb_IconExclamation, '');
    FontsInstalled := FALSE;
  end
  else
    FontsInstalled := TRUE;
end;

procedure TPaint.SetZoom
  (
  AZoom: Double
  );
begin
  Zoom := AZoom;
  if Zoom > 100 then
    Zoom := 100
  else
    if Zoom < 1E-20 then
      Zoom := 1E-20;
end;

function TPaint.BorderForRectCorners
  (
  const LowerLeft: TDPoint;
  const UpperRight: TDPoint
  )
  : Pointer;
var
  Corners: TGrRectCorners;
  Cnt: Integer;
  APoint: TDPoint;
begin
  RectCorners(RotRect(GrPoint(LowerLeft.X, LowerLeft.Y),
    GrPoint(UpperRight.X, UpperRight.Y), -ViewRotation), Corners);
  Result := New(PCPoly, Init);
  for Cnt := 0 to 3 do
  begin
    APoint.Init(LimitToLong(Corners[Cnt].X), LimitToLong(Corners[Cnt].Y));
    PCPoly(Result)^.InsertPoint(APoint);
  end;
  PCPoly(Result)^.ClosePoly;
  PCPoly(Result)^.CalculateClipRect;
  APoint.Done;
end;

procedure TPaint.FrameForRectCorners
  (
  const LowerLeft: TDPoint;
  const UpperRight: TDPoint;
  var ARect: TDRect
  );
var
  APoint: TDPoint;
  XSize: Double;
  YSize: Double;
  DX: Double;
  DY: Double;
begin
  ARect.Init;
  APoint.Init(LowerLeft.X, LowerLeft.Y);
  ARect.CorrectByPoint(APoint);
  DX := UpperRight.X * 1.0 - LowerLeft.X * 1.0;
  DY := UpperRight.Y * 1.0 - LowerLeft.Y * 1.0;
  XSize := DX * CosViewRotation - DY * SinViewRotation;
  YSize := DY * CosViewRotation + DX * SinViewRotation;
  APoint.Move(CosViewRotation * XSize, -SinViewRotation * XSize);
  ARect.CorrectByPoint(APoint);
  APoint.Move(SinViewRotation * YSize, CosViewRotation * YSize);
  ARect.CorrectByPoint(APoint);
  APoint.Move(-CosViewRotation * XSize, SinViewRotation * XSize);
  ARect.CorrectByPoint(APoint);
  APoint.Done;
end;

{******************************************************************************}
{ Procedure TPaint.WorldFrameForProjRect                                       }
{------------------------------------------------------------------------------}
{ Autor: Martin Forst                                                          }
{------------------------------------------------------------------------------}
{ Berechnet das umgebende Rechteck f�r das ins rotierte Koordinatensystem      }
{ gedrehte Rechteck. DrawRect und WorldFrame m�ssen verschieden sein.          }
{******************************************************************************}

procedure TPaint.WorldFrameForProjRect
  (
  const DrawRect: TDRect;
  var WorldFrame: TDRect
  );
var
  APoint: TDPoint;
  XSize: Double;
  YSize: Double;
  Width: Double;
  Height: Double;
  XPos: Double;
  YPos: Double;
begin
  WorldFrame.Init;
  XSize := DrawRect.XSize;
  YSize := DrawRect.YSize;
  APoint.Init(DrawRect.A.X, DrawRect.A.Y);
  ProjPointToWorldPoint(APoint, APoint);
  Width := XSize * Abs(CosViewRotation) + YSize * Abs(SinViewRotation);
  Height := YSize * Abs(CosViewRotation) + XSize * Abs(SinViewRotation);
  XPos := APoint.X + 0.5 * (XSize * (CosViewRotation - Abs(CosViewRotation))
    - YSize * (SinViewRotation + Abs(SinViewRotation)));
  YPos := APoint.Y + 0.5 * (XSize * (SinViewRotation - Abs(SinViewRotation))
    + YSize * (CosViewRotation - Abs(CosViewRotation)));
  WorldFrame.Assign(LimitToLong(XPos), LimitToLong(YPos), LimitToLong(XPos + Width),
    LimitToLong(YPos + Height));
end;

{******************************************************************************}
{ Function TPaint.ProjRectToWorldBorder                                        }
{------------------------------------------------------------------------------}
{ Autor: Martin Forst                                                          }
{------------------------------------------------------------------------------}
{ Berechnet f�r das angegebene achsenparallele Rechteck das entsprechende      }
{ gedrehte Rechteck im rotierten Koordinatensystem.                            }
{******************************************************************************}

function TPaint.ProjRectToWorldBorder
  (
  const DrawRect: TDRect
  )
  : Pointer;
var
  APoint: TDPoint;
  XSize: Double;
  YSize: Double;
begin
  Result := New(PCPoly, Init);
  with PCPoly(Result)^ do
  begin
    APoint.Init(DrawRect.A.X, DrawRect.A.Y);
    ProjPointToWorldPoint(APoint, APoint);
    InsertPoint(APoint);
    XSize := DrawRect.XSize;
    YSize := DrawRect.YSize;
    APoint.Move(CosViewRotation * XSize, SinViewRotation * XSize);
    InsertPoint(APoint);
    APoint.Move(-SinViewRotation * YSize, CosViewRotation * YSize);
    InsertPoint(APoint);
    APoint.Move(-CosViewRotation * XSize, -SinViewRotation * XSize);
    InsertPoint(APoint);
    APoint.Done;
    ClosePoly;
  end;
end;

procedure TPaint.VirtFrameForProjRect
  (
  const DrawRect: TDRect;
  var DispFrame: TDRect
  );
var
  APoint: TDPoint;
  BPoint: TPoint;
  XSize: Double;
  YSize: Double;
begin
  DispFrame.Init;
  APoint.Init(DrawRect.A.X, DrawRect.A.Y);
  ProjPointToVirtPoint(APoint, BPoint);
  XSize := DrawRect.XSize * Zoom;
  YSize := DrawRect.YSize * Zoom;
  APoint.Init(BPoint.X, BPoint.Y);
  DispFrame.CorrectByPoint(APoint);
  APoint.Move(CosViewRotation * XSize, SinViewRotation * XSize);
  DispFrame.CorrectByPoint(APoint);
  APoint.Move(-SinViewRotation * YSize, CosViewRotation * YSize);
  DispFrame.CorrectByPoint(APoint);
  APoint.Move(-CosViewRotation * XSize, -SinViewRotation * XSize);
  DispFrame.CorrectByPoint(APoint);
  APoint.Done;
end;

procedure TPaint.VirtFrameToProjRect
  (
  const VirtFrame: TRect;
  var ProjRect: TDRect
  );
var
  XPos: Double;
  YPos: Double;
  VirtWidth: Integer;
  VirtHeight: Integer;
  RectWidth: Double;
  RectHeight: Double;
begin
  VirtWidth := VirtFrame.Right - VirtFrame.Left;
  VirtHeight := VirtFrame.Top - VirtFrame.Bottom;
  RectWidth := (VirtWidth * CosViewRotation + VirtHeight * SinViewRotation) / Cos(2 * ViewRotation);
  RectHeight := (VirtHeight * CosViewRotation + VirtWidth * SinViewRotation) / Cos(2 * ViewRotation);
  ProjRect.Init;
  XPos := VirtFrame.Left - SinViewRotation * RectHeight;
  YPos := VirtFrame.Bottom;
  VirtPointToProjPoint(Point(LimitToLong(XPos), LimitToLong(YPos)), ProjRect.A);
  ProjRect.B.X := LimitToLong(ProjRect.A.X + RectWidth / Zoom);
  ProjRect.B.Y := LimitToLong(ProjRect.A.Y + RectHeight / Zoom);
end;

{******************************************************************************}
{ Procedure TPaint.ProjPointToWorldPoint                                       }
{------------------------------------------------------------------------------}
{ Autor: Martin Forst                                                          }
{------------------------------------------------------------------------------}
{ Berechnet den zu DrawPoint im gedrehten Koordinatensystem geh�renden Punkt.  }
{ DrawPoint und RotPoint d�rfen gleich sein.                                   }
{******************************************************************************}

procedure TPaint.ProjPointToWorldPoint
  (
  var DrawPoint: TDPoint;
  var RotPoint: TDPoint
  );
var
  XPos: Double;
  YPos: Double;
begin
  XPos := DrawPoint.X * CosViewRotation - DrawPoint.Y * SinViewRotation;
  YPos := DrawPoint.X * SinViewRotation + DrawPoint.Y * CosViewRotation;
  RotPoint.X := LimitToLong(XPos);
  RotPoint.Y := LimitToLong(YPos);
end;

{******************************************************************************}
{ Procedure TPaint.ProjPointToVirtPoint                                        }
{------------------------------------------------------------------------------}
{ Autor: Martin Forst                                                          }
{------------------------------------------------------------------------------}
{ Berechnet die Koordinaten des Punktes am Bildschirm in virtuellen            }
{ Koordinaten relativ zum Fensterursprung.                                     }
{******************************************************************************}

procedure TPaint.ProjPointToVirtPoint
  (
  var DrawPoint: TDPoint;
  var DispPoint: TPoint
  );
begin
  DispPoint.X := LimitToLong((DrawPoint.X * CosViewRotation - DrawPoint.Y * SinViewRotation) * Zoom - XOffset);
  DispPoint.Y := LimitToLong((DrawPoint.X * SinViewRotation + DrawPoint.Y * CosViewRotation) * Zoom - YOffset);
end;

{******************************************************************************}
{ Procedure TPaint.ProjPointToPhysPoint                                        }
{------------------------------------------------------------------------------}
{ Autor: Martin Forst                                                          }
{------------------------------------------------------------------------------}
{ Berechnet die Koordinaten des Punktes am Bildschirm in physikalischen        }
{ Koordinaten relativ zum Fensterursprung.                                     }
{******************************************************************************}

procedure TPaint.ProjPointToPhysPoint
  (
  var DrawPoint: TDPoint;
  var DispPoint: TPoint
  );
begin
  ProjPointToVirtPoint(DrawPoint, DispPoint);
  LpToDP(ExtCanvas.Handle, DispPoint, 1);
end;

{******************************************************************************}
{ Procedure TPaint.ProjPointToScreenPoint                                      }
{------------------------------------------------------------------------------}
{ Autor: Martin Forst                                                          }
{------------------------------------------------------------------------------}
{ Berechnet die Koordinaten des Punktes am Bildschirm in physikalischen        }
{ Koordinaten.                                                                 }
{******************************************************************************}

procedure TPaint.ProjPointToScreenPoint
  (
  var DrawPoint: TDPoint;
  var DispPoint: TPoint
  );
begin
  ProjPointToPhysPoint(DrawPoint, DispPoint);
  ClientToScreen(HWindow, DispPoint);
end;

{******************************************************************************}
{ Procedure TPaint.PhysRectToVirtRect                                          }
{------------------------------------------------------------------------------}
{ Autor: Martin Forst                                                          }
{------------------------------------------------------------------------------}
{ Rechnet ein Rechteck von den physikalischen Fensterkoordinaten in virtuelle  }
{ Fenster-Koordinaten um.                                                      }
{******************************************************************************}

procedure TPaint.PhysRectToVirtRect
  (
  const PhysRect: TRect;
  var VirtRect: TRect
  );
begin
  VirtRect := PhysRect;
  DpToLP(ExtCanvas.Handle, VirtRect, 2);
end;

procedure TPaint.VirtPointToProjPoint
  (
  const VirtPoint: TPoint;
  var ProjPoint: TDPoint
  );
var
  AXPos: Double;
  AYPos: Double;
begin
  AXPos := (VirtPoint.X + XOffset) / Zoom;
  AYPos := (VirtPoint.Y + YOffset) / Zoom;
  ProjPoint.Init(LimitToLong(AXPos * CosViewRotation + AYPos * SinViewRotation),
    LimitToLong(-AXPos * SinViewRotation + AYPos * CosViewRotation));
end;

procedure TPaint.CurrentVirtRect
  (
  var VirtRect: TRect
  );
begin
  GetClientRect(HWindow, VirtRect);
  PhysRectToVirtRect(VirtRect, VirtRect);
end;

{******************************************************************************+
  Function TPaint.ZoomToScale
--------------------------------------------------------------------------------
  Calculates the scale the project is drawn with if the specified zoom
  would be set.
+******************************************************************************}

function TPaint.ZoomToScale(AZoom: Double): Double;
begin
  Result := 10.0 * Zoom / UnitsToMM / DisplayScale;
end;

{******************************************************************************+
  Function TPaint.ScaleToZoom
--------------------------------------------------------------------------------
  Calculates the zoom needed for the given scale.
+******************************************************************************}

function TPaint.ScaleToZoom(AScale: Double): Double;
begin
  Result := AScale * UnitsToMM * DisplayScale / 10.0;
end;

procedure TPaint.SetProjectUnits
  (
  AUnits: TMeasureUnits
  );
begin
  ProjectUnits := AUnits;
  UnitsToMM := UnitsToBaseUnits(AUnits);
end;

procedure TPaint.CalculateLineWidth(const AStyle: TLineStyle);
begin
  with AStyle do
  begin
    case WidthType of
      stProjectScale:
        begin
          ActLineWidth := LimitToLong(CalculateDisp(Width * 10.0 / ProjectScale / UnitsToMM));
          ActLineScale := Scale * CalculateDispDouble(10.0 / ProjectScale / UnitsToMM)
            * GetDeviceCaps(ExtCanvas.Handle, LOGPIXELSX) / 2540.0;
        end;
      stDrawingUnits:
        begin
          ActLineWidth := LimitToLong(CalculateDisp(Width));
          ActLineScale := Scale * GetDeviceCaps(ExtCanvas.Handle, LOGPIXELSX) / 2540.0;
        end;
    else
      begin
        ActLineWidth := Round(Width);
        ActLineScale := Scale * GetDeviceCaps(ExtCanvas.Handle, LOGPIXELSX) / 2540.0;
      end;
    end;
  end;
end;

procedure TPaint.SetLineStyle(const AStyle: TLineStyle; SetActual: Boolean);
var
  XStyle: TXLineStyle;
begin
  if SetActual then
    ActLineStyle := AStyle;
  with AStyle, ExtCanvas do
  begin
    CalculateLineWidth(AStyle);
    Pen.Style := Style;
    Pen.Color := Color;
    Pen.FillColor := FillColor;
    if Style >= lt_UserDefined then
    begin
      XStyle := ProjStyles.XLineStyles[Style];
      if XStyle = nil then
        XLineStyle := nil
      else
        if (not Printing or not XStyle.GeneralizeWhenPrinting)
          and (XStyle.Generalize) and (ZoomToScale(Zoom) <= XStyle.GeneralizeScale) then
          with XStyle.GeneralizeStyle do
          begin
            if Style < 0 then
              Pen.Style := ActLineStyle.Style
            else
              Pen.Style := Style;
            CalculateLineWidth(XStyle.GeneralizeStyle);
            if Color < 0 then
              Pen.Color := ActLineStyle.Color
            else
              Pen.Color := Color;
          end
        else
          XLineStyle := XStyle.StyleDef;
    end;
    Pen.Width := ActLineWidth;
    Pen.Scale := ActLineScale;
  end;
end;

procedure TPaint.CalculateFillScale(const AStyle: TFillStyle);
begin
  if ProjectScale = 0 then
    ProjectScale := 1;
  with AStyle do
  begin
    if ScaleType = stScaleIndependend then
      ActFillScale := Scale *
        GetDeviceCaps(ExtCanvas.Handle, LOGPIXELSX) / 2540.0
    else
      ActFillScale := Scale * CalculateDispDouble(10.0 / ProjectScale / UnitsToMM)
        * GetDeviceCaps(ExtCanvas.Handle, LOGPIXELSX) / 2540.0;
  end;
end;

procedure TPaint.SetFillStyle(const AStyle: TFillStyle; SetActual: Boolean);
var
  XStyle: TCustomXFill;
begin
  if SetActual then
    ActFillStyle := AStyle;
  with AStyle do
  begin
    CalculateFillScale(AStyle);
    with ExtCanvas do
    begin
      Brush.Style := Pattern;
      Brush.ForeColor := ForeColor;
      Brush.BackColor := BackColor;
      if Pattern >= pt_UserDefined then
      begin
        XStyle := ProjStyles.XFillStyles[Pattern];
        if XStyle = nil then
          XFillStyle := nil
        else
          if (not Printing or not XStyle.GeneralizeWhenPrinting)
            and (XStyle.Generalize) and (ZoomToScale(Zoom) <= XStyle.GeneralizeScale) then
            with XStyle.GeneralizeStyle do
            begin
              CalculateFillScale(XStyle.GeneralizeStyle);
              if Pattern < 0 then
                Brush.Style := ActFillStyle.Pattern
              else
                Brush.Style := Pattern;
              if ForeColor < 0 then
                Brush.ForeColor := ActFillStyle.ForeColor
              else
                Brush.ForeColor := ForeColor;
              if BackColor < 0 then
                Brush.BackColor := ActFillStyle.BackColor
              else
                Brush.BackColor := BackColor;
            end
          else
            if XStyle is TVectorXFill then
            begin
              Brush.Style := ptVectorXFill;
              XFillStyle := TVectorXFill(XStyle).StyleDef;
            end
            else
              if XStyle is TBitmapXFill then
                with XStyle as TBitmapXFill do
                begin
                  Brush.Style := ptBitmapXFill;
                  BitmapXFill := StyleDef(ExtCanvas.Handle);
                end;
      end;
      Brush.Scale := ActFillScale;
    end;
  end;
end;

procedure TPaint.SetSymbolFill(const AStyle: TSymbolFill);
begin
  ActSymbolFill := AStyle;
  with ActSymbolFill do
    if FillType <> symfNone then
    begin
      CalculateLineStyle(AStyle.LineStyle, ActLineStyle, ActSymbolFill.LineStyle);
      CalculateFillStyle(AStyle.FillStyle, ActFillStyle, ActSymbolFill.FillStyle);
    end
    else
      ActSymbolFill.Symbol := 0;
end;

{******************************************************************************+
  Function TPaint.CalculateSize
--------------------------------------------------------------------------------
  Calculates a size or width in 1/10 millimeters depending on the specified
  SizeType. Uses project-units and current scale to calculate the size.
+******************************************************************************}

function TPaint.CalculateSize(SizeType: Integer; const Size: Double): Double;
begin
  if SizeType = stDrawingUnits then
    Result := Size
  else
    if SizeType <> stProjectScale then
      Result := Size / Zoom
    else
      Result := Size * 10 / ProjectScale / UnitsToMM;
end;

{******************************************************************************+
  Function TPaint.KeepWindowsAlive:Boolean
--------------------------------------------------------------------------------
  Processes windows-messages. Use this function to keep windows alive while
  eg. drawing the graphic. The function returns true if the current action
  shall be aborted. When printing if the user presses cancel in the print-
  status dialog otherwise if a mouse-button or the esc-key is pressed.
+******************************************************************************}
{++ Ivanoff BUG#New17 BUILD#106}
{The Esc button pressing not iterrupted the layers drawing.}

function TPaint.KeepWindowsAlive: Boolean;
var
  Msg: TMsg;
begin
  RESULT := FALSE;

  if CoreConnected then
    exit;

  if SELF.Printing then
  begin
    Application.ProcessMessages;
    RESULT := SELF.AbortPrinting;
  end
  else
  begin
    if PeekMessage(Msg, 0, 0, 0, pm_Remove) then
    begin
      with Msg do
      begin
        if (Message = wm_LButtonUp) then
        begin
          SendMessage(HWindow, wm_CancelMode, 0, 0);
        end
        else
          if (Message = wm_LButtonDown)
            or (Message = wm_RButtonDown)
            or (Message = wm_NCLButtonDown)
            or (Message = wm_NCRButtonDown) then
          begin
            Result := TRUE;
          end
          else
            if Message <> wm_Timer then
            begin
              if (Message = wm_KeyDown) and (wParam = vk_Escape) then
              begin
                Result := TRUE;
              end;
            end
            else
            begin
              SendMessage(HWindow, Msg.message, Msg.wParam, Msg.lParam);
            end;
      end;
    end;
  end;
end;
{-- Ivanoff}
{-----------------------------------------------------}

procedure TPaint.GetVirtScreen(var Rect: TRect);
begin
  GetClientRect(HWindow, Rect);
  DispToVirtualRect(Rect); { Convert to logical units and swap top/bottom }
end;
{---------------------------------------------------}

procedure TPaint.GetDispScreen(var DispRect: TRect);
begin
  GetVirtScreen(DispRect);
end;
{-------------------------------------------------------
  Procedure to get bounding rectangle of rotated project window in proj. units
}

procedure TPaint.GetProjScreen(var Rect: TDRect);
var
  DRect: TRect;

  procedure CorrectWithPoint(X, Y: Integer);
  var
    DPoint: TPoint;
    Point: TDPoint;
  begin
    DPoint.X := X;
    DPoint.y := Y;
    ConvertToDraw(DPoint, Point);
    Rect.CorrectByPoint(Point);
  end;

begin
  Rect.Init;
  GetVirtScreen(DRect);
  with DRect do
  begin
    CorrectWithPoint(Left, Bottom);
    CorrectWithPoint(Left, Top);
    CorrectWithPoint(Right, Top);
    CorrectWithPoint(Right, Bottom);
  end;
end;
{-----------------------------------------------------------------
  Convert project rectangle to virtual (in project window units) one
  with rotation of original and using of bounds if needed
}

procedure TPaint.ProjRectToDispRect(var DrawRect, DispRect: TRect);
var
  PPoint: TDPoint; {Project point}
  DPoint: TPoint; {Display logical point}
  {-------------------------}

  procedure CorrectWithPPoint;
  begin
    ConvertToDisp(PPoint, DPoint);
  { Correct Output rectangle with the point }
    with DispRect.TopLeft do
    begin
      if X > DPoint.X then
        X := DPoint.X;
      if Y > DPoint.Y then
        Y := DPoint.Y;
    end;
    with DispRect.BottomRight do
    begin
      if X < DPoint.X then
        X := DPoint.X;
      if Y < DPoint.Y then
        Y := DPoint.Y;
    end;
  end;
begin
  DispRect := Rect(MaxInt, MaxInt, -MaxInt, -MaxInt); {Init rect}
  PPoint.X := DrawRect.TopLeft.X; { Copy lower left point }
  PPoint.Y := DrawRect.TopLeft.Y; { Copy lower left point }
  CorrectWithPPoint;
  PPoint.Y := DrawRect.BottomRight.Y; {Now - upper left}
  CorrectWithPPoint;
  PPoint.X := DrawRect.BottomRight.X; {Set as an upper right point}
  CorrectWithPPoint;
  PPoint.Y := DrawRect.TopLeft.Y; {Now - lower right}
  CorrectWithPPoint;
end;
{ Overloaded versiopn of a previous procedure }

procedure TPaint.ProjRectToDispRect(var DrawRect: TDRect; var DispFrame: TRect);
var
  Temp: TRect;
begin
  with DrawRect do
    Temp := Rect(A.X, A.Y, B.X, B.Y);
  ProjRectToDispRect(Temp, DispFrame);
end;
{------------------------------------------}

procedure TPaint.ProjRectToWorldRect(var DrawRect: TDRect; var WorldFrame: TRect);
var
  Temp: TDRect;
begin
  WorldFrameForProjRect(DrawRect, Temp);
  with Temp do
  begin
    WorldFrame := Rect(A.X, A.Y, B.X, B.Y);
  end;
end;

// method to export user defined line and fill-styles to a wlf-File

procedure TPaint.ExportUserDefStylesToWlf(const Filename: string);
begin
   // write the linestyles
  ProjStyles.XLineStyles.WriteToFile(Filename);
   // write the fillstyles
  ProjStyles.XFillStyles.WriteToFile(Filename);
end;

// method to import user defined line and fill-styles from a wlf-File

procedure TPaint.ImportUserDefStylesFromWlf(const Filename: string);
begin
   // read the linestyles
  ProjStyles.XLineStyles.ImportFromFile(Filename);
   // read the fillstyles
  ProjStyles.XFillStyles.ImportFromFile(Filename);

//   PProj(AProj)^.Layers^.SetLayerPriority(

end;

// method to get the number of a user defined FillStyle

function TPaint.GetUserFillStyleNumberByName(const StyleName: string): integer;
begin
  result := ProjStyles.XFillStyles.GetNumberByName(StyleName);
end;

// method to get the name of a user defined FillStyle

function TPaint.GetUserFillStyleNameByNumber(StyleNumber: integer): string;
begin
  result := ProjStyles.XFillStyles.GetNameByNumber(StyleNumber);
end;

// method to get the number of a user defined LineStyle

function TPaint.GetUserLineStyleNumberByName(const StyleName: string): integer;
begin
  result := ProjStyles.XLineStyles.GetNumberByName(StyleName);
end;

// method to get the name of a user defined LineStyle

function TPaint.GetUserLineStyleNameByNumber(StyleNumber: integer): string;
begin
  result := ProjStyles.XLineStyles.GetNameByNumber(StyleNumber);
end;

const
  RPaint: TStreamRec = (
    ObjType: rn_Paint;
    VmtLink: TypeOf(TPaint);
    Load: @TPaint.Load;
    Store: @TPaint.Store);

function TPaint.ConvAngle(AAngle: Double; AMode: Boolean): Double;
begin
  Result := AAngle;
  if (CoordinateSystem.CoordinateType = ctGeodatic) and (PProj(AProj)^.Deg400Mode) then
  begin
    if AMode then
    begin
      Result := (AAngle * 400.0) / 360.0;
    end
    else
    begin
      Result := (AAngle * 360.0) / 400.0;
    end;
  end;
end;

procedure TPaint.SetProjectionByDialog;                                           //Projection setup and other...
var
  testX, testY : double;
  AxGisProjection: TAxGisProjection;
  LngCode: AnsiString;
  Buffer: array[0..255] of Char;
  IniName: AnsiString;
begin
{$IFNDEF WMLT}
   // setup projection Ax-Control
  AxGisProjection := TAxGisProjection.Create(Application);
  AxGisProjection.WorkingPath := OSInfo.WingisDir;

  if CoordinateSystem.CoordinateType = ctCarthesian then
  begin
    AxGisProjection.SourceCoordinateSystem := cs_Carthesic;
  end
  else
  begin
    AxGisProjection.SourceCoordinateSystem := cs_Geodetic;
  end;

  AxGisProjection.SourceProjSettings := ProjectionSettings.ProjectProjSettings;
  AxGisProjection.SourceProjection := ProjectionSettings.ProjectProjection;
  AxGisProjection.SourceDate := ProjectionSettings.ProjectDate;
  AxGisProjection.SourceXOffset := ProjectionSettings.ProjectionXOffset;
  AxGisProjection.SourceYOffset := ProjectionSettings.ProjectionYOffset;
  AxGisProjection.SourceScale := ProjectionSettings.ProjectionScale;
  AxGisProjection.ShowOnlySource := FALSE; // set to use only for projection selection
   // get language code

  IniName := OSInfo.WingisIniFileName;
  ZeroMemory(@Buffer, Sizeof(Buffer));
  GetPrivateProfileString('SETTINGS', 'Language', '044', Buffer, SizeOf(Buffer), PChar(IniName));
  LngCode := StrPas(Buffer);

  AxGisProjection.LngCode := LngCode;
  AxGisProjection.SetupByDialog;

   // get the projection settings
  if (AxGisProjection.SourceCoordinateSystem = cs_Carthesic) then
  begin
    CoordinateSystem.CoordinateType := ctCarthesian;
  end
  else
  begin
    CoordinateSystem.CoordinateType := ctGeodatic;
  end;

  ProjectionSettings.ProjectProjSettings := AxGisProjection.SourceProjSettings;
  ProjectionSettings.ProjectProjection := AxGisProjection.SourceProjection;
  ProjectionSettings.ProjectTargetProjSettings := AxGisProjection.TargetProjSettings;
  ProjectionSettings.ProjectTargetProjection := AxGisProjection.TargetProjection;
  ProjectionSettings.ProjectDate := AxGisProjection.SourceDate;
  ProjectionSettings.ProjectionXOffset := AxGisProjection.SourceXOffset;
  ProjectionSettings.ProjectionYOffset := AxGisProjection.SourceYOffset;
  ProjectionSettings.ProjectionScale := AxGisProjection.SourceScale;
  testX := 10;
  testY := 10;
  AxGisProjection.Calculate(testX, testY);
  {
  if ((NOT (ProjectionSettings.ProjectProjection = '')) AND (ProjectionSettings.ProjectTargetProjection = '') ) then
  begin
    ShowMessage('Now choose target Projection');
    AxGisProjection.SetupByDialog;
    ProjectionSettings.ProjectTargetProjection := AxGisProjection.SourceProjection;
  end;
  }
  ShowMessage('SourceProj: ' + ProjectionSettings.ProjectProjection + ' \\\ TargetProj: ' + ProjectionSettings.ProjectTargetProjection);



  AxGisProjection.Destroy;

  TMDIChild(Parent).Data^.SetModified;
{$ENDIF}
end;

function TPaint.GetObjCount: Integer;
begin
  Result := PLayer(Objects)^.Data^.GetCount;
end;

begin
  RegisterType(RPaint);
end.

