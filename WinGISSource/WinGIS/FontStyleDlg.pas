unit FontStyleDlg;

interface

{$IFNDEF AXDLL} // <----------------- AXDLL

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, StdCtrls,
  ExtCtrls, WCtrls, Spinbtn, UserIntf, Validate, StyleDef, AM_Font, AM_Def, MultiLng,
{++ Moskaliov BUG#268 BUILD#108 29.03.00}
//   PropCollect,ImgList, Combobtn, ColorSelector;
  PropCollect, ImgList, Combobtn, ColorSelector, Palette, TextPosPanel;
{-- Moskaliov BUG#268 BUILD#108 29.03.00}

type
  TFontType = (ftRaster, ftVector, ftTrueType, ftNotInstalled);

  TFontStyleDialog = class(TWForm)
    AlignCombo: TComboBox;
    Bevel1: TBevel;
    BoldCheck: TCheckBox;
    CancelBtn: TButton;
    ControlPanel: TPanel;
    ControlPanel1: TPanel;
    FixedHeightCheck: TCheckBox;
    FontEdit: TEdit;
    FontList: TWListBox;
    Group1: TWGroupBox;
    HeightEdit: TEdit;
    HeightSpin: TSpinBtn;
    HeightVal: TMeasureLookupValidator;
    ImageList: TImageList;
    ItalicCheck: TCheckBox;
    MlgSection: TMlgSection;
    OKBtn: TButton;
    PreviewWindow: TWindow;
    WLabel4: TWLabel;
    WLabel3: TWLabel;
    WLabel1: TWLabel;
    TransparentCheck: TCheckBox;
    UnderlinedCheck: TCheckBox;
    UseGlobalCheck: TCheckBox;
    ForeColorBtn: TColorSelectBtn;
    BackColorBtn: TColorSelectBtn;
{-- Moskaliov BUG#268 BUILD#108 29.03.00}

      {++brovak BUG83 BUILD#127 - Text Angle}
    AngleEdit: TEdit;
    AngleSpin: TSpinBtn;
    AngleLabel: TWLabel;
    AngleVal: TMeasureLookupValidator;
    FColorLabel: TLabel;
    BColorLabel: TLabel;
    TextPosPanel: TTextPosPanel;
    cbAlign: TCheckBox;
      {--brovak}
    procedure FormShow(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FontEditChange(Sender: TObject);
    procedure FontEditExit(Sender: TObject);
    procedure FontListClick(Sender: TObject);
    procedure FontListDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure PreviewWindowPaint(Sender: TObject);
    procedure UpdatePreviewWindow(Sender: TObject);
    procedure UseGlobalCheckClick(Sender: TObject);
    procedure ColorBtnSelectColor(Sender: TObject);
    procedure cbAlignClick(Sender: TObject);
  private
    FInitialized: Boolean;
    FFonts: PFonts;
    FFontStyle: PFontData;
    FOrgTextStyle: TTextProperties;
    FTextStyle: TTextProperties;
    FAllowHeight: Boolean;
    FGlobFontSetting: PFontSettings;
    FProjFontSetting: PFontSettings;
    FUsedGlobal: Boolean;
{++ Moskaliov BUG#268 BUILD#108 29.03.00}
    FColorDefSupport: Boolean;

    FPalette: TPalette;
{-- Moskaliov BUG#268 BUILD#108 29.03.00}
       {brovak}
    FlagShow: boolean;
    bShowAlign: Boolean;
    FAlign: Byte;
       {brovak}
    function FindFontByName(const Name: string; var FullyEqual: Boolean): Integer;
  public
   {++Brovak - was moved by me for prophndl}
    FForeColor: TColor;
    FBackColor: TColor;
   {--brovak}
    procedure AddFont(const Name: string; AType: TFontType);
    property Fonts: PFonts read FFonts write FFonts;
    property FontStyle: PFontData read FFontStyle write FFontStyle;
    property TextStyle: TTextProperties read FTextStyle write FOrgTextStyle;
    property AllowHeightOptions: Boolean read FAllowHeight write FAllowHeight;
    property GlobFontSettings: PFontSettings read FGlobFontSetting write FGlobFontSetting;
    property ProjFontSettings: PFontSettings read FProjFontSetting write FProjFontSetting;
{++ Moskaliov BUG#268 BUILD#108 29.03.00}
    property ColorSupport: Boolean read FColorDefSupport;
    property ForeColor: TColor read FForeColor write FForeColor;
    property BackColor: TColor read FBackColor write FBackColor;
{-- Moskaliov BUG#268 BUILD#108 29.03.00}
    procedure SetNewCaption(ANewCaption: Integer);
{++ Moskaliov BUG#268 BUILD#108 29.03.00}
    procedure SetColorSupport(AForeColor, ABackColor: TColor);
{-- Moskaliov BUG#268 BUILD#108 29.03.00}
{+++brovak BUG#32 BUILD#127 - Text Color}
    procedure SetColorSupport1;
{brovak}
    procedure SetAlign(AAlign: Byte);
  end;
{$ENDIF} // <----------------- AXDLL

implementation

{$IFNDEF AXDLL} // <----------------- AXDLL

{$R *.DFM}

uses Measures, NumTools, StrTools;

procedure TFontStyleDialog.FormShow(Sender: TObject);
var
  Cnt: Integer;
  FontDes: PFontDes;
  AType: TFontType;
begin
  if FProjFontSetting <> nil then
    with FProjFontSetting^ do
    begin
      Fonts := AllFonts;
      FontStyle := ChosenFont;
      if UseGlobals then
      begin
        FUsedGlobal := TRUE;
//++ Glukhov Bug#63 BUILD#128 14.09.00
{* Commented the incorrect code
      FontStyle^.Font:=FGlobFontSetting^.ChosenFont^.Font;
      FontStyle^.Style:=FGlobFontSetting^.ChosenFont^.Style;
      FontStyle^.Height:=FGlobFontSetting^.ChosenFont^.Height;
*}
//-- Glukhov Bug#63 BUILD#128 14.09.00
      end
      else
        FUsedGlobal := FALSE;
    end
  else
    if FGlobFontSetting <> nil then
      with FGlobFontSetting^ do
      begin
        FUsedGlobal := TRUE;
        Fonts := AllFonts;
        FontStyle := ChosenFont;
      end;
  if not FInitialized then
  begin
    if FFonts <> nil then
      for Cnt := 0 to FFonts^.Count - 1 do
      begin
        FontDes := FFonts^.At(Cnt);
        if FontDes^.Installed or (FontDes^.UseCount <> 0) then
          with FontDes^ do
          begin
            if not Installed then
              AType := ftNotInstalled
            else
              if FontType or TRUETYPE_FONTTYPE = FontType then
                AType := ftTrueType
              else
                if FontType or RASTER_FONTTYPE = FontType then
                  AType := ftRaster
                else
                  AType := ftVector;
            AddFont(PToStr(FontDes^.Name), AType);
          end;
      end;
    FInitialized := TRUE;
  end;
  if FOrgTextStyle <> nil then
    FTextStyle.Assign(FOrgTextStyle);
  if FFontStyle <> nil then
    with FFontStyle^, FTextStyle do
    begin
      PropertyMode := pmSet;
      FontDes := FFonts^.GetFont(Font);
      if FontDes <> nil then
        FontName := PToStr(FontDes^.Name)
      else
        FontName := sNone;
      if Style and ts_Bold <> 0 then
        Bold := bsYes
      else
        Bold := bsNo;
      if Style and ts_Italic <> 0 then
        Italic := bsYes
      else
        Italic := bsNo;
      if Style and ts_Underl <> 0 then
        Underlined := bsYes
      else
        Underlined := bsNo;
      if Style and ts_Transparent <> 0 then
        Transparent := bsYes
      else
        Transparent := bsNo;
      if Style and ts_FixHeight <> 0 then
        FixedHeight := bsYes
      else
        FixedHeight := bsNo;
      Height := FFontStyle^.Height / 100.0;
//++ Glukhov NonzeroText Build#166 27.11.01
      if Height = 0 then
        Height := cMinFontHeight;
//-- Glukhov NonzeroText Build#166 27.11.01
      if Style and ts_Right <> 0 then
        Alignment := taRight
      else
        if Style and ts_Center <> 0 then
          Alignment := taCentered
        else
          Alignment := taLeft;
    end;
  with FTextStyle do
  begin
    if (CompareText(FontName, sNone) = 0) or (CompareText(FontName, sFromLayer) = 0)
      or (CompareText(FontName, sFromObject) = 0)
      or (CompareText(FontName, sNotSet) = 0) then
      FontEdit.Text := ''
    else
      FontEdit.Text := FontName;
    SetDoubleField(Height, 1, HeightVal, muDrawingUnits, HeightOptions);
    SetChecked(FixedHeightCheck, FixedHeight);
    SetChecked(ItalicCheck, Italic);
    SetChecked(BoldCheck, Bold);
    SetChecked(UnderlinedCheck, Underlined);
    SetChecked(TransparentCheck, Transparent);
    {+++Brovak BUG #23 BUILD 127}
    if AlignCombo.Items.Count < 4 then
      AlignmentOptions.AddLongNamesToList(AlignCombo.Items);
    if Alignment >= 0 then
      AlignCombo.ItemIndex := Alignment
    else
      AlignCombo.ItemIndex := 3;
   {--brovak}

  // disable controls if editing not allowed
    if not UpdatesEnabled then
      SetControlGroupEnabled(ControlPanel, FALSE);

       {+++Brovak BUG #23 BUILD 127}
    if AngleEdit.Visible = true then
    begin;

      if FtextStyle.Rotation < 0 then
        SetDoubleField((2 * PI + Rotation), 1, AngleVal, muRad, AngleOptions)
      else
        SetDoubleField(Rotation, 1, AngleVal, muDrawingUnits, AngleOptions);
    end;
         {++Brovak BUG #32 BUILD 127}
    if AngleEdit.Visible = true then
    begin;
      if FlagShow = true then
      begin;
        ForeColorBtn.Options := FTextStyle.ForeColorOptions;
        ForeColorBtn.SelectedColor := FTextStyle.ForeColor;
        BackColorBtn.Options := FTextStyle.BackColorOptions;
        BackColorBtn.SelectedColor := FTextStyle.BackColor;
        if (ForeColorBtn.SelectedColor > -1)
          and (ForeColorBtn.SelectedColor <> intNoChange) then
          FForeColor := ForeColorBtn.SelectedColor;
        if (BackColorBtn.SelectedColor > -1)
          and (BackColorBtn.SelectedColor <> intNoChange) then
          FBackColor := BackColorBtn.SelectedColor;
      end;
      FlagShow := false;
    end;
  end;
     {--Brovak}
  // for "legend text style dialog" disable heigth elements
  if not FAllowHeight then
  begin
    SetControlEnabled(HeightEdit, FALSE);
    FixedHeightCheck.Enabled := False;
  end;
  UseGlobalCheck.Checked := FUsedGlobal;
  if FUsedGlobal then
    UseGlobalCheckClick(UseGlobalCheck);
end;

procedure TFontStyleDialog.FormHide(Sender: TObject);
begin
  if ModalResult = mrOK then
  begin
    with FTextStyle do
    begin
      PropertyMode := pmSet;
      if FontEdit.Text = '' then
        FontName := sNoChange
      else
        FontName := FontEdit.Text;
      Height := GetDoubleField(1, HeightVal, muDrawingUnits, HeightOptions);
      FixedHeight := GetChecked(FixedHeightCheck);
      Italic := GetChecked(ItalicCheck);
      Bold := GetChecked(BoldCheck);
      Underlined := GetChecked(UnderlinedCheck);
      Transparent := GetChecked(TransparentCheck);
      with AlignCombo do
        if ItemIndex > 2 then
          Alignment := AlignmentOptions.Values[ItemIndex - 2]
        else
          if ItemIndex = 2 then
            Alignment := taRight
          else
            if ItemIndex = 1 then
              Alignment := taCentered
            else
              Alignment := taLeft;
      if FOrgTextStyle <> nil then
        FOrgTextStyle.Assign(FTextStyle);
    end;
    if FFontStyle <> nil then
      with FFontStyle^, FTextStyle do
      begin
        if FontName[1] > sNone[1] then
          Font := FFonts^.IDFromName(FontName);
        if DblCompare(Height, dblNone) > 0 then
          FFontStyle^.Height := LimitToLong(Height * 100);
        if Bold = bsYes then
          Style := Style or ts_Bold
        else
          if Bold = bsNo then
            Style := Style and not ts_Bold;
        if Italic = bsYes then
          Style := Style or ts_Italic
        else
          if Italic = bsNo then
            Style := Style and not ts_Italic;
        if Underlined = bsYes then
          Style := Style or ts_Underl
        else
          if Underlined = bsNo then
            Style := Style and not ts_Underl;
        if Transparent = bsYes then
          Style := Style or ts_Transparent
        else
          if Transparent = bsNo then
            Style := Style and not ts_Transparent;
        if FixedHeight = bsYes then
          Style := Style or ts_FixHeight
        else
          if FixedHeight = bsNo then
            Style := Style and not ts_FixHeight;
        case Alignment of
          taRight: Style := (Style or ts_Right) and not (ts_Center);
          taCentered: Style := (Style or ts_Center) and not (ts_Right);
          taLeft: Style := Style and not (ts_Right or ts_Center);
        end;
      end;
    if FProjFontSetting <> nil then
      with FProjFontSetting^ do
      begin
        if UseGlobalCheck.Checked then
          UseGlobals := TRUE
        else
          UseGlobals := FALSE;
      end;
  end;
end;

procedure TFontStyleDialog.FormCreate(Sender: TObject);
begin
  FTextStyle := TTextProperties.Create;
  FAllowHeight := True;
{++ Moskaliov BUG#268 BUILD#108 29.03.00}
//  FUsedGlobal:=TRUE;
  FUsedGlobal := FALSE;
  FColorDefSupport := FALSE;
  FForeColor := clBlack;
  FBackColor := clWhite;
//  ForeColorLabel.Enabled:=FALSE;
//  ForeColorLabel.Visible:=FALSE;
  ForeColorBtn.Enabled := FALSE;
  ForeColorBtn.Visible := FALSE;
//  BackColorLabel.Enabled:=FALSE;
//  BackColorLabel.Visible:=FALSE;
  BackColorBtn.Enabled := FALSE;
  BackColorBtn.Visible := FALSE;
{-- Moskaliov BUG#268 BUILD#108 29.03.00}
{+++brovak BUG#83 BUILD127}
  AngleEdit.Visible := false;
  AngleSpin.Visible := false;
  AngleLabel.Visible := false;
  AngleVal.Edit := nil;
{---brovak}
  FlagShow := true;

  FGlobFontSetting := nil;
  FProjFontSetting := nil;
  bShowAlign := True;
  FAlign := 255;
end;

procedure TFontStyleDialog.FormDestroy(Sender: TObject);
begin
  FTextStyle.Free;
{++ Moskaliov BUG#268 BUILD#108 29.03.00}
  if FColorDefSupport then
    FPalette.Free;
{-- Moskaliov BUG#268 BUILD#108 29.03.00}
end;

procedure TFontStyleDialog.AddFont(const Name: string; AType: TFontType);
begin
  FontList.Items.AddObject(Name, Pointer(AType));
end;

procedure TFontStyleDialog.FontEditChange(Sender: TObject);
var
  AIndex: Integer;
  FullyEqual: Boolean;
begin
  AIndex := FindFontByName(FontEdit.Text, FullyEqual);
  if FullyEqual then
    FontList.ItemIndex := AIndex
  else
  begin
    FontList.ItemIndex := -1;
    FontList.TopIndex := AIndex;
  end;
  FontListClick(FontList);
end;

function TFontStyleDialog.FindFontByName(const Name: string; var FullyEqual: Boolean): Integer;
var
  MaxEqualLen: Integer;
  MaxEqualAllLen: Integer;
  EqualLen: Integer;
  Cnt: Integer;
begin
  MaxEqualLen := 0;
  MaxEqualAllLen := 0;
  Result := -1;
  for Cnt := 0 to FontList.Count - 1 do
  begin
    EqualLen := CalculateEqualLen(Name, FontList.Items[Cnt]);
    if EqualLen > MaxEqualLen then
    begin
      MaxEqualLen := EqualLen;
      Result := Cnt;
    end;
  end;
  if Result >= 0 then
    FullyEqual := Length(FontList.Items[Result]) = Length(Name)
  else
    FullyEqual := FALSE;
end;

procedure TFontStyleDialog.FontEditExit(Sender: TObject);
var
  FullyEqual: Boolean;
begin
  if FontEdit.Text <> '' then
  begin
    FontList.ItemIndex := FindFontByName(FontEdit.Text, FullyEqual);
    FontListClick(FontList);
  end;
end;

procedure TFontStyleDialog.FontListClick(Sender: TObject);
begin
  if FontList.ItemIndex >= 0 then
  begin
    FontEdit.Text := FontList.Items[FontList.ItemIndex];
    FontEdit.SelStart := Length(FontEdit.Text);
  end;
  UpdatePreviewWindow(Sender);
end;

procedure TFontStyleDialog.FontListDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  with FontList, Canvas do
  begin
    if not Enabled then
    begin
      Font.Color := clBtnShadow;
      if odSelected in State then
        Brush.Color := clBtnFace;
    end;
    case TFontType(Items.Objects[Index]) of
      ftRaster,
        ftVector: ImageList.Draw(Canvas, (ItemHeight - 9) div 2, CenterHeightInRect(9, Rect), 1);
      ftTrueType: ImageList.Draw(Canvas, (ItemHeight - 9) div 2, CenterHeightInRect(9, Rect), 0);
      ftNotInstalled: ImageList.Draw(Canvas, (ItemHeight - 9) div 2, CenterHeightInRect(9, Rect), 2);
    end;
    Inc(Rect.Left, ItemHeight);
    TextRect(Rect, Rect.Left + 3, CenterHeightInRect(TextHeight(Items[Index]), Rect), Items[Index]);
    if odFocused in State then
      DrawFocusRect(Rect);
  end;
end;

procedure TFontStyleDialog.PreviewWindowPaint(Sender: TObject);
var
  ARect: TRect;
  XPos: Integer;
{++ Moskaliov BUG#268 BUILD#108 29.03.00}
  BRect: TRect;
{-- Moskaliov BUG#268 BUILD#108 29.03.00}
begin
  with PreviewWindow.Canvas do
  begin
    if FTextStyle.UpdatesEnabled then
{++ Moskaliov BUG#268 BUILD#108 29.03.00}
//       Font.Color:=clBlack
       {+++Brovak BUG#32 BUILD 127}
      if (AngleEdit.Visible = false) or (FForecolor > -1) then
        Font.Color := FForeColor
      else
        Font.Color := clBlack
       {--Brovak}
{-- Moskaliov BUG#268 BUILD#108 29.03.00}
    else
      Font.Color := clBtnShadow;
    Font.Size := 11;
    Font.Name := FontEdit.Text;
    Font.Style := [];
    if ItalicCheck.State = cbChecked then
      Font.Style := Font.Style + [fsItalic];
    if BoldCheck.State = cbChecked then
      Font.Style := Font.Style + [fsBold];
    if UnderlinedCheck.State = cbChecked then
      Font.Style := Font.Style + [fsUnderline];
    ARect := PreviewWindow.ClientRect;
    if AlignCombo.ItemIndex = 0 then
      XPos := 5
    else
      if AlignCombo.ItemIndex = 2 then
        XPos := PreviewWindow.ClientWidth - TextWidth(FontEdit.Text) - 5
      else
        XPos := CenterWidthInRect(TextWidth(FontEdit.Text), ARect);
{++ Moskaliov BUG#268 BUILD#108 29.03.00}
    Brush.Color := clWhite;
    if TransparentCheck.State = cbChecked then
{-- Moskaliov BUG#268 BUILD#108 29.03.00}
      TextRect(ARect, XPos, CenterHeightInRect(TextHeight(FontEdit.Text), ARect), FontEdit.Text)
{++ Moskaliov BUG#268 BUILD#108 29.03.00}
    else
    begin

      FillRect(ARect);
   {+++broval BUG 32 BUILD #127}
      if (AngleEdit.Visible = true) and (BackColorBtn.SelectedColor < 0) then
        Brush.Color := clWhite
      else
        Brush.Color := FBackColor;
   {brovak}
      BRect.Left := XPos;
      BRect.Right := BRect.Left + TextWidth(FontEdit.Text);
      BRect.Top := CenterHeightInRect(TextHeight(FontEdit.Text), ARect);
      BRect.Bottom := BRect.Top + TextHeight(FontEdit.Text);
      TextRect(BRect, XPos, CenterHeightInRect(TextHeight(FontEdit.Text), BRect), FontEdit.Text);
    end;
{-- Moskaliov BUG#268 BUILD#108 29.03.00}
  end;
end;

procedure TFontStyleDialog.UpdatePreviewWindow(Sender: TObject);
begin
  PreviewWindow.Invalidate;
end;

procedure TFontStyleDialog.UseGlobalCheckClick(Sender: TObject);
begin
  SetControlGroupEnabled(ControlPanel, not UseGlobalCheck.Checked);
end;

procedure TFontStyleDialog.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  CanClose := CheckValidators(Self);
end;

procedure TFontStyleDialog.SetNewCaption(ANewCaption: Integer);
begin
  Caption := MlgSection[ANewCaption];
end;

{++ Moskaliov BUG#268 BUILD#108 29.03.00}

procedure TFontStyleDialog.SetColorSupport(AForeColor, ABackColor: TColor);
begin
  if not FColorDefSupport then
  begin
    FPalette := TPalette.Create;
    ForeColorBtn.Palette := FPalette;
    BackColorBtn.Palette := FPalette;
  end;
  FColorDefSupport := TRUE;
  FForeColor := AForeColor;
  FBackColor := ABackColor;
//   ForeColorLabel.Enabled:=TRUE;
//   ForeColorLabel.Visible:=TRUE;
  ForeColorBtn.Enabled := TRUE;
  ForeColorBtn.Visible := TRUE;
  ForeColorBtn.SelectedColor := AForeColor;
//   BackColorLabel.Enabled:=TRUE;
//   BackColorLabel.Visible:=TRUE;
  BackColorBtn.Enabled := TRUE;
  BackColorBtn.Visible := TRUE;
  BackColorBtn.SelectedColor := ABackColor;
  FixedHeightCheck.Enabled := FALSE;
  FixedHeightCheck.Visible := FALSE;
  if Self.Height < 290 then
    Self.Height := 290
  else
    Self.Height := 352;
end;

procedure TFontStyleDialog.ColorBtnSelectColor(Sender: TObject);
begin
  if Sender = ForeColorBtn then
    FForeColor := ForeColorBtn.SelectedColor
  else
    if Sender = BackColorBtn then
      FBackColor := BackColorBtn.SelectedColor;
  PreviewWindow.Invalidate;
end;
{-- Moskaliov BUG#268 BUILD#108 29.03.00}

{+++brovak BUG#32 BUILD 127 I Considerably has changed  orginal Moskaliov's SetColorSupport
For use in the my purposes - set/change colours of the text of selected objects}

procedure TFontStyleDialog.SetColorSupport1;
begin
  if not FColorDefSupport then
  begin
    FPalette := TPalette.Create;
    ForeColorBtn.Palette := FPalette;
    BackColorBtn.Palette := FPalette;
  end;
  FColorDefSupport := TRUE;
  AllowHeightOptions := TRUE;
  FixedHeightCheck.Visible := true;
  FixedHeightCheck.Enabled := true;
{
         FColorLabel.Top:=PreViewWindow.Top+PreViewWindow.Height+3;
         ForeColorBtn.Top:= FColorLabel.Top+FColorLabel.Height+3;
         ForecolorBtn.Left:=PreViewWindow.Left+3;
         ForecolorBtn.Width:=trunc(PreViewWindow.Width/2)-4;

         BColorLabel.Top:=FColorLabel.Top;
         BackColorBtn.Top:=ForeColorBtn.Top;
         BackColorBtn.Left:=ForecolorBtn.Left+ForecolorBtn.Width+4;
         BackColorLabel.Left:=BackColorBtn.Left;
         BackColorBtn.Width:=ForecolorBtn.Width;
         BackColorBtn.Height:= ForecolorBtn.Height;
}
  FColorLabel.Visible := true;
  BColorLabel.Visible := true;

  FColorLabel.Enabled := true;
  BColorLabel.Enabled := true;

  BackColorBtn.Visible := true;
  ForecolorBtn.Visible := true;

  BackColorBtn.Enabled := true;
  ForecolorBtn.Enabled := true;

end;

procedure TFontStyleDialog.SetAlign(AAlign: Byte);
begin
  if FAlign = 255 then
  begin
    FAlign := AAlign;
    TextPosPanel.TextAlign := TTextAlign(AAlign);
  end;
  if (bShowAlign) and (AAlign <> FAlign) then
  begin
    bShowAlign := False;
  end;
  cbAlign.Checked := bShowAlign;
  TextPosPanel.Visible := bShowAlign;
end;

procedure TFontStyleDialog.cbAlignClick(Sender: TObject);
begin
  TextPosPanel.Visible := cbAlign.Checked;
end;

{$ENDIF} // <----------------- AXDLL

end.

