{****************************************************************************}
{ Unit Text - Successor of AM_Text                                           }
{----------------------------------------------------------------------------}
{ Implements the string-processing                                           }
{----------------------------------------------------------------------------}
{ Created : 12.01.2000                                                       }
{****************************************************************************}
Unit Text;

Interface

Uses WinTypes,WinProcs,SysUtils,Objects,Classes;

Type  PTextProperties  = ^TTextProperties;
      TTextProperties  = Record
        Font           : TFontData;    {TextFont}
        Pos            : TDPoint;      {TextPosition}
        Style          : LongInt;      {TextStyle}
        Height         : LongInt;      {TextHeight}
        Angle          : Double;       {TextAngle}
        Text           : String;       {Text}
      end;

Implementation

end.


