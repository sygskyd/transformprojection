Unit PolyPartDlg;

Interface

Uses Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,StdCtrls,ExtCtrls,WCtrls,
     MultiLng,AM_Proj, Validate;

Const PPMlgSectionName = 'PolyPartDialog';
      auMeters         = 0;
      auar             = 1;
      auha             = 2;
      auKilometers     = 3;
      duMeters         = 0;
      duKilometers     = 1;

Type TPolyPartitionDlg = class(TWForm)
       MlgSection      : TMlgSection;
       LayerGroupBox: TWGroupBox;
       DLayername      : TLabel;
       DLayer          : TComboBox;
       ModeGroupBox    : TWGroupBox;
       WGroupBox2      : TWGroupBox;
       ParallelRadio   : TRadioButton;
       FreeRadio       : TRadioButton;
       WGroupBox3      : TWGroupBox;
       DistRadio       : TRadioButton;
       AreaRadio       : TRadioButton;
       OffsetEdit      : TEdit;
       OffsetUnit      : TLabel;
       AngleUnit       : TLabel;
       AngleEdit       : TEdit;
       AngleName       : TLabel;
       AreaEdit        : TEdit;
       AreaUnit        : TLabel;
       InputButton     : TButton;
       CalcButton      : TButton;
       OKButton        : TButton;
       CancelButton    : TButton;
       OffsetValidator : TFloatValidator;
       AngleValidator  : TFloatValidator;
       AreaValidator   : TFloatValidator;
    SAreaname: TLabel;
    SArea: TLabel;
    SAreaUnit: TLabel;
    DAreaUnit1: TLabel;
    DArea1: TLabel;
    DAreaname: TLabel;
    FillCheck: TCheckBox;
    DArea2: TLabel;
    DAreaUnit2: TLabel;
    Label1: TLabel;
       Procedure   FormCreate(Sender:TObject);
       Procedure   FormShow(Sender:TObject);
       Procedure   InputButtonClick(Sender:TObject);
       Procedure   CalcButtonClick(Sender:TObject);
       Procedure   DistRadioClick(Sender:TObject);
       Procedure   AreaRadioClick(Sender:TObject);
       procedure   OKButtonClick(Sender: TObject);
    procedure CancelButtonClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure AreaEditDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure AreaEditDragDrop(Sender, Source: TObject; X, Y: Integer);
      protected
       Procedure   WriteAreaName(AName:String);
       Procedure   WriteSLayer(ALayer:String);
       Procedure   WriteTLayer(ALayer:String);
       Function    ReadSourceArea:Double;
       Procedure   WriteSourceArea(AArea:Double);
       Procedure   WriteTargetArea1(AArea:Double);
       Procedure   WriteTargetArea2(AArea:Double);
       Function    ReadPartDist:Double;
       Procedure   WritePartDist(ADist:Double);
       Function    ReadPartAngle:Double;
       Procedure   WritePartAngle(AAngle:Double);
       Function    ReadPartArea:Double;
       Procedure   WritePartArea(AArea:Double);
       Procedure   WriteLayerList(AList:TStringList);
       Function    ReadFillArea:Boolean;
       Procedure   WriteFillArea(AFill:Boolean);
       Function    ReadPartMode:Integer;
       Procedure   WritePartMode(AMode:Integer);
       Function    ReadSizeMode:Integer;
       Procedure   WriteSizeMode(AMode:Integer);
       Procedure   WriteProj(AProj:PProj);
      private
       FProj           : PProj;
       DistUnits       : Integer;
       AreaUnits       : Integer;
       FAreaName       : String;
       FSLayer         : String;
       FTLayer         : String;
       FSourceArea     : Double;
       FTargetArea1    : Double;
       FTargetArea2    : Double;
       FPartDist       : Double;
       FPartAngle      : Double;
       FPartArea       : Double;
       FPartMode       : Integer;
       FSizeMode       : Integer;
       FStartIndex     : Integer;
       Procedure   ReadAllSettings;
      public
       FDDEMode        : Boolean;
       Property    PropAreaName:String write WriteAreaName;
       Property    PropSLayer:String write WriteSLayer;
       Property    PropTLayer:String write WriteTLayer;
       Property    LayerList:TStringList write WriteLayerList;
       Property    Proj:PProj write WriteProj;
//       procedure   SwapAreas;
      published
       Property    SourceArea:Double read ReadSourceArea write WriteSourceArea;
       Property    TargetArea1:Double write WriteTargetArea1;
       Property    TargetArea2:Double write WriteTargetArea2;
       Property    PartDist:Double read ReadPartDist write WritePartDist;
       Property    PartAngle:Double read ReadPartAngle write WritePartAngle;
       Property    PartArea:Double read ReadPartArea write WritePartArea;
       Property    FillArea:Boolean read ReadFillArea write WriteFillArea;
       Property    PartMode:Integer read ReadPartMode write WritePartMode;
       Property    SizeMode:Integer read ReadSizeMode write WriteSizeMode;
     end;

var PolyPartitionDlg : TPolyPartitionDlg;

Implementation

{$R *.DFM}

Uses PolyPartition,AM_Ini,AM_Def,AM_Child,AM_Layer,AM_Cut,MenuFn,AM_ProjO,LayerHndl;

Procedure TPolyPartitionDlg.FormCreate
   (
   Sender          : TObject
   );
  begin
    if Sender <> nil then begin
       Top:=5;
       Left:=Screen.Width-Width-5;
    end;
    if IniFile^.AreaFact < 1000000 then DistUnits:=duMeters          // m
    else DistUnits:=duKilometers;                                    // km
    if IniFile^.AreaFact < 100 then AreaUnits:=auMeters              // m�
    else if IniFile^.AreaFact < 10000 then AreaUnits:=auar           // a
    else if IniFile^.AreaFact < 1000000 then AreaUnits:=auha         // ha
    else AreaUnits:=auKilometers;                                    // km�
    FStartIndex:=0;
    FDDEMode:=FALSE;
  end;

Procedure TPolyPartitionDlg.FormShow
   (
   Sender          : TObject
   );
  var TmpString      : String;
  begin
    if FDDEMode then begin
      LayerGroupBox.Visible:=FALSE;
      ModeGroupBox.Top:=LayerGroupBox.Top;
      InputButton.Top:=212;
      CalcButton.Top:=212;
      OKButton.Top:=212;
      CancelButton.Top:=212;
      Height:=273;
      FillCheck.Enabled:=FALSE;
      AreaValidator.AsFloat:=PolyPart.PolyPartProps.PartArea;
    end;
    if FAreaName <> '' then ModeGroupBox.Caption:=FAreaName;
//    SLayer.Caption:=FSLayer;
    DLayer.ItemIndex:=FStartIndex;
    if DistUnits = duMeters then TmpString:=MlgStringList[PPMlgSectionName,6]
    else TmpString:=MlgStringList[PPMlgSectionName,7];
    OffsetUnit.Caption:=TmpString;
    if AreaUnits = auMeters then TmpString:=MlgStringList[PPMlgSectionName,8]
    else if AreaUnits = auar then TmpString:=MlgStringList[PPMlgSectionName,9]
    else if AreaUnits = auha then TmpString:=MlgStringList[PPMlgSectionName,10]
    else TmpString:=MlgStringList[PPMlgSectionName,11];
    SAreaUnit.Caption:=TmpString;
    DAreaUnit1.Caption:=TmpString;
    DAreaUnit2.Caption:=TmpString;
    AreaUnit.Caption:=TmpString;
    SArea.Caption:=FloatToStrF(FSourceArea,ffNumber,15,2);
    DArea1.Caption:=FloatToStrF(FTargetArea1,ffNumber,15,2);
    DArea2.Caption:=FloatToStrF(FTargetArea2,ffNumber,15,2);
    if FTargetArea1 > 0 then begin
       AreaEdit.Text:=FloatToStrF(FTargetArea1,ffNumber,15,2);
    end;
//    AreaEdit.Text:=DArea1.Caption;
    case FPartMode of
      pmParallel : ParallelRadio.Checked:=TRUE;
//      pmPuffer   : PufferRadio.Checked:=TRUE;
//      pmIsland   : IslandRadio.Checked:=TRUE;
      pmFree     : FreeRadio.Checked:=TRUE;
    end;
    case FSizeMode of
      smDist     : begin
                     DistRadio.Checked:=TRUE;
                     OffsetEdit.Enabled:=TRUE;
                     AngleEdit.Enabled:=TRUE;
                     AreaEdit.Enabled:=FALSE;
                   end;
      smArea     : begin
                     AreaRadio.Checked:=TRUE;
                     OffsetEdit.Enabled:=FALSE;
                     AngleEdit.Enabled:=TRUE;
                     AreaEdit.Enabled:=TRUE;
                   end;
    end;
    if PolyPart.CutLineOK then begin
      CalcButton.Enabled:=TRUE;
      if PolyPart.CalcOK then OKButton.Enabled:=TRUE
      else OKButton.Enabled:=FALSE;
    end
    else begin
      CalcButton.Enabled:=FALSE;
      OKButton.Enabled:=FALSE;
    end;
    if (Sender <> nil) and (CalcButton.Enabled) then begin
       CalcButtonClick(nil);
    end;
  end;

Procedure TPolyPartitionDlg.WriteProj
   (
   AProj           : PProj
   );
  begin
    FProj:=AProj;
  end;

Procedure TPolyPartitionDlg.WriteAreaName
   (
   AName           : String
   );
  begin
    FAreaName:=AName;
  end;

Procedure TPolyPartitionDlg.WriteSLayer
   (
   ALayer          : String
   );
  begin
    FSLayer:=ALayer;
  end;

Procedure TPolyPartitionDlg.WriteTLayer
   (
   ALayer          : String
   );
  begin
    FTLayer:=ALayer;
  end;

Function TPolyPartitionDlg.ReadSourceArea
   : Double;
  begin
    case AreaUnits of
      auMeters     : Result:=FSourceArea;
      auar         : Result:=FSourceArea*100;
      auha         : Result:=FSourceArea*10000;
      auKilometers : Result:=FSourceArea*1000000;
    end;
  end;

Procedure TPolyPartitionDlg.WriteSourceArea
   (
   AArea           : Double
   );
  begin
    case AreaUnits of
      auMeters     : FSourceArea:=AArea;
      auar         : FSourceArea:=AArea/100;
      auha         : FSourceArea:=AArea/10000;
      auKilometers : FSourceArea:=AArea/1000000;
    end;
  end;

Procedure TPolyPartitionDlg.WriteTargetArea1
   (
   AArea           : Double
   );
  begin
    case AreaUnits of
      auMeters     : FTargetArea1:=AArea;
      auar         : FTargetArea1:=AArea/100;
      auha         : FTargetArea1:=AArea/10000;
      auKilometers : FTargetArea1:=AArea/1000000;
    end;
  end;

Procedure TPolyPartitionDlg.WriteTargetArea2
   (
   AArea           : Double
   );
  begin
    case AreaUnits of
      auMeters     : FTargetArea2:=AArea;
      auar         : FTargetArea2:=AArea/100;
      auha         : FTargetArea2:=AArea/10000;
      auKilometers : FTargetArea2:=AArea/1000000;
    end;
  end;

Function TPolyPartitionDlg.ReadPartDist
   : Double;
  begin
    if OffsetEdit.Text = '' then Result:=0
    else begin
      {FPartDist:=StrToFloat(OffsetEdit.Text)*100;}
      FPartDist:=OffsetValidator.AsFloat*100;
      case DistUnits of
        duMeters     : Result:=FPartDist;
        duKilometers : Result:=FPartDist*1000;
      end;
    end;
  end;

Procedure TPolyPartitionDlg.WritePartDist
   (
   ADist           : Double
   );
  begin
    case DistUnits of
      duMeters     : FPartDist:=ADist;
      duKilometers : FPartDist:=ADist/1000;
    end;
    FPartDist:=FPartDist/100;
    OffsetEdit.Text:=FloatToStrF(FPartDist,ffNumber,15,2);
  end;

Function TPolyPartitionDlg.ReadPartAngle
   : Double;
  begin
    if AngleEdit.Text = '' then Result:=0
    else begin
      FPartAngle:=AngleValidator.AsFloat;
      Result:=FPartAngle;
    end;
  end;

Procedure TPolyPartitionDlg.WritePartAngle
   (
   AAngle          : Double
   );
  begin
    FPartAngle:=AAngle;
    AngleEdit.Text:=FloatToStrF(FPartAngle,ffNumber,15,2);
  end;

Function TPolyPartitionDlg.ReadPartArea
   : Double;
  begin
    if AreaEdit.Text = '' then Result:=0
    else begin
      FPartArea:=AreaValidator.AsFloat;
      case AreaUnits of
        auMeters     : Result:=FPartArea;
        auar         : Result:=FPartArea*100;
        auha         : Result:=FPartArea*10000;
        auKilometers : Result:=FPartArea*1000000;
      end;
    end;
  end;

Procedure TPolyPartitionDlg.WritePartArea
   (
   AArea           : Double
   );
  begin
    case AreaUnits of
      auMeters     : FPartArea:=AArea;
      auar         : FPartArea:=AArea/100;
      auha         : FPartArea:=AArea/10000;
      auKilometers : FPartArea:=AArea/1000000;
    end;
    AreaEdit.Text:=FloatToStrF(FPartArea,ffNumber,15,2);
  end;

Procedure TPolyPartitionDlg.WriteLayerList
   (
   AList           : TStringList
   );
  var i            : Integer;
  begin
    DLayer.Clear;
    for i:=0 to AList.Count-1 do begin
      DLayer.Items.Add(AList[i]);
      if AList[i] = FTLayer then FStartIndex:=i;
    end;
  end;

Function TPolyPartitionDlg.ReadFillArea
   : Boolean;
  begin
    Result:=FillCheck.Checked;
  end;

Procedure TPolyPartitionDlg.WriteFillArea
   (
   AFill           : Boolean
   );
  begin
    FillCheck.Checked:=AFill;
  end;

Function TPolyPartitionDlg.ReadPartMode
   : Integer;
  begin
    if ParallelRadio.Checked then Result:=0
//    else if PufferRadio.Checked then Result:=1
//    else if IslandRadio.Checked then Result:=2
    else Result:=3;
  end;

Procedure TPolyPartitionDlg.WritePartMode
   (
   AMode           : Integer
   );
  begin
    FPartMode:=AMode;
    case AMode of
      pmParallel : ParallelRadio.Checked:=TRUE;
//      pmPuffer   : PufferRadio.Checked:=TRUE;
//      pmIsland   : IslandRadio.Checked:=TRUE;
      pmFree     : FreeRadio.Checked:=TRUE;
    end;
  end;

Function TPolyPartitionDlg.ReadSizeMode
   : Integer;
  begin
    if DistRadio.Checked then Result:=0
    else Result:=1;
  end;

Procedure TPolyPartitionDlg.WriteSizeMode
   (
   AMode           : Integer
   );
  begin
    FSizeMode:=AMode;
    case AMode of
      smDist : DistRadio.Checked:=TRUE;
      smArea : AreaRadio.Checked:=TRUE;
    end;
  end;

Procedure TPolyPartitionDlg.DistRadioClick
   (
   Sender          : TObject
   );
  begin
    OffsetEdit.Enabled:=TRUE;
    AngleEdit.Enabled:=TRUE;
    AreaEdit.Enabled:=FALSE;
  end;

Procedure TPolyPartitionDlg.AreaRadioClick
   (
   Sender          : TObject
   );
  begin
    OffsetEdit.Enabled:=FALSE;
    AngleEdit.Enabled:=TRUE;
    AreaEdit.Enabled:=TRUE;
  end;

Procedure TPolyPartitionDlg.InputButtonClick
   (
   Sender          : TObject
   );
  begin
    if PartMode = pmFree then begin
       AngleEdit.Text:=FloatToStrF(0,ffNumber,15,2);
       OffsetEdit.Text:=FloatToStrF(0,ffNumber,15,2);
    end;
    ReadAllSettings;
    case PartMode of
      pmParallel : begin
                     FProj^.SetActualMenu(mn_PartParallel);
                     MenuFunctions['DrawSnapModeOnOff'].Checked:=TRUE;
                     TMDIChild(FProj^.Parent).DrawSnapModeOnOff;
                     FProj^.SetStatusText(GetLangText(11750),TRUE);
                   end;
      pmFree     : begin
                     FProj^.SetActualMenu(mn_PartFree);
                     FProj^.SetStatusText(GetLangText(11759),TRUE);
                   end;
    end;
    FProj^.InfoPoly^.EndIt(FProj^.PInfo);
    ModalResult:=100;
  end;

Procedure TPolyPartitionDlg.CalcButtonClick
   (                                                       
   Sender          : TObject
   );
  begin
    if (Visible) and (AreaRadio.Checked) and ((ReadPartArea <= 0) or (ReadPartArea >= ReadSourceArea)) then begin
      AreaEdit.SetFocus;
      Exit;
    end;
    ReadAllSettings;
    MenuFunctions['DrawSnapModeOnOff'].Checked:=FALSE;
    TMDIChild(FProj^.Parent).DrawSnapModeOnOff;
    FProj^.InfoPoly^.EndIt(FProj^.PInfo);
    PolyPart.DoPartition(PartMode,SizeMode,Round(PartDist),PartAngle,PartArea);
    ReadAllSettings();
    UpdatePartDlg(FProj);
end;

{
procedure TPolyPartitionDlg.SwapAreas;
var
ADrawPos: TDPoint;
begin
   FProj^.InfoPoly^.EndIt(FProj^.PInfo);
   PolyPart.CalculateRestPoly(PolyPart.ResultPoly);
   ADrawPos.Init(0,0);
   FProj^.InfoPoly^.SetPoly(PolyPart.ResultPoly);
   FProj^.InfoPoly^.StartIt(FProj^.PInfo,ADrawPos,FALSE);
   PolyPart.DoPartition(PartMode,SizeMode,Round(PartDist),PartAngle,PartArea);
//   PolyPart.CalculateRestPoly(PolyPart.RestLine);
//   ReadAllSettings();
   UpdatePartDlg(FProj);
end;
}

procedure TPolyPartitionDlg.OKButtonClick
   (
   Sender          : TObject
   );
  begin
    ReadAllSettings;
    FProj^.InfoPoly^.EndIt(FProj^.PInfo);
    PolyPart.InsertPolyToProject(DLayer.Text);
//    ProcDBDeSelectAll(FProj,TRUE);
  end;

Procedure TPolyPartitionDlg.ReadAllSettings;
  var ALayer       : PLayer;
  begin
    {PolyPart.PolyPartProps.SAreaName:=FAreaName;}
    {PolyPart.PolyPartProps.SArea:=FSourceArea;}
    {PolyPart.PolyPartProps.DArea1:=FTargetArea1;}
    {PolyPart.PolyPartProps.DArea2:=FTargetArea2;}
    {PolyPart.PolyPartProps.SLayerName:=FSLayer;
    ALayer:=FProj^.Layers^.NameToLayer(PolyPart.PolyPartProps.SLayerName);
    PolyPart.PolyPartProps.SLayerIndex:=ALayer^.Index;}
    PolyPart.PolyPartProps.DLayerName:=DLayer.Text;
    ALayer:=FProj^.Layers^.NameToLayer(PolyPart.PolyPartProps.DLayerName);
    if ALayer = nil then
       ALayer:=NewLayerDialog(FProj,GetLangText(10154),PolyPart.PolyPartProps.DLayerName);
    if ALayer <> nil then
       PolyPart.PolyPartProps.DLayerIndex:=ALayer^.Index;
    PolyPart.PolyPartProps.PartDist:=ReadPartDist;
    PolyPart.PolyPartProps.PartAngle:=ReadPartAngle;
    PolyPart.PolyPartProps.PartArea:=ReadPartArea;
    PolyPart.PolyPartProps.FillArea:=ReadFillArea;
    PolyPart.PolyPartProps.PartMode:=ReadPartMode;
    PolyPart.PolyPartProps.SizeMode:=ReadSizeMode;
    PolyPart.PolyPartProps.DDEMode:=FDDEMode;
  end;

procedure TPolyPartitionDlg.CancelButtonClick(Sender: TObject);
begin
    FProj^.InfoPoly^.EndIt(FProj^.PInfo);
//    ProcDBDeSelectAll(FProj,TRUE);
end;

procedure TPolyPartitionDlg.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    FProj^.InfoPoly^.EndIt(FProj^.PInfo);
    FProj^.NewPoly^.EndIt(FProj^.PInfo);
//    ProcDBDeSelectAll(FProj,TRUE);
end;

procedure TPolyPartitionDlg.AreaEditDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
     Accept:=False;
     if Source.ClassNameIs('TLabel') then
        Accept:=True;
end;

procedure TPolyPartitionDlg.AreaEditDragDrop(Sender, Source: TObject; X,
  Y: Integer);
begin
     if Source.ClassNameIs('TLabel') then begin
        TEdit(Sender).Text:=TLabel(Source).Caption;
        CalcButtonClick(Self);
     end;
end;

end.
