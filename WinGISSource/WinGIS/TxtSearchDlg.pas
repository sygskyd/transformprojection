//****************************************************************************
// Unit TxtSearchDlg
//----------------------------------------------------------------------------
// Provides a dialog that allows to search the text objects by their content.
//----------------------------------------------------------------------------
// Modifications
//  24.11.2000, Y.Glukhov, creation
//****************************************************************************
Unit TxtSearchDlg;

Interface

Uses
    Windows, Messages, SysUtils, ProjHndl,
    AM_View, AM_Def, AM_Proj, MultiLng, Objects, ExtCtrls, StdCtrls,
    Spinbtn, WCtrls, Controls, Classes;

Type TTxtSearchHandler = class(TProjMenuHandler)
    Procedure   OnStart; override;
end;

Type
  TTxtSearchDialog   = Class(TWForm)
    Bevel1           : TBevel;
    CancelBtn        : TButton;
    MlgSection       : TMlgSection;
    OKBtn            : TButton;
    ControlPanel1: TPanel;
    ControlPanel: TPanel;
    WLabel3: TWLabel;
    Group1: TWGroupBox;
    ChkSubStr: TCheckBox;
    edtTemplate: TEdit;
    ChkZoomFirst: TCheckBox;
    ChkCaseSens: TCheckBox;
    RadioGrpSelSrcSet: TRadioGroup;
    chkWildcards: TCheckBox;

    Procedure   FormShow(Sender: TObject);
    Procedure   FormHide(Sender: TObject);
    Procedure   FormCreate(Sender: TObject);
    Procedure   FormDestroy(Sender: TObject);

  Private
    bInitialized    : Boolean;

    Procedure   InitFormData;

  Public
    Project         : PProj;

  end;

var
// To save the last searching parameters
  TSLastTxtTemplate : String = '';
  TSLastChkSubStr   : Boolean = True;
  TSLastChkWildcards: Boolean = True;
  TSLastChkCaseSens : Boolean = True;
  TSLastGrpSelSrcSet: Integer = 1;
  TSLastChkZoomFirst: Boolean = False;

Implementation

{$R *.DFM}

Uses AM_Index, AM_Text, AM_Layer, AM_ProjO, AM_Admin, MenuFn;

Procedure TTxtSearchHandler.OnStart;
var
  SrcDialog   : TTxtSearchDialog;
  Selected    : PCollection;
  i           : Integer;
  Item        : PIndex;
  TxtItem     : PText;
  hcur        : integer;

//++ Glukhov Bug#387 Build#152 17.04.01
  function CollectObjects( AItem: PIndex ): Boolean; Far;
  var
    bSel      : Boolean;
    Idx       : PIndex;
    Layer     : PLayer;
    i         : Integer;
  begin
    CollectObjects:= False;
    bSel:= False;
    TxtItem:= Pointer( Project.Layers.IndexObject( Project.PInfo, AItem ) );
    if TxtItem.TemplateMatched( TSLastTxtTemplate, TSLastChkSubStr, TSLastChkCaseSens, TSLastChkWildcards )
    then  bSel:= True;
    if bSel and (TSLastGrpSelSrcSet = 2) then
    begin // whole project
      bSel:= False;
      for i:=1 to Project.Layers.LData.Count-1 do begin
        Layer:= Project.Layers.LData.At(i);
        if not Layer^.GetState( sf_LayerOff ) then begin
          Idx:= Layer.HasObject( TxtItem );
          if Assigned( Idx ) then begin
            bSel:= True;
            break;
          end;
        end;
      end;
    end;
    if bSel  then Selected.Insert( TxtItem );
  end;
//-- Glukhov Bug#387 Build#152 17.04.01

begin
  Project.SetActualMenu( mn_Select );
  SrcDialog:= TTxtSearchDialog.Create( Project.Parent );
  SrcDialog.Project:= Project;
  i:= SrcDialog.ShowModal;
  SrcDialog.Free;
  if i = mrOK then begin
    Selected:= New( PCollection, Init( 16, 16 ) );
  // Seach the text
    hcur := Project.PInfo.SetTheCursor(crHourGlass);
    case TSLastGrpSelSrcSet of
      0: begin  // selected objects
        Project.Layers.SelLayer.FirstObjectType( ot_Text, @CollectObjects );
      end;
      1: begin  // current layer
        Project.Layers.TopLayer.FirstObjectType( ot_Text, @CollectObjects );
      end;
      2: begin  // whole project
        PLayer( Project.PInfo.Objects).FirstObjectType( ot_Text, @CollectObjects );
      end;
    end;
    Project.PInfo.SetTheCursor( hcur );

    if Selected.Count = 0 then begin
    // No such objects - Exit
      MsgBox( Project.PInfo.HWindow, 11295, Mb_IconExclamation or Mb_OK, '' );
      Dispose( Selected );
      Exit;
    end;

  // Clear the current selection
    if Project.Layers.SelLayer.Data.GetCount > 0
    then  Project.DeSelectAll( False );
  // Mark the found objects as the selected ones
    for i:=0 to Selected.Count-1 do begin
      Item:= Selected.At( i );
      {$IFNDEF WMLT}
      SelectIndexOnAllLayers( Project, Item );
      {$ENDIF}
    end;
  // Save TopLayer to prerare data for procedure SetTransparentSelect (am_proc)
    Project.TSOldActiveLayer:= Project.Layers.TopLayer;
  // Check the transparency of selection
    {$IFNDEF WMLT}
    if SelectionOnTopLayer( Project )
    then  Project.Layers.TranspLayer:= False
    else  Project.Layers.TranspLayer:= True;
    {$ENDIF}
    MenuFunctions['SelectTransparent'].Checked:= Project.Layers.TranspLayer;

    {$IFNDEF WMLT}
    if TSLastChkZoomFirst then begin
    // Zoom in the first selected object
      Project.ShowNum:= 0;
      ProcShowNextSel( Project );
    end;
    {$ENDIF}
    Selected.DeleteAll;
    Dispose( Selected );

  end;
end;

Procedure TTxtSearchDialog.InitFormData;
  begin
    edtTemplate.Text:= TSLastTxtTemplate;
    ChkSubStr.Checked:= TSLastChkSubStr;
    ChkWildcards.Checked:= TSLastChkWildcards;
    ChkCaseSens.Checked:= TSLastChkCaseSens;
    RadioGrpSelSrcSet.ItemIndex:= TSLastGrpSelSrcSet;
    ChkZoomFirst.Checked:= TSLastChkZoomFirst;
  end;

Procedure TTxtSearchDialog.FormShow(Sender: TObject);
  begin
    if bInitialized then Exit;
    edtTemplate.SetFocus;
    bInitialized:= True;
  end;

Procedure TTxtSearchDialog.FormHide(Sender: TObject);
  begin
    if ModalResult=mrOK then begin
      TSLastTxtTemplate:= edtTemplate.Text;
      TSLastChkSubStr:= ChkSubStr.Checked;
      TSLastChkWildcards:= ChkWildcards.Checked;
      TSLastChkCaseSens:= ChkCaseSens.Checked;
      TSLastGrpSelSrcSet:= RadioGrpSelSrcSet.ItemIndex;
      TSLastChkZoomFirst:= ChkZoomFirst.Checked;
    end;
    bInitialized:= False;
  end;

Procedure TTxtSearchDialog.FormCreate(Sender: TObject);
  begin
    InitFormData;
    bInitialized:= False;
  end;

Procedure TTxtSearchDialog.FormDestroy(Sender: TObject);
  begin
    bInitialized:= False;
  end;

//=============================================================================
//  Initialization
//=============================================================================
initialization
  begin
      TTxtSearchHandler.Registrate('ExtrasTextSelect');
  end;

end.
