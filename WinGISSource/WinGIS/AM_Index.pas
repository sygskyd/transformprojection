{****************************************************************************}
{ Unit AM_Index                                                              }
{----------------------------------------------------------------------------}
{                                                                            }
{----------------------------------------------------------------------------}
{ Modifikationen                                                             }
{  16.02.1994 Martin Forst                                                   }
{  20.07.1995 Martin Forst                                                   }
{****************************************************************************}
unit AM_Index;

interface

uses AM_Def, AM_Paint, Objects, WinTypes, WinProcs;

const
  IndexFileVersion = 1; { Ab Flaeche mit Symbolen fuellen }

type
  PObjStyleV2 = ^TObjStyleV2;
  TObjStyleV2 = record
    LineStyle: TLineStyle;
    FillStyle: TFillStyle;
    SymbolFill: TSymbolFill;
  end;

  PIndex = ^TIndex;
  TIndex = object(TOldObject)
    Index: LongInt;
    ClipRect: TDRect;
    MPtr: Pointer;
    State: LongInt;
    ObjectStyle: PObjStyleV2;
    Tooltip: PString; { 03-140199-001 }
    constructor Init(AIndex: LongInt);
    procedure InitBy(AIndex: PIndex);
    constructor Load(S: TOldStream);
    destructor Done; virtual;
    function CheckMove(PInfo: PPaint; XMove, YMove: LongInt): Boolean; virtual;
       //changed by Brovak
    procedure ChangeStyle(PInfo: PPaint; const AStyle: TObjStyleV2; DoUpdateUseCount: Boolean = TRUE; CheckAllFlag: boolean = true);
    procedure CopyObjectStyleTo(Dest: PIndex);
    function GetObjType: Word; virtual;
    procedure GetSize(var ABorder: TDRect); virtual;
    function GetState(AState: LongInt): Boolean;
    function HasLineStyle: Boolean;
    function HasPattern: Boolean;
    function HasSymbolFill: Boolean;
    procedure Invalidate(PInfo: PPaint; FromGraphic: Boolean = TRUE); virtual;
    procedure MoveRel(XMove, YMove: LongInt); virtual;
    procedure Scale(XScale, YScale: Real); virtual;
    function SelectBetweenPolys(PInfo: PPaint; Poly1, Poly2: Pointer; const ObjTypes: TObjectTypes;
      Inside: Boolean): Boolean; virtual;
    function SearchForPoint(PInfo: PPaint; InCircle: Pointer; Exclude: LongInt; const ObjTypes: TObjectTypes;
      var Point: TDPoint; var SnObje: PIndex): Boolean; virtual;
    function SelectByCircle(PInfo: PPaint; Middle: TDPoint;
      Radius: LongInt; const ObjType: TObjectTypes; Inside: Boolean): Boolean; virtual;
    function SelectByNeighbour(PInfo: PPaint; Item: PIndex): Boolean; virtual;
    function SelectByPoint(PInfo: PPaint; Point: TDPoint; const ObjType: TObjectTypes): PIndex; virtual;
    function SelectByPoly(PInfo: PPaint; Poly: Pointer; const ObjType: TObjectTypes; Inside: Boolean): Boolean; virtual;
    function SelectByPolyLine(PInfo: PPaint; Poly: Pointer; const ObjType: TObjectTypes): Boolean; virtual;
    function SelectByRect(PInfo: PPaint; Rect: TDRect; Inside: Boolean): Boolean; virtual;
    procedure SetState(AState: LongInt; ASet: Boolean); virtual;
    procedure Store(S: TOldStream); virtual;
    function SearchForNearestPoint(PInfo: PPaint; var ClickPos: TDPoint; InCircle: Pointer;
      Exclude: Longint; const ObjType: TObjectTypes; var point: TDPoint; var SnObje: PIndex): Boolean; virtual;
    function Visible(PInfo: PPaint): Boolean;
    function NeedClipping(PInfo: PPaint): Boolean;
    function HasAllLayerStyle: Boolean;
  end;

implementation

uses AM_Layer, AM_Text, AM_Circl, NumTools, StyleDef;

constructor TIndex.Init
  (
  AIndex: LongInt
  );
begin
  inherited Init;
  Index := AIndex;
  State := sf_NewVersion;
  ClipRect.Init;
  ClipRect.Assign(0, 0, 0, 0);
  ObjectStyle := nil;
  Mptr := nil;
  Tooltip := nil; {03-140199-001}
end;

constructor TIndex.Load(S: TOldStream);
var
  Tmp: Byte;
  Version: Byte;
  PatColor: TColorRef;
  AType: Integer16;
  Transparent: Boolean;
begin
  ObjectStyle := nil;
  MPtr := nil;
  S.Read(Index, SizeOf(Index));
  S.Read(State, SizeOf(State));
  State := State and not (sf_UnVisible);
  if State and sf_NewVersion <> 0 then
  begin
    S.Read(Version, SizeOf(Version));
    if State and sf_OtherStyle <> 0 then
    begin
      ObjectStyle := New(PObjStyleV2);
      with ObjectStyle^ do
      begin
        if Version > 0 then
        begin
          S.Read(LineStyle, SizeOf(LineStyle));
          S.Read(FillStyle, SizeOf(FillStyle));
          S.Read(SymbolFill, SizeOf(SymbolFill));
        end
        else
        begin
          LineStyle := DefaultLineStyle;
          with LineStyle do
          begin
            S.Read(Color, SizeOf(Color));
            if Color = $1000000 then
              Color := clFromLayer;
            S.Read(Style, SizeOf(Style));
            S.Read(AType, SizeOf(AType));
            Width := AType;
            WidthType := stScaleIndependend;
          end;
          FillStyle := DefaultFillStyle;
          with FillStyle do
          begin
            S.Read(Pattern, SizeOf(Pattern));
            S.Read(ForeColor, SizeOf(ForeColor));
            if ForeColor = $1000000 then
              ForeColor := clFromLayer;
            S.Read(Transparent, SizeOf(Transparent));
            if Transparent then
              BackColor := clNone
            else
              BackColor := RGBColors[c_White];
          end;
          SymbolFill := DefaultSymbolFill;
        end;
      end;
    end;
  end
  else
  begin
    S.Read(Tmp, SizeOf(Tmp));
    S.Read(Tmp, Sizeof(Tmp));
    if Tmp = $FF then
      S.Read(PatColor, SizeOf(PatColor));
    if GetObjType = ot_Layer then
      PLayer(@Self)^.Text := S.ReadStr
    else
      if TypeOf(Self) = TypeOf(TText) then
        PText(@Self)^.Text := S.ReadStr
      else
        S.ReadStr;
  end;
  if ProjectVersion > 9 then
    ClipRect.Load(S);
  Tooltip := nil; {03-140199-001}
  if ProjectVersion > 20 then
    Tooltip := S.ReadStr;
end;

destructor TIndex.Done;
begin
  if ObjectStyle <> nil then
    Dispose(ObjectStyle);
  inherited Done;
end;

function TIndex.GetState
  (
  AState: LongInt
  )
  : boolean;
begin
  if (AState = sf_OtherStyle) and ((State and AState) <> 0) and (ObjectStyle = nil) then
  begin
    SetState(AState, False);
  end;
  GetState := (State and AState) <> 0;
end;

procedure TIndex.CopyObjectStyleTo(Dest: PIndex);
begin
  {++Sygsky:  Dest:=nil; }
  if (Dest <> nil) then
    if (not GetState(sf_OtherStyle)) then
    begin
      if (Dest^.ObjectStyle <> nil) then
      begin
        Dispose(Dest^.ObjectStyle); { eventuell eigenen Stil von Dest l�schen }
        Dest^.ObjectStyle := nil;
        Dest^.State := Dest^.State and (not sf_OtherStyle);
      end;
    end
    else
    begin
      if Dest^.ObjectStyle = nil then
        Dest^.ObjectStyle := new(PObjStyleV2); { neuen ObjektStil erzeugen}
      Dest^.State := Dest^.State or sf_OtherStyle; { f�r Speicherung setzen }
      Dest^.ObjectStyle^ := ObjectStyle^;
    end;
end;

procedure TIndex.Scale
  (
  XScale: Real;
  YScale: Real
  );
begin
end;

procedure TIndex.SetState
  (
  AState: LongInt;
  ASet: Boolean
  );
begin
  if ASet then
    State := State or AState
  else
    State := State and not (AState);
end;

procedure TIndex.Store
  (
  S: TOldStream
  );
var
  AState: Longint;
  Version: Byte;
begin
  State := State or sf_NewVersion;
  S.Write(Index, SizeOf(Index));
  AState := State and not (sf_Selected);
  S.Write(AState, SizeOf(AState));
  Version := IndexFileVersion;
  S.Write(Version, SizeOf(Version));
  if GetState(sf_OtherStyle) then
    with ObjectStyle^ do
    begin
      S.Write(LineStyle, SizeOf(LineStyle));
      S.Write(FillStyle, SizeOf(FillStyle));
      S.Write(SymbolFill, SizeOf(SymbolFill));
    end;
  ClipRect.Store(S);
  S.WriteStr(Tooltip); {03-140199}
     {+++ Brovak Bug 541 build 168 - show progress bar while saving project}
  PercentNowPut := PercentNowPut + 1;
     {-- Brovak}
end;

function TIndex.SelectByPoint
  (
  PInfo: PPaint;
  Point: TDPoint;
  const ObjType: TObjectTypes
  )
  : PIndex;
begin
  SelectByPoint := nil;
end;

function TIndex.SelectByRect
  (
  PInfo: PPaint;
  Rect: TDRect;
  Inside: Boolean
  )
  : Boolean;
begin
  SelectByRect := FALSE;
end;

function TIndex.SelectByNeighbour
  (
  PInfo: PPaint;
  Item: PIndex
  )
  : Boolean;
begin
  SelectByNeighbour := FALSE;
end;

function TIndex.SelectByCircle
  (
  PInfo: PPaint;
  Middle: TDPoint;
  Radius: LongInt;
  const ObjType: TObjectTypes;
  Inside: Boolean
  )
  : Boolean;
begin
  SelectByCircle := FALSE;
end;

procedure TIndex.MoveRel
  (
  XMove: LongInt;
  YMove: LongInt
  );
begin
  Abstract;
end;

function TIndex.SearchForPoint
  (
  PInfo: PPaint;
  InCircle: Pointer;
  Exclude: LongInt;
  const ObjTypes: TObjectTypes;
  var Point: TDPoint;
  var SnObje: PIndex
  )
  : Boolean;
begin
  SearchForPoint := FALSE;
end;

procedure TIndex.Invalidate
  (
  PInfo: PPaint;
  FromGraphic: Boolean
  );
begin
end;

function TIndex.GetObjType
  : Word;
begin
  if Index < mi_RText then
    GetObjType := ot_Text
  else
    if Index < mi_BusGraph then
      GetObjType := ot_RText
    else
      if Index < mi_MesLine then
        GetObjType := ot_BusGraph
      else
        if Index < mi_Symbol then
          GetObjType := ot_MesLine
        else
          if Index < mi_Circle then
            GetObjType := ot_Symbol {0504F}
          else
            if Index < mi_Arc then
              GetObjType := ot_Circle {0504F}
            else
              if Index < mi_Pixel then
                GetObjType := ot_Arc
              else
                if Index < mi_Group then
                  GetObjType := ot_Pixel
                else
                  if Index < mi_Spline then
                    GetObjType := ot_Group
                  else
                    if Index < mi_Poly then
                      GetObjType := ot_Spline
                    else
                      if Index < mi_CPoly then
                        GetObjType := ot_Poly
                      else
                        if Index < mi_Image then
                          GetObjType := ot_CPoly
                        else
                          if Index < mi_OleObj then
                            GetObjType := ot_Image {2007F}
                          else
                            GetObjType := ot_OleObj; {2007F}
end;

function TIndex.CheckMove
  (
  PInfo: PPaint;
  XMove: LongInt;
  YMove: LongInt
  )
  : Boolean;
begin
  CheckMove := TRUE;
end;

procedure TIndex.GetSize
  (
  var ABorder: TDRect
  );
begin
  ABorder.CorrectByRect(ClipRect);
end;

function TIndex.SelectByPoly
  (
  PInfo: PPaint;
  Poly: Pointer;
  const ObjType: TObjectTypes;
  Inside: Boolean
  )
  : Boolean;
begin
  SelectByPoly := FALSE;
end;

function TIndex.SelectByPolyLine
  (
  PInfo: PPaint;
  Poly: Pointer;
  const ObjType: TObjectTypes
  )
  : Boolean;
begin
  SelectByPolyLine := FALSE;
end;

{******************************************************************************+
  Procedure TIndex.ChangeStyle
--------------------------------------------------------------------------------
  Autor: Martin Forst
--------------------------------------------------------------------------------
  Sets a new style for the object. !?!?Caution! other than the version of
  this function in WinGIS 3 the function currently only copies the new
  style so no XXXNoChange-flags my be used.
+******************************************************************************}

procedure TIndex.ChangeStyle(PInfo: PPaint; const AStyle: TObjStyleV2; DoUpdateUseCount: Boolean; CheckAllFlag: boolean);
begin
  with AStyle do
  begin
    if ObjectStyle = nil then
    begin
      ObjectStyle := New(PObjStyleV2);
      with ObjectStyle^ do
      begin
        LineStyle := DefaultLineStyle;
        FillStyle := DefaultFillStyle;
        SymbolFill := DefaultSymbolFill;
      end;
    end
    else
      if DoUpdateUseCount then
        with ObjectStyle^ do
          UpdateUseCounts(PInfo, LineStyle, FillStyle, SymbolFill, FALSE);
    ObjectStyle^ := AStyle;
    if (CheckAllFlag = true) and (HasAllLayerStyle = true) then
    begin;
      Dispose(ObjectStyle);
      ObjectStyle := nil;
      SetState(sf_OtherStyle, FALSE);
    end
    else
    begin
      SetState(sf_OtherStyle, TRUE);
      if DoUpdateUseCount then
        UpdateUseCounts(PInfo, LineStyle, FillStyle, SymbolFill, TRUE);
    end;
  end;
end;

function TIndex.HasLineStyle: Boolean;
begin
  Result := not (GetObjType in [ot_Text, ot_RText, ot_BusGraph]);
end;

function TIndex.HasSymbolFill: Boolean;
begin
  Result := GetObjType in [ot_CPoly, ot_Circle];
end;

function TIndex.HasPattern: Boolean;
begin
  Result := GetObjType in [ot_CPoly, ot_Circle, ot_Symbol];
end;

function TIndex.SearchForNearestPoint
  (
  PInfo: PPaint;
  var ClickPos: TDPoint;
  InCircle: Pointer;
  Exclude: LongInt;
  const ObjType: TObjectTypes;
  var Point: TDPoint;
  var SnObje: PIndex
  )
  : Boolean;
begin
  Result := FALSE;
end;

function TIndex.SelectBetweenPolys
  (
  PInfo: PPaint;
  Poly1: Pointer;
  Poly2: Pointer;
  const ObjTypes: TObjectTypes;
  Inside: Boolean
  )
  : Boolean;
begin
  SelectBetweenPolys := FALSE;
end;

function TIndex.Visible
  (
  PInfo: PPaint
  )
  : Boolean;
begin
  Result := PInfo^.RepaintArea.IsVisible(ClipRect);
end;

function TIndex.NeedClipping
  (
  PInfo: PPaint
  )
  : Boolean;
var
  Point: TPoint;
begin
  with PInfo^ do
  begin
    ConvertToDisp(Self.ClipRect.A, Point);
    if not ptInRect(ClipLimits, Point) then
    begin
      Result := TRUE;
      Exit;
    end;
    ConvertToDisp(Self.ClipRect.B, Point);
    if not ptInRect(ClipLimits, Point) then
    begin
      Result := TRUE;
      Exit;
    end;
  end;
  Result := FALSE;
end;

{******************************************************************************+
  Function TIndex.HasAllLayerStyle
--------------------------------------------------------------------------------
  Determines if all object-styles are set to the layer-style. This is the case
  if the ObjectStyle-pointer is nil or all settings in the ObjectStyle are
  set to FromLayer.
+******************************************************************************}

function TIndex.HasAllLayerStyle
  : Boolean;
begin
  Result := FALSE;
  if ObjectStyle <> nil then
    with ObjectStyle^ do
    begin
      if HasLineStyle then
        with LineStyle do
        begin
          if Style <> ltFromLayer then
            Exit;
          if DblCompare(Width, dblFromLayer) <> 0 then
            Exit;
          if DblCompare(Scale, dblFromLayer) <> 0 then
            Exit;
          if Color <> clFromLayer then
            Exit;
          if FillColor <> clFromLayer then
            Exit;
        end;
      if HasPattern then
        with FillStyle do
        begin
          if Pattern <> ptFromLayer then
            Exit;
          if ForeColor <> clFromLayer then
            Exit;
          if BackColor <> clFromLayer then
            Exit;
          if DblCompare(Scale, dblFromLayer) <> 0 then
            Exit;
        end;
      if HasSymbolFill then
        with SymbolFill do
        begin
          if FillType <> symfFromLayer then
            Exit;
{!?!?!Check the following fields if they are from layer
       Symbol           : LongInt;
       Size             : Double;
       SizeType         : Integer8;
       SymbolAngle      : Double;
       DistanceType     : Integer8;
       Distance         : Double;
       LineDistance     : Double;
       Offset           : Double;
       OffsetType       : Integer8;
       Angle            : Double;
       }

          with LineStyle do
          begin
            if Style <> ltFromLayer then
              Exit;
            if DblCompare(Width, dblFromLayer) <> 0 then
              Exit;
            if DblCompare(Scale, dblFromLayer) <> 0 then
              Exit;
            if Color <> clFromLayer then
              Exit;
            if FillColor <> clFromLayer then
              Exit;
          end;
          with FillStyle do
          begin
            if Pattern <> ptFromLayer then
              Exit;
            if ForeColor <> clFromLayer then
              Exit;
            if BackColor <> clFromLayer then
              Exit;
            if DblCompare(Scale, dblFromLayer) <> 0 then
              Exit;
          end;
        end;
    end;
  Result := TRUE;
end;

procedure TIndex.InitBy(AIndex: PIndex);
begin

  Index := AIndex^.Index;
  State := AIndex^.State;
  ObjectStyle := nil;
  ClipRect.InitByRect(AIndex^.ClipREct);
  MPtr := AIndex^.Mptr;
end;

const
  RIndex: TStreamRec = (
    ObjType: rn_Index;
    VmtLink: TypeOf(TIndex);
    Load: @TIndex.Load;
    Store: @TIndex.Store);

begin
  RegisterType(RIndex);
end.

