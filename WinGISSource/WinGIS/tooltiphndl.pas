Unit TooltipHndl;

Interface

Uses MenuHndl,Tooltips;

Type TEditTooltipHandler = Class(TMenuHandler)
      Public
       Procedure  OnStart; override;
     end;

     TDeleteTooltipHandler = Class(TMenuHandler)
      Public
       Procedure  OnStart; override;
     end;

     TFixTooltipHandler = Class(TMenuHandler)
      Public
       Procedure  OnStart; override;
     end;

     {++Brovak Bug 106 BUILD 135}
       TSetChangeForSelectedHandler = Class(TMenuHandler)
      Public
       Procedure  OnStart; override;
     end;
     {--Brovak}


Implementation

{$R *.mfn}

Uses MenuFn;

{==============================================================================+
  TEditTooltipHandler
+==============================================================================}

Procedure TEditTooltipHandler.OnStart;
begin
  TooltipThread.ChangeCurrentFloatingText;
end;

{==============================================================================+
  TDeleteTooltipHandler
+==============================================================================}

Procedure TDeleteTooltipHandler.OnStart;
begin
  TooltipThread.DeleteCurrentFloatingText;
end;

{==============================================================================+
  TFixTooltipHandler
+==============================================================================}

Procedure TFixTooltipHandler.OnStart;
begin
  TooltipThread.FixFloatingText;
end;

 {++Brovak Bug 106 BUILD 135}
{==============================================================================+
  TSetChangeForSelectedHandler
+==============================================================================}

Procedure TSetChangeForSelectedHandler.OnStart;
begin
  TooltipThread.ChangeCurrentFloatingTextForSelected;
end;
  {--Brovak }


{==============================================================================+
  Initialization and Terminationcode
+==============================================================================}

Initialization
  MenuFunctions.RegisterFromResource(HInstance,'TooltipHndl','TooltipHndl');
  TEditTooltipHandler.Registrate('EditTooltip');
  TDeleteTooltipHandler.Registrate('DeleteTooltip');
  TFixTooltipHandler.Registrate('FixTooltip');
   {++Brovak Bug 106 BUILD 135}
  TSetChangeForSelectedHandler.Registrate('SetChangeForSelected');
   {--Brovak}

end.
