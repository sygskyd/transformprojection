{****************************************************************************}
{ Unit AM_Sym                                                                }
{----------------------------------------------------------------------------}
{                                                                            }
{----------------------------------------------------------------------------}
{ Modifikationen                                                             }
{  16.02.1994 Martin Forst                                                   }
{  10.11.1996 Raimund Leitner                                                }
{  01.03.1997 Raimund Leitner     Symbolnamen                                }
{                                 Symbolbibliothek                           }
{****************************************************************************}
unit AM_Sym;

interface

uses WinTypes, WinProcs, Controls, SysUtils, Dialogs, Objects, AM_Def, AM_Point,
  AM_Paint, AM_Index, AM_Layer, AM_View, AM_Group, AM_CPoly, AM_Poly, AM_Circl,
  AM_Text, AM_Splin, MultiLng, UserIntf;

//++ Glukhov Bug#167 BUILD#125 14.11.00
//Const NewSymLimit  = 50;       { Grenze bis zu der neue Symbole nach dem Schema }
//                               { 'Neues Symbol (xx)' erzeugt werden }
const
  NewSymLimit = 32767; // The limit was increased
//-- Glukhov Bug#167 BUILD#125 14.11.00
  SymFileVersion = 1; { Version f�r TSymbol }

type
  PSymbol = ^TSymbol;
  TSymbol = object(TView)
  public
    Position: TDPoint;
    SymIndex: LongInt;
    SymPtr: PSGroup;
    Size: Double;
    SizeType: Integer8;
    Angle: Double;
    constructor Init(PInfo: PPaint; DPos: TDPoint; ASym: LongInt; AScale, AAngle: Double);
    constructor InitName(PInfo: PPaint; DPos: TDPoint; ASym, ALib: string;
      const ASize: Double; ASizeType: Integer8; const AAngle: Double);
    constructor Load(S: TOldStream);
    procedure CalculateClipRect(PInfo: PPaint);
    procedure Draw(PInfo: PPaint; Clipping: Boolean); virtual;
    procedure GetBorder(PInfo: PPaint; APoly: PCPoly);
    function GetObjType: Word; virtual;
    function GetSymbol(PInfo: PPaint): PSGroup;
    function GivePosX: LongInt; virtual;
    function GivePosY: LongInt; virtual;
    procedure MoveRel(XMove, YMove: LongInt); virtual;
    function SelectBetweenPolys(PInfo: PPaint; Poly1, Poly2: Pointer; const ObjType: TObjectTypes;
      Inside: Boolean): Boolean; virtual;
    function SearchForPoint(PInfo: PPaint; InCircle: Pointer; Exclude: LongInt; const ObjType: TObjectTypes;
      var Point: TDPoint; var SnObje: PIndex): Boolean; virtual;
    function SelectByCircle(PInfo: PPaint; Middle: TDPoint;
      Radius: LongInt; const ObjType: TObjectTypes; Inside: Boolean): Boolean; virtual;
    function SelectByPoint(PInfo: PPaint; Point: TDPoint; const ObjType: TObjectTypes): PIndex; virtual;
    function SelectByPoly(PInfo: PPaint; Poly: Pointer; const ObjType: TObjectTypes; Inside: Boolean): Boolean; virtual;
    function SelectByPolyLine(PInfo: PPaint; Poly: Pointer; const ObjType: TObjectTypes): Boolean; virtual;
    procedure Store(S: TOldStream); virtual;
    function SearchForNearestPoint(PInfo: PPaint; var ClickPos: TDPoint; InCircle: Pointer;
      Exclude: Longint; const ObjType: TObjectTypes; var point: TDPoint; var SnObje: PIndex): Boolean; virtual;
    procedure SetSymPtr(Symbols: Pointer);
    function CalculateScale(PInfo: PPaint): Double;
//++ Glukhov BugCorrection BUILD#121 30.06.00
    function UsedDefaultAngle: Boolean;
//-- Glukhov BugCorrection BUILD#121 30.06.00
    function CalculateAngle: Double;
    procedure SetSymPtrByIndex(Symbols: Pointer; myIndex: integer);
    function GetSymPtrExt(AItem: PIndex): PSGroup;
  end;

{******************************************************************************}
{ Procedure ProcCreateSymbolFromSelection                                      }
{ Erzeugt aus Selektion ein Symbol und f�gt es in die                          }
{ Symbolbibliothek ein                                                         }
procedure ProcCreateSymbolFromSelection(PProject: Pointer);

{******************************************************************************}
{ Function ObjectToSymObject                                                   }
{ Erzeugt eine Kopie des symbol�quivalenten Typs von Item (mit Stil!!)         }
{ TPoly -> TSPoly etc.                                                         }
function ObjectToSymObject(AProj: Pointer; Item: PIndex): PView;

{******************************************************************************}
{ Function SymObjectToObject                                                   }
{ Erzeugt eine Kopie des Symbolelementes Item f�r WinGIS-Editor                }
{ TSPoly -> TPoly, etc.                                                        }
function SymObjectToObject(AProj: Pointer; Item: PView): PView;

implementation

uses AM_Proj, AM_Obj, ResDlg, SymLib, BMPImage, NumTools, ExtLib, AM_Main;

constructor TSymbol.Init
  (
  PInfo: PPaint;
  DPos: TDPoint;
  ASym: LongInt;
  AScale: Double;
  AAngle: Double
  );
var
  AIndex: PIndex;
begin
  inherited Init;
  Position.Init(DPos.X, DPos.Y);
  SymIndex := ASym;
  AIndex := New(PIndex, Init(ASym));
  SymPtr := PSGroup(PLayer(PInfo^.Symbols)^.IndexObject(PInfo, AIndex));
  Dispose(AIndex, Done);
  Size := AScale;
  SizeType := stPercent;
  Angle := AAngle;
  CalculateClipRect(PInfo);
end;

constructor TSymbol.InitName(PInfo: PPaint; DPos: TDPoint; ASym, ALib: string;
  const ASize: Double; ASizeType: Integer8; const AAngle: Double);
var
  i: Integer;
  ExtLibName: string;
  AExtLib: PExtLib;
  ExtSym: PSGroup;
  ps: PString;
begin
  SymPtr := nil;
  SymIndex := 0;
  ASym := Trim(ASym);
  ALib := Trim(ALib);
  if (ASym = '') then
  begin
    Fail;
  end;
  ALib := Lowercase(ExtractFileName(ALib));
  inherited Init;
  Position.Init(DPos.X, DPos.Y);
  if (ALib <> '') and (ALib <> MlgStringList.Strings['SymbolRollup', 17]) then
  begin
    for i := 0 to PSymbols(PInfo^.Symbols)^.ExternLibs.Count - 1 do
    begin
      AExtLib := PExtLib(PSymbols(PInfo^.Symbols)^.ExternLibs.Items[i]);
      ExtLibName := LowerCase(ExtractFileName(AExtLib^.LibFileName^));
      if StrComp(PChar(AnsiString(ExtLibName)), PChar(AnsiString(ALib))) = 0 then
      begin
        if AExtLib^.Symbols <> nil then
        begin
          ps := NewStr(ASym);
          ExtSym := AExtLib^.GetSymbol(ps);
          Dispose(ps);
          if ExtSym <> nil then
          begin
            PSymbols(PInfo^.Symbols)^.CopySymIntoProject(PInfo, ExtSym);
//                   PProj(PInfo^.AProj)^.ActualSym^.LibName^:='';
          end;
        end;
        break;
      end;
    end;
  end;
  SymPtr := PSymbols(PInfo^.Symbols)^.GetProjectSymbol(ASym, '');
  if (SymPtr <> nil) then
  begin
    SymIndex := SymPtr^.Index
  end
  else
  begin
    fail;
  end;
  Size := ASize;
  SizeType := ASizeType;
  Angle := AAngle;
  CalculateClipRect(PInfo);
end;

procedure TSymbol.CalculateClipRect
  (
  PInfo: PPaint
  );
var
  Border: PCPoly;
begin
  Border := New(PCPoly, Init);
  GetBorder(PInfo, Border);
  ClipRect := Border^.ClipRect;
  Dispose(Border, Done);
end;

constructor TSymbol.Load
  (
  S: TOldStream
  );
var
  Version: Byte;
  ScaleFact: Real48;
  OldAngle: Real48;
begin
  inherited Load(S);
  Position.Load(S);
  S.Read(SymIndex, SizeOf(SymIndex));
  if GetState(sf_SymName) then
  begin
    S.Read(Version, SizeOf(Version));
    S.Read(Size, SizeOf(Size));
    S.Read(SizeType, SizeOf(SizeType));
    S.Read(Angle, SizeOf(Angle));
  end
  else
  begin
    S.Read(ScaleFact, SizeOf(ScaleFact));
    Size := ScaleFact;
    SizeType := stPercent;
    S.Read(OldAngle, SizeOf(OldAngle));
    Angle := OldAngle;
  end;
  SymPtr := nil;
end;

procedure TSymbol.Store
  (
  S: TOldStream
  );
var
  Version: Byte;
begin
  if GetState(sf_SymName) then
    inherited Store(S)
  else
  begin
    SetState(sf_SymName, true);
    inherited Store(S);
    SetState(sf_SymName, false);
  end;
  Position.Store(S);
  S.Write(SymIndex, SizeOf(SymIndex));
  // store version for later use
  Version := SymFileVersion;
  S.Write(Version, SizeOf(Version));
  S.Write(Size, SizeOf(Size));
  S.Write(SizeType, SizeOf(SizeType));
  S.Write(Angle, SizeOf(Angle));
end;

{******************************************************************************+
  Function TSymbol.CalculateScale
--------------------------------------------------------------------------------
  Calculates the scale necessary to draw the symbol.
+******************************************************************************}

function TSymbol.CalculateScale(PInfo: PPaint): Double;
begin
  try
  // stPercent->Size specifies the percentage of the default-size
    if SizeType = stPercent then
      Result := PInfo^.CalculateSize(
        SymPtr^.DefaultSizeType, SymPtr^.DefaultSize) * Size / SymPtr^.RefLineLength
  // default-size specified
    else
      if DblCompare(Size, dblFromLayer) <= 0 then
        Result := PInfo^.CalculateSize(
          SymPtr^.DefaultSizeType, SymPtr^.DefaultSize) / SymPtr^.RefLineLength
  // size specified here
      else
        Result := PInfo^.CalculateSize(SizeType, Size) / SymPtr^.RefLineLength;
  except
    Result := 1;
  end;
end;

//++ Glukhov BugCorrection BUILD#121 30.06.00

function TSymbol.UsedDefaultAngle: Boolean;
begin
  if DblCompare(Angle, dblNone) < 0 then
    Result := True
  else
    Result := False;
end;

function TSymbol.CalculateAngle: Double;
begin
  if UsedDefaultAngle then
    Result := SymPtr^.DefaultAngle
  else
    Result := Angle;
end;
//-- Glukhov BugCorrection BUILD#121 30.06.00

procedure TSymbol.Draw(PInfo: PPaint; Clipping: Boolean);
begin
  try
//      if (SymPtr = nil) or (SymPtr^.Index < mi_Group) or (SymPtr^.GetObjType <> ot_Group) then begin
    SetSymPtr(PInfo^.Symbols);
    if SymPtr <> nil then
    begin
      SetState(sf_SymName, False);
    end;
//      end;
  except
{
         SetSymPtr(PInfo^.Symbols);
         if SymPtr <> nil then begin
            SetState(sf_SymName,False);
         end;
}
  end;

  if SymPtr <> nil then
  begin
    PInfo^.SetScaling(CalculateScale(PInfo), CalculateAngle, Position);
    if SizeType = stScaleIndependend then
    begin
      CalculateClipRect(PInfo);
      PProj(PInfo.AProj)^.UpdateCliprect(PSymbol(@Self));
    end;
  end;
  PSymbols(PInfo^.Symbols)^.DrawSymbol(PInfo, SymPtr);

   {

   if LogFile.Logging then begin

      LogFile.WriteLog('--- AM_Sym ---');

      LogFile.WriteLog('Index = ' + IntToStr(Index));
      LogFile.WriteLog('Size = ' + FloatToStr(Size));
      LogFile.WriteLog('SizeType = ' + IntToStr(SizeType));
      LogFile.WriteLog('Angle = ' + FloatToStr(Angle));

      LogFile.WriteLog('--- AM_Group ---');

      LogFile.WriteLog('Name = ' + SymPtr^.GetName);
      LogFile.WriteLog('RefPoint (X|Y) = ' + IntToStr(SymPtr^.RefPoint.X) + '|' + IntToStr(SymPtr^.RefPoint.Y));
      LogFile.WriteLog('RefLineLength = ' + FloatToStr(SymPtr^.RefLineLength));
      LogFile.WriteLog('DefaultAngle = ' + FloatToStr(SymPtr^.DefaultAngle));
      LogFile.WriteLog('DefaultSize = ' + FloatToStr(SymPtr^.DefaultSize));
      LogFile.WriteLog('DefaultSizeType = ' + IntToStr(SymPtr^.DefaultSizeType));

      LogFile.WriteLog('--- AM_Paint ---');

      LogFile.WriteLog('Zoom = ' + FloatToStr(PInfo^.Zoom));
      LogFile.WriteLog('ProjectScale = ' + FloatToStr(PInfo^.ProjectScale));
      LogFile.WriteLog('UnitsToMM = ' + FloatToStr(PInfo^.UnitsToMM));

      LogFile.WriteLog('------------------------------');

   end;

   }

end;

procedure TSymbol.SetSymPtrByIndex(Symbols: Pointer; myIndex: integer);
var
  SymItem: PIndex;
  ObjIndex: LongInt;
begin
  SymItem := New(PIndex, Init(myIndex)); { Symbol mit richtigem Index erzeugen }
  if (PSymbols(Symbols)^.Data^.Search(SymItem, ObjIndex)) then { gefunden }
    SymPtr := PSGroup(PSymbols(Symbols)^.Data^.At(ObjIndex))
  else
    SymPtr := GetSymPtrExt(SymItem);
  Dispose(SymItem, Done);
end;

procedure TSymbol.SetSymPtr(Symbols: Pointer);
var
  SymItem: PIndex;
  ObjIndex: LongInt;
begin
  SymItem := New(PIndex, Init(SymIndex)); { Symbol mit richtigem Index erzeugen }
  if (PSymbols(Symbols)^.Data^.Search(SymItem, ObjIndex)) then { gefunden }
    SymPtr := PSGroup(PSymbols(Symbols)^.Data^.At(ObjIndex))
  else
    SymPtr := GetSymPtrExt(SymItem);
  Dispose(SymItem, Done);
end;

function TSymbol.GetSymPtrExt(AItem: PIndex): PSGroup;
var
  PInfo: PPaint;
begin
  Result := nil;
{$IFNDEF AXDLL} // <----------------- AXDLL
  PInfo := WinGisMainForm.ActualChild.Data^.PInfo;
  if PInfo <> nil then
  begin
/////
  end;
{$ENDIF} // <----------------- AXDLL
end;

function TSymbol.GetSymbol
  (
  PInfo: PPaint
  )
  : PSGroup;
begin
  if SymPtr = nil then
    SetSymPtr(PInfo^.Symbols); { versuche SymPtr zu setzen }
  GetSymbol := SymPtr;
end;

function TSymbol.GetObjType
  : Word;
begin
  GetObjType := ot_Symbol;
end;

function TSymbol.SelectByPoint
  (
  PInfo: PPaint;
  Point: TDPoint;
  const ObjType: TObjectTypes
  )
  : PIndex;
var
  MDist: LongInt;
  ExpRect: TDRect;
begin
  MDist := PInfo^.CalculateDraw(poMaxDist);
  ExpRect.Init;
  ExpRect := ClipRect;
  ExpRect.Grow(MDist);
  if (ot_Symbol in ObjType) and ExpRect.PointInside(Point) then
    SelectByPoint := @Self
  else
    SelectByPoint := nil;
end;

procedure TSymbol.GetBorder
  (
  PInfo: PPaint;
  APoly: PCPoly
  );
var
  DPoint: TDPoint;
begin
    // set symbol-pointer if not already done
  if SymPtr = nil then
  begin
    SetSymPtr(PInfo^.Symbols);
    if SymPtr <> nil then
      SetState(sf_SymName, FALSE);
  end;
  if SymPtr <> nil then
  begin
    DPoint.Init(0, 0);
    PInfo^.SetScaling(CalculateScale(PInfo), CalculateAngle, Position);
    if SymPtr <> nil then
      with SymPtr^.ClipRect do
      begin
        DPoint := A;
        PInfo^.ScaleToDraw(DPoint);
        APoly^.Data^.Insert(@DPoint);
        DPoint.Init(A.X, B.Y);
        PInfo^.ScaleToDraw(DPoint);
        APoly^.Data^.Insert(@DPoint);
        DPoint := B;
        PInfo^.ScaleToDraw(DPoint);
        APoly^.Data^.Insert(@DPoint);
        DPoint.Init(B.X, A.Y);
        PInfo^.ScaleToDraw(DPoint);
        APoly^.Data^.Insert(@DPoint);
        APoly^.CalculateClipRect;
        APoly^.ClosePoly;
      end;
  end;
end;

procedure TSymbol.MoveRel
  (
  XMove: LongInt;
  YMove: LongInt
  );
begin
  inherited MoveRel(XMove, YMove);
  Position.Move(XMove, YMove);
end;

function TSymbol.SearchForPoint
  (
  PInfo: PPaint;
  InCircle: Pointer;
  Exclude: LongInt;
  const ObjType: TObjectTypes;
  var Point: TDPoint;
  var SnObje: PIndex
  )
  : Boolean;
begin
  if (ot_Symbol in ObjType) and ClipRect.IsVisible(PEllipse(InCircle)^.ClipRect) then
  begin
    if PEllipse(InCircle)^.PointInside(Position) then
    begin
      Point := Position;
      SearchForPoint := TRUE;
    end
    else
      SearchForPoint := FALSE;
  end
  else
    SearchForPoint := FALSE;
end;

function TSymbol.SelectByCircle
  (
  PInfo: PPaint;
  Middle: TDPoint;
  Radius: LongInt;
  const ObjType: TObjectTypes;
  Inside: Boolean
  )
  : Boolean;
var
  DPoly: TCPoly;
begin
  if (ot_Symbol in ObjType) then
  begin
    DPoly.Init;
    GetBorder(PInfo, @DPoly);
    SelectByCircle := DPoly.SelectByCircle(PInfo, Middle, Radius, [ot_CPoly], Inside);
    DPoly.Done;
  end
  else
    SelectByCircle := FALSE
end;

function TSymbol.SelectByPoly
  (
  PInfo: PPaint;
  Poly: Pointer;
  const ObjType: TObjectTypes;
  Inside: Boolean
  )
  : Boolean;
var
  DPoly: TCPoly;
begin
  SelectByPoly := FALSE;
  if (ot_Symbol in ObjType) then
  begin
    DPoly.Init;
    GetBorder(PInfo, @DPoly);
    SelectByPoly := DPoly.SelectByPoly(PInfo, Poly, [ot_CPoly], Inside);
    DPoly.Done;
  end;
end;

function TSymbol.SelectByPolyLine
  (
  PInfo: PPaint;
  Poly: Pointer;
  const ObjType: TObjectTypes
  )
  : Boolean;
var
  DPoly: TCPoly;
begin
  SelectByPolyLine := FALSE;
  if (ot_Symbol in ObjType) then
  begin
    DPoly.Init;
    GetBorder(PInfo, @DPoly);
    SelectByPolyLine := DPoly.SelectByPolyLine(PInfo, Poly, [ot_CPoly]);
    DPoly.Done;
  end;
end;

function TSymbol.GivePosX: LongInt;
begin
  GivePosX := Position.X;
end;

function TSymbol.GivePosY: LongInt;
begin
  GivePosY := Position.Y;
end;

{******************************************************************************}
{ Function SymObjectToObject                                                   }
{ Konvertiert Symbolelemente in "normale" WinGIS-Editor Elemente               }
{ TSPoly -> TPoly, etc. und kopiert (!) auch den Objektstil                    }
{******************************************************************************}

function SymObjectToObject
  (
  AProj: Pointer;
  Item: PView
  ): PView;
var
  APoly: PPoly;
  AObject: PView;
  ACircle: PEllipse;
  APoint: PPixel;
  AText: PText;
  AImage: PImage;
begin
  SymObjectToObject := nil;
  APoly := nil;
  AObject := PView(Item);
  case AObject^.GetObjType of
    ot_Poly,
      ot_CPoly,
      ot_Spline:
      begin
        if AObject^.GetObjType = ot_Poly then
          APoly := New(PPoly, Init);
        if AObject^.GetObjType = ot_CPoly then
          APoly := New(PCPoly, Init);
        if AObject^.GetObjType = ot_Spline then
          APoly := New(PSpline, Init);
        if APoly <> nil then
        begin
          APoly^.Data := MakeObjectCopy(PCPoly(AObject)^.Data);
        end;
        APoly^.CalculateClipRect;
        SymObjectToObject := APoly;
      end;
    ot_Pixel:
      begin
        APoint := New(PPixel, Init(PPixel(AObject)^.Position));
        SymObjectToObject := APoint;
      end;
    ot_Circle:
      with PEllipse(AObject)^ do
      begin
        ACircle := New(PEllipse, Init(Position, PrimaryAxis, SecondaryAxis, Angle));
        SymObjectToObject := ACircle;
      end;
    ot_Arc:
      with PEllipseArc(AObject)^ do
      begin
        ACircle := New(PEllipseArc, Init(Position, PrimaryAxis, SecondaryAxis, Angle, BeginAngle, EndAngle));
        SymObjectToObject := ACircle;
      end;
    ot_Text:
      with PText(AObject)^ do
      begin
        AText := New(PText, Init(Font, Text^, 0));
        AText^.Pos := Pos;
        AText^.Width := Width;
        AText^.Angle := Angle;
        AText^.CalculateClipRect(PProj(AProj)^.PInfo);
        SymObjectToObject := AText;
      end;
    ot_Image:
      with PSImage(AObject)^ do
      begin
        AImage := New(PImage, Init(FileName^, '', ShowOpt, PProj(AProj)^.PInfo^.BitmapSettings));
{++ Ivanoff BUG#NEW10 BUILD#100}
{Setting parameters of transparency of an image included in a symbol for correctly work in symbol editor.}
        AImage.Transparency := PSImage(AObject).Transparency;
        AImage.TransparencyType := PSImage(AObject).TransparencyType;
{-- Ivanoff}
        AImage.Options := PSImage(AObject).Options;
        AImage^.ClipRect.InitByRect(ClipRect);
        AImage^.ScaledClipRect.InitByRect(ScaledClipRect);
        AImage^.LoadImage(PProj(AProj)^.PInfo, bm_Display);
        SymObjectToObject := AImage;
      end;
  end;
end;

{******************************************************************************}
{ Function ObjectToSymObject                                                Ray}
{ Konvertiert Projektelemente in Symbolelemente                                }
{ (e.g. PPoly -> PSPoly, etc.)                                                 }
{ Objekt Stil (color, linestyle) wird ebenfalls kopiert                        }
{ Falls Item kein Symbol�quivalent hat, wird nil zur�ckgegeben                 }
{******************************************************************************}

function ObjectToSymObject
  (
  AProj: Pointer;
  Item: PIndex
  ): PView;
var
  APoly: PPoly;
  ACircle: PEllipse;
  APoint: PPixel;
  AText: PText;
  AObject: PView;
  AImage: PSImage;
  TopLayerItem: PIndex;
begin
  APoly := nil;
  ObjectToSymObject := nil;
  AObject := PPRoj(AProj)^.Layers^.IndexObject(PProj(AProj)^.PInfo, Item);
  TopLayerItem := PProj(AProj)^.Layers^.Toplayer^.HasObject(Item);
                                    { get "complete" object }
  case AObject^.GetObjType of
    ot_Poly, { any poly, line, spline, area }
      ot_CPoly,
      ot_Spline:
      begin
        if AObject^.GetObjType = ot_Poly then
          APoly := New(PSPoly, Init);
        if AObject^.GetObjType = ot_CPoly then
          APoly := New(PSCPoly, Init);
        if AObject^.GetObjType = ot_Spline then
          APoly := New(PSSpline, Init);
        if APoly <> nil then
        begin
          APoly^.Data := MakeObjectCopy(PCPoly(AObject)^.Data);
        end;

        if AObject^.GetObjType = ot_CPoly then
        begin
          APoly^.State := AObject^.State;
          PCPoly(APoly)^.IslandInfo := PCPoly(AObject)^.IslandInfo;
          PCPoly(APoly)^.IslandCount := PCPoly(AObject)^.IslandCount;
        end;

        if TopLayerItem <> nil then
          TopLayerItem^.CopyObjectStyleTo(APoly); { 0896Ray}
        APoly^.CalculateClipRect;
        ObjectToSymObject := APoly;
      end;
    ot_Pixel:
      begin
        APoint := New(PSPixel, Init(PPixel(AObject)^.Position));
        if TopLayerItem <> nil then
          TopLayerItem^.CopyObjectStyleTo(APoint); { 0896Ray}
        ObjectToSymObject := APoint;
      end;
    ot_Circle:
      with PEllipse(AObject)^ do
      begin
        ACircle := New(PSEllipse, Init(Position, PrimaryAxis, SecondaryAxis, Angle));
        if TopLayerItem <> nil then
          TopLayerItem^.CopyObjectStyleTo(ACircle); { 0896Ray}
        ObjectToSymObject := ACircle;
      end;
    ot_Arc:
      with PEllipseArc(AObject)^ do
      begin
        ACircle := New(PSEllipseArc, Init(Position, PrimaryAxis, SecondaryAxis, Angle, BeginAngle, EndAngle));
        if TopLayerItem <> nil then
          TopLayerItem^.CopyObjectStyleTo(ACircle); { 0896Ray}
        ObjectToSymObject := ACircle;
      end;
    ot_Text:
      with PText(AObject)^ do
      begin
        AText := New(PSText, Init(Font, Text^, 0));
        AText^.Pos := Pos;
        AText^.Width := Width; { text need to be set up a littel bit }
        AText^.Angle := Angle;
        AText^.CalculateClipRect(PProj(AProj)^.PInfo);
        if TopLayerItem <> nil then
          TopLayerItem^.CopyObjectStyleTo(AText);
        ObjectToSymObject := AText;
      end;
    ot_Image:
      with PImage(AObject)^ do
      begin
        AImage := New(PSImage, Init(FileName^, ShowOpt, PProj(AProj)^.PInfo^.BitmapSettings, FALSE));
        if (AImage <> nil) then
        begin {nur wenn Kopie erfolgreich }
          AImage^.ScaledClipRect.InitByRect(ScaledClipRect);
          AImage^.ClipRect.InitByRect(Item^.ClipRect);
          AImage^.LoadImage(PProj(AProj)^.PInfo, bm_Display);
{++ Ivanoff BUG#NEW10 BUILD#100}
{Setting parameters of transparency of an image included in a symbol for correctly displaying in a project.}
          AImage.Transparency := PImage(AObject).Transparency;
          AImage.TransparencyType := PImage(AObject).TransparencyType;
          AImage.Options := PImage(AObject).Options;
{-- Ivanoff}
          if TopLayerItem <> nil then
            TopLayerItem^.CopyObjectStyleTo(AImage);
          ObjectToSymObject := AImage;
        end;
      end;
  end;
end;

{------------------------------------------------------------------------------}
{ Procedure ProcCreateSymbolFromSelection                                   Ray}
{ Creates a new symbol from current selection. The new symbol is scaled        }
{ accordingly, and moved relative to the reference point                       }
{------------------------------------------------------------------------------}

procedure ProcCreateSymbolFromSelection(PProject: Pointer);
var
  NewSymbol: PSGroup;
  XScale: Double;
  YScale: Double;
  Project: PProj;
  NewSymName: ansistring;

  procedure DoAll
      (
      Item: PIndex
      ); far;
  var
    SymObject: PView;
  begin
    SymObject := ObjectToSymObject(Project, Item);
    if (SymObject <> nil) then
      NewSymbol^.Data^.Insert(SymObject);
  end;
begin
  Project := PProj(PProject);
  if (Project^.Layers^.SelLayer^.Data^.GetCount < 1) then { keine Objekte f�r Symbol }
  begin
    MessageDialog(MlgStringList['SymbolMsg', 8], mtInformation, [mbOk], 0);
    Exit; { Procedure beenden ... }
  end;

      { neuen Symbolnamen erzeugen }
  if (Project^.ActSymLib = '') then
    NewSymName := PSymbols(Project^.PInfo^.Symbols)^.GetNewSymName(
      MlgStringList['SymbolMsg', 3], '') {Name f�r neues Symbol }
  else
    NewSymName := PSymbols(Project^.PInfo^.Symbols)^.GetNewSymName(
      MlgStringList['SymbolMsg', 3], Project^.ActSymLib);

  if (not InputQuery(MlgStringList['SymbolMsg', 3], MlgStringList['SymbolMsg', 18], NewSymName)) or (Trim(NewSymName) = '') then
  begin
    exit;
  end;

  if (Project^.ActSymLib = '') then
    NewSymName := PSymbols(Project^.PInfo^.Symbols)^.GetNewSymName(
      MlgStringList['SymbolMsg', 3], '') {Name f�r neues Symbol }
  else
    NewSymName := PSymbols(Project^.PInfo^.Symbols)^.GetNewSymName(
      MlgStringList['SymbolMsg', 3], Project^.ActSymLib);

  if (Project^.ActSymLib = '') then { Symbol im Projekt "ohne" Bibliothek erzeugen }
    NewSymbol := New(PSGroup, InitName(NewSymName, '')) { new empty smybol }
  else
    NewSymbol := New(PSGroup, InitName(NewSymName, Project^.ActSymLib));
            { neues leeres Symbol f�r Bibliothek aktuelle Bibliothek }
  Project^.Layers^.SelLayer^.Data^.ForEach(@DoAll); { fill symbol }
  if (Project^.Layers^.SelLayer^.Data^.GetCount <> NewSymbol^.Data^.Count) then
    MessageDialog(MlgStringList['SymbolMsg', 9], mtInformation, [mbOk], 0);

  NewSymbol^.CalculateClipRect;
  XScale := -1;
  YScale := -1; {XScale,YScale bleibt negativ, wenn ClipRect.XSize,YSize=0}
  if NewSymbol^.ClipRect.XSize <> 0 then
    XScale := (2 * sym_SYMBOLSIZE) / NewSymbol^.ClipRect.XSize; { scale symbol to }
  if NewSymbol^.ClipRect.YSize <> 0 then { +/- 1000m range }
    YScale := (2 * sym_SYMBOLSIZE) / NewSymbol^.ClipRect.YSize;
  if XScale < 0 then
    if YScale < 0 then
      XScale := 1
    else
      XScale := YScale
  else
    if (XScale > YScale) and (YScale > 0) then
      XScale := YScale;
  NewSymbol^.Scale(XScale, XScale);
  with NewSymbol^.ClipRect do
    NewSymbol^.MoveRel(-LimitToLong(XSize / 2) - A.X,
      -LimitToLong(YSize / 2) - A.Y); {1196Ray}
                       { move symbol so that reference point is at (0,0) }
  if (PSymbols(Project^.PInfo^.Symbols)^.InsertLibSymbol(Project^.PInfo, Project^.ActSymLib, NewSymbol, TRUE)) then
  begin
    Project^.SetModified;
    UserInterface.Update([uiSymbols]);
  end
  else
  begin { Fehler beim Einf�gen in Symbolbibliothek (!Name) }
    Dispose(NewSymbol, Done);
    MessageDialog(MlgStringList['SymbolMsg', 6], mtInformation, [mbOk], 0);
  end;
end; {2705F}

function TSymbol.SearchForNearestPoint
  (
  PInfo: PPaint;
  var ClickPos: TDPoint;
  InCircle: Pointer;
  Exclude: LongInt;
  const ObjType: TObjectTypes;
  var Point: TDPoint;
  var SnObje: PIndex
  )
  : Boolean;
begin
  Result := FALSE;
  if (ot_Symbol in Objtype)
    and ClipRect.IsVisible(PEllipse(InCircle)^.ClipRect)
    and PEllipse(InCircle)^.PointInside(Position)
    and (ClickPos.Dist(Point) > ClickPos.Dist(Position)) then
  begin
    Point := Position;
    Result := TRUE;
  end;
end;

function TSymbol.SelectBetweenPolys
  (
  PInfo: PPaint;
  Poly1: Pointer;
  Poly2: Pointer;
  const ObjType: TObjectTypes;
  Inside: Boolean
  )
  : Boolean;
var
  DPoly: TCPoly;
begin
  SelectBetweenPolys := FALSE;
  if (ot_Symbol in ObjType) then
  begin
    DPoly.Init;
    GetBorder(PInfo, @DPoly);
    SelectBetweenPolys := DPoly.SelectBetweenPolys(PInfo, Poly1, Poly2, [ot_CPoly], Inside);
    DPoly.Done;
  end;
end;

const
  RSymbol: TStreamRec = (
    ObjType: rn_Symbol;
    VmtLink: TypeOf(TSymbol);
    Load: @TSymbol.Load;
    Store: @TSymbol.Store);

begin
  RegisterType(RSymbol);
end.

