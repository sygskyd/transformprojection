unit ReportDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, {Graphics,} Controls, Forms, Dialogs,
  StdCtrls, WCtrls, ExtCtrls,
  MultiLng;

type
  TReportForm = class(TWForm)
    Bevel1: TBevel;
    ControlPanel: TPanel;
    MlgSection: TMlgSection;
    Button2: TButton;
    ReportMemo: TMemo;
    CheckBox1: TCheckBox;
    Button1: TButton;
    Button3: TButton;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormShow(Sender: TObject);
  private
  public
  end;

function ShowReport(Caption: AnsiString; Msg: AnsiString; ReportId: Integer = 0; Buttons: Integer = MB_OK): Integer;

implementation

type
  TReportButtons = (rbOk, rbCancel, rbAbort, rbRetry, rbIgnore, rbYes, rbNo, rbNone);

  TButtonSet = record
    Id: Short;
//    Num: Short;
    Order: array[0..2] of TReportButtons;
  end;
  PUserButton = ^TButtonSet;
  TTranslation = record
    i : TReportButtons;                         // Input Id
    o: Integer;                         // Output Id
  end;

var
  AButton           : TButton = nil;
  UserButtons       : set of TReportButtons;
  ASet              : TButtonSet;
  MemCases          : TList = nil;
  ButtonArray       : array[MB_OK..MB_RETRYCANCEL] of TButtonSet =
    (
    (Id: MB_OK; {Num: 1;} Order: (rbNone, rbOk, rbNone)),
    (Id: MB_OKCANCEL; {Num: 2;} Order: (rbNone, rbOk, rbCancel)),
    (Id: MB_ABORTRETRYIGNORE; {Num: 3;} Order: (rbAbort, rbRetry, rbIgnore)),
    (Id: MB_YESNOCANCEL; {Num: 3;} Order: (rbYes, rbNo, rbCancel)),
    (Id: MB_YESNO; {Num: 2;} Order: (rbYes, rbYes, rbNo)),
    (Id: MB_RETRYCANCEL; {Num: 2;} Order: (rbNone, rbRetry, rbCancel))
    );
  ResultTranslation : array[rbOk..rbNone] of TModalresult =
    (mrOk, mrCancel, mrAbort, mrRetry, mrIgnore, mrYes, mrNo, mrNone);

{$R *.DFM}

  //uses ...;

function ShowReport(Caption: AnsiString; Msg: AnsiString; ReportId: Integer = 0; Buttons: Integer = MB_OK): Integer;
var
  ButtonNum         : Integer;
  ButtonOrder       : array[0..2] of TReportButtons;
  PSet              : PUserButton;
  I, J              : Integer;
  ReportForm        : TReportForm;
begin
  Result := 0;
  if (Msg = EmptyStr) then
    Exit;
  J := Buttons and 7;                   // Reveal only button set
  ASet := ButtonArray[0];               // Default cas
  for I := Low(ButtonArray) to High(ButtonArray) do
    if ButtonArray[I].Id = j then
    begin
      ASet := ButtonArray[I];
      break;
    end;
  ReportForm := TReportForm.Create(nil);
  try
    for I := 0 to 2 do
    begin
      case I of
        0: AButton := ReportForm.Button1;
        1: AButton := ReportForm.Button2;
        2: AButton := ReportForm.Button3;
      else
        ;
      end;
      if ASet.Order[I] = rbNone then
        AButton.Visible := False
      else
      begin
        AButton.Caption := ReportForm.MlgSection[Integer(ASet.Order[I])];
        AButton.ModalResult := ResultTranslation[ASet.Order[I]];
      end;
    end;

    J := Buttons and MB_DEFBUTTON4;     // Reveal button order if exists
    case J of
      MB_DEFBUTTON1: AButton := ReportForm.Button1;
      MB_DEFBUTTON2: AButton := ReportForm.Button2;
      MB_DEFBUTTON3: AButton := ReportForm.Button3;
      //      MB_DEFBUTTON4: ;
    else
      AButton := nil;
    end;
    //    ReportForm.ActiveControl := AButton;
    ReportForm.CheckBox1.Visible := False;
    ReportForm.ReportMemo.Lines.Text := Msg;
    ReportForm.Caption := Caption;
    Result := ReportForm.ShowModal;
  finally
    ReportForm.Free;
  end;

end;

procedure TReportForm.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  //  CanClose := ???;
end;

procedure TReportForm.FormShow(Sender: TObject);
begin
  if Assigned(AButton) then
    if AButton.Visible then
      AButton.SetFocus;
end;

end.

