 unit DTI_LibMan;

interface

Uses Windows, Classes, Forms, ActiveX;

Const
     cs_MSACCESS_TypeSign = 'MS_ACCESS';

     cs_ParameterSeparator = '|'; // This sign cannot be used in filenames.
                                          // 1. It follows just after database type and before database file name.
                                          // 2. It follows just after database file name and before database table name.
                                          // For example: 'MSACCESS|c:\required database file name.mdb|required table name'
Type
    TWorkMode = (wmBDE_OR_Local, wmADO);

    TrecFieldDesc = Record
        sFieldName: AnsiString;
        iFieldType: Integer;
        End;

    TInitLibrary = Procedure; stdcall;
    TSynchronizeLibrary = Procedure(AnApplication: TApplication; pcLanguageFileExtension: PChar); stdcall; 
    TFinLibrary = Procedure; stdcall;

    TGetGeoText = Function(pcTableName: PChar; iItemID: Integer; pcGeoTextIDFieldName, pcGeoTextValueFieldName: PChar): PChar; stdcall;
    TSetGeoText = Procedure(pcTableName: PChar; iItemID: Integer; pcGeoTextIDFieldName, pcGeoTextValueFieldName, pcGeoTextValue: PChar); stdcall;
    TCreateTable = Function(pcTableName, pcGeoTextIDFieldName, pcGeoTextValueFieldName: PChar): Boolean; stdcall;
    TGetFieldDescs = Function(pcTableName: PChar): TList; stdcall;

    TADO_GetGeoText = Function(pcTableName: PChar; iItemID: Integer; pcGeoTextIDFieldName, pcGeoTextValueFieldName: PChar): PChar; stdcall;
    TADO_SetGeoText = Procedure(pcTableName: PChar; iItemID: Integer; pcGeoTextIDFieldName, pcGeoTextValueFieldName, pcGeoTextValue: PChar); stdcall;
    TADO_CreateTable = Function(pcTableName, pcGeoTextIDFieldName, pcGeoTextValueFieldName: PChar): Boolean; stdcall;
    TADO_GetFieldDescs = Function(pcTableName: PChar): TList; stdcall;
    TADO_ChooseTableName= Function(pcAlreadySelectedTableName: PChar): PChar; stdcall;
    TADO_TableExists = Function(pcTableName: PChar): Boolean; stdcall;

    TDTI = Class
       Private
          hDTILibHandle: THandle;

          InitLibraryProc: TInitLibrary;
          SynchronizeLibraryProc: TSynchronizeLibrary;
          FinLibraryProc: TFinLibrary;
          GetGeoTextFunc: TGetGeoText;
          SetGeoTextProc: TSetGeoText;
          CreateTableFunc: TCreateTable;
          GetFieldDescsFunc: TGetFieldDescs;

          ADO_GetGeoTextFunc: TADO_GetGeoText;
          ADO_SetGeoTextProc: TADO_SetGeoText;
          ADO_CreateTableFunc: TADO_CreateTable;
          ADO_GetFieldDescsFunc: TADO_GetFieldDescs;
          ADO_ChooseTableNameFunc: TADO_ChooseTableName;
          ADO_TableExistsFunc: TADO_TableExists;
       Public
          Constructor Create;
          Destructor Free;

          Function GetGeoText(sTableName: AnsiString; iItemID: Integer; sGeoTextIDFieldName, sGeoTextValueFieldName: AnsiString): AnsiString;
          Procedure SetGeoText(sTableName: AnsiString; iItemID: Integer; sGeoTextIDFieldName, sGeoTextValueFieldName, sGeoTextValue: AnsiString);
          Function CreateTable(sTableName, sGeoTextIDFieldName, sGeoTextValueFieldName: AnsiString): Boolean;
          Function GetFieldDescs(sTableName: AnsiString): TList;

          Function ADO_GetGeoText(sTableName: AnsiString; iItemID: Integer; sGeoTextIDFieldName, sGeoTextValueFieldName: AnsiString): AnsiString;
          Procedure ADO_SetGeoText(sTableName: AnsiString; iItemID: Integer; sGeoTextIDFieldName, sGeoTextValueFieldName, sGeoTextValue: AnsiString);
          Function ADO_CreateTable(sTableName, sGeoTextIDFieldName, sGeoTextValueFieldName: AnsiString): Boolean;
          Function ADO_GetFieldDescs(sTableName: AnsiString): TList;
          Function ADO_ChooseTableName(sAlreadySelectedTableName: AnsiString): AnsiString;
          Function ADO_TableExists(sTableName: AnsiString): Boolean; 
       End;

implementation

Uses SysUtils, AM_Main;

Constructor TDTI.Create;
Var
   sLibName: AnsiString;
Begin
     sLibName := ExtractFilePath(Application.ExeName) + 'DTI.DLL';
//     sLibName := 'D:\DTI_DLL\DTI.DLL';

     hDTILibHandle := LoadLibrary(PChar(sLibName));
     {$IFNDEF AXDLL}
     WingisMainForm.CheckLibStatus(hDTILibHandle,'DTI.DLL','Tooltips');
     {$ENDIF}
     If hDTILibHandle = 0 Then EXIT;
     // Detect all function in DTI dll.
     InitLibraryProc := GetProcAddress(hDTILibHandle, 'InitLibrary');
     If @InitLibraryProc <> NIL Then InitLibraryProc;
     SynchronizeLibraryProc := GetProcAddress(hDTILibHandle, 'SynchronizeLibrary');
     If @SynchronizeLibraryProc <> NIL Then SynchronizeLibraryProc(Application, '044');

     FinLibraryProc := GetProcAddress(hDTILibHandle, 'FinLibrary');

     GetGeoTextFunc := GetProcAddress(hDTILibHandle, 'GetGeoText');
     SetGeoTextProc := GetProcAddress(hDTILibHandle, 'SetGeoText');
     CreateTableFunc := GetProcAddress(hDTILibHandle, 'CreateTable');
     GetFieldDescsFunc := GetProcAddress(hDTILibHandle, 'GetFieldDescs');

     ADO_GetGeoTextFunc := GetProcAddress(hDTILibHandle, 'ADO_GetGeoText');
     ADO_SetGeoTextProc := GetProcAddress(hDTILibHandle, 'ADO_SetGeoText');
     ADO_CreateTableFunc := GetProcAddress(hDTILibHandle, 'ADO_CreateTable');
     ADO_GetFieldDescsFunc := GetProcAddress(hDTILibHandle, 'ADO_GetFieldDescs');
     ADO_ChooseTableNameFunc := GetProcAddress(hDTILibHandle, 'ADO_ChooseTableName');
     ADO_TableExistsFunc := GetProcAddress(hDTILibHandle, 'ADO_TableExists');
End;

Destructor TDTI.Free;
Begin
     If @FinLibraryProc <> NIL Then FinLibraryProc;
     If hDTILibHandle <> 0 Then FreeLibrary(hDTILibHandle);
End;

////////////////////////////////////////////////////////////////////////////////
Function TDTI.GetGeoText(sTableName: AnsiString; iItemID: Integer; sGeoTextIDFieldName, sGeoTextValueFieldName: AnsiString): AnsiString;
Begin
     If @GetGeoTextFunc <> NIL Then
        RESULT := GetGeoTextFunc(PChar(sTableName), iItemID, PChar(sGeoTextIDFieldName), PChar(sGeoTextValueFieldName))
     Else
        RESULT := '';
End;

Procedure TDTI.SetGeoText(sTableName: AnsiString; iItemID: Integer; sGeoTextIDFieldName, sGeoTextValueFieldName, sGeoTextValue: AnsiString);
Begin
     If @SetGeoTextProc <> NIL Then
        SetGeoTextProc(PChar(sTableName), iItemID, PChar(sGeoTextIDFieldName), PChar(sGeoTextValueFieldName), PChar(sGeoTextValue));
End;

Function TDTI.CreateTable(sTableName, sGeoTextIDFieldName, sGeoTextValueFieldName: AnsiString): Boolean;
Begin
     If @CreateTableFunc <> NIL Then
        RESULT := CreateTableFunc(PChar(sTableName), PChar(sGeoTextIDFieldName), PChar(sGeoTextValueFieldName))
     Else
        RESULT := FALSE;
End;

Function TDTI.GetFieldDescs(sTableName: AnsiString): TList;
Begin
     If @GetFieldDescsFunc <> NIL Then
        RESULT := GetFieldDescsFunc(PChar(sTableName))
     Else
        RESULT := TList.Create;
End;

////////////////////////////////////////////////////////////////////////////////
Function TDTI.ADO_GetGeoText(sTableName: AnsiString; iItemID: Integer; sGeoTextIDFieldName, sGeoTextValueFieldName: AnsiString): AnsiString;
Begin
   CoInitialize(0);
   try
     If @ADO_GetGeoTextFunc <> NIL Then begin
        RESULT := ADO_GetGeoTextFunc(PChar(sTableName), iItemID, PChar(sGeoTextIDFieldName), PChar(sGeoTextValueFieldName));
     end
     Else
        RESULT := '';
   finally
     CoUninitialize;
   end;
End;

Procedure TDTI.ADO_SetGeoText(sTableName: AnsiString; iItemID: Integer; sGeoTextIDFieldName, sGeoTextValueFieldName, sGeoTextValue: AnsiString);
Begin
     If @ADO_SetGeoTextProc <> NIL Then
        ADO_SetGeoTextProc(PChar(sTableName), iItemID, PChar(sGeoTextIDFieldName), PChar(sGeoTextValueFieldName), PChar(sGeoTextValue));
End;

Function TDTI.ADO_CreateTable(sTableName, sGeoTextIDFieldName, sGeoTextValueFieldName: AnsiString): Boolean;
Begin
     If @ADO_CreateTableFunc <> NIL Then
        RESULT := ADO_CreateTableFunc(PChar(sTableName), PChar(sGeoTextIDFieldName), PChar(sGeoTextValueFieldName))
     Else
        RESULT := FALSE;
End;

Function TDTI.ADO_GetFieldDescs(sTableName: AnsiString): TList;
Begin
     If @ADO_GetFieldDescsFunc <> NIL Then
        RESULT := ADO_GetFieldDescsFunc(PChar(sTableName))
     Else
        RESULT := TList.Create;
End;

{ This function returns not only selected table name but also selected fields for GeoText.
  These parameters are separated with the help of ']' sign.
  For example: 'Table 1]ProgisID]GeotextInformation' }
Function TDTI.ADO_ChooseTableName(sAlreadySelectedTableName: AnsiString): AnsiString;
Begin
     If @ADO_ChooseTableNameFunc <> NIL Then
        RESULT := ADO_ChooseTableNameFunc(PChar(sAlreadySelectedTableName))
     Else
        RESULT := '';
End;

Function TDTI.ADO_TableExists(sTableName: AnsiString): Boolean;
Begin
     If @ADO_TableExistsFunc <> NIL Then
        RESULT := ADO_TableExistsFunc(PChar(sTableName))
     Else
        RESULT := FALSE;
End;

end.


