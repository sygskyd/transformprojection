Unit AM_Offse;

Interface

Uses Messages,WinProcs,WinTypes,SysUtils,AM_Paint,AM_Poly,AM_Def,AM_CPoly,AM_Layer,
     ResDlg,Classes,Controls;

Const idDontRound       = 1210;
      idRound           = 1211;
      idSingle          = 1212;
      idStreight        = 1213;
      idStreightDistance= 1214;
      idRounded         = 1215;
      idDouble          = 1216;

      id_Info           =  102;

Type TOffsetType   = (otSingle,otStreight,otStreightDistance,otRounded,otDouble,otPoints);

     {***********************************************************************}
     { Record TOffsetData                                                    }
     {-----------------------------------------------------------------------}
     {***********************************************************************}
     POffsetData        = ^TOffsetData;
     TOffsetData        = Record
       RoundEdges       : Boolean;
       OffsetType       : TOffsetType;
     end;

     {***********************************************************************}
     { Object TNSOffsetDlg                                                   }
     {-----------------------------------------------------------------------}
     { Verwendung als Offsetdialog f�r Selektieren und Nachbarsuche          }
     {***********************************************************************}
     TNSOffsetDlg       = Class(TGrayDialog)
      Public
       Data             : POffsetData;
       SingleAllowed    : Boolean;
       Constructor Init(AParent:TComponent;AData:POffsetData;ASingle:Boolean);
       Function    CanClose:Boolean; override;
       Procedure   SetupWindow; override;
       Procedure   WMCommand(var Msg:TMessage); message wm_First+wm_Command;
     end;

     {***********************************************************************}
     { Object TOffsetDlg                                                     }
     {-----------------------------------------------------------------------}
     {***********************************************************************}
     POffsetDlg         = ^TOffsetDlg;
     TOffsetDlg         = Class(TGrayDialog)
      Public
       Data             : POffsetData;
       SingleAllowed    : Boolean;
       Constructor Init(AParent:TComponent;AData:POffsetData;ASingle:Boolean);
       Function    CanClose:Boolean; override;
       Procedure   SetupWindow; override;
     end;

Function DoCreateOffset(AProj:Pointer;APoly:PPoly;Distance:LongInt;
    var OffsetData:TOffsetData;var Offset1,Offset2:PPoly):Integer;

Procedure DoCreateOffsetAndSelect(AProj:Pointer;AOffset:LongInt);

Procedure DoCreateOffsetAndSelectOnLayer(AProj:Pointer;AOffset:LongInt;ALayer:PLayer;LastLayer:Boolean);

Procedure ProcCreateOffset(AProj:Pointer;Distance:LongInt);

Implementation

Uses AM_Proj,AM_Input,UserIntf,AM_Index,AM_Point, AM_ProjO, GrTools;

Function CCW
   (
   P0              : PDPoint;
   P1              : PDPoint;
   P2              : PDPoint
   )
   : Integer;
  var DX1          : Double;
      DY1          : Double;
      DX2          : Double;
      DY2          : Double;
  begin
    DX1:=P1^.X-P0^.X;
    DY1:=P1^.Y-P0^.Y;
    DX2:=P2^.X-P0^.X;
    DY2:=P2^.Y-P0^.Y;
    if DX1*DY2>DY1*DX2 then CCW:=1
    else if DX1*DY2<DY1*DX2 then CCW:=-1
    else begin
      if (DX1*DX2<0)
          or (DY1*DY2<0) then CCW:=-1
      else if (DX1*DX1+DY1*DY1)>=(DX2*DX2+DY2*DY2) then CCW:=0
      else CCW:=1;
    end;
  end;

Constructor TOffsetDlg.Init
   (
   AParent         : TComponent;
   AData           : POffsetData;
   ASingle         : Boolean
   );
  begin
    inherited Init(AParent,'OffsetDlg');
    Data:=AData;
    SingleAllowed:=ASingle;
    HelpContext:=2080;
  end;

Function TOffsetDlg.CanClose
   : Boolean;
  begin
    CanClose:=True;
    Data^.RoundEdges:=IsDlgButtonChecked(Handle,idRound)=1;
    if IsDlgButtonChecked(Handle,idSingle)=1 then Data^.OffsetType:=otSingle
    else if IsDlgButtonChecked(Handle,idStreight)=1 then Data^.OffsetType:=otStreight
    else if IsDlgButtonChecked(Handle,idStreightDistance)=1 then Data^.OffsetType:=otStreightDistance
    else if IsDlgButtonChecked(Handle,idRounded)=1 then Data^.OffsetType:=otRounded;
  end;

Procedure TOffsetDlg.SetupWindow;
  begin
    inherited SetupWindow;
    CheckRadioButton(Handle,idDontRound,idRound,idDontRound+Byte(Data^.RoundEdges));
    CheckRadioButton(Handle,idSingle,idRounded,idSingle+Byte(Data^.OffsetType));
    if not SingleAllowed then begin
      if Data^.OffsetType=otSingle then CheckRadioButton(Handle,idSingle,idRounded,idSingle+Byte(otStreight));
      EnableWindow(GetItemHandle(idSingle),FALSE);
    end;
  end;

{****************************************************************************}
{ Function CutLines                                                          }
{----------------------------------------------------------------------------}
{ Ermittelt die Schnittpunkte der Linien L1P1->L1P2 und L2P1->L2P2.          }
{ Algorithmus aus [2] Seite 131. Die Schnittpunkte werden in die             }
{ VertexList eingef�gt.                                                      }
{----------------------------------------------------------------------------}
{ L1P1             i = Anfangspunkt erste Linie                              }
{ L1P2             i = Endpunkt erste Linie                                  }
{ L2P1             i = Anfangspunkt zweite Linie                             }
{ L2P2             i = Endpunkt zweite Linie                                 }
{ Cut1             o = Zeiger auf 1. Schnittpunkt, ung�ltig wenn 0 Schnittp. }
{ Cut2             o = Zeiger auf 2. Schnittpunkt, ung�ltig wenn weniger     }
{                      als zwei Schnittpunkte                                }
{----------------------------------------------------------------------------}
{ Ergebnis: Anzahl der Schnittpunkte.                                        }
{****************************************************************************}
Function CutLines
   (
   L1P1         : PDPoint;
   L1P2         : PDPoint;
   L2P1         : PDPoint;
   L2P2         : PDPoint;
   Cut1         : PDPoint;
   Cut2         : PDPoint;
   var CutOnLines : Boolean
   )
   : Integer;
  Const Tolerance    = 1E-8;             { Toleranz f�r Real-Vergleiche        }
  var D            : Double;
      DX1          : Double;
      DY1          : Double;
      DX2          : Double;
      DY2          : Double;
      t0           : Double;
      u0           : Double;
      tc           : Double;
      td           : Double;
  begin
    CutLines:=0;
    CutOnLines:=FALSE;
    DX1:=L1P2^.X-L1P1^.X;
    DX2:=L2P2^.X-L2P1^.X;
    DY1:=L1P2^.Y-L1P1^.Y;
    DY2:=L2P2^.Y-L2P1^.Y;
    D:=DX1*DY2-DY1*DX2;
    if Abs(D)>=Tolerance then begin
      t0:=((L2P1^.X-L1P1^.X)*DY2-(L2P1^.Y-L1P1^.Y)*DX2)/D;
      if Abs(DX2)>Abs(DY2) then u0:=((L1P1^.X-L2P1^.X)+DX1*t0)/DX2
      else u0:=((L1P1^.Y-L2P1^.Y)+DY1*t0)/DY2;
      Cut1^.X:=LimitToLong(L1P1^.X+DX1*t0);
      Cut1^.Y:=LimitToLong(L1P1^.Y+DY1*t0);
      CutLines:=1;
      CutOnLines:=(t0>-Tolerance)
          and (t0<1.0+Tolerance)
          and (u0>-Tolerance)
          and (u0<1.0+Tolerance);
    end
    else if Abs(DX1*(L2P1^.Y-L1P1^.Y)-DY1*(L2P1^.X-L1P1^.X))<Tolerance then begin
      if Abs(DX1)>Abs(DY1) then begin
        tc:=(L2P1^.X-L1P1^.X)/DX1;
        td:=(L2P2^.X-L1P1^.X)/DX1;
      end
      else begin
        tc:=(L2P1^.Y-L1P1^.Y)/DY1;
        td:=(L2P2^.Y-L1P1^.Y)/DY1;
      end;
      if tc<0.0 then tc:=0.0
      else if tc>1.0 then tc:=1.0;
      if td<0.0 then td:=0.0
      else if td>1.0 then td:=1.0;
      Cut1^.X:=LimitToLong(L1P1^.X+DX1*tc);
      Cut1^.Y:=LimitToLong(L1P1^.Y+DY1*tc);
      if Abs(tc-td)<Tolerance then CutLines:=1
      else begin
        Cut2^.X:=LimitToLong(L1P1^.X+DX1*td);
        Cut2^.Y:=LimitToLong(L1P1^.Y+DY1*td);
        CutLines:=2;
      end;
      CutOnLines:=((tc>-Tolerance) and (td<1.0+Tolerance))
          or ((tc<1.0+Tolerance) and (td>-Tolerance));
    end;
  end;

Function DoCreateOffset
   (
   AProj            :Pointer;
   APoly            : PPoly;
   Distance         : LongInt;
   var OffsetData   : TOffsetData;
   var Offset1      : PPoly;
   var Offset2      : PPoly
   )
   : Integer;
  var CreatePoly    : PPoly;
      CreatePoly1   : PPoly;
      DoCircles     : Boolean;
      Cnt           : Integer;
      Cnt2          : Integer;
      Line1P1       : TDPoint;
      Line1P2       : TDPoint;
      Line2P1       : TDPoint;
      Line2P2       : TDPoint;
      Point         : TDPoint;
      Point2        : TDPoint;
      ClosedPoly    : Boolean;
   Procedure CreateCircle;
     var T0            : Real;
         T1            : Real;
         Cnt1          : Integer;
         IsCCW         : Integer;
     begin
       T0:=PDPoint(APoly^.Data^.At(Cnt))^.CalculateAngle(Line1P2);
       T1:=PDPoint(APoly^.Data^.At(Cnt))^.CalculateAngle(Line2P1);
       IsCCW:=CCW(@Line1P1,@Point,@Line2P2);
       if IsCCW=1 then begin
         if T1<T0 then T1:=T1+2*Pi-T0
         else T1:=T1-T0;
       end
       else begin                      
         if T1>T0 then T1:=T1-2*Pi-T0
         else T1:=T1-T0;
       end;
       Cnt1:=Round(Abs(T1/(10*Pi/180)));
       if (Cnt1<2)
           or (Cnt1>35) then CreatePoly^.InsertPoint(Point)
       else begin
         CreatePoly^.InsertPoint(Line1P2);
         T1:=T1/Cnt1;
         for Cnt1:=Cnt1 downto 2 do begin
           T0:=T0+T1;
           Point.X:=Trunc(Cos(T0)*Abs(Distance)+PDPoint(APoly^.Data^.At(Cnt))^.X);
           Point.Y:=Trunc(Sin(T0)*Abs(Distance)+PDPoint(APoly^.Data^.At(Cnt))^.Y);
           CreatePoly^.InsertPoint(Point);
         end;
         CreatePoly^.InsertPoint(Line2P1);
       end;
     end;
  Procedure DoOffset;
    var DoCut        : Boolean;
        CutCnt       : Integer;
    Function MovePoints
       (
       Cnt           : Integer;
       var P1        : TDPoint;
       var P2        : TDPoint
       )
       : Boolean;
      var Angle      : Real;
          XMove      : LongInt;
          YMove      : LongInt;
      begin
        P1:=PDPoint(APoly^.Data^.At(Cnt))^;
        P2:=PDPoint(APoly^.Data^.At(Cnt+1))^;
        if P1.IsDiff(P2) then begin
          Angle:=P1.CalculateAngle(P2)-Pi/2;
          XMove:=LimitToLong(Distance*Cos(Angle));
          YMove:=LimitToLong(Distance*Sin(Angle));
          P1.Move(XMove,YMove);
          P2.Move(XMove,YMove);
          Result:=TRUE;
        end
        else Result:=FALSE;
      end;
    begin
      Line1P1.Init(0,0);
      Line1P2.Init(0,0);
      Line2P1.Init(0,0);
      Line2P2.Init(0,0);
      Point.Init(0,0);
      Point2.Init(0,0);
      Cnt:=0;
      while (Cnt<APoly^.Data^.Count-1)
          and not MovePoints(Cnt,Line1P1,Line1P2) do Inc(Cnt);
      Inc(Cnt);
      Line2P2:=Line1P2;
      while (Cnt<APoly^.Data^.Count-1)
          and not MovePoints(Cnt,Line2P1,Line2P2) do Inc(Cnt);
      if (APoly^.GetObjType<>ot_CPoly) and not ClosedPoly  then CreatePoly^.InsertPoint(Line1P1);
      while Cnt<=APoly^.Data^.Count-2 do begin
        if MovePoints(Cnt,Line2P1,Line2P2) then begin

          CutCnt:=CutLines(@Line1P1,@Line1P2,@Line2P1,@Line2P2,@Point,@Point2,DoCut);

          if CutCnt=0 then CreateCircle
          else if (DoCut) or (not DoCircles) then begin
               CreatePoly^.InsertPoint(Point);
          end
          else CreateCircle;

          Line1P1:=Line2P1;
          Line1P2:=Line2P2;
        end;
        Inc(Cnt);
      end;
      if (APoly^.GetObjType=ot_CPoly) or ClosedPoly then begin
        MovePoints(0,Line2P1,Line2P2);

        CutCnt:=CutLines(@Line1P1,@Line1P2,@Line2P1,@Line2P2,@Point,@Point2,DoCut);

        if CutCnt=0 then CreateCircle
        else if (DoCut) or (not DoCircles) then begin
                CreatePoly^.InsertPoint(Point);
        end
        else CreateCircle;

        CreatePoly^.ClosePoly;
      end
      else CreatePoly^.InsertPoint(Line2P2);

      CreatePoly^.Recreate(PProj(AProj)^.PInfo);

      CreatePoly^.CalculateClipRect;
    end;
  Procedure MovePoints2
     (
     Cnt           : Integer;
     Cnt1          : Integer;
     Cnt2          : Integer
     );
    var Angle      : Real;
        XMove      : LongInt;
        YMove      : LongInt;
        P1         : PDPoint;
        P2         : PDPoint;
    begin
      P1:=PDPoint(CreatePoly^.Data^.At(Cnt));
      P2:=PDPoint(CreatePoly^.Data^.At(Cnt1));

      Angle:=P2^.CalculateAngle(P1^);

      XMove:=LimitToLong(Abs(Distance)*Cos(Angle));
      YMove:=LimitToLong(Abs(Distance)*Sin(Angle));

      P1^.Move(XMove,YMove);
      PDPoint(CreatePoly1^.Data^.At(Cnt2))^.Move(XMove,YMove);
    end;
  begin
    ClosedPoly:=FALSE;
    DoCreateOffset:=0;
    DoCircles:=OffsetData.RoundEdges;
    if APoly^.GetObjType=ot_CPoly then begin
      CreatePoly:=New(PCPoly,Init);
      DoOffset;
      Offset1:=CreatePoly;
      DoCreateOffset:=1;
      if (OffsetData.OffsetType<>otSingle) and (OffsetData.OffsetType <> otPoints) then begin
        {
        Distance:=-Distance;
        CreatePoly:=New(PCPoly,Init);
        DoOffset;
        Offset2:=CreatePoly;
        DoCreateOffset:=2;
        }
      end;
    end
    else begin
      if OffsetData.OffsetType = otDouble then begin
        ClosedPoly:=TRUE;
        CreatePoly:=New(PCPoly,Init);
        DoOffset;
        Offset1:=CreatePoly;
        DoCreateOffset:=1;
        {
        Distance:=-Distance;
        CreatePoly:=New(PCPoly,Init);
        DoOffset;
        Offset2:=CreatePoly;
        DoCreateOffset:=2;
        }
        ClosedPoly:=FALSE;
      end
      else if (OffsetData.OffsetType<>otSingle) and (OffsetData.OffsetType<>otPoints) then begin
        CreatePoly:=New(PCPoly,Init);
        DoOffset;
        CreatePoly1:=CreatePoly;
        CreatePoly:=New(PCPoly,Init);
        Distance:=-Distance;
        DoOffset;

        if OffsetData.OffsetType=otStreightDistance then begin
          MovePoints2(0,1,0);
          MovePoints2(CreatePoly^.Data^.Count-1,CreatePoly^.Data^.Count-2,CreatePoly1^.Data^.Count-1);
        end
        else if OffsetData.OffsetType=otRounded then begin
          Cnt:=APoly^.Data^.Count-1;
          Line1P1:=PDPoint(CreatePoly^.Data^.At(CreatePoly^.Data^.Count-2))^;
          Line1P2:=PDPoint(CreatePoly^.Data^.At(CreatePoly^.Data^.Count-1))^;
          Point:=Line1P2;
          Line2P1:=PDPoint(CreatePoly1^.Data^.At(CreatePoly1^.Data^.Count-1))^;
          Line2P2:=Line2P1;
          CreateCircle;
        end;
        for Cnt2:=CreatePoly^.Data^.Count-1 downto 0 do
            CreatePoly1^.InsertPoint(PDPoint(CreatePoly^.Data^.At(Cnt2))^);
        Dispose(CreatePoly,Done);
        CreatePoly:=CreatePoly1;
        if OffsetData.OffsetType=otRounded then begin
          Cnt:=0;
          Line1P1:=PDPoint(CreatePoly^.Data^.At(CreatePoly^.Data^.Count-2))^;
          Line1P2:=PDPoint(CreatePoly^.Data^.At(CreatePoly^.Data^.Count-1))^;
          Point:=Line1P2;
          Line2P1:=PDPoint(CreatePoly1^.Data^.At(0))^;
          Line2P2:=Line2P1;
          CreateCircle;
        end;
        CreatePoly^.ClosePoly;
        Offset1:=CreatePoly;
        DoCreateOffset:=1;
      end
      else begin
        CreatePoly:=New(PPoly,Init);
        DoOffset;
        Offset1:=CreatePoly;
        DoCreateOffset:=1;
      end;
    end;
  end;

Procedure DoCreateOffsetAndSelect
   (
   AProj           : Pointer;
   AOffset         : LongInt
   );
  var Poly1        : PPoly;
      Poly2        : PPoly;
      PolyCount    : Integer;
      PolySwap     : Pointer;
  begin
    {$IFNDEF WMLT}
    with PProj(AProj)^ do begin
      PolyCount:=DoCreateOffset(AProj,OffsetItem,AOffset,SelectOffsetData,Poly1,Poly2);
      BreakInput(TRUE,FALSE);
      NewPoly^.Data:=MakeObjectCopy(Poly1^.Data);
      NewPoly^.CalculateClipRect;
      NewPoly^.Draw(PInfo,TRUE);
      if PolyCount > 1 then begin
        NewPoly1:=New(PNewPoly,Init(InputColor,InputLine,FALSE));
        NewPoly1^.Data:=MakeObjectCopy(Poly2^.Data);
        NewPoly1^.CalculateClipRect;
        NewPoly1^.Draw(PInfo,TRUE);
        Dispose(Poly2,Done);
      end;
      Dispose(Poly1,Done);
      if PolyCount > 1 then begin
        if (NewPoly^.ClipRect.B.X-NewPoly^.ClipRect.A.X) < (NewPoly1^.ClipRect.B.X-NewPoly1^.ClipRect.A.X) then begin
          PolySwap:=NewPoly1;
          NewPoly1:=NewPoly;
          NewPoly:=PolySwap;
        end;
      end;
      if  PolyCount > 1 then SelectBetweenPolys(NewPoly,NewPoly1,SelectData.SelectTypes,SelectData.SelectInside)
      else SelectByPoly(NewPoly,SelectData.SelectTypes,SelectData.SelectInside);
      if PolyCount > 1 then begin
        NewPoly1^.EndIt(PInfo);
        Dispose(NewPoly1,Done);
      end;
      NewPoly^.EndIt(PInfo);
    end;
    {$ENDIF}
  end;

Procedure DoCreateOffsetAndSelectOnLayer
   (
   AProj           : Pointer;
   AOffset         : LongInt;
   ALayer          : PLayer;
   LastLayer       : Boolean
   );
  var Poly1        : PPoly;
      Poly2        : PPoly;
      PolyCount    : Integer;
      PolySwap     : Pointer;
  begin
    with PProj(AProj)^ do begin
      PolyCount:=DoCreateOffset(AProj,OffsetItem,AOffset,SelectOffsetData,Poly1,Poly2);
      {if LastLayer then BreakInput(TRUE,FALSE);}
      NewPoly^.Data:=MakeObjectCopy(Poly1^.Data);
      NewPoly^.CalculateClipRect;
      NewPoly^.Draw(PInfo,TRUE);
      if PolyCount > 1 then begin
        NewPoly1:=New(PNewPoly,Init(InputColor,InputLine,FALSE));
        NewPoly1^.Data:=MakeObjectCopy(Poly2^.Data);
        NewPoly1^.CalculateClipRect;
        NewPoly1^.Draw(PInfo,TRUE);
        Dispose(Poly2,Done);
      end;
      Dispose(Poly1,Done);
      if PolyCount > 1 then begin
        if (NewPoly^.ClipRect.B.X-NewPoly^.ClipRect.A.X) < (NewPoly1^.ClipRect.B.X-NewPoly1^.ClipRect.A.X) then begin
          PolySwap:=NewPoly1;
          NewPoly1:=NewPoly;
          NewPoly:=PolySwap;
        end;
      end;
      if  PolyCount > 1 then ALayer^.SelectBetweenPolys(PInfo,NewPoly,NewPoly1,SelectData.SelectTypes,SelectData.SelectInside)
      else ALayer^.SelectByPoly(PInfo,NewPoly,SelectData.SelectTypes,SelectData.SelectInside);
      if PolyCount > 1 then begin
        {$IFNDEF AXDLL} // <----------------- AXDLL
        if DBSelection^.Selecting then DBSelection^.SaveOffsetPoly(1);
        {$ENDIF} // <----------------- AXDLL
        NewPoly1^.EndIt(PInfo);
        Dispose(NewPoly1,Done);
      end;
      {$IFNDEF AXDLL} // <----------------- AXDLL
      if DBSelection^.Selecting then DBSelection^.SaveOffsetPoly(0);
      {$ENDIF} // <----------------- AXDLL
      NewPoly^.EndIt(PInfo);
      {if LastLayer then BreakInput(TRUE,FALSE);}
    end;
  end;

Procedure ProcCreateOffset
   (
   AProj           : Pointer;
   Distance        : LongInt
   );
  var Poly1        : PPoly;
      Poly2        : PPoly;
      Polys        : Integer;
      i            : Integer;
      k            : Integer;
      ObjType      : Integer;
      APixel       : PPixel;
      APoint       : TDPoint;
  begin
    with PProj(AProj)^ do begin
      SetCursor(crHourGlass);
      PProj(AProj)^.SetStatusText(GetLangText(4008),TRUE);
      try
         for i:=0 to Layers^.SelLayer^.Data^.GetCount - 1 do begin
             ObjType:=Layers^.IndexObject(PInfo,Layers^.SelLayer^.Data^.At(i)).GetObjType;
             if (ObjType = ot_Poly) or (ObjType = ot_CPoly) then begin
                OffsetItem:=PPoly(Layers^.IndexObject(PInfo,Layers^.SelLayer^.Data^.At(i)));
                if OffsetItem <> nil then begin
                   Polys:=DoCreateOffset(AProj,OffsetItem,Distance,OffsetData,Poly1,Poly2);
                   if Polys>0 then begin
                      if OffsetData.OffsetType <> otPoints then begin
                         if InsertObject(Poly1,FALSE) then begin
                                   Poly1^.CalculateClipRect;
                                   {$IFNDEF WMLT}
                                   {$IFNDEF AXDLL} // <----------------- AXDLL
                                   ProcDBSendObject(PProj(AProj),Poly1,'EIN');
                                   {$ENDIF} // <----------------- AXDLL
                                   {$ENDIF}
                         end;
                      end
                      else begin
                         try
                            for k:=0 to Poly1^.Data^.Count-1 do begin
                                APoint.Init(PDPoint(Poly1^.Data^.At(k))^.X,PDPoint(Poly1^.Data^.At(k))^.Y);
                                APixel:=New(PPixel,Init(APoint));
                                if InsertObject(APixel,False) then begin
                                   {$IFNDEF WMLT}
                                   {$IFNDEF AXDLL} // <----------------- AXDLL
                                   ProcDBSendObject(PProj(AProj),APixel,'EIN');
                                   {$ENDIF} // <----------------- AXDLL
                                   {$ENDIF}
                                end;
                            end;
                         finally
                            Dispose(Poly1,Done);
                         end;
                      end;
                   end;
                   if (Polys>1) and (OffsetData.OffsetType = otPoints) then begin
                      Dispose(Poly2,Done);
                   end;
                   if (Polys>1) and not (OffsetData.OffsetType = otPoints) then begin
                      if InsertObject(Poly2,FALSE) then begin
                         Poly2.CalculateClipRect;
                      end;
                   end;
                end;
             end;
         end;
      finally
         ClearStatusText;
         DeselectAll(False);
         SetCursor(crDefault);
         SetModified;
      end;
    end;
  end;

Constructor TNSOffsetDlg.Init                                                                                   
   (
   AParent         : TComponent;
   AData           : POffsetData;
   ASingle         : Boolean
   );
  begin
    inherited Init(AParent,'OffsetDlgNS');
    Data:=AData;
    SingleAllowed:=ASingle;
  end;

Function TNSOffsetDlg.CanClose
   : Boolean;
  begin
    CanClose:=True;
    Data^.RoundEdges:=IsDlgButtonChecked(Handle,idRound)=1;
    if IsDlgButtonChecked(Handle,idSingle)=1 then Data^.OffsetType:=otSingle
    else if IsDlgButtonChecked(Handle,idStreight)=1 then Data^.OffsetType:=otStreight
    else if IsDlgButtonChecked(Handle,idStreightDistance)=1 then Data^.OffsetType:=otStreightDistance
    else if IsDlgButtonChecked(Handle,idRounded)=1 then Data^.OffsetType:=otRounded
    else if IsDlgButtonChecked(Handle,idDouble)=1 then Data^.OffsetType:=otDouble;
  end;

Procedure TNSOffsetDlg.SetupWindow;
  var Tmp    : Array[0..255] of Char;
  begin
    inherited SetupWindow;
    CheckRadioButton(Handle,idDontRound,idRound,idDontRound+Byte(Data^.RoundEdges));
    CheckRadioButton(Handle,idSingle,idDouble,idSingle+Byte(Data^.OffsetType));
    if Byte(Data^.OffsetType)+idSingle = idSingle then begin
       StrPCopy(Tmp,GetLangText(11055));
       SetDlgItemText(Handle,id_Info,Tmp);
    end
    else if Byte(Data^.OffsetType)+idSingle = idDouble then begin
      StrPCopy(Tmp,GetLangText(11056));
      SetDlgItemText(Handle,id_Info,Tmp);
    end
    else begin
      StrPCopy(Tmp,GetLangText(11057));
      SetDlgItemText(Handle,id_Info,Tmp);
    end;
    if not SingleAllowed then begin
      if Data^.OffsetType=otSingle then CheckRadioButton(Handle,idSingle,idRounded,idSingle+Byte(otStreight));
      EnableWindow(GetItemHandle(idSingle),FALSE);
    end;
  end;

Procedure TNSOffsetDlg.WMCommand
   (
   var Msg   : TMessage
   );
   var Tmp    : Array[0..255] of Char;
  begin
    case Msg.wParam of
      idSingle           : begin
                             StrPCopy(Tmp,GetLangText(11055));
                             SetDlgItemText(Handle,id_Info,Tmp);
                           end;
      idStreight,
      idStreightDistance,
      idRounded          : begin
                             StrPCopy(Tmp,GetLangText(11057));
                             SetDlgItemText(Handle,id_Info,Tmp);
                           end;
      idDouble           : begin
                             StrPCopy(Tmp,GetLangText(11056));
                             SetDlgItemText(Handle,id_Info,Tmp);
                           end;
      else inherited ;
    end;
  end;

end.
