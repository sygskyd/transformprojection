Unit EllipseEdit;

Interface

Uses EditHndl;

Type TEllipseEditor = Class(TEditHandler)
      Private
       FEllipseData     : TEllipseConstructData;
      Public
       Constructor Create(AParent:TComponent); override;
       Property    EllipseConstructData:TEllipseConstructData;
     end;

     TEllipseArcEditor = Class(TEditHandler)
      Private
      Public
     end;

Implementation

Constructor TEllipseEditor.Create(AParent:TComponent);
begin
  inherited Create(AParent);
  Options:=[ehoSize,ehoSizeMultiple,ehoSizeSymmetric,ehoRotate,ehoMoveBorder];
  Extend:=50;
end;

end.
 