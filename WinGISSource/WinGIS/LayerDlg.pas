{******************************************************************************+
  Unit LayerDlg
--------------------------------------------------------------------------------
  Autor: Martin Forst
--------------------------------------------------------------------------------
  Dialog zum Bearbeiten der Layerstruktur eines Projekts.
+******************************************************************************}
unit LayerDlg;

interface

uses WinTypes, WinProcs, Controls, ColorSelector, Classes, ExtCtrls, FillDlg, Forms, GenerDlg,
  Graphics, LineDlg, Messages, Objects, Palette, StdCtrls, StyleDef, InplEdit,
  Validate, Menus, UserIntf, WCtrls, AM_Proj, ComCtrls, Tabnotbk, TreeView, ListDlg,
  ProjStyle, MultiLng, PropCollect, ColumnList, Buttons, MultiDlg,
  TooltipDlg, LegendDlg, ActnList, ImgList;

const
  minDragMove = 2; { minimale Mausbewegung, um Drag zu starten  }

type
  TGroupEntry = class(TTreeEntry)
  private
    FLayerInfo: TLayerProperties;
    FIsGroup: Boolean;
  protected
    function GetCaption: string; override;
    procedure SetCaption(const ACaption: string); override;
  public
    constructor Create(AIsGroup: Boolean);
    destructor Destroy; override;
    property IsGroup: Boolean read FIsGroup;
    function EntryFromLayerNumber(ANumber: LongInt): TGroupEntry;
    property LayerInfo: TLayerProperties read FLayerInfo write FLayerInfo;
  end;

type
  TFieldIndex = (fiNone, fiCurrent, fiText, fiVisible, fiLocked, fiGeneralize,
    fiUseForSnap, fiPrintable, fiLineType, fiPattern, fiSymbolFill,
    fiHSDVisible);

  TLayerDialog = class(TMultiFormDialog)
    AddLayerMenu: TMenuItem;
    AddMenu: TMenuItem;
    CancelBtn: TButton;
    CreateGroupMenu: TMenuItem;
    DeleteLayerOrGroupMenu: TMenuItem;
    DeleteMenu: TMenuItem;
    GroupInplaceEditor: TInplaceEditor;
    GroupListbox: TTreeListView;
    GroupPopupMenu: TPopupMenu;
    GroupSymbols: TImage;
    LayerInplaceEditor: TInplaceEditor;
    Label2: TLabel;
    LayerListbox: TWListbox;
    LayerSymbols: TImage;
    MlgSection: TMlgSection;
    MoveUpMenu: TMenuItem;
    MoveDownMenu: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    Notebook: TWTabbedNoteBook;
    NewGroupMenu: TMenuItem;
    OkBtn: TButton;
    PopupMenu: TPopupMenu;
    PropertiesMenu: TMenuItem;
    SelectAllMenu: TMenuItem;
    SelectionDialog: TSelectionDialog;
    SearchEdit: TEdit;
    SearchBtn: TSpeedBtn;
    ToBottomMenu: TMenuItem;
    ToTopMenu: TMenuItem;
    ActionList: TActionList;
    ImageList: TImageList;
    MoveUpAction: TAction;
    MoveTopAction: TAction;
    MoveDownAction: TAction;
    MoveBottomAction: TAction;
    MoveToTopBtn: TSpeedBtn;
    MoveUpBtn: TSpeedBtn;
    MoveDownBtn: TSpeedBtn;
    MoveToBottomBtn: TSpeedBtn;
    SortHierarchicAction: TAction;
    SortAlphabeticAction: TAction;
    HierarchicBtn: TSpeedBtn;
    AlphabeticBtn: TSpeedBtn;
    PropertiesAction: TAction;
    DeleteAction: TAction;
    AddLayerAction: TAction;
    AddLayerBtn: TSpeedBtn;
    DeleteBtn: TSpeedBtn;
    Timer: TTimer;
    GeneraliseListbox: TColumnListbox;
    SortHierarchicBtn1: TSpeedBtn;
    SortAlphabeticBtn1: TSpeedBtn;
    Label1: TLabel;
    SearchEdit1: TEdit;
    SearchBtn1: TSpeedBtn;
    FixLayerButton: TSpeedButton;
    N7: TMenuItem;
    N671: TMenuItem;
    ApplyBtn: TButton;
    Panel1: TPanel;
    Image1: TImage;
    Panel2: TPanel;
    Image2: TImage;
    procedure AddBtnClick(Sender: TObject);
    procedure AddLayerMenuClick(Sender: TObject);
    procedure AlphabeticBtnClick(Sender: TObject);
    procedure CreateGroupMenuClick(Sender: TObject);
    procedure DeleteBtnClick(Sender: TObject);
    procedure DeleteLayerOrGroupMenuClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure GroupListboxClick(Sender: TObject);
    procedure GroupListboxDblClick(Sender: TObject);
    procedure GroupListboxDrawCaption(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure GroupInplaceEditorEdited(Sender: TObject; Index: Integer;
      const Text: string);
    procedure GroupInplaceEditorEditing(Sender: TObject; Index: Integer;
      var AllowEdit: Boolean; var Text: string);
    procedure GroupListboxMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure HirarchicBtnClick(Sender: TObject);
    procedure LayerInplaceEditorEdited(Sender: TObject; Index: Integer;
      const Text: string);
    procedure LayerInplaceEditorEditing(Sender: TObject; Index: Integer;
      var AllowEdit: Boolean; var Text: string);
    procedure LayerListboxClick(Sender: TObject);
    procedure LayerListboxDblClick(Sender: TObject);
    procedure LayerListboxDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure LayerListboxDrawItem(Control: TWinControl; AIndex: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure LayerListboxDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure LayerListboxMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure LayerListboxMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure LayerListboxMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure MoveUpMenuClick(Sender: TObject);
    procedure MoveDownMenuClick(Sender: TObject);
    procedure NewGroupMenuClick(Sender: TObject);
    procedure NotebookChange(Sender: TObject; NewTab: Integer;
      var AllowChange: Boolean);
    procedure PropertiesMenuClick(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure SearchEditChange(Sender: TObject);
    procedure SelectAllMenuClick(Sender: TObject);
    procedure ToTopMenuClick(Sender: TObject);
    procedure ToBottomMenuClick(Sender: TObject);
    procedure TimerTimer(Sender: TObject);
    procedure MoveBtnDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure MoveBtnUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure GeneraliseListboxColumns0Paint(Control: TWinControl;
      Index: Integer; Rect: TRect; State: TOwnerDrawState);
    procedure GeneraliseListboxColumns1Paint(Control: TWinControl;
      Index: Integer; Rect: TRect; State: TOwnerDrawState);
    procedure SearchBtn1Click(Sender: TObject);
    procedure SearchEdit1Change(Sender: TObject);
    procedure GeneraliseListboxColumns2Paint(Control: TWinControl;
      Index: Integer; Rect: TRect; State: TOwnerDrawState);
    procedure GeneraliseListboxColumns3Paint(Control: TWinControl;
      Index: Integer; Rect: TRect; State: TOwnerDrawState);
    procedure GeneraliseListboxDblClick(Sender: TObject);
    procedure GeneraliseListboxMouseDown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure GroupListboxMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FixLayerButtonClick(Sender: TObject);
  private
    FAttribFixed: Boolean;
    FClickField: TFieldIndex;
    FCurSorting: Boolean;
    FHSDLocked: Boolean;
    FHSDProject: Boolean;
    FMouseDownPos: TPoint;
    FFieldRects: array[TFieldIndex] of TPoint;
    FImages: TWImageList;
    FLastIndex: TFieldIndex;
    FLastDrag: Integer;
    FLastSelected: Integer;
    FLayerList: TList;
    FModified: Boolean;
    FProjectStyles: TProjectStyles;
    FPendingUps: Integer;
    FTopLayerIndex: Integer;
    FProject: PProj;
    FTooltipDialog: TTooltipDialog;
    FLegendDialog: TLegendDialog;
    function CheckEditLayersForHSD(Layers: TList; var ReadOnly: Boolean): Boolean;
    procedure CreateLayerGroupTree;
    function CreateNewGroup: TGroupEntry;
    procedure DoSearchBtnClick(SearchEdit: TEdit; Listbox: TWCustomListbox);
    procedure DoSearchEditChange(SearchEdit: TEdit; Listbox: TWCustomListbox;
      SearchBtn: TSpeedBtn);
    procedure DrawLayerInfo(Listbox: TWCustomListbox; AIndex: Integer; LayerInfo: TLayerProperties;
      Rect: TRect; State: TOwnerDrawState; DrawAction: TOwnerDrawAction);
    function EditGeneralize(ToEdit: TList): Boolean;
    function EditLineType(ToEdit: TList): Boolean;
    function EditPattern(ToEdit: TList): Boolean;
    function EditSymbolFill(ToEdit: TList): Boolean;
    function FieldAtPos(Listbox: TWCustomListbox; Pos: TPoint): TFieldIndex;
    function FieldRect(Listbox: TWCustomListbox; AIndex: TFieldIndex; ATop: Integer): TRect;
    function FieldHint(Field: TFieldIndex): string;
    function GeneralizeText(AValue: Double): string;
    procedure InvalidateLayer(LayerInfo: TLayerProperties; Field: TFieldIndex);
    function LayerInfoFromIndex(AIndex: LongInt): TLayerProperties;
    function LayerInfoFromName(const AName: string): TLayerProperties;
    procedure MoveSelection(Index: Integer);
    function GetTopLayer: string;
    procedure SetLayerList(AList: TList);
    procedure SetTopLayer(const ALayer: string);
    procedure SetLayerSorting(Alphabetic: Boolean);
    procedure SortLayers;
    procedure UpdateEnabled;
    procedure UpdateLayerGroups;
    procedure WriteLayerGroupsToRegistry;

    procedure UpDateCheckedItems(flag: Boolean); //Freddy demand N1  by Brovak

  protected
    procedure ChangeScale(M, D: Integer); override;
  public
    DDEString: AnsiString;
    property LayerList: TList write SetLayerList;
    property Project: PProj read FProject write FProject;
    property TopLayer: string read GetTopLayer write SetTopLayer;
    property HSDAttributesLocked: Boolean read FHSDLocked write FHSDLocked;
  end;

var
  LayerDialog: TLayerDialog;

implementation

{$R *.DFM}

uses AM_Def, AM_Pass, Dialogs, LayerPropertyDlg, Measures, NumTools, RegDB, StrTools,
{$IFNDEF WMLT}
  SymbolFillDlg,
{$ENDIF}
  SysUtils, XFills, XLines, XStyles, CommonResources, AM_DDE, LicenseHndl;

const
  indVisible = 0;
  indLocked = 1;
  indGeneralize = 2;
  indUseForSnap = 3;
  indPrintable = 4;

var
  LastTop: Integer = 0;
  LastLeft: Integer = 0;

{===============================================================================
| TGroupEntry
+==============================================================================}

constructor TGroupEntry.Create
  (
  AIsGroup: Boolean
  );
begin
  inherited Create;
  FIsGroup := AIsGroup;
  if not FIsGroup then
    Bitmap := 3
  else
    LayerInfo := TLayerProperties.Create;
  Expanded := FALSE;
end;

destructor TGroupEntry.Destroy;
begin
  if FIsGroup then
    LayerInfo.Free;
  inherited Destroy;
end;

function TGroupEntry.GetCaption
  : string;
begin
  Result := FLayerInfo.Name;
end;

procedure TGroupEntry.SetCaption
  (
  const ACaption: string
  );
begin
  FLayerInfo.Name := ACaption;
end;

function TGroupEntry.EntryFromLayerNumber
  (
  ANumber: LongInt
  )
  : TGroupEntry;
var
  Cnt: Integer;
begin
  for Cnt := 0 to Count - 1 do
    if TGroupEntry(Items[Cnt]).LayerInfo.Number = ANumber then
    begin
      Result := TGroupEntry(Items[Cnt]);
      Exit;
    end;
  Result := nil;
end;

{===============================================================================
| TLayerDialog
+==============================================================================}

procedure TLayerDialog.DrawLayerInfo
  (
  Listbox: TWCustomListbox;
  AIndex: Integer;
  LayerInfo: TLayerProperties;
  Rect: TRect;
  State: TOwnerDrawState;
  DrawAction: TOwnerDrawAction
  );
var
  ARect: TRect;
  ClipBox: TRect;
  Region: THandle;
  XLineStyle: TXLineStyle;
  XFillStyle: TCustomXFill;
  Points: TRectCorners;
  AZoom: Double;
  {*****************************************************************************
  | Procedure DrawSymbol
  |-----------------------------------------------------------------------------
  | Zeichnet das einem Feld zugeordnete Symbol in das angegebene Rechteck.
  |-----------------------------------------------------------------------------
  | in AIndex = Nummer des Feldes (fiXXXX)
  | in ARect = Rechteck in das das Symbol gezeichnet werden soll
  +****************************************************************************}

  procedure DrawSymbol
      (
      ARect: TRect;
      Index: Integer
      );
  var
    X: Integer;
    Y: Integer;
  begin
    X := ARect.Left + (ARect.Right - ARect.Left - 16) div 2;
    Y := ARect.Top + (ARect.Bottom - ARect.Top - 16) div 2;
    FImages.Draw(Listbox.Canvas, X, Y, Index);
  end;

  procedure DrawSeparator
      (
      Index: TFieldIndex
      );
  begin
    ARect := FieldRect(Listbox, Index, AIndex);
    with Listbox.Canvas do
    begin
      MoveTo(ARect.Left - 1, ARect.Top);
      LineTo(ARect.Left - 1, ARect.Bottom);
    end;
  end;
begin
  with ListBox, Canvas, LayerInfo do
  begin
    Brush.Color := clWindow;
    Font.Color := clWindowText;
    if AIndex > -1 then
    begin
      if odSelected in State then
      begin
        Brush.Color := clHighlight;
        Font.Color := clHighlightText
      end
      else
        if odGrayed in State then
        begin
          Brush.Color := clBtnFace;
          Font.Color := clBtnText
        end;
    end;
    ARect := FieldRect(Listbox, fiText, AIndex);
    InflateRect(ARect, -3, 0);
    TextRect(ARect, ARect.Left + 2, CenterHeightInRect(TextHeight('A'), ARect), Name);
    if odFocused in State then
      DrawFocusRect(ARect);
    if oaDrawEntire in DrawAction then
    begin
      SetBkMode(Handle, 1);
      ARect := FieldRect(Listbox, fiPattern, AIndex);
      InflateRect(ARect, -2, 0);
      with FillStyle do
        if ValidValues then
        begin

          if (Pattern >= pt_UserDefined) and (FProjectStyles.XFillStyles[Pattern] = nil) then
          begin
            Pattern := 0;
          end;

          if Pattern >= pt_UserDefined then
          begin
            CommonListBitmaps.Draw(Canvas, ARect.Left, CenterHeightInRect(14, ARect), clbUser);
            if HSDLayer and FHSDLocked then
              FImages.Draw(Listbox.Canvas, ARect.Left, ARect.Top, 9);
            InflateLeft(ARect, 16);
            { draw user-defined fill-style }
            XFillStyle := FProjectStyles.XFillStyles[Pattern];
            InflateRect(ARect, 0, -1);
            Dec(ARect.Right);
            Dec(ARect.Bottom);
            RectCorners(ARect, Points);
            if XFillStyle is TVectorXFill then
              with XFillStyle as TVectorXFill do
              begin
                XLFillPolygon(Handle, Points, 4, StyleDef^.XFillDefs, StyleDef^.Count,
                  0, 0, 5 / MinDistance, 0);
              end
            else
              if XFillStyle is TBitmapXFill then
                with XFillStyle as TBitmapXFill do
                  BMFillPolygon(Handle, Points, 4, StyleDef(Handle)^.BitmapHandle, srcCopy, 1);
          end
          else
            if Pattern <> 0 then
            begin
              CommonListBitmaps.Draw(Canvas, ARect.Left, CenterHeightInRect(14, ARect), clbSystem);
              if HSDLayer and FHSDLocked then
                FImages.Draw(Listbox.Canvas, ARect.Left, ARect.Top, 9);
              InflateLeft(ARect, 16);
            { draw windows-fill-type }
              Brush.Handle := GetBrush(ForeColor, Pattern, FProjectStyles.XFillStyles);
              if BackColor = clNone then
                SetBKMode(Handle, Transparent)
              else
                SetBKColor(Handle, BackColor);
              Pen.Style := psClear;
              with ARect do
                Rectangle(Left, Top + 1, Right, Bottom - 1);
              Brush.Handle := 0;
            end
            else
              if HSDLayer and FHSDLocked then
                FImages.Draw(Listbox.Canvas, ARect.Left, ARect.Top, 9);
        end
        else
          FImages.Draw(Listbox.Canvas, ARect.Right - 18, ARect.Top, 6);
      SetBKMode(Handle, Opaque);
        { Symbole f�r den Layer zeichnen }
      if (Listbox = LayerListbox) and (AIndex = FTopLayerIndex) then
        DrawSymbol(FieldRect(Listbox, fiCurrent, AIndex), 5);
      ARect := FieldRect(Listbox, fiVisible, AIndex);
      if Visible <> bsNo then
        DrawSymbol(ARect, 0);
      if Visible = bsNoChange then
        FImages.Draw(Listbox.Canvas, ARect.Right - 18, ARect.Top, 6);
      if FHSDProject then
      begin
        ARect := FieldRect(Listbox, fiHSDVisible, AIndex);
        if HSDVisible = bsYes then
          DrawSymbol(ARect, 8)
        else
          if HSDLayer then
            DrawSymbol(ARect, 9);
        if HSDVisible = bsNoChange then
          FImages.Draw(Listbox.Canvas, ARect.Right - 18, ARect.Top, 6);
      end;
      ARect := FieldRect(Listbox, fiLocked, AIndex);
      if Editable <> bsYes then
        DrawSymbol(ARect, 1);
      if Editable = bsNoChange then
        FImages.Draw(Listbox.Canvas, ARect.Right - 18, ARect.Top, 6);
      ARect := FieldRect(Listbox, fiGeneralize, AIndex);
      if Generalize <> gtShowAlways then
        DrawSymbol(ARect, 2);
      if Generalize = gtNoChange then
        FImages.Draw(Listbox.Canvas, ARect.Right - 18, ARect.Top, 6);
      ARect := FieldRect(Listbox, fiUseForSnap, AIndex);
      if UseForSnap <> bsNo then
        DrawSymbol(ARect, 4);
      if UseForSnap = bsNoChange then
        FImages.Draw(Listbox.Canvas, ARect.Right - 18, ARect.Top, 6);
      ARect := FieldRect(Listbox, fiPrintable, AIndex);
      if Printable <> bsNo then
        DrawSymbol(ARect, 3);
      if Printable = bsNoChange then
        FImages.Draw(Listbox.Canvas, ARect.Right - 18, ARect.Top, 6);
      ARect := FieldRect(Listbox, fiSymbolFill, AIndex);
      if SymbolFill.FillType <> symfNone then
        DrawSymbol(ARect, 7);
      if HSDLayer and FHSDLocked then
        FImages.Draw(Listbox.Canvas, ARect.Left, ARect.Top, 9);

      ARect := FieldRect(Listbox, fiLineType, AIndex);
      InflateRect(ARect, -2, 0);
      with LineStyle do
        if ValidValues then
        begin
          Brush.Color := clWhite;
          FillRect(ARECT);
          if (Style >= lt_UserDefined) and (FProjectStyles.XLineStyles[Style] = nil) then
          begin
            Style := 0;
          end;
          if Style >= lt_UserDefined then
          begin
            CommonListBitmaps.Draw(Canvas, ARect.Left, CenterHeightInRect(14, ARect), clbUser);
            if HSDLayer and FHSDLocked then
              FImages.Draw(Listbox.Canvas, ARect.Left, ARect.Top, 9);
            { draw user-defined type, set scale that line covers 4/5 of the item-height }

            InflateLeft(ARect, 16);

            GetClipBox(Handle, ClipBox);
            with ARect do
            begin
              IntersectClipRect(Handle, Left, Top, Right, Bottom);
            end;

            XLineStyle := FProjectStyles.XLineStyles[Style];
            with ARect do
            begin
              Points[0] := Point(Left, ARect.Top + ItemHeight div 2);
              Points[1] := Point(Right, ARect.Top + ItemHeight div 2);

              if RectHeight(XLineStyle.ClipRect) = 0 then
              begin
                AZoom := RectWidth(FieldRect(Listbox, fiText, AIndex)) / XLineStyle.Items[0].Cycle / 10
//                 AZoom:=(ListBox.ItemHeight-2)*5//AZoom := 1.7E10308
              end
              else
              begin
                AZoom := (ItemHeight - 2) / RectHeight(XLineStyle.ClipRect) / 5;
              end;

              XPolyline(Handle, Points, 2, XLineStyle.StyleDef^.XLineDefs,
                XLineStyle.StyleDef^.Count, LineStyle.Color,
                LineStyle.FillColor, True, AZoom);
            end;
          end
          else
          begin
            CommonListBitmaps.Draw(Canvas, ARect.Left, CenterHeightInRect(14, ARect), clbSystem);
            if HSDLayer and FHSDLocked then
              FImages.Draw(Listbox.Canvas, ARect.Left, ARect.Top, 9);
            { draw windows line-style }

            InflateLeft(ARect, 16);

            GetClipBox(Handle, ClipBox);
            with ARect do
            begin
              IntersectClipRect(Handle, Left, Top, Right, Bottom);
            end;
            Pen.Handle := GetPen(Color, Style, Round(Width));
            MoveTo(ARect.Left, ARect.Top + ItemHeight div 2);
            LineTo(ARect.Right, ARect.Top + ItemHeight div 2);
            MoveTo(ARect.Left, ARect.Top + ItemHeight div 2 + 1);
            LineTo(ARect.Right, ARect.Top + ItemHeight div 2 + 1);
          end;

          with ClipBox do
            Region := CreateRectRgn(Left, Top, Right, Bottom);
          SelectClipRgn(Handle, Region);
          DeleteObject(Region);
        end
        else
          FImages.Draw(Listbox.Canvas, ARect.Right - 18, ARect.Top, 6);

      { Trennlinien zwischen den Spalten zeichnen }
      Pen.Color := clBtnFace;
      Pen.Style := psSolid;
      if Listbox = LayerListbox then
        DrawSeparator(fiText);
      DrawSeparator(fiVisible);
      if FHSDProject then
        DrawSeparator(fiHSDVisible);
      DrawSeparator(fiLocked);
      DrawSeparator(fiGeneralize);
      DrawSeparator(fiUseForSnap);
      DrawSeparator(fiPrintable);
      DrawSeparator(fiLineType);
      DrawSeparator(fiPattern);
      DrawSeparator(fiSymbolFill);
    end;
  end;
end;

procedure TLayerDialog.LayerListBoxDrawItem(Control: TWinControl; AIndex: Integer;
  Rect: TRect; State: TOwnerDrawState);
var
  LayerInfo: TLayerProperties;
begin
  LayerInfo := FLayerList[AIndex];
  DrawLayerInfo(LayerListbox, AIndex, LayerInfo, Rect, State, LayerListbox.DrawAction);
end;

{*******************************************************************************
| Procedure TLayerDialog.SetLayerList
|-------------------------------------------------------------------------------
| Schreibprozedur f�r die Eigenschaft LayerList. F�llt die Layerlistbox mit den
| neuen Layernamen.
+******************************************************************************}

procedure TLayerDialog.SetLayerList(AList: TList);
begin
  FLayerList := AList;
  LayerListBox.Count := FLayerList.Count;
end;

{*******************************************************************************
| Procedure TLayerDialog
|-------------------------------------------------------------------------------
| Erzeugt die Bitmap-List, initialisiert die Spaltenpositionen der einzelnen
| Eintr�ge.
+******************************************************************************}

procedure TLayerDialog.FormCreate(Sender: TObject);

  procedure SetFieldRect(Field: TFieldIndex; Start, Stop: Integer);
  begin
    FFieldRects[Field].X := Trunc(Start * Screen.PixelsPerInch / 96);
    FFieldRects[Field].Y := Trunc(Stop * Screen.PixelsPerInch / 96);
  end;
begin
  //Scaleby(Trunc(11000000/(PixelsPerInch)),100000);
  DDEString := '';
  // setup multipages
  RemoveFirstPage := FALSE;
  DialogNotebook := NoteBook;
  // add additional dialogs
  FLegendDialog := TLegendDialog.Create(Self);
  AddDialog(FLegendDialog, FLegendDialog.Panel1, FLegendDialog.Caption);
  FTooltipDialog := TTooltipDialog.Create(Self);
  AddDialog(FTooltipDialog, FTooltipDialog, FTooltipDialog.Caption);
  // create temporary lists and other images
  FProjectStyles := TProjectStyles.Create;
  FImages := TWImageList.CreateSize(16, 16);
  FImages.AddBitmaps(LayerSymbols.Picture.Bitmap);
  // setup field-columns
  SetFieldRect(fiCurrent, 1, 20);
  SetFieldRect(fiText, 21, 191);
  SetFieldRect(fiVisible, 193, 212);
  SetFieldRect(fiHSDVisible, 213, 232);
  SetFieldRect(fiUseForSnap, 233, 252);
  SetFieldRect(fiPrintable, 253, 272);
  SetFieldRect(fiLocked, 273, 292);
  SetFieldRect(fiGeneralize, 293, 312);
  SetFieldRect(fiLineType, 313, 382);
  SetFieldRect(fiPattern, 383, 452);
  SetFieldRect(fiSymbolFill, 453, 472);
  GroupListbox.AddBitmaps(GroupSymbols.Picture.Bitmap, 3);
{$IFDEF WMLT}
  AddLayerBtn.Enabled := False;
  DeleteBtn.Enabled := False;
  PopUpmenu.AutoPopup := False;
  GroupPopUpmenu.AutoPopup := False;
  FixLayerButton.Visible := False;
{$ENDIF}
end;

procedure TLayerDialog.FormDestroy(Sender: TObject);
begin
  FImages.Free;
  FProjectStyles.Free;
end;

{******************************************************************************+
  Function TLayerDialog.FieldRect
--------------------------------------------------------------------------------
  Calculates the rectangle for the given field.
--------------------------------------------------------------------------------
  in AIndex = Nummer des Feldes (fiXXXX)
  in ATop = index of the entry then rectange is for
+******************************************************************************}

function TLayerDialog.FieldRect(Listbox: TWCustomListbox; AIndex: TFieldIndex;
  ATop: Integer): TRect;
var
  LayerEntry: TGroupEntry;
begin
  Result.Left := FFieldRects[AIndex].X;
  Result.Right := FFieldRects[AIndex].Y;
  Result.Top := (ATop - Listbox.TopIndex) * Listbox.ItemHeight;
  Result.Bottom := Result.Top + Listbox.ItemHeight;
  if (AIndex = fiText) and (Listbox = GroupListbox) then
  begin
    LayerEntry := TGroupEntry(GroupListbox.Tree.ExpandedItems[ATop]);
    if LayerEntry <> nil then
      Result.Left := (LayerEntry.Level + 1) * GroupListbox.ItemWidth;
  end
end;

procedure TLayerDialog.LayerListboxMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  Field: TFieldIndex;
  Index: Integer;
  LayerInfo: TLayerProperties;
  ARect: TRect;
begin
  if Button = mbLeft then
  begin
    Index := LayerListbox.ItemAtPos(Point(X, Y), TRUE);
    with LayerListbox do
      if FPendingUps > 0 then
        Dec(FPendingUps)
      else
        if Index >= 0 then
        begin
          Field := FieldAtPos(LayerListbox, Point(X, Y));
          if (FLastSelected = -1) and (Field in [fiCurrent, fiText]) then
            ItemIndex := Index;
          if (SelCount = 1)
            and (Field = FClickField)
            and (Field in [fiCurrent, fiVisible, fiHSDVisible, fiUseForSnap, fiPrintable])
            and Selected[Index] then
          begin
            LayerInfo := FLayerList[Index];
            case Field of
              fiCurrent:
                begin
                  ARect := FieldRect(LayerListbox, fiCurrent, FTopLayerIndex);
                  InvalidateRect(Handle, @ARect, TRUE);
                  FTopLayerIndex := Index;
                end;
              fiVisible:
                begin
                  LayerInfo.Visible := InvertBooleanStyle(LayerInfo.Visible, FALSE);
                  if LayerInfo.HSDLayer then
                  begin
                    LayerInfo.HSDVisible := LayerInfo.Visible;
                    ARect := FieldRect(LayerListbox, fiHSDVisible, ItemIndex);
                    InvalidateRect(Handle, @ARect, TRUE);
                  end;
                  {+++Brovak Bug 108 30.01.2001 If Layer Invisible then and not Print it}
                  LayerInfo.Printable := LayerInfo.Visible;
                  ARect := FieldRect(LayerListbox, FiPrintable, ItemIndex);
                  InvalidateRect(Handle, @ARect, TRUE);
                  {-- Brovak}
                end;
              fiHSDVisible:
                if not LayerInfo.HSDLayer then
                  Exit
                else
                  LayerInfo.HSDVisible := InvertBooleanStyle(LayerInfo.HSDVisible, FALSE);
              fiUseForSnap: LayerInfo.UseForSnap := InvertBooleanStyle(LayerInfo.UseForSnap, FALSE);
              fiPrintable:
               {+++Brovak Bug 108 30.01.2001 If Layer Invisible then and not Print it}
                if LayerInfo.Visible = 0 then
                  LayerInfo.Printable := 0
                else
                  LayerInfo.Printable := InvertBooleanStyle(LayerInfo.Printable, FALSE);
              {-- Brovak}
            end;
            ARect := FieldRect(LayerListbox, Field, ItemIndex);
            InvalidateRect(Handle, @ARect, TRUE);
            FModified := TRUE;
          end;
        end;
  end;
end;

procedure TLayerDialog.InvalidateLayer
  (
  LayerInfo: TLayerProperties;
  Field: TFieldIndex
  );
var
  AGroup: TGroupEntry;
  ALayer: TGroupEntry;
  ARect: TRect;
  Cnt: Integer;
  Cnt1: Integer;
begin
  with GroupListbox do
    for Cnt := 0 to Tree.Count - 1 do
    begin
      AGroup := TGroupEntry(Tree[Cnt]);
      for Cnt1 := 0 to AGroup.Count - 1 do
      begin
        ALayer := TGroupEntry(AGroup[Cnt1]);
        if ALayer.LayerInfo = LayerInfo then
        begin
        { invalidate corresponding field in the listbox }
          ARect := FieldRect(GroupListbox, Field, Tree.ExpandedIndexOf(ALayer));
          InvalidateRect(Handle, @ARect, TRUE);
        { invalidate corresponding field of the owning group }
          ARect := FieldRect(GroupListbox, Field, Tree.ExpandedIndexOf(AGroup));
          InvalidateRect(Handle, @ARect, TRUE);
        end;
      end;
    end;
end;

procedure TLayerDialog.GroupListboxMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  Field: TFieldIndex;
  Index: Integer;
  Group: TGroupEntry;
  Value: ShortInt;
  Cnt: Integer;

  procedure SetValue
      (
      LayerInfo: TLayerProperties
      );
  begin
    with LayerInfo do
      case Field of
        fiVisible:
          if Visible = Value then
            Exit
          else
          begin
            Visible := Value;
            if HSDLayer then
              HSDVisible := Value;
          end;
        fiHSDVisible:
          if not HSDLayer or (HSDVisible = Value) then
            Exit
          else
            HSDVisible := Value;
        fiUseForSnap:
          if UseForSnap = Value then
            Exit
          else
            UseForSnap := Value;
        fiPrintable:
          if Printable = Value then
            Exit
          else
            Printable := Value;
      end;
    InvalidateLayer(LayerInfo, Field);
    FModified := TRUE;
  end;
begin
  if Button = mbLeft then
  begin
    Index := GroupListbox.ItemAtPos(Point(X, Y), TRUE);
    with GroupListbox do
      if Index >= 0 then
      begin
        Field := FieldAtPos(GroupListbox, Point(X, Y));
        if (FLastSelected = -1) and (Field = fiText) then
          ItemIndex := Index;
        if (SelCount = 1)
          and (Field = FClickField)
          and (Field in [fiVisible, fiHSDVisible, fiUseForSnap, fiPrintable])
          and Selected[Index] then
        begin
          Group := TGroupEntry(Tree.ExpandedItems[Index]);
          case Field of
            fiVisible: Value := InvertBooleanStyle(Group.LayerInfo.Visible, FALSE);
            fiHSDVisible: Value := InvertBooleanStyle(Group.LayerInfo.HSDVisible, FALSE);
            fiUseForSnap: Value := InvertBooleanStyle(Group.LayerInfo.UseForSnap, FALSE);
            fiPrintable: Value := InvertBooleanStyle(Group.LayerInfo.Printable, FALSE);
          end;
          if not Group.IsGroup then
            SetValue(Group.LayerInfo)
          else
            for Cnt := 0 to Group.Count - 1 do
              SetValue(TGroupEntry(Group[Cnt]).LayerInfo);
          UpdateLayerGroups;
        end;
      end;
  end;
end;

function TLayerDialog.FieldAtPos(Listbox: TWCustomListbox; Pos: TPoint): TFieldIndex;
var
  Cnt: TFieldIndex;
  ARect: TRect;
begin
  for Cnt := Low(TFieldIndex) to High(TFieldIndex) do
  begin
    ARect := FieldRect(Listbox, Cnt, 0);
    if (Pos.X >= ARect.Left) and (Pos.X <= ARect.Right) then
    begin
      Result := Cnt;
      Exit;
    end;
  end;
  Result := fiNone;
end;

procedure TLayerDialog.LayerListboxMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  Index: Integer;
begin
  if Button = mbLeft then
    with TWCustomListbox(Sender) do
    begin
      FMouseDownPos := Point(X, Y);
      Index := ItemAtPos(FMouseDownPos, TRUE);
      FLastSelected := -1;
      if Index < 0 then
        FClickField := fiNone
      else
      begin
        FClickField := FieldAtPos(TWCustomListbox(Sender), FMouseDownPos);
        FTooltipDialog.LayerListBox.ItemIndex := Index;
        if (Shift * [ssShift, ssCtrl] <> []) then
          FLastSelected := Index
        else
        begin
          DoInherited := SelCount <= 1;
          if not Selected[Index] then
          begin
            FLastSelected := Index;
            DoInherited := DoInherited or (FClickField in [fiCurrent, fiText]);
          end
          else
            if FClickField in [fiCurrent, fiText] then
              FocusedIndex := Index;
        end;
      end;
    end;
end;

function TLayerDialog.EditPattern(ToEdit: TList): Boolean;
{$IFNDEF AXDLL} // <----------------- AXDLL
var
  Cnt: Integer;
  Dialog: TPatternDialog;
  LayerInfo: TLayerProperties;
  ReadOnly: Boolean;
{$ENDIF}
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  Result := FALSE;
  if not CheckEditLayersForHSD(ToEdit, Readonly) then
    Exit;
  if ToEdit.Count <= 0 then
    exit;
  Dialog := TPatternDialog.Create(Self);
  with Dialog do
  try
    FillStyle.PropertyMode := pmCollect;
    AllowXFillStyles := TRUE;
    ProjectStyles := FProjectStyles;
    Project := FProject;
    for Cnt := 0 to ToEdit.Count - 1 do
    begin
      LayerInfo := ToEdit[Cnt];
      FillStyle.Assign(LayerInfo.FillStyle);
    end;
    FillStyle.UpdatesEnabled := not ReadOnly;
    if ShowModal = mrOK then
    begin
      for Cnt := 0 to ToEdit.Count - 1 do
      begin
        LayerInfo := ToEdit[Cnt];
        LayerInfo.PropertyMode := pmUpdate;
        if not FHSDLocked or not LayerInfo.HSDLayer then
          LayerInfo.FillStyle.Assign(FillStyle);
      end;
      Result := TRUE;
    end;
  finally
    Dialog.Free;
  end;
{$ENDIF}
end;

function TLayerDialog.EditSymbolFill(ToEdit: TList): Boolean;
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
var
  Cnt: Integer;
  Dialog: TSymbolFillDialog;
  LayerInfo: TLayerProperties;
  ReadOnly: Boolean;
{$ENDIF}
{$ENDIF}
begin
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
  Result := FALSE;
  if not CheckEditLayersForHSD(ToEdit, Readonly) then
    Exit;
  if ToEdit.Count <= 0 then
    exit;
  Dialog := TSymbolFillDialog.Create(Self);
  Dialog.Project := Project;
  with Dialog do
  try
    SymbolFill.PropertyMode := pmCollect;
    ProjectStyles := FProjectStyles;
    for Cnt := 0 to ToEdit.Count - 1 do
    begin
      LayerInfo := ToEdit[Cnt];
      SymbolFill.Assign(LayerInfo.SymbolFill);
    end;
    SymbolFill.UpdatesEnabled := not ReadOnly;
    if ShowModal = mrOK then
    begin
      for Cnt := 0 to ToEdit.Count - 1 do
      begin
        LayerInfo := ToEdit[Cnt];
        LayerInfo.PropertyMode := pmUpdate;
        if not FHSDLocked or not LayerInfo.HSDLayer then
          LayerInfo.SymbolFill.Assign(SymbolFill);
      end;
      Result := TRUE;
    end
    else
      Result := FALSE;
  finally
    Dialog.Free;
  end;
{$ENDIF}
{$ENDIF}
end;

function TLayerDialog.EditLineType(ToEdit: TList): Boolean;
{$IFNDEF AXDLL} // <----------------- AXDLL
var
  Cnt: Integer;
  Dialog: TLineStyleDialog;
  LayerStyle: TLayerProperties;
  ReadOnly: Boolean;
{$ENDIF}
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  Result := FALSE;
  if not CheckEditLayersForHSD(ToEdit, Readonly) then
    Exit;
  if ToEdit.Count <= 0 then
    exit;
  Dialog := TLineStyleDialog.Create(Self);
  with Dialog do
  try
    Project := FProject;
    ProjectStyles := FProjectStyles;
    LineStyle.PropertyMode := pmCollect;
    for Cnt := 0 to ToEdit.Count - 1 do
    begin
      LayerStyle := ToEdit[Cnt];
      LineStyle.Assign(LayerStyle.LineStyle);
    end;
    LineStyle.UpdatesEnabled := not ReadOnly;
    if ShowModal = mrOK then
    begin
      for Cnt := 0 to ToEdit.Count - 1 do
      begin
        LayerStyle := ToEdit[Cnt];
        with LayerStyle.LineStyle do
        begin
          PropertyMode := pmUpdate;
          if not FHSDLocked or not LayerStyle.HSDLayer then
            Assign(LineStyle);
          Validate;
        end;
      end;
      Result := TRUE;
    end;
  finally
    Dialog.Free;
  end;
{$ENDIF}
end;
{function changed very hard by Brovak 7.9.2000 BUG#39 BUILD127}

function TLayerDialog.EditGeneralize(ToEdit: TList): Boolean;
var
  Cnt: Integer;
  Dialog: TGeneralizeDialog;
  LayerInfo: TLayerProperties;
  GenNotChange, tmpValue, tmpvalue2: double;

   {returns real value Min}

  function GetMinGen(TmpValue: double): double;
  begin;
    if (TmpValue = GENERALIZE_MIN_OLD) or (TmpValue = GENERALIZE_MIN) or (TmpValue <= dblNotSet) then
      Result := 0
    else
      if TmpValue < GENERALIZE_MIN then
        Result := abs(TmpValue - GENERALIZE_MIN)
      else
        Result := TmpValue;
  end;
   {returns real value Max}

  function GetMaxGen(TmpValue: double): double;
  begin;
    if (TmpValue = GENERALIZE_MAX_OLD) or (TmpValue = GENERALIZE_MAX) or (TmpValue <= dblNotSet) then
      Result := 0
    else
      if TmpValue > GENERALIZE_MAX then
        Result := (TmpValue - GENERALIZE_MAX) / 1E210
      else
        Result := TmpValue;
  end;

begin
  GenNotChange := dblNoChange;
  Result := FALSE;
  if ToEdit.Count <= 0 then
    exit;
  Dialog := TGeneralizeDialog.Create(Self);
  with Dialog, GeneralizeStyle do
  try
    PropertyMode := pmCollect;
    MinL := GENERALIZE_MAX; // Min of all layer
    MaxL := GENERALIZE_MIN; // Max of all layer
    for Cnt := 0 to ToEdit.Count - 1 do
    begin
      LayerInfo := ToEdit[Cnt];
      GeneralizeType := LayerInfo.Generalize;
      TmpValue := LayerInfo.GeneralMin;
      if TmpValue = GENERALIZE_MIN_OLD then
        GeneralizeMin := GENERALIZE_MIN
      else
        GeneralizeMin := TmpValue;

      TmpValue := LayerInfo.GeneralMax;
      if TmpValue = GENERALIZE_MAX_OLD then
        GeneralizeMax := GENERALIZE_MAX
      else
        GeneralizeMax := LayerInfo.GeneralMax;

      TmpValue := GetMinGen(LayerInfo.GeneralMin);
      if (TmpValue > 0) and (1 / TmpValue > 1 / MinL) then
        MinL := TmpValue;

      TmpValue := GetMaxGen(LayerInfo.GeneralMax);
      if (TmpValue > 0) and (1 / TmpValue < 1 / MaxL) then
        MaxL := TmpValue;

    end;

    if ShowModal = mrOK then
    begin
      for Cnt := 0 to ToEdit.Count - 1 do
      begin
        LayerInfo := ToEdit[Cnt];
        PropertyMode := pmUpdate;

        with LayerInfo do
        begin
          case Dialog.OptionsCombo.ItemIndex of
            0: //always
              begin;
                if GeneralizeMax <> GenNotChange then
                  GeneralMax := GeneralizeMax
                else
                  GeneralMax := GetMaxGen(GeneralMax) * 1E210 + GENERALIZE_MAX;

                if GeneralizeMin <> GenNotChange then
                  GeneralMin := GeneralizeMin
                else
                  GeneralMin := GENERALIZE_MIN - GetMinGen(GeneralMin);

              end;

            1:
              begin; //smaller
                if GeneralizeMax <> GenNotChange then
                  GeneralMax := GeneralizeMax
                else
                  GeneralMax := GetMaxGen(GeneralMax);

                GeneralMin := GENERALIZE_MIN - GetMinGen(GeneralMin);
              end;

            2:
              begin; //bigger
                if GeneralizeMin <> GenNotChange then
                  GeneralMin := GeneralizeMin
                else
                  GeneralMin := GetMinGen(GeneralMin);

                GeneralMax := GetMaxGen(GeneralMax) * 1E210 + GENERALIZE_MAX;
              end;

            3:
              begin; //beetween

                if not ((GeneralizeMin = GenNotChange) and (GeneralizeMax = GenNotChange)) {//<-don't change all} then
                  if (GeneralizeMin <> GenNotChange) //changed Min
                    and (GeneralizeMax <> GenNotChange) {// and Max} then
                  begin;
                    if GeneralizeMax > GeneralizeMin then
                    begin;
                      GeneralMin := GeneralizeMin;
                      GeneralMax := GeneralizeMax;
                    end
                    else
                    begin;
                      GeneralMin := GeneralizeMax;
                      GeneralMax := GeneralizeMin;
                    end
                  end // (GeneralizeMin<>GenNotChange) and  GeneralizeMax<>GenNotChange)

                  else
                    if GeneralizeMin = GenNotChange {//in this case  GeneralizeMax<> GenNotChange} then
                    begin;
                          //extracting GeneralMin value for this layer.
                      TmpValue := GetMinGen(GeneralMin);
                      if (TmpValue = 0) or (TmpValue < GeneralizeMax) then
                      begin;
                        GeneralMax := GeneralizeMax;
                            //   GeneralMin :=GENERALIZE_MIN-GetMinGen(GeneralMin);
                      end
                      else
                      begin;
                             //  GeneralMin := GeneralizeMax;
                        GeneralMax := TmpValue;
                      end;
                    end //GeneralizeMin=GenNotChange
                    else
                    begin; //GeneralizeMax=GenNotChange
                           //extracting GeneralMax value for this layer.
                      TmpValue := GetMaxGen(GeneralMax);
                      if (TmpValue = 0) or (TmpValue > GeneralizeMin) then
                      begin;
                        GeneralMin := GeneralizeMin;
                              // GeneralMax:=GetMaxGen(GeneralMax)*1E210+GENERALIZE_MAX;
                      end
                      else
                      begin;
                        GeneralMin := TmpValue;
                             //  GeneralMax := GeneralizeMin;
                      end;
                    end //GeneralizeMax=GenNotChange
                  else
                    begin; //GeneralizeMax=GenNotChange and GeneralizeMin=GenNotChange
                      TmpValue := GetMaxGen(GeneralMax);
                      TmpValue2 := GetMinGen(GeneralMin);
                      if (TmpValue = TmpValue2) and (TmpValue = 0) {//not set or gen const} then
                      begin;
                        GeneralMax := GENERALIZE_MAX;
                        GeneralMin := GENERALIZE_MIN
                      end
                      else
                        if TmpValue2 > TmpValue then
                        begin;
                          GeneralMax := TmpValue2;
                          GeneralMin := TmpValue
                        end
                        else
                        begin;
                          GeneralMax := TmpValue;
                          GeneralMin := TmpValue2;
                        end;
                    end; //GeneralizeMax=GenNotChange and GeneralizeMin=GenNotChange

              end; //between

          end; //case
        end; {with}
      end; {for}
      Result := TRUE;
    end {ShowModal}
    else
      Exit;
  finally
    Dialog.Free;
  end;

end;
{---brovak}

procedure TLayerDialog.LayerListboxDblClick(Sender: TObject);
var
  ARect: TRect;
  Cnt: Integer;
  Field: TFieldIndex;
  Index: Integer;
  LayerInfo: TLayerProperties;
  Value: ShortInt;
  ToEdit: TList;
begin
  with LayerListbox do
  begin
    Field := FieldAtPos(LayerListbox, FMouseDownPos);
    Index := ItemAtPos(FMouseDownPos, TRUE);
    if (Field <> fiNone) and (Index >= 0) and ((SelCount > 1)
      or (Field in [fiLocked, fiLineType, fiPattern, fiGeneralize, fiSymbolFill])) then
    begin
      ToEdit := TList.Create;
      try
        for Cnt := 0 to Count - 1 do
          if Selected[Cnt] then
            ToEdit.Add(FLayerList[Cnt]);
        LayerInfo := FLayerList[Index];
        case Field of
          fiVisible: Value := InvertBooleanStyle(LayerInfo.Visible, FALSE);
          fiHSDVisible:
            if not LayerInfo.HSDLayer then
              Exit
            else
              Value := InvertBooleanStyle(LayerInfo.HSDVisible, FALSE);
          fiLocked:
            if WingisLicense >= WG_STD then
            begin
                        {+++Fixed by Brovak - for password for FixLayer}
              if InputCheckPassword(Self) then
              begin
                Value := InvertBooleanStyle(LayerInfo.Editable, FALSE)
              end
              else
                Exit;
            end;
                        {---Brovak}
          fiUseForSnap: Value := InvertBooleanStyle(LayerInfo.UseForSnap, FALSE);
          fiPrintable: Value := InvertBooleanStyle(LayerInfo.Printable, FALSE);
          fiGeneralize:
            if not EditGeneralize(ToEdit) then
              Exit;
          fiLineType:
            if not EditLineType(ToEdit) then
              Exit;
          fiPattern:
            if not EditPattern(ToEdit) then
              Exit;
          fiSymbolFill:
            if not EditSymbolFill(ToEdit) then
              Exit;
        end;
        for Cnt := 0 to Count - 1 do
          if Selected[Cnt] then
          begin
            LayerInfo := FLayerList[Cnt];
            case Field of
              fiVisible:
                if LayerInfo.Visible = Value then
                  Continue
                else
                begin
                  LayerInfo.Visible := Value;
                  if LayerInfo.HSDLayer then
                  begin
                    LayerInfo.HSDVisible := LayerInfo.Visible;
                    ARect := FieldRect(LayerListbox, fiHSDVisible, Cnt);
                    InvalidateRect(Handle, @ARect, TRUE);
                  end;
                end;
              fiHSDVisible:
                if not LayerInfo.HSDLayer or (LayerInfo.HSDVisible = Value) then
                  Continue
                else
                  LayerInfo.HSDVisible := Value;
              fiLocked:
                if WingisLicense >= WG_STD then
                begin
                  if LayerInfo.Editable = Value then
                    Continue
                  else
                    LayerInfo.Editable := Value;
                end;
              fiUseForSnap:
                if LayerInfo.UseForSnap = Value then
                  Continue
                else
                  LayerInfo.UseForSnap := Value;
              fiPrintable:
                if LayerInfo.Printable = Value then
                  Continue
                else
                  LayerInfo.Printable := Value;
            end;
            ARect := FieldRect(LayerListbox, Field, Cnt);
            InvalidateRect(Handle, @ARect, TRUE);
            FModified := TRUE;
          end;
      finally
        ToEdit.Free;
      end;
    end
    else
      if (SelCount = 1) and (Field = fiText) then
        PropertiesMenuClick(Self);
  end;
end;

procedure TLayerDialog.GroupListboxDblClick(Sender: TObject);
var
  Cnt: Integer;
  Cnt1: Integer;
  Field: TFieldIndex;
  Index: Integer;
  LayerInfo: TLayerProperties;
  Value: ShortInt;
  ToEdit: TList;
  Group: TGroupEntry;
begin
  with GroupListbox do
  begin
    Field := FieldAtPos(GroupListbox, FMouseDownPos);
    Index := ItemAtPos(FMouseDownPos, TRUE);
    if (Index >= 0)
      and (Field <> fiNone)
      and ((GroupListbox.SelCount > 1)
      or (Field in [fiLocked, fiLineType, fiPattern, fiGeneralize, fiSymbolFill])) then
    begin
      ToEdit := TList.Create;
      try
        { create list of selected layers and layers in selected groups }
        Cnt := 0;
        while Cnt < Tree.ExpandedCount do
        begin
          if Selected[Cnt] then
          begin
            Group := TGroupEntry(Tree.ExpandedItems[Cnt]);
            if Group.IsGroup then
            begin
              for Cnt1 := 0 to Group.Count - 1 do
              begin
                LayerInfo := TGroupEntry(Group[Cnt1]).LayerInfo;
                if ToEdit.IndexOf(LayerInfo) < 0 then
                  ToEdit.Add(LayerInfo);
              end;
              Inc(Cnt, Group.Count);
            end
            else
              if ToEdit.IndexOf(Group.LayerInfo) < 0 then
                ToEdit.Add(Group.LayerInfo);
          end;
          Inc(Cnt);
        end;
        { calculate new value from the layer under the cursor }
        LayerInfo := TGroupEntry(Tree.ExpandedItems[Index]).LayerInfo;
        case Field of
          fiVisible: Value := InvertBooleanStyle(LayerInfo.Visible, FALSE);
          fiHSDVisible: Value := InvertBooleanStyle(LayerInfo.HSDVisible, FALSE);
          fiLocked: {+++Fixed by Brovak - for password for FixLayer}
            if InputCheckPassword(Self) then
              Value := InvertBooleanStyle(LayerInfo.Editable, FALSE)
            else
              Exit;
                        {---Brovak}
          fiUseForSnap: Value := InvertBooleanStyle(LayerInfo.UseForSnap, FALSE);
          fiPrintable: Value := InvertBooleanStyle(LayerInfo.Printable, FALSE);
          fiGeneralize:
            if not EditGeneralize(ToEdit) then
              Exit;
          fiLineType:
            if not EditLineType(ToEdit) then
              Exit;
          fiPattern:
            if not EditPattern(ToEdit) then
              Exit;
          fiSymbolFill:
            if not EditSymbolFill(ToEdit) then
              Exit;
        end;
        for Cnt := 0 to ToEdit.Count - 1 do
        begin
          LayerInfo := ToEdit[Cnt];
          case Field of
            fiVisible:
              if LayerInfo.Visible = Value then
                Continue
              else
              begin
                LayerInfo.Visible := Value;
                if LayerInfo.HSDLayer then
                  LayerInfo.HSDVisible := Value;
              end;
            fiHSDVisible:
              if not LayerInfo.HSDLayer or (LayerInfo.HSDVisible = Value) then
                Continue
              else
                LayerInfo.HSDVisible := Value;
            fiLocked:
              if LayerInfo.Editable = Value then
                Continue
              else
                LayerInfo.Editable := Value;
            fiUseForSnap:
              if LayerInfo.UseForSnap = Value then
                Continue
              else
                LayerInfo.UseForSnap := Value;
            fiPrintable:
              if LayerInfo.Printable = Value then
                Continue
              else
                LayerInfo.Printable := Value;
          end;
          InvalidateLayer(LayerInfo, Field);
          FModified := TRUE;
        end;
        UpdateLayerGroups;
      finally
        ToEdit.Free;
      end;
    end;
  end;
end;

procedure TLayerDialog.DeleteBtnClick(Sender: TObject);
var
  LayerInfo: TLayerProperties;
  DoDelete: Boolean;
  Cnt: Integer;
  NewSelected: Integer;
  GroupEntry: TGroupEntry;
  LayerEntry: TGroupEntry;
  Cnt1: Integer;
  Cnt2: Integer;
  FixedLayer: Boolean;
begin
  with LayerListbox do
    if SelCount > 0 then
    begin
    // determine if one of the selected layers is fixed
      FixedLayer := FALSE;
      for Cnt := 0 to Items.Count - 1 do
        if Selected[Cnt] then
        begin
          LayerInfo := TLayerProperties(FLayerList[Cnt]);
          if LayerInfo.Editable <> bsYes then
            FixedLayer := TRUE;
        end;
      if FixedLayer then
      begin
        if SelCount > 1 then
          DoDelete := MessageDialog(MlgSection[36], mtConfirmation,
            [mbYes, mbNo], 0, mbNo) = mrYes
        else
        begin
          MessageDialog(Format(MlgSection[35], [LayerInfo.Name]), mtInformation, [mbOK], 0);
          DoDelete := FALSE;
        end;
      end
      else
        if SelCount > 1 then
          DoDelete := MessageDialog(MlgSection[7],
            mtConfirmation, [mbYes, mbNo], 0, mbNo) = mrYes
        else
        begin
          LayerInfo := FLayerList[ItemIndex];
          DoDelete := MessageDialog(Format(MlgSection[6], [LayerInfo.Name]),
            mtConfirmation, [mbYes, mbNo], 0, mbNo) = mrYes;
        end;
      if DoDelete then
      begin
        NewSelected := ItemIndex;
        for Cnt := Count - 1 downto 0 do
          if Selected[Cnt] then
          begin
            LayerInfo := FLayerList[Cnt];
            if LayerInfo.Editable = bsYes then
            begin
          // remove the layer from the layer groups
              if GroupListbox <> nil then
                with GroupListbox do
                  for Cnt1 := 0 to Tree.Count - 1 do
                  begin
                    GroupEntry := TGroupEntry(Tree.Items[Cnt1]);
                    for Cnt2 := GroupEntry.Count - 1 downto 0 do
                    begin
                      LayerEntry := TGroupEntry(GroupEntry.Items[Cnt2]);
                      if LayerEntry.LayerInfo = FLayerList[Cnt] then
                      begin
                        GroupEntry.Delete(Cnt2);
                        LayerEntry.Free;
                      end;
                    end;
                  end;
          // correct toplayer-index
              if Cnt = FTopLayerIndex then
                FTopLayerIndex := 0
              else
                if Cnt < FTopLayerIndex then
                  Dec(FTopLayerIndex);
          // free memory of layer-style
              TLayerProperties(FLayerList[Cnt]).Free;
          // remove layer from the layer-list
              FLayerList.Delete(Cnt);
              NewSelected := Cnt;
            end;
          end;
        Count := FLayerList.Count;
        if NewSelected >= Count then
          NewSelected := Count - 1;
        if NewSelected > -1 then
          ItemIndex := NewSelected;
        GroupListbox.UpdateView;
        UpdateEnabled;
      end;
    end;
end;

procedure TLayerDialog.LayerListboxMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
var
  Index: TFieldIndex;
begin
  with LayerListbox do
  begin
    if not Dragging and not FCurSorting and (FClickField = fiText)
      and (ssLeft in Shift) and ((Abs(FMouseDownPos.X - X) >= minDragMove)
      or (Abs(FMouseDownPos.Y - Y) >= minDragMove)) then
    begin
      FPendingUps := 2;
      FLastDrag := -1;
      BeginDrag(TRUE);
    end;
    Index := FieldAtPos(LayerListbox, Point(X, Y));
    if Index <> FLastIndex then
    begin
      Application.CancelHint;
      FLastIndex := Index;
      LayerListbox.Hint := FieldHint(Index);
    end;
    DoInherited := FALSE;
  end;
end;

procedure TLayerDialog.LayerListboxDragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
var
  Index: Integer;
begin
  with LayerListbox do
  begin
    if State = dsDragLeave then
      Index := -1
    else
      Index := ItemAtPos(Point(X, Y), TRUE);
    Accept := (Y >= 0)
      and (Source = LayerListbox)
      and ((Index = -1) or not Selected[Index]);
    if Index <> FLastDrag then
    begin
      if (FLastDrag > -1)
        and not Selected[FLastDrag] then
        LayerListBoxDrawItem(LayerListbox,
          FLastDrag, FieldRect(LayerListbox, fiText, FLastDrag), []);
      FLastDrag := Index;
      if (FLastDrag > -1)
        and not Selected[FLastDrag] then
        LayerListBoxDrawItem(LayerListbox,
          FLastDrag, FieldRect(LayerListbox, fiText, FLastDrag), [odGrayed]);
    end;
  end;
end;

procedure TLayerDialog.MoveSelection(Index: Integer);
var
  ARect: TRect;
  Cnt: Integer;
  TopLayer: TLayerProperties;
  Inserted: Integer;
  FSelected: TList;
  First: Integer;
  Last: Integer;
begin
  with LayerListbox do
    if (Index >= 0) and (Index <= Items.Count - 1) then
    begin
      TopLayer := FLayerList[FTopLayerIndex];
      FSelected := TList.Create;
      try
        Inserted := 0;
        First := -1;
        for Cnt := Count - 1 downto 0 do
          if Selected[Cnt] then
          begin
            FSelected.Add(FLayerList[Cnt]);
            FLayerList.Delete(Cnt);
            if Cnt <= Index then
              Inc(Inserted);
            if First = -1 then
              First := Cnt;
            Last := Cnt;
          end;
        if Inserted > 0 then
          Index := Index - Inserted + 1;
        for Cnt := 0 to FSelected.Count - 1 do
          FLayerList.Insert(Index, FSelected[Cnt]);
        FTopLayerIndex := FLayerList.IndexOf(TopLayer);
        Selected[-1] := FALSE;
        RangeSelected[Index, Index + FSelected.Count - 1] := TRUE;
        SendMessage(Handle, lb_SetSel, 1, Index);
        First := Min(Index, First);
        Last := Max(Index + FSelected.Count - 1, Last);
        ARect := Rect(0, (First - TopIndex) * ItemHeight, ClientWidth, (Last - TopIndex) * ItemHeight);
        InvalidateRect(Handle, @ARect, TRUE);
        UpdateEnabled;
      finally
        FSelected.Free;
      end;
    end;
end;

procedure TLayerDialog.AddBtnClick(Sender: TObject);
var
  AIndex: Integer;
  LayerInfo: TLayerProperties;
  ARect: TRect;

  function ExistsLayer
      (
      const AName: string
      )
      : Boolean;
  var
    Cnt: Integer;
  begin
    for Cnt := 0 to FLayerList.Count - 1 do
      if AnsiCompareText(TLayerProperties(FLayerList[Cnt]).Name, AName) = 0 then
      begin
        Result := TRUE;
        Exit;
      end;
    Result := FALSE;
  end;

begin
  with LayerListbox do
  begin
    AIndex := 0;
    if ExistsLayer(MlgSection[13]) then
      repeat
        Inc(AIndex);
      until not ExistsLayer(MlgSection[13] + ' ' + IntToStr(AIndex));
    LayerInfo := TLayerProperties.Create;
    with LayerInfo do
    begin
      if AIndex = 0 then
        Name := MlgSection[13]
      else
        Name := MlgSection[13] + ' ' + IntToStr(AIndex);
    end;
    if FCurSorting then
    begin
      FLayerList.Add(LayerInfo);
      AIndex := FLayerList.IndexOf(LayerInfo);
    end
    else
    begin
      AIndex := Max(0, ItemIndex);
      FLayerList.Insert(AIndex, LayerInfo);
    end;
    Count := FLayerList.Count;
    if Count = 1 then {++Sygsky: check insertion to empty layer list }
    begin
      FTopLayerIndex := 0;
      ARect := FieldRect(LayerListbox, fiCurrent, FTopLayerIndex);
      InvalidateRect(Handle, @ARect, TRUE);
    end
    else
      if FTopLayerIndex >= AIndex then
      begin
        Inc(FTopLayerIndex);
        LayerListBox.Repaint;
      end;
    ItemIndex := AIndex;
  end;
  UpdateEnabled;
end;

procedure TLayerDialog.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
var
  LayerInfo: TLayerProperties;
  i: Integer;

  function CheckBlankNames: Boolean;
  var
    Cnt: Integer;
  begin
    for Cnt := 0 to FLayerList.Count - 1 do
    begin
      LayerInfo := FLayerList[Cnt];
      if LayerInfo.Name = '' then
      begin
        MessageDialog(MlgSection[11], mtInformation, [mbOK], 0);
        LayerListbox.ItemIndex := Cnt;
        Result := FALSE;
        Exit;
      end;
    end;
    Result := TRUE;
  end;

  function CheckDuplicateNames: Boolean;
  var
    Cnt1: Integer;
    Cnt2: Integer;
  begin
    for Cnt1 := 0 to FLayerList.Count - 2 do
    begin
      for Cnt2 := Cnt1 + 1 to FLayerList.Count - 1 do
      begin
        if AnsiCompareText(TLayerProperties(FLayerList[Cnt1]).Name,
          TLayerProperties(FLayerList[Cnt2]).Name) = 0 then
        begin
          MessageDialog(Format(MlgSection[12], [TLayerProperties(FLayerList[Cnt1]).Name]),
            mtInformation, [mbOK], 0);
          LayerListbox.ItemIndex := Cnt1;
          Result := FALSE;
          Exit;
        end;
      end;
    end;
    Result := TRUE;
  end;

  function CheckDuplicateGroupNames: Boolean;
  var
    Cnt1: Integer;
    Cnt2: Integer;
  begin
    if GroupListBox <> nil then
      with GroupListbox do
        for Cnt1 := 0 to Tree.Count - 2 do
        begin
          for Cnt2 := Cnt1 + 1 to Tree.Count - 1 do
          begin
            if AnsiCompareText(TGroupEntry(Tree[Cnt1]).Caption,
              TGroupEntry(Tree[Cnt2]).Caption) = 0 then
            begin
              MessageDialog(Format(MlgSection[33], [TGroupEntry(Tree[Cnt1]).Caption]),
                mtInformation, [mbOK], 0);
              ItemIndex := Tree.ExpandedIndexOf(Tree[Cnt1]);
              Result := FALSE;
              Exit;
            end;
          end;
        end;
    Result := TRUE;
  end;
begin
  CanClose := FALSE;
  if (ModalResult <> mrOK) and (ModalResult <> mrRetry) then
    CanClose := TRUE
  else
    if FLayerList.Count = 0 then
      MessageDialog(MlgSection[34], mtError, [mbOK], 0)
    else
      if CheckValidators(Self) then
      begin
        CanClose := FALSE;
        LayerInfo := FLayerList[FTopLayerIndex];
        if LayerInfo.Visible <> bsYes then
        begin
          for i := 0 to FLayerList.Count - 1 do
          begin
            LayerInfo := FLayerList[i];
            if LayerInfo.Visible = bsYes then
            begin
              FTopLayerIndex := i;
              CanClose := True;
              break;
            end;
          end;
          if not CanClose then
          begin
            MessageDialog(MlgSection[8], mtInformation, [mbOK], 0);
          end;
          LayerListbox.ItemIndex := FTopLayerIndex;
        end
        else
          if CheckBlankNames and CheckDuplicateNames
            and CheckDuplicateGroupNames then
            CanClose := TRUE;
      end;
end;

function TLayerDialog.GetTopLayer
  : string;
begin
  if FLayerList.Count = 0 then
    Result := ''
  else
    Result := TLayerProperties(FLayerList[FTopLayerIndex]).Name;
end;

procedure TLayerDialog.SetTopLayer(const ALayer: string);
var
  Cnt: Integer;
begin
  for Cnt := 0 to FLayerList.Count - 1 do
    if AnsiCompareText(ALayer, TLayerProperties(FLayerList[Cnt]).Name) = 0 then
    begin
      FTopLayerIndex := Cnt;
      Exit;
    end;
  FTopLayerIndex := -1;
end;

procedure TLayerDialog.SelectAllMenuClick(Sender: TObject);
begin
  LayerListbox.RangeSelected[0, FLayerList.Count - 1] := TRUE;
  UpdateEnabled;
end;

procedure TLayerDialog.FormShow(Sender: TObject);
var
  ARect: TRect;
begin
  Application.ProcessMessages;
  if (LastLeft <> 0) and (LastTop <> 0) then
  begin
    Left := LastLeft;
    Top := LastTop;
  end
  else
  begin
    Left := Trunc(Screen.Width / 2) - Trunc(Width / 2);
    Top := Trunc(Screen.Height / 2) - Trunc(Height / 2);
  end;
  FProjectStyles.Assign(FProject^.PInfo^.ProjStyles);
  // create the groups-tree
  CreateLayerGroupTree;
  // activate the sorting of the layers
  SortLayers;
  // select first layer
  LayerListbox.ItemIndex := 0;
  // setup tooltip dialog
  FTooltipDialog.Project := FProject;
  FTooltipDialog.LayerList := FLayerList;
  FLegendDialog.LayerList := FLayerList;
  FLegendDialog.Project := FProject;
  // load dialog-settings from the registry
  with FProject^.Registry do
  begin
    CurrentPath := '\User\WinGISUI\LayerDialog';
    Notebook.PageIndex := ReadInteger('ActivePage');

    FCurSorting := False;
    AlphabeticBtn.Down := ReadBool('AlphabeticSorting');
    if AlphabeticBtn.Down then
    begin
      SetLayerSorting(True);
    end;
  end;
  // determine if project is a HSD project, hide HSDVisible column if not
  FHSDProject := FProject^.TurboVector;
  FAttribFixed := FProject^.TurboVector and FProject^.AttribFix;
  if not FHSDProject then
  begin
    FFieldRects[fiVisible] := FFieldRects[fiHSDVisible];
    FFieldRects[fiText].Y := FFieldRects[fiVisible].X - 1;
    FFieldRects[fiHSDVisible] := Point(500, 510);
  end;
  ARect := FieldRect(LayerListbox, fiText, 0);
  // setup inplace-editor
  LayerInplaceEditor.EditLeft := ARect.Left;
  LayerInplaceEditor.EditRight := ARect.Right;
  GroupInplaceEditor.EditRight := ARect.Right;
  // update enabled-states of the controls
  UpdateEnabled;
//  HierarchicBtn.Down:=True;

  Image1.Canvas.Brush.Color := clBtnFace;
  Image1.Canvas.FillRect(Rect(0, 0, Image1.Width, Image1.Height));
  Image2.Canvas.Brush.Color := clBtnFace;
  Image2.Canvas.FillRect(Rect(0, 0, Image1.Width, Image1.Height));

  Image1.Canvas.Pen.Color := clGray;
  Image1.Canvas.MoveTo(FFieldRects[fiText].x, 0);
  Image1.Canvas.LineTo(FFieldRects[fiText].x, Image1.Height);
  Image1.Canvas.TextOut(FFieldRects[fiText].x + 4, 1, FieldHint(fiText));
  Image1.Canvas.MoveTo(FFieldRects[fiVisible].x, 0);
  Image1.Canvas.LineTo(FFieldRects[fiVisible].x, Image1.Height);
  FImages.Draw(Image1.Canvas, FFieldRects[fiVisible].x + 3, 0, 0);
  Image1.Canvas.MoveTo(FFieldRects[fiUseForSnap].x, 0);
  Image1.Canvas.LineTo(FFieldRects[fiUseForSnap].x, Image1.Height);
  FImages.Draw(Image1.Canvas, FFieldRects[fiUseForSnap].x + 3, 0, 4);
  Image1.Canvas.MoveTo(FFieldRects[fiPrintable].x, 0);
  Image1.Canvas.LineTo(FFieldRects[fiPrintable].x, Image1.Height);
  FImages.Draw(Image1.Canvas, FFieldRects[fiPrintable].x + 3, 0, 3);
  Image1.Canvas.MoveTo(FFieldRects[fiLocked].x, 0);
  Image1.Canvas.LineTo(FFieldRects[fiLocked].x, Image1.Height);
  FImages.Draw(Image1.Canvas, FFieldRects[fiLocked].x + 3, 0, 1);
  Image1.Canvas.MoveTo(FFieldRects[fiGeneralize].x, 0);
  Image1.Canvas.LineTo(FFieldRects[fiGeneralize].x, Image1.Height);
  FImages.Draw(Image1.Canvas, FFieldRects[fiGeneralize].x + 3, 0, 2);
  Image1.Canvas.MoveTo(FFieldRects[fiLinetype].x, 0);
  Image1.Canvas.LineTo(FFieldRects[fiLinetype].x, Image1.Height);
  Image1.Canvas.TextOut(FFieldRects[fiLinetype].x + 4, 1, FieldHint(fiLinetype));
  Image1.Canvas.MoveTo(FFieldRects[fiPattern].x, 0);
  Image1.Canvas.LineTo(FFieldRects[fiPattern].x, Image1.Height);
  Image1.Canvas.TextOut(FFieldRects[fiPattern].x + 4, 1, FieldHint(fiPattern));
  Image1.Canvas.MoveTo(FFieldRects[fiSymbolFill].x, 0);
  Image1.Canvas.LineTo(FFieldRects[fiSymbolFill].x, Image1.Height);
  FImages.Draw(Image1.Canvas, FFieldRects[fiSymbolFill].x + 3, 0, 7);
{$IFNDEF AXDLL} // <----------------- AXDLL
  if StrToIntDef(GetDDEItem(DDEString, 6), 1) = 0 then
  begin
    DeleteDialog(FTooltipDialog);
  end;
  if StrToIntDef(GetDDEItem(DDEString, 5), 1) = 0 then
  begin
    DeleteDialog(FLegendDialog);
  end;
  if StrToIntDef(GetDDEItem(DDEString, 4), 1) = 0 then
  begin
    Notebook.Pages.Delete(2);
    Notebook.Update;
  end;
  if StrToIntDef(GetDDEItem(DDEString, 3), 1) = 0 then
  begin
    Notebook.Pages.Delete(1);
    Notebook.Update;
  end;
  if StrToIntDef(GetDDEItem(DDEString, 2), 1) = 0 then
  begin
    Notebook.Pages.Delete(0);
    Notebook.Update;
  end;
{$ENDIF}
  LayerListBox.Selected[0] := True;
  FixLayerButton.Enabled := (WingisLicense >= WG_STD);
end;

procedure TLayerDialog.FormHide(Sender: TObject);
begin
{++ Ivanoff BUG#446}
{I have added 'ModalResult = mrRetry'}
  if (ModalResult = mrOK) or (ModalResult = mrRetry) then
{-- Ivanoff BUG#446}
  begin
    // Palette ins Projekt kopieren
    Project^.PInfo^.ProjStyles.Assign(FProjectStyles);
    // store layer-groups to the registry
    WriteLayerGroupsToRegistry;
    // write dialog-settings to the registry
    with FProject^.Registry do
    begin
      CurrentPath := '\User\WinGISUI\LayerDialog';
      WriteInteger('ActivePage', Notebook.PageIndex);
      WriteBool('AlphabeticSorting', FCurSorting);
    end;
    // set hirarchic sorting of the layer-list
    SetLayerSorting(FALSE);
  end;
  LastLeft := Left;
  LastTop := Top;
end;

procedure TLayerDialog.ToTopMenuClick(Sender: TObject);
begin
  MoveSelection(0);
end;

procedure TLayerDialog.ToBottomMenuClick(Sender: TObject);
begin
  MoveSelection(FLayerList.Count - 1);
end;

procedure TLayerDialog.LayerListboxDragDrop(Sender, Source: TObject; X, Y: Integer);
var
  Index: Integer;
begin
  with LayerListbox do
  begin
    Index := ItemAtPos(Point(X, Y), TRUE);
    if Index = -1 then
      Index := Count - 1;
 SELF.MoveSelection(Index);
  end;
end;

procedure TLayerDialog.UpdateEnabled;
var
  AEnable: Boolean;
begin
  with LayerListbox do
  begin
    AEnable := SelCount > 0;
    DeleteAction.Enabled := AEnable;
    CreateGroupMenu.Enabled := AEnable;
    AEnable := (SelCount > 0) and not FCurSorting and (ItemIndex > 0);
    MoveUpAction.Enabled := AEnable;
    MoveUpBtn.Enabled := AEnable;
    MoveTopAction.Enabled := AEnable;
    AEnable := (SelCount > 0) and not FCurSorting and (ItemIndex < Items.Count - 1);
    MoveDownAction.Enabled := AEnable;
    MoveDownBtn.Enabled := AEnable;
    MoveBottomAction.Enabled := AEnable;
    PropertiesAction.Enabled := SelCount = 01;
  end;
  SelectAllMenu.Enabled := FLayerList.Count > 0;
  SearchBtn.Enabled := (FLayerList.Count > 0) and (Length(SearchEdit.Text) > 0);
  if (SearchBtn1 <> nil) and (SearchEdit1 <> nil) then
  begin
    SearchBtn1.Enabled := (FLayerList.Count > 0) and (Length(SearchEdit1.Text) > 0);
  end;
  if GroupListBox <> nil then
    with GroupListbox do
    begin
      AddLayerMenu.Enabled := (SelCount = 1) and (Tree.Count > 0);
      DeleteLayerOrGroupMenu.Enabled := ItemIndex >= 0;
    end;
end;

procedure TLayerDialog.LayerListboxClick(Sender: TObject);
begin
  UpdateEnabled;
end;

procedure TLayerDialog.SearchEditChange(Sender: TObject);
begin
  DoSearchEditChange(SearchEdit, LayerListbox, SearchBtn);
end;

procedure TLayerDialog.DoSearchEditChange(SearchEdit: TEdit; Listbox: TWCustomListbox;
  SearchBtn: TSpeedBtn);
var
  MaxEqualLen: Integer;
  MaxEqualIndex: Integer;
  MaxEqualAllLen: Integer;
  EqualLen: Integer;
  Cnt: Integer;
  LayerInfo: TLayerProperties;
begin
  MaxEqualLen := 0;
  MaxEqualAllLen := 0;
  MaxEqualIndex := -1;
  for Cnt := 0 to FLayerList.Count - 1 do
  begin
    LayerInfo := FLayerList[Cnt];
    EqualLen := CalculateEqualLen(SearchEdit.Text, LayerInfo.Name);
    if (EqualLen > MaxEqualLen) or ((EqualLen = MaxEqualLen)
      and (MaxEqualAllLen > ByteToCharIndex(LayerInfo.Name, Length(LayerInfo.Name)))) then
    begin
      MaxEqualLen := EqualLen;
      MaxEqualAllLen := ByteToCharIndex(LayerInfo.Name, Length(LayerInfo.Name));
      MaxEqualIndex := Cnt;
    end;
  end;
  if MaxEqualIndex >= 0 then
    Listbox.ItemIndex := MaxEqualIndex;
  SearchEdit.Text := Copy(SearchEdit.Text, 1, CharToByteIndex(SearchEdit.Text, MaxEqualLen));
  SearchEdit.SelStart := Length(SearchEdit.Text);
  SearchBtn.Enabled := Length(SearchEdit.Text) > 0;
end;

procedure TLayerDialog.SearchBtnClick(Sender: TObject);
begin
  DoSearchBtnClick(SearchEdit, LayerListbox);
end;

function SortAlphabetic(Item1: Pointer; Item2: Pointer): Integer; far;
begin
  Result := AnsiCompareText(TLayerProperties(Item1).Name, TLayerProperties(Item2).Name);
end;

function SortHirarchic(Item1: Pointer; Item2: Pointer): Integer; far;
begin
  Result := LongCompare(TLayerProperties(Item1).Index, TLayerProperties(Item2).Index);
end;

procedure TLayerDialog.SetLayerSorting(Alphabetic: Boolean);
var
  Cnt: Integer;
begin
  if Alphabetic <> FCurSorting then
  begin
    FCurSorting := Alphabetic;
    if FCurSorting then
      for Cnt := 0 to FLayerList.Count - 1 do
        TLayerProperties(FLayerList[Cnt]).Index := Cnt + 1;
    SortLayers;
    LayerListbox.Invalidate;
    if GeneraliseListBox <> nil then
      GeneraliseListbox.Invalidate;
    UpdateEnabled;
  end;
end;

procedure TLayerDialog.SortLayers;
var
  FTopLayer: TLayerProperties;
  FSelLayers: TList;
  Cnt: Integer;
begin
  FSelLayers := TList.Create;
  try
    with LayerListbox do
      for Cnt := 0 to Count - 1 do
        if Selected[Cnt] then
          FSelLayers.Add(FLayerList[Cnt]);
    LayerListbox.Selected[-1] := FALSE;
    FTopLayer := FLayerList[FTopLayerIndex];
    if FCurSorting then
      FLayerList.Sort(SortAlphabetic)
    else
      FLayerList.Sort(SortHirarchic);
    FTopLayerIndex := FLayerList.IndexOf(FTopLayer);
    for Cnt := 0 to FSelLayers.Count - 1 do
      LayerListbox.Selected[FLayerList.IndexOf(FSelLayers[Cnt])] := TRUE;
  finally
    FSelLayers.Free;
  end;
end;

procedure TLayerDialog.HirarchicBtnClick(Sender: TObject);
begin
  SetLayerSorting(FALSE);
end;

procedure TLayerDialog.AlphabeticBtnClick(Sender: TObject);
begin
  SetLayerSorting(TRUE);
end;

procedure TLayerDialog.LayerInplaceEditorEditing(Sender: TObject;
  Index: Integer; var AllowEdit: Boolean; var Text: string);
begin
  Text := TLayerProperties(FLayerList[Index]).Name;
  UpdateEnabled;
  FClickField := fiNone;
end;

procedure TLayerDialog.LayerInplaceEditorEdited(Sender: TObject; Index: Integer;
  const Text: string);

  function ExistsLayer
      (
      const AName: string
      )
      : Boolean;
  var
    Cnt: Integer;
  begin
    for Cnt := 0 to FLayerList.Count - 1 do
      if AnsiCompareText(TLayerProperties(FLayerList[Cnt]).Name, AName) = 0 then
      begin
        Result := TRUE;
        Exit;
      end;
    Result := FALSE;
  end;

begin
//  if Project^.Layers^.NameToLayer(Text) = nil then begin
  if not ExistsLayer(Text) then
  begin
    TLayerProperties(FLayerList[Index]).Name := Text;
  end;
  if FCurSorting then
    SortLayers;

  UpdateEnabled;
end;

procedure TLayerDialog.CreateLayerGroupTree;
type
  PLongArray = ^TLongArray;
  TLongArray = array[0..0] of LongInt;
var
  GroupCnt: Integer;
  Group: TGroupEntry;
  LayerCnt: Integer;
  LayerList: PLongArray;
  LayerInfo: TLayerProperties;
  GroupEntry: TGroupEntry;
  Cnt: Integer;
  Cnt1: Integer;
begin
  with Project^.Registry do
  begin
    GroupListbox.BeginUpdate;
    try
      OpenKey('\Project\LayerGroups', FALSE);
      GroupCnt := ReadInteger('Count');
      for Cnt := 0 to GroupCnt - 1 do
      begin
        OpenKey(Format('\Project\LayerGroups\Group%d', [Cnt]), FALSE);
        if LastError <> rdbeNoError then
          break;
        Group := TGroupEntry.Create(TRUE);
        try
          Group.Caption := ReadString('Name');
          LayerCnt := ReadInteger('Count');
          GetMem(LayerList, LayerCnt * SizeOf(LongInt));
          try
            ReadBinary('Layers', LayerList^, LayerCnt * SizeOf(LongInt));
            if LastError <> rdbeNoError then
              break;
            for Cnt1 := 0 to LayerCnt - 1 do
            begin
              LayerInfo := LayerInfoFromIndex(LayerList^[Cnt1]);
              if LayerInfo <> nil then
              begin
                GroupEntry := TGroupEntry.Create(FALSE);
                GroupEntry.LayerInfo := LayerInfo;
                Group.Add(GroupEntry);
              end;
              Group.SortByCaption(TRUE);
            end;
            GroupListbox.Tree.Add(Group);
          finally
            FreeMem(LayerList, LayerCnt * SizeOf(LongInt));
          end;
        except
          Group.Free;
          raise;
        end;
      end;
    finally
      GroupListbox.EndUpdate;
    end;
  end;
end;

function TLayerDialog.LayerInfoFromIndex
  (
  AIndex: LongInt
  )
  : TLayerProperties;
var
  Cnt: Integer;
begin
  for Cnt := 0 to FLayerList.Count - 1 do
    if TLayerProperties(FlayerList[Cnt]).Number = AIndex then
    begin
      Result := FLayerList[Cnt];
      Exit;
    end;
  Result := nil;
end;

function TLayerDialog.LayerInfoFromName
  (
  const AName: string
  )
  : TLayerProperties;
var
  Cnt: Integer;
begin
  for Cnt := 0 to FLayerList.Count - 1 do
    if AnsiCompareText(TLayerProperties(FlayerList[Cnt]).Name, AName) = 0 then
    begin
      Result := FLayerList[Cnt];
      Exit;
    end;
  Result := nil;
end;

function TLayerDialog.CreateNewGroup
  : TGroupEntry;
var
  Cnt: Integer;
begin
  with GroupListbox do
  begin
    Result := TGroupEntry.Create(TRUE);
    if Tree.Find(MlgSection[19]) <> nil then
    begin
      Cnt := 1;
      while Tree.Find(MlgSection[19] + ' ' + IntToStr(Cnt)) <> nil do
        Inc(Cnt);
      Result.Caption := MlgSection[19] + ' ' + IntToStr(Cnt);
    end
    else
      Result.Caption := MlgSection[19];
    with Result.LayerInfo do
    begin
      Visible := bsNo;
      UseForSnap := bsNo;
      Printable := bsNo;
      Editable := bsYes;
    end;
    Tree.Add(Result);
    TTreeEntry(Tree).SortByCaption(TRUE);
  end;
end;

procedure TLayerDialog.NewGroupMenuClick(Sender: TObject);
var
  Group: TGroupEntry;
begin
  with GroupListbox do
  begin
    BeginUpdate;
    Group := CreateNewGroup;
    EndUpdate;
    ItemIndex := Tree.ExpandedIndexOf(Group);
    UpdateEnabled;
  end;
end;

{*******************************************************************************
  Procedure TLayerDialog.WriteLayerGroupsToRegistry
--------------------------------------------------------------------------------
  Writes the layer-group definitions to the project-registry.
+******************************************************************************}

procedure TLayerDialog.WriteLayerGroupsToRegistry;
type
  PLongArray = ^TLongArray;
  TLongArray = array[0..0] of LongInt;
var
  GroupEntry: TGroupEntry;
  Cnt: Integer;
  Cnt1: Integer;
  LayerList: PLongArray;
begin
  if GroupListbox <> nil then
    with Project^.Registry do
    begin
      OpenKey('\Project\LayerGroups', TRUE);
      WriteInteger('Count', GroupListbox.Tree.Count);
      for Cnt := 0 to GroupListbox.Tree.Count - 1 do
      begin
        GroupEntry := TGroupEntry(GroupListbox.Tree[Cnt]);
        OpenKey(Format('\Project\LayerGroups\Group%d', [Cnt]), TRUE);
        WriteInteger('Count', GroupEntry.Count);
        GetMem(LayerList, GroupEntry.Count * SizeOf(LongInt));
        try
          WriteString('Name', GroupEntry.Caption);
          for Cnt1 := 0 to GroupEntry.Count - 1 do
            LayerList^[Cnt1] := TGroupEntry(GroupEntry[Cnt1]).LayerInfo.Number;
          WriteBinary('Layers', LayerList^, GroupEntry.Count * SizeOf(LongInt));
        finally
          FreeMem(LayerList, GroupEntry.Count * SizeOf(LongInt));
        end;
      end;
    end;
end;

{*******************************************************************************
  Procedure TLayerDialog.UpdateLayerGroups
--------------------------------------------------------------------------------
  Updates the layer-group list. Calculates the settings of all groups from
  the current settings of the owned layers
+******************************************************************************}

procedure TLayerDialog.UpdateLayerGroups;
var
  GroupEntry: TGroupEntry;
  LayerEntry: TGroupEntry;
  Cnt: Integer;
  Cnt1: Integer;
  Caption: string;
begin
  with Grouplistbox do
  begin
    for Cnt := 0 to Tree.Count - 1 do
    begin
      GroupEntry := TGroupEntry(Tree[Cnt]);
      if GroupEntry.Count = 0 then
        with GroupEntry.LayerInfo do
        begin
          PropertyMode := pmSet;
          Editable := bsYes;
          Printable := bsNo;
          UseForSnap := bsNo;
          Visible := bsNo;
          GeneralMin := GENERALIZE_MIN;
          GeneralMax := GENERALIZE_MAX;
        end
      else
      begin
        Caption := GroupEntry.Caption;
        GroupEntry.LayerInfo.PropertyMode := pmCollect;
        for Cnt1 := 0 to GroupEntry.Count - 1 do
        begin
          LayerEntry := TGroupEntry(GroupEntry[Cnt1]);
          GroupEntry.LayerInfo.Assign(LayerEntry.LayerInfo);
        end;
        GroupEntry.LayerInfo.PropertyMode := pmSet;
        GroupEntry.Caption := Caption;
      end;
    end;
  end;
end;

procedure TLayerDialog.AddLayerMenuClick(Sender: TObject);
var
  Cnt: Integer;
  Group: TGroupEntry;
  LayerEntry: TGroupEntry;
  LayerInfo: TLayerProperties;
  ARect: TRect;
begin
  with SelectionDialog do
  begin
      // get selected group (already a group selected otherwise take parent of item)
    Group := TGroupEntry(GroupListbox.Tree.ExpandedItems[GroupListbox.ItemIndex]);
    if not Group.IsGroup then
      Group := TGroupEntry(Group.Parent);
      // stop updates and clear list of the selection-dialog
    Items.BeginUpdate;
    Items.Clear;
      // add layers not already in the group to the selection-dialog
    for Cnt := 0 to FLayerList.Count - 1 do
      if Group.Find(TLayerProperties(FLayerList[Cnt]).Name) = nil then
        Items.Add(TLayerProperties(FLayerList[Cnt]).Name);
      // update the list and execute selection-dialog
    Items.EndUpdate;
    if Execute then
      if SelCount > 0 then
      begin
        GroupListbox.BeginUpdate;
        try
          for Cnt := 0 to Items.Count - 1 do
            if Selected[Cnt] then
            begin
              LayerInfo := LayerInfoFromName(Items[Cnt]);
              LayerEntry := TGroupEntry(Group.EntryFromLayerNumber(LayerInfo.Number));
              if (LayerEntry = nil) and (LayerInfo <> nil) then
              begin
                LayerEntry := TGroupEntry.Create(FALSE);
                LayerEntry.LayerInfo := LayerInfo;
                Group.Expanded := TRUE;
                Group.Add(LayerEntry);
              end;
            end;
          Group.SortByCaption(TRUE);
        finally
          GroupListbox.EndUpdate;
        end;
        GroupListbox.ItemIndex := GroupListbox.Tree.ExpandedIndexOf(LayerEntry);
        with GroupListbox do
        begin
          Cnt := Tree.ExpandedIndexOf(Group);
          ARect := Rect(0, (Cnt - TopIndex) * ItemHeight, ClientWidth, (Cnt - TopIndex + 1) * ItemHeight);
          InvalidateRect(GroupListbox.Handle, @ARect, TRUE);
        end;
        UpdateLayerGroups;
      end;
  end;
end;

procedure TLayerDialog.CreateGroupMenuClick(Sender: TObject);
var
  Cnt: Integer;
  Group: TGroupEntry;
  LayerEntry: TGroupEntry;
begin
  with GroupListbox do
  begin
    BeginUpdate;
    Group := CreateNewGroup;
    for Cnt := 0 to FLayerList.Count - 1 do
      if LayerListbox.Selected[Cnt] then
      begin
        LayerEntry := TGroupEntry.Create(FALSE);
        LayerEntry.LayerInfo := FLayerList[Cnt];
        Group.Add(LayerEntry);
      end;
    Group.SortByCaption(TRUE);
    EndUpdate;
    ItemIndex := Tree.ExpandedIndexOf(Group);
    Notebook.PageIndex := 2;
  end;
end;

procedure TLayerDialog.GroupListboxDrawCaption(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
  LayerEntry: TGroupEntry;
begin
  LayerEntry := TGroupEntry(GroupListbox.Tree.ExpandedItems[Index]);
  DrawLayerInfo(GroupListbox, Index, LayerEntry.LayerInfo, Rect, State, GroupListbox.DrawAction);
end;

procedure TLayerDialog.GroupInplaceEditorEditing(Sender: TObject;
  Index: Integer; var AllowEdit: Boolean; var Text: string);
begin
  Text := TGroupEntry(GroupListbox.Tree.ExpandedItems[Index]).Caption;
  with GroupListbox do
    GroupInplaceEditor.EditLeft := (Tree.ExpandedItems[ItemIndex].Level + 1) * ItemWidth;
  FClickField := fiNone;
end;

procedure TLayerDialog.GroupInplaceEditorEdited(Sender: TObject;
  Index: Integer; const Text: string);
var
  Cnt: Integer;
  GroupEntry: TGroupEntry;
  ARect: TRect;
begin
  with GroupListbox do
  begin
      { set new layer-name or group-name }
    GroupEntry := TGroupEntry(Tree.ExpandedItems[Index]);
    GroupEntry.Caption := Text;
      { invalidate layer-name-field for layer in other groups }
    for Cnt := 0 to Tree.ExpandedCount - 1 do
    begin
      if TGroupEntry(Tree.ExpandedItems[Cnt]).LayerInfo = GroupEntry.LayerInfo then
      begin
        ARect := FieldRect(GroupListbox, fiText, Cnt);
        InvalidateRect(Handle, @ARect, TRUE);
      end;
    end;
  end;
end;

procedure TLayerDialog.NotebookChange(Sender: TObject; NewTab: Integer;
  var AllowChange: Boolean);
begin
  AllowChange := TRUE;
  FLastSelected := -1;
  FClickField := fiNone;
  FLastIndex := fiNone;
  if (NewTab = 0) and (GeneraliseListbox <> nil) then
  begin; //Freddy demand N1  by Brovak
    LayerListbox.ItemIndex := GeneraliseListbox.ItemIndex;
    UpDateCheckedItems(false);
  end;
  if (NewTab = 1) and (GeneraliseListbox <> nil) then
  begin
    GeneraliseListbox.Count := FLayerList.Count;
    GeneraliseListbox.ItemIndex := LayerListbox.ItemIndex;
    UpDateCheckedItems(true); //Freddy demand N1  by Brovak
  end;
  if NewTab = 2 then
    UpdateLayerGroups;
end;

procedure TLayerDialog.DeleteLayerOrGroupMenuClick(Sender: TObject);
var
  Cnt: Integer;
  Group: TGroupEntry;
  FirstSelected: Integer;
  ToDelete: TList;
begin
  with GroupListbox do
  begin
      { create list of selected layers or groups }
    ToDelete := TList.Create;
    Cnt := Tree.ExpandedCount - 1;
    while Cnt >= 0 do
    begin
      if Selected[Cnt] then
      begin
        Group := TGroupEntry(Tree.ExpandedItems[Cnt]);
        if not Group.IsGroup then
          if Selected[Tree.ExpandedIndexOf(Group.Parent)] then
          begin
            Group := TGroupEntry(Group.Parent);
            Cnt := Tree.ExpandedIndexOf(Group);
          end;
        ToDelete.Add(Group);
        FirstSelected := Cnt;
      end;
      Dec(Cnt);
    end;
    BeginUpdate;
      { delete the selected groups from the tree and free memory }
    for Cnt := ToDelete.Count - 1 downto 0 do
    begin
      Group := ToDelete[Cnt];
      DeleteItem(Tree.ExpandedIndexOf(Group));
      Group.Free;
    end;
    ItemIndex := Min(FirstSelected, Tree.ExpandedCount - 1);
      { update layer-group-settings }
    UpdateLayerGroups;
    EndUpdate;
    ToDelete.Free;
    UpdateEnabled;
  end;
end;

procedure TLayerDialog.GroupListboxClick(Sender: TObject);
begin
  UpdateEnabled;
end;

procedure TLayerDialog.ChangeScale(M, D: Integer);
var
  Cnt: TFieldIndex;
  ARect: TRect;
begin
  inherited ChangeScale(M, D);
  for Cnt := Low(TFieldIndex) to High(TFieldIndex) do
  begin
    FFieldRects[Cnt].X := Trunc(FFieldRects[Cnt].X * M / D);
    FFieldRects[Cnt].Y := Trunc(FFieldRects[Cnt].Y * M / D);
  end;
  ARect := FieldRect(LayerListbox, fiText, 0);
  LayerInplaceEditor.EditLeft := ARect.Left;
  LayerInplaceEditor.EditRight := ARect.Right;
  GroupInplaceEditor.EditRight := ARect.Right;
end;

procedure TLayerDialog.MoveUpMenuClick(Sender: TObject);
begin
  MoveSelection(LayerListBox.ItemIndex - (1 + (LayerListBox.SelCount - 1)));
end;

procedure TLayerDialog.MoveDownMenuClick(Sender: TObject);
begin
  MoveSelection(LayerListBox.ItemIndex + 1);
end;

procedure TLayerDialog.PropertiesMenuClick(Sender: TObject);
{$IFNDEF AXDLL} // <----------------- AXDLL
var
  Dialog: TLayerPropertyDialog;
{$ENDIF}
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  Dialog := TLayerPropertyDialog.Create(Self);
  try
    Dialog.LayerStyle := FLayerList[LayerListbox.ItemIndex];
    Dialog.Project := FProject;
    Dialog.ProjectStyles := FProjectStyles;
    Dialog.LayerList := FLayerList;
{++ Ivanoff Bug#92 14.08.2000}

    if Dialog.ShowModal = mrOK then
      LayerListbox.Invalidate;
{-- Ivanoff}
  finally
    Dialog.Free;
  end;
{$ENDIF}  
end;

function TLayerDialog.FieldHint(Field: TFieldIndex): string;
begin
  if Field = fiNone then
    Result := ''
  else
    Result := MlgSection[39 + Integer(Field)];
end;

procedure TLayerDialog.TimerTimer(Sender: TObject);
begin
  if MoveUpBtn.MouseInControl then
  begin
    MoveUpMenuClick(Sender);
    if Timer.Interval > 50 then
      Timer.Interval := Timer.Interval - 50;
  end
  else
    if MoveDownBtn.MouseInControl then
    begin
      MoveDownMenuClick(Sender);
      if Timer.Interval > 50 then
        Timer.Interval := Timer.Interval - 50;
    end;
end;

procedure TLayerDialog.MoveBtnDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  TimerTimer(Sender);
  Timer.Interval := 350;
  Timer.Enabled := TRUE;
end;

procedure TLayerDialog.MoveBtnUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Timer.Enabled := FALSE;
end;

function TLayerDialog.CheckEditLayersForHSD(Layers: TList;
  var ReadOnly: Boolean): Boolean;
var
  Cnt: Integer;
  HSDLayer: Boolean;
  HSPLayer: Boolean;
begin
  if not FHSDLocked then
  begin
    ReadOnly := FALSE;
    Result := TRUE;
  end
  else
  begin
    HSDLayer := FALSE;
    HSPLayer := FALSE;
    for Cnt := 0 to Layers.Count - 1 do
      if TLayerProperties(Layers[Cnt]).HSDLayer then
        HSDLayer := TRUE
      else
        HSPLayer := TRUE;
    ReadOnly := not HSPLayer;
    if HSPLayer and HSDLayer and FHSDLocked then
      Result := MessageDialog(MlgSection[37],
        mtInformation, [mbOK, mbCancel], 0) = mrOK
    else
      Result := TRUE;
  end;
end;

procedure TLayerDialog.GeneraliseListboxColumns0Paint(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  CommonListBitmaps.Draw(GeneraliseListbox.Canvas, CenterWidthInRect(16, Rect), Rect.Top, clbLayerGray);
end;

procedure TLayerDialog.GeneraliseListboxColumns1Paint(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  with GeneraliseListbox do
  begin
    TextRectEx(Canvas, Rect, TLayerProperties(FLayerList[Index]).Name, Color, State);
    Image2.Canvas.TextOut(Rect.Left + 4, 1, Columns[1].Hint);
  end;
end;

procedure TLayerDialog.SearchBtn1Click(Sender: TObject);
begin
  DoSearchBtnClick(SearchEdit1, GeneraliseListbox);
end;

procedure TLayerDialog.DoSearchBtnClick(SearchEdit: TEdit; Listbox: TWCustomListBox);
var
  Cnt: Integer;
  LayerInfo: TLayerProperties;
begin
  Cnt := (Listbox.ItemIndex + 1) mod FLayerList.Count;
  while Cnt <> Listbox.ItemIndex do
  begin
    LayerInfo := FLayerList[Cnt];
    if CalculateEqualLen(SearchEdit.Text, LayerInfo.Name) = Length(SearchEdit.Text) then
    begin
      Listbox.ItemIndex := Cnt;
      Break;
    end;
    Cnt := (Cnt + 1) mod FLayerList.Count;
  end;
end;

procedure TLayerDialog.SearchEdit1Change(Sender: TObject);
begin
  DoSearchEditChange(SearchEdit1, GeneraliseListbox, SearchBtn1);
end;

function TLayerDialog.GeneralizeText(AValue: Double): string;
begin
  if (Abs(AValue) <= 1E-200) or (Abs(AValue) > 1E200) then
    Result := ''
  else
  begin
    if AValue < 1 then
      Result := FloatToStrF(1.0 / AValue, ffNumber, 30, 0)
    else
      Result := FloatToStrF(AValue, ffNumber, 30, 0);
    if AValue < 1 then
      Result := '1:' + Result
    else
      Result := Result + ':1';
  end;
end;

procedure TLayerDialog.GeneraliseListboxColumns2Paint(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
  LayerInfo: TLayerProperties;
  Text: string;
begin
  LayerInfo := TLayerProperties(FLayerList[Index]);
  {+++brovak bug#39 BUILD 127}
  if (LayerInfo.Generalize = gtBiggerScale) or (LayerInfo.Generalize = gtScaleRange) then
  {--brovak bug#39 BUILD 127}
    with GeneraliseListbox do
    begin
      Text := GeneralizeText(LayerInfo.GeneralMin);
      if Text <> '' then
        TextRectEx(Canvas, Rect, Text, Color, State);
    end;

  Image2.Canvas.Pen.Color := clGray;
  Image2.Canvas.MoveTo(Rect.Left, 0);
  Image2.Canvas.LineTo(Rect.Left, Image2.Height);
  Image2.Canvas.TextOut(Rect.Left + 4, 1, GeneraliseListbox.Columns[2].Hint);
end;

procedure TLayerDialog.GeneraliseListboxColumns3Paint(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
  LayerInfo: TLayerProperties;
  Text: string;
begin
  LayerInfo := TLayerProperties(FLayerList[Index]);
  {+++brovak bug#39 BUILD 127}
  if (LayerInfo.Generalize = gtSmallerScale) or (LayerInfo.Generalize = gtScaleRange) then
  {--brovak}
    with GeneraliseListbox do
    begin
      Text := GeneralizeText(LayerInfo.GeneralMax);
      if Text <> '' then
        TextRectEx(Canvas, Rect, Text, Color, State);
    end;
  Image2.Canvas.Pen.Color := clGray;
  Image2.Canvas.MoveTo(Rect.Left, 0);
  Image2.Canvas.LineTo(Rect.Left, Image2.Height);
  Image2.Canvas.TextOut(Rect.Left + 4, 1, GeneraliseListbox.Columns[3].Hint);
end;

procedure TLayerDialog.GeneraliseListboxDblClick(Sender: TObject);
var
  ToEdit: TList;
  Cnt: Integer;
begin
  ToEdit := TList.Create;
  try
    with GeneraliseListbox do
    begin
      for Cnt := 0 to Items.Count - 1 do
        if Selected[Cnt] then
          ToEdit.Add(FLayerList[Cnt]);
      if EditGeneralize(ToEdit) then
        for Cnt := 0 to Items.Count - 1 do
          if Selected[Cnt] then
            InvalidateItems(Cnt, Cnt);
    end;
  finally
    ToEdit.Free;
  end;
end;

procedure TLayerDialog.GeneraliseListboxMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  Index: Integer;
  ClickColumn: TColumn;
begin
  if Button = mbLeft then
    with GeneraliseListbox do
    begin
      FMouseDownPos := Point(X, Y);
      ClickColumn := ColumnAtPosition(X, Y, Index, TRUE);
      FLastSelected := -1;
      if Index >= 0 then
      begin
        if (Shift * [ssShift, ssCtrl] <> []) then
          FLastSelected := Index
        else
        begin
          DoInherited := SelCount <= 1;
          if not Selected[Index] then
          begin
            FLastSelected := Index;
            DoInherited := DoInherited or (ClickColumn.Index > 0);
          end
          else
            if ClickColumn.Index > 0 then
              FocusedIndex := Index;
        end;
      end;
    end;
end;

procedure TLayerDialog.GroupListboxMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var
  Index: TFieldIndex;
begin
  with GroupListbox do
  begin
    Index := FieldAtPos(GroupListbox, Point(X, Y));
    if Index <> FLastIndex then
    begin
      Application.CancelHint;
      FLastIndex := Index;
      GroupListbox.Hint := FieldHint(Index);
    end;
  end;
end;

//Freddy demand N1  by Brovak
{if Flag =true, then selected items in LayerListBox are marked
in GeneraliseListBox ; else selected items in GeneraliseListBox
are marked in LayerListBox}

procedure TLayerDialog.UpDateCheckedItems(flag: Boolean);
var
  i: integer;
begin
  if Flag = true then
    for i := 0 to LayerListBox.Items.Count - 1 do
      GeneraliseListBox.Selected[i] := LayerListBox.Selected[i]
  else
  begin;
    for i := 0 to GeneraliseListBox.Items.Count - 1 do
      LayerListBox.Selected[i] := GeneraliseListBox.Selected[i];
    UpdateEnabled;
  end;
end;
{---- Brovak}

{++Brovak for password}

procedure TLayerDialog.FixLayerButtonClick(Sender: TObject);
begin
  ChangePassword(Self);
end;
{--Brovak}

end.

