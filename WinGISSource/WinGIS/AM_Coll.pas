{******************************************************************************+
  Unit AM_Coll
--------------------------------------------------------------------------------
  Contains different Collection-objects.
+******************************************************************************}
unit AM_Coll;

interface

uses AM_Index, AM_Def, Objects, SysUtils;

const
  MaxBlockSize = 16380;

type
  PPIndexColl = ^PIndexColl;
  PIndexColl = ^TIndexColl;
  TIndexColl = object(TSortedCollection)
    function Compare(Key1, Key2: Pointer): Integer; virtual;
  end;

  PViewColl = ^TViewColl;
  TViewColl = object(TOldObject)
    Data: PCollection;
    constructor Init;
    constructor Load(S: TOldStream);
    function At(Index: LongInt): PIndex;
    procedure AtPut(Index: LongInt; Item: PIndex);
    procedure Insert(Item: Pointer); virtual;
    procedure AtInsert(Index: LongInt; Item: Pointer);
    procedure Delete(Item: Pointer);
    procedure DeleteAll;
    procedure AtDelete(Index: LongInt);
    procedure Free(Item: Pointer);
    procedure AtFree(Index: LongInt);
    procedure FreeAll;
    function GetCount: LongInt;
    function Search(Key: Pointer; var Index: LongInt): Boolean;
    procedure ForEach(Action: Pointer); virtual;
    function FirstThat(Action: Pointer): PIndex;
    function LastThat(Action: Pointer): PIndex;
    procedure Store(S: TOldStream); virtual;
    destructor Done; virtual;
    function NewSubCollection: PSortedCollection; virtual;
    procedure Sort;
    procedure QuickSort(L, R: LongInt);
    procedure RemoveDuplicates;
  end;

  PUnsortedColl = ^TUnsortedColl;
  TUnsortedColl = object(TOldObject)
    Data: PCollection;
    constructor Init;
    constructor Load(S: TOldStream);
    destructor Done; virtual;
    function At(Index: LongInt): PIndex;
    procedure AtDelete(Index: LongInt);
    procedure AtFree(Index: LongInt);
    procedure AtInsert(Index: LongInt; Item: Pointer);
    procedure Delete(Item: Pointer);
    procedure DeleteAll;
    function FirstThat(Action: Pointer): Pointer;
    procedure ForEach(Action: Pointer); virtual;
    procedure Free(Item: Pointer);
    procedure FreeAll;
    function GetCount: LongInt;
    procedure Insert(Item: Pointer); virtual;
    function LastThat(Action: Pointer): Pointer;
    function NewSubCollection: PCollection; virtual;
    function Search(Key: Pointer; var Index: LongInt): Boolean;
    procedure Store(S: TOldStream); virtual;
  end;

  PBigUStrColl = ^TBigUStrColl;
  TBigUStrColl = object(TUnsortedColl)
    constructor Init;
    destructor Done; virtual;
    function At(Index: LongInt): PChar;
    procedure Delete(Item: Pointer);
    function Search(Key: Pointer; var Index: LongInt): Boolean;
  end;

  PLongColl = ^TLongColl;
  TLongColl = object(TCollection)
    procedure FreeItem(Item: Pointer); virtual;
    function GetItem(S: TOldStream): Pointer; virtual;
    procedure PutItem(S: TOldStream; Item: Pointer); virtual;
  end;

  PWArray = ^TWArray;
  TWArray = object(TOldObject)
    Count: Integer;
    Entries: PAWord;
    constructor Init;
    constructor Load(S: TOldStream);
    destructor Done; virtual;
    function At(Index: Integer): Word;
    function AtDelete(Index: Integer): Word;
    procedure AtInsert(Index: Integer; Item: Word);
    procedure DeleteAll;
    procedure DeleteAndRenumber(AIndex: Integer);
    procedure Insert(Item: Word);
    procedure InsertAndRenumber(AIndex: Integer);
    function Search(Item: Word; var Index: Integer): Boolean;
    procedure Store(S: TOldStream);
  end;

implementation

uses WinProcs;

type
  TItemProc = procedure(Item: Pointer; BP: Word);
  TItemFunc = function(Item: Pointer; BP: Word): Boolean;

function TIndexColl.Compare
  (
  Key1: Pointer;
  Key2: Pointer
  )
  : Integer;
begin
  if (Key1 <> nil) and (Key2 <> nil) then
  begin
    if PIndex(Key1)^.Index < PIndex(Key2)^.Index then
      Compare := -1
    else
      if PIndex(Key1)^.Index > PIndex(Key2)^.Index then
        Compare := 1
      else
        Compare := 0;
  end;
end;

constructor TViewColl.Init;
var
  NewColl: PCollection;
begin
  inherited Init;
  Data := New(PCollection, Init(128, 128));
  NewColl := NewSubCollection;
  Data^.Insert(NewColl);
end;

constructor TViewColl.Load(S: TOldStream);
begin
  Data := Pointer(S.Get);
end;

procedure TViewColl.RemoveDuplicates;
var
  Cnt: LongInt;
begin
  for Cnt := GetCount - 1 downto 1 do
    if PIndex(At(Cnt))^.Index = PIndex(At(Cnt - 1))^.Index then
      AtFree(Cnt);
end;

procedure TViewColl.QuickSort
  (
  L: LongInt;
  R: LongInt
  );
var
  I: Integer;
  J: Integer;
  P: Pointer;
  Temp: Pointer;
  FirstColl: PIndexColl;
begin
  repeat
    I := L;
    J := R;
    P := At((L + R) shr 1);
    FirstColl := Data^.At(0);
    repeat
      while FirstColl^.Compare(At(I), P) < 0 do
        Inc(I);
      while FirstColl^.Compare(At(J), P) > 0 do
        Dec(J);
      if I <= J then
      begin
        Temp := At(I);
        AtPut(I, At(J));
        AtPut(J, Temp);
        Inc(I);
        Dec(J);
      end;
    until I > J;
    if L < J then
      QuickSort(L, J);
    L := I;
  until I >= R;
end;

procedure TViewColl.Sort;
begin
  if GetCount > 1 then
    QuickSort(0, GetCount - 1);
end;

function TViewColl.At
  (
  Index: LongInt
  ): PIndex;
var
  Coll: PIndexColl;
begin
  Coll := Data^.At(Index div MaxBlockSize);
  At := Coll^.At((Index mod MaxBlockSize));
end;

procedure TViewColl.AtPut
  (
  Index: LongInt;
  Item: PIndex
  );
var
  Coll: PIndexColl;
begin
  Coll := Data^.At(Index div MaxBlockSize);
  Coll^.AtPut((Index mod MaxBlockSize), Item);
end;

procedure TViewColl.Insert
  (
  Item: Pointer
  );
var
  Coll: PCollection;
  Count: Integer;

  function DoSome
      (
      Item2: PIndexColl
      )
      : Boolean; far;
  begin
    Item2^.Search(Item, Count);
    DoSome := Count <> MaxBlockSize;
  end;
begin
  Coll := Data^.FirstThat(@DoSome);
  if Coll = nil then
  begin
    Coll := NewSubCollection;
    Data^.Insert(Coll);
    Coll^.AtInsert(0, Item);
  end
  else
    AtInsert(Data^.IndexOf(Coll) * LongInt(MaxBlockSize) + LongInt(Count), Item);
end;

procedure TViewColl.AtInsert
  (
  Index: LongInt;
  Item: Pointer
  );
var
  Coll: PIndexColl;
  AktColl: Integer;

  function DoSome
      (
      Item: PIndexColl
      )
      : Boolean; far;
  begin
    if AktColl > Index div MaxBlockSize then
    begin
      Item^.AtInsert(0, PIndexColl(Data^.At(AktColl - 1))^.At(MaxBlockSize - 1));
      PIndexColl(Data^.At(AktColl - 1))^.AtDelete(MaxBlockSize - 1);
      Dec(AktColl);
      DoSome := FALSE;
    end
    else
      DoSome := TRUE;
  end;
begin
  if PIndexColl(Data^.At(Data^.Count - 1))^.Count >= MaxBlockSize then
    Data^.Insert(NewSubCollection);
  AktColl := Data^.Count - 1;
  Data^.LastThat(@DoSome);
  Coll := Data^.At(Index div MaxBlockSize);
  Coll^.AtInsert(Index mod MaxBlockSize, Item);
end;

procedure TViewColl.Delete(Item: Pointer);
var
  Coll: PIndexColl;
  Count: Integer;

  function DoSome(Item2: PIndexColl): Boolean; far;
  begin
    Item2^.Search(Item, Count);
    DoSome := Count <> MaxBlockSize;
  end;
begin
  Coll := Data^.FirstThat(@DoSome);
  AtDelete(Data^.IndexOf(Coll) * LongInt(MaxBlockSize) + LongInt(Count));
end;

procedure TViewColl.AtDelete(Index: LongInt);
var
  Coll: PIndexColl;
  AktColl: Integer;
begin
  Coll := Data^.At(Index div MaxBlockSize);
  Coll^.AtDelete(Index mod MaxBlockSize);
  AktColl := Index div MaxBlockSize;
  while AktColl < Data^.Count - 1 do
  begin
    PIndexColl(Data^.At(AktColl))^.AtInsert(MaxBlockSize - 1, PIndexColl(Data^.At(AktColl + 1))^.At(0));
    PIndexColl(Data^.At(AktColl + 1))^.AtDelete(0);
    Inc(AktColl);
  end;
  if (Data^.Count > 1) and (PIndexColl(Data^.At(Data^.Count - 1))^.Count = 0) then
    Data^.AtDelete(Data^.Count - 1);
end;

procedure TViewColl.Free(Item: Pointer);
begin
  Delete(Item);
  Dispose(POldObject(Item), Done);
end;

procedure TViewColl.AtFree(Index: LongInt);
begin
  Dispose(POldObject(At(Index)), Done);
  AtDelete(Index);
end;

procedure TViewColl.FreeAll;
begin
  Data^.FreeAll;
  Data^.Insert(NewSubCollection);
end;

procedure TViewColl.DeleteAll;

  procedure DoAll
      (
      Item: PCollection
      ); far;
  begin
    Item^.DeleteAll;
  end;
begin
  Data^.ForEach(@DoAll);
  FreeAll;
end;

function TViewColl.Search
  (
  Key: Pointer;
  var Index: LongInt
  )
  : Boolean;

  function DoSome
      (
      Item: PIndexColl
      )
      : Boolean; far;
  var
    Count: Integer;
  begin
    if Item <> nil then
    begin
      DoSome := Item^.Search(Key, Count);
      Inc(Index, Count);
    end;
  end;

begin
  Index := 0;
  Search := Data^.FirstThat(@DoSome) <> nil;
end;

procedure TViewColl.ForEach
  (
  Action: Pointer
  );
var
  Cnt: Integer;
  Cnt1: Integer;
  Coll: PCollection;
  Item: Pointer;
begin
  for Cnt := 0 to Data^.Count - 1 do
  begin
    Coll := Data^.At(Cnt);
    if Coll = nil then
      continue;
    for Cnt1 := 0 to Coll^.Count - 1 do
    begin
      Item := Coll^.At(Cnt1);
      asm
          MOV EAX,Item
          PUSH DWORD PTR [EBP]
          CALL DWORD PTR Action
          ADD ESP,4
      end;
    end;
  end;
end;

function TViewColl.FirstThat
  (
  Action: Pointer
  )
  : PIndex;
var
  Cnt: Integer;
  Cnt1: Integer;
  Coll: PCollection;
  Item: Pointer;
begin
  Result := nil;
  for Cnt := 0 to Data^.Count - 1 do
  begin
    Coll := Data^.At(Cnt);
    for Cnt1 := 0 to Coll^.Count - 1 do
    begin
      Item := Coll^.At(Cnt1);
      asm
          MOV EAX,Item
          PUSH DWORD PTR [EBP]
          CALL DWORD PTR Action
          ADD ESP,4
          CMP AL,0
          JE @@1
          MOV EAX,Item
          MOV @Result,EAX
        @@1:
      end;
      if Result <> nil then
        Exit;
    end;
  end;
end;

function TViewColl.LastThat
  (
  Action: Pointer
  )
  : PIndex;
var
  Cnt: Integer;
  Cnt1: Integer;
  Coll: PCollection;
  Item: Pointer;
begin
  Result := nil;
  for Cnt := Data^.Count - 1 downto 0 do
  begin
    Coll := Data^.At(Cnt);
    for Cnt1 := Coll^.Count - 1 downto 0 do
    begin
      Item := Coll^.At(Cnt1);
      asm
          MOV EAX,Item
          PUSH DWORD PTR [EBP]
          CALL DWORD PTR Action
          ADD ESP,4
          CMP AL,0
          JE @@1
          MOV EAX,Item
          MOV @Result,EAX
        @@1:
      end;
      if Result <> nil then
        Exit;
    end;
  end;
end;

procedure TViewColl.Store
  (
  S: TOldStream
  );

begin
  S.Put(Data);
end;

destructor TViewColl.Done;

begin
  Dispose(Data, Done);
  inherited Done;
end;

function TViewColl.GetCount
  : LongInt;
begin
  if Data^.At(Data^.Count - 1) <> nil then
    GetCount := LongInt(Data^.Count - 1) * MaxBlockSize + PCollection(Data^.At(Data^.Count - 1))^.Count
  else
    GetCount := 0;
end;

constructor TUnsortedColl.Init;
var
  NewColl: PCollection;
begin
  inherited Init;
  Data := New(PCollection, Init(128, 128));
  NewColl := NewSubCollection;
  Data^.Insert(NewColl);
end;

destructor TUnsortedColl.Done;
begin
  Dispose(Data, Done);
  inherited Done;
end;

constructor TUnsortedColl.Load(S: TOldStream);
begin
  Data := Pointer(S.Get);
end;

procedure TUnsortedColl.Store
  (
  S: TOldStream
  );
begin
  S.Put(Data);
end;

procedure TUnsortedColl.Insert
  (
  Item: Pointer
  );
var
  Coll: PCollection;
begin
  if PCollection(Data^.At(Data^.Count - 1))^.Count >= MaxBlockSize then
    Data^.Insert(NewSubCollection);
  Coll := Data^.At(Data^.Count - 1);
  Coll^.Insert(Item);
end;

procedure TUnsortedColl.AtInsert
  (
  Index: LongInt;
  Item: Pointer
  );
var
  Coll: PCollection;
  AktColl: Integer;

  function DoSome
      (
      Item: PCollection
      ): Boolean; far;
  begin
    if AktColl > Index div MaxBlockSize then
    begin
      Item^.AtInsert(0, PCollection(Data^.At(AktColl - 1))^.At(MaxBlockSize - 1));
      PCollection(Data^.At(AktColl - 1))^.AtDelete(MaxBlockSize - 1);
      Dec(AktColl);
      DoSome := FALSE;
    end
    else
      DoSome := TRUE;
  end;
begin
  if PCollection(Data^.At(Data^.Count - 1))^.Count >= MaxBlockSize then
    Data^.Insert(NewSubCollection);
  AktColl := Data^.Count - 1;
  Data^.LastThat(@DoSome);
  Coll := Data^.At(Index div MaxBlockSize);
  Coll^.AtInsert(Index mod MaxBlockSize, Item);
end;

function TUnsortedColl.At
  (
  Index: LongInt
  ): PIndex;
var
  Coll: PCollection;
begin
  Coll := Data^.At(Index div MaxBlockSize);
  At := Coll^.At((Index mod MaxBlockSize));
end;

procedure TUnsortedColl.Delete
  (
  Item: Pointer
  );
var
  Coll: PCollection;
  IndexCount: Integer;

  function DoSome
      (
      Item2: PCollection
      ): Boolean; far;

    function FindIndIndex
        (
        Item3: PIndex
        ): Boolean; far;
    begin
      Inc(IndexCount);
      FindIndIndex := (Item3^.Index = PIndex(Item)^.Index);
    end;
  var
    FoundItem: PIndex;
  begin
    IndexCount := -1;
    FoundItem := Item2^.FirstThat(@FindIndIndex);
    DoSome := FoundItem <> nil;
  end;
begin
  Coll := Data^.FirstThat(@DoSome);
  if Coll <> nil then
    AtDelete(Data^.IndexOf(Coll) * LongInt(MaxBlockSize) + LongInt(IndexCount));
end;

procedure TUnsortedColl.AtDelete
  (
  Index: LongInt
  );

var
  Coll: PCollection;
  AktColl: Integer;
begin
  Coll := Data^.At(Index div MaxBlockSize);
  Coll^.AtDelete(Index mod MaxBlockSize);
  AktColl := Index div MaxBlockSize;
  while AktColl < Data^.Count - 1 do
  begin
    PCollection(Data^.At(AktColl))^.AtInsert(MaxBlockSize - 1, PCollection(Data^.At(AktColl + 1))^.At(0));
    PCollection(Data^.At(AktColl + 1))^.AtDelete(0);
    Inc(AktColl);
  end;
  if (Data^.Count > 1) and (PCollection(Data^.At(Data^.Count - 1))^.Count = 0) then
    Data^.AtDelete(Data^.Count - 1);
end;

procedure TUnsortedColl.DeleteAll;

  procedure DoAll
      (
      Item: PCollection
      ); far;
  begin
    Item^.DeleteAll;
  end;
begin
  Data^.ForEach(@DoAll);
  FreeAll;
end;

procedure TUnsortedColl.Free
  (
  Item: Pointer
  );
begin
  Delete(Item);
  Dispose(POldObject(Item), Done);
end;

procedure TUnsortedColl.AtFree
  (
  Index: LongInt
  );
begin
  Dispose(POldObject(At(Index)), Done);
  AtDelete(Index);
end;

procedure TUnsortedColl.FreeAll;
begin
  Data^.FreeAll;
  Data^.Insert(NewSubCollection);
end;

function TUnsortedColl.Search
  (
  Key: Pointer;
  var Index: LongInt
  ): Boolean;
var
  Coll: PCollection;

  function DoSome
      (
      Item2: PCollection
      ): Boolean; far;
  var
    FoundItem: PIndex;
    IndexCount: Integer;

    function FindIndIndex
        (
        Item3: PIndex
        ): Boolean; far;
    begin
      Inc(IndexCount);
      FindIndIndex := (Item3^.Index = PIndex(Key)^.Index);
    end;
  begin
    IndexCount := -1;
    FoundItem := Item2^.FirstThat(@FindIndIndex);
    if FoundItem = nil then
      Inc(Index, MaxBlockSize)
    else
      Inc(Index, IndexCount);
    DoSome := FoundItem <> nil;
  end;
begin
  Index := 0;
  Coll := Data^.FirstThat(@DoSome);
  if Coll = nil then
    Search := FALSE
  else
    Search := TRUE;
end;

procedure TUnsortedColl.ForEach
  (
  Action: Pointer
  );
var
  Cnt: Integer;
  Cnt1: Integer;
  Coll: PCollection;
  Item: Pointer;
begin
  for Cnt := 0 to Data^.Count - 1 do
  begin
    Coll := Data^.At(Cnt);
    for Cnt1 := 0 to Coll^.Count - 1 do
    begin
      Item := Coll^.At(Cnt1);
      asm
          MOV EAX,Item
          PUSH DWORD PTR [EBP]
          CALL DWORD PTR Action
          ADD ESP,4
      end;
    end;
  end;
end;

function TUnsortedColl.FirstThat
  (
  Action: Pointer
  )
  : Pointer;
var
  Cnt: Integer;
  Cnt1: Integer;
  Coll: PCollection;
  Item: Pointer;
begin
  Result := nil;
  for Cnt := 0 to Data^.Count - 1 do
  begin
    Coll := Data^.At(Cnt);
    for Cnt1 := 0 to Coll^.Count - 1 do
    begin
      Item := Coll^.At(Cnt1);
      asm
          MOV EAX,Item
          PUSH DWORD PTR [EBP]
          CALL DWORD PTR Action
          ADD ESP,4
          CMP AL,0
          JE @@1
          MOV EAX,Item
          MOV @Result,EAX
        @@1:
      end;
      if Result <> nil then
        Exit;
    end;
  end;
end;

function TUnsortedColl.LastThat
  (
  Action: Pointer
  )
  : Pointer;
var
  Cnt: Integer;
  Cnt1: Integer;
  Coll: PCollection;
  Item: Pointer;
begin
  Result := nil;
  for Cnt := Data^.Count - 1 downto 0 do
  begin
    Coll := Data^.At(Cnt);
    for Cnt1 := Coll^.Count - 1 downto 0 do
    begin
      Item := Coll^.At(Cnt1);
      asm
          MOV EAX,Item
          PUSH DWORD PTR [EBP]
          CALL DWORD PTR Action
          ADD ESP,4
          CMP AL,0
          JE @@1
          MOV EAX,Item
          MOV @Result,EAX
        @@1:
      end;
      if Result <> nil then
        Exit;
    end;
  end;
end;

function TUnsortedColl.GetCount
  : LongInt;
begin
  Result := LongInt(Data^.Count - 1) * MaxBlockSize + PCollection(Data^.At(Data^.Count - 1))^.Count;
end;

function TUnsortedColl.NewSubCollection
  : PCollection;
begin
  NewSubCollection := New(PCollection, Init(4096, 1024));
end;

constructor TBigUStrColl.Init;
begin
  inherited Init;
end;

function TBigUStrColl.At
  (
  Index: LongInt
  ): PChar;
var
  Coll: PCollection;
begin
  Coll := Data^.At(Index div MaxBlockSize);
  At := Coll^.At((Index mod MaxBlockSize));
end;

destructor TBigUStrColl.Done;
begin
  inherited Done;
end;

procedure TBigUStrColl.Delete
  (
  Item: Pointer
  );
var
  Coll: PCollection;
  IndexCount: Integer;

  function DoSome
      (
      Item2: PCollection
      ): Boolean; far;

    function FindIndIndex
        (
        Item3: PChar
        ): Boolean; far;
    begin
      Inc(IndexCount);
      FindIndIndex := StrComp(Item3, Item) = 0;
    end;
  var
    FoundItem: PIndex;
  begin
    IndexCount := -1;
    FoundItem := Item2^.FirstThat(@FindIndIndex);
    DoSome := FoundItem <> nil;
  end;
begin
  Coll := Data^.FirstThat(@DoSome);
  if Coll <> nil then
    AtDelete(Data^.IndexOf(Coll) * LongInt(MaxBlockSize) + LongInt(IndexCount));
end;

function TBigUStrColl.Search
  (
  Key: Pointer;
  var Index: LongInt
  ): Boolean;
var
  Coll: PCollection;

  function DoSome
      (
      Item2: PCollection
      ): Boolean; far;
  var
    FoundItem: PIndex;
    IndexCount: Integer;

    function FindIndIndex
        (
        Item3: PChar
        ): Boolean; far;
    begin
      Inc(IndexCount);
      FindIndIndex := StrComp(Item3, PChar(Key)) = 0;
    end;
  begin
    IndexCount := -1;
    FoundItem := Item2^.FirstThat(@FindIndIndex);
    if FoundItem = nil then
      Inc(Index, MaxBlockSize)
    else
      Inc(Index, IndexCount);
    DoSome := FoundItem <> nil;
  end;
begin
  Index := 0;
  Coll := Data^.FirstThat(@DoSome);
  if Coll = nil then
    Search := FALSE
  else
    Search := TRUE;
end;

procedure TLongColl.FreeItem
  (
  Item: Pointer
  );
begin
end;

function TLongColl.GetItem
  (
  S: TOldStream
  )
  : Pointer;
begin
  S.Read(Result, SizeOf(Result));
end;

procedure TLongColl.PutItem(S: TOldStream; Item: Pointer);
begin
  S.Write(Item, SizeOf(Item));
end;

constructor TWArray.Init;
begin
  inherited Init;
  Entries := nil;
  Count := 0;
end;

destructor TWArray.Done;
begin
  if Count > 0 then
    FreeMem(Entries, Count * SizeOf(Word));
  inherited Done;
end;

constructor TWArray.Load(S: TOldStream);
var
  Temp: Word;
begin
  inherited Init;
  S.Read(Temp, SizeOf(Temp));
  if Temp = $FFFF then
    S.Read(Count, SizeOf(Count))
  else
    Count := Temp;
  if Count = 0 then
    Entries := nil
  else
  begin
    GetMem(Entries, Count * SizeOf(Word));
    S.Read(Entries^, Count * SizeOf(Word));
  end;
end;

procedure TWArray.Store
  (
  S: TOldStream
  );
var
  Temp: Word;
begin
  Temp := $FFFF;
  S.Write(Temp, SizeOf(Temp));
  S.Write(Count, SizeOf(Count));
  S.Write(Entries^, Count * SizeOf(Word));
end;

function TWArray.At
  (
  Index: Integer
  )
  : Word;
begin
  if Index < Count then
    At := Entries^[Index];
end;

function TWArray.Search
  (
  Item: Word;
  var Index: Integer
  )
  : Boolean;
begin
  Index := 0;
  while (Index < Count) and (Entries^[Index] <> Item) do
    Inc(Index);
  Search := Index < Count;
end;

procedure TWArray.AtInsert
  (
  Index: Integer;
  Item: Word
  );
var
  HelpPtr: PAWord;
begin
  if Index > Count then
    Index := Count;
  GetMem(HelpPtr, (Count + 1) * SizeOf(Word));
  if Count > 0 then
  begin
    Move(Entries^, HelpPtr^, Index * SizeOf(Word));
    Move(Entries^[Index], HelpPtr^[Index + 1], (Count - Index) * SizeOf(Word));
    FreeMem(Entries, Count * SizeOf(Word));
  end;
  HelpPtr^[Index] := Item;
  Entries := HelpPtr;
  Inc(Count);
end;

procedure TWArray.Insert
  (
  Item: Word
  );
begin
  AtInsert(Count, Item);
end;

function TWArray.AtDelete
  (
  Index: Integer
  )
  : Word;
var
  HelpPtr: PAWord;
begin
  if Index < Count then
  begin
    AtDelete := Entries^[Index];
    GetMem(HelpPtr, (Count - 1) * SizeOf(Word));
    Move(Entries^, HelpPtr^, Index * SizeOf(Word));
    Move(Entries^[Index + 1], HelpPtr^[Index], (Count - Index - 1) * SizeOf(Word));
    FreeMem(Entries, Count * SizeOf(Word));
    Entries := HelpPtr;
    Dec(Count);
  end;
end;

procedure TWArray.DeleteAll;
begin
  if Count > 0 then
  begin
    FreeMem(Entries, Count * SizeOf(Word));
    Entries := nil;
    Count := 0;
  end;
end;

procedure TWArray.DeleteAndRenumber
  (
  AIndex: Integer
  );
var
  Cnt: Integer;
  Delete: Integer;
begin
  Delete := -1;
  for Cnt := 0 to Count - 1 do
  begin
    if Entries^[Cnt] = AIndex then
      Delete := Cnt
    else
      if Entries^[Cnt] > AIndex then
        Dec(Entries^[Cnt]);
  end;
  if Delete <> -1 then
    AtDelete(Delete);
end;

procedure TWArray.InsertAndRenumber
  (
  AIndex: Integer
  );
var
  Cnt: Integer;
begin
  for Cnt := 0 to Count - 1 do
  begin
    if Entries^[Cnt] >= AIndex then
      Inc(Entries^[Cnt]);
  end;
end;

function TViewColl.NewSubCollection
  : PSortedCollection;
begin
  NewSubCollection := New(PIndexColl, Init(4096, 1024));
end;

const
  RIndexColl: TStreamRec = (
    ObjType: rn_IndexColl;
    VmtLink: TypeOf(TIndexColl);
    Load: @TIndexColl.Load;
    Store: @TIndexColl.Store);

  RViewColl: TStreamRec = (
    ObjType: rn_ViewColl;
    VmtLink: TypeOf(TViewColl);
    Load: @TViewColl.Load;
    Store: @TViewColl.Store);

  RUnsortedColl: TStreamRec = (
    ObjType: rn_UnsortedColl;
    VmtLink: TypeOf(TUnsortedColl);
    Load: @TUnsortedColl.Load;
    Store: @TUnsortedColl.Store);

  RLongColl: TStreamRec = (
    ObjType: rn_LongColl;
    VmtLink: TypeOf(TLongColl);
    Load: @TLongColl.Load;
    Store: @TLongColl.Store);

  RWArray: TStreamRec = (
    ObjType: rn_WArray;
    VmtLink: TypeOf(TWArray);
    Load: @TWArray.Load;
    Store: @TWArray.Store);

begin
  RegisterType(RIndexColl);
  RegisterType(RViewColl);
  RegisterType(RUnsortedColl);
  RegisterType(RCollection);
  RegisterType(RLongColl);
  RegisterType(RWArray);
end.

