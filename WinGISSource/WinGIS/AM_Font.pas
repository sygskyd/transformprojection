{******************************************************************************+
  Unit
--------------------------------------------------------------------------------
  Modifikationen
  16.02.1994 Martin Forst
+******************************************************************************}
unit AM_Font;

interface

uses Win32Def, WinTypes, WinProcs, SysUtils, Objects;

//++ Glukhov Bug#114 Build#125 14.11.00
const
  cMinFontHeight = 4; // Minimal height of font
//-- Glukhov Bug#114 Build#125 14.11.00

type
  PFontData = ^TFontData;
  TFontData = object(TOldObject)
    Font: Integer16;
    Style: Integer16;
    Height: LongInt;
    constructor Init;
    constructor Load(S: TOldStream);
    function IsEqual(var AFont: TFontData): Boolean;
    procedure Store(S: TOldStream);
  end;

  PFontDes = ^TFontDes;
  TFontDes = object(TOldObject)
    FNum: Integer16;
      Name: PString;
    CharSet: Byte;
    Family: Byte;
    UseCount: LongInt;
    Installed: Boolean;
    FontType: Integer;
    IsTrueType: Boolean;
    constructor Init(ANum: Integer; AName: string; ASet, AFam: Byte);
    constructor Load(S: TOldStream);
    constructor LoadOld(S: TOldStream);
    destructor Done; virtual;
    procedure Store(S: TOldStream); virtual;
    function IsVertical: Boolean;
  end;

  PFonts = ^TFonts;
  TFonts = object(TSortedCollection)
    MaxNum: Integer16;
    constructor Init;
    constructor Load(S: TOldStream);
    procedure Assign(AFonts: PFonts);
    function Compare(Key1, Key2: Pointer): Integer; virtual;
    function CheckInsertFonts(HWindow: THandle): Boolean;
    function GetFont(ANum: Integer): PFontDes;
    function IDFromName(AName: string): Integer;
    procedure Store(S: TOldStream); virtual;
  end;

implementation

uses AM_Def;

function EnumerateFonts
  (
  var LFont: TLogFont;
  TMetric: PTextMetric;
  FType: Integer;
  Data: PFonts
  )
  : Integer; stdcall;
var
  AName: string;

  function DoAll
      (
      Item: PFontDes
      )
      : Boolean; far;
  begin
    DoAll := Item^.Name^ = AName;
  end;
var
  AFont: PFontDes;
  BFont: PFontDes;
begin
//++ Glukhov IncorrectFontLoading BUILD#135 13.11.00
// Let's include the raster fonts at beginning to delete them later,
// it allows to exlude the fonts that have the same names but different types
//--
//    if FType or Raster_FontType<>FType then begin
//-- Glukhov IncorrectFontLoading BUILD#135 13.11.00
  AName := StrPas(LFont.lfFaceName);
  Inc(Data^.MaxNum);
  AFont := New(PFontDes, Init(Data^.MaxNum, AName, LFont.lfCharSet, LFont.lfPitchAndFamily));
  BFont := Data^.FirstThat(@DoAll);
  if BFont = nil then
  begin
    Data^.Insert(AFont);
    BFont := AFont;
//++ Glukhov IncorrectFontLoading BUILD#135 14.11.00
    BFont.FontType := FType;
//-- Glukhov IncorrectFontLoading BUILD#135 14.11.00
  end
  else
  begin
    BFont^.Installed := True;
//++ Glukhov IncorrectFontLoading BUILD#135 13.11.00
//        BFont^.FontType:=FType;
//--
// Save the font type, if only the font was not a raster one before
    if BFont.FontType and Raster_FontType = 0 then
      BFont.FontType := FType;
//-- Glukhov IncorrectFontLoading BUILD#135 13.11.00
    Dispose(AFont, Done);
  end;
  BFont^.IsTrueType := FType and TRUETYPE_FONTTYPE = TRUETYPE_FONTTYPE;
//++ Glukhov IncorrectFontLoading BUILD#135 13.11.00
// Mark the raster font as 'not installed'
  if BFont.FontType and Raster_FontType <> 0 then
    BFont.Installed := False;
//    end;
//-- Glukhov IncorrectFontLoading BUILD#135 13.11.00
  EnumerateFonts := 1;
end;

constructor TFontDes.Init
  (
  ANum: Integer;
  AName: string;
  ASet: Byte;
  AFam: Byte
  );
begin
  TOldObject.Init;
  FNum := ANum;
  Name := NewStr(AName);
  CharSet := ASet;
  Family := AFam;
  Installed := TRUE;
  UseCount := 0;
end;

destructor TFontDes.Done;
begin
  DisposeStr(Name);
  TOldObject.Done;
end;

constructor TFontDes.LoadOld
  (
  S: TOldStream
  );
begin
  Name := S.ReadStr;
  S.Read(FNum, SizeOf(FNum));
  S.Read(CharSet, SizeOf(CharSet));
  S.Read(Family, SizeOf(Family));
  Installed := FALSE;
  UseCount := 0;
  Convert := TRUE;
end;

constructor TFontDes.Load
  (
  S: TOldStream
  );
begin
  Name := S.ReadStr;
  S.Read(FNum, SizeOf(FNum));
  S.Read(CharSet, SizeOf(CharSet));
  S.Read(Family, SizeOf(Family));
  Installed := FALSE;
  S.Read(UseCount, SizeOf(UseCount));
end;

procedure TFontDes.Store
  (
  S: TOldStream
  );
begin
  S.WriteStr(Name);
  S.Write(FNum, SizeOf(FNum));
  S.Write(CharSet, SizeOf(CharSet));
  S.Write(Family, SizeOf(Family));
  S.Write(UseCount, SizeOf(UseCount));
end;

function TFontDes.IsVertical
  : Boolean;
begin
  Result := PToStr(Name)[1] = '@';
end;

constructor TFonts.Init;
begin
  TSortedCollection.Init(10, 5);
  MaxNum := 0;
end;

constructor TFonts.Load
  (
  S: TOldStream
  );
begin
  TSortedCollection.Load(S);
  S.Read(MaxNum, SizeOf(MaxNum));
end;

procedure TFonts.Store
  (
  S: TOldStream
  );
begin
  TSortedCollection.Store(S);
  S.Write(MaxNum, SizeOf(MaxNum));
end;

function TFonts.Compare
  (
  Key1: Pointer;
  Key2: Pointer
  )
  : Integer;
var
  Font1: PFontDes absolute Key1;
  Font2: PFontDes absolute Key2;
begin
  if Font1^.FNum < Font2^.FNum then
    Compare := -1
  else
    if Font1^.FNum > Font2^.FNum then
      Compare := 1
    else
      Compare := 0;
end;

function TFonts.CheckInsertFonts
  (
  HWindow: THandle
  )
  : Boolean;
var
  DC: HDC;
  Cnt: Integer;
  AFont: PFontDes;
  s: AnsiString;
  i: Integer;
begin
  DC := GetDC(0);
  EnumFonts(DC, nil, @EnumerateFonts, @Self);
  ReleaseDC(0, DC);
  Cnt := 0;
  CheckInsertFonts := TRUE;
  while Cnt < Count do
  begin
    AFont := At(Cnt);
    if not AFont^.Installed then
    begin
      if AFont^.UseCount <= 0 then
        AtFree(Cnt)
      else
      begin
        CheckInsertFonts := FALSE;
        Inc(Cnt);
      end;
    end
    else
      Inc(Cnt);
  end;
end;

function TFonts.GetFont
  (
  ANum: Integer
  )
  : PFontDes;
var
  AFont: PFontDes;
  SIndex: Integer;
begin
  AFont := New(PFontDes, Init(ANum, '', 0, 0));
  if Search(AFont, SIndex) then
    GetFont := At(SIndex)
  else
    GetFont := nil;
  Dispose(AFont, Done);
end;

function TFonts.IDFromName
  (
  AName: string
  )
  : Integer;

  function DoAll
      (
      Item: PFontDes
      )
      : Boolean; far;
  begin
    DoAll := Upstr(Item^.Name^) = Upstr(AName);
  end;

var
  SFont: PFontDes;
begin
  SFont := FirstThat(@DoAll);
  if SFont <> nil then
    IDFromName := SFont^.FNum
  else
    IDFromName := 0;
end;

constructor TFontData.Init;
begin
  TOldObject.Init;
  Font := 1;
  Style := 0;
//++ Glukhov NonzeroText Build#166 27.11.01
//    Height:= 0;
  Height := cMinFontHeight;
//-- Glukhov NonzeroText Build#166 27.11.01
end;

constructor TFontData.Load
  (
  S: TOldStream
  );
var
  Version: Word;
begin
  TOldObject.Init;
  S.Read(Version, SizeOf(Version));
  S.Read(Font, SizeOf(Font));
  S.Read(Style, SizeOf(Style));
  S.Read(Height, SizeOf(Height));
//++ Glukhov NonzeroText Build#166 27.11.01
  if Height = 0 then
    Height := cMinFontHeight;
//-- Glukhov NonzeroText Build#166 27.11.01
end;

procedure TFontData.Store
  (
  S: TOldStream
  );
var
  Version: Word;
begin
  Version := 0;
  S.Write(Version, SizeOf(Version));
  S.Write(Font, SizeOf(Font));
  S.Write(Style, SizeOf(Style));
  S.Write(Height, SizeOf(Height));
end;

function TFontData.IsEqual
  (
  var AFont: TFontData
  )
  : Boolean;
begin
  IsEqual := (Font = AFont.Font)
    and (Style = AFont.Style)
    and (Height = AFont.Height);
end;

procedure TFonts.Assign(AFonts: PFonts);
var
  FontsCopy: PFonts;
  Cnt: Integer;
begin
  FontsCopy := MakeObjectCopy(AFonts);
  FreeAll;
  for Cnt := 0 to FontsCopy.Count - 1 do
    Insert(FontsCopy.At(Cnt));
  MaxNum := FontsCopy.MaxNum;
  FontsCopy.DeleteAll;
  Dispose(FontsCopy, Done);
end;

const
  ROldFontDes: TStreamRec = (
    ObjType: rn_OldFontDes;
    VmtLink: TypeOf(TFontDes);
    Load: @TFontDes.LoadOld;
    Store: @TFontDes.Store);

  RFontDes: TStreamRec = (
    ObjType: rn_FontDes;
    VmtLink: TypeOf(TFontDes);
    Load: @TFontDes.Load;
    Store: @TFontDes.Store);

  RFonts: TStreamRec = (
    ObjType: rn_Fonts;
    VmtLink: TypeOf(TFonts);
    Load: @TFonts.Load;
    Store: @TFonts.Store);

begin
  RegisterType(ROldFontDes);
  RegisterType(RFontDes);
  RegisterType(RFonts);
end.

