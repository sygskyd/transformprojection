{****************************************************************************}
{ Unit AM_Poly                                                               }
{----------------------------------------------------------------------------}
{ Grafikobjekt f�r Linien                                                    }
{----------------------------------------------------------------------------}
{ Modifikationen                                                             }
{  16.02.1994 Martin Forst                                                   }
{  12.04.1994 Martin Forst   Selektieren mit Kreis und Poly                  }
{  12.07.1994 Martin Forst   bei InsertPoint letzer Punkt=neuer Punkt?       }
{****************************************************************************}
unit AM_Poly;

interface

uses WinTypes, WinProcs, AM_Def, AM_View, AM_Paint, AM_Index, AM_Layer, AM_Coll, Objects,
  Clipping, AM_DPointColl;

const
  ciPoly = 16;
  cePoly = 16;

  HandleSize = 4;
  HandleSize1 = HandleSize + 1;
  HandleSizeSmall = 1;
  HandleSizeSmall1 = HandleSizeSmall + 1;

type
  PPoints = ^TPoints;
  TPoints = array[1..16380] of TPoint;

  PPoly = ^TPoly;
  TPoly = object(TView)
    Data: PDPointCollection;
    SnapInfo: PWArray;
    SnapStart: Integer;
    SnapEnd: Integer;
    constructor Init;
    destructor Done; virtual;
    constructor Load(S: TOldStream);
    constructor LoadOld(S: TOldStream);
    procedure CalculateClipRect;
    function CheckNormalDist(PInfo: PPaint; Point: PDPoint; MaxDist: LongInt): Boolean;
    function CheckPointDist(Point: PDPoint; MaxDist: LongInt): Boolean;
    procedure ClosePoly; virtual;
    procedure ReversePoly; virtual; // Glukhov Bug#84 BUILD#125 01.11.00
    procedure Draw(PInfo: PPaint; Clipped: Boolean); virtual;
    procedure DrawTransform(PInfo: PPaint; Clipped: Boolean); virtual;           //Draw with Projection transform
    function DeletePointByIndex(PointInd: Integer): Boolean; virtual;
    procedure EliminatePoints(Radius: LongInt); virtual;
    procedure Explode(PInfo: PPaint; ALayer: PLayer); virtual;
    function GetCutPoint(PInfo: PPaint; var Point1, Point2, Cut: TDPoint): Boolean;
    function GetObjType: Word; virtual;
    function GivePosX: LongInt; virtual;
    function GivePosY: LongInt; virtual;
    function GiveAPosX: LongInt; virtual;
    function GiveAPosY: LongInt; virtual;
    procedure FreePoints(Points: PPoints); virtual;
    function GetNextPoint(PointInd: Integer; var Point: TDPoint): Boolean; virtual;
    procedure GetClippedPoints(PInfo: PPaint; var Points: TClipPointList); virtual;
    procedure GetPoints(PInfo: PPaint; var Points: PPoints); virtual;
    function GetPreviousPoint(PointInd: Integer; var Point: TDPoint): Boolean; virtual;
    procedure InvalidateTriple(PInfo: PPaint; PointInd: Integer); virtual;
    function InsertPoint(Point: TDPoint): Boolean; virtual;
    procedure InsertPointByIndex(var PointInd: Integer; Point: TDPoint); virtual;
    function IsClosed: Boolean;
    procedure MovePoint(PointInd: Integer; Point: TDPoint; ReCalcClipRect: boolean = true); virtual;
    procedure MoveRel(XMove, YMove: LongInt); virtual;
    function SearchForLine(PInfo: PPaint; var Point: TDPoint; const ObjType: TObjectTypes; ADist: LongInt;
      var PointInd: Integer): PIndex; virtual;
    function ClipForLine(PInfo: PPaint; var Point: TDPoint; const ObjType: TObjectTypes; ADist: LongInt;
      var PointInd: Integer; var MCnt: Integer; Radius: Longint): PIndex; virtual;
    function SearchForNearestPoint(PInfo: PPaint; var ClickPos: TDPoint; InCircle: Pointer;
      Exclude: Longint; const ObjType: TObjectTypes; var point: TDPoint; var SnObje: PIndex): Boolean; virtual;
    function SearchForPoint(PInfo: PPaint; InCircle: Pointer; Exclude: LongInt; const ObjType: TObjectTypes;
      var Point: TDPoint; var SnObje: PIndex): Boolean; virtual;
    function SearchNearestPoint(InCircle: Pointer; ClickPos: TDPoint; var Point: TDPoint; var PointInd: Integer): Boolean;
    function SearchPoint(InRect: TDRect; var Point: TDPoint; var PointInd: Integer): Boolean;
    function SearchPointInCircle(InCircle: Pointer; var Point: TDPoint; var PointInd: Integer): Boolean;
    function SelectBetweenPolys(PInfo: PPaint; Poly1, Poly2: Pointer; const ObjType: TObjectTypes;
      Inside: Boolean): Boolean; virtual;
    function SelectByCircle(PInfo: PPaint; Middle: TDPoint;
      Radius: LongInt; const ObjType: TObjectTypes; Inside: Boolean): Boolean; virtual;
    function SelectByPoint(PInfo: PPaint; Point: TDPoint; const ObjType: TObjectTypes): PIndex; virtual;
    function ClipByPoint(PInfo: PPaint; Point: TDPoint; const ObjType: TObjectTypes; var Cnt: Integer;
      var Rpoint: TDPoint; Radius: Longint): PIndex; virtual;
    function SelectByPoly(PInfo: PPaint; Poly: Pointer; const ObjType: TObjectTypes; Inside: Boolean): Boolean; virtual;
    function SelectByPolyLine(PInfo: PPaint; Poly: Pointer; const ObjType: TObjectTypes): Boolean; virtual;
    procedure Scale(XScale, YScale: Real); virtual;
    procedure Store(S: TOldStream); virtual;
    procedure ValidateTriple(PInfo: PPaint; PointInd: Integer); virtual;
    function Laenge(PntIdx: Integer = -1): Double; {~Ray}
    function CheckOnBorder(Point: TDPoint): Boolean;
    function FindIndexOfPoint(Point: TDPoint): integer; //brovak
      //function DeleteCrosses (PInfo: PPaint): Boolean;
    function GetCenter: TDPoint;
    function IsCrossed(PInfo: PPaint): Boolean;
    function IsAngle180(AIndex: Integer): Boolean;
    procedure Recreate(PInfo: PPaint);
    function GetDirection: Boolean;
    function ReadWKT(WKTString: AnsiString): Boolean;
    function ToWKT: AnsiString;
  end;

  PSPoly = ^TSPoly;
  TSPoly = object(TPoly)
    procedure GetPoints(PInfo: PPaint; var Points: PPoints); virtual;
    procedure GetClippedPoints(PInfo: PPaint; var Points: TClipPointList); virtual;
  end;

implementation

uses Classes, AM_CPoly, AM_ProjM, AM_ProjO, AM_Circl, XLines, GrTools, NumTools,
  XStyles, AM_DDE, SysUtils, ToolsLib;

constructor TPoly.Init;
begin
  inherited Init;
  Data := New(PDPointCollection, Init(ciPoly, cePoly));
  SnapInfo := nil;
end;

destructor TPoly.Done;
begin
  Dispose(Data, Done);
  if SnapInfo <> nil then
    Dispose(SnapInfo, Done);
  inherited Done;
end;

constructor TPoly.LoadOld
  (
  S: TOldStream
  );
begin
  inherited Load(S);
  Data := Pointer(S.Get);
  SnapInfo := nil;
end;

constructor TPoly.Load(S: TOldStream);
var
  AData: PCollection;
begin
  inherited Load(S);
  Data := Pointer(S.GetPC(rn_DPointColl));
  SnapInfo := Pointer(S.Get);
  CalculateClipRect;
end;

type
  PPPoints = ^TPPoints;
  TPPoints = array[0..16380] of TPoint;

procedure TPoly.GetPoints
  (
  PInfo: PPaint;
  var Points: PPoints
  );
var
  Cnt: Integer;
  PPoints: PPPoints absolute Points;
begin
  GetMem(Points, SizeOf(TPoint) * Data^.Count);
  for Cnt := 0 to Data^.Count - 1 do
    PInfo^.ConvertToDisp(PDPoint(Data^.At(Cnt))^, PPoints^[Cnt]);
end;

procedure TPoly.GetClippedPoints
  (
  PInfo: PPaint;
  var Points: TClipPointList
  );
var
  Count: Integer;
  Point: TGrPoint;
begin
  Points := TClipPointList.Create;
  Points.Capacity := Data^.Count;
  for Count := 0 to Data^.Count - 1 do
  begin
    PInfo^.ConvertToDispDouble(PDPoint(Data^.At(Count))^, Point.X, Point.Y);
    Points.Add(Point);
  end;
end;

{++brovak}

function TPoly.FindIndexOfPoint(Point: TDPoint): integer;
var
  Cnt: integer;
  x, y: double;
begin;
  Result := -1;
  x := PDPoint(Data^.At(0))^.Y;
  for Cnt := 0 to Data^.Count - 1 do
  begin
    if (PDPoint(Data^.At(Cnt))^.X = Point.X)
      and
      (PDPoint(Data^.At(Cnt))^.Y = Point.Y) then
    begin;
      Result := Cnt;
      Exit;
    end;
  end;
end;
{--brovak}

procedure TPoly.FreePoints
  (
  Points: PPoints
  );
begin
  FreeMem(Points, Word(SizeOf(TPoint)) * Data^.Count);
end;

procedure TPoly.Draw
  (
  PInfo: PPaint;
  Clipped: Boolean
  );
var
  Points: PPoints;
  Cnt: Integer;
  OldPen: THandle;
  OldBrush: THandle;
  OldR2: THandle;
  DrawPoints: Integer;
  ClipPoints: TClipPointList;
  GrPoint: TGrPoint;
  AGrPoint, BGrPoint, CGrPoint: TGrPoint;
  AAngle: Double;
begin
  if Data^.Count = 0 then
  begin
    exit;
  end;

  if Clipped then
  begin
    GetClippedPoints(PInfo, ClipPoints);
    PInfo^.ExtCanvas.ClippedPolyline(ClipPoints, TRUE);
    ClipPoints.Free;
  end
  else
  begin
    GetPoints(PInfo, Points);
    PInfo^.ExtCanvas.Polyline(Points^, Data^.Count, TRUE);
    FreeMem(Points, Data^.Count * SizeOf(TPoint));
  end;
  if PInfo^.GetDrawMode(dm_EditPoly) then
  begin
    if Clipped then
    begin
      GetClippedPoints(PInfo, ClipPoints);
      ClipPolyline(ClipPoints, PInfo^.ClipRect);
      DrawPoints := ClipPoints.Count;
      if DrawPoints > 0 then
      begin
        GetMem(Points, Word(SizeOf(TPoint)) * DrawPoints);
        for Cnt := 0 to DrawPoints - 1 do
        begin
          GrPoint := ClipPoints[Cnt];
          Points^[Cnt + 1] := Point(Round(GrPoint.X), Round(GrPoint.Y));
        end;
      end;
      ClipPoints.Free;
    end
    else
    begin
      GetPoints(PInfo, Points);
      DrawPoints := Data^.Count;
    end;
    if DrawPoints > 0 then
    begin
      LPToDP(PInfo^.ExtCanvas.Handle, Points^, DrawPoints);
      SetMapMode(PInfo^.ExtCanvas.Handle, mm_Text);
      OldPen := SelectObject(PInfo^.ExtCanvas.Handle, GetStockObject(Black_Pen));
      OldBrush := SelectObject(PInfo^.ExtCanvas.Handle, GetStockObject(Hollow_Brush));
      OldR2 := SetRop2(PInfo^.ExtCanvas.Handle, r2_NOTXORPen);
      for Cnt := 1 to DrawPoints do
      begin
        with Points^[Cnt] do
        begin
          if (X > 0) and (Y > 0)
            and (X < PInfo^.ExtCanvas.PhysicalSize.Right) and (Y < PInfo^.ExtCanvas.PhysicalSize.Bottom) then
          begin

//                     TextOut(PInfo^.ExtCanvas.Handle,X,Y,PChar(IntToStr(Cnt)),StrLen(PChar(IntToStr(Cnt))));
            Rectangle(PInfo^.ExtCanvas.Handle, X - HandleSize, Y - HandleSize, X + HandleSize1, Y + HandleSize1);

                     { Direction - Mark }

            if (Cnt > 1) then
            begin
              AAngle := LineAngle(GrTools.GrPoint(X, Y), GrTools.GrPoint(Points^[Cnt - 1].X, Points^[Cnt - 1].Y));
              AGrPoint := GrTools.GrPoint(X, Y);
              GrTools.MovePointAngle(AGrPoint, 20, AAngle);
              MoveToEx(PInfo^.ExtCanvas.Handle, Round(AGrPoint.X), Round(AGrPoint.Y), nil);
              BGrPoint := GrTools.GrPoint(AGrPoint.X, AGrPoint.Y);
              GrTools.MovePointAngle(BGrPoint, 12, AAngle + Pi / 10 + Pi);
              LineTo(PInfo^.ExtCanvas.Handle, Round(BGrPoint.X), Round(BGrPoint.Y));
              MoveToEx(PInfo^.ExtCanvas.Handle, Round(AGrPoint.X), Round(AGrPoint.Y), nil);
              CGrPoint := GrTools.GrPoint(AGrPoint.X, AGrPoint.Y);
              GrTools.MovePointAngle(CGrPoint, 12, AAngle - PI / 10 + Pi);
              LineTo(PInfo^.ExtCanvas.Handle, Round(CGrPoint.X), Round(CGrPoint.Y));
              MoveToEx(PInfo^.ExtCanvas.Handle, Round(BGrPoint.X), Round(BGrPoint.Y), nil);
              LineTo(PInfo^.ExtCanvas.Handle, Round(CGrPoint.X), Round(CGrPoint.Y));
            end;

                     { End }

          end;
        end;
      end;
      SelectObject(PInfo^.ExtCanvas.Handle, OldPen);
      SelectObject(PInfo^.ExtCanvas.Handle, OldBrush);
      SetROP2(PInfo^.ExtCanvas.Handle, OldR2);
      SetMapMode(PInfo^.ExtCanvas.Handle, mm_LoMetric);
      FreeMem(Points, DrawPoints * SizeOf(TPoint));
    end;
  end;
end;

{******************************************************************************

Proc for drawing poly with transforming coords into new projection

*******************************************************************************}
procedure TPoly.DrawTransform
  (
  PInfo: PPaint;
  Clipped: Boolean
  );
var
  Points: PPoints;
  Cnt: Integer;
  OldPen: THandle;
  OldBrush: THandle;
  OldR2: THandle;
  DrawPoints: Integer;
  ClipPoints: TClipPointList;
  GrPoint: TGrPoint;
  AGrPoint, BGrPoint, CGrPoint: TGrPoint;
  AAngle: Double;
begin
  if Data^.Count = 0 then
  begin
    exit;
  end;

  if Clipped then
  begin
    GetClippedPoints(PInfo, ClipPoints);
    PInfo^.ExtCanvas.ClippedPolyline(ClipPoints, TRUE);
    ClipPoints.Free;
  end
  else
  begin
    GetPoints(PInfo, Points);
    PInfo^.ExtCanvas.Polyline(Points^, Data^.Count, TRUE);
    FreeMem(Points, Data^.Count * SizeOf(TPoint));
  end;
  if PInfo^.GetDrawMode(dm_EditPoly) then
  begin
    if Clipped then
    begin
      GetClippedPoints(PInfo, ClipPoints);
      ClipPolyline(ClipPoints, PInfo^.ClipRect);
      DrawPoints := ClipPoints.Count;
      if DrawPoints > 0 then
      begin
        GetMem(Points, Word(SizeOf(TPoint)) * DrawPoints);
        for Cnt := 0 to DrawPoints - 1 do
        begin
          GrPoint := ClipPoints[Cnt];
          Points^[Cnt + 1] := Point(Round(GrPoint.X), Round(GrPoint.Y));
        end;
      end;
      ClipPoints.Free;
    end
    else
    begin
      GetPoints(PInfo, Points);
      DrawPoints := Data^.Count;
    end;
    if DrawPoints > 0 then
    begin
      LPToDP(PInfo^.ExtCanvas.Handle, Points^, DrawPoints);
      SetMapMode(PInfo^.ExtCanvas.Handle, mm_Text);
      OldPen := SelectObject(PInfo^.ExtCanvas.Handle, GetStockObject(Black_Pen));
      OldBrush := SelectObject(PInfo^.ExtCanvas.Handle, GetStockObject(Hollow_Brush));
      OldR2 := SetRop2(PInfo^.ExtCanvas.Handle, r2_NOTXORPen);
      for Cnt := 1 to DrawPoints do
      begin
        with Points^[Cnt] do
        begin
          if (X > 0) and (Y > 0)
            and (X < PInfo^.ExtCanvas.PhysicalSize.Right) and (Y < PInfo^.ExtCanvas.PhysicalSize.Bottom) then
          begin

//                     TextOut(PInfo^.ExtCanvas.Handle,X,Y,PChar(IntToStr(Cnt)),StrLen(PChar(IntToStr(Cnt))));
            Rectangle(PInfo^.ExtCanvas.Handle, X - HandleSize, Y - HandleSize, X + HandleSize1, Y + HandleSize1);

                     { Direction - Mark }

            if (Cnt > 1) then
            begin
              AAngle := LineAngle(GrTools.GrPoint(X, Y), GrTools.GrPoint(Points^[Cnt - 1].X, Points^[Cnt - 1].Y));
              AGrPoint := GrTools.GrPoint(X, Y);
              GrTools.MovePointAngle(AGrPoint, 20, AAngle);
              MoveToEx(PInfo^.ExtCanvas.Handle, Round(AGrPoint.X), Round(AGrPoint.Y), nil);
              BGrPoint := GrTools.GrPoint(AGrPoint.X, AGrPoint.Y);
              GrTools.MovePointAngle(BGrPoint, 12, AAngle + Pi / 10 + Pi);
              LineTo(PInfo^.ExtCanvas.Handle, Round(BGrPoint.X), Round(BGrPoint.Y));
              MoveToEx(PInfo^.ExtCanvas.Handle, Round(AGrPoint.X), Round(AGrPoint.Y), nil);
              CGrPoint := GrTools.GrPoint(AGrPoint.X, AGrPoint.Y);
              GrTools.MovePointAngle(CGrPoint, 12, AAngle - PI / 10 + Pi);
              LineTo(PInfo^.ExtCanvas.Handle, Round(CGrPoint.X), Round(CGrPoint.Y));
              MoveToEx(PInfo^.ExtCanvas.Handle, Round(BGrPoint.X), Round(BGrPoint.Y), nil);
              LineTo(PInfo^.ExtCanvas.Handle, Round(CGrPoint.X), Round(CGrPoint.Y));
            end;

                     { End }

          end;
        end;
      end;
      SelectObject(PInfo^.ExtCanvas.Handle, OldPen);
      SelectObject(PInfo^.ExtCanvas.Handle, OldBrush);
      SetROP2(PInfo^.ExtCanvas.Handle, OldR2);
      SetMapMode(PInfo^.ExtCanvas.Handle, mm_LoMetric);
      FreeMem(Points, DrawPoints * SizeOf(TPoint));
    end;
  end;
end;

procedure TPoly.Scale {2705F}
  (
  XScale: Real;
  YScale: Real
  );

procedure DoAll
      (
      Item: PDPoint
      ); far;
  begin
    Item^.Scale(XScale, YScale);
  end;
begin
  inherited Scale(XScale, YScale);
  Data^.ForEach(@DoAll);
  Calculatecliprect;
end; {2705F}

{**************************************************************************************}
{ Function TPoly.SearchForLine                                                         }
{  Ermittelt, ob der Punkt in der N�he eine Linie des Polys liegt. Dabei mu� der       }
{  Normalabstand des Punktes von der Linie <ADist sein.                                }
{  m�gliche Optimierung: Abfrage, ob Punkt in Begrenzungsrechteck der Einzellinien     }
{**************************************************************************************}

function TPoly.SearchForLine
  (
  PInfo: PPaint;
  var Point: TDPoint; { Mausposition                                  }
  const ObjType: TObjectTypes;
  ADist: LongInt; { maximaler (normal) Abstand von der Linie      }
  var PointInd: Integer { Anfangspunkt der Linie zu der der Abstand     }
   { unterschritten wurde                          }
  )
  : PIndex;
var
  Cnt: Integer;
  CntTo: Integer;
  ExpRect: TDRect;
  BDist: Double;
  BPoint: TDPoint;
begin
  SearchForLine := nil;
  if GetObjType in ObjType then
  begin
    ExpRect.Init;
    ExpRect := ClipRect;
    ExpRect.Grow(ADist);
    if ExpRect.PointInside(Point) then
    begin
      Cnt := 0;
      CntTo := Data^.Count - 1;
      while (Cnt < CntTo) and (not (PInfo^.NormalDistance(Data^.At(Cnt), Data^.At(Cnt + 1), @Point, BDist, BPoint))
        or (BDist > ADist)) do
        Inc(Cnt);
      if Cnt < CntTo then
      begin
        SearchForLine := @Self;
        Point.Init(BPoint.X, BPoint.Y);
        PointInd := Cnt;
      end;
    end;
    ExpRect.Done;
  end;
end;

function TPoly.SelectByPoint
  (
  PInfo: PPaint;
  Point: TDPoint;
  const ObjType: TObjectTypes
  )
  : PIndex;
var
  MDist: LongInt;
  AIndex: Integer;
begin
  MDist := PInfo^.CalculateDraw(poMaxDist);
  SelectByPoint := SearchForLine(PInfo, Point, ObjType, MDist, AIndex);
end;

function TPoly.ClipForLine
  (
  PInfo: PPaint;
  var Point: TDPoint; { Mausposition                                  }
  const ObjType: TObjectTypes;
  ADist: LongInt; { maximaler (normal) Abstand von der Linie      }
  var PointInd: Integer; { Anfangspunkt der Linie zu der der Abstand     }
  var MCnt: Integer; { unterschritten wurde                          }
  Radius: Longint
  )
  : PIndex;
var
  ExpRect: TDRect;
  BDist: Double;
  BPoint: TDPoint;
  Cnt: Integer;
  CntTo: Integer;
begin
  ClipForLine := nil;
  MCnt := -1;
  if GetObjType in ObjType then
  begin
    ExpRect.Init;
    ExpRect := ClipRect;
    ExpRect.Grow(ADist);
    ADist := Radius;
    if ExpRect.PointInside(Point) then
    begin
      Cnt := 0;
      CntTo := Data^.Count - 1;
      while (Cnt < CntTo) and (not (PInfo^.NormalDistance(Data^.At(Cnt), Data^.At(Cnt + 1), @Point, BDist, BPoint))
        or (BDist > ADist)) do
        Inc(Cnt);
      if Cnt < CntTo then
      begin
        ClipForLine := @Self;
        Point.Init(BPoint.X, BPoint.Y);
        PointInd := Cnt;
        MCnt := Cnt;
      end;
    end;
    ExpRect.Done;
  end;
end;

function TPoly.ClipByPoint
  (
  PInfo: PPaint;
  Point: TDPoint;
  const ObjType: TObjectTypes;
  var Cnt: Integer;
  var RPoint: TDPoint;
  Radius: Longint
  )
  : PIndex;
var
  MDist: LongInt;
  AIndex: Integer;
begin
  MDist := PInfo^.CalculateDraw(poMaxDist);
  ClipByPoint := ClipForLine(PInfo, Point, ObjType, MDist, AIndex, Cnt, Radius);
  if cnt <> -1 then
    RPoint.Init(Point.x, Point.y)
  else
    RPoint.Init(0, 0);
end;

procedure TPoly.Store(S: TOldStream);
var
  AData: PCollection;
begin
  inherited Store(S);
  S.PutPC(Data, 50);
  S.Put(SnapInfo);
end;

function TPoly.GetObjType
  : Word;
begin
  GetObjType := ot_Poly;
end;

procedure TPoly.MoveRel
  (
  XMove: LongInt;
  YMove: LongInt
  );

procedure DoAll
      (
      Item: PDPoint
      ); far;
  begin
    Item^.Move(XMove, YMove);
  end;
begin
  inherited MoveRel(XMove, YMove);
  Data^.ForEach(@DoAll);
end;

function TPoly.InsertPoint
  (
  Point: TDPoint
  )
  : Boolean;
var
  LastPoint: PDPoint;
begin
  if Data^.Count < MaxCollectionSize - 1 then
  begin
    if Data^.Count = 0 then
      LastPoint := nil {1207F}
    else
      LastPoint := Data^.At(Data^.Count - 1);
    if (LastPoint = nil)
      or (LastPoint^.X <> Point.X)
      or (LastPoint^.Y <> Point.Y) then
    begin {1207F}
      Data^.Insert(@Point);
      ClipRect.CorrectByPoint(Point);
    end;
    InsertPoint := TRUE;
  end
  else
    InsertPoint := FALSE;
end;

function TPoly.GetNextPoint
  (
  PointInd: Integer;
  var Point: TDPoint
  )
  : Boolean;
begin
  if PointInd < Data^.Count - 1 then
  begin
    Point.Init(0, 0);
    Point := PDPoint(Data^.At(PointInd + 1))^;
    GetNextPoint := TRUE;
  end
  else
    GetNextPoint := FALSE;
end;

function TPoly.GetPreviousPoint
  (
  PointInd: Integer;
  var Point: TDPoint
  )
  : Boolean;
begin
  if PointInd > 0 then
  begin
    Point.Init(0, 0);
    Point := PDPoint(Data^.At(PointInd - 1))^;
    GetPreviousPoint := TRUE;
  end
  else
    GetPreviousPoint := FALSE;
end;

{************************************************************************************}
{ Function TPoly.SearchPoint                                                         }
{  Sucht einen Punkt, der innherhalb von InRect liegt. Wurde ein Punkt gefunden, so  }
{  liefert SearchPoint TRUE. DKoordinaten des Punktes werden in Point und der Index  }
{  des Punktes in PointInd zur�ckgegeben.                                            }
{************************************************************************************}

function TPoly.SearchPoint
  (
  InRect: TDRect;
  var Point: TDPoint;
  var PointInd: Integer
  )
  : Boolean;

function DoAll
      (
      Item: PDPoint
      )
      : Boolean; far;
  begin
    DoAll := InRect.PointInside(Item^);
  end;
var
  FPoint: PDPoint;
begin
  if ClipRect.IsVisible(InRect) then
  begin
    FPoint := Data^.FirstThat(@DoAll);
    if FPoint <> nil then
    begin
      Point.Init(FPoint^.X, FPoint^.Y);
      PointInd := Data^.IndexOf(FPoint);
      SearchPoint := TRUE;
    end
    else
      SearchPoint := FALSE;
  end
  else
    SearchPoint := FALSE;
end;

function TPoly.SearchNearestPoint
  (
  InCircle: Pointer;
  ClickPos: TDPoint;
  var Point: TDPoint;
  var PointInd: Integer
  )
  : Boolean;
var
  LastPoint: PDPoint;

  procedure DoAll
      (
      Item: PDPoint
      ); far;
  begin
    if PEllipse(InCircle)^.PointInside(Item^)
      and (ClickPos.Dist(Point) > ClickPos.Dist(Item^)) then
    begin
      Point := Item^;
      LastPoint := Item;
      Result := True;
    end;
  end;
begin
  Result := FALSE;
  if ClipRect.IsVisible(PEllipse(InCircle)^.ClipRect) then
  begin
    Data^.ForEach(@DoAll);
    if Result then
      PointInd := Data^.IndexOf(LastPoint);
  end;
end;

{************************************************************************************}
{ Function TPoly.SearchPointInCircle                                                 }
{  Sucht einen Punkt, der innherhalb von InCircle liegt. Wurde ein Punkt gefunden,   }
{  so liefert SearchPointInCircle TRUE. Die Koordinaten des Punktes werden in Point  }
{  und der Index des Punktes in PointInd zur�ckgegeben.                              }
{************************************************************************************}

function TPoly.SearchPointInCircle
  (
  InCircle: Pointer;
  var Point: TDPoint;
  var PointInd: Integer
  )
  : Boolean;

function DoAll
      (
      Item: PDPoint
      )
      : Boolean; far;
  begin
    DoAll := PEllipse(InCircle)^.PointInside(Item^);
  end;
var
  FPoint: PDPoint;
begin
  if ClipRect.IsVisible(PEllipse(InCircle)^.ClipRect) then
  begin
    FPoint := Data^.FirstThat(@DoAll);
    if FPoint <> nil then
    begin
      Point.Init(FPoint^.X, FPoint^.Y);
      PointInd := Data^.IndexOf(FPoint);
      SearchPointInCircle := TRUE;
    end
    else
      SearchPointInCircle := FALSE;
  end
  else
    SearchPointInCircle := FALSE;
end;

function TPoly.SearchForPoint
  (
  PInfo: PPaint;
  InCircle: Pointer;
  Exclude: LongInt;
  const ObjType: TObjectTypes;
  var Point: TDPoint;
  var SnObje: PIndex
  )
  : Boolean;
var
  Dummy: Integer;
begin
  if (GetObjType in ObjType) and (Index <> Exclude) then
    SearchForPoint := SearchPointInCircle(InCircle, Point, Dummy)
  else
    SearchForPoint := FALSE;
end;

procedure TPoly.InvalidateTriple
  (
  PInfo: PPaint;
  PointInd: Integer
  );
var
  InvRect: TDRect;
  Rect: TRect;
  Point: TDPoint;
begin
  InvRect.Init;
  if GetPreviousPoint(PointInd, Point) then
    InvRect.CorrectByPoint(Point);
  InvRect.CorrectByPoint(PDPoint(Data^.At(PointInd))^);
  if GetNextPoint(PointInd, Point) then
    InvRect.CorrectByPoint(Point);
  PInfo^.ConvertToPhysRect(InvRect, Rect);
  InflateRect(Rect, HandleSize1, HandleSize1);
  InvalidateRect(PInfo^.HWindow, @Rect, TRUE);
  PInfo^.FromGraphic := TRUE;
end;

procedure TPoly.ValidateTriple
  (
  PInfo: PPaint;
  PointInd: Integer
  );
var
  InvRect: TDRect;
  Rect: TRect;
  Point: TDPoint;
begin
  InvRect.Init;
  if GetPreviousPoint(PointInd, Point) then
    InvRect.CorrectByPoint(Point);
  InvRect.CorrectByPoint(PDPoint(Data^.At(PointInd))^);
  if GetNextPoint(PointInd, Point) then
    InvRect.CorrectByPoint(Point);
  PInfo^.ConvertToPhysRect(InvRect, Rect);
  InflateRect(Rect, 1, 1);
  ValidateRect(PInfo^.HWindow, @Rect);
end;

procedure TPoly.MovePoint
  (
  PointInd: Integer;
  Point: TDPoint;
  ReCalcClipRect: boolean = true
  );
begin
// ++ Cadmensky
  PDPoint(Data^.At(PointInd))^.Init(Point.X, Point.Y);
//   PDPoint (Data^.At (PointInd))^ := Point;
// -- Cadmensky
  if ReCalcClipRect = true then
    CalculateClipRect;
end;

function TPoly.GetDirection: Boolean;
var
  f: Double;
  i: Integer;
begin
  if GetObjType = ot_Poly then
  begin
    Result := (PDPoint(Data^.At(0))^.X) > (PDPoint(Data^.At(Data^.Count - 1))^.X);
  end
  else
  begin
    f := 0.0;
    for i := 1 to Data^.Count - 1 do
    begin
      f := f + ((0.0 + PDPoint(Data^.At(i))^.X + PDPoint(Data^.At(i - 1))^.X) * (0.0 + PDPoint(Data^.At(i))^.Y - PDPoint(Data^.At(i - 1))^.Y));
    end;
    Result := (f > 0.0);
  end;
end;

procedure TPoly.InsertPointByIndex
  (
  var PointInd: Integer;
  Point: TDPoint
  );
var
  APoint: TDPoint;
  NewPoint: PDPoint;
begin
  if Data^.Count >= MaxCollectionSize then
    Exit;
  NewPoint := New(PDPoint, Init(Point.X, Point.Y));
  if not GetPreviousPoint(PointInd, APoint) then
    Inc(PointInd);
  Data^.AtInsert(PointInd, NewPoint);
  Dispose(NewPoint, Done);
  if SnapInfo <> nil then
    SnapInfo^.InsertAndRenumber(PointInd);
  CalculateClipRect;
  UpdateData;
end;

function TPoly.DeletePointByIndex
  (
  PointInd: Integer
  )
  : Boolean;
begin
  if Data^.Count > 2 then
  begin
    Data^.AtFree(PointInd);
    CalculateClipRect;
    DeletePointByIndex := TRUE;
    UpdateData;
    if SnapInfo <> nil then
    begin
      SnapInfo^.DeleteAndRenumber(PointInd);
      if SnapInfo^.Count = 0 then
        Dispose(SnapInfo, Done);
    end;
  end
  else
    DeletePointByIndex := FALSE;
end;

procedure TPoly.CalculateClipRect;

  procedure DoAll
      (
      Item: PDPoint
      ); far;
  begin
    ClipRect.CorrectByPoint(Item^);
  end;

begin
  ClipRect.Init;
  Data^.ForEach(@DoAll);
end;

procedure TSPoly.GetPoints
  (
  PInfo: PPaint;
  var Points: PPoints
  );
var
  Cnt: Integer;
  PPoints: PPPoints absolute Points;
begin
  GetMem(Points, Word(SizeOf(TPoint)) * Data^.Count);
  for Cnt := 0 to Data^.Count - 1 do
    PInfo^.ScaleToDisp(PDPoint(Data^.At(Cnt))^, PPoints^[Cnt]);
end;

procedure TSPoly.GetClippedPoints
  (
  PInfo: PPaint;
  var Points: TClipPointList
  );
var
  Count: Integer;
  Point: TGrPoint;
begin
  Points := TClipPointList.Create;
  Points.Capacity := Data^.Count;
  for Count := 0 to Data^.Count - 1 do
  begin
    PInfo^.ScaleToDispDouble(PDPoint(Data^.At(Count))^, Point.X, Point.Y);
    Points.Add(Point);
  end;
end;

procedure TPoly.ClosePoly;
begin
end;

//++ Glukhov Bug#84 BUILD#125 01.11.00

procedure TPoly.ReversePoly;
var
  Point: TDPoint;
  i, iMax, Cnt: Integer;
begin
   // Calculate the maximal index of point to be exchanged with the reverse one
  Cnt := Data.Count - 1;
  iMax := Data.Count div 2 - 1;
   // Make an exchange
  for i := 0 to iMax do
  begin
    Point.Init(Data.Items[i].X, Data.Items[i].Y);
    Data.Items[i].Init(Data.Items[Cnt - i].X, Data.Items[Cnt - i].Y);
    Data.Items[Cnt - i].Init(Point.X, Point.Y);

  end;
end;
//-- Glukhov Bug#84 BUILD#125 01.11.00

function TPoly.IsClosed
  : Boolean;
var
  FirstPoint: PDPoint;
  LastPoint: PDPoint;
begin
  FirstPoint := Data^.At(0);
  LastPoint := Data^.At(Data^.Count - 1);
  if (FirstPoint^.X = LastPoint^.X) and (FirstPoint^.Y = LastPoint^.Y) then
    IsClosed := TRUE
  else
    IsClosed := FALSE;
end;

procedure TPoly.Explode
  (
  PInfo: PPaint;
  ALayer: PLayer
  );
var
  Cnt: Integer;
  APoly: PPoly;
begin
  for Cnt := 0 to Data^.Count - 2 do
  begin
    APoly := New(PPoly, Init);
    APoly^.InsertPoint(PDPoint(Data^.At(Cnt))^);
    APoly^.InsertPoint(PDPoint(Data^.At(Cnt + 1))^);
    if APoly^.Data^.Count < 2 then
      Dispose(APoly, Done)
    else
    begin
      if PLayer(PInfo^.Objects)^.InsertObject(PInfo, APoly, FALSE) then
      begin
        ALayer^.InsertObject(PInfo, New(PIndex, Init(APoly^.Index)), FALSE);
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
        if (DDEHandler.DoAutoIns) then
        begin
          ProcDBSendObject(PInfo^.AProj, PIndex(APoly), 'EIN');
        end;
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
      end;
    end;
  end;
end;

procedure TPoly.EliminatePoints
  (
  Radius: LongInt
  );
var
  SelRect: TDRect;
  IsInside: Boolean;
  NewX: Double;
  NewY: Double;
  Cnt: Integer;
  APoint: PDPoint;
  BPoint: PDPoint;
  PointCnt: Integer;
begin
  Cnt := 0;
  SelRect.Init;
  while (Cnt < Data^.Count - 1) and (Data^.Count > 2) do
  begin
    APoint := Data^.At(Cnt);
    NewX := APoint^.X;
    NewY := APoint^.Y;
    PointCnt := 1;
    SelRect.AssignByPoint(APoint^, Radius);
    repeat
      Inc(Cnt);
      BPoint := Data^.At(Cnt);
      IsInside := SelRect.PointInside(BPoint^);
      if IsInside then
      begin
        NewX := NewX + BPoint^.X;
        NewY := NewY + BPoint^.Y;
        Inc(PointCnt);
        Data^.Free(TDPoint(BPoint^));
        if SnapInfo <> nil then
          SnapInfo^.DeleteAndRenumber(Cnt);
      end;
    until (Cnt >= Data^.Count - 1) or (Data^.Count <= 2) or not IsInside;
    APoint^.Init(Trunc(NewX / PointCnt), Trunc(NewY / PointCnt));
  end;
  CalculateClipRect;
end;

function TPoly.GetCutPoint
  (
  PInfo: PPaint;
  var Point1: TDPoint;
  var Point2: TDPoint;
  var Cut: TDPoint
  )
  : Boolean;
var
  CutDistance: Double;
  Cnt: Integer;
  ACut: TDPoint;
begin
  Cut.Init(0, 0);
  ACut.Init(0, 0);
  CutDistance := MaxLongInt;
  for Cnt := 0 to Data^.Count - 2 do
  begin
    if PInfo^.LineCut(FALSE, PDPoint(Data^.At(Cnt))^, PDPoint(Data^.At(Cnt + 1))^, Point1, Point2, ACut) then
    begin
      if ACut.Dist(Point1) < CutDistance then
      begin
        CutDistance := ACut.Dist(Point1);
        Cut := ACut;
      end;
    end;
  end;
  GetCutPoint := CutDistance <> MaxLongInt;
end;

function TPoly.SelectByCircle
  (
  PInfo: PPaint;
  Middle: TDPoint;
  Radius: LongInt;
  const ObjType: TObjectTypes;
  Inside: Boolean
  )
  : Boolean;
var
  Clip: TDRect;
  Cnt: Integer;
begin
  if not GetState(sf_UnVisible) then
  begin
    SelectByCircle := FALSE;
    if ot_Poly in ObjType then
    begin
      Clip.Init;
      Clip.Assign(Middle.X - Radius, Middle.Y - Radius,
        Middle.X + Radius, Middle.Y + Radius);
      if Inside then
      begin
        if ClipRect.IsInside(Clip) then
        begin
          for Cnt := 1 to Data^.Count - 1 do
            if PDPoint(Data^.At(Cnt))^.Dist(Middle) > Radius then
              Exit;
          SelectByCircle := TRUE;
        end;
      end
      else
        SelectByCircle := ClipRect.IsVisible(Clip)
          and (CheckPointDist(@Middle, Radius)
          or CheckNormalDist(PInfo, @Middle, Radius));
    end;
  end
  else
    Result := FALSE;
end;

function TPoly.CheckNormalDist
  (
  PInfo: PPaint;
  Point: PDPoint;
  MaxDist: LongInt
  )
  : Boolean;
var
  Cnt: Integer;
  Stop: Integer;
  Found: Boolean;
begin
  Stop := Data^.Count - 1;
  Cnt := 0;
  Found := FALSE;
  while (Cnt < Stop) and not (Found) do
  begin
    Found := PInfo^.CheckNormalDistance(Data^.At(Cnt), Data^.At(Cnt + 1), Point, MaxDist);
    Inc(Cnt);
  end;
  CheckNormalDist := Found;
end;

function TPoly.CheckPointDist
  (
  Point: PDPoint;
  MaxDist: LongInt
  )
  : Boolean;

  function CheckPoints
      (
      Item: PDPoint
      )
      : Boolean; far;
  begin
    CheckPoints := Item^.Dist(Point^) <= MaxDist;
  end;

begin
  CheckPointDist := Data^.FirstThat(@CheckPoints) <> nil;
end;

function TPoly.SelectByPoly
  (
  PInfo: PPaint;
  Poly: Pointer;
  const ObjType: TObjectTypes;
  Inside: Boolean
  )
  : Boolean;

function CheckCut
      : Boolean;
  var
    Cnt: Integer;
    Cnt1: Integer;
    Cut: TDPoint;
  begin
    CheckCut := FALSE;
    for Cnt := 0 to Data^.Count - 2 do
      for Cnt1 := 0 to PCPoly(Poly)^.Data^.Count - 2 do
        if PInfo^.LineCut(TRUE, PDPoint(Data^.At(Cnt))^, PDPoint(Data^.At(Cnt + 1))^,
          PDPoint(PCPoly(Poly)^.Data^.At(Cnt1))^, PDPoint(PCPoly(Poly)^.Data^.At(Cnt1 + 1))^, Cut) then
        begin
          CheckCut := TRUE;
          Exit;
        end;
  end;
begin
  if not GetState(sf_UnVisible) then
  begin
    if (GetObjType in ObjType)
      and ClipRect.IsVisible(PView(Poly)^.ClipRect) then
      if Inside then
      begin
        Result := ClipRect.IsInside(PView(Poly)^.ClipRect)
          and PCPoly(Poly)^.PointInside(PInfo, PDPoint(Data^.At(0))^)
               {and not CheckCut};
      end
      else
      begin
        Result := PCPoly(Poly)^.PointInside(PInfo, PDPoint(Data^.At(0))^)
               //++ Glukhov SelectIntersecting BUILD#123 19.07.00
            // Or a Selecting Polygon is inside of a Selected Polygon
        or PCPoly(@Self)^.PointInside(PInfo, PDPoint(PCPoly(Poly)^.Data^.At(0))^)
               //-- Glukhov SelectIntersecting BUILD#123 19.07.00
        or CheckCut;
      end
    else
      Result := FALSE;
  end
  else
    Result := FALSE;
end;

function TPoly.SelectByPolyLine
  (
  PInfo: PPaint;
  Poly: Pointer;
  const ObjType: TObjectTypes
  )
  : Boolean;

function CheckCut
      : Boolean;
  var
    Cnt: Integer;
    Cnt1: Integer;
    Cut: TDPoint;
  begin
    CheckCut := FALSE;
    for Cnt := 0 to Data^.Count - 2 do
      for Cnt1 := 0 to PCPoly(Poly)^.Data^.Count - 2 do
        if PInfo^.LineCut(TRUE, PDPoint(Data^.At(Cnt))^, PDPoint(Data^.At(Cnt + 1))^,
          PDPoint(PCPoly(Poly)^.Data^.At(Cnt1))^, PDPoint(PCPoly(Poly)^.Data^.At(Cnt1 + 1))^, Cut) then
        begin
          CheckCut := TRUE;
          Exit;
        end;
  end;
begin
  if not GetState(sf_UnVisible) then
  begin
    if (GetObjType in ObjType)
      and ClipRect.IsVisible(PView(Poly)^.ClipRect) then
      SelectByPolyLine := CheckCut
    else
      SelectByPolyLine := FALSE;
  end
  else
    Result := FALSE;
end;

function TPoly.GivePosX: LongInt;
var
  Temp: LongInt;
  Count: Integer;

  procedure DoAll
      (
      Item: PDPoint
      ); far;
  begin
    Inc(Count);
    if Count <= Data^.Count then
    begin
      if Item^.X > Temp then
        Temp := Item^.X;
    end;
  end;
begin
  Temp := -MaxLongInt;
  Count := 0;
  Data^.ForEach(@DoAll);
  GivePosX := Temp;
end;

function TPoly.GivePosY: LongInt;
var
  TempX: LongInt;
  TempY: LongInt;
  Count: Integer;

  procedure DoAll
      (
      Item: PDPoint
      ); far;
  begin
    Inc(Count);
    if Count <= Data^.Count then
    begin
      if Item^.X > TempX then
      begin
        TempX := Item^.X;
        TempY := Item^.Y;
      end;
    end;
  end;
begin
  TempX := -MaxLongInt;
  TempY := 0;
  Count := 0;
  Data^.ForEach(@DoAll);
  GivePosY := TempY;
end;

function TPoly.GiveAPosX: LongInt;
begin
  Result := LimitToLong((ClipRect.A.X * 1.0 + ClipRect.B.X * 1.0) / 2.0);
end;

function TPoly.GiveAPosY: LongInt;
begin
  Result := LimitToLong((ClipRect.A.Y * 1.0 + ClipRect.B.Y * 1.0) / 2.0);
end;

function TPoly.Laenge(PntIdx: Integer = -1): Double;
var
  Count: Integer;
  L: Double;
begin
  L := 0;
  for Count := 1 to Data^.Count - 1 do
  begin
    if (PntIdx > -1) and (Count > PntIdx) then
    begin
      break;
    end;
    L := L + PDPoint(Data^.At(Count))^.Dist(PDPoint(Data^.At(Count - 1))^);
  end;
  Laenge := L / 100;
end;

function TPoly.SearchForNearestPoint
  (
  PInfo: PPaint;
  var ClickPos: TDPoint;
  InCircle: Pointer;
  Exclude: LongInt;
  const ObjType: TObjectTypes;
  var Point: TDPoint;
  var SnObje: PIndex
  )
  : Boolean;

procedure DoAll
      (
      Item: PDPoint
      ); far;
  begin
    if PEllipse(InCircle)^.PointInside(Item^)
      and (ClickPos.Dist(Point) > ClickPos.Dist(Item^)) then
    begin
      Point := Item^;
      Result := True;
    end;
  end;
begin
  Result := FALSE;
  if (GetObjType in ObjType) and (Index <> Exclude)
    and ClipRect.IsVisible(PEllipse(InCircle)^.ClipRect) then
    Data^.ForEach(@DoAll);
end;

function TPoly.SelectBetweenPolys
  (
  PInfo: PPaint;
  Poly1: Pointer;
  Poly2: Pointer;
  const ObjType: TObjectTypes;
  Inside: Boolean
  )
  : Boolean;

function CheckCut1
      : Boolean;
  var
    Cnt: Integer;
    Cnt1: Integer;
    Cut: TDPoint;
  begin
    CheckCut1 := FALSE;
    for Cnt := 0 to Data^.Count - 2 do
      for Cnt1 := 0 to PCPoly(Poly1)^.Data^.Count - 2 do
        if PInfo^.LineCut(TRUE, PDPoint(Data^.At(Cnt))^, PDPoint(Data^.At(Cnt + 1))^,
          PDPoint(PCPoly(Poly1)^.Data^.At(Cnt1))^, PDPoint(PCPoly(Poly1)^.Data^.At(Cnt1 + 1))^, Cut) then
        begin
          CheckCut1 := TRUE;
          Exit;
        end;
  end;

  function CheckCut2
      : Boolean;
  var
    Cnt: Integer;
    Cnt1: Integer;
    Cut: TDPoint;
  begin
    CheckCut2 := FALSE;
    for Cnt := 0 to Data^.Count - 2 do
      for Cnt1 := 0 to PCPoly(Poly2)^.Data^.Count - 2 do
        if PInfo^.LineCut(TRUE, PDPoint(Data^.At(Cnt))^, PDPoint(Data^.At(Cnt + 1))^,
          PDPoint(PCPoly(Poly2)^.Data^.At(Cnt1))^, PDPoint(PCPoly(Poly2)^.Data^.At(Cnt1 + 1))^, Cut) then
        begin
          CheckCut2 := TRUE;
          Exit;
        end;
  end;
begin
  if not GetState(sf_UnVisible) then
  begin
    if (GetObjType in ObjType)
      and (ClipRect.IsVisible(PView(Poly1)^.ClipRect) or ClipRect.IsVisible(PView(Poly2)^.ClipRect)) then
      if Inside then
        SelectBetweenPolys := (ClipRect.IsInside(PView(Poly1)^.ClipRect) or ClipRect.IsInside(PView(Poly2)^.ClipRect))
          and (PCPoly(Poly1)^.PointInside(PInfo, PDPoint(Data^.At(0))^)
          xor PCPoly(Poly2)^.PointInside(PInfo, PDPoint(Data^.At(0))^))
          and not CheckCut1 and not CheckCut2
      else
        SelectBetweenPolys := PCPoly(Poly1)^.PointInside(PInfo, PDPoint(Data^.At(0))^)
          xor PCPoly(Poly2)^.PointInside(PInfo, PDPoint(Data^.At(0))^)
          or CheckCut1 or CheckCut2
    else
      SelectBetweenPolys := FALSE;
  end
  else
    Result := FALSE;
end;

function TPoly.CheckOnBorder
  (
  POint: TDPoint
  ): Boolean;
var
  i: longint;
  APoint: PDPoint;
  APointn: PDPoint;
begin
  CheckOnborder := False;
  for i := 0 to Data^.Count - 2 do
  begin
    Apoint := Data^.At(i);
    APointn := Data^.At(i + 1);
    if (APoint^.x = Point.x) and (APoint^.y = Point.y) or (APointn^.x = Point.x) and (APointn^.y = point.y) then
    begin
      CheckOnborder := TRUE;
      Break;
    end;
  end;
end;

{
function TPoly.DeleteCrosses (PInfo: PPaint): Boolean;
var
   ObjType: Integer;

   function CheckPoly
         (
         Item: PCPoly
         )
         : Boolean;
      function DoCheckPoly
            (
            Start: Integer;
            Stop: Integer
            )
            : Boolean;
      var Cnt: Integer;
         Cnt2: Integer;
         i: Integer;
         C1: TDPoint;
         InsInd: Integer;
      begin
         Result := TRUE;
         if ObjType = ot_Poly then
         begin
            with Item^.Data^ do
            begin
               for Cnt := 0 to Count - 2 do
               begin
                  for Cnt2 := Cnt + 1 to Count - 2 do
                  begin
                     if PInfo^.LineCut (True, PDPoint (At (Cnt))^, PDPoint (At (Cnt + 1))^,
                        PDPoint (At (Cnt2))^, PDPoint (At (Cnt2 + 1))^, C1) then
                     begin
                        for i := Cnt to Cnt2 - 1 do
                        begin
                           DeletePointByIndex (Cnt + 1);
                        end;
                        InsInd := Cnt + 1;
                        InsertPointByIndex (InsInd, C1);
                        Result := False;
                     end;
                  end;
               end;
            end;
         end
         else
         if ObjType = ot_CPoly then
            begin
               with Item^.Data^ do
               begin
                  for Cnt := 0 to Count - 2 do
                  begin
                     for Cnt2 := Cnt + 2 to Count - 4 do
                     begin
                        if (PInfo^.LineCut (True, PDPoint (At (Cnt))^, PDPoint (At (Cnt + 1))^,
                           PDPoint (At (Cnt2))^, PDPoint (At (Cnt2 + 1))^, C1)) then
                        begin
                           for i := Cnt + 1 to Cnt2 do
                           begin
                              DeletePointByIndex (Cnt + 1);
                           end;
                           InsInd := Cnt + 1;
                           InsertPointByIndex (InsInd, C1);
                           Result := False;
                        end;
                     end;
                  end;
               end;
            end;
      end;

      procedure DoCheckPoly2(Start,Stop: Integer);
      var
      i: Integer;
      C1: TDPoint;
      InsInd: Integer;
      P1,P2,P3: PDPoint;
      A1,A2: Double;
      begin
           for i:=Start to Stop-3 do begin
               P1:=Data^.At(i);
               P2:=Data^.At(i+1);
               P3:=Data^.At(i+2);

               A1:=P1^.CalculateAngle(P2^)*180/Pi;
               A2:=P3^.CalculateAngle(P2^)*180/Pi;

               if Abs(A2-A1) < 0.1 then begin
                  DeletePointByIndex(i+1);
               end;
           end;
      end;

   var Start: Integer;
      Stop: Integer;
      Island: Integer;
   begin
      ObjType := Item^.GetObjType;
      if (ObjType = ot_Poly) or (ObjType = ot_CPoly) then
      begin
         if Item^.GetState (sf_IslandArea) then
            with Item^ do
            begin
               Start := 0;
               for Island := 0 to IslandCount - 1 do
               begin
                  Stop := Start + IslandInfo^[Island];
                  Result := DoCheckPoly (Start, Stop);
                  DoCheckPoly2(Start,Stop);
                  Start := Stop;
               end;
            end
         else begin
            Result := DoCheckPoly (0, Item^.Data^.Count);
            DoCheckPoly2(0,Item^.Data^.Count);
         end;
      end;
   end;

begin
   Result := CheckPoly (@Self);
end;
}

function TPoly.GetCenter: TDPoint;
var
  PolyItem, SItem: PIndex;
  NObj, SObj: PView;
  Poly: PPoly;
  a, b, c, xbegin, ybegin, xend, yend, xnew, ynew: Extended;
  Cmd: string;
  Id, Length: Integer;
  cnt, i, k: Integer;
  Angle: Double;
begin
  Length := Trunc(Laenge * 50);
  Cnt := Data^.Count;
  for i := Cnt - 1 downto 0 do
  begin
    if (Laenge(i) * 100) > Length then
    begin
      xend := PDPoint(Data^.At(i))^.x;
      yend := PDPoint(Data^.At(i))^.y;
    end
    else
    begin
      break;
    end;
  end;
  if (Laenge(i) * 100) <= Length then
  begin
    xbegin := PDPoint(Data^.At(i))^.x;
    ybegin := PDPoint(Data^.At(i))^.y;
    Length := Length - Round(Laenge(i) * 100);
    a := xend - xbegin;
    b := yend - ybegin;
    c := Sqrt(a * a + b * b);
    if xbegin < xend then
    begin
      xnew := xbegin + Sqrt(c * c - b * b) * (length / c)
    end
    else
    begin
      xnew := xbegin - Sqrt(c * c - b * b) * (length / c);
    end;
    if ybegin < yend then
    begin
      ynew := ybegin + Sqrt(c * c - a * a) * (Length / c)
    end
    else
    begin
      ynew := ybegin - Sqrt(c * c - a * a) * (Length / c);
    end;
    Result.Init(Trunc(xnew), Trunc(ynew));
  end;
end;

procedure TPoly.Recreate(PInfo: PPaint);
var
  AData: PDPointCollection;
  L1P1, L1P2, L2P1, L2P2: PDPoint;
  C: TDPoint;
  i, k: Integer;
  CF: Boolean;
begin
  exit; { Not finished B282 }

  AData := New(PDPointCollection, Init(ciPoly, cePoly));
  try

    for i := 0 to Data^.Count - 1 do
    begin

      L1P1 := Data^.At(i);
      L1P2 := Data^.At(i + 1);

      CF := False;

      for k := i + 2 to Data^.Count - 1 do
      begin

        L2P1 := Data^.At(k);
        L2P2 := Data^.At(k + 1);

        if PInfo^.LineCut(True, L1P1^, L1P2^, L2P1^, L2P2^, C) then
        begin
          CF := True;
          Break;
        end;

      end;

      AData^.Insert(L1P1);
    end;

  finally
    Dispose(Data, Done);
    Data := AData;
  end;
end;

function TPoly.IsCrossed(PInfo: PPaint): Boolean;
var
  ObjType: Integer;

  function CheckPoly
      (
      Item: PCPoly
      )
      : Boolean;

    function DoCheckPoly
        (
        Start: Integer;
        Stop: Integer
        )
        : Boolean;
    var
      Cnt: Integer;
      Cnt2: Integer;
      i: Integer;
      C1: TDPoint;
    begin
      Result := TRUE;
      if ObjType = ot_Poly then
      begin
        with Item^.Data^ do
        begin
          for Cnt := Start to Stop - 2 do
          begin
            for Cnt2 := Cnt + 2 to Stop - 2 do
            begin
              if PInfo^.LineCut(TRUE, PDPoint(At(Cnt))^, PDPoint(At(Cnt + 1))^,
                PDPoint(At(Cnt2))^, PDPoint(At(Cnt2 + 1))^, C1) then
              begin
                Result := False;
                break;
              end;
            end;
          end;
        end;
      end
      else
        if ObjType = ot_CPoly then
        begin
          with Item^.Data^ do
          begin
            for Cnt := Start to Stop - 2 do
            begin
              for Cnt2 := Cnt + 2 to Stop - 3 do
              begin
                if PInfo^.LineCut(TRUE, PDPoint(At(Cnt))^, PDPoint(At(Cnt + 1))^,
                  PDPoint(At(Cnt2))^, PDPoint(At(Cnt2 + 1))^, C1) then
                begin
                  Result := False;
                  break;
                end;
              end;
            end;
          end;
        end;
    end;
  var
    Start: Integer;
    Stop: Integer;
    Island: Integer;
  begin
    ObjType := Item^.GetObjType;
    if (ObjType = ot_Poly) or (ObjType = ot_CPoly) then
    begin
      if Item^.GetState(sf_IslandArea) then
        with Item^ do
        begin
          Start := 0;
          for Island := 0 to IslandCount - 1 do
          begin
            Stop := Start + IslandInfo^[Island];
            Result := DoCheckPoly(Start, Stop);
            Start := Stop;
          end;
        end
      else
        Result := DoCheckPoly(0, Item^.Data^.Count);
    end;
  end;

begin
  Result := not CheckPoly(@Self);
end;

function TPoly.IsAngle180(AIndex: Integer): Boolean;
var
  GrPoint, GrPointL, GrPointR: TGrPoint;
  APoint: PDPoint;
begin
  Result := False;
  if (AIndex > 0) and (AIndex < Data^.Count - 1) then
  begin
    APoint := Data^.At(AIndex);
    GrPoint.X := APoint^.X;
    GrPoint.Y := APoint^.Y;

    APoint := Data^.At(AIndex - 1);
    GrPointL.X := APoint^.X;
    GrPointL.Y := APoint^.Y;

    APoint := Data^.At(AIndex + 1);
    GrPointR.X := APoint^.X;
    GrPointR.Y := APoint^.Y;

    Result := Abs(LineNormalDistance(GrPointL, GrPointR, GrPoint)) < 1.0;
  end;
end;

function TPoly.ReadWKT(WKTString: AnsiString): Boolean;
var
  i: Integer;
  Start, Stop: Integer;
  s: AnsiString;
  PointList: TList;
  APoint: PDPoint;
  x, y: Integer;
begin
  Result := False;
  Start := Pos('(', WKTString);
  Stop := StrLen(PChar(WKTString));
  s := '';
  PointList := TList.Create;
  try
    for i := Start to Stop do
    begin
      if ((WKTString[i] >= Char('0')) and (WKTString[i] <= Char('9'))) or (WKTString[i] = '.') or (WKTString[i] = Char('-')) then
      begin
        s := s + WKTString[i];
      end
      else
        if (WKTString[i] = ' ') or (WKTString[i] = ',') or (WKTString[i] = ')') then
        begin
          if s <> '' then
          begin
            PointList.Add(Pointer(Round(SToF(s) * 100)));
          end;
          s := '';
        end
        else
          if (WKTString[i] <> '(') then
          begin
            break;
          end;
    end;
    for i := 0 to PointList.Count - 1 do
    begin
      if (i mod 2) = 0 then
      begin
        x := Integer(PointList[i]);
      end
      else
      begin
        y := Integer(PointList[i]);
        APoint := New(PDPoint, Init(x, y));
        Data^.Insert(APoint);
      end;
    end;
    CalculateClipRect;
    Result := (not ClipRect.IsEmpty) and (Data^.Count >= 2);
  finally
    PointList.Free;
  end;
end;

function TPoly.ToWKT: AnsiString;
var
  s, xy: AnsiString;
  i, n: Integer;
  p: PDPoint;
begin
  Result := '';
  s := 'LINESTRING ((';
  n := Data^.Count;
  for i := 0 to n - 1 do
  begin
    p := Data^.At(i);
    xy := FloatToStr(p^.x / 100) + ' ' + FloatToStr(p^.y / 100);
    s := s + xy;
    if i < n - 1 then
    begin
      s := s + ', ';
    end;
  end;
  s := s + '))';
  Result := s;
end;

const
  RPoly: TStreamRec = (
    ObjType: rn_Poly;
    VmtLink: TypeOf(TPoly);
    Load: @TPoly.Load;
    Store: @TPoly.Store);

  RPolyOld: TStreamRec = (
    ObjType: rn_PolyOld;
    VmtLink: TypeOf(TPoly);
    Load: @TPoly.LoadOld;
    Store: @TPoly.Store);

  RSPoly: TStreamRec = (
    ObjType: rn_SPoly;
    VmtLink: TypeOf(TSPoly);
    Load: @TSPoly.Load;
    Store: @TSPoly.Store);

  RSPolyOld: TStreamRec = (
    ObjType: rn_SPolyOld;
    VmtLink: TypeOf(TSPoly);
    Load: @TSPoly.LoadOld;
    Store: @TSPoly.Store);

begin
  RegisterType(RPolyOld);
  RegisterType(RPoly);
  RegisterType(RSPolyOld);
  RegisterType(RSPoly);
end.

