�
 TPOLYPARTITIONDLG 0(  TPF0TPolyPartitionDlgPolyPartitionDlgLeft~Top� HelpContextBorderStylebsDialogCaption!1ClientHeight2ClientWidthiColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderOnClose	FormCloseOnCreate
FormCreateOnShowFormShowPixelsPerInch`
TextHeight 
TWGroupBoxLayerGroupBoxLeftTopWidthYHeight<Caption!28TabOrder  TLabel
DLayernameLeftTopWidth<HeightAutoSizeCaption!4  	TComboBoxDLayerLeft� TopWidth� Height
ItemHeightTabOrder    
TWGroupBoxModeGroupBoxLeftTopHWidthYHeight� Caption!29TabOrder TLabel	SAreanameLeftTopWidth	HeightCaption!5  TLabelSAreaLeft� TopWidthwHeight	AlignmenttaRightJustifyAutoSizeCaption0Color	clBtnFaceParentColor  TLabel	SAreaUnitLeft5TopWidthHeightAutoSizeCaptionm�Color	clBtnFaceParentColor  TLabel
DAreaUnit1Left5Top(WidthHeightAutoSizeCaptionm�Color	clBtnFaceParentColor  TLabelDArea1Left� Top(WidthwHeight	AlignmenttaRightJustifyAutoSizeCaption0Color	clBtnFaceDragModedmAutomaticParentColor  TLabel	DAreanameLeftTop(WidthHeightCaption!22  TLabelDArea2Left� Top<WidthwHeight	AlignmenttaRightJustifyAutoSizeCaption0Color	clBtnFaceDragModedmAutomaticParentColor  TLabel
DAreaUnit2Left5Top<WidthHeightAutoSizeCaptionm�Color	clBtnFaceParentColor  TLabelLabel1LeftTop<WidthHeightCaption!30  
TWGroupBox
WGroupBox2LeftTopXWidth� HeightaCaption!14TabOrder BoxStyle	bsTopLine TRadioButtonParallelRadioLeft
TopWidthoHeightCaption!15Checked	TabOrder TabStop	  TRadioButton	FreeRadioLeft
Top2WidthoHeightCaption!18TabOrder   	TCheckBox	FillCheckLeftTop� WidthoHeightCaption!13TabOrder  
TWGroupBox
WGroupBox3Left� TopXWidth� HeightcCaption!19TabOrderBoxStyle	bsTopLine TLabel
OffsetUnitLeft� Top4WidthHeightAutoSizeCaptionmColor	clBtnFaceParentColor  TLabel	AngleUnitLeft� TopWidthHeightAutoSizeCaption!12Color	clBtnFaceParentColor  TLabel	AngleNameLeftTopWidthHeightCaption!23  TLabelAreaUnitLeft� TopLWidthHeightAutoSizeCaptionm�Color	clBtnFaceParentColor  TRadioButton	DistRadioLeftTop2WidthPHeightCaption!20Checked	TabOrder TabStop	OnClickDistRadioClick  TRadioButton	AreaRadioLeftTopJWidthPHeightCaption!21TabOrderOnClickAreaRadioClick  TEdit
OffsetEditLeftSTop0WidthQHeightBiDiModebdLeftToRightParentBiDiModeTabOrder  TEdit	AngleEditLeftSTopWidthQHeightBiDiModebdLeftToRightParentBiDiModeTabOrder  TEditAreaEditLeftSTopHWidthQHeightBiDiModebdLeftToRightParentBiDiModeTabOrder
OnDragDropAreaEditDragDrop
OnDragOverAreaEditDragOver    TButtonInputButtonLeftTopWidthKHeightCaption!24TabOrderOnClickInputButtonClick  TButton
CalcButtonLeftZTopWidthKHeightCaption!25TabOrderOnClickCalcButtonClick  TButtonOKButtonLeft� TopWidthKHeightCaption!26ModalResultTabOrderOnClickOKButtonClick  TButtonCancelButtonLeftTopWidthKHeightCaption!27Default	ModalResultTabOrderOnClickCancelButtonClick  TMlgSection
MlgSectionSectionPolyPartDialogLeft� Top��    TFloatValidatorOffsetValidatorEdit
OffsetEditCommas	MaxLengthMaxValue     �7�@Left� Top��    TFloatValidatorAngleValidatorEdit	AngleEditCommasMinValue       ��	MaxLengthMaxValue       �@Left� Top��    TFloatValidatorAreaValidatorEditAreaEdit	AllowSignCommasMinValue hl��7��?	MaxLengthMaxValue  ��)焑*@ShowZeroValuesLeft� Top��     