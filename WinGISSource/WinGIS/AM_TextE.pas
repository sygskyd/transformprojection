{****************************************************************************}
{ Unit                                                                       }
{----------------------------------------------------------------------------}
{ Autor: Bramberger Michael                                                  }
{----------------------------------------------------------------------------}
{ Modifikationen                                                             }
{  14.06.1994 Michael Bramberger                                             }
{  24.10.1994 Martin Forst                                                   }
{****************************************************************************}
Unit AM_TextE;
{$D+,L+}
Interface

Uses Messages,SysUtils,WinTypes,WinProcs,AM_Def,AM_Text,AM_Font,AM_Paint,
     AM_Ini,Objects,AM_Dlg3,AM_Index,AM_Layer,ResDlg,Classes,Controls,UserIntf;

Const id_UseWith             = 141;
      id_NewText             = 143;

      id_NoAlign        = 129;
      id_Transparent    = 389;

Type PDTxtSet      = ^TDTxtSet;
     TDTxtSet      = Class(TDFont)
      Public
       UseWith     : PInteger;
       SelCnt      : LongInt;
       Text        : PShortString;
       Other       : PInteger;
       Constructor Init(AParent:TComponent;APInfo:PPaint;
                        AData:PFontData;AOther:PInteger;ASelCnt:LongInt;ToUseWith:PInteger;
                        AText:PShortString);
       Function    CanClose:Boolean; override;
       Procedure   IDFontList(var Msg:TMessage); message id_First+id_FontList;
       Procedure   SetupWindow; override;
       Procedure   UseWithChanged(var Msg : TMessage); message id_First + id_UseWith;
     end;

     Procedure ProcEditText(Project : Pointer);

Implementation

Uses AM_Proj, AttachHndl;   // Glukhov Bug#114 Build#125 23.11.00

Procedure ProcEditText(Project : Pointer);
  var SelCnt       : Integer;
      UseWith      : Integer;
      Assigned     : Integer;
      OtherStyle   : Integer;
      NFont        : TFontData;
      AText        : String;
      NewFont      : TFontData;
      FixedText    : Boolean;
      RedrawRect   : TDRect;
      PProject     : PProj;
  Function CollectFormats
     (
     AItem          : PIndex
     )
     : Boolean; Far;
    var Item        : PText;
    Procedure UpdateBit
       (
       ABit         : Integer
       );
      begin
        if Assigned and ABit=0 then begin
          if Item^.Font.Style and ABit<>0 then NFont.Style:=NFont.Style or ABit;
          Assigned:=Assigned or ABit;
        end
        else if (Item^.Font.Style and ABit)<>(NFont.Style and ABit) then OtherStyle:=OtherStyle or ABit;
      end;
    begin
      CollectFormats:=FALSE;
      Item:=Pointer(PProject^.Layers^.IndexObject(PProject^.PInfo,AItem));
      UpdateBit(ts_Italic);
      UpdateBit(ts_Bold);
      UpdateBit(ts_Underl);
      UpdateBit(ts_FixHeight);
      UpdateBit(ts_Transparent);
      if Assigned and ts_Align=0 then begin
        NFont.Style:=NFont.Style or (Item^.Font.Style and ts_Align);
        Assigned:=Assigned or ts_Align;
      end
      else if Item^.Font.Style and ts_Align<>NFont.Style and ts_Align then OtherStyle:=OtherStyle or ts_Align;
      if Assigned and ts_OtherHeight=0 then begin
        NFont.Height:=Item^.Font.Height;
        Assigned:=Assigned or ts_OtherHeight;
      end
      else if Item^.Font.Height<>NFont.Height then OtherStyle:=OtherStyle or ts_OtherHeight;
      if Assigned and ts_OtherFont=0 then begin
        NFont.Font:=Item^.Font.Font;
        Assigned:=Assigned or ts_OtherFont;
      end
      else if Item^.Font.Font<>NFont.Font then OtherStyle:=OtherStyle or ts_OtherFont;
      if Assigned and ts_OtherText=0 then begin
        AText:=PToStr(Item^.Text);
        Assigned:=Assigned or ts_OtherText;
      end
      else if PToStr(Item^.Text)<>AText then OtherStyle:=OtherStyle or ts_OtherText;
    end;
//++ Glukhov Bug#114 Build#125 23.11.00
  Function UpdateText( AItem: PIndex ): Boolean; Far;
    var
      Item      : PText;
      OldRect   : TDRect;
    Procedure UpdateBits( AMask: Integer );
//-- Glukhov Bug#114 Build#125 23.11.00
      begin
        if OtherStyle and AMask<>0 then begin
          NewFont.Style:=(NewFont.Style and not AMask)
              or (NFont.Style and AMask);
        end;
      end;
    begin
      UpdateText:=FALSE;
      if PProject^.Layers^.IsObjectFixed(AItem) then FixedText:=TRUE
      else begin
        Item:=Pointer(PProject^.Layers^.IndexObject(PProject^.PInfo,AItem));
        NewFont:=Item^.Font;
        UpdateBits(ts_Italic);
        UpdateBits(ts_Bold);
        UpdateBits(ts_Underl);
        UpdateBits(ts_FixHeight);
        UpdateBits(ts_Transparent);
        if OtherStyle and ts_Align<>0 then NewFont.Style:=(NewFont.Style and not ts_Align)
            or (NFont.Style and ts_Align);
        if OtherStyle and ts_OtherHeight<>0 then NewFont.Height:=NFont.Height;
        if OtherStyle and ts_OtherFont<>0 then NewFont.Font:=NFont.Font;
        if OtherStyle and ts_OtherText=0 then AText:=PToStr(Item^.Text);
        if (AText<>PToStr(Item^.Text))
            or not NewFont.IsEqual(Item^.Font) then begin
          OldRect.InitByRect( Item.ClipRect );
//++ Glukhov Bug#468 Build#162 01.07.01
//          Item^.SetNewText( PProject.PInfo, AText, NewFont, True, False );
          Item^.SetNewText( PProject.PInfo, AText, NewFont );
//-- Glukhov Bug#468 Build#162 01.07.01
          UpdateWithAllAttached( PProject, Item, OldRect, RedrawRect );
        end;
      end;
    end;
  begin
    PProject := PProj(Project);
    PProject^.SetCursor(crHourGlass);
    AText:='';
    SelCnt:=0;
    Assigned:=0;
    NFont.Init;
    OtherStyle:=0;
    FixedText:=FALSE;
    if PProject^.Layers^.SelLayer^.HasObjectType(ot_Text) then
      begin
      PProject^.Layers^.SelLayer^.FirstObjectType(ot_Text,@CollectFormats);
      SelCnt:=1;
      end
    else if PProject^.Layers^.TopLayer^.HasObjectType(ot_Text) then
      begin
      PProject^.Layers^.TopLayer^.FirstObjectType(ot_Text,@CollectFormats);
      end
    else if PLayer(PProject^.PInfo^.Objects)^.HasObjectType(ot_Text) then
      begin
      PLayer(PProject^.PInfo^.Objects)^.FirstObjectType(ot_Text,@CollectFormats);
      end
    else MsgBox(PProject^.Parent.Handle,11020,mb_OK or mb_IconInformation,'');
    PProject^.SetCursor(crDefault);
    ExecDialog(TDTxtSet.Init(PProject^.Parent,PProject^.PInfo,@NFont,@OtherStyle,
        SelCnt,@UseWith,@AText));
    if SelCnt=0 then UseWith:=UseWith+1;
    if UseWith<=2 then begin
      PProject^.SetCursor(crHourGlass);
      NewFont.Init;
      RedrawRect.Init;
      case UseWith of
        0 : PProject^.Layers^.SelLayer^.FirstObjectType(ot_Text,@UpdateText);
        1 : PProject^.Layers^.TopLayer^.FirstObjectType(ot_Text,@UpdateText);
        2 : PLayer(PProject^.PInfo^.Objects)^.FirstObjectType(ot_Text,@UpdateText);
      end;
      PProject^.SetCursor(crDefault);
      PProject^.PInfo^.FromGraphic:=TRUE;
      PProject^.PInfo^.RedrawRect(RedrawRect);
      if FixedText then MsgBox(PProject^.Parent.Handle,11008,mb_OK or mb_IconInformation,'');
    end;
  end;

Constructor TDTxtSet.Init
   (
   AParent         : TComponent;
   APInfo          : PPaint;
   AData           : PFontData;
   AOther          : PInteger;
   ASelCnt         : LongInt;
   ToUseWith       : PInteger;
   AText           : PShortString
   );
  begin
    inherited Init(AParent,APInfo,AData,'TEXT',FALSE);
    UseWith:=ToUseWith;
    SelCnt:=ASelCnt;
    Text:=AText;
    Other:=AOther;
    HelpContext:=2030;
  end;

Procedure TDTxtSet.SetupWindow;
  Procedure DoAll
     (
     Item          : PFontDes
     ); Far;
    var AName      : String;
    begin
      AName:=Item^.Name^+#0;
      SendDlgItemMsg(126,lb_AddString,0,LongInt(@AName[1]));
    end;
  Procedure CheckTheButton
     (
     AButton       : Integer;
     AStyle        : Integer
     );
    begin
      if Other^ and AStyle<>0 then CheckDlgButton(Handle,AButton,2);
    end;
  var TmpText      : Array[0..255] of Char;
  begin
    inherited SetupWindow;
    if SelCnt<>0 then begin
      GetLangPChar(413,TmpText,SizeOf(TmpText));
      SendDlgItemMsg(id_UseWith,cb_AddString,0,LongInt(@TmpText));
    end;
    GetLangPChar(414,TmpText,SizeOf(TmpText));
    SendDlgItemMsg(id_UseWith,cb_AddString,0,LongInt(@TmpText));
    GetLangPChar(415,TmpText,SizeOf(TmpText));
    SendDlgItemMsg(id_UseWith,cb_AddString,0,LongInt(@TmpText));
    SendDlgItemMsg(id_UseWith,cb_SetCurSel,0,0);
    if Other^ and ts_OtherText<>0 then SetDlgItemText(Handle,id_NewText,NIL)
    else SetFieldText(id_NewText,Text^);
    EnableWindow(GetItemHandle(id_NewText),SelCnt>0);
    CheckTheButton(id_Italic,ts_Italic);
    CheckTheButton(id_Bold,ts_Bold);
    CheckTheButton(id_Underl,ts_Underl);
    CheckTheButton(id_FixHeight,ts_FixHeight);
    if Other^ and ts_Transparent<>0 then CheckDlgButton(Handle,id_Transparent,2)
    else if Data^.Style and ts_Transparent<>0 then CheckDlgButton(Handle,id_Transparent,1);
    if Other^ and ts_Align<>0 then CheckRadioButton(Handle,id_NoAlign,id_Right,id_NoAlign);
    if Other^ and ts_OtherHeight<>0 then SetDlgItemText(Handle,id_FixList,NIL);
    if Other^ and ts_OtherFont<>0 then SetDlgItemText(Handle,id_FontList,NIL);
  end;

Procedure TDTxtSet.UseWithChanged
   (
   var Msg         : TMessage
   );
  begin
    if Msg.lParamHi=cbn_SelChange then begin
      if (SelCnt>0)
          and (SendDlgItemMsg(id_UseWith, cb_GetCurSel, 0, 0) = 0) then
        EnableWindow(GetItemHandle(id_NewText), TRUE)
      else EnableWindow(GetItemHandle(id_NewText), FALSE);
    end;
  end;

Function TDTxtSet.CanClose
   : Boolean;
  var AHeight  : LongInt;
  begin
    CanClose:=FALSE;
    if not GetFieldLongZero(id_FixList,AHeight) then
      begin
      MsgBox(Handle,210,mb_IconExclamation or mb_Ok,'');
      SetFocus(GetItemHandle(id_FixList));
      end
    else
      begin
      SendMessage(Handle,wm_Command,id_FontList,MakeLong(GetItemHandle(id_FontList),cbn_KillFocus));
      GetFontStyle(Data^);
      Other^:=0;
      if IsDlgButtonChecked(Handle,id_Italic)<>2 then Other^:=Other^ or ts_Italic;
      if IsDlgButtonChecked(Handle,id_Bold)<>2 then Other^:=Other^ or ts_Bold;
      if IsDlgButtonChecked(Handle,id_Underl)<>2 then Other^:=Other^ or ts_Underl;
      if IsDlgButtonChecked(Handle,id_FixHeight)<>2 then Other^:=Other^ or ts_FixHeight;
      if IsDlgButtonChecked(Handle,id_Transparent)<>2 then Other^:=Other^ or ts_Transparent;
      if IsDlgButtonChecked(Handle,id_Transparent)=1 then Data^.Style:=Data^.Style or ts_Transparent;
      if IsDlgButtonChecked(Handle,id_NoAlign)=0 then Other^:=Other^ or ts_Align;
      if AHeight<>0 then Other^:=Other^ or ts_OtherHeight;
      UseWith^:=SendDlgItemMsg(id_UseWith,cb_GetCurSel,0,0);
      Text^:=GetFieldText(id_NewText);
      if (SelCnt>0)
          and (SendDlgItemMsg(id_UseWith,cb_GetCurSel,0,0)=0)
          and (Text^<>'') then
        Other^:=Other^ or ts_OtherText;
      if GetFieldText(id_FontList)<>'' then Other^:=Other^ or ts_OtherFont;
      CanClose:=TRUE;
      end
  end;

Procedure TDTxtSet.IDFontList
   (
   var Msg         : TMessage
   );
  var AText        : Array[0..255] of Char;
      ASel         : Integer;
  begin
    if (Msg.lParamHi=cbn_KillFocus) then begin
      GetDlgItemText(Handle,id_FontList,AText,SizeOf(AText));
      if StrComp(AText,'')<>0 then begin
        ASel:=SendDlgItemMsg(id_FontList,cb_FindString,-1,LongInt(@AText));
        if ASel<0 then ASel:=0;
        SendDlgItemMsg(id_FontList,cb_SetCurSel,ASel,0);
      end;
    end;
  end;

end.
