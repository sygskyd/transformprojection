Unit DigIntf;

Interface
{$IFNDEF AXDLL} // <----------------- AXDLL
Uses Forms, AM_Proj,AM_WTab,Classes,Controls,Messages,WinTypes,WinProcs,
  SerPort;

Const erLoadLib    = 3;
      erBadId      = 5;
      erBaudRate   = 6;
      erByteSize   = 7;
      erDefault	   = 8;
      erHardware   = 9;
      erMemory     = 10;
      erNOpen	   = 11;
      erOpen	   = 12;
      erBreak      = 13;
      erTimeOut    = 14;
      erOverrun    = 15;
      erQueueOver  = 16;
      erFrame      = 17;
      erParity     = 18;
      erInitDev    = 19;

      bsInBuffer   = 10000;
      bsOutBuffer  = 100;

      bsReadComm   = 100;

      idt_Serial   = 0;
      tvSerial     = 100;
type
  TDigForm = class(TForm)
  private
    { Private declarations }
  public
    { Public declarations }
  end;



function ExtractStringItems(var D: Array of String; S: AnsiString; Sep: Char): Integer;

Type TInitDriver   = Function(CID,HWindow:Integer):Integer; stdcall;
     TReceiveChar  = Function(CID:Integer;AChar:Char;var XPos,YPos:Real):Integer; stdcall;
     TDoneDriver   = Function:Integer; stdcall;

     TDigitizer         = Class
      Private
       FWindow          : TWinControl;
       FProject         : PProj;
       InitDriver       : TInitDriver;
       ReceiveChar      : TReceiveChar;
       DoneDriver       : TDoneDriver;

       LibHandle        : THandle;
       SerLibHandle     : THandle; //Handle of serport.dll
       CommID           : Integer; //ComPortHandle
       IsWinTAB         : Boolean;
       WinTAB           : PWinTAB;
       IsActive         : Boolean;
       Function    DoneDrv:Boolean;
       Function    DoneLibrary:Boolean;
       Function    DonePort:Boolean;
       Function    Error(ErNum:Integer):Integer;
       Function    HandleChar(AChar:Char):Integer;
       Function    HandleSerial:Integer;
       Function    GetActiveDigitizer:String;
       Function    InitDrv:Boolean;
       Function    InitLibrary:Boolean;
       Function    InitPort:Boolean;
       Function    StartDigit:Boolean;
      Public
       Constructor Create;
       Destructor  Destroy; override;
       Property    ActiveDigitizer:String read GetActiveDigitizer;
       Function    ActivateDigitizer(Project:PProj):Boolean;
       Procedure   DeActivateDigitizer;
       Function    EndDigit:Boolean;
       Procedure   WinTabMessage(var Msg:TMessage);
     end;

var Digitizer      : TDigitizer;
{$ENDIF} // <----------------- AXDLL

Implementation

{$IFNDEF AXDLL} // <----------------- AXDLL

{$R *.DFM}

Uses AM_Def,AM_Digit,AM_Ini,Dialogs,MultiLng,SysUtils, AM_Main;

Type TDigitizerWindow = Class(TWinControl)
      Protected
       Procedure   CreateParams(var Params:TCreateParams); override;
       Procedure   WMTimer(var Msg:TWMTimer); message wm_Timer;
       Procedure   WMWTPacket(var Msg:TMessage); message wm_WT_Packet;
     end;



Constructor TDigitizer.Create;
  begin
    inherited Create;
    FWindow:=TDigitizerWindow.Create(NIL);
    LibHandle:=0;
    CommId:=-1;
    WinTAB:=nil;
    IsWinTAB:=FALSE;
  end;

Destructor TDigitizer.Destroy;
  begin
    IsActive:=FALSE;
    FWindow.Free;
    if IsWinTAB then IsWinTAB:=FALSE
    else begin
      DoneDrv;
      DonePort;
      DoneLibrary;
    end;
    if WinTAB<>NIL then Dispose(WinTAB,Done);
  end;

Function TDigitizer.ActivateDigitizer
   (
   Project         : PProj
   )
   : Boolean;
  begin
    FProject:=Project;
    if not IsActive then StartDigit;
    Result:=IsActive;
  end;

Procedure TDigitizer.DeActivateDigitizer;
  begin
    if IsActive then EndDigit;
    FProject:=NIL;
  end;

Function TDigitizer.InitLibrary
   : Boolean;
  var LibH         : THandle;
      OldErrorMode : Word;
  begin
    InitLibrary:=FALSE;
    LibHandle:=0;
    OldErrorMode:=SetErrorMode($8000{=SEM_NOOPENFILEERRORBOX});
    LibH:=LoadLibrary(IniFile^.DigitData.LibName);
    WingisMainForm.CheckLibStatus(LibH,ExtractFileName(IniFile^.DigitData.LibName),'Digitizer');
    SetErrorMode(OldErrorMode);
    if LibH=0 then Error(erLoadLib)
    else begin
      LibHandle:=LibH;
      @InitDriver:=GetProcAddress(LibHandle,'InitDriver');
      @ReceiveChar:=GetProcAddress(LibHandle,'ReceiveChar');
      @DoneDriver:=GetProcAddress(LibHandle,'DoneDriver');
      if (@InitDriver=NIL) or (@ReceiveChar=NIL) or (@DoneDriver=NIL) then Error(erLibFormat)
      else InitLibrary:=TRUE;
    end;
  end;

Function TDigitizer.Error
   (
   ErNum           : Integer
   )
   : Integer;
  var HadTimer     : Boolean;
  begin
    HadTimer:=KillTimer(FWindow.Handle,idt_Serial);
    if ErNum=erLoadLib then begin
      MessageDialog(Format(GetLangText(7003),[StrPas(IniFile^.DigitData.LibName),
        SystemErrorText]),mtError,[mbOK],0);
      Result:=id_OK;
    end
    else Result:=MsgBox(FProject^.Parent.Handle,7000+ErNum,mb_OK or mb_IconExclamation,
        StrPas(IniFile^.DigitData.PortName));
    if HadTimer then SetTimer(FWindow.Handle,idt_Serial,tvSerial,NIL);
  end;

Function TDigitizer.InitPort
   : Boolean;
  var RetVal       : Integer;
      DCB          : TDCB;
      digparam     : Array of String;
      comopen       :function(parameter: PChar; inbuffsize: integer; ourbuffsize: integer): integer;
  begin
    with IniFile^.DigitData do begin
      setlength(digparam,9);
      if ExtractStringItems(digparam,stringreplace(portdef,':',',',[rfReplaceall]),',' ) > 4 then
        begin
         SerLibHandle:= Loadlibrary(pchar('serport.dll'));
         WingisMainForm.CheckLibStatus(SerLibHandle,'serport.dll','Digitizer');
         if SerLibHandle > 0 then
          begin
          @comopen:=getprocaddress(SerLibHandle,pchar('openc'));
          if @comopen <> NIL then
                 begin
                 RetVal:=comopen(portdef,bsInBuffer,bsOutBuffer);
                 if RetVal<= 0 then Error(erOpen) else CommID:=RetVal;
                 end
             else Error(erDefault);
          end
          else begin
                showmessage('Serport.dll not found!');
                RetVal:=0;
               end; 
        end;
    end;
  Result:=RetVal>0;
  end;

Function TDigitizer.DonePort
   : Boolean;
var comclose :function: integer;
  begin
    if SerLibHandle>0 then begin
         @comclose:=getprocaddress(SerLibHandle,pchar('closec')); //WAI  DigIntfForm.DigSerialPort.close;
         if @comclose <> Nil then comclose;
         FreeLibrary(SerLibHandle);
         end;
    SerLibHandle:=0;
    CommId:=-1;
    Result:=TRUE;
  end;

Function TDigitizer.HandleChar
   (
   AChar           : Char
   )
   : Integer;
  var RetVal       : Integer;
      XPos         : Real;
      YPos         : Real;
      AInfo        : PDigitInfo;
  begin
    if FProject<>NIL then begin
      RetVal:=TReceiveChar(ReceiveChar)(CommId,AChar,XPos,YPos);
      case RetVal of
        diAnyError  : Error(erAny);
        diInvData   : Error(erInvData);
        diPosition,
        diClear,
        diCancel,
        diSnap,
        diEndPoly,
        diPoint,
        diPoly,
        diCPoly     : begin
          AInfo:=New(PDigitInfo);
          AInfo^.Action:=RetVal;
          AInfo^.XPos:=XPos;
          AInfo^.YPos:=YPos;
          PostMessage(FProject^.Parent.Handle,wm_Digitize,0,LongInt(AInfo));
        end;
      end;
      RetVal:=diOK;
    end
    else RetVal:=diOK;
    HandleChar:=RetVal;
  end;

Function TDigitizer.InitDrv
   : Boolean;
  begin
    if InitDriver(CommID,FProject^.Parent.Handle)=diOK then InitDrv:=TRUE
    else begin
      Error(erInitDev);
      InitDrv:=FALSE;
    end;
  end;

Function TDigitizer.DoneDrv
   : Boolean;
  begin
    if LibHandle<>0 then DoneDrv:=TDoneDriver(DoneDriver)=diOK
    else DoneDrv:=TRUE;
  end;

Function TDigitizer.DoneLibrary
   : Boolean;
  begin
    if LibHandle<>0 then FreeLibrary(LibHandle);
    DoneLibrary:=TRUE;
    LibHandle:=0;
  end;

Function TDigitizer.HandleSerial
   : Integer;
  var ReadBuffer   : PChar;
      Copied       : Integer;
      AError       : DWord;
      AComStat     : TComStat;
      Cnt          : Integer;
      comread      :function(Readbuffer: PChar): integer;
  begin
    HandleSerial:=diAnyError;
    AError:=0;
    ClearCommError(CommID,AError,@AComStat);
    // wai: Read comm buffer here!!!
    Readbuffer:= StrAlloc(bsInBuffer);
    @comread:=Getprocaddress(SerLibHandle,pchar('readc'));
    if @comread<> Nil then
         Copied:= comread(ReadBuffer);          //DigIntfForm.DigSerialPort.Read(ReadBuffer, DigIntfForm.DigSerialPort.InBufferSize);

    if Copied<=0 then begin
      case AError of
        ce_Break    : Error(erBreak);
        ce_Frame    : Error(erFrame);
        ce_Overrun  : Error(erOverrun);
        ce_RXOver   : Error(erQueueOver);
        ce_RXParity : Error(erParity);
        else AError:=0;
      end;
    end
    else begin
      for Cnt:=0 to Copied-1 do HandleChar(ReadBuffer[Cnt]);
      HandleSerial:=diOK;
    end;
   StrDispose(Readbuffer);
  end;

Function TDigitizer.StartDigit
   : Boolean;
  begin
    Result:=FALSE;
    if StrComp(IniFile^.DigitData.DefInput,'')=0 then Error(erNoDev)
    else begin
      if IniFile^.DigitData.WinTAB then begin
        IsWinTAB:=TRUE;
        WinTAB:=new(PWinTAB,Init(FProject^.Parent));
        if WinTAB=nil then MsgBox(FProject^.Parent.Handle,7001,MB_OK or MB_IconExclamation,'');
        if WinTAB^.Load then begin
          WinTAB^.Active:=TRUE;
          if WinTAB^.Activate then StartDigit:=TRUE;
        end
      end
      else begin
        IsWinTAB:=FALSE;
        Result:=InitLibrary and InitPort and InitDrv;
        if not Result then begin
          DoneDrv;
          DonePort;
          DoneLibrary;
        end;
      end;
    end;
    if Result then SetTimer(FWindow.Handle,idt_Serial,tvSerial,NIL);
    IsActive:=Result;
  end;

Function TDigitizer.EndDigit
   : Boolean;
  var BResult       : Boolean;
  begin
    if IsActive then begin
      KillTimer(FWindow.Handle,idt_Serial);
      if IsWinTAB then begin
        EndDigit:=TRUE;
        if WinTAB<>nil then begin
          EndDigit:=WinTAB^.EndIt;
          Dispose(WinTAB,Done);
          WinTAB:=nil;
        end;
      end
      else begin
        BResult:=DoneDrv;
        BResult:=DonePort and BResult;
        EndDigit:=DoneLibrary and BResult;
      end;
      IsActive:=FALSE;
    end;
  end;

Procedure TDigitizerWindow.WMWTPacket
   (
   var Msg         : TMessage
   );
  begin
    Digitizer.WinTabMessage(Msg);
  end;

Procedure TDigitizer.WinTabMessage
   (
   var Msg         : TMessage
   );
  var APacket      : Packet;
      DInfo        : PDigitInfo;
  begin
    if (WinTab<>NIL) and WinTab^.WinTabLoaded
        and TWTPacketsGet(WinTab^.WTPacketsGet)(WinTab^.DefCtx,1,@APacket) then
      begin
      {old method is not working in NT  //if not {TWTPacket(WinTAB^.WTPacket)(Msg.lParam,Msg.wParam,@APacket)} 
       if HiWord(APacket.pkButtons)=2 then begin
        DInfo:=new(PDigitInfo);
        DInfo^.XPos:=APacket.pkX;
        DInfo^.YPos:=APacket.pkY;
        case LoWord(APacket.pkButtons) of
          0 : DInfo^.Action:=diPosition;
          1 : DInfo^.Action:=diEndPoly;
          2 : DInfo^.Action:=diCancel;
          3 : DInfo^.Action:=diCancel;
          else DInfo^.Action:=diOK;
        end;
        PostMessage(FProject^.Parent.Handle,wm_Digitize,0,LongInt(DInfo));
      end;
    end;  
  end;

Procedure TDigitizerWindow.WMTimer
   (
   var Msg         : TWMTimer
   );
  begin
  if Digitizer.WinTAB=Nil then Digitizer.HandleSerial;
  end;

Function TDigitizer.GetActiveDigitizer
   : String;
  begin
    Result:=StrPas(IniFile^.DigitData.DefInput);
  end;

Procedure TDigitizerWindow.CreateParams
   (
   var Params      : TCreateParams
   );
  begin
    inherited CreateParams(Params);
    Params.Style:=0;
  end;

Procedure ExitProc; Far;
  begin
    Digitizer.Free;
  end;

function ExtractStringItems(var D: Array of String; S: AnsiString; Sep: Char): Integer;
var
di,si,n: Integer;
Temp: String;
begin
     di:=0;
     Temp:='';
     n:=Length(S)+1;
     for si:=1 to n do begin
          if (S[si] = Sep) or (si = n) then begin
             if Length(Temp) > 0 then begin
                  D[di]:=Temp;
                  Inc(di);
             end;
             Temp:='';
          end
          else Temp:=Temp+Char(S[si]);
     end;
     Result:=di;
end;

Initialization
  begin
    Digitizer:=TDigitizer.Create;
    AddExitProc(ExitProc);
  end;
{$ENDIF} // <----------------- AXDLL
end.
