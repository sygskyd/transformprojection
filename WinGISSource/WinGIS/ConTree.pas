unit Contree;

interface
uses Classes;
type
     PCTreeBaseItem = ^TCTreeBaseItem;
     TCTreeBaseItem = class;
     TCBaseTree = class
      private
       FHead       : TCTreeBaseItem;
       FTail       : TCTreeBaseItem;
       pmin        : TCTreeBaseItem;
       pmax        : TCTreeBaseItem;
       MinNode     : TCTreeBaseItem;
       MaxNode     : TCTreeBaseItem;
       Function    InsertTree(var p:TCTreeBaseItem;key:TCTreeBaseItem;father:PCTreeBaseItem):Boolean;
      public
        FRoot       : TCTreeBaseItem;
       Constructor Create;
       Procedure   SetHead;
       Procedure   SetTail;
       Function    SeekItem(var p:TCTreeBaseItem;key:TCTreeBaseItem):TCTreeBaseItem;
       Procedure   TraverseTree(p:TCTreeBaseItem); virtual;
       Property    Root:TCTreeBaseItem read FRoot write FRoot;
       Function    InsertInTree(key:TCTreeBaseItem):Boolean;
       Procedure   Delete(var Item:TCTreeBaseItem);
       Procedure   DisposeTree(p:TCTreeBaseItem);
       Procedure   Traverse; virtual;
       Function    GetHead:TCTreeBaseItem;
       Function    OnCompareItems(key1,key2:Pointer):Integer; virtual; abstract;
       Function    Key1Of(Item:Pointer):Pointer; virtual; abstract;
       Function    Key2Of(Item:Pointer):Pointer; virtual; abstract;
       Function    ResultZero(p,k:TCTreeBaseItem):Boolean; virtual; abstract;
       Property    Head:TCTreeBaseItem read FHead write FHead;
       Property    Tail:TCTreeBaseItem read FTail write FTail;
       Procedure   Print;
       Procedure   Save;virtual;abstract;
       Destructor  Destroy; override;
     end;

     TCTreeBaseItem = class
      private
      public
       father     : PCTreeBaseItem;
       Left       : TCTreeBaseItem;
       Right      : TCTreeBaseItem;
       Next       : TCTreeBaseItem;  {list links}
       Prev       : TCTreeBaseItem;
       Procedure SaveNode; virtual; abstract;
       Procedure Print; virtual; abstract;
       Constructor Init(key:Pointer); virtual; abstract;
     end;

     TCTreeBaseItemRef = class of TCTreeBaseItem;

implementation
uses Area,SegTree;                                   {*************************}

Constructor TCBaseTree.Create;
  begin
    inherited Create;
    MinNode:=TCTreeBaseItem.create;
    MaxNode:=TCTreeBaseItem.Create;
  end;

Function TCBaseTree.GetHead : TCTreeBaseItem;
  begin
    Result:=Head;
  end;

Procedure TCBaseTree.DisposeTree                                                     {???}
   (
   p:TCTreeBaseItem
   );
  var h: TCTreeBaseItem;
  begin
    if p<>NIL then begin
      DisposeTree(p.Left);
      h:=p.Right;
      p.Free;
      DisposeTree(h);
    end;
  end;

Destructor TCBaseTree.Destroy;
  var p   : TCTreeBaseItem;
  begin
    if Root<>NIL then DisposeTree(Root);
    Root:=NIL;
    MinNode.Free;
    MaxNode.Free;
    inherited Destroy;
  end;

Function TCBaseTree.InsertInTree
   (
   key:TCTreeBaseItem
   )
   :Boolean;
  begin
    pmin:=MinNode;
    pmax:=MaxNode;
    Result:=InsertTree(FRoot,key,@FRoot); {Vater zeigt auf sich selbst}
    {SetHead;}                  {Erst nach vollstaendiegem Aufbau}
  end;

Procedure TCBaseTree.SetHead;
  var p: TCTreeBaseItem;
  begin
    p:=FRoot;
    while p.left<>NIL do p:=p.left;
    Head:=p;
    Head.prev:=NIL;
  end;

Procedure TCBaseTree.SetTail;
  var p: TCTreeBaseItem;
  begin
    p:=FRoot;
    while p.Right<>NIL do p:=p.Right;
    Tail:=p;
    Tail.next:=NIL;
  end;

Function TCBaseTree.InsertTree
   (
   var p          : TCTreeBaseItem;
       key        : TCTreeBaseItem;
       father     : PCTreeBaseItem
   )
   :Boolean;
  var Compare : Integer;
      Ref     : TCTreeBaseItemRef;
  begin
    if p<>NIL then begin
      Compare:=OnCompareItems(Key1Of(p),Key2Of(key));  {Vergleich key mit p    key<p : -1;}
      if Compare = -1 then begin                                              {key>p :  1;}
        pmax:=p;
        Result:=InsertTree(p.Left,key,@p.left);
      end
      else if Compare = 1 then begin
        pmin:=p;
        Result:=InsertTree(p.Right,key,@p.right);
      end
      else if Compare = 0 then begin
        Result:=ResultZero(p,key);
      end;
    end
    else begin
      p:=key;
      p.prev:=pmin;
      p.next:=pmax;
      pmin.next:=p;
      pmax.prev:=p;
      p.father:=father;

      if p.prev=MinNode then begin
        p.prev:=NIL;
        Head:=p;
      end;
      if p.next=MaxNode then begin
        p.next:=NIL;
        Tail:=p;
      end;
      Result:=TRUE;
    end;
  end;

Function TCBaseTree.SeekItem
   (
   var p          : TCTreeBaseItem;
       key        : TCTreeBaseItem
   )
   :TCTreeBaseItem;
  var Compare : Integer;
  begin
    if p<>NIL then begin
    Compare:=OnCompareItems(Key1Of(p),Key2Of(key));
      if Compare <=0 then begin
        if p.left<>NIL then Result:=SeekItem(p.Left,key) else
        Result:=p;
      end
      else if Compare = 1 then begin
        if p.right<>NIL then Result:=SeekItem(p.Right,key) else
        Result:=p;
      end;
    end else Result:=NIL;  {p<>NIL}
  end;


Procedure TCBaseTree.Traverse;
  begin
    TraverseTree(FRoot);
  end;

Procedure TCBaseTree.TraverseTree
   (
   p:TCTreeBaseItem
   );
  begin
    if p<>NIL then begin
      TraverseTree(p.left);
      p.SaveNode;
      TraverseTree(p.Right);
    end;
  end;

Procedure TCBaseTree.Delete
   (
    var Item: TCTreeBaseItem
   );
  var father: TCTreeBaseItem;
  var help : TCTreeBaseITem;
      son  : TCTreeBaseItem;
  begin
    If Item=NIL then raise ElistError.Create('Item = NIL ');
    If Item.prev<>NIL then Item.prev.next:=Item.next;
    If Item.next<>NIL then Item.next.prev:=Item.prev;

    if Item.father = nil then begin
      help:=Item;
      help.Free;
    end
    else if Item.right=NIL then begin
      help:=Item;
      {Item:=Item.left;}

      Item.father^:=Item.left;
      if help.left<>NIL then help.left.father:=help.father;
      help.Free;
    end
    else if Item.left=NIL then begin
      help:=Item;
      {Item:=Item.right;}
      Item.father^:=Item.Right;
      if help.Right<>NIL then help.Right.father:=help.father;
      help.Free;
    end
    else begin     {Neither Subtree is empty}
      if Item.next=NIL then raise ElistError.Create('Fehler in Delete!');
      help:=Item.next;
      help.left:=Item.left;
      if Item.left<>NIL then Item.left.father:=@help.left;
      help:=Item;
      {Item:=Item.Right;}
      Item.father^:=Item.Right;
      if help.Right<>NIL then help.Right.father:=help.father;
      help.Free
    end;
  end;


  (*
Procedure TCBaseTree.Delete
   (
   Item: TCTreeBaseItem
   );
  var father: TCTreeBaseItem;
  var help : TCTreeBaseITem;
      son  : TCTreeBaseItem;
  begin

    if (Item.left<>NIL)and(Item.Right<>NIL) then begin
      {2 Soehne}
      {raise EListError.Create('2 Sons');}
      Son:=Item.next;
      if (Son.left=NIL)and(Son.Right=NIL) then begin
        {Blatt}
        if Son.father.right=Son then Son.father.right:=NIL else
        if Son.father.left =Son then Son.father.left :=NIL;
        Son.prev:=Item.prev;
        Son.next:=Item.next;
        Son.father:=Item.father;
        Son.left:=Item.left;
        Son.Right:=Item.Right;
        Item.Free;
      end else begin
        {Node}
      end;
    end else begin
      If Item.prev<>NIL then Item.prev.next:=Item.next;
      If Item.next<>NIL then Item.next.prev:=Item.prev;
      If Item.left<>NIL then Son:=Item.left else
      If Item.right<>NIL then Son:=Item.right else
      Son:=NIL;
      if Item.father<>NIL then begin
        {}
        father:=Item.father;
        if father.left<>NIL then begin
          father.left:=Son;
          if Son<>NIL then Son.father:=father;
        end else
        if father.right<>NIL then begin
          father.right:=son;
          if Son<>NIL then Son.father:=father;
        end  else raise ElistError.Create(' F1 ');
      end else begin
        {Root}
        If Son<>NIL then Son.father:=NIL;
        Root:=Son;
      end;
      Item.Free;
    end;
  end;

    *)
Procedure TCBaseTree.Print;
  var p : TCTreeBaseItem;
      f : TextFile;
  begin
    p:=Head;
    while p<>NIL do begin
      p.Print;
      p:=p.next;
    end;
  end;


end.
