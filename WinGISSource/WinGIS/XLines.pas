{******************************************************************************+
  Module XLines
--------------------------------------------------------------------------------
  Author: Martin Forst
--------------------------------------------------------------------------------
  XLine definition and lists used by WinGIS.
--------------------------------------------------------------------------------
  Todo:
  - Error-checking and -handling for file-interface-Functions
+******************************************************************************}

Unit XLines;
 
Interface

Uses AM_Def,Windows,Classes,Graphics,Hash,IniFiles,Lists,RegDB,XStyles;

{$ALIGN ON}

Type {*************************************************************************+
       Class TXLineStyle
     ---------------------------------------------------------------------------
       Datastructure for an extended line-style.
     ---------------------------------------------------------------------------
       Add = adds a line-style to the list
       Clear = clears and frees the list of line-styles
       Count = number of line-styles in the list
       Data = pointer to the line-style definition, used for XLine-Functions
       Items = array of line-style-entries
       Name = name of the line-style
       Number = number of the line-style
       ReadFromRegistry = reads the line-style from the registry-database
       WriteToRegistry = writes the line-style to the registry-database
     +*************************************************************************}
     TXLineStyle   = Class(TPersistent)
      Private
       FData            : PXLineStyleDef;
       FGeneralize      : Boolean;
       FGeneralizeScale : Double;
       FGeneralizeStyle : TLineStyle;
       FGeneralizePrint : Boolean;
       FName            : String;
       FNumber          : Integer;
       FRefCount        : Integer;
       FWidth           : Integer;
       Function    GetClipRect:TRect;
       Function    GetCount:Integer;
       Function    GetData:PXLineDefs;
       Function    GetItem(AIndex:Integer):PXLineDef;
       Procedure   SetCapacity(ACapacity:Integer);
       Procedure   SetRefCount(const Value: Integer);
      Protected
       Procedure   AssignTo(Dest:TPersistent); override;
      Public
       Constructor Create;
       Destructor  Destroy; override;
       Function    Add(var Item:TXLineDef):PXLineDef;
       Procedure   AddRef;
       Procedure   Clear;
       Property    ClipRect:TRect read GetClipRect;
       Class Function ClipRectOfItem(const AItem:TXLineDef):TRect;
       Property    Count:Integer read GetCount;
       Property    Data:PXLineDefs read GetData;
       Procedure   Delete(AIndex:Integer);
       Procedure   Exchange(Index1,Index2:Integer);
       Property    Generalize:Boolean read FGeneralize write FGeneralize;
       Property    GeneralizeWhenPrinting:Boolean read FGeneralizePrint write FGeneralizePrint;
       Property    GeneralizeScale:Double read FGeneralizeScale write FGeneralizeScale;
       Property    GeneralizeStyle:TLineStyle read FGeneralizeStyle write FGeneralizeStyle;
       Function    IndexOf(Item:PXLineDef):Integer;
       Property    Items[AIndex:Integer]:PXLineDef read GetItem; default;
       Procedure   Move(FromIndex,ToIndex:Integer);
       Property    Name:String read FName write FName;
       Property    Number:Integer read FNumber write FNumber;
       Procedure   ReadFromFile(IniFile:TIniFile;Const KeyName:String);
       Procedure   ReadFromRegistry(Registry:TCustomRegistry;Const KeyName:String);
       Property    ReferenceCount:Integer read FRefCount write SetRefCount;
       Procedure   Release;
       Property    StyleDef:PXLineStyleDef read FData;
       Property    Width:Integer read FWidth write FWidth;
       Procedure   WriteToFile(IniFile:TIniFile;Const KeyName:String);
       Procedure   WriteToRegistry(Registry:TCustomRegistry;Const KeyName:String);
     end;

     TXLineStyleClass   = Class of TXLineStyle;

     {**************************************************************************
     | Class TXLineStyleList
     |--------------------------------------------------------------------------
     |--------------------------------------------------------------------------
     +*************************************************************************}
     TXLineStyleList    = Class(TAutoHashTable)
      Private
       FLastNumber : Integer;
       Function    GetStyleFromNumber(ANumber:Integer):TXLineStyle;
       Function    GetStyleFromName(const AName:String):TXLineStyle;
       Function    GetStyle(AIndex:Integer):TXLineStyle;
      Protected
       Function    KeyOf(Item:Pointer):Pointer; override;
       Function    OnCompareItems(Item1,Item2:Pointer):Boolean; override;
       Function    OnCopyItem(Item:Pointer):Pointer; override;
       Function    OnGetHashValue(Item:Pointer):LongInt; override;
       Procedure   AssignTo(Dest:TPersistent); override;
      Public
       Constructor Create;
       Function    Add(Item:Pointer):Boolean; override;
       Procedure   AddRef(Style:Integer);
       Property    LastNumber:Integer read FLastNumber write FLastNumber;
       Property    NumberedStyles[ANumber:Integer]:TXLineStyle read GetStyleFromNumber; default;
       Property    NamedStyles[const AName:String]:TXLineStyle read GetStyleFromName;
       Procedure   ReadFromFile(Const FileName:String);
       Procedure   ImportFromFile(Const FileName:String);
       Function    GetNumberByName(const StyleName:string):integer;
       Function    GetNameByNumber(StyleNumber:integer):string;
       Procedure   ReadFromRegistry(Registry:TCustomRegistry;Const KeyName:String);
       Procedure   Release(Style:Integer);
       Property    Styles[AIndex:Integer]:TXLineStyle read GetStyle;
       Procedure   WriteToFile(Const FileName:String);
       Procedure   WriteToRegistry(Registry:TCustomRegistry;Const KeyName:String);
     end;

Procedure ReadXLineDefFromFile(IniFile:TIniFile;Item:PXLineDef;Const CntStr:String);

Procedure WriteXLineDefToFile(IniFile:TIniFile;Item:PXLineDef;const CntStr:String);

Implementation

Uses GrTools,NumTools,Palette,StrTools,SysUtils;

Const lt_UserDefined    = 1000;   { number of first user-defined style         }
      clFromObject      = -2;

{===============================================================================
| Class TXLineStyle
+==============================================================================}

Constructor TXLineStyle.Create;
begin
  inherited Create;
  FGeneralizeStyle:=DefaultLineStyle;
end;

Destructor TXLineStyle.Destroy;
begin
  Clear;
  inherited Destroy;
end;

Function TXLineStyle.Add(var Item:TXLineDef):PXLineDef;
begin
  SetCapacity(Count+1);
  FData^.XLineDefs[FData^.Count-1]:=Item;
  Result:=@FData^.XLineDefs[FData^.Count-1];
end;

Procedure TXLineStyle.Clear;
begin
  if FData<>NIL then FreeMem(FData,XLineStyleDefSize(FData^.Count));
  FData:=NIL;
end;

Procedure TXLineStyle.Delete(AIndex:Integer);
begin
  Assert((AIndex>=0) and (AIndex<Count));
  System.Move(FData^.XLineDefs[AIndex+1],FData^.XLineDefs[AIndex],(Count-AIndex-1)*SizeOf(TXLineDef));
  SetCapacity(FData^.Count-1);
end;
   
Procedure TXLineStyle.Exchange(Index1:Integer;Index2:Integer);
var TempItem     : TXLineDef;
begin
  Assert((Index1>=0) and (Index1<Count) and (Index2>=0) and (Index2<Count));
  System.Move(FData^.XLineDefs[Index1],TempItem,SizeOf(TXLineDef));
  System.Move(FData^.XLineDefs[Index2],FData^.XLineDefs[Index1],SizeOf(TXLineDef));
  System.Move(TempItem,FData^.XLineDefs[Index2],SizeOf(TXLineDef));
end;

Procedure TXLineStyle.Move(FromIndex:Integer;ToIndex:Integer);
var TempItem     : TXLineDef;
begin
  Assert((FromIndex>=0) and (FromIndex<Count) and (ToIndex>=0) and (ToIndex<Count));
  System.Move(FData^.XLineDefs[FromIndex],TempItem,SizeOf(TXLineDef));
  if FromIndex>ToIndex then System.Move(FData^.XLineDefs[ToIndex],FData^.XLineDefs[ToIndex+1],
      (FromIndex-ToIndex)*SizeOf(TXLineDef))
  else System.Move(FData^.XLineDefs[FromIndex+1],FData^.XLineDefs[FromIndex],
      (ToIndex-FromIndex)*SizeOf(TXLineDef));
  System.Move(TempItem,FData^.XLineDefs[ToIndex],SizeOf(TXLineDef));
end;

Function TXLineStyle.IndexOf(Item:PXLineDef):Integer;
begin
  for Result:=0 to Count-1 do if @FData^.XLineDefs[Result]=Item then Exit;
  Result:=-1;
end;

Procedure TXLineStyle.ReadFromRegistry(Registry:TCustomRegistry;Const KeyName:String);
var Cnt          : Integer;
begin
  with Registry do begin
    OpenKey(KeyName,TRUE);
    Number:=ReadInteger('Number');
    Name:=ReadString('Name');
    FGeneralize:=ReadBool('Generalize');
    if LastError<>rdbeNoError then FGeneralizePrint:=TRUE;
    FGeneralizeScale:=ReadFloat('GeneralizeScale');
    if LastError<>rdbeNoError then FGeneralizeScale:=1;
    ReadBinary('GeneralizeStyle',FGeneralizeStyle,SizeOf(FGeneralizeStyle));
    FRefCount:=ReadInteger('ReferenceCount');
    Width:=ReadInteger('Width');
    SetCapacity(ReadInteger('Count'));
    for Cnt:=0 to FData^.Count-1 do ReadBinary(Format('Item%d',[Cnt]),
        FData^.XLineDefs[Cnt],SizeOf(TXLineDef));
  end;
end;

Procedure ReadXLineDefFromFile(IniFile:TIniFile;Item:PXLineDef;Const CntStr:String);
var Cnt1         : Integer;
    WorkStr      : String;
begin
  with IniFile,Item^ do begin
    FillChar(Item^,SizeOf(Item^),#0);
    WorkStr:=ReadString(CntStr,'Type','');
    if CompareText(WorkStr,'Circle')=0 then ItemType:=itCircle
    else if CompareText(WorkStr,'Rectangle')=0 then ItemType:=itRectangle
    else if CompareText(WorkStr,'Polygon')=0 then ItemType:=itPolygon
    else if CompareText(WorkStr,'Polyline')=0 then ItemType:=itPolyline
    else if CompareText(WorkStr,'Arc')=0 then ItemType:=itArc
    else if CompareText(WorkStr,'Chord')=0 then ItemType:=itChord
    else ;//!?!?!? invalid type
    PointCount:=ReadInteger(CntStr,'PointCount',4);
    for Cnt1:=1 to PointCount do begin
      WorkStr:=ReadString(CntStr,Format('Point%d',[Cnt1]),'');
      ScanString('%i,%i',WorkStr,[@Points[Cnt1].X,@Points[Cnt1].Y]);
    end;
    Cycle:=ReadInteger(CntStr,'Cycle',0);
{++ New XLine BUG#251}
//    ClipEdges:=ReadBool(CntStr,'ClipEdges',FALSE);
    Byte(ClipEdges) := ReadInteger(CntStr, 'ClipEdges', Byte(FALSE)); 
{-- New XLine BUG#251}
    WorkStr:=ReadString(CntStr,'LineColor','');
    if CompareText(WorkStr,'None')=0 then LineColor:=clNone
    else if CompareText(WorkStr,'Object')=0 then LineColor:=clFromObject
    else ScanString('%i',WorkStr,[@LineColor]);
    WorkStr:=ReadString(CntStr,'FillColor','');
    if CompareText(WorkStr,'None')=0 then FillColor:=clNone
    else if CompareText(WorkStr,'Object')=0 then FillColor:=clFromObject
    else ScanString('%i',WorkStr,[@FillColor]);
    LineWidth:=ReadInteger(CntStr,'LineWidth',0);
{          FillStyle   : Integer;}
  end;
end;

Procedure TXLineStyle.ReadFromFile(IniFile:TIniFile;Const KeyName:String);
var Entries      : Integer;
    Item         : TXLineDef;
    Cnt          : Integer;
    CntStr       : String;
begin                                                     
  with IniFile do begin
    Number:=0;
    Name:=ReadString(KeyName,'Name','');
    Width:=ReadInteger(KeyName,'Width',0);
    Entries:=ReadInteger(KeyName,'Count',0);
    for Cnt:=0 to Entries-1 do begin
      CntStr:=KeyName+Format('Item%d',[Cnt]);
      ReadXLineDefFromFile(IniFile,@Item,CntStr);
      Add(Item);
    end;
    FGeneralize:=ReadBool(KeyName,'Generalize',FALSE);
    FGeneralizePrint:=ReadBool(KeyName,'GeneralizeWhenPrinting',FALSE);
    FGeneralizeScale:=ReadFloat(KeyName,'GeneralizeScale',1);
    with FGeneralizeStyle do begin
      Style:=ReadInteger(KeyName,'Generalize.LineStyle',lt_Solid);
      Width:=ReadFloat(KeyName,'Generalize.Width',0);
      WidthType:=ReadInteger(KeyName,'Generalize.WidthType',stDrawingUnits);
    end;
  end;
end;

Procedure WriteXLineDefToFile(IniFile:TIniFile;Item:PXLineDef;const CntStr:String);
var Cnt1           : Integer;
begin
  with IniFile,Item^ do begin
    case ItemType of
      itCircle    : WriteString(CntStr,'Type','Circle');
      itRectangle : WriteString(CntStr,'Type','Rectangle');
      itPolygon   : WriteString(CntStr,'Type','Polygon');
      itPolyline  : WriteString(CntStr,'Type','Polyline');
      itArc       : WriteString(CntStr,'Type','Arc');
      itChord     : WriteString(CntStr,'Type','Chord');
    end;
    WriteInteger(CntStr,'PointCount',PointCount);
    for Cnt1:=1 to PointCount do WriteString(CntStr,Format('Point%d',[Cnt1]),
        Format('%d,%d',[Points[Cnt1].X,Points[Cnt1].Y]));
    WriteInteger(CntStr,'Cycle',Cycle);
{++ New XLine BUG#251}
//    WriteBool(CntStr,'ClipEdges',ClipEdges);
    WriteInteger(CntStr, 'ClipEdges', Byte(ClipEdges));
{-- New XLine BUG#251}
    if LineColor=-1 then WriteString(CntStr,'LineColor','None')
    else if LineColor<-1 then WriteString(CntStr,'LineColor','Object')
    else WriteString(CntStr,'LineColor',Format('0x%.6x',[LineColor]));
    if FillColor=-1 then WriteString(CntStr,'FillColor','None')
    else if FillColor<-1 then WriteString(CntStr,'FillColor','Object')
    else WriteString(CntStr,'FillColor',Format('0x%.6x',[FillColor]));
    WriteInteger(CntStr,'LineWidth',LineWidth);
{          FillStyle   : Integer;}
  end;
end;

Procedure TXLineStyle.WriteToFile(IniFile:TIniFile;Const KeyName:String);
var Cnt          : Integer;
begin
  with IniFile do begin
    EraseSection(KeyName);
    WriteString(KeyName,'Name',Name);
    WriteInteger(KeyName,'Width',Width);
    WriteInteger(KeyName,'Count',Count);
    for Cnt:=0 to Count-1 do WriteXLineDefToFile(IniFile,Items[Cnt],
        KeyName+Format('Item%d',[Cnt]));
    WriteBool(KeyName,'Generalize',FGeneralize);
    WriteBool(KeyName,'GeneralizeWhenPrinting',FGeneralizePrint);
    WriteFloat(KeyName,'GeneralizeScale',FGeneralizeScale);
    with FGeneralizeStyle do begin
      WriteInteger(KeyName,'Generalize.LineStyle',Style);
      if Color=-1 then WriteString(KeyName,'Generalize.Color','None')
      else if Color<-1 then WriteString(KeyName,'Generalize.Color','Layer')
      else WriteString(KeyName,'Generalize.Color',Format('0x%.6x',[Color]));
      WriteFloat(KeyName,'Generalize.Width',Width);
      WriteInteger(KeyName,'Generalize.WidthType',WidthType);
    end;
  end;
end;

Procedure TXLineStyle.WriteToRegistry(Registry:TCustomRegistry;Const KeyName:String);
var Cnt          : Integer;
begin
  with Registry do begin
    OpenKey(KeyName,TRUE);
    WriteInteger('Number',Number);
    WriteString('Name',Name);
    WriteBool('Generalize',FGeneralize);
    WriteBool('GeneralizeWhenPrinting',FGeneralizePrint);
    WriteFloat('GeneralizeScale',FGeneralizeScale);
    WriteBinary('GeneralizeStyle',FGeneralizeStyle,SizeOf(FGeneralizeStyle));
    WriteInteger('ReferenceCount',FRefCount);
    WriteInteger('Width',Width);
    WriteInteger('Count',Count);
    for Cnt:=0 to FData^.Count-1 do WriteBinary(Format('Item%d',[Cnt]),
        FData^.XLineDefs[Cnt],SizeOf(TXLineDef));
  end;
end;

Procedure TXLineStyle.AssignTo(Dest:TPersistent);
begin
  with Dest as TXLineStyle do begin
    Clear;
    SetCapacity(Self.Count);
    if FData<>NIL then System.Move(Self.FData^,FData^,XLineStyleDefSize(FData^.Count));
    FName:=Self.FName;
    FNumber:=Self.FNumber;
    FGeneralize:=Self.FGeneralize;
    FGeneralizePrint:=Self.FGeneralizePrint;
    FGeneralizeScale:=Self.FGeneralizeScale;
    FGeneralizeStyle:=Self.FGeneralizeStyle;
    FRefCount:=Self.FRefCount;
    FWidth:=Self.FWidth;
  end;                   
end;

Procedure TXLineStyle.SetCapacity(ACapacity:Integer);
var Temp         : PXLineStyleDef;
begin
  if ACapacity<>Count then begin
    GetMem(Temp,XLineStyleDefSize(ACapacity));
    if FData<>NIL then begin
      System.Move(FData^,Temp^,XLineStyleDefSize(Min(ACapacity,FData^.Count)));
      FreeMem(FData,XLineStyleDefSize(FData^.Count));
    end;
    FData:=Temp;
    FData^.Count:=ACapacity;
  end;
end;

Function TXLineStyle.GetItem
   (
   AIndex          : Integer
   )
   : PXLineDef;
  begin
    Result:=@FData^.XLineDefs[AIndex];
  end;

Function TXLineStyle.GetClipRect:TRect;
var Cnt          : Integer;
    Cnt1         : Integer;
    XLineDef     : PXLineDef;
begin
  if Count=0 then Result:=Rect(0,0,0,0)
  else Result:=Rect(MaxInt,MaxInt,-MaxInt,-MaxInt);
  for Cnt:=0 to Count-1 do begin
    XLineDef:=@FData^.XLineDefs[Cnt];        
    with XLineDef^ do begin
      if ItemType<itRectangle then begin
        InsertPointInRect(Point(Points[2].X-Points[1].X,
          Points[2].Y-Points[1].X),Result);
        InsertPointInRect(Point(Points[2].X+Points[1].X,
          Points[2].Y+Points[1].X),Result);
      end
      else for Cnt1:=1 to PointCount do InsertPointInRect(Points[Cnt1],Result);
      Result.Right:=Max(Result.Right,Cycle);
    end;
  end;
end;

Function TXLineStyle.GetCount:Integer;
begin
     if FData=NIL then Result:=0
     else Result:=FData^.Count;
end;

Procedure TXLineStyle.SetRefCount(const Value:Integer);
begin
  FRefCount:=Value;
  if FRefCount<0 then FRefCount:=0;
end;

Function TXLineStyle.GetData:PXLineDefs;
begin
  if FData=NIL then Result:=NIL
  else Result:=@FData^.XLineDefs;
end;

Procedure TXLineStyle.AddRef;
begin
  Inc(FRefCount);
end;

Procedure TXLineStyle.Release;
begin
  if FRefCount>0 then Dec(FRefCount);
end;

Function TXLineStyleList.GetStyle(AIndex:Integer):TXLineStyle;
var Cnt            : Integer;
begin
  for Cnt:=0 to FCapacity-1 do if Items[Cnt]<>NIL then begin
    if AIndex=0 then begin
      Result:=Items[Cnt];
      Exit;
    end;
    Dec(AIndex);
  end;
  Result:=NIL;
end;

{===============================================================================
  Class TXLineStyleList
+==============================================================================}

Constructor TXLineStyleList.Create;
begin
  inherited Create(9);
  FLastNumber:=lt_UserDefined;
end;

Function TXLineStyleList.KeyOf(Item:Pointer):Pointer;
begin
  Result:=Pointer(TXLineStyle(Item).Number);
end;

Function TXLineStyleList.OnCompareItems(Item1:Pointer;Item2:Pointer):Boolean;
begin
  Result:=LongCompare(LongInt(Item1),LongInt(Item2))=0;
end;

Function TXLineStyleList.OnGetHashValue(Item:Pointer):LongInt;
begin
  Result:=LongInt(Item);
end;

Procedure TXLineStyleList.ReadFromRegistry(Registry:TCustomRegistry;
    const KeyName:String);
var Cnt          : Integer;
    Item         : TXLineStyle;
    ItemCount    : Integer;
begin
  with Registry do begin
    OpenKey(KeyName,FALSE);
    ItemCount:=ReadInteger('Count');
    FLastNumber:=ReadInteger('LastNumber');
    if LastError<>rdbeNoError then FLastNumber:=lt_UserDefined;
    Capacity:=ItemCount;
    for Cnt:=0 to ItemCount-1 do begin
      Item:=TXLineStyle.Create;
      Item.ReadFromRegistry(Registry,KeyName+Format('\Style%d',[Cnt]));
      Add(Item);
    end;
  end;
end;

Procedure TXLineStyleList.ImportFromFile(Const FileName:String);
var IniFile      : TIniFile;
    Cnt          : Integer;
    Entries      : Integer;
    Item         : TXLineStyle;
    exists       : boolean;
    i,n          : integer;
begin
  IniFile:=TIniFile.Create(FileName);
  with IniFile do begin
    Entries:=ReadInteger('LibraryInfo','LineStyles',0);
    for Cnt:=0 to Entries-1 do begin
      Item:=TXLineStyle.Create;
      Item.ReadFromFile(IniFile,Format('LineStyle%d',[Cnt]));
      // check if the same item does not already exist
      exists:=FALSE;
      for i:=0 to Capacity-1 do if Used(Items[i]) then
      begin
         if TXLineStyle(Items[i]).Name = Item.Name then
         begin
            n:=TXLineStyle(Items[i]).FNumber;
            Item.AssignTo(Items[i]);
            TXLineStyle(Items[i]).FNumber:=n;
            exists:=TRUE;
            break;
         end;
      end;

      if not exists then begin
         Add(Item);
      end;
      
    end;
  end;
end;

Function TXLineStyleList.GetNumberByName(const StyleName:string):integer;
var retVal:integer;
    i     :integer;
begin
   retVal:=0;
   for i:=0 to Capacity-1 do if Used(Items[i]) then
   begin
      if TXLineStyle(Items[i]).Name = StyleName then
      begin
         retVal:=TXLineStyle(Items[i]).Number;
         break;
      end;
   end;
   result:=retVal;
end;

Function TXLineStyleList.GetNameByNumber(StyleNumber:integer):string;
var retVal:string;
    i:integer;
begin
   retVal:='';
   for i:=0 to Capacity-1 do if Used(Items[i]) then
   begin
      if TXLineStyle(Items[i]).Number = StyleNumber then
      begin
         retVal:=TXLineStyle(Items[i]).Name;
         break;
      end;
   end;
   result:=retVal;
end;

Procedure TXLineStyleList.ReadFromFile(Const FileName:String);
var IniFile      : TIniFile;
    Cnt          : Integer;
    Entries      : Integer;
    Item         : TXLineStyle;
begin
  Clear;
  IniFile:=TIniFile.Create(FileName);
  with IniFile do begin
    Entries:=ReadInteger('LibraryInfo','LineStyles',0);
    for Cnt:=0 to Entries-1 do begin
      Item:=TXLineStyle.Create;
      Item.ReadFromFile(IniFile,Format('LineStyle%d',[Cnt]));
      Add(Item);
    end;
  end;
end;

Procedure TXLineStyleList.WriteToFile(Const FileName:String);
var IniFile      : TIniFile;
    ItemIndex    : Integer;
    Cnt          : Integer;
begin
  IniFile:=TIniFile.Create(FileName);
  with IniFile do begin
    WriteString('FileInfo','FileType','WinGIS-Userdefined-Style-Library');
    WriteInteger('FileInfo','FileVersion',1);
    WriteInteger('LibraryInfo','LineStyles',Count);
    ItemIndex:=0;
    for Cnt:=0 to Capacity-1 do if Used(Items[Cnt]) then begin
      TXLineStyle(Items[Cnt]).WriteToFile(IniFile,Format('LineStyle%d',[ItemIndex]));
      Inc(ItemIndex);
    end;
  end;
end;

Procedure TXLineStyleList.WriteToRegistry(Registry:TCustomRegistry;
    const KeyName:String);
var Cnt          : Integer;
    ItemIndex    : Integer;
begin
  with Registry do begin
    OpenKey(KeyName,TRUE);
    WriteInteger('Version',1);
    WriteInteger('Count',Count);
    WriteInteger('LastNumber',FLastNumber);
    ItemIndex:=0;
    for Cnt:=0 to Capacity-1 do if Used(Items[Cnt]) then begin
      TXLineStyle(Items[Cnt]).WriteToRegistry(Registry,KeyName+Format('\Style%d',[ItemIndex]));
      Inc(ItemIndex);
    end;
  end;
end;

Function TXLineStyleList.GetStyleFromNumber(ANumber:Integer):TXLineStyle;
begin
  Result:=Find(Pointer(ANumber));
end;

Procedure TXLineStyleList.AssignTo(Dest:TPersistent);
begin
  inherited AssignTo(Dest);
  with Dest as TXLineStyleList do begin
    FLastNumber:=Self.FLastNumber;
  end;
end;

{******************************************************************************+
  Function TXLineStyleList.Add
--------------------------------------------------------------------------------
  Adds a new line-style to the library. If the line-style has no number
  assigned (number = 0) a new unique number is generated and assignd to
  the line-style.
+******************************************************************************}
Function TXLineStyleList.Add(Item:Pointer):Boolean;
begin
  if TXLineStyle(Item).Number=0 then begin
    TXLineStyle(Item).Number:=FLastNumber;
    Inc(FLastNumber);
  end;
  Result:=inherited Add(Item);
end;

Function TXLineStyleList.OnCopyItem(Item: Pointer): Pointer;
begin
  Result:=TXLineStyleClass(TXLineStyle(Item).ClassType).Create;
  TPersistent(Result).Assign(Item);
end;

class Function TXLineStyle.ClipRectOfItem(const AItem: TXLineDef):TRect;
var Cnt            : Integer;
    ARect          : TGrRect;
begin
  with AItem do begin
    if ItemType<itRectangle then begin
      if ItemType=itCircle then ARect:=CircleClipRect(GrPoint(Points[2].X,
          Points[2].Y),Points[1].X)
      else ARect:=ArcClipRect(GrPoint(Points[2].X,Points[2].Y),Points[1].X,
        GrPoint(Points[3].X,Points[3].Y),GrPoint(Points[4].X,Points[4].Y));
      with ARect do Result:=Rect(Round(Left),Round(Bottom),Round(Right),Round(Top));
    end
    else begin
      Result:=Rect(MaxInt,MaxInt,-MaxInt,-MaxInt);
      for Cnt:=1 to PointCount do InsertPointInRect(Points[Cnt],Result);
    end;  
  end;
end;

Function TXLineStyleList.GetStyleFromName(const AName:String):TXLineStyle;
var Cnt            : Integer;
    Item           : TXLineStyle;
begin
  for Cnt:=0 to Capacity-1 do begin
    Item:=Items[Cnt];
    if (Item<>NIL) and (AnsiCompareText(Item.Name,AName)=0) then begin
      Result:=Item;
      Exit;
    end;
  end;
  Result:=NIL;
end;

Procedure TXLineStyleList.AddRef(Style: Integer);
var XStyle         : TXLineStyle;
begin
  XStyle:=NumberedStyles[Style];
  if XStyle<>NIL then XStyle.AddRef;
end;

Procedure TXLineStyleList.Release(Style: Integer);
var XStyle         : TXLineStyle;
begin
  XStyle:=NumberedStyles[Style];
  if XStyle<>NIL then XStyle.Release;
end;

end.
