////////////////////////////////////////////////////////////////////////////////
// Created: by Sygsky, 23-JAN-2002
//
// This unit contains all needed utils to handle with functions of new image
// creation under frame set by user, i.e. copy cutted part, replce with cut parts
// or combine cut parts into one whole image!
//
//  @ by JHC, 1987, 2002
////////////////////////////////////////////////////////////////////////////////
unit NewImageHndl;

interface
uses
   Windows, SysUtils, Graphics, Classes, Dialogs, WCtrls, TrLib32, AM_Def, MenuFn, ProjHndl,
   AM_Proj, AM_Layer, AM_Index, AM_View, BMPImage, Img_Info, AM_Main, UserUtils,
   MultiLng, Controls, StdCtrls, ExtCtrls, ImgDlg, Validate, Math, HugeBMPStream, Forms;

type
   // ++ Cadmensky
   PCutImageInfo = ^TCutImageInfo;
   TCutImageInfo = record
      sFileName: AnsiString;
      PixRect: TRect;
      PrjRect: TRect;
      PrjDRect: TDRect;
   end;

   TProgressData = record
      SummaryCount: DWORD;
      WroteCount: DWORD;
   end;
   PData = ^TProgressData;

   TWGIndArray = array[0..MaxInt div SizeOf (PIndex) - 1] of PIndex;
   PWGIndArray = ^TWGIndArray;

   TSIStruct = record
      Size: Integer; // Should be equal to SizeOf(TSIStruct)
      Count: Integer;
      PWGArray: PWGIndArray; // Objects to process in array
      FrameRect: TDRect; // Processing frame rectangle
      Mode: Byte;
      FreeOnExit: Boolean;
   end;
   PSIStruct = ^TSIStruct;

   // -- Cadmensky

   TCreateMode = (cmCopy, cmReplace, cmSuperImage);

   TMyRec = record
      FileDir: AnsiString;
      PositiveCnt: Integer;
      NegativeCnt: Integer;
      FrameRect: TDRect;
      Positive: AnsiString;
      Negative: AnsiString;
   end;
   PMyRec = ^TMyRec;

   TColorRec = record
      Count: Integer; // Number of images counted
      PixAreaSum: Double; // Area in pixels
      PrjAreaSum: Double; // Area in WinGIS units
   end;

   TIRec = record
      BitCount: Integer; // Color depth of the image
      PixArea: Double; // Area in pixels of the image
      PrjArea: Double; // Area of 1 pixel of the image in WinGIS units
      XRes: Double; // X axe resolution
      YRes: Double; // Y axe resolution
      IsValid: LongBool; // flag to mark image be available at all
   end;
   TIArray = array[0..SizeOf (TIRec) - 1] of TIRec;
   PIArray = ^TIArray;
   PIRec = ^TIRec;

   TSuperRec = record
      Common: TMyRec; // Common data
      Idx: Integer;
      Colors1Bit: TColorRec; // Data for all possible color depth of images
      Colors4Bit: TColorRec;
      Colors8Bit: TColorRec;
      Colors24Bit: TColorRec;
      PixAreaSum: Double; // Total summary for pixels for all the images
      PrjAreaSum: Double; // Total summary for project units for all the images
      IArray: PIArray; // Array for special data of each image
      listImageInfo: TList;
   end;
   PSuperRec = ^TSuperRec;

   TSuperImageInfo = class (TWForm)
      Bevel1: TBevel;
      CancelBtn: TButton;
      MlgSection: TMlgSection;
      OKBtn: TButton;
      MainPanel: TPanel;
      MainMemo: TMemo;
      ImageSizePanel: TPanel;
      ColorGroupBox: TGroupBox;
      Radio1Bit: TRadioButton;
      Radio4Bits: TRadioButton;
      Radio8Bits: TRadioButton;
      Radio24Bits: TRadioButton;
      XSizeGroupBox: TGroupBox;
      XSizeLabel: TLabel;
      YSizeGroupBox: TGroupBox;
      YSizeLabel: TLabel;
      XResValidator: TFloatValidator;
      YResValidator: TFloatValidator;
      XRes: TEdit;
      YRes: TEdit;
      Label1: TLabel;
      XDim: TEdit;
      Label2: TLabel;
      YDim: TEdit;
      procedure FormShow (Sender: TObject);
      procedure FormResize (Sender: TObject);
      procedure XResChange (Sender: TObject);
      procedure YResChange (Sender: TObject);
      procedure FormCloseQuery (Sender: TObject; var CanClose: Boolean);
    procedure YDimChange(Sender: TObject);
    procedure XDimChange(Sender: TObject);
   private
      SumData: PSuperRec;
      // ++ Cadmensky
      function ExecuteCopy (sNewImageName: AnsiString; AList: TList; AProgressProc: THugeProgProc): boolean;
      procedure CopyAndCombineImage (sNewImageName: AnsiString; PSuperImg: PSIStruct);
      // -- Cadmensky
   public
      //    constructor Create(AOwner: TComponent; Mode: TCreateMode = cmCopy); override;
   end;

   TIntersectedSave = class (TProjMenuHandler)
      procedure OnStart; override;
   end;
   TIntersectedReplace = class (TProjMenuHandler)
      procedure OnStart; override;
   end;
   TIntersectedCombine = class (TProjMenuHandler)
      procedure OnStart; override;
   end;

   // Returns TRUE if exit wanted, else FALSE to continue processing
   LayerProc = function (ALayer: PLayer; ALayerItem: PIndex; UserParam: DWORD): Boolean;

function SaveDIBToFile (pDIB: PBitmapInfo; NewName: AnsiString): Boolean;
function ConvertBitMapToDIB (BitMap: PBitMap): PBitMapInfo;
function ProcessSuperImage (Data: PProj; PParam: Pointer): Integer;
procedure EnumIndexInstances (AProj: PProj; AItem: PIndex; DoAll: LayerProc; UserParam: Cardinal; CheckView: Boolean = True);

// ++ Cadmensky
function MyProgress (WroteBytes: DWORD; UserData: Cardinal): Boolean; // Alll data needed to construct form
procedure GetIRect (FrameRect, PrjDRect: TDRect; XDim, YDim: integer; var PrjRect: TRect);
// -- Cadmensky

implementation

uses PALUtils, UserIntf, TABFiles, ShellAPI, WinOSInfo;

{$R *.MFN}
{$R *.DFM}

function CheckExt: Boolean;
var
   Ext: AnsiString;
begin
   Ext := TrLib.ImageGetExpFormats;
   Result := TrLib32.IsExtInFilter (Ext, BMPExt);
   if not Result then
      SmartMsgBox (0, 'Main', 25, 26, MB_OK + MB_ICONERROR, []);
end;

procedure TIntersectedSave.OnStart;
begin
   //  NotImplemented;
   //  Exit;
   if not CheckExt then
      Exit;
   Self.Project.SuperImageOption := 0;
   Self.Project.SetActualMenu (mn_SuperImage);
end;

procedure TIntersectedReplace.OnStart;
begin
   NotImplemented;
   Exit;
   if not CheckExt then
      Exit;
   Self.Project.SuperImageOption := 1;
   Self.Project.SetActualMenu (mn_SuperImage);
end;

procedure TIntersectedCombine.OnStart;
begin
   {  NotImplemented;
     Exit;}
   if not CheckExt then
      Exit;
   Self.Project.SuperImageOption := 2;
   Self.Project.SetActualMenu (mn_SuperImage);
end;

function SaveDIBToFile (pDIB: PBitmapInfo; NewName: AnsiString): Boolean;
begin
   Result := False;
end;

function ConvertBitMapToDIB (BitMap: PBitMap): PBitMapInfo;
begin
   Result := nil;
end;

//)()()()())))()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()
// Enumeration routine for the PImage

function CutImage (Layer: PLayer; AItem: PImage; MyRec: PMyRec): Boolean;
var
   IDrive, IDir, IName, IExt, ISuffix, IPath: AnsiString;
   I: Integer;
begin
   //++ Check parameters section
   Result := False; // Don't stop enumeration by default
   if Assigned (Layer) then // We don't need layer checking
      Exit;
   if AItem.GetObjType <> ot_Image then // Work only on images
      Exit;
   //-- End of parameter checking section

   // So now AItem really is PView from the pool
   SplitFileName (AItem.AbsolutePath^, nil, nil, @IName, @IExt);
   SplitFileName (MyRec.FileDir, @IDrive, @IDir, nil, nil);
   IPath := IDrive + IDir + IName + BMPExt;
   for I := 2 to MaxInt do
   begin
      if not FileExists (IPath) then
         Break;
      IPath := IDrive + IDir + IName + Format ('(%d)', [I]) + BMPExt;
   end;
   if PImage (AItem).Cutting (IPath, MyRec.FrameRect) then
   begin
      Inc (MyRec.PositiveCnt);
      MyRec.Positive := MyRec.Positive +
         IntToStr (AItem.Index) + ':"' +
         ExtractFileName (AItem.AbsolutePath^) +
         '" -> "' + ExtractFileName (IPath) + '"'#13#10#13#10;
   end
   else
   begin
      Inc (MyRec.NegativeCnt);
      MyRec.Negative := #13#10 + MyRec.Negative + IntToStr (AItem.Index) + ': "' +
         ExtractFileName (PImage (AItem).AbsolutePath^) + '"';
   end;
   Result := True; // It is time to stop, as we need only PView to work on.
end;

//()()()()())))()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()
//()Enumeration route for the image to calculate statistics before super ()
//() image creation.                                                     ()
//()()()()())))()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()

function GetStat (Layer: PLayer; AImage: PImage; PSRec {UserData}: PSuperRec {Cardinal}): Boolean;
var
   CBitCount: Integer;
   PCRec: ^TColorRec;
   ProjRect: TDRect; // Result rect in proj coordinates
   PixInRect: TRect; // Result rect in image coordinates
   ACutImageInfo: PCutImageInfo;
begin
   //++ Check parameters section
   Result := False; // Don't stop enumeration by default
   if Assigned (Layer) then // We don't need layer checking
      Exit;
   if AImage.GetObjType <> ot_Image then // Work only on images
      Exit;
   //-- End of parameter checking section

   // So now AItem really is PView from the pool and we can fill all stat fields
 //    AImage := PImage(AItem);
   CBitCount := AImage.Info.BitCount;
   case CBitCount of
      1: PCRec := Addr (PSRec.Colors1Bit) {.Count};
      4: PCRec := Addr (PSRec.Colors4Bit) {.Count};
      8: PCRec := Addr (PSRec.Colors8Bit) {.Count};
     24: PCRec := Addr (PSRec.Colors24Bit) {.Count};
   else // Some strange image - how it was loaded at all I'll want to knew
      begin
         Inc (PSRec.Common.NegativeCnt);
         PSRec.Common.Negative := PSRec.Common.Negative +
            IntToStr (AImage.Index) + ':' +
            ExtractFileName (AImage.AbsolutePath^) + #13#10;
         Result := True;
         Exit;
      end;
   end;
   PSRec.IArray[PSRec.Idx].IsValid := True;
   PSRec.IArray[PSRec.Idx].BitCount := CBitCount;
   AImage.GetIRects (PSRec.Common.FrameRect, ProjRect, PixInRect);
   PSRec.IArray[PSRec.Idx].PrjArea := ProjRect.XSize * ProjRect.YSize;
   PSRec.IArray[PSRec.Idx].XRes := ProjRect.XSize / PRectWidth (@PixInRect);
   PSRec.IArray[PSRec.Idx].YRes := ProjRect.YSize / PRectHeight (@PixInRect);

   PSRec.IArray[PSRec.Idx].PixArea := PRectWidth (@PixInRect) * PRectHeight (@PixInRect);
   Inc (PCRec.Count);
   PCRec.PixAreaSum := PCRec.PixAreaSum + PSRec.IArray[PSRec.Idx].PixArea;
   PCRec.PrjAreaSum := PCRec.PrjAreaSum + PSRec.IArray[PSRec.Idx].PrjArea;
   PSRec.PixAreaSum := PSRec.PixAreaSum + PSRec.IArray[PSRec.Idx].PixArea;
   PSRec.PrjAreaSum := PSRec.PrjAreaSum + PSRec.IArray[PSRec.Idx].PrjArea;

   Inc (PSRec.Common.PositiveCnt);
   PSRec.Common.Positive := PSRec.Common.Positive + IntToStr (AImage.Index) + ': ' +
      ExtractFileName (AImage.AbsolutePath^) + #13#10;
   // ++ Cadmensky
   if PSRec.listImageInfo = nil then
      PSRec.listImageInfo := TList.Create;
   New (ACutImageInfo);
   ACutImageInfo.sFileName := AImage.AbsolutePath^;
   ACutImageInfo.PixRect := PixInRect;
   ACutImageInfo.PrjDRect.InitByRect (ProjRect);
   PSRec.listImageInfo.Add (ACutImageInfo);
   // -- Cadmensky
end;

///////////////////////////////////////////////////////////////////////////
// This proc really process super image functionality...
//
//
///////////////////////////////////////////////////////////////////////////

function ProcessSuperImage (Data: PProj; PParam: Pointer): Integer;
{$IFNDEF AXDLL} // <----------------- AXDLL
var
   PSuperImg: PSIStruct;
   PImg: PImage;
   I: Integer;
   Positive, Negative: AnsiString;
   OldTitle: AnsiString;
   pImgRec: PIRec;
   MyRec: TMyRec;
   SRec: TSuperRec;
   AverX, AverY,
      AverWPix, DTemp,
      MinXRes, MaxXRes,
      MinYRes, MaxYRes: Double;
   MinRect, MaxRect: TRect;

   sMemoLines: AnsiString;
   ASuperImageInfo: TSuperImageInfo;
{$ENDIF} // <----------------- AXDLL
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
   if not Assigned (Pointer (PParam)) then
      Exit;
   PSuperImg := PSIStruct (PParam);
   if PSuperImg.Size < SizeOf (TSIStruct) then // Not known structure
      Exit;
   with PSuperImg^ do
      try
         if Count <= 0 then
            Exit; // No images to process
         FillChar (MyRec, SizeOf (MyRec), 0);
         MyRec.FrameRect.InitByRect (FrameRect);
         case Mode of
            0: // Copy-And-Save
               begin
                  with WinGISMainForm.SelectFolderDialog do
                  begin
                     OldTitle := Title;
                     try
                        Title := GetLangText (15);
                        if Execute then
                           MyRec.FileDir := TrueDirPath (FileName)
                        else
                           Exit;
                     finally
                        Title := OldTitle;
                     end;
                  end;
                  for I := 0 to Count - 1 do
                  begin
                     PImg := PImage (PWGArray[I]);
                     EnumIndexInstances (Data, PImg, @CutImage, Cardinal (@MyRec), True);
                  end;
                  with TSuperImageInfo.Create (Data.Parent {, cmCopy}) do
                     try
                        Tag := Integer (cmCopy);
                        sMemoLines := Format (GetLangText (17),
                                             [FrameRect.A.X, FrameRect.A.Y, FrameRect.B.X, FrameRect.B.Y,
//                                            SRec.PixAreaSum, SRec.PrjAreaSum,
                                              MyRec.PositiveCnt + MyRec.NegativeCnt,
                                              MyRec.PositiveCnt, MyRec.FileDir, MyRec.Positive,
                                              MyRec.NegativeCnt, MyRec.Negative]);
                        MainMemo.Lines.Add (sMemoLines);
                        ShowModal;
                     finally
                        Free;
                     end;
               end;
            1: // Copy-And-Replace (not implemented)
               begin
                  NotImplemented;
                  Exit;
               end;
            2: // Copy-And-Combine
               begin
                  // Clean variable from the stack (i.e. garbaged)
                  FillChar (SRec, SizeOf (TSuperRec), 0);
                  CopyMemory (@SRec.Common, @MyRec, SizeOf (MyRec));
                  // Get clean info array
                  SRec.IArray := AllocMem (SizeOf (TIRec) * (Count + 1));
                  try
                     // Get statistics for future super image
                     for I := 0 to Count - 1 do
                     begin
                        PImg := PImage (PWGArray[I]); // Get next image
                        SRec.Idx := I;
                        // Process statistics on it
                        EnumIndexInstances (Data, PImg, @GetStat, Cardinal (@SRec), True);
                     end;
                     // Display option dialog for super image
                     ASuperImageInfo := TSuperImageInfo.Create (Data.Parent {, cmSuperImage});
                     try
                        ASuperImageInfo.Tag := Integer (cmSuperImage);
                        ASuperImageInfo.SumData := Addr (SRec);
                        // First fill common info
                        if SRec.Common.NegativeCnt > 0 then
                        sMemoLines := Format (GetLangText (19),
                                             [FrameRect.A.X, FrameRect.A.Y, FrameRect.B.X, FrameRect.B.Y,
                                              FrameRect.XSize * FrameRect.YSize / 10000, { load value in sq.meters! }
                                              SRec.Common.PositiveCnt + SRec.Common.NegativeCnt,
                                              SRec.Common.PositiveCnt, SRec.Common.FileDir, SRec.Common.Positive,
                                              SRec.Common.NegativeCnt, SRec.Common.Negative])
                        else
                        sMemoLines := Format (GetLangText (27),
                                             [FrameRect.A.X, FrameRect.A.Y, FrameRect.B.X, FrameRect.B.Y,
                                              FrameRect.XSize * FrameRect.YSize / 10000, { load value in sq.meters! }
                                              SRec.Common.PositiveCnt + SRec.Common.NegativeCnt,
                                              SRec.Common.PositiveCnt, SRec.Common.FileDir, SRec.Common.Positive]);
                        ASuperImageInfo.MainMemo.Lines.Add (sMemoLines);
                        // Now calculate all the statistics, i.e. average X/Y scales and
                        //  probable future image dimensions.

                        // Calc Weight-average for each image based on relation between
                        // square of each image part in pix to whole part square iin pix
                        // involved into calculations.
          //              AverWPix := SRec.PixAreaSum / Count;
                        AverX := 0;
                        AverY := 0;
                        MinXRes := 10E+300;
                        MinYRes := 10E+300;
                        MaxXRes := 0;
                        MaxYRes := 0;
                        for I := 0 to Count - 1 do
                        begin
                           PImgRec := @SRec.IArray[I]; // Point to the image structure
                           DTemp := PImgRec.PixArea / SRec.PixAreaSum; // Always < 1!
                           AverX := AverX + PImgRec.XRes * DTemp;
                           AverY := AverY + PImgRec.YRes * DTemp;
                           MinXRes := Min (MinXRes, PImgRec.XRes);
                           MinYRes := Min (MinYRes, PImgRec.YRes);
                           MaxXRes := Max (MaxXRes, PImgRec.XRes);
                           MaxYRes := Max (MaxYRes, PImgRec.YRes);
                        end;
                        ASuperImageInfo.XRes.Text := Format ('%n', [AverX / 100]);
                        // I added 10% of value to be available in both direction to change
                        // So minimum available is 10 % lesser that real, and
                        // maximum is 10% more than real ones
                        ASuperImageInfo.XResValidator.MinValue := Int (MinXRes * 0.9) / 100;
                        ASuperImageInfo.XResValidator.MaxValue := Int (MaxXRes * 1.1 + 0.5) / 100;
                        ASuperImageInfo.XRes.Hint := Format ('Set resolution between %f..%f',
                                                             [ASuperImageInfo.XResValidator.MinValue, ASuperImageInfo.XResValidator.MaxValue]);
                        ASuperImageInfo.YRes.Text := Format ('%n', [AverY / 100]);
                        // I added 10% of value to be available in both direction to change
                        ASuperImageInfo.YResValidator.MinValue := Int (MinYRes * 0.9) / 100;
                        ASuperImageInfo.YResValidator.MaxValue := Int (MaxYRes * 1.1 + 0.5) / 100;
                        ASuperImageInfo.YRes.Hint := Format ('Set resolution between %f..%f',
                                                             [ASuperImageInfo.YResValidator.MinValue, ASuperImageInfo.YResValidator.MaxValue]);
                        ASuperImageInfo.XDim.Text := IntToStr (Ceil (FrameRect.XSize / AverX));
                        // To min res corresponds max pixel size of the result image
                        // and vice veras to max res -> min  dimension
          //              XDimValidator.MaxValue := Ceil(FrameRect.XSize / MinXRes);
          //              XDimValidator.MinValue := Floor(FrameRect.XSize / MaxXRes);
                        ASuperImageInfo.XDim.Hint := 'Future image width in pixels';

                        ASuperImageInfo.YDim.Text := IntToStr (Ceil (FrameRect.YSize / AverY));
                        // To min res corresponds max pixel size of the result image
                        // and vice veras to max res -> min  dimension
          //              YDimValidator.MaxValue := Ceil(FrameRect.YSize / MinYRes);
          //              YDimValidator.MinValue := Floor(FrameRect.YSize / MaxYRes);
                        ASuperImageInfo.YDim.Hint := 'Future image height in pixels';

                        ASuperImageInfo.XRes.Tag := 1;
                        ASuperImageInfo.YRes.Tag := 1;
                        ASuperImageInfo.XDim.Tag := 1;
                        ASuperImageInfo.YDim.Tag := 1;

                        if ASuperImageInfo.ShowModal = mrOK then // Try to get new file name
                        begin
{                           if ImgDlg.SaveImageFile (MyRec.FileDir, 'BMP|*.bmp') then
                           begin}
                              ASuperImageInfo.CopyAndCombineImage (MyRec.FileDir, PSuperImg);
//                              ShellExecute (ASuperImageInfo.Handle, 'open', PChar ('c:\WinGIS4\Delphi4\VMSIPlus.exe'), PChar ('-O ' + MyRec.FileDir), nil, 0);
//                                 ShowMessage ('Very well, my sir... :o)');
                              Exit;
//                           end;
                        end;
//                        ShowMessage ('Not very well, my sir... :o(');
                     finally
                        ASuperImageInfo.Free; // of main info form
                     end;
                  finally
                     FreeMem (SRec.IArray);
                  end;
               end;
         else
         end;
      finally
         if FreeOnExit then //  parameter block should be freed by function, not caller
            FreeMem (PParam);
      end;
{$ENDIF} // <----------------- AXDLL
end;

////////////////////////////////////////////////////////////////////////////
// Procedures search all index instances, including project pool (of views)
// On each found index on some layer the DoAll(of type LayerProc) procedure
// is called with according parameters.
//...
// If CheckView = True, then common pool also is search through to call
// LayerProc with ALayer parameter = nil to mark this condition! That case
// AItem will be PView, not PIndex, of course.
// ...
// It is guarantied that DoAll is called first with PView from common pool, if
// CheckView is set to True, of course, and first layer with which it will be
// called will always be TopLayer!
////////////////////////////////////////////////////////////////////////////

procedure EnumIndexInstances (
   AProj: PProj;
   AItem: PIndex;
   DoAll: LayerProc;
   UserParam: Cardinal;
   CheckView: Boolean
   );
var
   View: PView;
   Idx: PIndex;
   Layer, TopLayer: PLayer;
   I, ICnt: Integer;

   function ForLayer (ALayer: PLayer): Boolean;
   begin
      Idx := ALayer.HasObject (AItem);
      Result := DoAll (ALayer, Idx, UserParam);
   end;

begin
   if not (Assigned (AProj) and Assigned (AItem) and Assigned (DoAll)) then
      Exit;
   // First check TopLayer
   try
      TopLayer := AProj.Layers.TopLayer;
      if CheckView then // But View checking has highest priority  :o)
      begin
         View := TopLayer.IndexObject (AProj.PInfo, AItem);
         if Assigned (View) then
            if DoAll (nil, View, UserParam) then
               Exit;
      end;
      if ForLayer (TopLayer) then
         Exit;
      for I := 1 to AProj.Layers.LData.Count - 1 do
      begin
         Layer := AProj.Layers.LData.At (I);
         if Layer = TopLayer then // Skip top layer as it is already processed
            continue;
         if ForLayer (Layer) then
            Exit;
      end;
   except
      ShowMessage ('Some exception occured in EnumIndexInstances call!');
      Exit;
   end;
end;

{ TSuperImageInfo }

procedure TSuperImageInfo.FormShow (Sender: TObject);
var
   Tmp64: Int64;

   procedure DoCaption (RB: TRadioButton; var ColRec: TColorRec);
   var
      Tmp1: Int64;
   begin
// ++ Cadmensky
//      RB.Enabled := ColRec.Count > 0;
      Tmp1 := Round (ColRec.PixAreaSum);
      if ColRec.Count > 0 then
         RB.Caption := RB.Caption + ' [' + IntToStr (ColRec.Count) + 'i ' + IntToStr (Tmp1) + ' sq.pix]'
      else
         RB.Caption := RB.Caption + ' [None]';
//      if not RB.Enabled then
//         Exit;
// -- Cadmensky
      if Tmp1 >= Tmp64 then
      begin
         Tmp64 := Tmp1;
         RB.Checked := True;
      end;
   end;

begin
   OkBtn.Visible := True;
   Bevel1.Visible := True;
   case TCreateMode (Self.Tag) of
      cmCopy:
         begin
            ImageSizePanel.Visible := False; // Move it out
         end;
      cmReplace: ;
      cmSuperImage:
         begin
            CancelBtn.Visible := True;
            Tmp64 := 0;
            Self.Width := Self.Width + ImageSizePanel.Width;
            //      Set color depths
            DoCaption (Radio1Bit, SumData.Colors1Bit);
            DoCaption (Radio4Bits, SumData.Colors4Bit);
            DoCaption (Radio8Bits, SumData.Colors8Bit);
            DoCaption (Radio24Bits, SumData.Colors24Bit);
         end;
   end;
end;

procedure TSuperImageInfo.FormResize (Sender: TObject);
begin
   if OKBtn.Visible then
   begin
      OKBtn.Left := (Self.Width - OKBtn.Width) div 2;
   end;
   if CancelBtn.Visible then
   begin
      CancelBtn.Left := OKBtn.Left + OKbtn.Width + 8;
   end
end;

procedure TSuperImageInfo.XResChange (Sender: TObject);
begin
   if XRes.Tag = 1 then
   begin
      XDim.Text := IntToStr (Round (SumData.Common.FrameRect.XSize / StrToFloat (XRes.Text) / 100));
      YDim.Text := IntToStr (Round (StrToFloat (XDim.Text) * SumData.Common.FrameRect.YSize / SumData.Common.FrameRect.XSize));
      YRes.Text := Format ('%n', [SumData.Common.FrameRect.YSize / StrToFloat (YDim.Text) / 100]);
   end;
end;

procedure TSuperImageInfo.YResChange (Sender: TObject);
begin
   if YRes.Tag = 1 then
   begin
      YDim.Text := IntToStr (Round (SumData.Common.FrameRect.YSize / StrToFloat (YRes.Text) / 100));
      XDim.Text := IntToStr (Round (StrToFloat (YDim.Text) * SumData.Common.FrameRect.XSize / SumData.Common.FrameRect.YSize));
      XRes.Text := Format ('%n', [SumData.Common.FrameRect.XSize / StrToFloat (XDim.Text) / 100]);
   end;
end;

procedure TSuperImageInfo.XDimChange(Sender: TObject);
var k: double;
begin
   if XDim.Tag = 1 then
   begin
      YDim.Tag := 0;
      XRes.Tag := 0;
      YRes.Tag := 0;

      XRes.Text := Format ('%n', [SumData.Common.FrameRect.XSize / StrToFloat (XDim.Text) / 100]);
      if (StrToFloat (XRes.Text) <= XResValidator.MaxValue) and (StrToFloat (XRes.Text) >= XResValidator.MinValue) then
         XRes.Font.Color := clBlack
      else
         XRes.Font.Color := clRed;
      YDim.Text := IntToStr (Round (StrToFloat (XDim.Text) * SumData.Common.FrameRect.YSize / SumData.Common.FrameRect.XSize));
      YRes.Text := Format ('%n', [SumData.Common.FrameRect.YSize / StrToFloat (YDim.Text) / 100]);
      if (StrToFloat (YRes.Text) <= YResValidator.MaxValue) and (StrToFloat (YRes.Text) >= YResValidator.MinValue) then
         YRes.Font.Color := clBlack
      else
         YRes.Font.Color := clRed;

      YDim.Tag := 1;
      XRes.Tag := 1;
      YRes.Tag := 1;
   end;
end;

procedure TSuperImageInfo.YDimChange(Sender: TObject);
begin
   if YDim.Tag = 1 then
   begin
      XDim.Tag := 0;
      XRes.Tag := 0;
      YRes.Tag := 0;

      YRes.Text := Format ('%n', [SumData.Common.FrameRect.YSize / StrToFloat (YDim.Text) / 100]);
      if (StrToFloat (YRes.Text) <= YResValidator.MaxValue) and (StrToFloat (YRes.Text) >= YResValidator.MinValue) then
         YRes.Font.Color := clBlack
      else
         YRes.Font.Color := clRed;
      XDim.Text := IntToStr (Round (StrToFloat (YDim.Text) * SumData.Common.FrameRect.XSize / SumData.Common.FrameRect.YSize));
      XRes.Text := Format ('%n', [SumData.Common.FrameRect.XSize / StrToFloat (XDim.Text) / 100]);
      if (StrToFloat (XRes.Text) <= XResValidator.MaxValue) and (StrToFloat (XRes.Text) >= XResValidator.MinValue) then
         XRes.Font.Color := clBlack
      else
         XRes.Font.Color := clRed;

      XDim.Tag := 1;
      XRes.Tag := 1;
      YRes.Tag := 1;
   end;
end;

procedure TSuperImageInfo.FormCloseQuery (Sender: TObject;
   var CanClose: Boolean);
begin
   case TCreateMode (Self.Tag) of
      cmCopy, cmReplace:
         CanClose := True;
      cmSuperImage:
         begin
            CanClose := CheckValidators (Self);
         end;
   end;
end;

function TSuperImageInfo.ExecuteCopy (sNewImageName: AnsiString; AList: TList; AProgressProc: THugeProgProc): boolean;
const
   RGB_ADDER: DWORD = $00010101;
var
   Hnd, Hnd1, Hnd2, DIBHnd: THandle;
   Huge: THugeBMPStream;
   PInfo, PInfo1, PInfo2: PBitMapInfo;
   I, Res, CopyMode: Integer;
   hPal: HPALETTE;
   ProgData: TProgressData;
   OldProc: THugeProgProc;
   ACutImageInfo: PCutImageInfo;
begin
   Result := true;
   if not ImageManAvailable then
   begin
      Result := false;
      Exit;
   end;

   PInfo := AllocMem (SizeOf (TBitMapInfo)); // allocate nullified memory buffer
   try
      PInfo.bmiHeader.biSize := $28; // 40 decimal
      PInfo.bmiHeader.biWidth := StrToInt (SELF.XDim.Text);
      //      IMAGE_H;
      PInfo.bmiHeader.biHeight := StrToInt (SELF.YDim.Text);
      //      IMAGE_W;
      PInfo.bmiHeader.biPlanes := 1;
      //      case ColorBits.ItemIndex of
      if SELF.Radio1Bit.Checked then
         //         0:
      begin
         ReallocMem (PInfo, SizeOf (TBitMapInfo) + SizeOf (TRGBQuad) * 2);
         PInfo.bmiHeader.biBitCount := 1;
         Set1BitSysPal (@PInfo.bmiColors[0]);
      end
      else
         if SELF.Radio4Bits.Checked then
            //         1:
         begin
            ReallocMem (PInfo, SizeOf (TBitMapInfo) + SizeOf (TRGBQuad) * 16);
            PInfo.bmiHeader.biBitCount := 4;
            Set4BitSysPal (@PInfo.bmiColors[0]);
         end
         else
            if SELF.Radio8Bits.Checked then
               //          2:
            begin
               ReallocMem (PInfo, SizeOf (TBitMapInfo) + SizeOf (TRGBQuad) * 256);
               PInfo.bmiHeader.biBitCount := 8;
               Set8BitGrayPal (@PInfo.bmiColors[0]);
            end
            else // if 3 or -1 then set default maximum one
               PInfo.bmiHeader.biBitCount := 24;
      //      end;
      PInfo.bmiHeader.biCompression := BI_RGB;

      Huge := THugeBMPStream.Create (sNewImageName, PInfo);
      if Assigned (Huge) then
         try
            //            Cancel.Visible := True;
            ProgData.WroteCount := 0;
            ProgData.SummaryCount := Huge.FileSize;
            OldProc := Huge.SetProgressProc (MyProgress, DWORD (@ProgData));

            for i := AList.Count - 1 downto 0 do
            begin
               ACutImageInfo := AList[i];
               Hnd := TrLib.ImageOpenSolo (PChar (ACutImageInfo.sFileName), nil);
               if Hnd <> 0 then
                  try
                     CopyMode := COPY_DEL;
                     case Huge.BitCount of
                        1: ;
                        24: CopyMode := COPY_INTERPOLATE;
                        4:
                           if HasGrayPalette (PInfo) then
                              CopyMode := COPY_ANTIALIAS;
                        8:
                           if HasGrayPalette (PInfo) then
                              CopyMode := COPY_ANTIALIAS256;
                     end;
                     Hnd1 := TrLib.ImageCopy (Hnd,
                        PRectWidth (@ACutImageInfo.PrjRect),
                        PRectHeight (@ACutImageInfo.PrjRect),
                        @ACutImageInfo.PixRect, COPY_DEL);
                     if Hnd1 <> 0 then
                        try
                           PInfo1 := TrLib.ImageGetInfoAddr (Hnd1);
                           if PInfo1.bmiHeader.biBitCount < Huge.BitCount then
                              try
                                 Hnd2 := TrLib.ImageIncreaseColors (Hnd1, Huge.BitCount);
                              except
                                 Hnd2 := 0;
                                 continue;
                              end
                           else
                              if PInfo1.bmiHeader.biBitCount > Huge.BitCount then
                                 try
                                    Hnd2 := TrLib.ImageReduceColors (Hnd1, Huge.ColorNum, IMG_BURKES);
                                 except
                                    Hnd2 := 0;
                                    continue;
                                 end
                              else // Source BitCount is the same as for destination
                              begin
                                 Hnd2 := Hnd1;
                                 Hnd1 := 0;
                              end;

                           if Hnd2 <> 0 then
                              try
                                 // First clear the garbage
                                 if TrLib.ImageClose (Hnd1) = IMG_OK then
                                    Hnd1 := 0;
                                 if TrLib.ImageClose (Hnd) = IMG_OK then
                                    Hnd := 0;

                                 // Main procedure is HERE +++++++++++++++++++++

                                 if Huge.BitCount < 24 then // Check palettes equality
                                 begin
                                    PInfo2 := TrLib.ImageGetInfoAddr (Hnd2); // Get its palette
                                    if not PalsAreEqual (PInfo, PInfo2) then
                                    begin
                                       // Try to adapt palette
                                       hPal := BuildPalette (PInfo);
                                       try
                                          try
                                             Hnd1 := TrLib.ImageReduceColorsPal (Hnd2, IMG_BAYER, hPal);
                                          except
                                             Hnd1 := 0;
                                          end;
                                          if Hnd1 <> 0 then
                                          begin
                                             TrLib.ImageClose (Hnd2);
                                             Hnd2 := Hnd1;
                                             Hnd1 := 0;
                                          end;
                                       finally
                                          DeleteObject (hPal);
                                       end;
                                    end;
                                 end;

                                 // Next read bits and load into
                                 DIBHnd := TrLib.ImageGetDIB (Hnd2, True, nil); // Get all the rect pixels
                                 if DIBHnd <> 0 then
                                    try // To guarantee the DIB buffer memory release
                                       PInfo2 := GlobalLock (DIBHnd);
                                       Inc (ProgData.SummaryCount, BitMapImageSize (PInfo2));
                                       Res := Huge.WriteBlock (ACutImageInfo.PrjRect, PInfo2);
                                    finally
                                       GlobalUnlock (DIBHnd);
                                       GlobalFree (DIBHnd);
                                    end
                                 else
                                    Result := false;
                              finally
                                 TrLib.ImageClose (Hnd2);
                              end
                           else
                              Result := false;
                        finally
                           if Hnd1 <> 0 then
                              TrLib.ImageClose (Hnd1);
                        end
                     else
                        Result := false;
                  finally
                     if Hnd <> 0 then
                        TrLib.ImageClose (Hnd);
                  end
               else
                  Result := false;
            end;
            Huge.DoComplete;
         finally
            Huge.Free;
         end
      else
         Result := false;
   finally
      FreeMem (PInfo);
      //      Cancel.Visible := False;
   end;
end;

procedure TSuperImageInfo.CopyAndCombineImage (sNewImageName: AnsiString; PSuperImg: PSIStruct);
{$IFNDEF AXDLL} // <----------------- AXDLL
var i: integer;
    ACutImageInfo: PCutImageInfo;
    iXDim, iYDim: integer;
    ARect: TRect;
    Dummy: TRefArray;
    sFilter, sConverterExeName: AnsiString;
    sRealImageName: AnsiString;
    bNeedToConvert: boolean;
    AnOldScreenCursor: TCursor;
    sMessage: AnsiString;
{$ENDIF AXDLL} // <----------------- AXDLL
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
   sConverterExeName := OSInfo.WingisDir + 'VMSIPlus.exe';
   if FileExists (sConverterExeName) then
      sFilter := 'BMP|*.bmp|PRE|*.pre'
   else
      sFilter := 'BMP|*.bmp';

   if not ImgDlg.SaveImageFile (sNewImageName, sFilter) then
      exit;

   AnOldScreenCursor := Screen.Cursor;
   Screen.Cursor := crHourGlass;
   sRealImageName := sNewImageName;

   if AnsiUpperCase (ExtractFileExt (sNewImageName)) = '.PRE' then
   begin
      sNewImageName := ChangeFileExt (sNewImageName, '.bmp');
      bNeedToConvert := true;
   end
   else
      bNeedToConvert := false;

   try
      iXDim := StrToInt (SELF.XDim.Text);
      iYDim := StrToInt (SELF.YDim.Text);
      for i := 0 to SELF.SumData.listImageInfo.Count - 1 do
      begin
         ACutImageInfo := SELF.SumData.listImageInfo[i];
         GetIRect (SELF.SumData.Common.FrameRect,
            ACutImageInfo.PrjDRect,
            iXDim,
            iYDim,
            ACutImageInfo.PrjRect);
      end;

      StatusBar.ProgressPanel := true;
      StatusBar.ProgressText := GetLangText (19002);

      try
         DeleteFile (sNewImageName);
      except
      end;

      if not Self.ExecuteCopy (sNewImageName, SELF.SumData.listImageInfo, MyProgress) then
      begin
         MsgBox(Application.Handle, 19001, MB_OK or MB_ICONERROR, '');
         if FileExists (sNewImageName) then
            DeleteFile (sNewImageName);
      end
      else
      begin
         if bNeedToConvert then
         begin
            try
               DeleteFile (sRealImageName);
            except
            end;

            try
               DeleteFile (ChangeFileExt (sRealImageName, '.TAB'));
            except
            end;
            ShellExecute (SELF.Handle, 'open', PChar (sConverterExeName), PChar ('-O ' + sNewImageName + ' -X'),
                          nil, 0);
         end;
         
         if not FileExists (sRealImageName) then
            MsgBox(Application.Handle, 19001, MB_OK or MB_ICONERROR, '')
         else
         begin
            ARect := Rect (SELF.SumData.Common.FrameRect.A.X,
                           SELF.SumData.Common.FrameRect.B.Y,
                           SELF.SumData.Common.FrameRect.B.X,
                           SELF.SumData.Common.FrameRect.A.Y);
            Dummy := nil;
            CreateTABFile(sRealImageName, iXDim, iYDim, @ARect, Dummy);
         end;
      end;

   finally

      Screen.Cursor := AnOldScreenCursor;

      if FileExists (sRealImageName) then
      begin
         sMessage := Format (MlgStringList['OmegaHndl', 58], [sRealImageName]);
         Application.MessageBox (PChar (sMessage), 'WinGIS', MB_OK + MB_DEFBUTTON1 + MB_ICONINFORMATION);
      end;
      StatusBar.ProgressPanel := false;
      for i := 0 to SELF.SumData.listImageInfo.Count - 1 do
         FreeMem (SELF.SumData.listImageInfo.Items[i]);
      SELF.SumData.listImageInfo.Free;

   end;
  {$ENDIF AXDLL} // <----------------- AXDLL
end;

function MyProgress (WroteBytes: DWORD; UserData: Cardinal): Boolean;
begin
   //   Result := CancelFlag;
      //  MainForm.ProgressBar.Visible := True;
   Inc (PData (UserData).WroteCount, WroteBytes);
   StatusBar.Progress := PData (UserData).WroteCount * 100 div PData (UserData).SummaryCount;
end;

procedure GetIRect (FrameRect, PrjDRect: TDRect; XDim, YDim: integer; var PrjRect: TRect);
var AverX, AverY: double;
   iBuf: integer;
begin
   AverX := FrameRect.XSize / XDim;
   AverY := FrameRect.YSize / YDim;

   PrjRect.Left := Ceil ((PrjDRect.A.X - FrameRect.A.X) / AverX);
   PrjRect.Right := Ceil ((PrjDRect.B.X - FrameRect.B.X) / AverX) + XDim - 1;
   PrjRect.Top := Ceil ((FrameRect.B.Y - PrjDRect.B.Y) / AverY);
   PrjRect.Bottom := Ceil ((FrameRect.A.Y - PrjDRect.A.Y) / AverY) + YDim - 1;
end;


initialization
   MenuFunctions.RegisterFromResource (HInstance, 'NewImageHndl', 'Hint');
   TIntersectedSave.Registrate ('MultiCopyAndSave');
   TIntersectedReplace.Registrate ('MultiCopyAndReplace');
   TIntersectedCombine.Registrate ('MultiCopyCombineAndSave');
end.

