unit PrnRep;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, MultiLng, PrintOptions;

type
  THeaderInfo = record
    Date: TDateTime;
    RegOwner: AnsiString;
    RegOrg: AnsiString;
    MailAddress: AnsiString;
    MailDisplayName: AnsiString;
    ComputerName: AnsiString;
    LogonName: AnsiString;
    WingisVersion: AnsiString;
    WingisBuild: AnsiString;
    ProjectName: AnsiString;
  end;

  TPrinterInfo = record
    PrinterType: AnsiString;
    Copies: Integer;
    PaperFormat: AnsiString;
    PrintToFile: Boolean;
    PrintToWMF: Boolean;
  end;


type
  TPrintReport = class(TForm)
    MlgSection: TMlgSection;
    Bevel1: TBevel;
    ButtonSave: TButton;
    ButtonClose: TButton;
    Memo: TMemo;
    SaveDialog: TSaveDialog;
    procedure ButtonSaveClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    HeaderInfo: THeaderInfo;
    PrinterInfo: TPrinterInfo;
  public
    Active: Boolean;
  end;

var
  PrintReport: TPrintReport;

procedure PrintReportShow;
procedure PrintReportGetHeader;
procedure PrintReportGetPrinter(PrintOptions: TPrintOptions);

implementation

uses Registry, AM_Main, LicenseHndl;

{$R *.DFM}

procedure PrintReportShow;
begin
     if (PrintReport <> nil) and (PrintReport.Active) then begin
        PrintReport.ShowModal;
     end;
end;

procedure PrintReportGetHeader;
const
os_None     = 0;
os_Windows  = 1;
os_NT       = 2;
BUFSIZE     = 256;
var
Reg: TRegistry;
OSInfo: OSVERSIONINFO;
PlatformId: Integer;
Buffer: Array[0..BUFSIZE-1] of Char;
n: DWord;
FileVersionInfo: PChar;
FileOffset: DWord;
StructSize: Integer;
InfoText: PChar;
s: AnsiString;
begin
     if (PrintReport <> nil) and (PrintReport.Active) then begin
        PlatformId:=os_None;
        OSInfo.dwOSVersionInfoSize:=SizeOf(OSVERSIONINFO);
        if GetVersionEx(OSInfo) then begin
           case OSInfo.dwPlatformId of
                VER_PLATFORM_WIN32_WINDOWS: PlatformId:=os_Windows;
                VER_PLATFORM_WIN32_NT: PlatformId:=os_NT;
           end;
        end;

        PrintReport.HeaderInfo.Date:=Now;

        PrintReport.HeaderInfo.ComputerName:='';
        n:=BUFSIZE;
        if GetComputerName(Buffer,n) then begin
           Buffer[n]:=#0;
           PrintReport.HeaderInfo.ComputerName:=AnsiString(Buffer);
        end;

        PrintReport.HeaderInfo.LogonName:='';
        n:=BUFSIZE;
        if GetUserName(Buffer,n) then begin
           Buffer[n]:=#0;
           PrintReport.HeaderInfo.LogonName:=AnsiString(Buffer);
        end;

        PrintReport.HeaderInfo.WingisVersion:=Application.Title;

        PrintReport.HeaderInfo.WingisBuild:='';
        GetModuleFileName(HInstance,Buffer,BUFSIZE);
        StructSize:=GetFileVersionInfoSize(Buffer,FileOffset);
        if StructSize > 0 then begin
           GetMem(FileVersionInfo,StructSize);
           if FileVersionInfo <> nil then begin
              if GetFileVersionInfo(Buffer,FileOffset,StructSize,FileVersionInfo) then begin
                 if VerQueryValue(FileVersionInfo,'\StringFileInfo\080904E4\FileVersion',Pointer(InfoText),n) then begin
                    PrintReport.HeaderInfo.WingisBuild:=AnsiString(InfoText);
                 end;
              end;
              FreeMem(FileVersionInfo,StructSize);
           end;
        end;

        Reg:=TRegistry.Create;
        try
           PrintReport.HeaderInfo.MailAddress:='';
           PrintReport.HeaderInfo.MailDisplayName:='';
           Reg.RootKey:=HKEY_CURRENT_USER;
           if Reg.OpenKeyReadOnly('\Software\Microsoft\Internet Account Manager') then begin
              if Reg.ValueExists('Default Mail Account') then begin
                 s:=Reg.ReadString('Default Mail Account');
                 if Reg.OpenKeyReadOnly('\Software\Microsoft\Internet Account Manager\Accounts\' + s) then begin
                    if Reg.ValueExists('SMTP Email Address') then begin
                       PrintReport.HeaderInfo.MailAddress:=Reg.ReadString('SMTP Email Address');
                    end;
                    if Reg.ValueExists('SMTP Display Name') then begin
                       PrintReport.HeaderInfo.MailDisplayName:=Reg.ReadString('SMTP Display Name');
                    end;
                 end;
              end;
           end;

           PrintReport.HeaderInfo.RegOwner:='';
           PrintReport.HeaderInfo.RegOrg:='';
           s:='';
           Reg.RootKey:=HKEY_LOCAL_MACHINE;
           if PlatformId = os_Windows then begin
              s:='\Software\Microsoft\Windows\CurrentVersion';
           end
           else if PlatformId = os_NT then begin
              s:='\Software\Microsoft\Windows NT\CurrentVersion';
           end;
           if (s <> '') and (Reg.OpenKeyReadOnly(s)) then begin
              if Reg.ValueExists('RegisteredOwner') then begin
                 PrintReport.HeaderInfo.RegOwner:=Reg.ReadString('RegisteredOwner');
              end;
              if Reg.ValueExists('RegisteredOrganization') then begin
                 PrintReport.HeaderInfo.RegOrg:=Reg.ReadString('RegisteredOrganization');
              end;
           end;
        finally
           Reg.Free;
        end;

        PrintReport.HeaderInfo.ProjectName:='';
        if WingisMainForm.ActualChild <> nil then begin
           PrintReport.HeaderInfo.ProjectName:=ExtractFileName(WingisMainForm.ActualChild.FName);
        end;
     end;
end;

procedure PrintReportGetPrinter(PrintOptions: TPrintOptions);
begin
     if (PrintReport <> nil) and (PrintReport.Active) then begin
        PrintReport.PrinterInfo.PrinterType:=PrintOptions.PageSetup.Printer.Typ;
        PrintReport.PrinterInfo.Copies:=PrintOptions.PageSetup.Copies;
        PrintReport.PrinterInfo.PaperFormat:=PrintOptions.PageSetup.ActivePaperFormat.Name;
        PrintReport.PrinterInfo.PrintToFile:=PrintOptions.PrintToFile;
        PrintReport.PrinterInfo.PrintToWMF:=PrintOptions.PageSetup.PrintToWMF;
     end;
end;
////////////////////////////////////////////////////////////////////////////////

procedure TPrintReport.FormCreate(Sender: TObject);
begin
     Active:=False;
end;

procedure TPrintReport.FormShow(Sender: TObject);
begin
     Memo.Clear;
     Memo.Lines.Add('Date: ' + FormatDateTime('dd.mm.yyyy hh:nn',HeaderInfo.Date));
     Memo.Lines.Add('OwnerName: ' + HeaderInfo.RegOwner);
     Memo.Lines.Add('OwnerOrg: ' + HeaderInfo.RegOrg);
     Memo.Lines.Add('MailAddress: ' + HeaderInfo.MailAddress);
     Memo.Lines.Add('MailDisplayName: ' + HeaderInfo.MailDisplayName);
     Memo.Lines.Add('ComputerName: ' + HeaderInfo.ComputerName);
     Memo.Lines.Add('LogonName: ' + HeaderInfo.LogonName);
     Memo.Lines.Add('WingisVersion: ' + HeaderInfo.WingisVersion);
     Memo.Lines.Add('WingisBuild: ' + HeaderInfo.WingisBuild);
     Memo.Lines.Add('Project: ' + HeaderInfo.ProjectName);

     Memo.Lines.Add('------------------------------');

     Memo.Lines.Add('PrinterType: ' + PrinterInfo.PrinterType);
     Memo.Lines.Add('Copies: ' + IntToStr(PrinterInfo.Copies));
     Memo.Lines.Add('PaperFormat: ' + PrinterInfo.PaperFormat);
     Memo.Lines.Add('PrintToFile: ' + IntToStr(Ord(PrinterInfo.PrintToFile)));
     Memo.Lines.Add('PrintToWMF: ' + IntToStr(Ord(PrinterInfo.PrintToWMF)));

     ButtonSave.Enabled:=(Memo.Lines.Count > 0);
end;

procedure TPrintReport.ButtonSaveClick(Sender: TObject);
begin
     if SaveDialog.Execute then begin
        Memo.Lines.SaveToFile(SaveDialog.Filename);
     end;
end;

end.
