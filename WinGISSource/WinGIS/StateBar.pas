Unit StateBar;

Interface

Uses Classes,Controls,ExtCtrls,Messages,WCtrls,WinTypes,
     CommCtrl,ComCtrls;

Type TWinGISStatusBar   = Class(TWStatusBar)
      Protected
       Procedure   Resize; override;
      Public
       Constructor Create(AOwner:TComponent); override;
     end;

Procedure InitStatusBar;

Implementation

Uses AM_Main,Dialogs,Forms,Graphics,SysUtils,UserIntf,WinProcs;

Procedure InitStatusBar;
  begin
    StatusBar:=TWinGISStatusBar.Create(WinGISMainForm);
    StatusBar.Parent:=WinGISMainForm;
  end;

Constructor TWinGISStatusBar.Create
   (
   AOwner          : TComponent
   );
  var StatusPanel  : TStatusPanel;
  begin
    inherited Create(AOwner);
    StatusPanel:=TStatusPanel.Create(Panels);
    StatusPanel.Width:=180;
    StatusPanel:=TStatusPanel.Create(Panels);
    StatusPanel:=TStatusPanel.Create(Panels);
    StatusPanel.Width:=90;
    StatusPanel:=TStatusPanel.Create(Panels);
    StatusPanel.Bevel:=pbNone;
  end;

Procedure TWinGISStatusBar.Resize;
  var NewWidth     : Integer;
  begin
    if not ProgressPanel then begin
      NewWidth:=Width-290;
      if NewWidth<120 then NewWidth:=120;
      Panels[1].Width:=NewWidth;
    end;
    inherited Resize;
  end;

end.
