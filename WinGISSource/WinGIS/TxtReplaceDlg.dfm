�
 TTXTREPLACEDIALOG 0�  TPF0TTxtReplaceDialogTxtReplaceDialogLeft�TopHelpContext�BorderStylebsDialogCaption!31ClientHeightClientWidth`Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrder	PositionpoScreenCenterOnCreate
FormCreateOnHideFormHideOnShowFormShowPixelsPerInch`
TextHeight TBevelBevel1LeftTop� WidthyHeightAnchorsakLeftakBottom ShapebsBottomLine  TButton	CancelBtnLeft� Top� WidthWHeightAnchorsakLeftakBottom Cancel	Caption!34ModalResultTabOrder  TButtonOKBtnLeftKTop� WidthWHeightAnchorsakLeftakBottom Caption!33Default	ModalResultTabOrder   TPanelControlPanel1Left Top�Width�HeightAnchorsakLeftakTopakBottom 
BevelOuterbvNoneTabOrder TPanelControlPanelLeft TopWidth�Height� AnchorsakLeftakTopakBottom 
BevelOuterbvNoneTabOrder  TWLabelWLabel3LeftTopWidth� HeightAutoSizeCaption!22FocusControledtSearchFor  TWLabelWLabel1Left	Top� Width� HeightAutoSizeCaption!23FocusControledtReplaceBy  TWLabelWLabel2Left
Top2Width� HeightAutoSizeCaption!32  TEditedtSearchForLeftTopWidthMHeightTabOrder OnChangeedtOnInvalidAction  TEditedtReplaceByLeft	Top� WidthMHeightTabOrder  	TRichEditredTextLeft	TopCWidthMHeightSLines.StringsredText 
ScrollBarsssBothTabOrderOnChangeedtOnInvalidAction  	TCheckBoxChkZoomFoundLeft
Top� Width� HeightCaption!36TabOrderOnClickChkZoomFoundOnClick  	TCheckBoxChkRestViewLeft� Top� Width� HeightCaption!37TabOrderOnClickChkRestViewOnClick    TButtonButton1LeftTop� WidthWHeightAnchorsakLeftakBottom Caption!35ModalResultTabOrder  TMlgSection
MlgSectionSectionTxtSearchDialogLeftTop�   TTimertim100EnabledOnTimer
OnTimer100Left)Top�    