unit IDB_Utils;
{
Internal database. Ver. II.
This module provides some functions that are useful for Internal database.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 01-02-2001
}
interface        

function MakeDBFileNameFromProjectFileName (sProjectFileName: AnsiString): AnsiString;
function FieldIsCalculatedField (sFieldName: AnsiString): Boolean;

implementation

uses SysUtils,
   IDB_Consts;

function MakeDBFileNameFromProjectFileName (sProjectFileName: AnsiString): AnsiString;
var
   iP: Integer;
   bF: Boolean;
begin
   bF := FALSE;
   for iP := Length (sProjectFileName) downto 1 do
      if Copy (sProjectFileName, iP, 1) = '.' then
      begin
         bF := TRUE;
         BREAK;
      end;
   if not bF then iP := Length (sProjectFileName);
   RESULT := Copy (sProjectFileName, 1, iP) + cs_IDB_DatabaseFileExt;
end;

function FieldIsCalculatedField (sFieldName: AnsiString): Boolean;
begin
   RESULT := FALSE;
   sFieldName := AnsiUpperCase (sFieldName);
   if (sFieldName = AnsiUpperCase (cs_CalcFieldAreaName)) or // '_Area';
// ++ Cadmensky IDB Version 2.3.8
      (sFieldName = AnsiUpperCase (cs_CalcFieldSizeName)) or  // '_Size';
      (sFieldName = AnsiUpperCase (cs_CalcFieldLinkIDName)) or // '_LinkID';
      (sFieldName = AnsiUpperCase (cs_CalcFieldTextName)) or  // '_Text';
//      (sFieldName = AnsiUpperCase (cs_CalcFieldLengthName)) or  // '_Length';
//      (sFieldName = AnsiUpperCase (cs_CalcFieldPerimeterName)) or  // '_Perimeter';
// -- Cadmensky IDB Version 2.3.8
      (sFieldName = AnsiUpperCase (cs_CalcFieldXName)) or  // '_X';
      (sFieldName = AnsiUpperCase (cs_CalcFieldYName)) or  // '_Y';
      (sFieldName = AnsiUpperCase (cs_CalcFieldVerticesCount)) or  // '_Vertices';
      (sFieldName = AnsiUpperCase (cs_CalcFieldSymbolName)) then // '_Symbol Name';
      RESULT := TRUE;
end;

end.

