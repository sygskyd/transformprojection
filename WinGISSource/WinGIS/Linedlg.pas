Unit Linedlg;

{
Correction: 27.07.2000 by Dennis Ivanoff, Progis-Moscow, Russia
}

Interface
{$IFNDEF AXDLL} // <----------------- AXDLL
Uses AM_Def,ColorSelector,Combobtn,Controls,DipGrid,Forms,Graphics,StdCtrls,
     Validate,Spinbtn,ExtCtrls,PropCollect,StyleDef,WinTypes,Classes,Menus,
     WCtrls,XLines,XStyles,MultiLng,ProjStyle,AM_Proj,Windows;

Type TLineStyleDialog   = Class(TWForm)
       AddMenu          : TMenuItem;
       Bevel1           : TBevel;
       CancelBtn        : TButton;
       ColorComboBtn    : TColorSelectBtn;
       ControlsPanel    : TPanel;
       DeleteMenu       : TMenuItem;
       FillColorComboBtn: TColorSelectBtn;
       MlgSection       : TMlgSection;
       Label3           : TWLabel;
       WidthLabel       : TWLabel;
       LineStylesList   : TDipList;
       N1               : TMenuItem;
       OKBtn            : TButton;
       OrganizeMenu     : TMenuItem;
       PopupMenu        : TPopupMenu;
       PropertiesMenu   : TMenuItem;
       ScaleEdit        : TEdit;
       ScaleIndependCheck: TCheckBox;
       ScaleSpin        : TSpinBtn;
       ScaleVal         : TMeasureLookupValidator;
       WidthEdit        : TEdit;
       WidthSpin        : TSpinBtn;
       WidthVal         : TMeasureLookupValidator;
       WLabel1          : TWLabel;
       FillColorLabel   : TWLabel;
       ScaleLabel       : TWLabel;
       N2               : TMenuItem;
       ChkChangeDirection: TCheckBox;
       Procedure   AddMenuClick(Sender: TObject);
       Procedure   OrganizeMenuClick(Sender: TObject);
       Procedure   DeleteMenuClick(Sender: TObject);
       Procedure   FormCloseQuery(Sender: TObject; var CanClose: Boolean);
       Procedure   FormCreate(Sender: TObject);
       Procedure   FormDestroy(Sender: TObject);
       Procedure   FormHide(Sender: TObject);
       Procedure   FormShow(Sender: TObject);
       Procedure   LineStylesListDblClick(Sender: TObject);
       Procedure   LineStylesListDrawCell(Sender: TObject; Canvas: TCanvas; Index: Integer; Rect: TRect);
       Procedure   LineStylesListClick(Sender: TObject);
       Procedure   OnChangeColor(Sender: TObject);
       Procedure   PropertiesMenuClick(Sender: TObject);
       Procedure   WidthEditChange(Sender: TObject);
       Procedure   WidthEditExit(Sender: TObject);
    procedure ScaleIndependCheckClick(Sender: TObject);
    procedure LineStylesListMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
      Private
       FAllowXLineStyles: Boolean;
       FOrgProjStyles   : TProjectStyles;
       FProjStyles      : TProjectStyles;
       FOrgStyle        : TLineProperties;
       FStyle           : TLineProperties;
       FProj            : PProj;
       FXLineList       : TStringList;
       Procedure   UpdateEnabled;
       Procedure   UpdateStylesList;
       Procedure   CheckSizeUnit;
      Public
       Property    AllowXLineStyles:Boolean read FAllowXLineStyles write FAllowXLineStyles default TRUE;
       Property    LineStyle:TLineProperties read FStyle write FOrgStyle;
       Property    Project:PProj read FProj write FProj;
       Property    ProjectStyles:TProjectStyles read FProjStyles write FOrgProjStyles;
     end;
{$ENDIF} // <----------------- AXDLL
Implementation
{$IFNDEF AXDLL} // <----------------- AXDLL
{$R *.DFM}
Uses CommonResources,Measures,NumTools,WinProcs
     {$IFNDEF WMLT}
     ,XLineDlg,OrganizeDlg,DK_ReplaceDlg
     {$ENDIF}
     ,AM_Obj
{++ Ivanoff Bug#320}
     ,SysUtils, Am_Layer, Am_Index, AM_Main

, Dialogs
{-- Ivanoff}
;

Const SystemLineStyles  = 5;

Procedure TLineStyleDialog.LineStylesListDrawCell(Sender: TObject; Canvas: TCanvas;
    Index:Integer; Rect: TRect);
var Y            : Integer;
    ARect        : TRect;
    Item         : TXLineStyle;
    AText        : String;
    Points       : Array[0..1] of TPoint;
    AZoom        : Double;
begin
  with Canvas do begin
    if not FStyle.UpdatesEnabled then begin
      Font.Color:=clBtnShadow;
      Pen.Color:=clBtnShadow;
    end;
    if Index<0 then begin
      AText:=FStyle.StyleOptions.ShortNames[Abs(Index)-1];
      TextRect(Rect,CenterWidthInRect(TextWidth(AText),Rect),
          CenterHeightInRect(TextHeight(AText),Rect),AText);
    end
    else begin
      FillRect(Rect);
      ARect:=Classes.Rect(Rect.Left+LineStylesList.RowHeight+3,
          Rect.Top,Rect.Left+80,Rect.Bottom);
      Y:=CenterHeightInRect(0,Rect);
      if Index<SystemLineStyles then begin
        CommonListBitmaps.Draw(Canvas,Rect.Left+2,CenterHeightInRect(14,Rect),clbSystem);
        Pen.Style:=TPenStyle(Index);
        Pen.Width:=0;
        with ARect do Polyline([Point(Left+1,Y-1),Point(Right-1,Y-1)]);
        with ARect do Polyline([Point(Left+1,Y),Point(Right-1,Y)]);
        AText:=MlgSection[20+Index];
      end
      else begin
        CommonListBitmaps.Draw(Canvas,Rect.Left+2,CenterHeightInRect(14,Rect),clbUser);
        Item:=TXLineStyle(FXLineList.Objects[Index-SystemLineStyles]);
        with ARect do begin
          Points[0]:=Point(Left+1,Y);
          Points[1]:=Point(Right-1,Y);
{++ Ivanoff BUG#New18 BUILD#106, BUG#234}
          If RectHeight(Item.ClipRect) = 0 Then
{++ New XLine BUG#251}
// Incorrect calculating of the scale.
//             AZoom:=(LineStylesList.RowHeight-2)*5{AZoom:=1.7E10308}
             AZoom := LineStylesList.CellWidth / Item.Items[0].Cycle / 10
{-- New XLine BUG#251}
{-- Ivanoff}
          else AZoom:=(LineStylesList.RowHeight-2)/RectHeight(Item.ClipRect)/5;
          XPolyline(Canvas.Handle,Points,2,Item.StyleDef^.XLineDefs,
              Item.StyleDef^.Count,ColorComboBtn.SelectedColor,
              FillColorComboBtn.SelectedColor,True,AZoom);
        end;
        AText:=Item.Name;
      end;
      ARect:=InflatedWidth(Rect,-85,0);
      TextRect(ARect,ARect.Left,CenterHeightInRect(TextHeight(AText),ARect),AText);
    end;
  end;
end;

Procedure TLineStyleDialog.FormCreate(Sender: TObject);
begin
  FAllowXLineStyles:=TRUE;
  FStyle:=TLineProperties.Create;
  FProjStyles:=TProjectStyles.Create;
  FXLineList:=TStringList.Create;
//  FXLineList.Sorted:=TRUE;
  FXLineList.Duplicates:=dupAccept;
  ColorComboBtn.Palette:=FProjStyles.Palette;
  FillColorComboBtn.Palette:=FProjStyles.Palette;
  {$IFDEF WMLT}
  Popupmenu.AutoPopup:=False;
  {$ENDIF}
end;

Procedure TLineStyleDialog.FormDestroy(Sender: TObject);
begin
  FStyle.Free;
  FProjStyles.Free;
  FXLineList.Free;
end;

Procedure TLineStyleDialog.FormShow(Sender: TObject);
var XLineStyle   : TXLineStyle;
begin
  if FOrgProjStyles<>NIL then FProjStyles.Assign(FOrgProjStyles);
  UpdateStylesList;
  with FStyle do begin
    PropertyMode:=pmSet;
    if FOrgStyle<>NIL then FStyle.Assign(FOrgStyle);
    { initialize color-dropdowns }
    ColorComboBtn.Options:=ColorOptions;
    ColorComboBtn.SelectedColor:=Color;
    FillColorComboBtn.Options:=FillColorOptions;
    FillColorComboBtn.SelectedColor:=FillColor;
    { initialize the line-style listbox }
    with LineStylesList do begin
      if AllowXLineStyles then Count:=FProjStyles.XLineStyles.Count+SystemLineStyles
      else Count:=SystemLineStyles;
      HeaderRows:=StyleOptions.Count;
      if Style<0 then ItemIndex:=-StyleOptions.IndexOfValue(Style)-1
      else if Style<lt_UserDefined then ItemIndex:=Style
      else begin
        XLineStyle:=FProjStyles.XLineStyles.NumberedStyles[Style];
        if XLineStyle=NIL then ItemIndex:=0
        else ItemIndex:=FXLineList.IndexOfObject(XLineStyle)+SystemLineStyles;
      end;
    end;
    { add optional line-styles to the list }       
    WidthVal.LookupList.Clear;
    SetSizeField(Width,WidthType,WidthVal,WidthOptions,ScaleIndependCheck);
    ScaleVal.LookupList.Clear;
    SetDoubleField(Scale,100,ScaleVal,muPercent,ScaleOptions);
  end;
  UpdateEnabled;
end;

Procedure TLineStyleDialog.LineStylesListDblClick(Sender: TObject);
begin
  ModalResult:=mrOK;
end;

Procedure TLineStyleDialog.LineStylesListClick(Sender: TObject);
begin
  { update enabled-state of entry-fields }
  UpdateEnabled;
end;

Procedure TLineStyleDialog.FormHide(Sender: TObject);
begin                                                        
  if ModalResult=mrOK then with FStyle do begin
    { get colors from color-buttons }
    Color:=ColorComboBtn.SelectedColor;
    FillColor:=FillColorComboBtn.SelectedColor;
    { determine style to use, optional style, windows style or user-defined style }
    with LineStylesList do
         if ItemIndex<0 then
            Style:=StyleOptions.Values[Abs(ItemIndex)-1]
         else if ItemIndex<SystemLineStyles then
            Style:=ItemIndex
         else
            Style:=TXLineStyle(FXLineList.Objects[ItemIndex-SystemLineStyles]).Number;

         Width:=GetSizeField(WidthVal,WidthOptions,ScaleIndependCheck);
         WidthType:=GetSizeTypeField(WidthVal,WidthOptions,ScaleIndependCheck);
         Scale:=GetDoubleField(1/100,ScaleVal,muPercent,ScaleOptions);

         { update original styles if set }
         if FOrgStyle<>NIL then
            FOrgStyle.Assign(FStyle);

         if FOrgProjStyles<>NIL then
            FOrgProjStyles.Assign(FProjStyles);
    end;
end;

Procedure TLineStyleDialog.WidthEditChange(Sender: TObject);
var AEnable      : Boolean;
begin
  if WidthEdit.Enabled then begin
    AEnable:=(WidthVal.LookupIndex=-1) and not (WidthVal.Units in [muDrawingUnits,muNone]);
//    if not AEnable then
//       ScaleIndependCheck.Checked:=FALSE;;
    ScaleIndependCheck.Enabled:=True;//AEnable;
  end;
end;

Procedure TLineStyleDialog.FormCloseQuery(Sender: TObject;
var CanClose: Boolean);
begin
  CanClose:=CheckValidators(Self);
end;

Procedure TLineStyleDialog.WidthEditExit(Sender: TObject);
begin
  if DblCompare(WidthVal.Value,0)=0 then WidthVal.LookupIndex:=0;
end;

Procedure TLineStyleDialog.PropertiesMenuClick(Sender: TObject);
{$IFNDEF WMLT}
var LineStyleDialog   : TXLineStyleDialog;
{$ENDIF}
begin
{$IFNDEF WMLT}
  LineStyleDialog:=TXLineStyleDialog.Create(Self);
  try
    LineStyleDialog.XLineStyle:=TXLineStyle(FXLineList
        .Objects[LineStylesList.ItemIndex-SystemLineStyles]);
    LineStyleDialog.XLineStyles:=FProjStyles.XLineStyles;
    LineStyleDialog.Palette:=FProjStyles.Palette;
    if LineStyleDialog.ShowModal=mrOK then begin
      LineStylesList.InvalidateDip(LineStylesList.ItemIndex);
    end;
  finally
    LineStyleDialog.Free;
  end;
{$ENDIF}
end;

Procedure TLineStyleDialog.UpdateEnabled;
var AEnabled     : Boolean;
    AColor       : TColor;
begin
  // enable/disable all controls depending on the updates-enabled flag
  if not LineStyle.UpdatesEnabled then SetControlGroupEnabled(ControlsPanel,FALSE)
  else with LineStylesList do begin
    AddMenu.Enabled:=FAllowXLineStyles;
    OrganizeMenu.Enabled:=FProj<>NIL;
    AEnabled:= ItemIndex>=SystemLineStyles;
//++ Glukhov Bug#580 Build#166 27.11.01
    // A line width can be set for a solid line or for a user defined style
    if AEnabled or (TPenStyle(ItemIndex) = psSolid) then begin
       WidthEdit.Enabled:= True
    end
    else begin
       WidthEdit.Enabled:= False;
       WidthVal.AsFloat:=0;
    end;
    ScaleEdit.Enabled:= AEnabled;
//-- Glukhov Bug#580 Build#166 27.11.01
//    if ItemIndex=0 then AColor:=clWindowText
//    else AColor:=clActiveCaption;
    WidthLabel.Enabled:=WidthEdit.Enabled;
//++ Glukhov Bug#580 Build#166 27.11.01
    ScaleIndependCheck.Enabled:=AEnabled or WidthEdit.Enabled;
    // ScaleIndependCheck has sense for a user defined style only
//    ScaleIndependCheck.Enabled:= True;//AEnabled;
//    if not ScaleIndependCheck.Enabled then
//       ScaleIndependCheck.Checked:= False;
//-- Glukhov Bug#580 Build#166 27.11.01
    DeleteMenu.Enabled:=AEnabled;
    PropertiesMenu.Enabled:=AEnabled;
//    if AEnabled then AColor:=clWindowText
//    else AColor:=clActiveCaption;
//    FillColorLabel.Font.Color:=AColor;
//    ScaleLabel.Font.Color:=AColor;
    FillColorLabel.Enabled:=AEnabled;
    ScaleLabel.Enabled:=AEnabled;
    FillColorComboBtn.Enabled:=AEnabled;
    if ItemIndex>=SystemLineStyles then begin
       WidthEdit.Enabled:= False;
       WidthVal.AsFloat:=0;
       WidthLabel.Enabled:=WidthEdit.Enabled;
//       ScaleIndependCheck.Checked:=True;
//       ScaleIndependCheck.Enabled:=False;
    end;
    CheckSizeUnit;
  end;
end;

Procedure TLineStyleDialog.AddMenuClick(Sender: TObject);
{$IFNDEF WMLT}
var LineStyleDialog   : TXLineStyleDialog;
    NewStyle          : TXLineStyle;
{$ENDIF}
begin
{$IFNDEF WMLT}
  LineStyleDialog:=TXLineStyleDialog.Create(Self);
  try
    LineStyleDialog.XLineStyles:=FProjStyles.XLineStyles;
    LineStyleDialog.Palette:=FProjStyles.Palette;
    if LineStyleDialog.ShowModal=mrOK then begin
      NewStyle:=TXLineStyle.Create;
      NewStyle.Assign(LineStyleDialog.XLineStyle);
      FProjStyles.XLineStyles.Add(NewStyle);
      LineStylesList.Count:=FProjStyles.XLineStyles.Count+SystemLineStyles;
      LineStylesList.ItemIndex:=FXLineList.AddObject(NewStyle.Name,NewStyle)
          +SystemLineStyles;
    end;
  finally
    LineStyleDialog.Free;
  end;
{$ENDIF}
end;

Procedure TLineStyleDialog.OrganizeMenuClick(Sender: TObject);
{$IFNDEF WMLT}
var Dialog       : TOrganizeDialog;
{$ENDIF}
begin
{$IFNDEF WMLT}
  if FProj<>NIL then begin
    Dialog:=TOrganizeDialog.Create(Self);
    with Dialog do try
      XLineStyles:=FProjStyles.XLineStyles;
      XFillStyles:=FProjStyles.XFillStyles;
      Symbols:=PSymbols(FProj^.PInfo^.Symbols);
      Fonts:=FProj^.PInfo^.Fonts;
      StartupPage:=opLineStyles;
      if ShowModal=mrOK then begin
        UpdateStylesList;
        LineStylesList.Count:=FProjStyles.XLineStyles.Count+SystemLineStyles;
        LineStylesList.Invalidate;
        if ProjectModified then FProj^.SetModified;
      end;
    finally
      Dialog.Free;
    end;
  end;
{$ENDIF}
end;

Procedure TLineStyleDialog.DeleteMenuClick(Sender: TObject);
{$IFNDEF WMLT}
var Item         : TXLineStyle;
{++ Ivanoff Bug#320}
    RD: TDKReplaceDialog;
    iI, iJ, iK, iRefCount, iUserChoice: Integer;
    AProject: PProj;
    ALayer: PLayer;
    ALayerItem: PIndex;
{-- Ivanoff}
{$ENDIF}
begin
{$IFNDEF WMLT}
  with LineStylesList do begin
    Item:=TXlineStyle(FXLineList.Objects[ItemIndex-SystemLineStyles]);
{++ Ivanoff Bug#320}
    AProject := WinGisMainForm.ActualProj;
    iRefCount := 0;
    For iI:=1 To AProject.Layers.LData.Count-1 Do Begin
        ALayer := AProject.Layers.LData.At(iI);
        If ALayer.LineStyle.Style = Item.Number Then Inc(iRefCount);
        End; // For iI end
    If iRefCount > 0 Then Begin
       MessageBox(Handle, PChar(Format(MlgSection[26], [iRefCount])), 'WinGIS', MB_OK + MB_DEFBUTTON1 + MB_ICONWARNING);
       EXIT;
       End;
    If Item.ReferenceCount > 1 Then Begin
       If MessageBox(Handle, PChar(Format(MlgSection[25], [Item.ReferenceCount])), 'WinGIS', MB_YESNO + MB_DEFBUTTON1 + MB_ICONQUESTION) = mrNO Then EXIT;
       RD := TDKReplaceDialog.Create(SELF);
       RD.SetLineStyles(Item.Name, LineStyle, FXLineList, MlgSection);
       If RD.ShowModal <> mrOK Then EXIT;
       iUserChoice := RD.GetUserSelect;         // With what replace
       For iI:=1 To AProject.Layers.LData.Count-1 Do Begin
           ALayer := AProject.Layers.LData.At(iI);
           // Check the layer line style.
           If ALayer.LineStyle.Style = Item.Number Then
              ALayer.LineStyle.Style := iUserChoice;
           For iJ:=0 To ALayer.Data.GetCount-1 Do Begin
               ALayerItem := ALayer.Data.At(iJ);
               // Check the layer's object line style.
               If ALAyerItem.ObjectStyle = NIL Then CONTINUE;
               If ALayerItem.ObjectStyle.LineStyle.Style = Item.Number Then
                  ALayerItem.ObjectStyle.LineStyle.Style := iUserChoice;
               End; // For iJ end
           End; // For iI end
       RD.Free;
       End;
{-- Ivanoff Bug#320}
    FXLineList.Delete(ItemIndex-SystemLineStyles);
    FProjStyles.XLineStyles.Delete(Item);
    Item.Free;
    ItemIndex:=Min(ItemIndex,Count-2);
    Count:=FProjStyles.XLineStyles.Count+SystemLineStyles;
{++ Ivanoff}
    AProject.SetModified;
    AProject.PInfo.RedrawScreen(TRUE);
{-- Ivanoff}
  end;
{$ENDIF}
end;

Procedure TLineStyleDialog.UpdateStylesList;
var XLineStyle   : TXLineStyle;
    Cnt          : Integer;
begin
  FXLineList.Clear;
  for Cnt:=0 to FProjStyles.XLineStyles.Capacity-1 do begin
    XLineStyle:=FProjStyles.XLineStyles.Items[Cnt];
    if XLineStyle<>NIL then FXLineList.AddObject(XLineStyle.Name,XLineStyle);
  end;
end;

Procedure TLineStyleDialog.OnChangeColor(Sender: TObject);
begin
  LineStylesList.InvalidateItems(SystemLineStyles,SystemLineStyles+FXLineList.Count-1);
end;

procedure TLineStyleDialog.CheckSizeUnit;
begin
     if ScaleIndependCheck.Checked then begin
        WidthVal.Units:=muMillimeters;
        WidthVal.UseableUnits:=32;
        WidthVal.MinValue.Value:=0;
        WidthVal.MinValue.Units:=muMillimeters;
        WidthVal.MaxValue.Units:=muMillimeters;
     end
     else begin
        WidthVal.Units:=muDrawingUnits;
        WidthVal.UseableUnits:=2;
        WidthVal.MinValue.Value:=0;
        WidthVal.MinValue.Units:=muDrawingUnits;
        WidthVal.MaxValue.Units:=muDrawingUnits;
     end;
end;

procedure TLineStyleDialog.ScaleIndependCheckClick(Sender: TObject);
begin
  CheckSizeUnit;
  try
     if (WidthEdit.Enabled) and (WidthEdit.Visible) then begin
        Windows.SetFocus(WidthEdit.Handle);
     end;
     if Sender <> nil then begin
        if TWinControl(Sender).Enabled then begin
           Windows.SetFocus(TWinControl(Sender).Handle);
        end;
     end;
  except
  end;
end;

procedure TLineStyleDialog.LineStylesListMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var
Pt: TPoint;
Index: Integer;
begin
     Pt.x:=x;
     Pt.y:=y;
     Index:=LineStylesList.ItemAtPos(Pt,True);
     if Index >= SystemLineStyles then begin
        LineStylesList.Hint:=IntToStr(TXLineStyle(FXLineList.Objects[Index-SystemLineStyles]).Number - 995);
     end
     else begin
        LineStylesList.Hint:='';
     end;
end;
{$ENDIF} // <----------------- AXDLL

end.
