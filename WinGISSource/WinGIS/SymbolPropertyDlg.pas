unit SymbolPropertyDlg;

interface
{$IFNDEF AXDLL} // <----------------- AXDLL
uses WinTypes, WinProcs, Classes, Graphics, Forms, Controls, SysUtils, StdCtrls, ExtCtrls,
  Validate, Dialogs, AM_Group, AM_Obj, AM_Def, AM_Proj, UserIntf, DipGrid, AM_Font, Spinbtn,
  MultiLng, WCtrls, LinkDlg, Objects;

type
  TSymbolPropertyDialog = class(TWForm)
    AngleSpin: TSpinBtn;
    AngleVal: TMeasureLookupValidator;
    Bevel1: TBevel;
    CancelBtn: TButton;
    DefaultSizeVal: TMeasureLookupValidator;
    EdDate: TEdit;
    EdSymAngle: TEdit;
    EdSymName: TEdit;
    EdSymSize: TEdit;
    EdSymLib: TEdit;
    EdUseCnt: TEdit;
    Label1: TWLabel;
    Label2: TWLabel;
    Label3: TWLabel;
    Label4: TWLabel;
    Label6: TWLabel;
    Label7: TWLabel;
    MlgSection1: TMlgSection;
    OKBtn: TButton;
    SearchBtn: TButton;
    ScaleIndependCheck: TCheckBox;
    SizeSpin: TSpinBtn;
    SymbolWindow: TWindow;
    AdditionalBtn: TButton;
    procedure AngleValError(Sender: TObject; ErrorCode: Word);
    procedure DefaultSizeValError(Sender: TObject; ErrorCode: Word);
    procedure EdSymAngleChange(Sender: TObject);
    procedure EdSymLibChange(Sender: TObject);
    procedure EdSymNameChange(Sender: TObject);
    procedure EdSymSizeChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormShow(Sender: TObject);
    procedure ScaleIndependCheckClick(Sender: TObject);
    procedure SymDipListDrawCell(Sender: TObject; Canvas: TCanvas; Index: Integer;
      Rect: TRect);
    procedure SymbolWindowPaint(Sender: TObject);

    procedure OKBtnClick(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure AdditionalBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    FSymbol: pSGroup;
    FSymbols: pSymbols;
    FProjSymbols: pSymbols;
    FFonts: pFonts;
    FHasChanged: Boolean;
//++ Glukhov SearchSymbol BUILD#115 27.06.00
    biSearchMode: Boolean;
    biImageChanged: Boolean;
    {++Brovak Buh 526}
    OlS: ToldMemoryStream;
    OlSFlag: boolean;
    {--Brovak}
    procedure SetSearchMode(bVal: Boolean);
    function CheckSymName: Boolean;
//-- Glukhov SearchSymbol BUILD#115 27.06.00
    procedure FillPropDlg;
    procedure SetSymbol(ASym: pSGroup);
    procedure SetSymbols(AList: pSymbols);
    procedure SetProjSymbols(ProjSymbols: pSymbols);
    procedure UpdateEnabled;
  public
    Project: PProj;
    constructor Create(AOwner: TComponent); override;
    property Fonts: pFonts read FFonts write FFonts;
    property HasChanged: Boolean read FHasChanged;
    property ProjSymbols: pSymbols read FProjSymbols write SetProjSymbols;
    property Symbol: pSGroup read FSymbol write SetSymbol;
    property Symbols: pSymbols read FSymbols write SetSymbols;
//++ Glukhov SearchSymbol BUILD#115 27.06.00
    property bSearchMode: Boolean read biSearchMode write SetSearchMode;
//-- Glukhov SearchSymbol BUILD#115 27.06.00
  end;
{$ENDIF} // <----------------- AXDLL
implementation
{$IFNDEF AXDLL} // <----------------- AXDLL
{$R *.DFM}

uses Measures, StyleDef, Am_main, symrol, menufn;

constructor TSymbolPropertyDialog.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FHasChanged := False;
  FFonts := nil;
//++ Glukhov SearchSymbol BUILD#115 27.06.00
  bSearchMode := False;
//-- Glukhov SearchSymbol BUILD#115 27.06.00
end;

procedure TSymbolPropertyDialog.SetSymbol
  (
  ASym: PSGroup
  );
begin
  if Assigned(ASym) then
  begin
    FSymbol := ASym;
    FillPropDlg;
  end
  else
    FSymbol := nil;
end;

procedure TSymbolPropertyDialog.SetSymbols
  (
  AList: PSymbols
  );
begin
  if Assigned(AList) then
  begin
    FSymbols := AList;
    FillPropDlg;
  end
  else
    FSymbols := nil;
end;

procedure TSymbolPropertyDialog.SetProjSymbols
  (
  ProjSymbols: PSymbols
  );
begin
  FProjSymbols := ProjSymbols;
end;

//++ Glukhov SearchSymbol BUILD#115 27.06.00

procedure TSymbolPropertyDialog.SetSearchMode(bVal: Boolean);
begin
  biSearchMode := bVal;
  EdSymSize.Enabled := not bVal;
  SizeSpin.Enabled := not bVal;
  EdSymAngle.Enabled := not bVal;
  AngleSpin.Enabled := not bVal;
  ScaleIndependCheck.Enabled := not bVal;
  SearchBtn.Visible := bVal;
  SearchBtn.Enabled := bVal;
  if bVal = True then
  begin
    Self.Caption := MlgStringList['SymPropDlg', 15];
//    FillPropDlg;
  end
  else
  begin
    Self.Caption := MlgStringList['SymPropDlg', 10];
  end;
end;

// The sense of name check depends on dialog mode:
//    a new name is Ok for the "Property" mode,
//    an existing name is Ok for the "Search" mode

function TSymbolPropertyDialog.CheckSymName: Boolean;
var
  bCheck: Boolean;
begin
  Result := True;
  if EdSymName.Text = FSymbol^.GetName then
    Exit;
  bCheck := not FSymbols^.CheckName(EdSymName.Text, FSymbol^.GetLibName, FSymbol);
  Result := not (biSearchMode xor bCheck);
end;
//-- Glukhov SearchSymbol BUILD#115 27.06.00

procedure TSymbolPropertyDialog.FillPropDlg;
var
  LibName: string;
  ASym: PSGroup;
begin
  if not (Assigned(FSymbol) and Assigned(FSymbols)) then
  begin
    EdSymName.Text := ''; { leer f�llen }
    EdSymLib.Text := '';
    EdSymSize.Text := '';
    EdSymAngle.Text := '';
    ScaleIndependCheck.Checked := False;
    EdUseCnt.Text := '';
    EdDate.Text := '';
  end
  else
  begin
    LibName := Symbol^.GetLibName;
    if (LibName = '') then
      LibName := MlgStringList['SymPropDlg', 12];
    EdSymLib.Text := LibName; { wenn keine Bibliothek angegeben 'Keine' anzeigen }
    EdSymName.Text := Symbol^.GetName; { Symbolname anzeigen }

    SetSizeField(Symbol^.DefaultSize, Symbol^.DefaultSizeType,
      DefaultSizeVal, nil, ScaleIndependCheck);
    SetDoubleField(Symbol^.DefaultAngle, 1, AngleVal, muRad, nil);
    if Symbol^.GetLibName = '' then
      EdUseCnt.Text := IntToStr(Symbol^.UseCount)
    else
    begin
      ASym := FProjSymbols^.GetProjectSymbol(Symbol^.GetName, Symbol^.GetLibName);
      if ASym = nil then
        EdUseCnt.Text := '-' { Symbol noch nicht im Projekt }
      else
        EdUseCnt.Text := IntToStr(Symbol^.UseCount);
    end;
    EdDate.Text := FormatDateTime('c', Symbol^.LastChange);
  end;
  FHasChanged := False;
  UpdateEnabled;
end;

procedure TSymbolPropertyDialog.EdSymNameChange(Sender: TObject);
begin
//++ Glukhov SearchSymbol BUILD#115 27.06.00
// The sense of name check depends on dialog mode
  if (not CheckSymName) then
  begin
//  if not FSymbols^.CheckName(EdSymName.Text,FSymbol^.GetLibName,FSymbol) then begin
//-- Glukhov SearchSymbol BUILD#115 27.06.00
{++ Ivanoff BUGListFromAustria16.06.2000#8 BUILD#121}
{I moved a check symbol name operation from here to OnOKButtonClick. Here I simply
do the font color as red color.}
{-- Ivanoff}
    EdSymName.Font.Color := clRed;
//++ Glukhov SearchSymbol BUILD#115 27.06.00
// This operation can create a problem during the form initialization
//      EdSymName.SetFocus;
//-- Glukhov SearchSymbol BUILD#115 27.06.00
  end
  else
{++ Ivanoff BUGListFromAustria16.06.2000#8 BUILD#121}
  begin
{-- Ivanoff}
    FHasChanged := True;
{++ Ivanoff BUGListFromAustria16.06.2000#8 BUILD#121}
    EdSymName.Font.Color := clBlack;
  end;
{-- Ivanoff}
end;

procedure TSymbolPropertyDialog.AngleValError(Sender: TObject; ErrorCode: Word);
begin
  MessageDialog(MlgStringList['SymbolMsg', 13], mtInformation, [mbOK], 0);
  EdSymAngle.SetFocus;
end;

procedure TSymbolPropertyDialog.DefaultSizeValError(Sender: TObject; ErrorCode: Word);
begin
  MessageDialog(MlgStringList['SymbolMsg', 12], mtInformation, [mbOK], 0);
  EdSymSize.SetFocus;
end;

procedure TSymbolPropertyDialog.FormClose(Sender: TObject; var Action: TCloseAction);
var
  TmpSymbol: PSGroup;
begin
//++ Glukhov SearchSymbol BUILD#115 27.06.00
  if not bSearchMode then
//-- Glukhov SearchSymbol BUILD#115 27.06.00
  begin;

    if Assigned(FSymbol) and (ModalResult = mrOK) then
    begin
 {+++Brovak Bug 526}
      if OlsFlag = true then
      begin;
        Ols.Seek(0, soFromBeginning);
        TmpSymbol := PSGroup(Pointer(OLs.Get));
        FSymbols.UpdateSym(FSymbol, TmpSymbol);
      end;
  {--Brovak}

      if (FSymbol^.GetName <> EdSymName.Text) then { Symbolname ge�ndert !!}
        FSymbols^.ChangeName(FSymbol, EdSymName.Text);
      Symbol^.DefaultSize := GetSizeField(DefaultSizeVal, nil, ScaleIndependCheck);
      Symbol^.DefaultSizeType := GetSizeTypeField(DefaultSizeVal, nil, ScaleIndependCheck);
      Symbol^.DefaultAngle := GetDoubleField(1, AngleVal, muRad, nil);
      FSymbol^.DefaultAngle := AngleVal.AsFloat * Deg2Rad;
      FSymbol^.SetLastChange; { �nderungsdatum speichern }
    end;
  end;
end;

procedure TSymbolPropertyDialog.SymDipListDrawCell(Sender: TObject; Canvas: TCanvas; Index: Integer;
  Rect: TRect);
begin
  Canvas.FillRect(Rect);
  Canvas.Pen.Color := clBlack; { Defaultfarben f�r Symbole ohne Farben }
end;

procedure TSymbolPropertyDialog.EdSymLibChange(Sender: TObject);
begin
  FHasChanged := True;
end;

procedure TSymbolPropertyDialog.EdSymSizeChange(Sender: TObject);
begin
  FHasChanged := True;
  UpdateEnabled;
end;

procedure TSymbolPropertyDialog.EdSymAngleChange(Sender: TObject);
begin
  FHasChanged := True;
end;

procedure TSymbolPropertyDialog.ScaleIndependCheckClick(Sender: TObject);
begin
  FHasChanged := True;
  if ScaleIndependCheck.Checked then
  begin
    DefaultSizeVal.Units := muMillimeters;
  end
  else
  begin
    DefaultSizeVal.Units := muDrawingUnits;
  end;
end;
{++Brovak changed for Bug 526 }

procedure TSymbolPropertyDialog.SymbolWindowPaint(Sender: TObject);
var
  ARect: TRect;
begin
  if OlsFlag = true then
  begin;
    ARect := Rect(2, 2, SymbolWindow.Height - 4, SymbolWindow.Height - 4);
    PSGroup(Pointer(Ols.Get))^.DrawSymbolInRect(SymbolWindow.Canvas.Handle, ARect, FFonts);
  end
  else
  begin;
    if FSymbol <> nil then
    begin
      ARect := Rect(2, 2, SymbolWindow.Height - 4, SymbolWindow.Height - 4);
      FSymbol^.DrawSymbolInRect(SymbolWindow.Canvas.Handle, ARect, FFonts);
    end
  end;
end;

procedure TSymbolPropertyDialog.FormShow(Sender: TObject);
begin
  // update size of preview-window
  SymbolWindow.Height := EdDate.Top + EdDate.Height - SymbolWindow.Top;

  {+++Brovak Bug 526}
  if Project = nil then
    Project := WinGisMainForm.ActualProj;
  if (SearchBtn.Visible = false) and (Project.SymbolMode <> sym_SymbolMode) then
  begin;
    AdditionalBtn.BoundsRect := SearchBtn.BoundsRect;
    AdditionalBtn.Visible := true;
  end;
   {--Brovak}
  ScaleIndependCheckClick(Self);
end;

procedure TSymbolPropertyDialog.UpdateEnabled;
var
  AEnable: Boolean;
begin
//++ Glukhov SearchSymbol BUILD#115 27.06.00
  if bSearchMode then
    Exit;
//-- Glukhov SearchSymbol BUILD#115 27.06.00
  AEnable := not (DefaultSizeVal.Units in [muDrawingUnits, muNone]);
//  ScaleIndependCheck.Enabled:=AEnable;
end;

procedure TSymbolPropertyDialog.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  CanClose := CheckValidators(Self);
end;

{++ Ivanoff BUGListFromAustria16.06.2000#8 BUILD#121}

procedure TSymbolPropertyDialog.OKBtnClick(Sender: TObject);
begin
//++ Glukhov SearchSymbol BUILD#115 27.06.00
// The sense of name check depends on dialog mode
  if not CheckSymName then
  begin
{If an user has typed a symbol name and this name already exists, I show message about it
and set a focus on TEdit for entering the symbol name.}
// if not FSymbols^.CheckName(EdSymName.Text,FSymbol^.GetLibName,FSymbol) then begin
    if bSearchMode then
      MessageDialog(MlgStringList['SymPropDlg', 16], mtInformation, [mbOk], 0)
    else
//-- Glukhov SearchSymbol BUILD#115 27.06.00
      MessageDialog(MlgStringList['SymPropDlg', 11], mtInformation, [mbOk], 0);
    EdSymName.Font.Color := clRed;
    EdSymName.SelectAll;
    EdSymName.SetFocus;
  end
  else
    SELF.ModalResult := mrOK;

end;
{-- Ivanoff}

//++ Glukhov SearchSymbol BUILD#115 27.06.00

procedure TSymbolPropertyDialog.SearchBtnClick(Sender: TObject);
var
  iIdx: Integer;
  oSym: PSGroup;
begin
  if not HasChanged then
    Exit;
  if not CheckSymName then
//  if Symbols.CheckName(EdSymName.Text, FSymbol^.GetLibName, nil) then
  begin
  // No such name (lib)
    MessageDialog(MlgStringList['SymPropDlg', 16], mtInformation, [mbOk], 0);
    EdSymName.Font.Color := clRed;
    EdSymName.SelectAll;
    EdSymName.SetFocus;
    Exit;
  end;

  Symbol := Symbols.GetSymbol(EdSymName.Text, FSymbol^.GetLibName);
  FillPropDlg;
  SymbolWindow.Repaint;
end;
//-- Glukhov SearchSymbol BUILD#115 27.06.00
{+++Brovak Bug 526}

procedure TSymbolPropertyDialog.AdditionalBtnClick(Sender: TObject);
var
  Dialog: TLinkDialog;
  i: integer;
  Cnt: integer;
  Arect: Trect;
  TmpSymbol: PSGroup;
begin
  Dialog := TLinkDialog.Create(Parent);
  try
    if OlsFlag <> true then //at first
    begin;
      OlS := ToldMemoryStream.Create;
//      OlS.Init(0, 0);
      Ols.Put(Symbol);
    end;
    Ols.Seek(0, soFromBeginning); // position:=0;
    Dialog.SingleSymbol := Pointer(Ols.Get);
    Dialog.Project := WinGisMainForm.ActualProj;
    Dialog.CallFromMenu := false;
    Dialog.ImagesInSymbolsCheck.Checked := true;

    if Dialog.ShowModal = mrOK then
    begin
      OlSFlag := true;

      Ols.Seek(0, soFromBeginning);
      Ols.Put(Dialog.SingleSymbol);
      Ols.Seek(0, soFromBeginning);

      FHasChanged := true;
    end

  finally
    Dialog.Free;
    Screen.Cursor := crDefault;
  end;
end;

procedure TSymbolPropertyDialog.FormCreate(Sender: TObject);
begin
  OlSFlag := false;
end;

procedure TSymbolPropertyDialog.FormDestroy(Sender: TObject);
begin
     //Ols.Done;
end;
{--Brovak}
{$ENDIF} // <----------------- AXDLL
end.

