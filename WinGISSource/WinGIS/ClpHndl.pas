{*******************************************************************************
| Unit ClpHndl
|-------------------------------------------------------------------------------
| Autor: Martin Forst
|-------------------------------------------------------------------------------
| Prozeduren und Men�handler f�r das Clipboard-Interface
+******************************************************************************}
unit ClpHndl;

interface

uses AM_Def, Classes, Controls, MenuHndl, ProjHndl;

type
  TCopyHandler = class(TProjMenuHandler)
  private
    procedure DoClipboardFunction(Reference: TDPoint); virtual;
  public
    class function HandlerType: Word; override;
    function OnMouseDown(const X, Y: Double; Button: TMouseButton; Shift: TShiftState): Boolean; override;
    procedure OnStart; override;
  end;

  TCutHandler = class(TCopyHandler)
  private
    procedure DoClipboardFunction(Reference: TDPoint); override;
  end;

  TPasteHandler = class(TCopyHandler)
  private
    procedure DoClipboardFunction(Reference: TDPoint); override;
  public
    procedure OnStart; override;
  end;
{$IFDEF RUSSIA}

     {++Sygsky: BMPClip}
  TViewBMPCopyHandler = class(TProjMenuHandler)
  private
  public
    procedure OnStart; override;
  end;
     {--Sygsky: BMPClip}
{$ENDIF}

var
  WinGISFormat: Word;

implementation

{$R *.mfn}
{$R *.r32}

uses AM_Coll,
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
  AM_DDE,
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
  AM_Group, AM_Index, AM_Obj, AM_OLE, AM_Proj, AM_Sym, AM_View, Forms,
  Objects, MenuFn, SysUtils, UserIntf, WinProcs, WinTypes, Win32Def,
  BMPImage, AM_Paint, NumTools, AM_ProjO
{++ Ivanoff BUG#NEW16 BUILD#100}
{$IFNDEF AXDLL} // <----------------- AXDLL
  , SymRol
{$ENDIF} // <----------------- AXDLL
{-- Ivanoff}
{++Sygsky: BMPClip}
  , AM_Main, ClipBrd, Graphics, Dialogs
{--Sygsky: BMPClip}
{++ Ivanoff. Object copying/moving.}
  , AM_Layer
{-- Ivanoff. Object copying/moving.}
  ;

var
  RefCursor: TCursor;

type
  LongType = record
    case Word of
      0: (Ptr: PChar);
      1: (Long: LongInt);
      2: (Lo, Hi: Word);
  end;

     {**************************************************************************
     | Object TClipboardStream
     |--------------------------------------------------------------------------
     | BlockSize = Anzahl Bytes, um die der Speicherblock bei �berlauf
     |     vergr��ert wird.
     | Size = aktuelle Gr��e des Stream in Bytes
     | Position = aktuelle Schreib-/Leseposition
     | MemHandle = Handle des globalen Speicherblocks
     | OwnerOfMemory = Speicherbereich geh�rt dem Stream (TRUE) oder
     |     der Zwischenablage (FALSE)
     | MemSize = Gr��e des globalen Speicherblocks
     +*************************************************************************}
  PClipboardStream = ^TClipboardStream;
  TClipboardStream = class(TOldStream)
  private
    BlockSize: Word;
    Size: LongInt;
    Position: LongType;
    MemHandle: THandle;
    OwnerOfMemory: Boolean;
    MemSize: LongInt;
  public
    constructor Create(ALimit: LongInt; ABlockSize: Word); reintroduce;
    constructor CreateByClipboard(ClpHandle: THandle);
    destructor Destroy; override;

    function GetClipboardHandle: THandle;
    function GetPos: Int64; override;
    function GetSize: Int64; override;
    function Seek(Offset: Int64): Int64; reintroduce;
    procedure Truncate;
    function Read(var Buffer; Count: Longint): Longint; override;
    function Write(const Buffer; Count: Longint): Longint; override;
  private
    function ChangeGlobalSize(ASize: LongInt): Boolean;
  end;

  PClpHeader = ^TClpHeader;
  TClpHeader = record
    ObjCnt: LongInt;
    SymCnt: LongInt;
    SymPos: LongInt;
    XPos: LongInt;
    YPos: LongInt;
    Version: Integer;
{++ Ivanoff BUG#NEW16 BUILD#100}
    pSourceProject: Pointer; // Pointer to a project-source of a clipboard's data
{-- Ivanoff}
  end;

  PSymXRef = ^TSymXRef;
  TSymXRef = object(TIndex)
    NewIndex: LongInt;
    constructor Init(AOldIndex, ANewIndex: LongInt);
  end;

const
  HeaderSize: Integer = 9; { Gr��e des Headers f�r den Stream      }
  Identifier = 'CLP'; { steht am Anfang vom Speicher          }

{===============================================================================
| TClipboardStream
+==============================================================================}

{******************************************************************************+
  Constructor TClipboardStream.Init
--------------------------------------------------------------------------------
  ALimit           i = Initialisierungsgr��e in Bytes
  ABlockSize       i = Anzahl bytes um die der Speicher erweitert wird
+******************************************************************************}

constructor TClipboardStream.Create(ALimit: LongInt; ABlockSize: Word);
begin
  inherited Create;
  BlockSize := ABlockSize;
  MemHandle := 0;
  OwnerOfMemory := TRUE;
  ChangeGlobalSize(ALimit);
  Size := 0;
  MemSize := 0;
  Position.Long := 0;
  SELF.Seek(0);
end;

{******************************************************************************+
  Constructor TClipboardStream.InitByClipboard
--------------------------------------------------------------------------------
  Initialisiert den Stream mit dem Speicherhandle der Zwischenablage. Am
  Anfang des Speicherblocks mu� der Identifier stehen, sonst wird ein Fehler
  angezeigt (stInitError).
+******************************************************************************}

constructor TClipboardStream.CreateByClipboard(ClpHandle: THandle);
var
  Source: LongType;
begin
  inherited Create;
  MemHandle := ClpHandle; { Handle und Speichergr��e    }
  MemSize := GlobalSize(MemHandle); { setzen                      }
  Size := MemSize;
  Position.Long := 0;
  Source.Ptr := GlobalLock(MemHandle);
  if (MemSize < HeaderSize) { Test auf Identifier         }
    or (StrLComp(Source.Ptr, Identifier, 3) <> 0) then
  begin
    MemHandle := 0;
    MemSize := 0;
    Error(stInitError, 0); { Fehler ausgeben             }
  end
  else
  begin
    Position.Long := 3;
    Read(BlockSize, SizeOf(BlockSize)); { Blockgr��e und Streamgr��e  }
    Read(Size, SizeOf(Size)); { setzen                      }
    OwnerOfMemory := FALSE; { Speicher geh�rt ZwAblage    }
  end;
  GlobalUnlock(MemHandle);
end;

destructor TClipboardStream.Destroy;
begin
  if (MemHandle <> 0)
    and OwnerOfMemory then
    GlobalFree(MemHandle);
  inherited Destroy;
end;

function TClipboardStream.ChangeGlobalSize(ASize: LongInt): Boolean;
var
  BSize: LongInt;
  NewHandle: THandle;
begin
  BSize := Round(0.5 + ASize / BlockSize) * BlockSize;
  if MemHandle = 0 then
  begin
    MemHandle := GlobalAlloc(GMEM_MOVEABLE or GMEM_DDESHARE, BSize);
    NewHandle := MemHandle;
    MemSize := BSize;
  end
  else
    if BSize <> MemSize then
    begin
      NewHandle := GlobalReAlloc(MemHandle, BSize, GMEM_MOVEABLE or GMEM_DDESHARE);
      if NewHandle <> 0 then
        MemHandle := NewHandle;
      MemSize := BSize;
    end;
  if NewHandle = 0 then
  begin
    Error(stWriteError, 0);
    ChangeGlobalSize := FALSE;
  end
  else
    ChangeGlobalSize := TRUE;
end;

function TClipboardStream.GetPos: Int64;
begin
  GetPos := Position.Long - HeaderSize;
end;

function TClipboardStream.GetSize: Int64;
begin
  GetSize := Size;
end;

function TClipboardStream.Read(var Buffer; Count: Longint): Longint;
var
  FromAddr: LongType;
  Source: LongType;
begin
  if Status = stOK then
  begin
    if Position.Long + Count > Size then
      Error(stReadError, 0)
    else
    begin
      Source.Ptr := GlobalLock(MemHandle);
      Source.Ptr := Source.Ptr + Position.Long;
      Move(Source.Ptr^, Buffer, Count);
      Position.Long := Position.Long + Count;
      GlobalUnlock(MemHandle);
    end;
  end;
end;

function TClipboardStream.Seek(Offset: Int64): Int64;
begin
  if Status = stOK then
  begin
    Inc(Offset, HeaderSize);
    if Offset > Size then
      Size := Offset;
    Position.Long := Offset;
  end;
end;

procedure TClipboardStream.Truncate;
begin
  if Status = stOK then
    Size := Position.Long;
end;

{******************************************************************************+
  Procedure TClipboardStream.Write
--------------------------------------------------------------------------------
  Schribt Daten in den Stream, sofern Status=stOK ist. Wurde der Stream mit
  InitByClipboard initialisiert, dann geh�rt der Speicherblock der Zwischen-
  ablage und wird nicht ge�ndert (stWriteError).
+******************************************************************************}

function TClipboardStream.Write(const Buffer; Count: Longint): Longint;
var
  ToAddr: LongType;
  Dest: LongType;
begin
  if Status = stOK then
  begin
    if not OwnerOfMemory then
      Error(stWriteError, 0)
    else
    begin
      if Position.Long + Count > MemSize then
        if not ChangeGlobalSize(Position.Long + Count) then
          Exit;
      Dest.Ptr := GlobalLock(MemHandle);
      Dest.Ptr := Dest.Ptr + Position.Long;
      Move(Buffer, Dest.Ptr^, Count);
      Position.Long := Position.Long + Count;
      if Position.Long > Size then
        Size := Position.Long;
      GlobalUnlock(MemHandle);
    end;
  end;
end;

{******************************************************************************+
 Function TClipboardStream.GetClipboardHandle
--------------------------------------------------------------------------------
  Schreibt den Header in den globalen Speicher und gibt den Handle des
  globalen Speichers f�r die Zwischenablage zur�ck.
--------------------------------------------------------------------------------
  Ergebnis: Handle des globalen Speichers, oder 0, wenn der Status des
            Stream <> stOK.
+******************************************************************************}

function TClipboardStream.GetClipboardHandle: THandle;
var
  AID: string[3];
begin
  if Status = stOK then
  begin
    Position.Long := 0; { Header an den Anfang des    }
    AId := Identifier; { Stream schreiben            }
    Write(AId[1], 3);
    Write(BlockSize, SizeOf(BlockSize));
    Write(Size, SizeOf(Size));
  end;
  if Status <> stOK then
    GetClipboardHandle := 0 { Fehler -> 0 zur�ckgeben     }
  else
  begin
    GetClipboardHandle := MemHandle; { Handle zur�ckgeben          }
    MemHandle := 0; { Stream neu initialisieren   }
    MemSize := 0;
    Position.Long := 0;
    Size := 0;
    Seek(0);
    OwnerOfMemory := TRUE;
  end;
end;

{==============================================================================+
  TSymXRef
+==============================================================================}

constructor TSymXRef.Init(AOldIndex: LongInt; ANewIndex: LongInt);
begin
  inherited Init(AOldIndex);
  NewIndex := ANewIndex;
end;

{==============================================================================+
  Methoden
+==============================================================================}

{******************************************************************************+
 Function CopyToClipBoard
--------------------------------------------------------------------------------
  Kopiert die selektierten Objekte ins Clipboard.
--------------------------------------------------------------------------------
  Liefert TRUE, wenn alles erfolgreich abgelaufen ist, FALSE, wenn Fehler
  aufgetreten sind (Clipboard war offen, zu wenig Speicher)
+******************************************************************************}

function CopyToClipBoard(Proj: PProj; Reference: TDPoint): Boolean;
var
  i: LongInt;
  Header: TClpHeader;
  Item: PIndex;
  SItem: PIndex; { Item with Object style in select layer}
//  ItemOS: Pointer; { Item ObjectStyle pointer }
  MetaFile: hDC;
  MF: ^TMetaFilePict;
  hMFP: THandle;
  ClpStream: TClipboardStream;
  CopiedSymbols: TIndexColl;
  ASymbol: PSGroup;
  FPInfo: TPaint;
  {++ Ivanoff. Object copying/moving.}
  iI: Integer;
  ALayer: PLayer;
  ALayerItem: PIndex;
  bF: Boolean;
  iSourceLayerIndex: Integer;
  {-- Ivanoff. Object copying/moving.}
begin
  CopyToClipBoard := FALSE;
  if not OpenClipBoard(Proj^.Parent.Handle) then
  begin
    MsgBox(Proj^.Parent.Handle, 10091, mb_IconExclamation or mb_OK, '');
    CloseClipboard;
    Exit;
  end;
  EmptyClipboard;
  MetaFile := CreateMetaFile(nil);
  hMFP := GlobalAlloc(GHND, sizeof(TMetaFilePict));
  SetWindowExt(MetaFile, 800, -600);
//!?!?!    SetWindowOrg(MetaFile,0,-600);
  ClpStream := TClipboardStream.Create(4096, 4096);
  ClpStream.Seek(SizeOf(Header));
  // create PInfo for printing and set options
  FPInfo.InitCopy(Proj^.PInfo);
  FPInfo.ExtCanvas.Cached := FALSE;
  FPInfo.ExtCanvas.Handle := MetaFile;

  FPInfo.ClipRect := InflatedRect(Rect(0, 0, 800, -600), 1000, -1000);
  FPInfo.ClipLimits := FPInfo.ClipRect;
  FPInfo.ClipLimits.Top := FPInfo.ClipRect.Bottom;
  FPInfo.ClipLimits.Bottom := FPInfo.ClipRect.Top;
  FPInfo.ExtCanvas.ClipRect := FPInfo.ClipRect;

  FPInfo.SetLineStyle(Proj^.Layers^.TopLayer^.LineStyle);
  FPInfo.SetFillStyle(Proj^.Layers^.TopLayer^.FillStyle);
  CopiedSymbols.Init(20, 10);
  for i := 0 to Proj^.Layers^.SelLayer^.Data^.GetCount - 1 do
  begin
    SItem := Proj^.Layers^.SelLayer^.Data^.At(i);
    Item := Proj^.Layers^.IndexObject(@FPInfo, SItem); { Item with data }
    if Item^.GetObjType = ot_Symbol then
    begin
      ASymbol := PSymbol(Item)^.GetSymbol(@FPInfo);
      if CopiedSymbols.IndexOf(ASymbol) = -1 then
        CopiedSymbols.Insert(ASymbol);
    end;
//{$ifdef TEST}
//    ItemOS := Item.ObjectStyle; { It should be nil, but Forst knows }

//    if ItemOS = nil then begin
    { Try to find this object with ObjectStyle set !}
    if SItem.ObjectStyle <> nil then
    begin
      Item.ObjectStyle := SItem.ObjectStyle; { Try to insert true style now}
      Item.SetState(sf_OtherStyle, True);
    end
    else
    begin
      Item.ObjectStyle := nil; { Try to insert true style now}
      Item.SetState(sf_OtherStyle, False);
    end;
//    end;
//{$endif}

    {++ Ivanoff. Object copying/moving.}
    { Copying of objects from one layer to another one.
      We need also to copy attribute information from the source layer table to destination layer table.
      To make this operation I need to know which layer is a source of the copied object. I put index of the
      source layer to the stream before the object itself. }
    bF := FALSE;
    for iI := 1 to Proj.Layers.LData.Count - 1 do
    begin
      ALayer := Proj.Layers.LData.At(iI);
      ALayerItem := ALayer.HasIndexObject(Item.Index);
      if ALayerItem <> nil then
      begin
        bF := TRUE;
        BREAK;
      end;
    end; // For iI end
    if bF then
      iSourceLayerIndex := ALayer.Index
    else
      iSourceLayerIndex := -1;
    ClpStream.Write(iSourceLayerIndex, SizeOf(iSourceLayerIndex));
    {-- Ivanoff. Object copying/moving.}

    ClpStream.Put(Item);
//{$ifdef TEST}
//    if ItemOS = nil then
//    begin
    Item.SetState(sf_OtherStyle, False);
    Item.ObjectStyle := nil;
//    end;
//{$endif}
//    PView(Item)^.Draw(@FPInfo, TRUE);
  end;
  Header.SymPos := ClpStream.GetPos;
  for i := 0 to CopiedSymbols.Count - 1 do
    ClpStream.Put(CopiedSymbols.At(i));
  if hMFP <> 0 then
  begin
    MF := GlobalLock(hMFP);
    MF^.mm := MM_ISOTROPIC;
    MF^.xExt := 800;
    MF^.yExt := 600;
    MF^.hMF := CloseMetaFile(MetaFile);
    GlobalUnlock(hMFP);
    if ClpStream.Status = stOK then
      SetClipboardData(cf_MetaFilePict, hMFP)
    else
      SetClipboardData(cf_MetaFilePict, 0);
  end;
  if ClpStream.Status <> stOK then
    MsgBox(Proj^.Parent.Handle, 3615, mb_IconExclamation or mb_OK, '')
  else
  begin
    Header.ObjCnt := Proj^.Layers^.SelLayer^.Data^.GetCount;
    Header.XPos := Reference.X;
    Header.YPos := Reference.Y;
    Header.SymCnt := CopiedSymbols.Count;
    Header.Version := vnProject;
{++ Ivanoff BUG#NEW16 BUILD#100}
{Set pointer to a project that is a source of clipboard's data.}
    Header.pSourceProject := Proj;
// {-- Ivanoff}
    ClpStream.Seek(0);
    ClpStream.Write(Header, SizeOf(Header));
    SetClipboardData(WinGISFormat, ClpStream.GetClipboardHandle);
  end;
  ClpStream.Free;
  CopiedSymbols.DeleteAll;
  CopiedSymbols.Done;
  CloseClipboard;
  CopyToClipBoard := TRUE;
  FPInfo.DoneCopy;
end;

{******************************************************************************+
  Procedure PasteFromClipBoard
--------------------------------------------------------------------------------
  Daten vom Clipboard holen
+******************************************************************************}

procedure PasteFromClipBoard(Proj: PProj; Reference: TDPoint);
type
  recSymbolNames = record
    sOldName, sNewName: AnsiString;
  end;
var
  hData: THandle;
  Header: TClpHeader;
  Item: PView;
  i: LongInt;
  XOffset: LongInt;
  YOffset: LongInt;
  ClpStream: TClipboardStream;
  ASymbol: PIndex;
  OldIndex: LongInt;
  Symbols: TIndexColl;
  SymXRef: PSymXRef;
  AIndex: Integer;
{++ Ivanoff BUG#NEW16 BUILD#100}
{$IFNDEF AXDLL} // <----------------- AXDLL
  Rollup: TSymbolRollupForm; // For update panel of symbol library
{$ENDIF} // <----------------- AXDLL
  iL: Integer; // Array and its length for store symbol item name
  arSymbolsNames: array of AnsiString; //
  pSourceProject: Pointer; // Pointer to a project-source of clipboard's data
{-- Ivanoff}
{++ Ivanoff. Object copying/moving.}
  iSourceLayerIndex, iSourceItemIndex: Integer;
  ASourceLayer: PLayer;
{-- Ivanoff. Object copying/moving.}
  Inserted: Boolean;
  IDStr: string;
  IDPChar: array[0..15] of Char;
begin
  if not OpenClipBoard(Proj^.Parent.Handle) then
  begin
    MsgBox(Proj^.Parent.Handle, 10091, mb_IconExclamation or mb_OK, '');
    CloseClipboard;
    Exit;
  end;
  if (WinGISFormat = 0)
    or not IsClipboardFormatAvailable(WinGISFormat) then
  begin
    MsgBox(Proj^.Parent.Handle, 10092, mb_IconExclamation or mb_OK, '');
    CloseClipboard;
    Exit;
  end;
{++ Ivanoff BUG#NEW16 BUILD#100}
  SetLength(arSymbolsNames, 0);
{-- Ivanoff}
  hData := GetClipboardData(WinGISFormat);
  ClpStream := TClipboardStream.CreateByClipboard(hData);
  ClpStream.Read(Header, SizeOf(Header));
  pSourceProject := Header.pSourceProject;
  ProjectVersion := Header.Version;
  ProjectName := StrPas(Proj^.FName);
  ClpStream.Seek(Header.SymPos);
  Symbols.Init(20, 10);
  if ClpStream.Status = stOK then
  begin
    for i := 1 to Header.SymCnt do
    begin
      ASymbol := Pointer(ClpStream.Get);
      if ASymbol <> nil then
      begin
        PSGroup(ASymbol)^.UseCount := 0;
        OldIndex := ASymbol^.Index;
{++ Ivanoff BUG#NEW16 BUILD#100}
{If I paste a symbol in project, that is not a source of this symbol, then I should get new name for this symbol.
After this I'll store symbol name (old or new) for later using.}

        {
        if (Proj <> pSourceProject)
          and
          not PSymbols(Proj.PInfo.Symbols).CheckName(PSGroup(ASymbol).Name^, Proj.ActSymLib, PSGroup(ASymbol)) then
          PSGroup(ASymbol).Name^ := PSymbols(Proj.PInfo.Symbols).GetNewSymName(PSGroup(ASymbol).GetName, Proj.ActSymLib);
        }

        iL := Length(arSymbolsNames);
        SetLength(arSymbolsNames, iL + 1);
        arSymbolsNames[iL] := PSGroup(ASymbol).GetName;
{-- Ivanoff}
        if PSymbols(Proj^.PInfo^.Symbols)^.GetProjectSymbol(PSGroup(ASymbol)^.GetName, '') = nil then
        begin
          PSymbols(Proj^.PInfo^.Symbols)^.CopySymIntoProject(Proj^.PInfo, PSGroup(ASymbol));
        end;
        Symbols.Insert(New(PSymXRef, Init(OldIndex, ASymbol^.Index)));
      end;
    end;
{++ Ivanoff BUG#NEW16 BUILD#100}
{After I have added new symbol(s) in library I must repaint symbol library panel.}
{$IFNDEF AXDLL} // <----------------- AXDLL
    if Symbols.Count > 0 then
    begin
      RollUp := UserInterface.RollupNamed['SymbolRollupForm'] as TSymbolRollupForm;
      if RollUp <> nil then
        RollUp.UpdateSymbols;
    end;
{$ENDIF} // <----------------- AXDLL
{-- Ivanoff}
    ClpStream.Seek(SizeOf(Header));
    XOffset := Reference.X - Header.XPos;
    YOffset := Reference.Y - Header.YPos;
    ASymbol := New(PIndex, Init(0));
    Proj^.RedrawAfterInsert := FALSE;
    try
      for i := 0 to Header.ObjCnt - 1 do
      begin

        {++ Ivanoff. Object copying/moving.}
        { Copying of objects from one layer to another one.
          We need also to copy attribute information from the source layer table to destination layer table.
          To make this operation I need to know which layer is a source of the copied object. I have put
          index of the source layer to the stream before the object itself. And now I have to get it from
          the stream. }
        ClpStream.Read(iSourceLayerIndex, SizeOf(iSourceLayerIndex));
        ASourceLayer := Proj.Layers.IndexToLayer(iSourceLayerIndex);
        {-- Ivanoff. Object copying/moving.}

        Item := Pointer(ClpStream.Get);
        OldIndex := Item^.Index;

        {++ Ivanoff. Object copying/moving.}
        iSourceItemIndex := Item.Index;
        {-- Ivanoff. Object copying/moving.}

        Item^.Index := 0;
        if Item <> nil then
        begin
          Item^.MoveRel(XOffset, YOffset);
          if Item^.GetObjType = ot_Symbol then
          begin
            ASymbol^.Index := PSymbol(Item)^.SymIndex;
            if Symbols.Search(ASymbol, AIndex) then
            begin
              SymXRef := Symbols.At(AIndex);
{!?!?!?!                PSymbol(Item)^.SymIndex:=SymXRef^.NewIndex;}
{++ Ivanoff BUG#NEW16 BUILD#100}
{I have TSymbol and TSGroup in symbol library. Now I must set link between them.
I'll find TSGroup by its name.}
              PSymbol(Item).SymPtr := PSymbols(Proj.PInfo.Symbols).GetSymbol(arSymbolsNames[AIndex], Proj.ActSymLib);
              PSymbol(Item).SymIndex := PSGroup(PSymbol(Item).SymPtr).Index;
{-- Ivanoff}
              if Item.GetState(sf_OtherStyle) then
                Inserted := Proj.InsertObjectWithStyle(Item, Proj.Layers.TopLayer, Item.ObjectStyle^)
              else
                Inserted := Proj^.InsertObject(Item, FALSE);
            end
            else
              Dispose(Item, Done);
          end
          else
          begin
            if (Item <> nil) then
            begin
              if Item.GetState(sf_OtherStyle) then
                Inserted := Proj.InsertObjectWithStyle(Item, Proj.Layers.TopLayer, Item.ObjectStyle^)
              else
                Inserted := Proj^.InsertObject(Item, FALSE);
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
              {++ Ivanoff. Object copying/moving.}
              if WinGISMainForm.WeAreUsingTheIDB[Proj] then
                 { If it is the same layer or the same item, the data copying will not be made. }
                WinGISMainForm.IDB_Man.CopyAttributeInfo(Proj, ASourceLayer, Proj.Layers.TopLayer, iSourceItemIndex, Item.Index);
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
            end;
              {-- Ivanoff. Object copying/moving.}
          end;
        end;
        if Inserted then
        begin
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
          if DDEHandler.FLayerInfo <> 0 then
          begin
            Str(Proj^.Layers^.TopLayer^.Index: 10, IDStr);
            StrPCopy(@IDPChar, IDStr);
            Proj^.InsertedObjects^.Insert(StrNew(@IDPChar));
          end;
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
          Str(OldIndex: 10, IDStr);
          StrPCopy(@IDPChar, IDStr);
          Proj^.InsertedObjects^.Insert(StrNew(@IDPChar));
          Str(Item^.Index: 10, IDStr);
          StrPCopy(@IDPChar, IDStr);
          Proj^.InsertedObjects^.Insert(StrNew(@IDPChar));
        end;
      end;
    except
    end;
    {++ Ivanoff. Object copying/moving.}
    { In order to see all made changes, all database window should be refreshed. }
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
    if WinGISMainForm.WeAreUsingTheIDB[Proj] then
      WinGISMainForm.IDB_Man.RefreshDataWindows(Proj);
{$ENDIF} // <----------------- AXDLL
    {-- Ivanoff. Object copying/moving.}
{$ENDIF}
    Proj^.RedrawAfterInsert := TRUE;
    if Proj^.BorderCorrected then
    begin
      Proj^.UpdatePaintOffset;
      Proj^.BorderCorrected := FALSE;
    end;
    Dispose(ASymbol, Done);
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
    ProcSendNewIDsToDB(Proj, 'CPY'); /////
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
  end;
  ClpStream.Free;
  CloseClipboard;
  Symbols.Done;
{$IFNDEF AXDLL} // <----------------- AXDLL
  if ClpStream.Status <> stOK then
    MsgBox(Proj^.Parent.Handle, 3618, mb_IconExclamation or mb_OK, '');
{$ENDIF} // <----------------- AXDLL
  Proj^.SetModified;

end;

{==============================================================================+
  TCutHandler
+==============================================================================}

procedure TCutHandler.DoClipboardFunction(Reference: TDPoint);
begin
  Cursor := crHourGlass;
  StatusText := MlgStringList['ClpHndl', 5];
  if CopyToClipboard(Project, Reference) then
  begin
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
    Project^.Layers^.DeleteSelected(Project, Project^.PInfo, DDEHandler, FALSE, Project^.BitmapCount, Project^.Multimedia, FALSE);
    Project^.SetModified;
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
  end;
  Cursor := crDefault;
  StatusText := '';
  Status := mhsReturnToDefault;
end;

{==============================================================================+
  TCopyHandler
+==============================================================================}

class function TCopyHandler.HandlerType: Word;
begin
  Result := mhtReturnToPushed;
end;

procedure TCopyHandler.DoClipboardFunction(Reference: TDPoint);
begin
  Cursor := crHourGlass;
  StatusText := MlgStringList['ClpHndl', 5];
  CopyToClipboard(Project, Reference);
  StatusText := '';
  Cursor := crDefault;
  Status := mhsReturnToDefault;
end;

procedure TCopyHandler.OnStart;
begin
  StatusText := MlgStringList['ClpHndl', 4];
  Status := mhsNotFinished;
  Cursor := RefCursor;
  CoordinateInput := TRUE;
end;

function TCopyHandler.OnMouseDown
  (
  const X: Double;
  const Y: Double;
  Button: TMouseButton;
  Shift: TShiftState
  )
  : Boolean;
var
  DrawPos: TDPoint;
  Point: TDPoint;
  SnapObject: PIndex;
begin
  DrawPos.Init(LimitToLong(X), LimitToLong(Y));
  if (Button = mbLeft) and (Project^.SnapPoint(DrawPos, 0, ot_Any, Point, SnapObject)) then
    DoClipboardFunction(Point);
  Result := TRUE;
end;

{==============================================================================+
  TPasteHandler
+==============================================================================}

procedure TPasteHandler.OnStart;
begin
  if CanPaste then
    Project^.SetActualMenu(mn_PasteOLEObj)
  else
    inherited OnStart;
  CoordinateInput := TRUE;
end;

procedure TPasteHandler.DoClipboardFunction(Reference: TDPoint);
begin
  Cursor := crHourGlass;
  StatusText := MlgStringList['ClpHndl', 5];
  PasteFromClipboard(Project, Reference);
  Cursor := crDefault;
  StatusText := '';
  Status := mhsReturnToDefault;
end;

{$IFDEF RUSSIA}

{ TViewBMPCopyHandler }{++Sygsky:BMPClip}

procedure TViewBMPCopyHandler.OnStart;
var
  MyFormat: Word;
  ABitmap: TBitmap;
  Str: AnsiString;
  ErrNum, I: Integer;
begin
  Cursor := crHourGlass;
  StatusText := MlgStringList['ClpHndl', 11];
  with Project.PInfo.ExtCanvas do
  begin
    if CachedDraw then
    begin
      ABitmap := TBItmap.Create; { Just in case }
      ABitmap.Handle := CacheBitmap;
    end
    else
      ABitMap := TForm(Parent).GetFormImage;
  end;
  try
    ErrNum := 0;
    if OpenClipBoard(0) then
    begin
      if EmptyClipBoard then
      begin
        I := SetClipBoardData(CF_BITMAP, ABitMap.Handle);
        if I = 0 then
          ErrNum := GetLastError;
      end
      else
        ErrNum := GetLastError;
    end
    else
    begin
      ErrNum := GetLastError;
    end;
    CloseClipBoard; { In any case }

  finally
    ABitMap.Free;
    StatusText := '';
    Cursor := crDefault;
    if ErrNum <> 0 then
    begin
      Str := Format(MlgStringList['ClpHndl', 12], [GetLastError]);
      MessageDlg(Str, mtWarning, [mbOK], 0);
    end;
  end;
end;
{$ENDIF}

{==============================================================================+
  Initialisierungs- und Terminierungscode
+==============================================================================}

initialization
  begin
    MenuFunctions.RegisterFromResource(HInstance, 'ClpHndl', 'ClpHndl');
    TCutHandler.Registrate('EditCut');
    TCopyHandler.Registrate('EditCopy');
    TPasteHandler.Registrate('EditPaste');
{$IFDEF RUSSIA}
    TViewBMPCopyHandler.Registrate('CopyAsBMP');
{$ENDIF}
    { WinGIS-Clipboard-Format registration }

    WinGISFormat := RegisterClipboardFormat('WinGIS4');
    RefCursor := LoadCursor(hInstance, 'REFERENCE');
    Screen.Cursors[RefCursor] := RefCursor;
  end;

end.

