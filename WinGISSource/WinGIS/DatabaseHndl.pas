{******************************************************************************+
  Unit DatabaseHndl
--------------------------------------------------------------------------------
  Autor: Martin Forst
--------------------------------------------------------------------------------
  Contains the menu-handler and the toolbar-handler for the database-interface.
--------------------------------------------------------------------------------
  Release: 1
--------------------------------------------------------------------------------
+******************************************************************************}
Unit DatabaseHndl;

Interface
                                       
Uses Classes,MenuHndl,ProjHndl;

Type TDatabaseActivateHandler = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;

     TDatabaseAnnotateHandler = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;

     TDatabaseAutoEditInsertHandler = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;

     TDatabaseAutoMonitoringHandler = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;

     TDatabaseChangeTableHandler = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;

     TDatabaseCombineHandler = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;

     TDatabaseCombineDKMHandler = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;

     TDatabaseDiagramsHandler = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;

     TDatabaseEditInsertHandler = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;

     TDatabaseMultipleInsertHandler = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;

     TDatabaseMonitoringHandler = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;

     TDatabaseReloadHandler = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;

     TDatabaseConnectHandler = Class(TMenuHandler)
       Procedure   OnStart; override;
     end;

Implementation

{$R *.mfn}

Uses AM_Child,DDEDef,AM_Ini,AM_Main,AM_ProjO,AM_ProjP,AM_Punkt,Controls,Windows,Forms,AM_Def,
     MenuFn
     {$IFNDEF AXDLL} // <----------------- AXDLL
     ,UComOld
       {$IFNDEF WMLT}
     ,AM_DDE,DatabaseOptionsDlg
{++ IDB}
     , IDB_MenuHndl
{-- IDB}
       {$ENDIF}
     {$ENDIF} // <----------------- AXDLL
     ;

{==============================================================================+
  TDatabaseActivateHandler
+==============================================================================}

Procedure TDatabaseActivateHandler.OnStart;
var
i: Integer;
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  {$IFNDEF WMLT}
     DDEHandler.SendString('[SHW][]');
{++ IDB}
     If WinGisMainForm.WeAreUsingTheIDBUserInterface[Project] Then begin
        WinGisMainForm.IDB_Man.ShowDataTable;
        for i:=0 to WingisMainForm.MDIChildCount-1 do begin
            if WingisMainForm.MDIChildren[i].ClassName = 'TIDB_ViewForm' then begin
               TForm(WingisMainForm.MDIChildren[i]).BringToFront;
               break;
            end;
            if WingisMainForm.MDIChildren[i].ClassName = 'TIDB_MonitorForm' then begin
               TForm(WingisMainForm.MDIChildren[i]).BringToFront;
               break;
            end;
        end;
     end;
{-- IDB}
   {$ENDIF}
{$ENDIF} // <----------------- AXDLL
end;

{==============================================================================+
  TDatabaseAnnotateHandler
+==============================================================================}

Procedure TDatabaseAnnotateHandler.OnStart;
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  {$IFNDEF WMLT}
     ProcDBSendCommand(Project,'GDT');
    //{$IFDEF RUSSIA}
{++ IDB}
     If WinGisMainForm.WeAreUsingTheIDBUserInterface[Project] Then
        WinGisMainForm.IDB_Man.MakeAnnotations(WinGisMainForm.ActualProj);
{-- IDB}
    //{$ENDIF}
  {$ENDIF}
{$ENDIF} // <----------------- AXDLL
end;

{==============================================================================+
  TDatabaseAutoEditInsertHandler
+==============================================================================}

Procedure TDatabaseAutoEditInsertHandler.OnStart;
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  {$IFNDEF WMLT}
  if MenuFunctions['DatabaseAutoEditInsert'].Checked then begin
    DDEHandler.DoAutoIns:=TRUE;
  end
  else begin
    DDEHandler.DoAutoIns:=FALSE;
  end;
  {$ENDIF}
{$ENDIF} // <----------------- AXDLL
end;

{==============================================================================+
  TDatabaseAutoMonitoringHandler
+==============================================================================}

Procedure TDatabaseAutoMonitoringHandler.OnStart;
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  {$IFNDEF WMLT}
  if MenuFunctions['DatabaseAutoMonitoring'].Checked then begin
    IniFile^.DoOneSelect:=TRUE;
    DDEHandler.DoOneSelect:=TRUE;
    IniFile^.FrameSelect:=FALSE;
    DDEHandler.FrameSelect:=FALSE;
    Project^.DeselectAll(FALSE);
  end
  else begin
    IniFile^.DoOneSelect:=FALSE;
    DDEHandler.DoOneSelect:=FALSE;
    IniFile^.FrameSelect:=TRUE;
    DDEHandler.FrameSelect:=TRUE;
    Project^.DeselectAll(FALSE);
  end;
  {$ENDIF}
{$ENDIF} // <----------------- AXDLL
end;

{==============================================================================+
  TDatabaseChangeTableHandler
+==============================================================================}

Procedure TDatabaseChangeTableHandler.OnStart;
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  {$IFNDEF WMLT}
     DDEHandler.SendString('[DBW][]');
    //{$IFDEF RUSSIA}
{++ IDB}
     If WinGisMainForm.WeAreUsingTheIDBUserInterface[Project] Then
        WinGisMainForm.IDB_Man.ShowDataTableWithSelectBefore(WinGisMainForm.ActualProj);
{-- IDB}
    //{$ENDIF}
  {$ENDIF}
{$ENDIF} // <----------------- AXDLL
end;

{==============================================================================+
  TDatabaseCombineHandler
+==============================================================================}

Procedure TDatabaseCombineHandler.OnStart;
{$IFNDEF AXDLL} // <----------------- AXDLL
var OldCombine   : TOldCombine;
{$ENDIF} // <----------------- AXDLL
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  OldCombine:=TOldCombine.Create(Project,Project^.Parent);
  OldCombine.DoAll;
  OldCombine.Free;
{$ENDIF} // <----------------- AXDLL
end;

{==============================================================================+
  TDatabaseCombineDKMHandler
+==============================================================================}

Procedure TDatabaseCombineDKMHandler.OnStart;
begin
  CombinePointArea(Project,Project^.PInfo);
end;

{==============================================================================+
  TDatabaseDiagramsHandler
+==============================================================================}

Procedure TDatabaseDiagramsHandler.OnStart;
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  {$IFNDEF WMLT}
  ProcDBSendCommand(Project,'GDI');
  {$ENDIF}
{$ENDIF} // <----------------- AXDLL
end;

{==============================================================================+
  TDatabaseEditInsertHandler
+==============================================================================}

Procedure TDatabaseEditInsertHandler.OnStart;
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  {$IFNDEF WMLT}
  Project^.DBEdit;
  {$ENDIF}
{$ENDIF} // <----------------- AXDLL
end;

{==============================================================================+
  TDatabaseMultipleInsertHandler
+==============================================================================}

Procedure TDatabaseMultipleInsertHandler.OnStart;
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  {$IFNDEF WMLT}
  ProcDBMultiInsert(Project);
  {$ENDIF}
{$ENDIF} // <----------------- AXDLL
end;

{==============================================================================+
  TDatabaseMonitoringHandler
+==============================================================================}

Procedure TDatabaseMonitoringHandler.OnStart;
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  {$IFNDEF WMLT}
  {if IniFile^.DoOneSelect and DDEHandler^.FOneSelect then begin}
    if WinGISMainForm.UseDkmApp then begin
       ProcDKMAPPMonitoring(Project);
       exit;
    end;
    if DDEHandler.FOneSelect then DDEHandler.SetDBSendStrings(DDECommMon,NIL);
    {$IFNDEF WMLT}
    ProcDBMonitoring(Project);
    {$ENDIF}
    if DDEHandler.FOneSelect then DDEHandler.SetDBSendStrings(DDECommAll,NIL);
  {end
  else ProcDBMonitoring(Project);}
{++ IDB}
     If WinGisMainForm.WeAreUsingTheIDBUserInterface[Project] Then
        WinGisMainForm.IDB_Man.ShowMonitoringTable (true);
{-- IDB}
  {$ENDIF}
{$ENDIF} // <----------------- AXDLL
end;

{==============================================================================+
  TDatabaseReloadHandler
+==============================================================================}

Procedure TDatabaseReloadHandler.OnStart;
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  {$IFNDEF WMLT}
  SetDBApplication;
  {$ENDIF}
{$ENDIF} // <----------------- AXDLL
end;

{==============================================================================+
  TDatabaseConnectHandler
+==============================================================================}

Procedure TDatabaseConnectHandler.OnStart;
{$IFNDEF AXDLL} // <----------------- AXDLL
  {$IFNDEF WMLT}
var DatabaseDialog : TDatabaseOptionsDialog;
  {$ENDIF}
{$ENDIF} // <----------------- AXDLL
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  {$IFNDEF WMLT}
  DatabaseDialog:=TDatabaseOptionsDialog.Create(WinGISMainForm);
  DatabaseDialog.DDEApplications:=DDEHandler.DDEApps;
  DatabaseDialog.DDEMLApplications:=DDEHandler.DDEMLApps;
// ++ Cadmensky
  DatabaseDialog.Project := WinGISMainForm.ActualProj;
// -- Cadmensky
  DatabaseDialog.FormShow(nil);
  DatabaseDialog.IDB_Enabled_CB.Checked:=not DatabaseDialog.IDB_Enabled_CB.Checked;
  DatabaseDialog.IDB_Enabled_CBClick(nil);
  DatabaseDialog.ModalResult:=mrOK;
  DatabaseDialog.FormHide(nil);
  //if DatabaseDialog.ShowModal=mrOK then begin
  //IniFile^.WriteDDEDDEMLSettings(DDEHandler.DDEApps,DDEHandler.DDEMLApps);
  //DDEHandler.UpdateAppLists;
  //DDEHandler.UnActivateConnections;
  SetDBApplication;
  //WinGISMainForm.OnGlobalOptionsChanged;
  //end;
  DatabaseDialog.Free;
  DatabaseDialog:=nil;
  WingisMainForm.ActualChild.Data^.SetModified;
  {$ENDIF}
{$ENDIF} // <----------------- AXDLL
end;

{==============================================================================+
  Initialization- and termination-code
+==============================================================================}

Initialization;
  MenuFunctions.RegisterFromResource(HInstance,'DatabaseHndl','DatabaseHndl');
  TDatabaseActivateHandler.Registrate('DatabaseActivate');
  TDatabaseAnnotateHandler.Registrate('DatabaseAnnotate');
  TDatabaseAutoEditInsertHandler.Registrate('DatabaseAutoEditInsert');
  TDatabaseAutoMonitoringHandler.Registrate('DatabaseAutoMonitoring');
  TDatabaseChangeTableHandler.Registrate('DatabaseChangeTable');
  TDatabaseCombineHandler.Registrate('DatabaseCombine');
  TDatabaseCombineDKMHandler.Registrate('DatabaseCombineDKM');
  TDatabaseDiagramsHandler.Registrate('DatabaseDiagrams');
  TDatabaseEditInsertHandler.Registrate('DatabaseEditInsert');
  TDatabaseMultipleInsertHandler.Registrate('DatabaseMultipleInsert');
  TDatabaseMonitoringHandler.Registrate('DatabaseMonitoring');
  TDatabaseReloadHandler.Registrate('DatabaseReload');
  TDatabaseConnectHandler.Registrate('DatabaseConnect');
{$IFNDEF AXDLL} // <----------------- AXDLL
  {$IFNDEF WMLT}                             
{++ IDB}
  TIDB_MonitoringHandler.Registrate('IDB_Monitoring');
  TIDB_ShowLayerTableHandler.Registrate('IDB_ShowLayerTable');
  TIDB_InfoHandler.Registrate('IDB_Info');       
{-- IDB}
  {$ENDIF}
{$ENDIF} // <----------------- AXDLL
end.
