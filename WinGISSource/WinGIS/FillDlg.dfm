�
 TPATTERNDIALOG 0�	  TPF0TPatternDialogPatternDialogLeft*Top� HelpContext�BorderStylebsDialogCaption!1ClientHeight� ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrder	PositionpoScreenCenterOnCloseQueryFormCloseQueryOnCreate
FormCreate	OnDestroyFormDestroyOnHideFormHideOnShowFormShowPixelsPerInch`
TextHeight TBevelBevel1LeftTop� WidthyHeightShapebsBottomLine  TButtonOkBtnLeft� Top� WidthYHeightCaption!2Default	ModalResultTabOrder   TButton	CancelBtnLeft(Top� WidthYHeightCancel	Caption!3ModalResultTabOrder  TPanelControlsPanelLeft Top Width�Height� 
BevelOuterbvNoneTabOrder TWLabel
ScaleLabelLeft� ToplWidthAHeightAutoSizeCaption!10FocusControl	ScaleEdit  TWLabelBackColorLabelLeft� Top8Width� HeightAutoSizeCaption!5FocusControlBackColorBtn  TWLabelForeColorLabelLeft� TopWidth� HeightAutoSizeCaption!4FocusControlForeColorBtn  TSpinBtn	ScaleSpinLeftrTophHeight	ArrowkeysCtrlIncrement       �@	Increment       �@Max �P]���CShiftIncrement       �@	ValidatorScaleVal  TEdit	ScaleEditLeft(TophWidthIHeightTabOrder  TColorSelectBtnBackColorBtnLeft� TopHWidth� TabOrder  TColorSelectBtnForeColorBtnLeft� TopWidth� TabOrder  TDipListPatternListLeftTopWidth� Height� ColCountColWidth� OnClickPatternListClick
OnDblClickPatternListDblClick
OnDrawCellPatternListDrawCellOnMouseMovePatternListMouseMoveParentShowHint	PopupMenu	PopupMenu	RowHeight
Scrollbars
sbVerticalShowHint	TabOrder TabStop	  	TCheckBoxScaleIndependCheckLeftTop� WidthyHeightCaption!11TabOrder   TMlgSection
MlgSectionSectionPatternDialogLeftTop�   
TPopupMenu	PopupMenuLeft(Top�  	TMenuItemAddVectorXFillMenuCaption!9OnClickAddVectorXFillMenuClick  	TMenuItemAddBitmapXFillMenuCaption!6OnClickAddBitmapXFillMenuClick  	TMenuItemPropertiesMenuCaption!8OnClickPropertiesMenuClick  	TMenuItemN2Caption-  	TMenuItem
DeleteMenuCaption!7OnClickDeleteMenuClick  	TMenuItemN1Caption-  	TMenuItemOrganizeMenuCaption!12OnClickOrganizeMenuClick   TMeasureLookupValidatorScaleValEdit	ScaleEditUseableUnits @MinValue.Units	muPercentMaxValue.Units	muPercentMaxValue.Value �P]���CUnits	muPercentLeftHTop�    