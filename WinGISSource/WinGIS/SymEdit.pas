{ Unit SymEditor                                                               }
{ Unit f�r zus�tzliche Funktionen des Symboleditors                            }
{      - Einheitslinie                                                         }
{      - Referenzpunkt                                                         }
{      - Symbol laden/speichern                                                }
{ Autor : Raimund Leitner                                                      }
{ Datum : 3.4.1997                                                             }
{------------------------------------------------------------------------------}
unit SymEdit;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Dialogs, UserIntf,
  AM_Sym, AM_Index, AM_View, AM_Layer, AM_Proj, AM_Def, AM_Sight,
  AM_Point, AM_Poly, AM_CPoly, AM_Circl, AM_Splin, AM_Text, StdCtrls,
  AM_Obj, ExtLib, Validate, Buttons,MenuFn,MultiLng;

const mrSymLoad = 101;   { Wert f�r ModalResult bei Fkt "Symbol laden" }

type
  TSymEditor = class
  private
    { Private-Deklarationen }
    FSymLib     : PExtLib;       { ext Bib des Symbols (f�r Projekt=nil) }
    FEditLayer  : String;
    FRefLayerName   : String;
  public
    { Public-Deklarationen }
    Project     : PProj;
    {++Brovak Bug 526}
    BossProject : PProj;
    {--Brovak}
    SymRect     : TDRect;
    UnitPoly    : PPoly;
    RefStyleData: TObjStyleV2;
    FRefLayer   : pLayer;
    SymRefPoint : PPixel;
    Constructor Create(Proj : PProj;ASymLib : PExtLib);
    Procedure   SetupSymbolEditor;
    Procedure   SaveSymbol;
    Procedure   SetUnitLine(AItem : PIndex);
    Procedure   SetupRefStyle;
    Procedure   SetRefPoint(APoint: PIndex);
    Procedure   DrawRefPoint;
    Procedure   DrawUnitLine;
    Procedure   OpenSymProperties;
    Procedure   SendToBack;
    Procedure   BringToFront;
    Function    CanEndEditor:Boolean;
  end;

implementation

uses AM_Child,AM_Admin,AM_Group,SymbolPropertyDlg,AM_Main,GrTools,AM_ZLayer;

{******************************************************************************}
{ Construktor Create                                                           }
{ Initialisiert den Symboleditor und sichert die aktuelle Ansicht des Projekts }
{******************************************************************************}
constructor TSymEditor.Create
   (
   Proj         : PProj;
   ASymLib      : PExtLib
   );
begin
  inherited Create;
  Project:=Proj;
  SetupRefStyle;
  SymRefPoint:=nil;
  UnitPoly:=nil;
  FSymLib:=ASymLib;
  FEditLayer:=MlgStringList['SymbolEditor',3];
  if (FEditLayer='') then FEditLayer:='SymbolEditorLayer';{Safety is our success}
  FRefLayerName:='ReferenceLayer';
  if (Project<>nil) then
    with Project^ do begin
      SetupSymbolEditor;        { Symboleditor initialisieren (Ansicht, etc.) }
    end;
  end;

{******************************************************************************}
{ Methode SaveSymbol                                                      Ray  }
{ Speichert das aktuelle Symbol mit automat. Skalierung durch Einheitslinie    }
{ und Referenzpunkt                                                            }
{******************************************************************************}
procedure TSymEditor.SaveSymbol;
var ProjSym        : PSGroup;
    i              : Integer;
    SymLayer       : pZLayer;
Procedure DoAll(AItem:PIndex);far;
var SymObject : PView;
  begin
  if (AItem^.Index<>UnitPoly^.Index) and     { unit line and reference point }
     (AItem^.Index<>SymRefPoint^.Index) then { werden ignoriert, nur f�r Berechnung }
       begin
       SymObject:=ObjectToSymObject(Project,AItem);
       if (SymObject<>nil) then Project^.SymEditInfo.SymbolToEdit^.Data^.Insert(SymObject);
       end;
  end;
begin
  if (Project<>nil) then
    with Project^,Project^.SymEditInfo do
      begin
        SymbolToEdit^.Data^.DeleteAll;               { delete old symbol elements }
        // copy objects in z-order into symbol (and convert them to symbol objects)
        SymLayer:=PZLayer(Layers^.Toplayer);
        For i:=SymLayer^.ZOrder.Count-1 downto 0 do
          begin
            DoAll(SymLayer^.ZOrder[i]);
          end;
        // move symbol so reference-point is at (0,0)
        SymbolToEdit.MoveRel(-SymRefPoint^.Position.X,-SymRefPoint^.Position.Y);
        // store unite-line position
        UnitPoly^.MoveRel(-SymRefPoint^.Position.X,-SymRefPoint^.Position.Y);
        with PDPoint(UnitPoly^.Data^.At(0))^ do
          SymbolToEdit^.RefLinePoint1:=GrPoint(X,Y);
        with PDPoint(UnitPoly^.Data^.At(1))^ do
          SymbolToEdit^.RefLinePoint2:=GrPoint(X,Y);
{++ Ivanoff BUG#NEW12 BUILD#100}
{After change of ReferenceLine length the symbol size was changed
only after reload of project.}
        SymbolToEdit.RefLineLength := GrPointDistance(SymbolToEdit.RefLinePoint1, SymbolToEdit.RefLinePoint2);
{-- Ivanoff}
        SymbolToEdit^.CalculateClipRect;
        SymbolToEdit^.SetLastChange;
        if (FSymLib=nil) then
          begin
            if (SymParent<>nil) and (SymExtLib=nil) then
              TMDIChild(SymParent).Data^.Modified:=True;  { Vaterprojekt wurde ge�ndert }
            Modified:=FALSE;        { Symbol ist gespeichert... }
          end
        else
          begin
            if (SymParent<>nil) {and (SymExtLib=nil)} then
              with PSymbols(TMDIChild(SymParent).Data^.PInfo^.Symbols)^ do
                begin
                  ProjSym:=GetProjectSymbol(SymbolToEdit^.GetName,SymbolToEdit^.GetLibName);
                  if (ProjSym<>nil) then
                    UpdateSym(ProjSym,SymbolToEdit);   { Projektsymbol updaten }
                end;
            FSymLib^.LibState:=FSymLib^.LibState or el_Modified;   { ext. Bib wurde ge�ndert }
{++ Moskaliov BUG#469 BUILD#100 12.01.00}
            Modified:=FALSE;        { Symbol was stored }
{-- Moskaliov BUG#469 BUILD#100 12.01.00}
          end;
      end;
end;

function TSymEditor.CanEndEditor:Boolean;
var Answer : Integer;
begin
  CanEndEditor:=False;
  Answer:=MessageDialog(MlgStringList['SymbolEditor',1],mtInformation,
             [mbYes,mbNo,mbCancel],0);
  case Answer of
      mrYes : begin
              SaveSymbol;                      { ge�ndertes Symbol speichern }
              CanEndEditor:=True;
              end;
      mrNo  : begin
              CanEndEditor:=True;
              end;
      mrCancel : CanEndEditor:=False;
      end;
end;

{******************************************************************************}
{ Procedure SetupSymbolEditor                                                  }
{ Sets up the symbol editor:                                                   }
{ Converting all symbol elements to their according drawing objects (TSPoly->TPoly,etc.)}
{ setting reference point and reference line                                   }
{ creates a temporary layer 'Symboleditor' and turns off all other layers      }
{ current project view is saved before in method Create                        }
{******************************************************************************}
Procedure TSymEditor.SetupSymbolEditor;
var HPkt     : TDPoint;
    RefLayer : pLayer;

Procedure DoAll                     { converts and inserts all object }
    (
    Item           : PView
    ); Far;
   var AItem       : PView;
   begin
     // convert object and insert into symlayer
     AItem:=SymObjectToObject(Project,Item);
     if (AItem<>NIL) then begin
       if Item^.ObjectStyle=NIL then Project^.InsertObject(AItem,False)
       else Project^.InsertObjectWithStyle(AItem,Project^.Layers^.TopLayer,
           Item^.ObjectStyle^);
       end;
   end;

  begin
  with Project^,Project^.SymEditInfo do begin
    Parent.Caption:=MlgStringList['SymbolEditor',2]+' ['+SymbolToEdit^.GetName+']';
    SymRect.Init;
    SymRect.Assign(-150000,-150000,150000,150000);
{++ Ivanoff BUG#635 BUILD#100}
{The order of creation of layers in project was rearranged. The layer with objects of the symbol
now is created as first. The refence layer is created as second.}
{-- Ivanoff}
    Layers^.InsertZLayer(FEditLayer,nl_Color,nl_Line,RGB(255,255,255),pt_Solid,
                         ilTop,DefaultSymbolFill);                 { Neuer temp. Layer }
    // reference layer is top (ref pnt and unit line always visible)
    // but FEditLayer is working layer !!!
    Layers^.SetTopLayer(Layers^.NameToLayer(FEditLayer));
    SymbolToEdit^.Data^.ForEach(@DoAll);              { Symbol generieren }
    PZLayer(Layers^.TopLayer)^.InvertZOrder;

    // insert layer with correct style for symbols  c_White    pt_Solid
    FRefLayer:=Layers^.InsertLayer(FRefLayerName,nl_Color,nl_Line,RGB(255,255,255),pt_Solid,
                         ilTop,DefaultSymbolFill);                 { Neuer temp Layer f�r Referenzpunkt }
                                                                   { und Ref-Linie }
    SymRefPoint:=new(PPixel,Init(SymbolToEdit^.RefPoint));
    InsertObjectWithStyle(SymRefPoint,FRefLayer,RefStyleData);

    UnitPoly:=New(PPoly,Init);                        { Einheitslinie erzeugen }
    with SymbolToEdit^.RefLinePoint1 do HPkt.Init(LimitToLong(X),LimitToLong(Y));
    UnitPoly.InsertPoint(HPkt);
    with SymbolToEdit^.RefLinePoint2 do HPkt.Init(LimitToLong(X),LimitToLong(Y));
    UnitPoly.InsertPoint(HPkt);
    InsertObjectWithStyle(UnitPoly,FRefLayer,RefStyleData);

    ZoomByRect(RotRect(SymRect.A.X,SymRect.A.Y,SymRect.XSize,SymRect.YSize,0),0,TRUE);      { MARTIN->Additional Frame ? }
{++ Ivanoff BUG#635 BUILD#100}
{Reference layer is top one. For select objects on layer with the symbol objects
that layer is made as top layer.}
    Layers^.SetTopLayer(Layers^.NameToLayer(FEditLayer));
{-- Ivanoff}
    PInfo^.RedrawScreen(True);
    Modified:=False;
    end;
  end;

{******************************************************************************}
{ Methode DrawUnit                                                             }
{ Einheitslinie zeichnen                                                       }
{******************************************************************************}
procedure TSymEditor.DrawUnitLine;
begin
  if (Project<>nil) then begin
    Project^.SetActualMenu(mn_DrawSymLine);
    end;
end;

{******************************************************************************}
{ Methode SetupRefStyle                                                        }
{ Stil f�r Referenzpunkt und Einheitslinie vorbereiten                         }
{******************************************************************************}
procedure TSymEditor.SetupRefStyle;
begin
  with RefStyleData do begin
    LineStyle.Color:=RGB(255,0,0);
    LineStyle.Style:=0;
    LineStyle.Width:=4;
    LineStyle.WidthType:=stScaleIndependend;
    FillStyle:=DefaultFillStyle;
    SymbolFill:=DefaultSymbolFill;
    end;
end;

{******************************************************************************}
{ Methode SetUnitLine                                                          }
{ Die Einheitslinie wird auf AItem gesetzt, eine ev. vorher eingestellte       }
{ Einheitslinie wird gel�scht.                                                 }
{******************************************************************************}
procedure TSymEditor.SetUnitLine(AItem:PIndex);
var Item    : PView;
    UpdRect : TDRect;
begin
  if (UnitPoly<>nil) and (Project<>nil) then
    with Project^ do begin
      Item:=Layers^.IndexObject(PInfo,UnitPoly);
      if (Item<>nil) then begin
        UpdRect.Init;
        UpdRect.CorrectByRect(Item^.ClipRect);
        Layers^.DeleteIndex(PInfo,Item);
        PObjects(PInfo^.Objects)^.DeleteIndex(PInfo,Item);
        PInfo^.FromGraphic:=TRUE;
        PInfo^.RedrawRect(UpdRect);
      end;
      end;
  UnitPoly:=PPoly(AItem);
end;

{******************************************************************************}
{ Methode DrawRefPoint                                                         }
{ Referenzpunkt zeichnen                                                       }
{******************************************************************************}
procedure TSymEditor.DrawRefPoint;
begin
  if (Project<>nil) then begin
    Project^.SetActualMenu(mn_SetRefPoint);
    end;
end;

{******************************************************************************}
{ Methode SetRefPoint                                                          }
{ Der Referenzpunkt wird auf APoint gesetzt, ein ev. bereits ex. Referenz-     }
{ punkt wird gel�scht                                                          }
{******************************************************************************}
procedure TSymEditor.SetRefPoint(APoint: PIndex);
var Item    : PView;
    UpdRect : TDRect;
begin
  if (SymRefPoint<>nil) and (Project<>nil) then
    with Project^ do begin
      Item:=Layers^.IndexObject(PInfo,SymRefPoint);
      if (Item<>nil) then begin
        UpdRect.Init;
        UpdRect.CorrectByRect(Item^.ClipRect);
        Layers^.DeleteIndex(PInfo,Item);
        PObjects(PInfo^.Objects)^.DeleteIndex(PInfo,Item);
        PInfo^.FromGraphic:=TRUE;
        PInfo^.RedrawRect(UpdRect);
        end;
      end;
  SymRefPoint:=PPixel(APoint);
end;

Procedure TSymEditor.OpenSymProperties;
var ExtLib : pExtLib;
  begin
(*  SymPropDlg:=TSymPropDlg.Create(WinGISMainForm.GetCurrentChild);
  SymPropDlg.Project:=Project;
  with Project^.SymEditInfo do begin
    if (SymExtLib=nil) then SymPropDlg.Symbols:=PSymbols(Project^.PInfo^.Symbols)
                       else SymPropDlg.Symbols:=SymExtLib^.Symbols;
    SymPropDlg.Symbol:=SymbolToEdit;
    end;

  if SymPropDlg.ShowModal=mrOK then begin
    ExtLib:=Project^.SymEditInfo.SymExtLib;
    if (ExtLib<>nil) then begin  { Projektsymbol aktuell halten }
      ExtLib^.LibState:=ExtLib^.LibState or el_Modified;
      end else Project^.SetModified;  { Projekt wurde ge�ndert ! }
    SymPropDlg.Free;
    end;*)
  end;

Procedure TSymEditor.BringToFront;
var SelLayer : pLayer;
    SymLayer : pZLayer;
  Function DoAll
     (
     Item          : PIndex
     )
     : Boolean; Far;
    var i    : Integer;
        aItem : PIndex;
    begin
      with SymLayer^ do begin
        i:=0;
        while (i<ZOrder.Count) and
              (PIndex(ZOrder[i])^.Index<>Item^.Index) do i:=i+1;
        if (i<ZOrder.Count) then begin
          // swap elements
          aItem:=ZOrder[i];
          ZOrder.Delete(i);
          ZOrder.Add(aItem);
          end;
        end;
      Result:=True;
    end;

begin
  with Project^ do begin
    SelLayer:=Layers^.SelLayer;
    Symlayer:=PZLayer(Layers^.TopLayer);
    SelLayer^.Data^.ForEach(@DoAll);
    DeselectAll(True);
    SetModified;
    PInfo^.RedrawScreen(True);
    end;
end;

Procedure TSymEditor.SendToBack;
var SelLayer : pLayer;
    SymLayer : pZLayer;
  Function DoAll
     (
     Item          : PIndex
     )
     : Boolean; Far;
    var i : Integer;
        aItem : PIndex;
    begin
      with SymLayer^ do begin
        i:=0;
        while (i<ZOrder.Count) and
              (PIndex(ZOrder[i])^.Index<>Item^.Index) do i:=i+1;
        if (i<ZOrder.Count) then begin
          // swap elements
          aItem:=ZOrder[i];
          ZOrder.Delete(i);
          ZOrder.Insert(0,aItem);
          end;
        end;
      Result:=True;
    end;

begin
  with Project^ do begin
    SelLayer:=Layers^.SelLayer;
    Symlayer:=PZLayer(Layers^.TopLayer);
    SelLayer^.Data^.ForEach(@DoAll);
    DeselectAll(True);
    SetModified;
    PInfo^.RedrawScreen(True);
    end;
end;

end.
