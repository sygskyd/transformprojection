{******************************************************************************+
  Unit SymbolFill
--------------------------------------------------------------------------------
  Autor: Martin Forst
--------------------------------------------------------------------------------
  Function to fill an area with a symbol.
--------------------------------------------------------------------------------
  Release: 1
+******************************************************************************}

Unit SymbolFill;

Interface

Uses AM_Def,AM_Paint;

Procedure SymbolFillRectangle(PInfo:PPaint;const Area:TDRect;Param3:LongInt);

Implementation

Uses AM_Obj,AM_Sym,Classes,Forms,GrTools,Messages,Windows,NumTools,AM_Layer;

{******************************************************************************+
  Procedure SymbolFillRectangle(PInfo:PPaint)
--------------------------------------------------------------------------------
  Fills the specified srea with the symbol-fill defined in
  PInfo^.ActiveSymbolFill.
+******************************************************************************}
Procedure SymbolFillRectangle(PInfo:PPaint;const Area:TDRect;Param3:LongInt);
var ARect          : TGrRect;
    DeltaX         : Double;
    DeltaY         : Double;
    SinAlpha       : Double;
    CosAlpha       : Double;
    ScanPosition   : TGrPoint;
    ScanVector     : TGrPoint;
    RHeight        : Double;
    Point1         : TGrPoint;
    Point2         : TGrPoint;
    Cnt            : Integer;
    ASymbol        : PSymbol;
    AOffset        : Double;
    CntY           : Integer;
    Left           : Double;
    Right          : Double;
    UseDistance    : Double;
    UseLineDist    : Double;
    UseOffset      : Double;
{++ Ivanoff BUG#309 BUILD#100}
    oldSF: TSymbolFill;
{-- Ivanoff}
begin
  with PInfo^,ActSymbolFill do begin
    ExtCanvas.Push;
    try
      // activate line- and fill-styles
      PInfo^.SetLineStyle(LineStyle,FALSE);
      PInfo^.SetFillStyle(FillStyle,FALSE);
        if PLayer(Pinfo^.NowDrawingLayer)<>nil then
         if PLayer(Pinfo^.NowDrawingLayer).SymbolFill.FillType= symfRegular
          then begin;
           if DblCompare(Angle,dblFromLayer)=0
            then Angle:= PLayer(Pinfo^.NowDrawingLayer).SymbolFill.Angle;
           if DblCompare(Distance,dblFromLayer)=0
            then Distance:= PLayer(Pinfo^.NowDrawingLayer).SymbolFill.Distance;
           if DblCompare(LineDistance,dblFromLayer)=0
            then LineDistance:= PLayer(Pinfo^.NowDrawingLayer).SymbolFill.LineDistance;
           if DblCompare(OffSet,dblFromLayer)=0
            then Offset:= PLayer(Pinfo^.NowDrawingLayer).SymbolFill.OffSet;
               end;

      // calculate width and height of area rotated by fill-angle
      with Area do ARect:=GrRect(A.X,A.Y,B.X,B.Y);
      DeltaX:=RectWidth(ARect);
      DeltaY:=RectHeight(ARect);
      SinCos(Angle,SinAlpha,CosAlpha);
      RHeight:=Abs(DeltaX*SinAlpha)+Abs(DeltaY*CosAlpha);
      // calculate start- and end-point of the scan-line
      DeltaX:=DeltaX*SinAlpha;
      ScanPosition:=GrPoint(ARect.Left+DeltaX*SinAlpha,ARect.Bottom-DeltaX*CosAlpha);
      ScanVector:=GrPoint(CosAlpha,SinAlpha);
      // initialize symbol to draw
      UseDistance:=PInfo^.CalculateSize(DistanceType,Distance);
      UseLineDist:=PInfo^.CalculateSize(DistanceType,LineDistance);
      if OffsetType=stPercent then UseOffset:=UseDistance*Offset
      else UseOffset:=PInfo^.CalculateSize(OffsetType,Offset);
      // calculate offset-parameter of the scanline
      DeltaX:=-UseLineDist*SinAlpha;
      DeltaY:=UseLineDist*CosAlpha;
      ASymbol:=New(PSymbol,Init(PInfo,Area.A,Symbol,10,SymbolAngle));
      ASymbol.Size:=Size;
      ASymbol.SizeType:=SizeType;
      // !?!?! check if symbol exists
      try
{++ Ivanoff BUG#309 BUILD#100
Saving the current symbol filling style.}
        oldSF := PInfo.ActSymbolFill;
{-- Ivanoff}
        AOffset:=0;
        for CntY:=0 to Trunc(RHeight/UseLineDist)+1 do begin
          // calculate cut of scan-line with the area
          if ClipVectorToRect(ScanPosition,ScanVector,ARect,Point1,Point2) then begin
            Left:=GrPointDistance(ScanPosition,Point1);
            Right:=GrPointDistance(ScanPosition,Point2);
            for Cnt:=Trunc((Left-AOffset)/UseDistance) to Trunc((Right-AOffset)/UseDistance)+1 do begin
              ASymbol^.Position.X:=LimitToLong(ScanPosition.X+(Cnt*UseDistance+AOffset)*CosAlpha);
              ASymbol^.Position.Y:=LimitToLong(ScanPosition.Y+(Cnt*UseDistance+AOffset)*SinAlpha);
              ASymbol^.Draw(PInfo,FALSE);
              if PInfo^.KeepWindowsAlive then Exit;
            end;
          end;                                    
          // move scan-line
          MovePoint(ScanPosition,DeltaX,DeltaY);
          // update interline-offset
          AOffset:=AOffset+UseOffset;
        end;
      finally
        Dispose(ASymbol,Done);
{++ Ivanoff BUG#309 BUILD#100
Restoring the current symbol filling style.}
        PInfo.ActSymbolFill := oldSF;
{-- Ivanoff}
      end;
    finally
      ExtCanvas.Pop;
    end;
  end;
end;

end.
