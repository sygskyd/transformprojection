unit IDB_MenuHndl;
{
Internal database.
Internal database WinGIS menu handler.

All operations that are linked with buttons on toolbars and items in menus
are placed in DatabaseHndl.pas.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 30-06-2000
}

interface
{
Internal database. Ver. II.
Internal database menu manager of WinGIS.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date:
}
Uses Classes, ProjHndl, Forms;

Type
    TIDB_MonitoringHandler = Class(TProjMenuHandler)
          Procedure   OnStart; override;
       End;

    TIDB_ShowLayerTableHandler = Class(TProjMenuHandler)
          Procedure   OnStart; override;
       End;

    TIDB_MakeAnnotationsHandler = Class(TProjMenuHandler)
          Procedure   OnStart; override;
       End;

    TIDB_InfoHandler = Class(TprojMenuHandler)
          Procedure   OnStart; override;
       End;


implementation

Uses MenuFn,
     Am_Main, AM_Proj;

{This procedure starts a monitoring process in the Internal Database.}
Procedure TIDB_MonitoringHandler.OnStart;
Begin
(*
     {$IFNDEF WMLT}
     If (WinGisMainForm.WeAreUsingTheIDBUserInterface[Project]) Then
        WinGisMainForm.IDB_Man.ShowMonitoringTable;
     {$ENDIF}
*)
End;

{This procedure starts a process that creates annotations objects in the project by calling the IDB procedure.}
Procedure TIDB_MakeAnnotationsHandler.OnStart;
Begin
     {$IFNDEF AXDLL}
     If (WinGisMainForm.WeAreUsingTheIDBUserInterface[Project]) Then
        WinGisMainForm.IDB_Man.MakeAnnotations(WinGisMainForm.ActualProj);
//        WinGisMainForm.IDB_Man.ShowDataTableWithSelectBefore(WinGisMainForm.ActualProj);
     {$ENDIF}
End;

{ If there is noone selecetd object in the project, this procedure starts a process that
  shows a layer database atributive table. If there is at least one object selecetd in the project,
  the procedure starts a monitoring process in the Internal Database. }
Procedure TIDB_ShowLayerTableHandler.OnStart;
var
i: Integer;
Begin
     {$IFNDEF AXDLL}
     If WinGisMainForm.WeAreUsingTheIDBUserInterface[Project] Then begin
        WinGisMainForm.IDB_Man.ShowDataTable;
        for i:=0 to WingisMainForm.MDIChildCount-1 do begin
            if WingisMainForm.MDIChildren[i].ClassName = 'TIDB_ViewForm' then begin
               TForm(WingisMainForm.MDIChildren[i]).BringToFront;
               break;
            end;
            if WingisMainForm.MDIChildren[i].ClassName = 'TIDB_MonitorForm' then begin
               TForm(WingisMainForm.MDIChildren[i]).BringToFront;
               break;
            end;
        end;
     end;
     {$ENDIF}
End;

Procedure TIDB_InfoHandler.OnStart;
Begin
     {$IFNDEF AXDLL}
     If (WinGisMainForm.WeAreUsingTheIDBUserInterface[Project]) Then
      begin;
        WinGISMainForm.ReverseUsingIDBInfo;
   {brovak}
        if WinGISMainForm.GetWeAreUsingTheIDBInfoWindow = true then
          If PProj(WinGISMainForm.ActualProj).Layers.SelLayer.Data.GetCount = 1 Then
           WinGisMainForm.IDB_Man.ShowInfoTable;
      end;
   {brovak}
     {$ENDIF}
End;

end.
