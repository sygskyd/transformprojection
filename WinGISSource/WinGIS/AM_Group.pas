{******************************************************************************+
  Unit
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
+******************************************************************************}
unit AM_Group;

{$D+,L+}

interface

uses WinProcs, Objects, WinTypes, SysUtils, ExtCtrls, Graphics,
  AM_Def, AM_Paint, AM_View, AM_Coll, AM_Index, AM_Font, GrTools, AM_CPoly,
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
  Forms, TeeProcs, TeEngine, Chart, Series;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

const
  co_InitSize = 10;
  co_Expand = 10;
  GroupVersion = 1;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
  ChartKindVersion = 1;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

type
  PGroup = ^TGroup;
  TGroup = object(TView)
    Data: PCollection;
    constructor Init;
    constructor Load(S: TOldStream);
    destructor Done; virtual;
    procedure CalculateClipRect;
    procedure Draw(PInfo: PPaint; Clipping: Boolean); virtual;
    procedure DeleteItem(AItem: PView); virtual;
    procedure MoveRel(XMove, YMove: LongInt); virtual;
    procedure InsertItem(AItem: PView); virtual;
    procedure Scale(XScale, YScale: Real); virtual; {2705F}
    function SelectByPoint(PInfo: PPaint; Point: TDPoint; const ObjType: TObjectTypes): PIndex; virtual;
    function SelectByRect(PInfo: PPaint; Rect: TDRect; Inside: Boolean): Boolean; virtual;
    procedure Store(S: TOldStream); virtual;
    function GetObjType: Word; virtual;
  end;

  PPSGroup = ^PSGroup;
  PSGroup = ^TSGroup;
  TSGroup = object(TGroup)
       // position of the reference-point
    RefPoint: TDPoint;
       // points of the reference-line
    RefLinePoint1: TGrPoint;
    RefLinePoint2: TGrPoint;
    RefLineLength: Double;
       // how often the symbol is used in the project
    UseCount: LongInt;
       // name of the symbol
    Name: PString;
       // name of the symbol-library, nil if internal library
    LibName: PString;
       // default insertion-angle
    DefaultAngle: Double;
       // default insertion-size
    DefaultSize: Double;
       // type of the default-size
    DefaultSizeType: Integer8;
       // date and time of the last change
    LastChange: TDateTime;
       // true if the symbol is opened in an editor
    InEditor: Boolean;
    constructor Init;
    constructor InitName(const AName, ALibName: string);
    constructor Load(S: TOldStream);
    constructor LoadOld(S: TOldStream);
    procedure Draw(PInfo: PPaint; Clipping: Boolean); virtual;
    procedure DrawSymbolInRect(aDC: hDC; Rect: TRect; Fonts: PFonts); overload;
    procedure DrawSymbolInRect(PInfo: PPaint; Rect: TRect); overload;
    function GetName: string;
    procedure SetName(NewName: string);
    procedure SetLibName(NewName: string);
    function GetLibName: string;
    function GetHashIndex: LongInt;
    procedure InsertItemDraw(AItem: PView; PInfo: PPaint);
    function IsNewSymbol: Boolean;
    procedure Store(S: TOldStream); virtual;
    procedure SetLastChange;
    function Visible(PInfo: PPaint): Boolean; virtual;
    destructor Done; virtual;
    procedure ReverseItemOrder;
  end;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}
  PChartKind = ^TChartKind;
  TChartKind = object(TGroup)
    DiagType: Byte;
    HashIndex: LongInt;
    TemplateChart: TChart;
    TemplateChartSeries: TChartSeries;
        // how often this Chart Kind is used in the project
    UseCount: LongInt;
    OldDeterminant: string; // temporarily for DEBUG
    PropFactor: Double;
    PropMode: Integer;
    bProp: Boolean;
    constructor Init;
    constructor InitKind(ADiagType: Byte; Width: Integer; Height: Integer;
      View3D: Boolean; Transparent: Boolean;
      BevelInner: TPanelBevel; BevelOuter: TPanelBevel; BevelWidth: Integer;
      BorderStyle: TBorderStyle; BorderWidth: Integer;
      MarginLeft: Integer; MarginTop: Integer; MarginRight: Integer; MarginBottom: Integer;
      BackWallTransparent: Boolean; LegVisible: Boolean; AxisVisible: Boolean;
      AxisFontName: string; AxisFontCharset: TFontCharset; AxisFontHeight: Integer;
      LeftAxisVisible: Boolean; LeftAxisGridVisible: Boolean;
      LeftAxisTicksVisible: Boolean; LeftAxisMinorTicksVisible: Boolean;
      LeftAxisLabels: Boolean; LeftAxisLabelsSeparation: Integer;
      LeftALabStyle: TAxisLabelStyle; LeftALabAngle: Integer; LeftALabSize: Integer;
      BottomAxisVisible: Boolean; BottomAxisGridVisible: Boolean;
      BottomAxisTicksVisible: Boolean; BottomAxisMinorTicksVisible: Boolean;
      BottomAxisLabels: Boolean; BottomAxisLabelsSeparation: Integer;
      BottomALabStyle: TAxisLabelStyle; BottomALabAngle: Integer; BottomALabSize: Integer;
      MarksVisible: Boolean; MarksStyle: TSeriesMarksStyle;
      MarksFontName: string; MarksFontCharset: TFontCharset; MarksFontHeight: Integer;
      MarksTransparent: Boolean; MarksColor: TColor;
      MarksFrameVisible: Boolean;
      MarksShadowSize: Integer; MarksShadowColor: TColor);
    constructor InitByExistChart(ExistChart: TChart; ExistSeries: TChartSeries;
      RelAbsSize: Boolean; Sum: Double);
    constructor Load(S: TOldStream);
    destructor Done; virtual;
    procedure Store(S: TOldStream); virtual;
    function GetObjType: Word; overload; virtual;
    function GetHashIndex: LongInt;
    function OnCompareItems(Item: PChartKind): Boolean;
  end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

implementation

uses Hash, Win32Def, NumTools, StyleDef, ExtLib,
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
  AM_Main, AM_BuGra, Controls;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

{==============================================================================+
  Object TGroup
+==============================================================================}

constructor TGroup.Init;
begin
  TView.Init;
  Data := New(PCollection, Init(co_InitSize, co_Expand));
end;

constructor TGroup.Load(S: TOldStream);
begin
  TView.Load(S);
  Data := Pointer(S.Get);
end;

procedure TGroup.Store(S: TOldStream);
begin
  TView.Store(S);
  S.Put(Data);
end;

destructor TGroup.Done;
begin
  if Data <> nil then
  begin
    Dispose(Data, Done);
    TView.Done;
  end;
end;

procedure TGroup.CalculateClipRect;

  procedure DoAll(Item: PView); far;
  begin
    ClipRect.CorrectByRect(Item^.ClipRect);
  end;

begin
  ClipRect.Assign(MaxLongInt, MaxLongInt, -MaxLongInt, -MaxLongInt);
  Data^.ForEach(@DoAll);
end;

procedure TGroup.Draw(PInfo: PPaint; Clipping: Boolean);
var
  Item: PView;
  Cnt: LongInt;
  UseLineStyle: TLineStyle;
  UseFillStyle: TFillStyle;
  AMode: Word;
begin
  if Data <> nil then
    for Cnt := Data^.Count - 1 downto 0 do
    begin
      Item := Data^.At(Cnt);
      if Item^.ObjectStyle = nil then
        Item^.Draw(PInfo, Clipping)
      else
        if PInfo^.DrawMode and dm_SelDesel <> 0 then
          Item^.Draw(PInfo, Clipping)
        else
          with Item^ do
          begin
            AMode := PInfo^.DrawMode;
            CalculateLineStyle(ObjectStyle^.LineStyle, PInfo^.ActLineStyle, UseLineStyle);
            CalculateFillStyle(ObjectStyle^.FillStyle, PInfo^.ActFillStyle, UseFillStyle);
            PInfo^.ExtCanvas.Push;
            try
              PInfo^.SetLineStyle(UseLineStyle, FALSE);
              PInfo^.SetFillStyle(UseFillStyle, FALSE);
              PInfo^.SetSymbolFill(DefaultSymbolFill);
              Item^.Draw(PInfo, Clipping);
            finally
              PInfo^.DrawMode := AMode;
              PInfo^.ExtCanvas.Pop;
            end;
          end;
    end;
end;

procedure TGroup.DeleteItem(AItem: PView);
begin
  Data^.Delete(AItem);
  CalculateClipRect;
end;

procedure TGroup.InsertItem(AItem: PView);
begin
  ClipRect.CorrectByRect(AItem^.ClipRect);
  Data^.Insert(AItem);
end;

function TGroup.SelectByPoint(PInfo: PPaint; Point: TDPoint; const ObjType: TObjectTypes): PIndex;

  function DoAll(Item: PView): Boolean;
  var
    FItem: PIndex;
  begin
    FItem := Item^.SelectByPoint(PInfo, Point, ObjType);
    if FItem <> nil then
    begin
      SelectByPoint := @Self;
      DoAll := TRUE;
    end
    else
      DoAll := FALSE;
  end;

begin
  SelectByPoint := nil;
  Data^.FirstThat(@DoAll);
end;

function TGroup.SelectByRect(PInfo: PPaint; Rect: TDRect; Inside: Boolean): Boolean;

  function DoAll(Item: PView): Boolean;
  begin
    DoAll := Item^.SelectByRect(PInfo, Rect, Inside);
  end;

begin
  SelectByRect := Data^.FirstThat(@DoAll) <> nil;
end;

function TGroup.GetObjType: Word;
begin
  GetObjType := ot_Group;
end;

procedure TGroup.Scale(XScale: Real; YScale: Real);

  procedure DoAll(Item: PView); far;
  begin
    Item^.Scale(XScale, YScale);
  end;

begin
  inherited Scale(XScale, YScale);
  Data^.ForEach(@DoAll);
end;

procedure TGroup.MoveRel(XMove: LongInt; YMove: LongInt);

  procedure DoAll(Item: PView); far;
  begin
    Item^.MoveRel(XMove, YMove);
  end;

begin
  inherited MoveRel(XMove, YMove);
  Data^.ForEach(@DoAll);
end; {2705F}

{===============================================================================
| Methoden Objekt TSGroup
+==============================================================================}

constructor TSGroup.Init;
begin
  inherited Init;
  RefPoint.Init(0, 0);
  UseCount := 0;
  Name := nil;
  LibName := nil;
  DefaultAngle := 0;
{++ Moskaliov NewDefParameters BUILD#125 01.08.00}
// DefaultSize:=100;
// DefaultSizeType:=stDrawingUnits;
  DefaultSizeType := stScaleInDependend; // Scale Independed Size type
  DefaultSize := 50; // 5 mm Size
{-- Moskaliov NewDefParameters BUILD#125 01.08.00}
  SetLastChange;
  SetState(sf_SymName, true); { neues Symbol (wegen UseCount-Bug)! }
  ClipRect.Init;
  ClipRect.Assign(-sym_SYMBOLSIZE, -sym_SYMBOLSIZE,
    sym_SYMBOLSIZE, sym_SYMBOLSIZE); { damit auch leere Symbole eine Gr��e haben }
  InEditor := False;
   // setup default reference-line
  RefLinePoint1 := GrPoint(-50000, 0);
  RefLinePoint2 := GrPoint(50000, 0);
  RefLineLength := 100000;
end;

constructor TSGroup.InitName(const AName: string; const ALibName: string);
begin
  TSGroup.Init;
  Name := NewStr(AName); { Kopie anlegen }
  if (ALibName = '') then
    LibName := nil
  else
    LibName := NewStr(ALibName);
end;

constructor TSGroup.LoadOld(S: TOldStream);

  procedure DoAll(Item: PIndex); far;
  begin
    Item^.Scale(1000, 1000);
  end;
var
  Zoom: Real48;
begin
  inherited Load(S);
  RefPoint.Load(S);
  S.Read(Zoom, SizeOf(Zoom));
  UseCount := 0;
  Convert := TRUE;
   // old symbols get their index as name
  Name := NewStr(IntToStr(Index - mi_Group));
  LibName := nil;
   // initialize all values with default-values
  DefaultAngle := 0;
   // the old zoom gives the default-size in drawing-units
  DefaultSize := Zoom * 100;
  DefaultSizeType := stDrawingUnits;
  RefPoint.Init(0, 0);
   // scale all sub-items
  Data^.ForEach(@DoAll);
   // setup reference-line
  RefLinePoint1 := GrPoint(-50000, 0);
  RefLinePoint2 := GrPoint(50000, 0);
  RefLineLength := 100000;
  SetLastChange;
  CalculateClipRect;
  InEditor := False;
  ReverseItemOrder;
end;

constructor TSGroup.Load(S: TOldStream);

  procedure DoAll(Item: PIndex); far;
  begin
    Item^.Scale(1000, 1000); { 1 x 1 m (alt) -> 1000 x 1000 m (neu) }
  end;
var
  Version: Byte;
  Zoom: Real48;
begin
  inherited Load(S);
  RefPoint.Load(S);
  S.Read(Zoom, SizeOf(Zoom));
  S.Read(UseCount, SizeOf(UseCount));
  SetLastChange;
  if GetState(sf_SymName) then
  begin { neues Symbol mit Namen, etc. }
    S.Read(Version, SizeOf(Version)); { Symbolversion lesen }
    Name := S.ReadStr; { Symbolname }
    LibName := S.ReadStr; { Bibliotheksname }
    if LibName <> nil then
    begin
      Dispose(LibName);
    end;
    LibName := NewStr(ExtLib.LibNameLoading);
    S.Read(DefaultAngle, SizeOf(DefaultAngle));
    S.Read(DefaultSize, SizeOf(DefaultSize));
    S.Read(DefaultSizeType, SizeOf(DefaultSizeType));
         // load reference-line and calculate it's length
    S.Read(RefLinePoint1, SizeOf(RefLinePoint1));
    S.Read(RefLinePoint2, SizeOf(RefLinePoint2));
    RefLineLength := GrPointDistance(RefLinePoint1, RefLinePoint2);
    S.Read(LastChange, SizeOf(LastChange))
  end
  else
  begin
    Name := nil;
    LibName := nil;
    DefaultAngle := 0;
         // the index gives the name of the symbol
    Name := NewStr(IntToStr(Index - mi_Group));
         // the old zoom gives the default-size
    DefaultSize := Zoom * 100;
    DefaultSizeType := stDrawingUnits;
         // setup default reference-line
    RefLinePoint1 := GrPoint(-50000, 0);
    RefLinePoint2 := GrPoint(50000, 0);
    RefLineLength := 100000;
    RefPoint.Init(0, 0);
    Data^.ForEach(@DoAll); { alle Symbolelemente konvertieren ! }
    ReverseItemOrder;
         // update the clipping-rectangle
    CalculateClipRect;
  end;
  InEditor := False;
end;

procedure TSGroup.Store(S: TOldStream);
var
  Version: Byte;
  Zoom: Real48;
begin
  SetState(sf_SymName, TRUE);
  inherited Store(S);
  RefPoint.Store(S);
  Zoom := 0;
  S.Write(Zoom, SizeOf(Zoom));
  S.Write(UseCount, SizeOf(UseCount));
  Version := GroupVersion; { f�r sp�tere �nderungen vorges. 220797Ray }
  S.Write(Version, SizeOf(Version));
  S.WriteStr(Name);
  S.WriteStr(LibName);
  S.Write(DefaultAngle, SizeOf(DefaultAngle));
  S.Write(DefaultSize, SizeOf(DefaultSize));
  S.Write(DefaultSizeType, SizeOf(DefaultSizeType));
  S.Write(RefLinePoint1, SizeOf(RefLinePoint1));
  S.Write(RefLinePoint2, SizeOf(RefLinePoint2));
  S.Write(LastChange, SizeOf(LastChange));
end;

procedure TSGroup.DrawSymbolInRect(PInfo: PPaint; Rect: TRect);
var
  AScale: Double;
  Pos: TDPoint;
begin
  SetMapMode(PInfo^.ExtCanvas.Handle, mm_LoMetric);
  PInfo^.ExtCanvas.Brush.ForeColor := $FFFFFF;
  PInfo^.ExtCanvas.Brush.Style := pt_Solid;
  DPToLP(PInfo^.ExtCanvas.Handle, Rect, 2);
  // setup dc and offsets
  PInfo^.StartPaint(PInfo^.ExtCanvas.Handle, Rect, -Rect.Left - RectWidth(Rect) div 2,
    -Rect.Top + RectHeight(Rect) div 2);
  // calculate symbol-scale and symbol-position
  InflateRect(Rect, 10, 10);
  AScale := Min(RectWidth(Rect), RectHeight(Rect)) / Max(ClipRect.XSize, ClipRect.YSize);
  Pos.Init(-LimitToLong((ClipRect.A.X + ClipRect.B.X) * AScale / 2),
    -LimitToLong((ClipRect.A.Y + ClipRect.B.Y) * AScale / 2));
  // setup PInfo
  PInfo^.SetScaling(AScale, 0, Pos);
  // draw the symbol
  Draw(PInfo, FALSE);
  SetMapMode(PInfo^.ExtCanvas.Handle, mm_Text);
end;

procedure TSGroup.DrawSymbolInRect(aDC: hDC; Rect: TRect; Fonts: PFonts);
var
  PInfo: PPaint;
begin
  // create a new pinfo-object, switch off caching, set fonts-pointer
  PInfo := New(PPaint, Init);
  PInfo.hWindow := WindowFromDC(aDC);
  PInfo^.ExtCanvas.Cached := FALSE;
  PInfo^.ExtCanvas.Handle := aDC;
  Dispose(PInfo^.Fonts, Done);
  PInfo^.Fonts := Fonts;
  DrawSymbolInRect(PInfo, Rect);
  // reset and dispose pinfo
  PInfo^.Fonts := nil;
  Dispose(PInfo, Done);
end;

procedure TSGroup.InsertItemDraw
  (
  AItem: PView;
  PInfo: PPaint
  );
begin
  InsertItem(AItem);
  AItem^.Draw(PInfo, FALSE);
  SetLastChange;
end;

procedure TSGroup.Draw
  (
  PInfo: PPaint;
  Clipping: Boolean
  );
var
  OldFillType: Integer;
  OldDrawPoints: Boolean;
begin
    // switch off symbol-filling for symbols
  OldFillType := PInfo^.ActSymbolFill.FillType;
  PInfo^.ActSymbolFill.FillType := symfNone;
  OldDrawPoints := PInfo^.ExtCanvas.DrawVertices;
  PInfo^.ExtCanvas.DrawVertices := False;
  inherited Draw(PInfo, Clipping);
  PInfo^.ActSymbolFill.FillType := OldFillType;
  PInfo^.ExtCanvas.DrawVertices := OldDrawPoints;
end;

function TSGroup.Visible
  (
  PInfo: PPaint
  )
  : Boolean;
begin
  Visible := TRUE;
end;

function TSGroup.GetName: string;
var
  Text: string;
begin
  if Name <> nil then
  begin
    GetName := Name^; { Name zur�ckgeben..}
  end
  else
  begin
    Str(Index, Text); { altes Symbol hat Nummer als Name }
    GetName := Text;
  end;
end;

procedure TSGroup.SetName { setzt neuen Namen f�r das Symbol }
  (
  NewName: string
  );
begin
  DisposeStr(Name); { bei nil passiert nichts }
  Name := NewStr(NewName); { Kopie anlegen }
end;

procedure TSGroup.SetLibName { setzt neuen LibName f�r das Symbol }
  (
  NewName: string
  );
begin
  DisposeStr(LibName); { bei nil passiert nichts }
  LibName := NewStr(NewName); { Kopie anlegen }
end;

function TSGroup.GetLibName: string;
begin
  if (LibName <> nil) then
    GetLibName := LibName^
  else
    GetLibName := '';
end;

function TSGroup.GetHashIndex: LongInt; { Erzeugt aus Symbolnamen LongInt-Wert }
begin
  GetHashIndex := String2HashIndex(GetName + Lowercase(ExtractFileName(GetLibName)));
end;

function TSgroup.IsNewSymbol: Boolean;
begin
  IsNewSymbol := Name <> nil;
end;

procedure TSGroup.SetLastChange;
begin
  LastChange := Time + Date; { Datum ist Vor-, Zeit Nachkommateil }
end;

destructor TSGroup.Done;
begin
  inherited Done;
  DisposeStr(Name);
  DisposeStr(LibName);
end;

procedure TSGroup.ReverseItemOrder;
var
  Cnt: Integer;
begin
  for Cnt := 0 to Data^.Count - 1 do
  begin
    Data^.AtInsert(Cnt, Data^.At(Data^.Count - 1));
    Data^.AtDelete(Data^.Count - 1);
  end;
end;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}
{***************************TChartKind*****************************************}

constructor TChartKind.Init;
begin
  inherited Init;
  DiagType := dtCircleDiag;
  TemplateChart := nil;
  TemplateChartSeries := nil;
  HashIndex := 0;
  UseCount := 0;
end;

constructor TChartKind.InitKind(ADiagType: Byte; Width: Integer; Height: Integer;
  View3D: Boolean; Transparent: Boolean;
  BevelInner: TPanelBevel; BevelOuter: TPanelBevel; BevelWidth: Integer;
  BorderStyle: TBorderStyle; BorderWidth: Integer;
  MarginLeft: Integer; MarginTop: Integer; MarginRight: Integer; MarginBottom: Integer;
  BackWallTransparent: Boolean; LegVisible: Boolean; AxisVisible: Boolean;
  AxisFontName: string; AxisFontCharset: TFontCharset; AxisFontHeight: Integer;
  LeftAxisVisible: Boolean; LeftAxisGridVisible: Boolean;
  LeftAxisTicksVisible: Boolean; LeftAxisMinorTicksVisible: Boolean;
  LeftAxisLabels: Boolean; LeftAxisLabelsSeparation: Integer;
  LeftALabStyle: TAxisLabelStyle; LeftALabAngle: Integer; LeftALabSize: Integer;
  BottomAxisVisible: Boolean; BottomAxisGridVisible: Boolean;
  BottomAxisTicksVisible: Boolean; BottomAxisMinorTicksVisible: Boolean;
  BottomAxisLabels: Boolean; BottomAxisLabelsSeparation: Integer;
  BottomALabStyle: TAxisLabelStyle; BottomALabAngle: Integer; BottomALabSize: Integer;
  MarksVisible: Boolean; MarksStyle: TSeriesMarksStyle;
  MarksFontName: string; MarksFontCharset: TFontCharset; MarksFontHeight: Integer;
  MarksTransparent: Boolean; MarksColor: TColor;
  MarksFrameVisible: Boolean;
  MarksShadowSize: Integer; MarksShadowColor: TColor);
var
  boolDoIt: Boolean;
begin
  TChartKind.Init;
  bProp := False;
  DiagType := ADiagType;
  TemplateChart := TChart.Create(WinGISMainForm.ActualChild);
  TemplateChart.Parent := WinGISMainForm.ActualChild;
  TemplateChart.Top := -Height;
  TemplateChart.Left := -Width;
  TemplateChart.Height := Height;
  TemplateChart.Width := Width;
  TemplateChart.View3D := View3D;
  TemplateChart.Color := clWhite;
  TemplateChart.ClipPoints := False;
  TemplateChart.BackWall.Transparent := Transparent;
  if Transparent then
  begin
    TemplateChart.BackImageInside := FALSE;
    TemplateChart.BackImageMode := pbmStretch;
  end;
  TemplateChart.BackWall.Visible := False;
  TemplateChart.BottomWall.Visible := False;
  TemplateChart.LeftWall.Visible := False;
  TemplateChart.RightWall.Visible := False;

  TemplateChart.BackWall.Transparent := BackWallTransparent;
  TemplateChart.BevelInner := BevelInner;
  TemplateChart.BevelOuter := BevelOuter;
  TemplateChart.BevelWidth := BevelWidth;
  TemplateChart.BorderStyle := BorderStyle;
  TemplateChart.BorderWidth := BorderWidth;
  TemplateChart.MarginLeft := MarginLeft;
  TemplateChart.MarginTop := MarginTop;
  TemplateChart.MarginRight := MarginRight;
  TemplateChart.MarginBottom := MarginBottom;
  TemplateChart.Legend.Visible := LegVisible;

  TemplateChart.AxisVisible := AxisVisible;
  boolDoIt := FALSE;
  if AxisVisible then
  begin
    TemplateChart.LeftAxis.Visible := LeftAxisVisible;
    if LeftAxisVisible then
    begin
      TemplateChart.LeftAxis.Labels := LeftAxisLabels;
      if LeftAxisLabels then
      begin
        TemplateChart.LeftAxis.LabelStyle := LeftALabStyle;
        TemplateChart.LeftAxis.LabelsAngle := LeftALabAngle;
        TemplateChart.LeftAxis.LabelsSize := LeftALabSize;
        if AxisFontName <> '' then
        begin
          TemplateChart.LeftAxis.LabelsFont.Name := AxisFontName;
          TemplateChart.LeftAxis.LabelsFont.Charset := AxisFontCharset;
          TemplateChart.LeftAxis.LabelsFont.Height := AxisFontHeight;
        end;
        TemplateChart.LeftAxis.LabelsSeparation := LeftAxisLabelsSeparation;
        TemplateChart.LeftAxis.Ticks.Visible := LeftAxisTicksVisible;
        TemplateChart.LeftAxis.MinorTicks.Visible := LeftAxisMinorTicksVisible;
        TemplateChart.LeftAxis.Grid.Visible := LeftAxisGridVisible;
        if not boolDoIt then
          boolDoIt := LeftALabStyle = talMark;
      end;
    end;
    TemplateChart.BottomAxis.Visible := BottomAxisVisible;
    if BottomAxisVisible then
    begin
      TemplateChart.BottomAxis.Labels := BottomAxisLabels;
      if BottomAxisLabels then
      begin
        TemplateChart.BottomAxis.LabelStyle := BottomALabStyle;
        TemplateChart.BottomAxis.LabelsAngle := BottomALabAngle;
        TemplateChart.BottomAxis.LabelsSize := BottomALabSize;
        if AxisFontName <> '' then
        begin
          TemplateChart.BottomAxis.LabelsFont.Name := AxisFontName;
          TemplateChart.BottomAxis.LabelsFont.Charset := AxisFontCharset;
          TemplateChart.BottomAxis.LabelsFont.Height := AxisFontHeight;
        end;
        TemplateChart.BottomAxis.LabelsSeparation := BottomAxisLabelsSeparation;
        TemplateChart.BottomAxis.Ticks.Visible := BottomAxisTicksVisible;
        TemplateChart.BottomAxis.MinorTicks.Visible := BottomAxisMinorTicksVisible;
        TemplateChart.BottomAxis.Grid.Visible := BottomAxisGridVisible;
        if not boolDoIt then
          boolDoIt := BottomALabStyle = talMark;
      end;
    end;
  end;

  case DiagType of
    dtCircleDiag: TemplateChartSeries := TPieSeries.Create(TemplateChart);
    dtBeamDiag: TemplateChartSeries := TBarSeries.Create(TemplateChart);
    dtLineDiag: TemplateChartSeries := TLineSeries.Create(TemplateChart);
    dtAreaDiag: TemplateChartSeries := TAreaSeries.Create(TemplateChart);
    dtPointDiag: TemplateChartSeries := TPointSeries.Create(TemplateChart);
  else
    begin
      DiagType := dtCircleDiag;
      TemplateChartSeries := TPieSeries.Create(TemplateChart);
    end;
  end; // End of CASE-statement

  if DiagType <> dtCircleDiag then
  begin
    TemplateChart.View3DOptions.Orthogonal := (TemplateChart.View3D);
  end;

  TemplateChartSeries.ParentChart := TemplateChart;
  TemplateChartSeries.Clear;

  TemplateChartSeries.Marks.Visible := MarksVisible;
  boolDoIt := MarksVisible or boolDoIt;
  if boolDoIt then
    TemplateChartSeries.Marks.Style := MarksStyle;
  if MarksVisible then
  begin
    if MarksFontName <> '' then
    begin
      TemplateChartSeries.Marks.Font.Name := MarksFontName;
      TemplateChartSeries.Marks.Font.Charset := MarksFontCharset;
      TemplateChartSeries.Marks.Font.Size := MarksFontHeight;

               //TemplateChartSeries.Marks.Font.Height:=-Trunc(TemplateChartSeries.Marks.Font.Size * TemplateChartSeries.Marks.Font.PixelsPerInch / 72);

               {
               if DiagType = dtCircleDiag then begin
                  TemplateChartSeries.Marks.Font.Height:=-13;
               end
               else begin
                  TemplateChartSeries.Marks.Font.Height:=-40;
               end;
               }

    end;
    TemplateChartSeries.Marks.Transparent := MarksTransparent;
    if not MarksTransparent then
    begin
      TemplateChartSeries.Marks.Color := MarksColor;
    end;
    TemplateChartSeries.Marks.Frame.Visible := MarksFrameVisible;
    TemplateChartSeries.Marks.ShadowSize := MarksShadowSize;
    TemplateChartSeries.Marks.ShadowColor := MarksShadowColor;
    TemplateChartSeries.Marks.MultiLine := False;
    TemplateChartSeries.Marks.ArrowLength := 0;
    TemplateChartSeries.Marks.Arrow.Visible := False;
    TemplateChartSeries.Marks.Bevel := bvNone;
  end;
  if DiagType = dtCircleDiag then
  begin
    TPieSeries(TemplateChartSeries).Circled := True;
    TPieSeries(TemplateChartSeries).RotationAngle := 90;
  end;
  if DiagType = dtBeamDiag then
  begin
    TBarSeries(TemplateChartSeries).SideMargins := False;
  end;
end;

constructor TChartKind.InitByExistChart(ExistChart: TChart; ExistSeries: TChartSeries;
  RelAbsSize: Boolean; Sum: Double);
var
  dblRatio: Double;
  THeight: Integer;
  Width: Double;
  Height: Double;
  AspectRatio: Double;
begin
  TChartKind.Init;
  TemplateChart := TChart.Create(WinGISMainForm.ActualChild);
  TemplateChart.Assign(ExistChart);
  TemplateChart.Parent := WinGISMainForm.ActualChild;

   // for the most chart series types
  AspectRatio := ExistChart.Width / ExistChart.Height;
  Width := co_BaseChartSize * AspectRatio;
  Height := co_BaseChartSize;
  dblRatio := co_BaseChartSize / ExistChart.Height;

  if ExistSeries is TPieSeries then
  begin
    DiagType := dtCircleDiag;
    TemplateChartSeries := TPieSeries.Create(TemplateChart);
    TPieSeries(TemplateChartSeries).Assign(ExistSeries);
    TPieSeries(TemplateChartSeries).Circled := True;
         {if RelAbsSize then
            begin
               dblRatio:=(100*co_RelChartPercent)/Sum;
               Width:=ExistChart.Width+(TPieSeries(ExistSeries).CustomXRadius*dblRatio-TPieSeries(ExistSeries).CustomXRadius)*2;
               Height:=ExistChart.Height+(TPieSeries(ExistSeries).CustomYRadius*dblRatio-TPieSeries(ExistSeries).CustomYRadius)*2;
               dblRatio:=co_HalfChartSize/(TPieSeries(ExistSeries).CustomXRadius*dblRatio);
               Width:=Width*dblRatio;
               Height:=Height*dblRatio;
            end
         else
            begin
               dblRatio:=co_HalfChartSize/TPieSeries(ExistSeries).CustomXRadius;
               Width:=ExistChart.Width*dblRatio;
               Height:=ExistChart.Height*dblRatio;
            end;
         AspectRatio:=TPieSeries(ExistSeries).CustomYRadius/TPieSeries(ExistSeries).CustomXRadius;
         TPieSeries(TemplateChartSeries).CustomXRadius:=co_HalfChartSize;
         TPieSeries(TemplateChartSeries).CustomYRadius:=Round(TPieSeries(TemplateChartSeries).CustomXRadius*AspectRatio);
      }
  end
  else
    if ExistSeries is TBarSeries then
    begin
      DiagType := dtBeamDiag;
      TemplateChartSeries := TBarSeries.Create(TemplateChart);
      TBarSeries(TemplateChartSeries).Assign(ExistSeries);
    end
    else
      if ExistSeries is TLineSeries then
      begin
        DiagType := dtLineDiag;
        TemplateChartSeries := TLineSeries.Create(TemplateChart);
        TLineSeries(TemplateChartSeries).Assign(ExistSeries);
      end
      else
        if ExistSeries is TAreaSeries then
        begin
          DiagType := dtAreaDiag;
          TemplateChartSeries := TAreaSeries.Create(TemplateChart);
          TAreaSeries(TemplateChartSeries).Assign(ExistSeries);
        end
        else
          if ExistSeries is TPointSeries then
          begin
            DiagType := dtPointDiag;
            TemplateChartSeries := TPointSeries.Create(TemplateChart);
            TPointSeries(TemplateChartSeries).Assign(ExistSeries);
            TPointSeries(TemplateChartSeries).Pointer.HorizSize := Round(TPointSeries(ExistSeries).Pointer.HorizSize * dblRatio);
            TPointSeries(TemplateChartSeries).Pointer.VertSize := Round(TPointSeries(ExistSeries).Pointer.VertSize * dblRatio);
          end
          else
          begin
            TemplateChartSeries := TPieSeries.Create(TemplateChart);
          end;

  TemplateChartSeries.ParentChart := TemplateChart;
  TemplateChartSeries.Clear;

  TemplateChart.Width := Round(Width);
  TemplateChart.Height := Round(Height);
  TemplateChart.Left := -TemplateChart.Width;
  TemplateChart.Top := -TemplateChart.Height;

  if TemplateChart.AxisVisible then
  begin
    if TemplateChart.LeftAxis.Visible then
      if TemplateChart.LeftAxis.Labels then
      begin
        THeight := Round(ExistChart.LeftAxis.LabelsFont.Height * dblRatio);
        if THeight < co_TextVisible then
          TemplateChart.LeftAxis.Labels := FALSE
        else
          TemplateChart.LeftAxis.LabelsFont.Height := THeight;
        TemplateChart.LeftAxis.LabelsSize := Round(ExistChart.LeftAxis.LabelsSize * dblRatio);
      end;
    if TemplateChart.BottomAxis.Visible then
      if TemplateChart.BottomAxis.Labels then
      begin
        THeight := Round(ExistChart.BottomAxis.LabelsFont.Height * dblRatio);
        if THeight < co_TextVisible then
          TemplateChart.BottomAxis.Labels := FALSE
        else
          TemplateChart.BottomAxis.LabelsFont.Height := THeight;
        TemplateChart.BottomAxis.LabelsSize := Round(ExistChart.BottomAxis.LabelsSize * dblRatio);
      end;
  end;

  if TemplateChartSeries.Marks.Visible then
  begin
    THeight := Round(ExistSeries.Marks.Font.Height * dblRatio);
{         if THeight < co_TextVisible then
            TemplateChartSeries.Marks.Visible:=FALSE
         else}
    TemplateChartSeries.Marks.Font.Height := THeight;
  end;
end;

constructor TChartKind.Load(S: TOldStream);
var
  Version: Byte;
begin
  inherited Load(S);
  S.Read(Version, SizeOf(Version)); {  Chart Kind version }
  S.Read(DiagType, SizeOf(DiagType)); {  Chart Kind type }
// TODO
//  TemplateChart := TChart.Load(S, WinGISMainForm.ActualChild);

// TODO
{  case DiagType of
    dtCircleDiag: TemplateChartSeries := TPieSeries.Load(S, TemplateChart);
    dtBeamDiag: TemplateChartSeries := TBarSeries.Load(S, TemplateChart);
    dtLineDiag: TemplateChartSeries := TLineSeries.Load(S, TemplateChart);
    dtAreaDiag: TemplateChartSeries := TAreaSeries.Load(S, TemplateChart);
    dtPointDiag: TemplateChartSeries := TPointSeries.Load(S, TemplateChart);
  else
    begin
      DiagType := dtCircleDiag;
      TemplateChartSeries := TPieSeries.Load(S, TemplateChart);
    end;
  end;   }
  TemplateChartSeries.ParentChart := TemplateChart;
  TemplateChartSeries.Clear;
end;

procedure TChartKind.Store(S: TOldStream);
var
  Version: Byte;
begin
  inherited Store(S);
  Version := ChartKindVersion;
  S.Write(Version, SizeOf(Version));
  S.Write(DiagType, SizeOf(DiagType));
// TODO
{  TemplateChart.Store(S);

  case DiagType of
    dtCircleDiag: TPieSeries(TemplateChartSeries).Store(S);
    dtBeamDiag: TBarSeries(TemplateChartSeries).Store(S);
    dtLineDiag: TLineSeries(TemplateChartSeries).Store(S);
    dtAreaDiag: TAreaSeries(TemplateChartSeries).Store(S);
    dtPointDiag: TPointSeries(TemplateChartSeries).Store(S);
  else
    begin
      DiagType := dtCircleDiag;
      TPieSeries(TemplateChartSeries).Store(S);
    end;
  end;  }
end;

destructor TChartKind.Done;
begin
  if TemplateChart <> nil then
  begin
    TemplateChart.FreeAllSeries;
    TemplateChart.Free;
  end;
  inherited Done;
end;

function TChartKind.GetObjType: Word;
begin
  GetObjType := ot_ChartKind;
end;

function TChartKind.GetHashIndex: LongInt;
var
  Determinant: string;
  Cnt: Integer;
begin
  SetLength(Determinant, SizeOf(DiagType));
  CopyMemory(@Determinant[1], @DiagType, SizeOf(DiagType));
  // TODO
{  Determinant := TemplateChart.CreateDeterminant(Determinant);
  case DiagType of
    dtCircleDiag: Determinant := TPieSeries(TemplateChartSeries).CreateDeterminant(Determinant);
    dtBeamDiag: Determinant := TBarSeries(TemplateChartSeries).CreateDeterminant(Determinant);
    dtLineDiag: Determinant := TLineSeries(TemplateChartSeries).CreateDeterminant(Determinant);
    dtAreaDiag: Determinant := TAreaSeries(TemplateChartSeries).CreateDeterminant(Determinant);
    dtPointDiag: Determinant := TPointSeries(TemplateChartSeries).CreateDeterminant(Determinant);
  else
  end;  }

  if Length(Determinant) = Length(OldDeterminant) then
  begin
    for Cnt := 0 to Length(Determinant) - 1 do
      if Determinant[Cnt] <> OldDeterminant[Cnt] then
      begin
        OldDeterminant := Determinant;
        Break;
      end;
  end
  else
    OldDeterminant := Determinant;

  HashIndex := String2HashIndex(Determinant);
  Result := HashIndex;
end;

function TChartKind.OnCompareItems(Item: PChartKind): Boolean;
begin
  Result := FALSE;
// TODO
{  with TChartKind(Item^) do
    if DiagType = Self.DiagType then
      if Self.TemplateChart.OnCompareItems(TemplateChart) then
        case Self.DiagType of
          dtCircleDiag: Result := TPieSeries(Self.TemplateChartSeries).OnCompareItems(TPieSeries(TemplateChartSeries));
          dtBeamDiag: Result := TBarSeries(Self.TemplateChartSeries).OnCompareItems(TBarSeries(TemplateChartSeries));
          dtLineDiag: Result := TLineSeries(Self.TemplateChartSeries).OnCompareItems(TLineSeries(TemplateChartSeries));
          dtAreaDiag: Result := TAreaSeries(Self.TemplateChartSeries).OnCompareItems(TAreaSeries(TemplateChartSeries));
          dtPointDiag: Result := TPointSeries(Self.TemplateChartSeries).OnCompareItems(TPointSeries(TemplateChartSeries));
        else
        end; }// End of CASE-statement
end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

{******************************************************************************}
{ Registrierung der Stream-Typen                                               }
{******************************************************************************}

const
  RGroup: TStreamRec = (
    ObjType: rn_Group;
    VmtLink: TypeOf(TGroup);
    Load: @TGroup.Load;
    Store: @TGroup.Store);

  RSGroup: TStreamRec = (
    ObjType: rn_SGroup;
    VmtLink: TypeOf(TSGroup);
    Load: @TSGroup.Load;
    Store: @TSGroup.Store);

  RSGroupOld: TStreamRec = (
    ObjType: rn_SGroupOld;
    VmtLink: TypeOf(TSGroup);
    Load: @TSGroup.LoadOld;
    Store: @TSGroup.Store);

{++ Moskaliov Business Graphics BUILD#150 17.01.01}
  RChartKind: TStreamRec = (
    ObjType: rn_ChartKind;
    VmtLink: TypeOf(TChartKind);
    Load: @TChartKind.Load;
    Store: @TChartKind.Store);
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

begin
  RegisterType(RGroup);
  RegisterType(RSGroupOld);
  RegisterType(RSGroup);
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
  RegisterType(RChartKind);
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
end.

