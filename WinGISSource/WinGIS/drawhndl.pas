Unit DrawHndl;

Interface

Uses Classes,Controls,MenuHndl,MenuFn,ProjHndl;

Type TDrawRectangleHandler   = Class(TProjMenuHandler)
      Protected
       Procedure   DoFinishedInput(Sender:TObject);
       Procedure   DoStartInput(Sender:TObject);
      Public
       Class Function HandlerType:Word; override;
       Procedure   OnStart; override;
     end;

     TDrawEllipseHandler      = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;

     TDrawConstructHandler    = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;

     TDrawEllipseArcHandler   = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;                                      

     TCustomDrawEllipseHandler = Class(TProjMenuHandler)
      Protected
       Procedure   DoFinishedInput(Sender:TObject);
      Public
       Class Function HandlerType:Word; override;
       Procedure   OnActivate; override;
       Procedure   OnDeActivate; override;
       Procedure   OnKeyDown(var Key:Word;Shift:TShiftState); override;
       Procedure   OnStart; override;
     end;

     TCustomDrawArcHandler = Class(TProjMenuHandler)
      Protected
       Procedure   DoFinishedInput(Sender:TObject);
      Public
       Class Function HandlerType:Word; override;
       Procedure   OnActivate; override;
       Procedure   OnDeActivate; override;
       Procedure   OnKeyDown(var Key:Word;Shift:TShiftState); override;
       Procedure   OnStart; override;
     end;

     TDrawCircle2PointsHandler = Class(TCustomDrawEllipseHandler)
       Procedure   OnStart; override;
     end;

     TDrawCircleCenterRadiusHandler = Class(TCustomDrawEllipseHandler)
       Procedure   OnStart; override;
     end;

     TDrawCircle3PointsHandler = Class(TCustomDrawEllipseHandler)
       Procedure   OnStart; override;
     end;

     TDrawCircle2PointsRadiusHandler = Class(TCustomDrawEllipseHandler)
       Procedure   OnStart; override;
     end;

     TDrawCircleInRectangleHandler = Class(TCustomDrawEllipseHandler)
       Procedure   OnStart; override;
     end;

     TDrawEllipseCenterAxesHandler = Class(TCustomDrawEllipseHandler)
       Procedure   OnStart; override;
     end;

     TDrawEllipseAxesHandler = Class(TCustomDrawEllipseHandler)
       Procedure   OnStart; override;
     end;

     TDrawEllipseInRectangleHandler = Class(TCustomDrawEllipseHandler)
       Procedure   OnStart; override;
     end;

     TDrawArcCenterHandler = Class(TCustomDrawArcHandler)
       Procedure   OnStart; override;
     end;

     TDrawArc3PointsHandler = Class(TCustomDrawArcHandler)
       Procedure   OnStart; override;
     end;

     TDrawArc2PointsRadiusHandler = Class(TCustomDrawArcHandler)
       Procedure   OnStart; override;
     end;

     TDrawArcInRectangleHandler =  Class(TCustomDrawArcHandler)
       Procedure   OnStart; override;
     end;

     TDrawEllipseArcCenterHandler =  Class(TCustomDrawArcHandler)
       Procedure   OnStart; override;
     end;

     TDrawEllipseArcAxesHandler =  Class(TCustomDrawArcHandler)
       Procedure   OnStart; override;
     end;

     TDrawEllipseArcInRectangleHandler =  Class(TCustomDrawArcHandler)
       Procedure   OnStart; override;
     end;

Implementation

{$R *.mfn}

Uses Forms,AM_Circl,AM_Def,Dialogs,InpHndl,MultiLng,RegDB,UserIntf,Windows,AM_ProjO,AM_DDE;

{===============================================================================
| TDrawRectangleHandler
+==============================================================================}

Procedure TDrawRectangleHandler.OnStart;
  begin
    SubHandler:=TRubberBox.Create(Self);
    with SubHandler as TRubberBox do begin
      TwoClicks:=TRUE;
      OnStartInput:=DoStartInput;
      OnFinishedInput:=DoFinishedInput;
    end;
    Status:=mhsNotFinished;
    MessageDialog('Sorry, not yet implemented.',mtInformation,[mbOK],0);
  end;

Class Function TDrawRectangleHandler.HandlerType
   : Word;
  begin
    Result:=mhtReturnAndReset;
  end;

Procedure TDrawRectangleHandler.DoStartInput(Sender:TObject);
begin
  with Project^, SubHandler as TRubberBox do begin
    ViewRotation:=PInfo^.ViewRotation;
    MinSize:=Project^.PInfo^.CalculateDraw(10);
  end;
end;

Procedure TDrawRectangleHandler.DoFinishedInput(Sender:TObject);
begin
end;

{==============================================================================+
  TDrawEllipseHandler
+==============================================================================}

{******************************************************************************+
  Procedure TDrawEllipseHandler.OnStart
--------------------------------------------------------------------------------
  Called if the draw-ellipses-menu was selected. The context-specific toolbar
  is blened in by the menu-system. Reads the name of the sub-function from
  the projects-registry and is activated. 
+******************************************************************************}
Procedure TDrawEllipseHandler.OnStart;
var CurrentFn      : String;
begin
  with Project^ do begin
    CurrentFn:=Registry.ReadString('\User\WinGISUI\DrawingTools\Ellipses\InputType');
    if Registry.LastError<>rdbeNoError then CurrentFn:='DrawCircleCenterRadius';
    MenuFunctions[CurrentFn].Execute;
  end;
end;

{==============================================================================+
  TDrawConstructHandler
+==============================================================================}

Procedure TDrawConstructHandler.OnStart;
var CurrentFn    : String;
begin                                                 
  with Project^ do begin
{      CurrentFn:=Registry.ReadString('\User\WinGISUI\DrawingTools\Construct\InputType');
    if Registry.LastError<>rdbeNoError then CurrentFn:='DrawCircleCenterRadius';
    MenuFunctions[CurrentFn].Execute;}
  end;
end;

{===============================================================================
| TDrawEllipseArcHandler
+==============================================================================}

Procedure TDrawEllipseArcHandler.OnStart;
  var CurrentFn    : String;
  begin
    with Project^ do begin
      CurrentFn:=Registry.ReadString('\User\WinGISUI\DrawingTools\EllipseArcs\InputType');
      if Registry.LastError<>rdbeNoError then CurrentFn:='DrawArcCenter';
      MenuFunctions[CurrentFn].Execute;
    end;
  end;

{==============================================================================+
  TCustomDrawEllipseHandler
+==============================================================================}

Class Function TCustomDrawEllipseHandler.HandlerType:Word;
begin
  Result:=mhtReturnAndReset;
end;

Procedure TCustomDrawEllipseHandler.OnStart;
begin
  SubHandler:=TRubberEllipse.Create(Self);
  with Project^ do begin
    SetCursor(idc_DrawCircle);
    with SubHandler as TRubberEllipse do begin
      ViewRotation:=PInfo^.ViewRotation;
      OnFinishedInput:=DoFinishedInput;
      Cursor:=idc_DrawCircle;
      {$IFNDEF AXDLL} // <----------------- AXDLL
      {+++ Brovak BUG 524,539 BUILD 167}
      FAllowDblclick:=true;
      Project^.ClickCnt1:=0;
      {-- Brovak }
      {$ENDIF} // <----------------- AXDLL
    end;
    Registry.WriteString('\User\WinGISUI\DrawingTools\Ellipses\InputType',MenuName);
  end;
  Status:=mhsNotFinished;
  CoordinateInput:=TRUE;
end;

Procedure TCustomDrawEllipseHandler.DoFinishedInput
   (
   Sender          : TObject
   );
  var AEllipse     : PEllipse;
      ACenter      : TDPoint;
  begin
    with SubHandler as TRubberEllipse do begin
      ACenter.Init(LimitToLong(Center.X),LimitToLong(Center.Y));
      AEllipse:=New(PEllipse,Init(ACenter,LimitToLong(PrimaryAxis),LimitToLong(SecondaryAxis),0.0));
      if Project^.InsertObject(AEllipse,FALSE) then begin
         {$IFNDEF WMLT}
         {$IFNDEF AXDLL} // <----------------- AXDLL
         if DDEHandler.DoAutoIns then begin
            ProcDBSendObject(Project,AEllipse,'EIN');
         end;
         {$ENDIF}         // <----------------- AXDLL
         {$ENDIF}
      end;
      Self.StatusText := ''; {++ Sygsky 24-SEP-1999 BUG#188 BUILD#93 }
    end;
  end;

Procedure TCustomDrawEllipseHandler.OnActivate;
begin
  MenuFunctions['DrawEllipses'].Checked:=TRUE;
end;

Procedure TCustomDrawEllipseHandler.OnDeActivate;
begin
  MenuFunctions['DrawEllipses'].Checked:=FALSE;
end;

Procedure TCustomDrawEllipseHandler.OnKeyDown(var Key:Word;Shift:TShiftState);
begin
  if Key=vk_Escape then OnUndo(0);
end;

{==============================================================================+
  TDrawCircleCenterRadiusHandler
+==============================================================================}

Procedure TDrawCircleCenterRadiusHandler.OnStart;
  begin
    inherited OnStart;
    with SubHandler as TRubberEllipse do InputType:=retCircleCenterRadius;
  end;

{==============================================================================+
  TDrawCircle2PointsHandler
+==============================================================================}

Procedure TDrawCircle2PointsHandler.OnStart;
begin
  inherited OnStart;
  with SubHandler as TRubberEllipse do InputType:=retCircle2Points;
end;

{==============================================================================+
  TDrawCircle3PointsHandler
+==============================================================================}

Procedure TDrawCircle3PointsHandler.OnStart;
begin
  inherited OnStart;
  with SubHandler as TRubberEllipse do InputType:=retCircle3Points;
end;

{==============================================================================+
  TDrawCircle2PointsRadiusHandlerHandler
+==============================================================================}

Procedure TDrawCircle2PointsRadiusHandler.OnStart;
begin
  inherited OnStart;
  with SubHandler as TRubberEllipse do InputType:=retCircle2PointsCenter;
end;

{==============================================================================+
  TDrawCircleInRectangleHandler
+==============================================================================}

Procedure TDrawCircleInRectangleHandler.OnStart;
begin
  inherited OnStart;
  with SubHandler as TRubberEllipse do InputType:=retCircleInRectangle;
end;

{==============================================================================+
  TDrawEllipseCenterAxesHandler
+==============================================================================}

Procedure TDrawEllipseCenterAxesHandler.OnStart;
begin
  inherited OnStart;
  with SubHandler as TRubberEllipse do InputType:=retCircleInRectangle;
  MessageDialog('Sorry, not yet implemented.',mtInformation,[mbOK],0);
end;

{==============================================================================+
  TDrawEllipseAxesHandler
+==============================================================================}

Procedure TDrawEllipseAxesHandler.OnStart;
begin
  inherited OnStart;
  with SubHandler as TRubberEllipse do InputType:=retCircleInRectangle;
  MessageDialog('Sorry, not yet implemented.',mtInformation,[mbOK],0);
end;

{==============================================================================+
  TDrawEllipseInRectangleHandler
+==============================================================================}

Procedure TDrawEllipseInRectangleHandler.OnStart;
begin
  inherited OnStart;
  with SubHandler as TRubberEllipse do InputType:=retCircleInRectangle;
  MessageDialog('Sorry, not yet implemented.',mtInformation,[mbOK],0);
end;

{==============================================================================+
  TCustomDrawArcHandler
+==============================================================================}

Class Function TCustomDrawArcHandler.HandlerType:Word;
begin
  Result:=mhtReturnAndReset;
end;

Procedure TCustomDrawArcHandler.OnStart;
begin
  SubHandler:=TRubberArc.Create(Self);
  with Project^ do begin
    SetCursor(idc_DrawArc);
    with SubHandler as TRubberArc do begin
      ViewRotation:=PInfo^.ViewRotation;
      OnFinishedInput:=DoFinishedInput;
      Cursor:=idc_DrawArc;
      {$IFNDEF AXDLL} // <----------------- AXDLL
      {+++ Brovak BUG 524,539 BUILD 167}
      FAllowDblclick:=true;
      Project^.ClickCnt1:=0;
       {-- Brovak }
      {$ENDIF} // <----------------- AXDLL
    end;
    Registry.WriteString('\User\WinGISUI\DrawingTools\EllipseArcs\InputType',MenuName);
  end;
  Status:=mhsNotFinished;
  CoordinateInput:=TRUE;
end;

Procedure TCustomDrawArcHandler.DoFinishedInput(Sender:TObject);
var AEllipse     : PEllipseArc;
    ACenter      : TDPoint;
begin
  with SubHandler as TRubberArc do begin
    ACenter.Init(LimitToLong(Center.X),LimitToLong(Center.Y));
    AEllipse:=New(PEllipseArc,Init(ACenter,LimitToLong(PrimaryAxis),LimitToLong(SecondaryAxis),
        0.0,BeginAngle,EndAngle));
    if Project^.InsertObject(AEllipse,FALSE) then begin
       {$IFNDEF WMLT}
       {$IFNDEF AXDLL} // <----------------- AXDLL
       if DDEHandler.DoAutoIns then begin
          ProcDBSendObject(Project,AEllipse,'EIN');
       end;
       {$ENDIF}         // <----------------- AXDLL
       {$ENDIF}
    end;
  end;
end;

Procedure TCustomDrawArcHandler.OnActivate;
begin
  MenuFunctions['DrawEllipseArcs'].Checked:=TRUE;
end;

Procedure TCustomDrawArcHandler.OnDeActivate;
begin
  MenuFunctions['DrawEllipseArcs'].Checked:=FALSE;
end;

Procedure TCustomDrawArcHandler.OnKeyDown(var Key:Word;Shift:TShiftState);
begin
  if Key=vk_Escape then OnUndo(0);
end;

{==============================================================================+
  TDrawArcCenterHandler
+==============================================================================}

Procedure TDrawArcCenterHandler.OnStart;
begin
  inherited OnStart;
  with SubHandler as TRubberArc do InputType:=ratArcCenter;
end;

{==============================================================================+
  TDrawArc3PointsHandler
+==============================================================================}

Procedure TDrawArc3PointsHandler.OnStart;
begin
  inherited OnStart;
  with SubHandler as TRubberArc do InputType:=ratArc3Points;
end;

{==============================================================================+
  TDrawArc2PointsRadiusHandler
+==============================================================================}

Procedure TDrawArc2PointsRadiusHandler.OnStart;
begin
  inherited OnStart;
  with SubHandler as TRubberArc do InputType:=ratArc2PointsCenter;
end;

{==============================================================================+
  TDrawArcInRectangleHandler
+==============================================================================}

Procedure TDrawArcInRectangleHandler.OnStart;
begin
  inherited OnStart;
  with SubHandler as TRubberArc do InputType:=ratArcInRectangle;
end;

{==============================================================================+
  TDrawEllipseArcCenterHandler
+==============================================================================}

Procedure TDrawEllipseArcCenterHandler.OnStart;
begin
  inherited OnStart;
  with SubHandler as TRubberArc do InputType:=ratEllipseArcCenter;
  MessageDialog('Sorry, not yet implemented.',mtInformation,[mbOK],0);
end;

{==============================================================================+
  TDrawEllipseArcAxesHandler
+==============================================================================}

Procedure TDrawEllipseArcAxesHandler.OnStart;
begin
  inherited OnStart;
  with SubHandler as TRubberArc do InputType:=ratEllipseArcAxes;
  MessageDialog('Sorry, not yet implemented.',mtInformation,[mbOK],0);
end;

{==============================================================================+
  TDrawEllipseArcInRectangleHandler
+==============================================================================}

Procedure TDrawEllipseArcInRectangleHandler.OnStart;
begin
  inherited OnStart;
  with SubHandler as TRubberArc do InputType:=ratEllipseArcInRectangle;
  MessageDialog('Sorry, not yet implemented.',mtInformation,[mbOK],0);
end;

{==============================================================================+
  Initialisierungs- und Terminierungscode
+==============================================================================}

Initialization;
  MenuFunctions.RegisterFromResource(HInstance,'DrawHndl','DrawHndl');
  TDrawRectangleHandler.Registrate('DrawRectangle');
  TDrawEllipseHandler.Registrate('DrawEllipses');
  TDrawCircle2PointsHandler.Registrate('DrawCircle2Points');
  TDrawCircle3PointsHandler.Registrate('DrawCircle3Points');
  TDrawCircle2PointsRadiusHandler.Registrate('DrawCircle2PointsRadius');
  TDrawCircleCenterRadiusHandler.Registrate('DrawCircleCenterRadius');
  TDrawCircleInRectangleHandler.Registrate('DrawCircleInRectangle');
  TDrawEllipseCenterAxesHandler.Registrate('DrawEllipseCenterAxes');
  TDrawEllipseAxesHandler.Registrate('DrawEllipseAxes');
  TDrawEllipseInRectangleHandler.Registrate('DrawEllipseInRectangle');
  TDrawEllipseArcHandler.Registrate('DrawEllipseArcs');
  TDrawConstructHandler.Registrate('DrawConstruct');
  TDrawArcCenterHandler.Registrate('DrawArcCenter');
  TDrawArc3PointsHandler.Registrate('DrawArc3Points');
  TDrawArc2PointsRadiusHandler.Registrate('DrawArc2PointsRadius');
  TDrawArcInRectangleHandler.Registrate('DrawArcInRectangle');
  TDrawEllipseArcCenterHandler.Registrate('DrawEllipseArcCenter');
  TDrawEllipseArcAxesHandler.Registrate('DrawEllipseArcAxes');
  TDrawEllipseArcInRectangleHandler.Registrate('DrawEllipseArcInRectangle');

end.
