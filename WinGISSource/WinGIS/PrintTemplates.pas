Unit PrintTemplates;

Interface

Uses Windows,ActiveX,Classes,Forms,Graphics,GrTools,Img_Info,OLECtnrs2,PageSetup,
     RegDB,Scroller,SysUtils,Legend,AM_Proj,StyleDef,AM_Paint,ExtCanvas,NumTools,AM_Def,Controls;

Const // current file-format version
//++ Glukhov Bug#293(PrintFrame) Build#175 15.02.02
      FileMainVersionMoskalev = 3;
      FileMainVersionFrames   = 4;
      FileMainVersion  = FileMainVersionFrames;
//-- Glukhov Bug#293(PrintFrame) Build#175 15.02.02

//    FileSubVersion   = 0;
//    FileSubVersion   = 1;  {++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
      FileSubVersion   = 2;  {++ Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
//++ Glukhov Bug#293(PrintFrame) Build#175 15.02.02
// The constants to set some bit flags saved instead of boolean value fInitialized
Const
  // The original value fInitialized
  cwPrnTempInitialized  = 1;
  // The frame border constants
  cwPrnTempFrmBrdLeft   = 2;
  cwPrnTempFrmBrdTop    = 4;
  cwPrnTempFrmBrdRight  = 8;
  cwPrnTempFrmBrdBottom = 16;
  cwPrnTempFrmBrdAll    = 30;
//-- Glukhov Bug#293(PrintFrame) Build#175 15.02.02

Type TPrintPosition        = (ppCustom,ppCenter,ppCenterHoriz,ppCenterVert);
     TPrintOrigin          = (poTop,poBottom);
     TPrintLocation        = (plOneTime,plOnPages);
     TPrintingRange        = (prProject,prSavedView,prCurrentView,prWindow);
     TPrintingSize         = (psScale,psFitToPage,psWidthAndHeight,psScaleAndFit);

     TFieldCode            = (fcPage,fcPageS,fcDate,fcTime,fcFile,fcAuthor,fcScale);

Const FieldsListVersion    = 1;
      FieldsCodesVersion   = 1;
      FieldsCodes: Array[0..6] of String = ('&Page.','&Pages.','&Date.','&Time.','&File.','&Author.','&Scale.');
      PageINDEX            = 0;
      PagesINDEX           = 1;
      DateINDEX            = 2;
      TimeINDEX            = 3;
      FileINDEX            = 4;
      AuthorINDEX          = 5;
      ScaleINDEX           = 6;
      FieldsLenMIN         = 6;
      FieldsLenMAX         = 8;
      DateCODE             = $644;
      FileCODE             = $646;
      PageCODE             = $650;
      TimeCODE             = $654;
      PagesCODE            = $750;
      ScaleCODE            = $753;
      AuthorCODE           = $841;

Type TFieldCodeWord = record
     case Integer of
          0: (Code: Word);
          1: (Sym: Byte; Len: Byte);
     end;

     EPrintTemplate = Class(Exception);

     TGraphics     = Class;

     TPaintEvent   = Function(Scroller:TGraphicSite):Boolean of object;

     TCustomGraphic= Class
      Private
       FBounds          : TGrRect;
       FOriginalBounds  : TGrRect;
       FOwner           : TGraphics;
       FOnPaint         : TPaintEvent;
       FPrintPosition   : TPrintPosition;
       FPrintOrigin     : TPrintOrigin;
       FInitialised     : Boolean;
//++ Glukhov Bug#293(PrintFrame) Build#175 15.02.02
        // To keep some bit flags including the border flags (see section Const)
        FBitFlags       : Byte;
//-- Glukhov Bug#293(PrintFrame) Build#175 15.02.02
      Protected
       Procedure   SetParent(AParent:TScroller); virtual;
      Public
       Constructor Create(AOwner:TGraphics); virtual;
       Destructor  Destroy; override;
       Procedure   Align(Scroller:TGraphicSite); virtual;
       Property    BoundsRect:TGrRect read FBounds write FBounds;
{++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
       Function    GetPageBaseExtent(var BaseRect: TGrRect; var IndX: Integer; var IndY: Integer):Double;
{-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
       Procedure   Invalidate(Scroller:TScroller); virtual;
       Procedure   LoadFromStream(S:TStream;Reader:TReader;Version:Integer); virtual;
       Property    Owner:TGraphics read FOwner;
       Property    OriginalBoundsRect:TGrRect read FOriginalBounds write FOriginalBounds;
       Property    OnPaint:TPaintEvent read FOnPaint write FOnPaint;
       Procedure   Paint(Scroller:TGraphicSite); virtual;
       Property    Parent:TScroller write SetParent;
       Property    PrintPosition:TPrintPosition read FPrintPosition write FPrintPosition;
       Property    PrintOrigin:TPrintOrigin read FPrintOrigin write FPrintOrigin;
       Property    Initialised:Boolean read FInitialised;
//++ Glukhov Bug#293(PrintFrame) Build#175 15.02.02
       Property    BitFlags: Byte read FBitFlags write FBitFlags;
//-- Glukhov Bug#293(PrintFrame) Build#175 15.02.02
       Procedure   SaveToStream(S:TStream;Writer:TWriter); virtual;
       // saves the settings to the registry
       Procedure   SaveToRegistry(Registry:TCustomRegistry; const KeyName: String);
       // loads the settings from a registry
       Procedure   LoadFromRegistry(Registry:TCustomRegistry; const KeyName: String);
     end;

     TPrintSignature    = Class(TCustomGraphic)
      Private
       FSignature       : String;
       FTextStyle       : TTextProperties;
       FForeColor       : TColor;
       FBackColor       : TColor;
       FPrintLocation   : TPrintLocation;
       FOnFirstPage     : Boolean;
       FOnInternalPages : Boolean;
       FOnLastPage      : Boolean;
       Function    GetSignatureValue:String;
       Procedure   SetSignature(ASignature:String);
      Public
       Constructor Create(AOwner:TGraphics); override;
       Destructor  Destroy; override;
       Procedure   LoadFromStream(S:TStream;Reader:TReader;Version:Integer); override;
       Procedure   SaveToStream(S:TStream;Writer:TWriter); override;
       Property    Signature:String read FSignature write SetSignature;
       Property    SignatureValue:String read GetSignatureValue;
       Property    TextStyle:TTextProperties read FTextStyle write FTextStyle;
       Property    ForeColor:TColor read FForeColor write FForeColor;
       Property    BackColor:TColor read FBackColor write FBackColor;
       Property    PrintLocation:TPrintLocation read FPrintLocation write FPrintLocation;
       Property    OnFirstPage:Boolean read FOnFirstPage write FOnFirstPage;
       Property    OnInternalPages:Boolean read FOnInternalPages write FOnInternalPages;
       Property    OnLastPage:Boolean read FOnLastPage write FOnLastPage;
       Procedure   Paint(Scroller:TGraphicSite); override;
       // saves the settings to the registry
       Procedure   SaveToRegistry(Registry:TCustomRegistry;const KeyName:String);
       // loads the settings from a registry
       Procedure   LoadFromRegistry(Registry:TCustomRegistry;const KeyName:String);
       function    LineCount: Integer;
       function    GetLine(Index: Integer): String;
     end;

     TWindowsGraphic    = Class(TCustomGraphic)
      Private
       FFileName        : String;
       FLinked          : Boolean;
       FRelativePath    : String;
       FImageInfo       : TrLibInfo;
       FKeepAspect      : Boolean;
{++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
       FPrintLocation   : TPrintLocation;
       FOnFirstPage     : Boolean;
       FOnInternalPages : Boolean;
       FOnLastPage      : Boolean;
{-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
       Procedure   FitToBounds;
       Procedure   FitToPage;
       Procedure   LoadFile;
      Public
       Constructor Create(AOwner:TGraphics); override;
       Destructor  Destroy; override;
       Function    CreateFromClipboard:Boolean;
       Procedure   CreateFromFile(const AFile,ARelativePath:String;ALinked:Boolean);
       Property    FileName:String read FFileName write FFileName;
       Property    Linked:Boolean read FLinked;
       Procedure   LoadFromStream(S:TStream;Reader:TReader;Version:Integer); override;
       Property    RelativePath:String read FRelativePath write FRelativePath;
{++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
       Property    PrintLocation:TPrintLocation read FPrintLocation write FPrintLocation;
       Property    OnFirstPage:Boolean read FOnFirstPage write FOnFirstPage;
       Property    OnInternalPages:Boolean read FOnInternalPages write FOnInternalPages;
       Property    OnLastPage:Boolean read FOnLastPage write FOnLastPage;
{-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
       Procedure   Paint(Scroller:TGraphicSite); override;
       Procedure   SaveToStream(S:TStream;Writer:TWriter); override;
       Property    KeepAspect:Boolean read FKeepAspect write FKeepAspect;
       // saves the settings to the registry
       Procedure   SaveToRegistry(Registry:TCustomRegistry;const KeyName:String);
       // loads the settings from a registry
       Procedure   LoadFromRegistry(Registry:TCustomRegistry;const KeyName:String);
     end;

     TProjectPlaceholder = Class(TCustomGraphic)
      Private
       FRange           : TPrintingRange;
       FSize            : TPrintingSize;
       FScale           : Double;
       FView            : String;
{++ Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
       FPrintProjFrame  : Boolean;
{-- Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
      Public
       Constructor Create(AOwner:TGraphics); override;
{++ Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
       Procedure   DrawFrame(Scroller:TGraphicSite);
{-- Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
       Procedure   LoadFromStream(S:TStream;Reader:TReader;Version:Integer); override;
       Procedure   Paint(Scroller:TGraphicSite); override;
       Property    PrintRange:TPrintingRange read FRange write FRange;
       Property    PrintSize:TPrintingSize read FSize write FSize;
       Property    PrintScale:Double read FScale write FScale;
{++ Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
       Property    PrintProjFrame:Boolean read FPrintProjFrame write FPrintProjFrame;
{-- Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
       Procedure   SaveToStream(S:TStream;Writer:TWriter); override;
       Property    ViewName:String read FView write FView;
       // saves the settings to the registry
       Procedure   SaveToRegistry(Registry:TCustomRegistry;const KeyName:String);
       // loads the settings from a registry
       Procedure   LoadFromRegistry(Registry:TCustomRegistry;const KeyName:String);
     end;

{     TOLEGraphic   = Class(TCustomGraphic)
      Private
       FOleContainer    : TOLEContainer;
      Public
       Constructor Create(AOwner:TGraphics); override;
       Destructor  Destroy; override;
       Procedure   SetParent(AParent:TScroller); override;
       Procedure   Align(Scroller:TGraphicSite); override;
       Property    OLEContainer:TOLEContainer read FOLEContainer write FOLEContainer;
       Procedure   Paint(Scroller:TGraphicSite); override;
     end;
}

     TLegendPlaceholder = Class(TCustomGraphic)
      Private
       FLegendName      : String;
       FLegendIndex     : Integer;
       Procedure   SetLegendName(ALegendName:String);
      Public
       LegendPos        : TPoint;
       OleObj           : TOLEContainer;
       OLEPath          : String;
       Constructor Create(AOwner: TGraphics); override;
       Destructor  Destroy; override;
       Property    LegendName:String read FLegendName write SetLegendName;
       Property    LegendIndex:Integer read FLegendIndex;
       Procedure   LoadFromStream(S:TStream;Reader:TReader;Version:Integer); override;
       Procedure   Paint(Scroller:TGraphicSite); override;
       Procedure   SaveToStream(S:TStream;Writer:TWriter); override;
       // saves the settings to the registry
       Procedure   SaveToRegistry(Registry:TCustomRegistry;const KeyName:String);
       // loads the settings from a registry
       Procedure   LoadFromRegistry(Registry:TCustomRegistry;const KeyName:String);
       function   LoadOLEFile(AParent: TWinControl; AFileName: String; AProjectPath: String): Boolean;
     end;

     TGraphics     = Class
      Private
       FFileName         : String;
       FList             : TList;
       // pointer to the project
       FProject          : PProj;
       FLegends          : TLegendList;
       FSignatureName    : String;
       FSignaturesCount  : Integer;  // it's not a realy signatures counter, it's counter for signature unique name creation
//++ Glukhov Bug#293(PrintFrame) Build#175 09.02.02
        // String to form the frame name (in combobox)
        FFrameName        : String;
        // Counter it used to generate the frame indexes (in combobox)
        FFramesCount      : Integer;
//-- Glukhov Bug#293(PrintFrame) Build#175 09.02.02
       FSigTextStyle     : TTextProperties;
       FSigForeColor     : TColor;
       FSigBackColor     : TColor;
       FCurrPrintPage    : Integer;
       FCurrPageClipRect : TRect;
       FRegistry         : TRegistryDatabase;
       FPageSetup        : TPageSetup;
       function    GetItem(AIndex: Integer): TCustomGraphic;
       function    GetItemCount: Integer;
       Procedure   SetProject(AProject:PProj);
      Private
       Procedure   SetParent(AParent:TScroller);
      Public
       Constructor Create;
       Destructor  Destroy; override;
       Procedure   Add(AGraphic:TCustomGraphic);
       Procedure   Align(Scroller:TGraphicSite);
       Procedure   Clear;
       Procedure   Delete(AIndex:Integer);
       Property    FileName:String read FFileName;
       Function    IndexOf(Item:TCustomGraphic):Integer;
       Procedure   Insert(AIndex:Integer;Item:TCustomGraphic);
       Function    ItemAtPosition(const APosition:TGrPoint):TCustomGraphic;
       Property    ItemCount:Integer read GetItemCount;
       Property    Items[AIndex:Integer]:TCustomGraphic read GetItem; default;
       Procedure   Paint(Scroller:TGraphicSite);
       Property    Parent:TScroller write SetParent;
       Property    PageSetup:TPageSetup read FPageSetup write FPageSetup;
       Property    Project:PProj read FProject write SetProject;
       Property    SignaturesCount:Integer read FSignaturesCount;
//++ Glukhov Bug#293(PrintFrame) Build#175 09.02.02
       Property    FramesCount : Integer read FFramesCount write FFramesCount;
       Property    FrameName:String read FFrameName;
//-- Glukhov Bug#293(PrintFrame) Build#175 09.02.02
       Property    SigTextStyle:TTextProperties read FSigTextStyle write FSigTextStyle;
       Property    SigForeColor:TColor read FSigForeColor write FSigForeColor;
       Property    SigBackColor:TColor read FSigBackColor write FSigBackColor;
       Property    CurrPrintPage:Integer read FCurrPrintPage write FCurrPrintPage;
       Property    CurrPageClipRect:TRect read FCurrPageClipRect write FCurrPageClipRect;
       // saves the settings to the registry
       Procedure   SaveToRegistry(Registry:TCustomRegistry;const KeyName:String);
       // loads the settings from a registry
       Procedure   LoadFromRegistry(Registry:TCustomRegistry;const KeyName:String);
       Function    LoadFromFile(const FileName:String):Boolean;
       Function    SaveToFile(const FileName:String = ''):Boolean;
     end;

Implementation

Uses ClipBrd,Dialogs,MultiLng,WPrinter;

{ TCustomGraphic }

Procedure TCustomGraphic.Align(Scroller: TGraphicSite);
begin
end;

Constructor TCustomGraphic.Create(AOwner:TGraphics);
begin
  inherited Create;
  FOwner:=AOwner;
  FBounds.Left:=0;
  FBounds.Bottom:=0;
  FBounds.Right:=0;
  FBounds.Top:=0;
  FPrintOrigin:=poTop;
  FPrintPosition:=ppCustom;
  FInitialised:=FALSE;
//++ Glukhov Bug#293(PrintFrame) Build#175 15.02.02
  FBitFlags:= 0;
//-- Glukhov Bug#293(PrintFrame) Build#175 15.02.02
   if FOwner<>NIL then FOwner.Add(Self);
end;

Destructor TCustomGraphic.Destroy;
begin
   if FOwner<>NIL then
      FOwner.FList.Remove(Self);
   inherited Destroy;
end;

//++ Glukhov Bug#293(PrintFrame) Build#175 15.02.02
Procedure TCustomGraphic.LoadFromStream(S:TStream;Reader:TReader;Version:Integer);
begin
  Reader.Read(FBounds,SizeOf(FBounds));
  if LongRec(Version).Hi>2 then  begin
    FPrintOrigin:=TPrintOrigin(Reader.ReadInteger);
    FPrintPosition:=TPrintPosition(Reader.ReadInteger);
    FInitialised:=Reader.ReadBoolean;
// YG: Added the byte to keep several bit flags
    if LongRec(Version).Hi >= FileMainVersionFrames then begin
      FBitFlags:= Reader.ReadInteger;
    end;
  end;
end;

Procedure TCustomGraphic.SaveToStream(S:TStream;Writer:TWriter);
begin
  Writer.Write(FBounds,SizeOf(FBounds));
  Writer.WriteInteger(Integer(FPrintOrigin));
  Writer.WriteInteger(Integer(FPrintPosition));
  Writer.WriteBoolean(FInitialised);
// YG: Added the byte to keep several bit flags
  Writer.WriteInteger(Integer(FBitFlags));
end;

Procedure TCustomGraphic.SaveToRegistry(Registry: TCustomRegistry; const KeyName: String);
begin
  with Registry do  begin
    WriteFloat(KeyName+'Bounds\Left',FBounds.Left);
    WriteFloat(KeyName+'Bounds\Bottom',FBounds.Bottom);
    WriteFloat(KeyName+'Bounds\Right',FBounds.Right);
    WriteFloat(KeyName+'Bounds\Top',FBounds.Top);
    WriteInteger(KeyName+'Origin',Integer(FPrintOrigin));
    WriteInteger(KeyName+'Position',Integer(FPrintPosition));
// YG: Now this byte keeps several bit flags
//    WriteBool(KeyName+'Initialised',FInitialised);
    if FInitialised
    then  FBitFlags:= FBitFlags or cwPrnTempInitialized
    else  FBitFlags:= FBitFlags and  (not cwPrnTempInitialized);
    WriteBinary(KeyName+'Initialised', FBitFlags, 1);
  end;
end;

Procedure TCustomGraphic.LoadFromRegistry(Registry: TCustomRegistry; const KeyName: String);
var dblValue  : Double;
    intValue  : Integer;
    boolValue : Boolean;
begin
  with Registry do  begin
    dblValue:=ReadFloat(KeyName+'Bounds\Left');
    if LastError=rdbeNoError then FBounds.Left:=dblValue;
    dblValue:=ReadFloat(KeyName+'Bounds\Bottom');
    if LastError=rdbeNoError then FBounds.Bottom:=dblValue;
    dblValue:=ReadFloat(KeyName+'Bounds\Right');
    if LastError=rdbeNoError then FBounds.Right:=dblValue;
    dblValue:=ReadFloat(KeyName+'Bounds\Top');
    if LastError=rdbeNoError then FBounds.Top:=dblValue;
    intValue:=ReadInteger(KeyName+'Origin');
    if LastError=rdbeNoError then FPrintOrigin:=TPrintOrigin(intValue);
    intValue:=ReadInteger(KeyName+'Position');
    if LastError=rdbeNoError then FPrintPosition:=TPrintPosition(intValue);
// YG: Now this byte keeps several bit flags
//    boolValue:=ReadBool(KeyName+'Initialised');
//    if LastError=rdbeNoError  then FInitialised:= TboolValue;
    ReadBinary(KeyName+'Initialised', FBitFlags, 1);
    if LastError <> rdbeNoError  then FBitFlags:= 0;
    FInitialised:= (FBitFlags and cwPrnTempInitialized)<>0;
  end;
end;
//-- Glukhov Bug#293(PrintFrame) Build#175 15.02.02

Procedure TCustomGraphic.Paint(Scroller: TGraphicSite);
begin
end;

Procedure TCustomGraphic.SetParent(AParent: TScroller);
begin
end;

Function IntersectTGrRect(FRect: TGrRect; SRect: TGrRect): TGrRect;
begin
   if (FRect.Left > SRect.Right) or (FRect.Right < SRect.Left) or
      (FRect.Bottom > SRect.Top) or (FRect.Top < SRect.Bottom) then
      begin
         Result:=GrRect(0,0,0,0);
         Exit;
      end;
   Result:=FRect;
   if FRect.Left < SRect.Left then
      Result.Left:=SRect.Left;
   if FRect.Right > SRect.Right then
      Result.Right:=SRect.Right;
   if FRect.Top > SRect.Top then
      Result.Top:=SRect.Top;
   if FRect.Bottom < SRect.Bottom then
      Result.Bottom:=SRect.Bottom;
end;   // End of the function IntersectTGrRect

{++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
// finding Page Extent which contains CustomGraphic Bounds Rectangle by MAX intersection square
Function TCustomGraphic.GetPageBaseExtent(var BaseRect: TGrRect; var IndX: Integer; var IndY: Integer):Double;
var TempRect       : TGrRect;
    dblSquare      : Double;
    CntX           : Integer;
    CntY           : Integer;
begin
   Result:=0.0;
   IndX:=0;
   IndY:=0;
   with FOwner.PageSetup do
      if (PrintLayoutModel = plmBook) and (HorizontalPages*VerticalPages > 1) then
         begin
            // finding Page Extent which contains Bounds Rectangle by MAX intersection square
            for CntY:=0 to VerticalPages-1 do
                for CntX:=0 to HorizontalPages-1 do
                    begin
                       TempRect:=IntersectTGrRect(FBounds,PageExtents[CntX,CntY]);
                       dblSquare:=RectWidth(TempRect)*RectHeight(TempRect);
                       if dblSquare > Result then
                          begin
                             Result:=dblSquare;
                             IndX:=CntX;
                             IndY:=CntY;
                          end;
                    end;
            BaseRect:=PageExtents[IndX,IndY];
         end
      else
         BaseRect:=PageExtent;
end;
{-- Moskaliov Pictures Location Control BUILD#131 18.09.00}

Procedure TCustomGraphic.Invalidate(Scroller: TScroller);
var CntX           : Integer;
    CntY           : Integer;
    IndX           : Integer;
    IndY           : Integer;
    TempRect       : TGrRect;
    BaseRect       : TGrRect;
    dblMax         : Double;
begin
   Scroller.InvalidateRect(FBounds);
   if Owner.PageSetup.PrintLayoutModel = plmBook then
{++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
      if (Self.ClassName = 'TPrintSignature') or (Self.ClassName = 'TWindowsGraphic') then
         with Owner.PageSetup do
            begin
               // finding Page Extent which contains Text by MAX intersection square
               dblMax:=GetPageBaseExtent(TempRect,IndX,IndY);
{-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
               if DblCompare(dblMax,0) <> 0 then
                  begin
                     // Text rectangle projection on the Page Extent [0,0]
                     BaseRect.Left:=FBounds.Left-PageExtents[IndX,IndY].Left;
                     BaseRect.Bottom:=FBounds.Bottom-PageExtents[IndX,IndY].Bottom;
                     BaseRect.Right:=BaseRect.Left+RectWidth(FBounds);
                     BaseRect.Top:=BaseRect.Bottom+RectHeight(FBounds);
                     for CntY:=VerticalPages-1 downto 0 do
                         for CntX:=0 to HorizontalPages-1 do
                             begin
                                TempRect.Left:=BaseRect.Left+PageExtents[CntX,CntY].Left;
                                TempRect.Bottom:=BaseRect.Bottom+PageExtents[CntX,CntY].Bottom;
                                TempRect.Right:=TempRect.Left+RectWidth(BaseRect);
                                TempRect.Top:=TempRect.Bottom+RectHeight(BaseRect);
                                Scroller.InvalidateRect(TempRect)
                             end;
                  end;
               Scroller.InvalidateRect(FBounds)
            end
      else
         Scroller.InvalidateRect(FBounds)
   else
      Scroller.InvalidateRect(FBounds);
end;

{ TOLEGraphic }

{

Procedure TOLEGraphic.Align(Scroller: TGraphicSite);
var ARect          : TRect;
begin
  Scroller.InternalToWindow(FBounds,ARect);
  LPToDP(Scroller.Canvas.Handle,ARect,2);
  OLEContainer.BoundsRect:=ARect;
end;

Constructor TOLEGraphic.Create(AOwner:TGraphics);
begin
  inherited Create(AOwner);
  FOLEContainer:=TOLEContainer.Create(Application);
end;

Destructor TOLEGraphic.Destroy;
begin
  FOLEContainer.Free;
  inherited Destroy;
end;

Procedure TOLEGraphic.Paint(Scroller: TGraphicSite);
begin
  if not Assigned(FOnPaint) or not FOnPaint(Scroller) then
      FOLEContainer.RePaint;
end;

Procedure TOLEGraphic.SetParent(AParent:TScroller);
begin
  FOLEContainer.Parent:=AParent.Window;
  FOLEContainer.Ctl3D:=FALSE;
  FOLECOntainer.BorderStyle:=bsNone;
  FOLEContainer.SizeMode:=smAutoSize;
  FOLECOntainer.Tabstop:=FALSE;
  FOLEContainer.ParentColor:=FALSE;
  FOLEContainer.Color:=clWindow;
end;

}

{ TGraphics }

Procedure TGraphics.Add(AGraphic: TCustomGraphic);
begin
   FList.Add(AGraphic);
   if AGraphic.ClassName = 'TPrintSignature' then Inc(FSignaturesCount);
end;

Procedure TGraphics.Align(Scroller: TGraphicSite);
var Cnt            : Integer;
    Graphic        : TCustomGraphic;
begin
   for Cnt:=0 to FList.Count-1 do
       begin
          Graphic:=FList[Cnt];
          Graphic.Align(Scroller);
       end;
end;

Constructor TGraphics.Create;
var intValue   : Integer;
    strValue   : String;
begin
   inherited Create;
   FList:=TList.Create;
   FSignatureName:='Signature';
//++ Glukhov Bug#293(PrintFrame) Build#175 09.02.02
   // String to form the frame name (in combobox)
   FFrameName:= 'Frame';
   // Counter it used to generate the frame indexes (in combobox)
   FFramesCount:= 0;
//-- Glukhov Bug#293(PrintFrame) Build#175 09.02.02
   strValue:=MlgStringList.SectionStrings[MlgStringList.Sections['PrintDialog'],106];
   intValue:=Length(strValue);
   if (intValue > 3) and (Copy(strValue,intValue-1,2) = '00') then
      FSignatureName:=Copy(strValue,1,intValue-2);
   FSignaturesCount:=0;
   FSigTextStyle:=TTextProperties.Create;
   FSigTextStyle.FontName:='Arial';
   FSigTextStyle.Alignment:=Ord(taLeft);
   FSigTextStyle.Bold:=0;
   FSigTextStyle.Italic:=0;
   FSigTextStyle.Underlined:=0;
   FSigTextStyle.Rotation:=0;
   FSigTextStyle.Transparent:=1;
   FSigTextStyle.Height:=12;
   FSigForeColor:=clBlack;
   FSigBackColor:=clWhite;
   FCurrPrintPage:=1;
   FRegistry:=TRegistryDatabase.Create;
   FPageSetup:=TPageSetup.Create;
end;

Destructor TGraphics.Destroy;
begin
   Clear;
   FList.Free;
   FRegistry.Free;
   FPageSetup.Free;
   inherited Destroy;
end;

Procedure TGraphics.SetProject(AProject:PProj);
begin
  FProject:=AProject;
  FLegends:=FProject^.Legends;
end;

Function TGraphics.ItemAtPosition(const APosition:TGrPoint):TCustomGraphic;
var Cnt            : Integer;
begin
   for Cnt:=FList.Count-1 downto 0 do
       begin
          Result:=FList[Cnt];
          if GrPointInRect(APosition,Result.BoundsRect) then
             Exit;
       end;
   Result:=NIL;
end;

Procedure TGraphics.Paint(Scroller: TGraphicSite);
var Cnt            : Integer;
    Graphic        : TCustomGraphic;
begin
   for Cnt:=0 to FList.Count-1 do
       begin
          Graphic:=FList[Cnt];
          Graphic.Paint(Scroller);
       end;
end;

Procedure TGraphics.SaveToRegistry(Registry: TCustomRegistry;const KeyName:String);
var Cnt            : Integer;
    Graphic        : TCustomGraphic;
begin
   with Registry do
      begin
         WriteString(KeyName+'FileName',FFileName);
         // save the page-setup
         FPageSetup.SaveToRegistry(Registry,KeyName+'PageSetup\');
         // save FSigTextStyle parameters
         WriteString(KeyName+'TextStyle\FontName',FSigTextStyle.FontName);
         WriteInteger(KeyName+'TextStyle\Alignment',FSigTextStyle.Alignment);
         WriteInteger(KeyName+'TextStyle\Bold',FSigTextStyle.Bold);
         WriteInteger(KeyName+'TextStyle\Italic',FSigTextStyle.Italic);
         WriteInteger(KeyName+'TextStyle\Underlined',FSigTextStyle.Underlined);
         WriteFloat(KeyName+'TextStyle\Rotation',FSigTextStyle.Rotation);
         WriteInteger(KeyName+'TextStyle\Transparent',FSigTextStyle.Transparent);
         WriteFloat(KeyName+'TextStyle\Height',FSigTextStyle.Height);

         WriteInteger(KeyName+'TextStyle\ForeColor',LongInt(FSigForeColor));
         WriteInteger(KeyName+'TextStyle\BackColor',LongInt(FSigBackColor));

         WriteInteger(KeyName+'ListCount',FList.Count);
      end;
   for Cnt:=0 to FList.Count-1 do
       begin
          Graphic:=FList[Cnt];
          with Graphic do
             begin
                Registry.WriteString(KeyName+'ClassName'+IntToStr(Cnt)+'\',ClassName);
                if ClassName='TProjectPlaceholder' then
                   TProjectPlaceholder(Graphic).SaveToRegistry(Registry,KeyName+'Progect'+IntToStr(Cnt)+'\')
                else
                   if ClassName='TLegendPlaceholder' then
                      TLegendPlaceholder(Graphic).SaveToRegistry(Registry,KeyName+'Legend'+IntToStr(Cnt)+'\')
                   else
                      if ClassName='TWindowsGraphic' then
                         TWindowsGraphic(Graphic).SaveToRegistry(Registry,KeyName+'Picture'+IntToStr(Cnt)+'\')
                      else
                         if ClassName='TPrintSignature' then
                            TPrintSignature(Graphic).SaveToRegistry(Registry,KeyName+'Signature'+IntToStr(Cnt)+'\')
                         else
                            TCustomGraphic(Graphic).SaveToRegistry(Registry,KeyName+'Something'+IntToStr(Cnt)+'\')
             end;
       end;
end;

Procedure TGraphics.LoadFromRegistry(Registry: TCustomRegistry;const KeyName:String);
var Cnt            : Integer;
    Count          : Integer;
    ClassName      : String;
    Graphic        : TCustomGraphic;
    intValue       : Integer;
    longValue      : LongInt;
    dblValue       : Double;
begin
   with Registry do
      begin
        FFileName:=ReadString(KeyName+'FileName');
        // read the page-setup
        FPageSetup.LoadFromRegistry(Registry,KeyName+'PageSetup\');
        // read FSigTextStyle parameters
        FSigTextStyle.FontName:=ReadString(KeyName+'TextStyle\FontName');
//++ Glukhov PrintSignature BUILD#166 15.12.01
        // Set font Arial, if no font is set
        if FSigTextStyle.FontName = ''  then FSigTextStyle.FontName:= 'Arial';
//-- Glukhov PrintSignature BUILD#166 15.12.01
         intValue:=ReadInteger(KeyName+'TextStyle\Alignment');
         if LastError=rdbeNoError then FSigTextStyle.Alignment:=intValue;
         intValue:=ReadInteger(KeyName+'TextStyle\Bold');
         if LastError=rdbeNoError then FSigTextStyle.Bold:=intValue;
         intValue:=ReadInteger(KeyName+'TextStyle\Italic');
         if LastError=rdbeNoError then FSigTextStyle.Italic:=intValue;
         intValue:=ReadInteger(KeyName+'TextStyle\Underlined');
         if LastError=rdbeNoError then FSigTextStyle.Underlined:=intValue;
         dblValue:=ReadFloat(KeyName+'TextStyle\Rotation');
         if LastError=rdbeNoError then FSigTextStyle.Rotation:=dblValue;
         intValue:=ReadInteger(KeyName+'TextStyle\Transparent');
         if LastError=rdbeNoError then FSigTextStyle.Transparent:=intValue;
         dblValue:=ReadFloat(KeyName+'TextStyle\Height');
         if LastError=rdbeNoError then FSigTextStyle.Height:=dblValue;

         longValue:=ReadInteger(KeyName+'TextStyle\ForeColor');
         if LastError=rdbeNoError then FSigForeColor:=TColor(longValue);
         longValue:=ReadInteger(KeyName+'TextStyle\BackColor');
         if LastError=rdbeNoError then FSigBackColor:=TColor(longValue);

         Count:=ReadInteger(KeyName+'ListCount');
      end;
   for Cnt:=0 to Count-1 do
       begin
          ClassName:=Registry.ReadString(KeyName+'ClassName'+IntToStr(Cnt));
          if ClassName='TProjectPlaceholder' then
             begin
                Graphic:=TProjectPlaceholder.Create(Self);
                TProjectPlaceholder(Graphic).LoadFromRegistry(Registry,KeyName+'Progect'+IntToStr(Cnt)+'\');
             end
          else
             if ClassName='TLegendPlaceholder' then
                begin
                   Graphic:=TLegendPlaceholder.Create(Self);
                   TLegendPlaceholder(Graphic).LoadFromRegistry(Registry,KeyName+'Legend'+IntToStr(Cnt)+'\');
                end
             else
                if ClassName='TWindowsGraphic' then
                   begin
                      Graphic:=TWindowsGraphic.Create(Self);
                      TWindowsGraphic(Graphic).LoadFromRegistry(Registry,KeyName+'Picture'+IntToStr(Cnt)+'\');
                   end
                else
                   if ClassName='TPrintSignature' then
                      begin
                         Graphic:=TPrintSignature.Create(Self);
                         TPrintSignature(Graphic).LoadFromRegistry(Registry,KeyName+'Signature'+IntToStr(Cnt)+'\');
                      end
                   else
                      begin
                         Graphic:=TCustomGraphic.Create(Self);
                         TCustomGraphic(Graphic).LoadFromRegistry(Registry,KeyName+'Something'+IntToStr(Cnt)+'\');
                      end;
       end;
end;

Function TGraphics.SaveToFile(const FileName: String): Boolean;
var S              : TFileStream;
    Writer         : TWriter;
    Cnt            : Integer;
    Graphic        : TCustomGraphic;
begin
   Result:=FALSE;
   if FileName<>'' then
      FFileName:=FileName;
   try
      S:=TFileStream.Create(FFileName,fmCreate);
      try
         // store internal data to the registry
         FPageSetup.SaveToRegistry(FRegistry,'\PageSetup');
         // store data to the file
         Writer:=TWriter.Create(S,4096);
         try
            Writer.WriteString('WinGIS Print Template File');
            Writer.WriteInteger(MakeLong(FileSubVersion,FileMainVersion));
            FRegistry.StoreToStream(Writer);
            // write FSigTextStyle parameters
            Writer.WriteString(FSigTextStyle.FontName);
            Writer.WriteInteger(FSigTextStyle.Alignment);
            Writer.WriteInteger(FSigTextStyle.Bold);
            Writer.WriteInteger(FSigTextStyle.Italic);
            Writer.WriteInteger(FSigTextStyle.Underlined);
            Writer.WriteFloat(FSigTextStyle.Rotation);
            Writer.WriteInteger(FSigTextStyle.Transparent);
            Writer.WriteFloat(FSigTextStyle.Height);

            Writer.WriteInteger(LongInt(FSigForeColor));
            Writer.WriteInteger(LongInt(FSigBackColor));

            Writer.WriteListBegin;
            for Cnt:=0 to FList.Count-1 do
                begin
                   Graphic:=FList[Cnt];
                   Writer.WriteString(Graphic.ClassName);
                   Graphic.SaveToStream(S,Writer);
                end;
            Writer.WriteListEnd;
            Result:=TRUE;
         finally
            Writer.Free;
         end;
      finally
         S.Free;
      end;
   except
      on EFCreateError do
         MessageBox(0,(PChar(Format(MlgStringList['PrintTemplate',6],[FileName]))),nil,MB_ICONERROR or MB_APPLMODAL);
      on EWriteError do
         MessageBox(0,(PChar(Format(MlgStringList['PrintTemplate',7],[FileName]))),nil,MB_ICONERROR or MB_APPLMODAL);
   else
      Raise;
   end;
end;

Function TGraphics.LoadFromFile(const FileName:String):Boolean;
var S              : TFileStream;
    ClassName      : String;
    Reader         : TReader;
    Graphic        : TCustomGraphic;
    Version        : Integer;
begin
   Result:=FALSE;
   try
      S:=TFileStream.Create(FileName,fmOpenRead);
      try
         Reader:=TReader.Create(S,4096);
         try
            Clear;
            if Reader.ReadString<>'WinGIS Print Template File' then
               Raise EPrintTemplate.Create(Format(MlgStringList['PrintTemplate',1],[FileName]));
            Version:=Reader.ReadInteger;
            if LongRec(Version).Hi>FileMainVersion then
               Raise EPrintTemplate.Create(Format(MlgStringList['PrintTemplate',2],[FileName]));
            FRegistry.LoadFromStream(Reader);
            // read FSigTextStyle parameters
            with FSigTextStyle do
               begin
                  FontName:=Reader.ReadString;
                  Alignment:=Reader.ReadInteger;
                  Bold:=Reader.ReadInteger;
                  Italic:=Reader.ReadInteger;
                  Underlined:=Reader.ReadInteger;
                  Rotation:=Reader.ReadFloat;
                  Transparent:=Reader.ReadInteger;
                  Height:=Reader.ReadFloat;
               end;

            FSigForeColor:=TColor(Reader.ReadInteger);
            FSigBackColor:=TColor(Reader.ReadInteger);

            Reader.ReadListBegin;
            while not Reader.EndOfList do
               begin
                  ClassName:=Reader.ReadString;
                  if ClassName='TProjectPlaceholder' then
                     Graphic:=TProjectPlaceholder.Create(Self)
                  else
                     if ClassName='TLegendPlaceholder' then
                        Graphic:=TLegendPlaceholder.Create(Self)
                     else
                        if ClassName='TWindowsGraphic' then
                           Graphic:=TWindowsGraphic.Create(Self)
                        else
                           if ClassName='TPrintSignature' then
                              Graphic:=TPrintSignature.Create(Self)
                           else
                              Raise EPrintTemplate.Create(Format(MlgStringList['PrintTemplate',3],[FileName]));
                  Graphic.LoadFromStream(S,Reader,Version);
               end;
            FFileName:=FileName;
            // load internal settings from the registry
            FPageSetup.LoadFromRegistry(FRegistry,'\PageSetup');
            Result:=TRUE;
         finally
            Reader.Free;
         end;
      finally
         S.Free;
      end;
   except
      on EFOpenError do
         Raise EPrintTemplate.Create(Format(MlgStringList['PrintTemplate',4],[FileName]));
      on EReadError do
         Raise EPrintTemplate.Create(Format(MlgStringList['PrintTemplate',5],[FileName]));
   else
      Raise;
   end;
end;

Procedure TGraphics.SetParent(AParent:TScroller);
var Cnt            : Integer;
    Graphic        : TCustomGraphic;
begin
   for Cnt:=0 to FList.Count-1 do
       begin
          Graphic:=FList[Cnt];
          Graphic.Parent:=AParent;
       end;
end;

Procedure TGraphics.Clear;
begin
   while FList.Count>0 do
      TCustomGraphic(FList[FList.Count-1]).Free;
   FSignaturesCount:=0;
end;

function TGraphics.GetItem(AIndex: Integer): TCustomGraphic;
begin
   Result:=TCustomGraphic(FList[AIndex]);
end;

function TGraphics.GetItemCount: Integer;
begin
   Result:=FList.Count;
end;

Procedure TGraphics.Delete(AIndex:Integer);
begin
   Items[AIndex].FOwner:=NIL;
   FList.Delete(AIndex);
end;

function TGraphics.IndexOf(Item:TCustomGraphic): Integer;
begin
   Result:=FList.IndexOf(Item);
end;

Procedure TGraphics.Insert(AIndex:Integer;Item:TCustomGraphic);
var intCount : Integer;
begin
   intCount:=FList.Count;
   FList.Insert(AIndex,Item);
   Item.FOwner:=Self;
   if intCount < FList.Count then
      if Item.ClassName = 'TPrintSignature' then Inc(FSignaturesCount);
end;

{ TPrintSignature }

Constructor TPrintSignature.Create(AOwner:TGraphics);
begin
   inherited Create(AOwner);
   FTextStyle:=TTextProperties.Create;
   FTextStyle.Assign(AOwner.FSigTextStyle);
   FForeColor:=AOwner.FSigForeColor;
   FBackColor:=AOwner.FSigBackColor;
   FPrintLocation:=plOneTime;
   FOnFirstPage:=TRUE;
   FOnInternalPages:=TRUE;
   FOnLastPage:=TRUE;
   if AOwner.FSignaturesCount < 10
   then  FSignature:=AOwner.FSignatureName+'0'+IntToStr(AOwner.FSignaturesCount)
   else  FSignature:=AOwner.FSignatureName+IntToStr(AOwner.FSignaturesCount);
end;

Destructor TPrintSignature.Destroy;
begin
   inherited Destroy;
   FTextStyle.Free;
end;

Procedure TPrintSignature.SetSignature(ASignature:String);
begin
  if ASignature = '' then
  begin
    if FInitialised then  with Owner do
    begin
      FSignature:= FSignatureName;
      Inc(FSignaturesCount);
      if FSignaturesCount < 10
      then  FSignature:= FSignatureName+'0'+IntToStr(FSignaturesCount)
      else  FSignature:= FSignatureName+IntToStr(FSignaturesCount);
      FInitialised:= FALSE;
    end;
  end
  else if FInitialised  then FSignature:= ASignature
  else if ASignature <> FSignature then begin
    FSignature:= ASignature;
    FInitialised:= TRUE;
  end;
end;

Function TPrintSignature.GetSignatureValue:String;
var strCompilePart : String;
    strRestPart    : String;
    intLength      : Integer;
    intBeginPos    : Integer;
    intEndPos      : Integer;
    intCnt         : Integer;

   // internal function
   Function GetFieldValue(AString:String):String;
   var Field       : TFieldCodeWord;
       Graphic     : TProjectPlaceholder;
       Cnt         : Integer;
   begin
      Field.Len:=Length(AString);
      Field.Sym:=Byte(AString[2]);
      Result:=AString;
      case Field.Code of
         DateCODE   : if AString = FieldsCodes[DateINDEX] then
                         Result:=DateToStr(Date);
         FileCODE   : if AString = FieldsCodes[FileINDEX] then
                         Result:=StrPas(Owner.FProject^.FName);
         PageCODE   : if AString = FieldsCodes[PageINDEX] then
                         Result:=IntToStr(Owner.FCurrPrintPage);
         TimeCODE   : if AString = FieldsCodes[TimeINDEX] then
                         Result:=TimeToStr(Time);
         PagesCODE  : if AString = FieldsCodes[PagesINDEX] then
                         with Owner.FPageSetup do
                            Result:=IntToStr(StartPageNum-1+HorizontalPages*VerticalPages);
{++ Moskaliov BUG#202b BUILD#144 04.12.00}
         ScaleCODE  : if AString = FieldsCodes[ScaleINDEX] then
                         begin
                            Graphic:=NIL;
                            // detect the project placeholder
                            for Cnt:=0 to Owner.ItemCount-1 do
                                if Owner[Cnt] is TProjectPlaceholder then
                                   begin
                                      Graphic:=TProjectPlaceholder(Owner[Cnt]);
                                      Break;
                                   end;
                            if Graphic<> NIL then
                               Result:='1:'+Format('%.0n',[1.0/Graphic.PrintScale]);
                         end;
{-- Moskaliov BUG#202b BUILD#144 04.12.00}
         AuthorCODE : if AString = FieldsCodes[AuthorINDEX] then
                         begin   // file InfoDlg
                            Owner.Project^.Registry.OpenKey('\Project\Info',FALSE);
                            Result:=Owner.Project^.Registry.ReadString('Author');
                         end;
      end;
   end;// End of internal function GetFieldValue

begin
   if not FInitialised then
      begin
         GetSignatureValue:=FSignature;
         Exit;
      end;
   strCompilePart:='';
   strRestPart:=FSignature;
   intLength:=Length(strRestPart);
   while intLength > 0 do
      if intLength >= FieldsLenMIN then
         begin
            intBeginPos:=Pos('&',strRestPart);
            if intBeginPos > 0 then
               begin
                  intLength:=intLength-intBeginPos+1;
                  if intLength >= FieldsLenMIN then
                     begin
                        strCompilePart:=strCompilePart+Copy(strRestPart,1,intBeginPos-1);
                        strRestPart:=Copy(strRestPart,intBeginPos,intLength);
                        intEndPos:=Pos('.',strRestPart);
                        if intEndPos > 0 then
                           begin
                              for intCnt:=intEndPos-1 downto 2 do
                                  if strRestPart[intCnt] = '&' then
                                     begin
                                        intLength:=intLength-intCnt+1;
                                        if intLength < FieldsLenMIN then
                                           begin
                                              strCompilePart:=strCompilePart+strRestPart;
                                              GetSignatureValue:=strCompilePart;
                                              Exit;
                                           end;
                                        strCompilePart:=strCompilePart+Copy(strRestPart,1,intCnt-1);
                                        strRestPart:=Copy(strRestPart,intCnt,intLength);
                                        intEndPos:=intEndPos-intCnt+1;
                                        Break;
                                     end;
                              intLength:=intLength-intEndPos+1;
                              if (intEndPos >= FieldsLenMIN) and (intEndPos <= FieldsLenMAX) then
                                 strCompilePart:=strCompilePart+GetFieldValue(Copy(strRestPart,1,intEndPos))
                              else
                                 strCompilePart:=strCompilePart+Copy(strRestPart,1,intEndPos);
                              strRestPart:=Copy(strRestPart,intEndPos+1,intLength);
                           end
                        else
                           begin
                              strCompilePart:=strCompilePart+strRestPart;
                              intLength:=0;
                           end;
                     end
                  else
                     begin
                        strCompilePart:=strCompilePart+strRestPart;
                        intLength:=0;
                     end;
               end
            else
               begin
                  strCompilePart:=strCompilePart+strRestPart;
                  intLength:=0;
               end;
         end
      else
         begin
            strCompilePart:=strCompilePart+strRestPart;
            intLength:=0;
         end;
   if strCompilePart = '' then
      strCompilePart:='   ';
   GetSignatureValue:=strCompilePart;
end;

Procedure TPrintSignature.SaveToRegistry(Registry: TCustomRegistry; const KeyName: String);
begin
   inherited SaveToRegistry(Registry,KeyName);
   with Registry do
      begin
         WriteString(KeyName+'Signature',FSignature);
         WriteInteger(KeyName+'Location',Integer(FPrintLocation));
         WriteBool(KeyName+'OnFirstPage',FOnFirstPage);
         WriteBool(KeyName+'OnInternalPages',FOnInternalPages);
         WriteBool(KeyName+'OnLastPage',FOnLastPage);
         // save FTextStyle parameters
         WriteString(KeyName+'TextStyle\FontName',FTextStyle.FontName);
         WriteInteger(KeyName+'TextStyle\Alignment',FTextStyle.Alignment);
         WriteInteger(KeyName+'TextStyle\Bold',FTextStyle.Bold);
         WriteInteger(KeyName+'TextStyle\Italic',FTextStyle.Italic);
         WriteInteger(KeyName+'TextStyle\Underlined',FTextStyle.Underlined);
         WriteFloat(KeyName+'TextStyle\Rotation',FTextStyle.Rotation);
         WriteInteger(KeyName+'TextStyle\Transparent',FTextStyle.Transparent);
         WriteFloat(KeyName+'TextStyle\Height',FTextStyle.Height);
         
         WriteInteger(KeyName+'TextStyle\ForeColor',LongInt(FForeColor));
         WriteInteger(KeyName+'TextStyle\BackColor',LongInt(FBackColor));
      end;
end;

Procedure TPrintSignature.LoadFromRegistry(Registry: TCustomRegistry; const KeyName: String);
var boolValue : Boolean;
    intValue  : Integer;
    longValue : LongInt;
    dblValue  : Double;
begin
   inherited LoadFromRegistry(Registry,KeyName);
   with Registry do
      begin
         FSignature:=ReadString(KeyName+'Signature');
         intValue:=ReadInteger(KeyName+'Location');
         if LastError=rdbeNoError then FPrintLocation:=TPrintLocation(intValue);
         boolValue:=ReadBool(KeyName+'OnFirstPage');
         if LastError=rdbeNoError then FOnFirstPage:=boolValue;
         boolValue:=ReadBool(KeyName+'OnInternalPages');
         if LastError=rdbeNoError then FOnInternalPages:=boolValue;
         boolValue:=ReadBool(KeyName+'OnLastPage');
         if LastError=rdbeNoError then FOnLastPage:=boolValue;
         // read FTextStyle parameters
         FTextStyle.FontName:=ReadString(KeyName+'TextStyle\FontName');
         intValue:=ReadInteger(KeyName+'TextStyle\Alignment');
         if LastError=rdbeNoError then FTextStyle.Alignment:=intValue;
         intValue:=ReadInteger(KeyName+'TextStyle\Bold');
         if LastError=rdbeNoError then FTextStyle.Bold:=intValue;
         intValue:=ReadInteger(KeyName+'TextStyle\Italic');
         if LastError=rdbeNoError then FTextStyle.Italic:=intValue;
         intValue:=ReadInteger(KeyName+'TextStyle\Underlined');
         if LastError=rdbeNoError then FTextStyle.Underlined:=intValue;
         dblValue:=ReadFloat(KeyName+'TextStyle\Rotation');
         if LastError=rdbeNoError then FTextStyle.Rotation:=dblValue;
         intValue:=ReadInteger(KeyName+'TextStyle\Transparent');
         if LastError=rdbeNoError then FTextStyle.Transparent:=intValue;
         dblValue:=ReadFloat(KeyName+'TextStyle\Height');
         if LastError=rdbeNoError then FTextStyle.Height:=dblValue;

         longValue:=ReadInteger(KeyName+'TextStyle\ForeColor');
         if LastError=rdbeNoError then FForeColor:=TColor(longValue);
         longValue:=ReadInteger(KeyName+'TextStyle\BackColor');
         if LastError=rdbeNoError then FBackColor:=TColor(longValue);
      end;
end;

Procedure TPrintSignature.SaveToStream(S:TStream;Writer:TWriter);
begin
   inherited SaveToStream(S,Writer);
   Writer.WriteString(FSignature);
   Writer.WriteInteger(Integer(FPrintLocation));
   Writer.WriteBoolean(FOnFirstPage);
   Writer.WriteBoolean(FOnInternalPages);
   Writer.WriteBoolean(FOnLastPage);
   // write FTextStyle parameters
   Writer.WriteString(FTextStyle.FontName);
   Writer.WriteInteger(FTextStyle.Alignment);
   Writer.WriteInteger(FTextStyle.Bold);
   Writer.WriteInteger(FTextStyle.Italic);
   Writer.WriteInteger(FTextStyle.Underlined);
   Writer.WriteFloat(FTextStyle.Rotation);
   Writer.WriteInteger(FTextStyle.Transparent);
   Writer.WriteFloat(FTextStyle.Height);
         
   Writer.WriteInteger(LongInt(FForeColor));
   Writer.WriteInteger(LongInt(FBackColor));
end;

Procedure TPrintSignature.LoadFromStream(S:TStream;Reader:TReader;Version:Integer);
begin
   inherited LoadFromStream(S,Reader,Version);
   FSignature:=Reader.ReadString;
   FPrintLocation:=TPrintLocation(Reader.ReadInteger);
   FOnFirstPage:=Reader.ReadBoolean;
   FOnInternalPages:=Reader.ReadBoolean;
   FOnLastPage:=Reader.ReadBoolean;
   // read FTextStyle parameters
   FTextStyle.FontName:=Reader.ReadString;
   FTextStyle.Alignment:=Reader.ReadInteger;
   FTextStyle.Bold:=Reader.ReadInteger;
   FTextStyle.Italic:=Reader.ReadInteger;
   FTextStyle.Underlined:=Reader.ReadInteger;
   FTextStyle.Rotation:=Reader.ReadFloat;
   FTextStyle.Transparent:=Reader.ReadInteger;
   FTextStyle.Height:=Reader.ReadFloat;

   FForeColor:=TColor(Reader.ReadInteger);
   FBackColor:=TColor(Reader.ReadInteger);
end;

Procedure TPrintSignature.Paint(Scroller:TGraphicSite);
var Rect           : TRect;
    dblHeight      : Double;
    dblWidth       : Double;
    intWidth       : Integer;
    Pos            : TPoint;
    dblMax         : Double;
    IndX           : Integer;
    IndY           : Integer;
    BaseRect       : TGrRect;
    i              : Integer;
    AText          : String;
    LCnt           : Integer;
begin
   if not Assigned(FOnPaint) or not FOnPaint(Scroller) then
      with Scroller.Canvas do
         begin
            Font.Name:=FTextStyle.FontName;
//++ Glukhov PrintSignature Build#166 17.01.02
// Font.Size should be negative (excluding the internal leading)
//            Font.Size:= Round(TextStyle.Height);
            Font.Size:= -Trunc(Abs(TextStyle.Height));
            // Calculate Font.Height, taking in account the window scale
            dblHeight:= (Font.Size * 25.4) / 72; // Font height in mm
            dblHeight:= Scroller.InternalToWindow(dblHeight);
            Font.Height:= -Round(dblHeight);
//-- Glukhov PrintSignature Build#166 17.01.02
            Font.Color:= FForeColor;
            Font.Style:= [];
            if FTextStyle.Italic = 1 then
               Font.Style:= Font.Style+[fsItalic];
            if FTextStyle.Bold = 1 then
               Font.Style:= Font.Style+[fsBold];
            if FTextStyle.Underlined = 1 then
               Font.Style:= Font.Style+[fsUnderline];

            Brush.Color:=FForeColor;
            Brush.Style:=bsClear;
            Pen.Color:=FForeColor;
            Pen.Style:=psSolid;

            SetBKColor(Handle,ColorToRGB(FBackColor));
            if FTextStyle.Transparent = 1 then
               SetBKMode(Handle,TRANSPARENT)
            else
               SetBKMode(Handle,OPAQUE);

            with FOwner.PageSetup do
               if (PrintLayoutModel = plmBook) and (FPrintLocation = plOnPages) then
                  begin
                     // finding Page Extent which contains Text by MAX intersection square
{-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
                     dblMax:=GetPageBaseExtent(BaseRect,IndX,IndY);
{-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
                     if DblCompare(dblMax,0) = 0 then
                        Exit;
                     dblHeight:=RectHeight(FBounds);
                     dblWidth:=RectWidth(FBounds);
                     // Text rectangle projection on the Page Extent [0,0]
                     BaseRect.Left:=FBounds.Left-PageExtents[IndX,IndY].Left;
                     BaseRect.Bottom:=FBounds.Bottom-PageExtents[IndX,IndY].Bottom;
                     BaseRect.Right:=FBounds.Left+dblWidth;
                     BaseRect.Top:=FBounds.Bottom+dblHeight;
                     // current Print Page Extent calculation
                     if PagesOrder then   // From left to right and down
                        begin
                           IndX:=FOwner.CurrPrintPage-StartPageNum;
                           IndY:=IndX div HorizontalPages;
                           IndX:=IndX-HorizontalPages*IndY;
                        end
                     else   // From top to bottom and right
                        begin
                           IndY:=FOwner.CurrPrintPage-StartPageNum;
                           IndX:=IndY div VerticalPages;
                           IndY:=IndY-VerticalPages*IndX;
                        end;
                     IndY:=VerticalPages-1-IndY;
                     // Text rectangle projection on the current Page Extent
                     BaseRect.Left:=BaseRect.Left+PageExtents[IndX,IndY].Left;
                     BaseRect.Bottom:=BaseRect.Bottom+PageExtents[IndX,IndY].Bottom;
                     BaseRect.Right:=BaseRect.Left+dblWidth;
                     BaseRect.Top:=BaseRect.Bottom+dblHeight;
                     Scroller.InternalToWindow(BaseRect,Rect);
                  end
               else
                  Scroller.InternalToWindow(FBounds,Rect);

/////////
          LCnt:=LineCount;
          for i:=0 to LCnt-1 do begin

            AText:=GetLine(i);

            intWidth:=TextWidth(AText);

           if i = 0 then begin
            if FPrintOrigin = poTop then
               Rect.Bottom:=Rect.Top-TextHeight(AText)*LCnt
            else
               Rect.Top:=Rect.Bottom+TextHeight(AText)*LCnt;
           end;
           
            case FTextStyle.Alignment of
               Ord(taRight)    : Rect.Left:=Rect.Right-intWidth;
               Ord(taCentered) : begin
                                    Rect.Left:=(Rect.Left+Rect.Right-intWidth) div 2;
                                    Rect.Right:=Rect.Left+intWidth;
                                 end;
            else { taLeft }
                                 Rect.Right:=Rect.Left+intWidth;
            end;

            Pos.x:=Rect.Left;
            Pos.y:=Rect.Top;

            with Owner.FCurrPageClipRect do
               begin
                  if Rect.Left < Left then
                     Rect.Left:=Left;
                  if Rect.Right > Right then
                     Rect.Right:=Right;
                  if Rect.Top > Top then
                     Rect.Top:=Top;
                  if Rect.Bottom < Bottom then
                     Rect.Bottom:=Bottom;
               end;

            if (Rect.Left < Rect.Right) and (Rect.Top > Rect.Bottom) then
               Scroller.Canvas.TextRect(Rect,Pos.x,Pos.y,AText);

            Rect.Top:=Rect.Top - TextHeight(AText);
          end;
       end;
end;

function TPrintSignature.LineCount: Integer;
var
i,n: Integer;
begin
     Result:=1;
     n:=StrLen(PChar(AnsiString(FSignature)));
     for i:=1 to n-1 do begin
         if FSignature[i] = '|' then begin
            Inc(Result);
         end;
     end;

end;

function TPrintSignature.GetLine(Index: Integer): String;
var
AList: TStringList;
s: String;
begin
     Result:='';
     if Index < LineCount then begin
        AList:=TStringList.Create;
        try
           s:=StringReplace(SignatureValue,'|',#13#10,[rfReplaceAll]);
           AList.Text:=s;
           Result:=AList[Index];
        finally
           AList.Free;
        end;
     end;
end;

{ TWindowsGraphic }

Constructor TWindowsGraphic.Create(AOwner:TGraphics);
begin
   inherited Create(AOwner);
   FLinked:=TRUE;
   FKeepAspect:=TRUE;
{++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
   FPrintLocation:=plOneTime;
   FOnFirstPage:=TRUE;
   FOnInternalPages:=TRUE;
   FOnLastPage:=TRUE;
{-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
end;

Destructor TWindowsGraphic.Destroy;
begin
   FImageInfo.Free;
   inherited Destroy;
end;

Procedure TWindowsGraphic.Paint(Scroller: TGraphicSite);
var ARect          : TRect;
    BRect          : TRect;
    AHeight        : Integer;
    AWidth         : Integer;
    BHeight        : Integer;
    BWidth         : Integer;
    dblValue       : Double;
{++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
    dblHeight      : Double;
    dblWidth       : Double;
    TempRect       : TGrRect;
    BaseRect       : TGrRect;
    dblMax         : Double;
    IndX           : Integer;
    IndY           : Integer;

   //   internal procedure
   Procedure CutTRect(AClipRect : TRect);
   begin
      if (RectWidth(AClipRect) > 0) and (RectHeight(AClipRect) > 0) then
         // checking up of the intersection with ClipRect
         begin
            if (ARect.Left >= AClipRect.Right) or (ARect.Right <= AClipRect.Left) or
               (ARect.Top <= AClipRect.Bottom) or (ARect.Bottom >= AClipRect.Top) then
               Exit;
            AWidth:=RectWidth(ARect);
            AHeight:=RectHeight(ARect);
            BWidth:=RectWidth(BRect);
            BHeight:=RectHeight(BRect);
            if ARect.Left < AClipRect.Left then
               begin
                  dblValue:=AClipRect.Left-ARect.Left;
                  dblValue:=(dblValue*BWidth)/AWidth;
                  BRect.Left:=BRect.Left+Round(dblValue);
                  ARect.Left:=AClipRect.Left;
               end;
            if ARect.Right > AClipRect.Right then
               begin
                  dblValue:=ARect.Right-AClipRect.Right;
                  dblValue:=(dblValue*BWidth)/AWidth;
                  BRect.Right:=BRect.Right-Round(dblValue);
                  ARect.Right:=AClipRect.Right;
               end;
            if ARect.Top > AClipRect.Top then
               begin
                  dblValue:=ARect.Top-AClipRect.Top;
                  dblValue:=(dblValue*BHeight)/AHeight;
                  BRect.Top:=BRect.Top+Round(dblValue);
                  ARect.Top:=AClipRect.Top;
               end;
            if ARect.Bottom < AClipRect.Bottom then
               begin
                  dblValue:=AClipRect.Bottom-ARect.Bottom;
                  dblValue:=(dblValue*BHeight)/AHeight;
                  BRect.Bottom:=BRect.Bottom-Round(dblValue);
                  ARect.Bottom:=AClipRect.Bottom;
               end;
         end;
   end;   // End of the internal procedure CutTRect

begin
   if not Assigned(FOnPaint) or not FOnPaint(Scroller) then
      if FImageInfo <> NIL then
         begin
            with Owner.PageSetup do
               if (PrintLayoutModel = plmBook) and (PrintLocation <> plOneTime) then
                  begin
                     // finding Page Extent which contains Picture by MAX intersection square
                     dblMax:=GetPageBaseExtent(TempRect,IndX,IndY);
                     if DblCompare(dblMax,0) = 0 then
                        Exit;
                     dblHeight:=RectHeight(FBounds);
                     dblWidth:=RectWidth(FBounds);
                     // Picture rectangle projection on the Page Extent [0,0]
                     BaseRect.Left:=BoundsRect.Left-PageExtents[IndX,IndY].Left;
                     BaseRect.Bottom:=BoundsRect.Bottom-PageExtents[IndX,IndY].Bottom;
                     BaseRect.Right:=BaseRect.Left+dblWidth;
                     BaseRect.Top:=BaseRect.Bottom+dblHeight;
                     // current Print Page Extent calculation
                     if PagesOrder then   // From left to right and down
                        begin
                           IndX:=FOwner.CurrPrintPage-StartPageNum;
                           IndY:=IndX div HorizontalPages;
                           IndX:=IndX-HorizontalPages*IndY;
                        end
                     else   // From top to bottom and right
                        begin
                           IndY:=FOwner.CurrPrintPage-StartPageNum;
                           IndX:=IndY div VerticalPages;
                           IndY:=IndY-VerticalPages*IndX;
                        end;
                     IndY:=VerticalPages-1-IndY;
                     // Text rectangle projection on the current Page Extent
                     BaseRect.Left:=BaseRect.Left+PageExtents[IndX,IndY].Left;
                     BaseRect.Bottom:=BaseRect.Bottom+PageExtents[IndX,IndY].Bottom;
                     BaseRect.Right:=BaseRect.Left+dblWidth;
                     BaseRect.Top:=BaseRect.Bottom+dblHeight;
                     Scroller.InternalToWindow(BaseRect,ARect);
                  end
               else
                  Scroller.InternalToWindow(FBounds,ARect);
            BRect:=FImageInfo.Bounds;
            CutTRect(FOwner.FCurrPageClipRect);
            FImageInfo.Draw(Scroller.Canvas.Handle,@ARect,@BRect,0,srcCopy);
         end;
end;
{-- Moskaliov Pictures Location Control BUILD#131 18.09.00}

Function TWindowsGraphic.CreateFromClipboard:Boolean;
var AFormat        : Word;
begin
   if ClipBoard.HasFormat(CF_METAFILEPICT) then
      AFormat:=CF_METAFILEPICT
   else
      if ClipBoard.HasFormat(CF_BITMAP) then
         AFormat:=CF_BITMAP
      else
         AFormat:=0;
   if AFormat<>0 then
      begin
//    FPicture.LoadFromClipboardFormat(AFormat,Clipboard.GetAsHandle(AFormat),0);
         FitToPage;
      end;
   Result:=AFormat<>0;
end;

Procedure TWindowsGraphic.CreateFromFile(const AFile,ARelativePath:String;ALinked:Boolean);
begin
   FLinked:=ALinked;
   if FLinked then
      begin
         FFileName:=AFile;
         FRelativePath:=ARelativePath;
         LoadFile;
      end;
   FitToBounds;
end;

Procedure TWindowsGraphic.LoadFile;
var Status         : Word;
    AFileName      : String;
begin
   FInitialised:=FALSE;
   AFileName:=FRelativePath+FFileName;
   if not FileExists(AFileName) then
      if FileExists(FFileName) then
         AFileName:=GetCurrentDir+'\'+FFileName
      else
         begin
//          Raise EPrintTemplate.Create(Format(MlgStringList['PrintTemplate',4],[AFileName]));
            Application.MessageBox(PChar(Format(MlgStringList['PrintTemplate',4],[AFileName])),
                                   PChar(MlgStringList['PrintTemplate',10]),
                                   MB_OK+MB_ICONERROR+MB_DEFBUTTON1);
            FOriginalBounds:=FBounds;
            Exit;
         end;

   FImageInfo:=TrLibInfo.Create(AFileName,Status,0);
   if Status <> 0 then
      begin
//       Raise EPrintTemplate.Create(Format(MlgStringList['PrintTemplate',Status+10],[AFileName]));
         Application.MessageBox(PChar(Format(MlgStringList['PrintTemplate',Status+10],[AFileName])),
                                PChar(MlgStringList['PrintTemplate',10]),
                                MB_OK+MB_ICONERROR+MB_DEFBUTTON1);
         FOriginalBounds:=FBounds;
      end
   else
      begin
         FOriginalBounds:=GrRect(0,0,FImageInfo.WidthMM,FImageInfo.HeightMM);
         FInitialised:=TRUE;
      end;
end;

Procedure TWindowsGraphic.LoadFromStream(S:TStream;Reader:TReader;Version:Integer);
begin
   inherited LoadFromStream(S,Reader,Version);
   FLinked:=Reader.ReadBoolean;
   if FLinked then
      begin
         FFileName:=Reader.ReadString;
         FRelativePath:=Reader.ReadString;
         if LongRec(Version).Hi > 2 then
            begin
               FKeepAspect:=Reader.ReadBoolean;
{++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
               if LongRec(Version).Lo > 0 then
                  begin
                     FPrintLocation:=TPrintLocation(Reader.ReadInteger);
                     FOnFirstPage:=Reader.ReadBoolean;
                     FOnInternalPages:=Reader.ReadBoolean;
                     FOnLastPage:=Reader.ReadBoolean;
                  end;
{-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
            end;
         LoadFile;
      end
   else
      begin
         Reader.FlushBuffer;
      end;
end;

Procedure TWindowsGraphic.SaveToStream(S:TStream;Writer:TWriter);
begin
   inherited SaveToStream(S,Writer);
   Writer.WriteBoolean(FLinked);
   if FLinked then
      begin
         Writer.WriteString(FFileName);
         Writer.WriteString(FRelativePath);
         Writer.WriteBoolean(FKeepAspect);
{++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
         Writer.WriteInteger(Integer(FPrintLocation));
         Writer.WriteBoolean(FOnFirstPage);
         Writer.WriteBoolean(FOnInternalPages);
         Writer.WriteBoolean(FOnLastPage);
{-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
      end
   else
      begin
//       Writer.FlushBuffer;
//       FPicture.Graphic.SaveToStream(S);
      end;
end;

Procedure TWindowsGraphic.SaveToRegistry(Registry: TCustomRegistry; const KeyName: String);
begin
   with Registry do
      begin
         WriteString(KeyName+'FileName',FFileName);
         WriteString(KeyName+'RelativePath',FRelativePath);
         WriteBool(KeyName+'Linked',FLinked);
         WriteBool(KeyName+'KeepAspect',FKeepAspect);
{++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
         WriteInteger(KeyName+'Location',Integer(FPrintLocation));
         WriteBool(KeyName+'OnFirstPage',FOnFirstPage);
         WriteBool(KeyName+'OnInternalPages',FOnInternalPages);
         WriteBool(KeyName+'OnLastPage',FOnLastPage);
{-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
      end;
   inherited SaveToRegistry(Registry,KeyName);
end;

Procedure TWindowsGraphic.LoadFromRegistry(Registry: TCustomRegistry; const KeyName: String);
var boolValue : Boolean;
{++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
    intValue  : Integer;
{-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
begin
   with Registry do
      begin
         FFileName:=ReadString(KeyName+'FileName');
         FRelativePath:=ReadString(KeyName+'RelativePath');
         boolValue:=ReadBool(KeyName+'Linked');
         if LastError=rdbeNoError then FLinked:=boolValue;
         boolValue:=ReadBool(KeyName+'KeepAspect');
         if LastError=rdbeNoError then FKeepAspect:=boolValue;
{++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
         intValue:=ReadInteger(KeyName+'Location');
         if LastError=rdbeNoError then FPrintLocation:=TPrintLocation(intValue);
         boolValue:=ReadBool(KeyName+'OnFirstPage');
         if LastError=rdbeNoError then FOnFirstPage:=boolValue;
         boolValue:=ReadBool(KeyName+'OnInternalPages');
         if LastError=rdbeNoError then FOnInternalPages:=boolValue;
         boolValue:=ReadBool(KeyName+'OnLastPage');
         if LastError=rdbeNoError then FOnLastPage:=boolValue;
{-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
         inherited LoadFromRegistry(Registry,KeyName);
         LoadFile;
      end;
end;

Procedure TWindowsGraphic.FitToBounds;
var Scale1         : Double;
    Scale2         : Double;
begin
   Scale1:=RectWidth(FBounds)/FImageInfo.Width;
   Scale2:=RectHeight(FBounds)/FImageInfo.Height;
   if Scale1>Scale2 then
      Scale1:=Scale2;
   FBounds.Right:=FBounds.Left+Scale1*FImageInfo.Width;
   if FPrintOrigin = poTop then
      FBounds.Bottom:=FBounds.Top-Scale1*FImageInfo.Height
   else
      FBounds.Top:=FBounds.Bottom+Scale1*FImageInfo.Height;
end;

Procedure TWindowsGraphic.FitToPage;
var Scale1         : Double;
    Scale2         : Double;
    ARect          : TGrRect;
begin
   ARect:=Owner.PageSetup.PageExtent;
   Scale1:=RectWidth(ARect)/FImageInfo.Width;
   Scale2:=RectHeight(ARect)/FImageInfo.Height;
   if Scale1<Scale2 then
      ARect.Bottom:=ARect.Top-Scale1*FImageInfo.Height
   else
      ARect.Right:=ARect.Left+Scale2*FImageInfo.Width;
   BoundsRect:=ARect;
end;

{ TProjectPlaceholder }

constructor TProjectPlaceholder.Create(AOwner: TGraphics);
begin
   inherited Create(AOwner);
   FRange:=prCurrentView;
   FSize:=psFitToPage;
   FScale:=0.0001;
   FInitialised:=TRUE;
{++ Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
   FPrintProjFrame:=FALSE;
{-- Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
end;

Procedure TProjectPlaceholder.Paint(Scroller:TGraphicSite);
var Rect            : TRect;
    FillPlaceholder : Boolean; {++ Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
begin
{++ Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
// if not Assigned(FOnPaint) or not FOnPaint(Scroller) then
   if Assigned(FOnPaint) then
      begin
         FillPlaceholder:=FOnPaint(Scroller);
         if FPrintProjFrame and FillPlaceholder then
            // Draw the Project Frame
            with Scroller.Canvas do
               begin
                  Pen.Style:=psSolid;
                  Pen.Color:=clBlack;
                  Brush.Color:=clNone;
                  Brush.Style:=bsClear;
                  Scroller.InternalToWindow(FBounds,Rect);
                  LPToDP(Handle,Rect,2);
                  Inc(Rect.Right); Inc(Rect.Bottom);
                  DPToLP(Handle,Rect,2);
                  SetBKColor(Handle,ColorToRGB(clNone));
                  SetBKMode(Handle,TRANSPARENT);
                  with Rect do
                     Rectangle(Left,Bottom,Right,Top);
                  Brush.Color:=clWindow;
               end;
         FillPlaceholder:= not FillPlaceholder;
      end
   else
      FillPlaceholder:=TRUE;
   if FillPlaceholder then
{-- Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
      with Scroller.Canvas do
         begin
            Pen.Style:=psSolid;
            Pen.Color:=clRed;
            Brush.Style:=bsBDiagonal;
            Brush.Color:=clRed;
            Scroller.InternalToWindow(FBounds,Rect);
            LPToDP(Handle,Rect,2);
            Inc(Rect.Right); Inc(Rect.Bottom);
            DPToLP(Handle,Rect,2);
            SetBKColor(Handle,ColorToRGB(clWindow));
            SetBKMode(Handle,OPAQUE);
            with Rect do
               Rectangle(Left,Bottom,Right,Top);
            Brush.Color:=clWindow;
         end;
end;

{++ Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
Procedure TProjectPlaceholder.DrawFrame(Scroller: TGraphicSite);
var
Rect           : TRect;
l,t,r,b        : Integer;
begin
   if FPrintProjFrame then
      with Scroller do
         begin
            Scroller.InternalToWindow(FBounds,Rect);

            //SetBKColor(Canvas.Handle,ColorToRGB(clNone));
            //SetBKMode(Canvas.Handle,TRANSPARENT);

            l:=Max(Rect.Left,FOwner.FCurrPageClipRect.Left)+1;
            t:=Min(Rect.Top,FOwner.FCurrPageClipRect.Top)-1;
            r:=Min(Rect.Right,FOwner.FCurrPageClipRect.Right)-1;
            b:=Max(Rect.Bottom,FOwner.FCurrPageClipRect.Bottom)+1;

            Canvas.Pen.Style:=psSolid;
            Canvas.Pen.Color:=TColor($010101);

            Canvas.MoveTo(l,t);
            Canvas.LineTo(l,b);
            Canvas.LineTo(r,b);
            Canvas.LineTo(r,t);
            Canvas.LineTo(l,t);
         end;
end;
{-- Moskaliov BUGS##202b,293 BUILD#152 19.01.01}

Procedure TProjectPlaceholder.SaveToRegistry(Registry: TCustomRegistry; const KeyName: String);
begin
   with Registry do
      begin
         WriteInteger(KeyName+'Range',Integer(FRange));
         WriteInteger(KeyName+'Size',Integer(FSize));
         WriteFloat(KeyName+'Scale',FScale);
         WriteString(KeyName+'View',FView);
{++ Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
         WriteBool(KeyName+'PrintProjFrame',FPrintProjFrame);
{-- Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
      end;
   inherited SaveToRegistry(Registry,KeyName);
end;

Procedure TProjectPlaceholder.LoadFromRegistry(Registry: TCustomRegistry; const KeyName: String);
var intValue  : Integer;
    dblValue  : Double;
{++ Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
    boolValue : Boolean;
{-- Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
begin
   with Registry do
      begin
         intValue:=ReadInteger(KeyName+'Range');
         if LastError=rdbeNoError then FRange:=TPrintingRange(intValue);
         intValue:=ReadInteger(KeyName+'Size');
         if LastError=rdbeNoError then FSize:=TPrintingSize(intValue);
         dblValue:=ReadFloat(KeyName+'Scale');
         if LastError=rdbeNoError then FScale:=dblValue;
         FView:=ReadString(KeyName+'View');
{++ Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
         boolValue:=ReadBool(KeyName+'PrintProjFrame');
         if LastError=rdbeNoError then FPrintProjFrame:=boolValue;
{-- Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
      end;
   inherited LoadFromRegistry(Registry,KeyName);
end;

Procedure TProjectPlaceholder.SaveToStream(S:TStream;Writer:TWriter);
begin
   inherited SaveToStream(S,Writer);
   Writer.WriteInteger(Integer(FRange));
   Writer.WriteInteger(Integer(FSize));
   Writer.WriteFloat(FScale);
   Writer.WriteString(FView);
{++ Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
   Writer.WriteBoolean(FPrintProjFrame);
{-- Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
end;

Procedure TProjectPlaceholder.LoadFromStream(S:TStream;Reader:TReader;Version:Integer);
begin
   inherited LoadFromStream(S,Reader,Version);
   FRange:=TPrintingRange(Reader.ReadInteger);
   FSize:=TPrintingSize(Reader.ReadInteger);
   FScale:=Reader.ReadFloat;
   FView:=Reader.ReadString;
{++ Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
   if LongRec(Version).Hi > 2 then
      if LongRec(Version).Lo > 1 then
         FPrintProjFrame:=Reader.ReadBoolean;
{-- Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
end;

{ TLegendPlaceholder }

constructor TLegendPlaceholder.Create(AOwner: TGraphics);
begin
   inherited Create(AOwner);
   FLegendIndex:=-1;
   OleObj:=TOLEContainer.Create(Application);
   OleObj.Visible:=False;
   OleObj.AllowInPlace:=False;
   OleObj.AllowActiveDoc:=False;
   OleObj.Ctl3D:=FALSE;
   OleObj.SizeMode:=smScale;//smStretch;
   OleObj.Tabstop:=FALSE;
   OleObj.ParentColor:=FALSE;
   OleObj.Color:=clWhite;
   OleObj.BorderStyle:=bsNone;
end;

function TLegendPlaceholder.LoadOLEFile(AParent: TWinControl; AFileName: String; AProjectPath: String): Boolean;
begin
   Result:=False;
   AProjectPath:=ExtractFilePath(AProjectPath);
   if not FileExists(AFileName) then begin
      AFileName:=AProjectPath + ExtractFileName(AFileName);
   end;
   if not FileExists(AFileName) then begin
      Exit;
   end;
   Screen.Cursor:=crHourGlass;
   try
      OleObj.Parent:=AParent;
      OleObj.Visible:=False;

      OleObj.CreateObjectFromFile(AFileName,False);
      if OleObj.State <> osEmpty then begin
         OlePath:=AFileName;
         Result:=True;
      end
      else begin
         OlePath:='';
      end;
   finally
      Screen.Cursor:=crDefault;
   end;
end;

destructor TLegendPlaceholder.Destroy;
begin
   if OleObj <> nil then begin
      OleObj.Free;
   end;
   inherited Destroy;
end;

Procedure TLegendPlaceholder.SetLegendName(ALegendName:String);
var Cnt  : Integer;    begin
  FLegendName:= ALegendName;
  FLegendIndex:= -1;
  FInitialised:= FALSE;
//++ Glukhov Bug#293(PrintFrame) Build#175 15.02.02
  if (FLegendName = csLegName4Frame) or (FLegendName = csOleFrame) then begin
    FInitialised:= TRUE;
    Exit;
  end;
//-- Glukhov Bug#293(PrintFrame) Build#175 15.02.02
  if FLegendName <> '' then
    for Cnt:=0 to Owner.FLegends.Count-1 do
      if FLegendName = Owner.FLegends.Legends[Cnt].Name then
      begin
        FLegendIndex:= Cnt;
        FInitialised:= TRUE;
        Break;
      end;
end;

Procedure TLegendPlaceholder.Paint(Scroller:TGraphicSite);
var Rect           : TRect;
begin
   if not Assigned(FOnPaint) or not FOnPaint(Scroller) then
      with Scroller.Canvas do
         begin
            Pen.Style:=psSolid;
            Pen.Color:=clBlue;
            Brush.Style:=bsFDiagonal;
            Brush.Color:=clBlue;
            Scroller.InternalToWindow(FBounds,Rect);
            LPToDP(Handle,Rect,2);
            Inc(Rect.Right); Inc(Rect.Bottom);
            DPToLP(Handle,Rect,2);
            SetBKColor(Handle,ColorToRGB(clWindow));
            SetBKMode(Handle,OPAQUE);
            with Rect do
               Rectangle(Left,Bottom,Right,Top);
            Brush.Color:=clWindow;
         end;
end;

Procedure TLegendPlaceholder.SaveToRegistry(Registry: TCustomRegistry; const KeyName: String);
begin
   with Registry do
      begin
         WriteString(KeyName+'LegendName',FLegendName);
         WriteString(KeyName+'OLEPath',OLEPath);
      end;
   inherited SaveToRegistry(Registry,KeyName);
end;

Procedure TLegendPlaceholder.LoadFromRegistry(Registry: TCustomRegistry; const KeyName: String);
var strValue  : String;
begin
   with Registry do
      begin
         strValue:=ReadString(KeyName+'LegendName');
         SetLegendName(strValue);

         strValue:=ReadString(KeyName+'OLEPath');
         OLEPath:=strValue;
      end;
   inherited LoadFromRegistry(Registry,KeyName);
end;

Procedure TLegendPlaceholder.LoadFromStream(S:TStream;Reader:TReader;Version:Integer);
var strValue  : String;
begin
   inherited LoadFromStream(S,Reader,Version);
   if LongRec(Version).Hi>1 then
      begin
        strValue:=Reader.ReadString;
        SetLegendName(strValue);
      end
end;

Procedure TLegendPlaceholder.SaveToStream(S:TStream;Writer:TWriter);
begin
   inherited SaveToStream(S,Writer);
   Writer.WriteString(FLegendName);
end;

end.
