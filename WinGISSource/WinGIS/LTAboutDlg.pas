unit LTAboutDlg;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls;

type
  TLTAboutBox = class(TForm)
    Panel1: TPanel;
    ProgramIcon: TImage;
    ProductName: TLabel;
    Version: TLabel;
    Copyright: TLabel;
    Comments: TLabel;
    OKButton: TButton;
    procedure FormCreate(Sender: TObject);
    procedure CommentsClick(Sender: TObject);
  end;

var
  LTAboutBox: TLTAboutBox;

implementation

uses ShellApi;

{$R *.DFM}

procedure TLTAboutBox.FormCreate(Sender: TObject);

   function GetFileVersion: String;
   var ProgName     : Array[0..255] of Char;
       VersionInfo  : PChar;
       FileOffset   : LongWord;
       StructSize   : LongInt;
       Info         : Pointer;
       InfoSize     : LongWord;
   begin
        GetModuleFileName(HInstance,ProgName,SizeOf(ProgName));
        StructSize:=GetFileVersionInfoSize(ProgName,FileOffset);
        if StructSize>0 then begin
           GetMem(VersionInfo,StructSize);
           if VersionInfo<>NIL then begin
              if GetFileVersionInfo(ProgName,FileOffset,StructSize,VersionInfo) then begin
                 VerQueryValue(VersionInfo,'\StringFileInfo\080904E4\FileVersion',Info,InfoSize);
              end;
              FreeMem(VersionInfo,StructSize);
           end;
        end;
        Result:=AnsiString(PChar(Info));
   end;

begin
     Version.Caption:='Version: ' + GetFileVersion;
end;

procedure TLTAboutBox.CommentsClick(Sender: TObject);
begin
     ShellExecute(0,'open','http://www.progis.com','','',SW_SHOWMAXIMIZED);
end;

end.

