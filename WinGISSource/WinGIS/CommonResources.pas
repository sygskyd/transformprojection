Unit CommonResources;

Interface

Uses Controls;

Const // common bitmaps for listboxes and tree-views
      clbLayer        = 0;
      clbLayerGray    = 1;
      clbZoom         = 2;
      clbLayerZoom    = 3;
      clbSystem       = 4;
      clbUser         = 5;
      clbVisible      = 6;
      clbGeneralised  = 7;
      clbLegend       = 8;
      clbHSDVisible   = 9;

var CommonListBitmaps : TImageList;

Implementation

end.
 