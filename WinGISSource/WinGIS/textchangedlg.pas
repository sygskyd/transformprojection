unit textchangedlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, WCtrls, ExtCtrls, MultiLng,am_def;

type
  TTextChangeDialog = class(TForm)
    ControlPanel: TPanel;
    WGroupBox1: TWGroupBox;
    OKBtn: TButton;
    CancelBtn: TButton;
    MlgSection: TMlgSection;
    Memo1: TMemo;
    procedure Memo1Change(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);


  private
    { Private declarations }
  public
  CurrentText:ansistring;
  Procedure LoadText;

    { Public declarations }
  end;

var
  TextChangeDialog: TTextChangeDialog;

implementation

{$R *.DFM}

procedure TTextChangeDialog.Loadtext;

begin
Memo1.Lines.Add(CurrentText);
end;

procedure TTextChangeDialog.Memo1Change(Sender: TObject);
var i:integer;
begin
CurrentText:='';
 for i:= 0 to Memo1.Lines.Count-2 do
CurrentText:= CurrentText+Memo1.Lines[i]+chr(13);
if Trim(Memo1.Lines[Memo1.Lines.Count-1])<>'' 
 then CurrentText:=CurrentText+ Memo1.Lines[Memo1.Lines.Count-1];
end;

procedure TTextChangeDialog.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
CanClose:=true;
if ModalResult= mrOk then
 CanClose:=Trim(CurrentText)<>'';
end;

end.
