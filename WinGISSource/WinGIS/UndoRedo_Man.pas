unit UndoRedo_Man;

interface

uses
  UndoRedo_WG_Lib,
  Forms, Dialogs, Classes, Windows, Messages,
  Controls, SysUtils, Gauges, Graphics, Objects,
  Am_Proj, Am_Layer, Am_View, Am_Index, Am_Group,
  IDB_CallBacksDef, IDB_Consts,
  MenuFn, lists, DK_DestLayer;

type
  TUndoRedo_OldMemoryStream = class(TOldMemoryStream)
//   TUndoRedo_OldMemoryStream = object (TOldMemoryStream)
  public
    procedure SetPointer(APointer: Pointer; Size: LongInt);
    function GetDataStreamPointer: Pointer;
  end;

  TUndoRedoManager = class
  private
    UndoRedoLib: TUndoRedoLib;

    AnOldScreenCursor: TCursor;

    bNessesaryToSaveUndoData: boolean;
    procedure SetScreenCursor;
    procedure RestoreScreenCursor;

    function GetObjectTypeByItemIndex(iIndex: Integer): Integer;

    procedure AddInUndoData(AProject: PProj; var SO: TOldMemoryStream; ALayer: PLayer; AView: PView; AItem: PIndex); overload; // << for separated items in the layer.
    procedure AddInUndoData(var SO: TOldMemoryStream; ASymbol: PSGroup); overload; // << for TSGroup (symbol itself).
    procedure AddInUndoData(var SO: TOldMemoryStream; AProject: PProj; ALayer: PLayer); overload; // << for whole layer.
    procedure AddInUndoData(var SO: TOldMemoryStream; AView: PView; ibTypeOfProjectItem: Byte = ci_ProjectItem); overload; // << for items of layer (is called from such procedure for whole layer).
    procedure AddInUndoData(var SO: TOldMemoryStream; ALayer: PLayer; iItemIndex: Integer); overload; // << for any new added object.

    procedure AddObjectInRedoData(AProject: PProj; AView: PView; bNeedToIncObjCount: Boolean = true); overload;
    procedure AddObjectInRedoData(AProject: PProj; ALayer: PLayer; AItem: PIndex); overload;
    procedure AddObjectInRedoData(AProject: PProj; ALayer: PLayer; iItemIndex: Integer); overload;
    procedure AddObjectInRedoData(AProject: PProj; ALayer: PLayer); overload;
  public
    constructor Create;
    destructor Free;
    function LibraryWasLoaded: Boolean;

    procedure AddProject(AProject: PProj);
    procedure DeleteProject(AProject: PProj);

    function ThereAreDataForRestoring(AProject: PProj): Boolean;
    function GetNextUndoDescription(AProject: PProj): AnsiString;

    function ThereAreDataForRedoing(AProject: PProj): Boolean;
    function GetNextRedoDescription(AProject: PProj): AnsiString;

    function AddObjectInUndoData(AProject: PProj; AView: PView): Boolean; overload;
    function AddObjectInUndoData(AProject: PProj; ALayer: PLayer; AItem: PIndex): Boolean; overload;
    function AddObjectInUndoData(AProject: PProj; ALayer: PLayer; iItemIndex: Integer): Boolean; overload;

    procedure SaveUndoData(AProject: PProj; AnUndoType: TUndoType = utUnknownUNDO);
    procedure OnChangeUndoStepsCount(AProject: PProj; iNewUndoStepsCount: integer);

    procedure SetSignOfMultiOperations(AProject: PProj; bValue: Boolean);

    procedure DeleteSymbol(AProject: PProj; ASymbol: PSGroup);
    procedure AddSymbolMappingEntry(AProject: PProj; sSymbolLibraryName, sSymbolName, sNewSymbolName: AnsiString);
    function SymbolNameInMappingExists(AProject: PProj; sSymbolLibraryName, sSymbolName: AnsiString): Boolean;

    procedure DeleteLayerTables(AProject: PProj; ALayer: PLayer);

    procedure Undo(AProject: PProj);
    procedure Redo(AProject: PProj);

    procedure SetNecessitySaveUndoDataSign(AProject: Pointer; bValue: Boolean);
    procedure GetReadyToReceiveUndoData(AProject: Pointer);

    procedure GetReadyToReceiveDeletedObjectID(AProject: PProj);
    procedure ReceiveDeletedObjectID(AProject: PProj; ALayer: PLayer; AView: PView; AItem: PIndex);
    procedure ExecuteDeleting(AProject: PProj; bShowProgressBarInMainWindow: Boolean = TRUE);
  end;

implementation

uses IniFiles,
  Am_Main, Am_Child, UserIntf, Am_Def, Am_ProjO, Am_Font, Am_DBGra, Am_Text, Am_Obj,
  ResDlg, Am_Dlg6, AM_Point, Am_Sym, StyleDef, XLines, XFills, Am_Ini,
  NumTools, PropCollect, GRTools, SymLib, SymRol, RollUp, MultiLngFile,
  VerMgr, ToolTips, UserUtils, AM_Poly, AM_CPoly, AM_Circl, Am_Admin,
  IDB_SelectInListBox, IDB_Utils, TM_WG_Lib_Manager, DK_AnnotSettings, attachhndl, WinOSInfo;

/////////////////////   Old stream type   //////////////////////////////////

procedure TUndoRedo_OldMemoryStream.SetPointer(APointer: Pointer; Size: LongInt);
begin
  inherited SetPointer(APointer, Size);
end;

function TUndoRedo_OldMemoryStream.GetDataStreamPointer: Pointer;
begin
  RESULT := SELF.Memory;
end;

/////////////////       Interaction (callback) routines       //////////////////

procedure SetProjectModified(AProject: Pointer); stdcall;
begin
  PProj(AProject).Modified := TRUE;
end;

function GetProjectUndoStepsCount(AProject: Pointer): integer; stdcall;
begin
  Result := WinGISMainForm.IDBUndoStepsCount[PProj(AProject)]
end;

function GetProjectFileSize(AProject: Pointer): integer; stdcall;
begin
  Result := PProj(AProject).FSize;
end;

constructor TUndoRedoManager.Create;
var
  IniFile: IniFiles.TIniFile;
  sUndoRedoLibFileName,
    sLanguageFileExtension,
    sUndoRedoVersion,
    sMessage: AnsiString;
begin
  IniFile := IniFiles.TIniFile.Create(OSInfo.WingisIniFileName);
  sLanguageFileExtension := IniFile.ReadString('Settings', 'Language', ''); // Default language is english.
  if Trim(sLanguageFileExtension) = '' then
    sLanguageFileExtension := '044';
  IniFile.Free;
   // Create and init the IDB library.
  sUndoRedoLibFileName := ExtractFilePath(Application.ExeName) + 'UndoRedo.DLL';
   //     sIDBLibFileName := 'd:\IDB_DLL\IDB.DLL';

  SELF.UndoRedoLib := TUndoRedoLib.Create(WinGisMainForm, sUndoRedoLibFileName, sLanguageFileExtension);

   // Connect all callbacks.
  SELF.UndoRedoLib.SetGetProjectUndoStepsCountCallback(GetProjectUndoStepsCount);
  SELF.UndoRedoLib.SetGetProjectFileSizeCallback(GetProjectFileSize);
  bNessesaryToSaveUndoData := true;

  if not FileExists(sUndoRedoLibFileName) then
  begin
      // 'UndoRedo.DLL was not loaded because the file is missing. You can put IDB.DLL (version ' + cs_IDB_NormalVersion + ') to WinGIS directory\n"' + ExtractFilePath(Application.ExeName) + '"\nand try to switch the Internal database on again.'
{      sMessage := SmartString (MlgStringList['IDB', 22], [cs_UndoRedo_NormalVersion, ExtractFilePath (Application.ExeName)]);
      Application.MessageBox (PChar (sMessage), 'WinGIS', MB_OK + MB_DEFBUTTON1 + MB_ICONWARNING);}
  end
  else
  begin
    sUndoRedoVersion := SELF.UndoRedoLib.GetLibraryVersion;
    if (not SELF.UndoRedoLib.LibraryWasLoaded) or
      (sUndoRedoVersion <> cs_UndoRedo_NormalVersion) then
    begin
         // 'UndoRedo.DLL was not loaded because the file has wrong version (You have ' + sIDBVersion + ' but You need ' + cs_IDB_NormalVersion + '). You can replace IDB.DLL file and try to switch the Internal database on again.'
      sMessage := Format(MlgStringList['IDB', 23], [sUndoRedoVersion, cs_UndoRedo_NormalVersion]);
      Application.MessageBox(PChar(sMessage), 'WinGIS', MB_OK + MB_DEFBUTTON1 + MB_ICONWARNING);
    end;
  end;
end;

destructor TUndoRedoManager.Free;
begin
  SELF.UndoRedoLib.Free;
end;

procedure TUndoRedoManager.SetScreenCursor;
begin
  AnOldScreenCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
end;

procedure TUndoRedoManager.RestoreScreenCursor;
begin
  Screen.Cursor := AnOldScreenCursor;
end;

function TUndoRedoManager.LibraryWasLoaded: Boolean;
begin
  RESULT := FALSE;
  if Self <> nil then
    RESULT := (SELF.UndoRedoLib.LibraryWasLoaded) and
      (SELF.UndoRedoLib.GetLibraryVersion = cs_UndoRedo_NormalVersion);
end;

procedure TUndoRedoManager.AddProject(AProject: PProj);
begin
   // At first I recognize whether a project is a symbol editor or not.
  if AProject.SymbolMode <> sym_Project then
    exit;
  SELF.UndoRedoLib.AddProject(TForm(AProject.Parent).Handle, AProject, false);
end;

{Delete project from project definitions list}

procedure TUndoRedoManager.DeleteProject(AProject: PProj);
begin
   // At first I recognize whether a project is a symbol editor or not.
  if AProject.SymbolMode <> sym_Project then
    exit;
  SELF.UndoRedoLib.DeleteProject(AProject);
end;

function TUndoRedoManager.ThereAreDataForRestoring(AProject: PProj): Boolean;
begin
  RESULT := SELF.UndoRedoLib.ThereAreDataForRestoring(AProject);
end;

function TUndoRedoManager.GetNextUndoDescription(AProject: PProj): AnsiString;
begin
  RESULT := SELF.UndoRedoLib.GetNextUndoDescription(AProject);
end;

function TUndoRedoManager.ThereAreDataForRedoing(AProject: PProj): Boolean;
begin
  RESULT := SELF.UndoRedoLib.ThereAreDataForRedoing(AProject);
end;

function TUndoRedoManager.GetNextRedoDescription(AProject: PProj): AnsiString;
begin
  RESULT := SELF.UndoRedoLib.GetNextRedoDescription(AProject);
end;

function TUndoRedoManager.GetObjectTypeByItemIndex(iIndex: Integer): Integer;
begin
  if iIndex < mi_RText then
    RESULT := ot_Text
  else
    if iIndex < mi_BusGraph then
      RESULT := ot_RText
    else
      if iIndex < mi_MesLine then
        RESULT := ot_BusGraph
      else
        if iIndex < mi_Symbol then
          RESULT := ot_MesLine
        else
          if iIndex < mi_Circle then
            RESULT := ot_Symbol
          else
            if iIndex < mi_Arc then
              RESULT := ot_Circle
            else
              if iIndex < mi_Pixel then
                RESULT := ot_Arc
              else
                if iIndex < mi_Group then
                  RESULT := ot_Pixel
                else
                  if iIndex < mi_Spline then
                    RESULT := ot_Group
                  else
                    if iIndex < mi_Poly then
                      RESULT := ot_Spline
                    else
                      if iIndex < mi_CPoly then
                        RESULT := ot_Poly
                      else
                        if iIndex < mi_Image then
                          RESULT := ot_CPoly
                        else
                          if iIndex < mi_OleObj then
                            RESULT := ot_Image
                          else
                            RESULT := ot_OleObj;
end;

procedure TUndoRedoManager.AddInUndoData(AProject: PProj; var SO: TOldMemoryStream; ALayer: PLayer; AView: PView; AItem: PIndex);
var
  ibUndoTypeOfObject: Byte;
  ilLength: LongInt;
  sSymbolLibraryName, sSymbolName: AnsiString;
begin
  if AView <> nil then
    SELF.AddInUndoData(SO, AView, ci_ProjectItemTiedToLayerItem);

  ibUndoTypeOfObject := ci_LayerItem; // 'Cause only a variable is required, not constant value.
  SO.Write(ibUndoTypeOfObject, SizeOf(ibUndoTypeOfObject));
  SO.Write(ALayer.Index, SizeOf(ALayer.Index));
   // if I work with symbol, I need to put also information about name of library to that
   // the symbol belongs and put also the name of the symbol.
  if AView.GetObjType = ot_Symbol then
  begin
//      try // Cadmensky
// ++ Cadmensky
    if PSymbol(AView).SymPtr = nil then
      PSymbol(AView).SetSymPtr(AProject.PInfo^.Symbols);
// -- Cadmensky
    sSymbolLibraryName := PSGroup(PSymbol(AView).SymPtr).GetLibName;
    sSymbolName := PSGroup(PSymbol(AView).SymPtr).GetName;
//      except // Cadmensky
//      end; // Cadmensky
      //         PSymbol(AView).SymPtr := nil; // Commented by me. Ivanoff
  end;
   //      AItem.MPtr := nil; // Commented by me. Ivanoff
  SO.Put(AItem);

  if AView.GetObjType = ot_Symbol then
  begin
      // Put information about symbol.
      // Symbol's library name.
    ilLength := Length(sSymbolLibraryName);
    SO.Write(ilLength, SizeOf(ilLength));
    SO.Write(Pointer(sSymbolLibraryName)^, ilLength);
      // Symbol's name.
    ilLength := Length(sSymbolName);
    SO.Write(ilLength, SizeOf(ilLength));
    SO.Write(Pointer(sSymbolName)^, ilLength);
  end;
end;

{ This procedure puts symbol from library in TBufStream.}

procedure TUndoRedoManager.AddInUndoData(var SO: TOldMemoryStream; ASymbol: PSGroup);
var
  ibRealTypeOfObject,
    ibUndoTypeOfObject: Byte;
begin
  ibRealTypeOfObject := ot_Symbol;
  SO.Write(ibRealTypeOfObject, SizeOf(ibRealTypeOfObject));
  ibUndoTypeOfObject := ci_SymbolsGroup;
  SO.Write(ibUndoTypeOfObject, SizeOf(ibUndoTypeOfObject));
  SO.Put(ASymbol);
end;

{ This procedure puts deleted layer in TBufStream.}

procedure TUndoRedoManager.AddInUndoData(var SO: TOldMemoryStream; AProject: PProj; ALayer: PLayer);
var
  ibUndoTypeOfObject,
    ibRealTypeOfObject: Byte;
  iI: Integer;
begin
  ibRealTypeOfObject := ot_Layer;
  SO.Write(ibRealTypeOfObject, SizeOf(ibRealTypeOfObject));
   // I put all project items of this layer before I'll put the layer itself.
   // I'll read them in the same way.
  for iI := 0 to ALayer.Data.GetCount - 1 do
      // I use here AddToUndoData(PView) overload variant of AddToUndoData.
    SELF.AddInUndoData(SO, PLayer(AProject.PInfo.Objects).IndexObject(AProject.PInfo, ALayer.Data.At(iI)));
  ibUndoTypeOfObject := ci_Layer; // 'Cause only a variable is required not constant value.
  SO.Write(ibUndoTypeOfObject, SizeOf(ibUndoTypeOfObject));
  SO.Put(ALayer);
end;

{ This procedure puts deleted object in TBufStream.
  (TView that can be found using TIndex.MPtr pointer.)}

procedure TUndoRedoManager.AddInUndoData(var SO: TOldMemoryStream; AView: PView; ibTypeOfProjectItem: Byte = ci_ProjectItem);
var
  ibUndoTypeOfObject: Byte;
begin
  ibUndoTypeOfObject := ibTypeOfProjectItem;
  SO.Write(ibUndoTypeOfObject, SizeOf(ibUndoTypeOfObject));
  SO.Put(AView);
end;

{ This procedure has the following purpose. if the user added new object, he'll be able to delete it
  (UnInsert operation). Therefore, during add operation I put in the Undo data an index of new added
  item. if later the user wants to delete this new added item, I read this index and delete from
  required layer this item. }

procedure TUndoRedoManager.AddInUndoData(var SO: TOldMemoryStream; ALayer: PLayer; iItemIndex: Integer);
var
  ibUndoTypeOfObject: Byte;
  ilLayerIndex: LongInt;
begin
  ibUndoTypeOfObject := ci_NewObject; // 'Cause only a variable is required not constant value.
  SO.Write(ibUndoTypeOfObject, SizeOf(ibUndoTypeOfObject));
  if ALayer <> nil then
    ilLayerIndex := ALayer.Index
  else
    ilLayerIndex := 0;
  SO.Write(ilLayerIndex, SizeOf(ilLayerIndex));
  SO.Write(iItemIndex, SizeOf(iItemIndex));
end;

procedure TUndoRedoManager.AddObjectInRedoData(AProject: PProj; AView: PView; bNeedToIncObjCount: Boolean = true);
var
  ibRedoTypeOfObject,
    ibRealTypeOfObject: Byte;
  SO: TUndoRedo_OldMemoryStream;
begin
  ibRedoTypeOfObject := ci_ProjectItem;
  SO := TUndoRedo_OldMemoryStream.Create;
//  SO.Init(0, 0);
  try
    ibRealTypeOfObject := AView.GetObjType;
    SO.Write(ibRealTypeOfObject, SizeOf(ibRealTypeOfObject));
    SO.Write(ibRedoTypeOfObject, SizeOf(ibRedoTypeOfObject));
    SO.Put(AView);
    SELF.UndoRedoLib.PutDataInRedo(AProject, SO.GetDataStreamPointer, SO.GetSize, bNeedToIncObjCount);
  finally
    SO.Free;
//    SO.Done;
  end;
end;

procedure TUndoRedoManager.AddObjectInRedoData(AProject: PProj; ALayer: PLayer; AItem: PIndex);
var
  ibRedoTypeOfObject,
    ibRealTypeOfObject: Byte;
  SO: TUndoRedo_OldMemoryStream;
begin
  ibRedoTypeOfObject := ci_LayerItem;
  SO := TUndoRedo_OldMemoryStream.Create;
//  SO.Init(0, 0);
  try
    ibRealTypeOfObject := AItem.GetObjType;
    SO.Write(ibRealTypeOfObject, SizeOf(ibRealTypeOfObject));
    SO.Write(ibRedoTypeOfObject, SizeOf(ibRedoTypeOfObject));
    SO.Write(ALayer.Index, SizeOf(ALayer.Index));
      //        AItem.MPtr := nil; // Commented by me. Ivanoff
    SO.Put(AItem);
    SELF.UndoRedoLib.PutDataInRedo(AProject, SO.GetDataStreamPointer, SO.GetSize, true);
  finally
    SO.Free;
//    SO.Done;
  end;
end;

procedure TUndoRedoManager.AddObjectInRedoData(AProject: PProj; ALayer: PLayer; iItemIndex: Integer);
var
  ibRedoTypeOfObject,
    ibRealTypeOfObject: Byte;
  SO: TUndoRedo_OldMemoryStream;
begin
  ibRedoTypeOfObject := ci_DeletedObject;
  SO := TUndoRedo_OldMemoryStream.Create;
//  SO.Init(0, 0);
  try
    ibRealTypeOfObject := SELF.GetObjectTypeByItemIndex(iItemIndex);
    SO.Write(ibRealTypeOfObject, SizeOf(ibRealTypeOfObject));
    SO.Write(ibRedoTypeOfObject, SizeOf(ibRedoTypeOfObject));
    SO.Write(ALayer.Index, SizeOf(ALayer.Index));
    SO.Write(iItemIndex, SizeOf(iItemIndex));
    SELF.UndoRedoLib.PutDataInRedo(AProject, SO.GetDataStreamPointer, SO.GetSize, true);
  finally
    SO.Free;
//    SO.Done;
  end;
end;

procedure TUndoRedoManager.AddObjectInRedoData(AProject: PProj; ALayer: PLayer);
var
  ibRedoTypeOfObject,
    ibRealTypeOfObject: Byte;
  SO: TUndoRedo_OldMemoryStream;
  iLayerIndex: Integer;
begin
  ibRedoTypeOfObject := ci_Layer;
  SO := TUndoRedo_OldMemoryStream.Create;
//  SO.Init(0, 0);
  try
    ibRealTypeOfObject := ot_Layer;
    SO.Write(ibRealTypeOfObject, SizeOf(ibRealTypeOfObject));
    SO.Write(ibRedoTypeOfObject, SizeOf(ibRedoTypeOfObject));
    iLayerIndex := -1;
    SO.Write(iLayerIndex, SizeOf(iLayerIndex));
    SO.Put(ALayer);
    SELF.UndoRedoLib.PutDataInRedo(AProject, SO.GetDataStreamPointer, SO.GetSize, true);
  finally
    SO.Free;
//    SO.Done;
  end;
end;

function TUndoRedoManager.AddObjectInUndoData(AProject: PProj; AView: PView): Boolean;
var
  SO: TUndoRedo_OldMemoryStream;
  ibRealTypeOfObject: Byte;
begin
  RESULT := FALSE;
   // At first I recognize whether a project is a symbol editor or not.
  if AProject.SymbolMode <> sym_Project then
    exit;
  SO := TUndoRedo_OldMemoryStream.Create;
//  SO.Init(0, 0);
  ibRealTypeOfObject := AView.GetObjType;
  SO.Write(ibRealTypeOfObject, SizeOf(ibRealTypeOfObject));
  try
    SELF.AddInUndoData(TOldMemoryStream(SO), AView);
    SELF.UndoRedoLib.PutDataInUndo(AProject, SO.GetDataStreamPointer, SO.GetSize);
  finally
    SO.Free;
//    SO.Done;
  end;
  RESULT := TRUE;
end;

function TUndoRedoManager.AddObjectInUndoData(AProject: PProj; ALayer: PLayer; AItem: PIndex): Boolean;
var
  SO: TUndoRedo_OldMemoryStream;
  ibRealTypeOfObject: Byte;
  AView: PView;
begin
  RESULT := FALSE;
   // At first I recognize whether a project is a symbol editor or not.
  if AProject.SymbolMode <> sym_Project then
    exit;
  SO := TUndoRedo_OldMemoryStream.Create;
//  SO.Init(0, 0);
  ibRealTypeOfObject := AItem.GetObjType;
  SO.Write(ibRealTypeOfObject, SizeOf(ibRealTypeOfObject));
  try
    AView := ALayer.IndexObject(AProject.PInfo, AItem);
    if AView <> nil then
    begin
      SELF.AddInUndoData(AProject, TOldMemoryStream(SO), ALayer, AItem.MPtr, AItem);
      SELF.UndoRedoLib.PutDataInUndo(AProject, SO.GetDataStreamPointer, SO.GetSize);
    end;
  finally
    SO.Free;
//    SO.Done;
  end;
  RESULT := TRUE;
end;

{ This function puts an index of new added object in the undo data of the IDB. This
  information can be used later when the user will want to do the undo operation.}

function TUndoRedoManager.AddObjectInUndoData(AProject: PProj; ALayer: PLayer; iItemIndex: Integer): Boolean;
var
  SO: TUndoRedo_OLDMemoryStream;
  ibRealTypeOfObject: Byte;
begin
  RESULT := FALSE;
  if not WinGisMAinForm.WeAreUsingUndoFunction[AProject] then
    exit;
   // At first I recognize whether a project is a symbol editor or not.
  if AProject.SymbolMode <> sym_Project then
    exit;
  SO := TUndoRedo_OldMemoryStream.Create;
//  SO.Init(0, 0);
  ibRealTypeOfObject := GetObjectType(iItemIndex);
  SO.Write(ibRealTypeOfObject, SizeOf(ibRealTypeOfObject));
  try
    SELF.AddInUndoData(TOldMemoryStream(SO), ALayer, iItemIndex);
    SELF.UndoRedoLib.PutDataInUndo(AProject, SO.GetDataStreamPointer, SO.GetSize);
  finally
    SO.Free;
//    SO.Done;
  end;
  RESULT := TRUE;
end;

procedure TUndoRedoManager.SaveUndoData(AProject: PProj; AnUndoType: TUndoType = utUnknownUNDO);
begin
   // This check was added by me because call from TIDB_ViewForm can't has an access to
   // WinGisMainForm... There I call this function but here I check a necessity to call it
   // indeed.
  if not WinGisMainForm.WeAreUsingUndoFunction[AProject] then
    exit;
   // At first I recognize whether a project is a symbol editor or not.
  if AProject.SymbolMode <> sym_Project then
    exit;
  if not SELF.bNessesaryToSaveUndoData then
    exit;
  SELF.UndoRedoLib.SaveUndoData(AProject, AnUndoType);

  if AnUndoButton <> nil then
    AnUndoButton.Hint := SELF.UndoRedoLib.GetNextUndoDescription(AProject);
end;

procedure TUndoRedoManager.SetSignOfMultiOperations(AProject: PProj; bValue: Boolean);
begin
   // At first I recognize whether a project is a symbol editor or not.
  if AProject.SymbolMode <> sym_Project then
    exit;
   {     GetProjectDefs(AProject, pPD);
        if pPD.IDB_UndoMan <> nil then
           pPD.IDB_UndoMan.SetMultiSign(bValue);}
end;

procedure TUndoRedoManager.DeleteSymbol(AProject: PProj; ASymbol: PSGroup);

  function GetSymLibInfo: TSymLibInfo;
  var
    iI: Integer;
    RollUp: TRollupForm;
  begin
    RESULT := nil;
      // Find RollupForm with symbols. All data about attached symbols libraries are contained in it.
    for iI := 0 to UserInterface.RollupCount - 1 do
    begin
      Rollup := UserInterface.Rollups[iI];
      if Rollup is TSymbolRollupForm then
      begin
            // Get information about attached symbol libraries.
        RESULT := TSymbolRollupForm(RollUp).GetSymLibInfo;
        exit;
      end;
    end; // for iI end
  end;

  function GenerateNewSymbolName(sSymbolLibraryName, sSymbolName: AnsiString): AnsiString;
  var
    iI: Integer;
    AProjectLibrarySymbols: PSymbols;
  begin
    RESULT := sSymbolName + ' - ' + sSymbolLibraryName;
    AProjectLibrarySymbols := GetSymLibInfo.GetLib(0);
    if AProjectLibrarySymbols = nil then
      exit;
    iI := 0;
    while AProjectLibrarySymbols.GetSymbol(RESULT, sSymbolLibraryName) <> nil do
    begin
      Inc(iI);
      RESULT := sSymbolName + '(' + IntToStr(iI) + ') - ' + sSymbolLibraryName;
    end;
  end;

var
  SO: TUndoRedo_OldMemoryStream;
begin
   // At first I recognize whether a project is a symbol editor or not.
  if AProject.SymbolMode <> sym_Project then
    exit;
  if not WinGisMainForm.WeAreUsingUndoFunction[AProject] then
    exit;
  SO := TUndoRedo_OldMemoryStream.Create;
//  SO.Init(0, 0);
  try
    SELF.UndoRedoLib.GetReadyToReceiveUndoData(AProject);
    SELF.AddInUndoData(TOldMemoryStream(SO), ASymbol);
    SELF.UndoRedoLib.PutDataInUndo(AProject, SO.GetDataStreamPointer, SO.GetSize);
    SELF.UndoRedoLib.SaveUndoData(AProject, utUnDelete);
  finally
    SO.Free;
//    SO.Done;
  end;
  SELF.UndoRedoLib.AddSymbolMappingEntry(AProject, ASymbol.GetLibName, ASymbol.GetName, GenerateNewSymbolName(ASymbol.GetLibName, ASymbol.GetName));
end;

procedure TUndoRedoManager.OnChangeUndoStepsCount(AProject: PProj; iNewUndoStepsCount: integer);
begin
  if AProject.SymbolMode <> sym_Project then
    exit;
  if not WinGisMainForm.WeAreUsingUndoFunction[AProject] then
    exit;
  SELF.UndoRedoLib.OnChangeUndoStepsCount(AProject, iNewUndoStepsCount);
end;

procedure TUndoRedoManager.AddSymbolMappingEntry(AProject: PProj; sSymbolLibraryName, sSymbolName, sNewSymbolName: AnsiString);
begin
  if AProject.SymbolMode <> sym_Project then
    exit;
  if not WinGisMainForm.WeAreUsingUndoFunction[AProject] then
    exit;
  SELF.UndoRedoLib.AddSymbolMappingEntry(AProject, sSymbolLibraryName, sSymbolName, sNewSymbolName);
end;

function TUndoRedoManager.SymbolNameInMappingExists(AProject: PProj; sSymbolLibraryName, sSymbolName: AnsiString): Boolean;
begin
  RESULT := FALSE;
  if AProject.SymbolMode <> sym_Project then
    exit;
  SELF.UndoRedoLib.SymbolNameExists(AProject, sSymbolLibraryName, sSymbolName);
end;

procedure TUndoRedoManager.DeleteLayerTables(AProject: PProj; ALayer: PLayer);
var
  SO: TUndoRedo_OldMemoryStream;
begin
   // At first I recognize whether a project is a symbol editor or not.
  if AProject.SymbolMode <> sym_Project then
    exit;

  if (WinGisMainForm.WeAreUsingUndoFunction[AProject]) then
  begin
    SO := TUndoRedo_OldMemoryStream.Create;
//  SO.Init(0, 0);
    try
      SELF.AddInUndoData(TOldMemoryStream(SO), AProject, ALayer);
      SELF.UndoRedoLib.PutDataInUndo(AProject, SO.GetDataStreamPointer, SO.GetSize);
    finally
      SO.Free;
//    SO.Done;
    end;
    SELF.SaveUndoData(AProject, utUnDelete);
  end;
end;

procedure TUndoRedoManager.Undo(AProject: PProj);

  function GetPositionOfSymbolsSet(ASymLibInfo: TSymLibInfo; sLibraryName: AnsiString): Integer;
  var
    iI: Integer;
  begin
    RESULT := 0;
    for iI := 0 to ASymLibInfo.LibCount - 1 do
    begin
      if AnsiUpperCase(ASymLibInfo.GetLibName(iI)) = sLibraryName then
      begin
            // Get position of symbols of required library.
        RESULT := iI;
        exit;
      end;
    end; // for iI end
  end;

var
  RollUp: TRollUpForm;

  procedure FindSymbolRollupForm;
  var
    iI: Integer;
  begin
    RollUp := nil;
      // Find RollupForm with symbols. All data about attached symbols libraries are contained in it.
    for iI := 0 to UserInterface.RollupCount - 1 do
    begin
      Rollup := UserInterface.Rollups[iI];
      if Rollup is TSymbolRollupForm then
        exit;
    end; // for iI end
  end;

type
   { This record contains a pointer to layer and an array with some IDs of items that belongs to this layer. }
  TrecLayerItems = record
    ALayer: pLayer; // Pointer to layer
    arObjectsIDs: array of Integer; // Array with IDs of layer objects
  end;
var
  iSize, iPos, iP, iI, iData: Integer;
  ABuffer: Pointer;
  SO: TUndoRedo_OldMemoryStream;
  ibTypeOfObject, ibData: Byte;
  ilLayerIndex, ilItemIndex, ilLength: LongInt;
  ALayer: PLayer;
  AView, APrevView: PView;
  AIndex, APrevIndex: PIndex;
  ASymbol, AnExistedSymbol: PSGroup;
  RedrawRect: TDRect;
  arLayers: array of TrecLayerItems;
  bSomeItemsWereRestored: Boolean;
  sSymbolLibraryName,
    sSymbolName: AnsiString;
  ASymLibInfo: TSymLibInfo;
  ASymbolsSet: PSymbols;
  ASymInfo: TSymInfo;
  AnUndoType: TUndoType;
  ARedoType: TRedoType;
  bExists: boolean;
// ++ Cadmensky IDB Version 2.3.8
  bOldSnapItemMustBeRestored: Boolean;
// -- Cadmensky IDB Version 2.3.8
begin
  try
      // At first I recognize whether a project is a symbol editor or not.
    if AProject.SymbolMode <> sym_Project then
      exit;
  except
    exit;
  end;

  ABuffer := nil;
  iSize := 0;
   // if we can't get undo data, we have to exit.
  if not SELF.UndoRedoLib.GetUndoData(AProject, ABuffer, iSize, AnUndoType) then
    exit;
  if (ABuffer = nil) or (iSize = 0) then
    exit;

  SetScreenCursor;
  try
    StatusBar.ProgressPanel := TRUE;
    StatusBar.Progress := 0;
      // 'Undo. Step 1.'
    StatusBar.ProgressText := MlgStringList['IDB', 1];
    UserInterface.Update([uiStatusBar]);

    Application.ProcessMessages;
    Application.HandleMessage;

    FindSymbolRollupForm;

// ++ Cadmensky IDB
    if WingisMainForm.WeAreUsingTheIDB[AProject] then
    begin
      WingisMainForm.IDB_Man.GetReadyToReceiveIDsOfObjectsToBeRestored(AProject);
      WingisMainForm.IDB_Man.GetReadyToReceiveIDsOfObjectToBeDeleted_FromWG(AProject);
    end;
// -- Cadmensky IDB
    SELF.UndoRedoLib.GetReadyToReceiveRedoData(AProject);

    SO := TUndoRedo_OldMemoryStream.Create;
//  SO.Init(0, 0);
    SO.SetPointer(ABuffer, iSize);
    SO.Seek(0, soFromBeginning);
    try
      iPos := 0;
      while (iPos < iSize) and (iPos <> -1) do
      begin
            // Read type of Undo object. (Layer, layer items, project items, etc).
        SO.Read(ibTypeOfObject, SizeOf(ibTypeOfObject));
        case ibTypeOfObject of
          ci_LayerItem:
            begin // TIndex
              bOldSnapItemMustBeRestored := false;
              SO.Read(ilLayerIndex, SizeOf(ilLayerIndex));
                     // Try to detect a layer to that restored item belongs. if this detection fails,
                     // get the toppest layer in the project.
              ALayer := AProject.Layers.IndexToLayer(ilLayerIndex);
              if ALayer = nil then
                ALayer := AProject.Layers.GetTopLayer;
              AIndex := Pointer(SO.Get);
              if (AIndex <> nil) then
              begin
                APrevIndex := ALayer.HasIndexObject(AIndex.Index);
                if (APrevIndex <> nil) then
                begin
// ++ Cadmensky IDB Version 2.3.8
                  if AProject.OldSnapItem = APrevIndex then
                    bOldSnapItemMustBeRestored := true;
// -- Cadmensky IDB Version 2.3.8
                           // The layer item exists. This is UnChange operation. Put it into Redo.
                  SELF.AddObjectInRedoData(AProject, ALayer, APrevIndex);
                  ALayer.Data.Search(APrevIndex, iP);
                  ALayer.Data.AtFree(iP);
                end
                else
                           // The layer item doesn't exist. It is UnDelete operation.
                           //  During Redo will be enough to delete it.
                  SELF.AddObjectInRedoData(AProject, ALayer, AIndex.Index);
              end;
                     // Now insert TIndex in a layer without painting of it.
              ALayer.InsertObject(AProject.PInfo, AIndex, FALSE);
// ++ Cadmensky IDB Version 2.3.8
              if bOldSnapItemMustBeRestored then
              begin
                AProject.OldSnapItem := AIndex;
                AProject.OldSnapItem^.SetState(sf_Selected, true);
              end;
// -- Cadmensky IDB Version 2.3.8
                     // And now I have to unmarked database record as just deleted. for this task I fill
                     // an array and later I'll work with this array.
              if WinGISMainForm.WeAreUsingTheIDB[AProject] then
                WingisMainForm.IDB_Man.ReceiveIDOfObjectToBeRestored(AProject, ALayer, AIndex.Index);

              if AView.GetObjType = ot_Symbol then
              begin
                        // Now I shall read the name of library to that the symbol belongs.
                SO.Read(ilLength, SizeOf(ilLength));
                SetLength(sSymbolLibraryName, ilLength);
                SO.Read(Pointer(sSymbolLibraryName)^, ilLength);
                        // Now I read the name of the symbol.
                SO.Read(ilLength, SizeOf(ilLength));
                SetLength(sSymbolName, ilLength);
                SO.Read(Pointer(sSymbolName)^, ilLength);
                        // Try to find the symbol in required library.
                AnExistedSymbol := PSymbols(AProject.PInfo.Symbols).GetSymbol(sSymbolName, sSymbolLibraryName);
                if AnExistedSymbol <> nil then
                begin
                  PSymbol(AView).SymPtr := AnExistedSymbol;
                  Inc(AnExistedSymbol.UseCount);
                end
                else
                begin
                           // if the symbol not found, I try to found it using symbol remapper.
                  sSymbolName := SELF.UndoRedoLib.GetSymbolActualName(AProject, sSymbolLibraryName, sSymbolName);
                  sSymbolLibraryName := '';
                  AnExistedSymbol := PSymbols(AProject.PInfo.Symbols).GetSymbol(sSymbolName, sSymbolLibraryName);
                  if AnExistedSymbol <> nil then
                  begin
                    PSymbol(AView).SymPtr := AnExistedSymbol;
                    Inc(AnExistedSymbol.UseCount);
                  end;
                end;
              end;
            end;
          ci_Layer:
            begin // TLayer
              ALayer := Pointer(SO.Get);
                     // I insert restored layer as the toppest layer.
              AProject.Layers.LData.AtInsert(1, ALayer);
              SELF.AddObjectInRedoData(AProject, ALayer);
              ALayer.Owner := AProject.Layers;
              AProject.Layers.NumberLayers;
              for iI := 0 to ALayer.Data.GetCount - 1 do
                ALayer.IndexObject(AProject.PInfo, ALayer.Data.At(iI));
              UserInterface.Update([uiLayers]);
            end;
          ci_ProjectItem,
            ci_ProjectItemTiedToLayerItem:
            begin // TView
              AView := Pointer(SO.Get);
                     // Insert the TView into the project's objects list if it is necessity.
              if (AView <> nil) then
              begin
                APrevView := PView(PLayer(AProject.PInfo.Objects).HasIndexObject(AView.Index));
                if (APrevView = nil) then
                  PLayer(AProject.PInfo.Objects).Data^.Insert(AView)
                else
                begin

                  if ibTypeOfObject = ci_ProjectItem then
                    SELF.AddObjectInRedoData(AProject, APrevView);

                  PLayer(AProject.PInfo.Objects).Data.Search(APrevView, iP);
                  PLayer(AProject.PInfo.Objects).Data.AtFree(iP);
                  PLayer(AProject.PInfo.Objects).Data.Insert(AView);
                  for iP := 0 to AProject.Layers.LData.Count - 1 do
                  begin
                    ALayer := PLayer(AProject.Layers.LData.At(iP));
                    AIndex := ALayer.HasIndexObject(AView.Index);
                    if AIndex = nil then
                      CONTINUE;
                    AIndex.MPtr := AView;

                    RedrawRect.CorrectByRect(AView.ClipRect);
                    AProject.UpdateClipRect(AView);
                    AIndex.ClipRect.InitByRect(AView.ClipRect);
                    RedrawRect.CorrectByRect(AView.ClipRect);
                    AProject.SelAllRect.CorrectByRect(AView.ClipRect);
                  end; // for iP end
                end;
              end;
            end;
          ci_SymbolsGroup:
            begin // TSGroup
              ASymbol := Pointer(SO.Get);
              SELF.AddObjectInRedoData(AProject, ASymbol);
              ASymLibInfo := TSymbolRollupForm(RollUp).GetSymLibInfo;
              if ASymLibInfo <> nil then
              begin
                iI := GetPositionOfSymbolsSet(ASymLibInfo, AnsiUpperCase(ASymbol.GetLibName));
                        { if required library was switched off, I have put the symbol into project symbols
                          and I have to correwct library name of the symbol. }
                if (ASymbol.GetLibName <> '') and (iI = 0) then
                begin
                           { if the external library of the symbol was switched off by the user, I put symbol name
                             and name of required external library in the Mapper and create new symbol name:
                             SymbolName_-_SymbolExternalLibraryName. then I put the symbol into project's symbol
                             library. information from the Mapper will be used when the user will restore
                             project's item that is purposed to display the symbol (I meant PSymbol object). }
                  ASymbol.SetName(SELF.UndoRedoLib.GetSymbolActualName(AProject, ASymbol.GetLibName, ASymbol.GetName));
                  ASymbol.SetLibName('');
                end;
                ASymbolsSet := ASymLibInfo.GetLib(iI);
                ASymInfo := ASymLibInfo.GetSymInfo(iI);
                        // Insert symbol into symbols of required library.
                if (ASymbolsSet <> nil)
                  and
                  (ASymbolsSet.GetSymbol(ASymbol.GetName, ASymbol.GetLibName) = nil) then
                  ASymbolsSet.InsertLibSymbolAfterUndo(ASymbol.GetLibName, ASymbol);
                        // Insert symbol into loaded library's symbols.
                if (ASymInfo <> nil)
                  and
                  (ASymInfo.IndexOf(ASymbol.GetName) < 0) then
                  ASymInfo.AddSym(ASymbol);
                        // Update RollUpForm with symbols.
                if AnsiUpperCase(ASymbol.GetLibName) = AnsiUpperCase(AProject.ActSymLib) then
                  TSymbolRollupForm(RollUp).UpdateLibChange;
              end;
            end;
          ci_NewObject:
            begin // An object was added and the user wants to delete it now.
              SO.Read(ilLayerIndex, SizeOf(ilLayerIndex));
              SO.Read(ilItemIndex, SizeOf(ilItemIndex));
                     // Which layer I have to do on?
              if ilLayerIndex > 0 then
                ALayer := AProject.Layers.IndexToLayer(ilLayerIndex)
              else
                ALayer := nil;
                     // if the layer doesnt exist, I think ilItemIndex is an index of new added layer and I have to delete it.
              if ALayer <> nil then
              begin
                AIndex := ALayer.HasIndexObject(ilItemIndex);
                        // if the item doesn't exist, I do nothing, too.
                if AIndex <> nil then
                begin
// ++ Cadmensky
                  AView := ALayer.IndexObject(AProject.PInfo, AIndex);
                  SELF.AddObjectInRedoData(AProject, AView, false);
// -- Cadmensky
                  SELF.AddObjectInRedoData(AProject, ALayer, AIndex);
                           // First of all I have to deselect this item.
                  AProject.Layers.SelLayer.DeSelect(AProject.PInfo, AIndex);
                  try
                              { I set the Flag that means there is no necissity to put data about objects to be
                                deleted in Undo data. Procedures that put these data will be called from
                                TLayer when it will delete new added object. After finish deleting operation
                                I set the Flag in FALSE state. }
                    SELF.UndoRedoLib.Set_ThereIsNoNecessityToPutDataInUndo_Sign(AProject, TRUE);
// ++ Cadmensky
                    bExists := AProject.Layers^.ExistsObject(AIndex) > 1;
// -- Cadmensky
                    ALayer.DeleteIndex(AProject.PInfo, AIndex);
// ++ Cadmensky
                    if not bExists then
                      PLayer(AProject.PInfo^.Objects)^.DeleteIndex(AProject.PInfo, AView);
// -- Cadmensky
                    SELF.UndoRedoLib.Set_ThereIsNoNecessityToPutDataInUndo_Sign(AProject, FALSE);
                  except
                    SELF.UndoRedoLib.Set_ThereIsNoNecessityToPutDataInUndo_Sign(AProject, FALSE);
                  end;
                end;
              end
              else
              begin
                ALayer := AProject.Layers.IndexToLayer(ilItemIndex);
                if ALayer <> nil then
                begin
                  try
                    SELF.UndoRedoLib.Set_ThereIsNoNecessityToPutDataInUndo_Sign(AProject, TRUE);
                    SELF.AddObjectInRedoData(AProject, ALayer);
                    AProject.Layers.DeleteLayer(ALayer);
                    SELF.UndoRedoLib.Set_ThereIsNoNecessityToPutDataInUndo_Sign(AProject, FALSE);
                  except
                    SELF.UndoRedoLib.Set_ThereIsNoNecessityToPutDataInUndo_Sign(AProject, FALSE);
                  end;
                end;
              end;
            end;
        end; // case ishTypeOfObject end
        iPos := SO.Position;

        StatusBar.Progress := 100 * iPos / iSize;
        Application.ProcessMessages;

      end; // while iPos < iSize end
    finally
      FreeMem(ABuffer, iSize);
      SO.Free;
//    SO.Done;
         // 'Undo. Step 2.'
      StatusBar.ProgressText := MlgStringList['IDB', 2];
      if WinGISMainForm.WeAreUsingTheIDB[AProject] then
        WinGISMainForm.IDB_Man.ExecuteRestoring(AProject);
      Application.ProcessMessages;
      if WinGISMainForm.WeAreUsingTheIDB[AProject] then
        WinGISMainForm.IDB_Man.DeleteObjects_FromWG(AProject);
      Application.ProcessMessages;
      StatusBar.ProgressPanel := FALSE;
    end;
    AProject.PInfo.RedrawScreen(TRUE);
  finally
    case AnUndoType of
      utUnInsert: ARedoType := rtInsert;
      utUnDelete: ARedotype := rtDelete;
      utUnChange: ARedoType := rtChange;
      utUnMove: ARedoType := rtMove;
      utUnResize: ARedoType := rtResize;
      utUnTransform: ARedoType := rtTransform;
    else
      ARedoType := rtUnknownREDO;
    end; // case end

    SELF.UndoRedoLib.SaveRedoData(AProject, ARedoType);

    if AnUndoButton <> nil then
      AnUndoButton.Hint := SELF.UndoRedoLib.GetNextUndoDescription(AProject);
    if ARedoButton <> nil then
      ARedoButton.Hint := SELF.UndoRedoLib.GetNextRedoDescription(AProject);
  end;
   // Restore possible attachements of just restored TViews.
   // This cycle was taken from TProj.Load (it was created by Yuri Glukhov).
  for iI := 0 to PLayer(AProject.PInfo.Objects).Data.GetCount - 1 do
  begin
    AView := PView(PLayer(AProject.PInfo.Objects).Data.At(iI));
    AView.GetObjMaster(AProject.PInfo);
  end; // for iI end

  SetProjectModified(AProject);
  if WinGISMainForm.WeAreUsingTheIDB[AProject] then
    WinGISMainForm.IDB_Man.RefreshDataWindows(AProject);
  RestoreScreenCursor;
end;

procedure TUndoRedoManager.Redo(AProject: PProj);
var
  ABuffer: Pointer;
  iSize, iPos, iP: Integer;
  ibTypeOfObject: Byte;
  SO: TUndoRedo_OldMemoryStream;
  iItemIndex, iLayerIndex: Integer;
  ALayer: PLayer;
  APrevIndex, AIndex: PIndex;
  AView, APrevView: PView;
  RedrawRect: TDRect;
  AnUndoType: TUndoType;
  ARedoType: TRedoType;

  bTheIndexIsASymbol: Boolean;
  ASymbol: PView;
  bExists: boolean;
// ++ Cadmensky IDB Version 2.3.8
  bOldSnapItemMustBeRestored: Boolean;
// -- Cadmensky IDB Version 2.3.8
begin
  try
      // At first I recognize whether a project is a symbol editor or not.
    if AProject.SymbolMode <> sym_Project then
      exit;
  except
    exit;
  end;

  ABuffer := nil;
  iSize := 0;
   // if we can't get redo data, we have to exit.
  if not SELF.UndoRedoLib.GetRedoData(AProject, ABuffer, iSize, ARedoType) then
    exit;
  if (ABuffer = nil) or (iSize = 0) then
    exit;

  SetScreenCursor;
  try
    StatusBar.ProgressPanel := TRUE;
    StatusBar.Progress := 0;
      // 'Redo...'
    StatusBar.ProgressText := MlgStringList['IDB', 14];
    UserInterface.Update([uiStatusBar]);

    Application.ProcessMessages;
    Application.HandleMessage;

    SELF.UndoRedoLib.GetReadyToReceiveUndoData(AProject);
    if WinGISMainForm.WeAreUsingTheIDB[AProject] then
    begin
      WinGISMainForm.IDB_Man.GetReadyToReceiveIDsOfObjectToBeDeleted_FromWG(AProject);
      WinGISMainForm.IDB_Man.GetReadyToReceiveIDsOfObjectsToBeRestored(AProject);
    end;

    SO := TUndoRedo_OldMemoryStream.Create;
//  SO.Init(0, 0);
    SO.SetPointer(ABuffer, iSize);
    SO.Seek(0, soFromBeginning);

    iPos := 0;
    while (iPos < iSize) and (iPos <> -1) do
    begin
         // Read type of Undo object. (Layer, layer items, project utems etc).
      SO.Read(ibTypeOfObject, SizeOf(ibTypeOfObject));
      case ibTypeOfObject of
        ci_Layer:
          begin
            SO.Read(iLayerIndex, SizeOf(iLayerIndex));
            ALayer := AProject.Layers.IndexToLayer(iLayerIndex);
            if ALayer <> nil then
              AProject.Layers.DeleteLayer(ALayer)
            else
            begin
              ALayer := Pointer(SO.Get);
                     // I insert restored layer as the toppest layer.
              AProject.Layers.LData.AtInsert(1, ALayer);
              SELF.AddObjectInUndoData(AProject, nil, ALayer.Index);
              ALayer.Owner := AProject.Layers;
              AProject.Layers.NumberLayers;
              for iP := 0 to ALayer.Data.GetCount - 1 do
                ALayer.IndexObject(AProject.PInfo, ALayer.Data.At(iP));
              UserInterface.Update([uiLayers]);
            end;
          end;
        ci_LayerItem:
          begin
// ++ Cadmensky IDB Version 2.3.8
            bOldSnapItemMustBeRestored := false;
// -- Cadmensky IDB Version 2.3.8
            SO.Read(iLayerIndex, SizeOf(iLayerIndex));
            ALayer := AProject.Layers.IndexToLayer(iLayerIndex);
            AIndex := Pointer(SO.Get);
            APrevIndex := ALayer.HasIndexObject(AIndex.Index);
            if APrevIndex <> nil then
            begin
// ++ Cadmensky IDB Version 2.3.8
              if AProject.OldSnapItem = APrevIndex then
                bOldSnapItemMustBeRestored := true;
// -- Cadmensky IDB Version 2.3.8
              SELF.AddObjectInUndoData(AProject, ALayer, APrevIndex);
              ALayer.Data.Search(APrevIndex, iP);
              ALayer.Data.AtFree(iP);
            end
            else
              SELF.AddObjectInUndoData(AProject, ALayer, AIndex.Index);
            if WinGISMainForm.WeAreUsingTheIDB[AProject] then
              WinGISMainForm.IDB_Man.ReceiveIDOfObjectToBeRestored(AProject, ALayer, AIndex.Index);

                  // Now insert TIndex in a layer without painting of it.
            ALayer.InsertObject(AProject.PInfo, AIndex, FALSE);
// ++ Cadmensky IDB Version 2.3.8
            if bOldSnapItemMustBeRestored then
            begin
              AProject.OldSnapItem := AIndex;
              AProject.OldSnapItem^.SetState(sf_Selected, true);
            end;
// -- Cadmensky IDB Version 2.3.8
          end;
        ci_ProjectItem:
          begin
            AView := Pointer(SO.Get);

            APrevView := PView(PLayer(AProject.PInfo.Objects).HasIndexObject(AView.Index));
            if (APrevView = nil) then
              PLayer(AProject.PInfo.Objects).Data^.Insert(AView)
            else
            begin
              SELF.AddObjectInUndoData(AProject, PView(APrevView));
              PLayer(AProject.PInfo.Objects).Data.Search(APrevView, iP);
              PLayer(AProject.PInfo.Objects).Data.AtFree(iP);
              PLayer(AProject.PInfo.Objects).Data.Insert(AView);
              for iP := 0 to AProject.Layers.LData.Count - 1 do
              begin
                ALayer := PLayer(AProject.Layers.LData.At(iP));
                AIndex := ALayer.HasIndexObject(AView.Index);
                if AIndex = nil then
                  CONTINUE;
                AIndex.MPtr := AView;

                RedrawRect.CorrectByRect(AView.ClipRect);
                AProject.UpdateClipRect(AView);
                AIndex.ClipRect.InitByRect(AView.ClipRect);
                RedrawRect.CorrectByRect(AView.ClipRect);
                AProject.SelAllRect.CorrectByRect(AView.ClipRect);
              end; // for iP end
            end;
          end;
        ci_DeletedObject:
          begin
            SO.Read(iLayerIndex, SizeOf(iLayerIndex));
            ALayer := AProject.Layers.IndexToLayer(iLayerIndex);
            if ALayer = nil then
              ALayer := AProject.Layers.GetTopLayer;
            SO.Read(iItemIndex, SizeOf(iItemIndex));
            APrevIndex := ALayer.HasIndexObject(iItemIndex);
            if APrevIndex <> nil then
            begin
{                     bTheIndexIsASymbol := APrevIndex.GetObjType = ot_Symbol;
                     if bTheIndexIsASymbol then
                        ASymbol := APrevIndex.MPtr;}
// ++ Cadmensky
              AView := ALayer.IndexObject(AProject.PInfo, APrevIndex);
              bExists := AProject.Layers^.ExistsObject(APrevIndex) > 1;
// -- Cadmensky
              try
                ALayer.DeleteIndex(AProject.PInfo, APrevIndex);
// ++ Cadmensky
                if not bExists then
                  PLayer(AProject.PInfo^.Objects)^.DeleteIndex(AProject.PInfo, AView);
{                        if bTheIndexIsASymbol then
                           PLayer (AProject.PInfo.Objects).DeleteIndex (AProject.PInfo, ASymbol);}
              except
              end;
// -- Cadmensky
            end;
          end;
      end; // case end

      iPos := SO.Position;

      StatusBar.Progress := 100 * iPos / iSize;
      Application.ProcessMessages;
    end; // while iPos < iSize end
  finally
    FreeMem(ABuffer, iSize);
    SO.Free;

    case ARedoType of
      rtInsert: AnUndoType := utUnInsert;
      rtDelete: AnUndoType := utUnDelete;
      rtChange: AnUndoType := utUnChange;
      rtMove: AnUndoType := utUnMove;
      rtResize: AnUndoType := utUnResize;
      rtTransform: AnUndoType := utUnTransform;
    else
      AnUndoType := utUnknownUNDO;
    end; // case end

    SELF.UndoRedoLib.SaveUndoData(AProject, AnUndoType, FALSE);
    if WinGISMainForm.WeAreUsingTheIDB[AProject] then
    begin
      WinGISMainForm.IDB_Man.ExecuteRestoring(AProject);
      WinGISMainForm.IDB_Man.DeleteObjects_FromWG(AProject);
    end;

    if AnUndoButton <> nil then
      AnUndoButton.Hint := SELF.UndoRedoLib.GetNextUndoDescription(AProject);
    if ARedoButton <> nil then
      ARedoButton.Hint := SELF.UndoRedoLib.GetNextRedoDescription(AProject);

    RestoreScreenCursor;
    StatusBar.ProgressPanel := FALSE;
    AProject.PInfo.RedrawScreen(TRUE);
    SetProjectModified(AProject);
    if WinGISMainForm.WeAreUsingTheIDB[AProject] then
      WinGISMainForm.IDB_Man.RefreshDataWindows(AProject);
    Application.ProcessMessages;
  end;
end;

procedure TUndoRedoManager.SetNecessitySaveUndoDataSign(AProject: Pointer; bValue: Boolean);
begin
  SELF.bNessesaryToSaveUndoData := bValue;
end;

procedure TUndoRedoManager.GetReadyToReceiveUndoData(AProject: Pointer);
begin
  SELF.UndoRedoLib.GetReadyToReceiveUndoData(AProject);
end;

{ Set IDB manager in state for receiveng IDs of objects to be deleted. }

procedure TUndoRedoManager.GetReadyToReceiveDeletedObjectID(AProject: PProj);
begin
   // At first I recognize whether a project is a symbol editor or not.
  if AProject.SymbolMode <> sym_Project then
    exit;
  SELF.GetReadyToReceiveUndoData(AProject);
end;

{ Receiving IDs of objects to be deleted. }

procedure TUndoRedoManager.ReceiveDeletedObjectID(AProject: PProj; ALayer: PLayer; AView: PView; AItem: PIndex);
begin
   // At first I recognize whether a project is a symbol editor or not.
  if AProject.SymbolMode <> sym_Project then
    exit;
  SELF.AddObjectInUndoData(AProject, ALayer, AItem);
end;

{ Execute deleting of objects. }

procedure TUndoRedoManager.ExecuteDeleting(AProject: PProj; bShowProgressBarInMainWindow: Boolean = TRUE);
begin
   // At first I recognize whether a project is a symbol editor or not.
  if AProject.SymbolMode <> sym_Project then
    exit;

  if not WinGisMainForm.WeAreUsingTheIDB[AProject] then
  begin
    StatusBar.ProgressPanel := true;
      // 'Deleting from DB'
    StatusBar.ProgressText := MlgStringList['IDB', 3];
  end;
  SELF.SaveUndoData(AProject, utUnDelete);
  StatusBar.ProgressPanel := false;
end;

end.

