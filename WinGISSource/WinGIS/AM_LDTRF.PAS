{****************************************************************************}
{ Unit LoadTRFiles - Dialog f�r das Laden v. Turbo Raster Files             }
{----------------------------------------------------------------------------}
{                                                                            }
{----------------------------------------------------------------------------}
{ Modifikationen                                                             }
{****************************************************************************}
Unit AM_LDTRF;
{$D+,L+}
Interface

Uses Messages,WinTypes,WinProcs,AM_Def,AM_Text,AM_Font,AM_Paint,
     AM_Ini,AM_Admin,AM_Layer,AM_Dlg1,Objects,AM_StdDl,
     AM_TRD, BmpImage, WinDOS, Controls,ResDlg,ProjStyle;

Const id_RasterLayer               = 101;
      id_NewLayer                  = 100;
      id_OverviewLayer             = 103;
      id_LoadOverview              = 105;
      id_DisplayOverviewAsFrame    = 106;
      id_DisplayRasterAsFrame      = 107;
      id_PictureList               = 108;

      RasterJustFrames             = 1;
      OverviewJustFrames           = 2;
      LoadOverview                 = 4;

Type IntArray = Array[0..0] of Integer;
     PInteger = ^Integer;

     PPChar   = ^PChar;

     PDLoadTRFiles = ^TDLoadTRFiles;
     TDLoadTRFiles = Class(TGrayDialog)
      Public
       pData                   : PCollection;
       Layers                  : PLayers;
       RasterStr               : PPChar;
       OverviewStr             : PPChar;
       RetFlags                : PInteger;
       IsOverviewEnabled       : Boolean;
       WasJustOverviewFrames   : Boolean;
       OrgProjStyles           : TProjectStyles;
       ProjectStyles           : TProjectStyles;

       Constructor Init(AParent:TWinControl;ApData:PCollection;ApLayers:PLayers;AProjStyle:TProjectStyles;ARasterStr:PPChar;AOverviewStr:PPChar;AFlags:PInteger);
       Destructor  Destroy; override;
       Procedure   SetupWindow; override;
       Procedure   IDNewLayer(var Msg:TMessage); message id_First+id_NewLayer;
       Procedure   EnableOverview(AEnable:Boolean);
       Procedure   WMCommand(var Msg:TMessage); message wm_First+wm_Command;
       Function    CanClose:Boolean; override;
     End;

Procedure ProcLoadTRFiles(AData:Pointer;FName:PChar);

Implementation

Uses AM_Main,AM_Proj,LayerHndl,SysUtils,UserIntf,Dialogs;

Constructor TDLoadTRFiles.Init
   (
   AParent         : TWinControl;
   ApData          : PCollection;
   ApLayers        : PLayers;
   AProjStyle      : TProjectStyles;
   ARasterStr      : PPChar;
   AOverviewStr    : PPChar;
   AFlags          : PInteger
   );
  begin
    inherited Init(AParent,'LOADTRFILES');
    pData:=ApData;
    Layers:=ApLayers;
    IsOverviewEnabled:=TRUE;
    RasterStr:=ARasterStr;
    OverviewStr:=AOverviewStr;
    RetFlags:=AFlags;
    OrgProjStyles:=AProjStyle;
    ProjectStyles:=TProjectStyles.Create;
    if OrgProjStyles<>NIL then ProjectStyles.Assign(OrgProjStyles);
  end;

Destructor TDLoadTRFiles.Destroy;
  begin
    ProjectStyles.Free;
    inherited Destroy;
  end;

Procedure TDLoadTRFiles.SetupWindow;
  var i            : LongInt;
      Layer        : PLayer;
      Temp         : String;
  begin
    inherited SetupWindow;
    for i:=0 to pData^.Count-1 do
      SendDlgItemMessage(Handle,id_PictureList,lb_AddString,0,LongInt(PInfoStr(pData^.At(i))^.Info));
    for i:=1 to Layers^.LData^.Count-1 do begin
      Layer:=Layers^.LData^.At(i);
      if (not Layer^.GetState(sf_Fixed)) and (not Layer^.GetState(sf_LayerOff)) then begin
        Temp:=Layer^.Text^;
        Temp:=Temp+#0;
        SendDlgItemMessage(Handle,id_RasterLayer,cb_AddString,0,LongInt(@Temp[1]));
        SendDlgItemMessage(Handle,id_OverviewLayer,cb_AddString,0,LongInt(@Temp[1]));
      end;
    end;
    SendDlgItemMessage(Handle,id_RasterLayer,cb_SetCurSel,0,0);
    SendDlgItemMessage(Handle,id_OverviewLayer,cb_SetCurSel,1,0);
    EnableOverview(FALSE); 
  end;

Procedure TDLoadTRFiles.EnableOverview
   (
   AEnable         : Boolean
   );
  begin
    IsOverviewEnabled:=AEnable;
    if not IsOverviewEnabled then begin
      if IsDlgButtonChecked(Handle,id_DisplayOverviewAsFrame) <> 0 then begin
        WasJustOverviewFrames:=TRUE;
        CheckDlgButton(Handle,id_DisplayOverviewAsFrame,0);
      end
      else WasJustOverviewFrames:=FALSE;
    end
    else CheckDlgButton(Handle,id_DisplayOverviewAsFrame,Ord(WasJustOverviewFrames));
    EnableWindow(GetItemHandle(id_OverviewLayer),IsOverviewEnabled);
    EnableWindow(GetItemHandle(id_DisplayOverviewAsFrame),IsOverviewEnabled);
  End;

Procedure TDLoadTRFiles.WMCommand
   (
   var Msg         : TMessage
   );
  var Selection    : ^IntArray;
      SelectionCnt : Integer;
      i            : Integer;
      Temp         : Array[0..512] of Char;
      RefTemp      : Array[0..512] of Char;
      Tmp          : Array[0..512] of Char;
      Spc          : PChar;
      IsOverview   : Boolean;
  begin
    IsOverview:=False;
    inherited ;
    if Msg.lParamHi = lbn_SelChange then begin
      SelectionCnt:=SendDlgItemMessage(Handle,id_PictureList,lb_GetSelCount,0,0);
      GetMem(Selection,SizeOf(Integer)*SelectionCnt);
      SendDlgItemMessage(Handle,id_PictureList,lb_GetSelItems,SelectionCnt,LongInt(Selection));
      GetLangPChar(11136,Temp,511);
      GetLangPChar(11134,RefTemp,511);
{$R-}
      for i:=0 to SelectionCnt-1 do begin
        SendDlgItemMessage(Handle,id_PictureList,lb_GetText,Selection^[i],LongInt(@Tmp[0]));
        Spc:=StrScan(@Tmp[0],'.');
        if Spc <> NIL then Spc[1]:=#0;
        if StrComp(Temp,Tmp) = 0 then
          SendDlgItemMessage(Handle,id_PictureList,lb_SetSel,Word(FALSE),LongInt(Selection^[i]));
        if StrComp(RefTemp,Tmp) = 0 then IsOverview:=TRUE;
      end;
{$R+}
      FreeMem(Selection,SizeOf(Integer)*SelectionCnt);
      EnableOverview(IsOverview);
    end;
  end;

Procedure TDLoadTRFiles.IDNewLayer
   (
   var Msg         : TMessage
   );
  var ALayer       : PLayer;
      NewName      : Array[0..255] of Char;
  begin
      ALayer:=SelectLayerDialog(Layers^.Parent,[sloNewLayer]);
      if ALayer<>NIL then begin
        StrPCopy(NewName,PToStr(ALayer^.Text));
        SendDlgItemMessage(Handle,id_RasterLayer,cb_AddString,0,LongInt(@NewName));
        SendDlgItemMessage(Handle,id_OverviewLayer,cb_AddString,0,LongInt(@NewName));
      end;
  end;

Function TDLoadTRFiles.CanClose
   : Boolean;
  var i            : LongInt;
      DataStr      : PInfoStr;
      Data         : PCollection;
      SelectionCnt : LongInt;
      Selection    : ^IntArray;
      Temp         : Array[0..512] of Char;
  Function DoAll
     (
     Item          : PInfoStr
     ) : Boolean; Far;
    begin
      if StrComp(Item^.Info,PInfoStr(Data^.At(i))^.Info) = 0 then DoAll:=TRUE
      else DoAll:=FALSE;
    end;
  begin
    Data:=New(PCollection,Init(5,2));
    SelectionCnt:=SendDlgItemMessage(Handle,id_PictureList,lb_GetSelCount,0,0);
    GetMem(Selection,SizeOf(Integer)*SelectionCnt);
    SendDlgItemMessage(Handle,id_PictureList,lb_GetSelItems,SelectionCnt,LongInt(Selection));
{$R-}
    for i:=0 to SelectionCnt-1 do begin
      DataStr:=New(PInfoStr);
      SendDlgItemMessage(Handle,id_PictureList,lb_GetText,Selection^[i],LongInt(@Temp[0]));
      DataStr^.Info:=StrNew(Temp);
      Data^.Insert(DataStr);
    end;
{$R-}
    for i:=0 to Data^.Count-1 do begin
      DataStr:=pData^.FirstThat(@DoAll);
      GetMem(PInfoStr(Data^.At(i))^.Detail,SizeOf(DataStr^.Detail));
      Move(DataStr^.Detail,PInfoStr(Data^.At(i))^.Detail,SizeOf(DataStr^.Detail));
    end;
    for i:=0 to pData^.Count-1 do begin
      DataStr:=pData^.At(0);
      pData^.Delete(DataStr);
      Dispose(DataStr^.Detail);
      StrDispose(DataStr^.Info);
      Dispose(DataStr);
    end;
    for i:=0 to Data^.Count-1 do begin
      pData^.Insert(Data^.At(0));
      Data^.Delete(Data^.At(0));
    end;
    Dispose(Data,Done);
    RetFlags^:=0;
    RetFlags^:=RetFlags^ or LoadOverview;
    if IsDlgButtonChecked(Handle,id_DisplayOverviewAsFrame) <> 0 then
      RetFlags^:=RetFlags^ or OverviewJustFrames;
    if IsDlgButtonChecked(Handle,id_DisplayRasterAsFrame) <> 0 then
      RetFlags^:=RetFlags^ or RasterJustFrames;
    SendDlgItemMessage(Handle,id_RasterLayer,cb_GetLBText,
      SendDlgItemMessage(Handle,id_RasterLayer,cb_GetCurSel,0,0),LongInt(@Temp[0]));
    RasterStr^:=StrNew(Temp);
    SendDlgItemMessage(Handle,id_OverviewLayer,cb_GetLBText,
      SendDlgItemMessage(Handle,id_OverviewLayer,cb_GetCurSel,0,0),LongInt(@Temp[0]));
    OverviewStr^:=StrNew(Temp);
    if OrgProjStyles<>NIL then OrgProjStyles.Assign(ProjectStyles);
    CanClose:=TRUE;
  end;

Procedure ProcLoadTRFiles
   (
   AData           : Pointer;
   FName           : PChar
   );
  var Temp         : Array[0..fsPathName] of Char;
      LTProj       : Bool;
      IDOffset     : Array [0..1] of Char;
      Point        : PChar;
      SearchRec    : TSearchRec;
      DataFile     : PTurboRasterDataFile;
      Data         : PCollection;
      RetFlags     : Integer;
      RasterStr    : PChar;
      OverviewStr  : PChar;
      sLayerName   : String;
      Layer        : PLayer;
      LayerIndex   : LongInt;
      oLayerIndex  : LongInt;
      Referenced   : Integer;
      FileName     : PChar;
      UpperLeft    : LPoint;
      LowerRight   : LPoint;
      sFileName    : String;
      IsCodedPic   : Boolean;
      Image        : PImage;
      i            : LongInt;
      j            : LongInt;
      Item         : PInfoStr;
      FileDialog   : TOpenDialog;
      ShowBMP      : Word;
  Procedure PToStr
     (
     Src           : PChar;
     var Dest      : String
     );
    var i          : Integer;
    begin
      i:=0;
      while Src[i]<>#0 do begin
        Dest[i+1]:=Src[i];
        i:=i+1;
      end;
      Dest[0]:=Char(i);
    end;
  Function DoAll
     (
     Item          : PLayer
     ) : Boolean; Far;
    var Tmp        : String;
    begin
      if Item^.Text = NIL then DoAll:=FALSE
      else begin
        Tmp:=Item^.Text^+#0;
        if StrComp(@Tmp[1],@sLayerName[1]) = 0 then DoAll:=TRUE
        else DoAll:=FALSE;
      end;
    end;
  begin
    if FName = NIL then begin
      FileDialog:=TOpenDialog.Create(PProj(AData)^.Parent);
      FileDialog.Title:=GetLangText(11128);
      try             
        if FileDialog.Execute then StrPCopy(Temp,FileDialog.FileName)
        else Exit;
      finally
        FileDialog.Free;
      end;
    end
    else StrCopy(@Temp[0],FName);
    Point:=StrRScan(@Temp[0],'.');
    StrCopy(Point,'.trd');
    DataFile:=New(PTurboRasterDataFile,Init(@Temp[0]));
    DataFile^.ReadFile(Data);
    if ExecDialog(TDLoadTRFiles.Init(PProj(AData)^.Parent,Data,PProj(AData)^.Layers,
                                   PProj(AData)^.PInfo^.ProjStyles,@RasterStr,
                                   @OverviewStr,@RetFlags)) = ID_OK then begin
      PToStr(RasterStr,sLayerName);
      sLayerName:=sLayerName+#0;
      Layer:=PProj(AData)^.Layers^.LData^.FirstThat(@DoAll);
      LayerIndex:=Layer^.Index;
      if (RetFlags and LoadOverview) <> 0 then begin
        PToStr(OverviewStr,sLayerName);
        sLayerName:=sLayerName+#0;
        Layer:=PProj(AData)^.Layers^.LData^.FirstThat(@DoAll);
        oLayerIndex:=Layer^.Index;
      end;
      for i:=0 to Data^.Count-1 do begin
        Referenced:=DataFile^.GetData(PInfoStr(Data^.At(i))^.Detail,FileName,@UpperLeft,@LowerRight);
        PToStr(FileName,sFileName);
        if(RetFlags AND OverviewJustFrames) = 0 then
          ShowBMP := so_ShowBMP
        else
          ShowBMP := 0;
        if (Referenced and out_Overview) <> 0 then Image:=New(PImage,Init(sFileName,
            ExtractRelativePath(PProj(AData)^.FName,sFileName),
            ShowBMP, PProj(AData)^.PInfo^.BitmapSettings))
        else
        Begin
          if(RetFlags AND RasterJustFrames) = 0 then
            ShowBMP := so_showBMP
          else
            ShowBMP := 0;
          Image:=New(PImage,Init(sFileName,ExtractRelativePath(PProj(AData)^.FName,sFileName),
              ShowBMP, PProj(AData)^.PInfo^.BitmapSettings));
        End;
{        Image^.Colors256:=PProj(AData)^.PInfo^.Colors256;
        Image^.ScreenWidth:=PProj(AData)^.PInfo^.ScreenWidth;
        Image^.ScreenHeight:=PProj(AData)^.PInfo^.ScreenHeight;}{MB_DEL}
        Image^.SetClipRect(PProj(AData)^.PInfo);
        if Image^.LoadImage(PProj(AData)^.PInfo, bm_Display) <> 0 then begin
          if (Referenced and out_Error) = 0 then begin
            if (Referenced and out_Overview) <> 0 then begin
              Image^.ScaledClipRect.Assign(UpperLeft.x,LowerRight.y,LowerRight.x,UpperLeft.y);
{              Image^.OldRealClipRect.Assign(UpperLeft.x,LowerRight.y,LowerRight.x,UpperLeft.y);}
              Image^.ClipRect.Assign(UpperLeft.x,LowerRight.y,LowerRight.x,UpperLeft.y);
{              Image^.ShowRect.Assign(UpperLeft.x,LowerRight.y,LowerRight.x,UpperLeft.y);}
            end
            else if (Referenced and out_IsReferenced) <> 0 then begin
              Image^.ScaledClipRect.Assign(UpperLeft.x,UpperLeft.y,LowerRight.x,LowerRight.y);
{              Image^.OldRealClipRect.Assign(UpperLeft.x,UpperLeft.y,LowerRight.x,LowerRight.y);}
              Image^.ClipRect.Assign(UpperLeft.x,UpperLeft.y,LowerRight.x,LowerRight.y);
{              Image^.ShowRect.Assign(UpperLeft.x,UpperLeft.y,LowerRight.x,LowerRight.y);}
            end;
          end;
        end;
{        Image^.UnLoad;
        if (Referenced and out_Overview) <> 0 then Image^.ShowBitmap:=(RetFlags and OverviewJustFrames) = 0
        else Image^.ShowBitmap:=(RetFlags and RasterJustFrames) = 0;
        if Image^.LoadBmp(bm_Display) then begin}
          if Referenced = out_Overview then begin
            if PProj(AData)^.InsertObjectOnLayer(Image,oLayerIndex) then Inc(PProj(AData)^.BitmapCount);
          end
          else if PProj(AData)^.InsertObjectOnLayer(Image, LayerIndex) then Inc(PProj(AData)^.BitmapCount);
{        end;}
      end;
{!?!?!      DrawMenu.EnableMenuEntries(sm_File);}
      for i:=0 to Data^.Count-1 do begin
        Item:=Data^.At(0);
        Data^.Delete(Item);
        StrDispose(Item^.Detail);
        StrDispose(Item^.Info);
      end;
      Dispose(Data,Done);
    end
    else begin
      for i:=0 to Data^.Count -1 do begin
        Item:=Data^.At(0);
        Data^.Delete(Item);
        StrDispose(Item^.Detail);
        StrDispose(Item^.Info);
        Dispose(Item);
      end;
      Dispose(Data,Done);
    end;
    Dispose(DataFile,Done);
  end;
  
end.

