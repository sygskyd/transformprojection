{****************************************************************************}
{ Unit AM_Punkte                                                             }
{----------------------------------------------------------------------------}
{ Autoren: Thalhammer Franz, Martin Forst                                    }
{----------------------------------------------------------------------------}
{ Modifikationen                                                             }
{****************************************************************************}
Unit AM_Punkt;
Interface

Uses SysUtils,Messages,Objects,AM_Admin,AM_Def,AM_Index,AM_Point,
     {$IFNDEF WMLT}
     AM_DDE,
     {$ENDIF}
     AM_Layer,AM_Poly,AM_CPoly,AM_StdDl,AM_Dlg1,AM_Coll,WinProcs,ResDlg,Controls,
     AM_Sym,AM_Text,AVLTree,AM_Paint,WinTypes,Classes,Am_View,ProjStyle;

const id_SourceList     = 101;
      id_SourcePoint    = 103;
      id_ObjectTypes    = 104;
      id_DestLayer      = 105;
      id_Symbol         = 110;
      Id_Invers         = 120;

      maxTypes          = 3;

      ObjTypes          : Array[0..maxTypes-1] of Word = (ot_Pixel,ot_Symbol,ot_Text);

Type {***********************************************************************}
     { Record TNode                                                          }
     {-----------------------------------------------------------------------}
     {***********************************************************************}
     PNode              = ^TNode;
     TNode              = Record
       Index            : Longint;
       Position         : PDPoint;
       ObjNumber        : LongInt;
       data1            : pString;
       data2            : pString;
       AView            : PView;
     end;

     {***********************************************************************}
     { Record TListNode                                                      }
     {-----------------------------------------------------------------------}
     {***********************************************************************}
     PListNode          = ^TListNode;
     TListNode          = Record
       ObjNumber        : LongInt;
       Index            : Longint;
       Ldata1           : PString;
       LData2           : PString;
       AView            : PView;
       Next             : PListNode;
     end;

     PPointData         = ^TPointData;
     TPointData         = Record
       LayersAreas      : PLongColl;
       LayersPoints     : PLongColl;
       ObjectTypes      : TObjectTypes;
       Invers           : Boolean;
       DestLayer        : Longint;
     end;

     PPointDlg           = ^TPointDlg;
     TPointDlg           = Class(TLayerDlg)
      Public
       Project          : Pointer;
       Data             : PPointData;
       TypesList        : TRListBox;
       Constructor Init(Proj:Pointer;AParent:TWinControl;AData:PPointData;ALayers:PLayers;
           AProjStyle:TProjectStyles);
       Function    CanClose:Boolean; override;
       Procedure   SetupWindow; override;
       Procedure   IdSymbol(var Msg:TMessage); message id_first + id_Symbol;
     end;

     {***********************************************************************}
     { Object TTree                                                          }
     {-----------------------------------------------------------------------}
     { Felder:                                                               }
     {***********************************************************************}
     PTree              = ^TTree;
     TTree              = Object(TAvlTree)
       Function    Compare(Key1,Key2:Pointer):Integer; virtual;
       Procedure   FreeItem(Item:Pointer); virtual;
       Procedure PrintAvlNode(var Data:Pointer;var f:textfile);
       Procedure Traverse(var Root:PAVlNode;var f:textfile);
     end;

Procedure CombinePointArea(Proj:Pointer;PInfo:PPaint);

PRocedure ProcDKMReply(Data:Pointer);
var MyList  : TStringList;
    MyPointData         : TPointData;
    avlcount: longint;
    WorkLayer : PLayer;

Implementation

Uses AM_Child,AM_Proj,AM_ProjO,StateBar,UserIntf;

var pAvlTree          : PTree;
    bTree             : PAVLNode;
    DoAllCount        : longint;

  Function SearchNode
     (
     SData           : Pointer;
     Tree            : PAVLNode
     )
     : PAVLNode;
    begin
      if Tree=NIL then SearchNode:=NIL
      else begin
        repeat
          case pAvlTree^.Compare(SData,Tree^.Data) of
            CPEQUAL   : begin
              SearchNode:=Tree;
              Exit;
            end;
            CPSMALLER : if Tree^.Left=NIL then begin
                  SearchNode:=Tree;
                  Exit;
                end
                else Tree:=Tree^.Left;
            CPBIGGER  : if Tree^.Right=NIL then begin
                  SearchNode:=Tree;
                  Exit;
                end
                else Tree:=Tree^.Right;
          end;
        until Tree=NIL;
        SearchNode:=NIL;
      end;
    end;

  Procedure InsertObjectToLayer
     (
     Item     : PCPoly;
     Project  : PProj
     );

    var APoly  : PCPoly;
        MPoly  : PCPoly;
        cnt    : integer;
        APoint : PDPoint;
        AItem  : PCpoly;
        IItem  : PIndex;
        CopyTo : PLayer;

    begin
      CopyTo:=Project^.Layers^.IndexToLayer(MyPointData.DestLayer);
      IItem:=New(PIndex,Init(Item^.Index));
      {IItem^.Cliprect.InitByrect(Item^.Cliprect);}
      CopyTo^.InsertObject(Project^.PInfo,IItem,FALSE);
      Project^.Layers^.IndexObject(Project^.PInfo,Item)^.Invalidate(Project^.PInfo);
      (*
      MPoly:=new(PCPoly,Init);
      {AItem:=Pointer(PLayer(PProj(Data)^.PInfo^.Objects)^.IndexObject(PProj(Data)^.PInfo,Item));}
      AItem:=Item;
      for cnt:=0 to AItem^.Data^.Count-1 do begin
        APoint:=AItem^.Data^.At(cnt);
        MPoly^.InsertPoint(APoint^);
       { writeln(Apoint^.x,' ',Apoint^.y);}
      end;
     { writeln('ende');}
      Project^.InsertObjectOnLayer(MPoly,MyPointdata.DestLayer);
      *)
    end;

 Function DoCPolys
     (
     Item          : PCPoly;
     Project       : PProj
     )
     : Boolean; Far;
    var Search         : TNode;
        SearchPos      : TDPoint;
        Node1          : PAVLNode;
        Node2          : PAvlNode;
        List           : PListNode;
        pList          : PListNode;
        NewNode        : PListNode;
        AItem          : PCPoly;
        Equal          : Boolean;
        CurObject      : LongInt;
        CompareSTring1 : String;
        CompareSTring2 : String;
        Aborted        : Boolean;
        MIndex         : Pindex;
        ALayer         : PLayer;
        AllCount       : LongInt;
        AIndex         : TIndex;
        AAreaStr       : String;
        AStr           : String;
    begin
      {AItem:=Pointer(PLayer(PProj(Data)^.PInfo^.Objects)^.IndexObject(PProj(Data)^.PInfo,Item)); }{the polyline}
      AItem:=Item;
      {InsertObjectToLayer(AItem,Project);}
      Search.Position:=@SearchPos;
      SearchPos.Init(AItem^.ClipRect.A.X,-MaxLongInt);
      Node1:=SearchNode(@Search,pAvlTree^.AvlRoot);
      SearchPos.Init(AItem^.ClipRect.B.X,MaxLongInt);
      Node2:=SearchNode(@Search,pAvlTree^.AvlRoot);
      List:=NIL;
      if (PNode(Node1^.Data)^.Position^.Y>=AItem^.ClipRect.A.Y)
          and (PNode(Node1^.Data)^.Position^.Y<=AItem^.ClipRect.B.Y)
          and (AItem^.PointInside(Project^.PInfo,PNode(Node1^.Data)^.Position^)) then begin
        New(NewNode);
        NewNode^.ObjNumber:=PNode(Node1^.Data)^.ObjNumber;
        NewNode^.LData1:=PNode(Node1^.DAta)^.Data1;
        NewNode^.LData2:=Pnode(Node1^.Data)^.Data2;
        NewNode^.Index:=Pnode(Node1^.Data)^.Index;
        NewNode^.AView:=PNode(Node1^.Data)^.AView;
        NewNode^.Next:=List;
        List:=NewNode;
      end;

      if Node1<>Node2 then repeat
        Node1:=pAvlTree^.NextNode(Node1);
        if (PNode(Node1^.Data)^.Position^.Y>=AItem^.ClipRect.A.Y)
            and (PNode(Node1^.Data)^.Position^.Y<=AItem^.ClipRect.B.Y)
            and (AItem^.PointInside(Project^.PInfo,PNode(Node1^.Data)^.Position^)) then begin
          New(NewNode);
          NewNode^.ObjNumber:=PNode(Node1^.Data)^.ObjNumber;
          NewNode^.LData1:=PNode(Node1^.DAta)^.Data1;
          NewNode^.LData2:=Pnode(Node1^.Data)^.Data2;
          NewNode^.Index:=Pnode(Node1^.Data)^.Index;
          NewNode^.AView:=PNode(Node1^.Data)^.AView;
          NewNode^.Next:=List;
          List:=NewNode;
        end;
      until Node1=Node2;

      if List=NIL then begin
        {if not Inverse then }InsertObjectToLayer(AItem,Project);        {********************************************}
      end else begin

         try
          if List^.ldata2^='1986'then raise ElistError.Create('Hallo');
          except end;

        pList:=List;
        if (List^.ldata1<>NIL)and(list^.ldata2<>NIL) then begin
          Equal:=TRUE;
          CompareString1:=List^.LData1^;
          CompareString2:=List^.LData2^;
          while pList<>NIL do begin
            if (plist^.ldata1<>NIL)and(plist^.ldata2<>NIL) then begin
              if CompareText(CompareSTring1,pList^.LData1^)<>0 then Equal:=false;
              if CompareText(CompareSTring2,pList^.LData2^)<>0 then Equal:=false;
            end;
            pList:=pList^.next;
          end;
        end;

        if (Equal)and(list^.Aview^.GetObjtype = ot_symbol) then begin
          if PSymbol(List^.Aview)^.SymPtr<>PProj(Project)^.ActualSym then Equal:=false;
        end;
        if Equal then begin
          AStr:=Format('[CHG][%10d][%10d]',[List^.ObjNumber,AItem^.Index]);
          Str(AItem^.Flaeche:15:2,AAreaStr);
          AStr:=AStr+'['+AAreaStr+']';
         { writeln(f4,Params[0],' ',List^.index,' ',Params[1]);}
          {$IFNDEF WMLT}
            {$IFNDEF AXDLL} // <----------------- AXDLL
          DDEHandler.SendString(AStr);
            {$ENDIF} // <----------------- AXDLL
          {$ENDIF}
          {if Inverse then InsertObjectToLayer(AItem,Project); }           {***************************************}


          pList:=List;
          while pList<>NIL do begin
            Aindex.Index:=pList^.ObjNumber;
            MIndex:=WorkLayer^.HasObject(@AIndex);
            if MIndex<>NIl then WorkLayer^.DeleteIndex(Project^.Pinfo,Mindex);
            pList:=pList^.next;
          end;

        end else begin
          plist:=list;
          {while plist<>NIL do begin
            writeln(f,plist^.ldata1^,'   ', plist^.ldata2^);
            plist:=plist^.next;
          end;}
          InsertObjectToLayer(AItem,Project);
        end;

        while List<>NIL do begin
          NewNode:=List;
          {if List^.Ldata1<>NIL then DisposeStr(List^.LData1);
          if List^.Ldata2<>NIL then DisposeStr(List^.LData2);}
          List:=List^.Next;
          Dispose(NewNode);
        end;

      end; {List <>NIL}
      Inc(CurObject,100);
     { AbortDlg.SetPercent(100*CurObject/AllCount);
      DoCPolys:=AbortDlg.CheckAskForAbort(3803);}

    end;



Procedure CombinePointArea
   (
   Proj            : Pointer;
   PInfo           : PPaint
   );
  var Data             : TPointData;
      Cnt              : longint;
      cnt2             : Longint;
      cnt3             : longint;
      ALayer           : PLayer;
      AllCount         : LongInt;
      CurObject        : LongInt;
      Aborted          : Boolean;
      Abort            : Boolean;
      MIndex           : Pindex;
      aTree            : PAVLNode;
      aNode            : PNode;
      AStr             : String;

  Function InsertPoints
     (
     Item              : PIndex
     )
     : Boolean; Far;
    var Node           : PNode;
        AItem          : PPixel;
    begin
      AItem:=Pointer(PLayer(PInfo^.Objects)^.IndexObject(PInfo,Item));
      if AItem^.GetObjtype = ot_Pixel then begin
        New(Node);
        Node^.data1:=NIL;
        Node^.data2:=NIL;
        {AItem:=Pointer(PLayer(PInfo^.Objects)^.IndexObject(PInfo,Item));}
        with Node^ do begin
          Position:=@AItem^.Position;
          ObjNumber:=AItem^.Index;
          AView:=AItem;
        end;
        if not pAvlTree^.Insert(Node) then begin
          Dispose(Node);
        end;
        Inc(CurObject);
        {AbortDlg.SetPercent(100*CurObject/AllCount);
        InsertPoints:=AbortDlg.CheckAskForAbort(3803);}
      end;
    end;

  Function InsertSymbols
     (
     Item              : PIndex
     )
     : Boolean; Far;
    var Node           : PNode;
        AItem          : PSymbol;
    begin
      AItem:=Pointer(PLayer(PInfo^.Objects)^.IndexObject(PInfo,Item));
      if AItem^.GetObjtype = ot_symbol then begin
        {if AItem^.SymPtr = PProj(Proj)^.ActualSym then }begin
          New(Node);
          Node^.Index:=AItem^.SymIndex;
          Node^.data1:=NIL;
          Node^.data2:=NIL;
          with Node^ do begin
            Position:=@AItem^.Position;
            ObjNumber:=AItem^.Index;
            AView:=AItem;
          end;
          if not pAvlTree^.Insert(Node) then Dispose(Node);
          Inc(CurObject);
          {AbortDlg.SetPercent(100*CurObject/AllCount);
          InsertSymbols:=AbortDlg.CheckAskForAbort(3803);}
        end;
      end;{sym}
    end;

  Function InsertText
     (
     Item              : PIndex
     )
     : Boolean; Far;
    var Node           : PNode;
        AItem          : PText;
    begin
      AItem:=Pointer(PLayer(PInfo^.Objects)^.IndexObject(PInfo,Item));
      if AItem^.GetObjtype = ot_Text then begin
        New(Node);
        Node^.data1:=NIL;
        Node^.data2:=NIL;
        with Node^ do begin
          Position:=@AItem^.Pos;
          ObjNumber:=AItem^.Index;
          AView:=AItem;
          {Text:=AItem^.text^;}
        end;
        if not pAvlTree^.Insert(Node) then Dispose(Node);
        Inc(CurObject);
        {AbortDlg.SetPercent(100*CurObject/AllCount);}
        {InsertText:=AbortDlg.CheckAskForAbort(3803);}
      end;
    end;



  Function DoCount
     (
     Item          : Pointer
     )
     : Boolean; Far;
    begin
      Inc(AllCount);
      DoCount:=FALSE;
    end;


   Function IdToNumbers
     (
     Item          : Pointer
     )
     : Boolean; Far;
    begin
      Inc(AllCount);
      IdToNumbers:=FALSE;
    end;

  {************ Beginn der Routine *************}
  begin
   { assign(f,'out.dat');
    rewrite(f);     }
    if ExecDialog(TPointDlg.Init(PRoj,PProj(Proj)^.Parent,
        @PProj(Proj)^.CombPointAreaData,PProj(Proj)^.Layers,PProj(Proj)^.PInfo^.ProjStyles))=id_OK then begin


      Data:=PProj(Proj)^.CombPointAreaData;
      MyPointdata:=Data;
      StatusBar.ProgressPanel:=TRUE;
      try
        StatusBar.ProgressText:=GetLangText(11082);
        StatusBar.AbortText:=GetLangText(3803);

        WorkLayer:=PProj(Proj)^.Layers^.IndexToLayer(LongInt(Data.LayersPoints^.At(0)));
        {Count}

        AllCount:=0;
        AVLCount:=0;
        (*
        for Cnt:=0 to Data.LayersAreas^.Count-1 do begin    {alle Layer durchgehen}
          ALayer:=PProj(Proj)^.Layers^.IndexToLayer(LongInt(Data.LayersAreas^.At(Cnt)));
          ALayer^.FirstObjectType(ot_CPoly,@DoCount);
        end;
        AllCount:=AllCount*100;
        for Cnt:=0 to Data.LayersPoints^.Count-1 do begin
          ALayer:=PProj(Proj)^.Layers^.IndexToLayer(LongInt(Data.LayersPoints^.At(Cnt)));
          if Data.ObjectTypes and ot_Pixel<>0 then
             ALayer^.FirstObjectType(ot_Pixel,@DoCount);
          if Data.ObjectTypes and ot_Symbol<>0 then
             ALayer^.FirstObjectType(ot_Symbol,@DoCount);
          if Data.ObjectTypes and ot_Text<>0 then
             ALayer^.FirstObjectType(ot_Text,@DoCount);
        end;
        {End count}
         *)


        {*********** Build Tree **********}
        pAvlTree:=New(PTree,Init);
        Aborted:=FALSE;
        CurObject:=0;
        for Cnt:=0 to Data.LayersPoints^.Count-1 do begin
          ALayer:=PProj(Proj)^.Layers^.IndexToLayer(LongInt(Data.LayersPoints^.At(Cnt)));
          if ot_Pixel in Data.ObjectTypes then begin
             {Aborted:=ALayer^.FirstObjectType(ot_Pixel,@InsertPoints)<>NIL;}
            for cnt2:=0 to ALayer^.Data^.getcount-1 do begin
              MIndex:=ALayer^.DAta^.at(cnt2);
              InsertPoints(MIndex);
            end;
          end;
          if ot_Symbol in Data.ObjectTypes then begin
            cnt3:=ALayer^.Data^.getcount-1;
            for cnt2:=0 to ALayer^.Data^.getcount-1 do begin
              MIndex:=ALayer^.DAta^.at(cnt2);
              InsertSymbols(MIndex);
            end;
          end;
          if ot_Text in Data.ObjectTypes then begin
            for cnt2:=0 to ALayer^.Data^.getcount-1 do begin
              MIndex:=ALayer^.DAta^.at(cnt2);
              InsertText(MIndex);
            end;
          end;
         { if Data.ObjectTypes and ot_Text<>0 then
             Aborted:=Aborted or (ALayer^.FirstObjectType(ot_Text,@InsertText)<>NIL);}
        end;
        {*********** Avl Tree ready ******}


        {Durchlaufe pAvlTree}

        Allcount:=0;
        if atree<>NIl then begin
          aTree:=pAvlTree^.LowestNode(pAvlTree^.AvlRoot);
          while aTree<>pAvlTree^.Highestnode(pAvlTree^.AvlRoot) do begin
            inc(Allcount);
            aTree:=pAvlTree^.NextNode(aTree);
          end;
        end;

        {assignfile(fdkm,'dkm');
        rewrite(fdkm);  }
        cnt3:=0;
        Abort:=false;
        StatusBar.ProgressText:=GetLangText(4580);
        if atree<>NIl then begin
          aTree:=pAvlTree^.LowestNode(pAvlTree^.AvlRoot);
          while aTree<>pAvlTree^.Highestnode(pAvlTree^.AvlRoot) do begin
            aNode:=Pnode(aTree^.Data);
            AStr:=Format('[DKM][%10d]',[ANode^.objnumber]);
            {riteln(fdkm,params[0]); }
            {$IFNDEF WMLT}
              {$IFNDEF AXDLL} // <----------------- AXDLL
            DDEHandler.SendString(AStr);
              {$ENDIF} // <----------------- AXDLL
            {$ENDIF}
            aTree:=pAvlTree^.NextNode(aTree);
            StatusBar.Progress:=100*Avlcount/AllCount;
             if StatusBar.QueryAbort then begin
              {DDEHandler^.SendString('[END][]');}
               {Exit1;}
               Abort:=TRUE;
               break;
             end;
            inc(AVlcount);
          end;                                            {???????????????????????????????????????????????/}
         {closefile(fdkm);}

          if not abort then begin
            aNode:=Pnode(aTree^.Data);
            AStr:=Format('[DKM][%10d]',[anode^.objnumber]);
            inc(AVlcount);
            {$IFNDEF WMLT}
              {$IFNDEF AXDLL} // <----------------- AXDLL
            DDEHandler.SendString(AStr);
            {Statusline.SetText(id_Line1,4581,FALSE);}
            DDEHandler.SendString('[END][]');
              {$ENDIF} // <----------------- AXDLL
            {$ENDIF}
          end;
        end;

        {AVlTree bleibt bestehen}
        if Abort then Dispose(pAvlTree,Done);
       { Statusline.Clear(id_Line1);}
      finally
        StatusBar.ProgressPanel:=FALSE;
      end;
    end;
   {closefile(f);}
  end;




{******************** Dialog *******************************}
Constructor TPointDlg.Init
   (
   PRoj            : Pointer;
   AParent         : TWinControl;
   AData           : PPointData;
   ALayers         : PLayers;
   AProjStyle      : TProjectStyles
   );
  begin
    inherited Init(AParent,'D_POINT',ALayers,AProjStyle);
    Data:=AData;
    Project:=Proj;
    Typeslist:=TRListbox.InitResource(Self,id_ObjectTypes);
  end;

Procedure TPointDlg.SetupWindow;
  var Cnt          : Integer;
      AText        : Array[0..255] of Char;
  begin
    inherited SetupWindow;
    FillLayerList(id_SourceList,TRUE,FALSE);
    FillLayerList(id_SourcePoint,TRUE,FALSE);
    FillLayerList(id_DestLayer,FALSE,TRUE);
    SetMultiLayerSelection(id_SourceList,Data^.LayersAreas,TRUE);
    SetMultiLayerSelection(id_SourcePoint,Data^.LayersPoints,TRUE);
    for Cnt:=0 to maxTypes-1 do begin
      GetLangPChar(11073+Cnt,AText,SizeOf(AText));
      TypesList.AddString(AText);
      if ObjTypes[Cnt] in Data^.ObjectTypes then
          SendDlgItemMsg(id_ObjectTypes,lb_SetCurSel,Cnt,0);
    end;
    CheckDlgButton(Handle,id_Invers,0);
  end;

Procedure   TPointDlg.IdSymbol(var Msg:TMessage);
  begin
{!?!?!    PProj(Project)^.SelectSymbol;}
  end;


Function TPointDlg.CanClose
   : Boolean;
  var Cnt          : Integer;
      ADestLayer   : LongInt;
  begin
    CanClose:=FALSE;
    Data^.Invers:=False;
    if SendDlgItemMsg(id_SourceList,lb_GetSelCount,0,0)=0 then begin
      MsgBox(Handle,841,mb_IconExclamation or mb_OK,'');
      SetFocus(GetItemHandle(id_SourceList));
    end
    else if SendDlgItemMsg(id_SourcePoint,lb_GetSelCount,0,0)=0 then begin
      MsgBox(Handle,841,mb_IconExclamation or mb_OK,'');
      SetFocus(GetItemHandle(id_SourcePoint));
    end
    else if GetLayer(id_DestLayer,ADestLayer,959,10125,10128,ilTop) then with Data^ do begin
      if IsDlgButtonChecked(Handle,id_Invers)=1 then Invers:=True;
      if LayersAreas<>NIL then Dispose(LayersAreas,Done);
      if LayersPoints<>NIL then Dispose(LayersPoints,Done);
      LayersAreas:=New(PLongColl,Init(5,5));
      LayersPoints:=New(PLongColl,Init(5,5));
      GetMultiLayerSelection(id_SourceList,TRUE,LayersAreas);
      GetMultiLayerSelection(id_SourcePoint,TRUE,LayersPoints);
      ObjectTypes:=[];
      DestLayer:=ADestLayer;
      for Cnt:=0 to maxTypes-1 do
          if SendDlgItemMsg(id_ObjectTypes,lb_GetSel,Cnt,0)<>0 then
        ObjectTypes:=ObjectTypes+[ObjTypes[Cnt]];
      CanClose:=TRUE;
    end;
  end;

Function TTree.Compare
   (
   Key1            : Pointer;
   Key2            : Pointer
   )
   : Integer;
  begin
    if PNode(Key1)^.Position^.X>PNode(Key2)^.Position^.X then Compare:=cpBigger
    else if PNode(Key1)^.Position^.X<PNode(Key2)^.Position^.X then Compare:=cpSmaller
    else begin
      if PNode(Key1)^.Position^.Y>PNode(Key2)^.Position^.Y then Compare:=cpBigger
      else if PNode(Key1)^.Position^.Y<PNode(Key2)^.Position^.Y then Compare:=cpSmaller
      else Compare:=cpEqual;
    end;
  end;

Procedure TTree.FreeItem
   (
   Item            : Pointer
   );
  begin
    Disposestr(PNode(Item)^.Data1);
    Disposestr(PNode(Item)^.Data2);
    Dispose(PNode(Item));
  end;


Procedure TTree.PrintAvlNode
   (
   var Data : Pointer;
   var f    : textfile
   );
  var AStr  : String;
  begin
    {$IFNDEF WMLT}
      {$IFNDEF AXDLL} // <----------------- AXDLL
    AStr:=Format('[DKM][%10d]',[PNode(Data)^.objnumber]);
    DDEHandler.SendString(AStr);
      {$ENDIF} // <----------------- AXDLL
    {$ENDIF}
   {writeln(f,astr);}
  end;

{Procedure ProcDoAll(ADAta:PProj;DDELine:PChar);}
Procedure ProcDoAll(ADAta:PProj;DDELine:string);
  var aNode : PNode;
      s1    : string;
      s2    : string;
      s     : string;
      l     : integer;
      i     : integer;
      state : integer;
  begin
    inc(doallcount);
    {DDELine in s1 und s2 umwandeln}
    aNode:=Pnode(bTree^.Data);
    {s:=StrPas(DDELine);}
    s:=DDELine;
    l:=Length(s);
    i:=7;
    s1:='';
    state:=0;
    while i<=l do begin
      if (s[i]<>',')and(s[i]<>']') then s1:=s1+s[i] else begin
      {meins}
        {
        if state=0 then begin
          aNode^.data1:=newstr(s1);
        end;
        if state=1 then begin
          aNode^.data2:=newstr(s1);
        end;
        }

        if state=1 then begin
          aNode^.data1:=newstr(s1);
        end;                                             {WILF}
        if state=2 then begin
          aNode^.data2:=newstr(s1);
        end;


        s1:='';
        inc(state);
      end;
      inc(i);
    end;

    bTree:=pAvlTree^.NextNode(bTree);
  end;


PRocedure ProcDKMReply(Data:Pointer);

  var
      anode    : PNode;
      ALayer   : PLayer;
      cnt      : Longint;
      cnt2     : Longint;
      cnt3     : longint;
      MIndex   : PIndex;
      APoly    : PCPoly;
      handled  : Boolean;
      count    : longint;
      ps       : Pchar;
      a        : string;
      f3       : textfile;
      Aborted  : Boolean;

  Procedure Exit2;
    begin
      Mylist.free;
      Mylist:=TStringlist.create;
      Dispose(pAvlTree,Done);
      MsgBox(PProj(Data)^.Parent.Handle,4584,mb_IconExclamation or mb_OkCancel ,'');
      PProj(Data)^.ClearStatusText;
    end;
  begin        {DDEReply}
     {
    assignfile(f3,'d:\wingis6\test1');
    rewrite(f3);
    assignfile(f4,'d:\wingis6\chg');
    rewrite(f4);
     }
    if AVlCount=Mylist.count then begin
      StatusBar.ProgressPanel:=TRUE;
      try
        StatusBar.ProgressText:=GetLangText(4582);
        StatusBar.AbortText:=GetLangText(3803);
        StatusBar.CancelText:=FALSE;
        if pAvlTree<>NIL then begin
          bTree:=pAvlTree^.LowestNode(pAvlTree^.AvlRoot);

          count:=Mylist.count;

          {
        for cnt2:=0 To Mylist.count-1 do begin
          writeln(f3,Mylist.strings[cnt2]);
        end;
          closefile(f3);
          }
          for cnt2:=0 to Mylist.count-1 do begin
            ProcDoAll(Data,Mylist.strings[cnt2]);
          end;

          (*
           bTree:=pAvlTree^.LowestNode(pAvlTree^.AvlRoot);
          while bTree<>pAvlTree^.Highestnode(pAvlTree^.AvlRoot) do begin
            aNode:=Pnode(bTree^.Data);
            if anode^.data1<>NIL then writeln(f3,doallcount,'tree      ',anode^.Data1^,' ',anode^.Data2^,' ',
            anode^.Position^.x,' ',anode^.Position^.y);
            inc(doallcount);
           { writeln(anode^.objnumber);}
            bTree:=pAvlTree^.NextNode(bTree);
          end;
            *)

          {Flaechen abarbeiten}

          Aborted:=False;
          StatusBar.ProgressText:=GetLangText(4582);
          if (pAvlTree^.AvlRoot<>NIL)then for Cnt:=0 to MyPointdata.LayersAreas^.Count-1 do begin
            ALayer:=PProj(Data)^.Layers^.IndexToLayer(LongInt(MyPointdata.LayersAreas^.At(Cnt)));
            cnt3:=ALayer^.Data^.getcount-1;
            for cnt2:=0 to ALayer^.Data^.getcount-1 do begin
                  MIndex:=ALayer^.DAta^.at(cnt2);
                  APoly:=Pointer(PLayer(PProj(Data)^.PInfo^.Objects)^.IndexObject(PProj(Data)^.PInfo,MIndex));
                  if APoly=NIL then raise ElistError.Create('Item Nil');
                  DoCPolys(APoly,PProj(Data));
                  StatusBar.Progress:=100*cnt2/cnt3;
                  (*                                                      {****************************}
                  if AbortDlg.CheckAskForAbort(3803) then begin
                    {DDEHandler^.SendString('[END][]');}
                    {Exit2;}
                    Aborted:=TRUE;
                    break;
                  end;*)
            end;
            {PProj(Data)^.Pinfo^.RedrawScreen(TRUE);}
            {if not Aborted then }
            {$IFNDEF WMLT}
              {$IFNDEF AXDLL} // <----------------- AXDLL
            DDEHandler.SendString('[END][]');
              {$ENDIF} // <----------------- AXDLL
            {$ENDIF}
          end;
          Dispose(pAvlTree,Done);
        end;
        Mylist.free;
        Mylist:=TStringlist.create;
      finally
        StatusBar.ProgressPanel:=FALSE;
      end;
    end else begin
      Mylist.free;
      Mylist:=TStringlist.create;
      Dispose(pAvlTree,Done);
      MsgBox(PProj(Data)^.Parent.Handle,4326,mb_IconExclamation or mb_OkCancel ,'');
    end;
   { closefile(f4);}
  end;


Procedure TTree.Traverse
   (
   var Root : PAVlNode;
   var f    : textfile
   );
  begin
    if Root<>NIL then begin
      if Root^.Left<>NIL then Traverse(Root^.Left,f);
      PrintAVlNode(Root^.data,f);
      if Root^.Right<>NIL then Traverse(Root^.Right,f);
    end;
  end;

Procedure ExitUnit; Far;
  begin
    MyList.Free;
  end;

Initialization
  begin
    MyList:=TStringList.Create;
    AddExitProc(ExitUnit);
  end;        

end.



