unit AM_Main;

interface

uses Messages, Windows, SysUtils, Objects, WinTypes, WinProcs, AM_Child, AM_Def, AM_Proj,
  AM_Ini, AM_Pass, AM_OLE, ResDlg,
  Forms, WinDos, Controls, Dialogs, VerMgr, MenuFn, StateBar,
  MenuList, UserIntf, ImgList, MultiLng, Classes, WCtrls, ProjHndl,
  StdCtrls, ExtCtrls, {ThreeD,} SelectFolderDlg, SaveDlg, IniFiles

{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
{++ IDB}
  , IDB_Man,
// ++ Cadmensky
  UndoRedo_Man
// -- Cadmensky
{-- IDB}
  , WXCore, DDEDef, AM_DDE, DDEML, AM_GPS, AM_TRDDE, commctrl, OLE
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
{++ Tooltips DLL}
  , DTI_LibMan, Graphics, // Psock,
  idHTTP, //NMHttp,
  DdeMan
{-- Tooltips DLL}
  ;

const
  ProgisAppID = '{88CC26B1-6FC2-11d5-99D7-0000D119E20D}';

type
  TDocumentEntry = ^DocumentEntry;
  DocumentEntry = record
    aDocument: Variant;
    aDocumentname: string;
    next: TDocumentEntry;
  end;

  pImpExpDataRec = ^ImpExpDataRec;
  ImpExpDataRec = record
    aImpExpMode: string;
    aDir: string;
    aProjName: string;
    aSourceDb: string;
    aDestDbMode: integer;
    aDestDb: string;
    aDestTable: string;
    aLngCode: string;
  end;

type
  HAA = Word;

type
  TWinGISMainForm = class(TWForm)
{$IFNDEF AXDLL} // <----------------- AXDLL
    FileHistory: TMenuHistoryList;
    MlgSection: TMlgSection;
    ImgSaveDlg: TSaveDialog;
    NewDialog: TSaveDialog;
    UIExtension: TUIExtension;
    UIInitialize: TUIInitialize;
    CommonListImages: TImageList;
    SavePictureDialog: TSaveDialog;
    OpenDialog1: TOpenDialog;
    SelectFolderDialog: TSelectFolderDialog;
    TimerFreeImpExpDll: TTimer;
    FinishModuleTimer: TTimer;
    DdeClient: TDdeClientConv;
    DdeClientItem: TDdeClientItem;
    WingisDKM: TDdeServerConv;
    WingisDKMItem: TDdeServerItem;
    MemoTab: TMemo;
    procedure FileHistoryClick(Item: string);
{$ENDIF} // <----------------- AXDLL
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
{$IFNDEF AXDLL} // <----------------- AXDLL
    procedure FormShown(Sender: TObject);
    procedure GetUpdateStatus;
    procedure SetUpdateStatus;
    procedure UIInitializeActivate(Sender: TObject);
    procedure UIInitializeMessages(var Msg: tagMSG; var Handled: Boolean);
    procedure UIExtensionUpdateMenus(Sender: TObject);
    procedure UIInitializeApplicationActivate(Sender: TObject);
{$ENDIF} // <----------------- AXDLL
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SelectFolderDialogClose(Sender: TObject);
    procedure TimerFreeImpExpDllTimer(Sender: TObject);
    procedure FinishModuleTimerTimer(Sender: TObject);
    procedure FormDblClick(Sender: TObject);
    procedure WingisDKMExecuteMacro(Sender: TObject; Msg: TStrings);
  public
    LoadedLibs: TList;
{$IFNDEF AXDLL} // <----------------- AXDLL
{++ IDB}
{$IFNDEF WMLT}
    ImpTxtLib: THandle;
    IDB_Man: TIDB;
    UndoRedo_Man: TUndoRedoManager;
{$ENDIF}
{$ENDIF} // <----------------- AXDLL
{-- IDB}
{++ Tooltips DLL}
    DTI_Man: TDTI;
{-- Tooltips DLL}
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
    TurboRasterDDE: PTurboRasterDDE;
{$ENDIF} // <----------------- AXDLL
       //ActChild3D       : TThreeDChild;
{$IFNDEF AXDLL} // <----------------- AXDLL
    DXApp: Variant;
    DXAppTCore: TCore;
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
    CloseFromDB: Boolean;
    IsActive: Boolean;
    hAa: HAA;
    NextProj: array[0..fsPathName] of Char;
    MWDDELine: array[0..255] of Char;
    MWDDELine1: array[0..255] of Char;
    FirstLoad: Boolean;
    CreateNewProj: Boolean;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
    LoadingProj: Pointer;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
    ActualProj: Pointer;
    ActualChild: TMDIChild;
    ThreeDActive: Boolean;
    SetRedraw: Boolean;
    HideWindow: Boolean;
    UseMarkSnapPoint: Boolean;
    CloseFileOnReload: Boolean;
    AllowDraw: Boolean;
    IsSaving: Boolean;
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
    ImpExpDll: THandle;
    ImpExpDllData: pImpExpDataRec;
    LastImpExpOperation: string;
{$ENDIF} // <----------------- AXDLL
    NewFileCount: Integer;
{$IFNDEF AXDLL} // <----------------- AXDLL
    IDBGlobalOnOff: Boolean;
{++ IDB}
    function SwitchIDBOn(AProject: PProj): Boolean;
    procedure SwitchIDBOff(AProject: PProj);
// ++ Cadmensky
    function SwitchUndoRedoOn(AProject: PProj): Boolean;
    procedure SwitchUndoRedoOff(AProject: PProj);
// -- Cadmensky

    function GetWeAreUsingTheIDB(AProject: PProj): Boolean;
    procedure SetWeAreUsingTheIDB(AProject: PProj; bValue: Boolean);
    function GetWeAreUsingTheIDBUserInterface(AProject: PProj): Boolean;
    procedure SetWeAreUsingTheIDBUserInterface(AProject: PProj; bValue: Boolean);
    function GetWeAreUsingUndoFunction(AProject: PProj): Boolean;
    procedure SetWeAreUsingUndoFunction(AProject: PProj; bValue: Boolean);
    function GetIDBUndoStepsCount(AProject: PProj): integer;
    procedure SetIDBUndoStepsCount(AProject: PProj; bValue: integer);

    procedure ReverseUsingIDBInfo;
    function GetWeAreUsingTheIDBInfoWindow: Boolean;
    procedure SetWeAreUsingTheIDBInfoWindow(bValue: Boolean);

    property WeAreUsingTheIDB[AProject: PProj]: Boolean read GetWeAreUsingTheIDB write SetWeAreUsingTheIDB;
    property WeAreUsingTheIDBUserInterface[AProject: PProj]: Boolean read GetWeAreUsingTheIDBUserInterface write SetWeAreUsingTheIDBUserInterface;
    property WeAreUsingUndoFunction[AProject: PProj]: Boolean read GetWeAreUsingUndoFunction write SetWeAreUsingUndoFunction;
    property WeAreUsingTheIDBInfoWindow: Boolean read GetWeAreUsingTheIDBInfoWindow write SetWeAreUsingTheIDBInfoWindow;
    property IDBUndoStepsCount[AProject: PProj]: integer read GetIDBUndoStepsCount write SetIDBUndoStepsCount;
{-- IDB}

    procedure DeleteTDocumentByName(Documentname: string);
    procedure SetCurrentTDocument(DocumentName: string);
    function GetCurrentTDocument: Variant;
    function GetCurrentTDocumentName: string;
    procedure RedrawCurrentTDocument;
    procedure SetCurrentTDocumentFilename(OldDocumentName: string; Documentname: string);
    function CheckTDocumentExists(Documentname: string): boolean;
    procedure CheckTDocumentIsCorrect;
    procedure FinishImpExpRoutine(ExitMode: boolean; ModuleType: string);
    procedure FinishModule(DllIdx: integer);
    function GetIdbProcessMask: integer;
    procedure SetIdbProcessMask(value: integer);
    procedure DoAmpDkmImport(StartDir: string; AmpFileName: string; DkmDBDir: string; DestDBMode: Integer; DestDatabase: string; DestTable: string; LngCode: string);
    procedure ExecAmpDkmImport;
    function AcceptExecute(HSZDGTop, HSZTopic: HSZ; DDEStr, ConvName: PChar): Boolean;
    procedure AddDBEntry(ACommunicName: PChar; ACon, AConProj, ASendStr, ASendProj: Boolean);
    procedure ActivateProjectFromDB(var Msg: TMessage); message cm_First + cm_ChangeProject;
    function CheckCDInDrive: Boolean;
    procedure CheckConnection(var Msg: TMessage); message cm_First + cm_ChkConnection;
    procedure CloseProjectFromDB(var Msg: TMessage); message cm_First + cm_CloseProject;
    procedure ExecDDEFile(AFileName: AnsiString);
    procedure DoDDECommands(SendedDDEString: PChar);
    procedure EndDDEMLSeq(var Msg: TMessage); message cm_First + cm_EndDDEMLSeq;
    procedure GisCommAddDBEntry(var Msg: TMessage); message cm_First + cm_AddDBEntry;
    procedure GisCommReadDDEWMIniSettings(var Msg: TMessage); message cm_First + cm_CGReadDDEWMIni;
    procedure GisCommReadDDEMLServerIniSettings(var Msg: TMessage); message cm_First + cm_CGReadDDEMLSer;
    procedure GisCommReadDDEMLIniSettings(var Msg: TMessage); message cm_First + cm_CGReadDDEMLIni;
    procedure GisCommGetDDECommand(var Msg: TMessage); message cm_First + cm_GetNewDDEString;
    procedure GisCommGetLayerName(var Msg: TMessage); message cm_First + cm_GetLayerName;
    procedure GisCommGetTopLayerName(var Msg: TMessage); message cm_First + cm_GetTopLayerName;
    procedure GisCommPostRDRCommAfterDDE(var Msg: TMessage); message cm_First + cm_PostRDRCommADDE;
    procedure GisCommSendProjDataToDB(var Msg: TMessage); message cm_First + cm_SendProjData;
    procedure GisCommSendRDRCommAfterDDE(var Msg: TMessage); message cm_First + cm_SendRDRCommADDE;
    procedure GisCommSetAutoInsert(var Msg: TMessage); message cm_First + cm_SetAutoInsert;
    procedure GisCommSetDBSelMode(var Msg: TMessage); message cm_First + cm_SetSelMode;
    procedure GisCommSetOneSelect(var Msg: TMessage); message cm_First + cm_SetOneSelect;
    procedure GisCommSetStatusText(var Msg: TMessage); message cm_First + cm_SetStatusText;
    procedure GisCommSetTRData(var Msg: TMessage); message cm_First + cm_SetTRData;
    procedure GisCommShowMsgBox(var Msg: TMessage); message cm_First + cm_DoShowMessage;
    procedure GisCommWriteDDEDDEMLSettings(var Msg: TMessage); message cm_First + cm_WriteDDEDDEML;
    procedure GisCommWriteLog(var Msg: TMessage); message cm_First + cm_WriteLog;
    procedure ListOpenAndActiveProjects(var Msg: TMessage); message cm_First + cm_OpActProjs;
    procedure PostRDRCommAfterDDE;
    procedure SendRDRCommAfterDDE;
    procedure OpenProjectsFromDB(var Msg: TMessage); message cm_First + cm_OpenProjects;
    procedure SetWinGISMainFormSize(var Msg: TMessage); message cm_First + cm_SetMainWinSize;
    procedure WMDDEAck(var Msg: TMessage); message wm_First + wm_DDE_Ack;
    procedure WMDDEExecute(var Msg: TMessage); message wm_First + wm_DDE_Execute;
    procedure WMDDEInitiate(var Msg: TMessage); message wm_First + wm_DDE_Initiate;
    procedure WMDDERequest(var Msg: TMessage); message wm_First + wm_dde_Request;
    procedure WMDDETerminate(var Msg: TMessage); message wm_First + wm_DDE_Terminate;
{$ENDIF} // <----------------- AXDLL
       //Procedure   Destroy3DConnection(A3DConnection:TThreeDChild);
{$IFNDEF AXDLL} // <----------------- AXDLL
    procedure FileExitFromDB(var Msg: TMessage); message cm_First + cm_ExitFromDB;
    procedure FilePostOpen1(var Msg: TMessage); message cm_First + cm_PostOpen;
    procedure LoadNextProj(var Msg: TMessage); message cm_First + cm_LoadNextProj;
    procedure FilePostOpen(AStr: PChar);
    procedure MsgFileOpen(var Msg: TMessage); message cm_FileOpen;
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
    function CountChildren: Integer;
    procedure FileNameClose(AFileName: PChar);
{$IFNDEF AXDLL} // <----------------- AXDLL
    function FileNameOpen(AFileName: PChar; Step: Integer; bMU: Boolean): Boolean;
    function FileNameOpenNOLOCK(AFileName: PChar): Boolean;
{$ENDIF} // <----------------- AXDLL
    function GetCurrentChild: TMDIChild;
    function IsFileOpened(AFName: PChar): TMDIChild;
{$IFNDEF AXDLL} // <----------------- AXDLL
    function OpenProjects: Boolean;
    procedure SendDDEToDkmApp(KGList, GSTList: TStringList);
{$ENDIF} // <----------------- AXDLL
{$IFDEF LBG}
{$ENDIF}
  private
{$IFNDEF AXDLL} // <----------------- AXDLL
    bWeAreUsingTheIDBInfoWindow: Boolean; //

    bWeAreUsingTheIDB: Boolean; // When this flag is set in TRUE state, I mean WinGIS uses the Internal Database kernel. Ivanoff.
    bWeAreUsingTheIDBUserInterface: Boolean; // When this flag is set in TRUE state, I mean WinGIS uses the Internal Database user interface.
                                                // This flag is ignored if bWeAreUsingTheIDB falg is set in FALSE state. Ivanoff.
    bWeAreUsingUndoFunction: Boolean; // When this flag is set to TRUE state I mean WinGIS uses the Internal Database
                                         // as base for an undo/redo function. This flag is ignored if bWeAreUsingTheIDB flag is set in FALSE state. Ivanoff.
    iIDBUndoStepsCount: integer;
{$IFNDEF WMLT}
    TDocumentList: TDocumentEntry;
    CurrentTDocument: TDocumentEntry;
    CurrentTDocumentFileName: string;
    GisModules: array of THandle;
{$ENDIF}
{$ENDIF} // <----------------- AXDLL
    procedure UpdateActiveMDIChild;
    procedure DoActivateApplication(Sender: TObject);
  public
    FutureName: PChar; // Sygsky for image relative name support
    CompName: PChar;
    InLoadingFromDB: Boolean;
{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}
    PmsProject: AnsiString;
    NextProjIsReadOnly: Boolean;
    procedure NewTDocument(Documentname: string);
    function GetTDocumentByIndex(index: integer): TDocumentEntry;
    function GetTDocumentByName(Documentname: string): TDocumentEntry;
    procedure ActivateWingis;
    procedure ExecuteMenuFunction(ItemName: AnsiString);
    procedure ActivateModule(var aModuleDll: THandle; DllName: string; DllIdx: integer; LngCode: string); // activate a Gis-Module
    procedure DeactivateModule(var aModuleDll: THandle);
{$ENDIF}
{$ENDIF} // <----------------- AXDLL
    procedure HandleException(Sender: TObject; E: Exception);
{$IFNDEF AXDLL} // <----------------- AXDLL
    procedure IdleProc(Sender: TObject; var Done: Boolean);
{$ENDIF} // <----------------- AXDLL
  protected
    procedure WndProc(var Message: TMessage); override;
  published
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
    procedure FileNew;
    procedure ExtrasRegistration;
    procedure FileSaveAll;
    procedure HelpUseHelp;
    procedure HelpActivate;
       {++brovak}
    procedure SetDefUserInterfaceMain;

       {--brovak}
    procedure WindowArrangeIcons;
    procedure WindowCascade;
    procedure WindowCloseAll;
    procedure WindowTile;
    procedure ConvertToWgpWithLayers(SaveOptions: TSaveAsOptions; const SerialNumber, InpFileName,
      HspFileName, HsdFileName: string; HspList, HsdList, OutFileLIst: TStringList; DrawIt, Close: Boolean);
    procedure ConvertToWgp(SaveOptions: TSaveAsOptions; const SerialNumber,
      AOutFileName, AWgpFileName: string; AList: TStringList; DrawIt, Close: Boolean);
    procedure TurboVektor;
    procedure ExtrasIUpdate;
    procedure ExtrasGetAddons;
    procedure ExtrasTABConverter;
    procedure ExtrasLanguage;
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
{$IFNDEF AXDLL} // <----------------- AXDLL
    function FileNameNew(AName: string): Pointer;
{$ENDIF} // <----------------- AXDLL
    procedure DefaultMenuHandler(Sender: TObject; MenuFunction: TMenuFunction);
{$IFNDEF AXDLL} // <----------------- AXDLL
    procedure FileOpen;
    procedure MUFileOpen;
    procedure FileOpenFromURL(AUrl: AnsiString);
{$ENDIF} // <----------------- AXDLL
    procedure FileQuit;
{$IFNDEF AXDLL} // <----------------- AXDLL
    procedure HelpAbout;
{$ENDIF} // <----------------- AXDLL
    procedure HelpContents;
    procedure OnGlobalOptionsChanged;
{$IFNDEF AXDLL} // <----------------- AXDLL
    procedure OptionsPassword;
    procedure ViewToolbars;

    procedure FileNameCloseHSP(AFileName: PChar); // Difference : Sendmessage!
    function OpenProjectTV(ALock: Boolean; AFileName: string): Boolean;
    procedure CheckLibStatus(Handle: Integer; Filename, Info: AnsiString);
    function GetWingisVersion: AnsiString;
{$ENDIF} // <----------------- AXDLL
    procedure SetAppTitle;
    procedure CleanDir;
    function IsOnline: Boolean;
    function ReadRemoteSettings: Boolean;
    function UseDkmApp: Boolean;
  end;

var
  WinGISMainForm: TWinGISMainForm;
  ThematicDLLHandle: Integer = 0;
  ISOLibHandle: Integer = 0;
  ResetUIOnStart: Boolean = False;
{++Brovak
Please, don't disable this declaration !
If you have any trouble with it - write me to brovak@progis.ru}
  CheckHandle: THandle;
{--Brovak}

{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}
procedure ExitIfAlreadyRunning;
{$ENDIF}
{$ENDIF} // <----------------- AXDLL

function CoreConnected: Boolean;

implementation

{$IFNDEF AXDLL} // <----------------- AXDLL
{$R *.DFM}
{$R win7.res}
{$R WG2010.res}
{$ELSE}
{$R C:\WingisSource\AXDll\AM_Main.DFM}
{$ENDIF} // <----------------- AXDLL
{$R menufn.mfn}

uses AM_ProjM, PrnRep, FileCtrl, UHeap, UFileH, AM_ImagC, CommonResources, Var_Cont, ListHndl, MenuHndl, WinInet, Registry,
  AM_Paint, AM_Layer, VEModule, MUModule, MUSyncDlg, Share, Buttons, WinOSInfo, MUOpenDlg, AddOnsDlg, LicenseHndl, ErrHndl,
  GPSConn, WMSModule, AM_ProjO
{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}
{++ IDB}
  , Tooltips, IDB_Messages, GisModuleDlg
{-- IDB}
  , Wingis_TLB, comobj, ActiveX, ShellApi, SymHndl
{$ELSE}
  , LTAboutDlg
{$ENDIF}
{$ENDIF} // <----------------- AXDLL
  ;

{!?!?!Route Procedure LoadStringsInRouteDll(MfName:String); far; external 'ROUTEDLL' Index 7;
Procedure DelStringsInRouteDll;                 far; external 'ROUTEDLL' Index 8;}

{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}

procedure ExitIfAlreadyRunning;
var
  PrevWnd: HWND;
  aAtom: ATOM;
begin
  CheckHandle := CreateMutex(nil, True, ProgisAppID);
  if GetLastError = ERROR_ALREADY_EXISTS then
  begin
    PrevWnd := FindWindow('TWingisMainForm', nil);
    if PrevWnd <> 0 then
    begin
      SetForeGroundWindow(PrevWnd);
      if ParamCount > 0 then
      begin
        aAtom := GlobalAddAtom(PChar(ParamStr(1)));
        if aAtom <> 0 then
        begin
          PostMessage(PrevWnd, cm_FileOpen, 0, aAtom);
        end;
      end;
    end;
    Halt;
  end;
end;

procedure TWinGISMainForm.HelpActivate;
begin
  AddLicense;
  WingisLicense := GetWingisLicense;
  SetAppTitle;
end;
{$ENDIF}
{$ENDIF} // <----------------- AXDLL

function CoreConnected: Boolean;
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}
  Result := (WingisMainForm <> nil) and (WingisMainForm.DXAppTCore <> nil) and (ComConnected);
  if Result then
  begin
    RunUpdateOnClose := False;
  end;
{$ENDIF}
{$ENDIF} // <----------------- AXDLL
end;

procedure TWinGISMainForm.HandleException(Sender: TObject; E: Exception);
begin
  DebugMsg('Exception', E.Message);
end;

procedure TWinGISMainForm.FormCreate(Sender: TObject);
{$IFNDEF AXDLL} // <----------------- AXDLL
var
  AServer: IUnknown;
  i: Integer;
{$ENDIF} // <----------------- AXDLL
begin
{$IFDEF AXDLL} // <----------------- AXDLL
  ThreeDActive := FALSE;
  LoadedLibs := TList.Create;
  CreateNewProj := FALSE;
  SetRedraw := FALSE;
  IsSaving := False;
{$ELSE} // <----------------- AXDLL
  Application.OnException := HandleException;
  Application.OnIdle := IdleProc;
  RunRemote := ReadRemoteSettings;
  SetAppTitle;
  NextProjIsReadOnly := False;
  ParamPwd := '';
  CloseFileOnReload := True;
  HideWindow := False;
{$IFNDEF WMLT}
  ImpTxtLib := 0;
  ImpExpDllData := nil;
  NewFileCount := 0;
    //ActChild3D:=NIL;
    //ThreeDActive:=FALSE;
{$ENDIF}
    { Statuszeile erzeugen }
  InitStatusBar;
  Application.OnActivate := DoActivateApplication;
    { Inifile initialisieren }
  IniFile := New(PIniFile, Init(Self));
    { initialize toolbars }
  UserInterface.IniFileName := StrPas(IniFile^.IniFile);
    { Men� f�r das Hauptfenster setzen und File-History einblenden             }
  FileHistory.MaxEntryCount := IniFile^.Options.HistSize;
  FileHistory.IniFileName := StrPas(IniFile^.IniFile);
    { Dezimal- und Tausendertrennzeichen umstellen }
  DecimalSeparator := '.';
  ThousandSeparator := ',';
    //IntroWin:=TIntroWindow.Init(Self); //old intro window
  LoadedLibs := TList.Create;
  CreateNewProj := FALSE;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
  LoadingProj := nil;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
    //IntroPlaying:=FALSE;
    // set help-file-name
  Application.HelpFile := GetHelpFileName;
    // create other things
  ReadConstructOpt;
{$IFNDEF WMLT}
  GPS.Init;
  DDEHandler := TDDEHandler.Create(Self);
  TurboRasterDDE := New(PTurboRasterDDE, Init(Self));
{$ELSE}
  LTOpenFile := IniFile^.ReadBoolean('LTSettings', 'LTOpenFile', False);
  if not LTOpenFile then
  begin
    MenuFunctions['FileOpen'].EnableGroup := 0;
    MenuFunctions['FileClose'].EnableGroup := 0;
  end;
{$ENDIF}
    // setup common-resources
  CommonListBitmaps := CommonListImages;
  UseMarkSnapPoint := True; //IniFile^.ReadBoolean('SETTINGS','MarkSnapPoint',True);
  SetRedraw := FALSE;
  if RunAsCanvas then
  begin
    HideWindow := True;
  end;
{$IFNDEF WMLT}
  try
    GetActiveObject(Class_Core, nil, AServer);
    if AServer <> nil then
    begin
      DXApp := AServer as ICore;
    end
    else
    begin
      DXAppTCore := TCore.Create;
      DXApp := IDispatch(DXAppTCore);
    end;
  except
    DXAppTCore := nil;
  end;

  TDocumentList := nil;
  CurrentTDocument := nil;

  if ResetUIOnStart then
  begin
    UserInterface.SetDefUserInterface(nil);
    WingisMainForm.WindowState := wsMaximized;
  end;

{$ENDIF}
  InLoadingFromDB := False; // Mark that now we are  in loading from DB process
{$IFNDEF WMLT}
{++ IDB}
  bWeAreUsingTheIDBInfoWindow := FALSE;
    {Read the Internal Database settings from the WinGIS ini-file. Ivanoff.}
//    SELF.bWeAreUsingTheIDB := IniFile.ReadBoolean('InternalDatabase', 'IDB', FALSE);
//    If bWeAreUsingTheIDB Then Begin
//       SELF.bWeAreUsingTheIDBUserInterface := IniFile.ReadBoolean('InternalDatabase', 'IDB_Interface', FALSE);
// ++ Cadmensky
//       SELF.bWeAreUsingUndoFunction := IniFile.ReadBoolean('InternalDatabase', 'IDB_UnDo', FALSE);
//       SELF.iIDBUndoStepsCount := IniFile.ReadInteger('InternalDatabase', 'IDB_UndoStepsCount', 500);
// -- Cadmensky
//       End
//    Else Begin
       // If we don't use the IDB kernel, we'll don't use also the IDB user interface and UNDO function.
  SELF.bWeAreUsingTheIDBUserInterface := FALSE;
  SELF.bWeAreUsingUndoFunction := FALSE;
//       SELF.iIDBUndoStepsCount := IniFile.ReadInteger('InternalDatabase', 'IDB_UndoStepsCount', 500);
//       End;
    {If we need the Internal Database kernel, I create the IDB manager. Ivanoff.}
{$IFNDEF WMLT}
  IDBGlobalOnOff := True;
  IDB_Man := nil;
  if SELF.bWeAreUsingTheIDB then
  begin
    IDB_Man := TIDB.Create;
    if not WinGISMainForm.IDB_Man.LibraryWasLoaded then
    begin
          // If the library loading failed, delete IDB manager object.
      WinGISMainForm.bWeAreUsingTheIDB := FALSE;
      WinGISMainForm.bWeAreUsingTheIDBUserInterface := FALSE;
//          WinGISMainForm.bWeAreUsingUndoFunction := FALSE;
//          WinGISMainForm.iIDBUndoStepsCount := 500;
      WinGISMainForm.IDB_Man.Free;
      WinGISMainForm.IDB_Man := nil;
    end;
  end;
// ++ Cadmensky

  SELF.bWeAreUsingUndoFunction := IniFile.ReadBoolean('UndoRedo', 'IDB_Undo', FALSE);
  if bWeAreUsingUndoFunction then
    SELF.iIDBUndoStepsCount := IniFile.ReadInteger('UndoRedo', 'IDB_UndoStepsCount', 500)
  else
  begin
       // If we don't use the IDB kernel, we'll don't use also the IDB user interface and UNDO function.
    SELF.bWeAreUsingUndoFunction := FALSE;
    SELF.iIDBUndoStepsCount := IniFile.ReadInteger('InternalDatabase', 'IDB_UndoStepsCount', 500);
  end;

  UndoRedo_Man := nil;
  if SELF.bWeAreUsingUndoFunction then
  begin
    UndoRedo_Man := TUndoRedoManager.Create;
    if not WinGISMainForm.UndoRedo_Man.LibraryWasLoaded then
    begin
          // If the library loading failed, delete IDB manager object.
//          WinGISMainForm.bWeAreUsingTheIDB := FALSE;
//          WinGISMainForm.bWeAreUsingTheIDBUserInterface := FALSE;
      WinGISMainForm.bWeAreUsingUndoFunction := FALSE;
      WinGISMainForm.iIDBUndoStepsCount := 500;
      WinGISMainForm.UndoRedo_Man.Free;
      WinGISMainForm.UndoRedo_Man := nil;
    end;
  end;
// -- Cadmensky
{$ENDIF}
{-- IDB}
{$ENDIF}
{++ Tooltips DLL}
  DTI_Man := TDTI.Create;
{-- Tooltips DLL}
{$ENDIF} // <----------------- AXDLL

  PrintReport := TPrintReport.Create(Self);
  VEForm := TVEForm.Create(Self);
  WMSForm := TWMSForm.Create(Self);
  MUSyncForm := TMUSyncForm.Create(Self);
  GPSConnection := TGPSConnection.Create;

    //Scaleby(Trunc(9600000/(PixelsPerInch)),100000);
end;

procedure TWinGISMainForm.FormDestroy(Sender: TObject);
begin
{$IFDEF AXDLL} // <----------------- AXDLL
  LoadedLibs.Free;
{$ELSE} // <----------------- AXDLL

{$IFNDEF WMLT}

{++ IDB}
  if TooltipThread <> nil then
  begin
    TooltipThread.Suspend; // suspend before free, otherwise free fails...
    TooltipThread.FreeOnTerminate := true;
    TooltipThread.Terminate;
//    TooltipThread.Free;
    TooltipThread := nil;
  end;

  if SELF.IDB_Man <> nil then
{        // This is used to close all project (only project) windows.
        // It is necessary because TWinGismainForm.FormDestroy event happens BEFORE (!!!)
        // TMDIChild.FormClose and TMDIChild.FormDestroy events!!!! And I use TMDIChild.FormClose
        // event to delete all information about a project from the Internal database manager.
        For iI:=0 To SELF.MDIChildCount-1 Do
            If SELF.MDIChildren[iI] IS TMDIChild Then Begin
               TMDIChild(SELF.MDIChildren[iI]).Close;
               Application.ProcessMessages;
               End;}
  try
    if Self.IDB_Man <> nil then
    begin
      SELF.IDB_Man.Destroy;
    end;
  finally
    SELF.IDB_Man := nil;
  end;
{-- IDB}
// ++ Cadmensky
{++ UndoRedo}
  try
    if SELF.UndoRedo_Man <> nil then
    begin
      SELF.UndoRedo_Man.Free;
    end;
  finally
    SELF.UndoRedo_Man := nil;
  end;
{-- UndoRedo}
// -- Cadmensky
{++ Tooltips DLL}
  if DTI_Man <> nil then
  begin
    DTI_Man.Free;
    DTI_Man := nil;
  end;
{-- Tooltips DLL}
  if (DDEHandler <> nil) {and not CloseFromDB} then
  begin
    DDEHandler.SendString('[CLO][]');
  end;
  if g_hColorPalette {LogPal256} <> 0 {NIL} then
  begin
    ReloadWinPalette; {01-005-100399}
  end;
  if TurboRasterDDE <> nil then
  begin
    Dispose(TurboRasterDDE, Done);
    TurboRasterDDE := nil;
  end;
  if DDEHandler <> nil then
  begin
    DDEHandler.Destroy;
    DDEHandler := nil;
  end;
{$ENDIF}
  if IniFile <> nil then
  begin
    Dispose(IniFile, Done);
    IniFile := nil;
  end;
  if LoadedLibs <> nil then
  begin
    LoadedLibs.Free;
    LoadedLibs := nil;
  end;
{$ENDIF} // <----------------- AXDLL
  if PrintReport <> nil then
  begin
    PrintReport.Free;
    PrintReport := nil;
  end;
  if VEForm <> nil then
  begin
    VEForm.Free;
    VEForm := nil;
  end;
  if WMSForm <> nil then
  begin
    WMSForm.Free;
    WMSForm := nil;
  end;
  if MUSyncForm <> nil then
  begin
    MUSyncForm.Free;
    MUSyncForm := nil;
  end;
  if GPSConnection <> nil then
  begin
    GPSConnection.Free;
    GPSConnection := nil;
  end;
  CleanDir;
end;

{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}

procedure TWinGISMainForm.FileNew;
var
  S: TOLEClientStream;
  Proj: PProj;
  FileName: array[0..1023] of Char;
  IDBFileName: AnsiString;
  Child: TMDIChild;
  TemplateFile: AnsiString;
  Ini: TIniFile;
begin
  TemplateFile := '';
//    if NewDialog.Execute then begin
  Ini := TIniFile.Create(OSInfo.WingisIniFileName);
  if Ini <> nil then
  begin
    TemplateFile := Ini.ReadString('Settings', 'TemplateFile', TemplateFile);
    Ini.Free;
  end;
      //if not FileExists(TemplateFile) then
         //TemplateFile:=CurrentDirectory + 'Default.amp';
//      StrPCopy(FileName,NewDialog.FileName);
  Inc(NewFileCount);
  StrPCopy(FileName, OSInfo.TempDataDir + GetLangText(20000) + IntToStr(NewFileCount) + '.amp');
  IDBFileName := ChangeFileExt(FileName, '.wgi');
  if FileExists(IDBFileName) then
  begin
    DeleteFile(PChar(IDBFileName));
  end;
  IDBFileName := ChangeFileExt(FileName, '.ldb');
  if FileExists(IDBFileName) then
  begin
    DeleteFile(PChar(IDBFileName));
  end;
  if CompareText(ExtractFileName(NewDialog.FileName), TemplateFile) <> 0 then
  begin
    S := TOLEClientStream.Create(TemplateFile, stOpenRead);
//    S.Init(PChar(TemplateFile), stOpenRead, StrBufSize);
    S.ParentWindow := nil;
    if S.Status <> stOK then
    begin
          //MsgBox(Handle,2014,mb_OK or mb_IconExclamation,'');
      Proj := New(PProj, Init);
    end
    else
      Proj := Pointer(S.Get);
    S.Free;
  end
  else
    Proj := New(PProj, Init);

  Child := IsFileOpened(FileName);
  if Child <> nil then
  begin
    Child.Data^.Modified := FALSE;
    Child.Close;
  end;

{++ IDB}
  Proj.bProjectIsNewAndNoSaved := TRUE;
{-- IDB}

       {+++ brovak  BUILD#114   enhancement}
  FileHistory.NoAddHistoryFlag := true;
      {-- brovak}
  CreateNewProj := TRUE;
  Actualchild := TMDIChild.Create(Self, FileName, Proj, False);
  if ActualChild <> nil then
    ActualChild.HasTitle := False;
  CreateNewProj := FALSE;
  NewTDocument(strpas(FileName));
  ActualChild.Data^.SetModified;
       {+++ brovak  BUILD#114   enhancement}
  FileHistory.NoAddHistoryFlag := False;
       {-- brovak}
//    end;
end;
{$ENDIF}
{$ENDIF} // <----------------- AXDLL

{$IFNDEF AXDLL} // <----------------- AXDLL

function TWinGISMainForm.OpenProjects
  : Boolean;
var
  Params: Word;
  ProjToOpen: array[0..fsPathName] of Char;
  bLastParamWasCommand: boolean;
begin
{$IFNDEF WMLT}
  Result := FALSE;
  if ParamCount > 0 then
  begin
      {++ aschupp: new nostart+ parameter implementation BUILD#116}
      {example: wingis.exe world.amp /c nostart=WINMON}
    bLastParamWasCommand := FALSE;
    for Params := 1 to ParamCount do
    begin
      if Lowercase(ParamStr(Params)) = STARTPARAM_CANVAS then
      begin
        bLastParamWasCommand := TRUE;
      end;
      if Lowercase(ParamStr(Params)) = STARTPARAM_PMS then
      begin
        bLastParamWasCommand := TRUE;
      end;
      if Lowercase(ParamStr(Params)) = STARTPARAM_SINGLEPROJ then
      begin
        SingleProj := TRUE;
        bLastParamWasCommand := TRUE;
      end;
         // dont interpret a param as project if it = '/c'
      if Lowercase(ParamStr(Params)) = STARTPARAM_COMMAND then
      begin
        bLastParamWasCommand := TRUE;
      end
      else
        if Lowercase(ParamStr(Params)) = STARTPARAM_PASSWORD then
        begin
          bLastParamWasCommand := TRUE;
        end
        else
        begin
            // dont interpret a param as project if the param before was '/c'
          if bLastParamWasCommand then
            bLastParamWasCommand := FALSE
          else
          begin
            StrPCopy(ProjToOpen, ParamStr(Params));
            if (ProjToOpen[0] <> '-') and ((ProjToOpen[0] <> '/')) then
            begin
              if FileNameOpen(ProjToOpen, 0, False) then
              begin
                NewTDocument(ParamStr(Params));
              end;
            end;
          end;
        end;
    end;
      {-- aschupp}
    Result := TRUE;
  end;
{$ELSE}
  OpenLTProject;
{$ENDIF}
end;
{$ENDIF} // <----------------- AXDLL

{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}

procedure TWinGISMainForm.OpenProjectsFromDB
  (
  var Msg: TMessage
  );
var
  OFString: PChar;
  MoreEntries: Boolean;
  HasEntry: Boolean;
  ParamNr: Integer;
  Cnt: Integer;
begin
  if CheckCDInDrive then
  begin
    HasEntry := FALSE;
    ParamNr := 0;
    repeat
      ReadDDECommandPChar(MWDDELine1, 1, ParamNr, WgBlockStart, DDEHandler.FDDESepSign,
        WgBlockEnd, OFString, MoreEntries);
      if SingleProj then
      begin
        for Cnt := 0 to MDIChildCount - 1 do
        begin
          if (MDIChildren[Cnt] <> nil) and (MDIChildren[Cnt].ClassName = MDIChildType) then
          begin
            if Uppercase(TMDIChild(MDIChildren[Cnt]).FName) <> Uppercase(OFString) then
            begin
              TMDIChild(MDIChildren[Cnt]).Close;
              Application.ProcessMessages;
            end;
          end;
        end;
      end;

      if (OFString <> nil) and (StrComp(OFString, '') <> 0) then
      begin
        HasEntry := TRUE;
        if FileNameOpen(OFString, 0, False) then
        begin
          NewTDocument(strpas(OFString));
        end;
        StrDispose(OFString);
        Inc(ParamNr);
      end;
    until not MoreEntries;
    if not HasEntry and DDEHandler.FSendMWR and FirstLoad then
    begin
      InLoadingFromDB := True;
      try
        OpenProjects;
      finally
        InLoadingFromDB := False;
      end;
      FirstLoad := False;
    end;
  end
  else
    PostMessage(Handle, wm_Close, 0, 0);
end;

procedure TWinGISMainForm.ActivateProjectFromDB
  (
  var Msg: TMessage
  );
var
  CFStringOK: Boolean;
  CFString: PChar;
  MoreEntries: Boolean;
  AChild: TMDIChild;
  Dir: array[0..fsPathName] of Char;
  DName: array[0..fsFileName + fsExtension] of Char;
  Ext: array[0..fsExtension] of Char;
  SendPath: Boolean;
  DDEString: string;
  FullFileName: array[0..255] of Char;
begin
  CFStringOK := ReadDDECommandPChar(MWDDELine1, 1, 0, WgBlockStart, DDEHandler.FDDESepSign,
    WgBlockEnd, CFString, MoreEntries);
  if CFStringOK and (CFString <> nil) and (StrComp(CFString, '') <> 0) then
  begin
    FileSplit(CFString, Dir, DName, Ext);
    if StrComp(Dir, '') <> 0 then
    begin
      SendPath := TRUE;
      StrCopy(FullFileName, CFString);
    end
    else
    begin
      SendPath := FALSE;
      FileExpand(FullFileName, CFString);
    end;
    if StrComp(Ext, '') = 0 then
    begin
      StrCat(FullFileName, '.amp');
    end;
    AnsiLower(FullFileName);
    AChild := IsFileOpened(FullFileName);
    if AChild <> nil then
      BringWindowToTop(AChild.Handle);
    AChild := GetCurrentChild;
    StrCopy(FullFileName, AChild.FName);

    FileSplit(FullFileName, Dir, DName, Ext);
    DDEString := '[NPR][';
    if SendPath then
      DDEString := DDEString + StrPas(Dir);
    DDEString := DDEString + StrPas(DName);
      {if SendExt then}DDEString := DDEString + StrPas(Ext);
    DDEString := DDEString + ']';
    DDEHandler.SendString(DDEString);
  end;
end;

procedure TWinGISMainForm.CloseProjectFromDB
  (
  var Msg: TMessage
  );
var
  ProjToClose: array[0..fsPathName] of Char;
  OFString: string;
  StrToLong: Boolean;
  MoreStrs: Boolean;
  StrOK: Boolean;
  ParamNr: Integer;
  hFileName: string;
begin
  SetFocus;
  ParamNr := 0;
  repeat
    StrOK := ReadDDECommandString(MWDDELine1, 1, ParamNr, WgBlockStart, DDEHandler.FDDESepSign,
      WgBlockEnd, OFString, StrToLong, MoreStrs);
    if StrOK and (OFString <> '') then
    begin
      hFileName := (OFString);
      if (AnsiUpperCase(ExtractFileExt(HFileName)) = '.AMP') or
        (AnsiUpperCase(ExtractFileExt(HFileName)) = '.SSP') then
      begin
        FileNameClose(StrPCopy(ProjToClose, OFString));
      end;
      if Ansiuppercase(ExtractFileExt(hFileName)) = '.HSP' then
      begin
        FileNameCloseHSP(StrPCopy(ProjToClose, OFString));
      end;
      Inc(ParamNr);
    end;
  until not MoreStrs;
end;

procedure TWinGISMainForm.FilePostOpen
  (
  AStr: PChar
  );
begin
  StrCopy(NextProj, AStr);
  PostMessage(Handle, wm_Command, cm_PostOpen, 0);
end;

procedure TWinGISMainForm.FilePostOpen1(var Msg: TMessage);
begin
  if FileNameOpen(NextProj, 0, False) then
  begin
    NewTDocument(strpas(NextProj));
  end;
end;

procedure TWingisMainForm.MsgFileOpen(var Msg: TMessage);
var
  Name: array[0..255] of Char;
begin
  if GlobalGetAtomName(Msg.LParam, Name, Length(Name)) > 0 then
  begin
    if Name[0] <> '/' then
    begin
      if FileNameOpen(Name, 0, False) then
      begin
        NewTDocument(strpas(Name));
      end;
    end;
  end;
end;
{$ENDIF}
{$ENDIF} // <----------------- AXDLL

function TWinGISMainForm.IsFileOpened(AFName: PChar): TMDIChild;
var
  Cnt: Integer;
begin
  IsFileOpened := nil;
  for Cnt := 0 to MDIChildCount - 1 do
    if (MDIChildren[Cnt].ClassName = MDIChildType)
      and (StrIComp(TMDIChild(MDIChildren[Cnt]).FName, AFName) = 0) then
    begin
      Result := TMDIChild(MDIChildren[Cnt]);
      Exit;
    end;
end;

{$IFNDEF AXDLL} // <----------------- AXDLL

procedure TWinGISMainForm.FileOpen;
var
  FileName: array[0..fsPathName] of Char;
  hFileName: string;
begin
  if OpenFile(Handle, hFileName) then
  begin
    StrPCopy(FileName, hFileName);
    FileHistory.NoAddHistoryFlag := False;
    if FileNameOpen(FileName, 0, False) then
    begin
{$IFNDEF WMLT}
      NewTDocument(strpas(FileName));
{$ENDIF}
    end;
  end;
end;

procedure TWinGISMainForm.MUFileOpen;
begin
  FileOpenFromURL('');
end;

procedure TWinGISMainForm.FileOpenFromURL(AUrl: AnsiString);
var
  AFileName: AnsiString;
begin
  if (AUrl = '') or (MU.LastServerAddress = '') then
  begin
    MUOpenForm := TMUOpenForm.Create(Self);
    try
      if MUOpenForm.ShowModal = mrOK then
      begin
        AUrl := MUOpenForm.ProjectUrl;
      end
      else
      begin
        AUrl := '';
      end;
    finally
      MUOpenForm.Free;
    end;
  end;
  if (AUrl <> '') and (MU.LastServerAddress <> '') and (MU.DownloadProject(AUrl, AFileName)) then
  begin
    FileHistory.NoAddHistoryFlag := True;
    MU.LoadingProjectUrl := AUrl;
    if (FileExists(AFileName)) and (FileNameOpen(PChar(AFileName), 0, True)) then
    begin
      NewTDocument(AFileName);
    end;
  end;
end;

function TWinGISMainForm.FileNameOpen(AFileName: PChar; Step: Integer; bMU: Boolean): Boolean;
var
  Dir: array[0..fsPathName] of Char;
  DName: array[0..fsFileName + fsExtension] of Char;
  Ext: array[0..fsExtension] of Char;
  FullFileName: array[0..255] of Char;
  AChild: TMDIChild;
  ResultHsp: Boolean;
  i: Integer;
  DDEParams: array[1..3] of string;
  DDELine: AnsiString;
begin
  if Pos('http://', AnsiString(AFileName)) = 1 then
  begin
    FileOpenFromUrl(AFileName);
    Result := False;
    exit;
  end;
  DDELine := AnsiString(AFileName);
  DDELine := StringReplace(DDELine, ' /PWD ', '|', [rfIgnoreCase]);
  if ExtractStringItems(DDEParams, DDELine, '|') > 1 then
  begin
    AFileName := PChar(AnsiString(DDEParams[1]));
    ParamPwd := DDEParams[2];
  end
  else
    ParamPwd := '';

  ResultHsp := TRUE;
  FileExpand(FullFileName, AFileName);
  AnsiLower(FullFileName);
  FileSplit(FullFileName, Dir, DName, Ext);
  if StrComp(Ext, '') = 0 then
    Ext := '.amp';

  StrCopy(FullFileName, Dir);
  StrCat(FullFileName, DName);
  StrCat(FullFileName, ext);

  AChild := IsFileOpened(FullFileName);
  if AChild <> nil then
  begin
{$IFNDEF WMLT}
    AChild.InitDB('PRO', True); {?}
{$ENDIF}
    Result := False;
    Exit;
  end;
  if CheckFileExist(FullFileName) then
  begin
    for i := 1 to ParamCount - 1 do
    begin
      if ParamStr(i) = STARTPARAM_PASSWORD then
        ParamPwd := ParamStr(i + 1);
    end;
     // look if the file is write-protected
    if not HideWindow then
      FutureName := FullFileName;
    ActualChild := TMDIChild.Create(Self, FullFileName, nil, bMU);
    if (ActualChild <> nil) and (ActualChild.Data <> nil) then
    begin
      if Step = 1 then
      begin
{$IFNDEF WMLT}
        NewTDocument(AFileName);
{$ENDIF}
      end;
    end;
    AChild := ActualChild;
    if (AChild <> nil) and (AChild.Data <> nil) then
    begin
      ActualChild.Data^.Main := @Self;
      if AnsiCompareText(StrPas(Ext), '.HSP') = 0 then
        ResultHsp := OpenProjectTV(TRUE, StrPas(FullFileName));
           {ResultHSP ist nur dann FALSE wenn HSP wegen Fehler nicht geladen werden konnte}
    end;
  end
  else
  begin
    MsgBox(Handle, 1013, mb_IconExclamation or mb_OK, StrPas(DName));
  end;
  if not ResultHSP then
    Result := FALSE
  else
    Result := AChild <> nil;
end;

function TWinGISMainForm.FileNameOpenNOLOCK(AFileName: PChar): Boolean;
var
  Dir: array[0..fsPathName] of Char;
  DName: array[0..fsFileName + fsExtension] of Char;
  Ext: array[0..fsExtension] of Char;
  AChild: TMDIChild;
  REsultHsp: Boolean;
  FullFileName: array[0..255] of Char;
begin
  ResultHsp := true;
  FileExpand(FullFileName, AFileName);
  AnsiLower(FullFileName);
  FileSplit(FullFileName, Dir, DName, Ext);
  if StrComp(Ext, '') = 0 then
    Ext := '.amp';
  StrCopy(FullFileName, Dir);
  StrCat(FullFileName, DName);
  StrCat(FullFileName, ext);
  AChild := IsFileOpened(FullFileName);
  if AChild <> nil then
  begin
    ActualChild := AChild;
    BringWindowToTop(AChild.Handle);
  end
  else
    if CheckFileExist(FullFileName) then
    begin
      ActualChild := TMDIChild.Create(Self, FullFileName, nil, False);
      AChild := ActualChild;
      if (AChild <> nil) and (AChild.Data <> nil) then
      begin
        ActualChild.Data^.Main := @Self;
        if AnsiCompareText(StrPas(Ext), '.HSP') = 0 then
          ResultHsp := OpenProjectTV(false, StrPas(FullFileName));
           {ResultHSP ist nur dann false wenn HSP wegen Fehler nicht geladen werden konnte}
      end;
    end
    else
      MsgBox(Handle, 1013, mb_IconExclamation or mb_OK, StrPas(DName));
  FileNameOpenNOLOCK := AChild <> nil;
  if not ResultHSP then
    FileNameOpenNOLOCK := false;
end;

{$IFNDEF WMLT}

procedure TWinGISMainForm.FileExitFromDB
  (
  var Msg: TMessage
  );
begin
  SetFocus;
  if (IniFile^.AskQuit) then
  begin
    if MsgBox(Handle, 2041, mb_IconQuestion or mb_YesNo, '') = id_Yes then
    begin
      CloseFromDB := TRUE;
      Close;
    end;
  end
  else
  begin
    CloseFromDB := TRUE;
    Close;
  end;
end;

procedure TWinGISMainForm.AddDBEntry
  (
  ACommunicName: PChar;
  ACon: Boolean;
  AConProj: Boolean;
  ASendStr: Boolean;
  ASendProj: Boolean
  );
var
  Cnt: Integer;
begin
  IniFile^.DatabaseSettings^.Databases^.Insert(New(PDBSetting, Init(ACommunicName, ACon, ASendStr)));
  for Cnt := 0 to MDIChildCount - 1 do
    if (MDIChildren[Cnt].ClassName = MDIChildType) then
      TMDIChild(MDIChildren[Cnt]).Data^.AddDBEntry(ACommunicName, AConProj, ASendProj);
end;
{$ENDIF}
{$ENDIF} // <----------------- AXDLL

{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL

procedure TWinGISMainForm.CheckConnection
  (
  var Msg: TMessage
  );
begin
  DDEHandler.SendString('[CCO][]');
end;

procedure TWinGISMainForm.WMDDEAck
  (
  var Msg: TMessage
  );
begin
  DDEHandler.DDEAcknowledge(Msg);
end;

procedure TWinGISMainForm.WMDDETerminate
  (
  var Msg: TMessage
  );
begin
  DDEHandler.DDETerminate(Msg.wParam, TRUE, FALSE);
  GPS.GPS_Terminate(Msg);
end;

procedure TWinGISMainForm.WMDDEInitiate
  (
  var Msg: TMessage
  );
var
  App: array[0..255] of Char;
  Topic: array[0..255] of Char;
begin
  GlobalGetAtomName(Msg.lParamLo, App, SizeOf(App));
  GlobalGetAtomName(Msg.lParamHi, Topic, SizeOf(Topic));
  if (StrIComp(App, 'WINGIS') = 0) and (StrIComp(Topic, 'GPSDATA') = 0) then
  begin
    GPS.GPS_DDEInitiate(Msg);
    SetDBApplication;
  end;
  if ((StrIComp(App, 'WinGIS') = 0) and (StrIComp(Topic, 'TurboRaster100') = 0)) then
  begin
    TurboRasterDDE^.DDEInitiate(Msg);
  end;
end;

procedure TWinGISMainForm.WMDDEExecute
  (
  var Msg: TMessage
  );
type
  Command = array[0..4] of Char;
var
  GlobalPtr: Pointer;
  MDIChild: TMDIChild;
begin
  MDIChild := WinGISMainForm.GetCurrentChild;
  if MDIChild <> nil then
    MDIChild.WMDDEExecute(Msg)
  else
  begin
    GlobalPtr := GlobalLock(Msg.lParam);
    if GlobalPtr <> nil then
      DoDDECommands(GlobalPtr);
    GlobalUnlock(Msg.lParam);
    DDEHandler.SendAck(Handle, PackDDElParam(wm_DDE_Ack, $8000, Msg.lParam));
  end;
end;

procedure TWinGISMainForm.ExecDDEFile(AFileName: AnsiString);
var
  DDEList: TStringList;
  s: AnsiString;
  i: Integer;
begin
  DDEList := TStringList.Create;
  try
    if FileExists(AFileName) then
    begin
      DDEList.LoadFromFile(AFileName);
      for i := 0 to DDEList.Count - 1 do
      begin
        s := Trim(DDEList[i]);
        if (s <> '') and (Pos('[', s) = 1) then
        begin
          if ActualChild <> nil then
          begin
            ActualChild.DoDDECommands(PChar(s));
          end
          else
          begin
            DoDDECommands(PChar(s));
          end;
        end;
      end;
    end;
  finally
    DDEList.Free;
  end;
end;

procedure TWinGISMainForm.DoDDECommands
  (
  SendedDDEString: PChar
  );
var
  ActMDIChild: TMDIChild;
begin
  if StrLComp(SendedDDEString, '[RUN]', 5) = 0 then
  begin
    ExecDDEFile(GetDDEItem(AnsiString(SendedDDEString), 2));
  end
  else
    if StrLComp(SendedDDEString, '[SHW]', 5) = 0 then
    begin
      ActivateWingis;
    end
    else
      if StrLComp(SendedDDEString, '[SWP]', 5) = 0 then
      begin
        StrCopy(MWDDELine, SendedDDEString);
        SendMessage(Handle, wm_Command, cm_SetMainWinSize, 0);
      end
      else
        if StrLComp(SendedDDEString, '[OPR]', 5) = 0 then
        begin
          StrCopy(MWDDELine1, SendedDDEString);
          SendMessage(Handle, wm_Command, cm_OpenProjects, 0);
        end
        else
          if StrLComp(SendedDDEString, '[IDB]', 5) = 0 then
          begin
            IDBGlobalOnOff := (StrToIntDef(GetDDEItem(AnsiString(SendedDDEString), 2), 1) <> 0)
          end
          else
            if StrLComp(SendedDDEString, '[EMF]', 5) = 0 then
            begin
              ExecuteMenuFunction(GetDDEItem(AnsiString(SendedDDEString), 2));
            end
            else
              if StrLComp(SendedDDEString, '[LOA]', 5) = 0 then
              begin
                SendMessage(Handle, wm_Command, cm_OpActProjs, 0);
              end
              else
                if StrLComp(SendedDDEString, '[CLO]', 5) = 0 then
                begin
                  SendMessage(Handle, wm_Command, cm_ExitFromDB, 0);
                end
                else
                  if StrLComp(SendedDDEString, '[CCO]', 5) = 0 then
                  begin
                    SendMessage(Handle, wm_Command, cm_ChkConnection, 0);
                  end
                  else
                    if StrLComp(SendedDDEString, '[WPR]', 5) = 0 then
                    begin
                      StrCopy(MWDDELine1, SendedDDEString);
                      SendMessage(Handle, wm_Command, cm_ChangeProject, 0);
                    end
                    else
                      if StrLComp(SendedDDEString, '[CPR]', 5) = 0 then
                      begin
                        StrCopy(MWDDELine1, SendedDDEString);
                        SendMessage(Handle, wm_Command, cm_CloseProject, 0);
                      end
                      else
                        if StrLComp(SendedDDEString, '[MCB]', 5) = 0 then
                        begin
                          DDEHandler.MacroMode := TRUE;
                          UserInterface.Update([uiMenus]);
                        end
                        else
                          if StrLComp(SendedDDEString, '[MCE]', 5) = 0 then
                          begin
                            SendMessage(Handle, wm_Command, cm_EndDDEMLSeq, 0);
                          end
                          else
                          begin
                            ActMDIChild := GetCurrentChild;
                            if ActMDIChild <> nil then
                              ActMDIChild.DoDDECommands(SendedDDEString);
                          end;
end;

procedure TWinGISMainForm.EndDDEMLSeq
  (
  var Msg: TMessage
  );
begin
  if ActualChild <> nil then
    ActualChild.WMCommand(Msg)
  else
  begin
    if DDEHandler.MacroMode then
    begin
      DDEHandler.SendString('[MAB][]');
      DDEHandler.SendString('[MAE][]');
      DDEHandler.MacroMode := FALSE;
      UserInterface.Update([uiMenus]);
    end;
  end;
end;

procedure TWinGISMainForm.SendRDRCommAfterDDE;
var
  Cnt: Integer;
begin
  for Cnt := 0 to MDIChildCount - 1 do
    if MDIChildren[Cnt].ClassName = MDIChildType then
      SendMessage(MDIChildren[Cnt].Handle, wm_Command, cm_RedrawADDE, 0);
end;

procedure TWinGISMainForm.PostRDRCommAfterDDE;
var
  Cnt: Integer;
begin
  for Cnt := 0 to MDIChildCount - 1 do
    if MDIChildren[Cnt].ClassName = MDIChildType then
      SendMessage(MDIChildren[Cnt].Handle, wm_Command, cm_RedrawADDE, 0);
end;

procedure TWinGISMainForm.ListOpenAndActiveProjects(var Msg: TMessage);
var
  AcTMDIChild: TMDIChild;
  Cnt: Integer;
  ChildCount: Integer;

  procedure SendNameInfo(AChild: TMDIChild);
  begin
    DDEHandler.SendString('[PRL][' + StrPas(AChild.FName) + ']');
    Inc(ChildCount);
  end;
begin
  ChildCount := 0;
  DDEHandler.SetAnswerMode(TRUE);
  AcTMDIChild := WinGISMainForm.GetCurrentChild;
  if AcTMDIChild <> nil then
    SendNameInfo(AcTMDIChild);
  for Cnt := 0 to MDIChildCount - 1 do
    if (MDIChildren[Cnt] <> AcTMDIChild) and
      (MDIChildren[Cnt].ClassName = MDIChildType) then
      SendNameInfo(TMDIChild(MDIChildren[Cnt]));
  if ChildCount = 0 then
    DDEHandler.SendString('[PRL][]');
  DDEHandler.SendString('[END][]');
  DDEHandler.SetAnswerMode(FALSE);
end;

procedure TWinGISMainForm.WMDDERequest(var Msg: TMessage);
begin
  if (Msg.wParam = TurboRasterDDE^.hClient) then
    TurboRasterDDE^.DDERequest(Msg);
end;
{$ENDIF} // <----------------- AXDLL
{$ENDIF}

function TWinGISMainForm.CountChildren: Integer;
var
  Cnt: Integer;
begin
  Result := 0;
  for Cnt := 0 to MDIChildCount - 1 do
    if MDIChildren[Cnt].ClassName = MDIChildType then
      Inc(Result);
end;

{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}

procedure TWinGISMainForm.HelpAbout;
begin
  ShowInfoDialog(GetWingisVersion, Application.Title);
end;
{$ENDIF}

{$IFNDEF WMLT}

procedure TWingisMainForm.ExtrasIUpdate;
var
  s, Build: AnsiString;
  hSession, hURL: HInternet;
  ProgName: array[0..1024] of Char;
  VersionInfo: PChar;
  Buffer: array[0..1024 * 256] of Char;
  FileOffset: LongWord;
  StructSize: LongInt;
  Info: Pointer;
  InfoSize: LongWord;
  BufferSize: DWord;
  AList: TStringList;
  ALink: AnsiString;
  AMsg: AnsiString;
  AConnected: Boolean;
begin
  RunUpdateOnClose := False;
  s := '';
  Build := '';
  ALink := '';
  AConnected := False;
  GetModuleFileName(HInstance, ProgName, SizeOf(ProgName));
  StructSize := GetFileVersionInfoSize(ProgName, FileOffset);
  if StructSize > 0 then
  begin
    GetMem(VersionInfo, StructSize);
    if VersionInfo <> nil then
    begin
      if GetFileVersionInfo(ProgName, FileOffset, StructSize, VersionInfo) then
      begin
        VerQueryValue(VersionInfo, '\StringFileInfo\080904E4\FileVersion', Info, InfoSize);
        Build := StrPas(PChar(Info));
      end;
      FreeMem(VersionInfo, StructSize);
    end;
  end;
  hSession := InternetOpen(nil, INTERNET_OPEN_TYPE_PRECONFIG, nil, nil, 0);
  if hSession <> nil then
  begin
    hURL := 0;
    if IsOnline then
    begin
      hURL := InternetOpenURL(hSession, 'http://www.progis.com/wingis_update.txt', nil, 0, INTERNET_FLAG_RELOAD, 0);
    end;
    if hURL <> nil then
    begin
      AConnected := True;
      try
        InternetReadFile(hURL, @Buffer, Sizeof(Buffer), BufferSize);
        if BufferSize > 0 then
        begin
          s := Copy(Buffer, 0, BufferSize);
        end;
      finally
        InternetCloseHandle(hURL);
      end;
    end;
    InternetCloseHandle(hSession);
  end;
  if AConnected then
  begin
    SetUpdateStatus;
    AList := TStringList.Create;
    try
      AList.Text := s;
      ALink := AList.Values[Build];
    finally
      AList.Free;
    end;
    if ALink <> '' then
    begin
      AMsg := Format(StringReplace(MlgSection[12], '\n', #13#10, [rfReplaceAll]), [ALink]);
      if MessageBox(Handle, PChar(AMsg), PChar(Application.Title), MB_ICONQUESTION or MB_YESNO or MB_APPLMODAL) = IDYES then
      begin
        UpdateLink := ALink;
        Close;
      end;
    end
    else
      if not RunUpdateSilent then
      begin
        AMsg := StringReplace(MlgSection[13], '\n', #13#10, [rfReplaceAll]);
        MessageBox(Handle, PChar(AMsg), PChar(Application.Title), MB_ICONINFORMATION or MB_OK or MB_APPLMODAL);
      end;
  end
  else
    if not RunUpdateSilent then
    begin
      AMsg := StringReplace(MlgSection[14], '\n', #13#10, [rfReplaceAll]);
      MessageBox(Handle, PChar(AMsg), PChar(Application.Title), MB_ICONERROR or MB_OK or MB_APPLMODAL);
    end;
end;

{$ENDIF}
{$ENDIF} // <----------------- AXDLL

{$IFNDEF AXDLL} // <----------------- AXDLL

procedure TWinGISMainForm.ExtrasGetAddons;
begin
  RunAddOnsDlg;
end;

procedure TWinGISMainForm.CheckLibStatus(Handle: Integer; Filename, Info: AnsiString);
begin
  if Handle = 0 then
  begin
  end;
end;

procedure TWinGISMainForm.OptionsPassword;
begin
  ChangePassword(Self);
end;

{$IFNDEF WMLT}

procedure TWinGISMainForm.ExtrasRegistration;
begin
end;
{$ENDIF}
{$ENDIF} // <----------------- AXDLL

function TWinGISMainForm.GetCurrentChild: TMDIChild;
begin
  if (ActiveMDIChild <> nil) and (ActiveMDIChild is TMDIChild) then
    Result := TMDIChild(ActiveMDIChild)
  else
    Result := nil;
end;

procedure TWinGISMainForm.HelpContents;
begin
  if (ActualChild <> nil) and (ActualChild.Data <> nil) then
  begin
    if not Application.HelpCommand(HELP_CONTEXT, ActualChild.Data^.ActualMen) then
    begin
      Application.HelpCommand(HELP_INDEX, 0);
    end;
  end
  else
  begin
    Application.HelpCommand(HELP_INDEX, 0);
  end;
end;

{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}

procedure TWinGISMainForm.HelpUseHelp;
begin
  Application.HelpCommand(HELP_HELPONHELP, 0);
end;

{******************************************************************************+
  Procedure TWinGISMainForm.WindowTile
--------------------------------------------------------------------------------
  Ordnet die MDI-Childfenster nicht �berlappend im MDI-Frame an.
+******************************************************************************}

procedure TWinGISMainForm.WindowTile;
begin
  Tile;
end;

{******************************************************************************+
  Procedure TWinGISMainForm.WindowArrangeIcons
--------------------------------------------------------------------------------
  Ordnet alle minimierten MDI-Childfenster im MDI-Frame an.
*******************************************************************************}

procedure TWinGISMainForm.WindowArrangeIcons;
begin
  ArrangeIcons;
end;

{******************************************************************************+
  Procedure TWinGISMainForm.WindowCascade
--------------------------------------------------------------------------------
  Ordnet die MDI-Childfenster �berlappend im MDI-Frame an.
+******************************************************************************}

procedure TWinGISMainForm.WindowCascade;
begin
  Cascade;
end;

{******************************************************************************+
  Procedure TWinGISMainForm.WindowCloseAll
--------------------------------------------------------------------------------
  Schlie�t alle MDI-Childfenster. Vor dem Schlie�en wird ggf. zum Speichern
  aufgefordert.
+******************************************************************************}

procedure TWinGISMainForm.WindowCloseAll;
begin
end;
{$ENDIF}
{$ENDIF} // <----------------- AXDLL

{******************************************************************************+
  Procedure TWinGISMainForm.FileQuit
--------------------------------------------------------------------------------
  Beendet WinGIS, sofern alle Projekte gespeichert sind oder der Anwender
  alle Projekte sichert bzw. die �nderungen verwirft.
+******************************************************************************}

procedure TWinGISMainForm.FileQuit;
begin
  Close;
end;

{$IFNDEF AXDLL} // <----------------- AXDLL
{******************************************************************************+
  Procedure TWinGISMainForm.UIInitializeActivate
--------------------------------------------------------------------------------
  Wird von TScreen aufgerufen, wenn ein Formular aktiviert wird.
+******************************************************************************}

procedure TWinGISMainForm.UIInitializeActivate;
begin
  if not (csDestroying in ComponentState) then
  begin
    if MDIChildCount <> 0 then
      UpdateActiveMDIChild;
    UserInterface.Update([uiMenus, uiStatusBar]);
  end;
end;

{******************************************************************************+
  Procedure TWinGISMainForm.FileHistoryClick
--------------------------------------------------------------------------------
  Wird aufgerufen, wenn ein Men�punkt der FileHistory-Liste im Men� angeclickt
  wird. Das angegebene Projekt wird ge�ffnet bzw. in den Vordergrund gebracht.
+*****************************************************************************}

procedure TWinGISMainForm.FileHistoryClick(Item: string);
var
  AName: array[0..255] of Char;
begin
  if FileNameOpen(StrPCopy(AName, Item), 0, False) then
  begin
{$IFNDEF WMLT}
    NewTDocument(Item);
{$ENDIF}
  end;
end;
{$ENDIF} // <----- AXDLL

{$IFNDEF AXDLL} // <----- AXDLL
{******************************************************************************+
  Procedure TWinGISMainForm.FormShown
--------------------------------------------------------------------------------
+******************************************************************************}

procedure TWinGISMainForm.FormShown(Sender: TObject);
begin
  OpenProjects;
{$IFNDEF WMLT}
  SetDBApplication;
  DDEHandler.EnableDDEReception(FALSE);
{$ENDIF}
  UIInitializeActivate(Self);
  UserInterface.Update([uiMenus]);
  MenuFunctions['DatabaseAutoEditInsert'].Checked := TRUE;
  MenuFunctions['FileNew'].Enabled := TRUE;
{$IFNDEF WMLT}
  DragAcceptFiles(Handle, True);
  if (not DDEHandler.FSendMWR) { and (not IntroPlaying)} then
  begin //old intro
    DDEHandler.EnableDDEReception(TRUE);
    FirstLoad := FALSE;
  end;
  if (DDEHandler.FSendMWR) { and (not IntroPlaying) } then
  begin //old intro
    DDEHandler.EnableDDEReception(TRUE);
    DDEHandler.SendString('[MWR][]');
    FirstLoad := TRUE;
  end;
{$ENDIF}
  if RunUpdateSilent then
  begin
    ExtrasIUpdate;
    RunUpdateSilent := False;
  end
  else
  begin
    GetUpdateStatus;
  end;
end;

procedure TWinGISMainForm.GetUpdateStatus;
var
  Reg: TRegistry;
  LastUpdate: Integer;
begin
  LastUpdate := 0;
  Reg := TRegistry.Create;
  try
    Reg.RootKey := HKEY_CURRENT_USER;
    if Reg.OpenKeyReadOnly('Software\PROGIS\Update') then
    begin
      if Reg.ValueExists('WinGIS') then
      begin
        LastUpdate := Reg.ReadInteger('WinGIS');
      end;
      Reg.CloseKey;
    end;
    if ((Now - LastUpdate) > UPDATE_INTERVAL) then
    begin
      RunUpdateOnClose := True;
    end;
  finally
    Reg.Free;
  end;
end;

procedure TWinGISMainForm.SetUpdateStatus;
var
  Reg: TRegistry;
begin
  Reg := TRegistry.Create;
  try
    Reg.RootKey := HKEY_CURRENT_USER;
    if Reg.OpenKey('Software\PROGIS\Update', True) then
    begin
      Reg.WriteInteger('WinGIS', Trunc(Now));
      Reg.CloseKey;
    end;
  finally
    Reg.Free;
  end;
end;

{$IFNDEF WMLT}

procedure TWinGISMainForm.LoadNextProj
  (
  var Msg: TMessage
  );
var
  OldChild: TMDIChild;
begin
  OldChild := ActualChild;
  if CloseFileOnReload then
  begin
    SendMessage(OldChild.Handle, WM_CLOSE, 0, 0);
  end;
  if FileNameOpen(@NextProj, 0, False) then
  begin
    NewTDocument(strpas(@NextProj));
  end;
end;
{$ENDIF}

procedure TWinGISMainForm.ViewToolbars;
begin
  UserInterface.ToolbarDialog(TRUE, FALSE);
end;
{++brovak}

{ Reset User's Interface for Default settings }
{ ------------------------------------------- }

{$IFNDEF WMLT}

procedure TWinGISMainForm.SetDefUserInterfaceMain;
begin
  AllowDraw := False;
  try
    UserInterface.SetDefUserInterface(nil);
  finally
    AllowDraw := True;
  end;
  if ActualChild <> nil then
  begin
    ActualChild.Data^.PInfo^.RedrawScreen(False);
  end;
end;
 {--brovak}

procedure TWinGISMainForm.FileSaveAll;
var
  Cnt: Integer;
begin
  for Cnt := 0 to MDIChildCount - 1 do
    CallMethodByName(MDIChildren[Cnt], 'FileSave');
end;
{$ENDIF}

{$ENDIF} // <------ AXDLL

procedure TWinGISMainForm.FormActivate(Sender: TObject);
var
  Child: TComponent;
  Cnt: Integer;
begin
  if HideWindow then
  begin
    ShowWindow(Handle, SW_HIDE);
  end;
  for Cnt := 0 to MDIChildCount - 1 do
  begin
    Child := MDIChildren[Cnt];
    if (Child is TMDIChild) and not (csDestroying in Child.ComponentState) then
    begin
      if TMDIChild(Child).Colors256 then
        SetPaletteForWindow(TMDIChild(Child).Data^.PInfo^.ExtCanvas.Handle);
      if SetRedraw then
        TMDIChild(Child).ChangeRedraw(TRUE);
    end;
  end;
  SetRedraw := FALSE;
end;

{$IFNDEF AXDLL} // <------ AXDLL
{$IFNDEF WMLT}

procedure TWinGISMainForm.SetWinGISMainFormSize
  (
  var Msg: TMessage
  );
var
  WorkRect: TRect;
  DeskWidth: LongInt;
  DeskHeight: LongInt;
  DBWidth: LongInt;
  DBHeight: LongInt;
  DBPos: LongInt;
  NewWinRect: TRect;
  SFound: Boolean;
  MStrs: Boolean;
  AllParams: Boolean;
  ValOK: Boolean;
begin
  AllParams := TRUE;
  ValOK := ReadDDECommandLongInt(MWDDELine, 1, 0, WgBlockStart, DDEHandler.FDDESepSign,
    WgBlockEnd, DeskWidth, SFound, MStrs);
  AllParams := AllParams and ValOK;
  ValOK := ReadDDECommandLongInt(MWDDELine, 1, 1, WgBlockStart, DDEHandler.FDDESepSign,
    WgBlockEnd, DeskHeight, SFound, MStrs);
  AllParams := AllParams and ValOK;
  ValOK := ReadDDECommandLongInt(MWDDELine, 1, 2, WgBlockStart, DDEHandler.FDDESepSign,
    WgBlockEnd, DBWidth, SFound, MStrs);
  AllParams := AllParams and ValOK;
  ValOK := ReadDDECommandLongInt(MWDDELine, 1, 3, WgBlockStart, DDEHandler.FDDESepSign,
    WgBlockEnd, DBHeight, SFound, MStrs);
  AllParams := AllParams and ValOK;
  ValOK := ReadDDECommandLongInt(MWDDELine, 1, 4, WgBlockStart, DDEHandler.FDDESepSign,
    WgBlockEnd, DBPos, SFound, MStrs);
  AllParams := AllParams and ValOK;
  if AllParams then
  begin
    SystemParametersInfo(SPI_GETWORKAREA, 0, @WorkRect, 0);
    if (DeskWidth > 0) and (DeskWidth <= WorkRect.Right) and
      (DeskHeight > 0) and (DeskHeight <= WorkRect.Bottom) then
    begin
      NewWinRect.Left := WorkRect.Left;
      NewWinRect.Top := WorkRect.Top;
      NewWinRect.Right := DeskWidth;
      NewWinRect.Bottom := DeskHeight;
    end
    else
      NewWinRect := WorkRect;
    case DBPos of
      0: ;
      1: NewWinRect.Left := DBWidth;
      2: NewWinRect.Right := NewWinRect.Right - DBWidth + 2;
      3: NewWinRect.Top := DBHeight;
      4: NewWinRect.Bottom := NewWinRect.Bottom - DBHeight + 2;
    end;
    Show;
    with NewWinRect do
      SetWindowPos(Handle, HWND_TOP, Left, Top, Right - Left, Bottom - Top, SWP_SHOWWINDOW);
  end;
end;
{$ENDIF}
{$ENDIF} // <----------------- AXDLL

procedure TWinGISMainForm.FileNameClose
  (
  AFileName: PChar
  );
var
  Dir: array[0..fsPathName] of Char;
  DName: array[0..fsFileName + fsExtension] of Char;
  Ext: array[0..fsExtension] of Char;
  AChild: TMDIChild;
  FullFileName: array[0..255] of Char;
begin
  FileExpand(FullFileName, AFileName);
  AnsiLower(FullFileName);
  FileSplit(FullFileName, Dir, DName, Ext);
  StrCopy(FullFileName, Dir);
  StrCat(FullFileName, DName);
  if StrComp(Ext, '') = 0 then
    Ext := '.amp';
  StrCat(FullFileName, Ext {'.amp'});
  AChild := IsFileOpened(FullFileName);

  if AChild <> nil then
  begin
{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}
    if AChild.Data^.IsCanvas then
    begin
      AChild.ResetGraphicParent;
    end;
{$ENDIF}
{$ENDIF} // <----------------- AXDLL
    if not AChild.IsClosing then
    begin
      SendMessage(AChild.Handle, wm_Close, 0, 0);
    end;
  end;
end;

{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}

procedure TWinGISMainForm.FinishImpExpRoutine(ExitMode: boolean; ModuleType: string);
begin
   // the import/export routine has been finished
   // now do a redraw in dependence of the ExitMode of the library
  if (ExitMode = TRUE) then
  begin
    if ModuleType = 'IMPORTMODULE' then
    begin
      UpdateSymbolRollup;
      UserInterface.Update([uiViews]);
      TMDIChild(ACtualchild).Data^.CalculateSize;
      TMDIChild(ACtualchild).Data^.SetOverView;
      TMDIChild(ACtualchild).Data^.Modified := true;
    end;
  end;
   // trigger the timer to release the impexp-dll
  TimerFreeImpExpDll.Interval := 100; // Import/Export DLL has 100ms time to release all data
  TimerFreeImpExpDll.Enabled := TRUE;
end;

procedure TWinGISMainForm.FinishModule(DllIdx: integer);
begin
  FinishModuleTimer.Interval := 100; // The Module has 100ms time to release all data
  FinishModuleTimer.Tag := DllIdx;
  FinishModuleTimer.Enabled := TRUE;
end;

function TWinGISMainForm.GetIdbProcessMask: integer;
var
  retVal: integer;
begin
   // 0 is Use nothing
   // 1 is Use the Idb
   // 2 is Use IdbUser interface
   // 4 is Use Undo
  retVal := 0;
  if WeAreUsingTheIDB[PProj(TMDIChild(ACtualchild).Data)] then
    retVal := retVal + 1;
  if WeAreUsingTheIDBUserInterface[PProj(TMDIChild(ACtualchild).Data)] then
    retVal := retVal + 2;
  if WeAreUsingUndoFunction[PProj(TMDIChild(ACtualchild).Data)] then
    retVal := retVal + 4;
  result := retVal;
end;

procedure TWinGISMainForm.SetIdbProcessMask(value: integer);
begin
  if ((value and 1) = 1) then
    WeAreUsingTheIDB[PProj(TMDIChild(ACtualchild).Data)] := TRUE
  else
    WeAreUsingTheIDB[PProj(TMDIChild(ACtualchild).Data)] := FALSE;
  if ((value and 2) = 2) then
    WeAreUsingTheIDBUserInterface[PProj(TMDIChild(ACtualchild).Data)] := TRUE
  else
    WeAreUsingTheIDBUserInterface[PProj(TMDIChild(ACtualchild).Data)] := FALSE;
  if ((value and 4) = 4) then
    WeAreUsingUndoFunction[PProj(TMDIChild(ACtualchild).Data)] := TRUE
  else
    WeAreUsingUndoFunction[PProj(TMDIChild(ACtualchild).Data)] := FALSE;
  SetDBApplication;
end;

procedure TWinGISMainForm.DoAmpDkmImport(StartDir: string; AmpFileName: string; DkmDBDir: string; DestDBMode: Integer; DestDatabase: string; DestTable: string; LngCode: string);
begin
   // create ImpExpDllData
  ImpExpDllData := new(pImpExpDataRec);
  ImpExpDllData^.aImpExpMode := 'AMPDKMIMPORT';
  ImpExpDllData^.aDir := StartDir;
  ImpExpDllData^.aProjName := AmpFilename;
  ImpExpDllData^.aSourceDb := DkmDBDir;
  ImpExpDllData^.aDestDbMode := DestDbMode;
  ImpExpDllData^.aDestDb := DestDatabase;
  ImpExpDllData^.aDestTable := DestTable;
  ImpExpDllData^.aLngCode := LngCode;
   // first the old dll has to be released before the amp-import can be executed
  FinishImpExpRoutine(TRUE, ''); // to avoid redraw
end;

// settings are stored in ImpExpDllData

procedure TWinGISMainForm.ExecAmpDkmImport;
type
  TAmpDkmImport = procedure(DIR: PCHAR; var AXHANDLE: VARIANT; DESTAMPFILE: Variant; SOURCEAMPFILENAME: PCHAR; DKMDBFFILENAME: PCHAR; DBDESTMODE: integer; DBDESTNAME: PCHAR; DBDESTTABLE: PCHAR; LNGCODE: PCHAR; WINHANDLE: integer; WTOP: integer; WLEFT: integer; WWIDTH: integer; WHEIGHT: integer); stdcall;
var
  AMPDKMIMPORT: TAmpDkmImport;
  AmpDkmImportPtr: TFarProc;
  pStartDir: PCHAR;
  pAmpFilename: PCHAR;
  pDKMDBDir: PCHAR;
  pDestDatabase: PCHAR;
  pDestTable: PCHAR;
  pLngCode: PCHAR;
  curdoc: Variant;
  aDestDbMode: integer;
begin
   // now the AMPDKMIMPORT routine of the Ampimp.dll has to be loaded
   // first the Ampimp.dll library has to be loaded
  ImpExpDll := LoadLibrary('AMPIMP.DLL');
  CheckLibStatus(ImpExpDll, 'AMPIMP.DLL', 'Import/Export');
  AmpDkmImportPtr := GetProcAddress(ImpExpDll, 'AMPDKMIMPORT');
  if AmpDkmImportPtr <> nil then
  begin
    pStartDir := stralloc(length(ImpExpDllData^.aDir) + 1);
    strpcopy(pStartdir, ImpExpDllData^.aDir);
    pAmpFilename := stralloc(length(ImpExpDllData^.aProjName) + 1);
    strpcopy(pAmpFilename, ImpExpDllData^.aProjName);
    pDKMDBDir := stralloc(length(ImpExpDllData^.aSourceDb) + 1);
    strpcopy(pDKMDBDir, ImpExpDllData^.aSourceDb);
    pDestDatabase := stralloc(length(ImpExpDllData^.aDestDb) + 1);
    strpcopy(pDestDatabase, ImpExpDllData^.aDestDb);
    pDestTable := stralloc(length(ImpExpDllData^.aDestTable) + 1);
    strpcopy(pDestTable, ImpExpDllData^.aDestTable);
    pLngCode := stralloc(length(ImpExpDllData^.aLngCode) + 1);
    strpcopy(pLngCode, ImpExpDllData^.aLngCode);
    aDestDbMode := ImpExpDllData^.aDestDbMode;

      // now the ImpExpDllData can be deleted
    dispose(ImpExpDllData);
    ImpExpDllData := nil;

    CheckTDocumentIsCorrect;
    curdoc := GetCurrentTDocument;
    @AMPDKMIMPORT := AmpDkmImportPtr;
    AMPDKMIMPORT(pStartDir, DXApp, curdoc, pAmpFilename, pDKMDBDir, aDestDbMode, pDestDatabase, pDestTable, pLngCode,
      WinGISMainForm.Handle,
      WinGISMainForm.Top,
      WinGISMainForm.Left,
      WinGISMainForm.Width,
      WinGISMainForm.Height);
    WinGISMainForm.LastImpExpOperation := 'AMPIMP';

    strdispose(pStartDir);
    strdispose(pAmpFilename);
    strdispose(pDKMDBDir);
    strdispose(pDestDatabase);
    strdispose(pDestTable);
    strdispose(pLngCode);
  end
  else
    FreeLibrary(ImpExpDll);
end;

procedure TWinGISMainForm.NewTDocument(DocumentName: string);
var
  newentry: TDocumentEntry;
  iterate: TDocumentEntry;
begin
  DocumentName := AnsiLowerCase(DocumentName);

   //procedure is used to add a new opened document to the TDocumentlist
  CurrentTDocumentFileName := DocumentName;
  if CheckTDocumentExists(DocumentName) then
    exit;
  new(newentry);
  newentry^.aDocument := DXAppTCore.AddDocument(DocumentName);
  newentry^.aDocumentName := DocumentName;

  if TDocumentList = nil then
  begin
    TDocumentList := newentry;
    TDocumentList^.next := nil;
  end
  else
  begin
    iterate := TDocumentList;
    while (iterate^.next <> nil) do
      iterate := iterate^.next;
    iterate^.next := newentry;
    newentry^.next := nil;
  end;
  CurrentTDocument := newentry;
end;

procedure TWinGISMainForm.SetCurrentTDocument(DocumentName: string);
var
  myTDocument: TDocumentEntry;
begin
  CurrentTDocumentFileName := DocumentName;
  if not CheckTDocumentExists(DocumentName) then
    exit;
  myTDocument := GetTDocumentByName(DocumentName);
  if myTDocument <> nil then
    CurrentTDocument := myTDocument;
end;

function TWinGISMainForm.GetTDocumentByIndex(index: integer): TDocumentEntry;
var
  iterate: TDocumentEntry;
  curindex: integer;
begin
  iterate := TDocumentList;
  curindex := 0;
  while curindex < index do
  begin
    iterate := iterate^.next;
    curindex := curindex + 1;
  end;
  result := iterate;
end;

function TWinGISMainForm.GetTDocumentByName(DocumentName: string): TDocumentEntry;
var
  iterate: TDocumentEntry;
  found: boolean;
begin
  iterate := TDocumentList;
  found := false;
  if TDocumentList <> nil then
  begin
    while (iterate <> nil) and (not found) do
    begin
      if AnsiUpperCase(iterate^.aDocumentName) = AnsiUpperCase(DocumentName) then
        found := true
      else
        iterate := iterate^.next;
    end;
  end;
  result := iterate;
end;

procedure TWinGISMainForm.CheckTDocumentIsCorrect;
begin
  if not CheckTDocumentExists(CurrentTDocumentFileName) then
    NewTDocument(CurrentTDocumentFileName);
end;

procedure TWinGISMainForm.DeleteTDocumentByName(DocumentName: string);
var
  iterate1: TDocumentEntry;
  iterate2: TDocumentEntry;
  curindex: integer;
  index: integer;
  found: boolean;
begin
   // procedure is used to delete an entry out of the TDocumentlist by Index
  iterate1 := TDocumentList;
  index := 0;
  found := false;
  while (iterate1 <> nil) and (not found) do
  begin
    if (AnsiUpperCase(iterate1^.aDocumentName) = AnsiUpperCase(DocumentName)) then
      found := true
    else
    begin
      index := index + 1;
      iterate1 := iterate1^.next;
    end;
  end;
  if found then
  begin
    iterate1 := TDocumentList;
    iterate2 := TDocumentList;
    if Index = 0 then //delete first entry
    begin
      TDocumentList := iterate1^.next;
      iterate1^.aDocument := 0;
      dispose(iterate1);
    end
    else
    begin
      for curindex := 1 to Index do
      begin
        iterate2 := iterate1;
        iterate1 := iterate1^.next;
      end;
      iterate2^.next := iterate1^.next;
      iterate1^.aDocument := 0;
      dispose(iterate1);
    end;
  end;
end;

procedure TWinGISMainForm.SetCurrentTDocumentFilename(OldDocumentName: string; Documentname: string);
begin
   // first it will be checked if the old document exists
  if (not CheckTDocumentExists(OldDocumentname)) then
    exit;
  CurrentTDocument^.aDocumentName := Documentname;
  CurrentTDocument^.aDocument.Name := Documentname;
  CurrentTDocumentFilename := DocumentName;
end;

function TWinGISMainForm.GetCurrentTDocument: Variant;
begin
  result := CurrentTDocument^.aDocument;
end;

function TWinGISMainForm.GetCurrentTDocumentName: string;
begin
  result := CurrentTDocument^.aDocumentname;
end;

function TWinGISMainForm.CheckTDocumentExists(Documentname: string): boolean;
var
  retVal: boolean;
  iterate: TDocumentEntry;
begin
  retVal := false;
  iterate := TDocumentList;
  while (iterate <> nil) do
  begin
    if AnsiUpperCase(iterate^.aDocumentname) = AnsiUpperCase(Documentname) then
      retVal := true;
    iterate := iterate^.next;
  end;
  result := retVal;
end;

procedure TWinGISMainForm.RedrawCurrentTDocument;
begin
  CurrentTDocument^.aDocument.Redraw;
end;

function TWinGISMainForm.AcceptExecute
  (
  HSZDGTop: HSZ;
  HSZTopic: HSZ;
  DDEStr: PChar;
  ConvName: PChar
  )
  : Boolean;
begin
  Result := FALSE;
  if (HSZTopic = HSZDGTop) then
  begin
    if DDEStr <> nil then
      DoDDECommands(DDEStr);
    Result := TRUE;
  end;
end;
{$ENDIF}
{$ENDIF} // <----------------- AXDLL

procedure TWinGISMainForm.DoActivateApplication(Sender: TObject);
begin
  UpdateActiveMDIChild;
end;

procedure TWinGISMainForm.UpdateActiveMDIChild;
begin
  if (ActiveMDIChild <> nil)
    and (Screen.ActiveForm = ActiveMDIChild)
    and Application.Active
    and (ActiveMDIChild is TMDIChild) then
  begin
    TMDIChild(ActiveMDIChild).OnBringToFront;
  end;
end;

procedure TWinGISMainForm.WndProc(var Message: TMessage);
var
  HDROP: THandle;
  FileName: array[0..255] of Char;
  Pt: TPoint;
begin
{$IFNDEF WMLT}
  case Message.Msg of
    wm_Command:
      begin
        with Message do
        begin
          Result := Perform(LoWord(wParam) + id_First, wParam, lParam);
          Result := PerForm(cm_Base + LoWord(wParam), wParam, lParam);
        end;
      end;
{$IFNDEF AXDLL} // <----------------- AXDLL
{++ IDB_Interaction}
    WM_IDB_Message:
      if Message.WParam = IDB_MustBeDisconnected then
      begin
        SELF.bWeAreUsingTheIDB := FALSE;
        SELF.bWeAreUsingTheIDBUserInterface := FALSE;
//          SELF.bWeAreUsingUndoFunction := FALSE;

        SELF.IDB_Man.Free;
        SELF.IDB_Man := nil;
        if ActualChild.Data <> nil then
        begin
          WeAreUsingTheIDB[ActualChild.Data] := False;
          WeAreUsingTheIDBUserInterface[ActualChild.Data] := False;
          ActualChild.Data^.SetModified;
        end;
      end;
{-- IDB_Interaction}
    WM_DROPFILES:
      begin
        HDROP := Message.WPARAM;
        DragQueryFile(HDROP, 0, FileName, Length(FileName));
        DragQueryPoint(HDROP, Pt);
        DragFinish(HDROP);
        if FileNameOpen(FileName, 0, False) then
        begin
          NewTDocument(strpas(FileName));
        end;
      end;
{$ENDIF} // <----------------- AXDLL
  end;
{$ENDIF}
  inherited WndProc(Message);
end;

procedure TWinGISMainForm.OnGlobalOptionsChanged;
var
  Cnt: Integer;
begin
  for Cnt := 0 to MDIChildCount - 1 do
    if MDIChildren[Cnt] is TMDIChild then
      TMDIChild(MDIChildren[Cnt]).OnGlobalOptionsChanged;
end;

{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}

function TWinGISMainForm.CheckCDInDrive: Boolean;
var
  CFileName: string;
  CheckDone: Boolean;
begin
  CFileName := IniFile^.ReadConnection;
  Result := False;
  if CFileName <> '' then
  begin
    repeat
      CheckDone := True;
      if fileexists(CFileName) then
        Result := True
      else
        if MsgBox(Handle, 2020, mb_OkCancel or mb_IconExclamation, '') = id_OK then
          CheckDone := FALSE
    until CheckDone;
  end
  else
    Result := TRUE;
end;
{$ENDIF}
{$ENDIF} // <----------------- AXDLL

{$IFNDEF AXDLL} // <----------------- AXDLL
{******************************************************************************+
  Procedure TWinGISMainForm.UIInitializeMessages
--------------------------------------------------------------------------------
  Hier landen alle Windows-Meldungen, bevor sie an eines der Formulare weiter-
  geleitet werden. Die Routine �bernimmt die Abarbeitung von Messages f�r
  "alte" Dialoge.
+******************************************************************************}

procedure TWinGISMainForm.UIInitializeMessages(var Msg: tagMSG;
  var Handled: Boolean);
begin
  if (Screen.ActiveForm <> nil) and (Screen.ActiveForm is TRDialog) then
    { Wenn das aktuelle Formular ein "alter" Dialog ist, dann wird die
    | Message von Windows als Dialog-Message bearbeitet. Eine weitere
    + Bearbeitung darf dann nicht mehr stattfinden (Handled setzen). }
  begin
    Handled := IsDialogMessage(Screen.ActiveForm.Handle, Msg);
  end;
end;
{$ENDIF} // <----------------- AXDLL

procedure TWinGISMainForm.DefaultMenuHandler(Sender: TObject; MenuFunction: TMenuFunction);
var
  AHandlerClass: TMenuHandlerClass;
  MenuHandler: TMenuHandler;
begin
  // search for a registered menu-handler
  AHandlerClass := MenuHandlers[MenuFunction.Name];
  if Assigned(AHandlerClass) then
  begin
    MenuHandler := AHandlerClass.Create(Self);
    try
      MenuHandler.OnActivate;
      MenuHandler.OnStart;
      MenuHandler.OnDeactivate;
      MenuHandler.OnEnd;
    finally
      MenuHandler.Free;
    end;
  end;
end;

{$IFNDEF AXDLL} // <----------------- AXDLL
{******************************************************************************+
  Procedure TWinGISMainForm.UpdateMenus
--------------------------------------------------------------------------------
  Enabled/Disabled die Men�punkte und die Toolbar-Buttons. Wenn ein
  Child-Fenster existiert, so wird auch EnableMenus des aktiven Childs
  aufgerufen.
+******************************************************************************}

procedure TWinGISMainForm.UIExtensionUpdateMenus(Sender: TObject);
var
  Cnt: Integer;
begin
  MenuFunctions.GroupVisible[9501] := True;
  if CountChildren = 0 then
  begin
    ActualChild := nil;
  end;
    //MenuFunctions['AddOnMenu'].Enabled:=True;
  MenuFunctions['WindowTile'].Enabled := True;
    // menus depending on an opened MDIChild
  if ActualChild = nil then
  begin
//       MenuFunctions.GroupEnabled[9998]:=False;
    MenuFunctions['IDB_UndoFunction'].Enabled := False;
    MenuFunctions['IDB_RedoFunction'].Enabled := False;
  end;

  MenuFunctions['HelpContents'].Enabled := (Application.HelpFile <> '');
  MenuFunctions['MUFileOpen'].Enabled := (WingisLicense >= WG_PROF);

  MenuFunctions.GroupEnabled[2] := MDIChildCount > 0;
  MenuFunctions.GroupEnabled[5] := TRUE;
    // determine if a MDI child is minimized
  for Cnt := 0 to MDIChildCount - 1 do
    if MDIChildren[Cnt].WindowState = wsMinimized then
    begin
      MenuFunctions.GroupEnabled[3] := TRUE;
    end;
end;
{$ENDIF} // <----------------- AXDLL

{$IFNDEF AXDLL} // <----------------- AXDLL

function TWinGISMainForm.FileNameNew(AName: string): Pointer;
var
  S: TOLEClientStream;
  Proj: PProj;
  FileName: array[0..255] of Char;
  Child: TMDIChild;
begin
  begin
    StrPCopy(FileName, AName);
    if CompareText(ExtractFileName(AName), 'Default.amp') <> 0 then
    begin
      S := TOLEClientStream.Create('Default.amp', stOpenRead);
//      S.Init('Default.amp', stOpenRead, StrBufSize);
      S.ParentWindow := nil;
      if S.Status <> stOK then
      begin
          //MsgBox(Handle,2014,mb_OK or mb_IConExclamation,'');
        Proj := New(PProj, Init);
      end
      else
        Proj := Pointer(S.Get);
      S.Free;
    end
    else
      Proj := New(PProj, Init);
    Child := IsFileOpened(FileName);
    if Child <> nil then
    begin
      Child.Data^.Modified := FALSE;
      Child.Close;
    end;
    CreateNewProj := TRUE;
    ActualChild := TMDIChild.Create(Self, FileName, Proj, False);
    CreateNewProj := FALSE;
    Result := ActualChild;
  end;
end;

procedure TWinGISMainForm.FileNameCloseHSP
  (
  AFileName: PChar
  );
var
  Dir: array[0..fsPathName] of Char;
  DName: array[0..fsFileName + fsExtension] of Char;
  Ext: array[0..fsExtension] of Char;
  AChild: TMDIChild;
  FullFileName: array[0..255] of Char;
begin
  FileExpand(FullFileName, AFileName);
  AnsiLower(FullFileName);
  FileSplit(FullFileName, Dir, DName, Ext);
  StrCopy(FullFileName, Dir);
  StrCat(FullFileName, DName);
  StrCat(FullFileName, '.hsp');
  AChild := IsFileOpened(FullFileName);
  if AChild <> nil then
  begin
    if AChild.Data^.IsCanvas then
    begin
{$IFNDEF WMLT}
      AChild.ResetGraphicParent;
{$ENDIF}
    end;
    if not AChild.IsClosing then
    begin
      SendMessage(AChild.Handle, wm_Close, 0, 0);
    end;
  end;
end;

function TWinGISMainForm.OpenProjectTV
  (
  ALock: Boolean;
  AFileName: string
  ): Boolean;
var
  Data: PProj;
  DoIt: Boolean;
  FileName: array[0..255] of Char;
  WGPFileName: string;
  isLoaded: Boolean;
  abort: Boolean;
  Dialog: TOpenDialog;
begin
  abort := false;
  Result := true;
  Data := TMDIChild(ActualChild).Data;
  WGPFileName := Data^.BaseFile;
  DoIt := CheckID(WGPFileName, AFileName, Data^.CheckId);
  if DoIt then
  begin
    WGPFileName := FileCorrect(WGPFileName);
    isloaded := Data^.Document.OnLoad(WGPFileName); {false wenn key nicht stimmt}
    if not Alock then
      isloaded := true; // NOLOCK !!!
    if isloaded then
    begin
      Data^.Document.ActualLayer := Data^.Layers^.TopLayer^.Text^;
      Data^.PInfo^.RedrawScreen(FALSE);
    end
    else
      Abort := true;
  end
  else
  begin
    if not abort then
    begin
      MsgBox(Handle, 4733, mb_Ok or mb_IconExclamation, '');
      Dialog := TOpenDialog.Create(Self);
      Dialog.Title := GetLangText(107);
      Dialog.Filter := MlgStringList['SaveAsDialog', 33];
      Dialog.FileName := WGPFileName;
      Dialog.DefaultExt := 'hsd';
      if Dialog.Execute then
      begin
        WGPFileName := Dialog.FileName;
        DoIt := CheckID(WGPFileName, AFileName, Data^.CheckId);

        if DoIt then
        begin
          WGPFileName := FileCorrect(WGPFileName);
          Data^.BaseFile := WGPFileName;
          Data^.SetModified;
          Data^.Document.OnLoad(WGPFileName);
          Data^.Document.ActualLayer := Data^.Layers^.TopLayer^.Text^;
          DAta^.PInfo^.RedrawScreen(FALSE);
        end;
      end;
    end;
  end;
  if not DoIt then
    MsgBox(Handle, 4734, mb_Ok or mb_IconExclamation, '');
  if Abort then
  begin
    StrPCopy(FileName, AFileName);
      {Meldung : Hardwarekey stimmt nicht}
    FileNameCloseHSP(FileName);
    MsgBox(Handle, 4729, mb_Ok or mb_IconExclamation, '');
    Result := false;
  end;
end;
{$ENDIF} // <----------------- AXDLL

{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}

procedure TWingisMainForm.TurboVektor;
var
  i: integer;
  FNames: TSTringList;
  ALayer: PLayer;
  Aset: TSaveAsOptions;
begin
  if ActualChild <> nil then
  begin
    FNames := TSTringLIst.Create;
    for i := 1 to Actualchild.Data^.Layers^.LData.Count - 1 do
    begin
      ALayer := Actualchild.Data^.Layers^.LData.at(i);
      FNames.Add(ALayer.TExt^);
    end;
    ConvertToWgpWithLayers(Aset, 'Serial0000', 'e:\wingis\dkm1.amp',
      'e:\wingis\dkm.amp', 'e:\wingis\dkm.hsd', nil, FNames, nil, true, true);
    FNames.Free;
  end;
end;

procedure TWingisMainForm.ConvertToWgpWithLayers(SaveOptions: TSaveAsOptions;
  const SerialNumber, InpFileName, HspFileName, HsdFileName: string;
  HspList, HsdList, OutFileLIst: TStringList; DrawIt, Close: Boolean);
var
  AChild: TMDIChild;
  FileName: array[0..fsPathName] of Char;
  LTProj: Boolean;
  InChild: TMDIChild;
  Doit: Boolean;
begin
  Doit := False;
{$IFNDEF WMLT}
  if DDEHandler <> nil then
  begin {0511MF}
      { close the dde link to the database }
    DDEHandler.CloseProject(GetCurrentChild);
      { disable dde-communication to avoid confusing the database }
    DDEHandler.FSuspendDDE := TRUE;
  end;
{$ENDIF}
  AChild := nil;
  if ActualChild <> nil then
  begin {0511MF}
    try
      InChild := ACtualChild;
      AChild := FileNameNew(HspFileName); {Erstelle Client-amp for wgp-file}
        //Application.ProcessMessages;
      if AChild <> nil then
      begin
        AChild.Data^.BaseMap := saoHSDLayersFirst in SaveOptions;
        AChild.Data^.ExtMap := not AChild.Data^.BaseMap;
        AChild.Data^.AttribFix := saoLockAttributes in SaveOptions;
        AChild.Data^.LogFile := saoCreateLogFile in SaveOptions;
        AChild.Data^.DBUpdate := FALSE;
        LTProj := saoPassword in SaveOptions;
        if saoBindToSerial in SaveOptions then
          AChild.Data^.Document.SetSerialNumber(SerialNumber)
        else
          AChild.Data^.Document.ResetSerialNumber;
        AChild.Data^.Document.ConvertWithLayers(InChild, Self, InpFileName, HspFileName, HsdFileName, HspList, HsdList, OutFileList);
        StrPCopy(FileName, HspFileName);
          //LTPRoj:=false;
          // ssp ... FileSaveAs2(AFName,TRUE,FALSE)
          //amp ... FileSaveAs2(AFName,false,FALSE)
        AChild.FileSaveAs2(FileName, LTProj, TRUE);
        Application.ProcessMessages;
        FileNameCloseHSP(FileName); {Host Hsp- schliessen}
        Application.ProcessMessages;
        AtConverting := FALSE;
      end;
    finally
        { reenable communication with the database }
{$IFNDEF WMLT}
      if DDEHandler <> nil then
        DDEHandler.FSuspendDDE := FALSE; {0511MF}
{$ENDIF}
    end;
  end
  else
  begin
      //MsgBox(HWindow,4326,mb_IconExclamation or mb_OK,'');
  end;
  if AChild <> nil then
    Doit := FileNameOpenNoLock(FileName);
  if Doit then
  begin
    TMDIChild(ACtualchild).Data^.Document.JustCreated := true;
    if saoBindToSerial in SaveOptions then
      TMDIChild(ACtualchild).Data^.PInfo^.Redrawscreen(false);
  end;

end;

procedure TWingisMainForm.ConvertToWgp(SaveOptions: TSaveAsOptions; const SerialNumber,
  AOutFileName, AWgpFileName: string; AList: TStringList; DrawIt, Close: Boolean);
var
  Achild: TMDIChild;
  FileName: array[0..fsPathName] of Char;
  LTProj: Boolean;
  Doit: Boolean;
begin
  Doit := False;
{$IFNDEF WMLT}
  if DDEHandler <> nil then
  begin {0511MF}
    { close the dde link to the database }
    DDEHandler.CloseProject(GetCurrentChild);
    { disable dde-communication to avoid confusing the database }
    DDEHandler.FSuspendDDE := TRUE;
  end;
{$ENDIF}
  try
    AChild := FileNameNew(AOutFileName); {Erstelle Host-hsp for wgp-file}
    Application.ProcessMessages;
    if AChild <> nil then
    begin
      AChild.Data^.BaseMap := saoHSDLayersFirst in SaveOptions;
      AChild.Data^.ExtMap := not AChild.Data^.BaseMap;
      AChild.Data^.AttribFix := saoLockAttributes in SaveOptions;
      AChild.Data^.LogFile := saoCreateLogFile in SaveOptions;
      AChild.Data^.DBUpdate := TRUE;
      LTProj := saoPassword in SaveOptions;
      if saoBindToSerial in SaveOptions then
        AChild.Data^.Document.SetSerialNumber(SerialNumber)
      else
        AChild.Data^.Document.ReSetSerialNumber;
      Application.ProcessMessages;
      AChild.Data^.Document.Convert(Self, AOutFileName, AWgpFileName, AList);
      StrPCopy(FileName, AOutFileName);
      AChild.FileSaveAs2(FileName, LTProj, true);
      Application.ProcessMessages;
      FileNameCloseHSP(FileName); {Host Hsp- schliessen}
      Application.ProcessMessages;
      AtConverting := FALSE;
    end;
  finally
    { reenable communication with the database }
{$IFNDEF WMLT}
    if DDEHandler <> nil then
      DDEHandler.FSuspendDDE := FALSE; {0511MF}
{$ENDIF}
  end;
  if AChild <> nil then
    Doit := FileNameOpenNoLock(FileName);
  if Doit then
  begin
    TMDIChild(ACtualchild).Data^.Document.JustCreated := true;
    if saoBindToSerial in SaveOptions then
      TMDIChild(Actualchild).Data^.PInfo^.Redrawscreen(false);
  end;
  //TMDIChild(ACtualchild).Data^.Document.JustCreated:=true;
  //if saoBindToSerial in SaveOptions then  TMDIChild(ACtualchild).Data^.PInfo^.Redrawscreen(false);
end;
{$ENDIF}

procedure TWinGISMainForm.UIInitializeApplicationActivate(Sender: TObject);
begin
  UserInterface.Update([uiMenus, uiCursor]);
end;

{$IFNDEF WMLT}

procedure TWinGISMainForm.GisCommReadDDEWMIniSettings(var Msg: TMessage);
begin
  DDEHandler.ReadDDEWMIniSettings;
end;

procedure TWinGISMainForm.GisCommReadDDEMLServerIniSettings(var Msg: TMessage);
begin
  DDEHandler.ReadDDEMLServerIniSettings;
end;

procedure TWinGISMainForm.GisCommReadDDEMLIniSettings(var Msg: TMessage);
begin
  DDEHandler.ReadDDEMLIniSettings;
end;

procedure TWinGISMainForm.GisCommGetDDECommand(var Msg: TMessage);
begin
  DDEHandler.GetNewDDECommand;
end;

procedure TWinGISMainForm.GisCommSetOneSelect(var Msg: TMessage);
begin
  if Msg.LParam = 1 then
    DDEHandler.SetOneSelect(TRUE)
  else
    DDEHandler.SetOneSelect(FALSE);
end;

procedure TWinGISMainForm.GisCommSetAutoInsert(var Msg: TMessage);
begin
  if Msg.LParam = 1 then
    DDEHandler.SetDBAutoInsert(TRUE)
  else
    DDEHandler.SetDBAutoInsert(FALSE);
end;

procedure TWinGISMainForm.GisCommSendProjDataToDB(var Msg: TMessage);
var
  ActMDIChild: TMDIChild;
begin
  ActMDIChild := GetCurrentChild;
  if ActMDIChild <> nil then
    ActMDIChild.SendProjDataToDB;
end;

procedure TWinGISMainForm.GisCommSetDBSelMode(var Msg: TMessage);
var
  ActMDIChild: TMDIChild;
begin
  ActMDIChild := GetCurrentChild;
  if ActMDIChild <> nil then
  begin
    if Msg.LParam = 1 then
      ActMDIChild.Data^.PInfo^.SelectionSettings.DBSelectionMode := TRUE
    else
      ActMDIChild.Data^.PInfo^.SelectionSettings.DBSelectionMode := FALSE;
  end;
end;

procedure TWinGISMainForm.GisCommAddDBEntry(var Msg: TMessage);
var
  ACommName: array[0..255] of Char;
  ACon: Boolean;
  AConProj: Boolean;
  ASendStr: Boolean;
  ASendProj: Boolean;
begin
  DDEHandler.GetNewDBEntry(ACommName, ACon, AConProj, ASendStr, ASendProj);
  AddDBEntry(ACommName, ACon, AConProj, ASendStr, ASendProj);
end;

procedure TWinGISMainForm.GisCommWriteDDEDDEMLSettings(var Msg: TMessage);
begin
  DDEHandler.WriteDDEDDEMLSettings;
end;

procedure TWinGISMainForm.GisCommPostRDRCommAfterDDE(var Msg: TMessage);
begin
  PostRDRCommAfterDDE;
end;

procedure TWinGISMainForm.GisCommSendRDRCommAfterDDE(var Msg: TMessage);
begin
  SendRDRCommAfterDDE;
  DDEHandler.SendSep;
end;

procedure TWinGISMainForm.GisCommGetLayerName(var Msg: TMessage);
begin
  DDEHandler.GetLayerName(Msg.LParam);
end;

procedure TWinGISMainForm.GisCommGetTopLayerName(var Msg: TMessage);
begin
  DDEHandler.GetTopLayerName;
end;

procedure TWinGISMainForm.GisCommSetStatusText(var Msg: TMessage);
begin
  DDEHandler.SetStatusText(Msg.LParam);
end;

procedure TWinGISMainForm.GisCommShowMsgBox(var Msg: TMessage);
begin
  DDEHandler.DoShowMessage;
end;

procedure TWinGISMainForm.GisCommWriteLog(var Msg: TMessage);
begin
  DDEHandler.WriteLog;
end;

procedure TWinGISMainForm.GisCommSetTRData(var Msg: TMessage);
begin
  DDEHandler.SetTRData;
end;
{$ENDIF}
{$ENDIF} // <----------------- AXDLL

procedure TWinGISMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if ThematicDLLHandle <> 0 then
  begin
    FreeLibrary(ThematicDLLHandle);
    ThematicDLLHandle := 0;
  end;
  if ISOLibHandle <> 0 then
  begin
    FreeLibrary(ISOLibHandle);
    ISOLibHandle := 0;
  end;
  if RunUpdateOnClose then
  begin
    RunUpdateSilent := True;
{$IFNDEF AXDLL}
    ExtrasIUpdate;
{$ENDIF}
  end;
end;

{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}

procedure TWinGISMainForm.ActivateWingis;
begin
  if not HideWindow then
  begin
    SetWindowPos(Handle, HWND_TOPMOST, Left, Top, Width, Height, SWP_SHOWWINDOW);
    SetWindowPos(Handle, HWND_NOTOPMOST, Left, Top, Width, Height, SWP_SHOWWINDOW);
    SetForegroundWindow(Handle);
  end;
end;
{$ENDIF}

{$IFNDEF WMLT}

procedure TWingisMainForm.ExecuteMenuFunction(ItemName: AnsiString);
begin
  if MenuFunctions[ItemName].Enabled then
  begin
    MenuFunctions.Call(ItemName);
  end;
end;

// a dll of a module has to be loaded and the module has to be executed

procedure TWingisMainForm.ActivateModule(var aModuleDll: THandle; DllName: string; DllIdx: integer; LngCode: string);
type
  TExecModule = procedure(DIR: PCHAR; var AXHANDLE: VARIANT; var DOCUMENT: VARIANT; LNGCODE: PCHAR; WINHANDLE: integer; WTOP: integer; WLEFT: integer; WWIDTH: integer; WHEIGHT: integer; DLLIDX: integer); stdcall;
var
  EXECMODULE: TExecModule;
  ExecModulePtr: TFarProc;
  pDllName: Pchar;
  pCurDir: Pchar;
  pLngCode: Pchar;
  curdoc: Variant;
begin
  DllName := DllName + '.DLL';
  pDllName := stralloc(length(DllName) + 1);
  strpcopy(pDllName, DllName);
  aModuleDll := LoadLibrary(pDllName);
  CheckLibStatus(aModuleDll, DllName, '');
      // get pointer to execute module function
  ExecModulePtr := GetProcAddress(aModuleDll, 'EXECMODULE');
  if ExecModulePtr <> nil then
  begin
    @EXECMODULE := ExecModulePtr;
    pCurDir := stralloc(length(OSInfo.WingisDir) + 1);
    strpcopy(pCurDir, OSInfo.WingisDir);
    pLngCode := stralloc(length(LngCode) + 1);
    strpcopy(pLngCode, LngCode);
    CheckTDocumentIsCorrect;
    curdoc := GetCurrentTDocument;
    if (ActualChild <> nil) and (ActualChild.Data <> nil) and (ActualChild.Data^.IsCanvas) then
    begin
      EXECMODULE(pCurdir, DXApp, curdoc, pLngCode, 0, Self.Top, Self.Left, Self.Width, Self.Height, DllIdx);
    end
    else
    begin
      EXECMODULE(pCurdir, DXApp, curdoc, pLngCode, Handle, Self.Top, Self.Left, Self.Width, Self.Height, DllIdx);
    end;
    strdispose(pCurDir);
    strdispose(pLngCode);
  end;
  strdispose(pDllName);
end;

// a module has to be closed and the dll released

procedure TWinGISMainForm.DeactivateModule(var aModuleDll: THandle);
type
  TFreeModule = procedure; stdcall;
var
  FREEMODULE: TFreeModule;
  FreeModulePtr: TFarProc;
begin
  FreeModulePtr := GetProcAddress(aModuleDll, 'FREEMODULE');
  if FreeModulePtr <> nil then
  begin
    @FREEMODULE := FreeModulePtr;
    FREEMODULE;
  end;
  FreeLibrary(aModuleDll);
  aModuleDll := 0;
end;

{$ENDIF}

procedure TWinGISMainForm.IdleProc(Sender: TObject; var Done: Boolean);
var
  PmsMutex: Integer;
  ACloseAction: TCloseAction;
  AChild: TMDIChild;
  i: Integer;
begin
  if (DoTerminate) or ((CanvasHandle <> 0) and (not IsWindow(CanvasHandle))) then
  begin
    if (not IsSaving) then
    begin
      try
        for i := 0 to MDIChildCount - 1 do
        begin
          AChild := GetCurrentChild;
          if AChild <> nil then
          begin
            AChild.Free;
          end;
        end;
        ACloseAction := caFree;
        FormClose(Self, ACloseAction);
        FormDestroy(nil);
      finally
        Halt;
      end;
    end;
  end;

  if (CanvasHandle <> 0) and (IsWindow(CanvasHandle)) then
  begin
    Application.BringToFront;
  end;
end;
{$ENDIF} // <----------------- AXDLL

{++ IDB}
{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}

function TWinGISMainForm.SwitchIDBOn(AProject: PProj): Boolean;
var
  bLoadIDB: Boolean;
begin
  RESULT := FALSE;

  if AProject = nil then
    bLoadIDB := bWeAreUsingTheIDB
  else
    bLoadIDB := AProject.IDBSettings.bLocalWeAreUsingTheIDB;

  if bLoadIDB then
  begin
    if IDB_Man <> nil then
    begin
      RESULT := TRUE;
      EXIT;
    end;
    IDB_Man := TIDB.Create;
    if WinGISMainForm.IDB_Man.LibraryWasLoaded then
    begin
      bWeAreUsingTheIDB := TRUE;
      RESULT := TRUE;
    end
    else
    begin
      IDB_Man.Free;
      IDB_Man := nil;
    end;
  end
end;

procedure TWinGISMainForm.SwitchIDBOff(AProject: PProj);
begin
  if AProject = nil then
    bWeAreUsingTheIDB := FALSE;
  try
    if IDB_Man <> nil then
      IDB_Man.Free;
    IDB_Man := nil;
  except
  end;
end;

function TWinGISMainForm.SwitchUndoRedoOn(AProject: PProj): Boolean;
var
  bLoadUndoRedo: Boolean;
begin
  RESULT := FALSE;

  bLoadUndoRedo := SELF.WeAreUsingUndoFunction[AProject];
{     if AProject = NIL Then
        bLoadUndoRedo := bWeAreUsingUndoFunction
     else
        bLoadUndoRedo := AProject.IDBSettings.bLocalWeAreUsingUndoFunction;}

  if bLoadUndoRedo then
  begin
    if UndoRedo_Man <> nil then
    begin
      RESULT := TRUE;
      EXIT;
    end
    else
    begin
      UndoRedo_Man := TUndoRedoManager.Create;
      if UndoRedo_Man.LibraryWasLoaded then
      begin
        bWeAreUsingUndoFunction := TRUE;
        RESULT := TRUE;
      end
      else
      begin
        UndoRedo_Man.Free;
        UndoRedo_Man := nil;
      end;
    end
{        else
        begin
           SwitchUndoRedoOff(nil);
           if AProject <> nil then
              AProject.IDBSettings.bLocalWeAreUsingUndoFunction := FALSE;
           WinGISMainForm.bWeAreUsingUndoFunction := FALSE;
        end;}
  end;
end;

procedure TWinGISMainForm.SwitchUndoRedoOff(AProject: PProj);
begin
  if AProject = nil then
    bWeAreUsingUndoFunction := false;
  try
    if UndoRedo_Man <> nil then
      UndoRedo_Man.Free;
    UndoRedo_Man := nil;
  except
  end;
end;

function TWinGISMainForm.GetWeAreUsingTheIDB(AProject: PProj): Boolean;
begin
  RESULT := False;
  if (AProject <> nil) {AND (NOT AProject.IDBSettings.bUseGlobalIDBSettings)} then
  begin
    RESULT := (IDBGlobalOnOff) and (AProject.IDBSettings.bLocalWeAreUsingTheIDB);

    if AProject^.PInfo^.IsMU then
    begin
      Result := False;
      exit;
    end;
  end;
end;

procedure TWinGISMainForm.SetWeAreUsingTheIDB(AProject: PProj; bValue: Boolean);
begin
  SELF.bWeAreUsingTheIDB := False;
  if (AProject <> nil) {AND (NOT AProject.IDBSettings.bUseGlobalIDBSettings)} then
    AProject.IDBSettings.bLocalWeAreUsingTheIDB := bValue;
end;

function TWinGISMainForm.GetWeAreUsingTheIDBUserInterface(AProject: PProj): Boolean;
begin
  RESULT := SELF.bWeAreUsingTheIDBUserInterface;

  if (AProject <> nil) {and
        (not AProject.IDBSettings.bUseGlobalIDBSettings)}then
  begin

    if AProject^.PInfo^.IsMU then
    begin
      Result := False;
      exit;
    end;

// ++ Cadmensky IDB Version 2.3.1
    if (IDBGlobalOnOff) and (AProject.IDBSettings.bLocalWeAreUsingTheIDB) then
// -- Cadmensky IDB Version 2.3.1
      RESULT := AProject.IDBSettings.bLocalWeAreUsingTheIDBUserInterface
// ++ Cadmensky IDB Version 2.3.1
    else
      RESULT := false;
// -- Cadmensky IDB Version 2.3.1
  end;
end;

procedure TWinGISMainForm.SetWeAreUsingTheIDBUserInterface(AProject: PProj; bValue: Boolean);
begin
  if (AProject <> nil) {AND (NOT AProject.IDBSettings.bUseGlobalIDBSettings)} then
    AProject.IDBSettings.bLocalWeAreUsingTheIDBUserInterface := bValue
  else
    SELF.bWeAreUsingTheIDBUserInterface := False;
end;

function TWinGISMainForm.GetWeAreUsingUndoFunction(AProject: PProj): Boolean;
begin
{     If NOT SELF.bWeAreUsingTheIDB Then Begin
        RESULT := FALSE;
        EXIT;
        End;}
  RESULT := SELF.bWeAreUsingUndoFunction;
     //If (AProject <> NIL) AND (NOT AProject.IDBSettings.bUseGlobalIDBSettings) Then
     //   RESULT := AProject.IDBSettings.bLocalWeAreUsingUndoFunction;
end;

procedure TWinGISMainForm.SetWeAreUsingUndoFunction(AProject: PProj; bValue: Boolean);
begin
  if (AProject <> nil) and (not AProject.IDBSettings.bUseGlobalIDBSettings) then
    AProject.IDBSettings.bLocalWeAreUsingUndoFunction := bValue
  else
    SELF.bWeAreUsingUndoFunction := bValue;
end;

function TWinGISMainForm.GetIDBUndoStepsCount(AProject: PProj): integer;
begin
{     If NOT SELF.bWeAreUsingUndoFunction Then Begin
        RESULT := -1;
        EXIT;
        End;}
  RESULT := SELF.iIDBUndoStepsCount;
{     if (AProject <> nil) and (not AProject.IDBSettings.bUseGlobalIDBSettings) Then
        RESULT := AProject.IDBSettings.iLocalUndoStepsCount;}
end;

procedure TWinGISMainForm.SetIDBUndoStepsCount(AProject: PProj; bValue: integer);
begin
  if SELF.bWeAreUsingUndoFunction then
  begin
{        If (AProject <> NIL) AND (NOT AProject.IDBSettings.bUseGlobalIDBSettings) Then
           AProject.IDBSettings.iLocalUndoStepsCount := bValue
        Else}
    SELF.iIDBUndoStepsCount := bValue;
  end;
end;

procedure TWinGISMainForm.ReverseUsingIDBInfo;
begin
  bWeAreUsingTheIDBInfoWindow := not bWeAreUsingTheIDBInfoWindow;
  if not bWeAreUsingTheIDBInfoWindow then
    Self.IDB_Man.CloseInfoTable;
end;

function TWinGISMainForm.GetWeAreUsingTheIDBInfoWindow: Boolean;
begin
  RESULT := bWeAreUsingTheIDBInfoWindow;
end;

procedure TWinGISMainForm.SetWeAreUsingTheIDBInfoWindow(bValue: Boolean);
begin
  bWeAreUsingTheIDBInfoWindow := bValue;
end;
{-- IDB}
{$ENDIF}
{$ENDIF} // <----------------- AXDLL

procedure TWinGISMainForm.SelectFolderDialogClose(Sender: TObject);
var
  DirName: AnsiString;
begin
  DirName := TSelectFolderDialog(Sender).FileName;
  if DirectoryExists(DirName) then
    TSelectFolderDialog(Sender).InitialDir := DirName;
end;

procedure TWinGISMainForm.TimerFreeImpExpDllTimer(Sender: TObject);
type
  TFreeDll = function: boolean; stdcall;
var
  FREEDLL: TFreeDll;
  FreeDllPtr: TFarProc;
  wasok: boolean;
begin
   // the import/export routines need a little time to finish
   // so the Dll�s will be released here in this timer
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
  TimerFreeImpExpDll.Enabled := FALSE;
  if (ImpExpDll = 0) then
    exit;
  FreeDllPtr := GetProcAddress(ImpExpDLL, 'FREEDLL');
  if FreeDllPtr <> nil then
  begin
    @FREEDLL := FreeDllPtr;
    wasok := FREEDLL;
  end
  else
    wasok := FALSE;
  if LastImpExpOperation <> 'SHPIMP' then // for Shape-Import the library will not be released due to still not analysed problem
    FreeLibrary(ImpExpDLL);
  ImpExpDLL := 0;
  LastImpExpOperation := ''; // reset the last import export operation

   // check if there has a postprocessing done for this import/export
  if ImpExpDllData <> nil then
  begin
    if ImpExpDllData^.aImpExpMode = 'AMPDKMIMPORT' then
      ExecAmpDkmImport;
  end
  else
  begin
    if wasok then
      DDEHandler.Sendstring('[ONIMPEXPFINISHED][OK]')
    else
      DDEHandler.Sendstring('[ONIMPEXPFINISHED][FAIL]');
  end;
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
end;

procedure TWinGISMainForm.FinishModuleTimerTimer(Sender: TObject);
var
  Dllidx: integer;
begin
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
  FinishModuleTimer.Enabled := FALSE;
  DllIdx := FinishModuleTimer.Tag;
  if ImpTxtLib <> 0 then // check if File->Import->Coordinate File used module interface
    DeactivateModule(ImpTxtLib)
  else
    DeactivateModule(GisModules[DllIdx]);
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
end;

procedure TWinGISMainForm.SetAppTitle;
var
  s: AnsiString;
begin
  if WingisLicense >= WG_PROF then
  begin
    s := 'WinGIS Professional';
  end
  else
    if WingisLicense = WG_STD then
    begin
      s := 'WinGIS Standard';
    end
    else
    begin
      s := 'WinGIS Viewer';
    end;
  if RunRemote then
  begin
    s := s + ' - Remote';
  end;
  Caption := s;
  Application.Title := s;
end;

{$IFNDEF AXDLL}

procedure TWinGISMainForm.ExtrasTABConverter;
begin
  ShellExecute(0, 'open', PChar(OSInfo.WingisDir + 'TFW2TAB.exe'), nil, nil, SW_SHOW);
end;

procedure TWinGISMainForm.ExtrasLanguage;
var
  OldLanguageID: AnsiString;
begin
  OldLanguageID := CurrentLanguageID;
  CheckLanguageEntries(True);
  if (CurrentLanguageID <> OldLanguageID) then
  begin
    MessageBox(Handle, PChar(AnsiString(GetLangText(25001))), PChar(AnsiString(Application.Title)), MB_OK or MB_ICONINFORMATION);
  end;
end;

function TWinGISMainForm.GetWingisVersion: AnsiString;
var
  ProgName: array[0..255] of Char;
  VersionInfo: PChar;
  FileOffset: LongWord;
  StructSize: LongInt;
  Info: Pointer;
  InfoSize: LongWord;
begin
  Result := '';
  GetModuleFileName(HInstance, ProgName, SizeOf(ProgName));
  StructSize := GetFileVersionInfoSize(ProgName, FileOffset);
  if StructSize > 0 then
  begin
    GetMem(VersionInfo, StructSize);
    if VersionInfo <> nil then
    begin
      if GetFileVersionInfo(ProgName, FileOffset, StructSize, VersionInfo) then
      begin
        VerQueryValue(VersionInfo, '\StringFileInfo\080904E4\FileVersion', Info, InfoSize);
        Result := PChar(StrPas(Info));
      end;
      FreeMem(VersionInfo, StructSize);
    end;
  end;
end;
{$ENDIF}

procedure TWinGISMainForm.CleanDir;
var
  ASearch: TSearchRec;
begin
  if SysUtils.FindFirst(OSInfo.WingisDir + '*.log', faAnyFile, ASearch) = 0 then
  begin
    DeleteFile(PChar(OSInfo.WingisDir + ASearch.Name));
    while SysUtils.FindNext(ASearch) = 0 do
    begin
      DeleteFile(PChar(OSInfo.WingisDir + ASearch.Name));
    end;
  end;
  SysUtils.FindClose(ASearch);
end;

function TWinGISMainForm.IsOnline: Boolean;
var
  dlvFlag: DWord;
begin
  dlvFlag := Internet_Connection_Modem or Internet_Connection_Lan or Internet_Connection_Proxy;
  Result := InternetGetConnectedState(@dlvFlag, 0);
end;

procedure TWinGISMainForm.FormDblClick(Sender: TObject);
var
  s: AnsiString;
begin
  if (GetKeyState(VK_CONTROL) < 0) then
  begin
    s := Trim(InputBox('Execute', 'Open:', ''));
    if s <> '' then
    begin
{$IFNDEF AXDLL}
      ShellExecute(0, 'open', PChar(s), nil, nil, SW_SHOW);
{$ENDIF}
    end;
  end;
end;

function TWinGISMainForm.ReadRemoteSettings: Boolean;
var
  Reg: TRegistry;
begin
  Reg := TRegistry.Create;
  try
    Reg.RootKey := HKEY_LOCAL_MACHINE;
    if Reg.OpenKeyReadOnly('SOFTWARE\PROGIS\WinGIS') then
    begin
      if Reg.ValueExists('Remote') then
      begin
        Result := (Reg.ReadInteger('Remote') <> 0);
      end;
    end;
  finally
    Reg.Free;
  end;
end;

{$IFNDEF AXDLL}

procedure TWinGISMainForm.SendDDEToDkmApp(KGList, GSTList: TStringList);
var
  s: AnsiString;
  DDEData: PChar;
  DDEDataSize: Integer;
  LogList: TStringList;
  AService, ATopic, AItem, AUserId, AStartFile: AnsiString;
  AWaitForStart: Integer;
  AMode: Integer;
  Ini: TIniFile;
  AFailed: Boolean;
  LinkOK: Boolean;
  ShellRes: Integer;
  ShellParams: AnsiString;
  ShowMsg: Boolean;
  i: Integer;
  GST, KG: AnsiString;
begin
  AFailed := False;
  AService := '';
  ATopic := '';
  AItem := '';
  AUserId := '';
  AStartFile := '';
  AWaitForStart := 0;
  LinkOK := False;
  ShellRes := 0;
  ShowMsg := True;
  Ini := TIniFile.Create(OSInfo.WingisIniFileName);
  try
    if Ini <> nil then
    begin
      AService := Ini.ReadString('DKMAPP', 'Service', '');
      ATopic := Ini.ReadString('DKMAPP', 'Topic', '');
      AItem := Ini.ReadString('DKMAPP', 'Item', '');
      AUserId := Ini.ReadString('DKMAPP', 'UserId', '');
      AStartFile := Ini.ReadString('DKMAPP', 'Start', '');
      AWaitForStart := Ini.ReadInteger('DKMAPP', 'WaitForStart', 0);
      AMode := Ini.ReadInteger('DKMAPP', 'Mode', 0);
    end;
  finally
    Ini.Free;
  end;
  if (AService = '') or (ATopic = '') then
  begin
    AFailed := True;
  end;
  LogList := TStringList.Create;
  try
    LogList.Add('DDE-Service: ' + AService);
    LogList.Add('DDE-Topic: ' + ATopic);
    LogList.Add('DDE-Item: ' + AItem);

    if (GSTList.Count > 0) then
    begin

      s := 'INF;' + IntToStr(GSTList.Count) + ';';

      for i := 0 to GSTList.Count - 1 do
      begin
        GST := GSTList[i];
        KG := KGList[i];
        if Pos('/', GST) > 0 then
        begin
          GST := StringReplace(GST, '/', ',', []);
        end
        else
        begin
          GST := GST + ',';
        end;
        if Pos('.', GST) > 0 then
        begin
          GST := StringReplace(GST, '.', '', []);
          GST := ',.,' + GST;
        end
        else
        begin
          GST := ',,' + GST;
        end;
        s := s + KG + GST + ';';
      end;

      LogList.Add('DDE-String: ' + s);
      DDEDataSize := StrLen(PChar(s)) + 1;
      GetMem(DDEData, DDEDataSize);
      ZeroMemory(DDEData, DDEDataSize);
      StrPCopy(DDEData, s);
      DDEClient.DdeService := AService;
      if DDEClient.SetLink(AService, ATopic) then
      begin
        LinkOK := DDEClient.OpenLink;
        if not LinkOK then
        begin
          ShowMsg := False;
          ShellParams := AUserID + ' ' + ATopic;
          ShellRes := ShellExecute(0, 'open', PChar(AStartFile), PChar(ShellParams), '', SW_SHOWDEFAULT);
          if ShellRes <= 32 then
          begin
            LogList.Add('ShellExecute: FAILED (' + IntToStr(ShellRes) + ')');
          end
          else
          begin
            Sleep(AWaitForStart);
            LinkOK := DDEClient.OpenLink;
          end;
        end;
        if not LinkOK then
        begin
          LogList.Add('DDE-OpenLink: FAILED');
          AFailed := True;
        end;
      end
      else
      begin
        LogList.Add('DDE-SetLink: FAILED');
        AFailed := True;
      end;
      if LinkOK then
      begin
        if AMode = 0 then
        begin
          if not DDEClient.PokeData(s, DDEData) then
          begin
            LogList.Add('DDE-PokeData: FAILED');
            AFailed := True;
          end;
        end
        else
        begin
          if not DDEClient.ExecuteMacro(DDEData, False) then
          begin
            LogList.Add('DDE-ExecuteMacro: FAILED');
            AFailed := True;
          end;
        end;
        DDEClient.CloseLink;
      end;
      FreeMem(DDEData);
    end;
    if (AFailed) and (ShowMsg) then
    begin
      Application.MessageBox(PChar(LogList.Text), PChar(Caption), MB_ICONERROR or MB_OK);
    end;
  finally
    LogList.Free;
  end;
end;
{$ENDIF}

function TWinGISMainForm.UseDkmApp: Boolean;
var
  Ini: TIniFile;
begin
  Result := False;
  Ini := TIniFile.Create(OSInfo.WingisIniFileName);
  try
    if Ini <> nil then
    begin
      Result := (Ini.ReadInteger('DKMAPP', 'Active', 0) <> 0);
    end;
  finally
    Ini.Free;
  end;
end;

procedure TWinGISMainForm.WingisDKMExecuteMacro(Sender: TObject; Msg: TStrings);
var
  s: AnsiString;
  GList: TStringList;
  i: Integer;
  GST, KG, P, G1, G2: AnsiString;
  ID: Integer;
begin
  if MDIChildCount = 0 then
  begin
    exit;
  end;
  ID := 0;
  Msg.Text := Trim(Msg.Text);
  Msg.Text := StringReplace(Msg.Text, ';', #13, [rfReplaceAll]);
  if Msg.Count = 1 then
  begin
    Msg.Text := StringReplace(Msg.Text, #13, #13#10, [rfReplaceAll]);
  end;
  GList := TStringList.Create;
  ActualChild.Data^.DeSelectAll(False);
  try
    for i := 2 to Msg.Count - 1 do
    begin
      s := Trim(Msg[i]);
      if s <> '' then
      begin
        GList.Text := s;
        GList.Text := StringReplace(GList.Text, ',', #13, [rfReplaceAll]);
        if GList.Count = 1 then
        begin
          GList.Text := StringReplace(GList.Text, #13, #13#10, [rfReplaceAll]);
        end;
        if GList.Count = 4 then
        begin
          KG := Trim(GList[0]);
          P := Trim(GList[1]);
          G1 := Trim(GList[2]);
          G2 := Trim(GList[3]);
          GST := P + G1;
          if G2 <> '' then
          begin
            GST := P + G1 + '/' + G2;
          end;
          ID := GSTToProgisID(ActualChild.Data^.FName, PChar(ActualChild.Data^.Layers^.TopLayer^.Text^), PChar(KG), PChar(GST));
          if ID <> 0 then
          begin
            ActualChild.Data^.SelectByIndex(ID);
          end;
        end;
      end;
    end;
  finally
    GList.Free;
  end;
  ProcShowAllSel(ActualChild.Data);
end;

initialization
  begin
    MenuFunctions.RegisterFromResource(HInstance, 'MenuFn', 'Hint');
{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}
    ExitIfAlreadyRunning;
{$ENDIF}
{$ENDIF} // <----------------- AXDLL
  end;

finalization
  begin
{$IFNDEF WMLT}
    if CheckHandle <> 0 then
    begin
      CloseHandle(CheckHandle);
    end;
{$ENDIF}
  end;
end.

