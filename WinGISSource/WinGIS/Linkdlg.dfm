�
 TLINKDIALOG 0�	  TPF0TLinkDialog
LinkDialogLeft� Top� HelpContextBorderStylebsDialogCaption!1ClientHeight� ClientWidth Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrder	PositionpoScreenCenterOnCreate
FormCreate	OnDestroyFormDestroyOnHideFormHideOnShowFormShowPixelsPerInch`
TextHeight TButton	CancelBtnLeft�Top%WidthyHeightCancel	Caption!10ModalResultTabOrder  TButtonOkBtnLeft�TopWidthyHeightCaption!9Default	ModalResultTabOrder  TButton	EmbeddBtnLeft�Top� WidthyHeightCaption!8TabOrderVisible  TButton	DeleteBtnLeft�TopUWidthyHeightCaption!7TabOrderOnClickDeleteBtnClick  TButtonChangeSourceBtnLeft�TopsWidthyHeightCaption!6TabOrderOnClickChangeSourceBtnClick  TFileTreeViewLinkListLeftTop.WidthqHeight� 
ItemHeightMultiSelect	ParentShowHint	PopupMenu	PopupMenuShowHint	TabOrderOnClickLinkListClickOnMouseMoveLinkListMouseMoveMergeEmptyDirectories	
OLEBitmaps	OnFilterItemsLinkListFilterItemsRelativeCaption!18
Scrollbars
ssVertical  
TWGroupBoxGroup1LeftTopWidthqHeight!Caption!2TabOrder BoxStyle	bsTopLine 	TCheckBoxPicturesCheckLeftTopWidthGHeightCaption!3Checked	State	cbCheckedTabOrder OnClickViewCheckClick  	TCheckBoxOLEObjectsCheckLeftPTopWidthMHeightCaption!4Checked	State	cbCheckedTabOrderOnClickViewCheckClick  	TCheckBoxMultimediaCheckLeft� TopWidthWHeightCaption!5Checked	State	cbCheckedTabOrderOnClickViewCheckClick  	TCheckBoxImagesInSymbolsCheckLeftTopWidthmHeightCaption!28Checked	State	cbCheckedTabOrderOnClickViewCheckClick   TMlgSection
MlgSectionSection
LinkDialogLeft|Top�   
TPopupMenu	PopupMenuLeft�Top�  	TMenuItem
DeleteMenuCaption!7OnClickDeleteBtnClick  	TMenuItemChangeSourceMenuCaption!6OnClickChangeSourceBtnClick  	TMenuItemN1Caption-  	TMenuItemModeMenuCaption!15 	TMenuItemAbsolutePathsMenuCaption!16
GroupIndex	RadioItem	OnClickAbsolutePathsMenuClick  	TMenuItemRelativePathsMenuCaption!17Checked	
GroupIndex	RadioItem	OnClickRelativePathsMenuClick    TWOpenDialog
OpenDialogFilter!20OptionsofHideReadOnlyofExtensionDifferentofPathMustExistofFileMustExistofEnableSizing Title!19Left�Top�   TSelectFolderDialogDirectorySelectDialogTitle!19Left�Top�    