//****************************************************************************
// Unit Helmert - used in BMPImage
//----------------------------------------------------------------------------
// Created by Y.Glukhov, 18.05.2000, Moscow
//			Calculates parameters for a Helmert transformation
// Changes
//****************************************************************************
// Glukhov HELMERT BUILD#115 18.05.00

{++ Glukhov HELMERT BUILD#115 23.05.00 }
{-- Glukhov HELMERT BUILD#115 23.05.00 }

unit Helmert;

interface

// uses Math, BMPImage, AM_Def;
// uses BMPImage, AM_Def;
uses AM_Def;

{++ Glukhov HELMERT BUILD#115 18.05.00 }
//---------

//---------
// Procedures makes move for the given point and vector
//---------
Procedure ProcMakeMove
(
  var ioPoint   : TLPoint;
  inDx, inDy 		: LongInt
);

//---------
// Procedures adjusts a given angle slightly.
// It changes the given angle, if it is not in range of limits
//---------
Function ProcAdjustAngleSlightly
(
  inAngle     : Double;     // given angle
  inMinLimit	: Double;			// lower limit
  inMaxLimit	: Double;			// upper limit
  inChange		: Double = 0.0		// value of correction
)
: Double;

//---------
// Procedures calculates the parameters of the affine transformation
// that transforms the given parallelogram1 to the given parallelogram2
//---------
// We assume that
//		x2 = a1*x1 + b1*y1 + c1
//		y2 = a2*x1 + b2*y1 + c2
//---------
Procedure ProcGetAffineTransformationMatrix
(
  inPar1LB   	: TLPoint;        // given parallelogram1
  inPar1RB   	: TLPoint;
  inPar1LT   	: TLPoint;
  inPar2LB   	: TLPoint;        // given parallelogram2
  inPar2RB   	: TLPoint;
  inPar2LT   	: TLPoint;
  var ouA1, ouB1   	: Double;
  var ouA2, ouB2   	: Double;
  var ouC1, ouC2   	: LongInt;
  var ouResultCode  : Integer		// additional result code
);

//---------
// Procedure calculates the parameters of the given affine transformation
// to perform it for a bitmap, using 1st rotation, X-stretch, and 2nd rotation;
// the last uniform X-Y stretch is ignored; the bitmap move is ignored also.
//---------
Procedure ProcGetSpecAffineTransformationData
(
  inA1, inB1	: Double;
  inA2, inB2	: Double;
  var ouR, ouK, ouA : Double
);

//---------
// The procedures makes the Helmert transformation for the given
// pairs of points (target,source) and for the given source parallelogram.
// It returns the transformed parallelogram.
//---------
Procedure ProcGetHelmertTransformedRect
(
	inPoints 	: Pointer;		// pairs of points defining the transformation
  inPtCnt 	: Word;       // number of pairs
  inParLT   	: TLPoint;        // given parallelogram
  inParRT   	: TLPoint;
  inParRB   	: TLPoint;
  inParLB   	: TLPoint;
  var ouParLT   	: TLPoint;    // result parallelogram
  var ouParRT   	: TLPoint;
  var ouParRB   	: TLPoint;
  var ouParLB   	: TLPoint;
  var ouResultCode  : Integer		// additional result code
);

//---------
// The procedures calculates ClipRect for the given 4 points
//---------
Procedure ProcGetClipRect
(
  inParLT   	: TLPoint;        // given 4 points (parallelogram)
  inParRT   	: TLPoint;
  inParRB   	: TLPoint;
  inParLB   	: TLPoint;
  var ouLB   	: TDPoint;        // result ClipRect
  var ouRT   	: TDPoint
);

//---------
// Procedures calculates the coordinates of a rectanle which is
// rotated by a given angle and inscribed in a given ortogonal rectangle
//---------
Procedure ProcGetRotatedInscribedRect
(
  inRectA		: TLPoint;		// left-bottom of a given Rectangle
  inRectB		: TLPoint;		// top-right of a given Rectangle
  inAngle   : Double;     // given rotation angle
  var ouRectLT   	: TLPoint;
  var ouRectRT   	: TLPoint;
  var ouRectRB   	: TLPoint;
  var ouRectLB   	: TLPoint;
  var ouResultCode  : Integer		// additional result code
);

//---------
{-- Glukhov HELMERT BUILD#115 18.05.00 }


implementation

{++ Glukhov HELMERT BUILD#115 18.05.00 }
//---------
uses Math, BMPImage;

const
	Eps = 0.00001;
  Eps2 = 0.0000000001;

//---------
// Procedures makes move for the given point and vector
//---------
Procedure ProcMakeMove
(
  var ioPoint   : TLPoint;
  inDx, inDy 		: LongInt
);
	begin
	  ioPoint.X:= ioPoint.X + inDx;
	  ioPoint.Y:= ioPoint.Y + inDy;
  end;

//---------
// Procedures adjusts a given angle slightly.
// It changes the given angle, if it is not in range of limits
// It also rounds the angle
//---------
Function ProcAdjustAngleSlightly
(
  inAngle     : Double;     // given angle
  inMinLimit	: Double;			// lower limit
  inMaxLimit	: Double;			// upper limit
  inChange		: Double = 0.0		// value of correction
)
: Double;
  begin
  // Check angle range
  	if inMaxLimit < inMinLimit then
    begin
      Result:= inMaxLimit;
      inMaxLimit:= inMinLimit;
      inMinLimit:= Result;
    end;
  	if inChange < 0.0  then inChange:= -inChange;
  	if inChange = 0.0  then inChange:= inMaxLimit - inMinLimit;

  // Make source rounding
    if Abs(inAngle) < Eps  then Result:= 0.0  else Result:= inAngle;

    if Result > inMaxLimit  then Result:= Result - inChange
    else begin
      if Result < inMinLimit  then Result:= Result + inChange;
    end;

  // Make result rounding
    if Abs(Result) < Eps  then Result:= 0.0;
  end;

//---------
// Procedures calculates the parameters of the affine transformation
// that transforms the given parallelogram1 to the given parallelogram2
//---------
// We assume that
//		x2 = a1*x1 + b1*y1 + c1
//		y2 = a2*x1 + b2*y1 + c2
//---------
Procedure ProcGetAffineTransformationMatrix
(
  inPar1LB   	: TLPoint;        // given parallelogram1
  inPar1RB   	: TLPoint;
  inPar1LT   	: TLPoint;
  inPar2LB   	: TLPoint;        // given parallelogram2
  inPar2RB   	: TLPoint;
  inPar2LT   	: TLPoint;
  var ouA1, ouB1   	: Double;
  var ouA2, ouB2   	: Double;
  var ouC1, ouC2   	: LongInt;
  var ouResultCode  : Integer 	// additional result code
);
  var
    dbDet, dbDetX, dbDetY			: Double;
  begin
  // Init result values as move from point inPar1LB to point inPar2LB
  	ouC1:= inPar2LB.X - inPar1LB.X;
  	ouC2:= inPar2LB.Y - inPar1LB.Y;
  	ouA1:= 1.0; ouB1:= 0.0;
  	ouA2:= 0.0; ouB2:= 1.0;
    ouResultCode:= 0;

  // To simplify the calculation make move for Parallelogram2:
  //		x2' = x2 - c1
  //		y2' = y2 - c2
  	ProcMakeMove( inPar2LB, -ouC1, -ouC2 );
  	ProcMakeMove( inPar2RB, -ouC1, -ouC2 );
  	ProcMakeMove( inPar2LT, -ouC1, -ouC2 );

  // We have a system relatively a1, b1:
  // 		a1*x1rb + b1*y1rb = x2rb
  // 		a1*x1lt + b1*y1lt = x2lt
  // and a system relatively a2, b2:
  // 		a2*x1rb + b2*y1rb = y2rb
  // 		a2*x1lt + b2*y1lt = y2lt

  // Calculate the determinant for the both systems
  	dbDet:= 1.0*inPar1RB.X*inPar1LT.Y - 1.0*inPar1RB.Y*inPar1LT.X;
    if abs(dbDet) < Eps2 then
    begin
    // Degenerative parallelogram1
    	ouResultCode:= -1;
    	Exit;
    end;

  // Calculate a1, b1
  	dbDetX:= 1.0*inPar1RB.Y*inPar2LT.X - 1.0*inPar2RB.X*inPar1LT.Y;
  	dbDetY:= 1.0*inPar1RB.X*inPar2LT.X - 1.0*inPar2RB.X*inPar1LT.X;
    ouA1:= -dbDetX / dbDet;
    ouB1:= dbDetY / dbDet;

  // Calculate a2, b2
  	dbDetX:= 1.0*inPar1RB.Y*inPar2LT.Y - 1.0*inPar2RB.Y*inPar1LT.Y;
  	dbDetY:= 1.0*inPar1RB.X*inPar2LT.Y - 1.0*inPar2RB.Y*inPar1LT.X;
    ouA2:= -dbDetX / dbDet;
    ouB2:= dbDetY / dbDet;
  end;

//---------
// Procedure calculates the parameters of the given affine transformation
// to perform it for a bitmap, using 1st rotation, X-stretch, and 2nd rotation;
// the last uniform X-Y stretch is ignored; the simple move is ignored also.
//---------
Procedure ProcGetSpecAffineTransformationData
(
  inA1, inB1	: Double;
  inA2, inB2	: Double;
  var ouR, ouK, ouA : Double
);
  var
    dbA12, dbA22		: Double;
    dbB12, dbB22		: Double;
    dbR12, dbR22		: Double;
    dbX10, dbY10		: Double;
    dbM, dbV1, dbV2 : Double;
    EpsVal          : Double;
  begin
  // Calculate the scalar product and vectors lengths for the reper image
  // (1,0) -> (inA1, inA2)
  // (0,1) -> (inB1, inB2)
    dbM:= inA1*inB1 + inA2*inB2;
    dbA12:= inA1 * inA1;  dbA22:= inA2 * inA2;
    dbB12:= inB1 * inB1;  dbB22:= inB2 * inB2;
    dbR12:= dbA12 + dbA22;
    dbR22:= dbB12 + dbB22;

  // Check the scalar product
    EpsVal:= Eps;
    if Abs(dbM) < EpsVal then
    begin
    // The image vectors are ortogonal to each other
    // So, the 1st rotation is not necessary, calculate the X-stretch ratio
      ouR:= 0.0;
      ouK:= sqrt( dbR12 / dbR22 );
    // Prepare data to calculate the 2nd angle
      dbX10:= ouK;
      dbY10:= 0.0;
    end
    else begin
    // Check the vectors lengths
      EpsVal:= 2*Eps*Abs(dbM);
      if Abs( dbR22 - dbR12 ) < EpsVal then
      begin
      // The lengths of the image vectors are equal to each other
      // So, the angle of the 1st rotation is Pi/4,
      // calculate the X-stretch ratio
        ouR:= Pi/4;
        dbV1:= (inA1-inB1)*(inA1-inB1) + (inA2-inB2)*(inA2-inB2);
        dbV2:= dbR12 + dbR22;
        ouK:= sqrt( dbV1 / dbV2 );
      // Prepare data to calculate the 2nd angle
        dbX10:= ouK/sqrt(2.0);
        dbY10:= 1.0/sqrt(2.0);
      end
      else begin
      //--------------------------
      // The 1st rotation and X-stretch provide the correct angle and
      // vectors lengths ratio for the image of the original reper.
      // To calculate them I used the following system:
      //    k*k*sqr(cosR) + sqr(sinR) = n*n*(a1*a1 + a2*a2)
      //    k*k*sqr(sinR) + sqr(cosR) = n*n*(b1*b1 + b2*b2)
      //    sinR*cosR*(1 - k*k) = n*n*(a1*b1 + a2*b2)
      // where
      //    R is a 1st rotation angle,
      //    k is an X-stretch ratio,
      //    n is a scale ratio
      //--------------------------
      // Calculate the 1st angle
        dbV1:= 2*dbM / (dbR22 - dbR12);
        dbV2:= ArcTan( dbV1 );
        ouR:= ProcAdjustAngleSlightly( dbV2, 0, Pi ) / 2;
      // Calculate the X-stretch ratio
        dbV1:= Tan(ouR);
        dbV2:= dbV1 * dbV1;
        dbV1:= dbR22 / dbR12;
        ouK:= sqrt( (dbV1*dbV2 - 1) / (dbV2 - dbV1) );
      // Prepare data to calculate the 2nd angle
        dbX10:= Cos(ouR)*ouK;
        dbY10:= Sin(ouR);
      end;
    end;

  // Calculate the 2nd rotation angle as difference between
  // the result angle and angle after the 1st transformation

  // Calculate the 1st angle
    EpsVal:= Eps*Abs(dbY10);
  	if Abs(dbX10) < EpsVal then
    begin // Pi/2
    	if dbY10 > 0  then dbV1:= Pi/2  else dbV1:= -Pi/2;
    end else  begin
    // regular angle
  		dbV1:= ArcTan( dbY10/dbX10 );
    	if dbX10 < 0  then dbV1:= dbV1 + Pi;
    end;

  // Calculate the 2nd angle
    EpsVal:= Eps*Abs(inA2);
  	if Abs(inA1) < EpsVal then
    begin // Pi/2
    	if inA2 > 0  then dbV2:= Pi/2  else dbV2:= -Pi/2;
    end else  begin
    // regular angle
  		dbV2:= ArcTan( inA2/inA1 );
    	if inA1 < 0  then dbV2:= dbV2 + Pi;
    end;

    ouA:= dbV2 - dbV1;
    ouA:= ProcAdjustAngleSlightly( ouA, 0, 2*Pi );
  end;

//---------
// The procedures makes the Helmert transformation for the given
// pairs of points (target,source) and for the given source parallelogram.
// It returns the transformed parallelogram.
//---------
Procedure ProcGetHelmertTransformedRect
(
	inPoints 	: Pointer;		// pairs of points defining the transformation
  inPtCnt 	: Word;       // number of pairs
  inParLT   	: TLPoint;        // given parallelogram
  inParRT   	: TLPoint;
  inParRB   	: TLPoint;
  inParLB   	: TLPoint;
  var ouParLT   	: TLPoint;    // result parallelogram
  var ouParRT   	: TLPoint;
  var ouParRB   	: TLPoint;
  var ouParLB   	: TLPoint;
  var ouResultCode  : Integer 	// additional result code
);
  var
  	Pts          	: PTLPoints;
    liDx, liDy   	: LongInt;
    Pnt1, Pnt2		: TDPoint;
    Ang1, Ang2   	: Double;
    dbK1, dbK2		: Double;

    i, i1, i2 : Integer;
    e1e1, e1e3, e1e4  : Double;
    e1e5      : Double;
    e2e5, e3e5, e4e5  : Double;

    dbA, dbB, dbE1    : Double;
    dbC, dbD, dbE2    : Double;
    dbDet, dbDet1, dbDet2	  : Double;

	begin
  // Init result values
  	ouParLT.X:= inParLT.X;
  	ouParLT.Y:= inParLT.Y;
  	ouParRT.X:= inParRT.X;
  	ouParRT.Y:= inParRT.Y;
  	ouParRB.X:= inParRB.X;
  	ouParRB.Y:= inParRB.Y;
  	ouParLB.X:= inParLB.X;
  	ouParLB.Y:= inParLB.Y;
    ouResultCode:= 0;
    Pts := PTLPoints(inPoints);

    if inPtCnt <= 0 then Exit;
    case inPtCnt of
    1:
    // 1 pair of points defines a linear move
      begin
        liDx:= Pts^[0].X - Pts^[1].X;
        liDy:= Pts^[0].Y - Pts^[1].Y;
  			ProcMakeMove( ouParLT, liDx, liDy );
  			ProcMakeMove( ouParRT, liDx, liDy );
  			ProcMakeMove( ouParRB, liDx, liDy );
  			ProcMakeMove( ouParLB, liDx, liDy );
        ouResultCode:= 1;      // data for one pair
        exit;
      end;
  	2:
  	// 2 pairs defines the transformation exactly
      begin
      // Let's assume that in this case:
      // Pts[1] moves to Pts[0],
      // Pts[3] rotates to put on line Pts[0]-Pts[2],
      // The factor of compression is calculated to make the lines equal to each other,

      // Make a linear move
        liDx:= Pts^[0].X - Pts^[1].X;
        liDy:= Pts^[0].Y - Pts^[1].Y;
				ProcMakeMove( ouParLT, liDx, liDy );
				ProcMakeMove( ouParRT, liDx, liDy );
				ProcMakeMove( ouParRB, liDx, liDy );
				ProcMakeMove( ouParLB, liDx, liDy );
      // calculate the vectors' angles for given pairs of points
      	Pnt1.Init( 0, 0 );
        liDx:= Pts^[2].X - Pts^[0].X;
        liDy:= Pts^[2].Y - Pts^[0].Y;
      	Pnt2.Init( liDx, liDy );
      // check the vector for zero length
        if (Pnt2.X = 0) AND (Pnt2.Y = 0) then
        begin
      	ouResultCode:= -1;		// degenerated target vector
        	exit;
        end;
        Ang2:= Pnt1.CalculateAngle( Pnt2 );
	    	Pnt2.Init( Pts^[3].X - Pts^[1].X, Pts^[3].Y - Pts^[1].Y );
	    // check the vector for zero length
	      if (Pnt2.X = 0) AND (Pnt2.Y = 0) then
	      begin
        	ouResultCode:= -2;		// degenerated source vector
        	exit;
        end;
        Ang1:= Pnt1.CalculateAngle( Pnt2 );
      // the rotation angle is the angle between the vectors
        Ang1:= Ang2 - Ang1;

      // Make a rotation of Pts[3] around Pts[0]
      Pnt2.Rotate( Ang1 );
      // Calculate the factor of a compression/stretching
        if Abs(liDx) < Abs(liDy)
        then dbK2:= Pnt2.Y / liDy  else dbK2:= Pnt2.X / liDX;
	      if dbK2 < Eps  then dbK1:= 10.0  else dbK1:= 1.0 / dbK2;

	    // Make a rotation around Pts[0] and a compression/stretching
	    // LeftTop
      	Pnt1.Init( ouParLT.X - Pts^[0].X, ouParLT.Y - Pts^[0].Y );
        Pnt1.Rotate( Ang1 );
        ouParLT.X:= Pts^[0].X + Round(dbK1*Pnt1.X);
        ouParLT.Y:= Pts^[0].Y + Round(dbK1*Pnt1.Y);
      // RightBottom
      	Pnt1.Init( ouParRB.X - Pts^[0].X, ouParRB.Y - Pts^[0].Y );
        Pnt1.Rotate( Ang1 );
        ouParRB.X:= Pts^[0].X + Round(dbK1*Pnt1.X);
      ouParRB.Y:= Pts^[0].Y + Round(dbK1*Pnt1.Y);
      // LeftBottom
      	Pnt1.Init( ouParLB.X - Pts^[0].X, ouParLB.Y - Pts^[0].Y );
        Pnt1.Rotate( Ang1 );
	      ouParLB.X:= Pts^[0].X + Round(dbK1*Pnt1.X);
	      ouParLB.Y:= Pts^[0].Y + Round(dbK1*Pnt1.Y);
	    // RightTop
	      ouParRT.X:= ouParLT.X + ouParRB.X - ouParLB.X;
        ouParRT.Y:= ouParLT.Y + ouParRB.Y - ouParLB.Y;
        ouResultCode:= 2;      // result for 2 pairs
        exit;
      end;
    else
  	// We have more than 2 pairs
    // We will use a method of the least squares to solve the problem
      begin
      // Any Helmert transformation is defined by the following equations set:
      //    x2 = a*x1 - b*y1 + c
      //    y2 = b*x1 + a*y1 + d
      // Every pair of points adds 2 conditional equations relatively a,b,c,d:
      //    x1*a + (-y1)*b + 1*c + 0*d + (-x2) = 0
      //    y1*a +    x1*b + 0*c + 1*d + (-y2) = 0
      // So, we have 2*inPtCnt conditional equations like
      //    e1k*a + e2k*b + e3k*c + e4k*d + e5k = 0
      // Let [eiej] is sum of eik*ejk for k = 1 to 2*inPtCnt
      // Obviously that in our case
      //    [e1e2] = 0, [e3e4] = 0,
      //    [e3e3] = inPtCnt, [e4e4] = inPtCnt,
      //    [e1e3] = [e2e4], [e2e3] = -[e1e4], [e1e1] = [e2e2]
      // So, to minimize the discrepancy we have to solve the equations set
      //    [e1e1]*a +      0*b +  [e1e3]*c +  [e1e4]*d + [e1e5] = 0
      //         0*a + [e1e1]*b -  [e1e4]*c +  [e1e3]*d + [e2e5] = 0
      //    [e1e3]*a - [e1e4]*b + inPtCnt*c +       0*d + [e3e5] = 0
      //    [e1e4]*a + [e1e3]*b +       0*c + inPtCnt*d + [e4e5] = 0

      // To improve a precision of calculations we will move the origin
      // to inParLB and will seek the vector (c,d) as (c',d')+Pts[0]-Pts[1]
        liDx:= Pts^[0].X - Pts^[1].X;
        liDy:= Pts^[0].Y - Pts^[1].Y;

      // Calculate the unknown factors
        e1e1:= 0; e1e3:= 0; e1e4:= 0; e1e5:= 0;
        e2e5:= 0; e3e5:= 0; e4e5:= 0;

        for i:= 0 to (inPtCnt-1) do
        begin
        // image (source) point
          Pnt1.X:= Pts^[2*i+1].X - inParLB.X;
          Pnt1.Y:= Pts^[2*i+1].Y - inParLB.Y;
        // object (target) point
          Pnt2.X:= Pts^[2*i].X - inParLB.X - liDx;
          Pnt2.Y:= Pts^[2*i].Y - inParLB.Y - liDy;
        // 1st equation
          e1e1:= e1e1 + 1.0*Pnt1.X * 1.0*Pnt1.X;
          e1e3:= e1e3 + 1.0*Pnt1.X;
          e1e5:= e1e5 - 1.0*Pnt1.X * 1.0*Pnt2.X;
          e2e5:= e2e5 + 1.0*Pnt1.Y * 1.0*Pnt2.X;
          e3e5:= e3e5 - 1.0*Pnt2.X;
        // 2nd equation
          e1e1:= e1e1 + 1.0*Pnt1.Y * 1.0*Pnt1.Y;
          e1e4:= e1e4 + 1.0*Pnt1.Y;
          e1e5:= e1e5 - 1.0*Pnt1.Y * 1.0*Pnt2.Y;
          e2e5:= e2e5 - 1.0*Pnt1.X * 1.0*Pnt2.Y;
          e4e5:= e4e5 - 1.0*Pnt2.Y;
        end;
      //    x1*a + (-y1)*b + 1*c + 0*d + (-x2) = 0
      //    y1*a +    x1*b + 0*c + 1*d + (-y2) = 0

      // Using the 3rd equation, we have
      //    c = -([e1e3]/inPtCnt)*a +([e1e4]/inPtCnt)*b - [e3e5]/inPtCnt
      // Using the 4th equation, we have
      //    d = -([e1e4]/inPtCnt)*a -([e1e3]/inPtCnt)*b - [e4e5]/inPtCnt
      // Form 2 linear equations relatively a and b:
      //    A*a + B*b + E1 = 0
      //    C*a + D*b + E2 = 0
        dbA := e1e1 - (e1e3*e1e3 + e1e4*e1e4)/inPtCnt;
//        dbB := 0;   //  e1e3*e1e4/inPtCnt - e1e4*e1e3/inPtCnt;
//        dbC := 0;   //  e1e4*e1e3/inPtCnt - e1e3*e1e4/inPtCnt;
//        dbD := dbA; // e1e1 - (e1e4*e1e4 + e1e3*e1e3)/inPtCnt;
        dbE1:= e1e5 - (e1e3*e3e5 + e1e4*e4e5)/inPtCnt;
        dbE2:= e2e5 + (e1e4*e3e5 - e1e3*e4e5)/inPtCnt;

      // Calculate the determinant
      	dbDet:= dbA*dbA;    // -dbC*dbB = 0
        if abs(dbDet) < Eps2 then
        begin
        // Degenerative data, call itself as for 2 pairs
        	ProcGetHelmertTransformedRect(
        		  inPoints, 2,
              inParLT, inParRT, inParRB, inParLB,
              ouParLT, ouParRT, ouParRB, ouParLB,
              ouResultCode );
        	Exit;
        end;

      // Calculate a and b
//      	dbDet1:= dbB*dbE2 - dbD*dbE1;  dbA:=  dbDet1 / dbDet;
//      	dbDet2:= dbA*dbE2 - dbC*dbE1;  dbB:= -dbDet2 / dbDet;
        dbB:= -dbE2 / dbA;
        dbA:= -dbE1 / dbA;

      // Calculate c and d
        dbC:= -(e1e3*dbA - e1e4*dbB + e3e5) / inPtCnt;
        dbD:= -(e1e4*dbA + e1e3*dbB + e4e5) / inPtCnt;

      // Calculate the transformed parallelogram
      // LeftTop
        Pnt1.X:= inParLT.X - inParLB.X;
        Pnt1.Y:= inParLT.Y - inParLB.Y;
        ouParLT.X:= Round( dbA*Pnt1.X - dbB*Pnt1.Y + dbC ) + inParLB.X + liDx;
        ouParLT.Y:= Round( dbB*Pnt1.X + dbA*Pnt1.Y + dbD ) + inParLB.Y + liDy;
      // RightBottom
        Pnt1.X:= inParRB.X - inParLB.X;
        Pnt1.Y:= inParRB.Y - inParLB.Y;
        ouParRB.X:= Round( dbA*Pnt1.X - dbB*Pnt1.Y + dbC ) + inParLB.X + liDx;
        ouParRB.Y:= Round( dbB*Pnt1.X + dbA*Pnt1.Y + dbD ) + inParLB.Y + liDy;
      // LeftBottom
        ouParLB.X:= Round( dbC ) + inParLB.X + liDx;
        ouParLB.Y:= Round( dbD ) + inParLB.Y + liDy;
      // RightTop
        ouParRT.X:= ouParLT.X + ouParRB.X - ouParLB.X;
        ouParRT.Y:= ouParLT.Y + ouParRB.Y - ouParLB.Y;
        ouResultCode:= 3;      // result for N pairs
        exit;
      end;
  	end;
	end;

//---------
// The procedures calculates ClipRect for the given 4 points
//---------
Procedure ProcGetClipRect
(
  inParLT   	: TLPoint;        // given 4 points (parallelogram)
  inParRT   	: TLPoint;
  inParRB   	: TLPoint;
  inParLB   	: TLPoint;
  var ouLB   	: TDPoint;        // result ClipRect
  var ouRT   	: TDPoint
);
	begin  
  	ouLB.X:= inParLT.X;
  	ouLB.Y:= inParLT.Y;
  	ouRT.X:= inParLT.X;
  	ouRT.Y:= inParLT.Y;
    if ouLB.X > inParRT.X  then ouLB.X:= inParRT.X;
    if ouLB.Y > inParRT.Y  then ouLB.Y:= inParRT.Y;
    if ouRT.X < inParRT.X  then ouRT.X:= inParRT.X;
    if ouRT.Y < inParRT.Y  then ouRT.Y:= inParRT.Y;
    if ouLB.X > inParRB.X  then ouLB.X:= inParRB.X;
    if ouLB.Y > inParRB.Y  then ouLB.Y:= inParRB.Y;
    if ouRT.X < inParRB.X  then ouRT.X:= inParRB.X;
    if ouRT.Y < inParRB.Y  then ouRT.Y:= inParRB.Y;
    if ouLB.X > inParLB.X  then ouLB.X:= inParLB.X;
    if ouLB.Y > inParLB.Y  then ouLB.Y:= inParLB.Y;
    if ouRT.X < inParLB.X  then ouRT.X:= inParLB.X;
    if ouRT.Y < inParLB.Y  then ouRT.Y:= inParLB.Y;
  end;

//---------
// Procedures calculates the coordinates of a rectanle which is
// rotated by a given angle and inscribed in a given ortogonal rectangle
//---------
Procedure ProcGetRotatedInscribedRect
(
  inRectA		: TLPoint;		// left-bottom of a given Rectangle
  inRectB		: TLPoint;		// top-right of a given Rectangle
  inAngle   : Double;     // given rotation angle
  var ouRectLT   	: TLPoint;
  var ouRectRT   	: TLPoint;
  var ouRectRB   	: TLPoint;
  var ouRectLB   	: TLPoint;
  var ouResultCode  : Integer		// additional result code
);
//---------
// Let's assume that that the angle is inside of (-3pi;3pi)
// In case of error it returns the source rectangle.
//---------
  var
  	liSrcLx       : LongInt;
  	liSrcLy       : LongInt;
  	liSrcLc       : LongInt;
  	fbAddPi       : Boolean;
  	fbNegative    : Boolean;
  	liCheck       : LongInt;
  	liDx, liDy    : LongInt;
  	dbDx, dbDy    : Double;
    dbVal					: Double;
  label
  	LRotate;
  begin
  // Init result values
  	ouResultCode:= 0;
    ouRectLT.X:= inRectA.X;
    ouRectLT.Y:= inRectB.Y;
    ouRectRT.X:= inRectB.X;
    ouRectRT.Y:= inRectB.Y;
    ouRectRB.X:= inRectB.X;
    ouRectRB.Y:= inRectA.Y;
    ouRectLB.X:= inRectA.X;
    ouRectLB.Y:= inRectA.Y;

  // Prepare the source data
  	inAngle:= ProcAdjustAngleSlightly( inAngle, -Pi, Pi );
    liSrcLx:= inRectB.X - inRectA.X;
    liSrcLy:= inRectB.Y - inRectA.Y;

	// Prepare the control data
  	fbAddPi:= False;
    fbNegative:= False;
  	if Abs(inAngle) > Pi/2 then
    begin
	  	fbAddPi:= True;
	  	inAngle:= ProcAdjustAngleSlightly( inAngle, -Pi/2, Pi/2 );
      inAngle:= -inAngle;
    end;
    if inAngle < 0 then
    begin
    	fbNegative:= True;
      inAngle:= -inAngle;
    end;
    if liSrcLx > liSrcLy  then liSrcLc:= liSrcLy  else liSrcLc:= liSrcLx;
    liSrcLc:= liSrcLc div 2;

  // Make angle 0 checking
    liCheck:= Round( liSrcLc * Sin(inAngle) );
    if liCheck = 0 then
    begin
   // Angle 0
    	if fbAddPi then begin
	    // Angles -Pi/Pi
		    ouRectLT.X:= inRectB.X;
		    ouRectLT.Y:= inRectB.Y;
    		ouRectRT.X:= inRectB.X;
		    ouRectRT.Y:= inRectA.Y;
    		ouRectRB.X:= inRectA.X;
		    ouRectRB.Y:= inRectA.Y;
		    ouRectLB.X:= inRectA.X;
    		ouRectLB.Y:= inRectB.Y;
      end;
      ouResultCode:= 1;
      Exit;
    end;

  // Make Pi/2 checking
    liCheck:= Round( liSrcLc * Cos(inAngle) );
    if liCheck = 0 then
    begin
    	if fbAddPi = fbNegative then begin
	    // Angle Pi/2
		    ouRectLT.X:= inRectA.X;
	  	  ouRectLT.Y:= inRectA.Y;
  			ouRectRT.X:= inRectA.X;
		    ouRectRT.Y:= inRectB.Y;
  			ouRectRB.X:= inRectB.X;
	  	  ouRectRB.Y:= inRectB.Y;
	    	ouRectLB.X:= inRectB.X;
	  		ouRectLB.Y:= inRectA.Y;
      end else  begin
	    // Angle -Pi/2
		    ouRectLT.X:= inRectB.X;
		    ouRectLT.Y:= inRectB.Y;
    		ouRectRT.X:= inRectB.X;
		    ouRectRT.Y:= inRectA.Y;
    		ouRectRB.X:= inRectA.X;
		    ouRectRB.Y:= inRectA.Y;
		    ouRectLB.X:= inRectA.X;
    		ouRectLB.Y:= inRectB.Y;
      end;
      ouResultCode:= 1;
      Exit;
    end;

  // Make Pi/4 checking
    liCheck:= Round( liSrcLc * Tan(inAngle) );
    if liCheck = liSrcLc then
    begin
    // Angle Pi/4
    	liDx:= liSrcLx Div 2;
    	liDy:= liSrcLy Div 2;
      goto LRotate;
    end;

  // Calculate the offsets for a regular angle from 0 to Pi/2
  	dbVal:= Tan( inAngle );
  // dy = (lx * tg(a) - ly) / (tg2(a) - 1)
  	dbDy:= (liSrcLx * dbVal - liSrcLy) / (dbVal * dbVal - 1);
    dbDx:= dbDy * dbVal;
  	liDy:= Round( dbDy );
    liDx:= Round( dbDx );

	LRotate:
  // Set verteces coordinates for the calculated offsets
	  ouRectLT.X:= inRectA.X;
    ouRectLT.Y:= inRectA.Y + liDy;
		ouRectRT.X:= inRectB.X - liDx;
    ouRectRT.Y:= inRectB.Y;
		ouRectRB.X:= inRectB.X;
    ouRectRB.Y:= inRectB.Y - liDy;
    ouRectLB.X:= inRectB.X + liDx;
		ouRectLB.Y:= inRectA.Y;

  // Make changes for negative angle
    if fbNegative then
    // use axial symmetry
    begin
		  ouRectLT.X:= ouRectLB.X;
			ouRectRB.X:= ouRectRT.X;
      ouRectLB.X:= inRectA.X;
      ouRectRT.X:= inRectB.X;
  	  ouRectRT.Y:= ouRectLT.Y;
			ouRectLB.Y:= ouRectRB.Y;
	    ouRectLT.Y:= inRectB.Y;
	    ouRectRB.Y:= inRectA.Y;
    end;

  // Make changes for large angles
    if fbAddPi then
    // use central symmetry
    begin
    	liCheck:= ouRectLT.X;     // Change x for LT - RB
      ouRectLT.X:= ouRectRB.X;
      ouRectRB.X:= liCheck;
    	liCheck:= ouRectLT.Y;     // Change y for LT - RB
      ouRectLT.Y:= ouRectRB.Y;
      ouRectRB.Y:= liCheck;
    	liCheck:= ouRectLB.X;     // Change x for LB - RT
      ouRectLB.X:= ouRectRT.X;
      ouRectRT.X:= liCheck;
    	liCheck:= ouRectLB.Y;			// Change y for LB - RT
      ouRectLB.Y:= ouRectRT.Y;
      ouRectRT.Y:= liCheck;
    end;

  	ouResultCode:= 1;
  end;

//---------
{-- Glukhov HELMERT BUILD#115 18.05.00 }

//==============================

end.

