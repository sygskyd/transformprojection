{****************************************************************************}
{ Unit AM_Const                                                              }
{----------------------------------------------------------------------------}
{ - Routinen zur Fl�chenteilung.                                             }
{----------------------------------------------------------------------------}
{ Modifikationen                                                             }
{  21.09.1996 Heinz Ofner                                                    }
{****************************************************************************}
Unit AM_Const;

Interface

Uses Classes,Messages,Objects,AM_Layer,Windows,AM_Stddl,AM_Admin,AM_Def,AM_Index,
     {$IFNDEF WMLT}
     AM_DDE,
     {$ENDIF}
     AM_Poly,AM_CPoly,AM_Point,ResDlg,ProjStyle;

Const id_Layer1    = 100;
      id_Layer2    = 101;
      id_WholeArea = 102;
      id_PartArea  = 103;

      id_DestLayer = 100;
      id_SourLayer = 101;
      id_DestArea  = 102;
      id_AreaName  = 103;

      id_MovePoint = 120;
      id_InsPoint  = 121;
      id_DelPoint  = 122;
      id_Parallel  = 123;
      id_Buffer    = 124;
      id_Island    = 125;
      id_ConstrOK  = 126;
      id_WArea     = 127;
      id_RArea     = 128;
      id_ParArea   = 129;
      id_DoConstr  = 130;
      id_CArea     = 131;
      id_ConsRetry = 132;
      id_GetArea   = 133;
      id_CutArea   = 134;
      id_Retour    = 135;
      id_ActArea   = 136;
      id_CInfo     = 137;
      id_BufferDim = 138;
      id_DrawLine  = 139;
      id_SelLine   = 140;

      id_m2_1      = 180;
      id_a_1       = 181;
      id_ha_1      = 182;
      id_km2_1     = 183;
      id_m2_2      = 190;
      id_a_2       = 191;
      id_ha_2      = 192;
      id_km2_2     = 193;
      id_m2_3      = 200;
      id_a_3       = 201;
      id_ha_3      = 202;
      id_km2_3     = 203;

      ConstructGS  = 0;
      ConstructFS  = 1;
      ConstructS   = 2;

      HL1          = 'Hilfslayer 001';
      HL2          = 'Hilfslayer 002';
      HL3          = 'Hilfslayer 003';
      HL4          = 'Hilfslayer 004';
      HL5          = 'Hilfslayer 005';
      HL6          = 'Hilfslayer 006';
      HL7          = 'Hilfslayer 007';
      CL1          = 'Konstruktionslayer 001';

Type PConstrData   = ^TConstrData;
     TConstrData   = Record
                       SLay            : PLayer;      {Source}
                       DLay            : PLayer;      {Destination}
                     end;

     TCParAreaDlg  = Class(TGrayDialog)
                      Public
                       AreaDat     : PChar;
                       RAreaDat    : PChar;
                       MaxVal      : Double;
                       GoRetour    : PBool;
                       DoAddOrDel  : PByte;
                       InfoStr     : Array[0..255] of Char;
                       Constructor Init(AParent:TComponent;AreaData,RAreaData:PChar;
                                        AAddOrDel:PByte;ARetour:PBool;Info:String);
                       Function    CanClose:Boolean; override;
                       Procedure   SetupWindow; override;
                       Procedure   CParReturn(var Msg : TMessage); message id_First + id_Retour;
                     end;

     TCBufAreaDlg  = Class(TGrayDialog)
                      Public
                       AreaDat     : PChar;
                       RAreaDat    : PChar;
                       GoRetour    : PBool;
                       DoAddOrDel  : PByte;
                       DrawOrSel   : PByte;
                       InfoStr     : Array[0..255] of Char;
                       Constructor Init(AParent:TComponent;AreaData,RAreaData:PChar;
                                        AAddOrDel,ADrawOrSel:PByte;ARetour:PBool;Info:String);
                       Function    CanClose:Boolean; override;
                       Procedure   SetupWindow; override;
                       Procedure   CBufReturn(var Msg : TMessage); message id_First + id_Retour;
                     end;

     TCAreaDlg     = Class(TGrayDialog)
                      Public
                       WArea       : Double;
                       RArea       : Double;
                       CArea       : Double;
                       InfoStr     : Array[0..255] of Char;
                       Constructor Init(AParent:TComponent;A1Area,A2Area,A3Area:Double;Info:String);
                       Function    CanClose:Boolean; override;
                       Procedure   SetupWindow; override;
                       Procedure   ConsRetryClick(var Msg : TMessage); message id_First + id_ConsRetry;
                     end;

     TConstructDlg = Class(TGrayDialog)
                      Public
                       BProj       : Pointer;
                       BType       : PByte;
                       WArea       : Double;
                       RArea       : Double;
                       InfoStr     : Array[0..255] of Char;
                       Constructor Init(AParent:TComponent;AProj:Pointer;AType:PByte;A1Area,A2Area:Double;Info:String);
                       Function    CanClose:Boolean; override;
                       Procedure   SetupWindow; override;
                       Procedure   DoConstr(var Msg : TMessage); message id_First + id_DoConstr;
                     end;

     TAreaPartDlg  = Class(TLayerDlg)
                      Public
                       SAreaName   : PChar;
                       DAreaName   : PChar;
                       AreaSize    : PChar;
                       AreaName    : PChar;
                       Constructor Init(AParent:TComponent;ALayers:PLayers;SAreaNam,DAreaNam,AreaNum,AreaNam:PChar;
                                        ProjectStyles:TProjectStyles);
                       Function    CanClose:Boolean; override;
                       Procedure   SetupWindow; override;
                     end;

     PConstruct    = ^TConstruct;
     TConstruct    = Object(TOldObject)
                       GSLayer         : PLayer;
                       FSLayer         : PLayer;
                       SLayer          : PLayer;
                       HelpLayer1      : PLayer;
                       HelpLayer2      : PLayer;
                       HelpLayer3      : PLayer;
                       HelpLayer4      : PLayer;
                       HelpLayer5      : PLayer;
                       HelpLayer6      : PLayer;
                       HelpLayer7      : PLayer;
                       ConstructType   : Byte;
                       DDEData         : PChar;
                       AreaName        : Array[0..255] of Char;
                       WantedArea      : Double;
                       RealArea        : Double;
                       WantedParArea   : Double;
                       Alpha           : Double;
                       SidePoint       : TDPoint;
                       BufferSize      : Real;
                       BufferLine      : PPoly;
                       BufferDir       : LongInt;
                       Neigh1          : PPoly;
                       Neigh2          : PPoly;
                       CutPoint1       : Integer;
                       CutPoint2       : Integer;
                       CutPoint3       : Integer;
                       ConstructRetry  : Boolean;
                       AddDel          : Byte;
                       DrawSel         : Byte;
                       CutAreaS        : Pointer;
                       CutLineS        : Pointer;
                       Data            : PConstrData;
                       DestiLayer      : PLayer;
                       HelpLayer       : PLayer;
                       ConstructLayer  : PLayer;
                       ResultLayer     : PLayer;
                       ErrorLayer      : PLayer;
                       SourceAreas     : PCollection;
                       Beta1           : Double;
                       ShowDlgs        : Boolean;
                       InAction        : Boolean;
                       SendUPD         : Boolean;
                       OldGSL          : PLayer;
                       OldFSL          : PLayer;
                       GraphicMode     : Boolean;
                       RealTopLayer    : PLayer;
                       RealTranspMode  : Boolean;
                       Constructor Init(AParent:TComponent;AData:Pointer);
                       Destructor  Done; virtual;
                       Procedure   BreakConstructDB(AData:Pointer);
                       Procedure   ChangeAreaLayers(AData:Pointer);
                       Procedure   CopyResult(AData:Pointer);
                       Procedure   CopyToHelpLayer(AData:Pointer);
                       Procedure   CopyToHelpLayers(AData:Pointer;SourceLayer,DestLayer:PLayer);
                       Procedure   CreateSingleArea(AData:Pointer;SourceLayer,DestLayer:PLayer);
                       Procedure   DeleteHelpLayers(AData:Pointer);
                       Procedure   DeleteHelpLayers1(AData:Pointer);
                       Procedure   DeleteHelpLayers2(AData:Pointer);
                       Procedure   DeleteSaveLayers(AData:Pointer);
                       Procedure   DoBuffer(AData:Pointer);
                       Function    DoConstructDB(AData:Pointer;AType:Byte):Boolean;
                       Procedure   DoConstructGr(AData:Pointer;AType:Byte);
                       Procedure   DoInsertIsland(AData:Pointer);
                       Procedure   DoInParallel(AData:Pointer);
                       Procedure   DoOutParallel(AData:Pointer);
                       Procedure   DoParallel(AData:Pointer;CutArea:PCPoly;CutLine:PPoly;
                                              InPoint,OutPoint:TDPoint;XInDir,YInDir:Integer);
                       Function    EndConstructDB(AData:Pointer):Boolean;
                       Procedure   MakeHelpLayers(AData:Pointer);
                       Procedure   MakeHelpLayers1(AData:Pointer);
                       Procedure   MakeHelpLayers2(AData:Pointer);
                       Procedure   MakeSaveLayers(AData:Pointer);
                       Function    MoveLineParallel(APoly:PPoly;BeginPoint,EndPoint:TDPoint;APercent:Real):LongInt;
                       Procedure   PartArea(AData:Pointer;AChild:Pointer);
                       Procedure   ResetProjStatus(AData:Pointer);
                       Procedure   SearchFreeArea(AData:Pointer);
                       Procedure   SetOldTop(AData:Pointer);
                       Function    ShowConstrDlg(AData:Pointer):Boolean;
                     end;

Implementation

Uses AM_Main,AM_Ini,AM_Cut,AM_View,AM_Ar
     {$IFNDEF WMLT}
     ,AM_Exp
     {$ENDIF}
     ,AM_Proj,AM_Combi,AM_Obj,AM_Offse,
     AM_ProjO,AM_ProjP,AM_Child,SysUtils,UserIntf,Controls,DDEDef;

Constructor TConstruct.Init
   (
   AParent         : TComponent;
   AData           : Pointer
   );
  begin
    Inherited Init;
    GSLayer:=PProj(AData)^.Layers^.NameToLayer(IniFile^.GSLayer);
    FSLayer:=PProj(AData)^.Layers^.NameToLayer(IniFile^.FSLayer);
    SLayer:=PProj(AData)^.Layers^.NameToLayer(IniFile^.SLayer);
    HelpLayer1:=NIL;
    HelpLayer2:=NIL;
    ConstructType:=0;
    DDEData:=NIL;
    ConstructRetry:=FALSE;
    ShowDlgs:=TRUE;
    InAction:=FALSE;
    SendUPD:=TRUE;
    GraphicMode:=FALSE;
  end;

Destructor TConstruct.Done;
  begin
    GSLayer:=NIL;
    FSLayer:=NIL;
    SLayer:=NIL;
    HelpLayer1:=NIL;
    HelpLayer2:=NIL;
    StrDispose(DDEData);
    Inherited Done;
  end;

Procedure TConstruct.ChangeAreaLayers
   (
   AData           : Pointer
   );
{$IFNDEF WMLT}
  var DDELine      : Array[0..1000] of Char;
      TempPointer  : PChar;
      LayerName    : String;
      AreaLayer    : PLayer;
      DDEStrEnd    : Boolean;
      OldPos       : Integer;
  Procedure ReadDDELine
     (
     Item          : PChar
     ); Far;
    begin
      if StrLComp(Item,'[JAH]',5) = 0 then begin
        StrCopy(DDELine,Item);
        Item[0]:=DDEStrUsed;
      end;
    end;
  Procedure SearchLayer
     (
     Item          : PLayer
     ); Far;
    begin
      if Item^.Text <> NIL then
        if Item^.Text^ = LayerName then AreaLayer:=Item;
    end;
{$ENDIF}
  begin
{$IFNDEF WMLT}
    {$IFNDEF AXDLL} // <----------------- AXDLL
    DDEHandler.DDEData^.ForEach(@ReadDDELine);
    TempPointer:=NextDDEPart(NextDDEPart(DDELine));
    DDEStrEnd:=ReadDDEString(TempPointer,']',LayerName);        { Grundst�cklayername }
    {$ENDIF} // <----------------- AXDLL
    AreaLayer:=NIL;
    PProj(AData)^.Layers^.LData^.ForEach(@SearchLayer);
    if AreaLayer = NIL then begin
      PProj(AData)^.Layers^.InsertLayer(LayerName,0,0,0,0,0,DefaultSymbolFill);
//!?!?      ProcSetManSnapOrder(PProj(AData));
    end
    else begin
      PProj(AData)^.DeselectAll(TRUE);
      OldPos:=PProj(AData)^.Layers^.LData^.IndexOf(AreaLayer);
      if OldPos <> 1 then begin
        PProj(AData)^.Layers^.LData^.AtDelete(OldPos);
        PProj(AData)^.Layers^.NumberLayers;
        PProj(AData)^.Layers^.DetermineTopLayer;
        PProj(AData)^.Layers^.LData^.AtInsert(PProj(AData)^.Layers^.LData^.IndexOf(PProj(AData)^.Layers^.TopLayer),AreaLayer);
        PProj(AData)^.Layers^.NumberLayers;
        PProj(AData)^.Layers^.DetermineTopLayer;
      end;
    end;
    GSLayer:=PProj(AData)^.Layers^.NameToLayer(LayerName);
    IniFile^.GSLayer:=LayerName;
    DDEStrEnd:=ReadDDEString(TempPointer,']',LayerName);        { Feldst�cklayername }
    AreaLayer:=NIL;
    PProj(AData)^.Layers^.LData^.ForEach(@SearchLayer);
    if AreaLayer = NIL then begin
      PProj(AData)^.Layers^.InsertLayer(LayerName,0,0,0,0,0,DefaultSymbolFill);
//!?!?!?      ProcSetManSnapOrder(PProj(AData));
    end
    else begin
      PProj(AData)^.DeselectAll(TRUE);
      OldPos:=PProj(AData)^.Layers^.LData^.IndexOf(AreaLayer);
      if OldPos <> 1 then begin
        PProj(AData)^.Layers^.LData^.AtDelete(OldPos);
        PProj(AData)^.Layers^.NumberLayers;
        PProj(AData)^.Layers^.DetermineTopLayer;
        PProj(AData)^.Layers^.LData^.AtInsert(PProj(AData)^.Layers^.LData^.IndexOf(PProj(AData)^.Layers^.TopLayer),AreaLayer);
        PProj(AData)^.Layers^.NumberLayers;
        PProj(AData)^.Layers^.DetermineTopLayer;
      end;
    end;
    FSLayer:=PProj(AData)^.Layers^.NameToLayer(LayerName);
    IniFile^.FSLayer:=LayerName;
    DDEStrEnd:=ReadDDEString(TempPointer,']',LayerName);        { Schlaglayername }
    AreaLayer:=NIL;
    PProj(AData)^.Layers^.LData^.ForEach(@SearchLayer);
    if AreaLayer = NIL then begin
      PProj(AData)^.Layers^.InsertLayer(LayerName,0,0,0,0,0,DefaultSymbolFill);
//!?!?!?      ProcSetManSnapOrder(PProj(AData));
    end
    else begin
      PProj(AData)^.DeselectAll(TRUE);
      OldPos:=PProj(AData)^.Layers^.LData^.IndexOf(AreaLayer);
      if OldPos <> 1 then begin
        PProj(AData)^.Layers^.LData^.AtDelete(OldPos);
        PProj(AData)^.Layers^.NumberLayers;
        PProj(AData)^.Layers^.DetermineTopLayer;
        PProj(AData)^.Layers^.LData^.AtInsert(PProj(AData)^.Layers^.LData^.IndexOf(PProj(AData)^.Layers^.TopLayer),AreaLayer);
        PProj(AData)^.Layers^.NumberLayers;
        PProj(AData)^.Layers^.DetermineTopLayer;
      end;
    end;
    SLayer:=PProj(AData)^.Layers^.NameToLayer(LayerName);
    IniFile^.SLayer:=LayerName;
    IniFile^.WriteProjectLayerNames;
    {$IFNDEF AXDLL} // <----------------- AXDLL
    DDEHandler.DeleteDDEData;
    {$ENDIF} // <----------------- AXDLL
    PProj(AData)^.Layers^.DetermineTopLayer;
{$ENDIF}
  end;

Procedure TConstruct.DoConstructGr
   (
   AData           : Pointer;
   AType           : Byte
   );
  begin
    ConstructType:=AType;
    MakeHelpLayers(AData);
    CopyToHelpLayer(AData);
    SearchFreeArea(AData);
    DeleteHelpLayers(AData);
  end;

Function TConstruct.DoConstructDB
   (
   AData           : Pointer;
   AType           : Byte
   )
   : Boolean;
  var TmpStr       : PChar;
      TempStr      : Array[0..50] of Char;
      Params       : Integer;
      Error        : Integer;
      AreaID       : LongInt;
      OldTop       : PLayer;
      SItem        : PIndex;
      AreaFound    : Boolean;
  Function SelArea
     (
     ID            : LongInt
     )
     : Boolean;
    var ExistObject: PIndex;
    begin
      SItem^.Index:=ID;
      ExistObject:=PProj(AData)^.Layers^.TopLayer^.HasObject(SItem);
      if ExistObject <> NIL then begin
        PProj(AData)^.Layers^.TopLayer^.Select(PProj(AData)^.PInfo,ExistObject);
        Result:=TRUE;
      end
      else Result:=FALSE;
    end;
  Procedure CalcArea
     (
     Item          : PIndex
     ); Far;
    var AObj       : PView;
    begin
      if Item^.GetObjType = ot_CPoly then begin
        AObj:=ConstructLayer^.IndexObject(PProj(AData)^.PInfo,Item);
        RealArea:=PCPoly(AObj)^.Flaeche;
      end;
    end;
  Procedure CleanLayer
     (
     ALayer        : PLayer
     );
    Procedure DelObject                                                                                    {Objekte l�schen}
       (
       Item          : PIndex
       );
      begin
        ALayer^.DeleteIndex(PProj(AData)^.PInfo,Item);
      end;
    begin
      while ALayer^.Data^.GetCount > 0 do DelObject(ALayer^.Data^.At(0));
    end;
  Procedure SelObjectsOnLayer
     (
     ALayer        : PLayer
     );
    Procedure SelObject                                                                               {Objekte selektieren}
       (
       Item          : PIndex
       ); Far;
      begin
        ALayer^.Select(PProj(AData)^.PInfo,Item);
      end;
    begin
      ALayer^.Data^.ForEach(@SelObject);
    end;
  begin
    RealTopLayer:=PProj(AData)^.Layers^.TopLayer;
    RealTranspMode:=PProj(AData)^.Layers^.TranspLayer;
    PProj(AData)^.DeSelectAll(FALSE);
    AreaFound:=FALSE;
    ConstructType:=AType;
    TmpStr:=NextDDEPart(NextDDEPart(DDEData));
    ReadDDEText(TmpStr,']',TempStr);            {Anzahl der Parameter}
    Val(TempStr,Params,Error);
    ReadDDEText(TmpStr,']',AreaName);           {Name der Fl�che}
    Dec(Params);
    ReadDDEText(TmpStr,']',TempStr);            {Fl�cheninhalt}
    Val(TempStr,WantedArea,Error);
    Dec(Params);
    OldTop:=PProj(AData)^.Layers^.TopLayer;
    if ConstructType = ConstructFS then PProj(AData)^.Layers^.TopLayer:=GSLayer
    else if ConstructType = ConstructS then PProj(AData)^.Layers^.TopLayer:=FSLayer;
    SItem:=New(PIndex,Init(0));
    while Params > 1 do begin
      ReadDDEText(TmpStr,']',TempStr);          {IDs der Einzelfl�chen}
      Val(TempStr,AreaID,Error);
      if Error = 0 then if SelArea(AreaID) then AreaFound:=TRUE;
      Dec(Params);
    end;
    Dispose(SItem,Done);
    PProj(AData)^.Layers^.TopLayer:=OldTop;
    ReadDDEText(TmpStr,']',TempStr);            {Dialoge erw�nscht}
    if StrComp(TempStr,'0') = 0 then ShowDlgs:=FALSE
    else ShowDlgs:=TRUE;
    if AreaFound then begin
      Result:=TRUE;
      {$IFNDEF WMLT}
      ProcShowAllSel(AData);
      {$ENDIF}
      MakeHelpLayers1(AData);
      CopyToHelpLayer(AData);
      PProj(AData)^.DeselectAll(FALSE);
      MyObject:=TAreaObject.Create(NIL,AData);
      MyObject.DoAreaWithoutDlg(HL1,HL2,ar_Envelope);               {Umh�llende aller beteiligten Fl�chen und Inselfl�chen}
      MyObject.Free;
      CleanLayer(HelpLayer1);                                                                           {HelpLayer1 leeren}
      ProcCreateIslandAreaWithoutDlg(PProj(AData),HelpLayer2,HelpLayer1);                        {Inselfl�chen einbeziehen}
      if ConstructType = ConstructFS then AreaCut5(AData,HL1,FSLayer^.Text^,CL1)
      else if ConstructType = ConstructS then AreaCut5(AData,HL1,SLayer^.Text^,CL1);
      RealArea:=0;
      ConstructLayer^.Data^.ForEach(@CalcArea);
    end
    else begin
      UserInterface.Update([uiMenus]);
      Result:=FALSE;
    end;
  end;

Function TConstruct.ShowConstrDlg
   (
   AData           : Pointer
   ): Boolean;
  var Dialog       : TConstructDlg;
      ConstrType   : Byte;
      FirstPoint   : TDPoint;
      DialogPar    : TCParAreaDlg;
      DialogBuff   : TCBufAreaDlg;
      StrArea      : Array[0..30] of Char;
      StrRArea     : Array[0..30] of Char;
      ParReturn    : Bool;
      Error        : Integer;
      InfoStr      : String;
      Posi         : Integer;
  Procedure DoAll
     (
     Item          : PIndex
     ); Far;
    var ConPoly    : PCPoly;
    begin
      if Item^.GetObjType = ot_CPoly then begin
        ConPoly:=PCPoly(ConstructLayer^.IndexObject(PProj(AData)^.PInfo,Item));
        FirstPoint.Init(PDPoint(ConPoly^.Data^.At(0))^.X,PDPoint(ConPoly^.Data^.At(0))^.Y);
      end;
    end;
  Procedure CalcArea
     (
     Item          : PIndex
     ); Far;
    var AObj       : PView;
    begin
      if Item^.GetObjType = ot_CPoly then begin
        AObj:=ConstructLayer^.IndexObject(PProj(AData)^.PInfo,Item);
        RealArea:=RealArea+PCPoly(AObj)^.Flaeche;
      end;
    end;
  begin
    {$IFNDEF WMLT}
    SendUPD:=TRUE;
    if (PProj(AData)^.ActualMen = mn_CParallel) or (PProj(AData)^.ActualMen = mn_CParallelS)
      or (PProj(AData)^.ActualMen = mn_CBufferDraw) or (PProj(AData)^.ActualMen = mn_CBufferSel)
      or (PProj(AData)^.ActualMen = mn_CIslands) then DeleteHelpLayers2(AData);                               {����������}
    PProj(AData)^.PInfo^.RedrawScreen(TRUE);
    ConstrType:=0;
    RealArea:=0;
    ConstructLayer^.Data^.ForEach(@CalcArea);
    if RealArea = 0 then begin
      if ConstructType = ConstructFS then MsgBox(PProj(AData)^.Parent.Handle,11358,mb_OK or mb_IconExclamation,'')
      else if ConstructType = ConstructS then MsgBox(PProj(AData)^.Parent.Handle,11378,mb_OK or mb_IconExclamation,'');
      Result:=FALSE;
    end
    else begin
      if ConstructType = ConstructFS then InfoStr:=GetLangText(11388)+#0
      else if ConstructType = ConstructS then InfoStr:=GetLangText(11389)+#0;
      Posi:=Pos('%',InfoStr);
      if Posi<>0 then begin
        Delete(InfoStr,Posi,1);
        Insert(StrPas(AreaName),InfoStr,Posi);
      end;
      Dialog:=TConstructDlg.Init(PProj(AData)^.Parent,AData,@ConstrType,WantedArea,RealArea,InfoStr);
      if ExecDialog(Dialog)=id_OK then begin
        Result:=TRUE;
        case ConstrType of
          0 : {Ausgangswert};
          1 : begin
                SendUPD:=FALSE;
                PProj(AData)^.SetActualMenu(mn_Snap);                {mn_Snap mn_CMovePoint}
                ConstructLayer^.Data^.ForEach(@DoAll);
                PProj(AData)^.PInfo^.EditPoly:=TRUE;
                PProj(AData)^.SnapItem:=PProj(AData)^.SelectByPoint(FirstPoint,[ot_Poly,ot_CPoly]);
                if PProj(AData)^.SnapItem<>NIL then begin
                  PProj(AData)^.ActualMen:=mn_SnapPoly;
                  PProj(AData)^.PointInd:=-1;
                end;
              end;
          2 : begin
                SendUPD:=FALSE;
                PProj(AData)^.SetActualMenu(mn_InsPoint);            {mn_InsPoint mn_CInsPoint}
                ConstructLayer^.Data^.ForEach(@DoAll);
                PProj(AData)^.PInfo^.EditPoly:=TRUE;
                PProj(AData)^.SnapItem:=PProj(AData)^.SelectByPoint(FirstPoint,[ot_Poly,ot_CPoly]);
                if PProj(AData)^.SnapItem<>NIL then begin
                  PProj(AData)^.ActualMen:=mn_InsPOK;
                  PProj(AData)^.PointInd:=-1;
                end;
              end;
          3 : begin
                SendUPD:=FALSE;
                PProj(AData)^.SetActualMenu(mn_DelPoint);            {mn_DelPoint mn_CDelPoint}
                ConstructLayer^.Data^.ForEach(@DoAll);
                PProj(AData)^.PInfo^.EditPoly:=TRUE;
                PProj(AData)^.SnapItem:=PProj(AData)^.SelectByPoint(FirstPoint,[ot_Poly,ot_CPoly]);
                if PProj(AData)^.SnapItem<>NIL then begin
                  PProj(AData)^.ActualMen:=mn_DelPOk;
                  PProj(AData)^.PointInd:=-1;
                end;
              end;
          4 : begin
                MakeHelpLayers2(AData);
                CopyToHelpLayers(AData,ConstructLayer,HelpLayer5);                 {Konstruktionsfl�che auf Layer kopieren}
                Str(WantedArea:0:2,StrArea);
                Str(RealArea:0:2,StrRArea);
                if ConstructType = ConstructFS then InfoStr:=GetLangText(11388)+#0
                else if ConstructType = ConstructS then InfoStr:=GetLangText(11389)+#0;
                Posi:=Pos('%',InfoStr);
                if Posi <> 0 then begin
                  Delete(InfoStr,Posi,1);
                  Insert(StrPas(AreaName),InfoStr,Posi);
                end;
                DialogPar:=TCParAreaDlg.Init(PProj(AData)^.Parent,StrArea,StrRArea,@AddDel,@ParReturn,InfoStr);
                if ExecDialog(DialogPar) = id_OK then begin
                  Val(StrArea,WantedParArea,Error);
                  PProj(AData)^.SetActualMenu(mn_CParallel);
                  PProj(AData)^.SetStatusText(GetLangText(11374),FALSE);
                end
                else begin
                  if ParReturn then begin
                    PostMessage(PProj(AData)^.Parent.Handle,wm_Command,cm_Constructis,0);
                    DeleteHelpLayers2(AData);
                  end
                  else begin
                    DeleteHelpLayers1(AData);
                    DeleteHelpLayers2(AData);
                    PProj(AData)^.SetActualMenu(mn_ConstructAbort);
                    PProj(AData)^.PInfo^.RedrawScreen(TRUE);
                  end;
                end;
              end;
          5 : begin
                Str(WantedArea:0:2,StrArea);
                Str(RealArea:0:2,StrRArea);
                if ConstructType = ConstructFS then InfoStr:=GetLangText(11388)+#0
                else if ConstructType = ConstructS then InfoStr:=GetLangText(11389)+#0;
                Posi:=Pos('%',InfoStr);
                if Posi <> 0 then begin
                  Delete(InfoStr,Posi,1);
                  Insert(StrPas(AreaName),InfoStr,Posi);
                end;
                DialogBuff:=TCBufAreaDlg.Init(PProj(AData)^.Parent,StrArea,StrRArea,@AddDel,@DrawSel,@ParReturn,InfoStr);
                if ExecDialog(DialogBuff) = id_OK then begin
                  MakeHelpLayers2(AData);                                                           {Hilfslayer 3, 4 und 5}
                  CopyToHelpLayers(AData,ConstructLayer,HelpLayer3);               {Konstruktionsfl�che auf Layer kopieren}
                  Val(StrArea,BufferSize,Error);                                       {Buffergr��e in Buffersize kopieren}
                  if DrawSel = 1 then begin
                    PProj(AData)^.SetStatusText(GetLangText(11401),FALSE);
                    PProj(AData)^.SetActualMenu(mn_CBufferDraw);
                  end
                  else if DrawSel = 2 then begin
                    {PProj(AData)^.SetStatusText(GetLangText(11402,FALSE);
                    PProj(AData)^.SetActualMenu(mn_CBufferSel);}
                    PostMessage(PProj(AData)^.Parent.Handle,wm_Command,cm_Constructis,0);  {l�schen wenn ausprogrammiert}
                  end;
                end
                else begin
                  if ParReturn then begin
                    PostMessage(PProj(AData)^.Parent.Handle,wm_Command,cm_Constructis,0);
                  end
                  else begin
                    DeleteHelpLayers1(AData);
                    PProj(AData)^.SetActualMenu(mn_ConstructAbort);
                    PProj(AData)^.PInfo^.RedrawScreen(TRUE);
                  end;
                end;
              end;
          6 : begin
                MakeHelpLayers2(AData);                                                             {Hilfslayer 3, 4 und 5}
                CopyToHelpLayers(AData,ConstructLayer,HelpLayer4);                 {Konstruktionsfl�che auf Layer kopieren}
                PProj(AData)^.SetActualMenu(mn_CIslands);
              end;
        end;
      end
      else Result:=FALSE;
    end;
    {$ENDIF}
  end;

Procedure TConstruct.DoInsertIsland
   (
   AData           : Pointer
   );
  Procedure DelCArea                                                                            {Konstruktionslayer leeren}
     (
     Item          : PIndex
     ); Far;
    begin
      ConstructLayer^.DeleteIndex(AData,Item);
    end;
  Procedure CopyAreas                                                                                    {Objekte kopieren}
     (
     Item          : PIndex
     ); Far;
    var IItem      : PIndex;
    begin
      if Item^.GetObjType = ot_CPoly then begin
        IItem:=New(PIndex,Init(Item^.Index));
        ConstructLayer^.InsertObject(PProj(AData)^.PInfo,IItem,FALSE);
        PProj(AData)^.Layers^.IndexObject(PProj(AData)^.PInfo,Item)^.Invalidate(PProj(AData)^.PInfo);
      end;
    end;
  begin
    AreaCut5(AData,HL4,HL5,HL3);                                                         {subtraktive Fl�chenverschneidung}
    ConstructLayer^.Data^.ForEach(@DelCArea);                                   {urspr�ngliche Konstruktionsfl�che l�schen}
    HelpLayer3^.Data^.ForEach(@CopyAreas);                                    {neue Fl�che auf Konstruktionslayer kopieren}
    PProj(AData)^.SetActualMenu(mn_CIslands);
  end;

Procedure TConstruct.DoBuffer
   (
   AData           : Pointer
   );
  var BuffLine     : PPoly;
      BufferDist   : LongInt;
  Procedure DelCArea                                                                            {Konstruktionslayer leeren}
     (
     Item          : PIndex
     ); Far;
    begin
      ConstructLayer^.DeleteIndex(AData,Item);
    end;
  Procedure CopyAreas                                                                                    {Objekte kopieren}
     (
     Item          : PIndex
     ); Far;
    var IItem      : PIndex;
    begin
      if Item^.GetObjType = ot_CPoly then begin
        IItem:=New(PIndex,Init(Item^.Index));
        ConstructLayer^.InsertObject(PProj(AData)^.PInfo,IItem,FALSE);
        PProj(AData)^.Layers^.IndexObject(PProj(AData)^.PInfo,Item)^.Invalidate(PProj(AData)^.PInfo);
      end;
    end;
  begin
    BuffLine:=PProj(AData)^.OffsetItem;

    BufferDist:=Trunc(BufferSize*100);
    PProj(AData)^.OffsetData.OffsetType:=otStreightDistance;
    PProj(AData)^.OffsetData.RoundEdges:=FALSE;
    ProcCreateOffset(AData,BufferDist);                                                        {Fl�che mit Offset erzeugen}
    PProj(AData)^.Layers^.DeleteIndex(PProj(AData)^.PInfo,PProj(AData)^.OffsetItem);                        {Linie l�schen}

    if AddDel = 1 then AreaCut6(AData,HL3,HL5,HL4)                           {Fl�chenverschneidung mit Konstruktionsfl�che}
    else AreaCut5(AData,HL3,HL5,HL4);                                                              {additiv oder subraktiv}

    ConstructLayer^.Data^.ForEach(@DelCArea);                                   {urspr�ngliche Konstruktionsfl�che l�schen}
    HelpLayer4^.Data^.ForEach(@CopyAreas);                                    {neue Fl�che auf Konstruktionslayer kopieren}

    PProj(AData)^.ClearStatusText;
    if DrawSel = 1 then PProj(AData)^.SetActualMenu(mn_CBufferDraw)
    else PProj(AData)^.SetActualMenu(mn_CBufferSel);
  end;

Function TConstruct.MoveLineParallel                                      {Linie zwischen zwei Punkten relativ verschieben}
   (
   APoly         : PPoly;
   BeginPoint    : TDPoint;
   EndPoint      : TDPoint;
   APercent      : Real
   )
   : LongInt;
  var XDist      : LongInt;
      YDist      : LongInt;
  begin
    XDist:=Round((EndPoint.X-BeginPoint.X)*APercent);
    YDist:=Round((EndPoint.Y-BeginPoint.Y)*APercent);
    APoly^.MoveRel(XDist,YDist);
    if Abs(XDist) > Abs(YDist) then Result:=Abs(XDist)
    else Result:=Abs(YDist);
  end;

Procedure TConstruct.DoOutParallel
   (
   AData           : Pointer
   );
  var CutArea      : PCPoly;
      CutLine      : PPoly;
      LineP1       : TDPoint;
      LineP2       : TDPoint;
      CutPoint     : TDPoint;
      RCutPoint    : TDPoint;
      InPoint      : TDPoint;
      OutPoint     : TDPoint;
      XInDir       : Integer;
      YInDir       : Integer;
      DistNear     : Double;
      DistFar      : Double;
      HasCut       : Boolean;
  Procedure DelCArea                                                                            {Konstruktionslayer leeren}
     (
     Item          : PIndex
     ); Far;
    var AObj       : PView;
    begin
      ConstructLayer^.DeleteIndex(AData,Item);
    end;
  Procedure GetObjects                                                           {Schnittfl�che und Schnittlinie ermitteln}
     (
     Item          : PIndex
     ); Far;
    var AObj       : PView;
    begin
      if Item^.GetObjType = ot_CPoly then begin
        CutArea:=PCPoly(ConstructLayer^.IndexObject(PProj(AData)^.PInfo,Item));
      end
      else if Item^.GetObjType = ot_Poly then begin
        CutLine:=PPoly(ConstructLayer^.IndexObject(PProj(AData)^.PInfo,Item));
      end;
    end;
  Procedure CalcDistances                                                      {n�chsten und entferntesten Punkt ermitteln}
     (
     Item          : PDPoint
     ); Far;
    var NDistance  : Double;
    begin
      PProj(AData)^.PInfo^.NormalDistance(@LineP1,@LineP2,Item,NDistance,CutPoint);
      if NDistance < DistNear then begin
        DistNear:=NDistance;
        InPoint.Init(Item^.X,Item^.Y);
      end;
      if NDistance > DistFar then begin
        DistFar:=NDistance;
        OutPoint.Init(Item^.X,Item^.Y);
        RCutPoint.Init(CutPoint.X,CutPoint.Y);
      end;
    end;
  begin
    HelpLayer5^.Data^.ForEach(@GetObjects);                                                  {beteiligte Objekte ermitteln}
    CutLineS:=CutLine;
    CutAreaS:=CutArea;
    LineP1.Init(PDPoint(CutLine^.Data^.At(0))^.X,PDPoint(CutLine^.Data^.At(0))^.Y);       {Anfangs- und Endpunkt der Linie}
    LineP2.Init(PDPoint(CutLine^.Data^.At(1))^.X,PDPoint(CutLine^.Data^.At(1))^.Y);
    if LineP2.X-LineP1.X = 0 then begin                                                                  {Winkel der Linie}
      if LineP2.Y > LineP1.Y then Beta1:=Pi/2
      else Beta1:=-Pi/2;
    end
    else Beta1:=ArcTan((LineP2.Y-LineP1.Y)/(LineP2.X-LineP1.X));
    if CutArea^.GetCutPoint(PProj(AData)^.PInfo,LineP1,LineP2,CutPoint) then HasCut:=TRUE                      {Schnittest}
    else HasCut:=FALSE;
    if HasCut then begin                                                  {Behandlung, wenn Parallellinie Fl�che schneidet}
      PProj(AData)^.SetStatusText(GetLangText(11384),FALSE);
      PProj(AData)^.SetActualMenu(mn_CParallelS);
    end
    else begin
      DistNear:=MaxLongInt;
      DistFar:=0;
      CutArea^.Data^.ForEach(@CalcDistances);                       {Abstand u. Koord von n�chstem und entferntestem Punkt}
      if OutPoint.X < RCutPoint.X then XInDir:=1                                                      {Verschieberichtung}
      else if OutPoint.X > RCutPoint.X then XInDir:=-1
      else XInDir:=0;
      if OutPoint.Y < RCutPoint.Y then YInDir:=1
      else if OutPoint.Y > RCutPoint.Y then YInDir:=-1
      else YInDir:=0;
      DoParallel(AData,CutArea,CutLine,InPoint,OutPoint,XInDir,YInDir);                         {Parallelteilung ausf�hren}
    end;
    if not HasCut then begin
      ConstructLayer^.Data^.ForEach(@DelCArea);
      ProcCreateIslandAreaWithoutDlg(PProj(AData),HelpLayer4,ConstructLayer);
    end;
    if not HasCut then PProj(AData)^.SetActualMenu(mn_CParallel);
  end;

Procedure TConstruct.DoInParallel
   (
   AData           : Pointer
   );
  var CutArea      : PCPoly;
      CutLine      : PPoly;
      CutPoint     : TDPoint;
      InPoint      : TDPoint;
      OutPoint     : TDPoint;
      XInDir       : Integer;
      YInDir       : Integer;
      LineP1       : TDPoint;
      LineP2       : TDPoint;
      DistIn       : Double;
      DistOut      : Double;
  Procedure DelCArea
     (
     Item          : PIndex
     ); Far;
    var AObj       : PView;
    begin
      ConstructLayer^.DeleteIndex(AData,Item);
    end;
  Procedure CalcDirSidePoint                                                             {Richtung des InPunktes ermitteln}
     (
     PL1           : TDPoint;
     PL2           : TDPoint;
     PD            : TDPoint
     );
    var NDistance  : Double;
    begin
      PProj(AData)^.PInfo^.NormalDistance(@PL1,@PL2,@PD,NDistance,CutPoint);
      if PD.X > CutPoint.X then XInDir:=1
      else if PD.X < CutPoint.X then XInDir:=-1
      else XInDir:=0;
      if PD.Y > CutPoint.Y then YInDir:=1
      else if PD.Y < CutPoint.Y then YInDir:=-1
      else YInDir:=0;
    end;
  Procedure CalcDistances                                                     {entfernteste Punkte beider Seiten ermitteln}
     (
     Item          : PDPoint
     ); Far;
    var NDistance  : Double;


        DirX       : Integer;
        DirY       : Integer;
    begin
      PProj(AData)^.PInfo^.NormalDistance(@LineP1,@LineP2,Item,NDistance,CutPoint);
      if Item^.X > CutPoint.X then DirX:=1
      else if Item^.X < CutPoint.X then DirX:=-1
      else DirX:=0;
      if Item^.Y > CutPoint.Y then DirY:=1
      else if Item^.Y < CutPoint.Y then DirY:=-1
      else DirY:=0;
      if (DirX = XInDir) and (DirY = YInDir) then begin
        if NDistance > DistIn then begin
          DistIn:=NDistance;
          InPoint.Init(Item^.X,Item^.Y);
        end;
      end
      else begin
        if NDistance > DistOut then begin
          DistOut:=NDistance;
          OutPoint.Init(Item^.X,Item^.Y);
        end;
      end;
      if NDistance = 0 then begin
        if DistIn = 0 then InPoint.Init(Item^.X,Item^.Y);
        if DistOut = 0 then OutPoint.Init(Item^.X,Item^.Y);
      end;
    end;
  begin
    CutLine:=PPoly(CutLineS);
    CutArea:=PCPoly(CutAreaS);
    LineP1.Init(PDPoint(CutLine^.Data^.At(0))^.X,PDPoint(CutLine^.Data^.At(0))^.Y);
    LineP2.Init(PDPoint(CutLine^.Data^.At(1))^.X,PDPoint(CutLine^.Data^.At(1))^.Y);
    CalcDirSidePoint(LineP1,LineP2,SidePoint);                                             {Richtung des Seitenwahlpunktes}
    DistIn:=0;
    DistOut:=0;
    CutArea^.Data^.ForEach(@CalcDistances);                         {Abstand u. Koord von n�chstem und entferntestem Punkt}
    DoParallel(AData,CutArea,CutLine,InPoint,OutPoint,XInDir,YInDir);                           {Parallelteilung ausf�hren}
    ConstructLayer^.Data^.ForEach(@DelCArea);
    PProj(AData)^.SetActualMenu(mn_CParallel);
    ProcCreateIslandAreaWithoutDlg(PProj(AData),HelpLayer4,ConstructLayer);
  end;

Procedure TConstruct.DoParallel
   (
   AData           : Pointer;
   CutArea         : PCPoly;
   CutLine         : PPoly;
   InPoint         : TDPoint;
   OutPoint        : TDPoint;
   XInDir          : Integer;
   YInDir          : Integer
   );
  var RealCArea    : Double;
      MovedCutLine : PPoly;
      CalcCutArea  : PCPoly;
      WholeArea    : Double;
      LineP1       : TDPoint;
      LineP2       : TDPoint;
      SwapPoint    : TDPoint;
      MCLMp        : TDPoint;
      NDistPercent : Real;
      ODistPercent : Real;
      LDistPercent : Real;
      BiggerMove   : LongInt;
      ExitWhile    : Boolean;
      MissingArea  : Double;
      CorrMove     : Real;
  Procedure CalcAreaSum                                     {Summer der Fl�chen auf einer Seite der Schnittlinie berechnen}
     (
     Item          : PIndex
     ); Far;
    var AObj       : PView;
    begin
      if Item^.GetObjType = ot_CPoly then begin
        AObj:=HelpLayer4^.IndexObject(PProj(AData)^.PInfo,Item);
        RealCArea:=RealCArea+PCPoly(AObj)^.Flaeche;
      end;
    end;
  Procedure DelObjects                                                                                    {Objekte l�schen}
     (
     Item          : PIndex
     ); Far;
    begin
      PObjects(PProj(AData)^.PInfo^.Objects)^.DeleteIndex(PProj(AData)^.PInfo,Item);
    end;
  Function IsInPoint                                             {Test, ob Punkt auf der richtigen Seite einer Linie liegt}
     (
     APoint         : PDPoint
     )
     : Boolean;
    var NDistance   : Double;
        ACutPoint   : TDPoint;
        XDir        : Integer;
        YDir        : Integer;
    begin
      PProj(AData)^.PInfo^.NormalDistance(PDPoint(MovedCutLine^.Data^.At(0)),PDPoint(MovedCutLine^.Data^.At(1)),
                                          APoint,NDistance,ACutPoint);
      if APoint^.X < ACutPoint.X then XDir:=-1
      else if APoint^.X > ACutPoint.X then XDir:=1
      else XDir:=0;
      if APoint^.Y < ACutPoint.Y then YDir:=-1
      else if APoint^.Y > ACutPoint.Y then YDir:=1
      else YDir:=0;
      if (XDir = XInDir) and (YDir = YInDir) then Result:=TRUE
      else Result:=FALSE;
    end;
  Procedure ConstructCutArea;                  {Fl�che f�r Verschneidung aus Fl�chenClipRect und Schnittlinie konstruieren}
    var NextPoint  : TDPoint;
        L1P1       : TDPoint;
        L1P2       : TDPoint;
        L2P1       : TDPoint;
        L2P2       : TDPoint;
        InsNow     : Boolean;
    Procedure SetPointCoord
       (
       var SPoint  : TDPoint;
       NXCoord     : LongInt;
       NYCoord     : LongInt
       );
      begin
        SPoint.X:=NXCoord;
        SPoint.Y:=NYCoord;
      end;
    begin
      while CalcCutArea^.Data^.Count > 0 do CalcCutArea^.DeletePointByIndex(0);                       {alle Punkte l�schen}
      NextPoint.Init(0,0);
      L1P1.Init(0,0);
      L1P2.Init(0,0);
      L2P1.Init(PDPoint(MovedCutLine^.Data^.At(0))^.X,PDPoint(MovedCutLine^.Data^.At(0))^.Y);
      L2P2.Init(PDPoint(MovedCutLine^.Data^.At(1))^.X,PDPoint(MovedCutLine^.Data^.At(1))^.Y);
      SetPointCoord(NextPoint,CutArea^.ClipRect.A.X,CutArea^.ClipRect.A.Y);
      if IsInPoint(@NextPoint) then begin                                               {erster Punkt auf richtiger Seite?}
        CalcCutArea^.InsertPoint(NextPoint);                                                         {1. Eckpunkt einf�gen}
        InsNow:=TRUE;
      end
      else InsNow:=FALSE;
      SetPointCoord(L1P1,CutArea^.ClipRect.A.X,CutArea^.ClipRect.A.Y);
      SetPointCoord(L1P2,CutArea^.ClipRect.A.X,CutArea^.ClipRect.B.Y);
      if PProj(AData)^.PInfo^.LineCut(FALSE,L1P1,L1P2,L2P1,L2P2,NextPoint) then begin                          {Schnittest}
        CalcCutArea^.InsertPoint(NextPoint);                                                        {Schnittpunkt einf�gen}
        InsNow:=not InsNow;
      end;
      if InsNow then begin
        SetPointCoord(NextPoint,L1P2.X,L1P2.Y);
        CalcCutArea^.InsertPoint(NextPoint);                                                         {2. Eckpunkt einf�gen}
      end;
      SetPointCoord(L1P1,CutArea^.ClipRect.A.X,CutArea^.ClipRect.B.Y);
      SetPointCoord(L1P2,CutArea^.ClipRect.B.X,CutArea^.ClipRect.B.Y);
      if PProj(AData)^.PInfo^.LineCut(FALSE,L1P1,L1P2,L2P1,L2P2,NextPoint) then begin                          {Schnittest}
        CalcCutArea^.InsertPoint(NextPoint);                                                        {Schnittpunkt einf�gen}
        InsNow:=not InsNow;
      end;
      if InsNow then begin
        SetPointCoord(NextPoint,L1P2.X,L1P2.Y);
        CalcCutArea^.InsertPoint(NextPoint);                                                         {3. Eckpunkt einf�gen}
      end;
      SetPointCoord(L1P1,CutArea^.ClipRect.B.X,CutArea^.ClipRect.B.Y);
      SetPointCoord(L1P2,CutArea^.ClipRect.B.X,CutArea^.ClipRect.A.Y);
      if PProj(AData)^.PInfo^.LineCut(FALSE,L1P1,L1P2,L2P1,L2P2,NextPoint) then begin                          {Schnittest}
        CalcCutArea^.InsertPoint(NextPoint);                                                        {Schnittpunkt einf�gen}
        InsNow:=not InsNow;
      end;
      if InsNow then begin
        SetPointCoord(NextPoint,L1P2.X,L1P2.Y);
        CalcCutArea^.InsertPoint(NextPoint);                                                         {4. Eckpunkt einf�gen}
      end;
      SetPointCoord(L1P1,CutArea^.ClipRect.B.X,CutArea^.ClipRect.A.Y);
      SetPointCoord(L1P2,CutArea^.ClipRect.A.X,CutArea^.ClipRect.A.Y);
      if PProj(AData)^.PInfo^.LineCut(FALSE,L1P1,L1P2,L2P1,L2P2,NextPoint) then begin                          {Schnittest}
        CalcCutArea^.InsertPoint(NextPoint);                                                        {Schnittpunkt einf�gen}
        InsNow:=not InsNow;
      end;
      CalcCutArea^.ClosePoly;                                                                           {Polygon schlie�en}
    end;
  begin
    InAction:=TRUE;
    PProj(AData)^.PInfo^.DoDrawObject:=FALSE;
    PProj(AData)^.SetCursor(crHourGlass);
    RealCArea:=RealArea;
    WholeArea:=RealArea;
    LineP1.Init(PDPoint(CutLine^.Data^.At(0))^.X,PDPoint(CutLine^.Data^.At(0))^.Y);       {Anfangs- und Endpunkt der Linie}
    LineP2.Init(PDPoint(CutLine^.Data^.At(1))^.X,PDPoint(CutLine^.Data^.At(1))^.Y);
    if AddDel = 2 then begin
      SwapPoint.Init(InPoint.X,InPoint.Y);
      InPoint.Done;
      InPoint.Init(OutPoint.X,OutPoint.Y);
      OutPoint.Done;
      OutPoint.Init(SwapPoint.X,SwapPoint.Y);
      SwapPoint.Done;
      WantedParArea:=RealArea-WantedParArea;
      XInDir:=-XInDir;
      YInDir:=-YInDir;
    end;
    MovedCutLine:=New(PPoly,Init);
    MovedCutLine^.InsertPoint(PDPoint(CutLine^.Data^.At(0))^);
    MovedCutLine^.InsertPoint(PDPoint(CutLine^.Data^.At(1))^);
    MCLMp.X:=PDPoint(MovedCutLine^.Data^.At(0))^.X+
             Round((PDPoint(MovedCutLine^.Data^.At(1))^.X-PDPoint(MovedCutLine^.Data^.At(0))^.X)/2);
    MCLMp.Y:=PDPoint(MovedCutLine^.Data^.At(0))^.Y+
             Round((PDPoint(MovedCutLine^.Data^.At(1))^.Y-PDPoint(MovedCutLine^.Data^.At(0))^.Y)/2);
    MoveLineParallel(MovedCutLine,MCLMp,InPoint,1);
    CalcCutArea:=New(PCPoly,Init);
    NDistPercent:=WantedParArea/WholeArea;                                           {n�chsten Verschiebeabstand berechnen}
    MoveLineParallel(MovedCutLine,InPoint,OutPoint,NDistPercent);                                     {parallelverschieben}
    ConstructCutArea;
    PProj(AData)^.InsertObjectOnLayer(CalcCutArea,HelpLayer3^.Index);
    AreaCut6(AData,HL3,HL5,HL4);
    RealCArea:=0;
    HelpLayer4^.Data^.ForEach(@CalcAreaSum);                                                          {konstruierte Fl�che}
    ODistPercent:=NDistPercent;
    BiggerMove:=MaxLongInt;
    ExitWhile:=FALSE;
    {***********************************************************************************************************}
    {* Schnittlinie abh�ngig vom letzten Resultat verschieben.                                                 *}
    {***********************************************************************************************************}
    while ((((RealCArea/WantedParArea) > 1.05) or ((RealCArea/WantedParArea) < 0.95)))
       and (BiggerMove > 0) and not ExitWhile do begin
      LDistPercent:=NDistPercent;
      if RealCArea < WantedParArea then begin
        MissingArea:=WantedParArea-RealCArea;                                                     {fehlender Fl�cheninhalt}
        NDistPercent:=MissingArea/(WholeArea-RealCArea)*(1-ODistPercent);            {n�chsten Verschiebeabstand berechnen}
        if (NDistPercent+LDistPercent < 1E-5) and (NDistPercent+LDistPercent > -1E-5) then NDistPercent:=NDistPercent/2;
        if (LDistPercent < 0) and (NDistPercent > Abs(LDistPercent)) then NDistPercent:=Abs(LDistPercent)/2;
      end
      else begin
        MissingArea:=RealCArea-WantedParArea;                                                 {�berfl�ssiger Fl�cheninhalt}
        NDistPercent:=-MissingArea/RealCArea*ODistPercent;                           {n�chsten Verschiebeabstand berechnen}
        if (NDistPercent+LDistPercent < 1E-5) and (NDistPercent+LDistPercent > -1E-5) then NDistPercent:=NDistPercent/2;
        if (LDistPercent > 0) and (Abs(NDistPercent) > LDistPercent) then NDistPercent:=-LDistPercent/2;
      end;
      if (NDistPercent > -0.01) and (NDistPercent < 0.01) then ExitWhile:=TRUE;            {Vermeidung von Endlosschleifen}
      ODistPercent:=ODistPercent+NDistPercent;
      HelpLayer3^.Data^.ForEach(@DelObjects);                                                        {alte Fl�chen l�schen}
      HelpLayer3^.Data^.DeleteAll;
      HelpLayer4^.Data^.ForEach(@DelObjects);
      HelpLayer4^.Data^.DeleteAll;
      BiggerMove:=MoveLineParallel(MovedCutLine,InPoint,OutPoint,NDistPercent);                       {parallelverschieben}
      CalcCutArea:=New(PCPoly,Init);
      ConstructCutArea;
      PProj(AData)^.InsertObjectOnLayer(CalcCutArea,HelpLayer3^.Index);
      AreaCut6(AData,HL3,HL5,HL4);
      RealCArea:=0;
      HelpLayer4^.Data^.ForEach(@CalcAreaSum);
    end;
    {***********************************************************************************************************}
    {* Schnittlinie in immer kleineren Abst�nden verschieben, bis kein Unterschied mehr bemerkbar ist.         *}
    {***********************************************************************************************************}
    while (BiggerMove > 0) do begin
      if RealCArea < WantedParArea then begin
        NDistPercent:=Abs(NDistPercent)/2;                                           {n�chsten Verschiebeabstand berechnen}
      end
      else begin
        NDistPercent:=-Abs(NDistPercent)/2;                                          {n�chsten Verschiebeabstand berechnen}
      end;
      ODistPercent:=ODistPercent+NDistPercent;
      HelpLayer3^.Data^.ForEach(@DelObjects);                                                        {alte Fl�chen l�schen}
      HelpLayer3^.Data^.DeleteAll;
      HelpLayer4^.Data^.ForEach(@DelObjects);
      HelpLayer4^.Data^.DeleteAll;
      BiggerMove:=MoveLineParallel(MovedCutLine,InPoint,OutPoint,NDistPercent);                       {parallelverschieben}
      if BiggerMove = 1 then CorrMove:=Abs(NDistPercent);
      CalcCutArea:=New(PCPoly,Init);
      ConstructCutArea;
      PProj(AData)^.InsertObjectOnLayer(CalcCutArea,HelpLayer3^.Index);
      AreaCut6(AData,HL3,HL5,HL4);
      RealCArea:=0;
      HelpLayer4^.Data^.ForEach(@CalcAreaSum);
    end;
    {***********************************************************************************************************}
    {* Die resultierende Fl�che mu� gleich oder gr��er sein, als die gew�nschte. Daher wird die bis hierher    *}
    {* ermittelte Fl�che bei Bedarf in gr��eren Schritten unter die Grenze gebracht und dann in kleineren      *}
    {* Schritten wieder �ber die Grenze, damit eine m�glichst gro�e Ann�herung erzielt wird.                   *}
    {***********************************************************************************************************}
    while RealCArea > WantedParArea do begin
      if (Beta1 > -Pi/4) and (Beta1 < Pi/4) then begin                                                  {Linie verschieben}
        if YInDir = 1 then MovedCutLine^.MoveRel(0,1)
        else MovedCutLine^.MoveRel(0,-1);
      end
      else begin
        if XInDir = 1 then MovedCutLine^.MoveRel(1,0)
        else MovedCutLine^.MoveRel(-1,0);
      end;
      HelpLayer3^.Data^.ForEach(@DelObjects);                                                        {alte Fl�chen l�schen}
      HelpLayer3^.Data^.DeleteAll;
      HelpLayer4^.Data^.ForEach(@DelObjects);
      HelpLayer4^.Data^.DeleteAll;
      CalcCutArea:=New(PCPoly,Init);
      ConstructCutArea;
      PProj(AData)^.InsertObjectOnLayer(CalcCutArea,HelpLayer3^.Index);
      AreaCut6(AData,HL3,HL5,HL4);
      RealCArea:=0;
      HelpLayer4^.Data^.ForEach(@CalcAreaSum);
    end;
    while RealCArea < WantedParArea do begin
      if (YInDir = 0) or ((Beta1 > -Pi/4) and (Beta1 < Pi/4) and (XInDir <> 0)) then begin
        if XInDir = 1 then MovedCutLine^.MoveRel(-1,0)                                                  {Linie verschieben}
        else MovedCutLine^.MoveRel(1,0);
      end
      else begin
        if YInDir = 1 then MovedCutLine^.MoveRel(0,-1)
        else MovedCutLine^.MoveRel(0,1);
      end;
      HelpLayer3^.Data^.ForEach(@DelObjects);                                                        {alte Fl�chen l�schen}
      HelpLayer3^.Data^.DeleteAll;
      HelpLayer4^.Data^.ForEach(@DelObjects);
      HelpLayer4^.Data^.DeleteAll;
      CalcCutArea:=New(PCPoly,Init);
      ConstructCutArea;
      PProj(AData)^.InsertObjectOnLayer(CalcCutArea,HelpLayer3^.Index);
      AreaCut6(AData,HL3,HL5,HL4);
      RealCArea:=0;
      HelpLayer4^.Data^.ForEach(@CalcAreaSum);
    end;
    Dispose(MovedCutLine,Done);
    PProj(AData)^.SetCursor(crDefault);
    PProj(AData)^.PInfo^.DoDrawObject:=TRUE;
    InAction:=FALSE;
  end;

Function TConstruct.EndConstructDB
   (
   AData           : Pointer
   ) : Boolean;
{$IFNDEF WMLT}
  var ID           : LongInt;
      Tmp          : String;
      NewArea      : Double;
      ConstrArea   : Double;
      DDEStr       : String;
      Dialog       : TCAreaDlg;
      DlgVal       : Integer;
      InfoStr      : String;
      Posi         : Integer;
  Procedure CopyToCL1
     (
     Item          : PIndex
     ); Far;
    var IItem      : PIndex;
        AObj       : PView;
    begin
      IItem:=New(PIndex,Init(Item^.Index));
      ConstructLayer^.InsertObject(PProj(AData)^.PInfo,IItem,FALSE);
      PProj(AData)^.Layers^.IndexObject(PProj(AData)^.PInfo,Item)^.Invalidate(PProj(AData)^.PInfo);
    end;
  Procedure CopyToHL6
     (
     Item          : PIndex
     ); Far;
    var IItem      : PIndex;
        AObj       : PView;
    begin
      IItem:=New(PIndex,Init(Item^.Index));
      HelpLayer6^.InsertObject(PProj(AData)^.PInfo,IItem,FALSE);
      PProj(AData)^.Layers^.IndexObject(PProj(AData)^.PInfo,Item)^.Invalidate(PProj(AData)^.PInfo);
    end;
  Procedure DoAll
     (
     Item          : PIndex
     ); Far;
    var IItem      : PIndex;
        AObj       : PView;
    begin
      if Item^.GetObjType = ot_CPoly then begin
        IItem:=New(PIndex,Init(Item^.Index));
        if ConstructType = ConstructFS then FSLayer^.InsertObject(PProj(AData)^.PInfo,IItem,FALSE)
        else if ConstructType = ConstructS then SLayer^.InsertObject(PProj(AData)^.PInfo,IItem,FALSE);
        PProj(AData)^.Layers^.IndexObject(PProj(AData)^.PInfo,Item)^.Invalidate(PProj(AData)^.PInfo);
        AObj:=HelpLayer2^.IndexObject(PProj(AData)^.PInfo,Item);
        ID:=AObj^.Index;
      end;
    end;
  Procedure CalcArea
     (
     Item          : PIndex
     ); Far;
    var AObj       : PView;
    begin
      if Item^.GetObjType = ot_CPoly then begin
        AObj:=HelpLayer7^.IndexObject(PProj(AData)^.PInfo,Item);
        NewArea:=NewArea+PCPoly(AObj)^.Flaeche;
      end;
    end;
  Procedure CalcCArea
     (
     Item          : PIndex
     ); Far;
    var AObj       : PView;
    begin
      if Item^.GetObjType = ot_CPoly then begin
        AObj:=ConstructLayer^.IndexObject(PProj(AData)^.PInfo,Item);
        ConstrArea:=ConstrArea+PCPoly(AObj)^.Flaeche;
      end;
    end;
  Procedure DelObjects6
     (
     Item          : PIndex
     ); Far;
    begin
      HelpLayer6^.DeleteIndex(AData,Item);
    end;
  Procedure DelObjects7
     (
     Item          : PIndex
     ); Far;
    begin
      HelpLayer7^.DeleteIndex(AData,Item);
    end;
  var a,b,c,d,e,f:LongInt;
{$ENDIF}
  begin
{$IFNDEF WMLT}
    MakeSaveLayers(AData);
    ConstrArea:=0;
    ConstructLayer^.Data^.ForEach(@CalcCArea);
    a:=ConstructLayer^.Data^.GetCount;                    {����������}
    b:=HelpLayer1^.Data^.GetCount;                    {����������}
    AreaCut6(AData,HL1,CL1,HL6);       {HL1}                                            {Fl�che innerhalb der Ausgangsfl�chen}
    c:=HelpLayer6^.Data^.GetCount;                    {����������}
    if ConstructType = ConstructFS then AreaCut5(AData,HL6,FSLayer^.Text^,HL7)   {�berlagerung mit alten Fl�chen vermeiden}
    else if ConstructType = ConstructS then AreaCut5(AData,HL6,SLayer^.Text^,HL7);
    d:=HelpLayer7^.Data^.GetCount;                    {����������}
    NewArea:=0;
    HelpLayer7^.Data^.ForEach(@CalcArea);                                                      {Wirkliche Fl�che berechnen}
    if ShowDlgs then begin
      if ConstructType = ConstructFS then InfoStr:=GetLangText(11388)+#0
      else if ConstructType = ConstructS then InfoStr:=GetLangText(11389)+#0;
      Posi:=Pos('%',InfoStr);
      if Posi<>0 then begin
        Delete(InfoStr,Posi,1);
        Insert(StrPas(AreaName),InfoStr,Posi);
      end;
      Dialog:=TCAreaDlg.Init(PProj(AData)^.Parent,WantedArea,NewArea,ConstrArea,InfoStr);
      if ExecDialog(Dialog)=id_OK then begin
        if PProj(AData)^.Layers^.TopLayer^.Data^.GetCount > 1 then begin
          HelpLayer6^.Data^.ForEach(@DelObjects6);                         {�ndern!!!!!}
          HelpLayer7^.Data^.ForEach(@CopyToHL6);
          HelpLayer7^.Data^.ForEach(@DelObjects7);
          ProcCreateIslandAreaWithoutDlg(PProj(AData),HelpLayer6,HelpLayer7);       { zusammenh�ngende Fl�chen einbeziehen}
          e:=HelpLayer7^.Data^.GetCount;                    {����������}
          {CreateSingleArea(AData,HelpLayer6,HelpLayer7);}
        end;
        PProj(AData)^.Layers^.TopLayer^.Data^.ForEach(@DoAll);
        if ConstructType = ConstructFS then DDEStr:='[NRF]['
        else if ConstructType = ConstructS then DDEStr:='[NRS][';
        Tmp:=StrPas(AreaName);
        DDEStr:=DDEStr+Tmp+'][';
        Str(ID:10,Tmp);
        DDEStr:=DDEStr+Tmp;
        Str(NewArea:0:2,Tmp);
        DDEStr:=DDEStr+']['+Tmp+']';
        {$IFNDEF AXDLL} // <----------------- AXDLL
        if not GraphicMode then DDEHandler.SendString(DDEStr);
        {$ENDIF} // <----------------- AXDLL
        DeleteHelpLayers1(AData);
        Result:=FALSE;
      end
      else begin
        if ConstructRetry then begin
          ConstructRetry:=FALSE;
          PProj(AData)^.Layers^.DeleteLayer(ConstructLayer);
          ConstructLayer:=PProj(AData)^.Layers^.InsertLayer(CL1,RGBColors[c_Black],lt_Solid,RGBColors[c_Black],
                                                            pt_DiagCross,ilTop,DefaultSymbolFill);
          ConstructLayer^.SetState(sf_Transparent,TRUE);
//!?!?!          ProcSetManSnapOrder(PProj(AData));
          HelpLayer7^.Data^.ForEach(@CopyToCL1);
          Result:=TRUE;
        end
        else begin
          DeleteHelpLayers1(AData);
          {PProj(AData)^.PInfo^.RedrawScreen(TRUE);}
          Result:=FALSE;
        end;
      end;
    end
    else begin
      PProj(AData)^.Layers^.TopLayer^.Data^.ForEach(@DoAll);
      if ConstructType = ConstructFS then DDEStr:='[NRF]['
      else if ConstructType = ConstructS then DDEStr:='[NRS][';
      Tmp:=StrPas(AreaName);
      DDEStr:=DDEStr+Tmp+'][';
      Str(ID:10,Tmp);
      DDEStr:=DDEStr+Tmp;
      Str(NewArea:0:2,Tmp);
      DDEStr:=DDEStr+']['+Tmp+']';
      {$IFNDEF AXDLL} // <----------------- AXDLL
      if not GraphicMode then DDEHandler.SendString(DDEStr);
      {$ENDIF} // <----------------- AXDLL
      DeleteHelpLayers1(AData);
      Result:=FALSE;
    end;
    DeleteSaveLayers(AData);
    ResetProjStatus(AData);
    PProj(AData)^.PInfo^.RedrawScreen(TRUE);
{$ENDIF}
  end;

Procedure TConstruct.ResetProjStatus
   (
   AData           : Pointer
   );
  begin
    if GraphicMode then begin
      GSLayer:=OldGSL;
      FSLayer:=OldFSL;
      GraphicMode:=FALSE;
    end;
    PProj(AData)^.Layers^.SetTopLayer(RealTopLayer);
{!?!?!?    case RealTranspMode of
      0 : PProj(AData)^.Layers^.TranspLayer:=0;
      1 : PProj(AData)^.Layers^.TranspLayer:=1;
      2 : PProj(AData)^.Layers^.TranspLayer:=2;
    end;                                          }
    UserInterface.Update([uiMenus]);
  end;

Procedure TConstruct.CreateSingleArea
   (
   AData           : Pointer;
   SourceLayer     : PLayer;
   DestLayer       : PLayer
   );
  var Proj         : PProj;
      A1           : PCPoly;
      A2           : PCPoly;
      A3           : PCPoly;
      P1           : PIndex;
      P2           : PIndex;
      SelLayer     : PLayer;
      i            : Longint;
      A1Count      : Longint;
      Start        : LongInt;
      LastA1Point  : PDPoint;
      A1Dir        : Boolean;
      A2Dir        : Boolean;
      A2InA1       : Boolean;
      NotA2InA1    : Boolean;
      InvertDir    : Boolean;
      DDEStr       : String;
      Tmp          : String;
      NewArea      : Double;
      First        : Boolean;
      OldItem      : PIndex;
      AreaNr       : Integer;
  Procedure DelObjectsOnDestLayer                                                                        {Ziellayer leeren}
     (
     Item          : PIndex
     ); Far;
    begin
      DestLayer^.DeleteIndex(AData,Item);
    end;
  Procedure DelObjectsOnSourceLayer                                                                      {Quellayer leeren}
     (
     Item          : PIndex
     ); Far;
    begin
      SourceLayer^.DeleteIndex(AData,Item);
    end;
  Procedure CopyDestToSource                                             {Resultatfl�che auf urspr�nglichen Layer kopieren}
     (
     Item          : PIndex
     ); Far;
    var IItem      : PIndex;
    begin
      if Item^.GetObjType = ot_CPoly then begin
        IItem:=New(PIndex,Init(Item^.Index));
        SourceLayer^.InsertObject(PProj(AData)^.PInfo,IItem,FALSE);
        PProj(AData)^.Layers^.IndexObject(PProj(AData)^.PInfo,Item)^.Invalidate(PProj(AData)^.PInfo);
      end;
    end;
  begin
    Proj:=PProj(AData);
    First:=TRUE;
    AreaNr:=0;
    DestLayer^.Data^.ForEach(@DelObjectsOnDestLayer);
    while AreaNr < SourceLayer^.Data^.GetCount do begin
      if First then begin
        P1:=PIndex(SourceLayer^.Data^.At(AreaNr));                                                { erste mu� gr��er sein }
        A1:=PCPoly(SourceLayer^.IndexObject(Proj^.PInfo,P1));
        Inc(AreaNr);
        P2:=PIndex(SourceLayer^.Data^.At(AreaNr));                                                { zweite ist kleinere ! }
        A2:=PCPoly(SourceLayer^.IndexObject(Proj^.PInfo,P2));
        Inc(AreaNr);
      end
      else begin
        A1:=A3;                                                                      { Resultat der ersten Fl�chenbildung }
        OldItem:=A1;
        P2:=PIndex(SourceLayer^.Data^.At(AreaNr));                                                       { n�chste Fl�che }
        A2:=PCPoly(SourceLayer^.IndexObject(Proj^.PInfo,P2));
        Inc(AreaNr);
      end;
      A2InA1:=FALSE;                                                                    { �berpr�fen ob A2 innerhalb oder }
      NotA2InA1:=FALSE;                                                                       { au�erhalb von A1 liegt... }
      for i:=0 to A2^.Data^.Count-1 do begin
        if (A1^.PointInSide(Proj^.PInfo,PDPoint(A2^.Data^.At(i))^)) then A2InA1:=TRUE                   { Punkt innerhalb?}
        else NotA2InA1:=TRUE;
      end;

      if (not (A2InA1 and NotA2InA1)) and                             { nur wenn entweder in oder au�er umgebender Fl�che }
         (not A2^.GetState(sf_IslandArea)) then begin                                    {A2 darf keine Inselfl�che sein! }
        A3:=MakeObjectCopy(A1);                                                                     { A1 nach A3 kopieren }
        A3^.State:=A3^.State or sf_IslandArea;                                          { A3 ist jetzt eine Inselfl�che !!}
        A1Count:=A1^.Data^.Count;                                                                   { Punkte vorher in A1 }
        LastA1Point:=MakeObjectCopy(PDPoint(A1^.Data^.At(A1Count-1)));                              { Schlie�punkt merken }
        A1Dir:=A1^.Direction;                                                          { Richtungssinn der �u�eren Fl�che }
        A2Dir:=A2^.Direction;                                                          { Richtungssinn der inneren Fl�che }
        InvertDir:=FALSE;
        if (A2InA1 and (A1Dir = A2Dir)) or (NotA2InA1 and (A1Dir <> A2Dir)) then InvertDir:=TRUE;
                                                             { ^^^ wenn innere Fl�che falschen Drehsinn hat dann umdrehen }
        if (A1^.GetState(sf_IslandArea)) then begin                                          { A1 ist bereits Inselfl�che }
          FreeMem(A3^.IslandInfo,A3^.IslandCount*SizeOf(LongInt));             { altes Info freigeben, von MakeObjectCopy }
          A3^.IslandCount:=A1^.IslandCount+1;                                                            { f�r neue Insel }
          GetMem(A3^.IslandInfo,A3^.IslandCount*SizeOf(LongInt));
          for i:=0 to A3^.IslandCount-1 do A3^.IslandInfo^[i]:=A1^.IslandInfo^[i];      { ev. vorhanden Inseln �bernehmen }
          Start:=A1^.IslandCount;                                                 { erst nach vorhandenen Inseln anh�ngen }
        end
        else begin
          A3^.IslandCount:=2;
          GetMem(A3^.IslandInfo,2*SizeOf(LongInt));
          A3^.IslandInfo^[0]:=A1Count;                                               { Anzahl Punkte erster Fl�che merken }
          Start:=1;                                                                              { freie Pl�tze am Anfang }
        end;

        if not InvertDir then
          for i:=0 to A2^.Data^.Count-1 do A3^.InsertPoint(PDPoint(A2^.Data^.At(i))^)       { aus A2 Inselfl�che kopieren }
        else
          for i:=A2^.Data^.Count-1 downto 0 do                                                 { mit umgekehrten Drehsinn }
            A3^.InsertPoint(PDPoint(A2^.Data^.At(i))^);                                     { aus A2 Inselfl�che kopieren }

        A3^.InsertPoint(LastA1Point^);                                                           { mit Endpunkt schlie�en }
        A3^.IslandInfo^[Start]:=A2^.Data^.Count;                                        { A2 Anzahl Punkte innen zeichnen }

        First:=FALSE;
      end;
    end;
    Proj^.InsertObject(A3,FALSE);                                                                  { Inselfl�che einf�gen }
  end;

Procedure TConstruct.BreakConstructDB
   (
   AData           : Pointer
   );
  begin
    DeleteHelpLayers1(AData);
    ResetProjStatus(AData);
  end;

Procedure TConstruct.MakeHelpLayers
   (
   AData           : Pointer
   );
  begin
    HelpLayer1:=PProj(AData)^.Layers^.InsertLayer(HL1,RGBColors[c_Black],lt_Solid,RGBColors[c_Black],pt_NoPattern,ilLast,DefaultSymbolFill);
    HelpLayer2:=PProj(AData)^.Layers^.InsertLayer(HL2,RGBColors[c_Black],lt_Solid,RGBColors[c_Black],pt_NoPattern,ilLast,DefaultSymbolFill);
//!?!?!    ProcSetManSnapOrder(PProj(AData));
  end;


Procedure TConstruct.MakeSaveLayers
   (
   AData           : Pointer
   );
  begin
    HelpLayer6:=PProj(AData)^.Layers^.InsertLayer(HL6,RGBColors[c_Black],lt_Solid,RGBColors[c_Black],pt_NoPattern,ilLast,DefaultSymbolFill);
    {HelpLayer6^.GeneralMin:=0;
    HelpLayer6^.GeneralMax:=1;
    HelpLayer6^.SetState(sf_General,TRUE);}
    {HelpLayer6^.SetState(sf_LayerOff,TRUE);}
    HelpLayer7:=PProj(AData)^.Layers^.InsertLayer(HL7,RGBColors[c_Black],lt_Solid,RGBColors[c_Black],pt_Cross,ilTop,DefaultSymbolFill);
    {HelpLayer7^.GeneralMin:=0;
    HelpLayer7^.GeneralMax:=1;
    HelpLayer7^.SetState(sf_General,TRUE);}
    {HelpLayer7^.SetState(sf_LayerOff,TRUE);}
//!?!?!    ProcSetManSnapOrder(PProj(AData));
  end;

Procedure TConstruct.MakeHelpLayers1
   (
   AData           : Pointer
   );
  begin
    {if HelpLayer1 = NIL then}
    HelpLayer1:=PProj(AData)^.Layers^.InsertLayer(HL1,RGBColors[c_Black],lt_Solid,RGBColors[c_Black],pt_NoPattern,ilLast,DefaultSymbolFill);
    {HelpLayer1^.GeneralMin:=0;
    HelpLayer1^.GeneralMax:=1;
    HelpLayer1^.SetState(sf_General,TRUE);}
    {HelpLayer1^.SetState(sf_LayerOff,TRUE);}
    {if HelpLayer2 = NIL then}
    HelpLayer2:=PProj(AData)^.Layers^.InsertLayer(HL2,RGBColors[c_Black],lt_Solid,RGBColors[c_Black],pt_NoPattern,ilLast,DefaultSymbolFill);
    {HelpLayer2^.GeneralMin:=0;
    HelpLayer2^.GeneralMax:=1;
    HelpLayer2^.SetState(sf_General,TRUE);}
    {HelpLayer2^.SetState(sf_LayerOff,TRUE);}
    {if ConstructLayer = NIL then}
    ConstructLayer:=PProj(AData)^.Layers^.InsertLayer(CL1,RGBColors[c_Black],lt_Solid,RGBColors[c_Black],pt_DiagCross,ilTop,DefaultSymbolFill);
    ConstructLayer^.SetState(sf_Transparent,TRUE);
//!?!?!    ProcSetManSnapOrder(PProj(AData));
  end;

Procedure TConstruct.MakeHelpLayers2
   (
   AData           : Pointer
   );
  begin
    HelpLayer3:=PProj(AData)^.Layers^.InsertLayer(HL3,RGBColors[c_Black],lt_Solid,RGBColors[c_Black],pt_NoPattern,ilTop,DefaultSymbolFill);
    {HelpLayer3^.GeneralMin:=0;
    HelpLayer3^.GeneralMax:=1;
    HelpLayer3^.SetState(sf_General,TRUE);}
    {HelpLayer3^.SetState(sf_LayerOff,TRUE);}
    HelpLayer4:=PProj(AData)^.Layers^.InsertLayer(HL4,RGBColors[c_Black],lt_Solid,RGBColors[c_Black],pt_NoPattern,ilTop,DefaultSymbolFill);
    {HelpLayer4^.GeneralMin:=0;
    HelpLayer4^.GeneralMax:=1;
    HelpLayer4^.SetState(sf_General,TRUE);}
    {HelpLayer4^.SetState(sf_LayerOff,TRUE);}
    HelpLayer5:=PProj(AData)^.Layers^.InsertLayer(HL5,RGBColors[c_Black],lt_Solid,RGBColors[c_Black],pt_NoPattern,ilTop,DefaultSymbolFill);
    {HelpLayer5^.GeneralMin:=0;
    HelpLayer5^.GeneralMax:=1;
    HelpLayer5^.SetState(sf_General,TRUE);}
    {HelpLayer5^.SetState(sf_LayerOff,TRUE);}
    PProj(AData)^.Layers^.DetermineTopLayer;
//!?!?!    ProcSetManSnapOrder(PProj(AData));
  end;

Procedure TConstruct.DeleteHelpLayers
   (
   AData           : Pointer
   );
  begin
    PProj(AData)^.Layers^.DeleteLayer(HelpLayer1);
    PProj(AData)^.Layers^.DeleteLayer(HelpLayer2);
//!?!?!    ProcSetManSnapOrder(PProj(AData));
  end;

Procedure TConstruct.DeleteSaveLayers
   (
   AData           : Pointer
   );
  begin
    PProj(AData)^.Layers^.DeleteLayer(HelpLayer6);
    PProj(AData)^.Layers^.DeleteLayer(HelpLayer7);
//!?!?!    ProcSetManSnapOrder(PProj(AData));
  end;

Procedure TConstruct.DeleteHelpLayers1
   (
   AData           : Pointer
   );
  begin
    PProj(AData)^.Layers^.DeleteLayer(HelpLayer1);
    PProj(AData)^.Layers^.DeleteLayer(HelpLayer2);
    PProj(AData)^.Layers^.DeleteLayer(ConstructLayer);
//!?!?!    ProcSetManSnapOrder(PProj(AData));
  end;

Procedure TConstruct.DeleteHelpLayers2
   (
   AData           : Pointer
   );
  begin
    PProj(AData)^.Layers^.DeleteLayer(HelpLayer3);
    PProj(AData)^.Layers^.DeleteLayer(HelpLayer4);
    PProj(AData)^.Layers^.DeleteLayer(HelpLayer5);
//!?!?!    ProcSetManSnapOrder(PProj(AData));
  end;

Procedure TConstruct.CopyToHelpLayer
   (
   AData           : Pointer
   );
  Procedure DoAll
     (
     Item          : PIndex
     ); Far;
    var IItem      : PIndex;
    begin
      if Item^.GetObjType = ot_CPoly then begin
        IItem:=New(PIndex,Init(Item^.Index));
        HelpLayer1^.InsertObject(PProj(AData)^.PInfo,IItem,FALSE);
        PProj(AData)^.Layers^.IndexObject(PProj(AData)^.PInfo,Item)^.Invalidate(PProj(AData)^.PInfo);
      end;
    end;
  begin
    PProj(AData)^.Layers^.SelLayer^.Data^.ForEach(@DoAll);
  end;

Procedure TConstruct.CopyToHelpLayers
   (
   AData           : Pointer;
   SourceLayer     : PLayer;
   DestLayer       : PLayer
   );
  Procedure DoAll
     (
     Item          : PIndex
     ); Far;
    var IItem      : PIndex;
    begin
      if Item^.GetObjType = ot_CPoly then begin
        IItem:=New(PIndex,Init(Item^.Index));
        DestLayer^.InsertObject(PProj(AData)^.PInfo,IItem,FALSE);
        PProj(AData)^.Layers^.IndexObject(PProj(AData)^.PInfo,Item)^.Invalidate(PProj(AData)^.PInfo);
      end;
    end;
  begin
    SourceLayer^.Data^.ForEach(@DoAll);
  end;

Procedure TConstruct.SearchFreeArea
   (
   AData           : Pointer
   );
{$IFNDEF WMLT}
  var GesFl        : Double;
      Tmp          : String;
  Procedure TestArea
     (
     Item          : PIndex
     ); Far;
    var AObj       : PView;
    begin
      AObj:=HelpLayer2^.IndexObject(PProj(AData)^.PInfo,Item);
      GesFl:=GesFl+PCPoly(AObj)^.Flaeche;
    end;
  Procedure GetIndex
     (
     Item          : PIndex
     ); Far;
    begin
      {$IFNDEF AXDLL} // <----------------- AXDLL
      DDEHandler.AppendList(Item^.Index);
      {$ENDIF} // <----------------- AXDLL
    end;
{$ENDIF}
  begin
{$IFNDEF WMLT}
    if ConstructType = ConstructFS then AreaCut5(AData,HL1,FSLayer^.Text^,HL2)
    else if ConstructType = ConstructS then AreaCut5(AData,HL1,SLayer^.Text^,HL2);
    GesFl:=0;
    HelpLayer2^.Data^.ForEach(@TestArea);
    if GesFl > 0 then begin
      {$IFNDEF AXDLL} // <----------------- AXDLL
      if ConstructType = ConstructFS then DDEHandler.StartList('[NFS][   ]')
      else if ConstructType = ConstructS then DDEHandler.StartList('[NSC][   ]');
      HelpLayer1^.Data^.ForEach(@GetIndex);
      Str(GesFl:0:2,Tmp);
      DDEHandler.AppendString(Tmp);
      DDEHandler.EndList(FALSE);
      {$ENDIF} // <----------------- AXDLL
    end
    else begin
      if ConstructType = ConstructFS then MsgBox(PProj(AData)^.Parent.Handle,11358,mb_OK or mb_IconExclamation,'')
      else if ConstructType = ConstructS then MsgBox(PProj(AData)^.Parent.Handle,11378,mb_OK or mb_IconExclamation,'');
    end;
    {AData^.DeSelectAll(FALSE);}
{$ENDIF}
  end;

Procedure TConstruct.SetOldTop;
  begin
    if HelpLayer <> NIL then PProj(AData)^.Layers^.SetTopLayer(HelpLayer);
    PProj(AData)^.SetActualMenu(mn_Select);
  end;

Procedure TConstruct.CopyResult
   (
   AData           : Pointer
   );
  Procedure DoAll
     (
     Item          : PIndex
     ); Far;
    var IItem      : PIndex;
    begin
      IItem:=New(PIndex,Init(Item^.Index));
      DestiLayer^.InsertObject(PProj(AData)^.PInfo,IItem,FALSE);
      PProj(AData)^.Layers^.IndexObject(PProj(AData)^.PInfo,Item)^.Invalidate(PProj(AData)^.PInfo);
    end;
  begin
    if ErrorLayer^.Data^.GetCount = 0 then begin
      ResultLayer^.Data^.ForEach(@DoAll);
      HelpLayer^.Data^.DeleteAll;
      ConstructLayer^.Data^.DeleteAll;
      ResultLayer^.Data^.DeleteAll;
      ErrorLayer^.Data^.DeleteAll;
    end
    else begin
      MsgBox(PProj(AData)^.Parent.Handle,11505,mb_OK or mb_IconQuestion or mb_DefButton2,'');
      ErrorLayer^.Data^.DeleteAll;
    end;
  end;

Procedure TConstruct.PartArea
   (
   AData           : Pointer;
   AChild          : Pointer
   );
  var Dialog       : TAreaPartDlg;
      AreaSum      : Double;
      SLayerName   : Array[0..255] of Char;
      DLayerName   : Array[0..255] of Char;
      AreaNum      : Array[0..255] of Char;
      AName        : Array[0..255] of Char;
      Error        : Integer;
      TempString   : String;
      TempString1  : String;
      TempVal      : Integer;
      TempStr      : Array[0..255] of Char;
  Procedure SumAreas
     (
     Item          : PIndex
     ); Far;
    var AView      : PView;
    begin
      AView:=PLayer(PProj(AData)^.PInfo^.Objects)^.IndexObject(PProj(AData)^.PInfo,Item);
      AreaSum:=AreaSum+PCPoly(AView)^.Flaeche;
    end;
  Procedure AddIDs
     (
     Item          : PIndex
     ); Far;
    var TmpString  : String;
    begin
      Str(Item^.Index:10,TmpString);
      TempString:=TempString+TmpString+'][';
    end;
  begin
    StrPCopy(SLayerName,PProj(AData)^.Layers^.TopLayer^.Text^);
    StrCopy(DLayerName,'');
    StrCopy(AName,'');
    AreaSum:=0;
    PProj(AData)^.Layers^.SelLayer^.Data^.ForEach(@SumAreas);
    Str(AreaSum:0:2,AreaNum);
    Dialog:=TAreaPartDlg.Init(PProj(AData)^.Parent,PProj(AData)^.Layers,SLayerName,DLayerName,AreaNum,AName,
                PProj(AData)^.PInfo^.ProjStyles);
    if ExecDialog(Dialog)=id_OK then begin
      OldGSL:=GSLayer;
      OldFSL:=FSLayer;
      GSLayer:=PProj(AData)^.Layers^.NameToLayer(StrPas(SLayerName));
      FSLayer:=PProj(AData)^.Layers^.NameToLayer(StrPas(DLayerName));
      if DDEData <> NIL then StrDispose(DDEData);
      TempString:='[FSC][';
      TempVal:=PProj(AData)^.Layers^.SelLayer^.Data^.GetCount+3;
      Str(TempVal:3,TempString1);
      TempString:=TempString+TempString1+']['+StrPas(AName)+']['+StrPas(AreaNum)+'][';
      PProj(AData)^.Layers^.SelLayer^.Data^.ForEach(@AddIDs);
      TempString:=TempString+'1]';
      StrPCopy(TempStr,TempString);
      DDEData:=StrNew(TempStr);
      PostMessage(TMDIChild(AChild).Handle,wm_Command,cm_ConstructFS,0);
      GraphicMode:=TRUE;
    end;
  end;

{**************************************** TConstructDlg ****************************************}

Constructor TConstructDlg.Init
   (
   AParent         : TComponent;
   AProj           : Pointer;
   AType           : PByte;
   A1Area          : Double;
   A2Area          : Double;
   Info            : String
   );
  begin
    Inherited Init(PProj(AProj)^.Parent,'CONSTRUCTIONS');
    BProj:=AProj;
    BType:=AType;
    WArea:=A1Area;
    RArea:=A2Area;
    StrPCopy(InfoStr,Info);
  end;

Procedure TConstructDlg.SetupWindow;
  var TempStr      : Array[0..30] of Char;
  begin
    inherited SetupWindow;
    CheckDlgButton(Handle,id_ConstrOK,1);
    if (IniFile^.AreaFact = 100) or (IniFile^.AreaFact = 10000) or (IniFile^.AreaFact = 1000000) then
      WArea:=WArea/IniFile^.AreaFact;
    Str(WArea:0:2,TempStr);
    SetDlgItemText(Handle,id_WArea,TempStr);
    if not((IniFile^.AreaFact = 100) or (IniFile^.AreaFact = 10000) or (IniFile^.AreaFact = 1000000)) then
      RArea:=RArea*IniFile^.AreaFact;
    Str(RArea:0:2,TempStr);
    SetDlgItemText(Handle,id_RArea,TempStr);
    SetDlgItemText(Handle,id_CInfo,InfoStr);
    if IniFile^.AreaFact = 100 then begin
      ShowWindow(GetItemHandle(id_km2_1),sw_Hide);
      ShowWindow(GetItemHandle(id_ha_1),sw_Hide);
      ShowWindow(GetItemHandle(id_m2_1),sw_Hide);
      ShowWindow(GetItemHandle(id_km2_2),sw_Hide);
      ShowWindow(GetItemHandle(id_ha_2),sw_Hide);
      ShowWindow(GetItemHandle(id_m2_2),sw_Hide);
    end
    else if IniFile^.AreaFact = 10000 then begin
      ShowWindow(GetItemHandle(id_km2_1),sw_Hide);
      ShowWindow(GetItemHandle(id_a_1),sw_Hide);
      ShowWindow(GetItemHandle(id_m2_1),sw_Hide);
      ShowWindow(GetItemHandle(id_km2_2),sw_Hide);
      ShowWindow(GetItemHandle(id_a_2),sw_Hide);
      ShowWindow(GetItemHandle(id_m2_2),sw_Hide);
    end
    else if IniFile^.AreaFact = 1000000 then begin
      ShowWindow(GetItemHandle(id_ha_1),sw_Hide);
      ShowWindow(GetItemHandle(id_a_1),sw_Hide);
      ShowWindow(GetItemHandle(id_m2_1),sw_Hide);
      ShowWindow(GetItemHandle(id_ha_2),sw_Hide);
      ShowWindow(GetItemHandle(id_a_2),sw_Hide);
      ShowWindow(GetItemHandle(id_m2_2),sw_Hide);
    end
    else begin
      ShowWindow(GetItemHandle(id_km2_1),sw_Hide);
      ShowWindow(GetItemHandle(id_ha_1),sw_Hide);
      ShowWindow(GetItemHandle(id_a_1),sw_Hide);
      ShowWindow(GetItemHandle(id_km2_2),sw_Hide);
      ShowWindow(GetItemHandle(id_ha_2),sw_Hide);
      ShowWindow(GetItemHandle(id_a_2),sw_Hide);
    end;
  end;

Function TConstructDlg.CanClose
   : Boolean;
  begin
    CanClose:=TRUE;
    if IsDlgButtonChecked(Handle,id_MovePoint) <> 0 then BType^:=1
    else if IsDlgButtonChecked(Handle,id_InsPoint) <> 0 then BType^:=2
    else if IsDlgButtonChecked(Handle,id_DelPoint) <> 0 then BType^:=3
    else if IsDlgButtonChecked(Handle,id_Parallel) <> 0 then BType^:=4
    else if IsDlgButtonChecked(Handle,id_Buffer) <> 0 then BType^:=5
    else if IsDlgButtonChecked(Handle,id_Island) <> 0 then BType^:=6
    else PProj(BProj)^.SetActualMenu(mn_Select);
  end;

Procedure TConstructDlg.DoConstr
   (
   var Msg         : TMessage
   );
  begin
    PProj(BProj)^.SetActualMenu(mn_InsPoint);
  end;

{**************************************** TCAreaDlg ****************************************}

Constructor TCAreaDlg.Init
   (
   AParent         : TComponent;
   A1Area          : Double;
   A2Area          : Double;
   A3Area          : Double;
   Info            : String
   );
  begin
    Inherited Init(AParent,'CONSTRUCTIONSINFO');
    WArea:=A1Area;
    RArea:=A2Area;
    CArea:=A3Area;
    StrPCopy(InfoStr,Info);
  end;

Procedure TCAreaDlg.SetupWindow;
  var TempStr      : Array[0..30] of Char;
  begin
    inherited SetupWindow;
    if (IniFile^.AreaFact = 100) or (IniFile^.AreaFact = 10000) or (IniFile^.AreaFact = 1000000) then
      WArea:=WArea/IniFile^.AreaFact;
    Str(WArea:0:2,TempStr);
    SetDlgItemText(Handle,id_WArea,TempStr);
    if not((IniFile^.AreaFact = 100) or (IniFile^.AreaFact = 10000) or (IniFile^.AreaFact = 1000000)) then
      RArea:=RArea*IniFile^.AreaFact;
    Str(RArea:0:2,TempStr);
    SetDlgItemText(Handle,id_RArea,TempStr);
    if not((IniFile^.AreaFact = 100) or (IniFile^.AreaFact = 10000) or (IniFile^.AreaFact = 1000000)) then
      CArea:=CArea*IniFile^.AreaFact;
    Str(CArea:0:2,TempStr);
    SetDlgItemText(Handle,id_CArea,TempStr);
    SetDlgItemText(Handle,id_CInfo,InfoStr);
    if IniFile^.AreaFact = 100 then begin
      ShowWindow(GetItemHandle(id_km2_1),sw_Hide);
      ShowWindow(GetItemHandle(id_ha_1),sw_Hide);
      ShowWindow(GetItemHandle(id_m2_1),sw_Hide);
      ShowWindow(GetItemHandle(id_km2_2),sw_Hide);
      ShowWindow(GetItemHandle(id_ha_2),sw_Hide);
      ShowWindow(GetItemHandle(id_m2_2),sw_Hide);
      ShowWindow(GetItemHandle(id_km2_3),sw_Hide);
      ShowWindow(GetItemHandle(id_ha_3),sw_Hide);
      ShowWindow(GetItemHandle(id_m2_3),sw_Hide);
    end
    else if IniFile^.AreaFact = 10000 then begin
      ShowWindow(GetItemHandle(id_km2_1),sw_Hide);
      ShowWindow(GetItemHandle(id_a_1),sw_Hide);
      ShowWindow(GetItemHandle(id_m2_1),sw_Hide);
      ShowWindow(GetItemHandle(id_km2_2),sw_Hide);
      ShowWindow(GetItemHandle(id_a_2),sw_Hide);
      ShowWindow(GetItemHandle(id_m2_2),sw_Hide);
      ShowWindow(GetItemHandle(id_km2_3),sw_Hide);
      ShowWindow(GetItemHandle(id_a_3),sw_Hide);
      ShowWindow(GetItemHandle(id_m2_3),sw_Hide);
    end
    else if IniFile^.AreaFact = 1000000 then begin
      ShowWindow(GetItemHandle(id_ha_1),sw_Hide);
      ShowWindow(GetItemHandle(id_a_1),sw_Hide);
      ShowWindow(GetItemHandle(id_m2_1),sw_Hide);
      ShowWindow(GetItemHandle(id_ha_2),sw_Hide);
      ShowWindow(GetItemHandle(id_a_2),sw_Hide);
      ShowWindow(GetItemHandle(id_m2_2),sw_Hide);
      ShowWindow(GetItemHandle(id_ha_3),sw_Hide);
      ShowWindow(GetItemHandle(id_a_3),sw_Hide);
      ShowWindow(GetItemHandle(id_m2_3),sw_Hide);
    end
    else begin
      ShowWindow(GetItemHandle(id_km2_1),sw_Hide);
      ShowWindow(GetItemHandle(id_ha_1),sw_Hide);
      ShowWindow(GetItemHandle(id_a_1),sw_Hide);
      ShowWindow(GetItemHandle(id_km2_2),sw_Hide);
      ShowWindow(GetItemHandle(id_ha_2),sw_Hide);
      ShowWindow(GetItemHandle(id_a_2),sw_Hide);
      ShowWindow(GetItemHandle(id_km2_3),sw_Hide);
      ShowWindow(GetItemHandle(id_ha_3),sw_Hide);
      ShowWindow(GetItemHandle(id_a_3),sw_Hide);
    end;
  end;

Procedure TCAreaDlg.ConsRetryClick
   (
   var Msg         : TMessage
   );
  begin
    SendMessage(Parent.Handle,wm_Command,cm_SetRetry,0);
    PostMessage(Handle,wm_Close,0,0);
  end;

Function TCAreaDlg.CanClose;
  begin
    CanClose:=TRUE;
  end;

{**************************************** TCParAreaDlg ****************************************}

Constructor TCParAreaDlg.Init
   (
   AParent         : TComponent;
   AreaData        : PChar;
   RAreaData       : PChar;
   AAddOrDel       : PByte;
   ARetour         : PBool;
   Info            : String
   );
  var Error        : Integer;
  begin
    Inherited Init(AParent,'CONSTRUCTIONSAREA');
    AreaDat:=AreaData;
    RAreaDat:=RAreaData;
    Val(RAreaDat,MaxVal,Error);
    GoRetour:=ARetour;
    GoRetour^:=FALSE;
    DoAddOrDel:=AAddOrDel;
    StrPCopy(InfoStr,Info);
  end;

Procedure TCParAreaDlg.SetupWindow;
  var WArea        : Double;
      RArea        : Double;
      Error        : Integer;
      TempStr      : Array[0..30] of Char;
  begin
    inherited SetupWindow;
    Val(RAreaDat,RArea,Error);
    if not((IniFile^.AreaFact = 100) or (IniFile^.AreaFact = 10000) or (IniFile^.AreaFact = 1000000)) then
      RArea:=RArea*IniFile^.AreaFact;
    Str(RArea:0:2,TempStr);
    SetDlgItemText(Handle,id_ActArea,TempStr);
    Val(AreaDat,WArea,Error);
    if (IniFile^.AreaFact = 100) or (IniFile^.AreaFact = 10000) or (IniFile^.AreaFact = 1000000) then
      WArea:=WArea/IniFile^.AreaFact;
    Str(WArea:0:2,TempStr);
    SetDlgItemText(Handle,id_ParArea,TempStr);
    CheckDlgButton(Handle,id_GetArea,1);
    SetDlgItemText(Handle,id_CInfo,InfoStr);
    if IniFile^.AreaFact = 100 then begin
      ShowWindow(GetItemHandle(id_km2_1),sw_Hide);
      ShowWindow(GetItemHandle(id_ha_1),sw_Hide);
      ShowWindow(GetItemHandle(id_m2_1),sw_Hide);
      ShowWindow(GetItemHandle(id_km2_2),sw_Hide);
      ShowWindow(GetItemHandle(id_ha_2),sw_Hide);
      ShowWindow(GetItemHandle(id_m2_2),sw_Hide);
    end
    else if IniFile^.AreaFact = 10000 then begin
      ShowWindow(GetItemHandle(id_km2_1),sw_Hide);
      ShowWindow(GetItemHandle(id_a_1),sw_Hide);
      ShowWindow(GetItemHandle(id_m2_1),sw_Hide);
      ShowWindow(GetItemHandle(id_km2_2),sw_Hide);
      ShowWindow(GetItemHandle(id_a_2),sw_Hide);
      ShowWindow(GetItemHandle(id_m2_2),sw_Hide);
    end
    else if IniFile^.AreaFact = 1000000 then begin
      ShowWindow(GetItemHandle(id_ha_1),sw_Hide);
      ShowWindow(GetItemHandle(id_a_1),sw_Hide);
      ShowWindow(GetItemHandle(id_m2_1),sw_Hide);
      ShowWindow(GetItemHandle(id_ha_2),sw_Hide);
      ShowWindow(GetItemHandle(id_a_2),sw_Hide);
      ShowWindow(GetItemHandle(id_m2_2),sw_Hide);
    end
    else begin
      ShowWindow(GetItemHandle(id_km2_1),sw_Hide);
      ShowWindow(GetItemHandle(id_ha_1),sw_Hide);
      ShowWindow(GetItemHandle(id_a_1),sw_Hide);
      ShowWindow(GetItemHandle(id_km2_2),sw_Hide);
      ShowWindow(GetItemHandle(id_ha_2),sw_Hide);
      ShowWindow(GetItemHandle(id_a_2),sw_Hide);
    end
  end;

Function TCParAreaDlg.CanClose;
  var TempStr      : Array[0..30] of Char;
      TempVal      : Double;
      Error        : Integer;
  begin
    CanClose:=FALSE;
    GetDlgItemText(Handle,id_ParArea,@TempStr,30);
    Val(TempStr,TempVal,Error);
    if (Error <> 0) then MsgBox(Handle,11376,mb_OK or mb_IconExclamation,'')
    else begin
      if (TempVal <= 0) then MsgBox(Handle,11385,mb_OK or mb_IconExclamation,'')
      else if (TempVal >= MaxVal) then MsgBox(Handle,11386,mb_OK or mb_IconExclamation,'')
      else begin
        CanClose:=TRUE;
        StrCopy(AreaDat,TempStr);
        if IsDlgButtonChecked(Handle,id_GetArea) <> 0 then DoAddOrDel^:=1
        else if IsDlgButtonChecked(Handle,id_CutArea) <> 0 then DoAddOrDel^:=2;
      end;
    end;
  end;

Procedure TCParAreaDlg.CParReturn
   (
   var Msg         : TMessage
   );
  begin
    GoRetour^:=TRUE;
    PostMessage(Handle,wm_Close,0,0);
  end;

{**************************************** TCBufAreaDlg ****************************************}

Constructor TCBufAreaDlg.Init
   (
   AParent         : TComponent;
   AreaData        : PChar;
   RAreaData       : PChar;
   AAddOrDel       : PByte;
   ADrawOrSel      : PByte;
   ARetour         : PBool;
   Info            : String
   );
  var Error        : Integer;
  begin
    Inherited Init(AParent,'CONSTRUCTIONSBUFF');
    AreaDat:=AreaData;
    RAreaDat:=RAreaData;
    GoRetour:=ARetour;
    GoRetour^:=FALSE;
    DoAddOrDel:=AAddOrDel;
    DrawOrSel:=ADrawOrSel;
    StrPCopy(InfoStr,Info);
  end;

Procedure TCBufAreaDlg.SetupWindow;
  var WArea        : Double;
      RArea        : Double;
      Error        : Integer;
      TempStr      : Array[0..30] of Char;
  begin
    inherited SetupWindow;
    Val(RAreaDat,RArea,Error);
    if not((IniFile^.AreaFact = 100) or (IniFile^.AreaFact = 10000) or (IniFile^.AreaFact = 1000000)) then
      RArea:=RArea*IniFile^.AreaFact;
    Str(RArea:0:2,TempStr);
    SetDlgItemText(Handle,id_ActArea,TempStr);
    Val(AreaDat,WArea,Error);
    if (IniFile^.AreaFact = 100) or (IniFile^.AreaFact = 10000) or (IniFile^.AreaFact = 1000000) then
      WArea:=WArea/IniFile^.AreaFact;
    Str(WArea:0:2,TempStr);
    SetDlgItemText(Handle,id_ParArea,TempStr);
    CheckDlgButton(Handle,id_CutArea,1);          {id_CutArea  id_GetArea}
    CheckDlgButton(Handle,id_DrawLine,1);
    SetDlgItemText(Handle,id_CInfo,InfoStr);
    if IniFile^.AreaFact = 100 then begin
      ShowWindow(GetItemHandle(id_km2_1),sw_Hide);
      ShowWindow(GetItemHandle(id_ha_1),sw_Hide);
      ShowWindow(GetItemHandle(id_m2_1),sw_Hide);
      ShowWindow(GetItemHandle(id_km2_2),sw_Hide);
      ShowWindow(GetItemHandle(id_ha_2),sw_Hide);
      ShowWindow(GetItemHandle(id_m2_2),sw_Hide);
    end
    else if IniFile^.AreaFact = 10000 then begin
      ShowWindow(GetItemHandle(id_km2_1),sw_Hide);
      ShowWindow(GetItemHandle(id_a_1),sw_Hide);
      ShowWindow(GetItemHandle(id_m2_1),sw_Hide);
      ShowWindow(GetItemHandle(id_km2_2),sw_Hide);
      ShowWindow(GetItemHandle(id_a_2),sw_Hide);
      ShowWindow(GetItemHandle(id_m2_2),sw_Hide);
    end
    else if IniFile^.AreaFact = 1000000 then begin
      ShowWindow(GetItemHandle(id_ha_1),sw_Hide);
      ShowWindow(GetItemHandle(id_a_1),sw_Hide);
      ShowWindow(GetItemHandle(id_m2_1),sw_Hide);
      ShowWindow(GetItemHandle(id_ha_2),sw_Hide);
      ShowWindow(GetItemHandle(id_a_2),sw_Hide);
      ShowWindow(GetItemHandle(id_m2_2),sw_Hide);
    end
    else begin
      ShowWindow(GetItemHandle(id_km2_1),sw_Hide);
      ShowWindow(GetItemHandle(id_ha_1),sw_Hide);
      ShowWindow(GetItemHandle(id_a_1),sw_Hide);
      ShowWindow(GetItemHandle(id_km2_2),sw_Hide);
      ShowWindow(GetItemHandle(id_ha_2),sw_Hide);
      ShowWindow(GetItemHandle(id_a_2),sw_Hide);
    end
  end;

Function TCBufAreaDlg.CanClose;
  var TempStr      : Array[0..30] of Char;
      TempVal      : Double;
      Error        : Integer;
  begin
    CanClose:=FALSE;
    GetDlgItemText(Handle,id_BufferDim,@TempStr,30);
    Val(TempStr,TempVal,Error);
    if (Error <> 0) then MsgBox(Handle,11376,mb_OK or mb_IconExclamation,'')
    else begin
      if (TempVal <= 0) then MsgBox(Handle,11403,mb_OK or mb_IconExclamation,'')
      else begin
        CanClose:=TRUE;
        StrCopy(AreaDat,TempStr);
        if IsDlgButtonChecked(Handle,id_GetArea) <> 0 then DoAddOrDel^:=1
        else if IsDlgButtonChecked(Handle,id_CutArea) <> 0 then DoAddOrDel^:=2;
        if IsDlgButtonChecked(Handle,id_DrawLine) <> 0 then DrawOrSel^:=1
        else if IsDlgButtonChecked(Handle,id_SelLine) <> 0 then DrawOrSel^:=2;
      end;
    end;
  end;

Procedure TCBufAreaDlg.CBufReturn
   (
   var Msg         : TMessage
   );
  begin
    GoRetour^:=TRUE;
    PostMessage(Handle,wm_Close,0,0);
  end;

{**************************************** TAreaPartDlg ****************************************}

Constructor TAreaPartDlg.Init
   (
   AParent         : TComponent;
   ALayers         : PLayers;
   SAreaNam        : PChar;
   DAreaNam        : PChar;
   AreaNum         : PChar;
   AreaNam         : PChar;
   ProjectStyles   : TProjectStyles
   );
  begin
    inherited Init(AParent,'AREAPART',ALayers,ProjectStyles);
    SAreaName:=SAreaNam;
    DAreaName:=DAreaNam;
    AreaSize:=AreaNum;
    AreaName:=AreaNam;
  end;

Procedure TAreaPartDlg.SetupWindow;
  begin
    inherited SetupWindow;
    FillLayerList(id_DestLayer,TRUE,TRUE);
    SetDlgItemText(Handle,id_SourLayer,SAreaName);
    SetDlgItemText(Handle,id_AreaName,AreaName);
    {if not((IniFile^.AreaFact = 100) or (IniFile^.AreaFact = 10000) or (IniFile^.AreaFact = 1000000)) then
      RArea:=RArea*IniFile^.AreaFact;}
    SetDlgItemText(Handle,id_DestArea,AreaSize);
    if IniFile^.AreaFact = 100 then begin
      ShowWindow(GetItemHandle(id_km2_2),sw_Hide);
      ShowWindow(GetItemHandle(id_ha_2),sw_Hide);
      ShowWindow(GetItemHandle(id_m2_2),sw_Hide);
    end
    else if IniFile^.AreaFact = 10000 then begin
      ShowWindow(GetItemHandle(id_km2_2),sw_Hide);
      ShowWindow(GetItemHandle(id_a_2),sw_Hide);
      ShowWindow(GetItemHandle(id_m2_2),sw_Hide);
    end
    else if IniFile^.AreaFact = 1000000 then begin
      ShowWindow(GetItemHandle(id_ha_2),sw_Hide);
      ShowWindow(GetItemHandle(id_a_2),sw_Hide);
      ShowWindow(GetItemHandle(id_m2_2),sw_Hide);
    end
    else begin
      ShowWindow(GetItemHandle(id_km2_2),sw_Hide);
      ShowWindow(GetItemHandle(id_ha_2),sw_Hide);
      ShowWindow(GetItemHandle(id_a_2),sw_Hide);
    end
  end;

Function TAreaPartDlg.CanClose
   : Boolean;
  var AName        : Array[0..255] of Char;
      AVal         : Double;
      CompVal      : Double;
      Error        : Integer;
  begin
    Result:=FALSE;
    GetDlgItemText(Handle,id_DestLayer,AName,SizeOf(AName));
    if StrComp(AName,'') = 0 then begin
      MsgBox(Handle,11417,mb_OK or mb_IconExclamation,'');
      SetFocus(GetItemHandle(id_DestLayer));
    end
    else begin
      if StrComp(AName,SAreaName) = 0 then begin
        MsgBox(Handle,11418,mb_OK or mb_IconExclamation,'');
        SetFocus(GetItemHandle(id_DestLayer));
      end
      else begin
        StrCopy(DAreaName,AName);
        Val(AreaSize,CompVal,Error);
        GetDlgItemText(Handle,id_DestArea,AName,SizeOf(AName));
        Val(AName,AVal,Error);
        if (Error <> 0) or (AVal <= 0) or (AVal > CompVal) then begin
          MsgBox(Handle,11419,mb_OK or mb_IconExclamation,'');
          SetFocus(GetItemHandle(id_DestArea));
        end
        else begin
          StrCopy(AreaSize,AName);
          GetDlgItemText(Handle,id_AreaName,AName,SizeOf(AName));
          StrCopy(AreaName,AName);
          Result:=TRUE;
        end;
      end;
    end;
  end;

end.

