{****************************************************************************}
{ Unit                                                                       }
{----------------------------------------------------------------------------}
{                                                                            }
{----------------------------------------------------------------------------}
{ Modifikationen                                                             }
{  16.02.1994 Martin Forst                                                   }
{****************************************************************************}
unit AM_Sel;

interface

uses WinTypes, WinProcs, AM_Def, AM_Paint, AM_Layer, AM_View,
  AM_Index, Objects, SelInfo, Messages;

type
  PSelect = ^TSelect;
  TSelect = object(TLayer)
    TranspMode: Integer;
    SelInfo: TSelectInfo;
//       SelOrderList     : TSelOrderList;
    constructor Init(AParent: PLayer);
    constructor Load(S: TOldStream);
    destructor Done; virtual;
    procedure DeleteAll;
    function DeleteIndex(PInfo: PPaint; AItem: PIndex): Boolean; virtual;
    function DeleteIndexFast(PInfo: PPaint; AItem: PIndex): Boolean;
    function DeleteObjectFast(PInfo: PPaint; AItem: PIndex): Boolean; virtual;
    function Draw(PInfo: PPaint): Boolean; virtual;
    function InsertObject(PInfo: PPaint; Item: PIndex; Paint: Boolean): Boolean; virtual;
    function InsertObjectFast(PInfo: PPaint; Item: PIndex): Boolean;
    function InsertObjectLineFast(ARect: TDRect; PInfo: PPaint; Item: PIndex; Paint: Boolean): Boolean; virtual;
    procedure Store(S: TOldStream); virtual;
  end;

implementation

uses AM_Ini, AM_Proj, MenuFn, Palette, AM_Coll, UserIntf, ExtCanvas, ToolTips,
  uview, usellst, uheap, AM_Main;

constructor TSelect.Init
  (
  AParent: PLayer
  );
begin
  inherited Init(AParent);
  SetStyle(RGBColors[c_LBlue], lt_Dot, 0, clNone, 0, nil, DefaultSymbolFill);
  SelInfo := CreateSelectInfo;
//    SelOrderList:=TSelOrderList.Create;
  TranspMode := 0;
end;

constructor TSelect.Load
  (
  S: TOldStream
  );
begin
  Init(nil);
end;

destructor TSelect.Done;
begin
  SelInfo.Free;
//    SelOrderList.Free;
  inherited Done;
end;

function TSelect.InsertObject
  (
  PInfo: PPaint;
  Item: PIndex;
  Paint: Boolean
  )
  : Boolean;
var
  InsPos: LongInt;
  Mode: Word;
  ObjStyle: PObjStyleV2;
begin
  if (PProj(PInfo^.AProj)^.Layers^.TranspLayer and (PInfo^.SelectionSettings.TransparentMode and $04 <> 0)
    and (PInfo^.SelectionSettings.TransparentMode and $80 <> 0)) or (not Data^.Search(Item, InsPos)) then
  begin
    PProj(PInfo^.AProj)^.CorrectSelAllRect(Item^.ClipRect);
    Data^.Insert(Item);
    if Paint then
    begin
        // selected objects are not drawn into the bitmap-cache
      PInfo^.ExtCanvas.Push;
      PInfo^.ExtCanvas.CachedDraw := FALSE;
      try
        if PInfo^.EditPoly then
        begin
          Mode := dm_SelDesel + dm_SelGra + dm_EditPoly + dm_NoPattern;
          PInfo^.SetLineStyle(LineStyle, TRUE);
          PInfo^.SetFillStyle(FillStyle, TRUE);
        end
        else
        begin
          Mode := dm_SelDesel + dm_SelGra;
          PInfo^.SetLineStyle(PInfo^.SelectionSettings.LineStyle, TRUE);
          PInfo^.SetFillStyle(PInfo^.SelectionSettings.FillStyle, TRUE);
        end;
        PInfo^.SetSymbolFill(DefaultSymbolFill);
        PInfo^.BeginPaint(0, 0, Mode, -1);
          {++ Sygsky: enable select object with select style redrawing }
        ObjStyle := Item.ObjectStyle; { Save personal object style}
        Item.ObjectStyle := nil; { Let select layer to draw with itsstyle}
        DrawObject(PInfo, Item, Item^.NeedClipping(PInfo));
        Item.ObjectStyle := ObjStyle; { Restore  style in case to able paste with style)}
          {--Sygsky }
      finally
        PInfo^.ExtCanvas.Pop;
      end;
      TooltipThread.DrawFloatingTextsForObject(PInfo, Item^.Index);
    end;
    InsertObject := TRUE;
    PProj(PInfo^.AProj)^.SelList^.Insert(Item);
    SelInfo.Add(IndexObject(PInfo, Item));
//      SelOrderList.Append(Item^.Index);
    UserInterface.Update([uiMenus, uiStatusBar]);
  end
  else
    InsertObject := FALSE;
end;

function TSelect.InsertObjectFast
  (
  PInfo: PPaint;
  Item: PIndex
  )
  : Boolean;
var
  InsPos: LongInt;
  AIndex: PIndex;
  AView: PView;
begin
  if not Data^.Search(Item, InsPos) then
  begin
    AView := IndexObject(PInfo, Item);
    if AView <> nil then
    begin
      AIndex := New(PIndex, Init(Item^.Index));
      AIndex^.ClipRect.InitByRect(Item^.ClipRect);
      AIndex^.MPtr := Item^.MPtr;
      if Item.ObjectStyle <> nil then
        Item.CopyObjectStyleTo(AIndex); {++Sygsky}
      Data^.AtInsert(InsPos, AIndex);
      SelInfo.Add(AView);
//      SelOrderList.Append(Item^.Index);
      PProj(PInfo^.AProj)^.SelList^.Insert(AIndex);
      Result := TRUE;
    end;
  end
  else
    Result := FALSE;
end;

function TSelect.InsertObjectLineFast
  (
  ARect: TDRect;
  PInfo: PPaint;
  Item: PIndex;
  Paint: Boolean
  )
  : Boolean;
var
  InsPos: LongInt;
  AIndex: PIndex;
begin
  if not Data^.Search(Item, InsPos) then
  begin
    AIndex := New(PIndex, Init(Item^.Index));
    AIndex^.ClipRect.InitByRect(Item^.ClipRect);
    AIndex^.MPtr := Item^.MPtr;
    Item.CopyObjectStyleTo(AIndex); {++Sygsky}
    Data^.AtInsert(InsPos, AIndex);
    PProj(PInfo^.AProj)^.SelList^.Insert(AIndex);
//      SelOrderList.Append(Item^.Index);
    InsertObjectLineFast := TRUE;
  end
  else
    InsertObjectLineFast := FALSE;
end;

function TSelect.DeleteIndexFast
  (
  PInfo: PPaint;
  AItem: PIndex
  )
  : Boolean;
begin
  if AItem <> nil then
  begin
    if PProj(PInfo^.AProj)^.ShowNum > 0 then
      Dec(PProj(PInfo^.AProj)^.ShowNum)
    else
      PProj(PInfo^.AProj)^.ShowNum := Data^.GetCount - 1;
    DeleteIndexFast := TRUE;
  end
  else
    DeleteIndexFast := FALSE;
end;

function TSelect.DeleteIndex
  (
  PInfo: PPaint;
  AItem: PIndex
  )
  : Boolean;
var
  DelInd: LongInt;
  FirstView: CView;
begin
  if Data^.Search(AItem, DelInd) then
  begin
    if SelInfo.Count = 2 then
    begin
      if Data^.GetCount = 2 then
      begin
          //remaining object is hsp
        if DelInd = 0 then
          SelInfo.Delete(IndexObject(PInfo, AItem), IndexObject(PInfo, Data^.At(1)), nil)
        else
          SelInfo.Delete(IndexObject(PInfo, AItem), IndexObject(PInfo, Data^.At(0)), nil);
      end
      else
      begin
          //remaining object is hsd
        FirstView := TSelList(GetHeapMan.SelList).Getfirst;
        Selinfo.Delete(IndexObject(PInfo, AItem), nil, FirstView);
      end;
    end
    else
    begin
      SelInfo.Delete(IndexObject(PInfo, AItem), nil, nil);
    end;
//      SelOrderList.Delete(AItem^.Index);
    PProj(PInfo^.AProj)^.SelList^.Delete(AItem);
    if PProj(PInfo^.AProj)^.ShowNum > 0 then
      Dec(PProj(PInfo^.AProj)^.ShowNum)
    else
      PProj(PInfo^.AProj)^.ShowNum := Data^.GetCount - 1;
    Data^.AtFree(DelInd);
    DeleteIndex := TRUE;
    UserInterface.Update([uiMenus, uiStatusBar]);
  end
  else
    DeleteIndex := FALSE;
end;

function TSelect.DeleteObjectFast
  (
  PInfo: PPaint;
  AItem: PIndex
  )
  : Boolean;
var
  DelInd: LongInt;
  FirstView: CView;
begin
  if Data^.Search(AItem, DelInd) then
  begin
    if SelInfo.Count = 2 then
    begin
      if Data^.GetCount = 2 then
      begin
          //remaining object is hsp
        if DelInd = 0 then
          SelInfo.Delete(IndexObject(PInfo, AItem), IndexObject(PInfo, Data^.At(1)), nil)
        else
          SelInfo.Delete(IndexObject(PInfo, AItem), IndexObject(PInfo, Data^.At(0)), nil);
      end
      else
      begin
          //remaining object is hsd
        FirstView := TSelList(GetHeapMan.SelList).GetFirst;
        SelInfo.Delete(IndexObject(PInfo, AItem), nil, FirstView);
      end;
    end
    else
      SelInfo.Delete(IndexObject(PInfo, AItem), nil, nil);
//      SelOrderList.Delete(AItem^.Index);
    PProj(PInfo^.AProj)^.SelList^.Delete(AItem);
    if PProj(PInfo^.AProj)^.ShowNum > 0 then
      Dec(PProj(PInfo^.AProj)^.ShowNum)
    else
      PProj(PInfo^.AProj)^.ShowNum := Data^.GetCount - 1;
    Data^.AtFree(DelInd);
    Result := TRUE;
  end
  else
    Result := FALSE;
end;

procedure TSelect.Store
  (
  S: TOldStream
  );
begin
end;

procedure TSelect.DeleteAll;
begin
  Data^.FreeAll;
  SelInfo.Clear;
//    SelOrderList.Clear;
{$IFNDEF AXDLL} // <----------------- AXDLL
  UserInterface.Update([uiMenus, uiStatusBar]);
{$ENDIF} // <----------------- AXDLL
end;

function TSelect.Draw
  (
  PInfo: PPaint
  )
  : Boolean;
var
  SelLayerData: PViewColl;

  function DoAll
      (
      Item: PIndex
      )
      : Boolean; far;
  var
    Msg: TMsg;
    ObjStyle: PObjStyleV2;
  begin
    DoAll := FALSE;
    if (Item <> nil) and ((Item^.Visible(PInfo))) then
    begin {++ Sygsky: enable select object with select style redrawing }
      ObjStyle := Item.ObjectStyle; { Save personal object style}
      Item.ObjectStyle := nil; { Let select layer to draw with itsstyle}
      DrawObject(PInfo, Item, Item^.NeedClipping(PInfo));
      Item.ObjectStyle := ObjStyle; { Restore  style in case to able paste with style)}
    end;

    if not CoreConnected then
    begin
      if PeekMessage(Msg, 0, 0, 0, pm_NoRemove or pm_NoYield) then
        with Msg do
        begin
          if (Message = wm_LButtonUp) then
            SendMessage(PInfo^.HWindow, wm_CancelMode, 0, 0);
          if (Message = wm_LButtonDown)
            or (Message = wm_RButtonDown)
            or (Message = wm_NCLButtonDown)
            or (Message = wm_NCRButtonDown) then
          begin
            Result := TRUE;
          end
          else
          begin
            PeekMessage(Msg, 0, 0, 0, pm_Remove or pm_NoYield);
            if (Message = wm_KeyDown) and (wParam = vk_Escape) then
              Result := TRUE;
          end;
        end;
    end;
  end;
begin
    // selected objects are not drawn into the bitmap-cache
  PInfo^.ExtCanvas.Push;
  PInfo^.ExtCanvas.CachedDraw := FALSE;
  try
    if PInfo^.EditPoly then
    begin
      PInfo^.SetLineStyle(LineStyle, TRUE);
      PInfo^.SetFillStyle(FillStyle, TRUE);
      PInfo^.SetSymbolFill(DefaultSymbolFill);
      PInfo^.BeginPaint(0, 0, dm_SelDesel + dm_SelGra + dm_EditPoly + dm_NoPattern, -1);
      Draw := Data^.LastThat(@DoAll) <> nil;
    end
    else
    begin
      PInfo^.SetLineStyle(PInfo^.SelectionSettings.LineStyle, TRUE);
      PInfo^.SetFillStyle(PInfo^.SelectionSettings.FillStyle, TRUE);
      PInfo^.SetSymbolFill(DefaultSymbolFill);
      PInfo^.WMPaint := False;
      PInfo^.BeginPaint(0, 0, dm_SelDesel + dm_SelGra, -1);
      if PProj(PInfo^.AProj)^.Layers^.TranspLayer then
      begin
        SelLayerData := Data;
        Data := PViewColl(PProj(PInfo^.AProj)^.SelList);
        Draw := Data^.FirstThat(@DoAll) <> nil;
        Data := SelLayerData;
      end
      else
        Draw := Data^.LastThat(@DoAll) <> nil;
    end;
  finally
    PInfo^.ExtCanvas.Pop;
  end;
end;

const
  RSelect: TStreamRec = (
    ObjType: rn_Select;
    VmtLink: TypeOf(TSelect);
    Load: @TSelect.Load;
    Store: @TSelect.Store);
begin
  RegisterType(RSelect);
end.

