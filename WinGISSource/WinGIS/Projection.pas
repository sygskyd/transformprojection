Unit Projection;

Interface

Type TCommonProjection = Class
      Private
      Protected
       FName            : String;
       FXName           : String;
       FYName           : String;
      Public
       Property    Name:String read FName;
       // converts from this system to project-internal coordinates
       Function    ConvertToProjectCoordinates(var XValue,YValue:Double):Boolean; virtual; abstract;
       // converts from project-internal coordinates to this system
       Function    ConvertFromProjectCoordinates(var XValue,YValue:Double):Boolean; virtual; abstract;
       Property    XName:String read FXName;
       Property    YName:String read FYName; 
       Function    Format(const XValue,YValue:Double):String; virtual;
       Function    Setup(Parent:TWinControl):Boolean; virtual;
     end;

Implementation

Function TCommonProjection.Setup(Parent:TWinControl):Boolean;
begin
  Result:=TRUE;
end;

end.
 