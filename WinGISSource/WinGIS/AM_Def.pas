{****************************************************************************}
{ Unit AM_Def                                                                }
{----------------------------------------------------------------------------}
{ Konstanten und oft ben�tigte Prozeduren.                                   }
{----------------------------------------------------------------------------}
{ Autor: Martin Forst                                                        }
{----------------------------------------------------------------------------}
{ Modifikationen                                                             }
{  16.02.1994 Martin Forst                                                   }
{  04.04.1994 Martin Forst   DBLoaded Typ ge�ndert + Initialisierung         }
{  05.04.1994 Martin Forst   Konstanten f�r Kreise                           }
{  08.04.1994 Martin Forst   Cursor-Konstanten                               }
{  07.06.1995 Martin Forst   dm_Print gel�scht                               }
{****************************************************************************}
unit AM_Def;

interface

uses Messages, AM_Res, ResDlg, WinTypes, WinProcs, SysUtils, Objects,
  AM_Font, Graphics, RegDB, IniFiles, Classes;

type
  Integer16 = SmallInt;
  Integer8 = ShortInt;

const
  WG_MIN = 10;
  WG_STD = 11;
  WG_PROF = 12;

const
  BORDER_AREA_WIDTH = 15;

const
  uiLayers = 10;
  uiWorkingLayer = 11;
  uiSymbols = 12;
  uiViews = 13;
  uiLegends = 14;
  uiStartup = 15;

  dblNone = -1E301;
  dblFromLayer = -2E301;
  dblFromObject = -3E301;

  intNone = -1;
  intFromLayer = -2;
  intFromObject = -3;

  ptFromLayer = -1;
  ptFromObject = -2;

  clNone = TColor(-1);
  clFromLayer = TColor(-2);
  clFromObject = TColor(-3);

  lwFromLayer = dblFromLayer;
  lwFromObject = dblFromObject;

  ltNone = -1;
  ltFromLayer = -2;
  ltFromObject = -3;

  symNone = intNone;
  symFromLayer = intFromLayer;
  symFromObject = intFromObject;

  symfRegular = 0;
//      symfSingle        = 1;
  symfNone = intNone;
  symfFromLayer = intFromLayer;

  lt_Solid = 0;
  lt_Dash = 1;
  lt_Dot = 2;
  lt_DashDot = 3;
  lt_DashDDot = 4;

  lt_UserDefined = 1000; { number of first user-defined linestyle }

  pt_NoPattern = 0;
  pt_FDiagonal = 1;
  pt_Cross = 2;
  pt_DiagCross = 3;
  pt_BDiagonal = 4;
  pt_Horizontal = 5;
  pt_Vertical = 6;
  pt_Solid = 7;

  pt_UserDefined = 8;

  stDrawingUnits = 0;
  stScaleInDependend = 1;
  stProjectScale = 2;
  stPercent = 3;
  ObjCountLimit = 3000;
       {brovak}
  PrintFrameLayerName = '$@Print#Project#Frame';
      {brovak}

type
  PLineStyle = ^TLineStyle;
  TLineStyle = record
    Style: Integer16; { windows-line-style or number of user-defined line-style }
    Width: Double; { with of the line in 1/10 mm }
    WidthType: Integer8; { type of the width-entry, stProjectScale or stScaleIndependend }
    Scale: Double; { scale for userdefined line-styles }
    Color: TColor; { color of the line or frames in user-defined line-styles }
    FillColor: TColor; { color of filled parts in user-defined line-styles }
  end;

  PFillStyle = ^TFillStyle;
  TFillStyle = record
    Pattern: Integer16;
    ForeColor: TColor;
    BackColor: TColor;
    Scale: Double;
    ScaleType: Integer8;
  end;

  PSymbolFill = ^TSymbolFill;
  TSymbolFill = record
    FillType: Integer8;
    Symbol: LongInt;
    Size: Double;
    SizeType: Integer8;
    SymbolAngle: Double;
    DistanceType: Integer8;
    Distance: Double;
    LineDistance: Double;
    Offset: Double;
    OffsetType: Integer8;
    Angle: Double;
    LineStyle: TLineStyle;
    FillStyle: TFillStyle;
  end;

  PSelectionSettings = ^TSelectionSettings;
  TSelectionSettings = record
    UseGlobals: Boolean;
    LineStyle: TLineStyle;
    FillStyle: TFillStyle;
    DBSelectionMode: Boolean;
    TransparentMode: Word;
  end;

  PVariousSettings = ^TVariousSettings;
  TVariousSettings = record
       // true if the global settings shall be used
    UseGlobals: Boolean;
       // true if the edges of polylines and polygons shall be marked
    ShowEdgePoints: Boolean;
       // area left blank when auto-zooming, depends on ZoomSizeType
    ZoomSize: Double;
       // type of the value specified by ZoomSize, 0 = Percentage (0..1),
       // 1 = millimeters
    ZoomSizeType: Integer;
       //
    CombineRadius: Double;
    CombineRadiusType: Integer;
       // true if the nearest snap-point shall be searched for
    SnapToNearest: Boolean;
       // radius for the snap, depends on SnapRadiusType
    SnapRadius: Double;
       // type of the value represented by SnapRadius, 0 = millimeters,
       // 1 = pixels, 2 = drawing-units
    SnapRadiusType: Integer;

         {++ Brovak BUG#574 BUILD#108 - Additional Condition for draw/no draw rect for hidden text
       if text is small for view then: if ShowHiddenText=true, then text's rect draw , else nothing draw
       To OldHiddenText The significance before call of the dialogue is recorded}
    ShowHiddenText: boolean;
    OldHiddenText: boolean;
       {-- Brovak }
//++ Glukhov ObjAttachment 18.10.00
       // Defines, if the attached objects will be deleted for a deleted object
    bDelAttached: Boolean;
       // Defines, if the deleting of the attached objects should be confirmed
    bDelAttachedAsk: Boolean; // makes sense when bDelAttached is True
//-- Glukhov ObjAttachment 18.10.00
//++ Glukhov Bug#199 Build#157 14.05.01
// Arc (Circle) conversion settings
    bConvRelMistake: Boolean; // True = relative value of a allowable variation (admissible error)
    dbConvRelMistake: Double; // Part of arc (circle) radius
    dbConvAbsMistake: Double; // An absolute value
//-- Glukhov Bug#199 Build#157 14.05.01
  end;

  PBitmapSettings = ^TBitmapSettings;
  TBitmapSettings = record
    UseGlobals: Boolean;
    ShowBitmap: Boolean;
    ShowFrame: Boolean;
    ShowMinSize: LongInt;
    Transparency: Integer;
    TransparencyType: Integer;
    GlobInvisiblity: Boolean; // Glukhov Bug#236 Build#144 29.11.00
  end;

  PThreeDSettings = ^TThreeDSettings;
  TThreeDSettings = record
    UseGlobals: Boolean;
    ModulePath: string;
    ModuleFile: string;
    DataFile: string;
    ResultPath: string;
    ImageFileName: string;
    BounderyFileName: string;
    PtsDataFileName: string;
    WebBrowser: string;
    HelpStartPage: string;
  end;

  PFontSettings = ^TFontSettings;
  TFontSettings = record
    UseGlobals: Boolean;
    ChosenFont: AM_Font.PFontData;
    AllFonts: AM_Font.PFonts;
  end;

  PAnnotSettings = ^TAnnotSettings;
  TAnnotSettings = record
       {UseGlobals       : Boolean;}
    AnnotLayer: LongInt; {Layerindex}
    AnnotFont: AM_Font.TFontData;
       {AllFonts         : AM_Font.PFonts;}
    Angle: Double;
    Color: TColorRef;
    Position: Integer;
    ShowDialog: Boolean;
{++ Ivanoff}
    KeepLinkToDBAlive: Boolean;
{-- Ivanoff}
  end;
       {brovak}

  PPrintSpecFrameSettings = ^TPrintSpecFrameSettings;
  TPrintSpecFrameSettings = record
    UseSpecFrame: boolean;
    UseTextMarks: boolean;
    Font: AM_Font.TFontData;
    Color: TColor;
    BackColor: TColor;

    UseLeft: boolean;
    UseRight: boolean;
    UseTop: boolean;
    UseBottom: boolean;

    StepLength: double;
    MarksLength: double;

    UseCross: boolean;
    CrossLength: double;
    CrossColor: integer;
    GridColor: integer;

    LabelOutside: Boolean;
  end;
     {brovak}

  PPolyPartProps = ^TPolyPartProps;
  TPolyPartProps = record
    SAreaName: string;
    SArea: Extended;
    DArea1: Extended;
    DArea2: Extended;
    SLayerName: string;
    SLayerIndex: LongInt;
    DLayerName: string;
    DLayerIndex: LongInt;
    SID: LongInt;
    PartDist: Extended;
    PartAngle: Extended;
    PartArea: Extended;
    FillArea: Boolean;
    PartMode: Integer;
    SizeMode: Integer;
    DDEMode: Boolean;
  end;

     // added by bruno
  PProjectionSettings = ^TProjectionSettings;
  TProjectionSettings = record
    ProjectProjection: string;
    ProjectTargetProjection: string;                       //added for projection converting
    ProjectTargetProjSettings: string;                     //same here 
    ProjectDate: string;
    ProjectProjSettings: string;
    ProjectionXOffset: Double;
    ProjectionYOffset: Double;
    ProjectionScale: Double;
       //added by Brovak
    ShowCoordInDegrees: boolean;
  end;

const
  DefaultLineStyle: TLineStyle = (
    Style: 0;
    Width: 0;
    WidthType: stScaleIndependend;
    Scale: 1;
    Color: 0;
    FillColor: 0
    );

  DefaultFillStyle: TFillStyle = (
    Pattern: 0;
    ForeColor: 0;
    BackColor: clNone;
    Scale: 1;
    ScaleType: stScaleIndependend
    );

  DefaultSymbolFill: TSymbolFill = (
    FillType: symfNone;
    Symbol: symNone;
    Size: dblFromObject;
    SizeType: stProjectScale;
    SymbolAngle: dblFromObject;
    DistanceType: stDrawingUnits;
    Distance: 1000.0;
    LineDistance: 1000.0;
    Offset: 0.0;
    OffsetType: stPercent;
    Angle: 0.0;
    LineStyle: (Style: ltFromObject; Width: lwFromObject; WidthType: stScaleIndependend;
    Scale: dblFromObject; Color: clFromObject; FillColor: clFromObject);
    FillStyle: (Pattern: ptFromObject; ForeColor: clFromObject; BackColor: clNone;
    Scale: dblFromObject; ScaleType: stScaleIndependend)
    );

type
  PRGBColors = ^TRGBColors;
  TRGBColors = array[0..15] of LongInt;

  PStdPalette = ^TStdPalette;
  TStdPalette = array[0..255] of TColorRef;

  TObjectType = Byte;
  TObjectTypes = set of TObjectType;

  TLPoint = record
    X, Y: LongInt;
  end;

const
  MaxShortInt = 32756;

  cm_SuperImage = 100; //++Sygsky: the minimum number in row is here!!!
  cm_DBLoadNext = 105;
  cm_TextGraph = 120;
  cm_BusGraph = 121;
  cm_InsObjs = 122;
  cm_DeselAll = 408;
  cm_CopyObject = 508;
  cm_DBObjInfos = 701;
  cm_DBMultiInsert = 702;

{$IFDEF LBG}
  cm_Info4 = 904;
{$ENDIF}
  cm_DBImASCII = 1002;
  cm_BTX = 1102;
  cm_DBImpExpErr = 1200;

  cm_DBSelCircle = 2028;
  cm_DBSelRect = 2029;
  cm_MoveFromDB = 2033;

  cm_GPSVerbdg = 2050; {!}
  cm_InsLinkObj = 2053; {!}

  cm_SymbolRequest = 2055;
  cm_NoDraw = 2056;

  cm_MON0 = 2058;
  cm_MON1 = 2059;
  cm_MON2 = 2060;
  cm_MON3 = 2061;
  cm_MON4 = 2062;
  cm_MON5 = 2063;
  cm_MON6 = 2064;
  cm_MON7 = 2065;
  cm_MON8 = 2066;
  cm_MON9 = 2067;
  cm_DBAddLayer = 2068;
  cm_DBDelLayer = 2069;
  cm_ExitFromDB = 2070;
  cm_LoadBitFromDB = 2074;
  cm_DBInsert = 2079;
  cm_DBChangedTable = 2080;
  cm_DBObjStyle = 2081;
  cm_DBSelObjects = 2082;
  cm_DBZoomObjects = 2083;
  cm_DBAddLayerNDlg = 2084;
  cm_DBDelLayerNDlg = 2085;
  cm_DBChaLayerNDlg = 2086;
  cm_DBSFGLayers = 2087;
  cm_RequestLayer = 2088;
  cm_DBRecharge = 2089;

  cm_LoadNextProj = 2090; {!?}

  cm_DBShowView = 2091;
  cm_DBSetOptions = 2092;
  cm_DBDelete = 2093;
  cm_GraphicReady = 2094;
  cm_DBSetText = 2097;
  cm_DKM = 2098;

  cm_CheckObjExist = 2109;
  cm_DBDeselAll = 2110;
  cm_LoadFilesATR = 2111;
  cm_MON10 = 2112;
  cm_MON11 = 2113;
  cm_MON12 = 2114;
  cm_MON13 = 2115;
  cm_MON14 = 2116;
  cm_MON15 = 2117;
  cm_MON16 = 2118;
  cm_MON17 = 2119;
  cm_MON18 = 2120;
  cm_MON19 = 2121;
  cm_DBSetTextAPar = 2123;
  cm_SetMainWinSize = 2124;
  cm_OpenProjects = 2125;
  cm_DBDZoomObjects = 2126;
  cm_DBDSelObjects = 2127;
  cm_PostOpen = 2128;
  cm_ObjTrans = 2129;
  cm_ShowProcess = 2130;
  cm_DBCopyObject = 2131;
  cm_CloseProject = 2132;
  cm_SetTranspMode = 2133;
  cm_GetTranspMode = 2134;
  cm_RequestLayers = 2135;
  cm_DBViewInfos = 2136;
  cm_GetNextObjIDs = 2137;
  cm_MoveAbsFromDB = 2140;
  cm_DBSetGraMode = 2141;
  cm_DBProjSaveAs = 2142;
  cm_OpActProjs = 2143;
  cm_NewGST = 2145;
  cm_NewHalt = 2146;
  cm_SelAndNBS = 2147;
  cm_SelModeDBEnd = 2148;
  cm_CancelIntro = 2149;
  cm_DBPrintGraph = 2150;
  cm_RedrawADDE = 2151;
  cm_DoGPSCommand = 2152;
  cm_SetActProj = 2154;
  cm_ChangeProject = 2155;
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 02.10.00}
//    cm_DBPrintSetup   = 2156;
  cm_DBPrintSetPrnParam = 2156;
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 02.10.00}
  cm_SendPoly = 2157;
  cm_DBSetTSLayer = 2158;
  cm_Save = 2159;
  cm_DBCopyObject2 = 2160;
  cm_ClearLayer = 2161;
  cm_SendObjProps = 2162;
  cm_SelModeGIS = 2163;
  cm_DBSetActSym = 2164;
  cm_DBSelectedObj = 2165;
  cm_RequestSyms = 2166;
  cm_RequLStyles = 2167;
  cm_RequHStyles = 2168;
  cm_ChkConnection = 2169;
  cm_DBDenySession = 2170;
  cm_SetProjPart = 2171;
  cm_ShowAllObjs = 2172;
  cm_SendProjPart = 2173;
  cm_DBAnnotOpts = 2174;
  cm_SuppressDlg = 2175;
  cm_PolyOverlay = 2176;
  cm_PolyPart = 2177;
  cm_SendPartData = 2178;
  cm_ShowPartDlg = 2179;
  cm_ScrRefStart = 2180;
  cm_ScrRefEnd = 2181;
  cm_DBProjRedraw = 2182;
{++ Ivanoff 4 Bolt}
  cm_DBGetPolygonData = 2183;
{-- Ivanoff 2 Bolt}
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 02.10.00}
  cm_DBPrintGetPrnParam = 2184;
  cm_DBPrintSetPgsParam = 2185;
  cm_DBPrintGetPgsParam = 2186;
  cm_DBPrintSetMapParam = 2187;
  cm_DBPrintGetMapParam = 2188;
  cm_DBPrintSetLegParam = 2189;
  cm_DBPrintGetLegParam = 2190;
  cm_DBPrintInsLegend = 2191;
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 02.10.00}
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 23.10.00}
  cm_DBPrintSetPicParam = 2192;
  cm_DBPrintGetPicParam = 2193;
  cm_DBPrintInsPicture = 2194;
  cm_DBPrintSetSigParam = 2195;
  cm_DBPrintGetSigParam = 2196;
  cm_DBPrintInsSignature = 2197;
  cm_DBPrintStoreTemplate = 2198;
  cm_DBPrintLoadTemplate = 2199;
  cm_DBPrintGetObjInform = 2200;
  cm_DBPrintDeleteObject = 2201;
  cm_DBPrintMoveObject = 2202;
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 03.11.00}
  cm_DBReversePolyLine = 2203; // Glukhov Bug#84 BUILD#125 02.11.00
  cm_GetCoordinates = 2204; // Glukhov Bug#203 Build#151 08.02.01
  cm_SetPicture = 2205; // Glukhov Bug#100 Build#152 05.03.01

      {cm_Constants 3000 - 3199 used in DDEDef.pas}
      {cm_Constants 3200 - 3299 used in PolyPartDef.pas}

  cm_SelStart = 6010;
  cm_SelDest = 6011;
  cm_SelGo = 6012;
  cm_SetSelect = 6014;
  cm_SetVLType = 6007;
  cm_StreetID = 6008;
  cm_combineST = 6009;
  cm_InsertItems = 6010;
  cm_SetGraphParent = 6011;
  cm_FSConstruct = 8000;
  cm_SConstruct = 8001;
  cm_ConstructFS = 8002;
  cm_ConstructS = 8003;
  cm_Constructis = 8004;
  cm_DoParallel = 8005;
  cm_SetRetry = 8006;
  cm_DoInParallel = 8007;
  cm_DoBuffer = 8008;
  cm_LBGText = 8009;
  cm_DoInsIsland = 8010;
  cm_PartArea = 8011;
  cm_FileOpen = 8012;

  cm_StartRoute = 9999;
  cm_Test = 10000;
  cm_Test1 = 10001;
  cm_Test2 = 10002;
  cm_Test3 = 10003;
  cm_ModifyView = 10005;

  cm_StopOmega = 11000; {++Sygsky: Omega: stop function }
  cm_ResetOmega = 11001; {++Sygsky: Omega: redraw graphics elements }
  cm_ShowOmega = 11002; {++Sygsky: Omega: show Omega control window }

  cm_Debug = 12000;

  im_TurboRaster = 'TrbRa';
  in_Turboraster = 'TurboRaster';
  im_GPSModule = 'GPSMo';
  in_GPSModule = 'GPS-Module';
  im_RoutePlaner = 'MuMed'; {aus altem Modul Multimedia verwendet, neu:'Route'}
  in_RoutePlaner = 'RoutePlaner';
  im_CnvtTools = 'CnvTo';
  in_CnvtTools = 'Convert-Tools'; {260695Ray}

  wm_DrawObject = wm_User + 1;
  wm_ICoord = wm_User + 2;
  wm_Digitize = wm_User + 3;
  wm_WT_Packet = wm_User + 4;
  wm_CallSymLib = wm_User + 10;
  wm_LogLine = 15003;
  wm_SetPriority = 15004;
  wm_ExecuteMenufunction = 15005;

  WM_WGMousePos = 15010;

  PmsGuid = '{A8D1F25B-EA04-488B-93EE-E162C6D88D1A}';

  WM_WELCOME_EXIT = wm_USER + 2001; //brovak for Welcome dll
  WM_TIPS_EXIT = wm_USER + 2002; //brovak for Tips dll

{++ IDB}
  WM_IDB_EndDrag = WM_USER + $2742; { This message is sent to a  that contains TDBGrid with an attribute information.
                                          This message means the TDBGrid must end Drag-and-Drop operation.
                                          Warning!
                                          This message is also declared in CommonLib\IDB\IDB_CDBGrid.pas.
                                          If you want to change its number, you must also change it there!
                                          Otherwise the end of Drag-and_Drop operation (from DBGrid to project window)
                                          won't be reached.
                                          Ivanoff.}
{-- IDB}

  idt_AutoSave = 4;
  idt_DDEInit = 5;
  idt_GPSInit = 6; {1904Ray}
  idt_Intro = 7;
      {idt_GReady   =  8;}
  idt_DDEDone = 9;
  idt_Mouse = 55;
  idt_Popup = 56;
  idt_DHint = 57;
  idt_CanvasResize = 58;

  ie_NoError = 0;
  ie_IniErr = 1;
  ie_FileErr = 2;

  tvAutoSave = 60000;
      {tvGReady     = 1000;}
  tvRDR = 1000;

  ts_Left = 0;
  ts_Center = 1;
  ts_Right = 2;
  ts_Italic = 4;
  ts_Bold = 8;
  ts_Underl = 16;
  ts_FixHeight = 32;
  ts_Transparent = 64;

  ts_OtherHeight = $4000;
  ts_OtherFont = $2000;
  ts_OtherText = $1000;

  ts_Align = ts_Left or ts_Center or ts_Right;
  ts_Style = ts_Italic or ts_Bold or ts_Underl;

  di_ProjOpen = $0001;
  di_ProjSave = $0002;
  di_ProjSaveAs = $0004;
  di_ProjNew = $0008;
  di_ProjChange = $0010;
  di_ProjClose = $0020;
  str_Wingis = 0;
  str_Desma = 1;
  li_None = 0;
  li_Text = 1;
  li_Index = 2;

  pfReadOnly = $01; { Projekt ist schreibgesch�tzt          }

  siStartX = 5000000;
  siStartY = 5000000;
  sc_StRange = 100000;

  siBorder = 10000;

  poMaxDist = 10; { maximaler Abstand f�r Selektierung (Bildschirm}
                              { Einheiten [1/10 mm])                          }
  dtPoint = 1;

  tfLinear = 0;
  tfHelmert = 1;
  tfAffine = 2;

  idc_SnapWin = 100;
  idc_Move = 101;
  idc_Del = 102;
  idc_Zoom = 103;
  idc_Pixel = 104;
  idc_Poly = 105;
  idc_CPoly = 106;
  idc_Sym = 107;
  idc_Text = 108;
  idc_Ins = 109;
  idc_Neigh = 110;
  idc_Circle = 111;
  idc_Rect = 112;
  idc_Offset = 113;
  idc_Distance = 114;
  idc_MesPoint = 115;
  idc_MesCut = 116;
  idc_MesOrtho = 117;
  idc_DrawCircle = 118;
  idc_DrawArc = 119;
  idc_TrimmBoth = 120;
  idc_TrimmFirst = 121;
  idc_CombineAreas = 122;
  idc_CombineLines = 123;
  idc_TranspSelect = 124;
  idc_TranspLayer = 125;
  idc_Rotate = 126;
  idc_Size = 127;
  idc_SelPoly = 128;
  idc_Neighbour = 129;
  idc_SelPoint = 130;
  idc_RefCircle = 210;

  idc_Right = 300;
  idc_Left = 301;
  idc_Up = 302;
  idc_Down = 303;
  idc_RightUp = 304;
  idc_RightDown = 305;
  idc_LeftUp = 306;
  idc_LeftDown = 307;

  mn_Select = $0001;
  mn_Pixel = $0002;
  mn_CPoly = $0003;
  mn_Poly = $0004;
  mn_Zoom = $0005;
  mn_Offset = $0007;
  mn_Move = $0009;
  mn_Snap = $000A;
  mn_SnapPoly = $000B;
  mn_InsPoint = $000C;
  mn_InsPOK = $000D;
  mn_DelPoint = $000E;
  mn_DelPOK = $000F;
  mn_Symbol = $0010;
  mn_Text = $0013;
  mn_OffsetForSel = $0014;
  mn_MovePoly = $001C;
  mn_SizeRot = $001D;
  mn_MoveSym = $001E;
  mn_Delete = $001F;
  mn_OffsetOK = $0020;
  mn_SymbolInp = $0021;
  mn_Distance = $0027;
  mn_RefPoints = $0029;
  mn_DBText = $0031;
  mn_GenerRef = $0035;
  mn_GenerRect = $0036;
  mn_SplineEdit = $003A;
  mn_EditImage = $003B;
  mn_EditTxtRect = $003C; // Glukhov Bug#170 BUILD#125 03.11.00
  mn_MesPoint = $003E;
  mn_MesCut = $0040;
  mn_MesOrtho = $0041;
  mn_LineCut = $0044;
  mn_Combine = $0045;
  mn_CombineSel = $0046;
  mn_CombineSelOne = $0047;
  mn_SelForSelect = $004B;
  mn_DrawForSelect = $004C;
  mn_RTrafo = $0051;
  mn_MoveCircle = $0058;
  mn_ShowDBSel = $0059;
  mn_MoveMulti = $005A;
  mn_InsertOLEObject = $005C; { ********* OLE ******** }
  mn_InsLinkObj = $005D; { ********* OLE ******** }
  mn_PasteOLEObj = $005F; { ********* OLE ******** }
  mn_EditOLE = $0060;
  mn_RefBitmap = $0061; { TR-DDE }
  mn_DBDDE0 = $0062;
  mn_DBDDE1 = $0063;
  mn_DBDDE2 = $0064;
  mn_DBDDE3 = $0065;
  mn_DBDDE4 = $0066;
  mn_DBDDE5 = $0067;
  mn_DBDDE6 = $0068;
  mn_DBDDE7 = $0069;
  mn_DBDDE8 = $006A;
  mn_DBDDE9 = $006B;
  mn_DBSelCircle = $006C;
  mn_DBSelRect = $006D;
  mn_DistMark = $006E;
  mn_OrthoMark = $006F;
  mn_CircleCutMark = $0070;
  mn_AngleMark = $0071;
  mn_CircleMark = $0072;
  mn_SelStart = $0075;
  mn_SelDest = $0076;
  mn_SelGo = $0077;
  mn_SetSel = $0078;
  mn_Ponti = $0079;
  mn_Inspect = $007A;
  mn_DBDDE10 = $007B;
  mn_DBDDE11 = $007C;
  mn_DBDDE12 = $007D;
  mn_DBDDE13 = $007E;
  mn_DBDDE14 = $007F;
  mn_DrawSymLine = $0081;
  mn_SetRefPoint = $0082;
  mn_DBDDE15 = $0083;
  mn_DBDDE16 = $0084;
  mn_DBDDE17 = $0085;
  mn_DBDDE18 = $0086;
  mn_DBDDE19 = $0087;
  mn_TranspLayerDB = $0085;
  mn_PrintDB = $0086;
  mn_LineMark = $0087;
  mn_GenArea = $0088;

  mn_SetOmegaPoint = $0090; {++Sygsky: Omega add-in }
  mn_SetImageRect = $0091; // Glukhov: to load image in rectangle
  mn_AttachObject = $0092; // Glukhov ObjAttachment BUILD#128 11.10.00
  mn_GetCoordinates = $0093; // Glukhov Bug#203 Build#151 08.02.01
  mn_SelToDrawAlong = $0094; // Glukhov Bug#468 Build#162 16.07.01
  mn_EntToDrawAlong = $0095; // Glukhov Bug#468 Build#162 16.08.01

       {++ brovak #MOD for copying part of image to file}
  mn_CutImage = $00A0;
       {-- brovak}
      //Brovak
  mn_PseudoSelect = $00A1;
  mn_SuperImage = $00A3; // to make super imposed image from images AND with rect
  mn_PartParallel = $00E0;
  mn_MovePartPara = $00E1;
  mn_PartPuffer = $00E2;
  mn_MovePartPuff = $00E3;
  mn_InfoPoly = $00E4;
  mn_PartFree = $00E5;
  mn_Construct = $00F0;
  mn_CInsPoint = $00F1;
  mn_CDelPoint = $00F2;
  mn_CMovePoint = $00F3;
  mn_CParallel = $00F4;
  mn_CBufferDraw = $00F5;
  mn_CBufferSel = $00F6;
  mn_CIslands = $00F7;
  mn_CParallelS = $00F8;
  mn_BufferDir = $00F9;
  mn_BufferDirOK = $00FA;
  mn_ConstructAbort = $00FB;
  mn_SplitLine = $00FC;
  mn_SelPoly = $00FD;
  mn_ScalePolyline = $00FE;

  mn_DBChange = $1000;
  mn_DBGraph = $1001;

  mn_Edit_Text = $9993; //brovak
  mn_Edit_Symbol = $9994;
  mn_Edit_Arc = $9995;
  mn_New_Arc = $9996;
  mn_Edit_Point = $9997;
  mn_Edit_Circle = $9998; //
  mn_New_Circle = $9999; //brovak

  dd_RefPoints = 0000; { TR-DDE }
  dd_SerialNumber = 0001;
  dd_ModuleCode = 0002;
  dd_ModuleName = 0003;
  dd_LicenceName = 0004;
  dd_RefPointsV2 = 0005;

      { Men�s bei denen Koordinateneingabe m�glich ist.                                }
  mnCoord = [mn_Pixel, mn_Poly, mn_CPoly, mn_Symbol, mn_OffsetOK, mn_Distance,
    mn_GenerRef, mn_GenerRect, mn_Move, mn_RefPoints, mn_InsPOK, mn_SnapPoly,
    mn_DrawForSelect, mn_OffsetForSel, mn_MesPoint,
    mn_InsertOLEObject, mn_InsLinkObj, mn_PasteOLEObj,
    mn_DistMark, mn_LineMark, mn_OrthoMark, mn_CircleMark, mn_CircleCutMark, mn_AngleMark,
    mn_SetOmegaPoint, //++Sygsky: Omega
    mn_CutImage, mn_SuperImage,
    mn_RTrafo, // Glukhov Bug#136 BUILD#128 30.08.00
    mn_SetImageRect, // Glukhov: to load image in rectangle
    mn_AttachObject, // Glukhov ObjAttachment BUILD#128 11.10.00
    mn_GetCoordinates, // Glukhov Bug#203 Build#151 08.02.01
    mn_SelToDrawAlong, // Glukhov Bug#468 Build#162 16.07.01
    mn_EntToDrawAlong, // Glukhov Bug#468 Build#162 16.08.01
    mn_Text, //++brovak
    mn_SplitLine
    ];

      { Men�s bei deren Anwahl alle Objekte deselektiert werden.                       }
  mnDeSelAll = [{mn_Offset,}mn_LineCut, mn_Combine, mn_RTrafo,
    mn_Snap, mn_InsPoint, mn_DelPoint,
    mn_CutImage {++brovak},
    mn_SuperImage, //++Sygsky: for super image
    mn_SetOmegaPoint, {++Sygsky: Omega}
    mn_SetImageRect, // Glukhov: to load image in rectangle
    mn_SelPoly
    ];

      { Men�s f�r die BreakInput aufgerufen wird.                                      }
  mnBreakESC = [mn_Poly, mn_CPoly, mn_Move, mn_Snap, mn_SnapPoly, mn_InsPOK, mn_GenerRef, mn_GenerRect, mn_Text,
    mn_Distance, mn_SizeRot, mn_RefPoints, mn_OffsetOK, mn_SplineEdit,
    mn_EditImage, mn_Move, mn_MesPoint, mn_MesCut, mn_MesOrtho, mn_LineCut, mn_Combine, mn_SymbolInp,
    mn_CombineSel, mn_CombineSelOne, mn_SelForSelect,
    mn_RTrafo, mn_DelPoint, mn_DelPOk, mn_InsPoint, mn_DrawForSelect, mn_OffsetForSel,
    mn_EditOLE, mn_DBSelCircle, mn_DBSelRect, mn_DistMark, mn_LineMark, mn_OrthoMark, mn_DBText, mn_CBufferDraw,
    mn_CircleCutMark, mn_CircleMark, mn_PartFree, mn_AngleMark, mn_CParallel, mn_BufferDirOK,
    mn_Snap, mn_SnapPoly, mn_InsPoint, mn_InsPOK, mn_DelPoint, mn_DelPOK,
    mn_PartParallel, mn_GenArea,
    mn_CutImage {++brovak},
    mn_SuperImage, //++Sygsky: for super image
    mn_SetOmegaPoint, {++Sygsky: Omega}
    mn_EditTxtRect, // Glukhov Bug#170 BUILD#125 03.11.00
    mn_AttachObject, // Glukhov ObjAttachment BUILD#128 11.10.00
    mn_GetCoordinates, // Glukhov Bug#203 Build#151 08.02.01
    mn_SelToDrawAlong, // Glukhov Bug#468 Build#162 16.07.01
    mn_EntToDrawAlong, // Glukhov Bug#468 Build#162 16.08.01
    mn_SetImageRect // Glukhov: to load image in rectangle
    ];

      { Konstruktionsmen�s                                                             }
  mnConstructs = [mn_Construct, mn_InsPoint, mn_CInsPoint, mn_DelPoint, mn_CDelPoint, mn_Snap, mn_CMovePoint,
    mn_CParallel, mn_CBufferDraw, mn_CBufferSel, mn_BufferDir, mn_CIslands, mn_CutImage];

  msProject = 2147000000;

  dm_Paint = $0001;
  dm_NoPattern = $0002;
  dm_SelDesel = $0004;
  dm_SelGra = $0010;
  dm_EditPoly = $0020;

      { Constants for TIndex status. Until $0080 the values can be used for TIndex and/or TView.
       From $0100 they are reserved for other objects (Layers f.e)}
  sf_LowPrior = $0001;
  sf_OtherStyle = $0002;
  sf_Selected = $0008;
  sf_HasArrows = $0010;
  sf_NoSnap = $0020;

  sf_LayerBeg = $0100;
  sf_LayerOff = $0100;
  sf_Fixed = $0200;
  sf_Transparent = $0400; { transparente Schraffur bzw transparenter Text          }
  sf_LayerOffAtGen = $0800; { Layer off at generalization                            }
  sf_DoSelect = $1000; { selektierbarer Layer im Transparentmodus               }
  sf_Eraseable = $8000; { Verwendung in WinMAP LT                     alt: $0800 }
  sf_IslandArea = $10000; { Inselfl�che erkennen }
  sf_HSDVisible = $10000; { visibility-flag for HSD-Layers } {3010MF}
  sf_SymName = $20000; { TSymbol : Symbolptr zeigt noch auf TNameRec }
                                       { TSGroup : neues Symbol (f�r Load-Routine) }
  sf_UnVisible = $40000; { Objekt nicht sichtbar und selektierbar }
  sf_Attached = $80000; // Glukhov ObjAttachment
  sf_NewVersion = $80000000; { gesetzt ab Version 3.2                                 }

  co_InitSize = 10;
  co_Expand = 10;

      { Konstanten f�r TIndex.GetObjType                                                        }
  ot_Pixel = 1; { Punkte                                                 }
  ot_Poly = 2; { offene Polygone                                        }
  ot_CPoly = 4; { Fl�chen                                                }
  ot_Group = 5; { Objektgruppen                                          }
  ot_Proj = 6;
  ot_Text = 7;
  ot_Symbol = 8;
  ot_Spline = 9;
  ot_Image = 10;
  ot_MesLine = 11;
  ot_Circle = 12; { Kreise                               } {0504F}
  ot_Arc = 13; { Kreisb�gen                           } {0504F}
  ot_OLEObj = 14;
  ot_RText = 16;
  ot_BusGraph = 17;
  ot_Layer = 18;
  ot_ChartKind = 19; //++ Moskaliov Business Graphics BUILD#150 17.01.01

//    LastObjectType  = 18; //-- Moskaliov Business Graphics BUILD#150 17.01.01
  LastObjectType = 19; //++ Moskaliov Business Graphics BUILD#150 17.01.01

  ot_Any = [0..255];

  mi_Text = 000000000;
  mi_RText = 090000000;
  mi_BusGraph = 095000000;
  mi_MesLine = 100000000;
  mi_Symbol = 200000000;
  mi_Circle = 250000000; {0504F}
  mi_Arc = 300000000; {0504F}
  mi_Pixel = 400000000;
  mi_Group = 600000000;
  mi_ChartKind = 650000000; //++ Moskaliov Business Graphics BUILD#150 17.01.01
  mi_Spline = 700000000;
  mi_Poly = 800000000;
  mi_CPoly = 1000000000;
  mi_Image = 1200000000;
  mi_OLEObj = 1250000000; {0707Ray}

  df_NoPattern = $0001;

  MAXBGCOL = 16;

  RGBColors: TRGBColors = (
    $000000FF, $00000080, $00FF00FF, $00800080, $00FF0000,
    $00800000, $00FFFF00, $00808000, $0000FF00, $00008000,
    $0000FFFF, $00008080, $00FFFFFF, $00C0C0C0, $00808080,
    $00000000);

  BusGraphColors: TRGBColors = (
    clRed, clBlue, clLime, clYellow, clAqua,
    clTeal, clNavy, clMaroon, clGray, clFuchsia,
    clSilver, clBlack, clWhite, clGreen, clOlive, clPurple);

  StdPalette: TStdPalette = (
    $00000000, $00000C3F, $0000183F, $0000243F, $0000303F, $00003F3F,
    $00003F30, $00003F24, $00003F18, $00003F0C, $00003F00, $000C3F00,
    $00183F00, $00243F00, $00303F00, $003F3F00, $003F3000, $003F2400,
    $003F1800, $003F0C00, $003F0000, $003F000A, $003F0014, $003F001E,
    $003F0028, $003F0032, $003F003F, $0030003F, $0024003F, $0018003F,
    $000C003F, $0000003F,
    $0000007E, $0000197E, $0000327E, $00004B7E, $0000647E, $00007E7E,
    $00007E64, $00007E4B, $00007E32, $00007E19, $00007E00, $00197E00,
    $00327E00, $004B7E00, $00647E00, $007E7E00, $007E6400, $007E4B00,
    $007E3200, $007E1900, $007E0000, $007E0015, $007E002A, $007E003F,
    $007E0054, $007E0069, $007E007E, $0064007E, $004B007E, $0032007E,
    $0019007E, $0000007E,
    $000000BD, $000025BD, $00004ABD, $00006FBD, $000094BD, $0000BDBD,
    $0000BD94, $0000BD6F, $0000BD4A, $0000BD25, $0000BD00, $0025BD00,
    $004ABD00, $006FBD00, $0094BD00, $00BDBD00, $00BD9400, $00BD6F00,
    $00BD4A00, $00BD2500, $00BD0000, $00BD001F, $00BD003E, $00BD005D,
    $00BD007C, $00BD009B, $00BD00BD, $009400BD, $006F00BD, $004A00BD,
    $002500BD, $000000BD,
    $000000FF, $000033FF, $000066FF, $000099FF, $0000CCFF, $0000FFFF,
    $0000FFCC, $0000FF99, $0000FF66, $0000FF33, $0000FF00, $0033FF00,
    $0066FF00, $0099FF00, $00CCFF00, $00FFFF00, $00FFCC00, $00FF9900,
    $00FF6600, $00FF3300, $00FF0000, $00FF002A, $00FF0054, $00FF007E,
    $00FF00A8, $00FF00D2, $00FF00FF, $00CC00FF, $009900FF, $006600FF,
    $003300FF, $000000FF,
    $003333FF, $00335BFF, $003383FF, $0033ABFF, $0033D3FF, $0033FFFF,
    $0033FFD3, $0033FFAB, $0033FF83, $0033FF5B, $0033FF33, $005BFF33,
    $0083FF33, $00ABFF33, $00D3FF33, $00FFFF33, $00FFD333, $00FFAB33,
    $00FF8333, $00FF5B33, $00FF3333, $00FF3355, $00FF3377, $00FF3399,
    $00FF33BB, $00FF33DD, $00FF33FF, $00D333FF, $00AB33FF, $008333FF,
    $005B33FF, $003333FF,
    $006666FF, $006684FF, $0066A2FF, $0066C0FF, $0066DEFF, $0066FFFF,
    $0066FFDE, $0066FFC0, $0066FFA2, $0066FF84, $0066FF66, $0084FF66,
    $00A2FF66, $00C0FF66, $00DEFF66, $00FFFF66, $00FFDE66, $00FFC066,
    $00FFA266, $00FF8466, $00FF6666, $00FF667F, $00FF6698, $00FF66B1,
    $00FF66CA, $00FF66E3, $00FF66FF, $00DE66FF, $00C066FF, $00A266FF,
    $008466FF, $006666FF,
    $009999FF, $0099ADFF, $0099C1FF, $0099D5FF, $0099E9FF, $0099FFFF,
    $0099FFE9, $0099FFD5, $0099FFC1, $0099FFAD, $0099FF99, $00ADFF99,
    $00C1FF99, $00D5FF99, $00E9FF99, $00FFFF99, $00FFE999, $00FFD599,
    $00FFC199, $00FFAD99, $00FF9999, $00FF99AA, $00FF99BB, $00FF99CC,
    $00FF99DD, $00FF99EE, $00FF99FF, $00E999FF, $00D599FF, $00C199FF,
    $00AD99FF, $009999FF,
    $00CCCCFF, $00CCD6FF, $00CCE0FF, $00CCEAFF, $00CCF4FF, $00CCFFFF,
    $00CCFFF4, $00CCFFEA, $00CCFFE0, $00CCFFD6, $00CCFFCC, $00D6FFCC,
    $00E0FFCC, $00EAFFCC, $00F4FFCC, $00FFFFCC, $00FFF4CC, $00FFEACC,
    $00FFE0CC, $00FFD6CC, $00FFCCCC, $00FFCCD4, $00FFCCDC, $00FFCCE4,
    $00FFCCEC, $00FFCCF4, $00FFCCFF, $00F4CCFF, $00EACCFF, $00E0CCFF,
    $00D6CCFF, $00FFFFFF);

  c_LRed = 0;
  c_Red = 1;
  c_LMagenta = 2;
  c_Magenta = 3;
  c_LBlue = 4;
  c_Blue = 5;
  c_LCyan = 6;
  c_Cyan = 7;
  c_LGreen = 8;
  c_Green = 9;
  c_Yellow = 10;
  c_Brown = 11;
  c_White = 12;
  c_LGray = 13;
  c_Gray = 14;
  c_Black = 15;

  nl_Color = c_Black;
  nl_Line = lt_Solid;
  nl_PatCol = c_Black;
  nl_Pat = pt_NoPattern;

  rn_DPoint = 1000;
  rn_DRect = 1001;
  rn_Layer = 1002;
  rn_View = 1003;
  rn_Pixel = 1004;
  rn_Group = 1005;
  rn_Paint = 1006;
  rn_PolyOld = 1007;
  rn_CPolyOld = 1008;
  rn_Proj = 1009;
  rn_IndexColl = 1010;
  rn_ViewColl = 1011;
  rn_LayersOld = 1012;
  rn_ObjectsOld = 1013;
  rn_Select = 1014;
  rn_Index = 1015;
  rn_Circle = 1016;
  rn_Arc = 1017;
  rn_SPolyOld = 1018;
  rn_SCPolyOld = 1019;
  rn_SPixel = 1020;
  rn_SymbolsOld = 1021;
  rn_SGroupOld = 1022;
  rn_Symbol = 1023;
  rn_TextOld = 1024;
  rn_OldFontDes = 1025;
  rn_Fonts = 1026;
  rn_Import = 1027;
  rn_ImpASCII = 1028;
  rn_ObjOld2 = 1029;
  rn_SymsOld2 = 1030;
  rn_Sight = 1031;
  rn_Sights = 1032;
  rn_Layers = 1033;
  rn_FontDes = 1034;
  rn_SGroup = 1035;
  rn_ObjOld3 = 1036;
  rn_SymOld3 = 1037;
  rn_SplineOld = 1038;
  rn_Image = 1039;
  rn_LongColl = 1040;
  rn_Poly = 1041;
  rn_CPoly = 1042;
  rn_WArray = 1043;
  rn_Text = 1044;
  rn_Objects = 1045;
  rn_Symbols = 1046;
  rn_Measure = 1047;
  rn_SPoly = 1048;
  rn_SCPoly = 1049;
  rn_Spline = 1050;
  rn_PatImage = 1051;
  rn_ImageV31 = 1052;
  rn_Ellipse = 1151;
  rn_SEllipse = 1152;
  rn_EllipseArc = 1153;
  rn_SEllipseArc = 1154;
  rn_RText = 1155;
  rn_BusGraph = 1156;
  rn_DGraph = 1157;
  rn_Annot = 1158;
  rn_PatImageColl = 1159;
  rn_UnsortedColl = 1160;
  rn_ImageV32 = 1161;
  rn_MMEntry = 1162;
  rn_MMList = 1163;
  rn_OLEObject = 1164;
  rn_ImageV321 = 1165;
  rn_TRImage = 1166;
  rn_OLEObjectV2 = 1167;
  rn_ImageV33 = 1168; {HideFrame}
  rn_TRImageV2 = 1169;
  rn_SightV33 = 1170;
  rn_SightLData = 1171;
  rn_SSizeInfo = 1172;
  rn_ImageV4 = 1173;
  rn_SImageV4 = 1174;
  rn_DBSetting = 1176;
  rn_DBSettings = 1177;

// ++ Cadmensky
  rn_DPointColl = 1178;
// -- Cadmensky

  rn_SightV40 = 1180;
  rn_SText = 1266; { 0996Ray Symboltexte }
  rn_STextOld = 1267;
  rn_SSpline = 1268; { 0996Ray Symbolsplines }
  rn_SSplineOld = 1269;
  rn_SImage = 1270; { 0397Ray Symbolbitmaps }
  rn_SymbolsV1 = 1272; { neuer TSymbols-Stream Typ}
  rn_ExtLib = 1273; { ext. Bib. im AMP-File }

   {++ Moskaliov Business Graphics BUILD#150 17.01.01}
  rn_ChartsLib = 1274; { TChartsLib-Stream Type}
  rn_ChartKind = 1275;
   {-- Moskaliov Business Graphics BUILD#150 17.01.01}

  rn_ImageV34 = 1280;
  rn_SImageV31 = 1282; { Symbol-Images Stream Typen }
  rn_SImageV32 = 1283;
  rn_SImageV321 = 1284;
  rn_SImageV33 = 1285;
  rn_SImageV34 = 1286;

{ SymLib Konstanten f�r Symboleditor }
  sym_Project = 0;
  sym_SymbolMode = 1;
  sym_Omega = 4; {++Sygsky: Omega}
  sym_SYMBOLSIZE = 100000; {cm entspricht -1000..+1000 Meter }
  sym_EditBtn = $1; { F�r Symboldialog, welche Schalter }
  sym_DelBtn = $2; { angezeigt werden }
  sym_CopyBtn = $4; { Symbol-Kopier Button }
  sym_SearchBtn = $8; { Such-Button }
  sym_PropBtn = $10; { Eigenschaften-Button }
  sym_DefaultBtns = $0; { keine Spezialschalter }

  id_Exit = 120;
  id_NewProj = 121;
  id_OpenProj = 122;
  id_SaveProj = 123;
  id_SaveAs = 124;
  id_Close = 125;
  id_Import = 126;
  id_ImpASCII = 127;
  id_ImpDXF = 128;
  id_Export = 129;
  id_ExpASCII = 130;
  id_ExpDXF = 131;
  id_LoadBitmap = 132;
  id_ShowBitFrame = 133;
  id_TurboRaster = 134;
  id_PartPict = 135;
  id_RasterPict = 136;
  id_PrintGraph = 137;
  id_ChangePrinter = 138;
  id_Modules = 139;
  id_ModulesGPS = 140;
  id_ModulesInstall = 141;
  id_Multimedia = 142;
  id_AddDelMM = 143;
  id_PlayMM = 144;
  id_Convert = 145;
  id_LoadNext = 146;
  id_DBLoadNext = 147;
  id_RouteNew = 148;
  id_RouteOpen = 149;
  id_RouteSave = 150;
  id_RouteSaveAs = 151;
  id_RouteWPOpen = 152;
  id_RouteWPClose = 153;
  id_RouteSpeed = 154;
  id_RouteExit = 155;

  id_IslandArea = 178;
  id_BMPTransp = 179;
  id_Cut = 180;
  id_Copy = 181;
  id_Paste = 182;
  id_Delete = 183;
  id_TextEdit = 184;
  id_DeselectAll = 185;
  id_InsOLEObj = 186;
  id_InsLinkObj = 187;
  id_EditObj = 188;
  id_ObjectStyle = 189;

      {id_RefPoints      = 190;}
  id_DigiPoint = 191;
  id_DigiLine = 192;
  id_DigiPoly = 193;
  id_LineSnap = 194;
  id_ManInpCoord = 195;
  id_NewRefPoints = 196;
  id_AddRefPoints = 197;
  id_TransformDlg = 198;
  id_ConstructFS = 199;
  id_ConstructS = 200;

      {id_NeighPoint     = 401;
      id_NeighCircle    = 402;
      id_NeighPoly      = 403;
      id_SelCircle      = 405;
      id_SelPoly        = 406;}
  id_CopyToLayer = 407;
  id_AreaCut = 408;
  id_AutoSnap = 409;
  id_Explode = 411;
  id_Area = 413;
  id_Border = 414;
  id_ConstructOpt = 415;
      {id_MesPoint       = 416;
      id_MesCut         = 417;
      id_MesOrtho       = 418;}
  id_ToCurve = 419;
  id_RTransform = 423;
  id_RTrafoLin = 424;
  id_RTrafo = 425;
  id_ProjBorder = 426;
  id_ProjScript = 427;
  id_Legende = 428;
  id_AreaAnalyses = 429;

  id_BSelArea = 430;
  id_BSelCircle = 431;
  id_BSelRect = 432;
  id_BSelPoly = 433;
  id_BSelOffset = 434;

  id_FirstBSel = 430;
  id_LastBSel = 434;

  id_BNeighArea = 440;
  id_BNeighCircle = 441;
  id_BNeighRect = 442;
  id_BNeighPoly = 443;
  id_BNeighOffset = 444;
  id_BNeighWithArea = 445;

  id_FirstBNeigh = 440;
  id_LastBNeigh = 445;

  id_DistMark = 450;
  id_OrthoMark = 451;
  id_CircleCutMark = 452;
  id_AngleMark = 453;

  id_FirstCunstruct = 450;
  id_LastConstruct = 453;

  id_MesPoint = 460;
  id_MesCut = 461;
  id_MesOrtho = 462;

  id_FirstMeasure = 460;
  id_LastMeasure = 462;

  id_OverAll = 504;
  id_Layer = 505;
  id_InpCoord = 508;

  id_ZoomOut = 517;

  id_Snap = 580;
  id_NextSel = 584;
  id_NextLabSel = 588;

  id_Sight = 590;
  id_SightL = 591;
  id_SightS = 592;
  id_Redraw = 593;
  id_ShowAllSel = 594; {0704H}

  id_NonTransparent = 581; {~Ray}
  id_TranspSelect = 582;
  id_TranspLayer = 583;
  id_TranspModes = 585;

  id_FirstDrawRb = 560;
  id_LastDrawRb = 564;

  id_Points = 560;
  id_Poly = 561;
  id_CPoly = 562;
  id_Circle = 563; {0404F}
  id_Arc = 564; {0404F}

  id_Constructis = 780;

  id_DBSelCircle = 798;
  id_DBSelRect = 799;
  id_DBMonitor = 800;
  id_DBEdit = 801;
  id_DBWechseln = 802;
  id_DBText = 803;
  id_DBGraph = 804;
  id_Point = 805;
  id_dB = 806;
  id_DBNewLoad = 807;
  id_DBObjInfos = 808;
  id_PointDKM = 819;

  id_DDE0 = 809;
  id_DDE1 = 810;
  id_DDE2 = 811;
  id_DDE3 = 812;
  id_DDE4 = 813;
  id_DDE5 = 814;
  id_DDE6 = 815;
  id_DDE7 = 816;
  id_DDE8 = 817;
  id_DDE9 = 818;

  id_Priority = 820;
  id_CPassword = 821;
  id_TextStyle = 822;
  id_MesOption = 823;
  id_CombineO = 824;
  id_SnapOpt = 825;
  id_ZoomOpt = 826;
  id_BitmapOpt = 827;
  id_SymbolBib = 828;
  id_DefSymb = 829;
  id_InputDev = 830;
  id_DigOpt = 831;
  id_Options = 832;
  id_MakeSymbol = 833;
  id_ColorPalette = 834;
  id_BmpPalette = 835;
  id_DBSelLayer = 836;
  id_InstModul = 837;
  id_GrayPalette = 838;

  id_CascadeChildren = 840;
  id_TileChildren = 841;
  id_ArrangeIcons = 842;
  id_CloseChildren = 843;

  id_Help = 845;
  id_HelpHelp = 846;
  id_Info = 847;
  id_Info_2 = 848;
  id_Info_3 = 849;

  id_DDE10 = 860;
  id_DDE11 = 861;
  id_DDE12 = 862;
  id_DDE13 = 863;
  id_DDE14 = 864;
  id_DDE15 = 865;
  id_DDE16 = 866;
  id_DDE17 = 867;
  id_DDE18 = 868;
  id_DDE19 = 869;

  id_Select = 900;
  id_Draw = 901;
  id_Symbol = 902;
  id_Text = 903;
  id_Neighbour = 904;
  id_Selection = 905;
  id_Measure = 906;
  id_Generate = 907;
  id_Combine = 908;
  id_LineCut = 909;
  id_EOffset = 910;
  id_Distance = 911;
  id_Move = 912;
  id_PointMove = 913;
  id_PointInsert = 914;
  id_PointDelete = 915;
  id_BNeighbour = 916;
  id_BSelection = 917;
  id_CSelArea = 918;
  id_CSelCircle = 919;
  id_CSelRect = 920;
  id_CSelPoly = 921;
  id_CSelOffset = 922;
  id_CNeighArea = 923;
  id_CNeighCircle = 924;
  id_CNeighRect = 925;
  id_CNeighPoly = 926;
  id_CNeighOffset = 927;
  id_CNeighWithArea = 928;
  id_RefPoints = 929;
  id_RefPointsAdd = 930;
                          {����������}
      {id_DistMark       = 931;
      id_OrthoMark      = 932;
      id_CircleCutMark  = 933;
      id_AngleMark      = 934;}
      {id_MesPoint       = 935;
      id_MesCut         = 936;
      id_MesOrtho       = 937;}

  id_FirstRb = 900;
  id_LastRb = 930;

  id_Info2 = 998;
  id_Info3 = 999;

  id_Test = 10000;
  id_Test1 = 10001;
  id_Test2 = 10002;
  id_Test3 = 10003;
{ Ende  AM_Menu Konstanten ( f�r Buttonleiste)}

{Button IDs f�r nachbarsuche, Selektionsdialoge}
  idSelectByPoly = 1200;
  idSelectByCircle = 1201;
  idSelectByRect = 1202;
  idSelectByLine = 1203;
  idSelectByOffset = 1204;
  idSelectByPoint = 1205;

      { Konstanten f�r TLayers.InsertLayer AWhere                            }
  ilTop = 0; { als neuen Toplayer einf�gen         }
  ilSecond = 1; { als zweiten Layer einf�gen          }
  ilLast = 2; { als letzten Layer einf�gen          }

  StrBufSize = 32000;

  bsSelLayer = 100;

  slDevName = 50;
  slPortDef = 100;
  slLibName = 50;
  slPortName = 10;
  slPrnName = 50;
  slLongInt = 20;

  CommaChar = '.';
  ThousandChar = ',';

  ciSnapInfo = 2;
  ceSnapInfo = 2;

  Rad2Deg = 180 / Pi;
  Deg2Rad = Pi / 180;

  CNoRedraw = 0;
  CDoRedraw = 1;
  COldRedraw = 2;

  symScFromLayer = -1.0;
  symAngFromLayer = -1.0;
  symDistFromLayer = $1000000;

  trNotTransp = 0;
  trTransp = 1;
  trFromLayer = 2;

var
  DBLoaded: Integer = 0;
  DDEMLLoaded: Integer = 0;
  MDIChildType: string;
  Convert: Boolean;
    // name and version of the project that is currently loaded
  ProjectVersion: Byte;
  ProjectName: string;
  MaxObjects: LongInt = MaxLongInt; {sl}
  ShowAsCanvas: Boolean = FALSE;
  ParamPwd: string;
  CanChangePwd: Boolean = TRUE;
  OldMemMgr: TMemoryManager;
  SingleProj: Boolean = False;
  bMouseZoom: Boolean = True;
  CanvasHandle: Integer = 0;
  DoTerminate: Boolean = False;
  InputColor: Word = c_LBlue;
  InputLine: Word = lt_Dot;

  RunAsCanvas: Boolean = False;
  RunUpdateSilent: Boolean = False;
  RunUpdateOnClose: Boolean = False;
  RunRemote: Boolean = False;
  UpdateLink: AnsiString = '';

const
  UPDATE_INTERVAL = 3; // days

const
  OutputToPlotter: Boolean = FALSE;

const
  Tolerance = 1E-4; { Tolernaz f�r Float-Vergleiche                   }

type
  PColorRef = ^TColorRef;
  PWord = ^Word;

  TCRArray = array[0..271] of TColorRef;
  PCRArray = ^TCRArray;
  PPCRArray = ^PCRArray;

  PCopyDataStruct = ^TCopyDataStruct;
  TCopyDataStruct = packed record
    dwData: LongInt;
    cbData: LongInt;
    lpData: Pointer;
  end;

  TRectToPoint = record
    P1: TPoint;
    P2: TPoint;
  end;

  PBoolean = ^Boolean;

  PALong = ^TALong;
  TALong = array[0..MaxShortInt div 2 - 1] of LongInt;

  PAReal = ^TAReal;
  TAReal = array[0..MaxShortInt div 3 - 1] of Real;

  PAByte = ^TAByte;
  TAByte = array[0..MaxShortInt] of Byte;

  PAWord = ^TAWord;
  TAWord = array[0..MaxShortInt - 1] of Word;

  PAnnot = ^TAnnot; { Objektstruktur f�r den Spaltendialog bei Text�bernahme }
  TAnnot = record
    LineName: PChar;
    LineValue: PChar;
    HasValue: Boolean;
  end;

  PNameColl = ^TNameColl; { Objektstruktur f�r den Spaltendialog bei Diagramm�bernahme }
  TNameColl = record
    Name: PString;
    Color: LongInt;
    Pen: Word;
    Pattern: Word;
  end;

  PDGRLayerData = ^TDGRLayerData; { Objektstruktur f�r Einf�gelayer bei der Objektgenerierung }
  TDGRLayerData = record
    LayerName: PChar;
    NewLName: PChar;
  end;

  POverviewData = ^TOverviewData;
  TOverviewData = object(TOldObject)
    ViewName: PChar;
    LayerName: PChar;
    constructor Init(VName: PChar; LName: PChar);
    destructor Done; virtual;
  end;

  PLSaveData = ^TLSaveData;
  TLSaveData = object(TOldObject)
    LayerIndex: LongInt;
    LayerOff: Boolean;
    GeneralOn: Boolean;
    GeneralMin: Double;
    GeneralMax: Double;
    constructor Init(ALayerIndex: LongInt; ALayerOff: Boolean; AGeneralOn: Boolean; AGeneralMin: Double; AGeneralMax: Double);
    destructor Done; virtual;
  end;

  PAnnotColl = ^TAnnotColl;
  TAnnotColl = object(TOldObject)
    Name: PChar;
    HasVal: Boolean;
    constructor Init(LName: PChar; HVal: Boolean);
    constructor Load(S: TOldStream);
    destructor Done; virtual;
    procedure Store(S: TOldStream);
  end;

  PDGraph = ^TDGraph;
  TDGraph = object(TOldObject)
    Name: PChar;
    Value: Real48;
    Color: LongInt;
    Pen: Word;
    Pattern: Word;
    constructor Init(AName: PChar; AValue: Real; AColor: LongInt; APen, APattern: Word);
    destructor Done; virtual;
    constructor Load(S: TOldStream);
    procedure Store(S: TOldStream);
  end;

  PSSizeInfo = ^TSSizeInfo;
  TSSizeInfo = object(TOldObject)
    Index: LongInt;
    ShowRSize: Boolean;
    ShowSize: Real48;
    constructor Init(AIndex: LongInt; AShowRSize: Boolean; AShowSize: Real);
    destructor Done; virtual;
    constructor Load(S: TOldStream);
    procedure Store(S: TOldStream);
  end;

  PDPoint = ^TDPoint;
  TDPoint = object(TOldObject)
    X: Longint;
    Y: Longint;

    constructor Init(XPos, YPos: longint);
    constructor Load(S: TOldStream);

    function CalculateAngle(Pos: TDPoint): Real;
    function Dist(Pos: TDPoint): Double;
    function IsDiff(var Pos: TDPoint): Boolean;
    procedure Move(XMove, YMove: Double);
    procedure MoveAngle(Angle: Real; ADist: LongInt);
    procedure Rotate(Angle: Real);
    procedure Scale(XScale, YScale: Real); virtual;
    procedure Store(S: TOldStream);
    function XDist(var Pos: TDPoint): Double;
    function YDist(var Pos: TDPoint): Double;
  end;

  PDRect = ^TDRect;
  TDRect = object(TOldObject)
    A: TDPoint;
    B: TDPoint;

    constructor Init;
    constructor InitByRect(ARect: TDRect);
    constructor Load(S: TOldStream);
    destructor Done; virtual;

    procedure Assign(X1, Y1, X2, Y2: longint);
    procedure AssignByPoint(Point: TDPoint; Width: LongInt);
    function CenterX: Double;
    function CenterY: Double;
    procedure CorrectByPoint(Point: TDPoint);
    procedure CorrectByRect(Rect: TDRect);
    procedure Grow(Dist: LongInt);
    procedure Inflate(const X, Y: Double);
    function IsEmpty: Boolean;
    function IsEqual(Rect: TDRect): Boolean;
    function Empty: Boolean;
    function IsInside(Rect: TDRect): Boolean;
    function IsInterior(Rect: TDRect; Frame: Boolean): Boolean;
    function IsPartInside(Rect: TDRect): Boolean;
    function Outside(Rect: TDRect): Boolean;
    function IsOutside(Rect: TDRect): Boolean;
    function IsVisible(Rect: TDRect): Boolean;
    procedure Intersect(Rect1: TDRect; var Inter: TDRect);
    procedure MoveRel(XMove, YMove: Longint);
    function PointInside(Pos: TDPoint): Boolean;
    procedure Scale(XScale, YScale: Real); virtual; {2705F}
    procedure Store(S: TOldStream);
    function XSize: Double;
    function YSize: Double;
    function Border: Pointer;
    procedure CorrectSize;
  end;

  TGrayDialog = class(TRDialogWindow)
    procedure GetItemRect(ID: Integer; var Rect: TRect);
    function GetFieldDouble(AId: Integer; var AValue: Double): Boolean;
    function GetFieldDoubleZero(AId: Integer; var AValue: Double): Boolean;
    function GetFieldLong(AId: Integer; var AValue: LongInt): Boolean;
    function GetFieldLongWithoutComma(AId: Integer; var AValue: LongInt): Boolean;
    function GetFieldLongZero(AId: Integer; var AValue: LongInt): Boolean;
    function GetFieldText(AId: Integer): string;
    procedure SetFieldLong(AId: Integer; AValue: LongInt);
    procedure SetFieldLongWithoutComma(AId: Integer; AValue: LongInt);
    procedure SetFieldLongZero(AId: Integer; AValue: LongInt);
    procedure SetFieldText(AId: Integer; const AText: string);
    procedure SetupWindow; override;
    procedure WMInitDialog(var Msg: TMessage); message wm_First + wm_InitDialog;
  end;

  TGrayDlgWin = class(TRDialogWindow)
  end;

  PSortedLong = ^TSortedLong;
  TSortedLong = object(TSortedCollection)
    function Compare(Key1, Key2: Pointer): Integer; virtual;
    procedure FreeItem(Item: Pointer); virtual;
  end;

  LongType = record
    case Word of
      0: (Ptr: Pointer);
      1: (Long: LongInt);
      2: (Lo, Hi: Word);
  end;

var
  MaxSize: TDRect;

//++ Glukhov Bug#63 BUILD#128 14.09.00
// Check a Chosen font for the given Font Settings and Global font list
// Return either current or some recommended value of Font
function CheckChosenFont(FSettings: TFontSettings; GFonts: PFonts): Integer16;
//-- Glukhov Bug#63 BUILD#128 14.09.00

function Sign(AValue: Real): Real;

function ExtractStringItems(var D: array of string; S: AnsiString; Sep: Char): Integer;

procedure ParseString(AStr: string; var ToStr: string; SepChar: Char; PPos: Integer);
procedure ParsePChar(AStr: PChar; ToStr: PChar; SepChar: Char; PPos: Integer);

function GetBrush(APatCol: TColorRef; APat: Word; APattern: Pointer): HBrush;

function GetPen(ACol: TColorRef; ALine: Word; AWidth: Integer): HPen;

function Max(Val1, Val2: LongInt): LongInt;

function Min(Val1, Val2: LongInt): LongInt;

procedure PStrToPChar(Dest: PChar; Source: PString);

function PToStr(AStr: PString): string;

function FormatStr(Value: Double; AComma: Integer): string;

function MsgBox(Owner: HWnd; ErNum: Integer; ErType: Integer; const ExtStr: string): Integer;

function MsgBox2(Owner: HWnd; ErNum: Integer; ErType: Integer; const ExtStr1: string; const ExtStr2: string): Integer;

procedure GetDeviceName(PName, DName, PPort: PChar);

function TwoHigh(AExp: Integer): Integer;

procedure ChangeBit(var AData: Word; ABits: Word; ASet: Boolean);

function BitSet(AData, ABit: Word): Boolean;

procedure SetBits(var AData: Word; ABit: Word);

procedure ClearBits(var AData: Word; ABit: Word);

procedure DlgScale(Dlg: THandle; var XPos, YPos: Integer);

procedure DeleteBlanks(AText: PChar);

procedure MakeValStr(AText: PChar);

function MDIChildMax(MenHandle: THandle): Integer;

function UpStr(InStr: string): string;

function CheckFileExist(Name: PChar): Boolean;

procedure CorrectRect(var Rect: TRect; Point: TPoint);

function LimitToLong(AValue: Double): LongInt;

function LimitToInt(AValue: Double): Integer;

function LongToHex(Num: LongInt; Len: Integer): string;

function HexToLong(AHex: string): LongInt;

function MakeObjectCopy(AObject: POldObject): Pointer;

function PStrIComp(In1: PShortString; In2: PShortString): Integer;

function BoolSign(AValue: Double): Boolean;

function UpString(Str: string): string;

function CCW(var P0, P1, P2: TDPoint): Integer;

function GetSepPos(AStr: PChar; ASep: Char; AEntry, AIndex: Integer): Integer;

function NextDDEPart(DDEStr: PChar): PChar;

function ReadDDEString(var DDEStr: PChar; Separator: Char; var Value: string): Boolean;

function ReadDDEText(var DDEStr: PChar; Separator: Char; Value: PChar): Boolean;

function ReadIniText(var IniStr: PChar; Separator, LastChar: Char; Value: PChar): Boolean;

procedure ReadLineStyleFromRegistry(Registry: TCustomRegistry; const KeyName: string; var LineStyle: TLineStyle);
procedure WriteLineStyleToRegistry(Registry: TCustomRegistry; const KeyName: string; const LineStyle: TLineStyle);
procedure ReadFillStyleFromRegistry(Registry: TCustomRegistry; const KeyName: string; var FillStyle: TFillStyle);
procedure WriteFillStyleToRegistry(Registry: TCustomRegistry; const KeyName: string; const FillStyle: TFillStyle);

function GetLangText(AId: Integer): string;

function GetLangPChar(AID: Integer; Str: PChar; MaxLen: Integer): PChar;

function SystemErrorText: AnsiString;
{++Sygsky}
function CStyleFormat(const Str: Ansistring): AnsiString;
function ReplaceText(const InsStr, DelStr, Str: Ansistring): AnsiString;
{--Sygsky}

function EncryptString(s: string): string;
function IsSuperPwd(s: string): Boolean;

function ValidateLayername(s: string): string;

procedure CheckLanguageEntries(AShowDialog: Boolean);
function ChangeLanguageEntries: AnsiString;

function GetRAMSizeMB: Integer;

var
  CurrentLanguageID: AnsiString;

const
  STARTPARAM_CANVAS = '/canvas';
  STARTPARAM_SINGLEPROJ = '/e';
  STARTPARAM_COMMAND = '/c';
  STARTPARAM_PROJECT = '/p';
  STARTPARAM_PASSWORD = '/pwd';
  STARTPARAM_PMS = '/pms';
  STARTPARAM_REG = '/regserver';
  STARTPARAM_UNREG = '/unregserver';
  STARTPARAM_BMASYNC = '/bmasync';
  STARTPARAM_UPDATE = '/update';
  STARTPARAM_CLOUD = '/cloud';

var
  BingMapsAsyncMode: Boolean = False;

implementation

uses Forms, AM_Main, WinDOS, AM_CPoly, UserIntf, MultiLng, Dialogs, VerMgr, Registry,
  UserUtils, SelLngDlg, FileCtrl, WinOSInfo;

var
  MainSectionNumber: Integer = -1;

{*******************************************************************************
| Function NextDDEPart
|-------------------------------------------------------------------------------
| Liefert einen Zeiger auf den n�chsten DDE-Abschnitt im DDEString. Der Zeiger
| zeigt auf das erste Zeichen nach der eckigen Klammer oder ist NIL, wenn keine
| eckige Klammer mehr vorhanden ist.
*******************************************************************************}

function NextDDEPart
  (
  DDEStr: PChar
  )
  : PChar;
begin
  if DDEStr = nil then
    Result := nil
  else
    Result := AnsiStrScan(DDEStr, '[');
  if Result <> nil then
  begin
    Result := Result + 1;
    if Result^ = #0 then
      Result := nil;
  end;
end;

function ReadDDEString
  (
  var DDEStr: PChar;
  Separator: Char;
  var Value: string
  )
  : Boolean;
begin
  Result := ReadDDEText(DDEStr, Separator, @Value[1]);
  Value[0] := Char(StrLen(@Value[1]));
end;

{*******************************************************************************
| Function ReadDDEText
|-------------------------------------------------------------------------------
| Kopiert den Text von DDEStr bis zum Auftreten des Separators bzw. der n�chsten
| schlie�enden, eckigen Klammer nach Value. Der Funktionswert gibt an, ob bis
| zum Separator oder bis zu eckigen Klammer kopiert wurde. DDEStr wird auf
| das Zeichen nach dem Separator bzw. auf das Zeichen nach der n�chsten
| �ffnenden Klammer gesetzt.
*******************************************************************************}

function ReadDDEText
  (
  var DDEStr: PChar;
  Separator: Char;
  Value: PChar
  )
  : Boolean;
var
  TokenPos: PChar;
  BracketPos: PChar;
begin
  if DDEStr = nil then
  begin
    StrCopy(Value, '');
    Result := TRUE;
  end
  else
  begin
    TokenPos := AnsiStrScan(DDEStr, Separator);
    BracketPos := AnsiStrScan(DDEStr, ']');
    if (TokenPos = nil) or (TokenPos > BracketPos) then
      TokenPos := BracketPos;
    if TokenPos = nil then
    begin
      StrCopy(Value, DDEStr);
      DDEStr := nil;
    end
    else
    begin
      StrLCopy(Value, DDEStr, TokenPos - DDEStr);
      DDEStr := TokenPos + 1;
      if DDEStr^ = #0 then
        DDEStr := nil
      else
        if TokenPos = BracketPos then
        begin
          DDEStr := AnsiStrScan(DDEStr, '[');
          if DDEStr <> nil then
            Inc(DDEStr);
        end;
    end;
    Result := TokenPos = BracketPos;
  end;
end;

function ReadIniText
  (
  var IniStr: PChar;
  Separator: Char;
  LastChar: Char;
  Value: PChar
  )
  : Boolean;
var
  TokenPos: PChar;
  BracketPos: PChar;
begin
  if IniStr = nil then
  begin
    StrCopy(Value, '');
    Result := TRUE;
  end
  else
  begin
    TokenPos := AnsiStrScan(IniStr, Separator);
    BracketPos := AnsiStrScan(IniStr, LastChar);
    if (TokenPos = nil) or (TokenPos > BracketPos) then
      TokenPos := BracketPos;
    if TokenPos = nil then
    begin
      StrCopy(Value, IniStr);
      IniStr := nil;
    end
    else
    begin
      StrLCopy(Value, IniStr, TokenPos - IniStr);
      IniStr := TokenPos + 1;
      if IniStr^ = #0 then
        IniStr := nil
      else
        if TokenPos = BracketPos then
        begin
          IniStr := AnsiStrScan(IniStr, '[');
          if IniStr <> nil then
            Inc(IniStr);
        end;
    end;
    Result := TokenPos = BracketPos;
  end;
end;

function GetSepPos
  (
  AStr: PChar;
  ASep: Char;
  AEntry: Integer;
  AIndex: Integer
  )
  : Integer;
var
  TempStr: PChar;
  Cnt: Integer;
  TokenPos: PChar;
  BracketPos: PChar;
begin
  TempStr := AStr;
  for Cnt := 0 to AEntry do
    TempStr := NextDDEPart(TempStr);
  BracketPos := AnsiStrScan(TempStr, ']');
  TokenPos := TempStr;
  for Cnt := 1 to AIndex do
  begin
    TokenPos := AnsiStrScan(TokenPos, ASep);
    if TokenPos = nil then
      Break
    else
      TokenPos := TokenPos + 1;
  end;
  if (TokenPos = nil) or (TokenPos > BracketPos) then
    TokenPos := BracketPos;
  if TokenPos = nil then
    Result := StrLen(AStr)
  else
    Result := TokenPos - AStr;
end;

function CCW
  (
  var P0: TDPoint;
  var P1: TDPoint;
  var P2: TDPoint
  )
  : Integer;
var
  DX1, DX2, DY1, DY2: Double;
begin
  DX1 := P1.X - P0.X;
  DY1 := P1.Y - P0.Y;
  DX2 := P2.X - P0.X;
  DY2 := P2.Y - P0.Y;
  if Abs(DX1 * DY2 - DY1 * DX2) < 1E-5 then
  begin
    if (DX1 * DX2 < 0)
      or (DY1 * DY2 < 0) then
      CCW := -1
    else
      if (DX1 * DX1 + DY1 * DY1) >= (DX2 * DX2 + DY2 * DY2) then
        CCW := 0
      else
        CCW := 1;
  end
  else
    if DX1 * DY2 > DY1 * DX2 then
      CCW := 1
    else
      CCW := -1
end;

  {**********************************************************************************}
  { Function BoolSign                                                                }
  {----------------------------------------------------------------------------------}
  { Liefert TRUE, wenn AValue nagativ ist, sonst FALSE.                              }
  {**********************************************************************************}

function BoolSign
  (
  AValue: Double
  )
  : Boolean;
begin
  if AValue < 0 then
    BoolSign := TRUE
  else
    BoolSign := FALSE;
end;

constructor TOverviewData.Init
  (
  VName: PChar;
  LName: PChar
  );
begin
  inherited Init;
  ViewName := StrNew(VName);
  LayerName := StrNew(LName);
end;

destructor TOverviewData.Done;
begin
  inherited Done;
end;

constructor TLSaveData.Init
  (
  ALayerIndex: LongInt;
  ALayerOff: Boolean;
  AGeneralOn: Boolean;
  AGeneralMin: Double;
  AGeneralMax: Double
  );
begin
  LayerIndex := ALayerIndex;
  LayerOff := ALayerOff;
  GeneralOn := AGeneralOn;
  GeneralMin := AGeneralMin;
  GeneralMax := AGeneralMax;
end;

destructor TLSaveData.Done;
begin
  inherited Done;
end;

constructor TAnnotColl.Init
  (
  LName: PChar;
  HVal: Boolean
  );
begin
  Name := LName;
  HasVal := HVal;
end;

destructor TAnnotColl.Done;
begin
  StrDispose(Name);
  inherited Done;
end;

constructor TAnnotColl.Load
  (
  S: TOldStream
  );
begin
  Name := S.StrRead;
  S.Read(HasVal, SizeOf(HasVal));
end;

procedure TAnnotColl.Store
  (
  S: TOldStream
  );
begin
  S.StrWrite(Name);
  S.Write(HasVal, SizeOf(HasVal));
end;

constructor TDGraph.Load
  (
  S: TOldStream
  );
begin
  Name := S.StrRead;
  S.Read(Value, SizeOf(Value));
  S.Read(Color, SizeOf(Color));
  S.Read(Pen, SizeOf(Pen));
  S.Read(Pattern, SizeOf(Pattern));
end;

procedure TDGraph.Store
  (
  S: TOldStream
  );
begin
  S.StrWrite(Name);
  S.Write(Value, SizeOf(Value));
  S.Write(Color, SizeOf(Color));
  S.Write(Pen, SizeOf(Pen));
  S.Write(Pattern, SizeOf(Pattern));
end;

constructor TDGraph.Init
  (
  AName: PChar;
  AValue: Real;
  AColor: LongInt;
  APen: Word;
  APattern: Word
  );
begin
  TOldObject.Init;
  Name := AName;
  Value := AValue;
  Color := AColor;
  Pen := APen;
  Pattern := APattern;
end;

destructor TDGraph.Done;
begin
  StrDispose(Name);
  inherited Done;
end;

constructor TSSizeInfo.Init
  (
  AIndex: LongInt;
  AShowRSize: Boolean;
  AShowSize: Real
  );
begin
  inherited Init;
  Index := AIndex;
  ShowRSize := AShowRSize;
  ShowSize := AShowSize;
end;

destructor TSSizeInfo.Done;
begin
  inherited Done;
end;

constructor TSSizeInfo.Load
  (
  S: TOldStream
  );
begin
  S.Read(Index, SizeOf(Index));
  S.Read(ShowRSize, SizeOf(ShowRSize));
  S.Read(ShowSize, SizeOf(ShowSize));
end;

procedure TSSizeInfo.Store
  (
  S: TOldStream
  );
begin
  S.Write(Index, SizeOf(Index));
  S.Write(ShowRSize, SizeOf(ShowRSize));
  S.Write(ShowSize, SizeOf(ShowSize));
end;

constructor TDPoint.Load
  (
  S: TOldStream
  );
begin
  S.Read(X, SizeOf(X));
  S.Read(Y, SizeOf(Y));
end;

procedure TDPoint.Store
  (
  S: TOldStream
  );
begin
  S.Write(X, SizeOf(X));
  S.Write(Y, SizeOf(Y));
end;

procedure TDPoint.Scale {2705F}
  (
  XScale: Real;
  YScale: Real
  );
begin
  X := LimitToLong(X * XScale);
  Y := LimitToLong(Y * YScale);
end;

procedure TDRect.Scale
  (
  XScale: Real;
  YScale: Real
  );
begin
  A.Scale(XScale, YScale);
  B.Scale(XScale, YScale);
end; {2705F}

constructor TDPoint.Init
  (
  XPos: longint;
  YPos: longint
  );
begin
  TOldObject.Init;
  X := XPos;
  Y := YPos;
end;

procedure TDPoint.Move
  (
  XMove: Double;
  YMove: Double
  );
begin
  X := LimitToLong(X + XMove);
  Y := LimitToLong(Y + YMove);
end;

function TDPoint.XDist
  (
  var Pos: TDPoint
  )
  : Double;
begin
  XDist := X * 1.0 - Pos.X;
end;

function TDPoint.YDist
  (
  var Pos: TDPoint
  )
  : Double;
begin
  YDist := Y * 1.0 - Pos.Y;
end;

function TDPoint.IsDiff
  (
  var Pos: TDPoint
  )
  : Boolean;
begin
  IsDiff := (X <> Pos.X) or (Y <> Pos.Y);
end;

function TDPoint.Dist
  (
  Pos: TDPoint
  )
  : Double;
var
  Alpha: Real;
  YDiff: Double;
  XDiff: Double;
begin
  YDiff := Abs(YDist(Pos));
  XDiff := Abs(XDist(Pos));
  if XDiff = 0 then
    Dist := YDiff
  else
    if YDiff = 0 then
      Dist := XDiff
    else
    begin
      Alpha := ArcTan(YDiff / XDiff);
      Dist := Abs(YDiff / Sin(Alpha));
    end;
end;

procedure TDPoint.Rotate
  (
  Angle: Real
  );
var
  Beta: Real;
  Point: TDPoint;
  ADist: Double;
begin
  Point.Init(0, 0);
  Beta := Point.CalculateAngle(Self) + Angle;
  ADist := Dist(Point);
  X := Round(ADist * Cos(Beta));
  Y := Round(ADist * Sin(Beta));
end;

constructor TDRect.Init;
begin
  TOldObject.Init;
  A.Init(MaxLongInt, MaxLongInt);
  B.Init(-MaxLongInt, -MaxLongInt);
end;

procedure TDRect.Assign
  (
  X1: longint;
  Y1: longint;
  X2: longint;
  Y2: longint
  );
begin
  A.X := X1;
  A.Y := Y1;
  B.X := X2;
  B.Y := Y2;
end;

constructor TDRect.Load
  (
  S: TOldStream
  );
begin
  A.Load(S);
  B.Load(S);
end;

procedure TDRect.Store
  (
  S: TOldStream
  );
begin
  A.Store(S);
  B.Store(S);
end;

destructor TDRect.Done;
begin
  A.Done;
  B.Done;
  inherited Done;
end;

function TDRect.PointInside
  (
  Pos: TDPoint
  )
  : Boolean;
begin
  PointInside := FALSE;
  if (Pos.X >= A.X) then
    if (Pos.Y >= A.Y) then
      if (Pos.X <= B.X) then
        if (Pos.Y <= B.Y) then
          PointInside := TRUE;
end;

function TDRect.IsEqual
  (
  Rect: TDRect
  )
  : Boolean;
begin
  IsEqual := not (A.IsDiff(Rect.A)) and not (B.IsDiff(Rect.B));
end;

function TDRect.IsInside
  (
  Rect: TDRect
  )
  : Boolean;
begin
  IsInside := FALSE;
  if Rect.A.X <= A.X then
    if Rect.A.Y <= A.Y then
      if Rect.B.X >= B.X then
        if Rect.B.Y >= B.Y then
          IsInside := TRUE;
end;

function TDRect.IsInterior
  (
  Rect: TDRect;
  Frame: Boolean
  )
  : Boolean;
begin
  IsInterior := IsInside(Rect); { The same as for IsInside }
  if ((A.x = B.x) or (A.y = b.y)) and (not frame) then
  begin
    if (A.x = Rect.A.x) or (A.y = Rect.a.y) then
      IsInterior := False;
    if (b.x = Rect.b.x) or (b.y = Rect.b.y) then
      IsInterior := False;
  end;
  if isEqual(Rect) then
    IsInterior := False;
end;

function TDRect.IsPartInside

(
  Rect: TDRect
  )
  : Boolean;
begin
  Result := (not Outside(Rect));
end;

function TDREct.Outside {Main method to detect non-intersection!}
  (
  Rect: TDRect {check of TDRect with A and B to be exclusively out of Rect}
  )
  : Boolean;
begin
  OutSide := FALSE;
  if (B.x < Rect.A.x) or
    (A.x > Rect.B.x) or
    (A.y > Rect.B.y) or
    (B.y < Rect.A.y) then
    OutSide := TRUE;
end;

function TDRect.IsOutside
  (
  Rect: TDRect {Check of Rect to be exclusively out of TDRect}
  )
  : Boolean;
begin
  Result := Outside(Rect); { The same value as for Outside!!!}
end;

function TDRect.IsVisible {The same value as for IsPartInside}
  (
  Rect: TDRect
  )
  : Boolean;
begin
  Result := not OutSide(Rect); { Keep it simple, stupid (KISS!) }
end;

procedure TDRect.CorrectByPoint
  (
  Point: TDPoint
  );
begin
  if A.X > Point.X then
    A.X := Point.X;
  if B.X < Point.X then
    B.X := Point.X;
  if A.Y > Point.Y then
    A.Y := Point.Y;
  if B.Y < Point.Y then
    B.Y := Point.Y;
end;

procedure TDRect.CorrectByRect
  (
  Rect: TDRect
  );
begin
  CorrectByPoint(Rect.A);
  CorrectByPoint(Rect.B);
end;

function TDRect.XSize
  : Double;
begin
  XSize := B.XDist(A);
end;

function TDRect.YSize
  : Double;
begin
  YSize := B.YDist(A);
end;

function GetBrush
  (
  APatCol: TColorRef;
  APat: Word;
  APattern: Pointer
  )
  : HBrush;
var
  Pattern: Integer;
begin
  GetBrush := 0;
  if APat = pt_Solid then
    GetBrush := CreateSolidBrush(APatCol)
  else
    if APat <= 7 then
    begin
      case APat of
        pt_FDiagonal: Pattern := hs_FDiagonal;
        pt_Cross: Pattern := hs_Cross;
        pt_DiagCross: Pattern := hs_DiagCross;
        pt_BDiagonal: Pattern := hs_BDiagonal;
        pt_Horizontal: Pattern := hs_Horizontal;
        pt_Vertical: Pattern := hs_Vertical;
      else
        Pattern := -1;
      end;
      if Pattern <> -1 then
        GetBrush := CreateHatchBrush(Pattern, APatCol);
    end
    else
      if APattern <> nil then
      begin
      //ShowMessage('GetBrush for Patterns no more supported');
        GetBrush := CreateSolidBrush(APatCol)
      end;
end;

function GetPen
  (
  ACol: TColorRef;
  ALine: Word;
  AWidth: Integer
  )
  : HPen;
var
  LineType: Integer;
begin
  case ALine of
    lt_Solid:
      if OutputToPlotter then
        LineType := ps_Solid
      else
        LineType := ps_InsideFrame;
    lt_Dash: LineType := ps_Dash;
    lt_Dot: LineType := ps_Dot;
    lt_DashDot: LineType := ps_DashDot;
  else
    LineType := ps_DashDotDot;
  end;
  GetPen := CreatePen(LineType, AWidth, ACol);
end;

function Max
  (
  Val1: LongInt;
  Val2: LongInt
  )
  : LongInt;
begin
  if Val1 > Val2 then
    Max := Val1
  else
    Max := Val2;
end;

function Min
  (
  Val1: LongInt;
  Val2: LongInt
  )
  : LongInt;
begin
  if Val1 < Val2 then
    Min := Val1
  else
    Min := Val2;
end;

procedure TDRect.Intersect
  (
  Rect1: TDRect;
  var Inter: TDRect
  );
begin
  Inter.Assign(Max(Rect1.A.X, A.X), Max(Rect1.A.Y, A.Y),
    Min(Rect1.B.X, B.X), Min(Rect1.B.Y, B.Y));
end;

procedure TDRect.AssignByPoint
  (
  Point: TDPoint;
  Width: LongInt
  );
begin
  A := Point;
  B := Point;
  A.Move(-Width, -Width);
  B.Move(Width, Width);
end;

procedure TDRect.MoveRel
  (
  XMove: LongInt;
  YMove: LongInt
  );
begin
  A.Move(XMove, YMove);
  B.Move(XMove, YMove);
end;

procedure TDRect.Grow
  (
  Dist: LongInt
  );
begin
  A.Move(-Dist, -Dist);
  B.Move(Dist, Dist);
end;

procedure TDRect.Inflate
  (
  const X: Double;
  const Y: Double
  );
begin
  A.Move(LimitToLong(-X), LimitToLong(-Y));
  B.Move(LimitToLong(X), LimitToLong(Y));
end;

procedure PStrToPChar
  (
  Dest: PChar;
  Source: PString
  );
var
  AText: string;
begin
  if Source = nil then
    AText := ''
  else
    AText := Source^;
  StrPCopy(Dest, AText);
end;

function TDPoint.CalculateAngle(Pos: TDPoint): Real;
var
  DX, DY: Double;
  Beta: Real;
const
  Gr90 = Pi / 2;
  Gr180 = Pi;
  Gr270 = 3 * Pi / 2;
  Gr360 = 2 * Pi;
begin
  DX := Pos.XDist(Self);
  DY := Pos.YDist(Self);
  if DX = 0 then
  begin
    if DY > 0 then
    begin
      Beta := Gr90;
    end
    else
    begin
      Beta := Gr270;
    end;
  end
  else
    if DY = 0 then
    begin
      if DX >= 0 then
      begin
        Beta := 0;
      end
      else
      begin
        Beta := Gr180;
      end;
    end
    else
    begin
      Beta := ArcTan(DY / DX);
      if (DX >= 0) then
      begin
        Beta := Beta + Gr360;
      end
      else
      begin
        Beta := Beta + Gr180;
      end;
      if Beta >= Gr360 then
      begin
        Beta := Beta - Gr360;
      end;
    end;
  Result := Beta;
end;

procedure EnableMenuCommand
  (
  AMenu: HMenu;
  AComm: Word;
  Enable: Boolean
  );
begin
  if Enable then
    EnableMenuItem(AMenu, AComm, mf_ByCommand or mf_Enabled)
  else
    EnableMenuItem(AMenu, AComm, mf_ByCommand or mf_Grayed)
end;

procedure CheckMenuCommand
  (
  AMenu: HMenu;
  AComm: Word;
  Check: Boolean
  );
begin
  if Check then
    CheckMenuItem(AMenu, AComm, mf_ByCommand or mf_Checked)
  else
    CheckMenuItem(AMenu, AComm, mf_ByCommand or mf_UnChecked)
end;

procedure EnableAllMenuCommands
  (
  AMenu: HMenu;
  Enable: Boolean
  );
begin
  if Enable then
  begin
    EnableMenuItem(AMenu, 0, mf_ByPosition or mf_Enabled);
    EnableMenuItem(AMenu, 1, mf_ByPosition or mf_Enabled);
    EnableMenuItem(AMenu, 2, mf_ByPosition or mf_Enabled);
    EnableMenuItem(AMenu, 3, mf_ByPosition or mf_Enabled);
    EnableMenuItem(AMenu, 4, mf_ByPosition or mf_Enabled);
    EnableMenuItem(AMenu, 5, mf_ByPosition or mf_Enabled);
    EnableMenuItem(AMenu, 6, mf_ByPosition or mf_Enabled);
    EnableMenuItem(AMenu, 7, mf_ByPosition or mf_Enabled);
  end
  else
  begin
    EnableMenuItem(AMenu, 0, mf_ByPosition or mf_Grayed);
    EnableMenuItem(AMenu, 1, mf_ByPosition or mf_Grayed);
    EnableMenuItem(AMenu, 2, mf_ByPosition or mf_Grayed);
    EnableMenuItem(AMenu, 3, mf_ByPosition or mf_Grayed);
    EnableMenuItem(AMenu, 4, mf_ByPosition or mf_Grayed);
    EnableMenuItem(AMenu, 5, mf_ByPosition or mf_Grayed);
    EnableMenuItem(AMenu, 6, mf_ByPosition or mf_Grayed);
    EnableMenuItem(AMenu, 7, mf_ByPosition or mf_Grayed);
  end;
end;

procedure EnableMenuPosition
  (
  AMenu: HMenu;
  APos: Word;
  Enable: Boolean
  );
begin
  if Enable then
    EnableMenuItem(AMenu, APos, mf_ByPosition or mf_Enabled)
  else
    EnableMenuItem(AMenu, APos, mf_ByPosition or mf_Grayed)
end;

function FormatStr
  (
  Value: Double;
  AComma: Integer
  )
  : string;
var
  Cnt: Integer;
  TLen: Integer;
  Text: string;
begin
  Str(Abs(Value): 0: 0, Text);
  TLen := Length(Text);
  if AComma > 0 then
  begin
    if TLen < AComma + 1 then
      Insert(Copy('000000', 1, AComma + 1 - TLen), Text, 1);
    TLen := Length(Text);
    Insert(CommaChar, Text, TLen - AComma + 1);
    Dec(TLen, AComma);
  end;
  Cnt := 1;
  while (Cnt < TLen) do
  begin
    if Cnt mod 3 = 0 then
      Insert(ThousandChar, Text, TLen - Cnt + 1);
    Inc(Cnt);
  end;
  if Value < 0 then
    FormatStr := '-' + Text
  else
    FormatStr := Text;
end;

procedure TGrayDialog.SetupWindow;
var
  AValue: Integer;
  Error: Integer;
  Text: array[0..255] of Char;
begin
  inherited SetupWindow;
  GetWindowText(Handle, Text, 255);
  Val(StrPas(Text), AValue, Error);
  if Error = 0 then
  begin
    GetLangPChar(AValue, Text, 255);
    SetWindowText(Handle, Text);
  end;
end;

{++Sygsky's changes}

function MsgBox
  (
  Owner: HWnd;
  ErNum: Integer;
  ErType: Integer;
  const ExtStr: string
  )
  : Integer;
begin
  Result := MsgBox2(Owner, ErNum, ErType, ExtStr, ''); { I like to shorten that :-}
end;

function MsgBox2
  (
  Owner: HWnd;
  ErNum: Integer;
  ErType: Integer;
  const ExtStr1: string;
  const ExtStr2: string
  ): Integer;
var
  ErStr: AnsiString;
  PosI: Integer;
begin
  ErStr := GetLangText(ErNum) + #0;
    {++Sygsky: add '\n' processing like C language}
  ErStr := CStyleFormat(ErStr);
    {--Sygsky}
  Posi := Pos('%', ErStr);
  if Posi <> 0 then
  begin
    Delete(ErStr, Posi, 1);
    Insert(ExtStr1, ErStr, Posi);
  end;
  Posi := Pos('%', ErStr);
  if Posi <> 0 then
  begin
    Delete(ErStr, Posi, 1);
    Insert(ExtStr2, ErStr, Posi);
  end;
  MsgBox2 := MessageBox(Owner, @ErStr[1], PChar(AnsiString(Application.Title)), ErType or MB_APPLMODAL); //To show msgbox in front
end;

procedure TGrayDialog.GetItemRect
  (
  ID: Integer;
  var Rect: TRect
  );
var
  Point: TPoint;
begin
  GetWindowRect(GetItemHandle(ID), Rect);
  Point.X := 0;
  Point.Y := 0;
  WinProcs.ScreenToClient(Handle, Point);
  OffsetRect(Rect, Point.X, Point.Y);
end;

procedure GetDeviceName
  (
  PName: PChar;
  DName: PChar;
  PPort: PChar
  );
var
  Printer: array[0..255] of char;
  CommaPtr: PChar;
  CommaPos: PChar;
begin
  GetProfileString('WINDOWS', 'DEVICE', '', Printer, SizeOf(Printer));
  StrCopy(PName, '');
  StrCopy(DName, '');
  StrCopy(PPort, '');
  CommaPtr := Printer;
  CommaPos := StrScan(CommaPtr, ',');
  if CommaPos <> nil then
  begin
    StrLCopy(PName, CommaPtr, (CommaPos - CommaPtr));
    CommaPtr := CommaPos + 1;
    CommaPos := StrScan(CommaPtr, ',');
    if CommaPos <> nil then
    begin
      StrLCopy(DName, CommaPtr, (CommaPos - CommaPtr));
      CommaPtr := CommaPos + 1;
      StrLCopy(PPort, CommaPtr, StrLen(CommaPtr));
    end;
  end;
end;

function TwoHigh
  (
  AExp: Integer
  )
  : Integer;
var
  AResult: Integer;
begin
  AResult := 1;
  AResult := AResult shl AExp;
  TwoHigh := AResult;
end;

function BitSet
  (
  AData: Word;
  ABit: Word
  )
  : Boolean; assembler;
asm MOV  AX,AData
      AND  AX,ABit
      JZ   @@1
      MOV  AX,1
  @@1:
end;

procedure SetBits
  (
  var AData: Word;
  ABit: Word
  );
begin
  AData := AData or ABit;
end;

procedure ClearBits
  (
  var AData: Word;
  ABit: Word
  );
begin
  AData := AData and not ABit;
end;

procedure DlgScale
  (
  Dlg: THandle;
  var XPos: Integer;
  var YPos: Integer
  );
var
  Rect: TRect;
begin
  Rect.Left := 0;
  Rect.Top := 0;
  Rect.Right := XPos;
  Rect.Bottom := YPos;
  MapDialogRect(Dlg, Rect);
  XPos := Rect.Right;
  YPos := Rect.Bottom;
end;

procedure DeleteBlanks
  (
  AText: PChar
  );
var
  Cnt: Integer;
  AStr: string;
begin
  AStr := StrPas(AText);
  Cnt := 1;
  while (Cnt <= Length(AStr)) do
  begin
    if AStr[Cnt] = ' ' then
      Delete(AStr, Cnt, 1)
    else
      Inc(Cnt);
  end;
  StrPCopy(AText, AStr);
end;

procedure MakeValStr
  (
  AText: PChar
  );
var
  Cnt: Integer;
  AStr: string;
  Vals: set of Char;
begin
  Vals := ['0'..'9', '.'];
  AStr := StrPas(AText);
  Cnt := 1;
  while (Cnt <= Length(AStr)) do
  begin
    if AStr[Cnt] in Vals then
      Inc(Cnt)
    else
      Delete(AStr, Cnt, 1);
  end;
  StrPCopy(AText, AStr);
end;

{************************************************************************************}
{ Function MDIChildMax                                                               }
{  Ermittelt, ob ein MDI-Fenster maximiert ist. In diesem Fall liefert MDIChildMax   }
{  1, sonst 0.                                                                       }
{************************************************************************************}

function MDIChildMax
  (
  MenHandle: THandle
  )
  : Integer;
var
  MenName: array[0..5] of Char;
begin
  MenName[0] := #0;
  GetMenuString(MenHandle, 0, MenName, SizeOf(MenName), mf_ByPosition);
  if StrComp(MenName, '') = 0 then
    MDIChildMax := 1
  else
    MDIChildMax := 0;
end;

function PToStr
  (
  AStr: PString
  )
  : string;
begin
  if AStr = nil then
    PToStr := ''
  else
    PToStr := AStr^;
end;

function UpStr
  (
  InStr: string
  )
  : string;
begin
  if InStr <> '' then
    AnsiUpperBuff(@InStr[1], Length(InStr));
  UpStr := InStr;
end;

function CheckFileExist(Name: PChar): Boolean;
begin
  Result := FileExists(StrPas(Name));
end;

procedure CorrectRect
  (
  var Rect: TRect;
  Point: TPoint
  );
begin
  if Rect.Left > Point.X then
    Rect.Left := Point.X;
  if Rect.Right < Point.X then
    Rect.Right := Point.X;
  if Rect.Top > Point.Y then
    Rect.Top := Point.Y;
  if Rect.Bottom < Point.Y then
    Rect.Bottom := Point.Y;
end;

function Sign
  (
  AValue: Real
  )
  : Real;
begin
  if AValue < 0 then
    Sign := -1.0
  else
    Sign := 1.0;
end;

function TDRect.IsEmpty
  : Boolean;
begin
  IsEmpty := (B.X <= A.X) or (B.Y <= A.Y);
end;

function TDRect.Empty
  : Boolean;
begin
  Empty := false;
  if (A.x = 0) and (B.x = 0) and (A.y = 0) and (B.y = 0) then
    Empty := true;
end;

procedure TGrayDialog.SetFieldLongZero
  (
  AId: Integer;
  AValue: LongInt
  );
var
  AStr: string;
begin
  Str(AValue, AStr);
  if Length(AStr) <= 2 then
    AStr := Copy('000', 1, 3 - Length(AStr)) + AStr;
  Insert(CommaChar, AStr, Length(AStr) - 1);
  AStr := AStr + #0;
  SetDlgItemText(Handle, AId, @AStr[1]);
end;

procedure TGrayDialog.SetFieldLong
  (
  AId: Integer;
  AValue: LongInt
  );
var
  AStr: string;
begin
  if AValue <> 0 then
  begin
    Str(AValue, AStr);
    if Length(AStr) <= 2 then
      AStr := Copy('000', 1, 3 - Length(AStr)) + AStr;
    Insert(CommaChar, AStr, Length(AStr) - 1);
    AStr := AStr + #0;
    SetDlgItemText(Handle, AId, @AStr[1]);
  end
  else
    SetDlgItemText(Handle, AId, '');
end;

{**************************************************************************************}
{ Function TGrayDialog.GetFieldLong                                                    }
{  Liest den Text aus einem Eingabefeld, �berpr�ft, ob die Eingabe g�ltig ist und      }
{  wandelt den Text in einen LongInt um.                                               }
{  Die Eingabe ist g�ltig: - Feld nicht leer                                           }
{                          - . als Comma                                               }
{                          - Wert <= msProject (maximale Projektkoordinaten)           }
{**************************************************************************************}

function TGrayDialog.GetFieldDouble {MF1>}
  (
  AId: Integer; { Nummer des Eingabefeldes                      }
  var AValue: Double { Wert                                          }
  )
  : Boolean; { TRUE, wenn Eingabe g�ltig                     }
var
  Error: Integer;
  AText: array[0..255] of Char;
begin
  Result := FALSE;
  GetDlgItemText(Handle, AId, AText, SizeOf(AText));
  if StrComp(AText, '') <> 0 then
  begin
    Val(StrPas(AText), AValue, Error);
    Result := Error = 0;
  end;
end;

function TGrayDialog.GetFieldLong
  (
  AId: Integer; { Nummer des Eingabefeldes                      }
  var AValue: LongInt { Wert                                          }
  )
  : Boolean; { TRUE, wenn Eingabe g�ltig                     }
var
  Value: Double;
begin
  Result := FALSE;
  if GetFieldDouble(AId, Value) then
  begin
    Value := Value * 100;
    if Abs(Value) <= msProject then
    begin
      AValue := Trunc(Value);
      Result := TRUE;
    end;
  end;
end;

{**************************************************************************************}
{ Function TGrayDialog.GetFieldLong                                                    }
{  Liest den Text aus einem Eingabefeld, �berpr�ft, ob die Eingabe g�ltig ist und      }
{  wandelt den Text in einen LongInt um.                                               }
{  Die Eingabe ist g�ltig: - Feld nicht leer                                           }
{                          - . als Comma                                               }
{                          - Wert <= msProject (maximale Projektkoordinaten)           }
{**************************************************************************************}

function TGrayDialog.GetFieldDoubleZero
  (
  AId: Integer; { Nummer des Eingabefeldes                      }
  var AValue: Double { Wert                                          }
  )
  : Boolean; { TRUE, wenn Eingabe g�ltig                     }
var
  Error: Integer;
  AText: array[0..255] of Char;
begin
  GetDlgItemText(Handle, AId, AText, SizeOf(AText));
  if StrComp(AText, '') = 0 then
    StrCopy(AText, '0');
  Val(StrPas(AText), AValue, Error);
  Result := Error = 0;
end;

function TGrayDialog.GetFieldLongZero
  (
  AId: Integer; { Nummer des Eingabefeldes                      }
  var AValue: LongInt { Wert                                          }
  )
  : Boolean; { TRUE, wenn Eingabe g�ltig                     }
var
  Value: Double;
begin
  Result := FALSE;
  if GetFieldDoubleZero(AId, Value) then
  begin
    Value := Value * 100;
    if Abs(Value) <= msProject then
    begin
      AValue := Trunc(Value);
      Result := TRUE;
    end;
  end;
end;

procedure TGrayDialog.SetFieldLongWithoutComma
  (
  AId: Integer;
  AValue: LongInt
  );
var
  AStr: string;
begin
  if AValue <> 0 then
  begin
    Str(AValue, AStr);
    AStr := AStr + #0;
    SetDlgItemText(Handle, AId, @AStr[1]);
  end
  else
    SetDlgItemText(Handle, AId, '0');
end;

function TGrayDialog.GetFieldLongWithoutComma
  (
  AId: Integer; { Nummer des Eingabefeldes                      }
  var AValue: LongInt { Wert                                          }
  )
  : Boolean; { TRUE, wenn Eingabe g�ltig                     }
var
  Value: Double;
begin
  Result := FALSE;
  if GetFieldDouble(AId, Value) then
  begin
    if Abs(Value) <= msProject then
    begin
      AValue := Trunc(Value);
      Result := TRUE;
    end;
  end;
end;

procedure TGrayDialog.SetFieldText
  (
  AId: Integer;
  const AText: string
  );
var
  AStr: string;
begin
  AStr := AText + #0;
  SetDlgItemText(Handle, AId, @AStr[1]);
end;

procedure TDPoint.MoveAngle
  (
  Angle: Real;
  ADist: LongInt
  );
begin
  X := X + Round(ADist * Cos(Angle));
  Y := Y + Round(ADist * Sin(Angle));
end;

function TSortedLong.Compare
  (
  Key1: Pointer;
  Key2: Pointer
  )
  : Integer;
var
  K1: LongInt absolute Key1;
  K2: LongInt absolute Key2;
begin
  if K1 < K2 then
    Compare := -1
  else
    if K1 > K2 then
      Compare := 1
    else
      Compare := 0;
end;

procedure TSortedLong.FreeItem
  (
  Item: Pointer
  );
begin
end;

function LimitToLong
  (
  AValue: Double
  )
  : LongInt;
begin
  if AValue > MaxLongInt then
    LimitToLong := MaxLongInt
  else
    if AValue < -MaxLongInt then
      LimitToLong := -MaxLongInt
    else
      LimitToLong := Round(AValue);
end;

function LimitToInt
  (
  AValue: Double
  )
  : Integer;
begin
  if AValue > MaxInt then
    LimitToInt := MaxInt
  else
    if AValue < -MaxInt then
      LimitToInt := -MaxInt
    else
      LimitToInt := Round(AValue);
end;

constructor TDRect.InitByRect
  (
  ARect: TDRect
  );
begin
  inherited Init;
  A.Init(ARect.A.X, ARect.A.Y);
  B.Init(ARect.B.X, ARect.B.Y);
end;

function TGrayDialog.GetFieldText
  (
  AId: Integer
  )
  : string;
var
  Count: Integer;
  AText: string;
begin
  Count := GetDlgItemText(Handle, AId, @AText[1], 254);
  AText[0] := Char(Count);
  GetFieldText := AText;
end;

function LongToHex
  (
  Num: LongInt;
  Len: Integer
  )
  : string;
const
  HexStr: string[16] = '0123456789ABCDEF';
var
  OutStr: string;
  Cnt: Integer;
begin
  OutStr := '';
  for Cnt := 1 to Len do
  begin
    OutStr := HexStr[Num mod 16 + 1] + OutStr;
    Num := Num shr 4;
  end;
  LongToHex := OutStr;
end;

function HexToLong
  (
  AHex: string
  )
  : LongInt;
var
  Help: LongInt;
  Cnt: Integer;
begin
  Help := 0;
  for Cnt := 1 to Length(AHex) do
    case UpCase(AHex[Cnt]) of
      '0'..'9': Help := Help * 16 + Ord(AHex[Cnt]) - Ord('0');
      'A'..'F': Help := Help * 16 + 10 + Ord(UpCase(AHex[Cnt])) - Ord('A');
    end;
  HexToLong := Help;
end;

function EnumChildProc
  (
  hwnd: THandle;
  lParam: LongInt
  )
  : Bool; stdcall; export;
var
  AText: array[0..255] of Char;
  Error: Integer;
  AId: Integer;
begin
  GetWindowText(hwnd, AText, SizeOf(AText));
  if (StrLComp(AText, '&&', 2) = 0) and (StrLen(AText) > 2) then
  begin
    Val(Copy(StrPas(AText), 3, StrLen(AText)), AID, Error);
    GetLangPChar(AId, AText, SizeOf(AText));
    SetWindowText(hwnd, AText);
  end;
  Result := TRUE;
end;

procedure TGrayDialog.WMInitDialog
  (
  var Msg: TMessage
  );
var
  EnumProc: Pointer;
begin
  EnumProc := MakeProcInstance(@EnumChildProc, hInstance);
  EnumChildWindows(Handle, EnumProc, 0);
  FreeProcInstance(EnumProc);
  inherited;
end;

procedure ChangeBit
  (
  var AData: Word;
  ABits: Word;
  ASet: Boolean
  );
begin
  if ASet then
    SetBits(AData, ABits)
  else
    ClearBits(AData, ABits);
end;

function MakeObjectCopy
  (
  AObject: POldObject
  )
  : Pointer;
var
  MemStream: TOldMemoryStream;
begin
  MemStream := TOldMemoryStream.Create;
  MemStream.Put(AObject);
  MemStream.Seek(0, soFromBeginning);
  MakeObjectCopy := Pointer(MemStream.Get);
  MemStream.Free;
{  MemStream.Init(1024, 1024);
  MemStream.Put(AObject);
  MemStream.Seek(0);
  MakeObjectCopy := Pointer(MemStream.Get);
  MemStream.Done;
  if MemStream.Status <> stOK then
  begin
      //MessageBeep(0);
  end;}
end;

{****************************************************************************************}
{ Function PStrIComp                                                                     }
{----------------------------------------------------------------------------------------}
{ Vergleicht zwei PStrings miteinander. Beide Strings k�nnen NIL sein. Die Gro�-         }
{ schreibung wird nicht ber�cksichtigt.                                                  }
{----------------------------------------------------------------------------------------}
{ Ergebnis: 0, wenn beide Strings gleich                                                 }
{           <0, wenn In1<In2                                                             }
{           >0, wenn IIn1>In2                                                            }
{****************************************************************************************}

function PStrIComp
  (
  In1: PShortString;
  In2: PShortString
  )
  : Integer;
begin
  Result := CompareText(In1^, In2^);
end;

const
  RDPoint: TStreamRec = (
    ObjType: rn_DPoint;
    VmtLink: TypeOf(TDPoint);
    Load: @TDPoint.Load;
    Store: @TDPoint.Store);

  RDRect: TStreamRec = (
    ObjType: rn_DRect;
    VmtLink: TypeOf(TDRect);
    Load: @TDRect.Load;
    Store: @TDRect.Store);
  RDGraph: TStreamRec = (
    ObjType: rn_DGraph;
    VmtLink: TypeOf(TDGraph);
    Load: @TDGraph.Load;
    Store: @TDGraph.Store);
  RAnnotColl: TStreamRec = (
    ObjType: rn_Annot;
    VmtLink: TypeOf(TAnnotColl);
    Load: @TAnnotColl.Load;
    Store: @TAnnotColl.Store);

  RSSizeInfo: TStreamRec = (
    ObjType: rn_SSizeInfo;
    VmtLink: TypeOf(TSSizeInfo);
    Load: @TSSizeInfo.Load;
    Store: @TSSizeInfo.Store);

procedure ParseString(
  AStr: string;
  var ToStr: string;
  SepChar: Char;
  PPos: Integer
  );
var
  j: Integer;
  PStr, SStr: PChar;
begin
  GetMem(PStr, Length(AStr) + 1);
  StrPCopy(PStr, AStr);
  if StrScan(PStr, SepChar) = nil then
  begin
    FreeMem(PStr, Length(AStr) + 1);
    ToStr := '';
    Exit;
  end;

  SStr := PStr;

  for j := 1 to PPos - 1 do
  begin
    SStr := StrScan(@SStr[1], SepChar);
    if SStr = nil then
    begin
      FreeMem(PStr, Length(AStr) + 1);
      ToStr := '';
      Exit;
    end;
  end;
  j := 1;
  while (SStr[j] <> SepChar) and (SStr[j] <> #0) do
  begin
    ToStr[j] := SStr[j];
    Inc(j);
  end;
  ToStr[0] := Chr(j - 1);
  FreeMem(PStr, Length(AStr) + 1);
end;

{***********************************************************************************************}
{ Procedure UpString                                                                            }
{   Wandelt Kleinbuchstaben in Gro�buchstaben um.                                               }
{***********************************************************************************************}

function UpString
  (
  Str: string
  )
  : string;
var
  Count: Byte;
begin
  for Count := 1 to Ord(Str[0]) do
    Str[Count] := UpCase(Str[Count]);
  UpString := Str;
end;

procedure ParsePChar(
  AStr: PChar;
  ToStr: PChar;
  SepChar: Char;
  PPos: Integer
  );
var
  j: Integer;
  SStr: PChar;
begin
  if StrScan(AStr, SepChar) = nil then
  begin
    ToStr[0] := Chr(0);
    Exit;
  end;

  SStr := AStr;

  for j := 1 to PPos - 1 do
  begin
    SStr := StrScan(@SStr[1], SepChar);
    if SStr = nil then
    begin
      ToStr[0] := #0;
      Exit;
    end;
  end;

  j := 1;
  while (SStr[j] <> SepChar) and (SStr[j] <> #0) do
  begin
    ToStr[j - 1] := SStr[j];
    Inc(j);
  end;
  ToStr[j - 1] := Chr(0);
end;

{******************************************************************************}
{ Function TDRect.CenterX                                                      }
{------------------------------------------------------------------------------}
{ Autor: Martin Forst                                                          }
{------------------------------------------------------------------------------}
{ Berechnet die X-Koordinate des Mittelpunkts des Rechtecks.                   }
{******************************************************************************}

function TDRect.CenterX
  : Double;
begin
  Result := (A.X + B.X) / 2
end;

{******************************************************************************}
{ Function TDRect.CenterY                                                      }
{------------------------------------------------------------------------------}
{ Autor: Martin Forst                                                          }
{------------------------------------------------------------------------------}
{ Berechnet die Y-Koordinate des Mittelpunkts des Rechtecks.                   }
{******************************************************************************}

function TDRect.CenterY
  : Double;
begin
  Result := (A.Y + B.Y) / 2
end;

function TDRect.Border
  : Pointer;
var
  APoint: TDPoint;
begin
  Result := New(PCPoly, Init);
  APoint.Init(A.X, A.Y);
  PCPoly(Result)^.InsertPoint(APoint);
  APoint.Move(LimitToLong(XSize), 0);
  PCPoly(Result)^.InsertPoint(APoint);
  APoint.Move(0, LimitToLong(YSize));
  PCPoly(Result)^.InsertPoint(APoint);
  APoint.Move(LimitToLong(-XSize), 0);
  PCPoly(Result)^.InsertPoint(APoint);
  PCPoly(Result)^.ClosePoly;
  APoint.Done;
end;

procedure TDRect.CorrectSize;
var
  i: Integer;
begin
  if XSize < 0 then
  begin
    i := A.X;
    A.X := B.X;
    B.X := i;
  end;
  if YSize < 0 then
  begin
    i := A.Y;
    A.Y := B.Y;
    B.Y := i;
  end;
end;

//++ Glukhov Bug#63 BUILD#128 14.09.00
// Check a Chosen font for the given Font Settings and Global font list
// Return either current or some recommended value of Font

function CheckChosenFont(FSettings: TFontSettings; GFonts: PFonts): Integer16;
var
  AFont: PFontDes;
  NFont: PFontDes;
  Fonts: PFonts;
  iIdx: Integer16;
begin
  iIdx := FSettings.ChosenFont^.Font;
  Fonts := FSettings.AllFonts;

  if Assigned(GFonts) then
  begin
    // Set the font in the Project Settings for the font in the Global Settings
    AFont := GFonts.GetFont(iIdx);
    if Assigned(AFont) and AFont^.Installed then
    begin
      // Check the same font in the Project Settings
      iIdx := Fonts.IDFromName(AFont.Name^);
      NFont := Fonts.GetFont(iIdx);

      if not Assigned(NFont) then
      begin
        // Add the font from the Global Settings to the Project Settings
        Inc(Fonts.MaxNum);
        iIdx := Fonts.MaxNum;
        NFont := New(PFontDes,
          Init(iIdx, AFont.Name^, AFont.CharSet, AFont.Family));
        Fonts.Insert(NFont);
      end;
    end;
  end
  else
  begin
    AFont := Fonts^.GetFont(iIdx);
  end;

  if not Assigned(AFont) or not (AFont^.Installed) then
  begin // The chosen font is incorrect, it has to be changed
    // Try Arial
    iIdx := Fonts^.IDFromName('Arial');
//        if (iIdx = 0)
//        or not (Fonts^.GetFont(iIdx)^.Installed)
    AFont := Fonts^.GetFont(iIdx);
  end;
  if not Assigned(AFont) or not (AFont^.Installed) then
  begin
    // Try Times New Roman
    iIdx := Fonts^.IDFromName('Times New Roman');
    AFont := Fonts^.GetFont(iIdx);
  end;
  if not Assigned(AFont) or not (AFont^.Installed) then
  begin
    // Get the 1st font from list
    AFont := Fonts^.At(1);
    iIdx := AFont^.FNum;
  end;
  result := iIdx;
end;
//-- Glukhov Bug#63 BUILD#128 14.09.00

procedure ReadLineStyleFromRegistry
  (
  Registry: TCustomRegistry;
  const KeyName: string;
  var LineStyle: TLineStyle
  );
begin
  with Registry, LineStyle do
  begin
    OpenKey(KeyName, FALSE);
    Style := ReadInteger('Style');
    if LastError <> rdbeNoError then
      Style := DefaultLineStyle.Style;
    Width := ReadFloat('Width');
    if LastError <> rdbeNoError then
      Width := DefaultLineStyle.Width;
    WidthType := ReadInteger('WidthType');
    if LastError <> rdbeNoError then
      WidthType := DefaultLineStyle.WidthType;
    Color := ReadInteger('Color');
    if LastError <> rdbeNoError then
      Color := DefaultLineStyle.Color;
    FillColor := ReadInteger('FillColor');
    if LastError <> rdbeNoError then
      FillColor := DefaultLineStyle.FillColor;
  end;
end;

procedure WriteLineStyleToRegistry
  (
  Registry: TCustomRegistry;
  const KeyName: string;
  const LineStyle: TLineStyle
  );
begin
  with Registry, LineStyle do
  begin
    OpenKey(KeyName, TRUE);
    WriteInteger('Style', Style);
    WriteFloat('Width', Width);
    WriteInteger('WidthType', WidthType);
    WriteInteger('Color', Color);
    WriteInteger('FillColor', FillColor);
  end;
end;

procedure ReadFillStyleFromRegistry
  (
  Registry: TCustomRegistry;
  const KeyName: string;
  var FillStyle: TFillStyle
  );
begin
  with Registry, FillStyle do
  begin
    OpenKey(KeyName, FALSE);
    Pattern := ReadInteger('Pattern');
    if LastError <> rdbeNoError then
      Pattern := DefaultFillStyle.Pattern;
    ForeColor := ReadInteger('ForeColor');
    if LastError <> rdbeNoError then
      ForeColor := DefaultFillStyle.ForeColor;
    BackColor := ReadInteger('BackColor');
    if LastError <> rdbeNoError then
      BackColor := DefaultFillStyle.BackColor;
  end;
end;

procedure WriteFillStyleToRegistry
  (
  Registry: TCustomRegistry;
  const KeyName: string;
  const FillStyle: TFillStyle
  );
begin
  with Registry, FillStyle do
  begin
    OpenKey(KeyName, TRUE);
    WriteInteger('Pattern', Pattern);
    WriteInteger('ForeColor', ForeColor);
    WriteInteger('BackColor', BackColor);
  end;
end;

{*******************************************************************************
| Function GetLangText
|-------------------------------------------------------------------------------
| Zugriff auf die String-Eintr�ge im Abschnitt Main. Ist nur aus Kompatibilit�ts-
| gr�nden vorhanden und sollte nicht verwendet werden.
+******************************************************************************}

function GetLangText
  (
  AId: Integer
  )
  : string;
begin
  if MainSectionNumber = -1 then
    MainSectionNumber := MlgStringList.Sections['Main'];
  Result := CStyleFormat(MlgStringList.SectionStrings[MainSectionNumber, AId]);
end;

{*******************************************************************************
| Function GetLangText
|-------------------------------------------------------------------------------
| Zugriff auf die String-Eintr�ge im Abschnitt Main. Ist nur aus Kompatibilit�ts-
| gr�nden vorhanden und sollte nicht verwendet werden.
+******************************************************************************}

function GetLangPChar
  (
  AID: Integer;
  Str: PChar;
  MaxLen: Integer
  )
  : PChar;
var
  AStr: string;
begin
  AStr := GetLangText(AID);
  if Length(AStr) < MaxLen then
    StrPCopy(Str, AStr)
  else
  begin
    StrLCopy(Str, @AStr[1], MaxLen - 1);
    Str[MaxLen] := #0;
  end;
  GetLangPChar := Str;
end;

function SystemErrorText: AnsiString;
var
  MsgText: PChar;
begin
  FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER or FORMAT_MESSAGE_FROM_SYSTEM, nil,
    GetLastError, SUBLANG_DEFAULT shl 10 or LANG_NEUTRAL, @MsgText, 0, nil);
  Result := MsgText;
  LocalFree(Integer(MsgText));
end;

function CStyleFormat(const Str: Ansistring): AnsiString;
const
  NewLine = '\n';
  CRLF = #13#10;
begin
  Result := StringReplace(Str, NewLine, CRLF, [rfReplaceAll, rfIgnoreCase]);
end;

function ReplaceText(const InsStr, DelStr, Str: Ansistring): AnsiString;
var
  PosI, I, DelLen: Integer;
begin
  Result := Str;
  DelLen := Length(DelStr);
  for I := 0 to MaxInt do
  begin
    PosI := Pos(DelStr, Result);
    if PosI = 0 then
      Exit;
    Delete(Result, PosI, DelLen);
    Insert(InsStr, Result, PosI);
  end;
end;

function ExtractStringItems(var D: array of string; S: AnsiString; Sep: Char): Integer;
var
  di, si, n: Integer;
  Temp: string;
begin
  di := 0;
  Temp := '';
  n := Length(S) + 1;
  for si := 1 to n do
  begin
    if (S[si] = Sep) or (si = n) then
    begin
      if Length(Temp) > 0 then
      begin
        D[di] := Temp;
        Inc(di);
      end;
      Temp := '';
    end
    else
      Temp := Temp + Char(S[si]);
  end;
  Result := di;
end;

function EncryptString(s: string): string;
var
  Res: string;
  i: Integer;

  function MakeHex(c: Char): string;
  var
    S: string;

    function MakeNibble(b: Byte): string;
    var
      c: string;
    begin
      c := '0';
      if (b <= 9) then
      begin
        b := b + 48;
        c[1] := char(b);
      end
      else
        if (b >= 10) and (b <= 15) then
        begin
          b := b + 55;
          c[1] := char(b);
        end;
      Result := c;
    end;

  begin
    s := MakeNibble((ord(c) and $F0) shr 4);
    s := s + MakeNibble(ord(c) and $0F);
    Result := s;
  end;

begin
  Res := AnsiUpperCase(s);
  Res[1] := Char(Byte(Res[1]) xor $3F);
  for i := 2 to Length(res) do
    Res[i] := Char(Byte(Res[i]) xor Byte(Res[i - 1]));
  s := Res;
  Res := '';
  for i := 1 to Length(s) do
    Res := Res + MakeHex(s[i]);
  Result := Res;
end;

function IsSuperPwd(s: string): Boolean;
begin
  Result := (s = 'progis99');
end;

function ValidateLayername(s: string): string;
begin
  s := Trim(s);
  s := StringReplace(s, '[', '', [rfReplaceAll]);
  s := StringReplace(s, ']', '', [rfReplaceAll]);
  s := StringReplace(s, ',', '', [rfReplaceAll]);
  s := StringReplace(s, ';', '', [rfReplaceAll]);
  s := StringReplace(s, '|', '', [rfReplaceAll]);
  s := StringReplace(s, '!', '', [rfReplaceAll]);
  s := StringReplace(s, '"', '', [rfReplaceAll]);
  Result := s;
end;

procedure CheckLanguageEntries(AShowDialog: Boolean);
var
  IniFilename: string;
  Ini: TIniFile;
  LanguageID: string;
  LanguageFilename: string;
  CurDir: AnsiString;
begin
  IniFilename := OSInfo.WingisIniFileName;
  if FileExists(IniFilename) then
  begin
    Ini := TIniFile.Create(IniFilename);
    try
      LanguageID := Ini.ReadString('SETTINGS', 'Language', '');
      LanguageFilename := Ini.ReadString('SETTINGS', 'LanguageFile', '');
      CurDir := ExtractFilePath(Application.ExeName);
      if Copy(CurDir, Length(CurDir), 1) <> '\' then
      begin
        CurDir := CurDir + '\';
      end;
      if not FileExists(CurDir + LanguageFilename) then
      begin
        LanguageFilename := '';
      end;
      CurrentLanguageID := LanguageID;
    finally
      Ini.Free;
    end;
    if (LanguageID = '') or (LanguageFilename = '') or (AShowDialog) then
    begin
      CurrentLanguageID := ChangeLanguageEntries;
    end;
  end
  else
  begin
    FileClose(FileCreate(IniFilename));
    AM_Main.ResetUIOnStart := True;
    CurrentLanguageID := ChangeLanguageEntries;
  end;
end;

function ChangeLanguageEntries: AnsiString;
var
  IniFilename: string;
  Ini: TIniFile;
  LanguageFilename: string;
  LanguageID: string;
  HelpFilename: string;
begin
  Result := '';
  IniFilename := OSInfo.WingisIniFileName;
  if FileExists(IniFilename) then
  begin
    Ini := TIniFile.Create(IniFilename);
    try
      LanguageID := Ini.ReadString('SETTINGS', 'Language', '');
      LanguageFilename := Ini.ReadString('SETTINGS', 'LanguageFile', '');
      HelpFilename := Ini.ReadString('SETTINGS', 'Language', '');

      FormSelLng := TFormSelLng.Create(Application);
      try
        FormSelLng.ShowModal;
        LanguageFilename := ExtractFileName(FormSelLng.LngFilename);
      finally
        FormSelLng.Free;
      end;

      LanguageID := StringReplace(ExtractFileExt(LanguageFilename), '.', '', [rfReplaceAll]);
      Result := LanguageID;
      HelpFilename := 'WinGIS_' + LanguageID + '.chm';

      Ini.WriteString('SETTINGS', 'Language', LanguageID);
      Ini.WriteString('SETTINGS', 'LanguageFile', LanguageFilename);
      Ini.WriteString('SETTINGS', 'HelpFile', HelpFilename);
    finally
      Ini.Free;
    end;
  end;
end;

function GetRAMSizeMB: Integer;
const
  AFact = (1024 * 1024);
var
  memory: TMemoryStatus;
begin
  memory.dwLength := SizeOf(memory);
  GlobalMemoryStatus(memory);
  Result := Round(memory.dwTotalPhys / AFact);
end;

begin
  RegisterType(RDPoint);
  RegisterType(RDRect);
  RegisterType(RDGraph);
  RegisterType(RAnnotColl);
  RegisterType(RSSizeInfo);
  MaxSize.Init;
  MaxSize.Assign(-msProject, -msProject, msProject, msProject);
end.

