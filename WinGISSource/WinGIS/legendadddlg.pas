unit LegendAddDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, WCtrls, StyleDef, MultiLng;

type
  TLegendAddDialog = class(TForm)
    LayerListBox: TWListBox;
    OkBtn: TWButton;
    CancelBtn: TWButton;
    MlgSection1: TMlgSection;
    procedure FormActivate(Sender: TObject);
  private
    { Private-Deklarationen}
    FLayerList  : TList;
    Function GetSelected:TList;
  public
    { Public-Deklarationen}
    property LayerList:TList read FLayerList write FLayerList;
    property Selected:TList read GetSelected;
  end;


implementation

{$R *.DFM}

procedure TLegendAddDialog.FormActivate(Sender: TObject);
var i         : Integer;
    LayerProp : TLayerProperties;
begin
  if Assigned(FlayerList) then
    with FLayerList do begin
      for i:=0 to Count-1 do begin
        LayerProp:=TLayerProperties(FLayerList[i]);
        LayerListBox.Items.Add(LayerProp.Name);
        end;
      end;
end;

{******************************************************************************+
I Function GetSelected                                                         I
I returns a list of TlayerProperties object which were selected in the         I
I list box                                                                     I
+******************************************************************************}
Function TLegendAddDialog.GetSelected:TList;
var aList : TList;
    i         : Integer;
    LayerProp : TLayerProperties;
begin
  aList:=TList.Create;
  if Assigned(FLayerList) then begin
    With FLayerList do
      for i:=0 to Count-1 do
        if LayerListBox.Selected[i] then aList.Add(FLayerList[i]);
    end;
  Result:=aList;
end;

end.
