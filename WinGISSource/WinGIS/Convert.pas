unit Convert;

interface
uses AM_Stddl,Am_Coll,Am_Admin,Am_Def,Am_Layer,WinTypes,WinProcs,Messages,
     UlBase,Am_Poly,Am_Index,Am_Proj,Uheap,Controls,ResDlg,ProjStyle;

const id_Radius         = 100;
      id_SourceList     = 101;
      id_DestLayer      = 102;

      acOneWay          = 001;
      acStopWay         = 002;

type
      PRouteData          = ^TRouteData;
      TRouteData          = Record
        Radius           : LongInt;
        Layers           : PLongColl;
        RouteLayer       : LongInt;  {new Layer to calculate}
        ReferenceLayer   : LongInt;  {Old existing Layer}
        SelectLayer      : LongInt;
      end;

      PRouteDlg          = ^TRouteDlg;
      TRouteDlg          = class(TLayerDlg)
       Public
        LData             : PRouteData;
        Constructor Init(AParent:TWinControl;AData:PRouteData;ALayers:PLayers;AProjStyle:TProjectStyles);
        Function    CanClose:Boolean; override;
        Procedure   SetupWindow; override;
      end;


      TConvert           = class
        FItems           : TSLBase;
        RouteData        : TRouteData;
        Proj             : PProj;
        Parent           : TWinControl;
        Constructor Create(AProj:PProj;AParent:TWinControl);
        Procedure DoAll;
      end;

      TLongPoint         = record
        X                :Double;
        Y                :Double;
      end;

      TLine              = class(TSLBaseITem)
        Start            :TLongPoint;
        Dest             :TLongPoint;
        constructor Create(x1,y1,x2,y2:Double);
      end;
var AConvert:TConvert;
implementation
Constructor TConvert.Create;
  Begin
    Proj:=AProj;
    Parent:=AParent;
  end;

Constructor TLine.Create
   (
   x1   : Double;
   y1   : Double;
   x2   : Double;
   y2   : Double
   );
  begin
    inherited Create;
    PLongInt(Self)^:=LongInt(TLine);
    FType:=Line;
    Start.x:=x1;
    Start.y:=y1;
    Dest.x:=x2;
    Dest.y:=y2;                                              {kein VMT-Pointer}
  end;

Procedure TConVert.DoAll;
 var  SaveFramePointer : LongInt;
      ReferencePage    : Pointer;
      MLayer           : PLayer;
      ALayer           : PLayer;
      APoint           : PDPoint;
      AObject          : PPoly;
      Status_Dlg       : TAbortDlg;
      cnt              : longint;
      cnt2             : integer;
      cnt3             : integer;
      AIndex           : PIndex;
  begin
    RouteData.Radius:=1000;
    if ExecDialog(TRouteDlg.Init( Proj^.Parent,@RouteData,Proj^.Layers,Proj^.PInfo^.ProjStyles))=id_OK then begin
    for Cnt3:=0 to RouteData.Layers^.Count-1 do begin
        ALayer:=PProj(Proj)^.Layers^.IndexToLayer(LongInt(RouteData.Layers^.At(Cnt3)));
        for Cnt:=0 to ALayer^.Data^.GetCount-1 do begin
           AIndex:=ALayer^.Data^.At(Cnt);
           AObject:=Pointer(PLayer(PProj(Proj)^.PInfo^.Objects)^.IndexObject(PProj(Proj)^.PInfo,AIndex));
           if (AObject<>NIL)then begin
              if ((AObject^.GetObjType=ot_Poly)or (AObject^.GetObjType=ot_CPoly)) then begin
                   for Cnt2:=0 to AObject^.Data^.Count-1 do begin
                        APoint := AObject^.Data^.at(Cnt2);
{                        AItem:=TListItem.Create;
                        AItem.x:=APoint^.x;
                        AItem.y:=APoint^.y;
                        AList.Add(AItem);   }
                        end;
                   end;
              end; {AObject<>NIL}
           end;   {1 Layer durchgehen}
      end;  {alle Layer}
    end;
  end;

{*************************************************************************************************************************}
Constructor TRouteDlg.Init
   (
   AParent         : TWinControl;
   AData           : PRouteData;
   ALayers         : PLayers;
   AProjStyle      : TProjectStyles
   );
  begin
    inherited Init(AParent,'D_ROUTE',ALayers,AProjStyle);
    LData:=AData;
  end;

Procedure TRouteDlg.SetupWindow;
  begin
    inherited SetupWindow;
    FillLayerList(id_SourceList,TRUE,FALSE);
    FillLayerList(id_DestLayer,FALSE,TRUE);
    SetMultiLayerSelection(id_SourceList,LData^.Layers,TRUE);
    SetFieldLong(id_Radius,LData^.Radius);
  end;

Function TRouteDlg.CanClose
   : Boolean;
  var ARadius      : LongInt;
      ADestLayer   : LongInt;
  begin
    CanClose:=FALSE;
    if not GetFieldLong(id_Radius,ARadius) then MsgBox(Handle,210,mb_IconExclamation or mb_Ok,'')
    else if SendDlgItemMsg(id_SourceList,lb_GetSelCount,0,0)=0 then begin
      MsgBox(Handle,841,mb_IconExclamation or mb_OK,'');
      SetFocus(GetItemHandle(id_SourceList));
    end
    else if GetLayer(id_DestLayer,ADestLayer,959,10125,10128,ilTop) then with LData^ do begin
      if Layers<>NIL then Dispose(Layers,Done);        {ilSecond}
      Layers:=New(PLongColl,Init(5,5));
      GetMultiLayerSelection(id_SourceList,TRUE,Layers);
      Radius:=ARadius;
      RouteLayer:=ADestLayer;
      {ReferenceLayer:=}
      CanClose:=TRUE;
    end;
  end;


end.
