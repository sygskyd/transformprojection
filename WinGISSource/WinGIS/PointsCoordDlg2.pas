{ Brovak Vladimir, Progis Russia}
unit PointsCoordDlg2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, MultiLng,Am_Ini;

type
  TPointsCoordDialog2 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    RadioGroup1: TRadioGroup;
    MlgSection: TMlgSection;
    RadioGroup2: TRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
    procedure ChangeData;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
  private
    { Private declarations }
  public                                   
    { Public declarations }
  end;

var
  PointsCoordDialog2: TPointsCoordDialog2;

implementation

{$R *.DFM}

procedure TPointsCoordDialog2.FormCreate(Sender: TObject);
begin
Radiogroup1.Items.Add(Mlgsection[6]);
Radiogroup1.Items.Add(Mlgsection[7]);

Radiogroup2.Items.Add(Mlgsection[8]);
Radiogroup2.Items.Add(Mlgsection[9]);
with IniFile.LoadPointsCoordDataFromFile do
  begin;
  Radiogroup1.ItemIndex:=InputMode;
  Radiogroup2.ItemIndex:=DegreeMode;
  end;
Radiogroup1.OnClick(Self);  
end;

procedure TPointsCoordDialog2.RadioGroup1Click(Sender: TObject);
begin
if Radiogroup1.ItemIndex=0
then
 begin;
 Radiogroup2.Enabled:=false;
 Radiogroup2.Font.Color:=clInactiveCaption;
 end
else
 begin;
 Radiogroup2.Enabled:=true;
 Radiogroup2.Font.Color:=clWindowText;
 end;
end;

procedure TPointsCoordDialog2.ChangeData;
 begin;
   with IniFile.LoadPointsCoordDataFromFile do
  begin;
   InputMode:=RadioGroup1.ItemIndex;
   DegreeMode:=RadioGroup2.ItemIndex;
  end;
 end;

procedure TPointsCoordDialog2.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
ChangeData;
IniFile.WriteDataForLoadPoint1;
end;

end.
