Unit ProjStyle;

Interface

Uses Classes,Palette,XFills,XLines;

Type TProjectStyles     = Class(TPersistent)
      Private
       FPalette         : TPalette;
       FXFillStyles     : TXFillStyleList;
       FXLineStyles     : TXLineStyleList;
       Procedure   SetPalette(APalette:TPalette);
       Procedure   SetXFillStyles(AXFillStyles:TXFillStyleList);
       Procedure   SetXLineStyles(AXLineStyles:TXLineStyleList);
      Protected
       Procedure   AssignTo(Dest:TPersistent); override;
      Public
       Constructor Create;
       Destructor  Destroy; override;
       Property    Palette:TPalette read FPalette write SetPalette;
       Property    XFillStyles:TXFillStyleList read FXFillStyles write SetXFillStyles;
       Property    XLineStyles:TXLineStyleList read FXLineStyles write SetXLineStyles;
     end;

Implementation

{===============================================================================
| TProjectStyles
+==============================================================================}

Constructor TProjectStyles.Create;
begin
  inherited Create;
  FPalette:=TPalette.Create;
  FXFillStyles:=TXFillStyleList.Create;
  FXLineStyles:=TXLineStyleList.Create;
end;

Destructor TProjectStyles.Destroy;
begin
  FXLineStyles.Free;
  FXFillStyles.Free;
  FPalette.Free;
  inherited Destroy;
end;    

Procedure TProjectStyles.SetXLineStyles(AXLineStyles:TXLineStyleList);
begin
  FXLineStyles.Assign(AXLineStyles);
end;
   
Procedure TProjectStyles.SetXFillStyles(AXFillStyles:TXFillStyleList);
begin
  FXFillStyles.Assign(AXFillStyles);
end;

Procedure TProjectStyles.SetPalette(APalette:TPalette);
begin
  FPalette.Assign(APalette);
end;

Procedure TProjectStyles.AssignTo(Dest:TPersistent);
begin
  with Dest as TProjectStyles do begin
    FPalette.Assign(Self.FPalette);
    FXFillStyles.Assign(Self.FXFillStyles);
    FXLineStyles.Assign(Self.FXLineStyles);
  end;
end;

end.

