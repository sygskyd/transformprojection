{******************************************************************************+
  Unit ProjHndl
--------------------------------------------------------------------------------
  Autor: Martin Forst
--------------------------------------------------------------------------------
  Contains the base-class for all menu-handlers that need access to the
  project (TProjMenuHandler) and the base-class for all rollups that need
  access to the project (TProjRollupForm).
--------------------------------------------------------------------------------
  Release: 1
+******************************************************************************}
Unit ProjHndl;

Interface

Uses Am_Proj,Classes,Controls,MenuHndl,Rollup;

Type TProjMenuHandler   = Class(TMenuHandler)
      Private
       FCoordInput : Boolean;
       FProject    : PProj;
      Protected
       Function    InternalToWindowDist(Const FromDist:Double):Double; override;
       Procedure   InternalToWindowPoint(Const IntX,IntY:Double;var WinX,WinY:Double); override;
       Function    WindowToInternalDist(Const FromDist:Double):Double; override;
       Procedure   WindowToInternalPoint(Const WinX,WinY:Double;var IntX,IntY:Double); override;
      Public
       Property    CoordinateInput:Boolean read FCoordInput write FCoordInput;
       Function    OnMouseDown(Const X,Y:Double;Button:TMouseButton;Shift:TShiftState):Boolean; override;
       Property    Project:PProj read FProject write FProject;
     end;

     TProjRollupForm    = Class(TRollupForm)
      Private
       FProject    : PProj;
      Protected
       Procedure   SetProject(AProject:PProj); virtual;
      Public
       Property    Project:PProj read FProject write SetProject;
     end;

Implementation

Uses AM_Def,AM_Index,WinTypes,WinProcs;

{===============================================================================
  TProjMenuHandler
+==============================================================================}

Function TProjMenuHandler.OnMouseDown
   (
   Const X         : Double;
   Const Y         : Double;
   Button          : TMouseButton;
   Shift           : TShiftState
   )
   : Boolean;
  var APoint       : TDPoint;
      SnapObject   : PIndex;
  begin
    APoint.Init(Round(X),Round(Y));
    if (Project^.ActualMen = mn_Select) or (Project^.SnapPoint(APoint,0,ot_Any,APoint,SnapObject,Shift,Button)) then
       Result:=inherited OnMouseDown(APoint.X,APoint.Y,Button,Shift)
    else Result:=FALSE;
  end;

Procedure TProjMenuHandler.InternalToWindowPoint
   (
   Const IntX      : Double;
   Const IntY      : Double;
   var WinX        : Double;
   var WinY        : Double
   );
  var IPoint       : TDPoint;
      WPoint       : TPoint;
  begin
    IPoint.Init(LimitToLong(IntX),LimitToLong(IntY));
    Project^.PInfo^.ProjPointToVirtPoint(IPoint,WPoint);
    WinX:=WPoint.X;
    WinY:=WPoint.Y;
  end;

Procedure TProjMenuHandler.WindowToInternalPoint
   (
   Const WinX      : Double;
   Const WinY      : Double;
   var IntX        : Double;
   var IntY        : Double
   );
  var IPoint       : TDPoint;
      WPoint       : TPoint;
  begin
    WPoint.X:=LimitToLong(WinX);
    WPoint.Y:=LimitToLong(WinY);
    Project^.PInfo^.VirtPointToProjPoint(WPoint,IPoint);
    IntX:=IPoint.X;
    IntY:=IPoint.Y;
  end;

Function TProjMenuHandler.InternalToWindowDist
   (
   Const FromDist  : Double
   )
   : Double;
  begin
    Result:=Project^.PInfo^.CalculateDisp(FromDist);
  end;

Function TProjMenuHandler.WindowToInternalDist(Const FromDist:Double):Double;
begin
  Result:=Project^.PInfo^.CalculateDraw(LimitToLong(FromDist));
end;

{===============================================================================
  TProjRollupForm
+==============================================================================}

Procedure TProjRollupForm.SetProject(AProject:PProj);
begin
  FProject:=AProject;
end;

end.