{
  Created by Sygsky approximately in the end of millenium
  to put here some useful, simple  but small functions
}
unit UserUtils;

interface

uses
  SysUtils, Windows
{$IFNDEF VMSI}
  , Objects
{$ENDIF}
  ;

{$IFNDEF VMSI}
function SmartString(const Str: AnsiString; const Args: array of const):AnsiString;
function SmartMsgBox(Owner: HWnd; SectionId, MsgId, CaptionId, MsgType: Integer;
  const Args: array of const
  ): Integer; overload;
function SmartMsgBox(Owner: HWnd; Section: AnsiString; MsgId, Caption,
  MsgType: Integer; const Args: array of const
  ): Integer; overload;
{$ENDIF}

procedure NotImplemented(AddInfo: AnsiString='');

function ExtractFileNameWithNoExt(FileName: AnsiString): AnsiString;
function GenerateUniqueName(const Path, Prefix, Extension: AnsiString): string;
function TrueDirPath(Path:AnsiString):AnsiString;
procedure SplitFileName(const FileName: AnsiString; Drive, Dir, Name, Ext :PAnsiString);

// Detect sign of parameter 0..MaxDouble will set to +1, -1 for others
{function Sign(Val: Integer): Integer; overload;
function Sign(var Val: Single): Integer; overload;
function Sign(var Val: Double): Integer; overload;}

procedure XChgDWORD(var V1: DWORD; var V2: DWORD);
procedure XChgWORD(var V1: Word; var V2: Word);
procedure XChgSingle(var V1: Single; var V2: Single);
procedure XChgDouble(var V1: Double; var V2: Double);
procedure XChgByte(var V1: Byte; var V2: Byte);

{$IFNDEF VMSI}

// Simplify the Collection of verteces
procedure PolySimplifyByPoints(Src: PCollection; Delta: Double; Res: PCollection);
procedure PolySimplify(Src: PCollection; Delta: Double; Res: PCollection);
{$ENDIF}

implementation

{$IFNDEF VMSI}
uses
  MultiLng, AM_Def;
//------------------------------------------------------------------------------
function SmartString(const Str: AnsiString; const Args: array of const):AnsiString;
begin
  try
    Result := Format(CStyleFormat(Str),Args);
  except
    Result := '#Err. in SmartString(''' + Str+''')';
  end;
end;
//------------------------------------------------------------------------------
function SmartMsgBox(Owner: HWnd; SectionId, MsgId, CaptionId, MsgType: Integer;
  const Args: array of const
  ): Integer; overload;
var
  Str, Str1, Str2: AnsiString;
begin
  Str := SmartString(MlgStringList.SectionStrings[SectionId, MsgId], Args);
  Str2 := SmartString(MlgStringList.SectionStrings[SectionId, CaptionId], []);
  Result := MessageBox(Owner, @Str[1], @Str2[1], MsgType);
end;
//--------------------------------------------------------------------
function SmartMsgBox(Owner: HWnd; Section: AnsiString; MsgId, Caption,
  MsgType: Integer; const Args: array of const
  ): Integer; overload;
var
  iSection: Integer;
begin
  if Section = EmptyStr then
    iSection := MlgStringList.Sections['Main']
  else
    iSection := MlgStringList.Sections[Section];
  Result := SmartMsgBox(Owner, iSection, MsgId, Caption, MsgType, Args);
end;

{$ENDIF}

procedure NotImplemented(AddInfo:AnsiString);
begin
  MessageBox(0, PChar('This option still not implemented'#13#10 + AddInfo),
  PChar('Not implemented'), MB_OK+MB_ICONWARNING);
end;

//------------------------------------------------------------------
function ExtractFileNameWithNoExt(FileName: AnsiString): AnsiString;
var
  I: Integer;
begin
  Result := EmptyStr;
  Result := ExtractFileName(FileName);
  I := Pos('.', Result);
  if I = 0 then Exit;
  Result := Copy(Result, 1, I - 1);
end;

{$IFNDEF VMSI}

//------------------------------------
// Simplify the Collection of verteces

// Simple variant of simplification

procedure PolySimplifyByPoints(
  Src: PCollection; Delta: Double; Res: PCollection);
var
  P1, P2: PDPoint;
  Del2: Double;
  i: Integer;
begin
  if Src.Count <= 0 then Exit;
  P1 := Src.At(0);
  Res.Insert(New(PDPoint, Init(P1.X, P1.Y)));
  if Src.Count <= 1 then Exit;
  Del2 := Delta * Delta;
  for i := 1 to Src.Count - 2 do
  begin
    P2 := Src.At(i);
    if ((P1.X - P2.X) * (P1.X - P2.X) + (P1.Y - P2.Y) * (P1.Y - P2.Y)) > Del2 then
    begin
      Res.Insert(New(PDPoint, Init(P2.X, P2.Y)));
      P1 := P2;
    end;
  end;
  P2 := Src.At(Src.Count - 1);
  Res.Insert(New(PDPoint, Init(P2.X, P2.Y)));
end;

// Checks the verteces from iFr+1 to iTo-1,
// returns True, if they are close to line [iFr;iTo]

function IsCloseToLine(
  Src: PCollection; iFr, iTo: Integer; Del2: Double): Boolean;
var
  P1, P2: PDPoint;
  PPnt: PDPoint;
  i: Integer;
  dbx, dby: Double;
  db1, db2: Double;
const
  Eps = 0.001;
begin
  Result := False;
  P1 := Src.At(iFr);
  P2 := Src.At(iTo);
  dbx := P2.X - P1.X; dby := P2.Y - P1.Y;
  db1 := dbx * dbx + dby * dby; // square of [iFr;iTo]
  if db1 < Eps then Exit;
  for i := iFr + 1 to iTo - 1 do
  begin
    PPnt := Src.At(i);
    db2 := (PPnt.X - P1.X) * dby - (PPnt.Y - P1.Y) * dbx; // vector product
    if (db2 * db2 / db1) > Del2 then Exit;
  end;
  Result := True;
end;

// Makes the step of recursion for verteces from iFr to iTo (inclusively)

procedure PolySimplificationStep(
  Src: PCollection; iFr, iTo: Integer; Del2: Double; Res: PCollection; iIn: Integer
  );
var
  PPnt: PDPoint;
  iBt, iFi: Integer;
begin
  if iFr >= (iTo - 1) then Exit;
  if IsCloseToLine(Src, iFr, iTo, Del2) then Exit;
  iFi := Res.Count - iIn; // save 2nd position to insert the verteces
  iBt := (iFr + iTo) div 2;
  PPnt := Src.At(iBt);
  Res.AtInsert(iIn, New(PDPoint, Init(PPnt.X, PPnt.Y)));
  PolySimplificationStep(Src, iFr, iBt, Del2, Res, iIn);
  PolySimplificationStep(Src, iBt, iTo, Del2, Res, Res.Count - iFi);
end;

// Smart variant of simplification

procedure PolySimplify(
  Src: PCollection; Delta: Double; Res: PCollection);
var
  TmpC: PCollection;
  P1, P2: PDPoint;
  Del2: Double;
//  i       : integer;
const
  Eps = 0.001;
begin
  if Src.Count <= 0 then Exit;
  TmpC := New(PCollection, Init(16, 16));
  if Delta < Eps then Delta := Eps;
  PolySimplifyByPoints(Src, Delta, TmpC);

//  for i:= 0 to TmpC.Count-1 do begin
//    P1:= TmpC.At( i );
//    Res.Insert( New( PDPoint, Init( P1.X, P1.Y ) ) );
//  end;

  P1 := TmpC.At(0);
  Res.Insert(New(PDPoint, Init(P1.X, P1.Y)));
  if TmpC.Count > 1 then
  begin
    P2 := TmpC.At(TmpC.Count - 1);
    Res.Insert(New(PDPoint, Init(P2.X, P2.Y)));
  end;
  if TmpC.Count > 2 then
  begin
    Del2 := Delta * Delta;
    PolySimplificationStep(TmpC, 0, TmpC.Count - 1, Del2, Res, 1);
  end;

  TmpC.FreeAll;
  Dispose(TmpC);
end;
//-----------

{$ENDIF}

//-------------- Use it to generate perfect unique file name -------- ++ Sygsky

function GenerateUniqueName(const Path, Prefix, Extension: AnsiString): string;

  function IntToBase32(Number: Integer): AnsiString;
  const
    Table: array[0..31] of Char = '0123456789ABCDEFGHIJKLMNOPQRSTUV';
  var
    I: Integer;
  begin
    Result := '';
    for I := 0 to 4 do
    begin
      Insert(Table[Number and 31], Result, 1);
      Number := Number shr 5;
    end;
  end;

var
  Rand, Len: Longint;
  Path1, Pre1, Ext1: AnsiString;
const
  DefExt = '.TMP';
  DefPre = 'JHC-'; // I alone know what it is
begin
  Path1 := Path;
  if Path1 = '' then
  begin
    SetLength(Path1, 256);
    Len := GetTempPath(255, PChar(Path1));
    if Len = 0 then
      Len := GetSystemDirectory(PChar(Path1), 255);
    SetLength(Path1, Len);
  end;
  if Path1[Length(Path1)] <> '\' then
    Path1 := Path1 + '\';

  Pre1 := Prefix;
  if Pre1 = '' then
    Pre1 := DefPre;

  Ext1 := ExtractFileExt(Extension);
  if Ext1 = '' then
  begin
    if Extension <> '' then
      Ext1 := '.' + Extension
    else
      Ext1 := DefExt;
  end;

  Rand := Random($2000000);
  Path1 := Path1 + Pre1;
  repeat
    Inc(Rand);
    if Rand > $1FFFFFF then Rand := 0;
    { Generate a random name }
    Result := Path1 + IntToBase32(Rand) + Ext1;
  until not FileExists(Result);
end;

function TrueDirPath(Path:AnsiString):AnsiString;
begin
  if Path = EmptyStr then
  begin
    Result := TrueDirPath(GetCurrentDir);
    Exit;
  end;
  If Path[Length(Path)] <> '\' then
    Result := Path + '\'
  else
    Result := Path;
end;

procedure SplitFileName(const FileName: AnsiString; Drive, Dir, Name, Ext :PAnsiString);
var
  Temp: AnsiString;
  I: Integer;
begin
  if Assigned(Drive) then
    Drive^ := ExtractFileDrive(FileName);
  if Assigned(Dir) then
  begin
    Temp := ExtractFileDir(FileName);
    Dir^ := TrueDirPath(StringReplace(Temp, ExtractFileDrive(Temp), EmptyStr, [rfIgnoreCase]));
  end;
  Temp := ExtractFileName(FileName);
  I := Pos('.', Temp);
  if I = 0 then   // No point detetced in file name. for example "name_no_ext"
  begin
    if Assigned(Name) then
      Name^ := Temp;
    if Assigned(Ext) then
      Ext^ :=  '.';
    Exit;
  end;
  if Assigned(Name) then
    Name^ := Copy(Temp, 1, I-1);
  if Assigned(Ext) then
    Ext^  := Copy(Temp, I, MaxInt);
end;

////////////////////////////////////////////////////////////////////////////
{ Internally callable only
  EAX - Value to estimate
}

procedure _iSign;
asm
  OR     EAX, EAX // Load value
  JS     @@1
  MOV    EAX, 1
  RET
@@1:
  OR     EAX, -1 // Load with SIGN extension
end;

(*{
  EAX = value to estimate
}

function Sign(Val: Integer): Integer;
asm
   JMP _iSign
end;
{
  EAX -> single float
}

function Sign(var Val: Single): Integer;
asm
   MOV   EAX,[EAX]                    // FLOAT TO INTEGER shortest step :)))
   JMP _iSign
end;

{
  EAX -> double float
}

function Sign(var Val: Double): Integer;
asm
   MOV   EAX, [EAX+4]
   JMP   _iSign
end;*)

procedure XChgDWORD(var V1: DWORD; var V2: DWORD);
asm
  MOV     ECX, [EAX]    // Load V1
  XCHG    ECX, [EDX]    // exchange value V2 from V1, V1 from V2
  MOV     [EAX], ECX    // Store new V1 to memory
end;

procedure XChgWORD(var V1: Word; var V2: Word);
asm
  MOV     ECX, [EAX]    // Load V1
  XCHG    ECX, [EDX]    // exchange value V2 from V1, V1 from V2
  MOV     [EAX], ECX    // Store new V1 to memory
end;

procedure XChgByte(var V1: Byte; var V2: Byte);
asm
  MOV     CL, [EAX];
  XCHG    CL, [EDX];
  MOV     [EAX], CL;
end;

procedure XChgSingle(var V1: Single; var V2: Single);
asm
  JMP     XChgDWORD
end;

procedure XChgDouble(var V1: Double; var V2: Double);
asm
  Call    XChgDWORD
  ADD     EAX, 4
  ADD     EDX, 4
  JMP     XChgDWORD
end;

end.

