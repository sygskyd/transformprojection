unit GisModuleDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  MultiLng, StdCtrls, CheckLst, ExtCtrls;

type
  TGisModuleDialog = class(TForm)
    MlgSection: TMlgSection;
    OkBtn: TButton;
    CancelBtn: TButton;
    Panel1: TPanel;
    Label1: TLabel;
    ModuleCheckLb: TCheckListBox;
    procedure FormShow(Sender: TObject);
    procedure OkBtnClick(Sender: TObject);
  private
    { Private declarations }
    ModuleMask:integer;
  public
    { Public declarations }
    procedure SetModuleMask(aMask:integer);  // set Mask which modules are activated
    function  GetModuleMask:integer;         // get Mask which modules are activated
  end;

var
  GisModuleDialog: TGisModuleDialog;

implementation

uses am_main, VerMgr, inifiles, AM_Def, WinOSInfo;

{$R *.DFM}


procedure TGisModuleDialog.FormShow(Sender: TObject);
var inifile:TIniFile;
    iniitems:TStrings;
    i:integer;
    moduledesc:string;
    mask:integer;
    dummymask:integer;
    maskidx:integer;
begin
   // Fill the Module List
   ModuleCheckLb.Items.Clear;
   // the list of the modules is stored in the WinGIS-ini Section [GisModules]
   inifile:=TIniFile.Create(OSInfo.WingisIniFileName);
   iniitems:=TStringList.Create;
   inifile.ReadSection('GisModules',iniitems);
   for i:=0 to iniitems.Count-1 do
   begin
      moduledesc:=inifile.ReadString('GisModules',iniitems[i],'');
      ModuleCheckLb.Items.Add(moduledesc);
      ModuleCheckLb.Checked[i]:=FALSE; // Module is not checked
   end;
   // now setup which modules are activated
   mask:=1;
   for i:=0 to iniitems.Count-1 do
      mask:=mask*2;
   mask:=mask div 2;
   dummymask:=ModuleMask;
   maskidx:=iniitems.Count-1;
   while (dummymask > 0) do
   begin
      if dummymask >= mask then // module is set
      begin
         ModuleCheckLb.Checked[maskidx]:=TRUE;
         dummymask:=dummymask-mask;
      end;
      mask:=mask div 2;
      maskidx:=maskidx-1;
   end;
   iniitems.Destroy;
   inifile.Destroy;
end;

// set Mask which modules are activated
procedure TGisModuleDialog.SetModuleMask(aMask:integer);
begin
   ModuleMask:=aMask;
end;

// get Mask which modules are activated
function TGisModuleDialog.GetModuleMask:integer;
begin
   result:=ModuleMask;
end;

procedure TGisModuleDialog.OkBtnClick(Sender: TObject);
var mask:integer;
    i:integer;
begin
   // calculate the Module-Mask
   mask:=1;
   ModuleMask:=0;
   for i:=0 to ModuleCheckLb.Items.Count-1 do
   begin
      if ModuleCheckLb.Checked[i] then
         ModuleMask:=ModuleMask+mask;
      mask:=mask*2;
   end;
end;

end.
