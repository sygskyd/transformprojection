{********************************************************************************}
{ Unit AM_ProjO                                                                  }
{--------------------------------------------------------------------------------}
{                                                                                }
{--------------------------------------------------------------------------------}
{ Modifikationen                                                                 }
{  11.12.1995 Heinz Ofner                                                        }
{********************************************************************************}
Unit AM_ProjO;

Interface

Uses Controls,SysUtils,WinTypes,WinProcs,Objects, Messages, AM_RText, AM_Proj, AM_BuGra, AM_Font,
     AM_Text,AM_Def,AM_Layer,AM_Admin,AM_Index,AM_View,AM_Point,AM_Paint,AM_Sight,AM_CPoly,
     AM_Pass,AM_Ini,AM_Dlg1,AM_Sym,AM_Obj
     {$IFNDEF WMLT}
     ,AM_DDE
     {$ENDIF}
     ,DDEDef,BmpImage,AM_Group,ResDlg,
//++ Glukhov Bug#66 BUILD#128 19.09.00
     SelInfo,
//-- Glukhov Bug#66 BUILD#128 19.09.00
     UserIntf;

Const DDELInfoSize   = 350;

Type PABInfo         = ^TABInfo;
     TABInfo         = object(TOldObject)
                         Image       : PImage;
                         StrDistArea : PChar;
                         PixelsX     : LongInt;
                         PixelsY     : LongInt;
                         Constructor Init(AImage:PImage;ADistArea:Extended;APixelsX:LongInt;APixelsY:LongInt);
                         Destructor  Done; virtual;
                       end;

     PSortedDInfo    = ^TSortedDInfo;
     TSortedDInfo    = object(TSortedCollection)
                         Function KeyOf(Item: Pointer): Pointer; virtual;
                         Function Compare(Key1, Key2: Pointer): Integer; virtual;
                       end;

     PSortedAInfo    = ^TSortedAInfo;
     TSortedAInfo    = object(TSortedCollection)
                         Function KeyOf(Item: Pointer): Pointer; virtual;
                         Function Compare(Key1, Key2: Pointer): Integer; virtual;
                       end;

//++ Glukhov Bug#236 Build#144 25.12.00
// Procedure ProcAutoShowBitmapOrFrame(AData:PProj;var Paint:TPaintStruct);
//-- Glukhov Bug#236 Build#144 25.12.00


function ProgisIDToDBValues(AProjName: PChar; ALayerName: PChar; AProgisID: Integer): PChar; stdcall; external 'WGDBTools.dll';
function GSTToProgisID(AProjName,ALayerName,KG,GN: PChar): Integer; stdcall; external 'WGDBTools.dll';

Procedure ProcDeleteLayer(AData:PProj; Deletelayer:PLayer);

{$IFNDEF WMLT}
  {$IFNDEF AXDLL} // <----------------- AXDLL
Procedure ProcDBAddLayer(AData:PProj);

Procedure ProcDBAddLayerWithoutDlg(AData:PProj);

Procedure ProcDBAddView(AName:String);

Procedure ProcDBChangeLayerWithoutDlg(AData:PProj);

Procedure ProcDBDelete(AData:PProj);

Procedure ProcDBDelLayer(AData:PProj);
  {$ENDIF} // <----------------- AXDLL

  {$IFNDEF AXDLL} // <----------------- AXDLL
Procedure ProcDBDelLayerWithoutDlg(AData:PProj);

Procedure ProcDBDeSelectAll(AData:PProj;ATextOut:Boolean);

Procedure ProcDBFreeProgableDDE(AData:PProj;ANumber:Word);

Procedure ProcDBMonitoring(AData:PProj);

Procedure ProcDKMAPPMonitoring(AData:PProj);

Procedure ProcDBMultiInsert(AData:PProj);

Procedure ProcDBObjectInfos(AData:PProj);

Procedure ProcDBSendCommand(AData:PProj;AText:String);

Procedure ProcDBSendObject(AData:PProj;Index:PIndex;AText:String);

Procedure ProcDBSendViews(AData:PProj);

Procedure ProcDBSetText(AData:PProj);

Procedure ProcDBSetTextEx(AData:PProj);

Procedure ProcGetDBText(AData:PProj;var DBText: String);

Procedure ProcInsertObjectFromDB(AData:PProj;DDELine:PChar);

Procedure ProcInsertObjectsFromDB(AData:PProj);

Procedure ProcMoveAbsFromDB(AData:PProj);

Procedure ProcMoveRelFromDB(AData:PProj);

Procedure ProcRequestLayer(AData:PProj);

Procedure ProcRequestLayerInfos(AData:PProj;ADDECommand:PChar);

Procedure ProcSelDBObjects(AData:PProj);

Procedure ProcSelDBObjects2(AData:PProj);

  {$ENDIF} // <----------------- AXDLL

//++ Glukhov Bug#387 Build#152 17.04.01
// Selects/DeSelects the item on the upper layer which it belongs to (but off)
Procedure ProcSelDeselTranspIndex( AData: PProj; AItem: PIndex );
// Selects the item on the upper layer which it belongs to (but off)
Procedure SelectIndexOnAllLayers( AData: PProj; AItem: PIndex );
// DeSelects the item on all the layers which it belongs to (but off)
Procedure DeSelectIndexOnAllLayers( AData: PProj; AItem: PIndex );
// Checks if the selected objects are on the top layer
Function  SelectionOnTopLayer( AData: PProj ): Boolean;
//-- Glukhov Bug#387 Build#152 17.04.01
  {$IFNDEF AXDLL} // <----------------- AXDLL
Function  ProcSelectSymbolForDGR(AData:PProj):Boolean;

Procedure ProcSendActualLayerToDB(AData:PProj);

Procedure ProcSendNewIDsToDB(AData:PProj;CommStr:String);

Procedure ProcSendNewPartPoliesToDB(AData:PProj);

Procedure ProcSendNewPartPolyToDB(AData:PProj;SLayerName:String;SLayerIndex:LongInt;DLayerName:String;
                                  DLayerIndex:LongInt;SPolyID:LongInt;APoly:PCPoly;SendEND:Boolean);

Procedure ProcSendSightInfosToDB(AData:PProj);

Procedure ProcSetSightFromDB(AData:PProj;AParent:TWinControl);
  {$ENDIF} // <----------------- AXDLL

Procedure ProcShowAllOnActiveLayer(AData:PProj);

Procedure ProcShowNextSel(AData:PProj);

Procedure ProcToCurve(AData:PProj);
//++ Glukhov Bug#192 Build#161 29.05.01
Procedure ProcConvToPLine ( AData: PProj );
Procedure ProcConvToPolygon ( AData: PProj );
//-- Glukhov Bug#192 Build#161 29.05.01

Procedure ProcSelDeselIndex(AData:PProj;AItem:PIndex);

Procedure ProcShowAllSel(AData:PProj);
  {$IFNDEF AXDLL} // <----------------- AXDLL
Procedure ProcSelectSymbolWithDlg(AData:PProj);
  {$ENDIF} // <----------------- AXDLL
{$ENDIF}

Procedure ProcSetSight(AData:PProj;ASight:PSight;UpdateView:Boolean=TRUE);

Procedure ProcSplitLine(AData:PProj);

Procedure ProcAddLegendEntry(AData:PProj; LegName:String; EntryName: String);

Procedure ProcDelLegendEntry(AData:PProj; LegName:String; EntryName: String);

Procedure ProcDBSetGTState(AData:PProj; ALayername:String; Active:Boolean);

Procedure ProcUpdatePolyByPoly(AData: PProj; DItem,SItem: PView);

Procedure ProcUpdateCPolyByPoly(AData: PProj; DItem,SItem: PView);

procedure ProcPolyUpdated(AData: PProj; AItem: PView);

function ProcPrepareUpdateCPolyByPoly(AData: PProj; DItem,SItem: PView): PCPoly;

Procedure ProcExplodeSelectedToPoints(AData: PProj; DestLayer: PLayer; ASym: PSGroup);

function ProcConvertSymToObj(AData: PProj; Idx: Integer; DestLayer: PLayer): Boolean;

function ProcPolyPointCut(AData: PProj; SLayer: PLayer; MLayer: PLayer; Dlayer: PLayer; ASym: PSGroup): Boolean;

procedure ProcDBPolyPointCut(AData: PProj; s1,s2,s3,s4: String);

procedure ProcRotateSymbols(AData: PProj);

procedure ProcRotateSymbol(AData: PProj; ASym: PSymbol);

procedure ProcPolyCircleCut(AData: PProj; PolyID: Integer; X,Y,R: Integer);

procedure ProcCopyLayerByName(AData: PProj; SLayerName,DLayerName: AnsiString);

procedure ProcDBGetPolyArea(AData: PProj; Id: Integer);

procedure ProcSetObjTypeFilter(AData: PProj; AObjType: Integer);

procedure ProcDBCreateTextFast(AData: PProj);

procedure ProcExplodeToLayer(AData: PProj; DLayerName: AnsiString);

procedure ProcDBGetCenter(AData: PProj);

procedure ProcSetIDBOnOff(AData: PProj; bActive: Boolean);

procedure ProcGetLayerPolyArea(AData: PProj; ALayerName: String);

procedure ProcGetGTDB(AData: PProj; ALayerName: String);

procedure ProcSetGTDB(AData: PProj; ALayerName: String; DBParams,IDField,FTField: String);

procedure ProcCreateCutPoints(AData: PProj);

procedure ProcExportLayerManager(AData: PProj);

procedure ProcSetLayerPrintable(AData: PProj; ALayername: String; bOn: Boolean);

procedure ProcSendAttObj(AData: PProj; AId: Integer);

function ProcGetAttText(AData: PProj; AId: Integer): AnsiString;

procedure ProcAttachTo(AData: PProj; AttId, AttToId: Integer);

procedure ProcReplacePolyPoints(AData: PProj; ALineIndex: PIndex);

procedure ProcOpenAttachedDocument(AData: PProj);

procedure ProcSelCrossPoly(AData: PProj; ALayername: String);

procedure ProcDeleteHiddenPolyPoints(AData: PProj; ALayername: String);

procedure ProcCheckView(AData: PProj; ASight: PSight);

procedure ProcDBSendUserSelection;

Implementation

Uses Classes,Forms,WinDOS,AM_Main,AM_Child,AM_Circl,AM_Splin,AM_Snap,AM_DBGra,Legend,Lists,
     AM_Coll,AM_ar,AM_Input,Var_Cont,AM_Selec,AM_Poly,AM_Meas,AM_OLE,NumTools,LayerHndl,AM_Exp,
     GrTools,UGlob,UHeap,USelLst,UIlst,CoordinateSystem,StyleDef,ExtLib,XLines,XFills,PolyPartition,
     ListHndl,PropCollect,Tooltips,IDB_CallBacksDef,MenuFn,WinOSInfo,MUModule,ToolsLib,AM_MMedi
     {$IFNDEF AXDLL} // <----------------- AXDLL
     ,SymRol
     {$ENDIF} // <----------------- AXDLL
     ,Dialogs
     {$IFNDEF AXDLL} // <----------------- AXDLL
     ,OmegaHndl
     ,SymSelDlg
     {$ENDIF} // <----------------- AXDLL
     ;

Constructor TABInfo.Init
   (
   AImage      : PImage;
   ADistArea   : Extended;
   APixelsX    : LongInt;
   APixelsY    : LongInt
   );
  var TempStr  : String;
      TempChar : Array[0..45] of Char;
  begin
    inherited Init;
    Image:=AImage;
    Str(ADistArea:30:10,TempStr);
    StrPCopy(TempChar,TempStr);
    StrDistArea:=StrNew(TempChar);
    PixelsX:=APixelsX;
    PixelsY:=APixelsY;
  end;

Destructor TABInfo.Done;
  begin
    StrDispose(StrDistArea);
    inherited Done;
  end;

Function TSortedDInfo.KeyOf
   (
   Item       : Pointer
   ) : Pointer;
  begin
    KeyOf := PABInfo(Item)^.StrDistArea;
  end;

Function TSortedDInfo.Compare
   (
   Key1, Key2 : Pointer
   ) : Integer;
  begin
    Compare:=StrComp(PChar(Key1),PChar(Key2));
  end;

Function TSortedAInfo.KeyOf
   (
   Item       : Pointer
   ) : Pointer;
  begin
    KeyOf := PABInfo(Item)^.StrDistArea;
  end;

Function TSortedAInfo.Compare
   (
   Key1, Key2 : Pointer
   ) : Integer;
  begin
    Compare:=StrComp(PChar(Key2),PChar(Key1));
  end;

{******************************************************************************}

procedure ProcDBSendUserSelection;
begin
     {$IFNDEF AXDLL}
     DDEHandler.SendString('[SELUSR][]');
     {$ENDIF}
end;

procedure ProcCheckView(AData: PProj; ASight: PSight);
var
PrRotRect: TRotRect;
VGrRect,PrGrRect: TGrRect;
ViewUpdated: Boolean;
begin
     ViewUpdated:=False;
     with AData^.Size do begin
          PrRotRect:=RotRect(A.X,A.Y,Abs(B.X-A.X),Abs(B.Y-A.Y),0);
     end;

     PrGrRect:=ISOFrame(PrRotRect);
     VGrRect:=ISOFrame(ASight^.ViewRect);

     if VGrRect.Left < PrGrRect.Left then begin
        VGrRect.Left:=PrGrRect.Left;
        ViewUpdated:=True;
     end;
     if VGrRect.Left > PrGrRect.Right then begin
        VGrRect.Left:=PrGrRect.Right;
        ViewUpdated:=True;
     end;

     if VGrRect.Right > PrGrRect.Right then begin
        VGrRect.Right:=PrGrRect.Right;
        ViewUpdated:=True;
     end;
     if VGrRect.Right < PrGrRect.Left then begin
        VGrRect.Right:=PrGrRect.Left;
        ViewUpdated:=True;
     end;

     if VGrRect.Top > PrGrRect.Top then begin
        VGrRect.Top:=PrGrRect.Top;
        ViewUpdated:=True;
     end;
     if VGrRect.Top < PrGrRect.Bottom then begin
        VGrRect.Top:=PrGrRect.Bottom;
        ViewUpdated:=True;
     end;

     if VGrRect.Bottom < PrGrRect.Bottom then begin
        VGrRect.Bottom:=PrGrRect.Bottom;
        ViewUpdated:=True;
     end;
     if VGrRect.Bottom > PrGrRect.Top then begin
        VGrRect.Bottom:=PrGrRect.Top;
        ViewUpdated:=True;
     end;

     if ViewUpdated then begin
        if (RectWidth(VGrRect) <= 0) or (RectHeight(VGrRect) <= 0) then begin
           ASight^.ViewRect:=RotRect(PrGrRect,0);
        end
        else begin
           ASight^.ViewRect:=RotRect(VGrRect,0);
        end;
        AData^.SetModified;
     end;
end;

procedure ProcDeleteHiddenPolyPoints(AData: PProj; ALayername: String);
var
ALayer: PLayer;
Cnt,AllCnt: Integer;

     procedure DoAll(AIndex: PIndex);
     var
     APoly: PPoly;
     APoint: PDPoint;
     i: Integer;
     SCircle: PEllipse;
     SObj: PIndex;
     begin
          Inc(Cnt);

          if AIndex^.GetObjType in [ot_Poly,ot_CPoly] then begin

             APoly:=PPoly(PLayer(AData^.PInfo^.Objects)^.IndexObject(AData^.PInfo,AIndex));
             if APoly <> nil then begin
                for i:=1 to APoly^.Data^.Count-1 do begin
                    if APoly^.IsAngle180(i) then begin
                       APoint:=APoly^.Data^.At(i);

                       SCircle:=New(PEllipse,Init(APoint^,1,1,0));
                       SObj:=nil;
                       try
                          if not AData^.Layers^.SearchForPoint(AData^.PInfo,SCircle,AIndex^.Index,[ot_Poly,ot_CPoly,ot_Pixel,ot_Symbol],APoint^,SObj) then begin
                             APoly^.DeletePointByIndex(i);
                             AData^.SetModified;
                          end;
                       finally
                          Dispose(SCircle,Done);
                       end;

                    end;
                end;
             end;

          end;

          StatusBar.Progress:=(Cnt*100)/AllCnt;
     end;

begin
     ALayer:=AData^.Layers^.NameToLayer(Trim(ALayername));
     if ALayer <> nil then begin
        StatusBar.CancelText:=False;
        StatusBar.ProgressPanel:=True;
        StatusBar.ProgressText:='';

        try
           Cnt:=0;
           AllCnt:=ALayer^.Data^.GetCount;
           ALayer^.Data^.ForEach(@DoAll);
        finally
           StatusBar.ProgressPanel:=False;
           StatusBar.CancelText:=True;
        end;
     end;
end;

procedure ProcSelCrossPoly(AData: PProj; ALayername: String);
var
ALayer: PLayer;

     procedure DoAll(AItem: PIndex);
     var
     AView: PView;
     begin
          if AItem^.GetObjType in [ot_Poly,ot_CPoly] then  begin
             AView:=PLayer(AData^.PInfo^.Objects)^.IndexObject(AData^.PInfo,AItem);
             if AView <> nil then begin
                 if PPoly(AView)^.IsCrossed(AData^.PInfo) then begin
                    ALayer^.Select(AData^.PInfo,AItem);
                 end;
             end;
          end;
     end;

begin
     ALayername:=Trim(ALayername);
     ALayer:=nil;

     if ALayername = '' then begin
        ALayer:=AData^.Layers^.TopLayer;
     end
     else begin
        ALayer:=AData^.Layers^.NameToLayer(ALayername);
     end;

     if ALayer <> nil then begin
        ALayer^.Data^.ForEach(@DoAll);
     end;

     AData^.PInfo^.RedrawScreen(False);
end;

procedure ProcOpenAttachedDocument(AData: PProj);
var
AIndex: PIndex;
n: Integer;
AText: AnsiString;
ASym: PSymbol;
begin
     if AData^.Layers^.SelLayer^.SelInfo.Count > 0 then begin
        AText:='';
        AIndex:=AData^.Layers^.SelLayer^.Data^.At(0);
        if AIndex <> nil then begin
           n:=AData^.MultiMedia^.EntriesForIDCount(AIndex^.Index);
           if n = 1 then begin
              try
                 if AIndex^.GetObjType = ot_Symbol then begin
                    ASym:=PSymbol(PLayer(AData^.PInfo^.Objects)^.IndexObject(AData^.PInfo,AIndex));
                    if ASym <> nil then begin
                       if ASym^.SymPtr <> nil then begin
                          AText:=ASym^.SymPtr^.Name^;
                       end;
                    end;
                 end;
              except
                 AText:='';
              end;
              AData^.MultiMedia^.OpenEntry(AData,AIndex^.Index,AText);
           end
           else if n > 1 then begin
              AData^.MultiMedia^.AddDelMedias(AData,AData^.Parent,AIndex^.Index);
           end;
        end;
     end
end;

procedure ProcAttachTo(AData: PProj; AttId, AttToId: Integer);
var
AIndex, BIndex: PIndex;
AView, BView: PView;
begin
     AIndex:=New(PIndex,Init(AttId));
     BIndex:=New(PIndex,Init(AttToId));
     try
        AView:=PLayer(AData^.PInfo^.Objects)^.IndexObject(AData^.PInfo,AIndex);
        BView:=PLayer(AData^.PInfo^.Objects)^.IndexObject(AData^.PInfo,BIndex);
        if (AView <> nil) and (BView <> nil) then begin
           AView^.AttachToObject(AData^.PInfo,BView);
        end;
     finally
        Dispose(AIndex,Done);
        Dispose(BIndex,Done);
     end;
end;

procedure ProcSendAttObj(AData: PProj; AId: Integer);
var
AIndex: PIndex;
AView: PView;
Coll: PCollection;

     procedure DoAll(AItem: PIndex);
     var
     BView: PView;
     AText: String;
     BId: String;
     begin
          AText:='';
          BView:=PLayer(AData^.PInfo^.Objects)^.IndexObject(AData^.PInfo,AItem);
          if BView <> nil then begin
             BId:=IntToStr(BView^.Index);
             if BView^.GetObjType = ot_Text then begin
                AText:=PText(BView)^.Text^;
             end;
             {$IFNDEF WMLT}
             {$IFNDEF AXDLL} // <----------------- AXDLL
             DDEHandler.SendString('[SATT][' + BId + '][' + AText + ']');
             {$ENDIF} // <----------------- AXDLL
             {$ENDIF}
          end;
     end;

begin
     if AData <> nil then begin
        AIndex:=New(PIndex,Init(AId));
        try
           AView:=PLayer(AData^.PInfo^.Objects)^.IndexObject(AData^.PInfo,AIndex);
           if (AView <> nil) and (Assigned(AView^.AttData)) then begin
              Coll:=AView^.GetAttachedObjs;
              if Coll <> nil then begin
                 Coll^.ForEach(@DoAll);
              end;
           end;
        finally
           Dispose(AIndex,Done);
        end;
     end;
     {$IFNDEF WMLT}
     {$IFNDEF AXDLL} // <----------------- AXDLL
     DDEHandler.SendString('[END][]');
     {$ENDIF} // <----------------- AXDLL
     {$ENDIF}
end;

function ProcGetAttText(AData: PProj; AId: Integer): AnsiString;
var
AIndex: PIndex;
AView: PView;
Coll: PCollection;

     procedure GetText(AItem: PIndex);
     var
     BView: PView;
     AText: String;
     BId: String;
     begin
          AText:='';
          BView:=PLayer(AData^.PInfo^.Objects)^.IndexObject(AData^.PInfo,AItem);
          if BView <> nil then begin
             BId:=IntToStr(BView^.Index);
             if BView^.GetObjType = ot_Text then begin
                AText:=PText(BView)^.Text^;
             end;
             Result:=AText;
          end;
     end;

begin
     Result:='';
     if AData <> nil then begin
        AIndex:=New(PIndex,Init(AId));
        try
           AView:=PLayer(AData^.PInfo^.Objects)^.IndexObject(AData^.PInfo,AIndex);
           if (AView <> nil) and (Assigned(AView^.AttData)) then begin
              Coll:=AView^.GetAttachedObjs;
              if (Coll <> nil) and (Coll^.Count > 0) then begin
                 GetText(Coll^.At(0));
              end;
           end;
        finally
           Dispose(AIndex,Done);
        end;
     end;
end;
procedure ProcSetLayerPrintable(AData: PProj; ALayername: String; bOn: Boolean);
var
ALayer: PLayer;
begin
     ALayer:=AData^.Layers^.NameToLayer(ALayerName);
     if ALayer <> nil then begin
        if ALayer^.Printable <> bOn then begin
           ALayer^.Printable:=bOn;
           AData^.SetModified;
        end;
     end;
end;

procedure ProcExportLayerManager(AData: PProj);
var
AFileName: String;
AList: TStringList;
i: Integer;

     procedure DoAll(ALayer: PLayer);
     begin
          if ALayer^.Text <> nil then begin
             AList.Add('');
             AList.Add('[' + ALayer^.Text^ + ']');
             if ALayer = AData^.Layers^.TopLayer then begin
                AList.Add('Active=1');
             end
             else begin
                AList.Add('Active=0');
             end;

             AList.Add('Visible=' + IntToStr(Ord(not ALayer^.GetState(sf_LayerOff))));
             AList.Add('Snap=' + IntToStr(Ord(ALayer^.UseForSnap)));
             AList.Add('Print=' + IntToStr(Ord(ALayer^.Printable)));
             AList.Add('Locked=' + IntToStr(Ord(ALayer^.GetState(sf_Fixed))));

             AList.Add('GeneralMin=' + FloatToStr(ALayer^.GeneralMin));
             AList.Add('GeneralMax=' + FloatToStr(ALayer^.GeneralMax));

             AList.Add('LineStyle=' + IntToStr(ALayer^.LineStyle.Style));
             AList.Add('LineColor=' + IntToHex(ALayer^.LineStyle.Color,8));
             AList.Add('LineFillColor=' + IntToHex(ALayer^.LineStyle.FillColor,8));
             AList.Add('LineWidthType=' + IntToStr(ALayer^.LineStyle.WidthType));
             AList.Add('LineWidth=' + FloatToStr(ALayer^.LineStyle.Width));

             AList.Add('FillStyle=' + IntToStr(ALayer^.FillStyle.Pattern));
             AList.Add('FillColor=' + IntToHex(ALayer^.FillStyle.ForeColor,8));
             AList.Add('FillBackColor=' + IntToHex(ALayer^.FillStyle.BackColor,8));

             if ALayer^.TooltipInfo <> nil then begin
                AList.Add('GeoTextMode=' + IntToStr(ALayer^.TooltipInfo.BDEOrLocal));
                AList.Add('GeoTextTable=' + ALayer^.TooltipInfo.BDETable);
                AList.Add('GeoTextIDColName=' + ALayer^.TooltipInfo.IDColName);
                AList.Add('GeoTextColName=' + ALayer^.TooltipInfo.FTColName);
             end;
          end;
     end;

begin
{$IFNDEF AXDLL} // <----------------- AXDLL
     if (AData <> nil) then begin
        AFileName:=OSInfo.TempDataDir + 'Layers.txt';
        AList:=TStringList.Create;
        try
           for i:=1 to AData^.Layers^.LData^.Count-1 do begin
               DoAll(AData^.Layers^.LData^.At(i));
               AList.SaveToFile(AFileName);
           end;
        finally
           AList.Free;
        end;
     end;
{$ENDIF} // <----------------- AXDLL
end;

procedure ProcCreateCutPoints(AData: PProj);
var
SLayer, DLayer: PLayer;
begin
     if AData^.Layers^.SelLayer^.SelInfo.Count > 0 then begin
        SLayer:=AData^.Layers^.SelLayer;
     end
     else begin
        SLayer:=AData^.Layers^.TopLayer;
     end;
     DLayer:=SelectLayerDialog(AData,[sloNewLayer]);
     if DLayer <> nil then begin
        MyObject:=TAreaObject.Create(nil,AData);
        try
           MyObject.DoAreaWithoutDlg3(SLayer,DLayer,0);
        finally
           MyObject.Free;
        end;
     end;
end;

procedure ProcGetGTDB(AData: PProj; ALayerName: String);
var
ALayer: PLayer;
s: String;
begin
     s:='';
     try
        ALayerName:=Trim(ALayerName);
        if ALayerName <> '' then begin
           ALayer:=AData^.Layers^.NameToLayer(ALayerName);
           if ALayer <> nil then begin
             if ALayer^.TooltipInfo <> nil then begin
                s:=ALayer^.TooltipInfo.BDETable;
             end;
           end;
        end
     finally
     {$IFNDEF WMLT}
     {$IFNDEF AXDLL} // <----------------- AXDLL
        DDEHandler.SendString('[GTDB][' + s + ']');
     {$ENDIF} // <----------------- AXDLL
     {$ENDIF}
     end;
end;

procedure ProcSetGTDB(AData: PProj; ALayerName: String; DBParams,IDField,FTField: String);
var
ALayer: PLayer;
begin
     try
        ALayerName:=Trim(ALayerName);
        if ALayerName = '' then begin
           ALayer:=AData^.Layers^.TopLayer;
        end
        else begin
           ALayer:=AData^.Layers^.NameToLayer(ALayerName);
        end;
        if (ALayer <> nil) and (ALayer^.TooltipInfo <> nil) then begin
           if DBParams <> '' then begin
              ALayer^.TooltipInfo.BDETable:=DBParams;
           end;
           if IDField <> '' then begin
              ALayer^.TooltipInfo.IDColName:=IDField;
           end;
           if FTField <> '' then begin
              ALayer^.TooltipInfo.FTColName:=FTField;
           end;
           AData^.SetModified;
        end;
     except
     end;
end;

procedure ProcGetLayerPolyArea(AData: PProj; ALayerName: String);
var
AArea: Double;
ALayer: PLayer;

     procedure DoAll(AItem: PIndex);
     var
     AView: PView;
     begin
          if AItem^.GetObjType = ot_CPoly then begin
             AView:=PLayer(AData^.PInfo^.Objects)^.IndexObject(AData^.PInfo,AItem);
             if AView <> nil then begin
                AArea:=AArea + PCPoly(AView)^.Flaeche;
             end;
          end;
     end;

begin
     AArea:=-1;
     try
        ALayerName:=Trim(ALayerName);
        if ALayerName = '' then begin
           ALayer:=AData^.Layers^.TopLayer;
        end
        else begin
           ALayer:=AData^.Layers^.NameToLayer(ALayerName);
        end;
        if ALayer <> nil then begin
           AArea:=0;
           ALayer^.Data^.ForEach(@DoAll);
        end;
     finally
     {$IFNDEF WMLT}
     {$IFNDEF AXDLL} // <----------------- AXDLL
        DDEHandler.SendString('[SLPA][' + FloatToStr(AArea) + ']');
     {$ENDIF} // <----------------- AXDLL
     {$ENDIF}
     end;
end;

procedure ProcSetIDBOnOff(AData: PProj; bActive: Boolean);
{$IFNDEF AXDLL} // <----------------- AXDLL
var
bLibraryWasLoaded: Boolean;
i: Integer;
AForm: TForm;
{$ENDIF} // <----------------- AXDLL
begin
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
     if AData <> nil then begin
      try
        AData^.SetCursor(crHourGlass);
        AData^.IDBSettings.bUseGlobalIDBSettings:=False;
        WinGISMainForm.WeAreUsingTheIDB[AData]:=bActive;
        WinGISMainForm.WeAreUsingTheIDBUserInterface[AData]:=bActive;
        bLibraryWasLoaded:=WinGISMainForm.SwitchIDBOn(AData);
        if bLibraryWasLoaded then begin
           if WinGISMainForm.WeAreUsingTheIDB[AData] then begin
              WinGISMainForm.IDB_Man.AddProject(AData);
           end;
        end;
        for i:=0 to WinGISMainForm.MDIChildCount-1 do begin
            AForm:=WinGISMainForm.MDIChildren[i];
            if (AForm is TMDIChild) then begin
               if (not WinGISMainForm.WeAreUsingTheIDB[TMDIChild(AForm).Data])
               and (WinGISMainForm.IDB_Man <> nil) then begin
                   WinGISMainForm.IDB_Man.DeleteProject(TMDIChild(AForm).Data);
               end;
            end;
        end;
        AData^.SetModified;
      finally
        AData^.SetCursor(crDefault);
      end;
     end;
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
end;


procedure ProcDBGetCenter(AData: PProj);
var
AView: PView;
AIndex: PIndex;
Center: TDPoint;
Id: Integer;
i: Integer;
ObjTypeOK: Boolean;
begin
     if AData <> nil then begin
        for i:=0 to AData^.IntList1.Count-1 do begin
            Center.Init(0,0);
            Id:=AData^.IntList1[i];
            AIndex:=New(PIndex,Init(Id));
            try
               AView:=PLayer(AData^.PInfo^.Objects)^.IndexObject(AData^.PInfo,AIndex);
               if AView <> nil then begin
                  ObjTypeOK:=True;
                  case AView^.GetObjType of
                     ot_Poly:   Center:=PPoly(AView)^.GetCenter;
                     ot_CPoly:  Center:=PCPoly(AView)^.GetCenter;
                     ot_Pixel:  Center.Init(PPixel(AView)^.Position.X,PPixel(AView)^.Position.Y);
                     ot_Text:   Center.Init(PText(AView)^.Pos.X,PText(AView)^.Pos.Y);
                     ot_Symbol: Center.Init(PSymbol(AView)^.Position.X,PSymbol(AView)^.Position.Y);
                     ot_Image:  Center:=PImage(AView)^.GetCenter(AData^.PInfo);
                     ot_Circle: Center.Init(PEllipse(AView)^.Position.X,PEllipse(AView)^.Position.Y);
                     ot_Arc:    Center.Init(PEllipseArc(AView)^.Position.X,PEllipseArc(AView)^.Position.Y);
                  else
                     ObjTypeOK:=False;
                  end;
               end;
            finally
               {$IFNDEF WMLT}
               {$IFNDEF AXDLL} // <----------------- AXDLL
               if ObjTypeOK then begin
                  DDEHandler.SendString('[CTR][' + IntToStr(Id) + '][' + IntToStr(Center.x) + '][' + IntToStr(Center.y) + ']');
               end;
               {$ENDIF} // <----------------- AXDLL
               {$ENDIF}
               Dispose(AIndex,Done);
            end;
        end;
        {$IFNDEF WMLT}
        {$IFNDEF AXDLL} // <----------------- AXDLL
        DDEHandler.SendString('[END][]');
        {$ENDIF} // <----------------- AXDLL
        {$ENDIF}
     end;
end;

procedure ProcExplodeToLayer(AData: PProj; DLayerName: AnsiString);
var
SLayer,DLayer: PLayer;
begin
     try
        if AData <> nil then begin
           DLayerName:=Trim(DLayerName);
           if AData^.Layers^.SelLayer^.SelInfo.Count > 0 then begin
              SLayer:=AData^.Layers^.SelLayer;
           end
           else begin
              SLayer:=AData^.Layers^.TopLayer;
           end;
           DLayer:=AData^.Layers^.NameToLayer(DLayerName);
           if DLayer = nil then begin
              DLayer:=NewLayerDialog1(AData,GetLangText(10154),DLayerName);
           end;
           if (DLayer <> nil) and (SLayer <> nil) then begin
              ExplodeWithoutDlg(AData,SLayer,DLayer);
           end;
        end;
     finally
     end;
end;

procedure ProcDBCreateTextFast(AData: PProj);
var
i: Integer;
begin
     try
        for i:=0 to AData^.CmdList.Count-1 do begin
            {$IFNDEF WMLT}
            {$IFNDEF AXDLL} // <----------------- AXDLL
            DDEHandler.InsertDDEData(PChar(AData^.CmdList[i]));
            ProcDBSetTextEx(AData);
            {$ENDIF} // <----------------- AXDLL
            {$ENDIF}
        end;
     finally
        AData^.CmdList.Clear;
        {$IFNDEF WMLT}
        {$IFNDEF AXDLL} // <----------------- AXDLL
        DDEHandler.SendString('[END][]');
        {$ENDIF} // <----------------- AXDLL
        {$ENDIF}
     end;
end;

procedure ProcSetObjTypeFilter(AData: PProj; AObjType: Integer);
begin
     if AData <> nil then begin
        if AObjType = 0 then begin
           AData^.ObjTypeFilter:=ot_Any;
        end
        else begin
           if AData^.ObjTypeFilter = ot_Any then begin
              AData^.ObjTypeFilter:=[];
           end;
           if AObjType < 0 then begin
              AObjType:=Abs(AObjType);
              Exclude(AData^.ObjTypeFilter,AObjType);
           end
           else begin
              Include(AData^.ObjTypeFilter,AObjType);
           end;
        end;
     end;
end;

procedure ProcDBGetPolyArea(AData: PProj; Id: Integer);
var
AIndex: PIndex;
AView: PView;
Area: Double;
begin
     Area:=0;
     if AData <> nil then begin
        AIndex:=New(PIndex,Init(Id));
        try
           AView:=PLayer(AData^.PInfo^.Objects)^.IndexObject(AData^.PInfo,AIndex);
           if (AView <> nil) and (AView^.GetObjType = ot_CPoly) then begin
              Area:=PCPoly(AView)^.Flaeche;
           end;
        finally
           Dispose(AIndex,Done);
        end;
     end;
     {$IFNDEF WMLT}
     {$IFNDEF AXDLL} // <----------------- AXDLL
     DDEHandler.SendString('[SPA][' + FloatToStr(Area) + ']');
     {$ENDIF} // <----------------- AXDLL
     {$ENDIF}
end;

procedure ProcCopyLayerByName(AData: PProj; SLayerName,DLayerName: AnsiString);
var
SLayer,DLayer: PLayer;
AnyInserted: Boolean;
AllCount,Cnt: Integer;
NIndex: PIndex;
s: String;
sn: Integer;

     procedure DoAll(AItem: PIndex);
     var
     AView: PView;
     NView: PView;
     begin
          Inc(Cnt);
          if AItem <> nil then begin
             AView:=PLayer(AData^.PInfo^.Objects)^.IndexObject(AData^.PInfo,AItem);
             if AView <> nil then begin
                NView:=MakeObjectCopy(AView);
                if NView <> nil then begin
                   if PLayer(AData^.PInfo^.Objects)^.InsertObject(AData^.PInfo,NView,False) then begin
                      NIndex:=New(PIndex,Init(NView^.Index));
                      if DLayer^.InsertObject(AData^.PInfo,NIndex,False) then begin
                         if AItem^.ObjectStyle <> nil then begin
                            AItem^.CopyObjectStyleTo(NIndex);
                            MU.LogUpdObjectStyle(AData^.PInfo,DLayer,NIndex);
                         end;
                         AnyInserted:=True;
                         AData^.SetModified;
                         s:=s + '[' + IntToStr(AItem^.Index) + '][' + IntToStr(NView^.Index) + ']';
                         Inc(sn);
                         if (Cnt mod 10) = 0 then begin
                            {$IFNDEF WMLT}
                            {$IFNDEF AXDLL} // <----------------- AXDLL
                            DDEHandler.SendString('[OBJCOP][' + IntToStr(sn) + ']' + s);
                            {$ENDIF} // <----------------- AXDLL
                            {$ENDIF}
                            s:='';
                            sn:=0;
                         end;
                      end
                      else begin
                         Dispose(NIndex,Done);
                      end;
                   end;
                end;
             end;
          end;
          StatusBar.Progress:=(Cnt*100)/AllCount;
     end;

begin
     AnyInserted:=False;
     s:='';
     sn:=0;
     try
        if AData <> nil then begin
           SLayerName:=Trim(SLayerName);
           DLayerName:=Trim(DLayerName);
           SLayer:=AData^.Layers^.NameToLayer(SLayerName);
           if SLayer <> nil then begin
              DLayer:=AData^.Layers^.NameToLayer(DLayerName);
              if DLayer = nil then begin
                 DLayer:=NewLayerDialog1(AData,GetLangText(10154),DLayerName);
              end;
              if DLayer <> nil then begin
                 StatusBar.CancelText:=False;
                 StatusBar.ProgressPanel:=True;
                 StatusBar.ProgressText:=GetLangText(4054);
                 try
                    Cnt:=0;
                    AllCount:=SLayer^.Data^.GetCount;
                    SLayer^.Data^.ForEach(@DoAll);
                 finally
                    StatusBar.ProgressPanel:=False;
                    StatusBar.CancelText:=True;
                    if sn > 0 then begin
                       {$IFNDEF WMLT}
                       {$IFNDEF AXDLL} // <----------------- AXDLL
                       DDEHandler.SendString('[OBJCOP][' + IntToStr(sn) + ']' + s);
                       {$ENDIF} // <----------------- AXDLL
                       {$ENDIF}
                    end;
                 end;
              end;
           end;
        end;
     finally
        if AnyInserted then begin
           {$IFNDEF WMLT}
           {$IFNDEF AXDLL} // <----------------- AXDLL
           DDEHandler.SendString('[END][]');
           {$ENDIF} // <----------------- AXDLL
           {$ENDIF}
           AData^.PInfo^.RedrawScreen(False);
        end;
     end;
end;

procedure ProcPolyCircleCut(AData: PProj; PolyID: Integer; X,Y,R: Integer);
var
AItem: PIndex;
APoly: PPoly;
i,n: Integer;
PC,P1,P2,PR1,PR2: TGrPoint;
Res: Integer;

     procedure SendPointToDB(APnt: TGrPoint);
     begin
          {$IFNDEF WMLT}
          {$IFNDEF AXDLL} // <----------------- AXDLL
          DDEHandler.SendString('[PCCP][' + IntToStr(Trunc(APnt.X)) + '][' + IntToStr(Trunc(APnt.Y)) + ']');
          {$ENDIF} // <----------------- AXDLL
          {$ENDIF}
     end;

begin
     if AData <> nil then begin
        AItem:=New(PIndex,Init(PolyID));
        try
           if AItem^.GetObjType in [ot_Poly,ot_CPoly] then begin
              APoly:=PPoly(PLayer(AData^.PInfo^.Objects)^.IndexObject(AData^.PInfo,AItem));
              if APoly <> nil then begin
                 PC:=GrPoint(X,Y);
                 n:=APoly^.Data^.Count-2;
                 for i:=0 to n do begin
                      P1:=GrPoint(TDPoint(APoly^.Data^.At(i)^).X,TDPoint(APoly^.Data^.At(i)^).Y);
                      P2:=GrPoint(TDPoint(APoly^.Data^.At(i+1)^).X,TDPoint(APoly^.Data^.At(i+1)^).Y);
                      PR1:=GrPoint(P1.X,P1.Y);
                      PR2:=GrPoint(P2.X,P2.Y);
                      Res:=CircleLineIntersection(PC,R,P1,P2,PR1,PR2);
                      if (P1.X <> PR1.X) or (P1.Y <> PR1.Y) then begin
                         SendPointToDB(PR1);
                      end;
                      if (P2.X <> PR2.X) or (P2.Y <> PR2.Y) then begin
                         SendPointToDB(PR2);
                      end;
                 end;
              end;
           end;
        finally
           {$IFNDEF WMLT}
           {$IFNDEF AXDLL} // <----------------- AXDLL
           DDEHandler.SendString('[END][]');
           {$ENDIF} // <----------------- AXDLL
           {$ENDIF}
           Dispose(AItem,Done);
        end;
     end;
end;

Procedure ProcSplitLine(AData:PProj);
var
APoly: PPoly;
begin
     APoly:=PPoly(AData.Layers^.SelLayer.Data.At(0));
     if APoly <> nil then begin
        AData^.OffsetItem := PPoly(AData^.Layers^.IndexObject(AData^.PInfo,PIndex(APoly)));
        if AData^.OffsetItem <> nil then begin
          AData^.InpOffset^.SetPoly(PPoly(AData^.OffsetItem));
          AData^.InpOffset^.StartIt(AData^.PInfo, AData^.CurPos, FALSE);
          AData^.SetActualMenu(mn_SplitLine);
//          AData.DeselectAll(False);
        end;
     end;
end;



Procedure ProcDBAddLayer
   (
   AData   : PProj
   );
  var CreateOnLayer  : PLayer;
  begin
    AData^.ChangeRedraw(CNoRedraw);
    CreateOnLayer:=NewLayerDialog(AData,GetLangText(10154),'');
    if CreateOnLayer<>NIL then AData^.SetModified;
    AData^.ChangeRedraw(COldRedraw);
  end;

Procedure ProcDeleteLayer
   (
   AData   : PProj;
   DeleteLayer: PLayer
   );
  var ObjToDel     : PLayer;
      LayerInfo    : PCollection;
      LayerList    : TList;
      NewOrder     : PCollection;
      NextIndex    : Integer;
      Swapp        : PCollection;
      IsFixed      : Boolean;
      IsHSDLayer   : Boolean;
      OldTopLayer  : PLayer;
      TopLayerDel  : Boolean;
      DeleteCnt    : Integer;
      OldTopIndex  : LongInt;
      NeedRedraw   : Boolean;
      FirstDelLayer: Boolean;
      AllCount     : LongInt;
      CurCount     : LongInt;
      Cnt          : Integer;
      NameLength   : Integer;
  Procedure DoAll
     (
     Item          : PLayer
     ); Far;
    begin
      if Item = DeleteLayer then DeleteCnt:=Item^.SortIndex-1;
      if Item^.SortIndex > 0 then LayerList.Add(Item^.GetLayerStyle);
    end;
  Procedure CopyLayer
     (
     Item          : TLayerProperties
     ); Far;
    var ALayer     : PLayer;
    begin
      with Item do begin
        if Index = 0 then begin
          ALayer:=New(PLayer,Init(AData^.Layers));
          Inc(AData^.Layers^.LastIndex);
          ALayer^.Index:=AData^.Layers^.LastIndex;
          NeedRedraw:=TRUE;
        end
        else begin
          ALayer:=AData^.Layers^.LData^.At(Index);
          AData^.Layers^.LData^.AtPut(Index,New(PLayer,Init(NIL)));
          if NextIndex <> Index then NeedRedraw:=TRUE;
          Inc(NextIndex);
        end;
        if ALayer^.SetLayerStyle(AData^.PInfo,Item) then NeedRedraw:=TRUE;
        NewOrder^.Insert(ALayer);
      end;
    end;
  Procedure CopyObjects                                             {kopiert alle zu l�schenden Objekte in eine L�schliste}
     (
     Item          : PLayer
     ); Far;
    var IndexStr   : String;
    Procedure DoAll
       (
       AItem       : PIndex
       ); Far;
      begin
        ObjToDel^.InsertObject(AData^.PInfo,AItem,FALSE);
        {$IFNDEF WMLT}
        {$IFNDEF AXDLL} // <----------------- AXDLL
        if DDEHandler.FLayerInfo <> 0 then DDEHandler.AppendList1(AItem^.Index,NameLength);
        {$ENDIF} // <----------------- AXDLL
        {$ENDIF}
        Inc(CurCount);
      end;
    begin
      if Item^.SortIndex > 0 then begin
        {$IFNDEF WMLT}
        {$IFNDEF AXDLL} // <----------------- AXDLL
        if DDEHandler.FLayerInfo = 1 then begin
          if not FirstDelLayer then DDEHandler.EndList1(FALSE,NameLength);
          NameLength:=Length(Item^.Text^);
          DDEHandler.StartList('[DEL]['+Item^.Text^+'][   ]');
        end
        else if DDEHandler.FLayerInfo = 2 then begin
          if not FirstDelLayer then DDEHandler.EndList1(FALSE,NameLength);
          Str(Item^.Index:10,IndexStr);
          NameLength:=10;
          DDEHandler.StartList('[DEL]['+IndexStr+'][   ]');
        end;
        {$ENDIF} // <----------------- AXDLL
        {$ENDIF}
        Item^.Data^.ForEach(@DoAll);
        if Item=OldTopLayer then TopLayerDel:=TRUE;
        NeedRedraw:=TRUE;
        Item^.DeSelectAll(AData^.PInfo);
        FirstDelLayer:=FALSE;
      end;
    end;
  Procedure DeleteObjects
     (
     Item          : PIndex
     ); Far;
    begin
      if AData^.Layers^.ExistsObject(Item)=0 then begin
        {$IFNDEF WMLT}
        {$IFNDEF AXDLL} // <----------------- AXDLL
        if DDEHandler.FLayerInfo = 0 then DDEHandler.AppendList(Item^.Index);
        {$ENDIF} // <----------------- AXDLL
        {$ENDIF}
        PObjects(AData^.PInfo^.Objects)^.DeleteIndex(AData^.PInfo,Item);
        Inc(CurCount,4);
      end;
    end;
  Procedure DeselOffLayers
     (
     Item          : PLayer
     ); Far;
    begin
      if Item^.GetState(sf_LayerOff) then Item^.DeSelectAll(AData^.PInfo);
    end;
  Function CheckTopLayer
     (
     Item          : PLayer
     )
     : Boolean; Far;
    begin
      if Item^.Index = OldTopIndex then begin
        TopLayerDel:=FALSE;
        Result:=TRUE;
      end
      else Result:=FALSE;
    end;
  Procedure CountObjects
     (
     Item          : PLayer
     ); Far;
    Procedure DoAll
       (
       AItem       : PIndex
       ); Far;
      begin
        Inc(AllCount);
      end;
    begin
      if Item^.SortIndex > 0 then Item^.Data^.ForEach(@DoAll);
    end;
  Procedure RemoveUseCounts
     (
     Item          : PLayer
     ); Far;
    begin
      with Item^ do UpdateUseCounts(AData^.PInfo,LineStyle,FillStyle,SymbolFill,FALSE);
    end;
  begin
    AData^.ChangeRedraw(CNoRedraw);
    IsFixed:=FALSE;
    IsHSDLayer:=FALSE;
    if DeleteLayer <> NIL then begin
      IsFixed:=DeleteLayer^.GetState(sf_Fixed);
      if AData^.TurboVector and AData^.Document.Loaded then
         IsHSDLayer:=AData^.Document.GetLayerByName(PToStr(DeleteLayer^.Text)) <> NIL
      else IsHSDLayer:=FALSE;
    end;
    if DeleteLayer <> NIL then begin
      if IsHSDLayer then MsgBox(AData^.Parent.Handle,2009,mb_OK or mb_IconExclamation,DeleteLayer^.Text^)
      else begin
        NeedRedraw:=FALSE;
        with AData^.Layers^ do begin
          LayerList:=TList.Create;
          LData^.ForEach(@DoAll);
          if LayerList.Count <= 1 then MsgBox(AData^.Parent.Handle,2005,mb_OK or mb_IconExclamation,'')
          else begin
            OldTopLayer:=TopLayer;
            TopLayerDel:=FALSE;
            TLayerProperties(LayerList[DeleteCnt]).Free;       {free memory of layer-style}
            LayerList.Delete(DeleteCnt);                       {remove layer from the layer-list}
            OldTopIndex:=TopLayer^.Index;
            NewOrder:=New(PCollection,Init(co_InitSize,co_Expand));
            NewOrder^.Insert(LData^.At(0));
            NextIndex:=1;
            for Cnt:=0 to LayerList.Count-1 do CopyLayer(LayerList[Cnt]);
            LData^.AtDelete(0);
            Swapp:=LData;
            LData:=NewOrder;
            TopLayerDel:=TRUE;
            LData^.FirstThat(@CheckTopLayer);
            LData^.ForEach(@DeselOffLayers);
            AData^.SetStatusText(GetLangText(4011),TRUE);
            AData^.SetCursor(crHourGlass);
            FirstDelLayer:=TRUE;
            ObjToDel:=New(PLayer,Init(NIL));
            if Swapp^.Count > 0 then begin
              AllCount:=0;
              Swapp^.ForEach(@CountObjects);
              if AllCount > 0 then begin
                try
                  AllCount:=AllCount*5;
                  CurCount:=0;
                  Swapp^.ForEach(@CopyObjects);
                  if ObjToDel^.Data^.GetCount>0 then begin
                    {$IFNDEF WMLT}
                    {$IFNDEF AXDLL} // <----------------- AXDLL
                    if DDEHandler.FLayerInfo = 0 then DDEHandler.StartList('[DEL][   ]');
                    {$ENDIF} // <----------------- AXDLL
                    {$ENDIF}
                    ObjToDel^.Data^.ForEach(@DeleteObjects);
                    {$IFNDEF WMLT}
                    {$IFNDEF AXDLL} // <----------------- AXDLL
                    if DDEHandler.FLayerInfo = 0 then DDEHandler.EndList(TRUE)
                    else DDEHandler.EndList1(TRUE,NameLength);
                    {$ENDIF} // <----------------- AXDLL
                    {$ENDIF}
                    ObjToDel^.Data^.DeleteAll;
                  end;
                finally
                end;
              end;
            end;
            Dispose(ObjToDel,Done);
            Swapp^.ForEach(@RemoveUseCounts);
            Dispose(Swapp,Done);
            AData^.ClearStatusText;
            AData^.SetCursor(crDefault);
            NumberLayers;
            if TopLayerDel then begin
              TopLayer:=LData^.At(1);
              SelLayer^.Data^.FreeAll;
              AData^.ShowNum:=0;
              AData^.SelList^.DeleteAll;
            end
            else AData^.DeSelectAll(TRUE);
            {$IFNDEF WMLT}
            {$IFNDEF AXDLL} // <----------------- AXDLL
            if DDEHandler.FSendLMSets then begin
              DDEHandler.SetDBSendStrings(DDECommLMI,NIL);
              ProcRequestLayerInfos(AData,'LMS');
              DDEHandler.SetDBSendStrings(DDECommAll,NIL);
            end;
            {$ENDIF} // <----------------- AXDLL
            {$ENDIF}
            if AData^.Document.Loaded then AData^.Document.ActualLayer:=TopLayer^.Text^;
            UserInterface.Update([uiMenus,uiStatusBar,uiLayers,uiWorkingLayer,uiViews]);
            AData^.SetModified;
          end;
          for Cnt:=0 to LayerList.Count-1 do TLayerProperties(LayerList[Cnt]).Free;
          LayerList.Free;
          UserINterface.Update([uiLegends]);
        end;
        if NeedRedraw then AData^.PInfo^.RedrawScreen(TRUE);
      end;
    end;
    AData^.ChangeRedraw(COldRedraw);
  end;

{$IFNDEF WMLT}
Procedure ProcDBDelLayer
   (
   AData   : PProj
   );
  var ObjToDel     : PLayer;
      LayerInfo    : PCollection;
      LayerList    : TList;
      NewOrder     : PCollection;
      NextIndex    : Integer;
      Swapp        : PCollection;
      IsFixed      : Boolean;
      IsHSDLayer   : Boolean;
      OldTopLayer  : PLayer;
      TopLayerDel  : Boolean;
      DeleteLayer  : PLayer;
      DeleteCnt    : Integer;
      OldTopIndex  : LongInt;
      NeedRedraw   : Boolean;
      FirstDelLayer: Boolean;
      AllCount     : LongInt;
      CurCount     : LongInt;
      Cnt          : Integer;
      NameLength   : Integer;
  Procedure DoAll
     (
     Item          : PLayer
     ); Far;
    begin
      if Item = DeleteLayer then DeleteCnt:=Item^.SortIndex-1;
      if Item^.SortIndex > 0 then LayerList.Add(Item^.GetLayerStyle);
    end;
  Procedure CopyLayer
     (
     Item          : TLayerProperties
     ); Far;
    var ALayer     : PLayer;
    begin
      with Item do begin
        if Index = 0 then begin
          ALayer:=New(PLayer,Init(AData^.Layers));
          Inc(AData^.Layers^.LastIndex);
          ALayer^.Index:=AData^.Layers^.LastIndex;
          NeedRedraw:=TRUE;
        end
        else begin
          ALayer:=AData^.Layers^.LData^.At(Index);
          AData^.Layers^.LData^.AtPut(Index,New(PLayer,Init(NIL)));
          if NextIndex <> Index then NeedRedraw:=TRUE;
          Inc(NextIndex);
        end;
        if ALayer^.SetLayerStyle(AData^.PInfo,Item) then NeedRedraw:=TRUE;
        NewOrder^.Insert(ALayer);
      end;
    end;
  Procedure CopyObjects                                             {kopiert alle zu l�schenden Objekte in eine L�schliste}
     (
     Item          : PLayer
     ); Far;
    var IndexStr   : String;
    Procedure DoAll
       (
       AItem       : PIndex
       ); Far;
      begin
        ObjToDel^.InsertObject(AData^.PInfo,AItem,FALSE);
        {$IFNDEF AXDLL} // <----------------- AXDLL
        if DDEHandler.FLayerInfo <> 0 then DDEHandler.AppendList1(AItem^.Index,NameLength);
        {$ENDIF} // <----------------- AXDLL
        Inc(CurCount);
        StatusBar.Progress:=100*CurCount/AllCount;
      end;
    begin
      if Item^.SortIndex > 0 then begin
        {$IFNDEF AXDLL} // <----------------- AXDLL
        if DDEHandler.FLayerInfo = 1 then begin
          if not FirstDelLayer then DDEHandler.EndList1(FALSE,NameLength);
          NameLength:=Length(Item^.Text^);
          DDEHandler.StartList('[DEL]['+Item^.Text^+'][   ]');
        end
        else if DDEHandler.FLayerInfo = 2 then begin
          if not FirstDelLayer then DDEHandler.EndList1(FALSE,NameLength);
          Str(Item^.Index:10,IndexStr);
          NameLength:=10;
          DDEHandler.StartList('[DEL]['+IndexStr+'][   ]');
        end;
        {$ENDIF} // <----------------- AXDLL
        Item^.Data^.ForEach(@DoAll);
        if Item=OldTopLayer then TopLayerDel:=TRUE;
        NeedRedraw:=TRUE;
        Item^.DeSelectAll(AData^.PInfo);
        FirstDelLayer:=FALSE;
      end;
    end;
  Procedure DeleteObjects
     (
     Item          : PIndex
     ); Far;
    begin
      if AData^.Layers^.ExistsObject(Item)=0 then begin
        {$IFNDEF AXDLL} // <----------------- AXDLL
        if DDEHandler.FLayerInfo = 0 then DDEHandler.AppendList(Item^.Index);
        {$ENDIF} // <----------------- AXDLL
        PObjects(AData^.PInfo^.Objects)^.DeleteIndex(AData^.PInfo,Item);
        Inc(CurCount,4);
        StatusBar.Progress:=100*CurCount/AllCount;
      end;
    end;
  Procedure DeselOffLayers
     (
     Item          : PLayer
     ); Far;
    begin
      if Item^.GetState(sf_LayerOff) then Item^.DeSelectAll(AData^.PInfo);
    end;
  Function CheckTopLayer
     (
     Item          : PLayer
     )
     : Boolean; Far;
    begin
      if Item^.Index = OldTopIndex then begin
        TopLayerDel:=FALSE;
        Result:=TRUE;
      end
      else Result:=FALSE;
    end;
  Procedure CountObjects
     (
     Item          : PLayer
     ); Far;
    Procedure DoAll
       (
       AItem       : PIndex
       ); Far;
      begin
        Inc(AllCount);
      end;
    begin
      if Item^.SortIndex > 0 then Item^.Data^.ForEach(@DoAll);
    end;
  Procedure RemoveUseCounts
     (
     Item          : PLayer
     ); Far;
    begin
      with Item^ do UpdateUseCounts(AData^.PInfo,LineStyle,FillStyle,SymbolFill,FALSE);
    end;
  begin
    AData^.ChangeRedraw(CNoRedraw);
    IsFixed:=FALSE;
    IsHSDLayer:=FALSE;
    DeleteLayer:=SelectLayerDialog(AData,[]);
    if DeleteLayer <> NIL then begin
      IsFixed:=DeleteLayer^.GetState(sf_Fixed);
      if AData^.TurboVector and AData^.Document.Loaded then
         IsHSDLayer:=AData^.Document.GetLayerByName(PToStr(DeleteLayer^.Text)) <> NIL
      else IsHSDLayer:=FALSE;
    end;
    if DeleteLayer <> NIL then begin
      if IsFixed then MsgBox(AData^.Parent.Handle,2008,mb_OK or mb_IconExclamation,DeleteLayer^.Text^)
      else if IsHSDLayer then MsgBox(AData^.Parent.Handle,2009,mb_OK or mb_IconExclamation,DeleteLayer^.Text^)
      else begin
        NeedRedraw:=FALSE;
        with AData^.Layers^ do begin
          LayerList:=TList.Create;
          LData^.ForEach(@DoAll);
          if LayerList.Count <= 1 then MsgBox(AData^.Parent.Handle,2005,mb_OK or mb_IconExclamation,'')
          else begin
            OldTopLayer:=TopLayer;
            TopLayerDel:=FALSE;
            TLayerProperties(LayerList[DeleteCnt]).Free;       {free memory of layer-style}
            LayerList.Delete(DeleteCnt);                       {remove layer from the layer-list}
            OldTopIndex:=TopLayer^.Index;
            NewOrder:=New(PCollection,Init(co_InitSize,co_Expand));
            NewOrder^.Insert(LData^.At(0));
            NextIndex:=1;
            for Cnt:=0 to LayerList.Count-1 do CopyLayer(LayerList[Cnt]);
            LData^.AtDelete(0);
            Swapp:=LData;
            LData:=NewOrder;
            TopLayerDel:=TRUE;
            LData^.FirstThat(@CheckTopLayer);
            LData^.ForEach(@DeselOffLayers);
            AData^.SetStatusText(GetLangText(4011),TRUE);
            AData^.SetCursor(crHourGlass);
            FirstDelLayer:=TRUE;
            ObjToDel:=New(PLayer,Init(NIL));
            if Swapp^.Count > 0 then begin
              AllCount:=0;
              Swapp^.ForEach(@CountObjects);
              if AllCount > 0 then begin
                StatusBar.CancelText:=FALSE;
                StatusBar.ProgressPanel:=TRUE;
                try
                  StatusBar.ProgressText:=GetLangText(11741);
                  AllCount:=AllCount*5;
                  CurCount:=0;
                  Swapp^.ForEach(@CopyObjects);
                  if ObjToDel^.Data^.GetCount>0 then begin
                    {$IFNDEF AXDLL} // <----------------- AXDLL
                    if DDEHandler.FLayerInfo = 0 then DDEHandler.StartList('[DEL][   ]');
                    {$ENDIF} // <----------------- AXDLL
                    ObjToDel^.Data^.ForEach(@DeleteObjects);
                    {$IFNDEF AXDLL} // <----------------- AXDLL
                    if DDEHandler.FLayerInfo = 0 then DDEHandler.EndList(TRUE)
                    else DDEHandler.EndList1(TRUE,NameLength);
                    {$ENDIF} // <----------------- AXDLL
                    ObjToDel^.Data^.DeleteAll;
                  end;
                finally
                  StatusBar.CancelText:=TRUE;
                  StatusBar.ProgressPanel:=FALSE;
                end;
              end;
            end;
            Dispose(ObjToDel,Done);
            Swapp^.ForEach(@RemoveUseCounts);
            Dispose(Swapp,Done);
            AData^.ClearStatusText;
            AData^.SetCursor(crDefault);
            NumberLayers;
            if TopLayerDel then begin
              TopLayer:=LData^.At(1);
              SelLayer^.Data^.FreeAll;
              AData^.ShowNum:=0;
              AData^.SelList^.DeleteAll;
            end
            else AData^.DeSelectAll(TRUE);
            {$IFNDEF AXDLL} // <----------------- AXDLL
            if DDEHandler.FSendLMSets then begin
              DDEHandler.SetDBSendStrings(DDECommLMI,NIL);
              ProcRequestLayerInfos(AData,'LMS');
              DDEHandler.SetDBSendStrings(DDECommAll,NIL);
            end;
            {$ENDIF} // <----------------- AXDLL
            if AData^.Document.Loaded then AData^.Document.ActualLayer:=TopLayer^.Text^;
            UserInterface.Update([uiMenus,uiStatusBar,uiLayers,uiWorkingLayer,uiViews]);
            AData^.SetModified;
          end;
          for Cnt:=0 to LayerList.Count-1 do TLayerProperties(LayerList[Cnt]).Free;
          LayerList.Free;
          UserINterface.Update([uiLegends]);
        end;
        if NeedRedraw then AData^.PInfo^.RedrawScreen(TRUE);
      end;
    end;
    AData^.ChangeRedraw(COldRedraw);
  end;

{$IFNDEF AXDLL} // <----------------- AXDLL
Procedure ProcDBDelLayerWithoutDlg
   (
   AData           : PProj
   );
  var LayerName    : String;
      ObjToDel     : PLayer;
      LayerInfo    : PCollection;
      LayerList    : TList;
      NewOrder     : PCollection;
      NextIndex    : Integer;
      Swapp        : PCollection;
      LayerIndex   : LongInt;
      StrOK        : Boolean;
      StrFound     : Boolean;
      StrToLong    : Boolean;
      MoreStrs     : Boolean;
      IsFixed      : Boolean;
      IsHSDLayer   : Boolean;
      OldTopLayer  : PLayer;
      TopLayerDel  : Boolean;
      DeleteLayer  : PLayer;
      DeleteCnt    : Integer;
      OldTopIndex  : LongInt;
      NeedRedraw   : Boolean;
      FirstDelLayer: Boolean;
      AllCount     : LongInt;
      CurCount     : LongInt;
      Cnt          : Integer;
      NameLength   : Integer;
  Procedure DoAll
     (
     Item          : PLayer
     ); Far;
    begin
      if Item = DeleteLayer then DeleteCnt:=Item^.SortIndex-1;
      if Item^.SortIndex > 0 then LayerList.Add(Item^.GetLayerStyle);
    end;
  Procedure CopyLayer
     (
     Item          : TLayerProperties
     ); Far;
    var ALayer     : PLayer;
    begin
      with Item do begin
        if Index = 0 then begin
          ALayer:=New(PLayer,Init(AData^.Layers));
          Inc(AData^.Layers^.LastIndex);
          ALayer^.Index:=AData^.Layers^.LastIndex;
          NeedRedraw:=TRUE;
        end
        else begin
          ALayer:=AData^.Layers^.LData^.At(Index);
          AData^.Layers^.LData^.AtPut(Index,New(PLayer,Init(NIL)));
          if NextIndex <> Index then NeedRedraw:=TRUE;
          Inc(NextIndex);
        end;
        if ALayer^.SetLayerStyle(AData^.PInfo,Item) then NeedRedraw:=TRUE;
        NewOrder^.Insert(ALayer);
      end;
    end;
  Procedure CopyObjects                                             {kopiert alle zu l�schenden Objekte in eine L�schliste}
     (
     Item          : PLayer
     ); Far;
    var IndexStr   : String;
    Procedure DoAll
       (
       AItem       : PIndex
       ); Far;
      begin
        ObjToDel^.InsertObject(AData^.PInfo,AItem,FALSE);
        if DDEHandler.FLayerInfo <> 0 then DDEHandler.AppendList1(AItem^.Index,NameLength);
        Inc(CurCount);
        StatusBar.Progress:=100*CurCount/AllCount;
      end;
    begin
      if Item^.SortIndex > 0 then begin
        if DDEHandler.FLayerInfo = 1 then begin
          if not FirstDelLayer then DDEHandler.EndList1(FALSE,NameLength);
          NameLength:=Length(Item^.Text^);
          DDEHandler.StartList('[DEL]['+Item^.Text^+'][   ]');
        end
        else if DDEHandler.FLayerInfo = 2 then begin
          if not FirstDelLayer then DDEHandler.EndList1(FALSE,NameLength);
          Str(Item^.Index:10,IndexStr);
          NameLength:=10;
          DDEHandler.StartList('[DEL]['+IndexStr+'][   ]');
        end;
        Item^.Data^.ForEach(@DoAll);
        if Item=OldTopLayer then TopLayerDel:=TRUE;
        NeedRedraw:=TRUE;
        Item^.DeSelectAll(AData^.PInfo);
        FirstDelLayer:=FALSE;
      end;
    end;
  Procedure DeleteObjects
     (
     Item          : PIndex
     ); Far;
    begin
      if AData^.Layers^.ExistsObject(Item)=0 then begin
        if DDEHandler.FLayerInfo = 0 then DDEHandler.AppendList(Item^.Index);
        PObjects(AData^.PInfo^.Objects)^.DeleteIndex(AData^.PInfo,Item);
        Inc(CurCount,4);
        StatusBar.Progress:=100*CurCount/AllCount;
      end;
    end;
  Procedure DeselOffLayers
     (
     Item          : PLayer
     ); Far;
    begin
      if Item^.GetState(sf_LayerOff) then Item^.DeSelectAll(AData^.PInfo);
    end;
  Function CheckTopLayer
     (
     Item          : PLayer
     )
     : Boolean; Far;
    begin
      if Item^.Index = OldTopIndex then begin
        TopLayerDel:=FALSE;
        Result:=TRUE;
      end
      else Result:=FALSE;
    end;
  Procedure CountObjects
     (
     Item          : PLayer
     ); Far;
    Procedure DoAll
       (
       AItem       : PIndex
       ); Far;
      begin
        Inc(AllCount);
      end;
    begin
      if Item^.SortIndex > 0 then Item^.Data^.ForEach(@DoAll);
    end;
  Procedure RemoveUseCounts
     (
     Item          : PLayer
     ); Far;
    begin
      with Item^ do UpdateUseCounts(AData^.PInfo,LineStyle,FillStyle,SymbolFill,FALSE);
    end;
  begin
    IsFixed:=FALSE;
    IsHSDLayer:=FALSE;
    if (DDEHandler.FGotLInfo = 0) or (DDEHandler.FGotLInfo = 1) then begin
      StrOK:=ReadDDECommandString(PChar(DDEHandler.DDEData^.At(0)),1,0,WgBlockStart,
                                              WgBlockEnd,WgBlockEnd,LayerName,StrToLong,MoreStrs);
      if StrOK and (LayerName <> '') then begin
        DeleteLayer:=AData^.Layers^.NameToLayer(LayerName);
        if DeleteLayer <> NIL then begin
          IsFixed:=DeleteLayer^.GetState(sf_Fixed);
          if AData^.TurboVector and AData^.Document.Loaded then
             IsHSDLayer:=AData^.Document.GetLayerByName(LayerName) <> NIL
          else IsHSDLayer:=FALSE;
        end;
      end
      else DeleteLayer:=NIL;
    end
    else if DDEHandler.FGotLInfo = 2 then begin
      if ReadDDECommandLongInt(PChar(DDEHandler.DDEData^.At(0)),1,0,WgBlockStart,
                                           WgBlockEnd,WgBlockEnd,LayerIndex,StrFound,MoreStrs) then begin
        DeleteLayer:=AData^.Layers^.IndexToLayer(LayerIndex);
        if DeleteLayer <> NIL then begin
          IsFixed:=DeleteLayer^.GetState(sf_Fixed);
          if AData^.TurboVector and AData^.Document.Loaded then
             IsHSDLayer:=AData^.Document.GetLayerByName(PToStr(DeleteLayer^.Text)) <> NIL
          else IsHSDLayer:=FALSE;
        end;
      end
      else DeleteLayer:=NIL;
    end;
    if DeleteLayer <> NIL then begin
      if IsFixed then MsgBox(AData^.Parent.Handle,2008,mb_OK or mb_IconExclamation,DeleteLayer^.Text^)
      else if IsHSDLayer then MsgBox(AData^.Parent.Handle,2009,mb_OK or mb_IconExclamation,DeleteLayer^.Text^)
      else begin
        NeedRedraw:=FALSE;
        with AData^.Layers^ do begin
          LayerList:=TList.Create;
          LData^.ForEach(@DoAll);
          if LayerList.Count <= 1 then MsgBox(AData^.Parent.Handle,2005,mb_OK or mb_IconExclamation,'')
          else begin
            OldTopLayer:=TopLayer;
            TopLayerDel:=FALSE;
            TLayerProperties(LayerList[DeleteCnt]).Free;       {free memory of layer-style}
            LayerList.Delete(DeleteCnt);                       {remove layer from the layer-list}
            OldTopIndex:=TopLayer^.Index;
            NewOrder:=New(PCollection,Init(co_InitSize,co_Expand));
            NewOrder^.Insert(LData^.At(0));
            NextIndex:=1;
            for Cnt:=0 to LayerList.Count-1 do CopyLayer(LayerList[Cnt]);
            LData^.AtDelete(0);
            Swapp:=LData;
            LData:=NewOrder;
            TopLayerDel:=TRUE;
            LData^.FirstThat(@CheckTopLayer);
            LData^.ForEach(@DeselOffLayers);
            AData^.SetStatusText(GetLangText(4011),TRUE);
            AData^.SetCursor(crHourGlass);
            FirstDelLayer:=TRUE;
            ObjToDel:=New(PLayer,Init(NIL));
            if Swapp^.Count > 0 then begin
              AllCount:=0;
              Swapp^.ForEach(@CountObjects);
              if AllCount > 0 then begin
                StatusBar.CancelText:=FALSE;
                StatusBar.ProgressPanel:=TRUE;
                try
                  StatusBar.ProgressText:=GetLangText(11741);
                  AllCount:=AllCount*5;
                  CurCount:=0;
                  Swapp^.ForEach(@CopyObjects);
                  if ObjToDel^.Data^.GetCount>0 then begin
                    if DDEHandler.FLayerInfo = 0 then DDEHandler.StartList('[DEL][   ]');
                    ObjToDel^.Data^.ForEach(@DeleteObjects);
                    if DDEHandler.FLayerInfo = 0 then DDEHandler.EndList(TRUE)
                    else DDEHandler.EndList1(TRUE,NameLength);
                    ObjToDel^.Data^.DeleteAll;
                  end;
                finally
                  StatusBar.CancelText:=TRUE;
                  StatusBar.ProgressPanel:=FALSE;
                end;
              end;
            end;
            Dispose(ObjToDel,Done);
            Swapp^.ForEach(@RemoveUseCounts);
            Dispose(Swapp,Done);
            AData^.ClearStatusText;
            AData^.SetCursor(crDefault);
            NumberLayers;
            if TopLayerDel then begin
              TopLayer:=LData^.At(1);
              SelLayer^.Data^.FreeAll;
              AData^.ShowNum:=0;
              AData^.SelList^.DeleteAll;
            end
            else AData^.DeSelectAll(TRUE);
            if DDEHandler.FSendLMSets then begin
              DDEHandler.SetDBSendStrings(DDECommLMI,NIL);
              ProcRequestLayerInfos(AData,'LMS');
              DDEHandler.SetDBSendStrings(DDECommAll,NIL);
            end;
            if AData^.Document.Loaded then AData^.Document.ActualLayer:=TopLayer^.Text^;
            UserInterface.Update([uiMenus,uiStatusBar,uiLayers,uiWorkingLayer,uiViews]);
          end;
          for Cnt:=0 to LayerList.Count-1 do TLayerProperties(LayerList[Cnt]).Free;
          LayerList.Free;
          UserINterface.Update([uiLegends]);
          AData^.SetModified;
        end;
        if NeedRedraw then AData^.PInfo^.RedrawScreen(TRUE);
      end;
    end;
    if DDEHandler.DDEData^.GetCount > 0 then PChar(DDEHandler.DDEData^.At(0))[0]:=DDEStrUsed;
    DDEHandler.DeleteDDEData;
  end;
{$ENDIF} // <----------------- AXDLL

{************************************************************************************}
{ Procedure ProcShowNextSel                                                          }
{  Zeigt nacheinander die selektierten Objekte an und schreibt Informationen �ber    }
{  Objektart und Layer in die Statuszeile.                                           }
{************************************************************************************}
Procedure ProcShowNextSel
   (
   AData           : PProj
   );
  var VItem        : PView;
      IndItem      : PIndex;
      PosX1        : LongInt;
      PosY1        : LongInt;
      PosX2        : LongInt;
      PosY2        : LongInt;
      RealPosX1    : Real;
      RealPosY1    : Real;
      RealPosX2    : Real;
      RealPosY2    : Real;
      Rect         : TDRect;
      XDist        : Real;
      YDist        : Real;
      NewDist      : Real;
      RealZoom     : Real;
      ObjType      : Word;
      ObjectLayer  : PLayer;
      StatusText   : String;
      AllObjCount  : LongInt;
      SameObjCount : LongInt;
      SameObjIndex : LongInt;
      ZoomFact     : Double;
      FrameSize    : Integer;
//++ Glukhov Bug#66 BUILD#128 19.09.00
// Data to form status text
      SelInfo      : TSelectInfo;
      DetailedText : String;
//-- Glukhov Bug#66 BUILD#128 19.09.00
  Function SearchObjectLayer                     {sucht den Layer, auf dem das xte Objekt mit gleichem Index liegt}
     : PLayer;
    var ObjIndex   : LongInt;
    Function SearchLayer
       (
       Item        : PLayer
       )
       : Boolean; Far;
      Function SearchObject
         (
         Item1       : PIndex
         )
         : Boolean; Far;
        begin
          Result:=Item1^.Index = IndItem^.Index;
        end;
      begin
        Result:=FALSE;
        if (Item^.Text <> NIL) and not Item^.GetState(sf_LayerOff) then begin
          if Item^.Data^.FirstThat(@SearchObject) <> NIL then begin
            if SameObjIndex = ObjIndex then Result:=TRUE
            else Inc(ObjIndex);
          end;
        end;
      end;
    begin
      ObjIndex:=0;
      Result:=AData^.Layers^.LData^.FirstThat(@SearchLayer);
    end;
  Procedure GetSameIndexes;                                     {pr�ft, wie oft ein Objekt in der Selektionsliste vorkommt}
    Procedure DoAllObjects                                                        {und das wievielte dieser Objekte es ist}
       (
       Item        : PIndex
       ); Far;
      begin
        if Item^.Index = IndItem^.Index then begin
          if AData^.ShowNum = AllObjCount then SameObjIndex:=SameObjCount;
          Inc(SameObjCount);
        end;
        Inc(AllObjCount);
      end;
    begin
      AllObjCount:=0;
      SameObjCount:=0;
      AData^.SelList^.ForEach(@DoAllObjects);
    end;
  begin
      if AData^.SelList^.GetCount > 0 then begin
       if (AData^.ShowNum > AData^.SelList^.GetCount-1) or (AData^.ShowNum < 0) then AData^.ShowNum:=0;
       IndItem:=AData^.SelList^.At(AData^.ShowNum);
       VItem:=AData^.Layers^.SelLayer^.IndexObject(AData^.PInfo,IndItem);
       Rect.Init;
       PosX1:=VItem^.ClipRect.A.X;
       PosY1:=VItem^.ClipRect.A.Y;
       PosX2:=VItem^.ClipRect.B.X;
       PosY2:=VItem^.ClipRect.B.Y;
       if (VItem^.ClipRect.A.X <> VItem^.ClipRect.B.X) and (VItem^.ClipRect.A.Y <> VItem^.ClipRect.B.Y) then begin
        if AData^.PInfo^.VariousSettings.ZoomSizeType=0 then begin
          ZoomFact:=AData^.PInfo^.VariousSettings.ZoomSize;
          FrameSize:=0;
        end
        else begin
          ZoomFact:=0;
          FrameSize:=Round(AData^.PInfo^.VariousSettings.ZoomSize*20);
        end;
        RealPosX1:=PosX1*1.0-(PosX2*1.0-PosX1*1.0)/1.00*ZoomFact;
        RealPosY1:=PosY1*1.0-(PosY2*1.0-PosY1*1.0)/1.17*ZoomFact;
        RealPosX2:=PosX2*1.0+(PosX2*1.0-PosX1*1.0)/1.00*ZoomFact;
        RealPosY2:=PosY2*1.0+(PosY2*1.0-PosY1*1.0)/1.17*ZoomFact;
        XDist:=RealPosX2-RealPosX1;
        YDist:=RealPosY2-RealPosY1;
        if XDist < YDist*1.33 then begin
          NewDist:=YDist*1.33;
          RealPosX1:=RealPosX1-(NewDist-XDist)/2;
          RealPosX2:=RealPosX2+(NewDist-XDist)/2;
        end
        else if XDist > YDist*1.33 then begin
          NewDist:=XDist/1.33;
          RealPosY1:=RealPosY1-(NewDist-YDist)/2;
          RealPosY2:=RealPosY2+(NewDist-YDist)/2;
        end;
        if RealPosX1 >= -MaxLongInt then Rect.A.X:=Round(RealPosX1)
        else Rect.A.X:=-MaxLongInt;
        if RealPosY1 >= -MaxLongInt then Rect.A.Y:=Round(RealPosY1)
        else Rect.A.Y:=-MaxLongInt;
        if RealPosX2 <= MaxLongInt then Rect.B.X:=Round(RealPosX2)
        else Rect.B.X:=MaxLongInt;
        if RealPosY2 <= MaxLongInt then Rect.B.Y:=Round(RealPosY2)
        else Rect.B.Y:=MaxLongInt;
        with Rect do RealZoom:=AData^.CalculateZoom(RotRect(A.X,A.Y,XSize,YSize,0),FrameSize,TRUE);
        if RealZoom > IniFile^.MaxZoom then begin
          XDist:=(Rect.B.X-Rect.A.X);
          YDist:=(Rect.B.Y-Rect.A.Y);
          NewDist:=Round(XDist*RealZoom/IniFile^.MaxZoom);
          Rect.A.X:=Rect.A.X-Round((NewDist-XDist)/2);
          Rect.B.X:=Rect.B.X+Round((NewDist-XDist)/2);
          NewDist:=Round(YDist*RealZoom/IniFile^.MaxZoom);
          Rect.A.Y:=Rect.A.Y-Round((NewDist-YDist)/2);
          Rect.B.Y:=Rect.B.Y+Round((NewDist-YDist)/2);
        end;
        with Rect do AData^.ZoomByRect(RotRect(A.X,A.Y,XSize,YSize,0),FrameSize,TRUE);
        Rect.Done;
       end;
        if AData^.Layers^.TranspLayer then begin
          GetSameIndexes;
          ObjectLayer:=SearchObjectLayer;
          if AData^.Layers^.TopLayer <> ObjectLayer then AData^.Layers^.SetTopLayer(ObjectLayer);
        end
        else ObjectLayer:=AData^.Layers^.TopLayer;
        ObjType:=IndItem^.GetObjType;
        case ObjType of
          ot_Pixel    : StatusText:=GetLangText(5142);
          ot_Poly     : StatusText:=GetLangText(5143);
          ot_CPoly    : StatusText:=GetLangText(5144);
          ot_Circle   : StatusText:=GetLangText(5145);
          ot_Arc      : StatusText:=GetLangText(5146);
          ot_Symbol   : StatusText:=GetLangText(5147);
          ot_Text     : StatusText:=GetLangText(5148);
          ot_Image    : StatusText:=GetLangText(5149);
          ot_Spline   : StatusText:=GetLangText(5150);
          ot_MesLine  : StatusText:=GetLangText(5151);
          ot_RText    : StatusText:=GetLangText(5152);
          ot_BusGraph : StatusText:=GetLangText(5153);
          ot_OLEObj   : StatusText:=GetLangText(5154);
          else          StatusText:=GetLangText(5155);
        end;
        StatusText:=StatusText+' '+ObjectLayer^.Text^;
//++ Glukhov Bug#66 BUILD#128 19.09.00
//----
//        AData^.SetStatusText(StatusText,FALSE);
//----
// Changed text and algorithm for StatusText
        AllObjCount:= AData^.SelList^.GetCount;
        if AllObjCount > 1 then begin
        // Get a regular text for a selected object
          SelInfo:=CreateSelectInfo;
          SelInfo.Add( VItem );
//          DetailedText:= SelInfo.StatusText;
          DetailedText:= Copy( SelInfo.StatusText, 3, 256 );
        // Prepare the status text
        // 1st variant
          StatusText:= Format(
              MlgStringList.Strings['SelectInfo',1], [AllObjCount] )
              + ' (' + IntToStr(AData^.ShowNum + 1) + ': '
              + StatusText + ')';
        // 2nd variant
          DetailedText:= Format(
              MlgStringList.Strings['SelectInfo',1], [AllObjCount] )
              + ' (' + IntToStr(AData^.ShowNum + 1) + ': '
              + DetailedText + ')';
        // Use the 2nd variant
          StatusText:= DetailedText;

          AData^.nSelObjs:= AllObjCount;
          AData^.ZoomRotRect:= AData^.CurrentViewRect;
          AData^.bShowNum:= True;
// Use StatusBar directly
//          StatusBar[1]:= StatusText;
// Use regular StatusText
          if AData^.StatusText.Count <= 0
          then AData^.SetStatusText( StatusText, True )
          else AData^.SetStatusText( StatusText, False );
        end;
//-- Glukhov Bug#66 BUILD#128 19.09.00
        Inc(AData^.ShowNum);
      end;
  end;

Procedure ProcShowAllSel
   (
   AData           : PProj
   );
  var PosX1        : LongInt;
      PosY1        : LongInt;
      PosX2        : LongInt;
      PosY2        : LongInt;
      RealPosX1    : Real;
      RealPosY1    : Real;
      RealPosX2    : Real;
      RealPosY2    : Real;
      Rect         : TDRect;
      XDist        : Real;
      YDist        : Real;
      NewDist      : Real;
      RealZoom     : Real;
      IsFirst      : Boolean;
      ZoomFact     : Double;
      FrameSize    : Integer;
  Procedure DoAll(Item:PIndex); Far;
    var AItem  : PView;
    begin
      AItem:=AData^.Layers^.SelLayer^.IndexObject(AData^.PInfo,Item);
      if IsFirst then begin
        PosX1:=AItem^.ClipRect.A.X;
        PosY1:=AItem^.ClipRect.A.Y;
        PosX2:=AItem^.ClipRect.B.X;
        PosY2:=AItem^.ClipRect.B.Y;
        IsFirst:=FALSE;
      end
      else begin
        if AItem^.ClipRect.A.X < PosX1 then PosX1:=AItem^.ClipRect.A.X;
        if AItem^.ClipRect.A.Y < PosY1 then PosY1:=AItem^.ClipRect.A.Y;
        if AItem^.ClipRect.B.X > PosX2 then PosX2:=AItem^.ClipRect.B.X;
        if AItem^.ClipRect.B.Y > PosY2 then PosY2:=AItem^.ClipRect.B.Y;
      end;
    end;
  begin
    if (AData^.SelList^.GetCount > 0) or (AData^.Layers^.SelLayer^.SelInfo.Count>0)  then begin
      IsFirst:=TRUE;
      if AData^.SelList^.GetCount = 0 then begin
         PosX1:=AData^.Document.SelREct.A.X;
         PosY1:=AData^.Document.SelREct.A.Y;
         PosX2:=AData^.Document.SelREct.B.X;
         PosY2:=AData^.Document.SelREct.B.Y;
      end else begin
         AData^.SelList^.ForEach(@DoAll);
      end;
      {Rect.Init;}
      if AData^.PInfo^.VariousSettings.ZoomSizeType=0 then begin
        ZoomFact:=AData^.PInfo^.VariousSettings.ZoomSize;
        FrameSize:=0;
      end
      else begin
        ZoomFact:=0;
        FrameSize:=Round(AData^.PInfo^.VariousSettings.ZoomSize*20);
      end;
      RealPosX1:=PosX1*1.0-(PosX2*1.0-PosX1*1.0)/1.00*ZoomFact;
      RealPosY1:=PosY1*1.0-(PosY2*1.0-PosY1*1.0)/1.17*ZoomFact;
      RealPosX2:=PosX2*1.0+(PosX2*1.0-PosX1*1.0)/1.00*ZoomFact;
      RealPosY2:=PosY2*1.0+(PosY2*1.0-PosY1*1.0)/1.17*ZoomFact;
      XDist:=RealPosX2-RealPosX1;
      YDist:=RealPosY2-RealPosY1;
      if XDist < YDist*1.33 then begin
        NewDist:=YDist*1.33;
        RealPosX1:=RealPosX1-(NewDist-XDist)/2;
        RealPosX2:=RealPosX2+(NewDist-XDist)/2;
      end
      else if XDist > YDist*1.33 then begin
        NewDist:=XDist/1.33;
        RealPosY1:=RealPosY1-(NewDist-YDist)/2;
        RealPosY2:=RealPosY2+(NewDist-YDist)/2;
      end;
      if RealPosX1 >= -MaxLongInt then Rect.A.X:=Round(RealPosX1)
      else Rect.A.X:=-MaxLongInt;
      if RealPosY1 >= -MaxLongInt then Rect.A.Y:=Round(RealPosY1)
      else Rect.A.Y:=-MaxLongInt;
      if RealPosX2 <= MaxLongInt then Rect.B.X:=Round(RealPosX2)
      else Rect.B.X:=MaxLongInt;
      if RealPosY2 <= MaxLongInt then Rect.B.Y:=Round(RealPosY2)
      else Rect.B.Y:=MaxLongInt;
      with Rect do RealZoom:=AData^.CalculateZoom(RotRect(A.X,A.Y,XSize,YSize,0),FrameSize,TRUE);
      if RealZoom > IniFile^.MaxZoom then begin
        XDist:=(Rect.B.X-Rect.A.X);
        YDist:=(Rect.B.Y-Rect.A.Y);
        NewDist:=Round(XDist*RealZoom/IniFile^.MaxZoom);
        Rect.A.X:=Rect.A.X-Round((NewDist-XDist)/2);
        Rect.B.X:=Rect.B.X+Round((NewDist-XDist)/2);
        NewDist:=Round(YDist*RealZoom/IniFile^.MaxZoom);
        Rect.A.Y:=Rect.A.Y-Round((NewDist-YDist)/2);
        Rect.B.Y:=Rect.B.Y+Round((NewDist-YDist)/2);
      end;
      with Rect do AData^.ZoomByRect(RotRect(A.X,A.Y,XSize,YSize,0),FrameSize,TRUE);
      {Rect.Done;}
    end;
  end;

Procedure ProcShowAllOnActiveLayer
   (
   AData           : PProj
   );
  var PosX1        : LongInt;
      PosY1        : LongInt;
      PosX2        : LongInt;
      PosY2        : LongInt;
      RealPosX1    : Real;
      RealPosY1    : Real;
      RealPosX2    : Real;
      RealPosY2    : Real;
      Rect         : TDRect;
      XDist        : Real;
      YDist        : Real;
      NewDist      : Real;
      RealZoom     : Real;
      IsFirst      : Boolean;
      ZoomFact     : Double;
      FrameSize    : Integer;
  Procedure DoAll(Item:PIndex); Far;
    var AItem  : PView;
    begin
      AItem:=AData^.Layers^.TopLayer^.IndexObject(AData^.PInfo,Item);
      if IsFirst then begin
        PosX1:=AItem^.ClipRect.A.X;
        PosY1:=AItem^.ClipRect.A.Y;
        PosX2:=AItem^.ClipRect.B.X;
        PosY2:=AItem^.ClipRect.B.Y;
        IsFirst:=FALSE;
      end
      else begin
        if AItem^.ClipRect.A.X < PosX1 then PosX1:=AItem^.ClipRect.A.X;
        if AItem^.ClipRect.A.Y < PosY1 then PosY1:=AItem^.ClipRect.A.Y;
        if AItem^.ClipRect.B.X > PosX2 then PosX2:=AItem^.ClipRect.B.X;
        if AItem^.ClipRect.B.Y > PosY2 then PosY2:=AItem^.ClipRect.B.Y;
      end;
    end;
  begin
    if AData^.Layers^.TopLayer^.Data^.Data^.Count > 0  then begin
      IsFirst:=TRUE;
      AData^.Layers^.TopLayer^.Data^.ForEach(@DoAll);
      {Rect.Init;}
      if AData^.PInfo^.VariousSettings.ZoomSizeType=0 then begin
        ZoomFact:=AData^.PInfo^.VariousSettings.ZoomSize;
        FrameSize:=0;
      end
      else begin
        ZoomFact:=0;
        FrameSize:=Round(AData^.PInfo^.VariousSettings.ZoomSize*20);
      end;
      RealPosX1:=PosX1*1.0-(PosX2*1.0-PosX1*1.0)/1.00*ZoomFact;
      RealPosY1:=PosY1*1.0-(PosY2*1.0-PosY1*1.0)/1.17*ZoomFact;
      RealPosX2:=PosX2*1.0+(PosX2*1.0-PosX1*1.0)/1.00*ZoomFact;
      RealPosY2:=PosY2*1.0+(PosY2*1.0-PosY1*1.0)/1.17*ZoomFact;
      XDist:=RealPosX2-RealPosX1;
      YDist:=RealPosY2-RealPosY1;
      if XDist < YDist*1.33 then begin
        NewDist:=YDist*1.33;
        RealPosX1:=RealPosX1-(NewDist-XDist)/2;
        RealPosX2:=RealPosX2+(NewDist-XDist)/2;
      end
      else if XDist > YDist*1.33 then begin
        NewDist:=XDist/1.33;
        RealPosY1:=RealPosY1-(NewDist-YDist)/2;
        RealPosY2:=RealPosY2+(NewDist-YDist)/2;
      end;
      if RealPosX1 >= -MaxLongInt then Rect.A.X:=Round(RealPosX1)
      else Rect.A.X:=-MaxLongInt;
      if RealPosY1 >= -MaxLongInt then Rect.A.Y:=Round(RealPosY1)
      else Rect.A.Y:=-MaxLongInt;
      if RealPosX2 <= MaxLongInt then Rect.B.X:=Round(RealPosX2)
      else Rect.B.X:=MaxLongInt;
      if RealPosY2 <= MaxLongInt then Rect.B.Y:=Round(RealPosY2)
      else Rect.B.Y:=MaxLongInt;
      with Rect do RealZoom:=AData^.CalculateZoom(RotRect(A.X,A.Y,XSize,YSize,0),FrameSize,TRUE);
      if RealZoom > IniFile^.MaxZoom then begin
        XDist:=(Rect.B.X-Rect.A.X);
        YDist:=(Rect.B.Y-Rect.A.Y);
        NewDist:=Round(XDist*RealZoom/IniFile^.MaxZoom);
        Rect.A.X:=Rect.A.X-Round((NewDist-XDist)/2);
        Rect.B.X:=Rect.B.X+Round((NewDist-XDist)/2);
        NewDist:=Round(YDist*RealZoom/IniFile^.MaxZoom);
        Rect.A.Y:=Rect.A.Y-Round((NewDist-YDist)/2);
        Rect.B.Y:=Rect.B.Y+Round((NewDist-YDist)/2);
      end;
      with Rect do AData^.ZoomByRect(RotRect(A.X,A.Y,XSize,YSize,0),FrameSize,TRUE);
      {Rect.Done;}
    end;
  end;

{$IFNDEF AXDLL} // <----------------- AXDLL
Procedure ProcInsertObjectFromDB
   (
   AData           : PProj;
   DDELine         : PChar
   );
  var Typ          : Byte;
      Number       : LongInt;
      RXPos        : Real;
      RYPos        : Real;
      XPos         : LongInt;
      YPos         : LongInt;
      Pos          : TDPoint;
      TmpID        : PChar;
      DBID         : Array[0..255] of Char;
      TempVal      : String;
      TempStr      : Array[0..255] of Char;
      Error        : Integer;
      TempPtr      : PChar;
      NewPoint     : PPixel;
      NewSymbol    : PSymbol;
      NewLine      : PPoly;
      NewArea      : PCPoly;
      NewCircle    : PEllipse;
      NewArc       : PEllipseArc;
      SymSize      : Real;
      NewPolyPoint : TDPoint;
      AllPoints    : LongInt;
      LinePoints   : LongInt;
      PointCount   : LongInt;
      ProjID       : LongInt;
      NewLName     : Array[0..255] of Char;
      LayerName    : Array[0..255] of Char;
      LName        : String;
      NLName       : String;
      NName        : String;
      CreateOnLayer: PLayer;
      OldTop       : PLayer;
      DGRLData     : PDGRLayerData;
      IsOK         : Boolean;
      HasSize      : Boolean;
      RealSymSize  : Real;
      AktSymbol    : LongInt;
      SItem        : PIndex;
      ShowNewLayDlg: Boolean;
      ResInt       : LongInt;
      StrOK        : Boolean;
      StrFound     : Boolean;
      MoreStrings  : Boolean;
      StrToLong    : Boolean;
      TempReal     : Real;
      AllParams    : Boolean;
      ObjectChanged: Boolean;
  Procedure MakeLayerColl;
    begin
      if LName <> CreateOnLayer^.Text^ then begin
        if MsgBox2(AData^.Parent.Handle,11148,mb_YesNo,LName,CreateOnLayer^.Text^) = idYes then begin
          DGRLData:=New(PDGRLayerData);
          DGRLData^.LayerName:=StrNew(LayerName);
          StrPCopy(NewLName,CreateOnLayer^.Text^);
          DGRLData^.NewLName:=StrNew(NewLName);
          AData^.DGRLayerDefColl^.Insert(DGRLData);
        end;
      end;
    end;
  Function SearchLName
     (
     Item          : PDGRLayerData
     )
     : Boolean; Far;
    begin
      if Item^.LayerName = NIL then begin
        if StrComp(LayerName,'') = 0 then Result:=TRUE
        else Result:=FALSE;
      end
      else Result:=StrIComp(Item^.LayerName,LayerName) = 0;
    end;
  Function SearchSymbol
     (
     Item          : PIndex
     )
     : Boolean; Far;
    begin
      Result:=Item^.Index = Number;
    end;
  begin
    AllParams:=TRUE;
    StrOK:=ReadDDECommandLongInt(DDELine,1,0,WgBlockStart,DDEHandler.FDDESepSign,                {Objektart}
                                             WgBlockEnd,ResInt,StrFound,MoreStrings);
    if StrFound and StrOK then Typ:=ResInt
    else AllParams:=FALSE;
    StrOK:=ReadDDECommandLongInt(DDELine,1,1,WgBlockStart,DDEHandler.FDDESepSign,             {Symbolnummer}
                                             WgBlockEnd,ResInt,StrFound,MoreStrings);
    if StrFound and StrOK then Number:=ResInt
    else AllParams:=FALSE;
    StrOK:=ReadDDECommandReal(DDELine,1,2,WgBlockStart,DDEHandler.FDDESepSign,                {x-Koordinate}
                                             WgBlockEnd,TempReal,StrFound,MoreStrings);
    if StrFound and StrOK then begin
      {if AData^.PInfo^.CoordinateSystem.CoordinateType=ctGeodatic then RYPos:=TempReal
      else} RXPos:=TempReal;
    end
    else AllParams:=FALSE;
    StrOK:=ReadDDECommandReal(DDELine,1,3,WgBlockStart,DDEHandler.FDDESepSign,                {y-Koordinate}
                                             WgBlockEnd,TempReal,StrFound,MoreStrings);
    if StrFound and StrOK then begin
      {if AData^.PInfo^.CoordinateSystem.CoordinateType=ctGeodatic then RXPos:=TempReal
      else} RYPos:=TempReal;
    end
    else AllParams:=FALSE;
    StrOK:=ReadDDECommandPChar(DDELine,1,4,WgBlockStart,DDEHandler.FDDESepSign,                      {DB-ID}
                                           WgBlockEnd,TmpID,MoreStrings);
    if TmpID <> NIL then begin
      StrCopy(DBID,TmpID);
      StrDispose(TmpID);
    end
    else StrCopy(DBID,'0');
    StrOK:=ReadDDECommandString(DDELine,1,5,WgBlockStart,DDEHandler.FDDESepSign,                     {Layer}
                                            WgBlockEnd,TempVal,StrToLong,MoreStrings);
    if TempVal = '' then begin
      if DDEHandler.FGotLInfo = 2 then Str(AData^.Layers^.TopLayer^.Index:0,TempVal)
      else TempVal:=PToStr(AData^.Layers^.TopLayer^.Text);
    end;
    if DDEHandler.FGotLInfo = 2 then begin
      Val(TempVal,ResInt,Error);
      if (Error = 0) and (ResInt <> 0) then begin
        CreateOnLayer:=AData^.Layers^.IndexToLayer(ResInt);
        if CreateOnLayer <> NIL then begin
          LName:=CreateOnLayer^.Text^;
          StrPCopy(LayerName,LName);
        end
        else begin
          LName:='';
          StrCopy(LayerName,'');
        end;
      end
      else begin
        CreateOnLayer:=NIL;
        LName:='';
        StrCopy(LayerName,'');
      end;
    end
    else begin
      LName:=TempVal;
      StrPCopy(LayerName,LName);
      CreateOnLayer:=AData^.Layers^.NameToLayer(LName);
    end;
    if MoreStrings then begin
      StrOK:=ReadDDECommandReal(DDELine,1,6,WgBlockStart,DDEHandler.FDDESepSign,               {Symbolgr��e}
                                            WgBlockEnd,SymSize,StrFound,MoreStrings);
      if SymSize > 0 then HasSize:=TRUE
      else HasSize:=FALSE;
    end
    else HasSize:=FALSE;
    if CreateOnLayer = NIL then ShowNewLayDlg:=TRUE
    else ShowNewLayDlg:=FALSE;
    if (CreateOnLayer <> NIL) and (CreateOnLayer^.GetState(sf_Fixed)) then begin
      DGRLData:=AData^.DGRLayerDefColl^.FirstThat(@SearchLName);
      if DGRLData <> NIL then begin
        NName:=StrPas(DGRLData^.NewLName);
        CreateOnLayer:=AData^.Layers^.NameToLayer(NName);
      end
      else begin
        if MsgBox(AData^.Parent.Handle,3540,mb_IconQuestion or mb_YesNo,LName) = id_Yes then begin
          if InputCheckPassword(AData^.Parent) then CreateOnLayer^.SetState(sf_Fixed,FALSE)
          else begin
            MsgBox(AData^.Parent.Handle,3541,mb_IconExclamation or mb_OK,'');
            CreateOnLayer:=NIL;
          end;
        end
        else CreateOnLayer:=NIL;
      end;
    end;
    if (CreateOnLayer <> NIL) and (CreateOnLayer^.GetState(sf_LayerOff)) then begin
      DGRLData:=AData^.DGRLayerDefColl^.FirstThat(@SearchLName);
      if DGRLData <> NIL then begin
        NName:=StrPas(DGRLData^.NewLName);
        CreateOnLayer:=AData^.Layers^.NameToLayer(NName);
      end
      else begin
//        if MsgBox(AData^.Parent.Handle,3544,mb_IconQuestion or mb_YesNo,LName) = id_Yes then
//          CreateOnLayer^.SetState(sf_LayerOff,FALSE)
//        else CreateOnLayer:=NIL;
      end;
    end;
    if CreateOnLayer = NIL then begin
      DGRLData:=AData^.DGRLayerDefColl^.FirstThat(@SearchLName);
      if DGRLData <> NIL then begin
        NName:=StrPas(DGRLData^.NewLName);
        CreateOnLayer:=AData^.Layers^.NameToLayer(NName);
      end
      else begin
        if ShowNewLayDlg then begin
          CreateOnLayer:=NewLayerDialog1(AData,GetLangText(10154),LName);
          if CreateOnLayer<>NIL then MakeLayerColl
          else ShowNewLayDlg:=FALSE;
        end;
        if not ShowNewLayDlg then repeat
          IsOK:=FALSE;
          CreateOnLayer:=SelectLayerDialog(AData,[sloNewLayer]);
          if CreateOnLayer<>NIL then begin
            MakeLayerColl;
            IsOK:=TRUE;
          end
          else if MsgBox(AData^.Parent.Handle,11149,mb_IconExclamation or mb_RetryCancel,'') = idCancel then IsOK:=TRUE;
        until IsOK;
      end;
    end;
    if AllParams and (CreateOnLayer <> NIL) then begin
      OldTop:=AData^.Layers^.TopLayer;
      AData^.Layers^.TopLayer:=CreateOnLayer;
      if AData^.Layers^.TopLayer^.GetState(sf_Fixed) then MsgBox(AData^.Parent.Handle,11014,mb_Ok+mb_IconExclamation,'')
      else begin
//++ Glukhov CoordinatesAreTruncated Build#158 10.05.01
//        XPos:=Trunc(RXPos);
//        YPos:=Trunc(RYPos);
        XPos:=Round( RXPos * 100 );
        YPos:=Round( RYPos * 100 );
//-- Glukhov CoordinatesAreTruncated Build#158 10.05.01
        Pos.Init(XPos,YPos);
        if Typ = 0 then begin
//++ Glukhov CoordinatesAreTruncated Build#158 10.05.01
//          Pos.x:=Pos.x*100;
//          Pos.y:=Pos.y*100;
//-- Glukhov CoordinatesAreTruncated Build#158 10.05.01
          NewPoint:=New(PPixel,Init(Pos));
          if AData^.InsertObject(NewPoint,FALSE) then begin
            if DDEHandler.FLayerInfo <> 0 then begin
              Str(CreateOnLayer^.Index:10,TempStr);
              AData^.InsertedObjects^.Insert(StrNew(TempStr));
            end;
            ProjID:=NewPoint^.Index;
            AData^.InsertedObjects^.Insert(StrNew(@DBID));
            Str(ProjID:10,TempStr);
            AData^.InsertedObjects^.Insert(StrNew(TempStr));
          end
          else begin
            if NewPoint <> NIL then begin
              Dispose(NewPoint,Done);
              NewPoint:=NIL;
            end;
          end;
        end
        else if Typ = 1 then begin
          if Number = 0 then begin
             if AData^.ActualSym <> nil then
                Number:=AData^.ActualSym^.Index;
          end
          else Number:=Number+600000000;
//++ Glukhov CoordinatesAreTruncated Build#158 10.05.01
//          Pos.x:=Pos.x*100;
//          Pos.y:=Pos.y*100;
//-- Glukhov CoordinatesAreTruncated Build#158 10.05.01
          SItem:=PSymbols(AData^.PInfo^.Symbols)^.Data^.FirstThat(@SearchSymbol);
          if SItem <> NIL then begin
            NewSymbol:=New(PSymbol,Init(AData^.PInfo,Pos,Number,1,0));
            if HasSize then begin
              PSymbols(AData^.PInfo^.Symbols)^.Data^.Search(PIndex(NewSymbol),AktSymbol);
              if DblCompare(SymSize,dblNoChange) <> 0 then begin
                NumTools.Update(NewSymbol^.Size,SymSize,ObjectChanged);
                NumTools.Update(NewSymbol^.SizeType,NewSymbol^.SizeType,ObjectChanged);
              end;
              NewSymbol^.CalculateClipRect(AData^.PInfo);
            end;
            if AData^.InsertObject(NewSymbol,FALSE) then begin
              if DDEHandler.FLayerInfo <> 0 then begin
                Str(CreateOnLayer^.Index:10,TempStr);
                AData^.InsertedObjects^.Insert(StrNew(TempStr));
              end;
              ProjID:=NewSymbol^.Index;
              AData^.InsertedObjects^.Insert(StrNew(@DBID));
              Str(ProjID:10,TempStr);
              AData^.InsertedObjects^.Insert(StrNew(TempStr));
            end
            else begin
              if NewSymbol <> NIL then begin
                Dispose(NewSymbol,Done);
                NewSymbol:=NIL;
              end;
            end;
          end
          else begin
            Number:=Number-600000000;
            Str(Number,NName);
            MsgBox(AData^.Parent.Handle,2101,mb_IconExclamation or mb_OK,NName);
          end;
        end
        else if Typ = 2 then begin                                                                                  {Linie}
          StrOK:=ReadDDECommandLongInt(DDELine,2,0,WgBlockStart,DDEHandler.FDDESepSign,WgBlockEnd,
                                                   AllPoints,StrFound,MoreStrings);
          StrOK:=ReadDDECommandLongInt(DDELine,3,0,WgBlockStart,DDEHandler.FDDESepSign,WgBlockEnd,
                                                   LinePoints,StrFound,MoreStrings);
//++ Glukhov Bug#100 Build#152 26.03.01
          AData^.NewPoly^.bNoDrawing:= True;
//-- Glukhov Bug#100 Build#152 26.03.01
          for PointCount:=4 to LinePoints+3 do begin
            StrOK:=ReadDDECommandReal(DDELine,PointCount,0,WgBlockStart,DDEHandler.FDDESepSign,
                                                  WgBlockEnd,TempReal,StrFound,MoreStrings);                 {X-Koordinate}
            NewPolyPoint.X:=Round(TempReal*100);
            StrOK:=ReadDDECommandReal(DDELine,PointCount,1,WgBlockStart,DDEHandler.FDDESepSign,
                                                  WgBlockEnd,TempReal,StrFound,MoreStrings);                 {Y-Koordinate}
            NewPolyPoint.Y:=Round(TempReal*100);
            AData^.NewPoly^.PointInsert(AData^.PInfo,NewPolyPoint,TRUE);
          end;
//++ Glukhov Bug#100 Build#152 26.03.01
          AData^.NewPoly^.bNoDrawing:= False;
//-- Glukhov Bug#100 Build#152 26.03.01
          if AData^.NewPoly^.Data^.Count >= AllPoints then begin
            NewLine:=New(PPoly,Init);
//++ Glukhov Bug#100 Build#152 26.03.01
//            if not AData^.NewPoly^.CheckCopyData(AData^.PInfo,NewLine,TRUE) then begin
            AData^.NewPoly^.bNoDrawing:= True;
            StrOK:= AData^.NewPoly^.CheckCopyData(AData^.PInfo,NewLine,TRUE);
            AData^.NewPoly^.bNoDrawing:= False;
            if not StrOK then begin
//-- Glukhov Bug#100 Build#152 26.03.01
              Dispose(NewLine,Done);
              NewLine:=NIL;
            end
            else begin
              if AData^.InsertObject(NewLine,FALSE) then begin
                if DDEHandler.FLayerInfo <> 0 then begin
                  Str(CreateOnLayer^.Index:10,TempStr);
                  AData^.InsertedObjects^.Insert(StrNew(TempStr));
                end;
                ProjID:=NewLine^.Index;
                AData^.InsertedObjects^.Insert(StrNew(@DBID));
                Str(ProjID:10,TempStr);
                AData^.InsertedObjects^.Insert(StrNew(TempStr));
              end
              else begin
                Dispose(NewLine,Done);
                NewLine:=NIL;
              end;
            end;
          end;
        end
        else if Typ = 3 then begin                                                                                 {Fl�che}
          StrOK:=ReadDDECommandLongInt(DDELine,2,0,WgBlockStart,DDEHandler.FDDESepSign,WgBlockEnd,
                                                   AllPoints,StrFound,MoreStrings);
          StrOK:=ReadDDECommandLongInt(DDELine,3,0,WgBlockStart,DDEHandler.FDDESepSign,WgBlockEnd,
                                                   LinePoints,StrFound,MoreStrings);
          AData^.NewPoly^.bNoDrawing:= True;
          for PointCount:=4 to LinePoints+3 do begin
            StrOK:=ReadDDECommandReal(DDELine,PointCount,0,WgBlockStart,DDEHandler.FDDESepSign,
                                                  WgBlockEnd,TempReal,StrFound,MoreStrings);                 {X-Koordinate}
            NewPolyPoint.X:=Round(TempReal*100);
            StrOK:=ReadDDECommandReal(DDELine,PointCount,1,WgBlockStart,DDEHandler.FDDESepSign,
                                                  WgBlockEnd,TempReal,StrFound,MoreStrings);                 {Y-Koordinate}
            NewPolyPoint.Y:=Round(TempReal*100);
            AData^.NewPoly^.PointInsert(AData^.PInfo,NewPolyPoint,TRUE);
          end;
          if AData^.NewPoly^.Data^.Count >= AllPoints then begin
            NewArea:=New(PCPoly,Init);
            if not AData^.NewPoly^.CheckCopyData(AData^.PInfo,NewArea,TRUE) then begin
              Dispose(NewArea,Done);
              NewArea:=NIL;
            end
            else begin
              if AData^.InsertObject(NewArea,FALSE) then begin
                if DDEHandler.FLayerInfo <> 0 then begin
                  Str(CreateOnLayer^.Index:10,TempStr);
                  AData^.InsertedObjects^.Insert(StrNew(TempStr));
                end;
                ProjID:=NewArea^.Index;
                AData^.InsertedObjects^.Insert(StrNew(@DBID));
                Str(ProjID:10,TempStr);
                AData^.InsertedObjects^.Insert(StrNew(TempStr));
              end
              else begin
                Dispose(NewArea,Done);
                NewArea:=NIL;
              end;
            end;
          end;
          AData^.NewPoly^.bNoDrawing:= False;
        end
        else if Typ = 4 then begin                                                                                  {Kreis}
          StrOK:=ReadDDECommandReal(DDELine,2,0,WgBlockStart,DDEHandler.FDDESepSign,
                                                WgBlockEnd,TempReal,StrFound,MoreStrings);                         {Radius}
          ResInt:=Round(TempReal*100);
          if ResInt > 0 then begin
            NewCircle:=New(PEllipse,Init(Pos,ResInt,ResInt,0));
            if AData^.InsertObject(NewCircle,FALSE) then begin
              if DDEHandler.FLayerInfo <> 0 then begin
                Str(CreateOnLayer^.Index:10,TempStr);
                AData^.InsertedObjects^.Insert(StrNew(TempStr));
              end;
              ProjID:=NewCircle^.Index;
              AData^.InsertedObjects^.Insert(StrNew(@DBID));
              Str(ProjID:10,TempStr);
              AData^.InsertedObjects^.Insert(StrNew(TempStr));
            end
            else begin
              Dispose(NewCircle,Done);
              NewCircle:=NIL;
            end;
          end;
        end
        else if Typ = 5 then begin                                                                             {Kreisbogen}
          StrOK:=ReadDDECommandReal(DDELine,2,0,WgBlockStart,DDEHandler.FDDESepSign,
                                                WgBlockEnd,TempReal,StrFound,MoreStrings);                         {Radius}
          ResInt:=Round(TempReal*100);
          StrOK:=ReadDDECommandReal(DDELine,3,0,WgBlockStart,DDEHandler.FDDESepSign,
                                                WgBlockEnd,RXPos,StrFound,MoreStrings);                     {Anfangswinkel}
          while RXPos >= 360 do RXPos:=RXPos-360;
          while RXPos < 0 do RXPos:=RXPos+360;
          RXPos:=RXPos*Pi/180;
          StrOK:=ReadDDECommandReal(DDELine,3,1,WgBlockStart,DDEHandler.FDDESepSign,
                                                WgBlockEnd,RYPos,StrFound,MoreStrings);                         {Endwinkel}
          while RYPos >= 360 do RYPos:=RYPos-360;
          while RYPos < 0 do RYPos:=RYPos+360;
          RYPos:=RYPos*Pi/180;
          if ResInt > 0 then begin
            NewArc:=New(PEllipseArc,Init(Pos,ResInt,ResInt,0,RXPos,RYPos));
            if AData^.InsertObject(NewArc,FALSE) then begin
              if DDEHandler.FLayerInfo <> 0 then begin
                Str(CreateOnLayer^.Index:10,TempStr);
                AData^.InsertedObjects^.Insert(StrNew(TempStr));
              end;
              ProjID:=NewArc^.Index;
              AData^.InsertedObjects^.Insert(StrNew(@DBID));
              Str(ProjID:10,TempStr);
              AData^.InsertedObjects^.Insert(StrNew(TempStr));
            end
            else begin
              Dispose(NewArc,Done);
              NewArc:=NIL;
            end;
          end;
        end;
        if AData^.SelNewObjectsDB then begin
          case Typ of
            0 : if NewPoint <> NIL then AData^.Select(NewPoint);
            1 : if NewSymbol <> NIL then AData^.Select(NewSymbol);
            2 : if NewLine <> NIL then AData^.Select(NewLine);
            3 : if NewArea <> NIL then AData^.Select(NewArea);
            4 : if NewCircle <> NIL then AData^.Select(NewCircle);
            5 : if NewArc <> NIL then AData^.Select(NewArc);
          end;
        end;
      end;
      AData^.Layers^.TopLayer:=OldTop;
    end;
  end;

Procedure ProcInsertObjectsFromDB
   (
   AData           : PProj
   );
  var ACursor      : PChar;
      ASize        : TDRect;
      OldMenu      : Integer;
  Procedure InsertObjsFromColl
     (
     Item : PChar
     ); Far;
    begin
      ProcInsertObjectFromDB(AData,Item);
      Item[0]:=DDEStrUsed;
    end;
  begin
    OldMenu:=AData^.ActualMen;
    AData^.SetActualMenu(mn_Select);
    AData^.SetCursor(crHourGlass);
    AData^.SetStatusText(GetLangText(4025),TRUE);
    AData^.RedrawAfterInsert:=FALSE;
    DDEHandler.DDEData^.ForEach(@InsertObjsFromColl);
    DDEHandler.DeleteDDEData;
    AData^.RedrawAfterInsert:=TRUE;
    if AData^.BorderCorrected then begin
{!?!?!      ACursor:=AData^.ActualCursor;
      if AData^.OverView then begin
        AData^.SetOverview;
        AData^.PInfo^.RedrawScreen(TRUE);
      end;
      AData^.SetCursor(ACursor);    }
      AData^.BorderCorrected:=FALSE;
    end;
    ProcSendNewIDsToDB(AData,'GRD');                  { Procedure in AM_ProjO }
    DDEHandler.SetAnswerMode(FALSE);
    AData^.Size.Init;
    PLayer(AData^.PInfo^.Objects)^.GetSize(AData^.Size);
    if AData^.Size.IsEmpty then begin
       AData^.Size.A.Init(0, 0);
       AData^.Size.B.Init(siStartX, siStartY);
    end;
    AData^.ClearStatusText;
    AData^.SetCursor(crDefault);
    AData^.SetActualMenu(OldMenu);
  end;

Procedure ProcSendNewPartPoliesToDB
   (
   AData           : PProj
   );
  var SItem        : PIndex;
  Procedure SendPolyInfos
     (
     Item          : PCPoly
     ); Far;
    var DDEString  : String;
        TempStr    : String;
        TempID     : LongInt;
        Error      : Integer;
        FoundItem  : PIndex;
        APoly      : PCPoly;
        TempVal    : Double;
    begin
      DDEString:='[PPR][';
      if DDEHandler.FLayerInfo = 2 then begin
        Str(AData^.PolyPartProps.SLayerIndex:10,TempStr);
        DDEString:=DDEString+TempStr+'][';
      end
      else DDEString:=DDEString+AData^.PolyPartProps.SLayerName+'][';
      Str(AData^.PolyPartProps.SID:10,TempStr);
      DDEString:=DDEString+TempStr+'][';
      if DDEHandler.FLayerInfo = 2 then begin
        Str(AData^.PolyPartProps.DLayerIndex:10,TempStr);
        DDEString:=DDEString+TempStr+'][';
      end
      else DDEString:=DDEString+AData^.PolyPartProps.DLayerName+'][';
      Str(Item^.Index:10,TempStr);
      DDEString:=DDEString+TempStr+'][';
      TempVal:=Item^.Flaeche;
      Str(TempVal:15:2,TempStr);
      DDEString:=DDEString+TempStr+'][';
      TempVal:=Item^.Laenge;
      Str(TempVal:15:2,TempStr);
      DDEString:=DDEString+TempStr+']';
      DDEHandler.SendString(DDEString);
    end;
  begin
    DDEHandler.SendString('[END][]');
  end;

Procedure ProcSendNewPartPolyToDB
   (
   AData           : PProj;
   SLayerName      : String;
   SLayerIndex     : LongInt;
   DLayerName      : String;
   DLayerIndex     : LongInt;
   SPolyID         : LongInt;
   APoly           : PCPoly;
   SendEND         : Boolean
   );
  var DDEString  : String;
      TempStr    : String;
      TempVal    : Double;
  begin
   if APoly <> nil then begin
    DDEString:='[PPR][';
    if DDEHandler.FLayerInfo = 2 then begin
      Str(SLayerIndex:10,TempStr);
      DDEString:=DDEString+TempStr+'][';
    end
    else DDEString:=DDEString+SLayerName+'][';
    Str(SPolyID:10,TempStr);
    DDEString:=DDEString+TempStr+'][';
    if DDEHandler.FLayerInfo = 2 then begin
      Str(DLayerIndex:10,TempStr);
      DDEString:=DDEString+TempStr+'][';
    end
    else DDEString:=DDEString+DLayerName+'][';
    Str(APoly^.Index:10,TempStr);
    DDEString:=DDEString+TempStr+'][';
    TempVal:=APoly^.Flaeche;
    Str(TempVal:15:2,TempStr);
    DDEString:=DDEString+TempStr+'][';
    TempVal:=APoly^.Laenge;
    Str(TempVal:15:2,TempStr);
    DDEString:=DDEString+TempStr+']';
    DDEHandler.SendString(DDEString);
    if SendEND then DDEHandler.SendString('[END][]');
   end;
  end;

Procedure ProcSendNewIDsToDB
   (
   AData           : PProj;
   CommStr         : String
   );
  var IDString     : String[25];
      TempStr      : Array[0..25] of Char;
      EntryNr      : Word;
      IDCount      : Integer;
      DDEStrColl   : PBigUStrColl;
      MaxIDCount   : Integer;
      NameLength   : Integer;
      LastName     : String;
      StrToLong    : Boolean;
      MoreStrs     : Boolean;
  Procedure DoAll
     (
     Item          : PChar
     ); Far;
    var TempItem   : PChar;
        ActName    : String;
    begin
      if DDEHandler.FLayerInfo = 0 then begin
        if IDCount = 0 then DDEHandler.StartList('['+CommStr+'][   ]');
        DDEHandler.AppendString(StrPas(Item));
      end
      else if (DDEHandler.FLayerInfo = 1) or (DDEHandler.FLayerInfo = 2) then begin
        if IDCount = 0 then begin
          ReadDDECommandString(Item,0,0,WgBlockStart,WgBlockEnd,WgBlockEnd,LastName,StrToLong,MoreStrs);
          NameLength:=Length(LastName);
          DDEHandler.StartList('['+CommStr+']['+LastName+'][   ]');
          TempItem:=StrPos(Item,']');
          TempItem:=StrPos(TempItem,'[');
          DDEHandler.AppendString(StrPas(@TempItem[1]));
        end
        else begin
          ReadDDECommandString(Item,0,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ActName,StrToLong,MoreStrs);
          if ActName <> LastName then begin
            DDEHandler.EndList1(FALSE,NameLength);
            IDCount:=0;
            LastName:=ActName;
            NameLength:=Length(LastName);
            DDEHandler.StartList('['+CommStr+']['+LastName+'][   ]');
          end;
          TempItem:=StrPos(Item,']');
          TempItem:=StrPos(TempItem,'[');
          DDEHandler.AppendString(StrPas(@TempItem[1]));
        end;
      end;
      Inc(IDCount);
      if IDCount = MaxIDCount then begin
        IDCount:=0;
        if DDEHandler.FLayerInfo = 0 then DDEHandler.EndList(FALSE)
        else if (DDEHandler.FLayerInfo = 1) or (DDEHandler.FLayerInfo = 2) then DDEHandler.EndList1(FALSE,NameLength);
      end;
    end;
  Procedure MakeDDEEntries
     (
     Item          : PChar
     ); Far;
    var TmpStr     : Array[0..255] of Char;
        TmpVal     : LongInt;
        Error      : Integer;
        DestLayer  : PLayer;
        DDEStr     : Array[0..255] of Char;
    begin
      if EntryNr = 0 then begin
        if DDEHandler.FLayerInfo = 0 then begin
          EntryNr:=1
        end
        else if DDEHandler.FLayerInfo = 1 then begin
          StrCopy(TmpStr,Item);
          Val(TmpStr,TmpVal,Error);
          DestLayer:=AData^.Layers^.IndexToLayer(TmpVal);
          if (DestLayer = nil) or (DestLayer^.Text = nil) then
              StrPCopy(TmpStr,'')
          else
              StrPCopy(TmpStr,DestLayer^.Text^);
          StrCopy(DDEStr,'[');
          StrCat(DDEStr,TmpStr);
          StrCat(DDEStr,'][');
          StrDispose(Item);
        end
        else if DDEHandler.FLayerInfo = 2 then begin
          StrCopy(DDEStr,'[');
          StrCat(DDEStr,Item);
          StrCat(DDEStr,'][');
          StrDispose(Item);
        end;
      end;
      if EntryNr = 1 then begin
        if DDEHandler.FLayerInfo = 0 then begin
           StrCopy(DDEStr,Item);
        end
        else begin
           StrCat(DDEStr,Item);
        end;
        StrCat(DDEStr,StrPCopy(TmpStr,DDEHandler.FOutSepSign));
        StrDispose(Item);
      end
      else if EntryNr = 2 then begin
        StrCat(DDEStr,Item);
        StrDispose(Item);
      end;
      Inc(EntryNr);
      if EntryNr > 2 then begin
        DDEStrColl^.Insert(StrNew(DDEStr));
        EntryNr:=0;
      end;
    end;
  Procedure DelEntries
     (
     Item          : PDGRLayerData
     ); Far;
    begin
      StrDispose(Item^.LayerName);
      StrDispose(Item^.NewLName);
    end;
  Procedure DelEntries1
     (
     Item          : PChar
     ); Far;
    begin
      StrDispose(Item);
    end;
  begin
    AData^.DGRLayerDefColl^.ForEach(@DelEntries);
    AData^.DGRLayerDefColl^.DeleteAll;
    if AData^.InsertedObjects^.GetCount > 0 then begin
      DDEStrColl:=New(PBigUStrColl,Init);
      EntryNr:=0;
      AData^.InsertedObjects^.ForEach(@MakeDDEEntries);
      AData^.InsertedObjects^.DeleteAll;
      if DDEHandler.FLayerInfo = 2 then MaxIDCount:=160
      else MaxIDCount:=170;
      IDCount:=0;
      DDEStrColl^.ForEach(@DoAll);
      if DDEHandler.FLayerInfo = 0 then DDEHandler.EndList(TRUE)
      else if (DDEHandler.FLayerInfo = 1) or (DDEHandler.FLayerInfo = 2) then DDEHandler.EndList1(TRUE,NameLength);
      DDEStrColl^.ForEach(@DelEntries1);
      DDEStrColl^.DeleteAll;
      Dispose(DDEStrColl,Done);
    end;
  end;

Procedure ProcSetSelModeGIS
   (
   AData           : PProj
   );
  var ResLInt      : LongInt;
      SFound       : Boolean;
      MStrs        : Boolean;
  begin
    if ReadDDECommandLongInt(DDEHandler.DDEData^.At(0),1,0,WgBlockStart,WgBlockEnd,
                                         WgBlockEnd,ResLInt,SFound,MStrs) then begin
      if ResLInt = 1 then AData^.SelNewObjectsGIS:=TRUE
      else AData^.SelNewObjectsGIS:=FALSE;
    end;
  end;

Procedure ProcMoveRelFromDB
   (
   AData    : PProj
   );
  var SItem        : PIndex;
      Entries      : Integer;
      Cnt          : Integer;
      ErrorCode    : Byte;
      Redraw       : TDRect;
      ResLInt      : LongInt;
      ResStr       : String;
      MoveOnAllLay : Boolean;
      MoveLayer    : PLayer;
      Moved        : Boolean;
  Procedure MoveObjs
     (
     Item    : PChar
     ); Far;
    var TempPointer  : PChar;
        TempVal      : String;
        ItemInd      : Longint;
        Error        : Integer;
        OldTop       : PLayer;
        ExistObject  : PIndex;
        i            : Integer;
        RealMove     : Real;
        XMove        : LongInt;
        YMove        : LongInt;
        DoIt         : Boolean;
        AView        : PView;
        SFound       : Boolean;
        MStrs        : Boolean;
        StrToLong    : Boolean;
        MoreStrs     : Boolean;
        StrOK        : Boolean;
    begin
      if DDEHandler.FGotLInfo = 0 then MoveOnAllLay:=TRUE
      else if DDEHandler.FGotLInfo = 1 then begin
        StrOK:=ReadDDECommandString(Item,1,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ResStr,StrToLong,MoreStrs);
        if StrOK and (ResStr <> '') then begin                                                                  {Layername}
          MoveLayer:=AData^.Layers^.NameToLayer(ResStr);
          if MoveLayer = NIL then MoveOnAllLay:=TRUE
          else MoveOnAllLay:=FALSE;
        end
        else MoveOnAllLay:=TRUE;
      end
      else if DDEHandler.FGotLInfo = 2 then begin
        if ReadDDECommandLongInt(Item,1,0,WgBlockStart,WgBlockEnd,                                 {Layerindex}
                                             WgBlockEnd,ResLInt,SFound,MStrs) then begin
          MoveLayer:=AData^.Layers^.IndexToLayer(ResLInt);
          if MoveLayer = NIL then MoveOnAllLay:=TRUE
          else MoveOnAllLay:=FALSE;
        end
        else MoveOnAllLay:=TRUE;
      end;
      TempPointer:=NextDDEPart(NextDDEPart(Item));
      if DDEHandler.FGotLInfo <> 0 then TempPointer:=NextDDEPart(TempPointer);
      ReadDDEString(TempPointer,']',TempVal);
      Val(TempVal,ItemInd,Error);
      SItem^.Index:=ItemInd;
      OldTop:=AData^.Layers^.TopLayer;
      if not MoveOnAllLay then AData^.Layers^.TopLayer:=MoveLayer;
      ExistObject:=AData^.Layers^.TopLayer^.HasObject(SItem);
      if (ExistObject = NIL) and MoveOnAllLay then begin                                 {>>>>>}
        i:=0;
        while (ExistObject = NIL) and (i < AData^.Layers^.LData^.Count) do begin         { Diese Zeilen ausklammern, }
          AData^.Layers^.TopLayer:=AData^.Layers^.LData^.At(i);                          { wenn Objekte nur am obersten }
          ExistObject:=AData^.Layers^.TopLayer^.HasObject(SItem);                        { Layer verschoben werden sollen }
          i:=i+1;
        end;
      end;                                                                               {<<<<<}
      if ExistObject <> NIL then begin
        ReadDDEString(TempPointer,DDEHandler.FDDESepSign,TempVal);
        Val(TempVal,RealMove,Error);
        if RealMove*100 < MaxLongInt then XMove:=Round(RealMove*100)
        else XMove:=MaxLongInt;
        ReadDDEString(TempPointer,']',TempVal);
        Val(TempVal,RealMove,Error);
        if RealMove*100 < MaxLongInt then YMove:=Round(RealMove*100)
        else YMove:=MaxLongInt;
        if not AData^.Layers^.TopLayer^.GetState(sf_Fixed) then DoIt:=TRUE
        else begin
          DoIt:=FALSE;
          ErrorCode:=ErrorCode or 1;
        end;
        if DoIt then begin
          if AData^.Layers^.TopLayer^.CheckMove(AData^.PInfo,XMove,YMove) then begin
            if not AData^.Layers^.IsObjectFixed(SItem) then begin
              AView:=AData^.Layers^.IndexObject(AData^.PInfo,SItem);
              Redraw.CorrectByRect(AView^.ClipRect);
              AView^.MoveRel(XMove,YMove);
              AData^.UpdateCliprect(AView);
              Redraw.CorrectByRect(AView^.ClipRect);
              Moved:=TRUE;
            end;
          end
          else MsgBox(AData^.Parent.Handle,4100,mb_OK or mb_IconStop,'');
        end;
        if AData^.Layers^.SelLayer^.HasObject(AView) <> NIL then AData^.SelAllRect.CorrectByRect(AView^.ClipRect);
      end
      else begin
        if MoveOnAllLay then ErrorCode:=ErrorCode or 2
        else ErrorCode:=ErrorCode or 4;
      end;
      Inc(Cnt);
      StatusBar.Progress:=100*Cnt/Entries;
      if StatusBar.QueryAbort then Exit;
      AData^.Layers^.TopLayer:=OldTop;
      Item[0]:=DDEStrUsed;
    end;
  begin
    AData^.SetCursor(crHourGlass);
    SItem:=New(PIndex,Init(0));
    StatusBar.ProgressPanel:=TRUE;
    try
      StatusBar.ProgressText:=GetLangText(4012);
      StatusBar.AbortText:=GetLangText(3803);
      Entries:=DDEHandler.DDEData^.GetCount;
      Cnt:=0;
      ErrorCode:=0;
      Moved:=FALSE;
      Redraw.Init;
      AData^.ChangeRedraw(CNoRedraw);
      DDEHandler.DDEData^.ForEach(@MoveObjs);
  {!?!?!    Redraw.Grow(AData^.PInfo^.CalculateDraw(AData^.Layers^.Toplayer^.LineStyle shr 8));}
      AData^.ChangeRedraw(COldRedraw);
      DDEHandler.DeleteDDEData;
      Dispose(SItem,Done);
      AData^.SetCursor(crDefault);
    finally
      StatusBar.ProgressPanel:=FALSE;
    end;
    if Moved then begin
      if not AData^.CorrectSize(Redraw,TRUE) then AData^.PInfo^.RedrawRect(Redraw);
      AData^.SetModified;
    end
    else AData^.PInfo^.RedrawAfterNoRedraw(TMDIChild(AData^.Parent).RDRArea);
  end;

Procedure ProcMoveAbsFromDB
   (
   AData    : PProj
   );
  var SItem        : PIndex;
      Entries      : Integer;
      Cnt          : Integer;
      ErrorCode    : Byte;
      Redraw       : TDRect;
      ResLInt      : LongInt;
      ResStr       : String;
      MoveOnAllLay : Boolean;
      MoveLayer    : PLayer;
      Moved        : Boolean;
  Procedure MoveObjs
     (
     Item    : PChar
     ); Far;
    var TempPointer  : PChar;
        TempVal      : String;
        ItemInd      : Longint;
        Error        : Integer;
        OldTop       : PLayer;
        ExistObject  : PIndex;
        i            : Integer;
        RealMove     : Real;
        XPos         : LongInt;
        YPos         : LongInt;
        XMove        : LongInt;
        YMove        : LongInt;
        DoIt         : Boolean;
        AView        : PView;
        SFound       : Boolean;
        MStrs        : Boolean;
        StrToLong    : Boolean;
        MoreStrs     : Boolean;
        StrOK        : Boolean;
    begin
      if DDEHandler.FGotLInfo = 0 then MoveOnAllLay:=TRUE
      else if DDEHandler.FGotLInfo = 1 then begin
        StrOK:=ReadDDECommandString(Item,1,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ResStr,StrToLong,MoreStrs);
        if StrOK and (ResStr <> '') then begin                                                                  {Layername}
          MoveLayer:=AData^.Layers^.NameToLayer(ResStr);
          if MoveLayer = NIL then MoveOnAllLay:=TRUE
          else MoveOnAllLay:=FALSE;
        end
        else MoveOnAllLay:=TRUE;
      end
      else if DDEHandler.FGotLInfo = 2 then begin
        if ReadDDECommandLongInt(Item,1,0,WgBlockStart,WgBlockEnd,                                 {Layerindex}
                                             WgBlockEnd,ResLInt,SFound,MStrs) then begin
          MoveLayer:=AData^.Layers^.IndexToLayer(ResLInt);
          if MoveLayer = NIL then MoveOnAllLay:=TRUE
          else MoveOnAllLay:=FALSE;
        end
        else MoveOnAllLay:=TRUE;
      end;
      TempPointer:=NextDDEPart(NextDDEPart(Item));
      if DDEHandler.FGotLInfo <> 0 then TempPointer:=NextDDEPart(TempPointer);
      ReadDDEString(TempPointer,']',TempVal);
      Val(TempVal,ItemInd,Error);
      SItem^.Index:=ItemInd;
      OldTop:=AData^.Layers^.TopLayer;
      if not MoveOnAllLay then AData^.Layers^.TopLayer:=MoveLayer;
      ExistObject:=AData^.Layers^.TopLayer^.HasObject(SItem);
      if (ExistObject = NIL) and MoveOnAllLay then begin                                 {>>>>>}
        i:=0;
        while (ExistObject = NIL) and (i < AData^.Layers^.LData^.Count) do begin         { Diese Zeilen ausklammern, }
          AData^.Layers^.TopLayer:=AData^.Layers^.LData^.At(i);                          { wenn Objekte nur am obersten }
          ExistObject:=AData^.Layers^.TopLayer^.HasObject(SItem);                        { Layer verschoben werden sollen }
          i:=i+1;
        end;
      end;                                                                               {<<<<<}
      if ExistObject <> NIL then begin
        ReadDDEString(TempPointer,DDEHandler.FDDESepSign,TempVal);
        Val(TempVal,RealMove,Error);
        if RealMove*100 < MaxLongInt then XPos:=Round(RealMove*100)
        else XPos:=MaxLongInt;
        ReadDDEString(TempPointer,']',TempVal);
        Val(TempVal,RealMove,Error);
        if RealMove*100 < MaxLongInt then YPos:=Round(RealMove*100)
        else YPos:=MaxLongInt;
        AView:=AData^.Layers^.IndexObject(AData^.PInfo,ExistObject);
        case ExistObject^.GetObjType of
          ot_Pixel     : begin
                           XMove:=XPos-PPixel(AView)^.Position.X;
                           YMove:=YPos-PPixel(AView)^.Position.Y;
                         end;
          ot_Poly      : begin
                           XMove:=XPos-PDPoint(PPoly(AView)^.Data^.At(0))^.X;
                           YMove:=YPos-PDPoint(PPoly(AView)^.Data^.At(0))^.Y;
                         end;
          ot_CPoly     : begin
                           XMove:=XPos-PDPoint(PCPoly(AView)^.Data^.At(0))^.X;
                           YMove:=YPos-PDPoint(PCPoly(AView)^.Data^.At(0))^.Y;
                         end;
          ot_Group     : begin
                           XMove:=0;
                           YMove:=0;
                         end;
          ot_Proj      : begin
                           XMove:=0;
                           YMove:=0;
                         end;
          ot_Text      : begin
                           XMove:=XPos-PText(AView)^.Pos.X;
                           YMove:=YPos-PText(AView)^.Pos.Y;
                         end;
          ot_Symbol    : begin
                           XMove:=XPos-PSymbol(AView)^.Position.X;
                           YMove:=YPos-PSymbol(AView)^.Position.Y;
                         end;
          ot_Spline    : begin
                           XMove:=XPos-PDPoint(PSpline(AView)^.Data^.At(0))^.X;
                           YMove:=YPos-PDPoint(PSpline(AView)^.Data^.At(0))^.Y;
                         end;
          ot_Image     : begin
                           XMove:=XPos-AView^.ClipRect.A.X;
                           YMove:=YPos-AView^.ClipRect.A.Y;
                         end;
          ot_MesLine   : begin
                           XMove:=XPos-PMeasureLine(AView)^.StartPoint.X;
                           YMove:=YPos-PMeasureLine(AView)^.StartPoint.Y;
                         end;
          ot_Circle    : begin
                           XMove:=XPos-PEllipse(AView)^.Position.X;
                           YMove:=YPos-PEllipse(AView)^.Position.Y;
                         end;
          ot_Arc       : begin
                           XMove:=XPos-PEllipseArc(AView)^.Position.X;
                           YMove:=YPos-PEllipseArc(AView)^.Position.Y;
                         end;
          ot_OLEObj    : begin
                           XMove:=XPos-POLEClientObject(AView)^.ClipRect.A.X;
                           YMove:=YPos-POLEClientObject(AView)^.ClipRect.A.Y;
                         end;
          ot_RText     : begin
                           XMove:=XPos-PRText(AView)^.TextRect.A.X;
                           YMove:=YPos-PRText(AView)^.TextRect.A.Y;
                         end;
          ot_BusGraph  : begin
                           if PBusGraph(AView)^.DiagType = dtCircleDiag then begin
                             XMove:=XPos-PBusGraph(AView)^.Position.X-Trunc(PBusGraph(AView)^.CDiameter/2);
                             YMove:=YPos-PBusGraph(AView)^.Position.Y+Trunc(PBusGraph(AView)^.CDiameter/2);
                           end
                           else begin
                             XMove:=XPos-PBusGraph(AView)^.Position.X;
                             YMove:=YPos-PBusGraph(AView)^.Position.Y+PBusGraph(AView)^.GesHeight;
                           end;
                         end;
          ot_Layer     : begin
                           XMove:=0;
                           YMove:=0;
                         end;
        end;
        if not AData^.Layers^.TopLayer^.GetState(sf_Fixed) then DoIt:=TRUE
        else begin
          DoIt:=FALSE;
          ErrorCode:=ErrorCode or 1;
        end;
        if DoIt then begin
          if AData^.Layers^.TopLayer^.CheckMove(AData^.PInfo,XMove,YMove) then begin
            if not AData^.Layers^.IsObjectFixed(SItem) then begin
              Redraw.CorrectByRect(AView^.ClipRect);
              AView^.MoveRel(XMove,YMove);
              AData^.UpdateCliprect(AView);
              Redraw.CorrectByRect(AView^.ClipRect);
              Moved:=TRUE;
            end;
          end
          else MsgBox(AData^.Parent.Handle,4100,mb_OK or mb_IconStop,'');
        end;
        if AData^.Layers^.SelLayer^.HasObject(AView) <> NIL then AData^.SelAllRect.CorrectByRect(AView^.ClipRect);
      end
      else begin
        if MoveOnAllLay then ErrorCode:=ErrorCode or 2
        else ErrorCode:=ErrorCode or 4;
      end;
      Inc(Cnt);
      StatusBar.Progress:=100*Cnt/Entries;
      if StatusBar.QueryAbort then Exit;
      AData^.Layers^.TopLayer:=OldTop;
      Item[0]:=DDEStrUsed;
    end;
  begin
    AData^.SetCursor(crHourGlass);
    SItem:=New(PIndex,Init(0));
    StatusBar.ProgressPanel:=TRUE;
    try
      StatusBar.ProgressText:=GetLangText(4012);
      StatusBar.AbortText:=GetLangText(3803);
      Entries:=DDEHandler.DDEData^.GetCount;
      Cnt:=0;
      ErrorCode:=0;
      Moved:=FALSE;
      Redraw.Init;
      AData^.ChangeRedraw(CNoRedraw);
      DDEHandler.DDEData^.ForEach(@MoveObjs);
  {!?!?!?    Redraw.Grow(AData^.PInfo^.CalculateDraw(AData^.Layers^.Toplayer^.LineStyle shr 8));}
      AData^.ChangeRedraw(COldRedraw);
      DDEHandler.DeleteDDEData;
      Dispose(SItem,Done);
      AData^.SetCursor(crDefault);
    finally
      StatusBar.ProgressPanel:=FALSE;
    end;
    if Moved then begin
      if not AData^.CorrectSize(Redraw,TRUE) then AData^.PInfo^.RedrawRect(Redraw);
      AData^.SetModified;
    end
    else AData^.PInfo^.RedrawAfterNoRedraw(TMDIChild(AData^.Parent).RDRArea);
  end;

Procedure ProcDBSendObject
   (
   AData           : PProj;
   Index           : PIndex;
   AText           : String
   );
  var Error        : Integer;
      WorkStr      : String;
      ObjSize      : Real;
      IObject      : PIndex;
      AExtLib      : PExtLib;
      PointCnt     : Integer;
      BText        : String;
      Last         : Integer;
      X,Y          : Integer;
      APoint       : TDPoint;
      i            : Integer;
      AView        : PView;
      AGUID        : TGUID;
  begin
    APoint.Init(0,0);
    AData^.SetCursor(crHourGlass);
    BText:='';
    Str(Index^.Index:10,WorkStr);
    AText:='['+AText+']['+WorkStr+']';
    IObject:=AData^.Layers^.IndexObject(AData^.PInfo,Index);
    if (IObject^.GetObjType=ot_Pixel) or (IObject^.GetObjType=ot_Symbol)
        or (IObject^.GetObjType=ot_Text) or (IObject^.GetObjType=ot_Image) then begin
      if IObject^.GetObjType=ot_Symbol then
          AText:=AText+ '[              0][' + FloatToStr(PSymbol(IObject)^.Size  ) + ']'
      else
          AText:=AText+'[              0][              0]';
      if IObject^.GetObjType=ot_Pixel then ObjSize:=PPixel(IObject)^.GivePosX/100
      else if IObject^.GetObjType=ot_Symbol then ObjSize:=PSymbol(IObject)^.GivePosX/100
      else if IObject^.GetObjType=ot_Text then ObjSize:=PText(IObject)^.GivePosX/100
      else if IObject^.GetObjType=ot_Image then ObjSize:=(PImage(IObject)^.ClipRect.A.X)/100;
      Str(ObjSize:15:2,WorkStr);
      AText:=AText+'['+WorkStr+']';
      if IObject^.GetObjType=ot_Pixel then ObjSize:=PPixel(IObject)^.GivePosY/100
      else if IObject^.GetObjType=ot_Symbol then ObjSize:=PSymbol(IObject)^.GivePosY/100
      else if IObject^.GetObjType=ot_Text then ObjSize:=PText(IObject)^.GivePosY/100
      else if IObject^.GetObjType=ot_Image then ObjSize:=(PImage(IObject)^.ClipRect.A.Y)/100;
      Str(ObjSize:15:2,WorkStr);
      AText:=AText+'['+WorkStr+']';
    end
    else if (IObject^.GetObjType=ot_Poly) or (IObject^.GetObjType=ot_Spline) then begin
      Last:=PPoly(IObject)^.Data^.Count-1;
      AText:=AText+'[              0]';
      if IObject^.GetObjType=ot_Poly then ObjSize:=PCPoly(IObject)^.Laenge
      else ObjSize:=PSpline(IObject)^.Laenge;
      Str(ObjSize:15:2,WorkStr);
      AText:=AText+'['+WorkStr+']';
      ObjSize:=(PDPoint(PPoly(IObject)^.Data^.At(Last))^.x)/100;
      Str(ObjSize:15:2,WorkStr);
      AText:=AText+'['+WorkStr+']';
      ObjSize:=(PDPoint(PPoly(IObject)^.Data^.At(Last))^.y)/100;
      Str(ObjSize:15:2,WorkStr);
      AText:=AText+'['+WorkStr+']';
      ObjSize:=(PDPoint(PPoly(IObject)^.Data^.At(0))^.x)/100;
      Str(ObjSize:15:2,WorkStr);
      BText:=BText+'['+WorkStr+']';
      ObjSize:=(PDPoint(PPoly(IObject)^.Data^.At(0))^.y)/100;;
      Str(ObjSize:15:2,WorkStr);
      BText:=BText+'['+WorkStr+']';
    end
    else if IObject^.GetObjType=ot_CPoly then begin
      ObjSize:=PCPoly(IObject)^.Flaeche;
      if IniFile^.AreaFact > 1 then begin
         Str(ObjSize:15:4,WorkStr);
      end
      else begin
         Str(ObjSize:15:2,WorkStr);
      end;
      AText:=AText+'['+WorkStr+']';
      ObjSize:=PCPoly(IObject)^.Laenge;
      Str(ObjSize:15:2,WorkStr);
      AText:=AText+'['+WorkStr+']';
      APoint:=PCPoly(IObject)^.GetCenter;
      ObjSize:=APoint.X/100;
      Str(ObjSize:15:2,WorkStr);
      AText:=AText+'['+WorkStr+']';
      ObjSize:=APoint.Y/100;
      Str(ObjSize:15:2,WorkStr);
      AText:=AText+'['+WorkStr+']';
    end
    else if (IObject^.GetObjType=ot_Circle) or (IObject^.GetObjType=ot_Arc) then begin
      if IObject^.GetObjType=ot_Circle then begin
        ObjSize:=PEllipse(IObject)^.Flaeche;
        Str(ObjSize:15:2,WorkStr);
      end
      else WorkStr:='              0';
      AText:=AText+'['+WorkStr+']';
      if IObject^.GetObjType=ot_Circle then ObjSize:=PEllipse(IObject)^.Laenge
      else ObjSize:=PEllipseArc(IObject)^.Laenge;
      Str(ObjSize:15:2,WorkStr);
      AText:=AText+'['+WorkStr+']';
      if IObject^.GetObjType=ot_Circle then ObjSize:=PEllipse(IObject)^.GivePosX/100
      else ObjSize:=PEllipseArc(IObject)^.GivePosX/100;
      Str(ObjSize:15:2,WorkStr);
      AText:=AText+'['+WorkStr+']';
      if IObject^.GetObjType=ot_Circle then ObjSize:=PEllipse(IObject)^.GivePosY/100
      else ObjSize:=PEllipseArc(IObject)^.GivePosY/100;
      Str(ObjSize:15:2,WorkStr);
      AText:=AText+'['+WorkStr+']';
    end
    else AText:=AText+'[              0][              0][              0][              0]';
    if (DDEHandler.FLayerInfo = 0) or (DDEHandler.FLayerInfo = 1) then AText:=AText+'['+AData^.Layers^.TopLayer^.Text^+']'
    else if DDEHandler.FLayerInfo = 2 then begin
      Str(AData^.Layers^.TopLayer^.Index:10,WorkStr);
      AText:=AText+'['+WorkStr+']';
    end;
    if IObject^.GetObjType = ot_Symbol then begin
      Str((PSymbol(IObject)^.SymIndex-600000000):10,WorkStr);
      AText:=AText+'['+WorkStr+']';
      if PSymbol(IObject)^.SymPtr <> nil then begin
         AText:=AText+'['+PSymbol(IObject)^.SymPtr^.Name^+']';
         if PSymbol(IObject)^.SymPtr^.LibName <> NIL then begin
            AExtLib:=PSymbols(AData^.PInfo^.Symbols)^.GetExtLib(PSymbol(IObject)^.SymPtr^.LibName^);
            if AExtLib <> NIL then AText:=AText+'['+AnsiLowerCase(AExtLib^.LibFileName^)+']'
            else AText:=AText+'['+AnsiLowerCase(PSymbol(IObject)^.SymPtr^.LibName^)+']';
         end
         else AText:=AText+'[]';
      end
      else AText:=AText+'[]';
    end;
    if (IObject^.GetObjType=ot_Circle) or (IObject^.GetObjType=ot_Arc) then begin
      Str(PEllipse(IObject)^.PrimaryAxis/100:15:2,WorkStr);
      AText:=AText+'['+WorkStr+']';
      if IObject^.GetObjType=ot_Arc then begin
        Str((PEllipseArc(IObject)^.BeginAngle*360/2/Pi):15:2,WorkStr);
        AText:=AText+'['+WorkStr+']';
        Str((PEllipseArc(IObject)^.EndAngle*360/2/Pi):15:2,WorkStr);
        AText:=AText+'['+WorkStr+']';
      end;
    end;
    if IObject^.GetObjType = ot_Text then begin
      AText:=AText+'['+PText(IObject)^.Text^+']';
    end;
    if IObject^.GetObjType = ot_Image then begin
      AText:=AText+'['+FloatToStr(IObject^.ClipRect.B.X/100)+']';
      AText:=AText+'['+FloatToStr(IObject^.ClipRect.B.Y/100)+']';
      AText:=AText+'['+PImage(IObject)^.FileName^+']';
    end;
    AText:=AText+BText;
    if AData^.Construct^.SendUPD then DDEHandler.SendString(AText);
    AData^.SetCursor(crDefault);
    if WinGisMainForm.WeAreUsingTheIDB[AData] then begin
        AView:=PLayer(AData^.PInfo^.Objects)^.IndexObject(AData^.PInfo,IObject);
        if AView <> nil then begin
           AGUID:=AView^.GUID;
        end;
        if AData^.Layers^.TopLayer^.IndexObject(AData^.PInfo,IObject) <> nil then begin
           WinGisMainForm.IDB_Man.InsertItem (AData, AData^.Layers.TopLayer,IObject);
        end
        else begin
             for i:=1 to AData^.Layers^.LData^.Count-1 do begin
                 if PLayer(AData^.Layers^.LData^.At(i))^.IndexObject(AData^.PInfo,IObject) <> nil then begin
                    WinGisMainForm.IDB_Man.InsertItem (AData,PLayer(AData^.Layers^.LData^.At(i)),IObject);
                 end;
             end;
        end;
    end;
  end;

Procedure ProcDBObjectInfos
   (
   AData           : PProj
   );
  Procedure DoAll
     (
     Item          : PIndex
     ); Far;
    begin
      ProcDBSendObject(AData,Item,'UPD');
    end;
  begin
    AData^.SetCursor(crHourGlass);
    AData^.SelList^.ForEach(@DoAll);
    AData^.SetCursor(crDefault);
  end;

Procedure ProcDBMultiInsert
   (
   AData           : PProj
   );
  Procedure DoAll
     (
     Item          : PIndex
     ); Far;
    begin
      ProcDBSendObject(AData,Item,'AIN');
    end;
  begin
    DDEHandler.SetDBSendStrings(DDECommMIns,NIL);
    AData^.Layers^.SelLayer^.Data^.ForEach(@DoAll);
//!?!?!TV    if AData^.Document.Loaded then ProcDBSendObjectMulti(AData) else
    DDEHandler.SendString('[END][]');
    DDEHandler.SetDBSendStrings(DDECommAll,NIL);
  end;
{$ENDIF} // <----------------- AXDLL

Procedure ProcSelDeselIndex
   (
   AData           : PProj;
   AItem           : PIndex
   );
  var AIndex       : PIndex;
  begin
    AIndex:=AData^.Layers^.TopLayer^.HasObject(AItem);
    if AIndex<>NIL then begin
      if AIndex^.GetState(sf_Selected) then AData^.Layers^.TopLayer^.DeSelect(AData^.PInfo,AIndex)
      else AData^.Layers^.TopLayer^.Select(AData^.PInfo,AIndex);
    end;
  end;

//++ Glukhov Bug#387 Build#152 17.04.01
// Selects/DeSelects the item on the upper layer which it belongs to (but off)
Procedure ProcSelDeselTranspIndex( AData: PProj; AItem: PIndex );
  var
    Idx       : PIndex;
    Layer     : PLayer;
    i         : Integer;
  begin
    Idx:= AData.Layers.TopLayer.HasObject( AItem );
    if Assigned( Idx ) then begin
      if Idx^.GetState( sf_Selected )
      then  AData.Layers.TopLayer.DeSelect( AData^.PInfo, Idx )
      else  AData.Layers.TopLayer.Select( AData.PInfo, Idx );
      Exit;
    end;
    for i:=1 to AData.Layers.LData.Count-1 do begin
      Layer:= AData.Layers.LData.At(i);
      if not Layer^.GetState( sf_LayerOff ) then begin
        Idx:= Layer.HasObject( AItem );
        if Assigned( Idx ) then begin
          if Idx^.GetState( sf_Selected )
          then  Layer.DeSelect( AData.PInfo, Idx )
          else  Layer.Select( AData.PInfo, Idx );
          Exit;
        end;
      end;
    end;
  end;

// Selects the item on the upper layer which it belongs to (but off)
// (The selected item remains the selected one)
Procedure SelectIndexOnAllLayers( AData: PProj; AItem: PIndex );
  var
    Idx       : PIndex;
    Layer     : PLayer;
    i         : Integer;
  begin
    Idx:= AData.Layers.TopLayer.HasObject( AItem );
    if Assigned( Idx ) then begin
      if not Idx^.GetState( sf_Selected ) then  begin
         AData.Layers.TopLayer.Select( AData.PInfo, Idx );
         Exit;
      end;
    end;

    for i:=1 to AData.Layers.LData.Count-1 do begin
      Layer:= AData.Layers.LData.At(i);
      if not Layer^.GetState( sf_LayerOff ) then begin
        Idx:= Layer.HasObject( AItem );
        if Assigned( Idx ) then begin
          if not Idx^.GetState( sf_Selected ) then begin
             Layer.Select( AData.PInfo, Idx );
          end;
        end;
      end;
    end;
  end;

// DeSelects the item on all the layers which it belongs to (but off)
// (The unselected item remains the unselected one)
Procedure DeSelectIndexOnAllLayers( AData: PProj; AItem: PIndex );
  var
    Idx       : PIndex;
    Layer     : PLayer;
    i         : Integer;
  begin
    for i:=1 to AData.Layers.LData.Count-1 do begin
      Layer:= AData.Layers.LData.At(i);
      if not Layer^.GetState( sf_LayerOff ) then begin
        Idx:= Layer.HasObject( AItem );
        if Assigned( Idx ) then begin
          if Idx^.GetState( sf_Selected )then begin
             Layer.DeSelect( AData.PInfo, Idx );
          end;
        end;
      end;
    end;
  end;

// Checks if the selected objects are on the top layer
Function SelectionOnTopLayer( AData: PProj ): Boolean;
  var
    Idx       : PIndex;
    i         : Integer;
  begin
    Result:= False;
    for i:=0 to AData.SelList.GetCount-1 do begin
      Idx:= AData.Layers.TopLayer.HasObject( AData.SelList.At( i ) );
      if not Assigned( Idx ) then Exit;
    end;
    Result:= True;
  end;
//-- Glukhov Bug#387 Build#152 17.04.01

{$IFNDEF AXDLL} // <----------------- AXDLL
Procedure ProcDBSendViews
   (
   AData           : PProj
   );
  Procedure DoAll
     (
     Item          : PSight
     ); Far;
    begin
      DDEHandler.AppendString(Item^.Name^);
    end;
  begin
    AData^.SetCursor(crHourGlass);
    DDEHandler.StartList('[VEW][   ]');
    AData^.Sights^.ForEach(@DoAll);
    DDEHandler.EndList(TRUE);
    AData^.SetCursor(crDefault);
  end;

Procedure ProcDBAddView
   (
   AName           : String
   );
  begin
    DDEHandler.StartList('[AVW][   ]');
    DDEHandler.AppendString(AName);
    DDEHandler.EndList(TRUE);
  end;

Procedure ProcSendSightInfosToDB
   (
   AData           : PProj
   );
  var SightCount   : Integer;
      RequestName  : String;
      SendLInfo    : Boolean;
      TmpPointer   : PChar;
      TmpString    : Array[0..255] of Char;
      RequestSight : PSight;
  Procedure MakeSightInfo
     (
     SItem         : PSight
     ); Far;
    var LayerPos   : Integer;
        TempStr    : String;
        TempDouble : Double;
        AColl      : PCollection;
        Cnt        : Integer;
        ALayer     : PLayer;
        Swap       : Pointer;
    Procedure MakeLayerInfo
       (
       Item          : PLayer;
       StateOff      : Boolean;
       StateGen      : Boolean;
       GenMin        : Double;
       GenMax        : Double
       );
      var TempStr    : String;
          TmpDouble  : Double;
          TmpInt     : Integer;
      begin
        if Item^.Index <> 0 then begin
          DDEHandler.Reset;
          DDEHandler.Append('[LVD][');
          Str(Item^.Index:10,TempStr);
          DDEHandler.Append(TempStr+DDEHandler.FOutSepSign);
          DDEHandler.Append(Item^.Text^);          {kann 255 Zeichen lang sein}
          DDEHandler.Append(DDEHandler.FOutSepSign);
          Inc(LayerPos);
          Str(LayerPos:0,TempStr);
          DDEHandler.Append(TempStr+DDEHandler.FOutSepSign);
          if StateOff then DDEHandler.Append('0'+DDEHandler.FOutSepSign)
          else DDEHandler.Append('1'+DDEHandler.FOutSepSign);
          if StateGen then DDEHandler.Append('1'+DDEHandler.FOutSepSign)
          else DDEHandler.Append('0'+DDEHandler.FOutSepSign);
          TmpDouble:=GenMin/100;
          Str(TmpDouble:0:2,TempStr);
          DDEHandler.Append(TempStr+DDEHandler.FOutSepSign);
          TmpDouble:=GenMax/100;
          Str(TmpDouble:0:2,TempStr);
          DDEHandler.Append(TempStr+DDEHandler.FOutSepSign);
          if Item^.GetState(sf_Fixed) then DDEHandler.Append('1'+DDEHandler.FOutSepSign)
          else DDEHandler.Append('0'+DDEHandler.FOutSepSign);
          TmpInt:=Item^.LineStyle.Style and $00FF;
          Str(TmpInt:0,TempStr);
          DDEHandler.Append(TempStr+DDEHandler.FOutSepSign);
          if TmpInt = 0 then begin
            TmpInt:=HiByte(Item^.LineStyle.Style and $FF00);
            Str(TmpInt:0,TempStr);
            DDEHandler.Append(TempStr+DDEHandler.FOutSepSign);
          end
          else DDEHandler.Append('0'+DDEHandler.FOutSepSign);
          Str(Item^.LineStyle.Color:0,TempStr);
          DDEHandler.Append(TempStr+DDEHandler.FOutSepSign);
          Str(Item^.FillStyle.Pattern:0,TempStr);
          DDEHandler.Append(TempStr+DDEHandler.FOutSepSign);
          Str(Item^.FillStyle.ForeColor:0,TempStr);
          DDEHandler.Append(TempStr+DDEHandler.FOutSepSign);
          if Item^.GetState(sf_Transparent) then DDEHandler.Append('1]')
          else DDEHandler.Append('0]');
          DDEHandler.Append(#0);
          DDEHandler.Execute;
        end;
      end;
      Function CheckViewLayerList
         (
         Item      : PLayer
         )
         : Boolean; Far;
        begin
          Result:=Item = AData^.Layers^.LData^.At(Cnt);
        end;
    begin
      LayerPos:=0;
      DDEHandler.Reset;
      DDEHandler.Append('[LVN][');
      DDEHandler.Append(SItem^.Name^+DDEHandler.FOutSepSign);
      if SightCount = AData^.Sights^.Default then DDEHandler.Append('1'+DDEHandler.FOutSepSign)
      else DDEHandler.Append('0'+DDEHandler.FOutSepSign);
      if SItem^.LayersData <> NIL then DDEHandler.Append('1'+DDEHandler.FOutSepSign)
      else DDEHandler.Append('0'+DDEHandler.FOutSepSign);
{!?!?!Rectangle now is a RotRect   if not SItem^.Rect.IsEmpty then DDEHandler.Append('1,')
      else DDEHandler.Append('0,');
      TempDouble:=SItem^.Rect.A.X/100;
      Str(TempDouble:0:2,TempStr);
      DDEHandler.Append(TempStr+DDEHandler.FOutSepSign);
      TempDouble:=SItem^.Rect.A.Y/100;
      Str(TempDouble:0:2,TempStr);
      DDEHandler.Append(TempStr+DDEHandler.FOutSepSign);
      TempDouble:=SItem^.Rect.B.X/100;
      Str(TempDouble:0:2,TempStr);
      DDEHandler.Append(TempStr+DDEHandler.FOutSepSign);
      TempDouble:=SItem^.Rect.B.Y/100;}
      Str(TempDouble:0:2,TempStr);
      DDEHandler.Append(TempStr+']');
      DDEHandler.Append(#0);
      DDEHandler.Execute;
      if SendLInfo then begin
        if SItem^.LayersData <> NIL then begin
          AColl:=New(PCollection,Init(10,10));
          for Cnt:=0 to SItem^.LayersData^.Count-1 do begin
            ALayer:=AData^.Layers^.IndexToLayer(PSightLData(SItem^.LayersData^.At(Cnt))^.LIndex);
            if ALayer <> NIL then begin
              MakeLayerInfo(ALayer,PSightLData(SItem^.LayersData^.At(Cnt))^.LFlags and lf_LayerOff <> 0,
                                   PSightLData(SItem^.LayersData^.At(Cnt))^.LFlags and lf_GenerOn <> 0,
                                   PSightLData(SItem^.LayersData^.At(Cnt))^.GeneralMin,
                                   PSightLData(SItem^.LayersData^.At(Cnt))^.GeneralMax);
              AColl^.Insert(ALayer);
            end;
          end;
          for Cnt:=0 to AData^.Layers^.LData^.Count-1 do begin
            ALayer:=AColl^.FirstThat(@CheckViewLayerList);
{?!?!            if ALayer = NIL then MakeLayerInfo(AData^.Layers^.LData^.At(Cnt),TRUE,
                                               PLayer(AData^.Layers^.LData^.At(Cnt))^.GetState(sf_General),
                                               PLayer(AData^.Layers^.LData^.At(Cnt))^.GeneralMin,
                                               PLayer(AData^.Layers^.LData^.At(Cnt))^.GeneralMax);}
          end;
          AColl^.DeleteAll;
          Dispose(AColl,Done);
        end;
        DDEHandler.Reset;
        DDEHandler.Append('[END][]'+#0);
        DDEHandler.Execute;
      end;
      Inc(SightCount);
    end;
  Function FindSight
     (
     Item          : PSight
     )
     : Boolean; Far;
    begin
      if Item^.Name^ = RequestName then Result:=TRUE
      else Result:=FALSE;
    end;
  begin
    TmpPointer:=NextDDEPart(NextDDEPart(DDEHandler.DDEData^.At(0)));
    PChar(DDEHandler.DDEData^.At(0))[0]:=DDEStrUsed;
    ReadDDEText(TmpPointer,DDEHandler.FOutSepSign,TmpString);
    RequestName:=StrPas(TmpString);
    ReadDDEText(TmpPointer,DDEHandler.FOutSepSign,TmpString);
    if StrComp(TmpString,'0') = 0 then SendLInfo:=FALSE
    else SendLInfo:=TRUE;
    SightCount:=0;
    DDEHandler.GetGlobal(DDELInfoSize);
    if RequestName = '' then AData^.Sights^.ForEach(@MakeSightInfo)
    else begin
      RequestSight:=AData^.Sights^.FirstThat(@FindSight);
      if RequestSight <> NIL then MakeSightInfo(RequestSight);
    end;
    DDEHandler.FreeGlobal;
    DDEHandler.SendString('[LVE][]');
    DDEHandler.SetAnswerMode(FALSE);
    DDEHandler.DeleteDDEData;
  end;

{$ENDIF} // <----------------- AXDLL
{$ENDIF}

Procedure ProcSetSight
   (
   AData           : PProj;
   ASight          : PSight;
   UpdateView      : Boolean
   );
  var AColl        : PCollection;
      SColl        : PCollection;
      Cnt          : Integer;
      ALayer       : PLayer;
      Swap         : Pointer;
      ToRedraw     : Boolean;
      OldTopLayer  : PLayer;
      NewOVLName   : String;
      LoadBmpNew   : Boolean;
      SLPOK        : Boolean;
      Rect         : TDRect;
      i            : Integer;
  Function CheckSightName
     : Boolean;
    var OData      : POverviewData;
    Function DoAll
       (
       Item        : POverviewData
       ) : Boolean; Far;
      begin
        Result:=StrPas(Item^.ViewName) = ASight^.Name^;
      end;
    begin
      OData:=IniFile^.GVLCModeStruct^.FirstThat(@DoAll);
      NewOVLName:=StrPas(OData^.LayerName);
      Result:=NewOVLName <> IniFile^.GeneralViewLayer;
    end;
  Procedure UnloadOldOverviewLayer;
//++ Glukhov Bug#236 Build#144 25.12.00
{
    Procedure SearchOldLayer
       (
       Item        : PLayer
       ); Far;
      begin
        if Item^.Text <> NIL then
          if Item^.Text^ = IniFile^.GeneralViewLayer then Item^.UnLoadBmps(AData);
      end;
}
    begin
//      AData^.Layers^.LData^.ForEach(@SearchOldLayer);
    end;
//-- Glukhov Bug#236 Build#144 25.12.00
  Procedure LoadNewOverviewLayer;
    var CRect      : TDRect;
        SRect      : TRect;
//++ Glukhov Bug#236 Build#144 25.12.00
{
    Procedure SearchNewLayer
       (
       Item        : PLayer
       ); Far;
      begin
        if Item^.Text <> NIL then
          if Item^.Text^ = IniFile^.GeneralViewLayer then Item^.LoadBmps(AData,CRect,SRect,AData^.PInfo^.Colors256);
      end;
}
    begin
      AData^.PInfo^.GetCurrentScreen(CRect);
      SRect.Left:=0;
      SRect.Right:=AData^.PInfo^.ScreenWidth;
      SRect.Bottom:=AData^.PInfo^.ScreenHeight;
      SRect.Top:=0;
//      AData^.Layers^.LData^.ForEach(@SearchNewLayer);
      CRect.Done;
    end;
//-- Glukhov Bug#236 Build#144 25.12.00
  Function CountLayerWithStateOn
     : LongInt;
    var CountOn    : LongInt;
    Procedure DoAll
       (
       Item        : PLayer
       ); Far;
      begin
        if Item^.Index <> 0 then if not Item^.GetState(sf_LayerOff) then Inc(CountOn);
      end;
    begin
      CountOn:=0;
      AData^.Layers^.LData^.ForEach(@DoAll);
      Result:=CountOn;
    end;
  Procedure SaveLData
     (
     Item          : PLayer
     ); Far;
    var SaveData   : PLSaveData;
    begin
      SColl^.Insert(New(PLSaveData,Init(Item^.Index,Item^.GetState(sf_LayerOff),FALSE,
                                        Item^.GeneralMin,Item^.GeneralMax)));
    end;
  Procedure SetLData
     (
     Item          : PLSaveData
     ); Far;
    var SetLayer   : PLayer;
        LIndex     : LongInt;
        LState     : Boolean;
        Gener      : Double;
    begin
      SetLayer:=AData^.Layers^.IndexToLayer(Item^.LayerIndex);
      if Item^.LayerOff then SetLayer^.SetState(sf_LayerOff,TRUE)
      else SetLayer^.SetState(sf_LayerOff,FALSE);
      SetLayer^.GeneralMin:=Item^.GeneralMin;
      SetLayer^.GeneralMax:=Item^.GeneralMax;
    end;
  begin
    if ASight<>NIL then begin
      ToRedraw:=FALSE;
      LoadBmpNew:=FALSE;
      if IniFile^.GVLChangeMode then begin
        if CheckSightName then begin
          UnloadOldOverviewLayer;
          IniFile^.GeneralViewLayer:=NewOVLName;
          ToRedraw:=TRUE;
          LoadBmpNew:=TRUE;
        end;
      end;
      if ASight^.LayersData <> NIL then begin
        SLPOK:=TRUE;
        SColl:=New(PCollection,Init(10,10));
        AData^.Layers^.LData^.ForEach(@SaveLData);
        AColl:=New(PCollection,Init(10,10));
        for Cnt:=0 to ASight^.LayersData^.Count-1 do begin
          ALayer:=AData^.Layers^.IndexToLayer(PSightLData(ASight^.LayersData^.At(Cnt))^.LIndex);
          if ALayer <> NIL then begin
            AColl^.Insert(ALayer);
            ALayer^.SetState(sf_LayerOff,(PSightLData(ASight^.LayersData^.At(Cnt))^.LFlags and lf_LayerOff <> 0));
            ALayer^.SetState(sf_HSDVisible,PSightLData(ASight^.LayersData^.At(Cnt))^.LFlags and lf_HSDVisible <> 0);
            ALayer^.GeneralMin:=PSightLData(ASight^.LayersData^.At(Cnt))^.GeneralMin;
            ALayer^.GeneralMax:=PSightLData(ASight^.LayersData^.At(Cnt))^.GeneralMax;
          end;
        end;
        Swap:=AData^.Layers^.LData;
        AData^.Layers^.LData:=AColl;
        AColl:=Swap;
        for Cnt:=0 to AColl^.Count-1 do begin
          ALayer:=AData^.Layers^.IndexToLayer(PLayer(AColl^.At(Cnt))^.Index);
          if ALayer = NIL then begin
            AData^.Layers^.LData^.Insert(AColl^.At(Cnt));
            PLayer(AColl^.At(Cnt))^.SetState(sf_LayerOff,True);
          end;
        end;
        if CountLayerWithStateOn = 0 then begin
          AData^.Layers^.NumberLayers;
          MsgBox(AData^.Parent.Handle,666,mb_OK or mb_IconExclamation,'');
          AData^.Layers^.SetLayerPriority(AData);
          if CountLayerWithStateOn = 0 then begin
            MsgBox(AData^.Parent.Handle,667,mb_OK or mb_IconExclamation,'');
            Swap:=AData^.Layers^.LData;
            AData^.Layers^.LData:=AColl;
            AColl:=Swap;
            SColl^.ForEach(@SetLData);
            SLPOK:=FALSE;
          end
          else SLPOK:=TRUE;
        end
        else SLPOK:=TRUE;
        AColl^.DeleteAll;
        Dispose(AColl,Done);
        SColl^.FreeAll;
        Dispose(SColl,Done);
        AData^.Layers^.NumberLayers;
        if SLPOK then begin
          if AData^.Layers^.TopLayer^.GetState(sf_LayerOff) then begin
            OldTopLayer:=AData^.Layers^.TopLayer;
            AData^.Layers^.DetermineTopLayer;
            if AData^.Layers^.TopLayer=NIL then begin
              PLayer(AData^.Layers^.LData^.At(1))^.SetState(sf_LayerOff,FALSE);
              AData^.Layers^.DetermineTopLayer;
            end;
            if AData^.Layers^.TopLayer<>OldTopLayer then AData^.DeselectAll(TRUE);
          end;
          ToRedraw:=TRUE;
{++Sygsky: Omega support during view load }
          {$IFNDEF AXDLL} // <----------------- AXDLL
          if AData.SymbolMode = sym_Omega then // Yesss, Omega owns us!
          begin
            AData.Layers.SetTopLayer(GetOmegaRefLayerId);
            AData.Layers.TopLayer.SetState(sf_LayerOff,False);
          end;
          {$ENDIF} // <----------------- AXDLL
{--Sygsky: Omega}
        end;
        UserInterface.Update([uiLayers,uiWorkingLayer]);
      end;
      if UpdateView then begin
        if not RectEmpty(ASight^.ViewRect) and not RectsEqual(
            AData^.PInfo^.GetCurrentScreen,ASight^.ViewRect) then begin

          if not MenuFunctions['VEOnOff'].Checked then begin
             AData^.PInfo^.SetViewRotation(-ASight^.ViewRect.Rotation);
          end;

          //++ Glukhov Bug#450 Build#161 07.06.01
//          ToRedraw:=not AData^.ZoomByRect(ASight^.ViewRect,0,TRUE);

          //ProcCheckView(AData,ASight);

          AData^.ZoomByRect(ASight^.ViewRect, 0, False);
          {$IFNDEF WMLT}
          {$IFNDEF AXDLL} // <----------------- AXDLL
          DDEHandler.SendString('[NSS]['+ASight^.Name^+']');
          {$ENDIF} // <----------------- AXDLL
          {$ENDIF}
//-- Glukhov Bug#450 Build#161 07.06.01
          ToRedraw:=False;
        end;

        if LoadBmpNew then begin
          LoadNewOverviewLayer;
        end;

        for i:=1 to AData^.Layers^.LData^.Count-1 do begin
            ALayer:=AData^.Layers^.LData^.At(i);
            ALayer^.Printable:=not ALayer^.GetState(sf_LayerOff);
        end;

        if ToRedraw then AData^.PInfo^.RedrawScreen(TRUE);
      end;
    end;
  end;

{$IFNDEF WMLT}
  {$IFNDEF AXDLL} // <----------------- AXDLL
Procedure ProcSetSightFromDB
   (
   AData           : PProj;
   AParent         : TWinControl
   );
  var DDELine      : Array[0..265] of Char;
      ViewName     : Array[0..49] of Char;
      BResult       : PSight;
      OldDefault   : Integer;
      TempPoint    : PChar;
  Procedure ReadDDELine
     (
     Item          : PChar
     ); Far;
    begin
      if StrLComp(Item,'[SHV]',5) = 0 then begin
        StrCopy(DDELine,Item);
        Item[0]:=DDEStrUsed;
      end;
    end;
  Function DoAll
     (
     Item         : PSight
     ):Boolean; Far;
    var ItemName  : Array[0..49] of Char;
    begin
      StrPCopy(ItemName,Item^.Name^);
      DoAll:=StrComp(ViewName,ItemName)=0;
    end;
  begin
    DDEHandler.DDEData^.ForEach(@ReadDDELine);
    DDEHandler.DeleteDDEData;
    BResult:=NIL;
    TempPoint:=NextDDEPart(NextDDEPart(DDELine));
    ReadDDEText(TempPoint,']',ViewName);
    OldDefault:=AData^.Sights^.Default;
    BResult:=AData^.Sights^.FirstThat(@DoAll);
    if BResult <> NIL then begin
      if DDEHandler.FSendGRAV then TMDIChild(AParent).SendRedraw:=TRUE;
      ProcSetSight(AData,BResult);
      //DDEHandler.SendString('[NSS]['+BResult^.Name^+']');
      //DDEHandler.SetDBSendStrings(DDECommGRDAV,NIL);
      //DDEHandler.SendString('[RED][]');
      //DDEHandler.SetDBSendStrings(DDECommAll,NIL);
    end;
    if AData^.Sights^.Default <> OldDefault then AData^.SetModified;
  end;

Procedure ProcSelDBObjects
   (
   AData           : PProj
   );
  Procedure SelObjectsFromDDELine
     (                                             
     Item          : PChar
     ); Far;
  var Numbers      : Array[0..4096] of Byte;
      Cnt          : Integer;
      ItemIndex    : Longint;
      Pos          : Integer;
      WorkStr      : String;
      Entries      : Integer;
      Error        : Integer;
      SItem        : PIndex;
      i            : Integer;
      ExistObject  : PIndex;
      OldRealTop   : PLayer;
      OldTop       : PLayer;
      ResLInt      : LongInt;
      ResStr       : String;
      SFound       : Boolean;
      MStrs        : Boolean;
      StrToLong    : Boolean;
      MoreStrs     : Boolean;
      SearchOnAllL : Boolean;
    begin
      SearchOnAllL:=FALSE;
      StrCopy(@Numbers,Item);
      StatusBar.ProgressPanel:=TRUE;
      try
        StatusBar.ProgressText:='';
        StatusBar.AbortText:=GetLangText(3800);
        OldRealTop:=AData^.Layers^.TopLayer;
        if DDEHandler.FGotLInfo = 0 then begin
          SearchOnAllL:=TRUE;
          SetLength(WorkStr,3);
          StrLCopy(@WorkStr[1],@Numbers[6],3);
          Val(WorkStr,Entries,Error);
          Pos:=11;
        end
        else if DDEHandler.FGotLInfo = 1 then begin
          OldTop:=AData^.Layers^.TopLayer;
          if ReadDDECommandString(Item,1,0,WgBlockStart,WgBlockEnd,WgBlockEnd,
                                              ResStr,StrToLong,MoreStrs) then begin
            if ResStr <> '' then AData^.Layers^.TopLayer:=AData^.Layers^.NameToLayer(ResStr)
            else SearchOnAllL:=TRUE;
            if AData^.Layers^.TopLayer = NIL then begin
              AData^.Layers^.TopLayer:=OldTop;
              SearchOnAllL:=TRUE;
            end;
          end
          else SearchOnAllL:=TRUE;
          Pos:=GetSepPos(Item,']',2,0);
          SetLength(WorkStr,3);
          StrLCopy(@WorkStr[1],@Numbers[Pos],3);
          Val(WorkStr,Entries,Error);
          Pos:=Pos+5;
        end
        else if DDEHandler.FGotLInfo = 2 then begin
          OldTop:=AData^.Layers^.TopLayer;
          if ReadDDECommandLongInt(Item,1,0,WgBlockStart,WgBlockEnd,WgBlockEnd,
                                               ResLInt,SFound,MStrs) then begin
            if ResLInt > 0 then AData^.Layers^.TopLayer:=AData^.Layers^.IndexToLayer(ResLInt)
            else SearchOnAllL:=TRUE;
            if AData^.Layers^.TopLayer = NIL then begin
              AData^.Layers^.TopLayer:=OldTop;
              SearchOnAllL:=TRUE;
            end;
          end
          else SearchOnAllL:=TRUE;
          Pos:=GetSepPos(Item,']',2,0);
          SetLength(WorkStr,3);
          StrLCopy(@WorkStr[1],@Numbers[Pos],3);
          Val(WorkStr,Entries,Error);
          Pos:=Pos+5;
        end;
        SItem:=New(PIndex,Init(0));
        SetLength(WorkStr,10);
        for Cnt:=1 to Entries do begin
          StrLCopy(@WorkStr[1],@Numbers[Pos],10);
          Val(WorkStr,ItemIndex,Error);
          SItem^.Index:=ItemIndex;
          if ((DDEHandler.FGotLInfo = 1) or (DDEHandler.FGotLInfo = 2)) and not SearchOnAllL then begin
            if AData^.Layers^.TopLayer <> NIL then ExistObject:=AData^.Layers^.TopLayer^.HasObject(SItem)
            else ExistObject:=NIL;
          end
          else begin
            OldTop:=AData^.Layers^.TopLayer;
            ExistObject:=AData^.Layers^.TopLayer^.HasObject(SItem);
            if (ExistObject = NIL) and AData^.PInfo^.SelectionSettings.DBSelectionMode then begin                                  {>>>}
              i:=0;
              while (ExistObject = NIL) and (i < AData^.Layers^.LData^.Count) do begin
                AData^.Layers^.TopLayer:=AData^.Layers^.LData^.At(i);                           { ausklammern, wenn nur am }
                if not AData^.Layers^.TopLayer^.GetState(sf_LayerOff) then                      { obersten Layer selektiert }
                  ExistObject:=AData^.Layers^.TopLayer^.HasObject(SItem);                       { werden soll }
                i:=i+1;
              end;
            end;                                                                                {<<<}
          end;
          if ExistObject <> NIL then AData^.Layers^.TopLayer^.Select(AData^.PInfo,ExistObject);
          StatusBar.Progress:=100*Cnt/Entries;
          if StatusBar.QueryAbort then Break;
          if DDEHandler.FGotLInfo = 0 then AData^.Layers^.TopLayer:=OldTop;
          Inc(Pos,12);
        end;
        Dispose(SItem,Done);
        AData^.Layers^.TopLayer:=OldRealTop;
      finally
        StatusBar.ProgressPanel:=FALSE;
      end;
    end;
  begin
    AData^.SetCursor(crHourGlass);
    AData^.SetStatusText(GetLangText(4005),TRUE);
    DDEHandler.DDEData^.ForEach(@SelObjectsFromDDELine);
    AData^.ClearStatusText;
    AData^.SetCursor(crDefault);
  end;

Procedure ProcSelDBObjects2
   (
   AData           : PProj
   );
  Procedure SelObjectsFromDDELine
     (
     Item          : PChar
     ); Far;
  var Numbers      : Array[0..4096] of Byte;
      Cnt          : Integer;
      ItemIndex    : Longint;
      Pos          : Integer;
      WorkStr      : String;
      Entries      : Integer;
      Error        : Integer;
      SItem        : PIndex;
      i            : Integer;
      ExistObject  : PIndex;
      OldRealTop   : PLayer;
      OldTop       : PLayer;
      ResLInt      : LongInt;
      ResStr       : String;
      SFound       : Boolean;
      MStrs        : Boolean;
      StrToLong    : Boolean;
      MoreStrs     : Boolean;
      IndexLst     : TDBIndexLst;
      aptr         : Pointer;
      hstr         : string;

    begin
      StrCopy(@Numbers,Item);
      StatusBar.ProgressPanel:=TRUE;
      try
        StatusBar.ProgressText:='';
        StatusBar.AbortText:=GetLangText(3800);
        OldRealTop:=AData^.Layers^.TopLayer;
        if DDEHandler.FGotLInfo = 0 then begin
          SetLength(WorkStr,3);
          StrLCopy(@WorkStr[1],@Numbers[6],3);
          Val(WorkStr,Entries,Error);
          Pos:=11;
        end
        else if DDEHandler.FGotLInfo = 1 then begin
          OldTop:=AData^.Layers^.TopLayer;
          ReadDDECommandString(Item,1,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ResStr,StrToLong,MoreStrs);
          if AData^.Layers^.NameToLayer(ResStr) <> nil then
             AData^.Layers^.TopLayer:=AData^.Layers^.NameToLayer(ResStr);
          Pos:=GetSepPos(Item,']',2,0);
          SetLength(WorkStr,3);
          StrLCopy(@WorkStr[1],@Numbers[Pos],3);
          Val(WorkStr,Entries,Error);
          Pos:=Pos+5;
        end
        else if DDEHandler.FGotLInfo = 2 then begin
          OldTop:=AData^.Layers^.TopLayer;
          ReadDDECommandLongInt(Item,1,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ResLInt,SFound,MStrs);
          if AData^.Layers^.IndexToLayer(ResLInt) <> nil then
             AData^.Layers^.TopLayer:=AData^.Layers^.IndexToLayer(ResLInt);
          Pos:=GetSepPos(Item,']',2,0);
          SetLength(WorkStr,3);
          StrLCopy(@WorkStr[1],@Numbers[Pos],3);
          Val(WorkStr,Entries,Error);
          Pos:=Pos+5;
        end;
        SItem:=New(PIndex,Init(0));
        SetLength(WorkStr,10);

        IndexLst:=TDBIndexLst.Create;

        for Cnt:=1 to Entries do begin
          StrLCopy(@WorkStr[1],@Numbers[Pos],10);
          Val(WorkStr,ItemIndex,Error);
          SItem^.Index:=ItemIndex;
          IndexLst.Add(ItemIndex);
          Inc(Pos,12);
        end;

        if Adata^.Document.loaded then  begin
          AData^.Document.GetLayers.LayersSelectByIndex(AData^.Document,IndexLst);
          AData^.Document.DrawSelection(AData^.PInfo);
          AData^.PInfo^.RedrawRect(AData^.Document.SelRect);
        end;

        IndexLst.free;

        Dispose(SItem,Done);
        AData^.Layers^.TopLayer:=OldRealTop;
      finally
        StatusBar.ProgressPanel:=FALSE;
      end;
      Item[0]:=DDEStrUsed;
    end;
  begin
    AData^.SetCursor(crHourGlass);
    AData^.SetStatusText(GetLangText(4005),TRUE);
    DDEHandler.DDEData^.ForEach(@SelObjectsFromDDELine);
    DDEHandler.DeleteDDEData;
    AData^.ClearStatusText;
    AData^.SetCursor(crDefault);
  end;

Procedure ProcDBAddLayerWithoutDlg
   (
   AData           : PProj
   );
  const WindowsLStyles = 5;
        WindowsHStyles = 8;
  var NewLStyle    : TLayerProperties;
      LayerName    : String;
      Position     : Integer;
      GenOnOff     : Boolean;
      ALineStyles  : TXLineStyleList;
      AFillStyles  : TXFillStyleList;
      LItem        : TXLineStyle;
      HItem        : TCustomXFill;
      StyleFound   : Boolean;
      ResLInt      : LongInt;
      ResReal      : Real;
      StrOK        : Boolean;
      StrFound     : Boolean;
      StrToLong    : Boolean;
      MoreStrs     : Boolean;
      i            : Integer;
      NameDouble   : Boolean;
      InsertLayer  : PLayer;
  Function IsNameDouble
     (
     Item          : PLayer
     )
     : Boolean; Far;
    begin
      Result:=FALSE;
      if Item^.Text <> NIL then
        if Item^.Text^ = LayerName then Result:=TRUE;
    end;
  begin
//    AData^.ChangeRedraw(CNoRedraw);
    // LayerName
    StrOK:=ReadDDECommandString(PChar(DDEHandler.DDEData^.At(0)),1,0,WgBlockStart,
                                            DDEHandler.FDDESepSign,WgBlockEnd,LayerName,
                                            StrToLong,MoreStrs);
    if StrOK and (LayerName <> '') then begin
      NewLStyle:=TLayerProperties.Create;
      NewLStyle.Name:=LayerName;
      // Layer on or off
      if ReadDDECommandLongInt(PChar(DDEHandler.DDEData^.At(0)),1,1,WgBlockStart,
                                           DDEHandler.FDDESepSign,WgBlockEnd,ResLInt,
                                           StrFound,MoreStrs) then begin
        if ResLInt = 0 then NewLStyle.Visible:=0
        else NewLStyle.Visible:=1;
      end
      else NewLStyle.Visible:=1;
      // Position
      if ReadDDECommandLongInt(PChar(DDEHandler.DDEData^.At(0)),1,2,WgBlockStart,
                                           DDEHandler.FDDESepSign,WgBlockEnd,ResLInt,
                                           StrFound,MoreStrs) then begin
        case ResLInt of
          0 : Position:=ilTop;
          1 : Position:=ilSecond;
          2 : Position:=ilLast;
          else Position:=ilLast;
        end;
      end
      else Position:=ilTop;
      // Generalisation
      if ReadDDECommandLongInt(PChar(DDEHandler.DDEData^.At(0)),1,3,WgBlockStart,
                                           DDEHandler.FDDESepSign,WgBlockEnd,ResLInt,
                                           StrFound,MoreStrs) then begin
        if ResLInt = 1 then GenOnOff:=TRUE
        else GenOnOff:=FALSE;
      end
      else GenOnOff:=FALSE;
      if GenOnOff then begin
        // lower Generalisation limit
        if ReadDDECommandReal(PChar(DDEHandler.DDEData^.At(0)),1,4,WgBlockStart,
                                          DDEHandler.FDDESepSign,WgBlockEnd,ResReal,
                                          StrFound,MoreStrs) then begin
          if (ResReal >= GENERALIZE_MIN) and (ResReal <= GENERALIZE_MAX) then NewLStyle.GeneralMin:=1/ResReal
          else NewLStyle.GeneralMin:=GENERALIZE_MIN;
        end
        else NewLStyle.GeneralMin:=GENERALIZE_MIN;
        // upper Generalisation limit
        if ReadDDECommandReal(PChar(DDEHandler.DDEData^.At(0)),1,5,WgBlockStart,
                                          DDEHandler.FDDESepSign,WgBlockEnd,ResReal,
                                          StrFound,MoreStrs) then begin
          if (ResReal >= GENERALIZE_MIN) and (ResReal < 1/NewLStyle.GeneralMin) then NewLStyle.GeneralMax:=1/ResReal
          else NewLStyle.GeneralMax:=GENERALIZE_MAX;
        end
        else NewLStyle.GeneralMax:=GENERALIZE_MAX;
      end;
      // Fix on or off
      if ReadDDECommandLongInt(PChar(DDEHandler.DDEData^.At(0)),1,6,WgBlockStart,
                                           DDEHandler.FDDESepSign,WgBlockEnd,ResLInt,
                                           StrFound,MoreStrs) then begin
        if ResLInt = 1 then NewLStyle.Editable:=0
        else NewLStyle.Editable:=1;
      end
      else NewLStyle.Editable:=1;
      // LineProperties
      // LineType
      if ReadDDECommandLongInt(PChar(DDEHandler.DDEData^.At(0)),1,7,WgBlockStart,
                                           DDEHandler.FDDESepSign,WgBlockEnd,ResLInt,
                                           StrFound,MoreStrs) then begin
        NewLStyle.LineStyle.Style:=lt_Solid;
        if ResLInt >= lt_Solid then begin
          if ResLInt < WindowsLStyles then NewLStyle.LineStyle.Style:=ResLInt
          else begin
            StyleFound:=FALSE;
            i:=0;
            ALineStyles:=AData^.PInfo^.ProjStyles.XLineStyles;
            repeat
              LItem:=ALineStyles.Items[i];
              if LItem <> NIL then begin
                if ResLInt = LItem.Number then begin
                  NewLStyle.LineStyle.Style:=LItem.Number;
                  StyleFound:=TRUE;
                end;
              end;
              Inc(i);
            until StyleFound or (i >= ALineStyles.Capacity);
          end;
        end;
      end
      else NewLStyle.LineStyle.Style:=0;
      // LineWidth
      if (NewLStyle.LineStyle.Style = 0) or (NewLStyle.LineStyle.Style >= WindowsLStyles) then begin
        if ReadDDECommandReal(PChar(DDEHandler.DDEData^.At(0)),1,8,WgBlockStart,
                                             DDEHandler.FDDESepSign,WgBlockEnd,ResReal,
                                             StrFound,MoreStrs) then begin
          if ResReal >= 0 then NewLStyle.LineStyle.Width:=ResReal*10
          else NewLStyle.LineStyle.Width:=0;
        end
        else NewLStyle.LineStyle.Width:=0;
      end
      else NewLStyle.LineStyle.Width:=0;
      // LinieColor
      if ReadDDECommandLongInt(PChar(DDEHandler.DDEData^.At(0)),1,9,WgBlockStart,
                                           DDEHandler.FDDESepSign,WgBlockEnd,ResLInt,
                                           StrFound,MoreStrs) then begin
        if (ResLInt >= 0) and (ResLInt <= $FFFFFF) then NewLStyle.LineStyle.Color:=ResLInt
        else NewLStyle.LineStyle.Color:=0;
      end
      else NewLStyle.LineStyle.Color:=0;
      // HatchProperties
      // HatchType
      if ReadDDECommandLongInt(PChar(DDEHandler.DDEData^.At(0)),1,10,WgBlockStart,
                                           DDEHandler.FDDESepSign,WgBlockEnd,ResLInt,
                                           StrFound,MoreStrs) then begin
        NewLStyle.FillStyle.Pattern:=pt_NoPattern;
        if ResLInt >= pt_NoPattern then begin
          if ResLInt < WindowsHStyles then NewLStyle.FillStyle.Pattern:=ResLInt
          else begin
            StyleFound:=FALSE;
            i:=0;
            AFillStyles:=AData^.PInfo^.ProjStyles.XFillStyles;
            repeat
              HItem:=AFillStyles.Items[i];
              if HItem <> NIL then begin
                if ResLInt = HItem.Number then begin
                  NewLStyle.FillStyle.Pattern:=HItem.Number;
                  StyleFound:=TRUE;
                end;
              end;
              Inc(i);
            until StyleFound or (i >= AFillStyles.Capacity);
          end;
        end;
      end
      else NewLStyle.FillStyle.Pattern:=0;
      // ForeColor
      if ReadDDECommandLongInt(PChar(DDEHandler.DDEData^.At(0)),1,11,WgBlockStart,
                                           DDEHandler.FDDESepSign,WgBlockEnd,ResLInt,
                                           StrFound,MoreStrs) then begin
        if (ResLInt >= 0) and (ResLInt <= $FFFFFF) then NewLStyle.FillStyle.ForeColor:=ResLInt
        else NewLStyle.FillStyle.ForeColor:=0;
      end
      else NewLStyle.FillStyle.ForeColor:=0;
      // Transparency = BackColor
      if ReadDDECommandLongInt(PChar(DDEHandler.DDEData^.At(0)),1,12,WgBlockStart,
                                           DDEHandler.FDDESepSign,WgBlockEnd,ResLInt,
                                           StrFound,MoreStrs) then begin
        if ResLInt = 1 then NewLStyle.FillStyle.BackColor:=intNone
        else NewLStyle.FillStyle.BackColor:=$FFFFFF;                                         {white}
      end
      else NewLStyle.FillStyle.BackColor:=$FFFFFF;

      InsertLayer:=AData^.Layers^.LData^.FirstThat(@IsNameDouble);
      if InsertLayer = NIL then begin
         InsertLayer:=AData^.Layers^.InsertLayer('',0,0,0,0,Position,DefaultSymbolFill);
         if InsertLayer <> NIL then begin
            InsertLayer^.SetLayerStyle(AData^.PInfo,NewLStyle);
            AData^.Layers^.DetermineTopLayer;
            AData^.SetModified;
         end;
      end
      else begin
           if PLayer(InsertLayer)^.SetLayerStyle(AData^.PInfo,NewLStyle) then
              AData^.SetModified;
      end;

      NewLStyle.Destroy;
    end;
//    AData^.ChangeRedraw(COldRedraw);
    if DDEHandler.DDEData^.GetCount > 0 then PChar(DDEHandler.DDEData^.At(0))[0]:=DDEStrUsed;
    DDEHandler.DeleteDDEData;
  end;

Procedure ProcDBChangeLayerWithoutDlg
   (
   AData           : PProj
   );
  const WindowsLStyles = 5;
        WindowsHStyles = 8;
  var ChangeLayer  : PLayer;
      NewLStyle    : TLayerProperties;
      LayerName    : String;
      Position     : Integer;
      GenOnOff     : Boolean;
      ALineStyles  : TXLineStyleList;
      AFillStyles  : TXFillStyleList;
      LItem        : TXLineStyle;
      HItem        : TCustomXFill;
      StyleFound   : Boolean;
      ResLInt      : LongInt;
      ResReal      : Real;
      StrOK        : Boolean;
      StrFound     : Boolean;
      StrToLong    : Boolean;
      MoreStrs     : Boolean;
      i            : Integer;
      NameDouble   : Boolean;
      OldTopLayer  : PLayer;
      OldPos       : Integer;
  Procedure IsNameDouble
     (
     Item          : PLayer
     ); Far;
    begin
      if Item^.Text <> NIL then
        if Item^.Text^ = LayerName then NameDouble:=TRUE;
    end;
  begin

    // ChangeLayer
    ChangeLayer:=NIL;
    if DDEHandler.FGotLInfo <> 2 then begin

      StrOK:=ReadDDECommandString(PChar(DDEHandler.DDEData^.At(0)),1,0,WgBlockStart,
                                              DDEHandler.FDDESepSign,WgBlockEnd,LayerName,
                                              StrToLong,MoreStrs);
      if StrOK and (LayerName <> '') then ChangeLayer:=AData^.Layers^.NameToLayer(LayerName)
      else ChangeLayer:=NIL;
    end
    else begin
      StrOK:=ReadDDECommandLongInt(PChar(DDEHandler.DDEData^.At(0)),1,0,WgBlockStart,
                                               DDEHandler.FDDESepSign,WgBlockEnd,ResLInt,
                                               StrFound,MoreStrs);
      if StrOK and (ResLInt > 0) then ChangeLayer:=AData^.Layers^.IndexToLayer(ResLInt)
      else ChangeLayer:=NIL;
    end;
    if ChangeLayer <> NIL then begin
      NewLStyle:=ChangeLayer^.GetLayerStyle;
      // LayerName
      StrOK:=ReadDDECommandString(PChar(DDEHandler.DDEData^.At(0)),2,0,WgBlockStart,
                                              DDEHandler.FDDESepSign,WgBlockEnd,LayerName,
                                              StrToLong,MoreStrs);
      if StrOK and (LayerName <> '') then begin
        NameDouble:=FALSE;
        AData^.Layers^.LData^.ForEach(@IsNameDouble);
        if not NameDouble then NewLStyle.Name:=LayerName;
      end;
      // Layer on or off
      if ReadDDECommandLongInt(PChar(DDEHandler.DDEData^.At(0)),2,1,WgBlockStart,
                                           DDEHandler.FDDESepSign,WgBlockEnd,ResLInt,
                                           StrFound,MoreStrs) then begin
        case ResLInt of
          -2  : NewLStyle.Visible:=1;
          -1  : {NoChange};
           0  : NewLStyle.Visible:=0;
           1  : NewLStyle.Visible:=1;
          else {NoChange};
        end;
      end;
      // Position
      if ReadDDECommandLongInt(PChar(DDEHandler.DDEData^.At(0)),2,2,WgBlockStart,
                                           DDEHandler.FDDESepSign,WgBlockEnd,ResLInt,
                                           StrFound,MoreStrs) then begin
        if ResLInt >= -3 then begin
          case ResLInt of
            -3 : Position:=AData^.Layers^.LData^.Count-2;
            -2 : Position:=0;
            -1 : Position:=-1;
            else begin
                   if ResLInt <= AData^.Layers^.LData^.Count-2 then Position:=ResLInt
                   else Position:=-1;
                 end;
          end;
        end
        else Position:=-1;
      end
      else Position:=-1;
      // Generalisation
      if ReadDDECommandLongInt(PChar(DDEHandler.DDEData^.At(0)),2,3,WgBlockStart,
                                           DDEHandler.FDDESepSign,WgBlockEnd,ResLInt,
                                           StrFound,MoreStrs) then begin
        case ResLInt of
          -2 : GenOnOff:=FALSE;
          -1 : begin
                 if (DblCompare(NewLStyle.GeneralMin,GENERALIZE_MIN) <> 0)
                    or (DblCompare(NewLStyle.GeneralMax,GENERALIZE_MAX) <> 0) then GenOnOff:=TRUE
                 else GenOnOff:=FALSE;
               end;
           0 : GenOnOff:=FALSE;
           1 : GenOnOff:=TRUE;
           else begin
                  if (DblCompare(NewLStyle.GeneralMin,GENERALIZE_MIN) <> 0)
                    or (DblCompare(NewLStyle.GeneralMax,GENERALIZE_MAX) <> 0) then GenOnOff:=TRUE
                  else GenOnOff:=FALSE;
                end;
        end;
      end
      else begin
        if (DblCompare(NewLStyle.GeneralMin,GENERALIZE_MIN) <> 0)
           or (DblCompare(NewLStyle.GeneralMax,GENERALIZE_MAX) <> 0) then GenOnOff:=TRUE
        else GenOnOff:=FALSE;
      end;
      if GenOnOff then begin
        // lower Generalisation limit
        if ReadDDECommandReal(PChar(DDEHandler.DDEData^.At(0)),2,4,WgBlockStart,
                                          DDEHandler.FDDESepSign,WgBlockEnd,ResReal,
                                          StrFound,MoreStrs) then begin

          if ResReal = -2 then NewLStyle.GeneralMin:=GENERALIZE_MIN
          else if ResReal = -1 then {NoChange}
          else if ResReal > 0 then begin
            if (ResReal >= GENERALIZE_MIN) and (ResReal <= GENERALIZE_MAX) then NewLStyle.GeneralMin:=1/ResReal
            else NewLStyle.GeneralMin:=GENERALIZE_MIN;
          end;
        end;
        // upper Generalisation limit
        if ReadDDECommandReal(PChar(DDEHandler.DDEData^.At(0)),2,5,WgBlockStart,
                                          DDEHandler.FDDESepSign,WgBlockEnd,ResReal,
                                          StrFound,MoreStrs) then begin
          if ResReal = -2 then NewLStyle.GeneralMax:=GENERALIZE_MAX
          else if ResReal = -1 then {NoChange}
          else if ResReal > 0 then begin
            if (ResReal >= GENERALIZE_MIN) and (ResReal < 1/NewLStyle.GeneralMin) then NewLStyle.GeneralMax:=1/ResReal
            else NewLStyle.GeneralMax:=GENERALIZE_MAX;
          end;
        end;
      end
      else begin
        NewLStyle.GeneralMin:=GENERALIZE_MIN;
        NewLStyle.GeneralMax:=GENERALIZE_MAX;
      end;
      // Fix on or off
      if ReadDDECommandLongInt(PChar(DDEHandler.DDEData^.At(0)),2,6,WgBlockStart,
                                           DDEHandler.FDDESepSign,WgBlockEnd,ResLInt,
                                           StrFound,MoreStrs) then begin
        case ResLInt of
          -2 : NewLStyle.Editable:=1;
          -1 : {NoChange};
           0 : NewLStyle.Editable:=1;
           1 : NewLStyle.Editable:=0;
           else {NoChange};
        end;
      end;
      // LineProperties
      // LineType
      if ReadDDECommandLongInt(PChar(DDEHandler.DDEData^.At(0)),2,7,WgBlockStart,
                                           DDEHandler.FDDESepSign,WgBlockEnd,ResLInt,
                                           StrFound,MoreStrs) then begin
        if ResLInt >= -2 then begin
          case ResLInt of
            -2 : NewLStyle.LineStyle.Style:=lt_Solid;
            -1 : {NoChange};
            else begin
                   if ResLInt < WindowsLStyles then NewLStyle.LineStyle.Style:=ResLInt
                   else begin
                     StyleFound:=FALSE;
                     i:=0;
                     ALineStyles:=AData^.PInfo^.ProjStyles.XLineStyles;
                     repeat
                       LItem:=ALineStyles.Items[i];
                       if LItem <> NIL then begin
                         if ResLInt = LItem.Number then begin
                           NewLStyle.LineStyle.Style:=LItem.Number;
                           StyleFound:=TRUE;
                         end;
                       end;
                       Inc(i);
                     until StyleFound or (i >= ALineStyles.Capacity);
                   end;
                 end;
          end;
        end;
      end;
      // LineWidth
      if (NewLStyle.LineStyle.Style = 0) or (NewLStyle.LineStyle.Style >= WindowsLStyles) then begin
        if ReadDDECommandReal(PChar(DDEHandler.DDEData^.At(0)),2,8,WgBlockStart,
                                             DDEHandler.FDDESepSign,WgBlockEnd,ResReal,
                                             StrFound,MoreStrs) then begin
          if ResReal >= -2 then begin
            if ResReal = -2 then NewLStyle.LineStyle.Width:=0
            else if ResReal = -1 then {NoChange}
            else if ResReal >= 0 then NewLStyle.LineStyle.Width:=ResReal*10
            else NewLStyle.LineStyle.Width:=0;
          end;
        end;
      end;
      // LinieColor
      if ReadDDECommandLongInt(PChar(DDEHandler.DDEData^.At(0)),2,9,WgBlockStart,
                                           DDEHandler.FDDESepSign,WgBlockEnd,ResLInt,
                                           StrFound,MoreStrs) then begin
        if (ResLInt >= -2) and (ResLInt <= $FFFFFF) then begin
          case ResLInt of
            -2 : NewLStyle.LineStyle.Color:=0;
            -1 : {NoChange};
            else begin
                   if (ResLInt >= 0) and (ResLInt <= $FFFFFF) then NewLStyle.LineStyle.Color:=ResLInt
                   else NewLStyle.LineStyle.Color:=0;
                 end;
          end;
        end;
      end;
      // HatchProperties
      // HatchType
      if ReadDDECommandLongInt(PChar(DDEHandler.DDEData^.At(0)),2,10,WgBlockStart,
                                           DDEHandler.FDDESepSign,WgBlockEnd,ResLInt,
                                           StrFound,MoreStrs) then begin
        if ResLInt >= -2 then begin
          case ResLInt of
            -2 : NewLStyle.FillStyle.Pattern:=pt_NoPattern;
            -1 : {NoChange};
            else begin
                   if ResLInt < WindowsHStyles then NewLStyle.FillStyle.Pattern:=ResLInt
                   else begin
                     StyleFound:=FALSE;
                     i:=0;
                     AFillStyles:=AData^.PInfo^.ProjStyles.XFillStyles;
                     repeat
                       HItem:=AFillStyles.Items[i];
                       if HItem <> NIL then begin
                         if ResLInt = HItem.Number then begin
                           NewLStyle.FillStyle.Pattern:=HItem.Number;
                           StyleFound:=TRUE;
                         end;
                       end;
                       Inc(i);
                     until StyleFound or (i >= AFillStyles.Capacity);
                   end;
                 end;
          end;
        end;
      end;
      // ForeColor
      if ReadDDECommandLongInt(PChar(DDEHandler.DDEData^.At(0)),2,11,WgBlockStart,
                                           DDEHandler.FDDESepSign,WgBlockEnd,ResLInt,
                                           StrFound,MoreStrs) then begin
        if (ResLInt >= -2) and (ResLInt <= $FFFFFF) then begin
          case ResLInt of
            -2 : NewLStyle.FillStyle.ForeColor:=0;
            -1 : {NoChange};
            else begin
                   if (ResLInt >= 0) and (ResLInt <= $FFFFFF) then NewLStyle.FillStyle.ForeColor:=ResLInt
                   else NewLStyle.FillStyle.ForeColor:=0;
                 end;
          end;
        end;
      end;
      // Transparency = BackColor
      if ReadDDECommandLongInt(PChar(DDEHandler.DDEData^.At(0)),2,12,WgBlockStart,
                                           DDEHandler.FDDESepSign,WgBlockEnd,ResLInt,
                                           StrFound,MoreStrs) then begin
        case ResLInt of
          -2 : NewLStyle.FillStyle.BackColor:=$FFFFFF;
          -1 : {NoChange};
           0 : NewLStyle.FillStyle.BackColor:=$FFFFFF;
           1 : NewLStyle.FillStyle.BackColor:=intNone;
           else {NoChange};
        end;
      end;
      ChangeLayer^.SetLayerStyle(AData^.PInfo,NewLStyle);
      NewLStyle.Destroy;
      if Position <> -1 then begin
        OldPos:=AData^.Layers^.LData^.IndexOf(ChangeLayer);
        if Position < AData^.Layers^.LData^.Count-1 then begin
          if OldPos-1 <> Position then begin
            OldTopLayer:=AData^.Layers^.TopLayer;
            if (Position = 0) or (OldPos = 1) then AData^.DeselectAll(TRUE);
            AData^.Layers^.LData^.AtDelete(OldPos);
            AData^.Layers^.NumberLayers;
            AData^.Layers^.LData^.AtInsert(Position+1,ChangeLayer);
            AData^.Layers^.NumberLayers;
            AData^.Layers^.SetTopLayer(OldTopLayer);
          end;
        end;
      end;
      UpdateLayersViewsLegendsLists(AData);
      AData^.SetModified;
    end;     {ChangeLayer <> NIL}
    if DDEHandler.DDEData^.GetCount > 0 then PChar(DDEHandler.DDEData^.At(0))[0]:=DDEStrUsed;
    DDEHandler.DeleteDDEData;
    AData^.PInfo^.RedrawScreen(TRUE);
  end;

Procedure ProcRequestLayer
   (
   AData           : PProj
   );
  var LayerName    : String;
      LayerIndex   : LongInt;
      Error        : Integer;
      RLayer       : PLayer;
      SendString   : String;
      OldDBLInfo   : Byte;
      StrToLong    : Boolean;
      MoreStrs     : Boolean;
  begin
    ReadDDECommandString(DDEHandler.DDEData^.At(0),1,0,WgBlockStart,
                                     DDEHandler.FDDESepSign,WgBlockEnd,LayerName,StrToLong,MoreStrs);
    if DDEHandler.FGotLInfo <> 2 then RLayer:=AData^.Layers^.NameToLayer(LayerName)
    else begin
      Val(LayerName,LayerIndex,Error);
      if Error = 0 then begin
        RLayer:=AData^.Layers^.IndexToLayer(LayerIndex);
        Str(LayerIndex:10,LayerName);
      end
      else RLayer:=NIL;
    end;
    SendString:='[LEX]['+LayerName+'][';
    if RLayer = NIL then SendString:=SendString+'0]'
    else SendString:=SendString+'1]';
    OldDBLInfo:=DDEHandler.FLayerInfo;
    DDEHandler.FLayerInfo:=DDEHandler.FGotLInfo;
    DDEHandler.SendString(SendString);
    DDEHandler.FLayerInfo:=OldDBLInfo;
    if DDEHandler.DDEData^.GetCount > 0 then PChar(DDEHandler.DDEData^.At(0))[0]:=DDEStrUsed;
    DDEHandler.DeleteDDEData;
    DDEHandler.SetAnswerMode(FALSE);
  end;

Procedure ProcRequestLayerInfos
   (
   AData           : PProj;
   ADDECommand     : PChar
   );
  var LayerPos     : Integer;
  Procedure DoAll
     (
     Item          : PLayer
     ); Far;
    var TempStr    : String;
        TmpDouble  : Double;
        TmpInt     : Integer;
        DDEComm    : String;
    begin
      if Item^.Index <> 0 then begin
        DDEComm:=StrPas(ADDECommand);
        DDEHandler.Reset;
        DDEHandler.Append('['+DDEComm+'][');
        Str(Item^.Index:10,TempStr);
        DDEHandler.Append(TempStr+DDEHandler.FOutSepSign);
        DDEHandler.Append(Item^.Text^);          {kann 255 Zeichen lang sein}
        DDEHandler.Append(DDEHandler.FOutSepSign);
        Inc(LayerPos);
        Str(LayerPos:0,TempStr);
        DDEHandler.Append(TempStr+DDEHandler.FOutSepSign);
        if Item = AData^.Layers^.TopLayer then DDEHandler.Append('1'+DDEHandler.FOutSepSign)
        else DDEHandler.Append('0'+DDEHandler.FOutSepSign);
        if Item^.GetState(sf_LayerOff) then DDEHandler.Append('0'+DDEHandler.FOutSepSign)
        else DDEHandler.Append('1'+DDEHandler.FOutSepSign);
        if (DblCompare(Item^.GeneralMin,GENERALIZE_MIN) <> 0)
           or (DblCompare(Item^.GeneralMax,GENERALIZE_MAX) <> 0) then DDEHandler.Append('1'+DDEHandler.FOutSepSign)
        else DDEHandler.Append('0'+DDEHandler.FOutSepSign);
        TmpDouble:=1/Item^.GeneralMin;
        Str(TmpDouble:0:2,TempStr);
        DDEHandler.Append(TempStr+DDEHandler.FOutSepSign);
        TmpDouble:=1/Item^.GeneralMax;
        Str(TmpDouble:0:2,TempStr);
        DDEHandler.Append(TempStr+DDEHandler.FOutSepSign);
        if Item^.GetState(sf_Fixed) then DDEHandler.Append('1'+DDEHandler.FOutSepSign)
        else DDEHandler.Append('0'+DDEHandler.FOutSepSign);
        TmpInt:=Item^.LineStyle.Style;
        Str(TmpInt:0,TempStr);
        DDEHandler.Append(TempStr+DDEHandler.FOutSepSign);
        TmpDouble:=Item^.LineStyle.Width/10;

        // Parameter is interpreted for usage of MapMaster!
        Str(Round(TmpDouble):0,TempStr);
        // normaly use this line
        {Str(TmpDouble:0:1,TempStr);}

        DDEHandler.Append(TempStr+DDEHandler.FOutSepSign);
        Str(Item^.LineStyle.Color:0,TempStr);
        DDEHandler.Append(TempStr+DDEHandler.FOutSepSign);
        Str(Item^.FillStyle.Pattern:0,TempStr);
        DDEHandler.Append(TempStr+DDEHandler.FOutSepSign);
        Str(Item^.FillStyle.ForeColor:0,TempStr);
        DDEHandler.Append(TempStr+DDEHandler.FOutSepSign);
        if Item^.FillStyle.BackColor=clNone then DDEHandler.Append('1]')
        else DDEHandler.Append('0]');
        DDEHandler.Append(#0);
        DDEHandler.Execute;
      end;
    end;
  begin
    LayerPos:=0;
    DDEHandler.GetGlobal(DDELInfoSize);
    AData^.Layers^.LData^.ForEach(@DoAll);
    DDEHandler.FreeGlobal;
    DDEHandler.SendString('[END][]');
    DDEHandler.SetAnswerMode(FALSE);
  end;

Procedure ProcGetDBText
   (
   AData           : PProj;
   var DBText      : String
   );
  var DDELine      : Array[0..300] of Char;
      TempPointer  : PChar;
  Procedure ReadDDELine
     (
     Item          : PChar
     ); Far;
    begin
      if StrLComp(Item,'[STX]',5) = 0 then begin
        StrCopy(DDELine,Item);
        Item[0]:=DDEStrUsed;
      end;
    end;
  begin
    DDEHandler.DDEData^.ForEach(@ReadDDELine);
    DDEHandler.DeleteDDEData;
    TempPointer:=NextDDEPart(NextDDEpart(DDELine));
    ReadDDEString(TempPointer,']',DBText);
  end;

Function ProcSelectSymbolForDGR
   (
   AData           : PProj
   ) : Boolean;

  var SymRollUp    : TSymbolRollupForm;
      LastSymbol   : LongInt;
      StrFound     : Boolean;
      MoreStrs     : Boolean;
  begin
    ReadDDECommandLongInt(AData^.SRQDatas,1,0,WgBlockStart,DDEHandler.FDDESepSign,
                                      WgBlockEnd,LastSymbol,StrFound,MoreStrs);
    SymRollUp:=TSymbolRollupForm(UserInterface.RollupNamed['SymbolRollupForm']);
    if SymRollUp <> NIL then begin
      if not SymRollUp.RollupToolWindow.Visible then begin
        SymRollUp.FWasVisible:=FALSE;
        SymRollUp.FSendSymInfo:=TRUE;
        SymRollUp.RollupToolWindow.Visible:=TRUE;
      end
      else begin
        SymRollUp.FWasVisible:=TRUE;
        SymRollUp.FSendSymInfo:=TRUE;
      end;
    end;
  end;

Procedure ProcDBDeSelectAll
   (
   AData           : PProj;
   ATextOut        : Boolean
   );
  begin
    AData^.DeSelectAll(ATextOut);
    DDEHandler.SendString('[SHW][]');
    if DDEHandler.FShowADSA then DDEHandler.SendSHWADSA;
  end;
  {$ENDIF} // <----------------- AXDLL

//++ Glukhov Bug#442 Build#161 30.05.01
Procedure ProcToCurve ( AData: PProj );
  var ACurve      : PSpline;
      APoly       : PPoly;
      AItem       : PIndex;
      iObj, nObj  : Integer;
  begin
    AData^.SetCursor( crHourGlass );
    nObj:= AData.Layers^.SelLayer^.Data^.GetCount;
    for iObj:=0 to nObj-1 do begin
//    if AData^.Layers^.SelLayer^.Data^.GetCount = 1 then begin
//      APoly:=PPoly(AData^.Layers^.IndexObject(AData^.PInfo,AData^.Layers^.SelLayer^.Data^.At(0)));
      APoly:= PPoly( AData.Layers.IndexObject( AData.PInfo, AData.Layers.SelLayer.Data.At( iObj ) ) );
      if APoly^.GetObjType = ot_Poly then begin
        if APoly.Data.Count > spMaxPoints then begin
          // The strange message was changed
//          MsgBox( AData.Parent.Handle, 1016, mb_IconExclamation or mb_OK, '' )
          MsgBox( AData.Parent.Handle, 1034, mb_IconExclamation or mb_OK, '' )
        end
        else begin
          AItem:= AData.Layers.TopLayer^.HasObject( APoly );
          if Assigned( AItem )  then AData.Layers.TopLayer.DeSelect( AData.PInfo, AItem );
          ACurve:= New( PSpline, Init );
          ACurve^.SetByPoly( APoly );
          AData^.InsertObject( ACurve,TRUE );
          {$IFNDEF AXDLL} // <----------------- AXDLL
          if (DDEHandler.DoAutoIns) {and (DDEHandler.FDBAutoIn)} then AData^.DBEditAuto(PIndex(ACurve));
          {$ENDIF} // <----------------- AXDLL
        end;
      end;
    end;
    AData^.SetCursor(crDefault);
  end;
//-- Glukhov Bug#442 Build#161 30.05.01

//++ Glukhov Bug#192 Build#161 29.05.01
Procedure ProcConvToPLine ( AData: PProj );
  var
    AView       : PView;
    APoly       : PPoly;
    Pnt         : TDPoint;
    iObj, nObj  : Integer;
    iVtx, nVtx  : Integer;
    iSeg, nSeg  : Integer;
    dVtx        : Double;
    AItem       : PIndex;
    APt         : TDPoint;
    SPt         : PDPoint;
    AEll        : PEllipse;
    SnObj       : PIndex;
  begin
    Pnt.Init( 0, 0 );
    AData^.SetCursor( crHourGlass );
    nObj:= AData.Layers^.SelLayer^.Data^.GetCount;
    for iObj:= nObj-1 downto 0 do begin
      AView:= AData.Layers.IndexObject( AData.PInfo, AData.Layers.SelLayer.Data.At( iObj ) );
      if (AView.GetObjType in [ot_Arc, ot_Circle, ot_Spline])  then begin

        AItem:= AData.Layers.TopLayer.HasObject( AView );
        if Assigned(AItem)  then AData.Layers.TopLayer.DeSelect( AData.PInfo, AItem );
        APoly:= New( PPoly, Init );

        if (AView.GetObjType = ot_Arc)
        or (AView.GetObjType = ot_Circle)
        then begin
          with AData.PInfo.VariousSettings do begin
            if bConvRelMistake
            then  nVtx:= PEllipse(AView).ApproximatePointsNumber( dbConvRelMistake, True )
            else  nVtx:= PEllipse(AView).ApproximatePointsNumber( dbConvAbsMistake, False );
          end;
          if nVtx >= MaxCollectionSize  then nVtx:= MaxCollectionSize-1;
          for iVtx:=0 to nVtx-1 do begin
            Pnt:= PEllipse(AView).ApproximatePoint( iVtx, nVtx );
            APoly.InsertPoint( Pnt );
          end;
        end;

        if (AView.GetObjType = ot_Spline) then begin
          // Insert the 1st spline point
          Pnt:= PDPoint( PSpline(AView).Data.At( 0 ) )^;
          APoly.InsertPoint( Pnt );
          nSeg:= PSpline(AView).SegmentsNum;

          // Evaluate the number of vertices
          nVtx:= 1;
          for iSeg:=0 to nSeg-1 do begin
            with PProj(AData).PInfo.VariousSettings do begin
              if bConvRelMistake
              then  iVtx:= PSpline(AView).ApprSegmPointsNumber( iSeg, dbConvRelMistake, True )
              else  iVtx:= PSpline(AView).ApprSegmPointsNumber( iSeg, dbConvAbsMistake, False );
            end;
            nVtx:= nVtx + iVtx - 1;
          end;
          if nVtx < MaxCollectionSize
          then  dVtx:= 1.0
          else  dVtx:= (MaxCollectionSize-64) / nVtx;

          // Insert points
          for iSeg:=0 to nSeg-1 do begin
            with PProj(AData).PInfo.VariousSettings do begin
              if bConvRelMistake
              then  iVtx:= PSpline(AView).ApprSegmPointsNumber( iSeg, dbConvRelMistake, True )
              else  iVtx:= PSpline(AView).ApprSegmPointsNumber( iSeg, dbConvAbsMistake, False );
            end;
            nVtx:= Round( dVtx * (iVtx-1) ) + 1;
            if nVtx < 2  then nVtx:= 2;
            for iVtx:=1 to nVtx-1 do begin
              Pnt:= PSpline(AView).ApprSegmPoint( iSeg, iVtx, nVtx );
              APoly.InsertPoint( Pnt );
            end;
          end;
        end;

        APt.Init(0,0);

        SPt:=APoly^.Data^.At(0);
        if SPt <> nil then begin
           AEll:=New(PEllipse,Init(SPt^,2,2,0));
           SnObj:=nil;
           try
              if AData^.Layers^.TopLayer^.SearchForPoint(AData^.PInfo,AEll,0,[ot_Poly,ot_CPoly,ot_Spline],APt,SnObj) then begin
                 SPt^.X:=APt.X;
                 SPt^.Y:=Apt.Y;
              end;
           finally
              Dispose(AEll,Done);
           end;
        end;

        SPt:=APoly^.Data^.At(APoly^.Data^.Count-1);
        if SPt <> nil then begin
           AEll:=New(PEllipse,Init(SPt^,2,2,0));
           SnObj:=nil;
           try
              if AData^.Layers^.TopLayer^.SearchForPoint(AData^.PInfo,AEll,0,[ot_Poly,ot_CPoly,ot_Spline],APt,SnObj) then begin
                 SPt^.X:=APt.X;
                 SPt^.Y:=Apt.Y;
              end;
           finally
              Dispose(AEll,Done);
           end;
        end;

        //////////

        AData.InsertObject( APoly, FALSE );
        {$IFNDEF AXDLL} // <----------------- AXDLL
        if (DDEHandler.DoAutoIns) {and (DDEHandler.FDBAutoIn)} then AData.DBEditAuto( APoly );
        {$ENDIF} // <----------------- AXDLL
      end;  // if, object type
    end;
    AData^.SetCursor( crDefault );
  end;

Procedure ProcConvToPolygon ( AData: PProj );
  var
    AView       : PView;
    APoly       : PCPoly;
    Pnt         : TDPoint;
    iObj, nObj  : Integer;
    iVtx, nVtx  : Integer;
    iSeg, nSeg  : Integer;
    dVtx        : Double;
    AItem       : PIndex;
  begin
    Pnt.Init( 0, 0 );
    AData^.SetCursor( crHourGlass );
    nObj:= AData.Layers^.SelLayer^.Data^.GetCount;
    for iObj:= nObj-1 downto 0 do begin
      AView:= AData.Layers.IndexObject( AData.PInfo, AData.Layers.SelLayer.Data.At( iObj ) );
      if (AView.GetObjType in [ot_Circle, ot_Poly])  then begin

        AItem:= AData.Layers.TopLayer.HasObject( AView );
        if Assigned(AItem)  then AData.Layers.TopLayer.DeSelect( AData.PInfo, AItem );
        APoly:= New( PCPoly, Init );

        if (AView.GetObjType = ot_Circle)
        then begin
          with AData.PInfo.VariousSettings do begin
            if bConvRelMistake
            then  nVtx:= PEllipse(AView).ApproximatePointsNumber( dbConvRelMistake, True )
            else  nVtx:= PEllipse(AView).ApproximatePointsNumber( dbConvAbsMistake, False );
          end;
          if nVtx >= MaxCollectionSize  then nVtx:= MaxCollectionSize-1;
          for iVtx:=0 to nVtx-1 do begin
            Pnt:= PEllipse(AView).ApproximatePoint( iVtx, nVtx );
            APoly.InsertPoint( Pnt );
          end;
        end;

        if (AView.GetObjType = ot_Poly) then begin
          // Insert the polyline points
          nVtx:= PPoly(AView).Data.Count;
          for iVtx:=0 to nVtx-1 do begin
            Pnt:= PDPoint( PPoly(AView).Data.At( iVtx ) )^;
            APoly.InsertPoint( Pnt );
          end;
          APoly.ClosePoly;
        end;
        AData.InsertObject( APoly, TRUE );
        {$IFNDEF AXDLL} // <----------------- AXDLL
        if (DDEHandler.DoAutoIns) {and (DDEHandler.FDBAutoIn)} then AData.DBEditAuto( APoly );
        {$ENDIF} // <----------------- AXDLL
      end;  // if, object type
    end;
    AData^.SetCursor( crDefault );
  end;
//-- Glukhov Bug#192 Build#161 29.05.01

  {$IFNDEF AXDLL} // <----------------- AXDLL
Procedure ProcDBMonitoring
   (
   AData           : PProj
   );
  begin
       ProcDBSendCommand(AData,'MON');
  end;

Procedure ProcDKMAPPMonitoring(AData:PProj);
var
ProgisID: Integer;
DBValues: AnsiString;
AList: TStringList;
KG,GST: AnsiString;
i,n: Integer;
KGList,GSTList: TStringList;
begin
     n:=AData^.Layers^.SelLayer^.Data^.GetCount;
     KGList:=TStringList.Create;
     GSTList:=TStringList.Create;
     try
        for i:=0 to n-1 do begin
            KG:='';
            GST:='';
            ProgisID:=PIndex(AData^.Layers^.SelLayer^.Data^.At(i))^.Index;
            DBValues:=ProgisIDToDBValues(AData^.FName,PChar(AData^.Layers^.TopLayer^.Text^),ProgisID);
            if DBValues <> '' then begin
               AList:=TStringList.Create;
               try
                  DBValues:=StringReplace(DBValues,'|',#13#10,[rfReplaceAll]);
                  AList.Text:=DBValues;
                  KG:=AList.Values['KGNR'];
                  GST:=AList.Values['GSTNR'];
                  KGList.Add(KG);
                  GSTList.Add(GST);
               finally
                  AList.Free;
               end;
            end;
        end;
        if GSTList.Count > 0 then begin
           WinGISMainForm.SendDDEToDkmApp(KGList,GSTList);
        end;
     finally
        KGList.Free;
        GSTList.Free;
     end;
end;

Procedure ProcDBFreeProgableDDE
   (
   AData           : PProj;
   ANumber         : Word
   );
  var CommandStr   : String;
  begin
    case ANumber of
      mn_DBDDE0  : CommandStr:='MO0';
      mn_DBDDE1  : CommandStr:='MO1';
      mn_DBDDE2  : CommandStr:='MO2';
      mn_DBDDE3  : CommandStr:='MO3';
      mn_DBDDE4  : CommandStr:='MO4';
      mn_DBDDE5  : CommandStr:='MO5';
      mn_DBDDE6  : CommandStr:='MO6';
      mn_DBDDE7  : CommandStr:='MO7';
      mn_DBDDE8  : CommandStr:='MO8';
      mn_DBDDE9  : CommandStr:='MO9';
      mn_DBDDE10 : CommandStr:='M00';
      mn_DBDDE11 : CommandStr:='M01';
      mn_DBDDE12 : CommandStr:='M02';
      mn_DBDDE13 : CommandStr:='M03';
      mn_DBDDE14 : CommandStr:='M04';
      mn_DBDDE15 : CommandStr:='M05';
      mn_DBDDE16 : CommandStr:='M06';
      mn_DBDDE17 : CommandStr:='M07';
      mn_DBDDE18 : CommandStr:='M08';
      mn_DBDDE19 : CommandStr:='M09';
    end;
    ProcDBSendCommand(AData,CommandStr);
  end;

Procedure ProcDBSendCommand
   (
   AData           : PProj;
   AText           : String
   );
  var NameLength   : Integer;
      IndexStr     : String;
      HspSelected  : Boolean;
      ObjectsFound : Boolean;
  Procedure AddIndex
     (
     Item          : PIndex
     ); Far;
    begin
      DDEHandler.AppendList(Item^.Index);
    end;
  Procedure AddIndex3
     (
     Item          : PIndex
     ); Far;
    begin
      if Item <> nil then begin
         WingisMainForm.DXAppTCore.StringList.Add(IntToStr(Item^.Index));
      end;
    end;
  Procedure AddIndex1
     (
     Item          : PIndex
     ); Far;
    begin
      DDEHandler.AppendList1(Item^.Index,NameLength);
    end;
  Procedure AddIndex2
     (
     Item          : PLayer
     ); Far;
    var TestItem   : PIndex;
    Function HasSelected
       (
       Item        : PIndex
       ) : Boolean; Far;
      begin
        Result:=Item^.GetState(sf_Selected);
      end;
    Procedure DoAll
       (
       Item        : PIndex
       ); Far;
      begin
        if Item^.GetState(sf_Selected) then DDEHandler.AppendList1(Item^.Index,NameLength);
      end;
    begin
      TestItem:=Item^.Data^.FirstThat(@HasSelected);
      if TestItem <> NIL then begin
        if DDEHandler.FLayerInfo = 1 then begin
          NameLength:=Length(Item^.Text^);
          DDEHandler.StartList('['+AText+']['+Item^.Text^+'][   ]');
        end
        else begin
          Str(Item^.Index:10,IndexStr);
          DDEHandler.StartList('['+AText+']['+IndexStr+'][   ]');
        end;
        Item^.Data^.ForEach(@DoAll);
        DDEHandler.EndList1(FALSE,NameLength);
        ObjectsFound:=TRUE;
      end;
    end;
  begin
    if CoreConnected then begin
       WingisMainForm.DXAppTCore.StringList.Clear;
       if AData^.SelList^.GetCount > 0 then begin
          AData^.SelList^.ForEach(@AddIndex3);
          WingisMainForm.DXAppTCore.FireMONEvent;
       end;
       exit;
    end;

    AData^.SetCursor(crHourGlass);
    ObjectsFound:=FALSE;
    HspSelected:=FALSE;
    if AData^.Document.Loaded then begin                    
      if TGlobal(GetHeapMan.GlobalObj).SelLst.SelectCount > 0 then HspSelected:=TRUE;
    end;
    if DDEHandler.FLayerInfo = 0 then begin
      if AData^.SelList^.GetCount>0 then begin
        DDEHandler.StartList('['+AText+'][   ]');
        AData^.SelList^.ForEach(@AddIndex);
        if HspSelected then DDEHandler.EndList(FALSE)
        else DDEHandler.EndList(TRUE);
        ObjectsFound:=TRUE;
      end;
    end
    else if DDEHandler.FLayerInfo = 1 then begin
      if not AData^.Layers^.TranspLayer then begin
        NameLength:=Length(AData^.Layers^.TopLayer^.Text^);
        if AData^.SelList^.GetCount>0 then begin
          DDEHandler.StartList('['+AText+']['+AData^.Layers^.TopLayer^.Text^+'][   ]');
          AData^.SelList^.ForEach(@AddIndex1);
          if HspSelected then DDEHandler.EndList1(FALSE,NameLength)
          else DDEHandler.EndList1(TRUE,NameLength);
          ObjectsFound:=TRUE;
        end;
      end
      else begin
        AData^.Layers^.LData^.ForEach(@AddIndex2);
        if ObjectsFound and not HspSelected then DDEHandler.SendString('[END][]');
      end;
    end
    else if DDEHandler.FLayerInfo = 2 then begin
      NameLength:=10;
      if not AData^.Layers^.TranspLayer then begin
        Str(AData^.Layers^.TopLayer^.Index:10,IndexStr);
        if AData^.SelList^.GetCount>0 then begin
          DDEHandler.StartList('['+AText+']['+IndexStr+'][   ]');
          AData^.SelList^.ForEach(@AddIndex1);
          if HspSelected then DDEHandler.EndList1(FALSE,NameLength)
          else DDEHandler.EndList1(TRUE,NameLength);
          ObjectsFound:=TRUE;
        end;
      end
      else begin
        AData^.Layers^.LData^.ForEach(@AddIndex2);
        if ObjectsFound and not HspSelected then DDEHandler.SendString('[END][]');
      end;
    end;
    if AData^.Document.Loaded then begin
      if TSelList(GetHeapMan.SelList).Count > 0 then begin
        AData^.Document.Monitoring;
        ObjectsFound:=TRUE;
      end;
    end;
    if not ObjectsFound then begin
      if DDEHandler.FLayerInfo = 0 then begin
        DDEHandler.StartList('['+AText+'][   ]');
        DDEHandler.EndList(TRUE);
      end
      else if DDEHandler.FLayerInfo = 1 then begin
        NameLength:=Length(AData^.Layers^.TopLayer^.Text^);
        DDEHandler.StartList('['+AText+']['+AData^.Layers^.TopLayer^.Text^+'][   ]');
        DDEHandler.EndList1(TRUE,NameLength);
      end
      else if DDEHandler.FLayerInfo = 2 then begin
        NameLength:=10;
        Str(AData^.Layers^.TopLayer^.Index:10,IndexStr);
        DDEHandler.StartList('['+AText+']['+IndexStr+'][   ]');
        DDEHandler.EndList1(TRUE,NameLength);
      end;
    end;
    AData^.SetCursor(crDefault);
  end;

Procedure ProcDBDelete
   (
   AData           : PProj
   );
  Procedure DelLine
     (
     Item            : PChar
     ); Far;
    var Numbers      : Array[0..4096] of Char;
        Pos          : Integer;
        WorkStr      : String;
        Entries      : Integer;
        Error        : Integer;
        Cnt          : Integer;
        Item1        : Longint;
        SItem        : PIndex;
        AItem        : PView;
        DelOnAllLay  : Boolean;
        ResStr       : String;
        ResLInt      : LongInt;
        DelLayer     : PLayer;
        SFound       : Boolean;
        MStrs        : Boolean;
        StrToLong    : Boolean;
        MoreStrs     : Boolean;
        StrOK        : Boolean;
    Function CheckExist
       (
       BItem          : PIndex
       )
       : Boolean;
      begin
        Result:=AData^.Layers^.ExistsObject(BItem) >= 1;
      end;
    begin
      StrCopy(@Numbers,Item);
      if DDEHandler.FGotLInfo = 0 then begin
        DelOnAllLay:=TRUE;
        Pos:=6;
      end
      else if DDEHandler.FGotLInfo = 1 then begin
        StrOK:=ReadDDECommandString(Item,1,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ResStr,StrToLong,MoreStrs);
        if StrOK and (ResStr <> '') then begin
          DelOnAllLay:=FALSE;
          DelLayer:=AData^.Layers^.NameToLayer(ResStr);
        end
        else DelOnAllLay:=TRUE;
        Pos:=GetSepPos(Item,']',2,0);
      end
      else if DDEHandler.FLayerInfo = 2 then begin
        if ReadDDECommandLongInt(Item,1,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ResLInt,SFound,MStrs) then begin
          DelOnAllLay:=FALSE;
          DelLayer:=AData^.Layers^.IndexToLayer(ResLInt);
        end
        else DelOnAllLay:=TRUE;
        Pos:=GetSepPos(Item,']',2,0);
      end;
      SetLength(WorkStr,3);
      StrLCopy(@WorkStr[1],@Numbers[Pos],3);
      Val(WorkStr,Entries,Error);
      Pos:=Pos+5;
      SItem:=New(PIndex,Init(0));
      SetLength(WorkStr,10);
      for Cnt:=1 to Entries do begin
        StrLCopy(@WorkStr[1],@Numbers[Pos],10);
        Val(WorkStr,Item1,Error);
        SItem^.Index:=Item1;
        if DelOnAllLay then begin
          AData^.Layers^.DeleteIndex(AData^.PInfo,SItem);
          AItem:=AData^.Layers^.IndexObject(AData^.PInfo,SItem);
          if AItem <> NIL then begin
            AItem^.Invalidate(AData^.PInfo);
            PLayer(AData^.PInfo^.Objects)^.DeleteIndex(AData^.PInfo,SItem);
          end;
          AData^.MultiMedia^.DelMediasAtDel(Item1);
        end
        else begin
          if DelLayer <> NIL then begin
            if DelLayer^.DeleteIndex(AData^.PInfo,SItem) then begin
              AItem:=AData^.Layers^.IndexObject(AData^.PInfo,SItem);
              if AItem <> NIL then begin
                AItem^.Invalidate(AData^.PInfo);
                if not CheckExist(AItem) then begin
                  PLayer(AData^.PInfo^.Objects)^.DeleteIndex(AData^.PInfo,SItem);
                  AData^.MultiMedia^.DelMediasAtDel(Item1);
                end;
              end;
            end;
          end;
        end;
        Inc(Pos,12);
      end;
      Dispose(SItem,Done);
      AData^.PInfo^.RedrawInvalidate;
      AData^.SetModified;
      Item[0]:=DDEStrUsed;
    end;
  begin
    AData^.DeselectAll(FALSE);
{++ IDB} // This procedare makes Intenal database to receive deleting object's ID's.
         // The IDs are transmitted to Internal database in the next line (ForEach).
    {$IFNDEF WMLT}
    If WinGisMainForm.WeAreUsingTheIDB[AData] Then
       WingisMainForm.IDB_Man.GetReadyToReceiveDeletedObjectID(AData);
// ++ Cadmensky
    if WinGisMainForm.WeAreUsingUndoFunction[AData] then
       WingisMainForm.UndoRedo_Man.GetReadyToReceiveDeletedObjectID (AData);
// -- Cadmensky
    {$ENDIF}
{-- IDB}
    DDEHandler.DDEData^.ForEach(@DelLine);
{++ IDB} // This procedure delete information about deleting objects from an attributive table.
    {$IFNDEF WMLT}
    If WinGisMainForm.WeAreUsingTheIDB[AData] Then
       WingisMainForm.IDB_Man.ExecuteDeleting(AData);
// ++ Cadmensky
    if WinGisMainForm.WeAreUsingUndoFunction[AData] then
       WingisMainForm.UndoRedo_Man.ExecuteDeleting(AData);
// -- Cadmensky
    {$ENDIF}
{-- IDB}
    DDEHandler.DeleteDDEData;
  end;

Procedure ProcDBSetText
   (
   AData           : PProj
   );
  var OldTop       : PLayer;
      ParamPos     : Integer;
      ResStr       : String;
      ResLInt      : LongInt;
      DestLayer    : PLayer;
      StrError     : Boolean;
      Value        : Real;
      Error        : Integer;
      PosX         : LongInt;
      PosY         : LongInt;
      TextFontName : String;
      TextStyle    : LongInt;
      TextHeight   : LongInt;
      Angle        : Integer;
      NewText      : String;
      AFont        : TFontData;
      z            : Integer;
      DispTxHeight : LongInt;
      TextFont     : HFont;
      TextRect     : TRect;
      TempStr      : Array[0..255] of Char;
      OldFont      : HFont;
      DispWidth    : LongInt;
      DispHeight   : LongInt;
      Width        : LongInt;
      Height       : LongInt;
      AText        : PText;
      AAngle       : Real;
      SFound       : Boolean;
      MStrs        : Boolean;
      StrToLong    : Boolean;
      MoreStrs     : Boolean;
      StrFound     : Boolean;
      StrOK        : Boolean;
  begin
    OldTop:=AData^.Layers^.TopLayer;
    if DDEHandler.FGotLInfo = 0 then begin
      ParamPos:=1;
      DestLayer:=AData^.Layers^.TopLayer;
    end
    else if DDEHandler.FGotLInfo = 1 then begin
      ParamPos:=2;
      StrOK:=ReadDDECommandString(DDEHandler.DDEData^.At(0),1,0,WgBlockStart,
                                              WgBlockEnd,WgBlockEnd,ResStr,StrToLong,MoreStrs);
      if StrOK and (ResStr <> '') then begin
        DestLayer:=AData^.Layers^.NameToLayer(ResStr);
        if DestLayer <> NIL then AData^.Layers^.TopLayer:=DestLayer
        else DestLayer:=AData^.Layers^.TopLayer;
      end
      else DestLayer:=AData^.Layers^.TopLayer;
    end
    else if DDEHandler.FGotLInfo = 2 then begin
      ParamPos:=2;
      if ReadDDECommandLongInt(DDEHandler.DDEData^.At(0),1,0,WgBlockStart,
                                           WgBlockEnd,WgBlockEnd,ResLInt,SFound,MStrs) then begin
        DestLayer:=AData^.Layers^.IndexToLayer(ResLInt);
        if DestLayer <> NIL then AData^.Layers^.TopLayer:=DestLayer
        else DestLayer:=AData^.Layers^.TopLayer;
      end
      else DestLayer:=AData^.Layers^.TopLayer;
    end;
    StrError:=FALSE;
    StrOK:=ReadDDECommandReal(DDEHandler.DDEData^.At(0),ParamPos,0,WgBlockStart,      { X-Koordinate }
                                          DDEHandler.FDDESepSign,WgBlockEnd,Value,StrFound,MoreStrs);
    if StrOK then PosX:=Round(Value*100)
    else StrError:=TRUE;
    StrOK:=ReadDDECommandReal(DDEHandler.DDEData^.At(0),ParamPos,1,WgBlockStart,      { Y-Koordinate }
                                          DDEHandler.FDDESepSign,WgBlockEnd,Value,StrFound,MoreStrs);
    if StrOK then PosY:=Round(Value*100)
    else StrError:=TRUE;
    StrOK:=ReadDDECommandString(DDEHandler.DDEData^.At(0),ParamPos,2,WgBlockStart,    { FontName }
                                            DDEHandler.FDDESepSign,WgBlockEnd,TextFontName,StrToLong,MoreStrs);
    StrOK:=ReadDDECommandLongInt(DDEHandler.DDEData^.At(0),ParamPos,3,WgBlockStart,   { Fontstyle }
                                             DDEHandler.FDDESepSign,WgBlockEnd,TextStyle,SFound,MStrs);
    StrOK:=ReadDDECommandReal(DDEHandler.DDEData^.At(0),ParamPos,4,WgBlockStart,      { Texth�he }
                                          DDEHandler.FDDESepSign,WgBlockEnd,Value,StrFound,MoreStrs);
    if StrOK then begin


      if (Value > 0) and (Value <= MaxLongInt/100) then TextHeight:=Trunc(Value*100)
      else if AData^.ActualFont.Height > 0 then TextHeight:=AData^.ActualFont.Height
      else StrError:=TRUE;
    end
    else begin
      if AData^.ActualFont.Height > 0 then TextHeight:=AData^.ActualFont.Height
      else StrError:=TRUE;
    end;

    StrOK:=ReadDDECommandLongInt(DDEHandler.DDEData^.At(0),ParamPos,5,WgBlockStart,   { Winkel }
                                             DDEHandler.FDDESepSign,WgBlockEnd,ResLInt,SFound,MStrs);
    if ResLInt <= MaxInt then Angle:=ResLInt
    else Angle:=0;

    StrOK:=ReadDDECommandString(DDEHandler.DDEData^.At(0),ParamPos+1,0,WgBlockStart,  { Text }
                                            WgBlockEnd,WgBlockEnd,ResStr,StrToLong,MoreStrs);
    if StrOK and (ResStr <> '') then NewText:=ResStr
    else StrError:=TRUE;
    if not StrError then begin
      AFont.Init;
      AFont:=AData^.ActualFont;
      if TextFontName <> '' then begin
        AFont.Font:=AData^.PInfo^.Fonts^.IDFromName(TextFontName);
        if AFont.Font = 0 then AFont.Font:=AData^.ActualFont.Font;
      end;
      AFont.Style:=TextStyle;
      AFont.Height:=TextHeight;
      z:=0;
      if AData^.PInfo^.CalculateDisp(TextHeight) > 500000 then begin
        DispTxHeight:=Trunc(TextHeight/10000);
        z:=1;
      end
      else if AData^.PInfo^.CalculateDisp(TextHeight) > 5000 then begin
        DispTxHeight:=Trunc(TextHeight/100);
        z:=2;
      end
      else DispTxHeight:=TextHeight;
      TextFont:=AData^.PInfo^.GetFont(AFont,AData^.PInfo^.CalculateDisp(DispTxHeight),0);
      if TextFont > 0 then begin
        TextRect.Left:=0;
        TextRect.Top:=0;
        StrPCopy(TempStr,NewText);
        if StrComp(TempStr,'') <> 0 then begin
          OldFont:=SelectObject(AData^.PInfo^.ExtCanvas.Handle,TextFont);
          DrawText(AData^.PInfo^.ExtCanvas.Handle,TempStr,StrLen(TempStr),TextRect,dt_CalcRect);
          case z of
            1  : begin
                   DispWidth:=LongInt(TextRect.Right)*10000;
                   DispHeight:=LongInt(TextRect.Bottom)*10000;
                 end;
            2  : begin
                   DispWidth:=LongInt(TextRect.Right)*100;
                   DispHeight:=LongInt(TextRect.Bottom)*100;
                 end;
            else begin
                   DispWidth:=TextRect.Right;
                   DispHeight:=TextRect.Bottom;
                 end;
          end;
          if AData^.PInfo^.Zoom*DispWidth <= MaxLongInt then begin
            Width:=AData^.PInfo^.CalculateDraw(DispWidth);
            Height:=AData^.PInfo^.CalculateDraw(Abs(DispHeight));
            Angle:=360-Angle;
            while (Angle >= 360) do Angle:=Angle-360;
            while (Angle < 0) do Angle:=Angle+360;
            AAngle:=Angle*Pi/180;
            if (TextStyle and 1) <> 0 then begin
              PosX:=PosX-Round(Width/2*cos(AAngle));
              PosY:=PosY+Round(Width/2*sin(AAngle));
            end
            else if (TextStyle and 2) <> 0 then begin
              PosX:=PosX-Round(Width*cos(AAngle));
              PosY:=PosY+Round(Width*sin(AAngle));
            end;
            AText:=New(PText,Init(AFont,NewText,0));
            AText^.Pos.X:=PosX;
            AText^.Pos.Y:=PosY;
            AText^.Angle:=Angle;
            AText^.ClipRect.Init;
            if AText^.Angle = 0 then AText^.ClipRect.Assign(PosX,PosY-Abs(Height),PosX+Abs(Width),PosY)
            else begin
              with AText^.ClipRect do begin
                if (Angle >= 0) and (Angle < 90) then begin
                  A.X:=PosX-Round(Height*sin(AAngle));
                  A.Y:=PosY-Round(Width*sin(AAngle)+Height*cos(AAngle));
                  B.X:=PosX+Round(Width*cos(AAngle));
                  B.Y:=PosY;
                end
                else if (Angle >= 90) and (Angle < 180) then begin
                  A.X:=PosX-Round(Height*sin(AAngle)-Width*cos(AAngle));
                  A.Y:=PosY-Round(Width*sin(AAngle));
                  B.X:=PosX;
                  B.Y:=PosY-Round(Height*cos(AAngle));
                end
                else if (Angle >= 180) and (Angle < 270) then begin
                  A.X:=PosX+Round(Width*cos(AAngle));
                  A.Y:=PosY;
                  B.X:=PosX-Round(Height*sin(AAngle));
                  B.Y:=PosY-Round(Width*sin(AAngle)+Height*cos(AAngle));
                end
                else begin
                  A.X:=PosX;
                  A.Y:=PosY-Round(Height*cos(AAngle));
                  B.X:=PosX+Round(Width*cos(AAngle)-Height*sin(AAngle));
                  B.Y:=PosY-Round(Width*sin(AAngle));
                end;
              end;
            end;
            AText^.Width:=Abs(Width);
            if AData^.InsertObject(AText,FALSE) then begin                     {sl}
              AData^.CorrectSize(AText^.ClipRect,FALSE);
              NewText:='[NTX][';
              if DDEHandler.FLayerInfo = 1 then NewText:=NewText+AData^.Layers^.TopLayer^.Text^+']['
              else if DDEHandler.FLayerInfo = 2 then begin
                Str(AData^.Layers^.TopLayer^.Index:10,ResStr);
                NewText:=NewText+ResStr+'][';
              end;
              Str(AText^.Index:10,ResStr);
              NewText:=NewText+ResStr+']';
              DDEHandler.SendString(NewText);
            end;
          end
          else MsgBox(AData^.Parent.Handle,10406,mb_IconExclamation or mb_OK,'');
          SelectObject(AData^.PInfo^.ExtCanvas.Handle,OldFont);
          DeleteObject(TextFont);
        end;
      end;
    end;
    AData^.Layers^.TopLayer:=OldTop;
    if DDEHandler.DDEData^.GetCount > 0 then PChar(DDEHandler.DDEData^.At(0))[0]:=DDEStrUsed;
    DDEHandler.DeleteDDEData;
    DDEHandler.SetAnswerMode(FALSE);
  end;

Procedure ProcDBSetTextEx
   (
   AData           : PProj
   );
  var OldTop       : PLayer;
      ParamPos     : Integer;
      ResStr       : String;
      ResLInt      : LongInt;
      DestLayer    : PLayer;
      StrError     : Boolean;
      Value        : Real;
      Error        : Integer;
      PosX         : LongInt;
      PosY         : LongInt;
      TextFontName : String;
      TextStyle    : LongInt;
      TextHeight   : LongInt;
      Angle        : Integer;
      TextAlign    : Byte;
      NewText      : String;
      AFont        : TFontData;
      z            : Integer;
      DispTxHeight : LongInt;
      TextFont     : HFont;
      TextRect     : TRect;
      TempStr      : Array[0..255] of Char;
      OldFont      : HFont;
      DispWidth    : LongInt;
      DispHeight   : LongInt;
      Width        : LongInt;
      Height       : LongInt;
      AText        : PText;
      AAngle       : Real;
      SFound       : Boolean;
      MStrs        : Boolean;
      StrToLong    : Boolean;
      MoreStrs     : Boolean;
      StrFound     : Boolean;
      StrOK        : Boolean;
  begin
    OldTop:=AData^.Layers^.TopLayer;
    DestLayer:=AData^.Layers^.TopLayer;
    ParamPos:=1;
    StrError:=FALSE;
    StrOK:=ReadDDECommandReal(DDEHandler.DDEData^.At(0),ParamPos,0,WgBlockStart,      { X-Koordinate }
                                          DDEHandler.FDDESepSign,WgBlockEnd,Value,StrFound,MoreStrs);
    if StrOK then PosX:=Round(Value*100)
    else StrError:=TRUE;
    StrOK:=ReadDDECommandReal(DDEHandler.DDEData^.At(0),ParamPos,1,WgBlockStart,      { Y-Koordinate }
                                          DDEHandler.FDDESepSign,WgBlockEnd,Value,StrFound,MoreStrs);
    if StrOK then PosY:=Round(Value*100)
    else StrError:=TRUE;
    StrOK:=ReadDDECommandString(DDEHandler.DDEData^.At(0),ParamPos,2,WgBlockStart,    { FontName }
                                            DDEHandler.FDDESepSign,WgBlockEnd,TextFontName,StrToLong,MoreStrs);
    StrOK:=ReadDDECommandLongInt(DDEHandler.DDEData^.At(0),ParamPos,3,WgBlockStart,   { Fontstyle }
                                             DDEHandler.FDDESepSign,WgBlockEnd,TextStyle,SFound,MStrs);
    StrOK:=ReadDDECommandReal(DDEHandler.DDEData^.At(0),ParamPos,4,WgBlockStart,      { Texth�he }
                                          DDEHandler.FDDESepSign,WgBlockEnd,Value,StrFound,MoreStrs);
    if StrOK then begin


      if (Value > 0) and (Value <= MaxLongInt/100) then TextHeight:=Trunc(Value*100)
      else if AData^.ActualFont.Height > 0 then TextHeight:=AData^.ActualFont.Height
      else StrError:=TRUE;
    end
    else begin
      if AData^.ActualFont.Height > 0 then TextHeight:=AData^.ActualFont.Height
      else StrError:=TRUE;
    end;

    StrOK:=ReadDDECommandLongInt(DDEHandler.DDEData^.At(0),ParamPos,5,WgBlockStart,   { Winkel }
                                             DDEHandler.FDDESepSign,WgBlockEnd,ResLInt,SFound,MStrs);
    if ResLInt <= MaxInt then Angle:=ResLInt
    else Angle:=0;

    StrOK:=ReadDDECommandLongInt(DDEHandler.DDEData^.At(0),ParamPos,6,WgBlockStart,   { Align }
                                             DDEHandler.FDDESepSign,WgBlockEnd,ResLInt,SFound,MStrs);
    if StrOK then TextAlign:=ResLInt
    else TextAlign:=0;

    StrOK:=ReadDDECommandString(DDEHandler.DDEData^.At(0),ParamPos+1,0,WgBlockStart,  { Text }
                                            WgBlockEnd,WgBlockEnd,ResStr,StrToLong,MoreStrs);
    if StrOK and (ResStr <> '') then NewText:=ResStr
    else StrError:=TRUE;
    if not StrError then begin
      AFont.Init;
      AFont:=AData^.ActualFont;
      if TextFontName <> '' then begin
        AFont.Font:=AData^.PInfo^.Fonts^.IDFromName(TextFontName);
        if AFont.Font = 0 then AFont.Font:=AData^.ActualFont.Font;
      end;
      AFont.Style:=TextStyle;
      AFont.Height:=TextHeight;
      z:=0;
      if AData^.PInfo^.CalculateDisp(TextHeight) > 500000 then begin
        DispTxHeight:=Trunc(TextHeight/10000);
        z:=1;
      end
      else if AData^.PInfo^.CalculateDisp(TextHeight) > 5000 then begin
        DispTxHeight:=Trunc(TextHeight/100);
        z:=2;
      end
      else DispTxHeight:=TextHeight;
      TextFont:=AData^.PInfo^.GetFont(AFont,AData^.PInfo^.CalculateDisp(DispTxHeight),0);
      if TextFont > 0 then begin
        TextRect.Left:=0;
        TextRect.Top:=0;
        StrPCopy(TempStr,NewText);
        if StrComp(TempStr,'') <> 0 then begin
          OldFont:=SelectObject(AData^.PInfo^.ExtCanvas.Handle,TextFont);
          DrawText(AData^.PInfo^.ExtCanvas.Handle,TempStr,StrLen(TempStr),TextRect,dt_CalcRect);
          case z of
            1  : begin
                   DispWidth:=LongInt(TextRect.Right)*10000;
                   DispHeight:=LongInt(TextRect.Bottom)*10000;
                 end;
            2  : begin
                   DispWidth:=LongInt(TextRect.Right)*100;
                   DispHeight:=LongInt(TextRect.Bottom)*100;
                 end;
            else begin
                   DispWidth:=TextRect.Right;
                   DispHeight:=TextRect.Bottom;
                 end;
          end;
          if AData^.PInfo^.Zoom*DispWidth <= MaxLongInt then begin
            Width:=AData^.PInfo^.CalculateDraw(DispWidth);
            Height:=AData^.PInfo^.CalculateDraw(Abs(DispHeight));
            Angle:=360-Angle;
            while (Angle >= 360) do Angle:=Angle-360;
            while (Angle < 0) do Angle:=Angle+360;
            AAngle:=Angle*Pi/180;
            AText:=New(PText,Init(AFont,NewText,TextAlign));
            AText^.Pos.X:=PosX;
            AText^.Pos.Y:=PosY;
            AText^.Angle:=Angle;
            AText^.ClipRect.Init;
            if AText^.Angle = 0 then AText^.ClipRect.Assign(PosX,PosY-Abs(Height),PosX+Abs(Width),PosY)
            else begin
              with AText^.ClipRect do begin
                if (Angle >= 0) and (Angle < 90) then begin
                  A.X:=PosX-Round(Height*sin(AAngle));
                  A.Y:=PosY-Round(Width*sin(AAngle)+Height*cos(AAngle));
                  B.X:=PosX+Round(Width*cos(AAngle));
                  B.Y:=PosY;
                end
                else if (Angle >= 90) and (Angle < 180) then begin
                  A.X:=PosX-Round(Height*sin(AAngle)-Width*cos(AAngle));
                  A.Y:=PosY-Round(Width*sin(AAngle));
                  B.X:=PosX;
                  B.Y:=PosY-Round(Height*cos(AAngle));
                end
                else if (Angle >= 180) and (Angle < 270) then begin
                  A.X:=PosX+Round(Width*cos(AAngle));
                  A.Y:=PosY;
                  B.X:=PosX-Round(Height*sin(AAngle));
                  B.Y:=PosY-Round(Width*sin(AAngle)+Height*cos(AAngle));
                end
                else begin
                  A.X:=PosX;
                  A.Y:=PosY-Round(Height*cos(AAngle));
                  B.X:=PosX+Round(Width*cos(AAngle)-Height*sin(AAngle));
                  B.Y:=PosY-Round(Width*sin(AAngle));
                end;
              end;
            end;
            AText^.Width:=Abs(Width);
            if AData^.InsertObject(AText,FALSE) then begin                     {sl}
              AData^.CorrectSize(AText^.ClipRect,FALSE);
              NewText:='[NTX][';
              if DDEHandler.FLayerInfo = 1 then NewText:=NewText+AData^.Layers^.TopLayer^.Text^+']['
              else if DDEHandler.FLayerInfo = 2 then begin
                Str(AData^.Layers^.TopLayer^.Index:10,ResStr);
                NewText:=NewText+ResStr+'][';
              end;
              Str(AText^.Index:10,ResStr);
              NewText:=NewText+ResStr+']';
              DDEHandler.SendString(NewText);
            end;
          end
          else MsgBox(AData^.Parent.Handle,10406,mb_IconExclamation or mb_OK,'');
          SelectObject(AData^.PInfo^.ExtCanvas.Handle,OldFont);
          DeleteObject(TextFont);
        end;
      end;
    end;
    AData^.Layers^.TopLayer:=OldTop;
    if DDEHandler.DDEData^.GetCount > 0 then PChar(DDEHandler.DDEData^.At(0))[0]:=DDEStrUsed;
    DDEHandler.DeleteDDEData;
    DDEHandler.SetAnswerMode(FALSE);
  end;

Procedure ProcSendActualLayerToDB
   (
   AData           : PProj
   );
  var TmpLInt      : LongInt;
      TmpStr       : String;
      NTLayer      : PLayer;
  begin
    if AData^.Layers^.TranspLayer then DDEHandler.SendString('[ALA][]')
    else begin
      if DDEHandler.FLayerInfo <> 2 then DDEHandler.SendString('[ALA]['+AData^.Layers^.TopLayer^.Text^+']')
      else begin
        Str(AData^.Layers^.TopLayer^.Index:10,TmpStr);
        DDEHandler.SendString('[ALA]['+TmpStr+']');
      end;
    end;
    DDEHandler.SetAnswerMode(FALSE);
  end;

Procedure ProcSelectSymbolWithDlg(AData:PProj);
  var SymRollUp    : TSymbolRollupForm;
      LastSymbol   : LongInt;
      StrFound     : Boolean;
      MoreStrs     : Boolean;
      OldPar       : TWinControl;
      OldLeft      : Integer;
      OldTop       : Integer;
      OldWidth     : Integer;
      OldHeight    : Integer;
      OldEvent     : TNotifyEvent;
      MRes         : Integer;

      procedure SendSym(Item: PSGroup);
      var
      SendStr,TmpStr: String;
      begin
           if Item = nil then begin
                SendStr:='[TSY][0][][][-1][-1][-1][-1][-1]';
           end
           else begin
                SendStr:='[TSY]['+IntToStr(Item^.Index-600000000)+']['+Item^.Name^+'][]';

                if PSymbol(Item)^.Size < 1.5990221981798E-313 then
                   TmpStr:='-1'
                else
                   TmpStr:=FloatToStr(PSymbol(Item)^.Size);

                SendStr:=SendStr + '[' + TmpStr  + ']';

                if PSymbol(Item)^.ObjectStyle <> nil then begin
                   TmpStr:=IntToStr(PSymbol(Item)^.ObjectStyle^.FillStyle.Pattern);
                   SendStr:=SendStr + '[' + TmpStr  + ']';
                   TmpStr:=IntToStr(PSymbol(Item)^.ObjectStyle^.FillStyle.ForeColor);
                   SendStr:=SendStr + '[' + TmpStr  + ']';
                   TmpStr:=IntToStr(PSymbol(Item)^.ObjectStyle^.FillStyle.BackColor);
                   SendStr:=SendStr + '[' + TmpStr  + ']';
                   TmpStr:=IntToStr(PSymbol(Item)^.ObjectStyle^.LineStyle.Style);
                   SendStr:=SendStr + '[' + TmpStr  + ']';
                   TmpStr:=IntToStr(PSymbol(Item)^.ObjectStyle^.LineStyle.Color);
                   SendStr:=SendStr + '[' + TmpStr  + ']';
                end
                else SendStr:=SendStr + '[-1][-1][-1][-1][-1]';
           end;
           DDEHandler.SendString(SendStr);
           DDEHandler.SendString('[END][]');
      end;

  begin
    SymRollUp:=TSymbolRollupForm(UserInterface.RollupNamed['SymbolRollupForm']);
    if SymRollUp <> NIL then begin
       SymbolSelDlg:=TSymbolSelDlg.Create(WingisMainForm);
       OldPar:=SymRollup.SymbolList.Parent;
       OldLeft:=SymRollup.SymbolList.Left;
       OldTop:=SymRollup.SymbolList.Top;
       OldWidth:=SymRollup.SymbolList.Width;
       OldHeight:=SymRollup.SymbolList.Height;
       OldEvent:=SymRollup.SymbolList.OnDblClick;
       with SymbolSelDlg do begin
            SymRollup.SymbolList.OnDblClick:=SymbolSelDlg.FormDblClick;
            SymRollup.SymbolList.Parent:=SymbolSelDlg;
            SymRollup.SymbolList.Left:=8;
            SymRollup.SymbolList.Top:=8;
            SymRollup.SymbolList.Width:=Width-2*SymRollup.SymbolList.Left;
            SymRollup.SymbolList.Height:=Height-SymRollup.SymbolList.Top-25;
            SymRollup.SymbolList.ColCount:=SymRollup.SymbolList.ClientWidth Div SymRollup.SymbolList.CellWidth;
            SymRollup.ShowHint:=True;

            ClientWidth:=SymRollup.SymbolList.Width+16;
            ClientHeight:=SymRollup.SymbolList.Height+49;
            SymRollup.SymbolPopupMenu.AutoPopup:=False;

            OkBtn.Left:=ClientWidth-OkBtn.Width-8;
            LabelTT.Left:=8;
            LabelTT.Top:=OkBtn.Top+6;

       end;
       if SymbolSelDlg.ShowModal = mrOK then begin
          SendSym(AData^.ActualSym);
       end
       else begin
          SendSym(nil);
       end;

       SymRollup.SymbolPopupMenu.AutoPopup:=True;
       SymRollup.SymbolList.Parent:=OldPar;
       SymRollup.SymbolList.Left:=OldLeft;
       SymRollup.SymbolList.Top:=OldTop;
       SymRollup.SymbolList.Width:=OldWidth;
       SymRollup.SymbolList.Height:=OldHeight;
       SymRollup.SymbolList.OnDblClick:=OldEvent;
       SymbolSelDlg.Destroy;
       SymbolSelDlg:=nil;
    end;
end;
  {$ENDIF} // <----------------- AXDLL
{$ENDIF}

Procedure ProcAddLegendEntry(AData:PProj; LegName: String; EntryName: String);
var
ALegend: TLegend;
AEntry: TLegendEntry;
ALayer: PLayer;
begin
     ALegend:=AData^.Legends.LegendByName[LegName];
     if ALegend = nil then begin
        ALegend:=TLegend.Create;
        ALegend.Name:=LegName;
        ALegend.Title:=LegName;
     end;

     AEntry:=TLegendEntry.Create;
     AEntry.FText:=EntryName;

     ALayer:=AData^.Layers^.NameToLayer(EntryName);
     if ALayer <> nil then begin
        AEntry.FNameFromLayer:=True;
        AEntry.FLayerNumber:=ALayer^.Index;
     end;

     ALegend.Add(AEntry);
     AData^.Legends.AddLegend(ALegend);
     AData^.SetModified;
     UserInterface.Update([uiLegends]);
end;

Procedure ProcDelLegendEntry(AData:PProj; LegName:String; EntryName: String);
var
ALegend: TLegend;
AEntry: TLegendEntry;
i: Integer;
begin
     ALegend:=AData^.Legends.LegendByName[LegName];
     if ALegend <> nil then begin
        if EntryName = '' then begin
           if AData^.Legends.DeleteLegend(LegName) then begin
              AData^.SetModified;
           end;
        end
        else begin
           for i:=0 to ALegend.Count-1 do begin
               AEntry:=ALegend.Items[i];
               if AEntry.FText = EntryName then begin
                  ALegend.Delete(i);
                  AData^.SetModified;
                  break;
               end;
           end;
        end;
     end;
     UserInterface.Update([uiLegends]);
end;

Procedure ProcDBSetGTState(AData:PProj; ALayername:String; Active:Boolean);
var
ALayer: PLayer;
begin
     ALayer:=AData^.Layers^.NameToLayer(ALayername);
     if (ALayer <> nil) and (ALayer^.TooltipInfo.BDEOrLocal <> int_None) then begin
        if Active then begin
           AData^.TooltipLayer:=ALayer;
           TooltipThread.StartFloatingText(AData);
        end
        else begin
           if ALayer = AData^.TooltipLayer then begin
              TooltipThread.EndFloatingText;
              AData.TooltipLayer:=nil;
           end;
        end;
     end;
end;

Procedure ProcExplodeSelectedToPoints(AData: PProj; DestLayer: PLayer; ASym: PSGroup);

     procedure InsertPointObjects(AItem: PView);
     var
     APoint: TDPoint;
     ASymbol: PSymbol;
     APixel: PPixel;
     d,i,x,y: Integer;
     begin
          if AItem^.GetObjType in [ot_Poly,ot_CPoly] then begin
             d:=Ord(AItem^.GetObjType = ot_CPoly)+1;
             for i:=0 to PPoly(AItem)^.Data^.Count-d do begin
                 x:=PDPoint(PPoly(AItem)^.Data^.At(i))^.X;
                 y:=PDPoint(PPoly(AItem)^.Data^.At(i))^.Y;
                 APoint.Init(x,y);
                 if ASym = nil then begin
                    APixel:=New(PPixel,Init(APoint));
                    AData^.InsertObjectOnLayer(APixel,DestLayer^.Index);
                    {$IFNDEF WMLT}
                    {$IFNDEF AXDLL} // <----------------- AXDLL
                    if (DDEHandler.DoAutoIns) then begin
                       AData^.DBEditAuto(PIndex(APixel));
                    end;
                    {$ENDIF} // <----------------- AXDLL
                    {$ENDIF}
                 end
                 else begin
                    ASymbol:=New(PSymbol,InitName(AData^.PInfo,APoint,ASym^.GetName,ASym^.GetLibName,AData^.ActSymSize,Adata^.ActSymSizeType,AData^.ActSymAngle));
                    AData^.InsertObjectOnLayer(ASymbol,DestLayer^.Index);
                    {$IFNDEF WMLT}
                    {$IFNDEF AXDLL} // <----------------- AXDLL
                    if (DDEHandler.DoAutoIns) then begin
                       AData^.DBEditAuto(PIndex(ASymbol));
                    end;
                    {$ENDIF} // <----------------- AXDLL
                    {$ENDIF}
                 end;
             end;
             AData^.SetModified;
          end;
     end;

     procedure DoAll(AItem: PIndex);
     var
     SItem: PView;
     begin
          SItem:=PLayer(AData^.PInfo^.Objects)^.IndexObject(AData^.PInfo,AItem);
          InsertPointObjects(SItem);
     end;

begin
   AData^.SetCursor(crHourGlass);
   try
     if AData^.Layers^.SelLayer^.Data^.GetCount > 0 then begin
        AData^.Layers^.SelLayer^.Data^.ForEach(@DoAll);
     end
     else begin
        AData^.Layers^.TopLayer^.Data^.ForEach(@DoAll);
     end;
   finally
     AData^.SetCursor(crDefault);
   end;
end;

function ProcConvertSymToObj(AData: PProj; Idx: Integer; DestLayer: PLayer): Boolean;
var
SymGroup: PSGroup;
AItem: PIndex;
ASym: PSymbol;

Procedure DoAll(Item: PView);
var
AItem: PView;
ScaleFactor: Double;
begin
     AItem:=SymObjectToObject(AData,Item);
     if AItem <> nil then begin
        if (ASym^.SizeType = stDrawingUnits) then begin
           ScaleFactor:=ASym^.Size/100000;
        end
        else begin
           ScaleFactor:=ASym^.SymPtr^.DefaultSize/100000;
        end;
        AItem^.Scale(ScaleFactor,ScaleFactor);
        AItem^.MoveRel(ASym^.Position.X,ASym^.Position.Y);
        AData^.InsertObjectOnLayer(AItem,DestLayer^.Index);
        {$IFNDEF WMLT}
        {$IFNDEF AXDLL} // <----------------- AXDLL
        if (DDEHandler.DoAutoIns) then begin
           AData^.DBEditAuto(AItem);
        end;
        {$ENDIF} // <----------------- AXDLL
        {$ENDIF}
     end;
end;

begin
     Result:=False;
     if (Idx > 0) then begin
        if DestLayer = nil then begin
           DestLayer:=AData^.Layers^.TopLayer;
        end;
        AItem:=New(PIndex,Init(Idx));
        ASym:=PSymbol(PLayer(AData^.PInfo^.Objects)^.IndexObject(AData^.PInfo,AItem));
        if (ASym <> nil)
        and (ASym^.GetObjType = ot_Symbol)
        and ((ASym^.SizeType = stDrawingUnits) or (ASym^.SizeType = stProjectScale))
        and ((ASym^.Angle = 0) or (DblCompare(ASym^.Angle,dblFromObject) = 0)) then begin
           SymGroup:=ASym^.SymPtr;
           if SymGroup <> nil then begin
              SymGroup^.Data^.ForEach(@DoAll);
              Result:=True;
           end;
        end;
        if AItem <> nil then Dispose(AItem);
     end;
end;

function ProcPolyPointCut(AData: PProj; SLayer: PLayer; MLayer: PLayer; DLayer: PLayer; ASym: PSGroup): Boolean;

     function PointObjectExists(x,y: Integer): Boolean;
     var
     APoint: TDPoint;
     APixel: PPixel;
     Asymbol: PSymbol;
     AObj: PIndex;
     Circle: PEllipse;
     begin
          APoint.Init(x,y);
          Result:=False;
          Circle:=New(PEllipse,Init (APoint,1,1,0));
          if (MLayer^.SearchObjExactByPoint(AData^.PInfo,APoint,ot_Pixel) = nil)
          and (MLayer^.SearchObjExactByPoint(AData^.PInfo,APoint,ot_Symbol) = nil)
          and (MLayer^.SearchForPoint(AData^.PInfo,Circle,0,[ot_Poly,ot_CPoly],APoint,AObj) = False) then begin
             if ASym = nil then begin
                APixel:=New(PPixel,Init(APoint));
                AData^.InsertObjectOnLayer(APixel,DLayer^.Index);
                {$IFNDEF WMLT}
                {$IFNDEF AXDLL} // <----------------- AXDLL
                if (DDEHandler.DoAutoIns) then begin
                   AData^.DBEditAuto(PIndex(APixel));
                end;
                {$ENDIF} // <----------------- AXDLL
                {$ENDIF}
             end
             else begin
                ASymbol:=New(PSymbol,InitName(AData^.PInfo,APoint,ASym^.GetName,ASym^.GetLibName,AData^.ActSymSize,Adata^.ActSymSizeType,AData^.ActSymAngle));
                AData^.InsertObjectOnLayer(ASymbol,DLayer^.Index);
                {$IFNDEF WMLT}
                {$IFNDEF AXDLL} // <----------------- AXDLL
                if (DDEHandler.DoAutoIns) then begin
                   AData^.DBEditAuto(PIndex(ASymbol));
                end;
                {$ENDIF} // <----------------- AXDLL
                {$ENDIF}
             end;
             Result:=True;
          end;
          Dispose (Circle, Done);
     end;

     procedure DoAll(Item: PIndex);
     var
     i: Integer;
     x,y: Integer;
     AItem: PView;
     begin
          AItem:=SLayer.IndexObject(AData^.PInfo,Item);
          if AItem <> nil then begin
             if AItem^.GetObjType = ot_Poly then begin
                for i:=0 to PPoly(AItem)^.Data^.Count-1 do begin
                    x:=PDPoint(PPoly(AItem)^.Data^.At(i))^.x;
                    y:=PDPoint(PPoly(AItem)^.Data^.At(i))^.y;
                    PointObjectExists(x,y);
                end;
             end
             else if AItem^.GetObjType = ot_CPoly then begin
                for i:=0 to PCPoly(AItem)^.Data^.Count-2 do begin
                    x:=PDPoint(PCPoly(AItem)^.Data^.At(i))^.x;
                    y:=PDPoint(PCPoly(AItem)^.Data^.At(i))^.y;
                    PointObjectExists(x,y);
                end;
             end
             else if AItem^.GetObjType = ot_Pixel then begin
                x:=PPixel(AItem)^.Position.X;
                y:=PPixel(AItem)^.Position.Y;
                PointObjectExists(x,y);
             end
             else if AItem^.GetObjType = ot_Symbol then begin
                x:=PSymbol(AItem)^.Position.X;
                y:=PSymbol(AItem)^.Position.Y;
                PointObjectExists(x,y);
             end;
          end;
     end;

begin
     Result:=False;
     if (SLayer <> nil) and (MLayer <> nil) and (DLayer <> nil) then begin
        AData^.SetCursor(crHourGlass);
        try
           SLayer^.Data^.ForEach(@DoAll);
           Result:=True;
        finally
           AData^.SetCursor(crDefault);
        end;
     end;
end;

procedure ProcDBPolyPointCut(AData: PProj; s1,s2,s3,s4: String);
var
Layer1,Layer2,Layer3: PLayer;
SGroup: PSGroup;
begin
     Layer1:=AData^.Layers^.NameToLayer(s1);
     Layer2:=AData^.Layers^.NameToLayer(s2);
     Layer3:=AData^.Layers^.NameToLayer(s3);

     SGroup:=PSymbols(AData^.PInfo^.Symbols)^.GetSymbol(s4,'');

     if (Layer1 <> nil)
     and (Layer2 <> nil)
     and (Layer3 <> nil) then begin
         ProcPolyPointCut(AData,Layer1,Layer2,Layer3,SGroup);
     end;
end;

procedure ProcRotateSymbol(AData: PProj; ASym: PSymbol);
var
APoint: TDPoint;


   procedure DoAll(LItem: PLayer);

             function DoAllLines(AItem: PIndex): PIndex;
             var
             APos: TDPoint;
             AView: PView;
             PtIdx: Integer;
             x1,x2,y1,y2: Integer;
             AAngle: Double;
             begin
                  Result:=nil;
                  if (AItem^.GetObjType in [ot_Poly,ot_CPoly]) then begin
                     AView:=LItem^.IndexObject(AData^.PInfo,AItem);
                     if AView <> nil then begin
                        APos.Init(APoint.x,APoint.y);
                        Result:=PPoly(AView)^.SearchForLine(AData^.PInfo,APos,[ot_Poly,ot_CPoly],1,PtIdx);
                        if Result <> nil then begin
                           x1:=PDPoint(PPoly(AView)^.Data^.At(PtIdx))^.x;
                           y1:=PDPoint(PPoly(AView)^.Data^.At(PtIdx))^.y;
                           x2:=PDPoint(PPoly(AView)^.Data^.At(PtIdx+1))^.x;
                           y2:=PDPoint(PPoly(AView)^.Data^.At(PtIdx+1))^.y;
                           AAngle:=LineAngle(GrPoint(x1,y1),GrPoint(x2,y2));
                           ASym^.Angle:=AAngle;
                           ASym^.CalculateClipRect(AData^.PInfo);
                           ASym^.Invalidate(AData^.PInfo);
                           AData^.SetModified;
                        end;
                     end;
                  end;
             end;

   begin
        if LItem^.UseForSnap then begin
           LItem^.Data^.FirstThat(@DoAllLines);
        end;
   end;

begin
     if ASym <> nil then begin
        APoint.Init(ASym^.Position.x,ASym.Position.y);
        AData^.Layers^.LData^.ForEach(@DoAll);
     end;
end;

procedure ProcRotateSymbols(AData: PProj);

     procedure DoAll(AItem: PIndex);
     var
     Sym: PSymbol;
     begin
          if AItem^.GetObjType = ot_Symbol then begin
             Sym:=PSymbol(PLayer(AData^.PInfo^.Objects)^.IndexObject(AData^.PInfo,AItem));
             if Sym <> nil then begin
                ProcRotateSymbol(AData,Sym);
             end;
          end;
     end;

begin
     AData^.SetCursor(crHourGlass);
     try
        if AData^.Layers^.SelLayer^.Data^.GetCount > 0 then begin
           AData^.Layers^.SelLayer^.Data^.ForEach(@DoAll);
        end
        else begin
           AData^.Layers^.TopLayer^.Data^.ForEach(@DoAll);
        end;
     finally
        AData^.SetCursor(crDefault);
     end;
     if AData^.Modified then begin
        AData^.PInfo^.RedrawScreen(False);
     end;
end;

{******************************************************************************}

procedure ProcReplacePolyPoints(AData: PProj; ALineIndex: PIndex);
var
SPoly,DPoly: PView;
AIndex: PIndex;
i: Integer;
begin
     if (ALineIndex <> nil) and (ALineIndex^.GetObjType in [ot_Poly,ot_CPoly]) then begin
        AData^.SetActualMenu(mn_Select);
        SPoly:=PLayer(AData^.PInfo^.Objects)^.IndexObject(AData^.PInfo,ALineIndex);
        if SPoly <> nil then begin
           AData^.SetCursor(crHourGlass);
           try
              for i:=0 to AData^.IntList1.Count-1 do begin
                  AIndex:=New(PIndex,Init(AData^.IntList1.Items[i]));
                  try
                     DPoly:=PLayer(AData^.PInfo^.Objects)^.IndexObject(AData^.PInfo,AIndex);
                     if DPoly <> nil then begin
                        if DPoly^.GetObjType = ot_Poly then begin
                           ProcUpdatePolyByPoly(AData,DPoly,SPoly);
                           ProcPolyUpdated(AData,DPoly);
                        end
                        else begin
                           ProcUpdateCPolyByPoly(AData,DPoly,SPoly);
                           ProcPolyUpdated(AData,DPoly);
                        end;
                     end;
                  finally
                      Dispose(AIndex,Done);
                  end;
              end;
           finally
              AData^.SetCursor(crDefault);
           end;
        end;
     end;
end;

{******************************************************************************}

procedure ProcPolyUpdated(AData: PProj; AItem: PView);
begin
     PPoly(AItem)^.CalculateClipRect;
     AData^.PInfo^.RedrawScreen(False);
     AData^.SetModified;
     {$IFNDEF WMLT}
     {$IFNDEF AXDLL} // <----------------- AXDLL
     ProcDBSendObject(AData,PIndex(AItem),'UPD');
     {$ENDIF} // <----------------- AXDLL
     {$ENDIF}
end;

{******************************************************************************}

Procedure ProcUpdatePolyByPoly(AData: PProj; DItem,SItem: PView);
var
SPoly,DPoly: PPoly;
RPoly: PPoly;
i,k: Integer;
AIdx,SIdx1,SIdx2,DIdx1,DIdx2: Integer;
SPoint1,SPoint2,DPoint1,DPoint2: PDPoint;
CutCount: Integer;
Cut1,Cut2,CutPoint: TDPoint;
DirFlag: Boolean;

     procedure SwapPoints(var APoint,BPoint: TDPoint; var AIndex,BIndex: Integer);
     var
     CPoint: TDPoint;
     CIndex: Integer;
     begin
          CPoint.Init(APoint.X,APoint.Y);
          CIndex:=AIndex;
          APoint.Init(BPoint.X,BPoint.Y);
          AIndex:=BIndex;
          BPoint.Init(CPoint.X,CPoint.Y);
          BIndex:=CIndex;
     end;

begin
     SPoly:=PPoly(SItem);
     DPoly:=PPoly(DItem);
     if (SPoly <> nil) and (DPoly <> nil) and (DPoly <> SPoly) then begin
        RPoly:=New(PPoly,Init);
        CutCount:=0;
        for i:=0 to DPoly^.Data^.Count-2 do begin
            DPoint1:=DPoly^.Data^.At(i);
            DPoint2:=DPoly^.Data^.At(i+1);
            for k:=0 to SPoly^.Data^.Count-2 do begin
                SPoint1:=SPoly^.Data^.At(k);
                SPoint2:=SPoly^.Data^.At(k+1);
                if AData^.PInfo^.LineCut(True,SPoint1^,SPoint2^,DPoint1^,DPoint2^,CutPoint) then begin
                   if CutCount = 0 then begin
                      Cut1.Init(CutPoint.X,CutPoint.Y);
                      SIdx1:=k;
                      DIdx1:=i;
                   end
                   else begin
                      Cut2.Init(CutPoint.X,CutPoint.Y);
                      SIdx2:=k;
                      DIdx2:=i;
                   end;
                   Inc(CutCount);
                end;
            end;
        end;
        if CutCount > 1 then begin
           {$IFNDEF WMLT}
           {$IFNDEF AXDLL} // <----------------- AXDLL
           if (WinGisMainForm.WeAreUsingUndoFunction[AData]) and (WinGisMainForm.UndoRedo_Man.AddObjectInUndoData(AData,DItem)) then begin
              WinGisMainForm.UndoRedo_Man.SaveUndoData(AData,utUnChange);
           end;
           {$ENDIF} // <----------------- AXDLL
           {$ENDIF}
           if DIdx1 = DIdx2 then begin
              if (Cut1.Dist(DPoly^.Data^.At(DIdx1)^) > Cut2.Dist(DPoly^.Data^.At(DIdx1)^)) then begin
                 SwapPoints(Cut1,Cut2,SIdx1,SIdx2);
              end;
           end;
           DirFlag:=(SIdx1 < SIdx2);
           if DirFlag then begin
              for i:=0 to DIdx1 do begin
                  RPoly^.InsertPoint(DPoly^.Data^.At(i)^);
              end;
              RPoly^.InsertPoint(Cut1);
              for i:=SIdx1+1 to SIdx2 do begin
                  RPoly^.InsertPoint(SPoly^.Data^.At(i)^);
              end;
              RPoly^.InsertPoint(Cut2);
              for i:=DIdx2+1 to DPoly^.Data^.Count-1 do begin
                  RPoly^.InsertPoint(DPoly^.Data^.At(i)^);
              end;
           end
           else begin
              for i:=0 to DIdx1 do begin
                  RPoly^.InsertPoint(DPoly^.Data^.At(i)^);
              end;
              RPoly^.InsertPoint(Cut1);
              for i:=SIdx1 downto SIdx2+1 do begin
                  RPoly^.InsertPoint(SPoly^.Data^.At(i)^);
              end;
              RPoly^.InsertPoint(Cut2);
              for i:=DIdx2+1 to DPoly^.Data^.Count-1 do begin
                  RPoly^.InsertPoint(DPoly^.Data^.At(i)^);
              end;
           end;
           DPoly^.Data^.FreeAll;
           for i:=0 to RPoly^.Data^.Count-1 do begin
               DPoly^.InsertPoint(RPoly^.Data^.At(i)^);
           end;
        end;
        Dispose(RPoly,Done);
     end;
end;

{******************************************************************************}

procedure ProcUpdateCPolyByPoly(AData: PProj; DItem,SItem: PView);
var
RPoly,TmpPoly: PCPoly;
i,n,idx: Integer;
begin
     TmpPoly:=nil;
     RPoly:=ProcPrepareUpdateCPolyByPoly(AData,DItem,SItem);
     if RPoly <> nil then begin

        ProcUpdatePolyByPoly(AData,RPoly,SItem);
        {$IFNDEF WMLT}
        {$IFNDEF AXDLL} // <----------------- AXDLL
        if (WinGisMainForm.WeAreUsingUndoFunction[AData]) and (WinGisMainForm.UndoRedo_Man.AddObjectInUndoData(AData,DItem)) then begin
           WinGisMainForm.UndoRedo_Man.SaveUndoData(AData,utUnChange);
        end;
        {$ENDIF} // <----------------- AXDLL
        {$ENDIF}
        if PCPoly(DItem)^.GetState(sf_IslandArea) then begin
           if RPoly^.Data^.Count > PCPoly(DItem)^.IslandInfo^[0] then begin
              n:=Abs(RPoly^.Data^.Count - PCPoly(DItem)^.IslandInfo^[0]);
              for i:=0 to n-1 do begin
                  Idx:=0;
                  PCPoly(DItem)^.InsertPointByIndex(Idx,RPoly^.Data^.At(0)^);
              end;
           end;
           if RPoly^.Data^.Count < PCPoly(DItem)^.IslandInfo^[0] then begin
              n:=Abs(RPoly^.Data^.Count - PCPoly(DItem)^.IslandInfo^[0]);
              for i:=0 to n-1 do begin
                  Idx:=0;
                  PCPoly(DItem)^.DeletePointByIndex(Idx);
              end;
           end;
           for i:=0 to RPoly^.Data^.Count-1 do begin
               PDPoint(PCPoly(DItem)^.Data^.At(i))^.X:=PDPoint(RPoly^.Data^.At(i))^.X;
               PDPoint(PCPoly(DItem)^.Data^.At(i))^.Y:=PDPoint(RPoly^.Data^.At(i))^.Y;
           end;
           n:=PCPoly(DItem)^.Data^.Count-1;
           PDPoint(PCPoly(DItem)^.Data^.At(n))^.X:=PDPoint(PCPoly(DItem)^.Data^.At(0))^.X;
           PDPoint(PCPoly(DItem)^.Data^.At(n))^.Y:=PDPoint(PCPoly(DItem)^.Data^.At(0))^.Y;
        end
        else begin
           PCPoly(DItem)^.Data^.FreeAll;
           for i:=0 to RPoly^.Data^.Count-1 do begin
               PCPoly(DItem)^.InsertPoint(RPoly^.Data^.At(i)^);
           end;
        end;

        Dispose(RPoly,Done);
     end;
end;

{******************************************************************************}

function ProcPrepareUpdateCPolyByPoly(AData: PProj; DItem,SItem: PView): PCPoly;
var
SPoly: PPoly;
DPoly,RPoly: PCPoly;
SPoint1,SPoint2,DPoint1,DPoint2: PDPoint;
i,k,n,AIdx,CutCount: Integer;
APoint,CutPoint: TDPoint;
ADist,BDist: Double;

     function CalcCuts(Start,Stop: Integer): Integer;
     var
     i,k: Integer;
     begin
        Result:=0;
        for i:=0 to SPoly^.Data^.Count-2 do begin
            SPoint1:=SPoly^.Data^.At(i);
            SPoint2:=SPoly^.Data^.At(i+1);
            for k:=Start to Stop do begin
                DPoint1:=DPoly^.Data^.At(k);
                DPoint2:=DPoly^.Data^.At(k+1);
                if AData^.PInfo^.LineCut(True,SPoint1^,SPoint2^,DPoint1^,DPoint2^,CutPoint) then begin
                   Inc(Result);
                end;
            end;
        end;
     end;

     function DataCount: Integer;
     begin
          if DPoly^.IslandCount = 0 then begin
             Result:=DPoly^.Data^.Count;
          end
          else begin
             Result:=DPoly^.IslandInfo^[0];
          end;
     end;

begin
     Result:=nil;
     SPoly:=PPoly(SItem);
     DPoly:=PCPoly(DItem);
     if (SPoly <> nil) and (DPoly <> nil) and (DPoly <> SPoly) then begin
        RPoly:=New(PCPoly,Init);
        CutCount:=0;
        if DPoly^.IslandCount = 0 then begin
           CutCount:=CalcCuts(0,DPoly^.Data^.Count-2);
        end
        else begin
           CutCount:=CalcCuts(0,DPoly^.IslandInfo^[0]-2);
           AIdx:=DPoly^.IslandInfo^[0];
           for i:=1 to DPoly^.IslandCount-1 do begin
               n:=CalcCuts(AIdx,AIdx+DPoly^.IslandInfo^[i]-2);
               AIdx:=AIdx + DPoly^.IslandInfo^[i]+1;
               if n > 0 then begin
                  CutCount:=-1;
                  break;
               end;
           end;
        end;
        if CutCount > 1 then begin
           if SPoly^.ClipRect.PointInside(DPoly^.Data^.At(0)^) then begin
              APoint.Init(0,0);
              APoint:=SPoly^.GetCenter;
              ADist:=0;
              n:=0;
              for i:=0 to DataCount-1 do begin
                  BDist:=PDPoint(DPoly^.Data^.At(i))^.Dist(APoint);
                  if (BDist > ADist) and (SPoly^.FindIndexOfPoint(DPoly^.Data^.At(i)^) = -1) then begin
                     n:=i;
                     ADist:=BDist;
                  end;
              end;
              for i:=n to DataCount-1 do begin
                  RPoly^.InsertPoint(DPoly^.Data^.At(i)^);
              end;
              for i:=0 to n do begin
                  RPoly^.InsertPoint(DPoly^.Data^.At(i)^);
              end;
           end
           else begin
              for i:=0 to DataCount-1 do begin
                  RPoly^.InsertPoint(DPoly^.Data^.At(i)^);
              end;
           end;
           Result:=RPoly;
        end;
     end;
end;

end.


