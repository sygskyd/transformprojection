{******************************************************************************+
  Unit AM_Text
--------------------------------------------------------------------------------
  Author: Martin Forst
--------------------------------------------------------------------------------
  Modifikationen
  16.02.1994 Martin Forst
  07.06.1995 Martin Forst   Keine Ausblendung wenn Drucken
  19.02.1997 Raimund Leitner TSText f�r Texte in Symbolen
  22.11.2000 Yuriy Glukhov  Multi-line text
  01.08.2001 Yuriy Glukhov  Drawn-along text
+******************************************************************************}
unit AM_Text;

interface

uses WinTypes, WinProcs, AM_Index, AM_View, AM_Paint, AM_Def,
  SysUtils, AM_CPoly, AM_Font, Objects
  , AM_Poly // Glukhov Bug#468 Build#162 01.07.01
  , GrTools // Glukhov Bug#468 Build#166 16.09.01
  , AM_DPointColl
  ;

const
  cMaxTxtLen = 255;
  minDrawHeight = 10; { Minimale Gr��e in 1/10 mm, die noch als Text dargestellt wird }

//++ Glukhov Bug#468 Build#162 01.07.01
// Constants to define the object state ( fFlagSt )
  cwTxtStInitial = 0;
  cwTxtStCalculated = 1;
  cwTxtStChanged = 2;
// Constants to define the advanced mode ( fAdvMode )
  cwTxtAdvRegular = 0;
  cwTxtAdvSpaced = 1;
  cwTxtAdvSelPoly = 2;
  cwTxtAdvManPoly = 3;

// Constants to define the Vertical alignment for a text drawn along
  cwTxtAdvFVertAU = 0; // up
  cwTxtAdvFVertAM = 1; // middle
  cwTxtAdvFVertAD = 2; // down
  cwTxtAdvFVertAA = 3; // all bits
// Constants to define some bit flags
  cwTxtAdvFRevDir = 4; // reverse direction
//-- Glukhov Bug#468 Build#162 01.07.01

//++ Glukhov Bug#468 Build#162 01.07.01
type
      // To draw a text as a set of letters
  TTxtBrkData = class(TObject)
  public
        // Letters data
    Letters: PCollection;
        // Horizontal Space between letters as part of font height (0 - default)
    hSpace: Double;
        // Vertical Space between letters as part of font height (0 - default)
    vSpace: Double;
    constructor Create;
    destructor Destroy;
    procedure Assign(Src: TTxtBrkData);
    procedure CalculateClipRect(var Rect: TDRect);
    procedure Draw(PInfo: PPaint; Clipped: Boolean);

  end;

      // To draw a text along the polyline
  TTxtPolyData = class(TObject)
//      Protected
  public
      // Index of master object along which the text is drawn
    IdxPoly: LongInt;
      // Pointer to master object along which the text is drawn
    ObjPoly: PView;
        // Conversion accuracy when the source line is an arc (circle, spline)
    bConvA: Boolean; // Relative/Absolute
    dConvA: Double;
        // Polyline vertices to draw along
    Vertices: PDPointCollection;
        // Offset where the text starts as a part of polyline length (0 - default)
    Offset: Double;
        // Horizontal Shift from the polyline letter point (0 - default)
    hShift: Integer;
        // Vertical Shift from the polyline letter point (0 - default)
    vShift: Integer;
        // Flag to put the text above(0), on(1), or under(2) the line (see constants, 0 - default)
    fVerAl: Integer;
        // Flag to use the reverse direction of line (False - default)
    bReDir: Boolean;
    constructor Create;
    destructor Destroy;
        // Copies the source object
    procedure Assign(Src: TTxtPolyData);
        // Copies the source collection, creating the copies of vertices
    procedure CopyVertices(VsSrc, VsRes: PDPointCollection);
        // Copies the source collection in the reverse order, not creating the copies of vertices
    procedure ReverseVsDir(VsSrc, VsRes: PDPointCollection);
        // Creates the vertices to form a collection spaced from the given vertices collection
    procedure AddVertSpace(VsSrc: PDPointCollection; dSpace: Double; VsRes: PDPointCollection);

    function PolyLength(VsSrc: PDPointCollection): Double;
    procedure OffsetPosition(VsSrc: PDPointCollection; var Pnt: TDPoint; var dIdx: Double);
  end;
//-- Glukhov Bug#468 Build#162 01.07.01

type
  PText = ^TText;
  TText = object(TView)
    Pos: TDPoint;
    Angle: Integer16;
    Width: LongInt;
    Font: TFontData;
    Text: PString;
//++ Glukhov Bug#114 Build#125 15.11.00
    Height: LongInt; // height of text
    nLines: LongInt; // number of lines
    fTrim: Integer; // 0 - not trim
//-- Glukhov Bug#114 Build#125 15.11.00
//++ Glukhov TextVersion Build#151 14.02.01
    fFlagSt: Integer; // Object state (see constants)
//-- Glukhov TextVersion Build#151 14.02.01
//++ Glukhov Bug#468 Build#162 01.07.01
        // Advanced mode (see constants)
    fAdvMode: Integer;
        // The data that define the text as a set of letters
    TxtBrkData: TTxtBrkData;
        // The data that define the text drawn along the polyline
    TxtPolyData: TTxtPolyData;
//-- Glukhov Bug#468 Build#162 01.07.01

         //+++brovak
//         temporary blocked
    IsItAnnot: boolean;
    IsUseOffset: boolean; //use or not offset mode (one from 9)
    ObjectOffSetType: integer; //spec byte, if spec mode for master object selected
                                      //-1 default
    FAlign: Byte;
    constructor Init(AFont: TFontData; AText: string; AAlign: Byte);
        //changed by brovak
    constructor InitCalculate(PInfo: PPaint; AFont: TFontData; AText: string; APos: TDPoint; AAngle: Integer; AAlign: Byte);
//++ Glukhov Bug#468 Build#162 01.07.01
        // Inits and Copies the source data
    constructor InitBy(Src: PText);
        // Frees its data and Copies the source ones
    procedure Assign(Src: PText);
    procedure SetModeRegular;
    procedure SetModeSpaced(dHor, dVer: Double);
    procedure SetModeSelPoly(Idx: Integer; Obj: PView; dOff: Double);
    procedure SetModeManPoly(pV: PDPointCollection; dOff: Double);
    procedure CalcSpacedText(PInfo: PPaint);
    procedure CalcDefVertices;
    procedure CalcObjVertices(PInfo: PPaint);
    procedure CalcTextAlong(PInfo: PPaint);
    procedure CheckTextState(PInfo: PPaint);
    procedure UpdateTextData(PInfo: PPaint);
//-- Glukhov Bug#468 Build#162 01.07.01
//++ Glukhov Bug#468 Build#166 16.09.01
// Gets the object data to edit it, using the special component.
// P1 is a left lower corner of border, p4 is a left upper one.
// bSimilar is used only when bResize is True.
    procedure InitObjEdit(PInfo: PPaint; var P1, P2, P3, P4: TGrPoint;
      var bRotate, bResize, bSimilar, bMove: Boolean); virtual;
// Modifies the object after its editing, using the special component.
    procedure ExecObjEdit(PInfo: PPaint; P1, P2, P3, P4: TGrPoint); virtual;
//-- Glukhov Bug#468 Build#166 16.09.01
    constructor Load(S: TOldStream);
    constructor LoadOld(S: TOldStream);
    destructor Done; virtual;
    procedure CalculateClipRect(PInfo: PPaint);
    procedure Draw(PInfo: PPaint; Clipped: Boolean); virtual;
    function GetBorder: PCPoly; virtual;
    function GetObjType: Word; virtual;
    function GivePosX: LongInt; virtual;
    function GivePosY: LongInt; virtual;
    procedure MoveRel(XMove, YMove: LongInt); virtual;
    procedure Scale(XScale: Real; YScale: Real); virtual;
    function SelectBetweenPolys(PInfo: PPaint; Poly1, Poly2: Pointer; const ObjType: TObjectTypes;
      Inside: Boolean): Boolean; virtual;
    function SelectByCircle(PInfo: PPaint; Middle: TDPoint; Radius: LongInt;
      const ObjType: TObjectTypes; Inside: Boolean): Boolean; virtual;
    function SelectByPoint(PInfo: PPaint; Point: TDPoint; const ObjType: TObjectTypes): PIndex; virtual;
    function SelectByPoly(PInfo: PPaint; Poly: Pointer; const ObjType: TObjectTypes; Inside: Boolean): Boolean; virtual;
    function SelectByPolyLine(PInfo: PPaint; Poly: Pointer; const ObjType: TObjectTypes): Boolean; virtual;
//++ Glukhov Bug#468 Build#162 01.07.01
//        Procedure   SetNewText( PInfo: PPaint; AText: String; NFont: TFontData;ChangeWidth:Boolean;Redraw:Boolean);
    procedure SetNewText(PInfo: PPaint; AText: string; NFont: TFontData);
//-- Glukhov Bug#468 Build#162 01.07.01
    procedure Store(S: TOldStream); virtual;
    procedure SetText(const AText: string);
//++ Glukhov Bug#114 Build#125 15.11.00
// Calculate nLines, using Text
    procedure CalculateLines;
// Get a line from the multi-line text (Num from 0 to Nlines-1)
    function GetLine(Num: Integer): string;
// Calculate Width, Height, and nLines, using Text and Font
    procedure CalculateSize(PInfo: PPaint);
//-- Glukhov Bug#114 Build#125 15.11.00
//++ Glukhov Bug#387 Build#152 17.04.01
// Returns True, if the Text matches the given template
//       Function    TemplateMatched( sTpl: AnsiString; bSubStr, bCaseSens: Boolean ): Boolean;
    function TemplateMatched(sTpl: AnsiString; bSubStr, bCaseSens, bWildcards: Boolean): Boolean;
//-- Glukhov Bug#387 Build#152 17.04.01
    function GetULX: Integer;
    function GetULY: Integer;

    procedure SetAlign(AAlign: Byte);
  end;

  PSText = ^TSText; { Texte f�r Symbole }
  TSText = object(TText)
    procedure Draw(PInfo: PPaint; Clipped: Boolean); virtual;
    function GetBorder: PCPoly; virtual;
  end;

//++ Glukhov Bug#205 Build#128 24.11.00
// Returns True, if the given String matches the given template
function StrMatchesTemplate(sStr, sTpl: AnsiString): Boolean;
//-- Glukhov Bug#205 Build#128 24.11.00

//++ Glukhov Bug#291 Build#150 24.01.01
// Constants to define the trim mode
const
  cwTrimNone = 0;
  cwTrimHead = 1;
  cwTrimTail = 2;
  cwTrimLeft = 4;
  cwTrimRight = 8;
  cwTrimAll = cwTrimHead + cwTrimTail + cwTrimLeft + cwTrimRight;
  cwTrimNorm = cwTrimHead + cwTrimTail;

// Get a lines number
function CountLines(sSrc: AnsiString): Integer;
function CountLinesManually(sSrc: AnsiString): Integer;
// Removes the extra empty symbols
function TrimText(sSrc: AnsiString; wTrim: LongWord): AnsiString;
function TrimTextManually(sSrc: AnsiString; wTrim: LongWord): AnsiString;
// Get a line from the multi-line text
function GetALine(sSrc: AnsiString; Idx: Integer): AnsiString;
function GetALineManually(sSrc: AnsiString; Idx: Integer): AnsiString;
//-- Glukhov Bug#291 Build#150 24.01.01

implementation

uses Win32Def, Clipping, ExtCanvas, NumTools, MUModule, AM_Main
{$IFNDEF AXDLL} // <----------------- AXDLL
  , TxtSearchDlg // Glukhov Bug#205 Build#128 27.11.00
  , TxtReplacePrm // Glukhov Bug#387 Build#152 17.04.01
  , TxtReplaceDlg // Glukhov Bug#387 Build#152 17.04.01
{$ENDIF} // <----------------- AXDLL
  , Classes // Glukhov Bug#291 Build#150 26.01.01
  , AM_Proj // Glukhov TextVersion Build#151 14.02.01
  , AM_Layer // Glukhov Bug#468 Build#162 01.08.01
  , AM_Circl // Glukhov Bug#468 Build#162 16.08.01
  , AM_Splin // Glukhov Bug#468 Build#162 16.08.01
  , Helmert // Glukhov Bug#468 Build#162 16.09.01
  ;
//++ Glukhov TextVersion Build#151 14.02.01
// Text object version
const
  cTxtVerOriginal = 0;
  cTxtVerDrawAlong = 1; // Glukhov Bug#468 Build#162 01.08.01
  cTxtVerConvAnnot = 2; // Glukhov Bug#468 Build#162 01.08.01
  cTxtVerAdvConvAnnot = 3;
  //brovak
  cTxtVersion = cTxtVerAdvConvAnnot;
//-- Glukhov TextVersion Build#151 14.02.01

//++ Glukhov Bug#291 Build#150 23.01.01
// Get a lines number

function CountLines(sSrc: AnsiString): Integer;
var
  Strs: TStringList;
begin
  Result := 0;
  Strs := TStringList.Create;
  Strs.Text := sSrc;
  Result := Strs.Count;
  Strs.Destroy;
end;

// Get a line from the multi-line text

function GetALine(sSrc: AnsiString; Idx: Integer): AnsiString;
var
  Strs: TStringList;
begin
  Result := '';
  Strs := TStringList.Create;
  Strs.Text := sSrc;
  if (Idx >= 0) and (Idx < Strs.Count) then
    Result := Strs.Strings[Idx];
  Strs.Destroy;
end;

// Removes the extra empty symbols

function TrimText(sSrc: AnsiString; wTrim: LongWord): AnsiString;
var
  Strs: TStringList;
  i: Integer;
  s: string;
begin
  Result := sSrc;
  if wTrim = 0 then
    Exit;
  Strs := TStringList.Create;
  Strs.Text := sSrc;
  if (wTrim and cwTrimHead <> 0) then
  begin
    // Trim Head
    i := 0;
    while i < Strs.Count do
    begin
      s := Trim(Strs.Strings[i]);
      if s = '' then
        Strs.Delete(i)
      else
        Inc(i);
    end;
  end;
  if (wTrim and cwTrimTail <> 0) then
  begin
    // Trim Tail
    i := Strs.Count - 1;
    while i >= 0 do
    begin
      s := Trim(Strs.Strings[i]);
      if s = '' then
        Strs.Delete(i)
      else
        Dec(i);
    end;
  end;
  for i := 0 to Strs.Count - 1 do
  begin
    // Trim Left
    if (wTrim and cwTrimLeft <> 0) then
      Strs.Strings[i] := TrimLeft(Strs.Strings[i]);
    // Trim Right
    if (wTrim and cwTrimRight <> 0) then
      Strs.Strings[i] := TrimRight(Strs.Strings[i]);
  end;
  if Strs.Count > 0 then
    Result := Strs.Text
  else
    Result := '';
  Strs.Destroy;
end;

//-------------
// Get a lines number (another version)

function CountLinesManually(sSrc: AnsiString): Integer;
var
  i, len: Integer;
begin
  Result := 1;

  len := Length(sSrc);
  for i := 1 to len do
  begin
    if sSrc[i] = #13 then
      Inc(Result);
  end;
end;

// Get a line from the multi-line text (another version)

function GetALineManually(sSrc: AnsiString; Idx: Integer): AnsiString;
var
  Num, i, n, len: Integer;
  sTmp: AnsiString;
begin
  Result := '';
  Num := CountLinesManually(sSrc);
  if (Idx < 0) or (Idx >= Num) then
    Exit;

  if Num <= 1 then
  begin
    Result := sSrc;
    Exit;
  end;

// Search the line beginning
  sTmp := sSrc;
  for n := 1 to Idx do
  begin
  // Find the next <CR>
    i := Pos(#13, sTmp);
    if i = 0 then
      Exit; // Incorrect number of lines or another mistake
  // Find the next line beginning
    Inc(i);
    len := Length(sTmp);
    while i <= len do
    begin
      if (sTmp[i] <> #10) then
        Break;
      Inc(i);
    end;
    if i > len then
      Exit; // Return the (last) empty line
  // Cut the text beginning
    sTmp := Copy(sTmp, i, len - i + 1);
  end;

// Search the line end (Find the next <CR>)
  i := Pos(#13, sTmp);
  if i = 1 then
    Exit; // Return the empty line
  if i = 0
  {// No <CR>: Return the whole line} then
    Result := sTmp
  // Return the line till the <CR>
  else
    Result := Copy(sTmp, 1, i - 1);
end;

// Removes the extra empty symbols (another version)

function TrimTextManually(sSrc: AnsiString; wTrim: LongWord): AnsiString;
var
  Num, i: Integer;
  ifr, ito: Integer;
  sTmp: AnsiString;
begin
  Result := sSrc;
  if wTrim = cwTrimNone then
    Exit;

  Num := CountLinesManually(sSrc);
  ifr := 0;
  ito := Num - 1;

  if (wTrim and cwTrimHead) <> 0 then
  begin
    // Trim Head
    for i := 0 to Num - 1 do
    begin
      sTmp := GetALineManually(sSrc, i);
      if Trim(sTmp) <> '' then
        break;
      ifr := i + 1;
    end;
  end;
  if (wTrim and cwTrimTail) <> 0 then
  begin
    // Trim Tail
    for i := Num - 1 downto ifr do
    begin
      sTmp := GetALineManually(sSrc, i);
      if Trim(sTmp) <> '' then
        break;
      ito := i - 1;
    end;
  end;
  // Make a result
  Result := '';
  for i := ifr to ito do
  begin
    sTmp := GetALineManually(sSrc, i);
    // Trim Left
    if (wTrim and cwTrimLeft) <> 0 then
      sTmp := TrimLeft(sTmp);
    // Trim Right
    if (wTrim and cwTrimRight) <> 0 then
      sTmp := TrimRight(sTmp);
    Result := Result + sTmp;
    if i <> ito then
      Result := Result + #13#10;
  end;
end;

//-- Glukhov Bug#291 Build#150 23.01.01

//++ Glukhov Bug#468 Build#162 01.07.01
//-------------------------
// TTxtBrkData
//-------------------------

constructor TTxtBrkData.Create;
begin
  Letters := New(PCollection, Init(ciPoly, cePoly));
  hSpace := 0;
  vSpace := 0;
end;

destructor TTxtBrkData.Destroy;
begin
  Dispose(Letters, Done);
end;

procedure TTxtBrkData.Assign(Src: TTxtBrkData);
var
  i: Integer;
  Ptr: PText;
begin
  hSpace := Src.hSpace;
  vSpace := Src.vSpace;
  Letters.FreeAll;
  for i := 0 to Src.Letters.Count - 1 do
  begin
    Ptr := New(PText, InitBy(PText(Src.Letters.At(i))));
    Letters.Insert(Ptr);
  end;
end;

procedure TTxtBrkData.CalculateClipRect(var Rect: TDRect);
var
  i: Integer;
  Ptr: PText;
begin
  Rect.Init;
  for i := 0 to Letters.Count - 1 do
  begin
    Ptr := PText(Letters.At(i));
    Rect.CorrectByRect(Ptr.ClipRect);
  end;
end;

procedure TTxtBrkData.Draw(PInfo: PPaint; Clipped: Boolean);
var
  i: Integer;
  Ptr: PText;
begin
  for i := 0 to Letters.Count - 1 do
  begin
    Ptr := PText(Letters.At(i));
    Ptr.Draw(PInfo, Clipped);
  end;
end;

//-------------------------
// TTxtPolyData
//-------------------------

constructor TTxtPolyData.Create;
begin
  IdxPoly := 0;
  ObjPoly := nil;
  Vertices := New(PDPointCollection, Init(ciPoly, cePoly));
  Offset := 0;
  hShift := 0;
  vShift := 0;
  fVerAl := cwTxtAdvFVertAU;
  bReDir := False;
end;

destructor TTxtPolyData.Destroy;
begin
  Dispose(Vertices, Done);
end;

// Sets Vertices, copying the source collection

procedure TTxtPolyData.CopyVertices(VsSrc, VsRes: PDPointCollection);
var
  i: Integer;
  Pt1, Pt2: PDPoint;
begin
  if not Assigned(VsSrc) then
    VsSrc := Vertices;
  if not Assigned(VsRes) then
    VsRes := Vertices;

  if VsSrc = VsRes then
    Exit;
  VsRes.FreeAll;

  for i := 0 to VsSrc.Count - 1 do
  begin
    Pt1 := PDPoint(VsSrc.At(i));
    if i > 0 then
      if (Pt1.X = Pt2.X) and (Pt1.Y = Pt2.Y) then
        Continue;
    Pt2 := New(PDPoint, Init(Pt1.X, Pt1.Y));
    VsRes.Insert(Pt2);
  end;
end;

// Copies the source collection in the reverse order, not creating the copies of vertices

procedure TTxtPolyData.ReverseVsDir(VsSrc, VsRes: PDPointCollection);
var
  i, n: Integer;
  Ptr: Pointer;
begin
  if not Assigned(VsSrc) then
    VsSrc := Vertices;
  if not Assigned(VsRes) then
    VsRes := Vertices;

  if VsSrc = VsRes then
  begin
    n := VsSrc.Count - 1;
    for i := 1 to n do
    begin
      Ptr := VsRes.At(0);
      VsRes.AtDelete(0);
      VsRes.AtInsert(n - i + 1, Ptr);
    end;
  end
  else
  begin
    VsRes.FreeAll;
    n := VsSrc.Count - 1;
    for i := 0 to n do
      VsRes.Insert(VsSrc.At(n - i));
  end;
end;

// Creates the vertices to form a collection spaced from the given vertices collection

procedure TTxtPolyData.AddVertSpace(VsSrc: PDPointCollection; dSpace: Double; VsRes: PDPointCollection);
const
  AngEps = PI / 180;
var
  i, n, k: Integer;
  VsRev, VsCur: PDPointCollection;
  Pt1, Pt2, Pt3: PDPoint;
  Pn1: PDPoint;
  Ps1, Ps2, Ps3: TDPoint;
  dA1, dA2, dA3: Double;
  Gr1, Gr2, Gr3: TGrPoint;
begin
  if not Assigned(VsSrc) then
    VsSrc := Vertices;
  if not Assigned(VsRes) then
    VsRes := Vertices;

  if VsSrc = VsRes then
    Exit;
  VsRes.FreeAll;

  n := VsSrc.Count - 1;
  if (Abs(dSpace) < 1) or (n < 2) then
  begin
      // Copy the vertices
    CopyVertices(VsSrc, VsRes);
    Exit;
  end;

  VsRev := nil;
  VsCur := VsSrc;
  if dSpace < 0 then
  begin
      // Prepare the reverse line
    VsRev := New(PDPointCollection, Init(ciPoly, cePoly));
    ReverseVsDir(VsSrc, VsRev);
    VsCur := VsRev;
    dSpace := -dSpace;
  end;

  n := VsCur.Count - 1;
  for i := 0 to n do
  begin
    if i = 0 then
    begin
        // Special case for a first vertex
      Pt1 := PDPoint(VsCur.At(0));
      Pt2 := PDPoint(VsCur.At(1));
      dA1 := Pt1.CalculateAngle(Pt2^);
      Gr1 := GrPoint(Pt1.X, Pt1.Y);
      Gr2 := MovedPointAngle(Gr1, dSpace, dA1 + PI / 2);
      Pn1 := New(PDPoint, Init(Round(Gr2.X), Round(Gr2.Y)));
      VsRes.Insert(Pn1);
      Continue;
    end;

    Gr1 := GrPoint(Pt2.X, Pt2.Y);
    Gr2 := MovedPointAngle(Gr1, dSpace, dA1 + PI / 2);
    Ps1.Init(Round(Gr2.X), Round(Gr2.Y));

    if i = n then
    begin
        // Special case for a last vertex
      Pn1 := New(PDPoint, Init(Ps1.X, Ps1.Y));
      VsRes.Insert(Pn1);
      Break;
    end;
    Pt3 := PDPoint(VsCur.At(i + 1));
    dA2 := Pt2.CalculateAngle(Pt3^);

      // the angle inside of lines
    dA3 := PI + dA1 - dA2;
    dA3 := ProcAdjustAngleSlightly(dA3, 0, 2 * PI);
    if dA3 < (AngEps / 100) then
      dA3 := 2 * PI;
      // the bisector angle
    dA1 := dA2 + (dA3 / 2);

    if Abs(dA3 - PI) < AngEps then
    begin
        // the same direction, insert 1 point
      Pn1 := New(PDPoint, Init(Ps1.X, Ps1.Y));
      VsRes.Insert(Pn1);
    end
    else
    begin
      if dA3 > PI then
      begin
          // the right turn
        Gr2 := MovedPointAngle(Gr1, dSpace, dA1);
        Ps2.Init(Round(Gr2.X), Round(Gr2.Y));
        Gr2 := MovedPointAngle(Gr1, dSpace, dA2 + PI / 2);
        Ps3.Init(Round(Gr2.X), Round(Gr2.Y));
        if (Ps1.X <> Pn1.X) or (Ps1.Y <> Pn1.Y) then
        begin
          Pn1 := New(PDPoint, Init(Ps1.X, Ps1.Y));
          VsRes.Insert(Pn1);
        end;
        if (Ps2.X <> Pn1.X) or (Ps2.Y <> Pn1.Y) then
        begin
          Pn1 := New(PDPoint, Init(Ps2.X, Ps2.Y));
          VsRes.Insert(Pn1);
        end;
        if (Ps3.X <> Pn1.X) or (Ps3.Y <> Pn1.Y) then
        begin
          Pn1 := New(PDPoint, Init(Ps3.X, Ps3.Y));
          VsRes.Insert(Pn1);
        end;
      end
      else
      begin
          // the left turn
        Gr2 := MovedPointAngle(Gr1, dSpace / Sin(dA3 / 2), dA1);
        Ps2.Init(Round(Gr2.X), Round(Gr2.Y));
        if Pt2.Dist(Ps2) < Pt2.Dist(Pn1^) then
        begin
          Pn1 := New(PDPoint, Init(Ps2.X, Ps2.Y));
          VsRes.Insert(Pn1);
        end;
      end;
    end;

      // Prepare data for the next step
    Pt1 := Pt2;
    Pt2 := Pt3;
    dA1 := dA2;
  end;

  if Assigned(VsRev) then
  begin
      // Reverse the result and free the auxiliary data
    ReverseVsDir(VsRes, VsRes);
    VsRev.DeleteAll;
    Dispose(VsRev, Done);
  end;
end;

// Copies the source object

procedure TTxtPolyData.Assign(Src: TTxtPolyData);
begin
  IdxPoly := Src.IdxPoly;
  if IdxPoly <> 0 then
    ObjPoly := Src.ObjPoly
  else
    ObjPoly := nil; // a dumb, need to be done
  Offset := Src.Offset;
  hShift := Src.hShift;
  vShift := Src.vShift;
  fVerAl := Src.fVerAl;
  bReDir := Src.bReDir;
  CopyVertices(Src.Vertices, Vertices);
end;

function TTxtPolyData.PolyLength(VsSrc: PDPointCollection): Double;
var
  i: Integer;
  PPt1, PPt2: PDPoint;
begin
  if not Assigned(VsSrc) then
    VsSrc := Vertices;
  Result := 0;
  PPt2 := PDPoint(VsSrc.At(0));
  for i := 1 to VsSrc.Count - 1 do
  begin
    PPt1 := PPt2;
    PPt2 := PDPoint(VsSrc.At(i));
    Result := Result + PPt1.Dist(PPt2^);
  end;
end;

// Returns the Offset Position and its segment index
// (f.i., 1.5 means a middle of the segment 1)

procedure TTxtPolyData.OffsetPosition(VsSrc: PDPointCollection; var Pnt: TDPoint; var dIdx: Double);
var
  i: Integer;
  PPt1, PPt2: PDPoint;
  dL1, dL2: Double;
  d1, d2, d3: Double;
begin
  if not Assigned(VsSrc) then
    VsSrc := Vertices;
    // Init the default offset position
  PPt2 := PDPoint(VsSrc.At(0));
  Pnt.Init(PPt2.X, PPt2.Y);
  dIdx := 0;
  if Offset <= 0 then
    Exit;
  if Offset < 1 then
  begin
      // Calculate the offset position
    dL1 := PolyLength(VsSrc);
    dL2 := Offset * dL1;
    d2 := 0;
    for i := 1 to VsSrc.Count - 1 do
    begin
      PPt1 := PPt2;
      PPt2 := PDPoint(VsSrc.At(i));
      d1 := PPt1.Dist(PPt2^);
      d2 := d1 + d2;
      if d2 > dL2 then
      begin
        Pnt.Init(PPt1.X, PPt1.Y);
        if d1 > 0.5 then
        begin
          d2 := d2 - d1;
          d3 := (dL2 - d2) / d1;
          dIdx := i - 1 + d3;
          Pnt.Move(d3 * (PPt2.X - PPt1.X), d3 * (PPt2.Y - PPt1.Y));
        end;
        Exit;
      end;
    end;
  end;
    // Set the offset position at the end of line
  PPt2 := PDPoint(VsSrc.At(VsSrc.Count - 1));
  Pnt.Init(PPt2.X, PPt2.Y);
  dIdx := VsSrc.Count - 1;
end;

// Calculates point Pt2 to draw line Pt1Pt2 along the polyline
// given by collection of vertices pV,
// dIdx defines the parameter of Pt1 (at beginning) along the polyline
// and returns the parameter of Pt2 (as result)

procedure CalcPointAlong2Right(
  Pt1: TDPoint; iW: Integer; pV: PDPointCollection; var dIdx, dA: Double; var Pt2: TDPoint);
var
  i, j, n: Integer;
  dK: Double;
  d1, d2, d3: Double;
  PtB, PtE: TDPoint;
  PPt1, PPt2: PDPoint;
begin
  Pt2.Init(Pt1.X, Pt1.Y);
    // Index of the last vertex
  n := pV.Count - 1;
    // Index of the vertex that is a segment beginning
  j := Trunc(dIdx);
  if j >= n then
  begin
    // There are no more vertices, use direction of the last segment
    PPt1 := PDPoint(pV.At(n - 1));
    PPt2 := PDPoint(pV.At(n));
    dA := PPt1.CalculateAngle(PPt2^);
    Pt2.Move(iW * Cos(dA), iW * Sin(dA));
    Exit;
  end;
  i := j;
  PPt1 := PDPoint(pV.At(i));
  while i < n do
  begin
    PPt2 := PDPoint(pV.At(i + 1));
    d1 := PPt1.Dist(PPt2^);
    if d1 < 1 then
    begin
      // Skip the small segment
      Inc(i);
      Continue;
    end;
    d2 := Pt1.Dist(PPt2^);
//      if iW <= (d1+0.3*d2) then begin   // 0.3 is a possible variant only
    if iW <= d2 then
    begin
      // Pt2 is on the segment
      if i = j then
      begin
        // Pt2 is on the 1st segment
        dA := Pt1.CalculateAngle(PPt2^);
        Pt2.Move(iW * Cos(dA), iW * Sin(dA));
        d3 := PPt1.Dist(Pt2);
        dK := d3 / d1;
        dIdx := i + dK;
        Exit;
      end;
        // Pt2 is on the some segment - calculate it, using iterative technique
      PtB.Init(PPt1.X, PPt1.Y);
      PtE.Init(PPt2.X, PPt2.Y);
      dK := 0.5;
      dA := 0.5;
      repeat
        Pt2.Init((PtB.X + PtE.X) div 2, (PtB.Y + PtE.Y) div 2);
        d2 := Pt1.Dist(Pt2);
        if Abs(iW - d2) < 1.5 then
        begin // 1.5 allows a small mistake
          // That is it
          dA := Pt1.CalculateAngle(Pt2);
          dIdx := i + dK;
          Exit;
        end;
        dA := dA / 2;
        if iW < d2 then
        begin
          PtE := Pt2;
          dK := dK - dA;
        end
        else
        begin
          PtB := Pt2;
          dK := dK + dA;
        end;
      until False;
    end;
      // Try the next segment
    PPt1 := PPt2;
    Inc(i);
  end;

    // There are no more vertices, use direction from Pt1 to the last vertex
  PPt2 := PDPoint(pV.At(n));
  dA := Pt1.CalculateAngle(PPt2^);
  Pt2.Move(iW * Cos(dA), iW * Sin(dA));
  dIdx := n + 1;
end;

//-------------------------
// TText
//-------------------------
//++ Glukhov Bug#468 Build#162 01.07.01
// Inits and Copies the object data

constructor TText.InitBy(Src: PText);
begin
  TView.Init;
  Font.Init;
  Pos.Init(0, 0);
  Self := Src^;

  AttData := nil;
  TView.InitBy(Src);

  Text := nil;
  SetText(Src.Text^);

  if Assigned(Src.TxtBrkData) then
  begin
    TxtBrkData := TTxtBrkData.Create;
    TxtBrkData.Assign(Src.TxtBrkData);
  end
  else
  begin
    if Assigned(TxtBrkData) then
    begin
      TxtBrkData.Destroy;
      TxtBrkData := nil;
    end;
  end;

  if Assigned(Src.TxtPolyData) then
  begin
    TxtPolyData := TTxtPolyData.Create;
    TxtPolyData.Assign(Src.TxtPolyData);
  end
  else
  begin
    if Assigned(TxtPolyData) then
    begin
      TxtPolyData.Destroy;
      TxtPolyData := nil;
    end;
  end;

  FAlign := Src^.FAlign;

end;

// Copies the object data

procedure TText.Assign(Src: PText);
begin
  Done;
  InitBy(Src);
end;

procedure TText.SetModeRegular;
begin
  fAdvMode := cwTxtAdvRegular;
  fFlagSt := cwTxtStChanged;
end;

procedure TText.SetModeSpaced(dHor, dVer: Double);
begin
  if not Assigned(TxtBrkData) then
    TxtBrkData := TTxtBrkData.Create;
  TxtBrkData.hSpace := dHor;
  TxtBrkData.vSpace := dVer;
  TxtBrkData.Letters.FreeAll;
  fAdvMode := cwTxtAdvSpaced;
  fFlagSt := cwTxtStChanged;
end;

procedure TText.SetModeSelPoly(Idx: Integer; Obj: PView; dOff: Double);
var
  Poly: PCPoly;
  Pt1, Pt2: PDPoint;
begin
  if not Assigned(TxtPolyData) then
    TxtPolyData := TTxtPolyData.Create;
  if dOff < 0 then
    dOff := 0;
  if dOff > 1 then
    dOff := 1;
  TxtPolyData.Offset := dOff;

  TxtPolyData.IdxPoly := Idx;
  TxtPolyData.ObjPoly := Obj;
  TxtPolyData.Vertices.FreeAll;

  fAdvMode := cwTxtAdvSelPoly;
  fFlagSt := cwTxtStChanged;
end;

procedure TText.SetModeManPoly(pV: PDPointCollection; dOff: Double);
var
  Poly: PCPoly;
  Pt1, Pt2: PDPoint;
begin
  if not Assigned(TxtPolyData) then
    TxtPolyData := TTxtPolyData.Create;
  if dOff < 0 then
    dOff := 0;
  if dOff > 1 then
    dOff := 1;
  TxtPolyData.Offset := dOff;

  TxtPolyData.IdxPoly := 0;
  TxtPolyData.ObjPoly := nil;
    // The old line vertices are used, if the new ones are not assigned
  if Assigned(pV) then
    TxtPolyData.CopyVertices(pV, TxtPolyData.Vertices);
    // Create a default line, if it is degenerative
  if TxtPolyData.Vertices.Count <= 1 then
    CalcDefVertices;

  fAdvMode := cwTxtAdvManPoly;
  fFlagSt := cwTxtStChanged;
end;

procedure TText.CalcSpacedText(PInfo: PPaint);
var
  i, j, len: Integer;
  iW, iH, iHs: Integer;
  Ptr: PText;
  Pt1, Pt2: TDPoint;
  dCos, dSin: Double;
  sTmp: AnsiString;
begin
  dCos := Cos(-Angle * Pi / 180.0);
  dSin := Sin(-Angle * Pi / 180.0);
  iH := Font.Height;
  iHs := Round(iH * (1.0 + TxtBrkData.vSpace));
  TxtBrkData.Letters.FreeAll;

    // Find the base point for the text
  CalculateSize(PInfo);
  Pt1.Init(Pos.X, Pos.Y);
  if (Font.Style and ts_Right) <> 0 then
  begin
    Pt1.Move(Width * dCos, Width * dSin);
  end
  else
    if (Font.Style and ts_Center) <> 0 then
    begin
      Pt1.Move(Width * dCos / 2.0, Width * dSin / 2.0);
    end;

  Height := iH + iHs * (nLines - 1);
  for i := 0 to nLines - 1 do
  begin
    // for each text line
    sTmp := GetLine(i);
    len := Length(sTmp);
    if len > 0 then
    begin
        // not empty line
      iW := 0;
        // Create a text for every letter
      for j := 1 to len do
      begin
        Ptr := New(PText, Init(Font, '', 0));
        Ptr.fTrim := cwTrimNone;
        Ptr.SetText(Copy(sTmp, j, 1));
        Ptr.Angle := Angle;
        Ptr.CalculateSize(PInfo);
        iW := iW + Ptr.Width;
        TxtBrkData.Letters.Insert(Ptr);
      end;
        // Define line width
      iW := Round(iW + (len - 1) * iH * TxtBrkData.hSpace);
      if Width < iW then
        Width := iW;
        // Set position for the line beginning
      Pt2.Init(Pt1.X, Pt1.Y);
      if (Font.Style and ts_Right) <> 0 then
      begin
        Pt2.Move(-iW * dCos, -iW * dSin);
      end
      else
        if (Font.Style and ts_Center) <> 0 then
        begin
          Pt2.Move(-iW * dCos / 2.0, -iW * dSin / 2.0);
        end;
        // Set ClipRect and Position for every letter
      for j := TxtBrkData.Letters.Count - len to TxtBrkData.Letters.Count - 1 do
      begin
        Ptr := PText(TxtBrkData.Letters.At(j));
        Ptr.Pos := Pt2;
        Ptr.CalculateClipRect(PInfo);
        Ptr.fFlagSt := cwTxtStCalculated;
        Pt2.Move(
          (Ptr.Width + iH * TxtBrkData.hSpace) * dCos,
          (Ptr.Width + iH * TxtBrkData.hSpace) * dSin);
      end;
    end;
    Pt1.Move(iHs * dSin, -iHs * dCos);
  end;
end;

procedure TText.CalcDefVertices;
var
  Poly: PCPoly;
  Pt1, Pt2: PDPoint;
  iTmp: Integer;
begin
  if not Assigned(TxtPolyData) then
    Exit;
  TxtPolyData.Vertices.FreeAll;
    // Create a single line (part of text border) to draw along
  iTmp := fAdvMode;
  fAdvMode := cwTxtAdvRegular;
  Poly := GetBorder;
  fAdvMode := iTmp;
  Pt1 := PDPoint(Poly.Data.At(3));
  Pt2 := New(PDPoint, Init(Pt1.X, Pt1.Y));
  TxtPolyData.Vertices.Insert(Pt2);
  Pt1 := PDPoint(Poly.Data.At(2));
  Pt2 := New(PDPoint, Init(Pt1.X, Pt1.Y));
  TxtPolyData.Vertices.Insert(Pt2);
  Dispose(Poly, Done);
end;

procedure TText.CalcObjVertices(PInfo: PPaint);
var
  TmpItem: PIndex;
  nSeg, iSeg: Integer;
  nPnt, iPnt: Integer;
  Pnt: PDPoint;
begin
  if not Assigned(TxtPolyData) then
    Exit;

  if (fAdvMode = cwTxtAdvManPoly) then
  begin
    // Use as is now
    DetachMe; // Can not be attached
    Exit;
  end;

    // (fAdvMode = cwTxtAdvSelPoly)
  if not Assigned(TxtPolyData.ObjPoly) then
  begin
      // Set the object pointer
    if TxtPolyData.IdxPoly <> 0 then
    begin
      TmpItem := New(PIndex, Init(TxtPolyData.IdxPoly));
      TxtPolyData.ObjPoly := PLayer(PInfo.Objects).IndexObject(PInfo, TmpItem);
      Dispose(TmpItem, Done);
    end;
  end;
  if not Assigned(TxtPolyData.ObjPoly) then
  begin
      // Set the manual mode for an incorrect master object
    TxtPolyData.Vertices.FreeAll;
    fAdvMode := cwTxtAdvManPoly;
    DetachMe;
    Exit;
  end;

    // Check a master object
  TmpItem := GetObjMaster(PInfo);
  if Assigned(TmpItem) then
  begin
    if TmpItem.Index <> TxtPolyData.IdxPoly then
    begin
      DetachMe;
      TmpItem := nil;
    end;
  end;
  if not Assigned(TmpItem) then
    AttachToObject(PInfo, TxtPolyData.ObjPoly);

  if TxtPolyData.ObjPoly.GetObjType = ot_Poly then
  begin
    // Polyline
    TxtPolyData.CopyVertices(PPoly(TxtPolyData.ObjPoly).Data, TxtPolyData.Vertices);
    Exit;
  end;

  if TxtPolyData.ObjPoly.GetObjType = ot_Arc then
  begin
    // Convert Arc to Polyline
    with TxtPolyData do
    begin
      Vertices.FreeAll;
      nPnt := PEllipse(ObjPoly).ApproximatePointsNumber(dConvA, bConvA);
      for iPnt := 0 to nPnt - 1 do
      begin
        Pnt := New(PDPoint, Init(0, 0));
        Pnt^ := PEllipse(ObjPoly).ApproximatePoint(iPnt, nPnt);
        Vertices.Insert(Pnt);
      end;
    end;
    Exit;
  end;

  if TxtPolyData.ObjPoly.GetObjType = ot_Spline then
  begin
    // Convert Spline to Polyline
    with TxtPolyData do
    begin
      Vertices.FreeAll;
      nSeg := PSpline(ObjPoly).SegmentsNum;
        // for each spline segment
      for iSeg := 0 to nSeg - 1 do
      begin
        nPnt := PSpline(ObjPoly).ApprSegmPointsNumber(iSeg, dConvA, bConvA);
          // for each approximate point
        for iPnt := 0 to nPnt - 1 do
        begin
          Pnt := New(PDPoint, Init(0, 0));
          Pnt^ := PSpline(ObjPoly).ApprSegmPoint(iSeg, iPnt, nPnt);
          Vertices.Insert(Pnt);
        end;
      end;
    end;
    Exit;
  end;

end;

procedure TText.CalcTextAlong(PInfo: PPaint);
var
  i, j, n, len: Integer;
  iH, iW, iWs: Integer;
  iLb, iLe: Integer; // Indices of the first and last line letters
  dIdx, dA: Double;
  d1, d2, d3: Double;
  Pt1, Pt2: TDPoint;
  Ptr: PText;
  VsRev, VsAdd: PDPointCollection;
  VsCur: PDPointCollection;
  bOneDirection: Boolean;
  sTmp: AnsiString;
begin
    // Evaluate the letters size
  CalcSpacedText(PInfo);
  if (not Assigned(TxtPolyData))
    or (nLines > 1) then
  begin
    // The drawing along the polyline works for a single text only
    fAdvMode := cwTxtAdvSpaced;
    Exit;
  end;
    // Create a default line, if it is degenerative
  if TxtPolyData.Vertices.Count <= 1 then
    CalcDefVertices;

  VsCur := TxtPolyData.Vertices;
  iH := Font.Height;
  iWs := Round(iH * TxtBrkData.hSpace);

  VsRev := New(PDPointCollection, Init(ciPoly, cePoly));
  if TxtPolyData.bReDir then
  begin
    // Prepare the reverse line
    TxtPolyData.ReverseVsDir(VsCur, VsRev);
    VsCur := VsRev;
  end;
  VsAdd := New(PDPointCollection, Init(ciPoly, cePoly));
  dA := iH * TxtBrkData.vSpace;
  case TxtPolyData.fVerAl of
    cwTxtAdvFVertAD: dA := -dA;
    cwTxtAdvFVertAM: dA := 0;
  end;
  repeat
    VsAdd.FreeAll;
    TxtPolyData.AddVertSpace(VsCur, dA, VsAdd);
    n := VsAdd.Count - 1;
    dA := dA * 0.95;
  until n > 0;
  VsCur := VsAdd;

    // Calculate the offset position
  TxtPolyData.OffsetPosition(VsCur, Pt1, dIdx);
    // Init data to draw along
  Pt2.Init(Pt1.X, Pt1.Y);
  dA := 0;
  bOneDirection := False;

    //--------------------------------------------------
    // This variant works for the left aligned text only
    //--------------------------------------------------
  i := 0;
  iLb := 0;
  sTmp := GetLine(i);
  len := Length(sTmp);
  iLe := iLb + len - 1;
    // Loop for each line letter
  for j := iLb to iLe do
  begin
    Ptr := PText(TxtBrkData.Letters.At(j));
    iW := Ptr.Width;

    if not bOneDirection then
    begin
        // Calculate the segment index, text angle, and the 2nd point of letter
      CalcPointAlong2Right(Pt1, iW, VsCur, dIdx, dA, Pt2);
      if dIdx >= n then
        bOneDirection := True; // Keep one direction from this moment
    end
    else
    begin
        // Calculate the 2nd point of letter
      Pt2.Move(iW * Cos(dA), iW * Sin(dA));
    end;
      // Calculate the letter text position)
    Ptr.Pos.Init(Pt1.X, Pt1.Y);
    case TxtPolyData.fVerAl of
      cwTxtAdvFVertAU: Ptr.Pos.Move(-iH * Sin(dA), iH * Cos(dA));
      cwTxtAdvFVertAM: Ptr.Pos.Move(-iH * Sin(dA) / 2, iH * Cos(dA) / 2);
      // cwTxtAdvVerAliD:
    end;
    Ptr.Pos.Move(TxtPolyData.hShift, TxtPolyData.vShift);
      // Set the other letter text data
    Ptr.Angle := -Round(dA * 180 / Pi);
    Ptr.CalculateClipRect(PInfo);
    Ptr.fFlagSt := cwTxtStCalculated;

      // Calculate the space
    if iWs > 0 then
    begin
      if not bOneDirection then
      begin
          // Calculate the segment index, text angle, and the start of the next letter
        CalcPointAlong2Right(Pt2, iWs, VsCur, dIdx, dA, Pt2);
        if dIdx >= n then
          bOneDirection := True; // Keep one direction from this moment
      end
      else
      begin
          // Calculate the start of the next letter
        Pt2.Move(iWs * Cos(dA), iWs * Sin(dA));
      end;
    end;

      // Prepare the next letter position
    Pt1 := Pt2;
  end;
  VsAdd.FreeAll;
  Dispose(VsAdd, Done);
  VsRev.DeleteAll;
  Dispose(VsRev, Done);
end;

procedure TText.UpdateTextData(PInfo: PPaint);
begin
  fFlagSt := cwTxtStChanged;
  CheckTextState(PInfo);
end;

procedure TText.CheckTextState(PInfo: PPaint);
var
  iW, iH: LongInt;
  OldClipRect: TDRect;
begin
  if fFlagSt = cwTxtStCalculated then
    Exit;
    // Check mode
  if not Assigned(TxtPolyData) then
    fAdvMode := cwTxtAdvSpaced;
  if not Assigned(TxtBrkData) then
    fAdvMode := cwTxtAdvRegular;

  if fAdvMode <> cwTxtAdvRegular then
  begin
      // Advanced text
    case fAdvMode of
      cwTxtAdvSpaced: CalcSpacedText(PInfo);
      cwTxtAdvSelPoly,
        cwTxtAdvManPoly:
        begin
          CalcObjVertices(PInfo);
          CalcTextAlong(PInfo);
        end;
    end;
    CalculateClipRect(PInfo);
    fFlagSt := cwTxtStCalculated;
    Exit;
  end;

    // Regular text
  if fFlagSt = cwTxtStChanged then
  begin
      // Recalculate Size
    CalculateSize(PInfo);
    CalculateClipRect(PInfo);
  end
  else
    if (nLines = 0) or (fFlagSt = cwTxtStInitial) then
    begin
      // Recalculate Size
      iW := Width;
      iH := Height;
      CalculateSize(PInfo);
      CalculateClipRect(PInfo);
      // Align the new ClipRect about the old one, if necessary
      if (iW <> Width) or (iH <> Height) then
      begin
        // Save the old clip rectangle
        OldClipRect.InitByRect(ClipRect);
        CalculateClipRect(PInfo);
        if (Font.Style and (ts_Center or ts_Right)) <> 0 then
        begin
          // Calculate correction for Pos and ClipRect
          iW := Round(OldClipRect.XSize - ClipRect.XSize);
          iH := Round(OldClipRect.YSize - ClipRect.YSize);
          if (Font.Style and ts_Center) <> 0 then
          begin
            iW := iW div 2;
            iH := iH div 2;
          end;
          if (Pos.X = OldClipRect.A.X) then
          begin
            ClipRect.MoveRel(iW, iH);
            Pos.Move(iW, iH);
          end
          else
            if (Pos.Y = OldClipRect.A.Y) then
            begin
              ClipRect.MoveRel(-iW, iH);
              Pos.Move(-iW, iH);
            end
            else
              if (Pos.X = OldClipRect.B.X) then
              begin
                ClipRect.MoveRel(-iW, -iH);
                Pos.Move(-iW, -iH);
              end
              else
                if (Pos.Y = OldClipRect.B.Y) then
                begin
                  ClipRect.MoveRel(iW, -iH);
                  Pos.Move(iW, -iH);
                end;
        end;
//++ Glukhov Bug#581 Build#162 20.11.01
//        if Assigned( PInfo.AProj )  then PProj(PInfo.AProj).SetModified;
        if Assigned(PInfo.AProj) then
        begin
          PProj(PInfo.AProj).SetModified;
          PProj(PInfo.AProj).UpdateClipRect(@Self); // IDB_Warning!!!
        end;
//-- Glukhov Bug#581 Build#162 20.11.01
      end;
    end;
  fFlagSt := cwTxtStCalculated;
end;
//-- Glukhov Bug#468 Build#162 01.07.01

constructor TText.Init(AFont: TFontData; AText: string; AAlign: Byte);
begin
  TView.Init;
  Text := nil;
  Font.Init;
  Pos.Init(0, 0);
//++ Glukhov Bug#291 Build#150 24.01.01
  Angle := 0;
  Width := 0;
  Font := AFont;
  if Font.Height < cMinFontHeight then
    Font.Height := cMinFontHeight;
  Height := 0;
  nLines := 0;
  fTrim := cwTrimNorm;
//    SetText(AText);
  SetText(TrimTextManually(AText, fTrim));
//-- Glukhov Bug#291 Build#150 24.01.01
//++ Glukhov TextVersion Build#151 14.02.01
  fFlagSt := cwTxtStInitial; // needs refresh of data before drawing
//-- Glukhov TextVersion Build#151 14.02.01
//++ Glukhov Bug#468 Build#162 01.07.01
  fAdvMode := cwTxtAdvRegular;
  TxtBrkData := nil;
  TxtPolyData := nil;
//-- Glukhov Bug#468 Build#162 01.07.01
    //brovak
//    temporary closed
  IsItAnnot := false;
  IsUseOffset := false;
  ObjectOffSetType := -1;
  FAlign := AAlign;
end;

//++ Glukhov Bug#114 Build#125 15.11.00
// I need function Pos

function MyPos(Substr: string; S: string): Integer;
begin
  Result := Pos(Substr, S);
end;

// Calculate NLines, using Text

procedure TText.CalculateLines;
var
  sRes: AnsiString;
begin
  if not Assigned(Text) then
  begin
    nLines := 0;
    Exit;
  end;
  sRes := TrimTextManually(Text^, fTrim);
  nLines := CountLinesManually(sRes);
  {
  nLines:= CountLinesManually( Text^ );
  if nLines = 1  then sRes:= Trim( Text^ )
  else begin
    sRes:= TrimTextManually( Text^, fTrim );
    nLines:= CountLinesManually( sRes );
    if nLines = 1  then sRes:= Trim( sRes );
  end;
  }
  SetText(sRes);
end;

// Get a line from the multi-line text (Num from 0 to Nlines-1)

function TText.GetLine(Num: Integer): string;
var
  i, n, len: Integer;
  sTmp: AnsiString;
begin
  Result := GetALineManually(Text^, Num);
end;

// Calculate Width, Height, and NLines, using Text and Font

procedure TText.CalculateSize(PInfo: PPaint);
begin
  if not Assigned(Text) then
  begin
    Width := 0;
    Height := 0;
    NLines := 0;
    Exit;
  end;
  CalculateLines;
  if Font.Height < cMinFontHeight then
    Font.Height := cMinFontHeight;
  Height := Font.Height;
  PInfo.GetTextSize(Font, Text^, Width, Height);
end;
//-- Glukhov Bug#114 Build#125 15.11.00

procedure TText.SetText(const AText: string);
begin
  DisposeStr(Text);
  Text := NewStr(AText);
end;

constructor TText.InitCalculate(PInfo: PPaint; AFont: TFontData; AText: string; APos: TDPoint; AAngle: Integer; AAlign: Byte);
begin
  Init(AFont, AText, AAlign);
    //brovak
  if aangle <> 0 then
    Angle := Aangle;
//++ Glukhov Bug#114 Build#125 15.11.00
  CalculateSize(PInfo);
//-- Glukhov Bug#114 Build#125 15.11.00
  Pos := APos;
  CalculateClipRect(PInfo);
end;

//++ Glukhov Bug#468 Build#162 01.07.01

procedure TText.CalculateClipRect(PInfo: PPaint); // PInfo is not used
var
  Border: PCPoly;
begin
  if not Assigned(TxtBrkData) then
    fAdvMode := cwTxtAdvRegular;
  if (fAdvMode = cwTxtAdvRegular)
    or (fAdvMode = cwTxtAdvSpaced) then
  begin
    Border := GetBorder;
    ClipRect := Border^.ClipRect;
    Dispose(Border, Done);
  end
  else
  begin
    TxtBrkData.CalculateClipRect(ClipRect);
  end;
end;
//-- Glukhov Bug#468 Build#162 01.07.01

constructor TText.LoadOld(S: TOldStream);
begin
  Text := nil;
  TView.Load(S);
  Pos.Load(S);
  S.Read(Angle, SizeOf(Angle));
  S.Read(Width, SizeOf(Width));
  Font.Init;
  with Font do
  begin
    S.Read(Font, SizeOf(Font));
    S.Read(Style, SizeOf(Style));
    S.Read(Height, SizeOf(Height));
  end;
  if ProjectVersion < 12 then
  begin
    while Angle > 360 do
      Angle := Angle - 360;
    while Angle < -360 do
      Angle := Angle + 360;
  end;
//++ Glukhov Bug#291 Build#150 24.01.01
  if Font.Height < cMinFontHeight then
    Font.Height := cMinFontHeight;
  fTrim := cwTrimNorm;
  CalculateLines;
  Height := nLines * Font.Height;
//-- Glukhov Bug#291 Build#150 24.01.01
//++ Glukhov TextVersion Build#151 14.02.01
  fFlagSt := cwTxtStInitial; // needs refresh of data before drawing
//-- Glukhov TextVersion Build#151 14.02.01
//++ Glukhov Bug#468 Build#162 01.07.01
  fAdvMode := cwTxtAdvRegular;
  TxtBrkData := nil;
  TxtPolyData := nil;
  FAlign := 0;
//-- Glukhov Bug#468 Build#162 01.07.01
end;

constructor TText.Load(S: TOldStream);
var
  Version: Word;
//++ Glukhov Bug#468 Build#162 01.08.01
  i, n, ix, iy: Integer;
  Pnt: PDPoint;
//-- Glukhov Bug#468 Build#162 01.08.01
begin
  Text := nil;
  S.Read(Version, SizeOf(Version));
  TView.Load(S);
  if State and sf_NewVersion = 0 then
  begin
    Pos.Load(S);
    S.Read(Angle, SizeOf(Angle));
    S.Read(Width, SizeOf(Width));
    Font.Load(S);
  end
  else
  begin
    Text := S.ReadStr;
    Pos.Load(S);
    S.Read(Angle, SizeOf(Angle));
    S.Read(Width, SizeOf(Width));
    Font.Load(S);
  end;
//++ Glukhov Bug#291 Build#150 24.01.01
  if Font.Height < cMinFontHeight then
    Font.Height := cMinFontHeight;
  fTrim := cwTrimNorm;
  CalculateLines;
  Height := nLines * Font.Height;
//-- Glukhov Bug#291 Build#150 24.01.01
//++ Glukhov TextVersion Build#151 14.02.01
  fFlagSt := cwTxtStInitial; // needs refresh of data before drawing
//-- Glukhov TextVersion Build#151 14.02.01
//++ Glukhov Bug#468 Build#162 01.08.01
  if Version < cTxtVerDrawAlong then
    fAdvMode := cwTxtAdvRegular
  else
  begin
    S.Read(fAdvMode, SizeOf(fAdvMode));
    FAlign := fAdvMode shr 16;
    fAdvMode := fAdvMode and $FFFF;
  end;
  TxtBrkData := nil;
  TxtPolyData := nil;
    // Check for an advanced mode
  if fAdvMode <> cwTxtAdvRegular then
  begin
    TxtBrkData := TTxtBrkData.Create;
    S.Read(TxtBrkData.hSpace, SizeOf(TxtBrkData.hSpace));
    S.Read(TxtBrkData.vSpace, SizeOf(TxtBrkData.vSpace));
  end;
  if (fAdvMode = cwTxtAdvSelPoly)
    or (fAdvMode = cwTxtAdvManPoly) then
  begin
    TxtPolyData := TTxtPolyData.Create;
    if fAdvMode = cwTxtAdvSelPoly then
    begin
      S.Read(TxtPolyData.IdxPoly, SizeOf(TxtPolyData.IdxPoly));
      S.Read(TxtPolyData.bConvA, SizeOf(TxtPolyData.bConvA));
      S.Read(TxtPolyData.dConvA, SizeOf(TxtPolyData.dConvA));
    end
    else
    begin
      TxtPolyData.IdxPoly := 0;
        // Get count of vertices
      S.Read(n, SizeOf(n));
        // Get vertices
      for i := 0 to n - 1 do
      begin
        S.Read(ix, SizeOf(ix));
        S.Read(iy, SizeOf(iy));
        Pnt := New(PDPoint, Init(ix, iy));
        TxtPolyData.Vertices.Insert(Pnt);
      end;
    end;
    S.Read(TxtPolyData.Offset, SizeOf(TxtPolyData.Offset));
    S.Read(TxtPolyData.hShift, SizeOf(TxtPolyData.hShift));
    S.Read(TxtPolyData.vShift, SizeOf(TxtPolyData.vShift));
    S.Read(TxtPolyData.fVerAl, SizeOf(TxtPolyData.fVerAl));
    TxtPolyData.bReDir := (TxtPolyData.fVerAl and cwTxtAdvFRevDir) <> 0;
    TxtPolyData.fVerAl := (TxtPolyData.fVerAl and cwTxtAdvFVertAA);
  end;
//-- Glukhov Bug#468 Build#162 01.08.01
  //++Brovak
//     temporary closed
  if Version >= cTxtVerConvAnnot then
    S.Read(IsItAnnot, SizeOf(IsItAnnot));

  if Version >= cTxtVerAdvConvAnnot then
  begin;
    S.Read(IsUseOffset, SizeOf(IsUseOffset));
    S.Read(ObjectOffSetType, SizeOf(ObjectOffSetType));
  end;

    //--Brovak
end;

procedure TText.Store(S: TOldStream);
var
  Version: Word;
//++ Glukhov Bug#468 Build#162 01.08.01
  i: Integer;
  Pnt: PDPoint;
//-- Glukhov Bug#468 Build#162 01.08.01
  AMode: Integer;
begin
//++ Glukhov TextVersion Build#151 14.02.01
// Glukhov, Version repair
  Version := cTxtVersion;
//    Version:= cTxtVerOriginal;  // 17.08.01, to repair for Austria
//-- Glukhov TextVersion Build#151 14.02.01
  S.Write(Version, SizeOf(Version));
  TView.Store(S);
  S.WriteStr(Text);
  Pos.Store(S);
  S.Write(Angle, SizeOf(Angle));
  S.Write(Width, SizeOf(Width));
  Font.Store(S);
//++ Glukhov Bug#468 Build#162 01.08.01
// Glukhov, Version repair
  if Version <= cTxtVerOriginal then
    Exit; // 17.08.01, to repair for Austria 17.08.01
  AMode := fAdvMode or FAlign shl 16;
  S.Write(AMode, SizeOf(AMode));
    // Check for an advanced mode
  if fAdvMode <> cwTxtAdvRegular then
  begin
    S.Write(TxtBrkData.hSpace, SizeOf(TxtBrkData.hSpace));
    S.Write(TxtBrkData.vSpace, SizeOf(TxtBrkData.vSpace));
  end;
  if (fAdvMode = cwTxtAdvSelPoly)
    or (fAdvMode = cwTxtAdvManPoly) then
  begin
    if fAdvMode = cwTxtAdvSelPoly then
    begin
      S.Write(TxtPolyData.IdxPoly, SizeOf(TxtPolyData.IdxPoly));
      S.Write(TxtPolyData.bConvA, SizeOf(TxtPolyData.bConvA));
      S.Write(TxtPolyData.dConvA, SizeOf(TxtPolyData.dConvA));
    end
    else
    begin
        // Put count of vertices
      S.Write(TxtPolyData.Vertices.Count, SizeOf(TxtPolyData.Vertices.Count));
        // Put vertices
      for i := 0 to TxtPolyData.Vertices.Count - 1 do
      begin
        Pnt := PDPoint(TxtPolyData.Vertices.At(i));
        S.Write(Pnt.X, SizeOf(Pnt.X));
        S.Write(Pnt.Y, SizeOf(Pnt.Y));
      end;
    end;
    S.Write(TxtPolyData.Offset, SizeOf(TxtPolyData.Offset));
    S.Write(TxtPolyData.hShift, SizeOf(TxtPolyData.hShift));
    S.Write(TxtPolyData.vShift, SizeOf(TxtPolyData.vShift));
    if TxtPolyData.bReDir then
      TxtPolyData.fVerAl := TxtPolyData.fVerAl or cwTxtAdvFRevDir;
    S.Write(TxtPolyData.fVerAl, SizeOf(TxtPolyData.fVerAl));
  end;
//-- Glukhov Bug#468 Build#162 01.08.01
  //Brovak
//   temporary closed
  S.Write(IsItAnnot, SizeOf(IsItAnnot));
  S.Write(IsUseOffset, SizeOf(IsUseOffset));
  S.Write(ObjectOffSetType, SizeOf(ObjectOffSetType));
end;

destructor TText.Done;
begin
//++ Glukhov Bug#468 Build#162 01.07.01
  if Assigned(TxtBrkData) then
  begin
    TxtBrkData.Destroy;
    TxtBrkData := nil;
  end;
  if Assigned(TxtPolyData) then
  begin
    TxtPolyData.Destroy;
    TxtPolyData := nil;
  end;
//-- Glukhov Bug#468 Build#162 01.07.01
  Pos.Done;
  DisposeStr(Text);
  TView.Done;
end;

procedure TText.Draw(PInfo: PPaint; Clipped: Boolean);
var
  AWidth: Double;
  AHeight: LongInt;
  FontDes: PFontDes;
  APoly: PCPoly;
  OldMode: Word;
  StartPos: TGrPoint;
  i: Integer;
  sTmp: AnsiString;
  OldClipRect: TDRect;
  APos: TDPoint;
begin
//++ Glukhov Bug#468 Build#162 01.07.01
    // ReCalculate the text, if necessary
  if not Assigned(TxtBrkData) then
    fAdvMode := cwTxtAdvRegular;

  CheckTextState(PInfo);

  if fAdvMode <> cwTxtAdvRegular then
  begin
    TxtBrkData.Draw(PInfo, Clipped);
    Exit;
  end;
//-- Glukhov Bug#468 Build#162 01.07.01
  // calculate current width and height round
  AHeight := Trunc(PInfo^.CalculateDispDouble(Font.Height));
  AWidth := PInfo^.CalculateDispDouble(Width);

  // get information about the font

  FontDes := PInfo^.Fonts^.GetFont(Font.Font);
  if FontDes = nil then
  begin
    Font.Font := PInfo^.Fonts^.IDFromName('Arial');
    FontDes := PInfo^.Fonts^.GetFont(Font.Font);
    if FontDes <> nil then
    begin
      Inc(FontDes^.UseCount);
      UpdateTextData(PInfo);
    end;
  end;

//++ Glukhov Bug#291 Build#150 25.01.01
//    if (AHeight>0) and (AWidth>0) and (Assigned(FontDes)) then begin
  if (AWidth > 0) and (Assigned(FontDes)) then
  begin
    if (AHeight <= 0) then
      AHeight := 1;
//-- Glukhov Bug#291 Build#150 25.01.01
    if (AHeight >= MaxInt)
      or ((AHeight < minDrawHeight) and (not PInfo^.Printing)) then
    begin
      if PInfo^.VariousSettings.ShowHiddenText then
      begin
        APoly := GetBorder;
        OldMode := PInfo^.DrawMode;
        PInfo^.DrawMode := PInfo^.DrawMode or dm_NoPattern;
        APoly^.Draw(PInfo, Clipped);
        Dispose(APoly, Done);
        PInfo^.DrawMode := OldMode;
      end;
    end
    else
      with PInfo^.ExtCanvas do
      begin
        // store current settings
        Push;
        try
          // setup font
          Font.Angle := -Angle * Pi / 180.0 + PInfo^.ViewRotation;
          Font.Name := FontDes^.Name^;
          Font.Size := AHeight;
          Font.Color := Pen.Color;
          Font.Style := (Self.Font.Style shr 2) and $7;
          // setup brush
          if Self.Font.Style and ts_Transparent <> 0 then
            Brush.BackColor := clNone;
          {+++Brovak BUG#32 BUILD 127}
        //  else Brush.BackColor:=$FFFFFF;
          {--Brovak}
          APos.Init(GetULX, GetULY);
          PInfo^.ConvertToDispDouble(APos, StartPos.X, StartPos.Y);
          // calculate position from text-alignment and draw text
          if Self.Font.Style and ts_Right <> 0 then
          begin
            StartPos.X := StartPos.X + AWidth * Font.CosAngle;
            StartPos.Y := StartPos.Y + AWidth * Font.SinAngle;
            Font.Alignment := taRight;
          end
          else
            if Self.Font.Style and ts_Center <> 0 then
            begin
              StartPos.X := StartPos.X + AWidth * Font.CosAngle / 2.0;
              StartPos.Y := StartPos.Y + AWidth * Font.SinAngle / 2.0;
              Font.Alignment := taCentered;
            end
            else
              Font.Alignment := taLeft;
//++ Glukhov Bug#114 Build#125 20.11.00
//          for i:= 1 to nLines do begin
          for i := 0 to nLines - 1 do
          begin
            sTmp := GetLine(i);
            if Clipped then
              ClippedTextOut(StartPos, @(sTmp[1]), Length(sTmp))
            else
              TextOut(StartPos, @(sTmp[1]), Length(sTmp));
            StartPos.X := StartPos.X + AHeight * Font.SinAngle;
            StartPos.Y := StartPos.Y - AHeight * Font.CosAngle;
          end;
//-- Glukhov Bug#114 Build#125 20.11.00
        finally
          // restore previous settings
          Pop;
        end;
      end;
  end;
end;

function TText.GetObjType: Word;
begin
  GetObjType := ot_Text;
end;

//++ Glukhov Bug#468 Build#162 16.07.01

function TText.SelectByPoint(
  PInfo: PPaint; Point: TDPoint; const ObjType: TObjectTypes): PIndex;
var
  CPoly: PCPoly;
  i: Integer;
  Ptr: PText;
begin
  SelectByPoint := nil;
  if ot_Text in ObjType then
  begin
    if (fAdvMode = cwTxtAdvRegular)
      or (not Assigned(TxtBrkData)) then
    begin
      CPoly := GetBorder;
      if Assigned(CPoly^.SelectByPoint(PInfo, Point, [ot_CPoly])) then
        SelectByPoint := @Self;
      Dispose(CPoly, Done);
    end
    else
      for i := 0 to TxtBrkData.Letters.Count - 1 do
      begin
        Ptr := PText(TxtBrkData.Letters.At(i));
        if Assigned(Ptr.SelectByPoint(PInfo, Point, [ot_Text])) then
        begin
          SelectByPoint := @Self;
          Exit;
        end;
      end;
  end;
end;
//-- Glukhov Bug#468 Build#162 16.07.01

function TText.GetBorder: PCPoly;
var
  APoly: PCPoly;
  Beta: Double;
  CosL: Double;
  SinL: Double;
  APoint: TDPoint;
begin
  APoly := New(PCPoly, Init);
//++ Glukhov Bug#468 Build#162 15.08.01
  if (fAdvMode = cwTxtAdvRegular)
    or (fAdvMode = cwTxtAdvSpaced) then
  begin
    Beta := Angle * Pi / 180;
    CosL := Cos(Beta);
    SinL := Sin(Beta);
    APoint.Init(GetULX, GetULY);
    APoly^.InsertPoint(APoint);
    APoint.Move(Trunc(CosL * Width), Trunc(-SinL * Width));
    APoly^.InsertPoint(APoint);
    APoint.Move(Trunc(-SinL * Height), Trunc(-CosL * Height));
    APoly^.InsertPoint(APoint);
    APoint.Move(Trunc(-CosL * Width), Trunc(SinL * Width));
  end
  else
  begin
    APoint.Init(ClipRect.A.X, ClipRect.B.Y);
    APoly^.InsertPoint(APoint);
    APoint.Init(ClipRect.B.X, ClipRect.B.Y);
    APoly^.InsertPoint(APoint);
    APoint.Init(ClipRect.B.X, ClipRect.A.Y);
    APoly^.InsertPoint(APoint);
    APoint.Init(ClipRect.A.X, ClipRect.A.Y);
  end;
//-- Glukhov Bug#468 Build#162 15.08.01
  APoly^.InsertPoint(APoint);
  APoly^.ClosePoly;
  GetBorder := APoly;
end;

function TText.GivePosX: LongInt;
begin
  GivePosX := Pos.X;
end;

function TText.GivePosY: LongInt;
begin
  GivePosY := Pos.Y;
end;

//++ Glukhov Bug#468 Build#166 16.09.01
// Gets the object data to edit it, using the special component.
// P1 is a left lower corner of border, p4 is a left upper one.

procedure TText.InitObjEdit(
  PInfo: PPaint; var P1, P2, P3, P4: TGrPoint;
  var bRotate, bResize, bSimilar, bMove: Boolean);
var
  Poly: PCPoly;
  Pnt: PDPoint;
begin
  TView.InitObjEdit(PInfo, P1, P2, P3, P4, bRotate, bResize, bSimilar, bMove);
  if (Font.Style and ts_FixHeight) = 0 then
    bResize := True;
  if (fAdvMode = cwTxtAdvRegular) or (fAdvMode = cwTxtAdvSpaced) then
    bRotate := True;
  Poly := GetBorder;
  Pnt := PDPoint(Poly.Data.At(3));
  P1 := GrPoint(Pnt.X, Pnt.Y);
  Pnt := PDPoint(Poly.Data.At(2));
  P2 := GrPoint(Pnt.X, Pnt.Y);
  Pnt := PDPoint(Poly.Data.At(1));
  P3 := GrPoint(Pnt.X, Pnt.Y);
  Pnt := PDPoint(Poly.Data.At(0));
  P4 := GrPoint(Pnt.X, Pnt.Y);
  Dispose(Poly, Done);
end;

// Modifies the object after its editing, using the special component.

procedure TText.ExecObjEdit(PInfo: PPaint; P1, P2, P3, P4: TGrPoint);
var
  Po1, Po2, Po3, Po4: TGrPoint;
  bRo, bRe, bSi, bMo: Boolean;
  Ratio: Double;
  Number: Integer;
begin
  InitObjEdit(PInfo, Po1, Po2, Po3, Po4, bRo, bRe, bSi, bMo);
  if GrPointsEqual(P1, Po1) and GrPointsEqual(P2, Po2) and
    GrPointsEqual(P3, Po3) and GrPointsEqual(P4, Po4) then
    Exit;

  if (fAdvMode = cwTxtAdvRegular) or (fAdvMode = cwTxtAdvSpaced) then
  begin
      // Set the new text data
    Pos.Init(Round(P4.X), Round(P4.Y));
    Angle := -Round(LineAngle(P1, P2) * 180 / PI);
  end
  else
  begin
      // cwTxtAdvSelPoly - text drawn along, set the add.shift
    MoveRel(Round(P4.X - Po4.X), Round(P4.Y - Po4.Y));
  end;

  if bRe then
  begin
      // Scale the font height
    Ratio := GrPointDistance(P1, P4) / GrPointDistance(Po1, Po4);
    Number := Round(Ratio * Font.Height);
    if Number < cMinFontHeight then
      Number := cMinFontHeight;
    Font.Height := Number;
  end;

    // Recalculate the other text data
  UpdateTextData(PInfo);
  if PInfo^.IsMU then
  begin
    MU.LogUpdObjectData(PInfo, WingisMainForm.ActualChild.Data^.Layers^.TopLayer, @Self);
  end;
end;
//-- Glukhov Bug#468 Build#166 16.09.01

procedure TText.MoveRel(XMove: LongInt; YMove: LongInt);
begin
  TView.MoveRel(XMove, YMove);
  Pos.Move(XMove, YMove);
//++ Glukhov Bug#468 Build#162 16.07.01
  if (fAdvMode = cwTxtAdvSelPoly)
    or (fAdvMode = cwTxtAdvManPoly) then
  begin
    TxtPolyData.hShift := TxtPolyData.hShift + XMove;
    TxtPolyData.vShift := TxtPolyData.vShift + YMove;
  end;
  fFlagSt := cwTxtStChanged;
//-- Glukhov Bug#468 Build#162 16.07.01
end;

//++ Glukhov Bug#468 Build#162 01.07.01

procedure TText.SetNewText(PInfo: PPaint; AText: string; NFont: TFontData);
var
  AFont: PFontDes;
begin
  SetText(AText);
  AFont := PInfo^.Fonts^.GetFont(Font.Font);
  if Assigned(AFont) then
    Dec(AFont^.UseCount);
  Font := NFont;
  AFont := PInfo^.Fonts^.GetFont(Font.Font);
  if Assigned(AFont) then
    Inc(AFont^.UseCount);
  if Font.Height < cMinFontHeight then
    Font.Height := cMinFontHeight;
  UpdateTextData(PInfo);
end;
//-- Glukhov Bug#468 Build#162 01.07.01

//++ Glukhov Bug#205 Build#128 24.11.00
// Returns True, if the given String matches the given template

function StrMatchesTemplate(sStr, sTpl: AnsiString): Boolean;
var
  st1: Char;
  lsT, lsS: AnsiString;
  bRes: Boolean;
begin
  Result := True;
// Process the empty template
  if sTpl = '' then
  begin
    if sStr <> '' then
      Result := False;
    Exit;
  end;
// Process the 1st template character
  st1 := sTpl[1];
  lsT := Copy(sTpl, 2, Length(sTpl) - 1);

  case st1 of
  // Process the asterisk
    '*':
      begin
        if lsT = '' then
          Exit;
        lsS := sStr;
        repeat
          if StrMatchesTemplate(lsS, lsT) then
            Exit;
          if lsS = '' then
            Break;
          lsS := Copy(lsS, 2, Length(lsS) - 1);
        until False;
      end;
  // Process the plus
    '+':
      begin
        if lsT = '' then
        begin
          if Length(sStr) > 0 then
            Exit;
        end
        else
        begin
          lsS := sStr;
          while lsS <> '' do
          begin
            lsS := Copy(lsS, 2, Length(lsS) - 1);
            if StrMatchesTemplate(lsS, lsT) then
              Exit;
          end;
        end;
      end;
  // Process the period
    '.':
      begin
        if sStr <> '' then
        begin
          lsS := Copy(sStr, 2, Length(lsS) - 1);
          Result := StrMatchesTemplate(lsS, lsT);
          Exit;
        end;
      end;
  else
    begin
      if sStr <> '' then
      begin
        if st1 = '\' then
        begin
        // Process the backslash
          if lsT <> '' then
          begin
          // Use the next template symbol as a regular one
            st1 := lsT[1];
            lsT := Copy(lsT, 2, Length(lsT) - 1);
          end;
        end;
        if sStr[1] = st1 then
        begin
          lsS := Copy(sStr, 2, Length(sStr) - 1);
          Result := StrMatchesTemplate(lsS, lsT);
          Exit;
        end;
      end;
    end;
  end; // find
  Result := False;
end;
//-- Glukhov Bug#205 Build#128 24.11.00

//++ Glukhov Bug#387 Build#152 17.04.01
// Returns True, if the Text matches the given template

function TText.TemplateMatched(
//  sTpl: AnsiString; bSubStr, bCaseSens: Boolean ): Boolean;
  sTpl: AnsiString; bSubStr, bCaseSens, bWildcards: Boolean): Boolean;
// Wildcards:
//    * - any substring
//    + - any substring but empty
//    . - any symbol
//    \ - use the next symbol as a regular one
var
  lsT, lsS: AnsiString;
  i: Integer;
begin
  lsS := Text^;
  if not bWildCards then
  begin
  // Make all the wild symbols to the regular ones, using '\'
    lsT := '';
    for i := 1 to Length(sTpl) do
    begin
      case sTpl[i] of
        // a wildcard
        '*', '+', '.', '\': lsT := lsT + '\' + sTpl[i];
      else
        lsT := lsT + sTpl[i];
      end;
    end;
  end
  else
    lsT := sTpl;
  if not bCaseSens then
  begin
    lsT := AnsiUpperCase(lsT);
    lsS := AnsiUpperCase(lsS);
  end;
  if bSubStr then
    lsT := '*' + lsT + '*';
  Result := StrMatchesTemplate(lsS, lsT);
end;
//-- Glukhov Bug#387 Build#152 17.04.01

//++ Glukhov Bug#468 Build#162 16.07.01

function TText.SelectByCircle(PInfo: PPaint; Middle: TDPoint; Radius: LongInt;
  const ObjType: TObjectTypes; Inside: Boolean): Boolean;
var
  CPoly: PCPoly;
  i: Integer;
  Ptr: PText;
begin
  Result := False;
  if ot_Text in ObjType then
  begin
    if (fAdvMode = cwTxtAdvRegular) then
    begin
      CPoly := GetBorder;
      Result := CPoly^.SelectByCircle(PInfo, Middle, Radius, [ot_CPoly], Inside);
      Dispose(CPoly, Done);
    end
    else
      for i := 0 to TxtBrkData.Letters.Count - 1 do
      begin
        Ptr := PText(TxtBrkData.Letters.At(i));
        if Ptr.SelectByCircle(PInfo, Middle, Radius, [ot_Text], Inside) then
        begin
          Result := True;
          Exit;
        end;
      end;
  end;
end;

function TText.SelectByPoly(PInfo: PPaint;
  Poly: Pointer; const ObjType: TObjectTypes; Inside: Boolean): Boolean;
var
  CPoly: PCPoly;
  i: Integer;
  Ptr: PText;
begin
  Result := False;
  if ot_Text in ObjType then
  begin
    if (fAdvMode = cwTxtAdvRegular) then
    begin
      CPoly := GetBorder;
      Result := CPoly^.SelectByPoly(PInfo, Poly, [ot_CPoly], Inside);
      Dispose(CPoly, Done);
    end
    else
      for i := 0 to TxtBrkData.Letters.Count - 1 do
      begin
        Ptr := PText(TxtBrkData.Letters.At(i));
        if Ptr.SelectByPoly(PInfo, Poly, [ot_Text], Inside) then
        begin
          Result := True;
          Exit;
        end;
      end;
  end;
end;

function TText.SelectByPolyLine(PInfo: PPaint; Poly: Pointer;
  const ObjType: TObjectTypes): Boolean;
var
  CPoly: PCPoly;
  i: Integer;
  Ptr: PText;
begin
  Result := False;
  if ot_Text in ObjType then
  begin
    if (fAdvMode = cwTxtAdvRegular) then
    begin
      CPoly := GetBorder;
      Result := CPoly.SelectByPolyLine(PInfo, Poly, [ot_CPoly]);
      Dispose(CPoly, Done);
    end
    else
      for i := 0 to TxtBrkData.Letters.Count - 1 do
      begin
        Ptr := PText(TxtBrkData.Letters.At(i));
        if Ptr.SelectByPolyLine(PInfo, Poly, [ot_Text]) then
        begin
          Result := True;
          Exit;
        end;
      end;
  end;
end;

function TText.SelectBetweenPolys(PInfo: PPaint; Poly1: Pointer; Poly2: Pointer;
  const ObjType: TObjectTypes; Inside: Boolean): Boolean;
var
  CPoly: PCPoly;
  i: Integer;
  Ptr: PText;
begin
  Result := False;
  if ot_Text in ObjType then
  begin
    if (fAdvMode = cwTxtAdvRegular) then
    begin
      CPoly := GetBorder;
      Result := CPoly.SelectBetweenPolys(PInfo, Poly1, Poly2, [ot_CPoly], Inside);
      Dispose(CPoly, Done);
    end
    else
      for i := 0 to TxtBrkData.Letters.Count - 1 do
      begin
        Ptr := PText(TxtBrkData.Letters.At(i));
        if Ptr.SelectBetweenPolys(PInfo, Poly1, Poly2, [ot_Text], Inside) then
        begin
          Result := True;
          Exit;
        end;
      end;
  end;
end;
//-- Glukhov Bug#468 Build#162 16.07.01

procedure TText.Scale
  (
  XScale: Real;
  YScale: Real
  );
begin
  inherited Scale(XScale, YScale);
  Pos.Scale(XScale, YScale);
  Font.Height := LimitToLong(Font.Height * XScale);
  Width := LimitToLong(Width * YScale);
  CalculateClipRect(nil); { braucht eigentlich kein PInfo ! 0996Ray}
end;

procedure TText.SetAlign(AAlign: Byte);
begin
  if FAlign <> AAlign then
  begin
    FAlign := AAlign;
    CalculateClipRect(nil);
  end;
end;

function TText.GetULX: Integer;
var
  AWidth: Integer;
  SinL, CosL: Double;
  Beta: Double;
begin
  if (fAdvMode = cwTxtAdvRegular) or (fAdvMode = cwTxtAdvSpaced) then
  begin
    Beta := Angle * Pi / 180;
    CosL := Cos(Beta);
    SinL := Sin(Beta);
    case FAlign of
      0: Result := Pos.X;
      1: Result := Pos.X + Trunc(SinL * Height / 2);
      2: Result := Pos.X + Trunc(SinL * Height);
      3: Result := Pos.X + Trunc(-CosL * Width / 2) + Trunc(SinL * Height);
      4: Result := Pos.X + Trunc(-CosL * Width) + Trunc(SinL * Height);
      5: Result := Pos.X + Trunc(-CosL * Width) + Trunc(SinL * Height / 2);
      6: Result := Pos.X + Trunc(-CosL * Width);
      7: Result := Pos.X + Trunc(-CosL * Width / 2);
      8: Result := Pos.X + Trunc(-CosL * Width / 2) + Trunc(SinL * Height / 2);
    else
      FAlign := 0;
      Result := Pos.X;
    end;
  end
  else
  begin
    Result := Pos.X;
  end;
end;

function TText.GetULY: Integer;
var
  AWidth: Integer;
  SinL, CosL: Double;
  Beta: Double;
begin
  if (fAdvMode = cwTxtAdvRegular) or (fAdvMode = cwTxtAdvSpaced) then
  begin
    Beta := Angle * Pi / 180;
    CosL := Cos(Beta);
    SinL := Sin(Beta);
    case FAlign of
      0: Result := Pos.Y;
      1: Result := Pos.Y + Trunc(CosL * Height / 2);
      2: Result := Pos.Y + Trunc(CosL * Height);
      3: Result := Pos.Y + Trunc(SinL * Width / 2) - Trunc(-CosL * Height);
      4: Result := Pos.Y + Trunc(SinL * Width) - Trunc(-CosL * Height);
      5: Result := Pos.Y + Trunc(SinL * Width) - Trunc(-CosL * Height / 2);
      6: Result := Pos.Y + Trunc(SinL * Width);
      7: Result := Pos.Y + Trunc(SinL * Width / 2);
      8: Result := Pos.Y + Trunc(SinL * Width / 2) - Trunc(-CosL * Height / 2);
    else
      FAlign := 0;
      Result := Pos.Y;
    end;
  end
  else
  begin
    Result := Pos.Y;
  end;

end;

procedure TSText.Draw
  (
  PInfo: PPaint;
  Clipped: Boolean
  );
var
  AWidth: Double;
  AHeight: LongInt;
  FontDes: PFontDes;
  APoly: PCPoly;
  OldMode: Word;
  StartPos: TGrPoint;
  i: Integer;
  sTmp: AnsiString;
begin
  // Recalculate Size, if necessary
//++ Glukhov TextVersion Build#151 14.02.01
//    if nLines = 0 then CalculateSize( PInfo );
  if (nLines = 0) or (fFlagSt = cwTxtStInitial) then
  begin
    i := Width;
    AHeight := Height;
    CalculateSize(PInfo);
    if (i <> Width) or (AHeight <> Height) then
    begin
      CalculateClipRect(PInfo);
      if Assigned(PInfo.AProj) then
        PProj(PInfo.AProj).SetModified;
    end;
    fFlagSt := cwTxtStCalculated;
  end;
//-- Glukhov TextVersion Build#151 14.02.01
  // calculate current width and height
  AWidth := PInfo^.ScaleDispDouble(Width);
  AHeight := PInfo^.ScaleDisp(Font.Height);
  // get information about the font
  FontDes := PInfo^.Fonts^.GetFont(Font.Font);
//++ Glukhov Bug#291 Build#150 25.01.01
//    if (AHeight>0) and (AWidth>0) and (Assigned(FontDes)) then begin
  if (AWidth > 0) and (Assigned(FontDes)) then
  begin
    if (AHeight <= 0) then
      AHeight := 1;
//-- Glukhov Bug#291 Build#150 25.01.01
//++ Glukhov Bug#114 Build#125 22.11.00
    if (AHeight >= MaxInt)
      or ((AHeight < minDrawHeight) and (not PInfo^.Printing)) then
    begin
      {++ Brovak BUG#574 BUILD#108 - Additional Condition for draw/no draw rect for hidden text}
      if PInfo^.VariousSettings.ShowHiddenText then
      begin
      {--  Brovak}
        APoly := GetBorder;
        OldMode := PInfo^.DrawMode;
        PInfo^.DrawMode := PInfo^.DrawMode or dm_NoPattern;
        APoly^.Draw(PInfo, Clipped);
        Dispose(APoly, Done);
        PInfo^.DrawMode := OldMode;
      end;
//-- Glukhov Bug#114 Build#125 22.11.00
    end
    else
      with PInfo^.ExtCanvas do
      begin
        // store current settings
        Push;
        try
          // setup font
          Font.Angle := -Angle * Pi / 180.0 + PInfo^.ViewRotation + PInfo^.Angle;
          Font.Name := FontDes^.Name^;
          Font.Size := AHeight;
          Font.Color := Pen.Color;
{++ Ivanoff BUG#417 BUILD#100}
{The definition of the font style was missed.}
          Font.Style := (Self.Font.Style shr 2) and $7;
{-- Ivanoff}
          // setup brush
          if Self.Font.Style and ts_Transparent <> 0 then
            Brush.BackColor := clNone
          else
            Brush.BackColor := $FFFFFF;
          PInfo^.ScaleToDispDouble(Pos, StartPos.X, StartPos.Y);
          // calculate position from text-alignment and draw text
          if Self.Font.Style and ts_Right <> 0 then
          begin
            StartPos.X := StartPos.X + AWidth * Font.CosAngle;
            StartPos.Y := StartPos.Y + AWidth * Font.SinAngle;
            Font.Alignment := taRight;
          end
          else
            if Self.Font.Style and ts_Center <> 0 then
            begin
              StartPos.X := StartPos.X + AWidth * Font.CosAngle / 2.0;
              StartPos.Y := StartPos.Y + AWidth * Font.SinAngle / 2.0;
              Font.Alignment := taCentered;
            end
            else
              Font.Alignment := taLeft;
//++ Glukhov Bug#114 Build#125 22.11.00
//          for i:= 1 to nLines do begin
          for i := 0 to nLines - 1 do
          begin
            sTmp := GetLine(i);
            if Clipped then
              ClippedTextOut(StartPos, @(sTmp[1]), Length(sTmp))
            else
              TextOut(StartPos, @(sTmp[1]), Length(sTmp));
            StartPos.X := StartPos.X + AHeight * Font.SinAngle;
            StartPos.Y := StartPos.Y - AHeight * Font.CosAngle;
          end;
//-- Glukhov Bug#114 Build#125 22.11.00
        finally
          // restore previous settings
          Pop;
        end;
      end;
  end;
end;

function TSText.GetBorder
  : PCPoly;
var
  APoly: PSCPoly;
  Beta: Double;
  CosL: Double;
  SinL: Double;
  APoint: TDPoint;
begin
  APoly := New(PSCPoly, Init);
  Beta := Angle * Pi / 180;
  CosL := Cos(Beta);
  SinL := Sin(Beta);
  APoint.Init(Pos.X, Pos.Y);
  APoly^.InsertPoint(APoint);
  APoint.Move(Round(CosL * Width), Round(-SinL * Width));
  APoly^.InsertPoint(APoint);
//++ Glukhov Bug#114 Build#125 22.11.00
//    APoint.Move(Round(-SinL*Font.Height),Round(-CosL*Font.Height));
  APoint.Move(Round(-SinL * Height), Round(-CosL * Height));
//-- Glukhov Bug#114 Build#125 22.11.00
  APoly^.InsertPoint(APoint);
  APoint.Move(Round(-CosL * Width), Round(SinL * Width));
  APoly^.InsertPoint(APoint);
  APoly^.ClosePoly;
  GetBorder := APoly;
end;

const
  RTextOld: TStreamRec = (
    ObjType: rn_TextOld;
    VmtLink: TypeOf(TText);
    Load: @TText.LoadOld;
    Store: @TText.Store);

  RText: TStreamRec = (
    ObjType: rn_Text;
    VmtLink: TypeOf(TText);
    Load: @TText.Load;
    Store: @TText.Store);
  RSTextOld: TStreamRec = (
    ObjType: rn_STextOld;
    VmtLink: TypeOf(TSText);
    Load: @TText.LoadOld;
    Store: @TText.Store);
  RSText: TStreamRec = (
    ObjType: rn_SText;
    VmtLink: TypeOf(TSText);
    Load: @TText.Load;
    Store: @TText.Store);

begin
  RegisterType(RTextOld);
  RegisterType(RText);
  RegisterType(RSTextOld);
  RegisterType(RSText);

end.

