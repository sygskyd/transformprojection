�
 TSUPERIMAGEINFO 0Q  TPF0TSuperImageInfoSuperImageInfoLefthTop�BorderStylebsDialogCaption!16ClientHeightClientWidthlColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrder	PositionpoScreenCenterOnCloseQueryFormCloseQueryOnResize
FormResizeOnShowFormShowPixelsPerInch`
TextHeight TBevelBevel1LeftTop� WidthyHeightAnchorsakLeftakBottom ShapebsBottomLineVisible  TButtonOKBtnLeft� Top� WidthYHeightAnchorsakLeftakBottom Caption!0Default	ModalResultTabOrder Visible  TButton	CancelBtnLeft� Top� WidthYHeightAnchorsakLeftakBottom Cancel	Caption!1ModalResultTabOrderVisible  TPanel	MainPanelLeft Top WidthlHeight� AlignalTopAnchorsakLeftakTopakRightakBottom Caption	MainPanelTabOrder TMemoMainMemoLeftTopWidth� Height� AlignalClientLines.Strings  ReadOnly	
ScrollBars
ssVerticalTabOrder   TPanelImageSizePanelLeft� TopWidth� Height� AlignalRightCaptionImageParamPanelTabOrder 	TGroupBoxColorGroupBoxLeftTopWidth� HeightVAlignalTopCaption!24TabOrder  TRadioButton	Radio1BitLeftTopWidth� HeightAnchorsakLeftakTopakRight Caption!20TabOrder   TRadioButton
Radio4BitsLeftTop Width� HeightAnchorsakLeftakTopakRight Caption!21TabOrder  TRadioButton
Radio8BitsLeftTop0Width� HeightAnchorsakLeftakTopakRight Caption!22TabOrder  TRadioButtonRadio24BitsLeftTopAWidth� HeightAnchorsakLeftakTopakRight Caption!23TabOrder   	TGroupBoxXSizeGroupBoxLeftTopWWidth� HeightEAlignalTopCaptionImage XTabOrder TLabel
XSizeLabelLeftQTopWidth1Height	AlignmenttaCenterAnchorsakLeftakBottom Caption
meters/pixWordWrap	  TLabelLabel1LeftRTop.WidthHeightAnchorsakLeftakBottom Captionpixels  TEditXResLeftTopWidthHHeightAnchorsakLeftakBottom ParentShowHintShowHint	TabOrder OnChange
XResChange  TEditXDimLeftTop+WidthHHeightAnchorsakLeftakBottom ParentShowHintShowHint	TabOrderOnChange
XDimChange   	TGroupBoxYSizeGroupBoxLeftTop� Width� HeightFAlignalTopCaptionImage YTabOrder TLabel
YSizeLabelLeftQTopWidth1Height	AlignmenttaCenterAnchorsakLeftakBottom Caption
meters/pixWordWrap	  TLabelLabel2LeftRTop/WidthHeightAnchorsakLeftakBottom Captionpixels  TEditYResLeftTopWidthFHeightAnchorsakLeftakBottom ParentShowHintShowHint	TabOrder OnChange
YResChange  TEditYDimLeftTop,WidthFHeightAnchorsakLeftakBottom ParentShowHintShowHint	TabOrderOnChange
YDimChange     TMlgSection
MlgSectionSectionMainLeftTop  TFloatValidatorXResValidatorEditXResDefaultValue       ��?CommasMinValue أp=
ף�?	MaxLengthMaxValue      $�@ShowZeroValuesLeftKTop
  TFloatValidatorYResValidatorEditYResDefaultValue       ��?CommasMinValue أp=
ף�?	MaxLengthMaxValue      $�@ShowZeroValuesLeft}Top
   