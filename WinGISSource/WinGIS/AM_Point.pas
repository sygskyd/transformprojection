{****************************************************************************}
{ Unit AM_Point                                                              }
{----------------------------------------------------------------------------}
{ Grafikobjekt f�r Punkte                                                    }
{----------------------------------------------------------------------------}
{ Modifikationen                                                             }
{  16.02.1994 Martin Forst                                                   }
{  12.04.1994 Martin Forst   Selektieren mit Kreis und Poly                  }
{****************************************************************************}
unit AM_Point;

interface

uses WinTypes, WinProcs, AM_Def, AM_View, AM_Paint, AM_Index, Objects;

const
  MaxDist = 10;
  LineLen = 10;

type
  PPixel = ^TPixel;
  TPixel = object(TView)
    Position: TDPoint;
    constructor Init(DPos: TDPoint);
    destructor Done; virtual;
    constructor Load(S: TOldStream);
    procedure CalculateClipRect;
    procedure Draw(PInfo: PPaint; Clipping: Boolean); virtual;
    function GetObjType: Word; virtual;
    function GivePosX: LongInt; virtual;
    function GivePosY: LongInt; virtual;
    procedure Invalidate(PInfo: PPaint; FromGraphic: Boolean = TRUE); virtual;
    procedure MoveRel(XMove, YMove: LongInt); virtual;
    procedure Scale(XScale, YScale: Real); virtual; {2705F}
    function SelectBetweenPolys(PInfo: PPaint; Poly1, Poly2: Pointer; const ObjType: TObjectTypes;
      Inside: Boolean): Boolean; virtual;
    function SearchForPoint(PInfo: PPaint; InCircle: Pointer; Exclude: LongInt; const ObjType: TObjectTypes;
      var Point: TDPoint; var SnObje: PIndex): Boolean; virtual;
    function SelectByPoly(PInfo: PPaint; Poly: Pointer; const ObjType: TObjectTypes; Inside: Boolean): Boolean; virtual;
    function SelectByCircle(PInfo: PPaint; Middle: TDPoint;
      Radius: LongInt; const ObjType: TObjectTypes; Inside: Boolean): Boolean; virtual;
    function SelectByPoint(PInfo: PPaint; Point: TDPoint; const ObjType: TObjectTypes): PIndex; virtual;
    procedure Store(S: TOldStream); virtual;
    function SearchForNearestPoint(PInfo: PPaint; var ClickPos: TDPoint; InCircle: Pointer;
      Exclude: Longint; const ObjType: TObjectTypes; var point: TDPoint; var SnObje: PIndex): Boolean; virtual;
    function ReadWKT(WKTString: AnsiString): Boolean;
    function ToWKT: AnsiString;
  end;

  PSPixel = ^TSPixel;
  TSPixel = object(TPixel)
    procedure Draw(PInfo: PPaint; Clipping: Boolean); virtual;
  end;

implementation

uses AM_CPoly, AM_Circl, Win32Def, SysUtils, ToolsLib, Classes;

constructor TPixel.Init
  (
  DPos: TDPoint
  );
begin
  TView.Init;
  Position.Init(DPos.X, DPos.Y);
  with Position do
    ClipRect.Assign(X - LineLen, Y - LineLen, X + LineLen, Y + LineLen);
end;

procedure TPixel.CalculateClipRect;
begin
  with Position do
    ClipRect.Assign(X - LineLen, Y - LineLen, X + LineLen, Y + LineLen);
end;

destructor TPixel.Done;
begin
  Position.Done;
  TView.Done;
end;

constructor TPixel.Load
  (
  S: TOldStream
  );
begin
  TView.Load(S);
  Position.Load(S);
end;

procedure TPixel.Draw(PInfo: PPaint; Clipping: Boolean);
var
  APoint: TPoint;
begin
  PInfo^.ConvertToDisp(Position, APoint);
  with PInfo^.ExtCanvas do
  begin
    Push;
    try
      Pen.Style := lt_Solid;
      MoveTo(APoint.X - LineLen, APoint.Y);
      LineTo(APoint.X + LineLen, APoint.Y);
      MoveTo(APoint.X, APoint.Y - LineLen);
      LineTo(APoint.X, APoint.Y + LineLen);
    finally
      Pop;
    end;
  end;
end;

function TPixel.SelectByPoint
  (
  PInfo: PPaint;
  Point: TDPoint;
  const ObjType: TObjectTypes
  )
  : PIndex;
var
  Rect: TDRect;
  MDist: LongInt;
begin
  Result := nil;
  if ot_Pixel in ObjType then
  begin
    MDist := PInfo^.CalculateDraw(MaxDist);
    Rect.Init;
    Rect.A := Position;
    Rect.B := Position;
    Rect.A.Move(-MDist, -MDist);
    Rect.B.Move(MDist, MDist);
    if Rect.PointInside(Point) then
    begin
      Result := @Self;
    end;
  end;
end;

procedure TPixel.Store
  (
  S: TOldStream
  );
begin
  TView.Store(S);
  Position.Store(S);
end;

function TPixel.GetObjType
  : Word;
begin
  GetObjType := ot_Pixel;
end;

procedure TPixel.MoveRel
  (
  XMove: LongInt;
  YMove: LongInt
  );
begin
  TView.MoveRel(XMove, YMove);
  Position.Move(XMove, YMove);
end;

function TPixel.SearchForPoint
  (
  PInfo: PPaint;
  InCircle: Pointer;
  Exclude: LongInt;
  const ObjType: TObjectTypes;
  var Point: TDPoint;
  var SnObje: PIndex
  )
  : Boolean;
begin
  if (ot_Pixel in ObjType) and ClipRect.IsVisible(PEllipse(InCircle)^.ClipRect) and (Index <> Exclude) then
  begin
    if PEllipse(InCircle)^.PointInside(Position) then
    begin
      Point := Position;
      SearchForPoint := TRUE;
    end
    else
      SearchForPoint := FALSE;
  end
  else
    SearchForPoint := FALSE;
end;

procedure TPixel.Invalidate
  (
  PInfo: PPaint;
  FromGraphic: Boolean
  );
var
  Disp: TPoint;
  DispRect: TRect;
begin
  PInfo^.ConvertToDisp(Position, Disp);
  LPToDP(PInfo^.ExtCanvas.Handle, Disp, 1);
  DispRect.Left := Disp.X - LineLen;
  DispRect.Top := Disp.Y - LineLen;
  DispRect.Right := Disp.X + LineLen;
  DispRect.Bottom := Disp.Y + LineLen;
  InvalidateRect(PInfo^.HWindow, @DispRect, TRUE);
  if FromGraphic then
    PInfo^.FromGraphic := TRUE;
end;

procedure TSPixel.Draw
  (
  PInfo: PPaint;
  Clipping: Boolean
  );
var
  APoint: TPoint;
begin
  PInfo^.ScaleToDisp(Position, APoint);
  with PInfo^.ExtCanvas do
  begin
    MoveTo(APoint.X - LineLen, APoint.Y);
    LineTo(APoint.X + LineLen, APoint.Y);
    MoveTo(APoint.X, APoint.Y - LineLen);
    LineTo(APoint.X, APoint.Y + LineLen);
  end;
end;

function TPixel.SelectByCircle
  (
  PInfo: PPaint;
  Middle: TDPoint;
  Radius: LongInt;
  const ObjType: TObjectTypes;
  Inside: Boolean
  )
  : Boolean;
var
  MDist: LongInt;
begin
  if ot_Pixel in ObjType then
  begin
    MDist := LimitToLong(Position.Dist(Middle));
    if MDist <= Radius then
      SelectByCircle := TRUE
    else
      SelectByCircle := FALSE;
  end
  else
    SelectByCircle := FALSE;
end;

function TPixel.SelectByPoly
  (
  PInfo: PPaint;
  Poly: Pointer;
  const ObjType: TObjectTypes;
  Inside: Boolean
  )
  : Boolean;
begin
  if (ot_Pixel in ObjType)
    and ClipRect.IsVisible(PView(Poly)^.ClipRect) then
    SelectByPoly := PCPoly(Poly)^.PointInside(PInfo, Position)
  else
    SelectByPoly := FALSE;
end;

function TPixel.GivePosX: LongInt;
begin
  GivePosX := Position.X;
end;

function TPixel.GivePosY: LongInt;
begin
  GivePosY := Position.Y;
end;

procedure TPixel.Scale {2705F}
  (
  XScale: Real;
  YScale: Real
  );
begin
  inherited Scale(XScale, YScale);
  Position.Scale(XScale, YScale);
end; {2705F}

function TPixel.SearchForNearestPoint
  (
  PInfo: PPaint;
  var ClickPos: TDPoint;
  InCircle: Pointer;
  Exclude: LongInt;
  const ObjType: TObjectTypes;
  var Point: TDPoint;
  var SnObje: PIndex
  )
  : Boolean;
begin
  Result := FALSE;
  if (ot_Pixel in ObjType)
    and ClipRect.IsVisible(PEllipse(InCircle)^.ClipRect)
    and (Index <> Exclude)
    and PEllipse(InCircle)^.PointInside(Position)
    and (ClickPos.Dist(Point) > ClickPos.Dist(Position)) then
  begin
    Point := Position;
    Result := TRUE;
  end;
end;

function TPixel.SelectBetweenPolys
  (
  PInfo: PPaint;
  Poly1: Pointer;
  Poly2: Pointer;
  const ObjType: TObjectTypes;
  Inside: Boolean
  )
  : Boolean;
begin
  if (ot_Pixel in ObjType)
    and (ClipRect.IsVisible(PView(Poly1)^.ClipRect) or ClipRect.IsVisible(PView(Poly2)^.ClipRect)) then
    SelectBetweenPolys := PCPoly(Poly1)^.PointInside(PInfo, Position) xor PCPoly(Poly2)^.PointInside(PInfo, Position)
  else
    SelectBetweenPolys := FALSE;
end;

const
  RPixel: TStreamRec = (
    ObjType: rn_Pixel;
    VmtLink: TypeOf(TPixel);
    Load: @TPixel.Load;
    Store: @TPixel.Store);

  RSPixel: TStreamRec = (
    ObjType: rn_SPixel;
    VmtLink: TypeOf(TSPixel);
    Load: @TSPixel.Load;
    Store: @TSPixel.Store);

function TPixel.ReadWKT(WKTString: AnsiString): Boolean;
var
  i: Integer;
  Start, Stop: Integer;
  s: AnsiString;
  PointList: TList;
begin
  Result := False;
  Start := Pos('(', WKTString);
  Stop := StrLen(PChar(WKTString));
  s := '';
  PointList := TList.Create;
  try
    for i := Start to Stop do
    begin
      if ((WKTString[i] >= Char('0')) and (WKTString[i] <= Char('9'))) or (WKTString[i] = '.') or (WKTString[i] = Char('-')) then
      begin
        s := s + WKTString[i];
      end
      else
        if (WKTString[i] = ' ') or (WKTString[i] = ',') or (WKTString[i] = ')') then
        begin
          if s <> '' then
          begin
            PointList.Add(Pointer(Round(SToF(s) * 100)));
          end;
          s := '';
        end
        else
          if (WKTString[i] <> '(') then
          begin
            break;
          end;
    end;
    if PointList.Count >= 2 then
    begin
      Position.X := Integer(PointList[0]);
      Position.Y := Integer(PointList[1]);
      CalculateClipRect;
      Result := True;
    end;
  finally
    PointList.Free;
  end;
end;

function TPixel.ToWKT: AnsiString;
var
  s, xy: AnsiString;
begin
  Result := '';
  s := 'POINT ((';
  s := s + FloatToStr(Position.x / 100) + ' ' + FloatToStr(Position.y / 100);
  s := s + '))';
  Result := s;
end;

begin
  RegisterType(RPixel);
  RegisterType(RSPixel);
end.

