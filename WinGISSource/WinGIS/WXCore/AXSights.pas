unit AXSights;

interface

uses ComObj, ActiveX, WINGIS_TLB, StdVcl;

type
  TSights = class(TAutoObject, ISights)
  private
    FSights  : pointer;
    Data     : Pointer;
  protected
    function Get_Count: Integer; safecall;
    function Get_Items(Index: Integer): ISight; safecall;
    function InsertSight(const RefSight: ISight): ISight; safecall;
    procedure DeleteSightByIndex(Index: Integer); safecall; 
  public
    Constructor Create(aData,AItem:Pointer);
  end;

implementation

uses ComServ, AXSight, am_sight, GrTools, AM_Proj, AM_Def, Objects, UserIntf;

Constructor TSights.create;
begin
   inherited Create;
   FSights:=aItem;
   DAta:=AData;
end;

procedure TSights.DeleteSightByIndex(Index: Integer);
begin
   if (FSights<>NIL) and (Index < PSights(FSights)^.Count) then begin
      PSights(FSights)^.AtFree (Index);
      if Index = PSights(FSights).Default then
         PSights(FSights).Default := -1
      else if Index < PSights(FSights).Default then
         Dec(PSights(FSights)^.Default);

      PProj(Data)^.SetModified;
      Userinterface.Update([uiViews]);
   end;
end;

function TSights.Get_Count: Integer;
begin
   Result:=-1;
   if FSights<>NIL then begin
      Result:=PSights(FSights)^.Count;
   end;
end;

function TSights.Get_Items(Index: Integer): ISight;
begin
   Result:=NIL;
   if FSights <> NIL then
      REsult:=TAXSight.Create(Data,PSights(FSights)^.at(Index),false);
end;

function TSights.InsertSight(const RefSight: ISight): ISight;
var aSight     : PSight;
    ARotRect   : TRotRect;
    i          : integer;
    aSightLData: PSightLData;
begin
   result:=NIL;
   if FSights <> NIL then
   begin
      // copy the sight
      ARotRect.X:=RefSight.X;
      ARotRect.Y:=RefSight.Y;
      ARotRect.Width:=RefSight.Width;
      ARotRect.Height:=RefSight.Height;
      ARotRect.Rotation:=RefSight.Rotation;
      ASight:=new(PSight,init(RefSight.Name, ARotRect));
      ASight^.Description:=RefSight.Description;
      if RefSight.Count > 0 then
      begin
         ASight^.LayersData:=New(PCollection,Init(colInit,colExpand));
         // copy Layer-Data for Sights
         for i:=0 to RefSight.Count-1 do
         begin
            aSightLData:=new(PSightLData,init(RefSight.SightLData[i].LIndex, RefSight.SightLData[i].LFlags,
                                              RefSight.SightLData[i].GeneralMin, RefSight.SightLData[i].GeneralMax));
            aSight^.LayersData^.Insert(aSightLData);
         end;
      end;
      PSights(FSights)^.Insert(aSight);
      result:=TAXSight.Create(Data,aSight,false);
   end;
end;

initialization
  TAutoObjectFactory.Create(ComServer, TSights, Class_Sights,
    ciInternal, tmApartment);

end.
