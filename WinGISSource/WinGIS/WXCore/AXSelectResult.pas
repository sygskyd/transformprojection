unit AXSelectResult;

interface

uses
  ComObj, ActiveX, WINGIS_TLB;

type
  TSelectResult = class(TAutoObject, ISelectResult)
  private
    FPolyLIne : Pointer;
    x,y : integer;
    FDistance : integer;   //Distance form start to snap point
  protected
    function Get_Poly: IPolyline; safecall;
    function Get_Position: IPoint; safecall;
    function Get_DistanceToSnapPoint:integer; safecall;
    { Protected declarations }
  public
    Constructor Create(AX,AY:integer;AItem:Pointer;ADist:integer);
  end;

implementation

uses ComServ,axpoint,axPolyLine,am_Def;

constructor TSelectREsult.Create;

begin
  inherited Create;
  FPolyLIne:=AItem;
  x:=ax;
  y:=ay;
  FDistance:=ADist;
end;

function TSelectResult.Get_Poly: IPolyline;
begin
Result:=AXPolyLine.TPolyLine.Create(NIL,FPolyLIne,false);
end;

function TSelectResult.Get_Position: IPoint;
var APoint : PdPOint;
begin
  APOint:=new(PDPOint,Init(x,y));
  Result:=AXPoint.TPoint.Create(APoint,true);
end;

function TSelectResult.Get_DistanceToSnapPoint:integer;
begin
  Result:=FDistance;
end;

initialization
  TAutoObjectFactory.Create(ComServer, TSelectResult, Class_SelectResult,
    ciInternal, tmApartment);
end.
