unit AXSymbols;

interface

uses
  ComObj, ActiveX, WINGIS_TLB, StdVcl;

type
  TSymbols = class(TAutoObject, ISymbols)
  private
    FSymbols : pointer;
    Data     : Pointer;
    hIndex   : Integer;
  protected
    function Get_Items(Index: Integer): ISymbolDef; safecall;
    function Get_Count: Integer; safecall;
    procedure InsertSymbolDef(const AGroup: ISymbolDef); safecall;
    function IDToSymbolName(SymbolNumber: Integer): WideString; safecall;
    function SymbolNameToID(const SymbolName: WideString): Integer; safecall;
    function GetSymbolByName(const SymbolName: WideString): ISymbolDef; safecall;
    { Protected declarations }
  public
    Constructor Create(aData,AItem:Pointer);
  end;

implementation

uses ComServ,am_obj,AXSymbolDef,am_Poly,am_cpoly,am_group,am_view,am_def,am_proj,
     am_point,AM_Circl,am_Splin,am_text,AXPolygon,AXPolyLIne,AXPixel,AXArc,AXCircle,
     AXSpline,AXText,am_font,axdef,am_index,am_Layer;

Constructor TSymbols.create;
  begin
    inherited Create;
    FSymbols:=aItem;
    DAta:=AData;
  end;

function TSymbols.Get_Items(Index: Integer): ISymbolDef;
begin
  Result:=NIL;
  if FSymbols<>NIL then begin
    REsult:=TSymbolDef.Create(Data,PSymbols(FSymbols)^.Data^.at(Index),false);
  end;
end;

function TSymbols.Get_Count: Integer;
begin
  Result:=-1;
  if FSymbols<>NIL then begin
    REsult:=PSymbols(FSymbols)^.Data^.GetCount;
  end;
end;

procedure TSymbols.InsertSymbolDef(const AGroup: ISymbolDef);
  var FGroup    : PSGroup;
      Test      : PSGroup;
      AView     : PView;
      cnt       : integer;
      AItem     : IBase;
      hRect     : TDRect;
      AType     : integer;
      AName     : string;
      inserted  : integer;
      ASize     : double;

begin
    AView:=NIL;
    FGroup:=new(PSGroup,init);
    FGroup^.Index:=AGroup.Index;
    if AGroup.Name<>'' then FGroup.SetName(AGroup.Name);
    if AGroup.LibName<>'' then FGroup.SetLibName(AGroup.LibName);
    FGroup^.DefaultSizeType:=AGroup.DefaultSizeType;
    FGroup^.Defaultsize:=AGroup.DefaultSize;
    //FGroup^.DefaultSizeType:=0;
    //FGroup^.Defaultsize:=100000;
    FGroup^.REfPoint.init(AGroup.RefPOint.x,AGroup.RefPOint.y);
    FGroup^.Reflinelength:=AGroup.REfline.length;
    FGroup^.REflinepoint1.x:=AGroup.Refline.x1;
    FGroup^.REflinepoint1.y:=AGroup.Refline.y1;
    FGroup^.REflinepoint2.x:=AGroup.Refline.x2;
    FGroup^.REflinepoint2.y:=AGroup.Refline.y2;
    AName:=AGroup.Name;
    Atype:=AGroup.DefaultSizeType;
    ASize:=AGroup.DefaultSize;
    hrect.init;
    inserted:=0;
    for cnt:=0 to AGroup.count-1 do begin
      AItem:=AGroup.Items[cnt];
      Atype:=AItem.ObjectType;
      AView:=CopySymbolObject(AItem);
      if AView<>NIL then begin
        FGroup^.InsertItem(AView);
        inc(inserted);
        if cnt=0 then hRect.InitByRect(AView^.ClipRect) else
          hRect.CorrectByrect(AView^.ClipRect);
      end;
    end;

    //PSymbols(PProj(Data)^.PInfo^.Symbols)^.Data^.Insert(FGroup);
    //FGroup^.CalculateClipREct;
    if inserted>0 then begin
      FGroup^.ClipRect.InitByRect(hRect);
      FGroup^.Defaultsize:=AGroup.Defaultsize;
      FGroup^.DefaultSizeType:=round(AGroup.DefaultsizeType);

      //PSymbols(PProj(Data)^.Pinfo^.Symbols)^.InsertLibSymbol(PProj(Data)^.Pinfo,'',PSGroup(FGroup),false);

      PSymbols(PProj(Data)^.PInfo^.Symbols)^.CopySymIntoProject(PProj(Data)^.PInfo,PSGroup(FGroup));

      AGroup.Index:=FGroup^.Index;
      test:=NIL;
    end else begin
      Dispose(FGroup,Done);
    end;
end;



function TSymbols.IDToSymbolName(SymbolNumber: Integer): WideString;
var AGroup   : PSGroup;
    ALayer   : PLayer;
    AIndex   : TIndex;
    BIndex   : PSGroup;
begin
  REsult:='';
  if FSymbols<>NIL then begin
    AIndex.Index:=SymbolNumber;
    ALayer:=PSymbols(PProj(Data)^.Pinfo^.Symbols);
    BIndex:=PSGroup(ALayer^.HasObject(@Aindex));
    if BIndex<> NIL then Result:=BIndex^.Name^;
  end;
end;

function TSymbols.SymbolNameToID(const SymbolName: WideString): Integer;
var AGroup : PSGroup;
begin
  Result:=-1;
  if FSymbols <> NIL then begin
    AGroup:=PSymbols(PProj(Data)^.Pinfo^.Symbols)^.GetSymbol(SymbolName,'');
    if AGroup<>NIl then Result:=AGroup^.Index;
  end;
end;

function TSymbols.GetSymbolByName(const SymbolName: WideString): ISymbolDef;
begin
     Result:=nil;
     if FSymbols <> NIL then begin
        Result:=TSymbolDef.Create(Data,PSymbols(FSymbols)^.GetSymbol(SymbolName,''),False);
     end;
end;

initialization
  TAutoObjectFactory.Create(ComServer, TSymbols, Class_Symbols,
    ciInternal, tmApartment);
end.
