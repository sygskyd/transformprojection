unit AXMesLine;

interface

uses
  ComObj, ActiveX, WINGIS_TLB, StdVcl,AXBase;

type
  TMesLine = class(TBase, IMesLine)
  private
    FMesLine  : POinter;
    Data      : Pointer;
    Release   : Boolean;
    hIndex    : Integer;
  protected
    function Get_EndPoint: IPoint; safecall;
    function Get_ObjectType: Integer; safecall;
    function Get_StartPoint: IPoint; safecall;
    procedure Set_EndPoint(const Value: IPoint); safecall;
    procedure Set_StartPoint(const Value: IPoint); safecall;
    function Get_OtherStyle: WordBool; safecall;
    function Get_ObjectStyle: IObjectStyle; safecall;
    procedure Set_ObjectStyle(const Value: IObjectStyle); safecall;
    function Get_Index: Integer; safecall;
    procedure Set_Index(Value: Integer); safecall;
    procedure CreateObjectStyle; safecall;
    { Protected declarations }
  Public
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
    Constructor Create(AData,AItem:POInter;ARelease:Boolean);
{$ELSE} // MAR - commented IDB due to memory problems:
    Constructor Create(AData,AItem:POInter;ARelease:Boolean; iOwnerLayerIndex: Integer = -1);
{$ENDIF}
{-- IDB_AXInterface}
    Destructor  Destroy; override;
  end;

implementation

uses ComServ,am_Meas,AXPoint,AXDef,am_def,axobjectstyle
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
{$ELSE} // MAR - commented IDB due to memory problems:
     , Am_Index
{$ENDIF}
{-- IDB_AXInterface}
     ;

{$IFNDEF AXIDB}
Constructor TMesline.Create;
{$ELSE} // MAR - commented IDB due to memory problems:
Constructor TMesline.Create(AData,AItem:POInter;ARelease:Boolean; iOwnerLayerIndex: Integer = -1);
{$ENDIF}
  begin
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
    inherited Create;
{$ELSE} // MAR - commented IDB due to memory problems:
    Inherited Create(AData, iOwnerLayerIndex, PIndex(AItem).Index);
{$ENDIF}
{-- IDB_AXInterface}
    FMesline:=AItem;
    BaseItem:=AItem;
    Data:=AData;
    Release:=ARelease;
    hIndex:=-1;
  end;

function TMesLine.Get_ObjectType: Integer;
begin
  Result:=-1;
  if FMesLine<>NIL then begin
    REsult:=PMeasureLine(FMesLine)^.GetObjtype;
  end;
end;

Destructor TMesLine.Destroy;
  begin
    if Release Then Dispose(PMeasureLine(FMesLine),Done);
    inherited Destroy;
  end;

function TMesLine.Get_EndPoint: IPoint;
begin
  REsult:=NIL;
  if FMesLIne<>NIL then begin
    Result:=AXPoint.TPoint.Create(@PMeasureLine(FMesLine)^.EndPoint,false);
  end;
end;

function TMesLine.Get_StartPoint: IPoint;
begin
  REsult:=NIL;
  if FMesLIne<>NIL then begin
    Result:=AXPoint.TPoint.Create(@PMeasureLine(FMesLine)^.StartPoint,false);
  end;
end;

procedure TMesLine.Set_EndPoint(const Value: IPoint);
begin
  if FMesLIne<>NIL then begin
    PMeasureLine(FMesLine)^.EndPoint.x:=Value.x;
    PMeasureLine(FMesLine)^.EndPoint.y:=Value.y;
  end;
end;

procedure TMesLine.Set_StartPoint(const Value: IPoint);
begin
  if FMesLIne<>NIL then begin
    PMeasureLine(FMesLine)^.StartPoint.x:=Value.x;
    PMeasureLine(FMesLine)^.STartPoint.y:=Value.y;
  end;
end;

function TMesLine.Get_OtherStyle: WordBool;
begin
  Result:=false;
  if FMesLine <>NIL then begin
  Result:=PMeasureLIne(FMesLine)^.GetState(sf_otherStyle);
  end;
end;

function TMesLine.Get_ObjectStyle: IObjectStyle;
begin
 Result:=NIL;
  if FMesLine<>NIL then begin
    if  PMeasureLine(FMesLine)^.ObjectStyle = NIL then CreateStyle(PMeasureLine(FMesLine));
    REsult:=TObjectStyle.Create(PMeasureLine(FMesLine)^.ObjectStyle,false);
  end;
end;

procedure TMesLine.Set_ObjectStyle(const Value: IObjectStyle);
begin
  if FMesLine<>NIL then begin
    SetStyle(PMeasureLine(FMesLine),Value);
  end;
end;

function TMesLine.Get_Index: Integer;
begin
 Result:=-1;
  if FMesLine<>NIL then begin
    if HIndex<>-1 then REsult:=HIndex else
    Result:=PMeasureLIne(FMesLine)^.Index;
  end;
end;

procedure TMesLine.Set_Index(Value: Integer);
begin
 if FMesLine<>NIL then begin
   //PMeasureLine(FMesLine)^.Index:=Value;
   HIndex:=Value;
 end;
end;

procedure TMesLine.CreateObjectStyle;
begin
   CreateStyle(PMeasureLine(FMesLine));
end;

initialization
  TAutoObjectFactory.Create(ComServer, TMesLine, Class_MesLine,
   ciInternal, tmApartment);
end.
