unit AXSightLData;

interface

uses  ComObj, ActiveX, WINGIS_TLB, StdVcl,AXBase;

type
  TAXSightLData = class(TBase, ISightLData)
    FSightLData: Pointer;
    Data       : Pointer;
    Release    : Boolean;
  protected
    function Get_LIndex: Integer; safecall;
    function Get_LFlags: Integer; safecall;
    function Get_GeneralMin: Double; safecall;
    function Get_GeneralMax: Double; safecall;
  Public
    Constructor Create(AData,AItem:Pointer;ARelease:Boolean);
    Destructor  Destroy; override;
  end;

implementation

uses ComServ, am_Sight;

Constructor TAXSightLData.Create;
begin
   inherited Create;
   FSightLData:=AItem;
   Data:=AData;
   Release:=ARelease;
   BAseItem:=AItem;
end;

Destructor TAXSightLData.Destroy;
begin
   if Release Then Dispose(PSightLData(FSightLData),Done);
   inherited Destroy;
end;

function TAXSightLData.Get_LIndex: Integer;
begin
   result:=0;
   if FSightLData <> nil then
      result:=PSightLData(FSightLData)^.LIndex;
end;

function TAXSightLData.Get_LFlags: Integer;
begin
   result:=0;
   if FSightLData <> nil then
      result:=PSightLData(FSightLData)^.LFlags;
end;

function TAXSightLData.Get_GeneralMin: Double;
begin
   result:=0;
   if FSightLData <> nil then
      result:=PSightLData(FSightLData)^.GeneralMin;
end;

function TAXSightLData.Get_GeneralMax: Double;
begin
   result:=0;
   if FSightLData <> nil then
      result:=PSightLData(FSightLData)^.GeneralMax;
end;

initialization
  TAutoObjectFactory.Create(ComServer, TAXSightLData, Class_SightLData,
    ciInternal, tmApartment);

end.
