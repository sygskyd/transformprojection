unit AXCustomerItem;

interface

uses
  ComObj, ActiveX, WINGIS_TLB;

type
  TCustomerItem = class(TAutoObject, ICustomerItem)
  private
    FCustomerID : integer;
    FEdgeId     : integer;
    FPercent    : integer;
  protected
    function Get_CustomerID: Integer; safecall;
    function Get_EdgeID: Integer; safecall;
    function Get_Percent: Integer; safecall;
    procedure Set_CustomerID(Value: Integer); safecall;
    procedure Set_EdgeID(Value: Integer); safecall;
    procedure Set_Percent(Value: Integer); safecall;
    { Protected declarations }
  end;

implementation

uses ComServ;

function TCustomerItem.Get_CustomerID: Integer;
begin
REsult:=FCustomerID;
end;

function TCustomerItem.Get_EdgeID: Integer;
begin
Result:=FEdgeID;
end;

function TCustomerItem.Get_Percent: Integer;
begin
Result:=FPercent;
end;

procedure TCustomerItem.Set_CustomerID(Value: Integer);
begin
FCustomerID:=Value;
end;

procedure TCustomerItem.Set_EdgeID(Value: Integer);
begin

FEdgeID:=value;
end;

procedure TCustomerItem.Set_Percent(Value: Integer);
begin
FPercent:=Value;
end;

initialization
  TAutoObjectFactory.Create(ComServer, TCustomerItem, Class_CustomerItem,
    ciInternal, tmApartment);
end.
