unit AXList;

interface

uses
  ComObj, ActiveX, WINGIS_TLB,classes;

type
  TList = class(TAutoObject, IList)
  private
    FList           : Classes.TList;
  protected
    function Get_Count: Integer; safecall;
    function Get_Items(Index: Integer): IBase; safecall;
    procedure Add(const Item: IBase); safecall;
    { Protected declarations }
  public
    constructor create;
    destructor  destroy; override;
  end;

  TListItem = class
  public
    Item          : IDispatch;
  end;

implementation

uses ComServ;

Constructor TList.Create;
  begin
    inherited Create;
    FList:=Classes.TList.Create;
  end;

Destructor TList.Destroy;
  var cnt      : integer;
      AItem    : TListItem;
  begin
    for cnt:=0 to FList.Count-1 do begin
      AItem:=FList.Items[cnt];
      AItem.Item:=nil;
      AItem.Free;
    end;
    FList.Free;
    inherited Destroy;
  end;

function TList.Get_Count: Integer;
begin
  Result:=Flist.Count;
end;

function TList.Get_Items(Index: Integer): IBase;
 var cnt      : integer;
     AItem    : TListItem;
begin
  if Index<FList.Count then begin
    AItem:=FList[Index];
    Result:=AItem.Item as IBase;
  end;
end;

procedure TList.Add(const Item: IBase);
  var cnt      : integer;
      AItem    : TListItem;
begin
  AItem:=TListItem.Create;
  AItem.Item:=Item;
  FList.Add(AItem);
end;
initialization
  TAutoObjectFactory.Create(ComServer, TList, Class_List,
    ciInternal, tmApartment);
end.
