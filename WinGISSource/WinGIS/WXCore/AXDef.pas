unit AXDef;

interface
uses  am_view,WINGIS_Tlb,am_def;

Procedure InsertObjectOnLayer(AProj:pointer;AView:PView;BLayer:Pointer);
Function  CopySymbolObject(AItem:IBase):PView;

//AXPolygon  -> PCPoly
//AXPolyLine -> PPoly
//AXPixel .....
Function  CopyObject(AData:Pointer;Item:IBase):PView;

//ASourceLayer    ... TList mit PCPoly - Objekten
//ADestinatLayer  ... ResultLayer auf den das Island PCPoly kopiert wird
Procedure CreateIslandArea(PProject : Pointer; ASourceLayer, ADestinatLayer : Pointer);

//Loescht alle PCPoly-Objekte auf TList
Procedure FreeLayer(ALayer:Pointer);


// not ready
Procedure SetStyle(AIndex:PView;AStyle:IObjectstyle);
//create new Style if Aview^.Objectstyl = NIL
Procedure CreateStyle(AIndex:PView);
//INterface -> REcord
Procedure SetLineStyle(var AStyle:TLineStyle;BStyle:ILineStyle);
Procedure SetFillStyle(var AStyle:TFillStyle;BStyle:IFillStyle);
Procedure SetSymbolFill(var AStyle:TSymbolFill;BStyle:ISymbolFill);

//REcord -> Interface
Procedure SetIFillStyle(AStyle:TFillStyle;BStyle:IFillStyle);
Procedure SetILineStyle(AStyle:TLineStyle;BStyle:ILineStyle);
Procedure SetISymbolFill(AStyle:TSymbolFill;BStyle:ISymbolFill);


implementation

uses am_main,am_proj,am_layer,am_index,am_obj,am_Poly,am_cpoly,am_group,
     am_point,AM_Circl,am_Splin,am_text,am_font,am_sym,bmpimage,am_RText,
     am_meas,classes,Islands{,AXSymbolGroup},wintypes,win32def,sysutils,
     Extcanvas, ErrHndl;

     //berechnet Laenge von Text uah von DC,Font
Function GetTExtWidth(AText:IText):Integer;
  var SText      : array[0..255] of char;
      DC         : HDC;
      cnt2       : integer;
      Asize      : TSize;
      AspectRat  : double;
  begin
    if (AText.width = 0) and (AText.Text <> '') then begin
      strpcopy(STExt,AText.TExt);
      cnt2:=length(Atext.text);
      DC:=GetDC(0);
      SetMapMode(DC,mm_lometric);
      GetTextExtentPoint32(DC,stext,cnt2,ASize);
      AspectRat:=ASize.cx/ASize.Cy;
      ReleaseDC(0,DC);
      REsult:=round(AText.FontHeight*Aspectrat);
    end else Result:=AText.Width;
  end;


Function  CopyObject(AData:POinter;Item:IBase):PView;
var cnt         : integer;
    cnt2        : integer;
    APolyLine   : IPolyLIne;
    APolygon    : IPolygon;
    AText       : ITExt;
    ASymbol     : ISymbol;
    ASpline     : ISpline;
    APixel      : IPixel;
    ACircle     : ICircle;
    AArc        : IArc;
    AImage      : IImage;
    ADBText     : IDBText;
    AMesLine    : IMesLine;

    BPolyLine   : PPoly;
    BPixel      : PPixel;
    BText       : PText;
    BSymbol     : PSymbol;
    BCircle     : PEllipse;
    BArc        : PEllipseArc;
    BImage      : PImage;
    BDbText     : PRText;
    BMesline    : PMeasureLine;
    BSpline     : PSpline;
    BFont       : TFontData;

    APoint      : IPoint;
    BPoint      : TDPOint;
    BPolygon    : PCPoly;
    AView       : PView;
    AProj       : PProj;
    AType       : integer;
    ObjStyle    : IObjectSTyle;
    ABmpSet     : TBitMapSettings;
    dummyDbText : string;
    pdummyDbText: PCHAR;
begin
  Result:=nil;
  try
    AProj:=PProj(AData);
    AView:=NIL;
    AType:=Item.ObjectType;
    if AType=ot_Cpoly then // copy CPoly
    begin
      APolygon:=Item as IPolygon;
      BPolygon:=new(PCPoly,Init);
      for cnt:=0 to APolygon.count-1 do begin
        APoint:=APolygon.Points[cnt];
        BPOint.Init(APOint.x,APoint.y);
        BPolygon^.InsertPOint(BPOint);
      end;
      BPolygon^.ClosePoly;
      if APolygon.IslandArea then begin
        with BPolygon^ do begin
          SetState(sf_IslandArea,true);
          IslandCount:=APolygon.IsLandCount;
          GetMem(IslandInfo,IslandCount*sizeof(IslandInfo^[0]));
          for cnt:=0 to Islandcount-1 do begin
            IsLandInfo^[cnt]:=APolygon.IslandInfo[cnt];
          end;
        end;
      end;
      if (BPolygon <> nil) and (BPolygon^.Data^.Count > 2) then begin
         AView:=BPolygon;
      end;
    end
    else if AType=ot_Poly then // copy Poly
    begin
      APolyLine:=Item as IPolyLine;
      BPolyLine:=new(PPoly,init);
      for cnt:=0 to APolyline.count-1 do begin
        APoint:=APolyLine.Points[cnt];
        BPOint.Init(APOint.x,APoint.y);
        BPolyLine^.InsertPOint(BPOint);
      end;
      if (BPolyline <> nil) and (BPolyline^.Data^.Count > 1) then begin
         Aview:=BPolyline;
      end;
    end
    else if AType=ot_Text then // copy text
    begin
      AText:=Item as IText;
      BText:=new(PText,init(BFont,'',0));
      BText^.Pos.init(AText.Position.x,ATExt.Position.y);

      BText^.Font.Style:=AText.FontStyle;
      BText^.Font.Height:=AText.FontHeight;
      BText^.Font.Font:=AProj^.Pinfo^.Fonts^.IDFromName(AText.FontName);

      BTExt^.Width:=GetTExtWidth(AText);
      BText^.Angle:=AText.Angle;
      BText^.FAlign:=AText.Align;
      BText^.SetTExt(AText.Text);
      BText^.CalculateCliprect(AProj^.Pinfo);
      AView:=BText;
    end
    else if AType=ot_Pixel then // copy Pixel
    begin
      APixel:=Item as IPixel;
      BPixel:=new(PPixel,Init(BPoint));
      APoint:=APixel.Position;
      BPixel^.Position.init(APoint.x,Apoint.y);
      BPixel^.CalculateClipRect;
      AView:=BPixel;
    end
    else if AType=ot_Symbol then // copy symbol
    begin
      ASymbol:=Item as ISymbol;
      BSymbol:=new(PSymbol,Init(AProj^.PInfo,BPoint,0,0,0));
      BSymbol^.Position.INit(ASymbol.Position.x,ASymbol.Position.y);
      BSymbol^.SymIndex:=ASymbol.SymIndex;
      BSymbol^.Size:=ASymbol.Size;
      BSymbol^.SizeType:=ASymbol.SizeType;
      BSymbol^.Angle:=ASymbol.Angle;
      BSymbol^.CalculateClipREct(AProj^.PInfo);
      AView:=BSymbol;
    end
    else if AType=ot_Circle then // copy circle
    begin
      ACircle:=Item as ICircle;
      BCircle:=new(PEllipse);
      BCircle^.Init(BPOint,0,0,0);
      BCircle^.Position.Init(ACircle.Position.x,ACircle.Position.y);
      BCircle^.PrimaryAxis:=ACircle.PrimaryAxis;
      BCircle^.SecondaryAxis:=ACircle.SecondaryAxis;
      BCircle^.Angle:=ACircle.Angle;
      BCircle^.CalculateClipREct;
      AView:=BCircle;
    end
    else if AType=ot_Arc then // copy Arc
    begin
      AArc:=Item as IArc;
      BArc:=new(PEllipseArc);
      BArc^.Init(BPoint,0,0,0,0,0);
      Barc^.Position.Init(AArc.Position.x,AArc.Position.y);
      BArc^.PrimaryAxis:=AArc.PrimaryAxis;
      Barc^.SecondaryAxis:=AArc.SecondaryAxis;
      Barc^.Angle:=AArc.Angle;
      BArc^.BeginAngle:=AArc.BeginAngle;
      BArc^.EndAngle:=AArc.EndAngle;
      BArc^.CalculateClipREct;
      AView:=BArc;
    end
    else if AType=ot_MesLine then // copy Mesline
    begin
      AMesLine:=Item as IMesLIne;
      BPoint.Init(AMesline.Startpoint.x,AMesline.StartPOint.y);
      BMesLine:=new(PMeasureLine,Init(BPoint,BPoint));
      BMesLine^.State:=sf_HasArrows;
      BMesLine^.StartPoint.init(AMesline.Startpoint.x,AMesline.StartPOint.y);
      BMesLine^.EndPoint.init(AMesline.Endpoint.x,AMesline.EndPOint.y);
      BMesLine^.ClipRect.CorrectByPoint(BMesLine^.StartPoint);
      BMesLine^.ClipRect.CorrectByPoint(BMesLine^.EndPoint);
      AView:=BMesLine;
    end
    else if AType=ot_Image then // copy Image
    begin
      AIMage:=Item as IIMage;
      BImage:=new(PImage,init(AImage.FileName,AImage.RelativePath,so_ShowBMP,AProj^.PInfo^.BitmapSettings));
      BImage^.ClipRect.Assign(AImage.ClipRect.A.x,AImage.ClipRect.A.y,AImage.ClipRect.B.x,AImage.ClipRect.B.y);
      BImage^.ScaledClipRect.Assign(AImage.ClipRect.A.x,AImage.ClipRect.A.y,AImage.ClipRect.B.x,AImage.ClipRect.B.y);
      BImage^.ShowOpt:=AImage.ShowOpt;
      BImage^.Options:=AImage.Options;
      BImage^.TransparencyType:=AImage.TransparencyType;
      BImage^.Transparency:=AImage.Transparency;
      AView:=BImage;
    end
    else if AType=ot_RText then // copy RText
    begin
      ADBText:= Item as IDBText;
      BDBText:=new(PRText,init(BFont,''));
      BDBText^.TextRect.A.X:=ADBText.TextRect.A.X;
      BDBText^.TextRect.A.Y:=ADBText.TextRect.A.Y;
      BDBText^.TextRect.B.X:=ADBText.TextRect.B.X;
      BDBText^.TextRect.B.Y:=ADBText.TextRect.B.Y;
      BDBText^.Font.Style:=ADBText.FontStyle;
      BDBText^.Font.Height:=ADBText.FontHeight;
      BDBText^.Font.Font:=AProj^.Pinfo^.Fonts^.IDFromName(ADBText.FontName);
      dummydbText:=ADBText.RText;
      pdummydbText:=stralloc(length(dummydbText)+1);
      strpcopy(pdummydbText, dummydbText);
      BDBText^.RText:=StrNew(pdummydbText);
      strdispose(pdummydbText);
      BDBText^.ID:=ADBText.ID;
      BDBText^.CalculateClipRect(AProj^.Pinfo);
      AView:=BDBText;
    end
    else if AType=ot_Spline then // copy Spline
    begin
      ASpline:=Item as ISpline;
      BSPline:=new(PSpline,Init);
      for cnt:=0 to ASPline.count-1 do begin
        APoint:=ASpline.Points[cnt];
        BPOint.Init(APOint.x,APoint.y);
        BSPline^.InsertPOint(BPOint);
      end;
      BSPline^.CalculateClipREct;
      AView:=BSpline;
    end;
    if Item.OtherStyle then
    begin
      ObjStyle:=Item.ObjectStyle;
      SetStyle(Aview,ObjStyle);
    end;
    if AView <> nil then begin
       AView.GUID:=Item.GUID;
    end;
    Result:=AView;
  except
    on E: Exception do begin
       DebugMsg('AXDef.CopyObject',E.Message);
    end;
  end;
end;

Function  CopySymbolObject(AItem:IBase):PView;
 var
      cnt       : integer;
      cnt1      : integer;
      AView     : PView;
      SPolygon  : PSCPoly;
      SPolyLIne : PSPoly;
      SPixel    : PSPixel;
      SCircle   : PSEllipse;
      SArc      : PSEllipseArc;
      SSPline   : PSSPline;
      SText     : PSTExt;
      SImage    : PSImage;

      ifPolygon : IPolygon;
      ifPolyLine: IPolyLIne;
      ifPixel   : IPixel;
      ifCircle  : ICircle;
      ifArc     : IArc;
      ifSpline  : ISpline;
      ifText    : Itext;
      ifImage   : IImage;

      APoint    : TDPOint;
      AFont     : TFontData;
      ifPOint   : IPoint;
      Aspectrat : double;
      ObjStyle  : IObjectSTyle;

  begin
    AView:=NIL;
    if AItem.ObjectType = ot_cpoly then begin
      ifPolygon:=AItem as IPolygon;
      SPolygon:=new(PSCPoly,init);
      for cnt1:=0 to ifPolygon.Count-1 do begin
        ifPoint:=ifPolygon.Points[cnt1];
        APoint.init(ifPOint.x,ifPoint.y);
        SPolygon^.InsertPoint(APoint);
      end;
      SPolygon^.ClosePoly;
      SPolygon^.CalculateClipRect;
      AView:=SPolygon;
    end else if AItem.ObjectType = ot_Poly then begin
      ifPolyLine:=AItem as IPolyLIne;
      SPolyLine:=new(PSPoly,init);
      for cnt1:=0 to ifPolyLine.Count-1 do begin
        ifPoint:=ifPolyLine.Points[cnt1];
        APoint.init(ifPOint.x,ifPoint.y);
        SPolyLine^.InsertPoint(APoint);
      end;
      SPolyLine^.CalculateClipRect;
      AView:=SPolyLIne;
    end

    else if AItem.ObjectType = ot_Pixel then begin
      ifPixel:=AItem as IPixel;
      SPixel:=new(PSPixel,Init(APoint));
      SPixel^.Position.Init(ifPixel.Position.x,ifPixel.Position.y);
      SPixel^.CalculateClipRect;
      AView:=SPixel;
    end
    else if AItem.ObjectType = ot_Circle then begin
      ifCircle:= AItem as ICircle;
      SCircle:=new(PSEllipse);
      SCircle^.Init(APOint,0,0,0);
      ifPOint:=ifCircle.Position;
      SCircle^.Position.Init(ifPoint.x,ifPOint.y);
      SCircle^.PrimaryAxis:=ifCircle.PrimaryAxis;
      SCircle^.SecondaryAxis:=ifCircle.SecondaryAxis;
      SCircle^.Angle:=ifCircle.Angle;
      SCircle^.CalculateClipREct;
      AView:=SCircle;
    end else
    if AItem.ObjectType = ot_Arc then begin
      ifARc:= AItem as IArc;
      SArc:=new(PSEllipseArc);
      SArc^.Init(APoint,0,0,0,0,0);
      ifPOint:=ifArc.Position;
      SArc^.Position.Init(ifPoint.x,ifPOint.y);
      SArc^.PrimaryAxis:=ifArc.PrimaryAxis;
      Sarc^.SecondaryAxis:=ifArc.SecondaryAxis;
      Sarc^.Angle:=ifArc.Angle;
      SArc^.BeginAngle:=ifArc.BeginAngle;
      SArc^.EndAngle:=ifArc.EndAngle;
      SArc^.CalculateClipREct;
      AView:=SArc;
    end else
    if AItem.ObjectType = ot_Spline then begin
      ifSpline:=AItem as ISpline;
      SSPline:=new(PSSpline,Init);
      for cnt1:=0 to ifSpline.count-1 do begin
        ifPoint:=ifSpline.Points[cnt];
        APOint.Init(ifPOint.x,ifPoint.y);
        SSPline^.InsertPOint(APOint);
      end;
      SSPline^.CalculateClipRect;
      AView:=SSPline;
    end else
    if AItem.ObjectType = ot_Text then begin
      ifText:=AItem as IText;
      SText:=new(PSText,init(AFont,'',0));

      SText^.Pos.init(ifText.Position.x,ifText.Position.y);
      SText^.Font.Style:=ifText.FontStyle;
      SText^.Font.Height:=ifText.FontHeight;
      SText^.Font.Font:=ifText.FontNumber;

      SText^.Width:=GetTExtWidth(ifText);
      SText^.Angle:=ifText.Angle;
      SText^.FAlign:=ifText.Align;
      SText^.SetTExt(ifText.Text);

      SText^.CalculateClipRect({PProj(Data)^.Pinfo}NIL);
      AView:=SText;
    end;

    if AItem.ObjectType = ot_Image then begin
      ifImage:=AItem as IImage;
      SImage:=New(PSImage,Init(ifImage.Filename,ifImage.ShowOpt,WingisMainForm.ActualChild.Data^.PInfo^.BitmapSettings,FALSE));

      SImage^.ClipRect.A.X:=ifImage.ClipRect.A.X;
      SImage^.ClipRect.A.Y:=ifImage.ClipRect.A.Y;
      SImage^.ClipRect.B.X:=ifImage.ClipRect.B.X;
      SImage^.ClipRect.B.Y:=ifImage.ClipRect.B.Y;

      AView:=SImage;
    end;


    if AItem.OtherStyle then // set objectstyle of symbol item
    begin
      ObjStyle:=AItem.ObjectSTyle;
      SetStyle(Aview,ObjStyle);
    end;

    if AView <> nil then
    begin
       if AView^.cliprect.XSize=0 then
       begin
          if AView^.cliprect.Ysize<>0 then
          begin
             AView^.cliprect.Grow(round(AView^.cliprect.Ysize/10));
          end;
       end;
       if AView^.ClipRect.YSize=0 then
       begin
          if AView^.cliprect.xSize<>0 then
          begin
             AView^.cliprect.grow(round(AView^.cliprect.xsize/10));
          end;
       end;
    end;
    Result:=AView;
  end;

Procedure InsertObjectOnLayer(AProj:pointer;AView:PView;BLayer:Pointer);
var
ALayer: PLayer;
LView: PIndex;
begin
    try
       ALayer:=PLayer(BLayer);
       if ALayer<>NIL then begin
          PLayer(PProj(AProj)^.PInfo^.Objects)^.InsertObject(PProj(AProj)^.PInfo,AView,TRUE);           {in Objektliste aufnehmen}
          LView:=New(PIndex,Init(AView^.Index));
          LView^.ClipRect.InitByRect(AView^.ClipRect);
          if PProj(AProj)^.Layers^.IndexObject(PProj(AProj)^.PInfo,LView) <> NIL then begin            {Objekt in Objektliste vorhanden?}
             if AView^.GetState(sf_otherstyle) then begin
                LView^.ChangeStyle(PProj(AProj)^.PInfo,AView^.ObjectSTyle^,FALSE);
             end;
             ALayer^.InsertObject(PProj(AProj)^.PInfo,LView,false); // false .. no redraw
             PProj(AProj)^.CorrectSize(AView^.ClipRect,FALSE);
             PProj(AProj)^.SetModified;
          end;
       end;
    except
       on E: Exception do begin
          DebugMsg('InsertObjectOnLayer',E.Message);
       end;
    end
end;

Procedure CreateIslandArea
   (
   PProject         : Pointer;
   aSourceLayer     : Pointer;
   aDestinatLayer   : POinter
   );
  var Proj         : PProj;
      A1, A2, A3   : PCPoly;
      P1           : PIndex;
      SelLayer     : TList;
      Island1      : PIslandStruct;
      Island2      : PIslandStruct;
      Cnt          : LongInt;
      DestLayer    : TList;
      IItem        : PIndex;
  begin
    Proj:=PProj(PProject);
    SelLayer:=aSourceLayer;
    DestLayer:=aDestinatLayer;
    if (SelLayer.Count>=2) then begin
      A1:=PCPoly(SelLayer[0]);
      Island1:=PolyToIslandStruct(Proj^.PInfo,A1);
      for Cnt:=1 to SelLayer.Count-1 do begin
        A2:=PCPoly(SelLayer[cnt]);
        Island2:=PolyToIslandStruct(Proj^.PInfo,A2);
        Island1:=InsertIsland(Proj^.PInfo,Island1,Island2);
      end;
      A3:=IslandStructToPoly(Island1,TRUE);
      if A3<>NIL then DestLayer.Add(A3);
      DestroyIslandStruct(Island1,TRUE);
    end
    else begin
      if (SelLayer.Count = 1) then begin
        if PView(SelLayer[0])^.GetObjType = ot_CPoly then begin
          DestLayer.Add(SelLayer[0]);
        end;
      end;
    end;
  end;

Procedure FreeLayer(ALayer:Pointer);
  var AList : TList;
      AItem : PView;
      cnt   : integer;
  begin
    if alist<>NIL then begin
      AList:=TList(ALayer);
      for cnt:=0 to Alist.count-1 do begin
        AItem:=Alist[cnt];
        if AItem.GetObjType=ot_Cpoly then
        Dispose(AItem,Done);
      end;
      AList.free
    end;
  end;

  
Procedure SetStyle(AIndex:PView;AStyle:IObjectstyle);
  begin
    if (AIndex<>nil) then begin
      if (AIndex^.ObjectStyle<>nil) then begin
        Dispose(AIndex^.ObjectStyle);           { eventuell eigenen Stil von AIndex l�schen }
        AIndex^.ObjectStyle:=nil;
        AIndex^.State:=AIndex^.State and (not sf_OtherStyle);
      end;
      if AIndex^.ObjectStyle=NIL then AIndex^.ObjectStyle:=new(PObjStyleV2);  { neuen ObjektStil erzeugen}
      AIndex^.State:=AIndex^.State or sf_OtherStyle;                          { f�r Speicherung setzen }
      SetLineStyle(AIndex^.ObjectStyle^.LineStyle,AStyle.LineStyle);
      SetFillStyle(AIndex^.ObjectStyle^.FillStyle,AStyle.FillStyle);
      SetSymbolFill(AIndex^.ObjectStyle^.SymbolFill,AStyle.SymbolFill);
    end;
  end;

Procedure CreateStyle(AIndex:PView);
  begin
    if (AIndex<>nil) then begin
      if (AIndex^.ObjectStyle<>nil) then begin
        Dispose(AIndex^.ObjectStyle);           { eventuell eigenen Stil von AIndex l�schen }
        AIndex^.ObjectStyle:=nil;
        AIndex^.State:=AIndex^.State and (not sf_OtherStyle);
      end;
      if AIndex^.ObjectStyle=NIL then AIndex^.ObjectStyle:=new(PObjStyleV2);  { neuen ObjektStil erzeugen}
      AIndex^.State:=AIndex^.State or sf_OtherStyle;                 { f�r Speicherung setzen }
      AIndex^.ObjectStyle^.LineStyle:=DefaultLineStyle;
      AIndex^.ObjectSTyle^.FillStyle:=DefaultFillStyle;
      AIndex^.ObjectStyle^.SymbolFill:=DefaultSymbolFill;
    end;
  end;

Procedure SetLineStyle(var AStyle:TLineStyle;BStyle:ILineStyle);
  begin
    AStyle.Style:=BStyle.Style;
    AStyle.Width:=BSTyle.Width;
    AStyle.WidthType:=BStyle.WidthType;
    AStyle.Scale:=BStyle.Scale;
    AStyle.Color:=BSTyle.Color;
    AStyle.FillColor:=BSTyle.FillColor;
  end;

Procedure SetFillStyle(var AStyle:TFillStyle;BStyle:IFillStyle);
  begin
    AStyle.Pattern:=BStyle.Pattern;
    AStyle.ForeColor:=BStyle.ForeColor;
    AStyle.BackColor:=BStyle.BackColor;
    AStyle.Scale:=BStyle.Scale;
    AStyle.ScaleType:=BStyle.ScaleType;
  end;

Procedure SetSymbolFill(var AStyle:TSymbolFill;BStyle:ISymbolFill);
  begin
    AStyle.Filltype:=BStyle.Filltype;
    AStyle.Symbol:=BStyle.Symbol;
    AStyle.Size:=BStyle.Size;
    AStyle.SizeType:=BStyle.SizeType;
    AStyle.SymbolAngle:=BStyle.SymbolAngle;
    AStyle.DistanceType:=BStyle.DistanceType;
    AStyle.Distance:=BStyle.Distance;
    AStyle.Offset:=BStyle.Offset;
    AStyle.OffsetType:=BStyle.Offsettype;
    AStyle.Angle:=BStyle.Angle;
    SetLineStyle(AStyle.LineStyle,BSTyle.LineStyle);
    SetFillStyle(AStyle.FillStyle,BStyle.FillStyle);
  end;

Procedure SetIFillStyle(AStyle:TFillStyle;BStyle:IFillStyle);
  begin
    BStyle.Pattern:=AStyle.Pattern;
    BStyle.ForeColor:=AStyle.ForeColor;
    BStyle.BackColor:=AStyle.BackColor;
    BStyle.Scale:=AStyle.Scale;
    BStyle.ScaleType:=AStyle.ScaleType;
  end;

Procedure SetILineStyle(AStyle:TLineStyle;BStyle:ILineStyle);
  begin
    BStyle.Style:=AStyle.Style;
    BStyle.Width:=ASTyle.Width;
    BStyle.WidthType:=AStyle.WidthType;
    BStyle.Scale:=AStyle.Scale;
    BStyle.Color:=ASTyle.Color;
    BStyle.FillColor:=ASTyle.FillColor;
  end;

Procedure SetISymbolFill(AStyle:TSymbolFill;BStyle:ISymbolFill);
  begin
    BStyle.Filltype:=AStyle.Filltype;
    BStyle.Symbol:=AStyle.Symbol;
    BStyle.Size:=AStyle.Size;
    BStyle.SizeType:=AStyle.SizeType;
    BStyle.SymbolAngle:=AStyle.SymbolAngle;
    BStyle.DistanceType:=AStyle.DistanceType;
    BStyle.Distance:=AStyle.Distance;
    BStyle.Offset:=AStyle.Offset;
    BStyle.OffsetType:=AStyle.Offsettype;
    BStyle.Angle:=AStyle.Angle;
    SetILineStyle(AStyle.LineStyle,BSTyle.LineStyle);
    SetIFillStyle(AStyle.FillStyle,BStyle.FillStyle);
  end;

end.
