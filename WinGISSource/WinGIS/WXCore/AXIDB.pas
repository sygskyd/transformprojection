unit AXIDB;
{
Internal database.
The class provides the AX interface of Internal database.
    Through this interface an external object can get the access
    to attribute information in Internal database.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 22-11-2000
}

interface

uses ComObj, WinGis_tlb, IDB_Man;

type
  TAXIDB = class(TAutoObject, IIDB)
  private
    AIDB: TIDB; // This is pointer to the WinGIS Internal database manager.
    AProject: Pointer; // This is a pointer to the project to that the item belongs.
    iLayerIndex: Integer; // This is index of layer to that the item belongs.
                                 // This information is required to getting attribute data from Internal Database.
    iItemIndex: Integer; // This is index of item to that the AXIDB interface belongs.

    function GotoObjectRecord: WordBool; safecall; // Before we can get attribute information about
                                 // item, we need to go to the rrquired record in the attribute table.
  protected
    function Get_FieldCount: Integer; safecall;
    function Get_IDBOn: integer; safecall;
    procedure Set_IDBOn(Value: integer); safecall;
    function FieldName(FieldIndex: Integer): WideString; safecall;

    function FieldValueByFieldIndex(FieldIndex: Integer): OleVariant; safecall;
    function FieldValueByFieldName(const FieldName: WideString): OleVariant; safecall;

    procedure SetFieldValueByFieldIndex(FieldIndex: Integer; FieldValue: OleVariant); safecall;
    procedure SetFieldValueByFieldName(const FieldName: WideString; FieldValue: OleVariant); safecall;

    procedure Set_OwnerLayerIndex(OwnerLayerIndex: Integer); safecall;
  public
    property IDBOn: integer read Get_IDBOn write Set_IDBOn;
    constructor Create(AProject: Pointer = nil; iOwnerLayerIndex: Integer = -1; iItemIndex: Integer = 0);
    destructor Destroy; override;
  end;
{
    TAXIDBStruct = Class(TAutoObject, IIDBStruct)
       Private

       Protected
           function Get_FieldCount: Integer; safecall;
           function FieldName(FieldIndex: Integer): WideString; safecall;
           procedure AddField(const FieldName: WideString; const FieldType: WideString; FieldSize: Integer); safecall;
           procedure DeleteField(const FieldName: WideString); safecall;
       Public

       End;
}
implementation

uses ComServ,
  Am_Main;

constructor TAXIDB.Create(AProject: Pointer = nil; iOwnerLayerIndex: Integer = -1; iItemIndex: Integer = 0);
begin
  inherited Create;
     // If we don't use Internal database, the pointer to it will be set as NIL.
     // If the pointer to project equals NIL, the pointer to IDB will be set as NIL, too.
     // Otherwise, we set the pointer to IDB.
{$IFNDEF AXDLL} // <----------------- AXDLL
  if (WinGisMainForm.WeAreUsingTheIDB[AProject]) and (AProject <> nil) then
    SELF.AIDB := WinGisMainForm.IDB_Man
  else
{$ENDIF}
    SELF.AIDB := nil;
  SELF.AProject := AProject;
  SELF.iLayerIndex := iOwnerLayerIndex;
  SELF.iItemIndex := iItemIndex;
end;

destructor TAXIDB.Destroy;
begin
  inherited Destroy;
end;

function TAXIDB.GotoObjectRecord: WordBool; safecall;
begin
  RESULT := FALSE;
  if SELF.AIDB = nil then
    EXIT;
  RESULT := SELF.AIDB.X_GotoObjectRecord(SELF.AProject, SELF.iLayerIndex, SELF.iItemIndex);
end;

function TAXIDB.Get_FieldCount: Integer; safecall;
begin
  RESULT := 0;
  if SELF.AIDB = nil then
    EXIT;
  if not SELF.GotoObjectRecord then
    EXIT;
  RESULT := SELF.AIDB.X_GetFieldCount(SELF.AProject, SELF.iLayerIndex);
end;

function TAXIDB.FieldName(FieldIndex: Integer): WideString; safecall;
begin
  RESULT := 'ProgisID';
  if SELF.AIDB = nil then
    EXIT;
  if not SELF.GotoObjectRecord then
    EXIT;
  RESULT := SELF.AIDB.X_GetFieldName(SELF.AProject, SELF.iLayerIndex, FieldIndex);
end;

function TAXIDB.FieldValueByFieldIndex(FieldIndex: Integer): OleVariant; safecall;
begin
  RESULT := 0;
  if SELF.AIDB = nil then
    EXIT;
  if not SELF.GotoObjectRecord then
    EXIT;
  RESULT := SELF.AIDB.X_GetFieldValue(SELF.AProject, SELF.iLayerIndex, FieldIndex);
end;

function TAXIDB.FieldValueByFieldName(const FieldName: WideString): OleVariant; safecall;
begin
  RESULT := 0;
  if SELF.AIDB = nil then
    EXIT;
  if not SELF.GotoObjectRecord then
    EXIT;
  RESULT := SELF.AIDB.X_GetFieldValue(SELF.AProject, SELF.iLayerIndex, FieldName);
end;

procedure TAXIDB.SetFieldValueByFieldIndex(FieldIndex: Integer; FieldValue: OleVariant);
begin
  if FieldValue = varNULL then
    EXIT;
  if not SELF.AIDB.X_AddRecord(SELF.AProject, SELF.iLayerIndex, SELF.iItemIndex) then
    EXIT;
  SELF.AIDB.X_SetFieldValue(SELF.AProject, SELF.iLayerIndex, FieldIndex, FieldValue);
end;

procedure TAXIDB.SetFieldValueByFieldName(const FieldName: WideString; FieldValue: OleVariant);
begin
  if FieldValue = varNULL then
    EXIT;
  if not SELF.AIDB.X_AddRecord(SELF.AProject, SELF.iLayerIndex, SELF.iItemIndex) then
    EXIT;
  SELF.AIDB.X_SetFieldValue(SELF.AProject, SELF.iLayerIndex, FieldName, FieldValue);
end;

{ This sets the index of layer to that the item belongs.
  Cause: we can create the item (while item is created, the index of layer will be set as
  index of TOP layer of the prject) and then we can insert the item on some layer (not
  obligatory on TOP layer of the project).
  Just after we need to set the correct index of layer to that the item belongs.
  See AXDLayer.pas procedure InsertObject.}

procedure TAXIDB.Set_OwnerLayerIndex(OwnerLayerIndex: Integer);
begin
  SELF.iLayerIndex := OwnerLayerIndex;
end;

{
function TAXIDBStruct.Get_FieldCount: Integer;
Begin
     RESULT := 0;
End;

function TAXIDBStruct.FieldName(FieldIndex: Integer): WideString;
Begin
     RESULT := 'ProgisID';
End;

procedure TAXIDBStruct.AddField(const FieldName: WideString; const FieldType: WideString; FieldSize: Integer);
Begin
//
End;

procedure TAXIDBStruct.DeleteField(const FieldName: WideString);
Begin
//
End;
}

function TAXIDB.Get_IDBOn: integer;
var
  UsingIDB: boolean;
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  UsingIDB := WinGISMainForm.WeAreUsingTheIDB[AProject];
  if UsingIDB then
    Result := 1
  else
    Result := 0;
{$ENDIF}
end;

procedure TAXIDB.Set_IDBOn(Value: integer);
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  if Value = 1 then
    WinGISMainForm.WeAreUsingTheIDB[AProject] := true
  else
    WinGISMainForm.WeAreUsingTheIDB[AProject] := false;
{$ENDIF}
end;

initialization
  TAutoObjectFactory.Create(ComServer, TAXIDB, Class_IDB, ciInternal, tmApartment);
//  TAutoObjectFactory.Create(ComServer, TAXIDBStruct, Class_IDBStruct, ciInternal, tmApartment);

end.

