unit AXFonts;

interface

uses
  ComObj, ActiveX, WINGIS_TLB, StdVcl;

type
  TFonts = class(TAutoObject, IFonts)
    FFonts   : POinter;
    Data     : POinter;   //PProj
  protected
    function Get_Count: Integer; safecall;
    function Get_Items(Index: Integer): IFontDef; safecall;
    procedure Clear; safecall;
    function FontNameToID(const FontName: WideString): Integer; safecall;
    function IDToFontName(FontNumber: Integer): WideString; safecall;
    function FontInstalled(const FontName: WideString): WordBool; safecall;

    { Protected declarations }
  public
    constructor create(AData:Pointer; AItem:POinter);   //PProj, Font
  end;

implementation

uses ComServ, am_Font, AXFontDef;

constructor TFonts.Create;
  begin
    inherited create;
    Data:=AData;
    FFonts:=AItem;
  end;

function TFonts.Get_Count: Integer;
begin
  Result:=-1;
  if FFonts<>NIL then begin
    REsult:=PFonts(FFonts)^.Count;
  end;
end;


function TFonts.Get_Items(Index: Integer): IFontDef;
begin
  Result:=NIL;
  if FFonts<>NIL then begin
    REsult:=TFontDef.Create(DAta,PFonts(FFonts)^.at(Index),false);
  end;
end;

procedure TFonts.Clear;
begin
  PFonts(FFonts)^.Freeall;
  PFonts(FFonts)^.Init;
end;

function TFonts.FontNameToID(const FontName: WideString): Integer;
begin
  REsult:=-1;
  if FFonts<>NIL then begin
    REsult:=PFonts(FFonts)^.IDFromName(FontName);
  end;
end;

function TFonts.IDToFontName(FontNumber: Integer): WideString;
var FontDes : PFontDes;
begin
  REsult:='not defined';
  if FFonts<>NIL then begin
    FontDes:=PFonts(FFonts)^.GetFont(FontNumber);
    Result:=FontDes^.Name^;
  end;
end;

function TFonts.FontInstalled(const FontName: WideString): WordBool;
begin
  REsult:=false;
  if FFonts<>NIL then begin
    if PFonts(FFonts)^.IDFromName(FontName)<> 0 then REsult:=true else
    REsult:=false;
  end;
end;

initialization
 TAutoObjectFactory.Create(ComServer, TFonts, Class_Fonts,
    ciInternal, tmApartment);
end.
