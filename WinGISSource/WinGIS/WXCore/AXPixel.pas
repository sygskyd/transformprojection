unit AXPixel;

interface

uses
  ComObj, ActiveX, WINGIS_TLB, StdVcl, AXBase;

type
  TPixel = class(TBase, IPixel)
  private
    FPixel: POinter;
    Data: Pointer;
    Release: Boolean;
    hIndex: Integer;
  protected
    function Get_ObjectType: Integer; safecall;
    function Get_Position: IPoint; safecall;
    procedure Set_Position(const Value: IPoint); safecall;
    function Get_OtherStyle: WordBool; safecall;
    function Get_ObjectStyle: IObjectStyle; safecall;
    procedure Set_ObjectStyle(const Value: IObjectStyle); safecall;
    function Get_Index: Integer; safecall;
    procedure Set_Index(Value: Integer); safecall;
    procedure CreateObjectStyle; safecall;
    function Get_ClipRect: IRect; safecall;
    procedure MoveAndRedraw(NewX, NewY: Integer); safecall;
    procedure Redraw; safecall;
    procedure FollowObject; safecall;
    procedure ZoomToObject; safecall;
    { Protected declarations }
  public
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
    constructor Create(AData, AItem: POInter; ARelease: Boolean);
{$ELSE} // MAR - commented IDB due to memory problems:
    constructor Create(AData, AItem: POInter; ARelease: Boolean; iOwnerLayerIndex: Integer = -1);
{$ENDIF}
{-- IDB_AXInterface}
    destructor Destroy; override;
  end;

implementation

uses ComServ, am_Point, AXPoint, am_def, axdef, axobjectstyle, AxRect, am_Proj, am_Projp
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
{$ELSE} // MAR - commented IDB due to memory problems:
  , AM_Index
{$ENDIF}
{-- IDB_AXInterface}
  ;

{$IFNDEF AXIDB}

constructor TPixel.Create(AData, AItem: POinter; ARelease: Boolean);
{$ELSE} // MAR - commented IDB due to memory problems:

constructor TPixel.Create(AData, AItem: POinter; ARelease: Boolean; iOwnerLayerIndex: Integer = -1);
{$ENDIF}
begin
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
  inherited Create;
{$ELSE} // MAR - commented IDB due to memory problems:
  inherited Create(AData, iOwnerLayerIndex, PIndex(AItem).Index);
{$ENDIF}
{-- IDB_AXInterface}
  FPixel := AItem;
  BaseItem := AItem;
  Data := AData;
  Release := ARelease;
  hIndex := -1;
end;

destructor TPixel.Destroy;
begin
  if Release then
    Dispose(PPixel(FPixel), Done);
  inherited Destroy;
end;

function TPixel.Get_ObjectType: Integer;
begin
  Result := -1;
  if FPixel <> nil then
  begin
    REsult := PPixel(FPixel)^.GetObjtype;
  end;
end;

function TPixel.Get_Position: IPoint;
begin
  Result := nil;
  if FPixel <> nil then
  begin
    Result := AXPoint.TPoint.Create(@PPixel(FPixel).Position, false);
  end;
end;

procedure TPixel.Set_Position(const Value: IPoint);
begin
  if FPixel <> nil then
  begin
    PPixel(FPixel)^.Position.x := Value.x;
    PPixel(FPixel)^.Position.y := Value.y;
  end;
end;

function TPixel.Get_OtherStyle: WordBool;
begin
  REsult := false;
  if FPixel <> nil then
  begin
    Result := PPixel(Fpixel)^.Getstate(sf_otherstyle);
  end;
end;

function TPixel.Get_ObjectStyle: IObjectStyle;
begin
  Result := nil;
  if FPixel <> nil then
  begin
    if PPixel(FPixel)^.ObjectStyle <> nil then
      REsult := TObjectStyle.Create(PPixel(FPixel)^.ObjectStyle, false);
  end;
end;

procedure TPixel.Set_ObjectStyle(const Value: IObjectStyle);
begin
  if FPixel <> nil then
  begin
    SetStyle(PPixel(FPixel), Value);
  end;
end;

function TPixel.Get_Index: Integer;
begin
  Result := -1;
  if FPixel <> nil then
  begin
    if Hindex <> -1 then
      result := HIndex
    else
      Result := PPixel(FPixel)^.Index;
  end;
end;

procedure TPixel.Set_Index(Value: Integer);
begin
  if FPixel <> nil then
  begin
   //PPixel(FPixel)^.Index:=Value;
    HIndex := Value;
  end;
end;

procedure TPixel.CreateObjectStyle;
begin
  CreateStyle(PPixel(FPixel));
end;

function TPixel.Get_ClipRect: IRect;
begin
  Result := nil;
  if FPixel <> nil then
  begin
    REsult := AXRect.TRect.Create(@PPixel(FPixel).ClipRect, false);
  end;
end;

// method is used to move the object to the new position
// and redraw it there

procedure TPixel.MoveAndRedraw(NewX, NewY: Integer);
begin
  PPixel(FPixel)^.Invalidate(PProj(Data)^.PInfo);
  PPixel(FPixel)^.Position.X := NewX;
  PPixel(FPixel)^.Position.Y := NewY;
  PPixel(FPixel)^.CalculateClipRect;
  PPixel(FPixel)^.Invalidate(PProj(Data)^.PInfo);
  PProj(Data)^.UpdateClipRect(PPixel(FPixel));
  PProj(Data)^.CorrectSize(PPixel(FPixel)^.ClipRect, TRUE);
  PProj(Data)^.PInfo^.RedrawInvalidate;
end;

// method to redraw pixel object

procedure TPixel.Redraw;
begin
  PPixel(FPixel)^.Invalidate(PProj(Data)^.PInfo);
  PPixel(FPixel)^.CalculateClipRect;
  PPixel(FPixel)^.Invalidate(PProj(Data)^.PInfo);
  PProj(Data)^.UpdateClipRect(PPixel(FPixel));
  PProj(Data)^.CorrectSize(PPixel(FPixel)^.ClipRect, TRUE);
  PProj(Data)^.PInfo^.RedrawInvalidate;
end;

// method is used to make shure that object is on current screen

procedure TPixel.FollowObject;
var
  CurrentView: TDRect;
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  PProj(Data)^.PInfo^.GetCurrentScreen(CurrentView);
  if ((PPixel(FPixel)^.Position.X <= CurrentView.A.X) or (PPixel(FPixel)^.Position.X >= CurrentView.B.X) or
    (PPixel(FPixel)^.Position.Y <= CurrentView.A.Y) or (PPixel(FPixel)^.Position.Y >= CurrentView.B.Y)) then
  begin
      // the object is outside of the current screen
      // so it has to be rezoomed
    FuncZoomToObject(PProj(Data), PPixel(FPixel)^.Index);
  end;
{$ENDIF}
end;

// method is used to zoom to pixel

procedure TPixel.ZoomToObject;
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  FuncZoomToObject(PProj(Data), PPixel(FPixel)^.Index);
{$ENDIF}
end;

initialization
  TAutoObjectFactory.Create(ComServer, TPixel, Class_Pixel,
    ciInternal, tmApartment);
end.


