unit AXArc;

interface

uses
  ComObj, ActiveX, WINGIS_TLB, StdVcl,AXBase;

type
  TArc = class(TBase, IArc)
    FArc   : Pointer;
    Data      : Pointer;
    Release   : Boolean;
    hIndex    : Integer;
  protected
    function Get_ObjectType: Integer; safecall;
    function Get_Angle: Double; safecall;
    function Get_BeginAngle: Double; safecall;
    function Get_EndAngle: Double; safecall;
    function Get_Position: IPoint; safecall;
    function Get_PrimaryAxis: Integer; safecall;
    function Get_SecondaryAxis: Integer; safecall;
    procedure Set_Angle(Value: Double); safecall;
    procedure Set_BeginAngle(Value: Double); safecall;
    procedure Set_EndAngle(Value: Double); safecall;
    procedure Set_Position(const Value: IPoint); safecall;
    procedure Set_PrimaryAxis(Value: Integer); safecall;
    procedure Set_SecondaryAxis(Value: Integer); safecall;
    function Get_Length: Double; safecall;
    function Get_Radius: Integer; safecall;
    procedure Set_Radius(Value: Integer); safecall;
    function Get_OtherStyle: WordBool; safecall;
    function Get_ObjectStyle: IObjectStyle; safecall;
    procedure Set_ObjectStyle(const Value: IObjectStyle); safecall;
    function Get_Index: Integer; safecall;
    procedure Set_Index(Value: Integer); safecall;
    procedure CreateObjectStyle; safecall;
    function Get_ClipRect: IRect; safecall;
    { Protected declarations }
  Public
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
    Constructor Create(AData,AItem:POInter;ARelease:Boolean);
{$ELSE} // MAR - commented IDB due to memory problems
    Constructor Create(AData,AItem:POInter;ARelease:Boolean; iOwnerLayerIndex: Integer = -1);
{$ENDIF}
{-- IDB_AXInterface}
    Destructor  Destroy; override;
  end;

implementation

uses ComServ,am_circl,am_def,axdef,AXPoint,axobjectstyle, AxRect
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
{$ELSE} // MAR - commented IDB due to memory problems:
     , Am_Index
{$ENDIF}
{-- IDB_AXInterface}
     ;

{$IFNDEF AXIDB}
Constructor TArc.Create;
{$ELSE} // MAR - commented IDB due to memory problems:
Constructor TArc.Create(AData,AItem:POInter;ARelease:Boolean; iOwnerLayerIndex: Integer = -1);
{$ENDIF}
  begin
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
    inherited Create;
{$ELSE} // MAR - commented IDB due to memory problems
    Inherited Create(AData, iOwnerLayerIndex, PIndex(AItem).Index);
{$ENDIF}
{-- IDB_AXInterface}
    FArc:=AItem;
    BaseItem:=AItem;
    Data:=AData;
    Release:=ARelease;
    hIndex:=-1;
  end;

Destructor TArc.Destroy;
  begin
    if Release Then Dispose(PEllipseArc(FArc),Done);
    inherited Destroy;
  end;

function TArc.Get_ObjectType: Integer;
begin
  Result:=-1;
  if FArc<>NIL then begin
    REsult:=PEllipseArc(FARc)^.GetObjtype;
  end;
end;

function TArc.Get_Angle: Double;
begin
  Result:=-1;
  if FArc<>NIL then begin
    REsult:=PEllipseArc(FARc)^.Angle;
  end;
end;

function TArc.Get_BeginAngle: Double;
begin
  Result:=-1;
  if FArc<>NIL then begin
    REsult:=PEllipseArc(FARc)^.BeginAngle;
  end;
end;

function TArc.Get_EndAngle: Double;
begin
  Result:=-1;
  if FArc<>NIL then begin
    REsult:=PEllipseArc(FARc)^.EndAngle;
  end;
end;

function TArc.Get_Position: IPoint;
//var APoint : PDPoint;
begin
  Result:=NIL;
  if FArc<>NIL then begin
    //APOint:=new(PDPOint,Init(PEllipseArc(FArc)^.Position.x,PEllipseArc(FArc)^.Position.y));
    //Result:=AXPoint.TPoint.Create(APoint,true);
    Result:=AXPoint.TPoint.Create(@PEllipseArc(FArc)^.Position,false);
  end;
end;

function TArc.Get_PrimaryAxis: Integer;
begin
  Result:=-1;
  if FArc<>NIL then begin
    REsult:=PEllipseArc(FARc)^.PrimaryAxis;
  end;
end;

function TArc.Get_SecondaryAxis: Integer;
begin
  Result:=-1;
  if FArc<>NIL then begin
    REsult:=PEllipseArc(FARc)^.SecondaryAxis;
  end;
end;

procedure TArc.Set_Angle(Value: Double);
begin
  if FArc<>NIL then begin
    PEllipseArc(FArc)^.Angle:=Value;
  end;
end;

procedure TArc.Set_BeginAngle(Value: Double);
begin
  if FArc<>NIL then begin
    PEllipseArc(FArc)^.BeginAngle:=Value;
  end;
end;

procedure TArc.Set_EndAngle(Value: Double);
begin
  if FArc<>NIL then begin
    PEllipseArc(FArc)^.EndAngle:=Value;
  end;
end;

procedure TArc.Set_Position(const Value: IPoint);
begin
  if FArc<>NIL then begin
    PEllipseArc(FArc)^.Position.x:=Value.x;
    PEllipseArc(FArc)^.Position.y:=Value.y;
  end;
end;

procedure TArc.Set_PrimaryAxis(Value: Integer);
begin
  if FArc<>NIL then begin
    PEllipseArc(FArc)^.PrimaryAxis:=Value;
  end;
end;

procedure TArc.Set_SecondaryAxis(Value: Integer);
begin
  if FArc<>NIL then begin
    PEllipseArc(FArc)^.SecondaryAxis:=Value;
  end;
end;


function TArc.Get_Length: Double;
begin
  Result:=-1;
  if FArc<>NIL then begin
    REsult:=PEllipseArc(FARc)^.Laenge;
  end;
end;

function TArc.Get_Radius: Integer;
begin
  Result:=-1;
  if FArc<>NIL then begin
    if PEllipseArc(FARc)^.SecondaryAxis = PEllipseArc(FARc)^.PrimaryAxis then
    REsult:=PEllipseArc(FARc)^.SecondaryAxis;
  end;
end;

procedure TArc.Set_Radius(Value: Integer);
begin
  if FArc<>NIL then begin
    PEllipseArc(FArc)^.PrimaryAxis:=Value;
  end;
end;

function TArc.Get_OtherStyle: WordBool;
begin
  Result:=false;
  if FArc<>NIL then begin
    Result:=PEllipseArc(FArc)^.GetState(sf_otherStyle);
  end;
end;

function TArc.Get_ObjectStyle: IObjectStyle;
begin
  Result:=NIL;
  if FArc<>NIL then begin
    if  PEllipseArc(FArc)^.ObjectStyle <> NIL then 
       REsult:=TObjectStyle.Create(PEllipseArc(FArc)^.ObjectStyle,false);
  end;
end;

procedure TArc.Set_ObjectStyle(const Value: IObjectStyle);
begin
 if FArc<>NIL then begin
    SetStyle(PEllipseArc(FArc),Value);
  end;
end;

function TArc.Get_Index: Integer;
begin
  Result:=-1;
  if FArc<>NIL then begin
    if HIndex<>-1 then Result:=HIndex else
    Result:=PEllipseArc(FArc)^.Index;
  end;
end;

procedure TArc.Set_Index(Value: Integer);
begin
 if FArc<>NIL then begin
   //PEllipseArc(FArc)^.Index:=Value;
   HIndex:=Value;
 end;
end;

procedure TArc.CreateObjectStyle;
begin
   CreateStyle(PEllipseArc(FArc));
end;

function TArc.Get_ClipRect: IRect;
begin
   Result:=NIL;
   if FArc <> NIL then
   begin
      REsult:=AXRect.TRect.Create(@PEllipseArc(FArc).ClipRect,false);
   end;
end;

initialization
  TAutoObjectFactory.Create(ComServer, TArc, Class_Arc,
    ciInternal, tmApartment);
end.
