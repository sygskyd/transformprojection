unit AXText;

interface

uses
  ComObj, ActiveX, WINGIS_TLB, StdVcl, AXBase, SysUtils;

type
  TText = class(TBase, IText)
  private
    FText     : Pointer;
    Data      : Pointer;
    Release   : Boolean;
    hIndex    : Integer;
  protected
    function Get_Angle: Integer; safecall;
    function Get_ObjectType: Integer; safecall;
    function Get_Position: IPoint; safecall;
    function Get_Text: WideString; safecall;
    function Get_Width: Integer; safecall;
    procedure Set_Angle(Value: Integer); safecall;
    procedure Set_Position(const Value: IPoint); safecall;
    procedure Set_Text(const Value: WideString); safecall;
    procedure Set_Width(Value: Integer); safecall;
    function Get_FontHeight: Integer; safecall;
    function Get_FontNumber: Integer; safecall;
    function Get_FontStyle: Integer; safecall;
    procedure Set_FontHeight(Value: Integer); safecall;
    procedure Set_FontStyle(Value: Integer); safecall;
    function Get_FontName: WideString; safecall;
    procedure Set_FontName(const Value: WideString); safecall;
    function Get_OtherStyle: WordBool; safecall;
    function Get_ObjectStyle: IObjectStyle; safecall;
    procedure Set_ObjectStyle(const Value: IObjectStyle); safecall;
    function Get_Index: Integer; safecall;
    procedure Set_Index(Value: Integer); safecall;
    function Get_PXFP1_X: integer; safecall;
    function Get_PXFP1_Y: integer; safecall;
    function Get_PXFP2_X: integer; safecall;
    function Get_PXFP2_Y: integer; safecall;
    procedure PositionateOnLine(X1: Integer; Y1: Integer; X2: Integer; Y2: Integer); safecall;
    procedure PositionateOnCenter(X1: Integer; Y1: Integer); safecall;
    procedure CreateObjectStyle; safecall;
    function Get_ClipRect: IRect; safecall;
    procedure AttachToObject(LinkId: Integer); safecall;
    function Get_LinkMaster: Integer; safecall;
    procedure Positionate(X, Y, h_align, v_align, Angle: Integer); safecall;
    function Get_Align: Byte; safecall;
    procedure Set_Align(Value: Byte); safecall;
    { Protected declarations }
  Public
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
    Constructor Create(AData,AItem:POInter;ARelease:Boolean);
{$ELSE} // MAR - commented IDB due to memory problems:
    Constructor Create(AData,AItem:POInter;ARelease:Boolean; iOwnerLayerIndex: Integer = -1);
{$ENDIF}
{-- IDB_AXInterface}
    Destructor  Destroy; override;
  end;

implementation

uses ComServ,am_Text,axdef,am_def,AXPoint,am_Proj,am_font,axObjectStyle,
     am_cpoly, am_main, am_child, AxRect, am_index, am_view, am_layer;

{$IFNDEF AXIDB}
Constructor TText.Create;
{$ELSE} // MAR - commented IDB due to memory problems:
Constructor TText.Create(AData,AItem:POInter;ARelease:Boolean; iOwnerLayerIndex: Integer = -1);
{$ENDIF}
  begin
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
    inherited Create;
{$ELSE} // MAR - commented IDB due to memory problems:
    Inherited Create(AData, iOwnerLayerIndex, PIndex(AItem).Index);
{$ENDIF}
{-- IDB_AXInterface}
    FText:=AItem;
    BaseItem:=AItem;
    Data:=AData;
    Release:=ARelease;
    hIndex:=-1;
  end;

Destructor TText.Destroy;
  begin
    if Release Then Dispose(PText(FText),Done);
    inherited Destroy;
  end;

function TText.Get_ObjectType: Integer;
begin
  Result:=-1;
  if FText<>NIL then begin
    REsult:=PText(FTExt)^.GetObjtype;
  end;
end;

function TText.Get_Angle: Integer;
begin
  Result:=-1;
  if FText<>NIL then begin
    Result:=PText(FText)^.Angle;
  end;
end;

function TText.Get_Position: IPoint;
begin
  Result:=NIL;
  if FText<>NIL then begin
    Result:=AXPoint.TPoint.Create(@PText(FText)^.Pos,false);
  end;
end;

// function returns X coordinate of left buttom corner of text
function TText.Get_PXFP1_X: integer;
begin
  Result:=-1;
  if FText<>NIL then
  begin
    result:=PDPoint(PText(FText)^.GetBorder^.Data^.At(3))^.X;
  end;
end;

// function returns Y coordinate of left buttom corner of text
function TText.Get_PXFP1_Y: integer;
var aBoarder:PCPoly;
begin
  Result:=-1;
  if FText<>NIL then
  begin
    result:=PDPoint(PText(FText)^.GetBorder^.Data^.At(3))^.Y;
  end;
end;

// function returns X coordinate of right buttom Corner of Text
function TText.Get_PXFP2_X: integer;
var aBoarder:PCPoly;
begin
  Result:=-1;
  if FText<>NIL then
  begin
    result:=PDPoint(PText(FText)^.GetBorder^.Data^.At(2))^.X;
  end;
end;

// function returns Y coordinate of right buttom Corner of Text
function TText.Get_PXFP2_Y: integer;
var aBoarder:PCPoly;
begin
  Result:=-1;
  if FText<>NIL then
  begin
    result:=PDPoint(PText(FText)^.GetBorder^.Data^.At(2))^.Y;
  end;
end;

// function is used to set text-object around specified center position
procedure TText.PositionateOnCenter(X1: Integer; Y1: Integer);
var XMove,YMove:double;
    AAngle:real;
    CurPosition: TDPoint;
    AText:PText;
begin
   CurPosition.X:=X1;
   CurPosition.Y:=Y1;

   PText(FText)^.Pos:=CurPosition;

   // calculate Y factor to move
   AAngle:=PText(FText)^.Angle*Pi/180;
   YMove:=Round(Cos(AAngle)*PText(FText)^.Font.Height);
   if AAngle = 0 then YMove:=YMove
   else YMove:=YMove*2;

   // calculate X factor to move
   PText(FText)^.CalculateSize(PProj(Data)^.Pinfo);
   if AAngle = 0 then XMove:=PText(FText)^.Width
   else XMove:=Round(Sin(AAngle)*PText(FText)^.Width);
   XMove:=(XMove/2)*-1;

   CurPosition.Move(XMove,YMove);
   PText(FText)^.Pos:=CurPosition;
   // due to position has changed -> calculate new cliprect
   PText(FText)^.CalculateClipRect(PProj(Data)^.PInfo);
end;

// function is used to set text-object on a specified line, see PXF-ASC text definition
procedure TText.PositionateOnLine(X1: Integer; Y1: Integer; X2: Integer; Y2: Integer);
var EndPos     : TDPoint;
    CurPosition: TDPoint;
    AAngle     : Real;
begin
   EndPos.X:=X2; EndPos.Y:=Y2;
   CurPosition.X:=X1; CurPosition.Y:=Y1;
   PText(FText)^.Pos:=CurPosition;

   // if startpos and endpos is different
   // the angle and the width has to be calculated
   if CurPosition.IsDiff(EndPos) then
   begin
      // if positions of line are different the new angle has to be calculated
      PText(FText)^.Angle:=360-Round(CurPosition.CalculateAngle(EndPos)*180/Pi);
      PText(FText)^.Width:=LimitToLong(CurPosition.Dist(EndPos));
   end
   else
   begin
      // keep width and angle of text
      if PText(FText)^.Font.Style and $80<>0 then PText(FText)^.Angle:=360-Round(PText(FText)^.Angle)+90
      else PText(FText)^.Angle:=360-Round(PText(FText)^.Angle);
      PText(FText)^.Width:=Round(PText(FText)^.Font.Height*PProj(Data)^.PInfo^.GetTextRatio(PText(FText)^.Font,PText(FText)^.Text^));
   end;
   while PText(FText)^.Angle>360 do PText(FText)^.Angle:=PText(FText)^.Angle-360;
   while PText(FText)^.Angle<-360 do PText(FText)^.Angle:=PText(FText)^.Angle+360;

   PText(FText)^.Pos:=CurPosition;

   AAngle:=PText(FText)^.Angle*Pi/180;
   // if points of line are not equal calculate new position
   if PText(FText)^.Font.Style and $80<>0 then PText(FText)^.Pos.Move(Round(Sin(AAngle) * PText(FText)^.Font.Height/2.0),Round(Cos(AAngle)*PText(FText)^.Font.Height/2.0))
   else PText(FText)^.Pos.Move(Round(Sin(AAngle)*PText(FText)^.Font.Height),Round(Cos(AAngle)*PText(FText)^.Font.Height));
   // due to position has changed -> calculate new cliprect
   PText(FText)^.CalculateClipRect(PProj(Data)^.PInfo);
end;

function TText.Get_Text: WideString;
begin
  Result:='';
  if FText<>NIL then
  begin
    if PText(FText)^.Text <> nil then
       Result:=PText(FText)^.Text^;
  end;
end;

function TText.Get_Width: Integer;
begin
  Result:=-1;
  if FText<>NIL then begin
    Result:=PText(FText)^.Width;
  end;
end;

procedure TText.Set_Angle(Value: Integer);
begin
  if FText<>NIL then begin
    PTExt(FText)^.Angle:=Value;
  end;
end;

procedure TText.Set_Position(const Value: IPoint);
begin
  if FText<>NIL then begin
    PTExt(FText)^.Pos.x:=Value.x;
    PTExt(FText)^.Pos.y:=Value.y;
  end;
end;

procedure TText.Set_Text(const Value: WideString);
begin
  if FText<>NIL then begin
    PText(FText)^.SetText(Value);
  end;
end;

procedure TText.Set_Width(Value: Integer);
begin
  if FText<>NIL then begin
    PTExt(FText)^.Width:=Value;
  end;
end;



function TText.Get_FontHeight: Integer;
begin
  Result:=-1;
  if FText<>NIL then begin
    Result:=PText(FText)^.Font.Height;
  end;
end;

function TText.Get_FontNumber: Integer;
begin
  Result:=-1;
  if FText<>NIL then begin
    Result:=PText(FText)^.Font.font;
  end;
end;

function TText.Get_FontStyle: Integer;
begin
  Result:=-1;
  if FText<>NIL then begin
    Result:=PText(FText)^.Font.Style;
  end;
end;

procedure TText.Set_FontHeight(Value: Integer);
begin
  if FText<>NIL then begin
    PTExt(FText)^.Font.Height:=Value;
  end;
end;

procedure TText.Set_FontStyle(Value: Integer);
begin
  if FText<>NIL then begin
    PTExt(FText)^.Font.Style:=INteger16(Value);
  end;

end;

function TText.Get_FontName: WideString;
var FontDes  : PFontDEs;
begin
  Result:='not defined';
  if FText<>NIl then begin
    FontDes:=PProj(Data)^.Pinfo^.Fonts^.GetFont(PText(FText)^.Font.Font);
    if FontDes = nil then
       result:='Courier New'
    else
       REsult:=FontDes^.Name^;
  end;
end;

procedure TText.Set_FontName(const Value: WideString);
begin
  if FText<>NIL then begin
    PTExt(FText)^.Font.Font:=PProj(Data)^.Pinfo^.Fonts^.IDFromName(Value);
    if PText(Ftext)^.Font.Font = 0 then
       PText(Ftext)^.Font.Font:=PProj(Data)^.Pinfo^.Fonts^.IDFromName('Courier New');
  end;
end;


function TText.Get_OtherStyle: WordBool;
begin
Result:=false;
if FText<>NIL then begin
  Result:=PTExt(FText)^.GetState(sf_otherStyle);
end;
end;

function TText.Get_ObjectStyle: IObjectStyle;
begin
  Result:=NIL;
  if FText<>NIL then begin
    if  PText(FText)^.ObjectStyle <> NIL then
       REsult:=TObjectStyle.Create(PText(FText)^.ObjectStyle,false);
  end;
end;

function TText.Get_ClipRect: IRect;
begin
   Result:=NIL;
   if FText <> NIL then
   begin
      REsult:=AXRect.TRect.Create(@PText(FText).ClipRect,false);
   end;
end;

procedure TText.Set_ObjectStyle(const Value: IObjectStyle);
begin
 if FText<>NIL then begin
    SetStyle(PText(FText),Value);
  end;
end;

function TText.Get_Index: Integer;
begin
   Result:=-1;
   if FText<>NIL then
   begin
      if HIndex<>-1 then
         Result:=Hindex
   else
      Result:=PText(FText)^.Index;
   end;
end;

procedure TText.Set_Index(Value: Integer);
begin
 if FText<>NIL then begin
   HIndex:=Value;
 end;
end;

procedure TText.CreateObjectStyle;
begin
   CreateStyle(PText(FText));
end;

procedure TText.AttachToObject(LinkId: Integer);
var numlayers: integer;
    layercnt : integer;
    AIndex   : PIndex;
    AView    : PView;
    ALayer   : PLayer;
begin
   if FText = nil then
      exit;
   AIndex:=nil;
   // first get the item
   numlayers:=PProj(Data)^.Layers^.LData^.Count-1;
   for layercnt:=1 to numlayers do
   begin
      ALayer:=PLayer(PProj(Data)^.Layers^.LData^.at(layercnt));
      if ALayer = nil then continue;
      // check if the item exists on the layer and return item
      AIndex:=ALayer^.HasIndexObject(LinkId);
      if (AIndex) <> nil then // object found
         break;
   end;
   if Aindex<>NIl then
   begin
      AView:=PView(PLayer(PProj(Data)^.PInfo^.Objects)^.IndexObject(PProj(Data)^.PInfo,AIndex));
      PText(FText)^.AttachToObject(PProj(Data)^.Pinfo, AView);
   end;
end;

function TText.Get_LinkMaster: Integer;
begin
   result:=-1; // no link object as default
   if FText <> nil then
   begin
      if PText(FText)^.AttData <> nil then
      begin
         result:=PText(FText)^.AttData.IdxMaster;
      end;
   end;
end;

procedure TText.Positionate(X, Y, h_align, v_align, Angle: Integer);
var CurPosition: TDPoint;
    TextHeight : integer;
    TextWidth  : integer;
    XMove, YMove:double;
    Poly       : PCPoly;
    MinX,MaxX,MinY,MaxY:integer;
    aPoint:PDPoint;
    AAngle     : Real;
    X1,X2,Y1,Y2: integer; // line coordinates
    DeltaY_twidth : integer;
    DeltaX_twidth : integer;
    DeltaX_theight:integer;
    DeltaY_theight:integer;
begin
   CurPosition.X:=X; CurPosition.Y:=Y; // first set the position as current
   PText(FText)^.Pos:=CurPosition;
   PText(FText)^.CalculateSize(PProj(Data)^.PInfo);

   // now get the Width and Height of the Text
   TextHeight:=round(PText(FText)^.Font.Height);
   TextWidth:=PText(FText)^.Width;

   // calculate the deltas for X and Y in dependence of the TextWidth
   AAngle:=Angle*Pi/180;
   DeltaY_twidth:=round(sin(AAngle) * TextWidth);
   DeltaX_twidth:=round(cos(AAngle) * TextWidth);

   // calculate the deltas for X and Y in dependence of the Textheight
   DeltaX_theight:=round(sin(AAngle) * TextHeight);
   DeltaY_theight:=round(cos(AAngle) * TextHeight);


   // now calculate the line on that the text should be displayed
   if v_align = 0 then  // positionate vertical dependend from bottom
   begin
      if h_align = 0 then     // positionate horizontal from left
      begin
         X1:=X;
         Y1:=Y;
         X2:=X1+DeltaX_twidth;
         Y2:=Y1+DeltaY_twidth;
      end
      else if h_align = 1 then // positionate horizontal from center
      begin
         X1:=X - round(DeltaX_twidth/2);
         Y1:=Y - round(DeltaY_twidth/2);
         X2:=X1+DeltaX_twidth;
         Y2:=Y1+DeltaY_twidth;
      end
      else if h_align = 2 then // positionate horizontal from right
      begin
         X1:=X - DeltaX_twidth;
         Y1:=Y - DeltaY_twidth;
         X2:=X1+DeltaX_twidth;
         Y2:=Y1+DeltaY_twidth;
      end;
   end
   else if v_align = 1 then // positionate vertical depended from center
   begin
      if h_align = 0 then // positionate horizontal from left
      begin
         X1:=X + round(DeltaX_theight/2);
         Y1:=Y - round(DeltaY_theight/2);
         X2:=X1 + DeltaX_twidth;
         Y2:=Y1 + DeltaY_twidth;
      end
      else if h_align = 1 then // positionate horizontal from center
      begin
         X1:=X - round(DeltaX_twidth/2) + round(DeltaX_theight/2);
         Y1:=Y - round(DeltaY_twidth/2) - round(DeltaY_theight/2);
         X2:=X1+DeltaX_twidth;
         Y2:=Y1+DeltaY_twidth;
      end
      else if h_align = 2 then // positionate horizontal from right
      begin
         X1:=X - DeltaX_twidth + round(DeltaX_theight/2);
         Y1:=Y - DeltaY_twidth - round(DeltaY_theight/2);
         X2:=X1+DeltaX_twidth;
         Y2:=Y1+DeltaY_twidth;
      end;
   end
   else if v_align = 2 then // postionate vertical dependend from top
   begin
      if h_align = 0 then // positionate horizontal from left
      begin
         X1:=X + DeltaX_theight;
         Y1:=Y - DeltaY_theight;
         X2:=X1 + DeltaX_twidth;
         Y2:=Y1 + DeltaY_twidth;
      end
      else if h_align = 1 then // positionate horizontal from center
      begin
         X1:=X - round(DeltaX_twidth/2) + DeltaX_theight;
         Y1:=Y - round(DeltaY_twidth/2) - DeltaY_theight;
         X2:=X1+DeltaX_twidth;
         Y2:=Y1+DeltaY_twidth;
      end
      else if h_align = 2 then // positionate horizontal from right
      begin
         X1:=X - DeltaX_twidth + DeltaX_theight;
         Y1:=Y - DeltaY_twidth - DeltaY_theight;
         X2:=X1+DeltaX_twidth;
         Y2:=Y1+DeltaY_twidth;
      end;
   end;
   // now positionate text on line
   PositionateOnLine(X1, Y1, X2, Y2);
end;

function TText.Get_Align: Byte;
begin
  Result:=0;
  if FText<>NIL then begin
    Result:=PText(FText)^.FAlign;
  end;
end;

procedure TText.Set_Align(Value: Byte);
begin
  if FText<>NIL then begin
    PTExt(FText)^.FAlign:=Value;
  end;
end;

initialization
  TAutoObjectFactory.Create(ComServer, TText, Class_Text,
    ciInternal, tmApartment);
end.
