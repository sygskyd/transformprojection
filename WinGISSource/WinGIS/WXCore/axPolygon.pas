unit AXPolygon;

interface

uses
  ComObj, ActiveX, WINGIS_TLB, StdVcl,AXBase, AM_Def;

type
  TPolygon = class(TBase, IPolygon)
  private
    FPolygon : Pointer;
    Data     : Pointer;
    Release  : Boolean;
    hIndex   : Integer;
    FCenter  : TDPoint;
  protected
    function Get_Count: Integer; safecall;
    function Get_IslandArea: WordBool; safecall;
    function Get_IslandCount: Integer; safecall;
    function Get_IslandInfo(Index: Integer): Integer; safecall;
    function Get_ObjectType: Integer; safecall;
    function Get_Points(Index: Integer): IPoint; safecall;
    procedure Assign(const Item: IPolygon); safecall;
    procedure ClosePoly; safecall;
    procedure InsertPoint(const APoint: IPoint); safecall;
    procedure Set_ObjectStyle(const Value: IObjectStyle); safecall;
    function Get_Area: Double; safecall;
    procedure InsertPointxy(x, y: Integer); safecall;
    function Get_Length: Double; safecall;
    function Get_OtherStyle: WordBool; safecall;
    function Get_ObjectStyle: IObjectStyle; safecall;
    function Get_Index: Integer; safecall;
    procedure Set_Index(Value: Integer); safecall;
    procedure Assign_(const Item: IPolygon); safecall;
    function Get_ClipRect: IRect; safecall;
    procedure CreateObjectStyle; safecall;
    procedure Redraw; safecall;
    function Get_Center: IPoint; safecall;
    procedure DeletePointByIndex(Index: Integer); safecall;
    function GetWKTString: WideString; safecall;
    { Protected declarations }
  public
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
    Constructor Create(AData,AItem:POInter;ARelease:Boolean);
{$ELSE} // MAR - commented IDB due to memory problems:
    Constructor Create(AData,AItem:POInter;ARelease:Boolean; iOwnerLayerIndex: Integer = -1);
{$ENDIF}
{-- IDB_AXInterface}
    Destructor  Destroy; override;
  end;

implementation

uses ComServ,am_Cpoly,AXPoint,am_proj,am_view,am_layer,am_index,axdef,
     axobjectstyle,AXREct;

{$IFNDEF AXIDB}
Constructor TPolygon.Create;
{$ELSE} // MAR - commented IDB due to memory problems:
Constructor TPolygon.Create(AData,AItem:POInter;ARelease:Boolean; iOwnerLayerIndex: Integer = -1);
{$ENDIF}
  begin
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
    inherited create;
{$ELSE} // MAR - commented IDB due to memory problems:
    Inherited Create (AData, iOwnerLayerIndex, PIndex(AItem).Index);
{$ENDIF}
{-- IDB_AXInterface}
    FPolygon:=AItem;
    BaseItem:=AItem;
    Data:=AData;
    Release:=ARelease;
    hIndex:=-1;
    FCenter.Init(0,0);
  end;

Destructor TPolygon.Destroy;
  begin
    if Release Then Dispose(PCPoly(FPolygon),Done);
    inherited Destroy;
  end;


function TPolygon.Get_Count: Integer;
begin
  Result:=-1;
  if FPolyGon<>NIL then begin
    REsult:=PCPoly(FPolyGon)^.Data^.Count;
  end;
end;

function TPolygon.Get_ObjectType: Integer;
begin
  Result:=-1;
  if FPolyGon<>NIL then begin
    REsult:=PCPoly(FPolyGon)^.GetObjtype;
  end;
end;

function TPolygon.Get_Points(Index: Integer): IPoint;
begin
  Result:=NIL;
  if FPolygon <> NIL then begin
    REsult:=TPoint.Create(PCPoly(FPolyGon)^.Data^.at(Index),false);
  end;
end;

procedure TPolygon.InsertPoint(const APoint: IPoint);
var BPoint : TDPOint;
begin
  if FPolygon<>NIL then begin
    BPoint.Init(APoint.x,APoint.y);
    PCPoly(FPolygon)^.InsertPoint(BPOint);
  end;
end;

procedure TPolygon.ClosePoly;
begin
  if FPolygon<>NIL then begin
    PCPoly(FPolygon)^.ClosePoly;
  end;
end;

procedure TPolygon.Assign(const Item: IPolygon);
begin

end;

function TPolygon.Get_IslandArea: WordBool;
begin
  if FPolygon<>NIL then begin
    Result:=PCPoly(FPolyGon)^.GetState(sf_IslandArea);
  end;
end;

function TPolygon.Get_IslandCount: Integer;
begin
  if FPolygon<>NIL then begin
    Result:=PCPoly(FPolyGon)^.IsLandCount;
  end;
end;

function TPolygon.Get_IslandInfo(Index: Integer): Integer;
begin
  if FPolygon<>NIL then begin
    Result:=PCPoly(FPolyGon)^.IslandInfo^[Index];
  end;
end;


function TPolygon.Get_Area: Double;
begin
  if FPolygon<>NIL then begin
    Result:=PCPoly(FPolygon)^.Flaeche;
  end;
end;

procedure TPolygon.InsertPointxy(x, y: Integer);
var BPoint : TDPOint;
begin
  if FPolygon<>NIL then begin
    BPoint.Init(x,y);
    PCPoly(FPolygon)^.InsertPoint(BPOint);
  end;
end;

function TPolygon.Get_Length: Double;
begin
  if FPolygon<>NIL then begin
    Result:=PCPoly(FPolygon)^.Laenge;
  end;
end;

function TPolygon.Get_OtherStyle: WordBool;
begin
  REsult:=False;
  if FPolygon<>NIL then begin
    Result:=PCPoly(FPolygon)^.GetState(sf_otherStyle);
  end;
end;

function TPolygon.Get_ObjectStyle: IObjectStyle;
begin
  Result:=NIL;
  if FPolygon<>NIL then begin
    if  PCPoly(FPolygon)^.ObjectStyle <> NIL then 
       REsult:=TObjectStyle.Create(PCPoly(FPolygon)^.ObjectStyle,false);
  end;
end;

procedure TPolygon.Set_ObjectStyle(const Value: IObjectStyle);
begin
  if FPolygon<>NIL then begin
    SetStyle(PCPoly(FPolygon),Value);
  end;
end;

function TPolygon.Get_Index: Integer;
begin
 Result:=-1;
  if FPolygon<>NIL then begin
    if HINdex<>-1 then REsult:=Hindex else
    Result:=PCPoly(FPolygon)^.Index;
  end;
end;

procedure TPolygon.Set_Index(Value: Integer);
begin
 if FPolygon<>NIL then begin
   //PCPoly(FPolygon)^.Index:=Value;
   HIndex:=Value;
 end;
end;

function TPolygon.Get_ClipRect: IRect;
begin
  REsult:=AXRect.TRect.Create(@PCPoly(FPolygon).ClipRect,false);
end;

procedure TPolygon.Assign_(const Item: IPolygon);
begin

end;

procedure TPolygon.CreateObjectStyle;
begin
   CreateStyle(PCPoly(FPolygon));
end;

procedure TPolygon.Redraw;
begin
   PCPoly(FPolygon)^.Invalidate(PProj(Data)^.PInfo);
   PCPoly(FPolygon)^.CalculateClipRect; // (PProj(Data)^.PInfo);
   PCPoly(FPolygon)^.Invalidate(PProj(Data)^.PInfo);
   PProj(Data)^.UpdateClipRect(PCPoly(FPolygon));
   PProj(Data)^.CorrectSize(PCPoly(FPolygon)^.ClipRect,TRUE);
   PProj(Data)^.PInfo^.RedrawInvalidate;
end;

function TPolygon.Get_Center: IPoint;
begin
     if FPolygon <> nil then begin
        FCenter:=PCPoly(FPolygon)^.GetCenter;
        Result:=TPoint.Create(@FCenter,False);
     end;
end;

procedure TPolygon.DeletePointByIndex(Index: Integer);
begin
  if FPolygon<>NIL then begin
    PCPoly(FPolygon)^.DeletePointByIndex(Index);
  end;
end;

function TPolygon.GetWKTString: WideString;
begin
  if FPolygon<>NIL then begin
     Result:=PCPoly(FPolygon)^.ToWKT;
  end;
end;

initialization
  TAutoObjectFactory.Create(ComServer, TPolygon, Class_Polygon,
    ciInternal, tmApartment);
end.
