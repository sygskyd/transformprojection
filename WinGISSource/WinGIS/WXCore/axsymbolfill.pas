unit AXSymbolFill;

interface

uses
  ComObj, ActiveX, WINGIS_TLB,am_Def, StdVcl;

type
  TSymbolFill = class(TAutoObject, ISymbolFill)
  private
    FSymbolFill   :  am_def.TSymbolFill;
  protected
    function Get_Angle: Double; safecall;
    function Get_Distance: Double; safecall;
    function Get_DistanceType: Integer; safecall;
    function Get_FillStyle: IFillStyle; safecall;
    function Get_FillType: Integer; safecall;
    function Get_LineStyle: ILineStyle; safecall;
    function Get_Offset: Double; safecall;
    function Get_OffsetType: Integer; safecall;
    function Get_Size: Double; safecall;
    function Get_SizeType: Integer; safecall;
    function Get_Symbol: Integer; safecall;
    function Get_SymbolAngle: Double; safecall;
    procedure Set_Angle(Value: Double); safecall;
    procedure Set_Distance(Value: Double); safecall;
    procedure Set_DistanceType(Value: Integer); safecall;
    procedure Set_FillStyle(const Value: IFillStyle); safecall;
    procedure Set_FillType(Value: Integer); safecall;
    procedure Set_LineStyle(const Value: ILineStyle); safecall;
    procedure Set_Offset(Value: Double); safecall;
    procedure Set_OffsetType(Value: Integer); safecall;
    procedure Set_Size(Value: Double); safecall;
    procedure Set_SizeType(Value: Integer); safecall;
    procedure Set_Symbol(Value: Integer); safecall;
    procedure Set_SymbolAngle(Value: Double); safecall;
    { Protected declarations }
  end;

implementation

uses ComServ,AXfillStyle,AXDef,AXLineStyle;

function TSymbolFill.Get_Angle: Double;
begin
  Result:=FSymbolFill.Angle;
end;

function TSymbolFill.Get_Distance: Double;
begin
  Result:=FSymbolFill.Distance;
end;

function TSymbolFill.Get_DistanceType: Integer;
begin
  Result:=FSymbolFill.DistanceType;
end;

function TSymbolFill.Get_FillStyle: IFillStyle;
begin
  Result:=AXfillStyle.TFillStyle.Create(@FSymbolFill.FillStyle,false);
  //SetIFillStyle(FSymbolFill.FillStyle,REsult);
end;

function TSymbolFill.Get_FillType: Integer;
begin
  REsult:=FSymbolFill.FillType;
end;

function TSymbolFill.Get_LineStyle: ILineStyle;
begin
  Result:=AXLineStyle.TLineStyle.Create(@FSymbolFill.LineStyle,false);
  //SetILineStyle(FSymbolFill.LineStyle,Result);
end;

function TSymbolFill.Get_Offset: Double;
begin
  REsult:=FSymbolFill.Offset;
end;

function TSymbolFill.Get_OffsetType: Integer;
begin
  Result:=FSymbolFill.OffsetType;
end;

function TSymbolFill.Get_Size: Double;
begin
  Result:=FSymbolFill.Size;
end;

function TSymbolFill.Get_SizeType: Integer;
begin
  Result:=FSymbolFill.SizeType;
end;

function TSymbolFill.Get_Symbol: Integer;
begin
  Result:=FSymbolFill.Symbol;
end;

function TSymbolFill.Get_SymbolAngle: Double;
begin
  Result:=FSymbolFill.SymbolAngle;
end;

procedure TSymbolFill.Set_Angle(Value: Double);
begin
  FSymbolFill.Angle:=Value;
end;

procedure TSymbolFill.Set_Distance(Value: Double);
begin
  FSymbolFill.Distance:=Value;
end;

procedure TSymbolFill.Set_DistanceType(Value: Integer);
begin
  FSymbolFill.DistanceType:=Value;
end;

procedure TSymbolFill.Set_FillStyle(const Value: IFillStyle);
begin
  SetFillStyle(FSymbolFill.FillStyle,Value);
end;

procedure TSymbolFill.Set_FillType(Value: Integer);
begin
  FSymbolFill.FillType:=Value;
end;

procedure TSymbolFill.Set_LineStyle(const Value: ILineStyle);
begin
  SetLineStyle(FSymbolFill.LineStyle,Value);
end;

procedure TSymbolFill.Set_Offset(Value: Double);
begin
  FSymbolFill.Offset:=Value;
end;

procedure TSymbolFill.Set_OffsetType(Value: Integer);
begin
  FSymbolFill.OffsetType:=Value;
end;

procedure TSymbolFill.Set_Size(Value: Double);
begin
  FSymbolFill.Size:=Value;
end;

procedure TSymbolFill.Set_SizeType(Value: Integer);
begin
  FSymbolFill.SizeType:=Value;
end;

procedure TSymbolFill.Set_Symbol(Value: Integer);
begin
  FSymbolFill.Symbol:=Value;
end;

procedure TSymbolFill.Set_SymbolAngle(Value: Double);
begin
  FSymbolFill.SymbolAngle:=Value;
end;


initialization
  TAutoObjectFactory.Create(ComServer, TSymbolFill, Class_SymbolFill,
    ciInternal, tmApartment);
end.
