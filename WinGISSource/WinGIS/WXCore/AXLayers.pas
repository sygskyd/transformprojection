unit AXLayers;

interface

uses
  ComObj, ActiveX, Wingis_TLB;

type
  TLayers = class(TAutoObject, ILayers)
  private
    FLayers   : Pointer;
    Data      : POinter;      //PProj
  protected
    function Get_ActiveLayer: ILayer; safecall;
    function Get_Count: Integer; safecall;
    function Get_Items(Index: Integer): ILayer; safecall;
    function Get_SelectLayer: ILayer; safecall;
    function GetLayerByName(const LayerName: WideString): ILayer; safecall;
    function InsertLayer(const RefLayer: ILayer; OnTop: WordBool): ILayer;
      safecall;
    function InsertLayerByName(const Name: WideString;
      OnTop: WordBool): ILayer; safecall;
    procedure SetActiveLayer(const Name: WideString);safecall;
    { Protected declarations }
  public
    constructor Create(Item,Owner:POinter);
  end;

implementation

uses ComServ,am_admin,AXLayer,am_Layer,am_def,sysutils,axdef;


constructor TLayers.Create(Item,Owner:POinter);
  begin
    inherited Create;
    Data:=Owner;
    FLayers:=Item;
  end;

function TLayers.Get_ActiveLayer: ILayer;
begin
  Result:=NIL;
  if FLayers<>NIL then begin
    Result:=AXLayer.TLayer.create(PLayers(FLayers)^.TopLayer,Data);
  end;
end;

function TLayers.Get_Count: Integer;
begin
  Result:=-1;
  if FLayers<>NIL then begin
    Result:=PLayers(FLayers)^.LData^.Count-1;
  end;
end;

function TLayers.Get_Items(Index: Integer): ILayer;
begin
  Result:=NIL;
  Inc(Index);
  if FLayers<>NIL then begin
    if PLayers(FLayers)^.Ldata^.at(Index) <> NIL then
    Result:=AXLayer.TLayer.create(PLayers(FLayers)^.Ldata^.at(Index),Data);
  end;
end;

function TLayers.Get_SelectLayer: ILayer;
begin
  Result:=NIL;
  if FLayers<>NIL then begin
    Result:=AXLayer.TLayer.create(PLayers(FLayers)^.SelLayer,Data);
  end;
end;

function TLayers.GetLayerByName(const LayerName: WideString): ILayer;
var ALayer  : PLayer;
begin
  Result:=NIL;
  if FLayers<>NIL then begin
    ALayer:=PLayers(FLayers)^.NameToLayer(LayerName);
    if ALayer<>NIL then Result:=AXLayer.TLayer.create(ALayer,Data)
  end;
end;

function TLayers.InsertLayer(const RefLayer: ILayer;
  OnTop: WordBool): ILayer;
var AFillStyle    : IFillStyle;
    ALineStyle    : ILineStyle;
    ASymbolfill   : ISymbolFill;
    InsertPos     : Integer;
    LayerName     : string;
    RLayer        : PLayer;
    cnt           : integer;
    LName         : string;
begin
  Result:=NIL;
  if Ontop then InsertPos:=0 else InsertPos:=2; //Head Tail
  if FLayers<>NIL then begin
    LayerName:=RefLayer.LayerName;
    RLayer:=PLayers(FLayers)^.NameToLayer(LayerName);
    AFillStyle:=RefLayer.FillStyle;
    ALineStyle:=RefLayer.LineStyle;
    AsymbolFill:=RefLayer.SymbolFill;

    (*
    cnt:=0;
    While RLayer<>NIL do begin
      if cnt=0 then
      RLayer:=PLayers(FLayers)^.NameToLayer(LayerName) else
      RLayer:=PLayers(FLayers)^.NameToLayer(LayerName+inttostr(cnt));
      inc(cnt);
    end;
    if cnt=0 then
    RLayer:=PLayers(FLayers)^.InsertLayer(LayerName,0,0,0,0,InsertPos,DefaultSymbolFill) else
    RLayer:=PLayers(FLayers)^.InsertLayer(LayerName+inttostr(cnt),0,0,0,0,InsertPos,DefaultSymbolFill);
     *)
    cnt:=1;
    LName:=LayerName;
    While RLayer<>NIL do begin
      RLayer:=PLayers(FLayers)^.NameToLayer(LName);
      if RLayer<>NIL then begin
        LName:=LayerName+' '+inttostr(cnt);
        inc(cnt);
      end;
    end;
    if cnt=1 then
    RLayer:=PLayers(FLayers)^.InsertLayer(LayerName,0,0,0,0,InsertPos,DefaultSymbolFill) else
    RLayer:=PLayers(FLayers)^.InsertLayer(LName,0,0,0,0,InsertPos,DefaultSymbolFill);
    Result:=AXLayer.TLayer.create(RLayer,Data);

    RLayer^.GeneralMin:=RefLayer.GeneralLower;
    RLayer^.GeneralMax:=RefLayer.GeneralUpper;
    RLayer^.State:=RefLayer.State;

    SetFillStyle(RLayer^.FillStyle,RefLayer.FillStyle);
    SetLineStyle(RLayer^.LineStyle,RefLayer.LineStyle);
    SetSymbolFill(RLayer^.SymbolFill,RefLayer.SymbolFill);

    Result:=AXLayer.TLayer.create(RLayer,DAta);
  end;
end;

function TLayers.InsertLayerByName(const Name: WideString;
  OnTop: WordBool): ILayer;
var InsertPos  : integer;
    RLayer     : PLayer;
    cnt        : integer;
    Layername  : string;
begin
  REsult:=NIL;
  if Ontop then InsertPos:=0 else InsertPos:=2; //Head Tail

  if FLayers<>NIL then begin
    cnt:=1;
    LayerName:=Name;
    While RLayer<>NIL do begin
      RLayer:=PLayers(FLayers)^.NameToLayer(LayerName);
      if RLayer<>NIL then begin
        LayerName:=Name+' '+inttostr(cnt);
        inc(cnt);
      end;
    end;
    if cnt=1 then
    RLayer:=PLayers(FLayers)^.InsertLayer(Name,0,0,0,0,InsertPos,DefaultSymbolFill) else
    RLayer:=PLayers(FLayers)^.InsertLayer(LayerName,0,0,0,0,InsertPos,DefaultSymbolFill);
    Result:=AXLayer.TLayer.create(RLayer,Data);
  end;

end;

procedure TLayers.SetActiveLayer(const Name: WideString);
var ALayer : PLayer;
begin
  if FLayers<>NIL then begin
    ALayer:=PLayers(FLayers)^.NameToLayer(Name);
    if ALayer<>NIL then PLayers(FLayers)^.SetTopLayer(ALayer);
  end;
end;

initialization
  TAutoObjectFactory.Create(ComServer, TLayers, Class_Layers,
    ciInternal, tmApartment);
end.
