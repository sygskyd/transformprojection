unit WXCore;

interface

uses
  ComObj, ActiveX, Wingis_TLB, AxCtrls, Classes, StdVcl, Windows, Messages, Forms;

type
  TCore = class(TAutoObject, IConnectionPointContainer, ICore)
  private
    CoreHandle: integer;
    FConnectionPoints: TConnectionPoints;
    FConnectionPoint: TConnectionPoint;
    FEvents: ICoreEvents;
  protected
    function OpenDocument(const FileName: WideString): IDocument; safecall;
    function OpenDocumentReadOnly(const FileName: WideString): IDocument; safecall;
    function CreateDocument(const FileName: WideString): IDocument; safecall;
    function GetActiveDocument: IDocument; safecall;
    procedure ExecDDE(const DDEString: WideString); safecall;
    procedure ExecMenu(const ItemName: WideString); safecall;
    procedure Terminate; safecall;
    procedure CloseDocument(const FileName: WideString); safecall;
    function GetMenuStatus(const ItemName: WideString): Word; safecall;
    procedure BingMapsLoadAsync(Async: WordBool); safecall;
    property ConnectionPoints: TConnectionPoints read FConnectionPoints implements IConnectionPointContainer;
    procedure EventSinkChanged(const EventSink: IUnknown); override;
  public
    StringList: TStringList;
    procedure Initialize; override;
    destructor Destroy; override;
    function AddDocument(const FileName: WideString): IDocument; safecall;
    procedure FireDDEEvent(s: AnsiString);
    procedure FireMONEvent;
    procedure Connect; safecall;
    function GetSinks: TInterfaceList;
  end;

var
  ComConnected: Boolean = False;
  DoRegister: Boolean = False;
  DoUnRegister: Boolean = False;
  RegUpdated: Boolean = False;

implementation

uses ComServ, am_Main, am_Child, sysutils, AXDocument, MenuFn, AM_Def, AM_DDE, VEModule, ErrHndl;

procedure TCore.Initialize;
begin
  try
    inherited Initialize;
    StringList := TStringList.Create;
    RegisterActiveObject(Self as IUnknown, CLASS_CORE, ACTIVEOBJECT_STRONG, CoreHandle);
    Factory.RegisterClassObject;
    FConnectionPoints := TConnectionPoints.Create(Self);
    if AutoFactory.EventTypeInfo <> nil then
    begin
      FConnectionPoint := FConnectionPoints.CreateConnectionPoint(AutoFactory.EventIID, ckMulti, EventConnect)
    end
    else
    begin
      FConnectionPoint := nil;
    end;
    if DoRegister then
    begin
      RegUpdated := True;
      try
        Factory.UpdateRegistry(True);
        ComServer.UpdateRegistry(True);
      except
        RegUpdated := False;
      end;
    end;
    if DoUnRegister then
    begin
      RegUpdated := True;
      try
        Factory.UpdateRegistry(False);
        ComServer.UpdateRegistry(False);
      except
        RegUpdated := False;
      end;
    end;
  except
    on E: Exception do
    begin
      DebugMsg('TCore.Initialize', E.Message);
    end;
  end;
end;

destructor TCore.Destroy;
begin
  try
    StringList.Free;
    RevokeActiveObject(CoreHandle, nil);
    inherited Destroy;
  except
    on E: Exception do
    begin
      DebugMsg('TCore.Destroy', E.Message);
    end;
  end;
end;

function TCore.OpenDocument(const FileName: WideString): IDocument;
var
  hName: array[0..1023] of char;
  hChild: TMDIChild;
begin
  Result := nil;
  try
    strpCopy(hName, FileName);
{$IFNDEF AXDLL} // <----------------- AXDLL
    if WingisMainForm.FileNameOpen(hName, 0, False) then
    begin
      WingisMainForm.NewTDocument(StrPas(hName));
      hChild := WingisMainForm.GetCurrentChild;
      Result := TDocument.Create(hChild);
      Application.ProcessMessages;
    end;
{$ENDIF}
  except
    on E: Exception do
    begin
      DebugMsg('TCore.OpenDocument', E.Message);
    end;
  end;
end;

function TCore.OpenDocumentReadOnly(const FileName: WideString): IDocument;
var
  hName: array[0..1023] of char;
  hChild: TMDIChild;
begin
  Result := nil;
{$IFNDEF AXDLL} // <----------------- AXDLL
  try
    strpCopy(hName, FileName);
    WingisMainForm.NextProjIsReadOnly := True;
    if WingisMainForm.FileNameOpen(hName, 0, False) then
    begin
      WingisMainForm.NewTDocument(StrPas(hName));
      hChild := WingisMainForm.GetCurrentChild;
      Result := TDocument.Create(hChild);
      Application.ProcessMessages;
    end;
  except
    on E: Exception do
    begin
      DebugMsg('TCore.OpenDocumentReadOnly', E.Message);
    end;
  end;
{$ENDIF}
end;

function TCore.AddDocument(const FileName: WideString): IDocument;
var
  hName: array[0..1023] of char;
  hChild: TMDIChild;
begin
  Result := nil;
  try
    strpCopy(hName, FileName);
    hChild := WingisMainForm.IsFileOpened(hName);
    if (hChild <> nil) and (hChild.Data <> nil) then
    begin
      Result := TDocument.Create(hChild);
    end;
  except
    on E: Exception do
    begin
      DebugMsg('TCore.AddDocument', E.Message);
    end;
  end;
end;

function TCore.CreateDocument(const FileName: WideString): IDocument;
var
  hName: array[0..1023] of char;
  hChild: TMDIChild;
begin
  Result := nil;
{$IFNDEF AXDLL} // <----------------- AXDLL
  try
    strpCopy(hName, FileName);
    WingisMainForm.FileNameNew(AnsiString(hName));
    hChild := WingisMainForm.GetCurrentChild;
    Result := TDocument.Create(hChild);
  except
    on E: Exception do
    begin
      DebugMsg('TCore.CreateDocument', E.Message);
    end;
  end;
{$ENDIF}
end;

function TCore.GetActiveDocument: IDocument;
var
  Disp: IDispatch;
begin
  Result := nil;
{$IFNDEF AXDLL} // <----------------- AXDLL
  try
    if WingisMainForm.CountChildren > 0 then
    begin
      Disp := WingisMainForm.GetCurrentTDocument;
    end;
    if Assigned(Disp) then
    begin
      Result := Disp as IDocument;
    end;
  except
    on E: Exception do
    begin
      DebugMsg('TCore.GetActiveDocument', E.Message);
    end;
  end;
{$ENDIF}
end;

procedure TCore.ExecDDE(const DDEString: WideString);
var
  s: AnsiString;
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  try
    s := AnsiString(DDEString);
    s := StringReplace(s, ',', DDEHandler.FDDESepSign, [rfReplaceAll]);
    WingisMainForm.DoDDECommands(PChar(s));
  except
    on E: Exception do
    begin
      DebugMsg('TCore.ExecDDE', E.Message);
    end;
  end;
{$ENDIF}
end;

procedure TCore.EventSinkChanged(const EventSink: IUnknown);
begin
  try
    Connect;
    FEvents := EventSink as ICoreEvents;
  except
    on E: Exception do
    begin
      DebugMsg('TCore.EventSinkChanged', E.Message);
    end;
  end;
end;

procedure TCore.FireDDEEvent(s: AnsiString);
var
  SinkList: tInterFaceList;
  i: integer;
begin
  try
    SinkList := GetSinks;
    try
      for i := 0 to SinkList.Count - 1 do
      begin
        (SinkList.Items[i] as ICoreEvents).OnDDEString(s);
      end;
    finally
      SinkList.Free;
    end;
  except
    on E: Exception do
    begin
      DebugMsg('TCore.FireDDEEvent', E.Message);
    end;
  end;
end;

procedure TCore.FireMONEvent;
var
  OleStrings: IStrings;
  SinkList: tInterFaceList;
  i: Integer;
begin
  try
    GetOleStrings(StringList, OleStrings);
    SinkList := GetSinks;
    try
      for i := 0 to SinkList.Count - 1 do
      begin
        (SinkList.Items[i] as ICoreEvents).OnMonitoring(OleStrings);
      end;
    finally
      SinkList.Free;
    end;
  except
    on E: Exception do
    begin
      DebugMsg('TCore.FireMONEvent', E.Message);
    end;
  end;
end;

procedure TCore.ExecMenu(const ItemName: WideString);
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  try
    WingisMainForm.ExecuteMenuFunction(AnsiString(ItemName));
  except
    on E: Exception do
    begin
      DebugMsg('TCore.ExecMenu', E.Message);
    end;
  end;
{$ENDIF}
end;

procedure TCore.Terminate;
begin
  DoTerminate := True;
end;

procedure TCore.CloseDocument(const FileName: WideString);
var
  hName: array[0..255] of char;
begin
  try
    strpCopy(hName, FileName);
    WingisMainForm.FileNameClose(hName);
  except
    on E: Exception do
    begin
      DebugMsg('TCore.CloseDocument', E.Message);
    end;
  end;
end;

function TCore.GetMenuStatus(const ItemName: WideString): Word;
var
  AChecked, AEnabled: Word;
begin
  Result := 0;
  try
    AChecked := Ord(MenuFunctions[AnsiString(ItemName)].Checked);
    AEnabled := Ord(MenuFunctions[AnsiString(ItemName)].Enabled) shl 1;
    Result := AChecked or AEnabled;
  except
    on E: Exception do
    begin
      DebugMsg('TCore.GetMenuStatus', E.Message);
    end;
  end;
end;

procedure TCore.Connect;
begin
  ComConnected := True;
end;

function TCore.GetSinks: TInterfaceList;
var
  connections: IenumConnections;
  conPoint: IconnectionPoint;
  ConnectData: tConnectData;
  NoFetched: cardinal;
begin
  Result := nil;
  try
    Result := tInterfaceList.Create;
    (Self as IConnectionPointContainer).FindConnectionPoint(DIID_ICoreEvents, conPoint);
    conPoint.EnumConnections(connections);
    if connections <> nil then
    begin
      while connections.Next(1, ConnectData, @NoFetched) = S_OK do
      begin
        if ConnectData.pUnk <> nil then
        begin
          Result.Add(ConnectData.pUnk)
        end;
      end;
    end;
  except
    on E: Exception do
    begin
      DebugMsg('TCore.GetSinks', E.Message);
    end;
  end;
end;

procedure TCore.BingMapsLoadAsync(Async: WordBool);
begin
  try
    if VEForm <> nil then
    begin
      VEForm.LoadAsync := Async;
    end;
  except
    on E: Exception do
    begin
      DebugMsg('TCore.BingMapsLoadAsync', E.Message);
    end;
  end;
end;

initialization
  TAutoObjectFactory.Create(ComServer, TCore, Class_Core,
    ciSingleInstance, tmSingle);
end.

