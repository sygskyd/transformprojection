unit AXCustomers;

interface

uses
  ComObj, ActiveX, WINGIS_TLB;

const   ot_Symbol    = 8;
        ot_Poly      = 2;


type
  TCustomers = class(TAutoObject, ICustomers)
  private
    FDocument : IDocument;
  protected

    function GetCustomers(const CustomerList, EdgeList: IList;
      Radius: Integer): IList; safecall;
    { Protected declarations }
  public
    constructor Create(ADocument:Idispatch);
  end;

implementation

uses ComServ;

constructor TCustomers.Create;
begin
  inherited Create;
  FDocument:=ADocument as IDocument;
end;


function TCustomers.GetCustomers(const CustomerList,
  EdgeList: IList; Radius: Integer): IList;
var AName   : widestring;
    AItem   : Variant;
    i       : integer;
    j       : integer;
    k       : integer;
    m       : integer;
    Layers  : ILayers;
    ADisp   : IDispatch;
    Layer   : ILayer;
    AObject : variant;
    ASymbol : ISymbol;
    APoint  : IPoint;

    BDisp   : IDispatch;
    BLayer   : ILayer;
    BObject : Idispatch;
    BPoint  : IPoint;
    BPoly   : IPolygon;
    BItem   : Variant;
    RObject : Variant;

    found   : boolean;
    found2  : boolean;
    CustomerObject : ICustomerItem;
    hint    : integer;
    SelREs  : ISelectREsult;
    DistanceToSnap : integer;
    FullDistance   : integer;
begin
  found:=false;
{  Result:=FDocument.CreateList;
  for i:=0 to CustomerList.count-1 do begin
    AItem:=CustomerList.Items[i];
    hInt:=vartype(aItem);
    if (vartype(AItem)=varolestr)or(vartype(AItem)=varstring) then begin
      AName:=AItem;
      Layers:=FDocument.Layers;
      ADisp:=Layers.GetLayerByName(AName);
      if ADisp<>NIL then begin
        Layer:=ADisp as ILayer;
        for k:=0 to Layer.Count-1 do begin
          ADisp:=Layer.Items[k];
          if ADisp<>NIL then begin
          AObject:=ADisp;
          if AObject.ObjectType = ot_symbol then begin
            Asymbol:=ADisp as ISymbol;
            APoint:=ASymbol.Position;

            found2:=false;
            for j:=0 to EdgeList.Count-1 do begin
              BItem:=EdgeList.Items[j];
              if (vartype(AItem)=varolestr)or(vartype(AItem)=varstring) then begin
                AName:=BItem;
                BDisp:=Layers.GetLayerByName(AName);
                if BDisp<>NIL then begin
                  BLayer:=BDisp as ILayer;
                  SelRes:=BLayer.ClipByPoint(APoint,Radius);
                  if SelRes<>NIL then begin
                    RObject:=SelRes.Poly;
                    if RObject.ObjectType = ot_Poly then begin
                      found2:=true;
                      CustomerObject:=FDocument.CreateCustomerItem;
                      CustomerObject.CustomerID:=ASymbol.Index;
                      CustomerObject.EdgeID:=RObject.Index;
                      FullDistance:=RObject.Length*100;
                      DistanceToSnap:=SelRes.DistanceToSnapPoint;
                      CustomerObject.Percent:=round(100*(DistancetoSnap/FullDistance));
                      Result.Add(CustomerObject);
                    end;
                  end;
                end;
                if found2 then begin
                  break;
                end;
              end;
            end; //Edgelist.count

          end;
          end; //ADisp<>NIL
        end;
      end;  //if ADisp<>NIL
    end;
  end;}
end;



initialization
  TAutoObjectFactory.Create(ComServer, TCustomers, Class_Customers,
    ciInternal, tmApartment);
end.

(*
function TCustomers.GetCustomers(const CustomerList,
  EdgeList: IList): IList;
var AName   : widestring;
    AItem   : Variant;
    i       : integer;
    j       : integer;
    k       : integer;
    m       : integer;
    Layers  : ILayers;
    ADisp   : IDispatch;
    Layer   : ILayer;
    AObject : variant;
    ASymbol : ISymbol;
    APoint  : IPoint;

    BDisp   : IDispatch;
    BLayer   : ILayer;
    BObject : variant;
    BPoint  : IPoint;
    BPoly   : IPolygon;
    BItem   : Variant;

    found   : boolean;
    found2  : boolean;
begin
  found:=false;
  for i:=0 to CustomerList.count-1 do begin
    AItem:=CustomerList.Items[i];
    if vartype(AItem) = varolestr or varstring then begin
      AName:=AItem;
      Layers:=FDocument.Layers;
      ADisp:=Layers.GetLayerByName(AName);
      if ADisp<>NIL then begin
        Layer:=ADisp as ILayer;
        for k:=0 to Layer.Count-1 do begin
          ADisp:=Layer.Items[k];
          AObject:=ADisp;
          if AObject.ObjectType = ot_symbol then begin
            Asymbol:=ADisp as ISymbol;
            APoint:=ASymbol.Position;

            found2:=false;
            for j:=0 to EdgeList.Count-1 do begin
              BItem:=EdgeList.Items[j];
              if vartype(BItem) = varolestr or varstring then begin
                AName:=BItem;
                BDisp:=Layers.GetLayerByName(AName);
                if BDisp<>NIL then begin
                  BLayer:=BDisp as ILayer;
                  for m:=0 to BLayer.Count-1 do begin
                    BDisp:=BLayer.Items[m];
                    BObject:=BDisp;
                    if BObject.ObjectType = ot_Poly then begin
                      BPoly:=BDisp as IPolygon;

                      found2:=true;
                    end;
                  end;
                end;
                if found2 then begin
                  break;
                end;
              end;
            end; //Edgelist.count

          end;
        end;
      end;  //if ADisp<>NIL
    end;
  end;
end;
*)