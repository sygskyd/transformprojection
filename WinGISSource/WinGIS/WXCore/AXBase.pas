unit AXBase;

interface

uses
  ComObj, ActiveX, WinGIS_TLB, AM_Layer, AM_def
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
{$ELSE} // MAR - commented IDB due to memory problems:
  , AXIDB
{$ENDIF}
{-- IDB_AXInterface}
;

type
  TBase = class(TAutoObject, IBase)
  private
{$IFDEF AXIDB}
    AIDB: TAXIDB;
{$ENDIF}
  protected
    BaseItem : pointer;
    hIndex   :integer;
    FRect: TDRect;
{$IFDEF AXIDB}
{++ IDB_AXInterface}
    Constructor Create(AProject: Pointer = NIL; iOwnerLayerIndex: Integer = -1; iItemIndex: Integer = 0);
    function Get_IDB: IIDB; safecall;
{-- IDB_AXInterface}
{$ENDIF}
    function Get_Index: Integer; safecall;
    procedure Set_Index(Value: Integer); safecall;
    function Get_ObjectType: Integer; safecall;
    function Get_ObjectStyle: IObjectStyle; safecall;
    procedure Set_ObjectStyle(const Value: IObjectStyle); safecall;
    function Get_OtherStyle: WordBool; safecall;
    function Get_ClipRect: IRect; safecall;
    function Get_GUID: TGUID; safecall;
    procedure Set_GUID(AGUID: TGUID); safecall;
  public
{$IFDEF AXIDB}
    Destructor Destroy; override;
{$ENDIF}
  end;

implementation

uses ComServ,am_view,axrect,axdef,axobjectstyle,sysutils;

{$IFDEF AXIDB}
{++ IDB_AXInterface}
Constructor TBase.Create(AProject: Pointer = NIL; iOwnerLayerIndex: Integer = -1; iItemIndex: Integer = 0);
Begin
    Inherited Create;
    SELF.AIDB := TAXIDB.Create(AProject, iOwnerLayerIndex, iItemIndex);
End;
{$ENDIF}

{$IFDEF AXIDB}
Destructor TBase.Destroy;
Begin
     SELF.AIDB.Destroy;
     Inherited Destroy;
End;
{$ENDIF}

{$IFDEF AXIDB}
function TBase.Get_IDB: IIDB;
Begin
     RESULT := SELF.AIDB;
End;
{-- IDB_AXInterface}
{$ENDIF}

function TBase.Get_Index: Integer;
begin
   Result:=-1;
   if BaseItem <> nil then
      Result:=PView(BaseItem)^.Index;
end;

procedure TBase.Set_Index(Value: Integer);
begin
   hIndex:=Value;
end;

function TBase.Get_ObjectType: Integer;
begin
   REsult:=0;
   if BaseITem<>NIL then
      Result:=PView(BaseItem)^.GetObjtype;
end;

function TBase.Get_ObjectStyle: IObjectStyle;
begin
 Result:=NIL;
  if BaseItem<>NIL then begin
    if  PView(BaseItem)^.ObjectStyle = NIL then CreateStyle(PView(BaseItem));
    Result:=TObjectStyle.Create(PView(BaseItem)^.ObjectStyle,false);
  end;
end;

procedure TBase.Set_ObjectStyle(const Value: IObjectStyle);
begin
end;

function TBase.Get_OtherStyle: WordBool;
begin
result:=false;
if BaseITem<>NIL then
  Result:=PView(BaseItem)^.GetState(sf_Otherstyle);
  PView(BaseItem)^.HasLineStyle;
end;


function TBase.Get_ClipRect: IRect;
begin
  If BaseItem<>NIL then begin
     if PView(BaseItem)^.GetObjType = ot_Layer then begin
        FRect.Init;

        PLayer(BaseItem)^.GetSize(FRect);
        Result:=AXRect.TRect.Create(@FRect,False)
     end
     else begin
          Result:=AXRect.TRect.Create(@PView(BaseItem).ClipRect,false)
     end;
  end
  else
     REsult:=NIL;
end;

function TBase.Get_GUID: TGUID;
begin
     if BaseItem <> nil then begin
        Result:=PView(BaseItem)^.GUID;
     end;
end;

procedure TBase.Set_GUID(AGUID: TGUID);
begin
     if BaseItem <> nil then begin
        PView(BaseItem)^.GUID:=AGUID;
     end;
end;

initialization
  // TAutoObjectFactory.Create(ComServer, TBase, Class_Base,
  //  ciInternal, tmApartment);
end.
