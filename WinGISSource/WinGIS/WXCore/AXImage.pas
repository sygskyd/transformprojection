unit AXImage;

interface

uses
  ComObj, ActiveX, WinGIS_TLB, StdVcl,AXBase;

type
  TImage = class(TBase, IImage)
    FImage  : Pointer;
    Data     : Pointer;
    Release  : Boolean;
    hIndex   : Integer;
  protected
    function Get_ObjectType: Integer; safecall;
    function Get_Otherstyle: WordBool; safecall;
    function Get_ObjectStyle: IObjectStyle; safecall;
    procedure Set_ObjectStyle(const Value: IObjectStyle); safecall;
    function Get_Index: Integer; safecall;
    procedure Set_Index(Value: Integer); safecall;
    function Get_Bounds: IRect; safecall;
    function Get_FileName: WideString; safecall;
    function Get_RealFileName: WideString; safecall;
    function Get_AbsolutePath: WideString; safecall;
    function Get_bRelPath: WordBool; safecall;
    function Get_RelativePath: WideString; safecall;
    procedure SetPosition(x, y, Width, Height: Integer); safecall;
    function Get_ShowOpt:Word; safecall;
    procedure Set_ShowOpt(Value: Word); safecall;
    function Get_Options:Word; safecall;
    procedure Set_Options(Value: Word); safecall;
    function Get_TransparencyType:Byte; safecall;
    procedure Set_TransparencyType(Value: Byte); safecall;
    function Get_Transparency:Byte; safecall;
    procedure Set_Transparency(Value: Byte); safecall;
    function Get_Height: Double; safecall;
    function Get_Width: Double; safecall;
    function Get_X: Integer; safecall;
    function Get_Y: Integer; safecall;
    function Get_PXFStyle: WideString; safecall;
    procedure Set_PXFStyle(const Value: WideString); safecall;
    procedure CreateObjectStyle; safecall;
    function Get_ClipRect: IRect; safecall;
    { Protected declarations }
  Public
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
    Constructor Create(AData,AItem:POInter;ARelease:Boolean);
{$ELSE} // MAR - commented IDB due to memory problems:
    Constructor Create(AData,AItem:POInter;ARelease:Boolean; iOwnerLayerIndex: Integer = -1);
{$ENDIF}
{-- IDB_AXInterface}
    Destructor  Destroy; override;
  end;

implementation

uses ComServ,bmpImage,am_def,axobjectstyle,axdef,axrect
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
{$ELSE} // MAR - commented IDB due to memory problems:
     , Am_Index
{$ENDIF}
{-- IDB_AXInterface}
     ;

{$IFNDEF AXIDB}
Constructor TImage.Create;
{$ELSE} // MAR - commented IDB due to memory problems:
Constructor TImage.Create(AData,AItem:POInter;ARelease:Boolean; iOwnerLayerIndex: Integer = -1);
{$ENDIF}
  begin
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
    inherited Create;
{$ELSE} // MAR - commented IDB due to memory problems:
    Inherited Create(AData, iOwnerLayerIndex, PIndex(AItem).Index);
{$ENDIF}
{-- IDB_AXInterface}
    FImage:=AItem;
    BaseItem:=AItem;
    Data:=AData;
    Release:=ARelease;
    BaseItem:=AItem;
    hIndex:=-1;
  end;

Destructor TImage.Destroy;
  begin
    if Release Then Dispose(PImage(FImage),Done);
    inherited Destroy;
  end;



function TImage.Get_Otherstyle: WordBool;
begin
  Result:=false;
  if FImage <>NIL then begin
  Result:=PImage(FImage)^.GetState(sf_otherStyle);
  end;
end;

function TImage.Get_ObjectStyle: IObjectStyle;
begin
  Result:=NIL;
  if FImage<>NIL then begin
    if  PImage(FImage)^.ObjectStyle <> NIL then 
       REsult:=TObjectStyle.Create(PImage(FImage)^.ObjectStyle,false);
  end;
end;

procedure TImage.Set_ObjectStyle(const Value: IObjectStyle);
begin
 if FImage<>NIL then begin
    SetStyle(PImage(FImage),Value);
  end;
end;

function TImage.Get_Index: Integer;
begin
 Result:=-1;
  if FImage<>NIL then begin
    if HINdex<>-1 then Result:=HIndex else
    Result:=PImage(FImage)^.Index;
  end;
end;

procedure TImage.Set_Index(Value: Integer);
begin
 if FImage<>NIL then begin
   //PImage(FImage)^.Index:=Value;
   HIndex:=Value;
 end;
end;

function TImage.Get_Bounds: IRect;
begin
   Result:=AXREct.Trect.Create(@PImage(FImage)^.ClipRect,false);
end;

function TImage.Get_ClipRect: IRect;
begin
   Result:=NIL;
   if FImage <> NIL then
   begin
      Result:=AXREct.Trect.Create(@PImage(FImage)^.ClipRect,false);
   end;
end;

function TImage.Get_FileName: WideString;
begin
  Result:=PImage(FImage)^.FileName^;
end;


function TImage.Get_RelativePath : WideString ; safecall;
begin
Result:=PImage(FImage)^.RelativePath^;
end;

function TImage.Get_AbsolutePath : WideString; safecall;
begin
Result:=PImage(FImage)^.AbsolutePath^;
end;

function TImage.Get_RealFileName : WideString ; safecall;
begin
Result:=PImage(FImage)^.RealFileName^;
end;

function TImage.Get_bRelPath: WordBool;
safecall;
begin
Result:=PImage(FImage)^.bRelPath;
end;

function TImage.Get_ObjectType: Integer;
begin
  Result:=-1;
  if FImage<>NIL then begin
    REsult:=PImage(FImage)^.GetObjtype;
  end;
end;

function TImage.Get_ShowOpt:Word;
begin
   result:=PImage(FImage)^.ShowOpt;
end;

procedure TImage.Set_ShowOpt(Value: Word);
begin
   PImage(FImage)^.ShowOpt:=Value;
end;

function TImage.Get_Options:Word;
begin
   result:=PImage(FImage)^.Options;
end;

procedure TImage.Set_Options(Value: Word);
begin
   PImage(FImage)^.Options:=Value;
end;

function TImage.Get_TransparencyType:Byte;
begin
   result:=PImage(FImage)^.TransparencyType;
end;

procedure TImage.Set_TransparencyType(Value: Byte);
begin
   PImage(FImage)^.TransparencyType:=Value;
end;

function TImage.Get_Transparency:Byte;
begin
   result:=PImage(FImage)^.Transparency;
end;

procedure TImage.Set_Transparency(Value: Byte);
begin
   PImage(FImage)^.Transparency:=Value;
end;

// function is used to set the position of the current image
procedure TImage.SetPosition(X, Y, Width, Height: Integer);
var CurPosition: TDPoint;
begin
   CurPosition.X:=X;
   CurPosition.Y:=Y;
   PImage(FImage)^.ClipRect.A.Init(CurPosition.X,CurPosition.Y);
   PImage(FImage)^.ClipRect.B.Init(CurPosition.X+Width,CurPosition.Y+Height);
   PImage(FImage)^.ScaledClipRect.Assign(CurPosition.X,CurPosition.Y,CurPosition.X+Width,CurPosition.Y+Height);
end;

function TImage.Get_PXFStyle: WideString;
var TempStr, TmpStr:string;
begin
   if(PImage(FImage)^.ShowOpt AND so_ShowBMP) <> 0 then TempStr:='1'
   else TempStr:='0';
   if PImage(FImage)^.Options AND opt_HideFrame = 1 then TempStr:=TempStr+'1'
   else TempStr:=TempStr+'0';
   if PImage(FImage)^.TransparencyType = 1 then TempStr:=TempStr+'1'
   else TempStr:=TempStr+'2';
   Str(PImage(FImage)^.Transparency:3,TmpStr);
   TempStr:=TempStr+TmpStr;
   result:=TempStr;
end;

procedure TImage.Set_PXFStyle(const Value: WideString);
var Attributes:string;
    Transp     : Byte;
    Error      : Integer;
begin
   Attributes:=Value;
   if Attributes[1] = '1' then PImage(FImage)^.ShowOpt := PImage(FImage)^.ShowOpt OR so_ShowBMP
   else PImage(FImage)^.ShowOpt := PImage(FImage)^.ShowOpt AND NOT so_ShowBMP;
   Delete(Attributes,1,1);
   if Attributes[1] = '1' then PImage(FImage)^.Options:=PImage(FImage)^.Options or opt_HideFrame
   else PImage(FImage)^.Options:=PImage(FImage)^.Options and not opt_HideFrame;
   Delete(Attributes,1,1);
   if Attributes[1] = '2' then PImage(FImage)^.TransparencyType:=tt_White
   else PImage(FImage)^.TransparencyType:=tt_Black;
   Delete(Attributes,1,1);

   // set the transparancy
   Val(Attributes,Transp,Error);
   if (Error = 0) and (Transp >= 0) and (Transp <= 100) then PImage(FImage)^.Transparency:=Transp
   else PImage(FImage)^.Transparency:=0;
end;

function TImage.Get_Height: Double;
begin
   result:=PImage(FImage)^.ClipRect.YSize;
end;

function TImage.Get_Width: Double;
begin
   result:=PImage(FImage)^.ClipRect.XSize;
end;

function TImage.Get_X: Integer;
begin
   result:=PImage(FImage)^.ClipRect.A.X;
end;

function TImage.Get_Y: Integer;
begin
   result:=PImage(FImage)^.ClipRect.A.Y;
end;

procedure TImage.CreateObjectStyle;
begin
   CreateStyle(PImage(FImage));
end;

initialization
  TAutoObjectFactory.Create(ComServer, TImage, Class_Image,
    ciInternal, tmApartment);
end.
