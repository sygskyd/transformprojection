unit AXLayer;

interface

uses
  ComObj, ActiveX, WINGIS_TLB, StdVcl, AXBase, AXRect, SysUtils;

type
  TLayer = class(TBase, ILayer)
  private
    FLayer: POinter;
    Data: Pointer; //PProj
  protected
    function Get_Count: Integer; safecall;
    function Get_Items(Index: Integer): IBase; safecall;
    function Get_LayerName: WideString; safecall;
    procedure AssignStyle(const Layer: ILayer); safecall;
    procedure InsertObject(const Item: IBase); safecall;
    function InsertObjectEx(const Item: IBase): WideString; safecall;
    function Get_FillStyle: IFillStyle; safecall;
    function Get_GeneralLower: Double; safecall;
    function Get_GeneralUpper: Double; safecall;
    function Get_LineStyle: ILineStyle; safecall;
    function Get_Locked: WordBool; safecall;
    function Get_State: Integer; safecall;
    function Get_SymbolFill: ISymbolFill; safecall;
    function Get_Visible: WordBool; safecall;
    procedure Set_FillStyle(const Value: IFillStyle); safecall;
    procedure Set_GeneralLower(Value: Double); safecall;
    procedure Set_GeneralUpper(Value: Double); safecall;
    procedure Set_LineStyle(const Value: ILineStyle); safecall;
    procedure Set_Locked(Value: WordBool); safecall;
    procedure Set_SymbolFill(const Value: ISymbolFill); safecall;
    procedure Set_Visible(Value: WordBool); safecall;
    function Get_Assigned: WordBool; safecall;
    function SelectByPoint(const APoint: IPoint): IDispatch; safecall;
    function ClipByPoint(const Apoint: IPoint;
      ARadius: Integer): ISelectResult; safecall;
    function Get_Index: Integer; safecall;
    function SnapPointToLine(const APoint: IPoint; Radius: Integer): IPoint; safecall;
    procedure SelectByIndex(Index: Integer); safecall;
    procedure DeSelectByIndex(Index: Integer); safecall;
    function Get_ObjectType: Integer; safecall;
    procedure DuplicateObject(Item: OleVariant); safecall;
    procedure SetPosition(Index: Integer); safecall;
    procedure Set_LayerName(const Value: WideString); safecall;
    function HasIndex(Index: Integer): WordBool; safecall;
    procedure Clear; safecall;
    procedure SetObjectVisible(Index: Integer; Visible: WordBool); safecall;
    procedure SetAllObjectsVisible(Visible: WordBool); safecall;
    function GetPolygonByPos(X, Y: Double): IPolygon; safecall;
    function GetObjectByPos(X, Y: Double): IBase; safecall;
    procedure DeleteObject(Index: Integer); safecall;
    function GUIDToProgisID(GUID: TGUID): Integer; safecall;
    function InsertWKTObject(const WKTString: WideString): IBase; safecall;
  public
    constructor create(ALayer, AOWner: Pointer);
  end;

implementation

uses ComServ, am_layer, am_def, am_Index, am_view, am_Child, axPolygon, am_Main,
  AXPolyLine, AXPixel, AXText, AXSymbol, AXMesLine, AXDBText, ListHndl,
  AXCircle, AXArc, AXSpline, AXImage, AXFillStyle, AXLineStyle, am_proj,
  am_Poly, am_CPoly, am_Point, am_Projp, am_Text, am_splin, am_circl, bmpimage, ErrHndl,
  am_rtext, am_Meas, am_sym, am_font, am_obj, axdef, AXSymbolFill, AXSelectResult, AXPoint
// ++ Cadmensky IDB Version 2.2.9
  , IDB_CallbacksDef
// -- Cadmensky IDB Version 2.2.9
  ;

constructor TLayer.create;
begin
  inherited create;
  FLayer := ALayer;
  Data := AOwner;
  BaseItem := ALayer;
end;

function TLayer.Get_Count: Integer;
begin
  Result := -1;
  if FLayer <> nil then
  begin
    Result := PLayer(FLayer)^.Data^.GetCount;
  end;
end;

function TLayer.Get_ObjectType: Integer;
begin
  Result := -1;
  if FLayer <> nil then
  begin
    REsult := PLayer(FLayer)^.GetObjtype;
  end;
end;

function TLayer.Get_Items(Index: Integer): IBase;
var
  AIndex: PIndex;
  AView: PView;
begin
  Result := nil;
  if FLayer <> nil then
  begin
    AIndex := PLayer(FLayer)^.Data^.at(Index);
    if AIndex <> nil then
    begin
      //AView:=PView(AIndex.Mptr);

      AView := PView(PLayer(PProj(Data)^.PInfo^.Objects)^.IndexObject(PProj(Data)^.PInfo, AIndex));

      // check if object-data exists
      if (AView = nil) then
      begin
        result := nil;
        exit;
      end;

      // copy the objectstyle
      if AIndex^.ObjectStyle = nil then
      begin
        AView^.ObjectStyle := nil;
      end
      else
      begin
        try
          AIndex^.CopyObjectStyleTo(AView);
        except
          AView^.ObjectStyle := nil;
        end;
      end;

      if AIndex.GetObjType = ot_CPoly then
      begin
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
        Result := AXPolyGon.TPolygon.Create(Data, AView, false);
{$ELSE} // MAR - commented IDB due to memory problems:
        Result := AXPolyGon.TPolygon.Create(Data, AView, false, PLayer(FLayer).Index);
{$ENDIF}
{-- IDB_AXInterface}
      end
      else
        if AIndex.GetObjType = ot_Poly then
        begin
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
          Result := AXPolyLine.TPolyLine.Create(Data, AView, false);
{$ELSE} // MAR - commented IDB due to memory problems:
          Result := AXPolyLine.TPolyLine.Create(Data, AView, false, PLayer(FLayer).Index);
{$ENDIF}
{-- IDB_AXInterface}
        end
        else
          if AIndex.GetObjType = ot_Text then
          begin
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
            Result := AXText.TText.Create(Data, AView, false);
{$ELSE} // MAR - commented IDB due to memory problems:
            Result := AXText.TText.Create(Data, AView, false, PLayer(FLayer).Index);
{$ENDIF}
{-- IDB_AXInterface}
          end
          else
            if AIndex.GetObjType = ot_Symbol then
            begin
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
              Result := AXSymbol.TSymbol.Create(Data, AView, false);
{$ELSE} // MAR - commented IDB due to memory problems:
              Result := AXSymbol.TSymbol.Create(Data, AView, false, PLayer(FLayer).Index);
{$ENDIF}
{-- IDB_AXInterface}
            end
            else
              if AIndex.GetObjType = ot_Pixel then
              begin
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
                Result := AXPixel.TPixel.Create(Data, AView, false);
{$ELSE} // MAR - commented IDB due to memory problems:
                Result := AXPixel.TPixel.Create(Data, AView, false, PLayer(FLayer).Index);
{$ENDIF}
{-- IDB_AXInterface}
              end
              else
                if AIndex.GetObjType = ot_Circle then
                begin
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
                  Result := AXCircle.TCircle.Create(Data, AView, false);
{$ELSE} // MAR - commented IDB due to memory problems:
                  Result := AXCircle.TCircle.Create(Data, AView, false, PLayer(FLayer).Index);
{$ENDIF}
{-- IDB_AXInterface}
                end
                else
                  if AIndex.GetObjType = ot_Arc then
                  begin
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
                    Result := TArc.Create(Data, AView, false);
{$ELSE} // MAR - commented IDB due to memory problems:
                    Result := TArc.Create(Data, AView, false, PLayer(FLayer).Index);
{$ENDIF}
{-- IDB_AXInterface}
                  end
                  else
                    if AIndex.GetObjType = ot_MesLine then
                    begin
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
                      Result := TMesLine.Create(Data, AView, false);
{$ELSE} // MAR - commented IDB due to memory problems:
                      Result := TMesLine.Create(Data, AView, false, PLayer(FLayer).Index);
{$ENDIF}
{-- IDB_AXInterface}
                    end
                    else
                      if AIndex.GetObjType = ot_Image then
                      begin
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
                        Result := AXImage.TImage.Create(Data, AView, false);
{$ELSE} // MAR - commented IDB due to memory problems:
                        Result := AXImage.TImage.Create(Data, AView, false, PLayer(FLayer).Index);
{$ENDIF}
{-- IDB_AXInterface}
                      end
                      else
                        if AIndex.GetObjType = ot_RText then
                        begin
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
                          Result := TDBText.Create(Data, AView, false);
{$ELSE} // MAR - commented IDB due to memory problems:
                          Result := TDBText.Create(Data, AView, false, PLayer(FLayer).Index);
{$ENDIF}
{-- IDB_AXInterface}
                        end
                        else
                          if AIndex.GetObjType = ot_Spline then
                          begin
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
                            Result := AXSpline.TSPLine.Create(Data, AView, false);
{$ELSE} // MAR - commented IDB due to memory problems:
                            Result := AXSpline.TSPLine.Create(Data, AView, false, PLayer(FLayer).Index);
{$ENDIF}
{-- IDB_AXInterface}
                          end;
    end
  end;
end;

procedure TLayer.InsertObject(const Item: IBase);
var
  AProj: PProj;
  AView: PView;
  AType: Integer;
begin
  try
    if FLayer <> nil then
    begin
      Aview := nil;
      AProj := PProj(Data);
      AView := CopyObject(AProj, Item);
      if AView <> nil then
      begin
        AType := Item.ObjectType;
        InsertObjectOnLayer(AProj, AView, FLayer);
        if Atype = ot_symbol then
        begin
          Psymbol(AView)^.Calculatecliprect(AProj^.PInfo);
        end;
        if AType in [ot_Poly, ot_CPoly, ot_pixel, ot_Circle, ot_text,
          ot_symbol, ot_spline, ot_image, ot_RText, ot_mesline, ot_Arc] then
        begin
          Item.Index := AView^.Index;
        end;
      end;
    end;
  except
    on E: Exception do
    begin
      DebugMsg('TLayer.InsertObject', E.Message);
    end;
  end;
end;

function TLayer.InsertObjectEx(const Item: IBase): WideString;
var
  AProj: PProj;
  AView: PView;
  AType: Integer;
begin
  Result := '';
  try
    if FLayer <> nil then
    begin
      Aview := nil;
      AProj := PProj(Data);
      AView := CopyObject(AProj, Item);
      if AView <> nil then
      begin
        AType := Item.ObjectType;
        InsertObjectOnLayer(AProj, AView, FLayer);
        if Atype = ot_symbol then
        begin
          Psymbol(AView)^.Calculatecliprect(AProj^.PInfo);
        end;
        if AType in [ot_Poly, ot_CPoly, ot_pixel, ot_Circle, ot_text,
          ot_symbol, ot_spline, ot_image, ot_RText, ot_mesline, ot_Arc] then
        begin
          Item.Index := AView^.Index;
        end;
        Result := IntToStr(AView^.Index);
      end;
    end;
  except
    on E: Exception do
    begin
      DebugMsg('TLayer.InsertObjectEx', E.Message);
    end;
  end;
end;

procedure TLayer.DuplicateObject(Item: OleVariant);
var
  SItem: PIndex;
  ItemExists: Boolean;
begin
  try
    if FLayer <> nil then
    begin
      SItem := PLayer(FLayer)^.HasIndexObject(Item.Index);
      ItemExists := SItem <> nil;

      if not ItemExists then
        SItem := New(PIndex, Init(Item.Index));

      if Item.OtherStyle then
      begin
// ++ Cadmensky IDB Version 2.2.9
{$IFNDEF AXDLL} // <----------------- AXDLL
        if ItemExists and
          (WinGisMainForm.WeAreUsingUndoFunction[Data]) and
          (WinGisMainForm.UndoRedo_Man.AddObjectInUndoData(Data, FLayer, SItem)) then
          WinGisMainForm.UndoRedo_Man.SaveUndoData(Data, utUnInsert);
{$ENDIF}
// -- Cadmensky IDB Version 2.2.9
        SetStyle(PView(SItem), IObjectStyle(IDispatch(Item.ObjectStyle)));
        PProj(Data)^.SetModified;
      end;
      if not ItemExists then
      begin
        if not PLayer(FLayer)^.InsertObject(PProj(Data)^.PInfo, SItem, False) then
          Dispose(SItem, Done)
// ++ Cadmensky IDB Version 2.2.9
        else
        begin
{$IFNDEF AXDLL} // <----------------- AXDLL
          if (WinGisMainForm.WeAreUsingUndoFunction[Data]) and
            (WinGisMainForm.UndoRedo_Man.AddObjectInUndoData(Data, FLayer, SItem.Index)) then
            WinGisMainForm.UndoRedo_Man.SaveUndoData(Data, utUnInsert);
{$ENDIF}
        end;
      end;
// -- Cadmensky IDB Version 2.2.9
      PProj(Data)^.SetModified;
    end;
  except
    on E: Exception do
    begin
      DebugMsg('TLayer.DuplicateObject', E.Message);
    end;
  end;
end;

function TLayer.Get_LayerName: WideString;
begin
  Result := '';
  if FLayer <> nil then
  begin
    Result := PLayer(FLayer)^.Text^;
  end;
end;

function TLayer.Get_LineStyle: ILineStyle;
begin
  Result := nil;
  if FLayer <> nil then
  begin
    Result := AXLineStyle.TLineStyle.Create(@PLayer(FLayer)^.LineStyle, false);
    {
    Result.Width:=PLayer(FLayer)^.LineStyle.Width;
    Result.WidthType:=PLayer(FLayer)^.LineStyle.WidthType;
    Result.Scale:=PLayer(FLayer)^.LineStyle.Scale;
    Result.Color:=PLayer(FLayer)^.LineStyle.Color;
    Result.FillColor:=PLayer(FLayer)^.LineStyle.FillColor;
    }
  end;
end;

function TLayer.Get_Visible: WordBool;
begin
  Result := not (PLayer(FLayer)^.GetSTate(sf_LayerOff));
end;

procedure TLayer.Set_FillStyle(const Value: IFillStyle);
begin
  if FLayer <> nil then
  begin
    PLayer(FLayer)^.Fillstyle.Pattern := Value.Pattern;
    PLayer(FLayer)^.Fillstyle.ForeColor := Value.ForeColor;
    PLayer(FLayer)^.Fillstyle.BAckColor := Value.BackColor;
    PLayer(FLayer)^.Fillstyle.Scale := Value.Scale;
    PLayer(FLayer)^.Fillstyle.ScaleType := Value.ScaleType;
  end;
end;

function TLayer.Get_FillStyle: IFillStyle;
var
  AStyle: IFillStyle;
begin
  Result := nil;
  if FLayer <> nil then
  begin
    AStyle := AXFillStyle.TFillStyle.Create(@PLayer(FLayer)^.Fillstyle, false);
    {
    AStyle.Pattern:=PLayer(FLayer)^.Fillstyle.Pattern;
    AStyle.BackColor:=PLayer(FLayer)^.Fillstyle.BAckColor;
    AStyle.Scale:=PLayer(FLayer)^.Fillstyle.Scale;
    AStyle.ScaleType:=PLayer(FLayer)^.Fillstyle.ScaleType;
    }
    Result := ASTyle;
  end;
end;

procedure TLayer.Set_LineStyle(const Value: ILineStyle);
begin
  if FLayer <> nil then
  begin
    PLayer(FLayer)^.LineStyle.Width := Value.Width;
    PLayer(FLayer)^.LineStyle.WidthType := Value.WidthType;
    PLayer(FLayer)^.LineStyle.Scale := Value.Scale;
    PLayer(FLayer)^.LineStyle.Color := VAlue.Color;
    PLayer(FLayer)^.LineStyle.FillColor := VAlue.FillColor;
  end;
end;

procedure TLayer.Set_Visible(Value: WordBool);
begin
  PLayer(FLayer)^.SetState(Sf_LayerOff, not (Value));
end;

function TLayer.Get_SymbolFill: ISymbolFill;
begin
  Result := AXSymbolFill.TSymbolFill.Create;
  SetISymbolFill(Player(FLayer)^.SymbolFill, REsult);
end;

procedure TLayer.Set_SymbolFill(const Value: ISymbolFill);
begin
  if FLayer <> nil then
  begin
    SetSymbolFill(Player(FLayer)^.SymbolFill, Value);
  end;
end;

function TLayer.Get_State: Integer;
begin
  Result := -1;
  if FLayer <> nil then
  begin
    Result := PLayer(FLayer)^.State;
  end;
end;

procedure TLayer.AssignStyle(const Layer: ILayer);
var
  RLayer: PLayer;
  AFillStyle: IFillStyle;
  ALineStyle: ILineStyle;
  ASymbolfill: ISymbolFill;
begin
  RLayer := FLayer;
  AFillStyle := Layer.FillStyle;
  ALineStyle := Layer.LineStyle;
  AsymbolFill := Layer.SymbolFill;
  if RLayer <> nil then
  begin
    RLayer^.GeneralMin := Layer.GeneralLower;
    RLayer^.GeneralMax := Layer.GeneralUpper;
    RLayer^.State := Layer.State;

    SetFillStyle(RLayer^.FillStyle, AFillStyle);
    SetLineStyle(RLayer^.LineStyle, ALineStyle);
    SetSymbolFill(RLayer^.SymbolFill, ASymbolFill);
  end;
end;

function TLayer.Get_GeneralLower: Double;
begin
  Result := PLayer(FLayer)^.GeneralMin;
end;

function TLayer.Get_GeneralUpper: Double;
begin
  Result := PLayer(FLayer)^.GeneralMax;
end;

procedure TLayer.Set_GeneralLower(Value: Double);
begin
  PLayer(FLayer)^.GeneralMin := Value;
end;

procedure TLayer.Set_GeneralUpper(Value: Double);
begin
  PLayer(FLayer)^.GeneralMax := Value;
end;

function TLayer.Get_Locked: WordBool;
begin
  Result := PLayer(FLayer)^.GetState(sf_fixed);
end;

procedure TLayer.Set_Locked(Value: WordBool);
begin
  PLayer(FLayer)^.SetState(sf_fixed, Value);
end;

function TLayer.Get_Assigned: WordBool;
begin
  Result := false;
  if FLayer <> nil then
    Result := true;
end;

function TLayer.SelectByPoint(const APoint: IPoint): IDispatch;
begin

end;

function TLayer.ClipByPoint(const Apoint: IPoint;
  ARadius: Integer): ISelectResult;
var
  BPOInt: TDPoint;
  RPoint: TDpoint;
  APoly: PPoly;
  cnt: integer;
  objtypes: TObjectTypes;
begin

  REsult := nil;
  if FLayer <> nil then
  begin
    Bpoint.x := APOint.x;
    BPoint.y := APOint.y;
    cnt := -1;
    ObjTypes := [ot_Poly];
    APoly := PPoly(PLayer(FLayer)^.CutByPOint(PProj(Data)^.PInfo, BPoint, objTypes, cnt, RPOint, ARadius));
    if APOly <> nil then
    begin
      REsult := TSelectREsult.create(RPoint.x, RPOint.y, APoly, cnt);
    end
    else
      Result := nil;
  end;

end;

function TLayer.Get_Index: Integer;
begin
  Result := -1;
  if FLayer <> nil then
  begin
    Result := PLayer(FLayer)^.Index;
  end;
end;

function TLayer.SnapPointToLine(const APoint: IPoint; Radius: Integer): IPoint;
var
  BPoint: TDPoint;
  RPoint: PDPoint;
begin
  Result := nil;
  if FLayer <> nil then
  begin
    BPoint.Init(APoint.x, APoint.Y);
    RPoint := New(PDPoint, INit(0, 0));
    if PLayer(FLayer)^.SnapPointToLIne(PProj(Data)^.PInfo, BPoint, RPoint, Radius) then
      REsult := AXPoint.TPoint.Create(RPoint, true);
  end;
end;

procedure TLayer.SelectByIndex(Index: Integer);
var
  MIndex: PIndex;
  AINdex: PIndex;
begin
  if FLayer <> nil then
  begin
    MIndex := New(PIndex, Init(Index));

    AIndex := PLayer(FLayer)^.HasObject(MIndex);
    if AIndex <> nil then
    begin
      PLayer(FLayer)^.Select(PProj(Data)^.PInfo, AIndex);
    end;

    Dispose(MIndex, Done);
  end;
end;

procedure TLayer.DeSelectByIndex(Index: Integer);
var
  MIndex: PIndex;
  AINdex: PIndex;
begin
  if FLayer <> nil then
  begin
    MIndex := New(PIndex, Init(Index));

    AIndex := PLayer(FLayer)^.HasObject(MIndex);
    if AIndex <> nil then
    begin
      PLayer(FLayer)^.DeSelect(PProj(Data)^.PInfo, AIndex);
    end;
    Dispose(MIndex, Done);
  end;
end;

procedure TLayer.SetPosition(Index: Integer);
var
  OldPos: Integer;
  OldTopLayer: PLayer;
begin
  if (FLayer <> nil) and (Index > -1) then
  begin
    OldPos := PProj(Data)^.Layers^.LData^.IndexOf(FLayer);
    if Index < PProj(Data)^.Layers^.LData^.Count - 1 then
    begin
      if OldPos - 1 <> Index then
      begin
        OldTopLayer := PProj(Data)^.Layers^.TopLayer;
        if (Index = 0) or (OldPos = 1) then
        begin
          PProj(Data)^.DeselectAll(False);
        end;
        PProj(Data)^.Layers^.LData^.AtDelete(OldPos);
        PProj(Data)^.Layers^.NumberLayers;
        PProj(Data)^.Layers^.LData^.AtInsert(Index + 1, FLayer);
        PProj(Data)^.Layers^.NumberLayers;
        PProj(Data)^.Layers^.SetTopLayer(OldTopLayer);

        UpdateLayersViewsLegendsLists(Data);
        PProj(Data)^.SetModified;
      end;
    end;
  end;
end;

procedure TLayer.Set_LayerName(const Value: WideString);
begin
  if FLayer <> nil then
  begin
    PLayer(FLayer)^.SetText(string(Value));
  end;
end;

function TLayer.HasIndex(Index: Integer): WordBool;
begin
  if FLayer <> nil then
  begin
    Result := (PLayer(FLayer)^.HasIndexObject(Index) <> nil);
  end;
end;

procedure TLayer.Clear;
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  try
    if FLayer <> nil then
    begin
      ProcClearLayer2(Data, FLayer);
    end;
  except
    on E: Exception do
    begin
      DebugMsg('TLayer.Clear', E.Message);
    end;
  end;
{$ENDIF}  
end;

procedure TLayer.SetObjectVisible(Index: Integer; Visible: WordBool);
begin
  if FLayer <> nil then
  begin
    PLayer(FLayer)^.SetObjVisible(Index, Visible);
  end;
end;

procedure TLayer.SetAllObjectsVisible(Visible: WordBool);
begin
  if FLayer <> nil then
  begin
    PLayer(FLayer)^.SetAllObjVisible(Visible);
  end;
end;

function TLayer.GetPolygonByPos(X, Y: Double): IPolygon;
var
  AIndex: PIndex;
  AView: PView;
  APoint: TDPoint;
begin
  if FLayer <> nil then
  begin
    APoint.Init(Trunc(X * 100), Trunc(Y * 100));
    AIndex := PLayer(FLayer)^.SearchByPoint(PProj(Data)^.PInfo, APoint, [ot_CPoly]);
    if AIndex <> nil then
    begin
      AView := PView(PLayer(PProj(Data)^.PInfo^.Objects)^.IndexObject(PProj(Data)^.PInfo, AIndex));
      if AView <> nil then
      begin
        Result := AXPolygon.TPolygon.Create(Data, AView, False);
      end;
    end;
  end;
end;

function TLayer.GetObjectByPos(X, Y: Double): IBase;
var
  AIndex: PIndex;
  AView: PView;
  APoint: TDPoint;
begin
  if FLayer <> nil then
  begin
    APoint.Init(Round(X * 100), Round(Y * 100));
    AIndex := PLayer(FLayer)^.SearchByPoint(PProj(Data)^.PInfo, APoint, ot_Any);
    if AIndex <> nil then
    begin
      AView := PView(PLayer(PProj(Data)^.PInfo^.Objects)^.IndexObject(PProj(Data)^.PInfo, AIndex));
      if AView <> nil then
      begin
        if AIndex.GetObjType = ot_CPoly then
        begin
          Result := AXPolygon.TPolygon.Create(Data, AView, false);
        end
        else
          if AIndex.GetObjType = ot_Poly then
          begin
            Result := AXPolyline.TPolyLine.Create(Data, AView, false);
          end
          else
            if AIndex.GetObjType = ot_Text then
            begin
              Result := AXText.TText.Create(Data, AView, false);
            end
            else
              if AIndex.GetObjType = ot_Symbol then
              begin
                Result := AXSymbol.TSymbol.Create(Data, AView, false);
              end
              else
                if AIndex.GetObjType = ot_Pixel then
                begin
                  Result := AXPixel.TPixel.Create(Data, AView, false);
                end
                else
                  if AIndex.GetObjType = ot_Circle then
                  begin
                    Result := AXCircle.TCircle.Create(Data, AView, false);
                  end
                  else
                    if AIndex.GetObjType = ot_Arc then
                    begin
                      Result := TArc.Create(Data, AView, false);
                    end
                    else
                      if AIndex.GetObjType = ot_MesLine then
                      begin
                        Result := TMesLine.Create(Data, AView, false);
                      end
                      else
                        if AIndex.GetObjType = ot_Image then
                        begin
                          Result := AXImage.TImage.Create(Data, AView, false);
                        end
                        else
                          if AIndex.GetObjType = ot_RText then
                          begin
                            Result := TDBText.Create(Data, AView, false);
                          end
                          else
                            if AIndex.GetObjType = ot_Spline then
                            begin
                              Result := AXSpline.TSPLine.Create(Data, AView, false);
                            end;
      end;
    end;
  end;
end;

procedure TLayer.DeleteObject(Index: Integer); safecall;
var
  AItem: PIndex;
  AView: PView;
begin
  try
    if FLayer <> nil then
    begin
      AItem := New(PIndex, Init(Index));
      try
        AView := PLayer(FLayer)^.IndexObject(PProj(Data)^.PInfo, AItem);
        if AView <> nil then
        begin
          AView^.Invalidate(PProj(Data)^.PInfo);
          PProj(Data)^.MultiMedia^.DelMediasAtDel(AItem^.Index);
          PLayer(FLayer)^.DeleteIndex(PProj(Data)^.PInfo, AItem);
          if PProj(Data)^.Layers^.ExistsObject(AItem) = 0 then
          begin
            PObjects(PProj(Data)^.PInfo^.Objects)^.DeleteIndex(PProj(Data)^.PInfo, AItem);
          end;
        end;
      finally
        Dispose(AItem, Done);
      end;
    end;
  except
    on E: Exception do
    begin
      DebugMsg('TLayer.DeleteObject', E.Message);
    end;
  end;
end;

function TLayer.GUIDToProgisID(GUID: TGUID): Integer;
begin
  Result := -1;
  try
    if FLayer <> nil then
    begin
      Result := PLayer(FLayer)^.GUIDToIndex(GUID);
    end;
  except
    on E: Exception do
    begin
      DebugMsg('TLayer.GUIDToProgisID', E.Message);
    end;
  end;
end;

function TLayer.InsertWKTObject(const WKTString: WideString): IBase;
var
  AView: PView;
begin
  Result := nil;
  try
    if FLayer <> nil then
    begin
      AView := PProj(Data)^.InsertWKTObject(PLayer(FLayer), AnsiString(WKTString));
      if AView <> nil then
      begin
        if AView.GetObjType = ot_CPoly then
        begin
          Result := AXPolygon.TPolygon.Create(Data, AView, false);
        end
        else
          if AView.GetObjType = ot_Poly then
          begin
            Result := AXPolyline.TPolyLine.Create(Data, AView, false);
          end
          else
            if AView.GetObjType = ot_Pixel then
            begin
              Result := AXPixel.TPixel.Create(Data, AView, false);
            end;
      end;
    end;
  except
    on E: Exception do
    begin
      DebugMsg('TLayer.InsertWKTObject', E.Message);
    end;
  end;
end;

initialization
  TAutoObjectFactory.Create(ComServer, TLayer, Class_Layer,
    ciInternal, tmApartment);
end.

