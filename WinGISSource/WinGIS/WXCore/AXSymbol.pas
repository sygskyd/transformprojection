unit AXSymbol;

interface

uses
  ComObj, ActiveX, WINGIS_TLB, StdVcl, AXBase;

type
  TSymbol = class(TBase, ISymbol)
    FSymbol: POinter;
    Data: Pointer;
    Release: Boolean;
    hIndex: Integer;
  protected
    function Get_ObjectType: Integer; safecall;
    function Get_Angle: Double; safecall;
    function Get_Position: IPoint; safecall;
    function Get_Size: Double; safecall;
    function Get_SizeType: Integer; safecall;
    function Get_SymIndex: Integer; safecall;
    procedure Set_Angle(Value: Double); safecall;
    procedure Set_Position(const Value: IPoint); safecall;
    procedure Set_Size(Value: Double); safecall;
    procedure Set_SizeType(Value: Integer); safecall;
    function Get_SymbolName: WideString; safecall;
    procedure Set_SymbolName(const Value: WideString); safecall;
    procedure Set_SymIndex(Value: Integer); safecall;
    function Get_OtherStyle: WordBool; safecall;
    function Get_ObjectStyle: IObjectStyle; safecall;
    procedure Set_ObjectStyle(const Value: IObjectStyle); safecall;
    function Get_Index: Integer; safecall;
    procedure Set_Index(Value: Integer); safecall;
    function Get_ClipRect: IRect; safecall;
    procedure CreateObjectStyle; safecall;
    procedure MoveAndRedraw(NewX, NewY, NewSymIdx: Integer); safecall;
    procedure Redraw; safecall;
    procedure FollowObject; safecall;
    procedure ZoomToObject; safecall;
    //procedure InsertObject(LayerIndex: Integer); safecall;
    { Protected declarations }
  public
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
    constructor Create(AData, AItem: POInter; ARelease: Boolean);
{$ELSE} // MAR - commented IDB due to memory problems:
    constructor Create(AData, AItem: POInter; ARelease: Boolean; iOwnerLayerIndex: Integer = -1);
{$ENDIF}
{-- IDB_AXInterface}
    destructor Destroy; override;
  end;

implementation

uses ComServ, am_sym, axdef, am_def, AXPoint, am_Proj, am_layer, am_index, am_group, am_obj,
  axobjectstyle, axrect, am_projp, SysUtils, ErrHndl;

{$IFNDEF AXIDB}

constructor TSymbol.Create;
{$ELSE} // MAR - commented IDB due to memory problems:

constructor TSymbol.Create(AData, AItem: POInter; ARelease: Boolean; iOwnerLayerIndex: Integer = -1);
{$ENDIF}
begin
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
  inherited Create;
{$ELSE} // MAR - commented IDB due to memory problems:
  inherited Create(AData, iOwnerLayerIndex, PIndex(AItem).Index);
{$ENDIF}
{-- IDB_AXInterface}
  FSymbol := AItem;
  BaseItem := AItem;
  Data := AData;
  Release := ARelease;
  hIndex := -1;
end;

destructor TSymbol.Destroy;
begin
  if Release then
    Dispose(PSymbol(FSymbol), Done);
  inherited Destroy;
end;

function TSymbol.Get_ObjectType: Integer;
begin
  Result := -1;
  if FSymbol <> nil then
  begin
    Result := PSymbol(FSymbol)^.GetObjtype;
  end;
end;

function TSymbol.Get_Angle: Double;
begin
  Result := -1;
  if FSymbol <> nil then
  begin
    Result := PSymbol(FSymbol)^.Angle;
  end;
end;

function TSymbol.Get_Position: IPoint;
//var APoint : PDPoint;
begin
  Result := nil;
  if FSymbol <> nil then
  begin
    //APOint:=new(PDPOint,Init(PSymbol(FSymbol)^.Position.x,PSymbol(FSymbol)^.Position.y));
    //Result:=AXPoint.TPoint.Create(APoint,true);
    Result := AXPoint.TPoint.Create(@PSymbol(FSymbol)^.Position, false);
  end;
end;

function TSymbol.Get_Size: Double;
begin
  Result := -1;
  if FSymbol <> nil then
  begin
    Result := PSymbol(FSymbol)^.Size;
  end;
end;

function TSymbol.Get_SizeType: Integer;
begin
  Result := -1;
  if FSymbol <> nil then
  begin
    Result := PSymbol(FSymbol)^.SizeType;
  end;
end;

function TSymbol.Get_SymIndex: Integer;
begin
  Result := -1;
  if FSymbol <> nil then
  begin
    Result := PSymbol(FSymbol)^.SymIndex;
  end;
end;

procedure TSymbol.Set_Angle(Value: Double);
begin
  if FSymbol <> nil then
  begin
    PSymbol(FSymbol)^.Angle := Value;
  end;
end;

procedure TSymbol.Set_Position(const Value: IPoint);
begin
  if FSymbol <> nil then
  begin
    PSymbol(FSymbol)^.Position.x := Value.x;
    PSymbol(FSymbol)^.Position.y := Value.y;
  end;
end;

procedure TSymbol.Set_Size(Value: Double);
begin
  if FSymbol <> nil then
  begin
    PSymbol(FSymbol)^.Size := Value;
  end;
end;

procedure TSymbol.Set_SizeType(Value: Integer);
begin
  if FSymbol <> nil then
  begin
    PSymbol(FSymbol)^.SizeType := Value;
  end;
end;

procedure TSymbol.Set_SymIndex(Value: Integer);
begin
  if FSymbol <> nil then
  begin
    PSymbol(FSymbol)^.SymIndex := Value;
  end;
end;

function TSymbol.Get_SymbolName: WideString;
var
  ALayer: PLayer;
  AIndex: TIndex;
  BIndex: PSGroup;
begin
  REsult := '';
  if FSymbol <> nil then
  begin
    AIndex.Index := PSymbol(FSymbol)^.SymIndex;
    ALayer := PSymbols(PProj(Data)^.Pinfo^.Symbols);
    BIndex := PSGroup(ALayer^.HasObject(@Aindex));
    if BIndex <> nil then
      Result := BIndex^.Name^;
  end;
end;

procedure TSymbol.Set_SymbolName(const Value: WideString);
var
  AGroup: PSGroup;
begin
  if FSymbol <> nil then
  begin
    AGroup := PSymbols(PProj(Data)^.Pinfo^.Symbols)^.GetSymbol(Value, '');
    if AGroup <> nil then
      PSymbol(FSymbol)^.SymIndex := AGroup^.Index;
  end;
end;

function TSymbol.Get_OtherStyle: WordBool;
begin
  result := false;
  if FSymbol <> nil then
  begin
    REsult := PSymbol(FSymbol)^.GetState(sf_otherStyle);
  end;
end;

function TSymbol.Get_ObjectStyle: IObjectStyle;
begin
  Result := nil;
  if FSymbol <> nil then
  begin
    if PSymbol(FSymbol)^.ObjectStyle <> nil then
      REsult := TObjectStyle.Create(PSymbol(FSymbol)^.ObjectStyle, false);
  end;
end;

procedure TSymbol.Set_ObjectStyle(const Value: IObjectStyle);
begin
  if FSymbol <> nil then
  begin
    SetStyle(PSymbol(FSymbol), Value);
  end;
end;

function TSymbol.Get_Index: Integer;
begin
  Result := -1;
  if FSymbol <> nil then
  begin
    if HIndex <> -1 then
      Result := HIndex
    else
      Result := PSymbol(FSymbol)^.Index;
  end;
end;

procedure TSymbol.Set_Index(Value: Integer);
begin
  if FSymbol <> nil then
  begin
   //PSymbol(FSymbol)^.Index:=Value;
    HIndex := Value;
  end;
end;

function TSymbol.Get_ClipRect: IRect;
begin
  REsult := AXRect.TRect.Create(@PSymbol(FSymbol).ClipRect, false);
end;

procedure TSymbol.CreateObjectStyle;
begin
  CreateStyle(PSymbol(FSymbol));
end;

// method is used to move the symbol and redraw it on screen
// at this operation it is also possible to change the symbol
// that should be displayed

procedure TSymbol.MoveAndRedraw(NewX, NewY, NewSymIdx: Integer);
begin
  try
    PSymbol(FSymbol)^.Invalidate(PProj(Data)^.PInfo);
      // setup the symbol-index
    PSymbol(FSymbol)^.SetSymPtrByIndex(PProj(Data)^.PInfo^.Symbols, NewSymIdx);
    PSymbol(FSymbol)^.SymIndex := NewSymIdx;

    PSymbol(FSymbol)^.Position.X := NewX;
    PSymbol(FSymbol)^.Position.Y := NewY;
    PSymbol(FSymbol)^.CalculateClipRect(PProj(Data)^.PInfo);
    PSymbol(FSymbol)^.Invalidate(PProj(Data)^.PInfo);

    PProj(Data)^.UpdateClipRect(PSymbol(FSymbol));
    PProj(Data)^.CorrectSize(PSymbol(FSymbol)^.ClipRect, TRUE);
    PProj(Data)^.PInfo^.RedrawInvalidate;
  except
    on E: Exception do
    begin
      DebugMsg('TSymbol.MoveAndRedraw', E.Message);
    end;
  end;
end;

// method is used to redraw the symbol

procedure TSymbol.Redraw;
begin
  PSymbol(FSymbol)^.Invalidate(PProj(Data)^.PInfo, False);
  PSymbol(FSymbol)^.CalculateClipRect(PProj(Data)^.PInfo);
  PSymbol(FSymbol)^.Invalidate(PProj(Data)^.PInfo);
  if not PProj(Data)^.CorrectSize(PSymbol(FSymbol)^.ClipRect, TRUE) then
  begin
    PProj(Data)^.PInfo^.RedrawInvalidate;
  end;
  PProj(Data)^.UpdateClipRect(PSymbol(FSymbol));
  PProj(Data)^.SelAllRect.CorrectByRect(PSymbol(FSymbol)^.ClipRect);
end;

procedure TSymbol.FollowObject;
var
  CurrentView: TDRect;
begin
  PProj(Data)^.PInfo^.GetCurrentScreen(CurrentView);
  if ((PSymbol(FSymbol)^.Position.X <= CurrentView.A.X) or (PSymbol(FSymbol)^.Position.X >= CurrentView.B.X) or
    (PSymbol(FSymbol)^.Position.Y <= CurrentView.A.Y) or (PSymbol(FSymbol)^.Position.Y >= CurrentView.B.Y)) then
  begin
      // the object is outside of the current screen
      // so it has to be rezoomed
{$IFNDEF AXDLL} // <----------------- AXDLL
    FuncZoomToObject(PProj(Data), PSymbol(FSymbol)^.Index);
{$ENDIF}
  end;
end;

procedure TSymbol.ZoomToObject;
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  FuncZoomToObject(PProj(Data), PSymbol(FSymbol)^.Index);
{$ENDIF}  
end;

initialization
  TAutoObjectFactory.Create(ComServer, TSymbol, Class_Symbol,
    ciInternal, tmApartment);
end.

