unit AXDocument;

interface

uses
  ComObj, ActiveX, Wingis_TLB, StdVcl, am_Coll, am_admin, AXIDB;

type
  TDocument = class(TAutoObject, IDocument)
  private
    FDocument: Pointer; //TXProj
    FFileName: string;
    ToExportCreated: boolean;
    ToExport: TViewColl;
    SelLayername: string;
    FIDB: IIDB;
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
{$ELSE} // MAR - commented IDB due to memory problems:
    iLastLayerWithSelectedItems: Integer;
{$ENDIF}
{-- IDB_AXInterface}
  protected
    { Protected declarations }
    function CreatePixel: IPixel; safecall;
    function CreatePoint: IPoint; safecall;
    function Get_Layers: ILayers; safecall;
    function Get_Name: WideString; safecall;
    procedure Set_Name(const Value: WideString); safecall;
    function CreatePolygon: IPolygon; safecall;
    function CreatePolyLine: IPolyline; safecall;
    function CreateText: IText; safecall;
    procedure Redraw; safecall;
    function CreateList: IList; safecall;
    procedure Save; safecall;
    procedure SaveAs(const FileName: WideString); safecall;
    function CreateArc: IArc; safecall;
    function CreateCircle: ICircle; safecall;
    function CreateDBText: IDBText; safecall;
    function CreateDPoint: IDPoint; safecall;
    function CreateImage(const FileName: WideString): IImage; safecall;
    function CreateMesLine: IMesLine; safecall;
    function CreateRect: IRect; safecall;
    function CreateSpline: ISpline; safecall;
    function CreateSymbol: ISymbol; safecall;
    function CreateSymbolDef: ISymbolDef; safecall;
    function CreateIslandArea(const List: IList): IPolygon; safecall;
    function Get_Bounds: IRect; safecall;
    function Get_Symbols: ISymbols; safecall;
    procedure Set_Bounds(const Value: IRect); safecall;
    function CreateFillStyle: IFillStyle; safecall;
    function CreateLineStyle: ILineStyle; safecall;
    function IDocument_CreateFontDef: IDispatch; safecall;
    function Get_Fonts: IFonts; safecall;
    procedure ReArrangeBounds; safecall;
    function CreateCustomers: ICustomers; safecall;
    function CreateCustomerItem: ICustomerItem; safecall;
    function CreateGenerateObject: IGenerateObject; safecall;
    function Get_Project: Integer; safecall;
    function CreateLine: ILine; safecall;
    procedure RedrawRect(const ARect: IRect); safecall;
    procedure ZoomByRect(const Rect: IRect; Frame: Integer); safecall;
    procedure DeselectAll; safecall;
    procedure CreateSelectedList; safecall;
    function Get_NrOfSelectedObjects: Integer; safecall;
    function Get_SelItems(Index: Integer): IBase; safecall;
    procedure DestroySelectedList; safecall;
    function Get_Projection: Integer; safecall;
    procedure Set_Projection(Value: Integer); safecall;
    function Get_ProjectProjection: WideString; safecall;
    procedure Set_ProjectProjection(const Value: WideString); safecall;
    function Get_Projectdate: WideString; safecall;
    procedure Set_Projectdate(const Value: WideString); safecall;
    function Get_ProjectProjSettings: WideString; safecall;
    procedure Set_ProjectProjSettings(const Value: WideString); safecall;
    function Get_ProjectionXOffset: Double; safecall;
    procedure Set_ProjectionXOffset(Value: Double); safecall;
    function Get_ProjectionYOffset: Double; safecall;
    procedure Set_ProjectionYOffset(Value: Double); safecall;
    function Get_ProjectionScale: Double; safecall;
    procedure Set_ProjectionScale(Value: Double); safecall;
    function GetObjectByIndex(Index: Integer): IBase; safecall;
    procedure DeleteObjectByIndex(Index: Integer); safecall;
    procedure DeleteLayerByName(const Name: WideString); safecall;
    procedure SelectByRect(const Rect: IRect); safecall;
    procedure SelectByRectTouch(const Rect: IRect); safecall;
    procedure FinishImpExpRoutine(ExitMode: WordBool; const ModuleType: WideString); safecall;
    procedure DoAmpDkmImport(const StartDir: WideString; const AmpFileName: WideString;
      const DkmDBDir: WideString; DestDBMode: Integer;
      const DestDatabase: WideString; const DestTable: WideString;
      const LngCode: WideString); safecall;
    procedure ExportUserDefStylesToWlf(const Filename: WideString); safecall;
    procedure ImportUserDefStylesFromWlf(const Filename: WideString); safecall;
    function GetUserFillStyleNumberByName(const StyleName: WideString): Integer; safecall;
    function GetUserFillStyleNameByNumber(StyleNumber: Integer): WideString; safecall;
    function GetUserLineStyleNumberByName(const StyleName: WideString): Integer; safecall;
    function GetUserLineStyleNameByNumber(StyleNumber: Integer): WideString; safecall;
    function CreateSight(const Name: WideString; X: Double; Y: Double; Height: Double;
      Width: Double; Rotation: Double): ISight; safecall;
    function Get_Sights: ISights; safecall;
    function Get_StartProjectGUID: WideString; safecall;
    function Get_IDB: IIDB; safecall;
    function Get_CurrentProjectGUID: WideString; safecall;
    function ConvToPoly(Item: OleVariant): IPolyline; safecall;
    procedure SelectSearchText(const aText: WideString); safecall;
    procedure DeleteSelectedObjects; safecall;
    procedure FinishModule(DllIdx: Integer); safecall;
    procedure SelectObjectByIndex(Index: integer); safecall;
    function IsModuleRegistered(ModuleIdx: Integer): WordBool; safecall;
    function Get_IdbProcessMask: Integer; safecall;
    procedure Set_IdbProcessMask(Value: Integer); safecall;
    procedure Close; safecall;
    procedure SetCanvas(ParentHandle: Integer; Mode: Integer); safecall;
    procedure ZoomByScale(Scale: Double); safecall;
    procedure ZoomByFactor(Scale: Double); safecall;
    procedure ZoomAll; safecall;
    procedure ZoomToSelection; safecall;
    procedure ImportSymbolLib(const Filename: WideString); safecall;
    function GetAttachedText(Index: Integer): WideString; safecall;
    function LongLatToXY(var X, Y: Double): Integer; safecall;
    function XYToLongLat(var X, Y: Double): Integer; safecall;
    function BingMapsOpen(const User, Pwd: WideString; Offline: WordBool): Integer; safecall;
    function BingMapsClose: Integer; safecall;
    procedure ZoomToPoint(X, Y, Scale: Double); safecall;
    function ProgisIDToGUID(Index: Integer): TGUID; safecall;
    function GUIDToProgisID(GUID: TGUID): Integer; safecall;
  public
    constructor Create(ADocument: Pointer);
    destructor Destroy; override;
  end;

implementation

uses ComServ, am_proj, am_main,
  am_def, am_child, AXLayers, AXPixel, am_Point, AXPoint, am_cpoly, AXPolygon,
  AXPolyLIne, am_Poly, Forms, am_text, AXText, am_font, am_Circl, AXCircle, AXArc, am_Meas,
  AXMesLine, am_rText, AXDBText, am_bugra, am_splin, AXSpline, Am_Sym, AXSymbol,
  bmpimage, AXImage, AXRect, am_obj, am_group, AXSymbolDef, AXSymbols, AXList, axdef, am_view,
  am_Layer, classes, sysutils, AXFillStyle, AXLineStyle, AXFonts, AXCustomers, AXCustomerItem,
  AXGenerateObject, axline, GrTools, am_index, axlayer, coordinatesystem,
  am_Sight, AXSight, AXSights, Idb_Consts, Objects, am_projo, LicenseHndl, Windows, Messages,
  ProjectionTools, ErrHndl;

constructor TDocument.Create;
begin
  inherited Create;
  FDocument := ADocument;
  FFileName := PProj(TMDIChild(FDocument).Data)^.FName;
  FIDB := IIDB(TAXIDB.Create);
  ToExportCreated := FALSE;
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
{$ELSE} // MAR - commented IDB due to memory problems:
  SELF.iLastLayerWithSelectedItems := -1;
{$ENDIF}
{-- IDB_AXInterface}
end;

destructor TDocument.Destroy;
var
  hName: array[0..255] of char;
begin
  if FDocument <> nil then
  begin
    strpCopy(hName, FFileName);
    WingisMainForm.FileNameClose(hName);
  end;
  inherited Destroy;
end;

function TDocument.CreatePixel: IPixel;
var
  APixel: PPixel;
  APOint: TDPoint;
begin
  Result := nil;
  if FDocument <> nil then
  begin
    APoint.Init(0, 0);
    APixel := new(PPixel, Init(APOint));
{++ IDB_AXInterface}
{ In all similar cases I guess the inserting is made on TOP layer of a project. Ivanoff. 28.11.2000.}
{$IFNDEF AXIDB}
    Result := AXPixel.TPixel.create(TMDIChild(FDocument).Data, APixel, true);
{$ELSE} // MAR - commented IDB due to memory problems:
    Result := AXPixel.TPixel.create(TMDIChild(FDocument).Data, APixel, true, TMDIChild(FDocument).Data.Layers.TopLayer.Index);
{$ENDIF}
{-- IDB_AXInterface}
  end;
end;

function TDocument.CreatePoint: IPoint;
var
  APoint: PDPOint;
begin
  APOint := new(PDPOint, Init(0, 0));
  Result := AXPoint.TPoint.Create(APoint, true);
end;

function TDocument.Get_Layers: ILayers;
begin
  Result := nil;
  if FDocument <> nil then
  begin
    Result := TLayers.Create(PProj(TMDIChild(FDocument).Data)^.Layers, TMDIChild(FDocument).Data);
  end;
end;

function TDocument.Get_Name: WideString;
begin
  // Result:=StrPas(PProj(TMDIChild(FDocument))^.FName);
  Result := FFilename;
end;

procedure TDocument.Set_Name(const Value: WideString);
begin
  FFilename := Value;
end;

function TDocument.CreatePolygon: IPolygon;
var
  APolygon: PCPoly;
begin
  APolygon := new(PCPoly, Init);
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
  Result := AXPolygon.TPolygon.Create(TMDIChild(FDocument).Data, APolygon, true);
{$ELSE} // MAR - commented IDB due to memory problems:
  Result := AXPolygon.TPolygon.Create(TMDIChild(FDocument).Data, APolygon, true, TMDIChild(FDocument).Data.Layers.TopLayer.Index);
{$ENDIF}
{-- IDB_AXInterface}
end;

function TDocument.CreatePolyLine: IPolyLine;
var
  APolyLine: PPoly;
begin
  APolyLine := new(PPoly, Init);
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
  Result := AXPolyLine.TPolyLine.Create(TMDIChild(FDocument).Data, APolyLIne, true);
{$ELSE} // MAR - commented IDB due to memory problems:
  Result := AXPolyLine.TPolyLine.Create(TMDIChild(FDocument).Data, APolyLIne, true, TMDIChild(FDocument).Data.Layers.TopLayer.Index);
{$ENDIF}
{-- IDB_AXInterface}
end;

procedure TDocument.Redraw;
begin
  if FDocument <> nil then
  begin
    PProj(TMDIChild(FDocument).Data)^.UpdatePaintOffset;
  end;
end;

function TDocument.CreateText: IText;
var
  AText: PText;
  AFont: TFontData;
begin
  AText := new(PText, Init(Afont, '', 0));
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
  Result := AXText.TText.Create(TMDIChild(FDocument).Data, AText, true);
{$ELSE} // MAR - commented IDB due to memory problems:
  Result := AXText.TText.Create(TMDIChild(FDocument).Data, AText, true, TMDIChild(FDocument).Data.Layers.TopLayer.Index);
{$ENDIF}
{-- IDB_AXInterface}
end;

function TDocument.CreateList: IList;
begin
  Result := AXList.TList.Create;
end;

procedure TDocument.Save;
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  TMDIChild(FDocument).FileSave;
{$ENDIF}
end;

function TDocument.CreateArc: IArc;
var
  AArc: PEllipseArc;
  APoint: TDPoint;
begin
  AArc := new(PEllipseArc);
  AArc^.INit(APoint, 0, 0, 0, 0, 0);
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
  REsult := AXArc.TArc.Create(TMDIChild(FDocument).Data, AArc, true);
{$ELSE} // MAR - commented IDB due to memory problems:
  REsult := AXArc.TArc.Create(TMDIChild(FDocument).Data, AArc, true, TMDIChild(FDocument).Data.Layers.TopLayer.Index);
{$ENDIF}
{-- IDB_AXInterface}
end;

function TDocument.CreateCircle: ICircle;
var
  ACirc: PEllipse;
  aPoint: TDPoint;
begin
  ACirc := new(PEllipse);
  ACirc^.INit(APoint, 0, 0, 0);
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
  REsult := AXCircle.Tcircle.Create(TMDIChild(FDocument).Data, ACirc, true);
{$ELSE} // MAR - commented IDB due to memory problems:
  REsult := AXCircle.Tcircle.Create(TMDIChild(FDocument).Data, ACirc, true, TMDIChild(FDocument).Data.Layers.TopLayer.Index);
{$ENDIF}
{-- IDB_AXInterface}
end;

function TDocument.CreateDBText: IDBText;
var
  AText: PRText;
  AFont: TFontData;
begin
  Result := nil;
  AText := new(PRText, init(AFont, ''));
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
  Result := AXDBText.TDBTExt.Create(TMDIChild(FDocument).Data, AText, true);
{$ELSE} // MAR - commented IDB due to memory problems:
  Result := AXDBText.TDBTExt.Create(TMDIChild(FDocument).Data, AText, true, TMDIChild(FDocument).Data.Layers.TopLayer.Index);
{$ENDIF}
{-- IDB_AXInterface}
end;

function TDocument.CreateImage(const FileName: WideString): IImage;
var
  AIMage: PImage;
  ASettings: TBitMapSettings;
begin
  Result := nil;
  AImage := new(PImage, init(FileName, ExtractRelativePath(PProj(TMDIChild(FDocument).Data)^.FName, FileName), so_ShowBMP, PProj(TMDIChild(FDocument).Data)^.PInfo^.BitmapSettings));
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
  Result := AXImage.TImage.Create(TMDIChild(FDocument).Data, AImage, true);
{$ELSE} // MAR - commented IDB due to memory problems:
  Result := AXImage.TImage.Create(TMDIChild(FDocument).Data, AImage, true, TMDIChild(FDocument).Data.Layers.TopLayer.Index);
{$ENDIF}
{-- IDB_AXInterface}
end;

function TDocument.CreateSpline: ISpline;
var
  ASpline: PSpline;
begin
  Result := nil;
  ASPline := new(PSpline, Init);
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
  Result := AXSpline.TSpline.Create(TMDIChild(FDocument).Data, ASpline, true);
{$ELSE} // MAR - commented IDB due to memory problems:
  Result := AXSpline.TSpline.Create(TMDIChild(FDocument).Data, ASpline, true, TMDIChild(FDocument).Data.Layers.TopLayer.Index);
{$ENDIF}
{-- IDB_AXInterface}
end;

function TDocument.CreateSymbol: ISymbol;
var
  ASymbol: PSymbol;
  APoint: TDPoint;
begin
  Result := nil;
  ASymbol := new(PSymbol, Init(PProj(TMDIChild(FDocument).Data)^.PInfo, APoint, 0, 0, 0));
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
  Result := AXSymbol.TSymbol.Create(TMDIChild(FDocument).Data, ASymbol, true);
{$ELSE} // MAR - commented IDB due to memory problems:
  Result := AXSymbol.TSymbol.Create(TMDIChild(FDocument).Data, ASymbol, true, TMDIChild(FDocument).Data.Layers.TopLayer.Index);
{$ENDIF}
{-- IDB_AXInterface}
end;

function TDocument.CreateRect: IRect;
begin
  REsult := AXREct.TRect.Create(nil, true);
end;

function TDocument.CreateSymbolDef: ISymbolDef;
var
  AGroup: PSGroup;
begin
  AGroup := new(PSGroup, init);
  Result := TSymbolDef.Create(TMDIChild(FDocument).Data, AGroup, true);
end;

function TDocument.Get_Symbols: ISymbols;
begin
  REsult := AXSymbols.TSymbols.Create(TMDIChild(FDocument).Data,
    PProj(TMDIChild(FDocument).Data)^.Pinfo^.Symbols);
end;

function TDocument.CreateIslandArea(const List: IList): IPolygon;
var
  APolygon: PCPoly;
  VarItem: IBase;
  cnt: integer;
  AType: Integer;
  Abort: Boolean;
  AView: PView;
  InsPoly: PCPoly;
  SrcLayer: TList;
  DestLayer: TList;
begin
  Result := nil;
  Abort := false;
  if List <> nil then
  begin
    SrcLayer := Classes.TList.Create;
    DestLayer := Classes.TList.Create;
    for cnt := 0 to List.count - 1 do
    begin
      VarItem := List.Items[cnt];
      try
        AType := VarItem.ObjectType;
      except
        Abort := true;
      end;
      if not Abort then
      begin //Object mit ObjectType
        if VarItem.ObjectType = ot_CPoly then
        begin
          //Polygon in Src-Layer List aufnehmen
          InsPoly := PCPoly(AXDef.CopyObject(TMDIChild(FDocument).Data, VarItem));
          if InsPoly <> nil then
          begin
            SrcLayer.Add(InsPoly);
          end;
        end;
      end;
    end;
    if SrcLayer.Count > 0 then
    begin
      //Polys auf Src-Layer nach DestLayer (Island konvertieren)
      AXDef.CreateIslandArea(TMDIChild(FDocument).Data, SrcLayer, DestLayer);
      FreeLayer(SrcLayer);
      //FreeLayer(DestLayer);
      //APolygon:=new(PCPoly,Init);
      APolygon := DestLayer[0];
      DestLayer.Free;
      Result := AXPolygon.TPolygon.Create(TMDIChild(FDocument).Data, APolygon, true);
    end;
  end;
end;

function TDocument.CreateMesLine: IMesLine;
var
  AMesLine: PMeasureLine;
  APoint: TDPoint;
begin
  AMesLine := new(PMeasureLine, Init(APoint, APoint));
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
  Result := AXMesLine.TMesLine.Create(TMDIChild(FDocument).Data, AMesLine, true);
{$ELSE} // MAR - commented IDB due to memory problems:
  Result := AXMesLine.TMesLine.Create(TMDIChild(FDocument).Data, AMesLine, true, TMDIChild(FDocument).Data.Layers.TopLayer.Index);
{$ENDIF}
{-- IDB_AXInterface}
end;

procedure TDocument.SaveAs(const FileName: WideString);
begin
  StrPCopy(TMDIChild(FDocument).FName, FileName);
  TMDIChild(FDocument).StoreData;
end;

function TDocument.CreateDPoint: IDPoint;
begin
     //
end;

function TDocument.Get_Bounds: IRect;
var
  ALayer: ILayer;
  ADis: IDispatch;
  ABase: IBase;
begin
  REsult := AXrect.TRect.Create(@PProj(TMDIChild(FDocument).Data)^.Size, false);
  {
  ALayer:=Get_Layers.Item[1];
  ADis:=ALayer.Item[1];
  ABase:=IBase(ADis);
  if ABase.ObjectType=1 then begin
  end;
  }
end;

procedure TDocument.Set_Bounds(const Value: IRect);
begin

end;

function TDocument.CreateFillStyle: IFillStyle;
begin
  Result := AXFillStyle.TFillStyle.Create(nil, true);
end;
{
function TDocument.CreateFontDef: IFontDef;
begin
  Result:=AXFontDef.TFontDef.Create;
end;
}

function TDocument.CreateLineStyle: ILineStyle;
begin
  REsult := AXLineStyle.TLineStyle.Create(nil, true);
end;

function TDocument.IDocument_CreateFontDef: IDispatch;
begin

end;

function TDocument.Get_Fonts: IFonts;
begin
  REsult := AXFonts.TFonts.Create(TMDIChild(FDocument).Data,
    PProj(TMDIChild(FDocument).Data)^.Pinfo^.Fonts);
end;

procedure TDocument.ReArrangeBounds;
begin
//PProj(TMDIChild(FDocument).Data)^.CalculateSize;
  PProj(TMDIChild(FDocument).Data)^.Size.Init;
  PLayer(PProj(TMDIChild(FDocument).Data)^.PInfo^.Objects)^.GetSize(PProj(TMDIChild(FDocument).Data)^.Size);
  if PProj(TMDIChild(FDocument).Data)^.Size.IsEmpty then
  begin
    PProj(TMDIChild(FDocument).Data)^.Size.A.Init(0, 0);
    PProj(TMDIChild(FDocument).Data)^.Size.B.Init(siStartX, siStartY);
  end;
  PProj(TMDIChild(FDocument).Data)^.SetModified;
end;

function TDocument.CreateCustomers: ICustomers;
begin
  Result := AXCustomers.TCustomers.Create(Self);
end;

function TDocument.CreateCustomerItem: ICustomerItem;
begin
  Result := TCustomerItem.Create;
end;

function TDocument.CreateGenerateObject: IGenerateObject;
begin
  Result := AXGenerateObject.TGenerateObject.Create(Self);
end;

function TDocument.Get_Project: Integer;
begin
  Result := Integer(TMDIChild(FDocument).Data);
end;

function TDocument.Get_IDB: IIDB;
begin
  Result := FIDB;
end;

function TDocument.CreateLine: ILine;
begin
  Result := TLine.Create(0, 0, 0, 0, true);
end;

procedure Tdocument.RedrawRect(const ARect: IRect);
var
  BRect: TDRect;
begin
  if FDocument <> nil then
  begin
    BRect.Assign(ARect.A.x, ARect.A.y, ARect.B.X, ARect.B.y);
    PProj(TMDIChild(FDocument).Data)^.Pinfo^.RedrawRect(BRect);
  end;
end;

procedure Tdocument.ZoomByRect(const Rect: IRect; Frame: Integer);
var
  ARect: TRotRect;
begin
  if FDocument <> nil then
  begin
    if (rect.Width > 0) or (Rect.height > 0) then
    begin
      ARect.x := Rect.A.x;
      ARect.y := Rect.A.y;
      ARect.width := abs(Rect.B.x - Rect.A.X);
      Arect.height := abs(Rect.B.y - Rect.A.y);
      ARect.Rotation := -PProj(TMDIChild(FDocument).Data)^.PInfo^.ViewRotation;
      PProj(TMDIChild(FDocument).Data)^.ZoomByRect(ARect, Frame, TRUE); // set TRUE to default
    end;
  end;
end;

procedure TDocument.DeselectAll;
begin
  if FDocument <> nil then
  begin
    PProj(TMDIChild(FDocument).Data)^.DeselectAll(False);
  end;
end;

// function that as used to create a collection of the items that are
// currently selected

procedure TDocument.CreateSelectedList;
var
  Cnt: integer;
  AItem: PIndex;
  ALayer: PLayer;
  layercnt: integer;
  FirstLayerItem: boolean;
  numlayers: integer;

  function DoCollectObjects(Item: PIndex): Boolean;
  begin
    ToExport.AtInsert(ToExport.GetCount, Item);
    DoCollectObjects := FALSE;
  end;

begin
  ToExportCreated := TRUE;
  ToExport.Init;
   // now all layers will be iterated to insert the objects sorted
   // by layer into the list of the selected objects
  numlayers := PProj(TMDIChild(FDocument).Data)^.Layers^.LData^.Count - 1;
  for layercnt := 1 to numlayers do
  begin
    FirstLayerItem := TRUE;
    ALayer := PLayer(PProj(TMDIChild(FDocument).Data)^.Layers^.LData^.at(layercnt));
    if ALayer = nil then
      continue;
      // search object on layer
    for Cnt := 0 to PProj(TMDIChild(FDocument).Data)^.Layers^.SelLayer^.Data^.GetCount - 1 do
    begin
      AItem := PProj(TMDIChild(FDocument).Data)^.Layers^.SelLayer^.Data^.At(Cnt);
      if (ALayer^.HasObject(AItem)) <> nil then
      begin
             // Layer has this item
        if (FirstLayerItem) then // if it�s the first item on this layer insert the layer itself
        begin
          ToExport.AtInsert(ToExport.GetCount, ALayer);
          FirstLayerItem := false;
        end;
        if DoCollectObjects(PProj(TMDIChild(FDocument).Data)^.Layers^.SelLayer^.Data^.At(Cnt)) then
          Break;
      end;
    end;
  end;
end;

// This function is used to remove the selected list after
// the export is finished

procedure TDocument.DestroySelectedList;
begin
  ToExport.DeleteAll;
  ToExport.Done;
  ToExportCreated := FALSE;
end;

function TDocument.Get_NrOfSelectedObjects: Integer;
begin
  if not ToExportCreated then
  begin
    result := PProj(TMDIChild(FDocument).Data)^.Layers^.SelLayer^.Data^.GetCount;
  end
  else
    result := ToExport.GetCount;
end;

// function is used to get item of selected objects

function TDocument.Get_SelItems(Index: Integer): IBase; safecall;
var
  AIndex: PIndex;
  AView: PView;
  Item: PIndex;
  ALayer: PLayer;
  Layername: string;
begin
  Result := nil;
  AIndex := nil;
  Item := ToExport.At(Index);
  if Item.GetObjType = ot_Layer then
  begin
    ALayer := PLayer(PProj(TMDIChild(FDocument).Data)^.Layers^.LData^.at(1));
    Layername := ALayer^.Text^;
    Result := AXLayer.TLayer.create(Item, PProj(TMDIChild(FDocument).Data));
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
{$ELSE} // MAR - commented IDB due to memory problems:
    SELF.iLastLayerWithSelectedItems := PLayer(Item).Index;
{$ENDIF}
{-- IDB_AXInterface}
  end
  else
  begin
    AIndex := PProj(TMDIChild(FDocument).Data)^.Layers^.IndexObject(PProj(TMDIChild(FDocument).Data)^.PInfo, Item);
  end;

  if Aindex <> nil then
  begin
    AView := PView(AIndex.Mptr);
    AView := PView(PLayer(PProj(TMDIChild(FDocument).Data)^.PInfo^.Objects)^.IndexObject(PProj(TMDIChild(FDocument).Data)^.PInfo, AIndex));

      // copy objectstyle
      {
      if AIndex^.ObjectStyle = nil then
      begin
         AView^.ObjectStyle:=nil;
      end
      else
      begin
         AIndex^.CopyObjectStyleTo(AView);
      end;
      }

    if AIndex.GetObjType = ot_CPoly then
    begin
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
      Result := AXPolyGon.TPolygon.Create(PProj(TMDIChild(FDocument).Data), AView, false);
{$ELSE} // MAR - commented IDB due to memory problems:
      Result := AXPolyGon.TPolygon.Create(PProj(TMDIChild(FDocument).Data), AView, false, SELF.iLastLayerWithSelectedItems);
{$ENDIF}
{-- IDB_AXInterface}
    end
    else
      if AIndex.GetObjType = ot_Poly then
      begin
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
        Result := AXPolyLine.TPolyLine.Create(PProj(TMDIChild(FDocument).Data), AView, false);
{$ELSE} // MAR - commented IDB due to memory problems:
        Result := AXPolyLine.TPolyLine.Create(PProj(TMDIChild(FDocument).Data), AView, false, SELF.iLastLayerWithSelectedItems);
{$ENDIF}
{-- IDB_AXInterface}
      end
      else
        if AIndex.GetObjType = ot_Text then
        begin
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
          Result := AXText.TText.Create(PProj(TMDIChild(FDocument).Data), AView, false);
{$ELSE} // MAR - commented IDB due to memory problems:
          Result := AXText.TText.Create(PProj(TMDIChild(FDocument).Data), AView, false, SELF.iLastLayerWithSelectedItems);
{$ENDIF}
{-- IDB_AXInterface}
        end
        else
          if AIndex.GetObjType = ot_Symbol then
          begin
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
            Result := AXSymbol.TSymbol.Create(PProj(TMDIChild(FDocument).Data), AView, false);
{$ELSE} // MAR - commented IDB due to memory problems:
            Result := AXSymbol.TSymbol.Create(PProj(TMDIChild(FDocument).Data), AView, false, SELF.iLastLayerWithSelectedItems);
{$ENDIF}
{-- IDB_AXInterface}
          end
          else
            if AIndex.GetObjType = ot_Pixel then
            begin
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
              Result := AXPixel.TPixel.Create(PProj(TMDIChild(FDocument).Data), AView, false);
{$ELSE} // MAR - commented IDB due to memory problems:
              Result := AXPixel.TPixel.Create(PProj(TMDIChild(FDocument).Data), AView, false, SELF.iLastLayerWithSelectedItems);
{$ENDIF}
{-- IDB_AXInterface}
            end
            else
              if AIndex.GetObjType = ot_Circle then
              begin
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
                Result := AXCircle.TCircle.Create(PProj(TMDIChild(FDocument).Data), AView, false);
{$ELSE} // MAR - commented IDB due to memory problems:
                Result := AXCircle.TCircle.Create(PProj(TMDIChild(FDocument).Data), AView, false, SELF.iLastLayerWithSelectedItems);
{$ENDIF}
{-- IDB_AXInterface}
              end
              else
                if AIndex.GetObjType = ot_Arc then
                begin
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
                  Result := TArc.Create(PProj(TMDIChild(FDocument).Data), AView, false);
{$ELSE} // MAR - commented IDB due to memory problems:
                  Result := TArc.Create(PProj(TMDIChild(FDocument).Data), AView, false, SELF.iLastLayerWithSelectedItems);
{$ENDIF}
{-- IDB_AXInterface}
                end
                else
                  if AIndex.GetObjType = ot_MesLine then
                  begin
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
                    Result := TMesLine.Create(PProj(TMDIChild(FDocument).Data), AView, false);
{$ELSE} // MAR - commented IDB due to memory problems:
                    Result := TMesLine.Create(PProj(TMDIChild(FDocument).Data), AView, false, SELF.iLastLayerWithSelectedItems);
{$ENDIF}
{-- IDB_AXInterface}
                  end
                  else
                    if AIndex.GetObjType = ot_Image then
                    begin
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
                      Result := AXImage.TImage.Create(PProj(TMDIChild(FDocument).Data), AView, false);
{$ELSE} // MAR - commented IDB due to memory problems:
                      Result := AXImage.TImage.Create(PProj(TMDIChild(FDocument).Data), AView, false, SELF.iLastLayerWithSelectedItems);
{$ENDIF}
{-- IDB_AXInterface}
                    end
                    else
                      if AIndex.GetObjType = ot_RText then
                      begin
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
                        Result := TDBText.Create(PProj(TMDIChild(FDocument).Data), AView, false);
{$ELSE} // MAR - commented IDB due to memory problems:
                        Result := TDBText.Create(PProj(TMDIChild(FDocument).Data), AView, false, SELF.iLastLayerWithSelectedItems);
{$ENDIF}
{-- IDB_AXInterface}
                      end
                      else
                        if AIndex.GetObjType = ot_Spline then
                        begin
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
                          Result := AXSpline.TSPLine.Create(PProj(TMDIChild(FDocument).Data), AView, false);
{$ELSE} // MAR - commented IDB due to memory problems:
                          Result := AXSpline.TSPLine.Create(PProj(TMDIChild(FDocument).Data), AView, false, SELF.iLastLayerWithSelectedItems);
{$ENDIF}
{-- IDB_AXInterface}
                        end;
  end;
end;

// procedure is used to select an object by knowing its index

procedure TDocument.SelectObjectByIndex(Index: Integer); safecall;
var
  AIndex: PIndex;
begin
  AIndex := New(PIndex, Init(Index));
  try
    SelectIndexOnAllLayers(PProj(TMDIChild(FDocument).Data), AIndex);
  finally
    Dispose(AIndex, Done);
  end;
end;

// procedure is used to delete an object by knowing its index

procedure TDocument.DeleteObjectByIndex(Index: Integer); safecall;
var
  i: Integer;
  AIndex: PIndex;
  ALayer: PLayer;
begin
  try
    AIndex := New(PIndex, Init(Index));
    try
      for i := 1 to PProj(TMDIChild(FDocument).Data)^.Layers^.LData^.Count - 1 do
      begin
        ALayer := PLayer(PProj(TMDIChild(FDocument).Data)^.Layers^.LData^.at(i));
        if (ALayer <> nil) and (ALayer^.HasIndexObject(Index) <> nil) then
        begin
          ALayer^.DeSelect(PProj(TMDIChild(FDocument).Data)^.PInfo, AIndex);
          PProj(TMDIChild(FDocument).Data)^.MultiMedia^.DelMediasAtDel(Index);
          ALayer^.DeleteIndex(PProj(TMDIChild(FDocument).Data)^.Pinfo, AIndex);
        end;
      end;
      if (PProj(TMDIChild(FDocument).Data)^.Layers^.ExistsObject(AIndex) = 0) then
      begin
        if (PLayer(PProj(TMDIChild(FDocument).Data)^.PInfo^.Objects)^.HasIndexObject(Index) <> nil) then
        begin
          PObjects(PProj(TMDIChild(FDocument).Data)^.Pinfo^.Objects)^.DeleteIndex(PProj(TMDIChild(FDocument).Data)^.PInfo, AIndex);
        end;
      end;
    finally
      Dispose(AIndex, Done);
    end;
  except
    on E: Exception do
    begin
      DebugMsg('TDocument.DeleteObjectByIndex', E.Message);
    end;
  end;
end;

// procedure is used to delete a layer by its name

procedure TDocument.DeleteLayerByName(const Name: WideString); safecall;
var
  numlayers, layercnt, i: integer;
  ALayer: PLayer;
  AIndex: PIndex;
  FoundLayer: PLayer;
begin
  try
   // first get the layer
    ALayer := nil;
    FoundLayer := nil;
    numlayers := PProj(TMDIChild(FDocument).Data)^.Layers^.LData^.Count - 1;
    for layercnt := 1 to numlayers do
    begin
      ALayer := PLayer(PProj(TMDIChild(FDocument).Data)^.Layers^.LData^.at(layercnt));
      if ALayer = nil then
        continue;
      // check if the item exists on the layer and return item
      if ALayer^.Text^ = Name then
      begin
        FoundLayer := ALayer;
        break;
      end;
    end;
    if FoundLayer <> nil then
    begin
      ALayer := FoundLayer;
      // first all objects on the layer have to be deleted
      for i := 0 to ALayer^.Data^.GetCount - 1 do
      begin
        AIndex := ALayer^.Data^.at(i);
        PLayer(PProj(TMDIChild(FDocument).Data)^.Pinfo^.Objects)^.DeleteIndex(PProj(TMDIChild(FDocument).Data)^.Pinfo, AIndex);
      end;
      // now remove all objects from layer itself
      ALayer^.Data^.FreeAll;
      // due to all objects have been deleted the layer can be removed
      PProj(TMDIChild(FDocument).Data)^.Layers^.DeleteLayer(ALayer);
      PProj(TMDIChild(FDocument).Data)^.Layers^.DetermineTopLayer;
    end;
  except
    on E: Exception do
    begin
      DebugMsg('TDocument.DeleteLayerByName', E.Message);
    end;
  end;
end;

// function is used to get an object by its index

function TDocument.GetObjectByIndex(Index: Integer): IBase; safecall;
var
  numlayers, layercnt: integer;
  AIndex: PIndex;
  AView: PView;
  ALayer: PLayer;
begin
  Result := nil;
  AIndex := nil;
  try
   // first get the item
    numlayers := PProj(TMDIChild(FDocument).Data)^.Layers^.LData^.Count - 1;
    for layercnt := 1 to numlayers do
    begin
      ALayer := PLayer(PProj(TMDIChild(FDocument).Data)^.Layers^.LData^.at(layercnt));
      if ALayer = nil then
        continue;
      // check if the item exists on the layer and return item
      AIndex := ALayer^.HasIndexObject(Index);
      if (AIndex) <> nil then // object found
        break;
    end;
    if Aindex <> nil then
    begin
      AView := PView(PLayer(PProj(TMDIChild(FDocument).Data)^.PInfo^.Objects)^.IndexObject(PProj(TMDIChild(FDocument).Data)^.PInfo, AIndex));
      // copy objectstyle
      {
      if AIndex^.ObjectStyle = nil then
      begin
         AView^.ObjectStyle:=nil;
      end
      else
      begin
         AIndex^.CopyObjectStyleTo(AView);
      end;
      }
      if AIndex.GetObjType = ot_CPoly then
      begin
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
        Result := AXPolyGon.TPolygon.Create(PProj(TMDIChild(FDocument).Data), AView, false);
{$ELSE} // MAR - commented IDB due to memory problems:
        Result := AXPolyGon.TPolygon.Create(PProj(TMDIChild(FDocument).Data), AView, false, SELF.iLastLayerWithSelectedItems);
{$ENDIF}
{-- IDB_AXInterface}
      end
      else
        if AIndex.GetObjType = ot_Poly then
        begin
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
          Result := AXPolyLine.TPolyLine.Create(PProj(TMDIChild(FDocument).Data), AView, false);
{$ELSE} // MAR - commented IDB due to memory problems:
          Result := AXPolyLine.TPolyLine.Create(PProj(TMDIChild(FDocument).Data), AView, false, SELF.iLastLayerWithSelectedItems);
{$ENDIF}
{-- IDB_AXInterface}
        end
        else
          if AIndex.GetObjType = ot_Text then
          begin
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
            Result := AXText.TText.Create(PProj(TMDIChild(FDocument).Data), AView, false);
{$ELSE} // MAR - commented IDB due to memory problems:
            Result := AXText.TText.Create(PProj(TMDIChild(FDocument).Data), AView, false, SELF.iLastLayerWithSelectedItems);
{$ENDIF}
{-- IDB_AXInterface}
          end
          else
            if AIndex.GetObjType = ot_Symbol then
            begin
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
              Result := AXSymbol.TSymbol.Create(PProj(TMDIChild(FDocument).Data), AView, false);
{$ELSE} // MAR - commented IDB due to memory problems:
              Result := AXSymbol.TSymbol.Create(PProj(TMDIChild(FDocument).Data), AView, false, SELF.iLastLayerWithSelectedItems);
{$ENDIF}
{-- IDB_AXInterface}
            end
            else
              if AIndex.GetObjType = ot_Pixel then
              begin
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
                Result := AXPixel.TPixel.Create(PProj(TMDIChild(FDocument).Data), AView, false);
{$ELSE} // MAR - commented IDB due to memory problems:
                Result := AXPixel.TPixel.Create(PProj(TMDIChild(FDocument).Data), AView, false, SELF.iLastLayerWithSelectedItems);
{$ENDIF}
{-- IDB_AXInterface}
              end
              else
                if AIndex.GetObjType = ot_Circle then
                begin
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
                  Result := AXCircle.TCircle.Create(PProj(TMDIChild(FDocument).Data), AView, false);
{$ELSE} // MAR - commented IDB due to memory problems:
                  Result := AXCircle.TCircle.Create(PProj(TMDIChild(FDocument).Data), AView, false, SELF.iLastLayerWithSelectedItems);
{$ENDIF}
{-- IDB_AXInterface}
                end
                else
                  if AIndex.GetObjType = ot_Arc then
                  begin
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
                    Result := TArc.Create(PProj(TMDIChild(FDocument).Data), AView, false);
{$ELSE} // MAR - commented IDB due to memory problems:
                    Result := TArc.Create(PProj(TMDIChild(FDocument).Data), AView, false, SELF.iLastLayerWithSelectedItems);
{$ENDIF}
{-- IDB_AXInterface}
                  end
                  else
                    if AIndex.GetObjType = ot_MesLine then
                    begin
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
                      Result := TMesLine.Create(PProj(TMDIChild(FDocument).Data), AView, false);
{$ELSE} // MAR - commented IDB due to memory problems:
                      Result := TMesLine.Create(PProj(TMDIChild(FDocument).Data), AView, false, SELF.iLastLayerWithSelectedItems);
{$ENDIF}
{-- IDB_AXInterface}
                    end
                    else
                      if AIndex.GetObjType = ot_Image then
                      begin
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
                        Result := AXImage.TImage.Create(PProj(TMDIChild(FDocument).Data), AView, false);
{$ELSE} // MAR - commented IDB due to memory problems:
                        Result := AXImage.TImage.Create(PProj(TMDIChild(FDocument).Data), AView, false, SELF.iLastLayerWithSelectedItems);
{$ENDIF}
{-- IDB_AXInterface}
                      end
                      else
                        if AIndex.GetObjType = ot_RText then
                        begin
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
                          Result := TDBText.Create(PProj(TMDIChild(FDocument).Data), AView, false);
{$ELSE} // MAR - commented IDB due to memory problems:
                          Result := TDBText.Create(PProj(TMDIChild(FDocument).Data), AView, false, SELF.iLastLayerWithSelectedItems);
{$ENDIF}
{-- IDB_AXInterface}
                        end
                        else
                          if AIndex.GetObjType = ot_Spline then
                          begin
{++ IDB_AXInterface}
{$IFNDEF AXIDB}
                            Result := AXSpline.TSPLine.Create(PProj(TMDIChild(FDocument).Data), AView, false);
{$ELSE} // MAR - commented IDB due to memory problems:
                            Result := AXSpline.TSPLine.Create(PProj(TMDIChild(FDocument).Data), AView, false, SELF.iLastLayerWithSelectedItems);
{$ENDIF}
{-- IDB_AXInterface}
                          end;
    end;
  except
    on E: Exception do
    begin
      DebugMsg('TDocument.GetObjectByIndex', E.Message);
    end;
  end;
end;

function TDocument.Get_Projection: Integer;
begin
  if PProj(TMDIChild(FDocument).Data)^.Pinfo^.CoordinateSystem.CoordinateType = ctGeodatic then
    result := 1
  else
    if PProj(TMDIChild(FDocument).Data)^.Pinfo^.CoordinateSystem.CoordinateType = ctCarthesian then
      result := 0;
end;

procedure TDocument.Set_Projection(Value: Integer);
begin
  if Value = 0 then
    PProj(TMDIChild(FDocument).Data)^.Pinfo^.CoordinateSystem.CoordinateType := ctCarthesian;
  if Value = 1 then
    PProj(TMDIChild(FDocument).Data)^.Pinfo^.CoordinateSystem.CoordinateType := ctGeodatic;
end;

function TDocument.Get_ProjectProjection: WideString;
begin
  result := PProj(TMDIChild(FDocument).Data)^.Pinfo^.ProjectionSettings.ProjectProjection;
end;

procedure TDocument.Set_ProjectProjection(const Value: WideString);
begin
  PProj(TMDIChild(FDocument).Data)^.Pinfo^.ProjectionSettings.ProjectProjection := Value;
end;

function TDocument.Get_Projectdate: WideString;
begin
  result := PProj(TMDIChild(FDocument).Data)^.Pinfo^.ProjectionSettings.ProjectDate;
end;

procedure TDocument.Set_Projectdate(const Value: WideString);
begin
  PProj(TMDIChild(FDocument).Data)^.Pinfo^.ProjectionSettings.ProjectDate := Value;
end;

function TDocument.Get_ProjectProjSettings: WideString;
begin
  result := PProj(TMDIChild(FDocument).Data)^.Pinfo^.ProjectionSettings.ProjectProjSettings;
end;

procedure TDocument.Set_ProjectProjSettings(const Value: WideString);
begin
  PProj(TMDIChild(FDocument).Data)^.Pinfo^.ProjectionSettings.ProjectProjSettings := Value;
end;

function TDocument.Get_ProjectionXOffset: Double;
begin
  result := PProj(TMDIChild(FDocument).Data)^.Pinfo^.ProjectionSettings.ProjectionXOffset;
end;

procedure TDocument.Set_ProjectionXOffset(Value: Double);
begin
  PProj(TMDIChild(FDocument).Data)^.Pinfo^.ProjectionSettings.ProjectionXOffset := Value;
end;

function TDocument.Get_ProjectionYOffset: Double;
begin
  result := PProj(TMDIChild(FDocument).Data)^.Pinfo^.ProjectionSettings.ProjectionYOffset;
end;

procedure TDocument.Set_ProjectionYOffset(Value: Double);
begin
  PProj(TMDIChild(FDocument).Data)^.Pinfo^.ProjectionSettings.ProjectionYOffset := Value;
end;

function TDocument.Get_ProjectionScale: Double;
begin
  result := PProj(TMDIChild(FDocument).Data)^.Pinfo^.ProjectionSettings.ProjectionScale;
end;

procedure TDocument.Set_ProjectionScale(Value: Double);
begin
  PProj(TMDIChild(FDocument).Data)^.Pinfo^.ProjectionSettings.ProjectionScale := Value;
end;

// procedure is used to make a selection by rectangle on the current toplayer
// only the object that are within the rect will be selected

procedure TDocument.SelectByRect(const Rect: IRect); safecall;
var
  ARect: TDRect;
begin
  ARect.Init;
  ARect.A.X := Rect.A.X;
  ARect.A.Y := Rect.A.Y;
  ARect.B.X := Rect.B.X;
  ARect.B.Y := Rect.B.Y;
  PProj(TMDIChild(FDocument).Data)^.Layers^.SelectByRectFastWithoutDlg(PProj(TMDIChild(FDocument).Data)^.Pinfo, ARect, TRUE); // select only object inside rect
end;

// procedure is used to make a selection by rectangle on the current toplayer

procedure TDocument.SelectByRectTouch(const Rect: IRect); safecall;
var
  ARect: TDRect;
begin
  ARect.Init;
  ARect.A.X := Rect.A.X;
  ARect.A.Y := Rect.A.Y;
  ARect.B.X := Rect.B.X;
  ARect.B.Y := Rect.B.Y;
  PProj(TMDIChild(FDocument).Data)^.Layers^.SelectByRectFastWithoutDlg(PProj(TMDIChild(FDocument).Data)^.Pinfo, ARect, FALSE); // select also the objects where a part is inside the rect
end;

procedure TDocument.FinishImpExpRoutine(ExitMode: WordBool; const ModuleType: WideString); safecall;
begin
  PProj(TMDIChild(FDocument).Data)^.Layers^.FinishImpExpRoutine(ExitMode, ModuleType);
end;

procedure TDocument.FinishModule(DllIdx: Integer); safecall;
begin
  PProj(TMDIChild(FDocument).Data)^.Layers^.FinishModule(DllIdx);
end;

procedure TDocument.DoAmpDkmImport(const StartDir: WideString; const AmpFileName: WideString;
  const DkmDBDir: WideString; DestDBMode: Integer;
  const DestDatabase: WideString; const DestTable: WideString;
  const LngCode: WideString); safecall;
begin
  PProj(TMDIChild(FDocument).Data)^.Layers^.DoAmpDkmImport(Startdir, AmpFilename,
    DkmDBDir, DestDBMode,
    DestDatabase, DestTable,
    LngCode);
end;

// method is used to export all user-defines line and fillstyles to a .wlf file

procedure TDocument.ExportUserDefStylesToWlf(const Filename: WideString);
begin
  PProj(TMDIChild(FDocument).Data)^.Pinfo^.ExportUserDefStylesToWlf(Filename);
end;

// method is used to import all user-defines line and fillstyles to a .wlf file

procedure TDocument.ImportUserDefStylesFromWlf(const Filename: WideString);
begin
  PProj(TMDIChild(FDocument).Data)^.Pinfo^.ImportUserDefStylesFromWlf(Filename);
end;

procedure TDocument.ImportSymbolLib(const Filename: WideString);
begin
  PProj(TMDIChild(FDocument).Data)^.ImportSymbolLib(Filename);
end;

// method to get the number of a user defined FillStyle

function TDocument.GetUserFillStyleNumberByName(const StyleName: WideString): Integer;
begin
  result := PProj(TMDIChild(FDocument).Data)^.Pinfo^.GetUserFillStyleNumberByName(StyleName);
end;

// method to get the name of a user defined FillStyle

function TDocument.GetUserFillStyleNameByNumber(StyleNumber: Integer): WideString;
begin
  result := PProj(TMDIChild(FDocument).Data)^.Pinfo^.GetUserFillStyleNameByNumber(StyleNumber);
end;

// method to get the number of a user defined LineStyle

function TDocument.GetUserLineStyleNumberByName(const StyleName: WideString): Integer;
begin
  result := PProj(TMDIChild(FDocument).Data)^.Pinfo^.GetUserLineStyleNumberByName(StyleName);
end;

// method to get the name of a user defined LineStyle

function TDocument.GetUserLineStyleNameByNumber(StyleNumber: Integer): WideString;
begin
  result := PProj(TMDIChild(FDocument).Data)^.Pinfo^.GetUserLineStyleNameByNumber(StyleNumber);
end;

// method to create new sight

function TDocument.CreateSight(const Name: WideString; X: Double; Y: Double; Height: Double;
  Width: Double; Rotation: Double): ISight;
var
  ASight: PSight;
  ARotRect: TRotRect;
begin
  Result := nil;
  ARotRect.X := X;
  ARotRect.Y := Y;
  ARotRect.Width := Width;
  ARotRect.Height := Height;
  ARotRect.Rotation := Rotation;
  ASight := new(PSight, init(Name, ARotRect));
  Result := AXSight.TAXSight.Create(TMDIChild(FDocument).Data, ASight, true);
end;

function TDocument.Get_Sights: ISights;
begin
  result := AXSights.TSights.Create(PProj(TMDIChild(FDocument).Data), PProj(TMDIChild(FDocument).Data)^.Sights);
end;

function TDocument.Get_StartProjectGUID: WideString;
var
  sGUID: AnsiString;
  Value: TGUID;
begin
  PProj(TMDIChild(FDocument).Data).Registry.OpenKey(cs_ProjectRegistry_IDB_KeyName, TRUE);
  sGUID := PProj(TMDIChild(FDocument).Data).Registry.ReadString(cs_ProjectRegistry_IDB_StartGUID_ParameterName);
   // if there still exists no GUID a new one has to be created
  if sGUID = '' then
  begin
    CoCreateGUID(Value);
    sGUID := GuidToString(Value);
    PProj(TMDIChild(FDocument).Data).Registry.WriteString(cs_ProjectRegistry_IDB_StartGUID_ParameterName, sGUID);
    PProj(TMDIChild(FDocument).Data).Registry.WriteString(cs_ProjectRegistry_IDB_GUID_ParameterName, sGUID);
  end;
  result := sGUID;
end;

function TDocument.Get_CurrentProjectGUID: WideString;
var
  sGUID: AnsiString;
  Value: TGUID;
begin
  PProj(TMDIChild(FDocument).Data).Registry.OpenKey(cs_ProjectRegistry_IDB_KeyName, TRUE);
  sGUID := PProj(TMDIChild(FDocument).Data).Registry.ReadString(cs_ProjectRegistry_IDB_GUID_ParameterName);
   // if there still exists no GUID a new one has to be created
  if sGUID = '' then
  begin
    CoCreateGUID(Value);
    sGUID := GuidToString(Value);
    PProj(TMDIChild(FDocument).Data).Registry.WriteString(cs_ProjectRegistry_IDB_StartGUID_ParameterName, sGUID);
    PProj(TMDIChild(FDocument).Data).Registry.WriteString(cs_ProjectRegistry_IDB_GUID_ParameterName, sGUID);
  end;
  result := sGUID;
end;

// procedure is used to select the texts that match with aText on active layer

procedure TDocument.SelectSearchText(const aText: WideString);
var
  TxtItem: PText;
  Selected: PCollection;
  i: integer;
  Item: PIndex;

  function CollectObjects(AItem: PIndex): Boolean; far;
  var
    bSel: Boolean;
    Idx: PIndex;
    Layer: PLayer;
    i: Integer;
  begin
    CollectObjects := False;
    bSel := False;
    TxtItem := Pointer(TMDIChild(FDocument).Data.Layers.IndexObject(PProj(TMDIChild(FDocument).Data)^.PInfo, AItem));
    if TxtItem.TemplateMatched(aText, True, True, True) then
      bSel := True;
    if bSel then
      Selected.Insert(TxtItem);
  end;

begin
  Selected := New(PCollection, Init(16, 16));
  TMDIChild(FDocument).Data.Layers.TopLayer.FirstObjectType(ot_Text, @CollectObjects);
  if TMDIChild(FDocument).Data.Layers.SelLayer.Data.GetCount > 0 then
    DeSelectAll;
   // mark the found items as selected
  for i := 0 to Selected.Count - 1 do
  begin
    Item := Selected.At(i);
    Item^.SetState(sf_Selected, TRUE);
    TMDIChild(FDocument).Data.Layers.SelLayer^.InsertObjectFast(PProj(TMDIChild(FDocument).Data)^.Pinfo, Item);
    PProj(TMDIChild(FDocument).Data)^.PInfo^.SelectionRect.CorrectByRect(Item^.ClipRect);
  end;
  Selected.DeleteAll;
  Dispose(Selected);
end;

function TDocument.ConvToPoly(Item: OleVariant): IPolyline;
var
  numlayers, layercnt: integer;
  AIndex: PIndex;
  AView: PView;
  ALayer: PLayer;
  OriArc: PEllipseArc;
  APoly: IPolyline;
  APoint: IPoint;
  Pnt: TDPoint;
  iVtx, nVtx: Integer;
  iSeg, nSeg: Integer;
  dVtx: Double;
begin
   // function is used to convert a specified Arc to a Polyline
   // and return a reference to this polyline
  result := nil;
   // first get the item
  numlayers := PProj(TMDIChild(FDocument).Data)^.Layers^.LData^.Count - 1;
  for layercnt := 1 to numlayers do
  begin
    ALayer := PLayer(PProj(TMDIChild(FDocument).Data)^.Layers^.LData^.at(layercnt));
    if ALayer = nil then
      continue;
      // check if the item exists on the layer and return item
    AIndex := ALayer^.HasIndexObject(Item.Index);
    if (AIndex) <> nil then // object found
      break;
  end;
  if Aindex <> nil then
  begin
    AView := PView(AIndex.Mptr);
    AView := PView(PLayer(PProj(TMDIChild(FDocument).Data)^.PInfo^.Objects)^.IndexObject(PProj(TMDIChild(FDocument).Data)^.PInfo, AIndex));
    Pnt.Init(0, 0);
    if (AView.GetObjType in [ot_Arc, ot_Circle, ot_Spline]) then
    begin
         // create polyline object
      APoly := CreatePolyLine;
      if (AView.GetObjType = ot_Arc) or (AView.GetObjType = ot_Circle) then
      begin
        with PProj(TMDIChild(FDocument).Data)^.PInfo.VariousSettings do
        begin
          if bConvRelMistake then
            nVtx := PEllipse(AView).ApproximatePointsNumber(dbConvRelMistake, True)
          else
            nVtx := PEllipse(AView).ApproximatePointsNumber(dbConvAbsMistake, False);
        end;
        if nVtx >= MaxCollectionSize then
          nVtx := MaxCollectionSize - 1;
        for iVtx := 0 to nVtx - 1 do
        begin
          Pnt := PEllipse(AView).ApproximatePoint(iVtx, nVtx);
          APoint := CreatePoint;
          APoint.X := Pnt.X;
          APoint.Y := Pnt.Y;
          APoly.InsertPoint(APoint);
        end;
      end;

      if (AView.GetObjType = ot_Spline) then
      begin
           // Insert the 1st spline point
        Pnt := PDPoint(PSpline(AView).Data.At(0))^;
        APoint := CreatePoint;
        APoint.X := Pnt.X;
        APoint.Y := Pnt.Y;
        APoly.InsertPoint(APoint);
        nSeg := PSpline(AView).SegmentsNum;

           // Evaluate the number of vertices
        nVtx := 1;
        for iSeg := 0 to nSeg - 1 do
        begin
          with PProj(TMDIChild(FDocument).Data)^.PInfo.VariousSettings do
          begin
            if bConvRelMistake then
              iVtx := PSpline(AView).ApprSegmPointsNumber(iSeg, dbConvRelMistake, True)
            else
              iVtx := PSpline(AView).ApprSegmPointsNumber(iSeg, dbConvAbsMistake, False);
          end;
          nVtx := nVtx + iVtx - 1;
        end;
        if nVtx < MaxCollectionSize then
          dVtx := 1.0
        else
          dVtx := (MaxCollectionSize - 64) / nVtx;

           // Insert points
        for iSeg := 0 to nSeg - 1 do
        begin
          with PProj(TMDIChild(FDocument).Data)^.PInfo.VariousSettings do
          begin
            if bConvRelMistake then
              iVtx := PSpline(AView).ApprSegmPointsNumber(iSeg, dbConvRelMistake, True)
            else
              iVtx := PSpline(AView).ApprSegmPointsNumber(iSeg, dbConvAbsMistake, False);
          end;
          nVtx := Round(dVtx * (iVtx - 1)) + 1;
          if nVtx < 2 then
            nVtx := 2;
          for iVtx := 1 to nVtx - 1 do
          begin
            Pnt := PSpline(AView).ApprSegmPoint(iSeg, iVtx, nVtx);
            APoint := CreatePoint;
            APoint.X := Pnt.X;
            APoint.Y := Pnt.Y;
            APoly.InsertPoint(APoint);
          end;
        end;
      end;
      result := APoly;
    end;
  end;
end;

procedure TDocument.DeleteSelectedObjects;
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  try
    PProj(TMDIChild(FDocument).Data).DeleteSelected(TRUE);
    PProj(TMDIChild(FDocument).Data).DeselectAll(False);
  except
    on E: Exception do
    begin
      DebugMsg('TDocument.DeleteSelectedObjects', E.Message);
    end;
  end;
{$ENDIF}
end;

function TDocument.IsModuleRegistered(ModuleIdx: Integer): WordBool;
begin
   // method checks if a module is registered or not
  Result := False; //LicenseList[ModuleIdx];
end;

function TDocument.Get_IdbProcessMask: Integer;
begin
  result := PProj(TMDIChild(FDocument).Data)^.Layers^.GetIdbProcessMask;
end;

procedure TDocument.Set_IdbProcessMask(Value: Integer);
begin
  PProj(TMDIChild(FDocument).Data)^.Layers^.SetIdbProcessMask(Value);
end;

procedure TDocument.Close;
begin
  if FDocument <> nil then
  begin
    TMDIChild(FDocument).Data^.Modified := False;
    if TMDIChild(FDocument).Data^.IsCanvas then
    begin
{$IFNDEF AXDLL} // <----------------- AXDLL
      TMDIChild(FDocument).ResetGraphicParent;
{$ENDIF}
    end;
    TMDIChild(FDocument).Close;
    FDocument := nil;
  end;
end;

procedure TDocument.SetCanvas(ParentHandle, Mode: Integer);
begin
  if FDocument <> nil then
  begin
{$IFNDEF AXDLL} // <----------------- AXDLL
    TMDIChild(FDocument).SetGraphicParent(ParentHandle);
{$ENDIF}    
  end;
end;

procedure TDocument.ZoomByScale(Scale: Double);
begin
  if FDocument <> nil then
  begin
    PProj(TMDIChild(FDocument).Data)^.ZoomByFactor(Scale / (PProj(TMDIChild(FDocument).Data)^.PInfo^.ZoomToScale(PProj(TMDIChild(FDocument).Data)^.PInfo^.Zoom)));
  end;
end;

procedure TDocument.ZoomByFactor(Scale: Double);
begin
  if FDocument <> nil then
  begin
    PProj(TMDIChild(FDocument).Data)^.ZoomByFactor(Scale);
  end;
end;

procedure TDocument.ZoomAll;
begin
  if FDocument <> nil then
  begin
    PProj(TMDIChild(FDocument).Data)^.SetOverView;
  end;
end;

procedure TDocument.ZoomToSelection;
begin
  if FDocument <> nil then
  begin
    ProcShowAllSel(TMDIChild(FDocument).Data);
  end;
end;

function TDocument.GetAttachedText(Index: Integer): WideString;
begin
  if FDocument <> nil then
  begin
    Result := ProcGetAttText(TMDIChild(FDocument).Data, Index);
  end;
end;

function TDocument.LongLatToXY(var X, Y: Double): Integer;
begin
  if FDocument <> nil then
  begin
    ProjectionTools.PInfo := PProj(TMDIChild(FDocument).Data)^.PInfo;
    Result := ProjectionTools.LongLatToXY(X, Y);
  end;
end;

function TDocument.XYToLongLat(var X, Y: Double): Integer;
begin
  if FDocument <> nil then
  begin
    ProjectionTools.PInfo := PProj(TMDIChild(FDocument).Data)^.PInfo;
    Result := ProjectionTools.XYToLongLat(X, Y);
  end;
end;

function TDocument.BingMapsOpen(const User, Pwd: WideString; Offline: WordBool): Integer;
begin
  if FDocument <> nil then
  begin
    Result := TMDIChild(FDocument).VEOnWithLoginData(User, Pwd, Offline);
  end;
end;

function TDocument.BingMapsClose: Integer;
begin
  if FDocument <> nil then
  begin
    Result := TMDIChild(FDocument).VEOff;
  end;
end;

procedure TDocument.ZoomToPoint(X, Y, Scale: Double);
begin
  if FDocument <> nil then
  begin
    TMDIChild(FDocument).Data^.ZoomByScaleWithCenter(X * 100, Y * 100, Scale);
  end;
end;

function TDocument.ProgisIDToGUID(Index: Integer): TGUID;
begin
  if FDocument <> nil then
  begin
    Result := TMDIChild(FDocument).Data^.IndexToGUID(Index);
  end;
end;

function TDocument.GUIDToProgisID(GUID: TGUID): Integer;
begin
  if FDocument <> nil then
  begin
    Result := TMDIChild(FDocument).Data^.GUIDToIndex(GUID);
  end;
end;

initialization
  TAutoObjectFactory.Create(ComServer, TDocument, Class_Document,
    ciInternal, tmApartment);
end.

