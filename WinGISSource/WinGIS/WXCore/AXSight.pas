unit AXSight;

interface

uses  ComObj, ActiveX, WINGIS_TLB, StdVcl,AXBase;

type
  TAXSight = class(TBase, ISight)
    FSight    : Pointer;
    Data      : Pointer;
    Release   : Boolean;
  protected
    function Get_Name : WideString; safecall;
    function Get_X : Double; safecall;
    function Get_Y : Double; safecall;
    function Get_Height : Double; safecall;
    function Get_Width  : Double; safecall;
    function Get_Description: WideString; safecall;
    procedure Set_Description(const Value: WideString); safecall;
    function Get_Rotation: Double; safecall;
    function Get_SightLData(Index: Integer): ISightLData; safecall;
    function Get_Count: Integer; safecall;
    function InsertSightLData(LIndex: Integer; LFlags: Integer; GeneralMin: Double; GeneralMax: Double): ISightLData; safecall;
  Public
    Constructor Create(AData,AItem:POInter;ARelease:Boolean);
    Destructor  Destroy; override;
  end;

implementation

uses ComServ, am_Sight, AXSightLData, Objects;

Constructor TAXSight.Create;
begin
   inherited Create;
   FSight:=AItem;
   BaseItem:=AItem;
   Data:=AData;
   Release:=ARelease;
   BAseItem:=AItem;
end;

Destructor TAXSight.Destroy;
begin
   if Release Then Dispose(PSight(FSight),Done);
   inherited Destroy;
end;

function TAXSight.Get_Name: WideString;
begin
   Result:='';
   if FSight <> nil then
      if PSight(FSight)^.Name <> NIL then
         Result:=PSight(FSight)^.Name^;
end;

function TAXSight.Get_X: double;
begin
   if FSight <> nil then
      result:=PSight(FSight)^.ViewRect.X;
end;

function TAXSight.Get_Y: double;
begin
   if FSight <> nil then
      result:=PSight(FSight)^.ViewRect.Y;
end;

function TAXSight.Get_Height : Double;
begin
   if FSight <> nil then
      result:=PSight(FSight)^.ViewRect.Height;
end;

function TAXSight.Get_Width  : Double;
begin
   if FSight <> nil then
      result:=PSight(FSight)^.ViewRect.Width;
end;

function TAXSight.Get_Description: WideString;
begin
   result:='';
   if FSight <> nil then
      result:=PSight(FSight)^.Description;
end;

procedure TAXSight.Set_Description(const Value: WideString);
begin
   if FSight <> nil then
      PSight(FSight)^.Description:=Value;
end;

function TAXSight.Get_Rotation: Double;
begin
   result:=0;
   if FSight <> nil then
      result:=PSight(FSight)^.ViewRect.Rotation;
end;

function TAXSight.Get_SightLData(Index: Integer): ISightLData;
begin
   result:=nil;
   if FSight <> NIL then
      if PSight(FSight)^.LayersData <> nil then
         result:=TAXSightLData.Create(Data,PSight(FSight)^.LayersData^.At(Index),false);
end;

function TAXSight.Get_Count: Integer;
begin
   result:=0;
   if FSight <> nil then
      if PSight(FSight)^.LayersData <> nil then
         result:=PSight(FSight)^.LayersData^.Count;
end;

function TAXSight.InsertSightLData(LIndex: Integer; LFlags: Integer; GeneralMin: Double; GeneralMax: Double): ISightLData;
var aSightLData:PSightLData;
begin
   result:=nil;
   if FSight <> nil then
   begin
      if PSight(FSight)^.LayersData = nil then
         PSight(FSight)^.LayersData:=New(PCollection,Init(colInit,colExpand));
      aSightLData:=new(PSightLData,init(LIndex, LFlags, GeneralMin, GeneralMax));
      PSight(FSight)^.LayersData^.Insert(aSightLData);
      result:=TAXSightLData.Create(Data,aSightLData,false);
   end;
end;

initialization
  TAutoObjectFactory.Create(ComServer, TAXSight, Class_Sight,
    ciInternal, tmApartment);

end.
