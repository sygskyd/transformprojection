Unit SymbolSelDlg;

Interface

Uses Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,StdCtrls,
     Validate,Spinbtn,WCtrls,Combobtn,DipGrid,Menus,SymLib,AM_Obj,AM_Font,ExtLib,
     AM_Group,AM_Paint,AM_Proj,AM_Def,MultiLng,ExtCtrls, Buttons, ActnList,
     ImgList;

Type TSymbolSelDialog = class(TWForm)
       LibraryCombo:TComboBox;
       SymbolList:TDipList;
       Label1:TWLabel;
       MlgSection:TMlgSection;
       OKBtn:TButton;
       CancelBtn:TButton;
       Bevel1:TBevel;
       LibraryPopupMenu:TPopupMenu;
       LibNewMenu:TMenuItem;
       LibOpenMenu:TMenuItem;
       LibSaveMenu:TMenuItem;
       LibAllSaveMenu:TMenuItem;
       N2:TMenuItem;
       LibCloseMenu:TMenuItem;
       N4:TMenuItem;
       OrganizeMenu:TMenuItem;
    ActionList: TActionList;
    ImageList: TImageList;
    NewAction: TAction;
    OpenAction: TAction;
    CloseAction: TAction;
    SaveAction: TAction;
       Procedure SymbolListDrawCell(Sender:TObject; Canvas:TCanvas;
         Index:Integer; Rect:TRect);
       Procedure SymbolListClick(Sender:TObject);
       Procedure LibraryComboClick(Sender:TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SymbolListDblClick(Sender: TObject);
    procedure SymbolListMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
      Private
       FLastIndex       : Integer;
       FProject         : pProj;
       FSymInfo         : TSymLibInfo;
       FSymbols         : pSymbols;
       FFonts           : pFonts;
       FExtLib          : pExtLib;
       FExternLibs      : TList;
       PInfo            : PPaint;
       Procedure   UpdateLibChange;
       Procedure   UpdateSymChange;
      Public
       Function    GetSelSym:pSGroup;
       Property    Project:PProj read FProject write FProject;
     end;

Function GetSymbol(aParent:TComponent;aProj:pProj):pSGroup;

implementation

{$R *.DFM}

uses AM_Child;

Function TSymbolSelDialog.GetSelSym:pSGroup;
begin
  if ModalResult=mrOk then GetSelSym:=FSymInfo.Sym
                      else GetSelSym:=nil;
end;

Function GetSymbol(aParent:TComponent;aProj:pProj):pSGroup;
var SymSelDlg   :TSymbolSelDialog;
    SymToReturn :pSGroup;
begin
  SymToReturn:=NIL;
  SymSelDlg:=TSymbolSelDialog.Create(aParent);
  SymSelDlg.Project:=aProj;
  try
    SymSelDlg.ShowModal;
    SymToReturn:=SymSelDlg.GetSelSym;
  finally
    SymSelDlg.Free;
  end;
  GetSymbol:=SymToReturn;
end;

Procedure TSymbolSelDialog.SymbolListDrawCell(Sender:TObject;
  Canvas:TCanvas; Index:Integer; Rect:TRect);
var aSym    :pSGroup;
begin
  if FSymInfo.SymCount<Index then Exit;
  aSym:=FSymInfo.GetSym(Index);
  Canvas.FillRect(Rect);
  Canvas.Pen.Color:=clBlack;     
  if aSym<>nil then begin
    PInfo^.ExtCanvas.Handle:=Canvas.Handle;
    aSym^.DrawSymbolInRect(PInfo,Rect);
  end;
end;

Procedure TSymbolSelDialog.SymbolListClick(Sender:TObject);
begin
  FSymInfo.SymIndex:=SymbolList.ItemIndex;
  Project^.ActualSym:=FSymInfo.Sym;
end;

Procedure TSymbolSelDialog.LibraryComboClick(Sender:TObject);
begin
  FSymInfo.LibIndex:=LibraryCombo.ItemIndex;
  UpdateLibChange;
  Project^.ActSymLib:=FSymInfo.LibName;
end;

procedure TSymbolSelDialog.FormCreate(Sender: TObject);
begin
  FExternLibs:=TList.Create;
  // setup pinfo
  PInfo:=New(PPaint,Init);
  PInfo^.ExtCanvas.Cached:=FALSE;
  Dispose(PInfo^.Fonts,Done);
end;

procedure TSymbolSelDialog.FormShow(Sender: TObject);
var RealProj       : PProj;
    SymParent      : TMDIChild;
begin
  RealProj:=Project;
  if (Project^.SymbolMode=sym_SymbolMode) then begin
    SymParent:=TMDIChild(Project^.SymEditInfo.SymParent);
    if (SymParent<>nil) then RealProj:=SymParent.Data;
  end;
  FSymbols:=PSymbols(RealProj^.PInfo^.Symbols);
  FFonts:=RealProj^.PInfo^.Fonts;
  PInfo^.Fonts:=FFonts;
  FExtLib:=nil;
  FSymInfo:=TSymLibInfo.Create(RealProj,FSymbols,MlgSection[17]);
  FSymInfo.CopyToListBox(LibraryCombo);
  if (Project^.ActSymLib='') then FSymInfo.LibIndex:=0
  else FSymInfo.LibIndex:=LibraryCombo.Items.IndexOf(Project^.ActSymLib);
  FSymInfo.SymIndex:=FSymInfo.IndexOf(Project^.ActualSym);
  SymbolList.ItemIndex:=FSymInfo.IndexOf(Project^.ActualSym);
  UpdateLibChange;
end;

Procedure TSymbolSelDialog.UpdateLibChange;
begin
  if Project=NIL then Exit;
  SymbolList.Count:=FSymInfo.SymCount;
  SymbolList.ItemIndex:=FSymInfo.SymIndex;
  LibraryCombo.ItemIndex:=FSymInfo.LibIndex;
  SymbolList.RePaint;
  UpdateSymChange;
end;

Procedure TSymbolSelDialog.UpdateSymChange;
var aSym  : pSGroup;
begin
  if Project<>NIL then Exit;
  aSym:=FSymInfo.Sym;
  if (Project^.SymbolMode<>sym_SymbolMode) then begin
    Project^.ActualSym:=aSym;
    Project^.ActSymLib:=FSymInfo.LibName
  end
  else begin
    Project^.ActualSym:=nil;
    Project^.ActSymLib:='';
  end;
end;

procedure TSymbolSelDialog.SymbolListDblClick(Sender: TObject);
begin
  ModalResult:=mrOK;
end;

procedure TSymbolSelDialog.SymbolListMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var Index          : Integer;
begin
  Index:=SymbolList.ItemAtPos(Point(X,Y),True);
  if Index<>FLastIndex then begin
    Application.CancelHint;
    FLastIndex:=Index;
    if Index<0 then SymbolList.Hint:=''
    else SymbolList.Hint:=PToStr(FSymInfo.GetSym(Index)^.Name);
  end;
end;

end.
