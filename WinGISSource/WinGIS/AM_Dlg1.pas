{****************************************************************************}
{ Unit AM_Dlg1                                                               }
{----------------------------------------------------------------------------}
{ - Dialog zur Auswahl des Linienstils                                       }
{ - Dialog zur Auswahl des Schraffurstils                                    }
{ - Layerdialogbox                                                           }
{ - Dialog zum Setzen der Stile eines neuen Layers                           }
{----------------------------------------------------------------------------}
{ Modifikationen                                                             }
{  16.02.1994 Martin Forst                                                   }
{  07.04.1994 Martin Forst   Neuer Layer Dialog                              }
{  17.08.1994 Martin Forst   TDLayer.CanClose neu                            }
{****************************************************************************}
Unit AM_Dlg1;

Interface

Uses WinTypes,WinProcs,AM_Def,Objects,SysUtils;

Type PLayerInfo = ^TLayerInfo;
     TLayerInfo = object(TOldObject)
       Name         : PString;
       Visible      : Boolean;
       Fix          : Boolean;
       DrawColor    : TColorRef;
       LineType     : Word;
       Pattern      : Word;
       PatternColor : TColorRef;
       BackGndColor : TColorRef;
       Index        : LongInt;
       General      : Boolean;
       MinGeneral   : Double;
       MaxGeneral   : Double;
       Print        : Boolean;
       Snap         : Boolean;
       FSymbol      : TSymbolFill;
       Constructor Init(AIndex:LongInt; AName : String; AVisible : Boolean; AFix : Boolean;
           ADrawColor : TColorRef; ALineType : Word; APattern : Word;  APatternColor : TColorRef;
           AMinGeneral : Double; AMaxGeneral : Double;
           ABackColor:TColorRef;APrint,ASnap:Boolean;SymFill : TSymbolFill);
       destructor Done; virtual;
     end;

     PDouble   = ^Double;

Implementation

Constructor TLayerInfo.Init
   (
   AIndex          : LongInt;
   AName           : String;
   AVisible        : Boolean;
   AFix            : Boolean;
   ADrawColor      : TColorRef;
   ALineType       : Word;
   APattern        : Word;
   APatternColor   : TColorRef;
   AMinGeneral     : Double;
   AMaxGeneral     : Double;
   ABackColor      : TColorRef;
   APrint          : Boolean;
   ASnap           : Boolean;
   SymFill         : TSymbolFill
   );
  begin
    TOldObject.Init;
    Index:=AIndex;
    Name:=NewStr(AName);
    Visible:=AVisible;
    Fix:=AFix;
    DrawColor:=ADrawColor;
    LineType:=ALineType;
    Pattern:=APattern;
    PatternColor:=APatternColor;
    General:=(AMinGeneral>1E-299) or (AMaxGeneral<1E299);
    MinGeneral:=AMinGeneral;
    MaxGeneral:=AMaxGeneral;
    BackGndColor:=ABackColor;
    Print:=APrint;
    Snap:=ASnap;
    Move(SymFill,FSymbol,sizeof(TSymbolFill));
  end;

Destructor TLayerInfo.Done;
  begin
    DisposeStr(Name);
    TOldObject.Done;
  end;

end.
