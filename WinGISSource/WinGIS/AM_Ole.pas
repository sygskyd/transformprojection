{****************************************************************************}
{ Unit AM_OLE                                                                }
{----------------------------------------------------------------------------}
{ OLE - Unterstützungsunit                                                   }
{----------------------------------------------------------------------------}
{ Modifikationen                                                             }
{****************************************************************************}
// Oct 07 2002. Now it works well for WinGIS2000 !
// changes by Brovak

unit AM_Ole;

{$D+,L+}
interface

uses Messages, Classes, WinProcs, WinTypes, WinDos, AM_Def, SysUtils, Objects, AM_Layer,
  AM_View, OLE, AM_Paint, ShellAPI, AM_Index, AM_CPoly, ResDlg, UserIntf, NumTools;

const
  id_OpenFileBox = 54;
  id_ServerBox = 101;
  id_TextFileName = 102;
  id_FileName = 103;
  id_LinkButton = 104;
  id_FromFile = 111;
  id_NewObject = 110;

  Mode_New = 1;
  Mode_FromFile = 2;

  RetModeLinkFile = 1;
  RetModeEmbedFile = 2;
  RetModeEmbedNew = 3;

var
  gBClassName,
    gServerApplication: array[0..150] of Char;

type
  POLENameBClassRec = ^TOLENameBClassRec;
  TOLENameBClassRec = record
    Name: array[0..150] of Char;
    BClass: array[0..32] of Char;
  end;

  POLEClientStream = ^TOLEClientStream;
  TOLEClientStream = class(TOldFileStream)
  public
//  TOLEClientStream = object(TBufStream)
    Project: Pointer;
    ParentWindow: TComponent;
    EachBMP: Boolean;
    EachAction: Word;
  end;

  POLEClientObject = ^TOLEClientObject;

  PClientItemRec = ^TClientItemRec;
  TClientItemRec = record
    Table: TOleClient;
    OLEObject: POLEClientObject;
  end;

  POLEStreamRec = ^TOLEStreamRec;
  TOLEStreamRec = record
    Table: TOLEStream;
    Stream: POldStream;
  end;

  TOLEClientObject = object(TView)
    OLEClientObject: TClientItemRec;
    ObjectPointer: POleObject;
    Document: Pointer;
      Name: PChar;
    BClassName,
      ServerApplication: array[0..150] of Char;

    constructor Init(Doc: Pointer; UL: TDPoint; LR: TDPoint);
    destructor Done; virtual;
{    Destructor Delete; virtual;
    Function  CanClose: Boolean;}
    procedure GetDocumentName(var AName: string);

    procedure Notification(var Code: TOle_Notification); virtual;
    function Check(OleBResult: TOleStatus): Boolean;

    constructor Load(S: TOLEClientStream);
    constructor LoadV2(S: TOLEClientStream);

    procedure Store(S: TOldStream); virtual;

    function CreateFromClipboard(AName: PChar; Protocol: PChar; RenderOpt: TOleOpt_Render;
      Format: TOLEClipFormat; Link: Boolean): Boolean;
    function CreateEmbeddedFromClipboard: Boolean;
    function CreateLinkFromClipboard: Boolean;
    function CreateEmbeddedFromFile(FileName: PChar): Boolean;
    function CreateLinkFromFile(FileName: PChar): Boolean;
    function CreateNew(BClassToCreate: PChar): Boolean;

    procedure CorrectOLESize;
    {++Brovak}
    function PrepareOLE: boolean;
    {--Brovak}

    procedure Draw(PInfo: PPaint; Clipping: Boolean); virtual;

    procedure GetObjectBClass(BClass: PChar; MaxLen: word);
    function GetType: Longint;
    function IsAssigned: boolean;

    procedure UpdateFromServer; { aktuelles Objekt vom Server holen }
    procedure OpenObject(Verb: Word); { Objekt im Server bearbeiten lassen }
    procedure CloseObject; { Server schließen }
    procedure ReleaseObject; { Objekt freigeben }
    procedure CopyToClipboard; { in die Zwischenablage kopieren }
    function GetObjType: Word; virtual;
    function SelectBetweenPolys(PInfo: PPaint; Poly1, Poly2: Pointer; const ObjType: TObjectTypes;
      Inside: Boolean): Boolean; virtual;
    function SelectByCircle(PInfo: PPaint; Middle: TDPoint; Radius: LongInt;
      const ObjType: TObjectTypes; Inside: Boolean): Boolean; virtual;
    function SelectByPoly(PInfo: PPaint; Poly: Pointer; const ObjType: TObjectTypes; Inside: Boolean)
      : Boolean; virtual;
    function SelectByPolyLine(PInfo: PPaint; Poly: Pointer; const ObjType: TObjectTypes)
      : Boolean; virtual;
    function SelectByPoint(PInfo: PPaint; Point: TDPoint; const ObjType: TObjectTypes): PIndex; virtual;
    function SearchForPoint(PInfo: PPaint; InCircle: Pointer; Exclude: LongInt; const ObjType: TObjectTypes;
      var Point: TDPoint; var SnObje: PIndex): Boolean; virtual;
    procedure GetBorder(PInfo: PPaint; APoly: PCPoly);
    function SearchForNearestPoint(PInfo: PPaint; var ClickPos: TDPoint; InCircle: Pointer;
      Exclude: Longint; const ObjType: TObjectTypes; var point: TDPoint; var SnObje: PIndex): Boolean; virtual;
  end;

  TDOLEServerDialog = class(TGrayDialog)
  private
    ServerList: PCollection;
    BResult: PChar;
    Mode: Integer;
    ReturnMode: PInteger;
    FileName: array[0..128] of Char;
  public
    constructor Init(AParent: TComponent; ABResult: PChar; OLEMode: PInteger);
    destructor Destroy; override;
    procedure SetupWindow; override;
    procedure WMCommand(var Msg: TWMCommand); message wm_First + wm_Command;
    procedure OK(var Msg: TWMCommand); message id_First + id_OK;
    procedure NewFileName(var Msg: TMessage); message id_First + id_OpenFileBox;
    function CanClose: Boolean; override;
  end;

function CanPaste: Boolean;
function CanPasteLink: Boolean;

function GetPasteProtocol: PChar;

procedure FreeUnit;

function LoadFromStream(var TheStream: TOldStream; Buffer: PChar; Size: Longint): Longint;
function SaveToStream(var TheStream: TOldStream; Buffer: PChar; Size: Longint): Longint;

procedure ProcInsertOLEObject(Project: Pointer; NewOLERect: TDRect);

implementation

uses Forms, AM_Proj, AM_Circl, AM_Main, Win32Def, Dialogs, Registry, Am_Child;

{ ----------- Allg. Fkt. ---------------------}

const
  PublicStandardProtocol: PChar = 'StdFileEditing';
  PublicStaticProtocol: PChar = 'Static';

var
  PublicClientVTBL: TOLEClientVTBL;
  PublicStreamVTBL: TOLEStreamVTBL;
  CFObjectLink,
    CFOwnerLink: Word;

function ClientCallBack(Client: POLEClient; Notification: TOLE_Notification;
  OLEObject: POLEClientObject): Integer; stdcall; export;
begin
  ClientCallBack := 1;
  PClientItemRec(Client)^.OLEObject^.Notification(Notification);
end;

function GetPasteProtocol: PChar;
var
  ABuffer: array[0..1200] of Char;
begin

  if OLEQueryCreateFromClip(PublicStandardProtocol, oleRender_Draw, 0) = ole_OK then
    GetPasteProtocol := PublicStandardProtocol
  else
    if OLEQueryCreateFromClip(PublicStaticProtocol, oleRender_Draw, 0) = ole_OK then
      GetPasteProtocol := PublicStaticProtocol
    else
      GetPasteProtocol := nil;
end;

function CanPaste: Boolean;
var
  Protocol: PChar;
begin
{++ Ivanoff BUG#518}
  Protocol := GetPasteProtocol;
  CanPaste := FALSE;
  if Protocol <> nil then
    CanPaste := StrComp(Protocol, PublicStandardProtocol) = 0;
 (* Protocol := NIL;
  RESULT := FALSE;  *)
{-- Ivanoff BUG#518}
end;

function CanPasteLink: Boolean;
begin
  CanPasteLink := OLEQueryLinkFromClip(PublicStandardProtocol, oleRender_Draw, 0) = ole_OK;
end;

function LoadFromStream(var TheStream: TOldStream; Buffer: PChar; Size: Longint): Longint;
var
  Rest, BlockSize: Longint;
begin
  LoadFromStream := Size;
  TheStream.Read(Buffer^, Size);
  if TheStream.Status <> 0 then
    LoadFromStream := 0;
end;

function SaveToStream(var TheStream: TOldStream; Buffer: PChar; Size: Longint): Longint;
var
  Rest, BlockSize: Longint;
begin
  SaveToStream := Size;
  TheStream.Write(Buffer^, Size);
  if TheStream.Status <> 0 then
    SaveToStream := 0;
end;

function OLEGetFromStream(Stream: POLEStream; Buffer: PChar; Size: Longint): Longint; stdcall; export;
begin
  OLEGetFromStream := LoadFromStream(POLEStreamRec(Stream)^.Stream^, Buffer, Size);
end;

function OLESetToStream(Stream: POLEStream; Buffer: PChar; Size: Longint): Longint; stdcall; export;
begin
  OLESetToStream := SaveToStream(POLEStreamRec(Stream)^.Stream^, Buffer, Size);
end;

{ *************** TOLEClientObject **************** }

constructor TOLEClientObject.Init(Doc: Pointer; UL: TDPoint; LR: TDPoint);
begin
  TView.Init;
  Document := Doc;
  Name := nil;
  ObjectPointer := nil;
  with OLEClientObject do
  begin
    Table.lpvtbl := @PublicClientVTBL;
    OLEObject := @self;
  end;
  ClipRect.Assign(UL.X, UL.Y, LR.X, LR.Y);
  BClassName[0] := #0;
  ServerApplication[0] := #0;
end;

destructor TOLEClientObject.Done;
begin
  CloseObject;
  Check(OLERelease(ObjectPointer));
  StrDispose(Name);
  TView.Done;
end;

constructor TOLEClientObject.Load(S: TOLEClientStream);
var
  LoadStream: TOLEStreamRec;
  AName: string;
  PName: array[0..20] of Char;
begin
  TView.Load(S);
  Str(Index, AName);
  StrPCopy(PName, AName);
  LoadStream.Table.lpstbl := @PublicStreamVTBL;
  LoadStream.Stream := @S;
  Document := S.Project;
  with OLEClientObject do
  begin
    Table.lpvtbl := @PublicClientVTBL;
    OLEObject := @self;
  end;
  Name := StrNew(PName);
  BClassName[0] := #0;
  ServerApplication[0] := #0;
  Check(OLELoadFromStream(@LoadStream.Table, PublicStandardProtocol, @OLEClientObject.Table,
    PProj(Document)^.OLEHandle, Name, ObjectPointer));
end;

constructor TOLEClientObject.LoadV2(S: TOLEClientStream);
var
  LoadStream: TOLEStreamRec;
  AName: string;
  PName: array[0..20] of Char;
  Temp: PString;
begin
  TView.Load(S);
  Str(Index, AName);
  StrPCopy(PName, AName);
  LoadStream.Table.lpstbl := @PublicStreamVTBL;
  LoadStream.Stream := @S;
  Document := S.Project;
  with OLEClientObject do
  begin
    Table.lpvtbl := @PublicClientVTBL;
    OLEObject := @self;
  end;
  Name := StrNew(PName);
  Temp := S.ReadStr;
  if (Temp <> nil) then
  begin
    StrPCopy(@BClassName[0], Temp^);
    DisposeStr(Temp);
  end
  else
    BClassName[0] := #0;

  Temp := S.ReadStr;
  if (Temp <> nil) then
  begin
    StrPCopy(@ServerApplication[0], Temp^);
    DisposeStr(Temp);
  end
  else
    ServerApplication[0] := #0;

  Check(OLELoadFromStream(@LoadStream.Table, PublicStandardProtocol, @OLEClientObject.Table,
    PProj(Document)^.OLEHandle, Name, ObjectPointer));
end;

procedure PToStr(Src: PChar; var Dest: string);
var
  i: Integer;
begin
  i := 0;
  while Src[i] <> #0 do
  begin
    Dest[i + 1] := Src[i];
    i := i + 1;
  end;
  Dest[0] := Char(i);
end;

procedure TOLEClientObject.Store(S: TOldStream);
var
  SaveStream: TOLEStreamRec;
  Temp: PString;
begin
  TView.Store(S);
  SaveStream.Table.lpstbl := @PublicStreamVTBL;
  SaveStream.Stream := @S;
  Temp := NewStr(StrPas(BClassName));
  S.WriteStr(Temp);
  AssignStr(Temp, StrPas(ServerApplication));
  S.WriteStr(Temp);
  DisposeStr(Temp);
  Check(OLESaveToStream(ObjectPointer, @SaveStream.Table));
end;

procedure TOLEClientObject.Notification(var Code: TOle_Notification);
begin
{**************}
end;
{+++Brovak}

function TOLEClientObject.PrepareOLE: boolean;

var
  ABuffer: array[0..1200] of Char;
begin;
  Result := true;
  if PProj(Document)^.OLEHandle = 0 then
  begin;
    Pproj(Document)^.OleStatus := OleRegisterClientDoc(ABuffer, TMDIChild(WingisMainForm.ActualChild).Fname, 0, PProj(Document)^.OleHandle);
    if (PProj(Document)^.OleHandle = 0) or ((Pproj(Document)^.OleStatus <> OLE_OK) and (Pproj(Document)^.OleStatus <> OLE_ERROR_ALREADY_REGISTERED)) then
    begin;
      MsgBox(WinGISMainForm.Handle, 11102, Mb_IconExclamation or Mb_OK, '');
      Result := false;
      Exit;
    end
    else
      Pproj(Document)^.OleStatus := OLE_OK
  end;
end;
{---brovak}

procedure TOLEClientObject.GetDocumentName(var AName: string);
begin
  AName := '';
  if ObjectPointer <> nil then
  begin
    AName := StrPas(ServerApplication);
  end;
end;

procedure TOLEClientObject.CorrectOLESize;
var
  RetVal: TOLEStatus;
  OLESize: TRect;
  OLEXSize,
    OLEYSize: Integer;
  ClipXSize,
    ClipYSize: double;
  XScale,
    YScale,
    RScale: Real;
begin
  if ObjectPointer <> nil then
  begin
    RetVal := OLEQueryBounds(ObjectPointer, OLESize);
    if RetVAL = OLE_OK then
    begin
      OLEXSize := OLESize.Right;
      OLEYSize := OLESize.Bottom;
      ClipXSize := ClipRect.XSize;
      ClipYSize := ClipRect.YSize;
      XScale := abs(ClipXSize / OLEXSize);
      YScale := abs(ClipYSize / OLEYSize);
      if XScale < YScale then
        RScale := XScale
      else
        RScale := YScale;
      ClipRect.Assign(ClipRect.A.X, ClipRect.A.Y, ClipRect.A.X + LimitToLong(OLEXSize * RScale),
        ClipRect.A.Y - LimitToLong(OLEYSize * RScale));
    end;
  end;
end;

function TOLEClientObject.Check(OLEBResult: TOLEStatus): Boolean;
var
  Msg: TMsg;
begin
  try
    Check := TRUE;
    if OLEBResult = ole_wait_for_release then
      while OleQueryReleaseStatus(ObjectPointer) = ole_busy do
      begin
        if GetMessage(Msg, 0, 0, 0) then
        begin
          TranslateMessage(Msg);
          DispatchMessage(Msg);
        end;
      end
    else
      if OLEBResult <> ole_ok then
      begin
        Check := FALSE;
      end;
  except
    on Exception do
    begin
      Check := False;
    end;
  end;
end;

function TOLEClientObject.CreateFromClipboard(AName: PChar; Protocol: PChar; RenderOpt: TOleOpt_Render;
  Format: TOLEClipFormat; Link: Boolean): Boolean;
var
  RetVal: Boolean;
begin
{++brovak}
  if PrepareOLE = false then
    Exit;
{--brovak}
  CreateFromClipBoard := FALSE;
  if OpenClipBoard(WinGISMainForm.Handle) = FALSE then
    MsgBox(WinGISMainForm.Handle, 10091, MB_IconExclamation or MB_OK, '')
  else
  begin
    Name := StrNew(AName);
    if Link then
      RetVal := Check(OLECreateLinkFromClip(Protocol, @OLEClientObject.Table, PProj(Document)^.OLEHandle,
        Name, ObjectPointer, 1 { RenderOpt}, Format))
    else
      RetVal := Check(OLECreateFromClip(Protocol, @OLEClientObject.Table, PProj(Document)^.OLEHandle,
        Name, ObjectPointer, 1 {RenderOpt}, Format));
    CloseClipBoard;
    CreateFromClipBoard := RetVal;

  end;
end;

function TOLEClientObject.CreateEmbeddedFromClipboard: Boolean;
var
  Protocol: PChar;
  AName: string;
  PName: array[0..9] of Char;
begin
{++brovak}
  if PrepareOLE = false then
    Exit;
{--brovak}
  CreateEmbeddedFromClipBoard := FALSE;
  Protocol := GetPasteProtocol;
  Str(Index, AName);
  StrPCopy(PName, AName);
  if Assigned(Protocol) then
    CreateEmbeddedFromClipBoard := CreateFromClipboard(PName, PublicStandardProtocol, oleRender_Draw, 0, False);
end;

function TOLEClientObject.CreateLinkFromClipboard: Boolean;
var
  AName: string;
  PName: array[0..9] of Char;
begin
  {++brovak}
  if PrepareOLE = false then
    Exit;
{--brovak}
  Str(Index, AName);
  StrPCopy(PName, AName);
  CreateLinkFromClipBoard := CreateFromClipboard(PName, PublicStandardProtocol, oleRender_Draw, 0, True);
end;

function TOLEClientObject.CreateEmbeddedFromFile(FileName: PChar): Boolean;
var
  AName: string;
  PName: array[0..9] of Char;
  Registry: TRegistry;
  ClassName: AnsiString;
begin
  {++brovak}
  if PrepareOLE = false then
    Exit;
{--brovak}
  StrCopy(@ServerApplication[0], @gServerApplication[0]);
  StrCopy(@BClassName[0], @gBClassName[0]);
  gBClassName[0] := chr(0);
  gServerApplication[0] := chr(0);
  Str(Index, AName);
  StrPCopy(PName, AName);
  Name := StrNew(PName);

  Registry := TRegistry.Create;
  try
    Registry.RootKey := HKEY_CLASSES_ROOT;
    Registry.OpenKeyReadOnly(ExtractFileExt(FileName));
    ClassName := Registry.ReadString('');

    CreateEmbeddedFromFile := Check(OLECreateFromFile(PublicStandardProtocol,
      @OLEClientObject.Table, PChar(ClassName), PChar(ExtractShortPathName(StrPas(FileName))),
      PProj(Document)^.OLEHandle, Name, ObjectPointer, oleRender_Draw,
      0));
  finally
    Registry.Free;
  end;
end;

function TOLEClientObject.CreateLinkFromFile(FileName: PChar): Boolean;
var
  AName: string;
  PName: array[0..9] of Char;
begin
  {++brovak}
  if PrepareOLE = false then
    Exit;
{--brovak}
  StrCopy(@ServerApplication[0], @gServerApplication[0]);
  StrCopy(@BClassName[0], @gBClassName[0]);
  gBClassName[0] := chr(0);
  gServerApplication[0] := chr(0);
  Str(Index, AName);
  StrPCopy(PName, AName);
  Name := StrNew(PName);

  CreateLinkFromFile := Check(OLECreateLinkFromFile(PublicStandardProtocol, @OLEClientObject.Table, nil, PChar(ExtractShortPathName(StrPas(FileName))),
    nil, PProj(Document)^.OLEHandle, Name, ObjectPointer, oleRender_Draw,
    0));
end;

function TOLEClientObject.CreateNew(BClassToCreate: PChar): Boolean;
var
  AName: string;
  PName: array[0..20] of Char;

begin
  {++brovak}
  if PrepareOLE = false then
    Exit;
{--brovak}
  StrCopy(ServerApplication, gServerApplication);
  StrCopy(BClassName, gBClassName);
  gBClassName[0] := #0;
  gServerApplication[0] := #0;
  Str(Index, AName);
  StrPCopy(PName, AName);
  Name := StrNew(PName);
  CreateNew := Check(OLECreate(PublicStandardProtocol, @OLEClientObject.Table, BClassToCreate,
    PProj(Document)^.OLEHandle, PName, ObjectPointer, oleRender_Draw, OT_EMBEDDED));

end;

procedure TOLEClientObject.Draw(PInfo: PPaint; Clipping: Boolean);
var
  Rect: TRect;
  OLERect: TRect;
  APoint: TDPoint;
  APoints: array[0..3] of TPoint;
begin
  if Visible(PInfo) then
  begin
    PInfo^.ConvertToDisp(ClipRect.A, TRectToPoint(Rect).P1);
    PInfo^.ConvertToDisp(ClipRect.B, TRectToPoint(Rect).P2);

    if not (PInfo^.GetDrawMode(dm_SelDesel)) then
    begin
      if Rect.left < Rect.right then
      begin
        OLERect.left := Rect.left;
        OLERect.right := Rect.right;
      end
      else
      begin
        OLERect.left := Rect.right;
        OLERect.right := Rect.left;
      end;
      if Rect.top > Rect.bottom then
      begin
        OLERect.top := Rect.top;
        OLERect.bottom := Rect.bottom;
      end
      else
      begin
        OLERect.top := Rect.bottom;
        OLERect.bottom := Rect.top;
      end;
      try
        if (DblCompare(PInfo^.ViewRotation, 0) = 0) then
        begin
          OLEDraw(ObjectPointer, PInfo^.ExtCanvas.Handle, OLERect, OLERect, 0);
        end;
      except
      end;
    end;

    APoint.Init(ClipRect.A.X, ClipRect.A.Y);
    PInfo^.ConvertToDisp(APoint, APoints[0]);

    APoint.Init(ClipRect.A.X, ClipRect.B.Y);
    PInfo^.ConvertToDisp(APoint, APoints[1]);

    APoint.Init(ClipRect.B.X, ClipRect.B.Y);
    PInfo^.ConvertToDisp(APoint, APoints[2]);

    APoint.Init(ClipRect.B.X, ClipRect.A.Y);
    PInfo^.ConvertToDisp(APoint, APoints[3]);

    PInfo^.ExtCanvas.MoveTo(APoints[0].X, APoints[0].Y);
    PInfo^.ExtCanvas.LineTo(APoints[1].X, APoints[1].Y);
    PInfo^.ExtCanvas.LineTo(APoints[2].X, APoints[2].Y);
    PInfo^.ExtCanvas.LineTo(APoints[3].X, APoints[3].Y);
    PInfo^.ExtCanvas.LineTo(APoints[0].X, APoints[0].Y);

  end;
end;

procedure TOLEClientObject.GetObjectBClass(BClass: PChar; MaxLen: word);
var
  Handle: THandle;
begin
  if (OLEGetData(ObjectPointer, CFObjectLink, Handle) = ole_OK) or
    (OLEGetData(ObjectPointer, CFOwnerLink, Handle) = ole_OK) then
  begin
    StrLCopy(BClass, GlobalLock(Handle), MaxLen);
    GlobalUnlock(Handle);
  end
  else
    BClass[0] := #0;
end;

function TOLEClientObject.GetType: Longint;
var
  ObjectType: LongInt;
  BResult: TOLEStatus;
begin
  BResult := OLEQueryType(ObjectPointer, ObjectType);
  if BResult <> ole_OK then
    ObjectType := ot_Embedded + ot_Link + ot_static;
  GetType := ObjectType;
end;

function TOLEClientObject.IsAssigned: Boolean;
begin
  IsAssigned := Assigned(ObjectPointer);
end;

procedure TOLEClientObject.UpdateFromServer;
begin
  Check(OLEUpdate(ObjectPointer));
end;

procedure TOLEClientObject.OpenObject(Verb: Word);
begin
  Check(OLEActivate(ObjectPointer, Verb, True, True, 0, nil));
end;

procedure TOLEClientObject.CloseObject;
begin
  if OLEQueryOpen(ObjectPointer) = ole_OK then
    Check(OLEClose(ObjectPointer));
end;

procedure TOLEClientObject.ReleaseObject;
begin
  Check(OLERelease(ObjectPointer));
  ObjectPointer := nil;
end;

procedure TOLEClientObject.CopyToClipboard;
begin
  if OpenClipBoard(WinGISMainForm.Handle) = FALSE then
    MsgBox(WinGISMainForm.Handle, 10091, MB_IconExclamation or MB_OK, '')
  else
  begin
    EmptyClipBoard;
    Check(OLECopyToClipBoard(ObjectPointer));
    CloseClipBoard;
  end;
end;

function TOLEClientObject.SelectByPoint
  (
  PInfo: PPaint;
  Point: TDPoint;
  const ObjType: TObjectTypes
  )
  : PIndex;
begin
  if (GetObjType in ObjType) and
    ClipRect.PointInside(Point) then
    SelectByPoint := @Self
  else
    SelectByPoint := nil;
end;

function TOLEClientObject.SelectByCircle
  (
  PInfo: PPaint;
  Middle: TDPoint;
  Radius: LongInt;
  const ObjType: TObjectTypes;
  Inside: Boolean
  )
  : Boolean;
var
  DPoly: TCPoly;
begin
  if (GetObjType in ObjType) then
  begin
    DPoly.Init;
    GetBorder(PInfo, @DPoly);
    SelectByCircle := DPoly.SelectByCircle(PInfo, Middle, Radius, [ot_CPoly], Inside);
    DPoly.Done;
  end
  else
    SelectByCircle := FALSE
end;

procedure TOLEClientObject.GetBorder
  (
  PInfo: PPaint;
  APoly: PCPoly
  );
var
  DPoint: TDPoint;
begin
  DPoint.Init(0, 0);
  with ClipRect do
  begin
    DPoint := A;
    APoly^.InsertPoint(DPoint);
    DPoint.Init(A.X, B.Y);
    APoly^.InsertPoint(DPoint);
    DPoint := B;
    APoly^.InsertPoint(DPoint);
    DPoint.Init(B.X, A.Y);
    APoly^.InsertPoint(DPoint);
  end;
  APoly^.ClosePoly;
end;

function TOLEClientObject.SelectByPoly
  (
  PInfo: PPaint;
  Poly: Pointer;
  const ObjType: TObjectTypes;
  Inside: Boolean
  )
  : Boolean;
var
  DPoly: TCPoly;
begin
  SelectByPoly := FALSE;
  if (GetObjType in ObjType) then
  begin
    DPoly.Init;
    GetBorder(PInfo, @DPoly);
    SelectByPoly := DPoly.SelectByPoly(PInfo, Poly, [ot_CPoly], Inside);
    DPoly.Done;
  end;
end;

function TOLEClientObject.SelectBetweenPolys
  (
  PInfo: PPaint;
  Poly1: Pointer;
  Poly2: Pointer;
  const ObjType: TObjectTypes;
  Inside: Boolean
  )
  : Boolean;
var
  DPoly: TCPoly;
begin
  SelectBetweenPolys := FALSE;
  if (GetObjType in ObjType) then
  begin
    DPoly.Init;
    GetBorder(PInfo, @DPoly);
    SelectBetweenPolys := DPoly.SelectBetweenPolys(PInfo, Poly1, Poly2, [ot_CPoly], Inside);
    DPoly.Done;
  end;
end;

function TOLEClientObject.SelectByPolyLine
  (
  PInfo: PPaint;
  Poly: Pointer;
  const ObjType: TObjectTypes
  )
  : Boolean;
var
  DPoly: TCPoly;
begin
  SelectByPolyLine := FALSE;
  if (GetObjType in ObjType) then
  begin
    DPoly.Init;
    GetBorder(PInfo, @DPoly);
    SelectByPolyLine := DPoly.SelectByPolyLine(PInfo, Poly, [ot_CPoly]);
    DPoly.Done;
  end;
end;

function TOLEClientObject.SearchForPoint
  (
  PInfo: PPaint;
  InCircle: Pointer;
  Exclude: LongInt;
  const ObjType: TObjectTypes;
  var Point: TDPoint;
  var SnObje: PIndex
  )
  : Boolean;
var
  Position: TDPoint;
begin
  SearchForPoint := FALSE;
  if (GetObjType in ObjType)
    and (Exclude <> Index) then
  begin
    SearchForPoint := TRUE;
    Position.Init(ClipRect.A.X, ClipRect.B.Y);
    if PEllipse(InCircle)^.PointInside(Position) then
      Point := Position
    else
    begin
      Position.Init(ClipRect.B.X, ClipRect.B.Y);
      if PEllipse(InCircle)^.PointInside(Position) then
        Point := Position
      else
      begin
        Position.Init(ClipRect.B.X, ClipRect.A.Y);
        if PEllipse(InCircle)^.PointInside(Position) then
          Point := Position
        else
        begin
          Position.Init(ClipRect.A.X, ClipRect.A.Y);
          if PEllipse(InCircle)^.PointInside(Position) then
            Point := Position
          else
            SearchForPoint := FALSE;
        end;
      end;
    end;
  end;
end;

function TOLEClientObject.GetObjType
  : Word;
begin
  GetObjType := ot_OLEObj;
end;

(************* TDOLEServerDialog ***************)

constructor TDOLEServerDialog.Init(AParent: TComponent; ABResult: PChar; OLEMode: PInteger);
var
  i: Integer;
  BClass,
    Search,
    ServerName: array[0..255] of Char;
  KeyHandle: HKey;
  Length: Longint;
  NameBClassItem: POLENameBClassRec;
  Temp: string;
  Cnt: Integer;
begin
  inherited Init(AParent, 'DLGOLE');
  BResult := ABResult;
  ServerList := new(PStrCollection, Init(5, 5));
  i := 0;
  while RegEnumKey(HKEY_ClassES_ROOT, i, BClass, 150) = ERROR_SUCCESS do
  begin
    Inc(i);
    if BClass[0] <> '.' then
    begin
      StrCopy(Search, BClass);
      StrCat(Search, '\protocol\StdFileEditing\server');
      if RegOpenKey(HKEY_ClassES_ROOT, Search, KeyHandle) = ERROR_SUCCESS then
      begin
        RegCloseKey(KeyHandle);
        Length := 150;
        RegQueryValue(HKEY_ClassES_ROOT, BClass, ServerName, Length);
        if Ord(ServerName[0]) = 0 then
        begin
          Str(i, Temp);
          StrPCopy(ServerName, 'Unknown objecttype ID ' + Temp);
        end;
        NameBClassItem := new(POLENameBClassRec);
        StrCopy(NameBClassItem^.Name, ServerName);
        StrCopy(NameBClassItem^.BClass, BClass);
        ServerList^.Insert(NameBClassItem);
      end;
    end;
  end;
  for Cnt := 0 to ServerList^.Count - 1 do
    SendDlgItemMessage(Handle, 101, lb_AddString, 0, LongInt(ServerList^.At(Cnt)));
  ReturnMode := OLEMode;
  Mode := Mode_New;
  HelpContext := 2050;
end;

destructor TDOLEServerDialog.Destroy;
var
  Cnt: Integer;
begin
  for Cnt := 0 to Serverlist^.Count - 1 do
    Dispose(POLENameBClassRec(ServerList^.At(Cnt)));
  ServerList^.DeleteAll;
  Dispose(ServerList, Done);
  inherited Destroy;
end;

procedure TDOLEServerDialog.SetupWindow;
begin
  inherited SetupWindow;
  CheckRadioButton(Handle, id_NewObject, id_FromFile, id_NewObject);
end;

procedure TDOLEServerDialog.WMCommand(var Msg: TWMCommand);
var
  i: Integer;
begin
  inherited;
  case Msg.ItemID of
    id_NewObject:
      begin
        if Mode = Mode_FromFile then
        begin
          Mode := Mode_New;
          for i := 0 to 2 do
          begin
            ShowWindow(GetItemHandle(id_TextFileName + i), SW_HIDE);
            EnableWindow(GetItemHandle(id_TextFileName + i), FALSE);
          end;
          ShowWindow(GetItemHandle(id_OpenFileBox), SW_HIDE);
          EnableWindow(GetItemHandle(id_OpenFileBox), FALSE);

          EnableWindow(GetItemHandle(id_ServerBox), TRUE);
          ShowWindow(GetItemHandle(id_ServerBox), SW_SHOW);

          SetFocus(GetItemHandle(id_OK));
        end;
      end;

    id_FromFile:
      begin
        if Mode = Mode_New then
        begin
          Mode := Mode_FromFile;

          ShowWindow(GetItemHandle(id_ServerBox), SW_HIDE);
          EnableWindow(GetItemHandle(id_ServerBox), FALSE);

          for i := 0 to 2 do
          begin
            EnableWindow(GetItemHandle(id_TextFileName + i), TRUE);
            ShowWindow(GetItemHandle(id_TextFileName + i), SW_SHOW);
          end;
          EnableWindow(GetItemHandle(id_OpenFileBox), TRUE);
          ShowWindow(GetItemHandle(id_OpenFileBox), SW_SHOW);

          SetFocus(GetItemHandle(id_OK));
        end;
      end;
    id_ServerBox:
      if Msg.NotifyCode = lbn_DblClk then
        Ok(Msg);
  end;
end;

procedure TDOLEServerDialog.OK(var Msg: TWMCommand);
var
  SelectedString: array[0..150] of Char;
  NameBClassItem: POLENameBClassRec;

  function DoAll(Item: Pointer): Boolean; far;
  begin
    DoAll := strComp(Item, @SelectedString[0]) = 0;
  end;

begin
  inherited OK(TMessage(Msg));

  if Mode = Mode_New then
  begin
    SendDlgItemMessage(Handle, 101, lb_GetText, SendDlgItemMessage(Handle, 101, lb_GetCurSel, 0, 0),
      LongInt(@SelectedString));
    NameBClassItem := ServerList^.FirstThat(@DoAll);
    StrCopy(BResult, @NameBClassItem^.BClass[0]);
    StrCopy(@gBClassName[0], @NameBClassItem^.BClass[0]);
    StrCopy(@gServerApplication[0], @NameBClassItem^.Name[0]);

    ReturnMode^ := RetModeEmbedNew;
  end
  else
  begin
    GetDlgItemText(Handle, id_FileName, BResult, 150);
    if IsDlgButtonChecked(Handle, id_LinkButton) = 0 then
      ReturnMode^ := RetModeEmbedFile
    else
      ReturnMode^ := RetModeLinkFile;
  end;
end;

function TDOLEServerDialog.CanClose: Boolean;
var
  Dummy: array[0..128] of Char;
begin
  if Mode = Mode_FromFile then
    if GetDlgItemText(Handle, id_FileName, @Dummy[0], 128) = 0 then
      CanClose := FALSE
    else
      CanClose := True
  else
    CanClose := True;
end;

procedure TDOLEServerDialog.NewFileName(var Msg: TMessage);
var
  Dialog: TOpenDialog;
begin
  Dialog := TOpenDialog.Create(Self);
  Dialog.Filter := GetLangText(3548);
  Dialog.Title := GetLangText(11117);
  if Dialog.Execute then
  begin
    StrPCopy(FileName, Dialog.FileName);
    SetDlgItemText(Handle, id_FileName, FileName);
  end;
  Dialog.Free;
end;

procedure FreeUnit;
begin
  FreeProcInstance(@PublicClientVTBL.CallBack);
  FreeProcInstance(@PublicStreamVTBL.Get);
  FreeProcInstance(@PublicStreamVTBL.Put);
end;

function TOLEClientObject.SearchForNearestPoint
  (
  PInfo: PPaint;
  var ClickPos: TDPoint;
  InCircle: Pointer;
  Exclude: LongInt;
  const ObjType: TObjectTypes;
  var Point: TDPoint;
  var SnObje: PIndex
  )
  : Boolean;
var
  Position: TDPoint;
begin
  Result := FALSE;
  if (GetObjType in ObjType) and (Exclude <> Index) then
  begin

    Result := TRUE;
    Position.Init(ClipRect.A.X, ClipRect.B.Y);
    if PEllipse(InCircle)^.PointInside(Position) and (ClickPos.Dist(Point) > ClickPos.Dist(Position)) then

      Point := Position
    else
    begin
      Position.Init(ClipRect.B.X, ClipRect.B.Y);
      if PEllipse(InCircle)^.PointInside(Position) and (ClickPos.Dist(Point) > ClickPos.Dist(Position)) then

        Point := Position
      else
      begin
        Position.Init(ClipRect.A.X, ClipRect.A.Y);
        if PEllipse(InCircle)^.PointInside(Position) and (ClickPos.Dist(Point) > ClickPos.Dist(Position)) then

          Point := Position
        else
        begin
          Position.Init(ClipRect.B.X, ClipRect.A.Y);
          if PEllipse(InCircle)^.PointInside(Position) and (ClickPos.Dist(Point) > ClickPos.Dist(Position)) then

            Point := Position
          else
            Result := FALSE;
        end;
      end;
    end;
  end;
end;

procedure ProcInsertOLEObject(Project: Pointer; NewOLERect: TDRect);
var
  BClass: array[0..80] of Char;
  Mode: Integer;
  OLEObj: POLEClientObject;
  RetVal: Boolean;
  PProject: PProj;
begin
  PProject := PProj(Project);
  RetVal := FALSE;
  case PProject^.ActualMen of
    mn_InsertOLEObject:
      begin
        if ExecDialog(TDOLEServerDialog.Init(PProject^.Parent, BClass, @Mode)) = id_OK then
        begin
          OLEObj := New(POLEClientObject, Init(PProject, NewOLERect.A, NewOLERect.B));
          case Mode of
            RetModeEmbedNew: RetVal := OLEObj^.CreateNew(BClass);
            RetModeEmbedFile: RetVal := OLEObj^.CreateEmbeddedFromFile(BClass);
            RetModeLinkFile: RetVal := OLEObj^.CreateLinkFromFile(BClass);
          end;
        end;
      end;
    mn_InsLinkObj:
      begin
        OLEObj := New(POLEClientObject, Init(PProject, NewOLERect.A, NewOLERect.B));
        RetVal := OLEObj^.CreateLinkFromClipboard;
      end;
    mn_PasteOLEObj:
      begin
        OLEObj := New(POLEClientObject, Init(PProject, NewOLERect.A, NewOLERect.B));
        RetVal := OLEObj^.CreateEmbeddedFromClipboard;
      end;
  end;
  if RetVal then
  begin
    OLEObj^.UpdateFromServer;
    if not ((PProject^.ActualMen = mn_InsertOLEObject) and (Mode = RetModeEmbedNew)) then {MB060697}
      OLEObj^.CorrectOLESize;
    PProject^.InsertObject(OLEObj, false);
    PProject^.Modified := true;
  end;
end;

const
  ROLEClientObject: TStreamRec = (
    ObjType: rn_OLEObject;
    VmtLink: TypeOf(TOLEClientObject);
    Load: @TOLEClientObject.Load;
    Store: @TOLEClientObject.Store);

const
  ROLEClientObjectV2: TStreamRec = (
    ObjType: rn_OLEObjectV2;
    VmtLink: TypeOf(TOLEClientObject);
    Load: @TOLEClientObject.LoadV2;
    Store: @TOLEClientObject.Store);


begin
  @PublicClientVTBL.CallBack := MakeProcInstance(@ClientCallBack, hInstance);

  @PublicStreamVTBL.Get := MakeProcInstance(@OLEGetFromStream, hInstance);
  @PublicStreamVTBL.Put := MakeProcInstance(@OLESetToStream, hInstance);

  CFOwnerLink := RegisterClipboardFormat('OwnerLink');
  CFObjectLink := RegisterClipboardFormat('ObjectLink');

  RegisterType(ROLEClientObject);
  RegisterType(ROLEClientObjectV2);

end.

