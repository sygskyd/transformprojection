unit IDB_DataView;
{
Internal database. Ver. II.
Quick information window.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 02-11-2001
}
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls,
  AM_Proj;

type
  TIDB_DataViewForm = class(TForm)
    Panel1: TPanel;
    ScrollBox1: TScrollBox;
    LeftPanel: TPanel;
    Splitter1: TSplitter;
    procedure Splitter1Moved(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    Procedure GetAttributeData(AProject: PProj);
  end;

implementation
{$R *.DFM}

Uses StdCtrls,
     AM_Main, AM_Layer, AM_Index, AM_Def,
     IDB_Utils, IDB_Consts, IDB_CallbacksDef, IDB_Man;

Procedure TIDB_DataViewForm.GetAttributeData(AProject: PProj);
Var
   ALabel: TLabel;
   iI, iJ, iLayerIndex, iItemID: Integer;
   AList: TStringList;
   AItem: PIndex;
   ALayer: PLayer;
   sFieldName, sFieldValue: AnsiString;
   bFound: Boolean;
Begin
     AList := TStringList.Create;
     TRY
        iItemID := AProject.Layers.SelLayer.Data.At(0).Index;
        bFound := FALSE;
        For iI:=1 To AProject.Layers.LData.Count-1 Do Begin
            ALayer := AProject.Layers.LData.At(iI);
            AItem := ALayer.HasIndexObject(iItemID);
            If (AItem <> NIL) AND (AItem.GetState(sf_Selected)) Then Begin
               bFound := TRUE;
               BREAK;
               End;
            End; // For iI end
        If bFound Then Begin
           iLayerIndex := ALayer.Index;
           If WinGISMainForm.IDB_Man.X_GotoObjectRecord(AProject, iLayerIndex, iItemID) Then Begin
              AList.Text := WinGISMainForm.IDB_Man.GetTableVisibleFieldsNamesList(AProject, ALayer);
              For iI:=0 To AList.Count-1 Do Begin
                  sFieldName := AList[iI];
                  If NOT FieldIsCalculatedField(sFieldName) Then Begin
                     sFieldValue := WinGISMainForm.IDB_Man.X_GetFieldValue(AProject, iLayerIndex, sFieldName);
(*
                  Else
                     If sFieldName = cs_CalcFieldAreaName Then // '_Area';
                        sFieldValue := GetObjectProps(ALayer, iItemID, dtArea)
                     Else
                        If sFieldName = cs_CalcFieldLengthName Then // '_Length';
                           sFieldValue := GetObjectProps(ALayer, iItemID, dtLength)
                        Else
                           If sFieldName = cs_CalcFieldPerimeterName Then // '_Perimeter';
                              sFieldValue := GetObjectProps(ALayer, iItemID, dtPerimeter)
                           Else
                              If sFieldName = cs_CalcFieldXName Then // '_X';
                                 sFieldValue := GetObjectProps(ALayer, iItemID, dtX)
                              Else
                                 If sFieldName = cs_CalcFieldYName Then // '_Y';
                                    sFieldValue := GetObjectProps(ALayer, iItemID, dtY);
*)
                  // FieldName
                  ALabel := TLabel.Create(SELF);
                  ALabel.Tag := 1;
                  ALabel.Parent := SELF.LeftPanel;
                  ALabel.Alignment := taRightJustify;
                  ALabel.Left := LeftPanel.ClientWidth - ALabel.Width - 5;
                  ALabel.Caption := sFieldName;
                  ALabel.Top := (iI * (ALabel.Height + 1));
                  // FieldData
                  ALabel := TLabel.Create(SELF);
                  ALabel.Tag := 2;
                  ALabel.Parent := ScrollBox1;
                  ALabel.Caption := sFieldValue;
                  ALabel.Left := 5;
                  ALabel.Top := (iI * (ALabel.Height + 1));
                  End;
                  End; // For iI end
              End;
           End;
     FINALLY
        AList.Clear;
        AList.Free;
     END;
End;

procedure TIDB_DataViewForm.Splitter1Moved(Sender: TObject);
Var
   iI: Integer;
   ALabel: TLabel;
begin
     If Splitter1.Left < 20 Then Begin
        Splitter1.Left := 20;
        LeftPanel.Width := 20;
        End;
     For iI:=0 To SELF.ComponentCount-1 Do
         If SELF.Components[iI] IS TLabel Then Begin
            ALabel := TLabel(SELF.Components[iI]);
            If ALabel.Tag = 1 Then
               ALabel.Left := LeftPanel.ClientWidth - ALabel.Width - 5;
            End;
end;

end.
