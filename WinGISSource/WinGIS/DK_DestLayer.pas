unit DK_DestLayer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, MultiLng, Am_proj, am_layer, AM_Admin, LayerPropertyDlg, am_def;

type
  TSetDestForm = class(TForm)
    Panel1: TPanel;
    Existcheck: TRadioButton;
    Label1: TLabel;
    Newcheck: TRadioButton;
    OKButton: TButton;
    MlgSection1: TMlgSection;
    ListLayersBox: TListBox;
    Edit1: TEdit;
    CancelButton: TButton;
    procedure NewcheckClick(Sender: TObject);
    procedure ExistcheckClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ListLayersBoxClick(Sender: TObject);
    procedure ListLayersBoxMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    function GetLayerIndex(var iLayerIndex: LongInt): Boolean;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { Private declarations }
  public
    CurrentLayer: Pointer;
    CurrentProject: Pointer;
    { Public declarations }
  end;

var
  SetDestForm: TSetDestForm;

implementation

uses
  LayerDlg;

{$R *.DFM}

procedure TSetDestForm.NewcheckClick(Sender: TObject);
begin
  Existcheck.Checked := false;
  ListLayersBox.Enabled := false;
  Edit1.Enabled := true;
end;

procedure TSetDestForm.ExistcheckClick(Sender: TObject);
begin
  Newcheck.Checked := false;
  Edit1.Enabled := false;
  ListLayersBox.Enabled := true;
end;

procedure TSetDestForm.FormShow(Sender: TObject);
var
  ii, ip: integer;
  ALayer: player;
begin
  for iI := 1 to pproj(CurrentProject).Layers.LData.Count - 1 do
  begin
    ALayer := PProj(CurrentProject).Layers.LData.At(iI);
    iP := ListLayersBox.Items.Add(ALayer.Text^);
    ListLayersBox.Items.Objects[iP] := Pointer(ALayer);
  end;
     // Set the destination layer name in the combobox.
  iP := -1;
  for iI := 0 to ListLayersBox.Items.Count - 1 do
  begin
    ALayer := PLayer(ListLayersBox.Items.Objects[iI]);
    if ALayer = CurrentLayer then
    begin
      iP := iI;
      BREAK;
    end;
  end;
  if iP < 0 then
  begin
    iP := ListLayersBox.Items.Add(Player(CurrentLayer).Text^);
    ListLayersBox.Items.Objects[iP] := Pointer(CurrentLayer);
  end;
  Edit1.Text := ListLayersBox.Items[iP];
  Perform(WM_VSCROLL, SB_LINEUP, 0);
  ListLayersBox.ItemIndex := ip;
end;

procedure TSetDestForm.ListLayersBoxClick(Sender: TObject);
begin
  CurrentLayer := PProj(CurrentProject).Layers.NameToLayer(ListLayersBox.Items[ListLayersBox.ItemIndex]);
end;

procedure TSetDestForm.ListLayersBoxMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  CurrentLayer := PProj(CurrentProject).Layers.NameToLayer(ListLayersBox.Items[ListLayersBox.ItemIndex]);
end;

function TSetDestForm.GetLayerIndex(var iLayerIndex: LongInt): Boolean;
{$IFNDEF AXDLL} // <----------------- AXDLL
var
  ADialog: TLayerPropertyDialog;
  ALayer: PLayer;
{$ENDIF}
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  RESULT := FALSE;
  iLayerIndex := -1;
  ADialog := TLayerPropertyDialog.Create(Self);
  try
    ADialog.Caption := GetLangText(10154);
    ADialog.Project := PProj(CurrentProject);
    ADialog.ProjectStyles := PProj(CurrentProject).PInfo.ProjStyles;
    ADialog.LayerStyle.Name := Edit1.Text;
    if ADialog.ShowModal = mrOK then
    begin
// ++ Cadmensky
      ALayer := PProj(CurrentProject).Layers.InsertLayer('', 0, 0, 0, 0, ilTop, DefaultSymbolFill);
//           ALayer := AProject.Layers.InsertLayer('', 0, 0, 0, 0, ilSecond, DefaultSymbolFill);
// -- Cadmensky
      ALayer.SetLayerStyle(PProj(CurrentProject).PInfo, ADialog.LayerStyle);
      iLayerIndex := ALayer.Index;
      RESULT := TRUE;
    end;
  finally
    ADialog.Free;
  end;
{$ENDIF}
end;

procedure TSetDestForm.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
var
  iLayerIndex: LongInt;
  ALayer: PLayer;
  ALayerName: ansistring;
  j: integer;
begin
  if SELF.ModalResult = mrOK then
  begin
    CanClose := FALSE;
    if ExistCheck.Checked = true then
      AlayerName := ListLayersBox.Items[ListLayersBox.ItemIndex]
    else
    begin;
      for J := 0 to ListLayersBox.Items.Count - 1 do
        if Edit1.TExt = ListLayersBox.Items[j] then
        begin;
          MessageBeeP(1);
          CanClose := false;
          exit;
        end;
      AlayerName := Edit1.Text;
    end;

    ALayer := PProj(CurrentProject).Layers.NameToLayer(AlayerName);

    if ALayer <> nil then
    begin
      Currentlayer := ALayer;
      CanClose := TRUE;
    end
    else
      if GetLayerIndex(iLayerIndex) then
      begin
        ALayer := PProj(CurrentProject).Layers.IndexToLayer(iLayerIndex);
        if ALayer <> nil then
        begin
          Currentlayer := ALayer;
        end;
        CanClose := TRUE;
      end;
  end;
end;

end.

