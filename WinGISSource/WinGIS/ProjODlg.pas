Unit ProjODlg;

Interface

Uses AM_Proj,WinProcs,WinTypes,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     Spinbtn,StdCtrls,WCtrls,Tabnotbk,Validate,MultiDlg,UserIntf,
     ComCtrls, ExtCtrls, MultiLng, OleCtrls, AxGisPro_TLB;

Type TProjectOptionsDialog = Class(TMultiFormDialog)
       CancelBtn        : TButton;
       Group1           : TWGroupBox;
       Notebook         : TWTabbedNoteBook;
       OkBtn            : TButton;
       ScaleVal         : TScaleValidator;
       Group2: TWGroupBox;
       LinearBtn: TRadioButton;
       HelmertBtn: TRadioButton;
       AffineBtn: TRadioButton;
    WGroupBox2: TWGroupBox;
    WGroupBox3: TWGroupBox;
    Label4: TLabel;
    DigitizerCombo: TComboBox;
    DigitizerSetupBtn: TButton;
    BackupFileCheck: TCheckBox;
    AutoSaveCheck: TCheckBox;
    AutoSaveSpin: TSpinBtn;
    Label3: TLabel;
    AutoSaveEdit: TEdit;
    MlgSection: TMlgSection;
    Panel1: TPanel;
    GeoBtn: TButton;
    Label5: TLabel;
    Label6: TLabel;
    ProjectionLabel: TLabel;
    DateLabel: TLabel;
    ShowCoordCheck: TCheckBox;
    CoordGeodeticLabel: TLabel;
    CoordCarthesicLabel: TLabel;
    cb400Deg: TCheckBox;
       Procedure   FormHide(Sender: TObject);
       Procedure   FormShow(Sender: TObject);
       Procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure GeoBtnClick(Sender: TObject);
    procedure ShowCoordCheckClick(Sender: TObject);
      Private
       FProject         : PProj;
       // projection settings
       ProjectCoordinateSystem:integer;
       ProjectProjSettings    :string;
       ProjectProjection      :string;
       ProjectDate            :string;
       ProjectionXOffset      :double;
       ProjectionYOffset      :double;
       ProjectionScale        :double;
      Public
       Property    Project:PProj read FProject write FProject;
     end;

Implementation

Uses AM_Def,DigIntf,Measures,MenuFn,CoordinateSystem, AM_Main, VerMgr, inifiles, WinOSInfo;//,coord32pas;

{$IFNDEF WMLT}
//Rem by Brovak  - now load dynamacally
//function GETPROJECTION(DIR:PCHAR; var PROSETTINGS:PCHAR; var PROJECTION:PCHAR; var DATE:PCHAR;var XOFFSET:DOUBLE; var YOFFSET:DOUBLE; var SCALE:DOUBLE; LNGCODE:PCHAR):boolean;stdcall;external 'SetProjection.dll';
{$ENDIF}

{$R *.DFM}

Procedure TProjectOptionsDialog.FormShow(Sender: TObject);
  Const AllowedUnits    = [muMillimeters,muCentimeters,muMeters,muKilometers,
                           muInches,muFeet,muYards,muMiles];
  var Units        : TMeasureUnits;
  begin
    with FProject^ do begin
      ScaleVal.AsFloat:=PInfo^.ProjectScale;
      if PInfo^.CoordinateSystem.CoordinateType=ctCarthesian then
      begin
         ProjectCoordinateSystem:=cs_Carthesic;
         CoordCarthesicLabel.visible:=TRUE;
         CoordGeodeticLabel.visible:=FALSE;
      end
      else
      begin
         ProjectCoordinateSystem:=cs_Geodetic;
         CoordCarthesicLabel.visible:=FALSE;
         CoordGeodeticLabel.visible:=TRUE;
      end;
      case Digitize^.DigitOptions^.Transform of
        tfHelmert : HelmertBtn.Checked:=TRUE;
        tfAffine  : AffineBtn.Checked:=TRUE;
        else LinearBtn.Checked:=TRUE;
      end;
      ProjectProjection:=PInfo^.ProjectionSettings.ProjectProjection;
      ProjectionLabel.Caption:=ProjectProjection;
      ProjectDate:=PInfo^.ProjectionSettings.ProjectDate;
      DateLabel.Caption:=ProjectDate;
      ProjectProjSettings:=PInfo^.ProjectionSettings.ProjectProjSettings;
      ProjectionXOffset:=PInfo^.ProjectionSettings.ProjectionXOffset;
      ProjectionYOffset:=PInfo^.ProjectionSettings.ProjectionYOffset;
      ProjectionScale:=PInfo^.ProjectionSettings.ProjectionScale;
      ShowCoordCheck.Checked:=PInfo^.ProjectionSettings.ShowCoordInDegrees;
      cb400Deg.Visible:=CoordGeodeticLabel.Visible;
      cb400Deg.Checked:=Deg400Mode;
    end;
    Notebook.PageIndex:=0;
  end;

Procedure TProjectOptionsDialog.FormHide(Sender: TObject);
  var NewTransform : Integer;
  begin
    if ModalResult=mrOK then with Project^,Registry do begin
      OpenKey('\Project\Settings',TRUE);
      PInfo^.ProjectScale:=ScaleVal.AsFloat;
      WriteFloat('Scale',ScaleVal.AsFloat);
      if ProjectCoordinateSystem = cs_Carthesic then PInfo^.CoordinateSystem.CoordinateType:=ctCarthesian
      else PInfo^.CoordinateSystem.CoordinateType:=ctGeodatic;
      WriteInteger('CoordinateSystem',Integer(PInfo^.CoordinateSystem.CoordinateType));
      if HelmertBtn.Checked then NewTransform:=tfHelmert
      else if AffineBtn.Checked then NewTransform:=tfAffine
      else NewTransform:=tfLinear;
      with Digitize^ do if DigitOptions^.Transform<>NewTransform then begin
        NewReference;
        DigitOptions^.Transform:=NewTransform;
        if DigitMode then begin
          Digitizer.DeActivateDigitizer;
          MenuFunctions['DigitDigitizerOnOff'].Checked:=FALSE;
          DigitMode:=FALSE;
        end;
      end;
      PInfo^.ProjectionSettings.ProjectProjection:=ProjectProjection;
      PInfo^.ProjectionSettings.ProjectDate:=ProjectDate;
      PInfo^.ProjectionSettings.ProjectProjSettings:=ProjectProjSettings;
      PInfo^.ProjectionSettings.ProjectionXOffset:=ProjectionXOffset;
      PInfo^.ProjectionSettings.ProjectionYOffset:=ProjectionYOffset;
      PInfo^.ProjectionSettings.ProjectionScale:=ProjectionScale;
      PInfo^.ProjectionSettings.ShowCoordInDegrees:=ShowCoordCheck.Checked;
      Deg400Mode:=(cb400Deg.Checked) and (PInfo^.CoordinateSystem.CoordinateType = ctGeodatic);
      UserInterface.Update([uiMenus]);
    end;
  end;

Procedure TProjectOptionsDialog.FormCreate(Sender: TObject);
  begin
    RemoveFirstPage:=FALSE;
    DialogNotebook:=Notebook;
  end;

procedure TProjectOptionsDialog.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  CanClose:=CheckValidators(Self);
end;

procedure TProjectOptionsDialog.GeoBtnClick(Sender: TObject);
var AxGisProjection:TAxGisProjection;
    inifile        :TIniFile;
    inifilename    :string;
    LngCode        :string;
begin
   {$IFNDEF WMLT}
   // setup projection Ax-Control
   AxGisProjection:=TAxGisProjection.Create(nil);
   AxGisProjection.WorkingPath:=OSInfo.WingisDir;
   AxGisProjection.SourceCoordinateSystem:=ProjectCoordinateSystem;
   AxGisProjection.SourceProjSettings:=ProjectProjSettings;
   AxGisProjection.SourceProjection:=ProjectProjection;
   AxGisProjection.SourceDate:=ProjectDate;
   AxGisProjection.SourceXOffset:=ProjectionXOffset;
   AxGisProjection.SourceYOffset:=ProjectionYOffset;
   AxGisProjection.SourceScale:=ProjectionScale;
   AxGisProjection.ShowOnlySource:=TRUE;  // set to use only for projection selection
   // get language code
   inifilename:=OSInfo.WingisIniFileName;
   inifile:=TIniFile.Create(inifilename);
   LngCode:=Inifile.ReadString('Settings','Language','txt');
   inifile.Free;
   AxGisProjection.LngCode:=LngCode;
   AxGisProjection.SetupByDialog;

   // get the projection settings
   ProjectCoordinateSystem:=AxGisProjection.SourceCoordinateSystem;
   if (ProjectCoordinateSystem = cs_Carthesic) then
   begin
      CoordCarthesicLabel.visible:=TRUE;
      CoordGeodeticLabel.visible:=FALSE;
   end
   else
   begin
      CoordCarthesicLabel.visible:=FALSE;
      CoordGeodeticLabel.visible:=TRUE;
   end;
   cb400Deg.Visible:=CoordGeodeticLabel.Visible;
   ProjectProjSettings:=AxGisProjection.SourceProjSettings;
   ProjectProjection:=AxGisProjection.SourceProjection;
   ProjectionLabel.Caption:=ProjectProjection;
   ProjectDate:=AxGisProjection.SourceDate;
   DateLabel.Caption:=ProjectDate;
   ProjectionXOffset:=AxGisProjection.SourceXOffset;
   ProjectionYOffset:=AxGisProjection.SourceYOffset;
   ProjectionScale:=AxGisProjection.SourceScale;
   AxGisProjection.Destroy;
   if ProjectProjSettings='NONE' then
     ShowCoordCheck.Checked:=false;
{$ENDIF}
end;

procedure TProjectOptionsDialog.ShowCoordCheckClick(Sender: TObject);
begin
if ShowCoordCheck.Checked = true
 then
  if ProjectProjSettings='NONE'
   then GeoBtn.Click;
  if ProjectProjSettings='NONE'
   then
    ShowCoordCheck.Checked:=false;

end;

end.
