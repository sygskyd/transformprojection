{$A-,B-,C-,D+,E-,F-,G+,H-,I-,J+,K-,L+,M-,N+,O-,P-,Q-,R-,S-,T-,U-,V-,W-,X+,Y+,Z1}

{$MINSTACKSIZE $00004000}

{$MAXSTACKSIZE $00100000}

{$IMAGEBASE $00400000}

{$APPTYPE GUI}

{$A-,B-,C-,D+,E-,F-,G+,H-,I-,J+,K-,L+,M-,N+,O-,P-,Q-,R-,S-,T-,U-,V-,W-,X+,Y+,Z1}

{$MINSTACKSIZE $00004000}

{$MAXSTACKSIZE $00100000}

{$IMAGEBASE $00400000}

{$APPTYPE GUI}

unit PrintDlg;

interface

{$IFNDEF AXDLL} // <----------------- AXDLL

{$H+}

uses WinProcs, WinTypes, AM_Def, AM_Proj, AM_Paint, Classes, Controls, EditHndl, ExtCtrls,
  Forms, Graphics, Measures, MenuHndl, Spinbtn, StdCtrls, Tabnotbk, Validate, WCtrls,
  ComCtrls, MultiLng, WPrinter, GrTools, Dialogs, PageSetup, ImgList, PrintOptions,
  PrintTemplates, Scroller, Buttons, InpHndl, AM_Sight,
  ExtPage, TrLib32, WOpenPicDlg, FontStyleDlg;

type
  TPrintingDialog = class(TWForm)
    BottomVal: TMeasureValidator;
    CancelBtn: TButton;
    CloseBtn: TButton;
    CopiesImages: TImageList;
    CopiesVal: TIntValidator;
    DeleteBtn: TSpeedButton;
    EditHandler: TEditHandler;
    HeightVal: TMeasureValidator;
    ImageList: TImageList;
    InterBottomVal: TMeasureValidator;
    InterLeftVal: TMeasureValidator;
    InterRightVal: TMeasureValidator;
    InterTopVal: TMeasureValidator;
    LeftPosVal: TMeasureValidator;
    LeftVal: TMeasureValidator;
    MlgSection: TMlgSection;
    LPModelImageList: TImageList;
    StartPageVal: TIntValidator;
    OrderImageList: TImageList;
    OkBtn: TButton;
    OverlapLeftVal: TMeasureValidator;
    OverlapRightVal: TMeasureValidator;
    OverlapTopVal: TMeasureValidator;
    OverlapBottomVal: TMeasureValidator;
    PageHeightVal: TMeasureValidator;
    PagesHorzVal: TIntValidator;
    PagesVertVal: TIntValidator;
    PageWidthVal: TMeasureValidator;
    PreviewBtn: TSpeedButton;
    PreviewWindow: TWindow;
    RightVal: TMeasureValidator;
    RubberBox: TRubberBox;
    ScaleVal: TScaleValidator;
    Scroller: TScroller;
    TopPosVal: TMeasureValidator;
    TopVal: TMeasureValidator;
    WidthVal: TMeasureValidator;
    SelectBtn: TSpeedButton;
    ZoomBtn: TSpeedButton;
    ZoomAllBtn: TSpeedButton;
    ZoomInBtn: TSpeedButton;
    ZoomOutBtn: TSpeedButton;
    InsertLegendBtn: TSpeedButton;
    InsertPictureBtn: TSpeedButton;
    InsertSignatureBtn: TSpeedButton;
    BringToFrontBtn: TSpeedButton;
    BringForwardBtn: TSpeedButton;
    SendBackwardBtn: TSpeedButton;
    SendToBackBtn: TSpeedButton;
    DialogPageControl: TExtPageControl;
    DialogPage1: TTabSheet;
    Group5: TWGroupBox;
    LocationLabel: TWLabel;
    Label11: TWLabel;
    TypeLabel: TWLabel;
    Label12: TWLabel;
    StatusLabel: TWLabel;
    Label10: TWLabel;
    Label1: TWLabel;
    WLabel9: TWLabel;
    CommentsLabel: TWLabel;
    PrintToFileCheck: TCheckBox;
    PrintToWMFCheck: TCheckBox;
    PropertiesBtn: TButton;
    PrinterCombo: TComboBox;
    Group7: TWGroupBox;
    SortPaintBox: TPaintBox;
    Label2: TWLabel;
    SortCopiesCheck: TCheckBox;
    CopiesSpin: TSpinBtn;
    CopiesEdit: TEdit;
    DialogPage2: TTabSheet;
    PLModelGroup: TWGroupBox;
    ModelPaintBox: TPaintBox;
    LayoutModelCombo: TComboBox;
    Group8: TWGroupBox;
    WLabel1: TWLabel;
    WLabel2: TWLabel;
    WLabel3: TWLabel;
    PaintBox: TPaintBox;
    FormatsCombo: TComboBox;
    PortraitBtn: TRadioButton;
    LandscapeBtn: TRadioButton;
    PageWidthEdit: TEdit;
    PageHeightEdit: TEdit;
    PageWidthSpin: TSpinBtn;
    PageHeightSpin: TSpinBtn;
    Group6: TWGroupBox;
    Label4: TWLabel;
    Label6: TWLabel;
    Label3: TWLabel;
    Label5: TWLabel;
    BottomSpin: TSpinBtn;
    BottomEdit: TEdit;
    RightSpin: TSpinBtn;
    RightEdit: TEdit;
    TopSpin: TSpinBtn;
    TopEdit: TEdit;
    LeftSpin: TSpinBtn;
    LeftEdit: TEdit;
    DialogPage3: TTabSheet;
    MultyPageGroup: TWGroupBox;
    ManualRpBtn: TRadioButton;
    AutoRpBtn: TRadioButton;
    Label15: TWLabel;
    Label13: TWLabel;
    VertPagesSpin: TSpinBtn;
    VertPagesEdit: TEdit;
    HorzPagesSpin: TSpinBtn;
    HorzPagesEdit: TEdit;
    Group9: TWGroupBox;
    WLabel4: TWLabel;
    WLabel5: TWLabel;
    WLabel6: TWLabel;
    WLabel7: TWLabel;
    InterBottomSpin: TSpinBtn;
    InterBottomEdit: TEdit;
    InterRightSpin: TSpinBtn;
    InterRightEdit: TEdit;
    InterTopSpin: TSpinBtn;
    InterTopEdit: TEdit;
    InterLeftSpin: TSpinBtn;
    InterLeftEdit: TEdit;
    OverlapGroup: TWGroupBox;
    OverlapBottomLab: TWLabel;
    OverlapRightLab: TWLabel;
    OverlapTopLab: TWLabel;
    OverlapLeftLab: TWLabel;
    OverlapBottomSpin: TSpinBtn;
    OverlapBottomEdit: TEdit;
    OverlapRightSpin: TSpinBtn;
    OverlapRightEdit: TEdit;
    OverlapTopSpin: TSpinBtn;
    OverlapTopEdit: TEdit;
    OverlapLeftSpin: TSpinBtn;
    OverlapLeftEdit: TEdit;
    OrderGroupBox: TWGroupBox;
    OrderPaintBox: TPaintBox;
    StartPageLab: TWLabel;
    PagesOrderLab: TWLabel;
    HorizOrderBtn: TRadioButton;
    VertOrderBtn: TRadioButton;
    StartPageEdit: TEdit;
    StartPageSpin: TSpinBtn;
    DialogPage4: TTabSheet;
    InternalPageControl: TExtPageControl;
    InternalPage1: TTabSheet;
    Group1: TWGroupBox;
    WindowSelBtn: TButton;
    ViewsCombo: TComboBox;
    PrintWindowBtn: TRadioButton;
    PrintCurrentViewBtn: TRadioButton;
    PrintSavedViewBtn: TRadioButton;
    PrintAllBtn: TRadioButton;
    Group2: TWGroupBox;
    HeightLabel: TWLabel;
    WidthLabel: TWLabel;
    HeightSpin: TSpinBtn;
    HeightEdit: TEdit;
    WidthSpin: TSpinBtn;
    WidthEdit: TEdit;
    ScaleSpin: TSpinBtn;
    btnSetFrameInDlg: TRadioButton;
    ScaleCombo: TComboBox;
    chkKeepScale: TCheckBox;
    Group4: TWGroupBox;
    LeftPosLabel: TWLabel;
    CenterPageBtn: TRadioButton;
    LeftPosSpin: TSpinBtn;
    LeftPosEdit: TEdit;
    LeftPosBtn: TRadioButton;
    TopPosLabel: TWLabel;
    TopPosEdit: TEdit;
    TopPosSpin: TSpinBtn;
{++ Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
    MarksGrBox: TWGroupBox;
    CropMarksCheckBox: TCheckBox;
    ProjFrameCheckBox: TCheckBox;
{-- Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
    InternalPage2: TTabSheet;
    LegendGroup: TWGroupBox;
    LegendCombo: TComboBox;
    LegPosGroup: TWGroupBox;
    LegCustomBtn: TRadioButton;
    LegCenterHorBtn: TRadioButton;
    LegCenterVertBtn: TRadioButton;
    LegCenterBtn: TRadioButton;
    LegOriginLab: TWLabel;
    LegOriginCombo: TComboBox;
    LegLeftLab: TWLabel;
    LegLeftPosEdit: TEdit;
    LegLeftPosSpin: TSpinBtn;
    LegLeftPosVal: TMeasureValidator;
    LegTopLab: TWLabel;
    LegTopPosEdit: TEdit;
    LegTopPosSpin: TSpinBtn;
    LegTopPosVal: TMeasureValidator;
    LegSizeGroup: TWGroupBox;
    LegWidthLab: TWLabel;
    LegWidthEdit: TEdit;
    LegWidthSpin: TSpinBtn;
    LegWidthVal: TMeasureValidator;
    LegHightLab: TWLabel;
    LegHeightEdit: TEdit;
    LegHeightSpin: TSpinBtn;
    LegHeightVal: TMeasureValidator;
    InternalPage3: TTabSheet;
    PicturesGroup: TWGroupBox;
    PicturesCombo: TComboBox;
    PicSizeGroup: TWGroupBox;
    PicWidthLab: TWLabel;
    PicWidthEdit: TEdit;
    PicWidthSpin: TSpinBtn;
    PicWidthVal: TMeasureValidator;
    PicHeightLab: TWLabel;
    PicHeightEdit: TEdit;
    PicHeightSpin: TSpinBtn;
    PicHeightVal: TMeasureValidator;
    PicWScaleLab: TWLabel;
    PicWScaleEdit: TEdit;
    PicWScaleSpin: TSpinBtn;
    PicWScaleVal: TMeasureValidator;
    PicHScaleLab: TWLabel;
    PicHScaleEdit: TEdit;
    PicHScaleSpin: TSpinBtn;
    PicHScaleVal: TMeasureValidator;
    KeepAspectCheck: TCheckBox;
    PicLocationGroup: TWGroupBox;
    PicLocOneTimeBtn: TRadioButton;
    PicLocOnPagesBtn: TRadioButton;
    PicFirstCheck: TCheckBox;
    PicInterCheck: TCheckBox;
    PicLastCheck: TCheckBox;
    PicPosGroup: TWGroupBox;
    PicCustomBtn: TRadioButton;
    PicCenterHorBtn: TRadioButton;
    PicCenterVertBtn: TRadioButton;
    PicCenterBtn: TRadioButton;
    PicOriginLab: TWLabel;
    PicOriginCombo: TComboBox;
    PicLeftLab: TWLabel;
    PicLeftPosEdit: TEdit;
    PicLeftPosSpin: TSpinBtn;
    PicLeftPosVal: TMeasureValidator;
    PicTopLab: TWLabel;
    PicTopPosEdit: TEdit;
    PicTopPosSpin: TSpinBtn;
    PicTopPosVal: TMeasureValidator;
    InternalPage4: TTabSheet;
    SignaturesGroup: TWGroupBox;
    SignaturesCombo: TComboBox;
    FontDlgBtn: TBitBtn;
    ComFildsGroup: TWGroupBox;
    InsPageField: TSpeedButton;
    InsPagesField: TSpeedButton;
    InsDateField: TSpeedButton;
    InsTimeField: TSpeedButton;
    InsFileField: TSpeedButton;
    InsAuthorField: TSpeedButton;
    InsScaleField: TSpeedButton;
    SigLocationGroup: TWGroupBox;
    SigLocOneTimeBtn: TRadioButton;
    SigLocOnPagesBtn: TRadioButton;
    SigFirstCheck: TCheckBox;
    SigInterCheck: TCheckBox;
    SigLastCheck: TCheckBox;
    SigPosGroup: TWGroupBox;
    SigCustomBtn: TRadioButton;
    SigCenterHorBtn: TRadioButton;
    SigCenterVertBtn: TRadioButton;
    SigCenterBtn: TRadioButton;
    SigOriginLab: TWLabel;
    SigOriginCombo: TComboBox;
    SigLeftLab: TWLabel;
    SigLeftPosEdit: TEdit;
    SigLeftPosSpin: TSpinBtn;
    SigLeftPosVal: TMeasureValidator;
    SigTopLab: TWLabel;
    SigTopPosEdit: TEdit;
    SigTopPosSpin: TSpinBtn;
    SigTopPosVal: TMeasureValidator;
    TemplateGroup: TWGroupBox;
    TemplateOpenBtn: TButton;
    TemplateSaveBtn: TButton;
    OpenTemplateDialog: TOpenDialog;
    SaveTemplateDialog: TWSaveDialog;
    SaveWMFDialog: TWSaveDialog;
    ScaleLabel: TWLabel;
    chkFitToPage: TCheckBox;
    WLabel8: TWLabel;
    btnSetFrameInPrj: TRadioButton;
    lblFnt: TWLabel;
    lblSigFont: TWLabel;
    FrameCombo: TComboBox;
    WLabel10: TWLabel;
    WLabel11: TWLabel;
    btnLegendObj: TRadioButton;
    btnFrameObj: TRadioButton;
    chkLegFrmLeft: TCheckBox;
    chkLegFrmTop: TCheckBox;
    chkLegFrmRight: TCheckBox;
    chkLegFrmBottom: TCheckBox;
    InsertFrameBtn: TSpeedButton;
    FrameAddBrn: TButton;
    InsertOLEBtn: TSpeedButton;
    OLEOpenDialog: TOpenDialog;
    ButtonOle: TButton;
    LabelOLE: TLabel;
    DialogSaveToFile: TSaveDialog;
    chkPrintPageNum: TCheckBox;
    procedure TemplateOpenBtnClick(Sender: TObject);
    procedure DeleteBtnClick(Sender: TObject);
    procedure EditChanged(Sender: TObject);
    procedure EditEntered(Sender: TObject);
    procedure EditHandlerMove(Sender: TObject);
    procedure EditHandlerResize(Sender: TObject);
    procedure FormatsComboClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormHide(Sender: TObject);
    {++ Moskaliov AXWinGIS Printing Methods BUILD#127 05.09.00}
    procedure FormLoad(Sender: TObject);
    procedure FormStore(Sender: TObject);
    {-- Moskaliov AXWinGIS Printing Methods BUILD#127 05.09.00}
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FrameForPagesCheckClick(Sender: TObject);
    procedure InsertNewFrame(Sender: TObject);
    procedure LeftOrTopPosExit(Sender: TObject);
    procedure LeftOrTopPosUpdated(Sender: TObject);
    procedure LegendComboClick(Sender: TObject);
    procedure MultiPagesUpdated(Sender: TObject);
    procedure PageFramesUpdated(Sender: TObject);
    procedure PaintBoxPaint(Sender: TObject);
    procedure PortraitBtnClick(Sender: TObject);
    procedure PreviewMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure PreviewMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure PreviewMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure PreviewPaint(Sender: TObject);
    procedure PrinterComboClick(Sender: TObject);
    procedure PrintRangeOrScaleUpdated(Sender: TObject);
    procedure PropertiesBtnClick(Sender: TObject);
    procedure RubberBoxFinishedInput(Sender: TObject);
    procedure ScaleEditExit(Sender: TObject);
    procedure ScaleUpdated(Sender: TObject);
    procedure SelectBtnClick(Sender: TObject);
    procedure ShowPreviewCheckClick(Sender: TObject);
    procedure SortPaintBoxPaint(Sender: TObject);
    procedure SortCopiesCheckClick(Sender: TObject);
    procedure ViewsComboClick(Sender: TObject);
    procedure WidthOrHeightEditExit(Sender: TObject);
    procedure WidthOrHeightUpdated(Sender: TObject);
    procedure WindowSelBtnClick(Sender: TObject);
    procedure ZoomAllBtnClick(Sender: TObject);
    procedure ZoomInBtnClick(Sender: TObject);
    procedure ZoomOutBtnClick(Sender: TObject);
    procedure ZoomBtnClick(Sender: TObject);
    procedure chkKeepScaleClick(Sender: TObject);
    procedure chkFitToPageClick(Sender: TObject);
    procedure PosEditClick(Sender: TObject);
    procedure SizeEditClick(Sender: TObject);
    procedure EditKeyPress(Sender: TObject; var Key: Char);
    procedure ScaleComboChange(Sender: TObject);
    procedure ModelPaintBoxPaint(Sender: TObject);
    procedure LayoutModelComboClick(Sender: TObject);
    procedure PageRpBtnClick(Sender: TObject);
    procedure OrderPaintBoxPaint(Sender: TObject);
    procedure PagesOrderBtnClick(Sender: TObject);
    procedure InterEditClick(Sender: TObject);
    procedure MarginEditClick(Sender: TObject);
    procedure PagesEditClick(Sender: TObject);
    procedure InterSpinClick(Sender: TObject);
    procedure MarginSpinClick(Sender: TObject);
    procedure MultiPagesSpinClick(Sender: TObject);
    procedure OverlapEditClick(Sender: TObject);
    procedure OverlapSpinClick(Sender: TObject);
    procedure StartPageEditClick(Sender: TObject);
    procedure InternalPageControlDrawTab(Control: TWinControl; Index: Integer;
      ActiveTab: Boolean; const RectFg, RectBg: TRect; State: TOwnerDrawState);
    procedure DialogPageControlChange(Sender: TObject);
    procedure InternalPageControlChange(Sender: TObject);
    procedure MoreOptBtnClick(Sender: TObject);
    procedure PageSetupBtnClick(Sender: TObject);
    procedure LegSizeUpdated(Sender: TObject);
    procedure LegPositionUpdated(Sender: TObject);
    procedure LegPositionBtnClick(Sender: TObject);
    procedure LegSizeOrPositionUpdated(Sender: TObject);
    procedure LegOriginComboClick(Sender: TObject);
    procedure PicKeepAspectCheckClick(Sender: TObject);
    {++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
    procedure PicLocationBtnClick(Sender: TObject);
    procedure PicLocationCheckClick(Sender: TObject);
    {-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
    procedure PicOriginComboClick(Sender: TObject);
    procedure PicPositionBtnClick(Sender: TObject);
    procedure PicPositionUpdated(Sender: TObject);
    procedure PicSizeUpdated(Sender: TObject);
    procedure PicSizeOrPositionUpdated(Sender: TObject);
    procedure PicturesComboClick(Sender: TObject);
    procedure OrderBtnClick(Sender: TObject);
    procedure SigOriginComboClick(Sender: TObject);
    procedure SigPositionUpdated(Sender: TObject);
    procedure SigSizeOrPositionUpdated(Sender: TObject);
    procedure SigPositionBtnClick(Sender: TObject);
    procedure SignaturesComboClick(Sender: TObject);
    procedure SignatureEditExit(Sender: TObject);
    procedure SignaturesEditKeyPress(Sender: TObject; var Key: Char);
    procedure SignatureEditChange(Sender: TObject);
    procedure TemplateSaveBtnClick(Sender: TObject);
    procedure InsFieldClick(Sender: TObject);
    procedure SigLocationBtnClick(Sender: TObject);
    procedure SigLocationCheckClick(Sender: TObject);
    procedure SigFontDlgBtnClick(Sender: TObject);
    procedure StartPageUpdated(Sender: TObject);
    procedure PrintToCheckClick(Sender: TObject);
    {++ Moskaliov AXWinGIS Printing Methods BUILD#133 25.10.00}
    procedure FillDialogComboBoxes;
    procedure CropMarksCheckBoxClick(Sender: TObject);
    procedure ProjFrameCheckBoxClick(Sender: TObject);
    procedure FrameRatioBtnClick(Sender: TObject);
    procedure btnLegendObjClick(Sender: TObject);
    procedure btnFrameObjClick(Sender: TObject);
    procedure chkLegFrmSideClick(Sender: TObject);
    procedure FrameAddBrnClick(Sender: TObject);
    {-- Moskaliov AXWinGIS Printing Methods BUILD#133 25.10.00}
    {brovak}
    procedure RedrawSpecPrintFrame;
    procedure ButtonOleClick(Sender: TObject);
    procedure chkPrintPageNumClick(Sender: TObject);
    {brovak}
  private
    // pointer to the current printing-rectangle
    FCurrentItem: PGrRect;
    FGraphics: TGraphics;
    FSignatureIndex: Integer;
    FKeyPressed: Boolean;
    FPageIndex: Integer;
    FSubPageIndex: Integer;
    FSelectAll: Boolean; // TRUE if contents of edit-box must be selected
    // active menu-handler
    FMenuHandler: TMenuHandler;
    // original event-handler for Application.OnAppActivate
    FOrgAppActivate: TNotifyEvent;
    // paint-info used for drawing
    FPInfo: TPaint;
    // printing-options
    FPrintOptions: TPrintOptions;
    // print-source-rect in project-coordinates
    FPrintRect: TRotRect;
    FPrintRect1:TRotRect;
    // pointer to the project
    FProject: PProj;
    // true if the dialog is reinvoked after selecting the print-range
    FReinvoke: Boolean;
    FUpdating: Boolean;
    // rectangle of the project-printing-area
    FProjGraphic: TProjectPlaceholder;
    // the view- and layer-settings when opening the dialog
    FSight: PSight;
    // true if the view was changed
    FSightChanged: Boolean;
    procedure DrawPicture(Graphic: TWindowsGraphic);
    procedure DrawSignature(Graphic: TPrintSignature);
    procedure DrawLegend(Graphic: TLegendPlaceholder);
    procedure DrawPreview(Graphic: TProjectPlaceholder);
{++ Moskaliov AXWinGIS Printing Methods BUILD#127 05.09.00}
    procedure ReadPrintOptFromReg;
{-- Moskaliov AXWinGIS Printing Methods BUILD#127 05.09.00}
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 25.10.00}
//     Procedure   FillDialogComboBoxes;   Moved to published
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 25.10.00}
//++ Glukhov PrintLayout Build#166 29.12.01
    // Changes PrintRect for the new Width
    procedure SetNewWidthToPrintRect( Width: Double );
    // Changes PrintRect for the new Height
    procedure SetNewHeightToPrintRect( Height: Double );
    // Changes PrintRect so that it has the same width/height ratio as the bounds
    procedure AdjustPrintRectToBounds;
//-- Glukhov PrintLayout Build#166 29.12.01
//++ Glukhov Bug#293(PrintFrame) Build#175 15.02.02
    function GetLegendGraphic : TLegendPlaceholder;
//-- Glukhov Bug#293(PrintFrame) Build#175 15.02.02
    procedure GetPrintOptions;
    function  PreviewRect(FromInputs: Boolean): TGrRect;
    procedure PageSizeUpdated;
    procedure PaperSizeUpdated;
    procedure RedrawPreview;
    procedure Repagenation;
    procedure SetCurrentItem(AItem: PGrRect);
    procedure SetEntryFieldUnits(AUnits: TMeasureUnits);
    procedure SetFieldsFromProjRect;
    procedure SetFieldsFromLegendRect;
    procedure SetFieldsFromPictureRect;
    procedure SetFieldsFromSignatureRect;
    procedure SetMenuHandler(AMenuHandler: TMenuHandler);
    procedure SetPrintOptions(APrintOptions: TPrintOptions);
    procedure SetProject(AProject: PProj);
    procedure SetupMenuHandler(AMenuHandler: TMenuHandler);
    procedure UpdateEditHandlerPosition;
    procedure UpdateEnabled;
    procedure UpdateKeepAspect;
    procedure UpdateModelEnabled;
    procedure UpdatePrinter;
    procedure UpdatePrintView;
  private
    property CurrentItem: PGrRect read FCurrentItem write SetCurrentItem;
    property MenuHandler: TMenuHandler read FMenuHandler write SetMenuHandler;
  public
    property PrintOptions: TPrintOptions read FPrintOptions write SetPrintOptions;
    property Project: PProj read FProject write SetProject;
    // set reinvoke to true if dialog is shown again and not created newly
    property Reinvoke: Boolean read FReinvoke write FReinvoke;
    function GetProjGraph: TProjectPlaceholder;
  end;
{$ENDIF} // <----------------- AXDLL
implementation
{$IFNDEF AXDLL} // <----------------- AXDLL

{$R *.DFM}

uses NumTools, RegDB, SysUtils, ExtCanvas, WinSpool, UIntTools, UserIntf, Legend,
  AM_ProjO, Am_layer,AM_Main,prnframe,Am_child, OleCtnrs2, LicenseHndl;

procedure TPrintingDialog.FormCreate(Sender: TObject);
begin
  PrinterCombo.Items:= WPrinters.Printers;
  FPrintOptions:= TPrintOptions.Create;
  FSignatureIndex:= -1;
  FGraphics:= FPrintOptions.Graphics;
  FGraphics.PageSetup:= FPrintOptions.PageSetup;
  Scroller.Canvas:= PreviewWindow.Canvas;
  { Editor Frame initialisation }
  SetupMenuHandler(EditHandler);
  SetupMenuHandler(RubberBox);
  FPInfo.Init;
  // hook the application-activate event
  FOrgAppActivate:= Application.OnActivate;
  //   Application.OnActivate:=DoAppActivate;
     // Layout Model ComboBox initialisation
  LayoutModelCombo.Items.AddObject(MlgSection[80], TObject(plmUniquePage));
  LayoutModelCombo.Items.AddObject(MlgSection[81], TObject(plmUniformLayout));
  LayoutModelCombo.Items.AddObject(MlgSection[82], TObject(plmBook));
  LayoutModelCombo.ItemIndex:= Ord(FPrintOptions.PageSetup.PrintLayoutModel);
  // Legend Position Origin ComboBox initialisation
  LegOriginCombo.Items.AddObject(MlgSection[49], TObject(poTop));
  LegOriginCombo.Items.AddObject(MlgSection[50], TObject(poBottom));
  LegOriginCombo.ItemIndex:= Ord(poTop);
  // Pictures Position Origin ComboBox initialisation
  PicOriginCombo.Items.AddObject(MlgSection[49], TObject(poTop));
  PicOriginCombo.Items.AddObject(MlgSection[50], TObject(poBottom));
  PicOriginCombo.ItemIndex:= Ord(poTop);
  // Signatures Position Origin ComboBox initialisation
  SigOriginCombo.Items.AddObject(MlgSection[49], TObject(poTop));
  SigOriginCombo.Items.AddObject(MlgSection[50], TObject(poBottom));
  SigOriginCombo.ItemIndex:= Ord(poTop);
  // Select All Contents Flag of edit-box initialisation
  FSelectAll:= TRUE;
end;

procedure TPrintingDialog.SortPaintBoxPaint(Sender: TObject);
begin
  with SortPaintBox, Canvas do begin
    if SortCopiesCheck.Checked then CopiesImages.Draw(SortPaintBox.Canvas, 0, 0, 0)
    else CopiesImages.Draw(SortPaintBox.Canvas, 0, 0, 1)
  end;
end;

procedure TPrintingDialog.SortCopiesCheckClick(Sender: TObject);
begin
  SortPaintBox.Invalidate;
end;

function TPrintingDialog.GetProjGraph: TProjectPlaceholder;
var
  Cnt: Integer;
begin
  GetProjGraph:= nil;
  // detect the project placeholder
  for Cnt:= 0 to FGraphics.ItemCount - 1 do
    if FGraphics.Items[Cnt].ClassName = 'TProjectPlaceholder' then
    begin
      GetProjGraph:= TProjectPlaceholder(FGraphics.Items[Cnt]);
      Break;
    end;
end;

procedure TPrintingDialog.SetPrintOptions(APrintOptions: TPrintOptions);
var
  Cnt: Integer;
begin
  FUpdating:= TRUE;
  try
    with APrintOptions do
    begin
      PrinterCombo.ItemIndex:= PrinterCombo.Items.IndexOf(PageSetup.Printer.Name);
      LeftVal[muMillimeters]:= PageSetup.PageFrames.Left;
      RightVal[muMillimeters]:= PageSetup.PageFrames.Right;
      BottomVal[muMillimeters]:= PageSetup.PageFrames.Bottom;
      TopVal[muMillimeters]:= PageSetup.PageFrames.Top;
      InterLeftVal[muMillimeters]:= PageSetup.InterPageFrames.Left;
      InterRightVal[muMillimeters]:= PageSetup.InterPageFrames.Right;
      InterBottomVal[muMillimeters]:= PageSetup.InterPageFrames.Bottom;
      InterTopVal[muMillimeters]:= PageSetup.InterPageFrames.Top;
      CopiesVal.AsInteger:= PageSetup.Copies;
      SortCopiesCheck.Checked:= PageSetup.SortCopies;

      PagesHorzVal.AsInteger:= PageSetup.HorizontalPages;
      PagesVertVal.AsInteger:= PageSetup.VerticalPages;

      Cnt:= ViewsCombo.Items.IndexOf(FProjGraphic.ViewName);
      if Cnt < 0
      then  ViewsCombo.ItemIndex:= Min(ViewsCombo.Items.Count - 1, 0)
      else  ViewsCombo.ItemIndex:= Cnt;

      PrintToFileCheck.Checked:= PrintToFile;
      PrintToWMFCheck.Checked:= PageSetup.PrintToWMF;
      OverlapLeftVal[muMillimeters]:= PageSetup.OverlapPageFrames.Left;
      OverlapRightVal[muMillimeters]:= PageSetup.OverlapPageFrames.Right;
      OverlapBottomVal[muMillimeters]:= PageSetup.OverlapPageFrames.Bottom;
      OverlapTopVal[muMillimeters]:= PageSetup.OverlapPageFrames.Top;
      LayoutModelCombo.ItemIndex:= Ord(PageSetup.PrintLayoutModel);
      HorizOrderBtn.Checked:= PageSetup.PagesOrder;
      VertOrderBtn.Checked:= not PageSetup.PagesOrder;
      StartPageVal.AsInteger:= PageSetup.StartPageNum;

      PageSetup.UpdatePaperFormats;
      FormatsCombo.Items:= PageSetup.PaperFormatNames;
      FormatsCombo.ItemIndex:= PageSetup.PaperFormatIndex;

      if PageSetup.Orientation = wpoPortrait then
        PortraitBtn.Checked:= TRUE
      else
        LandscapeBtn.Checked:= TRUE;
      AutoRpBtn.Checked:= PageSetup.AutoRepagenate;
      ManualRpBtn.Checked:= not PageSetup.AutoRepagenate;

      // YG: I changed a bit the interpretation of this property to make it clear
      // Now it means that the form of printing rectangle is set in the project,
      // and it is set in the dialog, if else
      if KeepScale
      then btnSetFrameInPrj.Checked:= True
      else btnSetFrameInDlg.Checked:= True;

      CropMarksCheckBox.Checked:= PageSetup.PrintCropMarks;
    end;
  finally
    FUpdating:= FALSE;
  end;
  FReinvoke:= TRUE;
  PaperSizeUpdated;
  UpdatePrinter;
  UpdateKeepAspect;
  UpdateModelEnabled;
  SetFieldsFromProjRect;
  SetFieldsFromLegendRect;
  SetFieldsFromPictureRect;
  SetFieldsFromSignatureRect;
  UpdateEnabled;
end;

procedure TPrintingDialog.PrinterComboClick(Sender: TObject);
var
  FPrinter: TWPrinter;
begin
  UserInterface.BeginWaitCursor;
  try
    with FPrintOptions.PageSetup do
    begin
      FPrinter:= WPrinters[WPrinters.Printers.IndexOf(PrinterCombo.Text)];
      // check if the paper-format is supported by the new printer, reset
      // to previous printer if the user decides to
      if (FPrinter.IndexOfPaperFormat(WindowsID) < 0) and
        (MessageDialog(MlgSection[66], mtConfirmation, [mbYes, mbNo], 0) = mrNo) then
      begin
        PrinterCombo.ItemIndex:= PrinterCombo.Items.IndexOf(Printer.Name);
        Exit;
      end;
      LastError:= [];
      Printer:= FPrinter;
      if erPaperFormatNotSupported in LastError then
        PaperSizeUpdated;
      SetPrintOptions(FPrintOptions);
    end;
    UpdatePrinter;
  finally
    UserInterface.EndWaitCursor;
  end;
end;

procedure TPrintingDialog.UpdatePrinter;
var
  Count: Integer;
  Number: Integer;
  Comments: string;
begin
  with FPrintOptions.PageSetup do begin
    TypeLabel.Caption:= Printer.Typ;
    // replace line-feeds by semicolons
    Comments:= Printer.Comments;
    Count:= Pos(#13#10, Comments);
    while Count <> 0 do begin
      Comments[Count]:= ';'; Comments[Count + 1]:= ' ';
      Count:= Pos(#13#10, Comments);
    end;
    CommentsLabel.Caption:= Comments;
    if Printer.Location = '' then LocationLabel.Caption:= Printer.Port
    else LocationLabel.Caption:= Printer.Location;
    if Printer.IsDefaultPrinter then StatusLabel.Caption:= MlgSection[41]
    else StatusLabel.Caption:= '';
    if Printer.Status <> 0 then begin
      Count:= Printer.WaitingDocuments;
      if Count = 1 then Number:= 126
      else Number:= 127;
      StatusLabel.Caption:= StatusLabel.Caption + Printer.StatusText + ' '
        + Format(MlgStringList['Printers', Number], [Count]);
    end
    else StatusLabel.Caption:= StatusLabel.Caption + Printer.StatusText;
    // update sort-copies check depending if printer supports collation
    SortCopiesCheck.Enabled:= wpcCollation in Printer.Capabilities;
    if not SortCopiesCheck.Enabled then SortCopiesCheck.Checked:= FALSE;
  end;
end;

procedure TPrintingDialog.PropertiesBtnClick(Sender: TObject);
begin
  with FPrintOptions.PageSetup do
  begin
    GetPrintOptions;
    SetOptionsToPrinter;
    if Printer.Properties(Handle) then
    begin
//      ShowMessage(Printer.PaperFormatNames.Text);
      GetOptionsFromPrinter;
      UpdatePrinter;
      SetPrintOptions(FPrintOptions);
    end;
  end;
end;

procedure TPrintingDialog.ViewsComboClick(Sender: TObject);
begin
  // check the view-button
  PrintSavedViewBtn.Checked:= TRUE;
  // set the view-settings to the project (only if previewing)
  UpdatePrintView;
  // scale, entry-fields, etc.
  PrintRangeOrScaleUpdated(Sender);
end;

procedure TPrintingDialog.WindowSelBtnClick(Sender: TObject);
begin
//++ Glukhov Bug#427 Build#166 10.12.01
  if not PrintWindowBtn.Checked then begin
    // Set the source rectangle by the current view for the 1st window setting
    if (not FUpdating) and Assigned(FProject) then begin
      FPrintRect:= FProject.PInfo.GetCurrentScreen;
      AdjustPrintRectToBounds;
      FPrintOptions.PrintRect:= FPrintRect;
    end;
  end;
//-- Glukhov Bug#427 Build#166 10.12.01
  PrintWindowBtn.Checked:= TRUE;
end;

{******************************************************************************+
  Procedure TPrintingDialog.PageSizeUpdated
--------------------------------------------------------------------------------
  Call this Function if the page-size has changed (page-format or page-margins
  have changed). Recalculates the page-size, the printable size and the
  size of the current plot.
+******************************************************************************}

procedure TPrintingDialog.PageSizeUpdated;
begin
  if not FUpdating then
  begin
    if not FReinvoke then
      GetPrintOptions;
    Scroller.LockUpdate;
    if FMenuHandler <> nil then
      FMenuHandler.Visible:= FALSE;
    try
      if FPrintOptions <> nil then begin
         Scroller.Size:= RotRect(FPrintOptions.PageSetup.PageSize, 0);
      end;
      Scroller.CurrentView:= Scroller.Size;
    finally
      Scroller.UnlockUpdate;
      if FMenuHandler <> nil then
        FMenuHandler.Visible:= TRUE;
    end;
    if FPrintOptions <> nil then begin
       RedrawPreview;
    end;
  end;
end;

{******************************************************************************+
  Procedure TPrintingDialog.PreviewPaint
--------------------------------------------------------------------------------
  Paints the preview-window. Displays the page, the page-separators and
  the graphic (if activated).
+******************************************************************************}

procedure TPrintingDialog.PreviewPaint(Sender: TObject);
var
  Rect: TRect;
  ARect: TRect;
  Cnt: Integer;
  APoint: TPoint;
  {++ Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
  dblDistance: Double;
  intDistance: Integer;
  intLength: Integer;
  CntY: Integer;
  OldPen: TPen;

  // Internal procedure
  procedure DrawCropMarks;
  begin
    with PreviewWindow.Canvas do
    begin
      // Draw Left-Top Corner
      APoint:= ARect.TopLeft;
      APoint.X:= APoint.X - intDistance;
      MoveTo(APoint.X, APoint.Y);
      APoint.X:= APoint.X - intLength;
      LineTo(APoint.X, APoint.Y);
      APoint:= ARect.TopLeft;
      APoint.Y:= APoint.Y + intDistance;
      MoveTo(APoint.X, APoint.Y);
      APoint.Y:= APoint.Y + intLength;
      LineTo(APoint.X, APoint.Y);
      // Draw Left-Bottom Corner
      APoint.Y:= ARect.Bottom;
      APoint.Y:= APoint.Y - intDistance;
      MoveTo(APoint.X, APoint.Y);
      APoint.Y:= APoint.Y - intLength;
      LineTo(APoint.X, APoint.Y);
      APoint.Y:= ARect.Bottom;
      APoint.X:= APoint.X - intDistance;
      MoveTo(APoint.X, APoint.Y);
      APoint.X:= APoint.X - intLength;
      LineTo(APoint.X, APoint.Y);
      // Draw Right-Bottom Corner
      APoint:= ARect.BottomRight;
      APoint.X:= APoint.X + intDistance;
      MoveTo(APoint.X, APoint.Y);
      APoint.X:= APoint.X + intLength;
      LineTo(APoint.X, APoint.Y);
      APoint:= ARect.BottomRight;
      APoint.Y:= APoint.Y - intDistance;
      MoveTo(APoint.X, APoint.Y);
      APoint.Y:= APoint.Y - intLength;
      LineTo(APoint.X, APoint.Y);
      // Draw Right-Top Corner
      APoint.Y:= ARect.Top;
      APoint.Y:= APoint.Y + intDistance;
      MoveTo(APoint.X, APoint.Y);
      APoint.Y:= APoint.Y + intLength;
      LineTo(APoint.X, APoint.Y);
      APoint.Y:= ARect.Top;
      APoint.X:= APoint.X + intDistance;
      MoveTo(APoint.X, APoint.Y);
      APoint.X:= APoint.X + intLength;
      LineTo(APoint.X, APoint.Y);
    end;
  end; // End of the internal procedure DrawCropMarks
  {-- Moskaliov BUGS##202b,293 BUILD#152 19.01.01}

begin
  Screen.Cursor:= crHourGlass;
  try
    with PreviewWindow.Canvas do
    begin
      Scroller.InternalToWindow(FPrintOptions.PageSetup.PageSize, Rect);
      with Rect do
      begin
        Brush.Style:= bsSolid;
        Brush.Color:= Color;
        // draw area around pages
        ARect:= PreviewWindow.ClientRect;
        DPToLP(Handle, ARect, 2);
        with ARect do
        begin
          FillRect(Classes.Rect(Left, Top, Right, Rect.Top));
          FillRect(Classes.Rect(Left, Rect.Bottom, Right, Bottom));
          FillRect(Classes.Rect(Left, Rect.Top, Rect.Left, Rect.Bottom));
          FillRect(Classes.Rect(Rect.Right, Top, Right, Rect.Bottom));
        end;
        // draw page in white with black border
        Pen.Style:= psSolid;
        Pen.Color:= clBlack;
        Brush.Style:= bsSolid;
        Brush.Color:= clWindow;
        // grow rectangle by one pixel
        LPToDP(Handle, Rect, 2);
        InflateRect(Rect, 1, 1); Inc(Rect.Right); Inc(Rect.Bottom);
        DPToLP(Handle, Rect, 2);
        Rectangle(Left, Top, Right, Bottom);
        // draw shadow of the page
        LPToDP(Handle, Rect, 2);
        InflateRect(Rect, 1, 1); Inc(Left, 5); Inc(Top, 5);
        DPToLP(Handle, Rect, 2);
        APoint:= Point(3, 3);
        DPToLP(Handle, APoint, 1);
        Pen.Color:= clBtnShadow;
        Pen.Width:= APoint.X;
        MoveTo(Left, Bottom);
        LineTo(Right, Bottom);
        LineTo(Right, Top);
        Pen.Width:= 1;
      end;
      Scroller.InternalToWindow(FPrintOptions.PageSetup.PageExtent, Rect);
      Pen.Style:= psDot;
      Pen.Color:= clSilver;
      Brush.Style:= bsClear;
      with Rect do
      begin
        LPToDP(Handle, Rect, 2);
        Inc(Rect.Right); Inc(Rect.Bottom);
        DPToLP(Handle, Rect, 2);
        Rectangle(Left, Bottom, Right, Top);
        for Cnt:= 0 to FPrintOptions.PageSetup.HorizontalPages - 2 do
        begin
          Scroller.InternalToWindow(FPrintOptions.PageSetup.PageExtents[Cnt, 0], ARect);
          MoveTo(ARect.Right, Rect.Top);
          LineTo(ARect.Right, Rect.Bottom);
        end;
        for Cnt:= 0 to FPrintOptions.PageSetup.VerticalPages - 2 do
        begin
          Scroller.InternalToWindow(FPrintOptions.PageSetup.PageExtents[0, Cnt], ARect);
          Rect.Top:= Rect.Top - (ARect.Top - ARect.Bottom);
          MoveTo(Rect.Left, Rect.Top);
          LineTo(Rect.Right, Rect.Top);
        end;
      end;
    end;
    // draw preview
    for Cnt:= 0 to FGraphics.ItemCount - 1 do
      if FGraphics.Items[Cnt].ClassName = 'TProjectPlaceholder' then
        DrawPreview(TProjectPlaceholder(FGraphics.Items[Cnt]))
      else if (FGraphics.Items[Cnt].ClassName = 'TLegendPlaceholder') then
        DrawLegend(TLegendPlaceholder(FGraphics.Items[Cnt]))
      else if FGraphics.Items[Cnt].ClassName = 'TWindowsGraphic' then
        DrawPicture(TWindowsGraphic(FGraphics.Items[Cnt]))
      else if FGraphics.Items[Cnt].ClassName = 'TPrintSignature' then
        DrawSignature(TPrintSignature(FGraphics.Items[Cnt]));

    // draw Crop Marks if it is necessary
    if CropMarksCheckBox.Checked then
    with PreviewWindow.Canvas do  begin
        OldPen:= TPen.Create;
        OldPen.Assign(Pen);
        Pen.Style:= psSolid;
        Pen.Color:= clBlack;
        //             Pen.Mode:=pmNotXor;
        dblDistance:= 1.0; // Distance in mm
        intDistance:= Round(Scroller.InternalToWindow(dblDistance));
        intLength:= intDistance * 5;
        for Cnt:= 0 to FPrintOptions.PageSetup.HorizontalPages - 1 do
          for CntY:= 0 to FPrintOptions.PageSetup.VerticalPages - 1 do
          begin
            Scroller.InternalToWindow(FPrintOptions.PageSetup.PageExtents[Cnt, CntY], ARect);
            DrawCropMarks;
          end;
        Pen.Assign(OldPen);
        OldPen.Free;
    end;

    // draw the edit-handler
    if FMenuHandler <> nil  then FMenuHandler.Paint;
  finally
    Screen.Cursor:= crDefault;
  end;
end;

function IntersectTGrRect(FRect: TGrRect; SRect: TGrRect): TGrRect;
begin
  if (FRect.Left > SRect.Right) or (FRect.Right < SRect.Left) or
    (FRect.Bottom > SRect.Top) or (FRect.Top < SRect.Bottom) then
  begin
    Result:= GrRect(0, 0, 0, 0);
    Exit;
  end;
  Result:= FRect;
  if FRect.Left < SRect.Left then
    Result.Left:= SRect.Left;
  if FRect.Right > SRect.Right then
    Result.Right:= SRect.Right;
  if FRect.Top > SRect.Top then
    Result.Top:= SRect.Top;
  if FRect.Bottom < SRect.Bottom then
    Result.Bottom:= SRect.Bottom;
end; // End of the function IntersectTGrRect

{******************************************************************************+
  Procedure TPrintingDialog.PageFramesUpdated
--------------------------------------------------------------------------------
  Called if one of the margin-editors looses the focus. Updates the page-size
  and preview-graphic.
+******************************************************************************}

procedure TPrintingDialog.PageFramesUpdated(Sender: TObject);
var
  Cnt: Integer;
  TempRect: TGrRect;
  ExtentHeight: Double;
  dblWidth: Double;
  dblHeight: Double;
  intValue: Integer;
  dblSquare: Double;
  dblMax: Double;
  CntX: Integer;
  CntY: Integer;
  IndX: Integer;
  IndY: Integer;
label
  labWholePageExtent;

begin
  if FKeyPressed then
  begin
    ExtentHeight:= RectHeight(FPrintOptions.PageSetup.PageExtent);
    PageSizeUpdated;
    PrintRangeOrScaleUpdated(Sender);
    for Cnt:= 0 to FGraphics.ItemCount - 1 do
      if FGraphics.Items[Cnt].ClassName <> 'TProjectPlaceholder' then
        {++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
        if (FGraphics.PageSetup.PrintLayoutModel = plmBook) and
          (FGraphics.PageSetup.HorizontalPages * FGraphics.PageSetup.VerticalPages > 1) and
          (FGraphics.Items[Cnt].PrintPosition <> ppCustom) then
          if (FGraphics.Items[Cnt].ClassName = 'TPrintSignature') then
            {-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
          begin
            if TPrintSignature(FGraphics.Items[Cnt]).PrintLocation = plOneTime then
              goto labWholePageExtent;
            // finding Page Extent which contains Text by MAX intersection square
            dblMax:= 0.0;
            IndX:= 0;
            IndY:= 0;
            for CntY:= 0 to FGraphics.PageSetup.VerticalPages - 1 do
              for CntX:= 0 to FGraphics.PageSetup.HorizontalPages - 1 do
              begin
                TempRect:= IntersectTGrRect(FGraphics.Items[Cnt].BoundsRect, FGraphics.PageSetup.PageExtents[CntX, CntY]);
                dblSquare:= RectWidth(TempRect) * RectHeight(TempRect);
                if dblSquare > dblMax then
                begin
                  dblMax:= dblSquare;
                  IndX:= CntX;
                  IndY:= CntY;
                end;
              end;
            dblWidth:= RectWidth(FGraphics.Items[Cnt].BoundsRect);
            dblHeight:= RectHeight(FGraphics.Items[Cnt].BoundsRect);
            if (FGraphics.Items[Cnt].PrintPosition = ppCenterHoriz) or (FGraphics.Items[Cnt].PrintPosition = ppCenter) then
              with FGraphics.PageSetup.PageExtents[IndX, IndY] do
                case TPrintSignature(FGraphics.Items[Cnt]).TextStyle.Alignment of
                  Ord(taRight): begin
                      TempRect.Right:= (Left + Right) * 0.5;
                      TempRect.Left:= TempRect.Right - dblWidth;
                    end;
                  Ord(taCentered): begin
                      TempRect.Left:= (Left + Right - dblWidth) * 0.5;
                      TempRect.Right:= TempRect.Left + dblWidth;
                    end;
                else { taLeft }  begin
                    TempRect.Left:= (Left + Right) * 0.5;
                    TempRect.Right:= TempRect.Left + dblWidth;
                  end;
                end;
            if (FGraphics.Items[Cnt].PrintPosition = ppCenterVert) or (FGraphics.Items[Cnt].PrintPosition = ppCenter) then
              with FGraphics.PageSetup.PageExtents[IndX, IndY] do
              begin
                TempRect.Bottom:= (Bottom + Top - dblHeight) * 0.5;
                TempRect.Top:= TempRect.Bottom + dblHeight;
              end
            else
              if FGraphics.Items[Cnt].PrintOrigin = poTop then
            begin
              TempRect.Top:= FGraphics.Items[Cnt].BoundsRect.Top + RectHeight(FPrintOptions.PageSetup.PageExtent) - ExtentHeight;
              TempRect.Bottom:= TempRect.Top - dblHeight;
            end
            else
            begin
              TempRect.Top:= FGraphics.Items[Cnt].BoundsRect.Top;
              TempRect.Bottom:= FGraphics.Items[Cnt].BoundsRect.Bottom;
            end;
            {++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
            intValue:= 0;
            if CurrentItem = @FGraphics.Items[Cnt].BoundsRect then
            begin
              CurrentItem:= nil;
              intValue:= 1;
            end;
            {-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
            FGraphics.Items[Cnt].Invalidate(Scroller);
            FGraphics.Items[Cnt].BoundsRect:= TempRect;
            FGraphics.Items[Cnt].Invalidate(Scroller);
            {++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
            if intValue > 0 then
              CurrentItem:= @FGraphics.Items[Cnt].BoundsRect;
            {-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
          end
            {++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
          else
            if (FGraphics.Items[Cnt].ClassName = 'TWindowsGraphic') then
          begin
            if TWindowsGraphic(FGraphics.Items[Cnt]).PrintLocation = plOneTime then
              goto labWholePageExtent;
            // finding Page Extent which contains Picture by MAX intersection square
            dblMax:= 0.0;
            IndX:= 0;
            IndY:= 0;
            for CntY:= 0 to FGraphics.PageSetup.VerticalPages - 1 do
              for CntX:= 0 to FGraphics.PageSetup.HorizontalPages - 1 do
              begin
                TempRect:= IntersectTGrRect(FGraphics.Items[Cnt].BoundsRect, FGraphics.PageSetup.PageExtents[CntX, CntY]);
                dblSquare:= RectWidth(TempRect) * RectHeight(TempRect);
                if dblSquare > dblMax then
                begin
                  dblMax:= dblSquare;
                  IndX:= CntX;
                  IndY:= CntY;
                end;
              end;
            dblWidth:= RectWidth(FGraphics.Items[Cnt].BoundsRect);
            dblHeight:= RectHeight(FGraphics.Items[Cnt].BoundsRect);

            if (FGraphics.Items[Cnt].PrintPosition = ppCenterHoriz) or (FGraphics.Items[Cnt].PrintPosition = ppCenter) then
              with FGraphics.PageSetup.PageExtents[IndX, IndY] do
              begin
                TempRect.Left:= (Left + Right - dblWidth) * 0.5;
                TempRect.Right:= TempRect.Left + dblWidth;
              end;

            if (FGraphics.Items[Cnt].PrintPosition = ppCenterVert) or (FGraphics.Items[Cnt].PrintPosition = ppCenter) then
              with FGraphics.PageSetup.PageExtents[IndX, IndY] do
              begin
                TempRect.Bottom:= (Bottom + Top - dblHeight) * 0.5;
                TempRect.Top:= TempRect.Bottom + dblHeight;
              end
            else
              if FGraphics.Items[Cnt].PrintOrigin = poTop then
            begin
              TempRect.Top:= FGraphics.Items[Cnt].BoundsRect.Top + RectHeight(FPrintOptions.PageSetup.PageExtent) - ExtentHeight;
              TempRect.Bottom:= TempRect.Top - dblHeight;
            end
            else
            begin
              TempRect.Top:= FGraphics.Items[Cnt].BoundsRect.Top;
              TempRect.Bottom:= FGraphics.Items[Cnt].BoundsRect.Bottom;
            end;
            intValue:= 0;
            if CurrentItem = @FGraphics.Items[Cnt].BoundsRect then
            begin
              CurrentItem:= nil;
              intValue:= 1;
            end;
            FGraphics.Items[Cnt].Invalidate(Scroller);
            FGraphics.Items[Cnt].BoundsRect:= TempRect;
            FGraphics.Items[Cnt].Invalidate(Scroller);
            if intValue > 0 then
              CurrentItem:= @FGraphics.Items[Cnt].BoundsRect;
          end
          else
            goto labWholePageExtent
              {-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
        else
          labWholePageExtent:
            with FGraphics.Items[Cnt] do
          begin
            TempRect:= BoundsRect;
            dblWidth:= RectWidth(BoundsRect);
            dblHeight:= RectHeight(BoundsRect);
            if (PrintPosition = ppCenterHoriz) or (PrintPosition = ppCenter) then
            begin
              TempRect.Left:= (RectWidth(FPrintOptions.PageSetup.PageExtent) - dblWidth) * 0.5;
              TempRect.Right:= TempRect.Left + dblWidth;
            end;
            if (PrintPosition = ppCenterVert) or (PrintPosition = ppCenter) then
            begin
              TempRect.Bottom:= (RectHeight(FPrintOptions.PageSetup.PageExtent) - dblHeight) * 0.5;
              TempRect.Top:= TempRect.Bottom + dblHeight;
            end
            else
              if PrintOrigin = poTop then
            begin
              TempRect.Top:= TempRect.Top + RectHeight(FPrintOptions.PageSetup.PageExtent) - ExtentHeight;
              TempRect.Bottom:= TempRect.Top - dblHeight;
            end;
            // redraw necessity calculation
            intValue:= Abs(DblCompare(BoundsRect.Left, TempRect.Left));
            Inc(intValue, Abs(DblCompare(BoundsRect.Bottom, TempRect.Bottom)));
            Inc(intValue, Abs(DblCompare(BoundsRect.Right, TempRect.Right)));
            Inc(intValue, Abs(DblCompare(BoundsRect.Top, TempRect.Top)));
            if intValue > 0 then
            begin
              if CurrentItem = @BoundsRect then
              begin
                CurrentItem:= nil;
                intValue:= 0;
              end;
              Scroller.InvalidateRect(BoundsRect);
              Scroller.InvalidateRect(TempRect);
              BoundsRect:= TempRect;
              if intValue = 0 then
                CurrentItem:= @BoundsRect;
            end;
          end;
  end;
  FSelectAll:= TRUE;
  OkBtn.Default:= TRUE;
end;

procedure TPrintingDialog.SetProject(AProject: PProj);
begin
  FProject:= AProject;
  with FProject^ do
  begin
    FPInfo.InitCopy(PInfo);
    FPInfo.Previewing:= TRUE;
    FPInfo.ExtCanvas.Cached:= FALSE;
    FPInfo.ExtCanvas.MapMode:= mmLoMetric;
  end;
  FGraphics.Project:= AProject;
end;

procedure TPrintingDialog.MultiPagesUpdated(Sender: TObject);
begin
  if (Sender = HorzPagesEdit) or (Sender = VertPagesEdit) then
    if AutoRpBtn.Checked then
      ManualRpBtn.Checked:= TRUE;
  PageFramesUpdated(Sender);
  FSelectAll:= TRUE;
  OkBtn.Default:= TRUE;
end;

//++ Glukhov PrintLayout Build#166 29.12.01
// Changes PrintRect for the new Width
procedure TPrintingDialog.SetNewWidthToPrintRect( Width: Double );
var
  dblDelta  : Double;
begin
  dblDelta:= (FPrintRect.Width - Width) * 0.5;
  FPrintRect.X:= FPrintRect.X + dblDelta * cos(FPrintRect.Rotation);
  FPrintRect.Y:= FPrintRect.Y + dblDelta * sin(FPrintRect.Rotation);
  FPrintRect.Width:= Width;
end;

// Changes PrintRect for the new Height
procedure TPrintingDialog.SetNewHeightToPrintRect( Height: Double );
var
  dblDelta  : Double;
begin
  dblDelta:= (FPrintRect.Height - Height) * 0.5;
  FPrintRect.X:= FPrintRect.X - dblDelta * sin(FPrintRect.Rotation);
  FPrintRect.Y:= FPrintRect.Y + dblDelta * cos(FPrintRect.Rotation);
  FPrintRect.Height:= Height;
end;

// Changes PrintRect so that it has the same width/height ratio as the bounds
procedure TPrintingDialog.AdjustPrintRectToBounds;
var
  Height  : Double;
  Width   : Double;
  dRatio  : Double;
begin
  Width:= RectWidth( FProjGraphic.BoundsRect );
  Height:= RectHeight( FProjGraphic.BoundsRect );
  if (Width > Tolerance) and (Height > Tolerance) and
     (FPrintRect.Width > Tolerance) and (FPrintRect.Height > Tolerance)
  then begin
    dRatio:= Width / Height;
    dRatio:= dRatio / (FPrintRect.Width / FPrintRect.Height);
    if Abs(dRatio - 1.0) > Tolerance then begin
    // There is a difference in the ratios
      if dRatio > 1.0 then begin
      // Need to extend the PrintRect width
        Width:= FPrintRect.Width * dRatio;
        SetNewWidthToPrintRect( Width );
      end
      else begin
      // Need to extend the PrintRect height
        Height:= FPrintRect.Height / dRatio;
        SetNewHeightToPrintRect( Height );
      end;
    end;
  end;
end;
//-- Glukhov PrintLayout Build#166 29.12.01

procedure TPrintingDialog.PrintRangeOrScaleUpdated(Sender: TObject);
var
  WorldPrintRect  : TGrRect;
  Height  : Double;
  Width   : Double;
  Scale1  : Double;
  Scale2  : Double;
  Sight   : PSight;
  NeedRedraw  : Boolean;
  PreviewSize : TGrRect;
  dblDelta    : Double;
  FProjRect   : TGrRect;
  bTmpKeepScale : Boolean;
  OldPrintRect  : TRotRect;   // Glukhov RedrawProject Build#175 15.02.02
  ResetPrintSpec:boolean;

  begin
  if (FUpdating) or (not Assigned(FProject)) then begin
    UpdateEnabled;
    Exit;
  end;
  FUpdating:= True;
  {brovak}
  ResetPrintSpec:=false;
  {brovak}

  // store the current preview-position for later invalidate
  PreviewSize:= PreviewRect(FALSE);
//++ Glukhov RedrawProject Build#175 15.02.02
  OldPrintRect:= FPrintOptions.PrintRect;
//-- Glukhov RedrawProject Build#175 15.02.02
  // determine the print-range depending on the user-selection
  if PrintAllBtn.Checked
  then  with FProject.Size do
   begin;
    FPrintRect:= RotRect(A.X, A.Y, XSize, YSize, -FProject.PInfo.ViewRotation);
    FPrintRect1:=FPrintRect;
    ResetPrintSpec:=true;
   end
  else
    if PrintCurrentViewBtn.Checked then
       FPrintRect:= FProject.PInfo^.GetCurrentScreen
    else if PrintSavedViewBtn.Checked then begin
      Sight:= FProject.Sights^.NamedSight(ViewsCombo.Text);
      if Sight <> nil  then
          FPrintRect:= Sight^.ViewRect;
    end
    else if PrintWindowBtn.Checked then begin
      FPrintRect:= FPrintOptions.PrintRect;
      if FPrintRect.Width < Tolerance
      then
        FPrintRect:= FProject.PInfo^.GetCurrentScreen;
    end;

  NeedRedraw:= FALSE;
  bTmpKeepScale:= chkKeepScale.Checked;
  if chkFitToPage.Checked then begin
    // Reset the frame to the page
    FProjGraphic.BoundsRect:= FPrintOptions.PageSetup.PageExtent;
//++ Glukhov RedrawProject Build#175 15.02.02
// YG: I added more correct check
    // The frame can be the same but it needs a redrawing
//    NeedRedraw:= TRUE;
//-- Glukhov RedrawProject Build#175 15.02.02
  end;

  // Check the consistency of the source and target rectangles
  if btnSetFrameInDlg.Checked then begin
    // The source rectangle should be adjusted to the target one
    if chkFitToPage.Checked then begin
      if not chkKeepScale.Checked then begin
        // Set the width/height ratio of PrintRect at the BoundsRect's one
        AdjustPrintRectToBounds;
      end;
    end
    else if (not PrintWindowBtn.Checked) then begin
      // Set the width/height ratio of PrintRect at the BoundsRect's one
      AdjustPrintRectToBounds;
    end
    else if Sender = EditHandler then begin
      // Compare the width/height ratios of PrintRect and BoundsRect
      Width:= RectWidth( FProjGraphic.BoundsRect ) / FPrintRect.Width;
      Height:= RectHeight( FProjGraphic.BoundsRect ) / FPrintRect.Height;
      if Height > Tolerance then
      if Abs(Width/Height - 1.0) > 0.05 then begin
        // Make the changes as per KeepScale variant
        bTmpKeepScale:= True;
      end;
    end;
               {brovak}
       if (ResetPrintSpec=false) and  (bTmpKeepScale= false) then begin;
         FPrintRect1:=FPrintRect;
         ResetPrintSpec:=true;
                                    end;
       {brovak}

  end
  else begin
    // The target rectangle should be adjusted to the source one
    if chkFitToPage.Checked then begin
      // Set the width/height ratio of BoundsRect at the PrintRect's one
      Width:= RectWidth( FProjGraphic.BoundsRect );
      Height:= RectHeight( FProjGraphic.BoundsRect );
      dblDelta:= Width / Height;
      dblDelta:= dblDelta / (FPrintRect.Width / FPrintRect.Height);
      if Abs(dblDelta - 1.0) > Tolerance then begin
        // There is a difference in the ratios
        FProjRect:= FProjGraphic.BoundsRect;
        if dblDelta > 1.0 then begin
          // Need to decrease the bounds width
          Width:= Width / dblDelta;
          FProjRect.Left:= (RectWidth(FPrintOptions.PageSetup.PageExtent) - Width) * 0.5;
          FProjRect.Right:= FProjRect.Left + Width;
        end
        else begin
          // Need to decrease the bounds height
          Height:= Height * dblDelta;
          FProjRect.Top:= (RectHeight(FPrintOptions.PageSetup.PageExtent) - Height) * 0.5;
          FProjRect.Bottom:= FProjRect.Top - Height;
        end;
        FProjGraphic.BoundsRect:= FProjRect;
      end;
    end;
  end;

  if bTmpKeepScale then begin
    if (Sender = WidthEdit) or (Sender = WidthSpin) then
    begin
      Width:= WidthVal[muMillimeters] / ScaleVal.AsFloat;
      Width:= Width * (100.0 / FPInfo.UnitsToMM);
      SetNewWidthToPrintRect( Width );
    end
    else if (Sender = HeightEdit) or (Sender = HeightSpin) then
    begin
      Height:= HeightVal[muMillimeters] / ScaleVal.AsFloat;
      Height:= Height * (100.0 / FPInfo.UnitsToMM);
      SetNewHeightToPrintRect( Height );
    end
    else if Sender = EditHandler then
    begin
        // set new position from edit-frame
        FProjGraphic.BoundsRect:=
            GrBounds(EditHandler.XPosition, EditHandler.YPosition, EditHandler.Width, EditHandler.Height);
        SetFieldsFromProjRect;
        if PreviewSize.Left <> FProjGraphic.BoundsRect.Left then
        begin
            dblDelta:= (FProjGraphic.BoundsRect.Left - PreviewSize.Left) / ScaleVal.AsFloat;
            dblDelta:= dblDelta * (100.0 / FPInfo.UnitsToMM);
            FPrintRect.X:= FPrintRect.X + dblDelta * cos(FPrintRect.Rotation);
            FPrintRect.Y:= FPrintRect.Y + dblDelta * sin(FPrintRect.Rotation);
        end;
        if PreviewSize.Bottom <> FProjGraphic.BoundsRect.Bottom then
        begin
            dblDelta:= (FProjGraphic.BoundsRect.Bottom - PreviewSize.Bottom) / ScaleVal.AsFloat;
            dblDelta:= dblDelta * (100.0 / FPInfo.UnitsToMM);
            FPrintRect.X:= FPrintRect.X - dblDelta * sin(FPrintRect.Rotation);
            FPrintRect.Y:= FPrintRect.Y + dblDelta * cos(FPrintRect.Rotation);
        end;
        Width:= RectWidth(FProjGraphic.BoundsRect) / ScaleVal.AsFloat;
        Width:= Width * (100.0 / FPInfo.UnitsToMM);
        FPrintRect.Width:= Width;
        Height:= RectHeight(FProjGraphic.BoundsRect) / ScaleVal.AsFloat;
        Height:= Height * (100.0 / FPInfo.UnitsToMM);
        FPrintRect.Height:= Height;
        // restore the correct current preview-position for later invalidate
        PreviewSize:= PreviewRect(FALSE);
    end
    else if (Sender = RightEdit) or (Sender = TopEdit) then
    begin
      // for redraw whole PrintPreview
      PreviewSize:= FPrintOptions.PageSetup.PageSize;
      NeedRedraw:= TRUE;
    end
    else if btnSetFrameInDlg.Checked or chkFitToPage.Checked then begin
    // The source rectangle should be adjusted to the target one
      // Set the width&height of PrintRect, using the scale
      Width:= RectWidth(FProjGraphic.BoundsRect) / ScaleVal.AsFloat;
      Width:= Width * (100.0 / FPInfo.UnitsToMM);
      if (Width < FPrintRect.Width) and (not PrintWindowBtn.Checked)
      then  PrintWindowBtn.Checked:= True;
      SetNewWidthToPrintRect( Width );
      Height:= RectHeight(FProjGraphic.BoundsRect) / ScaleVal.AsFloat;
      Height:= Height * (100.0 / FPInfo.UnitsToMM);
      if (Height < FPrintRect.Height) and (not PrintWindowBtn.Checked)
      then  PrintWindowBtn.Checked:= True;
      SetNewHeightToPrintRect( Height );
    end;

    // Adjust the target rectangle to the source one
    FPrintOptions.PrintRect:= FPrintRect;
    WorldPrintRect:= GrBounds(FPrintRect.X, FPrintRect.Y, FPrintRect.Width, FPrintRect.Height);
    // calculate widht and height of print rectangle in mm
    Width:= RectWidth(WorldPrintRect) * FPInfo.UnitsToMM / 100.0;
    Height:= RectHeight(WorldPrintRect) * FPInfo.UnitsToMM / 100.0;
  end
  else begin
    FPrintOptions.PrintRect:= FPrintRect;
    WorldPrintRect:= GrBounds(FPrintRect.X, FPrintRect.Y, FPrintRect.Width, FPrintRect.Height);
    // calculate widht and height of print rectangle in mm
    Width:= RectWidth(WorldPrintRect) * FPInfo.UnitsToMM / 100.0;
    Height:= RectHeight(WorldPrintRect) * FPInfo.UnitsToMM / 100.0;
    // recalculate the scale
    if (Sender = WidthEdit) or (Sender = WidthSpin)
    then  ScaleVal.AsFloat:= WidthVal[muMillimeters] / Width
    else
      if (Sender = HeightEdit) or (Sender = HeightSpin)
      then  ScaleVal.AsFloat:= HeightVal[muMillimeters] / Height
      else
        if Sender = EditHandler then begin
          Scale1:= RectWidth(FProjGraphic.BoundsRect) / Width;
          Scale2:= RectHeight(FProjGraphic.BoundsRect) / Height;
          if Scale1 > Scale2
          then  ScaleVal.AsFloat:= Scale2
          else  ScaleVal.AsFloat:= Scale1;
        end
        else if (Sender = RightEdit) or (Sender = TopEdit) then
        begin
          // for redraw whole PrintPreview
          PreviewSize:= FPrintOptions.PageSetup.PageSize;
          NeedRedraw:= TRUE;
        end
        else begin
          // Set the new scale value
          if chkFitToPage.Checked then begin
            // psFitToPage
            Scale1:= RectWidth(FPrintOptions.PageSetup.PageExtent) / Width;
            Scale2:= RectHeight(FPrintOptions.PageSetup.PageExtent) / Height;
          end
          else begin
            // Fit to frame
            Scale1:= WidthVal[muMillimeters] / Width;
            Scale2:= HeightVal[muMillimeters] / Height;
          end;
          if Scale1 > Scale2
          then  ScaleVal.AsFloat:= Scale2
          else  ScaleVal.AsFloat:= Scale1;
        end;
  end;

  FProjGraphic.PrintScale:= ScaleVal.AsFloat;
  Width:= Width * ScaleVal.AsFloat;
  Height:= Height * ScaleVal.AsFloat;
  WidthVal[muMillimeters]:= Width;
  HeightVal[muMillimeters]:= Height;

  // center image if center-option checked
  if CenterPageBtn.Checked then  begin
    LeftPosVal[muMillimeters]:= (RectWidth(FPrintOptions.PageSetup.PageExtent) - Width) * 0.5;
    TopPosVal[muMillimeters]:= (RectHeight(FPrintOptions.PageSetup.PageExtent) - Height) * 0.5;
  end;

  FProjRect:= FProjGraphic.BoundsRect;
  // calculate x-position of edit-frame
  NumTools.Update(FProjRect.Left, LeftPosVal[muMillimeters], NeedRedraw);
  // calculate y-position of edit-frame
  NumTools.Update(FProjRect.Bottom, RectHeight(FPrintOptions.PageSetup.PageExtent)
        - Height - TopPosVal[muMillimeters], NeedRedraw);
  if DblCompare(RectWidth(FProjRect), Width) <> 0 then begin
    FProjRect.Right:= FProjRect.Left + Width;
    NeedRedraw:= TRUE;
  end;
  if DblCompare(RectHeight(FProjRect), Height) <> 0 then begin
    FProjRect.Top:= FProjRect.Bottom + Height;
    NeedRedraw:= TRUE;
  end;
  FProjGraphic.BoundsRect:= FProjRect;
//++ Glukhov RedrawProject Build#175 15.02.02
  if not RectsEqual(OldPrintRect, FPrintRect)  then NeedRedraw:= TRUE;
//-- Glukhov RedrawProject Build#175 15.02.02

  FUpdating:= False;
{brovak}
  RedrawSpecPrintFrame;
{brovak}

  // redraw preview if it is necessary
  if NeedRedraw then begin
    UpdateEditHandlerPosition;
    Scroller.InvalidateRect(PreviewSize);
    RedrawPreview;
  end;
  // repaginating Print-Layout, if it is necessary
  if AutoRpBtn.Checked  then Repagenation;

  UpdateEnabled;
end;

procedure TPrintingDialog.ScaleUpdated(Sender: TObject);
var
  boolNeedRedraw: Boolean;
  Graphic: TPrintSignature;
  Cnt: Integer;
begin
  if Sender = ScaleSpin then begin
    if not ScaleCombo.Focused then
    begin
      ScaleCombo.SetFocus;
      ScaleCombo.SelLength:= 0;
    end;
  end;
  chkKeepScale.Checked:= True;
  PrintRangeOrScaleUpdated(Sender);
{++ Moskaliov BUG#202b BUILD#144 04.12.00}
  boolNeedRedraw:= FALSE;
  // detect the signature with SCALE field
  for Cnt:= 0 to FGraphics.ItemCount - 1 do
    if FGraphics.Items[Cnt] is TPrintSignature then
    begin
      Graphic:= TPrintSignature(FGraphics.Items[Cnt]);
      if Pos(FieldsCodes[Ord(fcScale)], Graphic.Signature) <> 0 then
      begin
        boolNeedRedraw:= TRUE;
        Break;
      end;
    end;
  if boolNeedRedraw  then PreviewPaint(Sender);
{-- Moskaliov BUG#202b BUILD#144 04.12.00}
  OkBtn.Default:= TRUE;
end;

procedure TPrintingDialog.WidthOrHeightUpdated(Sender: TObject);
begin
  // psScale or psWidthAndHeight
  chkFitToPage.Checked:= False;
  if Sender = WidthSpin then
  begin
    if not WidthEdit.Focused then
    begin
      WidthEdit.SetFocus;
      WidthEdit.SelLength:= 0;
    end;
  end
  else
    if Sender = HeightSpin then
    if not HeightEdit.Focused then
    begin
      HeightEdit.SetFocus;
      HeightEdit.SelLength:= 0;
    end;
  PrintRangeOrScaleUpdated(Sender);
  if chkKeepScale.Checked and not PrintWindowBtn.Checked
// YG: corrected to esclude extra call of PrintRangeOrScaleUpdated(Sender)
//  then  PrintWindowBtn.Checked:= TRUE;
  then begin
    FUpdating:= True;
    PrintWindowBtn.Checked:= TRUE;
    FUpdating:= False;
  end;
  OkBtn.Default:= TRUE;
end;

procedure TPrintingDialog.EditEntered(Sender: TObject);
begin
  FKeyPressed:= FALSE;
end;

procedure TPrintingDialog.ScaleEditExit(Sender: TObject);
begin
  if FKeyPressed then
  begin
    ScaleUpdated(Sender);
  end;
end;

procedure TPrintingDialog.WidthOrHeightEditExit(Sender: TObject);
begin
  if FKeyPressed then
    WidthOrHeightUpdated(Sender);
  FSelectAll:= TRUE;
end;

procedure TPrintingDialog.LeftOrTopPosExit(Sender: TObject);
begin
  if FKeyPressed then
    LeftOrTopPosUpdated(Sender);
  FSelectAll:= TRUE;
end;

procedure TPrintingDialog.LeftOrTopPosUpdated(Sender: TObject);
begin
  LeftPosBtn.Checked:= TRUE;
  chkFitToPage.Checked:= False;
  if Sender = LeftPosSpin then
  begin
    if not LeftPosEdit.Focused then
    begin
      LeftPosEdit.SetFocus;
      LeftPosEdit.SelLength:= 0;
    end;
  end
  else begin
    if Sender = TopPosSpin then
      if not TopPosEdit.Focused then
      begin
        TopPosEdit.SetFocus;
        TopPosEdit.SelLength:= 0;
      end;
  end;
  PrintRangeOrScaleUpdated(Sender);
  OkBtn.Default:= TRUE;
end;

procedure TPrintingDialog.PreviewMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var
  APoint: TPoint;
  BPoint: TGrPoint;
begin
  APoint:= Point(X, Y);
  DPToLP(PreviewWindow.Canvas.Handle, APoint, 1);
  BPoint:= GrPoint(APoint.X, APoint.Y);
  Scroller.WindowToInternal(BPoint);
  if FMenuHandler <> nil then
    FMenuHandler.OnMouseMove(BPoint.X, BPoint.Y, Shift);
end;

procedure TPrintingDialog.PreviewMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  APoint: TPoint;
  BPoint: TGrPoint;
  Cnt: Integer;
begin
  APoint:= Point(X, Y);
  DPToLP(PreviewWindow.Canvas.Handle, APoint, 1);
  BPoint:= GrPoint(APoint.X, APoint.Y);
  Scroller.WindowToInternal(BPoint);
  if ((FMenuHandler = nil) or not FMenuHandler.OnMouseDown(BPoint.X, BPoint.Y, Button, Shift)) and (Button = mbLeft) then
  begin
    CurrentItem:= nil;
    for Cnt:= FGraphics.ItemCount - 1 downto 0 do
      if GrPointInRect(BPoint, FGraphics.Items[Cnt].BoundsRect) then
      begin
        CurrentItem:= @FGraphics.Items[Cnt].BoundsRect;
        DialogPageControl.ActivePage:= DialogPageControl.Pages[3];
        if FGraphics.Items[Cnt].ClassName = 'TProjectPlaceholder' then
        begin
          InternalPageControl.ActivePage:= InternalPageControl.Pages[0];
        end
        else
        if (FGraphics.Items[Cnt].ClassName = 'TLegendPlaceholder') then
        begin
//++ Glukhov Bug#293(PrintFrame) Build#175 15.02.02
//          LegendCombo.ItemIndex:= LegendCombo.Items.IndexOfObject(TObject(FGraphics.Items[Cnt]));
          if (TLegendPlaceholder(FGraphics.Items[Cnt]).LegendName = csLegName4Frame)
          or (TLegendPlaceholder(FGraphics.Items[Cnt]).LegendName = csOleFrame)
          then begin
            btnFrameObj.Checked:= True;
            FrameCombo.ItemIndex:=
                FrameCombo.Items.IndexOfObject(TObject(FGraphics.Items[Cnt]))
          end
          else begin
            btnLegendObj.Checked:= True;
            LegendCombo.ItemIndex:=
                LegendCombo.Items.IndexOfObject(TObject(FGraphics.Items[Cnt]));
          end;
//-- Glukhov Bug#293(PrintFrame) Build#175 15.02.02
          SetFieldsFromLegendRect;
          InternalPageControl.ActivePage:= InternalPageControl.Pages[1];
        end
        else
          if FGraphics.Items[Cnt].ClassName = 'TWindowsGraphic' then
        begin
          with PicturesCombo do
            ItemIndex:= Items.IndexOfObject(TObject(FGraphics.Items[Cnt]));
          SetFieldsFromPictureRect;
          InternalPageControl.ActivePage:= InternalPageControl.Pages[2];
        end
        else
          if FGraphics.Items[Cnt].ClassName = 'TPrintSignature' then
        begin
          with SignaturesCombo do
          begin
            ItemIndex:= Items.IndexOfObject(TObject(FGraphics.Items[Cnt]));
            FSignatureIndex:= ItemIndex;
          end;
          SetFieldsFromSignatureRect;
          InternalPageControl.ActivePage:= InternalPageControl.Pages[3];
        end;
        Break;
      end;
  end;
end;

procedure TPrintingDialog.PreviewMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  APoint: TPoint;
  BPoint: TGrPoint;
  Factor: Integer;
begin
  APoint:= Point(X, Y);
  DPToLP(PreviewWindow.Canvas.Handle, APoint, 1);
  BPoint:= GrPoint(APoint.X, APoint.Y);
  Scroller.WindowToInternal(BPoint);
  if (Button = mbRight) and ZoomBtn.Down then
  begin
    Factor:= 2;
    if ssShift in Shift then
      Factor:= Factor * 2;
    if ssCtrl in Shift then
      Factor:= Factor * 4;
    Scroller.ZoomByFactorWithCenter(BPoint, 1 / Factor);
  end
  else
    if FMenuHandler <> nil then
    FMenuHandler.OnMouseUp(BPoint.X, BPoint.Y, Button, Shift);
end;

procedure TPrintingDialog.EditHandlerMove(Sender: TObject);
var
  intCnt: Integer;
begin
  if FCurrentItem <> nil then
  begin
    Scroller.InvalidateRect(FCurrentItem^);
    if FCurrentItem = @FProjGraphic.BoundsRect then
    begin
      FCurrentItem^:= GrBounds(EditHandler.XPosition, EditHandler.YPosition,
        EditHandler.Width, EditHandler.Height);
      SetFieldsFromProjRect;
      LeftPosBtn.Checked:= TRUE;
    end
    else
      for intCnt:= 0 to FGraphics.ItemCount - 1 do
        if FCurrentItem = @FGraphics.Items[intCnt].BoundsRect then
        begin
          FGraphics.Items[intCnt].PrintPosition:= ppCustom;
          if FGraphics.Items[intCnt].ClassName = 'TPrintSignature' then
          begin
            FGraphics.Items[intCnt].Invalidate(Scroller);
            // set new size and position from edit-frame
            FCurrentItem^:= GrBounds(EditHandler.XPosition, EditHandler.YPosition,
              EditHandler.Width, EditHandler.Height);
            SetFieldsFromSignatureRect;
            SigSizeOrPositionUpdated(Sender);
            FGraphics.Items[intCnt].Invalidate(Scroller);
          end
          else
            {++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
            if FGraphics.Items[intCnt].ClassName = 'TWindowsGraphic' then
          begin
            FGraphics.Items[intCnt].Invalidate(Scroller);
            // set new size and position from edit-frame
            FCurrentItem^:= GrBounds(EditHandler.XPosition, EditHandler.YPosition,
              EditHandler.Width, EditHandler.Height);
            SetFieldsFromPictureRect;
            PicSizeOrPositionUpdated(Sender);
            FGraphics.Items[intCnt].Invalidate(Scroller);
          end
          else
            {-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
          begin
            // set new size and position from edit-frame
            FCurrentItem^:= GrBounds(EditHandler.XPosition, EditHandler.YPosition,
              EditHandler.Width, EditHandler.Height);
            {++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
            //                          if FGraphics.Items[intCnt].ClassName='TWindowsGraphic' then
            //                             SetFieldsFromPictureRect
            //                          else
            {-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
            if FGraphics.Items[intCnt].ClassName = 'TLegendPlaceholder' then
              SetFieldsFromLegendRect;
          end;
          Break;
        end;
    Scroller.InvalidateRect(FCurrentItem^);
    // repaginating Print-Layout, if it is necessary
    if FPrintOptions.PageSetup.PrintLayoutModel <> plmUniquePage then
      if AutoRpBtn.Checked then
        Repagenation;
  end;
end;

procedure TPrintingDialog.EditHandlerResize(Sender: TObject);
var
  intCnt: Integer;
  boolNeedRedraw: Boolean;
  Graphic: TPrintSignature;
begin
  if FCurrentItem <> nil then
  begin
    // invalidate old frame-position
    Scroller.InvalidateRect(FCurrentItem^); {FCurrentItem - pointer to the current printing-rectangle}
    // update edit-fields if current item is the project-rectangle
    if FCurrentItem = @FProjGraphic.BoundsRect then
    begin
// YG: unclear code
//      if btnSetFrameInDlg.Checked then
//      if btnSetFrameInPrj.Checked then
      if not chkKeepScale.Checked then
      begin
        // set new position from edit-frame
        FCurrentItem^:=
          GrBounds(EditHandler.XPosition, EditHandler.YPosition, EditHandler.Width, EditHandler.Height);
        SetFieldsFromProjRect;
      end;
      // set to left-position
      LeftPosBtn.Checked:= TRUE;
      chkFitToPage.Checked:= False;
      // update size-fields
      PrintRangeOrScaleUpdated(Sender);
      if chkKeepScale.Checked then begin
        if not PrintWindowBtn.Checked  then PrintWindowBtn.Checked:= TRUE;
      end
      else begin
        boolNeedRedraw:= FALSE;
        // detect the signature with SCALE field
        for intCnt:= 0 to FGraphics.ItemCount - 1 do
          if FGraphics.Items[intCnt] is TPrintSignature then
          begin
            Graphic:= TPrintSignature(FGraphics.Items[intCnt]);
            if Pos(FieldsCodes[Ord(fcScale)], Graphic.Signature) <> 0 then
            begin
              boolNeedRedraw:= TRUE;
              Break;
            end;
          end;
        if boolNeedRedraw  then PreviewPaint(Sender);
      end
    end
    else
      for intCnt:= 0 to FGraphics.ItemCount - 1 do
        if FCurrentItem = @FGraphics.Items[intCnt].BoundsRect then
        begin
          FGraphics.Items[intCnt].PrintPosition:= ppCustom;
          if FGraphics.Items[intCnt].ClassName = 'TPrintSignature' then
          begin
            FGraphics.Items[intCnt].Invalidate(Scroller);
            // set new position from edit-frame
            FCurrentItem^:= GrBounds(EditHandler.XPosition, EditHandler.YPosition,
              EditHandler.Width, EditHandler.Height);
            with TPrintSignature(FGraphics.Items[intCnt]) do begin
                 TextStyle.Height:= Trunc(Abs(BoundsRect.Top - BoundsRect.Bottom) * 72 / 25.4);
                 TextStyle.Height:= TextStyle.Height / LineCount;
            end;
            SigSizeOrPositionUpdated(FontDlgBtn);
          end
          else
            {++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
            if FGraphics.Items[intCnt].ClassName = 'TWindowsGraphic' then
          begin
            FGraphics.Items[intCnt].Invalidate(Scroller);
            // set new position from edit-frame
            FCurrentItem^:= GrBounds(EditHandler.XPosition, EditHandler.YPosition,
              EditHandler.Width, EditHandler.Height);
            PicSizeOrPositionUpdated(Sender)
          end
          else
            {-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
          begin
            // set new position from edit-frame
            FCurrentItem^:= GrBounds(EditHandler.XPosition, EditHandler.YPosition,
              EditHandler.Width, EditHandler.Height);
            {++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
            //                             if FGraphics.Items[intCnt].ClassName='TWindowsGraphic' then
            //                                SetFieldsFromPictureRect
            //                             else
            {-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
            if FGraphics.Items[intCnt].ClassName = 'TLegendPlaceholder' then
              // update edit-fields if current item is the legend-rectangle
              SetFieldsFromLegendRect;
          end;
          Break;
        end;
    // invalidate current position
    Scroller.InvalidateRect(FCurrentItem^);
  end;
end;

procedure TPrintingDialog.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  //  EditFrame.OnKeyDown(Key,Shift);
end;

procedure TPrintingDialog.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  //  EditFrame.OnKeyUp(Key,Shift);
end;

procedure TPrintingDialog.SetEntryFieldUnits(AUnits: TMeasureUnits);
begin
  LeftVal.Units:= AUnits;
  RightVal.Units:= AUnits;
  TopVal.Units:= AUnits;
  BottomVal.Units:= AUnits;
  InterLeftVal.Units:= AUnits;
  InterRightVal.Units:= AUnits;
  InterTopVal.Units:= AUnits;
  InterBottomVal.Units:= AUnits;
  OverlapLeftVal.Units:= AUnits;
  OverlapRightVal.Units:= AUnits;
  OverlapTopVal.Units:= AUnits;
  OverlapBottomVal.Units:= AUnits;
  LeftPosVal.Units:= AUnits;
  TopPosVal.Units:= AUnits;
  WidthVal.Units:= AUnits;
  HeightVal.Units:= AUnits;
  PageWidthVal.Units:= AUnits;
  PageHeightVal.Units:= AUnits;
  LegWidthVal.Units:= AUnits;
  LegHeightVal.Units:= AUnits;
  LegLeftPosVal.Units:= AUnits;
  LegTopPosVal.Units:= AUnits;
  PicWidthVal.Units:= AUnits;
  PicHeightVal.Units:= AUnits;
  PicLeftPosVal.Units:= AUnits;
  PicTopPosVal.Units:= AUnits;
  SigLeftPosVal.Units:= AUnits;
  SigTopPosVal.Units:= AUnits;
end;

procedure TPrintingDialog.FrameForPagesCheckClick(Sender: TObject);
begin
  if (PagesHorzVal.AsInteger > 1) or (PagesVertVal.AsInteger > 1) then
  begin
    PageSizeUpdated;
    PrintRangeOrScaleUpdated(Sender);
  end;
end;

procedure TPrintingDialog.FormShow(Sender: TObject);
var
  Cnt: Integer;
begin
  UserInterface.BeginWaitCursor;
  try
  if FReinvoke
      then
       RedrawSpecPrintFrame;

    if not FReinvoke then
    begin
      // set default-units depending on the system-settings
      {if GetLocaleStr(GetSystemDefaultLCID, LOCALE_IMEASURE, '0') = '1' then
        SetEntryFieldUnits(muInches)
      else}
        SetEntryFieldUnits(muCentimeters);
      // read print-options from the registry
      ReadPrintOptFromReg;
//++ Glukhov BeginningPage Build#175 11.02.02
      // YG: set the beginning page always when the dialog starts
      DialogPageControl.ActivePage:= DialogPageControl.Pages[0];
//-- Glukhov BeginningPage Build#175 11.02.02
    end;
    // fill view-listbox
    with FProject^.Sights^ do
    begin
      ViewsCombo.Clear;
      for Cnt:= 0 to Count - 1 do
        if not RectEmpty(PSight(At(Cnt))^.ViewRect) then
          ViewsCombo.Items.Add(PToStr(PSight(At(Cnt))^.Name));
    end;
    // set print-options
    SetPrintOptions(FPrintOptions);
    FReinvoke:= FALSE;

    PrintToWMFCheck.Enabled:=(WingisLicense >= WG_STD);
    if not PrintToWMFCheck.Enabled then begin
       PrintToWMFCheck.Checked:=False;
    end;

    FrameAddBrn.Enabled:=(WingisLicense >= WG_PROF);
    ButtonOle.Enabled:=(WingisLicense >= WG_PROF);
    InsertOLEBtn.Enabled:=(WingisLicense >= WG_PROF);
    
  finally
    UserInterface.EndWaitCursor;
  end;
end;

{++ Moskaliov AXWinGIS Printing Methods BUILD#127 05.09.00}

procedure TPrintingDialog.FormLoad(Sender: TObject);
var
  Cnt: Integer;
begin
  // set default-units depending on the system-settings
  {if GetLocaleStr(GetSystemDefaultLCID, LOCALE_IMEASURE, '0') = '1' then
    SetEntryFieldUnits(muInches)
  else}
    SetEntryFieldUnits(muCentimeters);
  // read print-options from the registry
  ReadPrintOptFromReg;
  // fill view-listbox
  with FProject^.Sights^ do
  begin
    ViewsCombo.Clear;
    for Cnt:= 0 to Count - 1 do
      if not RectEmpty(PSight(At(Cnt))^.ViewRect) then
        ViewsCombo.Items.Add(PToStr(PSight(At(Cnt))^.Name));
  end;
  // set print-options
  SetPrintOptions(FPrintOptions);
end;

procedure TPrintingDialog.FormStore(Sender: TObject);
begin
  CurrentItem:= nil;
  //   GetPrintOptions;
  with FProject^, Registry do
  begin
    CurrentPath:= '\User\WinGISUI\PrintingDialog';
    FPrintOptions.WriteToRegistry(Registry);
    WriteInteger('ActivePage', FPageIndex);
    WriteInteger('ActiveSubPage', FSubPageIndex);
    WriteInteger('LegendIndex', LegendCombo.ItemIndex);
    WriteInteger('SignatureIndex', SignaturesCombo.ItemIndex);
    WriteInteger('PictureIndex', PicturesCombo.ItemIndex);
    // write history-lists
    WriteHistory(Registry, 'ScaleHistory', ScaleVal);
  end;
end;

procedure TPrintingDialog.ReadPrintOptFromReg;
var
  Cnt: Integer;
begin
  // read print-options from the registry
  with FProject^, Registry do
  begin
    CurrentPath:= '\User\WinGISUI\PrintingDialog';
    FPrintOptions.ReadFromRegistry(Registry);
    FPageIndex:= ReadInteger('ActivePage');
    if (FPageIndex < 0) or (FPageIndex > DialogPageControl.PageCount - 1) then
      FPageIndex:= 0;
    DialogPageControl.ActivePage:= DialogPageControl.Pages[FPageIndex];
    FSubPageIndex:= ReadInteger('ActiveSubPage');
    if (FSubPageIndex < 0) or (FSubPageIndex > InternalPageControl.PageCount - 1) then
      FSubPageIndex:= 0;
    InternalPageControl.ActivePage:= InternalPageControl.Pages[FSubPageIndex];
    FillDialogComboBoxes;
    Cnt:= ReadInteger('LegendIndex');
    if (Cnt < -1) or (Cnt >= LegendCombo.Items.Count)
    then  Cnt:= LegendCombo.Items.Count - 1;
    LegendCombo.ItemIndex:= Cnt;
    Cnt:= ReadInteger('SignatureIndex');
    if (Cnt < -1) or (Cnt >= SignaturesCombo.Items.Count)
    then  Cnt:= SignaturesCombo.Items.Count - 1;
    SignaturesCombo.ItemIndex:= Cnt;
    FSignatureIndex:= SignaturesCombo.ItemIndex;
    Cnt:= ReadInteger('PictureIndex');
    if (Cnt < -1) or (Cnt >= PicturesCombo.Items.Count)
    then  Cnt:= PicturesCombo.Items.Count - 1;
    PicturesCombo.ItemIndex:= Cnt;
    // read history-lists
    ReadHistory(Registry, 'ScaleHistory', ScaleVal);
    // save current view and layer-settings
    FSight:= New(PSight, Init('', PInfo^.GetCurrentScreen));
    FSight^.SetLayers(Layers);
    chkPrintPageNum.Checked:=ReadBool('PrintPageNum');
  end;
end;
{-- Moskaliov AXWinGIS Printing Methods BUILD#127 05.09.00}

procedure TPrintingDialog.FillDialogComboBoxes;
var
  Cnt           : Integer;
  boolProjDetect: Boolean;
  sTmp          : String;   // Glukhov Bug#293(PrintFrame) Build#175 09.02.02
begin
  boolProjDetect:= FALSE;
//++ Glukhov Bug#293(PrintFrame) Build#175 09.02.02
  // Prepare counter for the frames combo
  FGraphics.FramesCount:= 0;
//-- Glukhov Bug#293(PrintFrame) Build#175 09.02.02
  for Cnt:= 0 to FGraphics.ItemCount - 1 do
    if FGraphics.Items[Cnt].ClassName = 'TPrintSignature' then
      // fill the signatures-combo
      SignaturesCombo.Items.AddObject(TPrintSignature(FGraphics.Items[Cnt]).Signature, TObject(FGraphics.Items[Cnt]))
    else if FGraphics.Items[Cnt].ClassName = 'TWindowsGraphic' then
      // fill the pictures-combo
      PicturesCombo.Items.AddObject(TWindowsGraphic(FGraphics.Items[Cnt]).FileName, TObject(FGraphics.Items[Cnt]))
//++ Glukhov Bug#293(PrintFrame) Build#175 09.02.02
    else if FGraphics.Items[Cnt].ClassName = 'TLegendPlaceholder' then begin
      // fill the legendes-combo
//      LegendCombo.Items.AddObject(TLegendPlaceholder(FGraphics.Items[Cnt]).LegendName, TObject(FGraphics.Items[Cnt]))
      if (TLegendPlaceholder(FGraphics.Items[Cnt]).LegendName = csLegName4Frame)
      or (TLegendPlaceholder(FGraphics.Items[Cnt]).LegendName = csOleFrame)
      then begin // fill up the frames combo
        FGraphics.FramesCount:= FGraphics.FramesCount + 1;
        if FGraphics.FramesCount < 10
        then  sTmp:= FGraphics.FrameName + '0' + IntToStr(FGraphics.FramesCount)
        else  sTmp:= FGraphics.FrameName + IntToStr(FGraphics.FramesCount);
        FrameCombo.Items.AddObject( sTmp, TObject(FGraphics.Items[Cnt]) );
        btnFrameObj.Checked:= True;
      end
      else begin // fill up the legends combo
        LegendCombo.Items.AddObject(
            TLegendPlaceholder(FGraphics.Items[Cnt]).LegendName,
            TObject(FGraphics.Items[Cnt]) );
        btnLegendObj.Checked:= True;
      end;
    end
//-- Glukhov Bug#293(PrintFrame) Build#175 09.02.02
    else if FGraphics.Items[Cnt].ClassName = 'TProjectPlaceholder' then
    begin
      // detect the project placeholder
      FProjGraphic:= TProjectPlaceholder(FGraphics.Items[Cnt]);
      SetFieldsFromProjRect;
      boolProjDetect:= TRUE;
    end;
  FUpdating:= boolProjDetect; // FUpdating will be returned to FALSE after SetPrintOptions() calling
  if not boolProjDetect then begin
    FProjGraphic:= TProjectPlaceholder.Create(FGraphics);
  end;
  with FProjGraphic do
  begin
    ScaleVal.AsFloat:= PrintScale;
//++ Glukhov Bug#560 Build#171 15.01.02
    FPrintRect:= FPrintOptions.PrintRect;
    AdjustPrintRectToBounds;
    FPrintOptions.PrintRect:= FPrintRect;
//-- Glukhov Bug#560 Build#171 15.01.02

    if PrintPosition = ppCenter
    then  CenterPageBtn.Checked:= TRUE
    else  LeftPosBtn.Checked:= TRUE;

    case PrintRange of
      prSavedView:    PrintSavedViewBtn.Checked:= TRUE;
      prProject:      PrintAllBtn.Checked:= TRUE;
//      prCurrentView:  PrintCurrentViewBtn.Checked:= TRUE;
      prWindow:       PrintWindowBtn.Checked:= TRUE;
    else
      PrintCurrentViewBtn.Checked:= TRUE;
    end;

    case PrintSize of
      psFitToPage: begin
        chkKeepScale.Checked:= False;
        chkFitToPage.Checked:= True;
      end;
      psWidthAndHeight: begin
        chkKeepScale.Checked:= False;
        chkFitToPage.Checked:= False;
      end;
      psScale: begin
        chkKeepScale.Checked:= True;
        chkFitToPage.Checked:= False;
      end;
    else
      // psScaleAndFit:
      chkKeepScale.Checked:= True;
      chkFitToPage.Checked:= True;
    end;

    ProjFrameCheckBox.Checked:= PrintProjFrame;
  end;
end;

procedure TPrintingDialog.FormDestroy(Sender: TObject);
var
TmpLayer: PLayer;
begin
 try
  FPrintOptions.Free;
  FPInfo.DoneCopy;
  if FSight <> nil then
    Dispose(FSight, Done);
  Application.OnActivate:= FOrgAppActivate;
 finally
  TmpLayer:=FProject.Layers.NameToLayer(PrintFrameLayerName);
  if Tmplayer<>nil then begin
        Screen.Cursor:=crHourGlass;
        ProcDeleteLayer(FProject,TmpLayer);
        Fproject.UpdatePaintOffset;
        Screen.Cursor:=crDefault;
  end;
 end;
end;

{******************************************************************************+
  Procedure TPrintingDialog.FormHide
--------------------------------------------------------------------------------
  Called by the VCL when teh form was hidden. If the modalresult is not equal
  to mrCancel the printing-options are updated. if modalresult is mrOK the
  settings are stored to the registry of the project.
+******************************************************************************}

procedure TPrintingDialog.FormHide(Sender: TObject);
var TmpLayer:Player;
begin
  CurrentItem:= nil;
  if ModalResult <> mrCancel then
  begin
    GetPrintOptions;
    if ModalResult in [mrOK, mrNo] then
      with FProject^, Registry do
      begin
        CurrentPath:= '\User\WinGISUI\PrintingDialog';
        FPrintOptions.WriteToRegistry(Registry);
        WriteInteger('ActivePage', FPageIndex);
        WriteInteger('ActiveSubPage', FSubPageIndex);
        WriteInteger('LegendIndex', LegendCombo.ItemIndex);
        WriteInteger('SignatureIndex', SignaturesCombo.ItemIndex);
        WriteInteger('PictureIndex', PicturesCombo.ItemIndex);
        WriteBool('PrintPageNum',chkPrintPageNum.Checked);
        // write history-lists
        WriteHistory(Registry, 'ScaleHistory', ScaleVal);
//++ Glukhov SavePrintDlg Build#175 15.02.02
        if (ModalResult = mrNo) then SetModified;   // Mark Project as modified
//-- Glukhov SavePrintDlg Build#175 15.02.02
      end;
    if (ModalResult <> mrYes) and (FSight <> nil) and FSightChanged
    then  ProcSetSight(FProject, FSight, FALSE);
  end;

  {brovak}
  if ModalResult in [mrNo,mrYes,mrCancel] then begin
     TmpLayer:=FProject.Layers.NameToLayer(PrintFrameLayerName);
     if Tmplayer<>nil then begin
        Screen.Cursor:=crHourGlass;
        ProcDeleteLayer(FProject,TmpLayer);
        Fproject.UpdatePaintOffset;
        Screen.Cursor:=crDefault;
     end;
  end;

  {brovak}
end;

procedure TPrintingDialog.DrawPicture(Graphic: TWindowsGraphic);
var
  ARect: TRect;
  TempColor: TColor;
  {++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
  LastPage: Integer;
  BaseRect: TGrRect;
  CntX: Integer;
  CntY: Integer;
  IndX: Integer;
  IndY: Integer;
  dblMax: Double;
  {-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
begin
  if not RectEmpty(Graphic.BoundsRect) then
    with PreviewWindow, Canvas do
    begin
      if Graphic.Initialised then
        if PreviewBtn.Down then
          {++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
          with Graphic, Graphic.Owner.PageSetup do
            if (PrintLayoutModel = plmBook) and (Graphic.PrintLocation <> plOneTime) then
            begin
              // finding Page Extent which contains Picture by MAX intersection square
              dblMax:= GetPageBaseExtent(BaseRect, IndX, IndY);
              if DblCompare(dblMax, 0) <> 0 then
              begin
                // Text rectangle projection on the Page Extent [0,0]
                BaseRect.Left:= BoundsRect.Left - PageExtents[IndX, IndY].Left;
                BaseRect.Bottom:= BoundsRect.Bottom - PageExtents[IndX, IndY].Bottom;
                BaseRect.Right:= BaseRect.Left + RectWidth(BoundsRect);
                BaseRect.Top:= BaseRect.Bottom + RectHeight(BoundsRect);
                with Owner do
                begin
                  CurrPrintPage:= PageSetup.StartPageNum;
                  LastPage:= StartPageNum + HorizontalPages * VerticalPages - 1;
                  if PagesOrder then // From left to right and down
                    for CntY:= VerticalPages - 1 downto 0 do
                      for CntX:= 0 to HorizontalPages - 1 do
                      begin
                        Scroller.InternalToWindow(PageExtents[CntX, CntY], ARect);
                        CurrPageClipRect:= ARect;
                        if OnFirstPage and OnInternalPages and OnLastPage then
                          Graphic.Paint(Scroller)
                        else
                          if CurrPrintPage = StartPageNum then
                        begin
                          if OnFirstPage then
                            Graphic.Paint(Scroller);
                        end
                        else
                          if CurrPrintPage = LastPage then
                        begin
                          if OnLastPage then
                            Graphic.Paint(Scroller);
                        end
                        else
                        begin
                          if OnInternalPages then
                            Graphic.Paint(Scroller);
                        end;
                        CurrPrintPage:= CurrPrintPage + 1;
                      end
                  else // From top to bottom and right
                    for CntX:= 0 to HorizontalPages - 1 do
                      for CntY:= VerticalPages - 1 downto 0 do
                      begin
                        Scroller.InternalToWindow(PageExtents[CntX, CntY], ARect);
                        CurrPageClipRect:= ARect;
                        if OnFirstPage and OnInternalPages and OnLastPage then
                          Graphic.Paint(Scroller)
                        else
                          if CurrPrintPage = StartPageNum then
                        begin
                          if OnFirstPage then
                            Graphic.Paint(Scroller);
                        end
                        else
                          if CurrPrintPage = LastPage then
                        begin
                          if OnLastPage then
                            Graphic.Paint(Scroller);
                        end
                        else
                        begin
                          if OnInternalPages then
                            Graphic.Paint(Scroller);
                        end;
                        CurrPrintPage:= CurrPrintPage + 1;
                      end;
                end; // End of the "with Owner do" statement
              end;
              Exit;
            end
            else
              {-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
            begin
              {++ Moskaliov Pictures Location Control BUILD#131 25.09.00}
              ARect.Left:= 0;
              ARect.Top:= 0;
              ARect.Right:= 0;
              ARect.Bottom:= 0;
              Graphic.Owner.CurrPageClipRect:= ARect;
              {-- Moskaliov Pictures Location Control BUILD#131 25.09.00}
              Graphic.Paint(Scroller);
              Exit;
            end
          else
            TempColor:= clOlive
        else
          TempColor:= clRed;
      Brush.Style:= bsFDiagonal;
      Brush.Color:= TempColor;
      Pen.Style:= psSolid;
      Pen.Color:= TempColor;
      SetBKColor(Handle, ColorToRGB(clWindow));
      SetBKMode(Handle, TRANSPARENT);
      Scroller.InternalToWindow(Graphic.BoundsRect, ARect);
      LPToDP(Handle, ARect, 2);
      Inc(ARect.Right); Inc(ARect.Bottom);
      DPToLP(Handle, ARect, 2);
      with ARect do
        Rectangle(Left, Top, Right, Bottom);
    end;
end;

procedure TPrintingDialog.DrawSignature(Graphic: TPrintSignature);
var
  ARect: TRect;
  BRect: TRect;
  TempRect: TGrRect;
  BaseRect: TGrRect;
  TempColor: TColor;
  dblHeight: Double;
  dblMax: Double;
  Pos: TPoint;
  CntX: Integer;
  CntY: Integer;
  IndX: Integer;
  IndY: Integer;
  AText: String;
  LCnt,i: Integer;

  //   internal function
  function IntersectTRect: TRect;
  begin
    Result:= ARect;
    if ARect.Left < BRect.Left  then Result.Left:= BRect.Left;
    if ARect.Right > BRect.Right  then Result.Right:= BRect.Right;
//++ Glukhov PrintSignature Build#166 17.01.02
// When Top < Bottom, Top is like Left
//    if ARect.Top > BRect.Top  then Result.Top:= BRect.Top;
    if ARect.Top < BRect.Top  then Result.Top:= BRect.Top;
//-- Glukhov PrintSignature Build#166 17.01.02
    if ARect.Bottom > BRect.Bottom  then Result.Bottom:= BRect.Bottom;
  end; // End of the internal function IntersectTRect

begin // Begining of the procedure TPrintingDialog.DrawSignature
  if not RectEmpty(Graphic.BoundsRect) then
    with PreviewWindow.Canvas do begin
      if Graphic.Initialised then
        if PreviewBtn.Down then
        begin
          with Graphic do
          begin
            // set font settings
            Font.Name:= TextStyle.FontName;
//++ Glukhov PrintSignature Build#166 17.01.02
// Font.Size should be negative (excluding the internal leading)
//            Font.Size:= Round(TextStyle.Height);
            Font.Size:= -Trunc(Abs(TextStyle.Height));
            // Calculate Font.Height, taking in account the window scale
            dblHeight:= (Font.Size * 25.4) / 72; // Font height in mm
            dblHeight:= Scroller.InternalToWindow(dblHeight);
            Font.Height:= -Round(dblHeight);
//-- Glukhov PrintSignature Build#166 17.01.02
            Font.Color:= Graphic.ForeColor;
            Font.Style:= [];
            if TextStyle.Italic = 1 then
              Font.Style:= Font.Style + [fsItalic];
            if TextStyle.Bold = 1 then
              Font.Style:= Font.Style + [fsBold];
            if TextStyle.Underlined = 1 then
              Font.Style:= Font.Style + [fsUnderline];
            Brush.Style:= bsClear;
            if TextStyle.Transparent = 1
            then  SetBKMode(Handle, TRANSPARENT)
            else  SetBKMode(Handle, OPAQUE);
            SetBKColor(Handle, ColorToRGB(BackColor));
            Pen.Color:= Graphic.ForeColor;
            Pen.Style:= psSolid;

          LCnt:=Graphic.LineCount;
          for i:=0 to LCnt-1 do begin

            AText:=Graphic.GetLine(i);

            with Owner.PageSetup do
              if (PrintLayoutModel = plmBook) and (PrintLocation <> plOneTime) then
              begin
                {++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
                // finding Page Extent which contains Text by MAX intersection square
                dblMax:= GetPageBaseExtent(TempRect, IndX, IndY);
                {-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
                if DblCompare(dblMax, 0) <> 0 then
                begin
                  // Text rectangle projection on the Page Extent [0,0]
                  BaseRect.Left:= BoundsRect.Left - PageExtents[IndX, IndY].Left;
                  BaseRect.Bottom:= BoundsRect.Bottom - PageExtents[IndX, IndY].Bottom;
                  BaseRect.Right:= BaseRect.Left + RectWidth(BoundsRect);
                  BaseRect.Top:= BaseRect.Bottom + RectHeight(BoundsRect);
                  with Owner do
                    CurrPrintPage:= PageSetup.StartPageNum;
                  if PagesOrder then // From left to right and down
                    for CntY:= VerticalPages - 1 downto 0 do
                      for CntX:= 0 to HorizontalPages - 1 do
                      begin
                        TempRect.Left:= BaseRect.Left + PageExtents[CntX, CntY].Left;
                        TempRect.Bottom:= BaseRect.Bottom + PageExtents[CntX, CntY].Bottom;
                        TempRect.Right:= TempRect.Left + RectWidth(BaseRect);
                        TempRect.Top:= TempRect.Bottom + RectHeight(BaseRect);
                        Scroller.InternalToWindow(TempRect, ARect);
                        // qualify the text rectangle
                        case TextStyle.Alignment of
                          Ord(taRight): ARect.Left:= ARect.Right - TextWidth(AText);
                          Ord(taCentered): begin
                              ARect.Left:= (ARect.Left + ARect.Right - TextWidth(AText)) div 2;
                              ARect.Right:= ARect.Left + TextWidth(AText);
                            end;
                        else { taLeft }
                          ARect.Right:= ARect.Left + TextWidth(AText);
                        end;
                        if Graphic.PrintOrigin = poTop then
                          ARect.Bottom:= ARect.Top - TextHeight(AText)
                        else
                          ARect.Top:= ARect.Bottom + TextHeight(AText);

                        ARect.Top:=ARect.Top + Trunc(dblHeight*i);

                        Pos.x:= ARect.Left;
                        Pos.y:= ARect.Top;
                        TempRect:= PageExtents[CntX, CntY];
                        if CntX > 0 then
                          TempRect.Left:= PageExtents[CntX - 1, CntY].Right;
                        if CntY > VerticalPages - 1 then
                          TempRect.Top:= PageExtents[CntX, CntY + 1].Bottom;
                        Scroller.InternalToWindow(TempRect, BRect);
                        ARect:= IntersectTRect;
                        if OnFirstPage and OnInternalPages and OnLastPage
                        then  TextRect(ARect, Pos.x, Pos.y, AText)
                        else with Owner, Owner.PageSetup do begin
                          if CurrPrintPage = StartPageNum then
                          begin
                            if OnFirstPage then TextRect(ARect, Pos.x, Pos.y, AText);
                          end
                          else if CurrPrintPage = StartPageNum - 1 + HorizontalPages * VerticalPages then
                          begin
                            if OnLastPage  then TextRect(ARect, Pos.x, Pos.y, AText);
                          end
                          else begin
                            if OnInternalPages  then TextRect(ARect, Pos.x, Pos.y, AText);
                          end;
                        end;
                        with Owner do CurrPrintPage:= CurrPrintPage + 1;
                      end
                  else // From top to bottom and right
                    for CntX:= 0 to HorizontalPages - 1 do
                      for CntY:= VerticalPages - 1 downto 0 do
                      begin
                        TempRect.Left:= BaseRect.Left + PageExtents[CntX, CntY].Left;
                        TempRect.Bottom:= BaseRect.Bottom + PageExtents[CntX, CntY].Bottom;
                        TempRect.Right:= TempRect.Left + RectWidth(BaseRect);
                        TempRect.Top:= TempRect.Bottom + RectHeight(BaseRect);
                        Scroller.InternalToWindow(TempRect, ARect);
                        // qualify the text rectangle
                        case TextStyle.Alignment of
                          Ord(taRight): ARect.Left:= ARect.Right - TextWidth(AText);
                          Ord(taCentered): begin
                              ARect.Left:= (ARect.Left + ARect.Right - TextWidth(AText)) div 2;
                              ARect.Right:= ARect.Left + TextWidth(AText);
                            end;
                        else { taLeft }
                          ARect.Right:= ARect.Left + TextWidth(AText);
                        end;
                        if Graphic.PrintOrigin = poTop then
                          ARect.Bottom:= ARect.Top - TextHeight(AText)
                        else
                          ARect.Top:= ARect.Bottom + TextHeight(AText);

                        ARect.Top:=ARect.Top + Trunc(dblHeight*i);

                        Pos.x:= ARect.Left;
                        Pos.y:= ARect.Top;
                        TempRect:= PageExtents[CntX, CntY];
                        if CntX > 0 then
                          TempRect.Left:= PageExtents[CntX - 1, CntY].Right;
                        if CntY > VerticalPages - 1 then
                          TempRect.Top:= PageExtents[CntX, CntY + 1].Bottom;
                        Scroller.InternalToWindow(TempRect, BRect);
                        ARect:= IntersectTRect;
                        if OnFirstPage and OnInternalPages and OnLastPage then
                          TextRect(ARect, Pos.x, Pos.y, AText)
                        else
                          with Owner, Owner.PageSetup do
                            if CurrPrintPage = StartPageNum then
                            begin
                              if OnFirstPage then
                                TextRect(ARect, Pos.x, Pos.y, AText);
                            end
                            else
                              if CurrPrintPage = StartPageNum - 1 + HorizontalPages * VerticalPages then
                            begin
                              if OnLastPage then
                                TextRect(ARect, Pos.x, Pos.y, AText);
                            end
                            else
                            begin
                              if OnInternalPages then
                                TextRect(ARect, Pos.x, Pos.y, AText);
                            end;
                        with Owner do
                          CurrPrintPage:= CurrPrintPage + 1;
                      end;
                end; // End of dblMax compare with ZERO
              end
              else begin
                with Owner do
                  if PagesOrder
                  then // From left to right and down
                    CurrPrintPage:= StartPageNum + IndX + HorizontalPages * (VerticalPages - IndY - 1)
                  else // From top to bottom and right
                    CurrPrintPage:= StartPageNum + (VerticalPages - IndY - 1) + VerticalPages * IndX;

                Scroller.InternalToWindow(BoundsRect, ARect);

                ARect.Top:=ARect.Top + Trunc(dblHeight*i);

                Pos.x:= ARect.Left;
                Pos.y:= ARect.Top;
                TextRect(ARect, Pos.x, Pos.y, AText);
              end;
            end;
          end;
          Exit;
        end
        else
          TempColor:= clPurple
      else
        TempColor:= clRed;
      Brush.Style:= bsBDiagonal;
      Brush.Color:= TempColor;
      Pen.Style:= psSolid;
      Pen.Color:= TempColor;
      SetBKColor(Handle, ColorToRGB(clWindow));
      SetBKMode(Handle, TRANSPARENT);
      Scroller.InternalToWindow(Graphic.BoundsRect, ARect);
      LPToDP(Handle, ARect, 2);
      Inc(ARect.Right); Inc(ARect.Bottom);
      DPToLP(Handle, ARect, 2);
      with ARect do
        Rectangle(Left, Top, Right, Bottom);
    end;
end;

//++ Glukhov Bug#293(PrintFrame) Build#175 15.02.02
procedure TPrintingDialog.DrawLegend(Graphic: TLegendPlaceholder);
var
  ARect       : TRect;
  LegendRect  : TDRect;
  Legend      : TLegend;
  CurX, CurY  : Double;
  TempColor   : TColor;
  longX       : LongInt;
  longY       : LongInt;
  OldMapMode  : TMapMode;
begin
  if RectEmpty(Graphic.BoundsRect) then Exit;
  with PreviewWindow, Canvas do begin
//++ Glukhov Non-existentLegend Build#171 18.01.02
// A non-existent legend should be shown as not Initialised
//      if Graphic.Initialised then
    if (Graphic.Initialised and (Graphic.LegendIndex>=0))
    or (Graphic.LegendName = csLegName4Frame)
    or (Graphic.LegendName = csOleFrame) then begin
//-- Glukhov Non-existentLegend Build#171 18.01.02

      if Graphic.LegendName = csOleFrame then begin
         if Graphic.OlePath <> '' then begin
            if Graphic.OleObj.State = osEmpty then begin
               Graphic.LoadOLEFile(PreviewWindow,Graphic.OlePath,StrPas(Fproject^.FName));
               LabelOLE.Caption:=Graphic.OlePath;
            end;
         end;
      end;

      if PreviewBtn.Down and (Graphic.LegendName <> csLegName4Frame)
      then  with FProject^ do  begin
      // Draw a legend which is not a frame
          // PInfo initialisation
          FPInfo.ExtCanvas.Handle:= Handle;
          FPInfo.WMPaint:= FALSE;
          FPInfo.DisplayScale:= Scroller.Scale / 10;
          FPInfo.SetViewRotation(0);
          FPInfo.Zoom:= FPInfo.ScaleToZoom(ScaleVal.AsFloat);
          // Zeichenbereich in Fensterkoordinaten berechnen
          Scroller.InternalToWindow(Graphic.BoundsRect.Left, Graphic.BoundsRect.Bottom, CurX, CurY);
          ARect.Left:= Round(CurX);
          ARect.Bottom:= Round(CurY);
          ARect.Right:= Round(ARect.Left + Scroller.InternalToWindow(RectWidth(Graphic.BoundsRect)));
          ARect.Top:= Round(ARect.Bottom + Scroller.InternalToWindow(RectHeight(Graphic.BoundsRect)));
          // RepaintArea und Offsets berechnen
          FPInfo.XOffset:= -ARect.Left;
          FPInfo.YOffset:= -ARect.Bottom;
          with FPInfo.ExtCanvas do begin
            Push;
            PushClipping;
            try
              BeginPaint;
              try
                  if Graphic.LegendIndex >= 0 then begin
                     SetClippingRect(ARect);
                     ClipRect:= ARect;
                     longX:= FPInfo.CalculateDraw(Round(10 * FPInfo.DisplayScale * RectWidth(Graphic.BoundsRect)));
                     longY:= FPInfo.CalculateDraw(Round(10 * FPInfo.DisplayScale * RectHeight(Graphic.BoundsRect)));
                     LegendRect.Assign(0, 0, Abs(longX), Abs(longY));
                     Legend:= Legends[Graphic.LegendIndex];
                     Legend.SetScaleVal(ScaleVal.AsFloat);
                     Legend.Draw(@FPInfo, Layers, LegendRect);
                  end
                  else begin
                     if Graphic.OleObj.State <> osEmpty then begin
                        OldMapMode:=MapMode;
                        try
                           LPToDP(PreviewWindow.Canvas.Handle,ARect,2);
                           MapMode:=mmText;
                           SetClippingRect(ARect);
                           Graphic.OleObj.Width:=Abs(ARect.Right-ARect.Left)+1;
                           Graphic.OleObj.Height:=Abs(ARect.Bottom-ARect.Top)+1;
                           Graphic.OleObj.PaintTo(PreviewWindow.Canvas.Handle,ARect.Left,ARect.Top);
                        finally
                           MapMode:=OldMapMode;
                        end;
                     end;
                  end;
              finally
                  EndPaint;
              end;
            finally
              PopClipping;
              Pop;
            end;
          end;
      end
      else begin
          Graphic.OleObj.Visible:=False;
      end;

      if (PreviewBtn.Down or (Graphic.LegendName = csLegName4Frame))
      then begin
      // Draw a frame
          if (Graphic.LegendName <> csOleFrame) then begin
             Pen.Style:= psSolid;
             Pen.Color:= clBlack;
             Scroller.InternalToWindow(Graphic.BoundsRect, ARect);
             if (Graphic.BitFlags and cwPrnTempFrmBrdLeft)<>0 then begin
                MoveTo( ARect.Left, ARect.Bottom );
                LineTo( ARect.Left, ARect.Top );
             end;
             if (Graphic.BitFlags and cwPrnTempFrmBrdTop)<>0 then begin
                MoveTo( ARect.Left, ARect.Top );
                LineTo( ARect.Right, ARect.Top );
             end;
             if (Graphic.BitFlags and cwPrnTempFrmBrdRight)<>0 then begin
                MoveTo( ARect.Right, ARect.Top );
                LineTo( ARect.Right, ARect.Bottom );
             end;
             if (Graphic.BitFlags and cwPrnTempFrmBrdBottom)<>0 then begin
                MoveTo( ARect.Right, ARect.Bottom );
                LineTo( ARect.Left, ARect.Bottom );
             end;
             Exit;
          end
          else if Graphic.OleObj.State = osEmpty then begin
               TempColor:= clGreen;
          end
          else begin
               exit;
          end;
      end   // if PreviewBtn.Down
      else  TempColor:= clGreen;
    end
    else begin
      TempColor:= clRed;
    end;

    Brush.Style:= bsVertical;
    Brush.Color:= TempColor;
    Pen.Style:= psSolid;
    Pen.Color:= TempColor;
    SetBKColor(Handle, ColorToRGB(clWindow));
    SetBKMode(Handle, TRANSPARENT);
    Scroller.InternalToWindow(Graphic.BoundsRect, ARect);
    LPToDP(Handle, ARect, 2);
    Inc(ARect.Right); Inc(ARect.Bottom);
    DPToLP(Handle, ARect, 2);
    with ARect do Rectangle(Left, Top, Right, Bottom);
  end;
end;
//-- Glukhov Bug#293(PrintFrame) Build#175 15.02.02

procedure TPrintingDialog.DrawPreview(Graphic: TProjectPlaceholder);
var
  ARect: TRect;
  Rect: TRotRect;
  CurX: Double;
  CurY: Double;
begin
  with PreviewWindow, Canvas do
    if not PreviewBtn.Down then
    begin
      if not RectEmpty(Graphic.BoundsRect) then
      begin
        Brush.Style:= bsHorizontal;
        Brush.Color:= clBlue;
        Pen.Style:= psSolid;
        Pen.Color:= clBlue;
        SetBKColor(Handle, ColorToRGB(clWindow));
        SetBKMode(Handle, TRANSPARENT);
        Scroller.InternalToWindow(Graphic.BoundsRect, ARect);
        LPToDP(Handle, ARect, 2);
        Inc(ARect.Right); Inc(ARect.Bottom);
        DPToLP(Handle, ARect, 2);
        with ARect do
          Rectangle(Left, Top, Right, Bottom);
      end;
    end
    else
      with FProject^ do
      begin
        { PInfo initialisieren }
        FPInfo.ExtCanvas.Handle:= Handle;
        FPInfo.WMPaint:= TRUE;
        FPInfo.DisplayScale:= Scroller.Scale / 10;
        FPInfo.SetViewRotation(-FPrintRect.Rotation);
        { Zeichenbereich in Fensterkkordinaten berechnen }
        Scroller.InternalToWindow(Graphic.BoundsRect.Left, Graphic.BoundsRect.Bottom, CurX, CurY);
        ARect.Left:= Round(CurX);
        ARect.Bottom:= Round(CurY);
        ARect.Right:= Round(ARect.Left + Scroller.InternalToWindow(RectWidth(Graphic.BoundsRect)));
        ARect.Top:= Round(ARect.Bottom + Scroller.InternalToWindow(RectHeight(Graphic.BoundsRect)));
        { Zeichenbereich in Projektkoordinaten und Zoomfaktor berechnen }
        Rect:= RotatedRect(FPrintRect, FPInfo.ViewRotation);
        FPInfo.Zoom:= FPInfo.ScaleToZoom(ScaleVal.AsFloat);
        { RepaintArea und Offsets berechnen }
        with IsoFrame(FPrintRect) do
          FPInfo.RepaintArea.Assign(LimitToLong(Left), LimitToLong(Bottom), LimitToLong(Right), LimitToLong(Top));
        FPInfo.XOffset:= FPInfo.CalculateDispDouble(Rect.X) - ARect.Left;
        FPInfo.YOffset:= FPInfo.CalculateDispDouble(Rect.Y) - ARect.Bottom;
        with FPInfo.ExtCanvas do
        begin
          Push;
          PushClipping;
          try
            BeginPaint;
            try
              SetClippingRect(ARect);
              ClipRect:= ARect;
              { Project drawing }
              DrawGraphic(@FPInfo);
            finally
              EndPaint;
            end;
          finally
            Pop;
            PopClipping;
          end;
        end;
        {++ Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
        if FProjGraphic.PrintProjFrame then // Draw the Project Frame
        begin
          Pen.Style:= psSolid;
          Pen.Color:= clBlack;
          Brush.Color:= clNone;
          Brush.Style:= bsClear;
          Scroller.InternalToWindow(FProjGraphic.BoundsRect, ARect);
          LPToDP(Handle, ARect, 2);
          Inc(ARect.Right); Inc(ARect.Bottom);
          DPToLP(Handle, ARect, 2);
          SetBKColor(Handle, ColorToRGB(clNone));
          SetBKMode(Handle, TRANSPARENT);
          with ARect do
            Rectangle(Left, Bottom, Right, Top);
          Brush.Color:= clWindow;
        end;
        {-- Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
      end;
end;

procedure TPrintingDialog.ShowPreviewCheckClick(Sender: TObject);
var
  Cnt: Integer;
begin
  // view-settings may not be set till now
  UpdatePrintView;
  // project-, legend-, picture- and signature-areas must be redrawn
  for Cnt:= 0 to FGraphics.ItemCount - 1 do
    FGraphics.Items[Cnt].Invalidate(Scroller);
end;

procedure TPrintingDialog.FormResize(Sender: TObject);
begin
  PageSizeUpdated;
  PrintRangeOrScaleUpdated(Sender);
end;

procedure TPrintingDialog.EditChanged(Sender: TObject);
begin
  FKeyPressed:= TRUE;
end;

procedure TPrintingDialog.TemplateOpenBtnClick(Sender: TObject);
begin
  if OpenTemplateDialog.Execute then
  begin
    if CurrentItem <> nil  then CurrentItem:= nil;
    LegendCombo.Clear;
    PicturesCombo.Clear;
    SignaturesCombo.Clear;
    FGraphics.Clear;
    FGraphics.LoadFromFile(OpenTemplateDialog.FileName);
    FillDialogComboBoxes;
    LegendCombo.ItemIndex:= LegendCombo.Items.Count - 1;
    PicturesCombo.ItemIndex:= PicturesCombo.Items.Count - 1;
    SignaturesCombo.ItemIndex:= SignaturesCombo.Items.Count - 1;
    SetPrintOptions(FPrintOptions);
    PreviewPaint(Self);
  end;
end;

//++ Glukhov Bug#559 Build#166 10.12.01
procedure TPrintingDialog.TemplateSaveBtnClick(Sender: TObject);
begin
  SaveTemplateDialog.FileName:= OpenTemplateDialog.FileName;
  // Old variant
  //  if SaveTemplateDialog.Execute
  //  then  FGraphics.SaveToFile( SaveTemplateDialog.FileName );
  if SaveTemplateDialog.Execute then begin
    // Refresh options, including PrintSize, to fix the bug
    GetPrintOptions;
    FGraphics.SaveToFile( SaveTemplateDialog.FileName );
  end;
end;
//-- Glukhov Bug#559 Build#166 10.12.01

procedure TPrintingDialog.SetFieldsFromProjRect;
begin
  LeftPosVal[muMillimeters]:= FProjGraphic.BoundsRect.Left;
  TopPosVal[muMillimeters]:= RectHeight(FPrintOptions.PageSetup.PageExtent) - FProjGraphic.BoundsRect.Top;
  WidthVal[muMillimeters]:= RectWidth(FProjGraphic.BoundsRect);
  HeightVal[muMillimeters]:= RectHeight(FProjGraphic.BoundsRect);
end;

procedure TPrintingDialog.UpdateEnabled;
var
  AEnable: Boolean;
  BEnable: Boolean;
  CEnable: Boolean;
  Cnt: Integer;
begin

  if PrintToWMFCheck.Checked
  then  SetControlGroupEnabled(Group7, FALSE)
  else  SetControlGroupEnabled(Group7, TRUE);

  // YG: I am not sure that it is necessary
  //---------------------------------------
  SetControlGroupEnabled(Group4, TRUE);
  SetControlEnabled(WidthEdit, TRUE);
  SetControlEnabled(HeightEdit, TRUE);
  chkKeepScale.Enabled:= TRUE;
  //---------------------------------------

//++ Glukhov Bug#293(PrintFrame) Build#175 15.02.02
  if (LegendCombo.Items.Count+FrameCombo.Items.Count) > 0
//-- Glukhov Bug#293(PrintFrame) Build#175 15.02.02
  then  AEnable:= TRUE
  else begin
    AEnable:= FALSE;
    if InternalPageControl.ActivePage = InternalPage2
    then  InternalPageControl.ActivePage:= InternalPage1;
  end;
  InternalPage2.Enabled:= AEnable;
  SetControlGroupEnabled(LegendGroup, AEnable);
  SetControlGroupEnabled(LegSizeGroup, AEnable);
  SetControlGroupEnabled(LegPosGroup, AEnable);

  if PicturesCombo.Items.Count > 0 then
    AEnable:= TRUE
  else
  begin
    AEnable:= FALSE;
    if InternalPageControl.ActivePage = InternalPage3 then
      InternalPageControl.ActivePage:= InternalPage1;
  end;
  InternalPage3.Enabled:= AEnable;
  SetControlGroupEnabled(PicturesGroup, AEnable);
  SetControlGroupEnabled(PicSizeGroup, AEnable);
  SetControlGroupEnabled(PicPosGroup, AEnable);
  PicHScaleEdit.Enabled:= AEnable;
  PicHScaleSpin.Enabled:= AEnable;
  if KeepAspectCheck.Checked then
  begin
    PicHScaleEdit.Enabled:= FALSE;
    PicHScaleSpin.Enabled:= FALSE;
  end;
  {++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
  with LayoutModelCombo do
    if TPrintLayoutModel(Items.Objects[ItemIndex]) = plmBook then
    begin
      SetControlGroupEnabled(PicLocationGroup, AEnable);
      if AEnable then
        if PicLocOnPagesBtn.Checked then
        begin
          PicFirstCheck.Enabled:= TRUE;
          PicInterCheck.Enabled:= TRUE;
          PicLastCheck.Enabled:= TRUE;
        end
        else
        begin
          PicFirstCheck.Enabled:= FALSE;
          PicInterCheck.Enabled:= FALSE;
          PicLastCheck.Enabled:= FALSE;
        end;
    end
    else
      SetControlGroupEnabled(PicLocationGroup, FALSE);
  {-- Moskaliov Pictures Location Control BUILD#131 18.09.00}

  if SignaturesCombo.Items.Count > 0 then
    AEnable:= TRUE
  else
  begin
    AEnable:= FALSE;
    if InternalPageControl.ActivePage = InternalPage4 then
      InternalPageControl.ActivePage:= InternalPage1;
  end;
  InternalPage4.Enabled:= AEnable;
  SetControlGroupEnabled(SignaturesGroup, AEnable);
  SetControlGroupEnabled(ComFildsGroup, AEnable);
  SetControlGroupEnabled(SigPosGroup, AEnable);
  with LayoutModelCombo do
    if TPrintLayoutModel(Items.Objects[ItemIndex]) = plmBook then
    begin
      SetControlGroupEnabled(SigLocationGroup, AEnable);
      if AEnable then
        if SigLocOnPagesBtn.Checked then
        begin
          SigFirstCheck.Enabled:= TRUE;
          SigInterCheck.Enabled:= TRUE;
          SigLastCheck.Enabled:= TRUE;
        end
        else
        begin
          SigFirstCheck.Enabled:= FALSE;
          SigInterCheck.Enabled:= FALSE;
          SigLastCheck.Enabled:= FALSE;
        end;
    end
    else
      SetControlGroupEnabled(SigLocationGroup, FALSE);

  ViewsCombo.Enabled:= ViewsCombo.Items.Count > 0;
  PrintSavedViewBtn.Enabled:= ViewsCombo.Items.Count > 0;

  if CurrentItem = nil then
  begin
    DeleteBtn.Enabled:= FALSE;
    BringToFrontBtn.Enabled:= FALSE;
    BringForwardBtn.Enabled:= FALSE;
    SendBackwardBtn.Enabled:= FALSE;
    SendToBackBtn.Enabled:= FALSE;
  end
  else
  begin
    AEnable:= FALSE;
    BEnable:= FALSE;
    CEnable:= FALSE;
    for Cnt:= 0 to FGraphics.ItemCount - 1 do
      with FGraphics.Items[Cnt] do
        if CurrentItem = @BoundsRect then
        begin
          if not (FGraphics.Items[Cnt] is TProjectPlaceholder) then
            AEnable:= TRUE;
          if Cnt < FGraphics.ItemCount - 1 then
            BEnable:= TRUE;
          if Cnt > 0 then
            CEnable:= TRUE;
          Break;
        end;
    DeleteBtn.Enabled:= AEnable;
    BringToFrontBtn.Enabled:= BEnable;
    BringForwardBtn.Enabled:= BEnable;
    SendBackwardBtn.Enabled:= CEnable;
    SendToBackBtn.Enabled:= CEnable;
  end;
end;

procedure TPrintingDialog.UpdateModelEnabled;
begin
  with FPrintOptions.PageSetup do
    if PrintLayoutModel = plmUniquePage then
    begin
      SetControlGroupEnabled(MultyPageGroup, FALSE);
      SetControlGroupEnabled(Group9, FALSE);
      SetControlGroupEnabled(OverlapGroup, FALSE);
      SetControlGroupEnabled(OrderGroupBox, FALSE);
      if AutoRpBtn.Checked then
        ManualRpBtn.Checked:= TRUE;
      if PagesHorzVal.AsInteger + PagesVertVal.AsInteger > 2 then
      begin
        PagesHorzVal.AsInteger:= 1;
        PagesVertVal.AsInteger:= 1;
      end;
      if HorizOrderBtn.Checked then
        OrderImageList.Draw(OrderPaintBox.Canvas, 0, 0, 0, FALSE)
      else
        OrderImageList.Draw(OrderPaintBox.Canvas, 0, 0, 1, FALSE);
      DialogPage3.Enabled:= FALSE;
      DialogPageControl.Invalidate;
      {++ Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
      if CropMarksCheckBox.Enabled then
      begin
        if CropMarksCheckBox.Checked then
          CropMarksCheckBox.Checked:= FALSE;
        CropMarksCheckBox.Enabled:= FALSE;
      end;
      {-- Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
    end
    else
    begin
      SetControlGroupEnabled(MultyPageGroup, TRUE);
      SetControlGroupEnabled(OverlapGroup, TRUE);
      SetControlGroupEnabled(OrderGroupBox, TRUE);
      DialogPage3.Enabled:= TRUE;
      DialogPageControl.Invalidate;
      if PrintLayoutModel = plmUniformLayout then
      begin
        SetControlGroupEnabled(Group9, TRUE);
        {++ Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
        CropMarksCheckBox.Enabled:= not PrintToWMFCheck.Checked;
        {-- Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
      end
      else // PrintLayoutModel = plmBook
      begin
        SetControlGroupEnabled(Group9, FALSE);
        {++ Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
        if CropMarksCheckBox.Enabled then
        begin
          if CropMarksCheckBox.Checked then
            CropMarksCheckBox.Checked:= FALSE;
          CropMarksCheckBox.Enabled:= FALSE;
        end;
        {-- Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
      end;
    end;
  MultiPagesUpdated(LayoutModelCombo);
  {++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
  UpdateEnabled;
  {-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
end;

procedure TPrintingDialog.Repagenation;
var
  dblHeight: Double;
  dblWidth: Double;
  dblDelta: Double;
  boolUpdate: Boolean;
  intCnt: Integer;
begin
  boolUpdate:= FALSE;
  with FPrintOptions.PageSetup do
    if PrintLayoutModel <> plmUniquePage then
    begin
      // if the CenterPageBtn.Checked=TRUE,
      // then the needed Width is equal only WidthtVal[muMillimeters]
      dblWidth:= WidthVal[muMillimeters];
      // if the CenterPageBtn.Checked=TRUE,
      // then the needed Height is equal only HeightVal[muMillimeters]
      dblHeight:= HeightVal[muMillimeters];
      if not CenterPageBtn.Checked then
      begin
        // calculation of the needed Width
        if LeftPosVal[muMillimeters] < 0 then
        begin
          if HorizontalPages > 2 then
            dblDelta:= RectWidth(PageExtents[1, 0])
          else
            if HorizontalPages > 1 then
            // it's not correct from position of logic,
            // but it's workable in this realisation
            dblDelta:= RectWidth(PageExtents[2, 0])
          else
            dblDelta:= RectWidth(PageExtents[0, 0]);
          while DblCompare(LeftPosVal[muMillimeters], 0.0) = -1 do
          begin
            LeftPosVal[muMillimeters]:= LeftPosVal[muMillimeters] + dblDelta;
            boolUpdate:= TRUE;
          end;
        end
        else // LeftPosVal[muMillimeters] >= 0
        begin
          intCnt:= HorizontalPages;
          while DblCompare(LeftPosVal[muMillimeters], RectWidth(PageExtents[0, 0])) > -1 do
          begin
            if intCnt > 2 then
              LeftPosVal[muMillimeters]:= LeftPosVal[muMillimeters] - RectWidth(PageExtents[1, 0])
            else
              LeftPosVal[muMillimeters]:= LeftPosVal[muMillimeters] - RectWidth(PageExtents[0, 0]);
            Dec(intCnt);
            boolUpdate:= TRUE;
          end;
        end;
        dblWidth:= dblWidth + LeftPosVal[muMillimeters];
        // calculation of the needed Height
        if TopPosVal[muMillimeters] < 0 then
        begin
          if VerticalPages > 2 then
            dblDelta:= RectHeight(PageExtents[0, 1])
          else
            if VerticalPages > 1 then
            // it's not correct from position of logic,
            // but it's workable in this realisation
            dblDelta:= RectHeight(PageExtents[0, 2])
          else
            dblDelta:= RectHeight(PageExtents[0, 0]);
          while DblCompare(TopPosVal[muMillimeters], 0.0) = -1 do
          begin
            TopPosVal[muMillimeters]:= TopPosVal[muMillimeters] + dblDelta;
            boolUpdate:= TRUE;
          end;
        end
        else // TopPosVal[muMillimeters] >= 0
        begin
          intCnt:= VerticalPages;
          while DblCompare(TopPosVal[muMillimeters], RectHeight(PageExtents[0, 0])) > -1 do
          begin
            if intCnt > 2 then
              TopPosVal[muMillimeters]:= TopPosVal[muMillimeters] - RectHeight(PageExtents[0, 1])
            else
              TopPosVal[muMillimeters]:= TopPosVal[muMillimeters] - RectHeight(PageExtents[0, 0]);
            Dec(intCnt);
            boolUpdate:= TRUE;
          end;
        end;
        dblHeight:= dblHeight + TopPosVal[muMillimeters];
      end;
      // horizontal pages recalculation
      if DblCompare(dblWidth, PageExtent.Right) = 1 then
      begin
        boolUpdate:= TRUE;
        if HorizontalPages = 1 then
        begin
          PagesHorzVal.AsInteger:= PagesHorzVal.AsInteger + 1;
          if FReinvoke then
            //                         GetPrintOptions;
          begin
            FReinvoke:= FALSE;
            boolUpdate:= FALSE;
          end;
          //                      PageSizeUpdated;
          //                      PrintRangeOrScaleUpdated(RightEdit);
          PageFramesUpdated(RightEdit);
          FReinvoke:= not boolUpdate;
          // it's not enough, but it is necessary
          exit;
        end;
        dblWidth:= dblWidth - PageExtent.Right;
        if DblCompare(dblWidth, 0.0) = 1 then
        begin
          if HorizontalPages = 2 then
            // it's not correct from position of logic,
            // but it's workable in this realisation
            dblDelta:= RectWidth(PageExtents[2, 0])
          else
            dblDelta:= RectWidth(PageExtents[1, 0]);
          intCnt:= Round(dblWidth / dblDelta + 0.5);
          PagesHorzVal.AsInteger:= PagesHorzVal.AsInteger + intCnt;
        end;
      end
      else
        if HorizontalPages > 1 then
      begin
        if HorizontalPages > 2 then
        begin
          dblDelta:= RectWidth(PageExtents[1, 0]);
          intCnt:= Round(((PageExtent.Right - dblWidth) / dblDelta) - 0.5);
          if intCnt > 0 then
          begin
            boolUpdate:= TRUE;
            intCnt:= PagesHorzVal.AsInteger - intCnt;
            if intCnt < 2 then
              intCnt:= 2;
            PagesHorzVal.AsInteger:= intCnt;
          end
        end;
        if PagesHorzVal.AsInteger = 2 then
        begin
          dblDelta:= RectWidth(PageExtents[0, 0])
            + OverlapRightVal[muMillimeters];
          if PrintLayoutModel = plmUniformLayout then
            dblDelta:= dblDelta
              + InterRightVal[muMillimeters]
              - RightVal[muMillimeters];
          if DblCompare(dblWidth, dblDelta) < 1 then
          begin
            PagesHorzVal.AsInteger:= PagesHorzVal.AsInteger - 1;
            boolUpdate:= TRUE;
          end;
        end;
      end;
      // vertical pages recalculation
      if DblCompare(dblHeight, PageExtent.Top) = 1 then
      begin
        boolUpdate:= TRUE;
        if VerticalPages = 1 then
        begin
          PagesVertVal.AsInteger:= PagesVertVal.AsInteger + 1;
          if FReinvoke then
            //                         GetPrintOptions;
          begin
            FReinvoke:= FALSE;
            boolUpdate:= FALSE;
          end;
          //                      PageSizeUpdated;
          //                      PrintRangeOrScaleUpdated(RightEdit);
          PageFramesUpdated(RightEdit);
          FReinvoke:= not boolUpdate;
          // it's not enough, but it is necessary
          exit;
        end;
        dblHeight:= dblHeight - PageExtent.Top;
        if DblCompare(dblHeight, 0.0) = 1 then
        begin
          if VerticalPages = 2 then
            // it's not correct from position of logic,
            // but it's workable in this realisation
            dblDelta:= RectHeight(PageExtents[0, 2])
          else
            dblDelta:= RectHeight(PageExtents[0, 1]);
          intCnt:= Round(dblHeight / dblDelta + 0.5);
          PagesVertVal.AsInteger:= PagesVertVal.AsInteger + intCnt;
        end;
      end
      else
        if VerticalPages > 1 then
      begin
        if VerticalPages > 2 then
        begin
          dblDelta:= RectHeight(PageExtents[0, 1]);
          intCnt:= Round(((PageExtent.Top - dblHeight) / dblDelta) - 0.5);
          if intCnt > 0 then
          begin
            boolUpdate:= TRUE;
            intCnt:= PagesVertVal.AsInteger - intCnt;
            if intCnt < 2 then
              intCnt:= 2;
            PagesVertVal.AsInteger:= intCnt;
          end
        end;
        if PagesVertVal.AsInteger = 2 then
        begin
          dblDelta:= RectHeight(PageExtents[0, 0])
            + OverlapBottomVal[muMillimeters];
          if PrintLayoutModel = plmUniformLayout then
            dblDelta:= dblDelta
              + InterBottomVal[muMillimeters]
              - BottomVal[muMillimeters];
          if DblCompare(dblHeight, dblDelta) < 1 then
          begin
            PagesVertVal.AsInteger:= PagesVertVal.AsInteger - 1;
            boolUpdate:= TRUE;
          end;
        end;
      end;
      // Update changes, if it is necessary
      if boolUpdate then
      begin
        if FReinvoke then
          //                   GetPrintOptions;
        begin
          FReinvoke:= FALSE;
          boolUpdate:= FALSE;
        end;
        //                PageSizeUpdated;
        //                PrintRangeOrScaleUpdated(RightEdit);
        PageFramesUpdated(RightEdit);
        FReinvoke:= not boolUpdate;
      end;
    end;
end;

procedure TPrintingDialog.GetPrintOptions;
var
  Sight: PSight;
  PageFrames: TGrRect;
  Graphic: TProjectPlaceholder;
  Cnt: Integer;
begin
  if (FProject <> nil) and (FGraphics.ItemCount > 0) then
    with FProject^, Registry, FPrintOptions do
    begin
      Graphic:= nil;
      for Cnt:= 0 to FGraphics.ItemCount - 1 do
        if FGraphics.Items[Cnt].ClassName = 'TProjectPlaceholder' then
        begin
          Graphic:= TProjectPlaceholder(FGraphics.Items[Cnt]);
          Break;
        end;

      // get page-margins
      PageSetup.Copies:= CopiesVal.AsInteger;
      PageSetup.SortCopies:= SortCopiesCheck.Checked;
      if Graphic <> nil then
        with Graphic do
        begin
          if CenterPageBtn.Checked
          then  PrintPosition:= ppCenter
          else  PrintPosition:= ppCustom;

          if chkFitToPage.Checked
          then begin
            if chkKeepScale.Checked
            then  PrintSize:= psScaleAndFit
            else  PrintSize:= psFitToPage;
          end
          else begin
            if chkKeepScale.Checked
            then  PrintSize:= psScale
            else  PrintSize:= psWidthAndHeight;
          end;
          ViewName:= '';
          if PrintSavedViewBtn.Checked then
          begin
            PrintRange:= prSavedView;
            Sight:= Sights^.NamedSight(ViewsCombo.Text);
            if Sight <> nil then
            begin
              PrintRect:= Sight^.ViewRect;
              ViewName:= PToStr(Sight^.Name);
            end;
          end
          else
            if PrintCurrentViewBtn.Checked then
          begin
            PrintRange:= prCurrentView;
            PrintRect:= PInfo^.GetCurrentScreen;
          end
          else
            if PrintWindowBtn.Checked then
          begin
            PrintRange:= prWindow;
            PrintRect:= FPrintRect;
          end
          else
          begin
            PrintRange:= prProject;
            with Size do
              PrintRect:= RotRect(A.X, A.Y, XSize, YSize, 0);
          end;
          PrintScale:= ScaleVal.AsFloat;
        end;
      PageFrames.Bottom:= OverlapBottomVal[muMillimeters];
      PageFrames.Left:= OverlapLeftVal[muMillimeters];
      PageFrames.Right:= OverlapRightVal[muMillimeters];
      PageFrames.Top:= OverlapTopVal[muMillimeters];
      PageSetup.OverlapPageFrames:= PageFrames;

      with LayoutModelCombo do
        PageSetup.PrintLayoutModel:= TPrintLayoutModel(Items.Objects[ItemIndex]);
      PageSetup.PagesOrder:= HorizOrderBtn.Checked;
      PageSetup.StartPageNum:= StartPageVal.AsInteger;
      PageSetup.HorizontalPages:= PagesHorzVal.AsInteger;
      PageSetup.VerticalPages:= PagesVertVal.AsInteger;
      PageSetup.AutoRepagenate:= AutoRpBtn.Checked;
      PageSetup.PrintToWMF:= PrintToWMFCheck.Checked;

      PrintToFile:= PrintToFileCheck.Checked;

      PageFrames.Bottom:= BottomVal[muMillimeters];
      PageFrames.Left:= LeftVal[muMillimeters];
      PageFrames.Right:= RightVal[muMillimeters];
      PageFrames.Top:= TopVal[muMillimeters];
      PageSetup.PageFrames:= PageFrames;

      PageFrames.Bottom:= InterBottomVal[muMillimeters];
      PageFrames.Left:= InterLeftVal[muMillimeters];
      PageFrames.Right:= InterRightVal[muMillimeters];
      PageFrames.Top:= InterTopVal[muMillimeters];
      PageSetup.InterPageFrames:= PageFrames;

      // YG: I changed a bit the interpretation of property KeepScale to make it clear
      // Now it means that the form of printing rectangle is set in the project,
      // and it is set in the dialog, if else
      KeepScale:= btnSetFrameInPrj.Checked;
      PageSetup.PrintCropMarks:= CropMarksCheckBox.Checked;
    end;
end;

procedure TPrintingDialog.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  CanClose:= (ModalResult in [mrCancel, mrYes]) or CheckValidators(Self);
end;

procedure TPrintingDialog.PaintBoxPaint(Sender: TObject);
begin
  if FPrintOptions.PageSetup.Orientation = wpoPortrait then
    ImageList.Draw(PaintBox.Canvas, 0, 0, 0)
  else
    ImageList.Draw(PaintBox.Canvas, 0, 0, 1);
end;

procedure TPrintingDialog.FormatsComboClick(Sender: TObject);
begin
  with FPrintOptions.PageSetup do
    PaperFormatIndex:= PaperFormatNames.IndexOf(FormatsCombo.Text);
  PaperSizeUpdated;
end;

procedure TPrintingDialog.PortraitBtnClick(Sender: TObject);
begin
  // old paper orientation picture clearing
  PaintBox.Canvas.Brush.Color:= PaintBox.Color;
  PaintBox.Canvas.FillRect(PaintBox.ClientRect);
  if PortraitBtn.Checked then
  begin
    FPrintOptions.PageSetup.Orientation:= wpoPortrait;
    ImageList.Draw(PaintBox.Canvas, 0, 0, 0, TRUE);
  end
  else
  begin
    FPrintOptions.PageSetup.Orientation:= wpoLandscape;
    ImageList.Draw(PaintBox.Canvas, 0, 0, 1, TRUE);
  end;
  PaperSizeUpdated;
end;

procedure TPrintingDialog.PaperSizeUpdated;
begin
  try
    PageWidthVal[muMillimeters]:= RectWidth(FPrintOptions.PageSetup.PaperFormatSize);
    PageHeightVal[muMillimeters]:= RectHeight(FPrintOptions.PageSetup.PaperFormatSize);
    PageSizeUpdated;
    PrintRangeOrScaleUpdated(Self);
    LegSizeOrPositionUpdated(Self);
  finally
  end;
end;

function TPrintingDialog.PreviewRect(FromInputs: Boolean): TGrRect;
begin
  if FromInputs then
    Result:= GrBounds(LeftPosVal[muMillimeters], RectHeight(
      FPrintOptions.PageSetup.PageExtent) - TopPosVal[muMillimeters]
      - HeightVal[muMillimeters], WidthVal[muMillimeters],
      HeightVal[muMillimeters])
  else
    Result:= FProjGraphic.BoundsRect;
end;

procedure TPrintingDialog.RedrawPreview;
begin
  Scroller.InvalidateRect(PreviewRect(TRUE));
end;

procedure TPrintingDialog.ZoomAllBtnClick(Sender: TObject);
begin
  Scroller.CurrentView:= Scroller.Size;
end;

procedure TPrintingDialog.ZoomInBtnClick(Sender: TObject);
begin
  Scroller.ZoomByFactor(2);
end;

procedure TPrintingDialog.ZoomOutBtnClick(Sender: TObject);
begin
  Scroller.ZoomByFactor(0.5);
end;

procedure TPrintingDialog.UpdateEditHandlerPosition;
begin
  if FCurrentItem <> nil then with FCurrentItem^ do
    begin
      EditHandler.Visible:= FALSE;
      try
        EditHandler.XPosition:= Left;
        EditHandler.YPosition:= Bottom;
        EditHandler.Width:= RectWidth(FCurrentItem^);
        EditHandler.Height:= RectHeight(FCurrentItem^);
      finally
        EditHandler.Visible:= TRUE;
      end;
    end;
end;

procedure TPrintingDialog.SetCurrentItem(AItem: PGrRect);
begin
  if AItem <> FCurrentItem then
  begin
    if FCurrentItem <> nil then
      EditHandler.Visible:= FALSE;
    FCurrentItem:= AItem;
    if FCurrentItem <> nil then
    begin
      UpdateEditHandlerPosition;
      if (FCurrentItem = @FProjGraphic.BoundsRect) then
        if btnSetFrameInDlg.Checked
        then  EditHandler.Options:= EditHandler.Options - [ehoKeepAspect]
        else  EditHandler.Options:= EditHandler.Options + [ehoKeepAspect];
      EditHandler.Visible:= TRUE;
      MenuHandler:= EditHandler;
    end;
    UpdateEnabled;
  end;
end;

procedure TPrintingDialog.SetMenuHandler(AMenuHandler: TMenuHandler);
begin
  FMenuHandler:= AMenuHandler;
end;

procedure TPrintingDialog.InsertNewFrame(Sender: TObject);
begin
  CurrentItem:= nil;
  MenuHandler:= RubberBox;
end;

procedure TPrintingDialog.SetupMenuHandler(AMenuHandler: TMenuHandler);
begin
  AMenuHandler.Canvas:= PreviewWindow.Canvas;
  AMenuHandler.OnWinToIntPoint:= Scroller.WindowToInternal;
  AMenuHandler.OnWinToIntDistance:= Scroller.WindowToInternal;
  AMenuHandler.OnIntToWinPoint:= Scroller.InternalToWindow;
  AMenuHandler.OnIntToWinDistance:= Scroller.InternalToWindow;
end;

procedure TPrintingDialog.RubberBoxFinishedInput(Sender: TObject);
var
  Factor: Integer;
  Graphic: TCustomGraphic;
  FileName: string;
  FilePath: string;
  TempRect: TGrRect;
  OpenPicDialog: TWOpenPictureDialog;
  LegNameDialog: TLegendPrintDlg;
  Flags: DlgCtrlFlags;
  Status: OpenStatus;
  sTmp      : String;   // Glukhov Bug#293(PrintFrame) Build#175 09.02.02
begin
  if ZoomBtn.Down then begin
    if RubberBox.MinSizeReached then
      Scroller.CurrentView:= RotRect(RubberBox.Box, 0)
    else
    begin
      Factor:= 2;
      if GetKeyState(vk_Shift) < 0 then
        Factor:= Factor * 2
      else
        if GetKeyState(vk_Control) < 0 then
        Factor:= Factor * 4;
      Scroller.ZoomByFactorWithCenter(RubberBox.Position, Factor);
    end;
  end
  else if RubberBox.MinSizeReached then begin
//++ Glukhov Bug#293(PrintFrame) Build#175 09.02.02
    if (InsertFrameBtn.Down) then begin
    // Insert a frame (it is kind of legend)
      CurrentItem:= nil;
      Graphic:= TLegendPlaceholder.Create(FGraphics);
      Graphic.BoundsRect:= RubberBox.Box;
      Graphic.OriginalBoundsRect:= Graphic.BoundsRect;

      btnFrameObj.Checked:= True;
      TLegendPlaceholder(Graphic).LegendName:= csLegName4Frame;
      TLegendPlaceholder(Graphic).BitFlags:=
          TLegendPlaceholder(Graphic).BitFlags or cwPrnTempFrmBrdAll;

      FGraphics.FramesCount:= FGraphics.FramesCount + 1;
      if FGraphics.FramesCount < 10
      then  sTmp:= FGraphics.FrameName + '0' + IntToStr(FGraphics.FramesCount)
      else  sTmp:= FGraphics.FrameName + IntToStr(FGraphics.FramesCount);
      FrameCombo.Items.AddObject( sTmp, TObject(Graphic) );
      FrameCombo.ItemIndex:= FrameCombo.Items.Count - 1;

      SetFieldsFromLegendRect;
      UpdateEnabled;
      DialogPageControl.ActivePage:= DialogPageControl.Pages[3];
      InternalPageControl.ActivePage:= InternalPageControl.Pages[1];
      Scroller.InvalidateRect(Graphic.BoundsRect);
      MenuHandler:= nil;
      CurrentItem:= @Graphic.BoundsRect;
      Scroller.InvalidateRect(Graphic.BoundsRect);

      SelectBtn.Down:= TRUE;
      LegCustomBtn.Checked:= TRUE;
    end
////////////////////////////////////////////////////////////////////////////////
    else if (InsertOLEBtn.Down) then begin
      CurrentItem:= nil;
      Graphic:= TLegendPlaceholder.Create(FGraphics);
      Graphic.BoundsRect:= RubberBox.Box;
      Graphic.OriginalBoundsRect:= Graphic.BoundsRect;

      btnFrameObj.Checked:= True;

      TLegendPlaceholder(Graphic).LegendName:=csOleFrame;
      TLegendPlaceholder(Graphic).BitFlags:=TLegendPlaceholder(Graphic).BitFlags or cwPrnTempFrmBrdAll;

      FGraphics.FramesCount:= FGraphics.FramesCount + 1;
      if FGraphics.FramesCount < 10
      then  sTmp:= FGraphics.FrameName + '0' + IntToStr(FGraphics.FramesCount)
      else  sTmp:= FGraphics.FrameName + IntToStr(FGraphics.FramesCount);
      FrameCombo.Items.AddObject( sTmp, TObject(Graphic) );
      FrameCombo.ItemIndex:= FrameCombo.Items.Count - 1;

      SetFieldsFromLegendRect;
      UpdateEnabled;
      DialogPageControl.ActivePage:= DialogPageControl.Pages[3];
      InternalPageControl.ActivePage:= InternalPageControl.Pages[1];
      Scroller.InvalidateRect(Graphic.BoundsRect);
      MenuHandler:= nil;
      CurrentItem:= @Graphic.BoundsRect;
      Scroller.InvalidateRect(Graphic.BoundsRect);

      SelectBtn.Down:= TRUE;
      LegCustomBtn.Checked:= TRUE;

      ButtonOleClick(nil);
    end
////////////////////////////////////////////////////////////////////////////////
    else if InsertLegendBtn.Down then begin
    // Insert a legend
      CurrentItem:= nil;
      Graphic:= TLegendPlaceholder.Create(FGraphics);
      Graphic.BoundsRect:= RubberBox.Box;
      Graphic.OriginalBoundsRect:= Graphic.BoundsRect;
{
      with LegendCombo do ItemIndex:= Items.AddObject('', TObject(Graphic));
      SetFieldsFromLegendRect;
      UpdateEnabled;
      DialogPageControl.ActivePage:= DialogPageControl.Pages[3];
      InternalPageControl.ActivePage:= InternalPageControl.Pages[1];
      Scroller.InvalidateRect(Graphic.BoundsRect);
      MenuHandler:= nil;
      CurrentItem:= @Graphic.BoundsRect;
}
// YG: I think, there is no sense to enter a non-existent legend name
//      LegNameDialog:= TLegendPrintDlg.Create(Self, FProject^.Legends, TRUE);
      LegNameDialog:= TLegendPrintDlg.Create(Self, FProject^.Legends, False);
      case LegNameDialog.ShowModal of
      mrOK:
        begin
          btnLegendObj.Checked:= True;
          TLegendPlaceholder(Graphic).LegendName:= LegNameDialog.Name;
          LegendCombo.ItemIndex:= LegendCombo.Items.AddObject(LegNameDialog.Name, TObject(Graphic));
//          LegendCombo.Items[ItemIndex]:= TLegendPlaceholder(Graphic).LegendName;
          LegendCombo.ItemIndex:= LegendCombo.Items.Count - 1;

          SetFieldsFromLegendRect;
          UpdateEnabled;
          DialogPageControl.ActivePage:= DialogPageControl.Pages[3];
          InternalPageControl.ActivePage:= InternalPageControl.Pages[1];
          Scroller.InvalidateRect(Graphic.BoundsRect);
          MenuHandler:= nil;
          CurrentItem:= @Graphic.BoundsRect;
          Scroller.InvalidateRect(Graphic.BoundsRect);
        end;
      else
        begin
//          with LegendCombo do begin
//            Items.Delete(ItemIndex);
//            ItemIndex:= Items.Count - 1;
//          end;
          TempRect:= Graphic.BoundsRect;
//          CurrentItem:= nil;
          Graphic.Free;
//          SetFieldsFromLegendRect;
//          UpdateEnabled;
//          Scroller.InvalidateRect(TempRect);
        end;
      end;
      SelectBtn.Down:= TRUE;
      LegCustomBtn.Checked:= TRUE;
      LegNameDialog.Free;
    end
//-- Glukhov Bug#293(PrintFrame) Build#175 09.02.02
    else if InsertPictureBtn.Down then begin
      CurrentItem:= nil;
      Graphic:= TWindowsGraphic.Create(FGraphics);
      Graphic.BoundsRect:= RubberBox.Box;
      Graphic.OriginalBoundsRect:= Graphic.BoundsRect;
      with PicturesCombo do
        ItemIndex:= Items.AddObject('', TObject(Graphic));
      SetFieldsFromPictureRect;
      UpdateEnabled;
      DialogPageControl.ActivePage:= DialogPageControl.Pages[3];
      InternalPageControl.ActivePage:= InternalPageControl.Pages[2];
      Scroller.InvalidateRect(Graphic.BoundsRect);
      MenuHandler:= nil;
      CurrentItem:= @Graphic.BoundsRect;
      TempRect:= Graphic.BoundsRect;
      Flags:= [dfEnableVectorFormat];
      OpenPicDialog:= TWOpenPictureDialog.Create(Self, @Flags, @Status);
      {++ Moskaliov Picture Filter Filling BUILD#152 23.02.01}
      OpenPicDialog.Filter:= TrLib.ImageGetImpFormats;
      {-- Moskaliov Picture Filter Filling BUILD#152 23.02.01}
      if OpenPicDialog.Execute then
      begin
        FileName:= ExtractFileName(OpenPicDialog.FileName);
        FilePath:= ExtractFilePath(OpenPicDialog.FileName);
        try
          CurrentItem:= nil;
          Scroller.InvalidateRect(TempRect);
          TWindowsGraphic(Graphic).CreateFromFile(FileName, FilePath, TRUE);
          with PicturesCombo do
          begin
            Items[ItemIndex]:= TWindowsGraphic(Graphic).FileName;
            ItemIndex:= Items.Count - 1;
          end;
          CurrentItem:= @Graphic.BoundsRect;
          TempRect:= Graphic.BoundsRect;
          PicCustomBtn.Checked:= TRUE;
        except
          PicturesCombo.Items.Delete(PicturesCombo.ItemIndex);
          PicturesCombo.ItemIndex:= PicturesCombo.Items.Count - 1;
          CurrentItem:= nil;
          Graphic.Free;
          raise;
        end;
      end
      else
      begin
        PicturesCombo.Items.Delete(PicturesCombo.ItemIndex);
        PicturesCombo.ItemIndex:= PicturesCombo.Items.Count - 1;
        CurrentItem:= nil;
        Graphic.Free;
      end;
      SetFieldsFromPictureRect;
      UpdateEnabled;
      SelectBtn.Down:= TRUE;
      OpenPicDialog.Free;
      Scroller.InvalidateRect(TempRect);
    end
    else if InsertSignatureBtn.Down then begin
      CurrentItem:= nil;
      Graphic:= TPrintSignature.Create(FGraphics);
      Graphic.BoundsRect:= RubberBox.Box;
//++ Glukhov PrintSignature Build#166 17.01.02
      // Set the font height for the new signature
      with TPrintSignature(Graphic) do begin
          TextStyle.Height:= Trunc(Abs(BoundsRect.Top - BoundsRect.Bottom) * 72 / 25.4);
          TextStyle.Height:=TextStyle.Height / LineCount;
      end;
//-- Glukhov PrintSignature Build#166 17.01.02
      Graphic.OriginalBoundsRect:= Graphic.BoundsRect;
      with SignaturesCombo do
      begin
        ItemIndex:= Items.AddObject(TPrintSignature(Graphic).Signature, TObject(Graphic));
        FSignatureIndex:= ItemIndex;
      end;
      SetFieldsFromSignatureRect;
      UpdateEnabled;
      DialogPageControl.ActivePage:= DialogPageControl.Pages[3];
      InternalPageControl.ActivePage:= InternalPageControl.Pages[3];
      Scroller.InvalidateRect(Graphic.BoundsRect);
      MenuHandler:= nil;
      CurrentItem:= @Graphic.BoundsRect;
      SelectBtn.Down:= TRUE;
      SigSizeOrPositionUpdated(FontDlgBtn);
    end;
  end;
end;

procedure TPrintingDialog.ZoomBtnClick(Sender: TObject);
begin
  MenuHandler:= RubberBox;
end;

procedure TPrintingDialog.SelectBtnClick(Sender: TObject);
begin
  MenuHandler:= nil;
end;

//++ Glukhov Bug#293(PrintFrame) Build#175 15.02.02
function TPrintingDialog.GetLegendGraphic : TLegendPlaceholder;
begin
  Result:= nil;
  if btnLegendObj.Checked then begin
    with LegendCombo do
    if (Items.Count > 0) and (ItemIndex >= 0)
    then  Result:= TLegendPlaceholder(Items.Objects[ItemIndex]);
  end
  else begin
    with FrameCombo do
    if (Items.Count > 0) and (ItemIndex >= 0)
    then  Result:= TLegendPlaceholder(Items.Objects[ItemIndex]);
  end;
end;

procedure TPrintingDialog.LegendComboClick(Sender: TObject);
var
  Graphic: TLegendPlaceholder;
begin
  CurrentItem:= nil;
  Graphic:= GetLegendGraphic;
  if not Assigned( Graphic ) then Exit;
  CurrentItem:= @Graphic.BoundsRect;
  SetFieldsFromLegendRect;
end;
//-- Glukhov Bug#293(PrintFrame) Build#175 15.02.02

procedure TPrintingDialog.UpdatePrintView;
var
  Sight: PSight;
begin
  if PrintSavedViewBtn.Checked and PreviewBtn.Down then
  begin
    Sight:= FProject^.Sights^.NamedSight(ViewsCombo.Text);
    if Sight <> nil then
    begin
      FSightChanged:= TRUE;
      ProcSetSight(FProject, Sight, FALSE);
      Scroller.InvalidateRect(FProjGraphic.BoundsRect);
    end;
  end;
end;

procedure TPrintingDialog.DeleteBtnClick(Sender: TObject);
var
  Cnt: Integer;
begin
  for Cnt:= 0 to FGraphics.ItemCount - 1 do
    with FGraphics.Items[Cnt] do
      if CurrentItem = @BoundsRect then
      begin
        CurrentItem:= nil;
        if ClassName = 'TPrintSignature' then
        begin
          with SignaturesCombo do
            if (Items.Count > 0) and (ItemIndex >= 0) then
            begin
              Items.Delete(ItemIndex);
              ItemIndex:= Items.Count - 1;
              SetFieldsFromSignatureRect;
            end;
        end
        else if ClassName = 'TWindowsGraphic' then
        begin
          with PicturesCombo do
            if (Items.Count > 0) and (ItemIndex >= 0) then
            begin
              Items.Delete(ItemIndex);
              ItemIndex:= Items.Count - 1;
              SetFieldsFromPictureRect;
            end;
        end
        else if ClassName = 'TLegendPlaceholder' then
        begin
//++ Glukhov Bug#293(PrintFrame) Build#175 15.02.02
          if (TLegendPlaceholder(FGraphics.Items[Cnt]).LegendName = csLegName4Frame)
          or (TLegendPlaceholder(FGraphics.Items[Cnt]).LegendName = csOleFrame)
          then begin
            with FrameCombo do  if (Items.Count > 0) and (ItemIndex >= 0)
            then begin
              Items.Delete(ItemIndex);
              ItemIndex:= Items.Count - 1;
              if Items.Count <= 0 then Invalidate;
            end;
          end
          else begin
            with LegendCombo do  if (Items.Count > 0) and (ItemIndex >= 0)
            then begin
              Items.Delete(ItemIndex);
              ItemIndex:= Items.Count - 1;
              if Items.Count <= 0 then Invalidate;
            end;
          end;
          SetFieldsFromLegendRect;
//-- Glukhov Bug#293(PrintFrame) Build#175 15.02.02
        end;
        Scroller.InvalidateRect(BoundsRect);
        Free;
        Break;
      end;
  UpdateEnabled;
end;

procedure TPrintingDialog.UpdateKeepAspect;
begin
//  if btnSetFrameInPrj.Checked then begin
  if btnSetFrameInDlg.Checked then begin
    if FCurrentItem = @FProjGraphic.BoundsRect
    then  EditHandler.Options:= EditHandler.Options - [ehoKeepAspect];
  end
  else begin
    if FCurrentItem = @FProjGraphic.BoundsRect
    then  EditHandler.Options:= EditHandler.Options + [ehoKeepAspect];
  end;
end;

procedure TPrintingDialog.chkKeepScaleClick(Sender: TObject);
begin
  if chkKeepScale.Checked then
  begin
//    chkFitToPage.Checked:= False;
  end;
end;

procedure TPrintingDialog.chkFitToPageClick(Sender: TObject);
begin
  if chkFitToPage.Checked then
  begin
    if AutoRpBtn.Checked  then ManualRpBtn.Checked:= TRUE;
// YG: commented the next line, added new mode psScaleAndFit
//    chkKeepScale.Checked:= False;
    if CenterPageBtn.Checked
    then  PrintRangeOrScaleUpdated(Sender)
    else  CenterPageBtn.Checked:= True;   // it includes PrintRangeOrScaleUpdated(Sender)
  end;
end;

procedure TPrintingDialog.FrameRatioBtnClick(Sender: TObject);
begin
  UpdateKeepAspect;
  PrintRangeOrScaleUpdated(Sender);
end;

procedure TPrintingDialog.PosEditClick(Sender: TObject);
begin
  if FSelectAll then
    if Sender = LeftPosEdit then
    begin
      LeftPosEdit.SelectAll;
      FSelectAll:= FALSE;
    end
    else
      if Sender = TopPosEdit then
    begin
      TopPosEdit.SelectAll;
      FSelectAll:= FALSE;
    end
    else
      if Sender = LegLeftPosEdit then
    begin
      LegLeftPosEdit.SelectAll;
      FSelectAll:= FALSE;
    end
    else
      if Sender = LegTopPosEdit then
    begin
      LegTopPosEdit.SelectAll;
      FSelectAll:= FALSE;
    end
    else
      if Sender = PicLeftPosEdit then
    begin
      PicLeftPosEdit.SelectAll;
      FSelectAll:= FALSE;
    end
    else
      if Sender = PicTopPosEdit then
    begin
      PicTopPosEdit.SelectAll;
      FSelectAll:= FALSE;
    end
    else
      if Sender = SigLeftPosEdit then
    begin
      SigLeftPosEdit.SelectAll;
      FSelectAll:= FALSE;
    end
    else
      if Sender = SigTopPosEdit then
    begin
      SigTopPosEdit.SelectAll;
      FSelectAll:= FALSE;
    end;
end;

procedure TPrintingDialog.SizeEditClick(Sender: TObject);
begin
  if FSelectAll then
    if Sender = WidthEdit then
    begin
      WidthEdit.SelectAll;
      FSelectAll:= FALSE;
    end
    else
      if Sender = HeightEdit then
    begin
      HeightEdit.SelectAll;
      FSelectAll:= FALSE;
    end
    else
      if Sender = LegWidthEdit then
    begin
      LegWidthEdit.SelectAll;
      FSelectAll:= FALSE;
    end
    else
      if Sender = LegHeightEdit then
    begin
      LegHeightEdit.SelectAll;
      FSelectAll:= FALSE;
    end
    else
      if Sender = PicWidthEdit then
    begin
      PicWidthEdit.SelectAll;
      FSelectAll:= FALSE;
    end
    else
      if Sender = PicHeightEdit then
    begin
      PicHeightEdit.SelectAll;
      FSelectAll:= FALSE;
    end
    else
      if Sender = PicWScaleEdit then
    begin
      PicWScaleEdit.SelectAll;
      FSelectAll:= FALSE;
    end
    else
      if Sender = PicHScaleEdit then
    begin
      PicHScaleEdit.SelectAll;
      FSelectAll:= FALSE;
    end;
end;

procedure TPrintingDialog.EditKeyPress(Sender: TObject;
  var Key: Char);
begin
  if OkBtn.Default  then OkBtn.Default:= FALSE;
  FKeyPressed:= TRUE;
  if Key = Chr(13) then
  begin
    Key:= Chr(0);
    OkBtn.SetFocus;
  end;
end;

procedure TPrintingDialog.ScaleComboChange(Sender: TObject);
begin
  FKeyPressed:= TRUE;
  ScaleUpdated(Sender);
end;

procedure TPrintingDialog.ModelPaintBoxPaint(Sender: TObject);
begin
  with ModelPaintBox do
  begin
    // old Print Layout Model picture clearing
    Canvas.Brush.Color:= Color;
    Canvas.FillRect(ClientRect);
    // current Print Layout Model picture drawing
    LPModelImageList.Draw(Canvas, 0, 0, LayoutModelCombo.ItemIndex, TRUE);
  end;
end;

procedure TPrintingDialog.LayoutModelComboClick(Sender: TObject);
begin
  with FPrintOptions.PageSetup, LayoutModelCombo do
    if PrintLayoutModel = TPrintLayoutModel(Items.Objects[ItemIndex])
    then  Exit
    else  PrintLayoutModel:= TPrintLayoutModel(Items.Objects[ItemIndex]);
  ModelPaintBoxPaint(Sender);
  UpdateModelEnabled;
end;

procedure TPrintingDialog.PageRpBtnClick(Sender: TObject);
begin
  if Sender = AutoRpBtn then
  begin
    ManualRpBtn.Checked:= FALSE;
    chkFitToPage.Checked:= False;
    Repagenation;
  end
  else if Sender = ManualRpBtn then begin
    AutoRpBtn.Checked:= FALSE;
  end;
end;

procedure TPrintingDialog.OrderPaintBoxPaint(Sender: TObject);
begin
  if HorizOrderBtn.Checked then
    OrderImageList.Draw(OrderPaintBox.Canvas, 0, 0, 0, TRUE)
  else
    OrderImageList.Draw(OrderPaintBox.Canvas, 0, 0, 1, TRUE);
end;

procedure TPrintingDialog.PagesOrderBtnClick(Sender: TObject);
var
  Cnt: Integer;
  Graphic: TPrintSignature;
begin
  if Sender = HorizOrderBtn then
  begin
    FPrintOptions.PageSetup.PagesOrder:= TRUE;
    VertOrderBtn.Checked:= FALSE;
    OrderImageList.Draw(OrderPaintBox.Canvas, 0, 0, 0, TRUE);
  end
  else
    if Sender = VertOrderBtn then
  begin
    FPrintOptions.PageSetup.PagesOrder:= FALSE;
    HorizOrderBtn.Checked:= FALSE;
    OrderImageList.Draw(OrderPaintBox.Canvas, 0, 0, 1, TRUE);
  end;
  if PreviewBtn.Down then
    with SignaturesCombo do
      for Cnt:= 0 to Items.Count - 1 do
      begin
        Graphic:= TPrintSignature(Items.Objects[Cnt]);
        Graphic.Invalidate(Scroller);
      end;
end;

procedure TPrintingDialog.InterEditClick(Sender: TObject);
begin
  if FSelectAll then
    if Sender = InterLeftEdit then
    begin
      InterLeftEdit.SelectAll;
      FSelectAll:= FALSE;
    end
    else
      if Sender = InterRightEdit then
    begin
      InterRightEdit.SelectAll;
      FSelectAll:= FALSE;
    end
    else
      if Sender = InterTopEdit then
    begin
      InterTopEdit.SelectAll;
      FSelectAll:= FALSE;
    end
    else
      if Sender = InterBottomEdit then
    begin
      InterBottomEdit.SelectAll;
      FSelectAll:= FALSE;
    end;
end;

procedure TPrintingDialog.MarginEditClick(Sender: TObject);
begin
  if FSelectAll then
    if Sender = LeftEdit then
    begin
      LeftEdit.SelectAll;
      FSelectAll:= FALSE;
    end
    else
      if Sender = RightEdit then
    begin
      RightEdit.SelectAll;
      FSelectAll:= FALSE;
    end
    else
      if Sender = TopEdit then
    begin
      TopEdit.SelectAll;
      FSelectAll:= FALSE;
    end
    else
      if Sender = BottomEdit then
    begin
      BottomEdit.SelectAll;
      FSelectAll:= FALSE;
    end;
end;

procedure TPrintingDialog.PagesEditClick(Sender: TObject);
begin
  if FSelectAll then
    if Sender = HorzPagesEdit then
    begin
      HorzPagesEdit.SelectAll;
      FSelectAll:= FALSE;
    end
    else
      if Sender = VertPagesEdit then
    begin
      VertPagesEdit.SelectAll;
      FSelectAll:= FALSE;
    end;
end;

procedure TPrintingDialog.InterSpinClick(Sender: TObject);
begin
  if Sender = InterLeftSpin then
  begin
    if not InterLeftEdit.Focused then
    begin
      InterLeftEdit.SetFocus;
      InterLeftEdit.SelLength:= 0;
    end;
  end
  else
    if Sender = InterRightSpin then
  begin
    if not InterRightEdit.Focused then
    begin
      InterRightEdit.SetFocus;
      InterRightEdit.SelLength:= 0;
    end;
  end
  else
    if Sender = InterTopSpin then
  begin
    if not InterTopEdit.Focused then
    begin
      InterTopEdit.SetFocus;
      InterTopEdit.SelLength:= 0;
    end;
  end
  else
    if Sender = InterBottomSpin then
  begin
    if not InterBottomEdit.Focused then
    begin
      InterBottomEdit.SetFocus;
      InterBottomEdit.SelLength:= 0;
    end;
  end;
  PageFramesUpdated(Sender);
end;

procedure TPrintingDialog.MarginSpinClick(Sender: TObject);
begin
  if Sender = LeftSpin then
  begin
    if not LeftEdit.Focused then
    begin
      LeftEdit.SetFocus;
      LeftEdit.SelLength:= 0;
    end;
  end
  else
    if Sender = RightSpin then
  begin
    if not RightEdit.Focused then
    begin
      RightEdit.SetFocus;
      RightEdit.SelLength:= 0;
    end;
  end
  else
    if Sender = TopSpin then
  begin
    if not TopEdit.Focused then
    begin
      TopEdit.SetFocus;
      TopEdit.SelLength:= 0;
    end;
  end
  else
    if Sender = BottomSpin then
  begin
    if not BottomEdit.Focused then
    begin
      BottomEdit.SetFocus;
      BottomEdit.SelLength:= 0;
    end;
  end;
  PageFramesUpdated(Sender);
end;

procedure TPrintingDialog.MultiPagesSpinClick(Sender: TObject);
begin
  if Sender = HorzPagesSpin then
  begin
    if not HorzPagesEdit.Focused then
    begin
      HorzPagesEdit.SetFocus;
      HorzPagesEdit.SelLength:= 0;
    end;
  end
  else
    if Sender = VertPagesSpin then
    if not VertPagesEdit.Focused then
    begin
      VertPagesEdit.SetFocus;
      VertPagesEdit.SelLength:= 0;
    end;
  if AutoRpBtn.Checked then
    ManualRpBtn.Checked:= TRUE;
  MultiPagesUpdated(Sender);
end;

procedure TPrintingDialog.OverlapEditClick(Sender: TObject);
begin
  if FSelectAll then
    if Sender = OverlapLeftEdit then
    begin
      OverlapLeftEdit.SelectAll;
      FSelectAll:= FALSE;
    end
    else
      if Sender = OverlapRightEdit then
    begin
      OverlapRightEdit.SelectAll;
      FSelectAll:= FALSE;
    end
    else
      if Sender = OverlapTopEdit then
    begin
      OverlapTopEdit.SelectAll;
      FSelectAll:= FALSE;
    end
    else
      if Sender = OverlapBottomEdit then
    begin
      OverlapBottomEdit.SelectAll;
      FSelectAll:= FALSE;
    end;
end;

procedure TPrintingDialog.OverlapSpinClick(Sender: TObject);
begin
  if Sender = OverlapLeftSpin then
  begin
    if not OverlapLeftEdit.Focused then
    begin
      OverlapLeftEdit.SetFocus;
      OverlapLeftEdit.SelLength:= 0;
    end;
  end
  else
    if Sender = OverlapRightSpin then
  begin
    if not OverlapRightEdit.Focused then
    begin
      OverlapRightEdit.SetFocus;
      OverlapRightEdit.SelLength:= 0;
    end;
  end
  else
    if Sender = OverlapTopSpin then
  begin
    if not OverlapTopEdit.Focused then
    begin
      OverlapTopEdit.SetFocus;
      OverlapTopEdit.SelLength:= 0;
    end;
  end
  else
    if Sender = OverlapBottomSpin then
  begin
    if not OverlapBottomEdit.Focused then
    begin
      OverlapBottomEdit.SetFocus;
      OverlapBottomEdit.SelLength:= 0;
    end;
  end;
  PageFramesUpdated(Sender);
end;

procedure TPrintingDialog.StartPageEditClick(Sender: TObject);
var
  Cnt: Integer;
  Graphic: TPrintSignature;
begin
  if FSelectAll then
  begin
    StartPageEdit.SelectAll;
    FSelectAll:= FALSE;
  end;
  FPrintOptions.PageSetup.StartPageNum:= StartPageVal.AsInteger;
  if PreviewBtn.Down then
    with SignaturesCombo do
      for Cnt:= 0 to Items.Count - 1 do
      begin
        Graphic:= TPrintSignature(Items.Objects[Cnt]);
        Graphic.Invalidate(Scroller);
      end;
end;

function CreateRotatedFont(F: TFont; Angle: Integer): HFont;
var
  LF: TLogFont;
begin
  FillChar(LF, SizeOf(LF), #0);
  with LF do
  begin
    lfHeight:= F.Height;
    lfWidth:= 0;
    lfEscapement:= Angle * 10;
    lfOrientation:= 0;
    if fsBold in F.Style then
      lfWeight:= FW_BOLD
    else
      lfWeight:= FW_NORMAL;
    lfItalic:= Byte(fsItalic in F.Style);
    lfUnderline:= Byte(fsUnderline in F.Style);
    lfStrikeOut:= Byte(fsStrikeOut in F.Style);
    lfCharSet:= DEFAULT_CHARSET;
    StrPCopy(lfFaceName, F.Name);
    lfQuality:= DEFAULT_QUALITY;
    lfOutPrecision:= OUT_DEFAULT_PRECIS;
    lfClipPrecision:= CLIP_DEFAULT_PRECIS;
    case F.Pitch of
      fpVariable: lfPitchAndFamily:= VARIABLE_PITCH;
      fpFixed: lfPitchAndFamily:= FIXED_PITCH;
    else
      lfPitchAndFamily:= DEFAULT_PITCH;
    end;
  end;
  Result:= CreateFontIndirect(LF);
end;

procedure TPrintingDialog.InternalPageControlDrawTab(Control: TWinControl;
  Index: Integer; ActiveTab: Boolean; const RectFg, RectBg: TRect;
  State: TOwnerDrawState);
var
  OldColor: COLORREF;
  intBkMode: Integer;
begin
  with TExtPageControl(Control) do
  begin
    // Vertical text (270 degrees)
    Canvas.Font.Handle:= CreateRotatedFont(Canvas.Font, 270);
    // set Text-Alignment to center
    SetTextAlign(Canvas.Handle, TA_CENTER);
    if Pages[Index].Enabled then
      ExtTextOut(Canvas.Handle,
        RectFg.Left + (RectFg.Right - RectFg.Left) div 2 + 7,
        RectFg.Top + (RectFg.Bottom - RectFg.Top) div 2,
        ETO_CLIPPED, @RectFg, PChar(Pages[Index].Caption),
        Length(Pages[Index].Caption), nil)
    else
    begin
      OldColor:= SetTextColor(Canvas.Handle, ColorToRGB(clBtnHighlight));
      ExtTextOut(Canvas.Handle,
        RectFg.Left + (RectFg.Right - RectFg.Left) div 2 + 8,
        RectFg.Top + (RectFg.Bottom - RectFg.Top) div 2 + 1,
        ETO_CLIPPED, @RectFg, PChar(Pages[Index].Caption),
        Length(Pages[Index].Caption), nil);
      SetTextColor(Canvas.Handle, ColorToRGB(clGrayText));
      intBkMode:= SetBkMode(Canvas.Handle, TRANSPARENT);
      ExtTextOut(Canvas.Handle,
        RectFg.Left + (RectFg.Right - RectFg.Left) div 2 + 7,
        RectFg.Top + (RectFg.Bottom - RectFg.Top) div 2,
        ETO_CLIPPED, @RectFg, PChar(Pages[Index].Caption),
        Length(Pages[Index].Caption), nil);
      SetTextColor(Canvas.Handle, OldColor);
      SetBkMode(Canvas.Handle, intBkMode);
    end;
  end;
end;

procedure TPrintingDialog.DialogPageControlChange(Sender: TObject);
var
  intCnt: Integer;
begin
  with DialogPageControl do
    for intCnt:= 0 to PageCount - 1 do
      if Pages[intCnt] = ActivePage then
      begin
        FPageIndex:= intCnt;
        Break;
      end;
end;

procedure TPrintingDialog.InternalPageControlChange(Sender: TObject);
var
  intCnt: Integer;
begin
  with InternalPageControl do
    for intCnt:= 0 to PageCount - 1 do
      if Pages[intCnt] = ActivePage then
      begin
        FSubPageIndex:= intCnt;
        Break;
      end;
end;

procedure TPrintingDialog.MoreOptBtnClick(Sender: TObject);
begin
  DialogPageControl.ActivePage:= DialogPageControl.Pages[2];
end;

procedure TPrintingDialog.PageSetupBtnClick(Sender: TObject);
begin
  DialogPageControl.ActivePage:= DialogPageControl.Pages[1];
end;

procedure TPrintingDialog.LegSizeUpdated(Sender: TObject);
begin
  if FKeyPressed then
  begin
    if Sender = LegWidthSpin then
    begin
      if not LegWidthEdit.Focused then
      begin
        LegWidthEdit.SetFocus;
        LegWidthEdit.SelLength:= 0;
      end;
    end
    else
      if Sender = LegHeightSpin then
      if not LegHeightEdit.Focused then
      begin
        LegHeightEdit.SetFocus;
        LegHeightEdit.SelLength:= 0;
      end;
    LegSizeOrPositionUpdated(Sender);
    OkBtn.Default:= TRUE;
  end;
  FSelectAll:= TRUE;
end;

//++ Glukhov Bug#293(PrintFrame) Build#175 15.02.02
procedure TPrintingDialog.SetFieldsFromLegendRect;
var
  Graphic   : TLegendPlaceholder;
  bOldFUpd  : Boolean;
begin
  LegLeftPosVal[muMillimeters]:= 0;
  LegTopPosVal[muMillimeters]:= 0;
  LegWidthVal[muMillimeters]:= 0;
  LegHeightVal[muMillimeters]:= 0;

  bOldFUpd:= FUpdating;
  FUpdating:= True;
  chkLegFrmLeft.Checked:= False;
  chkLegFrmTop.Checked:= False;
  chkLegFrmRight.Checked:= False;
  chkLegFrmBottom.Checked:= False;
  FUpdating:= bOldFUpd;

  Graphic:= GetLegendGraphic;
  if not Assigned( Graphic ) then Exit;

  FUpdating:= True;

  ButtonOle.Visible:=(Graphic.LegendName = csOleFrame);
  LabelOLE.Visible:=ButtonOLE.Visible;

  chkLegFrmLeft.Visible:=not ButtonOle.Visible;
  chkLegFrmTop.Visible:=not ButtonOle.Visible;
  chkLegFrmRight.Visible:=not ButtonOle.Visible;
  chkLegFrmBottom.Visible:=not ButtonOle.Visible;

  chkLegFrmLeft.Checked:= (Graphic.BitFlags and cwPrnTempFrmBrdLeft) <> 0;
  chkLegFrmTop.Checked:= (Graphic.BitFlags and cwPrnTempFrmBrdTop) <> 0;
  chkLegFrmRight.Checked:= (Graphic.BitFlags and cwPrnTempFrmBrdRight) <> 0;
  chkLegFrmBottom.Checked:= (Graphic.BitFlags and cwPrnTempFrmBrdBottom) <> 0;
  FUpdating:= bOldFUpd;

  LegLeftPosVal[muMillimeters]:= Graphic.BoundsRect.Left;
  LegOriginCombo.ItemIndex:= Ord(Graphic.PrintOrigin);
  if LegOriginCombo.ItemIndex = Ord(poTop) then
  begin
    LegTopLab.Caption:= MlgSection[38];
    LegTopPosVal[muMillimeters]:= RectHeight(FPrintOptions.PageSetup.PageExtent) - Graphic.BoundsRect.Top
  end
  else begin
    LegTopLab.Caption:= MlgSection[16];
    LegTopPosVal[muMillimeters]:= Graphic.BoundsRect.Bottom;
  end;
  LegWidthVal[muMillimeters]:= RectWidth(Graphic.BoundsRect);
  LegHeightVal[muMillimeters]:= RectHeight(Graphic.BoundsRect);
  if FCurrentItem = @Graphic.BoundsRect
  then  EditHandler.Options:= EditHandler.Options - [ehoKeepAspect];
  if Graphic.PrintPosition = ppCenter then
  begin
    if not LegCenterBtn.Checked  then LegCenterBtn.Checked:= TRUE;
  end
  else if Graphic.PrintPosition = ppCenterVert then
  begin
    if not LegCenterVertBtn.Checked  then LegCenterVertBtn.Checked:= TRUE;
  end
  else if Graphic.PrintPosition = ppCenterHoriz then
  begin
    if not LegCenterHorBtn.Checked  then LegCenterHorBtn.Checked:= TRUE;
  end
  else begin
    Graphic.PrintPosition:= ppCustom;
    if not LegCustomBtn.Checked  then LegCustomBtn.Checked:= TRUE;
  end;
end;
//-- Glukhov Bug#293(PrintFrame) Build#175 15.02.02

procedure TPrintingDialog.LegPositionBtnClick(Sender: TObject);
var
  Graphic: TLegendPlaceholder;
begin
  with LegendCombo do
    if (Items.Count > 0) and (ItemIndex >= 0) then
      Graphic:= TLegendPlaceholder(Items.Objects[ItemIndex])
    else
      Exit;
  if LegCustomBtn.Checked then
    Graphic.PrintPosition:= ppCustom
  else
    if LegCenterHorBtn.Checked then
    Graphic.PrintPosition:= ppCenterHoriz
  else
    if LegCenterVertBtn.Checked then
    Graphic.PrintPosition:= ppCenterVert
  else
    if LegCenterBtn.Checked then
    Graphic.PrintPosition:= ppCenter;
  LegSizeOrPositionUpdated(Sender);
end;

procedure TPrintingDialog.LegSizeOrPositionUpdated(Sender: TObject);
var
  boolNeedRedraw: Boolean;
  PreviewSize: TGrRect;
  Graphic: TLegendPlaceholder;
  TempRect: TGrRect;
  dblWidth: Double;
  dblHeight: Double;
  intValue: Integer;
begin
//++ Glukhov Bug#293(PrintFrame) Build#175 15.02.02
  Graphic:= GetLegendGraphic;
  if not Assigned( Graphic ) then Exit;
//-- Glukhov Bug#293(PrintFrame) Build#175 15.02.02

  if (not FUpdating) and (not RectEmpty(Graphic.BoundsRect)) then
  begin
    // store the current preview-position for later invalidate
    PreviewSize:= Graphic.BoundsRect;
    boolNeedRedraw:= FALSE;
    TempRect:= Graphic.BoundsRect;
    dblWidth:= RectWidth(TempRect);
    dblHeight:= RectHeight(TempRect);
    if (Sender = LegWidthEdit) or (Sender = LegWidthSpin) then
    begin
      dblWidth:= LegWidthVal[muMillimeters];
      TempRect.Right:= TempRect.Left + dblWidth;
    end
    else
      if (Sender = LegHeightEdit) or (Sender = LegHeightSpin) then
    begin
      dblHeight:= LegHeightVal[muMillimeters];
      if Graphic.PrintOrigin = poTop then
        TempRect.Bottom:= TempRect.Top - dblHeight
      else
        TempRect.Top:= TempRect.Bottom + dblHeight;
    end
    else
      if (Sender = LegLeftPosEdit) or (Sender = LegLeftPosSpin) then
    begin
      TempRect.Left:= LegLeftPosVal[muMillimeters];
      TempRect.Right:= TempRect.Left + dblWidth;
    end
    else
      if (Sender = LegTopPosEdit) or (Sender = LegTopPosSpin) then
      if Graphic.PrintOrigin = poTop then
      begin
        TempRect.Top:= RectHeight(FPrintOptions.PageSetup.PageExtent) - LegTopPosVal[muMillimeters];
        TempRect.Bottom:= TempRect.Top - dblHeight;
      end
      else
      begin
        TempRect.Bottom:= LegTopPosVal[muMillimeters];
        TempRect.Top:= TempRect.Bottom + dblHeight;
      end;
    with Graphic do
    begin
      // center image if center-options checked
      if (PrintPosition = ppCenterHoriz) or (PrintPosition = ppCenter) then
      begin
        TempRect.Left:= (RectWidth(FPrintOptions.PageSetup.PageExtent) - dblWidth) * 0.5;
        TempRect.Right:= TempRect.Left + dblWidth;
      end;
      if (PrintPosition = ppCenterVert) or (PrintPosition = ppCenter) then
      begin
        TempRect.Bottom:= (RectHeight(FPrintOptions.PageSetup.PageExtent) - dblHeight) * 0.5;
        TempRect.Top:= TempRect.Bottom + dblHeight;
      end;
      // update legend print position, if it is necessary
      case PrintPosition of
        ppCustom: if not LegCustomBtn.Checked then
            LegCustomBtn.Checked:= TRUE;
        ppCenter: if not LegCenterBtn.Checked then
            LegCenterBtn.Checked:= TRUE;
        ppCenterHoriz: if not LegCenterHorBtn.Checked then
            LegCenterHorBtn.Checked:= TRUE;
        ppCenterVert: if not LegCenterVertBtn.Checked then
            LegCenterVertBtn.Checked:= TRUE;
      else begin
          PrintPosition:= ppCustom;
          if not LegCustomBtn.Checked then
            LegCustomBtn.Checked:= TRUE;
        end;
      end;
      // redraw necessity calculation
      intValue:= Abs(DblCompare(BoundsRect.Left, TempRect.Left));
      Inc(intValue, Abs(DblCompare(BoundsRect.Bottom, TempRect.Bottom)));
      Inc(intValue, Abs(DblCompare(BoundsRect.Right, TempRect.Right)));
      Inc(intValue, Abs(DblCompare(BoundsRect.Top, TempRect.Top)));
      if intValue <> 0 then boolNeedRedraw:= TRUE;
//++ Glukhov Bug#293(PrintFrame) Build#175 15.02.02
      if (Sender = chkLegFrmLeft) or (Sender = chkLegFrmTop)
      or (Sender = chkLegFrmRight) or (Sender = chkLegFrmBottom)
      then boolNeedRedraw:= TRUE;
//-- Glukhov Bug#293(PrintFrame) Build#175 15.02.02
      BoundsRect:= TempRect;
    end;
    LegWidthVal[muMillimeters]:= dblWidth;
    LegHeightVal[muMillimeters]:= dblHeight;
    LegLeftPosVal[muMillimeters]:= TempRect.Left;
    if Graphic.PrintOrigin = poTop then
      LegTopPosVal[muMillimeters]:= RectHeight(FPrintOptions.PageSetup.PageExtent) - TempRect.Top
    else
      LegTopPosVal[muMillimeters]:= TempRect.Bottom;
    // redraw if it is necessary
    if boolNeedRedraw then
    begin
      UpdateEditHandlerPosition;
      Scroller.InvalidateRect(PreviewSize);
      Scroller.InvalidateRect(TempRect);
    end;
  end;
end;

procedure TPrintingDialog.LegPositionUpdated(Sender: TObject);
var
  Graphic: TLegendPlaceholder;
begin
  with LegendCombo do
    if (Items.Count > 0) and (ItemIndex >= 0) then
      Graphic:= TLegendPlaceholder(Items.Objects[ItemIndex])
    else
      Exit;
  if FKeyPressed then
  begin
    with Graphic do
      if Sender = LegLeftPosSpin then
      begin
        if PrintPosition = ppCenter then
          PrintPosition:= ppCenterVert
        else
          if PrintPosition <> ppCenterVert then
          PrintPosition:= ppCustom;
        if not LegLeftPosEdit.Focused then
        begin
          LegLeftPosEdit.SetFocus;
          LegLeftPosEdit.SelLength:= 0;
        end;
      end
      else
        if Sender = LegTopPosSpin then
      begin
        if PrintPosition = ppCenter then
          PrintPosition:= ppCenterHoriz
        else
          if PrintPosition <> ppCenterHoriz then
          PrintPosition:= ppCustom;
        if not LegTopPosEdit.Focused then
        begin
          LegTopPosEdit.SetFocus;
          LegTopPosEdit.SelLength:= 0;
        end;
      end
      else
        if Sender = LegLeftPosEdit then
      begin
        if PrintPosition = ppCenter then
          PrintPosition:= ppCenterVert
        else
          if PrintPosition <> ppCenterVert then
          PrintPosition:= ppCustom;
      end
      else
        if Sender = LegTopPosEdit then
      begin
        if PrintPosition = ppCenter then
          PrintPosition:= ppCenterHoriz
        else
          if PrintPosition <> ppCenterHoriz then
          PrintPosition:= ppCustom;
      end;
    LegSizeOrPositionUpdated(Sender);
    OkBtn.Default:= TRUE;
  end;
  FSelectAll:= TRUE;
end;

procedure TPrintingDialog.LegOriginComboClick(Sender: TObject);
var
  Graphic: TLegendPlaceholder;
begin
  with LegendCombo do
    if (Items.Count > 0) and (ItemIndex >= 0) then
      Graphic:= TLegendPlaceholder(Items.Objects[ItemIndex])
    else
      Exit;
  with LegOriginCombo do
    Graphic.PrintOrigin:= TPrintOrigin(Items.Objects[ItemIndex]);
  if LegOriginCombo.ItemIndex = Ord(poTop) then
  begin
    LegTopLab.Caption:= MlgSection[38];
    LegTopPosVal[muMillimeters]:= RectHeight(FPrintOptions.PageSetup.PageExtent) - Graphic.BoundsRect.Top
  end
  else
  begin
    LegTopLab.Caption:= MlgSection[16];
    LegTopPosVal[muMillimeters]:= Graphic.BoundsRect.Bottom;
  end;
end;

procedure TPrintingDialog.PicOriginComboClick(Sender: TObject);
var
  Graphic: TWindowsGraphic;
  {++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
  IndX: Integer;
  IndY: Integer;
  BaseRect: TGrRect;
  {-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
begin
  with PicturesCombo do
    if (Items.Count > 0) and (ItemIndex >= 0) then
      Graphic:= TWindowsGraphic(Items.Objects[ItemIndex])
    else
      Exit;
  with PicOriginCombo do
    Graphic.PrintOrigin:= TPrintOrigin(Items.Objects[ItemIndex]);

  {++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
     // finding Page Extent which contains Picture by MAX intersection square
  Graphic.GetPageBaseExtent(BaseRect, IndX, IndY);
  {-- Moskaliov Pictures Location Control BUILD#131 18.09.00}

  if PicOriginCombo.ItemIndex = Ord(poTop) then
  begin
    PicTopLab.Caption:= MlgSection[38];
    {++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
    //       PicTopPosVal[muMillimeters]:=RectHeight(FPrintOptions.PageSetup.PageExtent)-Graphic.BoundsRect.Top
    PicTopPosVal[muMillimeters]:= BaseRect.Top - Graphic.BoundsRect.Top
      {-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
  end
  else
  begin
    PicTopLab.Caption:= MlgSection[16];
    {++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
    //       PicTopPosVal[muMillimeters]:=Graphic.BoundsRect.Bottom;
    PicTopPosVal[muMillimeters]:= Graphic.BoundsRect.Bottom - BaseRect.Bottom;
    {-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
  end;
end;

procedure TPrintingDialog.SetFieldsFromPictureRect;
var
  Graphic: TWindowsGraphic;
  dblWidth: Double;
  dblHeight: Double;
  {++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
  IndX: Integer;
  IndY: Integer;
  BaseRect: TGrRect;
  {-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
begin
  if (PicturesCombo.Items.Count > 0) and (PicturesCombo.ItemIndex >= 0) then
  begin
    with PicturesCombo do
      Graphic:= TWindowsGraphic(Items.Objects[ItemIndex]);
    {++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
    if Graphic.PrintLocation = plOnPages then
    begin
      if not PicLocOnPagesBtn.Checked then
        PicLocOnPagesBtn.Checked:= TRUE;
    end
    else
    begin
      Graphic.PrintLocation:= plOneTime;
      if not PicLocOneTimeBtn.Checked then
        PicLocOneTimeBtn.Checked:= TRUE;
    end;
    if PicFirstCheck.Checked <> Graphic.OnFirstPage then
      PicFirstCheck.Checked:= Graphic.OnFirstPage;
    if PicInterCheck.Checked <> Graphic.OnInternalPages then
      PicInterCheck.Checked:= Graphic.OnInternalPages;
    if PicLastCheck.Checked <> Graphic.OnLastPage then
      PicLastCheck.Checked:= Graphic.OnLastPage;

    {++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
             // finding Page Extent which contains Picture by MAX intersection square
    Graphic.GetPageBaseExtent(BaseRect, IndX, IndY);
    {-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
    dblWidth:= RectWidth(Graphic.BoundsRect);
    dblHeight:= RectHeight(Graphic.BoundsRect);
    //       PicLeftPosVal[muMillimeters]:=Graphic.BoundsRect.Left;
    PicLeftPosVal[muMillimeters]:= Graphic.BoundsRect.Left - BaseRect.Left;
    PicOriginCombo.ItemIndex:= Ord(Graphic.PrintOrigin);

    if PicOriginCombo.ItemIndex = Ord(poTop) then
      {++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
      //          PicTopPosVal[muMillimeters]:=RectHeight(FPrintOptions.PageSetup.PageExtent)-Graphic.BoundsRect.Top
    begin
      PicTopLab.Caption:= MlgSection[38];
      PicTopPosVal[muMillimeters]:= BaseRect.Top - Graphic.BoundsRect.Top
    end
    else
      //          PicTopPosVal[muMillimeters]:=Graphic.BoundsRect.Bottom;
    begin
      PicTopLab.Caption:= MlgSection[16];
      PicTopPosVal[muMillimeters]:= Graphic.BoundsRect.Bottom - BaseRect.Bottom;
    end;
    {-- Moskaliov Pictures Location Control BUILD#131 18.09.00}

    PicWidthVal[muMillimeters]:= dblWidth;
    PicHeightVal[muMillimeters]:= dblHeight;
    if KeepAspectCheck.Checked <> Graphic.KeepAspect then
      KeepAspectCheck.Checked:= Graphic.KeepAspect;
    if Graphic.KeepAspect then
    begin
      if dblWidth > dblHeight then
      begin
        PicWScaleVal[muPercent]:= (dblWidth * 100.0) / RectWidth(Graphic.OriginalBoundsRect);
        PicHScaleVal[muPercent]:= PicWScaleVal[muPercent];
      end
      else
      begin
        PicHScaleVal[muPercent]:= (dblHeight * 100.0) / RectHeight(Graphic.OriginalBoundsRect);
        PicWScaleVal[muPercent]:= PicHScaleVal[muPercent];
      end;
      if FCurrentItem = @Graphic.BoundsRect then
        EditHandler.Options:= EditHandler.Options + [ehoKeepAspect];
    end
    else
    begin
      PicWScaleVal[muPercent]:= (dblWidth * 100.0) / RectWidth(Graphic.OriginalBoundsRect);
      PicHScaleVal[muPercent]:= (dblHeight * 100.0) / RectHeight(Graphic.OriginalBoundsRect);
      if FCurrentItem = @Graphic.BoundsRect then
        EditHandler.Options:= EditHandler.Options - [ehoKeepAspect];
    end;
    if Graphic.PrintPosition = ppCenter then
    begin
      if not PicCenterBtn.Checked then
        PicCenterBtn.Checked:= TRUE;
    end
    else
      if Graphic.PrintPosition = ppCenterVert then
    begin
      if not PicCenterVertBtn.Checked then
        PicCenterVertBtn.Checked:= TRUE;
    end
    else
      if Graphic.PrintPosition = ppCenterHoriz then
    begin
      if not PicCenterHorBtn.Checked then
        PicCenterHorBtn.Checked:= TRUE;
    end
    else
    begin
      Graphic.PrintPosition:= ppCustom;
      if not PicCustomBtn.Checked then
        PicCustomBtn.Checked:= TRUE;
    end;
  end
  else
  begin
    PicLeftPosVal[muMillimeters]:= 0;
    PicTopPosVal[muMillimeters]:= 0;
    PicWidthVal[muMillimeters]:= 0;
    PicHeightVal[muMillimeters]:= 0;
    PicWScaleVal[muPercent]:= 100;
    PicHScaleVal[muPercent]:= 100;
    KeepAspectCheck.Checked:= TRUE;
  end;
end;

procedure TPrintingDialog.PicSizeUpdated(Sender: TObject);
begin
  if FKeyPressed then
  begin
    if Sender = PicWidthSpin then
    begin
      if not PicWidthEdit.Focused then
      begin
        PicWidthEdit.SetFocus;
        PicWidthEdit.SelLength:= 0;
      end;
    end
    else
      if Sender = PicHeightSpin then
    begin
      if not PicHeightEdit.Focused then
      begin
        PicHeightEdit.SetFocus;
        PicHeightEdit.SelLength:= 0;
      end;
    end
    else
      if Sender = PicWScaleSpin then
    begin
      if not PicWScaleEdit.Focused then
      begin
        PicWScaleEdit.SetFocus;
        PicWScaleEdit.SelLength:= 0;
      end;
    end
    else
      if Sender = PicHScaleSpin then
      if not PicHScaleEdit.Focused then
      begin
        PicHScaleEdit.SetFocus;
        PicHScaleEdit.SelLength:= 0;
      end;
    PicSizeOrPositionUpdated(Sender);
    OkBtn.Default:= TRUE;
  end;
  FSelectAll:= TRUE;
end;

procedure TPrintingDialog.PicPositionUpdated(Sender: TObject);
var
  Graphic: TWindowsGraphic;
begin
  with PicturesCombo do
    if (Items.Count > 0) and (ItemIndex >= 0) then
      Graphic:= TWindowsGraphic(Items.Objects[ItemIndex])
    else
      Exit;
  if FKeyPressed then
  begin
    with Graphic do
      if Sender = PicLeftPosSpin then
      begin
        if PrintPosition = ppCenter then
          PrintPosition:= ppCenterVert
        else
          if PrintPosition <> ppCenterVert then
          PrintPosition:= ppCustom;
        if not PicLeftPosEdit.Focused then
        begin
          PicLeftPosEdit.SetFocus;
          PicLeftPosEdit.SelLength:= 0;
        end;
      end
      else
        if Sender = PicTopPosSpin then
      begin
        if PrintPosition = ppCenter then
          PrintPosition:= ppCenterHoriz
        else
          if PrintPosition <> ppCenterHoriz then
          PrintPosition:= ppCustom;
        if not PicTopPosEdit.Focused then
        begin
          PicTopPosEdit.SetFocus;
          PicTopPosEdit.SelLength:= 0;
        end;
      end
      else
        if Sender = PicLeftPosEdit then
      begin
        if PrintPosition = ppCenter then
          PrintPosition:= ppCenterVert
        else
          if PrintPosition <> ppCenterVert then
          PrintPosition:= ppCustom;
      end
      else
        if Sender = PicTopPosEdit then
      begin
        if PrintPosition = ppCenter then
          PrintPosition:= ppCenterHoriz
        else
          if PrintPosition <> ppCenterHoriz then
          PrintPosition:= ppCustom;
      end;
    PicSizeOrPositionUpdated(Sender);
    OkBtn.Default:= TRUE;
  end;
  FSelectAll:= TRUE;
end;

procedure TPrintingDialog.PicSizeOrPositionUpdated(Sender: TObject);
var
  boolNeedRedraw: Boolean;
  PreviewSize: TGrRect;
  Graphic: TWindowsGraphic;
  TempRect: TGrRect;
  dblWidth: Double;
  dblHeight: Double;
  intValue: Integer;
  {++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
  IndX: Integer;
  IndY: Integer;
  BaseRect: TGrRect;
  {-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
begin
  with PicturesCombo do
    if (Items.Count > 0) and (ItemIndex >= 0) then
      Graphic:= TWindowsGraphic(Items.Objects[ItemIndex])
    else
      Exit;
  if (not FUpdating) and (not RectEmpty(Graphic.BoundsRect)) then
  begin
    // store the current preview-position for later invalidate
    PreviewSize:= Graphic.BoundsRect;
    boolNeedRedraw:= FALSE;
    TempRect:= Graphic.BoundsRect;
    dblWidth:= RectWidth(TempRect);
    dblHeight:= RectHeight(TempRect);

    {++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
             // finding Page Extent which contains Picture by MAX intersection square
    Graphic.GetPageBaseExtent(BaseRect, IndX, IndY);
    {-- Moskaliov Pictures Location Control BUILD#131 18.09.00}

    if (Sender = PicWidthEdit) or (Sender = PicWidthSpin) or
      (Sender = PicWScaleEdit) or (Sender = PicWScaleSpin) then
    begin
      if (Sender = PicWidthEdit) or (Sender = PicWidthSpin) then
        dblWidth:= PicWidthVal[muMillimeters]
      else
        dblWidth:= RectWidth(Graphic.OriginalBoundsRect) *
          (PicWScaleVal[muPercent] / 100.0);
      TempRect.Right:= TempRect.Left + dblWidth;
      if Graphic.KeepAspect then
      begin
        dblHeight:= dblWidth * (RectHeight(Graphic.OriginalBoundsRect) /
          RectWidth(Graphic.OriginalBoundsRect));
        if Graphic.PrintOrigin = poTop then
          TempRect.Bottom:= TempRect.Top - dblHeight
        else
          TempRect.Top:= TempRect.Bottom + dblHeight;
      end;
    end
    else
      if (Sender = PicHeightEdit) or (Sender = PicHeightSpin) or
      (Sender = PicHScaleEdit) or (Sender = PicHScaleSpin) then
    begin
      if (Sender = PicHeightEdit) or (Sender = PicHeightSpin) then
        dblHeight:= PicHeightVal[muMillimeters]
      else
        dblHeight:= RectHeight(Graphic.OriginalBoundsRect) *
          (PicHScaleVal[muPercent] / 100.0);
      if Graphic.PrintOrigin = poTop then
        TempRect.Bottom:= TempRect.Top - dblHeight
      else
        TempRect.Top:= TempRect.Bottom + dblHeight;
      if Graphic.KeepAspect then
      begin
        dblWidth:= dblHeight * (RectWidth(Graphic.OriginalBoundsRect) /
          RectHeight(Graphic.OriginalBoundsRect));
        TempRect.Right:= TempRect.Left + dblWidth;
      end;
    end
    else
      if (Sender = PicLeftPosEdit) or (Sender = PicLeftPosSpin) then
    begin
      //                   TempRect.Left:=PicLeftPosVal[muMillimeters];
      TempRect.Left:= PicLeftPosVal[muMillimeters] + BaseRect.Left;
      TempRect.Right:= TempRect.Left + dblWidth;
    end
    else
      if (Sender = PicTopPosEdit) or (Sender = PicTopPosSpin) then
      if Graphic.PrintOrigin = poTop then
      begin
        //                         TempRect.Top:=RectHeight(FPrintOptions.PageSetup.PageExtent)-PicTopPosVal[muMillimeters];
        TempRect.Top:= BaseRect.Top - PicTopPosVal[muMillimeters];
        TempRect.Bottom:= TempRect.Top - dblHeight;
      end
      else
      begin
        //                         TempRect.Bottom:=PicTopPosVal[muMillimeters];
        TempRect.Bottom:= BaseRect.Bottom + PicTopPosVal[muMillimeters];
        TempRect.Top:= TempRect.Bottom + dblHeight;
      end
        {++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
    else
      if Sender = EditHandler then
      boolNeedRedraw:= TRUE;
    {-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
    with Graphic do
    begin
      // center image if center-options checked
      if (PrintPosition = ppCenterHoriz) or (PrintPosition = ppCenter) then
      begin
        {++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
        //                   TempRect.Left:=(RectWidth(FPrintOptions.PageSetup.PageExtent)-dblWidth)*0.5;
        TempRect.Left:= (BaseRect.Left + BaseRect.Right - dblWidth) * 0.5;
        TempRect.Right:= TempRect.Left + dblWidth;
        {-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
      end;
      if (PrintPosition = ppCenterVert) or (PrintPosition = ppCenter) then
      begin
        {++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
        //                   TempRect.Bottom:=(RectHeight(FPrintOptions.PageSetup.PageExtent)-dblHeight)*0.5;
        TempRect.Bottom:= (BaseRect.Bottom + BaseRect.Top - dblHeight) * 0.5;
        TempRect.Top:= TempRect.Bottom + dblHeight;
        {-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
      end;
      // update picture print position, if it is necessary
      case PrintPosition of
        ppCustom: if not PicCustomBtn.Checked then
            PicCustomBtn.Checked:= TRUE;
        ppCenter: if not PicCenterBtn.Checked then
            PicCenterBtn.Checked:= TRUE;
        ppCenterHoriz: if not PicCenterHorBtn.Checked then
            PicCenterHorBtn.Checked:= TRUE;
        ppCenterVert: if not PicCenterVertBtn.Checked then
            PicCenterVertBtn.Checked:= TRUE;
      else begin
          PrintPosition:= ppCustom;
          if not PicCustomBtn.Checked then
            PicCustomBtn.Checked:= TRUE;
        end;
      end;
      // redraw necessity calculation
      intValue:= Abs(DblCompare(BoundsRect.Left, TempRect.Left));
      Inc(intValue, Abs(DblCompare(BoundsRect.Bottom, TempRect.Bottom)));
      Inc(intValue, Abs(DblCompare(BoundsRect.Right, TempRect.Right)));
      Inc(intValue, Abs(DblCompare(BoundsRect.Top, TempRect.Top)));
      if intValue <> 0 then
        boolNeedRedraw:= TRUE;
      {++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
      if boolNeedRedraw then
        Invalidate(Scroller);
      {-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
      BoundsRect:= TempRect;
    end;
    PicWidthVal[muMillimeters]:= dblWidth;
    PicHeightVal[muMillimeters]:= dblHeight;
    PicWScaleVal[muPercent]:= (dblWidth * 100.0) / RectWidth(Graphic.OriginalBoundsRect);
    PicHScaleVal[muPercent]:= (dblHeight * 100.0) / RectHeight(Graphic.OriginalBoundsRect);
    {++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
    //       PicLeftPosVal[muMillimeters]:=TempRect.Left;
    PicLeftPosVal[muMillimeters]:= TempRect.Left - BaseRect.Left;
    if Graphic.PrintOrigin = poTop then
      //          PicTopPosVal[muMillimeters]:=RectHeight(FPrintOptions.PageSetup.PageExtent)-TempRect.Top
      PicTopPosVal[muMillimeters]:= BaseRect.Top - TempRect.Top
    else
      //          PicTopPosVal[muMillimeters]:=TempRect.Bottom;
      PicTopPosVal[muMillimeters]:= TempRect.Bottom - BaseRect.Bottom;
    {-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
             // redraw if it is necessary
    if boolNeedRedraw then
    begin
      UpdateEditHandlerPosition;
      {++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
      //             Scroller.InvalidateRect(PreviewSize);
      //             Scroller.InvalidateRect(TempRect);
      Graphic.Invalidate(Scroller);
      {-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
    end;
  end;
end;

procedure TPrintingDialog.PicKeepAspectCheckClick(Sender: TObject);
var
  Graphic: TWindowsGraphic;
  TempRect: TGrRect;
  Scale1: Double;
  Scale2: Double;
begin
  with PicturesCombo do
    if (Items.Count > 0) and (ItemIndex >= 0) then
      Graphic:= TWindowsGraphic(Items.Objects[ItemIndex])
    else
      Exit;
  Graphic.KeepAspect:= KeepAspectCheck.Checked;
  if KeepAspectCheck.Checked then
  begin
    with Graphic do
    begin
      Scale1:= RectWidth(BoundsRect) / RectWidth(OriginalBoundsRect);
      Scale2:= RectHeight(BoundsRect) / RectHeight(OriginalBoundsRect);
      {++ Moskaliov AXWinGIS Printing Methods BUILD#133 25.10.00}
      //             if Scale1 > Scale2 then
      {-- Moskaliov AXWinGIS Printing Methods BUILD#133 25.10.00}
      Scale1:= (Scale1 + Scale2) * 0.5;
      TempRect:= BoundsRect;
      TempRect.Right:= BoundsRect.Left + Scale1 * RectWidth(OriginalBoundsRect);
      if PrintOrigin = poTop then
        TempRect.Bottom:= BoundsRect.Top - Scale1 * RectHeight(OriginalBoundsRect)
      else
        TempRect.Top:= BoundsRect.Bottom + Scale1 * RectHeight(OriginalBoundsRect);
      {++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
      //             Scroller.InvalidateRect(BoundsRect);
      Invalidate(Scroller);
      {-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
      BoundsRect:= TempRect;
      if CurrentItem = @BoundsRect then
      begin
        EditHandler.Options:= EditHandler.Options + [ehoKeepAspect];
        CurrentItem:= nil;
        CurrentItem:= @BoundsRect
      end;
      {++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
      //             Scroller.InvalidateRect(BoundsRect);
      Invalidate(Scroller);
      {-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
    end;
    SetFieldsFromPictureRect;
  end
  else
    if FCurrentItem = @Graphic.BoundsRect then
    EditHandler.Options:= EditHandler.Options - [ehoKeepAspect];
  UpdateEnabled;
end;

{++ Moskaliov Pictures Location Control BUILD#131 18.09.00}

procedure TPrintingDialog.PicLocationBtnClick(Sender: TObject);
var
  Graphic: TWindowsGraphic;
begin
  with PicturesCombo do
    if (Items.Count > 0) and (ItemIndex >= 0) then
      Graphic:= TWindowsGraphic(Items.Objects[ItemIndex])
    else
      Exit;
  if PicLocOneTimeBtn.Checked then
    Graphic.PrintLocation:= plOneTime
  else
    if PicLocOnPagesBtn.Checked then
    Graphic.PrintLocation:= plOnPages;
  if PreviewBtn.Down then
    Graphic.Invalidate(Scroller);
  UpdateEnabled;
end;

procedure TPrintingDialog.PicLocationCheckClick(Sender: TObject);
var
  Graphic: TWindowsGraphic;
begin
  with PicturesCombo do
    if (Items.Count > 0) and (ItemIndex >= 0) then
      Graphic:= TWindowsGraphic(Items.Objects[ItemIndex])
    else
      Exit;
  Graphic.OnFirstPage:= PicFirstCheck.Checked;
  Graphic.OnInternalPages:= PicInterCheck.Checked;
  Graphic.OnLastPage:= PicLastCheck.Checked;
  if PreviewBtn.Down then
    Graphic.Invalidate(Scroller);
end;
{-- Moskaliov Pictures Location Control BUILD#131 18.09.00}

procedure TPrintingDialog.PicPositionBtnClick(Sender: TObject);
var
  Graphic: TWindowsGraphic;
begin
  with PicturesCombo do
    if (Items.Count > 0) and (ItemIndex >= 0) then
      Graphic:= TWindowsGraphic(Items.Objects[ItemIndex])
    else
      Exit;
  if PicCustomBtn.Checked then
    Graphic.PrintPosition:= ppCustom
  else
    if PicCenterHorBtn.Checked then
    Graphic.PrintPosition:= ppCenterHoriz
  else
    if PicCenterVertBtn.Checked then
    Graphic.PrintPosition:= ppCenterVert
  else
    if PicCenterBtn.Checked then
    Graphic.PrintPosition:= ppCenter;
  PicSizeOrPositionUpdated(Sender);
end;

procedure TPrintingDialog.PicturesComboClick(Sender: TObject);
var
  Graphic: TWindowsGraphic;
begin
  with PicturesCombo do
    if (Items.Count > 0) and (ItemIndex >= 0) then
      Graphic:= TWindowsGraphic(Items.Objects[ItemIndex])
    else
      Exit;
  CurrentItem:= @Graphic.BoundsRect;
  SetFieldsFromPictureRect;
end;

procedure TPrintingDialog.OrderBtnClick(Sender: TObject);
var
  Index: Integer;
  Graphic: TCustomGraphic;
begin
  if CurrentItem <> nil then
    for Index:= 0 to FGraphics.ItemCount - 1 do
      with FGraphics.Items[Index] do
        if CurrentItem = @BoundsRect then
        begin
          Graphic:= FGraphics.Items[Index];
          if Sender = BringToFrontBtn then
          begin
            if Index < FGraphics.ItemCount - 1 then
            begin
              CurrentItem:= nil;
              FGraphics.Delete(Index);
              FGraphics.Insert(FGraphics.ItemCount, Graphic);
              CurrentItem:= @BoundsRect;
              Invalidate(Scroller);
            end;
          end
          else
            if Sender = BringForwardBtn then
          begin
            if Index < FGraphics.ItemCount - 1 then
            begin
              CurrentItem:= nil;
              FGraphics.Delete(Index);
              FGraphics.Insert(Index + 1, Graphic);
              CurrentItem:= @BoundsRect;
              Invalidate(Scroller);
            end;
          end
          else
            if Sender = SendBackwardBtn then
          begin
            if Index > 0 then
            begin
              CurrentItem:= nil;
              FGraphics.Delete(Index);
              FGraphics.Insert(Index - 1, Graphic);
              CurrentItem:= @BoundsRect;
              Invalidate(Scroller);
            end;
          end
          else
            if Sender = SendToBackBtn then
          begin
            if Index > 0 then
            begin
              CurrentItem:= nil;
              FGraphics.Delete(Index);
              FGraphics.Insert(0, Graphic);
              CurrentItem:= @BoundsRect;
              Invalidate(Scroller);
            end;
          end;
          Break;
        end;
end;

procedure TPrintingDialog.SigOriginComboClick(Sender: TObject);
var
  Graphic: TPrintSignature;
  IndX: Integer;
  IndY: Integer;
  BaseRect: TGrRect;
begin
  with SignaturesCombo do
    if (Items.Count > 0) and (ItemIndex >= 0) then
      Graphic:= TPrintSignature(Items.Objects[ItemIndex])
    else
      Exit;
  with SigOriginCombo do
    Graphic.PrintOrigin:= TPrintOrigin(Items.Objects[ItemIndex]);

  {++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
     // finding Page Extent which contains Text by MAX intersection square
  Graphic.GetPageBaseExtent(BaseRect, IndX, IndY);
  {-- Moskaliov Pictures Location Control BUILD#131 18.09.00}

  if SigOriginCombo.ItemIndex = Ord(poTop) then
  begin
    SigTopLab.Caption:= MlgSection[38];
    SigTopPosVal[muMillimeters]:= BaseRect.Top - Graphic.BoundsRect.Top
  end
  else
  begin
    SigTopLab.Caption:= MlgSection[16];
    SigTopPosVal[muMillimeters]:= Graphic.BoundsRect.Bottom - BaseRect.Bottom;
  end;
end;

procedure TPrintingDialog.SigPositionUpdated(Sender: TObject);
begin
  if FKeyPressed then
  begin
    if Sender = SigLeftPosSpin then
    begin
      if SigCenterBtn.Checked then
        SigCenterVertBtn.Checked:= TRUE
      else
        if not SigCenterVertBtn.Checked then
        SigCustomBtn.Checked:= TRUE;
      if not SigLeftPosEdit.Focused then
      begin
        SigLeftPosEdit.SetFocus;
        SigLeftPosEdit.SelLength:= 0;
      end;
    end
    else
      if Sender = SigTopPosSpin then
    begin
      if SigCenterBtn.Checked then
        SigCenterHorBtn.Checked:= TRUE
      else
        if not SigCenterHorBtn.Checked then
        SigCustomBtn.Checked:= TRUE;
      if not SigTopPosEdit.Focused then
      begin
        SigTopPosEdit.SetFocus;
        SigTopPosEdit.SelLength:= 0;
      end;
    end
    else
      if Sender = SigLeftPosEdit then
    begin
      if SigCenterBtn.Checked then
        SigCenterVertBtn.Checked:= TRUE
      else
        if not SigCenterVertBtn.Checked then
        SigCustomBtn.Checked:= TRUE;
    end
    else
      if Sender = SigTopPosEdit then
    begin
      if SigCenterBtn.Checked then
        SigCenterHorBtn.Checked:= TRUE
      else
        if not SigCenterHorBtn.Checked then
        SigCustomBtn.Checked:= TRUE;
    end;
    SigSizeOrPositionUpdated(Sender);
    OkBtn.Default:= TRUE;
  end;
  FSelectAll:= TRUE;
end;

procedure TPrintingDialog.SigSizeOrPositionUpdated(Sender: TObject);
var
  boolNeedRedraw: Boolean;
  Graphic: TPrintSignature;
  TempRect: TGrRect;
  dblWidth: Double;
  dblHeight: Double;
  dblPosX: Double;
  intValue: Integer;
  CntX: Integer;
  CntY: Integer;
  IndX: Integer;
  IndY: Integer;
  BaseRect: TGrRect;
  LCnt: Integer;
  MaxWidth: Double;
  i: Integer;
begin
  if (SignaturesCombo.Items.Count > 0) and
     (SignaturesCombo.ItemIndex >= 0)
  then  Graphic:= TPrintSignature(SignaturesCombo.Items.Objects[SignaturesCombo.ItemIndex])
  else  Exit;
  if (not FUpdating) and (not RectEmpty(Graphic.BoundsRect)) then
  begin
    boolNeedRedraw:= FALSE;
    TempRect:= Graphic.BoundsRect;
    dblWidth:= RectWidth(TempRect);
    dblHeight:= RectHeight(TempRect);
{++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
    // finding Page Extent which contains Text by MAX intersection square
    Graphic.GetPageBaseExtent(BaseRect, IndX, IndY);
{-- Moskaliov Pictures Location Control BUILD#131 18.09.00}
    case Graphic.TextStyle.Alignment of
      Ord(taRight): dblPosX:= TempRect.Right;
      Ord(taCentered): dblPosX:= (TempRect.Left + TempRect.Right) * 0.5;
    else // taLeft
      dblPosX:= TempRect.Left;
    end;

    LCnt:=Graphic.LineCount;

    if (Sender = FontDlgBtn) or (LCnt > 1) then
    begin
//
//++ Glukhov PrintSignature Build#166 17.12.01
      // Show font of signatre
      lblSigFont.Caption:= Format( MlgSection[134],
          [Graphic.TextStyle.FontName, Trunc(Graphic.TextStyle.Height)] );
      if Graphic.TextStyle.Transparent = 1
      then  lblSigFont.Caption:= lblSigFont.Caption + MlgSection[135]
      else  lblSigFont.Caption:= lblSigFont.Caption + MlgSection[136];

      // set font settings for label to create a font sample
      lblSigFont.Font.Name:= Graphic.TextStyle.FontName;
      lblSigFont.Font.Color:= Graphic.ForeColor;
      with lblSigFont.Font do
      begin
        if Graphic.TextStyle.Italic = 1
        then  Style:= Style + [fsItalic]
        else  Style:= Style - [fsItalic];
        if Graphic.TextStyle.Bold = 1
        then  Style:= Style + [fsBold]
        else  Style:= Style - [fsBold];
        if Graphic.TextStyle.Underlined = 1
        then  Style:= Style + [fsUnderline]
        else  Style:= Style - [fsUnderline];
      end;
//-- Glukhov PrintSignature Build#166 17.12.01
      with PreviewWindow.Canvas, Graphic do
      begin
        // set font settings for Bounds Rectangle calculation
        Font.Name:= TextStyle.FontName;
//++ Glukhov PrintSignature Build#166 17.01.02
// Font.Size should be negative (excluding the internal leading)
//        Font.Size:= Round(TextStyle.Height);
        // Extend the rectangle a bit, increasing size by 1
        Font.Size:= -Trunc(Abs(TextStyle.Height))-1;
        // Calculate Font.Height, taking in account the window scale
        dblHeight:= (Font.Size * 25.4) / 72;
        dblHeight:= Scroller.InternalToWindow(dblHeight);
        Font.Height:= -Round(dblHeight);
//-- Glukhov PrintSignature Build#166 17.01.02
        Font.Style:= [];
        if TextStyle.Italic = 1 then Font.Style:= Font.Style + [fsItalic];
        if TextStyle.Bold = 1 then Font.Style:= Font.Style + [fsBold];
        if TextStyle.Underlined = 1 then Font.Style:= Font.Style + [fsUnderline];
        with Owner.PageSetup do
        begin
          CntY:= StartPageNum - 1 + VerticalPages * HorizontalPages;
          CntX:= 9;
          CntY:= CntY div 10;
          while CntY > 0 do
          begin
            CntX:= CntX * 10 + 9;
            CntY:= CntY div 10;
          end;
          Owner.CurrPrintPage:= 1;// CntX; // for text rectangle width increasment
        end;

        MaxWidth:=0;
        for i:=0 to LCnt-1 do begin
            dblWidth:= TextWidth(GetLine(i));
            dblWidth:= Scroller.WindowToInternal(dblWidth); // Text width in mm
            if dblWidth > MaxWidth then begin
               MaxWidth:=dblWidth;
            end;
        end;
        dblWidth:=MaxWidth;


        dblHeight:= TextHeight(SignatureValue);
        dblHeight:= Scroller.WindowToInternal(dblHeight); // Text height in mm

        dblHeight:=dblHeight*LCnt;

        if PrintOrigin = poTop
        then  TempRect.Bottom:= TempRect.Top - dblHeight
        else  TempRect.Top:= TempRect.Bottom + dblHeight;
      end;
      boolNeedRedraw:= TRUE; // because SigFontDlgBtnClick() makes some modifications
    end
    else if (Sender = SigLeftPosEdit) or (Sender = SigLeftPosSpin) then
      dblPosX:= SigLeftPosVal[muMillimeters] + BaseRect.Left
    else if (Sender = SigTopPosEdit) or (Sender = SigTopPosSpin) then
      if Graphic.PrintOrigin = poTop then begin
        TempRect.Top:= BaseRect.Top - SigTopPosVal[muMillimeters];
        TempRect.Bottom:= TempRect.Top - dblHeight;
      end
      else begin
        TempRect.Bottom:= BaseRect.Bottom + SigTopPosVal[muMillimeters];
        TempRect.Top:= TempRect.Bottom + dblHeight;
      end;
    with Graphic do
    begin
      // center image if center-options checked
      if (PrintPosition = ppCenterHoriz) or (PrintPosition = ppCenter) then
        dblPosX:= (BaseRect.Left + BaseRect.Right) * 0.5;
      if (PrintPosition = ppCenterVert) or (PrintPosition = ppCenter) then
      begin
        TempRect.Bottom:= (BaseRect.Bottom + BaseRect.Top - dblHeight) * 0.5;
        TempRect.Top:= TempRect.Bottom + dblHeight;
      end;
      case TextStyle.Alignment of
        Ord(taRight): begin
            TempRect.Right:= dblPosX;
            TempRect.Left:= TempRect.Right - dblWidth;
          end;
        Ord(taCentered): begin
            TempRect.Left:= dblPosX - dblWidth * 0.5;
            TempRect.Right:= TempRect.Left + dblWidth;
          end;
      else { taLeft }  begin
          TempRect.Left:= dblPosX;
          TempRect.Right:= TempRect.Left + dblWidth;
        end;
      end;
      // update signature print position, if it is necessary
      case PrintPosition of
        ppCustom:
          if not SigCustomBtn.Checked then SigCustomBtn.Checked:= TRUE;
        ppCenter:
          if not SigCenterBtn.Checked then SigCenterBtn.Checked:= TRUE;
        ppCenterHoriz:
          if not SigCenterHorBtn.Checked then SigCenterHorBtn.Checked:= TRUE;
        ppCenterVert:
          if not SigCenterVertBtn.Checked then SigCenterVertBtn.Checked:= TRUE;
        else begin
          PrintPosition:= ppCustom;
          if not SigCustomBtn.Checked then SigCustomBtn.Checked:= TRUE;
        end;
      end;
      // redraw necessity calculation
      if not boolNeedRedraw then
      begin
        intValue:= Abs(DblCompare(BoundsRect.Left, TempRect.Left));
        Inc(intValue, Abs(DblCompare(BoundsRect.Bottom, TempRect.Bottom)));
        Inc(intValue, Abs(DblCompare(BoundsRect.Right, TempRect.Right)));
        Inc(intValue, Abs(DblCompare(BoundsRect.Top, TempRect.Top)));
        if intValue <> 0 then boolNeedRedraw:= TRUE;
      end;
      // redraw previous rectangles, if it is necessary
      if boolNeedRedraw then Invalidate(Scroller);
      BoundsRect:= TempRect;
    end;
    SigLeftPosVal[muMillimeters]:= dblPosX - BaseRect.Left;
    if Graphic.PrintOrigin = poTop
    then  SigTopPosVal[muMillimeters]:= BaseRect.Top - TempRect.Top
    else  SigTopPosVal[muMillimeters]:= TempRect.Bottom - BaseRect.Bottom;
    // redraw if it is necessary
    if boolNeedRedraw then
    begin
      UpdateEditHandlerPosition;
      Graphic.Invalidate(Scroller);
    end;
  end;
end;

procedure TPrintingDialog.SigPositionBtnClick(Sender: TObject);
var
  Graphic: TPrintSignature;
begin
  with SignaturesCombo do
    if (Items.Count > 0) and (ItemIndex >= 0) then
      Graphic:= TPrintSignature(Items.Objects[ItemIndex])
    else
      Exit;
  if SigCustomBtn.Checked then
    Graphic.PrintPosition:= ppCustom
  else
    if SigCenterHorBtn.Checked then
    Graphic.PrintPosition:= ppCenterHoriz
  else
    if SigCenterVertBtn.Checked then
    Graphic.PrintPosition:= ppCenterVert
  else
    if SigCenterBtn.Checked then
    Graphic.PrintPosition:= ppCenter;
  SigSizeOrPositionUpdated(Sender);
end;

procedure TPrintingDialog.SetFieldsFromSignatureRect;
var
  Graphic: TPrintSignature;
  IndX: Integer;
  IndY: Integer;
  BaseRect: TGrRect;
begin
  if (SignaturesCombo.Items.Count > 0) and (SignaturesCombo.ItemIndex >= 0) then
  begin
    with SignaturesCombo do
      Graphic:= TPrintSignature(Items.Objects[ItemIndex]);
    if Graphic.PrintLocation = plOnPages then
    begin
      if not SigLocOnPagesBtn.Checked then
        SigLocOnPagesBtn.Checked:= TRUE;
    end
    else
    begin
      Graphic.PrintLocation:= plOneTime;
      if not SigLocOneTimeBtn.Checked then
        SigLocOneTimeBtn.Checked:= TRUE;
    end;
    if SigFirstCheck.Checked <> Graphic.OnFirstPage then
      SigFirstCheck.Checked:= Graphic.OnFirstPage;
    if SigInterCheck.Checked <> Graphic.OnInternalPages then
      SigInterCheck.Checked:= Graphic.OnInternalPages;
    if SigLastCheck.Checked <> Graphic.OnLastPage then
      SigLastCheck.Checked:= Graphic.OnLastPage;

//++ Glukhov PrintSignature Build#166 17.12.01
      // Show font of signatre
      lblSigFont.Caption:= Format( MlgSection[134],
          [Graphic.TextStyle.FontName, Trunc(Graphic.TextStyle.Height)] );
      if Graphic.TextStyle.Transparent = 1
      then  lblSigFont.Caption:= lblSigFont.Caption + MlgSection[135]
      else  lblSigFont.Caption:= lblSigFont.Caption + MlgSection[136];

      // set font settings for label to create a font sample
      lblSigFont.Font.Name:= Graphic.TextStyle.FontName;
      lblSigFont.Font.Color:= Graphic.ForeColor;
      with lblSigFont.Font do
      begin
        if Graphic.TextStyle.Italic = 1
        then  Style:= Style + [fsItalic]
        else  Style:= Style - [fsItalic];
        if Graphic.TextStyle.Bold = 1
        then  Style:= Style + [fsBold]
        else  Style:= Style - [fsBold];
        if Graphic.TextStyle.Underlined = 1
        then  Style:= Style + [fsUnderline]
        else  Style:= Style - [fsUnderline];
      end;
//-- Glukhov PrintSignature Build#166 17.12.01

{++ Moskaliov Pictures Location Control BUILD#131 18.09.00}
             // finding Page Extent which contains Text by MAX intersection square
    Graphic.GetPageBaseExtent(BaseRect, IndX, IndY);
{-- Moskaliov Pictures Location Control BUILD#131 18.09.00}

    with Graphic do
    begin
      case TextStyle.Alignment of
        Ord(taRight): SigLeftPosVal[muMillimeters]:= BoundsRect.Right - BaseRect.Left;
        Ord(taCentered): SigLeftPosVal[muMillimeters]:= (BoundsRect.Left + BoundsRect.Right - BaseRect.Left) * 0.5;
      else { taLeft }
        SigLeftPosVal[muMillimeters]:= BoundsRect.Left - BaseRect.Left;
      end;
    end;

    SigOriginCombo.ItemIndex:= Ord(Graphic.PrintOrigin);
    if SigOriginCombo.ItemIndex = Ord(poTop) then
    begin
      SigTopLab.Caption:= MlgSection[38];
      //             SigTopPosVal[muMillimeters]:=RectHeight(FPrintOptions.PageSetup.PageExtent)-Graphic.BoundsRect.Top
      SigTopPosVal[muMillimeters]:= BaseRect.Top - Graphic.BoundsRect.Top
    end
    else
    begin
      SigTopLab.Caption:= MlgSection[16];
      //             SigTopPosVal[muMillimeters]:=Graphic.BoundsRect.Bottom;
      SigTopPosVal[muMillimeters]:= Graphic.BoundsRect.Bottom - BaseRect.Bottom;
    end;

    if Graphic.PrintPosition = ppCenter then
    begin
      if not SigCenterBtn.Checked then
        SigCenterBtn.Checked:= TRUE;
    end
    else
      if Graphic.PrintPosition = ppCenterVert then
    begin
      if not SigCenterVertBtn.Checked then
        SigCenterVertBtn.Checked:= TRUE;
    end
    else
      if Graphic.PrintPosition = ppCenterHoriz then
    begin
      if not SigCenterHorBtn.Checked then
        SigCenterHorBtn.Checked:= TRUE;
    end
    else
    begin
      Graphic.PrintPosition:= ppCustom;
      if not SigCustomBtn.Checked then
        SigCustomBtn.Checked:= TRUE;
    end;
  end
  else
  begin
    SigLeftPosVal[muMillimeters]:= 0;
    SigTopPosVal[muMillimeters]:= 0;
  end;
end;

procedure TPrintingDialog.SignaturesComboClick(Sender: TObject);
var
  Graphic: TPrintSignature;
begin
  with SignaturesCombo do
    if (Items.Count > 0) and (ItemIndex >= 0) then
    begin
      FSignatureIndex:= ItemIndex;
      Graphic:= TPrintSignature(Items.Objects[ItemIndex]);
      CurrentItem:= @Graphic.BoundsRect;
      SetFieldsFromSignatureRect;
    end;
end;

procedure TPrintingDialog.SignatureEditExit(Sender: TObject);
var
  Graphic: TPrintSignature;
begin
  if FKeyPressed then
  begin
    with SignaturesCombo do
      if (Items.Count > 0) and (FSignatureIndex >= 0) and (FSignatureIndex < Items.Count) then
      begin
        Graphic:= TPrintSignature(Items.Objects[FSignatureIndex]);
        Graphic.Signature:= SignaturesCombo.Text;
        Items[FSignatureIndex]:= Graphic.Signature;
        ItemIndex:= FSignatureIndex;
        SigSizeOrPositionUpdated(FontDlgBtn);
      end;
    OkBtn.Default:= TRUE;
  end;
  FSelectAll:= TRUE;
end;

procedure TPrintingDialog.SignaturesEditKeyPress(Sender: TObject;
  var Key: Char);
begin
  if OkBtn.Default then
    OkBtn.Default:= FALSE;
  with SignaturesCombo do
    if (ItemIndex >= 0) and (ItemIndex < Items.Count) then
      FSignatureIndex:= ItemIndex;
  FKeyPressed:= TRUE;
  if Key = Chr(13) then
  begin
    Key:= Chr(0);
    OkBtn.SetFocus;
  end;
end;

procedure TPrintingDialog.SignatureEditChange(Sender: TObject);
begin
  FKeyPressed:= TRUE;
  with SignaturesCombo do
  begin
    if (ItemIndex >= 0) and (ItemIndex < Items.Count) then
      FSignatureIndex:= ItemIndex;
    if (FSignatureIndex >= 0) and (FSignatureIndex < Items.Count) then
      if TPrintSignature(Items.Objects[FSignatureIndex]).Signature <> SignaturesCombo.Text then
        if OkBtn.Default then
          OkBtn.Default:= FALSE;
  end;
end;

procedure TPrintingDialog.InsFieldClick(Sender: TObject);
var
  strFirstPart: string;
  strSecondPart: string;
  strValue: string;
  intCount: Integer;
begin
  if SignaturesCombo.Focused then
  begin
    strValue:= SignaturesCombo.Text;
    strFirstPart:= Copy(strValue, 1, SignaturesCombo.SelStart);
    intCount:= SignaturesCombo.SelStart + SignaturesCombo.SelLength;
    strSecondPart:= Copy(strValue, intCount + 1, Integer(StrLen(PChar(strValue))) - intCount);
  end
  else
  begin
    strFirstPart:= SignaturesCombo.Text;
    strSecondPart:= '';
    SignaturesCombo.SetFocus;
  end;
  intCount:= Length(strFirstPart);
  if Sender = InsPageField then
  begin
    SignaturesCombo.Text:= strFirstPart + FieldsCodes[Ord(fcPage)] + strSecondPart;
    intCount:= intCount + Length(FieldsCodes[Ord(fcPage)]);
  end
  else
    if Sender = InsPagesField then
  begin
    SignaturesCombo.Text:= strFirstPart + FieldsCodes[Ord(fcPageS)] + strSecondPart;
    intCount:= intCount + Length(FieldsCodes[Ord(fcPageS)]);
  end
  else
    if Sender = InsDateField then
  begin
    SignaturesCombo.Text:= strFirstPart + FieldsCodes[Ord(fcDate)] + strSecondPart;
    intCount:= intCount + Length(FieldsCodes[Ord(fcDate)]);
  end
  else
    if Sender = InsTimeField then
  begin
    SignaturesCombo.Text:= strFirstPart + FieldsCodes[Ord(fcTime)] + strSecondPart;
    intCount:= intCount + Length(FieldsCodes[Ord(fcTime)]);
  end
  else
    if Sender = InsFileField then
  begin
    SignaturesCombo.Text:= strFirstPart + FieldsCodes[Ord(fcFile)] + strSecondPart;
    intCount:= intCount + Length(FieldsCodes[Ord(fcFile)]);
  end
  else
    if Sender = InsAuthorField then
  begin
    SignaturesCombo.Text:= strFirstPart + FieldsCodes[Ord(fcAuthor)] + strSecondPart;
    intCount:= intCount + Length(FieldsCodes[Ord(fcAuthor)]);
  end
  else
    if Sender = InsScaleField then
  begin
    SignaturesCombo.Text:= strFirstPart + FieldsCodes[Ord(fcScale)] + strSecondPart;
    intCount:= intCount + Length(FieldsCodes[Ord(fcScale)]);
  end;
  SignaturesCombo.SelStart:= intCount;
  SignaturesCombo.SelLength:= 0;
  SignatureEditChange(Sender);
end;

procedure TPrintingDialog.SigLocationBtnClick(Sender: TObject);
var
  Graphic: TPrintSignature;
begin
  with SignaturesCombo do
    if (Items.Count > 0) and (ItemIndex >= 0) then
      Graphic:= TPrintSignature(Items.Objects[ItemIndex])
    else
      Exit;
  if SigLocOneTimeBtn.Checked then
    Graphic.PrintLocation:= plOneTime
  else
    if SigLocOnPagesBtn.Checked then
    Graphic.PrintLocation:= plOnPages;
  if PreviewBtn.Down then
    Graphic.Invalidate(Scroller);
  UpdateEnabled;
end;

procedure TPrintingDialog.SigLocationCheckClick(Sender: TObject);
var
  Graphic: TPrintSignature;
begin
  with SignaturesCombo do
    if (Items.Count > 0) and (ItemIndex >= 0) then
      Graphic:= TPrintSignature(Items.Objects[ItemIndex])
    else
      Exit;
  Graphic.OnFirstPage:= SigFirstCheck.Checked;
  Graphic.OnInternalPages:= SigInterCheck.Checked;
  Graphic.OnLastPage:= SigLastCheck.Checked;
  if PreviewBtn.Down then
    Graphic.Invalidate(Scroller);
end;

procedure TPrintingDialog.SigFontDlgBtnClick(Sender: TObject);
var
  TextStyleDlg: TFontStyleDialog;
  Graphic: TPrintSignature;
  dblWidth: Double;
  TempRect: TGrRect;
  OldRect: TGrRect;
  boolNeedRedraw: Boolean;
begin
  with SignaturesCombo do
    if (Items.Count > 0) and (ItemIndex >= 0) then
      Graphic:= TPrintSignature(Items.Objects[ItemIndex])
    else
      Exit;
  TextStyleDlg:= TFontStyleDialog.Create(self);
  boolNeedRedraw:= FALSE;
  try
    with TextStyleDlg do
    begin
      TextStyleDlg.SetNewCaption(18);
      // enable height elements
      AllowHeightOptions:= TRUE;
      Fonts:= FProject^.PInfo^.Fonts;
      TextStyle.Assign(Graphic.TextStyle);
      TextStyle.UpdatesEnabled:= TRUE;
      SetColorSupport(Graphic.ForeColor, Graphic.BackColor);
      if ShowModal = mrOk then
      begin
        if Graphic.TextStyle.Alignment <> TextStyle.Alignment then
        begin
          dblWidth:= RectWidth(Graphic.BoundsRect);
          TempRect:= Graphic.BoundsRect;
          OldRect:= Graphic.BoundsRect;
          case Graphic.TextStyle.Alignment of
            Ord(taRight): case TextStyle.Alignment of
                Ord(taLeft): TempRect.Left:= TempRect.Right;
                Ord(taCentered): TempRect.Left:= TempRect.Right - dblWidth * 0.5;
              end;
            Ord(taCentered): case TextStyle.Alignment of
                Ord(taLeft): TempRect.Left:= TempRect.Left + dblWidth * 0.5;
                Ord(taRight): TempRect.Left:= TempRect.Left - dblWidth * 0.5;
              end;
          else //taLeft
            case TextStyle.Alignment of
              Ord(taRight): begin
                  TempRect.Right:= TempRect.Left;
                  TempRect.Left:= TempRect.Left - dblWidth;
                end;
              Ord(taCentered): TempRect.Left:= TempRect.Left - dblWidth * 0.5;
            end;
          end;
          TempRect.Right:= TempRect.Left + dblWidth;
          if CurrentItem = @Graphic.BoundsRect then
          begin
            CurrentItem:= nil;
            Graphic.BoundsRect:= TempRect;
            CurrentItem:= @Graphic.BoundsRect;
          end
          else
            Graphic.BoundsRect:= TempRect;
          Scroller.InvalidateRect(OldRect);
          boolNeedRedraw:= TRUE;
        end;
        Graphic.TextStyle.Assign(TextStyle);
        Graphic.ForeColor:= TextStyleDlg.ForeColor;
        Graphic.BackColor:= TextStyleDlg.BackColor;
        FGraphics.SigTextStyle.Assign(TextStyle);
        FGraphics.SigForeColor:= TextStyleDlg.ForeColor;
        FGraphics.SigBackColor:= TextStyleDlg.BackColor;
        SigSizeOrPositionUpdated(Sender);
        if boolNeedRedraw then
          with FGraphics.PageSetup do
            if (PrintLayoutModel = plmBook) and
              (Graphic.PrintLocation <> plOneTime) and
              (PreviewBtn.Down) then
              PreviewPaint(Self);
      end;
    end;
  finally
    TextStyleDlg.Free;
  end;
end;

procedure TPrintingDialog.StartPageUpdated(Sender: TObject);
var
  Cnt: Integer;
  Graphic: TPrintSignature;
begin
  FPrintOptions.PageSetup.StartPageNum:= StartPageVal.AsInteger;
  FSelectAll:= TRUE;
  OkBtn.Default:= TRUE;
  if PreviewBtn.Down then
    with SignaturesCombo do
      for Cnt:= 0 to Items.Count - 1 do
      begin
        Graphic:= TPrintSignature(Items.Objects[Cnt]);
        Graphic.Invalidate(Scroller);
      end;
end;

procedure TPrintingDialog.PrintToCheckClick(Sender: TObject);
begin
  if (Sender = PrintToFileCheck) and PrintToFileCheck.Checked then
  begin
    if PrintToWMFCheck.Checked then
    begin
      PrintToWMFCheck.Checked:= FALSE;
      {++ Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
      if FPrintOptions.PageSetup.PrintLayoutModel = plmUniformLayout then
        CropMarksCheckBox.Enabled:= TRUE;
      {-- Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
    end;
  end
  else
    {++ Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
    //    if (Sender = PrintToWMFCheck) and PrintToWMFCheck.Checked then
    if Sender = PrintToWMFCheck then
    if PrintToWMFCheck.Checked then
      {-- Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
    begin
      CopiesVal.AsInteger:= 1;
      {++ Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
      if FPrintOptions.PageSetup.PrintLayoutModel = plmUniformLayout then
      begin
        if CropMarksCheckBox.Checked then
          CropMarksCheckBox.Checked:= FALSE;
        CropMarksCheckBox.Enabled:= FALSE;
      end;
      {-- Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
      if PrintToFileCheck.Checked then
        PrintToFileCheck.Checked:= FALSE;
    end
      {++ Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
    else
      if FPrintOptions.PageSetup.PrintLayoutModel = plmUniformLayout then
      CropMarksCheckBox.Enabled:= TRUE;
  {-- Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
  UpdateEnabled;
end;

{++ Moskaliov BUGS##202b,293 BUILD#152 19.01.01}

procedure TPrintingDialog.CropMarksCheckBoxClick(Sender: TObject);
begin
  FPrintOptions.PageSetup.PrintCropMarks:= CropMarksCheckBox.Checked;
  PreviewPaint(Sender);
end;

procedure TPrintingDialog.ProjFrameCheckBoxClick(Sender: TObject);
begin
  FProjGraphic.PrintProjFrame:= ProjFrameCheckBox.Checked;
  if PreviewBtn.Down then
    Scroller.InvalidateRect(FProjGraphic.BoundsRect);
end;
{-- Moskaliov BUGS##202b,293 BUILD#152 19.01.01}

procedure TPrintingDialog.btnLegendObjClick(Sender: TObject);
begin
  LegendCombo.Enabled:= True;
  FrameCombo.Enabled:= False;
  CurrentItem:= nil;
  SetFieldsFromLegendRect;
end;

procedure TPrintingDialog.btnFrameObjClick(Sender: TObject);
begin
  LegendCombo.Enabled:= False;
  FrameCombo.Enabled:= True;
  CurrentItem:= nil;
  SetFieldsFromLegendRect;
end;

procedure TPrintingDialog.chkLegFrmSideClick(Sender: TObject);
var Graphic: TLegendPlaceholder;
begin
  if FUpdating then Exit;
  Graphic:= GetLegendGraphic;
  if not Assigned( Graphic ) then Exit;
  if Sender = chkLegFrmLeft
  then  Graphic.BitFlags:= Graphic.BitFlags xor cwPrnTempFrmBrdLeft
  else if Sender = chkLegFrmTop
  then  Graphic.BitFlags:= Graphic.BitFlags xor cwPrnTempFrmBrdTop
  else if Sender = chkLegFrmRight
  then  Graphic.BitFlags:= Graphic.BitFlags xor cwPrnTempFrmBrdRight
  else if Sender = chkLegFrmBottom
  then  Graphic.BitFlags:= Graphic.BitFlags xor cwPrnTempFrmBrdBottom;
  LegSizeOrPositionUpdated(Sender);
end;

procedure TPrintingDialog.ButtonOleClick(Sender: TObject);
var Graphic: TLegendPlaceholder;
begin
  if FUpdating then Exit;
  Graphic:= GetLegendGraphic;
  if Assigned(Graphic) then begin
     OLEOpenDialog.Filter:=MlgSection[146];
     if OLEOpenDialog.Execute then begin
        if Graphic.LoadOLEFile(PreviewWindow,OLEOpenDialog.FileName,StrPas(FProject^.FName)) then begin
           LabelOLE.Caption:=Graphic.OLEPath;
           Graphic.Invalidate(Scroller);
        end;
     end;
  end;
end;

procedure TPrintingDialog.FrameAddBrnClick(Sender: TObject);
var FrameDialog:TPrnFramedialog;
begin

FrameDialog:=TPrnFramedialog.Create(nil);
FrameDialog.ScaleLabel.Caption:=ScaleCombo.Text;
FrameDialog.FWidth:=FPrintRect.Width/100;
FrameDialog.FHeight:=FPrintRect.Height/100;
if FrameDialog.ShowModal= mrOk then begin
    UpdatePrintView;
//    PreviewWindow.Repaint;
    RedrawSpecPrintFrame;
    FProjGraphic.Invalidate(Scroller);
 end;
 FrameDialog.Free;
end;


procedure TPrintingDialog.RedrawSpecPrintFrame;
 var    UndoUsed,IDBused:boolean;
     TmpLayer:PLayer;
begin
  Screen.Cursor:=crHourGlass;
   IDBUsed:=WingisMainform.GetWeAreUsingTheIDB(FProject);
   UNdoUsed:=WingisMainform.GetWeAreUsingUndoFunction(FProject);
   If IDBUsed
      then
       WingisMainForm.SetWeAreUsingTheIDB(FProject,false);
   If UndoUsed
      then
       WingisMainForm.SetWeAreUsingUndoFunction(FProject,false);
    TmpLayer:=FProject.Layers.NameToLayer(PrintFrameLayerName);
     if Tmplayer<>nil then ProcDeleteLayer(FProject,TmpLayer);

    if FProject.FramePrintSpec.UseSpecFrame=true then begin
      TMDiChild(WingisMainForm.ActualChild).SpecialPrintFrameMakeAndShow(FPrintRect);
    end;
   If IDBUsed
      then
       WingisMainForm.SetWeAreUsingTheIDB(FProject,true);
   If UndoUsed
      then
       WingisMainForm.SetWeAreUsingUndoFunction(FProject,true);
   Screen.Cursor:=crDefault;
end;

procedure TPrintingDialog.chkPrintPageNumClick(Sender: TObject);
begin
     FPrintOptions.PageSetup.PrintPageNum:=chkPrintPageNum.Checked;
end;
{$ENDIF} // <----------------- AXDLL

end.
