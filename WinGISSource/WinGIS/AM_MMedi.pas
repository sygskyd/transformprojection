{****************************************************************************}
{ Unit                                                                       }
{----------------------------------------------------------------------------}
{ Multimedia - Verwaltung und Wiedergabe                                     }
{----------------------------------------------------------------------------}
{ Modifikationen                                                             }
{  11.05.1995 Heinz Ofner                                                    }
{****************************************************************************}
unit AM_MMedi;

interface

uses Controls, Messages, SysUtils, Windows, WinDos, AM_Def, Classes, ResDlg, Objects,
  UserIntf, AM_Ole;

const
  VideoWindowWidth = 500; { Max. Ma�e des Ausgabefensters }
  VideoWindowHeight = 300;

  NoMediaType = 0;
  TypeMidi = 1;
  TypeVideo = 2;
  TypeAnimation = 3;
  TypeWave = 4;

type
  PMMEntry = ^TMMEntry;
  TMMEntry = object(TOldObject)
    ID: LongInt;
    FileName: PChar;
    RelativeFilename: PChar;
    constructor Init(AID: LongInt; AName, ARelativeName: PChar);
    destructor Done; virtual;
    constructor Load(S: TOldStream);
    procedure Store(S: TOldStream);
  end;

  PMultiMedia = ^TMultiMedia;
  TMultiMedia = object(TOldObject)
    MediaList: PCollection;
    constructor Init;
    constructor Load(S: TOldStream);
    procedure Store(S: TOldStream);
    procedure AddDelMedias(AProj: Pointer; AParent: TComponent; ID: LongInt);
    procedure DelMediasAtDel(ID: LongInt);
    function HasEntriesForID(AID: LongInt): Boolean;
    function EntriesForIDCount(AID: LongInt): Integer;
    function OpenEntry(AData: Pointer; AID: Integer; AText: AnsiString): Boolean;
    destructor Done; virtual;
  end;

implementation

uses AM_Ini
{$IFNDEF WMLT}
  , AttachmentDlg
{$ENDIF}
  , AM_Proj, ShellApi, WinOSInfo;

{****************************************************************}

constructor TMultiMedia.Init;
begin
  MediaList := New(PCollection, Init(5, 5));
end;

constructor TMultiMedia.Load
  (
  S: TOldStream
  );
begin
  MediaList := New(PCollection, Load(S));
end;

procedure TMultiMedia.Store
  (
  S: TOldStream
  );
begin
  MediaList^.Store(S);
end;

procedure TMultiMedia.AddDelMedias
  (
  AProj: Pointer;
  AParent: TComponent;
  ID: LongInt
  );
{$IFNDEF WMLT}
var
  MMDialogList: PCollection;
  ChangePointer: Pointer;
  Dialog: TAttachmentDialog;

  procedure CopyIndex
      (
      Item: PMMEntry
      ); far;
  var
    MMFileData: PMMEntry;
  begin
    MMFileData := New(PMMEntry, Init(Item^.ID, StrNew(Item^.FileName),
      StrNew(Item^.RelativeFileName)));
    MMDialogList^.Insert(MMFileData);
  end;
{$ENDIF}
begin
{$IFNDEF WMLT}
  MMDialogList := New(PCollection, Init(5, 5));
  try
    MediaList^.ForEach(@CopyIndex);
    Dialog := TAttachmentDialog.Create(AParent);
    try
      Dialog.MMFiles := MMDialogList;
      Dialog.ObjectID := ID;
      Dialog.RelativeToPath := ExtractFilePath(PProj(AProj)^.FName);
      Dialog.Registry := PProj(AProj)^.Registry;
      if Dialog.ShowModal = mrOK then
      begin
        ChangePointer := MediaList;
        MediaList := MMDialogList;
        MMDialogList := ChangePointer;
          // set project's modified-flag if any change was made
        if Dialog.Modified then
          PProj(AProj)^.SetModified;
      end
      else
        MMDialogList^.DeleteAll;
    finally
      Dialog.Free;
    end;
  finally
    Dispose(MMDialogList, Done);
  end;
{$ENDIF}
end;

procedure TMultiMedia.DelMediasAtDel
  (
  ID: LongInt
  );
var
  i: Integer;
  AFailed: Boolean;
begin
  i := 0;
  AFailed := False;
  while (i < MediaList^.Count) and (not AFailed) do
  begin
    try
      if PMMEntry(MediaList^.At(i))^.ID = ID then
        MediaList^.AtFree(i)
      else
        inc(i);
    except
      AFailed := True;
    end;
  end;
end;

destructor TMultiMedia.Done;
begin
  Dispose(MediaList, Done);
end;

function TMultimedia.HasEntriesForID
  (
  AID: LongInt
  )
  : Boolean;
var
  Cnt: Integer;
begin
  Result := FALSE;

  for Cnt := 0 to MediaList^.Count - 1 do
    if PMMEntry(MediaList^.At(Cnt))^.ID = AID then
    begin
      Result := TRUE;
      Break;
    end;
end;

function TMultimedia.EntriesForIDCount
  (
  AID: LongInt
  )
  : Integer;
var
  Cnt: Integer;
begin
  Result := 0;
  for Cnt := 0 to MediaList^.Count - 1 do
    if PMMEntry(MediaList^.At(Cnt))^.ID = AID then
      Inc(Result);
end;

function TMultiMedia.OpenEntry(AData: Pointer; AID: Integer; AText: AnsiString): Boolean;
var
  i: Integer;
  AEntry: PMMEntry;
  APath, FileName, AbsPath, RelPath, ProjPath, ProjRelPath: AnsiString;
  Params: AnsiString;
  AExt: AnsiString;
begin
  Result := False;
  Params := '';
  APath := '';
  for i := 0 to MediaList^.Count - 1 do
  begin
    AEntry := PMMEntry(MediaList^.At(i));
    if AEntry^.ID = AID then
    begin

      FileName := ExtractFileName(StrPas(AEntry^.FileName));

      AbsPath := ExtractFilePath(StrPas(AEntry^.FileName));
      if Copy(AbsPath, Length(AbsPath), 1) <> '\' then
      begin
        AbsPath := AbsPath + '\';
      end;

      RelPath := ExtractFilePath(StrPas(AEntry^.RelativeFilename));
      if Copy(RelPath, Length(RelPath), 1) <> '\' then
      begin
        RelPath := RelPath + '\';
      end;

      ProjPath := ExtractFilePath(StrPas(PProj(AData)^.FName));
      if Copy(ProjPath, Length(ProjPath), 1) <> '\' then
      begin
        ProjPath := ProjPath + '\';
      end;

      ProjRelPath := ProjPath + RelPath;
      ProjRelPath := StringReplace(ProjRelPath, '\\', '\', [rfReplaceAll]);

      if FileExists(AbsPath + Filename) then
      begin
        APath := AbsPath;
      end
      else
        if FileExists(RelPath + Filename) then
        begin
          APath := RelPath;
        end
        else
          if FileExists(ProjPath + Filename) then
          begin
            APath := ProjPath;
          end
          else
            if FileExists(OSInfo.WingisDir + Filename) then
            begin
              APath := OSInfo.WingisDir;
            end
            else
              if FileExists(ProjRelPath + Filename) then
              begin
                APath := ProjRelPath;
              end;

      if Filename <> '' then
      begin
        if Lowercase(ExtractFileExt(Filename)) = '.exe' then
        begin
          Params := IntToStr(AID);
          if AText <> '' then
          begin
            Params := '"' + AText + '" ' + Params;
          end;
        end;

        AExt := Lowercase(ExtractFileExt(StrPas(AEntry^.FileName)));
        if (AExt = '.amp') or (AExt = '.ssp') or (AExt = '.hsp') then
        begin
          PProj(AData)^.Parent.Close;
        end;

        if ShellExecute(0, 'open', PChar(APath + FileName), PChar(Params), '', SW_SHOWDEFAULT) > 32 then
        begin
          Result := True;
        end;
      end;

    end;
  end;
end;

{****************************************************************}

constructor TMMEntry.Init
  (
  AID: LongInt;
  AName: PChar;
  ARelativeName: PChar
  );
begin
  inherited Init;
  ID := AID;
  FileName := AName;
  RelativeFileName := ARelativeName;
end;

destructor TMMEntry.Done;
begin
  StrDispose(FileName);
  StrDispose(RelativeFileName);
  inherited Done;
end;

constructor TMMEntry.Load
  (
  S: TOldStream
  );
var
  Dir: array[0..fsPathName] of Char;
  DName: array[0..fsFileName + fsExtension] of Char;
  Ext: array[0..fsExtension] of Char;
  Tmp: array[0..255] of Char;
  TmpStr: array[0..255] of Char;
  TmpString: string;
  FID: PChar;
begin
  if ProjectVersion >= 12 then
    S.Read(ID, SizeOf(ID))
  else
  begin
    FID := S.StrRead;
    ID := StrToInt(StrPas(FID));
  end;
  FileName := StrNew(S.StrRead);
  if IniFile^.MMDir <> '' then
  begin
    FileSplit(FileName, Dir, DName, Ext);
    StrPCopy(TmpStr, IniFile^.MMDir);
    StrCat(TmpStr, DName);
    StrCat(TmpStr, Ext);
    StrDispose(FileName);
    FileName := StrNew(TmpStr);
  end;
  if IniFile^.MMDrive <> '' then
  begin
    TmpString := StrPas(FileName);
    Delete(TmpString, 1, 3);
    Insert(IniFile^.MMDrive, TmpString, 1);
    StrPCopy(TmpStr, TmpString);
    StrDispose(FileName);
    FileName := StrNew(TmpStr);
  end;
  if ProjectVersion >= 12 then
    RelativeFileName := S.StrRead
  else
    RelativeFileName := StrNew(StrPCopy(Tmp,
      ExtractRelativePath(S.FileName, FileName)));
//        ExtractRelativePath(TFileRec(S.DataFile).Name,FileName)));
end;

procedure TMMEntry.Store
  (
  S: TOldStream
  );
begin
  S.Write(ID, SizeOf(ID));
  S.StrWrite(FileName);
  S.StrWrite(RelativeFileName);
end;

const
  RMMEntry: TStreamRec = (
    ObjType: rn_MMEntry;
    VmtLink: TypeOf(TMMEntry);
    Load: @TMMEntry.Load;
    Store: @TMMEntry.Store);

  RMMList: TStreamRec = (
    ObjType: rn_MMList;
    VmtLink: TypeOf(TMultiMedia);
    Load: @TMultiMedia.Load;
    Store: @TMultiMedia.Store);

begin
  RegisterType(RMMEntry);
  RegisterType(RMMList);
end.

