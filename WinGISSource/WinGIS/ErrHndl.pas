unit ErrHndl;

interface

uses Classes, SysUtils, WinOSInfo;

procedure DebugMsg(const AFunction, AMessage: AnsiString);

var LastDebugMessage: AnsiString = '';

implementation

procedure DebugMsg(const AFunction, AMessage: AnsiString);
begin
     LastDebugMessage:=FormatDateTime('hh:nn:ss',Now) + ' ' + AFunction + ': ' + AMessage;
end;

end.
