{******************************************************************************+
  Unit TooltipDlg
--------------------------------------------------------------------------------
  Author : Raimund Leitner
--------------------------------------------------------------------------------
  implements the floating text register page in the layermanager dialog.
{******************************************************************************}

unit TooltipDlg;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, WCtrls,
  AM_Proj, MultiLng, StdCtrls, ColumnList, StyleDef, Menus, ExtCtrls, AM_Layer,
  StrTools, Tooltips;

const
  MlgSectionName = 'TooltipDialog';

type
  TFieldIndex = (fiNone, fiShowText, fiLayer, fiSource);

  TTooltipDialog = class(TWForm)
    MlgSection: TMlgSection;
    LayerListBox: TColumnListbox;
    FloatingTextPopup: TPopupMenu;
    CreateFloatingText1: TMenuItem;
    EditFloatingText1: TMenuItem;
    DeleteFloatingText1: TMenuItem;
    LayerSymbols: TImage;
    Label2: TLabel;
    SearchBtn: TSpeedBtn;
    HierarchicBtn: TSpeedBtn;
    AlphabeticBtn: TSpeedBtn;
    SearchEdit: TEdit;
    OnlyGeoTextBtn: TCheckBox;
    Panel1: TPanel;
    Image1: TImage;
    procedure LayerListBoxColumns0Paint(Control: TWinControl;
      Index: Integer; Rect: TRect; State: TOwnerDrawState);
    procedure LayerListBoxColumns1Paint(Control: TWinControl;
      Index: Integer; Rect: TRect; State: TOwnerDrawState);
    procedure LayerListBoxColumns2Paint(Control: TWinControl;
      Index: Integer; Rect: TRect; State: TOwnerDrawState);
    procedure CreateFloatingText1Click(Sender: TObject);
    procedure LayerListBoxClick(Sender: TObject);
    procedure LayerListBoxMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormHide(Sender: TObject);
    procedure DeleteFloatingText1Click(Sender: TObject);
    procedure EditFloatingText1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure SearchEditChange(Sender: TObject);
    procedure LayerListBoxDblClick(Sender: TObject);
    procedure AlphabeticBtnClick(Sender: TObject);
    procedure HierarchicBtnClick(Sender: TObject);
    procedure OnlyGeoTextBtnClick(Sender: TObject);
    procedure LayerListBoxMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
  private
       // list of layers in the project, contains TLayerProperties objects
    FLayerList: TList;
    FProject: PProj;
    FImages: TWImageList;
       // list of geotextinfos, contains TTooltipInfo objects
       // and the layer names
    FGeoTextList: TStringList;
       // layers displayed in LayerListBox
    FLayerListBox: TStringList;
       // display only layers with GeoTexts
    FShowOnlyGeoText: Boolean;
       // current active GeoText-layer
    FGeoTextLayerName: string;
       // GeoText-layer before dialog started
    FOldGeoTextLayerName: string;
       // true only for the first FormActivate call
    FFirstActivate: Boolean;
    FLastIndex: TFieldIndex;
    procedure DrawColumnText(Rect: TRect; State: TOwnerDrawState; Text: string);
    procedure EnablePopupMenu;
    procedure DeleteFloatingText(aLayer: pLayer);
    function GetSelectedLayer: pLayer;
    function GeoTextListIndex(Index: Integer): Integer;
    procedure EstablishHierarchicOrder;
    procedure FillGeoTextList;
    procedure FillLayerListBox;
    function GeoTextInfoToIndex(Index: Integer): TTooltipInfo;
    function FieldAtPos(Listbox: TWCustomListbox; Pos: TPoint): TFieldIndex;
    function FieldHint(Field: TFieldIndex): string;
  public
    constructor Create(aOwner: TComponent); override;
    property LayerList: TList write FLayerList;
    property Project: PProj read FProject write FProject;
    destructor Destroy; override;
  end;

implementation

{$R *.DFM}

uses AM_Index, CreateTooltips, NumTools;

constructor TTooltipDialog.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  FImages := TWImageList.CreateSize(16, 16);
  FImages.AddBitmaps(LayerSymbols.Picture.Bitmap);
  FGeoTextList := TStringList.Create;
  FLayerListBox := TStringList.Create;
  FShowOnlyGeoText := False;
  FGeoTextLayerName := '';
  FOldGeoTextLayerName := '';
  FFirstActivate := True;
  FLastIndex := fiNone;
  {$IFDEF WMLT}
  FloatingTextPopup.AutoPopup:=False;
  {$ENDIF}
end;

destructor TTooltipDialog.Destroy;
begin
  FImages.Free;
  FGeoTextList.Free;
  FLayerListBox.Free;
  inherited Destroy;
end;

procedure TTooltipDialog.LayerListBoxColumns0Paint(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  if (FLayerListBox.Count > Index) and (Index > -1) then
    with LayerListBox, Canvas do
    begin
      Pen.Color := clWindow; // always like background (although selected)
      Brush.Color := clWindow;
      Rectangle(Rect.Left, Rect.Top, Rect.Right, Rect.Bottom);

      if FLayerListBox.Strings[Index] = FGeoTextLayerName then
        FImages.Draw(LayerListBox.Canvas, Rect.Left, Rect.Top, 0);
    end;
  FImages.Draw(Image1.Canvas,Rect.Left+3,0,0);
end;

procedure TTooltipDialog.LayerListBoxColumns1Paint(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
  LayerProp: TLayerProperties;
begin
  if (FLayerListBox.Count > Index) and (Index > -1) then
  begin
    DrawColumnText(Rect, State, FLayerListBox.Strings[Index]);
  end;
  Image1.Canvas.Pen.Color:=clGray;
  Image1.Canvas.MoveTo(Rect.Left,0);
  Image1.Canvas.LineTo(Rect.Left,Image1.Height);
  Image1.Canvas.TextOut(Rect.Left+4,1,FieldHint(fiLayer));
end;

// get the geotext info object (TTooltipInfo) to the index of the layerlistbox

function TTooltipDialog.GeoTextInfoToIndex(Index: Integer): TTooltipInfo;
var
  aName: string;
  i: Integer;
begin
  Result := nil;
  if (Index > -1) and (Index < FLayerListBox.Count) then
  begin
    aName := FLayerListBox.Strings[Index];
    i := FGeoTextList.IndexOf(aName);
    if (i <> -1) then
      Result := TTooltipInfo(FGeoTextList.Objects[i]);
  end;
end;

procedure TTooltipDialog.LayerListBoxColumns2Paint(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
  aLayer: PLayer;
  LayerProp: TLayerProperties;
  aGTInfo: TTooltipInfo;
begin
  if (FLayerListBox.Count > Index) and (Index > -1) then
  begin
    aGTInfo := GeoTextInfoToIndex(Index);
    if (aGTInfo <> nil) then
      case aGTInfo.BDEOrLocal of
        int_Local: DrawColumnText(Rect, [], MlgStringList[MlgSectionName, 5]);
        int_BDE:
          DrawColumnText(Rect, [],
            Format('%s', [ExtractFileName(aGTInfo.BDETable)]));
        int_DDE: DrawColumnText(Rect, [], MlgStringList[MlgSectionName, 9]);
{++ IDB}
        int_IDB: DrawColumnText(Rect, [], MlgStringList[MlgSectionName, 19]);
{-- IDB}
        int_ADO: DrawColumnText(Rect, [], MlgStringList[MlgSectionName, 20]); // 20='MS Access'
      end;
  end;

  Image1.Canvas.Pen.Color:=clGray;
  Image1.Canvas.MoveTo(Rect.Left,0);
  Image1.Canvas.LineTo(Rect.Left,Image1.Height);
  Image1.Canvas.TextOut(Rect.Left+4,1,FieldHint(fiSource));
end;

procedure TTooltipDialog.DrawColumnText
  (
  Rect: TRect;
  State: TOwnerDrawState;
  Text: string
  );
begin
  with LayerListBox, Canvas do
  begin
    Brush.Color := clWindow;
    Font.Color := clWindowText;
    if odSelected in State then
    begin
      Brush.Color := clHighlight;
      Font.Color := clHighlightText
    end
    else
      if odGrayed in State then
      begin
        Brush.Color := clBtnFace;
        Font.Color := clBtnText
      end;
    InflateRect(Rect, -3, 0);
    TextRect(Rect, Rect.Left + 2, CenterHeightInRect(TextHeight('A'), Rect), Text);
    if odFocused in State then DrawFocusRect(Rect);
  end;
end;

procedure TTooltipDialog.CreateFloatingText1Click(Sender: TObject);
var
  aLayer: PLayer;
  LayerName: string;
  aGTInfo: TTooltipInfo;
begin
  with LayerListBox do
    if (FLayerListBox.Count > ItemIndex) and (ItemIndex > -1) then
    begin
      LayerName := FLayerListBox.Strings[ItemIndex];
      aLayer := Project^.Layers^.NameToLayer(LayerName);
      if (aLayer <> nil) then
      begin
        CreateTooltipsForm := TCreateTooltipsForm.Create(self);
        try
          CreateTooltipsForm.Project := Project;
{++ IDB}
          CreateTooltipsForm.Layer := ALayer;
{-- IDB}
          CreateTooltipsForm.Layername := LayerName;
          if CreateTooltipsForm.ShowModal = mrOk then
          begin
            // update Geotext info
            aGTInfo := TTooltipInfo(FGeoTextList.Objects[GeoTextListIndex(ItemIndex)]);
            aGTInfo.CopyFrom(aLayer^.TooltipInfo);
            FGeoTextLayername:=LayerName;
          end;
        finally
          CreateTooltipsForm.Free;
        end;
      end;
    end;
{++ Ivanoff (in Austria). Work with TToolTips}
    SELF.LayerListBox.Invalidate;
{-- Ivanoff}
end;

procedure TTooltipDialog.LayerListBoxClick(Sender: TObject);
begin
  EnablePopupMenu;
end;

// returns the index of the layer in the layerlistbox with the given index
// (indices differ when ShowOnlyGeoTextLayers is active!!!)

function TTooltipDialog.GeoTextListIndex(Index: Integer): Integer;
var
  i: Integer;
begin
  Result := -1;
  with LayerListBox, FLayerListBox do
  begin
    if (Index <> -1) and (Index < FLayerListBox.Count) then
    begin
      Result := FGeoTextList.IndexOf(Strings[Index]);
    end;
  end;
end;

function TTooltipDialog.GetSelectedLayer: pLayer;
var
  LayerProp: TLayerProperties;
begin
  Result := nil;
  if (FLayerList = nil) then Exit;
  with LayerListBox do
    if (ItemIndex <> -1) then
    begin
      LayerProp := FLayerList.Items[LayerListBox.ItemIndex];
      if Assigned(LayerProp) then
        Result := PProj(FProject)^.Layers^.NameToLayer(LayerProp.Name);
    end;
end;

procedure TTooltipDialog.EnablePopupMenu;
var
  aSel,
    FTPresent: Boolean;
begin
  if (FLayerList = nil) then Exit;
  with LayerListBox do
  begin
    aSel := ItemIndex <> -1;
    FTPresent := False;
    try // Prevents bug: Layermanager Geolabel disable to show only the ones with geotext then, after you enable 'show all layers' once more, Wingis crashes.
      if (aSel) then
        FTPresent := GeoTextInfoToIndex(ItemIndex).BDEOrLocal <> int_None;
      CreateFloatingText1.Enabled := aSel and (not FTPresent);
      EditFloatingText1.Enabled := aSel and FTPresent;
      DeleteFloatingText1.Enabled := aSel and FTPresent;
    except
    end;
  end;
end;

procedure TTooltipDialog.LayerListBoxMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  aItem: Integer;
  LayerProp: TLayerProperties;
  aLayer: pLayer;
  aIndex: Integer;
begin
  if not (Button in [mbLeft]) then Exit;
  aItem := LayerListBox.ItemAtPos(Point(X, Y), True);
  if (X > 0) and (X <= 16) then // first column
    with LayerListBox, PProj(FProject)^, Layers^ do
      if (FLayerList.Count > aItem) and (aItem > -1) then
      begin
        aIndex := FLayerListBox.IndexOf(FGeoTextLayerName);
        if aItem = aIndex then
          FGeoTextLayerName := '' // floating text off
        else
        begin // only on if already created floating texts
          if (GeoTextInfoToIndex(aItem).BDEOrLocal <> int_None) then
            FGeoTextLayerName := FGeoTextList.Strings[GeoTextListIndex(aItem)];
        end;
        LayerListBox.Invalidate;
      end;
end;

procedure TTooltipDialog.FormHide(Sender: TObject);
var
  aGTInfo: TTooltipInfo;
  LayerName: string;
  aLayer: pLayer;
  i: Integer;
begin
  if (FProject <> nil) then
    with PProj(FProject)^, Layers^ do
    begin
      if (ModalResult = mrOk) then
      begin
        // do modifications to project
        for i := 0 to FGeoTextList.Count - 1 do
        begin
          aGTInfo := TTooltipInfo(FGeoTextList.Objects[i]);
          LayerName := FGeoTextList.Strings[i];
          aLayer := NameToLayer(LayerName);
          if aLayer <> nil then
          begin
            // if there are changes...
            if aGTInfo.IsDifferent(aLayer^.TooltipInfo) then
            begin
              // because create and edit are handled by a separate dlg
              // there can be only a GeoText delete...
              if aGTInfo.BDEOrLocal = int_None then DeleteFloatingText(aLayer);
            end;
          end;
        end;
      end;
      // restore situation before if user cancelled dialog...
      if (ModalResult <> mrOk) then FGeoTextLayerName := FOldGeoTextLayerName;
      // setup TooltipLayer and end/start tooltipthread (GeoText)
      if FGeoTextLayerName = '' then
      begin
        TooltipThread.EndFloatingText;
        TooltipLayer := nil;
      end
      else
      begin
        TooltipLayer := NameToLayer(FGeoTextLayerName);
        TooltipThread.StartFloatingText(FProject);
      end;
    end;
// ?!?!?
//  if (ModalResult<>mrOk) and FWasRunning then TooltipThread.Resume;
end;

procedure TTooltipDialog.DeleteFloatingText1Click(Sender: TObject);
var
  aGTInfo: TTooltipInfo;
  MsgNum: Integer;
begin
  with LayerListBox, PProj(FProject)^, Layers^ do
    if (FLayerListBox.Count > ItemIndex) and (ItemIndex > -1) then
    begin
      aGTInfo := GeoTextInfoToIndex(ItemIndex);
      case aGTInfo.BDEOrLocal of
        int_Local:
          MsgNum := 6;
{        begin
          if MessageDialog(MlgStringList[MlgSectionName,6],
             mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
//                                       DeleteFloatingText(aLayer);
//                                       SetModified;
            aGTInfo.SetNone;
            aGTInfo.AutoText:=0;
            aGTInfo.SetFillNone;
            if (FGeoTextLayerName=FGeoTextList.Strings[ItemIndex]) then FGeoTextLayerName:='';
            LayerListBox.Invalidate;
            end;
          end;}
        int_BDE, int_IDB:
          MsgNum := 7;

{        begin
          if MessageDialog(MlgStringList[MlgSectionName,7],
             mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
            aGTInfo.SetNone;
            aGTInfo.AutoText:=0;
            aGTInfo.SetFillNone;
            if (FGeoTextLayerName=FGeoTextList.Strings[ItemIndex]) then FGeoTextLayerName:='';
            LayerListBox.Invalidate;
            end;
          end;  }
        int_DDE:
          MsgNum := 8;

{        begin
          if MessageDialog(MlgStringList[MlgSectionName,8],
             mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
            aGTInfo.SetNone;
            aGTInfo.AutoText:=0;
            aGTInfo.SetFillNone;
            if (FGeoTextLayerName=FGeoTextList.Strings[ItemIndex]) then FGeoTextLayerName:='';
            LayerListBox.Invalidate;
            end;
          end;}
      end;
      if MessageDialog(MlgStringList[MlgSectionName, MsgNum],
        mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        aGTInfo.SetNone;
        aGTInfo.AutoText := 0;
        aGTInfo.SetFillNone;
        if (FGeoTextLayerName = FGeoTextList.Strings[ItemIndex]) then FGeoTextLayerName := '';
        LayerListBox.Invalidate;
      end;

      EnablePopupMenu;
    end;
end;

procedure TTooltipDialog.DeleteFloatingText
  (
  aLayer: pLayer
  );
var
  i: LongInt;
  aItem: PIndex;
begin
  if aLayer <> nil then
  begin
    with aLayer^ do
      case TooltipInfo.BDEOrLocal of
        int_Local:
          begin
            for i := 0 to Data^.GetCount - 1 do
            begin
              aItem := Data^.At(i);
              if aItem <> nil then
              begin
                DisposeStr(aItem^.Tooltip);
                aItem^.Tooltip := nil;
              end;
            end;
            TooltipInfo.SetNone;
            TooltipInfo.AutoText := 0;
            TooltipInfo.SetFillNone;
          end;
        int_BDE,
        int_IDB,
        int_ADO:
          begin
            TooltipInfo.SetNone;
            TooltipInfo.AutoText := 0;
            TooltipInfo.SetFillNone;
          end;
        int_DDE:
          begin
            TooltipInfo.SetNone;
            TooltipInfo.AutoText := 0;
            TooltipInfo.SetFillNone;
          end;
      end;
    EnablePopupMenu;
  end;
end;

procedure TTooltipDialog.EditFloatingText1Click(Sender: TObject);
var
  aLayer: PLayer;
  LayerName: string;
  aGTInfo: TTooltipInfo;
begin
  with LayerListBox do
    if (FLayerListBox.Count > ItemIndex) and (ItemIndex > -1) then
    begin
      LayerName := FLayerListBox.Strings[ItemIndex];
      aLayer := Project^.Layers^.NameToLayer(LayerName);
      aGTInfo := GeoTextInfoToIndex(ItemIndex);
      if (aGTInfo = nil) then Exit;
      if (aGTInfo.BDEOrLocal = int_None) then Exit;
      if (aLayer <> nil) then
      begin
        CreateTooltipsForm := TCreateTooltipsForm.Create(self);
        try
          CreateTooltipsForm.Project := Project;
{++ IDB}
          CreateTooltipsForm.Layer := ALayer;
{-- IDB}
          CreateTooltipsForm.Layername := LayerName;
          CreateTooltipsForm.ModifyDlg := True;
          if CreateTooltipsForm.ShowModal = mrOk then
          begin
            // update Geotext info
            aGTInfo := TTooltipInfo(FGeoTextList.Objects[GeoTextListIndex(ItemIndex)]);
            aGTInfo.CopyFrom(aLayer^.TooltipInfo);
          end;
        finally
          CreateTooltipsForm.Free;
        end;
      end;
    end;
{++ Ivanoff (in Austria). Work with TToolTips}
  SELF.LayerListBox.Invalidate;
{-- Ivanoff}
end;

procedure TTooltipDialog.FormActivate(Sender: TObject);
var
  i: Integer;
  layerProp: TLayerProperties;
  aGTInfo: TTooltipInfo;
  aLayer: pLayer;
  aStr: AnsiString;
begin
  TooltipThread.SureSuspend;
  if FLayerList <> nil then
  begin
    // copy current GeoText info only first time (only when dlg is opened)
//    if (FFirstActivate) then
//    begin
    if LayerListBox.ItemIndex > 0 then
      aStr := LayerListBox.Items[LayerListBox.ItemIndex]
    else
      aStr := EmptyStr;

    FillGeoTextList;
    LayerListBox.ItemIndex := 0;
    with PProj(FProject)^ do
      if TooltipLayer <> nil then
      begin
        FGeoTextLayerName := TooltipLayer^.Text^;
        FOldGeoTextLayerName := TooltipLayer^.Text^;
      end;
//    end;
    FFirstActivate := False;
    FillLayerListBox;
    LayerListBox.ItemIndex := LayerListBox.Items.IndexOf(aStr);
  end;
  // update menus
  EnablePopupMenu;
end;

procedure TTooltipDialog.FormShow(Sender: TObject);
begin
  Image1.Canvas.Brush.Color:=clBtnFace;
  Image1.Canvas.FillRect(Classes.Rect(0,0,Image1.Width,Image1.Height));
  FormActivate(Sender);
end;

procedure TTooltipDialog.SearchBtnClick(Sender: TObject);
var
  Cnt: Integer;
  LayerName: string;
begin
  if FLayerListBox.Count > 0 then begin
  Cnt := (LayerListbox.ItemIndex + 1) mod FLayerListBox.Count;
  while Cnt <> LayerListbox.ItemIndex do
  begin
    LayerName := FLayerListBox.Strings[Cnt];
    if CalculateEqualLen(SearchEdit.Text, LayerName) = Length(SearchEdit.Text) then
    begin
      LayerListbox.ItemIndex := Cnt;
      Break;
    end;
    Cnt := (Cnt + 1) mod FLayerListBox.Count;
  end;
  end;
end;

procedure TTooltipDialog.SearchEditChange(Sender: TObject);
var
  MaxEqualLen: Integer;
  MaxEqualIndex: Integer;
  MaxEqualAllLen: Integer;
  EqualLen: Integer;
  Cnt: Integer;
  LayerName: string;
begin
  MaxEqualLen := 0;
  MaxEqualAllLen := 0;
  MaxEqualIndex := -1;
  for Cnt := 0 to FLayerListBox.Count - 1 do
  begin
    LayerName := FLayerListBox.Strings[Cnt];
    EqualLen := CalculateEqualLen(SearchEdit.Text, LayerName);
    if (EqualLen > MaxEqualLen) or ((EqualLen = MaxEqualLen)
      and (MaxEqualAllLen > ByteToCharIndex(LayerName, Length(LayerName)))) then
    begin
      MaxEqualLen := EqualLen;
      MaxEqualAllLen := ByteToCharIndex(LayerName, Length(LayerName));
      MaxEqualIndex := Cnt;
    end;
  end;
  if MaxEqualIndex >= 0 then LayerListbox.ItemIndex := MaxEqualIndex;
  SearchEdit.Text := Copy(SearchEdit.Text, 1, CharToByteIndex(SearchEdit.Text, MaxEqualLen));
  SearchEdit.SelStart := Length(SearchEdit.Text);
  SearchBtn.Enabled := Length(SearchEdit.Text) > 0;
end;

procedure TTooltipDialog.LayerListBoxDblClick(Sender: TObject);
var
  aGTInfo: TTooltipInfo;
begin
  {$IFNDEF WMLT}
  with LayerListBox do
    if (FLayerList.Count > ItemIndex) and (ItemIndex > -1) then
    begin
      aGTInfo := GeoTextInfoToIndex(ItemIndex);
      if (aGTInfo = nil) then exit;
      if aGTInfo.BDEOrLocal = int_None then begin
         CreateFloatingText1Click(Self);
         exit;
      end;
      if (aGTInfo.BDEOrLocal = int_None) then Exit;
      CreateTooltipsForm := TCreateTooltipsForm.Create(self);
      try
        CreateTooltipsForm.Project := Project;
        CreateTooltipsForm.Layername := FLayerListBox.Strings[ItemIndex];
{++ IDB}
        CreateTooltipsForm.Layer := Project^.Layers^.NameToLayer(FLayerListBox.Strings[ItemIndex]);
{-- IDB}
        CreateTooltipsForm.ModifyDlg := True;
        if CreateTooltipsForm.ShowModal = mrOk then
        begin
        end;
      finally
        CreateTooltipsForm.Free;
      end;
    end;
    {$ENDIF}
end;

procedure TTooltipDialog.AlphabeticBtnClick(Sender: TObject);
var
  aStr: string;
begin
  if FGeoTextList = nil then Exit;
  aStr := '';
  with LayerListBox do
  begin
    if ItemIndex <> -1 then aStr := FLayerListBox.Strings[ItemIndex];
    FLayerListBox.Sorted := AlphabeticBtn.Down;
    if aStr <> '' then ItemIndex := FLayerListBox.IndexOf(aStr);
    LayerListBox.Invalidate;
  end;
end;

procedure TTooltipDialog.HierarchicBtnClick(Sender: TObject);
var
  aStr: string;
begin
  if FGeoTextList = nil then Exit;
  if HierarchicBtn.Down then
  begin
    if LayerListBox.ItemIndex <> -1 then aStr := FLayerListBox.Strings[LayerListBox.ItemIndex];
    FLayerListBox.Sorted := False;
    EstablishHierarchicOrder;
    if aStr <> '' then LayerListBox.ItemIndex := FLayerListBox.IndexOf(aStr);
    LayerListBox.Invalidate;
  end;
end;

procedure TTooltipDialog.EstablishHierarchicOrder;
var
  i, j: Integer;
  Layer: TLayerProperties;
  Pos: Integer;
  aStr1,
    aStr2: string;
begin
  Pos := 0;
  for i := 0 to FLayerListBox.Count - 1 do
  begin
    Layer := FLayerList[i];
    aStr2 := '';
    for j := Pos to FLayerListBox.Count - 1 do
    begin
      if FLayerListBox.Strings[j] = Layer.Name then
      begin
        aStr2 := Layer.Name;
        break;
      end;
    end;
    // "quasi" insertion sort
    if aStr2 <> '' then
    begin
      // swap entries (Entry2 is thr right for FLegend[Pos])
      aStr1 := FLayerListBox.Strings[Pos];
      FLayerListBox.Strings[Pos] := aStr2;
      FLayerListBox.Strings[j] := aStr1;
      // search next pos
      Inc(Pos);
    end;
    if Pos >= FLayerListBox.Count then break; // finished...
  end;
end;

procedure TTooltipDialog.FillGeoTextList;
var
  i: Integer;
  aGTInfo: TTooltipInfo;
  LayerProp: TLayerProperties;
  aLayer: pLayer;
begin
  FGeoTextList.Clear;
  // fill FGeoTextList
  for i := 0 to FLayerList.Count - 1 do
  begin
    LayerProp := TLayerProperties(FLayerList.Items[i]);
    aLayer := Project^.Layers^.NameToLayer(LayerProp.Name);
    if (aLayer <> nil) then
    begin
      aGTInfo := TTooltipInfo.Create;
      aGTInfo.CopyFrom(aLayer^.TooltipInfo);
      FGeoTextList.AddObject(LayerProp.Name, aGTInfo);
    end;
  end;
end;

procedure TTooltipDialog.FillLayerListBox;
var
  i: Integer;
  aGTInfo: TTooltipInfo;
  LayerProp: TLayerProperties;
  aLayer: pLayer;
  Add2List: Boolean;
begin
  FLayerListBox.Clear;
  // fill FLayerListBox (Strings for LayerListBox)
  for i := 0 to FGeoTextList.Count - 1 do
  begin
    if FShowOnlyGeoText then
    begin
      aGTInfo := TToolTipInfo(FGeoTextList.Objects[i]);
      if aGTInfo <> nil then
        Add2List := aGTInfo.BDEOrLocal <> int_None
      else
        Add2List := False;
    end
    else
      Add2List := True;
    if Add2List then
      FLayerListBox.Add(FGeoTextList.Strings[i]);

    LayerProp := TLayerProperties(FLayerList.Items[i]);
  end;
  LayerListBox.Count := FLayerListBox.Count;
end;

procedure TTooltipDialog.OnlyGeoTextBtnClick(Sender: TObject);
var
  aStr: string;
begin
  if FLayerList <> nil then begin
     aStr := '';
     if LayerListBox.ItemIndex <> -1 then aStr := FLayerListBox.Strings[LayerListBox.ItemIndex];
     if FLayerList.Count > 0 then
     begin
          FShowOnlyGeoText := OnlyGeoTextBtn.Checked;
          FillLayerListBox;
          if aStr <> '' then LayerListBox.ItemIndex := FLayerListBox.IndexOf(aStr);
          LayerListBox.Invalidate;
     end;
  end;
end;

procedure TTooltipDialog.LayerListBoxMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var
  Index: TFieldIndex;
begin
  with LayerListBox do
  begin
    Index := FieldAtPos(LayerListBox, Point(X, Y));
    if Index <> FLastIndex then
    begin
      Application.CancelHint;
      FLastIndex := Index;
      LayerListBox.Hint := FieldHint(Index);
    end
    else
      LayerListBox.Hint := FieldHint(Index);
    DoInherited := FALSE;
  end;
end;

function TTooltipDialog.FieldAtPos
  (
  Listbox: TWCustomListbox;
  Pos: TPoint
  )
  : TFieldIndex;
begin
  if (Pos.X >= 0) and (Pos.X <= 18) then
    Result := fiShowText
  else
    if (Pos.X > 18) and (Pos.X <= 365) then
      Result := fiLayer
    else
      if (Pos.X > 365) and (Pos.X <= 469) then
        Result := fiSource
      else
        Result := fiNone;
end;

function TTooltipDialog.FieldHint(Field: TFieldIndex): string;
begin
  case Field of
    fiShowText: Result := MlgStringList[MlgSectionName, 16];
    fiLayer: Result := MlgStringList[MlgSectionName, 17];
    fiSource: Result := MlgStringList[MlgSectionName, 18];
  else
    Result := '';
  end;
end;

end.

