unit DrawOrthoHndl;
{
Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 07.04.2000

Module for registrate contex toolbar on that a button is placed for change drawing
mode for new lines and polygons.
}
interface

implementation
{$R *.mfn}
Uses MenuFn;

Initialization;
MenuFunctions.RegisterFromResource(HInstance,'DrawOrthoHndl','DrawOrthoHndl');

end.
