{******************************************************************************+
  Unit AM_DDE
--------------------------------------------------------------------------------
  Interface to the GisComm.dll.
+******************************************************************************}
unit AM_DDE;

interface
{$IFNDEF AXDLL} // <----------------- AXDLL
uses Classes, Controls, Dialogs, ExtCtrls, Forms, Messages, WCtrls, Windows, ComCtrls, Objects, AM_Coll, DDEML, DDEDef, StrUtils;

type
  TDDEHandler = class(TComponent)
  private
    AMainWindow: Pointer; {Adresse des WinGIS-Hauptfensters}
    ActualChild: Pointer; {Adresse des aktiven Projektfensters}
    ModuleOK: Boolean; {TRUE, wenn KommunikationsDLL gestartet werden konnte}
    FSendSep: Boolean;
  protected
    function ReadDataMode: Boolean;
    function ReadDBSelMode: Boolean;
    function ReadDoAutoIns: Boolean;
    function ReadDoOneSelect: Boolean;
    function ReadFChange: Word;
    function ReadFClose: Word;
    function ReadFDDESepSign: Char;
    function ReadFLayerInfo: Word;
    function ReadFGotLInfo: Word;
    function ReadFMoreSelect: Boolean;
    function ReadFNew: Word;
    function ReadFNoDBAutoIn: Boolean;
    function ReadFOneSelect: Boolean;
    function ReadFOpen: Word;
    function ReadFOutSepSign: Char;
    function ReadFrameSelect: Boolean;
    function ReadFSave: Word;
    function ReadFSaveAs: Word;
    function ReadFSendGR: Boolean;
    function ReadFSendGRAV: Boolean;
    function ReadFSending: Boolean;
    function ReadFSendLMSets: Boolean;
    function ReadFSendMWR: Boolean;
    function ReadFShowADSA: Boolean;
    function ReadFSuspendDDE: Boolean;
    function ReadInAction: Boolean;
    function ReadMacroConv: HConv;
    function ReadMacroHSZCI: HSZ;
    function ReadMacroHSZCT: HSZ;
    function ReadMacroMode: Boolean;
    function ReadNWUMode: Boolean;
    function ReadRequestFrom: Word;
    procedure WriteDataMode(ADataMode: Boolean);
    procedure WriteDBSelMode(ADBSMode: Boolean);
    procedure WriteDoAutoIns(ADoAIns: Boolean);
    procedure WriteDoOneSelect(ADoOneSelect: Boolean);
    procedure WriteFGotLInfo(AFGLInfo: Word);
    procedure WriteFLayerInfo(AFLInfo: Word);
    procedure WriteFOneSelect(AFOneSelect: Boolean);
    procedure WriteFrameSelect(ADoOneSelect: Boolean);
    procedure WriteFSending(AFSending: Boolean);
    procedure WriteFSuspendDDE(ASuspend: Boolean);
    procedure WriteMacroConv(AHConv: HConv);
    procedure WriteMacroHSZCI(AHSZCI: HSZ);
    procedure WriteMacroHSZCT(AHSZCT: HSZ);
    procedure WriteMacroMode(AMode: Boolean);
    procedure WriteNWUMode(ANWUMode: Boolean);
    procedure WriteRequestFrom(ARFrom: Word);
  public
    DDEApps: PCollection; {Liste aller Anwendungen, die im DB-Abschnitt der ini aufgef�hrt sind}
    DDEMLApps: PCollection; {Liste aller Anwendungen, die im DDEML-Abschnitt der ini aufgef�hrt sind}
    DDEData: PBigUStrColl; {Liste f�r DDE-Strings, die mit [END][] abgeschlossen werden}
    AllDDEData: PBigUStrColl; {Liste f�r DDE-Strings, die innerhalb eines Makro-Blocks gesendet werden}
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function Append(AStr: string): Boolean;
    function AppendList(Index: LongInt): Boolean;
    function AppendList1(Index: LongInt; NameLength: Integer): Boolean;
    function AppendString(AStr: string): Boolean;
    procedure CloseProject(AChildWindow: Pointer);
    procedure CombineParamsOfAllActiveConnections;
    procedure DDEAcknowledge(var Msg: TMessage);
    function DDEOk: Boolean;
    procedure DDETerminate(ServerHandle: THandle; MustBeSender: Boolean; TermDDEML: Boolean);
    procedure DeleteAllDDEData;
    procedure DeleteDDEData;
    procedure DeleteDDEDataComplete;
    procedure DoAllDDECommands(ActChild: Pointer);
    procedure DoAllDDEMLCommands(ActChild: Pointer);
    procedure DoDDEInitiate(var Msg: TMessage);
    procedure DoDDETerminate(ServerHandle: THandle; MustBeSender: Boolean; TermDDEML: Boolean);
    procedure DoShowMessage;
    function EnableDDEMLConversation(AEnable: Boolean): Boolean;
    procedure EnableDDEReception(AEnable: Boolean);
    function EndList(SendEnd: Boolean): Boolean;
    function EndList1(SendEnd: Boolean; NameLength: Integer): Boolean;
    function Execute: Boolean;
    procedure FreeGlobal;
    function GetDBLInfo: Word;
    function GetDBLoaded: Integer;
    function GetDBStrFormat: Word;
    function GetDDEMLLoaded: Integer;
    function GetGlobal(Size: LongInt): Boolean;
    procedure GetLayerName(ALayerIndex: LongInt);
    procedure GetNewDBEntry(ACommName: PChar; var ACon: Boolean; var AConProj: Boolean;
      var ASendStr: Boolean; var ASendProj: Boolean);
    procedure GetNewDDECommand;
    procedure GetTopLayerName;
    function InitiateDDE(AWindow: THandle; LoadDatabase: Boolean; InitDDEML: Boolean): Boolean;
    procedure InsertAllDDEData(DDELine: PChar);
    procedure InsertDDEData(DDELine: PChar);
    function QuestHServer: Word;
    function QuestLCnt: Integer;
    procedure ReadDDEWMIniSettings;
    procedure ReadDDEMLServerIniSettings;
    procedure ReadDDEMLIniSettings;
    procedure Reset;
    procedure SendAck(AWindow: HWnd; AParam: LongInt);
    function SendPChar(AText: PChar): Boolean;
    procedure SendRefPoints(GeoReference: PLPointArray);
    procedure SendSHWADSA;
    function SendString(AText: string): Boolean;
    procedure SetActualChild(AChild: Pointer);
    procedure SetAnswerMode(SetMode: Boolean);
    procedure SetDBAutoInsert(ADBAutoInsert: Boolean);
    procedure SetDBSendStrings(SendMode: Word; ProjToInit: Pointer);
    procedure SetDBWMProjInited(SetInited: Boolean);
    procedure SetDDECommMode(AConversation: Word; AConvName: PChar);
    procedure SetDDEIniSettings;
    procedure SetOneSelect(AOneSelect: Boolean);
    procedure SetStatusText(AText: LongInt);
    procedure SetTRData;
    procedure StartList(AText: string);
    function TranslateInStringDesma(ADDEString: PChar; var EndMode: Boolean): PChar;
    procedure UnActivateConnections;
    procedure UpdateAppLists;
    function Write(Pos: LongInt; AStr: string): Boolean;
    procedure WriteDDEDDEMLSettings;
    procedure WriteLog;
    procedure SendSep;
  published
    property DataMode: Boolean read ReadDataMode write WriteDataMode default FALSE;
    property DoAutoIns: Boolean read ReadDoAutoIns write WriteDoAutoIns default FALSE;
    property DoOneSelect: Boolean read ReadDoOneSelect write WriteDoOneSelect default FALSE;
    property FChange: Word read ReadFChange default 0;
    property FClose: Word read ReadFClose default 0;
    property FDDESepSign: Char read ReadFDDESepSign default ',';
    property FGotLInfo: Word read ReadFGotLInfo write WriteFGotLInfo default 0;
    property FLayerInfo: Word read ReadFLayerInfo write WriteFLayerInfo default 0;
    property FMoreSelect: Boolean read ReadFMoreSelect default FALSE;
    property FNew: Word read ReadFNew default 0;
    property FNoDBAutoIn: Boolean read ReadFNoDBAutoIn default FALSE;
    property FOneSelect: Boolean read ReadFOneSelect write WriteFOneSelect default FALSE;
    property FOpen: Word read ReadFOpen default 0;
    property FOutSepSign: Char read ReadFOutSepSign default ',';
    property FrameSelect: Boolean read ReadFrameSelect write WriteFrameSelect default TRUE;
    property FSave: Word read ReadFSave default 0;
    property FSaveAs: Word read ReadFSaveAs default 0;
    property FSendGR: Boolean read ReadFSendGR default FALSE;
    property FSendGRAV: Boolean read ReadFSendGRAV default FALSE;
    property FSending: Boolean read ReadFSending write WriteFSending default FALSE;
    property FSendLMSets: Boolean read ReadFSendLMSets default FALSE;
    property FSendMWR: Boolean read ReadFSendMWR default FALSE;
    property FShowADSA: Boolean read ReadFShowADSA default FALSE;
    property FSuspendDDE: Boolean read ReadFSuspendDDE write WriteFSuspendDDE default FALSE;
    property InAction: Boolean read ReadInAction default FALSE;
    property MacroCnv: HConv read ReadMacroConv write WriteMacroConv default 0;
    property MacroHSZCI: HSZ read ReadMacroHSZCI write WriteMacroHSZCI default 0;
    property MacroHSZCT: HSZ read ReadMacroHSZCT write WriteMacroHSZCT default 0;
    property MacroMode: Boolean read ReadMacroMode write WriteMacroMode default FALSE;
    property NWUMode: Boolean read ReadNWUMode write WriteNWUMode default FALSE;
    property RequestFrom: Word read ReadRequestFrom write WriteRequestFrom default 0;
    property DBSelMode: Boolean read ReadDBSelMode write WriteDBSelMode default FALSE;
  end;

var
  DDEHandler: TDDEHandler;
  sLastSelInfo: string; // WAI: used in Selinfo.pas function TSelectInfo.GetStatusText
  bDDEToFile: Boolean = False;
  DDEOutputList: TStringList;

function CountDDEItems(s: AnsiString): Integer;
function GetDDEItem(s: AnsiString; n: Integer): ansistring;
function ProcSaveDDELine(s: PChar): Boolean;
procedure ProcSetDDEToFileMode(bOn: Boolean);
{$ENDIF} // <----------------- AXDLL

type
  TLogFile = class
  public
    Logging: Boolean;
    procedure WriteLog(s: AnsiString);
  end;

var
  LogFile: TLogFile;

implementation

{$IFNDEF AXDLL} // <----------------- AXDLL
uses Graphics, IniFiles, NumTools, SysUtils, ToolSel, TypInfo, WinProcs, WinDOS, AM_Main, AM_Child, AM_Ini,
  AM_Def, AM_TRDDE, AM_Layer, MenuFn, UserIntf, WinOSInfo;

const
  GCDLLName = 'GISCOMM.DLL';

{$IFNDEF WMLT}
function CreateCommunicationModule(AMainWindowHandle: HWnd; ALogging: Boolean): Boolean; external GCDLLName Index 1;
procedure DestroyCommunicationModule; external GCDLLName Index 2;
procedure DDEHandler_SetIniSettings(AIniSepSign: Char; AMWReady, AGReady, ASendLMSettings,
  AGReadyAfterView, AShowAfterDSA, AOneSelect, ADBAutoInsert,
  ADDEMLConnect, ADDEMLSend, ADBSelMode, ADoOneSelect,
  AFrameSelect: Boolean; ADBLayerInfos, ADDEMLCallBack: Word;
  ASendInfosToDB: LongInt); external GCDLLName Index 3;
procedure DDEHandler_DDEInitiate(var Msg: TMessage); external GCDLLName Index 4;
function DDEHandler_InitiateDDE(AWindow: THandle; LoadDatabase: Boolean;
  InitDDEML: Boolean): Boolean; external GCDLLName Index 5;
procedure DDEHandler_InsertDDEApp(ADDEApp: PDDEApp); external GCDLLName Index 6;
procedure DDEHandler_InsertDDEMLServerSettings(AService, ATopic, AItem: PChar); external GCDLLName Index 7;
procedure DDEHandler_InsertDDEMLApp(ADDEMLApp: PDDEMLApp); external GCDLLName Index 8;
procedure DDEHandler_EnableDDEReception(AEnable: Boolean); external GCDLLName Index 9;
procedure DDEHandler_DDETerminate(ServerHandle: THandle; MustBeSender, TermDDEML: Boolean); external GCDLLName Index 10;
procedure DDEHandler_CombineParamsOfAllActiveConnections; external GCDLLName Index 11;
procedure DDEHandler_SetDBSendStrings(SendMode: Word; ProjToInit: HWnd); external GCDLLName Index 12;
function DDEHandler_GetFSendMWR: Boolean; external GCDLLName Index 13;
function DDEHandler_SendString(AText: string): Boolean; external GCDLLName Index 14;
function DDEHandler_SendPChar(AText: PChar): Boolean; external GCDLLName Index 15;
procedure DDEHandler_DDEAcknowledge(var Msg: TMessage); external GCDLLName Index 16;
procedure DDEHandler_SendAck(AWindow: HWnd; AParam: LongInt); external GCDLLName Index 17;
procedure DDEHandler_SetAnswerMode(SetMode: Boolean); external GCDLLName Index 18;
procedure DDEHandler_SetActualChild(AChild: HWnd); external GCDLLName Index 19;
procedure DDEHandler_UnActivateConnections; external GCDLLName Index 20;
procedure DDEHandler_GetDDECommand(var AHSZClientTopic: HSZ; var AHSZClientItem: HSZ;
  var AStr: PChar; var ACnv: PChar); external GCDLLName Index 21;
function DDEHandler_GetDBLoaded: Integer; external GCDLLName Index 22;
function DDEHandler_GetDDEMLLoaded: Integer; external GCDLLName Index 23;
function DDEHandler_GetDDEOK: Boolean; external GCDLLName Index 24;
function DDEHandler_GetFLayerInfo: Integer; external GCDLLName Index 25;
procedure DDEHandler_StartList(AText: ShortString); external GCDLLName Index 26;
function DDEHandler_AppendList(Index: LongInt): Boolean; external GCDLLName Index 27;
function DDEHandler_AppendList1(Index: LongInt; NameLength: Integer): Boolean; external GCDLLName Index 28;
function DDEHandler_AppendString(AStr: ShortString): Boolean; external GCDLLName Index 29;
function DDEHandler_EndList(SendEnd: Boolean): Boolean; external GCDLLName Index 30;
function DDEHandler_EndList1(SendEnd: Boolean; NameLength: Integer): Boolean; external GCDLLName Index 31;
function DDEHandler_GetFGotLInfo: Integer; external GCDLLName Index 32;
function DDEHandler_QuestHServer: Word; external GCDLLName Index 33;
function DDEHandler_GetGlobal(Size: LongInt): Boolean; external GCDLLName Index 34;
procedure DDEHandler_FreeGlobal; external GCDLLName Index 35;
function DDEHandler_Write(Pos: LongInt; AStr: ShortString): Boolean; external GCDLLName Index 36;
function DDEHandler_Append(AStr: PChar): Boolean; external GCDLLName Index 37;
procedure DDEHandler_Reset; external GCDLLName Index 38;
function DDEHandler_QuestLCnt: Integer; external GCDLLName Index 39;
function DDEHandler_GetDBLInfo: Word; external GCDLLName Index 40;
function DDEHandler_GetDBStrFormat: Word; external GCDLLName Index 41;
procedure DDEHandler_SetDBWMProjInited(SetInited: Boolean); external GCDLLName Index 42;
function DDEHandler_EnableDDEMLConversation(AEnable: Boolean): Boolean; external GCDLLName Index 43;
function DDEHandler_Execute: Boolean; external GCDLLName Index 44;
function DDEHandler_GetFDDESepSign: Char; external GCDLLName Index 45;
function DDEHandler_GetFOutSepSign: Char; external GCDLLName Index 46;
procedure DDEHandler_SetFGotLInfo(AFGLInfo: Word); external GCDLLName Index 47;
function DDEHandler_GetFSendGR: Boolean; external GCDLLName Index 48;
function DDEHandler_GetFSendGRAV: Boolean; external GCDLLName Index 49;
procedure DDEHandler_SetDBSelMode(ADBSMode: Boolean); external GCDLLName Index 50;
function DDEHandler_GetDBSelMode: Boolean; external GCDLLName Index 51;
function DDEHandler_GetFOneSelect: Boolean; external GCDLLName Index 52;
procedure DDEHandler_SetFOneSelect(AOneSelect: Boolean); external GCDLLName Index 53;
function DDEHandler_GetIniDoOneSelect: Boolean; external GCDLLName Index 54;
procedure DDEHandler_SetIniDoOneSelect(ADoOneSelect: Boolean); external GCDLLName Index 55;
function DDEHandler_GetIniFrameSelect: Boolean; external GCDLLName Index 56;
procedure DDEHandler_SetIniFrameSelect(AFrameSelect: Boolean); external GCDLLName Index 57;
function DDEHandler_GetDataMode: Boolean; external GCDLLName Index 58;
procedure DDEHandler_SetDataMode(ADataMode: Boolean); external GCDLLName Index 59;
function DDEHandler_GetDoAutoIns: Boolean; external GCDLLName Index 60;
procedure DDEHandler_SetDoAutoIns(AAutoIns: Boolean); external GCDLLName Index 61;
function DDEHandler_GetFChange: Word; external GCDLLName Index 62;
function DDEHandler_GetFClose: Word; external GCDLLName Index 63;
procedure DDEHandler_SetFLayerInfo(AFLInfo: Word); external GCDLLName Index 64;
function DDEHandler_GetFMoreSelect: Boolean; external GCDLLName Index 65;
function DDEHandler_GetFNew: Word; external GCDLLName Index 66;
function DDEHandler_GetFNoDBAutoIn: Boolean; external GCDLLName Index 67;
function DDEHandler_GetFOpen: Word; external GCDLLName Index 68;
function DDEHandler_GetFSave: Word; external GCDLLName Index 69;
function DDEHandler_GetFSaveAs: Word; external GCDLLName Index 70;
function DDEHandler_GetFSending: Boolean; external GCDLLName Index 71;
procedure DDEHandler_SetFSending(AFSending: Boolean); external GCDLLName Index 72;
function DDEHandler_GetFLMSettings: Boolean; external GCDLLName Index 73;
function DDEHandler_GetFShowADSA: Boolean; external GCDLLName Index 74;
function DDEHandler_GetFSuspendDDE: Boolean; external GCDLLName Index 75;
procedure DDEHandler_SetFSuspendDDE(AFSuspendDDE: Boolean); external GCDLLName Index 76;
function DDEHandler_GetInAction: Boolean; external GCDLLName Index 77;
function DDEHandler_GetMacroCnv: HConv; external GCDLLName Index 78;
procedure DDEHandler_SetMacroCnv(ACnv: HConv); external GCDLLName Index 79;
function DDEHandler_GetMacroHSZCI: HSZ; external GCDLLName Index 80;
procedure DDEHandler_SetMacroHSZCI(AHSZ: HSZ); external GCDLLName Index 81;
function DDEHandler_GetMacroHSZCT: HSZ; external GCDLLName Index 82;
procedure DDEHandler_SetMacroHSZCT(AHSZ: HSZ); external GCDLLName Index 83;
function DDEHandler_GetMacroMode: Boolean; external GCDLLName Index 84;
procedure DDEHandler_SetMacroMode(AMacroMode: Boolean); external GCDLLName Index 85;
function DDEHandler_GetNWUMode: Boolean; external GCDLLName Index 86;
procedure DDEHandler_SetNWUMode(ANWUMode: Boolean); external GCDLLName Index 87;
function DDEHandler_GetRequestFrom: Word; external GCDLLName Index 88;
procedure DDEHandler_SetRequestFrom(ARequestFrom: Word); external GCDLLName Index 89;
procedure DDEHandler_SetDDECommMode(AConversation: Word; AConvName: PChar); external GCDLLName Index 90;
procedure DDEHandler_GetNewDBEntry(ACommName: PChar; var ACon: Boolean; var AConProj: Boolean;
  var ASendStr: Boolean; var ASendProj: Boolean); external GCDLLName Index 91;
function DDEHandler_GetDDEApp(AIndex: Integer): PDDEApp; external GCDLLName Index 92;
function DDEHandler_GetDDEMLApp(AIndex: Integer): PDDEMLApp; external GCDLLName Index 93;
procedure DDEHandler_SendRefPoints(GeoReference: PLPointArray); external GCDLLName Index 94;
procedure DDEHandler_SetLayerName(ALName: PChar); external GCDLLName Index 95;
function DDEHandler_TranslateInStringDesma(ADDEString: PChar; var EndMode: Boolean): PChar; external GCDLLName Index 96;
procedure DDEHandler_DoExecute(AMacroHSZCT, AMacroHSZCI: HSZ; AItem: PChar; AMacroCnv: HConv); external GCDLLName Index 97;
procedure DDEHandler_GetMsgBoxParams(AText: PChar; var AMessage: LongInt; var ATextInt: LongInt); external GCDLLName Index 98;
procedure DDEHandler_UpdateDDEApp(ADDEApp: PDDEApp); external GCDLLName Index 99;
procedure DDEHandler_UpdateDDEMLApp(ADDEMLApp: PDDEMLApp); external GCDLLName Index 100;
procedure DDEHandler_GetLogData(ALData: PChar; var ALInt0: LongInt; var ALInt1: LongInt); external GCDLLName Index 101;
procedure DDEHandler_SetTRData(pSerial, pModCod, pLizence, p_im_TR: PChar); external GCDLLName Index 102;
procedure DDEHandler_InsertDDEAppWithParams(AName, AExeFile, AService, ATopic: PChar;
  AMenu: Integer; AConnect, ASendStrings: Boolean;
  AStrFormat, ADBCLayerInf: Word;
  AConnected, AActive: Boolean); external GCDLLName Index 103;
function DDEHandler_GetDDEAppWithParams(AIndex: Integer; AName, AExeFile, AService,
  ATopic: PChar; var AMenu: Integer;
  var AConnect: Boolean; var ASendStrings: Boolean;
  var AStrFormat: Word; var ADBCLayerInf: Word;
  var AConnected: Boolean;
  var AActive: Boolean): Boolean; external GCDLLName Index 104;
procedure DDEHandler_UpdateDDEAppWithParams(AName, AExeFile, AService, ATopic: PChar;
  AConnect, ASendStrings: Boolean); external GCDLLName Index 105;
procedure DDEHandler_InsertDDEMLAppWithParams(ACObjAddress: Pointer; AName, AExeFile,
  AStartParams, AService, ATopic, AItem: PChar;
  AConnect, AConnectProj, ASendStrings,
  ASendStrProj: Boolean; AStrFormat,
  ADBCLayerInf: Word; ACDDESepSign: Char;
  ACCallBack: Word; ACMWReady, ACGReady, ACGReadyAV,
  ACDBSelMode, ACSendLMInfo, ACShowADSA,
  ACOneSelect, ACDBAutoIns: Boolean; ACOpenInfo,
  ACSaveInfo, ACSaveAsInfo, ACNewInfo,
  ACChangeInfo, ACCloseInfo: Word; AHSCnv: HConv;
  AConnected, AActive, AActChanged,
  ARealActive: Boolean; AInitedProj: HWnd;
  ASendGReady: Boolean); external GCDLLName Index 106;
function DDEHandler_GetDDEMLAppWithParams(AIndex: Integer; var ACObjAddress: Pointer;
  AName: PChar; AExeFile: PChar; AStartParams: PChar;
  AService: PChar; ATopic: PChar; AItem: PChar;
  var AConnect: Boolean; var AConnectProj: Boolean;
  var ASendStrings: Boolean;
  var ASendStrProj: Boolean; var AStrFormat: Word;
  var ADBCLayerInf: Word; var ACDDESepSign: Char;
  var ACCallBack: Word; var ACMWReady: Boolean;
  var ACGReady: Boolean; var ACGReadyAV: Boolean;
  var ACDBSelMode: Boolean; var ACSendLMInfo: Boolean;
  var ACShowADSA: Boolean; var ACOneSelect: Boolean;
  var ACDBAutoIns: Boolean; var ACOpenInfo: Word;
  var ACSaveInfo: Word; var ACSaveAsInfo: Word;
  var ACNewInfo: Word; var ACChangeInfo: Word;
  var ACCloseInfo: Word; var AHSCnv: HConv;
  var AConnected: Boolean; var AActive: Boolean;
  var AActChanged: Boolean; var ARealActive: Boolean;
  var AInitedProj: HWnd;
  var ASendGReady: Boolean): Boolean; external GCDLLName Index 107;
procedure DDEHandler_UpdateDDEMLAppWithParams(AName, AExeFile, AService, ATopic: PChar;
  AConnect, ASendStrings: Boolean); external GCDLLName Index 108;
procedure DDEHandler_DoAllDDECommands(AChild: Pointer); external GCDLLName Index 109;
procedure DDEHandler_DoAllDDEMLCommands; external GCDLLName Index 110;
procedure DDEHandler_DeleteAllDDEData; external GCDLLName Index 111;

{$ENDIF}

procedure ProcSetDDEToFileMode(bOn: Boolean);
var
  AFileName: AnsiString;
begin
  if bDDEToFile <> bOn then
  begin
    bDDEToFile := bOn;
    if bDDEToFile then
    begin
      DDEOutputList := TStringList.Create;
      DDEOutputList.Capacity := 100000;
    end
    else
    begin
      AFileName := OSInfo.TempDataDir + 'wingis.dde';
      try
        DDEOutputList.SaveToFile(AFileName);
      finally
        DDEOutputList.Free;
        DDEOutputList := nil;
      end;
    end;
  end;
end;

function ProcSaveDDELine(s: PChar): Boolean;
begin
  try
    Result := True;
    DDEOutputList.Add(StrPas(s));
  except
    Result := False;
  end;
end;

function CountDDEItems(s: AnsiString): Integer;
var
  i, len: Integer;
begin
  Result := 0;
  len := Length(s);
  for i := 1 to len do
  begin
    if s[i] = '[' then
      Inc(Result);
  end;
end;

function GetDDEItem(s: AnsiString; n: Integer): Ansistring;
var
  i, Start, Cnt, len: Integer;
  Found: Boolean;
begin
  Result := '';
  Found := False;
  Cnt := 0;
  len := Length(s);
  for i := 1 to len do
  begin
    if s[i] = '[' then
      Inc(Cnt);
    if Cnt = n then
    begin
      Start := i;
      Found := True;
      break;
    end;
  end;
  if Found then
  begin
    i := Start + 1;
    while (s[i] <> ']') and (i <= len) do
    begin
      Result := Result + s[i];
      Inc(i);
    end;
  end;
end;
{*****************************************************************************************************************************}
{**************************************** Initialization *********************************************************************}
{*****************************************************************************************************************************}

constructor TDDEHandler.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  AMainWindow := AOwner;
  ActualChild := nil;
  FSendSep := True;
{$IFNDEF WMLT}
  ModuleOK := CreateCommunicationModule(WinGISMainForm.Handle, False);
{$ENDIF}
  DDEApps := New(PCollection, Init(3, 3));
  DDEMLApps := New(PCollection, Init(3, 3));
  DDEData := New(PBigUStrColl, Init);
  AllDDEData := New(PBigUStrColl, Init);
  SetDDEIniSettings;
end;

destructor TDDEHandler.Destroy;
begin
  if AllDDEData^.GetCount > 0 then
    DeleteAllDDEData;
  Dispose(AllDDEData, Done);
  if DDEData^.GetCount > 0 then
    DeleteDDEDataComplete;
  Dispose(DDEData, Done);
  Dispose(DDEMLApps, Done);
  Dispose(DDEApps, Done);
{$IFNDEF WMLT}
  if ModuleOK then
    DestroyCommunicationModule;
{$ENDIF}
  inherited Destroy;
end;

{*****************************************************************************************************************************}
{**************************************** Methodes ***************************************************************************}
{*****************************************************************************************************************************}

procedure TDDEHandler.SetDDEIniSettings;
begin
{$IFNDEF WMLT}
  if ModuleOK then
  begin
    with IniFile^ do
    begin
      DDEHandler_SetIniSettings(DDESeparateSign, MWReady, GReady, SendLMSettings, GReadyAfterView, ShowAfterDSA, OneSelect,
        DBAutoInsert, DDEMLConnect, DDEMLSend, DBSelMode, DoOneSelect, TRUE, DBLayerInfos,
        DDEMLCallBack, SendInfosToDB);
    end;
  end;
{$ENDIF}
end;

procedure TDDEHandler.ReadDDEWMIniSettings;

  procedure SendToModule
      (
      Item: PDDEApp
      )
      ; far;
  var
    sParamItem: string;
    Params: Word;
    sNoStartSection: string;
  begin
     {++ aschupp: new nostart parameter implementation BUILD#116}
    if ModuleOK then
    begin
      // loop through startparams
      // we are looking for '/c nostart=sectionname'
      if ParamCount > 0 then
      begin
        for Params := 1 to ParamCount do
        begin
          if ParamStr(Params) = STARTPARAM_COMMAND then
          begin
             // found a /c param
            if Params < ParamCount then
            begin

// TODO
{              if GetSubString(ParamStr(Params + 1), 1, '=') = 'nostart' then
              begin
                   // found a nostart directive, get the sectionname
                sNoStartSection := GetSubString(ParamStr(Params + 1), 2, '=');
                   // if it is the current inisection, then dont start the app by setting connect to false
                if Item^.Name = sNoStartSection then
                  Item^.Connect := FALSE;
              end;}
            end;
          end;
        end;
      end;
      {-- aschupp}
      with Item^ do
      begin
{$IFNDEF WMLT}
        DDEHandler_InsertDDEAppWithParams(Name, ExeFile, Service, Topic, Menu, Connect, SendStrings,
          StrFormat, DBCLayerInf, Connected, Active);
{$ENDIF}
      end;
    end;
  end;
begin
    //IniFile^.ReadDDEApps(DDEApps);
    //DDEApps^.ForEach(@SendToModule);
end;

procedure TDDEHandler.ReadDDEMLServerIniSettings;
var
  Service: array[0..255] of Char;
  Topic: array[0..255] of Char;
  Item: array[0..255] of Char;
begin
  IniFile^.ReadDDEMLWingisServerSettings(Service, Topic, Item);
{$IFNDEF WMLT}
  if ModuleOK then
    DDEHandler_InsertDDEMLServerSettings(Service, Topic, Item);
{$ENDIF}
end;

procedure TDDEHandler.ReadDDEMLIniSettings;

  procedure SendToModule
      (
      Item: PDDEMLApp
      )
      ; far;
  var
    sParamItem: string;
    Params: Word;
    sNoStartSection: string;
  begin
    if ModuleOK then
    begin
      {++ aschupp: new nostart parameter implementation BUILD#116}
      // loop through the startparams
      // we are looking for a string in the form of: /c nostart=sectionname
      if ParamCount > 0 then
      begin
        for Params := 1 to ParamCount do
        begin
          if ParamStr(Params) = STARTPARAM_COMMAND then
          begin
             // found a /c param
            if Params < ParamCount then
            begin
// TODO
{              if GetSubString(ParamStr(Params + 1), 1, '=') = 'nostart' then
              begin
                   // found a nostart directive, get the sectionname
                sNoStartSection := GetSubString(ParamStr(Params + 1), 2, '=');
                   // if it is the current inisection, then dont start the app by setting connect to false
                if Item^.Name = sNoStartSection then
                  Item^.Connect := FALSE;
              end;}
            end;
          end;
        end;
      end;
      {-- aschupp}
      with Item^ do
      begin
{$IFNDEF WMLT}
        DDEHandler_InsertDDEMLAppWithParams(CObjAddress, Name, ExeFile, StartParams, Service, Topic, Item,
          Connect, ConnectProj, SendStrings, SendStrProj, StrFormat,
          DBCLayerInf, CDDESepSign, CCallBack, CMWReady, CGReady, CGReadyAV,
          CDBSelMode, CSendLMInfo, CShowADSA, COneSelect, CDBAutoIns, COpenInfo,
          CSaveInfo, CSaveAsInfo, CNewInfo, CChangeInfo, CCloseInfo, HSCnv,
          Connected, Active, ActChanged, RealActive, InitedProj, SendGReady);
{$ENDIF}
      end;
    end;
  end;
begin
    //IniFile^.ReadDDEMLApps(DDEMLApps);
    //if ModuleOK then DDEMLApps^.ForEach(@SendToModule);
end;

procedure TDDEHandler.UpdateAppLists;

  procedure UpdateDDEApp
      (
      Item: PDDEApp
      )
      ; far;
  begin
      {if ModuleOK then DDEHandler_UpdateDDEApp(Item);}
{$IFNDEF WMLT}
    if ModuleOK then
      with Item^ do
      begin
        DDEHandler_UpdateDDEAppWithParams(Name, ExeFile, Service, Topic, Connect, SendStrings);
      end;
{$ENDIF}
  end;

  procedure UpdateDDEMLApp
      (
      Item: PDDEMLApp
      )
      ; far;
  begin
      {if ModuleOK then DDEHandler_UpdateDDEMLApp(Item);}
{$IFNDEF WMLT}
    if ModuleOK then
      with Item^ do
      begin
        DDEHandler_UpdateDDEMLAppWithParams(Name, ExeFile, Service, Topic, Connect, SendStrings);
      end;
{$ENDIF}
  end;
begin
{$IFNDEF WMLT}
  if ModuleOK then
  begin
    DDEApps^.ForEach(@UpdateDDEApp);
    DDEMLApps^.ForEach(@UpdateDDEMLApp);
  end;
{$ENDIF}
end;

procedure TDDEHandler.SetDBSendStrings
  (
  SendMode: Word;
  ProjToInit: Pointer
  );
begin
{$IFNDEF WMLT}
  if ModuleOK then
  begin
    if ProjToInit <> nil then
      DDEHandler_SetDBSendStrings(SendMode, TMDIChild(ProjToInit).Handle)
    else
      DDEHandler_SetDBSendStrings(SendMode, 0);
  end;
{$ENDIF}
end;

function TDDEHandler.SendString(AText: ShortString): Boolean;
begin
{$IFNDEF WMLT}
  if ModuleOK then
  begin
    if CoreConnected then
    begin
      WinGISMainForm.DXAppTCore.FireDDEEvent(AText);
      Result := True;
    end
    else
      if (bDDEToFile) and (Pos('[END]', AText) = 0) then
      begin
        Result := ProcSaveDDELine(PChar(AnsiString(AText)));
      end
      else
      begin
        Result := DDEHandler_SendString(AText);
      end;
  end
  else
{$ENDIF}
    Result := FALSE;
end;

function TDDEHandler.SendPChar(AText: PChar): Boolean;
begin
{$IFNDEF WMLT}
  if ModuleOK then
  begin
    if CoreConnected then
    begin
      WingisMainForm.DXAppTCore.FireDDEEvent(AnsiString(AText));
      Result := True;
    end
    else
      if (bDDEToFile) and (Pos('[END]', StrPas(AText)) = 0) then
      begin
        Result := ProcSaveDDELine(AText);
      end
      else
      begin
        Result := DDEHandler_SendPChar(AText)
      end;
  end
  else
{$ENDIF}
    Result := FALSE;
end;

procedure TDDEHandler.DDEAcknowledge(var Msg: TMessage);
begin
{$IFNDEF WMLT}
  if ModuleOK then
    DDEHandler_DDEAcknowledge(Msg);
{$ENDIF}
end;

procedure TDDEHandler.DDETerminate(ServerHandle: THandle; MustBeSender: Boolean;
  TermDDEML: Boolean);
begin
{$IFNDEF WMLT}
  if ModuleOK then
    DDEHandler_DDETerminate(ServerHandle, MustBeSender, TermDDEML);
{$ENDIF}
end;

procedure TDDEHandler.SendAck(AWindow: HWnd; AParam: LongInt);
begin
{$IFNDEF WMLT}
  if ModuleOK then
    DDEHandler_SendAck(AWindow, AParam);
{$ENDIF}
end;

procedure TDDEHandler.SetAnswerMode(SetMode: Boolean);
begin
{$IFNDEF WMLT}
  if ModuleOK then
    DDEHandler_SetAnswerMode(SetMode)
{$ENDIF}
end;

procedure TDDEHandler.EnableDDEReception(AEnable: Boolean);
begin
{$IFNDEF WMLT}
  if ModuleOK then
    DDEHandler_EnableDDEReception(AEnable);
{$ENDIF}
end;

procedure TDDEHandler.CloseProject(AChildWindow: Pointer);
var
  Dir: array[0..fsPathName] of Char;
  DName: array[0..fsFileName + fsExtension] of Char;
  Ext: array[0..fsExtension] of Char;
  SendStr: string;
begin
{$IFNDEF WMLT}
  if AChildWindow <> nil then
    with TMDIChild(AChildWindow) do
    begin
      SetDBSendStrings(DDECommPInfo, nil);
      if FClose > 0 then
        SendString('[PCL][' + StrPas(FName) + ']')
      else
      begin
        FileSplit(FName, Dir, DName, Ext);
        SendStr := StrPas(DName);
        SendString('[PCL][' + SendStr + ']');
      end;
      SetDBSendStrings(DDECommAll, nil);
    end;
  SetActualChild(nil);
{$ENDIF}
end;

procedure TDDEHandler.UnActivateConnections;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    DDEHandler_UnActivateConnections;
{$ENDIF}
end;

procedure TDDEHandler.GetNewDDECommand;
var
  HSZSTopic: HSZ;
  HSZClientTopic: HSZ;
  AStr: PChar;
  ACnv: PChar;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    DDEHandler_GetDDECommand(HSZSTopic, HSZClientTopic, AStr, ACnv);
  if ActualChild <> nil then
  begin
    TMDIChild(ActualChild).AcceptExecute(HSZSTopic, HSZClientTopic, AStr, ACnv);
  end
  else
    if AMainWindow <> nil then
    begin
      TWinGISMainForm(AMainWindow).AcceptExecute(HSZSTopic, HSZClientTopic, AStr, ACnv);
    end;
{$ENDIF}
end;

procedure TDDEHandler.InsertAllDDEData(DDELine: PChar);
begin
  AllDDEData^.Insert(StrNew(DDELine));
end;

procedure TDDEHandler.InsertDDEData(DDELine: PChar);
{$IFDEF LOG}
var
  LogStr: string;
{$ENDIF}
begin
{$IFDEF LOG}
  if LogFile.Logging then
  begin
    Str(DDEData^.GetCount: 0, LogStr);
    LogFile.WriteLog('Start InsertDDEData with ' + LogStr);
  end;
{$ENDIF}
  DDEData^.Insert(StrNew(DDELine));
{$IFDEF LOG}
  if LogFile.Logging then
  begin
    LogFile.WriteLog('InsertDDEData: ' + DDELine);
    Str(DDEData^.GetCount: 0, LogStr);
    LogFile.WriteLog('End InsertDDEData with ' + LogStr);
  end;
{$ENDIF}
end;

procedure TDDEHandler.DeleteAllDDEData;

  procedure DelStrings(Item: PChar); far;
  begin
    StrDispose(Item);
  end;
begin
  AllDDEData^.ForEach(@DelStrings);
  AllDDEData^.DeleteAll;
{$IFNDEF WMLT}
  DDEHandler_DeleteAllDDEData;
{$ENDIF}
end;

procedure TDDEHandler.DeleteDDEData;
var
  i: LongInt;
{$IFDEF LOG}
  LogStr: string;
{$ENDIF}

  function DelString(Item: PChar): Boolean;
  begin
    Result := FALSE;
    if Item[0] = DDEStrUsed then
    begin
{$IFDEF LOG}
      if LogFile.Logging then
        LogFile.WriteLog('DeleteDDEData: ' + Item);
{$ENDIF}
      StrDispose(Item);
      Result := TRUE;
    end;
  end; // End of the internal function DelString

begin
{$IFDEF LOG}
  if LogFile.Logging then
  begin
    Str(DDEData^.GetCount: 0, LogStr);
    LogFile.WriteLog('Start DeleteDDEData with ' + LogStr);
  end;
{$ENDIF}
  for i := DDEData^.GetCount - 1 downto 0 do
  begin
    if DelString(DDEData^.At(i)) then
      DDEData^.AtDelete(i);
  end;
{$IFDEF LOG}
  if LogFile.Logging then
  begin
    Str(DDEData^.GetCount: 0, LogStr);
    LogFile.WriteLog('End DeleteDDEData with ' + LogStr);
  end;
{$ENDIF}

end; // End of the procedure TDDEHandler.DeleteDDEData

procedure TDDEHandler.DeleteDDEDataComplete;
{$IFDEF LOG}
var
  LogStr: string;
{$ENDIF}

  procedure DelStrings
      (
      Item: PChar
      ); far;
  begin
{$IFDEF LOG}
    if LogFile.Logging then
      LogFile.WriteLog('DeleteDDEDataComplete: ' + Item);
{$ENDIF}
    StrDispose(Item);
  end;
begin
{$IFDEF LOG}
  if LogFile.Logging then
  begin
    Str(DDEData^.GetCount: 0, LogStr);
    LogFile.WriteLog('Start DeleteDDEDataComplete with ' + LogStr);
  end;
{$ENDIF}
  DDEData^.ForEach(@DelStrings);
  DDEData^.DeleteAll;
{$IFDEF LOG}
  if LogFile.Logging then
  begin
    Str(DDEData^.GetCount: 0, LogStr);
    LogFile.WriteLog('End DeleteDDEDataComplete with ' + LogStr);
  end;
{$ENDIF}
end;

function TDDEHandler.InitiateDDE
  (
  AWindow: THandle;
  LoadDatabase: Boolean;
  InitDDEML: Boolean
  )
  : Boolean;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_InitiateDDE(AWindow, LoadDatabase, InitDDEML)
  else
{$ENDIF}
    Result := FALSE;
end;

procedure TDDEHandler.CombineParamsOfAllActiveConnections;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    DDEHandler_CombineParamsOfAllActiveConnections;
{$ENDIF}
end;

procedure TDDEHandler.SetActualChild
  (
  AChild: Pointer
  );
begin
  if ModuleOK then
  begin
    ActualChild := AChild;

{      if AChild <> NIL then
         DDEHandler_SetActualChild(TMDIChild(AChild).Handle)
      else
         DDEHandler_SetActualChild(0);
}
  end;
end;

procedure TDDEHandler.DoDDEInitiate
  (
  var Msg: TMessage
  );
begin
{$IFNDEF WMLT}
  if ModuleOK then
    DDEHandler_DDEInitiate(Msg);
{$ENDIF}
end;

procedure TDDEHandler.DoDDETerminate
  (
  ServerHandle: THandle;
  MustBeSender: Boolean;
  TermDDEML: Boolean
  );
begin
{$IFNDEF WMLT}
  if ModuleOK then
    DDEHandler_DDETerminate(ServerHandle, MustBeSender, TermDDEML);
{$ENDIF}
end;

procedure TDDEHandler.SetDDECommMode
  (
  AConversation: Word;
  AConvName: PChar
  );
begin
{$IFNDEF WMLT}
  if ModuleOK then
    DDEHandler_SetDDECommMode(AConversation, AConvName);
{$ENDIF}
end;

function TDDEHandler.GetDBLInfo
  : Word;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_GetDBLInfo
  else
{$ENDIF}
    Result := 0;
end;

function TDDEHandler.GetDBStrFormat
  : Word;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_GetDBStrFormat
  else
{$ENDIF}
    Result := 0;
end;

function TDDEHandler.TranslateInStringDesma
  (
  ADDEString: PChar;
  var EndMode: Boolean
  )
  : PChar;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_TranslateInStringDesma(ADDEString, EndMode)
  else
{$ENDIF}
    Result := nil;
end;

procedure TDDEHandler.GetLayerName
  (
  ALayerIndex: LongInt
  );
var
  ALayer: PLayer;
  TempName: array[0..255] of Char;
begin
{$IFNDEF WMLT}
  if ModuleOK then
  begin
    if ActualChild <> nil then
    begin
      ALayer := TMDIChild(ActualChild).Data^.Layers^.IndexToLayer(ALayerIndex);
      if ALayer <> nil then
      begin
        StrPCopy(TempName, ALayer^.Text^);
        DDEHandler_SetLayerName(TempName);
      end
      else
        DDEHandler_SetLayerName('');
    end
    else
      DDEHandler_SetLayerName('');
  end;
{$ENDIF}
end;

procedure TDDEHandler.GetTopLayerName;
var
  ALayer: PLayer;
  TempName: array[0..255] of Char;
begin
{$IFNDEF WMLT}
  if ModuleOK then
  begin
    if ActualChild <> nil then
    begin
      StrPCopy(TempName, TMDIChild(ActualChild).Data^.Layers^.TopLayer^.Text^);
      DDEHandler_SetLayerName(TempName);
    end
    else
      DDEHandler_SetLayerName('');
  end;
{$ENDIF}
end;

function TDDEHandler.QuestHServer
  : Word;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_QuestHServer
  else
{$ENDIF}
    Result := 0;
end;

procedure TDDEHandler.DoAllDDECommands
  (
  ActChild: Pointer
  );

  procedure DoAll
      (
      Item: PChar
      ); far;
  begin
{$IFNDEF WMLT}
    TMDIChild(ActChild).DoDDECommands(Item);
{$ENDIF}
  end;
begin
{$IFNDEF WMLT}
  SendString('[MAB][]');
  AllDDEData^.ForEach(@DoAll);
  DDEHandler_DoAllDDECommands(ActChild);
  SendString('[MAE][]');
  DeleteAllDDEData;
{$ENDIF}
end;

procedure TDDEHandler.DoAllDDEMLCommands
  (
  ActChild: Pointer
  );

  procedure DoAll
      (
      Item: PChar
      ); far;
  begin
{$IFNDEF WMLT}
    if ModuleOK then
      DDEHandler_DoExecute(MacroHSZCT, MacroHSZCI, Item, MacroCnv);
{$ENDIF}
  end;
begin
{$IFNDEF WMLT}
  SendString('[MAB][]');
  AllDDEData^.ForEach(@DoAll);
  DDEHandler_DoAllDDEMLCommands;
  SendString('[MAE][]');
  DeleteAllDDEData;
{$ENDIF}
end;

function TDDEHandler.DDEOk
  : Boolean;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_GetDDEOK
  else
{$ENDIF}
    Result := FALSE;
end;

procedure TDDEHandler.SetDBWMProjInited
  (
  SetInited: Boolean
  );
begin
{$IFNDEF WMLT}
  if ModuleOK then
    DDEHandler_SetDBWMProjInited(SetInited);
{$ENDIF}
end;

function TDDEHandler.EnableDDEMLConversation
  (
  AEnable: Boolean
  )
  : Boolean;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_EnableDDEMLConversation(AEnable)
  else
{$ENDIF}
    Result := FALSE;
end;

function TDDEHandler.AppendList1
  (
  Index: LongInt;
  NameLength: Integer
  )
  : Boolean;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_AppendList1(Index, NameLength)
  else
{$ENDIF}
    Result := FALSE;
end;

function TDDEHandler.EndList1
  (
  SendEnd: Boolean;
  NameLength: Integer
  )
  : Boolean;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_EndList1(SendEnd, NameLength)
  else
{$ENDIF}
    Result := FALSE;
end;

procedure TDDEHandler.StartList
  (
  AText: ShortString
  );
begin
{$IFNDEF WMLT}
  if ModuleOK then
    DDEHandler_StartList(AText);
{$ENDIF}
end;

function TDDEHandler.AppendList
  (
  Index: LongInt
  )
  : Boolean;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_AppendList(Index)
  else
{$ENDIF}
    Result := FALSE;
end;

function TDDEHandler.EndList
  (
  SendEnd: Boolean
  )
  : Boolean;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_EndList(SendEnd)
  else
{$ENDIF}
    Result := FALSE;
end;

function TDDEHandler.AppendString
  (
  AStr: string
  )
  : Boolean;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_AppendString(AStr)
  else
{$ENDIF}
    Result := FALSE;
end;

procedure TDDEHandler.Reset;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    DDEHandler_Reset;
{$ENDIF}
end;

function TDDEHandler.Append
  (
  AStr: string
  )
  : Boolean;
var
  TmpStr: array[0..255] of Char;
begin
{$IFNDEF WMLT}
  if ModuleOK then
  begin
    StrPCopy(TmpStr, Astr);
    Result := DDEHandler_Append(TmpStr);
  end
  else
{$ENDIF}
    Result := FALSE;
end;

function TDDEHandler.Execute
  : Boolean;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_Execute
  else
{$ENDIF}
    Result := FALSE;
end;

function TDDEHandler.GetGlobal
  (
  Size: LongInt
  )
  : Boolean;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_GetGlobal(Size)
  else
{$ENDIF}
    Result := FALSE;
end;

procedure TDDEHandler.FreeGlobal;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    DDEHandler_FreeGlobal;
{$ENDIF}
end;

procedure TDDEHandler.SendSHWADSA;
begin
{$IFNDEF WMLT}
  SetAnswerMode(TRUE);
  SetDBSendStrings(DDECommSADSA, 0);
  SendString('[SHW][]');
  SetDBSendStrings(DDECommAll, 0);
  SetAnswerMode(FALSE);
{$ENDIF}
end;

procedure TDDEHandler.SendRefPoints
  (
  GeoReference: PLPointArray
  );
begin
{$IFNDEF WMLT}
  if ModuleOK then
    DDEHandler_SendRefPoints(GeoReference);
{$ENDIF}
end;

function TDDEHandler.Write
  (
  Pos: LongInt;
  AStr: ShortString
  )
  : Boolean;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    DDEHandler_Write(Pos, AStr)
  else
{$ENDIF}
    Result := FALSE;
end;

function TDDEHandler.QuestLCnt
  : Integer;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_QuestLCnt
  else
{$ENDIF}
    Result := 0;
end;

function TDDEHandler.GetDBLoaded
  : Integer;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_GetDBLoaded
  else
{$ENDIF}
    Result := 0;
end;

function TDDEHandler.GetDDEMLLoaded
  : Integer;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_GetDDEMLLoaded
  else
{$ENDIF}
    Result := 0;

  if Result > 0 then
  begin
    RunUpdateOnClose := False;
  end;
end;

procedure TDDEHandler.SetDBAutoInsert
  (
  ADBAutoInsert: Boolean
  );
begin
{$IFNDEF WMLT}
  if ADBAutoInsert then
    MenuFunctions['DatabaseAutoEditInsert'].Checked := TRUE
  else
    MenuFunctions['DatabaseAutoEditInsert'].Checked := FALSE;
{$ENDIF}
end;

procedure TDDEHandler.SetOneSelect
  (
  AOneSelect: Boolean
  );
begin
{$IFNDEF WMLT}
  if AOneSelect then
  begin
    IniFile^.DoOneSelect := TRUE;
    IniFile^.FrameSelect := FALSE;
    MenuFunctions['DatabaseAutoMonitoring'].Checked := TRUE;
  end
  else
  begin
    IniFile^.DoOneSelect := FALSE;
    IniFile^.FrameSelect := TRUE;
    MenuFunctions['DatabaseAutoMonitoring'].Checked := FALSE;
  end;
{$ENDIF}
end;

procedure TDDEHandler.GetNewDBEntry
  (
  ACommName: PChar;
  var ACon: Boolean;
  var AConProj: Boolean;
  var ASendStr: Boolean;
  var ASendProj: Boolean
  );
begin
{$IFNDEF WMLT}
  if ModuleOK then
    DDEHandler_GetNewDBEntry(ACommName, ACon, AConProj, ASendStr, ASendProj);
{$ENDIF}
end;

procedure TDDEHandler.WriteDDEDDEMLSettings;
var
  i: Integer;
  NewDDEApp: PDDEApp;
  NewDDEMLApp: PDDEMLApp;
  EntryFound: Boolean;
  AName: array[0..255] of Char;
  AExeFile: array[0..255] of Char;
  AStartParams: array[0..255] of Char;
  AService: array[0..255] of Char;
  ATopic: array[0..255] of Char;
  AItem: array[0..255] of Char;
  AMenu: Integer;
  AConnect: Boolean;
  AConnectProj: Boolean;
  ASendStrings: Boolean;
  ASendStrProj: Boolean;
  AStrFormat: Word;
  ADBCLayerInf: Word;
  AConnected: Boolean;
  AActive: Boolean;
  ACObjAddress: Pointer;
  ACDDESepSign: Char;
  ACCallBack: Word;
  ACMWReady: Boolean;
  ACGReady: Boolean;
  ACGReadyAV: Boolean;
  ACDBSelMode: Boolean;
  ACSendLMInfo: Boolean;
  ACShowADSA: Boolean;
  ACOneSelect: Boolean;
  ACDBAutoIns: Boolean;
  ACOpenInfo: Word;
  ACSaveInfo: Word;
  ACSaveAsInfo: Word;
  ACNewInfo: Word;
  ACChangeInfo: Word;
  ACCloseInfo: Word;
  AHSCnv: HConv;
  AActChanged: Boolean;
  ARealActive: Boolean;
  AInitedProj: HWnd;
  ASendGReady: Boolean;
begin
{$IFNDEF WMLT}
  if ModuleOK then
  begin
    DDEApps^.FreeAll;
    i := 0;
      {repeat
        NewDDEApp:=DDEHandler_GetDDEApp(i);
        if NewDDEApp <> NIL then DDEApps^.Insert(New(PDDEApp,InitByListEntry(NewDDEApp)));
        Inc(i);
      until NewDDEApp = NIL;}
    repeat
      EntryFound := DDEHandler_GetDDEAppWithParams(i, AName, AExeFile, AService, ATopic, AMenu, AConnect,
        ASendStrings, AStrFormat, ADBCLayerInf, AConnected, AActive);
      if EntryFound then
      begin
        NewDDEApp := New(PDDEApp, Init(AName, AExeFile, AService, ATopic, AMenu, ASendStrings, AStrFormat, ADBCLayerInf));
        with NewDDEApp^ do
        begin
          Connect := AConnect;
          Connected := AConnected;
          Active := AActive;
        end;
        DDEApps^.Insert(NewDDEApp);
      end;
      Inc(i);
    until not EntryFound;
    DDEMLApps^.FreeAll;
    i := 0;
      {repeat
        NewDDEMLApp:=DDEHandler_GetDDEMLApp(i);
        if NewDDEMLApp <> NIL then DDEApps^.Insert(New(PDDEMLApp,InitByListEntry(NewDDEMLApp)));
        Inc(i);
      until NewDDEMLApp = NIL;}
    repeat
      EntryFound := DDEHandler_GetDDEMLAppWithParams(i, ACObjAddress, AName, AExeFile, AStartParams, AService, ATopic, AItem,
        AConnect, AConnectProj, ASendStrings, ASendStrProj, AStrFormat,
        ADBCLayerInf, ACDDESepSign, ACCallBack, ACMWReady, ACGReady,
        ACGReadyAV, ACDBSelMode, ACSendLMInfo, ACShowADSA, ACOneSelect,
        ACDBAutoIns, ACOpenInfo, ACSaveInfo, ACSaveAsInfo, ACNewInfo,
        ACChangeInfo, ACCloseInfo, AHSCnv, AConnected, AActive,
        AActChanged, ARealActive, AInitedProj, ASendGReady);
      if EntryFound then
      begin
        NewDDEMLApp := New(PDDEMLApp, Init(AName, AExeFile, AStartParams, AService, ATopic, AItem, AConnect, AConnectProj,
          ASendStrings, ASendStrProj, AStrFormat, ADBCLayerInf, ACDDESepSign, ACCallBack, ACMWReady,
          ACGReady, ACGReadyAV, ACDBSelMode, ACSendLMInfo, ACShowADSA, ACOneSelect, ACDBAutoIns, ACOpenInfo,
          ACSaveInfo, ACSaveAsInfo, ACNewInfo, ACChangeInfo, ACCloseInfo));
        with NewDDEMLApp^ do
        begin
          CObjAddress := ACObjAddress;
          HSCnv := AHSCnv;
          Connected := AConnected;
          Active := AActive;
          ActChanged := AActChanged;
          RealActive := ARealActive;
          InitedProj := AInitedProj;
          SendGReady := ASendGReady;
        end;
        DDEMLApps^.Insert(NewDDEMLApp);
      end;
      Inc(i);
    until not EntryFound;
      //IniFile^.WriteDDEDDEMLSettings(DDEApps,DDEMLApps);
  end;
{$ENDIF}
end;

procedure TDDEHandler.SetStatusText
  (
  AText: LongInt
  );
begin
  if AText > 0 then
    UserInterface.StatusText := GetLangText(AText)
  else
    UserInterface.StatusText := '';
end;

procedure TDDEHandler.DoShowMessage;
var
  AText: array[0..255] of Char;
  AMessage: LongInt;
  ATextInt: LongInt;
  TmpText: string;
begin
{$IFNDEF WMLT}
  if ModuleOK then
  begin
    DDEHandler_GetMsgBoxParams(AText, AMessage, ATextInt);
    if AMessage > 0 then
    begin
      if ATextInt > 0 then
        TmpText := GetLangText(ATextInt)
      else
        TmpText := StrPas(AText);
      MsgBox(TWinGISMainForm(AMainWindow).Handle, AMessage, mb_OK or mb_IconExclamation, TmpText);
    end;
  end;
{$ENDIF}
end;

{$H+}

procedure TDDEHandler.WriteLog;
var
  ALData: array[0..4096] of Char;
  ALInt0: LongInt;
  ALInt1: LongInt;
  TmpString: AnsiString;
begin
{$IFNDEF WMLT}
  if ModuleOK then
  begin
    DDEHandler_GetLogData(ALData, ALInt0, ALInt1);
    if (ALInt0 <> 0) or (ALInt1 <> 0) then
    begin
      TmpString := '';
      if ALInt0 <> 0 then
        TmpString := GetLangText(ALInt0);
      if ALInt1 <> 0 then
        TmpString := TmpString + GetLangText(ALInt1);
    end
    else
      TmpString := StrPas(ALData);
  end;
  if (LogFile.Logging) and (TmpString <> '') then
  begin
    LogFile.WriteLog(TmpString);
  end;
{$ENDIF}
end;
{$H-}

procedure TDDEHandler.SetTRData;
var
  pSerial: PChar;
  pModCod: PChar;
  pLizence: PChar;
  p_im_TR: PChar;
begin
{$IFNDEF WMLT}
  GetSerial(pSerial);
  GetModuleCode(pModCod);
  GetLicenceName(pLizence);
  StrCopy(p_im_TR, im_TurboRaster);
  DDEHandler_SetTRData(pSerial, pModCod, pLizence, p_im_TR);
{$ENDIF}
end;

{*****************************************************************************************************************************}
{****************************** Properties ***********************************************************************************}
{*****************************************************************************************************************************}

function TDDEHandler.ReadFGotLInfo: Word;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_GetFGotLInfo
  else
{$ENDIF}
    Result := 0;
end;

procedure TDDEHandler.WriteFGotLInfo(AFGLInfo: Word);
begin
{$IFNDEF WMLT}
  if ModuleOK then
    DDEHandler_SetFGotLInfo(AFGLInfo);
{$ENDIF}
end;

function TDDEHandler.ReadFDDESepSign
  : Char;
begin
  if ModuleOK then
    Result := IniFile^.DDESeparateSign
  else
    Result := ',';
end;

function TDDEHandler.ReadFSendLMSets
  : Boolean;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_GetFLMSettings
  else
{$ENDIF}
    Result := FALSE;
end;

function TDDEHandler.ReadFLayerInfo
  : Word;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_GetFLayerInfo
  else
{$ENDIF}
    Result := 0;
end;

function TDDEHandler.ReadFSendMWR
  : Boolean;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_GetFSendMWR
  else
{$ENDIF}
    Result := FALSE;
end;

function TDDEHandler.ReadMacroMode
  : Boolean;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_GetMacroMode
  else
{$ENDIF}
    Result := FALSE;
end;

procedure TDDEHandler.WriteMacroMode
  (
  AMode: Boolean
  );
begin
{$IFNDEF WMLT}
  if ModuleOK then
    DDEHandler_SetMacroMode(AMode);
{$ENDIF}
end;

function TDDEHandler.ReadFSuspendDDE
  : Boolean;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_GetFSuspendDDE
  else
{$ENDIF}
    Result := FALSE;
end;

procedure TDDEHandler.WriteFSuspendDDE
  (
  ASuspend: Boolean
  );
begin
{$IFNDEF WMLT}
  if ModuleOK then
    DDEHandler_SetFSuspendDDE(ASuspend);
{$ENDIF}
end;

function TDDEHandler.ReadDBSelMode
  : Boolean;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_GetDBSelMode
  else
{$ENDIF}
    Result := FALSE;
end;

procedure TDDEHandler.WriteDBSelMode
  (
  ADBSMode: Boolean
  );
begin
{$IFNDEF WMLT}
  if ModuleOK then
    DDEHandler_SetDBSelMode(ADBSMode);
{$ENDIF}
end;

function TDDEHandler.ReadDoAutoIns
  : Boolean;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_GetDoAutoIns
  else
{$ENDIF}
    Result := False;
  if Result = False then
    Result := IniFile^.ReadBoolean('Settings', 'DBAutoInsert', False);
end;

procedure TDDEHandler.WriteDoAutoIns
  (
  ADoAIns: Boolean
  );
begin
{$IFNDEF WMLT}
  if ModuleOK then
    DDEHandler_SetDoAutoIns(ADoAIns);
{$ENDIF}
end;

function TDDEHandler.ReadFOneSelect
  : Boolean;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_GetFOneSelect
  else
{$ENDIF}
    Result := FALSE;
end;

procedure TDDEHandler.WriteFOneSelect
  (
  AFOneSelect: Boolean
  );
begin
{$IFNDEF WMLT}
  if ModuleOK then
    DDEHandler_SetFOneSelect(AFOneSelect);
{$ENDIF}
end;

function TDDEHandler.ReadFOpen
  : Word;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_GetFOpen
  else
{$ENDIF}
    Result := 0;
end;

function TDDEHandler.ReadFSave
  : Word;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_GetFSave
  else
{$ENDIF}
    Result := 0;
end;

function TDDEHandler.ReadFSaveAs
  : Word;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_GetFSaveAs
  else
{$ENDIF}
    Result := 0;
end;

function TDDEHandler.ReadFChange
  : Word;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_GetFChange
  else
{$ENDIF}
    Result := 0;
end;

function TDDEHandler.ReadFNew
  : Word;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_GetFNew
  else
{$ENDIF}
    Result := 0;
end;

function TDDEHandler.ReadFClose
  : Word;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_GetFClose
  else
{$ENDIF}
    Result := 0;
end;

function TDDEHandler.ReadRequestFrom
  : Word;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_GetRequestFrom
  else
{$ENDIF}
    Result := 0;
end;

procedure TDDEHandler.WriteRequestFrom
  (
  ARFrom: Word
  );
begin
{$IFNDEF WMLT}
  if ModuleOK then
    DDEHandler_SetRequestFrom(ARFrom);
{$ENDIF}
end;

function TDDEHandler.ReadInAction
  : Boolean;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_GetInAction
  else
{$ENDIF}
    Result := FALSE;
end;

function TDDEHandler.ReadNWUMode
  : Boolean;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_GetNWUMode
  else
{$ENDIF}
    Result := FALSE;
end;

procedure TDDEHandler.WriteNWUMode
  (
  ANWUMode: Boolean
  );
begin
{$IFNDEF WMLT}
  if ModuleOK then
    DDEHandler_SetNWUMode(ANWUMode);
{$ENDIF}
end;

function TDDEHandler.ReadFSendGR
  : Boolean;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_GetFSendGR
  else
{$ENDIF}
    Result := FALSE;
end;

function TDDEHandler.ReadFSendGRAV
  : Boolean;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_GetFSendGRAV
  else
{$ENDIF}
    Result := FALSE;
end;

function TDDEHandler.ReadFNoDBAutoIn
  : Boolean;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_GetFNoDBAutoIn
  else
{$ENDIF}
    Result := FALSE;
end;

function TDDEHandler.ReadFMoreSelect
  : Boolean;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_GetFMoreSelect
  else
{$ENDIF}
    Result := FALSE;
end;

function TDDEHandler.ReadFOutSepSign
  : Char;
begin
{$IFNDEF WMLT}
  Result := IniFile^.DDESeparateSign;
{$ENDIF}
end;

procedure TDDEHandler.WriteFLayerInfo
  (
  AFLInfo: Word
  );
begin
{$IFNDEF WMLT}
  if ModuleOK then
    DDEHandler_SetFLayerInfo(AFLInfo);
{$ENDIF}
end;

function TDDEHandler.ReadFShowADSA
  : Boolean;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_GetFShowADSA
  else
{$ENDIF}
    Result := FALSE;
end;

function TDDEHandler.ReadMacroConv
  : HConv;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_GetMacroCnv
  else
{$ENDIF}
    Result := 0;
end;

procedure TDDEHandler.WriteMacroConv
  (
  AHConv: HConv
  );
begin
{$IFNDEF WMLT}
  if ModuleOK then
    DDEHandler_SetMacroCnv(AHConv);
{$ENDIF}
end;

function TDDEHandler.ReadMacroHSZCT
  : HSZ;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_GetMacroHSZCT
  else
{$ENDIF}
    Result := 0;
end;

procedure TDDEHandler.WriteMacroHSZCT
  (
  AHSZCT: HSZ
  );
begin
{$IFNDEF WMLT}
  if ModuleOK then
    DDEHandler_SetMacroHSZCT(AHSZCT);
{$ENDIF}
end;

function TDDEHandler.ReadMacroHSZCI
  : HSZ;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_GetMacroHSZCI
  else
{$ENDIF}
    Result := 0;
end;

procedure TDDEHandler.WriteMacroHSZCI
  (
  AHSZCI: HSZ
  );
begin
{$IFNDEF WMLT}
  if ModuleOK then
    DDEHandler_SetMacroHSZCI(AHSZCI);
{$ENDIF}
end;

function TDDEHandler.ReadDataMode
  : Boolean;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_GetDataMode
  else
{$ENDIF}
    Result := FALSE;
end;

procedure TDDEHandler.WriteDataMode
  (
  ADataMode: Boolean
  );
begin
{$IFNDEF WMLT}
  if ModuleOK then
    DDEHandler_SetDataMode(ADataMode);
{$ENDIF}
end;

function TDDEHandler.ReadFSending
  : Boolean;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_GetFSending
  else
{$ENDIF}
    Result := FALSE;
end;

procedure TDDEHandler.WriteFSending
  (
  AFSending: Boolean
  );
begin
{$IFNDEF WMLT}
  if ModuleOK then
    DDEHandler_SetFSending(AFSending);
{$ENDIF}
end;

function TDDEHandler.ReadDoOneSelect
  : Boolean;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_GetIniDoOneSelect
  else
{$ENDIF}
    Result := FALSE;
end;

procedure TDDEHandler.WriteDoOneSelect
  (
  ADoOneSelect: Boolean
  );
begin
{$IFNDEF WMLT}
  if ModuleOK then
    DDEHandler_SetIniDoOneSelect(ADoOneSelect);
{$ENDIF}
end;

function TDDEHandler.ReadFrameSelect
  : Boolean;
begin
{$IFNDEF WMLT}
  if ModuleOK then
    Result := DDEHandler_GetIniFrameSelect
  else
{$ENDIF}
    Result := FALSE;
end;

procedure TDDEHandler.WriteFrameSelect
  (
  ADoOneSelect: Boolean
  );
begin
{$IFNDEF WMLT}
  if ModuleOK then
    DDEHandler_SetIniFrameSelect(ADoOneSelect);
{$ENDIF}
end;

procedure TDDEHandler.SendSep;
begin
{
     if (ModuleOK) and (FSendSep) then begin
        FSendSep:=False;
        DDEHandler.SendString('[SEP_S][' + IniFile^.DDESeparateSign + ']');
     end;
}
end;

{$ENDIF} // <----------------- AXDLL

{ TLogFile }

procedure TLogFile.WriteLog(s: AnsiString);
begin
     {
     if LogForm = nil then begin
        LogForm:=TLogForm.Create(nil);
        LogForm.Show;
        Windows.SetParent(LogForm.Handle,0);
     end;
     LogForm.Memo.Lines.Add(s);
     }
end;

initialization

  LogFile := TLogFile.Create;
  LogFile.Logging := True;

finalization

  LogFile.Free;

end.

