{****************************************************************************}
{ Unit OmegaLayer                                                            }
{----------------------------------------------------------------------------}
{  Implements a layer object with a special ability                          }
{   to control insertion of objects - it can be only POINTs  !!!             }
{  Implements a point object with a special ability                          }
{  to display a big cross                                                    }
{----------------------------------------------------------------------------}
{ Date of creation:25-MAR-1999.                                              }
{ Full adaptation:                                                           }
{----------------------------------------------------------------------------}
{ Author: Sigolaeff Victor, PROGIS-Moscow, Russia, mailto: sygsky@progis.ru}
{----------------------------------------------------------------------------}
{ Changes:                                                                   }
{----------------------------------------------------------------------------}
{****************************************************************************}
unit OmegaLayer;

interface

uses Classes, Dialogs, Windows,
  AM_Layer, AM_Paint, AM_Index, AM_Def, AM_Point, AM_View;

type
  POmegaLayer = ^TOmegaLayer;
  TOmegaLayer = object(TLayer)
  private
    FItems: TList; // Collection of ProgisID !
    function GetObj(Ind: Integer): Integer;
    function GetObjIndexbyProgisId(ProgisId: Integer): Integer;
  public
    constructor Init(AOwner: PLayer);
    destructor Done; virtual;
    function InsertObject(PInfo: PPaint; Item: PIndex; Paint: Boolean): Boolean; virtual;
    function DeleteObj(PInfo: PPaint; Ind: Integer): Boolean;
    procedure InvalidatePoints(PInfo: PPaint; Indx: array of Integer);
    function DeleteIndex(PInfo: PPaint; AItem: PIndex): Boolean; virtual;
    procedure UpdateItem(Ind: Integer; Point: TDPoint; PInfo: PPaint);
    function ObjCounter: Integer;
    function SelectFromOmega(PInfo: PPaint; Ind: Integer): Boolean;
    function GetObjFromList(PInfo: PPaint; Ind: Integer): PView;
    procedure ShowAllObjects(PInfo: PPaint);
    procedure DeleteAllObjects(PInfo: PPaint);

    property Objects[Ind: Integer]: Integer read GetObj;
    property ObjIndexByProgisID[ProgisId: Integer]: Integer read GetObjIndexbyProgisId;
  end;

type
  POmegaPixel = ^TOmegaPixel;

  TOmegaPixel = object(TPixel)
//    constructor Init(DPos: TDPoint);
    procedure MoveRel(XMove, YMove: LongInt); virtual;
//    procedure Draw(PInfo: PPaint; Clipping: Boolean); virtual;
//    procedure CalculateClipRect;
//    procedure Invalidate(PInfo: PPaint; FromGraphic: Boolean); virtual;
  end;

implementation

uses OmegaHndl, AM_Proj, GrTools, Math, AM_Obj, Controls;

{ Local constants & variables }
const
  BaseLineLen = 17;
  BaseGapLen = 2;
{$J+}
  LineLen: Integer = BaseLineLen;
  LineGapLen: Integer = BaseGapLen;
  HalfLineLen: Integer = BaseLineLen div 2;
  SolidLineLen: Integer = BaseLineLen - (BaseGapLen div 2);
  LPoint: TPoint = (X: 1; Y: 1);

{ Local procedures & functions }

procedure ResetConst(PInfo: PPaint);
begin
  with LPoint do
  begin
        { Calculate the size of 1 pixel in logical units }
    DPToLP(PInfo.ExtCanvas.WindowDC, LPoint, 1);
    X := Abs(X);
    Y := Abs(Y);
    LineLen := LineLen * X;
    LineGapLen := 2 * X;
    HalfLineLen := LineLen div 2;
    SolidLineLen := HalfLineLen - (LineGapLen div 2);
  end;
end;

constructor TOmegaLayer.Init
  (
  AOwner: PLayer
  );
begin
  inherited;
  FItems := TList.Create;
end;

{
  Call this procedure from Omega window ONLY
}

function TOmegaLayer.DeleteObj(PInfo: PPaint; Ind: Integer): Boolean;
var
  SItem: PIndex;
  AItem: PIndex;
begin
  Result := False; { Default is false! }
  if Ind < FItems.Count then { Index in range! }
  begin
    SItem := New(PIndex, Init(0));
    SItem.Index := Objects[Ind]; { Emulate item }
    AItem := HasObject(SItem); { Find real one }
    DeSelect(PInfo, AItem); // Deselect just in case
    try
      if Self.DeleteIndex(PInfo, SItem) then {Not understand, may be it is deleted only in this layer ?}
      begin
        AItem := PProj(PInfo.AProj).Layers.IndexObject(PInfo, SItem); { Try to find main store??? }
        if AItem <> nil then { Found :-!}
        begin { Clear from screen }
          AItem^.Invalidate(PInfo);
          Result := PLayer(PInfo.Objects)^.DeleteIndex(PInfo, SItem);
          if Result then
            FItems.Delete(Ind);
        end;
      end;
    finally
      Dispose(SItem, Done);
      PInfo.RedrawInvalidate;
    end;
  end
end;

destructor TOmegaLayer.Done;
begin
  inherited;
  FItems.Destroy;
end;

{
  Function adds some functionality to normal WinGIS layer.
  Really from this event TOmegaLayer signals to Omega about insertion of
  an reference point.
}

function TOmegaLayer.InsertObject(PInfo: PPaint; Item: PIndex; Paint: Boolean): Boolean;
var
  Res: Integer;
  AIndex: LongInt;
begin
  if Item.GetObjType <> ot_Pixel then
  begin
    MessageDlg('You can insert only POINTS now!', mtInformation, [mbOk], 0);
    Result := False;
    Exit;
  end;

  Result := inherited InsertObject(PInfo, Item, True {Paint}); { Object is inserted ? }
  if Result then { Yes, it was inserted }
  begin
    AIndex := Item.Index;
    Res := FItems.IndexOf(Pointer(AIndex));
    if Res = -1 then // Totally new point
    begin // Add to Omega also
      Res := AddPoint2Omega(Item);
      if Res <> 0 then
        with FItems do
        begin
          if Count = 0 then
            ResetConst(PInfo);
          Expand;
          Add(Pointer(AIndex));
        end;
    end
    else // Point exists but was changed position
    begin
//???????
    end;

  end;
end;

{ Returns ProgisID by index in list }

function TOmegaLayer.GetObj(Ind: Integer): Integer;
begin
  if Ind < FItems.Count then
    Result := Integer(FItems.Items[Ind])
  else
    Result := 0;
end;

{
  Invalidate all the user wanted points in Omega Layer
}

procedure TOmegaLayer.InvalidatePoints(PInfo: PPaint; Indx: array of Integer);
var
  I: Integer;
  AItem, SItem: PIndex;
  AView: PView;
  ObjIndex: Integer;
begin
  SItem := New(PIndex, Init(0));
  try
    for I := 0 to High(Indx) do
    begin
      ObjIndex := Objects[Indx[I]];
      if ObjIndex <> 0 then
      begin
        SItem.Index := ObjIndex;
        AItem := HasObject(SItem);
        AView := PProj(PInfo^.AProj).Layers.IndexObject(PInfo, AItem);
        if AView <> nil then
          AView.Invalidate(PInfo, True);
      end;
    end;
  finally
    Dispose(SItem, Done);
  end;
end;

{
  Function adds some functionality to normal WinGIS layer.
  Really from this event TOmegaLayer signals to Omega about deletion of
  an reference point.
}

function TOmegaLayer.DeleteIndex(PInfo: PPaint; AItem: PIndex): Boolean;
var
  I: Integer;
begin
  I := FItems.IndexOf(Pointer(AItem.Index));
  if I <> -1 then // Object in our layer and in list
    if DeleteFromOmega(I) then // Delete it from Omega also
    begin
      FItems.Delete(I);
    end;
  DeleteIndex := inherited DeleteIndex(PInfo, AItem);
end;

procedure TOmegaLayer.UpdateItem(Ind: Integer; Point: TDPoint; PInfo: PPaint);
var
  DX, DY: Integer;
  AIndex: Integer;
  SItem, AItem: PIndex;
  AView: PView;
  Redraw: TDRect;
  AData: PProj;
begin

  AIndex := Objects[Ind]; // Get ProgisID
  if AIndex <> 0 then
  begin
    SItem := New(PIndex, Init(0)); // Create temp item with real ID
    try
      SItem.Index := AIndex;
      AItem := HasObject(SItem); // Object in Layer ?
      AData := PInfo.AProj;
      AView := AData.Layers.IndexObject(PInfo, AItem); // Get view of our point
      if AView <> nil then // Found !
        with PPixel(AView)^ do
        begin
          DX := Point.X - Position.X; // Define shift on X
          DY := Point.Y - Position.Y; //  and Y
          if (DX <> 0) or (DY <> 0) then // Point is moved by some value
          begin
            Redraw.Init; // Create rectagle for redraw operation
            try
              Redraw.CorrectByRect(AView^.ClipRect);
              MoveRel(DX, DY);
              AData.UpdateCliprect(AView);
              Redraw.CorrectByRect(AView^.ClipRect);
              if AData.Layers.SelLayer.HasObject(AView) <> nil then
                AData.SelAllRect.CorrectByRect(AView.ClipRect);
              if not AData.CorrectSize(Redraw, TRUE) then
                AData^.PInfo.RedrawRect(Redraw);
            finally
              Redraw.Done;
            end;
          end;
        end;
    finally
      Dispose(SItem, Done);
    end;
  end;
end;

function TOmegaLayer.ObjCounter: Integer;
begin
  Result := FItems.Count;
end;

function TOmegaLayer.GetObjIndexbyProgisId(ProgisId: Integer): Integer;
begin
  Result := FItems.IndexOf(Pointer(ProgisId));
end;

function TOmegaLayer.SelectFromOmega(PInfo: PPaint; Ind: Integer): Boolean;
var
  AView: PView;
begin
  AView := GetObjFromList(PInfo, Ind);
  if AView <> nil then
  begin
    Select(PInfo, AView);
    Result := True;
  end
  else
    Result := False;
end;

function TOmegaLayer.GetObjFromList(PInfo: PPaint; Ind: Integer): PView;
var
  AIndex: Integer;
  SItem, AItem: PIndex;
  AView: PView;
  AData: PProj;
begin
  Result := nil;
  if (Ind < 0) or (Ind >= ObjCounter) then Exit;
  AIndex := Objects[Ind]; // Get ProgisID
  if AIndex <> 0 then
  begin
    SItem := New(PIndex, Init(0)); // Create temp item with real ID
    try
      SItem.Index := AIndex;
      AItem := HasObject(SItem); // Object in Layer ?
      if AItem <> nil then
      begin
        AData := PInfo.AProj;
        AView := AData.Layers.IndexObject(PInfo, AItem); // Get view of our point
        Result := AView;
      end;
    finally
      Dispose(SItem, Done);
    end;
  end;
end;

{ TOmegaPixel }

{$ifdef FUTURE}

constructor TOmegaPixel.Init(DPos: TDPoint);
begin
  inherited;
  CalculateClipRect;
end;

{ Re-define function to mirror Omega point behaviour }

procedure TOmegaPixel.CalculateClipRect;
begin
  with Position do
    ClipRect.Assign(X - LineLen, Y - LineLen, X + LineLen, Y + LineLen);
end;

{ Changedrawing image of the Omega point }

procedure TOmegaPixel.Draw(PInfo: PPaint; Clipping: Boolean);
var
  APoint: TPoint;
begin
  PInfo^.ConvertToDisp(Position, APoint);
  with PInfo^.ExtCanvas do
  begin
    Push;
    try
      Pen.Style := lt_Solid;
      { Draw horizontal vertices and a point in centre of a cross }

      MoveTo(APoint.X - HalfLineLen, APoint.Y);
      LineTo(APoint.X - LineGapLen, APoint.Y);
      MoveTo(APoint.X, APoint.Y);
      LineTo(APoint.X + LPoint.X, APoint.Y);
      MoveTo(APoint.X + LineGapLen + LPoint.X, APoint.Y);
      LineTo(APoint.X + HalfLineLen + LPoint.X, APoint.Y);

      { Draw vertical vertices of a cross }

      MoveTo(APoint.X, APoint.Y - HalfLineLen);
      LineTo(APoint.X, APoint.Y - LineGapLen);
      MoveTo(APoint.X, APoint.Y + LineGapLen + LPoint.Y);
      LineTo(APoint.X, APoint.Y + HalfLineLen + LPoint.Y);
    finally
      Pop;
    end;
  end;
end;

procedure TOmegaPixel.Invalidate(PInfo: PPaint; FromGraphic: Boolean);
var
  Disp: TPoint;
  DispRect: TRect;
begin
  PInfo.ConvertToDisp(Position, Disp);
  LPToDP(PInfo^.ExtCanvas.Handle, Disp, 1);
  DispRect.Left := Disp.X - HalfLineLen;
  DispRect.Top := Disp.Y - HalfLineLen;
  DispRect.Right := Disp.X + HalfLineLen;
  DispRect.Bottom := Disp.Y + HalfLineLen;
  InvalidateRect(PInfo.HWindow, @DispRect, TRUE);
  if FromGraphic then PInfo.FromGraphic := TRUE;
end;
{$endif}

procedure TOmegaPixel.MoveRel(XMove, YMove: LongInt);
begin
  ChangeToOmega(Self.Index, XMove, YMove); // Try to change in Omega
  inherited;
end;

procedure TOmegaLayer.ShowAllObjects(PInfo: PPaint);
var
  ComRect: TDRect;
  SItem, AItem: PIndex;
  I: Integer;
begin
  if ObjCounter <= 0 then
    Exit;
  { Initiate bound rectangle for all objects }
  ComRect.Init;
  SItem := New(PIndex, Init(0)); // Create temp item with real ID
  for I := 0 to ObjCounter - 1 do
  begin
    SItem.Index := Objects[I];
    AItem := HasObject(SItem); // Object in Layer ?
    if AItem <> nil then
      ComRect.CorrectByRect(AItem.ClipRect);
  end;
  if not ComRect.IsEmpty then
    with ComRect do
    begin
      PProj(PInfo^.AProj).ZoomByRect(RotRect(A.X, A.Y, XSize, YSize, 0),
        Round(Max(XSize, YSize)) div 10, TRUE);
    end;
end;

procedure TOmegaLayer.DeleteAllObjects(PInfo: PPaint);
var
  SView: PView;
  SItem: PIndex;
  AProj: PProj;
  I: Integer;

  { Local procedure to delete all the traces of input object... Urrrrrr }

  procedure DelObjects(Item: PIndex);
  begin
    SItem.Index := Item.Index;
    SView := IndexObject(PInfo, SItem);
    SView.Invalidate(PInfo);
    AProj.MultiMedia.DelMediasAtDel(Item.Index);
    DeleteIndex(PInfo, Item);
    if AProj.Layers.ExistsObject(SItem) = 0 then
      PObjects(PInfo.Objects).DeleteIndex(PInfo, SItem);
  end;

begin
  if Data.GetCount > 0 then
  try
    AProj := PInfo.AProj;
    AProj.DeSelectAll(TRUE);
    AProj.SetStatusText(GetLangText(4011), TRUE);
    AProj.SetCursor(crHourGlass);
    SItem := New(PIndex, Init(0));
    while Data^.GetCount > 0 do
      DelObjects(Data^.At(0));
  finally
    PInfo.RedrawInvalidate;
    Dispose(SItem, Done);
    AProj.ClearStatusText;
    AProj.SetCursor(crDefault);
  end;
  for I := 0 to ObjCounter do
    DeleteFromOmega(I);
  FItems.Clear;
end;

initialization

end.

