Unit OptHndl;

Interface

Uses MenuHndl,ProjHndl;
                                              
Type TProjectOptionsHandler = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;

     TOptionsHandler = Class(TMenuHandler)
       Procedure   OnStart; override;
     end;

     TOrganizeHandler = Class(TProjMenuHandler)     
       Procedure   OnStart; override;
     end;

Implementation

Uses AM_Child,AM_Def,AM_Ini,AM_Main,AM_Obj,AM_Proj,Controls
     {$IFNDEF AXDLL} // <----------------- AXDLL
     ,OptDlg,ProjODlg,SelectOptionsDlg,UserIntf,FontStyleDlg
       {$IFNDEF WMLT}
     ,AM_DDE,OrganizeDlg,DatabaseOptionsDlg,VariousOptionsDlg,BitmapOptionsDlg
       {$ENDIF}
     {$ENDIF} // <----------------- AXDLL
     ,AM_Font;

{==============================================================================+
  TProjectOptionsHandler
+==============================================================================}

Procedure TProjectOptionsHandler.OnStart;
{$IFNDEF WMLT}
  {$IFNDEF AXDLL} // <----------------- AXDLL
  var Dialog            : TProjectOptionsDialog;
      SelectDialog      : TSelectOptionsDialog;
      VariousDialog     : TVariousOptionsDialog;
      DatabaseDialog    : TDatabaseOptionsDialog;
      FontDialog        : TFontStyleDialog;
      BitmapDialog      : TBitmapOptionsDialog;
  {$ENDIF} // <----------------- AXDLL
{$ENDIF}
  begin
{$IFNDEF WMLT}
  {$IFNDEF AXDLL} // <----------------- AXDLL
    Dialog:=TProjectOptionsDialog.Create(Parent);
    try
      UserInterface.BeginWaitCursor;
      try
        // setup the dialog and all sub-dialogs
        Dialog.Project:=Project;
        SelectDialog:=TSelectOptionsDialog.Create(Dialog);
        SelectDialog.SelectionSettings:=@Project^.PInfo^.SelectionSettings;
        SelectDialog.AllowExtendedStyles:=FALSE;
        Dialog.AddDialog(SelectDialog,SelectDialog.ControlPanel1,SelectDialog.Caption);

        VariousDialog:=TVariousOptionsDialog.Create(Dialog);
        VariousDialog.VariousSettings:=@Project^.PInfo^.VariousSettings;
        Dialog.AddDialog(VariousDialog,VariousDialog.ControlPanel1,VariousDialog.Caption);

//        VariousDialog.ShowForGlobal:=true;

        DatabaseDialog:=TDatabaseOptionsDialog.Create(Dialog);
        DatabaseDialog.DDEApplications:=DDEHandler.DDEApps;
        DatabaseDialog.DDEMLApplications:=DDEHandler.DDEMLApps;
{++ IDB}
        DatabaseDialog.Project := Project;
{-- IDB}
        //Dialog.AddDialog(DatabaseDialog,DatabaseDialog.ControlPanel1,DatabaseDialog.Caption);

        BitmapDialog:=TBitmapOptionsDialog.Create(Dialog);
        BitmapDialog.BitmapSettings:=@Project^.PInfo^.BitmapSettings;
        Dialog.AddDialog(BitmapDialog,BitmapDialog.ControlPanel1,BitmapDialog.Caption);

        FontDialog:=TFontStyleDialog.Create(Dialog);
        FontDialog.GlobFontSettings:=@IniFile^.FontSettings;
        FontDialog.ProjFontSettings:=@Project^.PInfo^.FontSettings;
        Dialog.AddDialog(FontDialog,FontDialog.ControlPanel1,FontDialog.Caption);

        //ThreeDDialog:=TThreeDOptionsDialog.Create(Dialog);
        //ThreeDDialog.ThreeDSettings:=@Project^.PInfo^.ThreeDSettings;
        //Dialog.AddDialog(ThreeDDialog,ThreeDDialog.ControlPanel1,ThreeDDialog.Caption);

      finally
        UserInterface.EndWaitCursor;
      end;
      // show the dialog
      if Dialog.ShowModal=mrOK then begin
{++ Ivanoff BUG#1001FG BUILD#106}
        If NOT Project.PInfo.FontSettings.UseGlobals Then Project.PInfo.AnnotSettings.AnnotFont := Project.ActualFont;
{-- Ivanoff}
        Project^.OnGlobalOptionsChanged;
        UserInterface.Update([uiStatusBar]);
        Project^.SetModified;
      end;
    finally
      Dialog.Free;
    end;
  {$ENDIF} // <----------------- AXDLL
{$ENDIF}
  end;

{==============================================================================+
  TOptionsHandler
+==============================================================================}

Procedure TOptionsHandler.OnStart;
{$IFNDEF WMLT}
  {$IFNDEF AXDLL} // <----------------- AXDLL
  var Dialog            : TOptionsDialog;
      SelectDialog      : TSelectOptionsDialog;
      VariousDialog     : TVariousOptionsDialog;
      DatabaseDialog    : TDatabaseOptionsDialog;
      BitmapDialog      : TBitmapOptionsDialog;
      FontDialog        : TFontStyleDialog;
  {$ENDIF} // <----------------- AXDLL
{$ENDIF}
  begin
{$IFNDEF WMLT}
  {$IFNDEF AXDLL} // <----------------- AXDLL
    Dialog:=TOptionsDialog.Create(Parent);
    try
      try
        SelectDialog:=TSelectOptionsDialog.Create(Dialog);
        IniFile^.SelectionSettings.DBSelectionMode:=IniFile^.DBSelMode;
        SelectDialog.SelectionSettings:=@IniFile^.SelectionSettings;
        SelectDialog.AllowExtendedStyles:=FALSE;
        Dialog.AddDialog(SelectDialog,SelectDialog.ControlPanel2,SelectDialog.Caption);

        VariousDialog:=TVariousOptionsDialog.Create(Dialog);
        VariousDialog.VariousSettings:=@IniFile^.VariousSettings;
        Dialog.AddDialog(VariousDialog,VariousDialog.ControlPanel2,VariousDialog.Caption);

        VariousDialog.ShowForGlobal:=true;

        DatabaseDialog:=TDatabaseOptionsDialog.Create(Dialog);
        DatabaseDialog.DDEApplications:=DDEHandler.DDEApps;
        DatabaseDialog.DDEMLApplications:=DDEHandler.DDEMLApps;
        Dialog.AddDialog(DatabaseDialog,DatabaseDialog.ControlPanel,DatabaseDialog.Caption);

        BitmapDialog:=TBitmapOptionsDialog.Create(Dialog);
        BitmapDialog.BitmapSettings:=@IniFile^.BitmapSettings;
        Dialog.AddDialog(BitmapDialog,BitmapDialog.ControlPanel,BitmapDialog.Caption);

        FontDialog:=TFontStyleDialog.Create(Dialog);
        FontDialog.GlobFontSettings:=@IniFile^.FontSettings;
        FontDialog.ProjFontSettings:=NIL;
        Dialog.AddDialog(FontDialog,FontDialog.ControlPanel,FontDialog.Caption);

        //ThreeDDialog:=TThreeDOptionsDialog.Create(Dialog);
        //ThreeDDialog.ThreeDSettings:=@IniFile^.ThreeDSettings;
        //Dialog.AddDialog(ThreeDDialog,ThreeDDialog.ControlPanel2,ThreeDDialog.Caption);

        if Dialog.ShowModal=mrOK then begin
           UserInterface.BeginWaitCursor;
           IniFile^.SaveOptions;
           IniFile^.DBSelMode:=SelectDialog.SelectionSettings^.DBSelectionMode;
           IniFile^.WriteDBSelOpt(IniFile^.DBSelMode);
           //IniFile^.WriteDDEDDEMLSettings(DDEHandler.DDEApps,DDEHandler.DDEMLApps);
           //DDEHandler.UnActivateConnections;
           {$IFNDEF WMLT}
           //SetDBApplication;
           {$ENDIF}
           WinGISMainForm.OnGlobalOptionsChanged;
        end;
      finally
        UserInterface.EndWaitCursor;
      end;

    finally
      Dialog.Free;
    end;
  {$ENDIF} // <----------------- AXDLL
{$ENDIF}
  end;

{==============================================================================+
  TOrganizeHandler
+==============================================================================}

Procedure TOrganizeHandler.OnStart;
{$IFNDEF WMLT}
  {$IFNDEF AXDLL} // <----------------- AXDLL
var Dialog         : TOrganizeDialog;
  {$ENDIF} // <----------------- AXDLL
{$ENDIF}
begin
{$IFNDEF WMLT}
  {$IFNDEF AXDLL} // <----------------- AXDLL
  Dialog:=TOrganizeDialog.Create(Parent);
  try
    if Project<>NIL then begin
      Dialog.XLineStyles:=Project^.PInfo^.ProjStyles.XLineStyles;
      Dialog.XFillStyles:=Project^.PInfo^.ProjStyles.XFillStyles;
      Dialog.Symbols:=PSymbols(Project^.PInfo^.Symbols);
      Dialog.Fonts:=Project^.PInfo^.Fonts;
    end;
    if Dialog.ShowModal=mrOK then UserInterface.Update([uiSymbols],TRUE);
  finally
    Dialog.Free;
  end;
  {$ENDIF} // <----------------- AXDLL
{$ENDIF}
end;

{==============================================================================+
  Initialisierungs- und Terminierungscode
+==============================================================================}

Initialization
  TProjectOptionsHandler.Registrate('ExtrasProjectOptions');
  TOptionsHandler.Registrate('ExtrasOptions');
  TOrganizeHandler.Registrate('ExtrasOrganize');

end.
