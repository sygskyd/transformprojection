Unit LinkHndl;

Interface
{$IFNDEF AXDLL} // <----------------- AXDLL
Uses ProjHndl;

Type TEditLinksHandler       = Class(TProjMenuHandler)
      Public
       Procedure   OnStart; override;
     end;
{$ENDIF} // <----------------- AXDLL
Implementation
{$IFNDEF AXDLL} // <----------------- AXDLL
{$R *.mfn}

Uses Controls,LinkDlg,MenuFn;

Procedure TEditLinksHandler.OnStart;
  var Dialog       : TLinkDialog;
  begin
    Dialog:=TLinkDialog.Create(Parent);
    try
     {++Brovak Bug526}
      Dialog.Project:=Project;
      Dialog.CallFromMenu:=true;
     {--Brovak}
     
      Dialog.Project:=Project;
      if Dialog.ShowModal=mrOK then begin
         Project^.PInfo^.RedrawScreen(False);
      end;
    finally
      Dialog.Free;
    end;
  end;

{===============================================================================
| Initialisierungs- und Terminierungscode
+==============================================================================}

Initialization;
  begin
    MenuFunctions.RegisterFromResource(HInstance,'LinkHndl','LinkHndl');
    TEditLinksHandler.Registrate('EditLinks');
  end;
{$ENDIF} // <----------------- AXDLL
end.
