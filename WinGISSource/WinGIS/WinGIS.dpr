program WinGIS;

{$IFOPT B+}
Fehler! Boolsche Auswertung darf nicht auf vollst�ndig eingestellt werden!
{$ENDIF}
{$IFOPT V+}
Fehler! Strenge Pr�fung von var
  Strings darf nicht aktiviert sein!
{$ENDIF}
{$IFOPT Q+}
  Fehler! Overflowpr�fung darf nicht aktiviert sein!
{$ENDIF}
{$IFOPT P+}
  Fehler! Offene Arraygrenzen darf nicht aktiviert sein!
{$ENDIF}

uses
  ShareMem,
  Forms,
  SysUtils,
  AM_Main in 'AM_Main.pas',
  AM_Res,
  AM_Def,
  LicenseHndl,
  WxCore,
  Windows,
  WinOSInfo,
  ShellApi,
  Wingis_TLB in 'Wingis_TLB.pas',
  GeoDlg in '..\..\ImpExp\AXGisPro\GeoDlg.pas' {GeoWnd};

{$R *.RES}
{$R *.TLB}

function IsStartedWithUpdate: Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := 1 to ParamCount do
  begin
    if Lowercase(ParamStr(i)) = STARTPARAM_UPDATE then
    begin
      Result := True;
      exit;
    end;
  end;
end;

function IsStartedAsBingMapsAsyncMode: Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := 1 to ParamCount do
  begin
    if Lowercase(ParamStr(i)) = STARTPARAM_BMASYNC then
    begin
      Result := True;
      exit;
    end;
  end;
end;

function IsStartedAsRegServer: Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := 1 to ParamCount do
  begin
    if Lowercase(ParamStr(i)) = STARTPARAM_REG then
    begin
      Result := True;
      WXCore.DoRegister := True;
      exit;
    end;
  end;
end;

function IsStartedAsUnRegServer: Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := 1 to ParamCount do
  begin
    if Lowercase(ParamStr(i)) = STARTPARAM_UNREG then
    begin
      Result := True;
      WXCore.DoUnRegister := True;
      exit;
    end;
  end;
end;

function IsStartedAsCanvas: Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := 1 to ParamCount do
  begin
    if Lowercase(ParamStr(i)) = STARTPARAM_CANVAS then
    begin
      Result := True;
      exit;
    end;
  end;
end;

procedure CheckIfLicenseChanged;
var
  s: AnsiString;
begin
  s := OSInfo.CommonAppDataDir + 'activated';
  if WinGISLicense >= WG_STD then
  begin
    if not FileExists(s) then
    begin
      FileClose(FileCreate(s));
    end;
  end
  else
  begin
    if FileExists(s) then
    begin
{$IFNDEF AXDLL} // <----------------- AXDLL
      AddLicense;
{$ENDIF}
    end;
  end;
end;

begin
  if OSInfo.OSVersion < 5.0 then
  begin
    MessageBox(0, 'WinGIS 2010 was developed for a newer version of Windows!', 'WinGIS 2010', MB_ICONERROR or MB_APPLMODAL);
    exit;
  end;

  if IsStartedAsRegServer or IsStartedAsUnRegServer then
  begin
    TCore.Create;
    exit;
  end;

  RunUpdateSilent := IsStartedWithUpdate;
  BingMapsAsyncMode := IsStartedAsBingMapsAsyncMode;
  RunAsCanvas := IsStartedAsCanvas;

  if RunAsCanvas then
  begin
    WingisLicense := WG_PROF;
  end
  else
  begin
{$IFNDEF AXDLL} // <----------------- AXDLL
    WingisLicense := GetWingisLicense;
    CheckIfLicenseChanged;
{$ENDIF}
  end;

  CheckLanguageEntries(False);

  RegisterWinGISClass;
  Application.Initialize;
  Application.CreateForm(TWinGISMainForm, WinGISMainForm);
  Application.CreateForm(TGeoWnd, GeoWnd);
  Application.Run;
  DoneClass;

  if Trim(UpdateLink) <> '' then
  begin
    ShellExecute(0, 'open', PChar(UpdateLink), nil, nil, SW_SHOW);
  end;
end.

