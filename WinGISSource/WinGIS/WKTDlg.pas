unit WKTDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, MultiLng, Buttons;

type
  TWKTForm = class(TForm)
    GroupBoxText: TGroupBox;
    GroupBoxButtons: TGroupBox;
    ButtonClose: TButton;
    ButtonCreate: TButton;
    Memo: TMemo;
    MlgSection: TMlgSection;
    ButtonOpen: TSpeedButton;
    ButtonSave: TSpeedButton;
    OpenDialog: TOpenDialog;
    SaveDialog: TSaveDialog;
    procedure MemoChange(Sender: TObject);
    procedure MemoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ButtonOpenClick(Sender: TObject);
    procedure ButtonSaveClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
  public
  end;

var
  WKTForm: TWKTForm;

implementation

{$R *.DFM}

procedure TWKTForm.MemoChange(Sender: TObject);
begin
     if Visible then begin
        ButtonCreate.Enabled:=Trim(Memo.Lines.Text) <> '';
     end;
     ButtonSave.Enabled:=Trim(Memo.Lines.Text) <> '';
end;

procedure TWKTForm.MemoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
    if ((Chr(Key) = 'a') or (Chr (Key) = 'A')) and (ssCtrl in Shift) then begin
        Memo.SelectAll;
     end;
end;

procedure TWKTForm.ButtonOpenClick(Sender: TObject);
begin
     if OpenDialog.Execute then begin
        Memo.Lines.LoadFromFile(OpenDialog.FileName);
     end;
end;

procedure TWKTForm.ButtonSaveClick(Sender: TObject);
begin
     if SaveDialog.Execute then begin
        Memo.Lines.SaveToFile(SaveDialog.FileName);
     end;
end;

procedure TWKTForm.FormCreate(Sender: TObject);
begin
     //Scaleby(Trunc(9600000/(PixelsPerInch)),100000);
end;

end.
