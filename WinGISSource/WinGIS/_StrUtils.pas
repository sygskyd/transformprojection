{******************************************************************************
Function:   Collection of functions for string parsing

Autor:      Schupp Alexander (aschupp)
Datum       19.5.2000

Tested:     not fully tested yet

References: SysUtils

History:
Version 0.1, 19.5.2000:  added the first two functions
                         GetSubString, GetSubStringCount

******************************************************************************}

unit StrUtils;

interface

uses SysUtils;

const
     //Error Codes used in this Unit
  ERR_SUCCESS = 0; // no error

function GetSubString(sCmd: string; iPos: integer; sSep: string): string;
function GetSubStringCount(sCmd: string; sSep: string): integer;

implementation

//  ************************************************************************
//  FUNCTION:  GetSubString
//  PARAMETER: sCmd.....string containing the full string to be parsed
//             iPos.....position of the substring to extract
//             sSep.....string seperator
//  RETURN:    a string containing the substring at iPos
//  BEISPIEL:  GetSubString("abc/123/def/456", 1, '/') returns "abc"
//  ************************************************************************

function GetSubString(sCmd: string; iPos: integer; sSep: string): string;
var
  iIndx: integer;
  iCount: integer;
  iNextIndx: integer;
  sSubStr: string;
begin

  try
    if iPos > GetSubStringCount(sCmd, sSep) then
      Result := ''
    else
    begin

      Result := '';

      iIndx := 1;
      iCount := 1;
      iNextIndx := -1;
     // loop through till iIndx points to the beginning of the iPos substring
      while (iNextIndx <> 0) and (iCount < iPos) do
      begin
        INC(iCount);
        iNextIndx := Pos(sSep, Copy(sCmd, iIndx, Length(sCmd) - iIndx + 1));
        iIndx := iIndx + iNextIndx;
      end;

     // found the substring beginning ?
      if (iNextIndx > 0) or (iCount = iPos) then
      begin

        sSubStr := Copy(sCmd, iIndx, Length(sCmd) - iIndx + 1);
        iIndx := Pos(sSep, sSubStr);
        if iIndx > 0 then
          sSubStr := Copy(sSubStr, 1, iIndx - 1);

        Result := sSubStr;
      end;

    end;

  except
    on exception do
      Result := '';
  end;
end;

//  ************************************************************************
//  FUNCTION:  GetSubStringCount
//  PARAMETER: sCmd...string
//             sSep...Seperator
//  RETURN:    number of substrings found in sCmd, seperated by sSep
//  EXAMPLE:   GetSubStringCount("abc/123/def/456",'/') returns 4
//             GetSubStringCount("abcX123XdefX456",'/') returns 1  !
//  ************************************************************************

function GetSubStringCount(sCmd: string; sSep: string): integer;
var
  iCount: integer;
  iNextPos: integer;
  iPos: integer;
begin

  try
    iCount := 0;
    iPos := 1;
    iNextPos := 0;
    repeat
      INC(iCount);
      iNextPos := Pos(sSep, Copy(sCmd, iPos, Length(sCmd) - iPos + 1));
      iPos := iPos + iNextPos;
    until iNextPos = 0;
    Result := iCount;

  except
    on exception do
      Result := 0;
  end;
end;

end.

