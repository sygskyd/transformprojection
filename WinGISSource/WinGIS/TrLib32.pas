{****************************************************************************}
{ Unit TrLib32                                                               }
{----------------------------------------------------------------------------}
{ Interface to handle images without TrLib32.DLL                             }
{----------------------------------------------------------------------------}
{ Date of creation:13-NOV-1999.                                              }
{ Full adaptation: 13-DEC-1999.                                              }
{----------------------------------------------------------------------------}
{ Author: Sigolaeff Victor, PROGIS-Moscow, Russia, mailto: sygsky@progis.ru  }
{----------------------------------------------------------------------------}
{ Changes:                                                                   }
{ #1:21-FEB-2000 by Sygsky - as AXDLL got exception on projects with BMPs    }
{   due to  absent of ImgMan32.dll on the path[es]                           }
{   now it also will search in DLL directory after standard ones.            }
{                                                                            }
{ #2:27-APR-2000 by Sygsky - now imgman32.dll is loaded only if requested but}
{   not at WinGIS startup.                                                   }
{ #3:10-AUG-2000 by Sygsky - little debugging of BitMapInfoSize procedure in }
{   detecting biCompression state properly.                                  }
{----------------------------------------------------------------------------}
{ It uses the same interface as previous TrLib32 unit with purpose to        }
{ simplify a shift to a new functionality.                                   }
{ Now it uses ImgMan32.DLL except of old ImgMan31.DLL                        }
{****************************************************************************}
unit TrLib32;

interface

uses WinTypes, WinProcs, Graphics, Classes;

const
  ImgManName        = 'ImgMan32.DLL';

const
  ec_IOERROR        : Integer = 1;      {File is not Win3.1/9X/NT Image file!}
  ec_NOMEM          : Integer = 2;      {Error on preparing image !}
  ec_WRONGMODCOD    : Integer = 3;      {The module code in bitmap % is not correct!}
  ec_UTILERROR      : Integer = 4;      {The module file ImgMan31.Dll could not be found. The image will not be loaded!}
  ec_NOSUPPORT      : Integer = 5;      {The Bitmap is not Windows 3.0 bitmap format, or does not contain the correct module number!}
  ec_INVALIDPARAM   : Integer = 6;      {The parameter is not correct, bitmap cannot be loaded!}
  ec_PROC_ERR       : Integer = 7;      {An error occurred in a DLL which is used from TrLib32.dll. Maybe the error is in the ImgMan.Dll. This was the reason why the picture was not loaded!}
  ec_PRINT_ERR      : Integer = 8;
  ec_BAD_PRN        : Integer = 9;
  ec_BAD_TYPE       : Integer = 10;
  ec_NO_DILS        : Integer = 11;     {No Img31%.DIL file was found}
  ec_NO_DELS        : Integer = 11;     {No Img31X%.DEL file was found}

  // Image flags
const
  IMG_RENDER_SELF   : Integer = 1;
  IMG_PRINT_SELF    : Integer = 2;
  IMG_PRNT_VECTOR   : Integer = 4;
  IMG_DISP_VECTOR   : Integer = 8;      // 1 if image will display in vector format
  IMG_MEM_BASED     : Integer = 16;     // 1 if image not read from Disk
  IMG_SCALE_SELF    : Integer = 32;     //++Sygsky: 1 if image can scale itself (ECW/PRE now)

  // Raster-Op defines
const
  IMG_TRANSPARENT   : UINT = ($FFFFFFFF);

  // Color Reduction Defines
  IMG_BAYER         : Integer = 1;
  IMG_BURKES        : Integer = 2;
  IMG_FLOYD         : Integer = 4;
  IMG_GRAYSCALE     : Integer = 8;
  IMG_FIXEDPALETTE  : Integer = 16;
  IMG_OCTREEPALETTE : Integer = 32;
  IMG_MEDIANCUT     : Integer = 64;

  //
  // ImgCopy flags (for B&W images)
const
  COPY_DEL          : Integer = 0;      // pick out individual pixels (no AND or OR)
  COPY_AND          : Integer = $01;    // preserve black pixels
  COPY_OR           : Integer = $02;    // preserve white pixels
  COPY_INTERPOLATE  : Integer = $04;    // interpoloate between pixels
  COPY_ANTIALIAS    : Integer = $10;    // supersample to produce 16 level grayscale
  COPY_ANTIALIAS256 : Integer = $20;    // produce 256 level grayscale

  // ImgXWriteDIB defines
const
  IMGXOPT_FILE_PROMPT: Integer = $0001;
  IMGXOPT_OVERWRITE : Integer = $0002;
  IMGXOPT_COMPRESS  : Integer = $0004;
  IMGXOPT_OPT_PROMPT: Integer = $0008;
  IMGXOPT_OVERWRITE_PROMPT: Integer = $00010;
  IMGXOPT_SAVE_PROMPT: Integer = $00020;

type
  LPVOID = Pointer;
  ImgManStatus = function(hImg: THandle; nPercent: Integer; dwUserInfo: DWORD): Integer; stdcall;

  PUserStatData = ^TUserStatData;
  TUserStatData = record
    UserStatusProc: ImgManStatus;
    UserSteps: DWORD;
    UserData: DWORD;
  end;

  LoadImgProc = function(sSrcSize: TRECT; sDestSize: TPOINT; uFactor: LongInt;
    lpFileName: LPSTR; uID: LongInt; psDestRect: PRECT; iPage: LongInt): LPVOID; cdecl;
  UnRegisterProc = procedure(ID: LongInt); cdecl;
  RegisterProc = function(ModuleCode, SerialNr, ModuleName: LPSTR): LongInt; cdecl;

  ImgGetExt = function: PAnsiChar; stdcall;
  ImgXGetExt = function(Ext: PAnsiChar): Integer; stdcall;
  ImgErrString = function(lpText: PAnsiChar; iSize: Integer): Integer; stdcall;
  ImgGetStatus = function: Integer; stdcall;
  ImgCopy = function(hImage: THandle; nWid, nHi: Integer; lpSrc: PRect; lFlags: Integer): THandle; stdcall;
  ImgGetInfo = function(hImage: LongInt; var Flags: Integer): PBitMapInfo; stdcall;
  ImgGetInfoStruct = function(hImage: THandle; lpDIB: PBitMapInfo; var lpFlags: Integer): Integer; stdcall;
  ImgOpenSolo = function(lpFileName: Pointer; Dummy: Pointer): Integer; stdcall;
  ImgClose = function(hImage: THandle): Integer; stdcall;
  ImgSetPage = function(hImage: THandle; nPage: UINT): Integer; stdcall;
  ImgPageCount = function(hImage: THandle; var lpCnt: Integer): Integer; stdcall;
  ImgGetPage = function(hImage: THandle; var lpPage: Integer): Integer; stdcall;
  ImgRotate = function(hImage: THandle; nDegree: UINT; rgbCol: TColor): THandle; stdcall;
  ImgXWriteImage = function(lpFile, lpExt: PAnsiChar; hImage, hOptBlk, hWnd: THandle; lOpts: Integer): Integer; stdcall;
  ImgGetDIB = function(hImage: THandle; bNewDib: BOOL; lpRect: PRect): THandle; stdcall;
  ImgDrawImage = function(hImage: THandle; hDC: HDC; lpDestRect, lpSrcRect: pRect): Integer; stdcall;
  ImgPrintImage = function(hImage: THandle; hPrnDC: HDC; lpDestRect, lpSrcRect: PRect): Integer; stdcall;
  ImgReduceColorsPal = function(hImage: THandle; nFlags: Integer; hPal: HPALETTE): THandle; stdcall;
  ImgGetPalette = function(hImage: THandle): HPALETTE; stdcall;
  ImgXOptBlkGet = function(hOptBlk: THandle; lpszKey, lpszBuf: PChar): Integer; stdcall;
  ImgXOptBlkDel = function(hOptBlk: THandle): Integer; stdcall;
  ImgXOptBlkCreate = function(lpszInit: PChar): THandle; stdcall;
  ImgXOptBlkAdd = function(hOptBlk: THandle; lpszKey, lpszValue: PChar): Integer; stdcall;
  ImgSetDefaultStatusProc = procedure(lpfnStat: ImgManStatus; lCnt: UINT; dwUser: DWORD); stdcall;
  ImgSetStatusProc = procedure(hImage: THandle; lpfnStat: ImgManStatus; lCnt: UINT; dwUser: DWORD); stdcall;

  ImgLoad = function(hImage: THandle; lpRect: PRect): Integer; stdcall;
  ImgGetDataPtr = function(hImage: THandle): Pointer; stdcall;
  ImgGetDIBPtr = function(hImage: THandle): PBitMapInfo; stdcall;
  ImgGetHBitmap = function(hImage: THandle): Pointer; stdcall;
  ImgIncreaseColors = function(hImage: THandle; nBitDepth: Integer): THandle; stdcall;
  ImgReduceColors = function(hImage: THandle; nColors, nFlags: Integer): THandle; stdcall;
type
  TTrLib = class(TObject)
  private
    fImgManLib: THandle;
    FLoaded: Boolean;
    {++ Sygsky }
    fImgGetExt: ImgGetExt;
    fImgXGetExt: ImgXGetExt;
    fImgErrString: ImgErrString;
    fImgGetStatus: ImgGetStatus;
    fImgCopy: ImgCopy;
    fImgGetInfo: ImgGetInfo;
    fImgGetInfoStruct: ImgGetInfoStruct;
    fImgOpenSolo: ImgOpenSolo;
    fImgClose: ImgClose;
    fImgSetPage: ImgSetPage;
    fImgPageCount: ImgPageCount;
    fImgGetPage: ImgGetPage;
    fImgRotate: ImgRotate;
    fImgXWriteImage: ImgXWriteImage;
    fImgGetDIB: ImgGetDIB;
    fImgDrawImage: ImgDrawImage;
    fImgPrintImage: ImgPrintImage;
    fImgReduceColorsPal: ImgReduceColorsPal;
    fImgGetPalette: ImgGetPalette;
    fImgXOptBlkCreate: ImgXOptBlkCreate;
    fImgXOptBlkGet: ImgXOptBlkGet;
    fImgXOptBlkDel: ImgXOptBlkDel;
    fImgXOptBlkAdd: ImgXOptBlkAdd;
    fImgSetDefaultStatusProc: ImgSetDefaultStatusProc;
    fImgSetStatusProc: ImgSetStatusProc;
    fImgLoad: ImgLoad;
    fImgGetDataPtr: ImgGetDataPtr;
    fImgGetDIBPtr: ImgGetDIBPtr;
    fImgGetHBitmap: ImgGetHBitmap;
    fImgIncreaseColors: ImgIncreaseColors;
    fImgReduceColors: ImgReduceColors;

    {--Sygsky }
    LastError: LongInt;
    LoadCounter: Integer;
    procedure ChangePal(var hImg: THandle; var Color: COLORREF);
    function CanFilterImage(PInfo: PBITMAPINFO): Boolean; overload;
    function CanFilterImage(Image: THandle): Boolean; overload;

    function GetTryLoad: Boolean;
    procedure ProcessVMSI(FName: AnsiString; hImg: THandle; uFactor: UINT;
      SrcRect: PRect);

  public
    constructor Create;
    destructor Destroy; override;
    function Register(ModuleCode, SerialNr, ModuleName: LPSTR): LongInt;
    function ImgOpenEx(lpFileName: Pointer; Dummy: Pointer): Integer;
    procedure UnRegister(ID: LongInt);
    function LoadImg(sSrcSize: TRECT; sDestSize: TPOINT; uFactor: LongInt;
      lpFileName: LPSTR; uID: LongInt; psDestRect: PRECT; iPage: LongInt = 1): LPVOID;
    //    function GetHeader(lpFileName: LPSTR; uID: LongInt; iPage: LongInt = 1): PBITMAPINFO; overload;
    function GetHeader(lpFileName: LPSTR; var PageCount: Integer;
      var IsVMSI: Boolean; iPage: LongInt = 1): PBITMAPINFO;
    function EnumImpFormats: LPSTR;
    function GetImpStrings: AnsiString;
    function EnumExpFormats: LPSTR;
    {++ Sygsky }
    function ImageClose(hImage: THandle): Integer;
    function ImageCopy(hImage: THandle; nWid, nHi: Integer; lpSrc: PRect;
      CopyMode: Integer): THandle;
    function ImageOpenSolo(lpFileName, Dummy: PAnsiChar): Integer;
    function ImageGetImpFormats: AnsiString;
    function ImageGetExpFormats: AnsiString;
    function ImageGetInfo(hImage: THandle; lpImgInfo: PBitMapInfo): Boolean;
    function ImageGetInfoAddr(hImage: THandle): PBitMapInfo;
    function ImageGetInfoStruct(hImage: THandle; lpDIB: PBitMapInfo; var lpFlags: Integer): Integer;
    function ImageSetPage(hImage: THandle; nPage: UINT): Boolean;
    function ImagePageCount(hImage: THandle; var lpCnt: Integer): Integer;
    function ImageGetPage(hImage: THandle; var lpPage: Integer): Integer;
    function ImageGetDIB(hImage: THandle; bNewDIB: Boolean; lpRect: PRECT): THandle;
    {-- Sygsky}
    {       Function    ImgGetFileName(HWindow : HWnd) : LPSTR;}
    function Rotate(FileName, outFileName: LPSTR; Angle: double; ID: LongInt; Hwindow: Hwnd): LongInt;
    function Stretch(InFileName, outFileName: AnsiString; Width, Height: Integer): Boolean;
    function getLastError(ID: LongInt = -1): LongInt;
    function getErrorText(ID: Integer = -1): AnsiString;
    function GetImgFlags(sFileName: LPSTR; ID: LongInt = -1): LongInt;
    function DrawImage(sFileName: LPSTR; hDC: HDC; lpDestRect, lpSrcRect: PRECT;
      uLoadFactor: LongInt; dwROP: DWORD; ID: LongInt): LongInt;
    function DrawFullImage(sFileName: LPSTR; hDC: HDC; lpDestRect: PRECT): Boolean;

    function GetPageCount(sFileName: LPSTR; ID: LongInt = -1): LongInt;
    function PrintImage(sFileName: LPSTR; hDC: HDC; lpDestRect, lpSrcRect: PRECT;
      uLoadFactor: LongInt; dwROP: DWORD; ID: LongInt = 0): LongInt;
    procedure ImageSetStatusProc(hImage: THandle; StatusProc: ImgManStatus; PerBytes: DWORD; UserData: DWORD);
    //    procedure StoreVMSIStatusProc(StatusProc: ImgManStatus = nil; dwSteps: DWORD = 0; dwUserData: DWORD = 0);
    procedure ImageSetDefaultStatusProc(StatusProc: ImgManStatus; PerBytes: DWORD; UserData: DWORD);
    function DLLName: AnsiString;
    property Loaded: Boolean read GetTryLoad;
    property IsLoaded: Boolean read FLoaded;
    function ImageGetPalette(hImage: THandle): HPALETTE;
    function ImageIncreaseColors(hImage: THandle; nBitDepth: Integer): THandle;
    function ImageReduceColors(hImage: THandle; nColors, nFlags: Integer): THandle;
    function ImageReduceColorsPal(hImage: THandle; nFlags: Integer; hPal: HPALETTE): THandle;
  end;

function BitMapInfoSize(InfoPtr: PBitMapInfo): Integer;
function BitMapImageSize(InfoPtr: PBitMapInfo): Integer;
function BitMapColorsNum(InfoPtr: PBitMapInfo): Integer; // Glukhov PaletteConverion Build#149 26.12.00
function RectInImage(PInfo: PBitMapInfo; Rect: PRect): Boolean;
function PRectWidth(const Rect: PRect): Integer; Register;
function PRectHeight(const Rect: PRect): Integer; Register;
function RectsAreEqual(var Rect1, Rect2: TRect): Boolean;
function RectsSizeAreEqual(var Rect1, Rect2: TRect): Boolean; Register;
function RectsAreNotIntersected(const PRect1, PRect2: PRect): Boolean; register;
function RectsAreIntersected(const PRect1, PRect2: PRect): Boolean; Register;
function InRange(LowBound, Value, HighBound: Integer; Include: Boolean): Boolean;

function GetAvailPhysMem: DWORD;
function GetAvailPageFile: DWORD;
function GetAvailVirtMem: DWORD;
function GetTotalPhysMem: DWORD;

function FileIsImageOne(const AFileName: AnsiString): Boolean;
function SaveImageAs(SrcFile, DstFile: AnsiString): Boolean;
function SavePartialImageAs(SrcFile, DstFile: AnsiString; SrcRect: TRect): Boolean;
function ExtractFilterExt(const Filter: AnsiString; Index: Integer): AnsiString;
function GetFilterIndex(const Filter, Ext: AnsiString): Integer;
function IsExtInFilter(const Filter, Ext: AnsiString): Boolean;

function Image2VMSI(SrcName, DstName: AnsiString; PageNum: Integer = 1;
  DefaultCompress: Boolean = False; UserData: PUserStatData = nil): Boolean;
function FileIsVMSI(FileName: AnsiString): Boolean;
function FileIsVMSIPlus(FileName: AnsiString): Boolean;
function FileIsECW(FileName: AnsiString): Boolean;
function FileIsBMP(FileName: AnsiString; ptrBitsCount: PWORD; ptrCompression: PDWORD): Boolean;
function ImageManAvailable: Boolean;
function GetLastImageManError: Integer;
function LRectToDRect(const DC: HDC; LRect: PRect): TRect;
function BestCopyMode(const Info: PBitMapInfo): Integer; overload;
function BestCopyMode(const hImage: THandle): Integer; overload;
function BytesPerRow(RowLen, BitsPerPixel: DWORD): DWORD;
function HasGrayPalette(Info: PBitMapInfo): Boolean;

{brovaK}
procedure SetValidRect(var CheckedRect: TRect);
{brovak}

function IsVMSIImage(AFileName: AnsiString): Boolean;

const

  PRIExt            = '.pri';
  PREExt            = '.pre';
  BMPExt            = '.bmp';
  TIFExt            = '.tif';
  ECWExt            = '.ecw';

  UnknownErrText    = 'Unknown error code: ';
  CopyRight         = 'CopyRight @ by JHC 1987, 2002';
  JHCGreeting1      = 'Hello!';
  Artist            = 'Sygsky from JHC. Mailto:sygsky@yandex.ru';
  VMSI_MAGIC        = 2773846;          // 'VS'+ $0042 or $56 $53 $42 $00
  PRE_MAGIC         : array[0..5] of Char = ('V', 'S', '1', '0', '3', '0');
  ECW_MAGIC         = 613;              // '??' or $65 $02
  MinVMSISize       : Integer = 512 * 512 {1024 * 1025}; { Minimal size of image to reduce}
  VMSIDivider       : Integer = 4;      { One step scale factor on X and Y axes }

  IMG_ERR           = 0;
  IMG_OK            = 1;
  IMG_INV_FIL       = 2;
  IMG_FILE_ERR      = 3;
  IMG_NOFILE        = 4;
  IMG_NOMEM         = 6;
  IMG_INV_HAND      = 8;
  IMG_NSUPPORT      = 9;
  IMG_PROC_ERR      = 10;
  IMG_PRINT_ERR     = 11;
  IMG_BAD_PRN       = 12;
  IMG_BAD_SRC       = 13;
  IMG_BAD_TYPE      = 14;
  IMG_NO_DILS       = 15;
  IMG_INV_PAGE      = 16;
  IMG_NO_PROC       = 17;
  IMG_TOOBIG        = 18;
  IMG_PASTECLIP     = 19;
  IMG_BLITERR       = 20;
  IMG_DIBERR        = 21;
  IMG_METAFILEERR   = 22;
  IMG_CANCELLED     = 23;
  IMG_BADBITMAP     = 24;
  IMG_IVNFILTER     = 25;
  IMG_DLGCANCEL     = 26;
  IMG_BUFFTOOSMALL  = 27;
  IMG_DLGERR        = 28;

  IMGPI_UNAVAIL     = 40;
  IMGPI_ALREADYLOADED = 41;
  IMGPI_NOTLOADED   = 42;
  IMGPI_CANTUNLOAD  = 43;
  IMGPI_ERRORLOADINGPLUGIN = 44;
  IMGPI_NOPLUGINSELECTED = 45;
  IMGPI_PLUGINERROR = 46;
  IMGPI_ERRORENUMERATING = 47;
  IMGPI_NOMEM       = 48;

  IMGFX_INVALIDPARAM = 100;
  IMGFX_INVALIDIMAGE = 102;
  IMGFX_OUTOFMEMORY = 103;
  IMGFX_OUTOFRESOURCES = 104;

  IMGX_ERR          = 0;
  IMGX_OK           = 1;
  IMGX_NO_DELS      = 202;
  IMGX_INV_FILE     = 203;
  IMGX_FILE_ERR     = 204;
  IMGX_NOMEM        = 205;
  IMGX_INV_HAND     = 206;
  IMGX_NSUPPORT     = 207;
  IMGX_BAD_DEL      = 208;
  IMGX_DISK_FULL    = 209;
  IMGX_EXISTS       = 210;
  IMGX_NO_OPEN      = 211;
  IMGX_DELETE_ERR   = 212;
  IMGX_ABORTED      = 213;
  IMGX_BADDIB       = 214;
  IMGX_CANCELLED    = 215;

  IMG_OFN_ALLOWTHUMBOPT = 1;            //Specifies that the 'Preview' button be enabled
  IMG_OFN_SHOWTHUMB = 2;                // Specified that the preview thumbnail should be shown when the dialog is opened.
  IMG_OFN_ALLOWSTATSOPT = 4;            // Specifies that the 'Stats' button be enabled.
  IMG_OFN_SHOWSTATS = 8;                //Specifies that the image's information be displayed when the dialog is opened.
  IMG_OFN_FULL      = 15;               // Specifies all the above flags.

  {--------------------------- internal defines ---------------------------------}
  IMGMAN_ERROR_NUM  = 53;
  IMGMAN_MIN_ERROR_CODE = IMG_OK;
  IMGMAN_MAX_ERROR_CODE = IMGX_CANCELLED;

var
  TrLib             : TTrLib = nil;
  bNoImageLoading   : Boolean = False;

const
  JHCGreeting2      = 'from&';

implementation

uses SysUtils, Windows, Img_Info, Math, AM_Main
  {$IFDEF DEBUG}
  , Dialogs
  {$ENDIF}
  {$IFNDEF VMSI}
  , LicenseHndl
  {$ELSE}
  {$IFDEF VERBOSE}
  , Main
  {$ENDIF}

  {$ENDIF}
  ;

var
  MemoryStatus      : TMemoryStatus;
const
  JHCGreeting3      = 'JHC';


function IsVMSIImage(AFileName: AnsiString): Boolean;
var
AExt: String;
begin
     AExt:=ExtractFileExt(AFileName);
     AExt:=Uppercase(AExt);
     Result:=((AExt = '.PRI') or (AExt = '.PRE') or (AExt = 'PRL'));
end;

procedure GetMemStatus;
begin
  //  MemoryStatus.dwLength := SizeOf(MemoryStatus);
  GlobalMemoryStatus(MemoryStatus);     { Refresh memory info }
end;

{$IFDEF FUTURE }
{##################################################}

function PosAnyChar(var SubStr, Str: string): Integer;
var
  SubLen, I         : Integer;
begin
  Result := 0;
  SubLen := Length(SubStr);
  if SubLen = 0 then Exit;
  if Length(Str) = 0 then Exit;
  for I := 1 to SubLen do
  begin
    Result := Pos(SubStr[I], Str);
    if Result <> 0 then Exit;
  end;
end;
{##################################################}

function PosSubSet(var SubSet, SrcStr: string; var Start, Len: Integer): Boolean;
var
  I, K              : Integer;
begin
  Result := False;
  Start := PosAnyChar(SubSet, SrcStr);
  if Start = 0 then
    Exit;
  for I := Start + 1 to Length(SrcStr) - Start do
  begin
    K := Pos(SrcStr[I], SubSet);
    if K = 0 then
    begin
      Len := I - Start;
      Exit;
    end;
  end;
end;
{$ENDIF}
{------------------------------}

function GetAvailPhysMem: DWORD;
begin
  GetMemStatus;
  //  GlobalMemoryStatus(MemoryStatus); { Refresh memory info }
  Result := MemoryStatus.dwAvailPhys;
end;
{------------------------------}

function GetTotalPhysMem: DWORD;
begin
  GetMemStatus;
  //  GlobalMemoryStatus(MemoryStatus); { Refresh memory info }
  Result := MemoryStatus.dwTotalPhys;
end;
{------------------------------}

function GetAvailPageFile: DWORD;
begin
  //  GlobalMemoryStatus(MemoryStatus); { Refresh memory info }
  GetMemStatus;
  Result := MemoryStatus.dwAvailPageFile;
end;
{-----------------------------}

function GetAvailVirtMem: DWORD;
begin
  //  GlobalMemoryStatus(MemoryStatus); { Refresh memory info }
  GetMemStatus;
  Result := MemoryStatus.dwAvailVirtual;
end;
{-------------------------------------------------------------}

function RectInImage(PInfo: PBitMapInfo; Rect: PRect): Boolean;
begin
  Result := False;
  with Rect^, PInfo^.bmiHeader do
  begin
    if (Right < Left) or (Bottom < Top) then // Illegal Rect
      Exit;
    if (Left < 0) or (Top < 0) then     // Rect out of image left/top bounds
      Exit;
    if (Left >= biWidth) or (Top >= biHeight) then // Rect out of image left/top bounds
      Exit;
    if (Right >= biWidth) or (Bottom >= biHeight) then
      Exit;
  end;
  Result := True;
end;

function PRectWidth(const Rect: PRect): Integer; register;
asm
 { Note that register convention of params passing for class is as :
  EAX -> TRect}
  PUSH   EDX;   { Save work register }
  MOV    EDX, EAX;                      { Copy address of a TRect }
  MOV    EAX, (TRect PTR [EDX]).Right;  { Load Right coordinate}
  SUB    EAX, (TRect PTR [EDX]).Left;   { Sub left one}
  JNS    @@Ok;                          { Not negative result}
  NEG    EAX;                           { Change the sign }
@@Ok:
  INC    EAX;                           { Increase to create physical value, not logical }
@Exit:
  POP    EDX;   { Save work register }
end;

function PRectHeight(const Rect: PRect): Integer; register;
asm
 { Note that register convention of params passing for a function is as :
  EAX -> TRect }
  PUSH   EDX;   { Save work register }
  MOV    EDX, EAX;
  MOV    EAX, (TRect PTR [EDX]).Bottom;
  SUB    EAX, (TRect PTR [EDX]).Top;
  JNS    @@Ok
  NEG    EAX
@@Ok:
  INC    EAX;
  POP    EDX
end;

function RectsAreEqual(var Rect1, Rect2: TRect): Boolean;
begin
  Result := CompareMem(@Rect1, @Rect2, SizeOf(TRect));
end;

{
  We can change EAX, EDX, ECX freely.
  Accoring to register calling convetion:
   EAX-> Rect1, EDX-> Rect2
}

function RectsSizeAreEqual(var Rect1, Rect2: TRect): Boolean; register;
asm
  PUSH   EBX
  PUSH   ECX
  PUSH   EDX;                 { Save work register[s] }
  MOV    EBX, EAX;            { Save @Rect1 }
  CALL   PRectHeight;          { Get height of a Rect1 }
  MOV    ECX, EAX;            { Store it }
  MOV    EAX, EDX;            { Load @Rect2 }
  CALL   PRectHeight;          { Get height of a Rect2 }
  CMP    EAX, ECX             { Compare heights }
  JNE    @@False;             { Exit if they are not equal}
  MOV    EAX, EBX;            { Again @Rect1}
  CALL   PRectWidth;           { Get width of a Rect1 }
  XCHG   EAX, EDX             { Store result in EDX and load @Rect2 to EAX }
  CALL   PRectWidth;
  CMP    EAX, EDX
  JNE    @@False;
@@True:
  MOV    AL, 1;
  JMP    @@Exit
@@False:
  SUB   EAX, EAX;
@@Exit:
  POP   EDX
  POP   ECX
  POP   EBX
end;

function RectsAreNotIntersected(const PRect1, PRect2: PRect): Boolean; register;
asm
  Call   RectsAreIntersected;  { Check intersection, result = 1 if yes, 0 if no }
  XOR    AL,1;          { if AL=1 it will be 0, if AL=0 it will be 1}
end;

function RectsAreIntersected(const PRect1, PRect2: PRect): Boolean; register;
{
  EAX->Rect1;
  EDX->Rect2;
}
asm
  MOV    ECX, EAX;                      { Now ECX -> Rect1 }
  MOV    EAX, (TRect PTR [ECX]).Right;  { if Rect1.Right }
  CMP    EAX, (TRect PTR [EDX]).Left;   { <  Rect2.left }
  JL     @@False;                       { Then not intersected }
  MOV    EAX, (TRect PTR [ECX]).Left;   { if Rect1.Left }
  CMP    EAX, (TRect PTR [EDX]).Right;  { >  Rect2.Right }
  JG     @@False;                       { Then not intersected }
  MOV    EAX, (TRect PTR [ECX]).Top;    { if Rect1.Top (Note: it is a WinGIS TRect!) }
  CMP    EAX, (TRect PTR [EDX]).Bottom; { >  Rect2.Bottom }
  JG     @@False;                       { Then not intersected }
  MOV    EAX, (TRect PTR [ECX]).Bottom; { if Rect1.Bottom (Note: it is a project TRect nor Windows one!) }
  CMP    EAX, (TRect PTR [EDX]).Top;    { <  Rect2.Top }
  JL     @@False;                       { Then not intersected }
@@True:
  MOV    AL, 1;
  JMP    @@Exit
@@False:
  SUB    EAX, EAX;
@@Exit:
end;
{-------------------------------------------------------------
 This function detects if Value in range [LowBound..HighBound]
 if Include = TRUE, bounds are included in comparison,
 else not.
--------------------------------------------------------------}

function InRange(LowBound, Value, HighBound: Integer; Include: Boolean): Boolean;
var
  MinV, MaxV        : Integer;
begin
  Result := False;
  MinV := Min(LowBound, HighBound);
  MaxV := Max(LowBound, HighBound);
  if (Value < MinV) or (Value > MaxV) then
    Exit;
  if (not Include) then
    if ((Value = MinV) or (Value = MaxV)) then
      Exit;
  Result := True;
end;
{-----------------------------------------------------}

constructor TTrLib.Create;
begin
end;
{-----------------------------------------------------}

destructor TTrLib.Destroy;
begin
  if (FLoaded) then
  begin
    FreeLibrary(fImgManLib);
    FLoaded := False;
  end;
  inherited Destroy;
end;

{-----------------------------------------------------}

function FileIsVMSI(FileName: AnsiString): Boolean;
var
  Hdr               : DWORD;
  PRIFile           : THandle;
begin
  Result := False;
  if FileExists(FIleName) then
  begin
    PRIFile := FileOpen(FileName, fmOpenRead or fmShareDenyNone);
    try
      if FileRead(PRIFile, Hdr, SizeOf(DWORD)) <> SizeOf(DWORD) then
        Exit;
      Result := Hdr = VMSI_MAGIC;
    finally
      FileClose(PRIFile);
    end;
  end
  else
    if CompareText(ExtractFileExt(FileName), PRIExt) = 0 then
      Result := True;
end;

{-----------------------------------------------------}

function FileIsVMSIPlus(FileName: AnsiString): Boolean;
var
  Hdr               : array[0..5] of Byte;
  PREFile           : THandle;
begin
  Result := False;
  if FileExists(FileName) then
  begin
    PREFile := FileOpen(FileName, fmOpenRead or fmShareDenyNone);
    try
      if FileRead(PREFile, Hdr, SizeOf(Hdr)) <> SizeOf(Hdr) then
        Exit;
      Result := CompareMem(@Hdr[0], @PRE_MAGIC[0], SizeOf(PRE_MAGIC));
    finally
      FileClose(PREFile);
    end;
  end
  else
    if CompareText(ExtractFileExt(FileName), PREExt) = 0 then
      Result := True;
end;

{-----------------------------------------------------}

function FileIsECW(FileName: AnsiString): Boolean;
var
  Hdr               : WORD;
  ECWFile           : THandle;
begin
  Result := False;
  if FileExists(FileName) then
  begin
    ECWFile := FileOpen(FileName, fmOpenRead or fmShareDenyNone);
    try
      if FileRead(ECWFile, Hdr, SizeOf(Hdr)) <> SizeOf(Hdr) then
        Exit;
      Result := Hdr = ECW_MAGIC;
    finally
      FileClose(ECWFile);
    end;
  end
  else
    if CompareText(ExtractFileExt(FileName), ECWExt) = 0 then
      Result := True;
end;

{-----------------------------------------------------}

function FileIsBMP(FileName: AnsiString; ptrBitsCount: PWORD; ptrCompression: PDWORD): Boolean;
type
  TBMPHeader = record
    FileHeader: TBITMAPFILEHEADER;
    InfoHeader: TBITMAPINFOHEADER;
  end;
var
  FHdr              : TBMPHeader;
  BMPFile           : THandle;
  BMPSize           : DWORD;
begin
  Result := False;
  if FileExists(FIleName) then
  begin
    BMPFile := _lopen(PChar(FileName), OF_READ + OF_SHARE_DENY_NONE);
    try
      BMPSize := _llseek(BMPFile, 0, FILE_END); { Get file size! }
      _llseek(BMPFIle, 0, FILE_BEGIN);
      if _hread(BMPFile, Addr(FHdr), SizeOf(FHdr)) <> SizeOf(FHdr) then
        Exit;
      if FHdr.FileHeader.bfType <> WORD($4D42) then { 'BM' - as signature of BMP }
        Exit;
      if (BMPSize <> FHdr.FileHeader.bfSize) then { File size is wrong counted in header}
        Exit;
      if ptrBitsCount <> nil then
        ptrBitsCount^ := FHdr.InfoHeader.biBitCount;
      if ptrCompression <> nil then     { Compression}
        ptrCompression^ := fHdr.InfoHeader.biCompression;
      Result := True;
    finally
      _lclose(BMPFile);
    end;
  end
  else
  begin
    if CompareText(ExtractFileExt(FileName), BMPExt) = 0 then
      Result := True;
  end;
end;

function TTrLib.LoadImg(sSrcSize: TRECT; sDestSize: TPOINT; uFactor: LongInt;
  lpFileName: LPSTR; uID: LongInt; psDestRect: PRECT; iPage: LongInt = 1): LPVOID;
var
  hImg, hImg2       : THandle;
  Stat, Res         : Integer;
  Info              : PBitMapInfo;
  CopyOp            : ULONG;
  ImgRect           : TRect;

begin
  LoadImg := nil;                       // Error as default
  LastError := ec_IOERROR;
  if not Loaded then
  begin
    LastError := 4;                     // DLL is not loaded
    Exit;
  end;
  hImg := ImgOpenEx(lpFileName, nil); // Try to open image for ImgMan
  if hImg = 0 then                      //  and it was unsuccesfull
    Exit;
  try
    if not ImageSetPage(hImg, iPage - 1) then
      Exit;
    ProcessVMSI(lpFileName, hImg, uFactor, @sSrcSize); { If VMSI, select best page }
    Info := fImgGetInfo(hImg, Stat);
    if not RectInImage(Info, @sSrcSize) then
    begin
      LastError := ec_INVALIDPARAM;
      Exit;
    end;
    //     hImg2 := fImgCopy(hImg,sDestSize.x,sDestSize.y,nil,COPY_DEL) // Copy all image
    with Info.bmiHeader do
    begin
      ImgRect := Rect(0, 0, biWidth - 1, biHeight - 1);
      CopyOp := COPY_DEL;               { Set default for fastest method }
      if not RectsSizeAreEqual(ImgRect, sSrcSize) then { Source <> dest. rect, i.e. we have to scale image }
        if Info.bmiHeader.biBitCount = 24 then { Try to optimize copying}
          if CanFilterImage(Info) then  { Enough small image detected }
            CopyOp := COPY_INTERPOLATE + COPY_ANTIALIAS; { True colors interpolation}
      hImg2 := fImgCopy(hImg, sDestSize.x, sDestSize.y, @sSrcSize, CopyOp); // Copy partial image
    end;
  finally
    fImgClose(hImg);                    // Close original image
  end;
  if hImg2 = 0 then
  begin                                 // Error of copying image with scaling
    LastError := ec_WRONGMODCOD;
    Exit;
  end;
  try
    {$IFDEF TEST}
    Info := fImgGetInfo(hImg, Stat);    // Load ImgMan pointer to TBitMapInfo
    BMPINFOPtr := fImgGetDIBPtr(hImg2);
    if BMPINFOPtr <> nil then
    begin
      DIBPtr1 := PChar(BMPINFOPtr) + BitMapInfoSize(PBitMapInfo(BMPINFOPtr));
    end;
    DIBSection := fImgGetHBitmap(hImg2);
    with Info.bmiHeader do
    begin
      ImgRect := Rect(0, 0, biWidth - 1, biHeight - 1);
    end;
    Stat := fImgLoad(hImg2, @ImgRect);
    DIBPtr := fImgGetDataPtr(hImg2);
    {$ENDIF}
    psDestRect^ := Bounds(0, 0, sDestSize.x - 1, sDestSize.y - 1); // Make new image rectangle
    Res := fImgGetDIB(hImg2, True, psDestRect); // Get DIB at last

    {    LoadImg     := PAnsiChar(GlobalLock(Res));}
    LoadImg := PAnsiChar(Res);
  finally
    fImgClose(hImg2);                   // Close scaled image
  end;
  LastError := 0;
  Inc(LoadCounter);
end;                                    // Good bye, guys

function TTrLib.Register(ModuleCode, SerialNr, ModuleName: LPSTR): LongInt;
asm
  mov    EAX, $FFFFFFFF;
end;

procedure TTrLib.UnRegister(ID: LongInt);
asm
end;

function TTrLib.EnumImpFormats: LPSTR;
var
  I                 : Integer;
begin
  EnumImpFormats := nil;
  if (Loaded) then
  begin
    Result := fImgGetExt;
    for I := 1 to MaxInt do
      if Result[I] = char(0) then
      begin
        if char(Result[I + 1]) = char(0) then break; // EOLN reached
        Result[I] := char('|');
      end;
  end
  else
    LastError := 4;
end;

// Funtion returns the extension available from ImageMan in form
// *.ex1;...;*.exN as an AnsiString

function TTrLib.GetImpStrings: AnsiString;
var
  Ext               : LPSTR;
  AllExt            : PChar;
begin
  Result := EmptyStr;
  if (Loaded) then
  begin
    Ext := fImgGetExt;                  // Get string with extensions ready for Windows function
    AllExt := StrEnd(Ext) + 1;
    Result := StrPas(AllExt);
    GlobalFreePtr(Ext);                 // We not need it more
  end
  else
    LastError := 4;
end;

{++ NEW Sygsky interface function --}

function TTrLib.ImageGetImpFormats: AnsiString;
var
  Ext               : PAnsiChar;
begin
  Result := EmptyStr;
  if not Loaded then Exit;
  Ext := EnumImpFormats;                { Get extensions supported }
  SetString(Result, Ext, StrLen(Ext));  // Copy it to our buffer
  GlobalFreePtr(Ext);                   // We not need it more
end;

function TTrLib.ImageGetExpFormats: AnsiString;
var
  lpExp             : LPSTR;
begin
  Result := EmptyStr;
  if not Loaded then Exit;
  lpExp := EnumExpFormats;
  if lpExp = nil then
    Exit;
  SetString(Result, lpExp, StrLen(lpExp));
  VirtualFree(lpExp, 0, MEM_RELEASE);
end;

function TTrLib.EnumExpFormats: LPSTR;
var
  Size, I           : Integer;
  ResStr            : AnsiString;
  Ptr1, Ptr2, Ptr3  : PChar;
begin
  Result := nil;                        // Mark bad result
  if not Loaded then Exit;
  Size := fImgXGetExt(nil);             // Determine the wanted size for export buffer
  if Size <= 0 then Exit;               // Check the unavailability for export functions

  Result := VirtualAlloc(nil, Size * 2, MEM_COMMIT, PAGE_READWRITE);

  if fImgXGetExt(Result) = IMG_OK then
  begin                                 // Read from ImgMan
    ResStr := '';
    Ptr1 := Result;
    for I := 0 to MaxInt do
    begin
      Ptr2 := StrScan(Ptr1, '.');
      if Ptr2 = nil then                // end of line detected
        break;
      Ptr3 := StrScan(Ptr2 + 1, ',');
      if Ptr3 = nil then                // end of line detected
      begin
        Ptr3 := Ptr2 + StrLen(Ptr2);
        (Ptr3 + 1)^ := #0;
      end;
      ResStr := ResStr +
        Copy(string(Ptr2), 2, Ptr3 - Ptr2 - 1) + '|' +
        Copy(string(Ptr1), 1, Ptr3 - Ptr1) + '|';
      Ptr1 := Ptr3 + 1;
    end;
    ResStr := Copy(ResStr, 1, Length(ResStr) - 1); // Cut last '|' symbol
    StrCopy(Result, PChar(ResStr));
  end
  else
  begin
    VirtualFree(Result, 0, MEM_RELEASE);
    Result := nil;
  end;
end;

function TTrLib.getLastError(ID: LongInt): LongInt;
begin
  Result := -1;
  if not Loaded then Exit;
  Result := LastError;
  if Result = 0 then
  begin                                 { No Error detected }
    Result := fImgGetStatus;
    case Result of
      IMG_OK: Result := 0;
      IMG_INV_FIL,
        IMG_NOFILE: Result := ec_IOERROR;
      IMG_NOMEM: Result := ec_NOMEM;
      IMG_FILE_ERR: Result := ec_WRONGMODCOD;
      IMG_NSUPPORT: Result := ec_NOSUPPORT;
      IMG_INV_HAND,
        IMG_INV_PAGE,
        IMG_BAD_SRC: Result := ec_INVALIDPARAM;
      IMG_PROC_ERR: Result := ec_PROC_ERR;
      IMG_PRINT_ERR: Result := ec_PRINT_ERR;
      IMG_BAD_PRN: Result := ec_BAD_PRN;
      IMG_BAD_TYPE: Result := ec_BAD_TYPE;
      IMG_NO_DILS: Result := ec_NO_DILS;
    end;
  end;
end;
{---------------------------------------------------------}

function TTrLib.GetHeader(lpFileName: LPSTR; var PageCount: Integer;
  var IsVMSI: Boolean; iPage: LongInt): PBITMAPINFO;
var
  PInfo             : PBitMapInfo;
  hImg, TmpFlags, Res, Size: Integer;
const
  STD_RESOLUTION    = 2835;             // 72 pixels per inch - screen resolution

  function SetPage(iPage: Integer): Boolean;
  begin
    Result := False;
    if (iPage = 1) and (PageCount = 1) then
    begin                               // Already set...
      Result := True;
      Exit;
    end;
    if (iPage > PageCount) then         // Too big page number (out of range)
      Exit;
    if fImgSetPage(hImg, iPage - 1) <> IMG_OK then
      Exit;                             // Can't set needed page!!!
    Result := True;
  end;

begin
  GetHeader := nil;
  if not Loaded then
  begin
    LastError := 4;
    Exit;
  end;

  hImg := ImgOpenEx(lpFilename, nil); // Try to open original image file
  if hImg = 0 then
    Exit;                               // Exit if some error[s] occured
  try                                   //  to load image and open its address

    { Check page count }
    Res := FImgPageCount(hImg, PageCount); // Count Pages
    if Res <> IMG_OK then
      PageCount := 1;                   // No pages? Single must be in any case ?
    if iPage <> 1 then
      if not SetPage(iPage) then
        Exit;                           { Wanted page not found! }
    PInfo := fImgGetInfo(hImg, TmpFlags); // Load ImgMan pointer to TBitMapInfo
    if PInfo = nil then
      Exit;                             // Error detected
    Size := BitMapInfoSize(PInfo);      // Find wanted size
    Result := VirtualAlloc(nil, Size, MEM_COMMIT, PAGE_READWRITE);
    if Result = nil then
      Exit;                             // We were unsuccessful to reserve the mem ? Damned Bill Gates !
    CopyMemory(Result, PInfo, Size);
    { Check resolution not to be zero }
    with Result.bmiHeader do
    begin
      if biXPelsPerMeter = 0 then
      begin
        if biYPelsPerMeter = 0 then
        begin
          biXPelsPerMeter := STD_RESOLUTION;
          biYPelsPerMeter := STD_RESOLUTION;
        end
        else
          biXPelsPerMeter := biYPelsPerMeter;
      end
      else
        if biYPelsPerMeter = 0 then
          biYPelsPerMeter := biXPelsPerMeter;
    end;
    { Detect VMSI file mode }
    IsVMSI := FileIsVMSI(lpFileName);
  finally
    TmpFlags := fImgClose(hImg);        // Close the image as we not need it more
  end;
end;
{---------------------------------------------------------}

function TTrLib.Rotate(FileName, outFileName: LPSTR; Angle: double; ID: LongInt; Hwindow: Hwnd): LongInt;
var
  hImg              : THandle;
  hImgRot           : Integer;
  iAngle            : Integer;
  nAngle            : Extended;
  Color             : COLORREF;
const
  FullCircleAngle   : Extended = 2 * Pi;
begin
  Result := 1;                          // default result is ERROR
  if not Loaded then
  begin
    LastError := 4;
    Exit;
  end;
  LastError := ec_IOERROR;
  hImg := ImgOpenEx(FileName, nil);
  if hImg = 0 then
    Exit;
  try
    nAngle := -Angle;                   { Note that ImgMan is thinking in opposite to WinGIS }
    if Abs(nAngle) > FullCircleAngle then { Input above 360 degrees! Dumkopff is driving? }
      nAngle := nAngle - Int(nAngle / FullCircleAngle) * FullCircleAngle;
    if nAngle < 0.0 then
    begin
      nAngle := FullCircleAngle + nAngle;
    end;
    nAngle := RadToDeg(nAngle);
    iAngle := Trunc(nAngle) * 100 + Round(Frac(nAngle) * 100.0); { Rotate angle in ints }
    { Now try to set the color to fill the image
      Note that the hImg itself can be changed during operation silently. Don't worry... }
    ChangePal(hImg, Color);
    hImgRot := fImgRotate(hImg, iAngle, Color); { Rotate it. }
  finally
    fImgClose(hImg);
  end;
  if hImgRot <> 0 then
  try
    Result := Integer(fImgXWriteImage(OutFileName, nil, hImgRot, 0, hWindow, IMGXOPT_OVERWRITE) <> IMG_OK);
    if Result <> 0 then
      if FileIsVMSI(OutFileName) then   { save as PRI! }
        Result := Integer(Image2VMSI(OutFileName, OutFileName));
  finally
    fImgClose(hImgRot);
  end;
  if Result = 0 then
    LastError := 0;
end;
{---------------------------------------------------------}

function TTrLib.GetImgFlags(sFileName: LPSTR; ID: LongInt = -1): LongInt;
var
  hImg              : Integer;
  iFlags            : Integer;
begin
  Result := 0;
  if not Loaded then
  begin                                 // Library is not loaded
    LastError := 4;
    Exit;
  end;
  hImg := ImgOpenEx(sFileName, nil); // Open file in ImageMan
  if hImg = 0 then                      // Can't be opened
    Exit;
  try
    fImgGetInfo(hImg, iFlags);          { Read Flags and get Info pointer }
    Result := ifDLLDraw;                { Set it always. Why is it needed ? }
    if (iFlags and IMG_DISP_VECTOR) <> 0 then // Image is vector one
      Result := Result or ifVector;
    if (iFlags and IMG_SCALE_SELF) <> 0 then // Image is vector one
      Result := Result or ifScaleSelf;
    LastError := 0;
  finally
    fImgClose(hImg);
  end;
end;
{---------------------------------------------------------}

function TTrLib.DrawImage(sFileName: LPSTR; hDC: HDC; lpDestRect, lpSrcRect: PRECT;
  uLoadFactor: LongInt; dwROP: DWORD; ID: LongInt): LongInt;
var
  hImg, hImg2       : Integer;
  hPal              : HPALETTE;
  {  PInfo: PBITMAPINFO;
    I: Integer;         }
  LoadFX, loadFY, LoadFactor: UINT;
begin
  Result := 0;
  if not Loaded then
  begin
    Exit;
  end;
  hImg := ImgOpenEX(sFileName, nil); { Open original file }
  if hImg <> 0 then                     { Go ahead }
  try
    {PInfo := FImgGetInfo(hImg, I);}
    LoadFX := ceil(PRectWidth(lpSrcRect) * 1.0 / PRectWidth(lpDestRect));
    LoadFY := ceil(PRectHeight(lpSrcRect) * 1.0 / PRectHeight(lpDestRect));
    LoadFactor := Max(Min(LoadFX, LoadFY), 1);
    ProcessVMSI(sFileName, hImg, LoadFactor, lpSrcRect); { Adjust setting for VMSI}
    { Create temp image to enhance visual impression[s] :=}

    hImg2:=0;

    //if not (Uppercase(ExtractFileExt(sFileName)) = '.BMP') then begin
    //   hImg2 := fImgCopy(HImg, PRectWidth(lpDestRect), PRectHeight(lpDestRect), lpSrcRect, {COPY_DEL + } COPY_INTERPOLATE + COPY_ANTIALIAS);
    //end;


    if hImg2 <> 0 then                  { Draw it at once }
    try
      FImgClose(hImg);
      hImg := 0;
      hPal := FImgGetPalette(hImg2);
      if hPal <> 0 then                 { Use the palette }
      try
        SelectPalette(hDC, hPal, FALSE);
        RealizePalette(hDC);
      finally
        DeleteObject(hPal);
      end;
      Result := FImgDrawImage(hImg2, hDC, lpDestRect, nil);
    finally                             { Clean-up }
      fImgClose(hImg2);
    end
    else                                { Some errors to create intermedate image, try to draw full one! }
    begin
      hPal := FImgGetPalette(hImg);
      if hPal <> 0 then                 { Use the palette }
      try
        SelectPalette(hDC, hPal, FALSE);
        RealizePalette(hDC);
      finally
        DeleteObject(hPal);
      end;
      Result := FImgDrawImage(hImg, hDC, lpDestRect, lpSrcRect);
    end;
  finally
    if hImg <> 0 then                   { Clean-up}
      FImgClose(hImg);
  end;
end;
{---------------------------------------------------------}

function TTrLib.GetPageCount(sFileName: LPSTR; ID: LongInt = -1): LongInt;
var
  hImg              : THandle;
  PageCnt           : Integer;
begin
  Result := 0;
  if not Loaded then
  begin
    LastError := 4;
    Exit;
  end;
  hImg := ImgOpenEx(sFileName, nil); // Open original file
  if hImg = 0 then                      // No such one
    Exit;
  try
    if fImgPageCount(hImg, PageCnt) <> IMG_OK then // Error[s]
      Exit;
    Result := PageCnt;
    LastError := 0;
  finally
    fImgClose(hImg);
  end;
end;
{---------------------------------------------------------}

function TTrLib.PrintImage(sFileName: LPSTR; hDC: HDC; lpDestRect, lpSrcRect: PRECT;
  uLoadFactor: LongInt; dwROP: DWORD; ID: LongInt = 0): LongInt;
var
  hImg, hImgDIB     : THandle;
  Info              : PBitMapInfo;
  hImgOpt           : THandle;
  CopyMode, Temp    : Integer;
  DevRect           : TRect;
  {$IFDEF DEBUG}                        // Just for info of debugging programmer :o)
  LoadFactor        : Integer;
  XLoadFactor, YLoadFactor: Single;
  iHorzRes, iVertRes, iHorzsize, iVertsize, iLogPixelSizeX, iLogPixelSizeY,
    iPWidth, iPHeight, iAspectX, iAspectY: Integer;
  {$ENDIF}
  iDevLWidth, iDevLHeight: Integer;
  iDevPWidth, iDevPHeight: Integer;
  iImgPWidth, iImgPHeight: Integer;
  iSrcWidth, iSrcHeight: Integer;
  Fmt: AnsiString;

begin
  {$IFDEF DEBUG}                        // Just for info of debugging programmer :o)
  iHorzRes := GetDeviceCaps(hDC, HORZRES);
  iVertRes := GetDeviceCaps(hDC, VERTRES);
  iHorzSize := GetDeviceCaps(hDC, HORZSIZE);
  iVertSize := GetDeviceCaps(hDC, VERTSIZE);
  iLogPixelSizeX := GetDeviceCaps(hDC, LOGPIXELSX);
  iLogPixelSizeY := GetDeviceCaps(hDC, LOGPIXELSY);
  iPWidth := GetDeviceCaps(hDC, PHYSICALWIDTH);
  iPHeight := GetDeviceCaps(hDC, PHYSICALHEIGHT);
  iAspectX := GetDeviceCaps(hDC, ASPECTX);
  iAspectY := GetDeviceCaps(hDC, ASPECTY);
  {$ENDIF}
  Result := 0;
  if not Loaded then
  begin
    LastError := ec_UTILERROR;
    Exit;
  end;
  LastError := ec_PRINT_ERR;
  hImg := ImgOpenEx(sFileName, nil); { Open file in ImageMan }
  if hImg = 0 then
    Exit;                               { Open error }
  try
    ProcessVMSI(sFileName, hImg, uLoadFactor, lpSrcRect); { In case of VMSI}
    // Try to optimize the printing
    Info := fImgGetInfo(hImg, Temp);    // Load ImgMan pointer to TBitMapInfo
    if Assigned(Info) then              // Image exists
    begin
      iSrcWidth := PRectWidth(lpSrcRect); // Physical width of printable image rectangle
      iSrcHeight := PRectHeight(lpSrcRect); //  and the phys. height of this rect
      iImgPWidth := Info.bmiHeader.biWidth; // Physical width of selected image
      iImgPHeight := Info.bmiHeader.biHeight; //  and physical  height
      DevRect := LRectToDRect(hDC, lpDestRect); // Convert logical units into physical ones
      iDevPWidth := PRectWidth(@DevRect); // Width and
      iDevPHeight := PRectHeight(@DevRect); //  height on the device in physical units (pixels)
      iDevLWidth := PRectWidth(lpDestRect); // Width and
      iDevLHeight := PRectHeight(lpDestRect); //  height on the device in logical unit
      {$IFDEF DEBUG}
      XLoadfactor := iDevLWidth / iDevPWidth;
      YLoadFactor := iDevLHeight / iDevPHeight;
      LoadFactor := Max(Min(ceil(Abs(XLoadFactor)), ceil(Abs(YLoadFactor))), 1);
      {$ENDIF}
      // Now check follow conditions:
      // 1. Output rectangle is already equal source rectangle
      // 2. Source rectangle is equal to whole image rectangle?
      // if one of them doesn't meet the condition, we will create subimage for printing
      // else we will use the existing whole image - it will be very-very rarely
      // meet case! I can't image this sutuation, but who knows?
      if ((iImgPWidth <> iSrcWidth) or (iImgPHeight <> iSrcHeight)) or
        ((iDevPWidth <> iSrcWidth) or (iDevPHeight <> iSrcHeight)) then
        // Image is not printed as a whole piece,
        // try to optimize it for the printer  driver
        // as we knew what ImageMan is doing,
        // but we can't be sure what a printer driver will do :)
      begin
        // Check if device area is larger than image source rectangle one
        if (iDevPWidth * iDevPHeight > iSrcWidth * iSrcHeight) then
          // simply use image 1:1 part as in any case no more logical information can
          // be get from picture than it contains in its pixels :o)
        begin                           // Cut wanted part before print
          iDevPWidth := iSrcWidth;
          iDevPHeight := iSrcheight;
        end;
        // Always try to do optimization
        // but first define copy mode. Just in case, if our image is a small one :)
        CopyMode := BestCopyMode(hImg);
        hImgOpt := fImgCopy(hImg, iDevPWidth, iDevPHeight, lpSrcRect, CopyMode);
        if hImgOpt <> 0 then            // Copied successfully
        begin
          fImgClose(hImg);              // Close unwanted large image
          hImg := hImgOpt;              // Replace with more fitting image
          DevRect := Rect(0, 0, Pred(iDevPWidth), Pred(iDevPHeight)); // Ensure we print the whole piece
          lpSrcRect := @DevRect {nil};  // Ensure we print the whole piece
          {            Result := fImgPrintImage(hImg, hDC, lpDestRect, nil);
                      LastError := 0;
                      Exit;}
        end;
      end;
//{$DEFINE OLD}
      {$IFDEF OLD}
      Result := fImgPrintImage(hImg, hDC, lpDestRect, lpSrcRect);
      {$ELSE}
      try
        hImgDIB := ImageGetDIB(hImg, True, lpSrcRect);
        if hImgDIB <> 0 then
        try
          Info := GlobalLock(hImgDIB);
          try
            Result := StretchDIBits(hDC, lpDestRect.Left, lpDestRect.Top,
              lpDestRect.Right - lpDestRect.Left + 1 {iDevLWidth},
              lpDestRect.Bottom - lpDestRect.Top + 1 {iDevLHeight},
              0, 0, Info.bmiHeader.biWidth, Info.bmiHeader.biHeight,
              PChar(Info) + BitMapInfoSize(Info), Info^, DIB_RGB_COLORS, dwROP);
{
	StretchDIBits(hPrnDC, lpDest->left, lpDest->top,
                     RECTWID(lpDest),
                     RECTHI(lpDest),
		      lpSrc->left, (int)lpbi->biHeight - (lpSrc->top + RECTHI(lpSrc)),RECTWID(lpSrc),
                     RECTHI(lpSrc),img->pDIB->GetDataPtr(), (LPBITMAPINFO)lpbi, DIB_RGB_COLORS, img->dwROP);
}

            {$IFNDEF VMSI}
            if Result <>  Info.bmiHeader.biHeight then
            begin
              SetLength(Fmt, 1024);
              Result := FormatMessage( FORMAT_MESSAGE_FROM_SYSTEM, nil, GetLastError,
                                    LANG_NEUTRAL, PChar(Fmt), 1024, nil);
              SetLength(Fmt, Result);
              {$IFDEF FORCE_NO_TRANSPARENCY}
              // Now try to print it directly
              Result := StretchDIBits(hDC, lpDestRect.Left, lpDestRect.Top,
                lpDestRect.Right - lpDestRect.Left + 1 {iDevLWidth},
                lpDestRect.Bottom - lpDestRect.Top + 1 {iDevLHeight},
                0, 0, Info.bmiHeader.biWidth, Info.bmiHeader.biHeight,
                PChar(Info) + BitMapInfoSize(Info), Info^, DIB_RGB_COLORS, SRCCOPY {except of dwROP});
              if Result <> Info.bmiHeader.biHeight then
                LogFile.WriteLog('And SRCCOPY not helped to print it :o(')
              else
                LogFile.WriteLog('But the image was printed when using SRCCOPY!');
              {$ENDIF}
            end;
            {$ENDIF}
          finally
            GlobalUnlock(hImgDIB);
          end
        finally
          GlobalFree(hImgDIB);
        end
        else
        begin
          Result := fImgPrintImage(hImg, hDC, lpDestRect, lpSrcRect);
        end;
      except
        Result := fImgPrintImage(hImg, hDC, lpDestRect, lpSrcRect);
      end;
      {$ENDIF}
      LastError := 0;
    end
    else
      LastError := ec_IOERROR;          // Not imageman file. How it sneaked to this place?
  finally
    fImgClose(hImg);
  end;
end;

//------------------------------------------

function TTrLib.getErrorText(ID: Integer): AnsiString;
var
  Res               : Integer;
  ErrStr            : AnsiString;
const
  ErrSize           = 2048;
begin
  Result := '';
  if not Loaded then Exit;
  SetLength(ErrStr, ErrSize);
  Res := fImgErrString(PChar(ErrStr), ErrSize);
  if Res <> 0 then
    SetString(Result, PChar(ErrStr), StrLen(PChar(ErrStr)))
  else
    Result := '?';
end;
//------------------------------------------

function TTrLib.ImageGetInfoStruct(hImage: THandle; lpDIB: PBITMAPINFO;
  var lpFlags: Integer): Integer;
begin
  Result := 0;
  if not Loaded then Exit;
  Result := fImgGetInfoStruct(hImage, lpDIB, lpFlags);
end;

{++ Sygsky: Note that nPage is 0-based index of page in a file }

function TTrLib.ImageSetPage(hImage: THandle; nPage: UINT): Boolean;
var
  PageCnt           : UINT;
begin
  nPage := Max(0, nPage);               { Don't use negative values }
  Result := ImagePageCount(hImage, Integer(PageCnt)) = IMG_OK;
  if (PageCnt > 0) and Result then      { We can set it }
  begin
    if (PageCnt = 1) and (nPage = 0) then { All is set }
    begin
      Result := True;
      Exit;
    end;
    if nPage < PageCnt then
      Result := fImgSetPage(hImage, nPage) = IMG_OK
    else
      Result := False;
  end
  else
    if nPage = 0 then
      Result := True
    else
      Result := False;
end;
//------------------------------------------

function TTrLib.ImagePageCount(hImage: THandle; var lpCnt: Integer): Integer;
begin
  Result := fImgPageCount(hImage, lpCnt);
end;
//------------------------------------------

function TTrLib.ImageGetPage(hImage: THandle; var lpPage: Integer): Integer;
begin
  Result := -1;
  if not Loaded then Exit;
  Result := fImgGetPage(hImage, lpPage);
end;
//------------------------------------------

function TTrLib.ImageGetInfo(hImage: THandle; lpImgInfo: PBitMapInfo): Boolean;
var
  fImageInfo        : PBitMapInfo;
  ImgFlags          : Integer;
begin
  Result := False;
  if not Loaded then Exit;
  fImageInfo := fImgGetInfo(hImage, ImgFlags);
  if fImageInfo = nil then
    Exit;
  Result := True;
  CopyMemory(lpImgInfo, fImageInfo, BitMapInfoSize(fImageInfo));
end;

// Use this procedure with care as it returns the internal address,
// don't change anything in buffer it points out

function TTrLib.ImageGetInfoAddr(hImage: THandle): PBitMapInfo;
var
  //  fImageInfo: PBitMapInfo;
  ImgFlags          : Integer;
begin
  Result := nil;
  if not Loaded then Exit;
  Result := fImgGetInfo(hImage, ImgFlags);
end;

{ Added by Sygsky for internal usage  }

procedure TTrlib.ChangePal(var hImg: THandle; var Color: COLORREF);
type
  TPalEntryArray = array[0..MaxInt div sizeof(PALETTEENTRY) - 1] of PALETTEENTRY;
  PPalEntryArray = ^TPalEntryArray;
var
  PInfo             : PBITMAPINFO;
  Pal               : HPALETTE;
  NewColor          : TRGBQuad;
  MaxClrNum         : Integer;
  NearestClr        : Integer;
  I, PalSize        : Integer;
  hImg2             : THandle;
  Ptr               : PPalEntryArray {PPALETTEENTRY} {Pointer};
begin
  PInfo := fImgGetInfo(hImg, I);        { Read information and status flags }
  if PInfo = nil then Exit;             { Some error, exit }
  Color := RGB(255, 255, 255);          { Set WHITE color as a default one }
  NewColor := TRGBQuad(Color);
  with PInfo^.bmiHeader do
    case biBitCount of
      4, 8:
        begin                           { Analyse 16 and 256 colored images ONLY !}
          MaxClrNum := 1 shl biBitCount;
          Pal := fImgGetPalette(hImg);  { Get image palette }
          if Pal = 0 then Exit;         { Exit if error }
          try
            GetMem(Ptr, SizeOf(PALETTEENTRY) * (MaxClrNum + 4)); { Temp storage for all the colors }
            try
              PalSize := GetPaletteEntries(Pal, 0, MaxClrNum, Ptr[0]); { Get all the palette }
              if PalSize = 0 then Exit; { Not read, exit on error state }
              NearestClr := GetNearestPaletteIndex(Pal, Cardinal(NewColor)); { Find matching color }
              if CompareMem(@Ptr[NearestClr], @NewColor, 3) then Exit; { WHITE in palette! Exit }
              { Wanted color not found in our palette, try to insert it now }
              if MaxClrNum <= PalSize then
              begin                     { No place for new color, set rotate color to the nearest one :-(}
                Color := COLORREF(Ptr[NearestClr]);
                Exit;
              end;
              { There is a free place in palette - rare chance to use our talent[s] :-)
                and try to add one more color into palette }
              if not ResizePalette(Pal, PalSize + 1) then Exit;
              if SetPaletteEntries(Pal, PalSize, 1, PALETTEENTRY(NewColor)) = 0 then Exit;
              {PalSize := } GetPaletteEntries(Pal, 0, MaxClrNum, Ptr[0]); { Get all the palette }
              hImg2 := fImgReduceColorsPal(hImg, IMG_BAYER, Pal); {IMG_BURKES,IMG_FLOYD}
              if hImg2 = 0 then Exit;   { Error while add WHITE to palette }
              fImgClose(hImg);
              hImg := hImg2;
            finally
              FreeMem(Ptr);
            end;
          finally
            DeleteObject(Pal);
          end;
        end;
    else                                { case of biBitCount value }
      Exit;                             {We not process monochrom and true colored images}
    end;
end;
{------------------------------------------------------------}

//++ Glukhov PaletteConverion Build#149 26.12.00

function BitMapColorsNum(InfoPtr: PBitMapInfo): Integer;
begin
  Result := 0;
  with InfoPtr^.bmiHeader do
  begin
    case biBitCount of
      1, 4, 8:
        begin
          // Max.number of color indeces used
          Result := 1 shl biBitCount;
          // Number of colors used in this image, if not maximum
          if biClrUsed <> 0 then Result := biClrUsed;
        end;
      //    24: Result := 0;
      16, 32:
        case biCompression of
          BI_RGB: Result := 0;
          BI_BITFIELDS: Result := 3;    // Special case! - masks used...
        end;
    end;
  end;
end;
//-- Glukhov PaletteConverion Build#149 26.12.00

function BitMapInfoSize(InfoPtr: PBitMapInfo): Integer;
begin
  with InfoPtr^.bmiHeader do
  begin
    if biSize = 0 then                  // For some vector formats
      biSize := 40;                     //  preset it to default value
    Result := BitMapColorsNum(InfoPtr) * SizeOf(TRGBQuad) + Integer(biSize); // True size of a color table
  end;
end;

function BitMapImageSize(InfoPtr: PBitMapInfo): Integer;
begin
  with InfoPtr.bmiHeader do
  begin
    Result := biSizeImage;
    if Result = 0 then
      Result := ((((biWidth * biBitCount + 31) div 32) * 4) * biHeight) * biPlanes;
    //                                          --->ROW   --->PLANE    -->Image
  end;
end;

function TTrLib.ImageOpenSolo(lpFileName, Dummy: PAnsiChar): Integer;
begin
  if Loaded then
    Result := ImgOpenEx(lpFileName, Dummy)
  else
    Result := 0;
end;

function TTrLib.ImageClose(hImage: THandle): Integer;
begin
  Result := fImgClose(hImage);
end;
//

function TTrLib.ImageCopy(hImage: THandle; nWid, nHi: Integer;
  lpSrc: PRect; CopyMode: Integer): THandle;
begin
  Result := fImgCopy(hImage, nWid, nHi, lpSrc, CopyMode {COPY_DEL});
end;
//

function TTrLib.CanFilterImage(PInfo: PBITMAPINFO): Boolean;
var
  MemImgSize        : UINT;
begin
  Result := True;
  with PInfo^.bmiHeader do
  begin
    { Calculate raw image size  }
    MemImgSize := ((biWidth * biBitCount + 7) div 8) * biHeight;
    { Test to be very small, less then 1 megabytes }
    if MemImgSize <= 1024 * 1024 then Exit; { Ok, is processible }
    { Test to be 1/16 of available physical memory }
    if MemImgSize < (GetTotalPhysMem div 16) then Exit; { Ok }
    Result := False;                    { No, it is too big to filter it}
  end;
end;

//

function TTrLib.CanFilterImage(Image: THandle): Boolean;
var
  Info              : PBitMapInfo;
  Temp              : Integer;
begin
  Result := False;                      { Default is FALSE }
  if Image = 0 then
    Exit;
  Info := fImgGetInfo(Image, Temp);     // Load ImgMan pointer to TBitMapInfo
  if Assigned(Info) then
    Result := CanFilterImage(Info);
end;

//

function SaveImageAs(SrcFile, DstFile: AnsiString): Boolean;
var
  Hnd1              : Integer;
begin
  Result := False;
  if TrLib = nil then Exit;
  if not TrLib.Loaded then Exit;
  if FileIsVMSI(DstFile) then           { save as PRI }
  begin
    Result := Image2VMSI(SrcFile, DstFile);
    Exit;
  end;
  Hnd1 := TrLib.ImgOpenEx(PChar(SrcFile), nil);
  if Hnd1 <> 0 then
  try
    Result := TrLib.FImgXWriteImage(PChar(DstFile), nil, Hnd1, 0, 0, 0) = IMGX_OK;
  finally
    if not Result then
    begin
      {$IFDEF DEBUG}
      TrLib.LastError := TrLib.fImgGetStatus;
      {$ENDIF}
      TrLib.LastError := ec_IOERROR;
    end;
    TrLib.FImgClose(Hnd1);
  end;
end;

function SavePartialImageAs(SrcFile, DstFile: AnsiString; SrcRect: TRect): Boolean;
var
  Hnd1, Hnd2        : Integer;
  PInfo             : PBitMapInfo;
  Stat              : Integer;
begin
  Result := False;
  TrLib.LastError := ec_UTILERROR;
  if TrLib = nil then Exit;
  if not TrLib.Loaded then Exit;

  TrLib.LastError := ec_IOERROR;
  try
    Hnd1 := TrLib.ImgOpenEx(PChar(SrcFile), nil);
  except
    Exit;
  end;
  if Hnd1 = 0 then
    Exit;
  { Read information and status flags }
  PInfo := TrLib.fImgGetInfo(Hnd1, Stat);
  { Check image type supported }
  if PInfo = nil then
    Exit;
  { Check new image to be totally in the source one }
  if not RectInImage(PInfo, @SrcRect) then
  begin
    TrLib.LastError := ec_INVALIDPARAM;
    Exit;
  end;
  if Hnd1 <> 0 then
  try
    Hnd2 := TrLib.fImgCopy(Hnd1, PRectWidth(@SrcRect),
      PRectHeight(@SrcRect), @SrcRect, COPY_DEL);
    if Hnd2 <> 0 then
    try
      TrLib.FImgClose(Hnd1);            { We don't need this image more }
      Hnd1 := 0;
      Result := TrLib.FImgXWriteImage(PChar(DstFile), nil, Hnd2, 0, 0, IMGXOPT_OVERWRITE) = IMGX_OK;
      if Result then
        TrLib.LastError := 0;
      if FileIsVMSI(DstFile) then       { save as PRI -> add more pages with smaller scale[s] }
        Result := Image2VMSI(DstFile, DstFile);
    finally
      TrLib.FImgClose(Hnd2);
    end;
  finally
    if Hnd1 <> 0 then
      TrLib.FImgClose(Hnd1);
  end;
end;

function FileIsImageOne(const AFileName: AnsiString): Boolean;
var
  FHnd              : Integer;
  ABuffer           : Array[0..4095] of Char;
begin

  Result := FALSE;

  if TrLib = nil then
    Exit;
  if not TrLib.Loaded then Exit;
  try
    ZeroMemory(@ABuffer,4095);
    StrCopy(ABuffer,PChar(AFileName));

    FHnd := TrLib.ImgOpenEX(@ABuffer,nil); { Try to open file }
    if FHnd <> 0 then Result := True;
    TrLib.FImgClose(FHnd);

  except
    Result:=False;
  end;

end;

{$IFDEF FUTURE}

function FileBitMapInfo(AFile: AnsiString): PBitmapInfo;
var
  FHnd              : Integer;
  PInfo             : PBitMapInfo;
  L                 : Integer;
begin
  Result := nil;
  if TrLib = nil then
    Exit;
  if not TrLib.Loaded then
    Exit;
  try
    FHnd := TrLib.ImgOpenEx(PChar(AFile), nil); { Try to open file }
    try
      if FHnd = 0 then
        Exit;
      PInfo := TrLib.ImageGetInfoAddr(FHnd);
      if PInfo = nil then
        Exit;
      L := BitMapInfoSize(PInfo);
      GetMem(Result, L);
      CopyMemory(Result, PInfo, L);
    finally
      TrLib.FImgClose(FHnd);
    end;
  except
  end;
end;
{$ENDIF}

//--------------------------------------------------------------
{
  Filter is a string in the following format:
  '1st pair 1st member|1st pair 2nd member|2st pair 1st member|2nd pair 2nd member...etc'

  for example:

  'Text files (*.txt)|*.TXT|Pascal files (*.pas)|*.PAS'
  \________1st_pair_______/\_________2nd__pair_______/

  Where 1st member of 1st pair is shown in dialog and 2nd one of 1s pair used physically to
   filter files to show in files box.
}
{----------------------------------------------
  Function extracts extension without period!
 ----------------------------------------------}

function ExtractFilterExt(const Filter: AnsiString; Index: Integer): AnsiString;
var
  I                 : Integer;
  Pos0, Pos1        : PAnsiChar;
const
  FSep              : Char = '|';
begin
  Result := '';
  if (Length(Filter) = 0) or (Index <= 0) then
    Exit;
  { Start from the beginning of string }
  Pos0 := PAnsiChar(Filter);
  for I := 1 to Index - 1 do
  begin                                 // Skip all previous extensions
    Pos0 := StrScan(Pos0, FSep);
    if Pos0 = nil then Exit;            { Not found, exit }
    Inc(Pos0);                          { Pos to 2nd member beginning }
    Pos0 := StrScan(Pos0, FSep);
    if Pos0 = nil then Exit;
    Inc(Pos0);
  end;
  { Pos -> begining of a wanted member}
  Pos0 := StrScan(Pos0, FSep);          { Find dividing separator }
  if Pos0 = nil then Exit;
  Inc(Pos0);
  Pos1 := StrScan(Pos0, FSep);
  if Pos1 = nil then                    { This can be the last pair }
    Result := AnsiString(Pos0)
  else
    Result := Copy(AnsiString(Pos0), 1, Pos1 - Pos0);
  I := Pos('.', Result);
  if I <> 0 then
    Result := Copy(Result, I + 1, MaxInt);
end;

function IsExtInFilter(const Filter, Ext: AnsiString): Boolean;
begin
  Result := GetFilterIndex(Filter, Ext) > 0;
end;
{--------------------------------------------------------------}
{ This function serach filter string for wanted extensiion and }
{ returns index + 1 if successful, else -1                     }
{ Ext can be any form: "*.ext", "filname.ext", "ext", ".ext" etc}
{--------------------------------------------------------------}

function GetFilterIndex(const Filter, Ext: AnsiString): Integer;
var
  Ext1, Sample      : AnsiString;
  I                 : Integer;
  Pos1, Pos2        : PAnsiChar;
begin
  Result := -1;                         // Default  result is 'no ext found'
  if (Length(Ext) = 0) or (Length(Filter) = 0) then // Checkout stupid input
    Exit;
  Ext1 := ExtractFileExt(Ext);          // Next try to rectify input
  if Ext1 = '' then                     // No case - we assume that input not contains period
    Ext1 := '*.' + Ext
  else
    Ext1 := '*' + Ext1;
  Ext1 := Ext1;                         { To ensure not found correct exit }
  Pos1 := PAnsiChar(Filter);
  for I := 1 to MaxInt do
  begin
    Pos1 := StrScan(Pos1, '|');         // Find first separator
    if Pos1 = nil then
      Exit;                             // End of scan, exit
    Inc(Pos1);
    Pos2 := StrScan(Pos1, '|');
    if Pos2 = nil then                  // End of line detected
      Sample := AnsiString(Pos1)        // If last member of filter it will be OK
    else
      Sample := Copy(AnsiString(Pos1), 1, Pos2 - Pos1);
    if CompareText(Sample, Ext1) = 0 then // Found !!!
    begin
      Result := I;
      Exit;
    end;
    if Pos2 = nil then                  { No more entries detected }
      Exit;
    Pos1 := Pos2 + 1;                   // Prepare for new search cycle
  end;
end;

function TTrLib.DrawFullImage(sFileName: LPSTR; hDC: HDC; lpDestRect: PRECT): Boolean;
var
  hImage, hImage2   : THandle;

  W, H              : Integer;

  hPal              : HPALETTE;
  PInfo             : PBITMAPINFO;
  I                 : Integer;
  SrcRect           : TRect;
  LoadFX, loadFY, LoadFactor: UINT;
begin
  Result := False;
  if not Loaded then Exit;
  hImage := ImgOpenEx(sFileName, nil);
  if hImage <> 0 then
  try
    PInfo := FImgGetInfo(hImage, I);
    SrcRect := Rect(0, 0, PInfo.bmiHeader.biWidth - 1, PInfo.bmiHeader.biHeight - 1);
    LoadFX := ceil(PRectWidth(@SrcRect) * 1.0 / PRectWidth(lpDestRect));
    LoadFY := ceil(PRectHeight(@SrcRect) * 1.0 / PRectHeight(lpDestRect));
    LoadFactor := Max(Min(LoadFX, LoadFY), 1);
    ProcessVMSI(sFileName, hImage, LoadFactor, @SrcRect); { In case of VMSI}
    {$IFDEF CCITTDEBUG}
    // To test CCIT strange behavior for cases not start line read
    if CompareText(ExtractFileExt(sFileName), '.TIF') = 0 then
      if Skip1stLine then
      begin
        SrcRect.Top := 1;
        //        Left := Right div 10;
        //        Right := Right - Right div 10;
        //        Bottom := Bottom - Bottom div 10;
      end;
    {$ENDIF}
    W := PRectWidth(lpDestRect);
    H := PRectHeight(lpDestRect);
    hImage2 := fImgCopy(HImage, W, H, @SrcRect, {COPY_DEL} COPY_INTERPOLATE + COPY_ANTIALIAS);
    //    hImage2 := fImgCopy(HImage, W, H, @SrcRect, BestCopyMode(PInfo));
    if HImage2 <> 0 then
    try
      FImgClose(hImage);
      hImage := 0;
      hPal := FImgGetPalette(hImage2);
      if hPal <> 0 then
      try
        SelectPalette(hDC, hPal, FALSE);
        RealizePalette(hDC);
      finally
        DeleteObject(hPal);
      end;
      FImgDrawImage(hImage2, hDC, lpDestRect, nil);
      Result := True;
    finally
      fImgClose(HImage2);
    end
    else
    begin
      {$IFDEF CCITTDEBUG}               // To test CCIT strange behavior for cases not start line read
      I := fImgGetStatus;
      {$ENDIF}
      hPal := FImgGetPalette(hImage);
      if hPal <> 0 then
      try
        SelectPalette(hDC, hPal, FALSE);
        RealizePalette(hDC);
      finally
        DeleteObject(hPal);
      end;
      FImgDrawImage(hImage, hDC, lpDestRect, nil);
    end;
  finally
    if HImage <> 0 then
      FImgClose(hImage);
  end
  else
    Result := False;
end;

function JHCGreeting: PChar;
begin
  Result := PChar(JHCGreeting1 + JHCGreeting2 + JHCGreeting3);
end;

function TTrLib.GetTryLoad: Boolean;
var
  Str               : AnsiString;
  ECWAtom           : TAtom;            // ECW suport is licensed by Freddie ;o)
  L                 : Integer;
begin

  ///////
  if bNoImageLoading then begin
     Result:=False;
     exit;
  end;
  ///////

  if FLoaded then                       { Already loaded, need not to load }
  begin
    Result := True;
    Exit;
  end;
  Result := False;
  LastError := 4;
  {++ Sygsky change#1 ON AXDLL Exception: first try to load from module path }
  SetLength(Str, 256);
  L := GetModuleFileName(HInstance, PChar(Str), 255);
  SetLength(Str, L);

  {$IFNDEF VMSI}
  // ECW support is allowed with this license file
    ECWAtom := GlobalAddAtom(JHCGreeting);
  {else
    ECWAtom := 0;}
  try
    {$ELSE}
  ECWAtom := GlobalAddAtom(JHCGreeting); // ECW always allowed for VMSI :o)
  try
    {$ENDIF}
    fImgManLib := LoadLibrary(PChar(ExtractFilePath(Str) + ImgManName));
    if fImgManLib <= 32 then            { Next let system to load it as it needs }
      fImgManLib := LoadLibrary(ImgManName);
    {-- Sygsky }
    {$IFNDEF AXDLL}
    WingisMainForm.CheckLibStatus(fImgmanLib,ImgManName,'Images');
    {$ENDIF}
    if fImgManLib <= 32 then
      Exit;
  finally
    if ECWAtom <> 0 then
      GlobalDeleteAtom(ECWAtom);
  end;

  { Load all addresses directly from ImagMan32.DLL }

  @fImgGetExt := GetProcAddress(fImgManLib, 'ImgGetExt');
  @fImgXGetExt := GetProcAddress(fImgManLib, 'ImgXGetExt');
  @fImgGetStatus := GetProcAddress(fImgManLib, 'ImgGetStatus');
  @fImgCopy := GetProcAddress(fImgManLib, 'ImgCopy');
  @fImgErrString := GetProcAddress(fImgManLib, 'ImgErrString');
  @fImgGetInfo := GetProcAddress(fImgManLib, 'ImgGetInfo');
  @fImgGetInfoStruct := GetProcAddress(fImgManLib, 'ImgGetInfoStruct');
  @fImgOpenSolo := GetProcAddress(fImgManLib, 'ImgOpenSolo');
  @fImgClose := GetProcAddress(fImgManLib, 'ImgClose');
  @fImgSetPage := GetProcAddress(fImgManLib, 'ImgSetPage');
  @fImgPageCount := GetProcAddress(fImgManLib, 'ImgPageCount');
  @fImgGetPage := GetProcAddress(fImgManLib, 'ImgGetPage');
  @fImgRotate := GetProcAddress(fImgManLib, 'ImgRotate');
  @fImgXWriteImage := GetProcAddress(fImgManLib, 'ImgXWriteImage');
  @fImgGetDIB := GetProcAddress(fImgManLib, 'ImgGetDIB');
  @fImgDrawImage := GetProcAddress(fImgManLib, 'ImgDrawImage');
  @fImgPrintImage := GetProcAddress(fImgManLib, 'ImgPrintImage');
  @fImgGetPalette := GetProcAddress(fImgManLib, 'ImgGetPalette');
  @fImgReduceColorsPal := GetProcAddress(fImgManLib, 'ImgReduceColorsPal');
  @fImgXOptBlkGet := GetProcAddress(fImgManLib, 'ImgXOptBlkGet');
  @fImgXOptBlkDel := GetProcAddress(fImgManLib, 'ImgXOptBlkDel');
  @fImgXOptBlkCreate := GetProcAddress(fImgManLib, 'ImgXOptBlkCreate');
  @fImgXOptBlkAdd := GetProcAddress(fImgManLib, 'ImgXOptBlkAdd');
  @fImgSetDefaultStatusProc := GetProcAddress(fImgManLib, 'ImgSetDefaultStatusProc');
  @fImgSetStatusProc := GetProcAddress(fImgManLib, 'ImgSetStatusProc');

  @fImgLoad := GetProcAddress(fImgManLib, 'ImgLoad');
  @fImgGetDataPtr := GetProcAddress(fImgManLib, 'ImgGetDataPtr');
  @fImgGetDIBPtr := GetProcAddress(fImgManLib, 'ImgGetDIBPtr');
  @fImgGetHBitmap := GetProcAddress(fImgManLib, 'ImgGetHBitmap');
  @fImgIncreaseColors := GetProcAddress(fImgManLib, 'ImgIncreaseColors');
  @fImgReduceColors := GetProcAddress(fImgManLib, 'ImgReduceColors');

  MemoryStatus.dwLength := SizeOf(TMemoryStatus);
  //  GlobalMemoryStatus(MemoryStatus); { Refresh memory info }
  FLoaded := True;
  LastError := 0;
  Result := True;
end;

{++brovak}

procedure SetValidRect(var CheckedRect: TRect);
var
  TmpCoord          : integer;
begin
  ;
  if CheckedRect.TopLeft.X > CheckedRect.BottomRight.X then
  begin
    TmpCoord := CheckedRect.TopLeft.X;
    CheckedRect.TopLeft.X := CheckedRect.BottomRight.X;
    CheckedRect.BottomRight.X := TmpCoord;
  end;
  if CheckedRect.TopLeft.Y > CheckedRect.BottomRight.Y then
  begin
    TmpCoord := CheckedRect.TopLeft.y;
    CheckedRect.TopLeft.y := CheckedRect.BottomRight.y;
    CheckedRect.BottomRight.y := TmpCoord;
  end;
end;
{--brovak}

function HasGrayPalette(Info: PBitMapInfo): Boolean;
type
  TPALETTEARRAY = array[0..255] of TRGBQuad; // TRGBQuad (4 bytes)
  PPALETTEARRAY = ^TPALETTEARRAY;
  {const
    ADDER = $00010101;}
var
  PPal              : PPALETTEARRAY;
  Val               : TRGBQuad;
  i                 : 0..255;
begin
  Result := False;
  case Info.bmiHeader.biBitCount of
    //    1, 24:; // Always gray - no any other  choice :))
    4, 8:
      begin
        PPal := @Info.bmiColors[0];
        for i := 0 to BitMapColorsNum(Info) - 1 do
        begin
          Val := PPal[i];
          if (Val.rgbBlue <> Val.rgbGreen) or (Val.rgbBlue <> Val.rgbRed) then
            Exit;
        end;
      end;
  end;
  Result := True;
end;

//------------------------------------------------------------------------------

function Image2VMSI(SrcName, DstName: AnsiString; PageNum: Integer = 1;
  DefaultCompress: Boolean = False; UserData: PUserStatData = nil): Boolean;
var
  Img1, Img2        : Integer;
  StartPage         : Integer;
  Opts, IOpts, ImgPixelNum: Integer;
  Info, Info2       : PBitMapInfo;
  ImgCnt, ImgX, ImgY, Factor: Integer;
  iFlags, Temp      : Integer;
  SrcRect           : TRect;
begin
  {$IFDEF VERBOSE}
  AddText('********* Entered Image2VMSI');
  {$ENDIF}
  Result := False;
  if TrLib = nil then Exit;
  if not TrLib.Loaded then Exit;
  with TrLib do
  begin
    // Try to open file source
    Img1 := TrLib.ImgOpenEx(PChar(SrcName), nil);
    if Img1 = 0 then                    // Image not exists
      Exit;
    if not ImageSetPage(Img1, PageNum - 1) then // We can't set wanted page!
    begin
      LastError := ec_INVALIDPARAM;
      Exit;
    end;
    Opts := 0;
    Img2 := 0;
    Info2 := nil;                       // Initialize secondary buffer address
    GetMem(Info, BitMapInfoSize(fImgGetInfo(Img1, ImgCnt)));
    try
      // Generate new file name
      StartPage := 1;
      if FileExists(DstName) then       // User wants to add scaled pages to existing image?
        if FileIsVMSI(DstName) then     // ... to VMSI image?
          if CompareText(SrcName, DstName) = 0 then // Source == Destination! He knows what to do...
          begin
            if (ImagePageCount(Img1, ImgCnt) = IMG_OK) then
              if ImgCnt <= 1 then       // We will add scaled images to source 1st page
                StartPage := 2;
          end
          else
            SysUtils.DeleteFile(DstName);
      if not ImageSetPage(Img1, 0) then Exit;
      if not ImageGetInfo(Img1, Info) then Exit;
      if UserData <> nil then
      begin
        Temp := DWORD(BitMapImageSize(Info)) div UserData.UserSteps;
        if Temp = 0 then
          Temp := BitMapImageSize(Info) - 1;
        TrLib.ImageSetStatusProc(Img1, UserData.UserStatusProc,
          Temp, UserData.UserData);
      end;
      Opts := fImgXOptBlkCreate(nil);
      // Check compression
      if Info.bmiHeader.biBitCount <= 8 then { except True Colored ones :}
        Opts := fImgXOptBlkAdd(Opts, PChar('COMPRESS'), PChar('ON'));
      {      iFlags := COPY_DEL; // Default approximation is set to NONE
            case Info.bmiHeader.biBitCount of
              1: iFlags := COPY_DEL; // COPY_DEL/AND/OR:Copy,black,white
              4: iFlags := COPY_ANTIALIAS;
              8: iFlags := COPY_ANTIALIAS256;
              16, 24: iFlags := COPY_INTERPOLATE;
            end;}
      iFlags := BestCopyMode(Info);
      if not DefaultCompress then       // Use NONE for all the files
        Opts := fImgXOptBlkAdd(Opts, PChar('COMPRESS'), PChar('OFF'));
      Opts := fImgXOptBlkAdd(Opts, PChar('APPEND'), PChar('ON'));
      { Don't change the lower used MAGIC number 200 ! }
      Opts := fImgXOptBlkAdd(Opts, PChar('TIFF_ROWSPERSTRIP'), PChar('200'));
      Opts := fImgXOptBlkAdd(Opts, PChar('TIFF_ARTIST'), PChar(Artist));
      Opts := fImgXOptBlkAdd(Opts, PChar('TIFF_COPYRIGHT'), PChar(CopyRight));
      ImgPixelNum := MinVMSISize + 1;
      Factor := 1;
      Img2 := Img1;                     // First frame will be source one
      for ImgCnt := 1 to MaxInt do      { Main loop to reduce image size }
      begin
        Opts := fImgXOptBlkAdd(Opts, PChar('TIFF_PAGES'), PChar(IntToStr(ImgCnt)));
        { Try to append next (if not first) image to destination file }
        IOpts := 0;
        if Img2 = Img1 then
          IOpts := IMGXOPT_OVERWRITE_PROMPT;
        if StartPage <= ImgCnt then
        begin
          Temp := fImgXWriteImage(PChar(DstName), PChar('PRI'), Img2, Opts, 0, IOpts);
          if Temp <> IMG_OK then
            Exit;
        end;
        if Img2 = Img1 then
          Img2 := 0;
        if ImgPixelNum < MinVMSISize then { Smallest subimage size is reached }
          Break;
        with Info.bmiHeader do
        begin
          ImgX := biWidth div (VMSIDivider * ImgCnt);
          ImgY := biHeight div (VMSIDivider * ImgCnt);
          ImgPixelNum := ImgX * ImgY;
          with SrcRect do
          begin
            Left := 0;
            Top := 0;
            Right := ImgX * VMSIDivider * ImgCnt - 1;
            Bottom := ImgY * VMSIDivider * ImgCnt - 1;
          end;
          Factor := Factor * VMSIDivider;
          if Img2 <> 0 then             { Delete temp handle }
            fImgClose(Img2);
          { Create new scaled image }
          Img2 := fImgCopy(Img1, ImgX, ImgY, @SrcRect, iFlags);
          if Img2 = 0 then
            Exit;
          if Info2 <> nil then
            FreeMem(Info2);
          GetMem(Info2, BitMapInfoSize(fImgGetInfo(Img2, Temp)));
          if not ImageGetInfo(Img2, Info2) then Exit;
          if UserData <> nil then
          begin
            Temp := DWORD(BitMapImageSize(Info2)) div UserData.UserSteps;
            if Temp = 0 then
              Temp := BitMapImageSize(Info2) - 1;
            TrLib.ImageSetStatusProc(Img2, UserData.UserStatusProc, Temp,
              UserData.UserData);
          end;
        end;
      end;
      { Complete operations}
      Result := True;
    finally
      FImgClose(Img1);
      if Img2 <> 0 then
        FImgClose(Img2);
      if Opts <> 0 then
        fImgXOptBlkDel(Opts);
      FreeMem(Info);
      if Info2 <> nil then
        FreeMem(Info2);
    end;
    {$IFDEF VERBOSE}
    AddText('********* Exited from Image2VMSI');
    {$ENDIF}
  end;
end;

function TTrLib.Stretch(InFileName, outFileName: AnsiString; Width, Height: Integer): Boolean;
var
  Img1, Img2        : Integer;
begin
  Result := False;
  if not TrLib.Loaded then Exit;
  with TrLib do
  begin
    Img1 := ImgOpenEx(@InFileName[1], nil); { Open src image }
    if Img1 = 0 then
      Exit;
    try
      Img2 := fImgCopy(Img1, Width, Height, nil, COPY_DEL);
    finally
      fImgClose(Img1);
    end;
    if Img2 <> 0 then
    try
      Result := fImgXWriteImage(@OutFileName[1], nil, Img2, 0, 0, IMGXOPT_OVERWRITE_PROMPT) = IMG_OK;
    finally
      fImgClose(Img2);
    end;
  end;
end;

procedure TTrLib.ProcessVMSI(FName: AnsiString; hImg: THandle; uFactor: UINT;
  SrcRect: PRect);
var
  WantedPage, ScaleFactor: Integer;
  PageCnt, Stat     : Integer;
  Info              : PBitMapInfo;
begin
  if not IsLoaded then
    Exit;
  if fImgPageCount(hImg, PageCnt) <> IMG_OK then
    PageCnt := 1;
  if (PageCnt > 1) then
    if (uFactor >= UINT(VMSIDivider)) and FileIsVMSI(FName) then { Calculate the page to use in case of multi-page file }
    begin
      WantedPage := Min((uFactor div UINT(VMSIDivider)) + 1, PageCnt);
      if WantedPage = 1 then            { Nothing to do, leave it for main proc }
        Exit;
      fImgSetPage(hImg, WantedPage - 1);
      ScaleFactor := (WantedPage - 1) * VMSIDivider;
      if ScaleFactor = 0 then
        Inc(ScaleFactor);
      { Get info from  scaled page }
      Info := fImgGetInfo(hImg, Stat);  // Load ImgMan pointer to TBitMapInfo
      with SrcRect^ do
      begin
        Left := Left div ScaleFactor;
        Top := Top div ScaleFactor;
        Right := Min(Info.bmiHeader.biWidth - 1, Right div ScaleFactor);
        Bottom := Min(Info.bmiHeader.biHeight - 1, Bottom div ScaleFactor);
      end;
    end;
end;

procedure TTrLib.ImageSetStatusProc(hImage: THandle;
  StatusProc: ImgManStatus; PerBytes, UserData: DWORD);
begin
  if not Self.Loaded then Exit;
  fImgSetStatusProc(hImage, StatusProc, PerBytes, UserData);
end;

procedure TTrLib.ImageSetDefaultStatusProc(StatusProc: ImgManStatus;
  PerBytes, UserData: DWORD);
begin
  if not Self.Loaded then Exit;
  fImgSetDefaultStatusProc(StatusProc, PerBytes, UserData);
end;

function TTrLib.DLLName: AnsiString;
var
  sRes              : AnsiString;
  iRes              : DWORD;
begin
  Result := ImgManName;
  if not IsLoaded then
    Exit;
  SetLength(sRes, 256);
  iRes := GetModuleFileName(fImgManLib, PChar(sRes), 255);
  if iRes > 0 then
    SetString(Result, PChar(sRes), iRes);
end;

function ImageManAvailable: Boolean;
var
  Lib               : THandle;
  L                 : UINT;
  Str               : AnsiString;
begin
  Result := True;
  if TrLib.IsLoaded then                { Already loaded, report on it }
    Exit;
  Lib := 0;
  try                                   { Find lib in our EXE (WInGIS, VMSI etc) directory first }
    SetLength(Str, 256);
    L := GetModuleFileName(HInstance, PChar(Str), 255);
    SetLength(Str, L);
    Lib := LoadLibrary(PChar(ExtractFilePath(Str) + ImgManName));
    if Lib <= 32 then                   { Let system to find the lib }
      Lib := LoadLibrary(ImgManName);
    {$IFNDEF AXDLL}
    WingisMainForm.CheckLibStatus(Lib,ImgManName,'Images');
    {$ENDIF}
  finally
    if Lib > 32 then
      { Unload library as we don't need it directly now }
    begin
      FreeLibrary(Lib);
      Result := True;                   // Loaded
    end
    else
      Result := False;
  end;
end;

function GetLastImageManError: Integer;
begin
  Result := 0;
  if not Assigned(TrLib) then
    Exit;
  if TrLib.IsLoaded then                { Already loaded, report on it }
    Exit;
  Result := TrLib.fImgGetStatus;
end;

function LRectToDRect(const DC: HDC; LRect: PRect): TRect;
begin
  CopyMemory(@Result, LRect, SizeOf(TRect));
  LPToDP(DC, Result, 2);
end;

// Remember, that if you will set copy mode not in correspondence with source pixel
// bit depth, for example COPY_ANTIALIAS for 1 bit image, resulting image will be 16
// colors one, not B/W as expected.

function BestCopyMode(const Info: PBitMapInfo): Integer;
begin
  Result := COPY_DEL;                   // Default approximation is set to NONE
  if Info = nil then
    Exit;
  case Info.bmiHeader.biBitCount of
    //    1: iFlags := COPY_DEL; // COPY_DEL/AND/OR:Copy,black,white
    4: Result := COPY_ANTIALIAS;
    8: Result := COPY_ANTIALIAS256;
    16, 24: Result := COPY_INTERPOLATE;
  end;
end;

function BestCopyMode(const hImage: THandle): Integer; overload;
var
  Info              : PBitMapInfo;
  Temp              : Integer;
begin
  Result := COPY_DEL;                   // Default approximation is set to NONE
  if hImage = 0 then
    Exit;
  Info := TrLib.fImgGetInfo(hImage, Temp); // Load ImgMan pointer to TBitMapInfo
  if TrLib.CanFilterImage(Info) then
    Result := BestCopyMode(Info);
end;

// This function calculates Windows' in memory BITMAP row size in bytes
// Inputs:
//        RowLen = length of image's row in pixels
//        BitsPerPixel = color depth of image in bits: 1,4,8,24 are most common

function BytesPerRow(RowLen, BitsPerPixel: DWORD): DWORD;
begin
  Result := ((RowLen * BitsPerPixel + 31) div 32) * 4;
end;

function TTrLib.ImageGetDIB(hImage: THandle; bNewDIB: Boolean;
  lpRect: PRECT): THandle;
begin
  Result := 0;
  if not IsLoaded then
    Exit;
  Result := fImgGetDIB(hImage, bNewDIB, lpRect);
end;

function TTrLib.ImageGetPalette(hImage: THandle): HPALETTE;
begin
  Result := 0;
  if not IsLoaded then
    Exit;
  Result := fImgGetPalette(hImage);
end;

function TTrLib.ImageIncreaseColors(hImage: THandle; nBitDepth: Integer): THandle;
begin
  Result := 0;
  if not IsLoaded then
    Exit;
  Result := fImgIncreaseColors(hImage, nBitDepth);
end;

function TTrLib.ImageReduceColors(hImage: THandle; nColors,
  nFlags: Integer): THandle;
begin
  Result := 0;
  if not IsLoaded then
    Exit;
  Result := fImgReduceColors(hImage, nColors, nFlags);
end;

function TTrLib.ImageReduceColorsPal(hImage: THandle; nFlags: Integer;
  hPal: HPALETTE): THandle;
begin
  Result := fImgReduceColorsPal(HImage, nFlags, hPal);
end;

function TTrLib.ImgOpenEx(lpFileName, Dummy: Pointer): Integer;
var
AImgFile,AImgPath,AImgFilePath: AnsiString;
ACurDir: AnsiString;
begin
     Result:=0;

     AImgFilePath:=StrPas(lpFileName);
     AImgPath:=ExtractFilePath(AImgFilePath);
     AImgFile:=ExtractFileName(AImgFilePath);

     if StrLen(PChar(AImgFilePath)) > 128 then begin
        ACurDir:=GetCurrentDir;
        try
           SetCurrentDir(AImgPath);
           Result:=fImgOpenSolo(PChar(AImgFile),Dummy);
        finally
           SetCurrentDir(ACurDir);
        end;
     end
     else begin
        Result:=fImgOpenSolo(PChar(AImgFilePath),Dummy);
     end;
end;

initialization
  TrLib := TTrLib.Create;
  MemoryStatus.dwLength := SizeOf(MemoryStatus);
  GetMemStatus;

finalization
  TrLib.Free;
end.

