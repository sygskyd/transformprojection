Unit BitmapXFillDlg;
  
Interface

Uses Classes,ColorSelector,Combobtn,Controls,Forms,Graphics,ImagEdit,Palette,
     StdCtrls,StyleDef,WCtrls,WinTypes,XFills,Validate,MultiLng,ComCtrls,Spinbtn,
     FillDropDown,ProjStyle,AM_Def;        

Type TBitmapXFillDialog = Class(TWForm)
       CancelBtn        : TButton;
       OKBtn            : TButton;
    GeneralPage: TPageControl;
    TabSheet1: TTabSheet;
    DefinitonPage: TTabSheet;
    NameEdit: TEdit;
    WLabel1: TWLabel;
    RedoBtn: TSpeedBtn;
    UndoBtn: TSpeedBtn;
    FillColorBtn: TColorSelectBtn;
    DrawColorBtn: TColorSelectBtn;
    FloodFillBtn: TSpeedBtn;
    DrawCircleBtn: TSpeedBtn;
    DrawRectBtn: TSpeedBtn;
    DrawLineBtn: TSpeedBtn;
    FreehandBtn: TSpeedBtn;
    ImageEditor: TImageEditor;
    MlgSection: TMlgSection;
    NameVal: TValidator;
    Group1: TWGroupBox;
    WLabel3: TWLabel;
    WLabel4: TWLabel;
    WLabel6: TWLabel;
    WLabel2: TWLabel;
    ScaleEdit: TEdit;
    BackColorSelectBtn: TColorSelectBtn;
    FillStyleDropDown: TDropDownBtn;
    ForeColorSelectBtn: TColorSelectBtn;
    ScaleSpin: TSpinBtn;
    GeneralizePrintCheck: TCheckBox;
    GeneralizeCheck: TCheckBox;
    ScaleVal: TScaleValidator;
    WidthEdit: TEdit;
    HeightEdit: TEdit;
    WidthVal: TIntValidator;
    HeightVal: TIntValidator;
    SpinBtn1: TSpinBtn;
    SpinBtn2: TSpinBtn;
    WLabel5: TWLabel;
    WLabel7: TWLabel;
       Procedure   DrawCircleBtnClick(Sender: TObject);
       Procedure   DrawColorBtnSelectColor(Sender: TObject);
       Procedure   DrawLineBtnClick(Sender: TObject);
       Procedure   DrawRectBtnClick(Sender: TObject);
       Procedure   FillColorBtnSelectColor(Sender: TObject);
       Procedure   FloodFillBtnClick(Sender: TObject);
       Procedure   FormCloseQuery(Sender: TObject; var CanClose: Boolean);
       Procedure   FormCreate(Sender: TObject);
       Procedure   FormDestroy(Sender: TObject);
       Procedure   FormHide(Sender: TObject);
       Procedure   FormShow(Sender: TObject);
       Procedure   FreehandBtnClick(Sender: TObject);
       Procedure   ImageEditorUpdateEnabled(Sender: TObject);
       Procedure   RedoBtnClick(Sender: TObject);
       Procedure   UndoBtnClick(Sender: TObject);
    procedure FillStyleDropDownPaintBtn(Sender: TObject; ARect: TRect);
    procedure GeneralizeCheckClick(Sender: TObject);
    procedure SizeEditEnter(Sender: TObject);
    procedure SizeEditExit(Sender: TObject);
    procedure WidthEditChange(Sender: TObject);
      Private
       FOrgBitmapXFill  : TBitmapXFill;
       FBitmapXFill     : TBitmapXFill;
       FKeyPressed      : Boolean;
       FProjStyles      : TProjectStyles;
       FOrgProjStyles   : TProjectStyles;
       FFillDropDown    : TFillDropDownForm;
       Procedure   DoStyleSelect(Sender: TObject);
       Procedure   UpdateEnabled;
       Procedure   UpdatePixelSize;
      Public
       Property    BitmapXFill:TBitmapXFill read FBitmapXFill write FOrgBitmapXFill;
       Property    ProjectStyles:TProjectStyles read FProjStyles write FOrgProjStyles;
     end;

Implementation

{$R *.DFM}

Uses CommonResources,Dialogs,NumTools,SysUtils;

Procedure TBitmapXFillDialog.FreehandBtnClick(Sender: TObject);
begin
  ImageEditor.DrawMode:=dmFreehand;
end;

Procedure TBitmapXFillDialog.DrawLineBtnClick(Sender: TObject);
begin
  ImageEditor.DrawMode:=dmLine;
end;

Procedure TBitmapXFillDialog.DrawRectBtnClick(Sender: TObject);
begin
  ImageEditor.DrawMode:=dmFilledRect;
end;

Procedure TBitmapXFillDialog.DrawCircleBtnClick(Sender: TObject);
begin
  ImageEditor.DrawMode:=dmFilledEllipse;
end;

Procedure TBitmapXFillDialog.DrawColorBtnSelectColor(Sender: TObject);
begin
  ImageEditor.DrawColor:=DrawColorBtn.SelectedColor;
end;

Procedure TBitmapXFillDialog.FillColorBtnSelectColor(Sender: TObject);
begin
  ImageEditor.BackgroundColor:=FillColorBtn.SelectedColor;
end;

Procedure TBitmapXFillDialog.FormCreate(Sender: TObject);
begin
  FProjStyles:=TProjectStyles.Create;
  DrawColorBtn.Palette:=FProjStyles.Palette;
  FillColorBtn.Palette:=FProjStyles.Palette;
  FillColorBtn.Options.Options:=[soNone];
  FBitmapXFill:=TBitmapXFill.Create;
  ForeColorSelectBtn.Palette:=FProjStyles.Palette;
  SetupSelectOptions(ForeColorSelectBtn.Options,sotColor,[soLayer]);
  BackColorSelectBtn.Palette:=FProjStyles.Palette;
  SetupSelectOptions(BackColorSelectBtn.Options,sotColor,[soNone,soLayer]);
  // create fill-dropdown
  FFillDropDown:=TFillDropDownForm.Create(Self);
  FillStyleDropDown.DropDown:=FFillDropDown;
  FFillDropDown.OnSelectStyle:=DoStyleSelect;
end;

Procedure TBitmapXFillDialog.DoStyleSelect(Sender:TObject);
begin
  FillStyleDropDown.Invalidate;
  UpdateEnabled;
end;

Procedure TBitmapXFillDialog.FormDestroy(Sender: TObject);
begin
  FProjStyles.Free;
  FBitmapXFill.Free;
end;

Procedure TBitmapXFillDialog.FloodFillBtnClick(Sender: TObject);
begin
  ImageEditor.DrawMode:=dmFloodFill;
end;

Procedure TBitmapXFillDialog.UndoBtnClick(Sender: TObject);
begin
  ImageEditor.Undo;
end;

Procedure TBitmapXFillDialog.RedoBtnClick(Sender: TObject);
begin
  ImageEditor.Redo;
end;

Procedure TBitmapXFillDialog.FormShow(Sender: TObject);
begin
  if FOrgProjStyles<>NIL then FProjStyles.Assign(FOrgProjStyles);
  if FOrgBitmapXFill<>NIL then FBitmapXFill.Assign(FOrgBitmapXFill);
  with FBitmapXFill do begin
    GeneralizeCheck.Checked:=Generalize;
    GeneralizePrintCheck.Checked:=not GeneralizeWhenPrinting;
    ScaleVal.AsFloat:=GeneralizeScale;
    with GeneralizeStyle do begin
      ForeColorSelectBtn.SelectedColor:=ForeColor;
      BackColorSelectBtn.SelectedColor:=BackColor;
      FFillDropDown.Style:=Pattern;
    end;
    NameVal.AsText:=FBitmapXFill.Name;
    if FBitmapXFill.DIBBitmap.Width=0 then begin
      ImageEditor.Bitmap.Width:=8;
      ImageEditor.Bitmap.Height:=8;
      ImageEditor.Bitmap.PixelFormat:=pf24bit;
      ImageEditor.Bitmap.HandleType:=bmDIB;
    end
    else ImageEditor.Bitmap:=FBitmapXFill.DIBBitmap;
    WidthVal.AsInteger:=ImageEditor.Bitmap.Width;
    HeightVal.AsInteger:=ImageEditor.Bitmap.Height;
    UpdatePixelSize;
    ImageEditorUpdateEnabled(Self);
  end;
  UpdateEnabled;                           
end;

Procedure TBitmapXFillDialog.FormHide(Sender: TObject);
var NewStyle       : TFillStyle;
    XFillStyle     : TCustomXFill;     
begin
  if ModalResult=mrOK then begin
    with FBitmapXFill do begin
      Generalize:=GeneralizeCheck.Checked;
      GeneralizeWhenPrinting:=not GeneralizePrintCheck.Checked;
      GeneralizeScale:=ScaleVal.AsFloat;
      NewStyle:=GeneralizeStyle;
      NewStyle.ForeColor:=ForeColorSelectBtn.SelectedColor;
      NewStyle.BackColor:=BackColorSelectBtn.SelectedColor;
      NewStyle.Pattern:=FFillDropDown.Style;
      GeneralizeStyle:=NewStyle;
      Name:=NameVal.AsText;
      DIBBitmap:=ImageEditor.Bitmap;
    end;
    if FOrgBitmapXFill<>NIL then FOrgBitmapXFill.Assign(FBitmapXFill);
    if FOrgProjStyles<>NIL then begin
      if FOrgBitmapXFill<>NIL then begin
        XFillStyle:=FProjStyles.XFillStyles[FOrgBitmapXFill.Number];
        if XFillStyle<>NIL then XFillStyle.Assign(FBitmapXFill);
      end;
      FOrgProjStyles.Assign(FProjStyles);
    end;
  end;
end;

Procedure TBitmapXFillDialog.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
var Item           : TCustomXFill;
begin
  CanClose:=FALSE;
  if ModalResult<>mrOK then CanClose:=TRUE
  else if CheckValidators(Self) then begin
    Item:=FProjStyles.XFillStyles.NamedStyles[NameVal.AsText];
    if (Item<>NIL) and (Item.Number<>FBitmapXFill.Number) then begin
      MessageDialog(Format(MlgSection[20],[NameVal.AsText]),mtError,[mbOK],0);
      CanClose:=FALSE;
    end
    else CanClose:=TRUE;
  end;
end;

Procedure TBitmapXFillDialog.ImageEditorUpdateEnabled(Sender: TObject);
begin
  UndoBtn.Enabled:=ImageEditor.UndoList.Count>0;
  RedoBtn.Enabled:=ImageEditor.RedoList.Count>0;
  FillColorBtn.Enabled:=ImageEditor.DrawMode in [dmFilledRect,dmFilledEllipse];
end;

Procedure TBitmapXFillDialog.UpdateEnabled;
begin
  SetControlEnabled(ForeColorSelectBtn,FFillDropDown.Style>0);
  SetControlEnabled(BackColorSelectBtn,(FFillDropDown.Style>0) and (FFillDropDown.Style<7));
  SetControlGroupEnabled(Group1,GeneralizeCheck.Checked);
end;

Procedure TBitmapXFillDialog.FillStyleDropDownPaintBtn(Sender: TObject;
  ARect: TRect);
var AText          : String;  
begin
  with FillStyleDropDown.Canvas do begin
    if FFillDropDown.Style<=0 then begin
      AText:=MlgStringList['SelectionNames',40];
      TextRect(ARect,ARect.Left,CenterHeightInRect(TextHeight(AText),ARect),AText);
    end
    else begin
      CommonListBitmaps.Draw(Canvas,ARect.Left+2,CenterHeightInRect(14,ARect),clbSystem);
      case FFillDropDown.Style of
        pt_FDiagonal  : Brush.Style:=bsFDiagonal;
        pt_Cross      : Brush.Style:=bsCross;
        pt_DiagCross  : Brush.Style:=bsDiagCross;
        pt_BDiagonal  : Brush.Style:=bsBDiagonal;
        pt_Horizontal : Brush.Style:=bsHorizontal;
        pt_Vertical   : Brush.Style:=bsVertical;
        pt_Solid      : Brush.Style:=bsSolid;
      end;
      if GeneralizeCheck.Checked then Brush.Color:=clBlack
      else Brush.Color:=clGray;
      SetBKColor(Handle,ColorToRGB(clBtnFace));
      SetBKMode(Handle,Opaque);
      Pen.Style:=psClear;
      with ARect do Rectangle(Left,Top,Right+1,Bottom+1);
    end;
  end;
end;

Procedure TBitmapXFillDialog.GeneralizeCheckClick(Sender: TObject);
begin
  UpdateEnabled;
end;

procedure TBitmapXFillDialog.SizeEditEnter(Sender: TObject);
begin
  FKeyPressed:=FALSE;
end;

procedure TBitmapXFillDialog.SizeEditExit(Sender: TObject);
begin
  if FKeyPressed then begin
    ImageEditor.BitmapSize:=Point(WidthVal.AsInteger,HeightVal.AsInteger);
    UpdatePixelSize;
  end;
end;

procedure TBitmapXFillDialog.WidthEditChange(Sender: TObject);
begin
  FKeyPressed:=TRUE;
end;

procedure TBitmapXFillDialog.UpdatePixelSize;
begin
  ImageEditor.PixelSize:=Min(ImageEditor.Width Div WidthVal.AsInteger,
      Min(ImageEditor.Height Div HeightVal.AsInteger,14));
end;

end.
