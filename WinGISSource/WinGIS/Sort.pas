unit Sort;

interface
uses SlBase,classes;

Type TMergeSort = class
      private
      FList : TSLBase;
      Drain : TSLBaseItem;
      public
       Constructor Create(AList : TSLBase);
       Destructor Destroy; override;
       Procedure MergeSort;
       Function Merge(p,q:TSLBaseItem):TSLBaseItem;
     end;

implementation
uses Area;

Constructor TMergeSort.Create
   (
   AList :TSLBase
   );
  var Ref     : TSLBaseItemRef;
      AObject : TSLBaseItem;
  begin
    inherited Create;
    {$IFDEF DEBUG}
    if AList.count=0 then Raise EListError.Create('Liste Leer');
    {$ENDIF}
    FList := AList;
    Ref:=TSLBaseItemREf(FList.GetHead.ClassType);
    AObject:=Ref.Init;   {AufRuf des virtuellen Constructors}
    FList.AddTail(AObject);
    AObject:=Ref.Init;
    FList.AddHead(AObject);
    Drain :=FList.GetTail.next;
  end;

Destructor TMergeSort.Destroy;
  begin
    FList.DeleteHead;
    FList.DeleteTail;
    inherited Destroy;
  end;

Procedure TMergeSort.MergeSort;
  var c          : TSLBaseItem;
      a          : TSLBaseItem;
      b          : TSLBaseItem;
      todo       : TSLBaseItem;
      t          : TSLBaseItem;
      i          : longint;
      N          : longint;
  begin
    N:=1;
    c:=FList.GetNext(FList.GetHead);
    if c<>Drain then begin
      repeat
        todo:=FList.GetHead.next;
        c:=FList.Gethead;
        repeat
           t:=todo;
           a:=t;
           for i:=1 to N-1 do t:=t.next;
           b:=t.next;
           t.next:=Drain;
           t:=b;
           for i:=1 to N-1 do t:=t.next;
           todo:=t.next;
           t.next:=Drain;
           c.next:=Merge(a,b);
           for i:=1 to N+N do c:=c.next;
         until todo = Drain;
        N:=N+N;
      until a=FList.GetHead.next;
      c:=FList.GetHead.next;
    end;
    t:=FList.GetHead;
    while t.next <> Drain do t:=t.next;
    FList.SetTail(t);
  end;

Function TMergeSort.Merge
   (
   p: TSLBaseItem;
   q: TSLBaseItem
   )
   :TSLBaseItem;
  var s       : TSLBaseItem;
      Compare : Integer;      {TEdge(TEdgeListItem(p).PtEdge).x}
  begin
    {$IFDEF DEBUG}
    if (p=Drain)and(q=Drain) then Raise EListError.Create('Merge, Liste Leer');
    {$ENDIF}
    Compare:=TEdgeListItem(p).OnCompareItems(TEdgeListItem(p).KeyOfX(TEdgeListItem(p)),
      TEdgeListItem(p).KeyOfX(TEdgeListItem(q)));
    if Compare =-1 then begin
      Result:=p;
      p:=p.next;
    end
    else if Compare = 1 then begin
      Result:=q;
      q:=q.next;
    end
    else if Compare =0 then begin
      Compare:=TEdgeListItem(p).OnCompareItems(TEdgeListItem(p).KeyOfY(TEdgeListItem(p)),
        TEdgeListItem(p).KeyOfY(TEdgeListItem(q)));
      {wenn virtual: TEdgeListItem weglassen!}
      if (Compare = 0) or (Compare=-1) then begin
        Result:=p;
        p:=p.next;
      end
      else if Compare = 1 then begin
        Result:=q;
        q:=q.next;
      end;
    end;
    s:=Result;
    while(p<>Drain)and(q<>Drain) do begin
      Compare:=TEdgeListItem(p).OnCompareItems(TEdgeListItem(p).KeyOfX(TEdgeListItem(p)),
        TEdgeListItem(p).KeyOfX(TEdgeListItem(q)));
      if Compare =-1 then begin
        s.next:=p;
        s:=p;
        p:=p.next;
      end
      else if Compare = 1 then begin
        s.next:=q;
        s:=q;
        q:=q.next;
      end
      else if Compare = 0 then begin
        Compare:=TEdgeListItem(p).OnCompareItems(TEdgeListItem(p).KeyOfY(TEdgeListItem(p)),
          TEdgeListItem(p).KeyOfY(TEdgeListItem(q)));
        if (Compare = 0) or (Compare=-1) then begin
          s.next:=p;
          s:=p;
          p:=p.next;
        end
        else if Compare = 1 then begin
          s.next:=q;
          s:=q;
          q:=q.next;
        end;
      end;
    end;{while}
    if p=Drain then s.next:=q
    else s.next:=p;
  end;


end.
