Unit BitmapOptionsDlg;

Interface

Uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, WCtrls, ExtCtrls, Spinbtn, AM_Def, Validate, MultiLng;

Type TBitmapOptionsDialog = class(TWForm)
       Bevel1: TBevel;
       OKBtn: TButton;
       CancelBtn: TButton;
    MinSizeVal: TMeasureValidator;
    TransparencyVal: TMeasureValidator;
    ControlPanel1: TPanel;
    ControlPanel: TPanel;
    Group2: TWGroupBox;
    WLabel1: TWLabel;
    BlackTransparencyBtn: TRadioButton;
    WhiteTransparencyBtn: TRadioButton;
    TransparencyEdit: TEdit;
    TransparencySpin: TSpinBtn;
    Group1: TWGroupBox;
    ShowPictureCheck: TCheckBox;
    ShowFrameCheck: TCheckBox;
    UseGlobalCheck: TCheckBox;
    MlgSection: TMlgSection;
    Label1: TWLabel;
    MinSizeEdit: TEdit;
    MinSizeSpin: TSpinBtn;
    ChkImgGlobInvisible: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure UseGlobalCheckClick(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
      Private
       FOrgSettings: PBitmapSettings;
       FSettings   : TBitmapSettings;
       Function    GetSettings:PBitmapSettings;
      Public
       Property    BitmapSettings:PBitmapSettings read GetSettings write FOrgSettings;
     end;

Implementation

{$R *.DFM}

Uses BMPImage,Measures;

Function TBitmapOptionsDialog.GetSettings
 : PBitmapSettings;
begin
  Result:=@FSettings;
end;   

procedure TBitmapOptionsDialog.FormShow(Sender: TObject);
begin
  if FOrgSettings<>NIL then FSettings:=FOrgSettings^;
  with FSettings do begin
    UseGlobalCheck.Checked:=UseGlobals;
    if UseGlobals then UseGlobalCheckClick(UseGlobalCheck);
    ShowPictureCheck.Checked:=ShowBitmap;
    ShowFrameCheck.Checked:=ShowFrame;
    MinSizeVal[muMillimeters]:=ShowMinSize/10;
    TransparencyVal[muPercent]:=Transparency;
    if TransparencyType=tt_Black then BlackTransparencyBtn.Checked:=TRUE
    else WhiteTransparencyBtn.Checked:=TRUE;
//++ Glukhov Bug#236 Build#144 04.12.00
    ChkImgGlobInvisible.Checked:= GlobInvisiblity;
//-- Glukhov Bug#236 Build#144 04.12.00
  end;
end;

procedure TBitmapOptionsDialog.UseGlobalCheckClick(Sender: TObject);
begin
  SetControlGroupEnabled(ControlPanel,not UseGlobalCheck.Checked);
end;

procedure TBitmapOptionsDialog.FormHide(Sender: TObject);
begin
  if ModalResult=mrOK then with FSettings do begin
    UseGlobals:=UseGlobalCheck.Checked;
    ShowBitmap:=ShowPictureCheck.Checked;
    ShowFrame:=ShowFrameCheck.Checked;
    ShowMinSize:=Round(MinSizeVal[muMillimeters]*10);
    Transparency:=Round(TransparencyVal[muPercent]);
    if BlackTransparencyBtn.Checked then TransparencyType:=tt_Black
    else TransparencyType:=tt_White;
//++ Glukhov Bug#236 Build#144 04.12.00
    GlobInvisiblity:= ChkImgGlobInvisible.Checked;
//-- Glukhov Bug#236 Build#144 04.12.00
    if FOrgSettings<>NIL then FOrgSettings^:=FSettings;
  end;
end;

procedure TBitmapOptionsDialog.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  CanClose:=CheckValidators(Self);
end;

end.
