�
 TTXTADVDIALOG 0  TPF0TTxtAdvDialogTxtAdvDialogLeftDTopBorderStylebsDialogCaption!1ClientHeight� ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrder	PositionpoScreenCenterOnCloseQueryFormCloseQueryOnCreate
FormCreate	OnDestroyFormDestroyOnHideFormHideOnShowFormShowPixelsPerInch`
TextHeight TBevelBevel1LeftTop� WidthyHeightAnchorsakLeftakBottom ShapebsBottomLine  TButton	CancelBtnLeft(Top� WidthYHeightAnchorsakLeftakBottom Cancel	Caption!3ModalResultTabOrder  TButtonOKBtnLeft� Top� WidthYHeightAnchorsakLeftakBottom Caption!2Default	ModalResultTabOrder  TPanelControlPanel1Left Top�Width�Height� AnchorsakLeftakTopakBottom 
BevelOuterbvNoneTabOrder  TPanelControlPanelLeft TopWidth�Height� AnchorsakLeftakTopakBottom 
BevelOuterbvNoneTabOrder  TWLabellblStateLeft2TopWidth� Height	AlignmenttaRightJustifyAutoSizeCaptionStatus  
TWGroupBoxgrpSpaceLeftTopWidth� HeightCCaption!9TabOrder BoxStyle	bsTopLine TWLabelWLabel1LeftTopWidthUHeightAutoSizeCaption!10FocusControl	edtHSpace  TWLabelWLabel3Left� TopWidthUHeight	AlignmenttaRightJustifyAutoSizeCaption!12FocusControl	edtVSpace  TWLabelWLabel2LeftTopWidthUHeight	AlignmenttaRightJustifyAutoSizeCaption!11FocusControl	edtHSpace  TEdit	edtHSpaceLeftTop(WidthKHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder OnChangeDataChanged  TSpinBtn
spinHSpaceLeft_Top(Height	ArrowkeysCtrlIncrement       ��?	Increment       ��?Max       �@ShiftIncrement       �@	Validator	valHSpace  TEdit	edtVSpaceLeft� Top(WidthKHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderOnChangeDataChanged  TSpinBtn
spinVSpaceLeft� Top(Height	ArrowkeysCtrlIncrement       ��?	Increment       ��?Max       �@ShiftIncrement       �@	Validator	valVSpace   
TWGroupBoxgrpPolyLeftTopXWidth{HeightmCaption!14TabOrderBoxStyle	bsTopLine TWLabelWLabel4LeftTopWidthhHeight	AlignmenttaRightJustifyAutoSizeCaption!15FocusControl	edtHShift  TWLabelWLabel5Left� Top,WidthUHeight	AlignmenttaRightJustifyAutoSizeCaption!12FocusControl	edtVShift  TWLabelWLabel6LeftTop,WidthUHeight	AlignmenttaRightJustifyAutoSizeCaption!11FocusControl	edtHShift  TWLabelWLabel7LeftTop WidthUHeightAutoSizeCaption!16FocusControl	edtHShift  TEdit	edtHShiftLeftTop:WidthKHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder OnChangeDataChanged  TSpinBtn
spinVShiftLeft� Top:Height	ArrowkeysCtrlIncrement       ��?	Increment       �@Max     (k�@Min     (k��ShiftIncrement      @�@	Validator	valVShift  TEdit	edtVShiftLeft� Top:WidthKHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderOnChangeDataChanged  TSpinBtn
spinHShiftLeft_Top:Height	ArrowkeysCtrlIncrement       ��?	Increment       �@Max     (k�@Min     (k��ShiftIncrement      @�@	Validator	valHShift  TEdit	edtOffsetLeft� TopWidthKHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderOnChangeDataChanged  TSpinBtn
spinOffsetLeft� TopHeight	ArrowkeysCtrlIncrement       ��?	Increment       ��?Max       �@ShiftIncrement       �@	Validator	valOffset  	TCheckBoxchkUnderLeftTop[WidthhHeightCaption!17TabOrderOnClickDataChanged  	TCheckBoxchkRedirLeftuTop[WidthjHeightCaption!18TabOrderOnClickDataChanged  TButton
btnSelPolyLeft� TopWidth}HeightCaption!24TabOrderOnClickbtnSelPolyClick  TButton
btnEntPolyLeft� Top&Width}HeightCaption!25TabOrder	OnClickbtnEntPolyClick   TPanelPanel1Left� TopWidth� HeightW
BevelOuterbvNoneTabOrder TButtonbtnOriginalLeftTopWidth}HeightCaption!21TabOrder OnClickbtnOriginalClick  TButton	btnSpacedLeftTopWidth}HeightCaption!22TabOrderOnClickbtnSpacedClick  TButtonbtnPolyModeLeftTop8Width}HeightCaption!23TabOrderOnClickbtnPolyModeClick     TMeasureLookupValidator	valHSpaceEdit	edtHSpaceUseableUnits @DefaultValue.Units	muPercentMinValue.Units	muPercentMaxValue.Units	muPercentMaxValue.Value       �@Units	muPercentLeft
Top�   TMlgSection
MlgSectionSectionTxtAdvDialogLeft� Top�   TMeasureLookupValidator	valVSpaceEdit	edtVSpaceUseableUnits @DefaultValue.Units	muPercentMinValue.Units	muPercentMaxValue.Units	muPercentMaxValue.Value       �@Units	muPercentLookupList.Stringsfdfgdfg LeftTop�   TMeasureLookupValidator	valOffsetEdit	edtOffsetUseableUnits @CommasDefaultValue.Units	muPercentMinValue.Units	muPercentMaxValue.Units	muPercentMaxValue.Value       �@Units	muPercentLookupList.Stringsfdfgdfg Left3Top�   TMeasureLookupValidator	valHShiftEdit	edtHShiftUseableUnits@DefaultValue.Units	muPercentMinValue.UnitsmuCentimetersMinValue.Value     (k��MaxValue.UnitsmuCentimetersMaxValue.Value     (k�@UnitsmuCentimetersLeftITop�   TMeasureLookupValidator	valVShiftEdit	edtVShiftUseableUnits@DefaultValue.Units	muPercentMinValue.UnitsmuCentimetersMinValue.Value     (k��MaxValue.UnitsmuCentimetersMaxValue.Value     (k�@UnitsmuCentimetersLeft[Top�    