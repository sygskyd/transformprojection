{ Load Points Coord from formatted text file with X,Y data.
  September-October 2001, Brovak Vladimir, Progis Russia
                        Release 1
   Release: 2
November 2001, Brovak Vladimir, Progis Russia - thru parent can read files with coordinates

   Release: 3
 December 2001 - Jan 2001. Now added Load various Symbols from file from various libraryes
 --------------------------------------------------------------------------------
}


unit PointsCoordDlg1;

interface

uses
  Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
  StdCtrls,ComCtrls,Spinbtn,Validate,VerMgr,Am_Ini,MultiLng, Menus;

type
  TPointsCoordDialog1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    CheckSpecial: TCheckBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    DelimEdit: TEdit;
    OpenSpecial: TEdit;
    CloseSpecial: TEdit;
    Label5: TLabel;
    FileNameEdit: TEdit;
    Button3: TButton;
    ReadFromEdit: TEdit;
    GroupBox1: TGroupBox;
    Label6: TLabel;
    KolonkaX: TEdit;
    KoeffEditX: TEdit;
    CheckX: TCheckBox;
    GroupBox2: TGroupBox;
    Label7: TLabel;
    KolonkaY: TEdit;
    KoeffEditY: TEdit;
    CheckY: TCheckBox;
    SpinBtn1: TSpinBtn;
    SpinBtn2: TSpinBtn;
    SpinBtn3: TSpinBtn;
    SaveAsButton: TButton;
    OpenDialog1: TOpenDialog;
    IntValidator1: TIntValidator;
    IntValidator2: TIntValidator;
    IntValidator3: TIntValidator;
    FloatValidator1: TFloatValidator;
    FloatValidator2: TFloatValidator;
    FileValidator1: TFileValidator;
    Label8: TLabel;
    ComboBox1: TComboBox;
    PopupMenu1: TPopupMenu;
    TAB1: TMenuItem;
    SPACE1: TMenuItem;
    N1: TMenuItem;
    MlgSection: TMlgSection;
    AddBtn: TButton;
    CheckBox1: TCheckBox;
    procedure CheckXClick(Sender: TObject);
    procedure CheckYClick(Sender: TObject);
    procedure CheckSpecialClick(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure LoadAndSetCoordPointsParameters;
    procedure ResetElements;
    procedure FormCreate(Sender: TObject);
    procedure SaveAsButtonClick(Sender: TObject);
    procedure ChangeData;
    procedure DelimEditExit(Sender: TObject);
    procedure TAB1Click(Sender: TObject);
    procedure SPACE1Click(Sender: TObject);
    procedure AddBtnClick(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
  private
    { Private declarations }
  public
  FlagUseAdditional:boolean;

  SSymbolName :String;
  SSLibName:string;
  SSumNum:integer;
  SAllSymCheck:boolean;
  SSymColumn:integer;
  SContainName:boolean;
  SUsespec:boolean;
  SOpenspec:string;
  SClosespec:string;
  SLibIndex:integer;
//  SSize:
//  SSizeType
//  SAngleT
   SIndepenent:boolean;
    { Public declarations }
  end;

var
  PointsCoordDialog1: TPointsCoordDialog1;


implementation

{$R *.DFM}

uses PointsCoordDlg3,Am_main,am_proj;
  var Z3:TPointsCoordDialog3;

procedure TPointsCoordDialog1.LoadAndSetCoordPointsParameters;
begin;
  with IniFile.LoadPointsCoordDataFromFile do
  begin;
    IntValidator1.AsInteger := ReadFromRow;
    DelimEdit.Text := BaseDelimeter;
    CheckSpecial.Checked := DataIntoDelimeters;
    OpenSpecial.Text := Delleft;
    CloseSpecial.Text := Delright;
    IntValidator3.AsInteger := XColumn;
    IntValidator2.AsInteger := YColumn;
    CheckX.Checked := KoeffX;
    FloatValidator1.AsFloat := KoeffDataX;
    CheckY.Checked := KoeffY;
    FloatValidator2.AsFloat := KoeffDataY;
    Combobox1.Items.Add(Mlgsection[20]);
    Combobox1.Items.Add(Mlgsection[21]);
    Combobox1.Items.Add(Mlgsection[22]);
    Combobox1.ItemIndex := LoadMode - 1;
  end;
  ResetElements;
end;

procedure TPointsCoordDialog1.ResetElements;
begin;

  if CheckSpecial.Checked then
  begin;
    OpenSpecial.Enabled := true;
    OpenSpecial.Color := clWindow;
    CloseSpecial.Enabled := true;
    CloseSpecial.Color := clWindow;
  end
  else
  begin;
    OpenSpecial.Enabled := false;
    OpenSpecial.Color := clBtnFace;
    CloseSpecial.Enabled := false;
    CloseSpecial.Color := clBtnFace;
  end;

  KoeffEditX.Enabled := CheckX.Checked;

  if CheckX.Checked then
  begin;
    KoeffEditX.Color := clWindow;
    FloatValidator1.Edit := KoeffEditX
  end
  else
  begin;
    KoeffEditX.Color := clBtnFace;
    FloatValidator1.Edit := nil;
  end;

  KoeffEditY.Enabled := CheckY.Checked;
  if CheckY.Checked then
  begin;
    KoeffEditY.Color := clWindow;
    FloatValidator2.Edit := KoeffEditY
  end
  else
  begin;
    KoeffEditY.Color := clBtnFace;
    FloatValidator2.Edit := nil;
  end;
end;

procedure TPointsCoordDialog1.CheckXClick(Sender: TObject);
begin
  ResetElements;
end;

procedure TPointsCoordDialog1.CheckYClick(Sender: TObject);
begin
  ResetElements
end;

procedure TPointsCoordDialog1.CheckSpecialClick(Sender: TObject);
begin
  ResetElements
end;

procedure TPointsCoordDialog1.Button3Click(Sender: TObject);
begin
  if OpenDialog1.Execute = true then
    FileNameEdit.Text := ExpandFileName(OpenDialog1.FileName);
end;

procedure TPointsCoordDialog1.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  CanClose := CheckValidators(Self);
  if Uppercase(DelimEdit.Text) = 'TAB' then DelimEdit.Text := chr(9);
  if Uppercase(DelimEdit.Text) = 'SPACE' then DelimEdit.Text := ' ';
  if CanClose=true
   then
    begin;
    if Z3<>nil then
    begin;
       Z3.Free;
       Z3:=nil;
    end;
    end;
end;

procedure TPointsCoordDialog1.FormCreate(Sender: TObject);
begin
  LoadAndSetCoordPointsParameters;
  if DelimEdit.Text = chr(9) then DelimEdit.Text := 'TAB';
  if DelimEdit.Text = ' ' then DelimEdit.Text := 'SPACE';
  Z3 := TPointsCoordDialog3.Create(Self);
  with Z3 do begin;
   SSymbolName:= PProj(WingisMainForm.ActualProj).ActualSym.GetName;   //FSymbol.GetName;
   SSLibName:= PProj(WingisMainForm.ActualProj).ActSymLib;
   SSumNum:=Z3.IntValidator1.AsInteger;
   SAllSymCheck:= AllSymbolsCheck.Checked;
   SSymColumn:=Z3.IntValidator2.AsInteger;
   if Combobox1.ItemIndex=0 then
   SContainName:=true else
   SContainName:=false;
   SUsespec:=CheckSpecial.Checked;
   SOpenspec:=OpenSpecial.Text;
   SClosespec:=CloseSpecial.Text;
  // SLibIndex:=ASymLibInfo.LibIndex;
              end;
  Z3.close;
  FlagUseAdditional:=false;
end;

procedure TPointsCoordDialog1.SaveAsButtonClick(Sender: TObject);
begin;
  ChangeData;
  IniFile.WriteDataForLoadPoint;
end;

procedure TPointsCoordDialog1.ChangeData;
begin;
  with IniFile.LoadPointsCoordDataFromFile do
  begin;
    ReadFromRow := IntValidator1.AsInteger;
    BaseDelimeter := DelimEdit.Text;
    DataIntoDelimeters := CheckSpecial.Checked;
    Delleft := OpenSpecial.Text;
    Delright := CloseSpecial.Text;
    XColumn := IntValidator3.AsInteger;
    YColumn := IntValidator2.AsInteger;
    KoeffX := CheckX.Checked;
    KoeffDataX := FloatValidator1.AsFloat;
    KoeffY := CheckY.Checked;
    KoeffDataY := FloatValidator2.AsFloat;
    LoadMode := Combobox1.ItemIndex + 1;
  end;
end;

procedure TPointsCoordDialog1.DelimEditExit(Sender: TObject);
begin
  if DelimEdit.Text = ',' then
  begin;
    ShowMessage(Mlgsection[19] + '!');
    DelimEdit.SetFocus;
  end;
end;

procedure TPointsCoordDialog1.TAB1Click(Sender: TObject);
begin
  DelimEdit.Text := 'TAB';
end;

procedure TPointsCoordDialog1.SPACE1Click(Sender: TObject);
begin
  DelimEdit.Text := 'SPACE';
end;

procedure TPointsCoordDialog1.AddBtnClick(Sender: TObject);

begin
if Z3.ShowModal=mrOK
 then
  begin;
     FlagUseAdditional:=true; //flag - use special
  with Z3 do
   begin;

    SSymbolName:=FSymbol.GetName;
    SSLibName:=Fsymbol.GetLibName;
    SSumNum:=Z3.IntValidator1.AsInteger;
    SAllSymCheck:= AllSymbolsCheck.Checked;
    SSymColumn:=Z3.IntValidator2.AsInteger;
    if Combobox1.ItemIndex=0 then
    SContainName:=true else
    SContainName:=false;
    SUsespec:=CheckSpecial.Checked;
    SOpenspec:=OpenSpecial.Text;
    SClosespec:=CloseSpecial.Text;
    SLibIndex:=ASymLibInfo.LibIndex;

   end;

  end;
end;

procedure TPointsCoordDialog1.CheckBox1Click(Sender: TObject);
begin
AddBtn.Visible:= CheckBox1.Checked;
if AddBtn.Visible then AddBtn.SetFocus;
end;

end.

