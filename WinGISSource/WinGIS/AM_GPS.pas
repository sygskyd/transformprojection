{*******************************************************************************
* AM_GPS                                                                       *
********************************************************************************
* Unit f�r die GPS Anbindung via DDE                                           *
*                                                                              *
* Autor : Raimund Leitner                 Datum : 19.04.1995                   *
*         Bruno Mariacher                         24.09.1997                   *
*                                                                              *
********************************************************************************}

Unit AM_GPS;

Interface

Uses Messages,Objects,Windows,AM_Def,SysUtils,AM_View,Am_Point,AM_Proj,AM_Sym,
     AM_Layer,AM_Index
     {$IFNDEF WMLT}
     ,AM_DDE
     {$ENDIF}
     ,AM_Poly,AM_Ini,AM_StdDl,AM_Admin,AM_CPoly,
     AM_Projp,AM_Obj,IniFiles,Transfor,AM_Group,AM_Offse,Dialogs,
     AxGisPro_TLB;

Const GPS1_Connect      =  150;    { ID des Connect-Buttons im GPS1 Dialog  }
      GPS1_Georef       =  151;    { ID des Buttons f�r die Georeferenzierung im GPSOpt1 Dialog}
      GPS1_online       =  152;    { ID des Buttons f�r den Onlinebetrieb im GPSOpt1 Dialog}
      GPS1_Database     =  153;    { ID des Buttons f�r die Datenbankeinstellungen }

      GPSRef1_X         =  105;    { ID des Feldes f�r den X-Offset }
      GPSRef1_Y         =  106;    { ID des Feldes f�r den Y-Offset }
      GPSRef1_Meridian  =  111;    { ID des Feldes f�r den Meridian }
      GPSRef1_OK        =    1;    { ID des OK-Buttons }
      GPSRef1_Abbruch   =    2;    { ID des Abbruchbuttons }
      GPSRef1_Affine    =  158;    { ID des Buttons f�r die Tranformationsparameter }
      GPSRef1_IDGK      =  103;    { ID des Gau�-Kr�ger Radiobuttons }
      GPSRef1_IDUTM     =  104;    { ID des UTM Radiobuttons }
      GPSRef1_IDAffin   =  102;    { ID des Affinradiobuttons }
      GPSRef1_laden     =  155;    { ID des Ladenbuttons im GPSRef1 Dialog }
      GPSRef1_speichern =  156;    { ID des Speichernbuttons im GPSRef1 Dialog }

      GPSDB1_Listbox    =  103;    { ID der Listbox in GPSDB1 Dialog }
      GPSDB1_plus       =   52;    { ID des Plusbuttons im GPSDB1 Dialog  }
      GPSDB1_minus      =   53;    { ID des Minusbuttons im GPSDB1 Dialog }
      GPSDB1_Laden      =  155;    { ID des Ladenbuttons im GPSDB1 Dialog }
      GPSDB1_Speichern  =  156;    { ID des Speichernbuttons im GPSDB1 Dialog }

      Obj_Punkt         =  000;
      Obj_Flaeche       =  002;
      Obj_Linie         =  001;
      Obj_Symbol        =  003;
      Obj_Objekt        =  099;

      Typ_permanent     =  000;
      Typ_aktuell       =  001;

      NONE              =  000;
      AFFIN             =  001;
      COORDDLL          =  002;

      GPS_online        =  000;
      GPS_offline       =  001;

      id_Text           =  106;

      Dir_STAND         =  000;
      Dir_UP            =  001;
      Dir_DOWN          =  002;
      Dir_LEFT          =  003;
      Dir_RIGHT         =  004;

      id_DB             =  109;
      DDEMemSize        = 1024;

Type  PGPSInfo    = ^GPSInfo;
      GPSInfo     = record
                      ID         : String;
                      Long       : Double;
                      Last_Long  : Double;
                      Lat        : Double;
                      Last_Lat   : Double;
                      sLong      : String;
                      sLat       : String;
                      N_S        : Boolean;
                      E_W        : Boolean;
                      Height     : String;
                      FIX_NOFIX  : String;
                      HDOP_PDOP  : String;
                      Typ        : String;
                      Date       : String;
                      Time       : String;
                      Sat        : String;
                      Alarm      : boolean;
                      Objektnummer: integer;       { F�r den Offlinebetrieb eine laufende Nummer }
                      Layerindex : longint;
                      Darstellung_Obj: integer;    { Bestimmung, ob Punkt, Symbol, Fl�che }
                      Darstellung_Typ: integer;    { Bestimmung, ob permanent oder aktuell }
                      ObjektIndex: longint;        { ID des Objekts merken f�r verschieben }
                      Autozoom   : boolean;        { Wenn true, wird Objekt mit Autozoom verfolgt }
                      Sendeformat: integer;        { Bestimmung, ob Objekt im On- bzw. Offline Modus gesendet wird }

                      SymbolIndex_stand: longint;  { Index des Symbols wenn das Symbol steht                  }
                      SymbolIndex_up   : longint;  { Index des Symbols wenn das Symbol in up-Richtung geht    }
                      SymbolIndex_down : longint;  { Index des Symbols wenn das Symbol in down-Richtung geht  }
                      SymbolIndex_left : longint;  { Index des Symbols wenn das Symbol in left-Richtung geht  }
                      SymbolIndex_right: longint;  { Index des Symbols wenn das Symbol in right-Richtung geht }

                      Last_Direction   : integer;  { Gibt die letzte Richtung des Symbols an }

                      Loesch_flag: boolean;        { Wenn man d�e Darstellungsart eines Objektes �ndert, muss
                                                     altes Symbol gel�scht werden }
                      New_created_Symbol:boolean;  { Gibt an das ein neues GPS-Symbol f�r Anzeigemodus aktuell kreiert wurde }
                      Zusatzinfo : pchar;          { Datenbankinformation welche sich im DDE-String befindet }
                    end;

      { Dynamische Liste in die GPS-Daten f�r die jeweiligen ID�s gespeichert sind }
      PGPS_Listeneintrag = ^GPS_Listeneintrag;
      GPS_Listeneintrag = record
         GPS_Listeneintrag_info :GPSInfo;
         next      :PGPS_Listeneintrag;
      end;

      PDBInfo = ^DBInfo;
      DBInfo = record
                      Spaltenname:string;
                      Typ        :string;
                      Laenge     :integer;
                      Position   :integer;
      end;

      PDB_Listeneintrag = ^DB_Listeneintrag;
      DB_Listeneintrag = record
         DB_Listeneintrag_info:DBInfo;
         next      :PDB_Listeneintrag;
      end;

      PGPS        = ^TGPS;
      TGPS        = object (TOldObject)
                      Handle          : HWnd;
                      GPS_HClient      : Word;
                      WaitCount        : Integer;
                      AktCon           : Boolean;
                      TheLine          : PPoly;
                      GPSProj          : PProj;
                      GPSactive        : Boolean;
                      DBImport         : Boolean;
                      GPS_Anfang       : PGPS_Listeneintrag;
                      GPS_Ende         : PGPS_Listeneintrag;
                      DB_Anfang        : PDB_Listeneintrag;
                      DB_Ende          : PDB_Listeneintrag;
                      GPS_Anzahl       : integer;
                      DB_Anzahl        : integer;
                      Last_Objektnummer: integer;
                      Last_Objekttyp   : integer;
                      Koordsystem      : integer;
                      Offset_X: Real;
                      Offset_Y: Real;
                      Meridian: integer;
                      Konvertierungstyp:integer;
                      Flaeche : pcpoly;
                      Linie   : ppoly;
                      IniFile : TIniFile;
                      Affine_Flag: boolean;
                      GK_Flag    : boolean;
                      Zoom_view  : TDRect; { Hier wird immer das Rectange der aktuellen Ansicht gespeichert }
                      Zoom_view_flag:boolean; { Wenn true muss Bildschirm neu aufgebaut werden }
                      Datenbankinfo:pchar;  { Hier wird immer der einzuf�gende Datenbankeintrag gespeichert }
                      GPFWindow  : boolean; { true wenn Online-Optionen Window aktiv, sonst false }
                      GDFWindow  : boolean; { true wenn Datenbank-Verbindungs Window aktiv, sonst false }
                      GRFWindow  : boolean; { true wenn Referenzierungs Window aktiv, sonst false }
                      Spalteninfo_flag:boolean; { Wenn true muss die Spaltendefinition zur Datenbank geschickt werden }

                      Offset_Linie  :integer; { Gibt des Typ der Offsetlinie an }
                      hOffset_Linie :integer; { Hilfsvariable f�r den Typ der Offsetlinie }
                      Offset_Ecke   :integer; { Gibt den Typ der Offsetecke an }
                      hOffset_Ecke  :integer; { Hilfsvariable der Offsetecke }
                      Offset_Abstand:longint; { Abstand der Offsetlinie zur Bezugslinie }
                      hOffset_Abstand:longint;{Hilfsvariable f�r den Offset_Abstand }
                      Offset_Flag   :boolean; { Wenn true -> setzen eines Offsetobjekts }
                      hOffset_Flag  :boolean; { Hilfsvariable f�r Offset_flag }
                      LastLayerindex :longint;

                      GPS_Batchmode  : boolean; { Variable gibt an ob GPS-Modul gerade im Batch arbeitet (GPSDATAFILE ) }

                      New_Line       :boolean; { Wenn true mu� im Online-Format eine neue Linie erzeugt werden }

                      AxGisProjection   :TAxGisProjection; // for projection calculation

                      Offsetres1       : ppoly; // Polys for line - offsets
                      Offsetres2       : ppoly;

                      MyParams: TTransformParam;
                      LongLatPoints,ProjectPoints :PCollection;
                      constructor Init;
                      destructor done; virtual;

                      procedure CreateGPS(AProj:PProj);
                      function GetAngle(X1:double;Y1:double;X2:double;Y2:double):double;
                      procedure GetGPSAngleDirection(X1:double;Y1:double;X2:double;Y2:double;
                                                     var Angle:real;var Direction:integer);
                      procedure GPS_DDEInitiate(Msg:TMessage);
                      procedure GPS_Execute(AProj:PProj;AData : PChar);
                      procedure GPS_Terminate(Msg:TMessage);
                      function  ParseGPSInfo(AGPSInfo:PGPSInfo;Data:PCHAR):Boolean;
                      procedure GPS_Ack;

                      function  Get_GPSPrioritaeten(AGPSInfo:PGPSInfo):boolean;
                      function  Change_GPSPrioritaeten(AGPSInfo:PGPSInfo; UpdatePosition:boolean):boolean;
                      function  Insert_GPSPrioritaeten(AGPSInfo:PGPSInfo):boolean;
                      function  Delete_GPSPrioritaeten(GPS_ID:string):boolean;
                      procedure Delete_AllPrioritaeten;
                      function  CheckLineanz:boolean;
                      procedure Delete_AllDBEinstellungen;

                      procedure LoadGRF(Dateiname:string);

                      function  Get_DBEinstellungen(ADBInfo:PDBInfo):boolean;
                      function  Insert_DBEinstellungen(ADBInfo:PDBInfo):boolean;
                      function  Delete_DBEinstellungen(DB_Spaltenname:string):boolean;

                      procedure Symbole_anpassen(GPS_ID:string);
                      procedure Datenbank_einfuegen(Zeile:pchar; Index:longint;Flache:real;Layer:string;Layerindex:longint);

                      function  Konvertierung(var Long:Double;var Lat:Double):boolean;
                      procedure DeleteIndexObject(AIndex          : LongInt);

                      procedure Delete_pchar(var Source:pchar;pos:integer;anz:integer);
                      procedure Copy_pchar(Source:pchar;pos:integer; anz:integer;var Dest:pchar);
                      function Pos_pchar(Zeichen:char;Source:pchar):integer;
                      procedure ParseMyPchar(Source:pchar;var res:string;Zeichen:char;pos:integer);

                      procedure GpsProASC(InASC:string;InADF:string;OutASC:string);
                      procedure ScanADF  (Dateiname:string;var X_von:integer; var X_bis:integer;
                                                           var Y_von:integer; var Y_bis:integer);
                      procedure CreateADF(InADF:string;OutASC:string;X_von_old:integer;X_bis_old:integer;
                                          Y_von_old:integer;Y_bis_old:integer);

      end;

var GPS    : TGPS;

Implementation

Uses AM_Main,Matrix,SymbolSelDlg,Math;

{ Externe Funktion in Coord.dll }
procedure CoordTransfLoLaToGK(lo,la: double;date_in:integer;
  var n_out,e_out:double;date_out:integer;zone_out:double); stdcall;// external 'Coord.DLL'  index 5;
  begin
  end;

procedure CoordTransfLoLaToUTM(lo,la: double;date_in:integer;
  var n_out,e_out:double;date_out:integer;zone_out:double); stdcall;// external 'Coord.DLL'  index 7;
  begin
  end;

{*******************************************************************************
* FUNKTION : TGPS.CheckLineanz                                                 *
********************************************************************************
* Funktion pr�ft ab, ob nicht schon eine Linie in den Onlineoptionen vorhanden *
* ist.                                                                         *
*                                                                              *
* RETURN:     TRUE  -> noch keine Linie vorhanden                              *
*             FALSE -> es ist bereits eine Linie vorhanden                     *
********************************************************************************}
function TGPS.CheckLineanz:boolean;
var Laufzeiger: PGPS_Listeneintrag;
    einfuegen : boolean;
begin
   Laufzeiger:=GPS_Anfang;
   einfuegen:=true;
   if GPS_Anfang <> NIL then
   repeat
      if Laufzeiger^.GPS_listeneintrag_info.Darstellung_Obj = Obj_Linie then einfuegen:=false;
      Laufzeiger:=Laufzeiger^.next;
   until (Laufzeiger = nil) or (einfuegen = false);
   Result:=einfuegen;
end;

{*******************************************************************************
* PROZEDUR : TGPS.Konvertierung                                                *
********************************************************************************
// Calculate Projection
// Input coordiantes are always carthesic: Long = X, Lat = Y
*                                                                              *
*                                                                              *
********************************************************************************}
function TGPS.Konvertierung(var Long:Double;var Lat:Double):boolean;
var h_Long,h_Lat:real;
    h_Lo:double;
    h_La:double;
    h_Meridian:double;
    NewX, NewY : Double;
    all_ok:boolean;
begin
   {$IFNDEF WMLT}
   h_Long:=Long; h_Lat:=Lat; h_Lo:=0; h_La:=0; all_ok:=true;

   if Konvertierungstyp = NONE then
   begin
      h_Lo:=Long;
      h_La:=Lat;
   end;

   if Konvertierungstyp = COORDDLL then
   begin
      h_Lo:=Long;
      h_La:=Lat;
      AxGisProjection.Calculate(h_Lo, h_La);
   end;

   if Konvertierungstyp = AFFIN then
   begin
      { Kontrolle, ob die Affinetransformation einmal korrekt verlassen wurde }
      if Affine_Flag = true then
      begin
         all_ok:=true;
         NewX:=0; NewY:=0;

         TransformCoords(Long, Lat, NewX, NewY, MyParams);

         Offset_X:=0; Offset_Y:=0;
         h_Lo:=(LimitToLong(NewX))/100;
         h_La:=(LimitToLong(NewY))/100;
      end
      else
      begin
         all_ok:=false; Lat:=0; Long:=0;
         MsgBox(WinGISMainForm.Handle,14044,mb_IconExclamation or mb_OK,'');
      end;
   end;

   { Wegen der Umrechnung in Meter mu� mit 100 multipliziert werden }
   Lat:=0; Long:=0;
   Long:=((h_Lo + Offset_X) * 100);
   Lat:=((h_La + Offset_Y) * 100);
   {$ENDIF}
   result:=all_ok;
end;

{*******************************************************************************
* PROZEDUR : TGPS.DeleteIndexObject                                            *
********************************************************************************
* Prozedur wird aufgerufen, um ein Objekt dessen ID man kennt zu loeschen.     *
*                                                                              *
*                                                                              *
*                                                                              *
********************************************************************************}
Procedure TGPS.DeleteIndexObject
(
   AIndex          : LongInt
);
var Index        : TIndex;
    Cnt          : Integer;
begin
   Index.Init(AIndex);
   with GPSProj^ do
   begin
      for Cnt:=0 to Layers^.LData^.Count-1 do
          PLayer(Layers^.LData^.At(Cnt))^.DeleteIndex(PInfo,@Index);
      PLayer(PInfo^.Objects)^.DeleteIndex(PInfo,@Index);
   end;
end;

{*******************************************************************************
* PROZEDUR : TGPS.LoadGRF                                                      *
********************************************************************************
* Prozedur dient dazu, dass das GPS-Referenzierung  auch via DDE-geladen werden*
* kann.                                                                        *
*                                                                              *
* Dateiname : Name der GRF-Datei, die geladen werden soll.                     *
*                                                                              *
* Returnwert:                                                                  *
*                                                                              *
********************************************************************************}
procedure TGPS.LoadGRF(Dateiname:string);
var GPS_Datei:Textfile;
    Zeile:string;
    Long_Flag,Project_Flag:boolean;
    RealPoint : PRealPoint;
    Point : PDPoint;
    count:integer;
    Weights:Pointer;
    h_Flag:boolean;
begin
   if Fileexists(Dateiname) then
   begin
      Assignfile(GPS_Datei,Dateiname);
      Reset(GPS_Datei);

      { Einlesen des Konvertierungstyps }
      Readln(GPS_Datei,Zeile); Konvertierungstyp:=strtoint(Zeile);
      if (Konvertierungstyp >= 0) then { Da jetzt sowieso nur noch Affine Transformationen geladen werden k�nnen }
      begin
         { Alte Transformationslisten l�schen }
         LongLatPoints^.FreeAll;
         ProjectPoints^.FreeAll;
         readln(GPS_Datei,Zeile); if (Zeile = '0') then h_Flag:=true else h_Flag:=false;

         repeat
            readln(GPS_Datei,Zeile);
            if (Zeile = '[LongLatPoints]') then begin readln(GPS_Datei,Zeile); Long_flag:=true; Project_flag:=false; end;
            if (Zeile = '[ProjectPoints]') then begin readln(GPS_Datei,Zeile); Project_flag:=true; Long_flag:=false end;

            if (Long_flag = true) then
            begin
               RealPoint:=New(PRealPoint,Init(0,0));
               val(Zeile,RealPoint^.X,count);
               Readln(GPS_Datei,Zeile); val(Zeile,RealPoint^.Y,count);
               LongLatPoints^.Insert(RealPoint);
            end;

            if (Project_flag = true) then
            begin
               Point:=New(PDPoint,Init(0,0));
               val(Zeile,Point^.X,count);
               Readln(GPS_Datei,Zeile); val(Zeile,Point^.Y,count);
               ProjectPoints^.Insert(Point);
            end;
         until EOF(GPS_Datei);

         Weights:=NIL;

         if not CalculateTransform(h_Flag,ProjectPoints,LongLatPoints,
             FALSE,MyParams,TMatrix(Weights),Handle) then Affine_Flag:=false
         else Affine_Flag:=true;
      end;
      Closefile(GPS_Datei);
   end;
end;

{*******************************************************************************
* PROZEDUR : TGPS.Delete_AllDBEinstellungen                                    *
********************************************************************************
* Prozedur l�scht alle Elemente, die sich zur Zeit in der GPS-Datenbank-       *
* verbindungsliste befinden.                                                   *
*                                                                              *
* Returnwert:                                                                  *
*                                                                              *
********************************************************************************}
procedure TGPS.Delete_AllDBEinstellungen;
var Laufzeiger1:PDB_Listeneintrag;
    Laufzeiger2:PDB_Listeneintrag;
begin
   Laufzeiger1:=DB_Anfang;
   if DB_Anfang <> NIL then
   repeat
      Laufzeiger2:=Laufzeiger1;
      Laufzeiger1:=Laufzeiger1^.next;
      dispose(Laufzeiger2);
   until Laufzeiger1 = nil;
   DB_Anzahl:=0; DB_Anfang:=nil; DB_Ende:=nil;
end;

{*******************************************************************************
* PROZEDUR : TGPS.Delete_AllPrioritaeten                                       *
********************************************************************************
* Prozedur l�scht alle Elemente, die sich zur Zeit in der GPS-Priorit�tenliste *
* befinden.                                                                    *
*                                                                              *
* Returnwert:                                                                  *
*                                                                              *
********************************************************************************}
procedure TGPS.Delete_AllPrioritaeten;
var Laufzeiger1:PGPS_Listeneintrag;
    Laufzeiger2:PGPS_Listeneintrag;
begin
   Laufzeiger1:=GPS_Anfang;
   if GPS_Anfang <> NIL then
   repeat
      Laufzeiger2:=Laufzeiger1;
      Laufzeiger1:=Laufzeiger1^.next;
      dispose(Laufzeiger2);
   until Laufzeiger1 = nil;
   GPS_Anzahl:=0;
   GPS_Anfang:=NIL; GPS_Ende:=NIL;
end;


{*******************************************************************************
* PROZEDUR : Destructor                                                        *
********************************************************************************
* Prozedur gibt des TGPSObjekt frei.                                           *
*                                                                              *
*                                                                              *
*                                                                              *
********************************************************************************}
Destructor TGPS.Done;
begin
   {$IFNDEF WMLT}
   AxGisProjection.Destroy;
   Dispose(LongLatPoints,Done);
   Dispose(ProjectPoints,Done);
   inherited Done;
   {$ENDIF}
end;

{*******************************************************************************
* PROZEDUR : TGPS.Init                                                         *
********************************************************************************
* Prozedur initialisiert ein TGPSObjekt                                        *
*                                                                              *
*                                                                              *
*                                                                              *
********************************************************************************}
constructor TGPS.Init;
var inivar:integer;
    initext:string;
begin
   {$IFNDEF WMLT}
   inherited Init;
   GPS_HClient:=0;
   TheLine:=nil;
   LastLayerindex:=0;
   GPS_Anfang:=nil;
   GPS_Ende:=nil;
   DB_Anfang:=nil;
   DB_Ende:=nil;
   GPS_Anzahl:=0;
   DB_Anzahl:=0;
   Konvertierungstyp:=NONE;
   Offset_X:=0;
   Offset_Y:=0;
   Handle:=WinGISMainForm.Handle;
   GPSactive:=FALSE;
   GPS_Batchmode:=FALSE;
   Last_Objektnummer:=-1;
   Last_Objekttyp:=-1;
   Affine_Flag:=false;
   GK_Flag:=false;
   LongLatPoints:=New(PCollection,Init(5,5));
   ProjectPoints:=New(PCollection,Init(5,5));
   Zoom_view_flag:=true;
   Spalteninfo_flag:=true;

   GPFWindow:=false;GRFWindow:=false; GDFWindow:=false;

   { Setzen der Linienoffsetparameter }
   Offset_Linie  :=1;
   hOffset_Linie:=Offset_Linie;
   Offset_Ecke   :=1;
   hOffset_Ecke:=Offset_Ecke;
   Offset_Abstand:=1;
   hOffset_Abstand:=Offset_Abstand;
   Offset_Flag:=false;
   hOffset_Flag:=Offset_Flag;
   New_Line:=true;
   AxGisProjection:=TAxGisProjection.Create(nil); // create projetion control
   Offsetres1:=nil;
   Offsetres2:=nil;
   {$ENDIF}
end;

{*******************************************************************************
* FUNKTION : TGPS.Delete_DBEinstellungen                                       *
********************************************************************************
* Funktion l�scht den mit DB_Spaltenname bestimmten Eintrag aus der            *
* Einstellungsliste.                                                           *
*                                                                              *
* GPS_ID  -> ID des zu l�schenden Eintrages.                                   *
*                                                                              *
* Returnwert: true  -> L�schen war erfolgreich.                                *
*             false -> L�schen war nicht erfolgreich.                          *
********************************************************************************}
function  TGPS.Delete_DBEinstellungen(DB_Spaltenname:string):boolean;
var Laufzeiger1:PDB_Listeneintrag;
    Laufzeiger2:PDB_Listeneintrag;
    gefunden   :boolean;
begin
   Laufzeiger1:=DB_Anfang;
   Laufzeiger2:=DB_Anfang;
   gefunden:=false;
   if DB_Anfang <> NIL then
   repeat
      if Laufzeiger1^.DB_listeneintrag_info.Spaltenname = DB_Spaltenname then gefunden:=true;
      if gefunden = false then
      begin
         Laufzeiger2:=Laufzeiger1;
         Laufzeiger1:=Laufzeiger1^.next;
      end;
   until (Laufzeiger1 = nil) or (gefunden = true);

   if (gefunden = true) then
   begin
      { L�schen des Eintrages aus der Liste }
      if (Laufzeiger1 = DB_Anfang) then DB_Anfang:=Laufzeiger1^.next;
      if (Laufzeiger1 = DB_Ende) then DB_Ende:=Laufzeiger2;

      if (Laufzeiger1 <> DB_Anfang) and (Laufzeiger1 <> DB_Ende) then
      begin
         Laufzeiger2^.next:=Laufzeiger1^.next;
      end;
      Dispose(Laufzeiger1);
      DB_Anzahl:=DB_Anzahl - 1;
   end;
   Result:=gefunden;
end;

{*******************************************************************************
* FUNKTION : TGPS.Delete_GPSPrioritaeten                                       *
********************************************************************************
* Prozedur l�scht den mit GPS_ID bestimmten Eintrag aus der Priorit�tenliste.  *
*                                                                              *
* GPS_ID  -> ID des zu l�schenden Eintrages.                                   *
*                                                                              *
* Returnwert: true  -> L�schen war erfolgreich.                                *
*             false -> L�schen war nicht erfolgreich.                          *
********************************************************************************}
function  TGPS.Delete_GPSPrioritaeten(GPS_ID:string):boolean;
var Laufzeiger1:PGPS_Listeneintrag;
    Laufzeiger2:PGPS_Listeneintrag;
    gefunden   :boolean;
begin
   Laufzeiger1:=GPS_Anfang;
   Laufzeiger2:=GPS_Anfang;
   gefunden:=false;
   if GPS_Anfang <> NIL then
   repeat
      if Laufzeiger1^.GPS_listeneintrag_info.ID = GPS_ID then gefunden:=true;
      if gefunden = false then
      begin
         Laufzeiger2:=Laufzeiger1;
         Laufzeiger1:=Laufzeiger1^.next;
      end;
   until (Laufzeiger1 = nil) or (gefunden = true);

   if (gefunden = true) then
   begin
      { L�schen des Eintrages aus der Liste }
      if (Laufzeiger1 = GPS_Anfang) then GPS_Anfang:=Laufzeiger1^.next;
      if (Laufzeiger1 = GPS_Ende) then GPS_Ende:=Laufzeiger2;

      if (Laufzeiger1 <> GPS_Anfang) and (Laufzeiger1 <> GPS_Ende) then
      begin
         Laufzeiger2^.next:=Laufzeiger1^.next;
      end;
      Dispose(Laufzeiger1);
      GPS_Anzahl:=GPS_Anzahl - 1;
   end;

   Result:=gefunden;
end;

{*******************************************************************************
* FUNKTION : TGPS.Insert_DBEinstellungen                                       *
********************************************************************************
* Prozedur f�gt einen neuen Eintrag in die Datenbankeinstellungsliste ein.     *
*                                                                              *
* ADBInfo  ->  Struktur in der die neuen DBEinstellungen sind.                 *
*                                                                              *
* Returnwert: true  -> Einf�gen war erfolgreich.                               *
*             false -> Einf�gen war nicht erfolgreich.                         *
********************************************************************************}
function  TGPS.Insert_DBEinstellungen(ADBInfo:PDBInfo):boolean;
var neuer_record: PDB_Listeneintrag;
    Laufzeiger  : PDB_Listeneintrag;
    gefunden    : boolean;
begin
   { Kontrolle, ob ID nicht schon vorhanden }
   Laufzeiger:=DB_Anfang;
   gefunden:=false;

   if DB_Anfang <> NIL then
   repeat
      if Laufzeiger^.DB_listeneintrag_info.Spaltenname = ADBInfo^.Spaltenname then gefunden:=true;
      if gefunden = false then Laufzeiger:=Laufzeiger^.next;
   until (Laufzeiger = nil) or (gefunden = true);

   if (gefunden = false) then
   begin
      { Einf�gen in die Liste immer am Ende }
      new(neuer_record);
      neuer_record^.DB_Listeneintrag_info:=ADBInfo^;
      if DB_Anfang = NIL then
      begin
         DB_Ende:= neuer_record;
         DB_Anfang:= neuer_record;
         neuer_record^.next:=NIL;
      end
      else
      begin
         DB_Ende^.next:=neuer_record;
         DB_Ende:=neuer_record;
         neuer_record^.next:=NIL;
      end;
      Result:=true;
   end
   else
      Result:=false;
end;



{*******************************************************************************
* FUNKTION : TGPS.Insert_GPSPrioritaeten                                       *
********************************************************************************
* Prozedur f�gt einen neuen Eintrag in die GPSPriorit�tenliste ein.            *
*                                                                              *
* AGPSInfo ->  Struktur in der die neuen GPSPriorit�ten sind.                  *
*                                                                              *
* Returnwert: true  -> Einf�gen war erfolgreich.                               *
*             false -> Einf�gen war nicht erfolgreich.                         *
********************************************************************************}
function  TGPS.Insert_GPSPrioritaeten(AGPSInfo:PGPSInfo):boolean;
var neuer_record: PGPS_Listeneintrag;
    Laufzeiger  : PGPS_Listeneintrag;
    gefunden    : boolean;
begin
   { Kontrolle, ob ID nicht schon vorhanden }
   Laufzeiger:=GPS_Anfang;
   gefunden:=false;

   if GPS_Anfang <> NIL then
   repeat
      if Laufzeiger^.GPS_listeneintrag_info.ID = AGPSInfo^.ID then gefunden:=true;
      if gefunden = false then Laufzeiger:=Laufzeiger^.next;
   until (Laufzeiger = nil) or (gefunden = true);

   if (gefunden = false) then
   begin
      { Einf�gen in die Liste immer am Ende }
      new(neuer_record);
      neuer_record^.GPS_Listeneintrag_info:=AGPSInfo^;
      if GPS_Anfang = NIL then
      begin
         GPS_Ende:= neuer_record;
         GPS_Anfang:= neuer_record;
         neuer_record^.next:=NIL;
      end
      else
      begin
         GPS_Ende^.next:=neuer_record;
         GPS_Ende:=neuer_record;
         neuer_record^.next:=NIL;
      end;
      Result:=true;
   end
   else
      Result:=false;
end;

{*******************************************************************************
* FUNKTION : TGPS.Change_GPSPrioritaeten                                       *
********************************************************************************
* �ndert in der Liste den mit PGPSInfo bestimmten Eintrag.                     *
*                                                                              *
* AGPSInfo -> Zeiger auf die Struktur in der die ge�nderten GPSPriorit�ten     *
*             sind.                                                            *
* UpdatePosition -> Flag that shows if Position of GPS-Priority should be      *
*                   changed                                                    *
*                                                                              *
* Returnwert: true  -> Daten waren in Liste vorhanden.                         *
*             false -> Kein Eintrag in Liste f�r diese ID                      *
********************************************************************************}
function  TGPS.Change_GPSPrioritaeten(AGPSInfo:PGPSInfo; UpdatePosition:boolean):boolean;
var Laufzeiger: PGPS_Listeneintrag;
    gefunden  : boolean;
begin
   Laufzeiger:=GPS_Anfang;
   gefunden:=false;

   if GPS_Anfang <> NIL then
   repeat
      if Laufzeiger^.GPS_listeneintrag_info.ID = AGPSInfo^.ID then gefunden:=true;
      if gefunden = false then Laufzeiger:=Laufzeiger^.next;
   until (Laufzeiger = nil) or (gefunden = true);

   if (gefunden = true) then
   begin
      {  Kopieren der Information aus der Liste in die AGPSInfo-Struktur }
      // only if the Darstellung_Obj or the the Layerindex
      // or the Darstellung Typ changes
      // the ObjectIndex has to be changed also
      if not UpdatePosition then // is only set in GPSUPD DDE-Command
      begin
         if (Laufzeiger^.GPS_Listeneintrag_info.Layerindex <> AGPSInfo^.Layerindex) or
            (Laufzeiger^.GPS_Listeneintrag_info.Darstellung_Obj <> AGPSInfo^.Darstellung_Obj) or
            (Laufzeiger^.GPS_Listeneintrag_info.Darstellung_Typ <> AGPSInfo^.Darstellung_Typ) then
            Laufzeiger^.GPS_Listeneintrag_info.Objektindex:=AGPSInfo^.Objektindex;
      end
      else
         Laufzeiger^.GPS_Listeneintrag_info.Objektindex:=AGPSInfo^.Objektindex;

      Laufzeiger^.GPS_Listeneintrag_info.Layerindex:=AGPSInfo^.Layerindex;
      Laufzeiger^.GPS_Listeneintrag_info.Darstellung_Obj:=AGPSInfo^.Darstellung_Obj;
      Laufzeiger^.GPS_Listeneintrag_info.Darstellung_Typ:=AGPSInfo^.Darstellung_Typ;
      Laufzeiger^.GPS_Listeneintrag_info.Autozoom:=AGPSInfo^.Autozoom;
      Laufzeiger^.GPS_Listeneintrag_info.Sendeformat:=AGPSInfo^.Sendeformat;
      Laufzeiger^.GPS_Listeneintrag_info.Symbolindex_stand:=AGPSInfo^.Symbolindex_stand;
      Laufzeiger^.GPS_Listeneintrag_info.Symbolindex_up:=AGPSInfo^.Symbolindex_up;
      Laufzeiger^.GPS_Listeneintrag_info.Symbolindex_down:=AGPSInfo^.Symbolindex_down;
      Laufzeiger^.GPS_Listeneintrag_info.Symbolindex_left:=AGPSInfo^.Symbolindex_left;
      Laufzeiger^.GPS_Listeneintrag_info.Symbolindex_right:=AGPSInfo^.Symbolindex_right;
      if UpdatePosition then
      begin
         Laufzeiger^.GPS_Listeneintrag_info.Last_Long:=AGPSInfo^.Last_Long;
         Laufzeiger^.GPS_Listeneintrag_info.Last_Lat:=AGPSInfo^.Last_Lat;
         Laufzeiger^.GPS_Listeneintrag_info.New_created_Symbol:=AGPSInfo^.New_created_Symbol;
      end;
   end;
   Result:=gefunden;
end;

{*******************************************************************************
* FUNKTION : TGPS.Get_DBEinstellungen                                          *
********************************************************************************
* Prozedur liefert die Datenbankeinstellungen mit der in der Struktur          *
* bestimmten Spaltenbezeichnung.                                               *
*                                                                              *
* ADBInfo ->  Zeiger auf die Struktur in der die Einstellungen gespeichert sind*
*                                                                              *
* Returnwert: true  -> Daten waren in Liste vorhanden.                         *
*             false -> Kein Eintrag in Liste f�r diese ID                      *
********************************************************************************}
function TGPS.Get_DBEinstellungen(ADBInfo:PDBInfo):boolean;
var Laufzeiger: PDB_Listeneintrag;
    gefunden  : boolean;
begin
   Laufzeiger:=DB_Anfang;
   gefunden:=false;

   if DB_Anfang <> NIL then
   repeat
      if Laufzeiger^.DB_listeneintrag_info.Spaltenname = ADBInfo^.Spaltenname then gefunden:=true;
      if gefunden = false then Laufzeiger:=Laufzeiger^.next;
   until (Laufzeiger = nil) or (gefunden = true);

   if (gefunden = true) then
   begin
      {  Kopieren der Information aus der Liste in die AGPSInfo-Struktur }
      ADBInfo^.Typ:=Laufzeiger^.DB_Listeneintrag_info.Typ;
      ADBInfo^.Laenge:=Laufzeiger^.DB_Listeneintrag_info.Laenge;
      ADBInfo^.Position:=Laufzeiger^.DB_Listeneintrag_info.Position;
   end;
   Result:=gefunden;
end;


{*******************************************************************************
* FUNKTION : TGPS.Get_GPSPrioritaeten                                          *
********************************************************************************
* Prozedur liefert die GPSPrioritaeten des mit der in der Struktur             *
* bestimmten ID.                                                               *
*                                                                              *
* AGPSInfo -> Zeiger auf die Struktur in der Priorit�ten gespeichert sind.     *
*                                                                              *
* Returnwert: true  -> Daten waren in Liste vorhanden.                         *
*             false -> Kein Eintrag in Liste f�r diese ID                      *
********************************************************************************}
function TGPS.Get_GPSPrioritaeten(AGPSInfo:PGPSInfo):boolean;
var Laufzeiger: PGPS_Listeneintrag;
    gefunden  : boolean;
begin
   Laufzeiger:=GPS_Anfang;
   gefunden:=false;

   if GPS_Anfang <> NIL then
   repeat
      if Laufzeiger^.GPS_listeneintrag_info.ID = AGPSInfo^.ID then gefunden:=true;
      if gefunden = false then Laufzeiger:=Laufzeiger^.next;
   until (Laufzeiger = nil) or (gefunden = true);

   if (gefunden = true) then
   begin
      {  Kopieren der Information aus der Liste in die AGPSInfo-Struktur }
      AGPSInfo^.Layerindex:=Laufzeiger^.GPS_Listeneintrag_info.Layerindex;
      AGPSInfo^.Darstellung_Obj:=Laufzeiger^.GPS_Listeneintrag_info.Darstellung_Obj;
      AGPSInfo^.Darstellung_Typ:=Laufzeiger^.GPS_Listeneintrag_info.Darstellung_Typ;
      AGPSInfo^.Objektindex:=Laufzeiger^.GPS_Listeneintrag_info.Objektindex;
      AGPSInfo^.Autozoom:=Laufzeiger^.GPS_Listeneintrag_info.Autozoom;
      AGPSInfo^.Symbolindex_stand:=Laufzeiger^.GPS_Listeneintrag_info.Symbolindex_stand;
      AGPSInfo^.Symbolindex_up:=Laufzeiger^.GPS_Listeneintrag_info.Symbolindex_up;
      AGPSInfo^.Symbolindex_down:=Laufzeiger^.GPS_Listeneintrag_info.Symbolindex_down;
      AGPSInfo^.Symbolindex_left:=Laufzeiger^.GPS_Listeneintrag_info.Symbolindex_left;
      AGPSInfo^.Symbolindex_right:=Laufzeiger^.GPS_Listeneintrag_info.Symbolindex_right;
      AGPSInfo^.Last_Long:=Laufzeiger^.GPS_Listeneintrag_info.Last_Long;
      AGPSInfo^.Last_Lat:=Laufzeiger^.GPS_Listeneintrag_info.Last_Lat;
      AGPSInfo^.New_created_Symbol:=Laufzeiger^.GPS_Listeneintrag_info.New_created_Symbol;
   end;
   Result:=gefunden;
end;

procedure TGPS.CreateGPS(AProj:PProj); { Prozedur wird aufgerufen um Modul mitzuteilen, dass er freigeschalten ist }
begin
   Zoom_view_flag:=false;
   GPSProj:=AProj;
   GPSActive:=true;
end;

procedure TGPS.GPS_Terminate(Msg:TMessage);
begin
   if Msg.wParam=GPS_HClient then SendMessage(GPS_HClient,wm_DDE_Terminate,Handle,0);
   GPS_HClient:=0;
end;

{*******************************************************************************
* PROZEDUR : TGPS.GPS_DDEInitiate                                              *
********************************************************************************
* Prozedur wird aufgerufen, wenn von einer 16-Bit Clientanwendung ein          *
* WM_DDE_INITIATE gesendet wird.                                               *
*                                                                              *
********************************************************************************}
procedure TGPS.GPS_DDEInitiate(Msg:TMessage);
var AppName      : Array[0..255] of Char;
    TopicName    : Array[0..255] of Char;
begin
   if GPSActive then
   begin
      GPS_HClient:=Msg.wParam;
      Handle:=WinGISMainForm.Handle;

      { Senden eines Acknowledge an die Clientanwendung um die das erhaltene Initiate zu best�tigen }
      SendMessage(GPS_HClient,wm_DDE_Ack,Handle,Msg.lParam);
   end;
end;


{*******************************************************************************
* PROZEDUR : TGPS.GPS_Ack                                                      *
********************************************************************************
* Prozedur dient dazu, einer 16-Bit Clientanwendung ein Acknowledge zu senden. *
*                                                                              *
********************************************************************************}
procedure TGPS.GPS_Ack;
begin
   if GPSactive then if GPS_HClient<>0 then SendMessage(GPS_HClient,wm_DDE_Ack,Handle,0);
end;

{*******************************************************************************
* FUNKTION : TGPS.ParseGPSInfo                                                 *
********************************************************************************
* Funktion zerlegt den via DDE-erhaltenen String und zerlegt ihn in ein GPSInfo*
* Objekt.                                                                      *
*                                                                              *
* AGPSInfo  ->  GPSInfo-Objekt in das der erhaltene String kopiert wird        *
* GPSData   ->  String, der via DDE erhalten wird.                             *
*                                                                              *
* R�ckgabe  ->  true, wenn DDE-String korrekt ist                              *
*           ->  false, wenn DDE-String nicht korrekt ist                       *
********************************************************************************}
function TGPS.ParseGPSInfo(AGPSInfo:PGPSInfo;Data:PCHAR):Boolean;
var Dummy : String;
    Code : Integer;
    h_Abstand:longint;
    MyGPSInfo   :PGPSInfo;
    MyPLayer    :PLayer;
    BIndex      :TIndex;
    MyDBInfo    :PDBInfo;
    GPSData,ID  :string;
    myData,hpchar,OptParams:pchar;
    h_int:integer;
    h_longint:longint;
    X,Y  :double;
    InASC,InADF,OutASC:string;
    from_System,from_Date,to_System,to_Date:string;
    Changed : Bool;
    GpsDataFile:Textfile;
    GpsDataFileLine:ansistring;
    GpsDataFilePtr:Pchar;
    GpsDataFileName:string;
begin
   {$IFNDEF WMLT}
   myData:=stralloc(strlen(Data)+1);
   strcopy(myData,Data);
   GPSData:=strpas(Data);

   { Abfrage, welches DDE-Kommando geschickt wurde }
   if copy(GPSData,1,7) = '[GPSDEL' then { Es soll eine GPS-ID gel�scht werden }
   begin
      Delete(GpsData,1,8); // delete [GPSDEL,
      ID:=copy(GpsData,1,Pos(']',GpsData)-1); // get the ID
      Delete_GPSPrioritaeten(ID); // remove the ID
      ParseGPSInfo:=FALSE;
   end;

   if copy(GPSData,1,7) = '[GPSUPD' then { Die GPS-ID soll upgedatet werden }
   begin
      delete_pchar(MyData,1,7); { Teilstring [GPSUPD aus dem DDE-String l�schen }
      ParseMyPchar(MyData,ID,DDEHandler.fddesepsign,1); { Herauslesen der ID }
      AGPSInfo^.ID:=ID;
      if Get_GPSPrioritaeten(AGPSInfo) then
      begin
         with AGPSInfo^ do
         begin
            ParseMyPchar(MyData,ID,DDEHandler.fddesepsign,1); { Herauslesen der ID }

            ParseMyPchar(MyData,Dummy,DDEHandler.fddesepsign,2); { Herauslesen des Objekttyps }
            val (Dummy,h_int,code);
            if (code =0) and
               ((h_int = 0) or (h_int = 1) or (h_int = 3)) then Darstellung_Obj:=h_int else Darstellung_Obj:=0;

            ParseMyPchar(MyData,Dummy,DDEHandler.fddesepsign,3); { Herauslesen des Darstellungstyps }
            val (Dummy,h_int,code);
            if (code =0) and
               ((h_int = 0) or (h_int = 1)) then Darstellung_Typ:=h_int else Darstellung_Typ:=0;

            ParseMyPchar(MyData,Dummy,DDEHandler.fddesepsign,4); { Herauslesen der Autozoomart }
            if (Dummy = 'T') then Autozoom:=True; if (Dummy = 'F') then Autozoom:=False;
            if not((Dummy = 'T') or (Dummy = 'F')) then Autozoom:=False;

            ParseMyPchar(MyData,Dummy,DDEHandler.fddesepsign,5); { Herauslesen des Layernamens }
            MyPLayer:=GPSProj^.Layers^.NameTolayer(Dummy);
            if MyPLayer <> nil then LayerIndex:=MyPLayer^.Index else LayerIndex:=GPSProj^.Layers^.Toplayer^.Index;

            ParseMyPchar(MyData,Dummy,DDEHandler.fddesepsign,6); { Herauslesen des Symbolindex_Stand }
            val (Dummy,h_longint,code); if (code =0) then Symbolindex_stand:=h_longint;
            { Nun mu� kontrolliert werden, ob dieses Symbol �berhaupt existiert }
            BIndex.Init(Symbolindex_stand);
            if PLayer(GPSProj^.PInfo^.Symbols)^.HasObject(@BIndex) = nil then
            begin
               if (Darstellung_Obj = 3) and (GpsProj^.ActualSym = nil) then
                  Darstellung_Obj:=0 // switch back to ot_Pixel
               else if GPSProj^.ActualSym <> nil then
                  Symbolindex_stand:=GPSProj^.ActualSym^.Index;
            end;

            ParseMyPchar(MyData,Dummy,DDEHandler.fddesepsign,7); { Herauslesen des Symbolindex_Up }
            val (Dummy,h_longint,code); if (code =0) then Symbolindex_up:=h_longint;
            { Nun mu� kontrolliert werden, ob dieses Symbol �berhaupt existiert }
            BIndex.Init(Symbolindex_up);
            if PLayer(GPSProj^.PInfo^.Symbols)^.HasObject(@BIndex) = nil then
            begin
               if (Darstellung_Obj = 3) and (GpsProj^.ActualSym = nil) then
                  Darstellung_Obj:=0 // switch back to ot_Pixel
               else if GPSProj^.ActualSym <> nil then
                  Symbolindex_up:=GPSProj^.ActualSym^.Index;
            end;
            ParseMyPchar(MyData,Dummy,DDEHandler.fddesepsign,8); { Herauslesen des Symbolindex_Down }
            val (Dummy,h_longint,code); if (code =0) then Symbolindex_down:=h_longint;
            { Nun mu� kontrolliert werden, ob dieses Symbol �berhaupt existiert }
            BIndex.Init(Symbolindex_down);
            if PLayer(GPSProj^.PInfo^.Symbols)^.HasObject(@BIndex) = nil then
            begin
               if (Darstellung_Obj = 3) and (GpsProj^.ActualSym = nil) then
                  Darstellung_Obj:=0 // switch back to ot_Pixel
               else if GPSProj^.ActualSym <> nil then
                  Symbolindex_down:=GPSProj^.ActualSym^.Index;
            end;

            ParseMyPchar(MyData,Dummy,DDEHandler.fddesepsign,9); { Herauslesen des Symbolindex_Left }
            val (Dummy,h_longint,code); if (code =0) then Symbolindex_left:=h_longint;
            { Nun mu� kontrolliert werden, ob dieses Symbol �berhaupt existiert }
            BIndex.Init(Symbolindex_left);
            if PLayer(GPSProj^.PInfo^.Symbols)^.HasObject(@BIndex) = nil then
            begin
               if (Darstellung_Obj = 3) and (GpsProj^.ActualSym = nil) then
                  Darstellung_Obj:=0 // switch back to ot_Pixel
               else if GPSProj^.ActualSym <> nil then
                  Symbolindex_left:=GPSProj^.ActualSym^.Index;
            end;

            ParseMyPchar(MyData,Dummy,DDEHandler.fddesepsign,10); { Herauslesen des Symbolindex_Right }
            Dummy:=copy(Dummy,1,length(Dummy)-1); { Um die ] am Schlu� des DDE-Strings wegzukopieren }
            val (Dummy,h_longint,code); if (code =0) then Symbolindex_right:=h_longint;
            { Nun mu� kontrolliert werden, ob dieses Symbol �berhaupt existiert }
            BIndex.Init(Symbolindex_right);
            if PLayer(GPSProj^.PInfo^.Symbols)^.HasObject(@BIndex) = nil then
            begin
               if (Darstellung_Obj = 3) and (GpsProj^.ActualSym = nil) then
                  Darstellung_Obj:=0 // switch back to ot_Pixel
               else if GPSProj^.ActualSym <> nil then
                  Symbolindex_right:=GPSProj^.ActualSym^.Index;
            end;

            Long:=0; Last_Long:=0;
            Lat:=0; Last_Lat:=0;
            sLong:='';
            sLat:='';
            Height:='';
            FIX_NOFIX:='';
            HDOP_PDOP:='';
            Typ:='';
            Date:='';
            Time:='';
            Sat:='';
            Zusatzinfo:='';
            New_created_Symbol:=true;

            if Darstellung_Obj <> Obj_Objekt then Objektindex:=0;
            Loesch_flag:=true;
            Sendeformat:=GPS_Online;
         end;
         Change_GPSPrioritaeten(AGPSInfo,FALSE); // FALSE -> keep last position
      end;
      ParseGPSInfo:=FALSE;
   end;

   if copy(GPSData,1,13) = '[GPSDATAFILE]' then { Es soll ein GPS-Datenfile ausgelesen werden }
   begin
      delete_pchar(MyData,1,14); { Teilstring [GPSDATAFILE][ aus dem DDE-String l�schen }
      GpsDataFilename:=strpas(MyData);
      GpsDataFilename:=copy(GpsDataFilename,1,pos(']',GpsDataFilename)-1);
      if Fileexists(GpsDatafilename) then
      begin
         Assignfile(GpsDataFile,GpsDatafilename);
         Reset(GpsDataFile);
         GPS_Batchmode:=true;
         repeat
            Readln(GpsDataFile,GpsDataFileLine);
            GpsDataFilePtr:=stralloc(length(GpsDataFileLine)+1);
            strpcopy(GpsDataFilePtr,GpsDataFileLine);
            GPS_Execute(GPSProj,GpsDataFilePtr);
            strdispose(GpsDataFilePtr);
         until EOF(GpsDataFile);
         CloseFile(GpsDataFile);
         GPS_Batchmode:=false;
      end;
      ParseGPSInfo:=FALSE;
   end;

   if copy(GPSData,1,7) = '[GPSNEW' then { Es wird eine neue GPS-ID eingetragen }
   begin
      delete_pchar(MyData,1,7); { Teilstring [GPSNEW] aus dem DDE-String l�schen }
      with AGPSInfo^ do
      begin
         ParseMyPchar(MyData,ID,DDEHandler.fddesepsign,1);    { Herauslesen der ID }
         ParseMyPchar(MyData,Dummy,DDEHandler.fddesepsign,2); { Herauslesen des Objekttyps }
         val (Dummy,h_int,code);
         if (code =0) and
            ((h_int = 0) or (h_int = 1) or (h_int = 3) or (h_int = 99)) then Darstellung_Obj:=h_int else Darstellung_Obj:=0;

         ParseMyPchar(MyData,Dummy,DDEHandler.fddesepsign,3); { Herauslesen des Darstellungstyps }
         val (Dummy,h_int,code);
         if (code =0) and ((h_int = 0) or (h_int = 1)) then Darstellung_Typ:=h_int else Darstellung_Typ:=0;

         ParseMyPchar(MyData,Dummy,DDEHandler.fddesepsign,4); { Herauslesen der Autozoomart }
         if (Dummy = 'T') then Autozoom:=True; if (Dummy = 'F') then Autozoom:=False;
         if not((Dummy = 'T') or (Dummy = 'F')) then Autozoom:=False;

         ParseMyPchar(MyData,Dummy,DDEHandler.fddesepsign,5); { Herauslesen des Layernamens }
         MyPLayer:=GPSProj^.Layers^.NameTolayer(Dummy);
         if MyPLayer <> nil then LayerIndex:=MyPLayer^.Index else LayerIndex:=GPSProj^.Layers^.Toplayer^.Index;

         ParseMyPchar(MyData,Dummy,DDEHandler.fddesepsign,6); { Herauslesen des Symbolindex_Stand }
         val (Dummy,h_longint,code); if (code =0) then Symbolindex_stand:=h_longint;
         { Nun mu� kontrolliert werden, ob dieses Symbol �berhaupt existiert }
         BIndex.Init(Symbolindex_stand);
         if PLayer(GPSProj^.PInfo^.Symbols)^.HasObject(@BIndex) = nil then
         begin
            if (Darstellung_Obj = 3) and (GpsProj^.ActualSym = nil) then
               Darstellung_Obj:=0 // switch back to ot_Pixel
            else if GPSProj^.ActualSym <> nil then
               Symbolindex_stand:=GPSProj^.ActualSym^.Index;
         end;

         ParseMyPchar(MyData,Dummy,DDEHandler.fddesepsign,7); { Herauslesen des Symbolindex_Up }
         val (Dummy,h_longint,code); if (code =0) then Symbolindex_up:=h_longint;
         { Nun mu� kontrolliert werden, ob dieses Symbol �berhaupt existiert }
         BIndex.Init(Symbolindex_up);
         if PLayer(GPSProj^.PInfo^.Symbols)^.HasObject(@BIndex) = nil then
         begin
            if (Darstellung_Obj = 3) and (GpsProj^.ActualSym = nil) then
               Darstellung_Obj:=0 // switch back to ot_Pixel
            else if GPSProj^.ActualSym <> nil then
               Symbolindex_up:=GPSProj^.ActualSym^.Index;
         end;

         ParseMyPchar(MyData,Dummy,DDEHandler.fddesepsign,8); { Herauslesen des Symbolindex_Down }
         val (Dummy,h_longint,code); if (code =0) then Symbolindex_down:=h_longint;
         { Nun mu� kontrolliert werden, ob dieses Symbol �berhaupt existiert }
         BIndex.Init(Symbolindex_down);
         if PLayer(GPSProj^.PInfo^.Symbols)^.HasObject(@BIndex) = nil then
         begin
            if (Darstellung_Obj = 3) and (GpsProj^.ActualSym = nil) then
               Darstellung_Obj:=0 // switch back to ot_Pixel
            else if GPSProj^.ActualSym <> nil then
               Symbolindex_down:=GPSProj^.ActualSym^.Index;
         end;

         ParseMyPchar(MyData,Dummy,DDEHandler.fddesepsign,9); { Herauslesen des Symbolindex_Left }
         val (Dummy,h_longint,code); if (code =0) then Symbolindex_left:=h_longint;
         { Nun mu� kontrolliert werden, ob dieses Symbol �berhaupt existiert }
         BIndex.Init(Symbolindex_left);
         if PLayer(GPSProj^.PInfo^.Symbols)^.HasObject(@BIndex) = nil then
         begin
            if (Darstellung_Obj = 3) and (GpsProj^.ActualSym = nil) then
               Darstellung_Obj:=0 // switch back to ot_Pixel
            else if GPSProj^.ActualSym <> nil then
               Symbolindex_left:=GPSProj^.ActualSym^.Index;
         end;

         ParseMyPchar(MyData,Dummy,DDEHandler.fddesepsign,10); { Herauslesen des Symbolindex_Right }
         Dummy:=copy(Dummy,1,length(Dummy)-1); { Um die ] am Schlu� des DDE-Strings wegzukopieren }
         val (Dummy,h_longint,code); if (code =0) then Symbolindex_right:=h_longint;
         { Nun mu� kontrolliert werden, ob dieses Symbol �berhaupt existiert }
         BIndex.Init(Symbolindex_right);
         if PLayer(GPSProj^.PInfo^.Symbols)^.HasObject(@BIndex) = nil then
         begin
            if (Darstellung_Obj = 3) and (GpsProj^.ActualSym = nil) then
               Darstellung_Obj:=0 // switch back to ot_Pixel
            else if GPSProj^.ActualSym <> nil then
               Symbolindex_right:=GPSProj^.ActualSym^.Index;
         end;

         Long:=0; Last_Long:=0;
         Lat:=0; Last_Lat:=0;
         New_created_Symbol:=true;
         sLong:='';
         sLat:='';
         Height:='';
         FIX_NOFIX:='';
         HDOP_PDOP:='';
         Typ:='';
         Date:='';
         Time:='';
         Sat:='';
         Zusatzinfo:='';

         if Darstellung_Obj <> Obj_Objekt then Objektindex:=0;
         Loesch_flag:=false;
         Sendeformat:=GPS_Online;
      end;

      { Einf�gen des neuen Eintrages in die Liste }
      if Insert_GPSPrioritaeten(AGPSInfo) then GPS_anzahl:=GPS_anzahl + 1;

      { ParseGPSInfo mu� FALSE gesetzt sein, damit die DDE_Execute nicht komplett durchlaufen wird }
      ParseGPSInfo:=FALSE;
   end;

   if copy(GPSData,1,7) = '[GPSPOS' then
   begin
      delete_pchar(MyData,1,7); { Teilstring [GPSPOS aus dem DDE-String l�schen }
      ParseGPSInfo:=TRUE;
      with AGPSInfo^ do
      begin
         ParseMyPchar(MyData,ID,DDEHandler.fddesepsign,1);
         ParseMyPchar(MyData,Dummy,DDEHandler.fddesepsign,2);
         sLat:=Dummy;
         case Upcase(Dummy[Ord(Dummy[0])]) of
            'N' : N_S:=TRUE;
            'S' : N_S:=FALSE;
            else
               ParseGPSInfo:=FALSE;
         end;

         Dummy[0]:=chr(Ord(Dummy[0])-1);
         Val(Dummy,Lat,Code);
         if Code<>0 then ParseGPSInfo:=FALSE;

         ParseMyPchar(MyData,Dummy,DDEHandler.fddesepsign,3);
         sLong:=Dummy;
         case Upcase(Dummy[Ord(Dummy[0])]) of
            'E' : E_W:=TRUE;
            'W' : E_W:=FALSE;
            else
               ParseGPSInfo:=FALSE;
         end;
         Dummy[0]:=chr(Ord(Dummy[0])-1);
         Val(Dummy,Long,Code);
         if Code<>0 then  ParseGPSInfo:=FALSE;

         ParseMyPchar(MyData,Height,DDEHandler.fddesepsign,4);

         ParseMyPchar(MyData,Typ,DDEHandler.fddesepsign,5);

         ParseMyPchar(MyData,FIX_NOFIX,DDEHandler.fddesepsign,6);

         ParseMyPchar(MyData,HDOP_PDOP,DDEHandler.fddesepsign,7);

         ParseMyPchar(MyData,Date,DDEHandler.fddesepsign,8);

         ParseMyPchar(MyData,Time,DDEHandler.fddesepsign,9);

         ParseMyPchar(MyData,Sat,DDEHandler.fddesepsign,10);
         Sat[0]:=Chr(Ord(Sat[0])-1);       {letztes Zeichen ']' entfernen}

         Zusatzinfo:=nil;

         { Wenn keine Optionalen Parameter im DDE-String vorhanden sind wird On-Line Betriebsart verwendet }
         if Pos_pchar('[',MyData) = 0 then Sendeformat:=GPS_Online
         else
         begin
            OptParams:=stralloc(strlen(MyData)); hpchar:=stralloc(4096);

            { Auslesen der Optionalen Parameter }
            copy_pchar(MyData,pos_pchar('[',mydata) + 1,strlen(MyData) - pos_pchar('[',MyData),OptParams);

            { Herauslesen des Sendeformates }
            copy_pchar(OptParams,1,1,hpchar); dummy:=strpas(hpchar);
            Val(Dummy,h_int,Code);

            if Code <> 0 then ParseGPSInfo:=FALSE;

            if  h_int = 1 then Sendeformat:=GPS_offline;
            if  h_int = 0 then Sendeformat:=GPS_online;

            { Herauslesen des Alarmflags }
            copy_pchar(OptParams,3,1,hpchar); dummy:=strpas(hpchar);
            if dummy = 'T' then Alarm:=true;
            if dummy = 'F' then Alarm:=false;

            { Herauslesen des Objekttyps }
            copy_pchar(OptParams,5,1,hpchar); dummy:=strpas(hpchar);
            val(dummy,Darstellung_Obj,code);

            if Code <> 0 then ParseGPSInfo:=FALSE;

            { Herauslesen der Objektnummer }
            delete_pchar(OptParams,1,6);

            if pos_pchar(DDEHandler.fddesepsign,OptParams) <> 0 then
            begin
               copy_pchar(OptParams,1,pos_pchar(DDEHandler.fddesepsign,OptParams)-1,hpchar); dummy:=strpas(hpchar);
               copy_pchar(OptParams,pos_pchar(DDEHandler.fddesepsign,OptParams)+1,strlen(OptParams),hpchar);

               Zusatzinfo:=stralloc(strlen(hpchar)+1);
               copy_pchar(hpchar,1,strlen(hpchar)-1,Zusatzinfo);
            end
            else
            begin
               copy_pchar(OptParams,1,strlen(OptParams)-1,hpchar); dummy:=strpas(hpchar);
            end;
            val(dummy,Objektnummer,code);
            if Code <> 0 then ParseGPSInfo:=FALSE;
            strdispose(OptParams); strdispose(hpchar);
         end;
      end;
      { Wenn Online-Betriebsart ausgew�hlt ist werden die Priorit�ten f�r diese GPS-ID aus der Liste gelesen }
      if (AGPSInfo^.Sendeformat = GPS_Online) and (AGPSInfo^.ID <> 'EOD') then ParseGPSInfo:=Get_GPSPrioritaeten(AGPSInfo);
   end;

   if copy(GPSData,1,11) = '[GPSOFFSET]' then
   begin
      { Setzen des Linien-Offsets }
      ParseGPSInfo:=False;
      Delete(GPSData,1,12);
      val(copy(GpsData,1,pos(']',GpsData)-1),h_Abstand,code);
      if (code = 0) and (CheckLineanz = false) then
      begin
         if h_Abstand <> 0 then
         begin
            { Als Offsettyp wird eine Runde Ecke mit umrahmenden Rechteck gew�hlt }
            Offset_Ecke:=1; Offset_Linie:=2;
            Offset_Abstand:=h_Abstand; Offset_Flag:=true;
         end
         else
            Offset_Flag:=false;
      end;
   end;

   if (copy(GPSData,1,9) ='[GPSSHOW]') then
   begin
      ParseGPSInfo:=FALSE;
      Delete(GPSData,1,10); GpsData:=copy(GpsData,1,length(GpsData)-1);
      if GPS_Anzahl <> 0 then
      begin
         new(MyGPSInfo);
         MyGPSInfo^.ID:=GPSData;
         { Ermitteln der GPS-Daten aus der Liste }
         Get_GPSPrioritaeten(MyGPSInfo);
         { Zoomen zum entsprechenden Feld }
         FuncZoomToObject(GPSProj,MyGPSInfo^.Objektindex);
         dispose(MyGPSInfo);
      end;
   end;

   if copy(GPSData,1,11) = '[SETGPSPRO]' then
   begin
      { Setzen der Referenzierungseinstellungen }
      Konvertierungstyp:=COORDDLL;
      ParseGPSInfo:=FALSE;
      Delete(GpsData,1,12);
      from_System:=copy(GpsData,1,Pos(']',GpsData)-1);
      Delete(GpsData,1,Pos(']',GpsData)+1);
      from_Date:=copy(GpsData,1,Pos(']',GpsData)-1);
      Delete(GpsData,1,Pos(']',GpsData)+1);
      to_System:=copy(GpsData,1,Pos(']',GpsData)-1);
      Delete(GpsData,1,Pos(']',GpsData)+1);
      to_Date:=copy(GpsData,1,Pos(']',GpsData)-1);
      Delete(GpsData,1,Pos(']',GpsData)+1);
      Dummy:=copy(GpsData,1,Pos(']',GpsData)-1);
      val(Dummy,Offset_X,code); if code <> 0 then Offset_X:=0;
      Delete(GpsData,1,Pos(']',GpsData)+1);
      Dummy:=copy(GpsData,1,Pos(']',GpsData)-1);
      val(Dummy,Offset_Y,code); if code <> 0 then Offset_Y:=0;
      AxGisProjection.SourceProjection:=from_System;
      AxGisProjection.SourceDate:=from_Date;
      AxGisProjection.SourceProjSettings:=from_System;
      AxGisProjection.TargetProjection:=to_System;
      AxGisProjection.TargetDate:=to_Date;
      AxGisProjection.TargetProjSettings:=to_System;
      AxGisProjection.UsedInputCoordSystem:=cs_Carthesic;  // input coordinates are always carthesic
      AxGisProjection.UsedOutputCoordSystem:=cs_Carthesic; // output coordiantes are always carthesic
   end;

   if copy(GPSData,1,9) = '[LOADGRF]' then
   begin
      { Einlesen des Referenzierungsfiles }
      { ParseGPSInfo mu� FALSE gesetzt sein, damit die DDE_Execute nicht komplett durchlaufen wird }
      ParseGPSInfo:=FALSE;
      Delete(GPSData,1,10);
      LoadGRF(copy(GPSData,1,length(GPSData)-1));
   end;
   if copy(GPSData,1,13) = '[GPSDBCOLUMN]' then
   begin
      { Einlesen einer Datenbankspaltendefinition }
      ParseGPSInfo:=FALSE;
      hpchar:=stralloc(4096);

      DDEHandler.SendString('[END][]'); { Der Datenbank senden, dass voriger Import nun beendet ist }
      Spalteninfo_flag:=true;

      delete_pchar(myData,1,14); { [GPSDBCOLUMN][ wird gel�scht }

      if DB_Anzahl <> 0 then
         Delete_AllDBEinstellungen;
      { Die Datenbankliste muss nun neu erzeugt werden }
      new(MyDBInfo);
      repeat
         { Auswerten der Daten aus der Zeile }
         { Erhalten des Spaltennamens }
         copy_pchar(myData,1,Pos_pchar(DDEHandler.fddesepsign,myData) - 1,hPchar);
         MyDBInfo^.Spaltenname:=strpas(hPchar);
         delete_pchar(myData,1,Pos_pchar(DDEHandler.fddesepsign,myData));

         { Ermitteln des Typs }
         copy_pchar(myData,1,POS_pchar(DDEHandler.fddesepsign,myData) - 1,hPchar);
         MyDBInfo^.Typ:=strpas(hPchar);
         delete_pchar(myData,1,Pos_pchar(DDEHandler.fddesepsign,myData));

         { Ermitteln der L�nge }
         copy_pchar(myData,1,POS_pchar(DDEHandler.fddesepsign,myData)-1,hPchar);
         val(strpas(hPchar),MyDBInfo^.Laenge,code);
         delete_pchar(myData,1,Pos_pchar(DDEHandler.fddesepsign,myData));

         { Ermitteln der Position }
         copy_pchar(myData,1,POS_pchar(']',myData)-1,hPchar);
         val(strpas(hPchar),MyDBInfo^.Position,code);
         delete_pchar(myData,1,Pos_pchar(']',myData));

         Insert_DBEinstellungen(MyDBInfo);
         DB_anzahl:=DB_anzahl + 1;
         if Pos_pchar('[',myData) <> 0 then Delete_pchar(myData,1,1);
      until Pos_pchar(DDEHandler.fddesepsign,myData) = 0;
      Dispose(MyDBInfo); strdispose(hpchar);
   end;

   if copy(GPSData,1,11) = '[GPSPROASC]' then { Projektionsrechnung eines ASC Files }
   begin
      { ParseGPSInfo mu� FALSE gesetzt sein, damit die DDE_Execute nicht komplett durchlaufen wird }
      ParseGPSInfo:=FALSE;

      { Ermitteln der Dateinamen }
      Delete(GpsData,1,12); { L�schen von [GPSPROASC][ }
      InASC:=copy(GpsData,1,Pos(']',GpsData)-1);
      Delete(GpsData,1,Pos('[',GpsData));
      InADF:=copy(GpsData,1,Pos(']',GpsData)-1);
      Delete(GpsData,1,Pos('[',GpsData));
      OutASC:=copy(GpsData,1,Pos(']',GpsData)-1);

      GpsProASC(InASC,InADF,OutASC);
   end;

   if copy(GPSData,1,12) = '[GPSPROCALC]' then
   begin
      { ParseGPSInfo mu� FALSE gesetzt sein, damit die DDE_Execute nicht komplett durchlaufen wird }
      ParseGPSInfo:=FALSE;

      { Durchf�hren einer Projektionsumrechnung �ber den GPS-Modul }
      { Ermitteln der X-Koordinate }
      Delete(GpsData,1,13); { L�schen von [GPSPROCALC][ }
      Dummy:=copy(GpsData,1,Pos(']',GpsData)-1);
      val(Dummy,X,code); if code <> 0 then X:=0;

      { Ermitteln der Y-Koordiante }
      Delete(GpsData,1,Pos('[',GpsData));
      Dummy:=copy(GpsData,1,Pos(']',GpsData)-1);
      val(Dummy,Y,code); if code <> 0 then Y:=0;

      Konvertierung(X,Y);

      { Zur�cksenden des Konvertierergebnisses }
      Dummy:='[GPSPROCALC][' + floattostr(X/100) + ']['+floattostr(Y/100)+']';
      DDEHandler.Sendstring(Dummy);
   end;
   strdispose(myData);
   {$ENDIF}
end;

{*******************************************************************************
* FUNKTION : TGPS.GetAngle                                                     *
********************************************************************************
* Funktion ermittelt den Winkel mit dem ein Text rotiert ist.                  *
*                                                                              *
* PARAMETER: X1 -> X-Koordinate des ersten Punktes.                            *
*            Y1 -> Y-Koordinate des ersten Punktes.                            *
*            X2 -> X-Koordinate des zweiten Punktes.                           *
*            Y2 -> Y-Koordinate des zweiten Punktes.                           *
*                                                                              *
* R�CKGABEWERT: Winkel der zwischen diesen zwei Punkten liegt.                 *
*                                                                              *
********************************************************************************}
function TGPS.GetAngle(X1:double;Y1:double;X2:double;Y2:double):double;
var A,B,C:double;
    Alpha:double;
    retVal:double;
begin
   { Zuwerst werden die Kantenl�ngen des Dreiecks ermittelt }
   if (X1 = X2) and (Y1 = Y2) then
   begin
      retVal:=0;
   end
   else
   begin
      A:=ABS(Y2 - Y1); B:=ABS(X2 - X1); C:=SQRT(SQR(A) + SQR(B));
      Alpha:=(A / C);  { Nun ist der Sinus von Alpha bekannt }
      { Nun muss man noch den Invers Sinus von Alpha berechnen }
      Alpha:=ArcSin(Alpha);
      retVal:=(Alpha*180) / PI;
      if (Y2 >= Y1) then { Man befindet sich im oberen Halbkreis des Kreises }
      begin
         if (X2 < X1) then   { Man geht nach oben links   }
         begin
            retVal:=180 - retVal;
         end;
         if (X2 > X1) and (Y1 = Y2) then { Man geht gerade nach rechts }
         begin
            retVal:=360;
         end;
      end;
      if (Y2 < Y1) then { Man befindet sich im unteren Halbkreis des Kreises }
      begin
         if (X2 >= X1) then { Man geht nach rechts }
            retVal:=360 - retVal
         else               { Man geht nach links }
            retVal:=retVal + 180;
      end;
   end;

   result:=retVal;
end;

{*******************************************************************************
* PROZEDUR : TGPS.GetGPSAngleDirection                                         *
********************************************************************************
* Prozedur ermittelt den Rotationswinkel und die Richtung des Symbols.         *
*                                                                              *
* PARAMETER: X1 -> X-Koordinate des ersten Punktes.                            *
*            Y1 -> Y-Koordinate des ersten Punktes.                            *
*            X2 -> X-Koordinate des zweiten Punktes.                           *
*            Y2 -> Y-Koordinate des zweiten Punktes.                           *
*            Angle -> Winkel mit dem GPS-Symbol rotiert werden mu�.            *
*            Direction -> Wert der Richtung in das sich GPS-Symbol bewegt.     *
*                                                                              *
* R�CKGABEWERT: Keiner                                                         *
*                                                                              *
********************************************************************************}
procedure TGPS.GetGPSAngleDirection(X1:double;Y1:double;X2:double;Y2:double;
                                    var Angle:real;var Direction:integer);
var Winkel:real;
begin
   X1:=X1/100; X2:=X2/100; Y1:=Y1/100; Y2:=Y2/100;
   Winkel:=GetAngle(X1,Y1,X2,Y2);

   { Nun mu� der Winkel aufgeschl�sselt werden }
   if (Winkel = 0) then
   begin
       Direction:=DIR_STAND;
       Angle:=Winkel * PI / 180;
       exit;
   end;

   { Das Symbol bewegt sich nach rechts }
   if ((Winkel >= 0) and (Winkel < 45)) or ((Winkel >= 315) and (Winkel <= 360)) then
   begin
      Direction:=DIR_RIGHT;
      Angle:=Winkel * PI / 180;
      exit;
   end;

   { Das Symbol bewegt sich nach oben }
   if (Winkel >= 45) and (Winkel < 135) then
   begin
      if Winkel >= 90 then Winkel:=Winkel-90
      else if Winkel < 90 then Winkel:= 360 - (90-Winkel);
      Direction:=DIR_UP;
      Angle:=Winkel * PI/180;
      exit;
   end;

   { Das Symbol bewegt sich nach links }
   if (Winkel >= 135) and (Winkel < 225) then
   begin
      if Winkel >=180 then Winkel:=Winkel - 180
      else if Winkel < 180 then Winkel:=360 - (180 - Winkel);
      Direction:=DIR_LEFT;
      Angle:=Winkel * PI/180;
      exit;
   end;

   { Das Symbol bewegt sich nach unten }
   if (Winkel >= 225) and (Winkel < 315) then
   begin
      if Winkel >= 270 then Winkel:=Winkel-270 else
      if Winkel < 270 then Winkel:=360 - (270 - Winkel);
      Direction:=DIR_DOWN;
      Angle:=Winkel * PI/180;
      exit;
   end;
end;


{*******************************************************************************
* PROZEDUR : TGPS.Delete_pchar                                                 *
********************************************************************************
* Prozedur l�scht aus einem String ab der Position pos anz Zeichen.            *
*                                                                              *
*                                                                              *
*                                                                              *
********************************************************************************}
procedure TGps.Delete_pchar(var Source:pchar;pos:integer;anz:integer);
var i,J:integer;
    Dest:pchar;
begin
   j:=0; Dest:=stralloc(strlen(Source)+1);
   for i:=0 to strlen(Source) do
   begin
      if not((i>=(pos - 1)) and (i < (pos+anz-1))) then begin Dest[j]:=Source[i]; j:=j + 1; end;
   end;
   Dest[j]:=#0;
   strcopy(Source,Dest);
   strdispose(Dest);
end;

{*******************************************************************************
* FUNKTION : TGPS.copy_pchar                                                   *
********************************************************************************
* Funktion liefert als R�ckgabewert die Zeichenkette anz Zeichen ab der        *
* Position pos der Zeichenkette Zeichen.                                       *
*                                                                              *
*                                                                              *
********************************************************************************}
procedure TGps.copy_pchar(Source:pchar;pos:integer;anz:integer;var Dest:pchar);
var i,J:integer;
begin
   j:=0;
   for i:=0 to strlen(Source) do
   begin
      if (i>=(pos - 1)) and (i < (pos+anz-1)) then begin Dest[j]:=Source[i]; j:=j + 1; end;
   end;
   Dest[j]:=#0;
end;


{*******************************************************************************
* PROZEDUR : TGPS.ParseMyPchar                                                 *
********************************************************************************
* Prozedur ermittelt jene Zeichenkette welche sich an Stelle pos befindet,     *
* wobei eine Zeichenkette durch das Zeichen getrennt ist.                      *
* z.B: Source ist : HALLO,DU,DA                                                *
*      Beim Kommando: ParsePchar(Source,res,',',2)                             *
*      steht in res 'DU';                                                      *
*                                                                              *
********************************************************************************}
procedure TGPS.ParseMyPchar(Source:pchar;var res:string;Zeichen:char;pos:integer);
var pos1,pos2,Stelle,i:integer;
    hpchar:pchar;
    gefunden:boolean;
begin
   { Suchen der Position des Zeichens im Source }
   pos1:=0; pos2:=0; Stelle:=0; hpchar:=stralloc(strlen(Source)+1); i:=0; gefunden:=false;
   while (i < strlen(Source)) and (gefunden = false) do
   begin
      { Wenn das Zeichen gefunden wurde }
      if Source[i] = Zeichen then
      begin
         pos1:=pos2; pos2:=i;
         Stelle:=Stelle + 1;
         if Stelle = (pos + 1) then gefunden:=true;
      end;
      i:=i + 1;
   end;

   if ((Stelle = pos) and (i = strlen(Source))) then
   begin
      pos1:=pos2;
      pos2:=strlen(Source);
      gefunden:=true;
   end;

   if gefunden = true then
   begin
      { Nun kann die Zeichenkette von Pos1 bis Pos2 nach res kopiert werden }
      copy_pchar(Source,pos1 + 2,pos2 - pos1 - 1,hpchar);
      res:=strpas(hpchar);
   end
   else
      res:='';
   strdispose(hpchar);
end;

function TGPS.Pos_pchar(Zeichen:char;Source:pchar):integer;
var i:integer;
begin
   for i:=0 to strlen(Source) do
   begin
      if Source[i] = Zeichen then begin Result:=i + 1; exit; end;
   end;
   Result:=0;
end;

{*******************************************************************************
* PROZEDUR : TGPS.Symbole_anpassen                                             *
********************************************************************************
* Prozedur zeigt alle Symbole im Projekt in der jeweiligen Zoomtiefe mit       *
* verschiedener Groesse an.                                                    *
*                                                                              *
*                                                                              *
*                                                                              *
*                                                                              *
********************************************************************************}
procedure TGPS.Symbole_anpassen(GPS_ID:string);
var Laufzeiger: PGPS_Listeneintrag;
    MyIndex : PIndex;
    MySymbol: PSymbol;
    geloscht: boolean;
    ASymbol : PSymbol;
    AIndex  : TIndex;
    SymGroup: PSGroup;
    CurrentView : TDRect;
    View_flag   :boolean;
    h_Scale     :double;
begin
   Laufzeiger:=GPS_Anfang;

   { Nur wenn sich das Symbol auch in der aktuellen Ansicht befindet muss es neu gezeichnet werden }
   GPSProj^.PInfo^.GetCurrentScreen(CurrentView);

   View_flag:=false;
   { Kontrolle, ob sich der Zoom ge�ndert hat }
   if not ((CurrentView.A.X = Zoom_View.A.X) and (CurrentView.A.Y = Zoom_View.A.Y) and
     (CurrentView.B.X = Zoom_View.B.X) and (CurrentView.B.Y = Zoom_View.B.Y)) then
   begin
      View_flag:=true;
      { Erhalten des neuen Views }
      GPSProj^.PInfo^.GetCurrentScreen(Zoom_View);
   end;

   if GPS_Anfang <> NIL then
   repeat
      { Durchlaufen der Priorit�tenliste }
      if ((Laufzeiger^.GPS_listeneintrag_info.Darstellung_Obj = Obj_Symbol) or
         (Laufzeiger^.GPS_listeneintrag_info.Darstellung_Obj = Obj_Objekt)) and
         (Laufzeiger^.GPS_listeneintrag_info.Darstellung_Typ = Typ_aktuell) and
         (Laufzeiger^.GPS_listeneintrag_info.Autozoom = false) and
         (Laufzeiger^.GPS_listeneintrag_info.ID <> GPS_ID) then
      begin
         { Kontrolle, ob Symbol nicht in der Zwischenzeit gel�scht wurde }
         new (MyIndex);
         MyIndex:=New(PIndex,Init(Laufzeiger^.GPS_listeneintrag_info.Objektindex));

         if GPSProj^.Layers^.IndexObject(GPSProj^.PInfo,MyIndex)<>NIL then
            geloscht:=false
         else geloscht:=true;
         dispose(MyIndex,done);

         if (geloscht <> true) then
         begin
            { Nur wenn Symbol nicht gel�scht wurde wird es in ver�nderter Gr�sse dargestellt }
            AIndex.Init(Laufzeiger^.GPS_listeneintrag_info.ObjektIndex);
            aSymbol:=PSymbol(GPSProj^.Layers^.IndexObject(GPSProj^.PInfo,@AIndex));

            if ((aSymbol^.Position.X >= CurrentView.A.X) and (aSymbol^.Position.Y >= CurrentView.A.Y) and
               (aSymbol^.Position.X <= CurrentView.B.X) and (aSymbol^.Position.Y <= CurrentView.B.Y)) and
               (View_flag = true) then
            begin
               { Ermitteln des Skalierungsfaktors des Symbols aufgrund der Zoomtiefe }
               SymGroup:=ASymbol^.getsymbol(GPSProj^.PInfo);
//!?!?!               h_Scale:=(SymGroup^.Zoom * GPSProj^.PInfo^.Zoom ) / 1000;
(*!?!?!               if (h_Scale <> ASymbol^.ScaleFact) then
               begin
                  { Nur wenn sich die Zoomtiefe f�r dieses Objekt auch ver�ndert hat wird neu gezeichnet }
                  ASymbol^.ScaleFact:=h_Scale;
                  aSymbol^.Invalidate(GPSProj^.PInfo);
                  aSymbol^.CalculateClipRect(GPSProj^.PInfo);
                  aSymbol^.Invalidate(GPSProj^.PInfo);
                  GPSProj^.UpdateClipRect(aSymbol);
                  GPSProj^.CorrectSize(aSymbol^.ClipRect,TRUE);
                  GPSProj^.PInfo^.RedrawInvalidate;
               end; *)
            end;
         end;
      end;
      Laufzeiger:=Laufzeiger^.next;
   until (Laufzeiger = NIL);
end;

{*******************************************************************************
* PROZEDUR : TGPS.Datenbank_einfuegen                                          *
********************************************************************************
* Schreibt die erhaltene Zeile in die Datenbank.                               *
*                                                                              *
*                                                                              *
*                                                                              *
*                                                                              *
*                                                                              *
********************************************************************************}
procedure TGPS.Datenbank_einfuegen(Zeile:pchar; Index:longint;Flache:real;Layer:string;Layerindex:longint);
var SStr1,h_SStr1 :string;
    AText        : String;
    i,anz,worked : integer;
    Laenge,gef_anz:integer;
    h_string:array [1..255] of char;
    Laufzeiger   : PDB_Listeneintrag;
    gefunden     :boolean;
    MyDef,MyImp  :pchar;
    hpchar       :pchar;
    code         :integer;
begin
   MyDef:=stralloc(4096); hpchar:=stralloc(4096); MyImp:=stralloc(4096);
   if strcomp(Zeile,'') <> 0 then
   begin
      if pos_pchar(DDEHandler.fddesepsign,Zeile) <> 0 then
      begin
         copy_pchar(Zeile,1,pos_pchar(DDEHandler.fddesepsign,Zeile)-1,hpchar); SStr1:=strpas(hpchar);
         val(SStr1,anz,code);
         delete_pchar(Zeile,1,pos_pchar(DDEHandler.fddesepsign,Zeile));
      end;
   end
   else
      anz:=0;

   if (anz <> 0) and (DDEHandler.DDEOK) and (DB_Anzahl <> 0) then
   begin
      if (Spalteninfo_flag = true) then
      begin
         strpcopy(MyDef,'[DEF]');

         { Anzahlfeld eintragen }
         AText:=inttostr(DB_Anzahl);
         while length(AText) < 3 do AText:=' ' + AText;

         strpcopy(hpchar,'['+AText+']'); strcat(MyDef,hpchar);

         { Spaltendefinitionen eintragen }
         worked:=0; gefunden:=false; worked:=0; gef_anz:=0;
         repeat
            if gefunden = true then gef_anz:=gef_anz + 1;
            Laufzeiger:=DB_Anfang; gefunden:=false;
            repeat
               if (Laufzeiger^.DB_listeneintrag_info.Position = worked) then
               begin
                  gefunden:=true;
                  AText:=Laufzeiger^.DB_listeneintrag_info.Spaltenname;
                  while length(AText) < 15 do AText:=AText + ' ';

                  strpcopy(hpchar,'['+AText+']'); strcat(MyDef,hpchar);

                  AText:=inttostr(Laufzeiger^.DB_listeneintrag_info.Laenge);
                  while length(AText) < 3 do AText:=' ' + AText;

                  strpcopy(hpchar,'['+AText+']'); strcat(MyDef,hpchar);
               end;
               Laufzeiger:=Laufzeiger^.next;
            until (gefunden = true) or (Laufzeiger = nil);
            worked:=worked + 1;
         until gef_anz = DB_Anzahl;
         DDEHandler.SendPchar(MyDef);
         Spalteninfo_flag:=false;
      end;

      strpcopy(MyImp,'[IMP]');
      { Falls erforderlich wird die Layerinformation an die Datenbank geschickt }
      if DDEHandler.FLayerInfo=1 then
      begin
         AText:=Layer; strpcopy(hpchar,'['+AText+']'); strcat(MyImp,hpchar);
      end
      else
      if DDEHandler.FLayerInfo=2 then
      begin
         { Layerinformation wird als Index gesendet }
         AText:=inttostr(Layerindex); strpcopy(hpchar,'['+AText+']'); strcat(MyImp,hpchar);
      end;

      { Senden des Objektindex an die Datenbank }
      AText:=inttostr(Index);
      while length(AText) < 10 do AText:=' ' + AText;

      strpcopy(hpchar,'['+AText+']'); strcat(MyImp,hpchar);

      { Senden der Objektflaeche an die Datenbank }
      AText:='';
      if Flache <> 0 then
      begin
         Str(Flache:15:2,AText); strpcopy(hpchar,'['+AText+']'); strcat(MyImp,hpchar);
      end
      else
      begin
         strpcopy(hpchar,'[              0]'); strcat(MyImp,hpchar);
      end;

      for i:=1 to anz do
      begin
         if pos_pchar(DDEHandler.fddesepsign,Zeile) <> 0 then copy_pchar(Zeile,1,pos_pchar(DDEHandler.fddesepsign,Zeile)-1,hpchar)
         else
         begin
            copy_pchar(Zeile,1,strlen(Zeile),hpchar);
         end;
         h_SStr1:=strpas(hpchar);

         { Formatieren des Datenbankwertes aufgrund der Datenbanktabelle }
         Laufzeiger:=DB_Anfang; gefunden:=false;
         repeat
            if Laufzeiger^.DB_listeneintrag_info.Position = i then gefunden:=true;
            if gefunden <> true then Laufzeiger:=Laufzeiger^.next;
         until (gefunden = true) or (Laufzeiger = nil);

         if gefunden = true then
         begin
            Laenge:=Laufzeiger^.DB_listeneintrag_info.Laenge;
            FillChar(h_SStr1[Length(h_SStr1)+1],Laenge - Length(h_SStr1),' ');
            h_SStr1[0]:=Char(Laenge);
            strpcopy(hpchar,'['+h_SStr1+']'); strcat(MyImp,hpchar);
         end;
         if pos_pchar(DDEHandler.fddesepsign,Zeile) <> 0 then delete_pchar(Zeile,1,pos_pchar(DDEHandler.fddesepsign,Zeile));
      end;
      DDEHandler.Sendpchar(MyImp);
   end;
   StrDispose(MyDef); strDispose(hpchar); strdispose(MyImp);
end;

{*******************************************************************************
* PROZEDUR : TGPS.GPS_Execute                                                  *
********************************************************************************
* Prozedur zeichnet den erhaltenen DDE String an die entsprechende Stelle ins  *
* Projekt.                                                                     *
*                                                                              *
*                                                                              *
*                                                                              *
*                                                                              *
********************************************************************************}
procedure TGPS.GPS_Execute(AProj:PProj;AData: PChar);
var GPSPos : PGPSInfo;
    GPSData,Dummy,SStr,h_SStr1 : String;
    ShowStr : array[0..255] of char;
    i,j,k   : PChar;
    Code    : Integer;
    Punkt   : TDPoint;
    PPunkt  : TDPoint;
    Index   : LongInt;
    MyIndex : PIndex;
    MySymbol: PSymbol;
    geloscht: boolean;
    APoint  : PPixel;
    ASymbol : PSymbol;
    AIndex  : TIndex;
    SymGroup: PSGroup;
    POS_X,POS_Y : Double;
    CurrentView : TDRect;
    Skalierungsfaktor:double;
    GPSAngle         :real;
    OffsetObjekt     :TOffsetdata;
    OffsetTyp        :TOffsetType;
    Alarmzoom        : boolean;
begin
   Alarmzoom:=false; { Diese Variable wird nur dann aktiviert, wenn das Alarm-Flag gesetzt ist }
   { Wenn die Verbindung nicht aktiv ist wird erhaltener DDE-String nicht ber�cksichtigt }
   if not GPSactive then exit;
   if not GPS_Batchmode then
   begin
      GPS_Ack;
   end;
   GPSPos:=new(PGPSInfo);
   if not ParseGPSInfo(GPSPos,AData) then       { DDE-String zerlegen und in GPSInfo-Struktur speichern}
   begin
      { Wenn DDE-String nicht g�ltig ist wird dieser im Projekt nicht dargestellt }
      dispose(GPSPos);
      exit;
   end;

   { Anzeigen, das GPS-DDE-Signale eintreffen in der Statusline }
   GPSProj^.SetStatusText('GPS ('+GPSPos^.sLong+','+GPSPos^.SLat+')'+ GPSPos^.ID,FALSE);

   { Berechnung der absoluten Position im Projekt }
   { Die Referenzierng wird im Objektdialog Georeferenzierung vorgenommen }
   POS_X:=GPSPos^.Long; POS_Y:=GPSPos^.Lat;
   if not GPSPos^.E_W then POS_Y:=-POS_Y;
   if not GPSPos^.N_S then POS_X:=-POS_X;
   if not Konvertierung(POS_X,POS_Y) then
   begin
      { Wenn die Projektion nicht ordnungsgem�� durchgef�hrt werden kann }
      dispose(GPSPos);
      exit;
   end;

   Punkt.X:=round(POS_X); Punkt.Y:=round(POS_Y);

   { Wenn beim Online-Format die Objekt-ID direkt angegeben wird wird es gleich wie ein Symbol behandelt }
   if GPSPOS^.Darstellung_Obj = Obj_Objekt then
      GPSPOS^.Darstellung_Obj:=Obj_Symbol;

   { Unterscheidung, ob Daten On- bzw. Offline erhalten werden }
   if GPSPOS^.Sendeformat = GPS_Online
   then
   begin
      if GPSPOS^.ID = 'EOD' then
      begin
         // close the line
         if (TheLine <> nil) then
         begin
            if TheLine^.Data^.Count >=2 then
            begin
               TheLine^.ClosePoly; TheLine^.CalculateClipRect; New_Line:=true;
            end;
            Offsetres1:=nil;
            Offsetres2:=nil;
         end;

         { Der Datenbank schicken, da� import beendet ist }
         if (DDEHandler.DDEOK) and (DB_Anzahl <> 0) then
         begin
            DDEHandler.SendString('[END][]');
            Spalteninfo_flag:=true;
         end;
         dispose(GPSPos);
         exit;
      end;

      { Wenn das alte Darstellungsobjekt geloescht werden muss }
      if GPSPOS^.Loesch_flag = true then
      begin
         DeleteIndexObject(GPSPOS^.Objektindex);
         GPSPOS^.Loesch_flag:=false; GPSPOS^.Objektindex:=0;
      end;
      case GPSPOS^.Darstellung_Obj of
         { Einf�gen, oder verschieben eines Punktes auf dem entsprechenden Layer }
         Obj_Punkt :
         begin
            if GPSPos^.Alarm = true then
            begin
               MsgBox(WinGISMainForm.Handle,14037,mb_IconExclamation or mb_OK,GPSPOS^.ID);
               Alarmzoom:=true;
            end;

            if GPSPOS^.Darstellung_Typ = Typ_permanent then
            begin
               { Bei jedem neuen DDE-Befehl wird ein neues Objekt erzeugt }
               APoint:=New(PPixel,Init(Punkt));
               AProj^.InsertObjectOnLayer(aPoint,GPSPOS^.Layerindex);
               GPSPOS^.ObjektIndex:=aPoint^.Index;
               Change_GPSPrioritaeten(GPSPOS, TRUE); // TRUE -> Update position
            end
            else
            begin
               { Verschieben des Punkts }

               { Kontrolle, ob Punkt nicht in der zwischenzeit gel�scht wurde }
               new (MyIndex);
               MyIndex:=New(PIndex,Init(GPSPOS^.Objektindex));
               if AProj^.Layers^.IndexObject(AProj^.PInfo,MyIndex)<>NIL then geloscht:=false
               else geloscht:=true;
               dispose(Myindex,done);

               { Wenn Punkt erst neu erzeugt werden muss }
               if (geloscht = true) then
               begin
                  APoint:=New(PPixel,Init(Punkt));
                  AProj^.InsertObjectOnLayer(aPoint,GPSPOS^.Layerindex);
                  GPSPOS^.ObjektIndex:=aPoint^.Index;
                  Change_GPSPrioritaeten(GPSPOS, TRUE);  // TRUE -> Update Position
               end
               else
               begin
                  { Das Objekt wird verschoben }
                  AIndex.Init(GPSPOS^.ObjektIndex);
                  aPoint:=PPixel(AProj^.Layers^.IndexObject(AProj^.PInfo,@AIndex));
                  aPoint^.Invalidate(AProj^.PInfo);
                  aPoint^.Position.X:=Punkt.X;
                  aPoint^.Position.Y:=Punkt.Y;
                  aPoint^.CalculateClipRect;
                  aPoint^.Invalidate(AProj^.PInfo);
                  AProj^.UpdateClipRect(aPoint);
                  AProj^.CorrectSize(aPoint^.ClipRect,TRUE);
                  AProj^.PInfo^.RedrawInvalidate;
               end;
            end;
            if (GPSPOS^.Autozoom = true) or (Alarmzoom = true) then
            begin
               { Abfrage, ob ein Zoom durchgef�hrt werden muss }
               if (Zoom_view_flag = true) then
               begin
                  { Beim ersten Aufkommen des Objekts muss gezommt werden }
                  FuncZoomToObject(AProj,GPSPOS^.Objektindex);
                  Zoom_view_flag:=false;
               end
               else
               begin
                  { Ermitteln der aktuellen Anzeigerectangles }
                  GPSProj^.PInfo^.GetCurrentScreen(CurrentView);
                  if ((Punkt.X <= CurrentView.A.X) or (Punkt.X >= CurrentView.B.X) or
                     (Punkt.Y <= CurrentView.A.Y) or (Punkt.Y >= CurrentView.B.Y)) or (Alarmzoom = true) then
                  begin
                     { Da sich das Objekt ausserhalb des Anzeigerectangles bewegt hat muss neu gezoomt werden }
                     FuncZoomToObject(AProj,GPSPOS^.Objektindex);
                  end;
               end;
            end;
            { Falls erforderlich wird Information in die Datenbank eingetragen }
            if (DDEHandler.DDEOK) and (DB_Anzahl <> 0) and (strcomp(GPSPOS^.Zusatzinfo,'') <> 0) then
            begin

               Datenbank_einfuegen(GPSPOS^.Zusatzinfo,aPoint^.Index,0,
                                   PtoStr(GPSProj^.Layers^.IndexToLayer(GPSPOS^.Layerindex)^.Text),GPSPOS^.Layerindex);
               strdispose(GPSPOS^.Zusatzinfo);
            end;
         end;

         { Einf�gen, oder verschieben eines Symbols auf dem entsprechenden Layer }
         Obj_Symbol :
         begin
            { Ermitteln der Richtung und des Winkels des Symbols }
            GetGPSAngleDirection(GPSPos^.Last_Long,GPSPos^.Last_Lat,Punkt.X,Punkt.Y,GPSAngle,GpsPos^.Last_Direction);
            GPSPos^.Last_Long:=Punkt.X; GPSPos^.Last_Lat:=Punkt.Y;

            if GpsPos^.New_created_Symbol then
            begin
               GpsPos^.Last_Direction:= Dir_STAND;
               GpsPos^.New_created_Symbol:=false;
               GpsAngle:=0;
            end;
            Change_GPSPrioritaeten(GPSPOS, TRUE); // TRUE -> Update position

            if GPSPos^.Alarm = true then
            begin
               MsgBox(WinGISMainForm.Handle,14037,mb_IconExclamation or mb_OK,GPSPOS^.ID);
               Alarmzoom:=true;
            end;

            if GPSPOS^.Darstellung_Typ = Typ_permanent then
            begin
               { Abfrage, ob Symbolbiblithek nicht leer }
               if PLayer(AProj^.PInfo^.Symbols)^.Data^.GetCount=0 then
                  MsgBox(WinGISMainForm.Handle,2011,mb_IconExclamation or mb_OK,'')
               else
               begin
                  { Nun wird entsprechend der Richtung ein neues Symbol erzeugt }
                  if GpsPos^.Last_Direction = Dir_STAND then
                     ASymbol:=New(PSymbol,Init(AProj^.PInfo,Punkt,GPSPos^.SymbolIndex_stand,1,GPSAngle));
                  if GpsPos^.Last_Direction = Dir_UP    then
                     ASymbol:=New(PSymbol,Init(AProj^.PInfo,Punkt,GPSPos^.SymbolIndex_up,1,GPSAngle));
                  if GpsPos^.Last_Direction = Dir_DOWN  then
                     ASymbol:=New(PSymbol,Init(AProj^.PInfo,Punkt,GPSPos^.SymbolIndex_down,1,GPSAngle));
                  if GpsPos^.Last_Direction = Dir_LEFT  then
                     ASymbol:=New(PSymbol,Init(AProj^.PInfo,Punkt,GPSPos^.SymbolIndex_left,1,GPSAngle));
                  if GpsPos^.Last_Direction = Dir_RIGHT then
                     ASymbol:=New(PSymbol,Init(AProj^.PInfo,Punkt,GPSPos^.SymbolIndex_right,1,GPSAngle));
                  AProj^.InsertObjectOnLayer(aSymbol,GPSPOS^.Layerindex);
                  GPSPOS^.ObjektIndex:=aSymbol^.Index;

                  { Nun mu� an die Datenbank der neue Index �bertragen werden }
                  Dummy:='[GPSID]['+GPSPOS^.ID+']['+inttostr(GPSPos^.ObjektIndex)+']';
                  DDEHandler.Sendstring(Dummy);

                  Change_GPSPrioritaeten(GPSPOS, TRUE); // TRUE -> update position
               end;
            end
            else
            begin
               { Verschieben des Symbols }
               { Kontrolle, ob Symbol nicht in der zwischenzeit gel�scht wurde }
               new (MyIndex);
               MyIndex:=New(PIndex,Init(GPSPOS^.Objektindex));
               if AProj^.Layers^.IndexObject(AProj^.PInfo,MyIndex)<>NIL then geloscht:=false
               else geloscht:=true;
               dispose(MyIndex,done);

               if (geloscht = true) then
               begin
                  { Wenn Symbol erst neu erzeugt werden muss }
                  if PLayer(AProj^.PInfo^.Symbols)^.Data^.GetCount=0 then
                     MsgBox(WinGISMainForm.Handle,2011,mb_IconExclamation or mb_OK,'')
                  else
                  begin
                     { Nun wird entsprechend der Richtung ein neues Symbol erzeugt }
                     if GpsPos^.Last_Direction = Dir_STAND then
                        ASymbol:=New(PSymbol,Init(AProj^.PInfo,Punkt,GPSPos^.SymbolIndex_stand,1,0));
                     if GpsPos^.Last_Direction = Dir_UP    then
                        ASymbol:=New(PSymbol,Init(AProj^.PInfo,Punkt,GPSPos^.SymbolIndex_up,1,0));
                     if GpsPos^.Last_Direction = Dir_DOWN  then
                        ASymbol:=New(PSymbol,Init(AProj^.PInfo,Punkt,GPSPos^.SymbolIndex_down,1,0));
                     if GpsPos^.Last_Direction = Dir_LEFT  then
                        ASymbol:=New(PSymbol,Init(AProj^.PInfo,Punkt,GPSPos^.SymbolIndex_left,1,0));
                     if GpsPos^.Last_Direction = Dir_RIGHT then
                        ASymbol:=New(PSymbol,Init(AProj^.PInfo,Punkt,GPSPos^.SymbolIndex_right,1,0));

                     aSymbol^.Angle:=GPSAngle;

                     AProj^.InsertObjectOnLayer(aSymbol,GPSPOS^.Layerindex);
                     GPSPOS^.ObjektIndex:=aSymbol^.Index;

                     { Nun mu� an die Datenbank der neue Index �bertragen werden }
                     Dummy:='[GPSID]['+GPSPOS^.ID+']['+inttostr(GPSPos^.ObjektIndex)+']';
                     DDEHandler.Sendstring(Dummy);

                     Change_GPSPrioritaeten(GPSPOS, TRUE); // TRUE -> means update position
                  end;
               end
               else
               begin
                  { Verschieben des Symbols an die neue Position }
                  AIndex.Init(GPSPOS^.ObjektIndex);
                  aSymbol:=PSymbol(AProj^.Layers^.IndexObject(AProj^.PInfo,@AIndex));

                  aSymbol^.Invalidate(AProj^.PInfo);
                  aSymbol^.Angle:=GPSAngle;

                  if GPSPos^.Last_Direction = DIR_STAND then
                  begin
                     aSymbol^.SetSymPtrByIndex(AProj^.PInfo^.Symbols,GPSPos^.Symbolindex_stand);
                     aSymbol^.SymIndex:=GPSPos^.Symbolindex_stand;
                  end;
                  if GPSPos^.Last_Direction = DIR_UP    then
                  begin
                     aSymbol^.SetSymPtrByIndex(AProj^.PInfo^.Symbols,GPSPos^.Symbolindex_up);
                     aSymbol^.SymIndex:=GPSPos^.Symbolindex_up;
                  end;
                  if GPSPos^.Last_Direction = DIR_DOWN  then
                  begin
                     aSymbol^.SetSymPtrByIndex(AProj^.PInfo^.Symbols,GPSPos^.Symbolindex_down);
                     aSymbol^.SymIndex:=GPSPos^.Symbolindex_down;
                  end;
                  if GPSPos^.Last_Direction = DIR_LEFT  then
                  begin
                     aSymbol^.SetSymPtrByIndex(AProj^.PInfo^.Symbols,GPSPos^.Symbolindex_left);
                     aSymbol^.SymIndex:=GPSPos^.Symbolindex_left;
                  end;
                  if GPSPos^.Last_Direction = DIR_RIGHT then
                  begin
                     aSymbol^.SetSymPtrByIndex(AProj^.PInfo^.Symbols,GPSPos^.Symbolindex_right);
                     aSymbol^.SymIndex:=GPSPos^.Symbolindex_right;
                  end;

                  aSymbol^.Position.X:=Punkt.X;
                  aSymbol^.Position.Y:=Punkt.Y;
                  aSymbol^.CalculateClipRect(AProj^.PInfo);
                  aSymbol^.Invalidate(AProj^.PInfo);
                  AProj^.UpdateClipRect(aSymbol);
                  AProj^.CorrectSize(aSymbol^.ClipRect,TRUE);
                  AProj^.PInfo^.RedrawInvalidate;
                end;
            end;
            if (GPSPOS^.Autozoom = true) or (Alarmzoom = true) then
            begin
               { Abfrage, ob ein Zoom durchgef�hrt werden muss }
               if (Zoom_view_flag = true) then
               begin
                  { Beim ersten Aufkommen des Objekts muss gezommt werden }
                  FuncZoomToObject(AProj,GPSPOS^.Objektindex);
                  Zoom_view_flag:=false;
               end
               else
               begin
                  { Ermitteln der aktuellen Anzeigerectangles }
                  GPSProj^.PInfo^.GetCurrentScreen(CurrentView);

                  { Kontrolle, ob sich das Objekt noch innerhalb des aktuellen Anzeigerectangles befindet }
                  if ((Punkt.X <= CurrentView.A.X) or (Punkt.X >= CurrentView.B.X) or
                     (Punkt.Y <= CurrentView.A.Y) or (Punkt.Y >= CurrentView.B.Y)) or (Alarmzoom = true) then
                  begin
                     { Da sich das Objekt ausserhalb des Anzeigerectangles bewegt hat muss neu gezoomt werden }
                     FuncZoomToObject(AProj,GPSPOS^.Objektindex);
                  end;
               end;
            end;

            { Falls erforderlich wird Information in die Datenbank eingetragen }
            if (DDEHandler.DDEOK) and (DB_Anzahl <> 0) and (strcomp(GPSPOS^.Zusatzinfo,'') <> 0) then
            begin
               Datenbank_einfuegen(GPSPOS^.Zusatzinfo,aSymbol^.Index,0,
                                   PtoStr(GPSProj^.Layers^.IndexToLayer(GPSPOS^.Layerindex)^.Text),GPSPOS^.Layerindex);
               strdispose(GPSPos^.Zusatzinfo);
            end;
         end;
         { Einf�gen einer Linie auf den aktuellen Layer }
         Obj_Linie  :
         begin
            { Wenn eine Linie neu angefangen wird muss sie erst erzeugt werden }
            if (New_Line = true) then
            begin
               TheLine:=new(PPoly,Init);
               New_Line:=false;
            end;
            TheLine^.InsertPoint(Punkt);
            if TheLine^.Data^.Count=2 then
            begin
               LastLayerindex:=GPSPOS^.Layerindex;
               if not AProj^.InsertObjectOnLayer(TheLine,GPSPOS^.Layerindex) then
               begin
                  Dispose(TheLine,Done);
                  TheLine:=NIL;
                  Exit;
               end;
            end;

            // redraw the line to see the changes
            TheLine^.CalculateClipRect;
            TheLine^.Invalidate(AProj^.PInfo);
            AProj^.UpdateClipRect(TheLine);
            AProj^.CorrectSize(TheLine^.ClipRect,TRUE);
            AProj^.PInfo^.RedrawInvalidate;
            TheLine^.Draw(AProj^.Pinfo,TRUE);

            if Offset_flag = true then
            begin
               { Zeichnen des Offsets }
               if Offset_Ecke = 1 then OffsetObjekt.RoundEdges:=true;
               if Offset_Ecke = 2 then OffsetObjekt.RoundEdges:=false;

               if Offset_Linie = 1 then OffsetTyp:=otSingle;
               if Offset_Linie = 2 then OffsetTyp:=otStreight;
               if Offset_Linie = 3 then OffsetTyp:=otStreightDistance;
               if Offset_Linie = 4 then OffsetTyp:=otRounded;
               OffsetObjekt.Offsettype:=OffsetTyp;

               if TheLine^.Data^.Count >=2 then
               begin
                  // delete the Offsetres if they already exist
                  if Offsetres1 <> nil then
                     DeleteIndexObject(Offsetres1^.Index);
                  if Offsetres2 <> nil then
                     DeleteIndexObject(Offsetres2^.Index);
                  case DoCreateOffset(AProj,TheLine,Offset_Abstand*100,OffsetObjekt,Offsetres1,Offsetres2) of
                       1 : begin
                              AProj^.InsertObjectOnLayer(Offsetres1,GPSPOS^.Layerindex);
                              Offsetres1^.CalculateClipRect;
                              Offsetres1^.Invalidate(AProj^.PInfo);
                              AProj^.UpdateClipRect(Offsetres1);
                              AProj^.CorrectSize(Offsetres1^.ClipRect,TRUE);
                              AProj^.PInfo^.RedrawInvalidate;
                              Offsetres1^.Draw(AProj^.Pinfo,TRUE);
                           end;
                       2 : begin
                              Offsetres1^.CalculateClipRect;
                              Offsetres1^.Invalidate(AProj^.PInfo);
                              AProj^.UpdateClipRect(Offsetres1);
                              AProj^.CorrectSize(Offsetres1^.ClipRect,TRUE);
                              AProj^.PInfo^.RedrawInvalidate;
                              Offsetres1^.Draw(AProj^.Pinfo,TRUE);
                              dispose(Offsetres1,done);

                              Offsetres2^.CalculateClipRect;
                              Offsetres2^.Invalidate(AProj^.PInfo);
                              AProj^.UpdateClipRect(Offsetres2);
                              AProj^.CorrectSize(Offsetres2^.ClipRect,TRUE);
                              AProj^.PInfo^.RedrawInvalidate;
                              Offsetres2^.Draw(AProj^.Pinfo,TRUE);
                              // dispose(Offsetres2,done);
                           end;

                  end;

               end;
            end;

            if (DDEHandler.DDEOK) and (DB_Anzahl <> 0) and (strcomp(GPSPOS^.Zusatzinfo,'')<> 0) then
            begin
               {Damit man in die Datenbank einf�gen kann mu� ein Hilfs-Punkt erzeugt werden }
               APoint:=New(PPixel,Init(Punkt));
               AProj^.InsertObjectOnLayer(APoint,GPSPOS^.Layerindex);
               Datenbank_einfuegen(GPSPOS^.Zusatzinfo,aPoint^.Index,0,
                                   PtoStr(GPSProj^.Layers^.IndexToLayer(GPSPOS^.Layerindex)^.Text),GPSPOS^.Layerindex);
               strdispose(GPSPos^.Zusatzinfo);
            end;
         end;
      end;
   end
   else
   begin
      { Daten werden im Offlineformat eingegeben }

      { Abfrage, ob Endemarkierung des Offlinebetriebes angekommen ist }
      if GPSPOS^.ID = 'EOD' then
      begin
         if Last_Objekttyp = Obj_Flaeche then
         begin
            { Fl�che schliessen }
            Flaeche^.ClosePoly;
            Flaeche^.CalculateClipRect;
            AProj^.InsertObject(Flaeche,FALSE);

            { Wenn ein Objekt abgeschlossen ist soll darauf gezoomt werden }
            if Flaeche^.Flaeche <> 0 then FuncZoomToObject(AProj,Flaeche^.Index);

            if Datenbankinfo <> nil then { Wenn Datenbankinformation vorhanden ist }
            begin
                { Einf�gen in die Datenbank }
               Datenbank_einfuegen(Datenbankinfo,Flaeche^.Index,Flaeche^.Flaeche,
                                   PtoStr(GPSProj^.Layers^.Toplayer^.Text),GPSProj^.Layers^.Toplayer^.Index);
               strdispose(Datenbankinfo);
            end;
         end;

         if Last_Objekttyp = Obj_Linie   then
         begin
            { Polyline schliessen }
            Linie^.ClosePoly;
            Linie^.CalculateClipRect;
            AProj^.InsertObject(Linie,FALSE);

            { Wenn ein Objekt abgeschlossen ist soll darauf gezoomt werden }
            FuncZoomToObject(AProj,Linie^.Index);

            if Datenbankinfo <> nil then { Wenn Datenbankinformation vorhanden ist }
            begin
               { Einf�gen in die Datenbank }
               Datenbank_einfuegen(Datenbankinfo,Linie^.Index,Linie^.Laenge,
                                   PtoStr(GPSProj^.Layers^.Toplayer^.Text),GPSProj^.Layers^.Toplayer^.Index);
               strdispose(Datenbankinfo);
            end;
         end;
         Last_Objekttyp := -1;
         Last_Objektnummer:= -1;

         { Senden des Endes der Information an die Datenbank }
         if (DDEHandler.DDEOK) and (DB_Anzahl <> 0) then
         begin
            DDEHandler.SendString('[END][]');
            Spalteninfo_flag:=true;
         end;
      end
      else
      case GPSPOS^.Darstellung_Obj of
         { Einf�gen eines Punktes auf den aktuellen Layer }
         Obj_Punkt :
         begin
            { Abfrage welches Objekt zuvor gezeichnet wurde }
            if Last_Objekttyp = Obj_Flaeche then
            begin
               { Fl�che schliessen }
               Flaeche^.ClosePoly;
               Flaeche^.CalculateClipRect;
               AProj^.InsertObject(Flaeche,FALSE);

               { Wenn ein Objekt abgeschlossen ist soll darauf gezoomt werden }
               if Flaeche^.Flaeche <> 0 then FuncZoomToObject(AProj,Flaeche^.Index);

               if Datenbankinfo <> nil then { Wenn Datenbankinformation vorhanden ist }
               begin
                  { Einf�gen in die Datenbank }
                  Datenbank_einfuegen(Datenbankinfo,Flaeche^.Index,Flaeche^.Flaeche,
                                      PtoStr(GPSProj^.Layers^.Toplayer^.Text),GPSProj^.Layers^.Toplayer^.Index);
                  strdispose(Datenbankinfo);
               end;
            end;
            if Last_Objekttyp = Obj_Linie   then
            begin
               { Polyline schliessen }
               Linie^.ClosePoly;
               Linie^.CalculateClipRect;
               AProj^.InsertObject(Linie,FALSE);

               { Wenn ein Objekt abgeschlossen ist soll darauf gezoomt werden }
               FuncZoomToObject(AProj,Linie^.Index);

               if (Datenbankinfo <> nil) then { Wenn Datenbankinfo vorhanden ist }
               begin
                  { Einf�gen in die Datenbank }
                  Datenbank_einfuegen(Datenbankinfo,Linie^.Index,Linie^.Laenge,
                                      PtoStr(GPSProj^.Layers^.Toplayer^.Text),GPSProj^.Layers^.Toplayer^.Index);
                  strdispose(Datenbankinfo);
               end;
            end;

            APoint:=New(PPixel,Init(Punkt));
            AProj^.InsertObject(APoint,FALSE);
            Last_Objektnummer:=GPSPos^.Objektnummer;
            Last_Objekttyp:=Obj_Punkt;

            { Wenn ein Objekt abgeschlossen ist soll darauf gezoomt werden }
            FuncZoomToObject(AProj,APoint^.Index);

            if GPSPos^.Zusatzinfo <> nil then { Wenn Datenbankinformation vorhanden ist }
            begin
               { Einf�gen in die Datenbank }
               Datenbank_einfuegen(GPSPos^.Zusatzinfo,APoint^.Index,0,
                                   PtoStr(GPSProj^.Layers^.Toplayer^.Text),GPSProj^.Layers^.Toplayer^.Index);
               strdispose(GPSPos^.Zusatzinfo);
            end;
         end;

         { Einf�gen eines Symbols auf den aktuellen Layer }
         Obj_Symbol :
         begin
            { Abfrage welches Objekt zuvor gezeichnet wurde }
            if Last_Objekttyp = Obj_Flaeche then
            begin
               { Fl�che schliessen }
               Flaeche^.ClosePoly;
               Flaeche^.CalculateClipRect;
               AProj^.InsertObject(Flaeche,FALSE);

               { Wenn ein Objekt abgeschlossen ist soll darauf gezoomt werden }
               if Flaeche^.Flaeche <> 0 then FuncZoomToObject(AProj,Flaeche^.Index);

               if Datenbankinfo <> nil then { Wenn Datenbankinformation vorhanden ist }
               begin
                  { Einf�gen in die Datenbank }
                  Datenbank_einfuegen(Datenbankinfo,Flaeche^.Index,Flaeche^.Flaeche,
                                      PtoStr(GPSProj^.Layers^.Toplayer^.Text),GPSProj^.Layers^.Toplayer^.Index);
                  strdispose(Datenbankinfo);
               end;
            end;
            if Last_Objekttyp = Obj_Linie then
            begin
               { Polygon schliessen }
               Linie^.ClosePoly;
               Linie^.CalculateClipRect;
               AProj^.InsertObject(Linie,FALSE);

               { Wenn ein Objekt abgeschlossen ist soll darauf gezoomt werden }
               FuncZoomToObject(AProj,Linie^.Index);

               if Datenbankinfo <> nil then { Wenn Datenbankinformation vorhanden ist }
               begin
                  { Einf�gen in die Datenbank }
                  Datenbank_einfuegen(Datenbankinfo,Linie^.Index,Linie^.Laenge,
                                      PtoStr(GPSProj^.Layers^.Toplayer^.Text),GPSProj^.Layers^.Toplayer^.Index);
                  strdispose(Datenbankinfo);
               end;
            end;

            if PLayer(AProj^.PInfo^.Symbols)^.Data^.GetCount=0 then
               MsgBox(WinGISMainForm.Handle,2011,mb_IconExclamation or mb_OK,'')
            else
            begin
               ASymbol:=New(PSymbol,Init(AProj^.PInfo,Punkt,AProj^.ActualSym^.Index,1,0));
               AProj^.InsertObject(ASymbol,FALSE);
               Last_Objektnummer:=GPSPos^.Objektnummer;
               Last_Objekttyp:=Obj_Symbol;

               { Wenn ein Objekt abgeschlossen ist soll darauf gezoomt werden }
               FuncZoomToObject(AProj,aSymbol^.Index);

               if GPSPos^.Zusatzinfo <> nil then { Wenn Datenbankinformation vorhanden ist }
               begin
                  { Einf�gen in die Datenbank }
                  Datenbank_einfuegen(GPSPos^.Zusatzinfo,ASymbol^.Index,0,
                                      PtoStr(GPSProj^.Layers^.Toplayer^.Text),GPSProj^.Layers^.Toplayer^.Index);
                  strdispose(GPSPos^.Zusatzinfo);
               end;
            end;
         end;

         { Einf�gen einer Linie auf den aktuellen Layer }
         Obj_Linie  :
         begin
            { Abfrage welches Objekt zuvor gezeichnet wurde }
            if (Last_Objekttyp = Obj_Flaeche) and (GPSPos^.Objektnummer <> Last_Objektnummer) then
            begin
               { Fl�che schliessen }
               Flaeche^.ClosePoly;
               Flaeche^.CalculateClipRect;
               AProj^.InsertObject(Flaeche,FALSE);

               { Wenn ein Objekt abgeschlossen ist soll darauf gezoomt werden }
               if Flaeche^.Flaeche <> 0 then FuncZoomToObject(AProj,Flaeche^.Index);

               if Datenbankinfo <> nil then { Wenn Datenbankinformation vorhanden ist }
               begin
                  { Einf�gen in die Datenbank }
                  Datenbank_einfuegen(Datenbankinfo,Flaeche^.Index,Flaeche^.Flaeche,
                                      PtoStr(GPSProj^.Layers^.Toplayer^.Text),GPSProj^.Layers^.Toplayer^.Index);
                  strdispose(Datenbankinfo);
               end;
            end;
            if (Last_Objekttyp = Obj_Linie  ) and (GPSPos^.Objektnummer <> Last_Objektnummer) then
            begin
               { Polyline schliessen }
               Linie^.ClosePoly;
               Linie^.CalculateClipRect;
               AProj^.InsertObject(Linie,FALSE);

               { Wenn ein Objekt abgeschlossen ist soll darauf gezoomt werden }
               FuncZoomToObject(AProj,Linie^.Index);

               if Datenbankinfo <> nil then { Wenn Datenbankinformation vorhanden ist }
               begin
                  { Einf�gen in die Datenbank }
                  Datenbank_einfuegen(Datenbankinfo,Linie^.Index,Linie^.Laenge,
                                      PtoStr(GPSProj^.Layers^.Toplayer^.Text),GPSProj^.Layers^.Toplayer^.Index);
                  strdispose(Datenbankinfo);
               end;
            end;

            { Wenn eine Linie neu angefangen wird muss sie erst erzeugt werden }
            if (Last_Objektnummer <> GPSPos^.Objektnummer) or (Last_Objekttyp <> Obj_Linie) then
            begin
               { Wenn eine Linie neu angefangen wird muss sie erst erzeugt werden }
               Linie:=New(PPoly,Init);
               if GPSPos^.Zusatzinfo <> nil then
               begin
                  Datenbankinfo:=stralloc(strlen(GPSPos^.Zusatzinfo)+1);
                  strcopy(Datenbankinfo,GPSPOS^.Zusatzinfo);
               end
               else Datenbankinfo:=nil;
            end;
            { Einf�gen des Punktes }
            PPunkt.Init(Punkt.X,Punkt.Y);
            Linie^.InsertPoint(PPunkt);

            { Linie^.Draw(AProj^.Pinfo,TRUE); }

            Last_Objektnummer:=GPSPos^.Objektnummer;
            Last_Objekttyp:=Obj_Linie;
         end;

         { Einf�gen einer Fl�che auf den aktuellen Layer }
         Obj_Flaeche  :
         begin
            if (Last_Objekttyp = Obj_Flaeche) and (GPSPos^.Objektnummer <> Last_Objektnummer) then
            begin
               { Fl�che schliessen }
               Flaeche^.ClosePoly;
               Flaeche^.CalculateClipRect;
               AProj^.InsertObject(Flaeche,FALSE);

               { Wenn ein Objekt abgeschlossen ist soll darauf gezoomt werden }
               if Flaeche^.Flaeche <> 0 then FuncZoomToObject(AProj,Flaeche^.Index);

               if Datenbankinfo <> nil then { Wenn Datenbankinformation vorhanden ist }
               begin
                  { Einf�gen in die Datenbank }
                  Datenbank_einfuegen(Datenbankinfo,Flaeche^.Index,Flaeche^.Flaeche,
                                      PtoStr(GPSProj^.Layers^.Toplayer^.Text),GPSProj^.Layers^.Toplayer^.Index);
                  strdispose(Datenbankinfo);
               end;
            end;
            if (Last_Objekttyp = Obj_Linie  ) and (GPSPos^.Objektnummer <> Last_Objektnummer) then
            begin
               { Polyline schliessen }
               Linie^.ClosePoly;
               Linie^.CalculateClipRect;
               AProj^.InsertObject(Linie,FALSE);

               { Wenn ein Objekt abgeschlossen ist soll darauf gezoomt werden }
               FuncZoomToObject(AProj,Linie^.Index);

               if Datenbankinfo <> nil then { Wenn Datenbankinformation vorhanden ist }
               begin
                  { Einf�gen in die Datenbank }
                  Datenbank_einfuegen(Datenbankinfo,Linie^.Index,Linie^.Laenge,
                                      PtoStr(GPSProj^.Layers^.Toplayer^.Text),GPSProj^.Layers^.Toplayer^.Index);
                  strdispose(Datenbankinfo);
               end;
            end;

            if (Last_Objektnummer <> GPSPos^.Objektnummer) or (Last_Objekttyp <> Obj_Flaeche)then
            begin
               { Wenn eine Fl�che neu angefangen wird muss sie erst erzeugt werden }
               if GPSPos^.Zusatzinfo <> nil then
               begin
                  Datenbankinfo:=stralloc(strlen(GPSPos^.Zusatzinfo)+1);
                  strcopy(Datenbankinfo,GPSPOS^.Zusatzinfo);
               end
               else Datenbankinfo:=nil;
               Flaeche:=New(PCPoly,Init);
            end;
            { Einf�gen des Punktes }
            PPunkt.Init(Punkt.X,Punkt.Y);
            Flaeche^.InsertPoint(PPunkt);

            { Flaeche^.Draw(AProj^.Pinfo,TRUE); }

            Last_Objektnummer:=GPSPos^.Objektnummer;
            Last_Objekttyp:=Obj_Flaeche;
         end;
      end;
   end;
   dispose(GPSPos);
end;

{*******************************************************************************
* PROZEDUR : TGPS.GpsProASC                                                    *
********************************************************************************
* Prozedur rechnet die Projektion eines ASC-Files um                           *
*                                                                              *
* InASC     ->  Dateiname der Eingabe ASC Datei                                *
* InADF     ->  Dateiname der Eingabe ADF Datei                                *
* OutASC    ->  Dateiname der Ausgabe ASC Datei                                *
*                                                                              *
* R�ckgabe  ->  keine                                                          *
********************************************************************************}
procedure TGPS.GpsProASC(InASC:string;InADF:string;OutASC:string);
var X_von_old,X_bis_old,Y_von_old,Y_bis_old:integer;
    Datei:file of byte;
    Bytes2work,Bytesworked:longint;
    ASCInputfile,ASCOutputfile:Textfile;
    ASCZeile:string;
    Prozent,count:integer;
    OldProzent:integer;
    X_str,Y_str,Teil1,Teil2,Teil3:string;
    X_Wert,Y_Wert:double;
begin
   { Ermitteln der Einstellungen des ADF-Files }
   ScanADF(InADF,X_von_old,X_bis_old,Y_von_old,Y_bis_old);

   { Ermitteln der Gr��e des ASC-Files }
   AssignFile(Datei,InASC); Reset(Datei); Bytes2work:=FileSize(Datei);
   Closefile(Datei);
   Bytesworked:=0;
   OldProzent:=0;

   { Aufmachen der Dateien }
   Assignfile(ASCInputfile,InASC); Reset(ASCInputfile);
   Assignfile(ASCOutputfile,OutASC); Rewrite(ASCOutputfile);

   DDEHandler.SendString('[GPSPROASC][INFO][STARTED]');

   { Nun wird die Datei eingelesen und umkonvertiert }
   repeat
      { Einlesen eines Elements }
      Readln(ASCInputfile,ASCZeile); Bytesworked:=Bytesworked + length(ASCZeile) + 2;
      if Bytes2work > 0 then Prozent:= Round (Bytesworked / Bytes2work * 100 ) else Prozent:= 100;

      if Prozent <> OldProzent then { Bei jedem Prozentwechsel wird ein DDE-Kommando �ber den Prozentstatus gesendet }
      begin
         DDEHandler.SendString('[GPSPROASC][PERCENT]['+inttostr(Prozent)+']');
         OldProzent:=Prozent;
      end;

      { Extrahieren der Koordinaten }
      X_str:=copy(ASCZeile,X_von_old,X_bis_old-X_von_old + 1); val(X_str,X_Wert,count); if count <> 0 then X_Wert:=0;
      Y_str:=copy(ASCZeile,Y_von_old,Y_bis_old-Y_von_old + 1); val(Y_str,Y_Wert,count); if count <> 0 then Y_Wert:=0;

      { Nun wird umgerechnet }
      Konvertierung(X_Wert,Y_Wert);
      X_str:=floattostr(Y_Wert/100); Y_str:=floattostr(X_Wert/100);

      { Nun muss der neue Wert ins ASC-File eingetragen werden }
      repeat X_str:=' '+X_str; until length(X_str) >=15;
      repeat Y_str:=' '+Y_str; until length(Y_str) >=15;

      if X_von_old < Y_von_old then
      begin
         Teil1:=copy(ASCZeile,1,x_von_old - 1);
         Teil2:=copy(ASCZeile,x_bis_old + 1,y_von_old - x_bis_old - 1);
         Teil3:=copy(ASCZeile,y_bis_old + 1,length(ASCZeile));
      end
      else
      begin
         Teil1:=copy(ASCZeile,1,y_von_old - 1);
         Teil2:=copy(ASCZeile,y_bis_old + 1,x_von_old - y_bis_old - 1);
         Teil3:=copy(ASCZeile,x_bis_old + 1,length(ASCZeile));
      end;

      { Schreiben der neuen ASC-Zeile }
      write(ASCOutputfile,Teil1);
      if x_von_old < y_von_old then write(ASCOutputfile,X_str) else write(ASCoutputfile,Y_str);
      write(ASCOutputfile,Teil2);
      if x_von_old < y_von_old then write(ASCOutputfile,Y_str) else write(ASCOutputfile,X_str);
      writeln(ASCOutputfile,Teil3);
    until EOF(ASCInputfile);
    Closefile(ASCInputfile); Closefile(ASCOutputfile);
    CreateADF(InADF,OutASC,X_von_old,X_bis_old,Y_von_old,Y_bis_old);
    DDEHandler.SendString('[GPSPROASC][INFO][FINISHED]');
end;

{*******************************************************************************
* PROZEDUR : TGPS.ScanADF                                                      *
********************************************************************************
* Prozedur analysiert das ADF-File.                                            *
*                                                                              *
* Dateiname ->  Dateiname der ADF-Datei, die Analysiert werden soll.           *
* X_von     ->  Vonwert der X-Koordinate                                       *
* X_bis     ->  Biswert der X-Koordinate                                       *
* Y_von     ->  Vonwert der Y-Koordinate                                       *
* Y_bis     ->  Biswert der Y-Koordiante                                       *
*                                                                              *
* R�ckgabe  ->  keine                                                          *
********************************************************************************}
procedure TGPS.ScanADF(Dateiname:string;var X_von:integer; var X_bis:integer; var Y_von:integer; var Y_bis:integer);
var ADFDatei:Textfile;
    Zeile:string;
    hpchar:pchar;
begin
   Assignfile(ADFDatei,Dateiname);
   Reset(ADFDatei);
   repeat
      readln(ADFDatei,Zeile);

      hpchar:=stralloc(length(Zeile)+1);
      strpcopy(hpchar,Zeile); hpchar:=strupper(hpchar); Zeile:=strpas(hpchar);
      strdispose(hpchar);
      while (copy(Zeile,1,1) = ' ') do Delete(Zeile,1,1); { L�schen der Leerzeichen vorne }
      while (copy(Zeile,length(Zeile)-1,1) = ' ') do Delete(Zeile,length(Zeile)-1,1); { L�schen der Leerzeichen hinten }

      if (Zeile = 'X-COORDINATE') or (Zeile = 'X-KOORDINATE') then
      begin
         readln(ADFDatei,Zeile);
         X_von:=strtoint(copy(Zeile,1,3));
         X_bis:=strtoint(copy(Zeile,5,3));
      end;

      if (Zeile = 'Y-COORDINATE') or (Zeile = 'Y-KOORDINATE') then
      begin
         readln(ADFDatei,Zeile);
         Y_von:=strtoint(copy(Zeile,1,3));
         Y_bis:=strtoint(copy(Zeile,5,3));
      end;
   until EOF(ADFDatei);
   Closefile(ADFDatei);
end;

{*******************************************************************************
* PROZEDUR : TGPS.CreateADF                                                    *
********************************************************************************
* Prozedur erzeugt ein neues ADF-File.                                         *
*                                                                              *
* InADF     ->  Dateiname der alten ADF-Datei.                                 *
* OutASC    ->  Dateiname der neuen ASC-Datei, zu der ADF erzeugt wird.        *
* X_von     ->  Vonwert der X-Koordinate                                       *
* X_bis     ->  Biswert der X-Koordinate                                       *
* Y_von     ->  Vonwert der Y-Koordinate                                       *
* Y_bis     ->  Biswert der Y-Koordiante                                       *
*                                                                              *
* R�ckgabe  ->  keine                                                          *
********************************************************************************}
procedure TGPS.CreateADF(InADF:string;OutASC:string;X_von_old:integer;X_bis_old:integer;Y_von_old:integer;Y_bis_old:integer);
var oldADF:Textfile;
    newADF:Textfile;
    newFilename,Teil1,Teil2:string;
    Zeile :string;
    akt_von,akt_bis,oldvon,oldbis,count,x_offset,y_offset:integer;
    change_adf:boolean;
    hpchar:pchar;
begin
   x_offset:=15 - (x_bis_old - x_von_old) - 1;
   y_offset:=15 - (y_bis_old - y_von_old) - 1;

   Assignfile(oldADF,InADF);
   Reset(oldADF);

   newFilename:=Changefileext(OutASC,'.ADF');

   hpchar:=stralloc(255);
   strpcopy(hpchar,newFilename); hpchar:=strupper(hpchar); newFilename:=strpas(hpchar);
   strpcopy(hpchar,InADF); hpchar:=strupper(hpchar); InADF:=strpas(hpchar);
   strdispose(hpchar);

   if newFilename = InADF then
   begin
      change_adf:=true;
      newFilename:=extractfilepath(InADF) + 'Sepp.ADF';
   end
   else change_adf:=false;
   Assignfile(newADF,newFilename);
   Rewrite(newADF);
   repeat
      readln(oldADF,Zeile); hpchar:=stralloc(length(Zeile)+1);
      strpcopy(hpchar,Zeile); hpchar:=strupper(hpchar); Zeile:=strpas(hpchar); strdispose(hpchar);
      if (Zeile = 'X-COORDINATE') or (Zeile = 'X-KOORDINATE') then
      begin
         writeln(newADF,Zeile);
         readln(oldADF,Zeile);
         val(copy(Zeile,1,3),oldvon,count); val(copy(Zeile,5,3),oldbis,count);
         { Wenn die Y-Koordinate vor der X-Koordinate im ASC-File liegt }
         if (y_von_old < x_von_old) then begin akt_von:=oldvon + y_offset; akt_bis:=oldbis + y_offset + x_offset; end
         else begin akt_von:=oldvon; akt_bis:=oldbis + x_offset; end;

         Teil1:=inttostr(akt_von);
         if length(Teil1) < 3 then repeat Teil1:='0' + Teil1; until length(Teil1) = 3;
         Teil2:=inttostr(akt_bis);
         if length(Teil2) < 3 then repeat Teil2:='0' + Teil2; until length(Teil2) = 3;
         Zeile:=Teil1 + ' ' + Teil2;
         writeln(newADF,Zeile);

         akt_von:=akt_bis + 1;
      end
      else
      if (Zeile = 'Y-COORDINATE') or (Zeile = 'Y-KOORDINATE') then
      begin
         writeln(newADF,Zeile);
         readln(oldADF,Zeile);
         val(copy(Zeile,1,3),oldvon,count); val(copy(Zeile,5,3),oldbis,count);
         { Wenn die Y-Koordinate vor der X-Koordinate im ASC-File liegt }
         if (x_von_old < y_von_old) then begin akt_von:=oldvon + x_offset; akt_bis:=oldbis + x_offset + y_offset; end
         else begin akt_von:=oldvon; akt_bis:=oldbis + y_offset; end;

         Teil1:=inttostr(akt_von);
         if length(Teil1) < 3 then repeat Teil1:='0' + Teil1; until length(Teil1) = 3;
         Teil2:=inttostr(akt_bis);
         if length(Teil2) < 3 then repeat Teil2:='0' + Teil2; until length(Teil2) = 3;
         Zeile:=Teil1 + ' ' + Teil2;
         writeln(newADF,Zeile);
         akt_von:=akt_bis + 1;
      end
      else
      begin
         writeln(newADF,Zeile);
         if (Zeile <> 'SYMBOLSCALE') and (Zeile <> '') and (Zeile <> 'BITMAPFILENAME') then
         begin
            readln(oldADF,Zeile);
            val(copy(Zeile,1,3),oldvon,count); val(copy(Zeile,5,3),oldbis,count);

            { Ermitteln von akt_von } akt_von:=oldvon;
            if (oldvon > x_von_old) then akt_von:=akt_von + x_offset;
            if (oldvon > y_von_old) then akt_von:=akt_von + y_offset;

            { Ermitteln von akt_bis } akt_bis:=oldbis;
            if (oldbis > x_bis_old) then akt_bis:=akt_bis + x_offset;
            if (oldbis > y_bis_old) then akt_bis:=akt_bis + y_offset;

            Teil1:=inttostr(akt_von);
            if length(Teil1) < 3 then repeat Teil1:='0' + Teil1; until length(Teil1) = 3;
            Teil2:=inttostr(akt_bis);
            if length(Teil2) < 3 then repeat Teil2:='0' + Teil2; until length(Teil2) = 3;
            Zeile:=Teil1 + ' ' + Teil2;
            writeln(newADF,Zeile);
         end
         else
         begin
            if Zeile <> '' then begin readln(oldADF,Zeile); writeln(newADF,Zeile); end;
         end;
      end;
   until EOF(oldADF);
   Closefile(oldADF); Closefile(newADF);

   if change_adf = true then
   begin
      Assignfile(oldADF,newFilename);
      Reset(oldADF);
      Assignfile(newADF,InADF);
      Rewrite(newADF);
      repeat
         readln(oldADF,Zeile); writeln(newADF,Zeile);
      until EOF(oldADF);
      Closefile(oldADF); closefile(newADF);
      Deletefile(newFilename);

      { Erzeugen des .SYM - Files }
      if not Fileexists(Changefileext(InADF,'.SYM')) then
      begin
         Assignfile(OldADF,Changefileext(InADF,'.SYM'));
         Rewrite(OldADF);
         Writeln(OldADF,'0000000001 0000000001');
         Closefile(OldADF);
      end;
   end
   else
   begin
      { Erzeugen des .SYM - Files }
      if not Fileexists(Changefileext(newFilename,'.SYM')) then
      begin
         Assignfile(OldADF,Changefileext(newFilename,'.SYM'));
         Rewrite(OldADF);
         Writeln(OldADF,'0000000001 0000000001');
         Closefile(OldADF);
      end;
   end;
end;


end.

