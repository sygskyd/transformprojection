�
 TLINESTYLEDIALOG 0�  TPF0TLineStyleDialogLineStyleDialogLeftKTop� HelpContext�BorderStylebsDialogCaption!1ClientHeight� ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrder	PositionpoScreenCenterOnCloseQueryFormCloseQueryOnCreate
FormCreate	OnDestroyFormDestroyOnHideFormHideOnShowFormShowPixelsPerInch`
TextHeight TBevelBevel1LeftTop� WidthyHeightShapebsBottomLine  TWLabelWLabel1Left� Top-Width	HeightCaption!2  TButton	CancelBtnLeft(Top� WidthYHeightCancel	Caption!5ModalResultTabOrder  TPanelControlsPanelLeft Top Width�Height� 
BevelOuterbvNoneTabOrder  TWLabel
ScaleLabelLeft� TopdWidth9HeightAutoSizeCaption!12FocusControl	ScaleEdit  TWLabel
WidthLabelLeft� TopLWidth9HeightAutoSizeCaption!3FocusControl	WidthEdit  TWLabelFillColorLabelLeft� Top-Width9HeightAutoSizeCaption!6FocusControlFillColorComboBtn  TWLabelLabel3Left� TopWidth9HeightAutoSizeCaption!2  	TCheckBoxScaleIndependCheckLeft� TopxWidth� HeightCaption!7TabOrderOnClickScaleIndependCheckClick  TSpinBtn	ScaleSpinLeftrTop`Height	ArrowkeysCtrlIncrement       �@	Increment       �@Max �P]���CShiftIncrement       �@	ValidatorScaleVal  TEdit	ScaleEditLeft Top`WidthQHeightTabOrderOnChangeWidthEditChangeOnExitWidthEditExit  TSpinBtn	WidthSpinLeftrTopHHeight	ArrowkeysCtrlIncrement       �@	Increment ��������?Max       �@ShiftIncrement       ��?	ValidatorWidthVal  TEdit	WidthEditLeft TopHWidthQHeightTabOrderOnChangeWidthEditChangeOnExitWidthEditExit  TColorSelectBtnFillColorComboBtnLeft Top(WidthaOnSelectColorOnChangeColorTabOrder  TColorSelectBtnColorComboBtnLeft TopWidthaOnSelectColorOnChangeColorTabOrder  TDipListLineStylesListLeftTopWidth� Height� ColCountColWidth� Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style OnClickLineStylesListClick
OnDblClickLineStylesListDblClick
OnDrawCellLineStylesListDrawCellOnMouseMoveLineStylesListMouseMove
ParentFontParentShowHint	PopupMenu	PopupMenu	RowHeight
Scrollbars
sbVerticalShowHint	TabOrder TabStop	  	TCheckBoxChkChangeDirectionLeft� Top� Width� HeightCaption!13TabOrderVisible   TButtonOKBtnLeft� Top� WidthYHeightCaption!4Default	ModalResultTabOrder  TMlgSection
MlgSectionSectionLineStyleDialogTop�   TMeasureLookupValidatorWidthValEdit	WidthEditUseableUnits CommasDefaultValue.UnitsmuNoneDefaultValue.Value �P]����	MaxLengthMaxValue.Value       �@Left Top�   
TPopupMenu	PopupMenuLeft@Top�  	TMenuItemAddMenuCaption!8OnClickAddMenuClick  	TMenuItemPropertiesMenuCaption!10OnClickPropertiesMenuClick  	TMenuItemN2Caption-  	TMenuItem
DeleteMenuCaption!9OnClickDeleteMenuClick  	TMenuItemN1Caption-  	TMenuItemOrganizeMenuCaption!11OnClickOrganizeMenuClick   TMeasureLookupValidatorScaleValEdit	ScaleEditUseableUnits @MaxValue.Value �P]���CUnits	muPercentLeft`Top�    