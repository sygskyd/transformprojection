{******************************************************************************+
  Module EditScalesDlg
--------------------------------------------------------------------------------
  Author: Martin Forst
--------------------------------------------------------------------------------
  Dialog to edit the default-scales-list of the zoom-sclae-dialog.
+******************************************************************************}

Unit EditScalesDlg;

Interface

Uses Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     ExtCtrls,StdCtrls, MultiLng, Validate, InputBox,WCtrls;

Type TEditScalesDialog = class(TWForm)
       AddBtn: TButton;
       Bevel2: TBevel;
       CancelBtn: TButton;
       EditBtn: TButton;
       InputBox: TInputBox;
       MlgSection: TMlgSection;
       OkBtn: TButton;
       RemoveBtn: TButton;
       ScalesList: TListBox;
       ScaleValidator: TScaleValidator;
       Procedure   FormShow(Sender: TObject);
       Procedure   RemoveBtnClick(Sender: TObject);
       Procedure   ScalesListClick(Sender: TObject);
    procedure AddBtnClick(Sender: TObject);
    procedure EditBtnClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
      Private
       Function    GetItems:TStrings;
       Procedure   SetItems(AItems:TStrings);
       Procedure   UpdateEnabled;
      Public
       Property    Items:TStrings read GetItems write SetItems;
     end;

Implementation

{$R *.DFM}

Uses NumTools;

Procedure TEditScalesDialog.FormShow(Sender: TObject);
begin
  UpdateEnabled;
end;

Procedure TEditScalesDialog.UpdateEnabled;
begin
  EditBtn.Enabled:=ScalesList.ItemIndex>-1;
  RemoveBtn.Enabled:=ScalesList.ItemIndex>-1;
end;

Procedure TEditScalesDialog.ScalesListClick(Sender: TObject);
begin
  UpdateEnabled;
end;

Procedure TEditScalesDialog.RemoveBtnClick(Sender: TObject);
var Index          : Integer;
begin
  with ScalesList do begin
    Index:=ItemIndex;
    Items.Delete(Index);
    ItemIndex:=Min(Index,Items.Count-1);
  end;
  UpdateEnabled;
end;

Function TEditScalesDialog.GetItems:TStrings;
begin
  Result:=ScalesList.Items;
end;

Procedure TEditScalesDialog.SetItems(AItems:TStrings);
begin
  ScalesList.Items:=AItems;
end;

procedure TEditScalesDialog.AddBtnClick(Sender: TObject);
begin
  InputBox.Caption:=MlgSection[7];
  if InputBox.Execute then begin
    if ScalesList.Items.IndexOf(InputBox.Text)=-1 then begin
      if ScalesList.ItemIndex=-1 then ScalesList.Items.Add(InputBox.Text)
      else ScalesList.Items.Insert(ScalesList.ItemIndex,InputBox.Text);
    end;
    ScalesList.ItemIndex:=ScalesList.Items.IndexOf(InputBox.Text);
  end;
end;

procedure TEditScalesDialog.EditBtnClick(Sender: TObject);
begin
  InputBox.Caption:=MlgSection[8];
  InputBox.Text:=ScalesList.Items[ScalesList.ItemIndex];
  if InputBox.Execute then ScalesList.Items[ScalesList.ItemIndex]:=InputBox.Text;
end;

procedure TEditScalesDialog.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  CanClose:=CheckValidators(Self);
end;

end.
