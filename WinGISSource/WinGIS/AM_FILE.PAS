{****************************************************************************}
{ Unit                                                                       }
{----------------------------------------------------------------------------}
{                                                                            }
{----------------------------------------------------------------------------}
{ Modifikationen                                                             }
{  16.02.1994 Martin Forst                                                   }
{****************************************************************************}
Unit AM_File;

Interface

Uses Messages,WinTypes,WinProcs,WinDos,SysUtils,AM_Def,Classes,ResDlg;

Const fd_FileOpen  = 1;
      fd_FileSave  = 2;
      fd_Import    = 3;
      fd_LTFileSave= 4;
      fd_FileNew   = 5;

      id_FName     = 100;
      id_FPath     = 101;
      id_FList     = 102;
      id_DList     = 103;
      id_LTProject = 104;
      id_IDOffset  = 105;
      id_Options   = 8;

      fsFileSpec   = fsFileName+fsExtension;

Type PFileDialog   = ^TFileDialog;
     TFileDialog   = Class(TGrayDialog)
      Public
       FParent     : TComponent;
       Caption     : PChar;
       FilePath    : PChar;
       Name        : Array[0..20] of Char;
       PathName    : Array[0..fsPathName] of Char;
       Extension   : Array[0..fsExtension] of Char;
       FileSpec    : Array[0..fsFileSpec] of Char;
       DType       : Integer;
       LTProject   : PBool;
       IDOff       : PLongInt;
       Constructor Init(AParent:TComponent;AType:Integer;AName,AFilePath:PChar;
                        ALTProject:PBool;AIDOff:PLongInt);
       Function    CanClose:Boolean; override;
       Procedure   HandleLTButton(var Msg:TMessage); message id_First+id_LTProject;
       Procedure   HandleFName(var Msg:TMessage); message id_First+id_FName;
       Procedure   HandleFList(var Msg:TMessage); message id_First+id_FList;
       Procedure   HandleDList(var Msg:TMessage); message id_First+id_DList;
       Procedure   SetupWindow; override;
      Private
       Procedure   SelectFileName;
       Procedure   UpdateFileName;
       Function    UpdateListBoxes:Boolean;
     end;

Implementation

Function GetFileName
   (
   FilePath        : PChar
   )
   : PChar;
  var P            : PChar;
  begin
    P:=StrRScan(FilePath,'\');
    if P=nil then P:=StrRScan(FilePath,':');
    if P=nil then GetFileName:=FilePath
    else GetFileName:=P+1;
  end;

Function GetExtension
   (
   FilePath        : PChar
   )
   : PChar;
  var P            : PChar;
  begin
    P:=StrScan(GetFileName(FilePath),'.');
    if P=nil then GetExtension:=StrEnd(FilePath)
    else GetExtension:=P;
  end;

Function HasWildCards
   (
   FilePath        : PChar
   )
   : Boolean;
  begin
    HasWildCards:=(StrScan(FilePath,'*')<>nil) or
        (StrScan(FilePath,'?')<>NIL);
  end;

Constructor TFileDialog.Init
   (
   AParent         : TComponent;
   AType           : Integer;
   AName           : PChar;
   AFilePath       : PChar;
   ALTProject      : PBool;
   AIDOff          : PLongInt
   );
  begin
    FParent:=AParent;
    case AType of
      fd_FileNew    : inherited Init(AParent,'OPENNEW');
      fd_FileOpen   : inherited Init(AParent,'OPEN');
      fd_FileSave   : inherited Init(AParent,'SAVEAS');
      fd_Import     : inherited Init(AParent,'OPEN');
      fd_LTFileSave : inherited Init(AParent,'SAVEASLT');
    end;
    DType:=AType;
    StrCopy(Name,AName);
    Caption:=NIL;
    FilePath:=AFilePath;
    LTProject:=ALTProject;
    IDOff:=AIDOff;
  end;

Function TFileDialog.CanClose
   : Boolean;
  var PathLen      : Word;
  begin
    CanClose:=False;
    GetDlgItemText(Handle,id_FName,PathName,fsPathName+1);
    FileExpand(PathName,PathName);
    PathLen:=StrLen(PathName);
    if (PathName[PathLen-1]='\') or HasWildCards(PathName) or
        (GetFocus=GetDlgItem(Handle,id_DList)) then begin
      if PathName[PathLen-1]='\' then StrLCat(PathName,FileSpec,fsPathName);
      if not UpdateListBoxes then begin
        //MessageBeep(0);
        SelectFileName;
      end;
      Exit;
    end;
    StrLCat(StrLCat(PathName,'\',fsPathName),FileSpec,fsPathName);
    if UpdateListBoxes then Exit;
    PathName[PathLen]:=#0;
    if GetExtension(PathName)[0]=#0 then
      StrLCat(PathName,Extension,fsPathName);
    AnsiLower(StrCopy(FilePath,PathName));
    LTProject^:=FALSE;
{    GetDlgItemText(Handle,id_IDOffset,CharOffs,20);
    StrOffs:=StrPas(CharOffs);
    Val(StrOffs,Offs,Error);
    if Offs > 4000000 then begin
      MsgBox(FParent^.Handle,112,mb_IconExclamation or mb_OK,'');
      Exit;
    end
    else IDOff^:=Offs;}
    IDOff^:=0;
    if IsDlgButtonChecked(Handle,id_LTProject) <> 0 then LTProject^:=TRUE
    else LTProject^:=FALSE;
    CanClose:=TRUE;
  end;

Procedure TFileDialog.SetupWindow;
  begin
    SetWindowText(Handle,Name);
    inherited SetupWindow;
    if DType=fd_FileOpen then ShowWindow(GetItemHandle(8),sw_Hide);
    SendDlgItemMessage(Handle,id_FName,em_LimitText,fsPathName,0);
    if Caption <> nil then SetWindowText(Handle,Caption);
    StrLCopy(PathName,FilePath,fsPathName);
    StrLCopy(Extension,GetExtension(PathName),fsExtension);
    if HasWildCards(Extension) then Extension[0]:=#0;
    if not UpdateListBoxes then begin
      StrCopy(PathName,'*.*');
      UpdateListBoxes;
    end;
    SelectFileName;
{    Str(IDOff^:0,StrOffs);
    StrPCopy(CharOffs,StrOffs);
    SetDlgItemText(Handle,id_IDOffset,CharOffs);}
  end;

Procedure TFileDialog.HandleLTButton
  (
   var Msg         : TMessage
  );
  begin
    if IsDlgButtonChecked(Handle,id_LTProject) <> 0 then
      SetDlgItemText(Handle,id_FName,AnsiLower('project.amp'));
  end;

Procedure TFileDialog.HandleFName
   (
   var Msg         : TMessage
   );
  begin
    if Msg.LParamHi=en_Change then EnableWindow(GetDlgItem(Handle,id_Ok),
       SendMessage(Msg.LParamLo,wm_GetTextLength,0,0)<>0);
  end;

Procedure TFileDialog.HandleFList
   (
   var Msg         : TMessage
   );
  begin
    case Msg.LParamHi of
      lbn_SelChange,
      lbn_DblClk     : begin
        DlgDirSelectEx(Handle,PathName,SizeOf(PathName),id_FList);
        UpdateFileName;
        if Msg.LParamHi=lbn_DblClk then Ok(Msg);
      end;
      lbn_KillFocus  : SendMessage(Msg.LParamLo,lb_SetCurSel,Word(-1),0);
    end;
  end;

Procedure TFileDialog.HandleDList
   (
   var Msg         : TMessage
   );
  begin
    case Msg.LParamHi of
      lbn_SelChange,
      lbn_DblClk     : begin
        DlgDirSelectEx(Handle,PathName,SizeOf(PathName),id_DList);
        StrCat(PathName,FileSpec);
        if Msg.LParamHi=lbn_DblClk then UpdateListBoxes
        else UpdateFileName;
      end;
      lbn_KillFocus  : SendMessage(Msg.LParamLo,lb_SetCurSel,Word(-1),0);
    end;
  end;

Procedure TFileDialog.SelectFileName;
  begin
    SendDlgItemMessage(Handle,id_FName,em_SetSel,0,$7FFF0000);
    SetFocus(GetDlgItem(Handle,id_FName));
  end;

Procedure TFileDialog.UpdateFileName;
  begin
    SetDlgItemText(Handle,id_FName,AnsiLower(PathName));
    SendDlgItemMessage(Handle,id_FName,em_SetSel,0,$7FFF0000);
  end;

Function TFileDialog.UpdateListBoxes
   : Boolean;
  var BResult       : Integer;
      Path         : Array[0..fsPathName] of Char;
  begin
    UpdateListBoxes:=False;
    if GetDlgItem(Handle,id_FList)<>0 then begin
      StrCopy(Path,PathName);
      BResult:=DlgDirList(Handle,Path,id_FList,id_FPath,0);
      if BResult<>0 then DlgDirList(Handle,'*.*',id_DList,0,$C010);
    end
    else begin
      StrLCopy(Path,PathName,GetFileName(PathName)-PathName);
      StrLCat(Path,'*.*',fsPathName);
      BResult:=DlgDirList(Handle,Path,id_DList,id_FPath,$C010);
    end;
    if BResult<>0 then begin
      StrLCopy(FileSpec,GetFileName(PathName),fsFileSpec);
      StrCopy(PathName,FileSpec);
      UpdateFileName;
      UpdateListBoxes:=True;
    end;
  end;

end.
