{****************************************************************************}
{ Unit AM_Pass                                                               }
{----------------------------------------------------------------------------}
{                                                                            }
{----------------------------------------------------------------------------}
{ Modifikationen                                                             }
{  16.02.1994 Martin Forst                                                   }
{****************************************************************************}
Unit AM_Pass;

Interface

Uses Objects,WinTypes,WinProcs,AM_Def,AM_Ini,SysUtils,Classes,ResDlg;

Const id_Input     = 102;
      id_New1      = 103;
      id_New2      = 104;

Type TPasswordDlg  = Class(TGrayDialog)
      Public
       PassWord    : PShortString;
       Constructor Init(AParent:TComponent;APassWord:PShortString);
       Function    CanClose:Boolean; override;
     end;

     TPWDChangeDlg  = Class(TGrayDialog)
      Public
       PassWord    : PShortString;
       Constructor Init(AParent:TComponent;APassWord:PShortString);
       Function    CanClose:Boolean; override;
     end;

Procedure ChangePassword(AParent:TComponent);

Function InputCheckPassword(AParent:TComponent):Boolean;

Function CheckSSPPassword(AParent:TComponent;APassWord:String):Boolean;

Implementation

Function CheckSSPPassword
  (
  AParent         : TComponent;
  APassWord       : String
  )
: Boolean;
begin
  Result:=FALSE;
{$IFNDEF WMLT}
  ParamPwd:=UpperCase(ParamPwd);
  UpStr(APassWord);
  if ParamPwd = '' then begin
     if ExecDialog(TPasswordDlg.Init(AParent,@APassWord))=id_Ok then
        Result:=TRUE;
  end
  else if ParamPwd = APassWord then begin
    Result:=TRUE;
    CanChangePwd:=False;
  end;
{$ELSE}
  if EncryptString(APassWord) = LTPwd then CheckLTPassword:=True
  else begin
     if ExecDialog(TPasswordDlg.Init(AParent,@APassWord))=id_Ok then begin
        CheckLTPassword:=TRUE;
     end
     else begin
       CheckLTPassword:=False;
       Halt;
     end;
  end;
{$ENDIF}
end;

Procedure ChangePassword
   (
   AParent         : TComponent
   );
  var AWord        : String;
  begin
    if IniFile^.ReadPassword(AWord) then begin
      UpStr(AWord);
      if ExecDialog(TPWDChangeDlg.Init(AParent,@AWord))=id_Ok then
          IniFIle^.WritePassWord(AWord);
    end;
  end;

Function InputCheckPassword
   (
   AParent         : TComponent
   )
   : Boolean;
   //changed by Brovak
  var AWord        : String;
  begin
    InputCheckPassword:=FALSE;
    if IniFile^.ReadPassword(AWord) then begin
     If AWord='' then
       begin;
        InputCheckPassword:=TRUE;
        Exit;
       end;

      UpStr(AWord);
      if ExecDialog(TPasswordDlg.Init(AParent,@AWord))=id_Ok then
         InputCheckPassword:=TRUE;
    end;
  end;


Constructor TPasswordDlg.Init
   (
   AParent         : TComponent;
   APassWord       : PShortString
   );
  begin
    inherited Init(AParent,'PWD');
    PassWord:=APassWord;
  end;

Function TPasswordDlg.CanClose
   : Boolean;
  var AText        : String;
  begin
    AText:=GetFieldText(id_Input);
    if (UpStr(AText)=Password^) or (IsSuperPwd(AText)) then CanClose:=TRUE
    else begin
      MsgBox(Handle,10134,mb_IconExclamation or mb_OK,'');
      CanClose:=FALSE;
    end;
  end;

Constructor TPWDChangeDlg.Init
   (
   AParent         : TComponent;
   APassWord       : PShortString
   );
  begin
    inherited Init(AParent,'PWDC');
    PassWord:=APassWord;
  end;

Function TPWDChangeDlg.CanClose
   : Boolean;
  var AText        : String;
      BText        : String;
  begin
    CanClose:=FALSE;
    AText:=GetFieldText(id_Input);
    if (UpStr(AText)<>Password^) and (not IsSuperPwd(AText)) then begin
      MsgBox(Handle,10134,mb_IconExclamation or mb_OK,'');
      SetFocus(GetItemHandle(id_Input));
    end
    else begin
      AText:=UpStr(GetFieldText(id_New1));
      BText:=UpStr(GetFieldText(id_New2));
       if AText<>BText then begin
        MsgBox(Handle,10139,mb_IconExclamation or mb_OK,'');
        SetFocus(GetItemHandle(id_New2));
      end
      else begin
        PassWord^:=AText;
        CanClose:=TRUE;
      end;
    end;
  end;

end.
