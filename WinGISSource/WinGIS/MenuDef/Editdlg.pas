unit Editdlg;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, StdCtrls, ExtCtrls, MenuFn, Validate, WCtrls;

type
  TEditDialog = class(TForm)
    Label1: TWLabel;
    Label2: TWLabel;
    Label4: TWLabel;
    ButtonImage: TImage;
    OkBtn: TButton;
    CancelBtn: TButton;
    LoadBtn: TButton;
    OpenDialog: TOpenDialog;
    Label3: TWLabel;
    CheckMenuCheck: TCheckBox;
    ToggleCheck: TCheckBox;
    NameEdit: TEdit;
    HintEdit: TEdit;
    GroupNameEdit: TEdit;
    EnableGroupEdit: TEdit;
    EditVal: TValidator;
    HintVal: TIntValidator;
    GroupNameVal: TIntValidator;
    EnableGroupVal: TIntValidator;
    ContextToolbarEdit: TEdit;
    Label5: TWLabel;
    WLabel1: TWLabel;
    HelpCtxEdit: TEdit;
    HelpCtxVal: TIntValidator;
    VisibleGroupEdit: TEdit;
    VisibleGroupVal: TIntValidator;
    WLabel2: TWLabel;
    ToolbarControlCombo: TComboBox;
    WLabel3: TWLabel;
    VerticalDockedEdit: TEdit;
    WLabel4: TWLabel;
    WLabel5: TWLabel;
    WidthEdit: TEdit;
    WidthVal: TIntValidator;
    ClearBtn: TButton;
    ReinvokeCheck: TCheckBox;
    procedure LoadBtnClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormShow(Sender: TObject);
    procedure ClearBtnClick(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    Function  GetData:TMenuFunction;
    Procedure SetData(AData:TMenuFunction);
  end;

var
  EditDialog: TEditDialog;

implementation

{$R *.DFM}
{$R menufn.mfn}

Procedure TEditDialog.SetData
   (
   AData           : TMenuFunction
   );
  begin
    if AData=NIL then begin
      NameEdit.Clear;
      HintVal.AsInteger:=0;
      GroupNameVal.AsInteger:=0;
      EnableGroupVal.AsInteger:=0;
      ButtonImage.Picture.Bitmap:=TBitmap.Create;
      CheckMenuCheck.Checked:=FALSE;
      ToggleCheck.Checked:=FALSE;
      ContextToolbarEdit.Text:='';
      HelpCtxVal.AsInteger:=0;
      VisibleGroupVal.AsInteger:=0;
      ToolbarControlCombo.ItemIndex:=0;
      VerticalDockedEdit.Text:='';
      WidthVal.AsInteger:=0;
      ReinvokeCheck.Checked:=FALSE;
    end
    else with AData do begin
      NameEdit.Text:=Name;
      HintVal.AsInteger:=HintNumber;
      GroupNameVal.AsInteger:=CheckGroup;
      EnableGroupVal.AsInteger:=EnableGroup;
      ButtonImage.Picture.Bitmap:=Bitmap;
      CheckMenuCheck.Checked:=CheckMenus;
      ToggleCheck.Checked:=ToggleCheckState;
      ContextToolbarEdit.Text:=ContextToolbar;
      HelpCtxVal.AsInteger:=HelpContext;
      VisibleGroupVal.AsInteger:=VisibleGroup;
      ToolbarControlCombo.ItemIndex:=Integer(ToolbarControlType);
      VerticalDockedEdit.Text:=VerticalDockedFunction;
      WidthVal.AsInteger:=ToolbarCOntrolWidth;
      ReinvokeCheck.Checked:=Reinvokeable;
    end;
  end;

procedure TEditDialog.LoadBtnClick(Sender: TObject);
var Bitmap         : TBitmap;
begin
  if OpenDialog.Execute then begin
    Bitmap:=TBitmap.Create;
    Bitmap.LoadFromFile(OpenDialog.FileName);
    ButtonImage.Picture.Bitmap:=Bitmap;
  end;
end;

Function TEditDialog.GetData                         
   : TMenuFunction;
  begin
    Result:=TMenuFunction.Create(NIL);
    with Result do begin
      Name:=NameEdit.Text;
      HintNumber:=HintVal.AsInteger;
      CheckGroup:=GroupNameVal.AsInteger;
      EnableGroup:=EnableGroupVal.AsInteger;
      Bitmap.Assign(ButtonImage.Picture.Bitmap);
      CheckMenus:=CheckMenuCheck.Checked;
      ToggleCheckState:=ToggleCheck.Checked;
      ContextToolbar:=ContextToolbarEdit.Text;
      HelpContext:=HelpCtxVal.AsInteger;
      VisibleGroup:=VisibleGroupVal.AsInteger;
      ToolbarControlType:=TToolbarControlType(ToolbarControlCombo.ItemIndex);
      VerticalDockedFunction:=VerticalDockedEdit.Text;
      ToolbarCOntrolWidth:=WidthVal.AsInteger;
      Reinvokeable:=ReinvokeCheck.Checked;
    end;
  end;

procedure TEditDialog.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  CanClose:=CheckValidators(Self);
end;

procedure TEditDialog.FormShow(Sender: TObject);
begin
  ActiveControl:=NameEdit;
end;

procedure TEditDialog.ClearBtnClick(Sender: TObject);
begin
  ButtonImage.Picture.Assign(NIL);
end;

end.
