program Menudef;

uses
  Forms,
  Editdlg in 'EDITDLG.PAS' {EditDialog},
  Main in 'MAIN.PAS' {MainForm};

{$R *.RES}

begin
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TEditDialog, EditDialog);
  Application.Run;
end.
