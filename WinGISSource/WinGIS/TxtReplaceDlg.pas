//****************************************************************************
// Unit TxtReplaceDlg
//----------------------------------------------------------------------------
// Provides a dialog to confirm the found text replacement.
//----------------------------------------------------------------------------
// Modifications
//  17.04.2001, Y.Glukhov, creation
//****************************************************************************
Unit TxtReplaceDlg;

Interface

Uses
  Windows, Messages, SysUtils, Objects, ExtCtrls, StdCtrls,
  MultiLng, Spinbtn, WCtrls, Controls, Classes, ComCtrls,
  AM_Def, AM_Proj;

Type
  TTxtReplaceDialog   = Class(TWForm)
    Bevel1           : TBevel;
    CancelBtn        : TButton;
    MlgSection       : TMlgSection;
    OKBtn            : TButton;
    ControlPanel1 : TPanel;
    ControlPanel  : TPanel;
    WLabel3       : TWLabel;
    edtSearchFor  : TEdit;
    edtReplaceBy  : TEdit;
    WLabel1: TWLabel;
    WLabel2: TWLabel;
    Button1: TButton;
    redText: TRichEdit;
    ChkZoomFound: TCheckBox;
    ChkRestView: TCheckBox;
    tim100: TTimer;

    procedure FormShow(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure edtOnInvalidAction(Sender: TObject);
    procedure ChkRestViewOnClick(Sender: TObject);
    procedure ChkZoomFoundOnClick(Sender: TObject);
    procedure OnTimer100(Sender: TObject);

  Private
    bInitialized    : Boolean;
    HorOffset       : LongInt;
    VerOffset       : LongInt;
    NumOfAttached   : LongInt;
    bRect           : Boolean;

    Procedure   InitFormData;

  Public
    Project       : PProj;
    // To keep the parameters
    sSearchFor    : String;
    sText         : String;
    // To highlight the replaced text
    iSelStart     : Integer;
    iSelLength    : Integer;
    // Text ClipRect
    TxtClipRect   : TDRect;

  end;

Implementation

{$R *.DFM}

Uses
  Graphics, AM_Input, AM_Child,
  TxtReplacePrm;

Procedure TTxtReplaceDialog.InitFormData;
  begin
    edtSearchFor.Text:= sSearchFor;
    redText.Text:= sText;
//    redText.SetFocus;
    redText.SelStart:= 0;
    redText.SelLength:= 1024;
    redText.SelAttributes.Color:= clWindowText;
    redText.SelAttributes.Style:= [];
    redText.SelStart:= iSelStart;
    redText.SelLength:= iSelLength;
    redText.SelAttributes.Color:= clRed;
//    redText.SelAttributes.Style:= [fsBold, fsUnderline];
    redText.SelAttributes.Style:= [fsUnderline];
    chkZoomFound.Checked:= RTLastChkZoomFnd;
    chkRestView.Checked:= False;
  end;

Procedure TTxtReplaceDialog.FormShow(Sender: TObject);
  begin
    if bInitialized then Exit;
    InitFormData;
    edtReplaceBy.SetFocus;
    bRect:= False;
    tim100.Enabled:= True;
    bInitialized:= True;
  end;

Procedure TTxtReplaceDialog.FormHide(Sender: TObject);
  begin
    tim100.Enabled:= False;
    // Clear the text box
    if bRect then Project.InputBox^.EndIt( Project.PInfo );

    RTLastChkZoomFnd:= chkZoomFound.Checked;
    RTLastChkOriViewC:= chkRestView.Checked;
    if ModalResult=mrOK then begin
      // Change parameter 'Replace by' for the Replace Text operation
      RTLastReplaceBy:= edtReplaceBy.Text;
    end;
    bInitialized:= False;
  end;

Procedure TTxtReplaceDialog.FormCreate(Sender: TObject);
  begin
    tim100.Enabled:= False;
    tim100.Interval:= 100;
    TxtClipRect.Init;
    bInitialized:= False;
    bRect:= False;
  end;

Procedure TTxtReplaceDialog.FormDestroy(Sender: TObject);
  begin
    bInitialized:= False;
  end;

procedure TTxtReplaceDialog.edtOnInvalidAction(Sender: TObject);
  begin
    if not bInitialized then Exit;
    InitFormData;
    edtReplaceBy.SetFocus;
  end;

procedure TTxtReplaceDialog.ChkZoomFoundOnClick(Sender: TObject);
begin
  if chkZoomFound.Checked
  then  if chkRestView.Checked  then chkRestView.Checked:= False;
end;

procedure TTxtReplaceDialog.ChkRestViewOnClick(Sender: TObject);
begin
  if chkRestView.Checked
  then  if chkZoomFound.Checked  then chkZoomFound.Checked:= False;
end;

procedure TTxtReplaceDialog.OnTimer100(Sender: TObject);
begin
  if bRect then Exit;
  if TMDIChild(Project.Parent).InWMPaint then Exit;

  tim100.Enabled:= False;
  bRect:= True;
  // Draw a box for the object
  Project.InputBox^.SetRect( TxtClipRect );
  Project.InputBox^.Mode := m_FixSize;
//    Project.InputBox^.Mode := m_CornersOnly;
//    Project.InputBox^.Mode := m_Default;
  Project.InputBox^.StartIt( Project.PInfo, TxtClipRect.A, True);
end;

//=============================================================================
//  Initialization
//=============================================================================

end.
