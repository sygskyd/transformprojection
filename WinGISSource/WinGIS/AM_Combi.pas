{***********************************************************************************************}
{ Unit AM_Combine                                                                               }
{-----------------------------------------------------------------------------------------------}
{ - Routinen und Dialog zu Zusammenfassen zu Linien oder Fl�chen.                               }
{ - Dialog f�r Trimmen                                                                          }
{-----------------------------------------------------------------------------------------------}
{ Modifikationen                                                                                }
{  16.02.1994 Martin Forst                                                                      }
{  07.04.1994 Martin Forst   Trimmdialog                                                        }
{***********************************************************************************************}
Unit AM_Combi;

Interface

Uses Messages,Objects,SysUtils,WinTypes,WinProcs,AM_Admin,AM_Def,AM_Dlg1,AM_Layer,
     AM_StdDl,AM_Poly,AM_CPoly,Classes,ResDlg,ProjStyle;

Const { Konstanten f�r TrimmOptions                                                             }
      to_FirstLine      = $0001;
      to_BothLines      = $0002;
      to_ToArea         = $0003;

Type PCombineData  = ^TCombineData;
     TCombineData  = Record
                       MakeAreas   : Boolean;
                       DestLayer   : LongInt;
                       Distance    : LongInt;
                     end;

     PDCombine     = ^TDCombine;
     TDCombine     = Class(TLayerDlg)
      Public
                       Data        : PCombineData;
                       Constructor Init(AParent:TComponent;AData:PCombineData;ALayers:PLayers;
                                        AProjStyle:TProjectStyles);
                       Function    CanClose:Boolean; override;
                       Procedure   SetupWindow; override;
                     end;

     PTrimmDlg          = ^TTrimmDlg;
     TTrimmDlg          = Class(TGrayDialog)
      Public
       Data             : PInteger;
       Constructor Init(AParent:TComponent;AData:PInteger);
       Function    CanClose:Boolean; override;
       Procedure   SetupWindow; override;
     end;

Function ProcTrimm(AProj:Pointer;CutObj2:PPoly;DrawPos:TDPoint):Boolean;

procedure ProcDBTrimm(AProj:Pointer;IdLine,IdArea:Integer);

procedure ProcCombineLines(AData: Pointer);

Implementation

Uses AM_Paint,AM_Proj,UserIntf, AM_Index, AM_View, LayerHndl, Controls;

Const id_Lines          = 100;
      id_Areas          = 101;
      id_Destination    = 102;

      id_TrimmFirst      = 100;
      id_TrimmBoth       = 101;
      id_TrimmToArea     = 102;

Constructor TDCombine.Init
   (
   AParent         : TComponent;
   AData           : PCombineData;
   ALayers         : PLayers;
   AProjStyle      : TProjectStyles
   );
  begin
    inherited Init(AParent,'COMBINE',ALayers,AProjStyle);
    Data:=AData;
    HelpContext:=5020;
  end;

Function TDCombine.CanClose
   : Boolean;
  begin
    CanClose:=FALSE;
    with Data^ do if GetLayer(id_Destination,DestLayer,959,10125,10128,ilSecond) then begin
      MakeAreas:=IsDlgButtonChecked(Handle,id_Areas)=1;
      CanClose:=TRUE
    end;
  end;

Procedure TDCombine.SetupWindow;
  Function LayerIndex
     (
     Layer         : LongInt;
     Fixed         : Boolean
     )
     : Integer;
    var ALayer     : PLayer;
        BResult     : Integer;
    begin
      if Layer=0 then BResult:=0
      else begin
        ALayer:=Layers^.IndexToLayer(Layer);
        if ALayer=NIL then BResult:=0
        else BResult:=Layers^.LData^.IndexOf(ALayer)-1;
      end;
      LayerIndex:=BResult;
    end;
  begin
    inherited SetupWindow;
    FillLayerList(id_Destination,FALSE,TRUE);
    SendDlgItemMsg(id_Destination,cb_SetCurSel,LayerIndex(Data^.DestLayer,FALSE),0);
    CheckRadioButton(Handle,id_Lines,id_Areas,Byte(Data^.MakeAreas)+id_Lines);
  end;

{===============================================================================================}
{ TTrimmDlg                                                                                     }
{===============================================================================================}

Constructor TTrimmDlg.Init
   (
   AParent         : TComponent;
   AData           : PInteger
   );
  begin
    inherited Init(AParent,'TRIMM');
    Data:=AData;
    HelpContext:=2090;
  end;
                   
Procedure TTrimmDlg.SetupWindow;
  begin
    inherited SetupWindow;
    CheckRadioButton(Handle,id_TrimmFirst,id_TrimmToArea,id_TrimmFirst+Data^-1);
  end;

Function TTrimmDlg.CanClose
   : Boolean;
  begin
    if IsDlgButtonChecked(Handle,id_TrimmFirst)=1 then Data^:=to_FirstLine
    else if IsDlgButtonChecked(Handle,id_TrimmBoth)=1 then Data^:=to_BothLines
    else Data^:=to_ToArea;
    CanClose:=TRUE;
  end;

{**********************************************************************************}
{ Procedure GetPoints                                                              }
{----------------------------------------------------------------------------------}
{ Holt die Punkte eine Linie aus dem Polygon. P1 liegt dabei immer im Inneren des  }
{ Polygons (2. oder vorletzter), P2 ist Anfangs- oder Endpunkt.                    }
{**********************************************************************************}
Procedure GetPoints
   (
   APoly         : PPoly;
   APointInd     : Integer;
   var P1        : PDPoint;
   var P2        : PDPoint
   );
  begin
    with APoly^,Data^ do begin
      if APointInd=0 then begin
        P1:=At(1);
        P2:=At(0);
      end
      else begin
        P1:=At(APointInd);
        P2:=At(APointInd+1);
      end;
    end;
  end;

{**********************************************************************************}
{ Procedure SwapPoints                                                             }
{----------------------------------------------------------------------------------}
{ Ermittelt, auf welcher Seite des Schnittpunktes der Geraden mit der Maus         }
{ geclickt wurde. P1 und P2 werden vertauscht, wenn die Maus auf der Seite von     }
{ P2 liegt. Grund: P2 wird verschoben.                                             }
{**********************************************************************************}
Procedure SwapPoints
   (
   PInfo         : PPaint;
   var P1        : PDPoint;
   var P2        : PDPoint;
   var Cut       : TDPoint;
   Mouse         : TDPoint
   );
  var ADistance  : Double;
      Cut2       : TDPoint;
      Swap       : PDPoint;          { Zwischenspeicher f�r Punktvertauschen       }
  begin
    if not PInfo^.NormalDistance(P1,@Cut,@Mouse,ADistance,Cut2) then begin
      Swap:=P1;
      P1:=P2;
      P2:=Swap;
    end;
  end;

{**********************************************************************************}
{ Function CheckAllow                                                              }
{----------------------------------------------------------------------------------}
{ �berpr�ft bei Polygonen, ob der Schnittpunkt in Richtung des Anfanges oder des   }
{ Endes ist. Nur dann ist der Schnitt erlaubt, CheckAllow liefert TRUE.            }
{ Funktionsweise: X- und Y-Abst�nde von P1 zu Cut und von P1 zu P2 m�ssen gleiche  }
{ Vorzeichen haben.                                                                }
{**********************************************************************************}
Function CheckAllow
   (
   P1            : PDPoint;
   P2            : PDPoint;
   var Cut       : TDPoint
   )
   : Boolean;
  begin
    CheckAllow:=not (BoolSign(P1^.XDist(Cut)) XOr BoolSign(P1^.XDist(P2^)))
        and not (BoolSign(P1^.YDist(Cut)) XOr BoolSign(P1^.YDist(P2^)));
  end;

{************************************************************************************}
{ Procedure LineCut                                                                  }
{------------------------------------------------------------------------------------}
{ Ermittelt den Schnittpunkt zweier Linien (bzw. Polygone) und verschneidet die      }
{ Linien. Bei Polygonen kann nur die Anfangs- oder Endlinie verschnitten werden      }
{ (wird bereits beim Anw�hlen abgefragt. Au�erden kann bei Polygonen nur in Richtung }
{ des Anfangs- oder Endpunktes verschnitten werden. Welcher Teil der Linien          }
{ bestehen bleibt h�ngt davon ab, auf welche Seite des Schnittpunktes angeclickt     }
{ wurde (gleiche Seite).                                                             }
{------------------------------------------------------------------------------------}
{ Poly1            i = Poly das zuerst selektiert wurde                              }
{ Point1           i = Nummer des gew�hlten Punktes dem Pos1 an n�chsten liegt       }
{ Pos1             i = Click-Position                                                }
{ Poly2            i = 2. Poly                                                       }
{ Point2           i = Nummer des gew�hlten Punktes dem Pos2 am n�chsten liegt       }
{ Pos2             i = Click-Position                                                }
{ Options          i = to_XXXX                                                       }
{************************************************************************************}
Procedure LineCut
   (
   Proj            : PProj;
   Poly1           : PPoly;
   Point1          : Integer;
   Pos1            : TDPoint;
   Poly2           : PPoly;
   Point2          : Integer;
   Pos2            : TDPoint
   );
  var P1           : PDPoint;          { Startpunkt 1. Linie                         }
      P2           : PDPoint;          { Endpunkt 1. Linie                           }
      P3           : PDPoint;          { Startpunkt 2. Linie                         }
      P4           : PDPoint;          { EndPunkt 2. Linie                           }
      Cut          : TDPoint;          { Schnittpunkt der Linien                     }
      IsAllowed    : Boolean;          { Schnitt bei Polygonen erlaubt?              }
      ARect        : TDRect;           { Begrenzung beider Polys f�r Display l�schen }
  begin
    with Proj^ do begin
      GetPoints(Poly1,Point1,P1,P2);
      GetPoints(Poly2,Point2,P3,P4);
      if not PInfo^.LineCut2(P1^,P2^,P3^,P4^,Cut) then
          MsgBox(Parent.Handle,10103,mb_IconExclamation or mb_OK,'')
      else if (Poly1=Poly2) and (Point1=Point2) then
          MsgBox(Parent.Handle,10104,mb_IconExclamation or mb_OK,'')
      else begin
        IsAllowed:=TRUE;
        if Poly1^.Data^.Count>2 then               { Bei Poly pr�fen ob erlaubt, oder  }
            IsAllowed:=CheckAllow(P1,P2,Cut)       { bei Linie Punkte gegebenenfalls   }
        else SwapPoints(PInfo,P1,P2,Cut,Pos1);    { vertauschen                       }
        if TrimmOptions=to_BothLines then begin
          if Poly2^.Data^.Count>2 then
              IsAllowed:=IsAllowed and CheckAllow(P3,P4,Cut)
          else SwapPoints(PInfo,P3,P4,Cut,Pos2);
        end;
        if IsAllowed then begin                                        {08.04F}
          Poly1^.Invalidate(PInfo);                { Bildschirm "l�schen"              }
          P2^:=Cut;                                { Punkt verschieben                 }
          Poly1^.CalculateClipRect;                { Begrenzungsrechteck berechnen     }
          Poly1^.Invalidate(PInfo);                { Bildschirm "l�schen"              }
          UpdateClipRect(Poly1);
          ARect.InitByRect(Poly1^.ClipRect);
          if TrimmOptions=to_BothLines then begin  { auch 2. Linie �ndern              }
            Poly2^.Invalidate(PInfo);
            P4^:=Cut;
            Poly2^.CalculateClipRect;
            Poly2^.Invalidate(PInfo);
            UpdateClipRect(Poly2);
            ARect.CorrectByRect(Poly2^.ClipRect);  { Begrenzung beider Polys berechnen }
          end;                                                         {08.04F}
          if not CorrectSize(ARect,TRUE) then      { Projektgrenze updaten, oder neu   }
              PInfo^.RedrawInvalidate;             { zeichnen, wenn keine �nderung     }
          SetModified;                             { Daten wurden ge�ndert!            }
        end;
      end;
    end;
  end;

procedure ProcDBTrimm(AProj:Pointer;IdLine,IdArea:Integer);
var
ALine: PPoly;
AArea: PCPoly;
LIndex,AIndex: PIndex;
LView,AView: PView;
APoint: TDPoint;
begin
     if (AProj <> nil) and (IdLine <> 0) and (IdArea <> 0) then begin
        LIndex:=New(PIndex,Init(IdLine));
        AIndex:=New(PIndex,Init(IdArea));
        try
           LView:=PLayer(PProj(AProj)^.PInfo^.Objects)^.IndexObject(PProj(AProj)^.PInfo,LIndex);
           AView:=PLayer(PProj(AProj)^.PInfo^.Objects)^.IndexObject(PProj(AProj)^.PInfo,AIndex);
           if (LView <> nil) and (AView <> nil) then begin
              if (LView.GetObjType = ot_Poly) and (AView.GetObjType = ot_CPoly) then begin
                 ALine:=PPoly(LView);
                 AArea:=PCPoly(AView);

                 APoint.Init(0,0);
                 APoint.X:=PDPoint(ALine^.Data^.At(0))^.X;
                 APoint.Y:=PDPoint(ALine^.Data^.At(0))^.Y;

                 PProj(AProj)^.CutObj1:=ALine;
                 PProj(AProj)^.TrimmOptions:=to_ToArea;

                 ProcTrimm(AProj,AArea,APoint);
                 
              end;
           end;
        finally
           Dispose(LIndex,Done);
           Dispose(AIndex,Done);
        end;
     end;
end;

Function ProcTrimm
   (
   AProj           : Pointer;
   CutObj2         : PPoly;
   DrawPos         : TDPoint
   )
   : Boolean;
  var PointInd2    : Integer;
      Cut          : TDPoint;
      Point1       : PDPoint;
      Point2       : PDPoint;
      ARect        : TDRect;
      IsAllowed    : Boolean;
  begin
    Result:=FALSE;
    with PProj(AProj)^ do begin
      if TrimmOptions=to_ToArea then begin
        GetPoints(CutObj1,PointInd,Point1,Point2);
        if CutObj2^.GetCutPoint(PInfo,Point1^,Point2^,Cut) then begin
          IsAllowed:=TRUE;
          if CutObj1^.Data^.Count>2 then                 { Bei Poly pr�fen ob erlaubt, oder  }
              IsAllowed:=CheckAllow(Point1,Point2,Cut)   { bei Linie Punkte gegebenenfalls   }
          else SwapPoints(PInfo,Point1,Point2,Cut,CutPos1); { vertauschen                    }
          if IsAllowed then begin
            CutObj1^.Invalidate(PInfo);                  { Bildschirm "l�schen"              }
            Point2^:=Cut;
            CutObj1^.CalculateClipRect;                  { Begrenzungsrechteck berechnen     }
            CutObj1^.Invalidate(PInfo);                  { Bildschirm "l�schen"              }
            ARect.InitByRect(CutObj1^.ClipRect);
            if not CorrectSize(ARect,TRUE) then          { Projektgrenze updaten, oder neu   }
                PInfo^.RedrawInvalidate;                 { zeichnen, wenn keine �nderung     }
            UpdateClipRect(CutObj1);
            DeselectAll(FALSE);
            SetStatusText(GetLangText(10101),FALSE);
            SetModified;
            Result:=TRUE;
          end;
        end;
      end
      else begin
        CutObj2^.SearchForLine(PInfo,DrawPos,[ot_Poly,ot_CPoly],
            PInfo^.CalculateDraw(poMaxDist),PointInd2);
        if (TrimmOptions=to_FirstLine)
            or (PointInd2=0)
            or (PointInd2=CutObj2^.Data^.Count-2) then begin
          if (TrimmOptions=to_BothLines)
              and Layers^.IsObjectFixed(CutObj2) then                 {3003F}
            MsgBox(Parent.Handle,10147,mb_IconExclamation or mb_OK,'')
          else begin
            LineCut(AProj,CutObj1,PointInd,CutPos1,CutObj2,PointInd2,DrawPos);
            DeselectAll(FALSE);
            SetStatusText(GetLangText(10101),FALSE);
            Result:=TRUE;
          end;                                                   {3003F}
        end;
      end;
    end;
  end;

{******************************************************************************}

procedure ProcCombineLines(AData: Pointer);

{******************************************************************************}

type TEdgeList  = TList;
     TNodeList  = TList;
     TPointList = TList;

type PNode = ^TNode;
     TNode = record
        Point: PDPoint;
        Edges: TEdgeList;
        Handled: Boolean;
end;

type PEdge = ^TEdge;
     TEdge = record
        FirstNode: PNode;
        SecNode: PNode;
        Neighbors: TEdgeList;
end;

{******************************************************************************}

var
SLayer, DLayer: PLayer;
NodeList: TNodeList;
EdgeList: TEdgeList;

{******************************************************************************}

     function PointsEqual(APoint, BPoint: PDPoint): Boolean;
     begin
          Result:=((APoint^.X = BPoint^.X) and (APoint^.Y = BPoint^.Y));
     end;

{******************************************************************************}

     function InsertNode(APoint: PDPoint; AEdge: PEdge): PNode;
     var
     i: Integer;
     NewNode: PNode;
     begin
          NewNode:=nil;
          for i:=0 to NodeList.Count-1 do begin
              if PointsEqual(PNode(NodeList[i])^.Point,APoint) then begin
                 NewNode:=NodeList[i];
                 break;
              end;
          end;
          if NewNode = nil then begin
             NewNode:=New(PNode);
             NewNode^.Edges:=TEdgeList.Create;
             NewNode^.Point:=APoint;
             NewNode^.Handled:=False;
             NodeList.Add(NewNode);
          end;
          for i:=0 to NewNode^.Edges.Count-1 do begin
             PEdge(NewNode^.Edges[i])^.Neighbors.Add(AEdge);
             AEdge^.Neighbors.Add(NewNode^.Edges[i]);
          end;
          NewNode^.Edges.Add(AEdge);
          Result:=NewNode;
     end;

{******************************************************************************}

     procedure FillLists(AIndex: PIndex);
     var
     APoly: PPoly;
     i: Integer;
     NewEdge: PEdge;
     begin
          if AIndex^.GetObjType = ot_Poly then begin
             APoly:=PPoly(PLayer(PProj(AData)^.PInfo^.Objects)^.IndexObject(PProj(AData)^.Pinfo,AIndex));
             if APoly <> nil then begin
                for i:=0 to APoly^.Data^.Count-2 do begin
                    NewEdge:=New(PEdge);
                    NewEdge^.Neighbors:=TEdgeList.Create;
                    NewEdge^.FirstNode:=InsertNode(APoly^.Data^.At(i),NewEdge);
                    NewEdge^.SecNode:=InsertNode(APoly^.Data^.At(i+1),NewEdge);
                    EdgeList.Add(NewEdge);
                end;
             end;
          end;
     end;

{******************************************************************************}

     function GetStartNode: PNode;
     var
     i,MinNeighborsCount: Integer;
     AEdge: PEdge;
     begin
          Result:=nil;
          if EdgeList.Count > 0 then begin
             MinNeighborsCount:=PEdge(EdgeList[0])^.Neighbors.Count;
             AEdge:=EdgeList[0];
             for i:=1 to EdgeList.Count-1 do begin
                 if PEdge(EdgeList[i])^.Neighbors.Count < MinNeighborsCount then begin
                    MinNeighborsCount:=PEdge(EdgeList[i])^.Neighbors.Count;
                    AEdge:=EdgeList[i];
                 end;
             end;
             if AEdge^.FirstNode^.Edges.Count < AEdge^.SecNode^.Edges.Count then begin
                Result:=AEdge^.FirstNode;
             end
             else begin
                Result:=AEdge^.SecNode;
             end;
          end;
     end;

{******************************************************************************}

     procedure InsertNewPoly(APointList: TPointList);
     var
     i: Integer;
     ANewPoly: PPoly;
     APoint: TDPoint;
     begin
          if APointList.Count > 1 then begin
             ANewPoly:=New(PPoly,Init);
             for i:=0 to APointList.Count-1 do begin
                APoint.Init(PDPoint(APointList[i])^.X,PDPoint(APointList[i])^.Y);
                ANewPoly^.InsertPoint(APoint);
             end;
             if not PProj(AData)^.InsertObjectOnLayer(ANewPoly,DLayer^.Index) then begin
                Dispose(ANewPoly,Done);
             end;
          end;
     end;

{******************************************************************************}

     procedure DoCombine(ANode: PNode);
     var
     APointList: TPointList;
     AEdge: PEdge;
     BNode: PNode;
     i: Integer;
     NextFound: Boolean;
     begin
          if ANode <> nil then begin
             APointList:=TPointList.Create;
             try
                while ANode <> nil do begin
                      APointList.Add(ANode^.Point);
                      ANode^.Handled:=True;
                      if ANode^.Edges.Count > 2 then begin
                         ANode:=nil;
                      end
                      else begin
                         NextFound:=False;
                         for i:=0 to ANode^.Edges.Count-1 do begin
                             BNode:=PEdge(ANode^.Edges[i])^.FirstNode;
                             if (BNode <> ANode) then begin
                                if not BNode^.Handled then begin
                                   ANode:=BNode;
                                   NextFound:=True;
                                   break;
                                end;
                             end
                             else begin
                                BNode:=PEdge(ANode^.Edges[i])^.SecNode;
                                if (BNode <> ANode) then begin
                                   if not BNode^.Handled then begin
                                      ANode:=BNode;
                                      NextFound:=True;
                                      break;
                                   end;
                                end;
                             end;
                         end;
                         if not NextFound then begin
                            ANode:=nil;
                         end;
                      end;
                end;
                InsertNewPoly(APointList);
             finally
                APointList.Free;
             end;
          end;
     end;

{******************************************************************************}

begin
     if PProj(AData)^.Layers^.SelLayer^.SelInfo.Count > 0 then begin
        SLayer:=PProj(AData)^.Layers^.SelLayer;
     end
     else begin
        SLayer:=PProj(AData)^.Layers^.TopLayer;
     end;
     DLayer:=SelectLayerDialog(AData,[sloNewLayer]);
     if DLayer <> nil then begin
        PProj(AData)^.SetCursor(crHourGlass);
        NodeList:=TNodeList.Create;
        EdgeList:=TEdgeList.Create;
        try
           SLayer^.Data^.ForEach(@FillLists);
           DoCombine(GetStartNode);
        finally
           NodeList.Free;
           EdgeList.Free;
           PProj(AData)^.SetCursor(crDefault);
        end;
     end;
end;

{******************************************************************************}

end.
