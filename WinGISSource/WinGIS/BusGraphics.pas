{****************************************************************************}
{ Unit BusGraphics                                                           }
{----------------------------------------------------------------------------}
{ ++ Moskaliov Business Graphics BUILD#150 17.01.01                          }
{ Modifications:                                                             }
{                                                                            }
{                                                                            }
{****************************************************************************}

unit BusGraphics;

interface

uses
  Windows, Classes, Graphics, Forms, ExtCtrls, TeeProcs, TeEngine, Chart,
  StdCtrls, MultiLng, WCtrls, AM_Def, Objects, AM_View, AM_Paint, AM_CPoly,
  NumTools, AM_Proj, AM_Index, AM_Group, AM_BuGra, TeeEdit, Validate, AM_Obj, Spinbtn,
  Controls, Dialogs, Series;

type
  TBusGraphDlg = class(TForm)
    PositionGrBox: TWGroupBox;
    XPosLab: TWLabel;
    YPosLab: TWLabel;
    XPosEdit: TEdit;
    XPosSpin: TSpinBtn;
    YPosEdit: TEdit;
    YPosSpin: TSpinBtn;
    SizeGrBox: TWGroupBox;
    WidthLab: TWLabel;
    WidthEdit: TEdit;
    WidthSpin: TSpinBtn;
    ChartPanel: TPanel;
    ViewChart: TChart;
    OkBtn: TButton;
    CancelBtn: TButton;
    ChangeBtn: TButton;
    SetTypeBtn: TButton;
    YPosVal: TFloatValidator;
    XPosVal: TFloatValidator;
    ChartWidthVal: TFloatValidator;
    MlgSection: TMlgSection;
    ColorDialog1: TColorDialog;
    ViewChartEditor: TChartEditor;
    ViewSeries: TPieSeries;
    cbProp: TCheckBox;
    EditFactor: TEdit;
    FactorVal: TFloatValidator;
    LabelFactor: TLabel;
    FactorSpin: TSpinBtn;
    ComboBoxMode: TComboBox;
    CBTransp: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure PositionEditClick(Sender: TObject);
    procedure PositionSpinClick(Sender: TObject);
    procedure SizeEditClick(Sender: TObject);
    procedure SizeSpinClick(Sender: TObject);
    procedure SizeUpdate(Sender: TObject);
    procedure ChangeBtnClick(Sender: TObject);
    procedure ViewChartClickSeries(Sender: TCustomChart;
      Series: TChartSeries; ValueIndex: Integer; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure SetTypeBtnClick(Sender: TObject);
    procedure ViewChartBeforeDrawChart(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure ViewChartClickBackground(Sender: TCustomChart;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure CBTranspClick(Sender: TObject);
    procedure cbPropClick(Sender: TObject);
  private
    FProj: PProj;
    FDiagType: Byte;
    FBusGraphPtr: PBusGraph;
    FChartKindPtr: PChartKind;
    FRelAbsSize: Boolean;
// TODO
//    FAlignment: TChartAlignment;
    FSum: Double;
    FAspect: Double;
    FKeyPressed: Boolean;
    FSelectAll: Boolean;
    procedure ResizeViewChart(NewAspect: Double);
    procedure SetBusGraphPtr(Ptr: PBusGraph);
    procedure UpdateParameters;
  public
    IsChangeDlg: Boolean;
    constructor Create(AOwner: TComponent; AData: PProj);
    property BusGraphPtr: PBusGraph read FBusGraphPtr write SetBusGraphPtr;
    property RelAbsSize: Boolean read FRelAbsSize;
// TODO
//    property Alignment: TChartAlignment read FAlignment;
    property Sum: Double read FSum;
  end;

function ProcChangeBusGraph(AData: PProj; SnaItem: PIndex; NewKind: PChartKind; ChangeExisting: Boolean): PChartKind;
function ProcSetBusGraph(AData: PProj; SnaItem: PIndex; KindPtr: PChartKind): PChartKind;

implementation

{$R *.DFM}

const
  ResName: array[0..10] of PChar = ('TPIESERIES', 'TBARSERIES', 'TLINESERIES',
    'TAREASERIES', 'TPOINTSERIES', 'THORIZBARSERIES',
    'TGANTTSERIES', 'TFASTLINESERIES', 'TBUBBLESERIES',
    'TCHARTSHAPE', 'TARROWSERIES');
const
  cnPanelCutting = 7;

constructor TBusGraphDlg.Create(AOwner: TComponent; AData: PProj);
begin
  inherited Create(AOwner);
  FProj := AData;
  FDiagType := dtCircleDiag;
  FBusGraphPtr := nil;
  FChartKindPtr := nil;
  FRelAbsSize := FALSE;
// TODO  
//  FAlignment := alLeftTop;
  FSelectAll := TRUE;
end;

procedure TBusGraphDlg.SetBusGraphPtr(Ptr: PBusGraph);
begin
  FBusGraphPtr := Ptr;
  FChartKindPtr := FBusGraphPtr^.ChartKindPtr;
  FDiagType := FChartKindPtr^.DiagType;
  FRelAbsSize := FBusGraphPtr^.RelAbsSize;
  FSum := FBusGraphPtr^.Sum;
end;

procedure TBusGraphDlg.UpdateParameters;
begin
  if FBusGraphPtr <> nil then
  begin
    XPosVal.AsFloat := FBusGraphPtr^.Position.X / 100;
    YPosVal.AsFloat := FBusGraphPtr^.Position.Y / 100;
    ChartWidthVal.AsFloat := FBusGraphPtr^.ChartWidth / 100;
// TODO
//    FAlignment := FBusGraphPtr^.Alignment;
    FactorVal.AsFloat := FactorVal.DefaultValue;
  end;

  FAspect := 1;
end;

procedure TBusGraphDlg.FormShow(Sender: TObject);
var
  Cnt: Integer;
  TmpItem: PDGraph;
  TmpValue: Double;
  Scale: Double;
  dblRatio: Double;
  THeight: Integer;
begin
  if FBusGraphPtr <> nil then
  begin
    ViewChart.Assign(FChartKindPtr^.TemplateChart);

    Scale := (ChartPanel.Height - cnPanelCutting) / FBusGraphPtr^.ChartHeight;
    TmpValue := (ChartPanel.Width - cnPanelCutting) / FBusGraphPtr^.ChartWidth;
    if TmpValue < Scale then
      Scale := TmpValue;
    ViewChart.Height := Round(FBusGraphPtr^.ChartHeight * Scale);
    ViewChart.Width := Round(FBusGraphPtr^.ChartWidth * Scale);
    ViewChart.Left := (ChartPanel.Width - ViewChart.Width) div 2;
    ViewChart.Top := (ChartPanel.Height - ViewChart.Height) div 2;
    dblRatio := ViewChart.Height / co_BaseChartSize;

    ViewSeries.Clear;
    if ViewSeries.ClassType <> FChartKindPtr^.TemplateChartSeries.ClassType then
      ChangeSeriesType(TChartSeries(ViewSeries), TChartSeriesClass(FChartKindPtr^.TemplateChartSeries.ClassType));

    case FDiagType of
      dtCircleDiag{stCircle}:
        begin
          TPieSeries(ViewSeries).Assign(FChartKindPtr^.TemplateChartSeries);
          if FBusGraphPtr^.RelAbsSize then
          begin
            dblRatio := FBusGraphPtr^.Sum / (100 * co_RelChartPercent);
            TPieSeries(ViewSeries).CustomXRadius := Round(TPieSeries(FChartKindPtr^.TemplateChartSeries).CustomXRadius * dblRatio);
            TPieSeries(ViewSeries).CustomYRadius := Round(TPieSeries(FChartKindPtr^.TemplateChartSeries).CustomYRadius * dblRatio);
            dblRatio := FChartKindPtr^.TemplateChart.Width + (TPieSeries(ViewSeries).CustomXRadius - TPieSeries(FChartKindPtr^.TemplateChartSeries).CustomXRadius) * 2;
            dblRatio := ViewChart.Width / dblRatio;
          end
          else
          begin
            dblRatio := ViewChart.Width / FChartKindPtr^.TemplateChart.Width;
          end;
          TPieSeries(ViewSeries).CustomXRadius := Round(TPieSeries(ViewSeries).CustomXRadius * dblRatio);
          TPieSeries(ViewSeries).CustomYRadius := Round(TPieSeries(ViewSeries).CustomYRadius * dblRatio);
          TPieSeries(ViewSeries).RotationAngle := TPieSeries(FChartKindPtr^.TemplateChartSeries).RotationAngle;
        end;
      dtBeamDiag{stBar}: TBarSeries(ViewSeries).Assign(FChartKindPtr^.TemplateChartSeries);
      dtLineDiag{stLine}: TLineSeries(ViewSeries).Assign(FChartKindPtr^.TemplateChartSeries);
      dtAreaDiag{stArea}: TAreaSeries(ViewSeries).Assign(FChartKindPtr^.TemplateChartSeries);
      dtPointDiag{stPoint}:
        begin
          TPointSeries(ViewSeries).Assign(FChartKindPtr^.TemplateChartSeries);
          TPointSeries(ViewSeries).Pointer.HorizSize := Round(TPointSeries(ViewSeries).Pointer.HorizSize * dblRatio);
          TPointSeries(ViewSeries).Pointer.VertSize := Round(TPointSeries(ViewSeries).Pointer.VertSize * dblRatio);
        end;
    end;

    ViewChart.View3DOptions.Assign(FChartKindPtr^.TemplateChart.View3DOptions);
    ViewSeries.ParentChart := ViewChart;

    if ViewChart.AxisVisible then
    begin
      if ViewChart.LeftAxis.Visible then
        if ViewChart.LeftAxis.Labels then
        begin
          THeight := Round(ViewChart.LeftAxis.LabelsFont.Height * dblRatio);
          if THeight < co_TextVisible then
            ViewChart.LeftAxis.Labels := FALSE
          else
            ViewChart.LeftAxis.LabelsFont.Height := THeight;
          ViewChart.LeftAxis.LabelsSize := Round(ViewChart.LeftAxis.LabelsSize * dblRatio);
        end;
      if ViewChart.BottomAxis.Visible then
        if ViewChart.BottomAxis.Labels then
        begin
          THeight := Round(ViewChart.BottomAxis.LabelsFont.Height * dblRatio);
          if THeight < co_TextVisible then
            ViewChart.BottomAxis.Labels := FALSE
          else
            ViewChart.BottomAxis.LabelsFont.Height := THeight;
          ViewChart.BottomAxis.LabelsSize := Round(ViewChart.BottomAxis.LabelsSize * dblRatio);
        end;
    end;

        { if ViewSeries.Marks.Visible then
            begin
               THeight:=Round(ViewSeries.Marks.Font.Height*dblRatio);
               ViewSeries.Marks.Font.Height:=THeight;
            end; }

    case FDiagType of
      dtCircleDiag{stCircle}:
        begin
          for Cnt := 0 to FBusGraphPtr^.ValItems^.Count - 1 do
          begin
            TmpItem := PDGraph(FBusGraphPtr^.ValItems^.Items[Cnt]);
            TmpValue := TmpItem.Value;
            TPieSeries(ViewSeries).AddPie(TmpValue, TmpItem.Name, TmpItem.Color);
          end;
        end;
      dtBeamDiag://stBar:
        begin
          for Cnt := 0 to FBusGraphPtr^.ValItems^.Count - 1 do
          begin
            TmpItem := PDGraph(FBusGraphPtr^.ValItems^.Items[Cnt]);
            TmpValue := TmpItem.Value;
            TBarSeries(ViewSeries).AddBar(TmpValue, TmpItem.Name, TmpItem.Color);
          end;
        end;
      dtLineDiag://stLine:
        begin
          for Cnt := 0 to FBusGraphPtr^.ValItems^.Count - 1 do
          begin
            TmpItem := PDGraph(FBusGraphPtr^.ValItems^.Items[Cnt]);
            TmpValue := TmpItem.Value;
            TLineSeries(ViewSeries).Add(TmpValue, TmpItem.Name, TmpItem.Color);
          end;
        end;
      dtAreaDiag://stArea:
        begin
          for Cnt := 0 to FBusGraphPtr^.ValItems^.Count - 1 do
          begin
            TmpItem := PDGraph(FBusGraphPtr^.ValItems^.Items[Cnt]);
            TmpValue := TmpItem.Value;
            TAreaSeries(ViewSeries).Add(TmpValue, TmpItem.Name, TmpItem.Color);
          end;
        end;
      dtPointDiag://stPoint:
        begin
          for Cnt := 0 to FBusGraphPtr^.ValItems^.Count - 1 do
          begin
            TmpItem := PDGraph(FBusGraphPtr^.ValItems^.Items[Cnt]);
            TmpValue := TmpItem.Value;
            TPointSeries(ViewSeries).Add(TmpValue, TmpItem.Name, TmpItem.Color);
          end;
        end;
    end;

    if IsChangeDlg then
    begin
      ViewSeries.Marks.Font.Size := Round(ViewSeries.Marks.Font.Size * dblRatio);
    end;

    UpdateParameters;
    CBTransp.Checked := ViewChart.BackWall.Transparent; //ViewChart.Transparent;
    ViewChart.Zoom.Allow := False;

    WidthEdit.OnChange := SizeUpdate;

    if ComboBoxMode.Items.Count > 0 then
    begin
      ComboBoxMode.ItemIndex := 0;
    end;
  end;
end;

procedure TBusGraphDlg.PositionEditClick(Sender: TObject);
begin
  if FSelectAll then
    if Sender = XPosEdit then
    begin
      XPosEdit.SelectAll;
      FSelectAll := FALSE;
    end
    else
      if Sender = YPosEdit then
      begin
        YPosEdit.SelectAll;
        FSelectAll := FALSE;
      end;
end;

procedure TBusGraphDlg.PositionSpinClick(Sender: TObject);
begin
  if Sender = XPosSpin then
  begin
    if not XPosEdit.Focused then
    begin
      XPosEdit.SetFocus;
      XPosEdit.SelLength := 0;
    end;
  end
  else
    if Sender = YPosSpin then
    begin
      if not YPosEdit.Focused then
      begin
        YPosEdit.SetFocus;
        YPosEdit.SelLength := 0;
      end;
    end;
end;

procedure TBusGraphDlg.SizeEditClick(Sender: TObject);
begin
  if FSelectAll then
    if Sender = WidthEdit then
    begin
      WidthEdit.SelectAll;
      FSelectAll := FALSE;
    end;
{      else
         if Sender=HeightEdit then
            begin
               HeightEdit.SelectAll;
               FSelectAll := FALSE;
            end;}
end;

procedure TBusGraphDlg.SizeSpinClick(Sender: TObject);
begin
  if Sender = WidthSpin then
  begin
    if not WidthEdit.Focused then
    begin
      WidthEdit.SetFocus;
      WidthEdit.SelLength := 0;
    end;
  end;
{   else
      if Sender=HeightSpin then
         begin
            if not HeightEdit.Focused then
               begin
                  HeightEdit.SetFocus;
                  HeightEdit.SelLength:=0;
               end;
         end;}
  SizeUpdate(Sender);
end;

procedure TBusGraphDlg.SizeUpdate(Sender: TObject);
var
  NewAspect: Double;
begin
  FSelectAll := TRUE;
  OkBtn.Default := TRUE;
end;

procedure TBusGraphDlg.ResizeViewChart(NewAspect: Double);
var
  NewWidth: Integer;
  NewHeight: Integer;
  PanelWidth: Integer;
  PanelHeight: Integer;
  Scale: Double;
  THeight: Integer;
begin
  if NewAspect > FAspect then
  begin
    NewWidth := Round(ViewChart.Height * NewAspect);
    PanelWidth := ChartPanel.Width - cnPanelCutting;
    if NewWidth > PanelWidth then
    begin
      Scale := PanelWidth / NewWidth;
      NewWidth := PanelWidth;
      NewHeight := Round(NewWidth / NewAspect);
    end
    else
    begin
      ViewChart.Left := (ChartPanel.Width - NewWidth) div 2;
      ViewChart.Width := NewWidth;
      FAspect := NewAspect;
      Exit;
    end;
  end
  else
    if NewAspect < FAspect then
    begin
      NewHeight := Round(ViewChart.Width / NewAspect);
      PanelHeight := ChartPanel.Height - cnPanelCutting;
      if NewHeight > PanelHeight then
      begin
        Scale := PanelHeight / NewHeight;
        NewHeight := PanelHeight;
        NewWidth := Round(NewHeight * NewAspect);
      end
      else
      begin
        ViewChart.Top := (ChartPanel.Height - NewHeight) div 2;
        ViewChart.Height := NewHeight;
        FAspect := NewAspect;
        Exit;
      end;
    end
    else
      Exit;

  ViewChart.Left := (ChartPanel.Width - NewWidth) div 2;
  ViewChart.Top := (ChartPanel.Height - NewHeight) div 2;
  ViewChart.Height := NewHeight;
  ViewChart.Width := NewWidth;

  case FDiagType of
    dtCircleDiag://stCircle:
      begin
        TPieSeries(ViewSeries).CustomXRadius := Round(TPieSeries(ViewSeries).CustomXRadius * Scale);
        TPieSeries(ViewSeries).CustomYRadius := Round(TPieSeries(ViewSeries).CustomYRadius * Scale);
      end;
    dtPointDiag://stPoint:
      begin
        TPointSeries(ViewSeries).Pointer.HorizSize := Round(TPointSeries(ViewSeries).Pointer.HorizSize * Scale);
        TPointSeries(ViewSeries).Pointer.VertSize := Round(TPointSeries(ViewSeries).Pointer.VertSize * Scale);
      end;
  end;

  if ViewChart.AxisVisible then
  begin
    if ViewChart.LeftAxis.Visible then
      if ViewChart.LeftAxis.Labels then
      begin
        THeight := Round(ViewChart.LeftAxis.LabelsFont.Height * Scale);
        if THeight < co_TextVisible then
          ViewChart.LeftAxis.Labels := FALSE
        else
          ViewChart.LeftAxis.LabelsFont.Height := THeight;
        ViewChart.LeftAxis.LabelsSize := Round(ViewChart.LeftAxis.LabelsSize * Scale);
      end;
    if ViewChart.BottomAxis.Visible then
      if ViewChart.BottomAxis.Labels then
      begin
        THeight := Round(ViewChart.BottomAxis.LabelsFont.Height * Scale);
        if THeight < co_TextVisible then
          ViewChart.BottomAxis.Labels := FALSE
        else
          ViewChart.BottomAxis.LabelsFont.Height := THeight;
        ViewChart.BottomAxis.LabelsSize := Round(ViewChart.BottomAxis.LabelsSize * Scale);
      end;
  end;

  if ViewSeries.Marks.Visible then
  begin
    THeight := Round(ViewSeries.Marks.Font.Height * Scale);
    ViewSeries.Marks.Font.Height := THeight;
  end;

  FAspect := NewAspect;
end;

function ProcChangeBusGraph(AData: PProj; SnaItem: PIndex; NewKind: PChartKind; ChangeExisting: Boolean): PChartKind;
var
  ChangeBusGraphDlg: TBusGraphDlg;
  AChartsLibPtr: PChartsLib;
  AChartKindPtr: PChartKind;
  i: Integer;
  NoDlg: Boolean;
begin
  NoDlg := False;
  Result := NewKind;
  ChangeBusGraphDlg := TBusGraphDlg.Create(AData^.Parent, AData);

  ChangeBusGraphDlg.IsChangeDlg := ChangeExisting;
  ChangeBusGraphDlg.CancelBtn.Visible := ChangeBusGraphDlg.IsChangeDlg;
  if not ChangeBusGraphDlg.IsChangeDlg then
  begin

    ChangeBusGraphDlg.Height := ChangeBusGraphDlg.Height - 89;

    ChangeBusGraphDlg.PositionGrBox.Enabled := False;
    ChangeBusGraphDlg.XPosEdit.Enabled := False;
    ChangeBusGraphDlg.YPosEdit.Enabled := False;

    ChangeBusGraphDlg.cbProp.Visible := True;
  end
  else
  begin
    ChangeBusGraphDlg.cbProp.Visible := False;
  end;
  ChangeBusGraphDlg.BusGraphPtr := PBusGraph(SnaItem);
  NoDlg := (not AData^.PInfo^.AnnotSettings.ShowDialog) and (not ChangeExisting);
  if NoDlg = true then
  begin
    ChangeBusGraphDlg.FormShow(nil);

  end;
   {brovak - PLEASE, DON'T CHANGE !!! }
  if ((NoDlg) or (ChangeBusGraphDlg.Showmodal = mrOk)) then
  begin
   {brovak}
    AChartsLibPtr := PChartsLib(AData.PInfo^.ChartViewKinds);
    AChartKindPtr := New(PChartKind, InitByExistChart(ChangeBusGraphDlg.ViewChart, ChangeBusGraphDlg.ViewSeries, ChangeBusGraphDlg.RelAbsSize, ChangeBusGraphDlg.Sum));
    AChartKindPtr := AChartsLibPtr^.AddChartKind(AChartKindPtr, False);
    if (PBusGraph(SnaItem)^.ChartKindPtr <> AChartKindPtr) then
    begin
      PBusGraph(SnaItem)^.DiagType := AChartKindPtr^.DiagType;
      PBusGraph(SnaItem)^.ChartKindPtr := AChartKindPtr;
      PBusGraph(SnaItem)^.ChartHashIndex := AChartKindPtr^.GetHashIndex;
      PBusGraph(SnaItem)^.ChartKindIndex := AChartKindPtr^.Index;
    end;
    with ChangeBusGraphDlg do
    begin
      PBusGraph(SnaItem)^.Position.X := Round(XPosVal.AsFloat * 100);
      PBusGraph(SnaItem)^.Position.Y := Round(YPosVal.AsFloat * 100);
      PBusGraph(SnaItem)^.ChartWidth := Round(ChartWidthVal.AsFloat * 100);
      PBusGraph(SnaItem)^.ChartHeight := Round(ChartWidthVal.AsFloat * 100);
      PBusGraph(SnaItem)^.RelAbsSize := RelAbsSize;
// TODO
//      PBusGraph(SnaItem)^.Alignment := Alignment;
      AChartKindPtr^.bProp := cbProp.Checked;

      AChartKindPtr^.PropFactor := Round(FactorVal.AsFloat * 100);
      AChartKindPtr^.PropMode := ComboBoxMode.ItemIndex;

      for i := 0 to PBusGraph(SnaItem)^.ValItems^.Count - 1 do
      begin
        PDGraph(PBusGraph(SnaItem)^.ValItems^.At(i))^.Color := ViewSeries.ValueColor[i];
      end;
    end;

    SnaItem^.Invalidate(AData^.PInfo);
    PBusGraph(SnaItem)^.CalculateClipRect;
    AData^.UpdateClipRect(PView(SnaItem));
    SnaItem^.Invalidate(AData^.PInfo);
    AData^.SetModified;
    Result := PBusGraph(SnaItem)^.ChartKindPtr;
  end
  else
  begin
    Result := nil;
  end;
  ChangeBusGraphDlg.Free;
end;

function ProcSetBusGraph(AData: PProj; SnaItem: PIndex; KindPtr: PChartKind): PChartKind;
var
  AChartsLibPtr: PChartsLib;
  AChartKindPtr: PChartKind;
  i: Integer;
begin
  Result := nil;
  AChartsLibPtr := PChartsLib(AData^.PInfo^.ChartViewKinds);
  if KindPtr = nil then
  begin
    AChartKindPtr := PBusGraph(SnaItem)^.ChartKindPtr;
    AChartKindPtr := AChartsLibPtr^.AddChartKind(AChartKindPtr, True);
  end
  else
  begin
    AChartKindPtr := KindPtr;
  end;
  PBusGraph(SnaItem)^.ChartKindPtr := AChartKindPtr;
  PBusGraph(SnaItem)^.ChartHashIndex := AChartKindPtr^.GetHashIndex;
  PBusGraph(SnaItem)^.ChartKindIndex := AChartKindPtr^.Index;
  PBusGraph(SnaItem)^.CalculateClipRect;
  Result := PBusGraph(SnaItem)^.ChartKindPtr;
  AData^.UpdateClipRect(PView(SnaItem));
  SnaItem^.Invalidate(AData^.PInfo);
  AData^.CorrectSize(PBusGraph(SnaItem)^.ClipRect, False);
  AData^.PInfo^.RedrawRect(PBusGraph(SnaItem)^.ClipRect);
end;

procedure TBusGraphDlg.ChangeBtnClick(Sender: TObject);
begin
  ViewChartEditor.Execute;
end;

procedure TBusGraphDlg.ViewChartClickSeries(Sender: TCustomChart;
  Series: TChartSeries; ValueIndex: Integer; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbLeft then
  begin
    ColorDialog1.Color := Series.ValueColor[ValueIndex];
    if ColorDialog1.Execute then
    begin
      Series.ValueColor[ValueIndex] := ColorDialog1.Color;
      if not IsChangeDlg then
      begin
        BusGraphColors[ValueIndex] := ColorDialog1.Color;
      end;
    end;
  end;
end;

procedure TBusGraphDlg.ViewChartClickBackground(Sender: TCustomChart;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbLeft then
  begin
    ColorDialog1.Color := Sender.Color;
    if ColorDialog1.Execute then
    begin
      Sender.Color := ColorDialog1.Color;
    end;
  end;
end;

procedure TBusGraphDlg.SetTypeBtnClick(Sender: TObject);
begin
  try
// TODO
//    ViewChartEditor.SetType;
    if (ViewChart.SeriesList.Items[0] <> nil) then
    begin
      ViewChart.View3DOptions.Orthogonal := (ViewChart.View3D) and (ViewChart.SeriesList.Items[0].ClassName <> 'TPieSeries');
    end;
  except
  end;
end;

procedure TBusGraphDlg.ViewChartBeforeDrawChart(Sender: TObject);
begin
  try
    if (ViewChart.SeriesList.Items[0].ClassName = 'TPieSeries') then
    begin
      TPieSeries(ViewChart.SeriesList.Items[0]).Circled := True;
    end;
  except
  end;
end;

procedure TBusGraphDlg.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  CanClose := True; //(ModalResult = mrOk) or (IsChangeDlg);

end;

procedure TBusGraphDlg.CBTranspClick(Sender: TObject);
begin
  ViewChart.BackWall.Transparent := CBTransp.Checked;
//  ViewChart.Transparent := CBTransp.Checked;
end;

procedure TBusGraphDlg.cbPropClick(Sender: TObject);
begin
  EditFactor.Visible := cbProp.Checked;
  LabelFactor.Visible := cbProp.Checked;
  FactorSpin.Visible := cbProp.Checked;
  ComboBoxMode.Visible := cbProp.Checked;
end;

end.

