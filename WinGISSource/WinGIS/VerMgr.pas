Unit VerMgr;

Interface

Function GetApplicationName:String;

Function GetHelpFileName:String;

Implementation

Uses ClpHndl,DrawHndl,InfoHndl,LayerHndl,WinOSInfo
     {$IFNDEF AXDLL} // <----------------- AXDLL
     ,LinkHndl,DigHndl
     {$ENDIF} // <----------------- AXDLL
     ,OptHndl, ObjEditHndl
     {$IFNDEF AXDLL} // <----------------- AXDLL
     ,PrnHndl,PropHndl
     {$ENDIF} // <----------------- AXDLL
     ,SelectHndl
     {$IFNDEF AXDLL} // <----------------- AXDLL
     ,SymHndl,ViewHndl
     {$ENDIF} // <----------------- AXDLL
     ,ConvertHndl,
     DimensioningHndl,ConstructHndl,ListHndl,TooltipHndl,DatabaseHndl,
     SysUtils,Forms, IniFiles, MultiLng, Dialogs
{++ Ivanoff NA2 BUILD#106}
     , DrawOrthoHndl
{-- Ivanoff}
{$IFNDEF WMLT}
     {$IFNDEF AXDLL} // <----------------- AXDLL
{++ IDB}
     , IDB_MenuHndl
{-- IDB}
     {$ENDIF} // <----------------- AXDLL
     
{$ENDIF}
;

Function GetApplicationName: String;
begin
     Result:=ChangeFileExt(ExtractFileName(Application.ExeName),'');
end;

Function GetHelpFileName: String;
var
WGIni: TIniFile;
AFileName: AnsiString;
begin
    Result:='';
    WGIni:=TIniFile.Create(OSInfo.WingisIniFileName);
    try
       AFileName:=WGIni.ReadString('SETTINGS','HelpFile','WinGIS_044.chm');
       if FileExists(OSInfo.WingisDir + AFileName) then begin
          Result:=OSInfo.WingisDir + AFileName;
       end
       else begin
          AFileName:='WinGIS_044.chm';
          if FileExists(OSInfo.WingisDir + AFileName) then begin
             Result:=OSInfo.WingisDir + AFileName;
          end;
       end;
    finally
       WGIni.Free;
    end;
end;

end.
