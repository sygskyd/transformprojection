library xplan32;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

  {
uses
  WinTypes,
  WinProcs,
  SysUtils,
  Classes;

 }
Const diNotFound   = -1;
      diAnyError   = -2;
      diInvData    = -3;

      diOK         = 0;
      diPosition   = 1;
      diClear      = 2;
      diCancel     = 3;
      diEndPoly    = 4;
      diPoint      = 5;
      diPoly       = 6;
      diCPoly      = 7;
      diSnap       = 8;

var RecStr         : String;
    PosX           : Real;
    PosY           : Real;
    Ignore         : Boolean;

Function InitDriver
   (
   CID             : Integer;
   HWindow         : Integer
   )
   : Integer; Export; stdcall;
  begin
    RecStr:='';
    PosX:=0;
    PosY:=0;
    InitDriver:=diOK;
    Ignore:=FALSE;
  end;

Function GetValue
   (
   AStr            : String;
   var AValue      : Real
   )
   : Boolean;
  var Cnt          : Integer;
      AError       : Integer;
  begin
    Delete(AStr,1,2);
    Delete(AStr,Length(AStr)-1,2);
    Cnt:=1;
    while (Cnt<=Length(AStr)) and (AStr[Cnt]=' ') do Inc(Cnt);
    Delete(AStr,1,Cnt-1);
    Val(AStr,AValue,AError);
    GetValue:=AError<>0;
  end;

Function ConvertStr
   : Integer;
  var WorkStr      : String;
  begin
    WorkStr:=Copy(RecStr,1,2);
    if (WorkStr='X ') or (WorkStr='XC') then begin
      if GetValue(RecStr,PosY) then ConvertStr:=diInvData
      else ConvertStr:=diOK;
    end
    else if (WorkStr='Y ') or (WorkStr='YC') then begin
      if GetValue(RecStr,PosX) then ConvertStr:=diInvData
      else if not Ignore then ConvertStr:=diPosition
      else begin
        ConvertStr:=diOK;
        Ignore:=FALSE;
      end;
    end
    else if WorkStr='CA' then ConvertStr:=diClear
    else if WorkStr='CL' then ConvertStr:=diCancel
    else if WorkStr='F0' then ConvertStr:=diPoint
    else if WorkStr='F1' then ConvertStr:=diPoly
    else if WorkStr='F2' then ConvertStr:=diCPoly
    else if WorkStr='+-' then begin
      Ignore:=TRUE;
      ConvertStr:=diSnap;
    end
    else if Copy(RecStr,1,3)='END' then ConvertStr:=diEndPoly
    else ConvertStr:=diOK;
  end;

Function ReceiveChar
   (
   CID             : Integer;
   AChar           : Char;
   var XPos        : Real;
   var YPos        : Real
   )
   : Integer; Export; StdCall;
  begin
    ReceiveChar:=diOK;
    if AChar=#10 then begin
      ReceiveChar:=ConvertStr;
      XPos:=PosX;
      YPos:=PosY;
      RecStr:='';
    end
    else if AChar<>#13 then RecStr:=RecStr+AChar;
  end;

Function DoneDriver
   : Integer; Export; StdCall;
  begin
    DoneDriver:=diOK;
  end;

Exports InitDriver,
        ReceiveChar,
        DoneDriver;

  
begin
end.
 