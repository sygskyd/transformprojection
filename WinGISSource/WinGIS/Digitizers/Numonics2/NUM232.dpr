library NUM232;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses  SysUtils;

{$R *.RES}

Const diAnyError   = -2;
      diInvData    = -3;

      diOK         = 0;
      diPosition   = 1;
      diClear      = 2;
      diCancel     = 3;
      diEndPoly    = 4;
      diPoint      = 5;
      diPoly       = 6;
      diCPoly      = 7;
      diSnap       = 8;

var RecStr         : String;
    PosX           : Real;
    PosY           : Real;

Function InitDriver
   (
   CID             : Integer;
   HWindow         : Integer
   )
   : Integer; Export; stdcall;
  begin
    RecStr:='';
    PosX:=0;
    PosY:=0;
    RecStr:=':AF'#13#10':PT'#13#10;
    if FileWrite(CID,RecStr[1],10)<0 then InitDriver:=diAnyError
    else InitDriver:=diOK;
  end;

Function SplitStr
   (
   var InStr       : String
   )
   : String;
  var OutStr       : String;
      APos         : Integer;
  begin
    APos:=Pos(',',InStr);
    if APos=0 then begin
      OutStr:=InStr;
      InStr:='';
    end
    else begin
      OutStr:=Copy(InStr,1,APos-1);
      Delete(InStr,1,APos);
    end;
    SplitStr:=OutStr;
  end;

Function ConvertStr
   : Integer;
  var Key          : Integer;
      AError       : Integer;
  begin
    ConvertStr:=diInvData;

    Val(Copy(RecStr,1,4),PosY,AError);

    if AError=0 then begin

      Val(Copy(RecStr,6,4),PosX,AError);

      if AError=0 then begin

        Val(Copy(RecStr,11,1),Key,AError);

        if AError=0 then begin
          case Key of
            1 : ConvertStr:=diPosition;
            4 : ConvertStr:=diClear;
            8 : ConvertStr:=diCancel;
            2 : ConvertStr:=diEndPoly;
            else ConvertStr:=diOK;
          end;
        end;
      end;
    end;
  end;

Function ReceiveChar
   (
   CID             : Integer;
   AChar           : Char;
   var XPos        : Real;
   var YPos        : Real
   )
   : Integer; Export; stdcall;
  begin
    ReceiveChar:=diOK;
    if AChar=#13 then begin
      ReceiveChar:=ConvertStr;
      XPos:=PosX*100;
      YPos:=PosY*100;
      RecStr:='';
    end
    else if AChar<>#10 then RecStr:=RecStr+AChar;
  end;

Function DoneDriver
   : Integer; Export; Stdcall;
  begin
    DoneDriver:=diOK;
  end;

Exports InitDriver,
        ReceiveChar,
        DoneDriver;

begin
end.
