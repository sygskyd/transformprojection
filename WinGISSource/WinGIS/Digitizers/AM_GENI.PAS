{****************************************************************************}
{ Unit                                                                       }
{----------------------------------------------------------------------------}
{                                                                            }
{----------------------------------------------------------------------------}
{ Modifikationen                                                             }
{  16.02.1994 Martin Forst                                                   }
{****************************************************************************}
Library AM_Geni;

Uses WinTypes,WinProcs,SysUtils;

Const diNotFound   = -1;
      diAnyError   = -2;
      diInvData    = -3;

      diOK         = 0;
      diPosition   = 1;
      diClear      = 2;
      diCancel     = 3;
      diEndPoly    = 4;
      diPoint      = 5;
      diPoly       = 6;
      diCPoly      = 7;
      diSnap       = 8;

var RecStr         : String;
    PosX           : Real;
    PosY           : Real;

Function InitDriver
   (
   CID             : Integer;
   HWindow         : Integer
   )
   : Integer; Export; stdcall;
  begin
    PosX:=0;
    PosY:=0;
    RecStr:='B';
    if FileWrite(CID,RecStr[1],1)<=0 then InitDriver:=diAnyError
    else InitDriver:=diOK;
    RecStr:='';
  end;

Function SplitStr
   (
   var InStr       : String
   )
   : String;
  var OutStr       : String;
      APos         : Integer;
  begin
    APos:=Pos(',',InStr);
    if APos=0 then begin
      OutStr:=InStr;
      InStr:='';
    end
    else begin
      OutStr:=Copy(InStr,1,APos-1);
      Delete(InStr,1,APos);
    end;
    SplitStr:=OutStr;
  end;

Function ConvertStr
   : Integer;
  var Key          : Integer;
      AError       : Integer;
  begin
    ConvertStr:=diInvData;
    Val(SplitStr(RecStr),PosX,AError);
    if AError=0 then begin
      Val(SplitStr(RecStr),PosY,AError);
      if AError=0 then begin
        Val(SplitStr(RecStr),Key,AError);
        if AError=0 then begin
          case Key of
            1 : ConvertStr:=diPosition;
            2 : ConvertStr:=diClear;
            3 : ConvertStr:=diCancel;
            4 : ConvertStr:=diEndPoly;
            else ConvertStr:=diOK;
          end;
        end;
      end;
    end;
  end;

Function ReceiveChar
   (
   CID             : Integer;
   AChar           : Char;
   var XPos        : Real;
   var YPos        : Real
   )
   : Integer; Export; stdcall;
  begin
    ReceiveChar:=diOK;
    if AChar=#10 then begin
      ReceiveChar:=ConvertStr;
      XPos:=PosX;
      YPos:=PosY;
      RecStr:='';
    end
    else if AChar<>#13 then RecStr:=RecStr+AChar;
  end;

Function DoneDriver
   : Integer; Export; stdcall;
  begin
    DoneDriver:=diOK;
  end;

Exports InitDriver,
        ReceiveChar,
        DoneDriver;

begin
end.
