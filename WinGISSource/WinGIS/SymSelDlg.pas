unit SymSelDlg;

interface
{$IFNDEF AXDLL} // <----------------- AXDLL

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, MultiLng, DipGrid;

type
  TSymbolSelDlg = class(TForm)
    OKBtn: TButton;
    MlgSection1: TMlgSection;
    LabelTT: TLabel;
    procedure FormDblClick(Sender: TObject);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
  private
  end;

var
  SymbolSelDlg: TSymbolSelDlg;

{$ENDIF} // <----------------- AXDLL
implementation
{$IFNDEF AXDLL} // <----------------- AXDLL
{$R *.DFM}

procedure TSymbolSelDlg.FormDblClick(Sender: TObject);
begin
     Close;
end;
{$ENDIF} // <----------------- AXDLL


procedure TSymbolSelDlg.FormMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
     LabelTT.Caption:='';
end;

end.
