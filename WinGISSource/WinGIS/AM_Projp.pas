{********************************************************************************}
{ Unit AM_ProjP                                                                  }
{--------------------------------------------------------------------------------}
{                                                                                }
{--------------------------------------------------------------------------------}
{ Modifikationen                                                                 }
{  06.06.1997 Heinz Ofner   Erweiterung von AM_ProjO                             }
{  16-MAY-2000 Sigolaeff: Add Omega support functions                            }
{  05.09.2000 Moskaliov: Add Printing support functions                          }
{********************************************************************************}
unit AM_ProjP;

interface

uses SysUtils, WinTypes, WinProcs, Objects, Messages, AM_Proj, AM_Def, AM_Ini,
  AM_Dlg6, AM_Text, AM_Index, AM_View, AM_Layer, Controls, BMPImage, Classes, AM_Poly,
  FileCtrl, MenuHndl, ProjHndl,
{$IFNDEF AXDLL} // <----------------- AXDLL
  PrnHndl,
{$ENDIF} // <----------------- AXDLL
  WPrinter, //++ Moskaliov
{$IFNDEF AXDLL} // <----------------- AXDLL
  WOpenPicDlg, ImgDlg, OmegaHndl,
{$ENDIF} // <----------------- AXDLL
  AM_Child, IniFiles, VerMgr,
  TabFiles,
{$IFNDEF AXDLL} // <----------------- AXDLL
  TabDlg,
{$ENDIF} // <----------------- AXDLL
  UserUtils
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
  , AM_DDE
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
  ;

{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
function FuncZoomToObject(AData: PProj; AObjIndex: LongInt): Boolean;

procedure ProcAutoSnap(AData: PProj);

procedure ProcAutoSnapFromDB(AData: PProj; ARadius: Integer; LayerName1, LayerName2: AnsiString);

procedure ProcCheckObjectExist(AData: PProj);

procedure ProcClearLayer(AData: PProj);

procedure ProcClearLayer2(AData: PProj; ALayer: PLayer; AComMode: Boolean = False);

procedure ProcCombine(AData: PProj);

procedure ProcCombineSearch(AData: PProj; APoly: PPoly; AReverse: Boolean);

procedure ProcCopyObjectFromDB(AData: PProj; AnswerStr: string);

procedure ProcDBDenySession(AData: PProj);

procedure ProcDBInsert(AData: PProj);

procedure ProcDBLoadNext(AData: PProj);

procedure ProcDBProjSaveAs(AData: PProj; AParent: TWinControl);

procedure ProcDBRecharge(AData: PProj);

procedure ProcDBRequestHatchStyles(AData: PProj);

procedure ProcDBRequestLineStyles(AData: PProj);

procedure ProcDBRequestSymbols(AData: PProj);

//Procedure ProcDBSendPolyData(AData:PProj); overload;
procedure ProcDBSendPolyData(AData: PProj; ilIndex: LongInt); //overload;

procedure ProcDBSetActSymbol(AData: PProj);

function ProcDBSetGraphicMode(AData: PProj): Boolean;

procedure ProcDBSetTranspSelLayers(AData: PProj);

procedure ProcDBSuppressDlgAnnotChart(AData: PProj);

procedure ProcGetNextObjIDs(AData: PProj);

{++Sygsky: Omega }
function ProcInsertBitmap(AWindow: TMDIChild; const AFileName: string; How2Open: OpenStatus): Boolean;
function ProcInsertCBitMap(AWindow: TMDIChild; const AFileName: AnsiString;
  Coords: array of double; ObjAddr: PPImage = nil;
  TabAddress: PTabFile = nil): Boolean;
{--Sygsky: Omega }

procedure ProcInsertBitmapsFromDB(AData: PProj);

procedure ProcMakeBTXFile(AData: PProj);

procedure ProcMakeOffset(AData: PProj);

procedure ProcMakeOffset2(AData: PProj);

procedure ProcMeasureOptions(AData: PProj);

 {+++BrovaK bug 169 build 135}
procedure RedrawMeasureLine(AData: PProj);
 {brovak}
{++ Sygsky }
function FuncChangeEllipse(Item: PIndex; AData: PProj; NewPos: TDPoint; NewRad: Integer): Boolean;
{-- Sygsky }
procedure ProcMoveRelSelected(AData: PProj; XMove: LongInt; YMove: LongInt);

{++ Moskaliov AXWinGIS Printing Methods BUILD#127 05.09.00}
{   This is never used.
Procedure ProcPrint(AData:PProj);   }
{-- Moskaliov AXWinGIS Printing Methods BUILD#127 05.09.00}

procedure ProcPrintFromDB(AData: PProj);

{++ Moskaliov AXWinGIS Printing Methods BUILD#127 05.09.00}
procedure ProcSendPrintResultToDB(CommandName: string; ErrorCode: Integer);
function FuncPrintFromDB(Mode: Integer; FileName: string; Copies: Integer;
  CollateCopies: Boolean; ShowProgress: Boolean;
  PrintMonitoring: Boolean; AData: PProj): Integer;
{-- Moskaliov AXWinGIS Printing Methods BUILD#127 05.09.00}

{++ Moskaliov AXWinGIS Printing Methods BUILD#133 02.10.00}
procedure ProcSetPrnParamFromDB(AData: PProj);
procedure ProcSetPgsPrnParamFromDB(AData: PProj);
procedure ProcSetMapParamFromDB(AData: PProj);
procedure ProcSetLegParamFromDB(AData: PProj);
procedure ProcInsLegendFromDB(AData: PProj);
procedure ProcGetPrnParamFromDB(AData: PProj);
procedure ProcGetPgsPrnParamFromDB(AData: PProj);
procedure ProcGetMapParamFromDB(AData: PProj);
procedure ProcGetLegParamFromDB(AData: PProj);
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 02.10.00}
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 23.10.00}
procedure ProcSetPicParamFromDB(AData: PProj);
procedure ProcGetPicParamFromDB(AData: PProj);
procedure ProcInsPictureFromDB(AData: PProj);
procedure ProcSetSigParamFromDB(AData: PProj);
procedure ProcGetSigParamFromDB(AData: PProj);
procedure ProcInsSignatureFromDB(AData: PProj);
procedure ProcStoreTemplateFromDB(AData: PProj);
procedure ProcLoadTemplateFromDB(AData: PProj);
procedure ProcGetObjInformFromDB(AData: PProj);
procedure ProcDeleteObjectFromDB(AData: PProj);
procedure ProcMoveObjectFromDB(AData: PProj);
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 03.11.00}

//++ Glukhov Bug#100 Build#152 05.03.01
// Auxiliary procedures
procedure ProcSendResultToDB(Topic: string; CmdName: string; Res: string);
procedure ProcSendIntResultToDB(Topic: string; CmdName: string; Res: Integer);
// Deletes the 1st DDE-string (command) which was read already
procedure ProcDDEDelString;
// Gets the layer for the DDE-command without parameter Layer
procedure ProcDDEReadLayer(AData: PProj; var Layer: PLayer; var PosShift: Integer);
// Gets the layer for the DDE-command with parameter Layer
procedure ProcDDEFormLayer(AData: PProj; sLayer: string; var Layer: PLayer);
//-- Glukhov Bug#100 Build#152 05.03.01

//++ Glukhov Bug#84 BUILD#125 02.11.00
procedure ProcReversePolyLine(AData: PProj);
//-- Glukhov Bug#84 BUILD#125 02.11.00

//++ Glukhov Bug#203 Build#151 08.02.01
procedure ProcGetCoordinates(AData: PProj);
procedure ProcSendCoordinates(AData: PProj; X, Y: LongInt);
//-- Glukhov Bug#203 Build#151 08.02.01

//++ Glukhov Bug#100 Build#152 05.03.01
// Creates image via DDE
procedure ProcSetPicture(AData: PProj);
//-- Glukhov Bug#100 Build#152 05.03.01

procedure ProcRasterTransform(AData: PProj);

procedure ProcRecharge(AData: PProj; AParent: TWinControl);

procedure ProcSendDistanceToDB(ADistance: Double);

procedure ProcSendObjPropertiesToDB(AData: PProj);

procedure ProcSendProjectPart(AData: PProj);

procedure ProcSendSelectedObjectsToDB(AData: PProj);

procedure ProcSetActSymbol(AData: PProj; SymID: Integer; IDFound: Boolean; SymName: string;
  SymNameFound: Boolean; LibName: string; LibNameFound: Boolean);

procedure ProcSetAnnotationOptsFromDB(AData: PProj);

procedure ProcSetBitmapOptionsFromDB(AData: PProj; DDELine: PChar);

procedure ProcSetDBSelLayerFromDB(AData: PProj; DDELine: PChar);

procedure ProcSetOptionsFromDB(AData: PProj);

procedure ProcSetSelModeGIS(AData: PProj);

procedure ProcSetTranspModeFromDB(AData: PProj);

procedure ProcSetZoomFactFromDB(AData: PProj; DDELine: PChar);

function ProcSnapObjects(AData: PProj; DrawPos: TDPoint; Exclude: LongInt; ObjTypes: TObjectTypes;
  var SnapPos: TDPoint; var SnapObj: PIndex): Boolean;

procedure ProcStartPolyOverlayFromDB(AData: PProj);

procedure ProcTrimmDlg(AData: PProj);

procedure ProcTurboRaster;

procedure ProcZoomToAllObjects(AData: PProj);

procedure ProcZoomToProjPart(AData: PProj);

procedure ProcSelectEqualObjects(AData: PProj; ALayer: PLayer);
procedure ProcFindObjectOnLayer(AData: PProj; ALayerName: string; AId: Integer);

procedure ProcSetMeasData(AData: PProj; ArrowLen, Distance: Integer; DrawAngle: Boolean; TextHeight: Integer);

procedure ProcSaveViewAsImage(AData: PProj; AFilename: AnsiString);

{$ENDIF} // <----------------- AXDLL
{$ENDIF}

implementation
{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}

uses WinDOS, AM_Main, AM_ProjO, ResDlg, AM_Obj, AM_Point, AM_Group, AM_Combi, TrLib32,
  AM_CPoly, UserIntf, AM_Selec, AM_Offse, Forms, AM_Circl, MenuFn, GrTools, AM_Snap,
  AM_Meas, MultiLng, Dialogs, CoordinateSystem, ListHndl, SymRol, ExtLib, AM_Cut,
  AttachHndl, AM_Sym, // Glukhov ObjAttachment 19.10.00
  AM_Pass, LayerHndl, // Glukhov Bug#100 Build#152 05.03.01
  XFills, XLines, DDEDef, Graphics, JPEG, Clipbrd, WinOSInfo, ProjectionTools
{++ IDB_UNDO}
  , IDB_CallbacksDef
{++ IDB_UNDO}
  ;

procedure ProcSetMeasData(AData: PProj; ArrowLen, Distance: Integer; DrawAngle: Boolean; TextHeight: Integer);
begin
  AData^.MeasureData.Distance := Distance;
  AData^.MeasureData.DrawAngle := DrawAngle;

  AData^.MeasureData.FontData.Height := TextHeight;

  if ArrowLen <= 0 then
    ArrowLen := 1;

  if AData^.MeasureData.ArrowLen <> ArrowLen then
  begin
    AData^.MeasureData.ArrowLen := ArrowLen;
    AData^.PInfo^.ArrowLen := ArrowLen;
    RedrawMeasureLine(AData);
  end;
end;

procedure ProcStartPolyOverlayFromDB
  (
  AData: PProj
  );
var
  DDELine: array[0..512] of Char;
  TempStr: string;
  TempInt: Integer;
  StrToLong: Boolean;
  MoreStrs: Boolean;
  StrFound: Boolean;
  AllParams: Boolean;
  SourceLayer1: PLayer;
  SourceLayer2: PLayer;
  TargetLayer: PLayer;
  AddSub: Integer;
  CheckAreas: Boolean;
  DBOverlay: Boolean;
  MergeAreas: Boolean;

  procedure ReadDDELine
      (
      Item: PChar
      ); far;
  begin
    if StrLComp(Item, '[DPO]', 5) = 0 then
    begin
      StrCopy(DDELine, Item);
      Item[0] := DDEStrUsed;
    end;
  end;
begin
  AllParams := TRUE;
  DDEHandler.DDEData^.ForEach(@ReadDDELine);
  DDEHandler.DeleteDDEData;
    // Layerinfos as text
  if (DDEHandler.FLayerInfo = 0) or (DDEHandler.FLayerInfo = 1) then
  begin
      // Sourcelayer1
    if ReadDDECommandString(DDELine, 1, 0, WgBlockStart, WgBlockEnd,
      WgBlockEnd, TempStr, StrToLong, MoreStrs) then
    begin
      if TempStr <> '' then
      begin
        SourceLayer1 := AData^.Layers^.NameToLayer(TempStr);
        if SourceLayer1 = nil then
          AllParams := FALSE;
      end
      else
        AllParams := FALSE;
    end
    else
      AllParams := FALSE;
      // Sourcelayer2
    if ReadDDECommandString(DDELine, 2, 0, WgBlockStart, WgBlockEnd,
      WgBlockEnd, TempStr, StrToLong, MoreStrs) then
    begin
      if TempStr <> '' then
      begin
        SourceLayer2 := AData^.Layers^.NameToLayer(TempStr);
        if SourceLayer2 = nil then
          AllParams := FALSE;
      end
      else
        AllParams := FALSE;
    end
    else
      AllParams := FALSE;
      // Targetlayer
    if ReadDDECommandString(DDELine, 3, 0, WgBlockStart, WgBlockEnd,
      WgBlockEnd, TempStr, StrToLong, MoreStrs) then
    begin
      if TempStr <> '' then
      begin
        TargetLayer := AData^.Layers^.NameToLayer(TempStr);
        if (TargetLayer = nil) or (TargetLayer^.GetState(sf_LayerOff)) or
          (TargetLayer^.GetState(sf_Fixed)) then
          AllParams := FALSE;
      end
      else
        AllParams := FALSE;
    end
    else
      AllParams := FALSE;
  end
    // Layerinfos as index
  else
    if (DDEHandler.FLayerInfo = 2) then
    begin
      // Sourcelayer1
      if ReadDDECommandLongInt(DDELine, 1, 0, WgBlockStart, WgBlockEnd,
        WgBlockEnd, TempInt, StrFound, MoreStrs) then
      begin
        if TempInt >= 0 then
        begin
          SourceLayer1 := AData^.Layers^.IndexToLayer(TempInt);
          if SourceLayer1 = nil then
            AllParams := FALSE;
        end
        else
          AllParams := FALSE;
      end
      else
        AllParams := FALSE;
      // Sourcelayer2
      if ReadDDECommandLongInt(DDELine, 2, 0, WgBlockStart, WgBlockEnd,
        WgBlockEnd, TempInt, StrFound, MoreStrs) then
      begin
        if TempInt >= 0 then
        begin
          SourceLayer2 := AData^.Layers^.IndexToLayer(TempInt);
          if SourceLayer2 = nil then
            AllParams := FALSE;
        end
        else
          AllParams := FALSE;
      end
      else
        AllParams := FALSE;
      // Targetlayer
      if ReadDDECommandLongInt(DDELine, 3, 0, WgBlockStart, WgBlockEnd,
        WgBlockEnd, TempInt, StrFound, MoreStrs) then
      begin
        if TempInt >= 0 then
        begin
          TargetLayer := AData^.Layers^.IndexToLayer(TempInt);
          if (TargetLayer = nil) or (TargetLayer^.GetState(sf_LayerOff)) or
            (TargetLayer^.GetState(sf_Fixed)) then
            AllParams := FALSE;
        end
        else
          AllParams := FALSE;
      end
      else
        AllParams := FALSE;
    end;
    // Additive or subractive
  if ReadDDECommandLongInt(DDELine, 4, 0, WgBlockStart, DDEHandler.FDDESepSign,
    WgBlockEnd, TempInt, StrFound, MoreStrs) then
  begin
    if TempInt = 0 then
      AddSub := 0 {add}
    else
      if TempInt = 1 then
        AddSub := 1 {sub}
      else
        AllParams := FALSE;
  end
  else
    AllParams := FALSE;
    // Check areas
  if ReadDDECommandLongInt(DDELine, 4, 1, WgBlockStart, DDEHandler.FDDESepSign,
    WgBlockEnd, TempInt, StrFound, MoreStrs) then
  begin
    if TempInt = 0 then
      CheckAreas := FALSE
    else
      if TempInt = 1 then
        CheckAreas := TRUE
      else
        CheckAreas := FALSE;
  end
  else
    CheckAreas := FALSE;
    // Database overlay
  DBOverlay := FALSE;
  if ReadDDECommandLongInt(DDELine, 4, 2, WgBlockStart, DDEHandler.FDDESepSign,
    WgBlockEnd, TempInt, StrFound, MoreStrs) then
  begin
    if TempInt = 1 then
      DBOverlay := TRUE;
  end;
    // Merge areas
  if ReadDDECommandLongInt(DDELine, 4, 3, WgBlockStart, DDEHandler.FDDESepSign,
    WgBlockEnd, TempInt, StrFound, MoreStrs) then
  begin
    if TempInt = 0 then
      MergeAreas := FALSE
    else
      if TempInt = 1 then
        MergeAreas := TRUE
      else
        MergeAreas := FALSE;
  end
  else
    MergeAreas := FALSE;
  if AllParams then
    AreaCut7(AData, SourceLayer1.Index, SourceLayer2.Index, TargetLayer.Index,
      AddSub, CheckAreas, DBOverlay, MergeAreas);
end;

procedure ProcDBSuppressDlgAnnotChart
  (
  AData: PProj
  );
var
  DDELine: array[0..20] of Char;
  TempInt: Integer;
  MoreStrs: Boolean;
  StrFound: Boolean;

  procedure ReadDDELine
      (
      Item: PChar
      ); far;
  begin
    if StrLComp(Item, '[SDL]', 5) = 0 then
    begin
      StrCopy(DDELine, Item);
      Item[0] := DDEStrUsed;
    end;
  end;
begin
  DDEHandler.DDEData^.ForEach(@ReadDDELine);
  DDEHandler.DeleteDDEData;
  if ReadDDECommandLongInt(DDELine, 1, 0, WgBlockStart, WgBlockEnd,
    WgBlockEnd, TempInt, StrFound, MoreStrs) then
  begin
    if TempInt = 0 then
      AData^.PInfo^.AnnotSettings.ShowDialog := TRUE
    else
      if TempInt = 1 then
        AData^.PInfo^.AnnotSettings.ShowDialog := FALSE;
  end;
end;

procedure ProcSetAnnotationOptsFromDB
  (
  AData: PProj
  );
var
  DDELine: array[0..255] of Char;
  TempStr: string;
  TempInt: Integer;
  TempReal: Real;
  ALayer: PLayer;
  StrToLong: Boolean;
  MoreStrs: Boolean;
  StrFound: Boolean;

  procedure ReadDDELine
      (
      Item: PChar
      ); far;
  begin
    if StrLComp(Item, '[STS]', 5) = 0 then
    begin
      StrCopy(DDELine, Item);
      Item[0] := DDEStrUsed;
    end;
  end;
begin
  DDEHandler.DDEData^.ForEach(@ReadDDELine);
  DDEHandler.DeleteDDEData;
    // FontName
  if ReadDDECommandString(DDELine, 1, 0, WgBlockStart, WgBlockEnd,
    WgBlockEnd, TempStr, StrToLong, MoreStrs) then
  begin
    if TempStr <> '' then
    begin
      TempInt := AData^.PInfo^.Fonts.IDFromName(TempStr);
      if TempInt <> 0 then
        AData^.PInfo^.AnnotSettings.AnnotFont.Font := TempInt;
    end;
  end;
    // FontStyle
  if ReadDDECommandLongInt(DDELine, 2, 0, WgBlockStart, WgBlockEnd,
    WgBlockEnd, TempInt, StrFound, MoreStrs) then
  begin
    if (TempInt > 0) and (TempInt < 128) then
      AData^.PInfo^.AnnotSettings.AnnotFont.Style := TempInt;
  end;
    // FontHeight
  if ReadDDECommandReal(DDELine, 3, 0, WgBlockStart, WgBlockEnd, WgBlockEnd,
    TempReal, StrFound, MoreStrs) then
  begin
    if (TempReal > 0) and (TempReal <= MaxLongInt / 100) then
      AData^.PInfo^.AnnotSettings.AnnotFont.Height := Trunc(TempReal * 100)
  end;
    // AnnotationAngle
  if ReadDDECommandReal(DDELine, 4, 0, WgBlockStart, WgBlockEnd, WgBlockEnd,
    TempReal, StrFound, MoreStrs) then
  begin
      {AData^.PInfo^.AnnotSettings.AnnotFont.Angle:=TempReal;}
  end;
    // AnnotationColor
  if ReadDDECommandLongInt(DDELine, 5, 0, WgBlockStart, WgBlockEnd,
    WgBlockEnd, TempInt, StrFound, MoreStrs) then
  begin
    if (TempInt >= 0) and (TempInt <= $FFFFFF) then
      AData^.PInfo^.AnnotSettings.Color := TempInt;
  end;
    // AnnotationPos
  if ReadDDECommandLongInt(DDELine, 6, 0, WgBlockStart, WgBlockEnd,
    WgBlockEnd, TempInt, StrFound, MoreStrs) then
  begin
    if (TempInt >= 1) and (TempInt <= 9) then
      AData^.PInfo^.AnnotSettings.Position := TempInt;
  end;
    // AnnotationLayer
  if (DDEHandler.FLayerInfo = 0) or (DDEHandler.FLayerInfo = 1) then
  begin
    if ReadDDECommandString(DDELine, 7, 0, WgBlockStart, WgBlockEnd,
      WgBlockEnd, TempStr, StrToLong, MoreStrs) then
    begin
      if TempStr <> '' then
      begin
        ALayer := AData^.Layers^.NameToLayer(TempStr);
        if ALayer <> nil then
          AData^.PInfo^.AnnotSettings.AnnotLayer := ALayer^.Index;
      end;
    end;
  end
  else
    if (DDEHandler.FLayerInfo = 2) then
    begin
      if ReadDDECommandLongInt(DDELine, 7, 0, WgBlockStart, WgBlockEnd,
        WgBlockEnd, TempInt, StrFound, MoreStrs) then
      begin
        if TempInt >= 0 then
        begin
          ALayer := AData^.Layers^.IndexToLayer(TempInt);
          if ALayer <> nil then
            AData^.PInfo^.AnnotSettings.AnnotLayer := ALayer^.Index;
        end;
      end;
    end;
end;

procedure ProcGetNextObjIDs
  (
  AData: PProj
  );
var
  DDEStr: array[0..255] of Char;
  TempStr: array[0..12] of Char;
begin
  StrCopy(DDEStr, '[NON][');
  Str(PObjects(AData^.PInfo^.Objects)^.LastPixel + 1: 10, TempStr);
  StrCat(DDEStr, TempStr);
  StrCat(DDEStr, StrPCopy(TempStr, DDEHandler.FOutSepSign));
  Str(PObjects(AData^.PInfo^.Objects)^.LastPoly + 1: 10, TempStr);
  StrCat(DDEStr, TempStr);
  StrCat(DDEStr, StrPCopy(TempStr, DDEHandler.FOutSepSign));
  Str(PObjects(AData^.PInfo^.Objects)^.LastCPoly + 1: 10, TempStr);
  StrCat(DDEStr, TempStr);
  StrCat(DDEStr, StrPCopy(TempStr, DDEHandler.FOutSepSign));
  Str(PObjects(AData^.PInfo^.Objects)^.LastCircle + 1: 10, TempStr);
  StrCat(DDEStr, TempStr);
  StrCat(DDEStr, StrPCopy(TempStr, DDEHandler.FOutSepSign));
  Str(PObjects(AData^.PInfo^.Objects)^.LastArc + 1: 10, TempStr);
  StrCat(DDEStr, TempStr);
  StrCat(DDEStr, StrPCopy(TempStr, DDEHandler.FOutSepSign));
  Str(PObjects(AData^.PInfo^.Objects)^.LastSymbol + 1: 10, TempStr);
  StrCat(DDEStr, TempStr);
  StrCat(DDEStr, StrPCopy(TempStr, DDEHandler.FOutSepSign));
  Str(PObjects(AData^.PInfo^.Objects)^.LastImage + 1: 10, TempStr);
  StrCat(DDEStr, TempStr);
  StrCat(DDEStr, StrPCopy(TempStr, DDEHandler.FOutSepSign));
  Str(PObjects(AData^.PInfo^.Objects)^.LastText + 1: 10, TempStr);
  StrCat(DDEStr, TempStr);
  StrCat(DDEStr, StrPCopy(TempStr, DDEHandler.FOutSepSign));
  Str(PObjects(AData^.PInfo^.Objects)^.LastRText + 1: 10, TempStr);
  StrCat(DDEStr, TempStr);
  StrCat(DDEStr, StrPCopy(TempStr, DDEHandler.FOutSepSign));
  Str(PObjects(AData^.PInfo^.Objects)^.LastBusGraph + 1: 10, TempStr);
  StrCat(DDEStr, TempStr);
  StrCat(DDEStr, StrPCopy(TempStr, DDEHandler.FOutSepSign));
  Str(PObjects(AData^.PInfo^.Objects)^.LastSpline + 1: 10, TempStr);
  StrCat(DDEStr, TempStr);
  StrCat(DDEStr, StrPCopy(TempStr, DDEHandler.FOutSepSign));
  Str(PObjects(AData^.PInfo^.Objects)^.LastMesLine + 1: 10, TempStr);
  StrCat(DDEStr, TempStr);
  StrCat(DDEStr, StrPCopy(TempStr, DDEHandler.FOutSepSign));
  Str(PObjects(AData^.PInfo^.Objects)^.LastOLEObj + 1: 10, TempStr);
  StrCat(DDEStr, TempStr);
  StrCat(DDEStr, ']');
  DDEHandler.SendString(StrPas(DDEStr));
  DDEHandler.SetAnswerMode(FALSE);
end;

{++ Sygsky 30-SEP-1999 BUG#186 BUILD #93: Full proc inserted }

function FuncChangeEllipse
  (
  Item: PIndex;
  AData: PProj;
  NewPos: TDPoint;
  NewRad: Integer
  ): Boolean;
var
  Redraw: TDRect;
  AEllipse: PEllipse;
  DDEStr: string;
  MoveStr: string;
  OldRad: Integer;
  XMove: Integer;
  YMove: Integer;
  OldPos: TDPoint;
begin
  Result := False;
  if AData^.Layers^.IsObjectFixed(Item) then
  begin
    MsgBox(AData^.Parent.Handle, 40147, mb_IconExclamation + mb_Ok, '');
    Exit;
  end;
  {
  For_future:
  if not Checked_new_values_to_be_legal then Exit_with_info_msg;
  }
  AEllipse := PEllipse(AData^.Layers^.IndexObject(AData^.PInfo, Item));
  with AEllipse^ do
  begin // Damned Delphi, she forced me to do so. Why?
    OldPos.X := Position.X;
    OldPos.Y := Position.Y;
    OldRad := AEllipse^.GiveRadius;
  end;
  XMove := NewPos.X - OldPos.X;
  YMove := NewPos.Y - OldPos.Y;
  if (XMove = 0) and (YMove = 0) and (OldRad = NewRad) then
    Exit; // No changed occured
  AData^.SetCursor(crHourGlass);
  Redraw.Init;
  Redraw.CorrectByRect(AEllipse^.ClipRect); // Add old rect to redraw
  with AEllipse^ do
  begin // Change values
    PrimaryAxis := NewRad;
    SecondaryAxis := NewRad;
    Position.X := NewPos.X;
    Position.Y := NewPos.Y;
    CalculateClipRect; // Calc. new one
  end;
  Result := True;
  AData^.SetModified; // Mark project to be modified
  AData^.UpdateClipRect(AEllipse); // Why I need it ? nicht fershteen...
  Item^.ClipRect.InitByRect(AEllipse^.ClipRect);
  Redraw.CorrectByRect(AEllipse^.ClipRect); // Add New rect to redraw area
  { Note that this func. is can be called ONLY for selected single object only }
  AData^.SelAllRect.CorrectByRect(AEllipse^.ClipRect); // Add this to slctd. area
  MoveStr := MlgStringList.Strings['SelectInfo', 8]; // Find our sample
  MoveStr := Format(MoveStr, [1, FormatStr(NewPos.X, 2),
    FormatStr(NewPos.Y, 2), FormatStr(NewRad, 2)]); // Make it
  AData^.SetStatusText(MoveStr, False); // Update ststus information
  AData^.SetCursor(crDefault);
  if not AData^.CorrectSize(Redraw, TRUE) then
  begin
    AData^.PInfo^.RedrawRect(Redraw);
    AData^.PInfo^.FromGraphic := TRUE;
  end;
  UserInterface.Update([uiStatusBar], True); // For any cases
  if OldRad <> NewRad then
  begin
    AData^.SetCursor(crHourGlass);
    DDEStr := '[MOD][';
    Str((XMove / 100): 0: 2, MoveStr);
    DDEStr := DDEStr + MoveStr + DDEHandler.FOutSepSign;
    Str((YMove / 100): 0: 2, MoveStr);
    DDEStr := DDEStr + MoveStr + ']';
    DDEHandler.SendString(DDEStr);
{$IFNDEF WMLT}
    ProcDBSendCommand(AData, 'MOV');
{$ENDIF}
    Result := True;
    AData^.SetCursor(crDefault);
  end;
end;
{-- Sygsky}

procedure ProcMoveRelSelected
  (
  AData: PProj;
  XMove: LongInt;
  YMove: LongInt
  );
var
  Redraw: TDRect;
  DoIt: Boolean;
  DDEStr: string;
  MoveStr: string;
{++ IDB_UNDO UnMove}
  bUndoDataMustBeStored: Boolean;
{-- IDB_UNDO UnMove}

  procedure DoAll(Item: PIndex); far;
  var
    AView: PView;
  begin
    if not AData^.Layers^.IsObjectFixed(Item) then
    begin
      AView := AData^.Layers^.IndexObject(AData^.PInfo, Item);
      Redraw.CorrectByRect(AView^.ClipRect);
{++ IDB_UNDO UnMove}
{$IFNDEF WMLT}
        // Put object in Undo Data that are stored in the IDB. Ivanoff.
      if WinGisMainForm.WeAreUsingUndoFunction[AData] then
      begin
        WinGisMainForm.UndoRedo_Man.AddObjectInUndoData(AData, AView);
        bUndoDataMustBeStored := TRUE;
      end;
{$ENDIF}
{-- IDB_UNDO UnMove}
      AView^.MoveRel(XMove, YMove);
      AView^.UpdateData;
      if (AData^.SnapState) and (AView^.GetObjType = ot_Symbol) and (GetKeyState(VK_CONTROL) < 0) then
      begin
        ProcRotateSymbol(AData, PSymbol(AView));
      end;
//++ Glukhov Bug#468 Build#162 16.09.01
      if (AView.GetObjType = ot_Text) and (PText(AView).fAdvMode = cwTxtAdvSelPoly) then
      begin
        if Assigned(AData.Layers.SelLayer.HasObject(AView.GetObjMaster(AData.PInfo))) then
        begin
          PText(AView).TxtPolyData.hShift := PText(AView).TxtPolyData.hShift - XMove;
          PText(AView).TxtPolyData.vShift := PText(AView).TxtPolyData.vShift - YMove;
        end;
      end;
//-- Glukhov Bug#468 Build#162 16.09.01
      AData^.UpdateClipRect(AView);
      Item^.ClipRect.InitByRect(AView^.ClipRect);
      Redraw.CorrectByRect(AView^.ClipRect);
      AData^.SelAllRect.CorrectByRect(AView^.ClipRect);
    end;
  end;

  function CheckFixed(Item: PIndex)
      : Boolean; far;
  begin
    CheckFixed := AData^.Layers^.IsObjectFixed(Item);
  end;

  procedure SendAll(Item: PIndex); far;
  begin
    DDEHandler.AppendList(Item^.Index);
  end;
//++ Glukhov ObjAttachment 19.10.00
var
  AddSelList: PCollection;
//-- Glukhov ObjAttachment 19.10.00
begin
  if AData^.Layers^.SelLayer^.Data^.FirstThat(@CheckFixed) <> nil then
    DoIt := MsgBox(AData^.Parent.Handle, 10166, mb_IconQuestion + mb_YesNo + mb_DefButton2, '') = id_Yes
  else
    DoIt := TRUE;
  if DoIt then
  begin
//++ Glukhov ObjAttachment 18.10.00
    AddSelList := New(PCollection, Init(16, 16));
    FillUpSelectionByAttached(AData, AddSelList, True); // recursively
//-- Glukhov ObjAttachment 18.10.00
    if AData^.Layers^.SelLayer^.CheckMove(AData^.PInfo, XMove, YMove) then
    begin
      Redraw.Init;
      AData^.SetStatusText(GetLangText(4012), TRUE);
      AData^.SetCursor(crHourGlass);
{++ IDB_UNDO UnMove}
      bUndoDataMustBeStored := FALSE;
{-- IDB_UNDO UnMove}
      AData^.Layers^.SelLayer^.Data^.ForEach(@DoAll);
{++ IDB_UNDO UnMove}
        // Save Undo data if it is necessary. Ivanoff.
{$IFNDEF WMLT}
      if (WinGisMainForm.WeAreUsingUndoFunction[AData]) and (bUndoDataMustBeStored) then
        WinGisMainForm.UndoRedo_Man.SaveUndoData(AData, utUnMove);
{$ENDIF}
{-- IDB_UNDO UnMove}
      AData^.ClearStatusText;
      AData^.SetCursor(crDefault);
{!?!?!        Redraw.Grow(AData^.PInfo^.CalculateDraw(AData^.Layers^.Toplayer^.LineStyle shr 8));}
      if not AData^.CorrectSize(Redraw, TRUE) then
      begin
        AData^.PInfo^.RedrawRect(Redraw);
        AData^.PInfo^.FromGraphic := TRUE;
      end;
      AData^.SetCursor(crHourGlass);
      DDEStr := '[MOD][';
      Str((XMove / 100): 0: 2, MoveStr);
      DDEStr := DDEStr + MoveStr + DDEHandler.FOutSepSign;
      Str((YMove / 100): 0: 2, MoveStr);
      DDEStr := DDEStr + MoveStr + ']';
      DDEHandler.SendString(DDEStr);
{$IFNDEF WMLT}
      ProcDBSendCommand(AData, 'MOV');
{$ENDIF}
      AData^.SetCursor(crDefault);
// ++ Cadmensky
// ++ Cadmensky IDB Version 2.3.1
      if WinGisMainForm.WeAreUsingTheIDB[AData] then
// -- Cadmensky IDB Version 2.3.1
        WinGisMainForm.IDB_Man.UpdateCalcFields(AData, AData^.Layers^.TopLayer, AData^.Layers^.SelLayer^.Data^.At(0)^.Index);
// -- Cadmensky
    end
    else
      MsgBox(AData^.Parent.Handle, 4100, mb_OK or mb_IconStop, '');
//++ Glukhov ObjAttachment 18.10.00
    ClearSelectionFromAdded(AData, AddSelList);
    AddSelList.DeleteAll;
    Dispose(AddSelList);
//-- Glukhov ObjAttachment 18.10.00
  end;
end;

procedure ProcSetOptionsFromDB
  (
  AData: PProj
  );
var
  DDELine: array[0..50] of Char;
  TempStr: PChar;
  NewVal: LongInt;
  StrFound: Boolean;
  MoreStrs: Boolean;
  Cnt: Integer;

  procedure ReadDDELine
      (
      Item: PChar
      ); far;
  begin
    if StrLComp(Item, '[OES]', 5) = 0 then
    begin
      StrCopy(DDELine, Item);
      Item[0] := DDEStrUsed;
    end;
  end;
begin
  DDEHandler.DDEData^.ForEach(@ReadDDELine);
  DDEHandler.DeleteDDEData;
  ReadDDECommandPChar(DDELine, 1, 0, WgBlockStart, DDEHandler.FDDESepSign, WgBlockEnd, TempStr, MoreStrs);
  if StrComp(TempStr, '0') = 0 then
    AData^.PInfo^.CoordinateSystem.CoordinateType := ctGeodatic
  else
    AData^.PInfo^.CoordinateSystem.CoordinateType := ctCarthesian;
  StrDispose(TempStr);
  ReadDDECommandPChar(DDELine, 1, 1, WgBlockStart, DDEHandler.FDDESepSign, WgBlockEnd, TempStr, MoreStrs);
  if StrComp(TempStr, '0') = 0 then
    IniFile^.VariousSettings.ShowEdgePoints := FALSE
  else
    IniFile^.VariousSettings.ShowEdgePoints := TRUE;
  StrDispose(TempStr);
  ReadDDECommandPChar(DDELine, 1, 2, WgBlockStart, DDEHandler.FDDESepSign, WgBlockEnd, TempStr, MoreStrs);
  if StrComp(TempStr, '0') = 0 then
    IniFile^.Options.BackupFile := FALSE
  else
    IniFile^.Options.BackupFile := TRUE;
  StrDispose(TempStr);
  ReadDDECommandPChar(DDELine, 1, 3, WgBlockStart, DDEHandler.FDDESepSign, WgBlockEnd, TempStr, MoreStrs);
  if StrComp(TempStr, '0') = 0 then
    IniFile^.Options.AutoSave := FALSE
  else
    IniFile^.Options.AutoSave := TRUE;
  StrDispose(TempStr);
  ReadDDECommandLongInt(DDELine, 1, 4, WgBlockStart, DDEHandler.FDDESepSign, WgBlockEnd, NewVal, StrFound, MoreStrs);
  IniFile^.Options.SaveInterv := NewVal;
  IniFile^.SaveOptions;
  for Cnt := 0 to WinGISMainForm.MDIChildCount do
    if WinGISMainForm.MDIChildren[Cnt] is TMDIChild then
      TMDICHild(WinGISMainForm.MDIChildren[Cnt]).SetAutoSave;
  AData^.PInfo^.RedrawScreen(TRUE);
end;

procedure ProcSetBitmapOptionsFromDB
  (
  AData: PProj;
  DDELine: PChar
  );
var
  TempStr: PChar;
  NewVal: LongInt;
  StrFound: Boolean;
  MoreStrs: Boolean;
begin
  ReadDDECommandPChar(DDELine, 1, 0, WgBlockStart, DDEHandler.FDDESepSign, WgBlockEnd, TempStr, MoreStrs);
{!?!?!    if StrComp(TempStr,'0') = 0 then AData^.ShowBitmaps:=FALSE
    else AData^.ShowBitmaps:=TRUE;}
  StrDispose(TempStr);
  ReadDDECommandPChar(DDELine, 1, 1, WgBlockStart, DDEHandler.FDDESepSign, WgBlockEnd, TempStr, MoreStrs);
{!?!?!?Optionen im Project    if StrComp(TempStr,'0') = 0 then IniFile^.TransparencyType := tt_White
    else IniFile^.TransparencyType := tt_Black;}
  StrDispose(TempStr);
  ReadDDECommandLongInt(DDELine, 1, 2, WgBlockStart, DDEHandler.FDDESepSign, WgBlockEnd, NewVal, StrFound, MoreStrs);
{!?!?!?Optionen im Project    IniFile^.BitmapTransparency:=NewVal;}
  IniFile^.SaveOptions;
end;

procedure ProcSetZoomFactFromDB
  (
  AData: PProj;
  DDELine: PChar
  );
var
  NewVal: Real;
  StrOK: Boolean;
  StrFound: Boolean;
  MoreStrs: Boolean;
begin
  StrOK := ReadDDECommandReal(DDELine, 1, 0, WgBlockStart, WgBlockEnd, WgBlockEnd,
    NewVal, StrFound, MoreStrs);
  if StrOK and (NewVal >= 0) then
  begin
    IniFile^.VariousSettings.ZoomSize := NewVal;
    IniFile^.VariousSettings.ZoomSizeType := 0;
    IniFile^.SaveOptions;
    AData^.PInfo^.VariousSettings.ZoomSize := NewVal;
  end;
  if StrOK then
    if DDEHandler.DDEData^.GetCount > 0 then
      DDELine[0] := DDEStrUsed;
  DDEHandler.DeleteDDEData;
end;

procedure ProcSetDBSelLayerFromDB
  (
  AData: PProj;
  DDELine: PChar
  );
var
  TempPointer: PChar;
  Cha: Integer;
  TempVal: string[20];
begin
  TempPointer := StrPos(DDELine, ']');
  TempPointer := StrPos(TempPointer, '[');
  Cha := 1;
  while (TempPointer[Cha] <> ']') do
  begin
    TempVal[Cha] := TempPointer[Cha];
    Inc(Cha);
  end;
  SetLength(TempVal, Cha - 1);
{!?!?!?    if TempVal = '0' then AData^.DBSelMode:=FALSE
    else AData^.DBSelMode:=TRUE;
    IniFile^.WriteDBSelOpt(AData^.DBSelMode);}
end;

procedure ProcRecharge
  (
  AData: PProj;
  AParent: TWinControl
  );
var
  LoadProj: array[0..fsPathName] of Char;
  Dir: array[0..fsPathName] of Char;
  DName: array[0..fsFileName + fsExtension] of Char;
  Ext: array[0..fsExtension] of Char;
  ADir: AnsiString;
  TextObj: PText;
  AChild: TWinControl;
  Ini: TIniFile;
  s: AnsiString;
begin
  GetDir(0, ADir);
  Ini := TIniFile.Create(OSInfo.WingisIniFileName);
  WingisMainForm.CloseFileOnReload := Ini.ReadBool('Settings', 'CloseOnReload', True);
  s := Ini.ReadString('Settings', 'ReloadDirectory', '');
  Ini.Free;

  TextObj := PText(AData^.Layers^.SelLayer^.IndexObject(AData^.PInfo, AData^.Layers^.SelLayer^.Data^.At(0)));
  PStrToPChar(LoadProj, TextObj^.Text);
  FileSplit(LoadProj, Dir, DName, Ext);
  if StrComp(Dir, '') = 0 then
  begin
    if s <> '' then
      StrCopy(PChar(ADir), PChar(s));
    StrPCopy(LoadProj, ADir);
    StrCat(LoadProj, '\');
    StrCat(LoadProj, DName);
    StrCat(LoadProj, '.amp');
    if not CheckFileExist(LoadProj) then
    begin
      GetDir(0, ADir);
      StrPCopy(LoadProj, ADir);
      StrCat(LoadProj, '\');
      StrCat(LoadProj, DName);
      StrCat(LoadProj, '.amp');
    end;
  end
  else
  begin
    StrCopy(LoadProj, Dir);
    StrCat(LoadProj, DName);
    StrCat(LoadProj, '.amp');
  end;
  AChild := WinGISMainForm.IsFileOpened(LoadProj);
  if not CheckFileExist(LoadProj) then
    MsgBox2(AData^.Parent.Handle, 1021, mb_IconExclamation or mb_OK, StrPas(DName), StrPas(Dir))
  else
    if AChild <> nil then
    begin
      BringWindowToTop(AChild.Handle);
    end
    else
    begin
      StrCopy(WinGISMainForm.NextProj, LoadProj);
      PostMessage(WinGISMainForm.Handle, wm_Command, cm_LoadNextProj, 0);
    end;
end;

procedure ProcDBLoadNext
  (
  AData: PProj
  );
var
  LoadProj: array[0..fsPathName] of Char;
  Dir: array[0..fsPathName] of Char;
  DName: array[0..fsFileName + fsExtension] of Char;
  Ext: array[0..fsExtension] of Char;
  ADir: string;
  TextObj: PText;
begin
  TextObj := PText(AData^.Layers^.SelLayer^.IndexObject(AData^.PInfo, AData^.Layers^.SelLayer^.Data^.At(0)));
  PStrToPChar(LoadProj, TextObj^.Text);
  FileSplit(LoadProj, Dir, DName, Ext);
  if StrComp(Dir, '') = 0 then
  begin
    GetDir(0, ADir);
    StrPCopy(LoadProj, ADir);
    StrCat(LoadProj, '\');
  end
  else
    StrCopy(LoadProj, Dir);
  StrCat(LoadProj, DName);
  StrCat(LoadProj, '.amp');
  DDEHandler.SendString('[REL][' + StrPas(LoadProj) + ']')
end;

procedure ProcDBRecharge
  (
  AData: PProj
  );
var
  LoadProj: array[0..fsPathName] of Char;
  AChild: TWinControl;
  Dir: array[0..fsPathName] of Char;
  ADir: string;
  DName: array[0..fsFileName + fsExtension] of Char;
  Ext: array[0..fsExtension] of Char;
  DDELine: array[0..300] of Char;
  TempPointer: PChar;

  procedure ReadDDELine
      (
      Item: PChar
      ); far;
  begin
    if StrLComp(Item, '[REL]', 5) = 0 then
    begin
      StrCopy(DDELine, Item);
      Item[0] := DDEStrUsed;
    end;
  end;
begin
  DDEHandler.DDEData^.ForEach(@ReadDDELine);
  DDEHandler.DeleteDDEData;
  TempPointer := NextDDEPart(NextDDEPart(DDELine));
  ReadDDEText(TempPointer, ']', LoadProj); { Projektname }
  FileSplit(LoadProj, Dir, DName, Ext);
  if StrComp(Dir, '') = 0 then
  begin
    GetDir(0, ADir);
    StrPCopy(LoadProj, ADir);
    StrCat(LoadProj, '\');
  end
  else
    StrCopy(LoadProj, Dir);
  StrCat(LoadProj, DName);
  StrCat(LoadProj, '.amp');
  AChild := WinGISMainForm.IsFileOpened(LoadProj);
  if not CheckFileExist(LoadProj) then
  begin
    MsgBox2(AData^.Parent.Handle, 1021, mb_IconExclamation or mb_OK, StrPas(DName), StrPas(Dir))
  end
  else
    if AChild <> nil then
    begin
      BringWindowToTop(AChild.Handle);
    end
    else
    begin
      StrCopy(WinGISMainForm.NextProj, LoadProj);
      PostMessage(WinGISMainForm.Handle, wm_Command, cm_LoadNextProj, 0);
    end;
end;

procedure ProcDBProjSaveAs
  (
  AData: PProj;
  AParent: TWinControl
  );
var
  TempPointer: PChar;
  TmpStr: array[0..255] of Char;
  SaveProj: array[0..fsPathName] of Char;
  Dir: array[0..fsPathName] of Char;
  DName: array[0..fsFileName + fsExtension] of Char;
  Ext: array[0..fsExtension] of Char;
  QuestOver: Boolean;
  AsLTProj: Boolean;
  LTPwd: array[0..255] of Char;
begin
  TempPointer := NextDDEPart(NextDDEPart(DDEHandler.DDEData^.At(0)));
  ReadDDEText(TempPointer, DDEHandler.FDDESepSign, SaveProj); { neuer Projektname }
  ReadDDEText(TempPointer, DDEHandler.FDDESepSign, TmpStr); { mit oder ohne Sicherheitsabfrage }
  if (StrComp(TmpStr, '1') = 0) then
    QuestOver := FALSE
  else
    QuestOver := TRUE;
  ReadDDEText(TempPointer, DDEHandler.FDDESepSign, TmpStr); { als LT-Projekt }
  if (StrComp(TmpStr, '1') = 0) then
    AsLTProj := TRUE
  else
    AsLTProj := FALSE;
  ReadDDEText(TempPointer, DDEHandler.FDDESepSign, LTPwd); { evtl. Passwort }

  FileSplit(SaveProj, Dir, DName, Ext);

  if StrComp(DName, '') <> 0 then
  begin

    if StrComp(Dir, '') = 0 then
    begin
      GetCurDir(Dir, 0);
      StrCopy(SaveProj, Dir);
      StrCat(SaveProj, '\');
    end
    else
      StrCopy(SaveProj, Dir);
    StrCat(SaveProj, DName);
    StrCat(SaveProj, '.amp');

    TMDIChild(AParent).FileSaveAs2(SaveProj, AsLTProj, QuestOver);
  end
  else
  begin
    TMDIChild(AParent).FileSaveAs1;
  end;
  if DDEHandler.DDEData^.GetCount > 0 then
    PChar(DDEHandler.DDEData^.At(0))[0] := DDEStrUsed;
  DDEHandler.DeleteDDEData;
end;

procedure ProcMakeBTXFile
  (
  AData: PProj
  );
var
  BTXFile: Text;
  FName: array[0..255] of Char;
  TmpStr: string;
  Text1: string;
  Text2: string;
  FirstTop: LongInt;
  First: Boolean;

  procedure DoAll
      (
      Item: PIndex
      ); far;
  var
    VItem: PView;
  begin
    if Item^.GetObjType = ot_Text then
    begin
      VItem := AData^.Layers^.SelLayer^.IndexObject(AData^.PInfo, Item);
      if First then
      begin
        Text1 := PText(VItem)^.Text^;
        FirstTop := VItem^.ClipRect.B.Y;
      end
      else
      begin
        if VItem^.ClipRect.B.Y > FirstTop then
        begin
          Text2 := Text1;
          Text1 := PText(VItem)^.Text^;
        end
        else
          Text2 := PText(VItem)^.Text^;
      end;
    end;
    First := FALSE;
  end;
begin
  First := TRUE;
  Text1 := '';
  Text2 := '';
  AData^.Layers^.SelLayer^.Data^.ForEach(@DoAll);
  TmpStr := Text1;
  if Text2 <> '' then
    TmpStr := TmpStr + '/' + Text2;
  Inc(AData^.BTXFileName);
  Str(AData^.BTXFileName, FName);
  StrCat(FName, '.001');
  Assign(BTXFile, FName);
  Rewrite(BTXFile);
  WriteLn(BTXFile, 'PRG  DKM');
  WriteLn(BTXFile, 'BER  WIN_GIS001');
  WriteLn(BTXFile, 'DR   N');
  WriteLn(BTXFile, 'L�   N');
  WriteLn(BTXFile, 'MAX  99');
  WriteLn(BTXFile, 'KG   73212');
  WriteLn(BTXFile, 'GST  ' + TmpStr);
  WriteLn(BTXFile, 'AUS  125');
  WriteLn(BTXFile, 'MER');
  WriteLn(BTXFile, 'MY');
  WriteLn(BTXFile, 'MX');
  Close(BTXFile);
end;

procedure ProcSetTranspModeFromDB
  (
  AData: PProj
  );
var
  TmpLInt: LongInt;
  TmpStr: string;
  NTLayer: PLayer;
  SFound: Boolean;
  MStrs: Boolean;
  StrToLong: Boolean;
  MoreStrs: Boolean;
  StrOK: Boolean;
  ActionOK: Boolean;
  Cnt: Integer;
  HadDesel: Boolean;
begin
  if ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 1, 0, WgBlockStart,
    DDEHandler.FDDESepSign, WgBlockEnd, TmpLInt, SFound, MStrs) then
  begin
    HadDesel := FALSE;
    case TmpLInt of
      0:
        begin
          ActionOK := FALSE;
          Cnt := 1;
          repeat
            NTLayer := AData^.Layers^.LData^.At(Cnt);
            if NTLayer <> nil then
            begin
              if not NTLayer^.GetState(sf_LayerOff) then
              begin
                ActionOK := TRUE;
                if AData^.Layers^.TranspLayer then
                begin
                  MenuFunctions['SelectTransparent'].Checked := FALSE;
                  AData^.SetTransparentSelect(MenuFunctions['SelectTransparent'].Checked, FALSE);
                  AData^.DeselectAll(True);
                  HadDesel := TRUE;
                end;
                if NTLayer <> AData^.Layers^.TopLayer then
                begin
                  if not HadDesel then
                    AData^.DeselectAll(True);
                  AData^.Layers^.SetTopLayer(NTLayer);
                end;
              end;
            end;
            Inc(Cnt);
          until ActionOK or (Cnt = AData^.Layers^.LData^.Count - 1);
        end;
      1:
        begin
          if not AData^.Layers^.TranspLayer then
          begin
            MenuFunctions['SelectTransparent'].Checked := TRUE;
            AData^.SetTransparentSelect(MenuFunctions['SelectTransparent'].Checked, FALSE);
            AData^.DeselectAll(True);
          end;
        end;
      2:
        begin
          ActionOK := FALSE;
          if DDEHandler.FGotLInfo <> 2 then
          begin
            StrOK := ReadDDECommandString(DDEHandler.DDEData^.At(0), 1, 1, WgBlockStart,
              DDEHandler.FDDESepSign, WgBlockEnd, TmpStr, StrToLong, MoreStrs);
            if StrOK and (TmpStr <> '') then
            begin
              NTLayer := AData^.Layers^.NameToLayer(TmpStr);
              if NTLayer <> nil then
              begin
                if NTLayer^.GetState(sf_LayerOff) then
                begin
                  NTLayer^.SetState(sf_LayerOff, FALSE);
                  UpdateLayersViewsLegendsLists(AData);
                  AData^.PInfo^.RedrawScreen(TRUE);
                end;
                if AData^.Document.Loaded then
                  AData^.Document.ActualLayer := NTLayer^.Text^;
                ActionOK := TRUE;
              end;
            end;
          end
          else
          begin
            if ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 1, 1, WgBlockStart,
              DDEHandler.FDDESepSign, WgBlockEnd, TmpLInt, SFound, MStrs) then
            begin
              NTLayer := AData^.Layers^.IndexToLayer(TmpLInt);
              if NTLayer <> nil then
              begin
                if NTLayer^.GetState(sf_LayerOff) then
                begin
                  NTLayer^.SetState(sf_LayerOff, FALSE);
                  UpdateLayersViewsLegendsLists(AData);
                  AData^.PInfo^.RedrawScreen(TRUE);
                end;
                if AData^.Document.Loaded then
                  AData^.Document.ActualLayer := NTLayer^.Text^;
                ActionOK := TRUE;
              end;
            end;
          end;
          if ActionOK then
          begin
            if AData^.Layers^.TranspLayer then
            begin
              MenuFunctions['SelectTransparent'].Checked := FALSE;
              AData^.SetTransparentSelect(MenuFunctions['SelectTransparent'].Checked, FALSE);
              AData^.DeselectAll(True);
              HadDesel := TRUE;
            end;
            if NTLayer <> AData^.Layers^.TopLayer then
            begin
              if not HadDesel then
                AData^.DeselectAll(True);
              AData^.Layers^.SetTopLayer(NTLayer);
            end;
          end;
        end;
      3:
        begin
          if not AData^.Layers^.TranspLayer then
          begin
            MenuFunctions['SelectTransparent'].Checked := TRUE;
            AData^.SetTransparentSelect(MenuFunctions['SelectTransparent'].Checked, FALSE);
            IniFile^.DoOneSelect := True;
            MenuFunctions['DatabaseAutoMonitoring'].Checked := True;
            AData^.DeselectAll(True);
          end;
        end;
    end;
  end;
{$IFNDEF WMLT}
  ProcSendActualLayerToDB(AData);
{$ENDIF}
  DDEHandler.SetAnswerMode(FALSE);
  if DDEHandler.DDEData^.GetCount > 0 then
    PChar(DDEHandler.DDEData^.At(0))[0] := DDEStrUsed;
  DDEHandler.DeleteDDEData;
end;

function ProcInsertBitmap(AWindow: TMDIChild; const AFileName: string; How2Open: OpenStatus): Boolean; {++Sygsky: Omega }
type
  TTABMode = (tmNone, tmDirect, tmIndirect, tmUseIt);
var
  ABitmap: PImage;
  Show: Word;
  NewFileName: AnsiString;
  AData: PProj;
  TABFile: TTabFile;
  Rect: TRect;
  TabMode: TTABMode;
  Res: Integer;
  Coords: array[0..3] of double;
  Coords1: TRefArray;

begin
  Result := False;
  AData := AWindow.Data;
  { Check file to be TAB one }
  if CompareText(ExtractFileExt(AFileName), TabExtension) = 0 then
    TabFile := TTabFile.Create(AFileName)
  else
    TabFile := TTabFile.Create(EmptyStr);
  TabMode := tmNone;
  try
{-------------------------- TAB support detection ----------------------------}
    if TabFile.IsValid and TabFile.NameCoincides(AFileName) then { *.TAB file was selected, not *.jpg for example! }
      TabMode := tmDirect
    else { Check for consequent TAB in the directory }
    begin
      TabFile.LoadTAB(ChangeFileExt(AFileName, TabExtension));
      if TabFile.IsValid and TabFile.NameCoincides(AFileName) then { TAB exists }
        TabMode := tmIndirect;
    end;

    case TabMode of
      { Direct usage of TAB, simply load from this TAB! }
      tmDirect:
        TabMode := tmUseIt;
      { Indirect one: file selected as image, but TAB exists, ask about}
      tmIndirect:
        case How2Open of
          { Ask about from where use coordinates, from user or TAB file? }
          osCoordinates:
            begin
              TabFile.FillRefPoints(Coords1);
              if ConfirmTABUse(AWindow, Coords1, TabFile.ImageName, FALSE, TRUE) then
                TabMode := tmUseIt;
            end;
          osTransformation: ; // Use Omega tool: not use TAB in any case,
                             // use points from it except
        else
        end;
    else
      TabMode := tmNone;
    end;

    if TabMode = tmUseIt then
    begin
      Rect := TabFile.ProjPixRect;
      Coords[0] := Rect.Left;
      Coords[1] := Rect.Top;
      Coords[2] := Rect.Right;
      Coords[3] := Rect.Bottom;
      Result := ProcInsertCBitMap(AWindow, TabFile.FullImageName, Coords);
      Exit;
    end;

{--------------------- HERE insertion of a BITMAP starts ----------------------}
    if AData^.PInfo^.BitmapSettings.ShowBitmap then
      Show := so_ShowBMP
    else
      Show := 0;

    case How2Open of
//++ Glukhov LoadImageInRect BUILD#123 20.07.00
      osCoordinates: //------ load bitmap in rectangle -------
        begin
          Result := True;
          ABitmap := New(PImage, Init(AFileName, ExtractRelativePath(AData^.FName,
            AFileName), Show, AData^.PInfo^.BitmapSettings));
          ABitmap^.SetClipRect(AData^.PInfo);
          if ABitmap^.LoadImage(AData^.PInfo, bm_Display) <> 0 then
          begin
      // Set Rectangle for the loaded image
            AData^.ActualData := ABitmap;
            AData^.SetActualMenu(mn_SetImageRect);
          end
          else
            Dispose(ABitmap, Done);
        end; // of loading in rectangle

      osNone: {------ Std bitmap loading --------}
//-- Glukhov LoadImageInRect BUILD#123 20.07.00
        begin
          Result := True;
          ABitmap := New(PImage, Init(AFileName, ExtractRelativePath(AData^.FName,
            AFileName), Show, AData^.PInfo^.BitmapSettings));
          ABitmap^.SetClipRect(AData^.PInfo);
          if ABitmap^.LoadImage(AData^.PInfo, bm_Display) <> 0 then
          begin
            ABitmap^.SetClipRect(AData^.PInfo);
            if AData^.InsertObject(ABitmap, FALSE) then
            begin
              if (DDEHandler.DoAutoIns) {and (DDEHandler^.FDBAutoIn)} then
                AData^.DBEditAuto(PIndex(ABitmap));
              Inc(AData^.BitmapCount);
              UserInterface.Update([uiMenus]);
            end;
          end { of if ABitmap^.LoadImage }
          else
            Dispose(ABitmap, Done);
        end; { of 1st case, no transformion wanted }

      osTransformation: {--------------- 2DNOW! -------------}
        begin
          NewFileName := AFileName; // Only to send extension information
          try // Just in case of any error
            if StartOmega(AWindow, AFileName) then
              Result := True; // Omega
          except
            Application.MessageBox(PChar('Some error[s] occured'),
              PChar('Warning!'), MB_OK + MB_ICONEXCLAMATION);
          end;
        end; {of 2nd case with transformation }
    end; { of case}
  finally
    TabFile.Free;
  end;
end;

{++Sygsky: Omega
     User can call this functiion to embed image with coordinates
     Input:
  AWindow: MDI window of the project,
  AFileName: image file name to insert
  Coords: coordinates of bottomleft and righttop pixels of an inserting picture in X:Y form
  TabAddress: Pointer to original TAB red from disk before loading (in AM_Child I think)
}

function ProcInsertCBitMap(AWindow: TMDIChild; const AFileName: AnsiString;
  Coords: array of double; ObjAddr: PPImage = nil;
  TabAddress: PTabFile = nil): Boolean;
var
  ABitmap: PImage;
  OItem: PImage; // Bitmap with the same cliprect on the same layer
  Show: Word;
  NewFileName: AnsiString;
  AData: PProj;
  ALayer: PLayer;
  ImgR: TRect;
  ARect: TDRect;
  PixH, PixW: Integer;

  function ImageExistOnTopLayer(AItem: PIndex): Boolean;
  begin
    Result := False;
    // Compare file names (just in a rare case of wrong user manipulation with TAB files)
    OItem := PImage(ALayer.IndexObject(AData.PInfo, AItem));
    if Assigned(OItem) then // Check it now
    begin
      if AnsiCompareFileName(OItem.FileName^, AFileName) = 0 then // the same file name!
        Result := True
      else
        OItem := nil;
    end;
  end;

begin
  Result := False;
  if High(Coords) < 3 then
    Exit; { Too few params }
  AData := AWindow.Data;
  ALayer := AData.Layers.TopLayer;
  // Check if this image already on the layer with the same frame!
  OItem := nil;
  ALayer.LastObjectType(ot_Image, @ImageExistOnTopLayer); // check from the end to the start of layer
  if AData^.PInfo^.BitmapSettings.ShowBitmap then
    Show := so_ShowBMP
  else
    Show := 0;
{--------------------- HERE insertion of a BITMAP starts ----------------------}
  ABitmap := New(PImage, Init(AFileName, ExtractRelativePath(AData^.FName,
    AFileName), Show, AData^.PInfo^.BitmapSettings));
  ABitmap^.SetClipRect(AData^.PInfo);
  if ABitmap^.LoadImage(AData^.PInfo, bm_Display) <> 0 then
  begin
    with ABitmap^ do
    begin { Calculate load points from corner pixel coordinates }
      // Define pixel width and height

      PixW := Round(Abs(Coords[2] - Coords[0]) / (iInitialW - 1));
      PixH := Round(Abs(Coords[3] - Coords[1]) / (iInitialH - 1));
      ImgR := Rect(Round(Coords[0] - PixW / 2), Round(Coords[1] - PixH / 2),
        Round(Coords[2] + PixW / 2), Round(Coords[3] + PixH / 2));
      ClipRect.Assign(ImgR.Left, ImgR.Top, ImgR.Right, ImgR.Bottom);

      // Now it is time to check if the same image with the same clip rect is present already on the layer
      if Assigned(OItem) then // File name already in USE!
        if OItem.ClipRect.IsEqual(ABitMap.ClipRect) then
        begin
          if Assigned(TabAddress) then
            TabAddress.LastError := teDuplicatedOnLayer; // Show true cause of mulfunction for user
          Dispose(ABitmap, Done); // Delete now
          Exit;
        end;
      ScaledClipRect.Assign(ImgR.Left, ImgR.Top, ImgR.Right, ImgR.Bottom);
      CorrectRotRect;
    end;

    // Put image to the top layer
    Result := AData^.InsertObject(ABitmap, FALSE);
    if Result then
    begin
      if (DDEHandler.DoAutoIns) then
        AData^.DBEditAuto(PIndex(ABitmap));
      Inc(AData^.BitmapCount);
      UserInterface.Update([uiMenus]);
      if Assigned(ObjAddr) then
        ObjAddr^ := ABitMap;
    end;
  end { of if ABitmap^.LoadImage }
  else
    Dispose(ABitmap, Done);
end;
{--Sygsky}

{++ Moskaliov AXWinGIS Insert Picture Method BUILD#144 27.11.00}
{-- Moskaliov AXWinGIS Insert Picture Method BUILD#144 27.11.00}

procedure ProcInsertBitmapsFromDB(AData: PProj);
var
  SendNewIDs: Boolean;

  procedure InsertBmpsFromColl(Item: PChar); far;
  var
    FileName: array[0..255] of Char;
    ABitmap: PImage;
    Point1: TDPoint;
    Point2: TDPoint;
    BitOrFram: Word;
    TempStr: PChar;
    TempReal: Real;
    TempLInt: LongInt;
    DBID: array[0..255] of Char;
    TempString: array[0..255] of Char;
    StrFound: Boolean;
    MoreStrs: Boolean;
    AllParams: Boolean;
  begin
    AllParams := TRUE;
      //  Name and full path of the bitmap-file
    if ReadDDECommandPChar(Item, 1, 0, WgBlockStart, DDEHandler.FDDESepSign, WgBlockEnd, TempStr, MoreStrs) then
    begin
      if (TempStr <> nil) and (StrComp(TempStr, '') <> 0) and CheckFileExist(TempStr) then
        StrCopy(FileName, TempStr)
      else
        AllParams := FALSE;
      if TempStr <> nil then
        StrDispose(TempStr);
    end
    else
      AllParams := FALSE;
      // X-Coordinate of the lower left corner
    if ReadDDECommandReal(Item, 1, 1, WgBlockStart, DDEHandler.FDDESepSign, WgBlockEnd, TempReal, StrFound, MoreStrs) then
    begin
      if (TempReal * 100 >= -MaxLongInt) and (TempReal * 100 <= MaxLongInt) then
        Point1.X := Round(TempReal * 100)
      else
        AllParams := FALSE;
    end
    else
      AllParams := FALSE;
      // Y-Coordinate of the lower left corner
    if ReadDDECommandReal(Item, 1, 2, WgBlockStart, DDEHandler.FDDESepSign, WgBlockEnd, TempReal, StrFound, MoreStrs) then
    begin
      if (TempReal * 100 >= -MaxLongInt) and (TempReal * 100 <= MaxLongInt) then
        Point1.Y := Round(TempReal * 100)
      else
        AllParams := FALSE;
    end
    else
      AllParams := FALSE;
      // X-Coordinate of the upper right corner
    if ReadDDECommandReal(Item, 1, 3, WgBlockStart, DDEHandler.FDDESepSign, WgBlockEnd, TempReal, StrFound, MoreStrs) then
    begin
      if (TempReal * 100 > Point1.X) and (TempReal * 100 <= MaxLongInt) then
        Point2.X := Round(TempReal * 100)
      else
        AllParams := FALSE;
    end
    else
      AllParams := FALSE;
      // Y-Coordinate of the upper right corner
    if ReadDDECommandReal(Item, 1, 4, WgBlockStart, DDEHandler.FDDESepSign, WgBlockEnd, TempReal, StrFound, MoreStrs) then
    begin
      if (TempReal * 100 > Point1.Y) and (TempReal * 100 <= MaxLongInt) then
        Point2.Y := Round(TempReal * 100)
      else
        AllParams := FALSE;
    end
    else
      AllParams := FALSE;
      // Show bitmap or frame
    if ReadDDECommandLongInt(Item, 1, 5, WgBlockStart, DDEHandler.FDDESepSign, WgBlockEnd, TempLInt, StrFound, MoreStrs) then
    begin
      if TempLInt = 0 then
        BitOrFram := 0
      else
        BitOrFram := so_ShowBMP;
    end
    else
      BitOrFram := so_ShowBMP;
      //  DatabaseID of the new object
    if ReadDDECommandPChar(Item, 2, 0, WgBlockStart, DDEHandler.FDDESepSign, WgBlockEnd, TempStr, MoreStrs) then
    begin
      if TempStr <> nil then
      begin
        StrCopy(DBID, TempStr);
        StrDispose(TempStr);
      end
      else
        StrCopy(DBID, '');
    end
    else
      StrCopy(DBID, '');
    ABitmap := New(PImage, Init(StrPas(FileName), ExtractRelativePath(AData^.FName, FileName), BitOrFram, AData^.PInfo^.BitmapSettings));
    ABitmap^.ClipRect.Assign(Point1.X, Point1.Y, Point2.X, Point2.Y);
    ABitmap^.ScaledClipRect.Assign(Point1.X, Point1.Y, Point2.X, Point2.Y);
    if ABitmap^.LoadImage(AData^.PInfo, bm_Display) <> 0 then
    begin
      if AData^.InsertObject(ABitmap, FALSE) then
      begin {sl}
        Inc(AData^.BitmapCount);
        if StrComp(DBID, '') <> 0 then
        begin
          SendNewIDs := TRUE;
          if DDEHandler.FLayerInfo <> 0 then
          begin
            Str(AData^.Layers^.TopLayer^.Index: 10, TempString);
            AData^.InsertedObjects^.Insert(StrNew(TempString));
          end;
          AData^.InsertedObjects^.Insert(StrNew(@DBID));
          Str(ABitmap^.Index: 10, TempString);
          AData^.InsertedObjects^.Insert(StrNew(TempString));
        end;
      end;
      UserInterface.Update([uiMenus]);
    end
    else
      Dispose(ABitmap, Done);
    Item[0] := DDEStrUsed;
  end;

begin
  AData^.ChangeRedraw(CNoRedraw);
  SendNewIDs := FALSE;
  DDEHandler.DDEData^.ForEach(@InsertBmpsFromColl);
  DDEHandler.DeleteDDEData;
  if SendNewIDs then
    ProcSendNewIDsToDB(AData, 'GRD'); { Procedure in AM_ProjO }
  AData^.ChangeRedraw(COldRedraw);
  AData^.PInfo^.RedrawScreen(TRUE);
end;

procedure ProcMakeOffset(AData: PProj);
var
  i: Integer;
  ObjType: Integer;
  AItem: PIndex;
begin
  if ExecDialog(TOffsetDlg.Init(AData^.Parent, @AData^.OffsetData, TRUE)) = id_OK then
  begin
    AData^.OffsetItem := nil;
    if (AData^.Layers^.SelLayer^.Data^.GetCount > 0) then
    begin
      for i := 0 to AData^.Layers^.SelLayer^.Data^.GetCount - 1 do
      begin
        ObjType := AData^.Layers^.IndexObject(AData^.PInfo, AData^.Layers^.SelLayer^.Data^.At(i)).GetObjType;
        if (ObjType = ot_Poly) or (ObjType = ot_CPoly) then
        begin
          AData^.OffsetItem := PPoly(AData^.Layers^.IndexObject(AData^.PInfo, AData^.Layers^.SelLayer^.Data^.At(i)));
          break;
        end;
      end;
      if AData^.OffsetItem <> nil then
      begin
        AData^.InpOffset^.SetPoly(PPoly(AData^.OffsetItem));
        AData^.InpOffset^.StartIt(AData^.PInfo, AData^.CurPos, FALSE);
        AData^.InpOffset^.MouseMove(AData^.PInfo, AData^.CurPos);
        AData^.SetActualMenu(mn_OffsetOK);
      end;
    end;
    if AData^.OffsetItem = nil then
    begin
      AData^.SetActualMenu(mn_Offset);
    end;
  end;
end;

procedure ProcMakeOffset2(AData: PProj);
var
  i: Integer;
  ObjType: Integer;
  AItem: PIndex;
begin
  AData^.OffsetData.OffsetType := otPoints;
  AData^.OffsetItem := nil;
  if (AData^.Layers^.SelLayer^.Data^.GetCount > 0) then
  begin
    AData^.OffsetItem := PPoly(AData^.Layers^.IndexObject(AData^.PInfo, AData^.Layers^.SelLayer^.Data^.At(0)));
    if AData^.OffsetItem <> nil then
    begin
      AData^.InpOffset^.SetPoly(PPoly(AData^.OffsetItem));
      AData^.InpOffset^.StartIt(AData^.PInfo, AData^.CurPos, FALSE);
      AData^.InpOffset^.MouseMove(AData^.PInfo, AData^.CurPos);
      AData^.SetActualMenu(mn_OffsetOK);
    end;
  end;
  if AData^.OffsetItem = nil then
  begin
    AData^.SetActualMenu(mn_Offset);
  end;
end;

{**************************************************************************************}
{ Procedure TProj.CombineSearch                                                        }
{--------------------------------------------------------------------------------------}
{ Sucht nach zusammenh�ngenden Linien (bzw. Polys). APoly wird dem Ergebnis angeh�ngt. }
{ Ist AReverse TRUE, so werden die Punkte in umgekehrter Reihenfolge kopiert.          }
{ Anschlie�end sucht die Routine nach Polys, die innerhalb des Suchradius ihren        }
{ Anfangs- bzw. Endpunkt haben. Diese werden in eine Liste (AList) eingef�gt. Wird     }
{ nur 1 Poly gefunden, so wird es in die Liste eingef�gt und die Prozedur sucht        }
{ mit dem Endpunkt dieses Polys weiter. Werden mehrere Polys gefunden, so werden diese }
{ selektiert und der Benutzer wird zur Auswahl eines Polys aufgefordert (mu� nicht     }
{ eines der selektierten sein). Wird kein Poly gefunden, so erfolgt ebenfalls eine     }
{ Aufforderung zur Auswahl.                                                            }
{**************************************************************************************}

procedure ProcCombineSearch
  (
  AData: PProj;
  APoly: PPoly;
  AReverse: Boolean
  );
var
  GoOn: Boolean; { weitersuchen                                  }
  APoint: TDPoint; { Punkt von den aus gesucht wird                }
  AList: PCollection; { Liste der gefundenen Polys                    }
  {************************************************************************************}
  { Callback Procedure SearchPoints                                                    }
  {------------------------------------------------------------------------------------}
  { Ist Item ein Poly, so wird �berpr�ft, ob der Anfangs- oder Endpunkt im Suchrect    }
  { ist. Dann wird Item in AList eingef�gt.                                            }
  {************************************************************************************}

  procedure SearchPoints
      (
      Item: PIndex
      ); far;
  var
    AObj: PPoly;
  begin
    if (Item^.Index <> APoly^.Index) and (Item^.GetObjType = ot_Poly) then
    begin
      AObj := Pointer(AData^.Layers^.IndexObject(AData^.PInfo, Item));
      if AData^.SearchRect.PointInside(PDPoint(AObj^.Data^.At(0))^) or
        AData^.SearchRect.PointInside(PDPoint(AObj^.Data^.At(AObj^.Data^.Count - 1))^) then
        AList^.Insert(AObj);
    end;
  end;
  {************************************************************************************}
  { Callback Function HasPointAlready                                                  }
  {------------------------------------------------------------------------------------}
  { Pr�ft, ob Item in der Liste der Anfangspunkte der bereits eingef�gten Punkte ist.  }
  {************************************************************************************}

  function HasPointAlready
      (
      Item: PDPoint
      )
      : Boolean; far;
  var
    APos: TDPoint;
    AInd: Integer;
  begin
    HasPointAlready := AData^.StartPoints^.SearchPoint(AData^.SearchRect, APos, AInd);
  end;
  {************************************************************************************}
  { Procedure SetPoints                                                                }
  {------------------------------------------------------------------------------------}
  { Setzt APoint, je nach Zustand von AReverse und f�gt den jeweils anderen in die     }
  { Liste der Startpunkte ein (verhindern, da� Poly mehrfach automatisch eingebunden   }
  { wird).                                                                             }
  {************************************************************************************}

  procedure SetPoints;
  begin
    with APoly^.Data^ do
    begin
      if AReverse then
      begin
        APoint := PDPoint(At(0))^;
        AData^.StartPoints^.InsertPoint(PDPoint(At(Count - 1))^);
      end
      else
      begin
        APoint := PDPoint(At(Count - 1))^;
        AData^.StartPoints^.InsertPoint(PDPoint(At(0))^);
      end;
    end;
  end;
  {************************************************************************************}
  { Callback Procedure SelectAll                                                       }
  {------------------------------------------------------------------------------------}
  { Selektiert Item.                                                                   }
  {************************************************************************************}

  procedure SelectAll
      (
      Item: PPoly
      ); far;
  begin
    AData^.Select(Item);
  end;

begin
  APoint.Init(0, 0);
  AData^.NewPoly^.CopyPoints(AData^.PInfo, APoly, AReverse);
  AData^.SearchRect.Init;
  AList := New(PCollection, Init(5, 5));
  SetPoints;
  repeat
    GoOn := FALSE;
{++ Ivanoff BUG#57 BUILD#95
Does not use CombineRadius from the ProjectOptions (GlobalSettings etc.) dialogue.
AData^.CombineData.Distance is set to 20 when the project was initialized.}
//      AData^.SearchRect.AssignByPoint(APoint,AData^.CombineData.Distance);
    AData.SearchRect.AssignByPoint(APoint, Round(AData.PInfo.VariousSettings.CombineRadius * 100));
{-- Ivanoff}
    AData^.Layers^.TopLayer^.Data^.ForEach(@SearchPoints);
    if AList^.Count = 0 then
    begin
      AData^.SetStatusText(GetLangText(10109), FALSE);
      AData^.ActualMen := mn_CombineSel;
    end
    else
      if AList^.Count = 1 then
        with PPoly(AList^.At(0))^ do
        begin
          AReverse := not AData^.SearchRect.PointInside(PDPoint(Data^.At(0))^);
          APoly := AList^.At(0);
          AData^.NewPoly^.CopyPoints(AData^.PInfo, APoly, AReverse);
          SetPoints;
{++ Ivanoff BUG#57 BUILD#95
Does not use CombineRadius from the ProjectOptions (GlobalSettings etc.) dialogue.
AData^.CombineData.Distance is installed in 20 when the project was initialized.}
//        AData^.SearchRect.AssignByPoint(APoint,AData^.CombineData.Distance);
          AData.SearchRect.AssignByPoint(APoint, Round(AData.PInfo.VariousSettings.CombineRadius * 100));
{-- Ivanoff}
          GoOn := AData^.NewPoly^.Data^.FirstThat(@HasPointAlready) = nil;
        end
      else
      begin
        AData^.SetStatusText(GetLangText(10110), FALSE);
        AList^.ForEach(@SelectAll);
        AData^.ActualMen := mn_CombineSelOne;
      end;
    AList^.DeleteAll;
  until not GoOn;
  Dispose(AList, Done);
end;

{$IFNDEF WMLT}

procedure ProcCombine(AData: PProj);
begin
  if ExecDialog(TDCombine.Init(AData^.Parent, @AData^.CombineData,
    AData^.Layers, AData^.PInfo^.ProjStyles)) = id_OK then
    AData^.SetActualMenu(mn_Combine);
end;
{$ENDIF}

 {+++BrovaK bug 169 build 135}

procedure RedrawMeasureLine(AData: PProj);

  procedure DoAll(Item: PIndex); far;
  begin;
    if Item.GetObjType = ot_MesLine then
      PMeasureLine(Item^.Mptr)^.Invalidate(AData.PInfo);
  end;
var
  I: integer;
begin;
  for I := 1 to AData.Layers.LData.Count - 1 do
    PLayer(Adata.Layers.LData.At(I)).Data.ForEach(@Doall);
end;
 {--brovak}

procedure ProcMeasureOptions(AData: PProj);
 {+++BrovaK bug 169 build 135}
var
  OldLength: Longint;
 {--BrovaK}
begin
 {+++BrovaK bug 169 build 135}
  OldLength := AData^.PInfo^.ArrowLen;
 {--brovak}
  if ExecDialog(TMeasureDlg.Init(AData^.Parent, @AData^.MeasureData, AData^.PInfo)) = id_OK then
    AData^.PInfo^.ArrowLen := AData^.MeasureData.ArrowLen;
       {+++BrovaK bug 169 build 135}
  if AData^.PInfo^.ArrowLen <> OldLength then
    RedrawMeasureLine(AData);
       {--BrovaK}
end;

procedure ProcTrimmDlg(AData: PProj);
begin
  if ExecDialog(TTrimmDlg.Init(AData^.Parent, @AData^.TrimmOptions)) = id_OK then
    AData^.SetActualMenu(mn_LineCut);
end;

procedure ProcRasterTransform(AData: PProj);
var
  ARasterInfo: TRasterTrafoInfo;
//++ Glukhov ObjAttachment 22.11.00
//    Image        : PImage;
  Rect: TDRect;
//-- Glukhov ObjAttachment 22.11.00
  AView: PView;
begin
{++ IDB_UNDO UnTransform}
  // The image will just be transformed. We need to save current data. Ivanoff.
{$IFNDEF WMLT}
  if (WinGisMainForm.WeAreUsingUndoFunction[AData])
    and
    (WinGisMainForm.UndoRedo_Man.AddObjectInUndoData(AData, PView(AData.SnapItem))) then
    WinGisMainForm.UndoRedo_Man.SaveUndoData(AData, utUnTransform);
{$ENDIF}
{-- IDB_UNDO UnTransform}
  ARasterInfo.Layers := AData^.Layers;
  ARasterInfo.SnapItem := Pointer(AData^.SnapItem);
  ARasterInfo.RTrafoType := AData^.RTrafoType;
  ARasterInfo.RTrafoInfo := AData^.RTrafoInfo;
  ARasterInfo.PInfo := AData^.PInfo;
  ARasterInfo.RTrafoCnt := AData^.RTrafoCnt;
//++ Glukhov ObjAttachment 22.11.00
{ Old code
  if ProcRasterTransformation(ARasterInfo) then AData^.SetModified;
  Image:=ARasterInfo.Image;
  AView:=AData^.Layers^.IndexObject(AData^.PInfo,AData^.SnapItem);
  AData^.UpdateClipRect(AView);
}
// Save the old ClipRect
  Rect.InitByRect(AData.SnapItem.ClipRect);

  if ((ARasterInfo.RTrafoType = tfLinear) or (not IsVMSIImage(PImage(ARasterInfo.SnapItem)^.FileName^))) then
  begin
    if ProcRasterTransformation(ARasterInfo) then
    begin
      RedrawWithAllAttached(AData, PView(AData.SnapItem), Rect);
    end;
  end;
//-- Glukhov ObjAttachment 22.11.00
  AData^.SetActualMenu(mn_Select);
end;

procedure ProcTurboRaster;
var
  Path: array[0..512] of Char;
  Error: Integer;
  Text: string;
  BS: PChar;
begin
  GetModuleFileName(hInstance, @Path[0], 127);
  BS := StrRScan(@Path[0], '\');
  Inc(BS);
  if IniFile^.ModuleData^.Check(in_TurboRaster) then
    StrCopy(BS, 'TrbRst32.Exe') {01-009-010699 - Changed}
  else
  begin
    MessageDialog(MlgStringList.Strings['Main', 4700], mtError, [mbOK], 0); {StrCopy(BS,'TRDemo.Exe')}
    ; {01-009-010699 - Changed}
    exit;
  end;
  Error := WinExec(Path, SW_SHOWMAXIMIZED);
  if (Error < 32) then
  begin
    Str(Error, Text);
    MsgBox(WinGISMainForm.Handle, 11132, MB_OK or MB_ICONHAND, Text);
  end;
end;

procedure ProcSendProjectPart
  (
  AData: PProj
  );
var
  Rect: TDRect;
  DDEString: string;
  TmpString: string;
  LLX: Double;
  LLY: Double;
  URX: Double;
  URY: Double;
  LLString: string;
  Res: Integer;
begin
  AData^.PInfo^.GetCurrentScreen(Rect);
  DDEString := '[APP][';
  Str(Rect.A.X / 100: 12: 2, TmpString);
  DDEString := DDEString + TmpString + '][';
  Str(Rect.A.Y / 100: 12: 2, TmpString);
  DDEString := DDEString + TmpString + '][';
  Str(Rect.B.X / 100: 12: 2, TmpString);
  DDEString := DDEString + TmpString + '][';
  Str(Rect.B.Y / 100: 12: 2, TmpString);
  DDEString := DDEString + TmpString + ']';

  LLX := Rect.A.X / 100;
  LLY := Rect.A.Y / 100;
  URX := Rect.B.X / 100;
  URY := Rect.B.Y / 100;

  ProjectionTools.PInfo := AData^.PInfo;
  Res := ProjectionTools.XYToLongLat(LLX, LLY);
  Res := ProjectionTools.XYToLongLat(URX, URY);

  if Res = 0 then
  begin
    LLString := '[' + Format('%.6f', [LLX]) + ']' +
      '[' + Format('%.6f', [LLY]) + ']' +
      '[' + Format('%.6f', [URX]) + ']' +
      '[' + Format('%.6f', [URY]) + ']';

    LLString := StringReplace(LLString, ',', '.', [rfReplaceAll]);
  end
  else
  begin
    LLString := '[][][][]';
  end;

  DDEString := DDEString + LLString;
  DDEHandler.SendString(DDEString);
end;

procedure ProcZoomToProjPart
  (
  AData: PProj
  );
var
  DDELine: array[0..255] of Char;
  TempReal: Real;
  StrOK: Boolean;
  StrFound: Boolean;
  MoreStrs: Boolean;
  LLX: LongInt;
  LLY: LongInt;
  URX: LongInt;
  URY: LongInt;
  TempLInt: LongInt;
  ParamOK: Boolean;
  ZoomRect: TDRect;

  procedure ReadDDELine
      (
      Item: PChar
      ); far;
  begin
    if StrLComp(Item, '[SPP]', 5) = 0 then
    begin
      StrCopy(DDELine, Item);
      Item[0] := DDEStrUsed;
    end;
  end;

  procedure SendPartError;
  var
    DDEString: string;
    TmpString: string;
  begin
    DDEString := '[FPP][';
    Str(AData^.Size.A.X / 100: 12: 2, TmpString);
    DDEString := DDEString + TmpString + '][';
    Str(AData^.Size.A.Y / 100: 12: 2, TmpString);
    DDEString := DDEString + TmpString + '][';
    Str(AData^.Size.B.X / 100: 12: 2, TmpString);
    DDEString := DDEString + TmpString + '][';
    Str(AData^.Size.B.Y / 100: 12: 2, TmpString);
    DDEString := DDEString + TmpString + ']';
    DDEHandler.SendString(DDEString);
  end;
begin
  ParamOK := TRUE;
  DDEHandler.DDEData^.ForEach(@ReadDDELine);
  DDEHandler.DeleteDDEData;
  StrOK := ReadDDECommandReal(DDELine, 1, 0, WgBlockStart, DDEHandler.FDDESepSign, WgBlockEnd,
    TempReal, StrFound, MoreStrs);
  if StrOK then
  begin
    LLX := LimitToLong(TempReal * 100);
    StrOK := ReadDDECommandReal(DDELine, 2, 0, WgBlockStart, DDEHandler.FDDESepSign, WgBlockEnd,
      TempReal, StrFound, MoreStrs);
    if StrOK then
    begin
      LLY := LimitToLong(TempReal * 100);
      StrOK := ReadDDECommandReal(DDELine, 3, 0, WgBlockStart, DDEHandler.FDDESepSign, WgBlockEnd,
        TempReal, StrFound, MoreStrs);
      if StrOK then
      begin
        URX := LimitToLong(TempReal * 100);
        StrOK := ReadDDECommandReal(DDELine, 4, 0, WgBlockStart, DDEHandler.FDDESepSign, WgBlockEnd,
          TempReal, StrFound, MoreStrs);
        if StrOK then
          URY := LimitToLong(TempReal * 100)
        else
          ParamOK := FALSE;
      end
      else
        ParamOK := FALSE;
    end
    else
      ParamOK := FALSE;
  end
  else
    ParamOK := FALSE;
  if ParamOK then
  begin
    if (LLX = URX) or (LLY = URY) then
      SendPartError
    else
    begin
      if LLX > URX then
      begin
        TempLInt := LLX;
        LLX := URX;
        URX := TempLInt;
      end;
      if LLY > URY then
      begin
        TempLInt := LLY;
        LLY := URY;
        URY := TempLInt;
      end;
      ZoomRect.Init;
      ZoomRect.Assign(LLX, LLY, URX, URY);
//        AData^.Size.Intersect(ZoomRect,ZoomRect);
      with ZoomRect do
        AData^.ZoomByRect(RotRect(A.X, A.Y, XSize, YSize, 0), 0, FALSE)
    end;
  end
  else
    SendPartError;
end;

procedure ProcZoomToAllObjects
  (
  AData: PProj
  );
var
  ProjSize: TDRect;
begin
  AData^.SetCursor(crHourGlass);
  AData^.SetStatusText(GetLangText(4015), TRUE);
  ProjSize.Init;
  PLayer(AData^.PInfo^.Objects)^.GetSize(ProjSize);
  if AData^.Document.Loaded then
    ProjSize.CorrectByRect(AData^.Document.GetProj.Size);
  if ProjSize.IsEmpty then
  begin
    ProjSize.A.Init(0, 0);
    ProjSize.B.Init(siStartX, siStartY);
  end;
  AData^.SetCursor(crDefault);
  AData^.ClearStatusText;
  with ProjSize do
    AData^.ZoomByRect(RotRect(A.X, A.Y, XSize, YSize, 0), 0, TRUE);
end;

{
Procedure ProcDBSendPolyData
   (
   AData           : PProj
   );
  var APoly        : PCPoly;
      Count        : Integer;
      PolyPoint    : PDPoint;
      SendStr      : AnsiString;
      TempStr      : Array[0..255] of Char;
      MaxPoints    : Integer;
      d            : Integer;
  begin
    if (AData^.Layers^.SelLayer^.Data^.GetCount = 1) and
       ((AData^.Layers^.SelLayer^.Data^.At(0)^.GetObjType) = ot_CPoly) then begin
      APoly:=PCPoly(AData^.Layers^.IndexObject(AData^.PInfo,AData^.Layers^.SelLayer^.Data^.At(0)));
      StrCopy(PChar(SendStr),'[SPD][');
      if DDEHandler.FLayerInfo = 1 then begin
        StrPCopy(TempStr,AData^.Layers^.TopLayer^.Text^);
        StrCat(PChar(SendStr),TempStr);
        StrCat(PChar(SendStr),'][');
      end
      else if DDEHandler.FLayerInfo = 2 then begin
        Str(AData^.Layers^.TopLayer^.Index:10,TempStr);
        StrCat(PChar(SendStr),TempStr);
        StrCat(PChar(SendStr),'][');
      end
      else begin
        StrCat(PChar(SendStr),'][');
      end;
      Str(APoly^.Index:10,TempStr);
      StrCat(PChar(SendStr),TempStr);
      StrCat(PChar(SendStr),'][');
      Str(APoly^.Flaeche:0:4,TempStr);
      StrCat(PChar(SendStr),TempStr);
      StrCat(PChar(SendStr),'][');
      Str(APoly^.Data^.Count-1:5,TempStr);
      StrCat(PChar(SendStr),TempStr);
      StrCat(PChar(SendStr),'][');
      MaxPoints:=StrLen(PChar(SendStr));
      MaxPoints:=10;//Trunc((4095-StrLen(PChar(SendStr))-4)/27);
      if APoly^.Data^.Count-1 <= MaxPoints then Str(APoly^.Data^.Count-1:3,TempStr)
      else Str(MaxPoints:3,TempStr);
      StrCat(PChar(SendStr),TempStr);
      StrCat(PChar(SendStr),']');
      if APoly^.GetObjType = ot_CPoly then d:=2
      else d:=1;
      for Count:=0 to APoly^.Data^.Count-d do begin
        if (Count > 0) and (Count mod MaxPoints = 0) then begin
          DDEHandler.SendPChar(PChar(SendStr));
          StrCopy(PChar(SendStr),'[SPD][');
          if DDEHandler.FLayerInfo = 1 then begin
            StrPCopy(TempStr,AData^.Layers^.TopLayer^.Text^);
            StrCat(PChar(SendStr),TempStr);
            StrCat(PChar(SendStr),'][');
          end
          else if DDEHandler.FLayerInfo = 2 then begin
            Str(AData^.Layers^.TopLayer^.Index:10,TempStr);
            StrCat(PChar(SendStr),TempStr);
            StrCat(PChar(SendStr),'][');
          end;
          Str(APoly^.Index:10,TempStr);
          StrCat(PChar(SendStr),TempStr);
          StrCat(PChar(SendStr),'][');
          Str(APoly^.Flaeche:0:4,TempStr);
          StrCat(PChar(SendStr),TempStr);
          StrCat(PChar(SendStr),'][');
          Str(APoly^.Data^.Count-1:5,TempStr);
          StrCat(PChar(SendStr),TempStr);
          StrCat(PChar(SendStr),'][');
          if (APoly^.Data^.Count-1-Count) <= MaxPoints then Str(APoly^.Data^.Count-1-Count:3,TempStr)
          else Str(MaxPoints:3,TempStr);
          StrCat(PChar(SendStr),TempStr);
          StrCat(PChar(SendStr),']');
        end;
        PolyPoint:=(PDPoint(APoly^.Data^.At(Count)));
        Str(PolyPoint^.X/100:12:2,TempStr);
        StrCat(PChar(SendStr),'[');
        StrCat(PChar(SendStr),TempStr);
        StrCat(PChar(SendStr),',');
        Str(PolyPoint^.Y/100:12:2,TempStr);
        StrCat(PChar(SendStr),TempStr);
        StrCat(PChar(SendStr),']');
      end;
      DDEHandler.SendPChar(PChar(SendStr));
      DDEHandler.SendString('[EPD][]');
    end;
  end;
}

procedure ProcDBSendPolyData(AData: PProj; ilIndex: LongInt);
var
  APoly: PCPoly;
  Count: Integer;
  PolyPoint: PDPoint;
  SendStr: array[0..4095] of Char;
  TempStr: array[0..255] of Char;
  MaxPoints: Integer;
  iI: Integer;
  bLayerWasFound: Boolean;
  ALayer: PLayer;
  d: Integer;
begin
  APoly := PCPoly(PLayer(AData.PInfo.Objects).HasIndexObject(ilIndex));
  if APoly = nil then
    EXIT;
  if (APoly.GetObjType <> ot_CPoly) and (APoly.GetObjType <> ot_Poly) then
    EXIT;
  StrCopy(SendStr, '[SPD][');
  if DDEHandler.FLayerInfo = 1 then
  begin
    bLayerWasFound := FALSE;
    for iI := 1 to AData.Layers.LData.Count - 1 do
    begin
      ALayer := AData.Layers.LData.At(iI);
      if ALayer.HasIndexObject(ilIndex) <> nil then
      begin
        bLayerWasFound := TRUE;
        BREAK;
      end;
    end;
    if bLayerWasFound then
      StrPCopy(TempStr, ALayer.Text^)
    else
      StrPCopy(TempStr, '');
    StrCat(SendStr, TempStr);
    StrCat(SendStr, '][');
  end
  else
    if DDEHandler.FLayerInfo = 2 then
    begin
      bLayerWasFound := FALSE;
      for iI := 1 to AData.Layers.LData.Count - 1 do
      begin
        ALayer := AData.Layers.LData.At(iI);
        if ALayer.HasIndexObject(ilIndex) <> nil then
        begin
          bLayerWasFound := TRUE;
          BREAK;
        end;
      end;
      if bLayerWasFound then
        Str(ALayer.Index: 10, TempStr)
      else
        StrPCopy(TempStr, '-1');
      StrCat(SendStr, TempStr);
      StrCat(SendStr, '][');
    end
    else
    begin
      StrCat(SendStr, '][');
    end;

  if APoly^.GetObjType = ot_CPoly then
    d := 2
  else
    d := 1;

  Str(APoly^.Index: 7, TempStr); //WAI von 10 auf 7 reduzeirt
  StrCat(SendStr, TempStr);
  StrCat(SendStr, '][');
  Str(APoly^.Flaeche: 0: 2, TempStr);
  StrCat(SendStr, TempStr);
  StrCat(SendStr, '][');
  Str(Min(7, APoly^.Data^.Count - (d - 1)): 5, TempStr);
  StrCat(SendStr, TempStr);
  StrCat(SendStr, '][');
  MaxPoints := StrLen(SendStr);
  MaxPoints := 7; //WAI von 10 auf 7 reduzeirt
  StrCat(SendStr, TempStr);
  StrCat(SendStr, ']');
  for Count := 0 to APoly^.Data^.Count - d do
  begin
    if (Count > 0) and (Count mod MaxPoints = 0) then
    begin
      DDEHandler.SendString(StrPas(SendStr));
      StrCopy(SendStr, '[SPD][');
      if DDEHandler.FLayerInfo = 1 then
      begin
        if bLayerWasFound then
          StrPCopy(TempStr, ALayer.Text^)
        else
          StrPCopy(TempStr, '');
        StrCat(SendStr, TempStr);
        StrCat(SendStr, '][');
      end
      else
        if DDEHandler.FLayerInfo = 2 then
        begin
          if bLayerWasFound then
            Str(ALayer.Index: 10, TempStr)
          else
            StrPCopy(TempStr, '-1');
          StrCat(SendStr, TempStr);
          StrCat(SendStr, '][');
        end
        else
        begin
          StrCat(SendStr, '][');
        end;
      Str(APoly^.Index: 10, TempStr);
      StrCat(SendStr, TempStr);
      StrCat(SendStr, '][');
      Str(APoly^.Flaeche: 0: 4, TempStr);
      StrCat(SendStr, TempStr);
      StrCat(SendStr, '][');
      Str(Min(7, APoly^.Data^.Count - (d - 1) - Count): 5, TempStr); //WAI von 10 auf 7 reduzeirt
      StrCat(SendStr, TempStr);
      StrCat(SendStr, '][');
      StrCat(SendStr, TempStr);
      StrCat(SendStr, ']');
    end;
    PolyPoint := (PDPoint(APoly^.Data^.At(Count)));
    Str(PolyPoint^.X / 100: 12: 2, TempStr);
    StrCat(SendStr, '[');
    StrCat(SendStr, TempStr);
    StrCat(SendStr, PChar(AnsiString(IniFile^.DDESeparateSign)));
    Str(PolyPoint^.Y / 100: 12: 2, TempStr);
    StrCat(SendStr, TempStr);
    StrCat(SendStr, ']');
  end;
  DDEHandler.SendString(StrPas(SendStr));
  DDEHandler.SendString('[EPD][]');
end;

procedure ProcCheckObjectExist
  (
  AData: PProj
  );
var
  NameLength: Integer;
  FirstLine: Boolean;
  LastLName: string;
  LastLIndex: LongInt;

  procedure SearchObjectsFromDDELine
      (
      Item: PChar
      ); far;
  var
    Numbers: array[0..4096] of Byte;
    Cnt: Integer;
    ItemIndex: Longint;
    Pos: Integer;
    WorkStr: string;
    Entries: Integer;
    Error: Integer;
    SItem: PIndex;
    i: Integer;
    ExistObject: PIndex;
    OldTop: PLayer;
    OldRealTop: PLayer;
    SearchAllLay: Boolean;
    ResStr: string;
    ResLInt: LongInt;
    SFound: Boolean;
    MStrs: Boolean;
    StrToLong: Boolean;
    MoreStrs: Boolean;
    StrOK: Boolean;
  begin
    StrCopy(@Numbers, Item);
    OldRealTop := AData^.Layers^.TopLayer;
    if DDEHandler.FGotLInfo = 0 then
    begin
      SearchAllLay := TRUE;
      WorkStr[0] := #3;
      StrLCopy(@WorkStr[1], @Numbers[6], 3);
      Val(WorkStr, Entries, Error);
      Pos := 11;
    end
    else
      if DDEHandler.FGotLInfo = 1 then
      begin
        StrOK := ReadDDECommandString(Item, 1, 0, WgBlockStart, WgBlockEnd, WgBlockEnd, ResStr, StrToLong, MoreStrs);
        if StrOK and (ResStr <> '') then
        begin
          SearchAllLay := FALSE;
          AData^.Layers^.TopLayer := AData^.Layers^.NameToLayer(ResStr);
        end
        else
          SearchAllLay := TRUE;
        NameLength := Length(ResStr);
        Pos := GetSepPos(Item, ']', 2, 0);
        WorkStr[0] := #3;
        StrLCopy(@WorkStr[1], @Numbers[Pos], 3);
        Val(WorkStr, Entries, Error);
        Pos := Pos + 5;
        if FirstLine or (LastLName <> ResStr) then
        begin
          if not FirstLine then
            DDEHandler.EndList1(FALSE, NameLength);
          DDEHandler.StartList('[ONE][' + ResStr + '][   ]');
        end;
        LastLName := ResStr;
      end
      else
        if DDEHandler.FGotLInfo = 2 then
        begin
          if ReadDDECommandLongInt(Item, 1, 0, WgBlockStart, WgBlockEnd, WgBlockEnd, ResLInt, SFound, MStrs) then
          begin
            SearchAllLay := FALSE;
            AData^.Layers^.TopLayer := AData^.Layers^.IndexToLayer(ResLInt);
          end
          else
          begin
            ResLInt := -1;
            SearchAllLay := TRUE;
          end;
          Pos := GetSepPos(Item, ']', 2, 0);
          WorkStr[0] := #3;
          StrLCopy(@WorkStr[1], @Numbers[Pos], 3);
          Val(WorkStr, Entries, Error);
          Pos := Pos + 5;
          if FirstLine or (LastLIndex <> ResLInt) then
          begin
            if not FirstLine then
              DDEHandler.EndList1(FALSE, NameLength);
            if ResLInt >= 0 then
              Str(ResLInt: 10, ResStr)
            else
              ResStr := '          ';
            NameLength := 10;
            DDEHandler.StartList('[ONE][' + ResStr + '][   ]');
          end;
          LastLIndex := ResLInt;
        end;
    SItem := New(PIndex, Init(0));
    WorkStr[0] := #10;
    for Cnt := 1 to Entries do
    begin
      StrLCopy(@WorkStr[1], @Numbers[Pos], 10);
      Val(WorkStr, ItemIndex, Error);
      SItem^.Index := ItemIndex;
      OldTop := AData^.Layers^.TopLayer;
      ExistObject := AData^.Layers^.TopLayer^.HasObject(SItem);
      if (ExistObject = nil) and SearchAllLay then
      begin {>>>}
        i := 0;
        while (ExistObject = nil) and (i < AData^.Layers^.LData^.Count) do
        begin
          AData^.Layers^.TopLayer := AData^.Layers^.LData^.At(i); { ausklammern, wenn nur am obersten }
          ExistObject := AData^.Layers^.TopLayer^.HasObject(SItem); { Layer gesucht werden soll }
          i := i + 1;
        end;
      end; {<<<}
      if ExistObject = nil then
      begin
        if DDEHandler.FLayerInfo = 0 then
          DDEHandler.AppendList(ItemIndex)
        else
          DDEHandler.AppendList1(ItemIndex, NameLength);
      end;
      AData^.Layers^.TopLayer := OldTop;
      Inc(Pos, 12);
    end;
    Dispose(SItem, Done);
    AData^.Layers^.TopLayer := OldRealTop;
    FirstLine := FALSE;
    Item[0] := DDEStrUsed;
  end;
begin
  AData^.SetCursor(crHourGlass);
  AData^.SetStatusText(GetLangText(4023), TRUE);
  FirstLine := TRUE;
  LastLName := '';
  LastLIndex := -1;
  if DDEHandler.FLayerInfo = 0 then
    DDEHandler.StartList('[ONE][   ]');
  DDEHandler.DDEData^.ForEach(@SearchObjectsFromDDELine);
  if DDEHandler.FLayerInfo = 0 then
    DDEHandler.EndList(TRUE)
  else
    DDEHandler.EndList1(TRUE, NameLength);
  DDEHandler.DeleteDDEData;
  AData^.ClearStatusText;
  AData^.SetCursor(crDefault);
  DDEHandler.SetAnswerMode(FALSE);
end;

procedure ProcDBSetTranspSelLayers
  (
  AData: PProj
  );
var
  DDEString: PChar;
  ResLInt: LongInt;
  ResString: string;
  StrOK: Boolean;
  SFound: Boolean;
  MoreStrs: Boolean;
  StrToLong: Boolean;
  LCount: Integer;
  ALayer: PLayer;

  procedure EnableAllLayers
      (
      AEnable: Boolean
      );

    procedure EnDisable
        (
        Item: PLayer
        ); far;
    begin
      if Item^.Index <> 0 then
        Item^.SetState(sf_DoSelect, AEnable);
    end;
  begin
    AData^.Layers^.LData^.ForEach(@EnDisable);
  end;
begin
  DDEString := StrNew(PChar(DDEHandler.DDEData^.At(0)));
  StrOK := ReadDDECommandLongInt(DDEString, 1, 0, WgBlockStart, DDEHandler.FDDESepSign,
    WgBlockEnd, ResLInt, SFound, MoreStrs);
  if SFound and StrOK then
  begin
    if ResLInt = 0 then
      EnableAllLayers(FALSE)
    else
      EnableAllLayers(TRUE);
  end;

  LCount := 2;
  repeat
    ALayer := nil;
    if DDEHandler.FGotLInfo <> 2 then
    begin
      StrOK := ReadDDECommandString(DDEString, LCount, 0, WgBlockStart, DDEHandler.FDDESepSign,
        WgBlockEnd, ResString, StrToLong, MoreStrs);
      if StrOK then
      begin
        SFound := TRUE;
        if (ResString <> '') then
          ALayer := AData^.Layers^.NameToLayer(ResString);
      end
      else
        SFound := FALSE;
    end
    else
    begin
      if ReadDDECommandLongInt(DDEString, LCount, 0, WgBlockStart, DDEHandler.FDDESepSign,
        WgBlockEnd, ResLInt, SFound, MoreStrs) then
      begin
        ALayer := AData^.Layers^.IndexToLayer(ResLInt);
      end;
    end;
    if ALayer <> nil then
    begin
      StrOK := ReadDDECommandLongInt(DDEString, LCount, 1, WgBlockStart, DDEHandler.FDDESepSign,
        WgBlockEnd, ResLInt, SFound, MoreStrs);
      if StrOK then
      begin
        if ResLInt = 1 then
          ALayer^.SetState(sf_DoSelect, TRUE)
        else
          if ResLInt = 0 then
            ALayer^.SetState(sf_DoSelect, FALSE);
      end;
    end;
    Inc(LCount);
  until not SFound;
  StrDispose(DDEString);
  if DDEHandler.DDEData^.GetCount > 0 then
    PChar(DDEHandler.DDEData^.At(0))[0] := DDEStrUsed;
  DDEHandler.DeleteDDEData;
end;

procedure ProcCopyObjectFromDB
  (
  AData: PProj;
  AnswerStr: string
  );
var
  OldTop: PLayer;
  ResStr: string;
  ResLInt: LongInt;
  SourceLayer: PLayer;
  DestLayer: PLayer;
  SearchOnAllL: Boolean;
  CopyObjects: Boolean;
  ParamPos: Integer;
  ObjCount: Integer;
  Entries: Integer;
  SItem: PIndex;
  SObj: PView;
  NObj: PView;
  Inserted: Boolean;
  IDStr: string;
  IDPChar: array[0..15] of Char;
  NewID: LongInt;
  SFound: Boolean;
  MStrs: Boolean;
  StrToLong: Boolean;
  MoreStrs: Boolean;
  StrOK: Boolean;
begin
  CopyObjects := TRUE;
  SearchOnAllL := TRUE;
  OldTop := AData^.Layers^.TopLayer;
  if DDEHandler.FLayerInfo = 0 then
  begin
    ParamPos := 2;
    StrOK := ReadDDECommandString(DDEHandler.DDEData^.At(0), 1, 0, WgBlockStart,
      WgBlockEnd, WgBlockEnd, ResStr, StrToLong, MoreStrs);
    if StrOK and (ResStr <> '') then
    begin
      DestLayer := AData^.Layers^.NameToLayer(ResStr);
      if DestLayer = nil then
        CopyObjects := FALSE
      else
        CopyObjects := TRUE;
    end
    else
      CopyObjects := FALSE;
  end
  else
    if DDEHandler.FLayerInfo = 1 then
    begin
      ParamPos := 3;
      StrOK := ReadDDECommandString(DDEHandler.DDEData^.At(0), 1, 0, WgBlockStart,
        WgBlockEnd, WgBlockEnd, ResStr, StrToLong, MoreStrs);
      if StrOK and (ResStr <> '') then
      begin
        SourceLayer := AData^.Layers^.NameToLayer(ResStr);
        if SourceLayer = nil then
          CopyObjects := FALSE
        else
          CopyObjects := TRUE;
        SearchOnAllL := FALSE;
      end
      else
        SearchOnAllL := TRUE;
      StrOK := ReadDDECommandString(DDEHandler.DDEData^.At(0), 2, 0, WgBlockStart,
        WgBlockEnd, WgBlockEnd, ResStr, StrToLong, MoreStrs);
      if StrOK and (ResStr <> '') then
      begin
        DestLayer := AData^.Layers^.NameToLayer(ResStr);
        if DestLayer = nil then
          CopyObjects := FALSE
        else
          CopyObjects := TRUE;
      end
      else
        CopyObjects := FALSE;
    end
    else
      if DDEHandler.FLayerInfo = 2 then
      begin
        ParamPos := 3;
        if ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 1, 0, WgBlockStart,
          WgBlockEnd, WgBlockEnd, ResLInt, SFound, MStrs) then
        begin
          SourceLayer := AData^.Layers^.IndexToLayer(ResLInt);
          if SourceLayer = nil then
            CopyObjects := FALSE
          else
            CopyObjects := TRUE;
          SearchOnAllL := FALSE;
        end
        else
          SearchOnAllL := TRUE;
        if ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 2, 0, WgBlockStart,
          WgBlockEnd, WgBlockEnd, ResLInt, SFound, MStrs) then
        begin
          DestLayer := AData^.Layers^.IndexToLayer(ResLInt);
          if DestLayer = nil then
            CopyObjects := FALSE
          else
            CopyObjects := TRUE;
        end
        else
          CopyObjects := FALSE;
      end;
  if CopyObjects then
  begin
    ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), ParamPos, 0, WgBlockStart,
      WgBlockEnd, WgBlockEnd, ResLInt, SFound, MStrs);
    if ResLInt > 0 then
    begin
      Entries := ResLInt;
      SItem := New(PIndex, Init(0));
      for ObjCount := 1 to Entries do
      begin
        if ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), ParamPos + ObjCount, 0, WgBlockStart,
          WgBlockEnd, WgBlockEnd, ResLInt, SFound, MStrs) then
        begin
          if SearchOnAllL then
          begin
            SItem^.Index := ResLInt;
            SObj := PView(AData^.Layers^.IndexObject(AData^.PInfo, SItem));
            if SObj <> nil then
            begin
              NObj := MakeObjectCopy(SObj);
              if AData^.InsertObjectOnLayer(NObj, DestLayer^.Index) then
                Inserted := TRUE;
              NewID := NObj^.Index;
              if Inserted then
              begin
                Str(SObj^.Index: 10, IDStr);
                StrPCopy(@IDPChar, IDStr);
                AData^.InsertedObjects^.Insert(StrNew(@IDPChar));
                Str(NewID: 10, IDStr);
                StrPCopy(@IDPChar, IDStr);
                AData^.InsertedObjects^.Insert(StrNew(@IDPChar));
              end;
            end;
          end
          else
          begin
              {wenn nicht auf allen Layern gesucht werden soll}
            SItem^.Index := ResLInt;
            AData^.Layers^.TopLayer := SourceLayer;
            SObj := PView(AData^.Layers^.TopLayer^.IndexObject(AData^.PInfo, SItem));
            if SObj <> nil then
            begin
              NObj := MakeObjectCopy(SObj);
              if AData^.InsertObjectOnLayer(NObj, DestLayer^.Index) then
                Inserted := TRUE;
              NewID := NObj^.Index;
              if Inserted then
              begin
                if DDEHandler.FLayerInfo <> 0 then
                begin
                  Str(DestLayer^.Index: 10, IDStr);
                  StrPCopy(@IDPChar, IDStr);
                  AData^.InsertedObjects^.Insert(StrNew(@IDPChar));
                end;
                Str(SObj^.Index: 10, IDStr);
                StrPCopy(@IDPChar, IDStr);
                AData^.InsertedObjects^.Insert(StrNew(@IDPChar));
                Str(NewID: 10, IDStr);
                StrPCopy(@IDPChar, IDStr);
                AData^.InsertedObjects^.Insert(StrNew(@IDPChar));
              end;
            end;
          end;
        end;
      end;
    end;
    Dispose(SItem, Done);
  end;
  AData^.Layers^.TopLayer := OldTop;
  ProcSendNewIDsToDB(AData, AnswerStr);
  DDEHandler.SetAnswerMode(FALSE);
  if DDEHandler.DDEData^.GetCount > 0 then
    PChar(DDEHandler.DDEData^.At(0))[0] := DDEStrUsed;
  DDEHandler.DeleteDDEData;
end;

{***************************************************************************}
{ Function FuncZoomToObject                                                 }
{---------------------------------------------------------------------------}
{ Berechnet abh�ngig von ABorder einen Projektausschnitt um ein Objekt und  }
{ zoomt zu diesem Ausschnitt. Der R�ckgabewert ist TRUE, wenn das Objekt    }
{ gefunden werden konnte.                                                   }
{***************************************************************************}

function FuncZoomToObject
  (
  AData: PProj;
  AObjIndex: LongInt
  )
  : Boolean;
var
  VItem: PView;
  IndItem: PIndex;
  ExistObject: PIndex;
  OldTop: PLayer;
  i: Integer;
  PosX1: LongInt;
  PosY1: LongInt;
  PosX2: LongInt;
  PosY2: LongInt;
  RealPosX1: Real;
  RealPosY1: Real;
  RealPosX2: Real;
  RealPosY2: Real;
  Rect: TDRect;
  XDist: Real;
  YDist: Real;
  NewDist: Real;
  RealZoom: Real;
  ZoomFact: Double;
  FrameSize: Integer;
begin
  Result := FALSE;
  IndItem := New(PIndex, Init(0));
  IndItem^.Index := AObjIndex;
  OldTop := AData^.Layers^.TopLayer;
  ExistObject := AData^.Layers^.TopLayer^.HasObject(IndItem);
  if (ExistObject = nil) then
  begin
    i := 0;
    while (ExistObject = nil) and (i < AData^.Layers^.LData^.Count) do
    begin
      AData^.Layers^.TopLayer := AData^.Layers^.LData^.At(i);
      ExistObject := AData^.Layers^.TopLayer^.HasObject(IndItem);
      i := i + 1;
    end;
  end;
  AData^.Layers^.TopLayer := OldTop;
  if ExistObject <> nil then
  begin
    VItem := AData^.Layers^.IndexObject(AData^.PInfo, ExistObject);
    if VItem <> nil then
    begin
      Result := TRUE;
      Rect.Init;
      PosX1 := VItem^.ClipRect.A.X;
      PosY1 := VItem^.ClipRect.A.Y;
      PosX2 := VItem^.ClipRect.B.X;
      PosY2 := VItem^.ClipRect.B.Y;
      if AData^.PInfo^.VariousSettings.ZoomSizeType = 0 then
      begin
        ZoomFact := AData^.PInfo^.VariousSettings.ZoomSize;
        FrameSize := 0;
      end
      else
      begin
        ZoomFact := 0;
        FrameSize := Round(AData^.PInfo^.VariousSettings.ZoomSize * 20);
      end;
      RealPosX1 := PosX1 * 1.0 - (PosX2 * 1.0 - PosX1 * 1.0) / 1.00 * ZoomFact;
      RealPosY1 := PosY1 * 1.0 - (PosY2 * 1.0 - PosY1 * 1.0) / 1.17 * ZoomFact;
      RealPosX2 := PosX2 * 1.0 + (PosX2 * 1.0 - PosX1 * 1.0) / 1.00 * ZoomFact;
      RealPosY2 := PosY2 * 1.0 + (PosY2 * 1.0 - PosY1 * 1.0) / 1.17 * ZoomFact;
      XDist := RealPosX2 - RealPosX1;
      YDist := RealPosY2 - RealPosY1;
      if XDist < YDist * 1.33 then
      begin
        NewDist := YDist * 1.33;
        RealPosX1 := RealPosX1 - (NewDist - XDist) / 2;
        RealPosX2 := RealPosX2 + (NewDist - XDist) / 2;
      end
      else
        if XDist > YDist * 1.33 then
        begin
          NewDist := XDist / 1.33;
          RealPosY1 := RealPosY1 - (NewDist - YDist) / 2;
          RealPosY2 := RealPosY2 + (NewDist - YDist) / 2;
        end;
      if RealPosX1 >= -MaxLongInt then
        Rect.A.X := Round(RealPosX1)
      else
        Rect.A.X := -MaxLongInt;
      if RealPosY1 >= -MaxLongInt then
        Rect.A.Y := Round(RealPosY1)
      else
        Rect.A.Y := -MaxLongInt;
      if RealPosX2 <= MaxLongInt then
        Rect.B.X := Round(RealPosX2)
      else
        Rect.B.X := MaxLongInt;
      if RealPosY2 <= MaxLongInt then
        Rect.B.Y := Round(RealPosY2)
      else
        Rect.B.Y := MaxLongInt;
      with Rect do
        RealZoom := AData^.CalculateZoom(RotRect(A.X, A.Y, XSize, YSize, 0), 0, TRUE);
      if RealZoom < IniFile^.MaxZoom then
      begin
        XDist := (Rect.B.X - Rect.A.X);
        YDist := (Rect.B.Y - Rect.A.Y);
        NewDist := Round(XDist / RealZoom * IniFile^.MaxZoom);
        Rect.A.X := Rect.A.X - Round((NewDist - XDist) / 2);
        Rect.B.X := Rect.B.X + Round((NewDist - XDist) / 2);
        NewDist := Round(YDist / RealZoom * IniFile^.MaxZoom);
        Rect.A.Y := Rect.A.Y - Round((NewDist - YDist) / 2);
        Rect.B.Y := Rect.B.Y + Round((NewDist - YDist) / 2);
      end;
      AData^.ZoomByRect(RotRect(Rect.A.X, Rect.A.Y, Rect.XSize, Rect.YSize, 0), FrameSize, TRUE);
      Rect.Done;
    end;
  end;
  Dispose(IndItem, Done);
end;

procedure ProcDBInsert
  (
  AData: PProj
  );
var
  Questioned: Boolean;
  DoDelete: Boolean;
  LastTopLayer: string;
  OldTopLayer: PLayer;
  FirstLine: Boolean;
  NameLength: Integer;
  FixedLList: PStrCollection;
  DidCopy: Boolean;
  FixedStr: string;

  procedure DoAll
      (
      Item: PChar
      ); far;
  begin
    if FirstLine then
      FixedStr := StrPas(Item)
    else
      FixedStr := FixedStr + ', ' + StrPas(Item);
    FirstLine := FALSE;
    StrDispose(Item);
  end;

  procedure DBInsertFromColl
      (
      Numbers: PByteArray
      ); far;
  var
    Pos: Integer;
    WorkStr: string;
    Entries: Integer;
    Error: Integer;
    Cnt: Integer;
    Item: Longint;
    SItem: PIndex;
    LayerIndex: LongInt;
    LayerName: string;
    NextTopLayer: PLayer;
    IndexStr: string;
    TmpStr: array[0..255] of Char;
    DoCopy: Boolean;
    SFound: Boolean;
    MStrs: Boolean;
    StrToLong: Boolean;
    MoreStrs: Boolean;
  begin
    DoCopy := FALSE;
    if DDEHandler.FGotLInfo = 0 then
    begin
      SetLength(WorkStr, 3);
      StrLCopy(@WorkStr[1], @Numbers^[6], 3);
      Val(WorkStr, Entries, Error);
      Pos := 11;
      DoCopy := TRUE;
    end
    else
      if DDEHandler.FGotLInfo = 1 then
      begin
        ReadDDECommandString(@Numbers^, 1, 0, WgBlockStart, WgBlockEnd, WgBlockEnd, LayerName, StrToLong, MoreStrs);
        NextTopLayer := AData^.Layers^.NameToLayer(LayerName);
        if NextTopLayer = nil then
          NextTopLayer := AData^.Layers^.TopLayer;
        if NextTopLayer^.GetState(sf_Fixed) then
          FixedLList^.Insert(StrNew(StrPCopy(TmpStr, NextTopLayer^.Text^)))
        else
        begin
          if (NextTopLayer <> AData^.Layers^.TopLayer) then
          begin
            AData^.Layers^.TopLayer := NextTopLayer;
            NameLength := Length(LayerName);
            if not FirstLine then
              DDEHandler.EndList1(FALSE, NameLength);
            DDEHandler.StartList('[DEL][' + LayerName + '][   ]');
          end
          else
          begin
            NameLength := Length(LayerName);
            if FirstLine then
              DDEHandler.StartList('[DEL][' + LayerName + '][   ]');
          end;
          SetLength(WorkStr, 3);
          StrLCopy(@WorkStr[1], @Numbers^[8 + NameLength], 3);
          Val(WorkStr, Entries, Error);
          Pos := 13 + NameLength;
          DoCopy := TRUE;
        end;
      end
      else
        if DDEHandler.FGotLInfo = 2 then
        begin
          if ReadDDECommandLongInt(@Numbers^, 1, 0, WgBlockStart, WgBlockEnd,
            WgBlockEnd, LayerIndex, SFound, MStrs) then
          begin
            NextTopLayer := AData^.Layers^.IndexToLayer(LayerIndex);
            if NextTopLayer = nil then
              NextTopLayer := AData^.Layers^.TopLayer;
            if NextTopLayer^.GetState(sf_Fixed) then
              FixedLList^.Insert(StrNew(StrPCopy(TmpStr, NextTopLayer^.Text^)))
            else
            begin
              if (NextTopLayer <> AData^.Layers^.TopLayer) then
              begin
                AData^.Layers^.TopLayer := NextTopLayer;
                Str(LayerIndex: 10, IndexStr);
                NameLength := 10;
                if not FirstLine then
                  DDEHandler.EndList1(FALSE, NameLength);
                DDEHandler.StartList('[DEL][' + IndexStr + '][   ]');
              end
              else
              begin
                Str(LayerIndex: 10, IndexStr);
                NameLength := 10;
                if FirstLine then
                  DDEHandler.StartList('[DEL][' + IndexStr + '][   ]');
              end;
              SetLength(WorkStr, 3);
              StrLCopy(@WorkStr[1], @Numbers^[18], 3);
              Val(WorkStr, Entries, Error);
              Pos := 23;
              DoCopy := TRUE;
            end;
          end;
        end;
    if DoCopy then
    begin
      SItem := New(PIndex, Init(0));
      SetLength(WorkStr, 10);
      for Cnt := 1 to Entries do
      begin
        StrLCopy(@WorkStr[1], @Numbers^[Pos], 10);
        Val(WorkStr, Item, Error);
        SItem^.Index := Item;
        if not AData^.Layers^.InsertObject(AData^.PInfo, SItem, TRUE) then
        begin
            {
            if not Questioned then begin
              WinGISMainForm.BringToFront;
              DoDelete:=MsgBox(AData^.Parent.Handle,1007,mb_YesNo or mb_IconQuestion,'')=id_Yes;
              Questioned:=TRUE;
            end;
            if DoDelete then begin
              case DDEHandler.FLayerInfo of
                0 : DDEHandler.AppendList(SItem^.Index);
                1 : DDEHandler.AppendList1(SItem^.Index,NameLength);
                2 : DDEHandler.AppendList1(SItem^.Index,NameLength);
              end;
            end;
            }
        end;
        Inc(Pos, 12);
      end;
      Dispose(SItem, Done);
      FirstLine := FALSE;
      DidCopy := TRUE;
    end;
    Numbers[0] := Byte(DDEStrUsed);
  end;
begin
  if (DDEHandler.FGotLInfo = 0) and (AData^.Layers^.TopLayer^.GetState(sf_Fixed)) then
    MsgBox(AData^.Parent.Handle, 11014, mb_Ok + mb_IconExclamation, '')
  else
  begin
    FirstLine := TRUE;
    DoDelete := FALSE;
    Questioned := FALSE;
    DidCopy := FALSE;
    FixedLList := New(PStrCollection, Init(3, 3));
    OldTopLayer := AData^.Layers^.TopLayer;
    if DDEHandler.FLayerInfo = 0 then
      DDEHandler.StartList('[DEL][   ]')
    else
      LastTopLayer := '';
    DDEHandler.DDEData^.ForEach(@DBInsertFromColl);
    if DDEHandler.FLayerInfo = 0 then
      DDEHandler.EndList(TRUE)
    else
      if DidCopy then
        DDEHandler.EndList1(TRUE, NameLength);
    if AData^.Layers^.TopLayer <> OldTopLayer then
      AData^.Layers^.TopLayer := OldTopLayer;
    if FixedLList^.Count > 0 then
    begin
      FirstLine := TRUE;
      FixedLList^.ForEach(@DoAll);
      FixedLList^.DeleteAll;
      MsgBox(AData^.Parent.Handle, 1016, mb_OK or mb_IconQuestion, FixedStr)
    end;
    Dispose(FixedLList, Done);
    AData^.SetModified;
  end;
  DDEHandler.DeleteDDEData;
  WinGISMainForm.BringToFront;
end;

function ProcDBSetGraphicMode
  (
  AData: PProj
  )
  : Boolean;
var
  TmpPointer: PChar;
  TmpString: array[0..255] of Char;
  TmpVal: Word;
  Error: Integer;
begin
  TmpPointer := NextDDEPart(NextDDEPart(DDEHandler.DDEData^.At(0)));
  ReadDDEText(TmpPointer, DDEHandler.FDDESepSign, TmpString);
  Val(TmpString, TmpVal, Error);
  case TmpVal of
    0: AData^.SetActualMenu(mn_Select);
    1: AData^.SetActualMenu(mn_Distance);
    2:
      begin
        IniFile^.DoOneSelect := TRUE;
        DDEHandler.DoOneSelect := TRUE;
        IniFile^.FrameSelect := FALSE;
        DDEHandler.FrameSelect := FALSE;
        AData^.DeselectAll(FALSE);
        MenuFunctions['DatabaseAutoMonitoring'].Checked := TRUE;
      end;
    3:
      begin
        IniFile^.DoOneSelect := FALSE;
        DDEHandler.DoOneSelect := FALSE;
        IniFile^.FrameSelect := TRUE;
        DDEHandler.FrameSelect := TRUE;
        AData^.DeselectAll(FALSE);
        MenuFunctions['DatabaseAutoMonitoring'].Checked := FALSE;
      end;
    4: AData^.SetActualMenu(mn_Pixel);
    5: AData^.SetActualMenu(mn_Poly);
    6: AData^.SetActualMenu(mn_CPoly);
    7: MenuFunctions['DrawEllipses'].Call(nil);
    8: MenuFunctions['DrawEllipseArcs'].Call(nil);
    9: AData^.SetActualMenu(mn_Symbol);
    10:
      begin
        AData^.SnapState := True;
        MenuFunctions['DrawSnapModeOnOff'].Checked := True;
      end;
    11:
      begin
        AData^.SnapState := False;
        MenuFunctions['DrawSnapModeOnOff'].Checked := False;
      end;
  end;
  ReadDDEText(TmpPointer, ']', TmpString);
  Val(TmpString, TmpVal, Error);
  if TmpVal = 1 then
    Result := TRUE
  else
    Result := FALSE;
  if DDEHandler.DDEData^.GetCount > 0 then
    PChar(DDEHandler.DDEData^.At(0))[0] := DDEStrUsed;
  DDEHandler.DeleteDDEData;
end;

//////////

procedure ProcAutoSnapFromDB(AData: PProj; ARadius: Integer; LayerName1, LayerName2: AnsiString);
var
  Layer1, Layer2: PLayer;
begin
  AData^.SnapData.Radius := ARadius;
  if ARadius = 0 then
  begin
    AData^.SnapData.LineSnap := 2;
    AData^.SnapData.Radius := 1;
  end
  else
  begin
    AData^.SnapData.LineSnap := 0;
  end;

  AData^.SnapData.SnapLayers^.DeleteAll;
  if LayerName1 <> '' then
  begin
    Layer1 := AData^.Layers^.NameToLayer(LayerName1);
    if Layer1 <> nil then
    begin
      AData^.SnapData.SnapLayers^.Insert(Pointer(Layer1^.Index));
    end
    else
    begin
      AData^.SnapData.SnapLayers^.Insert(Pointer(AData^.Layers^.TopLayer^.Index));
    end;
  end;

  AData^.SnapData.FixLayers^.DeleteAll;

  if LayerName2 <> '' then
  begin
    Layer2 := AData^.Layers^.NameToLayer(LayerName2);
    if Layer2 <> nil then
    begin
      AData^.SnapData.FixLayers^.Insert(Pointer(Layer2^.Index));
    end;
  end;

  AData^.DeselectAll(TRUE);
  DoSnap(AData^.Layers, AData, @AData^.SnapData);
  AData^.PInfo^.RedrawScreen(TRUE);
  AData^.SetModified;
end;

////////

procedure ProcAutoSnap
  (
  AData: PProj
  );
var
  Dir: array[0..fsPathName] of Char;
  DName: array[0..fsFileName + fsExtension] of Char;
  Ext: array[0..fsExtension] of Char;
  S: TOldFileStream;
  DoIt: Boolean;
  BResult: Integer;
begin
{++ BUG#316}
{    BResult:=ExecDialog(TSnapDlg.Init(AData^.Parent,@AData^.SnapData,
        @AData^.Layers,AData^.FName,AData^.PInfo));}
// The pointer to PInfo was sending in TSnapDlg.Init as the last parameter
//     instead of the pointer to the project. Ivanoff.
  BResult := ExecDialog(TSnapDlg.Init(AData^.Parent, @AData^.SnapData,
    @AData^.Layers, AData^.FName, AData));
{-- BUG#316}
  if BResult = id_OK then
  begin
    DoIt := TRUE;
    FileSplit(AData^.FName, Dir, DName, Ext);
    StrCat(DName, '.SNP');
    StrCat(Dir, DName);
    AData^.SetCursor(crHourGlass);
    AData^.SetStatusText(GetLangText(4001), TRUE);
    S := TOldFileStream.Create(Dir, stCreate);
    S.Put(AData^.Layers);
    S.Put(AData^.PInfo^.Objects);
    S.Put(AData^.PInfo^.Symbols);
    if S.Status <> stOK then
      DoIt := MsgBox(AData^.Parent.Handle, 827, mb_IconQuestion or mb_YesNo, '') = id_Yes;
    S.Free;
    AData^.ClearStatusText;
    AData^.SetCursor(crArrow);
    if DoIt then
    begin
      AData^.DeselectAll(TRUE);
      DoSnap(AData^.Layers, AData, @AData^.SnapData);
      AData^.SetModified;
    end;
  end
  else
    if BResult = id_Yes then
    begin
      AData^.PInfo^.RedrawScreen(TRUE);
      AData^.SetModified;
    end;
end;

function ProcSnapObjects
  (
  AData: PProj;
  DrawPos: TDPoint;
  Exclude: LongInt;
  ObjTypes: TObjectTypes;
  var SnapPos: TDPoint;
  var SnapObj: PIndex
  )
  : Boolean;
var
  SelectCircle: PEllipse;
  ADistance: Double;
  APoint: TPoint;

  function DoAll
      (
      Item: PLayer
      )
      : Boolean; far;
  begin
    DoAll := FALSE;
    if Item <> nil then
      DoAll := Item^.SearchForPoint(AData^.PInfo, SelectCircle,
        Exclude, ObjTypes, SnapPos, SnaPObj);
  end;

  procedure DoAllNearest
      (
      Item: PLayer
      ); far;
  begin
    if Item^.UseForSnap
      and Item^.SearchForNearestPoint(AData^.PInfo, DrawPos, SelectCircle,
      Exclude, ObjTypes, SnapPos, SnaPObj) then
      Result := TRUE;
  end;
begin
  Result := FALSE;
  SnapPos.Init(DrawPos.X, DrawPos.Y);
  with AData^ do
  begin
    SetCursor(crHourGlass);
    SetStatusText(GetLangText(4013), TRUE);
    case PInfo^.VariousSettings.SnapRadiusType of
      0: ADistance := PInfo^.CalculateDraw(LimitToLong(PInfo^.VariousSettings.SnapRadius * 10));
      1:
        begin
          APoint := Point(LimitToLong(PInfo^.VariousSettings.SnapRadius)
            , LimitToLong(PInfo^.VariousSettings.SnapRadius));
          DPToLP(PInfo^.ExtCanvas.Handle, APoint, 1);
          ADistance := PInfo^.CalculateDraw(APoint.X);
        end;
    else
      ADistance := PInfo^.VariousSettings.SnapRadius * 100.0;
    end;
    SelectCircle := New(PEllipse, Init(DrawPos, Round(ADistance), Round(ADistance), 0));
    if PInfo^.VariousSettings.SnapToNearest then
    begin
      SnapPos.Init(maxLongInt, maxLongInt);
      Layers^.LData^.ForEach(@DoAllNearest);
    end
    else
      Result := Layers^.LData^.FirstThat(@DoAll) <> nil;
    SetCursor(crDefault);
    ClearStatusText;
    Dispose(SelectCircle, Done);
  end;
end;

procedure ProcClearLayer
  (
  AData: PProj
  );
var
  LayerName: string;
  LayerIndex: LongInt;
  StrOK: Boolean;
  StrToLong: Boolean;
  StrFound: Boolean;
  MoreStrs: Boolean;
  DelLayer: PLayer;
  DelLNLength: Integer;
  IndexString: string;
  IsFixed: Boolean;
  IsHSDLayer: Boolean;
  OldCursor: Integer;
  SItem: PIndex;
  FoundObjs: Boolean;
  SendIDs: Boolean;

  procedure DelObjects
      (
      Item: PIndex
      );
  var
    SView: PView;
  begin
    FoundObjs := TRUE;
    SItem^.Index := Item^.Index;
    SView := DelLayer^.IndexObject(AData^.PInfo, SItem);
    SView^.Invalidate(AData^.PInfo);
    if SendIDs then
    begin
      if DDEHandler.FLayerInfo = 0 then
        DDEHandler.AppendList(Item^.Index)
      else
        DDEHandler.AppendList1(Item^.Index, DelLNLength);
    end;
{$IFNDEF WMLT}
    AData^.MultiMedia^.DelMediasAtDel(Item^.Index);
{$ENDIF}
    DelLayer^.DeleteIndex(AData^.PInfo, SItem);
    if AData^.Layers^.ExistsObject(SItem) = 0 then
      PObjects(AData^.PInfo^.Objects)^.DeleteIndex(AData^.PInfo, SItem);
  end;
begin
  FoundObjs := FALSE;
  if (DDEHandler.FGotLInfo = 0) or (DDEHandler.FGotLInfo = 1) then
  begin
    StrOK := ReadDDECommandString(PChar(DDEHandler.DDEData^.At(0)), 1, 0, WgBlockStart,
      WgBlockEnd, WgBlockEnd, LayerName, StrToLong, MoreStrs);
    if StrOK and (LayerName <> '') then
    begin
      DelLayer := AData^.Layers^.NameToLayer(LayerName);
      if DelLayer <> nil then
      begin
        IsFixed := DelLayer^.GetState(sf_Fixed);
        if AData^.TurboVector and AData^.Document.Loaded then
          IsHSDLayer := AData^.Document.GetLayerByName(LayerName) <> nil
        else
          IsHSDLayer := FALSE;
      end;
    end
    else
      DelLayer := nil;
  end
  else
    if DDEHandler.FGotLInfo = 2 then
    begin
      if ReadDDECommandLongInt(PChar(DDEHandler.DDEData^.At(0)), 1, 0, WgBlockStart,
        WgBlockEnd, WgBlockEnd, LayerIndex, StrFound, MoreStrs) then
      begin
        DelLayer := AData^.Layers^.IndexToLayer(LayerIndex);
        if DelLayer <> nil then
        begin
          IsFixed := DelLayer^.GetState(sf_Fixed);
          if AData^.TurboVector and AData^.Document.Loaded then
            IsHSDLayer := AData^.Document.GetLayerByName(PToStr(DelLayer^.Text)) <> nil
          else
            IsHSDLayer := FALSE;
        end;
      end
      else
        DelLayer := nil;
    end;
  if (DelLayer <> nil) and not IsFixed and not IsHSDLayer then
  begin
    if ReadDDECommandLongInt(PChar(DDEHandler.DDEData^.At(0)), 2, 0, WgBlockStart,
      WgBlockEnd, WgBlockEnd, LayerIndex, StrFound, MoreStrs) then
    begin
      if LayerIndex = 1 then
        SendIDs := TRUE
      else
        SendIDs := FALSE;
    end
    else
      SendIDs := FALSE;
    AData^.DeSelectAll(TRUE);
    AData^.SetStatusText(GetLangText(4011), TRUE);
    AData^.SetCursor(crHourGlass);
    SItem := New(PIndex, Init(0));
    if SendIDs then
    begin
      if DDEHandler.FLayerInfo = 0 then
        DDEHandler.StartList('[DEL][   ]')
      else
        if DDEHandler.FLayerInfo = 1 then
        begin
          DelLNLength := Length(LayerName);
          DDEHandler.StartList('[DEL][' + LayerName + '][   ]');
        end
        else
          if DDEHandler.FLayerInfo = 2 then
          begin
            Str(DelLayer^.Index: 10, IndexString);
            DelLNLength := 10;
            DDEHandler.StartList('[DEL][' + IndexString + '][   ]');
          end;
    end;
    if DelLayer^.Data^.GetCount > 0 then
      while DelLayer^.Data^.GetCount > 0 do
        DelObjects(DelLayer^.Data^.At(0));
    if SendIDs then
    begin
      if DDEHandler.FLayerInfo = 0 then
        DDEHandler.EndList(TRUE)
      else
        DDEHandler.EndList1(TRUE, DelLNLength);
    end;
    if FoundObjs then
    begin
      AData^.PInfo^.RedrawInvalidate;
      AData^.SetModified;
    end;
    Dispose(SItem, Done);
    AData^.ClearStatusText;
    AData^.SetCursor(crDefault);
  end;
  if DDEHandler.DDEData^.GetCount > 0 then
    PChar(DDEHandler.DDEData^.At(0))[0] := DDEStrUsed;
  DDEHandler.DeleteDDEData;
end;

procedure ProcClearLayer2
  (
  AData: PProj;
  ALayer: PLayer;
  AComMode: Boolean = False);
var
  IndexString: string;
  IsFixed: Boolean;
  IsHSDLayer: Boolean;
  OldCursor: Integer;
  SItem: PIndex;
  FoundObjs: Boolean;
  LayerName: string;
  i, n: Integer;
  AFailed: Boolean;

  procedure DelObjects
      (
      Item: PIndex
      );
  var
    SView: PView;
  begin
    SItem^.Index := Item^.Index;
    SView := ALayer^.IndexObject(AData^.PInfo, SItem);
    if SView <> nil then
    begin
      SView^.Invalidate(AData^.PInfo);
      try
        AData^.MultiMedia^.DelMediasAtDel(Item^.Index);
      except
        AFailed := True;
      end;
      try
        if ALayer^.DeleteIndex(AData^.PInfo, SItem) then
        begin
          FoundObjs := True;
          if AData^.Layers^.ExistsObject(SItem) = 0 then
          begin
            PObjects(AData^.PInfo^.Objects)^.DeleteIndex(AData^.PInfo, SItem);
          end;
        end;
      except
        AFailed := True;
      end;
    end;
  end;
begin
  FoundObjs := FALSE;

  if (AData = nil) or (ALayer = nil) then
  begin
    exit;
  end;

  IsFixed := ALayer^.GetState(sf_Fixed);
  LayerName := ALayer^.Text^;

  IsHSDLayer := (AData^.Document.Loaded) and (AData^.Document.GetLayerByName(LayerName) <> nil);

  if ((not IsFixed) and (not IsHSDLayer)) then
  begin
    AData^.DeSelectAll(TRUE);
    AData^.SetStatusText(GetLangText(4011), TRUE);
    AData^.SetCursor(crHourGlass);

    SItem := New(PIndex, Init(0));
    try
      AFailed := False;
      n := ALayer^.Data^.GetCount;
      for i := 0 to n - 1 do
      begin
        if AFailed then
        begin
          break;
        end;
        try
          DelObjects(ALayer^.Data^.At(0));
        except
          AFailed := True;
        end;
      end;
      if FoundObjs then
      begin
        AData^.SetModified;
      end;
    finally
      Dispose(SItem, Done);
    end;

    AData^.ClearStatusText;
    AData^.SetCursor(crDefault);

    if not AComMode then
    begin
      AData^.PInfo^.RedrawInvalidate;
    end;
  end;
end;

{*****************************************************************************************************************************}

//++ Glukhov Bug#84 BUILD#125 02.11.00

procedure ProcSendResultToDB(Topic: string; CmdName: string; Res: string);
var
  ResultString: string;
begin
  ResultString := '';
  if Topic <> '' then
    ResultString := ResultString + WgBlockStart + Topic + WgBlockEnd;
  if CmdName <> '' then
    ResultString := ResultString + WgBlockStart + CmdName + WgBlockEnd;
  if Res <> '' then
    ResultString := ResultString + WgBlockStart + Res + WgBlockEnd;

  DDEHandler.SetAnswerMode(True);
  DDEHandler.SendString(ResultString);
  DDEHandler.SetAnswerMode(False);
end;

procedure ProcSendIntResultToDB(Topic: string; CmdName: string; Res: Integer);
begin
  ProcSendResultToDB(Topic, CmdName, IntToStr(Res));
end;

//++ Glukhov Bug#203 Build#151 08.02.01

procedure ProcGetCoordinates(AData: PProj);
begin
  AData.RTrafoCnt := 0;
  AData.SetActualMenu(mn_GetCoordinates);
    // Delete the read DDE string
  ProcDDEDelString;
end;

procedure ProcSendCoordinates(AData: PProj; X, Y: LongInt);
var
  s: string;
begin
  s := '[SMC][' + IntToStr(X) + '][' + IntToStr(Y) + ']';
  DDEHandler.SendString(s);
end;
//-- Glukhov Bug#203 Build#151 08.02.01

//++ Glukhov Bug#84 BUILD#125 02.11.00

procedure ProcReversePolyLine(AData: PProj);
var
  Idx, Res: LongInt;
  bOk: Boolean;
  StrFound: Boolean;
  MoreStrs: Boolean;
  Item, Tmp: PIndex;
begin
  Res := 1; // Ok
  // Read the Mode parameter
  bOK := ReadDDECommandLongInt(
    DDEHandler.DDEData.At(0), 1, 0,
    WgBlockStart, DDEHandler.FDDESepSign, WgBlockEnd,
    Idx, StrFound, MoreStrs);

  if bOk then
  begin
    // Check the object index (Progis Id)
    Tmp := New(PIndex, Init(Idx));
    Item := AData.Layers.IndexObject(AData.PInfo, Tmp);
    Dispose(Tmp, Done);
    if not Assigned(Item) then
    begin
      bOK := False;
      Res := -2; // Object not found
    end;
  end
  else
    Res := -1; // Read DDE error

  if bOk then
  begin
    // Reverse the polyline or polygon
    case Item.GetObjType of
      ot_Poly:
        begin
          PPoly(Item).ReversePoly;
          AData.SetModified;
        end;
      ot_CPoly:
        begin
          PCPoly(Item).ReversePoly;
          AData.SetModified;
        end;
    else
      begin
        bOK := False;
        Res := -2; // Object is not a polyline (or polygon)
      end;
    end;
  end;

    // Delete the read DDE string
  ProcDDEDelString;
    // Send DDE result
  ProcSendIntResultToDB('', 'RPL', Res);
end;
//-- Glukhov Bug#84 BUILD#125 02.11.00

//++ Glukhov Bug#100 Build#152 05.03.01
// Deletes the 1st DDE-string (command) which was read already

procedure ProcDDEDelString;
begin
  if DDEHandler.DDEData^.GetCount > 0 then
  begin
    PChar(DDEHandler.DDEData^.At(0))[0] := DDEStrUsed;
    DDEHandler.DeleteDDEData;
  end;
end;

// Gets the layer for the DDE-command without parameter Layer

procedure ProcDDEReadLayer(AData: PProj; var Layer: PLayer; var PosShift: Integer);
var
  bOk: Boolean;
  sRes: string;
  iRes: Integer;
  bFound: Boolean;
  bMore: Boolean;
begin
  Layer := nil;
  if DDEHandler.FGotLInfo = 0 then
  begin
    // Use the top layer
    PosShift := 0;
    Layer := AData^.Layers^.TopLayer;
  end
  else
    if DDEHandler.FGotLInfo = 1 then
    begin
    // Read the Layer name
      PosShift := 1;
      bOK := ReadDDECommandString(DDEHandler.DDEData^.At(0), 1, 0, WgBlockStart,
        WgBlockEnd, WgBlockEnd, sRes, bFound, bMore);
      if bOK and (sRes <> '') then
        Layer := AData^.Layers^.NameToLayer(sRes);
    end
    else
      if DDEHandler.FGotLInfo = 2 then
      begin
    // Read the Layer index
        PosShift := 1;
        bOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 1, 0, WgBlockStart,
          WgBlockEnd, WgBlockEnd, iRes, bFound, bMore);
        if bOK then
          Layer := AData^.Layers^.IndexToLayer(iRes);
      end;
end;

// Gets the layer for the DDE-command from parameter Layer

procedure ProcDDEFormLayer(AData: PProj; sLayer: string; var Layer: PLayer);
var
  sRes: string;
  iRes: Integer;
  iTmp: Integer;
  bMore: Boolean;
begin
  Layer := nil;

  if sLayer = '' then
    Layer := AData^.Layers^.TopLayer
  else
  begin
    if DDEHandler.FGotLInfo = 2 then
    begin
        // Use the read value as the layer index
      Val(sLayer, iRes, iTmp);
      if iTmp = 0 then
      begin
        if iRes = 0 then
          Layer := AData^.Layers^.TopLayer
        else
          Layer := AData^.Layers^.IndexToLayer(iRes);
      end;
    end
      // Use the read value as the layer name
    else
      Layer := AData^.Layers^.NameToLayer(sLayer);
  end;
  if Assigned(Layer) then
    sRes := Layer^.Text^
  else
    sRes := '';

    // The layer should not be fixed
  if Assigned(Layer) and Layer^.GetState(sf_Fixed) then
  begin
    if MsgBox(AData.Parent.Handle, 3540, mb_IconQuestion or mb_YesNo, sRes) = id_Yes then
    begin
      if InputCheckPassword(AData^.Parent) then
        Layer.SetState(sf_Fixed, FALSE)
      else
      begin
        MsgBox(AData.Parent.Handle, 3541, mb_IconExclamation or mb_OK, '');
        Layer := nil;
      end;
    end
    else
      Layer := nil;
  end;

    // The layer should not be off
  if Assigned(Layer) and Layer^.GetState(sf_LayerOff) then
  begin
    if MsgBox(AData.Parent.Handle, 3544, mb_IconQuestion or mb_YesNo, sRes) = id_Yes then
      Layer.SetState(sf_LayerOff, FALSE)
    else
      Layer := nil;
  end;

    // If the layer is not defined, create it
  if not Assigned(Layer) then
    Layer := NewLayerDialog1(AData, GetLangText(10154), sRes);

    // If the layer is not defined yet, select it or cancel operation
  if Assigned(Layer) then
    bMore := False
  else
    bMore := True;

  while bMore do
  begin
    Layer := SelectLayerDialog(AData, [sloNewLayer]);
    if Assigned(Layer) then
      bMore := False
    else
    begin
      if MsgBox(AData.Parent.Handle, 11149, mb_IconExclamation or mb_RetryCancel, '') = idCancel then
        bMore := False;
    end;
  end;
end;

// Creates image via DDE

procedure ProcSetPicture(AData: PProj);
var
  Idx, Res: Integer;
  bOk: Boolean;
  OldTop: PLayer;
  Layer: PLayer;
  iShift: Integer;
  iTmp: Integer;
  rTmp: Real;
  x1, x2: LongInt;
  y1, y2: LongInt;
  sTmp, s: string;
  sLayer: string;
  wOpt: LongWord;
  bFound: Boolean;
  bMore: Boolean;
  Img: PImage;
begin
  bOk := True;
  Idx := 0;
  Res := 1;

    // Get the Layer
//    ProcDDEReadLayer( AData, Layer, iShift );
  bOK := ReadDDECommandString(DDEHandler.DDEData^.At(0), 1, 0, WgBlockStart,
    WgBlockEnd, WgBlockEnd, sLayer, bFound, bMore);
  ProcDDEFormLayer(AData, sLayer, Layer);
  iShift := 1;

    // Form Layer string
{
    case DDEHandler.FLayerInfo of
    1: sLayer:= AData^.Layers^.TopLayer^.Text^;
    2: Str( AData^.Layers^.TopLayer^.Index:10, sLayer );
    else sLayer:= '';
    end;
}
  if not Assigned(Layer) then
  begin
    sLayer := '';
    bOk := False;
  end
  else
    if DDEHandler.FGotLInfo = 2 then
      Str(Layer^.Index: 10, sLayer)
    else
      sLayer := Layer^.Text^;

    // Read the coordinates
  if bOk then
  begin
    bOK := ReadDDECommandReal(DDEHandler.DDEData^.At(0), 1 + iShift, 0,
      WgBlockStart, DDEHandler.FDDESepSign, WgBlockEnd, rTmp, bFound, bMore);
    x1 := Round(rTmp * 100.0);
  end;
  if bOk then
  begin
    bOK := ReadDDECommandReal(DDEHandler.DDEData^.At(0), 1 + iShift, 1,
      WgBlockStart, DDEHandler.FDDESepSign, WgBlockEnd, rTmp, bFound, bMore);
    y1 := Round(rTmp * 100.0);
  end;
  if bOk then
  begin
    bOK := ReadDDECommandReal(DDEHandler.DDEData^.At(0), 1 + iShift, 2,
      WgBlockStart, DDEHandler.FDDESepSign, WgBlockEnd, rTmp, bFound, bMore);
    x2 := Round(rTmp * 100.0);
  end;
  if bOk then
  begin
    bOK := ReadDDECommandReal(DDEHandler.DDEData^.At(0), 1 + iShift, 3,
      WgBlockStart, DDEHandler.FDDESepSign, WgBlockEnd, rTmp, bFound, bMore);
    y2 := Round(rTmp * 100.0);
  end;
    // Read the file name
  if bOk then
  begin
    bOK := ReadDDECommandString(DDEHandler.DDEData^.At(0), 2 + iShift, 0,
      WgBlockStart, DDEHandler.FDDESepSign, WgBlockEnd, sTmp, bFound, bMore);
  end;
    // Read the options
  if bOk then
  begin
    bOK := ReadDDECommandLongInt(DDEHandler.DDEData.At(0), 3 + iShift, 0,
      WgBlockStart, DDEHandler.FDDESepSign, WgBlockEnd, iTmp, bFound, bMore);
    wOpt := iTmp;
  end;

    // Delete the read DDE string
  ProcDDEDelString;

  if not bOk then
    Res := -1; // Invalid parameters
  if bOk and (not FileExists(sTmp)) then
    Res := -2; // Image was not found

  if bOk then
  begin
      // Create bitmap object
    Img := New(PImage,
      Init(sTmp, ExtractRelativePath(AData^.FName, sTmp), 0, AData^.PInfo^.BitmapSettings));

    if (wOpt and 1) = 0 then
      Img.Options := opt_HideFrame
    else
      Img.Options := 0;
    if (wOpt and 2) = 0 then
      Img.ShowOpt := so_ShowBMP
    else
      Img.ShowOpt := 0;
    if (wOpt and 4) = 0 then
      Img.TransparencyType := tt_White
    else
      Img.TransparencyType := tt_Black;
    Img.Transparency := (wOpt and $FF00) shr 8;
    if (Img.Transparency > 100) then
      Img.Transparency := 100;

    Img.ClipRect.A.X := min(x1, x2);
    Img.ClipRect.A.Y := min(y1, y2);
    Img.ClipRect.B.X := max(x1, x2);
    Img.ClipRect.B.Y := max(y1, y2);

      // Insert image in project
    OldTop := AData^.Layers^.TopLayer;
    if Assigned(Layer) then
      AData^.Layers^.TopLayer := Layer;
    bOk := AData.InsertObject(Img, False);
    AData^.Layers^.TopLayer := OldTop;

    Idx := Img.Index;
    if bOk then
    begin
      if (DDEHandler.DoAutoIns) then
        AData.DBEditAuto(PIndex(Img)); // The secret code that I do not understand
      Inc(AData.BitmapCount);
      Img.Invalidate(AData.PInfo);
//      UserInterface.Update([uiMenus]);
    end
    else
      Res := -3; // Image was not inserted
  end;

    // Send DDE result
  s := '[SPI][';
  s := s + sLayer + '][' + IntToStr(Idx) + '][' + IntToStr(Res) + ']';
  DDEHandler.SendString(s);
end;
{*)}
//-- Glukhov Bug#100 Build#152 05.03.01

{++ Moskaliov AXWinGIS Printing Methods BUILD#127 05.09.00}

procedure ProcPrintFromDB(AData: PProj);
var
  AMode: LongInt;
  intErrorCode: Integer;
  strFileName: string;
  intCopies: Integer;
  CollateCopies: Boolean;
  ShowProgress: Boolean;
  PrintMonitoring: Boolean;
  intValue: Integer;
  StrOK: Boolean;
  StrFound: Boolean;
  MoreStrs: Boolean;
begin // Procedure ProcPrintFromDB beginning
  strFileName := '';
  intCopies := 1;
  CollateCopies := FALSE;
  PrintMonitoring := FALSE;
  intErrorCode := 0;

   // read the Mode parameter
  StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 1, 0, WgBlockStart,
    DDEHandler.FDDESepSign, WgBlockEnd, AMode, StrFound, MoreStrs);
  StrOK := StrOK and (AMode >= 0) and (AMode <= 5); // the value is valid

  if StrOK then
    case AMode of
      2, 4:
        begin
                 // read the FileName parameter
          StrOK := ReadDDECommandString(DDEHandler.DDEData^.At(0), 2, 0, WgBlockStart,
            DDEHandler.FDDESepSign, WgBlockEnd, strFileName, StrFound, MoreStrs);
          StrOK := StrOK and (strFileName <> '');
          if StrOK then
            StrOK := StrOK and (ExtractFileName(strFileName) <> '');
          if StrOK then
            StrOK := StrOK and DirectoryExists(ExtractFilePath(strFileName));
          if not StrOK then
            intErrorCode := 2; // value of the FileName parameter is invalid
        end;
//    else the parameter FileName not used
    end
  else
    intErrorCode := 1; // value of the Mode parameter is invalid

  if StrOK then
  begin
         // read the Copies parameter
    StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 3, 0, WgBlockStart,
      DDEHandler.FDDESepSign, WgBlockEnd, intCopies, StrFound, MoreStrs);
    StrOK := StrOK and (intCopies > 0); // the value is valid
    if not StrOK then
      intErrorCode := 3; // value of the Copies parameter is invalid
  end;

  if StrOK then
  begin
         // read the CollateCopies parameter
    StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 4, 0, WgBlockStart,
      DDEHandler.FDDESepSign, WgBlockEnd, intValue, StrFound, MoreStrs);
    if StrOK then
      case intValue of
        0: CollateCopies := FALSE;
        1: CollateCopies := TRUE;
      else // value of the CollateCopies parameter is invalid
        begin
          StrOK := FALSE;
          intErrorCode := 4;
        end;
      end
    else // value of the CollateCopies parameter is invalid
      intErrorCode := 4;
  end;

  if StrOK then
  begin
         // read the ShowProgress parameter
    StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 5, 0, WgBlockStart,
      DDEHandler.FDDESepSign, WgBlockEnd, intValue, StrFound, MoreStrs);
    if StrOK then
      case intValue of
        0: ShowProgress := FALSE;
        1: ShowProgress := TRUE;
      else // value of the ShowProgress parameter is invalid
        begin
          StrOK := FALSE;
          intErrorCode := 5;
        end;
      end
    else // value of the ShowProgress parameter is invalid
      intErrorCode := 5;
  end;

  if StrOK then
  begin
         // read the PrintMonitoring parameter
    StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 6, 0, WgBlockStart,
      DDEHandler.FDDESepSign, WgBlockEnd, intValue, StrFound, MoreStrs);
    if StrOK then
      case intValue of
        0: PrintMonitoring := FALSE;
        1: PrintMonitoring := TRUE;
      else // value of the PrintMonitoring parameter is invalid
        begin
          StrOK := FALSE;
          intErrorCode := 6;
        end;
      end
    else // value of the PrintMonitoring parameter is invalid
      intErrorCode := 6;
  end;

  if StrOK then
  begin
    StrOK := WPrinters.Printers.Count > 0;
    if not StrOK then
      intErrorCode := 17; // there is no printers istalled in system
  end;

  if DDEHandler.DDEData^.GetCount > 0 then
    PChar(DDEHandler.DDEData^.At(0))[0] := DDEStrUsed;
  DDEHandler.DeleteDDEData;

  ProcSendPrintResultToDB('PRN', intErrorCode);

  if StrOK then
    intErrorCode := FuncPrintFromDB(AMode, strFileName, intCopies, CollateCopies,
      ShowProgress, PrintMonitoring, AData);
end; // End of the procedure ProcPrintFromDB

procedure ProcSendPrintResultToDB(CommandName: string; ErrorCode: Integer);
var
  ResultString: string;
begin
  DDEHandler.SetAnswerMode(TRUE);
  ResultString := WgBlockStart + 'PRNRES' + WgBlockEnd;
  ResultString := ResultString + WgBlockStart + CommandName + WgBlockEnd;
  ResultString := ResultString + WgBlockStart + IntToStr(ErrorCode) + WgBlockEnd;
  DDEHandler.SendString(ResultString);
  DDEHandler.SetAnswerMode(FALSE);
end;

function FuncCreatePrintHandler(var AMenuHandler: TMenuHandler; AProj: PProj): TPrintHandler;
var
  AHandlerClass: TMenuHandlerClass;
begin
  Result := nil;
  AMenuHandler := nil;
  AHandlerClass := MenuHandlers['FilePrint'];
  AMenuHandler := AHandlerClass.Create(nil);
  if AMenuHandler is TProjMenuHandler then
    TProjMenuHandler(AMenuHandler).Project := AProj;
  AMenuHandler.Parent := AProj^.Parent;
  AMenuHandler.Canvas := AProj^.Parent.Canvas;
  if AMenuHandler is TPrintHandler then
    Result := TPrintHandler(AMenuHandler);
end;

function FuncPrintFromDB(Mode: Integer; FileName: string; Copies: Integer;
  CollateCopies: Boolean; ShowProgress: Boolean;
  PrintMonitoring: Boolean; AData: PProj): Integer;
var
  AMenuHandler: TMenuHandler;
  PrintHandler: TPrintHandler;
begin
  Result := 0;
  PrintHandler := FuncCreatePrintHandler(AMenuHandler, AData);
  if PrintHandler <> nil then
  begin
    Result := PrintHandler.PrintFromDB(Mode, FileName, Copies, CollateCopies,
      ShowProgress, PrintMonitoring);
    if AMenuHandler.Status = mhsNotFinished then
    begin
      AData^.ActualMen := 0;
      AData^.MenuHandler := AMenuHandler;
      AData^.MenuHandler.OnActivate;
      UserInterface.Update([uiMenus, uiCursor, uiStatusBar]);
    end
    else
      AMenuHandler.Free;
  end
  else
    Result := 18; // WinGIS internal ERROR
end;
{-- Moskaliov AXWinGIS Printing Methods BUILD#127 05.09.00}

{++ Moskaliov AXWinGIS Printing Methods BUILD#133 02.10.00}

procedure ProcSetPrnParamFromDB(AData: PProj);
var
  AMenuHandler: TMenuHandler;
  PrintHandler: TPrintHandler;
  StrOK: Boolean;
  StrFound: Boolean;
  MoreStrs: Boolean;
  Printer: TWPrinter;
  intValue: Integer;
  intErrorCode: Integer;
  strDeviceName: string;
  strPaperFormat: string;
  intPaperFormatIndex: Integer;
  lOrientation: LongInt;
begin
  strDeviceName := '';
  strPaperFormat := '';
  intPaperFormatIndex := -1;
  lOrientation := 0;
  intErrorCode := 0;
  AMenuHandler := nil;

   // read the DeviceName parameter
  StrOK := ReadDDECommandString(DDEHandler.DDEData^.At(0), 1, 0, WgBlockStart,
    DDEHandler.FDDESepSign, WgBlockEnd, strDeviceName, StrFound, MoreStrs);
  if StrOK then
  begin
    StrOK := WPrinters.Printers.Count > 0;
    if StrOK then
      if strDeviceName = '' then // Printer will take the value DefaultPrinter
        Printer := WPrinters.DefaultPrinter
      else
      begin
        intValue := WPrinters.Printers.IndexOf(strDeviceName);
        if intValue > -1 then
          Printer := WPrinters[intValue]
        else
        begin
          intErrorCode := 1; // value of the DeviceName parameter is invalid
          StrOK := FALSE;
        end;
      end
    else
      intErrorCode := 17; // there is no printers istalled in system
  end
  else
    intErrorCode := 1; // value of the DeviceName parameter is invalid

  if StrOK then
  begin
         // read the PaperFormat parameter
    StrOK := ReadDDECommandString(DDEHandler.DDEData^.At(0), 2, 0, WgBlockStart,
      DDEHandler.FDDESepSign, WgBlockEnd, strPaperFormat, StrFound, MoreStrs);
    if StrOK then
    begin
      if strPaperFormat = '' then // There will be make an attempt to preserve current Paper Format
      begin
        PrintHandler := FuncCreatePrintHandler(AMenuHandler, AData);
        if PrintHandler <> nil then
          intPaperFormatIndex := PrintHandler.FuncGetPaperFormat(Printer)
        else
        begin
          intErrorCode := 18; // WinGIS internal ERROR
          StrOK := FALSE;
        end;
      end
      else
        intPaperFormatIndex := Printer.PaperFormatNames.IndexOf(strPaperFormat);
      if StrOK then
      begin
        StrOK := (intPaperFormatIndex > -1);
        if not StrOK then
          intErrorCode := 2; // value of the PaperFormat parameter is invalid
      end;
    end
    else
      intErrorCode := 2; // value of the PaperFormat parameter is invalid
  end;

  if StrOK then
  begin
         // read the Orientation parameter
    StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 3, 0, WgBlockStart,
      DDEHandler.FDDESepSign, WgBlockEnd, lOrientation, StrFound, MoreStrs);
    StrOK := StrOK and (lOrientation >= 0) and (lOrientation <= 1); // the value is valid
    if not StrOK then
      intErrorCode := 3; // value of the Orientation parameter is invalid
  end;

  if StrOK then
  begin
    if AMenuHandler = nil then
      PrintHandler := FuncCreatePrintHandler(AMenuHandler, AData);
    if PrintHandler <> nil then
      intErrorCode := PrintHandler.SetPrnParamFromDB(Printer, intPaperFormatIndex, lOrientation)
    else
      intErrorCode := 18; // WinGIS internal ERROR
  end;

  if AMenuHandler <> nil then
    AMenuHandler.Free;

  if DDEHandler.DDEData^.GetCount > 0 then
    PChar(DDEHandler.DDEData^.At(0))[0] := DDEStrUsed;
  DDEHandler.DeleteDDEData;

  ProcSendPrintResultToDB('PRNSPP', intErrorCode);
end;

procedure ProcSetPgsPrnParamFromDB(AData: PProj);
var
  AMenuHandler: TMenuHandler;
  PrintHandler: TPrintHandler;
  StrOK: Boolean;
  StrFound: Boolean;
  MoreStrs: Boolean;
  intValue: Integer;
  intErrorCode: Integer;
  lModel: LongInt;
  boolAutoRepaginate: Boolean;
  intHorizontalPages: Integer;
  intVerticalPages: Integer;
  PageFrames: TGrRect;
  InnerFrames: TGrRect;
  OverlapFrames: TGrRect;
  boolNumberingOrder: Boolean;
  intNumberingFrom: Integer;
begin
  lModel := 0;
  intErrorCode := 0;
  boolAutoRepaginate := FALSE;
  intHorizontalPages := 1;
  intVerticalPages := 1;
   // read the LayoutModel parameter
  StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 1, 0, WgBlockStart,
    DDEHandler.FDDESepSign, WgBlockEnd, lModel, StrFound, MoreStrs);
  StrOK := StrOK and (lModel >= 0) and (lModel <= 3); // the value is valid

  if StrOK then
  begin
         // read the AutoRepagination parameter
    StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 2, 0, WgBlockStart,
      DDEHandler.FDDESepSign, WgBlockEnd, intValue, StrFound, MoreStrs);
    if StrOK then
      case intValue of
        0: boolAutoRepaginate := FALSE;
        1: boolAutoRepaginate := TRUE;
      else
        begin
          StrOK := FALSE;
          intErrorCode := 2; // value of the AutoRepagination parameter is invalid
        end;
      end
    else
      intErrorCode := 2; // value of the AutoRepagination parameter is invalid
  end
  else
    intErrorCode := 1; // value of the LayoutModel parameter is invalid

  if StrOK then
  begin
         // read the HorizontalPages parameter
    StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 3, 0, WgBlockStart,
      DDEHandler.FDDESepSign, WgBlockEnd, intHorizontalPages, StrFound, MoreStrs);
    StrOK := StrOK and (intHorizontalPages > 0); // the value is valid
    if not StrOK then
      intErrorCode := 3; // value of the HorizontalPages parameter is invalid
  end;

  if StrOK then
  begin
         // read the VerticalPages parameter
    StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 4, 0, WgBlockStart,
      DDEHandler.FDDESepSign, WgBlockEnd, intVerticalPages, StrFound, MoreStrs);
    StrOK := StrOK and (intVerticalPages > 0); // the value is valid
    if not StrOK then
      intErrorCode := 4; // value of the VerticalPages parameter is invalid
  end;

  if StrOK then
  begin
         // read the Margins parameters
    StrOK := ReadDDECommandTGrRect(DDEHandler.DDEData^.At(0), 5, 0, WgBlockStart,
      DDEHandler.FDDESepSign, WgBlockEnd, PageFrames, StrFound, MoreStrs);
    StrOK := StrOK and (PageFrames.Left >= 0) and (PageFrames.Bottom >= 0)
      and (PageFrames.Right >= 0) and (PageFrames.Top >= 0); // the value is valid
    if not StrOK then
      intErrorCode := 5; // value of the Margins parameters is invalid
  end;

  if StrOK then
  begin
         // read the Inner Margins parameters
    StrOK := ReadDDECommandTGrRect(DDEHandler.DDEData^.At(0), 6, 0, WgBlockStart,
      DDEHandler.FDDESepSign, WgBlockEnd, InnerFrames, StrFound, MoreStrs);
    StrOK := StrOK and (InnerFrames.Left >= 0) and (InnerFrames.Bottom >= 0)
      and (InnerFrames.Right >= 0) and (InnerFrames.Top >= 0); // the value is valid
    if not StrOK then
      intErrorCode := 6; // value of the Inner Margins parameters is invalid
  end;

  if StrOK then
  begin
         // read the Overlap Margins parameters
    StrOK := ReadDDECommandTGrRect(DDEHandler.DDEData^.At(0), 7, 0, WgBlockStart,
      DDEHandler.FDDESepSign, WgBlockEnd, OverlapFrames, StrFound, MoreStrs);
    StrOK := StrOK and (OverlapFrames.Left >= 0) and (OverlapFrames.Bottom >= 0)
      and (OverlapFrames.Right >= 0) and (OverlapFrames.Top >= 0); // the value is valid
    if not StrOK then
      intErrorCode := 7; // value of the Overlap Margins parameters is invalid
  end;

  if StrOK then
  begin
         // read the NumberingOrder parameter
    StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 8, 0, WgBlockStart,
      DDEHandler.FDDESepSign, WgBlockEnd, intValue, StrFound, MoreStrs);
    if StrOK then
      case intValue of
        0: boolNumberingOrder := TRUE; // From left to right and down
        1: boolNumberingOrder := FALSE; // From top to bottom and right
      else
        begin
          StrOK := FALSE;
          intErrorCode := 8; // value of the NumberingOrder parameter is invalid
        end;
      end
    else
      intErrorCode := 8; // value of the NumberingOrder parameter is invalid
  end;

  if StrOK then
  begin
         // read the NumberingFrom parameter
    StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 9, 0, WgBlockStart,
      DDEHandler.FDDESepSign, WgBlockEnd, intNumberingFrom, StrFound, MoreStrs);
    StrOK := StrOK and (intNumberingFrom > 0); // the value is valid
    if not StrOK then
      intErrorCode := 9; // value of the NumberingFrom parameter is invalid
  end;

  if StrOK then
    if WPrinters.Printers.Count > 0 then
    begin
      PrintHandler := FuncCreatePrintHandler(AMenuHandler, AData);
      if PrintHandler <> nil then
        intErrorCode := PrintHandler.SetPgsParamFromDB(lModel, boolAutoRepaginate,
          intHorizontalPages, intVerticalPages, PageFrames, InnerFrames,
          OverlapFrames, boolNumberingOrder, intNumberingFrom)
      else
        intErrorCode := 18; // WinGIS internal ERROR
      if AMenuHandler <> nil then
        AMenuHandler.Free;
    end
    else
      intErrorCode := 17; // there is no printers istalled in system

  if DDEHandler.DDEData^.GetCount > 0 then
    PChar(DDEHandler.DDEData^.At(0))[0] := DDEStrUsed;
  DDEHandler.DeleteDDEData;

  ProcSendPrintResultToDB('PRNSPG', intErrorCode);

end;
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 02.10.00}

{++ Moskaliov AXWinGIS Printing Methods BUILD#133 09.10.00}

procedure ProcSetMapParamFromDB(AData: PProj);
var
  AMenuHandler: TMenuHandler;
  PrintHandler: TPrintHandler;
  StrOK: Boolean;
  StrFound: Boolean;
  MoreStrs: Boolean;
  intValue: Integer;
  realVal: Real;
  intErrorCode: Integer;
  lMapID: Integer;
  lRegion: Integer;
  strViewName: string;
  RegionBorders: TGrRect;
  RegionPoint1: TGrPoint;
  RegionPoint2: TGrPoint;
  bFitToPage: Boolean;
  dPrintScale: Double;
  bKeepScale: Boolean;
  dFrameWidth: Double;
  dFrameHeight: Double;
  lFrameCentering: Integer;
  dFrameLeft: Double;
  dFrameTop: Double;
begin
  intErrorCode := 0;
  lMapID := 0;
  lRegion := 0;
  strViewName := '';
  RegionBorders := GrRect(0, 0, 0, 0);
  RegionPoint1 := GrPoint(0, 0);
  RegionPoint2 := GrPoint(0, 0);
  bFitToPage := TRUE;
  dPrintScale := 0.1;
  bKeepScale := FALSE;
  dFrameWidth := 0.0;
  dFrameHeight := 0.0;
  lFrameCentering := 0;
  dFrameLeft := 0.0;
  dFrameTop := 0.0;

   // read the MapID parameter
  StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 1, 0, WgBlockStart,
    DDEHandler.FDDESepSign, WgBlockEnd, lMapID, StrFound, MoreStrs);
  StrOK := True; //StrOK and (lMapID = 0);   // the value is valid

  if StrOK then
  begin
         // read the PrintingRegion parameter
    StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 2, 0, WgBlockStart,
      DDEHandler.FDDESepSign, WgBlockEnd, lRegion, StrFound, MoreStrs);
    case lRegion of
      0: ;
      1:
        begin
                   // read the ViewName parameter
          StrOK := ReadDDECommandString(DDEHandler.DDEData^.At(0), 3, 0, WgBlockStart,
            DDEHandler.FDDESepSign, WgBlockEnd, strViewName, StrFound, MoreStrs);
          StrOK := StrOK and (strViewName <> '');
          if not StrOK then
            intErrorCode := 3; // value of the ViewName parameter is invalid
        end;
      2: ;
      3:
        begin
                   // read the RegionBorders parameters
          StrOK := ReadDDECommandTGrRect(DDEHandler.DDEData^.At(0), 4, 0, WgBlockStart,
            DDEHandler.FDDESepSign, WgBlockEnd, RegionBorders, StrFound, MoreStrs);
          if StrOK then
          begin
            RegionPoint1 := RegionBorders.BottomLeft;
            RegionPoint2 := RegionBorders.TopRight;
          end
          else
            intErrorCode := 4; // value of the RegionBorders parameters is invalid
        end;
    else
      begin
        StrOK := FALSE;
        intErrorCode := 2; // value of the PrintingRegion parameter is invalid
      end;
    end;
  end
  else
    intErrorCode := 1; // value of the MapID parameter is invalid

  if StrOK then
  begin
         // read the FitToPage parameter
    StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 5, 0, WgBlockStart,
      DDEHandler.FDDESepSign, WgBlockEnd, intValue, StrFound, MoreStrs);
    if StrOK then
      case intValue of
        0:
          begin
            bFitToPage := FALSE;
                      // read the PrintScale parameter
            StrOK := ReadDDECommandReal(DDEHandler.DDEData^.At(0), 6, 0, WgBlockStart,
              DDEHandler.FDDESepSign, WgBlockEnd, realVal, StrFound, MoreStrs);

            realVal := 1 / realVal;

            StrOK := StrOK and (realVal > 0) and (realVal < 1); // the value is valid
            if StrOK then
            begin
              dPrintScale := realVal;
                            // read the KeepScale parameter
              StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 7, 0, WgBlockStart,
                DDEHandler.FDDESepSign, WgBlockEnd, intValue, StrFound, MoreStrs);
              if StrOK then
                case intValue of
                  0: bKeepScale := FALSE;
                  1: bKeepScale := TRUE;
                else
                  begin
                    StrOK := FALSE;
                    intErrorCode := 7; // value of the KeepScale parameter is invalid
                  end;
                end;
              if StrOK then
              begin
                                  // read the FrameWidth parameter
                StrOK := ReadDDECommandReal(DDEHandler.DDEData^.At(0), 8, 0, WgBlockStart,
                  DDEHandler.FDDESepSign, WgBlockEnd, realVal, StrFound, MoreStrs);
                StrOK := True; //StrOK and (realVal > 0);   // the value is valid
                if StrOK then
                begin
                  dFrameWidth := realVal;
                                        // read the FrameHeight parameter
                  StrOK := ReadDDECommandReal(DDEHandler.DDEData^.At(0), 8, 1, WgBlockStart,
                    DDEHandler.FDDESepSign, WgBlockEnd, realVal, StrFound, MoreStrs);
                  StrOK := True; //StrOK and (realVal > 0);   // the value is valid
                  if StrOK then
                  begin
                    dFrameHeight := realVal;
                                              // read the FrameCentering parameter
                    StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 9, 0, WgBlockStart,
                      DDEHandler.FDDESepSign, WgBlockEnd, lFrameCentering, StrFound, MoreStrs);
                    StrOK := StrOK and (lFrameCentering >= 0) and (lFrameCentering < 4); // the value is valid
                    if StrOK then
                    begin
                      if lFrameCentering = 0 then
                      begin
                                                          // read the FrameLeft parameter
                        StrOK := ReadDDECommandReal(DDEHandler.DDEData^.At(0), 10, 0, WgBlockStart,
                          DDEHandler.FDDESepSign, WgBlockEnd, realVal, StrFound, MoreStrs);
                        StrOK := StrOK and (realVal > -dFrameWidth); // the value is valid
                        if StrOK then
                        begin
                          dFrameLeft := realVal;
                                                                // read the FrameLeft parameter
                          StrOK := ReadDDECommandReal(DDEHandler.DDEData^.At(0), 10, 1, WgBlockStart,
                            DDEHandler.FDDESepSign, WgBlockEnd, realVal, StrFound, MoreStrs);
                          StrOK := StrOK and (realVal > -dFrameHeight); // the value is valid
                          if StrOK then
                            dFrameTop := realVal
                          else
                            intErrorCode := 10; // value of the FrameTop parameter is invalid
                        end
                        else
                          intErrorCode := 10; // value of the FrameLeft parameter is invalid
                      end;
                    end
                    else
                      intErrorCode := 9; // value of the FrameCentering parameter is invalid
                  end
                  else
                    intErrorCode := 8; // value of the FrameHeight parameter is invalid
                end
                else
                  intErrorCode := 8; // value of the FrameWidth parameter is invalid
              end;
            end
            else
              intErrorCode := 6; // value of the PrintScale parameter is invalid
          end;
        1: bFitToPage := TRUE;
                   // all other parameters are ignored
      else
        begin
          StrOK := FALSE;
          intErrorCode := 5; // value of the FitToPage parameter is invalid
        end;
      end;
  end;

  if StrOK then
    if WPrinters.Printers.Count > 0 then
    begin
      PrintHandler := FuncCreatePrintHandler(AMenuHandler, AData);
      if PrintHandler <> nil then
        intErrorCode := PrintHandler.SetMapParamFromDB(lMapID, lRegion, strViewName,
          RegionPoint1, RegionPoint2, bFitToPage, dPrintScale, bKeepScale,
          dFrameWidth, dFrameHeight, lFrameCentering, dFrameLeft, dFrameTop)
      else
        intErrorCode := 18; // WinGIS internal ERROR
      if AMenuHandler <> nil then
        AMenuHandler.Free;
    end
    else
      intErrorCode := 17; // there is no printers istalled in system

  if DDEHandler.DDEData^.GetCount > 0 then
    PChar(DDEHandler.DDEData^.At(0))[0] := DDEStrUsed;
  DDEHandler.DeleteDDEData;

  ProcSendPrintResultToDB('PRNSM', intErrorCode);

end;

procedure ProcSetLegParamFromDB(AData: PProj);
var
  AMenuHandler: TMenuHandler;
  PrintHandler: TPrintHandler;
  StrOK: Boolean;
  StrFound: Boolean;
  MoreStrs: Boolean;
  realVal: Real;
  intErrorCode: Integer;
  LegendID: Integer;
  FrameWidth: Double;
  FrameHeight: Double;
  FrameCentering: Integer;
  FrameOridgin: Integer;
  FrameLeft: Double;
  FrameTopOrBot: Double;
begin

   // read the LegendID parameter
  StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 1, 0, WgBlockStart,
    DDEHandler.FDDESepSign, WgBlockEnd, LegendID, StrFound, MoreStrs);
  StrOK := StrOK and (LegendID >= 0); // the value is valid

  if StrOK then
  begin
         // read the FrameWidth parameter
    StrOK := ReadDDECommandReal(DDEHandler.DDEData^.At(0), 2, 0, WgBlockStart,
      DDEHandler.FDDESepSign, WgBlockEnd, realVal, StrFound, MoreStrs);
    StrOK := StrOK and (realVal > 0); // the value is valid
    if StrOK then
    begin
      FrameWidth := realVal;
               // read the FrameHeight parameter
      StrOK := ReadDDECommandReal(DDEHandler.DDEData^.At(0), 2, 1, WgBlockStart,
        DDEHandler.FDDESepSign, WgBlockEnd, realVal, StrFound, MoreStrs);
      StrOK := StrOK and (realVal > 0); // the value is valid
      if StrOK then
      begin
        FrameHeight := realVal;
                     // read the FrameCentering parameter
        StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 3, 0, WgBlockStart,
          DDEHandler.FDDESepSign, WgBlockEnd, FrameCentering, StrFound, MoreStrs);
        StrOK := StrOK and (FrameCentering >= 0) and (FrameCentering < 4); // the value is valid
        if StrOK then
        begin
                           // read the FrameOridgin parameter
          StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 4, 0, WgBlockStart,
            DDEHandler.FDDESepSign, WgBlockEnd, FrameOridgin, StrFound, MoreStrs);
          StrOK := StrOK and (FrameOridgin >= 0) and (FrameOridgin < 2); // the value is valid
          if StrOK then
          begin
                                 // read the FrameLeft parameter
            StrOK := ReadDDECommandReal(DDEHandler.DDEData^.At(0), 5, 0, WgBlockStart,
              DDEHandler.FDDESepSign, WgBlockEnd, realVal, StrFound, MoreStrs);
            StrOK := StrOK and (realVal > -FrameWidth); // the value is valid
            if StrOK then
            begin
              FrameLeft := realVal;
                                       // read the FrameTopOrBot parameter
              StrOK := ReadDDECommandReal(DDEHandler.DDEData^.At(0), 5, 1, WgBlockStart,
                DDEHandler.FDDESepSign, WgBlockEnd, realVal, StrFound, MoreStrs);
              StrOK := StrOK and (realVal > -FrameHeight); // the value is valid
              if StrOK then
              begin
                FrameTopOrBot := realVal;
                if WPrinters.Printers.Count > 0 then
                begin
                  PrintHandler := FuncCreatePrintHandler(AMenuHandler, AData);
                  if PrintHandler <> nil then
                    intErrorCode := PrintHandler.SetLegParamFromDB(LegendID, FrameWidth, FrameHeight,
                      FrameCentering, FrameOridgin, FrameLeft, FrameTopOrBot)
                  else
                    intErrorCode := 18; // WinGIS internal ERROR
                  if AMenuHandler <> nil then
                    AMenuHandler.Free;
                end
                else
                  intErrorCode := 17; // there is no printers istalled in system
              end
              else
                intErrorCode := 5; // value of the FrameTopOrBot parameter is invalid
            end
            else
              intErrorCode := 5; // value of the FrameLeft parameter is invalid
          end
          else
            intErrorCode := 4; // value of the FrameOridgin parameter is invalid
        end
        else
          intErrorCode := 3; // value of the FrameCentering parameter is invalid
      end
      else
        intErrorCode := 2; // value of the FrameHeight parameter is invalid
    end
    else
      intErrorCode := 2; // value of the FrameWidth parameter is invalid
  end
  else
    intErrorCode := 1; // value of the LegendID parameter is invalid

  if DDEHandler.DDEData^.GetCount > 0 then
    PChar(DDEHandler.DDEData^.At(0))[0] := DDEStrUsed;
  DDEHandler.DeleteDDEData;

  ProcSendPrintResultToDB('PRNSL', intErrorCode);

end;

procedure ProcInsLegendFromDB(AData: PProj);
var
  AMenuHandler: TMenuHandler;
  PrintHandler: TPrintHandler;
  StrOK: Boolean;
  StrFound: Boolean;
  MoreStrs: Boolean;
  realVal: Real;
  intErrorCode: Integer;
  LegendName: string;
  FrameWidth: Double;
  FrameHeight: Double;
  FrameCentering: Integer;
  FrameOridgin: Integer;
  FrameLeft: Double;
  FrameTopOrBot: Double;
begin

   // read the LegendName parameter
  StrOK := ReadDDECommandString(DDEHandler.DDEData^.At(0), 1, 0, WgBlockStart,
    DDEHandler.FDDESepSign, WgBlockEnd, LegendName, StrFound, MoreStrs);
  StrOK := StrOK and (LegendName <> ''); // the value is valid

  if StrOK then
  begin
         // read the FrameWidth parameter
    StrOK := ReadDDECommandReal(DDEHandler.DDEData^.At(0), 2, 0, WgBlockStart,
      DDEHandler.FDDESepSign, WgBlockEnd, realVal, StrFound, MoreStrs);
    StrOK := StrOK and (realVal > 0); // the value is valid
    if StrOK then
    begin
      FrameWidth := realVal;
               // read the FrameHeight parameter
      StrOK := ReadDDECommandReal(DDEHandler.DDEData^.At(0), 2, 1, WgBlockStart,
        DDEHandler.FDDESepSign, WgBlockEnd, realVal, StrFound, MoreStrs);
      StrOK := StrOK and (realVal > 0); // the value is valid
      if StrOK then
      begin
        FrameHeight := realVal;
                     // read the FrameCentering parameter
        StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 3, 0, WgBlockStart,
          DDEHandler.FDDESepSign, WgBlockEnd, FrameCentering, StrFound, MoreStrs);
        StrOK := StrOK and (FrameCentering >= 0) and (FrameCentering < 4); // the value is valid
        if StrOK then
        begin
                           // read the FrameOridgin parameter
          StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 4, 0, WgBlockStart,
            DDEHandler.FDDESepSign, WgBlockEnd, FrameOridgin, StrFound, MoreStrs);
          StrOK := StrOK and (FrameOridgin >= 0) and (FrameOridgin < 2); // the value is valid
          if StrOK then
          begin
                                 // read the FrameLeft parameter
            StrOK := ReadDDECommandReal(DDEHandler.DDEData^.At(0), 5, 0, WgBlockStart,
              DDEHandler.FDDESepSign, WgBlockEnd, realVal, StrFound, MoreStrs);
            StrOK := StrOK and (realVal > -FrameWidth); // the value is valid
            if StrOK then
            begin
              FrameLeft := realVal;
                                       // read the FrameTopOrBot parameter
              StrOK := ReadDDECommandReal(DDEHandler.DDEData^.At(0), 5, 1, WgBlockStart,
                DDEHandler.FDDESepSign, WgBlockEnd, realVal, StrFound, MoreStrs);
              StrOK := StrOK and (realVal > -FrameHeight); // the value is valid
              if StrOK then
              begin
                FrameTopOrBot := realVal;
                if WPrinters.Printers.Count > 0 then
                begin
                  PrintHandler := FuncCreatePrintHandler(AMenuHandler, AData);
                  if PrintHandler <> nil then
                    intErrorCode := PrintHandler.IsertLegendFromDB(LegendName, FrameWidth, FrameHeight,
                      FrameCentering, FrameOridgin, FrameLeft, FrameTopOrBot)
                  else
                    intErrorCode := 18; // WinGIS internal ERROR
                  if AMenuHandler <> nil then
                    AMenuHandler.Free;
                end
                else
                  intErrorCode := 17; // there is no printers istalled in system
              end
              else
                intErrorCode := 5; // value of the FrameTopOrBot parameter is invalid
            end
            else
              intErrorCode := 5; // value of the FrameLeft parameter is invalid
          end
          else
            intErrorCode := 4; // value of the FrameOridgin parameter is invalid
        end
        else
          intErrorCode := 3; // value of the FrameCentering parameter is invalid
      end
      else
        intErrorCode := 2; // value of the FrameHeight parameter is invalid
    end
    else
      intErrorCode := 2; // value of the FrameWidth parameter is invalid
  end
  else
    intErrorCode := 1; // value of the LegendName parameter is invalid

  if DDEHandler.DDEData^.GetCount > 0 then
    PChar(DDEHandler.DDEData^.At(0))[0] := DDEStrUsed;
  DDEHandler.DeleteDDEData;

  ProcSendPrintResultToDB('PRNIL', intErrorCode);

end;

procedure ProcGetPrnParamFromDB(AData: PProj);
var
  AMenuHandler: TMenuHandler;
  PrintHandler: TPrintHandler;
  intErrorCode: Integer;
  PrinterName: string;
  PaperFormatName: string;
  Orientation: Integer;
  ResultString: string;
begin
  PrinterName := '';
  ResultString := '';
  Orientation := 0;

  if WPrinters.Printers.Count > 0 then
  begin
    PrintHandler := FuncCreatePrintHandler(AMenuHandler, AData);
    if PrintHandler <> nil then
      intErrorCode := PrintHandler.GetPrnParamFromDB(PrinterName, PaperFormatName, Orientation)
    else
      intErrorCode := 18; // WinGIS internal ERROR
    if AMenuHandler <> nil then
      AMenuHandler.Free;
  end
  else
    intErrorCode := 17; // there is no printers istalled in system

  if DDEHandler.DDEData^.GetCount > 0 then
    PChar(DDEHandler.DDEData^.At(0))[0] := DDEStrUsed;
  DDEHandler.DeleteDDEData;

  DDEHandler.SetAnswerMode(TRUE);
  ResultString := WgBlockStart + 'PRNSPP' + WgBlockEnd;
  ResultString := ResultString + WgBlockStart + PrinterName + WgBlockEnd;
  ResultString := ResultString + WgBlockStart + PaperFormatName + WgBlockEnd;
  ResultString := ResultString + WgBlockStart + IntToStr(Orientation) + WgBlockEnd;
  ResultString := ResultString + WgBlockStart + IntToStr(intErrorCode) + WgBlockEnd;
  DDEHandler.SendString(ResultString);
  DDEHandler.SetAnswerMode(FALSE);
end;

procedure ProcGetPgsPrnParamFromDB(AData: PProj);
var
  AMenuHandler: TMenuHandler;
  PrintHandler: TPrintHandler;
  intErrorCode: Integer;
  intModel: Integer;
  intAutoRepaginate: Integer;
  intHorizontalPages: Integer;
  intVerticalPages: Integer;
  PageFrames: TGrRect;
  InnerFrames: TGrRect;
  OverlapFrames: TGrRect;
  intNumberingOrder: Integer;
  intNumberingFrom: Integer;
  ResultString: string;
begin
  intModel := 0;
  intAutoRepaginate := 0;
  intHorizontalPages := 1;
  intVerticalPages := 1;
  PageFrames := GrRect(0, 0, 0, 0);
  InnerFrames := GrRect(0, 0, 0, 0);
  OverlapFrames := GrRect(0, 0, 0, 0);
  intNumberingOrder := 0;
  intNumberingFrom := 1;

  if WPrinters.Printers.Count > 0 then
  begin
    PrintHandler := FuncCreatePrintHandler(AMenuHandler, AData);
    if PrintHandler <> nil then
      intErrorCode := PrintHandler.GetPgsParamFromDB(intModel, intAutoRepaginate,
        intHorizontalPages, intVerticalPages, PageFrames, InnerFrames,
        OverlapFrames, intNumberingOrder, intNumberingFrom)
    else
      intErrorCode := 18; // WinGIS internal ERROR
    if AMenuHandler <> nil then
      AMenuHandler.Free;
  end
  else
    intErrorCode := 17; // there is no printers istalled in system

  if DDEHandler.DDEData^.GetCount > 0 then
    PChar(DDEHandler.DDEData^.At(0))[0] := DDEStrUsed;
  DDEHandler.DeleteDDEData;

  DDEHandler.SetAnswerMode(TRUE);
  ResultString := WgBlockStart + 'PRNSPG' + WgBlockEnd;
  ResultString := ResultString + WgBlockStart + IntToStr(intModel) + WgBlockEnd;
  ResultString := ResultString + WgBlockStart + IntToStr(intAutoRepaginate) + WgBlockEnd;
  ResultString := ResultString + WgBlockStart + IntToStr(intHorizontalPages) + WgBlockEnd;
  ResultString := ResultString + WgBlockStart + IntToStr(intVerticalPages) + WgBlockEnd;

  with PageFrames do
  begin
    ResultString := ResultString + WgBlockStart + FloatToStr(Left) + DDEHandler.FDDESepSign;
    ResultString := ResultString + FloatToStr(Bottom) + DDEHandler.FDDESepSign;
    ResultString := ResultString + FloatToStr(Right) + DDEHandler.FDDESepSign;
    ResultString := ResultString + FloatToStr(Top) + WgBlockEnd;
  end;
  with InnerFrames do
  begin
    ResultString := ResultString + WgBlockStart + FloatToStr(Left) + DDEHandler.FDDESepSign;
    ResultString := ResultString + FloatToStr(Bottom) + DDEHandler.FDDESepSign;
    ResultString := ResultString + FloatToStr(Right) + DDEHandler.FDDESepSign;
    ResultString := ResultString + FloatToStr(Top) + WgBlockEnd;
  end;
  with OverlapFrames do
  begin
    ResultString := ResultString + WgBlockStart + FloatToStr(Left) + DDEHandler.FDDESepSign;
    ResultString := ResultString + FloatToStr(Bottom) + DDEHandler.FDDESepSign;
    ResultString := ResultString + FloatToStr(Right) + DDEHandler.FDDESepSign;
    ResultString := ResultString + FloatToStr(Top) + WgBlockEnd;
  end;

  ResultString := ResultString + WgBlockStart + IntToStr(intNumberingOrder) + WgBlockEnd;
  ResultString := ResultString + WgBlockStart + IntToStr(intNumberingFrom) + WgBlockEnd;
  ResultString := ResultString + WgBlockStart + IntToStr(intErrorCode) + WgBlockEnd;
  DDEHandler.SendString(ResultString);
  DDEHandler.SetAnswerMode(FALSE);
end;

procedure ProcGetMapParamFromDB(AData: PProj);
var
  AMenuHandler: TMenuHandler;
  PrintHandler: TPrintHandler;
  StrOK: Boolean;
  StrFound: Boolean;
  MoreStrs: Boolean;
  intErrorCode: Integer;
  ResultString: string;
  intMapID: Integer;
  intRegion: Integer;
  strViewName: string;
  RegionBorders: TGrRect;
  intFitToPage: Integer;
  dPrintScale: Double;
  intKeepScale: Integer;
  dFrameWidth: Double;
  dFrameHeight: Double;
  intFrameCentering: Integer;
  dFrameLeft: Double;
  dFrameTop: Double;
begin
  intErrorCode := 0;
  intMapID := 0;
  intRegion := 0;
  strViewName := '';
  RegionBorders := GrRect(0, 0, 0, 0);
  intFitToPage := 0;
  dPrintScale := 0.1;
  intKeepScale := 0;
  dFrameWidth := 0.0;
  dFrameHeight := 0.0;
  intFrameCentering := 0;
  dFrameLeft := 0.0;
  dFrameTop := 0.0;

   // read the MapID parameter
  StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 1, 0, WgBlockStart,
    DDEHandler.FDDESepSign, WgBlockEnd, intMapID, StrFound, MoreStrs);
  StrOK := StrOK and (intMapID = 0); // the value is valid
  if StrOK then
    if WPrinters.Printers.Count > 0 then
    begin
      PrintHandler := FuncCreatePrintHandler(AMenuHandler, AData);
      if PrintHandler <> nil then
        intErrorCode := PrintHandler.GetMapParamFromDB(intMapID, intRegion, strViewName,
          RegionBorders, intFitToPage, dPrintScale, intKeepScale,
          dFrameWidth, dFrameHeight, intFrameCentering, dFrameLeft, dFrameTop)
      else
        intErrorCode := 18; // WinGIS internal ERROR
      if AMenuHandler <> nil then
        AMenuHandler.Free;
    end
    else
      intErrorCode := 17 // there is no printers istalled in system
  else
    intErrorCode := 1; // value of the MapID parameter is invalid

  if DDEHandler.DDEData^.GetCount > 0 then
    PChar(DDEHandler.DDEData^.At(0))[0] := DDEStrUsed;
  DDEHandler.DeleteDDEData;

  DDEHandler.SetAnswerMode(TRUE);
  ResultString := WgBlockStart + 'PRNSM' + WgBlockEnd;

  ResultString := ResultString + WgBlockStart + IntToStr(intMapID) + WgBlockEnd;
  ResultString := ResultString + WgBlockStart + IntToStr(intRegion) + WgBlockEnd;
  ResultString := ResultString + WgBlockStart + strViewName + WgBlockEnd;

  with RegionBorders do
  begin
    ResultString := ResultString + WgBlockStart + FloatToStr(Left) + DDEHandler.FDDESepSign;
    ResultString := ResultString + FloatToStr(Bottom) + DDEHandler.FDDESepSign;
    ResultString := ResultString + FloatToStr(Right) + DDEHandler.FDDESepSign;
    ResultString := ResultString + FloatToStr(Top) + WgBlockEnd;
  end;

  ResultString := ResultString + WgBlockStart + IntToStr(intFitToPage) + WgBlockEnd;
  ResultString := ResultString + WgBlockStart + FloatToStr(dPrintScale) + WgBlockEnd;
  ResultString := ResultString + WgBlockStart + IntToStr(intKeepScale) + WgBlockEnd;
  ResultString := ResultString + WgBlockStart + FloatToStr(dFrameWidth) + DDEHandler.FDDESepSign;
  ResultString := ResultString + FloatToStr(dFrameHeight) + WgBlockEnd;
  ResultString := ResultString + WgBlockStart + IntToStr(intFrameCentering) + WgBlockEnd;
  ResultString := ResultString + WgBlockStart + FloatToStr(dFrameLeft) + DDEHandler.FDDESepSign;
  ResultString := ResultString + FloatToStr(dFrameTop) + WgBlockEnd;

  ResultString := ResultString + WgBlockStart + IntToStr(intErrorCode) + WgBlockEnd;
  DDEHandler.SendString(ResultString);
  DDEHandler.SetAnswerMode(FALSE);
end;

procedure ProcGetLegParamFromDB(AData: PProj);
var
  AMenuHandler: TMenuHandler;
  PrintHandler: TPrintHandler;
  StrOK: Boolean;
  StrFound: Boolean;
  MoreStrs: Boolean;
  intErrorCode: Integer;
  ResultString: string;
  LegendID: Integer;
  LegendName: string;
  FrameWidth: Double;
  FrameHeight: Double;
  FrameCentering: Integer;
  FrameOridgin: Integer;
  FrameLeft: Double;
  FrameTopOrBot: Double;
begin
  LegendID := 0;
  LegendName := '';
  FrameWidth := 0.0;
  FrameHeight := 0.0;
  FrameCentering := 0;
  FrameOridgin := 0;
  FrameLeft := 0.0;
  FrameTopOrBot := 0.0;

   // read the LegendID parameter
  StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 1, 0, WgBlockStart,
    DDEHandler.FDDESepSign, WgBlockEnd, LegendID, StrFound, MoreStrs);
  StrOK := StrOK and (LegendID >= 0); // the value is valid
  if StrOK then
    if WPrinters.Printers.Count > 0 then
    begin
      PrintHandler := FuncCreatePrintHandler(AMenuHandler, AData);
      if PrintHandler <> nil then
        intErrorCode := PrintHandler.GetLegParamFromDB(LegendID, LegendName,
          FrameWidth, FrameHeight, FrameCentering,
          FrameOridgin, FrameLeft, FrameTopOrBot)
      else
        intErrorCode := 18; // WinGIS internal ERROR
      if AMenuHandler <> nil then
        AMenuHandler.Free;
    end
    else
      intErrorCode := 17 // there is no printers istalled in system
  else
    intErrorCode := 1; // value of the LegendID parameter is invalid

  if DDEHandler.DDEData^.GetCount > 0 then
    PChar(DDEHandler.DDEData^.At(0))[0] := DDEStrUsed;
  DDEHandler.DeleteDDEData;

  DDEHandler.SetAnswerMode(TRUE);
  ResultString := WgBlockStart + 'PRNSL' + WgBlockEnd;

  ResultString := ResultString + WgBlockStart + IntToStr(LegendID) + WgBlockEnd;
  ResultString := ResultString + WgBlockStart + LegendName + WgBlockEnd;
  ResultString := ResultString + WgBlockStart + FloatToStr(FrameWidth) + DDEHandler.FDDESepSign;
  ResultString := ResultString + FloatToStr(FrameHeight) + WgBlockEnd;
  ResultString := ResultString + WgBlockStart + IntToStr(FrameCentering) + WgBlockEnd;
  ResultString := ResultString + WgBlockStart + IntToStr(FrameOridgin) + WgBlockEnd;
  ResultString := ResultString + WgBlockStart + FloatToStr(FrameLeft) + DDEHandler.FDDESepSign;
  ResultString := ResultString + FloatToStr(FrameTopOrBot) + WgBlockEnd;

  ResultString := ResultString + WgBlockStart + IntToStr(intErrorCode) + WgBlockEnd;
  DDEHandler.SendString(ResultString);
  DDEHandler.SetAnswerMode(FALSE);
end;
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 09.10.00}

{++ Moskaliov AXWinGIS Printing Methods BUILD#133 23.10.00}

procedure ProcSetPicParamFromDB(AData: PProj);
var
  AMenuHandler: TMenuHandler;
  PrintHandler: TPrintHandler;
  StrOK: Boolean;
  StrFound: Boolean;
  MoreStrs: Boolean;
  realVal: Real;
  intErrorCode: Integer;
  PictureID: Integer;
  PictureLocation: Integer;
  FrameSizeInterpr: Integer;
  FrameWidth: Double;
  FrameHeight: Double;
  FrameCentering: Integer;
  FrameOridgin: Integer;
  FrameLeft: Double;
  FrameTopOrBot: Double;
begin

   // read the PictureID parameter
  StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 1, 0, WgBlockStart,
    DDEHandler.FDDESepSign, WgBlockEnd, PictureID, StrFound, MoreStrs);
  StrOK := StrOK and (PictureID >= 0); // the value is valid

  if StrOK then
  begin
         // read the PictureLocation parameter
    StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 2, 0, WgBlockStart,
      DDEHandler.FDDESepSign, WgBlockEnd, PictureLocation, StrFound, MoreStrs);
    StrOK := StrOK and (PictureLocation >= 0) and (PictureLocation <= 1111); // the value is valid
    if StrOK then
    begin
               // read the FrameSizeInterpretation parameter
      StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 3, 0, WgBlockStart,
        DDEHandler.FDDESepSign, WgBlockEnd, FrameSizeInterpr, StrFound, MoreStrs);
      StrOK := StrOK and (FrameSizeInterpr >= 0) and (FrameSizeInterpr < 4); // the value is valid
      if StrOK then
      begin
                     // read the FrameWidth parameter
        StrOK := ReadDDECommandReal(DDEHandler.DDEData^.At(0), 4, 0, WgBlockStart,
          DDEHandler.FDDESepSign, WgBlockEnd, realVal, StrFound, MoreStrs);
        StrOK := StrOK and (realVal > 0); // the value is valid
        if StrOK then
        begin
          FrameWidth := realVal;
                           // read the FrameHeight parameter
          StrOK := ReadDDECommandReal(DDEHandler.DDEData^.At(0), 4, 1, WgBlockStart,
            DDEHandler.FDDESepSign, WgBlockEnd, realVal, StrFound, MoreStrs);
          StrOK := StrOK and (realVal > 0); // the value is valid
          if StrOK then
          begin
            FrameHeight := realVal;
                                 // read the FrameCentering parameter
            StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 5, 0, WgBlockStart,
              DDEHandler.FDDESepSign, WgBlockEnd, FrameCentering, StrFound, MoreStrs);
            StrOK := StrOK and (FrameCentering >= 0) and (FrameCentering < 4); // the value is valid
            if StrOK then
            begin
                                       // read the FrameOridgin parameter
              StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 6, 0, WgBlockStart,
                DDEHandler.FDDESepSign, WgBlockEnd, FrameOridgin, StrFound, MoreStrs);
              StrOK := StrOK and (FrameOridgin >= 0) and (FrameOridgin < 2); // the value is valid
              if StrOK then
              begin
                                             // read the FrameLeft parameter
                StrOK := ReadDDECommandReal(DDEHandler.DDEData^.At(0), 7, 0, WgBlockStart,
                  DDEHandler.FDDESepSign, WgBlockEnd, realVal, StrFound, MoreStrs);
                StrOK := StrOK and (realVal > -FrameWidth); // the value is valid
                if StrOK then
                begin
                  FrameLeft := realVal;
                                                   // read the FrameTopOrBot parameter
                  StrOK := ReadDDECommandReal(DDEHandler.DDEData^.At(0), 7, 1, WgBlockStart,
                    DDEHandler.FDDESepSign, WgBlockEnd, realVal, StrFound, MoreStrs);
                  StrOK := StrOK and (realVal > -FrameHeight); // the value is valid
                  if StrOK then
                  begin
                    FrameTopOrBot := realVal;
                    if WPrinters.Printers.Count > 0 then
                    begin
                      PrintHandler := FuncCreatePrintHandler(AMenuHandler, AData);
                      if PrintHandler <> nil then
                        intErrorCode := PrintHandler.SetPicParamFromDB(PictureID, PictureLocation,
                          FrameSizeInterpr, FrameWidth, FrameHeight,
                          FrameCentering, FrameOridgin, FrameLeft, FrameTopOrBot)
                      else
                        intErrorCode := 18; // WinGIS internal ERROR
                      if AMenuHandler <> nil then
                        AMenuHandler.Free;
                    end
                    else
                      intErrorCode := 17; // there is no printers istalled in system
                  end
                  else
                    intErrorCode := 7; // value of the FrameTopOrBot parameter is invalid
                end
                else
                  intErrorCode := 7; // value of the FrameLeft parameter is invalid
              end
              else
                intErrorCode := 6; // value of the FrameOridgin parameter is invalid
            end
            else
              intErrorCode := 5; // value of the FrameCentering parameter is invalid
          end
          else
            intErrorCode := 4; // value of the FrameHeight parameter is invalid
        end
        else
          intErrorCode := 4; // value of the FrameWidth parameter is invalid
      end
      else
        intErrorCode := 3; // value of the FrameSizeInterpretation parameter is invalid
    end
    else
      intErrorCode := 2; // value of the PictureLocation parameter is invalid
  end
  else
    intErrorCode := 1; // value of the PictureID parameter is invalid

  if DDEHandler.DDEData^.GetCount > 0 then
    PChar(DDEHandler.DDEData^.At(0))[0] := DDEStrUsed;
  DDEHandler.DeleteDDEData;

  ProcSendPrintResultToDB('PRNSP', intErrorCode);

end;

procedure ProcInsPictureFromDB(AData: PProj);
var
  AMenuHandler: TMenuHandler;
  PrintHandler: TPrintHandler;
  StrOK: Boolean;
  StrFound: Boolean;
  MoreStrs: Boolean;
  realVal: Real;
  intErrorCode: Integer;
  PictureFile: string;
  PictureLocation: Integer;
  FrameSizeInterpr: Integer;
  FrameWidth: Double;
  FrameHeight: Double;
  FrameCentering: Integer;
  FrameOridgin: Integer;
  FrameLeft: Double;
  FrameTopOrBot: Double;
begin

   // read the PictureFile parameter
  StrOK := ReadDDECommandString(DDEHandler.DDEData^.At(0), 1, 0, WgBlockStart,
    DDEHandler.FDDESepSign, WgBlockEnd, PictureFile, StrFound, MoreStrs);
  StrOK := StrOK and (PictureFile <> ''); // the value is valid

  if StrOK then
  begin
         // read the PictureLocation parameter
    StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 2, 0, WgBlockStart,
      DDEHandler.FDDESepSign, WgBlockEnd, PictureLocation, StrFound, MoreStrs);
    StrOK := StrOK and (PictureLocation >= 0) and (PictureLocation <= 1111); // the value is valid
    if StrOK then
    begin
               // read the FrameSizeInterpretation parameter
      StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 3, 0, WgBlockStart,
        DDEHandler.FDDESepSign, WgBlockEnd, FrameSizeInterpr, StrFound, MoreStrs);
      StrOK := StrOK and (FrameSizeInterpr >= 0) and (FrameSizeInterpr < 4); // the value is valid
      if StrOK then
      begin
                     // read the FrameWidth parameter
        StrOK := ReadDDECommandReal(DDEHandler.DDEData^.At(0), 4, 0, WgBlockStart,
          DDEHandler.FDDESepSign, WgBlockEnd, realVal, StrFound, MoreStrs);
        StrOK := StrOK and (realVal > 0); // the value is valid
        if StrOK then
        begin
          FrameWidth := realVal;
                           // read the FrameHeight parameter
          StrOK := ReadDDECommandReal(DDEHandler.DDEData^.At(0), 4, 1, WgBlockStart,
            DDEHandler.FDDESepSign, WgBlockEnd, realVal, StrFound, MoreStrs);
          StrOK := StrOK and (realVal > 0); // the value is valid
          if StrOK then
          begin
            FrameHeight := realVal;
                                 // read the FrameCentering parameter
            StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 5, 0, WgBlockStart,
              DDEHandler.FDDESepSign, WgBlockEnd, FrameCentering, StrFound, MoreStrs);
            StrOK := StrOK and (FrameCentering >= 0) and (FrameCentering < 4); // the value is valid
            if StrOK then
            begin
                                       // read the FrameOridgin parameter
              StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 6, 0, WgBlockStart,
                DDEHandler.FDDESepSign, WgBlockEnd, FrameOridgin, StrFound, MoreStrs);
              StrOK := StrOK and (FrameOridgin >= 0) and (FrameOridgin < 2); // the value is valid
              if StrOK then
              begin
                                             // read the FrameLeft parameter
                StrOK := ReadDDECommandReal(DDEHandler.DDEData^.At(0), 7, 0, WgBlockStart,
                  DDEHandler.FDDESepSign, WgBlockEnd, realVal, StrFound, MoreStrs);
                StrOK := StrOK and (realVal > -FrameWidth); // the value is valid
                if StrOK then
                begin
                  FrameLeft := realVal;
                                                   // read the FrameTopOrBot parameter
                  StrOK := ReadDDECommandReal(DDEHandler.DDEData^.At(0), 7, 1, WgBlockStart,
                    DDEHandler.FDDESepSign, WgBlockEnd, realVal, StrFound, MoreStrs);
                  StrOK := StrOK and (realVal > -FrameHeight); // the value is valid
                  if StrOK then
                  begin
                    FrameTopOrBot := realVal;
                    if WPrinters.Printers.Count > 0 then
                    begin
                      PrintHandler := FuncCreatePrintHandler(AMenuHandler, AData);
                      if PrintHandler <> nil then
                        intErrorCode := PrintHandler.IsertPictureFromDB(PictureFile, PictureLocation,
                          FrameSizeInterpr, FrameWidth, FrameHeight,
                          FrameCentering, FrameOridgin, FrameLeft, FrameTopOrBot)
                      else
                        intErrorCode := 18; // WinGIS internal ERROR
                      if AMenuHandler <> nil then
                        AMenuHandler.Free;
                    end
                    else
                      intErrorCode := 17; // there is no printers istalled in system
                  end
                  else
                    intErrorCode := 7; // value of the FrameTopOrBot parameter is invalid
                end
                else
                  intErrorCode := 7; // value of the FrameLeft parameter is invalid
              end
              else
                intErrorCode := 6; // value of the FrameOridgin parameter is invalid
            end
            else
              intErrorCode := 5; // value of the FrameCentering parameter is invalid
          end
          else
            intErrorCode := 4; // value of the FrameHeight parameter is invalid
        end
        else
          intErrorCode := 4; // value of the FrameWidth parameter is invalid
      end
      else
        intErrorCode := 3; // value of the FrameSizeInterpretation parameter is invalid
    end
    else
      intErrorCode := 2; // value of the PictureLocation parameter is invalid
  end
  else
    intErrorCode := 1; // value of the PictureID parameter is invalid

  if DDEHandler.DDEData^.GetCount > 0 then
    PChar(DDEHandler.DDEData^.At(0))[0] := DDEStrUsed;
  DDEHandler.DeleteDDEData;

  ProcSendPrintResultToDB('PRNIP', intErrorCode);

end;

procedure ProcGetPicParamFromDB(AData: PProj);
var
  AMenuHandler: TMenuHandler;
  PrintHandler: TPrintHandler;
  StrOK: Boolean;
  StrFound: Boolean;
  MoreStrs: Boolean;
  intErrorCode: Integer;
  ResultString: string;
  PictureID: Integer;
  PictureFile: string;
  PictureLocation: Integer;
  KeepAspect: Boolean;
  intKeepAspect: Integer;
  FrameWidth: Double;
  FrameHeight: Double;
  FrameCentering: Integer;
  FrameOridgin: Integer;
  FrameLeft: Double;
  FrameTopOrBot: Double;
begin
  PictureID := 0;
  PictureFile := '';
  PictureLocation := 0;
  KeepAspect := FALSE;
  intKeepAspect := 0;
  FrameWidth := 0.0;
  FrameHeight := 0.0;
  FrameCentering := 0;
  FrameOridgin := 0;
  FrameLeft := 0.0;
  FrameTopOrBot := 0.0;

   // read the PictureID parameter
  StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 1, 0, WgBlockStart,
    DDEHandler.FDDESepSign, WgBlockEnd, PictureID, StrFound, MoreStrs);
  StrOK := StrOK and (PictureID >= 0); // the value is valid
  if StrOK then
    if WPrinters.Printers.Count > 0 then
    begin
      PrintHandler := FuncCreatePrintHandler(AMenuHandler, AData);
      if PrintHandler <> nil then
        intErrorCode := PrintHandler.GetPicParamFromDB(PictureID, PictureFile,
          PictureLocation, KeepAspect, FrameWidth, FrameHeight,
          FrameCentering, FrameOridgin, FrameLeft, FrameTopOrBot)
      else
        intErrorCode := 18; // WinGIS internal ERROR
      if AMenuHandler <> nil then
        AMenuHandler.Free;
    end
    else
      intErrorCode := 17 // there is no printers istalled in system
  else
    intErrorCode := 1; // value of the PictureID parameter is invalid

  if KeepAspect then
    intKeepAspect := 1;

  if DDEHandler.DDEData^.GetCount > 0 then
    PChar(DDEHandler.DDEData^.At(0))[0] := DDEStrUsed;
  DDEHandler.DeleteDDEData;

  DDEHandler.SetAnswerMode(TRUE);
  ResultString := WgBlockStart + 'PRNSP' + WgBlockEnd;

  ResultString := ResultString + WgBlockStart + IntToStr(PictureID) + WgBlockEnd;
  ResultString := ResultString + WgBlockStart + PictureFile + WgBlockEnd;
  ResultString := ResultString + WgBlockStart + IntToStr(PictureLocation) + WgBlockEnd;
  ResultString := ResultString + WgBlockStart + IntToStr(intKeepAspect) + WgBlockEnd;
  ResultString := ResultString + WgBlockStart + FloatToStr(FrameWidth) + DDEHandler.FDDESepSign;
  ResultString := ResultString + FloatToStr(FrameHeight) + WgBlockEnd;
  ResultString := ResultString + WgBlockStart + IntToStr(FrameCentering) + WgBlockEnd;
  ResultString := ResultString + WgBlockStart + IntToStr(FrameOridgin) + WgBlockEnd;
  ResultString := ResultString + WgBlockStart + FloatToStr(FrameLeft) + DDEHandler.FDDESepSign;
  ResultString := ResultString + FloatToStr(FrameTopOrBot) + WgBlockEnd;

  ResultString := ResultString + WgBlockStart + IntToStr(intErrorCode) + WgBlockEnd;
  DDEHandler.SendString(ResultString);
  DDEHandler.SetAnswerMode(FALSE);
end;

procedure ProcSetSigParamFromDB(AData: PProj);
var
  AMenuHandler: TMenuHandler;
  PrintHandler: TPrintHandler;
  StrOK: Boolean;
  StrFound: Boolean;
  MoreStrs: Boolean;
  intValue: Integer;
  realValue: Real;
  intErrorCode: Integer;
  SignatureID: Integer;
  Signature: string;
  FontName: string;
  Italic: Boolean;
  Bold: Boolean;
  Underlined: Boolean;
  Transparent: Boolean;
  FontSize: Integer;
  FontColor: Integer;
  FontBackColor: Integer;
  TextAlignment: Integer;
  OnPagesLocation: Boolean;
  OnFirstPage: Boolean;
  OnInternalPages: Boolean;
  OnLastPage: Boolean;
  FrameCentering: Integer;
  FrameOridgin: Integer;
  FrameLeft: Double;
  FrameTopOrBot: Double;
begin
   // read the SignatureID parameter
  StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 1, 0, WgBlockStart,
    DDEHandler.FDDESepSign, WgBlockEnd, SignatureID, StrFound, MoreStrs);
  StrOK := StrOK and (SignatureID >= 0); // the value is valid

  if StrOK then
  begin
         // read the Signature parameter
    StrOK := ReadDDECommandString(DDEHandler.DDEData^.At(0), 2, 0, WgBlockStart,
      DDEHandler.FDDESepSign, WgBlockEnd, Signature, StrFound, MoreStrs);
    StrOK := StrOK and (Signature <> ''); // the value is valid
    if StrOK then
    begin
               // read the FontName parameter
      StrOK := ReadDDECommandString(DDEHandler.DDEData^.At(0), 3, 0, WgBlockStart,
        DDEHandler.FDDESepSign, WgBlockEnd, FontName, StrFound, MoreStrs);
      StrOK := StrOK and (FontName <> ''); // the value is valid
      if StrOK then
      begin
                     // read the Italic parameter
        StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 4, 0, WgBlockStart,
          DDEHandler.FDDESepSign, WgBlockEnd, intValue, StrFound, MoreStrs);
        StrOK := StrOK and ((intValue = 0) or (intValue = 1)); // the value is valid
        if StrOK then
        begin
          Italic := (intValue = 1);
                           // read the Bold parameter
          StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 4, 1, WgBlockStart,
            DDEHandler.FDDESepSign, WgBlockEnd, intValue, StrFound, MoreStrs);
          StrOK := StrOK and ((intValue = 0) or (intValue = 1)); // the value is valid
          if StrOK then
          begin
            Bold := (intValue = 1);
                                 // read the Underlined parameter
            StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 4, 2, WgBlockStart,
              DDEHandler.FDDESepSign, WgBlockEnd, intValue, StrFound, MoreStrs);
            StrOK := StrOK and ((intValue = 0) or (intValue = 1)); // the value is valid
            if StrOK then
            begin
              Underlined := (intValue = 1);
                                       // read the Transparent parameter
              StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 4, 3, WgBlockStart,
                DDEHandler.FDDESepSign, WgBlockEnd, intValue, StrFound, MoreStrs);
              StrOK := StrOK and ((intValue = 0) or (intValue = 1)); // the value is valid
            end;
          end;
        end;
        if StrOK then
        begin
          Transparent := (intValue = 1);
                           // read the FontSize parameter
          StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 5, 0, WgBlockStart,
            DDEHandler.FDDESepSign, WgBlockEnd, FontSize, StrFound, MoreStrs);
          StrOK := StrOK and (FontSize > 0); // the value is valid
          if StrOK then
          begin
                                 // read the FontColor parameter
            StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 6, 0, WgBlockStart,
              DDEHandler.FDDESepSign, WgBlockEnd, FontColor, StrFound, MoreStrs);
            StrOK := StrOK and (FontColor >= 0); // the value is valid
            if StrOK then
            begin
                                       // read the FontBackColor parameter
              StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 6, 1, WgBlockStart,
                DDEHandler.FDDESepSign, WgBlockEnd, FontBackColor, StrFound, MoreStrs);
              StrOK := StrOK and (FontBackColor >= 0); // the value is valid
            end;
            if StrOK then
            begin
                                       // read the TextAlignment parameter
              StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 7, 0, WgBlockStart,
                DDEHandler.FDDESepSign, WgBlockEnd, TextAlignment, StrFound, MoreStrs);
              StrOK := StrOK and (TextAlignment >= 0) and (TextAlignment < 3); // the value is valid
              if StrOK then
              begin
                                             // read the OnPagesLocation parameter
                StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 8, 0, WgBlockStart,
                  DDEHandler.FDDESepSign, WgBlockEnd, intValue, StrFound, MoreStrs);
                StrOK := StrOK and ((intValue = 0) or (intValue = 1)); // the value is valid
                if StrOK then
                begin
                  OnPagesLocation := (intValue = 1);
                                                   // read the OnFirstPage parameter
                  StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 9, 0, WgBlockStart,
                    DDEHandler.FDDESepSign, WgBlockEnd, intValue, StrFound, MoreStrs);
                  StrOK := StrOK and ((intValue = 0) or (intValue = 1)); // the value is valid
                  if StrOK then
                  begin
                    OnFirstPage := (intValue = 1);
                                                         // read the OnInternalPages parameter
                    StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 9, 1, WgBlockStart,
                      DDEHandler.FDDESepSign, WgBlockEnd, intValue, StrFound, MoreStrs);
                    StrOK := StrOK and ((intValue = 0) or (intValue = 1)); // the value is valid
                    if StrOK then
                    begin
                      OnInternalPages := (intValue = 1);
                                                               // read the OnLastPage parameter
                      StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 9, 2, WgBlockStart,
                        DDEHandler.FDDESepSign, WgBlockEnd, intValue, StrFound, MoreStrs);
                      StrOK := StrOK and ((intValue = 0) or (intValue = 1)); // the value is valid
                    end;
                  end;
                  if StrOK then
                  begin
                    OnLastPage := (intValue = 1);
                                                         // read the FrameCentering parameter
                    StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 10, 0, WgBlockStart,
                      DDEHandler.FDDESepSign, WgBlockEnd, FrameCentering, StrFound, MoreStrs);
                    StrOK := StrOK and (FrameCentering >= 0) and (FrameCentering < 4); // the value is valid
                    if StrOK then
                    begin
                                                               // read the FrameOridgin parameter
                      StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 11, 0, WgBlockStart,
                        DDEHandler.FDDESepSign, WgBlockEnd, FrameOridgin, StrFound, MoreStrs);
                      StrOK := StrOK and (FrameOridgin >= 0) and (FrameOridgin < 2); // the value is valid
                      if StrOK then
                      begin
                                                                     // read the FrameLeft parameter
                        StrOK := ReadDDECommandReal(DDEHandler.DDEData^.At(0), 12, 0, WgBlockStart,
                          DDEHandler.FDDESepSign, WgBlockEnd, realValue, StrFound, MoreStrs);
                        if StrOK then
                        begin
                          FrameLeft := realValue;
                                                                           // read the FrameTopOrBot parameter
                          StrOK := ReadDDECommandReal(DDEHandler.DDEData^.At(0), 12, 1, WgBlockStart,
                            DDEHandler.FDDESepSign, WgBlockEnd, realValue, StrFound, MoreStrs);
                          if StrOK then
                          begin
                            FrameTopOrBot := realValue;
                            if WPrinters.Printers.Count > 0 then
                            begin
                              PrintHandler := FuncCreatePrintHandler(AMenuHandler, AData);
                              if PrintHandler <> nil then
                                intErrorCode := PrintHandler.SetSigParamFromDB(SignatureID, Signature,
                                  FontName, Italic, Bold, Underlined, Transparent,
                                  FontSize, FontColor, FontBackColor, TextAlignment, OnPagesLocation,
                                  OnFirstPage, OnInternalPages, OnLastPage, FrameCentering,
                                  FrameOridgin, FrameLeft, FrameTopOrBot)
                              else
                                intErrorCode := 18; // WinGIS internal ERROR
                              if AMenuHandler <> nil then
                                AMenuHandler.Free;
                            end
                            else
                              intErrorCode := 17; // there is no printers istalled in system
                          end
                          else
                            intErrorCode := 12 // value of the FrameTopOrBot parameter is invalid
                        end
                        else
                          intErrorCode := 12 // value of the FrameLeft parameter is invalid
                      end
                      else
                        intErrorCode := 11 // value of the FrameOridgin parameter is invalid
                    end
                    else
                      intErrorCode := 10 // value of the FrameCentering parameter is invalid
                  end
                  else
                    intErrorCode := 9; // values of the OnPagesLocation parameters are invalid
                end
                else
                  intErrorCode := 8; // value of the OnPagesLocation parameter is invalid
              end
              else
                intErrorCode := 7; // value of the TextAlignment parameter is invalid
            end
            else
              intErrorCode := 6; // values of the FontColor parameters are invalid
          end
          else
            intErrorCode := 5; // value of the FontSize parameter is invalid
        end
        else
          intErrorCode := 4; // values of the FontStyle parameters are invalid
      end
      else
        intErrorCode := 3; // value of the FontName parameter is invalid
    end
    else
      intErrorCode := 2; // value of the Signature parameter is invalid
  end
  else
    intErrorCode := 1; // value of the SignatureID parameter is invalid

  if DDEHandler.DDEData^.GetCount > 0 then
    PChar(DDEHandler.DDEData^.At(0))[0] := DDEStrUsed;
  DDEHandler.DeleteDDEData;

  ProcSendPrintResultToDB('PRNSS', intErrorCode);

end;

procedure ProcInsSignatureFromDB(AData: PProj);
var
  AMenuHandler: TMenuHandler;
  PrintHandler: TPrintHandler;
  StrOK: Boolean;
  StrFound: Boolean;
  MoreStrs: Boolean;
  intValue: Integer;
  realValue: Real;
  intErrorCode: Integer;
  Signature: string;
  FontName: string;
  Italic: Boolean;
  Bold: Boolean;
  Underlined: Boolean;
  Transparent: Boolean;
  FontSize: Integer;
  FontColor: Integer;
  FontBackColor: Integer;
  TextAlignment: Integer;
  OnPagesLocation: Boolean;
  OnFirstPage: Boolean;
  OnInternalPages: Boolean;
  OnLastPage: Boolean;
  FrameCentering: Integer;
  FrameOridgin: Integer;
  FrameLeft: Double;
  FrameTopOrBot: Double;
begin
   // read the Signature parameter
  StrOK := ReadDDECommandString(DDEHandler.DDEData^.At(0), 1, 0, WgBlockStart,
    DDEHandler.FDDESepSign, WgBlockEnd, Signature, StrFound, MoreStrs);
  StrOK := StrOK and (Signature <> ''); // the value is valid

  if StrOK then
  begin
         // read the FontName parameter
    StrOK := ReadDDECommandString(DDEHandler.DDEData^.At(0), 2, 0, WgBlockStart,
      DDEHandler.FDDESepSign, WgBlockEnd, FontName, StrFound, MoreStrs);
    StrOK := StrOK and (FontName <> ''); // the value is valid
    if StrOK then
    begin
               // read the Italic parameter
      StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 3, 0, WgBlockStart,
        DDEHandler.FDDESepSign, WgBlockEnd, intValue, StrFound, MoreStrs);
      StrOK := StrOK and ((intValue = 0) or (intValue = 1)); // the value is valid
      if StrOK then
      begin
        Italic := (intValue = 1);
                     // read the Bold parameter
        StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 3, 1, WgBlockStart,
          DDEHandler.FDDESepSign, WgBlockEnd, intValue, StrFound, MoreStrs);
        StrOK := StrOK and ((intValue = 0) or (intValue = 1)); // the value is valid
        if StrOK then
        begin
          Bold := (intValue = 1);
                           // read the Underlined parameter
          StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 3, 2, WgBlockStart,
            DDEHandler.FDDESepSign, WgBlockEnd, intValue, StrFound, MoreStrs);
          StrOK := StrOK and ((intValue = 0) or (intValue = 1)); // the value is valid
          if StrOK then
          begin
            Underlined := (intValue = 1);
                                 // read the Transparent parameter
            StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 3, 3, WgBlockStart,
              DDEHandler.FDDESepSign, WgBlockEnd, intValue, StrFound, MoreStrs);
            StrOK := StrOK and ((intValue = 0) or (intValue = 1)); // the value is valid
          end;
        end;
      end;
      if StrOK then
      begin
        Transparent := (intValue = 1);
                     // read the FontSize parameter
        StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 4, 0, WgBlockStart,
          DDEHandler.FDDESepSign, WgBlockEnd, FontSize, StrFound, MoreStrs);
        StrOK := StrOK and (FontSize > 0); // the value is valid
        if StrOK then
        begin
                           // read the FontColor parameter
          StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 5, 0, WgBlockStart,
            DDEHandler.FDDESepSign, WgBlockEnd, FontColor, StrFound, MoreStrs);
          StrOK := StrOK and (FontColor >= 0); // the value is valid
          if StrOK then
          begin
                                 // read the FontBackColor parameter
            StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 5, 1, WgBlockStart,
              DDEHandler.FDDESepSign, WgBlockEnd, FontBackColor, StrFound, MoreStrs);
            StrOK := StrOK and (FontBackColor >= 0); // the value is valid
          end;
          if StrOK then
          begin
                                 // read the TextAlignment parameter
            StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 6, 0, WgBlockStart,
              DDEHandler.FDDESepSign, WgBlockEnd, TextAlignment, StrFound, MoreStrs);
            StrOK := StrOK and (TextAlignment >= 0) and (TextAlignment < 3); // the value is valid
            if StrOK then
            begin
                                       // read the OnPagesLocation parameter
              StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 7, 0, WgBlockStart,
                DDEHandler.FDDESepSign, WgBlockEnd, intValue, StrFound, MoreStrs);
              StrOK := StrOK and ((intValue = 0) or (intValue = 1)); // the value is valid
              if StrOK then
              begin
                OnPagesLocation := (intValue = 1);
                                             // read the OnFirstPage parameter
                StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 8, 0, WgBlockStart,
                  DDEHandler.FDDESepSign, WgBlockEnd, intValue, StrFound, MoreStrs);
                StrOK := StrOK and ((intValue = 0) or (intValue = 1)); // the value is valid
                if StrOK then
                begin
                  OnFirstPage := (intValue = 1);
                                                   // read the OnInternalPages parameter
                  StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 8, 1, WgBlockStart,
                    DDEHandler.FDDESepSign, WgBlockEnd, intValue, StrFound, MoreStrs);
                  StrOK := StrOK and ((intValue = 0) or (intValue = 1)); // the value is valid
                  if StrOK then
                  begin
                    OnInternalPages := (intValue = 1);
                                                         // read the OnLastPage parameter
                    StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 8, 2, WgBlockStart,
                      DDEHandler.FDDESepSign, WgBlockEnd, intValue, StrFound, MoreStrs);
                    StrOK := StrOK and ((intValue = 0) or (intValue = 1)); // the value is valid
                  end;
                end;
                if StrOK then
                begin
                  OnLastPage := (intValue = 1);
                                                   // read the FrameCentering parameter
                  StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 9, 0, WgBlockStart,
                    DDEHandler.FDDESepSign, WgBlockEnd, FrameCentering, StrFound, MoreStrs);
                  StrOK := StrOK and (FrameCentering >= 0) and (FrameCentering < 4); // the value is valid
                  if StrOK then
                  begin
                                                         // read the FrameOridgin parameter
                    StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 10, 0, WgBlockStart,
                      DDEHandler.FDDESepSign, WgBlockEnd, FrameOridgin, StrFound, MoreStrs);
                    StrOK := StrOK and (FrameOridgin >= 0) and (FrameOridgin < 2); // the value is valid
                    if StrOK then
                    begin
                                                               // read the FrameLeft parameter
                      StrOK := ReadDDECommandReal(DDEHandler.DDEData^.At(0), 11, 0, WgBlockStart,
                        DDEHandler.FDDESepSign, WgBlockEnd, realValue, StrFound, MoreStrs);
                      if StrOK then
                      begin
                        FrameLeft := realValue;
                                                                     // read the FrameTopOrBot parameter
                        StrOK := ReadDDECommandReal(DDEHandler.DDEData^.At(0), 11, 1, WgBlockStart,
                          DDEHandler.FDDESepSign, WgBlockEnd, realValue, StrFound, MoreStrs);
                        if StrOK then
                        begin
                          FrameTopOrBot := realValue;
                          if WPrinters.Printers.Count > 0 then
                          begin
                            PrintHandler := FuncCreatePrintHandler(AMenuHandler, AData);
                            if PrintHandler <> nil then
                              intErrorCode := PrintHandler.InsertSignatureFromDB(Signature, FontName,
                                Italic, Bold, Underlined, Transparent, FontSize,
                                FontColor, FontBackColor, TextAlignment, OnPagesLocation,
                                OnFirstPage, OnInternalPages, OnLastPage, FrameCentering,
                                FrameOridgin, FrameLeft, FrameTopOrBot)
                            else
                              intErrorCode := 18; // WinGIS internal ERROR
                            if AMenuHandler <> nil then
                              AMenuHandler.Free;
                          end
                          else
                            intErrorCode := 17; // there is no printers istalled in system
                        end
                        else
                          intErrorCode := 11; // value of the FrameTopOrBot parameter is invalid
                      end
                      else
                        intErrorCode := 11; // value of the FrameLeft parameter is invalid
                    end
                    else
                      intErrorCode := 10; // value of the FrameOridgin parameter is invalid
                  end
                  else
                    intErrorCode := 9 // value of the FrameCentering parameter is invalid
                end
                else
                  intErrorCode := 8; // values of the OnPagesLocation parameters are invalid
              end
              else
                intErrorCode := 7; // value of the OnPagesLocation parameter is invalid
            end
            else
              intErrorCode := 6; // value of the TextAlignment parameter is invalid
          end
          else
            intErrorCode := 5; // values of the FontColor parameters are invalid
        end
        else
          intErrorCode := 4; // value of the FontSize parameter is invalid
      end
      else
        intErrorCode := 3; // values of the FontStyle parameters are invalid
    end
    else
      intErrorCode := 2; // value of the FontName parameter is invalid
  end
  else
    intErrorCode := 1; // value of the Signature parameter is invalid

  if DDEHandler.DDEData^.GetCount > 0 then
    PChar(DDEHandler.DDEData^.At(0))[0] := DDEStrUsed;
  DDEHandler.DeleteDDEData;

  ProcSendPrintResultToDB('PRNIS', intErrorCode);

end;

procedure ProcGetSigParamFromDB(AData: PProj);
var
  AMenuHandler: TMenuHandler;
  PrintHandler: TPrintHandler;
  StrOK: Boolean;
  StrFound: Boolean;
  MoreStrs: Boolean;
  intErrorCode: Integer;
  ResultString: string;
  strValue: string;
  SignatureID: Integer;
  Signature: string;
  FontName: string;
  Italic: Boolean;
  Bold: Boolean;
  Underlined: Boolean;
  Transparent: Boolean;
  FontSize: Integer;
  FontColor: Integer;
  FontBackColor: Integer;
  TextAlignment: Integer;
  OnPagesLocation: Boolean;
  OnFirstPage: Boolean;
  OnInternalPages: Boolean;
  OnLastPage: Boolean;
  FrameCentering: Integer;
  FrameOridgin: Integer;
  FrameLeft: Double;
  FrameTopOrBot: Double;
begin
  SignatureID := 0;
  Signature := '';
  FontName := '';
  Italic := FALSE;
  Bold := FALSE;
  Underlined := FALSE;
  Transparent := FALSE;
  FontSize := 0;
  FontColor := 0;
  FontBackColor := 0;
  TextAlignment := 0;
  OnPagesLocation := FALSE;
  OnFirstPage := FALSE;
  OnInternalPages := FALSE;
  OnLastPage := FALSE;
  FrameCentering := 0;
  FrameOridgin := 0;
  FrameLeft := 0.0;
  FrameTopOrBot := 0.0;

   // read the SignatureID parameter
  StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 1, 0, WgBlockStart,
    DDEHandler.FDDESepSign, WgBlockEnd, SignatureID, StrFound, MoreStrs);
  StrOK := StrOK and (SignatureID >= 0); // the value is valid
  if StrOK then
    if WPrinters.Printers.Count > 0 then
    begin
      PrintHandler := FuncCreatePrintHandler(AMenuHandler, AData);
      if PrintHandler <> nil then
        intErrorCode := PrintHandler.GetSigParamFromDB(SignatureID, Signature, FontName,
          Italic, Bold, Underlined, Transparent, FontSize,
          FontColor, FontBackColor, TextAlignment, OnPagesLocation,
          OnFirstPage, OnInternalPages, OnLastPage, FrameCentering,
          FrameOridgin, FrameLeft, FrameTopOrBot)
      else
        intErrorCode := 18; // WinGIS internal ERROR
      if AMenuHandler <> nil then
        AMenuHandler.Free;
    end
    else
      intErrorCode := 17 // there is no printers istalled in system
  else
    intErrorCode := 1; // value of the SignatureID parameter is invalid

  if DDEHandler.DDEData^.GetCount > 0 then
    PChar(DDEHandler.DDEData^.At(0))[0] := DDEStrUsed;
  DDEHandler.DeleteDDEData;

  DDEHandler.SetAnswerMode(TRUE);
  ResultString := WgBlockStart + 'PRNSS' + WgBlockEnd;

  ResultString := ResultString + WgBlockStart + IntToStr(SignatureID) + WgBlockEnd;
  ResultString := ResultString + WgBlockStart + Signature + WgBlockEnd;
  ResultString := ResultString + WgBlockStart + FontName + WgBlockEnd;
  if Italic then
    strValue := '1'
  else
    strValue := '0';
  ResultString := ResultString + WgBlockStart + strValue + DDEHandler.FDDESepSign;
  if Bold then
    strValue := '1'
  else
    strValue := '0';
  ResultString := ResultString + strValue + DDEHandler.FDDESepSign;
  if Underlined then
    strValue := '1'
  else
    strValue := '0';
  ResultString := ResultString + strValue + DDEHandler.FDDESepSign;
  if Transparent then
    strValue := '1'
  else
    strValue := '0';
  ResultString := ResultString + strValue + WgBlockEnd;
  ResultString := ResultString + WgBlockStart + IntToStr(FontSize) + WgBlockEnd;
  ResultString := ResultString + WgBlockStart + IntToStr(FontColor) + DDEHandler.FDDESepSign;
  ResultString := ResultString + IntToStr(FontBackColor) + WgBlockEnd;
  ResultString := ResultString + WgBlockStart + IntToStr(TextAlignment) + WgBlockEnd;
  if OnPagesLocation then
    strValue := '1'
  else
    strValue := '0';
  ResultString := ResultString + WgBlockStart + strValue + WgBlockEnd;
  if OnFirstPage then
    strValue := '1'
  else
    strValue := '0';
  ResultString := ResultString + WgBlockStart + strValue + DDEHandler.FDDESepSign;
  if OnInternalPages then
    strValue := '1'
  else
    strValue := '0';
  ResultString := ResultString + strValue + DDEHandler.FDDESepSign;
  if OnLastPage then
    strValue := '1'
  else
    strValue := '0';
  ResultString := ResultString + strValue + WgBlockEnd;

  ResultString := ResultString + WgBlockStart + IntToStr(FrameCentering) + WgBlockEnd;
  ResultString := ResultString + WgBlockStart + IntToStr(FrameOridgin) + WgBlockEnd;
  ResultString := ResultString + WgBlockStart + FloatToStr(FrameLeft) + DDEHandler.FDDESepSign;
  ResultString := ResultString + FloatToStr(FrameTopOrBot) + WgBlockEnd;

  ResultString := ResultString + WgBlockStart + IntToStr(intErrorCode) + WgBlockEnd;
  DDEHandler.SendString(ResultString);
  DDEHandler.SetAnswerMode(FALSE);
end;

procedure ProcStoreTemplateFromDB(AData: PProj);
var
  AMenuHandler: TMenuHandler;
  PrintHandler: TPrintHandler;
  StrOK: Boolean;
  StrFound: Boolean;
  MoreStrs: Boolean;
  intErrorCode: Integer;
  ResultString: string;
  FileName: string;
  Options: Integer;
begin
   // read the FileName parameter
  StrOK := ReadDDECommandString(DDEHandler.DDEData^.At(0), 1, 0, WgBlockStart,
    DDEHandler.FDDESepSign, WgBlockEnd, FileName, StrFound, MoreStrs);
  StrOK := StrOK and (FileName <> ''); // the value is valid

  if StrOK then
  begin
         // read the Options parameter
    StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 2, 0, WgBlockStart,
      DDEHandler.FDDESepSign, WgBlockEnd, Options, StrFound, MoreStrs);
    StrOK := StrOK and (Options >= 0) and (Options <= 1); // the value is valid
    if StrOK then
      if WPrinters.Printers.Count > 0 then
      begin
        PrintHandler := FuncCreatePrintHandler(AMenuHandler, AData);
        if PrintHandler <> nil then
          intErrorCode := PrintHandler.StoreTemplateFromDB(FileName, Options)
        else
          intErrorCode := 18; // WinGIS internal ERROR
        if AMenuHandler <> nil then
          AMenuHandler.Free;
      end
      else
        intErrorCode := 17 // there is no printers istalled in system
    else
      intErrorCode := 2; // value of the Options parameter is invalid
  end
  else
    intErrorCode := 1; // value of the FileName parameter is invalid

  if DDEHandler.DDEData^.GetCount > 0 then
    PChar(DDEHandler.DDEData^.At(0))[0] := DDEStrUsed;
  DDEHandler.DeleteDDEData;

  ProcSendPrintResultToDB('PRNST', intErrorCode);
end;

procedure ProcLoadTemplateFromDB(AData: PProj);
var
  AMenuHandler: TMenuHandler;
  PrintHandler: TPrintHandler;
  StrOK: Boolean;
  StrFound: Boolean;
  MoreStrs: Boolean;
  intErrorCode: Integer;
  ResultString: string;
  FileName: string;
  Options: Integer;
begin
   // read the FileName parameter
  StrOK := ReadDDECommandString(DDEHandler.DDEData^.At(0), 1, 0, WgBlockStart,
    DDEHandler.FDDESepSign, WgBlockEnd, FileName, StrFound, MoreStrs);
  StrOK := StrOK and (FileName <> ''); // the value is valid

  if StrOK then
    if WPrinters.Printers.Count > 0 then
    begin
      PrintHandler := FuncCreatePrintHandler(AMenuHandler, AData);
      if PrintHandler <> nil then
        intErrorCode := PrintHandler.LoadTemplateFromDB(FileName)
      else
        intErrorCode := 18; // WinGIS internal ERROR
      if AMenuHandler <> nil then
        AMenuHandler.Free;
    end
    else
      intErrorCode := 17 // there is no printers istalled in system
  else
    intErrorCode := 1; // value of the FileName parameter is invalid

  if DDEHandler.DDEData^.GetCount > 0 then
    PChar(DDEHandler.DDEData^.At(0))[0] := DDEStrUsed;
  DDEHandler.DeleteDDEData;

  ProcSendPrintResultToDB('PRNRT', intErrorCode);
end;

procedure ProcGetObjInformFromDB(AData: PProj);
var
  AMenuHandler: TMenuHandler;
  PrintHandler: TPrintHandler;
  StrOK: Boolean;
  StrFound: Boolean;
  MoreStrs: Boolean;
  intErrorCode: Integer;
  ResultString: string;
  ObjectsNumber: Integer;
  TypesList: TStringList;
  strTypesList: string;
  Cnt: Integer;
begin
  strTypesList := '';
  TypesList := nil;
  if WPrinters.Printers.Count > 0 then
  begin
    PrintHandler := FuncCreatePrintHandler(AMenuHandler, AData);
    if PrintHandler <> nil then
      intErrorCode := PrintHandler.GetObjInformFromDB(TypesList)
    else
      intErrorCode := 18; // WinGIS internal ERROR
    if AMenuHandler <> nil then
      AMenuHandler.Free;
  end
  else
    intErrorCode := 17; // there is no printers istalled in system

  if DDEHandler.DDEData^.GetCount > 0 then
    PChar(DDEHandler.DDEData^.At(0))[0] := DDEStrUsed;
  DDEHandler.DeleteDDEData;

  DDEHandler.SetAnswerMode(TRUE);
  ResultString := WgBlockStart + 'PRNSOI' + WgBlockEnd;

  if TypesList <> nil then
    with TypesList do
    begin
      ObjectsNumber := Count;
      if Count > 0 then
        strTypesList := Strings[0];
      for Cnt := 1 to Count - 1 do
        strTypesList := strTypesList + DDEHandler.FDDESepSign + Strings[Cnt];
      Free;
    end
  else
    ObjectsNumber := 0;

  ResultString := ResultString + WgBlockStart + IntToStr(ObjectsNumber) + WgBlockEnd;
  ResultString := ResultString + WgBlockStart + strTypesList + WgBlockEnd;

  ResultString := ResultString + WgBlockStart + IntToStr(intErrorCode) + WgBlockEnd;
  DDEHandler.SendString(ResultString);
  DDEHandler.SetAnswerMode(FALSE);
end;

procedure ProcDeleteObjectFromDB(AData: PProj);
var
  AMenuHandler: TMenuHandler;
  PrintHandler: TPrintHandler;
  StrOK: Boolean;
  StrFound: Boolean;
  MoreStrs: Boolean;
  intErrorCode: Integer;
  ObjectType: Integer;
  ObjectID: Integer;
begin
   // read the ObjectType parameter
  StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 1, 0, WgBlockStart,
    DDEHandler.FDDESepSign, WgBlockEnd, ObjectType, StrFound, MoreStrs);
  StrOK := StrOK and (ObjectType >= 0) and (ObjectType < 4); // the value is valid
  if StrOK then
  begin
         // read the ObjectID parameter
    StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 2, 0, WgBlockStart,
      DDEHandler.FDDESepSign, WgBlockEnd, ObjectID, StrFound, MoreStrs);
    StrOK := StrOK and (ObjectID >= 0); // the value is valid
    if StrOK then
      if WPrinters.Printers.Count > 0 then
      begin
        PrintHandler := FuncCreatePrintHandler(AMenuHandler, AData);
        if PrintHandler <> nil then
          intErrorCode := PrintHandler.DeleteObjectFromDB(ObjectType, ObjectID)
        else
          intErrorCode := 18; // WinGIS internal ERROR
        if AMenuHandler <> nil then
          AMenuHandler.Free;
      end
      else
        intErrorCode := 17 // there is no printers istalled in system
    else
      intErrorCode := 2; // value of the ObjectID parameter is invalid
  end
  else
    intErrorCode := 1; // value of the ObjectType parameter is invalid

  if DDEHandler.DDEData^.GetCount > 0 then
    PChar(DDEHandler.DDEData^.At(0))[0] := DDEStrUsed;
  DDEHandler.DeleteDDEData;

  ProcSendPrintResultToDB('PRNDO', intErrorCode);
end;

procedure ProcMoveObjectFromDB(AData: PProj);
var
  AMenuHandler: TMenuHandler;
  PrintHandler: TPrintHandler;
  StrOK: Boolean;
  StrFound: Boolean;
  MoreStrs: Boolean;
  intErrorCode: Integer;
  ObjectType: Integer;
  ObjectID: Integer;
  Options: Integer;
begin
   // read the ObjectType parameter
  StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 1, 0, WgBlockStart,
    DDEHandler.FDDESepSign, WgBlockEnd, ObjectType, StrFound, MoreStrs);
  StrOK := StrOK and (ObjectType >= 0) and (ObjectType < 4); // the value is valid
  if StrOK then
  begin
         // read the ObjectID parameter
    StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 2, 0, WgBlockStart,
      DDEHandler.FDDESepSign, WgBlockEnd, ObjectID, StrFound, MoreStrs);
    StrOK := StrOK and (ObjectID >= 0); // the value is valid
    if StrOK then
    begin
               // read the Options parameter
      StrOK := ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 3, 0, WgBlockStart,
        DDEHandler.FDDESepSign, WgBlockEnd, Options, StrFound, MoreStrs);
      StrOK := StrOK and (Options >= 0) and (Options < 4); // the value is valid
      if StrOK then
        if WPrinters.Printers.Count > 0 then
        begin
          PrintHandler := FuncCreatePrintHandler(AMenuHandler, AData);
          if PrintHandler <> nil then
            intErrorCode := PrintHandler.MoveObjectFromDB(ObjectType, ObjectID, Options)
          else
            intErrorCode := 18; // WinGIS internal ERROR
          if AMenuHandler <> nil then
            AMenuHandler.Free;
        end
        else
          intErrorCode := 17 // there is no printers istalled in system
      else
        intErrorCode := 3; // value of the Options parameter is invalid
    end
  end
  else
    intErrorCode := 1; // value of the ObjectType parameter is invalid

  if DDEHandler.DDEData^.GetCount > 0 then
    PChar(DDEHandler.DDEData^.At(0))[0] := DDEStrUsed;
  DDEHandler.DeleteDDEData;

  ProcSendPrintResultToDB('PRNMO', intErrorCode);
end;
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 03.11.00}

procedure ProcSendDistanceToDB(ADistance: Double);
var
  ValInt: LongInt;
  TempString: string;
begin
  ValInt := Round(ADistance * 100);
  Str(ValInt: 0, TempString);
  DDEHandler.SendString('[GSD][' + TempString + ']');
end;

procedure ProcSendObjPropertiesToDB
  (
  AData: PProj
  );

  procedure DoAll
      (
      Item: PChar
      ); far;
  var
    AMode: LongInt;
    ResLInt: LongInt;
    ResString: string;
    SFound: Boolean;
    MStrs: Boolean;
    OldTop: PLayer;
    OCount: LongInt;
    Count: Integer;
    ALayer: PLayer;
    i: Integer;
    SItem: PIndex;
    AObject: PIndex;

    procedure DoAll1
        (
        Item1: PIndex
        ); far;
    begin
      ProcDBSendObject(AData, Item1, 'OBP');
    end;

    procedure DoAll2
        (
        Item2: PLayer
        ); far;

      procedure DoAll20
          (
          Item20: PIndex
          ); far;
      begin
        ProcDBSendObject(AData, Item20, 'OBP');
      end;
    begin
      if (Item2^.Index > 0) and not Item2^.GetState(sf_LayerOff) then
      begin
        AData^.Layers^.TopLayer := Item2;
        Item2^.Data^.ForEach(@DoAll20);
      end;
    end;
  begin
    if ReadDDECommandLongInt(Item, 1, 0, WgBlockStart, WgBlockEnd, WgBlockEnd, AMode, SFound, MStrs) then
    begin
      case AMode of
        0:
          begin
            OldTop := AData^.Layers^.TopLayer;
            SItem := New(PIndex, Init(0));
            if ReadDDECommandLongInt(Item, 2, 0, WgBlockStart, WgBlockEnd, WgBlockEnd,
              OCount, SFound, MStrs) then
            begin
              for Count := 0 to OCount - 1 do
              begin
                if ReadDDECommandLongInt(Item, Count + 3, 0, WgBlockStart, WgBlockEnd, WgBlockEnd,
                  ResLInt, SFound, MStrs) then
                begin
                  if SFound and (ResLInt > 0) then
                  begin
                    SItem^.Index := ResLInt;
                    AObject := AData^.Layers^.TopLayer^.HasObject(SItem);
                    if AObject = nil then
                    begin
                      i := 0;
                      while (AObject = nil) and (i < AData^.Layers^.LData^.Count) do
                      begin
                        AData^.Layers^.TopLayer := AData^.Layers^.LData^.At(i);
                        AObject := AData^.Layers^.TopLayer^.HasObject(SItem);
                        i := i + 1;
                      end;
                    end;
                    if AObject <> nil then
                      ProcDBSendObject(AData, AObject, 'OBP');
                  end;
                end;
              end;
            end;
            Dispose(SItem, Done);
            AData^.Layers^.TopLayer := OldTop;
          end;
        1:
          begin
            OldTop := AData^.Layers^.TopLayer;
            if ReadDDECommandLongInt(Item, 2, 0, WgBlockStart, WgBlockEnd, WgBlockEnd,
              OCount, SFound, MStrs) then
            begin
              for Count := 0 to OCount - 1 do
              begin
                ALayer := nil;
                if (DDEHandler.FGotLInfo = 0) or (DDEHandler.FGotLInfo = 1) then
                begin
                  ResString := '';
                  if ReadDDECommandString(Item, Count + 3, 0, WgBlockStart, WgBlockEnd, WgBlockEnd,
                    ResString, SFound, MStrs) then
                  begin
                    if {SFound and}(ResString <> '') then
                      ALayer := AData^.Layers^.NameToLayer(ResString);
                  end;
                end
                else
                  if DDEHandler.FGotLInfo = 2 then
                  begin
                    ResLInt := 0;
                    if ReadDDECommandLongInt(Item, Count + 3, 0, WgBlockStart, WgBlockEnd, WgBlockEnd,
                      ResLInt, SFound, MStrs) then
                    begin
                      if {SFound and}(ResLInt <> 0) then
                        ALayer := AData^.Layers^.IndexToLayer(ResLInt);
                    end;
                  end;
                if ALayer <> nil then
                begin
                  AData^.Layers^.TopLayer := ALayer;
                  ALayer^.Data^.ForEach(@DoAll1);
                end;
              end;
            end;
            AData^.Layers^.TopLayer := OldTop;
          end;
        2:
          begin
            OldTop := AData^.Layers^.TopLayer;
            AData^.Layers^.LData^.ForEach(@DoAll2);
            AData^.Layers^.TopLayer := OldTop;
          end;
      end;
    end;
    Item[0] := DDEStrUsed;
  end;
begin
  DDEHandler.DDEData^.ForEach(@DoAll);
  DDEHandler.SendString('[END][]');
  DDEHandler.DeleteDDEData;
end;

procedure ProcSetActSymbol
  (
  AData: PProj;
  SymID: Integer;
  IDFound: Boolean;
  SymName: string;
  SymNameFound: Boolean;
  LibName: string;
  LibNameFound: Boolean
  );
var
  SymNr: LongInt;
  LibSymName: string;
  LookForName: Boolean;
  FoundSym: PSGroup;
  SymRollUp: TSymbolRollupForm;
  AExtLib: PExtLib;
  ChangeLib: Boolean;
  OldLib: string;
  ChangeLibOK: Boolean;

  procedure SendSym(Item: PSGroup);
  var
    SendStr, TmpStr: string;
  begin
    SendStr := '[TSY][' + IntToStr(Item^.Index - 600000000) + '][' + Item^.Name^ + '][]';

    if PSymbol(Item)^.Size < 1.5990221981798E-313 then
      TmpStr := '-1'
    else
      TmpStr := FloatToStr(PSymbol(Item)^.Size);
    SendStr := SendStr + '[' + TmpStr + ']';

    if PSymbol(Item)^.ObjectStyle <> nil then
    begin
      TmpStr := IntToStr(PSymbol(Item)^.ObjectStyle^.FillStyle.Pattern);
      SendStr := SendStr + '[' + TmpStr + ']';
      TmpStr := IntToStr(PSymbol(Item)^.ObjectStyle^.FillStyle.ForeColor);
      SendStr := SendStr + '[' + TmpStr + ']';
      TmpStr := IntToStr(PSymbol(Item)^.ObjectStyle^.FillStyle.BackColor);
      SendStr := SendStr + '[' + TmpStr + ']';
      TmpStr := IntToStr(PSymbol(Item)^.ObjectStyle^.LineStyle.Style);
      SendStr := SendStr + '[' + TmpStr + ']';
      TmpStr := IntToStr(PSymbol(Item)^.ObjectStyle^.LineStyle.Color);
      SendStr := SendStr + '[' + TmpStr + ']';
    end
    else
      SendStr := SendStr + '[-1][-1][-1][-1][-1]';
    DDEHandler.SendString(SendStr);
    DDEHandler.SendString('[END][]');
  end;

  function SearchSymNumber
      (
      SItem: PSGroup
      )
      : Boolean; far;
  begin
    if SItem^.Index = SymNr then
      Result := TRUE
    else
      Result := FALSE;
  end;

  function SearchSymName
      (
      SItem: PSGroup
      )
      : Boolean; far;
  begin
    if SItem^.Name^ = LibSymName then
      Result := TRUE
    else
      Result := FALSE;
  end;

begin
  SymNr := 0;
  LibSymName := '';
  LookForName := FALSE;
  if IDFound then
  begin
    SymNr := SymID + 600000000;
    FoundSym := PSGroup(PSymbols(AData^.PInfo^.Symbols)^.Data^.FirstThat(@SearchSymNumber));
    if FoundSym <> nil then
    begin
      SymRollUp := TSymbolRollupForm(UserInterface.RollupNamed['SymbolRollupForm']);
      if SymRollUp <> nil then
        SymRollUp.SetActSymbol(FoundSym)
      else
        AData^.ActualSym := FoundSym;
    end
    else
      LookForName := TRUE;
  end
  else
    LookForName := TRUE;
  if LookForName then
  begin
    ChangeLib := TRUE;
    OldLib := AData^.ActSymLib;
    if LibNameFound then
    begin
      LibSymName := LibName;
      ChangeLib := FALSE;
      if LibSymName <> '' then
      begin
        LibSymName := AnsiLowerCase(LibSymName);
        if AData^.ActSymLib <> '' then
        begin
          AExtLib := PSymbols(AData^.PInfo^.Symbols)^.GetExtLib(AData^.ActSymLib);
          if AExtLib <> nil then
            OldLib := AExtLib^.LibFileName^
          else
            OldLib := '';
        end
        else
        begin
          AExtLib := nil;
          OldLib := '';
        end;
        if AExtLib <> nil then
        begin
          if AnsiLowerCase(AExtLib^.LibFileName^) <> LibSymName then
            ChangeLib := TRUE
          else
            ChangeLib := FALSE;
        end
        else
          ChangeLib := TRUE;
      end
      else
      begin
        if LibSymName <> AData^.ActSymLib then
          ChangeLib := TRUE
        else
          ChangeLib := FALSE;
      end;
      if ChangeLib then
      begin
        ChangeLibOK := FALSE;
        if (LibSymName = '') or FileExists(LibSymName) then
        begin
          SymRollUp := TSymbolRollupForm(UserInterface.RollupNamed['SymbolRollupForm']);
          if SymRollUp <> nil then
          begin
            AExtLib := PSymbols(AData^.PInfo^.Symbols)^.GetExtLib(ExtractFileName(LibSymName));
            if AExtLib <> nil then
            begin
              if AnsiLowerCase(AExtLib^.LibFileName^) = LibSymName then
              begin
                ChangeLibOK := SymRollUp.SetActLib(LibSymName);
              end
              else
              begin
                SymRollUp.CloseSymLib(ExtractFileName(LibSymName));
                ChangeLibOK := SymRollUp.OpenSymLib(LibSymName, FALSE);
              end;
            end
            else
            begin
              if LibSymName = '' then
                ChangeLibOK := SymRollUp.SetActLib(LibSymName)
              else
                ChangeLibOK := SymRollUp.OpenSymLib(LibSymName, FALSE);
            end;
          end;
        end;
      end
      else
        ChangeLibOK := TRUE;
    end
    else
      ChangeLibOK := TRUE;
    if ChangeLibOK then
    begin
      if SymNameFound then
      begin
        LibSymName := SymName;
        if AData^.ActSymLib = '' then
          FoundSym := PSGroup(PSymbols(AData^.PInfo^.Symbols)^.Data^.FirstThat(@SearchSymName))
        else
        begin
          AExtLib := PSymbols(AData^.PInfo^.Symbols)^.GetExtLib(AData^.ActSymLib);
          FoundSym := PSGroup(AExtLib^.Symbols^.Data^.FirstThat(@SearchSymName))
        end;
        if FoundSym <> nil then
        begin
          SymRollUp := TSymbolRollupForm(UserInterface.RollupNamed['SymbolRollupForm']);
          if SymRollUp <> nil then
            SymRollUp.SetActSymbol(FoundSym)
          else
            AData^.ActualSym := FoundSym;
        end
        else
          ChangeLibOK := FALSE;
      end
      else
        ChangeLibOK := FALSE;
    end;
    if not ChangeLibOK then
    begin
      if OldLib <> AData^.ActSymLib then
      begin
        SymRollUp := TSymbolRollupForm(UserInterface.RollupNamed['SymbolRollupForm']);
        if SymRollUp <> nil then
        begin
          if OldLib = '' then
            SymRollUp.SetActLib(OldLib)
          else
          begin
            AExtLib := PSymbols(AData^.PInfo^.Symbols)^.GetExtLib(OldLib);
            if AExtLib <> nil then
            begin
              if AnsiLowerCase(AExtLib^.LibFileName^) = AnsiLowerCase(OldLib) then
              begin
                SymRollUp.SetActLib(ExtractFileName(OldLib));
              end
              else
              begin
                SymRollUp.CloseSymLib(ExtractFileName(OldLib));
                ChangeLibOK := SymRollUp.OpenSymLib(OldLib, FALSE);
              end;
            end
            else
              SymRollUp.OpenSymLib(OldLib, FALSE);
          end;
        end;
      end;
    end;
  end;
  if FoundSym <> nil then
  begin
    SendSym(FoundSym);
  end;
end;

procedure ProcDBSetActSymbol
  (
  AData: PProj
  );
var
  SymNr: Integer;
  SymNrFound: Boolean;
  SymName: string;
  SymNameFound: Boolean;
  LibName: string;
  LibNameFound: Boolean;
  SFound: Boolean;
  MStrs: Boolean;
  StrToLong: Boolean;
begin
  if ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 1, 0, WgBlockStart, WgBlockEnd,
    WgBlockEnd, SymNr, SFound, MStrs) then
  begin
    if SFound and (SymNr > 0) then
      SymNrFound := TRUE
    else
      SymNrFound := FALSE;
  end
  else
    SymNrFound := FALSE;
  if ReadDDECommandString(DDEHandler.DDEData^.At(0), 2, 0, WgBlockStart,
    WgBlockEnd, WgBlockEnd, SymName, StrToLong, MStrs) then
  begin
    if SymName <> '' then
      SymNameFound := TRUE
    else
      SymNameFound := FALSE;
  end
  else
    SymNameFound := FALSE;
  if ReadDDECommandString(DDEHandler.DDEData^.At(0), 3, 0, WgBlockStart,
    WgBlockEnd, WgBlockEnd, LibName, StrToLong, MStrs) then
  begin
    LibNameFound := TRUE;
  end
  else
    LibNameFound := FALSE;
  ProcSetActSymbol(AData, SymNr, SymNrFound, SymName, SymNameFound, LibName, LibNameFound);
  if DDEHandler.DDEData^.GetCount > 0 then
    PChar(DDEHandler.DDEData^.At(0))[0] := DDEStrUsed;
  DDEHandler.DeleteDDEData;
end;

procedure ProcDBRequestSymbols
  (
  AData: PProj
  );
var
  ExtLibName: string;
  AExtLib: PExtLib;
  i: Integer;

  procedure SendSyms
      (
      Item: PSGroup
      ); far;
  var
    SendStr: string;
    TmpStr: string;
  begin
    Str(Item^.Index - 600000000: 10, TmpStr);
    SendStr := '[TSY][' + TmpStr + '][' + Item^.Name^ + '][' + ExtLibName + ']';

    if PSymbol(Item)^.Size < 1.5990221981798E-313 then
      TmpStr := '-1'
    else
      TmpStr := FloatToStr(PSymbol(Item)^.Size);
    SendStr := SendStr + '[' + TmpStr + ']';

    if PSymbol(Item)^.ObjectStyle <> nil then
    begin
      TmpStr := IntToStr(PSymbol(Item)^.ObjectStyle^.FillStyle.Pattern);
      SendStr := SendStr + '[' + TmpStr + ']';
      TmpStr := IntToStr(PSymbol(Item)^.ObjectStyle^.FillStyle.ForeColor);
      SendStr := SendStr + '[' + TmpStr + ']';
      TmpStr := IntToStr(PSymbol(Item)^.ObjectStyle^.FillStyle.BackColor);
      SendStr := SendStr + '[' + TmpStr + ']';
      TmpStr := IntToStr(PSymbol(Item)^.ObjectStyle^.LineStyle.Style);
      SendStr := SendStr + '[' + TmpStr + ']';
      TmpStr := IntToStr(PSymbol(Item)^.ObjectStyle^.LineStyle.Color);
      SendStr := SendStr + '[' + TmpStr + ']';
    end
    else
      SendStr := SendStr + '[-1][-1][-1][-1][-1]';

    DDEHandler.SendString(SendStr);
  end;
begin
  ExtLibName := MlgStringList.Strings['SymbolRollup', 17];
  PSymbols(AData^.PInfo^.Symbols)^.Data^.ForEach(@SendSyms);
  for i := 0 to PSymbols(AData^.PInfo^.Symbols)^.ExternLibs.Count - 1 do
  begin
    AExtLib := PExtLib(PSymbols(AData^.PInfo^.Symbols)^.ExternLibs.Items[i]);
    ExtLibName := AnsiLowerCase(AExtLib^.LibFileName^);
    AExtLib^.Symbols^.Data^.ForEach(@SendSyms);
  end;
  DDEHandler.SendString('[END][]');
end;

procedure ProcDBRequestLineStyles
  (
  AData: PProj
  );
const
  WindowsStyles = 5;
var
  ALineStyles: TXLineStyleList;
  Item: TXLineStyle;
  Count: Integer;
  i: Integer;
  SendString: string;
  TmpString: string;
begin
  for Count := 0 to WindowsStyles - 1 do
  begin
    Str(Count: 0, TmpString);
    SendString := '[TLI][' + TmpString + '][';
    TmpString := MlgStringList['LineStyleDialog', 20 + Count];
    SendString := SendString + TmpString + ']';
    DDEHandler.SendString(SendString);
  end;
  ALineStyles := AData^.PInfo^.ProjStyles.XLineStyles;
  for i := 0 to ALineStyles.Capacity - 1 do
  begin
    Item := ALineStyles.Items[i];
    if Item <> nil then
    begin
      Str(Item.Number: 0, TmpString);
      SendString := '[TLI][' + TmpString + '][' + Item.Name + ']';
      DDEHandler.SendString(SendString);
      Inc(Count);
    end;
  end;
  DDEHandler.SendString('[END][]');
end;

procedure ProcDBRequestHatchStyles
  (
  AData: PProj
  );
const
  WindowsStyles = 8;
var
  AFillStyles: TXFillStyleList;
  Item: TCustomXFill;
  Count: Integer;
  i: Integer;
  SendString: string;
  TmpString: string;
begin
  for Count := 0 to WindowsStyles - 1 do
  begin
    Str(Count: 0, TmpString);
    SendString := '[THA][' + TmpString + '][';
    TmpString := MlgStringList['PatternDialog', 29 + Count];
    SendString := SendString + TmpString + ']';
    DDEHandler.SendString(SendString);
  end;
  AFillStyles := AData^.PInfo^.ProjStyles.XFillStyles;
  for i := 0 to AFillStyles.Capacity - 1 do
  begin
    Item := AFillStyles.Items[i];
    if Item <> nil then
    begin
      Str(Item.Number: 0, TmpString);
      SendString := '[THA][' + TmpString + '][' + Item.Name + ']';
      DDEHandler.SendString(SendString);
      Inc(Count);
    end;
  end;
  DDEHandler.SendString('[END][]');
end;

procedure ProcSendSelectedObjectsToDB
  (
  AData: PProj
  );
var
  DDELineCount: Word;
  SendAllObjs: Boolean;

  procedure DoAll
      (
      Item: PChar
      )
      ; far;
  var
    ObjCount: LongInt;
    SFound: Boolean;
    MStrs: Boolean;
    ResLInt: LongInt;
    SObjCount: LongInt;
    TempString: string;

    procedure AppendObject
        (
        Item: PIndex
        )
        ; far;
    begin
      Str(Item^.Index: 10, TempString);
      TempString := TempString + DDEHandler.FOutSepSign + '1';
      DDEHandler.AppendString(TempString);
    end;

    function SearchObject
        (
        Item: PIndex
        )
        : Boolean; far;
    begin
      Result := Item^.Index = ResLInt;
    end;
  begin
    if not SendAllObjs then
    begin
      ObjCount := 0;
      if not ReadDDECommandLongInt(DDEHandler.DDEData^.At(DDELineCount), 1, 0, WgBlockStart,
        WgBlockEnd, WgBlockEnd, ObjCount, SFound, MStrs) then
      begin
        if DDELineCount = 0 then
          SendAllObjs := TRUE
        else
          ObjCount := 0;
      end;
      if SendAllObjs then
      begin {alle selektierten Objekte bekanntgeben}
        DDEHandler.StartList('[SOB][   ]');
        AData^.Layers^.SelLayer^.Data^.ForEach(@AppendObject);
        DDEHandler.EndList(FALSE);
      end
      else
      begin {nur angeforderte Objekte bekanntgeben}
        DDEHandler.StartList('[SOB][   ]');
        for SObjCount := 0 to ObjCount - 1 do
        begin
          if ReadDDECommandLongInt(DDEHandler.DDEData^.At(DDELineCount), SObjCount + 2, 0,
            WgBlockStart, WgBlockEnd, WgBlockEnd, ResLInt, SFound, MStrs) then
          begin
            if SFound and (ResLInt <> 0) then
            begin
              Str(ResLInt: 10, TempString);
              TempString := TempString + DDEHandler.FOutSepSign;
              if AData^.Layers^.SelLayer^.Data^.FirstThat(@SearchObject) <> nil then
                TempString := TempString + '1'
              else
                TempString := TempString + '0';
              DDEHandler.AppendString(TempString);
            end;
          end;
        end;
        DDEHandler.EndList(FALSE);
      end;
      Inc(DDELineCount);
    end;
    Item[0] := DDEStrUsed;
  end;
begin
  DDELineCount := 0;
  SendAllObjs := FALSE;
  DDEHandler.DDEData^.ForEach(@DoAll);
  DDEHandler.SendString('[END][]');
  DDEHandler.DeleteDDEData;
end;

procedure ProcSetSelModeGIS
  (
  AData: PProj
  );
var
  ResLInt: LongInt;
  SFound: Boolean;
  MStrs: Boolean;
begin
  if ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 1, 0, WgBlockStart, WgBlockEnd,
    WgBlockEnd, ResLInt, SFound, MStrs) then
  begin
    if ResLInt = 1 then
      AData^.SelNewObjectsGIS := TRUE
    else
      AData^.SelNewObjectsGIS := FALSE;
  end;
  if DDEHandler.DDEData^.GetCount > 0 then
    PChar(DDEHandler.DDEData^.At(0))[0] := DDEStrUsed;
  DDEHandler.DeleteDDEData;
end;

procedure ProcDBDenySession
  (
  AData: PProj
  );
var
  AppNameDeny: PChar;
  DoDeny: Boolean;
  ResLInt: LongInt;
  SFound: Boolean;
  MStrs: Boolean;
  AApp: PDDEMLApp;

  function SearchEntry
      (
      Item: PDDEMLApp
      )
      : Boolean; far;
  begin
    Result := StrIComp(Item^.Name, AppNameDeny) = 0;
  end;
begin
  if ReadDDECommandPChar(DDEHandler.DDEData^.At(0), 1, 0, WgBlockStart, WgBlockEnd,
    WgBlockEnd, AppNameDeny, MStrs) then
  begin
    AApp := DDEHandler.DDEMLApps^.FirstThat(@SearchEntry);
    if AApp <> nil then
    begin
      if ReadDDECommandLongInt(DDEHandler.DDEData^.At(0), 2, 0, WgBlockStart, WgBlockEnd,
        WgBlockEnd, ResLInt, SFound, MStrs) then
      begin
        if ResLInt = 0 then
          AApp^.SendStrings := TRUE
        else
          if ResLInt = 1 then
            AApp^.SendStrings := FALSE;
      end;
    end;
    StrDispose(AppNameDeny);
  end;
  if DDEHandler.DDEData^.GetCount > 0 then
    PChar(DDEHandler.DDEData^.At(0))[0] := DDEStrUsed;
  DDEHandler.DeleteDDEData;
end;

procedure ProcSelectEqualObjects(AData: PProj; ALayer: PLayer);
var
  AllCount, Cnt, i: Integer;

  procedure DoAll(AItem: PIndex);
  var
    APoly: PPoly;
    APixel: PPixel;

    procedure DoAll1(BItem: PIndex);
    var
      BPoly: PPoly;
      BPixel: PPixel;
      i: Integer;
      IsEqual: Boolean;
      APoint, BPoint: PDPoint;
    begin
      if (BItem^.Index <> AItem^.Index) and (BItem^.ClipRect.IsEqual(AItem^.ClipRect)) and (not BItem^.GetState(sf_Selected)) then
      begin
        if (BItem^.GetObjType in [ot_Poly, ot_CPoly]) then
        begin
          BPoly := PPoly(PLayer(AData^.PInfo^.Objects)^.IndexObject(AData^.PInfo, BItem));
          if BPoly <> nil then
          begin
            if APoly^.Data^.Count = BPoly^.Data^.Count then
            begin
              IsEqual := True;
              for i := 0 to APoly^.Data^.Count - 1 do
              begin
                APoint := APoly^.Data^.At(i);
                BPoint := BPoly^.Data^.At(i);
                if APoint^.IsDiff(BPoint^) then
                begin
                  IsEqual := False;
                  break;
                end;
              end;
              ALayer^.Select(AData^.PInfo, AItem);
            end;
          end;
        end
        else
          if (BItem^.GetObjType in [ot_Pixel]) then
          begin
            BPixel := PPixel(PLayer(AData^.PInfo^.Objects)^.IndexObject(AData^.PInfo, BItem));
            if BPixel <> nil then
            begin
              APoint := @APixel^.Position;
              BPoint := @BPixel^.Position;
              if not APoint^.IsDiff(BPoint^) then
              begin
                ALayer^.Select(AData^.PInfo, AItem);
              end;
            end;
          end;
      end;
    end;

  begin
    if (AItem^.GetObjType in [ot_Poly, ot_CPoly]) then
    begin
      APoly := PPoly(PLayer(AData^.PInfo^.Objects)^.IndexObject(AData^.PInfo, AItem));
      if APoly <> nil then
      begin
        ALayer^.Data^.ForEach(@DoAll1);
      end;
    end
    else
      if (AItem^.GetObjType in [ot_Pixel]) then
      begin
        APixel := PPixel(PLayer(AData^.PInfo^.Objects)^.IndexObject(AData^.PInfo, AItem));
        if APixel <> nil then
        begin
          ALayer^.Data^.ForEach(@DoAll1);
        end;
      end;
    Inc(Cnt);
    StatusBar.Progress := (Cnt * 100) / AllCount;
  end;

begin
  StatusBar.ProgressPanel := True;
  StatusBar.ProgressText := GetLangText(4023);
  Cnt := 0;
  AllCount := ALayer^.Data^.GetCount;
  AData^.SetCursor(crHourGlass);
  try
    AData^.DeSelectAll(False);
    for i := ALayer^.Data^.GetCount - 1 downto 0 do
    begin
      DoAll(ALayer^.Data^.At(i));
    end;
  finally
    StatusBar.ProgressPanel := False;
    AData^.SetCursor(crDefault);
    UserInterface.Update([uiStatusBar]);
  end;
end;

procedure ProcFindObjectOnLayer(AData: PProj; ALayerName: string; AId: Integer);
var
  AllCount, Cnt: Integer;
  ALayer: PLayer;
  SItem, HItem: PIndex;

  procedure CompareObjects(AItem: PIndex);
  var
    APoly: PPoly;
    APixel: PPixel;

    procedure DoAll(BItem: PIndex);
    var
      BPoly: PPoly;
      BPixel: PPixel;
      i: Integer;
      IsEqual: Boolean;
      APoint, BPoint: PDPoint;
    begin
      if (BItem^.ClipRect.IsEqual(AItem^.ClipRect)) and (not BItem^.GetState(sf_Selected)) then
      begin
        if (BItem^.GetObjType in [ot_Poly, ot_CPoly]) then
        begin
          BPoly := PPoly(PLayer(AData^.PInfo^.Objects)^.IndexObject(AData^.PInfo, BItem));
          if BPoly <> nil then
          begin
            if APoly^.Data^.Count = BPoly^.Data^.Count then
            begin
              IsEqual := True;
              for i := 0 to APoly^.Data^.Count - 1 do
              begin
                APoint := APoly^.Data^.At(i);
                BPoint := BPoly^.Data^.At(i);
                if APoint^.IsDiff(BPoint^) then
                begin
                  IsEqual := False;
                  break;
                end;
              end;
              DDEHandler.SendString('[SEO][' + IntToStr(BItem^.Index) + ']');
            end;
          end;
        end
        else
          if (BItem^.GetObjType in [ot_Pixel]) then
          begin
            BPixel := PPixel(PLayer(AData^.PInfo^.Objects)^.IndexObject(AData^.PInfo, BItem));
            if BPixel <> nil then
            begin
              APoint := @APixel^.Position;
              BPoint := @BPixel^.Position;
              if not APoint^.IsDiff(BPoint^) then
              begin
                DDEHandler.SendString('[SEO][' + IntToStr(BItem^.Index) + ']');
              end;
            end;
          end;
      end;
    end;

  begin
    if (AItem^.GetObjType in [ot_Poly, ot_CPoly]) then
    begin
      APoly := PPoly(PLayer(AData^.PInfo^.Objects)^.IndexObject(AData^.PInfo, AItem));
      if APoly <> nil then
      begin
        ALayer^.Data^.ForEach(@DoAll);
      end;
    end
    else
      if (AItem^.GetObjType in [ot_Pixel]) then
      begin
        APixel := PPixel(PLayer(AData^.PInfo^.Objects)^.IndexObject(AData^.PInfo, AItem));
        if APixel <> nil then
        begin
          ALayer^.Data^.ForEach(@DoAll);
        end;
      end;
    Inc(Cnt);
    StatusBar.Progress := (Cnt * 100) / AllCount;
  end;

begin
  ALayer := AData^.Layers^.NameToLayer(ALayerName);
  if (ALayer <> nil) and (AId <> 0) then
  begin
    StatusBar.ProgressPanel := True;
    StatusBar.ProgressText := GetLangText(4023);
    Cnt := 0;
    AllCount := ALayer^.Data^.GetCount;
    AData^.SetCursor(crHourGlass);
    HItem := New(PIndex, Init(AId));
    SItem := PLayer(AData^.PInfo^.Objects)^.IndexObject(AData^.PInfo, HItem);
    try
      if (SItem <> nil) and (AllCount > 0) then
      begin
        CompareObjects(SItem);
      end;
      if AllCount = 0 then
      begin
        DDEHandler.SendString('[SEO][]');
      end;
    finally
      StatusBar.ProgressPanel := False;
      AData^.SetCursor(crDefault);
      UserInterface.Update([uiStatusBar]);
      Dispose(HItem, Done);
    end;
  end;
end;

{******************************************************************************}

procedure ProcSaveViewAsImage(AData: PProj; AFilename: AnsiString);
var
  Bmp: Graphics.TBitmap;
  Jpg: TJpegImage;
  AHandle: THandle;
  APalette: HPalette;
  AFormat: Word;
  AMsg: TWMPaint;
  i, Q: Integer;
  s, TabFilename: AnsiString;
  Tab: TStringList;
  LRect: TDRect;
begin
  LRect.Init;
  if (GetKeyState(VK_SHIFT) < 0) then
  begin
    Bmp := TBitmap.Create;
    try
      Bmp.Handle := AData^.PInfo^.ExtCanvas.BitmapHandle;
      Bmp.HandleType := bmDIB;
      Bmp.SaveToClipBoardFormat(AFormat, AHandle, APalette);
      ClipBoard.SetAsHandle(AFormat, AHandle);
    finally
      Bmp.free
    end;
  end
  else
  begin
    if (AFilename <> '') or (WinGISMainForm.SavePictureDialog.Execute) then
    begin
      if AFilename = '' then
      begin
        AFilename := WinGISMainForm.SavePictureDialog.FileName;
      end;
      Bmp := TBitmap.Create;
      try
        Bmp.HandleType := bmDIB;
        Bmp.Handle := AData^.PInfo^.ExtCanvas.BitmapHandle;
        Bmp.PixelFormat := pf24bit;
        Jpg := TJpegImage.Create;
        Tab := TStringList.Create;
        try
          Q := 90;
          for i := 0 to 12 do
          begin
            try
              Jpg.Assign(Bmp);
              Jpg.CompressionQuality := Q;
              Jpg.SaveToFile(AFilename);

              TabFilename := ChangeFileExt(AFilename, '.tab');
              s := WingisMainForm.MemoTab.Text;

              AData^.PInfo^.GetCurrentScreen(LRect);

              s := StringReplace(s, '%F', ExtractFilename(AFilename), []);
              s := StringReplace(s, '%L', IntToStr(LRect.A.X), []);
              s := StringReplace(s, '%B', IntToStr(LRect.A.Y), []);
              s := StringReplace(s, '%R', IntToStr(LRect.B.X), []);
              s := StringReplace(s, '%T', IntToStr(LRect.B.Y), []);
              s := StringReplace(s, '%W', IntToStr(Jpg.Width), []);
              s := StringReplace(s, '%H', IntToStr(Jpg.Height), []);

              Tab.Text := s;
              Tab.SaveToFile(TabFilename);

              break;
            except
              Dec(Q, 5);
            end;
          end;
        finally
          Jpg.Free;
          Tab.Free;
        end;
      finally
        Bmp.Free;
      end;
    end;
  end;
end;

{$ENDIF}
{$ENDIF} // <----------------- AXDLL
end.

