{****************************************************************************}
{ Unit AM_BuGra                                                              }
{----------------------------------------------------------------------------}
{ Grafikobjekt f�r Balken und Kreisdiagramme                                 }
{----------------------------------------------------------------------------}
{ Modifikationen                                                             }
{                                                                            }
{                                                                            }
{  09.10.1994 Heinz Ofner                                                    }
{****************************************************************************}
unit AM_BuGra;

interface

uses WinTypes, WinProcs, SysUtils, AM_Def, AM_View, AM_Paint, AM_Index, AM_Layer,
  AM_Coll, AM_CPoly, Objects, AM_Dlg1, AM_Font, AM_Input, AM_Circl,
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
  Classes, Controls, NumTools, TeeProcs, TeEngine, Chart, Series,
  AM_Group, AM_Obj;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

{++ Moskaliov Business Graphics BUILD#150 17.01.01}
const
  vnBusGraphics = 1; // Business Graphics Object version
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

const
  ciMaxDist = 10; { Maximaler Abstand f�r SelectByPoint in 1/10 mm }
  MaxTxtLen = 255; { Maximale L�nge des Beschriftungstextes }

  dtCircleDiag = 0;
  dtBeamDiag = 1;
  dtLineDiag = 2;
  dtAreaDiag = 3;
  dtPointDiag = 4;

  dp_ShowVals = $0001; { Diagrammwerte anzeigen                          }
  dp_CDiamAbsRel = $0002; { Kreisdiagramm durchmesser absolut oder relativ  }
  dp_ValAbsRel = $0100; { Diagrammwerte absolut oder relativ anzeigen     }

type
  PBusGraph = ^TBusGraph;
  TBusGraph = object(TView)
    DiagType: Byte;
    DCircAbsRel: Boolean; // Absolute/Relative Diameter of circle (TRUE-Absolute, FALSE-Relative)
    ShowValues: Boolean; // Show Values Labels (TRUE-yes, FALSE-no)
    AnnotAbsRel: Boolean; // Show Absolute/Relative Values (TRUE-Absolute, FALSE-Relative)
    Position: TDPoint;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
    ChartKindIndex: LongInt;
    ChartHashIndex: LongInt;
    ChartKindPtr: PChartKind;
    ChartWidth: LongInt;
    ChartHeight: LongInt;
    RelAbsSize: Boolean; // Relative/Absolute Size of diagramm (TRUE-Relative, FALSE-Absolute)
// TODO
//    Alignment: TChartAlignment;
    OffsetX: LongInt;
    OffsetY: LongInt;
    ShowOld: Boolean; // Show old Business graphics (temporarily for DEBUG)
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
    BWidth: LongInt;
    BHeight: LongInt;
    NCDiameter: LongInt;
    CDiameter: LongInt;
    GesWidth: LongInt;
    GesHeight: LongInt;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
//                     Sum         : Real48;
    Sum: Double;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
    ValItems: PCollection;
    ID: LongInt;
    SVFont: TFontData;
    Selected: Boolean;
    DiagParams: Word;
    PLAColor: LongInt;
    PLAPen: Word;
    PLAPattern: Word;
    constructor Init(PInfo: PPaint; ABeam: Byte; AItems: PCollection; ASum: Real; APos: TDPoint;
      AWidth, AHeight, ADiameter, AGesWidth, AGesHeight: LongInt; ACircAbsRel: Boolean;
      AShowVals: Boolean; AAbsRel: Boolean; APLAColor: LongInt; APLAPen, APLAPattern: Word;
      AFont: TFontData; AID: LongInt; ASel: Boolean);
    destructor Done; virtual;
    constructor Load(S: TOldStream);
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
//                     Procedure   CalculateClipRect(PInfo: PPaint);
    procedure CalculateClipRect;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
    procedure Draw(PInfo: PPaint; Clipping: Boolean); virtual;
    function GetObjType: Word; virtual;
    procedure MoveRel(XMove, YMove: LongInt); virtual;
    function SelectByCircle(PInfo: PPaint; Middle: TDPoint;
      Radius: LongInt; const ObjType: TObjectTypes; Inside: Boolean): Boolean; virtual;
    function SelectByPoint(PInfo: PPaint; Point: TDPoint; const ObjType: TObjectTypes): PIndex; virtual;
    function SelectByPoly(PInfo: PPaint; Poly: Pointer;
      const ObjType: TObjectTypes; Inside: Boolean): Boolean; virtual;
    procedure Store(S: TOldStream); virtual;
    function SelectBetweenPolys(PInfo: PPaint; Poly1, Poly2: Pointer; const ObjType: TObjectTypes;
      Inside: Boolean): Boolean; virtual;
    function GetBorder(PInfo: PPaint): PCPoly;
    function GetMoveBorder(PInfo: PPaint): PCPoly;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
//                     Procedure   GetMultiMoveBorder(AMulti:PMoveMulti;PInfo:PPaint);
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
  end;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}
  POldBusGraph = ^TOldBusGraph;
  TOldBusGraph = object(TOldObject)
    NewObjectPtr: PBusGraph;
    DiagParams: Word;
    BWidth: LongInt;
    BHeight: LongInt;
    NCDiameter: LongInt;
    GesWidth: LongInt;
    GesHeight: LongInt;
    SVFont: TFontData;
    constructor Init(ObjectPtr: PBusGraph; Params: Word;
      AWidth: LongInt; AHeight: LongInt; Diameter: LongInt;
      GWidth: LongInt; GHeight: LongInt; AFont: TFontData);
    destructor Done; virtual;
  end;

  POldBuGraList = ^TOldBuGraList;
  TOldBuGraList = object(TOldObject)
    ObjList: TList;
    TextWidth: LongInt;
    constructor Init;
    destructor Done; virtual;
  end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

implementation

uses AM_Proj, AM_Poly, Win32Def,
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
  Graphics, AM_Main, Forms;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

{*****************************************************************************************************************************}
{**************************************** Init *******************************************************************************}
{*****************************************************************************************************************************}

constructor TBusGraph.Init(PInfo: PPaint; ABeam: Byte; AItems: PCollection;
  ASum: Real; APos: TDPoint; AWidth: LongInt; AHeight: LongInt;
  ADiameter: LongInt; AGesWidth: LongInt; AGesHeight: LongInt;
  ACircAbsRel: Boolean; AShowVals: Boolean; AAbsRel: Boolean;
  APLAColor: LongInt; APLAPen: Word; APLAPattern: Word;
  AFont: AM_Font.TFontData; AID: LongInt; ASel: Boolean);
begin
  TView.Init;
  ValItems := New(PCollection, Init(5, 5));
  DiagType := ABeam;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
  RelAbsSize := FALSE;
// TODO
//  Alignment := alLeftTop;
  OffsetX := 0;
  OffsetY := 0;
//   ChartKindIndex:=AIndex;
//   ChartKindPtr:=PChartKind;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
  ValItems := AItems;
  Sum := ASum;
  BWidth := AWidth;
  BHeight := AHeight;
  NCDiameter := ADiameter;
  GesWidth := AGesWidth;
  GesHeight := AGesHeight;
  Position.Init(APos.X, APos.Y);
  ID := AID;
  Selected := ASel;
  SVFont.Init;
  SVFont := AFont;
  DCircAbsRel := ACircAbsRel;
  ShowValues := AShowVals;
  AnnotAbsRel := AAbsRel;

  if DCircAbsRel then
    CDiameter := NCDiameter
  else
    CDiameter := Round(NCDiameter * Sum / 100);

  PLAColor := APLAColor;
  PLAPen := APLAPen;
  PLAPattern := APLAPattern;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
//-- CalculateClipRect(PInfo);
  CalculateClipRect;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
end;

{**************************************** Done *******************************************************************************}

destructor TBusGraph.Done;
begin
  Dispose(ValItems, Done);
  Position.Done;
  if SVFont.Font <> 0 then
    SVFont.Done;
  inherited Done;
end;

{**************************************** Load *******************************************************************************}

constructor TBusGraph.Load(S: TOldStream);
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
var
  BusGraphVersion: Byte;
  PInfo: PPaint;
  AChartsLibPtr: PChartsLib;
  OldBusGraphPtr: POldBusGraph;
  OldSum: Real48;
  i: Integer;
  bValue: Byte;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
begin
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
  if ProjectVersion < 27 then
    BusGraphVersion := 0
  else
    S.Read(BusGraphVersion, SizeOf(BusGraphVersion));
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
  TView.Load(S);
  S.Read(DiagType, SizeOf(DiagType));
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
{$IFNDEF AXDLL} // <----------------- AXDLL
  PInfo := PProj(WinGISMainForm.LoadingProj)^.PInfo;
{$ENDIF} // <----------------- AXDLL
  AChartsLibPtr := PChartsLib(PInfo^.ChartViewKinds);
  if BusGraphVersion > 0 then
  begin
    S.Read(RelAbsSize, SizeOf(Boolean)); // Relative/Absolute Size of diagramm (TRUE-Relative, FALSE-Absolute)
    S.Read(ID, SizeOf(ID));
    S.Read(ChartKindIndex, SizeOf(ChartKindIndex));
    S.Read(ChartHashIndex, SizeOf(ChartHashIndex));
    ChartKindPtr := AChartsLibPtr^.FindChartKind(ChartKindIndex, ChartHashIndex);
    S.Read(ChartWidth, SizeOf(LongInt));
    S.Read(ChartHeight, SizeOf(LongInt));
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
    Position.Load(S);
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
// TODO
    S.Read(bValue, SizeOf(Byte));
//    S.Read(Alignment, SizeOf(Alignment));
    S.Read(OffsetX, SizeOf(LongInt));
    S.Read(OffsetY, SizeOf(LongInt));
    ShowOld := FALSE;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
    S.Read(Sum, SizeOf(Sum));
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
  end
  else
  begin
    Position.Load(S);
    S.Read(OldSum, SizeOf(Real48));
    Sum := OldSum;
  end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

  ValItems := New(PCollection, Load(S));
  if (ValItems^.Count <> 0) and (StrComp(PDGraph(ValItems^.At(0))^.Name, '�$PLADiagInfos%&') = 0) then
  begin
    PLAColor := PDGraph(ValItems^.At(0))^.Color;
    PLAPen := PDGraph(ValItems^.At(0))^.Pen;
    PLAPattern := PDGraph(ValItems^.At(0))^.Pattern;
    ValItems^.AtFree(0);
  end
  else
  begin
    PLAColor := StdPalette[0];
    PLAPen := lt_Solid;
    PLAPattern := pt_Solid;
  end;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
  Selected := FALSE;

  if BusGraphVersion < 1 then
  begin
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
    S.Read(BWidth, SizeOf(BWidth));
    S.Read(BHeight, SizeOf(BHeight));
    S.Read(NCDiameter, SizeOf(NCDiameter));
    S.Read(GesWidth, SizeOf(GesWidth));
    S.Read(GesHeight, SizeOf(GesHeight));
    S.Read(ID, SizeOf(ID));
    S.Read(DiagParams, SizeOf(DiagParams));

    if DiagParams and dp_ShowVals > 0 then
      ShowValues := TRUE
    else
      ShowValues := FALSE;

    if DiagParams and dp_ValAbsRel > 0 then
      AnnotAbsRel := TRUE
    else
      AnnotAbsRel := FALSE;

    if DiagParams and dp_CDiamAbsRel > 0 then
      DCircAbsRel := FALSE
    else
      DCircAbsRel := TRUE;

    SVFont.Load(S);

    if DCircAbsRel then
      CDiameter := NCDiameter
    else
      CDiameter := Round(NCDiameter * Sum / 100);

{++ Moskaliov Business Graphics BUILD#150 17.01.01}
//--     Selected:=FALSE;
// TODO
//    Alignment := alLeftTop;

    ChartKindIndex := -1;
    ChartHashIndex := 0;
    ChartKindPtr := nil;
    OldBusGraphPtr := New(POldBusGraph, Init(@Self, DiagParams, BWidth, BHeight,
      NCDiameter, GesWidth, GesHeight, SVFont));
    POldBuGraList(PInfo^.OldBusGraphList)^.ObjList.Add(OldBusGraphPtr);
    ShowOld := TRUE;
  end;

{-- Moskaliov Business Graphics BUILD#150 17.01.01}
end;

{**************************************** Store ******************************************************************************}

procedure TBusGraph.Store(S: TOldStream);
var
  TName: PChar;
  AValue: PDGraph;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
  BusGraphVersion: Byte;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
  bValue: Byte;
begin
  TName := StrNew('�$PLADiagInfos%&');
  AValue := New(PDGraph, Init(TName, 0, PLAColor, PLAPen, PLAPattern));
  ValItems^.AtInsert(0, AValue);
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
  BusGraphVersion := vnBusGraphics; // Business Graphics Object version
  S.Write(BusGraphVersion, SizeOf(BusGraphVersion));
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
  TView.Store(S);
  S.Write(DiagType, SizeOf(DiagType));
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
  S.Write(RelAbsSize, SizeOf(Boolean)); // Relative/Absolute Size of diagramm (TRUE-Relative, FALSE-Absolute)
  S.Write(ID, SizeOf(ID));
  S.Write(ChartKindIndex, SizeOf(ChartKindIndex));
  S.Write(ChartHashIndex, SizeOf(ChartHashIndex));
  S.Write(ChartWidth, SizeOf(LongInt));
  S.Write(ChartHeight, SizeOf(LongInt));
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
  Position.Store(S);
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
  S.Write(bValue, SizeOf(Byte));
//  S.Write(Alignment, SizeOf(Alignment));
  S.Write(OffsetX, SizeOf(LongInt));
  S.Write(OffsetY, SizeOf(LongInt));
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
  S.Write(Sum, SizeOf(Sum));
  ValItems^.Store(S);
  ValItems^.AtFree(0);
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
{  S.Write(BWidth,SizeOf(BWidth));
   S.Write(BHeight,SizeOf(BHeight));
   S.Write(NCDiameter,SizeOf(NCDiameter));
   S.Write(GesWidth,SizeOf(GesWidth));
   S.Write(GesHeight,SizeOf(GesHeight));
   S.Write(ID,SizeOf(ID));
   DiagParams:=0;
   if ShowValues then
      DiagParams:=DiagParams or dp_ShowVals;
   if AnnotAbsRel then
      DiagParams:=DiagParams or dp_ValAbsRel;
   if not DCircAbsRel then
      DiagParams:=DiagParams or dp_CDiamAbsRel;
   S.Write(DiagParams,SizeOf(DiagParams));
   SVFont.Store(S);                               }
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
end;

{**************************************** Draw *******************************************************************************}

procedure TBusGraph.Draw(PInfo: PPaint; Clipping: Boolean);
var
  Num: Integer;
  StartRX: LongInt;
  StartRY: LongInt;
  EndRX: LongInt;
  EndRY: LongInt;
  EndX: LongInt;
  EndY: LongInt;
  PosDraw1: TDPoint;
  PosDraw2: TDPoint;
  PosDisp1: TPoint;
  PosDisp2: TPoint;
  Dist: Real;
  Angle: Real;
  MinVal: LongInt;
  AreaPoints: PPoints;
  AreaPos: TDPoint;
  AreaDPos: TPoint;
  Piece: Integer;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
  AChartKind: PChartKind;
  Tmp1Chart: TCustomChart;
  TmpChart: TChart;
  TmpCrtSeries: TChartSeries;
  TmpItem: PDGraph;
  TmpValue: Double;
  TmpRect: TRect;
  BmpRect: TRect;
  Cnt: Integer;
  tmpWidth: Integer;
  tmpHeight: Integer;
  dblRatio: Double;
  THeight: Integer;
  Points: PPoints;
  PosDraw3: TDPoint;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
{**************************************** Kreisdiagramme zeichnen ****************************************}

  procedure DrawCircle(Item: PDGraph); far;
  var
    Valu: Real;
    StartX: LongInt;
    StartY: LongInt;
    PosDraw3: TDPoint;
    PosDraw4: TDPoint;
    PosDisp3: TPoint;
    PosDisp4: TPoint;
    Pen: HPen;
    OldPen: HPen;
    Brush: HBrush;
    OldBrush: HBrush;
    TText: string;
    RText: array[0..MaxTxtLen] of Char;
    TFont: HFont;
    THeight: LongInt;
    TWidth: LongInt;
    OldBKMode: Word;
    OldFont: HFont;
    RWidth: LongInt;
    Alpha: Real;
    Beta: Real;
    Pos: TDPoint;
    StartPos: TPoint;
    PercentVal: Real;
    DispX: Integer;
    DispY: Integer;
    DispC: Integer;
  begin
    inc(Piece);
    Valu := Item^.Value;
    Beta := Angle; {Anfangswinkel des Tortenst�ckes}
    Angle := Angle - (2 * Pi * Valu / Sum); {Endwinkel des Tortenst�ckes}
    Alpha := Angle + (Beta - Angle) / 2; {Winkel f�r die Beschriftung}
    if Alpha < 0 then
      Alpha := Alpha + 2 * Pi
    else
      if Alpha > 2 * Pi then
        Alpha := Alpha - 2 * Pi;
    if Angle < 0 then
      Angle := Angle + 2 * Pi;
    if (Angle > (7 * Pi) / 4) or (Angle <= Pi / 4) then
    begin
      StartX := EndRX;
      StartY := StartRY + Trunc(Dist * (1 + Sin(Angle) / cos(Angle)));
    end
    else
      if (Angle > Pi / 4) and (Angle <= (3 * Pi) / 4) then
      begin
        StartX := StartRX + Trunc(Dist * (1 + Sin(Pi / 2 - Angle) / Cos(Pi / 2 - Angle)));
        StartY := EndRY;
      end
      else
        if (Angle > (3 * Pi) / 4) and (Angle <= (5 * Pi) / 4) then
        begin
          StartX := StartRX;
          StartY := StartRY + Trunc(Dist * (1 + Sin(Pi - Angle) / Cos(Pi - Angle)));
        end
        else
          if (Angle > (5 * Pi) / 4) and (Angle <= (7 * Pi) / 4) then
          begin
            StartX := EndRX - Trunc(Dist * (1 + Sin(3 * Pi / 2 - Angle) / Cos(3 * Pi / 2 - Angle)));
            StartY := StartRY;
          end;
    if Valu <> 0 then
    begin {keine Darstellung von 0-Werten (w�rden als Vollkreis gezeichnet)}
      PosDraw3.Init(StartX, StartY);
      PosDraw4.Init(EndX, EndY);
      PInfo^.ConvertToDisp(PosDraw3, PosDisp3);
      PInfo^.ConvertToDisp(PosDraw4, PosDisp4);
      if Selected then
        Pen := GetPen(RGBColors[c_LBlue], lt_Dot, 0)
      else
        Pen := GetPen(0, Item^.Pen, 0);
      OldPen := SelectObject(PInfo^.ExtCanvas.Handle, Pen);
      Brush := GetBrush(Item^.Color, Item^.Pattern, PInfo^.ProjStyles.XFillStyles);
      OldBrush := SelectObject(PInfo^.ExtCanvas.Handle, Brush);
      if (abs(PosDisp4.X - PosDisp3.X) > 2) or (abs(PosDisp4.Y - PosDisp3.Y) > 2) {St�ck gro� genug}
        or ((Piece = 1) and (Valu > Sum / 2)) {oder erstes St�ck und fast oder gleich Summe}
        or ((Piece = ValItems^.Count) and (Valu > Sum / 2)) then {oder letztes St�ck und fast oder gleich Summe}
        Pie(PInfo^.ExtCanvas.Handle, PosDisp1.X, PosDisp1.Y, PosDisp2.X, PosDisp2.Y, PosDisp3.X, PosDisp3.Y, PosDisp4.X, PosDisp4.Y)
      else
      begin
        DispC := Round((PosDisp2.X - PosDisp1.X) / 2);
        DispX := PosDisp1.X + DispC;
        DispY := PosDisp1.Y + DispC;
        MoveTo(PInfo^.ExtCanvas.Handle, DispX, DispY);
        LineTo(PInfo^.ExtCanvas.Handle, DispX + Round(DispC * cos(Beta)), DispY + Round(DispC * sin(Beta)));
      end;
      SelectObject(PInfo^.ExtCanvas.Handle, OldBrush);
      DeleteObject(Brush);
      SelectObject(PInfo^.ExtCanvas.Handle, OldPen);
      DeleteObject(Pen);
    end;
    EndX := StartX;
    EndY := StartY;
    if ShowValues then
    begin
      if AnnotAbsRel then
        Str(Valu: 0: 0, TText)
      else
      begin
        PercentVal := 100 / Sum * Valu;
        Str(PercentVal: 0: 2, TText);
        TText := TText + ' %';
      end;
      THeight := SVFont.Height;
      TWidth := Round(THeight * PInfo^.GetTextRatio(SVFont, TText));
      if (Alpha < Pi / 2) then
        Pos.Init(Trunc(Position.X + Dist + (Dist + THeight / 5 + THeight / 2 * sin(Alpha)) * cos(Alpha)),
          Trunc(Position.Y - Dist + (Dist + THeight / 5 + THeight / 2 * sin(Alpha)) * sin(Alpha) + THeight / 2))
      else
        if (Alpha < Pi) then
          Pos.Init(Trunc(Position.X + Dist + (Dist + THeight / 5 + THeight / 2 * sin(Alpha)) * cos(Alpha) - TWidth),
            Trunc(Position.Y - Dist + (Dist + THeight / 5 + THeight / 2 * sin(Alpha)) * sin(Alpha) + THeight / 2))
        else
          if (Alpha < 3 * Pi / 2) then
            Pos.Init(Trunc(Position.X + Dist + (Dist + THeight / 5 - THeight / 2 * sin(Alpha)) * cos(Alpha) - TWidth),
              Trunc(Position.Y - Dist + (Dist + THeight / 5 - THeight / 2 * sin(Alpha)) * sin(Alpha) + THeight / 2))
          else
            Pos.Init(Trunc(Position.X + Dist + (Dist + THeight / 5 - THeight / 2 * sin(Alpha)) * cos(Alpha)),
              Trunc(Position.Y - Dist + (Dist + THeight / 5 - THeight / 2 * sin(Alpha)) * sin(Alpha) + THeight / 2));
      TWidth := PInfo^.CalculateDisp(TWidth);
      THeight := PInfo^.CalculateDisp(THeight);
      if THeight > MaxInt then
        TFont := 0
      else
        TFont := PInfo^.GetFont(SVFont, THeight, 0);
      if TFont <> 0 then
      begin
        if SVFont.Style and ts_Transparent <> 0 then
          OldBKMode := SetBkMode(PInfo^.ExtCanvas.Handle, Transparent)
        else
          OldBKMode := SetBkMode(PInfo^.ExtCanvas.Handle, Opaque);
        OldFont := SelectObject(PInfo^.ExtCanvas.Handle, TFont);
        StrPCopy(RText, TText);
        RWidth := LoWord(GetTextExtent(PInfo^.ExtCanvas.Handle, RText, StrLen(RText)));
        PInfo^.ConvertToDisp(Pos, StartPos);
        SelectObject(PInfo^.ExtCanvas.Handle, OldFont);
        DeleteObject(TFont);
        TFont := PInfo^.GetFont(SVFont, THeight, 0);
        OldFont := SelectObject(PInfo^.ExtCanvas.Handle, TFont);
        TextOut(PInfo^.ExtCanvas.Handle, StartPos.X, StartPos.Y, RText, StrLen(RText));
        SelectObject(PInfo^.ExtCanvas.Handle, OldFont);
        DeleteObject(TFont);
        SetBkMode(PInfo^.ExtCanvas.Handle, OldBKMode);
      end;
    end;
  end;
{**************************************** Balkendiagramme zeichnen ****************************************}

  procedure DrawBeam(Item: PDGraph); far;
  var
    Valu: Real;
    Left: LongInt;
    Right: LongInt;
    Top: LongInt;
    Bottom: LongInt;
    PosD1: TDPoint;
    PosD2: TDPoint;
    Pos1: TPoint;
    Pos2: TPoint;
    Pen: HPen;
    OldPen: HPen;
    Brush: HBrush;
    OldBrush: HBrush;
    TText: string;
    RText: array[0..MaxTxtLen] of Char;
    TFont: HFont;
    THeight: LongInt;
    TWidth: LongInt;
    OldBKMode: Word;
    OldFont: HFont;
    RWidth: LongInt;
    Pos: TDPoint;
    StartPos: TPoint;
    Angle: Integer;
    PercentVal: Real;
  begin
    Valu := Item^.Value;
    Left := Position.X + Num * BWidth;
    Right := Position.X + (Num + 1) * BWidth;
    Top := Round(Position.Y - GesHeight + BHeight / 100 * Valu);
    Bottom := Position.Y - GesHeight;
    PosD1.Init(Left, Bottom);
    PosD2.Init(Right, Top);
    PInfo^.ConvertToDisp(PosD1, Pos1);
    PInfo^.ConvertToDisp(PosD2, Pos2);
    if Selected then
      Pen := GetPen(RGBColors[c_LBlue], lt_Dot, 0)
    else
      Pen := GetPen(0, Item^.Pen, 0);
    OldPen := SelectObject(PInfo^.ExtCanvas.Handle, Pen);
    Brush := GetBrush(Item^.Color, Item^.Pattern, PInfo^.ProjStyles.XFillStyles);
    OldBrush := SelectObject(PInfo^.ExtCanvas.Handle, Brush);
    if Valu <> 0 then
      Rectangle(PInfo^.ExtCanvas.Handle, Pos1.X, Pos1.Y, Pos2.X, Pos2.Y)
    else
    begin
      MoveTo(PInfo^.ExtCanvas.Handle, Pos1.X, Pos1.Y);
      LineTo(PInfo^.ExtCanvas.Handle, Pos2.X, Pos2.Y);
    end;
    SelectObject(PInfo^.ExtCanvas.Handle, OldBrush);
    DeleteObject(Brush);
    SelectObject(PInfo^.ExtCanvas.Handle, OldPen);
    DeleteObject(Pen);
    if ShowValues then
    begin
      if AnnotAbsRel then
        Str(Valu: 0: 0, TText)
      else
      begin
        PercentVal := 100 / Sum * Valu;
        Str(PercentVal: 0: 2, TText);
        TText := TText + ' %';
      end;
      THeight := SVFont.Height;
      TWidth := Round(THeight * PInfo^.GetTextRatio(SVFont, TText));
      if Valu >= 0 then
        Pos.Init(Trunc(Position.X + Num * BWidth + BWidth / 10), Trunc(Position.Y - GesHeight - THeight / 5 - TWidth))
      else
        Pos.Init(Trunc(Position.X + Num * BWidth + BWidth / 10), Trunc(Position.Y - GesHeight + THeight / 5));
      Angle := 270;
      TWidth := PInfo^.CalculateDisp(TWidth);
      THeight := PInfo^.CalculateDisp(THeight);
      if THeight > MaxInt then
        TFont := 0
      else
        TFont := PInfo^.GetFont(SVFont, THeight, 0);
      if TFont <> 0 then
      begin
        if SVFont.Style and ts_Transparent <> 0 then
          OldBKMode := SetBkMode(PInfo^.ExtCanvas.Handle, Transparent)
        else
          OldBKMode := SetBkMode(PInfo^.ExtCanvas.Handle, Opaque);
        OldFont := SelectObject(PInfo^.ExtCanvas.Handle, TFont);
        StrPCopy(RText, TText);
        RWidth := LoWord(GetTextExtent(PInfo^.ExtCanvas.Handle, RText, StrLen(RText)));
        PInfo^.ConvertToDisp(Pos, StartPos);
        SelectObject(PInfo^.ExtCanvas.Handle, OldFont);
        DeleteObject(TFont);
        TFont := PInfo^.GetFont(SVFont, THeight, Angle);
        OldFont := SelectObject(PInfo^.ExtCanvas.Handle, TFont);
        TextOut(PInfo^.ExtCanvas.Handle, StartPos.X, StartPos.Y, RText, StrLen(RText));
        SelectObject(PInfo^.ExtCanvas.Handle, OldFont);
        DeleteObject(TFont);
        SetBkMode(PInfo^.ExtCanvas.Handle, OldBKMode);
      end;
    end;
    Inc(Num);
  end;
{**************************************** gr��te Ausdehnung im Minusbereich ****************************************}

  procedure GetMinVal(Item: PDGraph); far;
  var
    Val: LongInt;
  begin
    Val := Round(BHeight / 100 * Item^.Value);
    if Val < MinVal then
      MinVal := Val;
  end;
{**************************************** Rahmen zeichnen ****************************************}

  procedure DrawBorder;
  var
    Left: LongInt;
    Right: LongInt;
    Top: LongInt;
    Bottom: LongInt;
    PosD1: TDPoint;
    PosD2: TDPoint;
    Pos1: TPoint;
    Pos2: TPoint;
    Pen: HPen;
    OldPen: HPen;
    Brush: HBrush;
    OldBrush: HBrush;
  begin
    Left := Round(Position.X + BWidth / 2);
    Right := Round(Position.X + GesWidth - BWidth / 2);
    Top := Round(Position.Y);
    Bottom := Position.Y - GesHeight - MinVal;
    PosD1.Init(Left, Bottom);
    PosD2.Init(Right, Top);
    PInfo^.ConvertToDisp(PosD1, Pos1);
    PInfo^.ConvertToDisp(PosD2, Pos2);
    if Selected then
      Pen := GetPen(RGBColors[c_LBlue], lt_Dot, 0)
    else
      Pen := GetPen(RGBColors[c_Black], lt_Solid, 0);
    OldPen := SelectObject(PInfo^.ExtCanvas.Handle, Pen);
    Brush := GetBrush(RGBColors[c_White], pt_Solid, PInfo^.ProjStyles.XFillStyles);
    OldBrush := SelectObject(PInfo^.ExtCanvas.Handle, Brush);
    Rectangle(PInfo^.ExtCanvas.Handle, Pos1.X, Pos1.Y, Pos2.X, Pos2.Y); {Rand um das Diagramm}
    PosD1.Init(Left, Position.Y - GesHeight);
    PosD2.Init(Right, Position.Y - GesHeight);
    PInfo^.ConvertToDisp(PosD1, Pos1);
    PInfo^.ConvertToDisp(PosD2, Pos2);
    MoveTo(PInfo^.ExtCanvas.Handle, Pos1.X, Pos1.Y);
    LineTo(PInfo^.ExtCanvas.Handle, Pos2.X, Pos2.Y); {Nullinie}
    SelectObject(PInfo^.ExtCanvas.Handle, OldBrush);
    DeleteObject(Brush);
    SelectObject(PInfo^.ExtCanvas.Handle, OldPen);
    DeleteObject(Pen);
  end;
{************************* Beschriftung f�r Fl�chen-, Linien- und Punktdiagramme zeichnen *************************}

  procedure DrawValText(AValu: Real);
  var
    TText: string;
    RText: array[0..MaxTxtLen] of Char;
    TFont: HFont;
    THeight: LongInt;
    TWidth: LongInt;
    OldBKMode: Word;
    OldFont: HFont;
    RWidth: LongInt;
    Pos: TDPoint;
    StartPos: TPoint;
    Angle: Integer;
    PercentVal: Real;
  begin
    if AnnotAbsRel then
      Str(AValu: 0: 0, TText)
    else
    begin
      PercentVal := 100 / Sum * AValu;
      Str(PercentVal: 0: 2, TText);
      TText := TText + ' %';
    end;
    THeight := SVFont.Height;
    TWidth := Round(THeight * PInfo^.GetTextRatio(SVFont, TText));
    Pos.Init(Trunc(Position.X + Num * BWidth + BWidth / 10), Trunc(Position.Y - GesHeight - MinVal - THeight / 5 - TWidth));
    Angle := 270;
    TWidth := PInfo^.CalculateDisp(TWidth);
    THeight := PInfo^.CalculateDisp(THeight);
    if THeight > MaxInt then
      TFont := 0
    else
      TFont := PInfo^.GetFont(SVFont, THeight, 0);
    if TFont <> 0 then
    begin
      if SVFont.Style and ts_Transparent <> 0 then
        OldBKMode := SetBkMode(PInfo^.ExtCanvas.Handle, Transparent)
      else
        OldBKMode := SetBkMode(PInfo^.ExtCanvas.Handle, Opaque);
      OldFont := SelectObject(PInfo^.ExtCanvas.Handle, TFont);
      StrPCopy(RText, TText);
      RWidth := LoWord(GetTextExtent(PInfo^.ExtCanvas.Handle, RText, StrLen(RText)));
      PInfo^.ConvertToDisp(Pos, StartPos);
      SelectObject(PInfo^.ExtCanvas.Handle, OldFont);
      DeleteObject(TFont);
      TFont := PInfo^.GetFont(SVFont, THeight, Angle);
      OldFont := SelectObject(PInfo^.ExtCanvas.Handle, TFont);
      TextOut(PInfo^.ExtCanvas.Handle, StartPos.X, StartPos.Y, RText, StrLen(RText));
      SelectObject(PInfo^.ExtCanvas.Handle, OldFont);
      DeleteObject(TFont);
      SetBkMode(PInfo^.ExtCanvas.Handle, OldBKMode);
    end;
  end;
{**************************************** Fl�cheneckpunkte ermitteln ****************************************}

  procedure GetAreaPoints(Item: PDGraph); far;
  var
    Valu: Real;
    CoordX: LongInt;
    CoordY: LongInt;
    PosD1: TDPoint;
    Pos1: TPoint;
    Pen: HPen;
    OldPen: HPen;
    Brush: HBrush;
    OldBrush: HBrush;
  begin
    Valu := Item^.Value;
    CoordX := Round(Position.X + Num * BWidth + BWidth / 2);
    if Num > 0 then
      CoordX := CoordX - 1;
    CoordY := Round(Position.Y - GesHeight + BHeight / 100 * Valu);
    PosD1.Init(CoordX, CoordY);
    PInfo^.ConvertToDisp(PosD1, Pos1);
    AreaPoints^[Num + 1] := Pos1;
    if ShowValues then
      DrawValText(Valu);
    Inc(Num);
  end;
{**************************************** Fl�chendiagramme zeichnen ****************************************}

  procedure DrawArea;
  var
    Pen: HPen;
    OldPen: HPen;
    Brush: HBrush;
    OldBrush: HBrush;
  begin
    if Selected then
      Pen := GetPen(RGBColors[c_LBlue], lt_Dot, 0)
    else
      Pen := GetPen(PLAColor, PLAPen, 0);
    OldPen := SelectObject(PInfo^.ExtCanvas.Handle, Pen);
    Brush := GetBrush(PLAColor, PLAPattern, PInfo^.ProjStyles.XFillStyles);
    OldBrush := SelectObject(PInfo^.ExtCanvas.Handle, Brush);
    Polygon(PInfo^.ExtCanvas.Handle, AreaPoints^, Num + 1);
    SelectObject(PInfo^.ExtCanvas.Handle, OldBrush);
    DeleteObject(Brush);
    SelectObject(PInfo^.ExtCanvas.Handle, OldPen);
    DeleteObject(Pen);
  end;
{**************************************** Liniendiagramme zeichnen ****************************************}

  procedure DrawLine(Item: PDGraph); far;
  var
    Valu: Real;
    CoordX: LongInt;
    CoordY: LongInt;
    PosD1: TDPoint;
    Pos1: TPoint;
    Pen: HPen;
    OldPen: HPen;
    Brush: HBrush;
    OldBrush: HBrush;
  begin
    Valu := Item^.Value;
    CoordX := Round(Position.X + Num * BWidth + BWidth / 2);
    CoordY := Round(Position.Y - GesHeight + BHeight / 100 * Valu);
    PosD1.Init(CoordX, CoordY);
    PInfo^.ConvertToDisp(PosD1, Pos1);
    if Selected then
      Pen := GetPen(RGBColors[c_LBlue], lt_Dot, 0)
    else
      Pen := GetPen(PLAColor, PLAPen, 0);
    OldPen := SelectObject(PInfo^.ExtCanvas.Handle, Pen);
    if Num = 0 then
      MoveTo(PInfo^.ExtCanvas.Handle, Pos1.X, Pos1.Y)
    else
      LineTo(PInfo^.ExtCanvas.Handle, Pos1.X, Pos1.Y);
    SelectObject(PInfo^.ExtCanvas.Handle, OldBrush);
    DeleteObject(Brush);
    SelectObject(PInfo^.ExtCanvas.Handle, OldPen);
    DeleteObject(Pen);
    if ShowValues then
      DrawValText(Valu);
    Inc(Num);
  end;
{**************************************** Punktdiagramme zeichnen ****************************************}

  procedure DrawPoint(Item: PDGraph); far;
  var
    Valu: Real;
    LeftX: LongInt;
    LeftY: LongInt;
    RightX: LongInt;
    RightY: LongInt;
    Middle: LongInt;
    PosD1: TDPoint;
    PosD2: TDPoint;
    Pos1: TPoint;
    Pos2: TPoint;
    Pen: HPen;
    OldPen: HPen;
    Brush: HBrush;
    OldBrush: HBrush;
    PointSize: LongInt;
  begin
    PointSize := Round(GesWidth / 50);
    Valu := Item^.Value;
    LeftX := Round(Position.X + Num * BWidth + BWidth / 2 - PointSize);
    LeftY := Round(Position.Y - GesHeight + BHeight / 100 * Valu - PointSize);
    RightX := Round(Position.X + Num * BWidth + BWidth / 2 + PointSize);
    RightY := Round(Position.Y - GesHeight + BHeight / 100 * Valu + PointSize);
    PosD1.Init(LeftX, LeftY);
    PosD2.Init(RightX, RightY);
    PInfo^.ConvertToDisp(PosD1, Pos1);
    PInfo^.ConvertToDisp(PosD2, Pos2);
    if Selected then
      Pen := GetPen(RGBColors[c_LBlue], lt_Dot, 0)
    else
      Pen := GetPen(PLAColor, Item^.Pen, 0);
    OldPen := SelectObject(PInfo^.ExtCanvas.Handle, Pen);
    Brush := GetBrush(RGBColors[c_White], pt_Solid, PInfo^.ProjStyles.XFillStyles);
    OldBrush := SelectObject(PInfo^.ExtCanvas.Handle, Brush);
    Rectangle(PInfo^.ExtCanvas.Handle, Pos1.X, Pos1.Y, Pos2.X, Pos2.Y);
    Middle := Round(Position.Y - GesHeight + BHeight / 100 * Valu);
    PosD1.Init(LeftX, Middle);
    PosD2.Init(RightX - 1, Middle);
    PInfo^.ConvertToDisp(PosD1, Pos1);
    PInfo^.ConvertToDisp(PosD2, Pos2);
    MoveTo(PInfo^.ExtCanvas.Handle, Pos1.X, Pos1.Y);
    LineTo(PInfo^.ExtCanvas.Handle, Pos2.X, Pos2.Y);
    Middle := Round(Position.X + Num * BWidth + BWidth / 2);
    if Num > 0 then
      Middle := Middle - 1;
    PosD1.Init(Middle, LeftY + 1);
    PosD2.Init(Middle, RightY);
    PInfo^.ConvertToDisp(PosD1, Pos1);
    PInfo^.ConvertToDisp(PosD2, Pos2);
    MoveTo(PInfo^.ExtCanvas.Handle, Pos1.X, Pos1.Y);
    LineTo(PInfo^.ExtCanvas.Handle, Pos2.X, Pos2.Y);
    SelectObject(PInfo^.ExtCanvas.Handle, OldBrush);
    DeleteObject(Brush);
    SelectObject(PInfo^.ExtCanvas.Handle, OldPen);
    DeleteObject(Pen);
    if ShowValues then
      DrawValText(Valu);
    Inc(Num);
  end;
{**************************************** Hauptteil ****************************************}
begin
  try
    if (Visible(PInfo) or PInfo^.GetDrawMode(dm_SelGra)) and (ChartKindPtr <> nil) then
    begin
      if PInfo^.GetDrawMode(dm_SelGra) then
        Selected := TRUE
      else
        Selected := FALSE;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}
      if ShowOld then // temporarily for DEBUG
      begin
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
        if DiagType = dtCircleDiag then
        begin
          if (Sum <> 0) then
          begin
            StartRX := Position.X; {Koordinaten des Begrenzungsrechteckes}
            StartRY := Position.Y - CDiameter;
            EndRX := StartRX + CDiameter;
            EndRY := StartRY + CDiameter;
            PosDraw1.Init(StartRX, StartRY);
            PosDraw2.Init(EndRX, EndRY);
            PInfo^.ConvertToDisp(PosDraw1, PosDisp1);
            PInfo^.ConvertToDisp(PosDraw2, PosDisp2);
            Dist := CDiameter / 2;
            EndX := EndRX - Trunc(Dist); {Diagramm f�ngt oben mittig an und l�uft im Uhrzeigersinn}
            EndY := EndRY;
            Angle := Pi / 2;
            Piece := 0;
            ValItems^.ForEach(@DrawCircle);
          end;
        end
        else
          if DiagType = dtBeamDiag then
          begin
            Num := 0;
            ValItems^.ForEach(@DrawBeam);
          end
          else
            if DiagType = dtAreaDiag then
            begin
              Num := 0;
              MinVal := 0;
              ValItems^.ForEach(@GetMinVal);
              MinVal := Round(-MinVal + BWidth / 5);
              DrawBorder;
              GetMem(AreaPoints, Word(SizeOf(TPoint)) * (ValItems^.Count + 2));
              ValItems^.ForEach(@GetAreaPoints);
              AreaPos.Init(Round(Position.X + GesWidth - BWidth / 2 - 1), Position.Y - GesHeight);
              PInfo^.ConvertToDisp(AreaPos, AreaDPos);
              AreaPoints^[Num + 1] := AreaDPos;
              Inc(Num);
              AreaPos.Init(Round(Position.X + BWidth / 2), Position.Y - GesHeight);
              PInfo^.ConvertToDisp(AreaPos, AreaDPos);
              AreaPoints^[Num + 1] := AreaDPos;
              DrawArea;
              FreeMem(AreaPoints, Word(SizeOf(TPoint)) * (ValItems^.Count + 2));
            end
            else
              if DiagType = dtLineDiag then
              begin
                Num := 0;
                MinVal := 0;
                ValItems^.ForEach(@GetMinVal);
                MinVal := Round(-MinVal + BWidth / 5);
                DrawBorder;
                ValItems^.ForEach(@DrawLine);
              end
              else
                if DiagType = dtPointDiag then
                begin
                  Num := 0;
                  MinVal := 0;
                  ValItems^.ForEach(@GetMinVal);
                  MinVal := Round(-MinVal + BWidth / 5);
                  DrawBorder;
                  ValItems^.ForEach(@DrawPoint);
                end;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
      end; // end of 'if ShowOld then' - statement (temporarily for DEBUG)

      TmpChart := TChart.Create(ChartKindPtr^.TemplateChart.Parent);
      TmpChart.Assign(ChartKindPtr^.TemplateChart);

      case DiagType of
        dtCircleDiag:
          begin
            TmpCrtSeries := TPieSeries.Create(TmpChart);
            TPieSeries(TmpCrtSeries).Assign(ChartKindPtr^.TemplateChartSeries);
          end;
        dtBeamDiag:
          begin
            TmpCrtSeries := TBarSeries.Create(TmpChart);
            TBarSeries(TmpCrtSeries).Assign(ChartKindPtr^.TemplateChartSeries);
          end;
        dtLineDiag:
          begin
            TmpCrtSeries := TLineSeries.Create(TmpChart);
            TLineSeries(TmpCrtSeries).Assign(ChartKindPtr^.TemplateChartSeries);
          end;
        dtAreaDiag:
          begin
            TmpCrtSeries := TAreaSeries.Create(TmpChart);
            TAreaSeries(TmpCrtSeries).Assign(ChartKindPtr^.TemplateChartSeries);
          end;
        dtPointDiag:
          begin
            TmpCrtSeries := TPointSeries.Create(TmpChart);
            TPointSeries(TmpCrtSeries).Assign(ChartKindPtr^.TemplateChartSeries);
          end;
      end; // End of CASE-statement

      TmpCrtSeries.ParentChart := TmpChart;

      StartRX := Position.X + OffsetX;
      StartRY := Position.Y - ChartHeight + OffsetY;
      EndRX := StartRX + ChartWidth;
      EndRY := StartRY + ChartHeight;
      PosDraw1.Init(StartRX, StartRY);
      PosDraw2.Init(EndRX, EndRY);
      PInfo^.ConvertToDisp(PosDraw1, TmpRect.TopLeft);
      PInfo^.ConvertToDisp(PosDraw2, TmpRect.BottomRight);
      BmpRect := TmpRect;
      LPtoDP(PInfo^.ExtCanvas.BitmapDC, BmpRect, 2);

      tmpWidth := RectWidth(BmpRect);
      tmpHeight := RectHeight(BmpRect);

      case DiagType of
        dtCircleDiag:
          begin
            if RelAbsSize then
            begin
              dblRatio := Sum / (100 * co_RelChartPercent);
              TPieSeries(TmpCrtSeries).CustomXRadius := Round(TPieSeries(ChartKindPtr^.TemplateChartSeries).CustomXRadius * dblRatio);
              TPieSeries(TmpCrtSeries).CustomYRadius := Round(TPieSeries(ChartKindPtr^.TemplateChartSeries).CustomYRadius * dblRatio);
              dblRatio := ChartKindPtr^.TemplateChart.Width + (TPieSeries(TmpCrtSeries).CustomXRadius - TPieSeries(ChartKindPtr^.TemplateChartSeries).CustomXRadius) * 2;
              dblRatio := tmpWidth / dblRatio;
            end
            else
            begin
              dblRatio := tmpWidth / ChartKindPtr^.TemplateChart.Width;
            end;
            TPieSeries(TmpCrtSeries).CustomXRadius := Round(TPieSeries(TmpCrtSeries).CustomXRadius * dblRatio);
            TPieSeries(TmpCrtSeries).CustomYRadius := Round(TPieSeries(TmpCrtSeries).CustomYRadius * dblRatio);
            TPieSeries(TmpCrtSeries).RotationAngle := TPieSeries(ChartKindPtr^.TemplateChartSeries).RotationAngle;
          end;
        dtBeamDiag:
          begin
            dblRatio := tmpHeight / co_BaseChartSize;
          end;
        dtLineDiag:
          begin
            dblRatio := tmpHeight / co_BaseChartSize;
          end;
        dtAreaDiag:
          begin
            dblRatio := tmpHeight / co_BaseChartSize;
          end;
        dtPointDiag:
          begin
            dblRatio := tmpHeight / co_BaseChartSize;
            TPointSeries(TmpCrtSeries).Pointer.HorizSize := Round(TPointSeries(TmpCrtSeries).Pointer.HorizSize * dblRatio);
            TPointSeries(TmpCrtSeries).Pointer.VertSize := Round(TPointSeries(TmpCrtSeries).Pointer.VertSize * dblRatio);
          end;
      end; // End of CASE-statement

      if TmpChart.AxisVisible then
      begin
        if TmpChart.LeftAxis.Visible then
          if TmpChart.LeftAxis.Labels then
          begin
            THeight := Round(TmpChart.LeftAxis.LabelsFont.Height * dblRatio);
            if THeight < co_TextVisible then
              TmpChart.LeftAxis.Labels := FALSE
            else
              TmpChart.LeftAxis.LabelsFont.Height := THeight;
            TmpChart.LeftAxis.LabelsSize := Round(TmpChart.LeftAxis.LabelsSize * dblRatio);
          end;
        if TmpChart.BottomAxis.Visible then
          if TmpChart.BottomAxis.Labels then
          begin
            THeight := Round(TmpChart.BottomAxis.LabelsFont.Height * dblRatio);
            if THeight < co_TextVisible then
              TmpChart.BottomAxis.Labels := FALSE
            else
              TmpChart.BottomAxis.LabelsFont.Height := THeight;
            TmpChart.BottomAxis.LabelsSize := Round(TmpChart.BottomAxis.LabelsSize * dblRatio);
          end;
      end;

      if TmpCrtSeries.Marks.Visible then
      begin
        THeight := Round(TmpCrtSeries.Marks.Font.Height * dblRatio);
        TmpCrtSeries.Marks.Font.Height := THeight;
      end;

      if (tmpWidth < WingisMainForm.ActualChild.ClientWidth * 3) or (tmpHeight < WingisMainForm.ActualChild.ClientHeight * 3) then
      begin

        if not TmpChart.BottomWall.Transparent then
        begin
          PInfo^.ExtCanvas.FillRect(TmpRect, CreateSolidBrush(TmpChart.Color));
        end;

        TmpChart.Width := tmpWidth;
        TmpChart.Height := tmpHeight;
        TmpChart.Top := -TmpChart.Height;
        TmpChart.Left := -TmpChart.Width;

        TmpChart.BackImage.Bitmap.Width := TmpChart.Width;
        TmpChart.BackImage.Bitmap.Height := TmpChart.Height;
        PInfo^.ExtCanvas.GetBitmapPartCopy(TmpChart.BackImage.Bitmap, TmpRect);

        case DiagType of
          dtCircleDiag:
            begin
              for Cnt := 0 to ValItems^.Count - 1 do
              begin
                TmpItem := PDGraph(ValItems^.Items[Cnt]);
                TmpValue := TmpItem.Value;
                TPieSeries(TmpCrtSeries).AddPie(TmpValue, TmpItem.Name, TmpItem.Color);
              end;
            end;
          dtBeamDiag:
            begin
              for Cnt := 0 to ValItems^.Count - 1 do
              begin
                TmpItem := PDGraph(ValItems^.Items[Cnt]);
                TmpValue := TmpItem.Value;
                TBarSeries(TmpCrtSeries).AddBar(TmpValue, TmpItem.Name, TmpItem.Color);
              end;
            end;
          dtLineDiag:
            begin
              for Cnt := 0 to ValItems^.Count - 1 do
              begin
                TmpItem := PDGraph(ValItems^.Items[Cnt]);
                TmpValue := TmpItem.Value;
                TLineSeries(TmpCrtSeries).Add(TmpValue, TmpItem.Name, TmpItem.Color);
              end;
            end;
          dtAreaDiag:
            begin
              for Cnt := 0 to ValItems^.Count - 1 do
              begin
                TmpItem := PDGraph(ValItems^.Items[Cnt]);
                TmpValue := TmpItem.Value;
                TAreaSeries(TmpCrtSeries).Add(TmpValue, TmpItem.Name, TmpItem.Color);
              end;
            end;
          dtPointDiag:
            begin
              for Cnt := 0 to ValItems^.Count - 1 do
              begin
                TmpItem := PDGraph(ValItems^.Items[Cnt]);
                TmpValue := TmpItem.Value;
                TPointSeries(TmpCrtSeries).Add(TmpValue, TmpItem.Name, TmpItem.Color);
              end;
            end;
        end; // End of CASE-statement

// TODO
//        TmpChart.Draw(PInfo^.ExtCanvas, TmpRect);
//        TmpChart.DrawDirectly(PInfo^.ExtCanvas.Handle, TmpRect);
      end;

      if (Selected) then
      begin
        PosDraw1.x := ClipRect.A.x;
        PosDraw1.y := ClipRect.A.y;
        PosDraw2.x := ClipRect.B.x;
        PosDraw2.y := ClipRect.B.y;
        Cnt := SizeOf(TPoint) * 5;
        GetMem(Points, Cnt);
        PInfo^.ConvertToDisp(PosDraw1, Points^[1]);
        PosDraw3.Init(PosDraw1.X, PosDraw2.Y);
        PInfo^.ConvertToDisp(PosDraw3, Points^[2]);
        PInfo^.ConvertToDisp(PosDraw2, Points^[3]);
        PosDraw3.Init(PosDraw2.X, PosDraw1.Y);
        PInfo^.ConvertToDisp(PosDraw3, Points^[4]);
        Points^[5] := Points^[1];
        PInfo^.ExtCanvas.Polygon(Points^, 5, TRUE);
        FreeMem(Points, Cnt);
      end;
      TmpChart.FreeAllSeries;
      TmpChart.Free;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
    end;
  finally
  end;
end;

{**************************************** GetObjType *************************************************************************}

function TBusGraph.GetObjType: Word;
begin
  GetObjType := ot_BusGraph;
end;

{**************************************** MoveRel ****************************************************************************}

procedure TBusGraph.MoveRel(XMove: LongInt; YMove: LongInt);
begin
  TView.MoveRel(XMove, YMove);
  Position.Move(XMove, YMove);
end;

{**************************************** CalculateClipRect ******************************************************************}

{++ Moskaliov Business Graphics BUILD#150 17.01.01}
{Procedure TBusGraph.CalculateClipRect(PInfo: PPaint);
var Border       : PCPoly;
begin
   Border:=GetBorder(PInfo);
   ClipRect:=Border^.ClipRect;
   Dispose(Border,Done);   }

procedure TBusGraph.CalculateClipRect;
begin
  ClipRect.A.X := Position.X + OffsetX;
  ClipRect.A.Y := Position.Y - ChartHeight + OffsetY;
  ClipRect.B.X := ClipRect.A.X + ChartWidth;
  ClipRect.B.Y := ClipRect.A.Y + ChartHeight;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
end;

{**************************************** SelectByPoint **********************************************************************}

function TBusGraph.SelectByPoint(PInfo: PPaint; Point: TDPoint;
  const ObjType: TObjectTypes): PIndex;
var
  Pos1: TDPoint;
  Pos2: TDPoint;
  Pos3: TDPoint;
  dblA: Double;
  dblB: Double;
  Dist: Double;
  MDist: Double;
begin
  SelectByPoint := nil;
  if (ot_BusGraph in ObjType) and (ClipRect.PointInside(Point)) then
    Result := @Self;
end;

{**************************************** SelectByCircle *********************************************************************}

function TBusGraph.SelectByCircle(PInfo: PPaint; Middle: TDPoint; Radius: LongInt;
  const ObjType: TObjectTypes; Inside: Boolean): Boolean;
var
  CPoly: PCPoly;
  Pos: TDPoint;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
  Pos1: TDPoint;
  Pos2: TDPoint;
  dblA: Double;
  dblB: Double;
  dblK: Double;
  dblValue: Double;
begin
{   SelectByCircle:=FALSE;
   if (ot_BusGraph in ObjType) and (DiagType = dtCircleDiag) then
      begin
         Pos.Init(Round(Position.X+CDiameter/2),Round(Position.Y-CDiameter/2));
         SelectByCircle:=Middle.Dist(Pos)-CDiameter/2<Radius
      end
   else
      if (ot_BusGraph in ObjType) and ((DiagType = dtBeamDiag) or (DiagType = dtAreaDiag)
            or (DiagType = dtLineDiag) or (DiagType = dtPointDiag)) then
         begin
            CPoly:=GetBorder(PInfo);
            SelectByCircle:=CPoly^.SelectByCircle(PInfo,Middle,Radius,[ot_CPoly],Inside);
            Dispose(CPoly,Done);
         end; }

  Result := FALSE;
  if ot_BusGraph in ObjType then
  begin
    Pos1.X := Position.X + OffsetX;
    Pos1.Y := Position.Y - ChartHeight + OffsetY;
    Pos2.X := Pos1.X + ChartWidth;
    Pos2.Y := Pos1.Y + ChartHeight;
    Pos1.X := Pos1.X + Round((ChartKindPtr^.TemplateChart.MarginLeft * ChartWidth) / 100.0);
    Pos1.Y := Pos1.Y + Round((ChartKindPtr^.TemplateChart.MarginBottom * ChartHeight) / 100.0);
    Pos2.X := Pos2.X - Round((ChartKindPtr^.TemplateChart.MarginRight * ChartWidth) / 100.0);
    Pos2.Y := Pos2.Y - Round((ChartKindPtr^.TemplateChart.MarginTop * ChartHeight) / 100.0);

    CPoly := New(PCPoly, Init);
    CPoly^.InsertPoint(Pos1);
    Pos.Init(Pos1.X, Pos2.Y);
    CPoly^.InsertPoint(Pos);
    CPoly^.InsertPoint(Pos2);
    Pos.Init(Pos2.X, Pos1.Y);
    CPoly^.InsertPoint(Pos);
    CPoly^.ClosePoly;

    Result := CPoly^.SelectByCircle(PInfo, Middle, Radius, [ot_CPoly], Inside);
    if DiagType = dtCircleDiag then
      if Inside then
      begin
        if not Result then
          if CPoly^.SelectByCircle(PInfo, Middle, Radius, [ot_CPoly], FALSE) then
          begin
            Pos.Init(Round((Pos1.X + Pos2.X) * 0.5), Round((Pos1.Y + Pos2.Y) * 0.5));
            dblA := Abs(Pos2.X - Pos1.X) * 0.5;
            dblB := Abs(Pos2.Y - Pos1.Y) * 0.5;
            if Middle.Dist(Pos) < Radius then
              if Middle.X = Pos.X then
                if Middle.Y = Pos.Y then
                  Result := (dblA <= Radius) and (dblB <= Radius)
                else
                  Result := (Abs(Middle.X - Pos.X) + dblA) <= Radius
              else
                if Middle.Y = Pos.Y then
                  Result := (Abs(Middle.Y - Pos.Y) + dblB) <= Radius
                else
                begin
                  Pos2.Init(Middle.X - Pos.X, Middle.Y - Pos.Y);
                  dblK := Pos2.Y / Pos2.X;
                  dblValue := dblB / dblA + (dblA / dblB) * dblK * dblK;
                  dblValue := Sqrt(dblA * dblB / dblValue);
                  Pos1.Init(Round(dblValue), Round(dblK * dblValue));
                  Pos2.Init(-Pos1.X, -Pos1.Y);
                  Pos1.X := Pos1.X + Pos.X;
                  Pos1.Y := Pos1.Y + Pos.Y;
                  Pos2.X := Pos2.X + Pos.X;
                  Pos2.Y := Pos2.Y + Pos.Y;
                  dblA := Middle.Dist(Pos1);
                  dblB := Middle.Dist(Pos2);
                  if dblA < dblB then
                    Result := dblB <= Radius
                  else
                    Result := dblA <= Radius;
                end;
          end;
      end
      else
        if Result then
        begin
          Pos.Init(Round((Pos1.X + Pos2.X) * 0.5), Round((Pos1.Y + Pos2.Y) * 0.5));
          dblA := Abs(Pos2.X - Pos1.X) * 0.5;
          dblB := Abs(Pos2.Y - Pos1.Y) * 0.5;
          if Middle.X = Pos.X then
            Result := TRUE
          else
            if Middle.Y = Pos.Y then
              Result := TRUE
            else
            begin
              Pos2.Init(Middle.X - Pos.X, Middle.Y - Pos.Y);
              dblK := Pos2.Y / Pos2.X;
              dblValue := dblB / dblA + (dblA / dblB) * dblK * dblK;
              dblValue := Sqrt(dblA * dblB / dblValue);
              Pos1.Init(Round(dblValue), Round(dblK * dblValue));
              Pos2.Init(-Pos1.X, -Pos1.Y);
              Pos1.X := Pos1.X + Pos.X;
              Pos1.Y := Pos1.Y + Pos.Y;
              Pos2.X := Pos2.X + Pos.X;
              Pos2.Y := Pos2.Y + Pos.Y;
              dblA := Middle.Dist(Pos1);
              dblB := Middle.Dist(Pos2);
              if dblA < dblB then
                Result := dblA <= Radius
              else
                Result := dblB <= Radius;
            end;
        end;
    Dispose(CPoly, Done);
  end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
end;

{**************************************** SelectByPoly ***********************************************************************}

function TBusGraph.SelectByPoly(PInfo: PPaint; Poly: Pointer; const ObjType: TObjectTypes;
  Inside: Boolean): Boolean;
var
  DPoly: PCPoly;
  Pos: TDPoint;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
  Cnt: Integer;
  Pos1: TDPoint;
  Pos2: TDPoint;
  Pos3: TDPoint;
  dblA: Double;
  dblB: Double;
  dblK: Double;
  dblValue: Double;
begin
{   SelectByPoly:=FALSE;
   if (ot_BusGraph in ObjType) and ClipRect.IsVisible(PView(Poly)^.ClipRect) then
      begin
         if (DiagType = dtCircleDiag) then
            begin
               Pos.Init(Position.X,Position.Y);
               if Inside then
                  SelectByPoly:=ClipRect.IsInside(PView(Poly)^.ClipRect)
                     and not PCPoly(Poly)^.CheckNormalDist(PInfo,@Pos,Round(CDiameter/2))
               else
                  SelectByPoly:=PCPoly(Poly)^.CheckPointDist(@Pos,Round(CDiameter/2))
                     or PCPoly(Poly)^.CheckNormalDist(PInfo,@Pos,Round(CDiameter/2))
                     or PCPoly(Poly)^.PointInside(PInfo,Pos);
            end
         else
            if ((DiagType = dtBeamDiag) or (DiagType = dtAreaDiag)
                  or (DiagType = dtLineDiag) or (DiagType = dtPointDiag)) then
               begin
                  DPoly:=GetBorder(PInfo);
                  SelectByPoly:=DPoly^.SelectByPoly(PInfo,Poly,[ot_CPoly],Inside);
                  Dispose(DPoly,Done);
               end;
      end;   }

  Result := FALSE;
  if (ot_BusGraph in ObjType) and ClipRect.IsVisible(PView(Poly)^.ClipRect) then
  begin
    Pos1.X := Position.X + OffsetX;
    Pos1.Y := Position.Y - ChartHeight + OffsetY;
    Pos2.X := Pos1.X + ChartWidth;
    Pos2.Y := Pos1.Y + ChartHeight;
    Pos1.X := Pos1.X + Round((ChartKindPtr^.TemplateChart.MarginLeft * ChartWidth) / 100.0);
    Pos1.Y := Pos1.Y + Round((ChartKindPtr^.TemplateChart.MarginBottom * ChartHeight) / 100.0);
    Pos2.X := Pos2.X - Round((ChartKindPtr^.TemplateChart.MarginRight * ChartWidth) / 100.0);
    Pos2.Y := Pos2.Y - Round((ChartKindPtr^.TemplateChart.MarginTop * ChartHeight) / 100.0);

    DPoly := New(PCPoly, Init);
    DPoly^.InsertPoint(Pos1);
    Pos.Init(Pos1.X, Pos2.Y);
    DPoly^.InsertPoint(Pos);
    DPoly^.InsertPoint(Pos2);
    Pos.Init(Pos2.X, Pos1.Y);
    DPoly^.InsertPoint(Pos);
    DPoly^.ClosePoly;

    Result := DPoly^.SelectByPoly(PInfo, Poly, [ot_CPoly], Inside);
    if DiagType = dtCircleDiag then
      if Inside then
      begin
        if not Result then
          if DPoly^.SelectByPoly(PInfo, Poly, [ot_CPoly], FALSE) then
          begin
            Dispose(DPoly, Done);
            Pos.Init(Round((Pos1.X + Pos2.X) * 0.5), Round((Pos1.Y + Pos2.Y) * 0.5));
            DPoly := New(PCPoly, Init);
            Pos3.Init(Pos1.X, Pos.Y);
            DPoly^.InsertPoint(Pos3);
            Pos3.Init(Pos.X, Pos2.Y);
            DPoly^.InsertPoint(Pos3);
            Pos3.Init(Pos2.X, Pos.Y);
            DPoly^.InsertPoint(Pos3);
            Pos3.Init(Pos.X, Pos1.Y);
            DPoly^.InsertPoint(Pos3);
            DPoly^.ClosePoly;
            if DPoly^.SelectByPoly(PInfo, Poly, [ot_CPoly], FALSE) then
            begin
              dblA := Abs(Pos2.X - Pos1.X) * 0.5;
              dblB := Abs(Pos2.Y - Pos1.Y) * 0.5;
              dblK := dblB / dblA;
              Result := TRUE;
              for Cnt := 0 to PCPoly(Poly)^.Data^.Count - 2 do
              begin
                with PCPoly(Poly)^.Data^ do
                  Pos3.Init(PDPoint(At(Cnt))^.X, PDPoint(At(Cnt))^.Y);
                Pos3.X := Pos3.X - Pos.X;
                Pos3.Y := Pos3.Y - Pos.Y;
                if Abs(Pos3.X) < dblA then
                begin
                  dblValue := dblK * Sqrt((dblA - Pos3.X) * (dblA + Pos3.X));
                  if Abs(Pos3.Y) < dblValue then
                  begin
                    Result := FALSE;
                    Break;
                  end;
                end;
              end;
            end;
          end;
      end
      else
        if Result then
        begin
          Dispose(DPoly, Done);
          Pos.Init(Round((Pos1.X + Pos2.X) * 0.5), Round((Pos1.Y + Pos2.Y) * 0.5));
          DPoly := New(PCPoly, Init);
          Pos3.Init(Pos1.X, Pos.Y);
          DPoly^.InsertPoint(Pos3);
          Pos3.Init(Pos.X, Pos2.Y);
          DPoly^.InsertPoint(Pos3);
          Pos3.Init(Pos2.X, Pos.Y);
          DPoly^.InsertPoint(Pos3);
          Pos3.Init(Pos.X, Pos1.Y);
          DPoly^.InsertPoint(Pos3);
          DPoly^.ClosePoly;
          Result := DPoly^.SelectByPoly(PInfo, Poly, [ot_CPoly], FALSE);
          if not Result then
          begin
            dblA := Abs(Pos2.X - Pos1.X) * 0.5;
            dblB := Abs(Pos2.Y - Pos1.Y) * 0.5;
            dblK := dblB / dblA;
            for Cnt := 0 to PCPoly(Poly)^.Data^.Count - 2 do
            begin
              with PCPoly(Poly)^.Data^ do
                Pos3.Init(PDPoint(At(Cnt))^.X, PDPoint(At(Cnt))^.Y);
              Pos3.X := Pos3.X - Pos.X;
              Pos3.Y := Pos3.Y - Pos.Y;
              if Abs(Pos3.X) <= dblA then
              begin
                dblValue := dblK * Sqrt((dblA - Pos3.X) * (dblA + Pos3.X));
                if Abs(Pos3.Y) <= dblValue then
                begin
                  Result := TRUE;
                  Break;
                end;
              end;
            end;
          end;
        end;
    Dispose(DPoly, Done);
  end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
end;

{**************************************** SelectBetweenPolys *****************************************************************}

function TBusGraph.SelectBetweenPolys(PInfo: PPaint; Poly1: Pointer; Poly2: Pointer;
  const ObjType: TObjectTypes; Inside: Boolean): Boolean;
var
  DPoly: PCPoly;
  Pos: TDPoint;
  DCircle: TEllipse;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
  Pos1: TDPoint;
  Pos2: TDPoint;
  dblA: Double;
  dblB: Double;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
begin
  SelectBetweenPolys := FALSE;
  if (GetObjType in ObjType) and (ClipRect.IsVisible(PView(Poly1)^.ClipRect)
    or ClipRect.IsVisible(PView(Poly2)^.ClipRect)) then
  begin
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
    Pos1.X := Position.X + OffsetX;
    Pos1.Y := Position.Y - ChartHeight + OffsetY;
    Pos2.X := Pos1.X + ChartWidth;
    Pos2.Y := Pos1.Y + ChartHeight;
    Pos1.X := Pos1.X + Round((ChartKindPtr^.TemplateChart.MarginLeft * ChartWidth) / 100.0);
    Pos1.Y := Pos1.Y + Round((ChartKindPtr^.TemplateChart.MarginBottom * ChartHeight) / 100.0);
    Pos2.X := Pos2.X - Round((ChartKindPtr^.TemplateChart.MarginRight * ChartWidth) / 100.0);
    Pos2.Y := Pos2.Y - Round((ChartKindPtr^.TemplateChart.MarginTop * ChartHeight) / 100.0);
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
    if (DiagType = dtCircleDiag) then
    begin
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
{--            Pos.Init(Position.X+Trunc(CDiameter/2),Position.Y-Trunc(CDiameter/2));
               DCircle.Init(Pos,Trunc(CDiameter/2),Trunc(CDiameter/2),0);       }
      Pos.Init(Round((Pos1.X + Pos2.X) * 0.5), Round((Pos1.Y + Pos2.Y) * 0.5));
      dblA := Abs(Pos2.X - Pos1.X) * 0.5;
      dblB := Abs(Pos2.Y - Pos1.Y) * 0.5;
      DCircle.Init(Pos, Round(dblA), Round(dblB), 0);
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
      SelectBetweenPolys := DCircle.SelectBetweenPolys(PInfo, Poly1, Poly2, [ot_Circle], Inside);
      DCircle.Done;
      Pos.Done;
    end
    else
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
{--         if ((DiagType = dtBeamDiag) or (DiagType = dtAreaDiag)
                  or (DiagType = dtLineDiag) or (DiagType = dtPointDiag)) then }
    begin
{--            DPoly:=GetBorder(PInfo);                                        }
      DPoly := New(PCPoly, Init);
      DPoly^.InsertPoint(Pos1);
      Pos.Init(Pos1.X, Pos2.Y);
      DPoly^.InsertPoint(Pos);
      DPoly^.InsertPoint(Pos2);
      Pos.Init(Pos2.X, Pos1.Y);
      DPoly^.InsertPoint(Pos);
      DPoly^.ClosePoly;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
      SelectBetweenPolys := DPoly^.SelectBetweenPolys(PInfo, Poly1, Poly2, [ot_CPoly], Inside);
      Dispose(DPoly, Done);
    end;
  end;
end;

{**************************************** GetBorder **************************************************************************}

function TBusGraph.GetBorder
  (
  PInfo: PPaint
  )
  : PCPoly;
var
  APoly: PCPoly;
  APoint: TDPoint;
  MDist: LongInt;
  LastVal: Real;
  Num: Integer;
  Re: Integer;
  TextLen: LongInt;
  LastTextLen: LongInt;
  LeRi: LongInt;
  ToBo: LongInt;
  MinVal: LongInt;
{**************************************** positive Teile des Balkendiagrammes ****************************************}

  procedure DoAllUp
      (
      Item: PDGraph
      ); far;
  var
    Val: Real;
    TText: string;
    PercentVal: Real;
  begin
    Val := Item^.Value;
    if Val < 0 then
    begin
      if ShowValues then
      begin
        if AnnotAbsRel then
          Str(Item^.Value: 0: 0, TText)
        else
        begin
          PercentVal := 100 / Sum * Item^.Value;
          Str(PercentVal: 0: 2, TText);
          TText := TText + ' %';
        end;
        TextLen := Round(SVFont.Height * PInfo^.GetTextRatio(SVFont, TText));
        Val := Trunc((TextLen + SVFont.Height / 5) * 100 / BHeight);
      end
      else
        Val := 0;
    end;
    if Num > 0 then
    begin
      if LastVal <= Val then
        APoint.Init(Position.X + Num * BWidth - MDist, Trunc(Position.Y - GesHeight + BHeight / 100 * LastVal) + MDist)
      else
        APoint.Init(Position.X + Num * BWidth + MDist, Trunc(Position.Y - GesHeight + BHeight / 100 * LastVal) + MDist);
      APoly^.InsertPoint(APoint);
    end;
    if LastVal <= Val then
      APoint.Init(Position.X + Num * BWidth - MDist, Trunc(Position.Y - GesHeight + BHeight / 100 * Val) + MDist)
    else
      APoint.Init(Position.X + Num * BWidth + MDist, Trunc(Position.Y - GesHeight + BHeight / 100 * Val) + MDist);
    APoly^.InsertPoint(APoint);
    LastVal := Val;
    Inc(Num);
  end;
{**************************************** negative Teile des Balkendiagrammes ****************************************}

  procedure DoAllDown
      (
      Item: PDGraph
      ); far;
  var
    Val: Real;
    TText: string;
    PercentVal: Real;
    LastSize: Real;
    Size: Real;
  begin
    Val := Item^.Value;
    if Val >= 0 then
    begin
      if ShowValues then
      begin
        if AnnotAbsRel then
          Str(Item^.Value: 0: 0, TText)
        else
        begin
          PercentVal := 100 / Sum * Item^.Value;
          Str(PercentVal: 0: 2, TText);
          TText := TText + ' %';
        end;
        TextLen := Round(SVFont.Height * PInfo^.GetTextRatio(SVFont, TText));
        Val := Trunc((-TextLen - SVFont.Height / 5) * 100 / BHeight);
      end
      else
        Val := 0;
    end;
    if Num < ValItems^.Count then
    begin
      if LastVal >= Val then
        APoint.Init(Position.X + Num * BWidth + MDist, Trunc(Position.Y - GesHeight + BHeight / 100 * LastVal) - MDist)
      else
        APoint.Init(Position.X + Num * BWidth - MDist, Trunc(Position.Y - GesHeight + BHeight / 100 * LastVal) - MDist);
      APoly^.InsertPoint(APoint);
    end;
    if LastVal >= Val then
      APoint.Init(Position.X + Num * BWidth + MDist, Trunc(Position.Y - GesHeight + BHeight / 100 * Val) - MDist)
    else
      APoint.Init(Position.X + Num * BWidth - MDist, Trunc(Position.Y - GesHeight + BHeight / 100 * Val) - MDist);
    APoly^.InsertPoint(APoint);
    LastVal := Val;
    Dec(Num);
  end;
{**************************************** gr��te Beschriftungsl�nge berechnen ****************************************}

  function GetMaxTextLen: LongInt;
  var
    MaxLen: LongInt;

    procedure DoAll
        (
        Item: PDGraph
        ); far;
    var
      TText: string;
      PercentVal: Real;
      Val: Real;
    begin
      if AnnotAbsRel then
        Str(Item^.Value: 0: 0, TText)
      else
      begin
        PercentVal := 100 / Sum * Item^.Value;
        Str(PercentVal: 0: 2, TText);
        TText := TText + ' %';
      end;
      TextLen := Round(SVFont.Height * PInfo^.GetTextRatio(SVFont, TText));
      if TextLen > MaxLen then
        MaxLen := TextLen;
    end;
  begin
    MaxLen := 0;
    ValItems^.ForEach(@DoAll);
    GetMaxTextLen := MaxLen;
  end;
{**************************************** gr��te Ausdehnung im Minusbereich ****************************************}

  procedure GetMinVal
      (
      Item: PDGraph
      ); far;
  var
    Val: LongInt;
  begin
    Val := Round(BHeight / 100 * Item^.Value);
    if Val < MinVal then
      MinVal := Val;
  end;
{*********************** Rand der Beschriftungen bei Punkte-, Linien- und Fl�chendiagrammen ***********************}

  procedure MakeTextBorder
      (
      Item: PDGraph
      ); far;
  var
    Val: LongInt;
    TText: string;
    PercentVal: Real;
  begin
    if AnnotAbsRel then
      Str(Item^.Value: 0: 0, TText)
    else
    begin
      PercentVal := 100 / Sum * Item^.Value;
      Str(PercentVal: 0: 2, TText);
      TText := TText + ' %';
    end;
    TextLen := Round(SVFont.Height * PInfo^.GetTextRatio(SVFont, TText));
    Val := Trunc(TextLen + SVFont.Height / 5);
    APoint.Init(Round(Position.X + Re * BWidth + BWidth / 2 + SVFont.Height / 2 + MDist), Position.Y - GesHeight - MinVal - MDist);
    APoly^.InsertPoint(APoint);
    APoint.Init(Round(Position.X + Re * BWidth + BWidth / 2 + SVFont.Height / 2 + MDist), Position.Y - GesHeight - MinVal - Val - MDist);
    APoly^.InsertPoint(APoint);
    APoint.Init(Round(Position.X + Re * BWidth + BWidth / 2 - SVFont.Height / 2 - MDist), Position.Y - GesHeight - MinVal - Val - MDist);
    APoly^.InsertPoint(APoint);
    APoint.Init(Round(Position.X + Re * BWidth + BWidth / 2 - SVFont.Height / 2 - MDist), Position.Y - GesHeight - MinVal - MDist);
    APoly^.InsertPoint(APoint);
  end;
{**************************************** Hauptteil ****************************************}
begin
  MDist := PInfo^.CalculateDraw(ciMaxDist);
  if DiagType = dtCircleDiag then
  begin { Kreisdiagramme }
    if ShowValues then
    begin
      LeRi := GetMaxTextLen + MDist;
      ToBo := Trunc(SVFont.Height * 1.5 + MDist);
    end
    else
    begin
      LeRi := MDist;
      ToBo := MDist;
    end;
    APoly := New(PCPoly, Init);
    APoint.Init(Position.X - LeRi, Position.Y + ToBo);
    APoly^.InsertPoint(APoint);
    APoint.Init(Position.X + GesWidth + LeRi, Position.Y + ToBo);
    APoly^.InsertPoint(APoint);
    APoint.Init(Position.X + GesWidth + LeRi, Position.Y - GesHeight - ToBo);
    APoly^.InsertPoint(APoint);
    APoint.Init(Position.X - LeRi, Position.Y - GesHeight - ToBo);
    APoly^.InsertPoint(APoint);
    APoly^.ClosePoly;
    GetBorder := APoly;
  end
  else
    if DiagType = dtBeamDiag then
    begin { Balkendiagramme }
      Num := 0;
      LastVal := 0;
      APoly := New(PCPoly, Init);
      APoint.Init(Position.X - MDist, Position.Y - GesHeight - MDist);
      APoly^.InsertPoint(APoint);
      ValItems^.ForEach(@DoAllUp);
      APoint.Init(Position.X + Num * BWidth + MDist, Trunc(Position.Y - GesHeight + BHeight / 100 * LastVal) + MDist);
      APoly^.InsertPoint(APoint);
      LastVal := 0;
      for Re := Num - 1 downto 0 do
        DoAllDown(ValItems^.At(Re));
      APoint.Init(Position.X - MDist, Trunc(Position.Y - GesHeight + BHeight / 100 * LastVal) - MDist);
      APoly^.InsertPoint(APoint);
      APoly^.ClosePoly;
      GetBorder := APoly;
    end
    else
    begin { Fl�chen-, Linien- und Punktdiagramme }
      Num := 0;
      MinVal := 0;
      ValItems^.ForEach(@GetMinVal);
      MinVal := Round(-MinVal + BWidth / 5);
      LastVal := 0;
      APoly := New(PCPoly, Init);
      if ShowValues then
        APoint.Init(Position.X + Round(BWidth / 2) - MDist, Position.Y - GesHeight - MinVal + MDist)
      else
        APoint.Init(Position.X + Round(BWidth / 2) - MDist, Position.Y - GesHeight - MinVal - MDist);
      APoly^.InsertPoint(APoint);
      APoint.Init(Position.X + Round(BWidth / 2) - MDist, Position.Y + MDist);
      APoly^.InsertPoint(APoint);
      APoint.Init(Position.X + GesWidth - Round(BWidth / 2) + MDist, Position.Y + MDist);
      APoly^.InsertPoint(APoint);
      if ShowValues then
      begin
        APoint.Init(Position.X + GesWidth - Round(BWidth / 2) + MDist, Position.Y - GesHeight - MinVal + MDist);
        APoly^.InsertPoint(APoint);
        APoint.Init(Round(Position.X + GesWidth - BWidth / 2 + SVFont.Height / 2 + MDist), Position.Y - GesHeight - MinVal + MDist);
        APoly^.InsertPoint(APoint);
        for Re := ValItems^.Count - 1 downto 0 do
          MakeTextBorder(ValItems^.At(Re));
        APoint.Init(Round(Position.X + BWidth / 2 - SVFont.Height / 2 - MDist), Position.Y - GesHeight - MinVal + MDist);
        APoly^.InsertPoint(APoint);
        APoly^.ClosePoly;
      end
      else
      begin
        APoint.Init(Position.X + GesWidth - Round(BWidth / 2) + MDist, Position.Y - GesHeight - MinVal - MDist);
        APoly^.InsertPoint(APoint);
        APoly^.ClosePoly;
      end;
      GetBorder := APoly;
    end;

end;

{**************************************** GetMoveBorder **********************************************************************}
{Verschieben von Balkendiagrammen mit und ohne Beschriftung}
{����� ���������� �������� � ��������� � ��� ���}

function TBusGraph.GetMoveBorder(PInfo: PPaint): PCPoly;
var
  APoly: PCPoly;
  APoint: TDPoint;
  LastVal: Real;
  Num: Integer;
  Re: Integer;
  TextLen: LongInt;
  LastTextLen: LongInt;
  MinVal: LongInt;
{**************************************** positive Teile des Balkendiagrammes ****************************************}

  procedure DoAllUp
      (
      Item: PDGraph
      ); far;
  var
    Val: Real;
    TText: string;
    PercentVal: Real;
  begin
    Val := Item^.Value;
    if Val < 0 then
    begin
      if ShowValues then
      begin
        if AnnotAbsRel then
          Str(Item^.Value: 0: 0, TText)
        else
        begin
          PercentVal := 100 / Sum * Item^.Value;
          Str(PercentVal: 0: 2, TText);
          TText := TText + ' %';
        end;
        TextLen := Round(SVFont.Height * PInfo^.GetTextRatio(SVFont, TText));
        Val := Trunc((TextLen + SVFont.Height / 5) * 100 / BHeight);
      end
      else
        Val := 0;
    end;
    if Num > 0 then
    begin
      APoint.Init(Position.X + Num * BWidth, Trunc(Position.Y - GesHeight + BHeight / 100 * LastVal));
      APoly^.InsertPoint(APoint);
    end;
    APoint.Init(Position.X + Num * BWidth, Trunc(Position.Y - GesHeight + BHeight / 100 * Val));
    APoly^.InsertPoint(APoint);
    LastVal := Val;
    Inc(Num);
  end;
{**************************************** negative Teile des Balkendiagrammes ****************************************}

  procedure DoAllDown
      (
      Item: PDGraph
      ); far;
  var
    Val: Real;
    TText: string;
    PercentVal: Real;
    LastSize: Real;
    Size: Real;
  begin
    Val := Item^.Value;
    if Val >= 0 then
    begin
      if ShowValues then
      begin
        if AnnotAbsRel then
          Str(Item^.Value: 0: 0, TText)
        else
        begin
          PercentVal := 100 / Sum * Item^.Value;
          Str(PercentVal: 0: 2, TText);
          TText := TText + ' %';
        end;
        TextLen := Round(SVFont.Height * PInfo^.GetTextRatio(SVFont, TText));
        Val := Trunc((-TextLen - SVFont.Height / 5) * 100 / BHeight);
      end
      else
        Val := 0;
    end;
    if Num < ValItems^.Count then
    begin
      APoint.Init(Position.X + Num * BWidth, Trunc(Position.Y - GesHeight + BHeight / 100 * LastVal));
      APoly^.InsertPoint(APoint);
    end;
    APoint.Init(Position.X + Num * BWidth, Trunc(Position.Y - GesHeight + BHeight / 100 * Val));
    APoly^.InsertPoint(APoint);
    LastVal := Val;
    Dec(Num);
  end;
{**************************************** gr��te Ausdehnung im Minusbereich ****************************************}

  procedure GetMinVal
      (
      Item: PDGraph
      ); far;
  var
    Val: LongInt;
  begin
    Val := Round(BHeight / 100 * Item^.Value);
    if Val < MinVal then
      MinVal := Val;
  end;
{*********************** Rand der Beschriftungen bei Punkte-, Linien- und Fl�chendiagrammen ***********************}

  procedure MakeTextBorder
      (
      Item: PDGraph
      ); far;
  var
    Val: LongInt;
    TText: string;
    PercentVal: Real;
  begin
    if AnnotAbsRel then
      Str(Item^.Value: 0: 0, TText)
    else
    begin
      PercentVal := 100 / Sum * Item^.Value;
      Str(PercentVal: 0: 2, TText);
      TText := TText + ' %';
    end;
    TextLen := Round(SVFont.Height * PInfo^.GetTextRatio(SVFont, TText));
    Val := Trunc(TextLen + SVFont.Height / 5);
    APoint.Init(Round(Position.X + Re * BWidth + BWidth / 2 + SVFont.Height / 2), Position.Y - GesHeight - MinVal);
    APoly^.InsertPoint(APoint);
    APoint.Init(Round(Position.X + Re * BWidth + BWidth / 2 + SVFont.Height / 2), Position.Y - GesHeight - MinVal - Val);
    APoly^.InsertPoint(APoint);
    APoint.Init(Round(Position.X + Re * BWidth + BWidth / 2 - SVFont.Height / 2), Position.Y - GesHeight - MinVal - Val);
    APoly^.InsertPoint(APoint);
    APoint.Init(Round(Position.X + Re * BWidth + BWidth / 2 - SVFont.Height / 2), Position.Y - GesHeight - MinVal);
    APoly^.InsertPoint(APoint);
  end;
{**************************************** Hauptteil ****************************************}
begin
  if DiagType = dtBeamDiag then
  begin { Balkendiagramme }
    Num := 0;
    LastVal := 0;
    APoly := New(PCPoly, Init);
    APoint.Init(Position.X, Position.Y - GesHeight);
    APoly^.InsertPoint(APoint);
    ValItems^.ForEach(@DoAllUp);
    APoint.Init(Position.X + Num * BWidth, Trunc(Position.Y - GesHeight + BHeight / 100 * LastVal));
    APoly^.InsertPoint(APoint);
    LastVal := 0;
    for Re := Num - 1 downto 0 do
      DoAllDown(ValItems^.At(Re));
    APoint.Init(Position.X, Trunc(Position.Y - GesHeight + BHeight / 100 * LastVal));
    APoly^.InsertPoint(APoint);
    APoly^.ClosePoly;
    GetMoveBorder := APoly;
  end
  else
  begin { Fl�chen-, Linien- und Punktdiagramme }
    Num := 0;
    MinVal := 0;
    ValItems^.ForEach(@GetMinVal);
    MinVal := Round(-MinVal + BWidth / 5);
    LastVal := 0;
    APoly := New(PCPoly, Init);
    APoint.Init(Position.X + Round(BWidth / 2), Position.Y - GesHeight - MinVal);
    APoly^.InsertPoint(APoint);
    APoint.Init(Position.X + Round(BWidth / 2), Position.Y);
    APoly^.InsertPoint(APoint);
    APoint.Init(Position.X + GesWidth - Round(BWidth / 2), Position.Y);
    APoly^.InsertPoint(APoint);
    APoint.Init(Position.X + GesWidth - Round(BWidth / 2), Position.Y - GesHeight - MinVal);
    APoly^.InsertPoint(APoint);
    if ShowValues then
      for Re := ValItems^.Count - 1 downto 0 do
        MakeTextBorder(ValItems^.At(Re));
    APoly^.ClosePoly;
    GetMoveBorder := APoly;
  end;
end;

{**************************************** GetMultiMoveBorder ****************************************}
{Verschieben von Kreisdiagrammen mit Beschriftung}
{����� �������� �������� � ��������}
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
{Procedure TBusGraph.GetMultiMoveBorder(AMulti: PMoveMulti; PInfo: PPaint);
var CRad    : LongInt;
    CPos    : TDPoint;
    CBorder : PEllipse;
    Angle   : Real;
    Dist    : Real;
    APoly   : PCPoly;
//**************************************** Rechtecke um Beschriftungen ****************************************
  Procedure DoAll
     (
     Item     : PDGraph
     ); Far;
    var Alpha        : Real;
        Beta         : Real;
        TText        : String;
        PercentVal   : Real;
        THeight      : LongInt;
        TWidth       : LongInt;
        APoint       : TDPoint;
        PosX         : LongInt;
        PosY         : LongInt;
    begin
      APoly:=New(PCPoly,Init);
      Beta:=Angle;                                          //Anfangswinkel des Tortenst�ckes
      Angle:=Angle-(2*Pi*Item^.Value/Sum);                  //Endwinkel des Tortenst�ckes
      Alpha:=Angle+(Beta-Angle)/2;                          //Winkel f�r die Beschriftung
      if Alpha < 0 then Alpha:=Alpha+2*Pi
      else if Alpha > 2*Pi then Alpha:=Alpha-2*Pi;
      if Angle < 0 then Angle:=Angle+2*Pi;
      if AnnotAbsRel then Str(Item^.Value:0:0,TText)
      else begin
        PercentVal:=100/Sum*Item^.Value;
        Str(PercentVal:0:2,TText);
        TText:=TText+' %';
      end;
      THeight:=SVFont.Height;
      TWidth:=Round(THeight*PInfo^.GetTextRatio(SVFont,TText));
      if (Alpha < Pi/2) then begin
        PosX:=Trunc(Position.X+Dist+(Dist+THeight/5+THeight/2*sin(Alpha))*cos(Alpha));
        PosY:=Trunc(Position.Y-Dist+(Dist+THeight/5+THeight/2*sin(Alpha))*sin(Alpha)+THeight/2);
      end
      else if (Alpha < Pi) then begin
        PosX:=Trunc(Position.X+Dist+(Dist+THeight/5+THeight/2*sin(Alpha))*cos(Alpha)-TWidth);
        PosY:=Trunc(Position.Y-Dist+(Dist+THeight/5+THeight/2*sin(Alpha))*sin(Alpha)+THeight/2);
      end
      else if (Alpha < 3*Pi/2) then begin
        PosX:=Trunc(Position.X+Dist+(Dist+THeight/5-THeight/2*sin(Alpha))*cos(Alpha)-TWidth);
        PosY:=Trunc(Position.Y-Dist+(Dist+THeight/5-THeight/2*sin(Alpha))*sin(Alpha)+THeight/2);
      end
      else begin
        PosX:=Trunc(Position.X+Dist+(Dist+THeight/5-THeight/2*sin(Alpha))*cos(Alpha));
        PosY:=Trunc(Position.Y-Dist+(Dist+THeight/5-THeight/2*sin(Alpha))*sin(Alpha)+THeight/2);
      end;
      APoint.Init(PosX,PosY);
      APoly^.InsertPoint(APoint);
      APoint.Init(PosX+TWidth,PosY);
      APoly^.InsertPoint(APoint);
      APoint.Init(PosX+TWidth,PosY-THeight);
      APoly^.InsertPoint(APoint);
      APoint.Init(PosX,PosY-THeight);
      APoly^.InsertPoint(APoint);
      APoly^.ClosePoly;
      AMulti^.Add(APoly);
    end;
//**************************************** Hauptteil ****************************************
begin
   CRad:=Trunc(CDiameter/2);
   CPos.Init(Position.X+CRad,Position.Y-CRad);
   CBorder:=New(PEllipse,Init(CPos,CRad,CRad,0));
   AMulti^.Add(CBorder);
   Dist:=CDiameter/2;
   Angle:=Pi/2;
   ValItems^.ForEach(@DoAll);
end; }
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

{++ Moskaliov Business Graphics BUILD#150 17.01.01}
{**********************************TOldBusGraph*********************************************}

constructor TOldBusGraph.Init(ObjectPtr: PBusGraph; Params: Word;
  AWidth: LongInt; AHeight: LongInt; Diameter: LongInt;
  GWidth: LongInt; GHeight: LongInt; AFont: AM_Font.TFontData);
begin
  inherited Init;
  NewObjectPtr := ObjectPtr;
  DiagParams := Params;
  BWidth := AWidth;
  BHeight := AHeight;
  NCDiameter := Diameter;
  GesWidth := GWidth;
  GesHeight := GHeight;
  SVFont.Init;
  SVFont.Font := AFont.Font;
  SVFont.Style := AFont.Style;
  SVFont.Height := AFont.Height;
end;

destructor TOldBusGraph.Done;
begin
  SVFont.Done;
  inherited Done;
end;

{**********************************TOldBusGraph*********************************************}

constructor TOldBuGraList.Init;
begin
  inherited Init;
  ObjList := TList.Create;
  TextWidth := 0;
end;

destructor TOldBuGraList.Done;
begin
  if ObjList <> nil then
    ObjList.Free;
  inherited Done;
end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

{*******************************************************************************************}

const
  RBusGraph: TStreamRec = (
    ObjType: rn_BusGraph;
    VmtLink: TypeOf(TBusGraph);
    Load: @TBusGraph.Load;
    Store: @TBusGraph.Store);
begin
  RegisterType(RBusGraph);
end.

