{****************************************************************************}
{ Unit                                                                       }
{----------------------------------------------------------------------------}
{                                                                            }
{----------------------------------------------------------------------------}
{ Modifikationen                                                             }
{  16.02.1994 Martin Forst                                                   }
{****************************************************************************}
unit AM_Meas;

interface

{$O-}

uses WinTypes, WinProcs, AM_Def, AM_Font, AM_Paint, ResDlg, Classes, Messages,
  SysUtils, AM_View, AM_Ini, AM_Index, Objects;

const
  id_Font = 507;
  id_THeight = 100;
  id_TSample = 101;
  id_Arrow = 102;
  id_Distance = 103;

  ArrowWidth = 10;

  MaxDist = 20;

type
  PMeasureLine = ^TMeasureLine;
  TMeasureLine = object(TView)
    StartPoint: TDPoint;
    EndPoint: TDPoint;
    constructor Init(var SPoint, EPoint: TDPoint);
    constructor Load(S: TOldStream);
    procedure Draw(PInfo: PPaint; Clipped: Boolean); virtual;
    function GetObjType: Word; virtual;
    procedure MoveRel(XMove, YMove: LongInt); virtual;
    function SelectBetweenPolys(PInfo: PPaint; Poly1, Poly2: Pointer; const ObjType: TObjectTypes;
      Inside: Boolean): Boolean; virtual;
    function SelectByPoint(PInfo: PPaint; Point: TDPoint; const ObjType: TObjectTypes): PIndex; virtual;
    function SelectByCircle(PInfo: PPaint; Middle: TDPoint;
      Radius: LongInt; const ObjType: TObjectTypes; Inside: Boolean): Boolean; virtual;
    function SelectByPoly(PInfo: PPaint; Poly: Pointer; const ObjType: TObjectTypes; Inside: Boolean): Boolean; virtual;
    function SelectByPolyLine(PInfo: PPaint; Poly: Pointer; const ObjType: TObjectTypes): Boolean; virtual;
    procedure Store(S: TOldStream); virtual;
{++ BUG#348 Ivanoff}
    function Length: Double;
{-- BUG#348 Ivanoff}
  end;

  PMeasureData = ^TMeasureData;
  TMeasureData = object(TOldObject)
    FontData: TFontData;
    ArrowLen: LongInt;
    Distance: LongInt;
    DrawAngle: Boolean;
    constructor Init;
    constructor Load(S: TOldStream);
    procedure Store(S: TOldStream);
  end;

  PMeasureDlg = ^TMeasureDlg;
  TMeasureDlg = class(TGrayDialog)
  public
    Data: PMeasureData;
    PInfo: PPaint;
    NFont: TFontData;
    constructor Init(AParent: TComponent; AData: PMeasureData; APInfo: PPaint);
    function CanClose: Boolean; override;
    procedure SelectFont(var Msg: TMessage); message id_First + id_Font;
    procedure SetFont;
    procedure SetupWindow; override;
    procedure WMDrawObject(var Msg: TMessage); message wm_First + wm_DrawObject;
  end;

procedure ProcInsertMeasure(AProj: Pointer; var DrawPos: TDPoint; bConstr: Boolean = False);

implementation

uses AM_CPoly, AM_Proj, AM_Text, Win32Def, FontStyleDlg, Controls,
  GrTools, Clipping, ExtCanvas, NumTools, AM_Main, AM_ProjO;

constructor TMeasureData.Init;
begin
  TOldObject.Init;
  FontData.Init;
  FontData.Height := 1000;
  FontData.Style := ts_Center;
  ArrowLen := 1000;
  Distance := 100;
  DrawAngle := True;
end;

constructor TMeasureData.Load
  (
  S: TOldStream
  );
var
  Version: Word;
begin
  TOldObject.Init;
  S.Read(Version, SizeOf(Version));
  FontData.Load(S);
  S.Read(ArrowLen, SizeOf(ArrowLen));
  S.Read(Distance, SizeOf(Distance));
  DrawAngle := True;
end;

procedure TMeasureData.Store
  (
  S: TOldStream
  );
var
  Version: Word;
begin
  Version := 0;
  S.Write(Version, SizeOf(Version));
  FontData.Store(s);
  S.Write(ArrowLen, SizeOf(ArrowLen));
  S.Write(Distance, SizeOf(Distance));
end;

constructor TMeasureDlg.Init
  (
  AParent: TComponent;
  AData: PMeasureData;
  APInfo: PPaint
  );
begin
  inherited Init(AParent, 'MEAS');
  Data := AData;
  PInfo := APInfo;
  NFont.Init;
  NFont := Data^.FontData;
end;

procedure TMeasureDlg.WMDrawObject
  (
  var Msg: TMessage
  );
var
  AFont: HFont;
  OldFont: HFont;
  Rect: TRect;
  TAlign: Word;
begin
  if Msg.wParam = id_TSample then
    with Msg do
    begin
      GetClientRect(GetItemHandle(wParam), Rect);
      SelectObject(lParamLo, GetStockObject(ltGray_Brush));
      PatBlt(lParamLo, 0, 0, Rect.Right, Rect.Bottom, PatCopy);
      AFont := PInfo^.GetFont(NFont, 20, 0);
      OldFont := SelectObject(lParamLo, AFont);
      case NFont.Style and $03 of
        0: TAlign := dt_Left;
        1: TAlign := dt_Center;
      else
        TAlign := dt_Right
      end;
      SetBkColor(lParamLo, RGBColors[c_LGray]);
      DrawText(lParamLo, 'AaBbCc', 6, Rect, TAlign or dt_VCenter);
      SelectObject(lParamLo, OldFont);
      DeleteObject(AFont);
    end;
end;

procedure TMeasureDlg.SelectFont
  (
  var Msg: TMessage
  );
{$IFNDEF AXDLL} // <----------------- AXDLL
var
  Dialog: TFontStyleDialog;
{$ENDIF} // <----------------- AXDLL
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  if PInfo^.FontsInstalled then
  begin
    Dialog := TFontStyleDialog.Create(Self);
    try
      Dialog.Fonts := PInfo^.Fonts;
      Dialog.FontStyle := @NFont;
      if Dialog.ShowModal = mrOK then
        SetFont;
    finally
      Dialog.Free;
    end;
  end;
{$ENDIF} // <----------------- AXDLL
end;

procedure TMeasureDlg.SetFont;
begin
  InvalidateRect(GetItemHandle(id_TSample), nil, FALSE);
  SetFieldLong(id_THeight, NFont.Height);
  SetFieldText(id_TSample, PInfo^.Fonts^.GetFont(NFont.Font)^.Name^);
end;

procedure TMeasureDlg.SetupWindow;
begin
  inherited SetupWindow;
  SetFont;
  SetFieldLong(id_Arrow, Data^.ArrowLen);
  SetFieldLong(id_Distance, Data^.Distance);
end;

function TMeasureDlg.CanClose
  : Boolean;
var
  AHeight: LongInt;
  AArrow: LongInt;
  ADist: LongInt;
begin
  CanClose := FALSE;
  if not GetFieldLong(id_THeight, AHeight) then
  begin
    MsgBox(Handle, 925, mb_IconExclamation or mb_OK, '');
    SetFocus(GetItemHandle(id_THeight));
  end
  else
    if not GetFieldLong(id_Distance, ADist) then
    begin
      MsgBox(Handle, 925, mb_IconExclamation or mb_OK, '');
      SetFocus(GetItemHandle(id_Distance));
    end
    else
      if not GetFieldLong(id_Arrow, AArrow) then
      begin
        MsgBox(Handle, 925, mb_IconExclamation or mb_OK, '');
        SetFocus(GetItemHandle(id_Arrow));
      end
      else
        with Data^ do
        begin
          NFont.Height := AHeight;
          FontData := NFont;
          ArrowLen := AArrow;
          Distance := ADist;
          CanClose := TRUE;
        end;
end;

constructor TMeasureLine.Init
  (
  var SPoint: TDPoint;
  var EPoint: TDPoint
  );
begin
  TView.Init;
  StartPoint.Init(SPoint.X, SPoint.Y);
  EndPoint.Init(EPoint.X, EPoint.Y);
  ClipRect.CorrectByPoint(SPoint);
  ClipRect.CorrectByPoint(EPoint);
end;

procedure TMeasureLine.Draw
  (
  PInfo: PPaint;
  Clipped: Boolean
  );
var
  DrawPos: TDPoint;
  Points: array[0..2] of TPoint;
  AWidth: Double;
  CosA: Double;
  SinA: Double;
  CosB: Double;
  SinB: Double;
  CosC: Double;
  SinC: Double;
  ClipPoints: TGrPointList;
  Point: TGrPoint;
begin

  with PInfo^, ExtCanvas do
  begin
     {+++Brovak bug N169 build 135}
    if ArrowLen <= 0 then
      Exit;
     {--brovak}
    Push;
    try
      Pen.Style := ltSolid;
      if Clipped then
      begin
        ClipPoints := TGrPointList.Create;
        ConvertToDispDouble(StartPoint, Point.X, Point.Y);
        ClipPoints.Add(Point);
        ConvertToDispDouble(EndPoint, Point.X, Point.Y);
        ClipPoints.Add(Point);
        ClippedPolyline(ClipPoints, FALSE);
      end
      else
      begin
        ConvertToDisp(StartPoint, Points[0]);
        ConvertToDisp(EndPoint, Points[1]);
        Polyline(Points, 2, FALSE);
      end;
      if GetState(sf_HasArrows) then
      begin
        Brush.Style := ptSolid;
        Brush.ForeColor := Pen.Color;
        Angle := StartPoint.CalculateAngle(EndPoint);
        AWidth := ArrowLen / ArrowWidth;
        SinCos(Angle, SinA, CosA);
        SinCos(Angle - Pi / 2.0, SinB, CosB);
        SinCos(Angle + Pi / 2.0, SinC, CosC);
        if Clipped then
        begin
            { draw first Points }
          ClipPoints.Clear;
          ConvertToDispDouble(StartPoint, Point.X, Point.Y);
          ClipPoints.Add(Point);
          DrawPos.Init(StartPoint.X + Round(ArrowLen * CosA + AWidth * CosB),
            StartPoint.Y + Round(ArrowLen * SinA + AWidth * SinB));
          ConvertToDispDouble(DrawPos, Point.X, Point.Y);
          ClipPoints.Add(Point);
          DrawPos.Move(Round(2.0 * CosC * AWidth), Round(2.0 * SinC * AWidth));
          ConvertToDispDouble(DrawPos, Point.X, Point.Y);
          ClipPoints.Add(Point);
          ClippedPolygon(ClipPoints, FALSE);
            { draw second Points }
          ClipPoints.Clear;
          ConvertToDispDouble(EndPoint, Point.X, Point.Y);
          ClipPoints.Add(Point);
          DrawPos.Init(EndPoint.X - Round(ArrowLen * CosA + AWidth * CosB),
            EndPoint.Y - Round(ArrowLen * SinA + AWidth * SinB));
          ConvertToDispDouble(DrawPos, Point.X, Point.Y);
          ClipPoints.Add(Point);
          DrawPos.Move(-Round(2.0 * CosC * AWidth), -Round(2.0 * SinC * AWidth));
          ConvertToDispDouble(DrawPos, Point.X, Point.Y);
          ClipPoints.Add(Point);
          ClippedPolygon(ClipPoints, FALSE);
        end
        else
        begin
          ConvertToDisp(StartPoint, Points[0]);
          DrawPos.Init(StartPoint.X + Round(ArrowLen * CosA + AWidth * CosB),
            StartPoint.Y + Round(ArrowLen * SinA + AWidth * SinB));
          ConvertToDisp(DrawPos, Points[1]);
          DrawPos.Move(Round(2.0 * CosC * AWidth), Round(2.0 * SinC * AWidth));
          ConvertToDisp(DrawPos, Points[2]);
          Polygon(Points, 3, FALSE);
          ConvertToDisp(EndPoint, Points[0]);
          DrawPos.Init(EndPoint.X - Round(ArrowLen * CosA + AWidth * CosB),
            EndPoint.Y - Round(ArrowLen * SinA + AWidth * SinB));
          ConvertToDisp(DrawPos, Points[1]);
          DrawPos.Move(-Round(2.0 * CosC * AWidth), -Round(2.0 * SinC * AWidth));
          ConvertToDisp(DrawPos, Points[2]);
          Polygon(Points, 3, FALSE);
        end;
      end;
      if Clipped then
        ClipPoints.Free;
    finally
      ExtCanvas.Pop;
    end;
  end;
end;

function TMeasureLine.GetObjType
  : Word;
begin
  GetObjType := ot_MesLine;
end;

constructor TMeasureLine.Load
  (
  S: TOldStream
  );
var
  Version: Word;
begin
  S.Read(Version, SizeOf(Version));
  TView.Load(S);
  StartPoint.Load(S);
  EndPoint.Load(S);
end;

procedure TMeasureLine.Store
  (
  S: TOldStream
  );
var
  Version: Word;
begin
  Version := 0;
  S.Write(Version, SizeOf(Version));
  TView.Store(S);
  StartPoint.Store(S);
  EndPoint.Store(S);
end;

function TMeasureLine.SelectByPoint
  (
  PInfo: PPaint;
  Point: TDPoint;
  const ObjType: TObjectTypes
  )
  : PIndex;
var
  ExpRect: TDRect;
  BDist: Double;
  BPoint: TDPoint;
  ADist: Double;
begin
  SelectByPoint := nil;
  if GetObjType in ObjType then
  begin
    ADist := PInfo^.CalculateDraw(MaxDist);
    ExpRect.Init;
    ExpRect := ClipRect;
    ExpRect.Grow(LimitToLong(ADist));
    if ExpRect.PointInside(Point) then
    begin
      if PInfo^.NormalDistance(@StartPoint, @EndPoint, @Point, BDist, BPoint) and (BDist <= ADist) then
        SelectByPoint := @Self;
    end;
    ExpRect.Done;
  end;
end;

procedure TMeasureLine.MoveRel
  (
  XMove: LongInt;
  YMove: LongInt
  );
begin
  TView.MoveRel(XMove, YMove);
  StartPoint.Move(XMove, YMove);
  EndPoint.Move(XMove, YMove);
end;

function TMeasureLine.SelectByCircle
  (
  PInfo: PPaint;
  Middle: TDPoint;
  Radius: LongInt;
  const ObjType: TObjectTypes;
  Inside: Boolean
  )
  : Boolean;
begin
  SelectByCircle := FALSE;
  if ot_MesLine in ObjType then
  begin
    if Inside then
      SelectByCircle := (StartPoint.Dist(Middle) < Radius)
        and (EndPoint.Dist(Middle) < Radius)
    else
      SelectByCircle := (StartPoint.Dist(Middle) < Radius)
        or (EndPoint.Dist(Middle) < Radius)
        or PInfo^.CheckNormalDistance(@StartPoint, @EndPoint, @Middle, Radius);
  end;
end;

function TMeasureLine.SelectByPoly
  (
  PInfo: PPaint;
  Poly: Pointer;
  const ObjType: TObjectTypes;
  Inside: Boolean
  )
  : Boolean;

function CheckCut
      : Boolean;
  var
    Cnt1: Integer;
    Cut: TDPoint;
  begin
    CheckCut := FALSE;
    for Cnt1 := 0 to PCPoly(Poly)^.Data^.Count - 2 do
      if PInfo^.LineCut(TRUE, StartPoint, EndPoint,
        PDPoint(PCPoly(Poly)^.Data^.At(Cnt1))^, PDPoint(PCPoly(Poly)^.Data^.At(Cnt1 + 1))^, Cut) then
      begin
        CheckCut := TRUE;
        Exit;
      end;
  end;
begin
  if (GetObjType in ObjType)
    and ClipRect.IsVisible(PView(Poly)^.ClipRect) then
    if Inside then
      SelectByPoly := ClipRect.IsInside(PView(Poly)^.ClipRect)
        and PCPoly(Poly)^.PointInside(PInfo, StartPoint)
        and not CheckCut
    else
      SelectByPoly := PCPoly(Poly)^.PointInside(PInfo, StartPoint)
        or CheckCut
  else
    SelectByPoly := FALSE;
end;

function TMeasureLine.SelectByPolyLine
  (
  PInfo: PPaint;
  Poly: Pointer;
  const ObjType: TObjectTypes
  )
  : Boolean;

function CheckCut
      : Boolean;
  var
    Cut: TDPoint;
    Cnt1: Integer;
  begin
    CheckCut := FALSE;
    for Cnt1 := 0 to PCPoly(Poly)^.Data^.Count - 2 do
      if PInfo^.LineCut(TRUE, StartPoint, EndPoint,
        PDPoint(PCPoly(Poly)^.Data^.At(Cnt1))^, PDPoint(PCPoly(Poly)^.Data^.At(Cnt1 + 1))^, Cut) then
      begin
        CheckCut := TRUE;
        Exit;
      end;
  end;
begin
  if (GetObjType in ObjType)
    and ClipRect.IsVisible(PView(Poly)^.ClipRect) then
    SelectByPolyLine := CheckCut
  else
    SelectByPolyLine := FALSE;
end;

procedure ProcInsertMeasure
  (
  AProj: Pointer;
  var DrawPos: TDPoint;
  bConstr: Boolean = False
  );
var
  Dist: LongInt;
  AText: string;
  AObject: PText;
  AAngle: Real;
  NAngle: Real;
  ATextPoint: TDPoint;
  ALine: PMeasureLine;
  Ortho: TDPoint;
  CutPoint: TDPoint;
begin
  with PProj(AProj)^ do
  begin
    AAngle := MesPoint1.CalculateAngle(MesPoint2);
    if (ActualMen = mn_MesCut) or (ActualMen = mn_MesPoint) or (bConstr) then
    begin
      ALine := New(PMeasureLine, Init(MesPoint1, MesPoint2));
      ALine^.State := sf_HasArrows;
      if InsertObject(ALine, FALSE) then
      begin
{$IFNDEF AXDLL}
        ProcDBSendObject(AProj, ALine, 'EIN');
{$ENDIF}
      end;
    end
    else
      if ActualMen = mn_MesOrtho then
      begin
        Ortho.Init(DrawPos.X, DrawPos.Y);
        Ortho.MoveAngle(Pi / 2 + AAngle, 1000);
        PInfo^.LineCut(FALSE, MesPoint1, MesPoint2, DrawPos, Ortho, CutPoint);
        if CutPoint.Dist(MesPoint1) < CutPoint.Dist(MesPoint2) then
          Ortho := MesPoint1
        else
          Ortho := MesPoint2;
        MesPoint1 := DrawPos;
        MesPoint2 := CutPoint;
        CutPoint.MoveAngle(Ortho.CalculateAngle(CutPoint), MeasureData.ArrowLen);
        ALine := New(PMeasureLine, Init(Ortho, CutPoint));

        if InsertObject(ALine, FALSE) then
        begin
{$IFNDEF AXDLL}
          ProcDBSendObject(AProj, ALine, 'EIN');
{$ENDIF}
        end;

        ALine := New(PMeasureLine, Init(MesPoint1, MesPoint2));
        ALine^.State := sf_HasArrows;
        if InsertObject(ALine, FALSE) then
        begin
{$IFNDEF AXDLL}
          ProcDBSendObject(AProj, ALine, 'EIN');
{$ENDIF}
        end;
      end;

    NAngle := MesPoint1.CalculateAngle(MesPoint2);
    NAngle := (NAngle * 180) / Pi;
    NAngle := 360 - NAngle + 90;
    if NAngle >= 360 then
      nAngle := 360 - NAngle;
    NAngle := Abs(NAngle);

    if MesPoint2.X < MesPoint1.X then
    begin
      CutPoint.Init(0, 0);
      CutPoint := MesPoint1;
      MesPoint1 := MesPoint2;
      MesPoint2 := CutPoint;
    end;
    AAngle := MesPoint1.CalculateAngle(MesPoint2);
    Dist := LimitToLong(MesPoint1.Dist(MesPoint2));

    ATextPoint.Init(MesPoint1.X, MesPoint1.Y);

    ATextPoint.MoveAngle(Pi / 2 + AAngle, MeasureData.Distance {+MeasureData.FontData.Height});
    ATextPoint.MoveAngle(AAngle, Dist div 2);

    AText := '- ' + FormatStr(Dist, 2) + ' -';
    AObject := New(PText, InitCalculate(PInfo, MeasureData.FontData, AText, ATextPoint, 0, 3));
    with AObject^ do
    begin
      if Width < Dist then
        Width := Dist;
      Angle := 360 - Round(AAngle * Rad2Deg);
      CalculateClipRect(PInfo);
    end;
    if InsertObject(AObject, FALSE) then
    begin
{$IFNDEF AXDLL}
      ProcDBSendObject(AProj, AObject, 'EIN');
{$ENDIF}
    end;

//      MesPoint1.MoveAngle(Pi/2+AAngle,-(2*MeasureData.Distance+MeasureData.FontData.Height));

    if MeasureData.DrawAngle then
    begin
      NAngle := PInfo^.ConvAngle(NAngle, True);
      AText := Format('%3.3f', [NAngle]) + '�';

      AObject := New(PText, InitCalculate(PInfo, MeasureData.FontData, AText, ATextPoint, 0, 7));
      with AObject^ do
      begin
        if Width < Dist then
          Width := Dist;
        Angle := 360 - Round(AAngle * Rad2Deg);
        CalculateClipRect(PInfo);
      end;
      if InsertObject(AObject, FALSE) then
      begin
{$IFNDEF AXDLL}
        ProcDBSendObject(AProj, AObject, 'EIN');
{$ENDIF}
      end;
    end;
  end;
end;

function TMeasureLine.SelectBetweenPolys
  (
  PInfo: PPaint;
  Poly1: Pointer;
  Poly2: Pointer;
  const ObjType: TObjectTypes;
  Inside: Boolean
  )
  : Boolean;

function CheckCut1
      : Boolean;
  var
    Cnt1: Integer;
    Cut: TDPoint;
  begin
    CheckCut1 := FALSE;
    for Cnt1 := 0 to PCPoly(Poly1)^.Data^.Count - 2 do
      if PInfo^.LineCut(TRUE, StartPoint, EndPoint,
        PDPoint(PCPoly(Poly1)^.Data^.At(Cnt1))^, PDPoint(PCPoly(Poly1)^.Data^.At(Cnt1 + 1))^, Cut) then
      begin
        CheckCut1 := TRUE;
        Exit;
      end;
  end;

  function CheckCut2
      : Boolean;
  var
    Cnt1: Integer;
    Cut: TDPoint;
  begin
    CheckCut2 := FALSE;
    for Cnt1 := 0 to PCPoly(Poly2)^.Data^.Count - 2 do
      if PInfo^.LineCut(TRUE, StartPoint, EndPoint,
        PDPoint(PCPoly(Poly2)^.Data^.At(Cnt1))^, PDPoint(PCPoly(Poly2)^.Data^.At(Cnt1 + 1))^, Cut) then
      begin
        CheckCut2 := TRUE;
        Exit;
      end;
  end;
begin
  if (GetObjType in ObjType)
    and (ClipRect.IsVisible(PView(Poly1)^.ClipRect) or ClipRect.IsVisible(PView(Poly2)^.ClipRect)) then
    if Inside then
      SelectBetweenPolys := (ClipRect.IsInside(PView(Poly1)^.ClipRect) or ClipRect.IsInside(PView(Poly2)^.ClipRect))
        and (PCPoly(Poly1)^.PointInside(PInfo, StartPoint)
        xor PCPoly(Poly2)^.PointInside(PInfo, StartPoint))
        and not CheckCut1 and not CheckCut2
    else
      SelectBetweenPolys := PCPoly(Poly1)^.PointInside(PInfo, StartPoint)
        xor PCPoly(Poly2)^.PointInside(PInfo, StartPoint)
        or CheckCut1 or CheckCut2
  else
    SelectBetweenPolys := FALSE;
end;

{++ BUG#348 Ivanoff}

function TMeasureLine.Length: Double;
begin
  RESULT := LimitToLong(StartPoint.Dist(EndPoint));
end;
{-- BUG#348 Ivanoff}

const
  RMeasure: TStreamRec = (
    ObjType: rn_Measure;
    VmtLink: TypeOf(TMeasureLine);
    Load: @TMeasureLine.Load;
    Store: @TMeasureLine.Store);
begin
  RegisterType(RMeasure);
end.

