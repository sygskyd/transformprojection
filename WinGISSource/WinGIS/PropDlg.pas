{******************************************************************************+
 Unit PropDlg
--------------------------------------------------------------------------------
  Autor: Martin Forst
--------------------------------------------------------------------------------
  Dialog to show and modify the properties of the selected objects.
--------------------------------------------------------------------------------

+******************************************************************************}
unit PropDlg;

interface
{$IFNDEF AXDLL} // <----------------- AXDLL
uses WinTypes, WinProcs, Messages, SysUtils, Classes, ComCtrls, Controls, Dialogs, ExtCtrls,
  Forms, Graphics, MultiDlg, MultiLng, StdCtrls, Tabnotbk, TreeView, WCtrls, AM_Proj,
{++ Sygsky}AM_View, AM_Coord, Buttons {-- Sygsky};

type
  TPropertyDialog = class(TMultiFormDialog)
    CancelBtn: TButton;
    Image: TImage;
    InfoList: TTreeListView;
    Label4: TLabel;
    MlgSection: TMlgSection;
    OkBtn: TButton;
    SelInfoLabel: TLabel;
    TabbedNoteBook: TWTabbedNoteBook;
    SetDefaultBtn: TButton;
    Image1: TImage;
    BtnPolyOrient1: TSpeedButton;
    BtnPolyOrient2: TSpeedButton; // brovak for Freddie reguest
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormShow(Sender: TObject);
    procedure InfoListDrawCaption(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure InfoListDblClick(Sender: TObject);
    procedure BtnPolyOrient1Click(Sender: TObject);
    procedure BtnPolyOrient2Click(Sender: TObject);

  private
    FProject: PProj;
         {++Sygsky}DlgData: TCoordData; {--Sygsky}
    function AddInfo(Tree: TTreeEntry; const ACaption: string; const AParam: string; ABitmap: Integer): TTreeEntry;
{$IFNDEF WMLT}
    procedure AddMMediaInfo(Tree: TTreeEntry; AID: LongInt);
{$ENDIF}
    function GetFirstObject(AType: Integer): PView;
    procedure SetBitmapInfo(SetInfoLabel: Boolean);
    procedure SetCPolyInfo(SetInfoLabel: Boolean);
    procedure SetEllipseInfo(SetInfoLabel: Boolean);
    procedure SetEllipseArcInfo(SetInfoLabel: Boolean);
    procedure SetPolyInfo(SetInfoLabel: Boolean);
    procedure SetPointInfo(SetInfoLabel: Boolean);
    procedure SetSymbolInfo(SetInfoLabel: Boolean);
    procedure SetTextInfo(SetInfoLabel: Boolean);
    procedure SetRTextInfo(SetInfoLabel: Boolean);
    procedure SetOLEInfo(SetInfoLabel: Boolean);
    procedure SetCurveInfo(SetInfoLabel: Boolean);
    procedure SetDiagramInfo(SetInfoLabel: Boolean);
{++ BUG#348 Ivanoff}
    procedure SetMesLineInfo(SetInfoLabel: Boolean);
{-- BUG#348 Ivanoff}
{++ IDB_INFO}
    procedure SetIDBInfo;
{-- IDB_INFO}
  public
    property Project: PProj read FProject write FProject;
  end;
{$ENDIF} // <----------------- AXDLL
implementation
{$IFNDEF AXDLL} // <----------------- AXDLL

{$R *.DFM}

uses AM_CPoly, AM_Circl, AM_Def, AM_Font, AM_Index, AM_Layer, AM_OLE, AM_Point, AM_Poly,
  AM_Sym, BmpImage, AM_Text, SelInfo, Validate, AM_Splin, AM_RText, AM_BuGra,
  uglob, uview, uheap, usellst, ucpoly, upoly, upixel, ucirc, uspline, usymbol, utext, TreeList,
    {++Sygsky}CoordinateSystem, ResDlg, Windows, AM_ProjP {--Sygsky 29-SEP-1999 BUG#186 BUILD #93}
{++ BUG#348 Ivanoff}
  , Am_Meas
{-- BUG#348 Ivanoff}
{++ IDB_INFO}
  , AM_Main, IDB_Utils
{-- IDB_INFO}
  , ComObj, Variants;

{******************************************************************************+
  Function TPropertyDialog.AddInfo
--------------------------------------------------------------------------------
  Creates a new tree-entry, sets all properties ad adds the entry to the tree.
  ACaption is show on the left side, AParam right aligned on the right side of
  the listbox.
+******************************************************************************}

function TPropertyDialog.AddInfo(Tree: TTreeEntry; const ACaption: string;
  const AParam: string; ABitmap: Integer): TTreeEntry;
begin
  Result := TTreeEntry.Create;
  Result.Caption := ACaption + '|' + AParam;
  Result.Bitmap := ABitmap;
  Tree.Add(Result);
end;

{******************************************************************************+
  Procedure TPropertyDialog.SetCPolyInfo
--------------------------------------------------------------------------------
  Creates the info-entry for the selected areas.
+******************************************************************************}

procedure TPropertyDialog.SetCPolyInfo;
var
  AInfo: TAreAInfo;
  SubEntry: TTreeEntry;
  ACPoly: PCPoly;
  AllCount: integer;
  BPoly: CCPoly;
begin
  AInfo := TAreAInfo(Project^.Layers^.SelLayer^.SelInfo[ot_CPoly]);
  AllCount := AInfo.HspCount + AInfo.HsdCount;
  if AllCount > 0 then
  begin
    SubEntry := AddInfo(InfoList.Tree, MlgSection[32], FormatFloat('#,##0', AllCount), 5);
    if (AllCount = 1) and (AInfo.HspCount = 1) then
    begin
      ACPoly := PCPoly(GetFirstObject(ot_CPoly));
      AddInfo(SubEntry, MlgSection[21], IntToStr(ACPoly^.Index), -3);
      AddInfo(SubEntry, MlgSection[77], GuidToString(ACPoly^.GUID), -3);
      AddInfo(SubEntry, MlgSection[6], FormatFloat('#,##0', ACPoly^.Data^.Count - 1), -3);
      AddInfo(SubEntry, MlgSection[7], FormatFloat('#,##0.00', ACPoly^.Flaeche), -3);
      AddInfo(SubEntry, MlgSection[13], FormatFloat('#,##0.00', ACPoly^.Laenge), -3);
{$IFNDEF WMLT}
      AddMMediaInfo(SubEntry, ACPoly^.Index);
{$ENDIF}
      if SetInfoLabel then
        SelInfoLabel.Caption := MlgSection[9];
    end
    else
      if (AllCount = 1) and (AInfo.HsdCount = 1) then
      begin
        BPoly := CCPoly(TSelList(GetHeapMan.SelList).GetfirstObject(ot_CPoly));
        AddInfo(SubEntry, MlgSection[21], IntToStr(BPoly.Index), -3);
        AddInfo(SubEntry, MlgSection[6], FormatFloat('#,##0', BPoly.Count - 1), -3);
        AddInfo(SubEntry, MlgSection[7], FormatFloat('#,##0.00', BPoly.Area), -3);
        AddInfo(SubEntry, MlgSection[13], FormatFloat('#,##0.00', BPoly.length), -3);
{$IFNDEF WMLT}
        AddMMediaInfo(SubEntry, BPoly.Index);
{$ENDIF}
        if SetInfoLabel then
          SelInfoLabel.Caption := MlgSection[9];
      end
      else
        if (AInfo.HspCount > 0) and (AInfo.HsdCount > 0) then
        begin
          AddInfo(SubEntry, MlgSection[6], FormatFloat('#,##0', AInfo.NodeCount), -3);
          if SetInfoLabel then
            SelInfoLabel.Caption := Format(MlgSection[10], [AllCount]);
          AddInfo(SubEntry, MlgSection[8], FormatFloat('#,##0.00', AInfo.Area), -3);
          AddInfo(SubEntry, MlgSection[14], FormatFloat('#,##0.00', AInfo.Length), -3);
        end;
    if SetInfoLabel then
      SelInfoLabel.Caption := SelInfoLabel.Caption + Format(MlgSection[17], [Project^.Layers^.TopLayer^.Text^]);
  end;
end;

{******************************************************************************+
  Procedure TPropertyDialog.SetPointInfo
--------------------------------------------------------------------------------
  Creates the info-entry for the selected areas.
+******************************************************************************}

procedure TPropertyDialog.SetPointInfo;
var
  AInfo: TPointInfo;
  SubEntry: TTreeEntry;
  APoint: PPixel;
  AllCount: integer;
  BPoint: CPixel;
begin
  AInfo := TPointInfo(Project^.Layers^.SelLayer^.SelInfo[ot_Pixel]);
  AllCount := AInfo.HspCount + AInfo.HsdCount;
  if AllCount > 0 then
  begin
    SubEntry := AddInfo(InfoList.Tree, MlgSection[29], FormatFloat('#,##0', AllCount), 5);
    if (AllCount = 1) and (AInfo.HspCount = 1) then
    begin
      APoint := PPixel(GetFirstObject(ot_Pixel));
      AddInfo(SubEntry, MlgSection[21], IntToStr(APoint^.Index), -3);
      AddInfo(SubEntry, MlgSection[77], GuidToString(APoint^.GUID), -3);
      AddInfo(SubEntry, MlgSection[52], Project^.PInfo^.CoordinateSystem
        .FormatCoordinates(APoint^.Position.X, APoint^.Position.Y, @Project^), -3);
{$IFNDEF WMLT}
      AddMMediaInfo(SubEntry, APoint^.Index);
{$ENDIF}
      if SetInfoLabel then
        SelInfoLabel.Caption := MlgSection[50];
    end
    else
      if (AllCount = 1) and (AInfo.HsdCount = 1) then
      begin
        BPoint := CPixel(TSelList(GetHeapMan.SelList).GetfirstObject(ot_pixel));
        AddInfo(SubEntry, MlgSection[21], IntToStr(BPoint.Index), -3);
        AddInfo(SubEntry, MlgSection[52], Project^.PInfo^.CoordinateSystem
          .FormatCoordinates(BPoint.Position.X, BPoint.Position.Y, @Project^), -3);
{$IFNDEF WMLT}
        AddMMediaInfo(SubEntry, BPoint.Index);
{$ENDIF}
        if SetInfoLabel then
          SelInfoLabel.Caption := MlgSection[50];
      end
      else
      begin
        if SetInfoLabel then
          SelInfoLabel.Caption := Format(MlgSection[51], [AllCount]);
      end;
    if SetInfoLabel then
      SelInfoLabel.Caption := SelInfoLabel.Caption
        + Format(MlgSection[17], [Project^.Layers^.TopLayer^.Text^]);
  end;
end;

{******************************************************************************+
  Procedure TPropertyDialog.SetPolyInfo
--------------------------------------------------------------------------------
  Creates the info-entry for the selected lines.
+******************************************************************************}

procedure TPropertyDialog.SetPolyInfo;
var
  AInfo: TLineInfo;
  SubEntry: TTreeEntry;
  APoly: PPoly;
  BPoly: CPoly;
  AllCount: integer;
begin
  AInfo := TLineInfo(Project^.Layers^.SelLayer^.SelInfo[ot_Poly]);
  AllCount := AInfo.HspCount + AInfo.HsdCount;
  if AllCount > 0 then
  begin
    SubEntry := AddInfo(InfoList.Tree, MlgSection[31], FormatFloat('#,##0', AllCount), 3);
    if (AllCount = 1) and (AInfo.HspCount = 1) then
    begin
      APoly := PPoly(GetFirstObject(ot_Poly));
      AddInfo(SubEntry, MlgSection[21], IntToStr(APoly^.Index), -3);
      AddInfo(SubEntry, MlgSection[77], GuidToString(APoly^.GUID), -3);
      AddInfo(SubEntry, MlgSection[6], FormatFloat('#,##0', APoly^.Data^.Count), -3);
      AddInfo(SubEntry, MlgSection[11], FormatFloat('#,##0.00', APoly^.Laenge), -3);
{$IFNDEF WMLT}
      AddMMediaInfo(SubEntry, APoly^.Index);
{$ENDIF}
      if SetInfoLabel then
        SelInfoLabel.Caption := MlgSection[15];
    end
    else
      if (AllCount = 1) and (AInfo.HsdCount = 1) then
      begin
        BPoly := CPoly(TSelList(GetHeapMan.SelList).GetfirstObject(ot_Poly));
        AddInfo(SubEntry, MlgSection[21], IntToStr(BPoly.Index), -3);
        AddInfo(SubEntry, MlgSection[6], FormatFloat('#,##0', BPoly.Count), -3);
        AddInfo(SubEntry, MlgSection[11], FormatFloat('#,##0.00', BPoly.Length), -3);
{$IFNDEF WMLT}
        AddMMediaInfo(SubEntry, BPoly.Index);
{$ENDIF}
        if SetInfoLabel then
          SelInfoLabel.Caption := MlgSection[15];
      end
      else
        if (AInfo.HspCount > 0) and (AInfo.HsdCount > 0) then
        begin
          AddInfo(SubEntry, MlgSection[6], FormatFloat('#,##0', AInfo.NodeCount), -3);
          if SetInfoLabel then
            SelInfoLabel.Caption := Format(MlgSection[16], [AllCount]);
          AddInfo(SubEntry, MlgSection[12], FormatFloat('#,##0.00', AInfo.Length), -3);
        end;
    if SetInfoLabel then
      SelInfoLabel.Caption := SelInfoLabel.Caption
        + Format(MlgSection[17], [Project^.Layers^.TopLayer^.Text^]);
  end;
end;

{++ BUG#348 Ivanoff}

procedure TPropertyDialog.SetMesLineInfo(SetInfoLabel: Boolean);
var
  AInfo: TLineInfo;
  SubEntry: TTreeEntry;
  AMesLine: PMeasureLine;
  BPoly: CPoly;
  AllCount: integer;
begin
  AInfo := TLineInfo(Project.Layers.SelLayer.SelInfo[ot_MesLine]);
  AllCount := AInfo.HspCount + AInfo.HsdCount;
  if AllCount > 0 then
  begin
    SubEntry := AddInfo(InfoList.Tree, MlgSection[31], FormatFloat('#,##0', AllCount), 3);
    if (AllCount = 1) and (AInfo.HspCount = 1) then
    begin
      AMesLine := PMeasureLine(GetFirstObject(ot_MesLine));
      // 'PROGIS ID'
      AddInfo(SubEntry, MlgSection[21], IntToStr(AMesLine.Index), -3);
      AddInfo(SubEntry, MlgSection[77], GuidToString(AMesLine^.GUID), -3);
      // 'Vertices'
      AddInfo(SubEntry, MlgSection[6], FormatFloat('#,##0', 2), -3);
      // 'Length'
      AddInfo(SubEntry, MlgSection[11], FormatFloat('#,##0.00', AMesLine.Length / 100), -3);
{$IFNDEF WMLT}
      AddMMediaInfo(SubEntry, AMesLine.Index);
{$ENDIF}
      if SetInfoLabel then
        SelInfoLabel.Caption := MlgSection[15];
    end
    else
      if (AllCount = 1) and (AInfo.HsdCount = 1) then
      begin
        AMesLine := PMeasureLine(TSelList(GetHeapMan.SelList).GetfirstObject(ot_MesLine));
      // 'PROGIS ID'
        AddInfo(SubEntry, MlgSection[21], IntToStr(AMesLine.Index), -3);
      // 'Vertices'
        AddInfo(SubEntry, MlgSection[6], FormatFloat('#,##0', 2), -3);
      // 'Length'
        AddInfo(SubEntry, MlgSection[11], FormatFloat('#,##0.00', AMesLine.Length / 100), -3);
{$IFNDEF WMLT}
        AddMMediaInfo(SubEntry, AMesLine.Index);
{$ENDIF}
        if SetInfoLabel then
          SelInfoLabel.Caption := MlgSection[15];
      end
      else
        if (AInfo.HspCount > 0) and (AInfo.HsdCount > 0) then
        begin
          AddInfo(SubEntry, MlgSection[6], FormatFloat('#,##0', AInfo.NodeCount), -3);
          if SetInfoLabel then
            SelInfoLabel.Caption := Format(MlgSection[16], [AllCount]);
          AddInfo(SubEntry, MlgSection[12], FormatFloat('#,##0.00', AInfo.Length), -3);
        end;
    // 17=' on Layer "%s"'
    if SetInfoLabel then
      SelInfoLabel.Caption := SelInfoLabel.Caption
        + Format(MlgSection[17], [Project.Layers.TopLayer.Text^]);
  end;
end;
{-- BUG#348 Ivanoff}

{******************************************************************************+
  Procedure TPropertyDialog.SetBitmapInfo
--------------------------------------------------------------------------------
  Creates the info-entry for the selected pictures.
+******************************************************************************}

procedure TPropertyDialog.SetBitmapInfo;
var
  AInfo: TImageInfo;
  SubEntry: TTreeEntry;
  AImage: PImage;
  ImageInfo: TImgDim;
begin
  AInfo := TImageInfo(Project^.Layers^.SelLayer^.SelInfo[ot_Image]);
  if AInfo.HspCount > 0 then
  begin
    SubEntry := AddInfo(InfoList.Tree, MlgSection[33], FormatFloat('#,##0', AInfo.HspCount), 14);
    if AInfo.HspCount = 1 then
    begin
      AImage := PImage(GetFirstObject(ot_Image));
      AddInfo(SubEntry, MlgSection[21], IntToStr(AImage^.Index), -3);
      AddInfo(SubEntry, MlgSection[77], GuidToString(AImage^.GUID), -3);
      AddInfo(SubEntry, MlgSection[53], ExtractFileName(PToStr(AImage^.AbsolutePath)), -3);
      AddInfo(SubEntry, MlgSection[54], ExtractFilePath(PToStr(AImage^.AbsolutePath)), -3);
      AddInfo(SubEntry, MlgSection[55], ExtractFilePath(PToStr(AImage^.RelativePath)), -3);
      if AImage.Info <> nil then
      begin
        AImage^.GetInfo(ImageInfo);
        AddInfo(SubEntry, MlgSection[56], Format('%d x %d %s', [ImageInfo.lWidth,
          ImageInfo.lHeight, MlgSection[60]]), -3);
        AddInfo(SubEntry, MlgSection[58], IntToStr(ImageInfo.byBitCount), -3);
      end;
{++ Glukhov HELMERT BUILD#115 08.06.00 }
//      AddInfo(SubEntry,MlgSection[59],FormatFloat('#,##0.00',AImage^.Rotation)+'�',-3);
      AddInfo(SubEntry, MlgSection[59], FormatFloat('#,##0.00', AImage^.Rotation * 180 / Pi) + '�', -3);
{-- Glukhov HELMERT BUILD#115 08.06.00 }
{$IFNDEF WMLT}
      AddMMediaInfo(SubEntry, AImage^.Index);
{$ENDIF}
      if SetInfoLabel then
        SelInfoLabel.Caption := MlgSection[40];
    end
    else
    begin
      if SetInfoLabel then
        SelInfoLabel.Caption := Format(MlgSection[41], [AInfo.HspCount]);
    end;
    if SetInfoLabel then
      SelInfoLabel.Caption := SelInfoLabel.Caption
        + Format(MlgSection[17], [Project^.Layers^.TopLayer^.Text^]);
  end;
end;

{******************************************************************************+
  Procedure TPropertyDialog.SetSymbolInfo
--------------------------------------------------------------------------------
  Creates the info-entry for the selected symbols.
+******************************************************************************}

procedure TPropertyDialog.SetSymbolInfo;
var
  AInfo: TSymbolInfo;
  SubEntry: TTreeEntry;
  ASymbol: PSymbol;
  AllCount: integer;
  BSymbol: CSymbol;
begin
  AInfo := TSymbolInfo(Project^.Layers^.SelLayer^.SelInfo[ot_Symbol]);
  AllCount := AInfo.HspCount + AInfo.HsdCount;
  if AllCount > 0 then
  begin
    SubEntry := AddInfo(InfoList.Tree, MlgSection[26], FormatFloat('#,##0', AllCount), 7);
    if (AllCount = 1) and (AInfo.HspCount = 1) then
    begin
      ASymbol := PSymbol(GetFirstObject(ot_Symbol));
      AddInfo(SubEntry, MlgSection[21], IntToStr(ASymbol^.Index), -3);
      AddInfo(SubEntry, MlgSection[77], GuidToString(ASymbol^.GUID), -3);
      if ASymbol^.SymPtr <> nil then
      begin
        AddInfo(SubEntry, MlgSection[61], ASymbol^.SymPtr^.Name^, -3);
      end;
      if ASymbol^.SymPtr <> nil then
      begin
        if PToStr(ASymbol^.SymPtr^.LibName) = '' then
          AddInfo(SubEntry, MlgSection[62], MlgSection[63], -3)
        else
          AddInfo(SubEntry, MlgSection[62], ExtractFileName(ASymbol^.SymPtr^.LibName^), -3);
      end;
      AddInfo(SubEntry, MlgSection[52], Project^.PInfo^.CoordinateSystem
        .FormatCoordinates(ASymbol^.Position.X, ASymbol^.Position.Y, @Project^), -3);
//++ Glukhov BugCorrection BUILD#121 30.06.00
      if ASymbol^.UsedDefaultAngle then
        AddInfo(SubEntry, MlgSection[59], MlgSection[76], -3)
      else
        AddInfo(SubEntry, MlgSection[59], FormatFloat('#,##0.00', ASymbol^.Angle * 180.0 / Pi) + '�', -3);
//-- Glukhov BugCorrection BUILD#121 30.06.00
//!?!?!        AddInfo(SubEntry,'Scale',FormatFloat('#,##0.00',ASymbol^.ScaleFact),-3);
{$IFNDEF WMLT}
      AddMMediaInfo(SubEntry, ASymbol^.Index);
{$ENDIF}
      if SetInfoLabel then
        SelInfoLabel.Caption := MlgSection[42];
    end
    else
      if (AllCount = 1) and (AInfo.HsdCount = 1) then
      begin
        BSymbol := CSymbol(TSelList(GetHeapMan.SelList).GetfirstObject(ot_symbol));
        BSymbol.SetSymptr(Project^.Pinfo^.Symbols);
        AddInfo(SubEntry, MlgSection[21], IntToStr(BSymbol.Index), -3);
        AddInfo(SubEntry, MlgSection[61], PToStr(BSymbol.SymPtr^.Name), -3);
        if PToStr(BSymbol.SymPtr^.LibName) = '' then
          AddInfo(SubEntry, MlgSection[62], MlgSection[63], -3)
        else
          AddInfo(SubEntry, MlgSection[62], ExtractFileName(PToStr(BSymbol.SymPtr^.LibName)), -3);
        AddInfo(SubEntry, MlgSection[52], Project^.PInfo^.CoordinateSystem
          .FormatCoordinates(BSymbol.Position.X, BSymbol.Position.Y, @Project^), -3);
        AddInfo(SubEntry, MlgSection[59], FormatFloat('#,##0.00', BSymbol.Angle * 180.0 / Pi), -3);
//!?!?!        AddInfo(SubEntry,'Scale',FormatFloat('#,##0.00',BSymbol.ScaleFact),-3);
{$IFNDEF WMLT}
        AddMMediaInfo(SubEntry, BSymbol.Index);
{$ENDIF}
        if SetInfoLabel then
          SelInfoLabel.Caption := MlgSection[42];
      end
      else
      begin
        if SetInfoLabel then
          SelInfoLabel.Caption := Format(MlgSection[43], [AllCount]);
      end;
    if SetInfoLabel then
      SelInfoLabel.Caption := SelInfoLabel.Caption
        + Format(MlgSection[17], [Project^.Layers^.TopLayer^.Text^]);
  end;
end;

{*******************************************************************************
  Procedure TPropertyDialog.SetTextInfo
--------------------------------------------------------------------------------
  Creates the info-entry for the selected text.
+******************************************************************************}

procedure TPropertyDialog.SetTextInfo;
var
  AInfo: TTextInfo;
  SubEntry: TTreeEntry;
  AText: PText;
  AllCount: integer;
  BText: CTExt;
begin
  AInfo := TTextInfo(Project^.Layers^.SelLayer^.SelInfo[ot_Text]);
  AllCount := AInfo.HspCount + AInfo.HsdCount;
  if AllCount > 0 then
  begin
    SubEntry := AddInfo(InfoList.Tree, MlgSection[22], FormatFloat('#,##0', AllCount), 8);
    if (AllCount = 1) and (AInfo.HspCount = 1) then
    begin
      AText := PText(GetFirstObject(ot_Text));
      AddInfo(SubEntry, MlgSection[21], IntToStr(AText^.Index), -3);
      AddInfo(SubEntry, MlgSection[77], GuidToString(AText^.GUID), -3);
      AddInfo(SubEntry, MlgSection[52], Project^.PInfo^.CoordinateSystem
        .FormatCoordinates(AText^.Pos.X, AText^.Pos.Y, @Project^), -3);
      AddInfo(SubEntry, MlgSection[59], FormatFloat('#,##0.00', (360 - AText^.Angle) mod 360) + '�', -3);
{$IFNDEF WMLT}
      AddMMediaInfo(SubEntry, AText^.Index);
{$ENDIF}
      if SetInfoLabel then
        SelInfoLabel.Caption := MlgSection[44];
    end
    else
      if (AllCount = 1) and (AInfo.HsdCount = 1) then
      begin
        BText := CText(TSelList(GetHeapMan.SelList).GetfirstObject(ot_Text));
        if BText <> nil then
        begin
          AddInfo(SubEntry, MlgSection[21], IntToStr(BText.Index), -3);
          AddInfo(SubEntry, MlgSection[52], Project^.PInfo^.CoordinateSystem
            .FormatCoordinates(BText.Position.X, BText.Position.Y, @Project^), -3);
          AddInfo(SubEntry, MlgSection[59], FormatFloat('#,##0.00', (360 - BText.Angle) mod 360) + '�', -3);
{$IFNDEF WMLT}
          AddMMediaInfo(SubEntry, BText.Index);
{$ENDIF}
          if SetInfoLabel then
            SelInfoLabel.Caption := MlgSection[44];
        end;
      end
      else
      begin
        if SetInfoLabel then
          SelInfoLabel.Caption := Format(MlgSection[45], [AllCount]);
      end;
    if SetInfoLabel then
      SelInfoLabel.Caption := SelInfoLabel.Caption + Format(MlgSection[17], [Project^.Layers^.TopLayer^.Text^]);
  end;
end;

{*******************************************************************************
  Procedure TPropertyDialog.SetTextInfo
--------------------------------------------------------------------------------
  Creates the info-entry for the selected text.
+******************************************************************************}

procedure TPropertyDialog.SetRTextInfo;
var
  AInfo: TRTextInfo;
  SubEntry: TTreeEntry;
  ARText: PRText;
begin
  AInfo := TRTextInfo(Project^.Layers^.SelLayer^.SelInfo[ot_RText]);
  if AInfo.HspCount > 0 then
  begin
    SubEntry := AddInfo(InfoList.Tree, MlgSection[23], FormatFloat('#,##0', AInfo.HspCount), 8);
    if AInfo.HspCount = 1 then
    begin
      ARText := PRText(GetFirstObject(ot_RText));
      AddInfo(SubEntry, MlgSection[21], IntToStr(ARText^.Index), -3);
      AddInfo(SubEntry, MlgSection[77], GuidToString(ARText^.GUID), -3);
      AddInfo(SubEntry, MlgSection[52], Project^.PInfo^.CoordinateSystem
        .FormatCoordinates(ARText^.ClipRect.A.X, ARText^.ClipRect.A.Y, @Project^), -3);
{$IFNDEF WMLT}
      AddMMediaInfo(SubEntry, ARText^.Index);
{$ENDIF}
      if SetInfoLabel then
        SelInfoLabel.Caption := MlgSection[70];
    end
    else
      if SetInfoLabel then
        SelInfoLabel.Caption := Format(MlgSection[71], [AInfo.HspCount]);
    if SetInfoLabel then
      SelInfoLabel.Caption := SelInfoLabel.Caption + Format(MlgSection[17], [Project^.Layers^.TopLayer^.Text^]);
  end;
end;

{*******************************************************************************
  Procedure TPropertyDialog.SetOLEInfo
--------------------------------------------------------------------------------
  Creates the info-entry for the selected OLE-object.
+******************************************************************************}

procedure TPropertyDialog.SetOLEInfo;
var
  AInfo: TOLEInfo;
  SubEntry: TTreeEntry;
  AOLEObject: POLEClientObject;
begin
  AInfo := TOLEInfo(Project^.Layers^.SelLayer^.SelInfo[ot_OLEObj]);
  if AInfo.HspCount > 0 then
  begin
    SubEntry := AddInfo(InfoList.Tree, MlgSection[34], FormatFloat('#,##0', AInfo.HspCount), 15);
    if AInfo.HspCount = 1 then
    begin
      AOLEObject := POLEClientObject(GetFirstObject(ot_OLEObj));
      AddInfo(SubEntry, MlgSection[21], IntToStr(AOLEObject^.Index), -3);
      AddInfo(SubEntry, MlgSection[77], GuidToString(AOLEObject^.GUID), -3);
{$IFNDEF WMLT}
      AddMMediaInfo(SubEntry, AOLEObject^.Index);
{$ENDIF}
      if SetInfoLabel then
        SelInfoLabel.Caption := MlgSection[44];
    end
    else
    begin
      if SetInfoLabel then
        SelInfoLabel.Caption := Format(MlgSection[45], [AInfo.HspCount]);
    end;
    if SetInfoLabel then
      SelInfoLabel.Caption := SelInfoLabel.Caption + Format(MlgSection[17], [Project^.Layers^.TopLayer^.Text^]);
  end;
end;

{******************************************************************************+
  Procedure TPropertyDialog.SetEllipseInfo
--------------------------------------------------------------------------------
  Creates the info-entry for the selected ellipses.
+******************************************************************************}

procedure TPropertyDialog.SetEllipseInfo;
var
  AInfo: TEllipseInfo;
  SubEntry: TTreeEntry;
  AEllipse: PEllipse;
  AllCount: integer;
  BEllipse: CEllipse;
begin
  AInfo := TEllipseInfo(Project^.Layers^.SelLayer^.SelInfo[ot_Circle]);
  AllCount := AInfo.HspCount + AInfo.HsdCount;
  if AllCount > 0 then
  begin
    SubEntry := AddInfo(InfoList.Tree, MlgSection[27], FormatFloat('#,##0', AllCount), 6);
    if (AllCount = 1) and (AInfo.HspCount = 1) then
    begin
      AEllipse := PEllipse(GetFirstObject(ot_Circle));
{++ Sygsky 29-SEP-1999 BUG#186 BUILD#93 }
      with DlgData do
      begin { Predefine values for possible input dialog }
        ValuesPredefined[0] := AEllipse^.Position.X;
        ValuesPredefined[1] := AEllipse^.Position.Y;
        ValuesPredefined[2] := AEllipse^.PrimaryAxis;
      end;
{-- Sygsky }
      AddInfo(SubEntry, MlgSection[21], IntToStr(AEllipse^.Index), -3);
      AddInfo(SubEntry, MlgSection[77], GuidToString(AEllipse^.GUID), -3);
      AddInfo(SubEntry, MlgSection[64], Project^.PInfo^.CoordinateSystem
        .FormatCoordinates(AEllipse^.Position.X, AEllipse^.Position.Y, @Project^), -3);
{++ Sygsky 30-SEP-1999 BUG#186 BUILD#93: Line changed -> not properly scaled output }
      AddInfo(SubEntry, MlgSection[67], FormatFloat('#,##0.00', AEllipse^.PrimaryAxis / 100), -3);
{-- Sygsky }
      AddInfo(SubEntry, MlgSection[65], FormatFloat('#,##0.00', AEllipse^.Flaeche), -3);
      AddInfo(SubEntry, MlgSection[66], FormatFloat('#,##0.00', AEllipse^.Laenge), -3);
{$IFNDEF WMLT}
      AddMMediaInfo(SubEntry, AEllipse^.Index);
{$ENDIF}
      if SetInfoLabel then
        SelInfoLabel.Caption := MlgSection[46];
    end
    else
      if (AllCount = 1) and (AInfo.HsdCount = 1) then
      begin
        BEllipse := CEllipse(TSelList(GetHeapMan.SelList).GetfirstObject(ot_Circle));
        AddInfo(SubEntry, MlgSection[21], IntToStr(BEllipse.Index), -3);
        AddInfo(SubEntry, MlgSection[64], Project^.PInfo^.CoordinateSystem
          .FormatCoordinates(BEllipse.Position.X, BEllipse.Position.Y, @Project^), -3);
        AddInfo(SubEntry, MlgSection[67], FormatFloat('#,##0.00', BEllipse.PrimaryAxis), -3);
        AddInfo(SubEntry, MlgSection[65], FormatFloat('#,##0.00', BEllipse.Flaeche), -3);
        AddInfo(SubEntry, MlgSection[66], FormatFloat('#,##0.00', BEllipse.Laenge), -3);
{$IFNDEF WMLT}
        AddMMediaInfo(SubEntry, BEllipse.Index);
{$ENDIF}
        if SetInfoLabel then
          SelInfoLabel.Caption := MlgSection[46];
      end
      else
        if (AInfo.HspCount > 0) and (AInfo.HsdCount > 0) then
        begin
          if SetInfoLabel then
            SelInfoLabel.Caption := Format(MlgSection[47], [AllCount]);
          AddInfo(SubEntry, 'Total area', FormatFloat('#,##0.00', AInfo.Area), -3);
          AddInfo(SubEntry, 'Total perimeter', FormatFloat('#,##0.00', AInfo.Perimeter), -3);
        end;
    if SetInfoLabel then
      SelInfoLabel.Caption := SelInfoLabel.Caption + Format(MlgSection[17], [Project^.Layers^.TopLayer^.Text^]);
  end;
end;

{******************************************************************************+
  Procedure TPropertyDialog.SetEllipseArcInfo
--------------------------------------------------------------------------------
  Creates the info-entry for the selected arcs.
+******************************************************************************}

procedure TPropertyDialog.SetEllipseArcInfo;
var
  AInfo: TEllipseArcInfo;
  SubEntry: TTreeEntry;
  AEllipseArc: PEllipseArc;
  BArc: CArc;
  AllCount: integer;
begin
  AInfo := TEllipseArcInfo(Project^.Layers^.SelLayer^.SelInfo[ot_Arc]);
  AllCount := AInfo.HspCount + AInfo.HsdCount;
  if AllCount > 0 then
  begin
    SubEntry := AddInfo(InfoList.Tree, MlgSection[28], FormatFloat('#,##0', AllCount), 4);
    if (AllCount = 1) and (AInfo.HspCount = 1) then
    begin
      AEllipseArc := PEllipseArc(GetfirstObject(ot_Arc));
      AddInfo(SubEntry, MlgSection[21], IntToStr(AEllipseArc^.Index), -3);
      AddInfo(SubEntry, MlgSection[77], GuidToString(AEllipseArc^.GUID), -3);
      AddInfo(SubEntry, 'Center', Project^.PInfo^.CoordinateSystem
        .FormatCoordinates(AEllipseArc^.Position.X, AEllipseArc^.Position.Y, @Project^), -3);
      AddInfo(SubEntry, 'Length', FormatFloat('#,##0.00', AEllipseArc^.Laenge), -3);
      AddInfo(SubEntry, 'Startangle', FormatFloat('#,##0.00', AEllipseArc^.BeginAngle * 180 / Pi) + '�', -3);
      AddInfo(SubEntry, 'Endangle', FormatFloat('#,##0.00', AEllipseArc^.EndAngle * 180 / Pi) + '�', -3);
{$IFNDEF WMLT}
      AddMMediaInfo(SubEntry, AEllipseArc^.Index);
{$ENDIF}
      if SetInfoLabel then
        SelInfoLabel.Caption := MlgSection[48];
    end
    else
      if (AllCount = 1) and (AInfo.HsdCount = 1) then
      begin
        BArc := CArc(TSelList(GetHeapMan.SelList).GetfirstObject(ot_arc));
        AddInfo(SubEntry, MlgSection[21], IntToStr(BArc.Index), -3);
        AddInfo(SubEntry, 'Center', Project^.PInfo^.CoordinateSystem
          .FormatCoordinates(BArc.Position.X, BArc.Position.Y, @Project^), -3);
        AddInfo(SubEntry, 'Length', FormatFloat('#,##0.00', BArc.Laenge), -3);
        AddInfo(SubEntry, 'Startangle', FormatFloat('#,##0.00', BArc.BeginAngle * 180 / Pi) + '�', -3);
        AddInfo(SubEntry, 'Endangle', FormatFloat('#,##0.00', BArc.EndAngle * 180 / Pi) + '�', -3);
{$IFNDEF WMLT}
        AddMMediaInfo(SubEntry, BArc.Index);
{$ENDIF}
        if SetInfoLabel then
          SelInfoLabel.Caption := MlgSection[48];
      end
      else
      begin
        if SetInfoLabel then
          SelInfoLabel.Caption := Format(MlgSection[49], [AllCount]);
        AddInfo(SubEntry, 'Total length', FormatFloat('#,##0.00', AInfo.Length), -3);
      end;
    if SetInfoLabel then
      SelInfoLabel.Caption := SelInfoLabel.Caption + Format(MlgSection[17], [Project^.Layers^.TopLayer^.Text^]);
  end;
end;

{******************************************************************************+
  Procedure TPropertyDialog.SetCurveInfo
--------------------------------------------------------------------------------
  Creates the info-entry for the selected areas.
+******************************************************************************}

procedure TPropertyDialog.SetDiagramInfo;
var
  AInfo: TDiagramInfo;
  SubEntry: TTreeEntry;
  ADiagram: PBusGraph;
  AllCount: integer;
begin
  AInfo := TDiagramInfo(Project^.Layers^.SelLayer^.SelInfo[ot_BusGraph]);
  AllCount := AInfo.HspCount + AInfo.HsdCount;
  if AllCount > 0 then
  begin
    SubEntry := AddInfo(InfoList.Tree, MlgSection[36], FormatFloat('#,##0', AllCount), 5);
    if (AllCount = 1) and (AInfo.HspCount = 1) then
    begin
      ADiagram := PBusGraph(GetFirstObject(ot_BusGraph));
      AddInfo(SubEntry, MlgSection[21], IntToStr(ADiagram^.Index), -3);
      AddInfo(SubEntry, MlgSection[77], GuidToString(ADiagram^.GUID), -3);
{$IFNDEF WMLT}
      AddMMediaInfo(SubEntry, ADiagram^.Index);
{$ENDIF}
      if SetInfoLabel then
        SelInfoLabel.Caption := MlgSection[72];
    end
    else
    begin
      if SetInfoLabel then
        SelInfoLabel.Caption := Format(MlgSection[73], [AllCount]);
    end;
    if SetInfoLabel then
      SelInfoLabel.Caption := SelInfoLabel.Caption + Format(MlgSection[17], [Project^.Layers^.TopLayer^.Text^]);
  end;
end;

{******************************************************************************+
  Procedure TPropertyDialog.SetCurveInfo
--------------------------------------------------------------------------------
  Creates the info-entry for the selected areas.
+******************************************************************************}

procedure TPropertyDialog.SetCurveInfo;
var
  AInfo: TSplineInfo;
  SubEntry: TTreeEntry;
  ASpline: PSpline;
  AllCount: integer;
  BSpline: CSpline;
begin
  AInfo := TSplineInfo(Project^.Layers^.SelLayer^.SelInfo[ot_Spline]);
  AllCount := AInfo.HspCount + AInfo.HsdCount;
  if AllCount > 0 then
  begin
    SubEntry := AddInfo(InfoList.Tree, MlgSection[30], FormatFloat('#,##0', AllCount), 5);
    if (AllCount = 1) and (AInfo.HspCount = 1) then
    begin
      ASpline := PSpline(GetFirstObject(ot_Spline));
      AddInfo(SubEntry, MlgSection[21], IntToStr(ASpline^.Index), -3);
      AddInfo(SubEntry, MlgSection[77], GuidToString(ASpline^.GUID), -3);
{$IFNDEF WMLT}
      AddMMediaInfo(SubEntry, ASpline^.Index);
{$ENDIF}
      if SetInfoLabel then
        SelInfoLabel.Caption := MlgSection[68];
    end
    else
      if (AllCount = 1) and (AInfo.HsdCount = 1) then
      begin
        BSpline := CSpline(TSelList(GetHeapMan.SelList).GetfirstObject(ot_spline));
        AddInfo(SubEntry, MlgSection[21], IntToStr(BSpline.Index), -3);
{$IFNDEF WMLT}
        AddMMediaInfo(SubEntry, BSpline.Index);
{$ENDIF}
        if SetInfoLabel then
          SelInfoLabel.Caption := MlgSection[68];
      end
      else
      begin
        if SetInfoLabel then
          SelInfoLabel.Caption := Format(MlgSection[69], [AllCount]);
      end;
    if SetInfoLabel then
      SelInfoLabel.Caption := SelInfoLabel.Caption + Format(MlgSection[17], [Project^.Layers^.TopLayer^.Text^]);
  end;
end;

procedure TPropertyDialog.FormShow(Sender: TObject);
begin
  with Project^.Layers^.SelLayer^.SelInfo do
  begin
    if SelectedType < 0 then
    begin
      if Count = 1 then
        SelInfoLabel.Caption := MlgSection[19]
      else
        SelInfoLabel.Caption := Format(MlgSection[20], [Count]);
      SelInfoLabel.Caption := SelInfoLabel.Caption + Format(MlgSection[17], [Project^.Layers^.TopLayer^.Text^]);
    end;
{++ Sygsky 30-SEP-1999 BUG#186 BUILD#93 }
    if not ((Count = 1) and (SelectedType = ot_Circle)) then
      InfoList.OnDblCLick := nil { Or not circle or not single object selected }
    else
      InfoList.OnDblClick := InfoListDblClick; { One circle only selected, do it}
{-- Sygsky}
    InfoList.BeginUpdate;
    SetPointInfo(SelectedType = ot_Pixel);
    SetCPolyInfo(SelectedType = ot_CPoly);
    SetPolyInfo(SelectedType = ot_Poly);
{++ BUG#348 Ivanoff}
    SetMesLineInfo(SelectedType = ot_MesLine);
{-- BUG#348 Ivanoff}
    SetEllipseInfo(SelectedType = ot_Circle);
    SetEllipseArcInfo(SelectedType = ot_Arc);
    SetBitmapInfo(SelectedType = ot_Image);
    SetSymbolInfo(SelectedType = ot_Symbol);
    SetTextInfo(SelectedType = ot_Text);
    SetRTextInfo(SelectedType = ot_RText);
    SetOLEInfo(SelectedType = ot_OLEObj);
    SetCurveInfo(SelectedType = ot_Spline);
    SetDiagramInfo(SelectedType = ot_BusGraph);
{++ IDB_INFO}
    SetIDBInfo;
{-- IDB_INFO}
    InfoList.EndUpdate;
  end;

  if Project^.PInfo.IsMU then
  begin
    BtnPolyOrient1.Visible := False;
    BtnPolyOrient2.Visible := False;
  end
  else
  begin
    BtnPolyOrient1.Visible := (((Project^.Layers^.SelLayer^.SelInfo.SelectCount[ot_CPoly]) + (Project^.Layers^.SelLayer^.SelInfo.SelectCount[ot_Poly])) >= 1);
    BtnPolyOrient2.Visible := BtnPolyOrient1.Visible;
  end;
end;

procedure TPropertyDialog.FormCreate(Sender: TObject);
begin
{++ IDB_INFO}
//  InfoList.AddBitmaps(Image.Picture.Bitmap,2);
  InfoList.AddBitmaps(Image1.Picture.Bitmap, 2);
{-- IDB_INFO}
  RemoveFirstPage := FALSE;
  DialogNotebook := TabbedNotebook;
  CallShowHide := TRUE;
end;

procedure TPropertyDialog.InfoListDrawCaption(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
  TreeItem: TTreeEntry;
  AText: string;
  ARect: TRect;
begin
  with InfoList.Canvas do
  begin
    TreeItem := TTreeEntry(InfoList.Tree.ExpandedItems[Index]);
    AText := GetShortHint(TreeItem.Caption);
    with Rect do
    begin
      ARect := Classes.Rect(Left, Top, Left + 4 + TextWidth(AText), Bottom);
      TextRect(ARect, Left + 2, (Top + Bottom - TextHeight(AText)) div 2, AText);
    end;
    if odFocused in State then
      DrawFocusRect(ARect);
    AText := GetLongHint(TreeItem.Caption);
    with Rect do
    begin
      Brush.Color := clWindow;
      Font.Color := clWindowText;
      ARect.Left := ARect.Right;
      ARect.Right := ClientWidth;
      TextRect(ARect, Right - TextWidth(AText) - 10,
        (Top + Bottom - TextHeight(AText)) div 2, AText);
    end;
  end;
end;

function TPropertyDialog.GetFirstObject(AType: Integer): PView;

  function DoAll(Item: PIndex): Boolean; far;
  begin
    Result := TRUE;
  end;
begin
  Result := Project^.Layers^.SelLayer^.FirstObjectType(AType, @DoAll);
  if Result <> nil then
    Result := Project^.Layers^.IndexObject(Project^.PInfo, Result);
end;

{$IFNDEF WMLT}

procedure TPropertyDialog.AddMMediaInfo(Tree: TTreeEntry; AID: LongInt);
var
  Count: Integer;
begin
  if Project^.Multimedia <> nil then
    Count := Project^.Multimedia^.EntriesForIDCount(AID)
  else
    Count := 0;
  if Count > 0 then
    AddInfo(Tree, MlgSection[57], IntToStr(Count), -3);
end;
{$ENDIF}

procedure TPropertyDialog.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  CanClose := CheckValidators(Self);
end;

{++ Sygsky 29-SEP-1999 BUG#186 BUILD#93: Insertion  of the full procedure text }

procedure TPropertyDialog.InfoListDblClick(Sender: TObject);
var
  Point: TDPoint;
  OrigPoint: TPoint;
  AIndex, I: Integer;
  TreeItem: TTreeEntry;
  AEllipse: PEllipse;

  function FindItemByName(Name: string): Integer;
  var
    I: Integer;
    TreeItem: TTreeEntry;
  begin
    Result := -1;
    for I := 0 to InfoList.Tree.ExpandedCount - 1 do
    begin
      TreeItem := TTreeEntry(InfoList.Tree.ExpandedItems[I]);
      if CompareText(GetShortHint(TreeItem.Caption), Name) = 0 then
      begin
        Result := I;
        Exit;
      end;
    end;
  end;

  procedure UpdateInfo;

    procedure ChangeInfo(const sCap, sParam: string);
    var
      Id: Integer;
      TreeItem: TTreeEntry;
    begin
      Id := FindItemByName(sCap);
      if Id < 0 then
        Exit;
      TreeItem := TTreeEntry(InfoList.Tree.ExpandedItems[Id]);
      TreeItem.Caption := sCap + '|' + sParam;
    end;
  begin
    ChangeInfo(MlgSection[64], Project^.PInfo^.CoordinateSystem
      .FormatCoordinates(AEllipse^.Position.X, AEllipse^.Position.Y, @Project^));
    ChangeInfo(MlgSection[67], FormatFloat('#,##0.00', AEllipse^.PrimaryAxis / 100.0));
    ChangeInfo(MlgSection[65], FormatFloat('#,##0.00', AEllipse^.Flaeche));
    ChangeInfo(MlgSection[66], FormatFloat('#,##0.00', AEllipse^.Laenge));
    InfoList.Repaint;
  end;

begin
  if not GetCursorPos(OrigPoint) then
    Exit;
  AIndex := FindItemByName(MlgSection[67]); { Find line with 'Circles' }
  if AIndex < 0 then
    Exit;
  AIndex := FindItemByName(MlgSection[21]); { Find line with 'PROGIS ID' }
  if AIndex < 0 then
    Exit;
  with DlgData do
  begin { Look for descr. in AM_Coord.pas }
    CoordSystem := Integer(Project^.PInfo^.CoordinateSystem.CoordinateType)
      or Integer(PresetBit); { Mark values to be predefined }
    Lines := 3;
    Text1 := 0;
    Text3 := 867; { RadiOus!!! Write it correctly, please... }
    NeedPos := True;
    NeedLine3 := True;
    if ExecDialog(TCoordInp.Init(Self, False, @DlgData)) = id_Ok then
    begin
    { Now it is just the time to change this damned circle... Brrr }
      AEllipse := PEllipse(GetFirstObject(ot_Circle));
      Point.X := XPos;
      Point.Y := YPos;
{$IFNDEF WMLT}
      if FuncChangeEllipse(AEllipse, Project, Point, Line3) then
        UpdateInfo; // Update prop. list entries itself except 'PROGIS ID'
{$ENDIF}
    end;
  end;
  SetCursorPos(OrigPoint.X, OrigPoint.Y); { Return mouse to orig. pos. }
end;
{-- Sygsky }

{++ IDB_INFO}

procedure TPropertyDialog.SetIDBInfo;
var
  SubEntry: TTreeEntry;
  iItemID: Integer;
  bFound: Boolean;
  iI, iLayerIndex: Integer;
  AList: TStringList;
  ALayer: PLayer;
  AItem: PIndex;
  sFieldName, sFieldValue: AnsiString;
// ++ Cadmensky IDB Version 2.3.8
  bValueAdded: Boolean;
// -- Cadmensky IDB Version 2.3.8
begin
{$IFNDEF WMLT}
   // Attribute data will be only shown for the only selected object.
  if (WinGISMainForm.WeAreUsingTheIDBUserInterface[Project]) and (Project.Layers.SelLayer.Data.GetCount = 1) then
  begin
    iItemID := Project.Layers.SelLayer.Data.At(0).Index;
    bFound := FALSE;
    for iI := 1 to Project.Layers.LData.Count - 1 do
    begin
      ALayer := Project.Layers.LData.At(iI);
      AItem := ALayer.HasIndexObject(iItemID);
      if (AItem <> nil) and (AItem.GetState(sf_Selected)) then
      begin
        bFound := TRUE;
        BREAK;
      end;
    end; // For iI end
    if bFound then
    begin
         // 17='Attribute data'
      SubEntry := AddInfo(InfoList.Tree, MlgStringList['IDB', 17], '', 16);

      AList := TStringList.Create;
      iLayerIndex := ALayer.Index;
      if WinGISMainForm.IDB_Man.X_GotoObjectRecord(Project, iLayerIndex, iItemID) then
      begin
// ++ Cadmensky IDB Version 2.3.8
        AList.Text := WinGISMainForm.IDB_Man.GetTableFieldsNamesList(Project, ALayer);
//            AList.Text := WinGISMainForm.IDB_Man.GetTableVisibleFieldsNamesList (Project, ALayer);
        bValueAdded := false;
// -- Cadmensky IDB Version 2.3.8
        for iI := 0 to AList.Count - 1 do
        begin
          sFieldName := AList[iI];
          if (not FieldIsCalculatedField(sFieldName)) and (AnsiUpperCase(sFieldName) <> 'PROGISID') then
          begin
// ++ Cadmensky IDB Version 2.3.8
//                  sFieldValue := WinGISMainForm.IDB_Man.X_GetFieldValue (Project, iLayerIndex, sFieldName);
            sFieldValue := VarToStr(WinGISMainForm.IDB_Man.X_GetFieldValue(Project, iLayerIndex, sFieldName));
            if sFieldValue <> '' then
            begin
              bValueAdded := true;
// -- Cadmensky IDB Version 2.3.8
              AddInfo(SubEntry, sFieldName, sFieldValue, -3);
            end;
          end;
        end; // For iI end
// ++ Cadmensky IDB Version 2.3.8
        if not bValueAdded then
          AddInfo(SubEntry, MlgStringList['IDB', 18], '', -3); // 18='No data'
// -- Cadmensky IDB Version 2.3.8
      end
      else
        AddInfo(SubEntry, MlgStringList['IDB', 18], '', -3); // 18='No data'
    end;
  end;
{$ENDIF}
end;
{-- IDB_INFO}
{$ENDIF} // <----------------- AXDLL

procedure TPropertyDialog.BtnPolyOrient1Click(Sender: TObject);
begin
  Project^.OrientPolys(True);
end;

procedure TPropertyDialog.BtnPolyOrient2Click(Sender: TObject);
begin
  Project^.OrientPolys(False);
end;

end.

