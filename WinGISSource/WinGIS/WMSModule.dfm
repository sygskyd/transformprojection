�
 TWMSFORM 0  TPF0TWMSFormWMSFormLeft�Top� BorderStylebsDialogCaption!1ClientHeight� ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 	FormStylefsStayOnTopOldCreateOrderPositionpoScreenCenterOnClose	FormCloseOnCreate
FormCreate	OnDestroyFormDestroyOnShowFormShowPixelsPerInch`
TextHeight TAxGisProjectionAxGisProjectionLeft� Top�Width2Height
ParentFontTabOrder ControlData
=   TPF0TPanel Left� Top�Width2HeightCaptionGisPro    	TGroupBoxGroupBoxSettingsLeftTopWidth�Height� TabOrder TLabel	LabelHostLeftTop(Width	HeightCaption!2  TLabelLabelLayersLeftTophWidth	HeightCaption!3  TLabel	LabelInfoLeftTopPWidth	HeightCursorcrHandPointCaption!7Font.CharsetDEFAULT_CHARSET
Font.ColorclBlueFont.Height�	Font.NameMS Sans Serif
Font.StylefsUnderline 
ParentFontOnClickLabelInfoClick  TLabelLabelLegendLeftTop� Width	HeightCursorcrHandPointCaption!8Font.CharsetDEFAULT_CHARSET
Font.ColorclBlueFont.Height�	Font.NameMS Sans Serif
Font.StylefsUnderline 
ParentFontOnClickLabelLegendClick  TLabel	LabelEPSGLeft� TophWidthHeightCaption!10  TLabelLabelFormatLeftXTophWidthHeightCaption!11  TSpeedButton
ButtonOpenLeft�TopWidthHeight
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU_�����UU     UUUwwwww_UP����UWu�UUUu�P𸸸��UW�_UUUW_P�����W�u����P��    W�WwwwwuP�����UUW�UUUW�UP�����UUW�UU_�UUP��� UUW_UUwuUUU��UUUUUu��UUUUUp UUUUUWwuUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU	NumGlyphsOnClickButtonOpenClick  TSpeedButton
ButtonSaveLeft�TopWidthHeight
Glyph.Data
�   �   BM�       v   (               �                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� ���������      �3   ��3   ��3   ��3    �333333�3    3�0�����0�����0�����0�����0���� �0�����      ��������OnClickButtonSaveClick  TLabelLabelEPSGLinkLeft� Top� WidthhHeightCursorcrHandPointCaptionwww.epsg-registry.orgFont.CharsetDEFAULT_CHARSET
Font.ColorclBlueFont.Height�	Font.NameMS Sans Serif
Font.StylefsUnderline 
ParentFontOnClickLabelEPSGLinkClick  TEditEditHostLeftTop8Width�HeightTabOrder OnChange
EditChange  TEdit
EditLayersLeftTopxWidth� HeightTabOrderOnChange
EditChange  	TComboBoxComboBoxFormatLeftXTopxWidthaHeightStylecsDropDownList
ItemHeightItems.Strings
image/jpeg	image/png TabOrderOnChange
EditChange  TEditEditEPSGLeft� TopxWidthaHeightTabOrderOnChange
EditChange  	TCheckBoxCheckBoxOnOffLeftTopWidth� HeightCaption!5Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderOnClickCheckBoxOnOffClick   	TGroupBoxGroupBoxButtonsLeftTop� Width�Height1TabOrder TButtonButtonApplyLeft TopWidthYHeightCaption!9Default	TabOrder OnClickButtonApplyClick  TButtonButtonCloseLeft`TopWidthYHeightCaption!6TabOrderOnClickButtonCloseClick   TMlgSection
MlgSectionSection	WMSModuleLeft  TOpenDialog
OpenDialogFilterWWC Files|*.wwcLeft0  TSaveDialog
SaveDialog
DefaultExt.wwcFilterWWC Files|*.wwcOptionsofOverwritePromptofHideReadOnlyofEnableSizing LeftP   