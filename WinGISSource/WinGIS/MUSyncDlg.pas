unit MUSyncDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  MultiLng, ComCtrls, StdCtrls;
                                                           
type
  TMUSyncForm = class(TForm)
    ProgressBar: TProgressBar;
    StaticTextStatus: TStaticText;
    MlgSection: TMlgSection;
  public
    procedure Display(AStatusText: AnsiString);
  end;

var
  MUSyncForm: TMUSyncForm;

implementation

{$R *.DFM}

procedure TMUSyncForm.Display(AStatusText: AnsiString);
begin
     ProgressBar.Position:=0;
     StaticTextStatus.Caption:=AStatusText;
     Show;
     Repaint;
end;

end.
