{$A-,B-,C-,D+,E-,F-,G+,H-,I-,J+,K-,L+,M-,N+,O-,P-,Q-,R-,S-,T-,U-,V-,W-,X+,Y+,Z1}

{$MINSTACKSIZE $00004000}

{$MAXSTACKSIZE $00100000}

{$IMAGEBASE $00400000}

{$APPTYPE GUI}

{******************************************************************************+
  Unit AM_Child
--------------------------------------------------------------------------------
  Projektfenster
+******************************************************************************}
Unit AM_Child;

Interface

Uses SysUtils,Messages,Windows,Classes,Dialogs,Forms,WinDos,Controls,Objects,
     AM_Proj,AM_Def,AM_Ini,AM_Pass,AM_Layer,AM_Punkt,
     UserIntf,WCtrls,CombPolyTxt,GrTools,MultiLng,MenuFn,AxGisPro_TLB,AM_Obj,AM_OLE, Am_CPoly,Attachhndl,Lists,

{$IFNDEF AXDLL} // <----------------- AXDLL
  {$IFNDEF WMLT}
     AM_Exp,AM_DBGra,AM_DDE,AM_Imp,AM_Cut,AM_ProjO,OmegaHndl{++Syg},
     DDEML,DDEDef,GifImage,Jpeg,Math,
  {$ENDIF}
{$ENDIF} // <----------------- AXDLL
     AM_LDTrf,AM_Texte,AM_Selec,AM_Ar,Transfor,ResDlg,USelect,ClpHndl,DigIntf,
     Rollup,ProjHndl,UDraw,IniFiles,
{$IFNDEF AXDLL} // <----------------- AXDLL
     WOpenPicDlg,
{$ENDIF} // <----------------- AXDLL
     Graphics,ExtCtrls,
{$IFNDEF AXDLL} // <----------------- AXDLL
     PrnHndl,
{$ENDIF} // <----------------- AXDLL
     StdCtrls, ComCtrls{++ Moskaliov BUG#90 BUILD#150 05.01.01};

//++ Glukhov TstTimer 29.03.01
// Don't work about midnight
var TstTimers : array[0..20] of Integer;
{++Brovak  Bug 500 This var set to true if user press Esc when process is working.
 Now it is for InsertTabs, but you can use it for your tasks.
 Set it to false before start your procedure}
StopExecutingFunction:boolean;
{--Brovak}

Procedure TstTimerInit( Idx: Integer );
Procedure TstTimersInit;
Procedure TstTimerStart( Idx: Integer );
Procedure TstTimerStop( Idx: Integer );
Function  TstTimerStr( Idx: Integer ) : String;    // Secs.msecs
//-- Glukhov TstTimer 29.03.01

const ZOM            =  1;    {f�r DDECommand -> [END]}
      GBE            =  2;
      DDD            =  3;
      DGR            =  4;
      SRQ            =  5;
      FIT            =  6;
      MOV            =  7;
      LBM            =  8;
      ASW            =  9;
      SOS            = 10;
      STA            = 11;
      DEL            = 12;
      COE            = 13;
      DK1            = 14;
      FID            = 15;
      ZOD            = 16;
      CPY            = 17;
      MOP            = 18;
      COP            = 19;
      TOP            = 20;
      IOS            = 21;
      CLPNT          = 22;
      FTXT           = 23;
      GCP            = 24;

      om_ChangeProj  = 0;
      om_OpenProj    = 1;
      om_NewProj     = 2;

      LASTIMAGEDIR: PChar = '\User\WINGISUI\LoadImage'; //++Sygsky

var DoesAutoSave   : Boolean;

type
  TMDIChild = class(TWForm)
    InsertTABsDialog: TWOpenDialog;
    MlgSection: TMlgSection;
    SaveDialog: TSaveDialog;
    UIExtension: TUIExtension;
    OpenDialogTxt: TOpenDialog;
    PanelAds: TPanel;
    ImgAds: TImage;
    LabelAdsLink: TLabel;
    LabelAdsText: TLabel;

    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanCloseForm: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormMDIActivate(Sender: TObject);
    procedure FormMDIDeactivate(Sender: TObject);
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure FormMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure FormResize(Sender: TObject);
{$IFNDEF AXDLL} // <----------------- AXDLL
    procedure FormShow(Sender: TObject);
{$ENDIF} // <----------------- AXDLL
    procedure UIExtensionUpdateContextToolbars(Sender: TObject);
    procedure UIExtensionUpdateCursor(Sender: TObject);
    procedure UIExtensionUpdateMenus(Sender: TObject);
    procedure UIExtensionUpdateStatusbar(Sender: TObject);
    procedure UIExtensionUpdateCustom(Sender: TObject; const PendingUpdates: TUpdateInfo);
    procedure UIExtensionShown(Sender: TObject);
{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}
    procedure SetGraphicParent(AParent: Integer);
    procedure ResetGraphicParent;
{$ENDIF}
    procedure FormDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
    procedure FormDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure FormCanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
    procedure FormDeactivate(Sender: TObject);
{$ENDIF} // <----------------- AXDLL
  private
{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}
    function GetAXObject: Variant;
{$ENDIF}
{$ENDIF} // <----------------- AXDLL
  public
    //AxGisProjection: TAxGisProjection;
    OriginalParent: THandle;
    FName: array[0..1023] of Char;
    HasTitle: Boolean;
    Data: PProj;
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
    ImpExport: PImport;
    DBGraphic: PBusGraphText;
{$ENDIF} // <----------------- AXDLL
       //ThreeDChild : TThreeDChild;
    ThreeDActiv: Boolean;
{$ENDIF}
    LastDDEEnd: Boolean;
    AutoCnt: Integer;
    DDECommand: Integer; {Erkennung bei [END] welcher Befehl vorher aktiv war}
    DoRedraw: Boolean; {kein Redraw bei manchen Datenbankaktivit�ten}
    OldRedraw: Boolean;
    HadNoRedraw: Boolean;
    DDEHadNoRdr: Boolean;
    RDRArea: TDRect; {zum Merken von nicht gezeichneten Bildschirmregionen}
    DDERDRArea: TDRect; {zum Merken von nicht gezeichneten Bildschirmregionen w�hrend DDE-Kommunikation}
//!?!?Routing       RouteMenu   : TRouteMenu;
    SendRedraw: Boolean;
    FirstDraw: Boolean;
    Colors256: Boolean;
    TRFString: array[0..255] of Char;
    OpenMode: Word;
    OMChanged: Boolean;
    LayerLst: TSTringList;
    LockHandle: Integer;
    WasReadOnly: Boolean;
       {WinHan3D    : THandle;}
    DDEString: string;
    SavedCursor: Integer;
    OpenAsMU: Boolean;
// ++ Cadmensky 2019
    constructor Create(AParent: TComponent; AFName: PChar; AProj: PProj; bMU: Boolean); reintroduce;
//       Constructor Create(AParent:TComponent;AFName:PChar;AProj:PProj;bMU:Boolean);
// -- Cadmensky 2019
    function CanClose: Boolean; virtual;
    procedure ChangeRedraw(DoRedrawWindow: Boolean);
    function LoadData: Boolean;
    procedure SetAutoSave;
    function StoreData: Boolean;
    procedure WndProc(var Message: TMessage); override;
    procedure WMTimer(var Msg: TMessage); message wm_First + wm_Timer;
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
    procedure RedrawCanvas;
    function DoDDECommands(SendedDDEString: PChar): Boolean;
    function AcceptExecute(hszDGTop, hszTopic: HSZ; pstr, ConvName: PChar): Boolean;
    procedure InitDB(Text: string; SendPath: Boolean);
    procedure SendProjDataToDB;
    procedure WMCommand(var Msg: TMessage); message wm_First + wm_Command;
    procedure WMDDEAck(var Msg: TMessage); message wm_First + wm_DDE_Ack;
    procedure WMDDEExecute(var Msg: TMessage); message wm_First + wm_DDE_Execute;
    procedure WMDDEInitiate(var Msg: TMessage); message wm_First + wm_DDE_Initiate;
    procedure WMDDETerminate(var Msg: TMessage); message wm_First + wm_DDE_Terminate;
    procedure WMMDIActivate(var Msg: TWMMDIActivate); message WM_MDIACTIVATE;
    procedure SetDBSendInitStrings(SendStrings: Boolean);
    property AXObject: Variant read GetAXObject;
    procedure FileSaveAs1;
    procedure FileSaveAs2(ANewName: PChar; ALTProj: Boolean; AskForOverwrite: Boolean);
    procedure DBFileSaveHSP(sdde: AnsiString);
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
    procedure WMPaint(var Msg: TWMPaint); message wm_First + wm_Paint;
{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}
    procedure GisManDisplayPass(Sender: TObject; const aPass: WideString);
    procedure GisManDisplayPercent(Sender: TObject; aValue: Integer);
{$ENDIF}
{$ENDIF} // <----------------- AXDLL
  private
    FCheckedMenus: TStringList;
    FInvalidRect: TRect;
    procedure WMEraseBkgnd(var Msg: TMessage); message wm_EraseBkgnd;
  protected
    procedure CreateParams(var Params: TCreateParams); override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure KeyPress(var Key: Char); override;
    procedure KeyUp(var Key: Word; Shift: TShiftState); override;
    procedure Loaded; override;
  public
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
    AxGisProjection: TAxGisProjection;
    InWMPaint: Boolean;
    IsClosing: Boolean;
       {}
    ProgisIdTMList: TIntList;
    CanvasParRect: TRect;
       {}
    IsOpenedReadOnly: Boolean;
    function GetLayerCount: Integer;
    function IsReadOnly: Boolean;
    procedure OnBringToFront;
    procedure OnGlobalOptionsChanged;
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
    procedure DDESnapObject(sDDE: AnsiString);
    procedure ExecuteMenuFunction(ItemName: AnsiString);
    procedure DBMoveToLayer(sDDE: AnsiString);
    procedure DBCutLineByLength(Id: Integer; Length: Double; Mode: Integer);
    procedure DBShowAllObjectsOnLayer(LayerName: string);
    procedure DBGenerateObjects(s: AnsiString);
    function DBPolyCombine(s: AnsiString): Integer;
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
  published
    procedure DefaultMenuHandler(Sender: TObject; MenuFunction: TMenuFunction);
{$IFNDEF AXDLL} // <----------------- AXDLL
    procedure DigitCalibrateDigitizer;
{$ENDIF} // <----------------- AXDLL
    procedure DigitTransformation;
    procedure DrawPoint;
    procedure DrawPolygon;
    procedure DrawPolyline;
    procedure DrawSnapModeOnOff;
    procedure VEOnOff;
    function VEOnWithLoginData(User, Pwd: AnsiString; Offline: Boolean): Integer;
    function VEOff: Integer;
    procedure VEOrtho;
    procedure VEOrthoLabels;
    procedure VERoad;
    procedure VEAutomatic;
    procedure VEGeoCode;
    procedure VEBirdsEye;
    procedure VEDBGeoCode;
    procedure VESLMap;
    procedure VESLExp;
    procedure VEShowInfo;
    procedure VEClearCache;
    procedure VEFillCache;
    procedure ExtrasWMS;
    procedure ConstructDimension;
{++ Ivanoff NA2 BUILD#106}
    procedure DrawOrthoOnOff;
                                    {-- Ivanoff}

{++ Brovak BUG 105 BUILD 132}
    procedure DrawRectByRubberBox; //brovak
    procedure DrawRectBy3Points; //brovak
{-- Brovak}
    procedure DrawSymbol;
    procedure DrawText;
{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}
      {++ brovak - 193 }
    procedure EditPointInVariousObjects;
    procedure SwitchEditPointInVariousObjects;
    procedure PreparePointInVariousObjects;
      {--brovak}
      {++ Brovak - for convert old island polygons to new}
    function ConvertOldIslandPoly(APoly: PCPoly; LayerIndex: integer): boolean;
    procedure SelectedConvertPoly;
    procedure OnLayerConvertPoly;
    function ConvertOldPolygonLayer(LayerIndex: integer): longint;
    procedure InProjectConvertPoly;
    procedure PseudoSelectMode;
      {-- Brovak}

      {++brovak - ole}
    procedure InsertOLEObj;
    procedure EditOLEObj;
      {--brovak}
       {brovak}
    procedure SpecialPrintFrameMakeAndShow(ThisRect: TRotRect);
       {brovak}
    procedure DBExplodePolys(DLayerName: string; SymName: string);
    procedure EditRotateSymbols;
    procedure EditDuplicateOnLayer;
    procedure EditDelete;
    procedure EditMove;
    procedure EditMoveToLayer;
    procedure EditOLEObject;
    procedure EditPointMove;
    procedure EditPointInsert;
    procedure EditPointDelete;
    procedure EditNeighbours;
    procedure EditText;
    procedure EditSelectAll;
    procedure EditScalePolyline;
    procedure ExtrasGPSLog;
    procedure ExtrasAreaAnalysis;
    procedure ExtrasAreaCut;
    procedure ExtrasObjectCut;
    procedure ExtrasPolyPointCut;
    procedure ExtrasAreaCutSelected;
    procedure ExtrasAutomaticSnap;
    procedure ExtrasCombine;
    procedure ExtrasCombinePolygonsWithTexts;
    procedure ExtrasCreateCutPoints;
    procedure ExtrasCoordinateInput;
    procedure ExtrasExplode;
    procedure ExtrasGenerate;
    procedure ExtrasGenerateObjects;
    procedure ExtrasGenerateArea;
    procedure ExtrasSplitLine;
    procedure ExtrasDKMCheck;
    procedure ExtrasMovePartParallel;
    procedure ExtrasNeighbourSearch;
    procedure ExtrasOffset;
    procedure ExtrasPolygonPart;
    procedure ExtrasFreePolygonPart;
    procedure ExtrasProjectBorder;
    procedure ExtrasSelect;
    procedure ExtrasTrimm;
    procedure ExtrasIUpdate;
    procedure ExtrasReplacePolyPoints;
    procedure ExtrasCreatePolyline;
    procedure ExtrasExplodeToPoints;
    procedure ExtrasExplodeToSymbols;
    procedure ExtrasFindEqualObjects;
    procedure ExtrasApplyTM;
    procedure FileExportASCII;
    procedure FileExportDXF;
    procedure ExtrasGPSConnect;
       // DX export functions
    procedure FileExportASCIIDX;
    procedure FileExportDXFDX;
    procedure FileExportAMPDX;
    procedure DDE_DoAmpExp;
    procedure DDE_DoAscExp;
    procedure DDE_DoDxfExp;
    procedure DDE_DoE00Exp;
    procedure DDE_DoMifExp;
    procedure DDE_DoShpExp;

    procedure DDE_DoExpSettings(DDECmd: string);
    procedure DDE_DoAddExpLayer(DDECmd: string);
    procedure DDE_DoSetDBSettings(DDECmd: string);
    procedure DDE_DoSetProjection(DDECmd: string);
    procedure DDE_DoExecExp;

    procedure FileExportE00DX;
    procedure FileExportMIFDX;
    procedure FileExportSHPDX;

    procedure FileExportOVL;

    procedure FileImportASCII;
    procedure FileImportDXF;
    procedure FileImportTXT;

       // DX import functions
    procedure FileImportASCIIDX;
    procedure FileImportDXFDX;
    procedure FileImportAMPDX;
    procedure FileImportMIFDX;
    procedure FileImportSHPDX;
    procedure FileImportE00DX;
    procedure FileImportDGNDX;
//       Procedure   FileImportDFK;

    procedure DDE_DoAmpImp;
    procedure DDE_DoAmpImpSettings(DDECmd: string);
    procedure DDE_DoAddImpLayer(DDECmd: string);
    procedure DDE_DoAscImp;
    procedure DDE_DoAscImpSettings(DDECmd: string);
    procedure DDE_DoAddImpFile(DDECmd: string);
    procedure DDE_DoDgnImp;
    procedure DDE_DoDxfImp;
    procedure DDE_DoDxfImpSettings(DDECmd: string);
    procedure DDE_DoE00Imp;
    procedure DDE_DoE00ImpSettings(DDECmd: string);
    procedure DDE_DoMifImp;
    procedure DDE_DoMifImpSettings(DDECmd: string);
    procedure DDE_DoShpImp;
    procedure DDE_DoShpImpSettings(DDECmd: string);
    procedure DDE_DoExecImp;

    function ProcCreateThematicMap(Connection: AnsiString; Command: AnsiString; ProgisIDField: AnsiString; ThematicField: AnsiString; Grouping: Integer; GroupCount: Integer; GroupingName: AnsiString; SchemaName: AnsiString; DestLayerName: AnsiString; AllowDialog: Boolean): Integer;
    function ProcApplyThematicMap: Integer;
    function ExtrasImportCoordinates(TxtFile: string; DefFile: string; ObjType: Integer; DBMode: Integer; DBName: string; DBTable: string): Boolean;
    procedure ExtrasIsoWizzard;
    procedure FileExportKML;

    procedure InsertAttachments;
    procedure SelectAttached;
    procedure InsertOLEObject;
    procedure InsertPicture;
    procedure FileSave;
    procedure MUSynchronize;
    procedure MUReorganize;
    procedure FileSaveAs;
    procedure FileReload;
    procedure InsertTABs;
    procedure TurboRasterEditImage;
    procedure TurboRasterLoadImages;
    procedure SelectTransparent;
    procedure ExtrasCombinePolygons;
    procedure ExtrasCombineSelectedPolygons;
    procedure ConvertSymbolToObjects;
    procedure CombinePolygonsFromDb(s: AnsiString);
{$ENDIF}
{$ENDIF} // <----------------- AXDLL
    procedure ExtrasMeasureDistance;
    procedure FileClose;
       {++brovak Close New Polyline with ShortCut}
    procedure ClosePolyline;
       {--brovak}

    procedure RasterTransformationLinear;
    procedure RasterTransformationHelmert;

        {++brovak #MOD for copying part of selected image}
    procedure CutSelectedPartOfImage;
       {--brovak}

    procedure ViewRedraw;
    procedure ViewColorPalette;
    procedure ViewGrayscalePalette;
    procedure ViewPicturePalette;
{$IFNDEF AXDLL} // <----------------- AXDLL
    procedure WMWTPacket(var Msg: TMessage); message wm_WT_Packet;
{$ENDIF} // <----------------- AXDLL
    procedure CreateIslandArea(LName: string);
    procedure ExtrasProjection;
    function MoveView(X, Y: Integer): Boolean;
    procedure ShowBorderCursor(X, Y: Integer);
    function CheckNewTempFile(ATempFileName: AnsiString): Boolean;
    procedure ShowAds;
    procedure ExtrasWKT;
    procedure ReadProjectIni;
  end;

{$IFNDEF WMLT}
Procedure SetDBApplication;
{$ENDIF}

Implementation

Uses AM_Main,AM_Paint,AM_View,Registry,AM_ImagC,Win32Def,FlatSB,ListHndl,Tooltips, AM_Group, AM_Combi, ProjRep
     {$IFNDEF AXDLL} // <----------------- AXDLL
     ,SymHndl
     {$ENDIF} // <----------------- AXDLL
     ,AM_Const
     {$IFNDEF AXDLL} // <----------------- AXDLL
     ,ViewHndl
     {$ENDIF} // <----------------- AXDLL
     ,Share,FileCtrl
     {$IFNDEF AXDLL} // <----------------- AXDLL
     ,PropHndl
     {$ENDIF} // <----------------- AXDLL
     ,BmpImage,AM_ProjP,LayerHndl,StyleDef,SymbolSelDlg,AM_Snap,AM_Index,LicenseHndl,ShellApi,
     AM_Point,AM_Poly,{AM_CPoly,}AM_Sym, VerMgr, PointCutDlg, AM_Coll,CoordinateSystem,
     Measures,Am_Sight
     {$IFNDEF AXDLL} // <----------------- AXDLL
       {$IFNDEF WMLT}
     ,SaveDlg,ImgDlg,PolyPartition,PolyPartDlg,AM_GPS,AXGisMan_TLB, AxGisCmb_TLB
     {++ IDB_DLL Interaction}
     , IDB_Messages, MultiLngFile
     {-- IDB_DLL Interaction}
       {$ENDIF}
     {$ENDIF} // <----------------- AXDLL
{++ Ivanoff NA2 BUILD#106}
     , Am_ProjM
{-- Ivanoff}
{++ Sygsky: add TAb support}
    , TabFiles, UserUtils, NewImageHndl, ReportDlg
{-- Sygsky }
    ,AM_Text,AM_Font, VEModule, WinOSInfo, MUOpenDlg, MUModule, ErrHndl, GPSConn,
    WMSModule, WKTDlg;

{$R *.DFM}

//++ Glukhov TstTimer 29.03.01
Procedure TstTimerInit( Idx: Integer );
begin
  TstTimers[Idx]:= 0;
end;
Procedure TstTimersInit;
var
  i : Integer;
begin
  for i:= 0 to 20 do TstTimerInit( i );
end;
// Doesn't work about midnight
Procedure TstTimerStart( Idx: Integer );
var
  vTime : TTimeStamp;
begin
  vTime:= DateTimeToTimeStamp( Time );
  TstTimers[Idx]:= TstTimers[Idx] - vTime.Time;
end;
// Doesn't work about midnight
Procedure TstTimerStop( Idx: Integer );
var
  vTime : TTimeStamp;
begin
  vTime:= DateTimeToTimeStamp( Time );
  TstTimers[Idx]:= TstTimers[Idx] + vTime.Time;
end;
Function  TstTimerStr( Idx: Integer ) : String;    // Secs.millisecs
var
  v : Double;
begin
  Result:= '';
  v:= TstTimers[Idx] / 1000;
  Str( v : 5 : 3, Result );
end;
//-- Glukhov TstTimer 29.03.01

constructor TMDIChild.Create(AParent: TComponent; AFName: PChar; AProj: PProj; bMU: Boolean);
begin
  WingisMainForm.AllowDraw := False;
  try
    OpenAsMU := bMU;
    HasTitle := True;
    IsClosing := False;
    InWMPaint := False;
{$IFNDEF AXDLL}
    IsOpenedReadOnly := WingisMainForm.NextProjIsReadOnly;
    WingisMainForm.NextProjIsReadOnly := False;
{$ENDIF}
    StrCopy(FName, AFName);
    Data := AProj;
    if Data <> nil then
    begin
      Data^.Registry.WriteDateTime('\Project\Info\CreationDate', Now);
      Data^.FName := FName;
    end;
    inherited Create(AParent);
{$IFNDEF AXDLL} // <----------------- AXDLL
    Caption := StrPas(FName);
    ControlStyle := ControlStyle - [csCaptureMouse];
{$ENDIF} // <----------------- AXDLL
    ProgisIdTMList := TIntList.Create;
  finally
    WingisMainForm.AllowDraw := True;
  end;
end;

Procedure TMDIChild.FormCloseQuery(Sender: TObject; var CanCloseForm: Boolean);
begin
  CanCloseForm:=CanClose;
end;

function TMDIChild.LoadData: Boolean;
var
  S: TOLEClientStream;
  Version: Byte;
  AFlags: Byte;
  Check: Word;
  KnowPwd: Boolean;
  SSPPwd: string[100];
begin
  Result := FALSE;
  Data := nil;
  try
    NotLoadedImagesCounter := 0;
    ProjectName := StrPas(FName);
    UserInterface.BeginWaitCursor;
    UserInterface.StatusText := GetLangText(4000);

    S := TOLEClientStream.Create(ProjectName, stOpenRead);

    S.Read(Check, SizeOf(Check));
    S.Read(Version, SizeOf(Version));

    S.EachBMP := False;
    KnowPwd := TRUE;
    if Version > 200 then
    begin
      KnowPwd := FALSE;
      Version := Version - 200;
      S.Read(SSPPwd, SizeOf(SSPPwd));
      SSPPwd := Uncode(SSPPwd);
      if CheckSSPPassword(WinGISMainForm, SSPPwd) then
      begin
        KnowPwd := TRUE;
      end;
    end;
    if KnowPwd then
    begin
      S.Read(AFlags, SizeOf(AFlags));
      S.ParentWindow := self;
      S.Seek(0, soFromBeginning);

      if Check = rn_Proj then
      begin
        if Version > vnProject then
        begin
          MsgBox(Handle, 3613, mb_IconExclamation or mb_Ok, IntToStr(Version))
        end
        else
        begin
          Data := Pointer(S.Get);
          if Data = nil then
          begin
            MsgBox(Handle, 1005, mb_OK or mb_IconExclamation, '');
          end
          else
          begin
            Data^.FSize := S.Size;
            if Data^.LoadResult then
            begin
              Result := True;
            end
            else
            begin
              MsgBox(Handle, 1005, mb_OK or mb_IconExclamation, '');
              if Data <> nil then
              begin
                Dispose(Data, Done);
              end;
              Data := nil;
            end;
          end;
        end;
      end
      else
      begin
        MsgBox(Handle, 1005, mb_OK or mb_IconExclamation, '');
      end;
    end;
    S.Free;

//    S.Done;
    if (Result = TRUE) then
    begin
      if MovedImagesCounter <> 0 then
      begin
        Data.SetModified;
        MovedImagesCounter := 0;
      end;
    end;
  finally
    UserInterface.StatusText := '';
    UserInterface.EndWaitCursor;
  end;
  if not Result then
  begin
    Close;
  end;
end;

{function TMDIChild.LoadData: Boolean;
var
  S: TOLEClientStream;
  Version: Byte;
  AFlags: Byte;
  Check: Word;
  KnowPwd: Boolean;
  SSPPwd: string[100];
begin
  Result := FALSE;
  Data := nil;
  try
    NotLoadedImagesCounter := 0;
    ProjectName := StrPas(FName);
    UserInterface.BeginWaitCursor;
    UserInterface.StatusText := GetLangText(4000);

    S.Init(FName, stOpenRead, StrBufSize);

    S.Read(Check, SizeOf(Check));
    S.Read(Version, SizeOf(Version));

    S.EachBMP := False;
    KnowPwd := TRUE;
    if Version > 200 then
    begin
      KnowPwd := FALSE;
      Version := Version - 200;
      S.Read(SSPPwd, SizeOf(SSPPwd));
      SSPPwd := Uncode(SSPPwd);
      if CheckSSPPassword(WinGISMainForm, SSPPwd) then
      begin
        KnowPwd := TRUE;
      end;
    end;
    if KnowPwd then
    begin
      S.Read(AFlags, SizeOf(AFlags));
      S.ParentWindow := self;
      S.Seek(0);

      if Check = rn_Proj then
      begin
        if Version > vnProject then
        begin
          MsgBox(Handle, 3613, mb_IconExclamation or mb_Ok, IntToStr(Version))
        end
        else
        begin
          Data := Pointer(S.Get);
          if Data = nil then
          begin
            MsgBox(Handle, 1005, mb_OK or mb_IconExclamation, '');
          end
          else
          begin
            Data^.FSize := S.GetSize;
            if Data^.LoadResult then
            begin
              Result := True;
            end
            else
            begin
              MsgBox(Handle, 1005, mb_OK or mb_IconExclamation, '');
              if Data <> nil then
              begin
                Dispose(Data, Done);
              end;
              Data := nil;
            end;
          end;
        end;
      end
      else
      begin
        MsgBox(Handle, 1005, mb_OK or mb_IconExclamation, '');
      end;
    end;
    S.Done;
    if (Result = TRUE) then
    begin
      if MovedImagesCounter <> 0 then
      begin
        Data.SetModified;
        MovedImagesCounter := 0;
      end;
    end;
  finally
    UserInterface.StatusText := '';
    UserInterface.EndWaitCursor;
  end;
  if not Result then
  begin
    Close;
  end;
end;}

function TMDIChild.StoreData: Boolean;
var
  S: TOldFileStream;
  ABuffer: array[0..1023] of Char;
  TempFileName: AnsiString;
  BakFileName: AnsiString;
  DataStored: Boolean;
begin
  Result := False;
  DataStored := False;
  if (IsOpenedReadOnly) or (WingisLicense < WG_STD) then
  begin
    exit;
  end;
  if (Data^.PInfo^.IsMU) then
  begin
    exit;
  end;
{$IFNDEF AXDLL}
  if not HasTitle then
  begin
    if SaveDialog.Execute then
    begin
      WinGISMainForm.SetCurrentTDocumentFilename(strpas(FName), SaveDialog.FileName);
      StrCopy(FName, PChar(SaveDialog.FileName));
    end
    else
    begin
      exit;
    end;
  end;
{$ENDIF}
  if (Data^.FileVersion < 20) and (MsgBox(Handle, 16000, mb_IconExclamation or mb_YesNo or mb_DefButton2, StrPas(FName)) <> ID_Yes) then
  begin
    exit;
  end;
{$IFNDEF AXDLL}
  if WinGisMainForm.WeAreUsingTheIDB[Data] then
  begin
    if HasTitle then
    begin
      WinGisMainForm.IDB_Man.ProjectIsSaving(Data)
    end
    else
    begin
      WinGISMainForm.IDB_Man.SaveDatabaseAs(Data, Data.FName);
    end;
    SELF.Data.bProjectIsNewAndNoSaved := FALSE;
  end;
{$ENDIF}
  UserInterface.BeginWaitCursor;
  UserInterface.StatusText := GetLangText(4001);
  WingisMainForm.IsSaving := True;
  try
    if not WasReadOnly then
    begin
      TempFileName := ChangeFileExt(StrPas(FName), '.$$$');
      BakFileName := ChangeFileExt(StrPas(FName), '.bak.amp');
      try
        S := TOldFileStream.Create(TempFileName, stCreate);
        Data^.Registry.WriteDateTime('\Project\Info\ModificationDate', Now);
        S.Put(Data);
        S.Free;
        DataStored := True;
      except
        on E: Exception do
        begin
          DebugMsg('TMDIChild.StoreData', E.Message);
        end;
      end;
      if (S.Status = stOK) and (DataStored) then
      begin
        UnlockFileWrites(LockHandle);
        try
          SetAutoSave;
          Result := CheckNewTempFile(TempFileName);
          if Result then
          begin
            if IniFile^.Options.BackupFile then
            begin
              if FileExists(BakFileName) then
              begin
                DeleteFile(StrPCopy(ABuffer, BakFileName));
              end;
              if FileExists(StrPas(FName)) then
              begin
                Result := RenameFile(StrPas(FName), BakFileName);
              end;
            end
            else
              if FileExists(StrPas(FName)) then
              begin
                Result := DeleteFile(FName);
              end;
            if Result then
            begin
              RenameFile(TempFileName, StrPas(FName));
            end;
          end;
        finally
          LockHandle := LockFileWrites(StrPas(FName));
        end;
        if HasTitle = False then
        begin
          HasTitle := True;
          SetWindowText(Handle, FName);
        end;
      end;
      if not Result then
      begin
        DeleteFile(StrPCopy(ABuffer, TempFileName));
        if not CoreConnected then
        begin
          MsgBox(Handle, 4101, mb_IconExclamation or mb_OK, StrPas(FName));
        end;
      end
      else
      begin
        Data^.Modified := FALSE;
        Data^.FileVersion := vnProject;
      end;
    end
    else
    begin
      MsgBox(Handle, 4105, mb_IconExclamation or mb_OK, StrPas(FName));
    end;
  finally
    UserInterface.EndWaitCursor;
    UserInterface.StatusText := '';
    WingisMainForm.IsSaving := False;
  end;
{$IFNDEF AXDLL}
  if (FileExists(FName)) then
  begin
    WinGisMainForm.FileHistory.NoAddHistoryFlag := false;
    WinGisMainForm.FileHistory.Add(AnsiString(FName));
  end;
{$ENDIF}
end;

Function TMDIChild.CanClose : Boolean;
  var BResult : Integer;
  begin
    CanClose:=TRUE;

    if DoTerminate then exit;

    if  ( {$IFNDEF AXDLL} {$IFNDEF WMLT}(not WinGISMainForm.CheckTDocumentExists(strpas(FNAME))) and {$ENDIF} {$ENDIF} //avoid check if the file should be saved twice
       (strpas(FNAME) <> 'SymbolEditor')) then // and MDIChild is not Symboleditor
    begin
       exit;
    end;
    if Data<>NIL then begin
      if Data.SymbolMode = sym_Omega then {++Sygsky: Omega }
      begin
        {$IFNDEF AXDLL} // <----------------- AXDLL
          {$IFNDEF WMLT}
        if OmegaHndl.OmegaInUse then
          if MsgBox(Handle,3150,MB_YESNO+MB_ICONWARNING+MB_DEFBUTTON2,IntToStr(OmegaCount)) = IDYES then
            StopOmega(True)
          else
          begin
            Result := False; { Can't close now as user wants to complete  Omega }
            Exit;
          end;
          {$ENDIF}
        {$ENDIF} // <----------------- AXDLL
      end
      else {--Sygsky: Omega}
      if (not Data^.SymEditInfo.CloseSymEditor) then
      begin
        Result := False;     { Symboleditor konnte nicht beendet werden }
        Exit;                { Benutzerabfrage }
      end;
      if (PSymbols(Data^.PInfo^.Symbols)^.StoreExtLibs(True)=mrCancel) then
                 CanClose:=False;
      if (Data^.SymbolMode=sym_SymbolMode) then { bei not Data^.Modified mu� auch aufger�umt werden }
                                                  { bzw. Demo Version ohne speichern }
          Data^.SymEditInfo.ResetEditChildPtr;
      {$IFNDEF WMLT}
        {$IFNDEF AXDLL} // <----------------- AXDLL
      if Result and (DDEHandler<>NIL) then DDEHandler.CloseProject(Self);
      if not Data^.IsCanvas then begin
         if (Data^.Modified) and (not Data^.PInfo^.IsMU) then begin
            if (Data^.SymbolMode<>sym_SymbolMode) then begin
               if not IniFile^.ReadBoolean('SETTINGS','SuppressSaveDlg',False) then begin
                  BResult:=MsgBox(Handle,2040,mb_YesNoCancel or mb_IconQuestion,StrPas(FName));
                  if BResult = 6 then begin
                     CanClose:=StoreData;
                  end
                  else if (BResult = 2) then begin
                     CanClose:=False
                  end;
               end;
            end;
         end;

         if (Data^.Modified) and (Data^.PInfo^.IsMU) then begin
            CanClose:=MU.CloseProject;
         end;

      end;
        {$ENDIF} // <----------------- AXDLL
      {$ELSE}
      CanClose:=True;
      {$ENDIF}
    end;
  end;

Procedure TMDIChild.ChangeRedraw
   (
   DoRedrawWindow   : Boolean
   );
  begin
    OldRedraw:=DoRedraw;
    DoRedraw:=DoRedrawWindow;
  end;

{$IFNDEF AXDLL} // <----------------- AXDLL
  {$IFNDEF WMLT}
Procedure TMDIChild.InitDB
   (
   Text            : String;
   SendPath        : Boolean
   );
  var AFName       : Array [0..255] of Char;
      Dir          : Array [0..255] of Char;
      Ext          : Array [0..255] of Char;
      SendStr      : String;
  begin
    DDEHandler.EnableDDEReception(FALSE);
    if SendPath then SendStr:=StrPas(FName)
    else begin
      FileSplit(FName,Dir,AFName,Ext);
      SendStr:=StrPas(AFName);
    end;
    DDEHandler.InitiateDDE(Handle,FALSE,FALSE);
    DDEHandler.CombineParamsOfAllActiveConnections;
    DDEHandler.SetActualChild(Self);
    DDEHandler.SendString('['+Text+']['+SendStr+']');
    DDEHandler.SendString('[RED][]');
    DDEHandler.EnableDDEReception(TRUE);
  end;

Procedure TMDIChild.SetDBSendInitStrings(SendStrings:Boolean);
begin
  if SendStrings then DDEHandler.SetDBSendStrings(DDECommAll,Self)
  else DDEHandler.SetDBSendStrings(DDECommInit,Self);
end;

Procedure TMDIChild.SendProjDataToDB;
var AFName       : Array [0..255] of Char;
    Dir          : Array [0..255] of Char;
    Ext          : Array [0..255] of Char;
    SendStr      : String;
begin
  DDEHandler.EnableDDEReception(FALSE);
  if DDEHandler.FChange = 1 then DDEHandler.SendString('[PRW]['+StrPas(FName)+']')
  else begin
    FileSplit(FName,Dir,AFName,Ext);
    SendStr:=StrPas(AFName);
    DDEHandler.SendString('[PRW]['+SendStr+']');
  end;
  ProcDBSendViews(Data);
  if DDEHandler.FLayerInfo <> 0 then Data^.Layers^.DBSendLayerInfo(Data);
  DDEHandler.SetAnswerMode(FALSE);
  DDEHandler.EnableDDEReception(TRUE);
end;

Procedure TMDIChild.WMDDEAck(var Msg:TMessage);
begin
  DDEHandler.DDEAcknowledge(Msg);
end;

Procedure TMDIChild.WMDDEInitiate(var Msg:TMessage);
begin
  DDEHandler.DoDDEInitiate(Msg);
end;

Procedure TMDIChild.WMDDETerminate(var Msg:TMessage);
begin
  if DDEHandler <> NIL then DDEHandler.DoDDETerminate(Msg.wParam,TRUE,FALSE);
  UserInterface.Update([uiMenus]);
end;

{******************************************************************************+
  Function TMDIChild.AcceptExecute
--------------------------------------------------------------------------------
  Pr�ft eine vom DB-Client kommendes DDE-Kommando und gibt bei G�ltigkeit
  eine Botschaft an das MDI-Fenster.
--------------------------------------------------------------------------------
  Parameter:
   hszDGTop        = akzepierte Spezifikation
   hszTopic        = Spezifikation der Daten
   PStr            = Datenblock
   ConvHandle      = Konversationshandle
--------------------------------------------------------------------------------
  Ergebnis: TRUE, wenn das Kommando bearbeitet wurde.
{******************************************************************************}
Function TMDIChild.AcceptExecute(hszDGTop:HSZ;hszTopic:HSZ;PStr:PChar;
    ConvName:PChar):Boolean;
begin
  Result:=FALSE;
  DDEHandler.RequestFrom:=AMDDEML;
  DDEHandler.SetDDECommMode(CommDDEML,ConvName);
  if hszTopic = hszDGTop then begin
    if PStr <> NIL then DoDDECommands(PStr);
    Result:=TRUE;
  end;
end;

{******************************************************************************+
  Procedure TMDIChild.WMDDEExecute
--------------------------------------------------------------------------------
  Verarbeitet die vom mit der fensterbotschaftsbasierten DDE-Kommunikation
  verbundenen Datenbank-Server eingehendenen DDE-Kommandostrings und gibt
  bei G�ltigkeit eine Botschaft an das MDI-Fenster.
--------------------------------------------------------------------------------
  Parameter:
   Msg             = vom Server kommende Fensterbotschaft
+******************************************************************************}
Procedure TMDIChild.WMDDEExecute(var Msg:TMessage);
Type GPSCom      = Array [0..6] of Char;
     TRCom       = Array[0..10] of Char;
     SeqCommand  = Array [0..4] of Char;
var GlobalPtr    : Pointer;
    GP           : Pointer;
    EndMode      : Boolean;
    SendAck      : Boolean;
    TRComm       : Boolean;
begin
  DDEHandler.RequestFrom:=AMDDEWM;
  DDEHandler.SetDDECommMode(CommDDEWM,NIL);
  SendAck:=TRUE;
  TRComm:=FALSE;
  GP:=GlobalLock(Msg.lParam);
  if GP <> NIL then begin
    DDEHandler.FGotLInfo:=DDEHandler.GetDBLInfo;
    case DDEHandler.GetDBStrFormat of
      WinGISStrFmt : GlobalPtr:=GP;
      DesmaStrFmt  : GlobalPtr:=DDEHandler.TranslateInStringDesma(GP,EndMode);
    else
      exit;
    end;
    if  (GPSCom(GlobalPtr^) = '[GPSPOS')
        or (GPSCom(GlobalPtr^) = '[GPSOFF') { Setzen des GPS-Offsets f�r Linien         }
        or (GPSCom(GlobalPtr^) = '[GPSSHO') { Zoomen auf die angegebene GPS-ID          }
        or (GPSCom(GlobalPtr^) = '[SETGPS') { Setzen der Projektion                     }
        or (GPSCom(GlobalPtr^) = '[LOADGR') { Laden einer Referenzierung                }
        or (GPSCom(GlobalPtr^) = '[GPSDBC') { Setzen der Datenbankeinstellungen         }
        or (GPSCom(GlobalPtr^) = '[GPSPRO') { Durchf�hren einer GPS-Projektionsrechnung }
        or (GPSCom(GlobalPtr^) = '[GPSNEW') { Erzeugen einer neune GPS-ID               }
        or (GPSCom(GlobalPtr^) = '[GPSDEL') { L�schen einer GPS-ID                      }
        or (GPSCom(GlobalPtr^) = '[GPSUPD') { �ndern der Einstellungen einer GPS-ID     }
        or (GPSCom(GlobalPtr^) = '[GPSDAT') then SendAck:=FALSE { Einlesen eines GPS-Datenfiles             }
    else if (TRCom(GlobalPtr^) = '[GETREFPTS]') or (TRCom(GlobalPtr^) = '[LDTRFILES(') then TRComm:=TRUE;
    if not DDEHandler.MacroMode then begin
      if SeqCommand(GlobalPtr^) = '[MCB]' then begin
        DDEHandler.MacroMode:=TRUE;
        UserInterface.Update([uiMenus]);
      end
      else DoDDECommands(GlobalPtr);
    end
    else if DDEHandler.MacroMode then begin
      if SeqCommand(GlobalPtr^) = '[MCE]' then PostMessage(Handle,wm_Command,cm_EndDDESeq,0)
      else DDEHandler.InsertAllDDEData(GlobalPtr)
    end;
    if DDEHandler.GetDBStrFormat = DesmaStrFmt then StrDispose(GlobalPtr);
  end;
  GlobalUnlock(Msg.lParamHi);
  if SendAck then begin
    if TRComm then begin
      if WinGISMainForm.TurboRasterDDE^.IsConnected then
        WinGISMainForm.TurboRasterDDE^.Acknowledge(PackDDElParam(wm_DDE_Ack,$8000,Msg.lParam));
    end
    else begin
      GlobalUnlock(Msg.lParam);
      DDEHandler.SendAck(Handle,PackDDElParam(wm_DDE_Ack,$8000,Msg.lParam));
    end;
  end;
end;

Function TMDIChild.DoDDECommands(SendedDDEString:PChar):Boolean;
var LdFName      : Array[0..128] of Char;
    Pos          : PChar;
    Pos2         : PChar;
{++ Ivanoff 4 Bolt}
    pcTemp: PChar;
    sTemp: AnsiString;
    ilTemp: LongInt;
{-- Ivanoff 4 Bolt}
    s            : String;
    i,c          : Integer;
    Res          : Integer;
begin
  Result:=FALSE;
  if StrLComp(SendedDDEString,'[RUN]',5) = 0 then begin
       WingisMainForm.ExecDDEFile(GetDDEItem(AnsiString(SendedDDEString),2));
  end
  else if StrLComp(SendedDDEString,'[SHW]',5) = 0 then begin
     Result:=True;
     WingisMainForm.ActivateWingis;
  end
  else if StrLComp(SendedDDEString,'[ASW]',5) = 0 then begin
    Data^.SetCursor(crHourGlass);
    Result:=TRUE;
    if DDECommand = 0 then DDEHandler.DeleteDDEData;
    DDECommand:=ASW;
    DDEHandler.InsertDDEData(SendedDDEString);
    LastDDEEnd:=FALSE;
  end
{**************************************** [END] - Anfang ****************************************}
  else if StrLComp(SendedDDEString,'[END]',5) = 0 then begin
    Result:=TRUE;
    LastDDEEnd:=TRUE;
    if (WinGISMainForm.LastImpExpOperation = 'AMPEXP') or
       (WinGISMainForm.LastImpExpOperation = 'ASCEXP') or
       (WinGISMainForm.LastImpExpOperation = 'DXFEXP') or
       (WinGISMainForm.LastImpExpOperation = 'E00EXP') or
       (WinGISMainForm.LastImpExpOperation = 'MIFEXP') or
       (WinGISMainForm.LastImpExpOperation = 'SHPEXP') then begin // check if Export batch is current in work
       DDE_DoExecExp;
    end
    else if (WinGISMainForm.LastImpExpOperation = 'AMPIMP') or
            (WinGISMainForm.LastImpExpOperation = 'ASCIMP') or
            (WinGISMainForm.LastImpExpOperation = 'DGNIMP') or
            (WinGISMainForm.LastImpExpOperation = 'DXFIMP') or
            (WinGISMainForm.LastImpExpOperation = 'E00IMP') or
            (WinGISMainForm.LastImpExpOperation = 'MIFIMP') or
            (WinGISMainForm.LastImpExpOperation = 'SHPIMP') then begin // check if import batch is current in work
            DDE_DoExecImp;
    end
    else if DDECommand=GCP then begin
      DDEHandler.EnableDDEReception(FALSE);
      ProcDBGetCenter(Data);
      DDEHandler.EnableDDEReception(TRUE);
      DDECommand:=0;
    end
    else if DDECommand=CLPNT then begin
      DDEHandler.EnableDDEReception(FALSE);
      Data^.CalcLinePoint;
      DDEHandler.EnableDDEReception(TRUE);
      DDECommand:=0;
    end
    else if DDECommand=FTXT then begin
      DDEHandler.EnableDDEReception(FALSE);
      ProcDBCreateTextFast(Data);
      DDEHandler.EnableDDEReception(TRUE);
      DDECommand:=0;
    end
    else if DDECommand=ZOM then begin                            { f�r Objekte selektieren mit zoomen }
      DDEHandler.EnableDDEReception(FALSE);
      SendMessage(Handle,wm_Command,cm_DBZoomObjects,0);
      DDECommand:=0;
    end
    else if DDECommand=FIT then begin                       { f�r Objekte selektieren }
      DDEHandler.EnableDDEReception(FALSE);
      SendMessage(Handle,wm_Command,cm_DBSelObjects,0);
      DDECommand:=0;
    end
    else if DDECommand=ZOD then begin                       { f�r Objekte selektieren mit zoomen und vorher deselektieren }
      DDEHandler.EnableDDEReception(FALSE);
      SendMessage(Handle,wm_Command,cm_DBDZoomObjects,0);
      DDECommand:=0;
    end
    else if DDECommand=FID then begin                       { f�r Objekte selektieren und vorher deselektieren }
      DDEHandler.EnableDDEReception(FALSE);
      SendMessage(Handle,wm_Command,cm_DBDSelObjects,0);
      DDECommand:=0;
    end
    else if DDECommand=IOS then begin                       { f�r selektierte Objekte anfordern }
      DDEHandler.EnableDDEReception(FALSE);
      SendMessage(Handle,wm_Command,cm_DBSelectedObj,0);
      DDECommand:=0;
    end
    else if DDECommand=TOP then begin                       { f�r Objektdatenanforderung }
      DDEHandler.EnableDDEReception(FALSE);
      SendMessage(Handle,wm_Command,cm_SendObjProps,0);
      DDECommand:=0;
    end
    else if DDECommand=ASW then begin                       { f�r Objekte auf obersten Layer kopieren }
      DDEHandler.EnableDDEReception(FALSE);
      SendMessage(Handle,wm_Command,cm_DBInsert,0);
      DDECommand:=0;
    end
    else if DDECommand=GBE then begin                       { f�r Text�bernahme in die Grafik }
      DDEHandler.EnableDDEReception(FALSE);
      SendMessage(Handle,wm_Command,cm_TextGraph,0);
      DDECommand:=0;
    end
    else if DDECommand=DDD then begin                       { f�r Diagramm�bernahme in die Grafik }
      DDEHandler.EnableDDEReception(FALSE);
      SendMessage(Handle,wm_Command,cm_BusGraph,0);
      DDECommand:=0;
    end
    else if DDECommand=DGR then begin                       { f�r Erzeugung neuer Objekte aus der Datenbank }
      DDEHandler.EnableDDEReception(FALSE);
      SendMessage(Handle,wm_Command,cm_InsObjs,0);
      DDECommand:=0;
    end
    else if DDECommand=LBM then begin                       { f�r Laden von Bitmaps aus der Datenbank }
      DDEHandler.EnableDDEReception(FALSE);
      SendMessage(Handle,wm_Command,cm_LoadBitFromDB,0);
      DDECommand:=0;
    end
    else if DDECommand=SRQ then begin                       { f�r Symbolauswahl aus der Datenbank }
      DDEHandler.EnableDDEReception(FALSE);
      SendMessage(Handle,wm_Command,cm_SymbolRequest,0);
      DDECommand:=0;
    end
(*!?!?!Route    else if DDECommand=STA then begin                       { f�r Symbolauswahl aus der Datenbank }
      if DDEHandler.MacroMode then SendMessage(Handle,wm_Command,cm_StartRoute,0)
      else SendMessage(Handle,wm_Command,cm_StartRoute,0);
      DDECommand:=0;
    end*)
    else if DDECommand=DK1 then begin
      DDEHandler.EnableDDEReception(FALSE);
      SendMessage(Handle,wm_Command,cm_DKM,0);
      DDECommand:=0;
    end
    else if DDECommand=SOS then begin                       { f�r Erzeugung neuer Objekte aus der Datenbank }
      DDEHandler.EnableDDEReception(FALSE);
      SendMessage(Handle,wm_Command,cm_DBObjStyle,0);
      DDECommand:=0;
    end
    else if DDECommand=COE then begin                       { f�r Erzeugung neuer Objekte aus der Datenbank }
      DDEHandler.EnableDDEReception(FALSE);
      SendMessage(Handle,wm_Command,cm_CheckObjExist,0);
      DDECommand:=0;
    end
    else if DDECommand=CPY then begin                       { f�r Kopieren von Objekten aus der Datenbank }
      DDEHandler.EnableDDEReception(FALSE);
      SendMessage(Handle,wm_Command,cm_DBCopyObject,0);
      DDECommand:=0;
    end
    else if DDECommand=COP then begin                       { f�r Kopieren von Objekten aus der Datenbank }
      DDEHandler.EnableDDEReception(FALSE);
      SendMessage(Handle,wm_Command,cm_DBCopyObject2,0);
      DDECommand:=0;
    end
    else if DDECommand=DEL then begin                       { f�r L�schen von Objekten aus der Datenbank }
      DDEHandler.EnableDDEReception(FALSE);
      SendMessage(Handle,wm_Command,cm_DBDelete,0);
      DDECommand:=0;
    end
    else if DDECommand=MOV then begin                       { f�r Objektverschiebung aus der Datenbank }
      DDEHandler.EnableDDEReception(FALSE);
      SendMessage(Handle,wm_Command,cm_MoveFromDB,0);
      DDECommand:=0;
    end
    else if DDECommand=MOP then begin                       { f�r Objektverschiebung auf abs. Position aus der Datenbank }
      DDEHandler.EnableDDEReception(FALSE);
      SendMessage(Handle,wm_Command,cm_MoveAbsFromDB,0);
      DDECommand:=0;
    end;
    ImpExport^.DBEnd;
    Data^.SetCursor(crDefault);
  end
{**************************************** [END] - Ende ****************************************}
  else if StrLComp(SendedDDEString,'[SCO]',5) = 0 then begin
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.DeleteDDEData;
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_SelModeGIS,0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[SMB]',5) = 0 then Data^.SelNewObjectsDB:=TRUE
  else if StrLComp(SendedDDEString,'[SME]',5) = 0 then begin
    DDEHandler.EnableDDEReception(FALSE);
    SendMessage(Handle,wm_Command,cm_SelModeDBEnd,0);
  end
  else if StrLComp(SendedDDEString,'[SWP]',5) = 0 then begin
    StrCopy(WinGISMainForm.MWDDELine,SendedDDEString);
    SendMessage(WinGISMainForm.Handle,wm_Command,cm_SetMainWinSize,0);
  end
  else if StrLComp(SendedDDEString,'[STX]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_DBSetText,0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[TXT]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_DBSetTextAPar,0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[COE]',5) = 0 then begin
    Data^.SetCursor(crHourGlass);
    Result:=TRUE;
    if DDECommand = 0 then DDEHandler.DeleteDDEData;
    DDECommand:=COE;
    DDEHandler.InsertDDEData(SendedDDEString);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[SPR]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    SendMessage(Handle,wm_Command,cm_Save,0);
  end
  else if StrLComp(SendedDDEString,'[SOS]',5) = 0 then begin
    Result:=TRUE;
    if DDECommand = 0 then DDEHandler.DeleteDDEData;
    DDECommand:=SOS;
    DDEHandler.InsertDDEData(SendedDDEString);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[LAN]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_DBAddLayerNDlg,0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[LAC]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_DBChaLayerNDlg,0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[LDN]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.InsertDDEData(SendedDDEString);
    ProcDBDelLayerWithoutDlg(Data);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[LRQ]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_RequestLayer,0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[LMR]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    SendMessage(Handle,wm_Command,cm_RequestLayers,0);
  end
  else if StrLComp(SendedDDEString,'[TSL]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_DBSetTSLayer,0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[REL]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_DBRecharge,0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[PSA]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_DBProjSaveAs,0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[PSA]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_DBProjSaveAs,0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[JAH]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_DBSFGLayers,0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[FSC]',5) = 0 then begin
    Result:=TRUE;
    {if not Data^.ActualMen in mnConstructs then begin}
    if (Data^.ActualMen <> mn_Construct) and (Data^.ActualMen <> mn_InsPoint)
       and (Data^.ActualMen <> mn_CInsPoint) and (Data^.ActualMen <> mn_DelPoint)
       and (Data^.ActualMen <> mn_CDelPoint) and (Data^.ActualMen <> mn_Snap)
       and (Data^.ActualMen <> mn_CMovePoint) and (Data^.ActualMen <> mn_CParallel)
       and (Data^.ActualMen <> mn_CBufferDraw) and (Data^.ActualMen <> mn_CBufferSel)
       and (Data^.ActualMen <> mn_CIslands) then begin
      DDEHandler.EnableDDEReception(FALSE);
      StrDispose(Data^.Construct^.DDEData);
      Data^.Construct^.DDEData:=StrNew(SendedDDEString);
      SendMessage(Handle,wm_Command,cm_ConstructFS,0);
      LastDDEEnd:=FALSE;
    end
    else begin
      LastDDEEnd:=TRUE;
    end;
  end
  else if StrLComp(SendedDDEString,'[SCC]',5) = 0 then begin
    Result:=TRUE;
    {if not Data^.ActualMen in mnConstructs then begin}
    if (Data^.ActualMen <> mn_Construct) and (Data^.ActualMen <> mn_InsPoint)
       and (Data^.ActualMen <> mn_CInsPoint) and (Data^.ActualMen <> mn_DelPoint)
       and (Data^.ActualMen <> mn_CDelPoint) and (Data^.ActualMen <> mn_Snap)
       and (Data^.ActualMen <> mn_CMovePoint) and (Data^.ActualMen <> mn_CParallel)
       and (Data^.ActualMen <> mn_CBufferDraw) and (Data^.ActualMen <> mn_CBufferSel)
       and (Data^.ActualMen <> mn_CIslands) then begin
      DDEHandler.EnableDDEReception(FALSE);
      StrDispose(Data^.Construct^.DDEData);
      Data^.Construct^.DDEData:=StrNew(SendedDDEString);
      SendMessage(Handle,wm_Command,cm_ConstructS,0);
      LastDDEEnd:=FALSE;
    end
    else begin
      LastDDEEnd:=TRUE;
    end;
  end
  else if StrLComp(SendedDDEString,'[DEL]',5) = 0 then begin
    Data^.SetCursor(crHourGlass);
    Result:=TRUE;
    if DDECommand = 0 then DDEHandler.DeleteDDEData;
    DDECommand:=DEL;
    DDEHandler.InsertDDEData(SendedDDEString);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[CLL]',5) = 0 then begin
    Data^.SetCursor(crHourGlass);
    DDEHandler.EnableDDEReception(FALSE);
    Result:=TRUE;
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_ClearLayer,0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[STS]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_DBAnnotOpts,0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[SDL]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_SuppressDlg,0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[GBE]',5) = 0 then begin
    Data^.SetCursor(crHourGlass);
    Result:=TRUE;
    DDECommand:=GBE;
    DBGraphic^.InsertString(SendedDDEString);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[DDD]',5) = 0 then begin   
    Data^.SetCursor(crHourGlass);
    Result:=TRUE;
    DDECommand:=DDD;
    DBGraphic^.InsertString(SendedDDEString);           
    Data^.ClearStatusText;
    Data^.SetStatusText(Format(GetLangText(17100),[DBGraphic^.LineColl^.Count]),True);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[CLPNT]',7) = 0 then begin
    Result:=TRUE;
    Data^.IntList1.Add(StrToIntDef(GetDDEItem(SendedDDEString,2),-1));
    Data^.IntList2.Add(StrToIntDef(GetDDEItem(SendedDDEString,3),-1));
    DDECommand:=CLPNT;
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[GCP]',5) = 0 then begin
    Result:=TRUE;
    Data^.IntList1.Add(StrToIntDef(GetDDEItem(SendedDDEString,2),-1));
    DDECommand:=GCP;
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[FTXT]',6) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    Data^.CmdList.Add(SendedDDEString);
    DDEHandler.EnableDDEReception(TRUE);
    DDECommand:=FTXT;
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[DGSIZ]',7) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    DBGraphic^.DiagSize:=StrToInt(GetDDEItem(SendedDDEString,2));
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[DGTYP]',7) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    DBGraphic^.DefaultType:=Min(Max(StrToInt(GetDDEItem(SendedDDEString,2)),0),4);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[DGLAB]',7) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    DBGraphic^.DefMarkVis:=StrToInt(GetDDEItem(SendedDDEString,2))<>0;
    DBGraphic^.DefMarkType:=Min(Max(StrToInt(GetDDEItem(SendedDDEString,3)),0),4);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[DGFON]',7) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    DBGraphic^.DefMarkFont:=GetDDEItem(SendedDDEString,2);
    DBGraphic^.DefMarkSize:=StrToIntDef(GetDDEItem(SendedDDEString,3),8);
    LastDDEEnd:=FALSE;
  end                                             
  else if StrLComp(SendedDDEString,'[DGTRD]',7) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    DBGraphic^.Def3D:=StrToInt(GetDDEItem(SendedDDEString,2))<>0;
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[SETGT]',7) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    ProcDBSetGTState(Data,GetDDEItem(SendedDDEString,2),(StrToInt(GetDDEItem(SendedDDEString,3))<>0));
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[SSL]',5) = 0 then begin
    Result:=TRUE;
    ProcSetDBSelLayerFromDB(Data,SendedDDEString);
  end
  else if StrLComp(SendedDDEString,'[SAL]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.DeleteDDEData;
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_SetTranspMode,0);
  end
  else if StrLComp(SendedDDEString,'[TAL]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    SendMessage(Handle,wm_Command,cm_GetTranspMode,0);
  end
  else if StrLComp(SendedDDEString,'[TOP]',5) = 0 then begin
    Data^.SetCursor(crHourGlass);
    Result:=TRUE;
    if DDECommand = 0 then DDEHandler.DeleteDDEData;
    DDECommand:=TOP;
    DDEHandler.InsertDDEData(SendedDDEString);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[ZOM]',5) = 0 then begin
    Data^.SetCursor(crHourGlass);
    Result:=TRUE;
    if DDECommand = 0 then DDEHandler.DeleteDDEData;
    DDECommand:=ZOM;
    DDEHandler.InsertDDEData(SendedDDEString);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[FIT]',5) = 0 then begin
    Data^.SetCursor(crHourGlass);
    Result:=TRUE;
    if DDECommand = 0 then DDEHandler.DeleteDDEData;
    DDECommand:=FIT;
    DDEHandler.InsertDDEData(SendedDDEString);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[ZOD]',5) = 0 then begin
    Data^.SetCursor(crHourGlass);
    Result:=TRUE;
    if DDECommand = 0 then DDEHandler.DeleteDDEData;
    DDECommand:=ZOD;
    DDEHandler.InsertDDEData(SendedDDEString);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[FID]',5) = 0 then begin
    Data^.SetCursor(crHourGlass);
    Result:=TRUE;
    if DDECommand = 0 then DDEHandler.DeleteDDEData;
    DDECommand:=FID;
    DDEHandler.InsertDDEData(SendedDDEString);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[SEL]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.DeleteDDEData;
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_SelAndNBS,0);
  end
  else if StrLComp(SendedDDEString,'[IOS]',5) = 0 then begin
    Result:=TRUE;
    if DDECommand = 0 then DDEHandler.DeleteDDEData;
    DDECommand:=IOS;
    DDEHandler.InsertDDEData(SendedDDEString);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[SPP]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_SetProjPart,0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[SAO]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    SendMessage(Handle,wm_Command,cm_ShowAllObjs,0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[TPP]',5) = 0 then begin
    Result:=TRUE;
    //DDEHandler.EnableDDEReception(FALSE);
    SendMessage(Handle,wm_Command,cm_SendProjPart,0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[DPO]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_PolyOverlay,0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[DPP]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_PolyPart,0);
    LastDDEEnd:=FALSE;
  end
{++ Ivanoff 4 Bolt}
  else if StrLComp(SendedDDEString,'[GPD]',5) = 0 then begin
    GetMem(pcTemp, 16);
    try
      ReadDDEText(SendedDDEString, DDEHandler.FDDESepSign, pcTemp);
      ReadDDEText(SendedDDEString, DDEHandler.FDDESepSign, pcTemp);
      sTemp := pcTemp;
    finally
      FreeMem(pcTemp);
    end;
    TRY
       ilTemp := StrToInt(sTemp);
    EXCEPT
       LastDDEEnd:=TRUE;
       EXIT;
    END;
    SendMessage(SELF.Handle, wm_Command, cm_DBGetPolygonData, ilTemp);
    LastDDEEnd:=TRUE;
    RESULT := TRUE;
    end
{-- Ivanoff 4 Bolt}
  else if StrLComp(SendedDDEString,'[DBE]',5) = 0 then begin
    Result:=TRUE;
    ImpExport^.InsertDDEData(SendedDDEString)
  end
  else if StrLComp(SendedDDEString,'[GON]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    SendMessage(Handle,wm_Command,cm_GetNextObjIDs,0);
  end
  else if StrLComp(SendedDDEString,'[DGR]',5) = 0 then begin
    Data^.SetCursor(crHourGlass);
    Result:=TRUE;
    if DDECommand = 0 then DDEHandler.DeleteDDEData;
    DDECommand:=DGR;
    DDEHandler.InsertDDEData(SendedDDEString);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[CPY]',5) = 0 then begin
    Data^.SetCursor(crHourGlass);
    Result:=TRUE;
    if DDECommand = 0 then DDEHandler.DeleteDDEData;
    DDECommand:=CPY;
    DDEHandler.InsertDDEData(SendedDDEString);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[COP]',5) = 0 then begin
    Data^.SetCursor(crHourGlass);
    Result:=TRUE;
    if DDECommand = 0 then DDEHandler.DeleteDDEData;
    DDECommand:=COP;
    DDEHandler.InsertDDEData(SendedDDEString);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[LBM]',5) = 0 then begin
    Data^.SetCursor(crHourGlass);
    Result:=TRUE;
    if DDECommand = 0 then DDEHandler.DeleteDDEData;
    DDECommand:=LBM;
    DDEHandler.InsertDDEData(SendedDDEString);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[MOV]',5) = 0 then begin
    Data^.SetCursor(crHourGlass);
    Result:=TRUE;
    if DDECommand = 0 then DDEHandler.DeleteDDEData;
    DDECommand:=MOV;
    DDEHandler.InsertDDEData(SendedDDEString);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[MOP]',5) = 0 then begin
    Data^.SetCursor(crHourGlass);
    Result:=TRUE;
    if DDECommand = 0 then DDEHandler.DeleteDDEData;
    DDECommand:=MOP;
    DDEHandler.InsertDDEData(SendedDDEString);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[SHV]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.DeleteDDEData;
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_DBShowView,0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[RCS]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    if Data^.PInfo^.CoordinateSystem.CoordinateType = ctCarthesian then
        DDEHandler.SendString('[SCS][1]')
    else
        DDEHandler.SendString('[SCS][0]');
    LastDDEEnd:=FALSE;
  end
//++ Glukhov Bug#203 Build#151 08.02.01
  else if StrLComp(SendedDDEString,'[RMC]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    DDEHandler.EnableDDEReception(FALSE);
    SendMessage(Handle,wm_Command,cm_GetCoordinates,0);
    LastDDEEnd:=FALSE;
  end
//-- Glukhov Bug#203 Build#151 08.02.01
  else if StrLComp(SendedDDEString,'[RPS]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    s:='[SPS][' + UnitsToName(Data^.PInfo^.ProjectUnits) + ']' +
    '[' + FloatToStr(Data^.PInfo^.ProjectScale) + ']' +
    '[' + Data^.PInfo^.ProjectionSettings.ProjectProjection + ']' +
    '[' + Data^.PInfo^.ProjectionSettings.ProjectDate + ']';
    DDEHandler.SendString(s);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[CSO]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    AreaCutSelected(Data,GetDDEItem(SendedDDEString,2),StrToIntDef(GetDDEItem(SendedDDEString,3),0));
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[CIS]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    Data^.ChangeImagePropertiesFromDB(
        StrToInt(GetDDEItem(SendedDDEString,2)),
        StrToInt(GetDDEItem(SendedDDEString,3)),
        StrToInt(GetDDEItem(SendedDDEString,4)),
        StrToInt(GetDDEItem(SendedDDEString,5))
        );
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[PPL]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    ProcPolyPartWithLine(Data,
                         GetDDEItem(AnsiString(SendedDDEString),2),
                         StrToInt(GetDDEItem(AnsiString(SendedDDEString),3)),
                         StrToInt(GetDDEItem(AnsiString(SendedDDEString),4)),False);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[ATT]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    ProcAttachTo(Data,StrToInt(GetDDEItem(AnsiString(SendedDDEString),2)),StrToInt(GetDDEItem(AnsiString(SendedDDEString),3)));
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[GATT]',6) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    ProcSendAttObj(Data,StrToInt(GetDDEItem(AnsiString(SendedDDEString),2)));
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[DDETOFILE]',9) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    ProcSetDDEToFileMode(StrToInt(GetDDEItem(AnsiString(SendedDDEString),2)) <> 0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[SGTDB]',7) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    ProcSetGTDB(Data,GetDDEItem(AnsiString(SendedDDEString),2),
                     GetDDEItem(AnsiString(SendedDDEString),3),
                     GetDDEItem(AnsiString(SendedDDEString),4),
                     GetDDEItem(AnsiString(SendedDDEString),5));
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[MESDAT]',8) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    ProcSetMeasData(Data,
                    StrToInt(GetDDEItem(AnsiString(SendedDDEString),2)),
                    StrToInt(GetDDEItem(AnsiString(SendedDDEString),3)),
                    (StrToInt(GetDDEItem(AnsiString(SendedDDEString),4)) <> 0),
                    StrToInt(GetDDEItem(AnsiString(SendedDDEString),5)));


    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[GGTDB]',7) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    ProcGetGTDB(Data,GetDDEItem(AnsiString(SendedDDEString),2));
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[EMF]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    ExecuteMenuFunction(GetDDEItem(AnsiString(SendedDDEString),2));
    DDEHandler.EnableDDEReception(TRUE);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[SHSP]',6) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DBFileSaveHSP(AnsiString(SendedDDEString));
    DDEHandler.EnableDDEReception(TRUE);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[TRIM]',6) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    ProcDBTrimm(Data,
                StrToIntDef(GetDDEItem(AnsiString(SendedDDEString),2),0),
                StrToIntDef(GetDDEItem(AnsiString(SendedDDEString),3),0));

    DDEHandler.EnableDDEReception(TRUE);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[DHPP]',6) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    ProcDeleteHiddenPolyPoints(Data,GetDDEItem(AnsiString(SendedDDEString),2));
    DDEHandler.EnableDDEReception(TRUE);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[SLM]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    ProcExportLayerManager(Data);
    DDEHandler.EnableDDEReception(TRUE);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[SXP]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    ProcSelCrossPoly(Data,GetDDEItem(AnsiString(SendedDDEString),2));
    DDEHandler.EnableDDEReception(TRUE);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[COMBP]',7) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    CombinePolygonsFromDB(SendedDDEString);
    DDEHandler.EnableDDEReception(TRUE);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[COPLAY]',8) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    ProcCopyLayerByName(Data,GetDDEItem(AnsiString(SendedDDEString),2),GetDDEItem(AnsiString(SendedDDEString),3));
    DDEHandler.EnableDDEReception(TRUE);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[PTP]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DBExplodePolys(GetDDEItem(AnsiString(SendedDDEString),2),GetDDEItem(AnsiString(SendedDDEString),3));
    DDEHandler.EnableDDEReception(TRUE);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[SLP]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    ProcSetLayerPrintable(Data,GetDDEItem(AnsiString(SendedDDEString),2),(StrToInt(GetDDEItem(AnsiString(SendedDDEString),3))<>0));
    DDEHandler.EnableDDEReception(TRUE);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[SOTF]',6) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    ProcSetObjTypeFilter(Data,StrToIntDef(GetDDEItem(AnsiString(SendedDDEString),2),0));
    DDEHandler.EnableDDEReception(TRUE);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[FEO]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    ProcFindObjectOnLayer(Data,GetDDEItem(AnsiString(SendedDDEString),2),StrToIntDef(GetDDEItem(AnsiString(SendedDDEString),3),0));
    DDEHandler.EnableDDEReception(TRUE);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[SHWLD]',7) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    Data^.SetLayerPriority(SendedDDEString);
    DDEHandler.EnableDDEReception(TRUE);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[SETMZ]',7) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    bMouseZoom:=(StrToInt(GetDDEItem(SendedDDEString,2))<>0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[PRNMRK]',8) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    Data^.FramePrintSpec.UseSpecFrame:=(StrToInt(GetDDEItem(SendedDDEString,2))<>0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[D400]',6) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    Data^.Deg400Mode:=(StrToInt(GetDDEItem(SendedDDEString,2)) <> 0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[SETAD]',7) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    IniFile^.AreaFact:=StrToInt(GetDDEItem(SendedDDEString,2));
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[SETDC]',7) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    DBGraphic^.SetItemColor(StrToInt(GetDDEItem(SendedDDEString,2)),GetDDEItem(SendedDDEString,3));
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[CTM]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;

    Res:=ProcCreateThematicMap(GetDDEItem(SendedDDEString,2),
                    GetDDEItem(SendedDDEString,3),
                    GetDDEItem(SendedDDEString,4),
                    GetDDEItem(SendedDDEString,5),
                    StrToInt(GetDDEItem(SendedDDEString,6)),
                    StrToInt(GetDDEItem(SendedDDEString,7)),
                    GetDDEItem(SendedDDEString,8),
                    GetDDEItem(SendedDDEString,9),
                    GetDDEItem(SendedDDEString,10),
                    (StrToInt(GetDDEItem(SendedDDEString,11)) <> 0));
    DDEHandler.SendString('[TMS][' + IntToStr(Res) + ']');
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[PPC]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    ProcDBPolyPointCut(Data,
                       GetDDEItem(SendedDDEString,2),
                       GetDDEItem(SendedDDEString,3),
                       GetDDEItem(SendedDDEString,4),
                       GetDDEItem(SendedDDEString,5));
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[AUTSNP]',8) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    ProcAutoSnapFromDB(Data,StrToIntDef(GetDDEItem(SendedDDEString,2),1),GetDDEItem(SendedDDEString,3),GetDDEItem(SendedDDEString,4));
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[PROREP]',8) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    ProcExecProjRep(Data);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[CPC]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    ProcPolyCircleCut(Data,
                       StrToInt(GetDDEItem(SendedDDEString,2)),
                       StrToInt(GetDDEItem(SendedDDEString,3)),
                       StrToInt(GetDDEItem(SendedDDEString,4)),
                       StrToInt(GetDDEItem(SendedDDEString,5)));
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[GETUID]',8) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    DDEHandler.SendString('[PGUID][' + Data^.ProjGuid + ']');
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[SETUID]',8) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    Data^.ProjGuid:=GetDDEItem(SendedDDEString,2);
    Data^.SetModified;
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[IMPTXT]',8) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    if ExtrasImportCoordinates(GetDDEItem(SendedDDEString,2),
                               GetDDEItem(SendedDDEString,3),
                               StrToIntDef(GetDDEItem(SendedDDEString,4),-1),
                               StrToIntDef(GetDDEItem(SendedDDEString,5),0),
                               GetDDEItem(SendedDDEString,6),
                               GetDDEItem(SendedDDEString,7)) then begin
       DDEHandler.SendString('[IMPEND][]');
    end;
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[EXPSYM]',8) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    ProcConvertSymToObj(Data,StrToIntDef(GetDDEItem(SendedDDEString,2),-1),Data^.Layers^.NameToLayer(GetDDEItem(SendedDDEString,3)));
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[SPV]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    Data^.PInfo^.ExtCanvas.DrawVertices := (StrToIntDef(GetDDEItem(SendedDDEString,2),0) <> 0);
    Data^.PInfo^.ExtCanvas.VerticesColor := StrToIntDef(GetDDEItem(SendedDDEString,3),0);
    Data^.PInfo^.RedrawScreen(False);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[EXPLODE]',9) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    ProcExplodeToLayer(Data,GetDDEItem(SendedDDEString,2));
    LastDDEEnd:=FALSE;                                                       
  end
  else if StrLComp(SendedDDEString,'[SVI]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    ProcSaveViewAsImage(Data,GetDDEItem(SendedDDEString,2));
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[CVW]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    Data^.DBCreateView(GetDDEItem(SendedDDEString,2),
                       StrToInt(GetDDEItem(SendedDDEString,3)),
                       StrToInt(GetDDEItem(SendedDDEString,4)),
                       StrToInt(GetDDEItem(SendedDDEString,5)));

    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[DVW]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    Data^.DBDeleteView(GetDDEItem(SendedDDEString,2));
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[GPA]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    ProcDBGetPolyArea(Data,StrToInt(GetDDEItem(SendedDDEString,2)));
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[GLPA]',6) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    ProcGetLayerPolyArea(Data,GetDDEItem(SendedDDEString,2));
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[DLL]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    Data^.DeleteLayerListFromDB(AnsiString(SendedDDEString));
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[LSNAP]',7) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    Data^.SetLayerSnap(GetDDEItem(SendedDDEString,2),StrToInt(GetDDEItem(SendedDDEString,3)));
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[FSNAP]',7) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    Data^.AllowOnlySnappedPoints:=(StrToIntDef(GetDDEItem(SendedDDEString,2),0) > 0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[SMR]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    Data^.CoordMsgTarget:=StrToIntDef(GetDDEItem(SendedDDEString,2),0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[NLEG]',6) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    ProcAddLegendEntry(Data,GetDDEItem(SendedDDEString,2),GetDDEItem(SendedDDEString,3));
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[SMI]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    IniFile^.SendMouseInfo:=(StrToIntDef(GetDDEItem(SendedDDEString,2),1) <> 0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[DLEG]',6) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    ProcDelLegendEntry(Data,GetDDEItem(SendedDDEString,2),GetDDEItem(SendedDDEString,3));
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[SSR]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    Data^.PInfo^.VariousSettings.SnapRadius:=StrToIntDef(GetDDEItem(SendedDDEString,2),Trunc(Data^.PInfo^.VariousSettings.SnapRadius));
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[SMC]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    Data^.MouseActionFromDB(Trunc(StrToFloat(GetDDEItem(AnsiString(SendedDDEString),2))*100),
                            Trunc(StrToFloat(GetDDEItem(AnsiString(SendedDDEString),3))*100),
                            StrToInt(GetDDEItem(AnsiString(SendedDDEString),4)));
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[UNDO]',6) = 0 then begin
    Result:=TRUE;
    s:=SendedDDESTring;
    DDEHandler.DeleteDDEData;
    {w:=Integer(Char('Z'));
    Data^.MsgKeyDown(w,[ssCtrl]);
    Data^.MsgKeyUp(w,[ssCtrl]); }
    if length(s)> 7 then   //WAI (July 17th 2001)
       begin
       s:=copy(s,8,4);
       s:=stringreplace(s,'[','',[rfReplaceAll]);
       s:=stringreplace(s,']','',[rfReplaceAll]);
       c:=strtointdef(s,1);
       end  else c:=1;
    for i:= 1 to c do  ExecuteMenuFunction('IDB_UndoFunction');
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[REDO]',6) = 0 then begin    //by WAIDACHER (July 17th 2001)
    Result:=TRUE;
    s:=SendedDDESTring;
    DDEHandler.DeleteDDEData;
    if length(s)> 7 then   //WAI (July 17th 2001)
       begin
       s:=copy(s,8,4);
       s:=stringreplace(s,'[','',[rfReplaceAll]);
       s:=stringreplace(s,']','',[rfReplaceAll]);
       c:=strtointdef(s,1);
       end  else c:=1;
    for i:= 1 to c do  ExecuteMenuFunction('IDB_RedoFunction');
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[SSD]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    ProcSelectSymbolWithDlg(Data);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[SFT]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.InsertDDEData(SendedDDEString);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[CCD]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    Data^.CommitCurrentDigitalization;
    LastDDEEnd:=FALSE;
  end
   else if StrLComp(SendedDDEString,'[CSY]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    Data^.DBInsertSymbol(GetDDEItem(AnsiString(SendedDDEString),2),
                         GetDDEItem(AnsiString(SendedDDEString),3),
                         StrToFloat(GetDDEItem(AnsiString(SendedDDEString),4)),
                         StrToFloat(GetDDEItem(AnsiString(SendedDDEString),5)),
                         DegToRad(StrToFloat(GetDDEItem(AnsiString(SendedDDEString),6))),
                         StrToFloat(GetDDEItem(AnsiString(SendedDDEString),7)),
                         StrToIntDef(GetDDEItem(AnsiString(SendedDDEString),8),0));
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[COMBL]',7) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    DBPolyCombine(SendedDDEString);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[CISL]',6) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    CreateIslandArea(GetDDEItem(SendedDDEString,2));
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[GOB]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    DBGenerateObjects(AnsiString(SendedDDEString));
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[SOL]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    DBShowAllObjectsOnLayer(GetDDEItem(AnsiString(SendedDDEString),2));
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[LLN]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    DBCutLineByLength(StrToInt(GetDDEItem(SendedDDEString,2)),
                      StrToFloat(GetDDEItem(SendedDDEString,3)),
                      StrToInt(GetDDEItem(SendedDDEString,4)));
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[IDB]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    Res:=StrToIntDef(GetDDEItem(SendedDDEString,2),0);
    ProcSetIDBOnOff(Data,Res <> 0);
    LastDDEEnd:=FALSE;                               
  end                                        
  else if StrLComp(SendedDDEString,'[SGTEXT]',8) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    Data^.DBSetGeoText(StrToInt(GetDDEItem(SendedDDEString,2)),GetDDEItem(SendedDDEString,3));
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[GGTEXT]',8) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    Data^.DBGetGeoText(StrToInt(GetDDEItem(SendedDDEString,2)));
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[CBU]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    Data^.DBCreateBuffer(StrToInt(GetDDEItem(SendedDDEString,2)),
                         GetDDEItem(SendedDDEString,3),
                         StrToInt(GetDDEItem(SendedDDEString,4)),
                         StrToInt(GetDDEItem(SendedDDEString,5)),
                         Round(StrToFloat(GetDDEItem(SendedDDEString,6))*100));
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[MOL]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    DBMoveToLayer(AnsiString(SendedDDEString));
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[SNO]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.DeleteDDEData;
    DDESnapObject(SendedDDEString);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[LVR]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.DeleteDDEData;
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_DBViewInfos,0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[SRQ]',5) = 0 then begin
    Data^.SetCursor(crHourGlass);
    Result:=TRUE;
    DDECommand:=SRQ;
    Data^.CreateSRQDatas(SendedDDEString);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[RSY]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    SendMessage(Handle,wm_Command,cm_RequestSyms,0);
  end
  else if StrLComp(SendedDDEString,'[SAS]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.DeleteDDEData;
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_DBSetActSym,0);
  end
  else if StrLComp(SendedDDEString,'[RLI]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    SendMessage(Handle,wm_Command,cm_RequLStyles,0);
  end
  else if StrLComp(SendedDDEString,'[RHA]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    SendMessage(Handle,wm_Command,cm_RequHStyles,0);
  end
  else if StrLComp(SendedDDEString,'[DSA]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    SendMessage(Handle,wm_Command,cm_DBDeselAll,0);
  end
  else if StrLComp(SendedDDEString,'[LAA]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    SendMessage(Handle,wm_Command,cm_DBAddLayer,0);
  end
  else if StrLComp(SendedDDEString,'[LAD]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    SendMessage(Handle,wm_Command,cm_DBDelLayer,0);
  end
  else if StrLComp(SendedDDEString,'[SZF]',5) = 0 then begin
    Result:=TRUE;
    ProcSetZoomFactFromDB(Data,SendedDDEString);
  end
  else if StrLComp(SendedDDEString,'[OES]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_DBSetOptions,0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[OBM]',5) = 0 then begin
    Result:=TRUE;
    ProcSetBitmapOptionsFromDB(Data,SendedDDEString);
  end
  else if StrLComp(SendedDDEString,'[DK1]',5) = 0 then begin
    Data^.SetCursor(crHourGlass);
    Result:=TRUE;
    DDECommand:=DK1;
    MyList.Add(StrPas(SendedDDEString));
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[NRD]',5) = 0 then begin
    Result:=TRUE;
    WinGISMainForm.SetRedraw:=FALSE;
    Data^.ChangeRedraw(CNoRedraw);
  end
  else if StrLComp(SendedDDEString,'[RDR]',5) = 0 then begin
    Result:=TRUE;
    WinGISMainForm.SetRedraw:=TRUE;
    Data^.ChangeRedraw(CDoRedraw);
    HadNoRedraw:=FALSE;
    Data^.PInfo^.RedrawAfterNoRedraw(RDRArea);
  end
  else if StrLComp(SendedDDEString,'[PRD]',5) = 0 then begin
    Result:=TRUE;
    SendMessage(Handle,wm_Command,cm_DBProjRedraw,0);
  end
  else if StrLComp(SendedDDEString,'[SLS]',5) = 0 then begin
  end
  else if StrLComp(SendedDDEString,'[SGP]',5) = 0 then begin
    Result:=True;
    SetGraphicParent(StrToInt(GetDDEItem(SendedDDEString,2)));
  end
  else if StrLComp(SendedDDEString,'[CCO]',5) = 0 then begin
    Result:=TRUE;
    SendMessage(WinGISMainForm.Handle,wm_Command,cm_ChkConnection,0);
  end
  else if StrLComp(SendedDDEString,'[CHT]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    SendMessage(Handle,wm_Command,cm_DBChangedTable,0);
  end
  else if StrLComp(SendedDDEString,'[CLO]',5) = 0 then begin
    Result:=TRUE;
    SendMessage(WinGISMainForm.Handle,wm_Command,cm_ExitFromDB,0);
  end
  else if StrLComp(SendedDDEString,'[OPR]',5) = 0 then begin
    Result:=TRUE;
    StrCopy(WinGISMainForm.MWDDELine1,SendedDDEString);
    SendMessage(WinGISMainForm.Handle,wm_Command,cm_OpenProjects,0);
  end
  else if StrLComp(SendedDDEString,'[WPR]',5) = 0 then begin
    Result:=TRUE;
    StrCopy(WinGISMainForm.MWDDELine1,SendedDDEString);
    SendMessage(WinGISMainForm.Handle,wm_Command,cm_ChangeProject,0);
  end
  else if StrLComp(SendedDDEString,'[CPR]',5) = 0 then begin
    Result:=TRUE;
    StrCopy(WinGISMainForm.MWDDELine1,SendedDDEString);
    SendMessage(WinGISMainForm.Handle,wm_Command,cm_CloseProject,0);
  end
  else if StrLComp(SendedDDEString,'[SGM]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.DeleteDDEData;
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_DBSetGraMode,0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[LOA]',5) = 0 then begin
    Result:=TRUE;
    SendMessage(WinGISMainForm.Handle,wm_Command,cm_OpActProjs,0);
  end
//++ Glukhov Bug#84 BUILD#125 02.11.00
  else if StrLComp(SendedDDEString,'[RPL]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.DeleteDDEData;
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_DBReversePolyLine,0);
    LastDDEEnd:=FALSE;
  end
//-- Glukhov Bug#84 BUILD#125 02.11.00
//++ Glukhov Bug#100 Build#152 05.03.01
  else if StrLComp(SendedDDEString,'[SPI]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.DeleteDDEData;
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_SetPicture,0);
    LastDDEEnd:=FALSE;
  end
//-- Glukhov Bug#100 Build#152 05.03.01
  else if StrLComp(SendedDDEString,'[PRN]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.DeleteDDEData;
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_DBPrintGraph,0);
    LastDDEEnd:=FALSE;
  end
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 02.10.00}
  else if StrLComp(SendedDDEString,'[PRNSPP]',8) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.DeleteDDEData;
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_DBPrintSetPrnParam,0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[PRNSPG]',8) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.DeleteDDEData;
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_DBPrintSetPgsParam,0);
    LastDDEEnd:=FALSE;
  end
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 02.10.00}
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 09.10.00}
  else if StrLComp(SendedDDEString,'[PRNSM]',7) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.DeleteDDEData;
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_DBPrintSetMapParam,0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[PRNSL]',7) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.DeleteDDEData;
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_DBPrintSetLegParam,0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[PRNIL]',7) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.DeleteDDEData;
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_DBPrintInsLegend,0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[PRNGPP]',8) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.DeleteDDEData;
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_DBPrintGetPrnParam,0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[PRNGPG]',8) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.DeleteDDEData;
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_DBPrintGetPgsParam,0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[PRNGM]',7) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.DeleteDDEData;
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_DBPrintGetMapParam,0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[PRNGL]',7) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.DeleteDDEData;
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_DBPrintGetLegParam,0);
    LastDDEEnd:=FALSE;
  end
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 09.10.00}
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 23.10.00}
  else if StrLComp(SendedDDEString,'[PRNSP]',7) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.DeleteDDEData;
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_DBPrintSetPicParam,0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[PRNIP]',7) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.DeleteDDEData;
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_DBPrintInsPicture,0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[PRNGP]',7) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.DeleteDDEData;
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_DBPrintGetPicParam,0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[PRNSS]',7) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.DeleteDDEData;
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_DBPrintSetSigParam,0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[PRNIS]',7) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.DeleteDDEData;
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_DBPrintInsSignature,0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[PRNGS]',7) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.DeleteDDEData;
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_DBPrintGetSigParam,0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[PRNST]',7) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.DeleteDDEData;
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_DBPrintStoreTemplate,0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[PRNRT]',7) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.DeleteDDEData;
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_DBPrintLoadTemplate,0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[PRNGOI]',8) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.DeleteDDEData;
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_DBPrintGetObjInform,0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[PRNDO]',7) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.DeleteDDEData;
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_DBPrintDeleteObject,0);
    LastDDEEnd:=FALSE;
  end
  else if StrLComp(SendedDDEString,'[PRNMO]',7) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.DeleteDDEData;
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_DBPrintMoveObject,0);
    LastDDEEnd:=FALSE;
  end
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 03.11.00}
  else if StrLComp(SendedDDEString,'[AIM]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    ImpExport^.imDDELines^.Insert(StrNew(SendedDDEString));
    if ImpExport^.imInProgress=ipNone then begin
      SendMessage(Handle,wm_Command,cm_DBImASCII,0);
    end;
  end
  else if StrLComp(SendedDDEString,'[ERR]',5) = 0 then begin
    Result:=TRUE;
    DDEHandler.EnableDDEReception(FALSE);
    ImpExport^.imErrString:=StrNew(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_DBImpExpErr,0);
  end
  else if (StrLComp(SendedDDEString,'[GPSPOS',7) = 0) or
     (StrLComp(SendedDDEString,'[GPSOFFSET]',11) = 0) or
     (StrLComp(SendedDDEString,'[GPSSHOW]',9) = 0) or
     (StrLComp(SendedDDEString,'[SETGPSPRO]',11) = 0) or
     (StrLComp(SendedDDEString,'[LOADGRF]',9) = 0) or
     (StrLComp(SendedDDEString,'[GPSDBCOLUMN]',13) = 0) or
     (StrLComp(SendedDDEString,'[GPSPROASC]',11) = 0) or
     (StrLComp(SendedDDEString,'[GPSPROCALC]',12) = 0) or
     (StrLComp(SendedDDEString,'[GPSNEW',7) = 0) or
     (StrLComp(SendedDDEString,'[GPSDEL',7) = 0) or
     (StrLComp(SendedDDEString,'[GPSUPD',7) = 0) or
     (StrLComp(SendedDDEString,'[GPSDATAFILE]',13) = 0) then begin
    DDEHandler.EnableDDEReception(FALSE);
    DDEHandler.DeleteDDEData;
    DDEHandler.InsertDDEData(SendedDDEString);
    SendMessage(Handle,wm_Command,cm_DoGPSCommand,0);
  end
  else if (StrLComp(SendedDDEString,'[DOAMPEXP]',10) = 0) then begin
     Result:=TRUE;
     DDE_DoAmpExp; // init amp-export
  end
  else if (StrLComp(SendedDDEString,'[DOASCEXP]',10) = 0) then begin
     Result:=TRUE;
     DDE_DoAscExp; // init asc-export
  end
  else if (StrLComp(SendedDDEString,'[DODXFEXP]',10) = 0) then begin
     Result:=TRUE;
     DDE_DoDxfExp; // init dxf-export
  end
  else if (StrLComp(SendedDDEString,'[DOE00EXP]',10) = 0) then begin
     Result:=TRUE;
     DDE_DoE00Exp; // init E00-export
  end
  else if (StrLComp(SendedDDEString,'[DOMIFEXP]',10) = 0) then begin
     Result:=TRUE;
     DDE_DoMifExp; // init Mif-export
  end
  else if (StrLComp(SendedDDEString,'[DOSHPEXP]',10) = 0) then begin
     Result:=TRUE;
     DDE_DoShpExp; // init Shp-export
  end
  else if (StrLComp(SendedDDEString,'[DOEXPSETTINGS]',15) = 0) then begin
     Result:=TRUE;
     DDE_DoExpSettings(strpas(SendedDDEString)); // do settings for export
  end
  else if (StrLComp(SendedDDEString,'[DOADDEXPLAYER]',15) = 0) then begin
     Result:=TRUE;
     DDE_DoAddExpLayer(strpas(SendedDDEString)); // add layer to export
  end
  else if (StrLComp(SendedDDEString,'[DOSETDBSETTINGS]',17) =0) then begin
     Result:=TRUE;
     DDE_DoSetDBSettings(strpas(SendedDDEString)); // set database settings
  end
  else if (StrLComp(SendedDDEString,'[DOSETPROJECTION]',17) =0) then begin
     Result:=TRUE;
     DDE_DoSetProjection(strpas(SendedDDEString)); // set projection for import/export
  end
  else if (StrLComp(SendedDDEString,'[DOAMPIMP]',10) = 0) then begin
     Result:=TRUE;
     DDE_DoAmpImp; // init amp-import
  end
  else if (StrLComp(SendedDDEString,'[DOAMPIMPSETTINGS]',18) = 0) then begin
     Result:=TRUE;
     DDE_DoAmpImpSettings(strpas(SendedDDEString)); // set amp-import settings
  end
  else if (StrLComp(SendedDDEString,'[DOADDIMPLAYER]',15) = 0) then begin
     Result:=TRUE;
     DDE_DoAddImpLayer(strpas(SendedDDEString)); // add layer that should be imported
  end
  else if (StrLComp(SendedDDEString,'[DOASCIMP]',10) = 0) then begin
     Result:=TRUE;
     DDE_DoAscImp; // init asc-import
  end
  else if (StrLComp(SendedDDEString,'[DOASCIMPSETTINGS]',18) = 0) then begin
     Result:=TRUE;
     DDE_DoAscImpSettings(strpas(SendedDDEString)); // set asc-import settings
  end
  else if (StrLComp(SendedDDEString,'[DOADDIMPFILE]',14) = 0) then begin
     Result:=TRUE;
     DDE_DoAddImpFile(strpas(SendedDDEString)); // add file that should be imported
  end
  else if (StrLComp(SendedDDEString,'[DODGNIMP]',10) = 0) then begin
     Result:=TRUE;
     DDE_DoDgnImp; // init dgn-import
  end
  else if (StrLComp(SendedDDEString,'[DODXFIMP]',10) = 0) then begin
     Result:=TRUE;
     DDE_DoDxfImp; // init dxf-import
  end
  else if (StrLComp(SendedDDEString,'[DODXFIMPSETTINGS]',18) = 0) then begin
     Result:=TRUE;
     DDE_DoDxfImpSettings(strpas(SendedDDEString)); // do the settings for dxf-import
  end
  else if (StrLComp(SendedDDEString,'[DOE00IMP]',10) = 0) then begin
     Result:=TRUE;
     DDE_DoE00Imp; // init E00-import
  end
  else if (StrLComp(SendedDDEString,'[DOE00IMPSETTINGS]',18) = 0) then begin
     Result:=TRUE;
     DDE_DoE00ImpSettings(strpas(SendedDDEString)); // do the settings for E00-import
  end
  else if (StrLComp(SendedDDEString,'[DOMIFIMP]',10) = 0) then begin
     Result:=TRUE;
     DDE_DoMifImp; // init Mif-import
  end
  else if (StrLComp(SendedDDEString,'[DOMIFIMPSETTINGS]',18) = 0) then begin
     Result:=TRUE;
     DDE_DoMifImpSettings(strpas(SendedDDEString)); // do the settings for Mif-import
  end
  else if (StrLComp(SendedDDEString,'[DOSHPIMP]',10) = 0) then begin
     Result:=TRUE;
     DDE_DoShpImp; // init Shp-import
  end
  else if (StrLComp(SendedDDEString,'[DOSHPIMPSETTINGS]',18) = 0) then begin
     Result:=TRUE;
     DDE_DoShpImpSettings(strpas(SendedDDEString)); // do the settings for Shp-import
  end
  else if StrLComp(SendedDDEString,'[GETREFPTS]',11) = 0 then begin
    Data^.SetActualMenu(mn_RefBitmap);
  end
  else if StrLComp(SendedDDEString,'[LDTRFILES(',11) = 0 then begin
    DDEHandler.EnableDDEReception(FALSE);
    Pos:=StrScan(SendedDDEString,'(');
    Inc(Pos);
    Pos2:=StrScan(Pos,')');
    if (Pos2 = NIL) then Exit;
    Pos2^:=#0;
    StrCopy(@LDFName[0],Pos);
    StrCopy(TRFString,@LDFName[0]);
    SendMessage(Handle,wm_Command,cm_LoadFilesATR,0);
  end;
end;
  {$ENDIF}
{$ENDIF} // <----------------- AXDLL

Procedure TMDIChild.WndProc(var Message:TMessage);
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  with Message do case Msg of
    {$IFNDEF WMLT}
    wm_SetFocus    : begin
      ThreeDActiv:=FALSE;
      WinGISMainForm.ThreeDActive:=FALSE;
      UserInterface.F3DActive:=FALSE;
      inherited WndProc(Message);
    end;
    wm_ICoord      : Data^.MsgCoordInput(Pointer(lParam));
    {$ENDIF}
    wm_ShowWindow  : begin
      if wParam=1 then begin
        {
        Data^.SetupProject(Self,FName);
        if Data^.NotUsedFonts then StoreData;
        Data^.NotUsedFonts:=FALSE;
        }
      end;
      inherited WndProc(Message);
    end;
    wm_Digitize      : Data^.MsgDigitize(Pointer(lParam));
    wm_MouseWheel    : Data^.MsgMouseWheel(Message);
    wm_Close: begin
      if Data^.SymbolMode = sym_SymbolMode then
      { So this window is the Symbol editor window and can be closed only with special button! }
        with Data^.SymEditInfo do CanCloseSymEditor
      else inherited WndProc(Message);
    end;
{++ IDB_DLL Interaction}
{$IFNDEF WMLT}                                  
    WM_IDB_Message: Begin
         Case WParam Of
         IDB_DeleteObjectsAction:
              WinGISMainForm.IDB_Man.DeleteObjectsInProject(SELF.Data, PLayer(LParam));
         End; // Case end
    End;
{$ENDIF}
{-- IDB_DLL Interaction}
    else {if not InWMPaint then} begin
         inherited WndProc(Message);
    end;
  end;
{$ENDIF} // <----------------- AXDLL
end;

{$IFNDEF AXDLL} // <----------------- AXDLL
  {$IFNDEF WMLT}
Procedure TMDIChild.WMCommand(var Msg:TMessage);
 begin
   {DDEHandler.EnableDDEReception(FALSE);}
   with Msg do case WParam of
      cm_RedrawADDE   : begin
                          if DDEHadNoRdr and (not DDEHandler.InAction)
                             and (Data^.Construct <> nil) and (not Data^.Construct^.InAction) and (not DDEHandler.NWUMode) then begin
                            DDEHadNoRdr:=FALSE;
                            Data^.PInfo^.RedrawAfterNoRedraw(DDERDRArea);
                          end;
                        end;
      cm_DBDelete     : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcDBDelete(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_ClearLayer   : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcClearLayer(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_MoveFromDB   : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcMoveRelFromDB(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_MoveAbsFromDB: begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcMoveAbsFromDB(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBPrintGraph : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcPrintFromDB(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
//++ Glukhov Bug#84 BUILD#125 02.11.00
      cm_DBReversePolyLine:
        begin
          ProcReversePolyLine( Data );
          DDEHandler.EnableDDEReception(TRUE);
        end;
//-- Glukhov Bug#84 BUILD#125 02.11.00
//++ Glukhov Bug#203 Build#151 08.02.01
      cm_GetCoordinates:
        begin
          ProcGetCoordinates( Data );                                   
          DDEHandler.EnableDDEReception(TRUE);
        end;
//-- Glukhov Bug#203 Build#151 08.02.01
//++ Glukhov Bug#100 Build#152 05.03.01
      cm_SetPicture:
        begin
          ProcSetPicture( Data );
          DDEHandler.EnableDDEReception(TRUE);
        end;
//-- Glukhov Bug#100 Build#152 05.03.01
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 02.10.00}
      cm_DBPrintSetPrnParam :
                        begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcSetPrnParamFromDB(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBPrintSetPgsParam :
                        begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcSetPgsPrnParamFromDB(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 02.10.00}
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 09.10.00}
      cm_DBPrintSetMapParam :
                        begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcSetMapParamFromDB(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBPrintSetLegParam :
                        begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcSetLegParamFromDB(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBPrintInsLegend :
                        begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcInsLegendFromDB(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBPrintGetPrnParam :
                        begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcGetPrnParamFromDB(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBPrintGetPgsParam :
                        begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcGetPgsPrnParamFromDB(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBPrintGetMapParam :
                        begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcGetMapParamFromDB(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBPrintGetLegParam :
                        begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcGetLegParamFromDB(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 09.10.00}
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 23.10.00}
      cm_DBPrintSetPicParam :
                        begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcSetPicParamFromDB(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBPrintInsPicture :
                        begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcInsPictureFromDB(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBPrintGetPicParam :
                        begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcGetPicParamFromDB(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBPrintSetSigParam :
                        begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcSetSigParamFromDB(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBPrintInsSignature :
                        begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcInsSignatureFromDB(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBPrintGetSigParam :
                        begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcGetSigParamFromDB(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBPrintStoreTemplate :
                        begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcStoreTemplateFromDB(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBPrintLoadTemplate :
                        begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcLoadTemplateFromDB(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBPrintGetObjInform :
                        begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcGetObjInformFromDB(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBPrintDeleteObject :
                        begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcDeleteObjectFromDB(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBPrintMoveObject :
                        begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcMoveObjectFromDB(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 03.11.00}
      cm_DeselAll     : begin
                          DDEHandler.EnableDDEReception(FALSE);
                          Data^.DeSelectAll(TRUE);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBDeselAll   : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcDBDeSelectAll(Data,TRUE);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBProjRedraw : begin
                          Data^.PInfo^.RedrawScreen(TRUE);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBLoadNext   : begin
                          DDEHandler.EnableDDEReception(FALSE);
                          ProcDBLoadNext(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBRecharge   : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcDBRecharge(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBProjSaveAs : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcDBProjSaveAs(Data,Self);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBShowView   : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcSetSightFromDB(Data,Self);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_Save         : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          StoreData;
                          if DDEHandler.FSave <> 0 then begin
                            DDEHandler.SetDBSendStrings(DDECommPInfo,NIL);
                            DDEHandler.SendString('[PRS]['+StrPas(FName)+']');
                            DDEHandler.SetDBSendStrings(DDECommAll,NIL);
                          end;
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBObjInfos   : begin
                          DDEHandler.EnableDDEReception(FALSE);
                          ProcDBObjectInfos(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_MON0         : begin
                          DDEHandler.EnableDDEReception(FALSE);
                          ProcDBFreeProgableDDE(Data,mn_DBDDE0);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_MON1         : begin
                          DDEHandler.EnableDDEReception(FALSE);
                          ProcDBFreeProgableDDE(Data,mn_DBDDE1);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_MON2         : begin
                          DDEHandler.EnableDDEReception(FALSE);
                          ProcDBFreeProgableDDE(Data,mn_DBDDE2);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_MON3         : begin
                          DDEHandler.EnableDDEReception(FALSE);
                          ProcDBFreeProgableDDE(Data,mn_DBDDE3);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_MON4         : begin
                          DDEHandler.EnableDDEReception(FALSE);
                          ProcDBFreeProgableDDE(Data,mn_DBDDE4);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_MON5         : begin
                          DDEHandler.EnableDDEReception(FALSE);
                          ProcDBFreeProgableDDE(Data,mn_DBDDE5);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_MON6         : begin
                          DDEHandler.EnableDDEReception(FALSE);
                          ProcDBFreeProgableDDE(Data,mn_DBDDE6);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_MON7         : begin
                          DDEHandler.EnableDDEReception(FALSE);
                          ProcDBFreeProgableDDE(Data,mn_DBDDE7);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_MON8         : begin
                          DDEHandler.EnableDDEReception(FALSE);
                          ProcDBFreeProgableDDE(Data,mn_DBDDE8);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_MON9         : begin
                          DDEHandler.EnableDDEReception(FALSE);
                          ProcDBFreeProgableDDE(Data,mn_DBDDE9);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_MON10        : begin
                          DDEHandler.EnableDDEReception(FALSE);
                          ProcDBFreeProgableDDE(Data,mn_DBDDE10);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_MON11        : begin
                          DDEHandler.EnableDDEReception(FALSE);
                          ProcDBFreeProgableDDE(Data,mn_DBDDE11);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_MON12        : begin
                          DDEHandler.EnableDDEReception(FALSE);
                          ProcDBFreeProgableDDE(Data,mn_DBDDE12);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_MON13        : begin
                          DDEHandler.EnableDDEReception(FALSE);
                          ProcDBFreeProgableDDE(Data,mn_DBDDE13);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_MON14        : begin
                          DDEHandler.EnableDDEReception(FALSE);
                          ProcDBFreeProgableDDE(Data,mn_DBDDE14);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_MON15        : begin
                          DDEHandler.EnableDDEReception(FALSE);
                          ProcDBFreeProgableDDE(Data,mn_DBDDE15);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_MON16        : begin
                          DDEHandler.EnableDDEReception(FALSE);
                          ProcDBFreeProgableDDE(Data,mn_DBDDE16);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_MON17        : begin
                          DDEHandler.EnableDDEReception(FALSE);
                          ProcDBFreeProgableDDE(Data,mn_DBDDE17);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_MON18        : begin
                          DDEHandler.EnableDDEReception(FALSE);
                          ProcDBFreeProgableDDE(Data,mn_DBDDE18);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_MON19        : begin
                          DDEHandler.EnableDDEReception(FALSE);
                          ProcDBFreeProgableDDE(Data,mn_DBDDE19);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBImASCII    : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ImpExport^.ImportASCIIFromDB;
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBImpExpErr  : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ImpExport^.DBError;
                          DDEHandler.EnableDDEReception(TRUE);
                        end;                                            
      cm_LoadBitFromDB: begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcInsertBitmapsFromDB(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DoGPSCommand : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          GPS.GPS_Execute(Data,PChar(DDEHandler.DDEData^.At(0)));
                          if DDEHandler.DDEData^.GetCount > 0 then PChar(DDEHandler.DDEData^.At(0))[0]:=DDEStrUsed;
                          DDEHandler.DeleteDDEData;
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_LoadFilesATR : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcLoadTRFiles(Data,TRFString);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBAnnotOpts  : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcSetAnnotationOptsFromDB(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_SuppressDlg  : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcDBSuppressDlgAnnotChart(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_TextGraph    : begin
                          DDEHandler.EnableDDEReception(FALSE);
                          DBGraphic^.DBText(Data);
                          Data^.PInfo^.RedrawScreen(False);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_BusGraph     : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          DBGraphic^.DBGraphic(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_GetNextObjIDs: begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          DDEHandler.SetAnswerMode(TRUE);
                          ProcGetNextObjIDs(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_InsObjs      : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          DDEHandler.SetAnswerMode(TRUE);
                          ProcInsertObjectsFromDB(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
{!?!?!Route      cm_SetVLType:begin
       RouteMenu.SetVLType;
      end;}
      cm_SelModeDBEnd : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          Data^.SelNewObjectsDB:=FALSE;
                          ProcShowAllSel(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_SelModeGIS   : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcSetSelModeGIS(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBSelObjects : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcSelDBObjects(Data);
                          ProcSelDBObjects2(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBZoomObjects: begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcSelDBObjects(Data);
                          ProcSelDBObjects2(Data);
                          ProcShowAllSel(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBDSelObjects: begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          Data^.DeselectAll(FALSE);
                          ProcSelDBObjects(Data);
                          ProcSelDBObjects2(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBDZoomObjects:begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          Data^.DeselectAll(FALSE);
                          ProcSelDBObjects(Data);
                          ProcSelDBObjects2(Data);
                          ProcShowAllSel(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBSelectedObj: begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcSendSelectedObjectsToDB(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_SetProjPart  : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcZoomToProjPart(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_ShowAllObjs  : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcZoomToAllObjects(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_SendProjPart : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcSendProjectPart(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_PolyOverlay  : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcStartPolyOverlayFromDB(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_PolyPart     : begin
                          {DDEHandler.EnableDDEReception(FALSE);}             
                          ProcStartPolyPartitionFromDB(Data);
                          {DDEHandler.EnableDDEReception(TRUE);}
                        end;
      cm_SendPartData : begin
                          DDEHandler.EnableDDEReception(FALSE);
                          ProcSendNewPartPoliesToDB(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_ShowPartDlg  : ShowPartDlg(Data);
      cm_SendObjProps : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcSendObjPropertiesToDB(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_SelAndNBS    : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          if Data^.DBSelection^.Selecting then begin
                             Data^.DBSelection^.EndDBSelection(False,False);
                          end;
                          Data^.DBSelection^.StartDBSelection;
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBSelCircle  : begin
                          DDEHandler.EnableDDEReception(FALSE);
                          Data^.SetActualMenu(mn_DBSelCircle);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBSelRect    : begin
                          DDEHandler.EnableDDEReception(FALSE);
                          Data^.SetActualMenu(mn_DBSelRect);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_SymbolRequest: begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          DDEHandler.SetAnswerMode(TRUE);
                          ProcSelectSymbolForDGR(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_RequestSyms  : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcDBRequestSymbols(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBSetActSym  : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcDBSetActSymbol(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_RequLStyles  : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcDBRequestLineStyles(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_RequHStyles  : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcDBRequestHatchStyles(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBAddLayer   : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcDBAddLayer(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBAddLayerNDlg:begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcDBAddLayerWithoutDlg(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBChaLayerNDlg:begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcDBChangeLayerWithoutDlg(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBDelLayer   : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcDBDelLayer(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBSFGLayers  : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          Data^.Construct^.ChangeAreaLayers(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_RequestLayer : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          DDEHandler.SetAnswerMode(TRUE);
                          ProcRequestLayer(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_RequestLayers: begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          DDEHandler.SetAnswerMode(TRUE);
                          ProcRequestLayerInfos(Data,'LMA');
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBSetTSLayer : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcDBSetTranspSelLayers(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBInsert     : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcDBInsert(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBChangedTable:begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          DDEHandler.SetAnswerMode(TRUE);
                          SendProjDataToDB;
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DKM          : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcDKMReply(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBObjStyle   : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcSetObjectStyleFromDB(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBSetOptions : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          ProcSetOptionsFromDB(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_BTX          : begin
                          DDEHandler.EnableDDEReception(FALSE);
                          ProcMakeBTXFile(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBCopyObject : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          DDEHandler.SetAnswerMode(TRUE);
                          ProcCopyObjectFromDB(Data,'CPY');
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBCopyObject2: begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          DDEHandler.SetAnswerMode(TRUE);
                          ProcCopyObjectFromDB(Data,'COI');
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_SetTranspMode: begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          DDEHandler.SetAnswerMode(TRUE);
                          ProcSetTranspModeFromDB(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_GetTranspMode: begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          DDEHandler.SetAnswerMode(TRUE);
                          ProcSendActualLayerToDB(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBViewInfos  : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          DDEHandler.SetAnswerMode(TRUE);
                          ProcSendSightInfosToDB(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_PartArea     : begin
                          DDEHandler.EnableDDEReception(FALSE);
                          Data^.Construct^.PartArea(Data,Self);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      {cm_DBDenySession: begin
                          ProcDBDenySession(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;}
      cm_FSConstruct  : begin
                          DDEHandler.EnableDDEReception(FALSE);
                          Data^.Construct^.DoConstructGr(Data,ConstructFS);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_SConstruct   : begin
                          DDEHandler.EnableDDEReception(FALSE);
                          Data^.Construct^.DoConstructGr(Data,ConstructS);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_ConstructFS  : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          if Data^.Construct^.DoConstructDB(Data,ConstructFS) then begin
                            Data^.SetActualMenu(mn_Construct);
                            if Data^.Construct^.ShowDlgs then begin
                              if Data^.Construct^.ShowConstrDlg(Data) then begin
                                if Data^.ActualMen = mn_Select then begin
                                  if Data^.Construct^.EndConstructDB(Data) then
                                    PostMessage(Handle,wm_Command,cm_Constructis,0);
                                end
                                else if Data^.ActualMen = mn_ConstructAbort then begin
                                  Data^.Construct^.ResetProjStatus(Data);
                                  Data^.SetActualMenu(mn_Select);
                                end;
                              end
                              else begin
                                Data^.Construct^.BreakConstructDB(Data);
                                Data^.SetActualMenu(mn_Select);
                                Data^.PInfo^.RedrawScreen(TRUE);
                              end;
                            end
                            else begin
                              Data^.SetActualMenu(mn_Select);
                              if Data^.Construct^.EndConstructDB(Data) then
                                PostMessage(Handle,wm_Command,cm_Constructis,0);
                            end;
                          end
                          else MsgBox(Handle,11410,mb_OK or mb_IconExclamation,'');
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_ConstructS   : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          if Data^.Construct^.DoConstructDB(Data,ConstructS) then begin
                            Data^.SetActualMenu(mn_Construct);
                            if Data^.Construct^.ShowDlgs then begin
                              if Data^.Construct^.ShowConstrDlg(Data) then begin
                                if Data^.ActualMen = mn_Select then begin
                                  if Data^.Construct^.EndConstructDB(Data) then begin
                                    PostMessage(Handle,wm_Command,cm_Constructis,0);
                                  end;
                                end
                                else if Data^.ActualMen = mn_ConstructAbort then begin
                                  Data^.Construct^.ResetProjStatus(Data);
                                  Data^.SetActualMenu(mn_Select);
                                end;
                              end
                              else begin
                                Data^.Construct^.BreakConstructDB(Data);
                                Data^.SetActualMenu(mn_Select);
                                Data^.PInfo^.RedrawScreen(TRUE);
                              end;
                            end
                            else begin
                              Data^.SetActualMenu(mn_Select);
                              if Data^.Construct^.EndConstructDB(Data) then
                                PostMessage(Handle,wm_Command,cm_Constructis,0);
                            end;
                          end
                          else MsgBox(Handle,11411,mb_OK or mb_IconExclamation,'');
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_Constructis  : begin
                          DDEHandler.EnableDDEReception(FALSE);
                          if Data^.ActualMen = mn_CBufferDraw then Data^.BreakInput(FALSE,FALSE);
                          Data^.DeselectAll(FALSE);
                          if Data^.Construct^.ShowConstrDlg(Data) then begin
                            if Data^.NewMenu = mn_Select then begin
                              if Data^.Construct^.EndConstructDB(Data) then
                                PostMessage(Handle,wm_Command,cm_Constructis,0);
                            end
                            else if Data^.ActualMen = mn_ConstructAbort then begin
                              Data^.Construct^.ResetProjStatus(Data);
                              Data^.SetActualMenu(mn_Select);
                            end;
                          end
                          else begin
                            Data^.DeselectAll(FALSE);
                            Data^.Construct^.BreakConstructDB(Data);
                            Data^.SetActualMenu(mn_Select);
                            Data^.PInfo^.RedrawScreen(TRUE);
                          end;
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DoParallel   : begin
                          DDEHandler.EnableDDEReception(FALSE);
                          Data^.Construct^.DoOutParallel(Data);
                          if Data^.ActualMen <> mn_CParallelS then begin
                            if Data^.Construct^.ShowConstrDlg(Data) then begin
                              if Data^.NewMenu = mn_Select then begin
                                if Data^.Construct^.EndConstructDB(Data) then
                                  PostMessage(Handle,wm_Command,cm_Constructis,0);
                              end;
                            end
                            else begin
                              Data^.DeselectAll(FALSE);
                              Data^.Construct^.BreakConstructDB(Data);
                              Data^.SetActualMenu(mn_Select);
                              Data^.PInfo^.RedrawScreen(TRUE);
                            end;
                          end;
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DoInParallel : begin
                          DDEHandler.EnableDDEReception(FALSE);
                          Data^.Construct^.DoInParallel(Data);
                          if Data^.Construct^.ShowConstrDlg(Data) then begin
                            if Data^.NewMenu = mn_Select then begin
                              if Data^.Construct^.EndConstructDB(Data) then
                                PostMessage(Handle,wm_Command,cm_Constructis,0);
                            end;
                          end
                          else begin
                            Data^.DeselectAll(FALSE);
                            Data^.Construct^.BreakConstructDB(Data);
                            Data^.SetActualMenu(mn_Select);
                            Data^.PInfo^.RedrawScreen(TRUE);
                          end;
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DoBuffer     : begin
                          DDEHandler.EnableDDEReception(FALSE);
                          Data^.Construct^.DoBuffer(Data);
                          if Data^.Construct^.ShowConstrDlg(Data) then begin
                            if Data^.NewMenu = mn_Select then begin
                              if Data^.Construct^.EndConstructDB(Data) then
                                PostMessage(Handle,wm_Command,cm_Constructis,0);
                            end;
                          end
                          else begin
                            Data^.DeselectAll(FALSE);
                            Data^.Construct^.BreakConstructDB(Data);
                            Data^.SetActualMenu(mn_Select);
                            Data^.PInfo^.RedrawScreen(TRUE);
                          end;
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DoInsIsland  : begin
                          DDEHandler.EnableDDEReception(FALSE);
                          Data^.Construct^.DoInsertIsland(Data);
                          if Data^.Construct^.ShowConstrDlg(Data) then begin
                            if Data^.NewMenu = mn_Select then begin
                              if Data^.Construct^.EndConstructDB(Data) then
                                PostMessage(Handle,wm_Command,cm_Constructis,0);
                            end;
                          end
                          else begin
                            Data^.DeselectAll(FALSE);
                            Data^.Construct^.BreakConstructDB(Data);
                            Data^.SetActualMenu(mn_Select);
                            Data^.PInfo^.RedrawScreen(True);
                          end;
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_SetRetry     : begin
                          DDEHandler.EnableDDEReception(FALSE);
                          Data^.Construct^.ConstructRetry:=TRUE;
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_LBGText      : begin
                          DDEHandler.EnableDDEReception(FALSE);
                          DDEHandler.SendString('[DBW][]');
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_Test         : begin
                        end;
{!?!?Route      cm_StartRoute   : begin
                          Data^.SetStartPoint;
                        end;}
      cm_GraphicReady : begin
                          DDEHandler.SetDBSendStrings(DDECommGRD,NIL);
                          DDEHandler.SendString('[RED][]');
                          DDEHandler.SetDBSendStrings(DDECommAll,NIL);
                          {DDEMLClient.ExecuteString('[GREADY()]');}
                        end;
      cm_ScrRefStart  : begin
                          DDEHandler.SendString('[SRS][]');
                        end;
      cm_ScrRefEnd    : begin
                          RedrawCanvas;
                        end;
      cm_CheckObjExist: begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          DDEHandler.SetAnswerMode(TRUE);
                          ProcCheckObjectExist(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBSetText    : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          Data^.SetActualMenu(mn_DBText);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBSetTextAPar: begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          DDEHandler.SetAnswerMode(TRUE);
                          ProcDBSetText(Data);
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_DBSetGraMode : begin
                          {DDEHandler.EnableDDEReception(FALSE);}
                          if ProcDBSetGraphicMode(Data) then WingisMainForm.ActivateWingis;
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_EndDDESeq    : begin
                          DDEHandler.EnableDDEReception(FALSE);
                          if DDEHandler.MacroMode then begin
                            DDEHandler.NWUMode:=TRUE;
                            DDEHandler.DoAllDDECommands(Self);
                            DDEHandler.NWUMode:=FALSE;
                            DDEHandler.MacroMode:=FALSE;
                            WinGISMainForm.SendRDRCommAfterDDE;
                          end;
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_EndDDEMLSeq  : begin
                          DDEHandler.EnableDDEReception(FALSE);
                          if DDEHandler.MacroMode then begin
                            DDEHandler.NWUMode:=TRUE;
                            DDEHandler.DoAllDDEMLCommands(Self);
                            DDEHandler.NWUMode:=FALSE;
                            DDEHandler.MacroMode:=FALSE;
                            WinGISMainForm.SendRDRCommAfterDDE;
                          end;
                          DDEHandler.EnableDDEReception(TRUE);
                        end;
      cm_StopOmega    : begin  {++Sygsky: Stop message from Omega}
                          { 'Cancel' parameter = TRUE or FALSE }
                          OmegaHndl.StopOmega(LParam <> 0);
                        end;
{++ Ivanoff }
      cm_DBGetPolygonData: begin
                          DDEHandler.EnableDDEReception(FALSE);
                          ProcDBSendPolyData(SELF.Data, LParam);
                          DDEHandler.EnableDDEReception(TRUE);
                        End;
{-- Ivanoff }
      cm_SuperImage://++Sygsky: to process mass framed image processing...
        begin    // First message id by value  but last processed. Unpretentious!
          Result := ProcessSuperImage(Data, Pointer(LParam));
        end; //--Sygsky
    end;
    UserInterface.Update([uiMenus]);
  end;
  {$ENDIF}
{$ENDIF} // <----------------- AXDLL

Procedure TMDIChild.WMTimer
   (
   var Msg         : TMessage
   );
  var
  ParentHandle: Integer;
  begin
    case Msg.wParam of
      idt_AutoSave : begin
        Dec(AutoCnt);
        if (AutoCnt<=0) and (WingisLicense >= WG_STD) then begin
          if DoesAutoSave then AutoCnt:=1
          else begin
            if Data^.Modified then begin
              DoesAutoSave:=TRUE;

              if Screen.ActiveForm <> nil then begin
                 ParentHandle:=Screen.ActiveForm.Handle
              end
              else begin
                 ParentHandle:=Handle;
              end;

              if MsgBox(ParentHandle,10105,mb_IconQuestion or mb_YesNo,StrPas(FName))=id_Yes then begin
                 StoreData;
              end;

              DoesAutoSave:=FALSE;
            end;
            AutoCnt:=IniFile^.Options.SaveInterv;
            SetTimer(Handle,idt_AutoSave,tvAutoSave,NIL)
          end;
        end;
      end;
      {$IFNDEF AXDLL} // <----------------- AXDLL
      idt_CanvasResize: begin
                             KillTimer(Handle,idt_CanvasResize);
                             SetGraphicParent(-1);
                        end;
      {$ENDIF} // <----------------- AXDLL
    else
        if Data<>NIL then Data^.MsgTimer(TWMTimer(Msg));
    end;
  end;

Procedure TMDIChild.SetAutoSave;
begin
  with IniFile^.Options do begin
    AutoCnt:=SaveInterv;
    if ((WingisLicense >= WG_STD) and (AutoSave)) then SetTimer(Handle,idt_AutoSave,tvAutoSave,NIL)
    else KillTimer(Handle,idt_AutoSave);
  end;
end;

Procedure SetDBApplication;
var MDIChild     : TMDIChild;
    AWindow      : THandle;
begin
  {$IFNDEF AXDLL} // <----------------- AXDLL
    {$IFNDEF WMLT}
  DDEHandler.EnableDDEReception(FALSE);
  UserInterface.BeginWaitCursor;
  MDIChild:=WinGISMainForm.GetCurrentChild;
  if MDIChild<>NIL then AWindow:=MDIChild.Handle
  else AWindow:=WinGISMainForm.Handle;
  DDEHandler.InitiateDDE(AWindow,TRUE,TRUE);
  if MDIChild <> NIL then begin
    MDIChild.SetDBSendInitStrings(FALSE);
    if (MDIChild.OpenMode = om_NewProj) and (DDEHandler.FNew = 1) then begin
      MDIChild.InitDB('PRN',TRUE);
      MDIChild.OpenMode:=om_ChangeProj;
    end
    else if (MDIChild.OpenMode = om_OpenProj) and (DDEHandler.FOpen = 1) then begin
      MDIChild.InitDB('PRO',TRUE);
      MDIChild.OpenMode:=om_ChangeProj;
    end
    else if DDEHandler.FChange = 1 then begin
      MDIChild.InitDB('PRW',TRUE);
      MDIChild.OpenMode:=om_ChangeProj;
    end
    else MDIChild.InitDB('PRW',FALSE);
    if DDEHandler.DDEOK then begin
      ProcDBSendViews(MDIChild.Data);
      if DDEHandler.FLayerInfo <> 0 then MDIChild.Data^.Layers^.DBSendLayerInfo(MDIChild.Data);
    end;
    MDIChild.SetDBSendInitStrings(TRUE);
    if DDEHandler.FSendGR then begin
      DDEHandler.SetDBSendStrings(DDECommGRD,NIL);
      DDEHandler.SendString('[RED][]');
      DDEHandler.SetDBSendStrings(DDECommAll,NIL);
    end;
  end
  else begin
    if DDEHandler <> NIL then begin
      DDEHandler.DDETerminate(AWindow,TRUE,FALSE);
      DDEHandler.CombineParamsOfAllActiveConnections;
    end;
  end;
  UserInterface.EndWaitCursor;
  UserInterface.Update([uiMenus]);
  DDEHandler.EnableDDEReception(TRUE);
    {$ENDIF}
  {$ENDIF} // <----------------- AXDLL
end;

Procedure TMDIChild.FormClose(Sender:TObject;var Action:TCloseAction);
var Cnt          : Integer;
    Rollup       : TRollupForm;
begin
  IsClosing:=True;
  Action:=caFree;
  {$IFNDEF AXDLL}
  if Data <> nil then begin
     try
        if WinGisMainForm.WeAreUsingTheIDB[Data] then begin
           WinGisMainForm.IDB_Man.DeleteProject(SELF.Data);
        end;
     except
        on E: Exception do begin
           DebugMsg('TMDIChild.FormClose',E.Message);
        end;
     end;
     try
        if WinGisMainForm.WeAreUsingUndoFunction[Data] then begin
           WinGisMainForm.UndoRedo_Man.DeleteProject(SELF.Data);
        end;
     except
        on E: Exception do begin
           DebugMsg('TMDIChild.FormClose',E.Message);
        end;
     end;
  end;
  {$ENDIF}
  for Cnt:=0 to UserInterface.RollupCount-1 do begin
    Rollup:=UserInterface.Rollups[Cnt];
    if Rollup is TProjRollupForm then TProjRollupForm(Rollup).Project:=NIL;
  end;
  UpdateLayerLists(NIL);
  UpdateLayersViewsLegendsLists(NIL);
  {$IFNDEF AXDLL}
  WinGisMainForm.DeleteTDocumentByName(StrPas(FName));
  WinGISMainForm.ActualProj := nil;
  if GPSConnection <> nil then begin
     GPSConnection.EndGPS;
  end;
  {$ENDIF}
end;

Procedure TMDIChild.CreateParams(var Params:TCreateParams);
begin
  inherited CreateParams(Params);
  with Params.WindowClass do Style:=Style or cs_OwnDC or cs_DblClks;
  Params.Style:=Params.Style or WS_MAXIMIZE;
end;

Procedure TMDIChild.KeyDown;
begin
     Data^.MsgKeyDown(Key,Shift);
end;

Procedure TMDIChild.KeyUp;
begin
  Data^.MsgKeyUp(Key,Shift);
end;

Procedure TMDIChild.KeyPress(var Key:Char);
begin
     {$IFNDEF AXDLL}
     DDEHandler.SendString('[KEY][' + Key + ']');
     {$ENDIF}
end;

Procedure TMDIChild.FormMouseDown(Sender: TObject; Button: TMouseButton;
    Shift: TShiftState; X, Y: Integer);
begin
  Windows.SetFocus(Handle);
  if not MoveView(X,Y) then begin
     Data^.OnMouseDown(X,Y,Button,Shift);
  end;
end;

Procedure TMDIChild.FormMouseUp(Sender: TObject; Button: TMouseButton;
    Shift: TShiftState; X, Y: Integer);
begin
  Data^.OnMouseUp(X,Y,Button,Shift);
end;

procedure TMDIChild.FormMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  Data^.OnMouseMove(X,Y,Shift);
  if not ((ssLeft in Shift) or (ssRight in Shift)) then begin
     ShowBorderCursor(X,Y);
  end;
end;

Procedure TMDIChild.DefaultMenuHandler;
begin
  Data^.DefaultMenuHandler(MenuFunction);
end;

{******************************************************************************+
  Procedure TMDIChild.FormDestroy
--------------------------------------------------------------------------------
+******************************************************************************}
Procedure TMDIChild.FormDestroy(Sender: TObject);
//var FAttr        : Integer;
{++ IDB}
Var
    sProjectFileName: AnsiString;
    bDeletingIsNecessary: Boolean;
{-- IDB}
  sHistory    : String;   // Glukhov Bug#574 Build#166 12.10.01
begin
//++ Glukhov Bug#574 Build#166 12.10.01
  if not IsClosing  then begin
    // to add the project to history I used the fragment from FormClose
    sHistory:= StrPas(FName);
    {$IFNDEF AXDLL} // <----------------- AXDLL
    {$IFNDEF WMLT}
    if FileExists(sHistory) and (not Data^.PInfo^.IsMU) then
    begin
      WinGisMainForm.FileHistory.NoAddHistoryFlag:= False;
      WinGisMainForm.FileHistory.Add(sHistory);
    end;
    {$ENDIF}
    {$ENDIF} // <----------------- AXDLL
  end;
//-- Glukhov Bug#574 Build#166 12.10.01
//  OnDestroy:=nil;
  {$IFNDEF AXDLL} // <----------------- AXDLL
    {$IFNDEF WMLT}
    {$ENDIF}
  {$ENDIF} // <----------------- AXDLL
  FCheckedMenus.Free;
  UserInterface.BeginWaitCursor;
  if not (csDestroying in WinGISMainForm.ComponentState) then
      UserInterface.StatusText:=GetLangText(2022);
  if (Colors256) and (Data <> nil) then ReloadOldPalette(Data^.PInfo^.ExtCanvas.WindowDC);
{++ IDB}
  bDeletingIsNecessary := FALSE;
{-- IDB}
  if Data<>NIL then begin
    if Data^.Construct<>NIL then Dispose(Data^.Construct,Done);
{++ IDB} // Store project file name and define the necessity of deleting project's and database's files.
    bDeletingIsNecessary := (Data.bProjectIsNewAndNoSaved) AND (Data.SymbolMode = sym_Project);
    sProjectFileName := Data.FName;
{-- IDB}
    Dispose(Data,Done);
    Data:=NIL;
  end;
//Route  RouteMenu.Free;
  {$IFNDEF AXDLL} // <----------------- AXDLL
    {$IFNDEF WMLT}
  if ImpExport<>NIL then Dispose(ImpExport, Done);
  if DBGraphic<>NIL then Dispose(DBGraphic, Done);
  if DDEHandler <> NIL then begin
    DDEHandler.DDETerminate(0,FALSE,FALSE);
    DDEHandler.SetActualChild(NIL);
  end;
    {$ENDIF}
  {$ENDIF} // <----------------- AXDLL
  if not WasReadOnly then UnlockFileWrites(LockHandle);
  {$IFNDEF AXDLL} // <----------------- AXDLL
    {$IFNDEF WMLT}
  if WinGISMainForm.CountChildren = 0 then
     if DDEHandler <> NIL then
        DDEHandler.SetActualChild(NIL);
{++ IDB} // If we have the necessity of deleting project's and database's files, we have to do it.
         // We do it here because here only the project file is unlocked by WinGis.
  If (bDeletingIsNecessary) and (Data <> nil) Then Begin
     TRY
     If WinGisMainForm.WeAreUsingTheIDB[Data] Then
        WinGisMainForm.IDB_Man.DeleteProjectAndDatabaseFiles(sProjectFileName);
     EXCEPT
     END;
  End;
    {$ENDIF}
{-- IDB}
  UserInterface.EndWaitCursor;
  {$ENDIF} // <----------------- AXDLL
  ProgisIdTMList.Clear;
  ProgisIdTMList.Free;
  UserInterface.StatusText:='';
end;

{******************************************************************************+
  Procedure TMDIChild.FormMDIActivate
--------------------------------------------------------------------------------
  Wird von Windows aufgerufen, wenn das Fenster das aktive MDI-Fenster wird.
  Die Datenbank wird vom Projektwechsel informiert, die File-History und die
  Window-Liste wird in das Men� des Fensters eingeblendet und die
  Aktualisierung der Men�s, der Statuszeile und der Layer-Listen veranla�t.
+******************************************************************************}
Procedure TMDIChild.FormMDIActivate(Sender: TObject);
var Cnt          : Integer;
    Rollup       : TRollupForm;
begin
  if (Data<>NIL) then begin
    {$IFNDEF AXDLL} // <----------------- AXDLL
      {$IFNDEF WMLT}
    DDEHandler.EnableDDEReception(TRUE);
      {$ENDIF}
    {$ENDIF} // <----------------- AXDLL
    UserInterface.StatusText:=GetLangText(4020);
    if not OMChanged then begin
      if WinGISMainForm.CreateNewProj then begin
        OpenMode:=om_NewProj;
        // set file-version to avoid "overwrite WinGIS 3 project" warning
        Data^.FileVersion:=vnProject;
      end
      else OpenMode:=om_OpenProj;
      OMChanged:=TRUE;
    end;
    {$IFNDEF AXDLL} // <----------------- AXDLL
      {$IFNDEF WMLT}
    DDEHandler.SetDBWMProjInited(FALSE);
    SetDBSendInitStrings(FALSE);
    if (OpenMode = om_NewProj) and (DDEHandler.FNew = 1) then begin
      InitDB('PRO',TRUE);
      OpenMode:=om_ChangeProj;
    end
    else if (OpenMode = om_OpenProj) and (DDEHandler.FOpen = 1) then begin
      InitDB('PRO',TRUE);
      OpenMode:=om_ChangeProj;
    end
    else if DDEHandler.FChange = 1 then begin
      InitDB('PRW',TRUE);
      OpenMode:=om_ChangeProj;
    end
    else InitDB('PRW',FALSE);
    if DDEHandler.DDEOK then ProcDBSendViews(Data);
    if DDEHandler.FLayerInfo <> 0 then Data^.Layers^.DBSendLayerInfo(Data);
    SetDBSendInitStrings(TRUE);
    if DDEHandler.FSendGR then SendRedraw:=TRUE;
      {$ENDIF}
    {$ENDIF} // <----------------- AXDLL
    { aktuell ausgew�hlte Men�punkte wiederherstellen }
    UserInterface.StatusText:='';
    MenuFunctions.CheckedMenuFunctions:=FCheckedMenus;
    { Rollups updaten }
    for Cnt:=0 to UserInterface.RollupCount-1 do begin
      Rollup:=UserInterface.Rollups[Cnt];
      if Rollup is TProjRollupForm then TProjRollupForm(Rollup).Project:=Data;
    end;

    { Enable/Disable der Men�s und Toolbar-Buttons durchf�hren, Statuszeile }
    { Cursor und Layerlisten updaten }
    Data^.MsgActivate;
    // set color-pallet menus
    if WhichPalette=vp_Colors then MenuFunctions['ViewColorPalette'].Checked:=TRUE
    else if WhichPalette=vp_Gray then MenuFunctions['ViewGrayscalePalette'].Checked:=TRUE
    else MenuFunctions['ViewPicturePalette'].Checked:=TRUE;
    UserInterface.Update([uiMenus,uiCursor,uiStatusBar,uiLayers,uiWorkingLayer,
        uiContextToolbars]);
    {$IFNDEF AXDLL} // <----------------- AXDLL
      {$IFNDEF WMLT}
    DDEHandler.EnableDDEReception(TRUE);
    WinGISMainForm.SetCurrentTDocument(strpas(self.FNAME));
    if Data^.SymbolMode = sym_Project then begin
       VEForm.AChild:=Self;
    end;
      {$ENDIF}
    {$ENDIF} // <----------------- AXDLL
  end;
end;

{******************************************************************************+
  Procedure TMDIChild.FormMDIDeactivate
--------------------------------------------------------------------------------
  Wird von Windows aufgerufen, wenn das aktive MDI-Fenster deaktiviert wird.
+******************************************************************************}
Procedure TMDIChild.FormMDIDeactivate(Sender:TObject);
begin
  // derzeit gew�hlte Men�punkte sichern
  FCheckedMenus.Assign(MenuFunctions.CheckedMenuFunctions);
  Data^.MsgDeActivate;
end;

{******************************************************************************+
  Procedure TMDIChild.FormCreate
--------------------------------------------------------------------------------
  Initialisiert die Variablen des Fensters.
+******************************************************************************}
Procedure TMDIChild.FormCreate(Sender: TObject);
begin
  Caption:='';
  FCheckedMenus:=TStringList.Create;
  DisableAutoRange;
  AutoScroll:=FALSE;
  OpenMode:=om_ChangeProj;
  OMChanged:=FALSE;
  //Scaleby(Trunc(11500000/(PixelsPerInch)),100000);
end;

{******************************************************************************+
  Procedure TMDIChild.WMEraseBkgnd
--------------------------------------------------------------------------------
  Verhindert, da� Windows des Fensterhintergrund neu zeichnet (wird in
  TProj.Draw durchgef�hrt).
+******************************************************************************}
Procedure TMDIChild.WMEraseBkgnd;
begin
  Msg.Result:=1;
end;


{==============================================================================+
  Men�functions
+==============================================================================}

Procedure TMDIChild.ExtrasMeasureDistance;
begin
  Data^.SetActualMenu(mn_Distance);
end;

{$IFNDEF AXDLL} // <----------------- AXDLL
  {$IFNDEF WMLT}
Procedure TMDIChild.TurboRasterEditImage;
begin
  ProcTurboRaster;
end;

Procedure TMDIChild.TurboRasterLoadImages;
begin
  ProcLoadTRFiles(Data, nil);
end;

Procedure TMDIChild.ExtrasOffset;
begin
  ProcMakeOffset(Data);
end;

Procedure TMDIChild.ExtrasAutomaticSnap;
begin
  ProcAutoSnap(Data);
end;

Procedure TMDIChild.ExtrasTrimm;
begin
  ProcTrimmDlg(Data);
end;

Procedure TMDIChild.FileReload;
begin
  ProcRecharge(Data,Self);
end;
  {$ENDIF}
{$ENDIF} // <----------------- AXDLL

Procedure TMDIChild.RasterTransformationLinear;
begin
  Data^.RTrafoType:=tfLinear;
  Data^.SetActualMenu(mn_RTrafo);
end;

Procedure TMDIChild.RasterTransformationHelmert;
begin
  Data^.RTrafoType:=tfHelmert;
  Data^.SetActualMenu(mn_RTrafo);
end;
{+++brovak #MOD}
Procedure TMDIChild.CutSelectedPartOfImage;
 begin;
 Data^.CSelectForCut:=1;
 Data^.SetActualMenu(mn_CutImage);
  end;
{--brovak}

{$IFNDEF AXDLL} // <----------------- AXDLL
  {$IFNDEF WMLT}
{+++brovak #MOD 193}
Procedure TMDIChild.EditPointInVariousObjects;
begin;
     with Data^ do
      begin;
       if Layers^.SelLayer^.SelInfo.Count = 1 then begin
          SnapItem:=Layers^.SelLayer^.Data^.At(0);
       end
       else begin
          SnapItem:=nil;
       end;
        if ((IsEditVarious=false) and (not CheckFixedOrNot))
          then //Call Function;
         begin
            IsEditVarious:=true;
            OldSnapItem:=nil;
            FindOLdSnapItem;
            if MenuFunctions['EditPointInVariousObjects'].Checked=false
             then MenuFunctions['EditPointInVariousObjects'].Checked:=true;
            SwitchEditPointInVariousObjects;
         end
         else
         begin;
          MenuFunctions['EditPointInVariousObjects'].Checked:=false;
           Data^.DeSelectAll(true);
           Data^.SetActualMenu(mn_Select,false);
          Data^.IsEditVarious:=false;
         end;
      end;

  end;

Procedure TMDIChild.SwitchEditPointInVariousObjects;
  Procedure SetPolyMode;
      begin;
       case Data.EditVertexMode of
      0,1:EditPointMove;
        2:EditPointInsert ;
        3:EditPointDelete;
        4:begin;
          Data.DeselectAll(true);
          Data^.Pinfo.EditPoly:=true;
          MenuFunctions['EditObjects'].Execute;
          end;
        5:PseudoSelectMode;
      end //case
      end;


 var objT:word;
     TempPoint:TDPOint;

   begin;
    TempPOint.Init(0,0);
   if Data.SnapItem<>nil then
    begin;
     if Data.CheckFixedOrNot = true then Exit;
       ObjT:= Data.SnapItem.GetObjType;

      if ObjT in [ot_poly,ot_cpoly] then
      SetPolyMode
      else
      begin;
      if ObjT=ot_Pixel then
       begin;
        if Data^.ActualMen<>mn_Snap then
        begin;
         TempPoint:=PPixel(Data^.SnapItem)^.Position;
         EditPointMove;
         Data.MsgLeftClick(TempPOint);
        end
        else
        begin;
         MenuFunctions['EditPointMove'].Checked:=true;
        end;
       end
      else
      begin;
      Data.EditVertexMode:=4;
      Data.DeselectAll(true);Data^.Pinfo.EditPoly:=false;
      MenuFunctions['EditObjects'].Execute;
      end;
      end;
    end
    else
      SetPolyMode;
   end;


 Procedure TMDIChild.PreparePointInVariousObjects;
   begin;
    with Data^ do
     begin;
     Layers.TopLayer.Select(Pinfo,OldSnapItem);
     SnapItem:=OLdSnapItem.Mptr;
     PInfo^.EditPoly:=TRUE;
     POintInd:=-1;
     end;
     end;
{--brovak}

{++Brovak - for convert old island to new}
  Procedure TMDIChild.SelectedConvertPoly;
  var i:integer;
      Item:Pindex;
      BPoly:PCPoly;
      Rect:TDRect;
      TopL:Player;
  begin;
  TopL:=Data.Layers.TopLayer;
  for i:=0 to Data^.Layers^.SelLayer^.Data^.GetCount-1 do
   begin;
   Item:= Data^.Layers^.SelLayer^.Data^.At(i);
   Item:= Data^.Layers^.IndexObject(Data^.PInfo,Item);
   if Item.GetObjType=ot_Cpoly
    then
     if CheckIfOldCpoly(PCPoly(Item))= true
      then
       begin;
       BPoly:=PCPoly(Item);
       Rect.InitByRect(Bpoly.ClipRect);
       ConvertOldIslandPoly(PCpoly(Item), Data^.Layers^.TopLayer^.Index);
       RedrawWithAllAttached(Data,PView(BPoly),Rect);
       end;

   end;
    Data.Layers.SetTopLayer(TopL);
  end;

function TMDIChild.ConvertOldPolygonLayer(LayerIndex:integer):longint;
  var i, Converted:longint;
      Item:Pindex;
      BPoly:PCPoly;
      Rect:TDRect;
      ALayer:Player;
      AllCount,Allcount1:longint;
      PerCentNow,PerCentOld,PercentPut:double;
  begin;
  Alayer:= Data.Layers.IndexToLayer(LayerIndex);
  StatusBar.ProgressText:= GetLangText(3999)+' '+ptoStr(Alayer.Text);
  UserInterface.Update([uiStatusBar]);
  AllCount:=Alayer.Data^.GetCount;
  Allcount1:=AllCount-1;
  PerCentOld:=0;
  PercentPut:=AllCount/19;
  StatusBar.Progress:=0;
  Converted:=0;
  UserInterface.Update([uiStatusBar]);
  if (Alayer.GetState(sf_fixed)=false)
  and (AllCount>0)
  then
    for i:=0 to AllCount1 do
    begin;
       PerCentNow:=100*i/(AllCount);
       if (PercentNow-PercentOld)>PercentPut
        then
         begin;
          StatusBar.Progress:=100*i/(AllCount);
          PercentOldPut:=PerCentNow;
          UserInterface.Update([uiStatusBar]);
        //  Application.ProcessMessages;
         end;
   Item:= Alayer.Data^.At(i);
   Item:= Data^.Layers^.IndexObject(Data^.PInfo,Item);
   if Item.GetObjType=ot_Cpoly
    then
     if CheckIfOldCpoly(PCPoly(Item))= true
      then
       begin;
       BPoly:=PCPoly(Item);
       Rect.InitByRect(Bpoly.ClipRect);
      if ConvertOldIslandPoly(PCpoly(Item),LayerIndex)=true
       then Inc(Converted);
       RedrawWithAllAttached(Data,PView(BPoly),Rect);
       end;
   end;
   Result:=Converted;
 end;

Procedure TMDIChild.OnLayerConvertPoly;
 var Count:longint;
     Topl:PLayer;
 begin;
   Topl:=Data.Layers.TopLayer;
   Screen.Cursor:=crHourGlass;
   StatusBar.ProgressPanel := TRUE;
   Count:=ConvertOldPolygonLayer(Data^.Layers^.Toplayer^.Index);
   StatusBar.ProgressPanel := False;
   Screen.Cursor:=crDefault;
   Data.Layers.SetTopLayer(TopL);
   ShowMessage(Format(GetLangText(3997),[PtOStr(Data.Layers.Toplayer.Text)])+IntToStr(Count)+' '+GetLangText(3998));
 end;

Procedure TMDIChild.InProjectConvertPoly;
 var i:integer;
    Count:integer;
 begin;
  Count:=0;
  Screen.Cursor:=crHourGlass;
  StatusBar.ProgressPanel := TRUE;
  for i:=1 to Data.Layers.Ldata.Count-1 do
  Count:=Count+ConvertOldPolygonLayer(Player(Data^.Layers^.Ldata.At(i))^.Index);

StatusBar.ProgressPanel := false;
Screen.Cursor:=crDefault;
ShowMessage(Format(GetLangText(3996),[StrPas(Data.Fname)])+IntToStr(Count)+' '+GetLangText(3998));

 end;

function TMDIChild.ConvertOldIslandPoly(APoly:PCPoly;LayerIndex:integer):Boolean;
 var Apoly1:PCPoly;
     Convert_2000:TConvert2000;
 begin;
     Result:=false;
     Convert_2000 := TConvert2000.Create;
//     Apoly1:=New(PCPoly,Init);
     Apoly1:=MakeObjectCopy(Convert_2000.ConvertOldIslandAreaToVersion2000(APoly));
     if APoly^.IslandCount<APoly1^.IslandCount
      then Result:=true;
     APoly^.State:=APoly1^.State;
     APoly^.IslandInfo:=Apoly1.IslandInfo;
     APoly^.IslandCount:=Apoly1.IslandCount;
     Apoly.Data:=MakeObjectCopy(Apoly1.Data);
     Convert_2000.Destroy;
 //    Data.Layers.SetTopLayer(Data.Layers.IndexToLayer(LayerIndex));
 end;



{--Brovak}

Procedure TMDIChild.EditOLEObject;
begin
  Data^.EditOLEObject;
end;

Procedure TMDIChild.ExtrasAreaAnalysis;
begin
  SelectEmpty(Data,Data^.PInfo);
end;

Procedure TMDIChild.EditText;
begin
  ProcEditText(Data);
end;

Procedure TMDIChild.FileSave;
begin
  if Data^.Modified then begin
     StoreData;
     if DDEHandler.FSave <> 0 then begin
        DDEHandler.SetDBSendStrings(DDECommPInfo,NIL);
        DDEHandler.SendString('[PRS]['+StrPas(FName)+']');
        DDEHandler.SetDBSendStrings(DDECommAll,NIL);
     end;
  end;
end;

Procedure TMDIChild.MUSynchronize;
begin                             
     MU.SendChanges;
     MU.ApplyChanges(False);
end;

Procedure TMDIChild.MUReorganize;
begin
     MU.ReorganizeProject;
end;

{$ENDIF}

Procedure TMDIChild.DigitCalibrateDigitizer;
begin
  Digitizer.DeActivateDigitizer;
  if {Data^.DigitMode or }Digitizer.ActivateDigitizer(Data) then
      Data^.SetActualMenu(mn_RefPoints)
  else MenuFunctions['DigitCalibrateDigitizer'].Checked:=FALSE;
end;
{$ENDIF} // <----------------- AXDLL

Procedure TMDIChild.DigitTransformation;
begin
  TransformDlg(Data);
end;

Procedure TMDIChild.ViewColorPalette;
begin
  ProcSetColorsPalette(Data);
end;

Procedure TMDIChild.ViewGrayscalePalette;
begin
  ProcSetGrayPalette(Data);
end;

Procedure TMDIChild.ViewPicturePalette;
begin
  ProcSetBmpPalette(Data);
end;

Procedure TMDIChild.FileClose;
begin        
  Close;
end;

 {Close New polyline with ShortCut}
   Procedure TMDIChild.ClosePolyline;

  begin;

  if Data^.ActualMen= mn_Poly
   then
    if Data^.NewPoly.Data.Count>2
     then
      begin;      //close poly
         Data^.NewPoly.PointInsert(Data.PInfo,TDPOINT(Data.NewPoly^.Data.At(0)^), TRUE);
        //virtual tmp point, because last line will be deleted in WinGIS ideology
       Data^.NewPoly.PointInsert(Data.PInfo,TDPOINT(Data.NewPoly^.Data.At(1)^), TRUE);
       Data.MsgLeftDblClick(Data.NewPoly.StartPos);

    end;

  end;



{$IFNDEF WMLT}
  {$IFNDEF AXDLL} // <----------------- AXDLL
Procedure TMDIChild.ExtrasDKMCheck;
type TCheckDkm=procedure(DIR:PCHAR;LNGCODE:PCHAR;var PROJECT:VARIANT; var AXHANDLE:VARIANT);stdcall;
var DkmCheckDll    :THandle;
    CHECKDKM       :TCheckDkm;
    CheckDkmPtr    :TFarProc;
    curdir :PCHAR;
    langext:PCHAR;
    inifilename:string;
    Languageext:string;
    Ini: TIniFile;
    curdoc:Variant;
begin
   WinGISMainForm.Enabled:=false;
   curdir:=stralloc(length(OSInfo.WingisDir)+1);
   strpcopy(curdir,OSInfo.WingisDir);

   // get language code
   inifilename:=OSInfo.WingisIniFileName;
   Ini:=TIniFile.Create(OSInfo.WingisIniFileName);
   LanguageExt:=Ini.ReadString('Settings','Language','txt');
   Ini.Free;
   langext:=stralloc(length(LanguageExt)+1); strpcopy(langext, LanguageExt);

   DkmCheckDll:=LoadLibrary('DKMCHECK.DLL');
   WingisMainForm.CheckLibStatus(DkmCheckDll,'DKMCHECK.DLL','DKM');
   CheckDkmPtr:=GetProcAddress(DkmCheckDll,'CHECKDKM');
   if CheckDkmPtr <> NIL then
   begin
      @CHECKDKM:=CheckDkmPtr;

      WinGISMainForm.CheckTDocumentIsCorrect;
      curdoc:=WinGISMainForm.GetCurrentTDocument;

      CHECKDKM(curdir,langext,curdoc,WinGISMainForm.DXApp);
   end;
   FreeLibrary(DkmCheckDll);
   WinGISMainForm.Enabled:=true;
   UpdateLayersViewsLegendsLists(Data);
   UpdateSymbolRollup;
   Data^.Pinfo^.RedrawScreen(false);
   Data^.Modified:=true;
   strdispose(curdir);
   strdispose(langext);
end;

Procedure TMDIChild.FileExportASCII;
begin
  ImpExport^.ExportASCII;
end;

Procedure TMDIChild.FileExportDXF;
begin
  ImpExport^.ExportDXF;
end;

Procedure TMDIChild.FileImportASCIIDX;
type TAscImport=procedure (DIR:PCHAR;var AXHANDLE:Variant;var DEST:Variant;REGMODE:integer;LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer); stdcall;
var inifilename:string;
    Languageext:string;
    Ini: TIniFile;
    ASCIMPORT   :TAscImport;
    AscImportPtr:TFarProc;
    curdoc:Variant;
    pdir  :PCHAR;
    plng  :PCHAR;
    WinHandle: Integer;
begin
   {$IFNDEF WMLT}
   if Data^.IsCanvas then begin
      WinHandle:=Application.Handle;
   end
   else begin
      WinHandle:=WingisMainForm.Handle;
   end;

   pdir:=stralloc(length(OSInfo.WingisDir)+1);
   strpcopy(pdir,OSInfo.WingisDir);

   // get language code
   inifilename:=OSInfo.WingisIniFileName;
   Ini:=TIniFile.Create(OSInfo.WingisIniFileName);
   LanguageExt:=Ini.ReadString('Settings','Language','044'); // default is english
   Ini.Free;
   plng:=stralloc(length(LanguageExt)+1);
   strpcopy(plng, LanguageExt);

   WinGISMainForm.ImpExpDll:=LoadLibrary('ASCIMP.DLL');
   WingisMainForm.CheckLibStatus(WingisMainForm.ImpExpDll,'ASCIMP.DLL','Import/Export');
   AscImportPtr:=GetProcAddress(WinGISMainForm.ImpExpDll,'ASCIMPORT');
   if AscImportPtr <> NIL then
   begin
      @ASCIMPORT:=AscImportPtr;
      // call pxf-asc import DLL
      WinGISMainForm.CheckTDocumentIsCorrect;
      curdoc:=WinGISMainForm.GetCurrentTDocument;
      ASCIMPORT(pdir,WinGISMainForm.DXApp,curdoc,1, plng,
                WinHandle,
                WinGISMainForm.Top,
                WinGISMainForm.Left,
                WinGISMainForm.Width,
                WinGISMainForm.Height
               );
      WinGISMainForm.LastImpExpOperation:='ASCIMP';
   end
   else
   begin
      FreeLibrary(WinGISMainForm.ImpExpDll);
   end;
   strdispose(pdir);
   strdispose(plng);
   {$ENDIF}
end;

Procedure TMDIChild.FileExportASCIIDX;
type TAscExport=procedure (DIR:PCHAR;var AXHANDLE:VARIANT;var SOURCE:VARIANT;REGMODE:integer; LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);stdcall;
var inifilename:string;
    Languageext:string;
    Ini: TIniFile;
    ASCEXPORT   :TAscExport;
    AscExportPtr:TFarProc;
    curdoc:Variant;
    pdir  :PCHAR;
    plng  :PCHAR;
    WinHandle: Integer;
begin
   {$IFNDEF WMLT}
   if Data^.IsCanvas then begin
      WinHandle:=Application.Handle;
   end
   else begin
      WinHandle:=WingisMainForm.Handle;
   end;

   pdir:=stralloc(length(OSInfo.WingisDir)+1);
   strpcopy(pdir,OSInfo.WingisDir);

   // get language code
   inifilename:=OSInfo.WingisIniFileName;
   Ini:=TIniFile.Create(OSInfo.WingisIniFileName);
   LanguageExt:=Ini.ReadString('Settings','Language','044'); // default is english
   Ini.Free;
   plng:=stralloc(length(LanguageExt)+1);
   strpcopy(plng, LanguageExt);

   WinGISMainForm.ImpExpDll:=LoadLibrary('ASCEXP.DLL');
   WingisMainForm.CheckLibStatus(WingisMainForm.ImpExpDll,'ASCEXP.DLL','Import/Export');
   AscExportPtr:=GetProcAddress(WinGISMainForm.ImpExpDll,'ASCEXPORT');
   if AscExportPtr <> NIL then
   begin
      @ASCEXPORT:=AscExportPtr;
      // call pxf-asc import DLL
      WinGISMainForm.CheckTDocumentIsCorrect;
      curdoc:=WinGISMainForm.GetCurrentTDocument;
      ASCEXPORT(pdir,WinGISMainForm.DXApp,curdoc,1, plng,
                   WinHandle,
                   WinGISMainForm.Top,
                   WinGISMainForm.Left,
                   WinGISMainForm.Width,
                   WinGISMainForm.Height
                  );
      WinGISMainForm.LastImpExpOperation:='ASCEXP';
   end
   else
   begin
      FreeLibrary(WinGISMainForm.ImpExpDll);
   end;
   strdispose(pdir);
   strdispose(plng);
   {$ENDIF}
end;

Procedure TMDIChild.FileImportE00DX;
type TE00Import=procedure (DIR:PCHAR;var AXHANDLE:Variant;var DEST:Variant;REGMODE:integer;LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer); stdcall;
var inifilename:string;
    Languageext:string;
    Ini: TIniFile;
    E00IMPORT   :TE00Import;
    E00ImportPtr:TFarProc;
    curdoc:Variant;
    pdir  :PCHAR;
    plng  :PCHAR;
    WinHandle: Integer;
begin
   {$IFNDEF WMLT}
   if Data^.IsCanvas then begin
      WinHandle:=Application.Handle;
   end
   else begin
      WinHandle:=WingisMainForm.Handle;
   end;

   pdir:=stralloc(length(OSInfo.WingisDir)+1);
   strpcopy(pdir,OSInfo.WingisDir);

   // get language code
   inifilename:=OSInfo.WingisIniFileName;
   Ini:=TIniFile.Create(OSInfo.WingisIniFileName);
   LanguageExt:=Ini.ReadString('Settings','Language','044'); // default is english
   Ini.Free;
   plng:=stralloc(length(LanguageExt)+1);
   strpcopy(plng, LanguageExt);

   WinGISMainForm.ImpExpDll:=LoadLibrary('E00IMP.DLL');
   WingisMainForm.CheckLibStatus(WingisMainForm.ImpExpDll,'E00IMP.DLL','Import/Export');
   E00ImportPtr:=GetProcAddress(WinGISMainForm.ImpExpDll,'E00IMPORT');
   if E00ImportPtr <> NIL then
   begin
      @E00IMPORT:=E00ImportPtr;
      // call pxf-asc import DLL
      WinGISMainForm.CheckTDocumentIsCorrect;
      curdoc:=WinGISMainForm.GetCurrentTDocument;
      E00IMPORT(pdir,WinGISMainForm.DXApp,curdoc,1, plng,
                WinHandle,
                WinGISMainForm.Top,
                WinGISMainForm.Left,
                WinGISMainForm.Width,
                WinGISMainForm.Height
               );
      WinGISMainForm.LastImpExpOperation:='E00IMP';
   end
   else
   begin
      FreeLibrary(WinGISMainForm.ImpExpDll);
   end;
   strdispose(pdir);
   strdispose(plng);
   {$ENDIF}
end;

Procedure TMDIChild.FileExportE00DX;
type TE00Export=procedure (DIR:PCHAR;var AXHANDLE:VARIANT;var SOURCE:VARIANT;REGMODE:integer; LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);stdcall;
var inifilename:string;
    Languageext:string;
    Ini: TIniFile;
    E00EXPORT   :TE00Export;
    E00ExportPtr:TFarProc;
    curdoc:Variant;
    pdir  :PCHAR;
    plng  :PCHAR;
    WinHandle: Integer;
begin
   {$IFNDEF WMLT}
   if Data^.IsCanvas then begin
      WinHandle:=Application.Handle;
   end
   else begin
      WinHandle:=WingisMainForm.Handle;
   end;

   pdir:=stralloc(length(OSInfo.WingisDir)+1);
   strpcopy(pdir,OSInfo.WingisDir);

   // get language code
   inifilename:=OSInfo.WingisIniFileName;
   Ini:=TIniFile.Create(OSInfo.WingisIniFileName);
   LanguageExt:=Ini.ReadString('Settings','Language','044'); // default is english
   Ini.Free;
   plng:=stralloc(length(LanguageExt)+1);
   strpcopy(plng, LanguageExt);

   WinGISMainForm.ImpExpDll:=LoadLibrary('E00EXP.DLL');
   WingisMainForm.CheckLibStatus(WingisMainForm.ImpExpDll,'E00EXP.DLL','Import/Export');
   E00ExportPtr:=GetProcAddress(WinGISMainForm.ImpExpDll,'E00EXPORT');
   if E00ExportPtr <> NIL then
   begin
      @E00EXPORT:=E00ExportPtr;
      // call pxf-asc import DLL
      WinGISMainForm.CheckTDocumentIsCorrect;
      curdoc:=WinGISMainForm.GetCurrentTDocument;
      E00EXPORT(pdir,WinGISMainForm.DXApp,curdoc,1, plng,
                WinHandle,
                WinGISMainForm.Top,
                WinGISMainForm.Left,
                WinGISMainForm.Width,
                WinGISMainForm.Height
               );
      WinGISMainForm.LastImpExpOperation:='E00EXP';
   end
   else
   begin
      FreeLibrary(WinGISMainForm.ImpExpDll);
   end;
   strdispose(pdir);
   strdispose(plng);
   {$ENDIF}
end;

Procedure TMDIChild.FileImportDXFDX;
type TDxfImport=procedure (DIR:PCHAR;var AXHANDLE:Variant;var DEST:Variant;REGMODE:integer;LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer; DKMREG:integer); stdcall;
var inifilename:string;
    Languageext:string;
    Ini: TIniFile;
    DXFIMPORT   :TDxfImport;
    DxfImportPtr:TFarProc;
    curdoc:Variant;
    pdir  :PCHAR;
    plng  :PCHAR;
    allowdkm:integer;
    WinHandle: Integer;
begin
   {$IFNDEF WMLT}
   if Data^.IsCanvas then begin
      WinHandle:=Application.Handle;
   end
   else begin
      WinHandle:=WingisMainForm.Handle;
   end;

   pdir:=stralloc(length(OSInfo.WingisDir)+1);
   strpcopy(pdir,OSInfo.WingisDir);

   // get language code
   inifilename:=OSInfo.WingisIniFileName;
   Ini:=TIniFile.Create(OSInfo.WingisIniFileName);
   LanguageExt:=Ini.ReadString('Settings','Language','044'); // default is english
   Ini.Free;
   plng:=stralloc(length(LanguageExt)+1);
   strpcopy(plng, LanguageExt);

   WinGISMainForm.ImpExpDll:=LoadLibrary('DXFIMP.DLL');
   WingisMainForm.CheckLibStatus(WingisMainForm.ImpExpDll,'DXFIMP.DLL','Import/Export');
   DxfImportPtr:=GetProcAddress(WinGISMainForm.ImpExpDll,'DXFIMPORT');
   if DxfImportPtr <> NIL then
   begin
      @DXFIMPORT:=DxfImportPtr;
      // call pxf-asc import DLL
      WinGISMainForm.CheckTDocumentIsCorrect;
      curdoc:=WinGISMainForm.GetCurrentTDocument;
      //if LicenseList[30]  = TRUE then  // start DXFIMPORT with allowed DKM
         allowdkm:=1;
      //else
      //   allowdkm:=0;
      DXFIMPORT(pdir,WinGISMainForm.DXApp,curdoc,1, plng,
                WinHandle,
                WinGISMainForm.Top,
                WinGISMainForm.Left,
                WinGISMainForm.Width,
                WinGISMainForm.Height,
                allowdkm
               );
      WinGISMainForm.LastImpExpOperation:='DXFIMP';
   end
   else
   begin
      FreeLibrary(WinGISMainForm.ImpExpDll);
   end;
   strdispose(pdir);
   strdispose(plng);
   {$ENDIF}
end;

Procedure TMDIChild.FileExportDXFDX;
type TDxfExport=procedure (DIR:PCHAR;var AXHANDLE:VARIANT;var SOURCE:VARIANT;REGMODE:integer; LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);stdcall;
var inifilename:string;
    Languageext:string;
    Ini: TIniFile;
    DXFEXPORT   :TDxfExport;
    DxfExportPtr:TFarProc;
    curdoc:Variant;
    pdir  :PCHAR;
    plng  :PCHAR;
    WinHandle: Integer;
begin
   {$IFNDEF WMLT}
   if Data^.IsCanvas then begin
      WinHandle:=Application.Handle;
   end
   else begin
      WinHandle:=WingisMainForm.Handle;
   end;

   pdir:=stralloc(length(OSInfo.WingisDir)+1);
   strpcopy(pdir,OSInfo.WingisDir);

   // get language code
   inifilename:=OSInfo.WingisIniFileName;
   Ini:=TIniFile.Create(OSInfo.WingisIniFileName);
   LanguageExt:=Ini.ReadString('Settings','Language','044'); // default is english
   Ini.Free;
   plng:=stralloc(length(LanguageExt)+1);
   strpcopy(plng, LanguageExt);

   WinGISMainForm.ImpExpDll:=LoadLibrary('DXFEXP.DLL');
   WingisMainForm.CheckLibStatus(WingisMainForm.ImpExpDll,'DXFEXP.DLL','Import/Export');
   DxfExportPtr:=GetProcAddress(WinGISMainForm.ImpExpDll,'DXFEXPORT');
   if DxfExportPtr <> NIL then
   begin
      @DXFEXPORT:=DxfExportPtr;
      // call pxf-asc import DLL
      WinGISMainForm.CheckTDocumentIsCorrect;
      curdoc:=WinGISMainForm.GetCurrentTDocument;
      DXFEXPORT(pdir,WinGISMainForm.DXApp,curdoc,1, plng,
                WinHandle,
                WinGISMainForm.Top,
                WinGISMainForm.Left,
                WinGISMainForm.Width,
                WinGISMainForm.Height
               );
      WinGISMainForm.LastImpExpOperation:='DXFEXP';
   end
   else
   begin
      FreeLibrary(WinGISMainForm.ImpExpDll);
   end;
   strdispose(pdir);
   strdispose(plng);
   {$ENDIF}
end;

Procedure TMDIChild.FileImportSHPDX;
type TShpImport=procedure (DIR:PCHAR;var AXHANDLE:Variant;var DEST:Variant;REGMODE:integer;LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer); stdcall;
var inifilename:string;
    Languageext:string;
    Ini: TIniFile;
    SHPIMPORT   :TShpImport;
    ShpImportPtr:TFarProc;
    curdoc:Variant;
    pdir  :PCHAR;
    plng  :PCHAR;
    WinHandle: Integer;
begin
   {$IFNDEF WMLT}
   if Data^.IsCanvas then begin
      WinHandle:=Application.Handle;
   end
   else begin
      WinHandle:=WingisMainForm.Handle;
   end;

   pdir:=stralloc(length(OSInfo.WingisDir)+1);
   strpcopy(pdir,OSInfo.WingisDir);

   // get language code
   inifilename:=OSInfo.WingisIniFileName;
   Ini:=TIniFile.Create(OSInfo.WingisIniFileName);
   LanguageExt:=Ini.ReadString('Settings','Language','044'); // default is english
   Ini.Free;
   plng:=stralloc(length(LanguageExt)+1);
   strpcopy(plng, LanguageExt);

   WinGISMainForm.ImpExpDll:=LoadLibrary('SHPIMP.DLL');
   WingisMainForm.CheckLibStatus(WingisMainForm.ImpExpDll,'SHPIMP.DLL','Import/Export');
   ShpImportPtr:=GetProcAddress(WinGISMainForm.ImpExpDll,'SHPIMPORT');
   if ShpImportPtr <> NIL then
   begin
      @SHPIMPORT:=ShpImportPtr;
      // call pxf-asc import DLL
      WinGISMainForm.CheckTDocumentIsCorrect;
      curdoc:=WinGISMainForm.GetCurrentTDocument;
      SHPIMPORT(pdir,WinGISMainForm.DXApp,curdoc,1, plng,
                WinHandle,
                WinGISMainForm.Top,
                WinGISMainForm.Left,
                WinGISMainForm.Width,
                WinGISMainForm.Height
               );
      WinGISMainForm.LastImpExpOperation:='SHPIMP';
   end
   else
   begin
      FreeLibrary(WinGISMainForm.ImpExpDll);
   end;
   strdispose(pdir);
   strdispose(plng);
   {$ENDIF}
end;

Procedure TMDIChild.FileExportSHPDX;
type TShpExport=procedure (DIR:PCHAR;var AXHANDLE:VARIANT;var SOURCE:VARIANT;REGMODE:integer; LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);stdcall;
var inifilename:string;
    Languageext:string;
    Ini: TIniFile;
    SHPEXPORT   :TShpExport;
    ShpExportPtr:TFarProc;
    curdoc:Variant;
    pdir  :PCHAR;
    plng  :PCHAR;
    WinHandle: Integer;
begin
   {$IFNDEF WMLT}
   if Data^.IsCanvas then begin
      WinHandle:=Application.Handle;
   end
   else begin
      WinHandle:=WingisMainForm.Handle;
   end;

   pdir:=stralloc(length(OSInfo.WingisDir)+1);
   strpcopy(pdir,OSInfo.WingisDir);

   // get language code
   inifilename:=OSInfo.WingisIniFileName;
   Ini:=TIniFile.Create(OSInfo.WingisIniFileName);
   LanguageExt:=Ini.ReadString('Settings','Language','044'); // default is english
   Ini.Free;
   plng:=stralloc(length(LanguageExt)+1);
   strpcopy(plng, LanguageExt);

   WinGISMainForm.ImpExpDll:=LoadLibrary('SHPEXP.DLL');
   WingisMainForm.CheckLibStatus(WingisMainForm.ImpExpDll,'SHPEXP.DLL','Import/Export');
   ShpExportPtr:=GetProcAddress(WinGISMainForm.ImpExpDll,'SHPEXPORT');
   if ShpExportPtr <> NIL then
   begin
      @SHPEXPORT:=ShpExportPtr;
      // call pxf-asc import DLL
      WinGISMainForm.CheckTDocumentIsCorrect;
      curdoc:=WinGISMainForm.GetCurrentTDocument;
      SHPEXPORT(pdir,WinGISMainForm.DXApp,curdoc,1, plng,
                WinHandle,
                WinGISMainForm.Top,
                WinGISMainForm.Left,
                WinGISMainForm.Width,
                WinGISMainForm.Height
               );
      WinGISMainForm.LastImpExpOperation:='SHPEXP';
   end
   else
   begin
      FreeLibrary(WinGISMainForm.ImpExpDll);
   end;
   strdispose(pdir);
   strdispose(plng);
   {$ENDIF}
end;

Procedure TMDIChild.FileImportAMPDX;
type TAmpImport=procedure (DIR:PCHAR;var AXHANDLE:Variant;var DEST:Variant;REGMODE:integer;LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer); stdcall;
var inifilename:string;
    Languageext:string;
    Ini: TIniFile;
    AMPIMPORT   :TAmpImport;
    AmpImportPtr:TFarProc;
    curdoc:Variant;
    pdir  :PCHAR;
    plng  :PCHAR;
    WinHandle: Integer;
begin
   {$IFNDEF WMLT}
   if Data^.IsCanvas then begin
      WinHandle:=Application.Handle;
   end
   else begin
      WinHandle:=WingisMainForm.Handle;
   end;

   pdir:=stralloc(length(OSInfo.WingisDir)+1);
   strpcopy(pdir,OSInfo.WingisDir);

   // get language code
   inifilename:=OSInfo.WingisIniFileName;
   Ini:=TIniFile.Create(OSInfo.WingisIniFileName);
   LanguageExt:=Ini.ReadString('Settings','Language','044'); // default is english
   Ini.Free;
   plng:=stralloc(length(LanguageExt)+1);
   strpcopy(plng, LanguageExt);

   WinGISMainForm.ImpExpDll:=LoadLibrary('AMPIMP.DLL');
   WingisMainForm.CheckLibStatus(WingisMainForm.ImpExpDll,'AMPIMP.DLL','Import/Export');
   AmpImportPtr:=GetProcAddress(WinGISMainForm.ImpExpDll,'AMPIMPORT');
   if AmpImportPtr <> NIL then
   begin
      @AMPIMPORT:=AmpImportPtr;
      // call pxf-asc import DLL
      WinGISMainForm.CheckTDocumentIsCorrect;
      curdoc:=WinGISMainForm.GetCurrentTDocument;
      AMPIMPORT(pdir,WinGISMainForm.DXApp,curdoc,1, plng,
                WinHandle,
                WinGISMainForm.Top,
                WinGISMainForm.Left,
                WinGISMainForm.Width,
                WinGISMainForm.Height
               );
      WinGISMainForm.LastImpExpOperation:='AMPIMP';
   end
   else
   begin
      FreeLibrary(WinGISMainForm.ImpExpDll);
   end;
   strdispose(pdir);
   strdispose(plng);
   {$ENDIF}
end;

Procedure TMDIChild.DDE_DoAmpImp;
type TCreateImp=procedure (DIR:PCHAR;var AXHANDLE:VARIANT;var DEST:VARIANT;REGMODE:integer; LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);stdcall;
var inifilename:string;
    Languageext:string;
    Ini: TIniFile;
    CREATEIMP      :TCreateImp;
    CreateImpExpPtr:TFarProc;
    curdoc:Variant;
    pdir  :PCHAR;
    plng  :PCHAR;
    WinHandle: Integer;
begin
   {$IFNDEF WMLT}
   if Data^.IsCanvas then begin
      WinHandle:=Application.Handle;
   end
   else begin
      WinHandle:=WingisMainForm.Handle;
   end;

   pdir:=stralloc(length(OSInfo.WingisDir)+1);
   strpcopy(pdir,OSInfo.WingisDir);

   // get language code
   inifilename:=OSInfo.WingisIniFileName;
   Ini:=TIniFile.Create(OSInfo.WingisIniFileName);
   LanguageExt:=Ini.ReadString('Settings','Language','044'); // default is english
   Ini.Free;
   plng:=stralloc(length(LanguageExt)+1);
   strpcopy(plng, LanguageExt);

   WinGISMainForm.ImpExpDll:=LoadLibrary('AMPIMP.DLL');
   WingisMainForm.CheckLibStatus(WingisMainForm.ImpExpDll,'AMPIMP.DLL','Import/Export');
   CreateImpExpPtr:=GetProcAddress(WinGISMainForm.ImpExpDll,'CREATEIMP');
   if CreateImpExpPtr <> NIL then
   begin
      @CREATEIMP:=CreateImpExpPtr;
      // call pxf-asc import DLL
      WinGISMainForm.CheckTDocumentIsCorrect;
      curdoc:=WinGISMainForm.GetCurrentTDocument;
      CREATEIMP(pdir,WinGISMainForm.DXApp,curdoc,1, plng,
                WinHandle,
                WinGISMainForm.Top,
                WinGISMainForm.Left,
                WinGISMainForm.Width,
                WinGISMainForm.Height
               );
      WinGISMainForm.LastImpExpOperation:='AMPIMP';
   end
   else
   begin
      FreeLibrary(WinGISMainForm.ImpExpDll);
   end;
   strdispose(pdir);
   strdispose(plng);
   {$ENDIF}
end;

// method is used to init asc-import
Procedure TMDIChild.DDE_DoAscImp;
type TCreateImp=procedure (DIR:PCHAR;var AXHANDLE:VARIANT;var DEST:VARIANT;REGMODE:integer; LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);stdcall;
var inifilename:string;
    Languageext:string;
    Ini: TIniFile;
    CREATEIMP      :TCreateImp;
    CreateImpExpPtr:TFarProc;
    curdoc:Variant;
    pdir  :PCHAR;
    plng  :PCHAR;
    WinHandle: Integer;
begin
   {$IFNDEF WMLT}

   if Data^.IsCanvas then begin
      WinHandle:=Application.Handle;
   end
   else begin
      WinHandle:=WingisMainForm.Handle;
   end;

   pdir:=stralloc(length(OSInfo.WingisDir)+1);
   strpcopy(pdir,OSInfo.WingisDir);

   // get language code
   inifilename:=OSInfo.WingisIniFileName;
   Ini:=TIniFile.Create(OSInfo.WingisIniFileName);
   LanguageExt:=Ini.ReadString('Settings','Language','044'); // default is english
   Ini.Free;
   plng:=stralloc(length(LanguageExt)+1);
   strpcopy(plng, LanguageExt);

   WinGISMainForm.ImpExpDll:=LoadLibrary('ASCIMP.DLL');
   WingisMainForm.CheckLibStatus(WingisMainForm.ImpExpDll,'ASCIMP.DLL','Import/Export');
   CreateImpExpPtr:=GetProcAddress(WinGISMainForm.ImpExpDll,'CREATEIMP');
   if CreateImpExpPtr <> NIL then
   begin
      @CREATEIMP:=CreateImpExpPtr;
      // call pxf-asc import DLL
      WinGISMainForm.CheckTDocumentIsCorrect;
      curdoc:=WinGISMainForm.GetCurrentTDocument;
      CREATEIMP(pdir,WinGISMainForm.DXApp,curdoc,1, plng,
                WinHandle,
                WinGISMainForm.Top,
                WinGISMainForm.Left,
                WinGISMainForm.Width,
                WinGISMainForm.Height
               );
      WinGISMainForm.LastImpExpOperation:='ASCIMP';
   end
   else
   begin
      FreeLibrary(WinGISMainForm.ImpExpDll);
   end;
   strdispose(pdir);
   strdispose(plng);
   {$ENDIF}
end;

// method is used to init dgn-import
Procedure TMDIChild.DDE_DoDgnImp;
type TCreateImp=procedure (DIR:PCHAR;var AXHANDLE:VARIANT;var DEST:VARIANT;REGMODE:integer; LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);stdcall;
var inifilename:string;
    Languageext:string;
    Ini: TIniFile;
    CREATEIMP      :TCreateImp;
    CreateImpExpPtr:TFarProc;
    curdoc:Variant;
    pdir  :PCHAR;
    plng  :PCHAR;
    WinHandle: Integer;
begin
   {$IFNDEF WMLT}
   if Data^.IsCanvas then begin
      WinHandle:=Application.Handle;
   end
   else begin
      WinHandle:=WingisMainForm.Handle;
   end;

   pdir:=stralloc(length(OSInfo.WingisDir)+1);
   strpcopy(pdir,OSInfo.WingisDir);

   // get language code
   inifilename:=OSInfo.WingisIniFileName;
   Ini:=TIniFile.Create(OSInfo.WingisIniFileName);
   LanguageExt:=Ini.ReadString('Settings','Language','044'); // default is english
   Ini.Free;
   plng:=stralloc(length(LanguageExt)+1);
   strpcopy(plng, LanguageExt);

   WinGISMainForm.ImpExpDll:=LoadLibrary('DGNIMP.DLL');
   WingisMainForm.CheckLibStatus(WingisMainForm.ImpExpDll,'DGNIMP.DLL','Import/Export');
   CreateImpExpPtr:=GetProcAddress(WinGISMainForm.ImpExpDll,'CREATEIMP');
   if CreateImpExpPtr <> NIL then
   begin
      @CREATEIMP:=CreateImpExpPtr;
      // call pxf-asc import DLL
      WinGISMainForm.CheckTDocumentIsCorrect;
      curdoc:=WinGISMainForm.GetCurrentTDocument;
      CREATEIMP(pdir,WinGISMainForm.DXApp,curdoc,1, plng,
                WinHandle,
                WinGISMainForm.Top,
                WinGISMainForm.Left,
                WinGISMainForm.Width,
                WinGISMainForm.Height
               );
      WinGISMainForm.LastImpExpOperation:='DGNIMP';
   end
   else
   begin
      FreeLibrary(WinGISMainForm.ImpExpDll);
   end;
   strdispose(pdir);
   strdispose(plng);
   {$ENDIF}
end;

// method is used to init dxf-import
Procedure TMDIChild.DDE_DoDxfImp;
type TCreateImp=procedure (DIR:PCHAR;var AXHANDLE:VARIANT;var DEST:VARIANT;REGMODE:integer; LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);stdcall;
var inifilename:string;
    Languageext:string;
    Ini: TIniFile;
    CREATEIMP      :TCreateImp;
    CreateImpExpPtr:TFarProc;
    curdoc:Variant;                 
    pdir  :PCHAR;                 
    plng  :PCHAR;
    WinHandle: Integer;
begin
   {$IFNDEF WMLT}
   if Data^.IsCanvas then begin
      WinHandle:=Application.Handle;
   end
   else begin
      WinHandle:=WingisMainForm.Handle;
   end;

   pdir:=stralloc(length(OSInfo.WingisDir)+1);
   strpcopy(pdir,OSInfo.WingisDir);

   // get language code
   inifilename:=OSInfo.WingisIniFileName;
   Ini:=TIniFile.Create(OSInfo.WingisIniFileName);
   LanguageExt:=Ini.ReadString('Settings','Language','044'); // default is english
   Ini.Free;
   plng:=stralloc(length(LanguageExt)+1);
   strpcopy(plng, LanguageExt);

   WinGISMainForm.ImpExpDll:=LoadLibrary('DXFIMP.DLL');
   WingisMainForm.CheckLibStatus(WingisMainForm.ImpExpDll,'DXFIMP.DLL','Import/Export');
   CreateImpExpPtr:=GetProcAddress(WinGISMainForm.ImpExpDll,'CREATEIMP');
   if CreateImpExpPtr <> NIL then
   begin
      @CREATEIMP:=CreateImpExpPtr;
      // call pxf-asc import DLL
      WinGISMainForm.CheckTDocumentIsCorrect;
      curdoc:=WinGISMainForm.GetCurrentTDocument;
      CREATEIMP(pdir,WinGISMainForm.DXApp,curdoc,1, plng,
                WinHandle,
                WinGISMainForm.Top,
                WinGISMainForm.Left,
                WinGISMainForm.Width,
                WinGISMainForm.Height
               );
      WinGISMainForm.LastImpExpOperation:='DXFIMP';
   end
   else
   begin
      FreeLibrary(WinGISMainForm.ImpExpDll);
   end;
   strdispose(pdir);
   strdispose(plng);
   {$ENDIF}
end;

// method is used to init E00-import
Procedure TMDIChild.DDE_DoE00Imp;
type TCreateImp=procedure (DIR:PCHAR;var AXHANDLE:VARIANT;var DEST:VARIANT;REGMODE:integer; LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);stdcall;
var inifilename:string;
    Languageext:string;
    Ini: TIniFile;
    CREATEIMP      :TCreateImp;
    CreateImpExpPtr:TFarProc;
    curdoc:Variant;
    pdir  :PCHAR;
    plng  :PCHAR;
    WinHandle: Integer;
begin
   {$IFNDEF WMLT}
   if Data^.IsCanvas then begin
      WinHandle:=Application.Handle;
   end
   else begin
      WinHandle:=WingisMainForm.Handle;
   end;

   pdir:=stralloc(length(OSInfo.WingisDir)+1);
   strpcopy(pdir,OSInfo.WingisDir);

   // get language code
   inifilename:=OSInfo.WingisIniFileName;
   Ini:=TIniFile.Create(OSInfo.WingisIniFileName);
   LanguageExt:=Ini.ReadString('Settings','Language','044'); // default is english
   Ini.Free;
   plng:=stralloc(length(LanguageExt)+1);
   strpcopy(plng, LanguageExt);

   WinGISMainForm.ImpExpDll:=LoadLibrary('E00IMP.DLL');
   WingisMainForm.CheckLibStatus(WingisMainForm.ImpExpDll,'E00IMP.DLL','Import/Export');
   CreateImpExpPtr:=GetProcAddress(WinGISMainForm.ImpExpDll,'CREATEIMP');
   if CreateImpExpPtr <> NIL then
   begin
      @CREATEIMP:=CreateImpExpPtr;
      // call pxf-asc import DLL
      WinGISMainForm.CheckTDocumentIsCorrect;
      curdoc:=WinGISMainForm.GetCurrentTDocument;
      CREATEIMP(pdir,WinGISMainForm.DXApp,curdoc,1, plng,
                WinHandle,
                WinGISMainForm.Top,
                WinGISMainForm.Left,
                WinGISMainForm.Width,
                WinGISMainForm.Height
               );
      WinGISMainForm.LastImpExpOperation:='E00IMP';
   end
   else
   begin
      FreeLibrary(WinGISMainForm.ImpExpDll);
   end;
   strdispose(pdir);
   strdispose(plng);
   {$ENDIF}
end;

// method is used to init MIF-import
Procedure TMDIChild.DDE_DoMifImp;
type TCreateImp=procedure (DIR:PCHAR;var AXHANDLE:VARIANT;var DEST:VARIANT;REGMODE:integer; LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);stdcall;
var inifilename:string;
    Languageext:string;
    Ini: TIniFile;
    CREATEIMP      :TCreateImp;
    CreateImpExpPtr:TFarProc;
    curdoc:Variant;
    pdir  :PCHAR;
    plng  :PCHAR;
    WinHandle: Integer;
begin
   {$IFNDEF WMLT}
   if Data^.IsCanvas then begin
      WinHandle:=Application.Handle;
   end
   else begin
      WinHandle:=WingisMainForm.Handle;
   end;

   pdir:=stralloc(length(OSInfo.WingisDir)+1);
   strpcopy(pdir,OSInfo.WingisDir);

   // get language code
   inifilename:=OSInfo.WingisIniFileName;
   Ini:=TIniFile.Create(OSInfo.WingisIniFileName);
   LanguageExt:=Ini.ReadString('Settings','Language','044'); // default is english
   Ini.Free;
   plng:=stralloc(length(LanguageExt)+1);
   strpcopy(plng, LanguageExt);

   WinGISMainForm.ImpExpDll:=LoadLibrary('MIFIMP.DLL');
   WingisMainForm.CheckLibStatus(WingisMainForm.ImpExpDll,'MIFIMP.DLL','Import/Export');
   CreateImpExpPtr:=GetProcAddress(WinGISMainForm.ImpExpDll,'CREATEIMP');
   if CreateImpExpPtr <> NIL then
   begin
      @CREATEIMP:=CreateImpExpPtr;
      // call pxf-asc import DLL
      WinGISMainForm.CheckTDocumentIsCorrect;
      curdoc:=WinGISMainForm.GetCurrentTDocument;
      CREATEIMP(pdir,WinGISMainForm.DXApp,curdoc,1, plng,
                WinHandle,
                WinGISMainForm.Top,
                WinGISMainForm.Left,
                WinGISMainForm.Width,
                WinGISMainForm.Height
               );
      WinGISMainForm.LastImpExpOperation:='MIFIMP';
   end
   else
   begin
      FreeLibrary(WinGISMainForm.ImpExpDll);
   end;
   strdispose(pdir);
   strdispose(plng);
   {$ENDIF}
end;

// method is used to init SHP-import
Procedure TMDIChild.DDE_DoShpImp;
type TCreateImp=procedure (DIR:PCHAR;var AXHANDLE:VARIANT;var DEST:VARIANT;REGMODE:integer; LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);stdcall;
var inifilename:string;
    Languageext:string;
    Ini: TIniFile;
    CREATEIMP      :TCreateImp;
    CreateImpExpPtr:TFarProc;
    curdoc:Variant;
    pdir  :PCHAR;
    plng  :PCHAR;
    WinHandle: Integer;
begin
   {$IFNDEF WMLT}
   if Data^.IsCanvas then begin
      WinHandle:=Application.Handle;
   end
   else begin
      WinHandle:=WingisMainForm.Handle;
   end;

   pdir:=stralloc(length(OSInfo.WingisDir)+1);
   strpcopy(pdir,OSInfo.WingisDir);

   // get language code
   inifilename:=OSInfo.WingisIniFileName;
   Ini:=TIniFile.Create(OSInfo.WingisIniFileName);
   LanguageExt:=Ini.ReadString('Settings','Language','044'); // default is english
   Ini.Free;
   plng:=stralloc(length(LanguageExt)+1);
   strpcopy(plng, LanguageExt);

   WinGISMainForm.ImpExpDll:=LoadLibrary('SHPIMP.DLL');
   WingisMainForm.CheckLibStatus(WingisMainForm.ImpExpDll,'SHPIMP.DLL','Import/Export');
   CreateImpExpPtr:=GetProcAddress(WinGISMainForm.ImpExpDll,'CREATEIMP');
   if CreateImpExpPtr <> NIL then
   begin
      @CREATEIMP:=CreateImpExpPtr;
      // call pxf-asc import DLL
      WinGISMainForm.CheckTDocumentIsCorrect;
      curdoc:=WinGISMainForm.GetCurrentTDocument;
      CREATEIMP(pdir,WinGISMainForm.DXApp,curdoc,1, plng,
                WinHandle,
                WinGISMainForm.Top,
                WinGISMainForm.Left,
                WinGISMainForm.Width,
                WinGISMainForm.Height
               );
      WinGISMainForm.LastImpExpOperation:='SHPIMP';
   end
   else
   begin
      FreeLibrary(WinGISMainForm.ImpExpDll);
   end;
   strdispose(pdir);
   strdispose(plng);
   {$ENDIF}
end;

// method to setup amp-import
Procedure TMDIChild.DDE_DoAmpImpSettings(DDECmd:string);
type TSetAmpImpSettings=procedure (IMPPROJ:PCHAR; IMPPROJMODE:INTEGER; SHOWMODE:INTEGER);stdcall;
var  SETAMPIMPSETTINGS   :TSetAmpImpSettings;
     SetAmpImpSettingsPtr:TFarProc;
     aDDECmd:string;
     ImpTarget:string;
     pImpTarget:pchar;
     ImpMode:integer;
     ShowMode:integer;
     dummy:string;
     count:integer;
begin
   aDDECmd:=DDECmd;
   delete(aDDECmd,1,19); // delete [DOAMPIMPSETTINGS][
   ImpTarget:=copy(aDDECmd,1,Pos(',',aDDECmd)-1); // get the project that should be imported
   delete(aDDECmd,1,Pos(',',aDDECmd));
   if Pos(',',aDDECmd) <> 0 then
   begin
      dummy:=copy(aDDECmd,1,Pos(',',aDDECmd)-1);
      val (dummy, ImpMode, count); if (count <> 0) then ImpMode:=0;
      delete(aDDECmd,1,Pos(',',aDDECmd));
      dummy:=copy(aDDECmd,1,Pos(']',aDDECmd)-1);
      val (dummy, ShowMode, count); if (count <> 0) then ShowMode:=0;
   end
   else
   begin
      dummy:=copy(aDDECmd,1,Pos(']',aDDECmd)-1);
      val (dummy, ImpMode, count); if (count <> 0) then ImpMode:=0;
      ShowMode:=0; // no showmode set -> Show the window
   end;
   SetAmpImpSettingsPtr:=GetProcAddress(WinGISMainForm.ImpExpDll,'SETAMPIMPSETTINGS');
   if SetAmpImpSettingsPtr <> nil then
   begin
      @SETAMPIMPSETTINGS:=SetAmpImpSettingsPtr;
      pImpTarget:=stralloc(length(ImpTarget)+1); strpcopy(pImpTarget, ImpTarget);
      SETAMPIMPSETTINGS(pImpTarget, ImpMode, ShowMode);
      strdispose(pImpTarget);
   end
   else
      FreeLibrary(WinGISMainForm.ImpExpDll);
end;

// method is used to do settings for asc-import
Procedure TMDIChild.DDE_DoAscImpSettings(DDECmd:string);
type TSetAscImpSettings=procedure (IMPMODE:INTEGER; IMPADFFILE:PCHAR; IMPSYMFILE:PCHAR);stdcall;
var  SETASCIMPSETTINGS:TSetAscImpSettings;
     SetAscImpSettingsPtr:TFarProc;
     aDDECmd:string;
     ImpMode:integer;
     dummy:string;
     count:integer;
     ImpAdfFilename:string;
     ImpSymFilename:string;
     pImpAdfFilename:pchar;
     pImpSymFilename:pchar;
begin
   aDDECmd:=DDECmd;
   delete(aDDECmd,1,19); // delete [DOASCIMPSETTINGS][
   dummy:=copy(aDDECmd,1,Pos(',',aDDECmd)-1);
   delete(aDDECmd,1,Pos(',',aDDECmd));
   val(dummy,ImpMode,count); if count <> 0 then ImpMode:=0;
   ImpAdfFilename:=copy(aDDECmd,1,Pos(',',aDDECmd)-1);
   delete(aDDECmd,1,Pos(',',aDDECmd));
   ImpSymFilename:=copy(aDDECmd,1,Pos(']',aDDECmd)-1);
   SetAscImpSettingsPtr:=GetProcAddress(WinGISMainForm.ImpExpDll,'SETASCIMPSETTINGS');
   if SetAscImpSettingsPtr <> NIL then
   begin
      @SETASCIMPSETTINGS:=SetAscImpSettingsPtr;
      pImpAdfFilename:=stralloc(length(ImpAdfFilename)+1); strpcopy(pImpAdfFilename, ImpAdfFilename);
      pImpSymFilename:=stralloc(length(ImpSymFilename)+1); strpcopy(pImpSymFilename, ImpSymFilename);
      SETASCIMPSETTINGS(ImpMode, pImpAdfFilename, pImpSymFilename);
      strdispose(pImpAdfFilename);
      strdispose(pImpSymFilename);
   end
   else
      FreeLibrary(WinGISMainForm.ImpExpDll);
end;

// method is used to do settings for dxf-import
Procedure TMDIChild.DDE_DoDxfImpSettings(DDECmd:string);
type TSetDxfImpSettings=procedure (DKMMODE:INTEGER; SELMODE:INTEGER; UMLAUTMODE:INTEGER; HEADERMODE:INTEGER; HEADERNAME:PCHAR;ADDITIONALOPTIONS:PCHAR);stdcall;
var  SETDXFIMPSETTINGS:TSetDxfImpSettings;
     SetDxfImpSettingsPtr:TFarProc;
     aDDECmd:string;
     SelMode:integer;
     UmlautMode:integer;
     HeaderMode:integer;
     dummy:string;
     count:integer;
     HeaderFilename:string;
     pHeaderFilename:pchar;
     AddSettings:string;
     pAddSettings   :pchar;
begin
   aDDECmd:=DDECmd;
   delete(aDDECmd,1,19); // delete [DODXFIMPSETTINGS][
   dummy:=copy(aDDECmd,1,Pos(',',aDDECmd)-1);
   delete(aDDECmd,1,Pos(',',aDDECmd));
   dummy:=copy(aDDECmd,1,Pos(',',aDDECmd)-1);
   delete(aDDECmd,1,Pos(',',aDDECmd));
   val(dummy,SelMode,count); if count <> 0 then SelMode:=0;
   dummy:=copy(aDDECmd,1,Pos(',',aDDECmd)-1);
   delete(aDDECmd,1,Pos(',',aDDECmd));
   val(dummy,UmlautMode,count); if count <> 0 then UmlautMode:=0;
   dummy:=copy(aDDECmd,1,Pos(',',aDDECmd)-1);
   delete(aDDECmd,1,Pos(',',aDDECmd));
   val(dummy,HeaderMode,count); if count <> 0 then HeaderMode:=0;
   HeaderFilename:=copy(aDDECmd,1,Pos(']',aDDECmd)-1);
   delete(aDDECmd,1,Pos(']',aDDECmd));
   // check if additional settings exist
   if (Pos('[', aDDECmd) <> 0) then
   begin
      delete(aDDECmd,1,Pos('[',aDDECmd));
      AddSettings:=copy(aDDECmd,1,Pos(']',aDDECmd)-1);
   end;
   SetDxfImpSettingsPtr:=GetProcAddress(WinGISMainForm.ImpExpDll,'SETDXFIMPSETTINGS');
   if SetDxfImpSettingsPtr <> NIL then
   begin
      @SETDXFIMPSETTINGS:=SetDxfImpSettingsPtr;
      pHeaderFilename:=stralloc(length(HeaderFilename)+1); strpcopy(pHeaderFilename,HeaderFilename);
      pAddSettings:=stralloc(length(AddSettings)+1); strpcopy(pAddSettings, AddSettings);
      SETDXFIMPSETTINGS(1, SelMode, UmlautMode, HeaderMode, pHeaderFilename, pAddSettings);
      strdispose(pHeaderFilename); strdispose(pAddSettings);
   end
   else
      FreeLibrary(WinGISMainForm.ImpExpDll);
end;

// method is used to do settings for E00-import
Procedure TMDIChild.DDE_DoE00ImpSettings(DDECmd:string);
type TSetE00ImpSettings=procedure (IMPMODE:INTEGER);stdcall;
var  SETE00IMPSETTINGS:TSetE00ImpSettings;
     SetE00ImpSettingsPtr:TFarProc;
     aDDECmd:string;
     ImpMode:integer;
     dummy:string;
     count:integer;
begin
   aDDECmd:=DDECmd;
   delete(aDDECmd,1,19); // delete [DOE00IMPSETTINGS][
   dummy:=copy(aDDECmd,1,Pos(']',aDDECmd)-1);
   val(dummy,ImpMode,count); if count <> 0 then ImpMode:=0;
   SetE00ImpSettingsPtr:=GetProcAddress(WinGISMainForm.ImpExpDll,'SETE00IMPSETTINGS');
   if SetE00ImpSettingsPtr <> NIL then
   begin
      @SETE00IMPSETTINGS:=SetE00ImpSettingsPtr;
      SETE00IMPSETTINGS(ImpMode);
   end
   else
      FreeLibrary(WinGISMainForm.ImpExpDll);
end;

// method is used to do settings for MIF-import
Procedure TMDIChild.DDE_DoMifImpSettings(DDECmd:string);
type TSetMifImpSettings=procedure (IMPMODE:INTEGER);stdcall;
var  SETMIFIMPSETTINGS:TSetMifImpSettings;
     SetMifImpSettingsPtr:TFarProc;
     aDDECmd:string;
     ImpMode:integer;
     dummy:string;
     count:integer;
begin
   aDDECmd:=DDECmd;
   delete(aDDECmd,1,19); // delete [DOMIFIMPSETTINGS][
   dummy:=copy(aDDECmd,1,Pos(']',aDDECmd)-1);
   val(dummy,ImpMode,count); if count <> 0 then ImpMode:=0;
   SetMifImpSettingsPtr:=GetProcAddress(WinGISMainForm.ImpExpDll,'SETMIFIMPSETTINGS');
   if SetMifImpSettingsPtr <> NIL then
   begin
      @SETMIFIMPSETTINGS:=SetMifImpSettingsPtr;
      SETMIFIMPSETTINGS(ImpMode);
   end
   else
      FreeLibrary(WinGISMainForm.ImpExpDll);
end;

// method is used to do settings for SHP-import
Procedure TMDIChild.DDE_DoShpImpSettings(DDECmd:string);
type TSetShpImpSettings=procedure (APRMODE:INTEGER; APRNAME:PCHAR);stdcall;
var  SETSHPIMPSETTINGS:TSetShpImpSettings;
     SetShpImpSettingsPtr:TFarProc;
     aDDECmd:string;
     ImpMode:integer;
     dummy:string;
     count:integer;
     AprFilename:string;
     pAprFilename:pchar;
begin
   aDDECmd:=DDECmd;
   delete(aDDECmd,1,19); // delete [DOMIFIMPSETTINGS][
   dummy:=copy(aDDECmd,1,Pos(',',aDDECmd)-1);
   val(dummy,ImpMode,count); if count <> 0 then ImpMode:=0;
   delete(aDDECmd,1,Pos(',',aDDEcmd));
   AprFilename:=copy(aDDECmd,1,Pos(']',aDDECmd)-1);
   SetShpImpSettingsPtr:=GetProcAddress(WinGISMainForm.ImpExpDll,'SETSHPIMPSETTINGS');
   if SetShpImpSettingsPtr <> NIL then
   begin
      @SETSHPIMPSETTINGS:=SetShpImpSettingsPtr;
      pAprfilename:=stralloc(length(AprFilename)+1); strpcopy(pAprFilename, AprFilename);
      SETSHPIMPSETTINGS(ImpMode,pAprFilename);
      strdispose(pAprFilename);
   end
   else
      FreeLibrary(WinGISMainForm.ImpExpDll);
end;

// method is used to add a file that should be imported
Procedure TMDIChild.DDE_DoAddImpFile(DDECmd:string);
type TAddImpFile=procedure (AFILE:PCHAR);stdcall;
var  ADDIMPFILE    :TAddImpFile;
     AddImpFilePtr :TFarProc;
     Filename:string;
     pFilename:pchar;
     aDDECmd:string;
begin
   aDDECmd:=DDECmd;
   delete(aDDECmd,1,15); // delete [DOADDIMPFILE][
   Filename:=copy(aDDECmd,1,Pos(']',aDDECmd)-1);
   AddImpFilePtr:=GetProcAddress(WinGISMainForm.ImpExpDll,'ADDIMPFILE');
   if AddImpFilePtr <> NIL then
   begin
      @ADDIMPFILE:=AddImpFilePtr;
      pFilename:=stralloc(length(Filename)+1); strpcopy(pFilename, Filename);
      ADDIMPFILE(pFilename);
      strdispose(pFilename);
   end
   else
      FreeLibrary(WinGISMainForm.ImpExpDll);
end;

// method to add layer that should be imported
Procedure TMDIChild.DDE_DoAddImpLayer(DDECmd:string);
type TAddImpLayer=procedure (ALAYER:PCHAR);stdcall;
var  ADDIMPLAYER   :TAddImpLayer;
     AddImpLayerPtr:TFarProc;
     aDDECmd:string;
     Layer:string;
     pLayer:Pchar;
begin
   aDDECmd:=DDECmd;
   delete(aDDECmd,1, 16); // delete [DOADDIMPLAYER][
   Layer:=copy(aDDECmd,1,Pos(']',aDDECmd)-1);
   AddImpLayerPtr:=GetProcAddress(WinGISMainForm.ImpExpDLL,'ADDIMPLAYER');
   if AddImpLayerPtr <> NIL then
   begin
      @ADDIMPLAYER:=AddImpLayerPtr;
      pLayer:=stralloc(length(Layer)+1); strpcopy(pLayer,Layer);
      ADDIMPLAYER(pLayer);
      strdispose(pLayer);
   end
   else
      FreeLibrary(WinGISMainForm.ImpExpDll);
end;

// method to initiate AMP-Export via DDE
procedure TMDIChild.DDE_DoAmpExp;
type TCreateExp=procedure (DIR:PCHAR;var AXHANDLE:VARIANT;var SOURCE:VARIANT;REGMODE:integer; LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);stdcall;
var inifilename:string;
    Languageext:string;
    Ini: TIniFile;
    CREATEEXP   :TCreateExp;
    CreateExpPtr:TFarProc;
    curdoc:Variant;
    pdir  :PCHAR;
    plng  :PCHAR;
    WinHandle: Integer;
begin
   {$IFNDEF WMLT}
   if Data^.IsCanvas then begin
      WinHandle:=Application.Handle;
   end
   else begin
      WinHandle:=WingisMainForm.Handle;
   end;

   pdir:=stralloc(length(OSInfo.WingisDir)+1);
   strpcopy(pdir,OSInfo.WingisDir);

   // get language code
   inifilename:=OSInfo.WingisIniFileName;
   Ini:=TIniFile.Create(OSInfo.WingisIniFileName);
   LanguageExt:=Ini.ReadString('Settings','Language','044'); // default is english
   Ini.Free;
   plng:=stralloc(length(LanguageExt)+1);
   strpcopy(plng, LanguageExt);

   WinGISMainForm.ImpExpDll:=LoadLibrary('AMPEXP.DLL');
   WingisMainForm.CheckLibStatus(WingisMainForm.ImpExpDll,'AMPEXP.DLL','Import/Export');
   CreateExpPtr:=GetProcAddress(WinGISMainForm.ImpExpDll,'CREATEEXP');
   if CreateExpPtr <> NIL then
   begin
      @CREATEEXP:=CreateExpPtr;
      // call pxf-asc import DLL
      WinGISMainForm.CheckTDocumentIsCorrect;
      curdoc:=WinGISMainForm.GetCurrentTDocument;
      CREATEEXP(pdir,WinGISMainForm.DXApp,curdoc,1, plng,
                   WinHandle,
                   WinGISMainForm.Top,
                   WinGISMainForm.Left,
                   WinGISMainForm.Width,
                   WinGISMainForm.Height
                  );
      WinGISMainForm.LastImpExpOperation:='AMPEXP';
   end
   else
   begin
      FreeLibrary(WinGISMainForm.ImpExpDll);
   end;
   strdispose(pdir);
   strdispose(plng);
   {$ENDIF}
end;

// method to initiate ASC-Export via DDE
procedure TMDIChild.DDE_DoAscExp;
type TCreateExp=procedure (DIR:PCHAR;var AXHANDLE:VARIANT;var SOURCE:VARIANT;REGMODE:integer; LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);stdcall;
var inifilename:string;
    Languageext:string;
    Ini: TIniFile;
    CREATEEXP   :TCreateExp;
    CreateExpPtr:TFarProc;
    curdoc:Variant;
    pdir  :PCHAR;
    plng  :PCHAR;
    WinHandle: Integer;
begin
   {$IFNDEF WMLT}
   if Data^.IsCanvas then begin
      WinHandle:=Application.Handle;
   end
   else begin
      WinHandle:=WingisMainForm.Handle;
   end;

   pdir:=stralloc(length(OSInfo.WingisDir)+1);
   strpcopy(pdir,OSInfo.WingisDir);

   // get language code
   inifilename:=OSInfo.WingisIniFileName;
   Ini:=TIniFile.Create(OSInfo.WingisIniFileName);
   LanguageExt:=Ini.ReadString('Settings','Language','044'); // default is english
   Ini.Free;
   plng:=stralloc(length(LanguageExt)+1);
   strpcopy(plng, LanguageExt);

   WinGISMainForm.ImpExpDll:=LoadLibrary('ASCEXP.DLL');
   WingisMainForm.CheckLibStatus(WingisMainForm.ImpExpDll,'ASCEXP.DLL','Import/Export');
   CreateExpPtr:=GetProcAddress(WinGISMainForm.ImpExpDll,'CREATEEXP');
   if CreateExpPtr <> NIL then
   begin
      @CREATEEXP:=CreateExpPtr;
      // call pxf-asc import DLL
      WinGISMainForm.CheckTDocumentIsCorrect;
      curdoc:=WinGISMainForm.GetCurrentTDocument;
      CREATEEXP(pdir,WinGISMainForm.DXApp,curdoc,1, plng,
                WinHandle,
                WinGISMainForm.Top,
                WinGISMainForm.Left,
                WinGISMainForm.Width,
                WinGISMainForm.Height
               );
      WinGISMainForm.LastImpExpOperation:='ASCEXP';
   end
   else
   begin
      FreeLibrary(WinGISMainForm.ImpExpDll);
   end;
   strdispose(pdir);
   strdispose(plng);
   {$ENDIF}
end;

// method to initiate DXF-Export via DDE
procedure TMDIChild.DDE_DoDxfExp;
type TCreateExp=procedure (DIR:PCHAR;var AXHANDLE:VARIANT;var SOURCE:VARIANT;REGMODE:integer; LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);stdcall;
var inifilename:string;
    Languageext:string;
    Ini: TIniFile;
    CREATEEXP   :TCreateExp;
    CreateExpPtr:TFarProc;
    curdoc:Variant;
    pdir  :PCHAR;
    plng  :PCHAR;
    WinHandle: Integer;
begin
   {$IFNDEF WMLT}
   if Data^.IsCanvas then begin
      WinHandle:=Application.Handle;
   end
   else begin
      WinHandle:=WingisMainForm.Handle;
   end;

   pdir:=stralloc(length(OSInfo.WingisDir)+1);
   strpcopy(pdir,OSInfo.WingisDir);

   // get language code
   inifilename:=OSInfo.WingisIniFileName;
   Ini:=TIniFile.Create(OSInfo.WingisIniFileName);
   LanguageExt:=Ini.ReadString('Settings','Language','044'); // default is english
   Ini.Free;
   plng:=stralloc(length(LanguageExt)+1);
   strpcopy(plng, LanguageExt);

   WinGISMainForm.ImpExpDll:=LoadLibrary('DXFEXP.DLL');
   WingisMainForm.CheckLibStatus(WingisMainForm.ImpExpDll,'DXFEXP.DLL','Import/Export');
   CreateExpPtr:=GetProcAddress(WinGISMainForm.ImpExpDll,'CREATEEXP');
   if CreateExpPtr <> NIL then
   begin
      @CREATEEXP:=CreateExpPtr;
      // call pxf-asc import DLL
      WinGISMainForm.CheckTDocumentIsCorrect;
      curdoc:=WinGISMainForm.GetCurrentTDocument;
      CREATEEXP(pdir,WinGISMainForm.DXApp,curdoc,1, plng,
                WinHandle,
                WinGISMainForm.Top,
                WinGISMainForm.Left,
                WinGISMainForm.Width,
                WinGISMainForm.Height
               );
      WinGISMainForm.LastImpExpOperation:='DXFEXP';
   end
   else
   begin
      FreeLibrary(WinGISMainForm.ImpExpDll);
   end;
   strdispose(pdir);
   strdispose(plng);
   {$ENDIF}
end;

// method to initiate E00-Export via DDE
procedure TMDIChild.DDE_DoE00Exp;
type TCreateExp=procedure (DIR:PCHAR;var AXHANDLE:VARIANT;var SOURCE:VARIANT;REGMODE:integer; LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);stdcall;
var inifilename:string;
    Languageext:string;
    Ini: TIniFile;
    CREATEEXP   :TCreateExp;
    CreateExpPtr:TFarProc;
    curdoc:Variant;
    pdir  :PCHAR;
    plng  :PCHAR;
    WinHandle: Integer;
begin
   {$IFNDEF WMLT}
   if Data^.IsCanvas then begin
      WinHandle:=Application.Handle;
   end
   else begin
      WinHandle:=WingisMainForm.Handle;
   end;

   pdir:=stralloc(length(OSInfo.WingisDir)+1);
   strpcopy(pdir,OSInfo.WingisDir);

   // get language code
   inifilename:=OSInfo.WingisIniFileName;
   Ini:=TIniFile.Create(OSInfo.WingisIniFileName);
   LanguageExt:=Ini.ReadString('Settings','Language','044'); // default is english
   Ini.Free;
   plng:=stralloc(length(LanguageExt)+1);
   strpcopy(plng, LanguageExt);

   WinGISMainForm.ImpExpDll:=LoadLibrary('E00EXP.DLL');
   WingisMainForm.CheckLibStatus(WingisMainForm.ImpExpDll,'E00EXP.DLL','Import/Export');
   CreateExpPtr:=GetProcAddress(WinGISMainForm.ImpExpDll,'CREATEEXP');
   if CreateExpPtr <> NIL then
   begin
      @CREATEEXP:=CreateExpPtr;
      // call pxf-asc import DLL
      WinGISMainForm.CheckTDocumentIsCorrect;
      curdoc:=WinGISMainForm.GetCurrentTDocument;
      CREATEEXP(pdir,WinGISMainForm.DXApp,curdoc,1, plng,
                WinHandle,
                WinGISMainForm.Top,
                WinGISMainForm.Left,
                WinGISMainForm.Width,
                WinGISMainForm.Height
               );
      WinGISMainForm.LastImpExpOperation:='E00EXP';
   end
   else
   begin
      FreeLibrary(WinGISMainForm.ImpExpDll);
   end;
   strdispose(pdir);
   strdispose(plng);
   {$ENDIF}
end;

// method to initiate MIF-Export via DDE
procedure TMDIChild.DDE_DoMifExp;
type TCreateExp=procedure (DIR:PCHAR;var AXHANDLE:VARIANT;var SOURCE:VARIANT;REGMODE:integer; LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);stdcall;
var inifilename:string;
    Languageext:string;
    Ini: TIniFile;
    CREATEEXP   :TCreateExp;
    CreateExpPtr:TFarProc;
    curdoc:Variant;
    pdir  :PCHAR;
    plng  :PCHAR;
    WinHandle: Integer;
begin
   {$IFNDEF WMLT}
   if Data^.IsCanvas then begin
      WinHandle:=Application.Handle;
   end
   else begin
      WinHandle:=WingisMainForm.Handle;
   end;

   pdir:=stralloc(length(OSInfo.WingisDir)+1);
   strpcopy(pdir,OSInfo.WingisDir);

   // get language code
   inifilename:=OSInfo.WingisIniFileName;
   Ini:=TIniFile.Create(OSInfo.WingisIniFileName);
   LanguageExt:=Ini.ReadString('Settings','Language','044'); // default is english
   Ini.Free;
   plng:=stralloc(length(LanguageExt)+1);
   strpcopy(plng, LanguageExt);

   WinGISMainForm.ImpExpDll:=LoadLibrary('MIFEXP.DLL');
   WingisMainForm.CheckLibStatus(WingisMainForm.ImpExpDll,'MIFEXP.DLL','Import/Export');
   CreateExpPtr:=GetProcAddress(WinGISMainForm.ImpExpDll,'CREATEEXP');
   if CreateExpPtr <> NIL then
   begin
      @CREATEEXP:=CreateExpPtr;
      // call pxf-asc import DLL
      WinGISMainForm.CheckTDocumentIsCorrect;
      curdoc:=WinGISMainForm.GetCurrentTDocument;
      CREATEEXP(pdir,WinGISMainForm.DXApp,curdoc,1, plng,
                WinHandle,
                WinGISMainForm.Top,
                WinGISMainForm.Left,
                WinGISMainForm.Width,
                WinGISMainForm.Height
               );
      WinGISMainForm.LastImpExpOperation:='MIFEXP';
   end
   else
   begin
      FreeLibrary(WinGISMainForm.ImpExpDll);
   end;
   strdispose(pdir);
   strdispose(plng);
   {$ENDIF}
end;

// method to initiate SHP-Export via DDE
procedure TMDIChild.DDE_DoShpExp;
type TCreateExp=procedure (DIR:PCHAR;var AXHANDLE:VARIANT;var SOURCE:VARIANT;REGMODE:integer; LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);stdcall;
var inifilename:string;
    Languageext:string;
    Ini: TIniFile;
    CREATEEXP   :TCreateExp;
    CreateExpPtr:TFarProc;
    curdoc:Variant;
    pdir  :PCHAR;
    plng  :PCHAR;
    WinHandle: Integer;
begin
   {$IFNDEF WMLT}
   if Data^.IsCanvas then begin
      WinHandle:=Application.Handle;
   end
   else begin
      WinHandle:=WingisMainForm.Handle;
   end;

   pdir:=stralloc(length(OSInfo.WingisDir)+1);
   strpcopy(pdir,OSInfo.WingisDir);

   // get language code
   inifilename:=OSInfo.WingisIniFileName;
   Ini:=TIniFile.Create(OSInfo.WingisIniFileName);
   LanguageExt:=Ini.ReadString('Settings','Language','044'); // default is english
   Ini.Free;
   plng:=stralloc(length(LanguageExt)+1);
   strpcopy(plng, LanguageExt);

   WinGISMainForm.ImpExpDll:=LoadLibrary('SHPEXP.DLL');
   WingisMainForm.CheckLibStatus(WingisMainForm.ImpExpDll,'SHPEXP.DLL','Import/Export');
   CreateExpPtr:=GetProcAddress(WinGISMainForm.ImpExpDll,'CREATEEXP');
   if CreateExpPtr <> NIL then
   begin
      @CREATEEXP:=CreateExpPtr;
      // call pxf-asc import DLL
      WinGISMainForm.CheckTDocumentIsCorrect;
      curdoc:=WinGISMainForm.GetCurrentTDocument;
      CREATEEXP(pdir,WinGISMainForm.DXApp,curdoc,1, plng,
                WinHandle,
                WinGISMainForm.Top,
                WinGISMainForm.Left,
                WinGISMainForm.Width,
                WinGISMainForm.Height
               );
      WinGISMainForm.LastImpExpOperation:='SHPEXP';
   end
   else
   begin
      FreeLibrary(WinGISMainForm.ImpExpDll);
   end;
   strdispose(pdir);
   strdispose(plng);
   {$ENDIF}
end;

// method to do base settings for DDE initiated export
procedure  TMDIChild.DDE_DoExpSettings(DDECmd:string);
type TSetExpSettings=procedure (EXPPROJ:PCHAR; EXPPROJMODE:INTEGER);stdcall;
var aDDELine:string;
    ExpTarget:string;
    pExpTarget :PCHAR;
    ExpModeStr:string;
    ExpMode   :integer;
    count     :integer;
    SETEXPSETTINGS :TSetExpSettings;
    SetExpSettingsPtr:TFarProc;
begin
   aDDELine:=DDECmd;
   delete(aDDELine,1,16); // delete [DOEXPSETTINGS][
   ExpTarget:=copy(aDDELine,1, Pos(',',aDDELine)-1); // get export target
   Delete(aDDELine,1,Pos(',',aDDELine)); // delete till ','
   ExpModeStr:=copy(aDDELine,1,Pos(']',aDDELine)-1);
   val(ExpModeStr, ExpMode, count); if (count <> 0) then ExpMode:=0;

   SetExpSettingsPtr:=GetProcAddress(WinGISMainForm.ImpExpDll,'SETEXPSETTINGS');
   if SetExpSettingsPtr <> NIL then
   begin
      pExpTarget:=stralloc(length(ExpTarget)+1); strpcopy(pExpTarget, ExpTarget);
      @SETEXPSETTINGS:=SetExpSettingsPtr;
      SETEXPSETTINGS(pExpTarget, ExpMode);
      strdispose(pExpTarget);
   end
   else
      FreeLibrary(WinGISMainForm.ImpExpDll);
end;

// method to add layer that should be exported
procedure TMDIChild.DDE_DoAddExpLayer(DDECmd:string);
type TAddExpLayer=procedure(ALAYER:PCHAR);stdcall;
var aDDELine:string;
    aLayer  :string;
    pLayer  :PCHAR;
    ADDEXPLAYER :TAddExpLayer;
    AddExpLayerPtr:TFarProc;
begin
   aDDELine:=DDECmd;
   delete(aDDELine,1,16); // delete [DOADDEXPLAYER][
   aLayer:=copy(aDDELine,1,Pos(']',aDDELine)-1);
   AddExpLayerPtr:=GetProcAddress(WinGISMainForm.ImpExpDll,'ADDEXPLAYER');
   if AddExpLayerPtr <> nil then
   begin
      pLayer:=stralloc(length(aLayer)+1); strpcopy(pLayer,aLayer);
      @ADDEXPLAYER:=AddExpLayerPtr;
      ADDEXPLAYER(pLayer);
      strdispose(pLayer);
   end
   else
      FreeLibrary(WinGISMainForm.ImpExpDll);
end;

// procedure is used to set database settings
procedure TMDIChild.DDE_DoSetDBSettings(DDECmd:string);
type TSetDBSettings=procedure(SOURCEDBMODE:INTEGER; SOURCEDBNAME:PCHAR; SOURCEDBTABLE:PCHAR; DESTDBMODE  :INTEGER; DESTDBNAME  :PCHAR; DESTDBTABLE  :PCHAR);stdcall;
var aDDELine:string;
    SourceDBMode :integer;
    SourceDBName :string;
    pSourceDBName:PCHAR;
    SourceDBTable:string;
    pSourceDBTable:PCHAR;
    DestDBMode   :integer;
    DestDBName   :string;
    pDestDBName  :PCHAR;
    DestDBTable  :string;
    pDestDBTable :PCHAR;
    NumStr:string;
    count :integer;
    SETDBSETTINGS :TSetDBSettings;
    SetDBSettingsPtr:TFarProc;
begin
   aDDELine:=DDECmd;
   delete(aDDELine,1, 18); // delete [DOSETDBSETTINGS][
   // get SourceDBMode
   NumStr:=copy(aDDELine,1,Pos(']',aDDELine)-1);
   val(NumStr, SourceDBMode, count); if count <> 0 then SourceDBMode:=0;
   delete(aDDELine,1,Pos('[',aDDELine));
   // get SourceDBName
   SourceDBName:=copy(aDDELine,1,Pos(']',aDDELine)-1);
   delete(aDDELine,1,Pos('[',aDDELine));
   // get SourceDBTable
   SourceDBTable:=copy(aDDELine,1,Pos(']',aDDELine)-1);
   delete(aDDELine,1,Pos('[',aDDELine));
   // get DestDBMode
   NumStr:=copy(aDDELine,1,Pos(']',aDDELine)-1);
   val(NumStr, DestDBMode, count); if count <> 0 then DestDBMode:=0;
   delete(aDDELine,1,Pos('[',aDDELine));
   // get DestDBName
   DestDBName:=copy(aDDELine,1,Pos(']',aDDELine)-1);
   delete(aDDELine,1,Pos('[',aDDELine));
   // get DestDBTable
   DestDBTable:=copy(aDDELine,1,Pos(']',aDDELine)-1);

   SetDBSettingsPtr:=GetProcAddress(WinGISMainForm.ImpExpDll,'SETDBSETTINGS');
   if SetDBSettingsPtr <> nil then
   begin
      pSourceDBName:=stralloc(length(SourceDBName)+1); strpcopy(pSourceDBName,SourceDBName);
      pSourceDBTable:=stralloc(length(SourceDBTable)+1); strpcopy(pSourceDBTable,SourceDBTable);
      pDestDBName:=stralloc(length(DestDBName)+1); strpcopy(pDestDBName,DestDBName);
      pDestDBTable:=stralloc(length(DestDBTable)+1); strpcopy(pDestDBTable,DestDBTable);
      @SETDBSETTINGS:=SetDBSettingsPtr;
      SETDBSETTINGS(SourceDBMode, pSourceDBName, pSourceDBTable,
                    DestDBMode, pDestDBName, pDestDBTable);
      strdispose(pSourceDBName);
      strdispose(pSourceDBTable);
      strdispose(pDestDBName);
      strdispose(pDestDBTable);
   end
   else
      FreeLibrary(WinGISMainForm.ImpExpDll);
end;

// procedure is used to set database settings
procedure TMDIChild.DDE_DoSetProjection(DDECmd:string);
type TSetProjection=procedure(SOURCECOORDINATESYSTEM:INTEGER; // source projection
                              SOURCEPROJECTION      :PCHAR;
                              SOURCEDATE            :PCHAR;
                              SOURCEPROJSETTINGS    :PCHAR;
                              SOURCEXOFFSET         :DOUBLE;
                              SOURCEYOFFSET         :DOUBLE;
                              SOURCESCALE           :DOUBLE;
                              TARGETCOORDINATESYSTEM:INTEGER; // target projection
                              TARGETPROJECTION      :PCHAR;
                              TARGETDATE            :PCHAR;
                              TARGETPROJSETTINGS    :PCHAR;
                              TARGETXOFFSET         :DOUBLE;
                              TARGETYOFFSET         :DOUBLE;
                              TARGETSCALE           :DOUBLE);stdcall;
var aDDELine:string;
    NumStr:string;
    count  :integer;
    SourceCoordSystem:integer;
    SourceProjection :string;
    pSourceProjection:PCHAR;
    SourceDate       :string;
    pSourceDate      :PCHAR;
    SourceProjSettings:string;
    pSourceProjSettings:PCHAR;
    SourceXOffset    :double;
    SourceYOffset    :double;
    SourceScale      :double;
    TargetCoordSystem:integer;
    TargetProjection :string;
    pTargetProjection:PCHAR;
    TargetDate       :string;
    pTargetDate      :PCHAR;
    TargetProjSettings:string;
    pTargetProjSettings:PCHAR;
    TargetXOffset    :double;
    TargetYOffset    :double;
    TargetScale      :double;
    SETPROJECTION    :TSetProjection;
    SetProjectionPtr :TFarProc;
begin
   aDDELine:=DDECmd;
   delete(aDDELine,1, 18); // delete [DOSETPROJECTION][
   // get SourceCoordSystem
   NumStr:=copy(aDDELine,1,Pos(']',aDDELine)-1);
   val(NumStr, SourceCoordSystem, count); if count <> 0 then SourceCoordSystem:=0;
   delete(aDDELine,1,Pos('[',aDDELine));
   // get SourceProjection
   SourceProjection:=copy(aDDELine,1,Pos(']',aDDELine)-1);
   delete(aDDELine,1,Pos('[',aDDELine));
   // get SourceDate
   SourceDate:=copy(aDDELine,1,Pos(']',aDDELine)-1);
   delete(aDDELine,1,Pos('[',aDDELine));
   // get SourceProjSettings
   SourceProjSettings:=copy(aDDELine,1,Pos(']',aDDELine)-1);
   delete(aDDELine,1,Pos('[',aDDELine));
   // get SourceXOffset
   NumStr:=copy(aDDELine,1,Pos(']',aDDELine)-1);
   val(NumStr, SourceXOffset, count); if count <> 0 then SourceXOffset:=0;
   delete(aDDELine,1,Pos('[',aDDELine));
   // get SourceYOffset
   NumStr:=copy(aDDELine,1,Pos(']',aDDELine)-1);
   val(NumStr, SourceYOffset, count); if count <> 0 then SourceYOffset:=0;
   delete(aDDELine,1,Pos('[',aDDELine));
   // get SourceScale
   NumStr:=copy(aDDELine,1,Pos(']',aDDELine)-1);
   val(NumStr, SourceScale, count); if count <> 0 then SourceScale:=0;
   delete(aDDELine,1,Pos('[',aDDELine));
   // get TargetCoordSystem
   NumStr:=copy(aDDELine,1,Pos(']',aDDELine)-1);
   val(NumStr, TargetCoordSystem, count); if count <> 0 then TargetCoordSystem:=0;
   delete(aDDELine,1,Pos('[',aDDELine));
   // get TargetProjection
   TargetProjection:=copy(aDDELine,1,Pos(']',aDDELine)-1);
   delete(aDDELine,1,Pos('[',aDDELine));
   // get TargetDate
   TargetDate:=copy(aDDELine,1,Pos(']',aDDELine)-1);
   delete(aDDELine,1,Pos('[',aDDELine));
   // get TargetProjSettings
   TargetProjSettings:=copy(aDDELine,1,Pos(']',aDDELine)-1);
   delete(aDDELine,1,Pos('[',aDDELine));
   // get TargetXOffset
   NumStr:=copy(aDDELine,1,Pos(']',aDDELine)-1);
   val(NumStr, TargetXOffset, count); if count <> 0 then TargetXOffset:=0;
   delete(aDDELine,1,Pos('[',aDDELine));
   // get TargetYOffset
   NumStr:=copy(aDDELine,1,Pos(']',aDDELine)-1);
   val(NumStr, TargetYOffset, count); if count <> 0 then TargetYOffset:=0;
   delete(aDDELine,1,Pos('[',aDDELine));
   // get TargetScale
   NumStr:=copy(aDDELine,1,Pos(']',aDDELine)-1);
   val(NumStr, TargetScale, count); if count <> 0 then TargetScale:=0;

   SetProjectionPtr:=GetProcAddress(WinGISMainForm.ImpExpDll,'SETPROJECTION');
   if SetProjectionPtr <> nil then
   begin
      pSourceProjection:=stralloc(length(SourceProjection)+1); strpcopy(pSourceProjection,SourceProjection);
      pSourceDate:=stralloc(length(SourceDate)+1); strpcopy(pSourceDate, SourceDate);
      pSourceProjSettings:=stralloc(length(SourceProjSettings)+1); strpcopy(pSourceProjSettings,SourceProjSettings);
      pTargetProjection:=stralloc(length(TargetProjection)+1); strpcopy(pTargetProjection,TargetProjection);
      pTargetDate:=stralloc(length(TargetDate)+1); strpcopy(pTargetDate,TargetDate);
      pTargetProjSettings:=stralloc(length(TargetProjSettings)+1); strpcopy(pTargetProjSettings,TargetProjSettings);

      @SETPROJECTION:=SetProjectionPtr;
      SETPROJECTION(SourceCoordSystem, pSourceProjection, pSourceDate, pSourceProjSettings, SourceXOffset, SourceYOffset, SourceScale,
                    TargetCoordSystem, pTargetProjection, pTargetDate, pTargetProjSettings, TargetXOffset, TargetYOffset, TargetScale);

      strdispose(pSourceProjection);
      strdispose(pSourceDate);
      strdispose(pSourceProjSettings);
      strdispose(pTargetProjection);
      strdispose(pTargetDate);
      strdispose(pTargetProjSettings);
   end
   else
      FreeLibrary(WinGISMainForm.ImpExpDll);
end;

// procedure is used to execute an export
procedure TMDIChild.DDE_DoExecExp;
type TExecExp=procedure;stdcall;
var EXECEXP   :TExecExp;
    ExecExpPtr:TFarProc;
begin
   ExecExpPtr:=GetProcAddress(WinGISMainForm.ImpExpDll,'EXECEXP');
   if ExecExpPtr <> nil then
   begin
      @EXECEXP:=ExecExpPtr;
      EXECEXP;
   end
   else
      FreeLibrary(WinGISMainForm.ImpExpDll);
end;

// procedure is used to execute an import
procedure TMDIChild.DDE_DoExecImp;
type TExecImp=procedure;stdcall;
var EXECIMP   :TExecImp;
    ExecImpPtr:TFarProc;
begin
   ExecImpPtr:=GetProcAddress(WinGISMainForm.ImpExpDll,'EXECIMP');
   if ExecImpPtr <> nil then
   begin
      @EXECIMP:=ExecImpPtr;
      EXECIMP;
   end
   else
      FreeLibrary(WinGISMainForm.ImpExpDll);
end;

Procedure TMDIChild.FileExportAMPDX;
type TAmpExport=procedure (DIR:PCHAR;var AXHANDLE:VARIANT;var SOURCE:VARIANT;REGMODE:integer; LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);stdcall;
var inifilename:string;
    Languageext:string;
    Ini: TIniFile;
    AMPEXPORT   :TAmpExport;
    AmpExportPtr:TFarProc;
    curdoc:Variant;
    pdir  :PCHAR;
    plng  :PCHAR;
    WinHandle: Integer;
begin
   {$IFNDEF WMLT}
   if Data^.IsCanvas then begin
      WinHandle:=Application.Handle;
   end
   else begin
      WinHandle:=WingisMainForm.Handle;
   end;

   pdir:=stralloc(length(OSInfo.WingisDir)+1);
   strpcopy(pdir,OSInfo.WingisDir);

   // get language code
   inifilename:=OSInfo.WingisIniFileName;
   Ini:=TIniFile.Create(OSInfo.WingisIniFileName);
   LanguageExt:=Ini.ReadString('Settings','Language','044'); // default is english
   Ini.Free;
   plng:=stralloc(length(LanguageExt)+1);
   strpcopy(plng, LanguageExt);

   WinGISMainForm.ImpExpDll:=LoadLibrary('AMPEXP.DLL');
   WingisMainForm.CheckLibStatus(WingisMainForm.ImpExpDll,'AMPEXP.DLL','Import/Export');
   AmpExportPtr:=GetProcAddress(WinGISMainForm.ImpExpDll,'AMPEXPORT');
   if AmpExportPtr <> NIL then
   begin
      @AMPEXPORT:=AmpExportPtr;
      // call pxf-asc import DLL
      WinGISMainForm.CheckTDocumentIsCorrect;
      curdoc:=WinGISMainForm.GetCurrentTDocument;
      AMPEXPORT(pdir,WinGISMainForm.DXApp,curdoc,1, plng,
                   WinHandle,
                   WinGISMainForm.Top,
                   WinGISMainForm.Left,
                   WinGISMainForm.Width,
                   WinGISMainForm.Height
                  );
      WinGISMainForm.LastImpExpOperation:='AMPEXP';
   end
   else
   begin
      FreeLibrary(WinGISMainForm.ImpExpDll);
   end;
   strdispose(pdir);
   strdispose(plng);
   {$ENDIF}
end;

Procedure TMDIChild.FileImportMIFDX;
type TMifImport=procedure (DIR:PCHAR;var AXHANDLE:Variant;var DEST:Variant;REGMODE:integer;LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer); stdcall;
var inifilename:string;
    Languageext:string;
    Ini: TIniFile;
    MIFIMPORT   :TMifImport;
    MifImportPtr:TFarProc;
    curdoc:Variant;
    pdir  :PCHAR;
    plng  :PCHAR;
    WinHandle: Integer;
begin
   {$IFNDEF WMLT}
   if Data^.IsCanvas then begin
      WinHandle:=Application.Handle;
   end
   else begin
      WinHandle:=WingisMainForm.Handle;
   end;

   pdir:=stralloc(length(OSInfo.WingisDir)+1);
   strpcopy(pdir,OSInfo.WingisDir);

   // get language code
   inifilename:=OSInfo.WingisIniFileName;
   Ini:=TIniFile.Create(OSInfo.WingisIniFileName);
   LanguageExt:=Ini.ReadString('Settings','Language','044'); // default is english
   Ini.Free;
   plng:=stralloc(length(LanguageExt)+1);
   strpcopy(plng, LanguageExt);

   WinGISMainForm.ImpExpDll:=LoadLibrary('MIFIMP.DLL');
   WingisMainForm.CheckLibStatus(WingisMainForm.ImpExpDll,'MIFIMP.DLL','Import/Export');
   MIFImportPtr:=GetProcAddress(WinGISMainForm.ImpExpDll,'MIFIMPORT');
   if MifImportPtr <> NIL then
   begin
      @MIFIMPORT:=MifImportPtr;
      // call pxf-asc import DLL
      WinGISMainForm.CheckTDocumentIsCorrect;
      curdoc:=WinGISMainForm.GetCurrentTDocument;
      MIFIMPORT(pdir,WinGISMainForm.DXApp,curdoc,1, plng,
                WinHandle,
                WinGISMainForm.Top,
                WinGISMainForm.Left,
                WinGISMainForm.Width,
                WinGISMainForm.Height
               );
      WinGISMainForm.LastImpExpOperation:='MIFIMP';
   end
   else
   begin
      FreeLibrary(WinGISMainForm.ImpExpDll);
   end;
   strdispose(pdir);
   strdispose(plng);
   {$ENDIF}
end;

Procedure TMDIChild.FileExportMIFDX;
type TMifExport=procedure (DIR:PCHAR;var AXHANDLE:VARIANT;var SOURCE:VARIANT;REGMODE:integer; LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);stdcall;
var inifilename:string;
    Languageext:string;
    Ini: TIniFile;
    MIFEXPORT   :TMifExport;
    MifExportPtr:TFarProc;
    curdoc:Variant;
    pdir  :PCHAR;
    plng  :PCHAR;
    WinHandle: Integer;
begin
   {$IFNDEF WMLT}
   if Data^.IsCanvas then begin
      WinHandle:=Application.Handle;
   end
   else begin
      WinHandle:=WingisMainForm.Handle;
   end;

   pdir:=stralloc(length(OSInfo.WingisDir)+1);
   strpcopy(pdir,OSInfo.WingisDir);

   // get language code
   inifilename:=OSInfo.WingisIniFileName;
   Ini:=TIniFile.Create(OSInfo.WingisIniFileName);
   LanguageExt:=Ini.ReadString('Settings','Language','044'); // default is english
   Ini.Free;
   plng:=stralloc(length(LanguageExt)+1);
   strpcopy(plng, LanguageExt);

   WinGISMainForm.ImpExpDll:=LoadLibrary('MIFEXP.DLL');
   WingisMainForm.CheckLibStatus(WingisMainForm.ImpExpDll,'MIFEXP.DLL','Import/Export');
   MifExportPtr:=GetProcAddress(WinGISMainForm.ImpExpDll,'MIFEXPORT');
   if MifExportPtr <> NIL then
   begin
      @MIFEXPORT:=MifExportPtr;
      // call pxf-asc import DLL
      WinGISMainForm.CheckTDocumentIsCorrect;
      curdoc:=WinGISMainForm.GetCurrentTDocument;
      MIFEXPORT(pdir,WinGISMainForm.DXApp,curdoc,1, plng,
                WinHandle,
                WinGISMainForm.Top,
                WinGISMainForm.Left,
                WinGISMainForm.Width,
                WinGISMainForm.Height
               );
      WinGISMainForm.LastImpExpOperation:='MIFEXP';
   end
   else
   begin
      FreeLibrary(WinGISMainForm.ImpExpDll);
   end;
   strdispose(pdir);
   strdispose(plng);
   {$ENDIF}
end;

Procedure TMDIChild.FileImportDGNDX;
type TDgnImport=procedure (DIR:PCHAR;var AXHANDLE:Variant;var DEST:Variant;REGMODE:integer;LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer); stdcall;
var inifilename:string;
    Languageext:string;
    Ini: TIniFile;
    DGNIMPORT   :TDgnImport;
    DgnImportPtr:TFarProc;
    curdoc:Variant;
    pdir  :PCHAR;
    plng  :PCHAR;
    WinHandle: Integer;
begin
   {$IFNDEF WMLT}
   if Data^.IsCanvas then begin
      WinHandle:=WingisMainForm.Handle;
   end
   else begin
      WinHandle:=WingisMainForm.Handle;
   end;

   pdir:=stralloc(length(OSInfo.WingisDir)+1);
   strpcopy(pdir,OSInfo.WingisDir);

   // get language code
   inifilename:=OSInfo.WingisIniFileName;
   Ini:=TIniFile.Create(OSInfo.WingisIniFileName);
   LanguageExt:=Ini.ReadString('Settings','Language','044'); // default is english
   Ini.Free;
   plng:=stralloc(length(LanguageExt)+1);
   strpcopy(plng, LanguageExt);

   WinGISMainForm.ImpExpDll:=LoadLibrary('DGNIMP.DLL');
   WingisMainForm.CheckLibStatus(WingisMainForm.ImpExpDll,'DGNIMP.DLL','Import/Export');
   DgnImportPtr:=GetProcAddress(WinGISMainForm.ImpExpDll,'DGNIMPORT');
   if DgnImportPtr <> NIL then
   begin
      @DGNIMPORT:=DgnImportPtr;
      // call pxf-asc import DLL
      WinGISMainForm.CheckTDocumentIsCorrect;
      curdoc:=WinGISMainForm.GetCurrentTDocument;
      DGNIMPORT(pdir,WinGISMainForm.DXApp,curdoc,1, plng,
                WinHandle,
                WinGISMainForm.Top,
                WinGISMainForm.Left,
                WinGISMainForm.Width,
                WinGISMainForm.Height
               );
      WinGISMainForm.LastImpExpOperation:='DGNIMP';
   end
   else
   begin
      FreeLibrary(WinGISMainForm.ImpExpDll);
   end;
   strdispose(pdir);
   strdispose(plng);
   {$ENDIF}
end;

function TMDIChild.ProcCreateThematicMap(Connection: AnsiString; Command: AnsiString; ProgisIDField: AnsiString; ThematicField: AnsiString; Grouping: Integer; GroupCount: Integer; GroupingName: AnsiString; SchemaName: AnsiString; DestLayerName: AnsiString; AllowDialog: Boolean): Integer;
var
   Create: function (Handle: Integer;
                     Doc: Variant;
                     Lng: PChar;
                     Connection: PChar;
                     Command: PChar;
                     ProgisIDField: PChar;
                     ThematicField: PChar;
                     Grouping: Integer;
                     GroupCount: Integer;
                     GroupingName: PChar;
                     SchemaName: PChar;
                     DestLayerName: PChar;
                     AllowDialog: Boolean;
                     MaxTMClasses: Integer;
                     AutoClose: Boolean): Integer; stdcall;
   Ini: TIniFile;
begin
     Result:=-1;

     if ThematicDLLHandle = 0 then
     begin
        ThematicDLLHandle := LoadLibrary('Thematic.dll');
        WingisMainForm.CheckLibStatus (ThematicDLLHandle,'Thematic.dll','Thematic Map');
     end;

     if ThematicDLLHandle <> 0 then
     begin
        @Create := GetProcAddress(ThematicDLLHandle,'Create');
        if @Create <> nil then
        begin
           Ini := TIniFile.Create(OSInfo.WingisIniFileName);
           Data^.SetCursor(crHourGlass);
           try
              Result := Create (Application.Handle,
                             WinGISMainForm.GetCurrentTDocument,
                             PChar(Ini.ReadString('Settings','Language','044')),
                             PChar(Connection),
                             PChar(Command),
                             PChar(ProgisIDField),
                             PChar(ThematicField),
                             Grouping,
                             GroupCount,
                             PChar(GroupingName),
                             PChar(SchemaName),
                             PChar(DestLayerName),
                             AllowDialog,
                             IniFile^.MaxTMClasses,
                             not Data^.PInfo^.AnnotSettings.ShowDialog);
           finally
              Data^.SetCursor(crDefault);
              Ini.Free;
           end;
        end;
     end;
end;

function TMDIChild.ProcApplyThematicMap: Integer;
var
   Apply: function (Handle: Integer;
                    Doc: Variant;
                    Lng: PChar;
                    MaxTMClasses: Integer): Integer; stdcall;
   Ini: TIniFile;
begin
     Result:=-1;

     if ThematicDLLHandle = 0 then
     begin
        ThematicDLLHandle := LoadLibrary('Thematic.dll');
        WingisMainForm.CheckLibStatus (ThematicDLLHandle,'Thematic.dll','Thematic Map');
     end;

     if ThematicDLLHandle <> 0 then
     begin
        @Apply := GetProcAddress(ThematicDLLHandle,'Apply');
        if @Apply <> nil then
        begin
           Ini := TIniFile.Create(OSInfo.WingisIniFileName);
           Data^.SetCursor(crHourGlass);
           try
              Result := Apply (Application.Handle,
                             WinGISMainForm.GetCurrentTDocument,
                             PChar(Ini.ReadString('Settings','Language','044')),
                             IniFile^.MaxTMClasses);
           finally
              Data^.SetCursor(crDefault);
              Ini.Free;
           end;
        end;
     end;
end;

Procedure TMDIChild.FileImportDXF;
begin
  ImpExport^.ImportDXF(Data);
end;

Procedure TMDIChild.FileImportASCII;
begin
  ImpExport^.ImportASCII;
end;

Procedure TMDIChild.EditDelete;
begin
  Data^.DeleteSelected(False);
end;

Procedure TMDIChild.EditMove;
begin
  Data^.SetActualMenu(mn_Move);
end;

Procedure TMDIChild.ExtrasProjectBorder;
begin
  Data^.SetOverview;
end;

Procedure TMDIChild.ExtrasAreaCut;
begin
  AreaCut4(Data);
end;

Procedure TMDIChild.ExtrasObjectCut;
begin
  ObjectCut(Data);
end;

Procedure TMDIChild.ExtrasAreaCutSelected;
begin                               
  AreaCutSelected(Data,'',0);
end;

Procedure TMDIChild.ExtrasPolygonPart;
var AResult        : Integer;
begin
  if ((Data^.Layers^.SelLayer^.SelInfo.Count = 1) and (Data^.Layers^.SelLayer^.SelInfo.SelectedType = ot_CPoly)) then begin
     DDEHandler.EnableDDEReception(FALSE);
     AResult:=DoPartPoly(Data,TRUE);
     if (AResult = 0) or (AResult = 1) then DDEHandler.EnableDDEReception(TRUE);
  end
  else begin
     ProcPolyPartWithLineSelected(Data,False);
  end;
end;

Procedure TMDIChild.ExtrasFreePolygonPart;
begin
  if ((Data^.Layers^.SelLayer^.SelInfo.Count = 1) and (Data^.Layers^.SelLayer^.SelInfo.SelectedType = ot_CPoly)) then begin
     DoPartPolyFree(Data,True);
  end
  else begin
     ProcPolyPartWithLineSelected(Data,True);
  end;
end;

Procedure TMDIChild.ExtrasMovePartParallel;
begin
  Data^.SetActualMenu(mn_MovePartPara);
end;

Procedure TMDIChild.ExtrasCombine;
begin
  ProcCombine(Data);
end;

Procedure TMDIChild.ExtrasExplode;
begin
  Explode(Data);
end;

Procedure TMDIChild.ExtrasGenerate;
begin
  Data^.GenerateDlg;
end;

Procedure TMDIChild.ExtrasGenerateObjects;
begin
  MyObject:=TAreaObject.Create(Self,Data);
{++ IDB_UNDO}
{ I suppose that during creating of new objects, sources objects will be deleted. }
  If WinGisMainForm.WeAreUsingTheIDB[Data] Then Begin
     WinGisMainForm.IDB_Man.SetSignOfMultiOperations(SELF.Data, TRUE);
     WinGisMainForm.IDB_Man.GetReadyToReceiveDeletedObjectID(SELF.Data);
  End;
// ++ Cadmensky
  if WinGisMainForm.WeAreUsingUndoFunction[Data] then
  begin
//     WinGisMainForm.IDB_Man.SetSignOfMultiOperations(SELF.Data, TRUE);
     WingisMainForm.UndoRedo_Man.GetReadyToReceiveDeletedObjectID (SELF.Data);
  end;
// -- Cadmensky

{-- IDB_UNDO}
  MyObject.DoArea;
{++ IDB_UNDO}
  If WinGisMainForm.WeAreUsingTheIDB[Data] Then Begin
     WinGisMainForm.IDB_Man.SetSignOfMultiOperations(SELF.Data, FALSE);
     WinGisMainForm.IDB_Man.ExecuteDeleting(SELF.Data);
     End;
  if WinGisMainForm.WeAreUsingUndoFunction[Data] then
  begin
//     WinGisMainForm.IDB_Man.SetSignOfMultiOperations(SELF.Data, FALSE);
     WinGisMainForm.UndoRedo_Man.ExecuteDeleting(SELF.Data);
  end;
{-- IDB_UNDO}
  MyObject.Free;
end;

procedure TMDIChild.ExtrasGenerateArea;
begin
     Data^.SetActualMenu(mn_GenArea);
end;

Procedure TMDIChild.ExtrasCombinePolygons;
var CurDoc: Variant;
    AxGisObjMan:TAXGisObjMan;
    SLayerName,DLayerName: AnsiString;
    DLayer: PLayer;
    Radius: Double;
    aDocument:OleVariant;
    aApp     :OleVariant;
begin
   if Data^.Layers^.SelLayer^.SelInfo.Count > 1 then
   begin
      ExtrasCombineSelectedPolygons;
   end
   else
   begin
      SLayerName:=Data^.Layers^.TopLayer^.Text^;
      DLayer:=SelectLayerDialog(Data,[sloNewLayer]);
      if DLayer <> nil then
      begin
         DLayerName:=DLayer^.Text^;
         Radius:=Data^.PInfo^.VariousSettings.CombineRadius;
         Data^.SetCursor(crHourGlass);

         // create instance of Gis-Object-Manipulation object
         AxGisObjMan:=TAXGisObjMan.Create(nil);
         AxGisObjMan.WorkingDir:=OSInfo.WingisDir; // set working directory
         AxGisObjMan.LngCode:='044';                              // set default language

         // set dx reference pointer
         aApp:=WinGISMainForm.DXApp;
         AxGisObjMan.SetApp(aApp);                     

         // get current document
         WinGISMainForm.CheckTDocumentIsCorrect;
         CurDoc:=WinGISMainForm.GetCurrentTDocument;
         aDocument:=CurDoc;
         AxGisObjMan.SetDocument(aDocument);

         // set the snapradius
         AxGisObjMan.SnapRadius:=Radius;

         AxGisObjMan.OnDisplayPercent:=GisManDisplayPercent;
         AXGisObjMan.OnDisplayPass:=GisManDisplayPass;
         StatusBar.ProgressPanel:=True;
         try
            // execute the combine cpolys on layer method
            AxGisObjMan.CombineCPolysOnLayer(SLayerName, DLayerName, FALSE {do not combine selected objects} );
         finally
            // destroy the Gis-Object-Manipulation object
            Data^.SetCursor(crDefault);
            StatusBar.ProgressPanel:=False;
            AxGisObjMan.Destroy;
         end;

         Data^.SetModified;
         Data^.PInfo^.RedrawScreen(False);
      end;
   end;
end;

procedure TMDIChild.ExtrasCombineSelectedPolygons;
var curDoc: Variant;
    DLayerName: AnsiString;
    DLayer: PLayer;
    Radius: Double;
    AxGisObjMan:TAXGisObjMan;
    aDocument:OleVariant;
    aApp     :OleVariant;
begin
   DLayer:=SelectLayerDialog(Data,[sloNewLayer]);
   if DLayer <> nil then
   begin
      DLayerName:=DLayer^.Text^;
      Radius:=2;//Data^.PInfo^.VariousSettings.CombineRadius;
      Data^.SetCursor(crHourGlass);

      // create instance of Gis-Object-Manipulation object
      AxGisObjMan:=TAXGisObjMan.Create(nil);
      AxGisObjMan.WorkingDir:=OSInfo.WingisDir; // set working directory
      AxGisObjMan.LngCode:='044';                              // set default language

      // set dx reference pointer
      aApp:=WinGISMainForm.DXApp;
      AxGisObjMan.SetApp(aApp);

      // get current document
      WinGISMainForm.CheckTDocumentIsCorrect;
      CurDoc:=WinGISMainForm.GetCurrentTDocument;
      CurDoc.CreateSelectedList; // create list for selected objects
      aDocument:=CurDoc;
      AxGisObjMan.SetDocument(aDocument);

      // set the snapradius
      AxGisObjMan.SnapRadius:=Radius;

      AxGisObjMan.OnDisplayPercent:=GisManDisplayPercent;
      AXGisObjMan.OnDisplayPass:=GisManDisplayPass;
      StatusBar.ProgressPanel:=True;
      try
         // execute the combine cpolys on layer method
         AxGisObjMan.CombineCPolysOnLayer('' { Empty due to source is selected}, DLayerName, TRUE {combine selected objects} );
      finally
         // destroy the Gis-Object-Manipulation object
         Data^.SetCursor(crDefault);
         StatusBar.ProgressPanel:=False;
         AxGisObjMan.Destroy;
      end;

      Data^.SetModified;
      Data^.PInfo^.RedrawScreen(False);
      CurDoc.DestroySelectedList; // destroy list for selected objects
   end;
end;

procedure TMDIChild.CombinePolygonsFromDB(s: AnsiString);
var CurDoc: Variant;
    DLayerName: AnsiString;
    DLayer: PLayer;
    Radius: Double;
    n,i: Integer;
    Id: Integer;
    IdFirst,IdLast: Integer;
    DBID,TempStr: Array [0..255] of Char;
    AxGisObjMan:TAXGisObjMan;
    aDocument:OleVariant;
    aApp     :OleVariant;
begin
   DLayerName:=GetDDEItem(s,2);
   DLayer:=Data^.Layers^.NameToLayer(DLayerName);
   if DLayer = nil then
   begin
      DLayer:=NewLayerDialog1(Data,GetLangText(10154),DLayerName);
   end;
   if DLayer <> nil then
   begin
      // select objects for combine
      Data^.DeSelectAll(False);
      n:=StrToInt(GetDDEItem(s,3));
      for i:=0 to n-1 do
      begin
         Id:=StrToIntDef(GetDDEItem(s,i+4),0);
         if Id > 0 then
         begin
            Data^.SelectByIndex(Id);
         end;
      end;
      DLayerName:=DLayer^.Text^;
      Radius:=Data^.PInfo^.VariousSettings.CombineRadius;
      IdFirst:=PObjects(Data^.PInfo^.Objects)^.LastCPoly+1;

      // create instance of Gis-Object-Manipulation object
      AxGisObjMan:=TAXGisObjMan.Create(nil);
      AxGisObjMan.WorkingDir:=OSInfo.WingisDir; // set working directory
      AxGisObjMan.LngCode:='044';                              // set default language

      // set dx reference pointer
      aApp:=WinGISMainForm.DXApp;
      AxGisObjMan.SetApp(aApp);

      // get current document
      WinGISMainForm.CheckTDocumentIsCorrect;
      CurDoc:=WinGISMainForm.GetCurrentTDocument;
      CurDoc.CreateSelectedList; // create list for selected objects
      aDocument:=CurDoc;
      AxGisObjMan.SetDocument(aDocument);

      // set the snapradius
      AxGisObjMan.SnapRadius:=Radius;

      Data^.SetCursor(crHourGlass);
      AxGisObjMan.OnDisplayPercent:=GisManDisplayPercent;
      AXGisObjMan.OnDisplayPass:=GisManDisplayPass;
      StatusBar.ProgressPanel:=True;
      try
         // execute the combine cpolys on layer method
         AxGisObjMan.CombineCPolysOnLayer('' { Empty due to source is selected}, DLayerName, TRUE {combine selected objects} );
      finally
         // destroy the Gis-Object-Manipulation object
         Data^.SetCursor(crDefault);
         StatusBar.ProgressPanel:=False;
         AxGisObjMan.Destroy;
      end;

      // send list of new created objects to database
      IdLast:=PObjects(Data^.PInfo^.Objects)^.LastCPoly;
      StrCopy(DBID,'0');
      for i:=IdFirst to IdLast do
      begin
         if DDEHandler.FLayerInfo <> 0 then
         begin
            StrCopy(TempStr,' ');
            Data^.InsertedObjects^.Insert(StrNew(TempStr));
         end;
         Data^.InsertedObjects^.Insert(StrNew(@DBID));
         Str(i:10,TempStr);
         Data^.InsertedObjects^.Insert(StrNew(TempStr));
      end;
      ProcSendNewIDsToDB(Data,'GRD');

      Data^.DeSelectAll(False);
      Data^.SetModified;
      Data^.PInfo^.RedrawScreen(False);
      CurDoc.DestroySelectedList; // remove list of selected objects
   end;
end;

Procedure TMDIChild.DBGenerateObjects(s: AnsiString);
var
DLayer: String;
i,lcnt,pcnt,Typ: Integer;
SLayer: Array[0..100] of String;
begin
  MyObject:=TAreaObject.Create(Self,Data);
{++ IDB_UNDO}
  If WinGisMainForm.WeAreUsingTheIDB[Data] Then Begin
     WinGisMainForm.IDB_Man.SetSignOfMultiOperations(SELF.Data, TRUE);
     WinGisMainForm.IDB_Man.GetReadyToReceiveDeletedObjectID(SELF.Data);
     End;
  If WinGisMainForm.WeAreUsingUndoFunction[Data] Then
  Begin
//     WinGisMainForm.IDB_Man.SetSignOfMultiOperations(SELF.Data, TRUE);
     WinGisMainForm.UndoRedo_Man.GetReadyToReceiveDeletedObjectID(SELF.Data);
  End;
{-- IDB_UNDO}
     pcnt:=CountDDEItems(s);
     lcnt:=StrToInt(GetDDEItem(s,2));
     Typ:=StrToInt(GetDDEItem(s,pcnt));
     DLayer:=GetDDEItem(s,pcnt-1);
     for i:=0 to lcnt-1 do begin
         SLayer[i]:=GetDDEItem(s,i+3);
     end;
     MyObject.DoAreaWithoutDlg2(lcnt,SLayer,DLayer,Typ);
{++ IDB_UNDO}
  If WinGisMainForm.WeAreUsingTheIDB[Data] Then Begin
     WinGisMainForm.IDB_Man.SetSignOfMultiOperations(SELF.Data, FALSE);
     WinGisMainForm.IDB_Man.ExecuteDeleting(SELF.Data);
     End;
// ++ Cadmensky
  if WinGisMainForm.WeAreUsingUndoFunction[Data] then
  begin
//     WinGisMainForm.IDB_Man.SetSignOfMultiOperations(SELF.Data, FALSE);
     WinGisMainForm.UndoRedo_Man.ExecuteDeleting(SELF.Data);
  end;
// -- Cadmensky
{-- IDB_UNDO}
  MyObject.Free;
end;

Procedure TMDIChild.ExtrasCoordinateInput;
begin
  Data^.InputCoord;
end;

Procedure TMDIChild.ExtrasNeighbourSearch;
begin
  ProcSelection(Data,TRUE);
end;

Procedure TMDIChild.ExtrasSelect;
begin
  ProcSelection(Data,FALSE);
end;

Procedure TMDIChild.InsertOLEObject;
begin
  Data^.SetActualMenu(mn_InsertOLEObject);
end;

Procedure TMDIChild.InsertPicture; {++ Sygsky:Omega }
var
  OpenStat : OpenStatus;
  Res : Boolean;
  FName : AnsiString;
  InsertBitMapDialog1:TWOpenPictureDialog;
  Flags : DlgCtrlFlags;
  i,n: Integer;
begin
  if Data = nil then
    Exit;           // Ouuupssss???!!!
  Flags := [dfCoordinatesOn]{,dfEnableVectorFormat]}; { Defaul only coordinates }

  if not (OmegaInUse or (Data.SymbolMode = sym_SymbolMode)) then { Omega also is available! }
      if WinGISLicense >= WG_PROF then { WinGIS}
        Flags := Flags +[dfTransformationOn]; { transformation is enabled by default}

  InsertBitMapDialog1 := TWOpenPictureDialog.Create(Self,@Flags,@OpenStat);
  with Data.Registry do
  begin
    CurrentPath := LASTIMAGEDIR;
    InsertBitmapDialog1.InitialDir := ReadString('InitialDir');
    InsertBitmapDialog1.Options := InsertBitmapDialog1.Options + [ofAllowMultiSelect];
  end;
//  try
    Res := OpenImageFile(InsertBitmapDialog1); { Call customized dialog }
    FName := InsertBitmapDialog1.FileName;
    n:=InsertBitmapDialog1.Files.Count;

    if (OpenStat = osNotImage) then begin
       Exit;{ Selected files is not an image one }
    end;

    if Res then { Picture exists and has supported format }
    begin

      if (n > 1) then begin
           for i:=0 to n-1 do begin
               FName:=InsertBitmapDialog1.Files[i];
               ProcInsertBitmap(Self,FName,OpenStat);
           end;
      end
      else begin
           if (ProcInsertBitmap(Self,FName,OpenStat) and (dfTransformationOn in Flags)) then begin
              SetForegroundWindow(Self.Handle); { Try to activate project window }
           end;
      end;

      InsertBitMapDialog1.Free; {++ Syg -> free resources }

      // Store last used directory
      with Data.Registry do
      begin
        CurrentPath := LASTIMAGEDIR;
        WriteString('InitialDir',ExtractFilePath(InsertBitmapDialog1.FileName));
      end;
    end;
//  except
//  end;
end;

{++ Sygsky: specially for Freddie :}
Procedure TMDIChild.InsertTABs;
var
  I, Cnt: Integer;
  Tab: TTabFile;
  TabRect : TRect;
  Coords : array [0..3] of Double;
  Str1, Report : AnsiString;
  LastDir, RealName : AnsiString;

  procedure AddToReport(FullName:AnsiString);
  begin
    Inc(Cnt); { Bump counter of bad TABs }
    Report := Report + #10#13+ '"' + ExtractFileName(FullName) + '":'
              +TAB.LastErrorText + ', ';
  end;

begin
  Tab := nil;
  Cnt := 0;
  with InsertTabsDialog do
    begin
      if InitialDir = EmptyStr then
        with Data.Registry do
        begin
          CurrentPath := LASTIMAGEDIR;
          InitialDir := ReadString('InitialDir');
          LastDir := InitialDir;
        end;
      if Execute then
      begin
      UserInterface.BeginWaitCursor;
      try
        Str1 := MlgStringList[MlgSection.Section,7];
        {+++Brovak Bug 500}
        StopExecutingFunction:=false;
        {--Brovak}
        for I := 0 to InsertTabsDialog.Files.Count-1 do
        begin
          if Tab = nil then
            Tab := TTabFile.Create(Files[I])
          else
            Tab.LoadTab(Files[I]);
          if TAB.IsValid then // It was opened and parsed succesfully
          begin
            // Get USER choice inm case if wildcards in file name are used
            RealName := TAB.RealImageNameUsed;
            if (RealName <> EmptyStr) then
            begin
              TabRect := Tab.ProjPixRect; // Read corner pixel centers bounding rect!
              Coords[0] := TabRect.Left;
              Coords[1] := TabRect.Top;
              Coords[2] := TabRect.Right;
              Coords[3] := TabRect.Bottom;
              UserInterface.StatusText := Format(Str1, [Tab.ImageName]);
              if not ProcInsertCBitMap(Self, RealName, Coords, nil, @Tab) then
                AddToReport(Files[I]);
              UserInterface.StatusText:='';
            end
            else
              AddToReport(Files[I]);
          end
          else
            AddToReport(Files[I]);
          UserInterface.StatusText:='';
       {+++Brovak Bug 500 - Stop of loading Batch of Tab ?}
       Application.ProcessMessages;
       if StopExecutingFunction=true
         then
          if MessageDialog(MlgSection[17],mtConfirmation,[mbYes,mbNo],0)=mrYes
           then Break
            else StopExecutingFunction:=false;
       {--Brovak}
        end;
        finally
          UserInterface.EndWaitCursor;
        end;
        if Cnt > 0 then
        begin
          SetLength(Report,Length(Report)-2); { Delete terminating suffix ', ' }
{          SmartMsgBox(Self.Handle, MlgSection.Section, 15, 16, MB_OK + MB_ICONWARNING,
                      [Cnt, Files.Count, ExtractFilePath(Files[0]),Report + '.']);}
          Report := SmartString(MlgStringList.Strings[ MlgSection.Section, 15],
                    [Cnt, Files.Count, ExtractFilePath(Files[0]),Report + '.']);
          Str1 := SmartString(MlgStringList.Strings[MlgSection.Section, 16], []);
          ShowReport(Str1, Report,0, MB_OK+MB_ICONEXCLAMATION);
        end;
        if ExtractFilePath(FileName) <> LastDir then
          with Data.Registry do
          begin
            CurrentPath := LASTIMAGEDIR;
            WriteString('InitialDir',ExtractFilePath(FileName));
          end;
      end;
    end;
  Tab.Free;
end;
{-- Sygsky: add for TAB files support}

Procedure TMDIChild.InsertAttachments;
begin
  Data^.MultiMedia^.AddDelMedias(Data,WingisMainForm,
      Data^.Layers^.SelLayer^.Data^.At(0)^.Index);
end;

Procedure TMDIChild.EditDuplicateOnLayer;
begin
  Data^.LayerCopy(FALSE,nil);
end;

Procedure TMDIChild.EditMoveToLayer;
begin
  Data^.LayerCopy(TRUE,nil);
end;

Procedure TMDIChild.FileSaveAs;
begin
   {++ str Build #151 removed compileroption OBJLIM - check is now done in TMDIChild.Storedata}
  FileSaveAs1;
end;
  {$ENDIF} // <----------------- AXDLL
{$ENDIF}

Procedure TMDIChild.DrawPoint;
begin
  Data^.SetActualMenu(mn_Pixel);
end;

Procedure TMDIChild.DrawPolygon;
begin
  Data^.SetActualMenu(mn_CPoly);
end;

Procedure TMDIChild.DrawPolyline;
begin
  Data^.SetActualMenu(mn_Poly);
end;

Procedure TMDIChild.DrawSymbol;
begin
  Data^.SetActualMenu(mn_Symbol);
end;

Procedure TMDIChild.DrawText;
begin
  Data^.SetActualMenu(mn_Text);
end;

Procedure TMDIChild.ViewRedraw;
begin
  Data^.PInfo^.RedrawScreen(True);
end;

{$IFNDEF WMLT}
  {$IFNDEF AXDLL} // <----------------- AXDLL

procedure TMDIChild.DBFileSaveHSP(sdde: AnsiString);
var AFName       : Array[0..255] of Char;
    Settings     : TSaveAsSettings;
    Cnt          : Integer;
    ALayerCount   : Integer;
    ALayerName   : AnsiString;
    ALayerNames  : TStringList;
    i            : Integer;

  Procedure CollectProjects(Item:TMDIChild); Far;
  var AFile        : string;
  begin
    if Item.ClassName=MDIChildType then begin
      AFile:=StrPas(Item.Data^.FName);
      if AnsiCompareText(ExtractFileExt(AFile),'.HSP')<>0 then
        Settings.OpenedProjects.Add(AFile);
    end;
  end;
  Procedure CollectLayers(Item:PLayer); Far;
  begin
    if Item^.Index>0 then Settings.ActiveProjectLayers.Add(PToStr(Item^.Text));
  end;
begin
  Settings:=TSaveAsSettings.Create;
  { setup information about the active project }
  Settings.ActiveProject:=StrPas(Data^.FName);
  if AnsiCompareText(ExtractFileExt(Settings.ActiveProject),'.HSP')=0 then
      Settings.ActiveProjectType:=ptHSP
  else if AnsiCompareText(ExtractFileExt(Settings.ActiveProject),'.SSP')=0 then
      Settings.ActiveProjectType:=ptSSP
  else Settings.ActiveProjectType:=ptAMP;
  { create list of opened projects }
  with WinGISMainForm do for Cnt:=0 to MDIChildCount-1 do
      CollectProjects(TMDIChild(MDIChildren[Cnt]));
  { create list of layers }
  Data^.Layers^.LData^.ForEach(@CollectLayers);

  with Settings do begin
    ProjectType:=ptHSP;
    FileName1:=ChangeFileExt(ActiveProject,'.hsp');
    FileName2:=ChangeFileExt(ActiveProject,'.hsd');
    Files.Add(ActiveProject);

    ALayerNames:=TStringList.Create;
    try
       ALayerCount:=StrToIntDef(GetDDEItem(sdde,2),0);
       if ALayerCount > 0 then begin

          for i:=0 to ALayerCount-1 do begin
              ALayerName:=GetDDEItem(sdde,i+3);
              ALayerName:=Trim(ALayerName);
              if ALayerName <> '' then begin
                 ALayerNames.Add(ALayerName);
              end;
          end;

          for i:=1 to Data^.Layers^.LData^.Count-1 do begin
              ALayerName:=PLayer(Data^.Layers^.LData^.At(i))^.Text^;
              if ALayerNames.IndexOf(ALayerName) > -1 then begin
                 HSPLayers.Add(ALayerName);
              end
              else begin
                 HSDLayers.Add(ALayerName);
              end;
          end;
       end;
    finally
       ALayerNames.Free;
    end;

    Options:=[];
  end;

  if ((Settings.HSDLayers.Count > 0) and (Settings.HSPLayers.Count > 0)) then begin
    StrPCopy(AFName,Settings.FileName1);
    HasTitle:=True;
    if Settings.ProjectType=ptAMP then FileSaveAs2(AFName,FALSE,FALSE)
    else if Settings.ProjectType=ptSSP then FileSaveAs2(AFName,TRUE,FALSE)
    else if Settings.ProjectType=ptHSP then with Settings do begin
      { if project is already a hsp-project just copy the files }
      if Data^.Document.Loaded then begin
        { but currently not implemented, so show error-message }
        MsgBox(Handle,4730,mb_IconExclamation or mb_Ok,'');
      end
      { if only one file to convert and file is active-project then convert
        using the selected layers }
      else if (Files.Count=1) and (AnsiCompareText(Files[0],ActiveProject)=0) then begin
        { remove the project-name from the file-list }
        Files.Clear;
        { update of database and log-file are not needed }
        Options:=Options-[saoCreateLogFile];

{++ IDB}
        // Before we can start 'save as' of the project, we have to make 'save as' of the database of this project.
        If (WinGisMainForm.WeAreUsingTheIDB[Data]) Then
           WingisMainForm.IDB_Man.SaveDatabaseAs(SELF.Data, FileName1); // *.hsp file name.
{-- IDB}

        WinGISMainForm.ConvertToWgpWithLayers(Options,SerialNumber,
          ActiveProject,FileName1,FileName2,HSPLayers,HSDLayers,Files,TRUE,TRUE);
      end
      { convert the specified files }
      else WinGISMainForm.ConvertToWgp(Options,SerialNumber,
          FileName1,FileName2,Files,TRUE,TRUE);
    end;
  end;
  Settings.Free;
end;

Procedure TMDIChild.FileSaveAs1;
var AFName       : Array[0..255] of Char;
    Settings     : TSaveAsSettings;
    Cnt          : Integer;
  Procedure CollectProjects(Item:TMDIChild); Far;
  var AFile        : string;
  begin
    if Item.ClassName=MDIChildType then begin
      AFile:=StrPas(Item.Data^.FName);
      if AnsiCompareText(ExtractFileExt(AFile),'.HSP')<>0 then
        Settings.OpenedProjects.Add(AFile);
    end;
  end;
  Procedure CollectLayers(Item:PLayer); Far;
  begin
    if Item^.Index>0 then Settings.ActiveProjectLayers.Add(PToStr(Item^.Text));
  end;
begin
  Settings:=TSaveAsSettings.Create;
  { setup information about the active project }
  Settings.ActiveProject:=StrPas(Data^.FName);
  if AnsiCompareText(ExtractFileExt(Settings.ActiveProject),'.HSP')=0 then
      Settings.ActiveProjectType:=ptHSP
  else if AnsiCompareText(ExtractFileExt(Settings.ActiveProject),'.SSP')=0 then
      Settings.ActiveProjectType:=ptSSP
  else Settings.ActiveProjectType:=ptAMP;
  { create list of opened projects }
  with WinGISMainForm do for Cnt:=0 to MDIChildCount-1 do
      CollectProjects(TMDIChild(MDIChildren[Cnt]));
  { create list of layers }
  Data^.Layers^.LData^.ForEach(@CollectLayers);  
  if SaveFileAs(Handle,Settings) then begin
    StrPCopy(AFName,Settings.FileName1);
    HasTitle:=True;
    if Settings.ProjectType=ptAMP then FileSaveAs2(AFName,FALSE,FALSE)
    else if Settings.ProjectType=ptSSP then FileSaveAs2(AFName,TRUE,FALSE)
    else if Settings.ProjectType=ptHSP then with Settings do begin
      { if project is already a hsp-project just copy the files }
      if Data^.Document.Loaded then begin
        { but currently not implemented, so show error-message }
        MsgBox(Handle,4730,mb_IconExclamation or mb_Ok,'');
      end
      { if only one file to convert and file is active-project then convert
        using the selected layers }
      else if (Files.Count=1) and (AnsiCompareText(Files[0],ActiveProject)=0) then begin
        { remove the project-name from the file-list }
        Files.Clear;
        { update of database and log-file are not needed }
        Options:=Options-[saoCreateLogFile];

{++ IDB}
        // Before we can start 'save as' of the project, we have to make 'save as' of the database of this project.
        If (WinGisMainForm.WeAreUsingTheIDB[Data]) Then
           WingisMainForm.IDB_Man.SaveDatabaseAs(SELF.Data, FileName1); // *.hsp file name.
{-- IDB}

        WinGISMainForm.ConvertToWgpWithLayers(Options,SerialNumber,
          ActiveProject,FileName1,FileName2,HSPLayers,HSDLayers,Files,TRUE,TRUE);
      end
      { convert the specified files }
      else WinGISMainForm.ConvertToWgp(Options,SerialNumber,
          FileName1,FileName2,Files,TRUE,TRUE);
    end;
  end;
  Settings.Free;
end;

Procedure TMDIChild.FileSaveAs2(ANewName:PChar;ALTProj:Boolean;AskForOverwrite:Boolean);
var AFName       : Array[0..fsPathName] of Char;
    Dir          : Array[0..fsPathName] of Char;
    DName        : Array[0..fsFileName+fsExtension] of Char;
    Ext          : Array[0..fsExtension] of Char;
    DoIt         : Boolean;
    Child        : TMDIChild;
    LTProj       : Bool;
    SendStr      : String;
    OldName      : Array[0..255] of Char;
    OldWasReadOnly : Boolean;
begin
  StrCopy(AFName,ANewName);
  LTProj:=ALTProj;
  FileSplit(AFName,Dir,DName,Ext);
  StrCat(DName,Ext);
  if StrIComp(AFName,FName)=0 then DoIt:=TRUE
  else if not CheckFileExist(AFName) then DoIt:=TRUE
  else begin
    Child:=WinGISMainForm.IsFileOpened(AFName);
    if Child=NIL then begin
      if IsFileWriteLocked(StrPas(AFName)) then begin
        MsgBox(Handle,4106,mb_IconExclamation or mb_OK,StrPas(AFName));
        DoIt:=FALSE;
      end
      else DoIt:=(not AskForOverwrite) or (MsgBox(Handle,1006,mb_IconQuestion or mb_YesNo,StrPas(AFName))=idYes);
    end
    else begin
      DoIt:=FALSE;
      if MsgBox(Handle,3614,mb_IconQuestion or mb_YesNo,StrPas(AFName))=idYes then BringWindowToTop(Child.Handle);
    end;
    {DoIt:=FALSE;}
  end;
  if DoIt then begin
    StrCopy(OldName,FName);
    StrCopy(FName,AFName);
    OldWasReadOnly:=WasReadOnly;
    WasReadOnly:=FALSE;

{++ IDB}
    // Before we can start 'save as' of the project, we have to make 'save as' of the database of this project.
    If (WinGisMainForm.WeAreUsingTheIDB[Data]) Then
       WingisMainForm.IDB_Man.SaveDatabaseAs(SELF.Data, FName);
{-- IDB}
    HasTitle:=True;
    if StoreData then begin
//      UserInterface.FileHistoryList.Add(StrPas(Data^.FName));
      SetWindowText(Handle,AFName);
      DDEHandler.SetDBSendStrings(DDECommPInfo,NIL);
      if DDEHandler.FSaveAs = 1 then DDEHandler.SendString('[PSA]['+StrPas(FName)+']')
      else begin
        FileSplit(FName,Dir,DName,Ext);
        SendStr:=StrPas(DName);
        DDEHandler.SendString('[PRW]['+SendStr+']');
      end;
      DDEHandler.SetDBSendStrings(DDECommAll,NIL);
      WinGISMainForm.SetCurrentTDocumentFilename(strpas(OldName),strpas(FName));
    end
    else begin
      StrCopy(FName,OldName);
      WasReadOnly:=OldWasReadOnly;
    end;
  end;
end;

Procedure TMDIChild.SelectTransparent;
begin
  Data^.SetTransparentSelect(MenuFunctions['SelectTransparent'].Checked,TRUE);
end;
  {$ENDIF} // <----------------- AXDLL
{$ENDIF}

Procedure TMDIChild.DrawSnapModeOnOff;
begin
  Data^.SnapState:=MenuFunctions['DrawSnapModeOnOff'].Checked;
end;

{++ Ivanoff NA2 BUILD#106}
{Change draw mode for new lines amd polygons}
Procedure TMDIChild.DrawOrthoOnOff;
Begin
     Data.NewPolyP.EndIt(Data.PInfo);
     {++Brovak Build 132 BUG 105}
     If  Data.SelRect^.IsOn then
         begin;                                
          Data.NewPoly^.EndIt(Data.Pinfo);
          Data.NewPoly^.PointInsert(Data.PInfo,Data.SelRect^.StartPos,TRUE);
          Data.NewPoly.StartPos:=  Data.SelRect^.StartPos;
          Data.SelRect^.Endit(Data.Pinfo);
          end;                                                   
      {--Brovak}
{++ Ivanoff OrthoDraw}
     Data.NewPoly.bDrawOrtho := NOT Data.NewPoly.bDrawOrtho;
     Data.PInfo.RedrawScreen(FALSE);
     MenuFunctions['DrawOrthoOnOff'].Checked := Data.NewPoly.bDrawOrtho;
  {-- Ivanoff}
{++++++ Brovak Build 132 BUG 105 +++++++}
  MenuFunctions['DrawRectBy3Points'].Checked:=false;
  MenuFunctions['DrawRectByRubberBox'].Checked:=false;                 
  Data^.bDraw3Points:=false;
  Data^.bDrawRubberBox:=false;                                       
End;

// Draw rectangle easy with RubberBox (with help 2 points)
Procedure TMDIChild.DrawRectByRubberBox;
 begin;
 ChangeDrawRubberBoxMode(Data);
 MenuFunctions['DrawRectByRubberBox'].Checked := not MenuFunctions['DrawRectByRubberBox'].Checked; //;Data.bOrthoInput;
 MenuFunctions['DrawRectBy3Points'].Checked:=false;
 MenuFunctions['DrawOrthoOnOff'].Checked:=false;
 Data.NewPoly.bDrawOrtho := FALSE;
 Data^.bDraw3Points:=false;
end;

// Draw rectangle easy with help 3 points
Procedure TMDIChild.DrawRectBy3Points;
 begin;
  ChangeDraw3PointMode(Data);
  MenuFunctions['DrawRectBy3Points'].Checked := not MenuFunctions['DrawRectBy3Points'].Checked; //;Data.bOrthoInput;
  MenuFunctions['DrawRectByRubberBox'].Checked:=false;
  MenuFunctions['DrawOrthoOnOff'].Checked:=false;
  Data.NewPoly.bDrawOrtho := FALSE;
  Data^.bDrawRubberBox:=false;
 end;

{---- Brovak -----}

Procedure TMDIChild.OnBringToFront;
begin
     InvalidateRect(Handle,@FInvalidRect,FALSE);
end;

Procedure TMDIChild.WMPaint(var Msg:TWMPaint);
 var  ScreenPixels   : TRect;
      PaintInfo      : TPaintStruct;
      TmpPoint       : TPoint;
      TmpDPoint      : TDPoint;
  Procedure AddArea;
    begin
      TmpPoint.X:=PaintInfo.rcPaint.Left;
      TmpPoint.Y:=PaintInfo.rcPaint.Bottom;
      Data^.PInfo^.ConvertToDraw(TmpPoint,TmpDPoint);
      RDRArea.CorrectByPoint(TmpDPoint);
      TmpPoint.X:=PaintInfo.rcPaint.Right;
      TmpPoint.Y:=PaintInfo.rcPaint.Top;
      Data^.PInfo^.ConvertToDraw(TmpPoint,TmpDPoint);
      RDRArea.CorrectByPoint(TmpDPoint);
    end;
  Procedure AddAreaDDE;
    begin
      TmpPoint.X:=PaintInfo.rcPaint.Left;
      TmpPoint.Y:=PaintInfo.rcPaint.Bottom;
      Data^.PInfo^.ConvertToDraw(TmpPoint,TmpDPoint);
      DDERDRArea.CorrectByPoint(TmpDPoint);
      TmpPoint.X:=PaintInfo.rcPaint.Right;
      TmpPoint.Y:=PaintInfo.rcPaint.Top;
      Data^.PInfo^.ConvertToDraw(TmpPoint,TmpDPoint);
      DDERDRArea.CorrectByPoint(TmpDPoint);
    end;
  begin
    {$IFNDEF WMLT}
      {$IFNDEF AXDLL} // <----------------- AXDLL
    DDEHandler.EnableDDEMLConversation(FALSE);                 {DDE-Kommunikation unterbinden}
      {$ENDIF} // <----------------- AXDLL
    {$ENDIF}
    {+++brovak}
    {$IFNDEF AXDLL} // <----------------- AXDLL
       if not WasReadOnly then
      if Data^.WasConverted=false
       then
        Data^.ConvertAnnotationToTexts;
    {$ENDIF} // <----------------- AXDLL
    {--brovak}

    BeginPaint(Handle,PaintInfo);
    if Msg.Unused <> 1 then begin
       Data^.SetStatusText(GetLangText(4002),TRUE);
    end;
    try
      InWMPaint:=True;
      with Data^.PInfo^ do begin
        if not FromGraphic and ExtCanvas.Cached and
            not ExtCanvas.IsInvalidated(PaintInfo.rcPaint) then
                ExtCanvas.DrawFromCache(PaintInfo.rcPaint)
        else begin
          ExtCanvas.Validate(PaintInfo.rcPaint);
          ExtCanvas.CachedDraw:=TRUE;
          ExtCanvas.FillRect(PaintInfo.rcPaint,IniFile.BgProjBrush); //++Sygsky bug #333
          ExtCanvas.BeginPaint;     
          try
            if DoRedraw then with Data^ do begin
              if {$IFNDEF AXDLL} {$IFNDEF WMLT}(not DDEHandler.InAction) and {$ENDIF} {$ENDIF} (Data^.Construct <> nil) and (not Data^.Construct^.InAction) then begin
                PInfo^.StartPaint(PaintInfo.hDC,PaintInfo.rcPaint,ScrollXOffset,ScrollYOffset);
                if HadNoRedraw then begin
                    HadNoRedraw:=FALSE;
                    AddArea;
                    Data^.PInfo^.RedrawAfterNoRedraw(RDRArea);
                end
                else begin
                   ScreenPixels:=GetClientRect;                                           {Bildschirmaufl�sung}
                   if FirstDraw then begin
                    if not Data^.BmpLoadAction then begin
                      Data^.BmpLoadAction:=TRUE;
                      StopPaint;
                      ScreenWidth:=ScreenPixels.Right-ScreenPixels.Left;
                      ScreenHeight:=ScreenPixels.Bottom-ScreenPixels.Top;
//++ Glukhov Bug#236 Build#144 14.12.00
//                      Data^.Layers^.LoadBmps(Data,CurrScreen,ScreenPixels,Colors256);
                      ProcSearchAndDeleteBmp(Data);
//-- Glukhov Bug#236 Build#144 14.12.00
                      Data^.BmpLoadAction:=FALSE;
                    end;
                  end
                  else begin
                    if not Data^.BmpLoadAction then begin
                      Data^.BmpLoadAction:=TRUE;
//++ Glukhov Bug#236 Build#144 14.12.00
{ Excluded the obviously unnecessary operations
                      if (IniFile^.BmpPixelsExact or IniFile^.BmpAreaExact) then begin
                        StopPaint;
                        ScreenWidth:=ScreenPixels.Right-ScreenPixels.Left;
                        ScreenHeight:=ScreenPixels.Bottom-ScreenPixels.Top;
                        Data^.Layers^.ReLoadBmps(Data,CurrScreen);
                      end                    
                      else begin
                        StopPaint;
                        ScreenWidth:=ScreenPixels.Right-ScreenPixels.Left;
                        ScreenHeight:=ScreenPixels.Bottom-ScreenPixels.Top;
                        Data^.Layers^.UnLoadReLoadBmps(Data,CurrScreen,ScreenPixels,Colors256);
                      end;
}
                      StopPaint;
                      ScreenWidth:=ScreenPixels.Right-ScreenPixels.Left;
                      ScreenHeight:=ScreenPixels.Bottom-ScreenPixels.Top;
//-- Glukhov Bug#236 Build#144 14.12.00
                      Data^.BmpLoadAction:=FALSE;
                    end;
                  end;
//++ Glukhov Bug#236 Build#144 14.12.00
//                  if not Data^.BmpLoadAction then begin
//                    Data^.BmpLoadAction:=TRUE;
//                    if IniFile^.AutoBmpReload <> 0 then ProcAutoShowBitmapOrFrame(Data,PaintInfo);
//                    Data^.BmpLoadAction:=FALSE;
//                  end;
//-- Glukhov Bug#236 Build#144 14.12.00
//                  ExtCanvas.FillRect(PaintInfo.rcPaint,GetStockObject(White_Brush));
                  ExtCanvas.FillRect(PaintInfo.rcPaint,IniFile.BgProjBrush); //++Sygsky bug#333

                  if WingisMainForm.AllowDraw then begin
                     Data^.DrawGraphic;
                  end;

                  RDRArea.Assign(MaxLongInt,MaxLongInt,-MaxLongInt,-MaxLongInt);
                  HadNoRedraw:=FALSE;
                  DDERDRArea.Assign(MaxLongInt,MaxLongInt,-MaxLongInt,-MaxLongInt);
                  DDEHadNoRdr:=FALSE;
                end;
                FromGraphic:=FALSE;
              end
              else begin
                PInfo^.StartPaint(PaintInfo.hDC,PaintInfo.rcPaint,ScrollXOffset,ScrollXOffset);
                StopPaint;
                if DDEHadNoRdr then AddAreaDDE
                else begin
                  DDEHadNoRDR:=TRUE;
                  TmpPoint.X:=PaintInfo.rcPaint.Left;
                  TmpPoint.Y:=PaintInfo.rcPaint.Bottom;
                  Data^.PInfo^.ConvertToDraw(TmpPoint,TmpDPoint);
                  DDERDRArea.CorrectByPoint(TmpDPoint);
                  TmpPoint.X:=PaintInfo.rcPaint.Right;
                  TmpPoint.Y:=PaintInfo.rcPaint.Top;
                  Data^.PInfo^.ConvertToDraw(TmpPoint,TmpDPoint);
                  DDERDRArea.CorrectByPoint(TmpDPoint);
                end;
              end;
            end                                           
            else with Data^ do begin
              PInfo^.StartPaint(PaintInfo.hDC,PaintInfo.rcPaint,ScrollXOffset,ScrollYOffset);
              PInfo^.StopPaint;
              if HadNoRedraw then AddArea
              else begin
                HadNoRedraw:=TRUE;
                TmpPoint.X:=PaintInfo.rcPaint.Left;
                TmpPoint.Y:=PaintInfo.rcPaint.Bottom;
                Data^.PInfo^.ConvertToDraw(TmpPoint,TmpDPoint);
                RDRArea.CorrectByPoint(TmpDPoint);
                TmpPoint.X:=PaintInfo.rcPaint.Right;
                TmpPoint.Y:=PaintInfo.rcPaint.Top;
                Data^.PInfo^.ConvertToDraw(TmpPoint,TmpDPoint);
                RDRArea.CorrectByPoint(TmpDPoint);
              end;
            end;
          finally
            ExtCanvas.EndPaint;
          end;
        end;

        if WingisMainForm.AllowDraw then begin
           ExtCanvas.CachedDraw:=FALSE;
           Data^.DrawSelection;
           ExtCanvas.CachedDraw:=TRUE;
        end;

        if FirstDraw or SendRedraw then begin
          FirstDraw:=FALSE;
          SendRedraw:=FALSE;
        end;
      end;
    finally
      EndPaint(Handle,PaintInfo);
      InWMPaint:=False;
      if Msg.Unused <> 1 then begin
         Data^.ClearStatusText;
      end;

      {$IFNDEF AXDLL} // <----------------- AXDLL
        {$IFNDEF WMLT}

      DDEHandler.EnableDDEMLConversation(TRUE);

      if (NotLoadedImagesCounter <> 0) then begin
          if (not WingisMainForm.HideWindow) and (not Data^.PInfo^.IsMU) then begin
             MsgBox(Handle,11731,mb_OK or mb_IconExclamation,'');
          end;
          NotLoadedImagesCounter:=0;
      end;

        {$ENDIF}
      {$ENDIF} // <----------------- AXDLL
    end;
end;

{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}
procedure TMDIChild.WMMDIActivate(var Msg: TWMMDIActivate);
var
AStyle: Integer;
begin
     if (Msg.ActiveWnd = Handle) and (biSystemMenu in BorderIcons) then begin
        AStyle:=GetWindowLong(Handle,GWL_STYLE);
        if (AStyle and WS_MAXIMIZE <> 0) and (AStyle and WS_SYSMENU = 0) then begin
           SetWindowLong(Handle, GWL_STYLE, AStyle or WS_SYSMENU);
        end;
     end;
     inherited;
end;
{$ENDIF}
{$ENDIF} // <----------------- AXDLL

{******************************************************************************+
  Procedure TMDIChild.OnGlobalOptionsChanged
--------------------------------------------------------------------------------
  Called if the ini-settings are changed by the global-settings-dialog. Updates
  the auto-save-timer and the project-options that are set to "use globals".
+******************************************************************************}
Procedure TMDIChild.OnGlobalOptionsChanged;
begin
  SetAutoSave;
  if Data<>NIL then Data^.OnGlobalOptionsChanged;
end;

{******************************************************************************+
  Procedure TMDIChild.UIExtensionUpdateMenus
--------------------------------------------------------------------------------
  Called by the user-interface to update the enabled-status of the menus.
+******************************************************************************}
Procedure TMDIChild.UIExtensionUpdateMenus(Sender: TObject);
{$IFNDEF AXDLL} // <----------------- AXDLL
var FixedLayer   : Boolean;
    ReadOnly     : Boolean;
    Selected     : Boolean;
    OneSelected  : Boolean;
    MoreSelected : Boolean;
    DocSelected  : Boolean;
    OmegaPoints  : Integer;
//++ Glukhov ObjAttachment 11.10.00
    IdxItem      : PIndex;
//-- Glukhov ObjAttachment 11.10.00
//++ Glukhov Bug#192 Build#167 10.10.01
    i, n         : Integer;
//-- Glukhov Bug#192 Build#167 10.10.01
{++ Ivanoff BUG#443, 447}
    bIAmUnderPrintRegionSelecting: Boolean;
{-- Ivanoff BUG#443, 447}
    CheckIndex: PIndex;
{$ENDIF} // <----------------- AXDLL
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
{++ Ivanoff BUG#443, 447}
  TRY
     bIAmUnderPrintRegionSelecting := Data.MenuHandler IS TPrintHandler;
  EXCEPT
     bIAmUnderPrintRegionSelecting := FALSE;
  END;
{-- Ivanoff BUG#443, 447}

  with MenuFunctions do begin
    FixedLayer:=Data^.Layers^.TopLayer^.GetState(sf_Fixed);
    ReadOnly:=IsReadOnly;
    Selected:=(Data^.Layers^.SelLayer^.SelInfo.Count>0);
    OneSelected:=Data^.Layers^.SelLayer^.SelInfo.HSPCount=1;
    MoreSelected:=Data^.Layers^.SelLayer^.SelInfo.Count>1;
    DocSelected:=False;

     GroupEnabled[9753]:=true;

//     GroupVisible[9501]:=(Data <> nil) and (not Data^.FMan.Active);

    {$IFNDEF WMLT}
    GroupVisible[5001]:=False;
    GroupVisible[5002]:=False;
    //MenuFunctions['FileSaveAs'].Enabled:=not IsOpenedReadOnly;
    //MenuFunctions['FileSaveAll'].Enabled:=True;
    {$ELSE}
    if not LTOpenFile then begin
       MenuFunctions['FileOpen'].Enabled:=False;
    end;
    {$ENDIF}

{++ Brovak - MOD# Copy part of selected  Image}
   GroupEnabled[1060]:=PLayer(Data^.PInfo^.Objects)^.HasObjectType(ot_Image);
{-- Brovak}
   GroupEnabled[1061] := GroupEnabled[1060]; //++ Sygsky for SuperImages

 {brovak OLE}
     GroupVisible[3232]:=true;
     GroupEnabled[3232]:=not ReadOnly and not FixedLayer;
     MenuFunctions['EditOleObj'].Visible:= (OneSelected) and (Data^.Layers^.SelLayer^.SelInfo.SelectedType=ot_OleObj) ;
     MenuFunctions['EditOleObj'].Enabled:=MenuFunctions['EditOleObj'].Visible and (not ReadOnly and not FixedLayer);
  {brovak}


   //++ Glukhov ObjAttachment 11.10.00
    if (not ReadOnly) and (Data^.SymbolMode=sym_Project) then begin
      GroupVisible[8]:= True;             // Attach To
      GroupVisible[9]:= False;            // Detach From

      if Data^.Layers^.SelLayer^.SelInfo.Count > 0 then begin
        IdxItem:=Data.Layers.IndexObject(Data.PInfo, Data^.Layers^.SelLayer^.Data^.At(0));
        if (IdxItem <> nil) and (IdxItem.GetObjType in [ot_Text,ot_Symbol,ot_BusGraph]) then
        begin
          GroupEnabled[1280]:= True;      // Attach/Detach
          if IdxItem.GetState( sf_Attached ) then begin
            GroupVisible[8]:= False;      // Attach To
            GroupVisible[9]:= True;       // Detach From
          end;
        end
        else GroupEnabled[1280]:= False;  // Attach/Detach
        if (IdxItem <> nil) and (Data^.MultiMedia <> nil) then begin
           DocSelected:=Data^.MultiMedia^.HasEntriesForID(IdxItem^.Index);
        end;
      end;
    end
    else begin
      GroupVisible[8]:= False;    // Attach To
      GroupVisible[9]:= False;    // Detach From
    end;
//-- Glukhov ObjAttachment 11.10.00
//++ Glukhov Bug#468 Build#162 16.09.01
    if (not ReadOnly) and (Data^.SymbolMode=sym_Project) then begin
      if (Data^.Layers^.SelLayer^.SelInfo.SelectCount[ot_Text] > 0)
      then  GroupVisible[10]:= True   // Advanced text
      else  GroupVisible[10]:= False;
      if OneSelected
      then  GroupEnabled[1281]:= True // Advanced text
      else  GroupEnabled[1281]:= False;
    end
    else begin
      GroupVisible[10]:= False;   // Advanced text
    end;
//-- Glukhov Bug#468 Build#162 16.09.01

{++ Moskaliov BUG#469 BUILD#100 11.01.00}
//    GroupEnabled[4]:=TRUE;
//    GroupEnabled[5]:=TRUE;

      if IsOpenedReadOnly then begin
         Data^.Modified:=False;
      end;

      if Data^.Modified then
        if Data^.SymbolMode=sym_SymbolMode then
          begin
            GroupEnabled[4]:=FALSE;       // FileSave group
            GroupEnabled[5]:=FALSE;       // FileSaveAll group
            GroupEnabled[1045]:=TRUE;     // StoreSymbol group
          end
        else
          begin
            GroupEnabled[4]:=TRUE;
            GroupEnabled[5]:=TRUE;
            GroupEnabled[1045]:=FALSE;
          end
      else
        begin
          GroupEnabled[4]:=FALSE;
          GroupEnabled[5]:=TRUE;
          GroupEnabled[1045]:=FALSE;
        end;
    MenuFunctions['EditSelectAll'].Enabled:=not IniFile^.DoOneSelect;
{-- Moskaliov BUG#469 BUILD#100 11.01.00}
    GroupEnabled[1000]:=TRUE;
{++ Moskaliov BUG#148 BUILD#100 10.01.00}
//    GroupEnabled[1001]:=Data^.SymbolMode=sym_SymbolMode;
      GroupEnabled[1001]:=not (Data^.SymbolMode=sym_SymbolMode);
{-- Moskaliov BUG#148 BUILD#100 10.01.00}
{brovAK}
    GroupEnabled[1971]:= Selected;
{BROVAK}
     {+++Brovak BUG 526}
     if Data^.SymbolMode= sym_SymbolMode
       then  GroupEnabled[1002]:=TRUE;
     {---Brovak}
    if Data^.SymbolMode=sym_Project then begin
      GroupEnabled[1002]:=TRUE;
      GroupEnabled[1006]:=PLayer(Data^.PInfo^.Objects)^.HasObjectType(ot_Image);
      {$IFNDEF WMLT}

      DBLoaded:=DDEHandler.GetDBLoaded;
      DDEMLLoaded:=DDEHandler.GetDDEMLLoaded;

      if (DBLoaded>0) or (DDEMLLoaded>0) or (WinGisMainForm.WeAreUsingTheIDB[Data]) then begin
        if (DDEHandler.DDEOk) or (WinGisMainForm.WeAreUsingTheIDB[Data]) then begin
          GroupEnabled[1008]:=TRUE;
          GroupEnabled[1023]:=OneSelected and ((DBLoaded>0) or (DDEMLLoaded>0) or (WinGisMainForm.WeAreUsingTheIDB[Data]));
          GroupEnabled[1042]:=((DBLoaded>0) or (DDEMLLoaded>0));
          GroupEnabled[1036]:=Selected and (DDEHandler.FNoDBAutoIn or WinGisMainForm.WeAreUsingTheIDB[Data]);
          if DDEHandler.DoAutoIns then MenuFunctions['DatabaseAutoEditInsert'].Checked:=TRUE;
          GroupEnabled[1029]:=Selected and not IniFile^.DoOneSelect;
          GroupEnabled[1043]:=(DDEHandler.FMoreSelect) and ((DBLoaded>0) or (DDEMLLoaded>0));
          if IniFile^.DoOneSelect then MenuFunctions['DatabaseAutoMonitoring'].Checked:=TRUE;
          GroupEnabled[1032]:=Selected and not ReadOnly and not FixedLayer;
        end;
        if (((DBLoaded>0) or (DDEMLLoaded>0)) and (not DDEHandler.DDEOk)) then begin
             GroupEnabled[1007]:=TRUE;
        end;
      end;
      {$ENDIF}
      if not ReadOnly then begin
        GroupEnabled[1009]:=TRUE;
        GroupEnabled[1024]:=OneSelected;
        GroupVisible[8001]:=(OneSelected) and (Data^.HasLinkedObj(Data.Layers.SelLayer.Data.At(0)));
        if not FixedLayer then GroupEnabled[1015]:=TRUE;
        GroupEnabled[1011]:=TRUE;
        GroupEnabled[1046]:=OneSelected and (Data^.Layers^.SelLayer^.SelInfo.SelectedType=ot_CPoly);
//++ Glukhov Bug#442 Build#161 30.05.01
//        GroupEnabled[1025]:=OneSelected and (Data^.Layers^.SelLayer^.SelInfo.SelectedType=ot_Poly);
        GroupEnabled[1025]:= Selected and
          (Data.Layers.SelLayer.SelInfo.Count = Data.Layers.SelLayer.SelInfo.SelectCount[ot_Poly]);
//-- Glukhov Bug#442 Build#161 30.05.01
//++ Glukhov Bug#192 Build#161 29.05.01
//        GroupEnabled[1054]:= OneSelected and
//          ( Data^.Layers^.SelLayer^.SelInfo.SelectedType in [ot_Arc, ot_Circle, ot_Spline] );
        GroupEnabled[1054]:= Selected and (Data^.Layers^.SelLayer^.SelInfo.Count =
          Data^.Layers^.SelLayer^.SelInfo.SelectCount[ot_Arc] +
          Data^.Layers^.SelLayer^.SelInfo.SelectCount[ot_Circle] +
          Data^.Layers^.SelLayer^.SelInfo.SelectCount[ot_Spline]);
//        GroupEnabled[1055]:= OneSelected and
//          ( Data^.Layers^.SelLayer^.SelInfo.SelectedType in [ot_Circle] );
        GroupEnabled[1055]:= Selected and (Data^.Layers^.SelLayer^.SelInfo.Count =
          Data^.Layers^.SelLayer^.SelInfo.SelectCount[ot_Circle] +
          Data^.Layers^.SelLayer^.SelInfo.SelectCount[ot_Poly]);
        if GroupEnabled[1055] then begin
          n:= Data.Layers^.SelLayer^.Data^.GetCount;
          for i:= n-1 downto 0 do begin
            IdxItem:= Data.Layers.IndexObject( Data.PInfo, Data.Layers.SelLayer.Data.At( i ) );
            if (IdxItem.GetObjType in [ot_Poly]) then begin
              if not PPoly( IdxItem ).IsClosed then begin
                GroupEnabled[1055]:= False;
                Break;
              end;
            end;
          end;
        end;
//-- Glukhov Bug#192 Build#161 29.05.01

        GroupVisible[1002]:=(Data^.Layers^.SelLayer^.SelInfo.SelectCount[ot_CPoly] > 1);
        GroupVisible[8002]:=((Data^.Layers^.SelLayer^.SelInfo.SelectCount[ot_CPoly] + Data^.Layers^.SelLayer^.SelInfo.SelectCount[ot_Image]) > 1);
        GroupVisible[1003]:=OneSelected and (Data^.Layers^.SelLayer^.SelInfo.SelectedType=ot_Poly);
        GroupVisible[1048]:=OneSelected and (Data^.Layers^.SelLayer^.SelInfo.SelectedType=ot_CPoly);

        MenuFunctions['ExtrasCombineSelectedPolygons'].Visible:=(Data^.Layers^.SelLayer^.SelInfo.SelectCount[ot_CPoly] > 1);
        MenuFunctions['ExtrasReplacePolyPoints'].Visible:=((Data^.Layers^.SelLayer^.SelInfo.SelectCount[ot_Poly]) + (Data^.Layers^.SelLayer^.SelInfo.SelectCount[ot_CPoly]) >= 1);
        MenuFunctions['ConvertSymbolToObjects'].Enabled:=(not Data^.PInfo^.IsMU) and (Data^.Layers^.SelLayer^.SelInfo.SelectCount[ot_Symbol] >= 1);
        MenuFunctions['OpenAttachedDocument'].Enabled:=DocSelected;
        //MenuFunctions['EditMoveToLayer'].Enabled:=Selected;

        MenuFunctions['ExtrasPolygonPart'].Enabled:=(WingisLicense >= WG_PROF) and
            ((OneSelected and (Data^.Layers^.SelLayer^.SelInfo.SelectedType=ot_CPoly))
            or (((Data^.Layers^.SelLayer^.SelInfo.SelectCount[ot_Poly] = 1) and (Data^.Layers^.SelLayer^.SelInfo.SelectCount[ot_CPoly] >= 1))));

        MenuFunctions['ExtrasFreePolygonPart'].Enabled:=(WingisLicense >= WG_STD) and
            ((OneSelected and (Data^.Layers^.SelLayer^.SelInfo.SelectedType=ot_CPoly))
            or (((Data^.Layers^.SelLayer^.SelInfo.SelectCount[ot_Poly] = 1) and (Data^.Layers^.SelLayer^.SelInfo.SelectCount[ot_CPoly] >= 1))));

      end;
    end;
    if not ReadOnly then begin                          
      GroupEnabled[1010]:=TRUE;
      GroupEnabled[1012]:=MoreSelected and (Data^.Layers^.SelLayer^.SelInfo.SelectedType=ot_CPoly);
      GroupEnabled[1013]:=PLayer(Data^.PInfo^.Objects)^.HasObjectType(ot_Image);
      if not FixedLayer then begin
        GroupEnabled[1018]:=TRUE;
        GroupEnabled[1019]:=Data^.Layers^.SelLayer^.HasObjectType(ot_OLEObj);
        GroupEnabled[1020]:=PLayer(Data^.PInfo^.Objects)^.HasObjectType(ot_Text);
      end;
    end;
    if not ReadOnly and not FixedLayer then begin
          GroupEnabled[1014]:=TRUE;
          GroupEnabled[1016]:=Data^.Digitize^.RefsInput;
          //GroupEnabled[1017]:=TRUE;
          GroupEnabled[7890]:=TRUE;

//          GroupEnabled[1099]:=true;

          if Data.SymbolMode <> sym_Omega then
          begin;
             GroupEnabled[1099]:=true;
             GroupEnabled[1017]:=true;
             if OneSelected then
             begin;
                CheckIndex := Data^.GiveMeSourceObjectFromSelect(Data.Layers^.SelLayer.Data.At(0));
                if CheckIndex <> nil then
                begin;
                   if (CheckIndex.GetObjType in [ot_poly,ot_cpoly]) then
                      GroupEnabled[1017]:=TRUE
                end
                else
                   try
                      if Data^.OldSnapItem <> nil then
                         if Data^.OldSnapItem.GetObjtype in [ot_poly, ot_cpoly] then
                            GroupEnabled[1017] := TRUE
                   except
                   end;
             end;
          end;

          MenuFunctions['ExtrasSplitLine'].Visible:=(Data^.Layers^.SelLayer^.SelInfo.SelectedType=ot_Poly) and (OneSelected);
          GroupVisible[1049]:=(Data^.Layers^.SelLayer^.SelInfo.SelectedType=ot_CPoly);
       {++ brovak bug #105 BUILD 105}
          if Data^.ActualMen=mn_CPoly then
             begin
                MenuFunctions['DrawRectByRubberBox'].Visible:=TRUE;
                MenuFunctions['DrawRectBy3Points'].Visible:=TRUE;
                if Data^.NewPoly.Data.Count>2 then
                   MenuFunctions['DrawRectByRubberBox'].Enabled:=FALSE
                else
                   MenuFunctions['DrawRectByRubberBox'].Enabled:=TRUE;
                if Data^.NewPoly.Data.Count>3 then
                   MenuFunctions['DrawRectBy3Points'].Enabled:=FALSE
                else
                   MenuFunctions['DrawRectBy3Points'].Enabled:=TRUE;
             end
          else
             begin
                MenuFunctions['DrawRectByRubberBox'].Visible:=FALSE;
                MenuFunctions['DrawRectBy3Points'].Visible:=FALSE;
             end;
       {-- brovak}

{++ Moskaliov BUG#312 BUILD#95 01.12.99}
          GroupEnabled[1044]:= not (Data^.SymbolMode=sym_SymbolMode);
{-- Moskaliov BUG#312 BUILD#95 01.12.99}

          if Data^.PInfo^.IsMU then begin
             GroupEnabled[1028]:=False;
             GroupEnabled[1030]:=OneSelected;
             GroupEnabled[1031]:=False;
             GroupEnabled[1033]:=False;
             GroupEnabled[1034]:=OneSelected;
             GroupEnabled[1035]:=(OneSelected) and (not FixedLayer);
          end
          else begin
             GroupEnabled[1028]:=(Selected) and (not Data^.Layers^.SelLayer^.HasObjectType(ot_OLEObj));
             GroupEnabled[1030]:=Selected;
             GroupEnabled[1031]:=(Selected) and (not Data^.Layers^.SelLayer^.HasObjectType(ot_OLEObj));
             GroupEnabled[1033]:=IsClipboardFormatAvailable(WinGisFormat) or CanPaste;
             GroupEnabled[1034]:=Selected;
             GroupEnabled[1035]:=Selected and not FixedLayer;
          end;

    end;
    GroupEnabled[1003]:=Data^.Digitize^.RefsInput;

    if Data^.PInfo^.IsMU then begin
       GroupEnabled[1026]:=OneSelected;
       MenuFunctions['SelectDeselectAll'].Enabled:=Selected;
    end
    else begin
       GroupEnabled[1026]:=Selected;
    end;

    if OneSelected then
       begin
          GroupEnabled[1021]:=(Data^.Layers^.SelLayer^.SelInfo.SelectedType=ot_Text)
             and (CompareText(PToStr(Data^.Layers^.TopLayer^.Text),IniFile^.ProjLayerName)=0)
             and (Data^.SymbolMode=sym_Project);
          GroupEnabled[1022]:=Data^.Layers^.SelLayer^.SelInfo.SelectedType=ot_Image;
          {$IFNDEF WMLT}
          if Data^.MultiMedia <> nil then begin
             GroupEnabled[1036]:=Data^.Multimedia^.HasEntriesForID(Data^.Layers^.SelLayer^.Data^.At(0)^.Index);
          end;
          {$ENDIF}
       end;
    GroupEnabled[1038]:=Colors256 and ((WhichPalette=vp_Bmp) or GroupEnabled[1022]);
    GroupEnabled[1037]:=Colors256;
    with TooltipThread do
       begin
          GroupVisible[1]:=NewEditPossible;
          GroupVisible[2]:=DeletePossible;
          GroupVisible[3]:=NewEditPossible or DeletePossible or FixPossible;
          GroupVisible[4]:=FixPossible;
{++ Moskaliov BUG#148 BUILD#100 10.01.00}
          GroupVisible[1001]:=NewEditPossible;
{-- Moskaliov BUG#148 BUILD#100 10.01.00}
       end;
    if Data^.MenuHandler is TProjMenuHandler then
       begin
          GroupEnabled[1039]:=TProjMenuHandler(Data^.MenuHandler).CoordinateInput;
{++ Moskaliov BUG#90 BUILD#150 05.01.01}
          if Data^.MenuHandler is TPrintHandler then
             begin
                GroupEnabled[5]    := FALSE; { Disable Database options }
                GroupEnabled[1017] := FALSE; { Disable Draw tools }
                GroupEnabled[1044] := FALSE; { Disable Symbols insertion }
             end;
{-- Moskaliov BUG#90 BUILD#150 05.01.01}
       end
    else
       GroupEnabled[1039]:=Data^.ActualMen in mnCoord;   // 1039 - ExtrasCoordinateInput function
    {$IFNDEF WMLT}
    //if (ThreeDChild = NIL) and (WinGISMainForm.ActChild3D = NIL) then
       GroupEnabled[1050]:=TRUE;
    //else
    //   GroupEnabled[1050]:=FALSE;
    {$ENDIF}
    GroupEnabled[1051]:=FALSE;
    if ((Data^.ActualMen = mn_PartParallel) or (Data^.ActualMen = mn_PartPuffer) or
        (Data^.ActualMen = mn_PartFree)) and (Data^.NewPoly^.Data^.Count > 1) then
       GroupEnabled[1047]:=TRUE
    else
       GroupEnabled[1047]:=FALSE;

    {$IFNDEF WMLT}
{++Sygsky: Is Omega interface on air ?}
    if Data.SymbolMode = sym_Omega then
    begin
      { Omega tool bar options set }
      GroupEnabled[2000] := True;            { Info is always visible }
      OmegaPoints := OmegaCount;             { Number of points existing }
      GroupEnabled[2001] := OmegaPoints > 0; { Enable Move & Select }
{$ifdef FUTURE}
      GroupEnabled[2002] := OmegaPoints > 1; { Enable Next. & Prev. operations }
      GroupEnabled[2004] := Selected;  { Enable Delete operation }
{$endif}
      case OmegaParam of
        1: GroupEnabled[2003] := OmegaPoints > 2 ;
        2..4:GroupEnabled[2003] := OmegaPoints > OmegaParam + 2;
      end;
      GroupEnabled[5]    := False; { Disable Database options }
      GroupEnabled[1017] := False; { Disable Draw tools }
      GroupEnabled[7890]:=TRUE;
      GroupEnabled[1044] := False; { Disable Symbols insertion }
    end;
{--Sygsky: Omega }
    {$ENDIF}

{++ IDB}
{$IFNDEF WMLT}
{The menu of the user interface of the Intenal Database.}

      If (Data.SymbolMode = sym_Project) AND ((WinGisMainForm.WeAreUsingTheIDBUserInterface[Data]) or (CoreConnected)) Then Begin
         GroupVisible[9999] := WinGisMainForm.WeAreUsingTheIDBUserInterface[Data];

         MenuFunctions['IDB_ShowLayerTable'].Enabled := WinGisMainForm.WeAreUsingTheIDBUserInterface[Data] AND (NOT bIAmUnderPrintRegionSelecting);
         MenuFunctions['IDB_Monitoring'].Enabled := Selected AND (NOT bIAmUnderPrintRegionSelecting); //Data.Layers.SelLayer.Data.GetCount > 0;}
         MenuFunctions['IDB_Info'].Enabled := {Selected AND (NOT MoreSelected) AND} (NOT bIAmUnderPrintRegionSelecting);
{$IFDEF RUSSIA}
         MenuFunctions['IDB_MakeAnnotations'].Enabled := Selected; //Data.Layers.SelLayer.Data.GetCount > 0;

         MenuFunctions['DatabaseMonitoring'].Enabled := Selected; //Data.Layers.SelLayer.Data.GetCount > 0;
         MenuFunctions['DatabaseAnnotate'].Enabled := Selected; //Data.Layers.SelLayer.Data.GetCount > 0;
         MenuFunctions['DatabaseChangeTable'].Enabled := TRUE;
         MenuFunctions['DatabaseActivate'].Enabled := TRUE;
{$ENDIF}

         if CoreConnected then begin
            MenuFunctions['DatabaseMonitoring'].Enabled := Selected;
            MenuFunctions['DatabaseAutoMonitoring'].Enabled := True;
            MenuFunctions['DatabaseAutoEditInsert'].Enabled := True;
            MenuFunctions['DatabaseEditInsert'].Enabled := Selected;
         end;

         End;
{$IFDEF RUSSIA}
      If (Data.SymbolMode = sym_Project) AND ((DBLoaded<1) OR (DDEMLLoaded<1)) AND (NOT WinGisMainForm.WeAreUsingTheIDBUserInterface[Data]) Then Begin
         MenuFunctions['DatabaseChangeTable'].Enabled := FALSE;
         MenuFunctions['DatabaseActivate'].Enabled := FALSE;
         End;
{$ENDIF}
{The menu items of UnDelete function.}
      MenuFunctions['IDB_UndoFunction'].Visible := WinGisMainForm.WeAreUsingUndoFunction[Data];
      MenuFunctions['IDB_RedoFunction'].Visible := WinGisMainForm.WeAreUsingUndoFunction[Data];
      If WinGisMainForm.WeAreUsingUndoFunction[Data] Then Begin
         TRY
            MenuFunctions['IDB_UndoFunction'].Enabled := (Data.SymbolMode = sym_Project)
                                                         AND
                                                         (SELF.Data.ActualMen = mn_Select)
                                                         AND
                                                         WinGisMainForm.UndoRedo_Man.ThereAreDataForRestoring(SELF.Data);
         EXCEPT
            MenuFunctions['IDB_UndoFunction'].Visible := FALSE;
         END;
         TRY
            MenuFunctions['IDB_RedoFunction'].Enabled := (Data.SymbolMode = sym_Project)
                                                         AND
                                                         (SELF.Data.ActualMen = mn_Select)
                                                         AND
                                                         WinGisMainForm.UndoRedo_Man.ThereAreDataForRedoing(SELF.Data);
         EXCEPT
            MenuFunctions['IDB_RedoFunction'].Visible := FALSE;
         END;
         End;
{$ENDIF}
{-- IDB}
  end;

  MenuFunctions['FileImportMenu'].Enabled:=(not Data^.PInfo^.IsMU);

  MenuFunctions['FileSave'].Enabled:=(WingisLicense >= WG_STD) and (not Data^.PInfo^.IsMU) and (Data^.Modified) and (Data^.SymbolMode <> sym_SymbolMode);
  MenuFunctions['FileSaveAs'].Enabled:=(WingisLicense >= WG_STD) and (not Data^.PInfo^.IsMU);
  MenuFunctions['FileSaveAll'].Enabled:=(WingisLicense >= WG_STD) and (not Data^.PInfo^.IsMU);

  MenuFunctions['InsertPicture'].Enabled:=(WingisLicense >= WG_STD) and (not Data^.PInfo^.IsMU);
  MenuFunctions['InsertTABs'].Enabled:=(WingisLicense >= WG_STD) and (not Data^.PInfo^.IsMU);

  MenuFunctions['NewImage'].Enabled:=(not Data^.PInfo^.IsMU);
  MenuFunctions['EditText'].Enabled:=(not Data^.PInfo^.IsMU);
  MenuFunctions['EditTxtAdvanced'].Enabled:=(not Data^.PInfo^.IsMU);

  MenuFunctions['InsertOLEObj'].Enabled:=(WingisLicense >= WG_STD) and (not Data^.PInfo^.IsMU);
  MenuFunctions['InsertAttachments'].Enabled:=(WingisLicense >= WG_STD) and (not Data^.PInfo^.IsMU);

  MenuFunctions['EditAttachTo'].Enabled:=(not Data^.PInfo^.IsMU);
  MenuFunctions['EditDetachFrom'].Enabled:=(not Data^.PInfo^.IsMU);
  MenuFunctions['ExtrasTrimm'].Enabled:=(not Data^.PInfo^.IsMU);

  MenuFunctions['ConvertToCurve'].Enabled:=(not Data^.PInfo^.IsMU);
  MenuFunctions['ConvertToSymbol'].Enabled:=(not Data^.PInfo^.IsMU);

  MenuFunctions['ExtrasTextReplace'].Enabled:=(not Data^.PInfo^.IsMU);
  MenuFunctions['ExtrasAreaCut'].Enabled:=(not Data^.PInfo^.IsMU);

  MenuFunctions['ExtrasDKMCheck'].Enabled:=(WingisLicense >= WG_STD) and (not Data^.PInfo^.IsMU);

  MenuFunctions['ExtrasGenerate'].Enabled:=(not Data^.PInfo^.IsMU);
  MenuFunctions['ExtrasAutomaticSnap'].Enabled:=(not Data^.PInfo^.IsMU);

  MenuFunctions['ExtrasTABConverter'].Enabled:=(WingisLicense >= WG_STD) and (not Data^.PInfo^.IsMU);
  MenuFunctions['RasterTransformationMenu'].Enabled:=(WingisLicense >= WG_STD) and (not Data^.PInfo^.IsMU);

  MenuFunctions['ExtrasProjectOptions'].Enabled:=(not Data^.PInfo^.IsMU);
  MenuFunctions['ExtrasOrganize'].Enabled:=(not Data^.PInfo^.IsMU);

  MenuFunctions['DatabaseConnect'].Enabled:=(not Data^.PInfo^.IsMU);

  MenuFunctions['ExtrasApplyTM'].Enabled:=(WingisLicense >= WG_STD) and (not Data^.PInfo^.IsMU);

  MenuFunctions['DrawConstructions'].Enabled:=(not Data^.PInfo^.IsMU);
  MenuFunctions['DrawDimensioning'].Enabled:=(not Data^.PInfo^.IsMU);

  MenuFunctions['ViewLayerManager'].Enabled:=(not Data^.PInfo^.IsMU);

  MenuFunctions['DrawConstructions'].Enabled:=(WingisLicense >= WG_STD);
  MenuFunctions['DrawDimensioning'].Enabled:=(WingisLicense >= WG_STD);

  MenuFunctions['FileImportASCIIDX'].Enabled:=(WingisLicense >= WG_STD);
  MenuFunctions['FileImportDXFDX'].Enabled:=(WingisLicense >= WG_STD);
  MenuFunctions['FileImportSHPDX'].Enabled:=(WingisLicense >= WG_STD);

  MenuFunctions['FileExportASCIIDX'].Enabled:=(WingisLicense >= WG_STD);
  MenuFunctions['FileExportAMPDX'].Enabled:=(WingisLicense >= WG_STD);
  MenuFunctions['FileExportDXFDX'].Enabled:=(WingisLicense >= WG_STD);
  MenuFunctions['FileExportSHPDX'].Enabled:=(WingisLicense >= WG_STD);

  MenuFunctions['FilePrint'].Enabled:=(WingisLicense >= WG_STD);

  MenuFunctions['CutSelectedPartOfImage'].Enabled:=(WingisLicense >= WG_PROF);
  MenuFunctions['ExtrasExplodeToPoints'].Enabled:=(WingisLicense >= WG_PROF) and (not Data^.PInfo^.IsMU);
  MenuFunctions['ExtrasExplodeToSymbols'].Enabled:=(WingisLicense >= WG_PROF) and (not Data^.PInfo^.IsMU);
  MenuFunctions['ExtrasCreateCutPoints'].Enabled:=(WingisLicense >= WG_PROF) and (not Data^.PInfo^.IsMU);
  MenuFunctions['ExtrasPolyPointCut'].Enabled:=(WingisLicense >= WG_PROF) and (not Data^.PInfo^.IsMU);
  MenuFunctions['ExtrasCombinePolygons'].Enabled:=(WingisLicense >= WG_PROF) and (not Data^.PInfo^.IsMU);
  MenuFunctions['ExtrasCombinePolygonsWithTexts'].Enabled:=(WingisLicense >= WG_PROF) and (not Data^.PInfo^.IsMU);
  MenuFunctions['MultiCopyAndSave'].Enabled:=(WingisLicense >= WG_PROF);
  MenuFunctions['MultiCopyCombineAndSave'].Enabled:=(WingisLicense >= WG_PROF);
  MenuFunctions['EditRotateSymbols'].Enabled:=(WingisLicense >= WG_PROF) and (not Data^.PInfo^.IsMU);
  MenuFunctions['ExtrasCreatePolyline'].Enabled:=(WingisLicense >= WG_PROF) and (not Data^.PInfo^.IsMU) and (Data^.Layers^.SelLayer^.SelInfo.SelectCount[ot_Poly] > 1);
  MenuFunctions['ExtrasGPSLog'].Enabled:=(WingisLicense >= WG_PROF) and (not Data^.PInfo^.IsMU);
  MenuFunctions['ViewSaveAsImage'].Enabled:=(WingisLicense >= WG_PROF);
  MenuFunctions['ExtrasExplode'].Enabled:=(WingisLicense >= WG_PROF) and (not Data^.PInfo^.IsMU);
  MenuFunctions['ExtrasCombine'].Enabled:=(WingisLicense >= WG_PROF) and (not Data^.PInfo^.IsMU);
  MenuFunctions['ExtrasGenerateObjects'].Enabled:=(WingisLicense >= WG_PROF) and (not Data^.PInfo^.IsMU);
  MenuFunctions['DigitMenu'].Enabled:=(WingisLicense >= WG_PROF);
  MenuFunctions['DigitDigitizerOnOff'].Enabled:=(WingisLicense >= WG_PROF);
  MenuFunctions['DigitCalibrateDigitizer'].Enabled:=(WingisLicense >= WG_PROF);
  MenuFunctions['DigitTransformation'].Enabled:=(WingisLicense >= WG_PROF);
  MenuFunctions['RasterTransformationHelmert'].Enabled:=(WingisLicense >= WG_PROF);
  MenuFunctions['ShowOmega'].Enabled:=(WingisLicense >= WG_PROF);
  MenuFunctions['ExtrasSplitLine'].Enabled:=(WingisLicense >= WG_PROF);
  MenuFunctions['ExtrasReplacePolyPoints'].Enabled:=(WingisLicense >= WG_PROF);
  MenuFunctions['ExtrasAreaCutSelected'].Enabled:=(WingisLicense >= WG_PROF);
  MenuFunctions['ExtrasObjectCut'].Enabled:=(WingisLicense >= WG_PROF) and (not Data^.PInfo^.IsMU);
  MenuFunctions['ExtrasCombineSelectedPolygons'].Enabled:=(WingisLicense >= WG_PROF);
  MenuFunctions['ExtrasGenerateArea'].Enabled:=(WingisLicense >= WG_PROF) and (not Data^.PInfo^.IsMU);

  MenuFunctions['FileImportTXT'].Enabled:=(WingisLicense >= WG_PROF);
  MenuFunctions['FileImportE00DX'].Enabled:=(WingisLicense >= WG_PROF);
  MenuFunctions['FileImportMIFDX'].Enabled:=(WingisLicense >= WG_PROF);
  MenuFunctions['FileImportDGNDX'].Enabled:=(WingisLicense >= WG_PROF);

  MenuFunctions['FileExportE00DX'].Enabled:=(WingisLicense >= WG_PROF);
  MenuFunctions['FileExportMIFDX'].Enabled:=(WingisLicense >= WG_PROF);
  MenuFunctions['FileExportOVL'].Enabled:=(WingisLicense >= WG_PROF);
  MenuFunctions['FileExportKML'].Enabled:=(WingisLicense >= WG_PROF);
                                                                                     
  MenuFunctions['ExtrasIsoWizzard'].Enabled:=(WingisLicense >= WG_PROF) and (not Data^.PInfo^.IsMU);

  MenuFunctions['ExtrasGPSConnect'].Enabled:=((WingisLicense >= WG_PROF) and
                                             (OneSelected) and (Data^.Layers^.SelLayer^.SelInfo.SelectedType=ot_Symbol) and
                                             (Data^.PInfo^.ProjectionSettings.ProjectProjection <> 'NONE') and
                                             (GPSConnection <> nil) and
                                             (GPSConnection.ComPortAvailable))
                                             or
                                             ((GPSConnection <> nil) and
                                             (GPSConnection.Active));

  MenuFunctions.GroupEnabled[2325]:=MenuFunctions['VEOnOff'].Checked;
               
  MenuFunctions['VEClearCache'].Enabled:=MenuFunctions['VEOnOff'].Checked;
  MenuFunctions['VEFillCache'].Enabled:=(MenuFunctions['VEOnOff'].Checked);

  MenuFunctions['VESLMap'].Enabled:=MenuFunctions.GroupEnabled[2325] and (WingisLicense >= WG_PROF);
  MenuFunctions['VESLExp'].Enabled:=MenuFunctions.GroupEnabled[2325] and (WingisLicense >= WG_PROF);

  MenuFunctions['VEDBGeoCode'].Enabled:=MenuFunctions.GroupEnabled[2325] and (WingisLicense >= WG_PROF) and (not VEForm.IsOffline);
  MenuFunctions['VEGeoCode'].Enabled:=MenuFunctions.GroupEnabled[2325] and (WingisLicense >= WG_PROF) and (not VEForm.IsOffline);

  MenuFunctions['VEBirdsEye'].Enabled:=MenuFunctions.GroupEnabled[2325] and (WingisLicense >= WG_PROF);

  MenuFunctions['ExtrasWMS'].Enabled:=(WingisLicense >= WG_PROF) and (Data^.PInfo^.ProjectionSettings.ProjectProjection <> 'NONE');
  MenuFunctions['ViewRotate'].Enabled:=(not WMSForm.CheckBoxOnOff.Checked) and not (MenuFunctions['VEOnOff'].Checked);

  MenuFunctions['MUSynchronize'].Enabled:=(Data^.PInfo^.IsMU);
  MenuFunctions['ExtrasWKT'].Enabled:=(WingisLicense >= WG_PROF);

  MenuFunctions['EditScalePolyline'].Visible:=(WingisLicense >= WG_PROF) and (OneSelected) and (Data^.Layers^.SelLayer^.SelInfo.SelectedType=ot_Poly);

  Data^.EnableMenus;
{$ENDIF} // <----------------- AXDLL

end;

{******************************************************************************+
  Procedure TMDIChild.UIExtensionUpdateStatusbar
--------------------------------------------------------------------------------
  Called by the user-interface if the statusbar-text must be updated. Sets
  the current status-text of the project.
+******************************************************************************}
Procedure TMDIChild.UIExtensionUpdateStatusbar(Sender: TObject);
begin                          
  if Data<>NIL then begin
     Data^.UpdateStatusBar;                                                 
  end;
end;

{******************************************************************************+
  Procedure TMDIChild.UIExtensionUpdateCursor
--------------------------------------------------------------------------------
  Called by the user-interface to update the cursor. Sets the cursor currently
  selected for the project.
+******************************************************************************}
Procedure TMDIChild.UIExtensionUpdateCursor(Sender: TObject);
begin
  if Data<>NIL then begin
    Cursor:=Data^.Cursor;
    if GetCapture<>0 then Windows.SetCursor(Screen.Cursors[Cursor]);
  end;
end;

Procedure TMDIChild.FormResize(Sender: TObject);
begin
  if (Data<>NIL) then Data^.MsgSize;
  UserInterface.Update([uiMenus]);
  ShowAds;
end;

Procedure TMDIChild.UIExtensionUpdateCustom(Sender: TObject; const PendingUpdates:TUpdateInfo);
begin
  if Data<>NIL then begin
    {$IFNDEF AXDLL} // <----------------- AXDLL
    if uiViews in PendingUpdates then UpdateViews;
    if uiLegends in PendingUpdates then UpdateLegends;
    {$ENDIF} // <----------------- AXDLL
    if [uiLayers,uiViews,uiLegends]*PendingUpdates<>[] then UpdateLayersViewsLegendsLists(Data);
    if uiLayers in PendingUpdates then UpdateLayerLists(Data);
    if uiWorkingLayer in PendingUpdates then begin
      UpdateWorkingLayer(Data);
      if Data^.Document.Loaded then Data^.Document.ActualLayer:=PToStr(Data^.Layers^.TopLayer^.Text);
    end;
    {$IFNDEF AXDLL} // <----------------- AXDLL
    if uiSymbols in PendingUpdates then UpdateSymbolRollup;
    {$ENDIF} // <----------------- AXDLL
    if uiStartup in PendingUpdates then begin
      {$IFNDEF WMLT}
      MenuFunctions['EditSelect'].Execute;
      MenuFunctions['ListShowLayers'].Execute;
      {$ELSE}
      MenuFunctions['ViewZoom'].Execute;
      MenuFunctions['ListShowViews'].Execute;
      {$ENDIF}
    end;
  end;
end;

Procedure TMDIChild.UIExtensionShown(Sender: TObject);
begin
  UserInterface.Update([uiStartup]);
  ReadProjectIni;
end;

Procedure TMDIChild.UIExtensionUpdateContextToolbars(Sender: TObject);
begin
  if (Data<>NIL) then begin {++Sygsky}
    if (Data^.SymbolMode=sym_SymbolMode) then
    begin
      UserInterface.AddContextToolbar('Symboleditor');
      UIExtension.MainMenuName := 'SymbolEditorMenu';
    end
    else if (Data^.SymbolMode=sym_Omega) then {++Sygsky: Omega adding }
      begin
        UserInterface.AddContextToolbar('ImageTransformation');
//        UIExtension.MainMenuName := 'OmegaMenu'; { Later }
      end
      else
        UIExtension.MainMenuName := 'ChildMenu';
  end;
end;

{$IFNDEF AXDLL} // <----------------- AXDLL
  {$IFNDEF WMLT}

 //changed by Brovak;
Procedure TMDIChild.EditPointMove;                         
begin
Data^.TranspActive:=MenuFunctions['SelectTransparent'].Checked;

 if Data^.SnapItem<>nil
  then
   if not (Data.SnapItem.GetObjType in [ot_poly,ot_cpoly,ot_pixel])
    then
     begin;
      MessageDialog(GetLangText(80004),mtInformation,[mbOK],0);
      SwitchEditPointInVariousObjects;
      if Data^.TranspActive=true then
      MenuFunctions['SelectTransparent'].Execute;
      Exit;
     end;

 with Data^ do
  begin;
   EditVertexMode:=1;
   SetActualMenu(mn_Snap);
   if OldSnapItem<>nil then
   begin;
    PreparePointInVariousObjects;
    ActualMen:=mn_SnapPoly;
   end;
  end;
  if Data^.TranspActive=true then
MenuFunctions['SelectTransparent'].Execute;

end;

 //changed by Brovak;
Procedure TMDIChild.EditPointInsert;
begin
 Data^.TranspActive:=MenuFunctions['SelectTransparent'].Checked;

 if Data^.SnapItem<>nil
  then
   if not (Data.SnapItem.GetObjType in [ot_poly,ot_cpoly])
    then
     begin;
      MessageDialog(GetLangText(80004),mtInformation,[mbOK],0);
      SwitchEditPointInVariousObjects;
      if Data^.TranspActive=true then
      MenuFunctions['SelectTransparent'].Execute;
      Exit;
     end;

  with Data^ do
   begin;
   EditVertexMode:=2;
   SetActualMenu(mn_InsPoint);
    if OldSnapItem<>nil then
     begin;
      PreparePointInVariousObjects;
      ActualMen:=mn_InsPOK;
     end;
  end;
  if Data^.TranspActive=true then
MenuFunctions['SelectTransparent'].Execute;

end;
{+++Brovak - for select }
Procedure TMDIChild.PseudoSelectMode;
begin;
Data^.TranspActive:=MenuFunctions['SelectTransparent'].Checked;
Data^.DeSelectAll(true);
Data^.PInfo^.EditPoly:=false;
 with Data^ do
  begin;
   EditVertexMode:=5;
   SetActualMenu(mn_PseudoSelect,false);
   if OldSnapItem<>nil then
   begin;
   with Data^ do
     begin;
     Layers.TopLayer.Select(Pinfo,OldSnapItem);
     SnapItem:=OLdSnapItem.Mptr;
     POintInd:=-1;
     end;
   end;
  end;
  if Data^.TranspActive=true then
MenuFunctions['SelectTransparent'].Execute;
end;
{--Brovak}

 //changed by Brovak;
Procedure TMDIChild.EditPointDelete;
begin
 Data^.TranspActive:=MenuFunctions['SelectTransparent'].Checked;

 if Data^.SnapItem<>nil
  then
   if not (Data.SnapItem.GetObjType in [ot_poly,ot_cpoly])
    then
     begin;
      MessageDialog(GetLangText(80004),mtInformation,[mbOK],0);
      SwitchEditPointInVariousObjects;
      if Data^.TranspActive=true then
      MenuFunctions['SelectTransparent'].Execute;

      Exit;
     end;

 with Data^ do
  begin;
   EditVertexMode:=3;
   SetActualMenu(mn_DelPoint);
    if OldSnapItem<>nil then
     begin;
      PreparePointInVariousObjects;
      ActualMen:=mn_DelPOk;
     end;
  end;
  if Data^.TranspActive=true then
MenuFunctions['SelectTransparent'].Execute;

end;

Procedure TMDIChild.EditNeighbours;
begin
     if Data <> nil then begin
        Data^.EditNeighboursMode:=MenuFunctions['EditNeighbours'].Checked;
     end;
end;

{+++brovak}
 Procedure TMDIChild.InsertOLEObj;
begin
  Data^.SetActualMenu(mn_InsertOLEObject);
end;

Procedure TMDIChild.EditOleObj;
begin
  Data^.EditOLEObject;
  Data^.Modified:=true;
end;

{--brovak}


function TMDIChild.GetAXObject:Variant;
  begin
    //FDocument.
//    Result:=FDocument as IDispatch;
  end;
  {$ENDIF}
{$ENDIF} // <----------------- AXDLL

function TMDIChild.GetLayerCount:integer;
  begin
    Result:=Data^.Layers^.LData^.Count;
  end;

{$IFNDEF AXDLL} // <----------------- AXDLL
Procedure TMDIChild.FormShow(Sender: TObject);
var DC             : HDC;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
    boolProcess    : Boolean;
    OldChild       : TMDIChild;
begin
// if (Data<>NIL) or (LoadData) then begin
   OldChild:=NIL;
   if Data = NIL then begin
      OldChild:=WinGISMainForm.ActualChild;
      WinGISMainForm.ActualChild:=Self;
      boolProcess:=LoadData;
   end
   else begin
      boolProcess:=TRUE;
   end;

   if Data = nil then begin
      exit;
   end;

   Data^.PInfo^.IsMU:=OpenAsMU;
   if Data^.PInfo^.IsMU then begin
      Data^.PInfo^.MUServerAddress:=MU.LastServerAddress;
      Data^.PInfo^.MUProjectAddress:=MU.LoadingProjectUrl;
   end;
   if boolProcess then begin
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
    // set a write-lock to the file if it is not currenlty locked
    WasReadOnly:=IsFileWriteLocked(StrPas(FName));
    if (not IsOpenedReadOnly) and (not WasReadOnly) then begin
       LockHandle:=LockFileWrites(StrPas(FName));
    end;
//    if (Data^.SymbolMode=sym_Project) and (not IsMU) then
//         UserInterface.FileHistoryList.Add(StrPas(FName));
    Data^.Registry.WriteDateTime('\Project\Info\OpenDate',Now);
    LastDDEEnd:=TRUE;
    {$IFNDEF WMLT}
    ImpExport:=New(PImport,Init(Self,Data));
    DBGraphic:=New(PBusGraphText,Init);
    {$ENDIF}
    DDECommand:=0;
    DoRedraw:=TRUE;
    Data^.SetupWindow(Self);
    DC:=GetDC(Handle);
    SetMapMode(DC,MM_LOMETRIC);
    ReleaseDC(Handle,DC);

    if GetDeviceCaps(DC,RASTERCAPS) and RC_PALETTE > 0 then begin                                             {256 Farben?}
      if GetDeviceCaps(DC,COLORRES) > 0 then begin
        if g_hColorPalette{LogPal256} = 0{NIL} then begin   {01-005-100399}
          InitColors256(Data^.PInfo^.ExtCanvas.WindowDC);
          g_hActualPalette := g_hColorPalette; {01-005-100399}
          WhichPalette:=vp_Colors;
        end;
        Data^.WinGISPalette:=SetPaletteForWindow(Data^.PInfo^.ExtCanvas.WindowDC);
        Colors256:=TRUE;
        Data^.PInfo^.Colors256:=TRUE;
      end
      else begin
        Colors256:=FALSE;
        Data^.PInfo^.Colors256:=FALSE;
      end;
    end
    else begin
      Colors256:=FALSE;
      Data^.PInfo^.Colors256:=FALSE;
    end;

    Data^.SetupProject(Self,FName);

    if not Data^.PInfo^.IsMU then begin
    {++ IDB}
        If WinGISMainForm.WeAreUsingTheIDB[Data] Then
        If WinGISMainForm.SwitchIDBOn(Data) Then
          WinGISMainForm.IDB_Man.AddProject(Data);
{-- IDB}
{++ UndoRedo}
       If WinGISMainForm.WeAreUsingUndoFunction[Data] Then
       If WinGISMainForm.SwitchUndoRedoOn(Data) Then
          WinGISMainForm.UndoRedo_Man.AddProject(Data);
{-- UndoRedo}
        SetAutoSave;
    end
    else begin
        KillTimer(Handle,idt_AutoSave);
    end;

    {$IFNDEF WMLT}
    DBGraphic^.DC:=DC;
    {$ENDIF}
    HadNoRedraw:=FALSE;
    DDEHadNoRdr:=FALSE;
    RDRArea.Init;
    DDERDRArea.Init;
    SendRedraw:=TRUE;
    FirstDraw:=TRUE;
    Data^.Construct:=New(PConstruct,Init(Self,Data));
//    RouteMenu:=TRouteMenu.Create(Self,Data);
    if not OMChanged then begin
      if WinGISMainForm.CreateNewProj then begin
        OpenMode:=om_NewProj;
        // set file-version to avoid "overwrite WinGIS 3 project" warning
        Data^.FileVersion:=vnProject;
      end
      else OpenMode:=om_OpenProj;
      OMChanged:=TRUE;
    end;
    {$IFNDEF WMLT}
    GPS.CreateGPS(Data);
    //ThreeDChild:=NIL;
    //ThreeDActiv:=FALSE;
    OriginalParent:=Windows.GetParent(Handle);
    if (not WingisMainForm.CreateNewProj) and (FileExists(FName)) then begin
       //WinGisMainForm.FileHistory.NoAddHistoryFlag:=false;
       WinGisMainForm.FileHistory.Add(AnsiString(FName));
    end;
    {$ENDIF}
  end
  else
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
//     Close;
       begin
          if OldChild <> NIL then
             WinGISMainForm.ActualChild:=OldChild;
             Close;
       end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
end;
{$ENDIF} // <----------------- AXDLL

Procedure TMDIChild.Loaded;
begin
  if (Data<>NIL) and (Data^.SymbolMode=sym_SymbolMode) then
      UIExtension.MainMenuName:='SymbolEditorMenu';
  inherited Loaded;
end;

{******************************************************************************+
  Function TMDIChild.IsReadOnly
--------------------------------------------------------------------------------
  Determines if the project is write-protected. This may be because the
  ReadOnly-flag of the project is set or because the project was opened by
  another station.
+******************************************************************************}
Function TMDIChild.IsReadOnly:Boolean;
begin
  Result:=(Data^.Flags and pfReadOnly<>0)
  // activate the next line if project opened on another computer shall be read-only
  //   or WasReadOnly;
end;

{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL

procedure TMDIChild.SetGraphicParent(AParent: Integer);
const SM_CXPADDEDBORDER = 92;
var
OnlyResize: Boolean;
CurRect: TRect;
RectChanged: Boolean;
begin
   RunUpdateOnClose:=False;
   try
     OnlyResize:=False;

     if AParent = 0 then begin
        AParent:=OriginalParent;
     end
     else if (AParent = -1) or (AParent = CanvasHandle) then begin
        OnlyResize:=True;
        AParent:=CanvasHandle;
     end
     else begin
        OnlyResize:=False;
     end;

     if not OnlyResize then begin
        Windows.SetParent(Handle,AParent);
     end;

     RectChanged:=False;
     if OnlyResize then begin
        GetWindowRect(AParent,CurRect);
        if (CurRect.Left <> CanvasParRect.Left)
        or (CurRect.Top <> CanvasParRect.Top)
        or (CurRect.Right <> CanvasParRect.Right)
        or (CurRect.Bottom <> CanvasParRect.Bottom) then begin
           RectChanged:=True;
        end;
     end
     else begin
          RectChanged:=True;
     end;                                             

     if AParent = OriginalParent then begin
        Data^.IsCanvas:=False;
        ShowWindow(Handle,SW_SHOWMAXIMIZED);
        ShowWindow(WingisMainForm.Handle,SW_SHOW);
        ShowWindow(FindWindow('TApplication',PChar(Application.Title)),SW_SHOW);
        WingisMainForm.HideWindow:=False;
        CanvasHandle:=0;
     end
     else if RectChanged then begin
        Data^.IsCanvas:=False;
        GetWindowRect(AParent,CanvasParRect);

        SetWindowPos(Handle,HWND_TOP,
               -GetSystemMetrics(SM_CXFRAME)-GetSystemMetrics(SM_CXPADDEDBORDER),
               -GetSystemMetrics(SM_CYCAPTION)-GetSystemMetrics(SM_CYFRAME)-GetSystemMetrics(SM_CXPADDEDBORDER),
               CanvasParRect.Right-CanvasParRect.Left+2*GetSystemMetrics(SM_CXFRAME)+GetSystemMetrics(SM_CXPADDEDBORDER)+1,
               CanvasParRect.Bottom-CanvasParRect.Top+2*GetSystemMetrics(SM_CYFRAME)+GetSystemMetrics(SM_CXPADDEDBORDER)+GetSystemMetrics(SM_CYCAPTION)+1,
               SWP_NOREDRAW);

        if not OnlyResize then begin
           ShowWindow(WingisMainForm.Handle,SW_HIDE);
           ShowWindow(FindWindow('TApplication',PChar(Application.Title)),SW_HIDE);

           WingisMainForm.HideWindow:=True;
           CanvasHandle:=AParent;
        end;

        Data^.IsCanvas:=True;
        Data^.UpdatePaintOffset;

        SendMessage(Handle,WM_NCPAINT,0,0);

        if not OnlyResize then begin
           DDEHandler.SendString('[RED][]');
        end;
     end;
   finally
     if AParent <> OriginalParent then begin
        SetTimer(Handle,idt_CanvasResize,100,nil);
     end
     else begin
        KillTimer(Handle,idt_CanvasResize);
     end;
   end;
end;

procedure TMDIChild.ResetGraphicParent;
var
ParHandle    : Integer;
ParRect      : TRect;
begin
     RunUpdateOnClose:=False;
     if OriginalParent <> 0 then begin
       ParHandle:=OriginalParent;
       GetWindowRect(ParHandle,ParRect);
       Windows.SetParent(Handle,ParHandle);
       if ParHandle = OriginalParent then begin
            Data^.IsCanvas:=False;
            ShowWindow(Handle,SW_SHOWMAXIMIZED);
            CanvasHandle:=0;
       end;
     end;
end;

//------------------------------------------------------------------------------
Procedure TMDIChild.RedrawCanvas;
//------------------------------------------------------------------------------
var
Rect           : TDRect;
DDEString      : String;
TempString     : String;
begin
   try
     if (Self <> nil) and (Data <> nil) then begin
        Data^.PInfo^.GetCurrentScreen(Rect);
        DDEString:='[SRE][';
        TempString:=IntToStr(Round(1/Data^.PInfo^.ZoomToScale(Data^.PInfo^.Zoom)));
        DDEString:=DDEString+TempString+'][';
        Str(Rect.A.X/100:12:2,TempString);
        DDEString:=DDEString+TempString+'][';
        Str(Rect.A.Y/100:12:2,TempString);
        DDEString:=DDEString+TempString+'][';
        Str(Rect.B.X/100:12:2,TempString);
        DDEString:=DDEString+TempString+'][';
        Str(Rect.B.Y/100:12:2,TempString);
        DDEString:=DDEString+TempString+']';
        DDEHandler.SendString(DDEString);
     end;
   except
   end;
end;

procedure TMDIChild.DDESnapObject(sDDE: AnsiString);
var
NewLStyle      : TLayerProperties;
APointObj      : PIndex;
DestLayer      : PLayer;
IItem,SItem    : PIndex;
SourceLayer    : PLayer;
NewSnapData    : TSnapData;
begin
     SItem:=New(PIndex,Init(0));
     SItem^.Index:=StrToInt(GetDDEItem(sDDE,2));
     APointObj:=Data^.Layers^.TopLayer^.HasObject(SItem);
     if APointObj <> nil then begin
        APointObj:=Data^.Layers^.IndexObject(Data^.PInfo,APointObj);
        if (APointObj^.GetObjType=ot_Symbol) or (APointObj^.GetObjType=ot_Pixel) then begin
           SourceLayer:=Data^.Layers^.InsertLayer('',0,0,0,0,1,DefaultSymbolFill);
           if SourceLayer <> NIL then begin
              NewLStyle:=Data^.Layers^.TopLayer.GetLayerStyle;
              NewLStyle.Name:='___SNPSRC___';
              NewLStyle.Visible:=1;
              SourceLayer^.SetLayerStyle(Data^.PInfo,NewLStyle);
              NewLStyle.Destroy;
              Data^.Layers^.DetermineTopLayer;
           end;
           DestLayer:=Data^.Layers^.NameToLayer(GetDDEItem(sDDE,3));
           if DestLayer <> nil then begin
              IItem:=SourceLayer^.HasObject(SItem);
              if IItem=NIL then begin
                 IItem:=New(PIndex,Init(SItem^.Index));
                 IItem.ClipRect.InitByRect(SItem.ClipRect);
                 if SourceLayer^.InsertObject(Data^.PInfo,IItem,FALSE) then begin
                    Data^.Layers^.IndexObject(Data^.PInfo,SItem)^.Invalidate(Data^.PInfo);
                    Data^.PInfo^.FromGraphic:=TRUE;
                 end
                 else begin
                    Dispose(IItem,Done);
                 end;
              end;
              NewSnapData.Init;
              NewSnapData.Radius:=StrToInt(GetDDEItem(sDDE,4))*100;
              NewSnapData.LineSnap:=lsMovePoint;
              NewSnapData.SnapLayers^.DeleteAll;
              NewSnapData.SnapLayers^.Insert(Pointer(SourceLayer^.Index));
              NewSnapData.FixLayers^.DeleteAll;
              NewSnapData.FixLayers^.Insert(Pointer(DestLayer^.Index));
              DoSnap(Data^.Layers,Data,@NewSnapData);
              NewSnapData.Done;
              Data^.Layers^.DeleteLayer(SourceLayer);
              Data^.PInfo^.RedrawScreen(False);
           end;
        end;
     end;
     Dispose(SItem,Done);
end;

procedure TMDIChild.ExecuteMenuFunction(ItemName: AnsiString);
begin
     if MenuFunctions[ItemName].Enabled then begin
        MenuFunctions.Call(ItemName);
     end;
end;

procedure TMDIChild.DBMoveToLayer(sDDE: AnsiString);
var
OldTop: PLayer;
DestLayer: PLayer;
ExistObject,SItem: PIndex;
n,i,cnt: Integer;
begin
      OldTop:=Data^.Layers^.TopLayer;
      cnt:=CountDDEItems(sDDE);
      for n:=4 to cnt do begin
          SItem:=New(PIndex,Init(StrToInt(GetDDEItem(sDDE,n))));
          ExistObject:=Data^.Layers^.TopLayer^.HasObject(SItem);
          if ExistObject = nil then begin
             i:=1;
             while (ExistObject = nil) and (i < Data^.Layers^.LData^.Count) do begin
                Data^.Layers^.TopLayer:=Data^.Layers^.LData^.At(i);
                ExistObject:=Data^.Layers^.TopLayer^.HasObject(SItem);
                Inc(i);
             end;
          end;
          if ExistObject <> nil then
             Data^.Layers^.Select(Data^.PInfo,ExistObject);
          if SItem <> nil then begin
            Dispose(SItem,Done);
          end;
      end;
      DestLayer:=Data^.Layers^.NameToLayer(GetDDEItem(sDDE,2));
      Data^.LayerCopy(True,DestLayer);
      Data^.DeSelectAll(False);
      Data^.Layers^.TopLayer:=OldTop;
end;

procedure TMDIChild.ExtrasIUpdate;
begin
     WingisMainForm.ExtrasIUpdate;
end;

procedure TMDIChild.DBShowAllObjectsOnLayer(LayerName: String);
var
OldTop: PLayer;
SLayer: PLayer;
begin
      OldTop:=Data^.Layers^.TopLayer;
      if LayerName <> '' then begin
         SLayer:=Data^.Layers^.NameToLayer(LayerName);
         if SLayer <> nil then Data^.Layers^.TopLayer:=SLayer;
      end;
      PLayer(Data^.PInfo^.Objects)^.GetSize(Data^.Size);
//      Data^.Layers^.TopLayer.SelectByRect(Data^.PInfo,Data^.Size,True);
      ProcShowAllOnActiveLayer(Data);
//      Data^.DeSelectAll(False);
      Data^.Layers^.TopLayer:=OldTop;
end;

procedure TMDIChild.EditSelectAll;
var
OldEdit: Boolean;
begin
     OldEdit:=False;
     try
        OldEdit:=Data^.PInfo^.EditPoly;
        Data^.PInfo^.EditPoly:=False;
        PLayer(Data^.PInfo^.Objects)^.GetSize(Data^.Size);
        Data^.Layers^.TopLayer^.SelectByRect(Data^.PInfo,Data^.Size,False);
        Data^.Layers^.SelLayer^.Draw(Data^.PInfo);
        Data^.UpdateStatusBar;
        ProcDBSendUserSelection;
     finally
        Data^.PInfo^.EditPoly:=OldEdit;
     end;
end;

procedure TMDIChild.DBCutLineByLength(Id: Integer; Length: Double; Mode: Integer);
begin
    Length:=Length*100;
    if Length >= 1 then
       Data^.CutLineByLength(Id,Length,Mode,Data^.Layers^.TopLayer);
end;
  {$ENDIF} // <----------------- AXDLL
{$ENDIF}

{$IFNDEF AXDLL} // <----------------- AXDLL
procedure TMDIChild.FormDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
Var
   bThereAreSelectedObjects: Boolean;
begin
{$IFNDEF WMLT}
{++ IDB_DragAndDrop}
     Accept := FALSE;
     bThereAreSelectedObjects := SELF.Data.Layers.SelLayer.Data.GetCount > 0;
     if (bThereAreSelectedObjects)
     and (Source.ClassNameIs('TDragControlObject'))
     and (TDragControlObject(Source).Control <> nil)
     and (TDragControlObject(Source).Control.ClassNameIs('TIDB_DBGrid')) then
         Accept := TRUE;
{-- IDB_DragAndDrop}
{$ENDIF}
end;

procedure TMDIChild.FormDragDrop(Sender, Source: TObject; X, Y: Integer);
{++ IDB_DragAndDrop}
// ++ Commented by Cadmensky IDB Version 2.3.0
{Var
   iI, iJ: Integer;
   ALayer: PLayer;
   AItem: PIndex;}
// -- Commented by Cadmensky IDB Version 2.3.0
{-- IDB_DragAndDrop}
begin
{$IFNDEF WMLT}
{++ IDB_DragAndDrop}
// ++ Commented by Cadmensky IDB Version 2.3.0
{     If (TDragControlObject(Source).Control.ClassNameIs('TIDB_DBGrid')) Then Begin
        // End Drag-and-Drop operation in TDBGrid.
        SendMessage(TWinControl(TDragControlObject(Source).Control).Handle, WM_IDB_EndDrag, 0, 0);
        If (WinGisMainForm.WeAreUsingTheIDBUserInterface[Data])
           AND
           (SELF.Data.Layers.SelLayer.Data.GetCount > 0) Then Begin
              WinGisMainForm.IDB_Man.GetReadyToReceiveDroppedIDs(SELF.Data);
              // For each selected object.
              For iI:=0 To SELF.Data.Layers.SelLayer.Data.GetCount-1 Do
                  // Try to find it on layers.
                  For iJ:=1 To SELF.Data.Layers.LData.Count-1 Do Begin
                      ALayer := SELF.Data.Layers.LData.At(iJ);
                      AItem := ALayer.HasObject(SELF.Data.Layers.SelLayer.Data.At(iI));
                      If AItem = NIL Then CONTINUE;
                      // The item must be selected, we must avoid work with unselected copies of the selected object.
                      If NOT Boolean(AItem.State AND sf_Selected) Then CONTINUE;
                      // The item was found and we are sure this is just the same item we was looking for.
                      WinGisMainForm.IDB_Man.DropDataFromTable(SELF.Data, ALayer, AItem.Index, TControl(Source).Parent);
                      End; // For iJ end
              WinGISMainForm.IDB_Man.ExecuteDragAndDrop(SELF.Data, TDragControlObject(Source).Control);
              End; // If then end
        End;}
// -- Commented by Cadmensky IDB Version 2.3.0
{-- IDB_DragAndDrop}
{$ENDIF}
end;
{$ENDIF} // <----------------- AXDLL

{$IFNDEF AXDLL} // <----------------- AXDLL
procedure TMDIChild.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
     if Data <> nil then Resize:=not Data^.IsCanvas;
end;
{$ENDIF} // <----------------- AXDLL

{$IFNDEF WMLT}
  {$IFNDEF AXDLL} // <----------------- AXDLL

function TMDIChild.DBPolyCombine(s: AnsiString): Integer;
var
a: Array[0..256] of Integer;
i: Integer;
IdCnt: Integer;
begin
     IdCnt:=StrToInt(GetDDEItem(s,3));
     for i:=0 to IdCnt-1 do begin
         a[i]:=StrToInt(GetDDEItem(s,i+4));
     end;
     Result:=Data^.PolyCombine(GetDDEItem(s,2),a,IdCnt);
end;

procedure TMDIChild.ExtrasSplitLine;
begin
     ProcSplitLine(Data);
end;
  {$ENDIF} // <----------------- AXDLL
{$ENDIF}

{$IFNDEF AXDLL} // <----------------- AXDLL
{++ IDB}
procedure TMDIChild.FormDeactivate(Sender: TObject);
begin
     MenuFunctions['IDB_UndoFunction'].Enabled := FALSE;
     MenuFunctions['IDB_RedoFunction'].Enabled := FALSE;
end;
{-- IDB}

procedure TMDIChild.WMWTPacket(var Msg: TMessage);
begin
    Digitizer.WinTabMessage(Msg);
end;
{$ENDIF} // <----------------- AXDLL

procedure TMDIChild.CreateIslandArea(LName: String);
var
DestLayer: PLayer;
begin
     DestLayer:=Data^.Layers^.NameToLayer(LName);
     ProcCreateIslandArea(Data,DestLayer);
end;

{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
procedure TMDIChild.ExtrasReplacePolyPoints;

     procedure DoAll(AIndex: PIndex);
     begin
          Data^.IntList1.Add(AIndex^.Index);
     end;

begin
     Data^.IntList1.Clear;
     try
        Data^.Layers^.SelLayer^.Data^.ForEach(@DoAll);
     finally
        Data^.SetActualMenu(mn_SelPoly);    
        Data^.SetStatusText(GetLangText(79002),True);
     end;
end;

function TMDIChild.ExtrasImportCoordinates(TxtFile: String; DefFile: String; ObjType: Integer; DBMode: Integer; DBName: String; DBTable: String): Boolean;
var
Ini: TIniFile;
DLLHandle: Integer;
DLLName: AnsiString;
Filter: Integer;
Res: Integer;
Doc: Variant;
AProc: function (DIR:PCHAR;var AXHANDLE:VARIANT;var DOCUMENT:VARIANT;LNGCODE:PCHAR;
                 FILTER:integer; TXTNAME:PCHAR; TDFNAME:PCHAR;
                 DBMODE:integer; DBNAME:PCHAR; DBTABLE:PCHAR):integer;stdcall;
begin
     Result:=False;
     Filter:=-1;
     if ObjType in [ot_Pixel,ot_Poly,ot_CPoly,ot_Symbol,ot_Circle] then begin
        Filter:=ObjType;
     end;
     OpenDialogTxt.Filter:=MlgSection[18];
     TxtFile:=Trim(TxtFile);
     DefFile:=Trim(DefFile);
     if (TxtFile <> '') and (not FileExists(TxtFile)) then begin
        TxtFile:='';
     end;
     if (DefFile <> '') and (not FileExists(DefFile)) then begin
        DefFile:='';
     end;
     if TxtFile = '' then begin
        if (OpenDialogTxt.Execute) then begin
           TxtFile:=OpenDialogTxt.FileName;
        end;
     end;
     if (TxtFile <> '')  then begin
        Res:=0;
        DLLName:=OSInfo.WingisDir + 'TxtImp.dll';
        DLLHandle:=LoadLibrary(PChar(DLLName));
        WingisMainForm.CheckLibStatus(DLLHandle,DLLName,'TXT Import');
        if DLLHandle <> 0 then begin
           @AProc:=GetProcAddress(DLLHandle,'EXECMODULEWITHFILTER');
           if @AProc <> nil then begin
              Ini:=TIniFile.Create(OSInfo.WingisIniFileName);
              Doc:=WingisMainForm.GetCurrentTDocument;
              if Filter = -1 then begin
                 if MenuFunctions['DrawEllipses'].Checked then begin
                    Filter:=ot_Circle;
                 end
                 else begin
                    case Data^.ActualMen of
                         mn_Pixel:    Filter:=ot_Pixel;
                         mn_Poly:     Filter:=ot_Poly;
                         mn_CPoly:    Filter:=ot_CPoly;
                         mn_Symbol:   Filter:=ot_Symbol;
                    end;
                 end;
              end;
              if Filter <> -1 then begin
                 try
                    Res:=AProc(PChar(OSInfo.WingisDir),
                         WingisMainForm.DXApp,
                         Doc,
                         PChar(Ini.ReadString('Settings','Language','044')),
                         Filter,
                         PChar(AnsiString(TxtFile)),
                         PChar(AnsiString(DefFile)),
                         DBMode,
                         PChar(AnsiString(DBName)),
                         PChar(AnsiString(DBTable)));
                 finally
                    if Res > 0 then begin
                       Result:=True;
                       Data^.PInfo^.RedrawScreen(False);
                    end;
                 end;
              end;
              Ini.Free;
           end;
           FreeLibrary(DLLHandle);
        end
        else begin
           MessageBox(0,PChar(SystemErrorText + ' (TxtImp.dll)'),PChar(Application.Title),MB_ICONERROR or MB_OK or MB_APPLMODAL);
        end;
     end;
end;

procedure TMDIChild.ExtrasExplodeToPoints;
var
DLayer: PLayer;
begin
     DLayer:=SelectLayerDialog(Data,[sloNewLayer],2020);
     if DLayer <> nil then begin
        ProcExplodeSelectedToPoints(Data,DLayer,nil);
     end;
end;

procedure TMDIChild.ExtrasExplodeToSymbols;
var
DLayer: PLayer;
ASGroup: Pointer;
begin
     DLayer:=SelectLayerDialog(Data,[sloNewLayer],2020);
     if DLayer <> nil then begin
        ASGroup:=GetSymbol(Self,Data);
        if ASGroup <> nil then begin
           ProcExplodeSelectedToPoints(Data,DLayer,ASGroup);
        end;
     end;
end;

Procedure TMDIChild.ConvertSymbolToObjects;
var
DLayer: PLayer;
Failed: Integer;

     procedure DoAll(AItem: PIndex);
     begin
          if AItem^.GetObjType = ot_Symbol then begin
             if not ProcConvertSymToObj(Data,AItem^.Index,DLayer) then begin
                Inc(Failed);
             end;
          end;
     end;

begin
     Failed:=0;
     DLayer:=SelectLayerDialog(Data,[sloNewLayer],2020);
     if DLayer <> nil then begin
        Data^.Layers^.SelLayer^.Data^.ForEach(@DoAll);
        if Failed > 0 then begin
           MessageBox(Handle,PChar(Format(GetLangText(79003),[Failed])),PChar(WingisMainForm.Caption),MB_OK or MB_ICONWARNING or MB_APPLMODAL);
        end;
        Data^.PInfo^.RedrawScreen(False);
     end;
end;

Procedure TMDIChild.ExtrasPolyPointCut;
var
ASGroup: Pointer;
begin
     PointCutForm:=TPointCutForm.Create(nil);
     try
        PointCutForm.Project:=Data;
        if PointCutForm.ShowModal = mrOk then begin
           ASGroup:=nil;
           if PointCutForm.CheckBoxSymbol.Checked then begin
              ASGroup:=GetSymbol(Self,Data);
           end;
           if (not PointCutForm.CheckBoxSymbol.Checked) or (ASGroup <> nil) then begin
              ProcPolyPointCut(Data,PointCutForm.Layer1,PointCutForm.Layer2,PointCutForm.DLayer,ASGroup);
           end;
        end;
     finally
        PointCutForm.Free;
     end;
end;

procedure TMDIChild.EditRotateSymbols;
begin
     ProcRotateSymbols(Data);
end;

procedure TMDIChild.SpecialPrintFrameMakeAndShow(ThisRect:TRotRect);
var
Apoly:PPoly;
TmpPoint:tdpoint;
Alayer:PLayer;
i:integer;
Atext:PText;
RealXStart:double;
RealYStart:double;
CurrentValue:double;
Asize,BSize:TDRect;
AHeightCount, AWidthCount:integer;
AFontDes: PFontDes;
DrawText:boolean;
AStepLength:longint;
AKoef:double;
DoubAX,DoubAY,DoubBX,DoubBY:double;
AObjectStyle : PObjstyleV2;

   procedure SetPoint(X,Y:longint);
   begin
        TmpPoint.X:=X;
        TmpPoint.Y:=Y;
        APoly.InsertPoint(TmpPoint);
   end;

   procedure SetPoint1(X,Y:Longint);
   begin
        TmpPoint.X:=X;
        TmpPoint.Y:=Y;
   end;

   procedure SetLineColor(AColor:integer;Flag:integer);
   var
   AIndex:PIndex;
   begin
        case Flag of
             0,1:
                 if Data.FramePrintSpec.UseCross then begin;
                     AIndex:=Alayer.HasIndexObject (Apoly.Index);
                     //BIndex:=PLayer(Data.PInfo^.Objects)^.IndexObject(Data.PInfo,AIndex);
                     PPoly(AIndex).ChangeStyle(Data.Pinfo,AObjectStyle^,false);
                 end;

             2:
                 if Data.FramePrintSpec.UseCross then begin;
                    AIndex:=Alayer.HasIndexObject(AText.Index);
                    //BIndex:=PLayer(Data.PInfo^.Objects)^.IndexObject(Data.PInfo,AIndex);
                    PText(Aindex).ChangeStyle(Data.Pinfo,AObjectStyle^,false,false);
                 end;

        end;
   end;

   procedure DrawAxis;
   begin
      if not Data^.FramePrintSpec.UseTextMarks then Exit;

      //Draw Left Axis if exist
      if Data^.FramePrintSpec.UseLeft then begin
         Apoly:=New (PPoly,Init);

         SetPoint(ASize.A.X,ASize.A.Y);
         SetPoint(ASize.A.X,ASize.B.Y);

         Data^.InsertObject(Apoly,false);
         SetLineColor(Data^.FramePrintSpec.GridColor,0);
      end;

      //Draw Right Axis if exist
      if  Data^.FramePrintSpec.UseRight then begin
          Apoly:=New(PPoly,Init);

          SetPoint(ASize.B.X,ASize.A.Y);
          SetPoint(ASize.B.X,ASize.B.Y);

          Data^.InsertObject(Apoly,false);
          SetLineColor(Data^.FramePrintSpec.GridColor,0);
      end;

      //Draw Top Axis if exist
      if Data^.FramePrintSpec.UseTop then begin
         Apoly:=New (PPoly,Init);

         SetPoint(ASize.A.X,ASize.B.Y);
         SetPoint(ASize.B.X,ASize.B.Y);

         Data^.InsertObject(Apoly,false);
         SetLineColor(Data^.FramePrintSpec.GridColor,0);
      end;

      //Draw Bottom Axis if exist
      if Data^.FramePrintSpec.UseBottom then begin
         Apoly:=New(PPoly,Init);

         SetPoint(ASize.A.X,ASize.A.Y);
         SetPoint(ASize.B.X,ASize.A.Y);

         Data^.InsertObject(Apoly,false);
         SetLineColor(Data^.FramePrintSpec.GridColor,0);
      end;
   end;

   procedure InsertText(SText:ansistring; Direction:integer);
   var
   Koeff:longint;
   begin
        if not DrawText then Exit;
        if not Data^.FramePrintSpec.UseTextMarks then Exit;

        Atext:=New(PText,Init(Data^.FramePrintSpec.Font,SText,0));
        Atext.CalculateSize(Data^.Pinfo);

        Koeff:=trunc(Data^.FramePrintSpec.MarksLength) ;

        with TmpPoint do begin

             case Direction of
                  0: {Left Axis}
                    begin
                         X:=X+8+Koeff;
                         Y:=Y+Trunc(AText.Height/2);
                    end;
                  1: {Right Axis}
                    begin
                         X:=X-8-AText.Width-Koeff;
                         Y:=Y+Trunc(Atext.Height/2);
                    end;

                  2: {Top Axis}
                    begin
                         AText^.Angle:=270;
                         Y:=Y-8-Koeff-AText.Width;
                         X:=X-Trunc(Atext.Height/2);
                    end;

                  3: {Bottom Axis}
                    begin
                         AText^.Angle:=270;
                         Y:=Y+8+Koeff;
                         X:=X-Trunc(AText.Height/2);
                    end;
             end;
        end;

        AText.Pos.X:=TmpPoint.X;
        AText.Pos.Y:=TmpPoint.Y;

        AText.UpdateTextData(Data^.Pinfo);

        if Data.Layers.IndexObject(Data.PInfo,AText) = nil then begin
           if not Data.InsertObjectOnLayer (AText, PLayer(Data.Layers.TopLayer).Index) then begin
              Data^.CorrectSize(AText.ClipRect,FALSE);
           end;
        end;

        SetLineColor(Data^.FramePrintSpec.GridColor,2);
   end;

   procedure InsertMark(Direction:integer);
   var
   InPoint:TDPoint;
   Koeff:longint;
   begin
        if not Data^.FramePrintSpec.UseTextMarks then Exit;

        Apoly:=New(PPoly,Init);

        with TmpPoint do begin
             Apoly.InsertPoint(TmpPoint);
             Koeff:=trunc(Data^.FramePrintSpec.MarksLength);

             case Direction of
                  0: InPoint.Init(x+Koeff,y); {Left Axis}
                  1: InPoint.Init(x-Koeff,y); {Right Axis}
                  2: InPoint.Init(x,y-Koeff); {Top Axis}
                  3: InPoint.Init(x,y+Koeff); {Bottom Axis}
             end;

             APoly.InsertPoint(InPoint);

             if Data.Layers.IndexObject (Data.PInfo, APoly) = nil then begin
                Data.InsertObjectOnLayer (APoly, PLayer(Data.Layers.TopLayer).Index);
             end;
        end;

        SetLineColor(Data^.FramePrintSpec.GridColor,1);
   end;


   procedure SetCrosses;
   var
   Cur1,Cur2:longint;
   i,j:integer;
   LengLine:integer;
   begin
        Lengline:= trunc(Data^.FramePrintSpec.Crosslength/20) ;

        for i:=0 to AWidthCount do begin
            Cur1:=Round(RealXstart) + round(i*AStepLength);

            for j:=0 to AHeightCount do begin
                Cur2:=Round(RealYstart) + round(j*AStepLength);

                // horizontal

                Apoly:= New(Ppoly,Init);

                SetPoint(Cur1-LengLine,Cur2);
                SetPoint(Cur1+LengLine,Cur2);

                Data.RedrawAfterInsert := FALSE;

                if Data.Layers.IndexObject (Data.PInfo, APoly) = nil then begin
                   Data.InsertObjectOnLayer (APoly, PLayer(Data.Layers.TopLayer).Index);
                end;

                // vertical

                Apoly:= New(Ppoly,Init);

                SetPoint(Cur1,Cur2-LengLine);
                SetPoint(Cur1,Cur2+LengLine);

                Data.RedrawAfterInsert := FALSE;

                if Data.Layers.IndexObject (Data.PInfo, APoly) = nil then begin
                   Data.InsertObjectOnLayer (APoly, PLayer(Data.Layers.TopLayer).Index);
                end;

            end;
        end;
   end;

begin
     Data.RedrawAfterInsert := FALSE;

     BSize.InitByRect(Data^.Size);

     ASize.Init;
     with Asize do begin
          A.X:= Trunc(ThisRect.X);
          A.Y:= Trunc(ThisRect.Y);
          B.X:= A.X + Trunc(ThisRect.Width);
          B.Y:= A.Y + Trunc(ThisRect.Height);
     end;

     if not Data^.FramePrintSpec.UseSpecFrame then Exit;

     if Data^.FramePrintSpec.UseCross then begin
        ALayer:=Data^.Layers.InsertLayer(PrintFrameLayerName,Data^.FramePrintSpec.CrossColor, 0, 0, 0, ilTop, DefaultSymbolFill);

        AOBjectStyle:=New(PObjStyleV2);
        AOBjectStyle.LineStyle:= DefaultLineStyle;
        AObjectStyle.FillStyle:= DefaultFillStyle;
        AObjectStyle.SymbolFill:= DefaultSymbolFill;
        AOBjectStyle.LineStyle.Color:=Data^.FramePrintSpec.GridColor;
        AOBjectStyle.FillStyle.BackColor:=clWhite;
     end
     else begin
        ALayer:=Data^.Layers.InsertLayer(PrintFrameLayerName,Data^.FramePrintSpec.GridColor, 0, 0, 0, ilTop, DefaultSymbolFill);
     end;

     ALayer.SetState(sf_LayerOff,true);

     Data^.Layers.SetTopLayer(Alayer);

     AStepLength:= Trunc(Data^.FramePrintSpec.StepLength/10);

     with ASize do begin
          DoubAX:= A.X;
          DoubAY:=A.Y;
          DoubBX:=B.X;
          DoubBY:=B.Y;
     end;

     RealXStart:=AStepLength*(trunc((DoubAX+AStepLength-1) / AStepLength));
     RealYStart:=AStepLength*(trunc((DoubAY+AStepLength-1) / AStepLength));

     Akoef:= DoubBX-RealXStart;
     AWidthCount:= abs(trunc(Akoef/AStepLength));

     Akoef:= DoubBY-RealYStart;
     AHeightCount:= abs(trunc(Akoef/AStepLength));

     TmpPoint.Init(0,0);

     AFontDes:=Data^.PInfo.Fonts.GetFont(Data^.FramePrintSpec.Font.Font);

     if AFontDes=nil then begin
        DrawText:=False
     end
     else begin
        DrawText:=True;
     end;

     with Data^.FramePrintSpec do begin

          for i:=0 to AHeightCount do begin
              CurrentValue:=round(RealYstart) + round(i*AStepLength);

              if UseLeft then begin
                 SetPoint1(Asize.A.X,round(CurrentValue));
                 InsertMark(0);
                 InsertText(FLoatToStr(round(CurrentValue/100)),0);
              end;

              if UseRight then begin
                 SetPoint1(Asize.B.X,round(CurrentValue));
                 InsertMark(1);
                 InsertText(FloatToStr(round(CurrentValue/100)),1);
              end;
          end;

          for i:=0 to AWidthCount do begin
              CurrentValue:=Round(RealXstart) + round(i*AStepLength);

              if UseTop then begin
                 SetPoint1(round(CurrentValue),Asize.B.Y);
                 InsertMark(2);
                 InsertText(FloatToStr(round(CurrentValue/100)),2);
              end;

              if UseBottom then begin
                 SetPoint1(round(CurrentValue),Asize.A.Y);
                 InsertMark(3);
                 InsertText(FloatToStr(Round(CurrentValue/100)),3);
              end;
          end;
     end;

     if Data^.FramePrintSpec.UseCross then begin
        SetCrosses;
     end;

     DrawAxis;

     if Data^.FramePrintSpec.UseCross then begin
        Dispose(AObjectStyle);
     end;

     ALayer.SetState(sf_LayerOff,False);

     Data^.Size.InitByRect(BSize);
end;



{$ENDIF} // <----------------- AXDLL
{$ENDIF}

{$IFNDEF WMLT}
   {$IFNDEF AXDLL} // <----------------- AXDLL
procedure TMDIChild.FileImportTXT;
var
Ini: TIniFile;
LngCode: String;
begin
   Ini:=TIniFile.Create(OSInfo.WingisIniFileName);
   try
      LngCode:=Ini.ReadString('Settings','Language','044');
   finally
      Ini.Free;
   end;
   if WingisMainForm.ImpTxtLib <> 0 then begin
      WingisMainForm.DeactivateModule(WingisMainForm.ImpTxtLib);
   end;
   WingisMainForm.ActivateModule(WingisMainForm.ImpTxtLib,'TxtImp',0,LngCode);
   Data^.SetModified;
end;

procedure TMDIChild.ExtrasCombinePolygonsWithTexts;
var
Buffer: Array[0..255] of Char;
i: Integer;
PolyErrorLayer: WideString;
TextErrorLayer: WideString;
LinkTextToPoly: Boolean;
AddTextToDatabase: Boolean;
RemoveCombinedTexts: Boolean;
TextDatabaseColumn: WideString;
ErrorResultFilename: WideString;
AApp: OleVariant;
ADocument: OleVariant;
// mar 13-05-2004: add new objects for combine cpolys with texts
CPolyList, TextList:AxCmbStringList;
Setup:AxCmbTextsWithCPolysSetup;

     procedure DoAllLayers(ALayer: PLayer);
     begin
          if (ALayer^.Text <> nil) and (not ALayer^.GetState(sf_Fixed)) and (not ALayer^.GetState(sf_LayerOff)) then begin
             CombPolyTxtForm.ListBoxPoly.Items.Add(ALayer^.Text^);
             CombPolyTxtForm.ListBoxText.Items.Add(ALayer^.Text^);
          end;
     end;

begin
     PolyErrorLayer:='';
     TextErrorLayer:='';
     TextDatabaseColumn:='';
     ErrorResultFilename:=OSInfo.TempDataDir + 'CombPolyTxt.log';

     CombPolyTxtForm:=TCombPolyTxtForm.Create(nil);
     try
        Data^.Layers^.LData^.ForEach(@DoAllLayers);
        GetPrivateProfileString('SETTINGS','Language','044',Buffer,SizeOf(Buffer),PChar(OSInfo.WingisIniFileName));

        CombPolyTxtForm.AXImpExpDbc.LngCode:=StrPas(Buffer);
        CombPolyTxtForm.AXImpExpDbc.WorkingDir:=OSInfo.WingisDir;
        CombPolyTxtForm.AXGisCombine.LngCode:=StrPas(Buffer);
        CombPolyTxtForm.AXGisCombine.WorkingDir:=OSInfo.WingisDir;

        if CombPolyTxtForm.ShowModal = mrOK then begin
           Data^.SetCursor(crHourGlass);
           try
              with CombPolyTxtForm do begin
                   StatusBar.ProgressPanel:=True;
                   CPolyList:=AxGisCombine.CreateStringList;
                   for i:=0 to ListBoxPoly.Items.Count-1 do begin
                       if ListBoxPoly.Selected[i] then begin
                          CPolyList.Add(ListBoxPoly.Items[i]);
                       end;
                   end;
                   TextList:=AxGisCombine.CreateStringList;
                   for i:=0 to ListBoxText.Items.Count-1 do begin
                       if ListBoxText.Selected[i] then begin
                          TextList.Add(ListBoxText.Items[i]);
                       end;
                   end;

                   if CheckBoxErrorLayers.Checked then begin
                      PolyErrorLayer:=CombPolyTxtForm.MlgSection[10];
                      TextErrorLayer:=CombPolyTxtForm.MlgSection[11];
                   end;

                   LinkTextToPoly:=CheckBoxLinkTexts.Checked;

                   AddTextToDatabase:=(AXImpExpDbc.ImpDbMode <> 0);
                   AXGisCombine.DBMode:=AXImpExpDbc.ImpDbMode;
                   AXGisCombine.DBName:=AXImpExpDbc.ImpDatabase;
                   AXGisCombine.DBTable:=AXImpExpDbc.ImpTable;

                   if AddTextToDatabase then begin
                      TextDatabaseColumn:=EditDBField.Text;
                   end;

                   RemoveCombinedTexts:=CheckBoxRemoveCombined.Checked;

                   // mar 13-05-2004
                   // prepare setup for combine of cpolys with texts
                   Setup:=AXGisCombine.CreateCombineTextsWithCPolysSetup;
                   Setup.CPolyErrorLayer:=PolyErrorLayer;
                   Setup.TextErrorLayer:=TextErrorLayer;
                   Setup.LinkTextToCPoly:=LinkTextToPoly;
                   Setup.ApplyTextDatabaseInfoToCPoly:=AddTextToDatabase;
                   Setup.TextDatabaseColumn:=TextDatabaseColumn;
                   Setup.RemoveCombinedTexts:=RemoveCombinedTexts;
                   Setup.ErrorResultFilename:=ErrorResultFilename;

                   AApp:=WingisMainForm.DXApp;
                   AXGisCombine.SetApp(AApp);

                   ADocument:=WingisMainForm.GetCurrentTDocument;
                   AXGisCombine.SetDocument(ADocument);

                   AXGisCombine.CombineCPolysWithTexts(CPolyList, TextList, Setup, ds_Layers, ds_Layers); // datasource of polygons and texts is layers

                   if RemoveCombinedTexts then begin
                      Data^.PInfo^.RedrawScreen(False);
                   end;

              end;
           finally
              Data^.SetCursor(crDefault);
              StatusBar.ProgressPanel:=False;
           end;
        end;
     finally
            CombPolyTxtForm.Free;
     end;
end;

procedure TMDIChild.GisManDisplayPass(Sender: TObject; const aPass: WideString);
begin
     StatusBar.ProgressText:=aPass;
end;

procedure TMDIChild.GisManDisplayPercent(Sender: TObject; aValue: Integer);
begin
     StatusBar.Progress:=aValue;
     Application.ProcessMessages;
end;

procedure TMDIChild.SelectAttached;
var
AIndex: PIndex;
AView: PView;
i: Integer;
Coll: PCollection;

     procedure DoAll(AItem: PIndex);
     begin
          Data^.Layers^.Select(Data^.PInfo,AItem);
     end;

begin
     for i:=0 to Data^.Layers^.SelLayer^.SelInfo.Count-1 do begin
       try
         AIndex:=Data^.Layers^.SelLayer^.Data^.At(i);
         AView:=PLayer(Data^.PInfo^.Objects)^.IndexObject(Data^.PInfo,AIndex);
         if (AView <> nil) and (Assigned(AView^.AttData)) then begin
            Coll:=AView^.GetAttachedObjs;
            if Coll <> nil then begin
               Coll^.ForEach(@DoAll);
            end
            else begin
               AView:=AView^.GetObjMaster(Data^.PInfo);
               if AView <> nil then begin
                  Data^.Layers^.Select(Data^.PInfo,AView);
                  Coll:=AView^.GetAttachedObjs;
                  if Coll <> nil then begin
                     Coll^.ForEach(@DoAll);
                  end
               end;
            end;
         end;
       except
         break;
       end;
     end;
end;

procedure TMDIChild.DBExplodePolys(DLayerName: String; SymName: String);
var
DLayer: PLayer;
ASym: PSGroup;
begin
     ASym:=nil;
     DLayer:=Data^.Layers^.NameToLayer(DLayerName);
     if DLayer <> nil then begin
        if SymName <> '' then begin
           ASym:=PSymbols(Data^.PInfo^.Symbols)^.GetSymbol(SymName,'');
        end;
        ProcExplodeSelectedToPoints(Data,DLayer,ASym);
     end;
end;

   {$ENDIF} // <----------------- AXDLL
{$ENDIF}

procedure TMDIChild.ConstructDimension;
begin
     Data^.bConstrDim:=MenuFunctions['ConstructDimension'].Checked;
end;

{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}
procedure TMDIChild.ExtrasCreateCutPoints;
begin
     ProcCreateCutPoints(Data);
end;

procedure TMDIChild.ExtrasCreatePolyline;
begin
     ProcCombineLines(Data);
end;

procedure TMDIChild.ExtrasFindEqualObjects;
begin
     ProcSelectEqualObjects(Data,Data^.Layers^.TopLayer);
end;

procedure TMDIChild.ExtrasApplyTM;
begin
     ProcApplyThematicMap;
end;

procedure TMDIChild.ExtrasIsoWizzard;
var
CreateIsoObjects: function (Handle: Integer; Doc: Variant; Core: Variant): HRESULT; stdcall;
begin
     if ISOLibHandle = 0 then begin
        ISOLibHandle:=LoadLibrary('IsoWiz.dll');
     end;
     if ISOLibHandle <> 0 then begin
        @CreateIsoObjects:=GetProcAddress(ISOLibHandle,'CreateIsoObjects');
        if @CreateIsoObjects <> nil then begin
              Screen.Cursor:=crHourGlass;
              CreateIsoObjects(Application.Handle,WinGISMainForm.GetCurrentTDocument,WinGISMainForm.DXApp);
              Screen.Cursor:=crDefault;
        end;
     end;
end;

procedure TMDIChild.FileExportKML;
var
ExportKml: function (Handle: Integer; Doc: Variant): PChar; stdcall;
LibHandle: Integer;
s,DDEString: AnsiString;
begin
     s:='';
     DDEString:='';
     LibHandle:=LoadLibrary('KmlExp.dll');
     if LibHandle <> 0 then begin
        @ExportKml:=GetProcAddress(LibHandle,'ExportKml');
        if @ExportKml <> nil then begin
              s:=ExportKml(Application.Handle,WinGISMainForm.GetCurrentTDocument);
              if s <> '' then begin
                 DDEString:='[NEWKML][' + s + ']';
                 DDEHandler.SendPChar(PChar(DDEString));
              end;
        end;
        FreeLibrary(LibHandle);
     end;
end;

procedure TMDIChild.FileExportOVL;
type TOvlExport=procedure (DIR:PCHAR;var AXHANDLE:VARIANT;var SOURCE:VARIANT;REGMODE:integer; LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);stdcall;
var inifilename:string;
    Languageext:string;
    Ini: TIniFile;
    OVLEXPORT   :TOVLExport;
    OVLExportPtr:TFarProc;
    curdoc:Variant;
    pdir  :PCHAR;
    plng  :PCHAR;
begin
   {$IFNDEF WMLT}
   pdir:=stralloc(length(OSInfo.WingisDir)+1);
   strpcopy(pdir,OSInfo.WingisDir);

   // get language code
   inifilename:=OSInfo.WingisIniFileName;
   Ini:=TIniFile.Create(OSInfo.WingisIniFileName);
   LanguageExt:=Ini.ReadString('Settings','Language','044'); // default is english
   Ini.Free;
   plng:=stralloc(length(LanguageExt)+1);
   strpcopy(plng, LanguageExt);

   WinGISMainForm.ImpExpDll:=LoadLibrary('OVLEXP.DLL');
   WingisMainForm.CheckLibStatus(WingisMainForm.ImpExpDll,'OVLEXP.DLL','Import/Export');
   OvlExportPtr:=GetProcAddress(WinGISMainForm.ImpExpDll,'OVLEXPORT');
   if OvlExportPtr <> NIL then
   begin
      @OVLEXPORT:=OvlExportPtr;
      // call pxf-asc import DLL
      WinGISMainForm.CheckTDocumentIsCorrect;
      curdoc:=WinGISMainForm.GetCurrentTDocument;
      OVLEXPORT(pdir,WinGISMainForm.DXApp,curdoc,1, plng,
                WinGISMainForm.Handle,
                WinGISMainForm.Top,
                WinGISMainForm.Left,
                WinGISMainForm.Width,
                WinGISMainForm.Height
               );
      WinGISMainForm.LastImpExpOperation:='OVLEXP';
   end
   else
   begin
      FreeLibrary(WinGISMainForm.ImpExpDll);
   end;
   strdispose(pdir);
   strdispose(plng);
   {$ENDIF}
end;

{$ENDIF} // <----------------- AXDLL
{$ENDIF}

{$IFNDEF AXDLL}
procedure TMDIChild.ExtrasGPSLog;
var
ALib: Integer;
WintecToCsv: function (Password: PChar; TargetDir: PChar): Integer; stdcall;
Res: Integer;
GpsDir: AnsiString;
begin
     ALib:=LoadLibrary('gpsapi.dll');
     if ALib <> 0 then begin
        @WintecToCsv:=GetProcAddress(ALib,'WintecToCsv');
        if @WintecToCsv <> nil then begin
           if FileCtrl.SelectDirectory(GetLangText(25000),'',GpsDir) then begin
              if GpsDir[Length(GpsDir)] <> '\' then begin
                 GpsDir:=GpsDir + '\';
              end;
              Res:=WintecToCsv(nil,PChar(GpsDir));
              if Res > 0 then begin
                 FileImportTxt;
              end
              else begin
                 MessageBox(0,PChar('Error Code: ' + IntToStr(Res)),'GPSAPI.DLL',MB_OK or MB_ICONERROR or MB_APPLMODAL);
              end;
           end;
        end;
        FreeLibrary(ALib);
     end;
end;

procedure TMDIChild.ExtrasGPSConnect;
var
AItem: PIndex;
begin
     if GPSConnection <> nil then begin
        if GPSConnection.Active then begin
           MenuFunctions['ExtrasGPSConnect'].Checked:=False;
           GPSConnection.EndGPS;
           exit;
        end;
        if Data^.Layers^.SelLayer^.Data^.GetCount > 0 then begin
           AItem:=Data.Layers.IndexObject(Data.PInfo,Data^.Layers^.SelLayer^.Data^.At(0));
           if (AItem <> nil) and (AItem.GetObjType = ot_Symbol) then begin
              if MenuFunctions['ExtrasGPSConnect'].Checked then begin
                 if not GPSConnection.StartGPS(Data,PSymbol(AItem)) then begin
                    MenuFunctions['ExtrasGPSConnect'].Checked:=False;
                 end;
              end
              else begin
                 GPSConnection.EndGPS;
              end;
           end;
        end;
     end;
end;
{$ENDIF}

procedure TMDIChild.VEOnOff;
begin
  if not VEForm.SetProjection then begin
     exit;
  end;
  if VEForm.Login then begin
     VEForm.SetVectorRotation;
     if (VEForm.VEActive) and (Data^.PInfo^.ViewRotation <> 0) then begin
        Data^.PInfo^.SetViewRotation(0);
     end;
     VEForm.RefreshView;
  end;
end;

function TMDIChild.VEOnWithLoginData(User, Pwd: AnsiString; Offline: Boolean): Integer;
var
LoginOK: Boolean;
begin
  Result:=1;
  User:=Trim(User);
  Pwd:=Trim(Pwd);
  if not VEForm.SetProjection then begin
     exit;
  end;
  if (User <> '') then begin
     VEForm.LoggedIn:=False;
     VEForm.EditUser.Text:=User;
  end;
  if (Pwd <> '') then begin
     VEForm.LoggedIn:=False;
     VEForm.EditPwd.Text:=Pwd;
  end;
  VEForm.CheckBoxOffline.Checked:=Offline;
  if (User <> '') and (Pwd <> '') then begin
    LoginOK:=VEForm.LoginWithData;
  end
  else begin
    LoginOK:=VEForm.Login;
  end;
  if LoginOK then begin
     MenuFunctions['VEOnOff'].Checked:=True;
     Result:=0;
     VEForm.SetVectorRotation;
     if (VEForm.VEActive) and (Data^.PInfo^.ViewRotation <> 0) then begin
        Data^.PInfo^.SetViewRotation(0);
     end;
     VEForm.RefreshView;
  end;
end;

function TMDIChild.VEOff: Integer;
begin
     MenuFunctions['VEOnOff'].Checked:=False;
     VEForm.SetVectorRotation;
     if (VEForm.VEActive) and (Data^.PInfo^.ViewRotation <> 0) then begin
        Data^.PInfo^.SetViewRotation(0);
     end;
     VEForm.RefreshView;
     Result:=0;
end;

procedure TMDIChild.VEOrtho;
begin
     Data^.PInfo^.RedrawScreen(False);
end;

procedure TMDIChild.VEOrthoLabels;
begin
     Data^.PInfo^.RedrawScreen(False);
end;

procedure TMDIChild.VERoad;
begin
     Data^.PInfo^.RedrawScreen(False);
end;

procedure TMDIChild.VEAutomatic;
begin
     Data^.PInfo^.RedrawScreen(False);
end;

procedure TMDIChild.VEGeoCode;
begin
     VEForm.GeoCode;
end;

procedure TMDIChild.VEDBGeoCode;
begin
     VEForm.DBGeoCode;
end;

procedure TMDIChild.VESLExp;
begin
     VEForm.CreateXMLFile;
end;

procedure TMDIChild.VESLMap;
begin
     VEForm.ShowSLMap;
end;

procedure TMDIChild.VEShowInfo;
begin
     VEForm.ShowInfo;
end;

procedure TMDIChild.VEClearCache;
begin
     VEForm.ClearCache;
end;

procedure TMDIChild.VEFillCache;
begin
     VEForm.FillCache;
end;

procedure TMDIChild.VEBirdsEye;
begin
     VEForm.BirdsEye;
end;




procedure TMDIChild.ExtrasProjection;
begin
    //set projection Source and Destination
    AxGisProjection := TAxGisProjection.Create(Application);
    AxGisProjection.WorkingPath := OSInfo.WingisDir;
    AxGisProjection.ShowOnlySource := FALSE;
    AxGisProjection.SetupByDialog;
    {
    if Data <> nil then begin
      Data^.PInfo^.SetProjectionByDialog;
    end;
    }
    if WingisMainForm.AllowDraw then begin
      Data^.DrawGraphicTransform;
    end;
end;

function TMDIChild.MoveView(X,Y: Integer): Boolean;
var
ADist: Double;
begin
  Result:=False;
  {$IFNDEF AXDLL}
  Data^.CurrentViewRect:=Data^.PInfo^.GetCurrentScreen;
  ADist:=Min(Data^.CurrentViewRect.Width/4,Data^.CurrentViewRect.Height/4);
  if X < BORDER_AREA_WIDTH then begin
     MoveRect(Data^.CurrentViewRect,-ADist,0);
     Result:=True;
  end;
  if X > ClientWidth-BORDER_AREA_WIDTH then begin
     MoveRect(Data^.CurrentViewRect,ADist,0);
     Result:=True;
  end;
  if Y < BORDER_AREA_WIDTH then begin
     MoveRect(Data^.CurrentViewRect,0,ADist);
     Result:=True;
  end;
    if Y > ClientHeight-BORDER_AREA_WIDTH then begin
     MoveRect(Data^.CurrentViewRect,0,-ADist);
     Result:=True;
  end;
  if Result then begin
     Data^.ZoomByFactor(1.0);
  end;
  {$ENDIF}
end;

procedure TMDIChild.ShowBorderCursor(X, Y: Integer);
begin
     if (Data^.Cursor < idc_Right) or (Data^.Cursor > idc_LeftDown) then begin
        SavedCursor:=Data^.Cursor;
     end;
     if (X > ClientWidth-BORDER_AREA_WIDTH) and (Y < BORDER_AREA_WIDTH) then begin
        Data^.SetCursor(idc_RightUp);
     end
     else if (X > ClientWidth-BORDER_AREA_WIDTH) and (Y > ClientHeight-BORDER_AREA_WIDTH) then begin
        Data^.SetCursor(idc_RightDown);
     end
     else if (X < BORDER_AREA_WIDTH) and (Y < BORDER_AREA_WIDTH) then begin
        Data^.SetCursor(idc_LeftUp);
     end
     else if (X < BORDER_AREA_WIDTH) and (Y > ClientHeight-BORDER_AREA_WIDTH) then begin
        Data^.SetCursor(idc_LeftDown);
     end
     else if (X > ClientWidth-BORDER_AREA_WIDTH) then begin
        Data^.SetCursor(idc_Right);
     end
     else if (X < BORDER_AREA_WIDTH) then begin
        Data^.SetCursor(idc_Left);
     end
     else if (Y < BORDER_AREA_WIDTH) then begin
        Data^.SetCursor(idc_Up);
     end
     else if (Y > ClientHeight-BORDER_AREA_WIDTH) then begin
        Data^.SetCursor(idc_Down);
     end
     else begin
        Data^.SetCursor(SavedCursor);
        if (Data^.ActualMen = mn_Select) and (Data^.MenuHandler = nil) then begin
           Data^.SetCursor(-2);
        end;
     end;
end;

function TMDIChild.CheckNewTempFile(ATempFileName: AnsiString): Boolean;
const
ABufferSize = 32;
var
ABuffer: Array[0..31] of Byte;
i,n: Integer;
AFileStream: TFileStream;
begin
     Result:=False;
     AFileStream:=TFileStream.Create(ATempFileName,fmOpenRead);
     try
        ZeroMemory(@ABuffer,32);
        n:=AFileStream.Read(ABuffer,ABufferSize);
        for i:=0 to n-1 do begin
            if ABuffer[i] <> 0 then begin
               Result:=True;
               break;
            end;
        end;
     finally
        AFileStream.Free;
     end;
end;

procedure TMDIChild.ShowAds;
begin
     exit;

     PanelAds.Height:=52;
     PanelAds.Left:=0;
     PanelAds.Width:=ClientWidth;
     PanelAds.Top:=ClientHeight-PanelAds.Height;

     ImgAds.Top:=(PanelAds.Height div 2) - (ImgAds.Height div 2);
     ImgAds.Left:=50;

     LabelAdsLink.Top:=(PanelAds.Height div 2) - (LabelAdsLink.Height div 2);
     LabelAdsLink.Left:=PanelAds.Width - LabelAdsLink.Width - 50;

     LabelAdsText.Top:=(PanelAds.Height div 2) - (LabelAdsText.Height div 2);
     LabelAdsText.Left:=(PanelAds.Width div 2) - (LabelAdsText.Width div 2);

     PanelAds.Visible:=True;
end;

procedure TMDIChild.ExtrasWMS;
begin
     WMSForm.Show;
end;

procedure TMDIChild.ExtrasWKT;
var
FillText: Boolean;
i: Integer;
AIndex: PIndex;
AView: PView;
s: AnsiString;
SelCount: Integer;
begin
     SelCount:=0;
     WKTForm:=TWKTForm.Create(WingisMainForm);
     try
        if Data^.Layers^.SelLayer^.Data^.GetCount > 0 then begin
          SelCount:=Data.Layers^.SelLayer^.Data^.GetCount;
          if SelCount > 0 then begin
             Data^.SetCursor(crHourGlass);
             try
                for i:=0 to SelCount-1 do begin
                    AIndex:=Data^.Layers^.SelLayer^.Data^.At(i);
                    if (AIndex <> nil) and (AIndex^.GetObjType in [ot_Pixel,ot_Poly,ot_CPoly]) then begin
                       AView:=Data^.Layers^.IndexObject(Data^.PInfo,AIndex);
                       if AView <> nil then begin
                          s:='';
                          case AView^.GetObjType of
                             ot_Pixel: s:=PPixel(AView)^.ToWKT;
                             ot_Poly: s:=PPoly(AView)^.ToWKT;
                             ot_CPoly: s:=PCPoly(AView)^.ToWKT;
                          end;
                          if s <> '' then begin
                             WKTForm.Memo.Lines.Add(s);
                          end;
                       end;
                    end;
                end;
             finally
                Data^.SetCursor(crDefault);
             end;
          end;
        end;
        if WKTForm.ShowModal = mrOK then begin
           for i:=0 to WKTForm.Memo.Lines.Count-1 do begin
               Data^.DeSelectAll(False);
               Data^.InsertWKTObject(Data^.Layers^.TopLayer,Trim(WKTForm.Memo.Lines[i]));
           end;
        end;
     finally
        WKTForm.Free;
     end;
end;

{$IFNDEF AXDLL}
procedure TMDIChild.EditScalePolyline;
var
AIndex: PIndex;
AView: PView;
begin
     AIndex:=Data^.Layers^.SelLayer^.Data^.At(0);
     if (AIndex <> nil) and (AIndex^.GetObjType = ot_Poly) then begin
        AView:=Data^.Layers^.IndexObject(Data^.PInfo,AIndex);
        if AView <> nil then begin
           Data^.ScalePolyObj:=PPoly(AView);
           Data^.SetActualMenu(mn_ScalePolyline);
           Data^.SetStatusText(GetLangText(11750),True);
        end;
     end;
end;
{$ENDIF}

procedure TMDIChild.ReadProjectIni;
var
AFilename: AnsiString;
Ini: TIniFile;
BMUser,BMPwd,BMMode: AnsiString;
WMSFilename: AnsiString;
begin
     AFilename:=StrPas(FName);
     AFilename:=ChangeFileExt(AFilename,'.ini');
     if FileExists(AFilename) then begin
        Ini:=TIniFile.Create(AFilename);
        try
           if Ini.ReadInteger('BM','Active',0) > 0 then begin
              BMUser:=Ini.ReadString('BM','User','');
              BMPwd:=Ini.ReadString('BM','Pwd','');
              BMMode:=Ini.ReadString('BM','Mode','');
              if (BMUser <> '') and (BMPwd <> '') then begin
                 VEForm.EditUser.Text:=BMUser;
                 VEForm.EditPwd.Text:=BMPwd;
                 VEOnWithLoginData(BMuser,BMPwd,False);
                 if MenuFunctions['VEOnOff'].Checked then begin
                    if BMMode <> '' then begin
                       MenuFunctions.Call(BMMode);
                    end;
                 end;
              end;
           end;
           if Ini.ReadInteger('WMS','Active',0) > 0 then begin
              WMSFilename:=Ini.ReadString('WMS','File','');
              if Pos('\',WMSFilename) = 0 then begin
                 WMSFilename:=ExtractFilePath(StrPas(FName)) + WMSFilename;
              end;
              if (WMSFilename <> '') and (FileExists(WMSFilename)) then begin
                 WMSForm.OpenWWCFile(WMSFilename);
              end;
           end;
        finally
           Ini.Free;
        end;
     end;
end;

initialization
  MDIChildType:=TMDIChild.ClassName;
  DoesAutoSave:=FALSE;
end.

