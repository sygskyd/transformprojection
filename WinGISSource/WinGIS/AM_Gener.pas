{****************************************************************************}
{ Unit                                                                       }
{----------------------------------------------------------------------------}
{                                                                            }
{----------------------------------------------------------------------------}
{ Modifikationen                                                             }
{  16.02.1994 Martin Forst                                                   }
{  04-06-1998 Martin Forst  Corrected problem with big coordinates           }
{****************************************************************************}
unit AM_Gener;

interface

uses Messages, WinProcs, WinTypes, AM_Def, AM_Layer, AM_Admin, SysUtils,
  AM_Dlg1, AM_StdDl, Classes, ResDlg, Objects, Controls, ProjStyle;

const
  id_YCoordinate = 500; {1304F}
  id_XCoordinate = 501; {1304F}

type
  PGenerDat = ^TGenerDat;
  TGenerDat = object(TOldObject)
    GenRect: Boolean;
    XSpace: LongInt;
    YSpace: LongInt;
    SetPoints: Boolean;
    SetAreas: Boolean;
    PointsLayer: LongInt;
    AreasLayer: LongInt;
    StartPoint: TDPoint;
    constructor Init;
    constructor Load(S: TOldStream);
    procedure Store(S: TOldStream);
  end;

  PGenerDlg = ^TGenerDlg;
  TGenerDlg = class(TLayerDlg)
  public
    Data: PGenerDat;
    Coordinates: Integer;
    constructor Init(AParent: TComponent; AData: PGenerDat; ALayers: PLayers; AProjStyle: TProjectStyles;
      ACoordinates: Integer);
    function CanClose: Boolean; override;
    function GetSpacing(AID: Integer; var AValue: LongInt): Boolean;
    procedure SetupWindow; override;
    procedure SetLayerText(AId: Integer; ALayer: LongInt);
    procedure WMCommand(var Msg: TMessage); message wm_First + wm_Command;
  end;

procedure DoGenerate(AProj: Pointer; Rect: TDRect);

implementation

uses AM_Point, AM_Poly, AM_CPoly, AM_Proj, AM_Index, StateBar, UserIntf,
  CoordinateSystem;

const
  id_Rect = 100;
  id_Tri = 101;
  id_XSp = 102;
  id_YSp = 103;
  id_Points = 104;
  id_Areas = 105;
  id_PLayer = 106;
  id_ALayer = 107;

constructor TGenerDlg.Init
  (
  AParent: TComponent;
  AData: PGenerDat;
  ALayers: PLayers;
  AProjStyle: TProjectStyles;
  ACoordinates: Integer
  );
begin
  inherited Init(AParent, 'GEN', ALayers, AProjStyle);
  Data := AData;
  Coordinates := ACoordinates;
  HelpCOntext := 5000;
end;

procedure TGenerDlg.SetupWindow;
begin
  inherited SetupWindow;
  with Data^ do
  begin
    if GenRect then
      CheckRadioButton(Handle, id_Rect, id_Tri, id_Rect)
    else
      CheckRadioButton(Handle, id_Rect, id_Tri, id_Tri);
    SetFieldLong(id_XSp, XSpace);
    SetFieldLong(id_YSp, YSpace);
    if SetPoints then
      CheckDlgButton(Handle, id_Points, 1)
    else
      EnableWindow(GetItemHandle(id_PLayer), FALSE);
    if SetAreas then
      CheckDlgButton(Handle, id_Areas, 1)
    else
      EnableWindow(GetItemHandle(id_ALayer), FALSE);
    FillLayerList(id_PLayer, FALSE, TRUE);
    FillLayerList(id_ALayer, FALSE, TRUE);
    SetLayerText(id_PLayer, PointsLayer);
    SetLayerText(id_ALayer, AreasLayer);
  end;
  if Coordinates = Integer(ctGeodatic) then
  begin {1304F}
    SetFieldText(id_YCoordinate, GetLangText(866));
    SetFieldText(id_XCoordinate, GetLangText(865));
  end
  else
  begin
    SetFieldText(id_YCoordinate, GetLangText(865));
    SetFieldText(id_XCoordinate, GetLangText(866));
  end; {1304F}
end;

procedure TGenerDlg.WMCommand
  (
  var Msg: TMessage
  );
begin
  inherited;
{++ Ivanoff Bug#182}
{ The lParam was used instead of the wParam in detection whether a button has been clicked
  or menu item. This problem was OK under Win9x and appears under Win NT/2000.}
  case Msg.wParamHi of // case Msg.lParamHi of
{-- Ivanoff Bug#182}
    bn_Clicked:
      begin
        if Msg.wParam = id_Points then
          EnableWindow(GetItemHandle(id_PLayer), IsDlgButtonChecked(Handle, id_Points) = 1);
        if Msg.wParam = id_Areas then
          EnableWindow(GetItemHandle(id_ALayer), IsDlgButtonChecked(Handle, id_Areas) = 1);
      end;
  end;
end;

procedure TGenerDlg.SetLayerText
  (
  AID: Integer;
  ALayer: LongInt
  );
var
  BLayer: PLayer;
  AStr: array[0..255] of Char;
begin
  BLayer := Layers^.IndexToLayer(ALayer);
  if BLayer <> nil then
  begin
    PStrToPChar(AStr, BLayer^.Text);
    SendDlgItemMsg(AId, cb_SelectString, -1, LongInt(@AStr));
  end;
end;

function TGenerDlg.GetSpacing
  (
  AID: Integer;
  var AValue: LongInt
  )
  : Boolean;
var
  AText: array[0..255] of Char;
  AStr: string;
  Value: Real;
  Error: Integer;
begin
  if not GetFieldLong(AId, AValue) then
  begin
    MsgBox(Handle, 860, mb_IconExclamation or mb_OK, '');
    SetFocus(GetItemHandle(AID));
    GetSpacing := FALSE;
  end
  else
    GetSpacing := TRUE;
end;

function TGenerDlg.CanClose
  : Boolean;
var
  XSp: LongInt;
  YSp: LongInt;
  PLayer: LongInt;
  ALayer: LongInt;

  function DoGetLayer
      (
      AButton: Integer;
      AList: Integer;
      var ALayer: LongInt
      )
      : Boolean;
  begin
    if IsDlgButtonChecked(Handle, AButton) = 0 then
      DoGetLayer := TRUE
    else
      DoGetLayer := GetLayer(AList, ALayer, 860, 10125, 10128, ilSecond);
  end;
begin
  CanClose := FALSE;
  if GetSpacing(id_XSp, XSp) and GetSpacing(id_YSp, YSp) then
  begin
    if (IsDlgButtonChecked(Handle, id_Points) = 0)
      and (IsDlgButtonChecked(Handle, id_Areas) = 0) then
    begin
      MsgBox(Handle, 861, mb_IconExclamation or mb_OK, '');
      SetFocus(GetItemHandle(id_Points));
    end
    else
      if DoGetLayer(id_Points, id_PLayer, PLayer)
      and DoGetLayer(id_Areas, id_ALayer, ALayer) then
        with Data^ do
        begin
          if IsDlgButtonChecked(Handle, id_Rect) = 1 then
            Data^.GenRect := TRUE
          else
            Data^.GenRect := FALSE;
          XSpace := XSp;
          YSpace := YSp;
          SetPoints := IsDlgButtonChecked(Handle, id_Points) = 1;
          if SetPoints then
            PointsLayer := PLayer;
          SetAreas := IsDlgButtonChecked(Handle, id_Areas) = 1;
          if SetAreas then
            AreasLayer := ALayer;
          CanClose := TRUE;
        end;
  end;
end;

constructor TGenerDat.Init;
begin
  TOldObject.Init;
  GenRect := TRUE;
  XSpace := 0;
  YSpace := 0;
  SetPoints := TRUE;
  SetAreas := FALSE;
  PointsLayer := 0;
  AreasLayer := 0;
  StartPoint.Init(0, 0);
end;

constructor TGenerDat.Load
  (
  S: TOldStream
  );
var
  Version: Word;
begin
  TOldObject.Init;
  S.Read(Version, SizeOf(Version));
  S.Read(GenRect, SizeOf(GenRect));
  S.Read(XSpace, SizeOf(XSpace));
  S.Read(YSpace, SizeOf(YSpace));
  S.Read(SetPoints, SizeOf(SetPoints));
  S.Read(SetAreas, SizeOf(SetAreas));
  S.Read(PointsLayer, SizeOf(PointsLayer));
  S.Read(AreasLayer, SizeOf(AreasLayer));
  StartPoint.Load(S);
end;

procedure TGenerDat.Store
  (
  S: TOldStream
  );
var
  Version: Word;
begin
  Version := 0;
  S.Write(Version, SizeOf(Version));
  S.Write(GenRect, SizeOf(GenRect));
  S.Write(XSpace, SizeOf(XSpace));
  S.Write(YSpace, SizeOf(YSpace));
  S.Write(SetPoints, SizeOf(SetPoints));
  S.Write(SetAreas, SizeOf(SetAreas));
  S.Write(PointsLayer, SizeOf(PointsLayer));
  S.Write(AreasLayer, SizeOf(AreasLayer));
  StartPoint.Store(S);
end;

procedure DoGenerate
  (
  AProj: Pointer;
  Rect: TDRect
  );
var
  XPos: Double;
  YPos: Double;
  ALayer: PLayer;
  BLayer: PLayer;
  Pixel: PPixel;
  Point: TDPoint;
  StartX: Double;
  APoly: PCPoly;
  AOffset: Boolean;
  BOffset: Boolean;
  ItemCnt: LongInt;
  AllCount: LongInt;
  XSpaceDbl: Double;
  YSpaceDbl: Double;
begin
  with PProj(AProj)^, GenerData do
  begin
    XPos := StartPoint.X;
    YPos := StartPoint.Y;

    XPos := XPos + XSpace div 2;
    YPos := YPos - YSpace div 2;

    SetCursor(crHourGlass);
    StatusBar.ProgressPanel := TRUE;
    StatusBar.ProgressText := GetLangText(11740);
    StatusBar.AbortText := GetLangText(864);
    ItemCnt := 0;
    XSpaceDbl := XSpace;
    YSpaceDbl := YSpace;
    if XPos > Rect.A.X then
      XPos := XPos - Int((XPos - 1.0 * Rect.A.X) / XSpaceDbl) * XSpaceDbl
    else
      XPos := XPos + Int((1.0 * Rect.A.X - XPos) / XSpaceDbl) * XSpaceDbl;
    if YPos > Rect.A.Y then
      YPos := YPos - Int((YPos - 1.0 * Rect.A.Y) / YSpaceDbl) * YSpaceDbl
    else
      YPos := YPos + Int((1.0 * Rect.A.Y - YPos) / YSpaceDbl) * YSpaceDbl;
    StartX := XPos;
    BOffset := FALSE;
    AllCount := LimitToLong(0.5 + (1.0 * Rect.B.X - XPos) / XSpaceDbl) * LimitToLong(0.5 + (1.0 * Rect.B.Y - YPos) / YSpaceDbl);
    if SetPoints then
      ALayer := Layers^.IndexToLayer(PointsLayer);
    if SetAreas then
      BLayer := Layers^.IndexToLayer(AreasLayer);
    try
      while YPos <= Rect.B.Y do
      begin
        XPos := StartX;
        AOffset := BOffset;
        while XPos <= Rect.B.X do
        begin
          Inc(ItemCnt);
          Point.Init(LimitToLong(XPos), LimitToLong(YPos));
          if SetPoints then
          begin
            Pixel := New(PPixel, Init(Point));
            CorrectSize(Pixel^.ClipRect, FALSE);
{++ Ivanoff Bug#148}
            PProj(AProj).InsertObjectOnLayer(Pixel, ALayer.Index);
// It was >     ALayer^.InsertObject(PInfo,New(PIndex,Init(Pixel^.Index)),FALSE);
{-- Ivanoff Bug#148}
          end;
          if SetAreas then
          begin
            if GenRect then
            begin
              APoly := New(PCPoly, Init);
              Point.Init(LimitToLong(XPos - XSpace div 2), LimitToLong(YPos - YSpace div 2));
              APoly^.InsertPoint(Point);
              Point.Move(0, YSpace);
              APoly^.InsertPoint(Point);
              Point.Move(XSpace, 0);
              APoly^.InsertPoint(Point);
              Point.Move(0, -YSpace);
              APoly^.InsertPoint(Point);
              APoly^.ClosePoly;
              CorrectSize(APoly^.ClipRect, FALSE);
{++ Ivanoff Bug#148}
              PProj(AProj).InsertObjectOnLayer(APoly, BLayer.Index);
// It was >       BLayer^.InsertObject(PInfo,New(PIndex,Init(APoly^.Index)),FALSE);
{-- Ivanoff Bug#148}
            end
            else
            begin
              APoly := New(PCPoly, Init);
              if AOffset then
              begin
                Point.Init(LimitToLong(XPos), LimitToLong(YPos + YSpace div 2));
                APoly^.InsertPoint(Point);
                Point.Move(-XSpace, -YSpace);
                APoly^.InsertPoint(Point);
                Point.Move(2 * XSpace, 0);
                APoly^.InsertPoint(Point);
              end
              else
              begin
                Point.Init(LimitToLong(XPos - XSpace), LimitToLong(YPos + YSpace div 2));
                APoly^.InsertPoint(Point);
                Point.Move(XSpace, -YSpace);
                APoly^.InsertPoint(Point);
                Point.Move(XSpace, YSpace);
                APoly^.InsertPoint(Point);
              end;
              APoly^.ClosePoly;
              CorrectSize(APoly^.ClipRect, FALSE);
{++ Ivanoff Bug#148}
              PProj(AProj).InsertObjectOnLayer(APoly, BLayer.Index);
// It was >       BLayer^.InsertObject(PInfo,New(PIndex,Init(APoly^.Index)),FALSE);
{-- Ivanoff Bug#148}
              AOffset := not AOffset;
            end;
          end;
          XPos := XPos + XSpace;
          if Statusbar.QueryAbort then
          begin
            XPos := Rect.B.X + 1;
            YPos := Rect.B.Y + 1;
          end;
        end;
        YPos := YPos + YSpace;
        BOffset := not BOffset;
        StatusBar.Progress := 100 * ItemCnt / AllCount;
      end;
    finally
      StatusBar.ProgressPanel := FALSE;
      SetCursor(crDefault);
    end;
    UpdatePaintOffset;
    PInfo^.RedrawScreen(TRUE);
  end;
end;

end.

