Unit SelectLayerDlg;

Interface
{$IFNDEF AXDLL} // <----------------- AXDLL
Uses Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     StdCtrls,MultiLng,AM_Proj,StyleDef,ProjStyle,WCtrls;

Type TSelectLayerDialog = class(TWForm)
       ListBox     : TListBox;
       OkBtn       : TButton;
       CancelBtn   : TButton;
       NewLayerBtn : TButton;
       MlgSection  : TMlgSection;
       Procedure   FormCreate(Sender: TObject);
       procedure   FormDestroy(Sender: TObject);
       Procedure   FormHide(Sender: TObject);
       Procedure   FormShow(Sender: TObject);
       Procedure   NewLayerBtnClick(Sender: TObject);
       Procedure   ListBoxDblClick(Sender: TObject);
    procedure ListBoxClick(Sender: TObject);
      Private
       FNewLayerBtn     : Boolean;
       FNewLayerStyle   : TLayerProperties;
       FProject         : PProj;
       FProjectStyles   : TProjectStyles;
       FShowInvisible   : Boolean;
       FShowLocked      : Boolean;
       FSelectedLayer   : LongInt;
      Public
       Property    Project:PProj read FProject write FProject;
       Property    ProjectStyles:TProjectStyles read FProjectStyles write FProjectStyles;
       Property    SelectedLayer:LongInt read FSelectedLayer write FSelectedLayer;
       Property    ShowNewLayerButton:Boolean read FNewLayerBtn write FNewLayerBtn default False;
       Property    ShowInvisibleLayers:Boolean read FShowInvisible write FShowInvisible default False;
       Property    ShowLockedLayers:Boolean read FShowLocked write FShowLocked default False;
     end;
{$ENDIF} // <----------------- AXDLL
Implementation
{$IFNDEF AXDLL} // <----------------- AXDLL
{$R *.DFM}

Uses AM_Def,AM_Layer,LayerPropertyDlg;

Procedure TSelectLayerDialog.FormShow(Sender: TObject);
var Cnt          : Integer;
    ALayer       : PLayer;
begin
  // fill the list with the layer-names
  for Cnt:=1 to FProject^.Layers^.LData^.Count-1 do begin
    ALayer:=FProject^.Layers^.LData^.At(Cnt);
    if (FShowLocked or not(ALayer^.GetState(sf_Fixed)))
        and (FShowInvisible or not ALayer^.GetState(sf_LayerOff)) then
      Listbox.Items.Add(PToStr(ALayer^.Text));
  end;
  // focus the currently selected layer
  ALayer:=FProject^.Layers^.IndexToLayer(FSelectedLayer);
  if ALayer<>NIL then Listbox.Items.IndexOf(PToStr(ALayer^.Text));
  // select first layer if the selected layer was not found
  if (Listbox.ItemIndex<0) and (Listbox.Items.Count>0) then Listbox.ItemIndex:=0;
  // update controls
  NewLayerBtn.Visible:=FNewLayerBtn;
  if FProject^.PInfo^.IsMU then begin
     NewLayerBtn.Visible:=False;
  end;
end;

Procedure TSelectLayerDialog.FormCreate(Sender: TObject);
begin
  FSelectedLayer:=-1;
  FNewLayerStyle:=TLayerProperties.Create;
end;

Procedure TSelectLayerDialog.FormHide(Sender: TObject);
var ALayer       : PLayer;
begin
  if ModalResult=mrOK then begin
    ALayer:=FProject^.Layers^.NameToLayer(Listbox.Items[Listbox.ItemIndex]);
    if ALayer<>NIL then FSelectedLayer:=ALayer^.Index
    else FSelectedLayer:=-1;
  end
  else if ModalResult=mrYes then begin
    ALayer:=FProject^.Layers^.NameToLayer(FNewLayerStyle.Name);
    if ALayer = nil then begin
       ALayer:=FProject^.Layers^.InsertLayer(FNewLayerStyle.Name,0,0,0,0,ilSecond,DefaultSymbolFill);
       ALayer^.SetLayerStyle(FProject^.PInfo,FNewLayerStyle);
    end;
    FSelectedLayer:=ALayer^.Index;
  end;
end;

Procedure TSelectLayerDialog.NewLayerBtnClick(Sender: TObject);
var Dialog       : TLayerPropertyDialog;
begin
  Dialog:=TLayerPropertyDialog.Create(Self);
  try
    Dialog.Project:=FProject;
    Dialog.ProjectStyles:=FProject^.PInfo^.ProjStyles;
    Dialog.LayerStyle:=FNewLayerStyle;
    if Dialog.ShowModal=mrOK then ModalResult:=mrYes;
  finally
    Dialog.Free;
  end;
end;

Procedure TSelectLayerDialog.ListBoxDblClick(Sender: TObject);
begin
  ModalResult:=mrOK;
end;

Procedure TSelectLayerDialog.FormDestroy(Sender: TObject);
begin
  FNewLayerStyle.Free;
end;

procedure TSelectLayerDialog.ListBoxClick(Sender: TObject);
begin
     If ListBox.ItemIndex <> -1 then OkBtn.Enabled:=True;
end;
{$ENDIF} // <----------------- AXDLL
end.
