unit prnframe;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, WCtrls, Combobtn, ColorSelector, Buttons, Validate, Spinbtn,
  ExtCtrls,AM_Group,AM_Sym,SymbolSelDlg,Am_font,am_proj,numtools ,FontStyleDlg, Am_def,measures,styledef,
  MultiLng,PropCollect,AM_INI;

type
  TPrnFramedialog = class(TForm)
    WGroupBox1: TWGroupBox;
    Button1: TButton;
    Button2: TButton;
    AddModeCheck: TCheckBox;
    WGroupBox2: TWGroupBox;
    Usecrosscheck: TCheckBox;
    Label1: TLabel;
    WGroupBox3: TWGroupBox;
    WGroupBox4: TWGroupBox;
    Window1: TWindow;
    Addmarkcheck: TCheckBox;
    RightCheck: TCheckBox;
    TopCheck: TCheckBox;
    LeftCheck: TCheckBox;
    BottomCheck: TCheckBox;
    SpinBtn1: TSpinBtn;
    DipEdit: TEdit;
    Crosspanel: TPanel;
    ColorDialog1: TColorDialog;
    Steplenghtlabel: TLabel;
    Label2: TLabel;
    CrossEdit: TEdit;
    MarkEdit: TEdit;
    Marklabel: TLabel;
    SpinBtn2: TSpinBtn;
    FontBtn: TButton;
    CrossValid: TMeasureLookupValidator;
    MarkValid: TMeasureLookupValidator;
    StepValid: TMeasureLookupValidator;
    LoadBtn: TButton;
    SaveBtn: TButton;
    Label3: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    DimLabel: TLabel;
    Scalelabel: TLabel;
    DimValid: TMeasureValidator;
    WLabel2: TWLabel;
    Ax: TLabel;
    Ma: TLabel;
    Cr: TLabel;
    SpinBtn3: TSpinBtn;
    Label7: TLabel;
    Gridpanel: TPanel;
    MlgSection: TMlgSection;
    CheckBoxOutside: TCheckBox;
    procedure AddModeCheckClick(Sender: TObject);
    procedure AddmarkcheckClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure UsecrosscheckClick(Sender: TObject);
    procedure usesymbcheckClick(Sender: TObject);
    procedure FontBtnClick(Sender: TObject);
    procedure CrosspanelClick(Sender: TObject);
    procedure SelectSymClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ChangeFontPreview;
    function StatisticCalculate(Sender:TObject):longint;
    procedure ShowDimension;
    procedure StepChange(Sender:TObject);
    procedure LoadBtnClick(Sender: TObject);
    procedure SaveBtnClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure GridpanelClick(Sender: TObject);
    procedure LeftCheckClick(Sender: TObject);
    procedure RightCheckClick(Sender: TObject);
    procedure TopCheckClick(Sender: TObject);
    procedure BottomCheckClick(Sender: TObject);
    procedure CrossEditChange(Sender: TObject);
    procedure DipEditChange(Sender: TObject);
    procedure MarkEditChange(Sender: TObject);
  private
    { Private declarations }
  public
  FSymbol: PSGroup;
  FFonts:Pfonts;
  Fproject:PProj;
  FWidth:double;
  FHeight:double;
  GlobalFlag:boolean;
  FTextStyle : TTextProperties;
 // TmpTextstyle:
    { Public declarations }
  procedure UpdateSettings;
 end;

var
  PrnFramedialog: TPrnFramedialog;

implementation

{$R *.DFM}

uses Am_Main,PrintDlg;

procedure TPrnFramedialog.AddModeCheckClick(Sender: TObject);
begin
 //WGroupBox3.Visible:=AddModeCheck.Checked;
// WGroupBox2.Visible:=AddModeCheck.Checked;
// WGroupBox4.Visible:=AddModeCheck.Checked;
 Usecrosscheck.Enabled:=AddModeCheck.Checked;
 Addmarkcheck.Enabled:=AddModeCheck.Checked;
 Addmarkcheck.OnClick(Self);
// Usesymbcheck.OnClick(Self);
 Usecrosscheck.OnClick(self);
 StatisticCalculate(nil);
end;

procedure TPrnFramedialog.AddmarkcheckClick(Sender: TObject);
begin
 FontBtn.Enabled:= Addmarkcheck.Checked;
// FontWindow.Enabled:= Addmarkcheck.Checked;
 LeftCheck.Enabled:= Addmarkcheck.Checked;
 RightCheck.Enabled:= Addmarkcheck.Checked;
 TopCheck.Enabled:= Addmarkcheck.Checked;
 BottomCheck.Enabled:= Addmarkcheck.Checked;
 DipEdit.Enabled:= Addmarkcheck.Checked;
 SpinBtn1.Enabled:=  Addmarkcheck.Checked;
 Steplenghtlabel.Enabled:=  Addmarkcheck.Checked;
 MarkLabel.Enabled:= Addmarkcheck.Checked;
 MarkEdit.Enabled:= Addmarkcheck.Checked;
 SpinBtn3.Enabled:=  Addmarkcheck.Checked;
 CheckBoxOutside.Enabled:=AddMarkcheck.Checked;
// Ax.Visible:=  Addmarkcheck.Checked;
// Ma.Visible:=  Addmarkcheck.Checked;
// Te.Visible:=  Addmarkcheck.Checked;
 StatisticCalculate(nil);
end;

procedure TPrnFramedialog.FormShow(Sender: TObject);
begin
 AddModeCheck.OnClick(self);
   if Fproject.FramePrintSpec.StepLength=0
    then
      begin;
        if FWidth<FHeight then
        StepValid[muDrawingUnits]:= abs(Round(FWidth/10))
                      else
       StepValid[muDrawingUnits]:= abs(Round(FHeight/10));
     end
else
begin;
StepValid[muDrawingUnits]:= abs(Round(ConvertMeasure(Fproject.FramePrintSpec.StepLength,muDrawingUnits,muDrawingUnits)))/1000;
end;
CrossValid[muDrawingUnits]:= Round(ConvertMeasure(Fproject.FramePrintSpec.CrossLength,muDrawingUnits,muDrawingUnits))/1000;
MarkValid[muDrawingUnits]:= Round(ConvertMeasure(Fproject.FramePrintSpec.MarksLength,muDrawingUnits,muDrawingUnits))/100;

with Fproject.FramePrintSpec do begin;
  LeftCheck.Checked:=UseLeft;
  RightCheck.Checked:=UseRight;
  TopCheck.Checked:=UseTop;
  BottomCheck.Checked:=UseBottom;

  UseCrossCheck.Checked:=UseCross;
  AddmarkCheck.Checked:=UseTextMarks;
  AddModeCheck.Checked:=UseSpecFrame;

  CrossPanel.COlor:=CrossColor;
  GridPanel.COlor:=GridColor;

  CheckBoxOutside.Checked:=LabelOutside;
end;

StepValid.OnChangeUnits:=StepChange;
//StepValid.OnEditChangeData:= StatisticCalculate;
//MarkValid.OnEditChangeData:=StatisticCalculate;
//CrossValid.OnEditChangeData:=StatisticCalculate;
ShowDimension;
StatisticCalculate(nil);
end;

procedure TPrnFramedialog.UsecrosscheckClick(Sender: TObject);
begin
  Label1.Enabled:= Usecrosscheck.Checked;
  Label2.Enabled:= Usecrosscheck.Checked;
   Crosspanel.Enabled:= Usecrosscheck.Checked;
   Spinbtn2.Enabled:= Usecrosscheck.Checked;
   CrossEdit.Enabled:= Usecrosscheck.Checked;
  // StatisticCalculate(nil);
 StatisticCalculate(nil);
end;

procedure TPrnFramedialog.usesymbcheckClick(Sender: TObject);
begin
(*SymName.Enabled:= usesymbcheck.checked;
SymLib.Enabled:=  usesymbcheck.checked;
Window2.Enabled:=  usesymbcheck.checked;
SelectSym.Enabled:= usesymbcheck.checked; *)
 StatisticCalculate(nil);
end;

procedure TPrnFramedialog.FontBtnClick(Sender: TObject);
 var  FontstyleDialog: TFontstyleDialog;
      ARect        : TRect;
      XPos         : Integer;
      BRect        : TRect;
      AFontDes     : PFontDes;
begin

     FontStyleDialog := TFontStyleDialog.Create (Self);
     FontStyleDialog.Fonts := FProject.Pinfo.Fonts;


      with FProject^ do begin;
       AFontDes := PInfo^.Fonts^.GetFont(FramePrintSpec.Font.Font);
       FontStyleDialog.TextStyle.UpdatesEnabled := TRUE;
       FontStyleDialog.FixedHeightCheck.Visible:=False;
       //FontStyleDialog.SetColorSupport(FramePrintSpec.Color,FramePrintSpec.BackColor);
       CollectFontStyle (@FramePrintSpec.Font, AFontDes, FontStyleDialog.TextStyle,
       0,FramePrintSpec.Color,FramePrintSpec.BackColor);
         with FontstyleDialog do
          FontstyleDialog.SetNewCaption(18);

   if FontStyleDialog.ShowModal = mrOk
   then
     BEGIN;
      with FProject^ do
        begin;
        If FontStyleDialog.TextStyle.FontName=''
         then
          FProject.FramePrintSpec.Font.Font:=1
         else
          FProject.FramePrintSpec.Font.Font:=PInfo^.Fonts^.IDFromName(FontStyleDialog.TextStyle.FontName);
          FramePrintSpec.Font.Height := LimitToLong (FontStyleDialog.TextStyle.Height * 100);
       if FontStyleDialog.TextStyle.Bold = bsYes
        then
         FramePrintSpec.Font.Style := FramePrintSpec.Font.Style or ts_Bold
        else
        if FontStyleDialog.TextStyle.Bold = bsNo then FramePrintSpec.Font.Style := FramePrintSpec.Font.Style and not ts_Bold;
        if FontStyleDialog.TextStyle.Italic = bsYes
        then
         FramePrintSpec.Font.Style := FramePrintSpec.Font.Style or ts_Italic
        else
        if FontStyleDialog.TextStyle.Italic = bsNo then FramePrintSpec.Font.Style := FramePrintSpec.Font.Style and not ts_Italic;

        if FontStyleDialog.TextStyle.UnderLined = bsYes
        then
         FramePrintSpec.Font.Style := FramePrintSpec.Font.Style or ts_Underl
        else
        if FontStyleDialog.TextStyle.Underlined = bsNo then FramePrintSpec.Font.Style := FramePrintSpec.Font.Style and not ts_Underl;

     if FontStyleDialog.TextStyle.Transparent = bsYes
        then
         FramePrintSpec.Font.Style := FramePrintSpec.Font.Style or ts_Transparent
        else
        if FontStyleDialog.TextStyle.Transparent = bsNo then FramePrintSpec.Font.Style := FramePrintSpec.Font.Style and not ts_Transparent;

     if FontStyleDialog.TextStyle.FixedHeight = bsYes
        then
         FramePrintSpec.Font.Style := FramePrintSpec.Font.Style or ts_FixHeight
        else
        if FontStyleDialog.TextStyle.FixedHeight = bsNo then FramePrintSpec.Font.Style := FramePrintSpec.Font.Style and not ts_FixHeight;

             case FontStyleDialog.TextStyle.Alignment of
               taRight: FramePrintSpec.Font.Style := (FramePrintSpec.Font.Style or ts_Right) and not (ts_Center);
               taCentered: FramePrintSpec.Font.Style := (FramePrintSpec.Font.Style or ts_Center) and not (ts_Right);
               taLeft: FramePrintSpec.Font.Style := (FramePrintSpec.Font.Style) and not (ts_Right or ts_Center);
                                       end;


     FramePrintSpec.Color:= FontStyleDialog.FForeColor;
     FramePrintSpec.BackColor:= FontStyleDialog.FBackColor;
          end; //with
        end;//with
     END; //mrok

    ChangeFontPreview;
    StatisticCalculate(nil);
end;

procedure TPrnFramedialog.CrosspanelClick(Sender: TObject);
begin
//ShowMessage('Sorry,not implemented yet');
//exit;
ColorDialog1.Color:=Crosspanel.Color;
if ColorDialog1.Execute
then
if CrossPanel.Color <> ColorDialog1.Color
 then
  begin;
  CrossPanel.Color:= ColorDialog1.Color;
  end;
 StatisticCalculate(nil);
end;

procedure TPrnFramedialog.SelectSymClick(Sender: TObject);
begin
(*Fsymbol:=GetSymbol(Self,WingisMainForm.ActualProj);
SymName.Caption:=FSymbol.GetName;
SymLib.Caption:= FSymbol.GetLibName;
if SymLib.Caption='' then SymLib.Caption:='ProjSymLib';
WindowPaint;*)
 StatisticCalculate(nil);
end;

procedure TPrnFramedialog.FormCreate(Sender: TObject);
begin
 FProject:=PProj(WingisMainForm.ActualProj);
 //FFonts:=FProject.PInfo^.Fonts;
 FTextStyle:=TTextProperties.Create;
 //FProject.FramePrintSpec.Font:=-1;
 if FProject.FramePrintSpec.Font.Font <= 0 then begin
    FProject.FramePrintSpec.Font.Font:=PProj(WingisMainForm.ActualProj)^.PInfo^.Fonts^.IDFromName('Arial');
 end;
 if FProject.FramePrintSpec.Font.Height < 10 then begin
    FProject.FramePrintSpec.Font.Height:=500;
 end;
end;

procedure TPrnFramedialog.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
 FTextStyle.Free;

 if ModalResult=mrOk then begin
    UpdateSettings;
 end;
end;

Procedure TPrnFrameDialog.ChangeFontPreview;

  var ARect        : TRect;
    XPos         : Integer;
    BRect        : TRect;
    FontString:string;
    AFontDes: PFontDes;
begin
     with FProject.FramePrintSpec do begin
          AFontDes := FProject^.PInfo^.Fonts^.GetFont(FProject.FramePrintSpec.Font.Font);
          IF AFontDes=nil then Exit;
          FontString:= PToStr(AFontDes^.Name);
    end;
 end;


Function TPrnFrameDialog.StatisticCalculate(Sender:TObject):longInt;
  var AxesCount:longint;
      TextCount:longint;
      MarkCount:longint;
      CrossCount:longint;
      Awidth:double;
      AHeight:double;
      tmp:string;

  Procedure Calc(ACheckbox:TCheckBox;Key:boolean);
   var TmpValue:integer;
   begin;
       if ACheckBox.Checked
         then
          begin;
           Inc(AxesCount);
           if Key=true then
           TmpValue:= round(AHeight / StepValid.AsFloat)-1
                       else
           TmpValue:= round(AWidth / StepValid.AsFloat)-1;
           if TmpValue>0 then MarkCount:= MarkCount+TmpValue;
         end;
   end;


   begin;
   Result:=0;

   if AddModeCheck.Checked  then begin
     Button1.Enabled:=not ((AddMarkCheck.Checked=false) and  (UseCrossCheck.Checked=false));
   end;

  if AddModecheck.Checked = false
    then Exit;

  Awidth:=ConvertMeasure(FWidth,muDrawingUnits,StepValid.Units);
  AHeight:=ConvertMeasure(FHeight,muDrawingUnits,StepValid.Units);

   AxesCount:=0;
   TextCount:=0;
   MarkCount:=0;

   Ax.Caption:='0';
   Ma.Caption:='0';
   Cr.Caption:='0';


     if AddMarkCheck.Checked=true then begin;
       Calc(LeftCheck,true);
       Calc(RightCheck,true);
       Calc(TopCheck,false);
       Calc(BottomCheck,false);

          Ax.Caption:= IntToStr(AxesCount);
          Ma.Caption:= IntToStr(MarkCount);


                                         end;

       If Usecrosscheck.checked = true
       then
        begin;
         Result:=(trunc(AWidth/StepValid.AsFloat)-1)*(trunc(AHeight/StepValid.AsFloat)-1);
         Cr.Caption:=IntToStr(Result);
        end;

       Result:= AxesCount+MarkCount+TextCount+Result;
       //

   end;

 Procedure TPrnFrameDialog.ShowDimension;
 // var TmpString:Ansistring;
  var FloatT:double;
  begin;
  try
 //  FloatT:=ConvertMeasure(FWidth,muMillimeters,StepValid.Units);
  // DimLabel.Caption:=FloatToStr(FloatT);

 //  exit;
//   DimValid.Units:=muMillimeters;
   DimValid.Value:=FWidth;
   DimValid.Units:= StepValid.Units;
   DimLabel.Caption:= DimValid.AsText;
//   DimValid.Units:=muMillimeters;
   DimValid.Value:=FHeight;
   DimValid.Units:= StepValid.Units;
   DimLabel.Caption:= DimLabel.Caption + ' x '+ DimValid.AsText;
   SpinBtn3.Increment:=Round(StepValid.asFloat/100);
   except
     DimLabel.Caption:='';
   end;
  end;

Procedure TPrnFramedialog.StepChange(Sender:TObject);
 begin;
 ShowDimension;
 StatisticCalculate(nil);
 end;



procedure TPrnFramedialog.LoadBtnClick(Sender: TObject);
begin
IniFile.ReadSpecFrameRect(FProject);
Self.FormShow(Self);
ChangeFontPreview;
StatisticCalculate(nil);
end;

procedure TPrnFramedialog.SaveBtnClick(Sender: TObject);
begin
if CheckValidators(Self)= true then begin
   UpdateSettings;
   IniFile.WriteSpecFrameRect(FProject);
end;
end;

procedure TPrnFramedialog.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
if ModalResult = mrOk then begin
   CanClose:=CheckValidators(Self);
end;
end;

procedure TPrnFramedialog.GridpanelClick(Sender: TObject);
begin
ColorDialog1.Color:=Gridpanel.Color;
if ColorDialog1.Execute
then
if GridPanel.Color <> ColorDialog1.Color
 then
  begin;
  GridPanel.Color:= ColorDialog1.Color;
  end;
end;

procedure TPrnFramedialog.LeftCheckClick(Sender: TObject);
begin
     StatisticCalculate(nil);
end;

procedure TPrnFramedialog.RightCheckClick(Sender: TObject);
begin
     StatisticCalculate(nil);
end;

procedure TPrnFramedialog.TopCheckClick(Sender: TObject);
begin
     StatisticCalculate(nil);
end;

procedure TPrnFramedialog.BottomCheckClick(Sender: TObject);
begin
     StatisticCalculate(nil);
end;

procedure TPrnFramedialog.UpdateSettings;
begin
 with FProject.FramePrintSpec do begin
  UseSpecFrame:= AddModeCheck.Checked;
  UseTextMarks:= AddmarkCheck.Checked;
  UseLeft := LeftCheck.Checked;
  UseRight:= RightCheck.Checked;
  UseTop:= TopCheck.Checked;
  UseBottom:= BottomCheck.Checked;
  StepLength:=Round(abs(ConvertMeasure(StepValid.Value,StepValid.Units,muDrawingUnits))*1000);
  MarksLength:=Round(ConvertMeasure(MarkValid.Value,MarkValid.Units,muDrawingUnits)*100);
  UseCross:=UseCrossCheck.Checked;
  CrossLength:=Round(ConvertMeasure(CrossValid.Value,CrossValid.Units,muDrawingUnits)*1000);
  CrossColor:=CrossPanel.Color;
  GridColor:=GridPanel.Color;
  LabelOutside:=CheckBoxOutside.Checked;
 end;
end;

procedure TPrnFramedialog.CrossEditChange(Sender: TObject);
begin
     StatisticCalculate(nil);
end;

procedure TPrnFramedialog.DipEditChange(Sender: TObject);
begin
     StatisticCalculate(nil);
end;

procedure TPrnFramedialog.MarkEditChange(Sender: TObject);
begin
     StatisticCalculate(nil);
end;

end.
