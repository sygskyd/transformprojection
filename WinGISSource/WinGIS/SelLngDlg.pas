unit SelLngDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TFormSelLng = class(TForm)
    ButtonOK: TButton;
    ListBox: TListBox;
    procedure FormShow(Sender: TObject);
    procedure ListBoxClick(Sender: TObject);
    procedure ListBoxDblClick(Sender: TObject);
  public
    CurDir: AnsiString;
    LngFilename: AnsiString;
    ApplName: AnsiString;
    EnFileName: AnsiString;
    DeFileName: AnsiString;
    RsFileName: AnsiString;
    RoFileName: AnsiString;
    RuFileName: AnsiString;
  end;

var
  FormSelLng: TFormSelLng;

const
  EnName = 'English';
  DeName = 'Deutsch';
  RsName = 'Srpski';
  RoName = 'Rom�na';
  RuName = 'Russian';

implementation

{$R *.DFM}

procedure TFormSelLng.FormShow(Sender: TObject);
begin
     CurDir:=ExtractFilePath(Application.ExeName);
     if Copy(CurDir,Length(CurDir),1) <> '\' then CurDir:=CurDir + '\';

     ApplName:=ExtractFileName(Application.ExeName);
     ApplName:=ChangeFileExt(ApplName,'');

     EnFileName:=CurDir + ApplName + '.044';
     DeFileName:=CurDir + ApplName + '.049';
     RsFileName:=CurDir + ApplName + '.381';
     RoFileName:=CurDir + ApplName + '.040';
     RuFileName:=CurDir + ApplName + '.075';

     if FileExists(EnFileName) then begin
        ListBox.Items.Add(EnName);
     end;

     if FileExists(DeFileName) then begin
        ListBox.Items.Add(DeName);
     end;

     if FileExists(RuFileName) then begin
        ListBox.Items.Add(RuName);
     end;

     if FileExists(RsFileName) then begin
        ListBox.Items.Add(RsName);
     end;

     if FileExists(RoFileName) then begin
        ListBox.Items.Add(RoName);
     end;
end;

procedure TFormSelLng.ListBoxClick(Sender: TObject);
var
s: AnsiString;
begin
     s:=ListBox.Items[ListBox.ItemIndex];
     if s = EnName then begin
        LngFileName:=EnFileName;
     end
     else if s = DeName then begin
        LngFileName:=DeFileName;
     end
     else if s = RsName then begin
        LngFileName:=RsFileName;
     end
     else if s = RoName then begin
        LngFileName:=RoFileName;
     end
     else if s = RuName then begin
        LngFileName:=RuFileName;
     end
     else begin
        LngFileName:=EnFileName;
     end;
     ButtonOK.Enabled:=True;
end;

procedure TFormSelLng.ListBoxDblClick(Sender: TObject);
begin
     ModalResult:=mrOK;
end;

end.
