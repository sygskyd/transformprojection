unit ProjectionTools;

interface

uses AM_Paint, AXGisPro_TLB, WinOSInfo;

function LongLatToXY(var X,Y: Double): Integer;
function XYToLongLat(var X,Y: Double): Integer;

var
AxGisPro: TAXGisProjection = nil;
PInfo: PPaint;

implementation

function CheckProj: Boolean;
begin
     Result:=True;

     if PInfo = nil then begin
        Result:=False;
        exit;
     end;

     if PInfo^.ProjectionSettings.ProjectProjection = 'NONE' then begin
        Result:=False;
        exit;
     end;

     if AxGisPro = nil then begin
        try
           AxGisPro:=TAxGisProjection.Create(nil);
           AxGisPro.WorkingPath:=OSInfo.WingisDir;
           AxGisPro.LngCode:='044';
           Result:=AxGisPro <> nil;
        except
           Result:=False;
        end;
     end;
end;

function LongLatToXY(var X,Y: Double): Integer;
begin
     Result:=1;

     if not CheckProj then exit;

     AxGisPro.SourceProjSettings:='Geodetic Phi/Lamda';
     AxGisPro.SourceProjection:='Phi/Lamda values';
     AxGisPro.SourceDate:='WGS84';
     AxGisPro.SourceXOffset:=0;
     AxGisPro.SourceYOffset:=0;
     AxGisPro.SourceScale:=1;

     AxGisPro.TargetProjSettings:=PInfo^.ProjectionSettings.ProjectProjSettings;
     AxGisPro.TargetProjection:=PInfo^.ProjectionSettings.ProjectProjection;
     AxGisPro.TargetDate:=PInfo^.ProjectionSettings.ProjectDate;
     AxGisPro.TargetXOffset:=PInfo^.ProjectionSettings.ProjectionXOffset;
     AxGisPro.TargetYOffset:=PInfo^.ProjectionSettings.ProjectionYOffset;
     AxGisPro.TargetScale:=PInfo^.ProjectionSettings.ProjectionScale;

     AxGisPro.Calculate(X,Y);

     Result:=0;
end;

function XYToLongLat(var X,Y: Double): Integer;
begin
     Result:=1;

     if not CheckProj then exit;

     AxGisPro.SourceProjSettings:=PInfo^.ProjectionSettings.ProjectProjSettings;
     AxGisPro.SourceProjection:=PInfo^.ProjectionSettings.ProjectProjection;
     AxGisPro.SourceDate:=PInfo^.ProjectionSettings.ProjectDate;
     AxGisPro.SourceXOffset:=PInfo^.ProjectionSettings.ProjectionXOffset;
     AxGisPro.SourceYOffset:=PInfo^.ProjectionSettings.ProjectionYOffset;
     AxGisPro.SourceScale:=PInfo^.ProjectionSettings.ProjectionScale;

     AxGisPro.TargetProjSettings:='Geodetic Phi/Lamda';
     AxGisPro.TargetProjection:='Phi/Lamda values';
     AxGisPro.TargetDate:='WGS84';
     AxGisPro.TargetXOffset:=0;
     AxGisPro.TargetYOffset:=0;
     AxGisPro.TargetScale:=1;

     AxGisPro.Calculate(X,Y);

     Result:=0;
end;

initialization

finalization

if AxGisPro <> nil then begin
   AxGisPro.Free;
   AXGisPro:=nil;
end;

end.



