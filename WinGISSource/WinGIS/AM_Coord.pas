{****************************************************************************}
{ Unit AM_Coord                                                              }
{----------------------------------------------------------------------------}
{ Dialoge zur Koordinateneingabe                                             }
{----------------------------------------------------------------------------}
{ Modifikationen                                                             }
{  16.02.1994 Martin Forst                                                   }
{  06.04.1994 Martin Forst   Koordinatenumschaltung                          }
{****************************************************************************}
Unit AM_Coord;

Interface

Uses WinProcs,WinTypes,AM_Def,SysUtils,Classes,ResDlg,CoordinateSystem;

const     PresetBit         = $80000000;

Type PCoordData    = ^TCoordData;
     TCoordData    = Record
                       XPos        : LongInt;
                       YPos        : LongInt;
                       Line3       : LongInt;
                       Line4       : LongInt;
{++ Moskaliov BUG#90 BUILD#150 05.01.01}
                       Line5       : LongInt;
{-- Moskaliov BUG#90 BUILD#150 05.01.01}
                       Lines       : Integer;
                       Text1       : Integer;
                       Text2       : Integer;
                       Text3       : Integer;
                       Text4       : Integer;
{++ Moskaliov BUG#90 BUILD#150 05.01.01}
                       Text5       : Integer;
{-- Moskaliov BUG#90 BUILD#150 05.01.01}
                       NeedPos     : Boolean;
                       NeedLine3   : Boolean;
                       CoordSystem : Integer; 
{++ Sygsky 29-SEP-1999 BUG#186 BUILD#93: Insertion }
                       {ValuesPredefined:array [0..3] of Integer;}
   {++ Moskaliov BUG#90 BUILD#150 05.01.01}
                       {ValuesPredefined:array [0..3] of Double;}
                       ValuesPredefined:array [0..4] of Double;
   {-- Moskaliov BUG#90 BUILD#150 05.01.01}
                        {+++ Brovak BUG 499 BUILD 165}
                         AlternativeCaption:ansistring;
                        {--- Brovak 499}
                        PredefVal: Boolean;

{-- Sygsky }
                     end;

     PCoordInp     = ^TCoordInp;
     TCoordInp     = Class(TGrayDialog)
      Public
       More        : Boolean;
       Data        : PCoordData;
       Constructor Init(AParent:TComponent;AMore:Boolean;AData:PCoordData);
       Function    CanClose:Boolean; override;
       Function    GetValue(AID:Integer;ANeeded:Boolean;var AValue:LongInt):Boolean;
{++ Moskaliov BUG#90 BUILD#150 05.01.01}
       Function    GetDoubleValue(AID: Integer; ANonZero: Boolean; var AValue: Double): Boolean;
{-- Moskaliov BUG#90 BUILD#150 05.01.01}
       Procedure   SetupWindow; override;
     end;

Implementation

Uses UserIntf;

Const id_XPos      = 101;
      id_YPos      = 100;
      id_Line3     = 102;
      id_Line4     = 105;
      id_Text      = 103;
      id_Text4     = 104;
{++ Moskaliov BUG#90 BUILD#150 05.01.01}
      id_Text5     = 107;
      id_Line5     = 106;
{-- Moskaliov BUG#90 BUILD#150 05.01.01}

      id_YCoordinate    = 500;                                       {0604F}
      id_XCoordinate    = 501;                                       {0604F}

Function TCoordInp.GetValue(AID: Integer; ANeeded: Boolean; var AValue: LongInt): Boolean;
var AText        : Array[0..255] of Char;
    BStr         : String;
begin
   GetValue:=FALSE;
   GetDlgItemText(Handle,AID,AText,SizeOf(AText));
   BStr:=StrPas(AText);
   if BStr='' then
      if ANeeded then
         MsgBox(Handle,210,mb_Ok or mb_IconExclamation,'')
      else
         begin
            AValue:=MaxLongInt;
            GetValue:=TRUE;
         end
   else
      if not GetFieldLong(AId,AValue) then
         begin
            MsgBox(Handle,210,mb_Ok or mb_IconExclamation,'');
            SetFocus(AID);
         end
      else
         GetValue:=TRUE;
end;

{++ Moskaliov BUG#90 BUILD#150 05.01.01}
Function TCoordInp.GetDoubleValue(AID: Integer; ANonZero: Boolean; var AValue: Double): Boolean;
begin
   Result:=GetFieldDouble(AId,AValue);
   if Result then
      if ANonZero then
         if AValue = 0 then
            Result:=FALSE;
   if not Result then
      begin
         MsgBox(Handle,210,mb_Ok or mb_IconExclamation,'');
         SetFocus(AID);
      end;
end;
{-- Moskaliov BUG#90 BUILD#150 05.01.01}

Constructor TCoordInp.Init(AParent: TComponent; AMore: Boolean; AData: PCoordData);
begin
   case AData^.Lines of
      1 : inherited Init(AParent,'CO1');
      2 : inherited Init(AParent,'CO2');
      3 : inherited Init(AParent,'CO3');
      4 : inherited Init(AParent,'CO4');
{++ Moskaliov BUG#90 BUILD#150 05.01.01}
      5 : inherited Init(AParent,'CO5');
{-- Moskaliov BUG#90 BUILD#150 05.01.01}
   end;
   More:=AMore;
   Data:=AData;
end;

Procedure TCoordInp.SetupWindow;
var   AText      : Array[0..50] of Char;
      I          : Integer;
      DataPreset : Boolean;
      Focused    : Boolean;
{++ Moskaliov BUG#90 BUILD#150 05.01.01}
//const EditId     : array[0..3] of integer = (id_XPos,id_YPos,id_Line3,id_Line4); {++ Sygsky 29-SEP-1999 BUG#186 BUILD#93: Insertion }
const EditId     : array[0..4] of integer = (id_XPos,id_YPos,id_Line3,id_Line4,id_Line5); {++ Moskaliov BUG#90 BUILD#150 05.01.01 }
{-- Moskaliov BUG#90 BUILD#150 05.01.01}
begin
   inherited SetupWindow;
   with Data^ do
      begin
{++ Sygsky 29-SEP-1999 BUG#186 BUILD#93: Insertion }
         DataPreset := (CoordSystem and PresetBit) <> 0; { This bit is the sign of }
         CoordSystem := CoordSystem and (not PresetBit); { preset data availability! }
{-- Sygsky }
         if Lines = 1 then
            SetDlgItemText(Handle,id_Text,GetLangPChar(Text3,AText,SizeOf(AText)));
         if Lines > 2 then
            SetDlgItemText(Handle,id_Text,GetLangPChar(Text3,AText,SizeOf(AText)));
         if Lines > 3 then
            SetDlgItemText(Handle,id_Text4,GetLangPChar(Text4,AText,SizeOf(AText)));
{++ Moskaliov BUG#90 BUILD#150 05.01.01}
         if Lines > 4 then
            SetDlgItemText(Handle,id_Text5,GetLangPChar(Text5,AText,SizeOf(AText)));
{-- Moskaliov BUG#90 BUILD#150 05.01.01}
         if Text1 = 0 then
            if CoordSystem=Integer(ctGeodatic) then
               begin
                  SetFieldText(id_YCoordinate,GetLangText(866));
                  SetFieldText(id_XCoordinate,GetLangText(865));
               end
            else
               begin
                  SetFieldText(id_YCoordinate,GetLangText(865));
                  SetFieldText(id_XCoordinate,GetLangText(866));
               end
         else
            begin
               SetDlgItemText(Handle,id_YCoordinate,GetLangPChar(Text1,AText,SizeOf(AText)));
               SetDlgItemText(Handle,id_XCoordinate,GetLangPChar(Text2,AText,SizeOf(AText)));
            end;
{++ Sygsky 29-SEP-1999 BUG#186 BUILD#93: Insertion }
         if DataPreset then
            begin
               Focused := False;
               for i := 0 to Lines-1 do
   {++ Moskaliov BUG#90 BUILD#150 05.01.01}
      {--          if ValuesPredefined[i] <> 0 then begin      }
                   begin
      {--             Str(ValuesPredefined[i]/100:0:2,AText);  }
                      if i <> 4 then
                         Str(ValuesPredefined[i]/100:0:2,AText)
                      else
                         Str(ValuesPredefined[i]:0:2,AText);
   {-- Moskaliov BUG#90 BUILD#150 05.01.01}
                      SetDlgItemText(Handle,EditId[i],AText);
                      if not Focused then
                         begin
                            SetFocus(GetItemHandle(EditId[i]));{ Preset 1st occured and focus it }
                            Focused := False;
                         end;
                   end;
            end;

            if PredefVal then begin
               Str(ValuesPredefined[2]/100:0:2,AText);
               SetDlgItemText(Handle,id_Line3,AText);
            end;

{-- Sygsky }
{++brovak BUG 499}
if AlternativeCaption<>''
 then
   SetWindowText(Handle,pchar(AlternativeCaption));
{--brovak}
      end;
end;

Function TCoordInp.CanClose: Boolean;
var LineData3    : LongInt;
    LineData4    : LongInt;
    LineData5    : LongInt;   //++ Moskaliov BUG#90 BUILD#150 05.01.01
    InpOK        : Boolean;
begin
   CanClose:=FALSE;
   InpOK:=FALSE;
   LineData3:=0;
{++ Moskaliov BUG#90 BUILD#150 05.01.01}
   LineData4:=0;
   LineData5:=0;
   with Data^ do
      begin
         case Lines of
            1 : InpOK:=GetValue(id_Line3,TRUE,LineData3);
            2 : InpOK:=GetValue(id_XPos,TRUE,XPos) and GetValue(id_YPos,TRUE,YPos);
            5 : { It's impossible to use next approch:
                InpOK:=GetValue(id_XPos,TRUE,XPos)and GetValue(id_YPos,TRUE,YPos) and
                       GetValue(id_Line3,TRUE,LineData3) and GetValue(id_Line4,TRUE,LineData4) and
                       GetValue(id_Line5,TRUE,LineData5);
                because WIDHT and HEIGTH of print frame multiplied by 100 often exceed MaxLongInt}
                InpOK:=GetDoubleValue(id_XPos,FALSE,ValuesPredefined[0]) and
                       GetDoubleValue(id_YPos,FALSE,ValuesPredefined[1]) and
                       GetDoubleValue(id_Line3,TRUE,ValuesPredefined[2]) and
                       GetDoubleValue(id_Line4,TRUE,ValuesPredefined[3]) and
                       GetDoubleValue(id_Line5,FALSE,ValuesPredefined[4]);
         else   if not NeedPos then
                   begin
                      if (GetFieldText(id_XPos)<>'') or (GetFieldText(id_YPos)<>'') then
                         InpOK:=GetValue(id_XPos,TRUE,XPos) and GetValue(id_YPos,TRUE,YPos)
                      else
                         begin
                            if (Lines=3) or (GetFieldText(id_Line3)<>'') then
                               begin
                                  if GetValue(id_Line3,TRUE,LineData3) then
                                     begin
                                        XPos:=MaxLongInt;
                                        YPos:=MaxLongInt;
                                        InpOK:=TRUE;
                                     end;
                               end
                            else
                               if GetValue(id_Line4,TRUE,LineData4) then
                                  begin
                                     XPos:=MaxLongInt;
                                     YPos:=MaxLongInt;
                                     LineData3:=MaxLongInt;
                                     InpOK:=TRUE;
                                  end;
                         end;
                   end
                else
                   InpOK:=GetValue(id_XPos,TRUE,XPos) and GetValue(id_YPos,TRUE,YPos) and
                          GetValue(id_Line3,NeedLine3,LineData3) and ((Lines<>4) or
                          GetValue(id_Line4,TRUE,LineData4));
         end;   // End of CASE-statement
{-- Moskaliov BUG#90 BUILD#150 05.01.01}
         if InpOK then
            begin
               Data^.XPos:=XPos;
               Data^.YPos:=YPos;
               Data^.Line3:=LineData3;
               Data^.Line4:=LineData4;
               Data^.Line5:=LineData5;   //++ Moskaliov BUG#90 BUILD#150 05.01.01
               if More then
                  begin
                     SendMessage(Parent.Handle,wm_ICoord,0,LongInt(Data));
                     SetDlgItemText(Handle,id_XPos,'');
                     SetDlgItemText(Handle,id_YPos,'');
                     SetDlgItemText(Handle,id_Line3,'');
                     SetFocus(GetItemHandle(id_XPos));
                  end;
               CanClose:=not More;
            end;
      end;
end;

end.
