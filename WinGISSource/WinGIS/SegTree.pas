Unit Segtree;

Interface

Uses Classes,ConTree,SLBase,vector;

Const Eps4     = 0.01;

Type TSegItem  = class;
     TDVertex  = class;
     TSegTree  = class;

     TSegTree = class(TCBaseTree)
      private
      public
       Radius  : double;
       Owner   : TObject;
       Abort   : Boolean;
       ENorm   : TObject;
       ENormn  : TObject;
       Constructor Create(ARadius:double;AOwner:TObject);
       Function  CheckAbove(p:TSLBaseItem;Vertex:TDVertex;SegItem:TSegITem;var SItem2:TSegItem;
                 var flag:integer;var q:TSLBaseItem;var Same:Boolean):Boolean;
       Function  CheckBelow(p:TSLBaseItem;Vertex:TDVertex;SegItem:TSegItem;var SItem2:TSegItem;
                 var flag:integer;var q:TSLBaseItem;var Same:Boolean):Boolean;
       Function  CheckAboveBelow(p:TSLBaseItem;Vertex:TDVertex;SegItem:TSegItem;var SItem1,SItem2:TSegItem;
                 var flag:integer;var q:TSLBaseItem;var Same:Boolean):Boolean;
       Function  Compare(Vertex:TDVertex;Item,Key:TObject):Boolean;
       Function  Comp(Vertex:TDVertex;Item1,Item2:TObject;var dzero:Boolean;var q:TSLBaseItem):Boolean;
       Function  Cross(p:TSLBaseItem;Vertex:TDVertex;SegItem1,SegItem2:TSegItem;
                 var flag:integer;var q:TSLBaseItem;var same:Boolean):Boolean;
       Procedure CalculateCrossing(Vertex:TDVertex;Item1,Item2:TObject);
       Function  OnCompareItems(key1,key2:Pointer):Integer; override;
       Function  Key1Of(Item:Pointer):Pointer; override;
       Function  Key2Of(Item:Pointer):Pointer; override;
       Function  ResultZero(p,k:TCTreeBaseItem):Boolean; override;
       Procedure ThreatB(p,k:TCTreeBaseItem); {TEdge}
{       Function  CheckAboveEqual(Vertex:TDVertex;Edge:TObject;var Above:TSegItem):Boolean;}
       Function  SearchItemAbove(Item:TSegItem):TSegItem;
       Procedure Save;override;
       Procedure Save2;
       Procedure TraceTree(var p:TCTreeBaseItem);
       Procedure TraceTree2(var p:TCTreeBaseItem);
       Destructor Destroy;override;
     end;

     TSegItem = class(TCTReeBaseItem)
      private
      public
       StartEdge  : TObject;
       Procedure Print; override;
     end;

     TDVertex = class
       X: Double;
       Y: Double;
     end;

implementation
uses Area,AM_Point,AM_Poly,Am_Proj,AM_Def;


Constructor TSegTree.Create
   (
    ARadius : Double;
    AOwner  : TObject
   );
  begin
    inherited Create;
    Radius := ARadius;
    Owner  := AOwner;
    Abort  := False;
    TEdge(Enorm)  :=TEdge.Create;
    TEdge(Enormn) :=TEdge.Create;
    TEdge(Enorm).next:=TEdge(ENormN);
    TEdge(EnormN).next:=TEdge(ENorm);
  end;

Destructor TSegTree.Destroy;
  begin
    ENorm.Free;
    Enormn.Free;
    inherited Destroy;
  end;

  {key2<Key1 : -1;}
  {key2>key1 :  1;}
  {Tree, key}
Function TSegTree.OnCompareItems
   (
   key1    : Pointer;     {Tree}
   key2    : Pointer      {InsertEdge ; Run}
   )
   :Integer;
  var ETree  : TEdge;
      EKey   : TEdge;
      vec    : Double;
  begin
    ETree:=TEdge(key1^);
    EKey :=TEdge(key2^);
 {   If abs(EKey.x-EKey.next.x)<Eps2 then raise ElistError.Create('vertical Edge!');}

    vec:=
      (ETree.next.x - ETree.x)*
      (EKey.y  - ETree.y)-
      (EKey.x  - ETree.x)*
      (ETree.next.y - ETree.y);

    if vec < -eps then Result:=1     {key2 > key1 below}
    else
    if vec > eps then  Result:=-1    {key2 < key1 above}
    else
    if abs(vec) < eps then begin
      vec:=
        (Etree.next.x - ETree.x)*         {StartPoint Key = StartPoint Tree}
        (EKey.next.y  - EKey.y)-
        (Ekey.next.x  - EKey.x)*
        (ETree.next.y - ETree.y);
      if vec < -eps then Result:= 1
      else
      if vec > eps then  Result:=-1
      else
      if abs(vec) < eps then Result:=0;
    end;
  end;


Function TSegTree.Key1Of {Tree}
   (
   Item:Pointer
   )
   : Pointer;
  begin
    Result:= @TSegItem(Item).StartEdge;
  end;

Function TSegTree.Key2Of {Key}
   (
   Item:Pointer
   )
   : Pointer;
  begin
    Result:= @TSegItem(Item).StartEdge;
  end;

Function TSegTree.CheckAbove
   (
   p            : TSLBaseItem;
   Vertex       : TDVertex;
   SegItem      : TSegItem;
   var SItem2   : TSegItem;
   var flag     : integer;
   var q        : TSLBaseItem;
   var same     : Boolean
   )
   :Boolean;
  var Above : TCTreeBaseItem;
  begin
    Result:=False;
    flag:=0;
    q:=NIL;
    if {SegItem<>Head}SegItem.prev<>NIL then begin
      Above:=SegItem.prev;
      Result:=Cross(p,Vertex,SegItem,TSegItem(Above),flag,q,same);
      SItem2:=TSegItem(Above);
    end;
  end;

Function TSegTree.CheckBelow
   (
   p            : TSLBaseItem;
   Vertex       : TDVertex;
   SegItem      : TSegItem;
   var SItem2   : TSegItem;
   var flag     : integer;
   var q        : TSLBaseItem;
   var same     : Boolean
   )
   :Boolean;
  var Below : TCTreeBaseItem;
  begin
    Result:=False;
    flag:=0;
    q:=NIL;
    if {SegItem<>Tail}SegItem.next<>NIL then begin
      Below:=SegItem.next;
      Result:=Cross(p,Vertex,SegItem,TSegItem(Below),flag,q,same);
      SItem2:=TSegItem(Below);
    end;
  end;

Function TSegTree.CheckAboveBelow
   (
   p          :TSLBaseItem;
   Vertex     :TDVertex;
   SegItem    :TSegItem;
   var SItem1 :TSegItem;
   var SItem2 :TSegItem;
   var flag   :integer;
   var q      :TSLBAseItem;
   var same   :Boolean
   )
   :Boolean;
  var Below    : TCTreeBaseItem;
      Above    : TCTreeBaseItem;
      insert1  : TSegItem;
      insert2  : TSegItem;
  begin
    Result:=False;
    flag:=0;
    q:=NIL;

    Below:=NIL;
    Above:=NIL;
    Insert1:=SItem1;
    Insert2:=SItem2;

    if {SegItem<>Head} SegItem.prev<>NIL then Above:=SegItem.prev;
    if {SegItem<>Tail} SegItem.next<>NIL then Below:=SegItem.next;
    if (Below<>NIL) and (Above<>NIL) then begin
      SItem1:=TSegItem(Below);
      SItem2:=TSegITem(Above);
      Result:=Cross(p,Vertex,SItem1,SItem2,flag,q,same);
    end;
  end;

Function TSegTree.Cross
   (
   p         : TSLBaseItem;
   Vertex    : TDVertex ;
   SegItem1  : TSegItem;  {key}
   SegItem2  : TSegItem;   {tree}
   var flag  : integer;
   var q     : TSLBaseItem;
   var same  : Boolean
   )
   :Boolean;
  var cross1 : Double;
      cross2 : Double;
      cross1n: Double;
      cross2n: Double;
      Item1  : TEdge;
      Item2  : TEdge;
      c1n    : Integer;
      c1     : Integer;
      c2     : Integer;
      c2n    : Integer;
      found  : Boolean;
      dzero  : Boolean;
  begin
    Result:=False;
    found :=False;
    Item1:=TEdge(SegItem1.StartEdge); {key}
    Item2:=TEdge(SegItem2.StartEdge);

    Cross1n:=
      (Item1.next.x - Item1.x)*
      (Item2.next.y - Item1.y)-
      (Item2.next.x - Item1.x)*
      (Item1.next.y - Item1.y);
    if Cross1n < -Eps then c1n:= -1 else
    if Cross1n >  Eps then c1n:= 1 else
    c1n:=0;

    Cross1:=
      (Item1.next.x - Item1.x)*
      (Item2.y - Item1.y)-
      (Item2.x - Item1.x)*
      (Item1.next.y - Item1.y);
    if Cross1 < -Eps then c1:= -1 else
    if Cross1 >  Eps then c1:= 1 else
    c1:=0;

    Cross2n:=
      (Item2.next.x - Item2.x)*
      (Item1.next.y - Item2.y)-
      (Item1.next.x - Item2.x)*
      (Item2.next.y - Item2.y);
    if Cross2n < -Eps then c2n:= -1 else
    if Cross2n >  Eps then c2n:= 1 else
    c2n:=0;

    Cross2:=
      (Item2.next.x - Item2.x)*
      (Item1.y - Item2.y)-
      (Item1.x - Item2.x)*
      (Item2.next.y - Item2.y);
    if Cross2 < -Eps then c2:= -1 else
    if Cross2 >  Eps then c2:= 1 else
    c2:=0;

    if (c1=-c1n)and(c2=-c2n)then begin
      CalculateCrossing(Vertex,Item1,Item2);
      Result:=TRUE;
    end else begin

      Result:=Comp(Vertex,Item2,Item1.next,dzero,q);
      if Result then begin
        flag:=2;
        {if not dzero then inc(flag,10);}
      end else begin
        Result:=Comp(Vertex,Item1,Item2.next,dzero,q);
        if Result then begin
          flag:=1;
          {if not dzero then inc(flag,10);}
        end else begin
          Result:=Comp(Vertex,Item2,Item1,dzero,q);
          if Result then begin
            flag:=2;
            if dzero then Same:=TRUE;
           { if not dzero then inc(flag,10);}
          end;
        end;
      end;

    end;
  end;

              (*
Function TSegTree.CheckAboveEqual
   (
   Vertex         : TDVertex;
   Edge           : TObject;
   var Above      : TSegItem
   )
   :Boolean;
  var SItem : TSegItem;
      dzero : Boolean;
      q     :
  begin
    Result:=False;
    SItem:=TSegItem.Create;
    SItem.StartEdge:=Edge;
    TEdge(Edge).SegItem:=SItem;
    TEdge(Edge).next.SegItem:=SItem;
    Above:=SearchItemAbove(SItem);
    if Above <>NIL then
      Result:=Comp(Vertex,TEdge(Above.StartEdge),TEdge(Edge),dzero,q);
      {if not Result then Result:=Comp(Vertex,TEdge(Edge),TEdge(Above.StartEdge).next);}
    SItem.Free;
  end;
                *)
Function TSegTree.SearchItemAbove
  (
  Item:TSegITem
  )
  :TSegItem;
 var Compare  : integer;
     h        : TSegItem;
     p        : TCTreeBaseItem;
     prev     : TCTreeBaseItem;
 begin
   p:=FRoot;
   h:=TSegItem(SeekItem(p,Item));
   if h<>NIL then begin
     Compare:=OnCompareItems(Key1Of(h),Key2Of(Item));
     if Compare <=0 then begin
       prev:=TCTreeBaseItem(h).prev;
       Result:=TSegItem(prev);
     end else begin
       Result:=h;
     end;
   end else Result:=NIL;
 end;

Function TSegTree.Comp
   (
   Vertex    : TDVertex;
   Item1     : TObject;      {Tree}
   Item2     : TObject;       {key}
   var dzero : Boolean;
   var q     : TSLBaseItem
   )
   :Boolean;
  var EItem1   : TEdge;
      EItem2   : TEdge;
      vec1     : vec2;
      nvec     : vec2;
      kvec     : vec2;
      distance : double;
      nveca    : vec2;
      nvecb    : vec2;
      kvec1    : vec2;
      kvec2    : vec2;
      d1       : double;
      d2       : double;

  begin
    Result:=false;
    EItem1:=TEdge(Item1);
    EItem2:=TEdge(Item2);
    if ((abs(EItem2.x-EItem1.x)>Radius)or(abs(EItem2.y-EItem1.y)>Radius))and
       ((abs(EItem2.x-EItem1.next.x)>Radius)or(abs(EItem2.y-EItem1.next.y)>Radius)) then begin
      vec1.x:=EItem1.next.x - EItem1.x;
      vec1.y:=EItem1.next.y - EItem1.y;
      nvec:=Normvecup(vec1);
      kvec.x:=EItem2.x-EItem1.x;
      kvec.y:=EItem2.y-EITem1.y;
      distance:=InProd(kvec,nvec);
      if abs(distance)< Radius then begin
        nveca:=Norm(vec1);
        nvecb:=InvertVec(nveca);
        kvec1.x:=EItem2.x-EItem1.x;
        kvec1.y:=EItem2.y-EItem1.y;
        kvec2.x:=EItem2.x-EItem1.next.x;
        kvec2.y:=EItem2.y-EItem1.next.y;
        d1:=InProd(kvec1,nveca);
        d2:=InProd(kvec2,nvecb);
        if(d1>Radius)and(d2>Radius)then begin
          {Inside, Schnitt auf Item1,Item1.next}
          if distance = 0 then begin
            Vertex.x:=EItem2.x;
            Vertex.y:=EItem2.y;
            q:=TSLBaseITem(EItem2.back);
            dzero:=True;
          end else
          if abs(distance)>0 then begin
            dzero:=false;
            Tedge(Enorm).x:=EItem2.x;
            TEdge(Enorm).y:=EItem2.y;
            TEdge(ENormn).x:=EItem2.x+nvec.x;
            TEdge(Enormn).y:=EItem2.y+nvec.y;
            CalculateCrossing(Vertex,EItem1,TEdge(ENorm));
          end;
          Result:=TRUE;
        end;
      end;
    end;
  end;

Function TSegTree.Compare
   (
   Vertex   : TDVertex;
   Item     : TObject;
   Key      : TObject
   )
   :Boolean;
  begin
    {Einfuegen wenn Key <> Item.x,y bzw. Item.next.x,y sonst False retour}
    Result:=false;
    if (not(abs(TEdge(Item).x-TEdge(Key).x)<Eps))and
       (not(abs(TEdge(Item).y-TEdge(Key).y)<Eps))and
       (not(abs(TEdge(Item).next.x-TEdge(Key).x)<Eps))and
       (not(abs(TEDge(Item).next.y-TEdge(Key).y)<Eps)) then begin
         Result:=TRUE;
         Vertex.x:=TEdge(Key).x;
         Vertex.y:=TEdge(Key).y;
       end;
  end;

Procedure TSegTree.CalculateCrossing
   (
   Vertex      : TDVertex;
   Item1       : TObject;
   Item2       : TObject
   );
  var  Itemab  : TEdge;
       Itemcd  : TEdge;
       v       : Double;
  begin
    Itemab:=TEdge(Item1);
    Itemcd:=TEdge(Item2);
    v:=((Itemcd.x-Itemab.x)*(Itemab.next.y-Itemab.y)-(Itemab.next.x-Itemab.x)*(Itemcd.y-Itemab.y))/
       ((Itemcd.next.y-Itemcd.y)*(Itemab.next.x-Itemab.x)-(Itemcd.next.x-Itemcd.x)*(Itemab.next.y-Itemab.y));
    Vertex.x:=Itemcd.x+(Itemcd.next.x-Itemcd.x)*v;
    Vertex.y:=Itemcd.y+(Itemcd.next.y-Itemcd.y)*v;
  end;

Procedure TSegItem.Print;
  var f : TExtFile;
      e : TEdge;
  begin
  end;

Procedure TSegTree.TraceTree
   (
    var p   : TCTReeBaseItem
   );
  var e      : Tedge;
      SItem,d  : TSegItem;
      a,b,c,l,r  : TSegItem;
  begin
    if p<>NIL then begin
      TraceTree(p.left);
      SItem:=TSegItem(p);
      a:=TSegITem(p.prev);
      b:=TSegITem(p.next);
      d:=TSegItem(p.father);
      r:=TSegItem(p.right);
      l:=TSegItem(p.left);
      e:=TEdge(SItem.StartEdge);
    {  writeln('   self: ',longint(p),' f: ',longint(p.father),' prev: ',longint(p.prev),' next: ',longint(p.next),
              ' l: ',longint(p.left),' r: ',longint(p.right) );
     } writeln('rEdge: ',e.x:5:2,' ',e.next.x:5:2,' ');
     {
      try
      if a<>NIL then write('  prev: ',TEdge(a.StartEdge).x:5:2);
      if b<>NIL then writeln('  next: ',TEdge(b.StartEdge).x:5:2);
 {     if l<>NIL then write('  left: ',TEdge(l.StartEdge).x:5:2) else write(' left: NIL');
      if r<>NIL then write('  right: ',TEdge(r.StartEdge).x:5:2) else write(' right: NIL');
     { if d<>NIL then writeln('  father: ', TEdge(d.StartEdge).x:5:2,
      ' ', TEdge(d.StartEdge).next.x:5:2)else writeln(' NIL');}
      {except  writeln('Miss Hit Save');
      end;}
      TraceTree(p.right);
    end;
  end;
      
Procedure TSegTree.TraceTree2
   (
    var p   : TCTReeBaseItem
   );
  var Edge      : Tedge;
      SItem     : TSegItem;
      APoint    : PDPoint;
      AObject   : PPoly;
  begin
    if p<>NIL then begin
      TraceTree2(p.left);

      SItem:=TSegItem(p);
      Edge:=TEdge(SItem.StartEdge);

      APoint:=new(PDPoint);
      AObject:=new(PPoly,Init);
      APoint^.Init(round(Edge.x),round(Edge.y));
      AObject^.InsertPoint(APOint^);
      APoint^.Init(round(Edge.next.x),round(Edge.next.y));
      AObject^.InsertPoint(APoint^);
      AObject^.CalculateClipRect;
      PProj(TArea(Owner).Proj)^.InsertObjectonLayer(AObject,TArea(Owner).Data.DestLayer);
      Dispose(APoint,Done);
      TraceTree2(p.right);
    end;
  end;

Procedure TSegTree.Save;
  var p:TCTreeBaseItem;
      f:TextFile;
  begin
  end;

Procedure TSegTree.Save2;               
  var p:TCTreeBaseItem;
  begin
    TArea(Owner).DeleteLayer(TArea(Owner).Data.DestLayer);
    {TArea(Owner).DrawLines3;}
    p:=Root;
    TraceTree2(p);
  end;


Function TSegTree.ResultZero
   (
   p           : TCTreeBaseITem;
   k           : TCTreeBaseItem
   )
   :Boolean;
  var Etree    : TEdge;
      key      : TEdge;
      Edge1    : TEdge;
      Edge2    : TEdge;
      New1     : TEdge;
      New2     : TEdge;
      Link     : TLink;
      Link2    : TLink;
      help     : TEdge;


  begin
    Result:=False;
    key :=TEdge(TSegItem(k).StartEdge);
    Etree:=TEdge(TSegItem(p).StartEdge);

    if (Etree.x = key.x)and(Etree.y=key.y) then begin
      if (Etree.next.x > key.next.x)or(Etree.next.y > key.next.y) then begin
        {A1}
        raise EListError.Create('A1 not implemented');
        Result:=TRUE;
        New1:=TEdge.Create;
        New1.x:=key.next.x;
        New1.y:=key.next.y;
        New1.next:=Etree.next;

      end else
      if (Etree.next.x < key.next.x)or(Etree.next.y < key.next.y)then begin
        {A2}
        Result:=TRUE;
        raise ElistError.Create('A2 not implemented');
      end;
    end else
    if (Etree.x < key.x)or(Etree.y < key.y) then begin
      if (Etree.next.x > key.next.x)or(Etree.next.y > key.next.y) then begin
        {A3}
        Result:=TRUE;
        New1:=TEdge.Create;
        New2:=TEdge.Create;
        Edge1:=key.next;
        Edge2:=Etree.next;
        New1.x:=key.x;
        New1.y:=key.y;
        New2.x:=key.next.x;
        New2.y:=key.next.y;
        Etree.next:=New1;
        New1.next:=Etree;
        New2.next:=Edge2;
        Edge2.next:=New2;

        TEdge(TSegITem(p).StartEdge):=key;
        key.SegItem:=TSegItem(p);
        key.next.SegItem:=TSegItem(p);

        Link:=Key.Back;
        Link.InsertBackEdge(TArea(Owner),New1,NIL,NIL);
        Link:=key.next.Back;
        Link.InsertFrontEdge(TArea(Owner),New2,NIL,NIL);
      end else
      if (ETree.next.x = key.next.x) and (Etree.next.y = key.next.y) then begin
      {A4}
        //raise ElistError.Create('not implemented');
        Result:=False;
      end else
      if (Etree.next.x < key.next.x) or (Etree.next.y < key.next.y) then begin
        {A5}
        Result:=TRUE;
        New1:=TEdge.Create;
        New2:=TEdge.Create;
        New1.x:=key.x;
        New1.y:=key.y;
        New2.x:=Etree.next.x;
        New2.y:=Etree.next.y;

        Edge1:=Etree.next;
        Edge2:=key.next;

        Etree.next:=New1;
        New1.next:=Etree;
        key.next:=Edge1;
        Edge1.next:=key;
        New2.next:=Edge2;
        Edge2.next:=New2;

        TEdge(TSegITem(p).StartEdge):=key;
        key.SegItem:=TSegItem(p);
        key.next.SegItem:=TSegItem(p);

        Link:=Key.Back;
        Link.InsertBackEdge(TArea(Owner),New1,NIL,NIL);
        Link:=Key.next.Back;
        Link.InsertFrontEdge(TArea(Owner),New2,NIL,NIL);
      end;
    end;{ else
    if (Etree.next.x = key.x)and(Etree.next.y = key.y) then begin
      raise EListError.Create('not allowed here!');
      help:=TEdge(TSegItem(p).StartEdge);
      help.SegItem:=NIL;
      help.next.SegItem:=NIL;
      TEdge(TSegITem(p).StartEdge):=key;
      key.SegItem:=TSegItem(p);
      key.next.SegItem:=TSegItem(p);
    end; }
  end;


Procedure TSegTree.ThreatB
   (
   p      : TCTreeBAseItem;
   k      : TCTreeBaseITem
   );
  var tree   : TEdge;
      key    : TEdge;
  begin
    {raise ElistError.Create('not implemented');}
  end;

end.


(*
  begin
    Result:=False;
    key :=TEdge(TSegItem(k).StartEdge);
    Etree:=TEdge(TSegItem(p).StartEdge);

    if (Etree.next.x <> key.x)and(Etree.next.y <> key.y) then begin
      if (Etree.next.x > key.next.x)or(Etree.next.y > key.next.y) then begin
        {A3}
       { raise ElistError.Create('darf nich vorkommen 3');}
        Result:=TRUE;
        New1:=TEdge.Create;
        New2:=TEdge.Create;
        Edge1:=key.next;
        Edge2:=Etree.next;
        New1.x:=key.x;
        New1.y:=key.y;
        New2.x:=key.next.x;
        New2.y:=key.next.y;
        Etree.next:=New1;
        New1.next:=Etree;
        New2.next:=Edge2;
        Edge2.next:=New2;

        TEdge(TSegITem(p).StartEdge):=key;
        key.SegItem:=TSegItem(p);
        key.next.SegItem:=TSegItem(p);

        Link:=Key.Back;
        Link.InsertBackEdge(TArea(Owner),New1);
        Link:=key.next.Back;
        Link.InsertFrontEdge(TArea(Owner),New2);
      end else
      if (ETree.next.x = key.next.x)or(Etree.next.y = key.next.y) then begin
      {A4}
        raise ElistError.Create('not implemented');
      end else
      if (Etree.next.x < key.next.x)or(Etree.next.y < key.next.y) then begin
        {A5}
  {      raise ElistError.Create('darf nich vorkommen 5');}
        Result:=TRUE;
        New1:=TEdge.Create;
        New2:=TEdge.Create;
        New1.x:=key.x;
        New1.y:=key.y;
        New2.x:=Etree.next.x;
        New2.y:=Etree.next.y;

        Edge1:=Etree.next;
        Edge2:=key.next;

        Etree.next:=New1;
        New1.next:=Etree;
        key.next:=Edge1;
        Edge1.next:=key;
        New2.next:=Edge2;
        Edge2.next:=New2;

        TEdge(TSegITem(p).StartEdge):=key;
        key.SegItem:=TSegItem(p);
        key.next.SegItem:=TSegItem(p);

        Link:=Key.Back;
        Link.InsertBackEdge(TArea(Owner),New1);
        Link:=Key.next.Back;
        Link.InsertFrontEdge(TArea(Owner),New2);
      end;
    end else
    if (Etree.next.x = key.x)and(Etree.next.y = key.y) then begin
      raise EListError.Create('not allowed here!');
      help:=TEdge(TSegItem(p).StartEdge);
      help.SegItem:=NIL;
      help.next.SegItem:=NIL;
      TEdge(TSegITem(p).StartEdge):=key;
      key.SegItem:=TSegItem(p);
      key.next.SegItem:=TSegItem(p);
    end;
  end;






    if (Etree.x = key.x)or(Etree.y=key.y) then begin
      if (Etree.next.x > key.next.x)or(Etree.next.y > key.next.y) then begin
        {A1}
        raise EListError.Create('A1 not implemented');
      end else
      if (Etree.next.x < key.next.x)or(Etree.next.y < key.next.y)then begin
        {A2}
        raise ElistError.Create('A2 not implemented');
      end;
    end else
    if (Etree.x < key.x)or(Etree.y < key.y) then begin
      if (Etree.next.x > key.next.x)or(Etree.next.y > key.next.y) then begin
        {A3}
*)
