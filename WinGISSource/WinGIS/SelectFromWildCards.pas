unit SelectFromWildCards;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, WCtrls, ExtCtrls, MultiLng;

type
  TWildCardsSelector = class(TForm)
    ListPanel: TPanel;
    ButtonPanel: TPanel;
    OkButton: TButton;
    ListBox: TListBox;
    DirPanel: TPanel;
    DirLabel: TLabel;
    CancelButton: TButton;
    MlgSection: TMlgSection;
    procedure ListBoxMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure SetHint(NewHint: AnsiString);
    procedure ListBoxClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  WildCardsSelector: TWildCardsSelector;

function NameFromWildCards(NameWithWilds: AnsiString; ExcludeName: AnsiString): AnsiString;
function WildCardExists(FileName: AnsiString): Boolean;

implementation
uses
  TrLib32, TABFiles;

{$R *.DFM}

//-----------------------------------------------------

function WildCardExists(FileName: AnsiString): Boolean;
begin
  Result := (Pos('*', FileName) + Pos('?', FileName)) > 0;
end;

//-----------------------------------------------------

function NameFromWildCards(NameWithWilds: AnsiString; ExcludeName: AnsiString): AnsiString;
var
  SearchRec: TSearchRec;
  I, ErrCode: Integer;
  Names: TStringList;
  PInfo: PBitMapInfo;
  Hnd: THandle;
  Name, Ext: AnsiString;
const
  faOrdinaryFile = faReadOnly + faArchive;
begin
  Result := NameWithWilds;
  if not WildCardExists(NameWithWilds) then // Ok, use original name
    Exit;
  Result := EmptyStr;
  Names := MakeImageList(NameWithWilds); // Now in list only images
  try
    for I := 0 to Names.Count - 1 do
    begin
      Name := Names.Strings[I];
      Ext := UpperCase(Copy(ExtractFileExt(Name), 2, MaxInt));
        // Please, use only standard extensions
        Hnd := TrLib.ImageOpenSolo(PChar(Name), PChar(Ext));
        if Hnd <> 0 then
        try
          PInfo := TrLib.ImageGetInfoAddr(Hnd);
          Names.Strings[I] := Format('%s: (%dx%dx%d)', [ExtractFileName(Name),
            PInfo.bmiHeader.biWidth,
              PInfo.bmiHeader.biHeight,
              PInfo.bmiHeader.biBitCount]);
        finally
          TrLib.ImageClose(Hnd);
        end;
    end;

    case Names.Count of
      0: ; // Nothig  to do
      1:
        Result := Name;
    else // More then one image detected, force user to select manually
      begin
        with TWildCardsSelector.Create(nil) do
        try
          DirLabel.Caption := MlgStringList[MlgSection.Section, 20] +
            ' "' + ExtractFilePath(NameWithWilds) + '"'#10#13 +
            MlgStringList[MlgSection.Section, 21] + ' "' +
            ExtractFileName(NameWithWilds) + '"';
          FindClose(SearchRec);
          ListBox.Items.Assign(Names);
          ListBox.ItemIndex := 0;
          ListBoxClick(ListBox);
          SetHint(ListBox.Items[0]);
          ErrCode := ShowModal;
          if ErrCode = mrOK then
          begin
            I := Pos(':', ListBox.Items[ListBox.ItemIndex]) - 1;
            Result := Copy(ListBox.Items[ListBox.ItemIndex], 1, I);
          end;
        finally
          Free;
        end;
      end; // of case else clause
    end; // of case
  finally
    Names.Free;
  end;
end;

//---------------------------------------------------------

procedure TWildCardsSelector.SetHint(NewHint: AnsiString);
begin
  ListBox.Hint := NewHint;
end;

//-----------------------------------------------------
// Change hint to the file name under cursor

procedure TWildCardsSelector.ListBoxMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var
  Index: Integer;
begin
  with ListBox do
  begin
    Index := {WildCardsSelector.ListBox.} ItemAtPos(Point(X, Y), True);
    if Index = -1 then
    begin
      SetHint(EmptyStr);
      Exit;
    end;
    SetHint(ListBox.Items[Index]);
  end;
end;

//---------------------------------------------------------

procedure TWildCardsSelector.ListBoxClick(Sender: TObject);
var
  I : Integer;
begin
  I := Pos(':', ListBox.Items[ListBox.ItemIndex]);
  if I > 0 then
    OkButton.Hint := Format(MlgStringList[MlgSection.Section, 22], [Copy(ListBox.Items[ListBox.ItemIndex], 1, I - 1)]);
end;

end.

