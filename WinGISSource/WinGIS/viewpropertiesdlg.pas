Unit ViewPropertiesDlg;

Interface

Uses Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,ComCtrls,
     Tabnotbk,WCtrls,StdCtrls,Validate,MultiLng,AM_Proj,AM_Sight,Objects,
     Spinbtn,ColumnList,GrTools,ImgList;

Type TViewPropertiesDialog = Class(TWForm)
       CancelBtn        : TButton;
       GeneralSheet     : TTabSheet;
       MlgSection       : TMlgSection;
       LayersSheet      : TTabSheet;                      
       NameVal          : TValidator;
       Notebook         : TPageControl;
       RangeSheet       : TTabSheet;
       OKBtn            : TButton;
       Group1           : TWGroupBox;           
       StartupCheck     : TCheckBox;
       SaveRangeCheck   : TCheckBox;
       SaveLayersCheck  : TCheckBox;
       NameEdit         : TEdit;
       LayerListbox     : TColumnListbox;
       XEdit            : TEdit;
       YEdit            : TEdit;
       WidthEdit        : TEdit;
       HeightEdit       : TEdit;
       RotationEdit     : TEdit;
       WLabel1          : TWLabel;
       WLabel2          : TWLabel;
       WLabel3          : TWLabel;
       WLabel4          : TWLabel;
       WLabel5          : TWLabel;
       WLabel6          : TWLabel;
       XSpin            : TSpinBtn;
       YSpin            : TSpinBtn;
       WidthSpin        : TSpinBtn;
       HeightSpin       : TSpinBtn;
       RotationSpin     : TSpinBtn;
       XPosVal          : TMeasureLookupValidator;
       YPosVal          : TMeasureLookupValidator;
       WidthVal         : TMeasureLookupValidator;
       HeightVal        : TMeasureLookupValidator;
       RotationVal      : TMeasureLookupValidator;
       WLabel7          : TWLabel;
       DescriptionEdit  : TEdit;
       Procedure   FormCloseQuery(Sender: TObject; var CanClose: Boolean);
       Procedure   FormDestroy(Sender: TObject);
       Procedure   FormHide(Sender: TObject);
       Procedure   FormShow(Sender: TObject);
       Procedure   NameEditExit(Sender: TObject);
       Procedure   SaveRangeCheckClick(Sender: TObject);
       Procedure   LayerListboxColumns0Paint(Control: TWinControl;
                       Index: Integer; Rect: TRect; State: TOwnerDrawState);
       Procedure   LayerListboxColumns1Paint(Control: TWinControl;
                       Index: Integer; Rect: TRect; State: TOwnerDrawState);
       Procedure   LayerListboxColumns2Paint(Control: TWinControl;
                       Index: Integer; Rect: TRect; State: TOwnerDrawState);
       Procedure   SaveLayersCheckClick(Sender: TObject);
       Procedure   LayerListboxColumns3Paint(Control: TWinControl;
                       Index: Integer; Rect: TRect; State: TOwnerDrawState);
      Private
       FDefault    : Boolean;
       FModified   : Boolean;
       FNameChanged: Boolean;
       FProject    : PProj;
       FSight      : PSight;
       FLayers     : PCollection;
       FViewRect   : TRotRect;
       Procedure   UpdateControls;
      Public
       Property    DefaultView:Boolean read FDefault write FDefault;
       Property    Modified:Boolean read FModified;
       Property    NameChanged:Boolean read FNameChanged;
       Property    Project:PProj read FProject write FProject;
       Property    Sight:PSight read FSight write FSight;
     end;

Implementation

{$R *.DFM}

Uses AM_Def,AM_Layer,CommonResources,Measures,NumTools;

Procedure TViewPropertiesDialog.FormShow(Sender: TObject);
var Cnt            : Integer;
    Item           : PSightLData;
    Layer          : PLayer;
begin
  Notebook.ActivePage:=GeneralSheet;
  FViewRect:=FSight^.ViewRect;
  FLayers:=MakeObjectCopy(FSight^.LayersData);
  if FLayers=NIL then FLayers:=New(PCollection,Init(16,16));
  NameVal.AsText:=PToStr(FSight^.Name);
  DescriptionEdit.Text:=FSight^.Description;
  Caption:=Format(MlgSection[1],[NameVal.AsText]);
  SaveRangeCheck.Checked:=not RectEmpty(FSight^.ViewRect);
  SaveLayersCheck.Checked:=(FLayers<>NIL) and (FLayers^.Count<>0);
  StartupCheck.Checked:=FDefault;
  // fill layer-listbox
  for Cnt:=FLayers.Count-1 downto 0 do begin
    Item:=FLayers^.At(Cnt);
    Layer:=Project^.Layers^.IndexToLayer(Item^.LIndex);
    if (Layer<>NIL) and (Layer^.Index<>0) then LayerListbox.Items.Insert(0,
        PToStr(Layer^.Text));
  end;
  // setup range-settings
  XPosVal[muMeters]:=FViewRect.X/100;
  YPosVal[muMeters]:=FViewRect.Y/100;
  WidthVal[muMeters]:=FViewRect.Width/100;
  HeightVal[muMeters]:=FViewRect.Height/100;
  RotationVal[muRad]:=-FViewRect.Rotation;
  UpdateControls;
end;

Procedure TViewPropertiesDialog.FormDestroy(Sender: TObject);
begin
  if FLayers<>NIL then Dispose(FLayers,Done);
end;

Procedure TViewPropertiesDialog.FormHide(Sender: TObject);
begin
  if ModalResult=mrOK then begin

    if FProject^.Sights^.NamedSight(NameVal.AsText) <> nil then begin
       //exit;
    end;

    FNameChanged:=AnsiCompareText(PToStr(FSight^.Name),NameVal.AsText)<>0;
    if FNameChanged then AssignStr(FSight^.Name,NameVal.AsText);
    FModified:=FNameChanged;
    FSight^.Description:=DescriptionEdit.Text;
    NumTools.Update(FDefault,StartupCheck.Checked,FModified);
    FModified:=FModified or (SaveRangeCheck.Checked XOr RectEmpty(FSight^.ViewRect));
    if not SaveRangeCheck.Checked then FSight^.ViewRect:=RotRect(0,0,0,0,0)
    else FSight^.ViewRect:=FViewRect; 
    if FSight^.LayersData<>NIL then Dispose(FSight^.LayersData,Done);
    if not SaveLayersCheck.Checked or (FLayers^.Count=0) then FSight^.LayersData:=NIL
    else begin
      FSight^.LayersData:=FLayers;
      FLayers:=NIL;
    end;
    // setup range-settings
    FViewRect.X:=XPosVal[muMeters]*100;
    FViewRect.Y:=YPosVal[muMeters]*100;
    FViewRect.Width:=WidthVal[muMeters]*100;
    FViewRect.Height:=HeightVal[muMeters]*100;
    FViewRect.Rotation:=-RotationVal[muRad];
    if SaveRangeCheck.Checked then begin
       FSight^.ViewRect:=FViewRect;
    end;
  end;
end;

Procedure TViewPropertiesDialog.NameEditExit(Sender: TObject);
begin
  Caption:=Format(MlgSection[1],[NameVal.AsText]);
end;

Procedure TViewPropertiesDialog.UpdateControls;
begin
  RangeSheet.TabVisible:=SaveRangeCheck.Checked;
  LayersSheet.TabVisible:=SaveLayersCheck.Checked;
end;

Procedure TViewPropertiesDialog.SaveRangeCheckClick(Sender: TObject);
begin
  UpdateControls;
end;

Procedure TViewPropertiesDialog.LayerListboxColumns0Paint(Control: TWinControl;
    Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  CommonListBitmaps.Draw(LayerListbox.Canvas,CenterWidthInRect(16,Rect),Rect.Top,clbLayerGray);
end;

Procedure TViewPropertiesDialog.LayerListboxColumns1Paint(Control: TWinControl;
    Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  with LayerListbox do TextRectEx(Canvas,Rect,Items[Index],Color,State);
end;

Procedure TViewPropertiesDialog.LayerListboxColumns2Paint(Control: TWinControl;
    Index: Integer; Rect: TRect; State: TOwnerDrawState);
var LayerData      : PSightLData;
begin
  with LayerListbox do begin
    LayerData:=FLayers^.At(Index+1);
    if LayerData.LFlags and lf_LayerOff=0 then CommonListBitmaps.Draw(Canvas,
        CenterWidthInRect(16,Rect),Rect.Top,clbVisible);
  end;
end;

procedure TViewPropertiesDialog.LayerListboxColumns3Paint(Control:TWinControl;
    Index:Integer;Rect:TRect;State:TOwnerDrawState);
var LayerData      : PSightLData;
begin
  with LayerListbox do begin
    LayerData:=FLayers^.At(Index+1);
    if LayerData.LFlags and lf_GenerOn <> 0 then begin
       CommonListBitmaps.Draw(Canvas,CenterWidthInRect(16,Rect),Rect.Top,clbGeneralised);
    end;
  end;
end;

Procedure TViewPropertiesDialog.SaveLayersCheckClick(Sender: TObject);
begin
  // set view to current view if the view has no view-rectangle
  if SaveLayersCheck.Checked and RectEmpty(FViewRect) then
      FViewRect:=FProject^.PInfo^.GetCurrentScreen;
  UpdateControls;
end;

Procedure TViewPropertiesDialog.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  CanClose:=CheckValidators(Self);
end;

end.
