unit DK_ReplaceDlg;

{
Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 14.01.2000

TReplaceDialog is intended for user selection of user_line_style/fill_pattern_style
when replacing deleted line_style/fill_pattern_style.
On bug#320.
}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DipGrid, StdCtrls, ExtCtrls,
  WCtrls, StyleDef, XFills, NumTools, MultiLng;

type
  mdModeType = (rpmLineStyle, rpmFillPattern);
//             (work_with_line_styles, work_with_pattern-fill_styles)
  TDKReplaceDialog = class(TForm)
    OkButton: TButton;
    DipList1: TDipList;
    Bevel1: TBevel;
    CancelButton: TButton;
    MlgSection1: TMlgSection;
    procedure DipList1DrawCell(Sender: TObject; Canvas: TCanvas; Index: Integer; Rect: TRect);
    procedure FormCreate(Sender: TObject);
    procedure OkButtonClick(Sender: TObject);
  private
    { Private declarations }
    iUserSelect: Integer;        // Number of user select
    rpmMode: mdModeType;         // Work with line style or fill-pattern style
    FFillStyle: TFillProperties;
    FXFillList: TStringList;     // Fill-pattern styles list. In Objects property of list is stored TCustomXFill.
    FStyle: TLineProperties;
    FXLineList: TStringList;     // Line styles list. In Objects property of list is stored TXLineStyle.
    FMlgSection: TMlgSection;
  public
    { Public declarations }
    Procedure SetPatternStyles(sReplacedPatternName: AnsiString; AFillStyle: TFillProperties; AXFillList: TStringList; AMlgSection: TMlgSection);
    Procedure SetLineStyles(sReplacedLineStyleName: AnsiString; AStyle: TLineProperties; AXLineList: TStringList; AMlgSection: TMlgSection);
    Function GetUserSelect: Integer;
  end;

implementation

USES
    LineDlg, FillDlg, CommonResources, Am_Def, XStyles, XLines;

Const
     SystemFillStyles = 8;
     SystemLineStyles = 5;

{$R *.DFM}

{The procedure loads work fill-pattern styles.}
Procedure TDKReplaceDialog.SetPatternStyles(sReplacedPatternName: AnsiString; AFillStyle: TFillProperties; AXFillList: TStringList; AMlgSection: TMlgSection);
Begin
     rpmMode := rpmFillPattern;
     FFillStyle := AFillStyle;
     FXFillList.Assign(AXFillList);
     DipList1.ItemIndex := 0;
     FMlgSection := AMlgSection;
     // Delete replaced fill-pattern style from list for displaying.
     FXFillList.Delete(FXFillList.IndexOf(sReplacedPatternName));
     DipList1.Count := SystemFillStyles + FXFillList.Count - 1;
End;

{The procedure loads work line styles.}
Procedure TDKReplaceDialog.SetLineStyles(sReplacedLineStyleName: AnsiString; AStyle: TLineProperties; AXLineList: TStringList; AMlgSection: TMlgSection);
Begin
     rpmMode := rpmLineStyle;
     FStyle := AStyle;
     FXLineList.Assign(AXLineList);
     DipList1.ItemIndex := 0;
     FMlgSection := AMlgSection;
     // Delete replaced line style from list for imaging.
     FXLineList.Delete(FXLineList.IndexOf(sReplacedLineStyleName));
     DipList1.Count := SystemLineStyles + FXLineList.Count;
End;

{The procedure draws line styles and fill-pattern styles in the same TDipList.}
procedure TDKReplaceDialog.DipList1DrawCell(Sender: TObject; Canvas: TCanvas; Index: Integer; Rect: TRect);
var
    AText: String;
    ARect: TRect;
    FillItem: TCustomXFill;
    LineStyleItem: TXLineStyle;
    Points: TRectCorners;
    Y: Integer;
    LinePoints: Array[0..1] of TPoint;
    AZoom: Double;
begin
     Case rpmMode Of
// Drawing fill-pattern styles /////////////////////////////////////////////////
     rpmFillPattern: Begin
          With Canvas Do Begin
               If Index<0 Then Begin
                  AText := FFillStyle.PatternOptions.LongNames[Abs(Index)-1];
                  TextRect(Rect,CenterWidthInRect(TextWidth(AText),Rect), CenterHeightInRect(TextHeight(AText),Rect),AText);
                  End
               Else Begin
                  FillRect(Rect);
                  ARect := Classes.Rect(Rect.Left+DipList1.RowHeight+3, Rect.Top,Rect.Left+80,Rect.Bottom);
                  If Index<SystemFillStyles-1 Then Begin
                     CommonListBitmaps.Draw(Canvas,Rect.Left+2,CenterHeightInRect(14,Rect),clbSystem);
                     Case Index+1 Of
                     pt_FDiagonal : Brush.Style:=bsFDiagonal;
                     pt_Cross     : Brush.Style:=bsCross;
                     pt_DiagCross : Brush.Style:=bsDiagCross;
                     pt_BDiagonal : Brush.Style:=bsBDiagonal;
                     pt_Horizontal: Brush.Style:=bsHorizontal;
                     pt_Vertical  : Brush.Style:=bsVertical;
                     pt_Solid     : Brush.Style:=bsSolid;
                     End; // Case index end
                     If NOT FFillStyle.UpdatesEnabled Then Brush.Color:=clBtnShadow
                     Else Brush.Color:=clBlack;
                     SetBKColor(Handle,ColorToRGB(clWindow));
                     SetBKMode(Handle,Opaque);
                     Pen.Style := psClear;
                     With ARect Do Rectangle(Left,Top+1,Right+1,Bottom);
                     AText := FMlgSection[30+Index];
                     End
                  Else Begin
                     CommonListBitmaps.Draw(Canvas,Rect.Left+2,CenterHeightInRect(14,Rect),clbUser);
                     FillItem := TCustomXFill(FXFillList.Objects[Index-SystemFillStyles+1]);
                     RectCorners(InflatedRect(ARect,0,-1),Points);
                     If FillItem IS TVectorXFill Then With FillItem AS TVectorXFill Do
                        XLFillPolygon(Handle,Points,4,StyleDef^.XFillDefs,StyleDef^.Count, 0,0,5/MinDistance,0)
                     Else If FillItem IS TBitmapXFill Then
                             With FillItem AS TBitmapXFill Do BMFillPolygon(Handle,Points,4,StyleDef(Handle)^.BitmapHandle,srcCopy,1);
                     AText := FillItem.Name;
                     End;
                  ARect := InflatedWidth(Rect,-85,0);
                  TextRect(ARect,ARect.Left,CenterHeightInRect(TextHeight(AText),ARect),AText);
                  End;
               End; // With canvas end
          End; // rpmFillPattern end
// Drawing line styles /////////////////////////////////////////////////////////
     rpmLineStyle: Begin
          With Canvas Do Begin
               If NOT FStyle.UpdatesEnabled Then Begin
                  Font.Color:=clBtnShadow;
                  Pen.Color:=clBtnShadow;
                  End;
               If Index<0 Then Begin
                  AText := FStyle.StyleOptions.ShortNames[Abs(Index)-1];
                  TextRect(Rect,CenterWidthInRect(TextWidth(AText),Rect), CenterHeightInRect(TextHeight(AText),Rect),AText);
                  End
               Else Begin
                  FillRect(Rect);
                  ARect := Classes.Rect(Rect.Left+DipList1.RowHeight+3, Rect.Top,Rect.Left+80,Rect.Bottom);
                  Y := CenterHeightInRect(0,Rect);
                  If Index<SystemLineStyles Then Begin
                     CommonListBitmaps.Draw(Canvas,Rect.Left+2,CenterHeightInRect(14,Rect),clbSystem);
                     Pen.Style := TPenStyle(Index);
                     Pen.Width:=0;
                     With ARect Do Polyline([Point(Left+1,Y-1),Point(Right-1,Y-1)]);
                     With ARect Do Polyline([Point(Left+1,Y),Point(Right-1,Y)]);
                     AText := FMlgSection[20+Index];
                     End
                  Else Begin
                     CommonListBitmaps.Draw(Canvas,Rect.Left+2,CenterHeightInRect(14,Rect),clbUser);
                     LineStyleItem := TXLineStyle(FXLineList.Objects[Index-SystemLineStyles]);
                     With ARect Do Begin
                          LinePoints[0]:=Point(Left+1,Y);
                          LinePoints[1]:=Point(Right-1,Y);
{++ Ivanoff BUG#New18 BUILD#106}
                          If RectHeight(LineStyleItem.ClipRect) = 0 Then
{++ New XLine BUG#251}
// Incorrect calculating of the scale.
//                             AZoom:=(DipList1.RowHeight-2)*5{AZoom:=1.7E10308}
                             AZoom := DipList1.CellWidth / LineStyleItem.Items[0].Cycle / 10
{-- New XLine BUG#251}
{-- Ivanoff}
                          Else AZoom := (DipList1.RowHeight-2)/RectHeight(LineStyleItem.ClipRect)/5;
                          XPolyline(Canvas.Handle,LinePoints,2,LineStyleItem.StyleDef^.XLineDefs, LineStyleItem.StyleDef^.Count,{ColorComboBtn.SelectedColor}clBlack, {FillColorComboBtn.SelectedColor} clBlack,True,AZoom);
                          End;
                     AText := LineStyleItem.Name;
                     End;
                  ARect := InflatedWidth(Rect,-85,0);
                  TextRect(ARect,ARect.Left,CenterHeightInRect(TextHeight(AText),ARect),AText);
                  End;
               End; // With canvas end
          End; // rpmLineStyle end
     End; // Case end
end;

procedure TDKReplaceDialog.FormCreate(Sender: TObject);
begin
     FXFillList := TStringList.Create;
     FXLineList := TStringList.Create;
     // Read captions of objects from wingis.lng
     Self.Caption := MlgStringList['ReplaceDialog', 1];
     OkButton.Caption := MlgStringList['ReplaceDialog', 2];
     CancelButton.Caption := MlgStringList['ReplaceDialog', 3];
end;

{The procedure returns index of list style or fill-pattern style which was selected by user.}
Function TDKReplaceDialog.GetUserSelect: Integer;
Begin
     RESULT := iUserSelect;
End;

{The procedure calculates user selection.}
procedure TDKReplaceDialog.OkButtonClick(Sender: TObject);
begin
     Case rpmMode Of
     rpmFillPattern: Begin
          If DipList1.ItemIndex < SystemFillStyles-1 Then iUserSelect := DipList1.ItemIndex + 1
          Else iUserSelect := TCustomXFill(FXFillList.Objects[DipList1.ItemIndex-SystemFillStyles+1]).Number;
          End;
     rpmLineStyle: Begin
          If DipList1.ItemIndex < SystemLineStyles Then iUserSelect := DipList1.ItemIndex
          Else iUserSelect := TXLineStyle(FXLineList.Objects[DipList1.ItemIndex-SystemLineStyles]).Number;
          End;
     End; // Case end
end;

end.
