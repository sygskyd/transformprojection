{****************************************************************************}
{ Unit                                                                       }
{----------------------------------------------------------------------------}
{                                                                            }
{----------------------------------------------------------------------------}
{ Modifikationen                                                             }
{  16.02.1994 Martin Forst                                                   }
{  06.04.1994 Martin Forst   TInputArc                                       }
// 08.11.2000 Yuri Glukhov   TInputBox was corrected
// 22.11.2000 Yuri Glukhov   TInputText was corrected for the multi-line text
// 26.03.2001 Yuri Glukhov   TNewPoly was corrected to control the drawing of a created object
// 15.06.2001 Yuri Glukhov   TEditTxtRect was created to rotate a text rectangle
{****************************************************************************}
Unit AM_Input;

Interface

{$O-}

Uses WinProcs,WinTypes,AM_Poly,AM_Paint,AM_Def,AM_Text,
     SysUtils,AM_Font,AM_CPoly,AM_Sym,AM_Splin,AM_Coll,Objects,AM_Circl,AM_View, AM_Index;

Const MinTextH     = 30;

      spFixRect    = 10;
      spPoints     = 7;

      ibRect       = 10;

      m_Default    = $0;   {TInputBox : Defaultmode, seitenverh�ltnisrichtiges       }
                           {            vergr��eren/verkleinern, Breite/L�nge �ndern }
      m_FixSize    = $1;   {TInputBox : Rechteckgr��e kann nicht ver�ndert werden}
      m_NoAspect   = $2;   {TInputBox : Gr��e kann in den Eckpunkten beliebig ge�ndert werden }
                           {            ( ohne R�cksicht auf Seitenverh�ltnis ) }
//++ Glukhov Bug#170 BUILD#125 03.11.00
// Defines that only corner points can be used to resize the box
// As well I changed mode m_FixSize: only one corner is marked now
      m_CornersOnly   = $4;
//-- Glukhov Bug#170 BUILD#125 03.11.00
//++ Glukhov Bug#264 Build#161 15.06.01
      m_Rotation      = $8;
//-- Glukhov Bug#264 Build#161 15.06.01

      R2_DEFPEN = R2_NOTXORPEN;

Type PNewPoly      = ^TNewPoly;
     TNewPoly      = Object(TCPoly)
       StartPos    : TDPoint;
       LastPos     : TDPoint;
       FirstPos    : TDPoint;
       LineOn      : Boolean;
       LastIns     : Boolean;   {letzter Punkt wurde eingef�gt}
       Capture     : Boolean;
       Pen         : HPen;
       AktDist     : Double;
       DrawReally  : Boolean;
{++ Ivanoff OrthoDraw}
       bDrawOrtho: Boolean;
{-- Ivanoff}
//++ Glukhov Bug#100 Build#152 26.03.01
// Set to True to forbid the drawing
       bNoDrawing  : Boolean;
//-- Glukhov Bug#100 Build#152 26.03.01
       Closed      : Boolean;
       DeleteLast  : Boolean;
       SavedCount  : Integer;
       SnapObject  : PIndex;
       DirFlag     : Boolean;
       LastSnapPos : TDPoint;
       Constructor Init(ACol,ALine:Word;ACapt:Boolean);
       Destructor  Done; virtual;
       Function    CheckCopyData(PInfo:PPaint;AItem:PPoly;DelLast:Boolean):Boolean;
       Procedure   CopyPoints(PInfo:PPaint;APoly:PPoly;AReverse:Boolean);
       Procedure   Draw(PInfo:PPaint;Clipping:Boolean); virtual;
       Procedure   DrawMarks(PInfo: PPaint);
       Procedure   EndIt(PInfo:PPaint);
       Function    GetDistance:Double;
       Function    GetLastDist(PInfo:PPaint):Double;
       Function    GetPoly(PInfo:PPaint;AItem:PPoly;DelLast:Boolean):Boolean;
       Function    GetXDist:LongInt;
       Function    GetYDist:LongInt;
       Function    PointInsert(PInfo:PPaint;Point:TDPoint;SnapIt:Boolean):Boolean;
{++ Ivanoff NA1 BUILD#106}
       Procedure   DeleteLastPoint(PInfo: PPaint);
{-- Ivanoff}
{++ Brovak BUG 104 BUILD 130}
       Function GetLastPoint(PInfo:PPaint):PDPoint;
{--Brovak}
{++ Ivanoff OrthoDraw}
       Procedure DrawLastLineAsOrtho(PInfo: PPaint);
{-- Ivanoff}
       Procedure   MouseMove(PInfo:PPaint;APos:TDPoint);
{++ Ivanoff BUG#378}
       Function GetAngle: Double;
{-- Ivanoff BUG#378}
       Procedure ShowAreaStatus(PInfo: PPaint);
       Procedure Save(AItem: PIndex);
       Procedure Reset(PInfo: PPaint);
       Procedure DrawAlong(PInfo: PPaint; APoly: PPoly);
       function GetLastOrthoDist(PInfo: PPaint): Double;
     end;

     PInput        = ^TInput;
     TInput        = Object(TOldObject)
       LastPos     : TDPoint;
       StartPos    : TDPoint;
       IsOn        : Boolean;
       Pen         : HPen;
       Capture     : Boolean;
       Constructor Init(ACol,ALine:Word);
       Destructor  Done; virtual;
       Procedure   Draw(PInfo:PPaint);
       Procedure   DrawIt(PInfo:PPaint); virtual;
       Procedure   EndIt(PInfo:PPaint); virtual;
       Function    IsItOn:Boolean;
       Procedure   MouseMove(PInfo:PPaint;APos:TDPoint); virtual;
       Procedure   StartIt(PInfo:PPaint;Pos:TDPoint;ACapt:Boolean); virtual;
     end;

     PInputRect    = ^TInputRect;
     TInputRect    = Object(TInput)
       Function    IsBigEnough(PInfo:PPaint;XSize,YSize:Integer;Both:Boolean):Boolean;
       Procedure   GetRect(var Rect:TDRect);
       Procedure   GetIsoDrawRect(PInfo:PPaint;var Rect:TDRect);
       Function    GetBorder(PInfo:PPaint):PCPoly;
     end;

     PInputCircle  = ^TInputCircle;
     TInputCircle  = Object(TInput)
                       Procedure   DrawIt(PInfo:PPaint); virtual;
                       Procedure   GetCircle(var Middle:TDPoint;var Radius:LongInt);
                       Function    GetRadius:LongInt;
                       Procedure   MoveMiddle(PInfo:PPaint;var AMiddle:TDPoint);    {MF}
                     end;

     PInputLine    = ^TInputLine;
     TInputLine    = Object(TInput)
                       Procedure   DrawIt(PInfo:PPaint); virtual;
                       Procedure   GetLine(var StartP,EndP:TDPoint);
                       Function    GetDistance:LongInt;
                       Procedure   MoveIt(PInfo:PPaint;var Start,EndP:TDPoint);
                     end;

     PInputLines   = ^TInputLines;
     TInputLines   = Object(TInput)
                       EndPos      : TDPoint;
                       HasEnd      : Boolean;
                       Procedure   DrawIt(PInfo:PPaint); virtual;
                       Procedure   GetLines(var ALast:TDPoint);
                       Procedure   SetEnd(PInfo:PPaint;AktPos:TDPoint;AHasEnd:Boolean;AEnd:TDPoint);
                       Procedure   StartIt(PInfo:PPaint;Pos:TDPoint;ACapt:Boolean); virtual;
                     end;

     PInputArc     = ^TInputArc;
     TInputArc     = Object(TInput)
                       DrawArc     : Boolean;
                       Radius      : Longint;
                       BeginArc    : TDPoint;

                       Constructor Init(ACol,ALine:word;ARadius:longint);
                       Procedure   DrawIt(PInfo : PPaint); virtual;
                       Function    GetAngle:Integer;
                       Procedure   SetDrawArc(PInfo : PPaint;Pos:TDPoint); virtual;
                     end;

     PInputText    = ^TInputText;
     TInputText    = Object(TInput)
                       Rotate      : Boolean;
                       FixHeight   : Boolean;
                       Angle       : Integer;
                       Height      : LongInt;
                       Width       : LongInt;
                       Point1      : TDPoint;
                       Point2      : TDPoint;
                       Point3      : TDPoint;
                       AspectRat   : Real;
                       Vertical    : Boolean; {mf}
                       Constructor Init(ACol,ALine:Word);
                       Destructor  Done; virtual;
                       Procedure   CalculateRectangle;
                       Procedure   CalculateSize;
                       Procedure   GetText(PInfo:PPaint;AItem:PText);
                       Procedure   DrawIt(PInfo:PPaint); virtual;
                       Function    GetDisplayAngle:Double;
                       Procedure   EndIt(PInfo:PPaint); virtual;
                       Procedure   InvertRotate;
                       Procedure   MouseMove(PInfo:PPaint;APos:TDPoint); virtual;
                       Procedure   StartIt(PInfo:PPaint;Pos:TDPoint;ACapt:Boolean); virtual;
                       Procedure   SetNewText(PInfo:PPaint;ActFont:PFontData;AText:String);
                       Procedure   SetText(PInfo:PPaint;AText:PText);
                     end;

     PInputSymbol  = ^TInputSymbol;
     TInputSymbol  = Object(TInput)
                       Symbol      : PSCPoly;
                       SymCopy     : PSymbol;
                       Scaling     : Double;
                       Width1      : LongInt;
                       Height1     : LongInt;
                       Width2      : LongInt;
                       Height2     : LongInt;
                       Rotate      : Boolean;
                       AktAngle    : Real;
                       StartAngle  : Real;
                       RotAngle    : Real;
                       Constructor Init(ACol,ALine:Word);
                       Destructor  Done; virtual;
                       Procedure   CalculateAngle;
                       Procedure   CalculateRect;
                       Procedure   DrawIt(PInfo:PPaint); virtual;
                       Procedure   EndIt(PInfo:PPaint); virtual;
                       Procedure   GetSymbol(PInfo:PPaint;ASymbol:PSymbol);
                       Procedure   InvertRotation(Pos:TDPoint);
                       Procedure   MouseMove(PInfo:PPaint;APos:TDPoint); virtual;
                       Procedure   SetSymbol(PInfo:PPaint;ASymbol:PSymbol);
                     end;

     PMovePoly     = ^TMovePoly;
     TMovePoly     = Object(TInput)
                       Poly        : PPoly;
                       Constructor Init(ACol,ALine:Word);
                       Destructor  Done; virtual;
                       Procedure   DrawIt(PInfo:PPaint); virtual;
                       Procedure   GetMove(var XMove,YMove:LongInt);
                       Function    IsEnoughToMove(PInfo:PPaint;XSize,YSize:Integer):Boolean;
                       Procedure   SetPoly(APoly:PPoly);
                     end;

     PMoveCircle   = ^TMoveCircle;
     TMoveCircle   = Object(TInput)
                       Circle      : PEllipse;
                       Constructor Init(ACol,ALine:Word);
                       Destructor  Done; virtual;
                       Procedure   DrawIt(PInfo:PPaint); virtual;
                       Procedure   GetMove(var XMove,YMove:LongInt);
                       Function    IsEnoughToMove(PInfo:PPaint;XSize,YSize:Integer):Boolean;
                       Procedure   SetCircle(ACircle:PEllipse);
                     end;

     PMoveMulti    = ^TMoveMulti;
     TMoveMulti    = Object(TInput)
                       MultiBord   : PCollection;
                       Constructor Init(ACol,ALine:Word);
                       Destructor  Done; virtual;
                       Procedure   Add(AItem:PView);
                       Procedure   DrawIt(PInfo:PPaint); virtual;
                       Procedure   GetMove(var XMove:LongInt;var YMove:LongInt);
                       Function    IsEnoughToMove(PInfo:PPaint;XSize,YSize:Integer):Boolean;
                       Procedure   SetMulti;
                     end;

     PInpOffset    = ^TInpOffset;
     TInpOffset    = Object(TInput)
                       Poly        : PPoly;
                       AktDist     : Double;
                       PoiNum      : Integer;
                       Constructor Init(ACol,ALine:Word);
                       Procedure   DrawIt(PInfo:PPaint); virtual;
                       Procedure   MouseMove(PInfo:PPaint;APos:TDPoint); virtual;
                       Procedure   SetPoly(APoly:PPoly);
                       Procedure   GetOffset(var Ofs:LongInt);
                     end;

     PInputSpline  = ^TInputSpline;
     TInputSpline  = Object(TInput)
                       Spline      : PSpline;
                       Pen2        : HPen;
                       CurSpline   : Integer;
                       MovePnt     : Integer;
                       IsFixPoint  : Boolean;
                       Constructor Init(ACol,ALine,BCol,BLine:Word);
                       Destructor  Done; virtual;
                       Procedure   DrawIt(PInfo:PPaint); virtual;
                       Procedure   DrawSpline(PInfo:PPaint;ASpline:Integer);
                       Procedure   DrawHandles(PInfo:PPaint);
                       Procedure   GetSpline(ASpline:PSpline);
                       Procedure   MouseMove(PInfo:PPaint;APos:TDPoint); virtual;
                       Procedure   MouseClick(PInfo:PPaint;DrawPos:TDPoint);
                       Procedure   MouseUp;
                       Procedure   SetSpline(ASpline:PSpline);
                     end;

     PInputBox     = ^TInputBox;
     TInputBox     = Object(TInput)
       Rect        : TDRect;
       Pen2        : HPen;
       MovePoint   : Integer;
       StartWidth  : LongInt;
       StartHeight : LongInt;
       Mode        : Integer;
       Constructor Init(ACol,ALine,BCol,BLine:Word;AMode:Integer);
       Destructor  Done; virtual;
       Procedure   DrawIt(PInfo:PPaint); virtual;
       Procedure   GetPoint(APoint:Integer;var BPoint:TDPoint);
       Procedure   GetRect(var ARect:TDRect);
       Procedure   MouseClick(PInfo:PPaint;DrawPos:TDPoint);
       Procedure   MouseMove(PInfo:PPaint;APos:TDPoint); virtual;
       Procedure   MouseUp(PInfo:PPaint);
       Procedure   SetRect(ARect:TDRect);
       Function    GetMode(Amode:Integer):Boolean;
     end;

//++ Glukhov Bug#264 Build#161 15.06.01
//---------
// Object TEditTxtRect was created to rotate a text rectangle
// (Object TInputBox allows to move and resize it)
//---------
    PEditTxtRect     = ^TEditTxtRect;
    TEditTxtRect     = Object(TInputBox)
       TxtPos     : TDPoint;  // Text position
       Point1     : TDPoint;  // The points to draw the rotated text rectangle
       Point2     : TDPoint;
       Point3     : TDPoint;
       RotPos     : TDPoint;  // Tag position
       Angle      : Integer;
       Height     : LongInt;
       Width      : LongInt;
       bRotate    : Boolean;
       bTxtRect   : Boolean;
       Constructor Init( ACol, ALine, BCol, BLine: Word; AMode: Integer );
       Destructor  Done; virtual;
       Procedure   CalcPoints;    // Calculates Point1-3
       Procedure   ReCalcRect;    // ReCalculates Rect
       Procedure   SetTxtRect( ARect: TDRect; APos: TDPoint; AAngle: Integer; AHeight, AWidth: LongInt );
       Procedure   DrawIt( PInfo: PPaint ); virtual;
       Procedure   MouseClick( PInfo: PPaint; DrawPos: TDPoint );
       Procedure   MouseMove( PInfo: PPaint; APos: TDPoint ); virtual;
       Procedure   MouseUp( PInfo: PPaint );
    {
       Procedure   GetRect(var ARect:TDRect);
       Procedure   SetRect(ARect:TDRect);
       Function    GetMode(Amode:Integer):Boolean;
    }
    end;
//-- Glukhov Bug#264 Build#161 15.06.01

     PInfoPoly     = ^TInfoPoly;
     TInfoPoly     = Object(TInput)
                       Poly        : PPoly;
                       Constructor Init(ACol,ALine:Word);
                       Destructor  Done; virtual;
                       Procedure   DrawIt(PInfo:PPaint); virtual;
                       Procedure   SetPoly(APoly:PPoly);
                     end;

Implementation

Uses AM_ProjM,Win32Def,GrTools,Clipping,NumTools,AM_Ini,AM_Proj;

Procedure DrawLine
   (
   PInfo           : PPaint;
   Point1          : TDPoint;
   Point2          : TDPoint
   );
  var ClipPoints   : TClipPointList;
      Cnt          : Integer;
      Point        : TGrPoint;
  begin
    ClipPoints:=TClipPointList.Create;
    PInfo^.ConvertToDispDouble(Point1,Point.X,Point.Y);
    ClipPoints.Add(Point);
    PInfo^.ConvertToDispDouble(Point2,Point.X,Point.Y);
    ClipPoints.Add(Point);
    ClipPolyline(ClipPoints,PInfo^.ClipRect);
    if ClipPoints.Count>0 then begin
      MoveTo(PInfo^.ExtCanvas.WindowDC,Round(ClipPoints[0].X),Round(ClipPoints[0].Y));
      for Cnt:=1 to ClipPoints.Count-1 do LineTo(PInfo^.ExtCanvas.WindowDC,
          Round(ClipPoints[Cnt].X),Round(ClipPoints[Cnt].Y));
    end;
    ClipPoints.Free;
  end;

Procedure DrawRectangle( PInfo: PPaint; Rect: TDRect );
  var ClipPoints   : TClipPointList;
      Cnt          : Integer;
      Point1       : TGrPoint;
      Point2       : TGrPoint;
  begin
    ClipPoints:=TClipPointList.Create;
    PInfo^.ConvertToDispDouble(Rect.A,Point1.X,Point1.Y);
    PInfo^.ConvertToDispDouble(Rect.B,Point2.X,Point2.Y);
    ClipPoints.Add(GrPoint(Point1.X,Point1.Y));
    ClipPoints.Add(GrPoint(Point2.X,Point1.Y));
    ClipPoints.Add(GrPoint(Point2.X,Point2.Y));
    ClipPoints.Add(GrPoint(Point1.X,Point2.Y));
    ClipPolygon(ClipPoints,PInfo^.ClipRect);
    if ClipPoints.Count>0 then begin
      MoveTo(PInfo^.ExtCanvas.WindowDC,Round(ClipPoints[0].X),Round(ClipPoints[0].Y));
      for Cnt:=1 to ClipPoints.Count-1 do LineTo(PInfo^.ExtCanvas.WindowDC,
          Round(ClipPoints[Cnt].X),Round(ClipPoints[Cnt].Y));
    end;
    ClipPoints.Free;
  end;

//++ Glukhov Bug#264 Build#161 15.06.01
// Procedure DrawRect had a strange code to set a size.
// I separated the right part and put it in the function.
// I need the function for the other drawing procedures.
Function DrawSize( PInfo: PPaint; Size: Integer ): Integer;
  var DPos         : TPoint;
  begin
    DPos.X:= Size;
    DPos.Y:= 0;
    LpToDP( PInfo^.ExtCanvas.WindowDC, DPos, 1 );
    Result:= DPos.X;
  end;

Procedure DrawBox( PInfo: PPaint; Pos: PDPoint; Size: Integer );
  var DPos         : TPoint;
      ARect        : TRect;
  begin
    Size:= DrawSize( PInfo, Size );
    PInfo^.ConvertToDisp( Pos^, DPos );
    ARect:= RectWithPixelSize( PInfo^.ExtCanvas.WindowDC, DPos.X, DPos.Y, Size );
    Rectangle( PInfo^.ExtCanvas.WindowDC, ARect.Left, ARect.Top, ARect.Right, ARect.Bottom );
  end;

// YG: In this procedure the size is ignored !?!
Procedure DrawRect( PInfo: PPaint; Pos: PDPoint; Size: Integer );
  begin
{
  var DPos         : TPoint;
      ARect        : TRect;
  begin
    DPos.X:=ibRect;   // YG: the size is ignored !?!
    DPos.Y:=0;
    LpToDP(PInfo^.ExtCanvas.WindowDC,DPos,1);
    Size:=DPos.X;
    PInfo^.ConvertToDisp( Pos^, DPos );
    ARect:= RectWithPixelSize( PInfo^.ExtCanvas.WindowDC, DPos.X, DPos.Y, Size );
    Rectangle( PInfo^.ExtCanvas.WindowDC, ARect.Left, ARect.Top, ARect.Right, ARect.Bottom );
}
    DrawBox( PInfo, Pos, ibRect );    // YG: I kept the size ignoring
  end;

Procedure DrawCross( PInfo: PPaint; Pos: PDPoint; Size: Integer; Flag: Integer );
  var DPos         : TPoint;
      ARect        : TRect;
  begin
    Size:= DrawSize( PInfo, Size );
    PInfo^.ConvertToDisp( Pos^, DPos );
    ARect:= RectWithPixelSize( PInfo^.ExtCanvas.WindowDC, DPos.X, DPos.Y, Size );
    if (Flag and 1) <> 0    // skip left
    then  MoveTo( PInfo^.ExtCanvas.WindowDC, DPos.X, DPos.Y )
    else  MoveTo( PInfo^.ExtCanvas.WindowDC, ARect.Left, DPos.Y );
    if (Flag and 4) <> 0    // skip right
    then  LineTo( PInfo^.ExtCanvas.WindowDC, DPos.X, DPos.Y )
    else  LineTo( PInfo^.ExtCanvas.WindowDC, ARect.Right, DPos.Y );
    if (Flag and 2) <> 0    // skip top
    then  MoveTo( PInfo^.ExtCanvas.WindowDC, DPos.X, DPos.Y )
    else  MoveTo( PInfo^.ExtCanvas.WindowDC, DPos.X, ARect.Top );
    if (Flag and 8) <> 0    // skip bottom
    then  LineTo( PInfo^.ExtCanvas.WindowDC, DPos.X, DPos.Y )
    else  LineTo( PInfo^.ExtCanvas.WindowDC, DPos.X, ARect.Bottom );
  end;
//-- Glukhov Bug#264 Build#161 15.06.01

Constructor TNewPoly.Init( ACol, ALine: Word; ACapt: Boolean );
  begin
    TPoly.Init;
    Pen:=GetPen(RGBColors[ACol],ALine,0);
    LineOn:=FALSE;
    LastIns:=False;
    StartPos.Init(0,0);
    LastPos.Init(0,0);
    Capture:=ACapt;
    DrawReally:=FALSE;
{++ Ivanoff OrthoDraw}
    SELF.bDrawOrtho := FALSE;
{-- Ivanoff}
//++ Glukhov Bug#100 Build#152 26.03.01
    bNoDrawing:= False;
//-- Glukhov Bug#100 Build#152 26.03.01
    Closed:=False;
    SavedCount:=0;
  end;

Destructor TNewPoly.Done;
  begin
    StartPos.Done;
    LastPos.Done;
    DeleteObject(Pen);
    TPoly.Done;
  end;

Function TNewPoly.CheckCopyData
   (
   PInfo           : PPaint;
   AItem           : PPoly;
   DelLast         : Boolean
   )
   : Boolean;
  var Swapp        : Pointer;
      AMin         : Byte;
  begin
    if Data^.Count>0 then begin
//++ Glukhov Bug#100 Build#152 26.03.01
//      Draw(PInfo,TRUE);
      if not bNoDrawing  then Draw( PInfo, TRUE );
//-- Glukhov Bug#100 Build#152 26.03.01
      LineOn:=FALSE;
      if DelLast and LastIns then Data^.AtFree(Data^.Count-1);
      if AItem^.GetObjType=ot_Poly then AMin:=2
      else AMin:=3;
      if Data^.Count<AMin then CheckCopyData:=FALSE
      else begin
        CalculateClipRect;
        Swapp:=AItem^.Data;
        AItem^.Data:=Data;
        Data:=Swapp;
        Swapp:=AItem^.SnapInfo;
        AItem^.SnapInfo:=SnapInfo;
        SnapInfo:=Swapp;
        AItem^.ClipRect:=ClipRect;
        AItem^.ClosePoly;
        CheckCopyData:=TRUE;
      end;
      Data^.FreeAll;
      if SnapInfo<>NIL then begin
        Dispose(SnapInfo,Done);
        SnapInfo:=NIL;
      end;
    end
    else CheckCopyData:=FALSE;
  end;

Function TNewPoly.GetPoly
   (
   PInfo           : PPaint;
   AItem           : PPoly;
   DelLast         : Boolean
   )
   : Boolean;
  var Swapp        : Pointer;
      AMin         : Byte;
  begin
    if Data^.Count>0 then begin
      Draw(PInfo,TRUE);
      LineOn:=FALSE;
      if DelLast and LastIns then Data^.AtFree(Data^.Count-1);
      if AItem^.GetObjType=ot_Poly then AMin:=2
      else AMin:=3;
      if Data^.Count<AMin then GetPoly:=FALSE
      else begin
        CalculateClipRect;
        if AItem^.GetObjType=ot_CPoly then ClosePoly;
        Dispose(AItem^.Data,Done);
        AItem^.Data:=MakeObjectCopy(Data);
        Swapp:=AItem^.SnapInfo;
        AItem^.SnapInfo:=SnapInfo;
        SnapInfo:=Swapp;
        AItem^.ClipRect:=ClipRect;
        GetPoly:=TRUE;
      end;
      if SnapInfo<>NIL then begin
        Dispose(SnapInfo,Done);
        SnapInfo:=NIL;
      end;
      Draw(PInfo,TRUE);
    end
    else GetPoly:=FALSE;
  end;

Procedure TNewPoly.EndIt
   (
   PInfo           : PPaint
   );
  begin
    SavedCount:=0;
    Draw(PInfo,TRUE);
    Data^.FreeAll;
    if SnapInfo<>NIL then begin
      Dispose(SnapInfo,Done);
      SnapInfo:=NIL;
    end;
    LineOn:=FALSE;
    LastIns:=False;
    if Closed then begin
       PProj(PInfo^.AProj)^.ClearStatusText;
    end;
    Closed:=False;
  end;

{++ Ivanoff OrthoDraw}
Procedure TNewPoly.DrawLastLineAsOrtho(PInfo: PPaint);
Var
   NDist: Double;
   NPoint: TDPoint;
   ABaseStartPoint, ABaseEndPoint: PDPoint;
Begin
     ABaseStartPoint := SELF.Data.At(SELF.Data.Count-2);
     ABaseEndPoint := SELF.Data.At(SELF.Data.Count-1);
     PInfo^.NormalDistance(ABaseStartPoint, ABaseEndPoint, @LastPos, NDist, NPoint);
     DrawLine(Pinfo, NPoint, LastPos);
End;
{-- Ivanoff}

function TNewPoly.GetLastOrthoDist(PInfo: PPaint): Double;
var
ADist,BDist,NDist: Double;
AAngle,BAngle: Double;
NPoint: TDPoint;
ABaseStartPoint,ABaseEndPoint: PDPoint;
begin
     Result:=0;
     if (bDrawOrtho) and (Data^.Count > 1) then begin
        ABaseStartPoint:=Data^.At(Data^.Count-2);
        ABaseEndPoint:=Data^.At(Data^.Count-1);
        PInfo^.NormalDistance(ABaseStartPoint,ABaseEndPoint,@LastPos,NDist,NPoint);

        Result:=ABaseStartPoint^.Dist(NPoint);

        AAngle:=ABaseStartPoint^.CalculateAngle(ABaseEndPoint^);
        BAngle:=ABaseStartPoint^.CalculateAngle(NPoint);

        if Abs(AAngle-BAngle) > 1 then begin
           Result:=-Result;
        end;
     end;
end;

procedure TNewPoly.Draw(PInfo: PPaint; Clipping: Boolean);
var
OldR2: Integer;
Cnt: Integer;
begin
    PInfo^.BeginPaint(0,Pen,dm_NoPattern,1);
    SetBKColor(PInfo^.ExtCanvas.WindowDC,$FFFFFF);
    OldR2:=SetROP2(PInfo^.ExtCanvas.WindowDC,R2_DEFPEN);
    for Cnt:=1 to Data^.Count-1 do begin
        DrawLine(PInfo,PDPoint(Data^.At(Cnt-1))^,PDPoint(Data^.At(Cnt))^);
    end;
    if LineOn then begin
       if (SELF.Data.Count < 2) or (not Self.bDrawOrtho) then begin
          DrawLine(PInfo,StartPos,LastPos);                 {1104F}
          if (Closed) and (Data^.Count > 1) then begin
             DrawLine(PInfo,LastPos,FirstPos);
          end;
       end
       else begin
          Self.DrawLastLineAsOrtho(PInfo);
       end;
    end;
    SetROP2(PInfo^.ExtCanvas.WindowDC,OldR2);
    PInfo^.EndPaint;
end;

procedure TNewPoly.DrawMarks(PInfo: PPaint);
var
OldR2: Integer;
i: Integer;
APen: HPen;
begin
    APen:=GetPen(RGBColors[c_White],lt_Solid,0);
    PInfo^.BeginPaint(0,APen,dm_Paint,0);
    SetBKColor(PInfo^.ExtCanvas.WindowDC,$FFFFFF);
    OldR2:=SetROP2(PInfo^.ExtCanvas.WindowDC,R2_WHITE);
    try
       if Data^.Count > 0 then for i:=0 to Data^.Count-1 do begin
          DrawBox(PInfo,PDPoint(Data^.At(i)),7);
       end;
    finally
       SetROP2(PInfo^.ExtCanvas.WindowDC,OldR2);
       PInfo^.EndPaint;
       DeleteObject(APen);
    end;
end;

{************************************************************************************}
{ Function TNewPoly.PointInsert                                                      }
{  F�gt einen Punkt in das Poly ein. Das Poly wird daraufhin neu gezeichnet (XOR).   }
{  Wenn SnapIt TRUE, so wird der Punkt in SnapInfo eingef�gt.                        }
{  Liefert FALSE, wenn die maximale Punktanzahl (MaxCollectionSize) �berschritten    }
{  wurde, sonst TRUE.                                                                }
{************************************************************************************}
Function TNewPoly.PointInsert
   (
   PInfo           : PPaint;
   Point           : TDPoint;
   SnapIt          : Boolean
   )
   : Boolean;
  var
  OldCount: Integer;
  begin
    OldCount:=Data^.Count;
    if TPoly.InsertPoint(Point) then begin
//++ Glukhov Bug#100 Build#152 26.03.01
//      MouseMove(PInfo,Point);
      if not bNoDrawing  then MouseMove(PInfo,Point);
//-- Glukhov Bug#100 Build#152 26.03.01
      if Data^.Count=1 then begin
        AktDist:=0;
        StartPos:=Point;
        LastPos:=Point;
        FirstPos.Init(Point.x,Point.y);
        ClipRect.Init;
        ClipRect.CorrectByPoint(Point);
        LineOn:=TRUE;
      end
      else begin
        LastPos:=Point;
        StartPos:=Point;
      end;
      if Data^.Count>1 then AktDist:=AktDist+PDPoint(Data^.At(Data^.Count-2))^.Dist(Point);
      if not SnapIt then begin
        if SnapInfo=NIL then SnapInfo:=New(PWArray,Init);
        SnapInfo^.Insert(Data^.Count-1);
      end;
      PointInsert:=TRUE;
      if (not bNoDrawing) and (Data^.Count = 2) then begin
         LineOn:=False;
         Draw(PInfo,False);
         LineOn:=True;
         Draw(PInfo,False);
      end;
      ShowAreaStatus(PInfo);
    end
    else PointInsert:=FALSE;
    DeleteLast:=(OldCount <> Data^.Count);
    if not PProj(PInfo^.AProj)^.bDraw3Points then begin
       DrawMarks(PInfo);
    end;
  end;

{++ Ivanoff NA1 BUILD#106}
{Procedure deletes last input point for new polygon or line.}
Procedure TNewPoly.DeleteLastPoint(PInfo: PPaint);
Var
   Point: PDPoint;
Begin
     If Data.Count < 2 Then EXIT;
     Data.AtFree(Data.Count-1);
     Point := Data.At(Data.Count-1);
     StartPos := Point^;
     ClipRect.CorrectByPoint(LastPos);
     Invalidate(PInfo, FALSE);
     MouseMove(PInfo, LastPos);
End;
{-- Ivanoff}

{++Brovak BUG 104 BUILD 130}
Function TNewPoly.GetLastPoint(PInfo:PPaint):PDPoint;
 begin;
  Result:=nil;
  If Data.Count < 1 then Exit;
  Result:=Data.At(Data.Count-1);
 end;
{--Brovak}

Function TNewPoly.GetDistance
   : Double;
  begin
    GetDistance:=Laenge*100;
  end;

function TNewPoly.GetLastDist(PInfo:PPaint): Double;
var
P1,P2: PDPoint;
NDist: Double;
NPoint: TDPoint;
ABaseStartPoint, ABaseEndPoint: PDPoint;
begin
     Result:=0;
     if Data.Count < 2 then exit;

     if Self.bDrawOrtho then begin
          ABaseStartPoint := SELF.Data.At(SELF.Data.Count-3);
          ABaseEndPoint := SELF.Data.At(SELF.Data.Count-2);
          PInfo^.NormalDistance(ABaseStartPoint, ABaseEndPoint, @LastPos, NDist, NPoint);
          Result:=NDist;
     end
     else begin
          P1:=Data.At(Data.Count-1);
          P2:=Data.At(Data.Count-2);

          Result:=P1^.Dist(P2^);
     end;
end;

Function TNewPoly.GetXDist
   : LongInt;
  begin
    GetXDist:=LimitToLong(ClipRect.XSize);
  end;

Function TNewPoly.GetYDist
   : LongInt;
  begin
    GetYDist:=LimitToLong(ClipRect.YSize);
  end;

Procedure TNewPoly.MouseMove
   (
   PInfo           : PPaint;
   APos            : TDPoint
   );
  var OldR2        : Integer;
  begin
    if (LineOn) and (not bNoDrawing) then begin
      PInfo^.BeginPaint(0,Pen,dm_NoPattern,1);
      OldR2:=SetROP2(PInfo^.ExtCanvas.WindowDC,R2_DEFPEN);
      SetBKColor(PInfo^.ExtCanvas.WindowDC,$FFFFFF);
      if (Data.Count < 2) or (not SELF.bDrawOrtho) then begin
         DrawLine(PInfo,StartPos,LastPos);
         DrawLine(PInfo,StartPos,APos);
      end
      else begin
         DrawLastLineAsOrtho(PInfo);
      end;
      if (Closed) and (Data^.Count > 1) and (not bDrawOrtho) then begin
         DrawLine(PInfo,LastPos,FirstPos);
         DrawLine(PInfo,APos,FirstPos);
      end;
      LastPos:=APos;
      if (Data.Count < 2) or (not SELF.bDrawOrtho) then else begin
         DrawLastLineAsOrtho(PInfo);
      end;
      SetROP2(PInfo^.ExtCanvas.WindowDC,OldR2);
      PInfo^.EndPaint;
      ShowAreaStatus(PInfo);
    end;
  end;

procedure TNewPoly.ShowAreaStatus(PInfo: PPaint);
var
f: Double;
i: Integer;
begin
    if (Closed) and (not bNoDrawing) then begin
       f:=0.0;
       try
          if (Data^.Count > 1) then begin
             for i:=1 to Data^.Count-1 do begin
                 f:=f+((0.0+PDPoint(Data^.At(i))^.X+PDPoint(Data^.At(i-1))^.X)*(0.0+PDPoint(Data^.At(i))^.Y-PDPoint(Data^.At(i-1))^.Y));
             end;
             f:=f+((0.0+LastPos.X+PDPoint(Data^.At(Data^.Count-1))^.X)*(0.0+LastPos.Y-PDPoint(Data^.At(Data^.Count-1))^.Y));
             f:=f+((0.0+PDPoint(Data^.At(0))^.X+LastPos.X)*(0.0+PDPoint(Data^.At(0))^.Y-LastPos.Y));
          end;
       finally
          f:=Abs(f/20000/IniFile^.AreaFact);
          PProj(PInfo^.AProj)^.ClearStatusText;
          PProj(PInfo^.AProj)^.SetStatusText(GetLangText(11370) + ' ' + FormatStr(f*100.0,2) + IniFile^.GetAreaUnit,True);
       end;
    end;
end;

procedure TNewPoly.Reset(PInfo: PPaint);
begin
     while (SavedCount >0) and (Data^.Count > SavedCount) do begin
           DeleteLastPoint(PInfo);
     end;
end;

procedure TNewPoly.Save(AItem: PIndex);
begin
     SnapObject:=AItem;
     SavedCount:=Data^.Count;
end;

procedure TNewPoly.DrawAlong(PInfo: PPaint; APoly: PPoly);
var
APoint: TDPoint;
Idx1,Idx2,i: Integer;

     procedure DoInsert;
     begin
          if (APoly^.Data^.Count > i) then begin
             APoint.Init(TDPoint(APoly^.Data^.At(i)^).X,TDPoint(APoly^.Data^.At(i)^).Y);
             if FindIndexOfPoint(APoint) = -1 then begin
                PointInsert(PInfo,APoint,False);
             end;
          end;
     end;

begin
     if (APoly <> nil) then begin
        Idx1:=APoly^.SnapStart;
        Idx2:=APoly^.SnapEnd;
        if (Idx1 < 0) or (Idx2 < 0) then exit;
        if APoly^.GetObjType = ot_Poly then begin
           if Idx1 < Idx2 then begin
              for i:=Idx1+1 to Idx2-1 do begin
                  DoInsert;
              end;
           end
           else if Idx1 > Idx2 then begin
              for i:=Idx1-1 downto Idx2+1 do begin
                  DoInsert;
              end;
           end;
        end
        else if APoly^.GetObjType = ot_CPoly then begin
           if DirFlag then begin
              if Idx1 < Idx2 then begin
                 for i:=Idx1-1 downto 1 do begin
                     DoInsert;
                 end;
                 for i:=APoly^.Data^.Count-1 downto Idx2+1 do begin
                     DoInsert;
                 end;
              end
              else if Idx1 > Idx2 then begin
                 for i:=Idx1+1 to APoly^.Data^.Count-1 do begin
                     DoInsert;
                 end;
                 for i:=1 to Idx2-1 do begin
                     DoInsert;
                 end;
              end;
           end
           else begin
              if Idx1 < Idx2 then begin
                 for i:=Idx1+1 to Idx2-1 do begin
                     DoInsert;
                 end;
              end
              else if Idx1 > Idx2 then begin
                 for i:=Idx1-1 downto Idx2+1 do begin
                     DoInsert;
                 end;
              end;
           end;
        end;
     end;
end;

Constructor TInput.Init
   (
   ACol            : Word;
   ALine           : Word
   );
  begin
    inherited Init;
    StartPos.Init(0,0);
    LastPos.Init(0,0);
    IsOn:=FALSE;
    Pen:=GetPen(RGBColors[ACol],ALine,0);
    Capture:=FALSE;
  end;

Destructor TInput.Done;
  begin
    LastPos.Done;
    StartPos.Done;
    DeleteObject(Pen);
    inherited Done;
  end;

Function TInput.IsItOn
   : Boolean;
  begin
    IsItOn:=IsOn;
  end;

Procedure TInput.Draw
   (
   PInfo           : PPaint
   );
  var OldR2        : Integer;
  begin
    if IsOn then begin
      PInfo^.BeginPaint(0,Pen,dm_NoPattern,1);
      OldR2:=SetROP2(PInfo^.ExtCanvas.WindowDC,R2_DEFPEN);
      SetBKColor(PInfo^.ExtCanvas.WindowDC,$FFFFFF);
      DrawIt(PInfo);
      SetROP2(PInfo^.ExtCanvas.WindowDC,OldR2);
      PInfo^.EndPaint;
    end;
  end;

Procedure TInput.DrawIt
   (
   PInfo           : PPaint
   );
  var ClipPoints   : TClipPointList;
      Cnt          : Integer;
      Point1       : TGrPoint;
      Point2       : TGrPoint;
  begin
    ClipPoints:=TClipPointList.Create;
    PInfo^.ConvertToDispDouble(StartPos,Point1.X,Point1.Y);
    PInfo^.ConvertToDispDouble(LastPos,Point2.X,Point2.Y);
    ClipPoints.Add(GrPoint(Point1.X,Point1.Y));
    ClipPoints.Add(GrPoint(Point2.X,Point1.Y));
    ClipPoints.Add(GrPoint(Point2.X,Point2.Y));
    ClipPoints.Add(GrPoint(Point1.X,Point2.Y));
    ClipPolygon(ClipPoints,PInfo^.ClipRect);
    if ClipPoints.Count>0 then begin
      MoveTo(PInfo^.ExtCanvas.WindowDC,Round(ClipPoints[0].X),Round(ClipPoints[0].Y));
      for Cnt:=1 to ClipPoints.Count-1 do LineTo(PInfo^.ExtCanvas.WindowDC,
          Round(ClipPoints[Cnt].X),Round(ClipPoints[Cnt].Y));
    end;
    ClipPoints.Free;
  end;

Procedure TInput.EndIt
   (
   PInfo           : PPaint
   );
  begin
    if IsOn then Draw(PInfo);
    IsOn:=FALSE;
  end;

Procedure TInput.MouseMove
   (
   PInfo           : PPaint;
   APos            : TDPoint
   );
  var OldR2        : Integer;
  begin
    if IsOn then begin
      PInfo^.BeginPaint(0,Pen,dm_NoPattern,1);
      OldR2:=SetROP2(PInfo^.ExtCanvas.WindowDC,R2_DEFPEN);
      SetBKColor(PInfo^.ExtCanvas.WindowDC,$FFFFFF);
      DrawIt(PInfo);
      LastPos:=APos;
      DrawIt(PInfo);
      SetROP2(PInfo^.ExtCanvas.WindowDC,OldR2);
      PInfo^.EndPaint;
    end;
  end;

Procedure TInput.StartIt
   (
   PInfo           : PPaint;
   Pos             : TDPoint;
   ACapt           : Boolean
   );
  begin
    if not IsOn then begin
      IsOn:=TRUE;
      StartPos:=Pos;
      LastPos:=Pos;
      Capture:=ACapt;
      Draw(PInfo);
    end;
  end;

Function TInputRect.IsBigEnough
   (
   PInfo           : PPaint;
   XSize           : Integer;
   YSize           : Integer;
   Both            : Boolean
   )
   : Boolean;
  var Point1       : TPoint;
      Point2       : TPoint;
  begin
    PInfo^.ConvertToDisp(StartPos,Point1);
    PInfo^.ConvertToDisp(LastPos,Point2);
    if Both then IsBigEnough:=(Abs(Point1.X-Point2.X)>XSize) and
        (Abs(Point1.Y-Point2.Y)>YSize)
    else IsBigEnough:=(Abs(Point1.X-Point2.X)>XSize) or
       (Abs(Point1.Y-Point2.Y)>YSize)
  end;

Procedure TInputRect.GetRect
   (
   var Rect        : TDRect
   );
  begin
    Rect.Init;
    Rect.CorrectByPoint(StartPos);
    Rect.CorrectByPoint(LastPos);
  end;

Procedure TInputRect.GetIsoDrawRect
   (
   PInfo           : PPaint;
   var Rect        : TDRect
   );
  var Point1       : TDPoint;
      Point2       : TDPoint; 
  begin
    PInfo^.ProjPointToWorldPoint(StartPos,Point1);
    PInfo^.ProjPointToWorldPoint(LastPos,Point2);
    Rect.Init;
    Rect.CorrectByPoint(Point1);
    Rect.CorrectByPoint(Point2);
  end;

{******************************************************************************}
{ Function TInputRect.GetBorder                                                }
{------------------------------------------------------------------------------}
{ Autor: Martin Forst                                                          }
{------------------------------------------------------------------------------}
{ Liefert das Rechteck als geschlossenes Polygon zur�ck. Das Polygon wird      }
{ entgegen der Drehung der Ansicht gedreht. Damit entspricht es, wenn es       }
{ am Bildschirm gezeichnet w�rde, einem Bildschirmkanten-parallelem Rechteck.  }
{******************************************************************************}
Function TInputRect.GetBorder
   (
   PInfo           : PPaint
   )
   : PCPoly;
  begin
    Result:=PInfo^.BorderForRectCorners(StartPos,LastPos);
  end;

Procedure TInputCircle.MoveMiddle         
   (
   PInfo           : PPaint;
   var AMiddle     : TDPoint
   );
  var OldR2        : Integer;
  begin
    if IsOn then begin
      PInfo^.BeginPaint(0,Pen,dm_NoPattern,1);
      OldR2:=SetROP2(PInfo^.ExtCanvas.WindowDC,R2_DEFPEN);
      SetBKColor(PInfo^.ExtCanvas.WindowDC,$FFFFFF);
      DrawIt(PInfo);
      StartPos:=AMiddle;
      DrawIt(PInfo);
      SetROP2(PInfo^.ExtCanvas.WindowDC,OldR2);
      PInfo^.EndPaint;
    end;
  end;                                      

Procedure TInputCircle.DrawIt
   (
   PInfo           : PPaint
   );
  var XMiddle      : Double;
      YMiddle      : Double;
      BPrimaryAxis : Double;
      ClipCount    : Integer;
      ClipInfo     : TClippingInfo;
      CurVisible   : Boolean;
      PointCnt     : Integer;
      Cnt          : Integer;
      Cnt1         : Integer;
      Points       : Array[0..2000] of TPoint;
      Angle1       : Double;
      Angle2       : Double;
      Steps        : Integer;
  begin
    PInfo^.ConvertToDispDouble(StartPos,XMiddle,YMiddle);
    BPrimaryAxis:=PInfo^.CalculateDispDouble(GetRadius);
    ClipCount:=ClipCircle(GrPoint(XMiddle,YMiddle),BPrimaryAxis,PInfo^.ClipRect,ClipInfo);
    if ClipCount>0 then begin
      CurVisible:=not ClipInfo.FirstVisible;
      PointCnt:=0;
      for Cnt:=0 to ClipCount-1 do begin
        if not CurVisible or ClipInfo.EdgeInfo[Cnt] then begin
          Points[PointCnt].X:=Round(ClipInfo.Points[Cnt].X);
          Points[PointCnt].Y:=Round(ClipInfo.Points[Cnt].Y);
          Inc(PointCnt);
        end;
        if not ClipInfo.EdgeInfo[Cnt] then begin
          CurVisible:=not CurVisible;
          if CurVisible then begin
            Angle1:=LineAngle(GrPoint(XMiddle,YMiddle),ClipInfo.Points[Cnt]);
            Angle2:=LineAngle(GrPoint(XMiddle,YMiddle),ClipInfo.Points[(Cnt+1) Mod ClipCount]);
            if Angle2<Angle1 then Angle2:=Angle2+2*Pi-Angle1
            else Angle2:=Angle2-Angle1;
            if PInfo^.Printing then Steps:=Round(Angle2*4*180/Pi)
            else Steps:=Round(Angle2*180/Pi);
            if Steps=0 then Steps:=1;
            Angle2:=Angle2/Steps;
            for Cnt1:=PointCnt to PointCnt+Steps do begin
              Points[Cnt1].X:=Round(XMiddle+BPrimaryAxis*Cos(Angle1));
              Points[Cnt1].Y:=Round(YMiddle+BPrimaryAxis*Sin(Angle1));
              Angle1:=Angle1+Angle2;
            end;
            PointCnt:=PointCnt+Steps+1;
          end;
        end;
      end;
      Polyline(PInfo^.ExtCanvas.WindowDC,Points,PointCnt);
    end
    else if (BPrimaryAxis<PInfo^.ClipRect.Right-PInfo^.ClipRect.Left) then
        if (XMiddle<PInfo^.ClipRect.Right) then if (XMiddle>PInfo^.ClipRect.Left) then
        if (YMiddle<PInfo^.ClipRect.Top) then if (YMiddle>PInfo^.ClipRect.Bottom) then begin
      Angle1:=0;
      Angle2:=2*Pi;
      if PInfo^.Printing then PointCnt:=Round(Angle2*4*180/Pi)
      else PointCnt:=Round(Angle2*180/Pi);
      if PointCnt=0 then PointCnt:=1;
      Angle2:=Angle2/PointCnt;
      for Cnt1:=0 to PointCnt do begin
        Points[Cnt1].X:=Round(XMiddle+BPrimaryAxis*Cos(Angle1));
        Points[Cnt1].Y:=Round(YMiddle+BPrimaryAxis*Sin(Angle1));
        Angle1:=Angle1+Angle2;
      end;
      Polygon(PInfo^.ExtCanvas.WindowDC,Points,PointCnt+1)
    end;
  end;

Procedure TInputCircle.GetCircle
   (
   var Middle      : TDPoint;
   var Radius      : LongInt
   );
  begin
    Middle.Init(StartPos.X,StartPos.Y);
    Radius:=GetRadius;
  end;

Function TInputCircle.GetRadius
   : LongInt;
  begin
    GetRadius:=LimitToLong(StartPos.Dist(LastPos));
  end;

Procedure TInputLine.DrawIt
   (
   PInfo           : PPaint
   );

  begin
    DrawLine(PInfo,StartPos,LastPos);
  end;

Procedure TInputLine.GetLine
   (
   var StartP      : TDPoint;
   var EndP        : TDPoint
   );
  begin
    StartP.Init(StartPos.X,StartPos.Y);
    EndP.Init(LastPos.X,LastPos.Y);
  end;

Function TInputLine.GetDistance
   : LongInt;
  begin
    GetDistance:=LimitToLong(StartPos.Dist(LastPos));
  end;

Procedure TInputLines.DrawIt
   (
   PInfo           : PPaint
   );

  begin
    DrawLine(PInfo,StartPos,LastPos);
    if HasEnd then DrawLine(PInfo,LastPos,EndPos);
  end;

Procedure TInputLines.GetLines
   (
   var ALast       : TDPoint
   );
  begin
    ALast.Init(LastPos.X,LastPos.Y);
  end;

Procedure TInputLines.SetEnd
   (
   PInfo           : PPaint;
   AktPos          : TDPoint;
   AHasEnd         : Boolean;
   AEnd            : TDPoint
   );
  begin
    LastPos.Init(AktPos.X,AktPos.Y);
    HasEnd:=AHasEnd;
    EndPos.Init(AEnd.X,AEnd.Y);
    Draw(PInfo);
  end;

Procedure TInputLines.StartIt
   (
   PInfo           : PPaint;
   Pos             : TDPoint;
   ACapt           : Boolean
   );
  begin
    if not IsOn then begin
      IsOn:=TRUE;
      StartPos:=Pos;
      LastPos:=Pos;
      Capture:=ACapt;
    end;
  end;

constructor TInputArc.Init
  (
  ACol : word;
  ALine : word;
  ARadius : longint
  );

begin
  TInput.Init(ACol, ALine);
  Radius:=ARadius;
  DrawArc:=FALSE;
end;

procedure TInputArc.SetDrawArc                                       {0604F}
  (
  PInfo : PPaint;
  Pos : TDPoint
  );

begin
  Draw(PInfo);
  BeginArc.Init(Pos.X,Pos.Y);
  DrawArc:=TRUE;
  Draw(PInfo);
end;                                                                 {0604F}

Function TInputArc.GetAngle
   : Integer;
  begin
    GetAngle:=Round(StartPos.CalculateAngle(LastPos)*Rad2Deg);
  end;

Procedure TInputArc.DrawIt
  (
  PInfo : PPaint
  );
  var Points       : Array[0..2000] of TPoint;
      BPrimaryAxis : Double;
      XMiddle      : Double;
      YMiddle      : Double;
      ClipCount    : Integer;
      ClipInfo     : TClippingInfo;
      Cnt          : Integer;
      Cnt1         : Integer;
      Angle1       : Double;
      Angle2       : Double;
      PointCnt     : Integer;
      BeginAngle   : Double;
      EndAngle     : Double;
  begin
    DrawLine(PInfo,StartPos,LastPos);
    BeginAngle:=StartPos.CalculateAngle(BeginArc);
    EndAngle:=StartPos.CalculateAngle(LastPos);
    PInfo^.ConvertToDispDouble(StartPos,XMiddle,YMiddle);
    BPrimaryAxis:=PInfo^.CalculateDispDouble(Radius);
    ClipCount:=ClipArc(GrPoint(XMiddle,YMiddle),BPrimaryAxis,BeginAngle,
        EndAngle,PInfo^.ClipRect,ClipInfo);
    if ClipCount>0 then begin
      for Cnt:=0 to ClipCount Div 2-1 do begin
        Angle1:=LineAngle(GrPoint(XMiddle,YMiddle),ClipInfo.Points[Cnt*2]);
        Angle2:=LineAngle(GrPoint(XMiddle,YMiddle),ClipInfo.Points[Cnt*2+1]);
        if Angle2<Angle1 then Angle2:=Angle2+2*Pi-Angle1
        else Angle2:=Angle2-Angle1;
        if PInfo^.Printing then PointCnt:=Round(Angle2*4*180/Pi)
        else PointCnt:=Round(Angle2*180/Pi);
        if PointCnt=0 then PointCnt:=1;
        Angle2:=Angle2/PointCnt;
        for Cnt1:=0 to PointCnt do begin
          Points[Cnt1].X:=Round(XMiddle+BPrimaryAxis*Cos(Angle1));
          Points[Cnt1].Y:=Round(YMiddle+BPrimaryAxis*Sin(Angle1));
          Angle1:=Angle1+Angle2;
        end;
        Polyline(PInfo^.ExtCanvas.WindowDC,Points,PointCnt+1);
      end;
    end
    { symbol is clipped but not the arc, check if arc lies inside the clip-rect
    | because than it must be drawn }
    else if (BPrimaryAxis<PInfo^.ClipRect.Right-PInfo^.ClipRect.Left) then
        if (XMiddle<PInfo^.ClipRect.Right) then if (XMiddle>PInfo^.ClipRect.Left) then
        if (YMiddle<PInfo^.ClipRect.Top) then if (YMiddle>PInfo^.ClipRect.Bottom) then begin
      Angle1:=BeginAngle;
      Angle2:=EndAngle;
      if Angle2<Angle1 then Angle2:=Angle2+2*Pi-Angle1
      else Angle2:=Angle2-Angle1;
      if PInfo^.Printing then PointCnt:=Round(Angle2*4*180/Pi)
      else PointCnt:=Round(Angle2*180/Pi);
      if PointCnt=0 then PointCnt:=1;
      Angle2:=Angle2/PointCnt;
      for Cnt1:=0 to PointCnt do begin
        Points[Cnt1].X:=Round(XMiddle+BPrimaryAxis*Cos(Angle1));
        Points[Cnt1].Y:=Round(YMiddle+BPrimaryAxis*Sin(Angle1));
        Angle1:=Angle1+Angle2;
      end;
      Polyline(PInfo^.ExtCanvas.WindowDC,Points,PointCnt+1);
    end;
  end;

Constructor TInputText.Init
   (
   ACol            : Word;
   ALine           : Word
   );
  begin
    TInput.Init(ACol,ALine);
    Point1.Init(0,0);
    Point2.Init(0,0);
    Point3.Init(0,0);
    Rotate:=FALSE;
    FixHeight:=FALSE;
    Angle:=0;
    AspectRat:=1;
  end;

Procedure TInputText.CalculateRectangle;
  var Beta       : Real;
      CosL       : Real;
      SinL       : Real;
  begin
    Beta:=Angle*Deg2Rad;
    CosL:=Cos(Beta);
    SinL:=Sin(Beta);
    Point1:=StartPos;
    Point1.Move(Round(CosL*Width),Round(SinL*Width));
    Point2:=Point1;
    Point2.Move(Round(-SinL*Height),Round(CosL*Height));
    Point3:=Point2;
    Point3.Move(Round(-CosL*Width),Round(-SinL*Width));
  end;

Procedure TInputText.CalculateSize;
  var Diag       : Double;
      Alpha      : Real;
      Beta       : Real;
      AWidth     : LongInt;
  begin
    Beta:=Angle*Deg2Rad;
    Alpha:=StartPos.CalculateAngle(LastPos);
    Diag:=StartPos.Dist(LastPos);
    if FixHeight then begin
      if Sin(Alpha-Beta)<0 then Height:=-Abs(Height)
      else Height:=Abs(Height);
    end
    else Height:=LimitToLong(Sin(Alpha-Beta)*Diag);
    Width:=LimitToLong(Cos(Alpha-Beta)*Diag);
    if Width<0 then begin
      Height:=-Height;
      Width:=-Width;
      Angle:=(Angle+180) Mod 360;
    end;
    AWidth:=LimitToLong(Abs(Height)*AspectRat);
    if Width<AWidth then begin
      if FixHeight then Width:=AWidth
      else begin
        if Height<0 then Height:=-LimitToLong(Width/AspectRat)
        else Height:=LimitToLong(Width/AspectRat);
      end;
    end;
    if Abs(Height)<=MinTextH then begin
      if Height<0 then Height:=-MinTextH
      else Height:=MinTextH;
      AWidth:=LimitToLong(Abs(Height)*AspectRat);
      if Width<AWidth then Width:=AWidth;
    end;
    CalculateRectangle;
  end;

Procedure TInputText.DrawIt
   (
   PInfo           : PPaint
   );                   
  var ClipPoints   : TClipPointList;
      Cnt          : Integer;
      Point        : TGrPoint;
  begin
    ClipPoints:=TClipPointList.Create;
    PInfo^.ConvertToDispDouble(StartPos,Point.X,Point.Y);
    ClipPoints.Add(Point);
    PInfo^.ConvertToDispDouble(Point1,Point.X,Point.Y);
    ClipPoints.Add(Point);
    PInfo^.ConvertToDispDouble(Point2,Point.X,Point.Y);
    ClipPoints.Add(Point);
    PInfo^.ConvertToDispDouble(Point3,Point.X,Point.Y);
    ClipPoints.Add(Point);
    ClipPolygon(ClipPoints,PInfo^.ClipRect);
    if ClipPoints.Count>0 then begin
      MoveTo(PInfo^.ExtCanvas.WindowDC,Round(ClipPoints[0].X),Round(ClipPoints[0].Y));
      for Cnt:=1 to ClipPoints.Count-1 do LineTo(PInfo^.ExtCanvas.WindowDC,
          Round(ClipPoints[Cnt].X),Round(ClipPoints[Cnt].Y));
    end;
    ClipPoints.Free;
  end;

Procedure TInputText.StartIt
   (
   PInfo           : PPaint;
   Pos             : TDPoint;
   ACapt           : Boolean
   );
  begin
    StartPos:=Pos;
    LastPos:=Pos;
    if Vertical then Angle:=90  {mf}
    else Angle:=0;
    CalculateSize;
    TInput.StartIt(PInfo,Pos,ACapt);
    Rotate:=FALSE;
  end;

Procedure TInputText.MouseMove
   (
   PInfo           : PPaint;
   APos            : TDPoint
   );
  var OldR2        : Integer;
      NewAngle     : Integer;
      DiagAngle    : Real;
  begin
    if IsOn then begin
      PInfo^.BeginPaint(0,Pen,dm_NoPattern,1);
      OldR2:=SetROP2(PInfo^.ExtCanvas.WindowDC,R2_DEFPEN);
      SetBKColor(PInfo^.ExtCanvas.WindowDC,$FFFFFF);
      DrawIt(PInfo);
      LastPos:=APos;
      if Rotate then begin
        DiagAngle:=StartPos.CalculateAngle(LastPos);
        NewAngle:=Trunc((DiagAngle-ArcTan(Height/Width))*Rad2Deg) Mod 360;
        if NewAngle<>Angle then begin
          Angle:=NewAngle;
          CalculateRectangle;
        end;
      end
      else CalculateSize;
      DrawIt(PInfo);
      SetROP2(PInfo^.ExtCanvas.WindowDC,OldR2);
      PInfo^.EndPaint;
    end;
  end;

Function TInputText.GetDisplayAngle: Double;
  begin
    if Height>0 then Result:=(Angle+180) Mod 360
    else Result:=Angle;
    if Vertical then Result:=Result-270;
    if Result<0 then Result:=Result+360;
  end;

Procedure TInputText.GetText( PInfo: PPaint; AItem: PText );
  begin
    Draw(PInfo);
    if Height<0 then AItem^.Pos:=StartPos
    else begin
      AItem^.Pos:=Point1;
      Angle:=(Angle+180) Mod 360;
    end;
    AItem^.Angle:=360-Angle;
//++ Glukhov Bug#114 Build#125 21.11.00
    AItem.CalculateLines;
    AItem.Font.Height:= Round( Abs(Height) / AItem.nLines );
    if AItem.Font.Height < cMinFontHeight
    then AItem.Font.Height:= cMinFontHeight;
//++ Glukhov Bug#468 Build#162 01.07.01
//    AItem.CalculateSize( PInfo );
//    AItem.CalculateClipRect( PInfo );
    AItem.UpdateTextData( PInfo );
//-- Glukhov Bug#468 Build#162 01.07.01
//-- Glukhov Bug#114 Build#125 21.11.00
  end;

Destructor TInputText.Done;
  begin
    Point1.Done;
    Point2.Done;
    Point3.Done;
    TInput.Done;
  end;

Procedure TInputText.InvertRotate;
  begin
    Rotate:=not(Rotate);
  end;

Procedure TInputText.SetNewText
   (
   PInfo           : PPaint;
   ActFont         : PFontData;
   AText           : String
   );
  var AFont        : PFontDes;
  begin
//++ Glukhov Bug#291 Build#150 24.01.01
//    AspectRat:=PInfo^.GetTextRatio(ActFont^,AText);
//    Height:=ActFont^.Height;
    if ActFont.Height < cMinFontHeight
    then  ActFont.Height:= cMinFontHeight;
// YG    Height:= ActFont.Height;
    Height:= ActFont.Height;
    PInfo.GetTextSize( ActFont^, AText, Width, Height );
    if Height < 1  then Height:= 1;
    AspectRat:= Width / Height;
    if AspectRat < 0.001  then AspectRat:= 0.001;
//-- Glukhov Bug#291 Build#150 24.01.01
    FixHeight:=ActFont^.Style and ts_FixHeight<>0;
    AFont:=PInfo^.Fonts^.GetFont(ActFont^.Font);  {mf}
    if (AFont<>NIL) and AFont^.IsVertical then Vertical:=TRUE
    else Vertical:=FALSE;
  end;

Procedure TInputText.SetText
   (
   PInfo           : PPaint;
   AText           : PText
   );
  var Border       : PPoly;
      Point        : TPoint;
      AFont        : PFontDes;
  begin
//++ Glukhov Bug#291 Build#150 24.01.01
//    AspectRat:=PInfo^.GetTextRatio(AText^.Font,AText^.Text^);
//    Height:=AText^.Font.Height;
    Angle:=360-AText^.Angle;
    Height:=AText^.Height;
    Width:=AText^.Width;
    if Height < 1  then Height:= 1;
    AspectRat:= Width / Height;
    if AspectRat < 0.001  then AspectRat:= 0.001;
//-- Glukhov Bug#291 Build#150 24.01.01
    Border:=AText^.GetBorder;
    FixHeight:=AText^.Font.Style and ts_FixHeight<>0;
    AFont:=PInfo^.Fonts^.GetFont(AText^.Font.Font);  {mf}
    if (AFont<>NIL) and AFont^.IsVertical then Vertical:=TRUE
    else Vertical:=FALSE;
    StartPos:=PDPoint(Border^.Data^.At(0))^;
    LastPos:=PDPoint(Border^.Data^.At(2))^;
    PInfo^.ConvertToDisp(LastPos,Point);
    LPToDP(PInfo^.ExtCanvas.WindowDC,Point,1);
    ClientToScreen(PInfo^.HWindow,Point);
    SetCursorPos(Point.X,Point.Y);
    CalculateSize;
    TInput.StartIt(PInfo,StartPos,TRUE);
    Rotate:=FALSE;
  end;

Procedure TInputText.EndIt
   (
   PInfo           : PPaint
   );
  begin
    TInput.EndIt(PInfo);
  end;

Constructor TMovePoly.Init
   (
   ACol            : Word;
   ALine           : Word
   );
  begin
    TInput.Init(ACol,ALine);
    Poly:=NIL;
  end;

Procedure TMovePoly.SetPoly
   (
   APoly           : PPoly
   );
  Procedure DoAll
     (
     Item          : PDPoint
     ); Far;
    begin
      Poly^.InsertPoint(Item^);
    end;
  begin
    if Poly<>NIL then Dispose(Poly,Done);
    Poly:=New(PPoly,Init);
    APoly^.Data^.ForEach(@DoAll);
  end;

Destructor TMovePoly.Done;
  begin
    if Poly<>NIL then Dispose(Poly,Done);
    TInput.Done;
  end;

Procedure TMovePoly.DrawIt
   (
   PInfo           : PPaint
   );
  var XM           : LongInt;

      YM           : LongInt;
      ClipPoints   : TClipPointList;
      Cnt          : Integer;
  begin

    GetMove(XM,YM);
    Poly^.MoveRel(XM,YM);
    Poly^.GetClippedPoints(PInfo,ClipPoints);
    ClipPolyline(ClipPoints,PInfo^.ClipRect);
    if ClipPoints.Count>0 then begin
      MoveTo(PInfo^.ExtCanvas.WindowDC,Round(ClipPoints[0].X),Round(ClipPoints[0].Y));
      for Cnt:=1 to ClipPoints.Count-1 do LineTo(PInfo^.ExtCanvas.WindowDC,
          Round(ClipPoints[Cnt].X),Round(ClipPoints[Cnt].Y));
    end;
    ClipPoints.Free;
    Poly^.MoveRel(-XM,-YM);
  end;

Procedure TMovePoly.GetMove
   (
   var XMove       : LongInt;
   var YMove       : LongInt
   );
  begin
    XMove:=LimitToLong(LastPos.XDist(StartPos));
    YMove:=LimitToLong(LastPos.YDist(StartPos));
  end;

Function TMovePoly.IsEnoughToMove
   (
   PInfo           : PPaint;
   XSize           : Integer;
   YSize           : Integer
   )
   : Boolean;
  var Point1       : TPoint;
      Point2       : TPoint;
  begin
    PInfo^.ConvertToDisp(StartPos,Point1);
    PInfo^.ConvertToDisp(LastPos,Point2);
    IsEnoughToMove:=(Abs(Point1.X-Point2.X)>XSize) or
        (Abs(Point1.Y-Point2.Y)>YSize);
  end;

Constructor TInpOffset.Init
   (
   ACol            : Word;
   ALine           : Word
   );
  begin
    TInput.Init(ACol,ALine);
    Poly:=NIL;
  end;

Procedure TInpOffset.SetPoly
   (
   APoly           : PPoly
   );
  begin
    Poly:=APoly;
  end;

Procedure TInpOffset.DrawIt
   (
   PInfo           : PPaint
   );

  begin
    DrawLine(PInfo,StartPos,LastPos);
  end;

Procedure TInpOffset.MouseMove
   (
   PInfo           : PPaint;
   APos            : TDPoint
   );

  var NewDist      : Double;
      Cut          : TDPoint;
      Cnt          : Integer;
      OldR2        : Integer;
      Angle        : Real;
      p1,p2        : TDPoint;
      gp1,gp2,gp3  : TGRPoint;
  begin
    if (IsOn) and (Poly <> nil) then begin
      PInfo^.BeginPaint(0,Pen,dm_NoPattern,1);
      OldR2:=SetROP2(PInfo^.ExtCanvas.WindowDC,R2_DEFPEN);
      SetBKColor(PInfo^.ExtCanvas.WindowDC,$FFFFFF);
      DrawIt(PInfo);
      AktDist:=MaxLongInt;
      Cut.Init(0,0);
      with Poly^ do for Cnt:=0 to Data^.Count-2 do begin
        if PInfo^.NormalDistance(Data^.At(Cnt),Data^.At(Cnt+1),@APos,NewDist,Cut) and (NewDist<=AktDist) then begin
          StartPos:=Cut;
          if (GetKeyState(VK_SHIFT) < 0) and (PProj(PInfo^.AProj)^.ActualMen = mn_SplitLine) and (Data^.Count > 2) then begin
             p1.Init(TDPoint(Poly^.Data^.At(Max(PoiNum,1))^).x,TDPoint(Poly^.Data^.At(Max(PoiNum,1))^).y);
             p2.Init(TDPoint(Poly^.Data^.At(Min(PoiNum+1,Poly^.Data^.Count-2))^).x,TDPoint(Poly^.Data^.At(Min(PoiNum+1,Poly^.Data^.Count-2))^).y);
             gp1.x:=p1.x;
             gp1.y:=p1.y;
             gp2.x:=p2.x;
             gp2.y:=p2.y;
             gp3.x:=APos.x;
             gp3.y:=APos.y;
             if GrPointDistance(gp1,gp3) < GrPointDistance(gp2,gp3) then begin
                StartPos.Init(TDPoint(Poly^.Data^.At(Max(PoiNum,1))^).x,TDPoint(Poly^.Data^.At(Max(PoiNum,1))^).y);
             end
             else begin
                StartPos.Init(TDPoint(Poly^.Data^.At(Min(PoiNum+1,Poly^.Data^.Count-2))^).x,TDPoint(Poly^.Data^.At(Min(PoiNum+1,Poly^.Data^.Count-2))^).y);
             end;
          end;
          AktDist:=NewDist;
          PoiNum:=Cnt;
        end;
      end;
      if AktDist<MaxLongInt then LastPos:=APos
      else begin
        Cut:=StartPos;
        AktDist:=Cut.Dist(LastPos);
      end;
      if (AktDist>0) then begin
        Angle:=PDPoint(Poly^.Data^.At(PoiNum))^.CalculateAngle(PDPoint(Poly^.Data^.At(PoiNum+1))^)-Pi;
        if Angle=0 then Angle:=1;
        NewDist:=StartPos.XDist(LastPos);
        if NewDist=0 then NewDist:=LastPos.YDist(StartPos);
        AktDist:=Round(AktDist*(Angle/Abs(Angle))*(NewDist/Abs(NewDist)));
      end;
      DrawIt(PInfo);
      SetROP2(PInfo^.ExtCanvas.WindowDC,OldR2);
      PInfo^.EndPaint;
    end;
  end;

Procedure TInpOffset.GetOffset
   (
   var Ofs         : LongInt
   );
  begin
    Ofs:=LimitToLong(AktDist);
  end;

Constructor TInputSymbol.Init
   (
   ACol            : Word;
   ALine           : Word
   );
  begin
    TInput.Init(ACol,ALine);
    Symbol:=NIL;
    SymCopy:=NIL;
  end;

Destructor TInputSymbol.Done;
  begin
    if Symbol<>NIL then Dispose(Symbol,Done);
    if SymCopy<>NIL then Dispose(SymCopy,Done);
    TInput.Done;
  end;

Procedure TInputSymbol.SetSymbol(PInfo:PPaint;ASymbol:PSymbol);
var Point        : TPoint;
begin
  // free previous objects
  if Symbol<>NIL then Dispose(Symbol,Done);
  if SymCopy<>NIL then Dispose(SymCopy,Done);
  // setup input-handlers
  SymCopy:=MakeObjectCopy(ASymbol);
  with SymCopy^ do begin
    SetSymPtr(PInfo^.Symbols);
    CalculateClipRect(PInfo);

    Rotate:=FALSE;
    RotAngle:=0;
    Scaling:=1;
    AktAngle:=CalculateAngle;

    Symbol:=New(PSCPoly,Init);
    GetBorder(PInfo,Symbol);
    LastPos:=PDPoint(Symbol^.Data^.At(3))^;
    PInfo^.ConvertToDisp(LastPos,Point);
    LPToDP(PInfo^.ExtCanvas.WindowDC,Point,1);
    ClientToScreen(PInfo^.HWindow,Point);
    SetCursorPos(Point.X,Point.Y);
    Symbol^.Data^.FreeAll;

    Angle:=0;
    CalculateClipRect(PInfo);
    GetBorder(PInfo,Symbol);

    with ASymbol^,Symbol^.Data^ do begin
      Height1:=LimitToLong(Abs(PDPoint(At(2))^.YDist(Position)));
      Width1:=LimitToLong(Abs(PDPoint(At(2))^.XDist(Position)));
      Height2:=LimitToLong(Abs(PDPoint(At(0))^.YDist(Position)));
      Width2:=LimitToLong(Abs(PDPoint(At(0))^.XDist(Position)));
    end;

    Symbol^.MoveRel(-Position.X,-Position.Y);
    StartIt(PInfo,Position,TRUE);
  end;                        
end;

Procedure TInputSymbol.DrawIt(PInfo:PPaint);
var ClipPoints   : TClipPointList;
    Cnt          : Integer;
begin
    PInfo^.SetScaling(Scaling,AktAngle+RotAngle,StartPos);
    Symbol^.GetClippedPoints(PInfo,ClipPoints);
    ClipPolygon(ClipPoints,PInfo^.ClipRect);
    if ClipPoints.Count>0 then begin
      MoveTo(PInfo^.ExtCanvas.WindowDC,Round(ClipPoints[0].X),Round(ClipPoints[0].Y));
      for Cnt:=1 to ClipPoints.Count-1 do LineTo(PInfo^.ExtCanvas.WindowDC,
          Round(ClipPoints[Cnt].X),Round(ClipPoints[Cnt].Y));
    end;
    ClipPoints.Free;
end;

Procedure TInputSymbol.MouseMove
   (
   PInfo           : PPaint;
   APos            : TDPoint
   );
  var OldR2        : Integer;
  begin
    if IsOn then begin
      PInfo^.BeginPaint(0,Pen,dm_NoPattern,1);
      OldR2:=SetROP2(PInfo^.ExtCanvas.WindowDC,R2_DEFPEN);
      SetBKColor(PInfo^.ExtCanvas.WindowDC,$FFFFFF);
      DrawIt(PInfo);
      LastPos:=APos;
      if Rotate then CalculateAngle
      else CalculateRect;
      DrawIt(PInfo);
      SetROP2(PInfo^.ExtCanvas.WindowDC,OldR2);
      PInfo^.EndPaint;
    end;
  end;

Procedure TInputSymbol.CalculateRect;
  var Angle        : Real;
      XDist        : Real;
      YDist        : Real;
      Scale1       : Real;
      Scale2       : Real;
      Dist         : Real;
      Width        : LongInt;
      Height       : LongInt;
  begin
    Angle:=StartPos.CalculateAngle(LastPos)-AktAngle;
    Dist:=StartPos.Dist(LastPos);
    XDist:=Dist*Cos(Angle);
    YDist:=Dist*Sin(Angle);
    if XDist>0 then Width:=Width1
    else Width:=Width2;
    if YDist>0 then Height:=Height1
    else Height:=Height2;
    if Width<>0 then Scale1:=Abs(XDist/Width)
    else Scale1:=0;
    if Height>0 then Scale2:=Abs(YDist/Height)
    else Scale2:=0;
    if Scale1>Scale2 then Scaling:=Scale2
    else Scaling:=Scale1;
    if Scaling<0.05 then Scaling:=0.05;
 end;

Procedure TInputSymbol.CalculateAngle;
begin
  RotAngle:=StartPos.CalculateAngle(LastPos)-StartAngle;
end;

Procedure TInputSymbol.InvertRotation(Pos:TDPoint);
begin
  Rotate:=not Rotate;
  if Rotate then StartAngle:=StartPos.CalculateAngle(Pos)
  else begin
    AktAngle:=AktAngle+RotAngle;
    RotAngle:=0;
  end;
end;                            

Procedure TInputSymbol.GetSymbol(PInfo:PPaint;ASymbol:PSymbol);
begin
  with ASymbol^ do begin
    if DblCompare(Size,dblFromLayer)>0 then Size:=Size*Scaling
    else begin
      Size:=Scaling;
      SizeType:=stPercent;
    end;
    Angle:=AktAngle+RotAngle;
  end;
  ASymbol^.CalculateClipRect(PInfo);
end;

Procedure TInputSymbol.EndIt
   (
   PInfo           : PPaint
   );
  begin
    TInput.EndIt(PInfo);
  end;

Constructor TInputSpline.Init
   (
   ACol            : Word;
   ALine           : Word;
   BCol            : Word;
   BLine           : Word
   );
  begin
    TInput.Init(ACol,ALine);
    Spline:=NIL;
    Pen2:=GetPen(RGBColors[BCol],BLine,0);
    CurSpline:=-1;
    MovePnt:=-1;
  end;

Procedure TInputSpline.SetSpline
   (
   ASpline         : PSpline
   );
  Procedure DoAll
     (
     Item          : PDPoint
     ); Far;
    begin
      Spline^.InsertPoint(Item^);
    end;
  begin
    if Spline<>NIL then Dispose(Spline,Done);
    Spline:=New(PSpline,Init);
    ASpline^.Data^.ForEach(@DoAll);
    CurSpline:=-1;
    MovePnt:=-1;
  end;

Destructor TInputSpline.Done;
  begin
    if Spline<>NIL then Dispose(Spline,Done);
    DeleteObject(Pen2);
    TInput.Done;
  end;

Procedure TInputSpline.DrawIt
   (
   PInfo           : PPaint
   );
  var Cnt          : Integer;
      Screen       : TDRect;
      APoint       : PDPoint;
  begin
    if Spline<>NIL then with Spline^ do begin
      PInfo^.SetTools(0,Pen2);
      PInfo^.GetCurrentScreen(Screen);
      Draw(PInfo,TRUE);
      for Cnt:=0 to Data^.Count Div 3 do begin
        APoint:=Data^.At(3*Cnt);
        if Screen.PointInside(APoint^) then DrawRect(PInfo,APoint,spFixRect);
      end;
      PInfo^.ResetTools;
      if CurSpline<>-1 then DrawHandles(PInfo);
    end;
  end;

Procedure TInputSpline.DrawHandles
   (
   PInfo           : PPaint
   );
  var APoint       : PDPoint;
  begin
    if (Spline<>NIL) and (CurSpline<>-1) then with Spline^ do begin
      PInfo^.SetTools(0,Pen2);
      if CurSpline*3-1>=0 then DrawRect(PInfo,Data^.At(3*CurSpline-1),spPoints);
      DrawRect(PInfo,Data^.At(3*CurSpline+1),spPoints);
      DrawRect(PInfo,Data^.At(3*CurSpline+2),spPoints);
      if CurSpline*3+4<Data^.Count then DrawRect(PInfo,Data^.At(3*CurSpline+4),spPoints);
      PInfo^.SetTools(0,Pen);
      if CurSpline*3-1>=0 then APoint:=Data^.At(3*CurSpline-1)
      else APoint:=Data^.At(3*CurSpline);
      DrawLine(PInfo,APoint^,PDPoint(Data^.At(3*CurSpline+1))^);
      if CurSpline*3+4<Data^.Count then APoint:=Data^.At(3*CurSpline+4)
      else APoint:=Data^.At(3*CurSpline+3);
      DrawLine(PInfo,PDPoint(Data^.At(3*CurSpline+2))^,APoint^);
    end;
  end;

Procedure TInputSpline.MouseClick
   (
   PInfo           : PPaint;
   DrawPos         : TDPoint
   );
  var Point        : TPoint;
      NewSpline    : Integer;
      Cnt          : Integer;
      Stop         : Integer;
      SRect        : TDRect;
      Found        : Boolean;
      OldR2        : Integer;
  begin
    if Spline = nil then exit;

    SRect.Init;
    SRect.AssignByPoint(DrawPos,PInfo^.CalculateDraw(spFixRect));
    Cnt:=0;
    Found:=FALSE;
    while (Cnt<=Spline^.Data^.Count Div 3) and not(Found) do begin
      Found:=SRect.PointInside(PDPoint(Spline^.Data^.At(3*Cnt))^);
      Inc(Cnt);
    end;
    if Found then begin
      MovePnt:=3*(Cnt-1);
      IsFixPoint:=TRUE;
    end;
    if CurSpline<>-1 then begin
      SRect.AssignByPoint(DrawPos,PInfo^.CalculateDraw(spPoints));
      Found:=FALSE;
      Cnt:=3*CurSpline-1;
      Stop:=3*CurSpline+4;
      if Stop>=Spline^.Data^.Count then Stop:=Spline^.Data^.Count-2;
      if Cnt<0 then Cnt:=0;
      while (Cnt<=Stop) and not(Found) do begin
        if Cnt Mod 3<>0 then Found:=SRect.PointInside(PDPoint(Spline^.Data^.At(Cnt))^);
        Inc(Cnt);
      end;
      if Found then begin
        MovePnt:=Cnt-1;
        IsFixPoint:=FALSE;
      end;
    end;
    if MovePnt=-1 then begin
      PInfo^.ConvertToDisp(DrawPos,Point);
      NewSpline:=Spline^.SearchSpline(PInfo,Point,spSelDist);
      if (NewSpline<>-1) and (NewSpline<>CurSpline) then begin
        PInfo^.BeginPaint(0,Pen,dm_NoPattern,1);
        OldR2:=SetROP2(PInfo^.ExtCanvas.WindowDC,R2_DEFPEN);
        SetBKColor(PInfo^.ExtCanvas.WindowDC,$FFFFFF);
        if CurSpline<>-1 then DrawHandles(PInfo);
        CurSpline:=NewSpline;
        DrawHandles(PInfo);
        SetROP2(PInfo^.ExtCanvas.WindowDC,OldR2);
        PInfo^.EndPaint;
      end;
    end;
    LastPos:=DrawPos;
    StartPos:=DrawPos;
  end;

Procedure TInputSpline.MouseUp;
  begin
    MovePnt:=-1;
  end;

Procedure TInputSpline.DrawSpline
   (
   PInfo           : PPaint;
   ASpline         : Integer
   );
  var Points       : PPoints;
      Result       : PPoints;
      PointCnt     : Integer;
      Cnt          : Integer;
  begin
    Spline^.GetPoints(PInfo,Points);
    PointCnt:=Spline^.GetSegment(@Points^[ASpline*3+1],Result);
    if PointCnt>0 then begin
      MoveTo(PInfo^.ExtCanvas.WindowDC,Result^[1].X,Result^[1].Y);
      for Cnt:=1 to PointCnt do LineTo(PInfo^.ExtCanvas.WindowDC,Result^[Cnt].X,Result^[Cnt].Y);
      FreeMem(Result,PointCnt*SizeOf(TPoint));
    end;
    Spline^.FreePoints(Points);
  end;

Procedure TInputSpline.MouseMove
   (
   PInfo           : PPaint;
   APos            : TDPoint
   );
  var OldR2        : Integer;
      Size         : Integer;
      XMove        : LongInt;
      YMove        : LongInt;
      APoint       : PDPoint;
      Spline1      : Integer;
      Spline2      : Integer;
      BPoint       : PDPoint;
      CPoint       : PDPoint;
      MidPoint     : Integer;
      Dist         : Real;
      Grad         : Real;
  begin
    if MovePnt<>-1 then begin
      PInfo^.BeginPaint(0,Pen2,dm_NoPattern,1);
      OldR2:=SetROP2(PInfo^.ExtCanvas.WindowDC,R2_DEFPEN);
      SetBKColor(PInfo^.ExtCanvas.WindowDC,$FFFFFF);
      if IsFixPoint then MidPoint:=MovePnt
      else begin
        if MovePnt Mod 3=1 then MidPoint:=MovePnt-1
        else MidPoint:=MovePnt+1;
      end;
      Spline1:=MidPoint Div 3-1;
      Spline2:=MidPoint Div 3;
      if Spline2>Spline^.Data^.Count Div 3-1 then Spline2:=-1;
      PInfo^.SetTools(0,Pen);
      if Spline1<>-1 then DrawSpline(PInfo,Spline1);
      if Spline2<>-1 then DrawSpline(PInfo,Spline2);
      if (CurSpline<>-1) and ((Spline1=CurSpline) or (Spline2=CurSpline)) then DrawHandles(PInfo);
      APoint:=Spline^.Data^.At(MovePnt);
      if IsFixPoint then begin
        Size:=spFixRect;
        PInfo^.SetTools(0,Pen2);
        DrawRect(PInfo,APoint,Size);
        XMove:=LimitToLong(APos.XDist(APoint^));
        YMove:=LimitToLong(APos.YDist(APoint^));
        if MovePnt>0 then PDPoint(Spline^.Data^.At(MovePnt-1))^.Move(XMove,YMove);
        if MovePnt<Spline^.Data^.Count-1 then PDPoint(Spline^.Data^.At(MovePnt+1))^.Move(XMove,YMove);
        APoint^.Move(XMove,YMove);
        DrawRect(PInfo,APoint,Size);
      end
      else begin
        if MovePnt Mod 3=1 then begin
          if MovePnt-2>0 then CPoint:=Spline^.Data^.At(MovePnt-2)
          else CPoint:=NIL;
        end
        else if MovePnt+2<Spline^.Data^.Count then CPoint:=Spline^.Data^.At(MovePnt+2)
        else CPoint:=NIL;
        if CPoint<>NIL then begin
          BPoint:=Spline^.Data^.At(MidPoint);
          Dist:=CPoint^.Dist(BPoint^);
          if APos.XDist(BPoint^)=0 then begin
            CPoint^.X:=0;
            if CPoint^.YDist(BPoint^)<0 then CPoint^.Y:=-Trunc(Dist)
            else CPoint^.Y:=Trunc(Dist);
          end
          else begin
            Grad:=APos.YDist(BPoint^)/APos.XDist(BPoint^);
            if APos.XDist(BPoint^)>0 then CPoint^.X:=-Round(Sqrt(Sqr(Dist)/(1+Sqr(Grad))))
            else CPoint^.X:=Round(Sqrt(Sqr(Dist)/(1+Sqr(Grad))));
            CPoint^.Y:=Round(Grad*CPoint^.X);
          end;
          CPoint^.Move(BPoint^.X,BPoint^.Y);
        end;
        APoint^:=APos;
      end;
      if (CurSpline<>-1) and ((Spline1=CurSpline) or (Spline2=CurSpline)) then DrawHandles(PInfo);
      PInfo^.SetTools(0,Pen);
      if Spline1<>-1 then DrawSpline(PInfo,Spline1);
      if Spline2<>-1 then DrawSpline(PInfo,Spline2);
      SetROP2(PInfo^.ExtCanvas.WindowDC,OldR2);
      PInfo^.EndPaint;
    end;
    LastPos:=APos;
  end;

Procedure TInputSpline.GetSpline
   (
   ASpline         : PSpline
   );
  Procedure DoAll
     (
     Item          : PDPoint
     ); Far;
    begin
      ASpline^.Data^.Insert(Item);
    end;
  begin
    ASpline^.Data^.FreeAll;
    Spline^.Data^.ForEach(@DoAll);
    ASPline^.CalculateClipRect;
    Spline^.Data^.DeleteAll;
    Dispose(Spline,Done);
    Spline:=NIL;
  end;

Constructor TInputBox.Init
   (
   ACol            : Word;
   ALine           : Word;
   BCol            : Word;
   BLine           : Word;
   AMode           : Integer
   );
  begin
    TInput.Init(ACol,ALine);
    Pen2:=GetPen(RGBColors[BCol],BLine,0);
    Rect.Init;
    MovePoint:=-1;
    Mode:=AMode;
  end;

Procedure TInputBox.GetPoint
   (
   APoint          : Integer;
   var BPoint      : TDPoint
   );
  begin
    case APoint of
      0 : BPoint.Init(Rect.A.X,Rect.A.Y);
      1 : BPoint.Init(Rect.A.X+LimitToLong(Rect.XSize) Div 2,Rect.A.Y);
      2 : BPoint.Init(Rect.B.X,Rect.A.Y);
      3 : BPoint.Init(Rect.B.X,Rect.A.Y+LimitToLong(Rect.YSize) Div 2);
      4 : BPoint.Init(Rect.B.X,Rect.B.Y);
      5 : BPoint.Init(Rect.B.X-LimitToLong(Rect.XSize) Div 2,Rect.B.Y);
      6 : BPoint.Init(Rect.A.X,Rect.B.Y);
      7 : BPoint.Init(Rect.A.X,Rect.B.Y-LimitToLong(Rect.YSize) Div 2);
    end;
  end;

function TInputBox.GetMode(AMode : Integer):Boolean;
  begin
  GetMode:=(Mode and AMode)=AMode;
  end;

Procedure TInputBox.DrawIt( PInfo: PPaint );
  var Cnt          : Integer;
      APoint       : TDPoint;
  begin
    DrawRectangle(PInfo,Rect);
    if MovePoint=-1 then
    begin
      PInfo^.SetTools(0,Pen2);
//++ Glukhov Bug#170 BUILD#125 08.11.00
{ Old code
      Cnt:=0;
      while Cnt<8 do
      begin
        GetPoint(Cnt,APoint);
        DrawRect(PInfo,@APoint,ibRect);
        if GetMode(m_FixSize) then Inc(Cnt);
        Inc(Cnt);
      end
}
      for Cnt:=0 to 7 do begin
        GetPoint( Cnt, APoint );
        if GetMode( m_FixSize ) then begin
        // Draw rectangle for the 6th point only
          if Cnt = 6 then DrawRect( PInfo, @APoint, ibRect );
        end else
        if GetMode( m_CornersOnly ) then begin
        // Draw rectangle for the corner (even) points only
          if (Cnt mod 2) = 0 then DrawRect( PInfo, @APoint, ibRect );
        end else  begin
        // Draw every rectangle
          DrawRect( PInfo, @APoint, ibRect );
        end;
      end;
//-- Glukhov Bug#170 BUILD#125 08.11.00
    end;
  end;

Procedure TInputBox.MouseClick( PInfo: PPaint; DrawPos: TDPoint );
  var InRect       : TDRect;
      APoint       : TDPoint;
      Found        : Boolean;
      Cnt          : Integer;
  begin
    InRect.Init;
    InRect.AssignByPoint(DrawPos,PInfo^.CalculateDraw(ibRect));
    Cnt:=0;
    Found:=FALSE;
//++ Glukhov Bug#170 BUILD#125 08.11.00
    if GetMode( m_FixSize ) then begin
      GetPoint( 6, APoint );
      Found:= InRect.PointInside( APoint );
      if Found then begin
        Draw( PInfo );
        MovePoint:= 6;
        StartPos:= DrawPos;
        Draw( PInfo );
        Exit;
      end;
    end else
//-- Glukhov Bug#170 BUILD#125 08.11.00
    while (Cnt<=7) and not(Found) do begin
      GetPoint(Cnt,APoint);
      Found:=InRect.PointInside(APoint);
//++ Glukhov Bug#170 BUILD#125 08.11.00
//      if GetMode(m_FixSize) then Inc(Cnt,2)
      if GetMode( m_CornersOnly ) then Inc(Cnt,2)
//-- Glukhov Bug#170 BUILD#125 08.11.00
                                  else Inc(Cnt);
    end;
    if Found then begin
      Draw(PInfo);
//++ Glukhov Bug#170 BUILD#125 08.11.00
//      if GetMode(m_FixSize) then MovePoint:=Cnt-2
      if GetMode( m_CornersOnly ) then MovePoint:= Cnt-2
//-- Glukhov Bug#170 BUILD#125 08.11.00
                                  else MovePoint:= Cnt-1;
      Draw(PInfo);
      case MovePoint of
        0 : GetPoint(4,StartPos);
        2 : GetPoint(6,StartPos);
        4 : GetPoint(0,StartPos);
        6 : GetPoint(2,StartPos);
        else GetPoint(MovePoint,StartPos);
      end;
      StartWidth:=LimitToLong(Rect.XSize);
      StartHeight:=LimitToLong(Rect.YSize);
//++ Glukhov Bug#170 BUILD#125 08.11.00
//      if GetMode(m_FixSize) then StartPos:=DrawPos;
//-- Glukhov Bug#170 BUILD#125 08.11.00
    end
    else if Rect.PointInside(DrawPos) then begin
      Draw(PInfo);
      MovePoint:=-2;
      StartPos:=DrawPos;
      Draw(PInfo);
    end
    else MovePoint:=-1;
  end;

Procedure TInputBox.SetRect( ARect: TDRect );
  begin
    Rect:=ARect;
    MovePoint:=-1;
  end;

Procedure TInputBox.MouseUp;
  begin
    if MovePoint<>-1 then begin
      Draw(PInfo);
      MovePoint:=-1;
      Draw(PInfo);
    end
    else MovePoint:=-1;
  end;

Procedure TInputBox.MouseMove( PInfo: PPaint; APos: TDPoint );
  var Scale1       : Real;
      Scale2       : Real;
  begin
    if MovePoint=-2 then begin
      Draw(PInfo);
      Rect.MoveRel(LimitToLong(APos.XDist(StartPos)),LimitToLong(APos.YDist(StartPos)));
      Draw(PInfo);
      StartPos:=APos;
    end
    else if MovePoint<>-1 then begin
      Draw(PInfo);
      Scale1:=Abs(APos.XDist(StartPos)/StartWidth);
      Scale2:=Abs(APos.YDist(StartPos)/StartHeight);
      if not GetMode(m_NoAspect) then
        if Scale2<Scale1 then Scale1:=Scale2
                         else Scale2:=Scale1;
      case MovePoint of
        0 : if GetMode(m_FixSize) then   {fixe Gr��e -> Rechteck verschieben }
              begin
              Rect.MoveRel(LimitToLong(APos.XDist(StartPos)),LimitToLong(APos.YDist(StartPos)));
              StartPos:=APos;
              end
            else
              begin
              Rect.A.X:=Trunc(StartPos.X+Sign(APos.XDist(StartPos))*StartWidth*Scale1);
              Rect.A.Y:=Trunc(StartPos.Y+Sign(APos.YDist(StartPos))*StartHeight*Scale2);
              end;
        1 : Rect.A.Y:=APos.Y;
        2 : if GetMode(m_FixSize) then   {fixe Gr��e -> Rechteck verschieben }
              begin
              Rect.MoveRel(LimitToLong(APos.XDist(StartPos)),LimitToLong(APos.YDist(StartPos)));
              StartPos:=APos;
              end
            else
              begin
              Rect.B.X:=Trunc(StartPos.X+Sign(APos.XDist(StartPos))*StartWidth*Scale1);
              Rect.A.Y:=Trunc(StartPos.Y+Sign(APos.YDist(StartPos))*StartHeight*Scale2);
              end;
        3 : Rect.B.X:=APos.X;
        4 : if GetMode(m_FixSize) then   {fixe Gr��e -> Rechteck verschieben }
              begin
              Rect.MoveRel(LimitToLong(APos.XDist(StartPos)),LimitToLong(APos.YDist(StartPos)));
              StartPos:=APos;
              end
            else
              begin
              Rect.B.X:=Trunc(StartPos.X+Sign(APos.XDist(StartPos))*StartWidth*Scale1);
              Rect.B.Y:=Trunc(StartPos.Y+Sign(APos.YDist(StartPos))*StartHeight*Scale2);
              end;
        5 : Rect.B.Y:=APos.Y;
        6 : if GetMode(m_FixSize) then   {fixe Gr��e -> Rechteck verschieben }
              begin
              Rect.MoveRel(LimitToLong(APos.XDist(StartPos)),LimitToLong(APos.YDist(StartPos)));
              StartPos:=APos;
              end
            else
              begin
              Rect.A.X:=Trunc(StartPos.X+Sign(APos.XDist(StartPos))*StartWidth*Scale1);
              Rect.B.Y:=Trunc(StartPos.Y+Sign(APos.YDist(StartPos))*StartHeight*Scale2);
              end;
        7 : Rect.A.X:=APos.X;
      end;
      Draw(PInfo);
    end;
  end;

Procedure TInputBox.GetRect( var ARect: TDRect );
  begin
    ARect.Init;
    ARect.CorrectByPoint(Rect.A);
    ARect.CorrectByPoint(Rect.B);
  end;

Destructor TInputBox.Done;
  begin
    DeleteObject(Pen2);
    TInput.Done;
  end;

//++ Glukhov Bug#264 Build#161 15.06.01
//---------
// Object TEditTxtRect was created to rotate a text rectangle
// (Object TInputBox allows to move and resize it)
//---------
Constructor TEditTxtRect.Init( ACol, ALine, BCol, BLine: Word; AMode: Integer );
  begin
    TInputBox.Init( ACol, ALine, BCol, BLine, AMode );
    TxtPos.Init(0,0);
    Point1.Init(0,0);
    Point2.Init(0,0);
    Point3.Init(0,0);
    RotPos.Init(0,0);
    bRotate:= False;
    bTxtRect:= False;
    Angle:= 0;
    Height:= cMinFontHeight;
    Width:= cMinFontHeight;
  end;

Destructor TEditTxtRect.Done;
  begin
    TxtPos.Done;
    Point1.Done;
    Point2.Done;
    Point3.Done;
    RotPos.Done;
    TInputBox.Done;
  end;

// Calculates the Point1-3
Procedure TEditTxtRect.CalcPoints;
  var
    Beta    : Double;
    CosL    : Double;
    SinL    : Double;
  begin
    Beta:= -Angle * Pi / 180.0;   // Set the regular orientation
    CosL:= Cos(Beta);
    SinL:= Sin(Beta);
    Point1:= TxtPos;
    Point1.Move( Round(SinL*Height), Round(-CosL*Height) );
    Point3:= TxtPos;
    Point3.Move( Round(CosL*Width), Round(SinL*Width) );
    Point2.X:= Point1.X + Point3.X - TxtPos.X;
    Point2.Y:= Point1.Y + Point3.Y - TxtPos.Y;
//    RotPos.X:= (Point2.X + Point3.X) div 2;
//    RotPos.Y:= (Point2.Y + Point3.Y) div 2;
    RotPos.X:= (TxtPos.X + Point3.X) div 2;
    RotPos.Y:= (TxtPos.Y + Point3.Y) div 2;
  end;

Procedure TEditTxtRect.ReCalcRect;
  begin
    Rect.Init;
    Rect.CorrectByPoint( TxtPos );
    Rect.CorrectByPoint( Point1 );
    Rect.CorrectByPoint( Point2 );
    Rect.CorrectByPoint( Point3 );
  end;

Procedure TEditTxtRect.SetTxtRect(
  ARect: TDRect; APos: TDPoint; AAngle: Integer; AHeight, AWidth: LongInt );
  begin
    TInputBox.SetRect( ARect );
    TxtPos:= APos;
    Angle:= AAngle;
    Height:= AHeight;
    Width:= AWidth;
    bRotate:= False;
    CalcPoints;
    bTxtRect:= False;
  end;

Procedure TEditTxtRect.DrawIt( PInfo: PPaint );
  var
    Pt1, Pt2, Pt3 : TPoint;
    bOrtho        : Boolean;
    APoint        : TDPoint;
  begin
    // Draw InputBox (ClipRect with tags)
    TInputBox.DrawIt( PInfo );

    // Check the rotation
    PInfo.ConvertToDisp( TxtPos, Pt1 );
    PInfo.ConvertToDisp( Rect.A, Pt2 );
    PInfo.ConvertToDisp( Rect.B, Pt3 );
    if ((Pt1.X = Pt2.X) and (Pt1.Y <> Pt2.Y) and (Pt1.Y <> Pt3.Y))
    or ((Pt1.X = Pt3.X) and (Pt1.Y <> Pt2.Y) and (Pt1.Y <> Pt3.Y))
    or ((Pt1.Y = Pt2.Y) and (Pt1.X <> Pt2.X) and (Pt1.X <> Pt3.X))
    or ((Pt1.Y = Pt3.Y) and (Pt1.X <> Pt2.X) and (Pt1.X <> Pt3.X))
    then  bOrtho:= False
    else  bOrtho:= True;

    // Draw the text rectangle, if rotated
    if not bOrtho  then begin
      PInfo^.SetTools(0,Pen);
      DrawLine( PInfo, TxtPos, Point1 );
      DrawLine( PInfo, Point1, Point2 );
      DrawLine( PInfo, Point2, Point3 );
      DrawLine( PInfo, Point3, TxtPos );
      PInfo^.SetTools(0,Pen2);
    end;

    // Draw the rotation tags, if needed
    if GetMode( m_Rotation ) then begin
      DrawBox( PInfo, @TxtPos, ibRect div 2 );
      DrawBox( PInfo, @TxtPos, ibRect div 3 );
      DrawBox( PInfo, @TxtPos, ibRect div 4 );
      DrawBox( PInfo, @RotPos, ibRect );
      DrawBox( PInfo, @RotPos, ibRect div 2 );
      DrawBox( PInfo, @RotPos, ibRect div 3 );
      DrawBox( PInfo, @RotPos, ibRect div 4 );
    end;
  end;

Procedure TEditTxtRect.MouseClick( PInfo: PPaint; DrawPos: TDPoint );
  var InRect      : TDRect;
      APoint      : TDPoint;
      Found       : Boolean;
  begin
    InRect.Init;
    InRect.AssignByPoint( DrawPos, PInfo^.CalculateDraw(ibRect) );
    Found:= False;
    if GetMode( m_Rotation ) then begin
      Found:= InRect.PointInside( RotPos );
      if Found then begin
        Draw( PInfo );
        bRotate:= True;
        Draw( PInfo );
        Exit;
      end;
    end;
    TInputBox.MouseClick( PInfo, DrawPos );
  end;

Procedure TEditTxtRect.MouseMove( PInfo: PPaint; APos: TDPoint );
  var OldRect     : TDRect;
      iX, iY      : Integer;
      Ratio       : Double;
  begin
    if bRotate then begin
      Draw(PInfo);
      Angle:= Round( -TxtPos.CalculateAngle(APos) * 180 / PI );
      CalcPoints;
      ReCalcRect;
      Draw(PInfo);
      Exit;
    end;

    if MovePoint = -1 then Exit;
    OldRect.InitByRect( Rect );
    TInputBox.MouseMove( PInfo, APos );

    if not OldRect.IsEqual(Rect) then begin
      // The movement or resizing of a text
      Draw(PInfo);

      // Adjust the text data for the new rectangle
      iY:= Round( Rect.YSize );
      if iY < cMinFontHeight then iY:= cMinFontHeight;
      Ratio:= iY / OldRect.YSize;
      Width:= Round( Ratio * Width );
      Height:= Round( Ratio * Height );

      // Move rectangle to match the left-up corners (the text position is moved also)
      iX:= Rect.A.X - OldRect.A.X;
      iY:= Rect.B.Y - OldRect.B.Y;
      OldRect.MoveRel( iX, iY );
      TxtPos.Move( iX, iY );
      // Scale the text position
      TxtPos.Move(
              Round((Ratio - 1) * (TxtPos.X - OldRect.A.X)),
              Round((Ratio - 1) * (TxtPos.Y - OldRect.B.Y)));

      // Recalculate the other text data
      CalcPoints;
      ReCalcRect;

      // Draw the new state
      Draw(PInfo);
    end;
  end;

Procedure TEditTxtRect.MouseUp( PInfo: PPaint );
  begin
    if bRotate then begin
      Draw(PInfo);
      bRotate:= False;
      Draw( PInfo );
    end
    else TInputBox.MouseUp( PInfo );
  end;
//-- Glukhov Bug#264 Build#161 15.06.01

{**************************************************************************************}
{ Procedure TNewPoly.CopyPoints                                                        }
{  H�ngt alle Punkte von APoly an sich selbst an. Ist AReverse TRUE, so werden die     }
{  Punkte von APoly in umgekehrter Reihenfolge kopiert.                                }
{**************************************************************************************}
Procedure TNewPoly.CopyPoints
   (
   PInfo           : PPaint;
   APoly           : PPoly;
   AReverse        : Boolean
   );
  Function DoCopyPoints
     (
     Item          : PDPoint
     )
     : Boolean; Far;
    begin
      PointInsert(PInfo,Item^,TRUE);
      DoCopyPoints:=FALSE;
    end;
  begin
    if AReverse then APoly^.Data^.LastThat(@DoCopyPoints)
    else APoly^.Data^.FirstThat(@DoCopyPoints);
  end;

Constructor TMoveCircle.Init
   (
   ACol            : Word;
   ALine           : Word
   );
  begin
    TInput.Init(ACol,ALine);
    Circle:=NIL;
  end;

Procedure TMoveCircle.SetCircle
   (
   ACircle         : PEllipse
   );
  begin
    if Circle<>NIL then Dispose(Circle,Done);
    Circle:=New(PEllipse,Init(ACircle^.Position,ACircle^.PrimaryAxis,ACircle^.SecondaryAxis,0));
  end;

Destructor TMoveCircle.Done;
  begin
    if Circle<>NIL then Dispose(Circle,Done);
    TInput.Done;
  end;

Procedure TMoveCircle.DrawIt
   (
   PInfo           : PPaint
   );
  var XM           : LongInt;
      YM           : LongInt;
  begin
    PInfo^.RepaintArea.Assign(-MaxLongInt,-MaxLongInt,MaxLongInt,MaxLongInt);
    GetMove(XM,YM);
    Circle^.MoveRel(XM,YM);
    Circle^.Draw(PInfo,Circle^.NeedClipping(PInfo));
    Circle^.MoveRel(-XM,-YM);
  end;

Procedure TMoveCircle.GetMove
   (
   var XMove       : LongInt;
   var YMove       : LongInt
   );
  begin
    XMove:=LimitToLong(LastPos.XDist(StartPos));
    YMove:=LimitToLong(LastPos.YDist(StartPos));
  end;

Function TMoveCircle.IsEnoughToMove
   (
   PInfo           : PPaint;
   XSize           : Integer;
   YSize           : Integer
   )
   : Boolean;
  var Point1       : TPoint;
      Point2       : TPoint;
  begin
    PInfo^.ConvertToDisp(StartPos,Point1);
    PInfo^.ConvertToDisp(LastPos,Point2);
    IsEnoughToMove:=(Abs(Point1.X-Point2.X)>XSize) or
        (Abs(Point1.Y-Point2.Y)>YSize);
  end;

Constructor TMoveMulti.Init
   (
   ACol            : Word;
   ALine           : Word
   );
  begin
    TInput.Init(ACol,ALine);
    MultiBord:=New(PCollection,Init(10,5));
  end;

Procedure TMoveMulti.SetMulti;
  begin
    if MultiBord^.Count > 0 then MultiBord^.FreeAll;
  end;

Procedure TMoveMulti.Add
   (
   AItem           : PView
   );
  begin
    MultiBord^.Insert(AItem);
  end;

Destructor TMoveMulti.Done;
  begin
    Dispose(MultiBord,Done);
    TInput.Done;
  end;

Procedure TMoveMulti.DrawIt
   (
   PInfo           : PPaint
   );
  var XM           : LongInt;
      YM           : LongInt;

  Procedure DrawAll
     (
     Item        : PView
     ); Far;
    begin
      Item^.MoveRel(XM,YM);
      Item^.Draw(PInfo,Item^.NeedClipping(PInfo));
      Item^.MoveRel(-XM,-YM);
    end;

  begin
    PInfo^.RepaintArea.Assign(-MaxLongInt,-MaxLongInt,MaxLongInt,MaxLongInt);
    GetMove(XM,YM);
    MultiBord^.ForEach(@DrawAll);
  end;

Procedure TMoveMulti.GetMove
   (
   var XMove       : LongInt;
   var YMove       : LongInt
   );
  begin
    XMove:=LimitToLong(LastPos.XDist(StartPos));
    YMove:=LimitToLong(LastPos.YDist(StartPos));
  end;

Function TMoveMulti.IsEnoughToMove
   (
   PInfo           : PPaint;
   XSize           : Integer;
   YSize           : Integer
   )
   : Boolean;
  var Point1       : TPoint;
      Point2       : TPoint;
  begin
    PInfo^.ConvertToDisp(StartPos,Point1);
    PInfo^.ConvertToDisp(LastPos,Point2);
    IsEnoughToMove:=(Abs(Point1.X-Point2.X)>XSize) or
        (Abs(Point1.Y-Point2.Y)>YSize);
  end;

Procedure TInputLine.MoveIt
   (
   PInfo           : PPaint;
   var Start       : TDPoint;
   var EndP        : TDPoint
   );
  var OldR2        : Integer;
  begin
    if IsOn then begin
      PInfo^.BeginPaint(0,Pen,dm_NoPattern,1);
      OldR2:=SetROP2(PInfo^.ExtCanvas.WindowDC,R2_DEFPEN);
      SetBKColor(PInfo^.ExtCanvas.WindowDC,$FFFFFF);
      DrawIt(PInfo);
      StartPos:=Start;
      LastPos:=EndP;
      DrawIt(PInfo);
      SetROP2(PInfo^.ExtCanvas.WindowDC,OldR2);
      PInfo^.EndPaint;
    end;
  end;

Constructor TInfoPoly.Init
   (
   ACol            : Word;
   ALine           : Word
   );
  begin
    TInput.Init(ACol,ALine);
    Poly:=NIL;
  end;

Destructor TInfoPoly.Done;
  begin
    if Poly<>NIL then Dispose(Poly,Done);
    TInput.Done;
  end;

Procedure TInfoPoly.SetPoly
   (
   APoly           : PPoly
   );
  Procedure DoAll
     (
     Item          : PDPoint
     ); Far;
    begin
      Poly^.InsertPoint(Item^);
    end;
  begin
    if Poly<>NIL then Dispose(Poly,Done);
    Poly:=New(PPoly,Init);
    APoly^.Data^.ForEach(@DoAll);
  end;

Procedure TInfoPoly.DrawIt
   (
   PInfo           : PPaint
   );
  var ClipPoints   : TClipPointList;
      Cnt          : Integer;
  begin
    Poly^.GetClippedPoints(PInfo,ClipPoints);
    ClipPolyline(ClipPoints,PInfo^.ClipRect);
    if ClipPoints.Count>0 then begin
      MoveTo(PInfo^.ExtCanvas.WindowDC,Round(ClipPoints[0].X),Round(ClipPoints[0].Y));
      for Cnt:=1 to ClipPoints.Count-1 do LineTo(PInfo^.ExtCanvas.WindowDC,
          Round(ClipPoints[Cnt].X),Round(ClipPoints[Cnt].Y));
    end;
    ClipPoints.Free;
  end;

{++ Ivanoff BUG#378}
{ This function returns the angle (in degrees) between north (vertical up)
  direction and the line that is constructuion through 2 last points of
  the measure line. I mean the last and the next to last point. }
Function TNewPoly.GetAngle: Double;
Var
   APoint: PDPoint;
   AStPoint, AnEndPoint: TGRPoint;
Begin
     RESULT := 0;
     If SELF.Data.Count < 2 Then EXIT;
     // The first point.
     APoint := SELF.Data.At(SELF.Data.Count-2);
     AStPoint.X := APoint.X;
     AStPoint.Y := APoint.Y;
     // The second point.
     APoint := SELF.Data.At(SELF.Data.Count-1);
     AnEndPoint.X := APoint.X;
     AnEndPoint.Y := APoint.Y;
     // Calculate the angle from north (vertical up) direction.
     // The result will be returned in radians.
     RESULT := LineAngle(AStPoint, AnEndPoint);
     // The function can return the result as negative value for right-down field of coordinate axes.
     If RESULT < 0 Then RESULT := 2*PI + RESULT;
     // Count the result in degrees instead of radians. The result is in degrees from horizontal rigth line
     // by counter-clockwise direction.
     RESULT := RESULT  / (PI/180);
     // Count the result as degrees from vertical up line by clockwise direction.
     RESULT := (360 + 90) - RESULT;
     // The result can exceed 360 degrees. Make it right.
     If RESULT > 360 Then RESULT := RESULT - 360;
End;
{-- Ivanoff BUG#378}

end.
