{******************************************************************************+
  Module XFills
--------------------------------------------------------------------------------
  Author: Martin Forst
--------------------------------------------------------------------------------
  XFill definitions and lists used by WinGIS.
--------------------------------------------------------------------------------
  Todo:
  - Error-checking and -handling for the file-interface-Functions
+******************************************************************************}

Unit XFills;

Interface

Uses WinProcs,WinTypes,Classes,Graphics,Hash,IniFiles,RegDB,XStyles,AM_Def;

Type TFillType     = (xftBitmap, xftVector);
     TFillTypes    = Set of TFillType;    

     TCustomXFill  = Class(TPersistent)
      Private
       FGeneralize      : Boolean;
       FGeneralizePrint : Boolean;
       FGeneralizeScale : Double;
       FGeneralizeStyle : TFillStyle;
       FName            : String;
       FNumber          : Integer;
       FRefCount        : Integer;
       FType            : TFillType;
       Procedure   SetRefCount(const Value: Integer);
      Protected
       Procedure   AssignTo(Dest:TPersistent); override;
      Public
       Constructor Create; virtual;
       Procedure   AddRef;
       Property    FillType:TFillType read FType write FType;
       Property    Generalize:Boolean read FGeneralize write FGeneralize;
       Property    GeneralizeWhenPrinting:Boolean read FGeneralizePrint write FGeneralizePrint; 
       Property    GeneralizeScale:Double read FGeneralizeScale write FGeneralizeScale;
       Property    GeneralizeStyle:TFillStyle read FGeneralizeStyle write FGeneralizeStyle;
       Property    Name:String read FName write FName;
       Property    Number:Integer read FNumber write FNumber;
       Property    ReferenceCount:Integer read FRefCount write SetRefCount;
       Procedure   ReadFromFile(IniFile:TIniFile;const Key:String); virtual;
       Procedure   ReadFromRegistry(Registry:TCustomRegistry;Const KeyName:String); virtual;
       Procedure   Release;
       Procedure   WriteToFile(IniFile:TIniFile;const Key:String); virtual;
       Procedure   WriteToRegistry(Registry:TCustomRegistry;Const KeyName:String); virtual;
     end;

     TCustomXFillClass  = Class of TCustomXFill;

     TVectorXFill  = Class(TCustomXFill)
      Private
       FData       : PXFillStyleDef;
       Function    GetClipRect:TRect;
       Function    GetCount:Integer;
       Function    GetItem(AIndex:Integer):PXFillDef;
       Function    GetMinDistance:Integer;
       Procedure   SetCapacity(ACapacity:Integer);
      Protected
       Procedure   AssignTo(Dest:TPersistent); override;
      Public
       Constructor Create; override;
       Destructor  Destroy; override;
       Procedure   Add(var Item:TXFillDef);
       Procedure   Clear;
       Property    ClipRect:TRect read GetClipRect;
       Property    Count:Integer read GetCount;
       Procedure   Delete(AIndex:Integer);
       Procedure   Exchange(Index1,Index2:Integer);
       Function    IndexOf(Item:PXFillDef):Integer;
       Property    Items[AIndex:Integer]:PXFillDef read GetItem; default;
       Property    MinDistance:Integer read GetMinDistance;
       Procedure   Move(FromIndex,ToIndex:Integer);
       Procedure   ReadFromFile(IniFile:TIniFile;Const KeyName:String); override;
       Procedure   ReadFromRegistry(Registry:TCustomRegistry;Const KeyName:String); override;
       Property    StyleDef:PXFillStyleDef read FData;
       Procedure   WriteToFile(IniFile:TIniFile;Const KeyName:String); override;
       Procedure   WriteToRegistry(Registry:TCustomRegistry;Const KeyName:String); override;
     end;

     TBitmapXFill  = Class(TCustomXFill)
      Private
       FDIBBitmap  : TBitmap;
       FBitmapXFill: TBitmapXFillDef;
       FDC         : HDC;
       Procedure   FreeDDB;
       Procedure   SetDIBBitmap(const Value: TBitmap);
      Protected
       Procedure   AssignTo(Dest:TPersistent); override;
      Public
       Constructor Create; override;
       Destructor  Destroy; override;
       Property    DIBBitmap:TBitmap read FDIBBitmap write SetDIBBitmap;
       Procedure   ReadFromFile(IniFile:TIniFile;Const KeyName:String); override;
       Procedure   ReadFromRegistry(Registry:TCustomRegistry;Const KeyName:String); override;
       Function    StyleDef(DC:HDC):PBitmapXFillDef;
       Procedure   WriteToFile(IniFile:TIniFile;Const KeyName:String); override;
       Procedure   WriteToRegistry(Registry:TCustomRegistry;Const KeyName:String); override;
     end;

     {**************************************************************************
     | Class TCustomXFill
     |--------------------------------------------------------------------------
     |--------------------------------------------------------------------------
     +*************************************************************************}
     TXFillStyleList    = Class(TAutoHashTable)
      Private
       FLastNumber : Integer;
       Function    GetStyleFromNumber(ANumber:Integer):TCustomXFill;
       Function    GetStyleFromName(const AName:String):TCustomXFill;
       Function    GetStyle(AIndex:Integer):TCustomXFill;
      Protected
       Function    KeyOf(Item:Pointer):Pointer; override;
       Function    OnCopyItem(Item:Pointer):Pointer; override;
       Function    OnCompareItems(Item1,Item2:Pointer):Boolean; override;
       Function    OnGetHashValue(Item:Pointer):LongInt; override;
       Procedure   AssignTo(Dest:TPersistent); override;
      Public
       Constructor Create;
       Function    Add(Item:Pointer):Boolean; override;
       Procedure   AddRef(Style:Integer);
       Property    LastNumber:Integer read FLastNumber write FLastNumber;
       Property    NumberedStyles[ANumber:Integer]:TCustomXFill read GetStyleFromNumber; default;
       Property    NamedStyles[const AName:String]:TCustomXFill read GetStyleFromName;
       Procedure   ReadFromFile(Const FileName:String);
       Procedure   ImportFromFile(const FileName:String);
       Function    GetNumberByName(const StyleName:string):integer;
       Function    GetNameByNumber(StyleNumber:integer):string;
       Procedure   ReadFromRegistry(Registry:TCustomRegistry;Const KeyName:String);
       Procedure   Relase(Style:Integer);
       Property    Styles[AIndex:Integer]:TCustomXFill read GetStyle;
       Procedure   WriteToFile(Const FileName:String);
       Procedure   WriteToRegistry(Registry:TCustomRegistry;Const KeyName:String);
     end;

Function IdentToXFillType(const Ident:String):TFillType;

Function XFillTypeToIdent(FillType:TFillType):String;

Implementation

Uses NumTools,StyleDef,StrTools,SysUtils,XLines;

Const pt_UserDefined    = 8;      { number of first user-defined style         }
      clFromObject      = -2;

{==============================================================================+
  TCustomXFill
+==============================================================================}

Constructor TCustomXFill.Create;
begin
  inherited Create;
  FGeneralizeStyle:=DefaultFillStyle;
end;

Procedure TCustomXFill.SetRefCount(const Value: Integer);
begin
  FRefCount:=Value;
  if FRefCount<0 then FRefCount:=0;
end;

Procedure TCustomXFill.ReadFromRegistry(Registry:TCustomRegistry;Const KeyName:String);
begin
  with Registry do begin
    OpenKey(KeyName,TRUE);
    FGeneralize:=ReadBool('Generalize');
    FGeneralizePrint:=ReadBool('GeneralizeWhenPrinting');
    FGeneralizeScale:=ReadFloat('GeneralizeScale');
    if LastError<>rdbeNoError then FGeneralizeScale:=1;
    ReadBinary('GeneralizeStyle',FGeneralizeStyle,SizeOf(FGeneralizeStyle));
    FNumber:=ReadInteger('Number');
    FName:=ReadString('Name');
    FRefCount:=ReadInteger('ReferenceCount');
    FType:=TFillType(ReadInteger('Type'));
  end;
end;

Procedure TCustomXFill.WriteToRegistry(Registry:TCustomRegistry;Const KeyName:String);
begin
  with Registry do begin
    OpenKey(KeyName,TRUE);
    WriteBool('Generalize',FGeneralize);
    WriteBool('GeneralizeWhenPrinting',FGeneralizePrint);
    WriteFloat('GeneralizeScale',FGeneralizeScale);
    WriteBinary('GeneralizeStyle',FGeneralizeStyle,SizeOf(FGeneralizeStyle));
    WriteInteger('Number',FNumber);
    WriteString('Name',FName);
    WriteInteger('ReferenceCount',FRefCount);
    WriteInteger('Type',Integer(FType));
  end;
end;

Procedure TCustomXFill.ReadFromFile(IniFile:TIniFile;const Key:String);
var ColorName      : String;
begin
  with IniFile do begin
    FNumber:=0;
    FName:=ReadString(Key,'Name','');
    FType:=IdentToXFillType(ReadString(Key,'Type','Vector'));
    FGeneralize:=ReadBool(Key,'Generalize',FALSE);
    FGeneralizePrint:=ReadBool(Key,'GeneralizeWhenPrinting',FALSE);
    FGeneralizeScale:=ReadFloat(Key,'GeneralizeScale',1);
    with FGeneralizeStyle do begin
      Pattern:=ReadInteger(Key,'Generalize.Pattern',pt_NoPattern);
      ColorName:=ReadString(Key,'Generalize.ForeColor','Layer');
      if AnsiCompareText(ColorName,'None')=0 then ForeColor:=clNone
      else if AnsiCompareText(ColorName,'Layer')=0 then ForeColor:=clFromLayer
      else ScanString('%i',ColorName,[@ForeColor]);
      ColorName:=ReadString(Key,'Generalize.BackColor','Layer');
      if AnsiCompareText(ColorName,'None')=0 then BackColor:=clNone
      else if AnsiCompareText(ColorName,'Layer')=0 then BackColor:=clFromLayer
      else ScanString('%i',ColorName,[@BackColor]);
    end;
  end;
end;

Procedure TCustomXFill.WriteToFile(IniFile:TIniFile;const Key:String);
begin
  with IniFile do begin
    EraseSection(Key);
    WriteString(Key,'Name',FName);
    WriteString(Key,'Type',XFillTypeToIdent(FType));
    WriteBool(Key,'Generalize',FGeneralize);
    WriteBool(Key,'GeneralizeWhenPrinting',FGeneralizePrint);
    WriteFloat(Key,'GeneralizeScale',FGeneralizeScale);
    with FGeneralizeStyle do begin
      WriteInteger(Key,'Generalize.Pattern',Pattern);
      if ForeColor=-1 then WriteString(Key,'Generalize.ForeColor','None')
      else if ForeColor<-1 then WriteString(Key,'Generalize.ForeColor','Layer')
      else WriteString(Key,'Generalize.ForeColor',Format('0x%.6x',[ForeColor]));
      if BackColor=-1 then WriteString(Key,'Generalize.BackColor','None')
      else if BackColor<-1 then WriteString(Key,'Generalize.BackColor','Layer')
      else WriteString(Key,'Generalize.BackColor',Format('0x%.6x',[BackColor]));
    end;
  end;
end;

Procedure TCustomXFill.AssignTo(Dest:TPersistent);
begin
  with Dest as TCustomXFill do begin
    FGeneralize:=Self.FGeneralize;
    FGeneralizePrint:=Self.FGeneralizePrint;
    FGeneralizeScale:=Self.FGeneralizeScale;
    FGeneralizeStyle:=Self.FGeneralizeStyle;
    FName:=Self.FName;
    FNumber:=Self.FNumber;
    FType:=Self.FType;
    FRefCount:=Self.FRefCount;
  end;
end;

Procedure TCustomXFill.AddRef;
begin
  Inc(FRefCount);
end;

Procedure TCustomXFill.Release;
begin
  if FRefCount>0 then Dec(FRefCount);
end;

{===============================================================================
| Class TBitmapXFill
+==============================================================================}

Constructor TBitmapXFill.Create;
begin
  inherited Create;
  FType:=xftBitmap;
  FDIBBitmap:=TBitmap.Create;
  FDIBBitmap.handleType:=bmDIB;
end;

Destructor TBitmapXFill.Destroy;
begin
  FDIBBitmap.Free;
  FreeDDB;
  inherited Destroy;
end;

Procedure TBitmapXFill.ReadFromRegistry(Registry:TCustomRegistry;Const KeyName:String);
var MemStream      : TMemoryStream;
begin
  inherited ReadFromRegistry(Registry,KeyName);
  MemStream:=TMemoryStream.Create;
  try
    MemStream.Size:=Registry.DataSize['Bitmap'];
    Registry.ReadBinary('Bitmap',MemStream.Memory^,MemStream.Size);
    FDIBBitmap.LoadFromStream(MemStream);
  finally
    MemStream.Free;
  end;
end;

Procedure TBitmapXFill.WriteToRegistry(Registry:TCustomRegistry;Const KeyName:String);
var MemStream      : TMemoryStream;
begin
  inherited WriteToRegistry(Registry,KeyName);
  MemStream:=TMemoryStream.Create;
  try
    FDIBBitmap.SaveToStream(MemStream);
    Registry.WriteBinary('Bitmap',MemStream.Memory^,MemStream.Size);
  finally
    MemStream.Free;
  end;
end;

Procedure TBitmapXFill.AssignTo(Dest:TPersistent);
begin
  inherited AssignTo(Dest);
  with Dest as TBitmapXFill do begin
    DIBBitmap.Assign(Self.FDIBBitmap);
  end;
end;

Procedure TBitmapXFill.SetDIBBitmap(const Value: TBitmap);
begin
  FDIBBitmap.Assign(Value); 
end;

Function TBitmapXFill.StyleDef(DC: HDC): PBitmapXFillDef;
var BitmapInfo     : TDIBSection;
begin
  if DC<>FDC then begin
    FreeDDB;
    FDC:=DC;
  end;
  if FBitmapXFill.BitmapHandle=0 then begin
    GetObject(FDIBBitmap.Handle,SizeOf(BitmapInfo),@BitmapInfo);
    FBitmapXFill.BitmapHandle:=CreateDIBitmap(FDC,BitmapInfo.dsbmih,
            CBM_INIT,PChar(BitmapInfo.dsBm.bmBits),
            PBITMAPINFO(@BitmapInfo.dsbmih)^,DIB_RGB_COLORS);
  end;
  Result:=@FBitmapXFill;
end;           

Procedure TBitmapXFill.FreeDDB;
begin
  if FBitmapXFill.BitmapHandle<>0 then begin
    DeleteObject(FBitmapXFill.BitmapHandle);
    FBitmapXFill.BitmapHandle:=0;
  end;
end;

Procedure TBitmapXFill.ReadFromFile(IniFile:TIniFile;Const KeyName:String);
var MemoryStream   : TMemoryStream;
    DataLines      : Integer;
    Buffer         : String;
    Data           : Byte;
    Cnt            : Integer;
    Cnt1           : Integer;
begin
  inherited ReadFromFile(IniFile,KeyName);
  MemoryStream:=TMemoryStream.Create;
  try
    DataLines:=IniFile.ReadInteger(KeyName,'DataLines',0);
    for Cnt:=0 to DataLines-1 do begin
      Buffer:=Trim(IniFile.ReadString(KeyName,'Data'+IntToStr(Cnt),''));
      for Cnt1:=0 to Length(Buffer) Div 2-1 do begin
        Data:=StrToInt('$'+Copy(Buffer,Cnt1*2+1,2));
        MemoryStream.Write(Data,SizeOf(Data));
      end;
    end;
    MemoryStream.Seek(0,soFromBeginning);
    FDIBBitmap.LoadFromStream(MemoryStream);
  finally
    MemoryStream.Free;
  end;
end;

Procedure TBitmapXFill.WriteToFile(IniFile:TIniFile;Const KeyName:String);
var MemoryStream   : TMemoryStream;
    DataLines      : Integer;
    Buffer         : String;
    Data           : Byte;
    Cnt            : Integer;
    Cnt1           : Integer;
begin
  inherited WriteToFile(IniFile,KeyName);
  MemoryStream:=TMemoryStream.Create;
  try
    FDIBBitmap.SaveToStream(MemoryStream);
    DataLines:=(MemoryStream.Size+31) Div 32;
    IniFile.WriteInteger(KeyName,'DataLines',DataLines);
    MemoryStream.Seek(0,soFromBeginning);
    for Cnt:=0 to DataLines-1 do begin
      Buffer:='';
      for Cnt1:=0 to Min(32,MemoryStream.Size-MemoryStream.Position)-1 do begin
        MemoryStream.Read(Data,SizeOf(Data));
        Buffer:=Buffer+IntToHex(Data,2);
      end;
      IniFile.WriteString(KeyName,'Data'+IntToStr(Cnt),Buffer);
    end;
  finally
    MemoryStream.Free;
  end;
end;

{===============================================================================
| Class TXFillStyleList
+==============================================================================}

Constructor TXFillStyleList.Create;
begin
  inherited Create(9);
  FLastNumber:=pt_UserDefined;
end;

Function TXFillStyleList.KeyOf(Item:Pointer):Pointer;
begin
  Result:=Pointer(TCustomXFill(Item).Number);
end;

Function TXFillStyleList.OnCompareItems(Item1:Pointer;Item2:Pointer):Boolean;
begin
  Result:=LongCompare(LongInt(Item1),LongInt(Item2))=0;
end;

Function TXFillStyleList.OnGetHashValue(Item:Pointer):LongInt;
begin
  Result:=LongInt(Item);
end;

Procedure TXFillStyleList.ReadFromRegistry(Registry:TCustomRegistry;Const KeyName:String);
var Cnt          : Integer;
    Item         : TCustomXFill;
    ItemCount    : Integer;
    FillType     : TFillType;
begin
  with Registry do begin
    OpenKey(KeyName,FALSE);
    ItemCount:=ReadInteger('Count');
    FLastNumber:=ReadInteger('LastNumber');
    if LastError<>rdbeNoError then FLastNumber:=pt_UserDefined;
    Capacity:=ItemCount;
    for Cnt:=0 to ItemCount-1 do begin
      FillType:=TFillType(ReadInteger(KeyName+Format('\Style%d',[Cnt])+'\Type'));
      case FillType of
        xftBitmap : Item:=TBitmapXFill.Create;
        xftVector : Item:=TVectorXFill.Create;
        else Item:=TCustomXFill.Create;
      end;
      Item.ReadFromRegistry(Registry,KeyName+Format('\Style%d',[Cnt]));
      Add(Item);
    end;
  end;
end;

Procedure TXFillStyleList.WriteToRegistry(Registry:TCustomRegistry;Const KeyName:String);
var Cnt          : Integer;
    ItemIndex    : Integer;
begin
  with Registry do begin
    OpenKey(KeyName,TRUE);
    WriteInteger('Count',Count);
    WriteInteger('LastNumber',FLastNumber);
    ItemIndex:=0;
    for Cnt:=0 to Capacity-1 do if Used(Items[Cnt]) then begin
      TCustomXFill(Items[Cnt]).WriteToRegistry(Registry,KeyName+Format('\Style%d',[ItemIndex]));
      Inc(ItemIndex);
    end;
  end;
end;

Function TXFillStyleList.GetStyleFromNumber(ANumber:Integer):TCustomXFill;
begin
  Result:=Find(Pointer(ANumber));
end;

Procedure TXFillStyleList.AssignTo(Dest:TPersistent);
begin
  inherited AssignTo(Dest);
  with Dest as TXFillStyleList do begin
    FLastNumber:=Self.FLastNumber;
  end;
end;                                    

Function TXFillStyleList.Add(Item:Pointer):Boolean;
begin
  if TCustomXFill(Item).Number=0 then begin
    TCustomXFill(Item).Number:=FLastNumber;
    Inc(FLastNumber);
  end;
  Result:=inherited Add(Item);
end;

Function TXFillStyleList.GetNumberByName(const StyleName:string):integer;
var retVal:integer;
    i:integer;
begin
   retVal:=0;
   for i:=0 to Capacity-1 do if Used(Items[i]) then
   begin
      if TCustomXFill(Items[i]).Name = StyleName then
      begin
         retVal:=TCustomXFill(Items[i]).Number;
         break;
      end;
   end;
   result:=retVal;
end;

Function TXFillStyleList.GetNameByNumber(StyleNumber:integer):string;
var retVal:string;
    i:integer;
begin
   retVal:='';
   for i:=0 to Capacity-1 do if Used(Items[i]) then
   begin
      if TCustomXFill(Items[i]).Number = StyleNumber then
      begin
         retVal:=TCustomXFill(Items[i]).Name;
         break;
      end;
   end;
   result:=retVal;
end;

Procedure TXFillStyleList.ImportFromFile(const FileName:String);
var IniFile      : TIniFile;
    Cnt          : Integer;
    Entries      : Integer;
    Item         : TCustomXFill;
    FillType     : TFillType;
    exists       : boolean;
    i,n          : integer;
begin
  IniFile:=TIniFile.Create(FileName);
  with IniFile do begin
    Entries:=ReadInteger('LibraryInfo','FillStyles',0);
    for Cnt:=0 to Entries-1 do begin
      FillType:=IdentToXFillType(ReadString(Format('FillStyle%d',[Cnt]),
          'Type','Vector'));
      case FillType of
        xftVector : Item:=TVectorXFill.Create;
        else Item:=TBitmapXFill.Create;
      end;
      Item.ReadFromFile(IniFile,Format('FillStyle%d',[Cnt]));

      // check if the same item does not already exist
      exists:=FALSE;
      for i:=0 to Capacity-1 do if Used(Items[i]) then
      begin
         if TCustomXFill(Items[i]).Name = Item.Name then
         begin
            n:=TCustomXFill(Items[i]).FNumber;
            Item.AssignTo(Items[i]);
            TCustomXFill(Items[i]).FNumber:=n;

            if TCustomXFill(Items[i]).FType = xftBitmap then begin
               TBitmapXFill(Items[i]).FBitmapXFill.BitmapHandle:=0;
               TBitmapXFill(Items[i]).StyleDef(TBitmapXFill(Items[i]).FDC);
            end;

            exists:=TRUE;
            break;
         end;
      end;

      if not exists then begin
         Add(Item)
      end;
    end;
  end;
end;

Procedure TXFillStyleList.ReadFromFile(const FileName:String);
var IniFile      : TIniFile;
    Cnt          : Integer;
    Entries      : Integer;
    Item         : TCustomXFill;
    FillType     : TFillType;
    exists       : boolean;
    i            : integer;
begin
  Clear;
  IniFile:=TIniFile.Create(FileName);
  with IniFile do begin
    Entries:=ReadInteger('LibraryInfo','FillStyles',0);
    for Cnt:=0 to Entries-1 do begin
      FillType:=IdentToXFillType(ReadString(Format('FillStyle%d',[Cnt]),
          'Type','Vector'));
      case FillType of
        xftVector : Item:=TVectorXFill.Create;
        else Item:=TBitmapXFill.Create;
      end;
      Item.ReadFromFile(IniFile,Format('FillStyle%d',[Cnt]));
      Add(Item);
    end;
  end;
end;

Procedure TXFillStyleList.WriteToFile(const FileName:String);
var IniFile      : TIniFile;
    ItemIndex    : Integer;
    Cnt          : Integer;
begin
  IniFile:=TIniFile.Create(FileName);
  with IniFile do begin
    WriteString('FileInfo','FileType','WinGIS-Userdefined-Style-Library');
    WriteInteger('FileInfo','FileVersion',1);
    WriteInteger('LibraryInfo','FillStyles',Count);
    ItemIndex:=0;
    for Cnt:=0 to Capacity-1 do if Used(Items[Cnt]) then begin
      TCustomXFill(Items[Cnt]).WriteToFile(IniFile,Format('FillStyle%d',[ItemIndex]));
      Inc(ItemIndex);
    end;
  end;
end;

Function TXFillStyleList.OnCopyItem(Item: Pointer): Pointer;
begin
  Result:=TCustomXFillClass(TCustomXFill(Item).ClassType).Create;
  TPersistent(Result).Assign(Item);
end;

{===============================================================================
| Class TVectorXFill
+==============================================================================}

Constructor TVectorXFill.Create;
begin
  inherited Create;
  FType:=xftVector;
end;

Destructor TVectorXFill.Destroy;
begin
  Clear;
  inherited Destroy;
end;

Procedure TVectorXFill.Add(var Item:TXFillDef);
begin
  SetCapacity(Count+1);
  FData^.XFillDefs[FData^.Count-1]:=Item;
end;

Procedure TVectorXFill.Clear;
var Cnt          : Integer;
    XFillDef     : PXFillDef;              
begin
  if FData<>NIL then begin
    for Cnt:=0 to FData^.Count-1 do begin
      XFillDef:=@FData.XFillDefs[Cnt];
      with XFillDef^ do if XLineStyle<>NIL then FreeMem(XLineStyle,
          XLineStyleDefSize(XLineStyle^.Count));
    end;
    FreeMem(FData,XFillStyleDefSize(FData^.Count));
  end;
  FData:=NIL;
end;

Procedure TVectorXFill.Delete(AIndex:Integer);
var XFillDef     : PXFillDef;
begin
  Assert((AIndex>=0) and (AIndex<Count));
  XFillDef:=@FData.XFillDefs[AIndex];
  with XFillDef^ do if XLineStyle<>NIL then FreeMem(XLineStyle,
      XLineStyleDefSize(XLineStyle.Count));
  System.Move(FData^.XFillDefs[AIndex+1],FData^.XFillDefs[AIndex],
      (Count-AIndex-1)*SizeOf(TXFillDef));
  SetCapacity(FData^.Count-1);
end;
   
Procedure TVectorXFill.Exchange(Index1,Index2:Integer);
var TempItem     : TXFillDef;
begin
  Assert((Index1>=0) and (Index1<Count) and (Index2>=0) and (Index2<Count));
  System.Move(FData^.XFillDefs[Index1],TempItem,SizeOf(TXFillDef));
  System.Move(FData^.XFillDefs[Index2],FData^.XFillDefs[Index1],SizeOf(TXFillDef));
  System.Move(TempItem,FData^.XFillDefs[Index2],SizeOf(TXFillDef));
end;

Procedure TVectorXFill.Move(FromIndex,ToIndex:Integer);
var TempItem     : TXFillDef;
begin
  Assert((FromIndex>=0) and (FromIndex<Count) and (ToIndex>=0) and (ToIndex<Count));
  System.Move(FData^.XFillDefs[FromIndex],TempItem,SizeOf(TXFillDef));
  if FromIndex>ToIndex then System.Move(FData^.XFillDefs[ToIndex],FData^.XFillDefs[ToIndex+1],
      (FromIndex-ToIndex)*SizeOf(TXFillDef))
  else System.Move(FData^.XFillDefs[FromIndex+1],FData^.XFillDefs[FromIndex],
      (ToIndex-FromIndex)*SizeOf(TXFillDef));
  System.Move(TempItem,FData^.XFillDefs[ToIndex],SizeOf(TXFillDef));
end;

Function TVectorXFill.IndexOf(Item:PXFillDef):Integer;
begin
  for Result:=0 to Count-1 do if @FData^.XFillDefs[Result]=Item then Exit;
  Result:=-1;
end;

Function TVectorXFill.GetCount:Integer;
begin
  if FData=NIL then Result:=0
  else Result:=FData^.Count;
end;

Procedure TVectorXFill.ReadFromRegistry(Registry:TCustomRegistry;Const KeyName:String);
var Cnt          : Integer;
    ItemName     : String;
    XFillDef     : PXFillDef;
    DefCount     : Integer;
begin
  inherited ReadFromRegistry(Registry,KeyName);
  with Registry do begin
    SetCapacity(ReadInteger('Count'));
    for Cnt:=0 to FData^.Count-1 do begin
      ItemName:=Format('Item%d',[Cnt]);
      ReadBinary(ItemName,FData^.XFillDefs[Cnt],SizeOf(TXFillDef));
      XFillDef:=@FData^.XFillDefs[Cnt];
      with XFillDef^ do if Style>=lt_UserDefined then begin
        ReadBinaryOffset(ItemName,DefCount,SizeOf(TXFillDef),SizeOf(DefCount));
        GetMem(XLineStyle,XLineStyleDefSize(DefCount));
        XLineStyle^.Count:=DefCount;
        ReadBinaryOffset(ItemName,XLineStyle^.XLineDefs,SizeOf(TXFillDef)+
            SizeOf(Integer),XLineStyle^.Count*SizeOf(TXLineDef));
      end;
    end;
  end;
end;

Procedure TVectorXFill.ReadFromFile(IniFile:TIniFile;Const KeyName:String);
var Entries      : Integer;
    LineEntries  : Integer;
    Cnt          : Integer;
    Cnt1         : Integer;
    EntryName    : String;
    WorkStr      : String;
    Item         : TXFillDef;
begin
  inherited ReadFromFile(IniFile,KeyName);
  with IniFile do begin
    Entries:=ReadInteger(KeyName,'Count',0);
    for Cnt:=0 to Entries-1 do begin
      EntryName:=KeyName+Format('Item%d',[Cnt]);
      FillChar(Item,SizeOf(Item),#0);
      with Item do begin
        Style:=ReadInteger(EntryName,'Style',0);
        Cycle:=ReadInteger(EntryName,'Cycle',0);
        XOffset:=ReadInteger(EntryName,'XOffset',0);
        YOffset:=ReadInteger(EntryName,'YOffset',0);
        Phase:=ReadInteger(EntryName,'Phase',0);
        Angle:=ReadFloat(EntryName,'Angle',0)*Pi/180.0;
        LineWidth:=ReadFloat(EntryName,'LineWidth',0);
        WorkStr:=ReadString(EntryName,'LineColor','');
        if CompareText(WorkStr,'None')=0 then LineColor:=clNone
        else if CompareText(WorkStr,'Object')=0 then LineColor:=clFromObject
        else ScanString('%i',WorkStr,[@LineColor]);
        WorkStr:=ReadString(EntryName,'FillColor','');
        if CompareText(WorkStr,'None')=0 then FillColor:=clNone
        else if CompareText(WorkStr,'Object')=0 then FillColor:=clFromObject
        else ScanString('%i',WorkStr,[@FillColor]);
        if Style>=lt_UserDefined then begin
          LineEntries:=ReadInteger(EntryName,'Count',0);
          GetMem(XLineStyle,XLineStyleDefSize(LineEntries));
          XLineStyle^.Count:=LineEntries; 
          for Cnt1:=0 to LineEntries-1 do ReadXLineDefFromFile(IniFile,
              @XLineStyle^.XLineDefs[Cnt1],EntryName+Format('Line%d',[Cnt1]));
        end;
        Add(Item);
      end;
    end;
  end;
end;

Procedure TVectorXFill.WriteToFile(IniFile:TIniFile;Const KeyName:String);
var Cnt          : Integer;
    Cnt1         : Integer;
    EntryName    : String;
begin
  inherited WriteToFile(IniFile,KeyName);
  with IniFile do begin
    WriteInteger(KeyName,'Count',Count);
    for Cnt:=0 to Count-1 do begin
      EntryName:=KeyName+Format('Item%d',[Cnt]);
      with FData^.XFillDefs[Cnt] do begin
        WriteInteger(EntryName,'Style',Style);
        WriteInteger(EntryName,'Cycle',Cycle);
        WriteInteger(EntryName,'XOffset',XOffset);
        WriteInteger(EntryName,'YOffset',YOffset);
        WriteInteger(EntryName,'Phase',Phase);
        WriteInteger(EntryName,'Angle',Round(Angle*180/Pi));
        WriteFloat(EntryName,'LineWidth',LineWidth);
        if LineColor=-1 then WriteString(EntryName,'LineColor','None')
        else if LineColor<-1 then WriteString(EntryName,'LineColor','Object')
        else WriteString(EntryName,'LineColor',Format('0x%.6x',[LineColor]));
        if FillColor=-1 then WriteString(EntryName,'FillColor','None')
        else if FillColor<-1 then WriteString(EntryName,'FillColor','Object')
        else WriteString(EntryName,'FillColor',Format('0x%.6x',[FillColor]));
        if Style>=lt_UserDefined then begin
          WriteInteger(EntryName,'Count',XLineStyle^.Count);
          for Cnt1:=0 to XLineStyle^.Count-1 do WriteXLineDefToFile(IniFile,
              @XLineStyle^.XLineDefs[Cnt1],EntryName+Format('Line%d',[Cnt1]));
        end;
      end;
    end;  
  end;
end;

Procedure TVectorXFill.WriteToRegistry(Registry:TCustomRegistry;Const KeyName:String);
var Cnt          : Integer;
    XFillDef     : PXFillDef;
    ItemName     : String;
    TempData     : TMemoryStream;
begin
  inherited WriteToRegistry(Registry,KeyName);
  with Registry do begin
    WriteInteger('Count',Count);
    TempData:=TMemoryStream.Create;
    for Cnt:=0 to Count-1 do begin
      XFillDef:=@FData^.XFillDefs[Cnt];
      ItemName:=Format('Item%d',[Cnt]);
      TempData.Clear;
      TempData.Write(XFillDef^,SizeOf(TXFillDef));
      if XFillDef^.Style>=lt_UserDefined then TempData.Write(XFillDef^.XLineStyle^,
          XLineStyleDefSize(XFillDef^.XLineStyle^.Count));
      WriteBinary(ItemName,TempData.Memory^,TempData.Size);
    end;
    TempData.Free;
  end;
end;

Procedure TVectorXFill.AssignTo(Dest:TPersistent);
var Cnt          : Integer;
    XFillDef     : PXFillDef;
    XFillSource  : PXFillDef;
begin
  inherited AssignTo(Dest);
  with Dest as TVectorXFill do begin
    TVectorXFill(Dest).Clear;
    SetCapacity(Self.Count);
    if FData<>NIL then begin
      System.Move(Self.FData^,FData^,XFillStyleDefSize(FData^.Count));
      for Cnt:=0 to FData^.Count-1 do begin
        XFillDef:=@FData^.XFillDefs[Cnt];
        if XFillDef.XLineStyle<>NIL then begin
          XFillSource:=@Self.FData^.XFillDefs[Cnt];
          GetMem(XFillDef^.XLineStyle,XLineStyleDefSize(XFillSource^.XLineStyle.Count));
          System.Move(XFillSource^.XLineStyle^,XFillDef^.XLineStyle^,
              XLineStyleDefSize(XFillSource^.XLineStyle.Count));
        end;
      end;  
    end;
  end;
end;

Procedure TVectorXFill.SetCapacity(ACapacity:Integer);
var Temp         : PXFillStyleDef;
begin
  if ACapacity<>Count then begin
    GetMem(Temp,XFillStyleDefSize(ACapacity));
    if FData<>NIL then begin
      System.Move(FData^,Temp^,XFillStyleDefSize(Min(ACapacity,FData^.Count)));
      FreeMem(FData,XFillStyleDefSize(FData^.Count));
    end;
    FData:=Temp;
    FData^.Count:=ACapacity;
  end;
end;

Function TVectorXFill.GetItem(AIndex:Integer):PXFillDef;
begin
  Result:=@FData^.XFillDefs[AIndex];
end;

Function TVectorXFill.GetClipRect:TRect;
begin
  Result:=Rect(0,0,0,0);
end;

Function TVectorXFill.GetMinDistance:Integer;
var Cnt            : Integer;
begin
  if Count=0 then Result:=0
  else begin
    Result:=MaxInt;
    for Cnt:=0 to Count-1 do if Items[Cnt].Cycle<Result then
        Result:=Items[Cnt].Cycle;
  end;
end;

{==============================================================================+
  Other
+==============================================================================}

Function IdentToXFillType(const Ident:String):TFillType;
begin
  if AnsiCompareText(Ident,'Vector')=0 then Result:=xftVector
  else Result:=xftBitmap;
end;

Function XFillTypeToIdent(FillType:TFillType):String;
begin
  if FillType=xftVector then Result:='Vector'
  else Result:='Bitmap';
end;

Function TXFillStyleList.GetStyleFromName(const AName:String):TCustomXFill;
var Cnt            : Integer;
    Item           : TCustomXFill;
begin
  for Cnt:=0 to Capacity-1 do begin
    Item:=Items[Cnt];
    if (Item<>NIL) and (AnsiCompareText(Item.Name,AName)=0) then begin
      Result:=Item;
      Exit;
    end;
  end;
  Result:=NIL;
end;

Procedure TXFillStyleList.AddRef(Style: Integer);
var XStyle         : TCustomXFill;
begin
  XStyle:=NumberedStyles[Style];
  if XStyle<>NIL then XStyle.AddRef;
end;

Procedure TXFillStyleList.Relase(Style: Integer);
var XStyle         : TCustomXFill;
begin
  XStyle:=NumberedStyles[Style];
  if XStyle<>NIL then XStyle.Release;
end;

Function TXFillStyleList.GetStyle(AIndex:Integer):TCustomXFill;
var Cnt            : Integer;
begin
  for Cnt:=0 to FCapacity-1 do if Items[Cnt]<>NIL then begin
    if AIndex=0 then begin
      Result:=Items[Cnt];
      Exit;
    end;
    Dec(AIndex);
  end;
  Result:=NIL;
end;                       

end.
