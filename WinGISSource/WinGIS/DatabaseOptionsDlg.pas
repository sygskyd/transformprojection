unit DatabaseOptionsDlg;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
   StdCtrls, ExtCtrls, Chklist, WCtrls, ComCtrls, Objects, LicenseHndl, DDEDef, MultiLng, ImgList
{$IFNDEF WMLT}
   , AM_DDE
{++ IDB}
   , AM_Proj
{-- IDB}
{$ENDIF}
   ;

type
   TDatabaseOptionsDialog = class (TWForm)
      Bevel1: TBevel;
      CancelBtn: TButton;
      ControlPanel1: TPanel;
      ControlPanel: TPanel;
      DDECombo: TComboBox;
      DDEMLListView: TWListView;
      Group1: TWGroupBox;
      Group2: TWGroupBox;
      ImageList: TImageList;
      Label2: TLabel;
      MlgSection: TMlgSection;
      OKBtn: TButton;
      SendDDECheck: TCheckBox;
      GroupBox1: TGroupBox;
      IDB_Enabled_CB: TCheckBox;
      IDB_Interface_CB: TCheckBox;
      GroupBox2: TGroupBox;
      IDB_UndoStepsCount_Label: TLabel;
      IDB_UNDO_CB: TCheckBox;
      IDB_UndoStepsCount_Edit: TEdit;
      procedure DDEComboClick (Sender: TObject);
      procedure DDEMLListViewDrawItem (Control: TWinControl; Index: Integer;
         Rect: TRect; State: TOwnerDrawState);
      procedure DDEMLListViewEditing (Sender: TObject; Item: TListItem;
         var AllowEdit: Boolean);
      procedure DDEMLListViewMouseDown (Sender: TObject; Button: TMouseButton;
         Shift: TShiftState; X, Y: Integer);
      procedure DDEMLListViewMouseUp (Sender: TObject; Button: TMouseButton;
         Shift: TShiftState; X, Y: Integer);
      procedure FormCreate (Sender: TObject);
      procedure FormDestroy (Sender: TObject);
      procedure FormHide (Sender: TObject);
      procedure FormShow (Sender: TObject);
      procedure SendDDECheckClick (Sender: TObject);
      procedure IDB_Enabled_CBClick (Sender: TObject);
      procedure UseGlobalIDBCheckClick (Sender: TObject);
      procedure IDB_UNDO_CBClick (Sender: TObject);
   private
      FOrgDDEApps: PCollection;
      FDDEApps: PCollection;
      FOrgDDEMLApps: PCollection;
      FDDEMLApps: PCollection;
      FMouseDownPos: Integer;
{++ IDB}
      {$IFNDEF WMLT}
      FProject: PProj;
      {$ENDIF}
{-- IDB}
      function CheckIndexFromPos (X, Y: Integer): Integer;
      function DDEAppFromName (const Name: string): PDDEApp;
      function OrgDDEMLAppFromName (const Name: string): PDDEMLApp;
   public
      property DDEApplications: PCollection read FDDEApps write FOrgDDEApps;
      property DDEMLApplications: PCollection read FDDEMLApps write FOrgDDEMLApps;
      {$IFNDEF WMLT}
      property Project: PProj write FProject;
      {$ENDIF}
   end;

implementation

{$R *.DFM}

uses NumTools
{$IFNDEF WMLT}
{++ IDB}
   , AM_Main, AM_Child, AM_Ini, AM_Def
{$IFNDEF AXDLL} // <----------------- AXDLL
   , IDB_Man
{$ENDIF} // <----------------- AXDLL
{-- IDB}
{$ENDIF}
   ;

procedure TDatabaseOptionsDialog.FormCreate (Sender: TObject);
begin
   FDDEApps := New (PCollection, Init (10, 10));
   FDDEMLApps := New (PCollection, Init (10, 10));
end;

procedure TDatabaseOptionsDialog.FormDestroy (Sender: TObject);
begin
   Dispose (FDDEApps, Done);
   Dispose (FDDEMLApps, Done);
end;

procedure TDatabaseOptionsDialog.FormShow (Sender: TObject);
var
   Cnt: Integer;
   DDEApp: PDDEApp;
   ConnectApp: PDDEApp;
   DDEMLApp: PDDEMLApp;
   ListItem: TListItem;
{++ IDB}
//      bLibraryWasLoaded: Boolean;
{-- IDB}
begin
   if FOrgDDEApps <> nil then
      for Cnt := 0 to FOrgDDEApps^.Count - 1 do
         FDDEApps^.Insert (New (PDDEApp, InitByListEntry (FOrgDDEApps^.At (Cnt))));
   if FOrgDDEMLApps <> nil then
      for Cnt := 0 to FOrgDDEMLApps^.Count - 1 do
         FDDEMLApps^.Insert (New (PDDEMLApp, InitByListEntry (FOrgDDEMLApps^.At (Cnt))));
   ConnectApp := nil;
   for Cnt := 0 to FDDEApps^.Count - 1 do
   begin
      DDEApp := FDDEApps^.At (Cnt);
      DDECombo.Items.Add (StrPas (DDEApp^.Name));
      if DDEApp^.Connect then ConnectApp := DDEApp;
   end;
   if ConnectApp <> nil then
   begin
      Cnt := DDECombo.Items.IndexOf (StrPas (ConnectApp^.Name));
      if Cnt >= 0 then
      begin
         DDECombo.ItemIndex := Cnt;
         SendDDECheck.Checked := ConnectApp^.SendStrings;
      end;
   end;
{$IFNDEF WMLT}
{++ IDB}
{$IFNDEF AXDLL} // <----------------- AXDLL
{    TRY
       bLibraryWasLoaded := WinGISMainForm.IDB_Man.LibraryWasLoaded;
    EXCEPT
       bLibraryWasLoaded := FALSE;
    END;}

   if FProject = nil then {Global}
   begin
      Group2.Height:=137;
      GroupBox1.Top:=98;
      GroupBox1.Visible:=False;
      GroupBox2.Visible:=True;

      IDB_Enabled_CB.Checked := WinGISMainForm.WeAreUsingTheIDB[SELF.FProject];
      IDB_Interface_CB.Checked := WinGISMainForm.WeAreUsingTheIDBUserInterface[SELF.FProject];
      IDB_UNDO_CB.Checked := WinGISMainForm.WeAreUsingUndoFunction[SELF.FProject];
      IDB_UndoStepsCount_Edit.Text := IntToStr (WinGISMainForm.IDBUndoStepsCount[Self.FProject]);
      IDB_Enabled_CBClick (SENDER);
   end
   else
   begin                  {Project}
      Group2.Height:=124;
      GroupBox1.Top:=136;
      GroupBox1.Visible:=True;
      GroupBox2.Visible:=False;
      IDB_Enabled_CB.Checked := FProject.IDBSettings.bLocalWeAreUsingTheIDB;
      IDB_Interface_CB.Checked := FProject.IDBSettings.bLocalWeAreUsingTheIDBUserInterface;
      IDB_UNDO_CB.Checked := FProject.IDBSettings.bLocalWeAreUsingUndoFunction;
      IDB_UndoStepsCount_Edit.Text := IntToStr (FProject.IDBSettings.iLocalUndoStepsCount);
      UseGlobalIDBCheckClick (SENDER);
   end;

   if not FileExists (ExtractFilePath (Application.ExeName) + 'IDB.DLL') then
      IDB_Enabled_CB.Enabled := FALSE;
// ++ Cadmensky
   if not FileExists (ExtractFilePath (Application.ExeName) + 'UndoRedo.DLL') then
   begin
      IDB_UNDO_CB.Enabled := False;
      IDB_UndoStepsCount_Edit.Enabled := False;
      IDB_UndoStepsCount_Label.Enabled := False;
   end;
// -- Cadmensky

{$ENDIF} // <----------------- AXDLL
{-- IDB}
{$ENDIF}
   for Cnt := 0 to FDDEMLApps^.Count - 1 do
   begin
      DDEMLApp := FDDEMLApps^.At (Cnt);
      ListItem := DDEMLListView.Items.Add;
      ListItem.Caption := StrPas (DDEMLApp^.Name);
      ListItem.Data := Pointer (DDEMLApp^.SendStrings);
   end;
   for Cnt := 0 to FDDEMLApps^.Count - 1 do
      DDEMLListView.Items[Cnt].Checked := PDDEMLApp (FDDEMLApps^.At (Cnt)).Connect;

   if (WingisLicense < WG_STD) then begin
      IDB_Interface_CB.Enabled:=False;
      IDB_Interface_CB.Checked:=False;
   end;
    IDB_UNDO_CBClick(nil);
end;

procedure TDatabaseOptionsDialog.DDEComboClick (Sender: TObject);
var
   DDEApp: PDDEApp;
begin
   DDEApp := DDEAppFromName (DDECombo.Text);
   if DDEApp <> nil then SendDDECheck.Checked := DDEApp^.SendStrings;
end;

procedure TDatabaseOptionsDialog.SendDDECheckClick (Sender: TObject);
var
   DDEApp: PDDEApp;
begin
   DDEApp := DDEAppFromName (DDECombo.Text);
   if DDEApp <> nil then DDEApp^.SendStrings := SendDDECheck.Checked;
end;

function TDatabaseOptionsDialog.DDEAppFromName
   (
   const Name: string
   )
   : PDDEApp;
var
   Cnt: Integer;
begin
   for Cnt := 0 to FDDEApps^.Count - 1 do
   begin
      Result := FDDEApps^.At (Cnt);
      if AnsiCompareText (StrPas (Result^.Name), Name) = 0 then Exit;
   end;
   Result := nil;
end;

function TDatabaseOptionsDialog.OrgDDEMLAppFromName
   (
   const Name: string
   )
   : PDDEMLApp;
var
   Cnt: Integer;
begin
   for Cnt := 0 to FOrgDDEMLApps^.Count - 1 do
   begin
      Result := FOrgDDEMLApps^.At (Cnt);
      if AnsiCompareText (StrPas (Result^.Name), Name) = 0 then Exit;
   end;
   Result := nil;
end;

procedure TDatabaseOptionsDialog.DDEMLListViewDrawItem (
   Control: TWinControl; Index: Integer; Rect: TRect;
   State: TOwnerDrawState);
var
   ListItem: TListItem;
   ARect: TRect;
   ImageIndex: Integer;
begin
   with DDEMLListView, Canvas do
   begin
      ListItem := DDEMLListView.Items[Index];
      ARect := ItemRect[Index, 0];
      if ListItem.Checked then
         ImageIndex := 1
      else
         ImageIndex := 0;
{$IFNDEF WMLT}
{++ IDB}
{$IFNDEF AXDLL} // <----------------- AXDLL
//      if not (WinGisMainForm.WeAreUsingTheIDB[FProject] and (Index = 0)) then
{$ENDIF} // <----------------- AXDLL
{-- IDB}
{$ENDIF}
      ImageList.Draw (Canvas, ARect.Left + 4, CenterHeightInRect (ImageList.Height, ARect), ImageIndex);
      Inc (ARect.Left, ImageList.Width + 6);
      ARect.Right := ARect.Left + TextWidth (ListItem.Caption) + 6;
      TextRectEx (Canvas, ARect, ListItem.Caption, Color, State);
      Brush.Color := Color;
      ARect := ItemRect[Index, 1];
      FillRect (ARect);
      ImageList.Draw (Canvas, CenterWidthInRect (ImageList.Width, ARect),
         CenterHeightInRect (ImageList.Height, ARect), Integer (ListItem.Data));
   end;
end;

procedure TDatabaseOptionsDialog.DDEMLListViewEditing (Sender: TObject;
   Item: TListItem; var AllowEdit: Boolean);
begin
   AllowEdit := FALSE;
end;

procedure TDatabaseOptionsDialog.DDEMLListViewMouseDown (Sender: TObject;
   Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
   FMouseDownPos := CheckIndexFromPos (X, Y);
end;

function TDatabaseOptionsDialog.CheckIndexFromPos
   (
   X: Integer;
   Y: Integer
   )
   : Integer;
var
   ListItem: TListItem;
   ARect: TRect;
   BRect: TRect;
begin
   Result := 0;
   ListItem := DDEMLListView.GetItemAt (X, Y);
   if ListItem <> nil then
   begin
      if (X >= 4) and (X < ImageList.Width + 4) then
         Result := ListItem.Index + 1
      else
      begin
         ARect := DDEMLListView.ItemRect[ListItem.Index, 1];
         BRect := Rect (0, 0, ImageList.Width, ImageList.Height);
         BRect := CenterRectInRect (BRect, ARect);
         if ptInRect (BRect, Point (X, Y)) then Result := -ListItem.Index - 1;
      end;
   end;
end;

procedure TDatabaseOptionsDialog.DDEMLListViewMouseUp (Sender: TObject;
   Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
   ListItem: TListItem;
begin
   if FMouseDownPos <> 0 then
   begin
      FMouseDownPos := CheckIndexFromPos (X, Y);
      if FMouseDownPos <> 0 then
      begin
         ListItem := DDEMLListView.GetItemAt (X, Y);
         if ListItem <> nil then
            with ListItem do
            begin
               if FMouseDownPos > 0 then
                  Checked := not Checked
               else
                  Data := Pointer (not (Boolean (Data)));
               DDEMLListView.UpdateItems (Index, Index);
            end;
      end;
   end;
end;

procedure TDatabaseOptionsDialog.FormHide (Sender: TObject);
var
   DDEApp: PDDEApp;
   OrgDDEApp: PDDEApp;
   OrgDDEMLApp: PDDEMLApp;
   ListItem: TListItem;
   Cnt: Integer;
{++ IDB}
   iI: Integer;
   bLibraryWasLoaded,
      bAddProject: Boolean;
   AForm: TForm;
{-- IDB}
begin
   if ModalResult = mrOK then
   begin
      if FOrgDDEApps <> nil then
         for Cnt := 0 to FOrgDDEApps.Count - 1 do
         begin
            DDEApp := FDDEApps^.At (Cnt);
            OrgDDEApp := FOrgDDEApps^.At (Cnt);
            if AnsiCompareText (StrPas (DDEApp^.Name), DDECombo.Text) = 0 then
               OrgDDEApp^.Connect := TRUE
            else
               OrgDDEApp^.Connect := FALSE;
            OrgDDEApp^.SendStrings := DDEApp^.SendStrings;
         end;
{++ IDB}
{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}
      if FProject <> nil then
         FProject.IDBSettings.bUseGlobalIDBSettings := False;
      WinGISMainForm.WeAreUsingTheIDB[FProject] := IDB_Enabled_CB.Checked;
      WinGISMainForm.WeAreUsingTheIDBUserInterface[SELF.FProject] := IDB_Interface_CB.Checked;
      WinGISMainForm.WeAreUsingUndoFunction[SELF.FProject] := IDB_UNDO_CB.Checked;
(*brovak    if (FProject <> nil) and (WinGISMainForm.IDBUndoStepsCount[SELF.FProject] <> StrToInt (IDB_UndoStepsCount_Edit.Text)) then
       WinGISMainForm.IDB_Man.OnChangeUndoStepsCount (FProject, StrToInt (IDB_UndoStepsCount_Edit.Text));
brovak*)
      try
         WinGISMainForm.IDBUndoStepsCount[SELF.FProject] := StrToInt (IDB_UndoStepsCount_Edit.Text);
      except
      end;

      bLibraryWasLoaded := WinGISMainForm.SwitchIDBOn (FProject);
      if bLibraryWasLoaded then
         if (FProject <> nil) then
         begin
            if WinGISMainForm.WeAreUsingTheIDB[FProject] then begin
               WinGISMainForm.IDB_Man.AddProject (FProject);
            end
         end
         else
         begin
          // If the IDB library was loaded successfully, add all open projects in it.
            for iI := 0 to WinGISMainForm.MDIChildCount - 1 do
            begin
               AForm := WinGISMainForm.MDIChildren[iI];
               if (AForm is TMDIChild) and (WinGISMainForm.WeAreUsingTheIDB[TMDIChild (AForm).Data]) then
                  WinGISMainForm.IDB_Man.AddProject (TMDIChild (AForm).Data);
            end; // For iI end
         end;

{++ UndoRedo}
      bLibraryWasLoaded := WinGISMainForm.SwitchUndoRedoOn (FProject);
//      bLibraryWasLoaded := WinGISMainForm.UndoRedo_Man.LibraryWasLoaded;
      if bLibraryWasLoaded then
         if (FProject <> nil) then
         begin
            if WinGISMainForm.WeAreUsingUndoFunction[FProject] then
               WinGISMainForm.UndoRedo_Man.AddProject (FProject);
         end
         else
         begin
          // If the IDB library was loaded successfully, add all open projects in it.
            for iI := 0 to WinGISMainForm.MDIChildCount - 1 do
            begin
               AForm := WinGISMainForm.MDIChildren[iI];
               if (AForm is TMDIChild) and (WinGISMainForm.WeAreUsingUndoFunction[TMDIChild (AForm).Data]) then
                  WinGISMainForm.UndoRedo_Man.AddProject (TMDIChild (AForm).Data);
            end; // For iI end
         end;

      for iI := 0 to WinGISMainForm.MDIChildCount - 1 do
      begin
         AForm := WinGISMainForm.MDIChildren[iI];
         if (AForm is TMDIChild) then
         begin
// ++ Cadmensky IDB Version 2.3.1
            if (not WinGISMainForm.WeAreUsingTheIDB[TMDIChild (AForm).Data]) and
               (WinGISMainForm.IDB_Man <> nil) then
               WinGISMainForm.IDB_Man.DeleteProject (TMDIChild (AForm).Data);
// -- Cadmensky IDB Version 2.3.1
            if (not WinGISMainForm.WeAreUsingUndoFunction[TMDIChild (AForm).Data]) and
               (WinGISMainForm.UndoRedo_Man <> nil) then
               WinGISMainForm.UndoRedo_Man.DeleteProject (TMDIChild (AForm).Data);
         end;
      end; // For iI end

{-- UndoRedo}
    // Write all global IDB settings in WinGIS.ini file.
      if FProject = nil then
      begin
//         IniFile.WriteBoolean ('InternalDatabase', 'IDB', WinGisMainForm.WeAreUsingTheIDB[nil]);
//         IniFile.WriteBoolean ('InternalDatabase', 'IDB_Interface', WinGisMainForm.WeAreUsingTheIDBUserInterface[nil]);
         IniFile.WriteBoolean ('UndoRedo', 'IDB_Undo', WinGisMainForm.WeAreUsingUndoFunction[nil]);
         IniFile.WriteInteger ('UndoRedo', 'IDB_UndoStepsCount', WinGisMainForm.IDBUndoStepsCount[nil]);
      end;
{$ENDIF}
{$ENDIF} // <----------------- AXDLL
{-- IDB}
      if FOrgDDEMLApps <> nil then
         for Cnt := 0 to FOrgDDEMLApps.Count - 1 do
         begin
            ListItem := DDEMLListView.Items[Cnt];
            OrgDDEMLApp := OrgDDEMLAppFromName (ListItem.Caption);
            if (OrgDDEMLApp <> nil) and (not OrgDDEMLApp^.Connected) then
            begin
               OrgDDEMLApp^.Connect := ListItem.Checked;
               OrgDDEMLApp^.SendStrings := Boolean (ListItem.Data)
            end;
         end;
{$IFNDEF WMLT}
      DDEHandler.UpdateAppLists;
{$ENDIF}
   end;
end;

procedure TDatabaseOptionsDialog.IDB_Enabled_CBClick (Sender: TObject);
begin
     IDB_Interface_CB.Checked := IDB_Enabled_CB.Checked;
     IDB_Interface_CB.Enabled := IDB_Enabled_CB.Checked;
end;

{++ IDB}

procedure TDatabaseOptionsDialog.UseGlobalIDBCheckClick (Sender: TObject);
begin
{$IFNDEF WMLT}
   if FileExists (ExtractFilePath (Application.ExeName) + 'IDB.DLL') then
   begin
      IDB_Enabled_CB.Checked := FProject.IDBSettings.bLocalWeAreUsingTheIDB;
      IDB_Enabled_CB.Enabled := TRUE;
      IDB_Interface_CB.Checked := FProject.IDBSettings.bLocalWeAreUsingTheIDBUserInterface;
      IDB_Interface_CB.Enabled := TRUE;
   end
   else
   begin
      IDB_Enabled_CB.Checked := false;
      IDB_Enabled_CB.Enabled := false;
      IDB_Interface_CB.Checked := false;
      IDB_Interface_CB.Enabled := false;
   end;

   if FileExists (ExtractFilePath (Application.ExeName) + 'UndoRedo.DLL') then
   begin
      IDB_UNDO_CB.Checked := FProject.IDBSettings.bLocalWeAreUsingUndoFunction;
      IDB_UNDO_CB.Enabled := TRUE;
      IDB_UndoStepsCount_Label.Enabled := true;
      IDB_UndoStepsCount_Edit.Enabled := true;
      IDB_UndoStepsCount_Edit.Text := IntToStr (FProject.IDBSettings.iLocalUndoStepsCount);
      IDB_Enabled_CBClick (SENDER);
   end
   else
   begin
      IDB_UNDO_CB.Checked := false;
      IDB_UNDO_CB.Enabled := false;
      IDB_UndoStepsCount_Label.Enabled := false;
      IDB_UndoStepsCount_Edit.Enabled := false;
      IDB_UndoStepsCount_Edit.Text := IntToStr (WinGISMainForm.IDBUndoStepsCount[Self.FProject]);
   end;
   {$ENDIF}
end;
{-- IDB}

procedure TDatabaseOptionsDialog.IDB_UNDO_CBClick (Sender: TObject);
begin
   IDB_UndoStepsCount_Label.Enabled := IDB_UNDO_CB.Checked;
   IDB_UndoStepsCount_Edit.Enabled := IDB_UNDO_CB.Checked;
end;

end.

