Unit AVLTree;

Interface

Uses Objects;

Const CPEQUAL      =  0;
      CPSMALLER    = -1;
	  CPBIGGER     =  1;

Type {***********************************************************************}
     { Object TAVLNode                                                       }
     {   Type eines Knotens im Baum.                                         }
     {***********************************************************************}
     PAVLNode      = ^TAVLNode;
     TAVLNode      = Object
       BalFact     : ShortInt;
       Left        : PAVLNode;
       Right       : PAVLNode;
       Last        : PAVLNode;
       Data        : Pointer;
       Constructor Init(AData:Pointer);
     end;

     {***********************************************************************}
     { Object TAVLTree                                                       }
     {***********************************************************************}
     PAVLTree      = ^TAVLTree;
     TAVLTree      = Object
       AvlRoot     : PAVLNode;
	   Constructor Init;
	   Destructor  Done;
	   Function    Compare(Key1,Key2:Pointer):Integer; virtual;
	   Function    Delete(Data:Pointer;var DData:Pointer):Boolean;
	   Function    FirstThat(Test:Pointer):Pointer;
       Function    ForList(Comp,Test:Pointer;SData:Pointer):Boolean;
	   Function    Free(Data:Pointer):Boolean;
       Procedure   FreeItem(Data:Pointer); virtual;
	   Function    Insert(Data:Pointer):Boolean;
	   Function    LastThat(Test:Pointer):Pointer;
	   Function    Replace(Data:Pointer):Boolean;
	   Function    Search(SData:Pointer;var Data:Pointer):Boolean;

	   Procedure   AllBalanceFactors(Tree:PAVLNode;var Height:Integer);
       Procedure   BalanceTree(var CrTree:PAVLNode);
       Procedure   DeleteAllTree(var DelTree:PAVLNode);
       Procedure   DoubleRotLeft(var CritNode:PAVLNode);
       Procedure   DoubleRotRight(var CritNode:PAVLNode);
       Function    HighestNode(Node:PAVLNode):PAVLNode;
       Function    LastNode(Node:PAVLNode):PAVLNode;
       Function    LowestNode(Node:PAVLNode):PAVLNode;
	   Function    NextNode(Node:PAVLNode):PAVLNode;
       Function    RemoveNode(var DelTree,BTree:PAVLNode;DTree:PAVLNode):Pointer;
       Function    SearchNode(SData:Pointer;var Data:Pointer;Tree:PAVLNode):PAVLNode;
       Procedure   SingleRotLeft(var CritNode:PAVLNode);
       Procedure   SingleRotRight(var CritNode:PAVLNode);
     end;

Implementation

Type			TTestFunc    = Function(IndData:Pointer;AltBP:Word):Boolean;
      TCompFunc    = Function(Key1,Key2:Pointer):Integer;

      PBA          = ^TByteArray;
			TByteArray   = Array[0..65534] of Byte;

Const BFMIN2       = -2;
      BFMIN1       = -1;
      BFZERO       =  0;
      BFPL1        =  1;
      BFPL2        =  2;

{****************************************************************************}
{ Function TAVLTree.Compare                                                  }
{   Vergleicht zwei Eintr�ge des Baumes miteinander. Dient zum einsortieren  }
{   und zur Suche eines Datenobjektes. Diese Methode mu� �bersschrieben      }
{   werden, damit die Baum-Routinen mit den verwendeten Daten arbeiten       }
{   k�nnen.                                                                  }
{****************************************************************************}
Function TAVLTree.Compare
   (
   Key1            : Pointer;
   Key2            : Pointer
   )
   : Integer;
  begin
    RunError(210);
  end;

{****************************************************************************}
{ Function TAVLTree.Delete                                                   }
{   Sucht ein Datenobjekt im Baum und entfernt den zugeh�rigen Knoten aus    }
{   dem Baum. Der Baum wird anschlie�end neu ausbalanziert. Das DatenObjekt  }
{   wird nicht von Heap entfernt sondern der Pointer darauf zur�ckgegeben,   }
{   damit bei mehrfachverkn�pften B�umen die Daten des anderen nicht         }
{   zerst�rt werden. Sollen die Daten auch gleich entfernt werden, sollte    }
{   die Methode Free aufgerufen werden. Die Funktion liefert FALSE, wenn     }
{   das zu l�schende Datenobjekt nicht gefunden wurde.                       }
{****************************************************************************}
Function TAVLTree.Delete
   (
   Data            : Pointer;
   var DData       : Pointer
   )
   : Boolean;
  var BalTree      : PAVLNode;
      DTree        : PAVLNode;
      FData        : Pointer;
      Dummy        : Integer;
  begin
    Delete:=TRUE;
    DTree:=SearchNode(Data,FData,AvlRoot);
    if DTree=NIL then Delete:=FALSE
    else begin
      DData:=RemoveNode(AvlRoot,BalTree,DTree);
      AllBalanceFactors(AvlRoot,Dummy);
      while BalTree<>NIL do begin
        if (BalTree^.BalFact=BFPL2) or (BalTree^.BalFact=BFMIN2) then begin
          if BalTree=AvlRoot then begin
            BalanceTree(BalTree);
            AvlRoot:=BalTree;
          end
          else BalanceTree(BalTree);
          AllBalanceFactors(AvlRoot,Dummy);
        end;
        BalTree:=BalTree^.Last;
      end;
    end;
  end;

{****************************************************************************}
{ Function TAVLTree.FirstThat                                                }
{   FirstThat ruft f�r alle Datenobjekte die als Adresse �bergebene Funktion }
{   mit dem Pointer auf dieses Datenobjekt auf, und zwar in aufsteigender    }
{   Reihenfolge. Die aufzurufende Funktion mu� als lokale Funktion der       }
{   Funktion definiert sein, von der der Aufruf von FirstThat erfolgt.       }
{   Die aufzurufende Funktion kann auf die lokalen Daten dieser Funktion     }
{   zugreifen. Die aufzurufende Funktion mu� au�erdem vom Aufruftyp Far sein }
{   (TURBO 5.5 : Compilerdirektieve $F+, TURBO 6.0 : Far direktieve).        }
{   Die Funktion FirstThat ruft solange die aufzurufende Funktion auf, bis   }
{   diese FALSE zur�ckliefert. FirstThat liefert dann als Funktionswert den  }
{   Pointer auf das aktuelle DatenObjekt.                                    }
{****************************************************************************}
Function TAVLTree.FirstThat
	 (
	 Test            : Pointer
	 )
	 : Pointer;
	var AktNode      : PAVLNode;
			Found        : Boolean;
      AltBP        : Integer;
	begin
		AktNode:=LowestNode(AVLRoot);
		Found:=FALSE;
    ASM MOV EAX,[EBP]
        MOV AltBp,EAX
    end;
		while (AktNode<>NIL) and not(Found) do begin
			Found:=TTestFunc(Test)(AktNode^.Data,AltBP);
			AktNode:=NextNode(AktNode);
		end;
		if Found then FirstThat:=AktNode^.Data
		else FirstThat:=NIL;
	end;

{****************************************************************************}
{ Function TAVLTree.ForList                                                  }
{   ForList sucht mittels der �bergebenen Comparefunktion den kleinsten      }
{   Eintrag, f�r den die Comparefunktion CPEQUAL liefert. Dann wird in       }
{   aufsteigender Reihenfolge mit jeden Datenobjekt die zweite (Test)        }
{   Funktion aufgerufen, bis die Comparemethode nicht mehr CPEQUAL liefert.  }
{   Liefert die Testfunktion TRUE zur�ck, so wird der Vorgang abgebrochen.   }
{   F�r die Testfunktion gelten die gleichen Voraussetzungen wie f�r die von }
{   FirstThat. Die Comparefunktion darf nicht als Nested-Funktion deklariert }
{   werden. ForList liefert FALSE, sofern die Comparefunktion f�r keinen     }
{   Eintrag CPEQUAL liefert.                                                 }
{****************************************************************************}
Function TAVLTree.ForList
   (
   Comp            : Pointer;
   Test            : Pointer;
   SData           : Pointer
   )
   : Boolean;
  var Found        : Boolean;
      AltBP        : Integer;

  {$F+}
  Function DoAll
     (
     Item          : Pointer
     )
     : Boolean;
    begin
      if TCompFunc(Comp)(SData,Item)=CPEQUAL then begin
        DoAll:=TTestFunc(Test)(Item,AltBP);
        Found:=TRUE;
      end
      else DoAll:=FALSE;
    end;
  {$F-}

  begin
    Found:=FALSE;  {***************}
    ASM MOV EAX,[EBP]
        MOV AltBp,EAX
    end;
    FirstThat(@DoAll);
    ForList:=Found;
  end;

{****************************************************************************}
{ Function TAVLTree.Free                                                     }
{   Sucht nach einem Datenobjekt und l�scht den entsprechenden Knoten aus    }
{   dem Baum, sowie das Datenobjekt von Heap durch den Aufruf des Done       }
{   Destrukors. Free liefert FALSE, wenn das Suchobjekt nicht gefunden wurde.}
{****************************************************************************}
Function TAVLTree.Free
   (
   Data            : Pointer
   )
   : Boolean;
  var Success      : Boolean;
      DData        : Pointer;
  begin
    Success:=Delete(Data,DData);
    if Success then FreeItem(DData);
    Free:=Success;
  end;

{****************************************************************************}
{ Function TAVLTree.Insert                                                   }
{   F�gt ein Datenobjekt in den Baum ein. Insert liefert FALSE, wenn die     }
{   Comparemethode w�hrend des Einf�ges CPEQUAL liefert. Der Datenzeiger     }
{   des neuen Baumknotens wird auf Data gesetzt. Data darf daher von auf-    }
{   rufenden Programm nicht mehr von Heap gel�scht werden.                   }
{****************************************************************************}
Function TAVLTree.Insert
   (
   Data            : Pointer
   )
   : Boolean;
  var HelpT        : PAVLNode;
      SeekT        : PAVLNode;
      Comp         : Integer;
  begin
    SeekT:=AvlRoot;
    HelpT:=NIL;
    Insert:=TRUE;
    while SeekT<>NIL do begin
      HelpT:=SeekT;
			Comp:=Compare(Data,SeekT^.Data);
			case Comp of
        CPEQUAL   : begin
          Insert:=FALSE;
          Exit;
        end;
        CPSMALLER : SeekT:=SeekT^.Left;
        CPBIGGER  : SeekT:=SeekT^.Right;
      end;
    end;
    SeekT:=New(PAVLNode,Init(Data));
    if HelpT=NIL then AvlRoot:=SeekT
    else begin
      SeekT^.Last:=HelpT;
      Case Comp of
        CPSMALLER,CPEQUAL : HelpT^.Left:=SeekT;
        CPBIGGER          : HelpT^.Right:=SeekT;
      end;
      repeat
        HelpT:=SeekT;
        SeekT:=SeekT^.Last;
        with SeekT^ do begin
					if HelpT=Left then Inc(BalFact)
					else Dec(BalFact);
					if (BalFact=BFMIN2) or (BalFact=BFPL2) then begin
						if SeekT=AvlRoot then begin
							BalanceTree(AvlRoot);
							SeekT:=AvlRoot;
						end
						else BalanceTree(SeekT);
					end;
				end;
			until (SeekT^.BalFact=BFZERO) or (SeekT^.Last=NIL);
		end;
	end;

{****************************************************************************}
{ Function TAVLTree.LastThat                                                 }
{   Arbeitet gleich wie FirstThat, nur wird der Baum in ansteigender Reihen- }
{   folge bearbeitet.                                                        }
{****************************************************************************}
Function TAVLTree.LastThat
	 (
	 Test            : Pointer
	 )
	 : Pointer;
	var AktNode      : PAVLNode;
			Found        : Boolean;
      AltBP        : Integer;
	begin
		AktNode:=HighestNode(AVLRoot);
		Found:=FALSE;
    ASM MOV EAX,[EBP]
        MOV AltBp,EAX
    end;
		while (AktNode<>NIL) and not(Found) do begin
			Found:=TTestFunc(Test)(AktNode^.Data,AltBP);
			AktNode:=LastNode(AktNode);
		end;
		if Found then LastThat:=AktNode^.Data
		else LastThat:=NIL;
	end;

{****************************************************************************}
{ Function TAVLTree.Replace                                                  }
{   Ersetzt ein Datenobjekt des Baumes durch das �bergebene. Der Sortier-    }
{   Schl�ssel des Objektes kann nicht ge�ndert werden, daher wird auch nur   }
{   ein Datenobjekt als Parameter ben�tigt. Das alte Datenobjekt wird durch  }
{   den Aufruf des Done Destructors von Heap entfernt und das neue an den    }
{   Baumknoten geh�ngt. Data darf daher von aufrufenden Programm nicht mehr  }
{   vom Heap gel�scht werden.                                                }
{****************************************************************************}
Function TAVLTree.Replace
   (
   Data            : Pointer
   )
   : Boolean;
  var RepNode      : PAVLNode;
      RData        : Pointer;
  begin
    Replace:=TRUE;
    RepNode:=SearchNode(Data,RData,AvlRoot);
    if RepNode=NIL then Replace:=FALSE
    else begin
      FreeItem(RData);
      RepNode^.Data:=Data;
    end;
  end;

{****************************************************************************}
{ Function TAVLTree.Search                                                   }
{   Sucht nach einem Datenobjekt im Baum und liefert einen Pointer auf das   }
{   gefundene Datenobjekt. SData mu� nur den Suchschl�ssel enthalten, der    }
{   in der Comparemethode verwendet wird. Search liefert TRUE, wenn das      }
{   Objekt gefunden wurde.                                                   }
{****************************************************************************}
Function TAVLTree.Search
   (
   SData           : Pointer;
   var Data        : Pointer
   )
   : Boolean;
  begin
    if SearchNode(SData,Pointer(Data),AvlRoot)=NIL then Search:=FALSE
    else Search:=TRUE;
  end;

Procedure TAVLTree.AllBalanceFactors
   (
   Tree            : PAVLNode;
   var Height      : Integer
   );
  var B            : Integer;
      HLeft        : Integer;
      HRight       : Integer;
  begin
    if Tree=NIL then Height:=0
    else begin
      AllBalanceFactors(Tree^.Left,HLeft);
      AllBalanceFactors(Tree^.Right,HRight);
			B:=HLeft-HRight;
			Tree^.BalFact:=B;
			if B<0 then Height:=HRight
			else Height:=HLeft;
			Inc(Height);
    end;
  end;

Procedure TAVLTree.BalanceTree
   (
   var CrTree      : PAVLNode
   );
	var Dummy        : Integer;
	begin
		with CrTree^ do begin
      if (BalFact>BFZERO) and
					(Left^.BalFact<BFZERO) then
				DoubleRotRight(CrTree)
			else if (BalFact<BFZERO) and
					(Right^.BalFact>BFZERO) then
				DoubleRotLeft(CrTree)
			else if (BalFact>BFZERO) and
					(Left^.BalFact>=BFZERO) then
				SingleRotRight(CrTree)
			else If (BalFact<BFZERO) and
					(Right^.BalFact<=BFZERO) then
				SingleRotLeft(CrTree);
			AllBalanceFactors(CrTree,Dummy);
    end;
  end;

Procedure TAVLTree.DeleteAllTree
   (
   var DelTree     : PAVLNode
   );
  begin
    if DelTree<>NIL then begin
			DeleteAllTree(DelTree^.Left);
			DeleteAllTree(DelTree^.Right);
			Dispose(DelTree);
			DelTree:=NIL;
		end;
	end;

Procedure TAVLTree.DoubleRotLeft
   (
   var CritNode    : PAVLNode
   );
	var Crit         : PAVLNode;
			CritR        : PAVLNode;
			CritRL       : PAVLNode;
			CrLL         : PAVLNode;
			CrLR         : PAVLNode;
  begin
    Crit:=CritNode;
    CritR:=Crit^.Right;
    CritRL:=CritR^.Left;
    CrLL:=CritRL^.Left;
    CrLR:=CritRL^.Right;
    CritRL^.Last:=Crit^.Last;
    if Crit^.Last<>NIL then begin
      if Crit^.Last^.Left=Crit then Crit^.Last^.Left:=CritRL
      else Crit^.Last^.Right:=CritRL;
    end;
    CritNode:=CritRL;
    CritRL^.Left:=Crit;
    Crit^.Last:=CritRL;
    CritRL^.Right:=CritR;
    CritR^.Last:=CritRL;
    Crit^.Right:=CrLL;
    CritR^.Left:=CrLR;
    if CrLL<>NIL then CrLL^.Last:=Crit;
    if CrLr<>NIL then CrLR^.Last:=CritR;
  end;

Procedure TAVLTree.DoubleRotRight
   (
   var CritNode    : PAVLNode
   );
  var Crit         : PAVLNode;
      CritL        : PAVLNode;
      CritLR       : PAVLNode;
      ClRR         : PAVLNode;
      ClRL         : PAVLNode;
  begin
    Crit:=CritNode;
    CritL:=Crit^.Left;
    CritLR:=CritL^.Right;
    ClRL:=CritLR^.Left;
    ClRR:=CritLR^.Right;
    CritLR^.Last:=Crit^.Last;
    if Crit^.Last<>NIL then begin
      if Crit^.Last^.Left=Crit then Crit^.Last^.Left:=CritLR
      else Crit^.Last^.Right:=CritLR;
    end;
    CritNode:=CritLR;
    CritLR^.Right:=Crit;
    Crit^.Last:=CritLR;
    CritLR^.Left:=CritL;
    CritL^.Last:=CritLR;
    CritL^.Right:=ClRL;
    Crit^.Left:=ClRR;
    if ClRL<>NIL then ClRL^.Last:=CritL;
    if ClRR<>NIL then ClRR^.Last:=Crit;
  end;

Function TAVLTree.HighestNode
   (
   Node            : PAVLNode
   )
   : PAVLNode;
  begin
    if Node=NIL then HighestNode:=NIL
    else begin
      while Node^.Right<>NIL do
          Node:=Node^.Right;
      HighestNode:=Node;
    end;
  end;

Function TAVLTree.LastNode
   (
   Node            : PAVLNode
   )
   : PAVLNode;
  var UpperNode    : PAVLNode;
  begin
    if Node=NIL then LastNode:=NIL
    else begin
      if Node^.Left<>NIL then
          LastNode:=HighestNode(Node^.Left)
      else begin
        UpperNode:=Node;
        repeat
          Node:=UpperNode;
          UpperNode:=UpperNode^.Last;
        until (UpperNode=NIL) or (UpperNode^.Right=Node);
        LastNode:=UpperNode;
      end;
    end;
  end;

Function TAVLTree.LowestNode
   (
   Node            : PAVLNode
   )
   : PAVLNode;
  begin
    if Node=NIL then LowestNode:=NIL
    else begin
      while Node^.Left<>NIL do
          Node:=Node^.Left;
      LowestNode:=Node;
    end;
  end;

Function TAVLTree.NextNode
   (
   Node            : PAVLNode
   )
   : PAVLNode;
  var UpperNode    : PAVLNode;
  begin
    if Node=NIL then NextNode:=NIL
    else begin
      if Node^.Right<>NIL then
          NextNode:=LowestNode(Node^.Right)
      else begin
        UpperNode:=Node;
        repeat
          Node:=UpperNode;
          UpperNode:=UpperNode^.Last;
        until (UpperNode=NIL) or (UpperNode^.Left=Node);
        NextNode:=UpperNode;
      end;
    end;
  end;

Function TAVLTree.RemoveNode
   (
   var DelTree     : PAVLNode;
   var BTree       : PAVLNode;
   DTree           : PAVLNode
   )
   : Pointer;
  var SucTree      : PAVLNode;
      HelpT        : PAVLNode;
      Swapp        : Pointer;
  begin
    if DTree<>NIL then begin
      if (DTree^.Left=NIL) and (DTree^.Right=NIL) then begin
        if DTree^.Last=NIL then begin
          DelTree:=NIL;
          BTree:=NIL;
        end
        else with DTree^.Last^ do if Left=DTree then Left:=NIL
        else Right:=NIL;
        BTree:=DTree^.Last;
        RemoveNode:=DTree^.Data;
        Dispose(DTree);
        DTree:=NIL;
      end
      else if (DTree^.Left<>NIL) xor (DTree^.Right<>NIL) then begin
        if DTree^.Left=NIL then SucTree:=DTree^.Right
        else SucTree:=DTree^.Left;
        if DTree^.Last<>NIL then with DTree^.Last^ do begin
          if Left=DTree then Left:=SucTree
          else Right:=SucTree;
        end;
        SucTree^.Last:=DTree^.Last;
        if DTree=DelTree then DelTree:=SucTree;
        RemoveNode:=DTree^.Data;
        Dispose(DTree);
        DTree:=NIL;
        BTree:=SucTree;
      end
      else begin
        HelpT:=LowestNode(DTree^.Right);
        Swapp:=DTree^.Data;
        DTree^.Data:=HelpT^.Data;
        HelpT^.Data:=Swapp;
        RemoveNode:=RemoveNode(DelTree,BTree,HelpT);
      end;
    end;
  end;

Function TAVLTree.SearchNode
   (
   SData           : Pointer;
   var Data        : Pointer;
   Tree            : PAVLNode
   )
   : PAVLNode;
  begin
    if Tree=NIL then SearchNode:=NIL
    else begin
      repeat
        case Compare(SData,Tree^.Data) of
          CPEQUAL   : begin
            Data:=Tree^.Data;
            SearchNode:=Tree;
            Exit;
          end;
          CPSMALLER : Tree:=Tree^.Left;
          CPBIGGER  : Tree:=Tree^.Right;
        end;
      until Tree=NIL;
      SearchNode:=NIL;
      Data:=NIL;
    end;
  end;

Procedure TAVLTree.SingleRotLeft
   (
   var CritNode    : PAVLNode
   );
  var Crit         : PAVLNode;
      CritR        : PAVLNode;
      CritRL       : PAVLNode;
  begin
    Crit:=CritNode;
    CritR:=Crit^.Right;
    CritRL:=CritR^.Left;
    CritR^.Last:=Crit^.Last;
    if Crit^.Last<>NIL then begin
      if Crit^.Last^.Left=Crit then
        Crit^.Last^.Left:=CritR
      else Crit^.Last^.Right:=CritR;
    end;
    CritNode:=CritR;
    CritR^.Left:=Crit;
    Crit^.Last:=CritR;
    Crit^.Right:=CritRL;
    if CritRL<>NIL then CritRL^.Last:=Crit;
  end;

Procedure TAVLTree.SingleRotRight
   (
   var CritNode    : PAVLNode
   );
  var Crit         : PAVLNode;
      CritL        : PAVLNode;
      CritLR       : PAVLNode;
  begin
    Crit:=CritNode;
    CritL:=Crit^.Left;
    CritLR:=CritL^.Right;
    CritL^.Last:=Crit^.Last;
    if Crit^.Last<>NIL then begin
      if Crit^.Last^.Left=Crit then Crit^.Last^.Left:=CritL
      else Crit^.Last^.Right:=CritL;
    end;
    CritNode:=CritL;
    CritL^.Right:=Crit;
    Crit^.Last:=CritL;
    Crit^.Left:=CritLR;
    if CritLR<>NIL then CritLR^.Last:=Crit;
  end;

Constructor TAVLNode.Init
   (
   AData           : Pointer
   );
  begin
		BalFact:=BFZERO;
    Left:=NIL;
    Last:=NIL;
    Right:=NIL;
    Data:=AData;
  end;

Destructor TAVLTree.Done;
  begin
    DeleteAllTree(AvlRoot);
  end;

Constructor TAVLTree.Init;
  begin
   AvlRoot:=NIL;
  end;

Procedure TAvlTree.FreeItem
   (
   Data            : Pointer
   );
  begin
  end;

end.