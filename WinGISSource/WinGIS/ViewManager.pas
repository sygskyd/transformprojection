Unit ViewManager;

Interface

Uses SysUtils,Windows,Messages,Classes,Graphics,Controls,Dialogs,Forms,Menus,
     ProjHndl,RollUp,StdCtrls,TreeView,WCtrls,AM_Proj,AM_Sight,ExtCtrls,ActnList,
     ComCtrls, ColumnList, ImgList, MultiLng, InplEdit, Validate,AM_Paint,
     AM_Layer,AM_Group, Combobtn,AM_Main, Buttons,GrTools,AM_def;

Type TViewManagerRollupForm  = Class(TProjRollupForm)
       ActionList       : TActionList;
       ActivateAction   : TAction;
       ActivateMenu     : TMenuItem;
       MlgSection       : TMlgSection;
       N1               : TMenuItem;
       N2               : TMenuItem;
       N3               : TMenuItem;
       NewMenu          : TMenuItem;
       NewViewAction    : TAction;
       PageControl      : TPageControl;
       PropertiesAction : TAction;
       PropertiesMenu   : TMenuItem;
       RemoveMenu       : TMenuItem;
       RemoveViewAction : TAction;
       Rollup           : TRollup;
       UpdateViewMenu   : TMenuItem;
       UpdateLayersMenu : TMenuItem;
       UpdateAllMenu    : TMenuItem;
       UpdateLayersAction: TAction;
       UpdateViewAction : TAction;
       UpdateAllAction  : TAction;
       ViewsListbox     : TColumnListbox;
       ViewsPopupMenu   : TPopupMenu;
       ViewsSheet       : TTabSheet;
       ViewsInplaceEditor: TInplaceEditor;
       ViewNameValidator: TValidator;
       LegendeSymbols: TImage;
       ImageList: TImageList;
       MenuBtn: TPopupMenuBtn;
       SpeedBtn1: TSpeedBtn;
    Label1: TLabel;
       Procedure   ActivateActionExecute(Sender: TObject);
       Procedure   FormResize(Sender: TObject);
       Procedure   FormShow(Sender: TObject);
       Procedure   NewViewActionExecute(Sender: TObject);
       Procedure   PropertiesActionExecute(Sender: TObject);
       Procedure   PropertiesMenuClick(Sender: TObject);
       Procedure   RemoveViewActionExecute(Sender: TObject);
       Procedure   UpdateLayersActionExecute(Sender: TObject);
       Procedure   UpdateAllActionExecute(Sender: TObject);
       Procedure   UpdateViewActionExecute(Sender: TObject);
       Procedure   UpdateViewMenuClick(Sender: TObject);
       Procedure   ViewsListboxColumns0Paint(Control: TWinControl;
                       Index: Integer; Rect: TRect; State: TOwnerDrawState);
       Procedure   ViewsListboxClick(Sender: TObject);
       Procedure   ViewsListboxDblClick(Sender: TObject);
       Procedure   ViewsListboxColumns1Paint(Control: TWinControl;
                       Index: Integer; Rect: TRect; State: TOwnerDrawState);
       Procedure   ViewsSheetResize(Sender: TObject);
       Procedure   ViewsInplaceEditorEditing(Sender: TObject; Index: Integer;
                       var AllowEdit: Boolean; var Text: String);
       Procedure   ViewsInplaceEditorEdited(Sender: TObject; Index: Integer;
                       const Text: String);
       Procedure   ViewsInplaceEditorValidate(Sender: TObject; Index: Integer;
                       const Text: String; var Valid: Boolean);
       Procedure FormCreate(Sender: TObject);
       Procedure FormDestroy(Sender: TObject);
      Private
       FImages     : TWImageList;
       // for symbol drawing...
       FPInfo      : PPaint;
       OvDragDropStarted: Boolean;
       OvDragDropPoint: TPoint;
       CurRect: TGrRect;
       MaxRect: TGrRect;
       LastChild: AnsiString;
       Procedure   CalculateCurrentView;
       Function    AddTreeItem(Parent:TTreeEntry;const ACaption:String):TTreeEntry;
       Procedure   CreateViewSubtree(Parent:TTreeEntry;Sight:PSight);
       Function    GetSelectedView:PSight;
       Procedure   UpdateEnabled;
       Procedure   UpdateViewsList;
      Protected
       Procedure   SetProject(AProject:PProj); override;
      Public
       Procedure   UpdateViews;
     end;

Implementation

{$R *.DFM}

Uses AM_ProjO,CommonResources
     {$IFNDEF WMLT}
     ,NewViewDlg,ViewPropertiesDlg
     {$ENDIF}
     ,NumTools,TreeList,
     UserIntf,Legend,Styledef,XStyles,XLines,XFills,AM_Obj;

{==============================================================================+
  TViewManagerRollup
+==============================================================================}

Function TViewManagerRollupForm.AddTreeItem(Parent:TTreeEntry;const ACaption:String):TTreeEntry;
begin
  Result:=TTreeEntry.Create;
  Result.Caption:=ACaption;
  Result.Bitmap:=-3;
  Parent.Add(Result);
end;

{******************************************************************************+
  TViewManagerRollup.CreateViewSubtree
--------------------------------------------------------------------------------
  Creates the sub-entries for a view-tree.
+******************************************************************************}
Procedure TViewManagerRollupForm.CreateViewSubtree(Parent:TTreeEntry;Sight:PSight);
var Item           : TTreeEntry;
    Cnt            : Integer;
    SightData      : PSightLData;
    Layer          : PLayer;
begin
  if not RectEmpty(Sight^.ViewRect) then begin
    Item:=AddTreeItem(Parent,'Range');
    Item.Expanded:=FALSE;
    AddTreeItem(Item,'X');
    AddTreeItem(Item,'Y');
    AddTreeItem(Item,'Width');
    AddTreeItem(Item,'Height');
    AddTreeItem(Item,'Rotation');
  end;
  if Sight^.LayersData<>NIL then begin
    Item:=AddTreeItem(Parent,'Layer');
    Item.Expanded:=FALSE;
    for Cnt:=0 to Sight^.LayersData^.Count-1 do begin
      SightData:=Sight^.LayersData^.At(Cnt);
      if SightData^.LIndex<>0 then begin
        Layer:=Project^.Layers^.IndexToLayer(SightData^.LIndex);
        if Layer<>NIL then AddTreeItem(Item,PToStr(Layer^.Text));
      end;
    end;
  end;
end;

Procedure TViewManagerRollupForm.UpdateViewsList;
begin
  if (Project = nil) or (Project^.Sights = nil) then begin
     ViewsListbox.Count:=0
  end
  else begin
       ViewsListbox.Count:=Project^.Sights^.Count;
  end;

  ViewsListbox.Invalidate;
  UpdateEnabled;
  UserInterface.Update([uiViews]);
end;

Procedure TViewManagerRollupForm.UpdateViews;
begin
  if Project=NIL then begin
     ViewsListbox.Count:=0
  end
  else if Project^.Sights <> nil then begin
       ViewsListbox.Count:=Project^.Sights^.Count;
  end;
  ViewsListbox.Invalidate;
  UpdateEnabled;
end;

{******************************************************************************+
  Procedure TViewManagerRollup.SetProject
--------------------------------------------------------------------------------
  Called by the user-interface if the active project changes.
+******************************************************************************}
Procedure TViewManagerRollupForm.SetProject(AProject:PProj);
begin
  inherited SetProject(AProject);
  UpdateViewsList;
end;

{==============================================================================+
  Initialisation- and termination-code
+==============================================================================}

Procedure TViewManagerRollupForm.FormResize(Sender: TObject);
begin
//if PageControl.Parent<>NIL then
//     PageControl.BoundsRect:=InflatedRect(PageControl.Parent.ClientRect,-2,-2);
       if PageControl.Parent<>NIL then
          PageControl.BoundsRect:=InflatedWidthHeight(PageControl.Parent.ClientRect,6,6,25,6);
end;

{******************************************************************************+
  TViewManager.SelectedView
--------------------------------------------------------------------------------
  Returns the tree-entry representing the currently selected view. If no
  view selected NIL is returned.
+******************************************************************************}
Function TViewManagerRollupForm.GetSelectedView:PSight;
begin
  if ViewsListBox.Count <= 0 then begin
    Result:=NIL;
    Exit;
  end
  else if ViewsListBox.ItemIndex < 0 then ViewsListBox.ItemIndex := 0;
  Result:=Project^.Sights^.At(ViewsListbox.ItemIndex);
end;

Procedure TViewManagerRollupForm.UpdateViewMenuClick(Sender: TObject);
begin
end;

{******************************************************************************+
  Procedure TViewManagerRollup.UpdateEnabled
--------------------------------------------------------------------------------
  Updates the enabled-status of the manager-controls.
+******************************************************************************}
Procedure TViewManagerRollupForm.UpdateEnabled;
var AEnable        : Boolean;
begin
  AEnable:=Project<>NIL;
  SetControlGroupEnabled(ViewsListbox,AEnable);
  MenuBtn.Enabled:=AEnable;
  AEnable:=ViewsListbox.Count > 0;
  ActivateAction.Enabled:=AEnable;
  UpdateViewAction.Enabled:=AEnable;
  UpdateAllAction.Enabled:=AEnable;
  UpdateLayersAction.Enabled:=AEnable;
  RemoveMenu.Enabled:=AEnable;
end;

Procedure TViewManagerRollupForm.ViewsListboxClick(Sender: TObject);
begin
  UpdateEnabled;
end;

Procedure TViewManagerRollupForm.PropertiesMenuClick(Sender: TObject);
begin
end;

{******************************************************************************+
  TViewManagerRollup.ActivateActionExecute
--------------------------------------------------------------------------------
  Activates the currently selected view. Called by the corresponding menu.
+******************************************************************************}
Procedure TViewManagerRollupForm.ActivateActionExecute(Sender: TObject);
var Sight          : PSight;
begin
  // get the currently selected view
  Sight:=GetSelectedView;
  if Sight<>NIL then ProcSetSight(Project,Sight);
end;

Procedure TViewManagerRollupForm.ViewsListboxDblClick(Sender: TObject);
begin
  ActivateAction.Execute;
end;

{******************************************************************************+
  Procedure TViewManagerRollup.UpdateLayersActionExecute
--------------------------------------------------------------------------------
  Updates the layer-setting of the currently selected view with the
  current layer-priority.
+******************************************************************************}
Procedure TViewManagerRollupForm.UpdateLayersActionExecute(Sender: TObject);
var Sight          : PSight;
begin
  Sight:=GetSelectedView;
  if Sight<>NIL then begin
    Sight^.SetLayers(Project^.Layers);
    Project^.SetModified;
    UpdateViewsList;
  end;
end;

{******************************************************************************+
  TViewManagerRollup.UpdateViewMenuClick
--------------------------------------------------------------------------------
  Updates the view-rectangle stored in the currently selected view with the
  current display-view.
+******************************************************************************}
Procedure TViewManagerRollupForm.UpdateViewActionExecute(Sender: TObject);
var Sight          : PSight;
begin
  Sight:=GetSelectedView;
  if Sight<>NIL then begin
    Sight^.ViewRect:=Project^.PInfo^.GetCurrentScreen;
    Project^.SetModified;
    UpdateViewsList;
  end;
end;

{******************************************************************************+
  TViewManagerRollup.UpdateAllActionExecute
--------------------------------------------------------------------------------
  Updates the view-rectangle and the layer-information stored in the currently
  selected view.
+******************************************************************************}
Procedure TViewManagerRollupForm.UpdateAllActionExecute(Sender: TObject);
var Sight          : PSight;
begin
  Sight:=GetSelectedView;
  if Sight<>NIL then begin
    if Sight^.LayersData <> nil then begin
       Sight^.SetLayers(Project^.Layers);
    end;
    if not RectEmpty(Sight^.ViewRect) then begin
       Sight^.ViewRect:=Project^.PInfo^.GetCurrentScreen;
    end;
    Project^.SetModified;
    UpdateViewsList;
  end;
end;

{******************************************************************************+
  TViewManagerRollupForm.PropertiesActionExecute
--------------------------------------------------------------------------------
  Shows the properties-dialog for the currently selected view.
+******************************************************************************}
Procedure TViewManagerRollupForm.PropertiesActionExecute(Sender: TObject);
{$IFNDEF WMLT}
var Dialog         : TViewPropertiesDialog;
    Sight          : PSight;
    WasDefault     : Boolean;
    DefaultView    : PSight;
    Index          : Integer;
{$ENDIF}
begin
{$IFNDEF WMLT}
  Sight:=GetSelectedView;
  if Sight<>NIL then begin
    Dialog:=TViewPropertiesDialog.Create(Self);
    try
      Dialog.Project:=Project;
      Dialog.Sight:=Sight;
      Index:=Project^.Sights^.IndexOf(Sight);
      if Project^.Sights^.Default<0 then DefaultView:=NIL
      else DefaultView:=Project^.Sights^.At(Project^.Sights^.Default);
      WasDefault:=Project^.Sights^.IndexOf(Sight)=Project^.Sights^.Default;
      Dialog.DefaultView:=WasDefault;
      if Dialog.ShowModal=mrOK then begin
        if Dialog.NameChanged then begin
          Project^.Sights^.AtDelete(Index);
          Project^.Sights^.Insert(Sight);
          Index:=Project^.Sights^.IndexOf(Sight);
          {$IFNDEF WMLT}
          ProcDBSendViews(Project);
          {$ENDIF}
        end;
        if Dialog.DefaultView then Project^.Sights^.Default:=Index
        else if WasDefault then Project^.Sights^.Default:=-1
        else if DefaultView<>NIL then Project^.Sights^.Default:=
            Project^.Sights^.IndexOf(DefaultView);
        if Dialog.Modified then Project^.SetModified;
        UpdateViewsList;
        ViewsListbox.ItemIndex:=Index;
      end;
    finally
      Dialog.Free;
    end;
  end;
{$ENDIF}
end;

{******************************************************************************+
  TViewManagerRollupForm.NewViewActionExecute
--------------------------------------------------------------------------------
  Creates a new view. Displays a dialog-box to enter the name and the
  options for the new view.
+******************************************************************************}
Procedure TViewManagerRollupForm.NewViewActionExecute(Sender: TObject);
{$IFNDEF WMLT}
var Dialog         : TNewViewDialog;
{$ENDIF}
begin
{$IFNDEF WMLT}
  Dialog:=TNewViewDialog.Create(Self);
  try
    Dialog.Project:=Project;
    if Dialog.ShowModal=mrOK then begin
      UpdateViewsList;
      ViewsListbox.ItemIndex:=Dialog.InsertPosition;
      Project^.SetModified;
    end;
  finally
    Dialog.Free;
  end;
{$ENDIF}  
end;

Procedure TViewManagerRollupForm.ViewsSheetResize(Sender: TObject);
begin
  ViewsListbox.BoundsRect:=InflatedRect(ViewsSheet.ClientRect,-8,-8);
  ViewsListbox.Top:=ViewsListbox.Top+15;
  ViewsListbox.Height:=ViewsListbox.Height-15;
  MenuBtn.Left:=ViewsSheet.Width-26;
end;

Procedure TViewManagerRollupForm.ViewsListboxColumns0Paint(
  Control: TWinControl; Index: Integer; Rect: TRect;
  State: TOwnerDrawState);
var Sight          : PSight;
    Bitmap         : Integer;
begin
  with ViewsListbox do begin
    Sight:=Project^.Sights^.At(Index);
    if not RectEmpty(Sight^.ViewRect) and (Sight^.LayersData<>NIL) then Bitmap:=clbLayerZoom
    else if Sight^.LayersData<>NIL then Bitmap:=clbLayerGray
    else Bitmap:=clbZoom;
    CommonListBitmaps.Draw(Canvas,CenterWidthInRect(16,Rect),Rect.Top,Bitmap,TRUE);
  end;
end;

Procedure TViewManagerRollupForm.ViewsListboxColumns1Paint(
  Control: TWinControl; Index: Integer; Rect: TRect;
  State: TOwnerDrawState);
var Sight          : PSight;
begin
  with ViewsListbox do begin
    Sight:=Project^.Sights^.At(Index);
    if Index=Project^.Sights^.Default then Canvas.Font.Style:=Canvas.Font.Style+[fsBold];
    TextRectEx(Canvas,Rect,PToStr(Sight^.Name),Color,State);
  end;
end;

Procedure TViewManagerRollupForm.RemoveViewActionExecute(Sender: TObject);
var Sight          : PSight;
    Index          : Integer;
begin
  Sight:=GetSelectedView;                    
  if (Sight<>NIL) and (MessageDialog(Format(MlgSection[12],[PToStr(Sight^.Name)]),
      mtConfirmation,[mbYes,mbNo],0)=mrYes) then begin
    Index:=Project^.Sights^.IndexOf(Sight);
    Project^.Sights^.AtFree(Index);
    if Index=Project.Sights.Default then Project.Sights.Default:=-1
    else if Index<Project.Sights.Default then Dec(Project^.Sights^.Default);
    Project^.SetModified;
    UpdateViewsList;
    ProcDBSendViews(Project);
  end;
end;

Procedure TViewManagerRollupForm.FormShow(Sender: TObject);
begin
  ViewsInplaceEditor.Rehook;
  UpdateEnabled;
end;

Procedure TViewManagerRollupForm.ViewsInplaceEditorEditing(Sender:TObject;
  Index:Integer; var AllowEdit:Boolean; var Text:String);
begin
  Text:=PToStr(PSight(Project^.Sights^.At(Index))^.Name);
end;

Procedure TViewManagerRollupForm.ViewsInplaceEditorEdited(Sender: TObject;
  Index: Integer; const Text: String);
var Sight          : PSight;
    DefaultView    : PSight;
begin
  with Project^.Sights^ do begin
    if Default<0 then DefaultView:=NIL
    else DefaultView:=At(Default);
    Sight:=At(Index);
    AtDelete(Index);
    AssignStr(Sight^.Name,Text);
    Insert(Sight);
    if DefaultView<>NIL then Default:=IndexOf(DefaultView);
    ViewsListbox.ItemIndex:=IndexOf(Sight);
  end;
end;

Procedure TViewManagerRollupForm.ViewsInplaceEditorValidate(
  Sender: TObject; Index: Integer; const Text: String; var Valid: Boolean);
var Sight          : PSight;
begin
  Sight:=Project^.Sights^.At(Index);
  Valid:=TRUE;
  if (AnsiCompareText(PToStr(Sight^.Name),Text)<>0)
      and (Project^.Sights^.NamedSight(Text)<>NIL) then begin
    MessageDialog(MlgSection[13],mtError,[mbOK],0);
    Valid:=FALSE;
  end;
end;

Procedure TViewManagerRollupForm.FormCreate(Sender: TObject);
begin
  FImages:=TWImageList.CreateSize(16,16);
  FImages.AddBitmaps(LegendeSymbols.Picture.Bitmap);
  // setup pinfo (for drawing symbols)
  //FPInfo:=New(PPaint,Init);
  //FPInfo^.ExtCanvas.Cached:=FALSE;
  //Dispose(FPInfo^.Fonts,Done);
  //FPinfo^.Fonts:=nil;
  //Scaleby(Trunc(9600000/(PixelsPerInch)),100000);
end;

Procedure TViewManagerRollupForm.FormDestroy(Sender: TObject);
begin
  FImages.Free;
  //Dispose(FPInfo,Done);
end;

procedure TViewManagerRollupForm.CalculateCurrentView;
var
TmpRect: TGrRect;
begin
     with WingisMainForm.ActualChild do begin
          TmpRect:=ISOFrame(RotatedRect(Data^.PInfo^.GetCurrentScreen,Data^.PInfo^.ViewRotation));
          CurRect.Left:=TmpRect.Left;
          CurRect.Bottom:=TmpRect.Bottom;
          CurRect.Right:=TmpRect.Right;
          CurRect.Top:=TmpRect.Top;

          TmpRect:=ISOFrame(RotatedRect(Data^.ProjSizeOfScreen,Data^.PInfo^.ViewRotation));
          MaxRect.Left:=TmpRect.Left;
          MaxRect.Bottom:=TmpRect.Bottom;
          MaxRect.Right:=TmpRect.Right;
          MaxRect.Top:=TmpRect.Top;

          CurRect.Left:=Max(CurRect.Left,MaxRect.Left);
          CurRect.Bottom:=Max(CurRect.Bottom,MaxRect.Bottom);
          CurRect.Right:=Min(CurRect.Right,MaxRect.Right);
          CurRect.Top:=Min(CurRect.Top,MaxRect.Top);
     end;
end;
end.
