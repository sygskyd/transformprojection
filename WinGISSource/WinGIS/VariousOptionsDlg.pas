Unit VariousOptionsDlg;

Interface

Uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Spinbtn, WCtrls,AM_Def,Palette, Validate,
  MultiLng, LicenseHndl;

Type TVariousOptionsDialog = Class(TWForm)
       ControlPanel1    : TPanel;
       UseGlobalsCheck  : TCheckBox;
       Bevel1           : TBevel;
       OKBtn            : TButton;
       CancelBtn        : TButton;
       SnapRadiusVal    : TMeasureLookupValidator;
       ZoomSizeVal      : TMeasureLookupValidator;
       CombineRadiusVal : TMeasureLookupValidator;
       ControlPanel2    : TPanel;
       Group1           : TWGroupBox;
       Label11          : TWLabel;
       ShowPointsCheck  : TCheckBox;
       ZoomSizeEdit     : TEdit;
       ZoomSizeSpin     : TSpinBtn;
       Group2           : TWGroupBox;
       Label12          : TWLabel;
       CombineRadiusEdit: TEdit;
       CombineRadiusSpin: TSpinBtn;
       Group3           : TWGroupBox;
       Label13          : TWLabel;
       SnapToNearestCheck: TCheckBox;
       SnapRadiusEdit   : TEdit;
       SnapRadiusSpin   : TSpinBtn;
       MlgSection       : TMlgSection;
       {++ Brovak BUG#574 BUILD#108 - Additional CheckBox - Condition for draw/no draw rect for hidden text}
       ShowHiddenTextCheck: TCheckBox ;
       {++ Brovak}
//++ Glukhov Bug#199 Build#157 14.05.01
      // Arc (Circle) conversion settings
      WGroupBox1        : TWGroupBox;
      WLabel1           : TWLabel;
      edtConvPrecision  : TEdit;
      spinConvPrecision : TSpinBtn;
      chkRelPrecision   : TCheckBox;
     


      ConvPrecisionVal  : TMeasureLookupValidator;
      Procedure   SetConvPrecision;
      Procedure   GetConvPrecision;
      Procedure   chkRelPrecisionClick(Sender: TObject);
//-- Glukhov Bug#199 Build#157 14.05.01
       Procedure   FormCloseQuery(Sender: TObject; var CanClose: Boolean);
       Procedure   FormCreate(Sender: TObject);
       Procedure   FormDestroy(Sender: TObject);
       Procedure   FormHide(Sender: TObject);
       Procedure   FormShow(Sender: TObject);
       Procedure   UseGlobalsCheckClick(Sender: TObject);

      Private
//++ Glukhov Bug#199 Build#157 14.05.01
      bInit             : Boolean;
//-- Glukhov Bug#199 Build#157 14.05.01
       FOrgPalette      : TPalette;
       FOrgSettings     : PVariousSettings;
       FPalette         : TPalette;
       FSettings        : TVariousSettings;
       Function    GetSettings:PVariousSettings;
      Public
       ShowForGlobal:boolean;
       Property    Palette:TPalette read FPalette write FOrgPalette;
       Property    VariousSettings:PVariousSettings read GetSettings write FOrgSettings;



     end;

Implementation

{$R *.DFM}

Uses Measures,StyleDef;
    var Welcome,Tips:boolean;


Procedure TVariousOptionsDialog.FormCreate(Sender: TObject);
  begin
    FPalette:=TPalette.Create;
  end;

Procedure TVariousOptionsDialog.FormDestroy(Sender: TObject);
  begin
    FPalette.Free;
  end;

Function  TVariousOptionsDialog.GetSettings:PVariousSettings;
  begin
    Result:=@FSettings;
  end;

Procedure TVariousOptionsDialog.FormShow(Sender: TObject);
  begin
    if FOrgPalette<>NIL then FPalette.Assign(FOrgPalette);
    if FOrgSettings<>NIL then FSettings:=FOrgSettings^;

    with FSettings do begin
      UseGlobalsCheck.Checked:=UseGlobals;
      if not UseGlobals then UseGlobalsCheckClick(UseGlobalsCheck);
      ShowPointsCheck.Checked:=FSettings.ShowEdgePoints;
      SnapToNearestCheck.Checked:=SnapToNearest;
      {++ Brovak BUG#574 BUILD#108 - Additional Condition for draw/no draw rect for hidden text}
      ShowHiddenTextCheck.Checked:= ShowHiddenText; //CheckBox
      OldHiddenText:=ShowHiddenText; //For a comparison after closing the dialogue of options with selected significance of a flag
      {-- Brovak}
      case SnapRadiusType of
        0 : begin
            SnapRadiusVal.Units:=muMillimeters;
            SnapRadiusVal[muMillimeters]:=SnapRadius;
          end;
        1 : begin
            SnapRadiusVal.Units:=muPixels;
            SnapRadiusVal.Value:=SnapRadius;
          end;
        else begin
          SnapRadiusVal.Units:=muDrawingUnits;
          SnapRadiusVal.Value:=SnapRadius;
        end;
      end;
      if ZoomSizeType=0 then begin
        ZoomSizeVal.Units:=muPercent;
        ZoomSizeVal.Value:=ZoomSize*100.0;
      end
      else begin
        ZoomSizeVal.Units:=muMillimeters;
        ZoomSizeVal[muMillimeters]:=ZoomSize;
      end;
      CombineRadiusVal[muDrawingUnits]:=CombineRadius;

//++ Glukhov Bug#199 Build#157 14.05.01
      // Arc (Circle) conversion settings
      bInit:= False;
      chkRelPrecision.Checked:= bConvRelMistake;
      SetConvPrecision;
//-- Glukhov Bug#199 Build#157 14.05.01
    end;

  end;

Procedure TVariousOptionsDialog.FormHide(Sender: TObject);
begin
  if ModalResult=mrOK then with FSettings do begin
    UseGlobals:=UseGlobalsCheck.Checked;
    ShowEdgePoints:=ShowPointsCheck.Checked;
    SnapToNearest:=SnapToNearestCheck.Checked;
    {++ Brovak BUG#574 BUILD#108 - Additional Condition for draw/no draw rect for hidden text}
    ShowHiddenText:=ShowHiddenTextCheck.Checked; 
    {-- Brovak}
    case SnapRadiusVal.Units of
      muPixels : begin
          SnapRadiusType:=1;
          SnapRadius:=Round(SnapRadiusVal[muPixels]);
        end;
      muDrawingUnits : begin
          SnapRadiusType:=2;
          SnapRadius:=Round(SnapRadiusVal[muDrawingUnits]);
        end;
      else begin
        SnapRadiusType:=0;
        SnapRadius:=Round(SnapRadiusVal[muMillimeters]);
      end;
    end;
    if ZoomSizeVal.Units=muPercent then begin
      ZoomSizeType:=0;
      ZoomSize:=ZoomSizeVal.Value/100.0;
    end
    else begin
      ZoomSizeType:=1;
      ZoomSize:=ZoomSizeVal[muMillimeters];
    end;
    CombineRadius:=CombineRadiusVal[muDrawingUnits];
//++ Glukhov Bug#199 Build#157 15.05.01
    // Arc (Circle) conversion settings
    bConvRelMistake:= chkRelPrecision.Checked;
    GetConvPrecision;
//-- Glukhov Bug#199 Build#157 15.05.01

    if FOrgSettings<>NIL then FOrgSettings^:=FSettings;
    if FOrgPalette<>NIL then FOrgPalette.Assign(FPalette);

  end;

end;

Procedure TVariousOptionsDialog.UseGlobalsCheckClick(Sender: TObject);
begin
  SetControlGroupEnabled(ControlPanel2,not UseGlobalsCheck.Checked);
end;

Procedure TVariousOptionsDialog.FormCloseQuery(Sender:TObject;var CanClose:Boolean);
begin
  CanClose:=CheckValidators(Self);
end;

//++ Glukhov Bug#199 Build#157 15.05.01
Procedure TVariousOptionsDialog.SetConvPrecision;
begin
  if FSettings.bConvRelMistake then begin
    ConvPrecisionVal.Units:= muPercent;
    ConvPrecisionVal.Value:= FSettings.dbConvRelMistake*100.0;
  end
  else begin
    ConvPrecisionVal.Units:= muCentimeters;
    ConvPrecisionVal[muCentimeters]:= FSettings.dbConvAbsMistake;
  end;
end;

Procedure TVariousOptionsDialog.GetConvPrecision;
begin
  if FSettings.bConvRelMistake then begin
    FSettings.dbConvRelMistake:= ConvPrecisionVal.Value/100.0;
  end
  else begin
    FSettings.dbConvAbsMistake:= ConvPrecisionVal[muCentimeters];
  end;
end;

Procedure TVariousOptionsDialog.chkRelPrecisionClick(Sender: TObject);
begin
  if not bInit then begin
    bInit:= True;
    Exit;
  end;
  GetConvPrecision;
  FSettings.bConvRelMistake:= chkRelPrecision.Checked;
  SetConvPrecision;
end;
//-- Glukhov Bug#199 Build#157 15.05.01

end.
