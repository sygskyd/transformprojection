Unit LegendPropertiesDlg;

Interface

Uses Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     ExtCtrls,StdCtrls,WCtrls,PrintTemplates, MultiLng;

Type TLegendPropertiesDialog = class(TForm)
       ControlsPanel    : TPanel;
       Group1           : TWGroupBox;
       LegendsCombo     : TComboBox;
       MlgSection       : TMlgSection;
       Procedure   FormHide(Sender: TObject);
       Procedure   FormShow(Sender: TObject);
      Private
       FLegends    : TStringList;
       FGraphic    : TLegendPlaceholder;
      Public
       Property    Graphic:TLegendPlaceholder read FGraphic write FGraphic;
       Property    Legends:TStringList read FLegends write FLegends;
     end;

Implementation

{$R *.DFM}

Procedure TLegendPropertiesDialog.FormShow(Sender: TObject);
begin
  if FLegends<>NIL then LegendsCombo.Items:=FLegends;
  LegendsCombo.Text:=FGraphic.LegendName;
end;

Procedure TLegendPropertiesDialog.FormHide(Sender: TObject);
begin
  if ModalResult=mrOK then FGraphic.LegendName:=LegendsCombo.Text;
end;

end.
