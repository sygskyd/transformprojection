Unit PageSetupDlg;

Interface

Uses Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     StdCtrls,WCtrls,Spinbtn,Validate,ExtCtrls,ComCtrls,PageSetup,Measures,
     ImgList;

Type TPageSetupDialog = class(TWForm)
       PageControl: TPageControl;
       TabSheet1: TTabSheet;
       TabSheet2: TTabSheet;
       Group1: TWGroupBox;
       WLabel1: TWLabel;
       WLabel2: TWLabel;
       WLabel3: TWLabel;
       FormatsCombo: TComboBox;
       PortraitBtn: TRadioButton;
       LandscapeBtn: TRadioButton;
       WidthEdit: TEdit;
       HeightEdit: TEdit;
       WidthSpin: TSpinBtn;
       HeightSpin: TSpinBtn;
       Group2: TWGroupBox;
       Label4: TWLabel;
       Label6: TWLabel;
       Label3: TWLabel;
       Label5: TWLabel;
       BottomSpin: TSpinBtn;
       BottomEdit: TEdit;
       RightSpin: TSpinBtn;
       RightEdit: TEdit;
       TopSpin: TSpinBtn;
       TopEdit: TEdit;
       LeftSpin: TSpinBtn;
       LeftEdit: TEdit;
       Group3: TWGroupBox;
       Label15: TWLabel;
       Label9: TWLabel;
       Label13: TWLabel;
       VertPagesSpin: TSpinBtn;
       VertPagesEdit: TEdit;
       OverlapSpin: TSpinBtn;
       OverlapEdit: TEdit;
       HorzPagesSpin: TSpinBtn;
       HorzPagesEdit: TEdit;
       OkBtn: TButton;
       CancelBtn: TButton;
       Group4: TWGroupBox;
       WLabel4: TWLabel;
       WLabel5: TWLabel;
       WLabel6: TWLabel;
       WLabel7: TWLabel;
       InterBottomSpin: TSpinBtn;
       InterBottomEdit: TEdit;
       InterRightSpin: TSpinBtn;
       InterRightEdit: TEdit;
       InterTopSpin: TSpinBtn;
       InterTopEdit: TEdit;
       InterLeftSpin: TSpinBtn;
       InterLeftEdit: TEdit;
       LeftVal: TMeasureValidator;
       RightVal: TMeasureValidator;
       TopVal: TMeasureValidator;
       BottomVal: TMeasureValidator;
       WidthVal: TMeasureValidator;
       HeightVal: TMeasureValidator;
       InterLeftVal: TMeasureValidator;
       InterRightVal: TMeasureValidator;
       InterTopVal: TMeasureValidator;
       InterBottomVal: TMeasureValidator;
       PagesHorzVal: TIntValidator;
       PagesVertVal: TIntValidator;
       OverlapVal: TMeasureValidator;
       ImageList: TImageList;
       PaintBox: TPaintBox;
       Procedure   FormatsComboClick(Sender: TObject);
       Procedure   FormCloseQuery(Sender: TObject; var CanClose: Boolean);
       Procedure   FormCreate(Sender: TObject);
       Procedure   FormHide(Sender: TObject);
       Procedure   FormShow(Sender: TObject);
       Procedure   PaintBoxPaint(Sender: TObject);
       Procedure   UpdatePageOrientation(Sender: TObject);
      Private
       FOrgPageSetup    : TPageSetup;
       FPageSetup       : TPageSetup;      
       Procedure   SetFieldUnits(AUnits:TMeasureUnits);
       Procedure   UpdatePageSize;
      Public
       Property    PageSetup:TPageSetup read FPageSetup write FOrgPageSetup;
     end;

Implementation                              

{$R *.DFM}

Uses GrTools,WPrinter;

Procedure TPageSetupDialog.FormCreate(Sender: TObject);
begin
  FPageSetup:=TPageSetup.Create;
end;

Procedure TPageSetupDialog.SetFieldUnits(AUnits:TMeasureUnits);
begin
  WidthVal.Units:=AUnits;
  HeightVal.Units:=AUnits;
  LeftVal.Units:=AUnits;
  RightVal.Units:=AUnits;
  TopVal.Units:=AUnits;
  BottomVal.Units:=AUnits;
  InterLeftVal.Units:=AUnits;
  InterRightVal.Units:=AUnits;
  InterTopVal.Units:=AUnits;
  InterBottomVal.Units:=AUnits;
end;

Procedure TPageSetupDialog.FormShow(Sender: TObject);
begin
  if GetLocaleStr(GetSystemDefaultLCID,LOCALE_IMEASURE,'0')='1' then
      SetFieldUnits(muInches)
  else SetFieldUnits(muCentimeters);
  if FOrgPageSetup<>NIL then FPageSetup.Assign(FOrgPageSetup);
  with FPageSetup do begin
    FormatsCombo.Items:=PaperFormatNames;
    FormatsCombo.ItemIndex:=PaperFormatIndex;
    UpdatePageSize;      
    LeftVal[muMillimeters]:=PageFrames.Left;
    RightVal[muMillimeters]:=PageFrames.Right;
    TopVal[muMillimeters]:=PageFrames.Top;
    BottomVal[muMillimeters]:=PageFrames.Bottom;
    InterLeftVal[muMillimeters]:=InterPageFrames.Left;
    InterRightVal[muMillimeters]:=InterPageFrames.Right;
    InterTopVal[muMillimeters]:=InterPageFrames.Top;
    InterBottomVal[muMillimeters]:=InterPageFrames.Bottom;
    PagesHorzVal.AsInteger:=HorizontalPages;
    PagesVertVal.AsInteger:=VerticalPages;
    OverlapVal.Units:=Overlap.Units;
    OverlapVal[Overlap.Units]:=Overlap.Value;
    if Orientation=wpoPortrait then PortraitBtn.Checked:=TRUE
    else LandscapeBtn.Checked:=TRUE;
  end;
end;

Procedure TPageSetupDialog.FormHide(Sender: TObject);
var FPageFrames    : TGrRect;
begin
  if ModalResult=mrOK then with FPageSetup do begin
    PaperFormatIndex:=FormatsCombo.ItemIndex;
{    PageSize.Right:=WidthVal[muMillimeters];
    PageSize.Top:=HeightVal[muMillimeters];}
    FPageFrames.Left:=LeftVal[muMillimeters];
    FPageFrames.Right:=RightVal[muMillimeters];
    FPageFrames.Top:=TopVal[muMillimeters];
    FPageFrames.Bottom:=BottomVal[muMillimeters];
    PageFrames:=FPageFrames; 
    FPageFrames.Left:=InterLeftVal[muMillimeters];    
    FPageFrames.Right:=InterRightVal[muMillimeters];
    FPageFrames.Top:=InterTopVal[muMillimeters];
    FPageFrames.Bottom:=InterBottomVal[muMillimeters];
    InterPageFrames:=FPageFrames;
    HorizontalPages:=PagesHorzVal.AsInteger;
    VerticalPages:=PagesVertVal.AsInteger;
    Overlap.Units:=OverlapVal.Units;
    Overlap.Value:=OverlapVal[Overlap.Units];
    if PortraitBtn.Checked then Orientation:=wpoPortrait
    else Orientation:=wpoLandscape;
    if FOrgPageSetup<>NIL then FOrgPageSetup.Assign(FPageSetup);
  end;
end;

Procedure TPageSetupDialog.FormatsComboClick(Sender: TObject);
begin
  with PageSetup do begin
    PaperFormatIndex:=FormatsCombo.ItemIndex;
    UpdatePageSize;
  end;                               
end;
                                                        
Procedure TPageSetupDialog.UpdatePageSize;
begin
  WidthVal[muMillimeters]:=RectWidth(FPageSetup.PaperFormatSize);
  HeightVal[muMillimeters]:=RectHeight(FPagesetup.PaperFormatSize);
end;

Procedure TPageSetupDialog.UpdatePageOrientation(Sender: TObject);
begin
  if PortraitBtn.Checked then FPageSetup.Orientation:=wpoPortrait
  else FPageSetup.Orientation:=wpoLandscape;
  UpdatePageSize;
  PaintBox.Repaint;
end;

Procedure TPageSetupDialog.PaintBoxPaint(Sender: TObject);
begin
  if PortraitBtn.Checked then ImageList.Draw(PaintBox.Canvas,0,0,0)
  else ImageList.Draw(PaintBox.Canvas,0,0,1);
end;

Procedure TPageSetupDialog.FormCloseQuery(Sender:TObject;var CanClose:Boolean);
begin
  CanClose:=CheckValidators(Self);
end;

end.
