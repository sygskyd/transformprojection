Unit ProjectPropertiesDlg;

Interface

Uses Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     StdCtrls,Spinbtn,WCtrls,ExtCtrls,MultiLng,PrintTemplates, Validate;

Type TProjectPropertiesDialog = Class(TWForm)
       Bevel1           : TBevel;
       CancelBtn        : TButton;
       ControlsPanel    : TPanel;
       FitToPageBtn     : TRadioButton;
       Group1           : TWGroupBox;
       Group2           : TWGroupBox;
       MlgSection       : TMlgSection;     
       OkBtn            : TButton;
       PrintAllBtn      : TRadioButton;
       PrintCurrentViewBtn: TRadioButton;
       PrintSavedViewBtn: TRadioButton;
       ScaleEdit        : TEdit;
       ScaleBtn         : TRadioButton;
       ScaleSpin        : TSpinBtn;
       ScaleVal         : TScaleValidator;
       ViewsCombo       : TComboBox;
       Procedure   FormHide(Sender: TObject);
       Procedure   FormShow(Sender: TObject);
       Procedure   ViewsComboExit(Sender: TObject);
      Private
       FGraphic    : TProjectPlaceholder;
       FViews      : TStringList;
      Public
       Property    Graphic:TProjectPlaceholder read FGraphic write FGraphic;
       Property    Views:TStringList read FViews write FViews;
     end;

Implementation

{$R *.DFM}

Uses PrintOptions;

Procedure TProjectPropertiesDialog.FormShow(Sender: TObject);
begin
  if FGraphic<>NIL then with FGraphic do begin
    case PrintRange of
      prProject     : PrintAllBtn.Checked:=TRUE;
      prSavedView   : PrintSavedViewBtn.Checked:=TRUE;
      prCurrentView : PrintCurrentViewBtn.Checked:=TRUE;
    end;
    case PrintSize of
      psScale          : ScaleBtn.Checked:=TRUE;
      psFitToPage      : FitToPageBtn.Checked:=TRUE;
    end;
    ScaleVal.AsFloat:=PrintScale;
    ViewsCombo.Items:=FViews;
    ViewsCombo.Text:=ViewName;
  end;
end;

Procedure TProjectPropertiesDialog.FormHide(Sender: TObject);
begin
  if (ModalResult=mrOK) and (FGraphic<>NIL) then with FGraphic do begin
    if PrintAllBtn.Checked then PrintRange:=prProject
    else if PrintSavedViewBtn.Checked then PrintRange:=prSavedView
    else PrintRange:=prCurrentView;
    if ScaleBtn.Checked then PrintSize:=psScale
    else PrintSize:=psFitToPage;
    PrintScale:=ScaleVal.AsFloat;
    ViewName:=ViewsCombo.Text;
  end;
end;

procedure TProjectPropertiesDialog.ViewsComboExit(Sender: TObject);
begin
  if ViewsCombo.Text<>'' then PrintSavedViewBtn.Checked:=TRUE;
end;

end.
