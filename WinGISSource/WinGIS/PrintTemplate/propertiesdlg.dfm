�
 TPROPERTIESDIALOG 0Y  TPF0TPropertiesDialogPropertiesDialogLeft�Top� BorderStylebsDialogCaption
PropertiesClientHeightEClientWidthOColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnCloseQueryFormCloseQueryOnCreate
FormCreateOnHideFormHideOnShowFormShowPixelsPerInch`
TextHeight TTabbedNotebookNotebookLeftTopWidthCHeightTabFont.CharsetDEFAULT_CHARSETTabFont.Color	clBtnTextTabFont.Height�TabFont.NameMS Sans SerifTabFont.Style TabOrder  TTabPage LeftTopCaptionSize and Position 
TWGroupBoxGroup1LeftTopWidth)Height1CaptionSizeTabOrder BoxStyle	bsTopLine TWLabelWLabel1LeftTopWidth1HeightAutoSizeCaption&Width:FocusControl	WidthEditOnClickWLabel1ClickAlignToControl	alcCenter  TWLabelWLabel2Left� TopWidth1HeightAutoSizeCaption&Height:FocusControl
HeightEditOnClickWLabel2ClickAlignToControl	alcCenter  TEdit	WidthEditLeft8TopWidthAHeightTabOrder OnChange
EditChangeOnEnter	EditEnterOnExitEditExit  TEdit
HeightEditLeft� TopWidthAHeightTabOrderOnChange
EditChangeOnEnter	EditEnterOnExitEditExit  TSpinBtn	WidthSpinLeftzTopHeight	ArrowkeysCtrlIncrement       �@	Increment ��������?Max �P]���CMin ��������?OnClickWidthSpinClickShiftIncrement       ��?	ValidatorWidthVal  TSpinBtn
HeightSpinLeftTopHeight	ArrowkeysCtrlIncrement       �@	Increment ��������?Max �P]���CMin ��������?OnClickWidthSpinClickShiftIncrement       ��?	Validator	HeightVal   
TWGroupBoxGroup2LeftTop8Width)Height1CaptionScaleTabOrderBoxStyle	bsTopLine TWLabelWLabel3LeftTopWidth1HeightAutoSizeCaptionW&idth:FocusControl
XScaleEditOnClickWLabel3ClickAlignToControl	alcCenter  TWLabelWLabel4Left� TopWidth1HeightAutoSizeCaptionH&eight:FocusControl
YScaleEditOnClickWLabel4ClickAlignToControl	alcCenter  TEdit
XScaleEditLeft8TopWidthAHeightTabOrder OnChange
EditChangeOnEnter	EditEnterOnExitEditExit  TEdit
YScaleEditLeft� TopWidthAHeightTabOrderOnChange
EditChangeOnEnter	EditEnterOnExitEditExit  TSpinBtn
XScaleSpinLeftzTopHeight	ArrowkeysCtrlIncrement       �@	Increment       ��?Max �P]���CMin ��������?OnClickWidthSpinClickShiftIncrement       �@	Validator	XScaleVal  TSpinBtn
YScaleSpinLeftTopHeight	ArrowkeysCtrlIncrement       �@	Increment       ��?Max �P]���CMin ��������?OnClickWidthSpinClickShiftIncrement       �@	Validator	YScaleVal   
TWGroupBoxGroup3LeftTop� Width)Height9CaptionOriginal SizeTabOrderBoxStyle	bsTopLine TWLabelWLabel5LeftTopWidth1HeightAutoSizeCaptionWidth:AlignToControl	alcCenter  TWLabelWLabel6LeftTop&Width1HeightAutoSizeCaptionHeight:AlignToControl	alcCenter  TWLabel
WidthLabelLeft8TopWidthYHeightAutoSize  TWLabelHeightLabelLeft8Top&WidthQHeightAutoSize  TButton
DefaultBtnLeft� TopWidthQHeightCaption&DefaultTabOrder OnClickDefaultBtnClick   
TWGroupBoxGroup4LeftTop� Width)Height1CaptionPositionTabOrderBoxStyle	bsTopLine TWLabelWLabel7LeftTopWidth1HeightAutoSizeCaption&X:FocusControlXPosEditOnClickWLabel7ClickAlignToControl	alcCenter  TWLabelWLabel8Left� TopWidth1HeightAutoSizeCaption&Y:FocusControlYPosEditOnClickWLabel8ClickAlignToControl	alcCenter  TEditXPosEditLeft8TopWidthAHeightTabOrder   TEditYPosEditLeft� TopWidthAHeightTabOrder  TSpinBtnXPosSpinLeftzTopHeight	ArrowkeysCtrlIncrement       �@	Increment ��������?Max �P]���CMin �P]����ShiftIncrement       ��?	ValidatorXPosVal  TSpinBtnYPosSpinLeftTopHeight	ArrowkeysCtrlIncrement       �@	Increment ��������?Max �P]���CMin �P]����ShiftIncrement       ��?	ValidatorYPosVal   	TCheckBoxKeepAspectCheckLeftTophWidth� HeightCaptionKeep AspectChecked	State	cbCheckedTabOrder  	TCheckBoxOriginalSizeCheckLeftTopxWidth� HeightCaptionRelative to original sizeChecked	EnabledState	cbCheckedTabOrderOnClickOriginalSizeCheckClick    TButtonOkBtnLeft� Top(WidthYHeightCaptionOkDefault	ModalResultTabOrder  TButton	CancelBtnLeft� Top(WidthYHeightCancel	CaptionCancelModalResultTabOrder  TMeasureValidatorWidthValEdit	WidthEditUseableUnits`CommasMinValue.Value أp=
ף�?MaxValue.Value �P]���CLeft
Top(  TMeasureValidator	HeightValEdit
HeightEditUseableUnits`CommasMinValue.Value أp=
ף�?MaxValue.Value �P]���CLeftTop(  TMeasureValidator	XScaleValEdit
XScaleEditUseableUnits @CommasMinValue.Units	muPercentMinValue.Value ��������?MaxValue.Value �P]���CUnits	muPercentLeft*Top(  TMeasureValidator	YScaleValEdit
YScaleEditUseableUnits @CommasMinValue.Value أp=
ף�?MaxValue.Value �P]���CUnits	muPercentLeft:Top(  TMeasureValidatorXPosValEditXPosEditUseableUnits`CommasMinValue.Value �P]����MaxValue.Value �P]���CLeftJTop(  TMeasureValidatorYPosValEditYPosEditUseableUnits`CommasMinValue.Value �P]����MaxValue.Value �P]���CLeftZTop(   