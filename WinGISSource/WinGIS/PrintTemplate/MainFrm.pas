Unit MainFrm;

Interface

Uses WCtrls,Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     UserIntf,MultiLng,Scroller,OleCtnrs,GrTools,ExtCtrls,ExtDlgs,MenuHndl,
     EditHndl,InpHndl,ComCtrls,Menulist,RegDB,PageSetup,PrintTemplates,TrLib32,
     Img_Info,WPrinter;

Type TMainForm = class(TWForm)
       EditHandler      : TEditHandler;
       FileHistoryList  : TMenuHistoryList;
       MlgSection       : TMlgSection;
       OpenDialog       : TWOpenDialog;
       OpenFileDialog   : TWOpenDialog;
       RubberBox        : TRubberBox;
       SaveDialog       : TWSaveDialog;
       Scroller         : TScroller;
       UIExtension      : TUIExtension;
       UIInitialize     : TUIInitialize;
       Window           : TWindow;
       WStatusBar       : TWStatusBar;
       Procedure   EditHandlerMove(Sender: TObject);
       Procedure   EditHandlerResize(Sender: TObject);
       Procedure   FileHistoryListClick(Item: String);
       Procedure   FormCloseQuery(Sender: TObject; var CanClose: Boolean);
       Procedure   FormCreate(Sender: TObject);
       Procedure   FormDestroy(Sender: TObject);
       Procedure   FormShow(Sender: TObject);
       Procedure   RubberBoxFinishedInput(Sender: TObject);
       Procedure   ScrollerScroll(Sender: TObject);
       Procedure   ScrollerZoom(Sender: TObject);
       Procedure   UIInitializeApplicationActivate(Sender: TObject);
       Procedure   UIExtensionUpdateCursor(Sender: TObject);
       Procedure   UIExtensionUpdateMenus(Sender: TObject);
       Procedure   WindowMouseDown(Sender: TObject; Button: TMouseButton;
                       Shift: TShiftState; X, Y: Integer);
       Procedure   WindowMouseMove(Sender: TObject; Shift: TShiftState; X,
                       Y: Integer);
       Procedure   WindowMouseUp(Sender: TObject; Button: TMouseButton;
                       Shift: TShiftState; X, Y: Integer);
       Procedure   WindowPaint(Sender: TObject);
       Procedure   WindowResize(Sender: TObject);
      Private
       FCurrentItem     : TCustomGraphic;
       FGraphics        : TGraphics;
       FIsNewFile       : Boolean;
       FMenuHandler     : TMenuHandler;
       FModified        : Boolean;
       FLegends         : TStringList;
       FViews           : TStringList;
       Procedure   OleContainerObjectMove(OleContainer:TOleContainer;const Bounds:TRect);
       Function    Open(const FileName:String):Boolean;
       Function    Save:Boolean;
       Procedure   SetCurrentItem(AItem:TCustomGraphic);
       Procedure   SetMenuHandler(AMenuHandler:TMenuHandler);
       Procedure   SetupMenuHandler(AMenuHandler:TMenuHandler);
       Procedure   SetRubberboxCursor(Cursor:TCursor);
       Procedure   UpdateEditHandlerPosition;
       Procedure   UpdateOLEContainer;
       Procedure   UpdateSize;
      Public
       Property    CurrentItem:TCustomGraphic read FCurrentItem write SetCurrentItem;
       Property    MenuHandler:TMenuHandler read FMenuHandler write SetMenuHandler;
       Property    Modified:Boolean read FModified write FModified;
      Published
       Procedure   EditCut;
       Procedure   EditCopy;
       Procedure   EditDelete;
       Procedure   EditPaste;
       Procedure   EditProperties;
       Procedure   EditSelect;
       Procedure   EditLinks;
       Procedure   FileNew;
       Procedure   FileSave;
       Procedure   FileSaveAs;
       Procedure   FileOpen;
       Procedure   FilePageSetup;
       Procedure   FilePrinterSetup;
       Procedure   FilePrint;
       Procedure   FileQuit;
       Procedure   HelpInfo;
       Procedure   InsertPicture;
       Procedure   InsertObject;
       Procedure   InsertProjectFrame;
       Procedure   InsertLegendFrame;
       Procedure   OrderBringToFront;
       Procedure   OrderBringForward;
       Procedure   OrderSendToBack;
       Procedure   OrderSendBackward;
       Procedure   ViewToolbars;
       Procedure   ViewZoomAll;
       Procedure   ViewZoomIn;
       Procedure   ViewZoomList;
       Procedure   ViewZoomOut;
       Procedure   ViewZoom;
     end;

var MainForm: TMainForm;
                                            
Implementation

{$R *.DFM}
{$R PrintTemplate.mfn}

Uses ClipBrd,MenuFn,NumTools,PageSetupDlg,ProjectPropertiesDlg,PropertiesDlg,
     PrnStDlg,PrintSetupDlg,Measures,LegendPropertiesDlg;

var FZoomInCursor  : THandle;

{ TMainForm }                        

Procedure TMainForm.FileNew;
begin
  if CloseQuery then begin
    CurrentItem:=NIL;
    FGraphics.Free;
    FGraphics:=TGraphics.Create;
    FIsNewFile:=TRUE;
    FModified:=FALSE;
    UpdateSize;
  end;
end;

Procedure TMainForm.FileOpen;
begin
  if OpenDialog.Execute then Open(OpenDialog.FileName);
end;

Procedure TMainForm.FileQuit;
begin
  Close;
end;

Procedure TMainForm.FileSave;
begin
  if FIsNewFile then FileSaveAs
  else Save;
end;

Procedure TMainForm.FileSaveAs;
begin
  if SaveDialog.Execute then Save;
end;
                                                  
Procedure TMainForm.InsertObject;
//var OLEGraphic     : TOLEGraphic;
begin
{  OLEGraphic:=TOLEGraphic.Create(FGraphics);
  OLEGraphic.Parent:=Scroller;
  OLEGraphic.BoundsRect:=GrRect(200,200,1900,2770);
  OLEGraphic.Align(Scroller);
  if not OLEGraphic.OLEContainer.InsertObjectDialog then
      OLEGraphic.Free;
  CurrentItem:=OLEGraphic;}
end;

Procedure TMainForm.WindowMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var APoint         : TPoint;
    BPoint         : TGrPoint;
begin                        
  APoint:=Point(X,Y);
  DPToLP(Window.Canvas.Handle,APoint,1);
  BPoint:=GrPoint(APoint.X,APoint.Y);
  Scroller.WindowToInternal(BPoint);
  if ((FMenuHandler=NIL) or not FMenuHandler.OnMouseDown(BPoint.X,BPoint.Y,Button,Shift))
      and (Button=mbLeft) then CurrentItem:=FGraphics.ItemAtPosition(BPoint);
end;
                                       
Procedure TMainForm.FormCreate(Sender: TObject);
var Cnt            : Integer;
begin
  SetupMenuHandler(EditHandler);
  SetupMenuHandler(RubberBox);
  FGraphics:=TGraphics.Create;
  Scroller.Canvas:=Window.Canvas;
  UpdateSize;
  FIsNewFile:=TRUE;
  // fill the zoom-list
  MenuFunctions['ViewZoomList'].Items.Add('');
  for Cnt:=4 to 11 do MenuFunctions['ViewZoomList'].Items.Add(MlgSection[Cnt]);
  FViews:=TStringList.Create;
  FLegends:=TStringList.Create;
end;

Procedure TMainForm.ViewZoomIn;
begin
  Scroller.ZoomByFactor(2);
end;

Procedure TMainForm.ViewZoomOut;
begin
  Scroller.ZoomByFactor(0.5);
end;

Procedure TMainForm.ViewZoomAll;
begin
  Scroller.ZoomAll;
end;

Procedure TMainForm.WindowPaint(Sender: TObject);
var Rect           : TRect;
    ARect          : TRect;
    Cnt            : Integer;
    APoint         : TPoint;
begin
  UserInterface.BeginWaitCursor;
  try
    with Window.Canvas do begin
      Scroller.InternalToWindow(FGraphics.PageSetup.PageSize,Rect);
      with Rect do begin
        // draw page in white with black border
        Pen.Style:=psSolid;
        Pen.Color:=clBlack;
        Brush.Style:=bsClear;
        // grow rectangle by one pixel
        LPToDP(Handle,Rect,2);
        InflateRect(Rect,1,1); Inc(Rect.Right); Inc(Rect.Bottom);
        DPToLP(Handle,Rect,2);
        Rectangle(Left,Top,Right,Bottom);
        // draw shadow of the page
        LPToDP(Handle,Rect,2);
        InflateRect(Rect,1,1); Inc(Left,5); Inc(Top,5);
        DPToLP(Handle,Rect,2);
        APoint:=Point(3,3);
        DPToLP(Handle,APoint,1);
        Pen.Color:=clBtnShadow;
        Pen.Width:=APoint.X;
        MoveTo(Left,Bottom);
        LineTo(Right,Bottom);
        LineTo(Right,Top);
        Pen.Width:=1;
      end;
      Scroller.InternalToWindow(FGraphics.PageSetup.PageExtent,Rect);
      with Rect do begin
        Pen.Style:=psDot;              
        Pen.Color:=clSilver;
        Brush.Style:=bsClear;
        Rectangle(Left,Bottom,Right,Top);
        for Cnt:=0 to FGraphics.PageSetup.HorizontalPages-2 do begin
          Scroller.InternalToWindow(FGraphics.PageSetup.PageExtents[Cnt,0],ARect);
          MoveTo(ARect.Right,Rect.Top);
          LineTo(ARect.Right,Rect.Bottom);
        end;
        for Cnt:=0 to FGraphics.PageSetup.VerticalPages-2 do begin
          Scroller.InternalToWindow(FGraphics.PageSetup.PageExtents[0,Cnt],ARect);
          MoveTo(Rect.Left,ARect.Top);
          LineTo(Rect.Right,ARect.Top);
        end;
      end;
    end;
    FGraphics.Paint(Scroller);
    if MenuHandler<>NIL then MenuHandler.Paint;
  finally
    UserInterface.EndWaitCursor;
  end;
end;

Procedure TMainForm.UpdateOLEContainer;
begin
  if FGraphics<>NIL then FGraphics.Align(Scroller);
end;

Procedure TMainForm.ScrollerScroll(Sender: TObject);
begin
  UpdateOLEContainer;
end;

Procedure TMainForm.OleContainerObjectMove(OleContainer: TOleContainer;
  const Bounds: TRect);
begin
  OleContainer.BoundsRect:=Bounds;
end;

Procedure TMainForm.WindowResize(Sender: TObject);
begin
  UpdateOLEContainer;
end;

Procedure TMainForm.ViewZoom;
begin
  MenuHandler:=RubberBox;
  SetRubberboxCursor(FZoomInCursor);
end;

Procedure TMainForm.SetupMenuHandler(AMenuHandler:TMenuHandler);
begin
  AMenuHandler.Canvas:=Window.Canvas;
  AMenuHandler.OnWinToIntPoint:=Scroller.WindowToInternal;
  AMenuHandler.OnWinToIntDistance:=Scroller.WindowToInternal;
  AMenuHandler.OnIntToWinPoint:=Scroller.InternalToWindow;
  AMenuHandler.OnIntToWinDistance:=Scroller.InternalToWindow;
end;

Procedure TMainForm.FilePageSetup;
var Dialog         : TPageSetupDialog;
begin
  Dialog:=TPageSetupDialog.Create(Self);
  try
    Dialog.PageSetup:=FGraphics.PageSetup;
    if Dialog.ShowModal=mrOK then UpdateSize;
  finally
    Dialog.Free;
  end;
end;

Procedure TMainForm.FilePrint;
var SetupDialog       : TPrinterSetupDialog;
    ClippingRect      : TRect;
    XPage             : Integer;
    YPage             : Integer;
    OverlapX          : Integer;     // page-overlap in virtual units
    OverlapY          : Integer;
    Dialog            : TPrintStatusDialog;
    Disabled          : Pointer;     // pointer to disabled windows-information
    GraphicSite       : TGraphicSite;
    ARect             : TRect;
begin
  SetupDialog:=TPrinterSetupDialog.Create(Self);
  try
    FGraphics.PageSetup.SetOptionsToPrinter;
    SetupDialog.Printer:=FGraphics.PageSetup.Printer;
    SetupDialog.ShowPrintToFile:=FALSE;
    SetupDialog.Caption:=MlgSection[21];
    if SetupDialog.ShowModal<>mrOK then Exit;
    FGraphics.PageSetup.GetOptionsFromPrinter;
  finally
    SetupDialog.Free;
  end;
  Dialog:=TPrintStatusDialog.Create(Application);
  Disabled:=DisableTaskWindows(0);
  Dialog.Show;
  Dialog.Update;                             
  try                                       
    with FGraphics,PageSetup,Printer do begin
      // setup document-title and status-dialog-labels
      Title:='WinGIS Print Template - '+ExtractFileName(FileName);
      Dialog.FileLabel.Caption:=ExtractFileName(FileName);
      Dialog.PrinterLabel.Caption:=Name;
      Dialog.PortLabel.Caption:=Format(MlgStringList['PrnHndl',5],[Port]);
      Dialog.Update;
      // setup the printer with the settings form the page-setup
      SetOptionsToPrinter;
      // start printing the document
      BeginDoc;
//      Dialog.CancelPressed:=@FPInfo.AbortPrinting;
      // setup GraphicSite and Template
      GraphicSite:=TGraphicSite.Create(NIL);
      GraphicSite.Scale:=10;
      SetMapMode(Handle,mm_LoMetric);
      GraphicSite.Canvas:=Canvas;
      GraphicSite.YOffset:=RectHeight(PageSetup.PageExtent)*10+PrintableArea.Top;
      // calculate page-overlap in virutal and project-units
      if PageSetup.Overlap.Units=muPercent then begin
        OverlapX:=Round(RectWidth(PageSize)*PageSetup.Overlap.Value/100.0);
        OverlapY:=Round(RectHeight(PageSize)*PageSetup.Overlap.Value/100.0);
      end
      else begin
        OverlapX:=Round(PageSetup.Overlap.Value*10);
        OverlapY:=OverlapX;
      end;
      // for all pages in x- and y-direction
      for YPage:=0 to PageSetup.VerticalPages-1 do begin
        // calculate top-position of current rectangle
        if YPage=0 then ClippingRect.Top:=-Round(PageSetup.PageFrames.Top*10)
        else ClippingRect.Top:=-Round(PageSetup.InterPageFrames.Top*10);
        // calculate bottom of current rectangle
        if YPage=PageSetup.VerticalPages-1 then ClippingRect.Bottom:=
            PageSize.Bottom+Round(PageSetup.PageFrames.Bottom*10)
        else ClippingRect.Bottom:=PageSize.Bottom+Round(PageSetup.InterPageFrames.Bottom*10);
        // correct bottom if print-range is smaller
        GraphicSite.XOffset:=PrintableArea.Left;
        GraphicSite.YOffset:=GraphicSite.YOffset-ClippingRect.Top;
        for XPage:=0 to PageSetup.HorizontalPages-1 do begin
          // update the page-number-display in the printing-status-dialog
          Dialog.PageLabel.Caption:=Format(MlgStringList['PrnHndl',6],[PageNumber,PageSetup.VerticalPages*PageSetup.HorizontalPages]);
          Dialog.Update;
          // calculate left-position of current rectangle
          if XPage=0 then ClippingRect.Left:=Round(PageSetup.PageFrames.Left*10)
          else ClippingRect.Left:=Round(PageSetup.InterPageFrames.Left*10);
          // calculate right-position of current rectangle
          if XPage=PageSetup.HorizontalPages-1 then ClippingRect.Right:=PageSize.Right-
              Round(PageSetup.PageFrames.Right*10)
          else ClippingRect.Right:=PageSize.Right-Round(PageSetup.InterPageFrames.Right*10);
          try
            ARect:=MovedRect(ClippingRect,-PrintableArea.Left,-PrintableArea.Top);
            LPToDP(Handle,ARect,2);
            Inc(ARect.Right); Inc(ARect.Bottom);
            DPToLP(Handle,ARect,2);
            IntersectClipRect(Handle,ARect.Left,ARect.Top,ARect.Right,ARect.Bottom);
            GraphicSite.XOffset:=GraphicSite.XOffset-ClippingRect.Left;
            FGraphics.Paint(GraphicSite);
            GraphicSite.XOffset:=GraphicSite.XOffset+ClippingRect.Left
                +RectWidth(ClippingRect)-OverlapX;
          finally
            SelectClipRgn(Handle,0);
          end;
          if (XPage<PageSetup.HorizontalPages-1) or (YPage<PageSetup.VerticalPages-1) then begin
            NewPage;
            // set map-mode to lo-metric because some printer-drivers
            // reset the map-mode if a new page is started
            SetMapMode(Handle,mm_LoMetric);
          end;
        end;
        GraphicSite.YOffset:=GraphicSite.YOffset+ClippingRect.Top-RectHeight(ClippingRect)+OverlapY;
      end;
      EndDoc;
      GraphicSite.Free;
    end;
  finally
    EnableTaskWindows(Disabled);
    Dialog.Free;
  end;
end;

Procedure TMainForm.InsertLegendFrame;
begin
  MenuHandler:=RubberBox;
  SetRubberboxCursor(crCross);
end;

Procedure TMainForm.InsertProjectFrame;
begin
  MenuHandler:=RubberBox;
  SetRubberboxCursor(crCross);
end;

Procedure TMainForm.EditCopy;
begin
  ShowMessage('Sorry, not yet implemented!');
end;

Procedure TMainForm.EditCut;
begin
  ShowMessage('Sorry, not yet implemented!');
end;
                                 
Procedure TMainForm.EditDelete;
begin
  if (FCurrentItem<>NIL) and (MessageDialog(MlgSection[17],mtConfirmation,
      [mbYes,mbNo],0)=mrYes) then begin
    FCurrentItem.Invalidate(Scroller);
    FCurrentItem.Free;
    CurrentItem:=NIL;
  end;
end;

Procedure TMainForm.HelpInfo;
begin
  ShowMessage('Sorry, not yet implemented');
end;

Procedure TMainForm.OrderBringForward;
var Index          : Integer;
begin
  if CurrentItem<>NIL then begin
    Index:=FGraphics.IndexOf(CurrentItem);
    if Index<FGraphics.ItemCount-1 then begin
      FGraphics.Delete(Index);
      FGraphics.Insert(Index+1,CurrentItem);
      CurrentItem.Invalidate(Scroller);
    end;  
  end;
end;

Procedure TMainForm.OrderBringToFront;
var Index          : Integer;
begin
  if CurrentItem<>NIL then begin
    Index:=FGraphics.IndexOf(CurrentItem);
    if Index<FGraphics.ItemCount-1 then begin
      FGraphics.Delete(Index);
      FGraphics.Insert(FGraphics.ItemCount,CurrentItem);
      CurrentItem.Invalidate(Scroller);
    end;  
  end;
end;

Procedure TMainForm.OrderSendBackward;
var Index          : Integer;
begin
  if CurrentItem<>NIL then begin
    Index:=FGraphics.IndexOf(CurrentItem);
    if Index>0 then begin
      FGraphics.Delete(Index);
      FGraphics.Insert(Index-1,CurrentItem);
      CurrentItem.Invalidate(Scroller);
    end;  
  end;
end;

Procedure TMainForm.OrderSendToBack;
var Index          : Integer;
begin
  if CurrentItem<>NIL then begin
    Index:=FGraphics.IndexOf(CurrentItem);
    if Index>0 then begin
      FGraphics.Delete(Index);
      FGraphics.Insert(0,CurrentItem);
      CurrentItem.Invalidate(Scroller);
    end;  
  end;
end;

Procedure TMainForm.EditProperties;
var Dialog         : TPropertiesDialog;
    ProjectDialog  : TProjectPropertiesDialog;
    LegendDialog   : TLegendPropertiesDialog;
    OldBounds      : TGrRect;
begin
  if CurrentItem=NIL then Exit;
  Dialog:=TPropertiesDialog.Create(Self);
  try
    Dialog.Graphic:=CurrentItem;
    // add context-specific dialog-pages
    if CurrentItem is TProjectPlaceholder then begin
      ProjectDialog:=TProjectPropertiesDialog.Create(Self);
      ProjectDialog.Graphic:=TProjectPlaceholder(CurrentItem);
      ProjectDialog.Views:=FViews;
      Dialog.AddDialog(ProjectDialog,ProjectDialog.ControlsPanel,ProjectDialog.Caption);
    end
    else if CurrentItem is TLegendPlaceholder then begin
      LegendDialog:=TLegendPropertiesDialog.Create(Self);
      LegendDialog.Legends:=FLegends;
      LegendDialog.Graphic:=TLegendPlaceholder(CurrentItem);
      LegendDialog.Legends:=FLegends;
      Dialog.AddDialog(LegendDialog,LegendDialog.ControlsPanel,LegendDialog.Caption);
    end;
    // show dialog
    Dialog.Notebook.PageIndex:=0;
    OldBounds:=CurrentItem.BoundsRect;
    if Dialog.ShowModal=mrOK then begin
      Scroller.InvalidateRect(OldBounds);
      CurrentItem.Invalidate(Scroller);
      UpdateEditHandlerPosition;
    end;
  finally
    Dialog.Free;
  end;
end;

Procedure TMainForm.InsertPicture;
var Graphic        : TWindowsGraphic;
begin                         
  OpenFileDialog.Filter:=TrLib.EnumImpFormats;
  if OpenFileDialog.Execute then begin            
    if not(ofReadOnly in OpenFileDialog.Options) then
      ShowMessage('Sorry, only linked files are currently supported.')
    else begin
      Graphic:=TWindowsGraphic.Create(FGraphics);
      try
        Graphic.CreateFromFile(OpenFileDialog.FileName,'',
            ofReadOnly in OpenFileDialog.Options);
        Graphic.Invalidate(Scroller);
        CurrentItem:=Graphic;
        Modified:=TRUE;
      except
        Graphic.Free;
        Raise;
      end;
    end;
  end;
end;

Procedure TMainForm.WindowMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
var APoint         : TPoint;
    BPoint         : TGrPoint;
begin
  APoint:=Point(X,Y);                         
  DPToLP(Window.Canvas.Handle,APoint,1);
  BPoint:=GrPoint(APoint.X,APoint.Y);
  Scroller.WindowToInternal(BPoint);
  StatusBar.Panels[0].Text:=Format('X: %.2f  Y: %.2f',[BPoint.X,BPoint.Y]);
  if FMenuHandler<>NIL then FMenuHandler.OnMouseMove(BPoint.X,BPoint.Y,Shift);
end;

Procedure TMainForm.WindowMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var APoint         : TPoint;
    BPoint         : TGrPoint;
begin
  APoint:=Point(X,Y);
  DPToLP(Window.Canvas.Handle,APoint,1);
  BPoint:=GrPoint(APoint.X,APoint.Y);
  Scroller.WindowToInternal(BPoint);
  if FMenuHandler<>NIL then FMenuHandler.OnMouseUp(BPoint.X,BPoint.Y,Button,Shift);
end;

Procedure TMainForm.SetMenuHandler(AMenuHandler:TMenuHandler);
begin
  FMenuHandler:=AMenuHandler;
end;

Procedure TMainForm.RubberBoxFinishedInput(Sender: TObject);
var PlaceholderType: TClass;
    Graphic        : TCustomGraphic;
    Cnt            : Integer;
    Result         : TModalResult;
begin
  if MenuFunctions['ViewZoom'].Checked then begin
    if RubberBox.MinSizeReached then Scroller.CurrentView:=RotRect(RubberBox.Box,0)
    else Scroller.ZoomByFactorWithCenter(RubberBox.Position,2);
  end
  else if RubberBox.MinSizeReached then begin
    if MenuFunctions['InsertProjectFrame'].Checked then PlaceholderType:=TProjectPlaceholder
    else PlaceholderType:=TLegendPlaceholder;
    MenuFunctions['EditSelect'].Execute;
    CurrentItem:=NIL;
    // look if a placeholder of this type already exists
    for Cnt:=0 to FGraphics.ItemCount-1 do begin
      Graphic:=FGraphics.Items[Cnt];
      if Graphic is PlaceholderType then begin
        if Graphic is TProjectPlaceholder then Result:=MessageDialog(MlgSection[14],
            mtConfirmation,[mbYes,mbNo],0)
        else Result:=MessageDialog(MlgSection[15],mtConfirmation,[mbYes,mbNo],0);
        if Result=mrYes then begin
          Graphic.Invalidate(Scroller);
          Graphic.Free;
          Break;
        end
        else Exit;
      end;
    end;
    // create a new placeholder-object
    if PlaceholderType=TProjectPlaceholder then Graphic:=TProjectPlaceholder.Create(FGraphics)
    else Graphic:=TLegendPlaceholder.Create(FGraphics);
    Graphic.BoundsRect:=RubberBox.Box;
    Graphic.Invalidate(Scroller);
    CurrentItem:=Graphic;
    Modified:=TRUE;
  end;  
end;

Procedure TMainForm.ViewZoomList;
begin
  case MenuFunctions['ViewZoomList'].ItemIndex of
    0:;
    1: Scroller.Scale:=50;
    2: Scroller.Scale:=20;
    3: Scroller.Scale:=15;
    4: Scroller.Scale:=10;
    5: Scroller.Scale:=7.5;
    6: Scroller.Scale:=5;
    7: Scroller.Scale:=2.5;
    8: Scroller.Scale:=1;
    9: ;
  end;
end;

Procedure TMainForm.ScrollerZoom(Sender: TObject);
var MenuFunction   : TMenuFunction; 
begin
  MenuFunction:=MenuFunctions['ViewZoomList'];
  if DblCompare(Scroller.Scale,50)=0 then MenuFunction.ItemIndex:=1
  else if DblCompare(Scroller.Scale,20)=0 then MenuFunction.ItemIndex:=2
  else if DblCompare(Scroller.Scale,15)=0 then MenuFunction.ItemIndex:=3
  else if DblCompare(Scroller.Scale,10)=0 then MenuFunction.ItemIndex:=4
  else if DblCompare(Scroller.Scale,7.5)=0 then MenuFunction.ItemIndex:=5
  else if DblCompare(Scroller.Scale,5)=0 then MenuFunction.ItemIndex:=6
  else if DblCompare(Scroller.Scale,2.5)=0 then MenuFunction.ItemIndex:=7
  else if DblCompare(Scroller.Scale,1)=0 then MenuFunction.ItemIndex:=8
  else MenuFunction.ItemIndex:=0;
  UpdateOLEContainer;
end;

Procedure TMainForm.EditSelect;
begin
  MenuHandler:=NIL;
  Cursor:=crDefault;
end;

Procedure TMainForm.SetCurrentItem(AItem:TCustomGraphic);
begin
  if AItem<>FCurrentItem then begin
    if FCurrentItem<>NIL then EditHandler.Visible:=FALSE;
    FCurrentItem:=AItem;
    if FCurrentItem<>NIL then begin
      UpdateEditHandlerPosition;
      EditHandler.Visible:=TRUE;
      MenuHandler:=EditHandler;
    end;
    UserInterface.Update([uiMenus]);
  end;
end;

Procedure TMainForm.EditHandlerMove(Sender: TObject);
begin
  CurrentItem.Invalidate(Scroller);
  with EditHandler do CurrentItem.BoundsRect:=GrBounds(CurrentX,CurrentY,
      CurrentWidth,CurrentHeight);
  CurrentItem.Invalidate(Scroller);
  Modified:=TRUE;
end;

Procedure TMainForm.EditHandlerResize(Sender: TObject);
begin
  CurrentItem.Invalidate(Scroller);
  with EditHandler do CurrentItem.BoundsRect:=GrBounds(CurrentX,CurrentY,
      CurrentWidth,CurrentHeight);
  CurrentItem.Invalidate(Scroller);
  Modified:=TRUE;
end;

Procedure TMainForm.UIExtensionUpdateMenus(Sender: TObject);
var Index          : Integer;
begin
  with MenuFunctions do begin
    GroupEnabled[2]:=Clipboard.HasFormat(CF_BITMAP)
        or Clipboard.HasFormat(CF_METAFILEPICT);
    GroupEnabled[6]:=FGraphics.ItemCount>0;
    if CurrentItem<>NIL then begin
      GroupEnabled[5]:=TRUE;
      Index:=FGraphics.IndexOf(CurrentItem);
      GroupEnabled[3]:=Index>0;
      GroupEnabled[4]:=(Index>=0) and (Index<FGraphics.ItemCount-1);
    end;
    GroupEnabled[7]:=GroupEnabled[6] and (WPrinters.Printers.Count>0);
    GroupEnabled[8]:=WPrinters.Printers.Count>0;
  end;
end;

Procedure TMainForm.FormDestroy(Sender: TObject);
begin
  FGraphics.Free;
  FViews.Free;
  FLegends.Free;
end;

Procedure TMainForm.EditPaste;
var Graphic        : TWindowsGraphic;
begin
  ClipBoard.Open;
  try
    Graphic:=TWindowsGraphic.Create(FGraphics);
    if Graphic.CreateFromClipboard then begin
      CurrentItem:=Graphic;
      Graphic.Invalidate(Scroller)
    end  
    else Graphic.Free;
  finally
    ClipBoard.Close;
  end;
end;

Procedure TMainForm.UIInitializeApplicationActivate(Sender:TObject);
begin
  UserInterface.Update([uiMenus]);
end;

Procedure TMainForm.UpdateEditHandlerPosition;
begin
  EditHandler.Visible:=FALSE;
  EditHandler.XPosition:=FCurrentItem.BoundsRect.Left;
  EditHandler.YPosition:=FCurrentItem.BoundsRect.Bottom;
  EditHandler.Width:=RectWidth(FCurrentItem.BoundsRect);
  EditHandler.Height:=RectHeight(FCurrentItem.BoundsRect);
  EditHandler.Visible:=TRUE;
end;

Procedure TMainForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
var Result         : TModalResult;
begin
  if not Modified then CanClose:=TRUE
  else begin
    Result:=MessageDialog(Format(MlgSection[18],[FGraphics.FileName]),
        mtConfirmation,mbYesNoCancel,0);
    if Result=mrYes then begin
      if not FIsNewFile then CanClose:=Save
      else if SaveDialog.Execute then CanClose:=Save
      else CanClose:=FALSE;
    end  
    else CanClose:=Result=mrNo;
  end;  
end;

Function TMainForm.Save:Boolean;
var Cnt            : Integer;
begin
  // look if the template contains a project-placeholder
  Cnt:=0;
  while Cnt<FGraphics.ItemCount do begin
    if FGraphics[Cnt] is TProjectPlaceholder then break;
    Inc(Cnt);
  end;
  if Cnt>=FGraphics.ItemCount then begin
    MessageDialog(MlgSection[19],mtError,[mbOK],0);
    Result:=FALSE;
  end
  else begin
    UserInterface.BeginWaitCursor;
    try
      Result:=FGraphics.SaveToFile(SaveDialog.FileName);
      if Result then begin
        FModified:=FALSE;
        FIsNewFile:=FALSE;
        // update the caption of the main-window
        Caption:=ExtractFileName(FGraphics.FileName)+' - '+MlgSection[1];
        // add the file-name to the file-history
        FileHistoryList.Add(FGraphics.FileName);
      end;
    finally
      UserInterface.EndWaitCursor;
    end;
  end;  
end;

Function TMainForm.Open(const FileName: String): Boolean;
var NewGraphics    : TGraphics;
begin
  Result:=FALSE;
  if CloseQuery then begin
    UserInterface.BeginWaitCursor;
    try
      // uninitialize old graphic
      CurrentItem:=NIL;
      // load graphic from file
      NewGraphics:=TGraphics.Create;
      try
        NewGraphics.LoadFromFile(FileName);
        FGraphics.Free;
        FGraphics:=NewGraphics;
        // add file to history-list
        FileHistoryList.Add(FileName);
      except
        NewGraphics.Free;
        Raise;
      end;
      // update the caption of the main-window
      Caption:=ExtractFileName(FGraphics.FileName)+' - '+MlgSection[1];
      // update scroller-size and set overview
      UpdateSize;
      // update file-information
      FIsNewFile:=FALSE;                              
      FModified:=FALSE;
      // redraw the window
      Window.Invalidate;
      UserInterface.Update([uiMenus]);
      Result:=TRUE;
    finally
      UserInterface.EndWaitCursor;
    end;
  end;  
end;

Procedure TMainForm.FileHistoryListClick(Item: String);
begin
  Open(Item);
end;

Procedure TMainForm.UIExtensionUpdateCursor(Sender: TObject);
begin
  if MenuHandler<>NIL then Cursor:=MenuHandler.Cursor
  else Cursor:=crDefault;
end;

Procedure TMainForm.SetRubberboxCursor(Cursor: TCursor);
begin
  RubberBox.ActiveCursor:=Cursor;
  RubberBox.InactiveCursor:=Cursor;
  RubberBox.MinSizeReachedCursor:=Cursor;
end;
                                    
Procedure TMainForm.ViewToolbars;
begin
  UserInterface.ToolbarDialog;
end;

Procedure TMainForm.FilePrinterSetup;         
var Dialog         : TPrinterSetupDialog;
begin
  Dialog:=TPrinterSetupDialog.Create(Self);
  try                                               
    FGraphics.PageSetup.SetOptionsToPrinter;
    Dialog.Printer:=FGraphics.PageSetup.Printer;
    Dialog.ShowPrintToFile:=FALSE;
    Dialog.Caption:=MlgSection[20];
    if Dialog.ShowModal=mrOK then begin
      FGraphics.PageSetup.Printer:=Dialog.Printer;
      FGraphics.PageSetup.GetOptionsFromPrinter;
      UpdateSize;
    end;
  finally
    Dialog.Free;
  end;
end;

Procedure TMainForm.UpdateSize;
begin
  Scroller.LockUpdate;
  Scroller.Size:=RotRect(FGraphics.PageSetup.PageSize,0);
  Scroller.CurrentView:=Scroller.Size;
  Scroller.UnlockUpdate;
end;

Procedure TMainForm.EditLinks;
begin
  ShowMessage('Sorry, not yet implemented!');
end;

Procedure TMainForm.FormShow(Sender: TObject);
var Cnt            : Integer;
    ParamType      : Integer;
    FileName       : String;
begin
  // analyze the command-line parameters
  ParamType:=0;
  FileName:='';
  for Cnt:=1 to ParamCount do begin
    if CompareText(ParamStr(Cnt),'/V')=0 then ParamType:=1
    else if CompareText(ParamStr(Cnt),'/L')=0 then ParamType:=2
    else if ParamType=1 then FViews.Add(ParamStr(Cnt))
    else if ParamType=2 then FLegends.Add(ParamStr(Cnt))
    else FileName:=ParamStr(Cnt);
  end;
  if FileName<>'' then Open(FileName); 
end;

Initialization
  MenuFunctions.RegisterFromResource(HInstance,'PrintTemplate','Hint');
  FZoomInCursor:=AddCursor(LoadCursor(hInstance,'ZOOMIN'));

end.
