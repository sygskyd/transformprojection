Unit PropertiesDlg;

Interface

Uses Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     ComCtrls,Tabnotbk,Spinbtn,StdCtrls,WCtrls,Validate,MultiDlg,Measures,
     PrintTemplates;

Type TPropertiesDialog = Class(TMultiFormDialog)
       CancelBtn        : TButton;
       DefaultBtn       : TButton;
       Notebook         : TTabbedNotebook;
       Group1           : TWGroupBox;
       Group2           : TWGroupBox;
       Group3           : TWGroupBox;
       Group4           : TWGroupBox;
       HeightEdit       : TEdit;
       HeightLabel      : TWLabel;
       HeightSpin       : TSpinBtn;
       HeightVal        : TMeasureValidator;
       KeepAspectCheck  : TCheckBox;
       OkBtn            : TButton;
       OriginalSizeCheck: TCheckBox;
       WLabel1          : TWLabel;
       WLabel2          : TWLabel;
       WLabel3          : TWLabel;
       WLabel4          : TWLabel;
       WidthEdit        : TEdit;
       WidthSpin        : TSpinBtn;
       XScaleEdit       : TEdit;
       XScaleSpin       : TSpinBtn;
       YScaleEdit       : TEdit;
       YScaleSpin       : TSpinBtn;
       WLabel5          : TWLabel;
       WLabel6          : TWLabel;
       WLabel7          : TWLabel;
       WLabel8          : TWLabel;
       WidthLabel       : TWLabel;
       WidthVal         : TMeasureValidator;
       XPosEdit         : TEdit;
       XPosSpin         : TSpinBtn;
       XPosVal          : TMeasureValidator;
       YPosEdit         : TEdit;
       YPosSpin         : TSpinBtn;
       YPosVal          : TMeasureValidator;
       XScaleVal        : TMeasureValidator;
       YScaleVal        : TMeasureValidator;
       Procedure   DefaultBtnClick(Sender: TObject);
       Procedure   EditChange(Sender: TObject);
       Procedure   EditEnter(Sender: TObject);
       Procedure   EditExit(Sender: TObject);
       Procedure   FormCloseQuery(Sender: TObject; var CanClose: Boolean);
       Procedure   FormCreate(Sender: TObject);
       Procedure   FormHide(Sender: TObject);
       Procedure   FormShow(Sender: TObject);
       Procedure   OriginalSizeCheckClick(Sender: TObject);
       Procedure   WidthSpinClick(Sender: TObject);
    procedure WLabel3Click(Sender: TObject);
    procedure WLabel4Click(Sender: TObject);
    procedure WLabel1Click(Sender: TObject);
    procedure WLabel2Click(Sender: TObject);
    procedure WLabel7Click(Sender: TObject);
    procedure WLabel8Click(Sender: TObject);
{-- Sygsky  10-SEP-1999 BUG#260 BUILD#90
       procedure KeepAspectCheckClick(Sender: TObject);}
      Private
       FGraphic    : TCustomGraphic;
       FKeyPressed : Boolean;
       Procedure   SetFieldUnits(AUnits:TMeasureUnits);
{++ Sygsky 10-SEP-1999 BUG#260 BUILD#90 }
       Procedure   UpdateSizeFields(Sender: TObject);
{       Procedure   UpdateSizeFields(FromScaleFields:Boolean);}
{--}
      Public
       Property    Graphic:TCustomGraphic read FGraphic write FGraphic;
     end;

Implementation

{$R *.DFM}

Uses GrTools;

Procedure TPropertiesDialog.FormCreate(Sender: TObject);
begin
  RemoveFirstPage:=FALSE;
  DialogNotebook:=NoteBook;
end;

Procedure TPropertiesDialog.SetFieldUnits(AUnits: TMeasureUnits);
begin
  WidthVal.Units:=AUnits;
  HeightVal.Units:=AUnits;
  XPosVal.Units:=AUnits;
  YPosVal.Units:=AUnits;
end;

Procedure TPropertiesDialog.FormShow(Sender: TObject);
begin
  if GetLocaleStr(GetSystemDefaultLCID,LOCALE_IMEASURE,'0')='1' then
      SetFieldUnits(muInches)
  else SetFieldUnits(muCentimeters);
  if FGraphic<>NIL then with FGraphic do begin
{++ Sygsky 10-SEP-1999 BUG#260 BUILD#90 }
    OriginalBoundsRect := BoundsRect; { Initialize initial rectangle first }
{-- Sygsky}
    WidthVal[muMillimeters]:=RectWidth(BoundsRect);
    HeightVal[muMillimeters]:=RectHeight(BoundsRect);
    XPosVal[muMillimeters]:=BoundsRect.Left;
    YPosVal[muMillimeters]:=BoundsRect.Bottom;
    if RectEmpty(OriginalBoundsRect) then begin
      SetControlGroupEnabled(Group3,FALSE);
      OriginalSizeCheck.Checked:=FALSE;
      OriginalSizeCheck.Enabled:=FALSE;
      WidthLabel.Caption:='';
      HeightLabel.Caption:='';
    end
    else begin
      WidthLabel.Caption:=FloatToStrF(ConvertMeasure(RectWidth(OriginalBoundsRect),
          muMillimeters,WidthVal.Units),ffNumber,10,2)+' '+UnitsToShortName(WidthVal.Units);
      HeightLabel.Caption:=FloatToStrF(ConvertMeasure(RectHeight(OriginalBoundsRect),
          muMillimeters,HeightVal.Units),ffNumber,10,2)+' '+UnitsToShortName(WidthVal.Units);
    end;
    UpdateSizeFields(nil); { Reinitialize edit fields }
  end;
end;

{$ifdef NO_SYGSKY}
Procedure TPropertiesDialog.UpdateSizeFields(FromScaleFields:Boolean);
begin
  if FromScaleFields then begin
    WidthVal[muMillimeters]:=RectWidth(FGraphic.OriginalBoundsRect)
        *XScaleVal[muPercent]/100;
    HeightVal[muMillimeters]:=RectHeight(FGraphic.OriginalBoundsRect)
        *YScaleVal[muPercent]/100;
  end
  else if OriginalSizeCheck.Checked then begin
    XScaleVal[muPercent]:=100*WidthVal[muMillimeters]
        /RectWidth(FGraphic.OriginalBoundsRect);
    YScaleVal[muPercent]:=100*HeightVal[muMillimeters]
        /RectHeight(FGraphic.OriginalBoundsRect);
  end
  else begin
    XScaleVal[muPercent]:=100*WidthVal[muMillimeters]
        /RectWidth(FGraphic.BoundsRect);
    YScaleVal[muPercent]:=100*HeightVal[muMillimeters]
        /RectHeight(FGraphic.BoundsRect);
  end;
end;
{$else} {for ifdef NO_SYGSKY}
{++ Sygsky 10-SEP-1999 BUG#260 BUILD #90 }
Procedure TPropertiesDialog.UpdateSizeFields(Sender: TObject);
begin
  if (Sender = XScaleEdit) or (Sender = XScaleSpin) then begin
  { X percentage was changed}
    WidthVal[muMillimeters]:=RectWidth(FGraphic.OriginalBoundsRect)
      *XScaleVal[muPercent]/100;
    if KeepAspectCheck.Checked then begin
   { We have to adjust pair members also }
      YScaleVal[muPercent] := XScaleVal[muPercent];
      HeightVal[muMillimeters]:= RectHeight(FGraphic.OriginalBoundsRect)
        *YScaleVal[muPercent]/100;
    end;
    Exit;
  end;
  if (Sender = YScaleEdit) or (Sender = YScaleSpin) then begin
    { Y percentage was changed }
    HeightVal[muMillimeters]:=RectHeight(FGraphic.OriginalBoundsRect)
      *YScaleVal[muPercent]/100;
    if KeepAspectCheck.Checked then begin
    { We have to adjust pair members also }
      XScaleVal[muPercent] := YScaleVal[muPercent];
      WidthVal[muMillimeters]:=RectWidth(FGraphic.OriginalBoundsRect)
        *XScaleVal[muPercent]/100;
    end;
    Exit;
  end;
  if (Sender = WidthEdit) or (Sender = WidthSpin) then begin
    { X value was changed }
      XScaleVal[muPercent]:=100*WidthVal[muMillimeters]
        /RectWidth(FGraphic.OriginalBoundsRect);
    if KeepAspectCheck.Checked then begin
    { We have to adjust pair members also }
      YScaleVal[muPercent] := XScaleVal[muPercent];
      HeightVal[muMillimeters]:=RectHeight(FGraphic.OriginalBoundsRect)
        *YScaleVal[muPercent]/100;
    end;
    Exit;
  end;
  if (Sender = HeightEdit) or (Sender = HeightSpin) then begin
    YScaleVal[muPercent]:=100*HeightVal[muMillimeters]
      /RectHeight(FGraphic.OriginalBoundsRect);
    if KeepAspectCheck.Checked then begin
    { We have to adjust pair members also }
      XScaleVal[muPercent] := YScaleVal[muPercent];
      WidthVal[muMillimeters]:=RectWidth(FGraphic.OriginalBoundsRect)
        *XScaleVal[muPercent]/100;
    end;
    Exit;
  end;
  if Sender = nil then begin { It is the call during form first show }
    XScaleVal[muPercent] := 100.0;
    WidthVal[muMillimeters]:=RectWidth(FGraphic.OriginalBoundsRect);
    HeightVal[muMillimeters]:=RectHeight(FGraphic.OriginalBoundsRect);
    YScaleVal[muPercent] := 100.0;
    Exit;
  end;
end;
{-- Sygsky}
{$endif} {ifdef NO_SYSGKY}
Procedure TPropertiesDialog.FormHide(Sender: TObject);
begin
  if (ModalResult=mrOK) and (FGraphic<>NIL) then with FGraphic do begin
    BoundsRect:=GrBounds(XPosVal[muMillimeters],YPosVal[muMillimeters],
        WidthVal[muMillimeters],HeightVal[muMillimeters]);
  end;
end;

Procedure TPropertiesDialog.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  CanClose:=CheckValidators(Self);
end;

Procedure TPropertiesDialog.EditChange(Sender: TObject);
begin
  FKeyPressed:=TRUE;
end;

Procedure TPropertiesDialog.EditEnter(Sender: TObject);
begin
  FKeyPressed:=FALSE;
end;

Procedure TPropertiesDialog.EditExit(Sender: TObject);
begin
  if FKeyPressed then UpdateSizeFields(Sender);
end;

Procedure TPropertiesDialog.WidthSpinClick(Sender: TObject);
begin
  UpdateSizeFields(Sender);
end;

Procedure TPropertiesDialog.DefaultBtnClick(Sender: TObject);
begin
{  XScaleVal[muPercent]:=100; ++ Sygsky 13-SEP-1999 BUG#260 BUILD#90
  YScaleVal[muPercent]:=100;
  UpdateSizeFields(Sender); }
  UpdateSizeFields(nil);    // ++ Sygsky. To reinitialize the values
end;

{++ Sygsky 10-SEP-1999 BUG#260 BUILD#90.
  Proc now is useless and is set disabled for a future developers...
--}
Procedure TPropertiesDialog.OriginalSizeCheckClick(Sender: TObject);
begin
  UpdateSizeFields(Sender);
end;

{++ Sygsky 10-SEP-1999 BUG#260 BUILD#90. Commented as to be useless
procedure TPropertiesDialog.KeepAspectCheckClick(Sender: TObject);
begin
  UpdateSizeFields(Sender);
end;
--}
{++ Added by Sygsky 14-SEP-1999 BUG#260 BUILD#90 }
procedure TPropertiesDialog.WLabel3Click(Sender: TObject);
var
  V : Double;
begin
  V := Round(XScaleVal[muPercent]);
  if V <= 0.0 then V := 1.0;
  XScaleVal[muPercent] := V;
  UpdateSizeFields(XScaleEdit);
end;
procedure TPropertiesDialog.WLabel4Click(Sender: TObject);
var
  V : Double;
begin
  V := Round(YScaleVal[muPercent]);
  if V <= 0.0 then V := 1.0;
  YScaleVal[muPercent] := V;
  UpdateSizeFields(YScaleEdit);
end;
procedure TPropertiesDialog.WLabel1Click(Sender: TObject);
var
  V : Double;
begin
  V := Round(WidthVal[muMillimeters]);
  if V <= 0.0 then V := 1.0;
  WidthVal[muMillimeters] := V;
  UpdateSizeFields(WidthEdit);
end;
procedure TPropertiesDialog.WLabel2Click(Sender: TObject);
var
  V : Double;
begin
  V := Round(HeightVal[muMillimeters]);
  if V <= 0.0 then V := 1.0;
  HeightVal[muMillimeters] := V;
  UpdateSizeFields(HeightEdit);
end;
procedure TPropertiesDialog.WLabel7Click(Sender: TObject);
begin
  XPosVal[muMillimeters] := Round(XPosVal[muMillimeters]);
end;
procedure TPropertiesDialog.WLabel8Click(Sender: TObject);
begin
  YPosVal[muMillimeters] := Round(YPosVal[muMillimeters]);
end;
{-- Sygsky 14-SEP-1999 BUG#260 BUILD#90 }
end.
