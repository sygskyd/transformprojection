[Language]
DialogFontName='MS Sans Serif'
DialogFontSize=8
LanguageID=0809

[ShortCuts]
FileNew='Ctrl+N'
FileOpen='Ctrl+O'
FilePrint='Ctrl+P'
FileSave='Ctrl+S'
EditCut='Ctrl+X'
EditCut='Ctrl+Del'
EditCopy='Ctrl+C'
EditCopy='Ctrl+Ins'
EditPaste='Ctrl+V'
EditPaste='Shift+Ins'
EditDelete='Del'
ViewZoomIn='Ctrl+>'
ViewZoomOut='Ctrl+<'

[Strings,LegendPropertiesDialog]
1='Legend'
2='Legend'

[Strings,PrintTemplate]
1='The file %s is not WinGIS print-template.'
2='The file %s could not be loaded. The file was created with a newer version of the application.'
3='The file %s could not be loaded. The file has an invalid format.'
4='The file %s could not be loaded. The file does not exist or the access was denied.'
5='The file %s could not be loaded. A read-error occurred.'
6='The file %s could not be created. The file is write-protected or the access was denied.'
7='The file %s could not be loaded. A write-error occurred.'

11='The file %s could not be loaded. The file-format is not supported or the import-filter is not installed correctly.'
12='The file %s could not be loaded. A read-error occurred.'
13='The file %s could not be loaded. There is not enough memory to load the file.'

[Strings,PrnHndl]
1='Ok'
2='Cancel'
3='is being printed with'
4='Printing'
5='on %s'
6='Page %d of %d'
7='There are not printer-drivers installed on this system.'

[Strings,Validators]
1='You may not leave this field blank.'
2='You must enter %s characters.'
3='You must enter a value between %s and %s.'
4='The value you entered is invalid.'

50='Select &All'
51='C&ut'
52='&Copy'
53='&Paste'
54='&Delete'
55='C&onvert to'
56='&Units'
57='&Special Values'

[Strings,Printers]
100='Ready'
101='Paused;'
102='Error;'
103='Pending deletion;'
104='Paper jam;'
105='Paper out;'
106='Manual feed;'
107='Paper problem;'
108='Offline'
109='I/O active;'
110='Busy;'
111='Printing;'
112='Output bin full;'
113='Not available; '
114='Waiting;'
115='Processing;'
116='Initializing;'
117='Warming up;'
118='Toner low;'
119='No toner;'
120='Page punt;'
121='User intervention;'
122='Out of memory;'
123='Door open;'
124='Unavailable;'
125='Default printer;'
126='1 Document waiting'
127='%d Documents waiting'

[Strings,ProjectPropertiesDialog]
1='Layout'
2='Ok'
3='Cancel'
20='Region'
21='&Whole Project'
22='Saved &View'
23='&Current View'          
26='Size'
27='&Scale:'
28='&Fit to placeholder'

[Strings,MessageDialog]
1='Warning'
2='Error'
3='Information'
4='Confirm'

50='&Yes'
51='&No'
52='Ok'
53='Cancel'
54='&Cancel'
55='&Retry'
56='&Ignore'
57='&All'
58='Yes to All'
59='No to All'

[Strings,MainForm]
1='WinGIS Print-Template'
2='WinGIS Print Templates (*.wpt)|*.wpt'
3='wpt'
4='500%'
5='200%'
6='150%'
7='100%'
8='75%'
9='50%'
10='25%'
11='10%'
12='Pagewidth'
14='A placeholder for the project already exists. Do you want to replace it?'
15='A placeholder for the legend already exists. Do you want to replace it?'
16='&Link with file'
17='Do you really want to delete the selected item?'
18='The file %s has been modified. Do you want to save the changes?'
19='A print-template must contain a placeholder for the project.'
20='Printer Setup'
21='Print' 

[Strings,Hint]
1='Copy|copy'
2='Cut|'
3='Delete|'
4='Paste|'
5='Properties|'
6='Select|'
7='Close|'
8='New|'
9='Open|'
10='Page Setup|'
11='Print|'
12='Quit|'
13='Save|'
14='SaveAs|'
15='Contents|'
16='Info|'
17='Insert Legend Placeholder|'
18='Insert Object|'
19='Insert Picture|'
20='Insert Project Placeholder|'
21='Bring Forward|'
22='Bring to Front|'
23='Send Backward|'
24='Send to Back|'
25='Zoom|'
26='Overview|'
27='Zoom In|'
28='Zoom Out|'

[Strings,UserInterface]
1='Move, close or resize the window'
2='Close|Close the active window'
3='Restore the window to normal size'
4='Change the window position'
5='Change the size of the window'
6='Reduce the window to an icon'
7='Enlarge the window to full size'
8='Switch to the next window'
9='Switch to the previous window'
10='Move, close or resize the application-window'
11='Restore the application-window to normal size'
12='Change the application-window position'
13='Change the size of the application-window'
14='Reduce the application-window to an icon'
15='Enlarge the application-window to full size'
16='Exit|Quit the application'

[Strings,ToolbarSelectDialog]
1='Toolbars'
2='Ok'
3='Cancel'
4='Rollups'

[Strings,PrinterSetupDialog]
1='Printer Setup'
2='Ok'
3='Cancel'
6='&Name:'
7='Status:'
8='Type:'
9='Location:'
10='Comments:'
11='Properties...'
12='Printer'
13='Print to &File'

[MenuBar,MainMenu]
Menu={
FileMenu='&File'
  {
  FileNew='&New...'
  FileOpen='&Open...'
  <Separator
  FileSave='&Save'
  FileSaveAs='Save &As...'
  <Separator
  FilePageSetup='Pa&ge Setup...'
  FilePrinterSetup='P&rinter Setup...'
  FilePrint='&Print...'
  <FileHistory
  FileQuit='E&xit'
  }
EditMenu='&Edit'
  {
  _EditCut='C&ut'
  _EditCopy='&Copy'
  _EditPaste='&Paste'
  EditDelete='&Delete'
  <Separator
  EditProperties='P&roperties...'
  <Separator
  EditLinks='&Links...'
  }
}                                                               

[Toolbar,StandardToolbar]
Type=User
Caption='Standard'
Items={
FileNew
FileOpen
FileSave
<Separator
FilePrint
}

[Toolbar,ViewToolbar]
Type=User
Caption='View'
Items={
ViewZoom
ViewZoomAll
ViewZoomIn
ViewZoomOut
ViewZoomList
}
                                                               
[Toolbar,DrawToolbar]
Type=User                                             
Caption='Draw'
Items={
EditSelect
<Separator
InsertPicture  
InsertProjectFrame
InsertLegendFrame
<Separator
OrderBringToFront
OrderBringForward
OrderSendBackward
OrderSendToBack
<Separator
EditProperties
}

[Strings,PageSetup]
1='Letter 8 � x 11 inch'
2='Legal 8 � x 14 inch'
3='A4 210 x 297 mm'
4='C 17 x 22 inch'
5='D 22 x 34 inch'
6='E 34 x 44 inch'
7='Tabloid 11 x 17 inch'
8='Ledger 17 x 11 inch'
9='Statement 5 � x 8 � inch'
10='Executive 7 � x 10 � inch'
11='A3 297 x 420 mm'
12='A5 148 x 210 mm'
13='B4 250 x 354 mm'
14='B5 182 x 257 mm'
15='Folio 8 � x 13 inch'
16='Quarto 215 x 275 mm'
17='10 x 14 inch'
18='11 x 17 inch'
19='#9 Envelope 3 7/8 x 8 7/8 inch'
20='#10 Envelope 4 1/8 x 9 � inch'
21='#11 Envelope 4 � x 10 3/8 inch'
22='#12 Envelope 4 � x 11 inch'
23='#14 Envelope 5 x 11 � inch'
24='DL Envelope 110 x 220 mm'
25='C5 Envelope 162 x 229 mm'
26='C3 Envelope  324 x 458 mm'
27='C4 Envelope  229 x 324 mm'
28='C6 Envelope  114 x 162 mm'
29='C65 Envelope 114 x 229 mm'
30='B4 Envelope  250 x 353 mm'
31='B5 Envelope  176 x 250 mm'
32='B6 Envelope  176 x 125 mm'
33='Italy Envelope 110 x 230 mm'
34='Monarch Envelope 3 7/8 x 7 � inch'
35='6 � Envelope 3 5/8 x 6 � inch'
36='US Std Fanfold 14 7/8 x 11 inch'
37='German Std Fanfold 8 � x 12 inch'
38='German Legal Fanfold 8 � x 13 inch'

[Strings,UnitsNames]
0=''
1='Drawing units'
2='Pixels'
3='Points'
4='Picas'
5='Millimeters'
6='Centimeters'
7='Meters'
8='Kilometers'
9='Inches'
10='Feet'
11='Yards'
12='Miles'
13='Lines'
14='Percent'
15='rad'
16='Degrees'
17='Grad'

50=''
51='du'
52='px'
53='pt'
54='pic'
55='mm'
56='cm'
57='m'
58='km'
59='"'
60='ft'
61='yd'
62='mi'
63='li'
64='%'
65='rad'
66='gon'
67='�'

[PopupMenu,MainPopupMenu]
Menu={
  _EditCut='C&ut'
  _EditCopy='&Copy'
  _EditPaste='&Paste'
  EditDelete='&Delete'
  <Separator
  EditProperties='P&roperties...'
}
