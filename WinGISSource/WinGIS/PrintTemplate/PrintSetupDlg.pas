Unit PrintSetupDlg;

Interface

Uses Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     ExtCtrls,StdCtrls,WCtrls,MultiLng,WPrinter;

Type TPrinterSetupDialog = Class(TWForm)
       OkBtn: TButton;
       CancelBtn: TButton;
       MlgSection: TMlgSection;
       Group1: TWGroupBox;
       PrinterCombo: TComboBox;   
       Label10: TWLabel;
       StatusLabel: TWLabel;
       Label12: TWLabel;
       TypeLabel: TWLabel;
       Label11: TWLabel;
       LocationLabel: TWLabel;
       WLabel9: TWLabel;
       CommentsLabel: TWLabel;
       PropertiesBtn: TButton;
       Label1: TWLabel;
       PrintToFileCheck: TCheckBox;
       Bevel1: TBevel;
       Procedure   FormCreate(Sender: TObject);
       procedure   FormHide(Sender: TObject);
       Procedure   FormShow(Sender: TObject);
       Procedure   PrinterComboClick(Sender: TObject);
       Procedure   PropertiesBtnClick(Sender: TObject);
      Private
       FPrintToFile: Boolean;
       FPrinter    : TWPrinter;
       FOrgPrinter : TWPrinter;
       Procedure   UpdatePrinter;
      Public
       Property    ShowPrintToFile:Boolean read FPrintToFile write FPrintToFile default TRUE;
       Property    Printer:TWPrinter read FPrinter write FOrgPrinter;
     end;

Implementation                                                   

{$R *.DFM}

Uses UserIntf;

Procedure TPrinterSetupDialog.FormCreate(Sender: TObject);
begin
  PrinterCombo.Items:=WPrinters.Printers;
  FPrinter:=WPrinters.DefaultPrinter;
  FPrintToFile:=TRUE;
end;                                          

Procedure TPrinterSetupDialog.UpdatePrinter;
var Count         : Integer;
    Number        : Integer;
    Comments      : String;
begin
  TypeLabel.Caption:=FPrinter.Typ;
  Comments:=FPrinter.Comments;
  // replace line-feeds by semicolons
  Count:=Pos(#13#10,Comments);
  while Count<>0 do begin
    Comments[Count]:=';'; Comments[Count+1]:=' ';
    Count:=Pos(#13#10,Comments);
  end;
  CommentsLabel.Caption:=Comments;
  if FPrinter.Location='' then LocationLabel.Caption:=FPrinter.Port
  else LocationLabel.Caption:=FPrinter.Location;
  if FPrinter.IsDefaultPrinter then StatusLabel.Caption:=MlgStringList['Printers',125]
  else StatusLabel.Caption:='';
  if FPrinter.Status<>0 then begin
    Count:=FPrinter.WaitingDocuments;
    if Count=1 then Number:=126
    else Number:=127;
    StatusLabel.Caption:=StatusLabel.Caption+FPrinter.StatusText+' '
        +Format(MlgStringList['Printers',Number],[Count]);
  end
  else StatusLabel.Caption:=StatusLabel.Caption+FPrinter.StatusText;
  PrintToFileCheck.Checked:=FPrinter.PrintToFile;
end;

Procedure TPrinterSetupDialog.FormShow(Sender: TObject);
begin                                          
  PrintToFileCheck.Visible:=FPrintToFile;
  if FOrgPrinter<>NIL then FPrinter:=FOrgPrinter;
  FPrinter:=WPrinters[WPrinters.Printers.IndexOf(FPrinter.Name)];
  if FPrinter=NIL then FPrinter:=WPrinters.DefaultPrinter;
  PrinterCombo.ItemIndex:=PrinterCombo.Items.IndexOf(FPrinter.Name);
  UpdatePrinter;
end;

Procedure TPrinterSetupDialog.PrinterComboClick(Sender: TObject);
begin
  UserInterface.BeginWaitCursor;
  FPrinter:=WPrinters[WPrinters.Printers.IndexOf(PrinterCombo.Text)];
  UpdatePrinter;
  UserInterface.EndWaitCursor;
end;

Procedure TPrinterSetupDialog.PropertiesBtnClick(Sender: TObject);
begin
  FPrinter.Properties(Handle); 
end;

procedure TPrinterSetupDialog.FormHide(Sender: TObject);
begin
  if ModalResult=mrOK then FPrinter.PrintToFile:=PrintToFileCheck.Checked;
end;

end.
