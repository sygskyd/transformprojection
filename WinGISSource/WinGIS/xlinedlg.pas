Unit XLineDlg;
                                                     
Interface
                                             
Uses Windows,Classes,Controls,Dialogs,Forms,Graphics,Messages,SysUtils,WCtrls,
     Menus,Combobtn,Spinbtn,StdCtrls,Validate,XLines,XStyles,Scroller,MenuHndl,
     GrTools,EditHndl,InpHndl,ColorSelector,Palette,ComCtrls,ExtCtrls,Measures,
     MultiLng,SnapHndl,Buttons,Tabnotbk,ActnList,ImgList,RectHndl,LineDropDown,
     PolyEdit;

Type TXLineStyleDialog  = Class(TWForm)
       ActionList       : TActionList;
       Arc2PointsCenterBtn: TSpeedBtn;
       Arc3PointsBtn    : TSpeedBtn;
       ArcBtn           : TSpeedBtn;
       ArcCenterBtn     : TSpeedBtn;
       ArcRectBtn       : TSpeedBtn;
       ArcsPanel        : TPanel;
       CancelBtn        : TButton;
       ChordBtn         : TSpeedBtn;
       CirclesPanel     : TPanel;
       Circle2PointsCenterBtn: TSpeedBtn;
       CircleCenterBtn  : TSpeedBtn;
       Circle2PointsBtn : TSpeedBtn;
       Circle3PointsBtn : TSpeedBtn;
       CircleRectBtn    : TSpeedBtn;
       CircleBtn        : TSpeedBtn;
       DeleteMenu       : TMenuItem;
       CutEdgesCheck    : TCheckBox;
       CycleVal         : TMeasureValidator;
       CycleEdit        : TEdit;
       CycleSpin        : TSpinBtn;
       DefaultPopupMenu : TPopupMenu;
       EditBtn          : TSpeedBtn;
       EditHandler      : TEditHandler;
       EditWindow       : TWindow;
       FillColorBtn     : TColorSelectBtn;
       Label4           : TWLabel;
       LineColorBtn     : TColorSelectBtn;
       LineWidthEdit    : TEdit;
       LineWidthSpin    : TSpinBtn;
       LineWidthVal     : TMeasureValidator;
       MlgSection       : TMlgSection;
       N1               : TMenuItem;
       N3               : TMenuItem;
       OKBtn            : TButton;
       OneToFrontMenu   : TMenuItem;
       OneToBackMenu    : TMenuItem;
       PolygonBtn       : TSpeedBtn;
       PropertiesMenu   : TMenuItem;
       RectBtn          : TSpeedBtn;
       RubberArc        : TRubberArc;
       RubberBox        : TRubberBox;
       RubberEllipse    : TRubberEllipse;
       RubberPolygon    : TRubberPolygon;
       Scroller         : TScroller;
       StatusBar        : TWStatusBar;
       ToFrontMenu      : TMenuItem;
       ToBackMenu       : TMenuItem;
       UnitsCombo       : TComboBox;
       WLabel2          : TWLabel;
       ZoomBtn          : TSpeedBtn;
       ZoomAllBtn       : TSpeedBtn;
       ZoomBox          : TRubberBox;
       ZoomInBtn        : TSpeedBtn;
       ZoomOutBtn       : TSpeedBtn;
       NoteBook         : TPageControl;
       WLabel1          : TWLabel;
       NameEdit         : TEdit;
       Bevel2           : TBevel;
       LineBtn          : TSpeedBtn;
       SpeedBtn2        : TSpeedBtn;
       NameVal          : TValidator;
       ImageList        : TImageList;
       EditCutAction    : TAction;
       EditCopyAction   : TAction;
       EditPasteAction  : TAction;
       EditDeleteAction : TAction;
       BringToFrontAction: TAction;
       BringForwardAction: TAction;
       SendToBackAction : TAction;
       SendBackwardAction: TAction;
       EditPropertiesAction: TAction;
       CutMenu          : TMenuItem;
       CopyMenu         : TMenuItem;
       PasteMenu        : TMenuItem;
       Group1           : TWGroupBox;
       WLabel3          : TWLabel;
       WLabel4          : TWLabel;
       WLabel5          : TWLabel;
       WLabel6          : TWLabel;
       ScaleEdit        : TEdit;
       WidthEdit        : TEdit;
       ColorSelectBtn   : TColorSelectBtn;
       WidthSpin: TSpinBtn;
       LineStyleDropDown: TDropDownBtn;
       GeneralSheet     : TTabSheet;
       DefinitionSheet  : TTabSheet;
       GuideLine        : TGuideLine;
       PropertiesBtn: TSpeedBtn;
       ScaleVal: TScaleValidator;
       GeneralizeCheck: TCheckBox;
       ScaleSpin: TSpinBtn;
    WidthVal: TMeasureLookupValidator;
    ScaleIndependCheck: TCheckBox;
    GeneralizePrintCheck: TCheckBox;
    TurnItCheckBox: TCheckBox;
       Procedure   CircleBtnClick(Sender:TObject);
       Procedure   ColorBtnSelectColor(Sender:TObject);
       Procedure   DeleteMenuClick(Sender:TObject);
       Procedure   EditBtnClick(Sender:TObject);
       Procedure   EditHandlerChange(Sender:TObject);
       Procedure   EditHandlerMove(Sender:TObject);
       Procedure   EditHandlerResize(Sender:TObject);
       Procedure   EditHandlerRotate(Sender:TObject);
       Procedure   EditWindowPaint(Sender:TObject);
       Procedure   EditWindowMouseDown(Sender:TObject; Button:TMouseButton;
                      Shift:TShiftState; X, Y:Integer);
       Procedure   EditWindowMouseMove(Sender:TObject; Shift:TShiftState; X,
                      Y:Integer);
       Procedure   EditWindowMouseUp(Sender:TObject; Button:TMouseButton;
                      Shift:TShiftState; X, Y:Integer);
       Procedure   FormCreate(Sender:TObject);
       Procedure   FormDestroy(Sender:TObject);
       Procedure   FormHide(Sender:TObject);
       Procedure   FormShow(Sender:TObject);
       Procedure   LineWidthChanged(Sender:TObject);
       Procedure   OneToBackMenuClick(Sender:TObject);
       Procedure   OneToFrontMenuClick(Sender:TObject);
       Procedure   PolygonBtnClick(Sender:TObject);
       Procedure   RectBtnClick(Sender:TObject);
       Procedure   RubberBoxFinishedInput(Sender:TObject);
       Procedure   RubberEllipseFinishedInput(Sender:TObject);
       Procedure   RubberPolygonFinishedInput(Sender:TObject);
       Procedure   ToFrontMenuClick(Sender:TObject);
       Procedure   ToBackMenuClick(Sender:TObject);
       Procedure   UnitsComboClick(Sender:TObject);
       Procedure   ZoomAllBtnClick(Sender:TObject);
       Procedure   ZoomBoxFinishedInput(Sender:TObject);
       Procedure   ZoomBtnClick(Sender:TObject);
       Procedure   ZoomInMenuClick(Sender:TObject);
       Procedure   ZoomOutMenuClick(Sender:TObject);
       Procedure   EditCutActionExecute(Sender:TObject);
       Procedure   EditCopyActionExecute(Sender:TObject);
       Procedure   EditPasteActionExecute(Sender:TObject);
       Procedure   LineBtnClick(Sender:TObject);
       Procedure   CircleModeBtnClick(Sender:TObject);
       Procedure   ArcModeBtnClick(Sender:TObject);
       Procedure   ArcBtnClick(Sender:TObject);
       Procedure   ChordBtnClick(Sender:TObject);
       Procedure   RubberArcFinishedInput(Sender:TObject);
       Procedure   FormCloseQuery(Sender:TObject; var CanClose: Boolean);
       Procedure   GuideLineMove(Sender:TObject);
       Procedure   EditPropertiesActionExecute(Sender:TObject);
       Procedure   GeneralizeCheckClick(Sender:TObject);
       Procedure   LineStyleDropDownPaintBtn(Sender: TObject; ARect: TRect);
       Procedure   WidthEditChange(Sender: TObject);
    procedure CycleEditChange(Sender: TObject);
    procedure CutEdgesCheckClick(Sender: TObject);
    procedure TurnItCheckBoxClick(Sender: TObject);
      Private
       FClipboardSet    : Boolean;
       FClipboard       : TXLineDef;
       FCurrentItem     : PXLineDef;
       FLineDropDown    : TLineDropDownForm;
       FMenuHandler     : TMenuHandler;
       FOrgPalette      : TPalette;
       FOrgLineStyle    : TXLineStyle;
       FPalette         : TPalette;
       FShift           : TShiftState;
       FSnapGrid        : TSnapGrid;
       FSnapHandlers    : TSnapHandlers;
       FUnits           : TMeasureUnits;
       FUnitsValue      : Double;
       FXLineStyle      : TXLineStyle;
       FXLineStyles     : TXLineStyleList;

       iCycle: Integer;
       bClipIt,
       bTurnIt: Boolean;

       Function    ClipRectOfCurrentItem:TRect;
       Procedure   DoSnapFinished;
       Procedure   DoStyleSelect(Sender:TObject);
       Function    FindItemFromPoint(Const Point:TGrPoint):PXLineDef;
       Procedure   MoveItem(FromIndex,ToIndex:Integer);
       Procedure   SetCurrentItem(AItem:PXLineDef);
       Procedure   SetMenuHandler(AMenuHandler:TMenuHandler);
       Procedure   SetupMenuHandler(AMenuHandler:TMenuHandler);
       Procedure   SetRectFromEditHandler;
       Procedure   SetUnits(AUnits:TMeasureUnits);
       procedure SetEditHandlerSize;
      Private
       Property    CurrentItem:PXLineDef read FCurrentItem write SetCurrentItem;
       Property    MenuHandler:TMenuHandler read FMenuHandler write SetMenuhandler;
       Property    Units:TMeasureUnits read FUnits write SetUnits;
       Procedure   UpdateEnabled;
       Procedure   UpdateSize;
       Procedure   UpdateStatus;
      Public
       Property    Palette:TPalette read FPalette write FOrgPalette;
       Property    XLineStyle:TXLineStyle read FXLineStyle write FOrgLineStyle;
       Property    XLineStyles:TXLineStyleList read FXLineStyles write FXLineStyles;
     end;

Implementation

{$R *.DFM}

Uses AM_Def,NumTools,StyleDef;

Procedure TXLineStyleDialog.FormCreate(Sender:TObject);
Const AllowedUnits = [muMillimeters,muCentimeters,muInches,muPicas];
var Cnt          : TMeasureUnits;
    SnapVector   : TSnapVector;
begin
  { initialize scroller }
  Scroller.Canvas:=EditWindow.Canvas;
  { set default size for the graphic }
  FPalette:=TPalette.Create;
  FXLineStyle:=TXLineStyle.Create;
  { setup color-selectors }
  LineColorBtn.Palette:=FPalette;
  SetupSelectOptions(LineColorBtn.Options,sotColor,[soObject]);
  FillColorBtn.Palette:=FPalette;
  SetupSelectOptions(FillColorBtn.Options,sotColor,[soNone,soObject]);
  ColorSelectBtn.Palette:=FPalette;
  SetupSelectOptions(ColorSelectBtn.Options,sotColor,[soLayer]);
  { setup units-combo }
  for Cnt:=Low(TMeasureUnits) to High(TMeasureUnits) do
      if Cnt in AllowedUnits then UnitsCombo.Items.Add(UnitsToShortName(Cnt));
  Units:=muMillimeters;
  UnitsCombo.ItemIndex:=UnitsCombo.Items.IndexOf(UnitsToShortName(FUnits));
  // create the snap-system and default snap-handlers
  FSnapHandlers:=TSnapHandlers.Create;
  FSnapHandlers.OnFinished:=DoSnapFinished;
  FSnapHandlers.Scroller:=Scroller;
  // x-axis snap-vector
  SnapVector:=TSnapVector.Create(FSnapHandlers);
  SnapVector.Direction:=GrPoint(1,0);
  // y-axis snap-vector
  SnapVector:=TSnapVector.Create(FSnapHandlers);
  SnapVector.Direction:=GrPoint(0,1);
  // grid
  FSnapGrid:=TSnapGrid.Create(FSnapHandlers);
  FSnapGrid.XDistance:=50;
  FSnapGrid.YDistance:=50;
  // cycle-guideline
  Guideline.Angle:=Pi/2;
  Guideline.Selected:=TRUE;
  SetupMenuHandler(Guideline);
  // setup the menu-handlers
  SetupMenuHandler(EditHandler);
  SetupMenuHandler(RubberEllipse);
  SetupMenuHandler(RubberBox);
  SetupMenuHandler(ZoomBox);
  SetupMenuHandler(RubberPolygon);
  SetupMenuHandler(RubberArc);
  GeneralizeCheck.Width:=Canvas.TextWidth(GeneralizeCheck.Caption)+16;
  // create line-dropdown
  FLineDropDown:=TLineDropDownForm.Create(Self);
  LineStyleDropDown.DropDown:=FLineDropDown;
  FLineDropDown.OnSelectStyle:=DoStyleSelect;
end;

Procedure TXLineStyleDialog.SetupMenuHandler(AMenuHandler:TMenuHandler);
begin
  AMenuHandler.Canvas:=EditWindow.Canvas;
  AMenuHandler.OnWinToIntPoint:=Scroller.WindowToInternal;
  AMenuHandler.OnWinToIntDistance:=Scroller.WindowToInternal;
  AMenuHandler.OnIntToWinPoint:=Scroller.InternalToWindow;
  AMenuHandler.OnIntToWinDistance:=Scroller.InternalToWindow;
end;

Procedure TXLineStyleDialog.FormDestroy(Sender:TObject);
begin
  FPalette.Free;
  FXLineStyle.Free;
  FSnapHandlers.Free;
end;

Procedure TXLineStyleDialog.FormShow(Sender:TObject);
begin
  if FOrgLineStyle<>NIL then FXLineStyle.Assign(FOrgLineStyle);
  if FOrgPalette<>NIL then FPalette.Assign(FOrgPalette);
  with FXLineStyle do begin
    GeneralizeCheck.Checked:=Generalize;
    GeneralizePrintCheck.Checked:=not GeneralizeWhenPrinting;
    ScaleVal.AsFloat:=GeneralizeScale;
    with GeneralizeStyle do begin
      FLineDropDown.Style:=Style;
      ColorSelectBtn.SelectedColor:=Color;
      SetSizeField(Width,WidthType,WidthVal,NIL,ScaleIndependCheck);
    end;
    NameEdit.Text:=Name;
  end;
  { set scroller-size, if line-style is empty set default size }
  UpdateSize;
  { set overview }
  Scroller.CurrentView:=Scroller.Size;
  UpdateEnabled;
{++ New XLine BUG#251}
  // Detect segment parameters. They are storaged in the first item of the style.
  If FXLineStyle.Count > 0 Then Begin
     SELF.iCycle := FXLineStyle.Items[0].Cycle;
     SELF.bClipIt := Boolean(Byte(FXLineStyle.Items[0].ClipEdges) AND ci_XLine_ClipIt);
     SELF.bTurnIt := Boolean(Byte(FXLineStyle.Items[0].ClipEdges) AND ci_XLine_TurnIt);
     End
  Else Begin
  // If there isn't any style item, the parameters will be set as default.
     SELF.iCycle := 1000;
     SELF.bClipIt := FALSE;
     SELF.bTurnIt := TRUE;
     End;
  // Set visual components.
  CycleVal[muMillimeters] := SELF.iCycle/100.0;
  TurnItCheckBox.Checked := SELF.bTurnIt;
  CutEdgesCheck.Checked := SELF.bClipIt;
  // Set Guide Line.
  Guideline.Position := GrPoint(SELF.iCycle, 0);
  Guideline.Visible:=TRUE;
  EditWindow.Invalidate;
{-- New XLine BUG#251}
end;

Procedure TXLineStyleDialog.EditWindowPaint(Sender:TObject);
var Cnt          : Integer;
    Cnt1         : Integer;
    WinPoints    : Array[1..PolyPoints] of TPoint;
    XLineDef     : PXLineDef;
    WinX         : Double;
    WinY         : Double;
    ARadius      : Double;
    ARect        : TRect;
begin
  with EditWindow,Canvas do begin
    ARect:=ClientRect;
    DPToLP(Handle,ARect,2);
    // paint coordinate-axes 
    Pen.Style:=psDashDot;
    Pen.Color:=clBlue;
    Pen.Width:=0;
    Scroller.InternalToWindow(0,0,WinX,WinY);
    Polyline([Point(ARect.Left,Round(WinY)),Point(ARect.Right,Round(WinY))]);
    Polyline([Point(Round(WinX),ARect.Top),Point(Round(WinX),ARect.Bottom)]);
    // paint XLineDefs
    Pen.Style:=psSolid;
    for Cnt:=0 to FXLineStyle.Count-1 do begin
      XLineDef:=FXLineStyle[Cnt];
      with XLineDef^ do begin
        Pen.Color:=LineColor;
        Pen.Width:=LineWidth;
        if FillColor<>clNone then begin
          Brush.Style:=bsSolid;
          Brush.Color:=FillColor;
        end
        else Brush.Style:=bsClear;
        for Cnt1:=1 to PointCount do begin
          Scroller.InternalToWindow(Points[Cnt1].X,Points[Cnt1].Y,WinX,WinY);
          WinPoints[Cnt1].X:=Round(WinX);
          WinPoints[Cnt1].Y:=Round(WinY);
        end;
        ARadius:=Scroller.InternalToWindow(Points[1].X);
        case ItemType of
          itArc      : Arc(Round(WinPoints[2].X-ARadius),Round(WinPoints[2].Y-ARadius),
              Round(WinPoints[2].X+ARadius),Round(WinPoints[2].Y+ARadius),
              Round(WinPoints[3].X),Round(WinPoints[3].Y),
              Round(WinPoints[4].X),Round(WinPoints[4].Y));
          itChord,
          itCircle   : Chord(Round(WinPoints[2].X-ARadius),Round(WinPoints[2].Y-ARadius),
              Round(WinPoints[2].X+ARadius),Round(WinPoints[2].Y+ARadius),
              Round(WinPoints[3].X),Round(WinPoints[3].Y),
              Round(WinPoints[4].X),Round(WinPoints[4].Y));
          itRectangle,itPolygon: Polygon(Slice(WinPoints,PointCount));
          else Polyline(Slice(WinPoints,PointCount));
        end;
      end;
    end;
    // paint grid
    FSnapHandlers.Paint;
    // paint currently active menu-handler
    if FMenuHandler<>NIL then FMenuHandler.Paint;
    Guideline.Paint;
  end;
end;

{******************************************************************************+
  Function TXLinestyleDialog.FindItemFromPoint
--------------------------------------------------------------------------------
  Determines the XLineDef on the specified position or returns NIL if there
  is none.
+******************************************************************************}
Function TXLineStyleDialog.FindItemFromPoint(Const Point:TGrPoint):PXLineDef;
var Cnt          : Integer;
    Cnt1         : Integer;
    CutPoint     : TGrPoint;
    PointOnLine  : Boolean;
    Polygon      : Array[1..PolyPoints] of TGrPoint;
begin
  for Cnt:=0 to FXLineStyle.Count-1 do begin
    Result:=FXLineStyle[Cnt];
    with Result^ do begin
      if ItemType<itRectangle then begin
        if DblCompare(GrPointDistance(GrPoint(Points[2].X,Points[2].Y),
            Point),Points[1].X)<=0 then Exit;
      end
      else if ItemType=itPolyline then begin
        for Cnt1:=1 to PointCount-1 do if (Abs(LineNormalIntersection(
            GrPoint(Points[Cnt1].X,Points[Cnt1].Y),GrPoint(Points[Cnt1+1].X,
            Points[Cnt1+1].Y),Point,CutPoint,PointOnLine))<Scroller.WindowToInternal(100))
            and PointOnLine then Exit;
      end
      else begin
        for Cnt1:=1 to PointCOunt do Polygon[Cnt1]:=GrPoint(Points[Cnt1].X,Points[Cnt1].Y);
        if PointInsidePolygon(Point,Polygon,PointCount) then Exit;
      end;
    end;    
  end;
  Result:=NIL;
end;

{******************************************************************************+
  Function TXLineStyleDialog.ClipRectOfCurrentItem
--------------------------------------------------------------------------------
  Calculates the clipping-rectangle of the current item.
+******************************************************************************}
Function TXLineStyleDialog.ClipRectOfCurrentItem:TRect;
begin
  Result:=TXlineStyle.ClipRectOfItem(FCurrentItem^);
end;

Procedure TXLineStyleDialog.SetEditHandlerSize;
var ARect          : TRect;
begin
  Assert(FCurrentItem<>NIL);
  with EditHandler,FCurrentItem^ do begin
    if ItemType=itRectangle then begin
      XPosition:=Points[1].X;
      YPosition:=Points[1].Y;
      Width:=GrPointDistance(GrPoint(Points[1].X,Points[1].Y),GrPoint(Points[2].X,Points[2].Y));
      Height:=GrPointDistance(GrPoint(Points[2].X,Points[2].Y),GrPoint(Points[3].X,Points[3].Y));
      Rotation:=LineAngle(GrPoint(Points[1].X,Points[1].Y),GrPoint(Points[2].X,Points[2].Y));
    end
    else begin
      ARect:=ClipRectOfCurrentItem;
      XPosition:=ARect.Left;
      YPosition:=ARect.Top;
      Width:=RectWidth(ARect);
      Height:=RectHeight(ARect);
      Rotation:=0;
    end;
  end;
end;  

Procedure TXLineStyleDialog.SetCurrentItem(AItem:PXLineDef);
begin
  if AItem<>FCurrentItem then begin
    // store the values of the edit-fields for the current-item
    if FCurrentItem<>NIL then with FCurrentItem^ do begin
      Cycle:=Round(CycleVal[muMillimeters]*100);
      LineWidth:=Round(LineWidthVal[muMillimeters]*100);
      ClipEdges:=CutEdgesCheck.Checked;
{++ New XLine BUG#251}
{ It was... We don't need it now. Ivanoff.
      GuideLine.Visible:=FALSE;}
{-- New XLine BUG#251}
    end;
    MenuHandler:=NIL;
    FCurrentItem:=AItem;
    if FCurrentItem<>NIL then with EditHandler,FCurrentItem^ do begin
      SetEditHandlerSize;

      if ItemType=itCircle then Options:=[ehoSize,ehoSizeMultiple,ehoSizeSymmetric,
          ehoKeepAspect,ehoMoveInside]
      else if ItemType<itRectangle then Options:=[ehoSize,ehoSizeMultiple,ehoSizeSymmetric,
          ehoRotate,ehoKeepAspect,ehoMoveInside]
      else Options:=[ehoSize,ehoSizeMultiple,ehoSizeSymmetric,
          ehoRotate,ehoMoveInside];
      MenuHandler:=EditHandler;
      { set current color to color-selectors }
      LineColorBtn.SelectedColor:=LineColor;
      FillColorBtn.SelectedColor:=FillColor;
      { fill edit-fields }
      LineWidthVal[muMillimeters]:=LineWidth/100.0;
{++ New XLine BUG#251}
{ It was earlier. Now we don't need it now. Ivanoff.
      CycleVal[muMillimeters]:=Cycle/100.0;
      CutEdgesCheck.Checked:=ClipEdges;
      // setup Guideline
      Guideline.Position:=GrPoint(Cycle,0);
      Guideline.Visible:=TRUE;
{-- New XLine BUG#251}
    end;
  end;
  UpdateEnabled;
  UpdateStatus;
end;

Procedure TXLineStyleDialog.SetMenuHandler(AMenuHandler:TMenuHandler);
begin
  if FMenuHandler<>NIL then begin
    if FMenuHandler=RubberEllipse then CirclesPanel.Visible:=FALSE
    else if FMenuHandler=RubberArc then ArcsPanel.Visible:=FALSE;
    FMenuHandler.Visible:=FALSE;
  end;  
  FMenuHandler:=AMenuHandler;
  if FMenuHandler<>NIL then with FMenuHandler do begin
    if FMenuHandler=RubberEllipse then CirclesPanel.Visible:=TRUE
    else if FMenuHandler=RubberArc then ArcsPanel.Visible:=TRUE;
    Canvas:=EditWindow.Canvas;
    Visible:=TRUE;
  end
  else EditBtn.Down:=TRUE;
  if FMenuHandler=ZoomBox then EditWindow.PopupMenu:=NIL
  else EditWindow.PopupMenu:=DefaultPopupMenu;
end;

Procedure TXLineStyleDialog.EditWindowMouseDown(Sender:TObject;
    Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var IntX         : Double;
    IntY         : Double;
    APoint       : TPoint;
    Handled      : Boolean;
begin
  APoint:=Point(X,Y);
  DPToLP(EditWindow.Canvas.Handle,APoint,1);
  Scroller.WindowToInternal(APoint.X,APoint.Y,IntX,IntY);
  Handled:=(FMenuHandler<>NIL) and FMenuHandler.OnMouseDown(IntX,IntY,Button,Shift);
  if not Handled then Handled:=Guideline.OnMouseDown(IntX,IntY,Button,Shift);
  if not Handled and (Button=mbLeft) then CurrentItem:=FindItemFromPoint(GrPoint(IntX,IntY));
end;

Procedure TXLineStyleDialog.EditWindowMouseMove(Sender:TObject;
    Shift: TShiftState; X, Y: Integer);
var IntX         : Double;
    IntY         : Double;
    APoint       : TPoint;
    Handled      : Boolean;
begin
  APoint:=Point(X,Y);
  DPToLP(EditWindow.Canvas.Handle,APoint,1);
  Scroller.WindowToInternal(APoint.X,APoint.Y,IntX,IntY);
//FSnapHandlers.SnapPoint(GrPoint(IntX,IntY),50);
  StatusBar[0]:=Format('X: %.2f Y: %.2f ',[IntX*FUnitsValue,
      IntY*FUnitsValue]);
  Handled:=(FMenuHandler<>NIL) and not Guideline.Editing
      and FMenuHandler.OnMouseMove(IntX,IntY,FShift);
  if not Handled then Guideline.OnMouseMove(IntX,IntY,Shift);
end;

Procedure TXLineStyleDialog.EditWindowMouseUp(Sender:TObject;
    Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var IntX         : Double;
    IntY         : Double;
    APoint       : TPoint;
    Factor       : Double;
begin
  APoint:=Point(X,Y);
  DPToLP(EditWindow.Canvas.Handle,APoint,1);
  Scroller.WindowToInternal(APoint.X,APoint.Y,IntX,IntY);
  if (Button=mbRight) and ZoomBtn.Down then begin
    Factor:=2;
    if ssShift in Shift then Factor:=Factor*2;
    if ssCtrl in Shift then Factor:=Factor*4;
    Scroller.ZoomByFactorWithCenter(GrPoint(IntX,IntY),1/Factor);
  end
  else if (FMenuHandler=NIL) or not FMenuHandler.OnMouseUp(IntX,IntY,Button,Shift) then
      Guideline.OnMouseUp(IntX,IntY,Button,Shift);
end;

{******************************************************************************+
  Procedure TXLineStyleDialog.EditHandlerMove(Sender:TObject)
--------------------------------------------------------------------------------
  Called if the user has finished moving an object.
+******************************************************************************}
Procedure TXLineStyleDialog.EditHandlerMove(Sender:TObject);
var OldClipRect  : TRect;
    Cnt          : Integer;
    XMove        : Integer;
    YMove        : Integer;
begin
  if FCurrentItem<>NIL then with FCurrentItem^ do begin
    Scroller.InvalidateRect(ClipRectOfCurrentItem);
    if ItemType=itRectangle then SetRectFromEditHandler
    else begin
      OldClipRect:=ClipRectOfCurrentItem;
      with EditHandler do begin
        XMove:=Round(XPosition-OldClipRect.Left);
        YMove:=Round(YPosition-OldClipRect.Top);
      end;
      if ItemType<itRectangle then for Cnt:=2 to 4 do MovePoint(Points[Cnt],XMove,YMove)
      else for Cnt:=1 to PointCount do MovePoint(Points[Cnt],XMove,YMove);
    end;
    Scroller.InvalidateRect(ClipRectOfCurrentItem);
    UpdateSize;
    UpdateStatus;
  end;
end;

{******************************************************************************+
  Procedure TXLineStyleDialog.EditHandlerResize(Sender:TObject)
--------------------------------------------------------------------------------
  Called if the user has finished resizig an object.
+******************************************************************************}
Procedure TXLineStyleDialog.EditHandlerResize(Sender:TObject);
var OldClipRect  : TRect;
    Cnt          : Integer;
    XScale       : Double;
    YScale       : Double;
    XMove        : Double;
    YMove        : Double;
begin
  if FCurrentItem<>NIL then with FCurrentItem^ do begin
    Scroller.InvalidateRect(ClipRectOfCurrentItem);
    with EditHandler do if ItemType=itRectangle then SetRectFromEditHandler
    else begin
      OldClipRect:=ClipRectOfCurrentItem;
      if Abs(RectWidth(OldClipRect))<1E-6 then XScale:=1
      else XScale:=Width/RectWidth(OldClipRect);
      if Abs(RectHeight(OldCLipRect))<1E-6 then YScale:=1
      else YScale:=Height/RectHeight(OldClipRect);
      XMove:=OldClipRect.Left*XScale-XPosition;
      YMove:=OldClipRect.Top*YScale-YPosition;
      if ItemType<itRectangle then begin
        Points[1].X:=Round(Points[1].X*XScale);
        for Cnt:=2 to 4 do with Points[Cnt] do begin
          X:=Round(X*XScale-XMove);
          Y:=Round(Y*YScale-YMove);
        end;
      end
      else for Cnt:=1 to PointCount do with Points[Cnt] do begin
        X:=Round(X*XScale-XMove);
        Y:=Round(Y*YScale-YMove);
      end;
    end;
    SetEditHandlerSize;
    Scroller.InvalidateRect(ClipRectOfCurrentItem);
    UpdateSize;
    UpdateStatus;
  end;
end;

{******************************************************************************+
  Procedure TXLineStyleDialog.EditHandlerRotate(Sender:TObject)
--------------------------------------------------------------------------------
  Called if the user has finished rotating an object. Rectangles are handled
  by SetRectFromEditHandler, points of polylines are rotated.
+******************************************************************************}
Procedure TXLineStyleDialog.EditHandlerRotate(Sender:TObject);
var Point        : TPoint;
    NewPoint     : TPoint;
    ARect        : TRect;
    CenterX      : Double;
    CenterY      : Double;
    Cnt          : Integer;
    CosRotation  : Double;
    SinRotation  : Double;
begin
  if FCurrentItem<>NIL then with FCurrentItem^ do begin
    Scroller.InvalidateRect(ClipRectOfCurrentItem);
    with EditHandler do begin
         SinCos(Rotation,SinRotation,CosRotation);
         CenterX:=XPosition+Width*CosRotation/2-Height*SinRotation/2;
         CenterY:=YPosition+Width*SinRotation/2+Height*CosRotation/2;
         if ItemType<itRectangle then Cnt:=2
         else Cnt:=1;
         for Cnt:=Cnt to PointCount do begin
             Point:=MovedPoint(Points[Cnt],-Round(CenterX),-Round(CenterY));
             NewPoint.X:=Round(Point.X*CosRotation-Point.Y*SinRotation+CenterX);
             NewPoint.Y:=Round(Point.X*SinRotation+Point.Y*CosRotation+CenterY);
             Points[Cnt]:=NewPoint;
         end;
         if (ItemType<>itRectangle) and (Sender <> nil) then begin
             ARect:=ClipRectOfCurrentItem;
             XPosition:=ARect.Left;
             YPosition:=ARect.Top;
             Width:=RectWidth(ARect);
             Height:=RectHeight(ARect);
             Rotation:=0;
         end;
         EditHandlerResize(Sender);
         Scroller.InvalidateRect(ClipRectOfCurrentItem);
         UpdateSize;
         UpdateStatus;
    end;
  end;
end;

Procedure TXLineStyleDialog.CircleBtnClick(Sender:TObject);
begin
  CurrentItem:=NIL;
  MenuHandler:=RubberEllipse;
end;

Procedure TXLineStyleDialog.RectBtnClick(Sender:TObject);
begin
  CurrentItem:=NIL;
  MenuHandler:=RubberBox;
end;

Procedure TXLineStyleDialog.RubberBoxFinishedInput(Sender:TObject);
var XLineDef   : TXLineDef;
begin
  FillChar(XLineDef,SizeOf(XLineDef),#0);
  with RubberBox,XLineDef do begin
    PointCount:=4;
    Points[1]:=Point(Round(Position.X),Round(Position.Y));
    Points[2]:=Point(Round(Position.X+Width),Round(Position.Y));
    Points[3]:=Point(Round(Position.X+Width),Round(Position.Y+Height));
    Points[4]:=Point(Round(Position.X),Round(Position.Y+Height));
    Cycle:=1000;
    ItemType:=itRectangle;
    ClipEdges:=TRUE;
    LineColor:=LineColorBtn.SelectedColor;
    FillColor:=FillColorBtn.SelectedColor;
    LineWidth:=0;
  end;
  FXLineStyle.Add(XLineDef);
  CurrentItem:=FXLineStyle[FXLineStyle.Count-1];
  Scroller.InvalidateRect(ClipRectOfCurrentItem);
  UpdateSize;
end;

Procedure TXLineStyleDialog.EditBtnClick(Sender:TObject);
begin
  MenuHandler:=NIL;
end;

Procedure TXLineStyleDialog.UpdateEnabled;
var AEnable      : Boolean;
    CurrentIndex : Integer;
begin
  AEnable:=FCurrentItem<>NIL;
  EditPropertiesAction.Enabled:=AEnable;
  CurrentIndex:=FXLineStyle.IndexOf(FCurrentItem);
  SendBackwardAction.Enabled:=AEnable and (CurrentIndex>0);
  SendToBackAction.Enabled:=AEnable and (CurrentIndex>0);
  BringForwardAction.Enabled:=AEnable and (CurrentIndex<FXLineStyle.Count-1);
  BringToFrontAction.Enabled:=AEnable and (CurrentIndex<FXLineStyle.Count-1);
  EditCutAction.Enabled:=AEnable;
  EditCopyAction.Enabled:=AEnable;
  EditPasteAction.Enabled:=FClipboardSet;
  EditDeleteAction.Enabled:=AEnable;
  SetControlEnabled(LineWidthEdit,AEnable);
  
{++ New XLine BUG#251}
// It was commented by me. Ivanoff.  SetControlEnabled(CycleEdit,AEnable);
// It was commented by me. Ivanoff.  CutEdgesCheck.Enabled:=AEnable;
  CycleEdit.Enabled := FXLineStyle.Count > 0;
  CutEdgesCheck.Enabled := FXLineStyle.Count > 0;
  TurnItCheckBox.Enabled := FXLineStyle.Count > 0;
{-- New XLine BUG#251}

  SetControlGroupEnabled(Group1,GeneralizeCheck.Checked);
  SetControlEnabled(WidthEdit,FLineDropDown.Style=0);
end;

Procedure TXLineStyleDialog.ColorBtnSelectColor(Sender:TObject);
begin
  if FCurrentItem<>NIL then begin
    if Sender=LineColorBtn then FCurrentItem.LineColor:=LineColorBtn.SelectedColor
    else FCurrentItem.FillColor:=FillColorBtn.SelectedColor;
    EditWindow.Invalidate;
  end;  
end;

Procedure TXLineStyleDialog.FormHide(Sender:TObject);
var
   NewStyle       : TLineStyle;
{++ New XLine BUG#251}
   ibClipMode: Byte;
{-- New XLine BUG#251}
begin
  CurrentItem:=NIL;
  if ModalResult=mrOK then begin
    with FXLineStyle do begin
      Generalize:=GeneralizeCheck.Checked;
      GeneralizeWhenPrinting:=not GeneralizePrintCheck.Checked;
      GeneralizeScale:=ScaleVal.AsFloat;
      NewStyle:=GeneralizeStyle;
      NewStyle.Color:=ColorSelectBtn.SelectedColor;
      NewStyle.Style:=FLineDropDown.Style;
      GeneralizeStyle:=NewStyle;
      Name:=NameEdit.Text;

{++ New XLine BUG#251}
      // Save parameters of the style segment. 
      If FXLineStyle.Count> 0 Then Begin
         FXLineStyle.Items[0].Cycle := SELF.iCycle;
         ibClipMode := 0;
         If SELF.bClipIt Then ibClipMode := ibClipMode OR ci_XLine_ClipIt;
         If SELF.bTurnIt Then ibClipMode := ibClipMode OR ci_XLine_TurnIt;
         Byte(FXLineStyle.Items[0].ClipEdges) := ibClipMode;
         End;
{-- New XLine BUG#251}

    end;
    if FOrgLineStyle<>NIL then FOrgLineStyle.Assign(FXLineStyle);
    if FOrgPalette<>NIL then FOrgPalette.Assign(FPalette);
  end;
end;

Procedure TXLineStyleDialog.PolygonBtnClick(Sender:TObject);
begin
  CurrentItem:=NIL;
  MenuHandler:=RubberPolygon;
  RubberPolygon.DrawClosed:=TRUE;
end;

Procedure TXLineStyleDialog.MoveItem(FromIndex,ToIndex:Integer);
begin
  FXLineStyle.Move(FromIndex,ToIndex);
  FCurrentItem:=FXLineStyle[ToIndex];
  EditWindow.Invalidate;
  UpdateEnabled;
end;

Procedure TXLineStyleDialog.ToBackMenuClick(Sender:TObject);
var CurrentIndex : Integer;
begin
  CurrentIndex:=FXLineStyle.IndexOf(FCurrentItem);
  if CurrentIndex>0 then MoveItem(CurrentIndex,0);
end;

Procedure TXLineStyleDialog.OneToBackMenuClick(Sender:TObject);
var CurrentIndex : Integer;
begin
  CurrentIndex:=FXLineStyle.IndexOf(FCurrentItem);
  if CurrentIndex>0 then MoveItem(CurrentIndex,CurrentIndex-1);
end;

Procedure TXLineStyleDialog.ToFrontMenuClick(Sender:TObject);
var CurrentIndex : Integer;
begin
  CurrentIndex:=FXLineStyle.IndexOf(FCurrentItem);
  if CurrentIndex<FXLineStyle.Count-1 then MoveItem(CurrentIndex,FXLineStyle.Count-1);
end;

Procedure TXLineStyleDialog.OneToFrontMenuClick(Sender:TObject);
var CurrentIndex : Integer;
begin
  CurrentIndex:=FXLineStyle.IndexOf(FCurrentItem);
  if CurrentIndex<FXLineStyle.Count-1 then MoveItem(CurrentIndex,CurrentIndex+1);
end;

Procedure TXLineStyleDialog.DeleteMenuClick(Sender:TObject);
var CurrentIndex : Integer;
begin
  CurrentIndex:=FXLineStyle.IndexOf(FCurrentItem);
  Scroller.InvalidateRect(ClipRectOfCurrentItem);
  CurrentItem:=NIL;
  if CurrentIndex>=0 then begin
    FXLineStyle.Delete(CurrentIndex);
    UpdateSize;
  end;
end;

Procedure TXLineStyleDialog.ZoomAllBtnClick(Sender:TObject);
begin
  Scroller.CurrentView:=Scroller.Size;
end;

Procedure TXLineStyleDialog.ZoomBtnClick(Sender:TObject);
begin
  CurrentItem:=NIL;
  MenuHandler:=ZoomBox;
end;

Procedure TXLineStyleDialog.ZoomBoxFinishedInput(Sender:TObject);
var Factor         : Double;
begin
  if ZoomBox.MinSizeReached then Scroller.CurrentView:=RotRect(ZoomBox.Box,0)
  else begin
    Factor:=2;
    if GetKeyState(vk_Shift)<0 then Factor:=Factor*2
    else if GetKeyState(vk_Control)<0 then Factor:=Factor*4;
    Scroller.ZoomByFactorWithCenter(ZoomBox.Position,Factor);
  end;
end;

Procedure TXLineStyleDialog.LineWidthChanged(Sender:TObject);
begin
  if FCurrentItem<>NIL then begin
    FCurrentItem.LineWidth:=Round(LineWidthVal[muMillimeters]*100);
    EditWindow.Invalidate;
  end;
end;  

Procedure TXLineStyleDialog.SetRectFromEditHandler;
var XPos         : Double;
    YPos         : Double;
begin
  with FCurrentItem^,EditHandler do begin
    XPos:=XPosition;
    YPos:=YPosition;
    Points[1]:=Point(Round(XPos),Round(YPos));
    XPos:=XPos+Width*Cos(Rotation);
    YPos:=YPos+Width*Sin(Rotation);
    Points[2]:=Point(Round(XPos),Round(YPos));
    XPos:=XPos-Height*Sin(Rotation);
    YPos:=YPos+Height*Cos(Rotation);
    Points[3]:=Point(Round(XPos),Round(YPos));
    XPos:=XPos-Width*Cos(Rotation);
    YPos:=YPos-Width*Sin(Rotation);
    Points[4]:=Point(Round(XPos),Round(YPos));
  end;
end;

Procedure TXLineStyleDialog.RubberPolygonFinishedInput(Sender:TObject);
var XLineDef   : TXLineDef;
   Cnt          : Integer;
begin
  FillChar(XLineDef,SizeOf(XLineDef),#0);
  with XLineDef do begin
    PointCount:=RubberPolygon.PointCount;
    for Cnt:=0 to PointCount-1 do Points[Cnt+1]:=Point(Round(RubberPolygon[Cnt].X),
        Round(RubberPolygon[Cnt].Y));
    if LineBtn.Down then ItemType:=itPolyline
    else ItemType:=itPolygon;    
    Cycle:=1000;
    ClipEdges:=TRUE;
    LineColor:=LineColorBtn.SelectedColor;
    FillColor:=FillColorBtn.SelectedColor;
    LineWidth:=0;
  end;
  FXLineStyle.Add(XLineDef);             
  CurrentItem:=FXLineStyle[FXLineStyle.Count-1];
  Scroller.InvalidateRect(ClipRectOfCurrentItem);
  UpdateSize;
end;

Procedure TXLineStyleDialog.ZoomInMenuClick(Sender:TObject);
begin
  Scroller.ZoomByFactor(2.0);
end;

Procedure TXLineStyleDialog.ZoomOutMenuClick(Sender:TObject);
begin
  Scroller.ZoomByFactor(0.5);
end;

Procedure TXLineStyleDialog.UpdateStatus;
var Text         : String;
    XScale       : Double;
    YScale       : Double;
begin
  Text:='';
  if FCurrentItem<>NIL then with FCurrentItem^ do begin
    with EditHandler do if Rotating then Text:=Format(MlgSection[6],[CurrentRotation*180/Pi])
    else if Moving then Text:=Format(MlgSection[7],[(CurrentX-XPosition)*FUnitsValue,
        (CurrentY-YPosition)*FUnitsValue,CurrentX*FUnitsValue,CurrentY*FUnitsValue])
    else if Sizing then begin
      if Abs(Width)<1E-6 then XScale:=0 else XScale:=100.0*CurrentWidth/Width;
      if Abs(Height)<1E-6 then YScale:=0 else YScale:=100.0*100.0*CurrentHeight/Height;
      Text:=Format(MlgSection[8],[XScale,YScale,CurrentWidth*FUnitsValue,
          CurrentHeight*FUnitsValue]);
    end
    else if ItemType=itRectangle then Text:=Format(MlgSection[4],[CurrentX*FUnitsValue,
        CurrentY*FUnitsValue,CurrentWidth*FUnitsValue,
        CurrentHeight*FUnitsValue,CurrentRotation*180/Pi])
    else if ItemType<itRectangle then Text:=Format(MlgSection[14],[CurrentX*FUnitsValue,
        CurrentY*FUnitsValue,CurrentWidth*FUnitsValue/2.0])
    else Text:=Format(MlgSection[5],[CurrentX*FUnitsValue,
        CurrentY*FUnitsValue,CurrentWidth*FUnitsValue,CurrentHeight*FUnitsValue]);
  end;
  StatusBar[1]:=Text;
end;  

Procedure TXLineStyleDialog.EditHandlerChange(Sender:TObject);
begin
  UpdateStatus;                 
end;

Procedure TXLineStyleDialog.RubberEllipseFinishedInput(Sender:TObject);
var XLineDef   : TXLineDef;
begin
  FillChar(XLineDef,SizeOf(XLineDef),#0);
  with XLineDef do with RubberEllipse do begin
    PointCount:=4;
    Points[1].X:=Round(PrimaryAxis);
    Points[2]:=Point(Round(Center.X),Round(Center.Y));
    ItemType:=itCircle;
    Cycle:=1000;
    ClipEdges:=TRUE;
    LineColor:=LineColorBtn.SelectedColor;
    FillColor:=FillColorBtn.SelectedColor;
    FillStyle:=0;
    LineWidth:=0;
  end;
  FXLineStyle.Add(XLineDef);
  CurrentItem:=FXLineStyle[FXLineStyle.Count-1];
  Scroller.InvalidateRect(ClipRectOfCurrentItem);
  UpdateSize;
end;

Procedure TXLineStyleDialog.SetUnits(AUnits:TMeasureUnits);
begin
  if AUnits<>FUnits then begin
    FUnits:=AUnits;
    FUnitsValue:=1/UnitsToBaseUnits(FUnits)/100.0;
  end;
end;

Procedure TXLineStyleDialog.UnitsComboClick(Sender:TObject);
begin
  Units:=ShortNameToUnits(UnitsCombo.Text);
  CycleVal.Units:=Units;
  LineWidthVal.Units:=Units;
  UpdateStatus;
end;
                                                  
Procedure TXLineStyleDialog.DoSnapFinished;
begin
end;

Procedure TXLineStyleDialog.EditCutActionExecute(Sender:TObject);
var CurrentIndex : Integer;
begin
  if FCurrentItem<>NIL then begin
    Scroller.InvalidateRect(ClipRectOfCurrentItem);
    Move(FCurrentItem^,FClipboard,SizeOf(FClipboard));
    CurrentIndex:=FXLineStyle.IndexOf(FCurrentItem);
    CurrentItem:=NIL;
    FXLineStyle.Delete(CurrentIndex);
    FClipboardSet:=TRUE;
    UpdateSize;
    UpdateEnabled;
  end;
end;

Procedure TXLineStyleDialog.EditCopyActionExecute(Sender:TObject);
begin
  if FCurrentItem<>NIL then begin
    Move(FCurrentItem^,FClipboard,SizeOf(FClipboard));
    FClipboardSet:=TRUE;
    UpdateEnabled;
  end;
end;

Procedure TXLineStyleDialog.EditPasteActionExecute(Sender:TObject);
begin
  if FClipboardSet then begin
    CurrentItem:=FXLineStyle.Add(FClipboard);
    Scroller.InvalidateRect(ClipRectOfCurrentItem);
    UpdateSize;
    UpdateEnabled;
  end;
end;

Procedure TXLineStyleDialog.LineBtnClick(Sender:TObject);
begin
  CurrentItem:=NIL;
  MenuHandler:=RubberPolygon;
  RubberPolygon.DrawClosed:=FALSE;
end;

Procedure TXLineStyleDialog.CircleModeBtnClick(Sender:TObject);
begin
  if Sender=Circle2PointsCenterBtn then RubberEllipse.InputType:=retCircle2PointsCenter
  else if Sender=CircleCenterBtn then RubberEllipse.InputType:=retCircleCenterRadius
  else if Sender=Circle2PointsBtn then RubberEllipse.InputType:=retCircle2Points
  else if Sender=Circle3PointsBtn then RubberEllipse.InputType:=retCircle3Points
  else if Sender=CircleRectBtn then RubberEllipse.InputType:=retCircleInRectangle
  else RubberEllipse.InputType:=retCircleCenterRadius;
end;

Procedure TXLineStyleDialog.ArcModeBtnClick(Sender:TObject);
begin
  if Sender=Arc2PointsCenterBtn then RubberArc.InputType:=ratArc2PointsCenter
  else if Sender=ArcCenterBtn then RubberArc.InputType:=ratArcCenter
  else if Sender=Arc3PointsBtn then RubberArc.InputType:=ratArc3Points
  else if Sender=ArcRectBtn then RubberArc.InputType:=ratArcInRectangle
  else RubberArc.InputType:=ratArcCenter;
end;

Procedure TXLineStyleDialog.ArcBtnClick(Sender:TObject);
begin
  CurrentItem:=NIL;
  RubberArc.ChordMode:=FALSE;
  MenuHandler:=RubberArc;
end;

Procedure TXLineStyleDialog.ChordBtnClick(Sender:TObject);
begin
  CurrentItem:=NIL;
  RubberArc.ChordMode:=TRUE;
  MenuHandler:=RubberArc;
end;

Procedure TXLineStyleDialog.RubberArcFinishedInput(Sender:TObject);
var XLineDef   : TXLineDef;
begin
  FillChar(XLineDef,SizeOf(XLineDef),#0);
  with XLineDef do with RubberArc do begin
    PointCount:=4;
    Points[1].X:=Round(PrimaryAxis);
    Points[2]:=Point(Round(Center.X),Round(Center.Y));
    Points[3].X:=Points[2].X+Round(1000*Cos(BeginAngle));
    Points[3].Y:=Points[2].Y+Round(1000*Sin(BeginAngle));
    Points[4].X:=Points[2].X+Round(1000*Cos(EndAngle));
    Points[4].Y:=Points[2].Y+Round(1000*Sin(EndAngle));
    Cycle:=1000;
    ClipEdges:=TRUE;
    LineColor:=LineColorBtn.SelectedColor;
    if RubberArc.ChordMode then begin
      FillColor:=FillColorBtn.SelectedColor;
      ItemType:=itChord;
    end
    else begin
      FillColor:=clNone;
      ItemType:=itArc;
    end;
    FillStyle:=0;
    LineWidth:=0;
  end;
  FXLineStyle.Add(XLineDef);
  CurrentItem:=FXLineStyle[FXLineStyle.Count-1];
  Scroller.InvalidateRect(ClipRectOfCurrentItem);
  UpdateSize;
end;

Procedure TXLineStyleDialog.FormCloseQuery(Sender:TObject;var CanClose:Boolean);
var Item           : TXLineStyle;
begin
  CanClose:=FALSE;
  if ModalResult<>mrOK then CanClose:=TRUE
  else if FXLineStyle.Count=0 then MessageDialog(MlgSection[24],mtError,[mbOK],0)
  else if CheckValidators(Self) then begin
    Item:=FXLineStyles.NamedStyles[NameVal.AsText];
    if (Item<>NIL) and (Item.Number<>FXLineStyle.Number) then begin
      MessageDialog(Format(MlgSection[10],[NameVal.AsText]),mtError,[mbOK],0);
      CanClose:=FALSE;
    end
    else CanClose:=TRUE;
  end;
end;

Procedure TXLineStyleDialog.UpdateSize;
var ClipRect       : TRect;
begin
  ClipRect:=FXLineStyle.ClipRect;
  UnionRect(ClipRect,ClipRect,Rect(-100,-200,1100,200));
  with ClipRect do Scroller.Size:=RotRect(GrRect(Left,Top,Right,Bottom),0);
end;

Procedure TXLineStyleDialog.GuideLineMove(Sender:TObject);
begin
{++ New XLine BUG#251}
{It was commented by me. Ivanoff.
    if FCurrentItem<>NIL then begin
       FCurrentItem.Cycle:=Round(Guideline.Position.X);
       CycleVal[muMillimeters]:=FCurrentItem.Cycle/100.0;
    end;}
    // Set cycle length and show this new length in the editor window.
    SELF.iCycle := Round(GuideLine.Position.X);
    CycleVal[muMillimeters] := SELF.iCycle/100.0;
{-- New XLine BUG#251}
end;

Procedure TXLineStyleDialog.EditPropertiesActionExecute(Sender:TObject);
var Dialog         : TRectangleDialog;
begin
  if FCurrentItem<>NIL then with FCurrentItem^ do begin
    Dialog:=TRectangleDialog.Create(Self);
    try
      Dialog.UnitsToMM:=1/100;
      Dialog.KeepAspect:=ehoKeepAspect in EditHandler.Options;
      Dialog.AllowRotation:=ehoRotate in EditHandler.Options;
      Dialog.Handler:=EditHandler;
      Dialog.ShowModal;
    finally
      Dialog.Free;
    end;
  end;
end;

Procedure TXLineStyleDialog.GeneralizeCheckClick(Sender:TObject);
begin
  UpdateEnabled;
end;

Procedure TXLineStyleDialog.DoStyleSelect(Sender: TObject);
begin
  LineStyleDropDown.Invalidate;
  UpdateEnabled;
end;

Procedure TXLineStyleDialog.LineStyleDropDownPaintBtn(Sender: TObject;
  ARect: TRect);
var Y              : Integer;
    AText          : String;
begin
  with LineStyleDropDown.Canvas do begin
    if FLineDropDown.Style<0 then begin
      AText:=MlgStringList['SelectionNames',61];
      TextRect(ARect,ARect.Left,CenterHeightInRect(TextHeight(AText),ARect),AText);
    end
    else begin
      Y:=CenterHeightInRect(0,ARect);
      Pen.Style:=TPenStyle(FLineDropDown.Style);
      if GeneralizeCheck.Checked then Pen.Color:=clBlack
      else Pen.Color:=clGray;
      Pen.Width:=0;
      with ARect do Polyline([Point(Left+1,Y-1),Point(Right-1,Y-1)]);
      with ARect do Polyline([Point(Left+1,Y),Point(Right-1,Y)]);
    end;  
  end;                              
end;

Procedure TXLineStyleDialog.WidthEditChange(Sender: TObject);
var AEnable      : Boolean;
begin
  if WidthEdit.Enabled then begin
    AEnable:=not (WidthVal.Units in [muDrawingUnits,muNone]);
    if not AEnable then ScaleIndependCheck.Checked:=FALSE;
    ScaleIndependCheck.Enabled:=GeneralizeCheck.Checked and AEnable;
  end;  
end;

{++ New XLine BUG#251}
procedure TXLineStyleDialog.CycleEditChange(Sender: TObject);
begin
     // Set new cucle length and move the guide line to a new position.
     SELF.iCycle := Round(CycleVal[muMillimeters] * 100.0);
     GuideLine.Position := GRPoint(SELF.iCycle, 0);
end;

procedure TXLineStyleDialog.CutEdgesCheckClick(Sender: TObject);
begin
     // Set clipping mode.
     SELF.bClipIt := CutEdgesCheck.Checked;
     If bClipIt Then
        TurnItCheckBox.Checked := FALSE;
end;

procedure TXLineStyleDialog.TurnItCheckBoxClick(Sender: TObject);
begin
     // Set turning mode.
     SELF.bTurnIt := TurnItCheckBox.Checked;
     If SELF.bTurnIt Then
        CutEdgesCheck.Checked := FALSE;
end;
{-- New XLine BUG#251}

end.
