unit HugeBMPStream;

interface
uses
  Windows, Classes, Sysutils, Math;

type
  TColorBits = (cbNone, cb1, cb4, cb8, cb24); // Allowed bits number for the BMP
  {
    If progress procedure will return True then any consequent write operations
     will be disabled
  }
  THugeProgProc = function(WroteBytesNum: DWORD; UserData: DWORD): Boolean;

  THugeBMPStream = class(TFileStream)
  private
    fBitMapFileHeader: TBitMapFileHeader; // To write it at beginning of the file
    fBitMapInfo: PBitMapInfo;
    fMaxWrotePos: DWORD;                // Max wrote position for the image
    fFileSize: DWORD;                   // BMP on disk real size
    fProgressProc: THugeProgProc;       // To call progress callback
    fUserData: DWORD;                   // The user parameter for callback

    fHeaded: Boolean;                   // if BMP has header
    fEnabled: Boolean;                  // Define can you use Read/Seek/Write
    fInCheckEOF: Boolean;               // Special flag to avoid recursion

    function GetCompleted: Boolean;
    function GetHeight: Integer;
    function GetWidth: Integer;
    //    function GetBitsOffset: Pointer;
    function GetColorBits: TColorBits;
    //    function GetPosition: Longint;
        //    function GetSize: Longint;
    procedure CheckEOFMoved;
    function GetBitCount: Integer;
    function RowOffsetInFile(Row: Integer): DWORD;
    function PixOffsetInRow(PixId: Integer): DWORD;
//    function GetCopyMode: Integer;
    function GetColorNum: Integer;
    function GetPosition: DWORD;
    procedure SetPosition(Pos: DWORD);
    function DoProgress(WriteSize: DWORD):Boolean;
  protected
    BytesInRow: Integer;
    property fBitCount: Integer read GetBitCount;

  public

    destructor Destroy; override;
    constructor Create(const FileName: string; PInfo: PBitMapInfo);
    property Width: Integer read GetWidth;
    property Height: Integer read GetHeight;
    property ColorBits: TColorBits read GetColorBits;
    property BitCount: Integer read GetBitCount;
    property SizeIsCompleted: Boolean read GetCompleted;
    property BitMapInfoPtr: PBitMapInfo read fBitMapInfo;
    property BitsOffset: Cardinal read fBitMapFileHeader.bfOffBits {GetBitsOffset};
//    property CopyMode: Integer read GetCopyMode;
    property ColorNum: Integer read GetColorNum;
    property Position: DWORD read GetPosition write SetPosition;
    property FIleSize: DWORD read fFileSize;
    procedure WriteBuffer(const Buffer; Count: Longint);
    function SetProgressProc(ProgProc: THugeProgProc; dwUserData: DWORD): THugeProgProc;
    function GetProgressProc: THugeProgProc;
    function Read(var Buffer; Count: Longint): Longint; override;
    function Write(const Buffer; Count: Longint): Longint; override;
    { to allow rectangular block write directly}
    function WriteBlock(var BlockRect: TRect; PInfo: PBitMapInfo): Integer;
    { to check if this bitmap is the writable to current one }
    function IsWritableBitMap(PInfo: PBitMapInfo): Boolean;
    { to check if this rectangel is internal to bitmap }
    function IsWritableRect(var BlockRect: TRect): Boolean;
    { to ensure correct size of result image }
    function DoComplete: Boolean;
  end;

implementation
uses
  TrLib32;

{ THugeBMPStream }

procedure THugeBMPStream.CheckEOFMoved;
var
  MemBuf            : PByte;
  Size, BufSize, OldPos: DWORD;
  Process, Priority : DWORD;
begin
  if fInCheckEOF then                   // Avoid recursion
    Exit;
  fInCheckEOF := True;
  try
    if DWORD(Position) <= Succ(fMaxWrotePos) then
      Exit;
    Process := GetCurrentProcess;
    Priority := GetPriorityClass(Process);
    SetPriorityClass(Process, IDLE_PRIORITY_CLASS);
    try
      Size := DWORD(Position) - fMaxWrotePos;
      // Initialize the buffer to write all the pixels to WHITE
      BufSize := Min(Size, (BytesInRow * 10 + 511) and $FFFFFE00); // Get good block size
      GetMem(MemBuf, BufSize);
      try
        FillChar(MemBuf^, BufSize, $FF);
        // Fill the whole image
        OldPos := Position;
        Position := fMaxWrotePos;
        try
          while Size <> 0 do
          begin
            BufSize := Min(BufSize, Size);
            WriteBuffer(MemBuf^, BufSize);
            Dec(Size, BufSize);
          end;
        finally
          Position := OldPos;
          fMaxWrotePos := Pred(OldPos);
        end;
      finally
        FreeMem(MemBuf);
      end;
    finally
      SetPriorityClass(Process, Priority);
    end;
  finally
    fInCheckEOF := False;
  end;
end;

constructor THugeBMPStream.Create(const FileName: string; PInfo: PBitMapInfo);
var
  Res               : Int64;
  InfoSize          : Integer;
begin
  DeleteFile(FileName);
  inherited Create(FileName, fmCreate);

  case PInfo.bmiHeader.biBitCount of
    1, 4, 8, 24: ;
  else
    raise EInvalidImage('Color num=' + IntToStr(PInfo.bmiHeader.biBitCount) +
      ' while only 1,4,8,24 are still supported');
  end;
  InfoSize := BitMapInfoSize(PInfo);
  Res := DWORD(PInfo.bmiHeader.biHeight) * BytesPerRow(PInfo.bmiHeader.biWidth,
    PInfo.bmiHeader.biBitCount);
  if Res > High(DWORD) - ((DWORD(InfoSize) + SizeOf(TBitMapFileHeader))) then
    raise EInvalidImage.Create('Size of image exeeds 4 gigabytes. Mailto:sygsky@progis.ru or his descendant[s] to update over there :o)');

  GetMem(fBitMapInfo, InfoSize);
  CopyMemory(fBitMapInfo, PInfo, InfoSize);

  // Write initial header

  fBitMapFileHeader.bfType := WORD($4D42);
  fBitMapFileHeader.bfOffBits := InfoSize + SizeOf(TBitMapFileHeader);
  fEnabled := True;
  try
    WriteBuffer(fBitMapFileHeader, SizeOf(TBitMapFileHeader));
    // Write bitmap info block

    WriteBuffer(fBitMapInfo^, InfoSize);
    // Well, now we are at the beginning of the image bits. So all follow
    // operations need to be done on WHITE background!

    fHeaded := True;
    fMaxWrotePos := Position;
  finally
    fEnabled := False;
  end;
  BytesInRow := BytesPerRow(Width, fBitCount);
  fFileSize := fMaxWrotePos + DWORD(Height * BytesInRow);
end;

destructor THugeBMPStream.Destroy;
var
  Size              : DWORD;
begin
  if Assigned(fBitMapInfo) then
    FreeMem(fBitMapInfo);
  fEnabled := True;
  try
    if not GetCompleted then
    begin
      Position := Succ(fFileSize);      // Point to the wanted EOF
    end;
    if GetCompleted then                // It is time to write file size into structure
    begin
      Position := Cardinal(@fBitMapFileHeader.bfSize) - Cardinal(@fBitMapFileHeader);
      Size := fFileSize;
      WriteBuffer(Size, SizeOf(DWORD));
    end;
  finally
    fEnabled := False;
  end;
  inherited;
end;

function THugeBMPStream.GetBitCount: Integer;
begin
  if not fHeaded then
  begin
    Result := 0;
    Exit;
  end;
  Result := fBitMapInfo.bmiHeader.biBitCount;
end;

{
function THugeBMPStream.GetBitsOffset: Pointer;
begin
  if not fHeaded then
  begin
    Result := nil;
    Exit;
  end;
  Result := Pointer(SizeOf(TBitMapFileHeader) + BitMapInfoSize(fBitMapInfo));
end;
}

function THugeBMPStream.GetColorBits: TColorBits;
begin
  Result := cbNone;
  if not fHeaded then
    Exit;
  case fBitMapInfo.bmiHeader.biBitCount of
    1: Result := cb1;
    4: Result := cb4;
    8: Result := cb8;
    24: Result := cb24;
  else
  end;
end;

function THugeBMPStream.GetCompleted: Boolean;
begin
  Result := fHeaded and (Succ(fMaxWrotePos) >= fFileSize);
end;

function THugeBMPStream.GetHeight: Integer;
begin
  if not fHeaded then
  begin
    Result := 0;
    Exit;
  end;
  Result := fBitMapInfo.bmiHeader.biHeight;
end;

{
function THugeBMPStream.GetPosition: Longint;
begin
  if not fEnabled then
  begin
    Result := -1;
    Exit;
  end;
  inherited;
end;
}

function THugeBMPStream.GetWidth: Integer;
begin
  if not fHeaded then
  begin
    Result := 0;
    Exit;
  end;
  Result := fBitMapInfo.bmiHeader.biWidth;
end;

function THugeBMPStream.IsWritableBitMap(PInfo: PBitMapInfo): Boolean;
begin
  if not fHeaded then
  begin
    Result := False;
    Exit;
  end;
  Result := fHeaded and Assigned(PInfo) and
    (PInfo.bmiHeader.biBitCount = fBitCount);
end;

function THugeBMPStream.IsWritableRect(var BlockRect: TRect): Boolean;
begin
  Result := RectInImage(fBitMapInfo, @BlockRect);
end;

function THugeBMPStream.Read(var Buffer; Count: Longint): Longint;
begin
  Result := 0;
  if not fEnabled then
    Exit;
  Result := inherited Read(Buffer, Count);
end;

function THugeBMPStream.RowOffsetInFile(Row: Integer): DWORD;
begin
  Result := 0;
  if not fHeaded then
    Exit;
  if Row > (Height - 1) then
    Exit;
  Result := DWORD(BitsOffset) + DWORD(BytesInRow * Row);
end;

function THugeBMPStream.PixOffsetInRow(PixId: Integer): DWORD;
begin
  Result := 0;
  if not fHeaded then
    Exit;
  Result := (PixId * fBitMapInfo.bmiHeader.biBitCount) div 8;
end;

{
function THugeBMPStream.Seek(Offset: Longint; Origin: Word): Longint;
begin
  if not fEnabled then
  begin
    Result := 0;
    Exit;
  end;
  Result := inherited Seek(Offset, Origin);
end;
}

procedure THugeBMPStream.SetPosition(Pos: DWORD);
begin
  if not fEnabled then
    Exit;
  Seek(Pos, 0);
  CheckEOFMoved;
end;

{
function THugeBMPStream.GetPosition: Longint;
begin
  inherited;
end;
}

function THugeBMPStream.Write(const Buffer; Count: Longint): Longint;
begin
  if not fEnabled then
  begin
    Result := 0;
    Exit;
  end;
  Result := inherited Write(Buffer, Count);
end;

procedure ShiftBitsRight(P: Pointer; L: Cardinal; Shift: Byte);
{
  EAX = P
  EDX = L
  ECX = Shift
}
asm
        OR      EDX, EDX
        JZ      @@Exit     // Zero count

        PUSH    EBX
        PUSH    EDI
        MOV     EDI, EAX   // Load address buffer
        XOR     BX, BX     // Clear work register[s]
        MOV     AL, BL     //...
@@Loop:
        MOV     BH, BYTE PTR [EDI]  // Load next byte to BH
        XOR     BL, BL              // Clear shift destination REG
        SHR     BX, CL              // Shift bits to AL
        OR      AL, BH              // Set bits stored from previous step
        STOSB                       // Store changed byte and bump to next
        MOV     AL, BL              // Store shifted bits for next step
        DEC     EDX
        JNZ     @@Loop
        STOSB
@@Pop:
        POP     ESI
        POP     EBX
@@Exit:
end;

// This is a main function of the class.
// It writes wanted image' rectangle into BMP on disk

function THugeBMPStream.WriteBlock(var BlockRect: TRect; PInfo: PBitMapInfo): Integer;
var
  Res, FirstRow, LastRow: Integer;
  StartPos, OldPos  : DWORD;
  BitPrefix, BitTrail: DWORD;
  W1, BitCnt, I     : Integer;
  MemBuf            : PByteArray;
  NewRowOffset      : PByte;
  PreMask, PostMask, DestByte: Byte;
  NewRowSize, NewRowStride: DWORD;
  Process, Priority : DWORD;
begin
  Result := 0;
  if not (IsWritableBitMap(PInfo) and IsWritableRect(BlockRect)) then
    Exit;
  if not ((PInfo.bmiHeader.biWidth = PRectWidth(@BlockRect)) and
    (PInfo.bmiHeader.biHeight = PRectHeight(@BlockRect))) then
    Exit;

  Process := GetCurrentProcess;
  Priority := GetPriorityClass(Process);
  SetPriorityClass(Process, IDLE_PRIORITY_CLASS);
  try
    // Start position in result BMP
    W1 := PInfo.bmiHeader.biWidth;

    FirstRow := Pred(Height - BlockRect.Bottom); // Khe-khe-khe
    LastRow := Pred(Height - BlockRect.Top); //  oh, that stupid De-Bill Gates

    fEnabled := True;
    try
      // First check the possibility to write after current EOF, in that
      // case we will need to initialize new allocated part of image to WHITES
      if not SizeIsCompleted then       // it is possible to alloc new blocks
        Position := RowOffsetInFile(LastRow) + DWORD(BytesInRow); // so do it with pre-init to WHITE

      // Start in result BMP
      StartPos := RowOffsetInFile(FirstRow) + PixOffsetInRow(BlockRect.Left);

      BitCnt := fBitMapInfo.bmiHeader.biBitCount; // Common for both BMP

      //    H1 := PInfo.bmiHeader.biHeight;
      NewRowStride := BytesPerRow(W1, BitCnt);
      NewRowOffset := Pointer(Cardinal(BitMapInfoSize(PInfo)) + Cardinal(PInfo));
      NewRowSize := (W1 * fBitCount + 7) div 8; // In bytes
      GetMem(MemBuf, NewRowSize + 10);
      try
        case BitCnt of
          1:
            begin
              BitPrefix := BlockRect.Left mod 8;
              BitTrail := Succ(BlockRect.Right) mod 8;
            end;
          4:
            begin
              BitPrefix := (BlockRect.Left mod 2) * 4; // How much to skip in first result byte
              BitTrail := (Succ(BlockRect.Right) mod 2) * 4; // How much to fill in last result byte
            end;
        else
          begin
            BitPrefix := 0;
            BitTrail := 0;
          end;
        end;
        PreMask := 255 shl (8 - BitPrefix);
        PostMask := $FF shr BitTrail;
        for I := BlockRect.Top to BlockRect.Bottom do
        begin
          Position := StartPos;
          case BitCnt of
            1, 4:
              begin
                CopyMemory(MemBuf, Pointer(NewRowOffset), NewRowSize); // Put to a temp buffer
                if BitPrefix > 0 then
                begin
                  ShiftBitsRight(MemBuf, NewRowSize, BitPrefix);
                  OldPos := Position;
                  {Res := } Read(DestByte, 1);
                  DestByte := DestByte and PreMask;
                  MemBuf[0] := MemBuf[0] or DestByte;
                  Position := StartPos + Pred(NewRowSize); // Point to last changing byte
                  {Res := } Read(DestByte, 1);
                  DestByte := DestByte and PostMask;
                  MemBuf[Pred(NewRowSize)] := MemBuf[Pred(newRowSize)] or DestByte;
                  Position := OldPos;
                end;
              end;
            8, 24:                      // Simply copy row by row up to the end of new image rect
              begin
              end;
          end;
          Res := NewRowSize;
          try
            WriteBuffer(NewRowOffset^, NewRowSize);
          except
            Res := 0;
          end;
          if Res = 0 then               // Some error was detected by exception
            Exit;
          Inc(Result, NewRowSize);      // Bump write counter
          Inc(Cardinal(NewRowOffset), NewRowStride); // Bump to the next new start pix offset
          Inc(StartPos, BytesInRow);    // Bump to the new result start pix offset
        end;
      finally
        FreeMem(MemBuf);
      end;
    finally
      fEnabled := False;
    end;
  finally
    SetPriorityClass(Process, Priority); // return to the priority
  end;
end;
{
function THugeBMPStream.GetCopyMode: Integer;
begin
  Result := COPY_DEL;
  if not fHeaded then
    Exit;
  case fBitCount of
    1: Result := COPY_DEL;
    4: Result := COPY_ANTIALIAS;
    8: Result := COPY_ANTIALIAS256;
    24: Result := COPY_INTERPOLATE;
  end;
end;
}
function THugeBMPStream.GetColorNum: Integer;
begin
  Result := 0;
  if not fHeaded then
    Exit;
  Result := 1 shl fBitCount;
end;

function THugeBMPStream.DoComplete: Boolean;
begin
  fEnabled := True;
  try
    if not SizeIsCompleted then
      Position := fFileSize;
    Result := True;
  finally
    FEnabled := False;
  end;
end;

function THugeBMPStream.GetPosition: DWORD;
begin
  Result := Seek(0, 1);
end;

function THugeBMPStream.GetProgressProc: THugeProgProc;
begin
  Result := fProgressProc;
end;

function THugeBMPStream.SetProgressProc(ProgProc: THugeProgProc; dwUserData: DWORD): THugeProgProc;
begin
  Result := fProgressProc;
  fProgressProc := ProgProc;
  fUserData := dwUserdata;
end;

function THugeBMPStream.DoProgress(WriteSize: DWORD): Boolean;
begin
  Result := False; // Non-stop
  if not Assigned(fProgressProc) then
    Exit;
  if fProgressProc(WriteSize, fUserData) then
  begin
    Result := True;
    fHeaded := False;
  end;
end;

procedure THugeBMPStream.WriteBuffer(const Buffer; Count: Integer);
begin
  inherited;
  if DoProgress(Count) then
  begin
    if SizeIsCompleted then
      DoComplete;
    fHeaded := False;
  end;
end;

end.

