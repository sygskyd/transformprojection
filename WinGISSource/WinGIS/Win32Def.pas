Unit Win32Def;

Interface

Uses WinTypes;

Type Integer16     = SmallInt;
     Integer8      = ShortInt;

Const gww_ID       = gwl_ID;
      gcw_HBrBackground = gcl_HBrBackground;
      SelectorInc  = 16;

Function GetTextExtent(DC:THandle;AText:PChar;ALen:Integer):LongInt;

Procedure MoveTo(DC:HDC;X,Y:Integer);

Function SelectorOf(Ptr:Pointer):THandle;

Function MakeLP(Seg,Ofs:Word):Pointer;

Function OffsetOf(Ptr:Pointer):Word;

Function ScaleWindowExt(hDC:THandle;Xnum,Xdenom,Ynum,Ydenom:Integer):Bool;

Function SetWindowOrg(hDC:THandle;X,Y:Integer):Bool;

Function SetViewPortOrg(hDC:THandle;X,Y:Integer):Bool;

Function SetWindowExt(hDC:THandle;X,Y:Integer):Bool;


Implementation

Function GetTextExtent
   (
   DC              : THandle;
   AText           : PChar;
   ALen            : Integer
   )
   : LongInt;
  var Size         : TSize;
  begin
    GetTextExtentPoint32(DC,AText,ALen,Size);
    Result:=MakeLong(Size.cx,Size.cy);
  end;

Function SetViewPortOrg(hDC:THandle;X,Y:Integer):Bool;
  begin
    Result:=SetViewPortOrgEx(hDC,X,Y,NIL);
  end;
    
Procedure MoveTo
   (
   DC              : HDC;
   X               : Integer;
   Y               : Integer
   );
  begin
    MoveToEx(DC,X,Y,NIL);
  end;

Function ScaleWindowExt(hDC:THandle;Xnum,Xdenom,Ynum,Ydenom:Integer):Bool;
  begin
    Result:=ScaleWindowExtEx(hDC,Xnum,Xdenom,Ynum,Ydenom,NIL);
  end;

Function SetWindowExt(hDC:THandle;X,Y:Integer):Bool;
  begin
    Result:=SetWindowExtEx(hDC,X,Y,NIL);
  end;
    
Function SetWindowOrg(hDC:THandle;X,Y:Integer):Bool;
  begin
    Result:=SetWindowOrgEx(hDC,X,Y,NIL);
  end;
    
Function SelectorOf
   (
   Ptr             : Pointer
   )
   : THandle;
  begin
    Result:=0;
  end;

Function MakeLP
   (
   Seg             : Word;
   Ofs             : Word
   )
   : Pointer;
  begin
    Result:=Pointer(Seg*16+Ofs);
  end;

Function OffsetOf
   (
   Ptr             : Pointer
   )
   : Word;
  begin
    Result:=LongInt(Ptr);
  end;      


end.
