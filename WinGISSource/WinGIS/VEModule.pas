unit VEModule;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
//  Psock,
  AM_Child, JPEG, AM_Def, OleCtrls, AxGisPro_TLB, AM_Paint,
  ExtCtrls, PNGImage, StdCtrls, MultiLng, WinInet, SyncObjs, GrTools;

type
  TGeoCodeResult = record
    Valid: Boolean;
    Latitude: Double;
    Longitude: Double;
    South: Double;
    West: Double;
    North: Double;
    East: Double;
    Address: AnsiString;
  end;

  TVEMetaData = record
    CaptureDates: AnsiString;
  end;

  TLoadThread = class(TThread)
  private
    MemStream: TMemoryStream;
    ImgBuffer: array[0..250000] of Char;
    function GetTileFromUrl(AUrl: AnsiString): Integer;
    function SaveJpg(ABuffer: Pointer; ABufferSize: Integer; AFilename: AnsiString): Boolean;
    function SavePng(ABuffer: Pointer; ABufferSize: Integer; AFilename: AnsiString): Boolean;
    function CheckJpgData(AStream: TMemoryStream): Boolean;
    function CheckPngData(AStream: TMemoryStream): Boolean;
  public
    Loading: Boolean;
      Index: Integer;
    UrlList: TStringList;
    procedure Initialize;
    procedure Finalize;
    procedure AddUrl(AUrl: AnsiString);
    procedure LimitStack;
  protected
    procedure Execute; override;
  end;

type
  TVEForm = class(TForm)
    MlgSection: TMlgSection;
    GroupBoxAccount: TGroupBox;
    LabelUser: TLabel;
    EditUser: TEdit;
    LabelPwd: TLabel;
    EditPwd: TEdit;
    GroupBoxButtons: TGroupBox;
    ButtonOK: TButton;
    ButtonCancel: TButton;
    CheckBoxSavePwd: TCheckBox;
    AxGisProjection: TAxGisProjection;
    SaveDialog: TSaveDialog;
    TimerRefresh: TTimer;
    GroupBoxOptions: TGroupBox;
    CheckBoxOffline: TCheckBox;
    MemoHTML: TMemo;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure LabelTermsLinkClick(Sender: TObject);
    procedure CheckBoxTermsClick(Sender: TObject);
    procedure EditUserChange(Sender: TObject);
    procedure EditPwdChange(Sender: TObject);
    procedure ButtonOKClick(Sender: TObject);
    procedure LabelActivationClick(Sender: TObject);
    procedure TimerRefreshTimer(Sender: TObject);
  private
    ProjRefPoint: TDPoint;
    TileBmp: TBitmap;
    TileJpg: TJpegImage;
    TilePng: TPngObject;
    XProjRes, YProjRes: Double;
    QuadKey: AnsiString;
    Subdomain: Char;
    ULX, ULY: Double;
    LLX, LLY: Double;
    URX, URY: Double;
    LRX, LRY: Double;
    CX, CY: Double;
    MapStyle: AnsiString;
    Zoom: Integer;
    XOffset: Integer;
    YOffset: Integer;
    XCorr, YCorr: Integer;
    TileCountX: Integer;
    TileCountY: Integer;
    XScaleFactor, YScaleFactor: Double;
    AvailableTransactions: Integer;
    CurrentTransactionsLogin: Integer;
    CurrentTransactionsGeocoder: Integer;
    CurrentTransactionsBrowser: Integer;
    CanvasRect: TRect;
    CurrentResolution: Double;
    CurrentMetaData: TVEMetaData;
    BingMapsKey: AnsiString;
    DrawnTiles: TStringList;
    CmdFromServer: AnsiString;
    AbortLoading: Boolean;
    MaxZoom: Integer;
    FirstGeocodingFree: Boolean;
    UntilDate: Integer;
    LoginTime: Double;
    WingisVersion: AnsiString;
    HardwareID: AnsiString;
    function ReadLanguage: AnsiString;
    procedure GetProjRes;
    procedure GetMapStyle;
    procedure SetMapStyle;
    procedure GetZoom;
    function SetTileUrl: AnsiString;
    function AllowedToDraw: Boolean;
    function DrawTile(ACol, ARow: Integer; AUrl: AnsiString): Boolean;
    procedure GetTileCountXY;
    function InputOK: Boolean;
    function SendLoginData(ASilent: Boolean): Boolean;
    function GetResponseParam(s: AnsiString; i: Integer): AnsiString;
    procedure ShowUserErrorMessage(AStatus: AnsiString);
    procedure ShowGeoCodeErrorMessage(AStatus: AnsiString);
    procedure UpdateUserTransactionCount(ACount: Integer; AType: Integer);
    function EscPressed: Boolean;
    procedure ReadRegSettings;
    procedure WriteRegSettings;
    function GetCanvasRect: Boolean;
    function IsRectEmpty(ARect: TRect): Boolean;
    procedure CheckCache;
    function GetCurrentMetaData: Boolean;
    function ConvertGeoCodeResult(s: AnsiString): AnsiString;
    procedure EnableOffline;
    procedure DoCommand(ACmd: AnsiString);
    function LatLongToUTMZone(ALat, ALong: Double): AnsiString;
    function ThreadsLoading: Boolean;
    function WasFullRedraw: Boolean;
    function ValidZoom(AUrl: AnsiString): Boolean;
    function IsDummyImage(AFileName: AnsiString): Boolean;
  public
    VEActive: Boolean;
    AChild: TMDIChild;
    APInfo: PPaint;
    GeoCodeResults: array[0..9] of TGeoCodeResult;
    LoggedIn: Boolean;
    LanguageCode: AnsiString;
    LoadAsync: Boolean;
    IsFlatRate: Boolean;
    IsOffline: Boolean;
    AutoStartGeocoder: Boolean;
    procedure DrawBingMapsView;
    function Login: Boolean;
    function LoginWithData: Boolean;
    procedure ShowInfo;
    function SetProjection: Boolean;
    procedure GeoCode;
    procedure DBGeoCode;
    procedure BirdsEye;
    function SearchAddress(AQuery: AnsiString): Boolean;
    procedure CreateAddressObject(AGeoCodeResult: TGeoCodeResult);
    procedure ShowAddressRegion(AGeoCodeResult: TGeoCodeResult);
    procedure CreateXMLFile;
    procedure ShowSLMap;
    function GetResFactor: Double;
    procedure ClearCache;
    procedure FillCache;
    procedure XYToLongLat(var ALongitude, ALatitude: Double);
    procedure LongLatToXY(var X, Y: Double);
    function GetUTMZoneByIP: AnsiString;
    procedure GetViewRect;
    procedure SetVectorRotation;
    procedure RefreshView;
  end;

const
  THREADS_COUNT = 4;
const
  MAX_LOAD_COUNT = 16;
const
  THREAD_WAIT_INTERVAL = 100;
const
  MIN_ZOOM_LEVEL = 5;
const
  MAX_ZOOM_LEVEL = 20;
const
  MAX_DOWNLOAD_ZOOM_LEVEL = 20;
const
  MAX_DOWNLOAD_LEVELS_COUNT = 7;
const
  RES_CHECK_ZOOM_LEVEL = 19;
const
  T_LOGIN = 0;
const
  T_GEOCODER = 1;
const
  T_BROWSER = 2;

const
  EarthRadius = 6378137;
const
  MinLatitude = -85.05112878;
const
  MaxLatitude = 85.05112878;
const
  MinLongitude = -180;
const
  MaxLongitude = 180;
const
  TileSize = 256;

const
  TermsOfUseLinkEN = 'http://www.microsoft.com/maps/product/terms.html';
const
  TermsOfUseLinkDE = 'http://www.microsoft.com/maps/product/terms.html';

const
  VECurrentGeneration = '1135';

const
  BingMapsKey_PROGIS_Enterprise = 'AloV8VffEv47PpMjlZRSPGmt2Z2EE4SlZPXHQEpkJNkuQvRdJRFvlI1gdPNMBLmM';
const
  BingMapsKey_PROGIS_Developer = 'ArK7k6jnq1eeuDnuLVKAcFIVKJdx_4_VFJcP9_8CjqCUX00oRTm6vvBH3gaUseCz';
const
  BingMapsKey_Aigner_Trial = 'AlvVrDcmci1UBvpgFOHP8zewHTP-IqmST_521Gc_ONk-cOQyT9CG_zNXqDIz3qna';
const
  BingMapsKey_Aigner_Basic = 'AsuOeclqDpKfbVaANb_DZo1bzqVDovMJsWp7e50Uchzu70yP_MBUKWpD4UZGZTvT';

//Standard URL
const
  BMTileUrl = 'http://ecn.t{subdomain}.tiles.virtualearth.net/tiles/{mapstyle}{quadkey}?g={generation}&mkt={culture}&shading=hill&stl=H';

// Ordnance Survey
//const BMTileUrl = 'http://ecn.t{subdomain}.tiles.virtualearth.net/tiles/{mapstyle}{quadkey}?g={generation}&productSet=mmOS';

// London Street Map
//const BMTileUrl = 'http://ecn.t{subdomain}.tiles.virtualearth.net/tiles/{mapstyle}{quadkey}?g={generation}&productSet=mmCB';

const
  BMBasicMetaDataUrl = 'http://dev.virtualearth.net/REST/V1/Imagery/BasicMetadata/{imageryset}/{centerpoint}?zl={zoomlevel}&mv=v1&output=xml&key={bingmapskey}';
const
  BMMetaDataUrl = 'http://dev.virtualearth.net/REST/V1/Imagery/Metadata/{imageryset}/{centerpoint}?zl={zoomlevel}&mv=v1&output=xml&key={bingmapskey}';
const
  BMGeoCodeUrl = 'http://dev.virtualearth.net/REST/v1/Locations/{query}?o=xml&key={bingmapskey}';
const
  BMUserUrl = 'http://www.progis.com/bmuser/bmuser.dll?op={op}&user={user}&pwd={pwd}&n={n}&v={version}&hid={hardwareid}';
const
  IPLocationUrl = 'http://api.hostip.info/get_html.php?position=true';

const
  RegKey = 'Software\PROGIS\VE';

var
  LoadThreads: array[0..THREADS_COUNT - 1] of TLoadThread;
var
  VEForm: TVEForm;
var
  BMUserDirName: AnsiString = '';
var
  CacheDir: AnsiString = '';

var
  FileSync: TCriticalSection;
var
  ListSync: TCriticalSection;

implementation

uses AM_Main, AM_Proj, ExtCanvas, AM_DDE, Math, IniFiles, Registry, ShellAPI,
  FileCtrl, MenuFn, VEGeoCode, Crypt, AM_Point, AM_Sym, ToolsLib,
  AM_Index, WinOSInfo, LicenseHndl, ErrHndl, UserIntf, WMSModule;

{$R *.DFM}

{ Threads }

procedure CreateLoadThreads;
var
  i: Integer;
begin
  for i := 0 to THREADS_COUNT - 1 do
  begin
    LoadThreads[i] := TLoadThread.Create(False);
    LoadThreads[i].Loading := False;
    LoadThreads[i].Index := i;
    LoadThreads[i].Initialize;
  end;
end;

procedure DestroyLoadThreads;
var
  i: Integer;
begin
  for i := 0 to THREADS_COUNT - 1 do
  begin
    LoadThreads[i].Finalize;
    LoadThreads[i].Free;
  end;
end;

function DummyImageSize(ASize: Integer): Boolean;
begin
  Result := ((ASize > 1030) and (ASize < 1040));
end;

function StringBetween(var s: AnsiString; s1, s2: AnsiString; ARemove: Boolean): AnsiString;
var
  p1, p2: Integer;
begin
  Result := '';
  p1 := Pos(s1, s) + StrLen(PChar(s1));
  p2 := Pos(s2, s);
  if (p1 > 0) and (p2 > 0) and (p2 > p1) then
  begin
    Result := Copy(s, p1, p2 - p1);
    if ARemove then
    begin
      s := StringReplace(s, s1, '', []);
      s := StringReplace(s, s2, '', []);
    end;
  end;
end;

function AlternateUrl(AUrl: AnsiString): AnsiString;
begin
  AUrl := StringReplace(AUrl, '/tiles/h', '/tiles/r', []);
  AUrl := StringReplace(AUrl, '/tiles/a', '/tiles/r', []);
  Result := AUrl;
end;

function UrlToJpgFileName(AUrl: AnsiString): AnsiString;
begin
  Result := CacheDir + '\' + StringBetween(AUrl, '/tiles/', '?g=', False) + '.jpg';
end;

function UrlToPngFileName(AUrl: AnsiString): AnsiString;
begin
  Result := CacheDir + '\' + StringBetween(AUrl, '/tiles/', '?g=', False) + '.png';
end;

function Clip(n, minValue, maxValue: Double): Double;
begin
  Result := Min(Math.Max(n, minValue), maxValue);
end;

function MapSize(LevelOfDetail: Integer): Double;
var
  i: Int64;
begin
  i := TileSize shl LevelOfDetail;
  Result := i;
end;

function GroundResolution(Latitude: Double; LevelOfDetail: Integer): Double;
begin
  Latitude := Clip(Latitude, MinLatitude, MaxLatitude);
  Result := Cos(Latitude * PI / 180) * 2 * PI * EarthRadius / MapSize(LevelOfDetail);
end;

procedure LatLongToPixelXY(Latitude, Longitude: Double; LevelOfDetail: Integer; var PixelX, PixelY: Integer);
var
  X, Y, SinLat: Double;
  AMapSize: Double;
begin
  Latitude := Clip(Latitude, MinLatitude, MaxLatitude);
  Longitude := Clip(Longitude, MinLongitude, MaxLongitude);

  X := (Longitude + 180) / 360;
  SinLat := Sin(Latitude * PI / 180);
  Y := 0.5 - Ln((1 + SinLat) / (1 - SinLat)) / (4 * PI);

  AMapSize := MapSize(LevelOfDetail);
  PixelX := Trunc(Clip(X * AMapSize + 0.5, 0, AMapSize - 1));
  PixelY := TRunc(Clip(y * AMapSize + 0.5, 0, AMapSize - 1));
end;

procedure PixelXYToTileXY(PixelX, PixelY: Integer; var TileX, TileY: Integer);
begin
  TileX := PixelX div TileSize;
  TileY := PixelY div TileSize;
end;

procedure PixelXYToOffsetXY(PixelX, PixelY: Integer; var OffsetX, OffsetY: Integer);
begin
  OffsetX := PixelX mod TileSize;
  OffsetY := PixelY mod TileSize;
end;

function TileXYToQuadKey(TileX, TileY, LevelOfDetail: Integer): AnsiString;
var
  s: AnsiString;
  i: Integer;
  Digit: Char;
  Mask: Integer;
begin
  s := '';
  i := LevelOfDetail;
  while i > 0 do
  begin
    Digit := '0';
    Mask := 1 shl (i - 1);
    if (TileX and Mask) <> 0 then
    begin
      Inc(Digit);
    end;
    if (TileY and Mask) <> 0 then
    begin
      Inc(Digit, 2);
    end;
    s := s + Digit;
    Dec(i);
  end;
  Result := s;
end;

procedure DelCacheFiles(ADir: AnsiString);
var
  Rec: TSearchRec;
  Found: Integer;
begin
  if ADir[Length(ADir)] <> '\' then
  begin
    ADir := ADir + '\';
  end;
  Found := FindFirst(ADir + '*.*', faAnyFile, Rec);
  while Found = 0 do
  begin
    if (Rec.Name[1] <> '.') then
    begin
      DeleteFile(ADir + Rec.Name);
    end;
    if (Rec.Attr and faDirectory > 0) and (Rec.Name[1] <> '.') then
    begin
      DelCacheFiles(ADir + Rec.Name);
    end;
    Found := FindNext(Rec);
  end;
  FindClose(Rec);
end;

{ TVEForm }

procedure TVEForm.FormCreate(Sender: TObject);
begin
     //BingMapsKey:=BingMapsKey_Enterprise;
  BingMapsKey := BingMapsKey_Aigner_Basic;

  VEActive := False;
  LoggedIn := False;
  FirstGeocodingFree := False;
  AvailableTransactions := 0;
  CurrentTransactionsLogin := 0;
  CurrentTransactionsGeocoder := 0;
  CurrentTransactionsBrowser := 0;
  UntilDate := 0;
  LoginTime := 0;
  WingisVersion := '';
  HardwareID := '';
  CX := 0;
  CY := 0;
  AxGisProjection.Top := -100;
  AxGisProjection.WorkingPath := OSInfo.WingisDir;
  LanguageCode := ReadLanguage;
  AxGisProjection.LngCode := LanguageCode;
  AXGisProjection.ShowOnlySource := True;
  ProjRefPoint.Init(0, 0);
  MapStyle := '-';
  XCorr := 0;
  YCorr := 0;
  LoadAsync := BingMapsAsyncMode;
  IsFlatRate := False;
  IsOffline := False;
  AbortLoading := False;
  CmdFromServer := '';
  DrawnTiles := TStringList.Create;
  AutoStartGeocoder := False;
  TileBmp := TBitmap.Create;
  TileBmp.Width := TileSize;
  TileBmp.Height := TileSize;
  TileJpg := TJpegImage.Create;
  TilePng := TPngObject.Create;
  VEGeoCodeForm := TVEGeoCodeForm.Create(Self);
  ReadRegSettings;
  CreateLoadThreads;
  ListSync := TCriticalSection.Create;
  FileSync := TCriticalSection.Create;
  if RunRemote then
  begin
    CheckBoxSavePwd.Checked := False;
    CheckBoxSavePwd.Enabled := False;
  end;
end;

procedure TVEForm.FormDestroy(Sender: TObject);
begin
  DestroyLoadThreads;
  if not RunRemote then
  begin
    WriteRegSettings;
  end;
  TileBmp.Free;
  TileJpg.Free;
  TilePng.Free;
  DrawnTiles.Free;
  VEGeoCodeForm.Free;
  ListSync.Free;
  FileSync.Free;
end;

function TVEForm.GetResFactor: Double;
begin
  Result := 1.0;

  if (not GetCanvasRect) or (not AllowedToDraw) or (APInfo^.ProjectionSettings.ProjectProjSettings = 'NONE') then
  begin
    exit;
  end;

  GetViewRect;
  GetProjRes;
  GetZoom;

  Result := 1 / ((XScaleFactor + YScaleFactor) / 2);
end;

procedure TVEForm.GetViewRect;
begin
  if not GetCanvasRect then
  begin
    exit;
  end;

  APInfo^.MsgToDrawPos(CanvasRect.Left, CanvasRect.Top, ProjRefPoint);
  ULX := ProjRefPoint.X / 100;
  ULY := ProjRefPoint.Y / 100;
  XYToLongLat(ULX, ULY);

  APInfo^.MsgToDrawPos(CanvasRect.Left, CanvasRect.Bottom, ProjRefPoint);
  LLX := ProjRefPoint.X / 100;
  LLY := ProjRefPoint.Y / 100;
  XYToLongLat(LLX, LLY);

  APInfo^.MsgToDrawPos(CanvasRect.Right, CanvasRect.Top, ProjRefPoint);
  URX := ProjRefPoint.X / 100;
  URY := ProjRefPoint.Y / 100;
  XYToLongLat(URX, URY);

  APInfo^.MsgToDrawPos(CanvasRect.Right, CanvasRect.Bottom, ProjRefPoint);
  LRX := ProjRefPoint.X / 100;
  LRY := ProjRefPoint.Y / 100;
  XYToLongLat(LRX, LRY);

  CX := (ULX + LLX + URX + LRX) / 4;
  CY := (ULY + LLY + URY + LRY) / 4;
end;

procedure TVEForm.SetVectorRotation;
var
  UpperRot, LowerRot, Rotation: Double;
  LX, LY, RX, RY: Double;
  PL, PR: TGrPoint;
  ARot, NRot, SRot: Double;
begin
  if not GetCanvasRect then
  begin
    exit;
  end;

  GetViewRect;
  LX := ULX;
  LY := ULY;
  RX := URX;
  RY := ULY;
  LongLatToXY(LX, LY);
  LongLatToXY(RX, RY);
  PL := GrPoint(LX, LY);
  PR := GrPoint(RX, RY);
  NRot := LineAngle(PL, PR);
  NRot := (NRot * 180) / PI;

  GetViewRect;
  LX := LLX;
  LY := LLY;
  RX := LRX;
  RY := LLY;
  LongLatToXY(LX, LY);
  LongLatToXY(RX, RY);
  PL := GrPoint(LX, LY);
  PR := GrPoint(RX, RY);
  SRot := LineAngle(PL, PR);
  SRot := (SRot * 180) / PI;

  ARot := (NRot + SRot) / 2;

  if Abs(ARot) < 0.01 then
  begin
    ARot := 0;
  end
  else
  begin
    ARot := -(ARot * PI) / 180;
  end;

  APInfo^.SetViewRotation(ARot);
end;

function TVEForm.ValidZoom(AUrl: AnsiString): Boolean;
var
  Inet, hURL: HInternet;
  SizeBuffer: array[0..32] of Char;
  Dummy, SizeBufferSize: DWORD;
  FileSize: Integer;
  AFileName: AnsiString;
  ReadSize: DWORD;
begin
  Result := True;
  if Zoom < RES_CHECK_ZOOM_LEVEL then
  begin
    exit;
  end;
  Inet := InternetOpen(nil, INTERNET_OPEN_TYPE_PRECONFIG, nil, nil, 0);
  if Inet <> nil then
  begin
    hURL := InternetOpenURL(Inet, PChar(AUrl), nil, 0, INTERNET_FLAG_RELOAD, 0);
    if hURL <> nil then
    begin
      SizeBufferSize := SizeOf(SizeBuffer);
      Dummy := 0;
      if HttpQueryInfo(hURL, HTTP_QUERY_CONTENT_LENGTH, @SizeBuffer, SizeBufferSize, Dummy) then
      begin
        FileSize := StrToInt(Copy(SizeBuffer, 0, SizeBufferSize));
        if DummyImageSize(FileSize) and (MaxZoom >= MIN_ZOOM_LEVEL) then
        begin
          Dec(MaxZoom);
          Result := False;
        end;
      end;
      InternetCloseHandle(hURL);
    end;
    InternetCloseHandle(Inet);
  end;
end;

procedure TVEForm.DrawBingMapsView;
var
  PixelX, PixelY: Integer;
  TileX, TileY: Integer;
  X, Y: Integer;
  AUrl: AnsiString;
  i: Integer;
  AIndex: Integer;
  Stop: Boolean;
begin
  try

    if (WMSForm <> nil)
      and (WMSForm.CheckBoxOnOff.Checked)
      and (Trim(WMSForm.EditHost.Text) <> '')
      and (Trim(WMSForm.EditLayers.Text) <> '')
      and (not WMSForm.Transparent) then
    begin
      exit;
    end;

    if (not GetCanvasRect) or (not AllowedToDraw) or (APInfo^.ProjectionSettings.ProjectProjSettings = 'NONE') then
    begin
      exit;
    end;

    MaxZoom := MAX_ZOOM_LEVEL;

    if not IsOffline then
    begin
      repeat
        GetMapStyle;
        GetViewRect;
        GetProjRes;
        GetZoom;
        LatLongToPixelXY(ULY, ULX, Zoom, PixelX, PixelY);
        PixelXYToTileXY(PixelX, PixelY, TileX, TileY);
        PixelXYToOffsetXY(PixelX, PixelY, XOffset, YOffset);
        GetTileCountXY;
        QuadKey := TileXYToQuadKey(TileX + X, TileY + Y, Zoom);
        Subdomain := QuadKey[Length(QuadKey)];
        AUrl := SetTileUrl;
      until ValidZoom(AUrl);
    end;

    GetMapStyle;
    GetViewRect;
    GetProjRes;
    GetZoom;

    LatLongToPixelXY(ULY, ULX, Zoom, PixelX, PixelY);
    PixelXYToTileXY(PixelX, PixelY, TileX, TileY);
    PixelXYToOffsetXY(PixelX, PixelY, XOffset, YOffset);
    GetTileCountXY;

    if Zoom < MIN_ZOOM_LEVEL then
    begin
      exit;
    end;

    DrawnTiles.Clear;

    for Y := 0 to TileCountY - 1 do
    begin
      for X := 0 to TileCountX - 1 do
      begin
        QuadKey := TileXYToQuadKey(TileX + X, TileY + Y, Zoom);
        Subdomain := QuadKey[Length(QuadKey)];
        AUrl := SetTileUrl;
        if not DrawTile(X, Y, AUrl) then
        begin
          AIndex := StrToIntDef(Subdomain, 0);
          if not IsOffline then
          begin
            if not APInfo^.Printing then
            begin
              LoadThreads[AIndex].LimitStack;
            end;
            LoadThreads[AIndex].AddUrl(AUrl);
          end;
        end;
      end;
    end;

    if (LoadAsync) and (not WasFullRedraw) then
    begin
      TimerRefresh.Enabled := True;
    end;

    if (not WasFullRedraw) and (not LoadAsync) and (not IsOffline) then
    begin
      repeat
        Stop := not ThreadsLoading;
        for Y := 0 to TileCountY - 1 do
        begin
          for X := 0 to TileCountX - 1 do
          begin
            QuadKey := TileXYToQuadKey(TileX + X, TileY + Y, Zoom);
            Subdomain := QuadKey[Length(QuadKey)];
            AUrl := SetTileUrl;
            if DrawnTiles.IndexOf(AUrl) = -1 then
            begin
              DrawTile(X, Y, AUrl);
            end;
          end;
        end;
        if EscPressed then
        begin
          break;
        end;
      until Stop;
    end;

    if IsOffline then
    begin
      AutoStartGeocoder := False;
    end;

    if AutoStartGeocoder then
    begin
      AutoStartGeocoder := False;
      FirstGeocodingFree := True;
      GeoCode;
    end;

    if (LoginTime > 0) and ((Now - LoginTime) > 1) then
    begin
      LoginTime := Now;
      UpdateUserTransactionCount(1, T_LOGIN);
    end;

  except
    on E: Exception do
    begin
      DebugMsg('TVEForm.DrawBingMapsView', E.Message);
    end;
  end;
end;

function TVEForm.ThreadsLoading: Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := 0 to THREADS_COUNT - 1 do
  begin
    Result := Result or LoadThreads[i].Loading;
    if Result then
    begin
      break;
    end;
  end;
end;

function TVEForm.WasFullRedraw: Boolean;
begin
  Result := (DrawnTiles.Count >= (TileCountX * TileCountY));
end;

function TVEForm.GetCanvasRect: Boolean;
begin
  Result := True;
  if APInfo = nil then
  begin
    Result := False;
    exit;
  end;
  if IsRectEmpty(APInfo^.ExtCanvas.PhysicalSize) then
  begin
    CanvasRect := APInfo^.ExtCanvas.ClipRect;
    LPToDP(APInfo^.ExtCanvas.Handle, CanvasRect, 2);
  end
  else
  begin
    CanvasRect := APInfo^.ExtCanvas.PhysicalSize;
  end;
  if IsRectEmpty(CanvasRect) then
  begin
    Result := False;
  end;
end;

procedure TVEForm.GetTileCountXY;
var
  X, Y: Double;
begin
  X := (TileSize - XOffset) * XScaleFactor;
  TileCountX := 1;
  while X < Abs(CanvasRect.Right - CanvasRect.Left) do
  begin
    X := X + TileSize * XScaleFactor;
    Inc(TileCountX);
  end;

  Y := (TileSize - YOffset) * YScaleFactor;
  TileCountY := 1;
  while Y < Abs(CanvasRect.Bottom - CanvasRect.Top) do
  begin
    Y := Y + TileSize * YScaleFactor;
    Inc(TileCountY);
  end;
end;

function TVEForm.IsDummyImage(AFileName: AnsiString): Boolean;
var
  ASearch: TSearchRec;
  ASize: Int64;
begin
  ASize := 0;
  if FindFirst(AFileName, faAnyFile, ASearch) = 0 then
  begin
    ASize := Int64(ASearch.FindData.nFileSizeHigh) shl Int64(32) + Int64(ASearch.FindData.nFileSizeLow)
  end;
  FindClose(ASearch);
  Result := DummyImageSize(ASize);
end;

function TVEForm.DrawTile(ACol, ARow: Integer; AUrl: AnsiString): Boolean;
var
  AXPos, AYPos: Double;
  ARect: TRect;
  AOldStretchMode: Integer;
  AOldMapMode: Integer;
  AFileName: AnsiString;
  Loaded: Boolean;
begin
  Result := False;
  Loaded := False;
  FileSync.Enter;
  try
    AFileName := UrlToPngFileName(AUrl);
    if IsDummyImage(AFileName) then
    begin
      AUrl := AlternateUrl(AUrl);
    end;
    AFileName := UrlToJpgFileName(AUrl);
    if FileExists(AFileName) then
    begin
      TileJpg.LoadFromFile(AFileName);
      TileBmp.Canvas.Draw(0, 0, TileJpg);
      Loaded := True;
    end
    else
    begin
      AFileName := UrlToPngFileName(AUrl);
      if FileExists(AFileName) then
      begin
        TilePng.LoadFromFile(AFileName);
        TileBmp.Canvas.Draw(0, 0, TilePng);
        Loaded := True;
      end;
    end;

    if Loaded then
    begin
      AXPos := (ACol * TileSize * XScaleFactor) - (XOffset * XScaleFactor);
      AYPos := (ARow * TileSize * YScaleFactor) - (YOffset * YScaleFactor);

      ARect.Left := Round(AXPos + XCorr);
      ARect.Top := Round(AYPos + YCorr);
      ARect.Right := Round(AXPos + TileSize * XScaleFactor + XCorr);
      ARect.Bottom := Round(AYPos + TileSize * YScaleFactor + YCorr);

      AOldMapMode := SetMapMode(APInfo^.ExtCanvas.Handle, MM_TEXT);
      AOldStretchMode := SetStretchBltMode(APInfo^.ExtCanvas.Handle, HALFTONE);
      SetBrushOrgEx(APInfo^.ExtCanvas.Handle, 0, 0, nil);

      Result := StretchBlt(APInfo^.ExtCanvas.Handle,
        ARect.Left + CanvasRect.Left,
        ARect.Top + CanvasRect.Top,
        ARect.Right - ARect.Left,
        ARect.Bottom - ARect.Top,
        TileBmp.Canvas.Handle,
        0,
        0,
        TileBmp.Width,
        TileBmp.Height,
        SRCCOPY);

      SetStretchBltMode(APInfo^.ExtCanvas.Handle, AOldStretchMode);
      SetMapMode(APInfo^.ExtCanvas.Handle, AOldMapMode);

      if Result then
      begin
        DrawnTiles.Add(AUrl);
      end;
    end;
  finally
    FileSync.Leave;
  end;
end;

function TVEForm.IsRectEmpty(ARect: TRect): Boolean;
begin
  Result := (Abs(ARect.Right - ARect.Left) <= 0) or (Abs(ARect.Right - ARect.Left) <= 0);
end;

procedure TVEForm.GetProjRes;
var
  APoint: TPoint;
  AFact: Double;
  ADist: Integer;
begin
  ADist := CanvasRect.Right - CanvasRect.Left;
  AFact := ADist * 100;
  APoint := Point(ADist, 0);
  DPToLP(APInfo^.ExtCanvas.Handle, APoint, 1);
  XProjRes := Abs(APInfo^.CalculateDrawDouble(APoint.X) / AFact);

  ADist := CanvasRect.Bottom - CanvasRect.Top;
  AFact := ADist * 100;
  APoint := Point(0, ADist);
  DPToLP(APInfo^.ExtCanvas.Handle, APoint, 1);
  YProjRes := Abs(APInfo^.CalculateDrawDouble(APoint.Y) / AFact);
end;

procedure TVEForm.GetZoom;
var
  ACanvasRes: Double;
  AvgScaleFactor: Double;
  ULYRes, LLYRes, URYRes, LRYRes: Double;
  ARes: Double;
begin
  Zoom := MaxZoom + 1;
  repeat
    Dec(Zoom);
    ULYRes := GroundResolution(ULY, Zoom);
    LLYRes := GroundResolution(LLY, Zoom);
    URYRes := GroundResolution(URY, Zoom);
    LRYRes := GroundResolution(LRY, Zoom);
    ARes := (ULYRes + LLYRes + URYRes + LRYRes) / 4;
    CurrentResolution := ARes;
    XScaleFactor := ARes / XProjRes;
    YScaleFactor := ARes / YProjRes;
    AvgScaleFactor := (XScaleFactor + YScaleFactor) / 2;
    ACanvasRes := Min(XProjRes, YProjRes);
  until (ARes > ACanvasRes) or (Zoom = 1) or ((AvgScaleFactor > 0.99) and (AvgScaleFactor < 1.01));
end;

procedure TVEForm.GetMapStyle;
begin
  if MenuFunctions['VERoad'].Checked then
  begin
    MapStyle := 'r';
  end
  else
    if MenuFunctions['VEOrthoLabels'].Checked then
    begin
      MapStyle := 'h';
    end
    else
      if MenuFunctions['VEOrtho'].Checked then
      begin
        MapStyle := 'a';
      end
      else
        if MenuFunctions['VEAutomatic'].Checked then
        begin
          MapStyle := '-';
        end
        else
        begin
          MapStyle := '-';
          SetMapStyle;
        end;
end;

procedure TVEForm.SetMapStyle;
begin
  if MapStyle = 'r' then
  begin
    MenuFunctions['VERoad'].Checked := True;
  end
  else
    if MapStyle = 'h' then
    begin
      MenuFunctions['VEOrthoLabels'].Checked := True;
    end
    else
      if MapStyle = 'a' then
      begin
        MenuFunctions['VEOrtho'].Checked := True;
      end
      else
      begin
        MenuFunctions['VEAutomatic'].Checked := True;
      end;
end;

function TVEForm.SetProjection: Boolean;
var
  UTMZone: AnsiString;
  EmptyProject: Boolean;
begin
  Result := False;
  if AChild.Data^.PInfo^.ProjectionSettings.ProjectProjSettings <> 'NONE' then
  begin
    Result := True;
  end
  else
  begin
    EmptyProject := (APInfo^.GetObjCount = 0);

    AxGisProjection.WorkingPath := OSInfo.WingisDir;
    AxGisProjection.LngCode := ReadLanguage;
    AXGisProjection.ShowOnlySource := True;

    AxGisProjection.SourceProjSettings := 'NONE';
    AxGisProjection.SourceProjection := 'NONE';
    AxGisProjection.SourceDate := 'NONE';
    AxGisProjection.SourceXOffset := 0;
    AxGisProjection.SourceYOffset := 0;
    AXGisProjection.SourceScale := 1;

    if EmptyProject then
    begin
      UTMZone := GetUTMZoneByIP;
    end
    else
    begin
      UTMZone := '';
    end;

    if UTMZone <> '' then
    begin
      AxGisProjection.SourceCoordinateSystem := cs_Carthesic;
      AxGisProjection.SourceProjSettings := 'Universal Transverse Mercator';
      AxGisProjection.SourceProjection := UTMZone;
      AxGisProjection.SourceDate := 'WGS84';
      AxGisProjection.SourceXOffset := 0;
      AxGisProjection.SourceYOffset := 0;
      AXGisProjection.SourceScale := 1;
    end;

    AxGisProjection.SetupByDialog;

    APInfo^.ProjectionSettings.ProjectProjSettings := AxGISProjection.SourceProjSettings;
    APInfo^.ProjectionSettings.ProjectProjection := AxGISProjection.SourceProjection;
    APInfo^.ProjectionSettings.ProjectDate := AxGISProjection.SourceDate;
    APInfo^.ProjectionSettings.ProjectionXOffset := AxGISProjection.SourceXOffset;
    APInfo^.ProjectionSettings.ProjectionYOffset := AxGISProjection.SourceYOffset;
    APInfo^.ProjectionSettings.ProjectionScale := AxGisProjection.SourceScale;

    if AChild.Data^.PInfo^.ProjectionSettings.ProjectProjSettings <> 'NONE' then
    begin
      Result := True;
      AutoStartGeocoder := EmptyProject;
      AChild.Data^.SetModified;
    end
    else
    begin
      MenuFunctions['VEOnOff'].Checked := False;
    end;
  end;
end;

procedure TVEForm.XYToLongLat(var ALongitude, ALatitude: Double);
begin
  try
    AxGISProjection.SourceProjSettings := APInfo^.ProjectionSettings.ProjectProjSettings;
    AxGISProjection.SourceProjection := APInfo^.ProjectionSettings.ProjectProjection;
    AxGISProjection.SourceDate := APInfo^.ProjectionSettings.ProjectDate;
    AxGISProjection.SourceXOffset := APInfo^.ProjectionSettings.ProjectionXOffset;
    AxGISProjection.SourceYOffset := APInfo^.ProjectionSettings.ProjectionYOffset;
    AxGisProjection.SourceScale := APInfo^.ProjectionSettings.ProjectionScale;

    AxGISProjection.TargetProjSettings := 'Geodetic Phi/Lamda';
    AxGISProjection.TargetProjection := 'Phi/Lamda values';
    AxGISProjection.TargetDate := 'WGS84';
    AxGISProjection.TargetXOffset := 0;
    AxGISProjection.TargetYOffset := 0;
    AxGisProjection.TargetScale := 1;

    AXGisProjection.Calculate(ALongitude, ALatitude);
  except
    on E: Exception do
    begin
      DebugMsg('TVEForm.XYToLongLat', E.Message);
    end;
  end;
end;

procedure TVEForm.LongLatToXY(var X, Y: Double);
begin
  try
    AxGISProjection.SourceProjSettings := 'Geodetic Phi/Lamda';
    AxGISProjection.SourceProjection := 'Phi/Lamda values';
    AxGISProjection.SourceDate := 'WGS84';
    AxGISProjection.SourceXOffset := 0;
    AxGISProjection.SourceYOffset := 0;
    AxGisProjection.SourceScale := 1;

    AxGISProjection.TargetProjSettings := APInfo^.ProjectionSettings.ProjectProjSettings;
    AxGISProjection.TargetProjection := APInfo^.ProjectionSettings.ProjectProjection;
    AxGISProjection.TargetDate := APInfo^.ProjectionSettings.ProjectDate;
    AxGISProjection.TargetXOffset := APInfo^.ProjectionSettings.ProjectionXOffset;
    AxGISProjection.TargetYOffset := APInfo^.ProjectionSettings.ProjectionYOffset;
    AxGisProjection.TargetScale := APInfo^.ProjectionSettings.ProjectionScale;

    AXGisProjection.Calculate(X, Y);
  except
    on E: Exception do
    begin
      DebugMsg('TVEForm.XYToLongLat', E.Message);
    end;
  end;
end;

function TVEForm.ReadLanguage: AnsiString;
var
  Ini: TIniFile;
begin
  Ini := TIniFile.Create(OSInfo.WingisIniFileName);
  try
    Result := Ini.ReadString('Settings', 'Language', '044');
  finally
    Ini.Free;
  end;
end;

function TVEForm.SetTileUrl: AnsiString;
var
  Culture: AnsiString;
begin
  Result := BMTileUrl;
  if MapStyle = '-' then
  begin
    if Zoom > 13 then
    begin
      Result := StringReplace(Result, '{mapstyle}', 'h', [rfReplaceAll]);
    end
    else
    begin
      Result := StringReplace(Result, '{mapstyle}', 'r', [rfReplaceAll]);
    end;
  end
  else
  begin
    Result := StringReplace(Result, '{mapstyle}', MapStyle, [rfReplaceAll]);
  end;

  if (CurrentLanguageID = '049') then
  begin
    Culture := 'de-de';
  end
  else
  begin
    Culture := 'en-en';
  end;

  Result := StringReplace(Result, '{subdomain}', Subdomain, [rfReplaceAll]);
  Result := StringReplace(Result, '{quadkey}', QuadKey, [rfReplaceAll]);
  Result := StringReplace(Result, '{generation}', VECurrentGeneration, [rfReplaceAll]);
  Result := StringReplace(Result, '{culture}', Culture, [rfReplaceAll]);
end;

function TVEForm.AllowedToDraw: Boolean;
begin
  VEActive := MenuFunctions['VEOnOff'].Checked;

  Result := (VEActive) and
    (AChild <> nil) and
    ((AvailableTransactions > 0) or (IsFlatRate) or (IsOffline)) and
    (AChild.Data^.SymbolMode = sym_Project);
end;

function TVEForm.Login: Boolean;
var
  ASilentLogin: Boolean;
begin
  ASilentLogin := False;
  if ((not LoggedIn) or ((MenuFunctions['VEOnOff'].Checked) and (GetKeyState(VK_SHIFT) < 0))) then
  begin
    if (ASilentLogin) or (ShowModal = mrOK) then
    begin
      LoggedIn := True;
      BMUserDirName := 'bm_' + EditUser.Text;
      CacheDir := OSInfo.CommonAppDataDir + BMUserDirName;
      CheckCache;
      SetMapStyle;
{$IFNDEF AXDLL}
      DDEHandler.SendString('[VEL][1]');
{$ENDIF}
      if CmdFromServer <> '' then
      begin
        DoCommand(CmdFromServer);
        CmdFromServer := '';
      end;
    end
    else
    begin
      MenuFunctions['VEOnOff'].Checked := False;
{$IFNDEF AXDLL}
      DDEHandler.SendString('[VEL][0]');
{$ENDIF}
    end;
  end;
  Result := LoggedIn;
end;

function TVEForm.LoginWithData: Boolean;
begin
  if not LoggedIn then
  begin
    if SendLoginData(True) then
    begin
      LoggedIn := True;
      BMUserDirName := 'bm_' + EditUser.Text;
      CacheDir := OSInfo.CommonAppDataDir + BMUserDirName;
      CheckCache;
      SetMapStyle;
{$IFNDEF AXDLL}
      DDEHandler.SendString('[VEL][1]');
{$ENDIF}
    end
    else
    begin
      MenuFunctions['VEOnOff'].Checked := False;
{$IFNDEF AXDLL}
      DDEHandler.SendString('[VEL][0]');
{$ENDIF}
    end;
  end;
  Result := LoggedIn;
end;

procedure TVEForm.RefreshView;
begin
  AChild.Data^.ZoomByRect(AChild.Data^.CurrentViewRect, 0, False);
end;

procedure TVEForm.LabelTermsLinkClick(Sender: TObject);
var
  ALink: AnsiString;
begin
  if ReadLanguage = '049' then
  begin
    ALink := TermsOfUseLinkDE;
  end
  else
  begin
    ALink := TermsOfUseLinkEN;
  end;
  ShellExecute(0, 'open', PChar(ALink), nil, nil, SW_SHOWMAXIMIZED);
end;

function TVEForm.InputOK: Boolean;
begin
  Result := (Trim(EditUser.Text) <> '') and (Trim(EditPwd.Text) <> '');
end;

procedure TVEForm.CheckBoxTermsClick(Sender: TObject);
begin
  ButtonOK.Enabled := InputOK;
end;

procedure TVEForm.EditUserChange(Sender: TObject);
begin
  ButtonOK.Enabled := InputOK;
  EnableOffline;
end;

procedure TVEForm.EditPwdChange(Sender: TObject);
begin
  ButtonOK.Enabled := InputOK;
end;

procedure TVEForm.ButtonOKClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    if SendLoginData(False) then
    begin
      ModalResult := mrOK;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TVEForm.SendLoginData(ASilent: Boolean): Boolean;
var
  Inet, hURL: HInternet;
  s, AUrl: AnsiString;
  ABuffer: array[0..1024] of Char;
  ReadSize: DWORD;
  AlternateBingMapsKey: AnsiString;
  i: Integer;
begin
  Result := False;
  IsFlatRate := False;

  IsOffline := CheckBoxOffline.Checked;
  if IsOffline then
  begin
    Result := True;
    exit;
  end;
{$IFNDEF AXDLL}
  WingisVersion := WinGISMainForm.GetWingisVersion;
  s := IntToStr(GetUserId);
{$ELSE}
  WingisVersion := '';
  s := '';
{$ENDIF}
  while StrLen(PChar(s)) < 12 do
  begin
    s := '0' + s;
  end;
  HardwareID := '';
  for i := 1 to StrLen(PChar(s)) do
  begin
    HardwareID := HardwareID + s[i];
    if (i = 4) or (i = 8) then
    begin
      HardwareID := HardwareID + '-';
    end;
  end;

  AUrl := BMUserUrl;
  AUrl := StringReplace(AUrl, '{op}', 'login', []);
  AUrl := StringReplace(AUrl, '{user}', Trim(EditUser.Text), []);
  AUrl := StringReplace(AUrl, '{pwd}', Trim(EditPwd.Text), []);
  AUrl := StringReplace(AUrl, '{n}', '0', []);
  AUrl := StringReplace(AUrl, '{version}', WingisVersion, []);
  AUrl := StringReplace(AUrl, '{hardwareid}', HardwareID, []);

  Inet := InternetOpen(nil, INTERNET_OPEN_TYPE_PRECONFIG, nil, nil, 0);
  if Inet <> nil then
  begin
    hURL := InternetOpenURL(Inet, PChar(AUrl), nil, 0, INTERNET_FLAG_RELOAD, 0);
    if hURL <> nil then
    begin
      InternetReadFile(hURL, @ABuffer, Sizeof(ABuffer), ReadSize);
      s := Copy(ABuffer, 0, ReadSize);
      if GetResponseParam(s, 0) = 'RES_OK' then
      begin
        Result := True;
        AvailableTransactions := StrToInt(GetResponseParam(s, 1));
        if AvailableTransactions = -1 then
        begin
          IsFlatRate := True;
        end;
        UntilDate := StrToInt(GetResponseParam(s, 2));
        if UntilDate > 0 then
        begin
          if Trunc(Now) > UntilDate then
          begin
            AvailableTransactions := 0;
            IsFlatRate := False;
            Result := False;
            ShowUserErrorMessage('RES_ZERO');
          end;
        end;
        if Result then
        begin
          UpdateUserTransactionCount(1, T_LOGIN);
          LoginTime := Now;
        end;
        AlternateBingMapsKey := Trim(GetResponseParam(s, 3));
        if AlternateBingMapsKey <> '' then
        begin
          BingMapsKey := AlternateBingMapsKey;
        end;
        CmdFromServer := Trim(GetResponseParam(s, 4));
      end
      else
      begin
        if GetResponseParam(s, 0) = 'RES_ZERO' then
        begin
          ShowUserErrorMessage(GetResponseParam(s, 0));
        end
        else
        begin
          if not ASilent then
          begin
                 //ShowUserErrorMessage(GetResponseParam(s,0));
          end;
          Result := True;
          IsFlatRate := True;
        end;
      end;
      InternetCloseHandle(hURL);
    end
    else
    begin
      if not ASilent then
      begin
        ShowUserErrorMessage('');
      end;
    end;
    InternetCloseHandle(Inet);
  end;
  if (AvailableTransactions > 0) and (AvailableTransactions <= 10) then
  begin
    s := Format(MlgSection[8], [IntToStr(AvailableTransactions)]);
    MessageBox(Handle, PChar(s), PChar(Caption), MB_ICONINFORMATION or MB_OK or MB_APPLMODAL);
  end;
end;

function TVEForm.GetResponseParam(s: AnsiString; i: Integer): AnsiString;
var
  AStringList: TStringList;
begin
  Result := '';
  AStringList := TStringList.Create;
  try
    s := StringReplace(s, '|', #13#10, [rfReplaceAll]);
    AStringList.Text := s;
    if i < AStringList.Count then
    begin
      Result := AStringList[i];
    end;
  finally
    AStringList.Free;
  end;
end;

procedure TVEForm.ShowUserErrorMessage(AStatus: AnsiString);
var
  s: AnsiString;
begin
  if AStatus = 'RES_ERROR_USER' then
  begin
    s := MlgSection[9];
  end
  else
    if (AStatus = 'RES_ZERO') then
    begin
      s := MlgSection[10];
    end
    else
    begin
      s := MlgSection[11];
    end;
  s := StringReplace(s, '\n', #10#13, [rfReplaceAll]);
  MessageBox(Handle, PChar(s), PChar(Caption), MB_OK or MB_ICONERROR or MB_SYSTEMMODAL);
end;

procedure TVEForm.ShowGeoCodeErrorMessage(AStatus: AnsiString);
var
  s: AnsiString;
begin
  s := MlgSection[24];
  s := StringReplace(s, '\n', #10#13, [rfReplaceAll]);
  VEGeoCodeForm.ListBoxResults.Items.Add(s);
end;

procedure TVEForm.UpdateUserTransactionCount(ACount: Integer; AType: Integer);
var
  Inet, hURL: HInternet;
  s, AUrl: AnsiString;
  ABuffer: array[0..100] of Char;
  ReadSize: DWORD;
begin
  if ACount <= 0 then
  begin
    exit;
  end;

  case AType of
    T_LOGIN: Inc(CurrentTransactionsLogin, ACount);
    T_GEOCODER: Inc(CurrentTransactionsGeocoder, ACount);
    T_BROWSER: Inc(CurrentTransactionsBrowser, ACount);
  end;

  AUrl := BMUserUrl;
  AUrl := StringReplace(AUrl, '{op}', 'count', []);
  AUrl := StringReplace(AUrl, '{user}', Trim(EditUser.Text), []);
  AUrl := StringReplace(AUrl, '{pwd}', Trim(EditPwd.Text), []);
  AUrl := StringReplace(AUrl, '{n}', IntToStr(ACount), []);
  AUrl := StringReplace(AUrl, '{version}', WingisVersion, []);
  AUrl := StringReplace(AUrl, '{hardwareid}', HardwareID, []);

  Inet := InternetOpen(nil, INTERNET_OPEN_TYPE_PRECONFIG, nil, nil, 0);
  if Inet <> nil then
  begin
    hURL := InternetOpenURL(Inet, PChar(AUrl), nil, 0, INTERNET_FLAG_RELOAD, 0);
    if hURL <> nil then
    begin
      InternetReadFile(hURL, @ABuffer, Sizeof(ABuffer), ReadSize);
      s := Copy(ABuffer, 0, ReadSize);
      if GetResponseParam(s, 0) = 'RES_OK' then
      begin
        AvailableTransactions := StrToInt(GetResponseParam(s, 1));
      end
      else
      begin
        if GetResponseParam(s, 0) = 'RES_ZERO' then
        begin
          AvailableTransactions := 0;
        end;
      end;
      InternetCloseHandle(hURL);
    end;
    InternetCloseHandle(Inet);
  end;
end;

procedure TVEForm.ShowInfo;
var
  AList: TStringList;
  CacheSize: Int64;
  SizeUnit: AnsiString;
  FilesCount: Integer;
  FormatedDate: AnsiString;
begin
  Screen.Cursor := crHourGlass;
  AList := TStringList.Create;
  try
    if not IsOffline then
    begin
      if IsFlatRate then
      begin
        AList.Add(Format(MlgSection[8], [MlgSection[35]]) + #13#10);
      end
      else
      begin
        AList.Add(Format(MlgSection[8], [IntToStr(AvailableTransactions)]) + #13#10);
      end;
    end;

    AList.Add(Format(MlgSection[31], [IntToStr(CurrentTransactionsLogin + CurrentTransactionsGeocoder + CurrentTransactionsBrowser)]));

    if CurrentTransactionsLogin > 0 then
    begin
      AList.Add(Format(MlgSection[45], [IntToStr(CurrentTransactionsLogin)]));
    end;
    if CurrentTransactionsGeocoder > 0 then
    begin
      AList.Add(Format(MlgSection[46], [IntToStr(CurrentTransactionsGeocoder)]));
    end;
    if CurrentTransactionsBrowser > 0 then
    begin
      AList.Add(Format(MlgSection[47], [IntToStr(CurrentTransactionsBrowser)]));
    end;

    AList.Add('');

    if UntilDate > 0 then
    begin
      FormatedDate := FormatDateTime('yyyy-mm-dd', UntilDate);
      AList.Add(Format(MlgSection[44], [FormatedDate]) + #13#10);
    end;

    FilesCount := 0;
    CacheSize := GetDirSize(CacheDir, True, FilesCount);
    CacheSize := CacheSize div 1024;
    SizeUnit := 'KB';

    if CacheSize > 1024 then
    begin
      CacheSize := CacheSize div 1024;
      SizeUnit := 'MB';
    end;
    if CacheSize > 1024 then
    begin
      CacheSize := CacheSize div 1024;
      SizeUnit := 'GB';
    end;

    AList.Add(Format(MlgSection[36], [IntToStr(CacheSize) + ' ' + SizeUnit]));
    AList.Add(Format(MlgSection[37], [IntToStr(FilesCount)]) + #13#10);

    AList.Add(Format(MlgSection[12], [IntToStr(Zoom)]));
    AList.Add(Format(MlgSection[32], [Format('%.2f', [CurrentResolution])]) + #13#10);

    AList.Add(Format(MlgSection[34], [Format('%.5f', [CY])]));
    AList.Add(Format(MlgSection[33], [Format('%.5f', [CX])]));

    if not IsOffline then
    begin
      if GetCurrentMetaData then
      begin
        AList.Add(Format(#13#10 + MlgSection[40], [CurrentMetaData.CaptureDates]));
      end;
    end;

  finally
    Screen.Cursor := crDefault;
    MessageBox(WinGISMainForm.Handle, PChar(AList.Text), PChar(Caption), MB_ICONINFORMATION or MB_OK or MB_APPLMODAL);
    AList.Free;
  end;
end;

function TVEForm.EscPressed: Boolean;
var
  AMsg: TMsg;
begin
  Result := False;
  if not CoreConnected then
  begin
    if PeekMessage(AMsg, 0, 0, 0, PM_REMOVE) then
    begin
      if (AMsg.Message = WM_KEYDOWN) and (AMsg.wParam = VK_ESCAPE) then
      begin
        Result := True;
        AbortLoading := True;
      end;
    end;
  end;
end;

procedure TVEForm.ReadRegSettings;
var
  Reg: TRegistry;
begin
  MapStyle := '';

  Reg := TRegistry.Create;
  try
    Reg.RootKey := HKEY_CURRENT_USER;
    if Reg.OpenKeyReadOnly(RegKey) then
    begin
      if Reg.ValueExists('User') then
      begin
        EditUser.Text := Reg.ReadString('User');
      end;
      if Reg.ValueExists('Pwd') then
      begin
        EditPwd.Text := DecryptString(Reg.ReadString('Pwd'));
        if EditPwd.Text <> '' then
        begin
          CheckBoxSavePwd.Checked := True;
        end;
      end;
    end;
  finally
    Reg.Free;
  end;

  if MapStyle = '' then
  begin
    MapStyle := '-';
  end;
end;

procedure TVEForm.WriteRegSettings;
var
  Reg: TRegistry;
begin
  Reg := TRegistry.Create;
  try
    Reg.RootKey := HKEY_CURRENT_USER;
    if Reg.KeyExists(RegKey) then
    begin
      Reg.DeleteKey(RegKey);
    end;
    if Reg.OpenKey(RegKey, True) then
    begin
      Reg.WriteString('User', Trim(EditUser.Text));
      if (CheckBoxSavePwd.Checked) and (Trim(EditPwd.Text) <> '') then
      begin
        Reg.WriteString('Pwd', CryptString(Trim(EditPwd.Text)));
      end
      else
      begin
        Reg.DeleteValue('Pwd');
      end;
      Reg.CloseKey;
    end;
  finally
    Reg.Free;
  end;
end;

procedure TVEForm.LabelActivationClick(Sender: TObject);
var
  s: AnsiString;
begin
  s := 'mailto:wingis@progis.com?subject=' + MlgSection[21];
  ShellExecute(0, 'open', PChar(s), nil, nil, SW_SHOW);
end;

function TVEForm.SearchAddress(AQuery: AnsiString): Boolean;
var
  Inet, hURL: HInternet;
  AUrl: AnsiString;
  s: WideString;
  i, ResCount: Integer;
  ResLine: AnsiString;
  ABuffer: array[0..10000] of Char;
  ReadSize: DWORD;
begin
  Result := False;

  AQuery := Trim(AQuery);
  if (AQuery = '') then
  begin
    exit;
  end;

  AUrl := BMGeoCodeUrl;
  AUrl := StringReplace(AUrl, '{query}', AQuery, []);
  AUrl := StringReplace(AUrl, '{bingmapskey}', BingMapsKey, []);

  Inet := InternetOpen(nil, INTERNET_OPEN_TYPE_PRECONFIG, nil, nil, 0);
  if Inet <> nil then
  begin
    hURL := InternetOpenURL(Inet, PChar(AUrl), nil, 0, INTERNET_FLAG_RELOAD, 0);
    if hURL <> nil then
    begin
      s := '';
      repeat
        InternetReadFile(hURL, @ABuffer, Sizeof(ABuffer), ReadSize);
        s := s + Copy(ABuffer, 0, ReadSize);
      until (ReadSize = 0);
      s := ConvertGeoCodeResult(s);
      ResCount := StrToIntDef(GetResponseParam(s, 0), 0);
      if ResCount > 0 then
      begin
        if FirstGeocodingFree then
        begin
          FirstGeocodingFree := False;
        end
        else
        begin
          UpdateUserTransactionCount(1, T_GEOCODER);
        end;
        ResCount := Min(ResCount, 10);
        for i := 0 to ResCount - 1 do
        begin
          GeoCodeResults[i].Latitude := SToF(GetResponseParam(s, (i * 7) + 1));
          GeoCodeResults[i].Longitude := SToF(GetResponseParam(s, (i * 7) + 2));
          GeoCodeResults[i].South := SToF(GetResponseParam(s, (i * 7) + 3));
          GeoCodeResults[i].West := SToF(GetResponseParam(s, (i * 7) + 4));
          GeoCodeResults[i].North := SToF(GetResponseParam(s, (i * 7) + 5));
          GeoCodeResults[i].East := SToF(GetResponseParam(s, (i * 7) + 6));
          if (GeoCodeResults[i].Latitude <> 0) and
            (GeoCodeResults[i].Longitude <> 0) and
            (GeoCodeResults[i].South <> 0) and
            (GeoCodeResults[i].West <> 0) and
            (GeoCodeResults[i].North <> 0) and
            (GeoCodeResults[i].East <> 0) then
          begin
            Result := True;
            GeoCodeResults[i].Valid := True;
            GeoCodeResults[i].Address := GetResponseParam(s, (i * 7) + 7);
            ResLine := GeoCodeResults[i].Address;
            ResLine := ResLine + ' (' + Format('%.2f', [GeoCodeResults[i].Latitude]);
            ResLine := ResLine + ', ' + Format('%.2f', [GeoCodeResults[i].Longitude]) + ')';
            VEGeoCodeForm.ListBoxResults.Items.Add(ResLine);
          end;
        end;
      end
      else
      begin
        ShowGeoCodeErrorMessage(GetResponseParam(s, 0));
      end;
      InternetCloseHandle(hURL);
    end
    else
    begin
      ShowGeoCodeErrorMessage('');
    end;
    InternetCloseHandle(Inet);
  end;
end;

function TVEForm.ConvertGeoCodeResult(s: AnsiString): AnsiString;
var
  ResultCount: Integer;
  AName, ALat, ALong: AnsiString;
  ASouth, AWest, ANorth, AEast: AnsiString;
begin
  ResultCount := 0;
  Result := '';
  repeat
    AName := Utf8Decode(StringBetween(s, '<Name>', '</Name>', True));
    ALat := StringBetween(s, '<Latitude>', '</Latitude>', True);
    ALong := StringBetween(s, '<Longitude>', '</Longitude>', True);
    ASouth := StringBetween(s, '<SouthLatitude>', '</SouthLatitude>', True);
    AWest := StringBetween(s, '<WestLongitude>', '</WestLongitude>', True);
    ANorth := StringBetween(s, '<NorthLatitude>', '</NorthLatitude>', True);
    AEast := StringBetween(s, '<EastLongitude>', '</EastLongitude>', True);
    StringBetween(s, '<Latitude>', '</Latitude>', True);
    StringBetween(s, '<Longitude>', '</Longitude>', True);
    if (AName <> '') and (ALat <> '') and (ALong <> '') and
      (ASouth <> '') and (AWest <> '') and (ANorth <> '') and (AEast <> '') then
    begin
      Result := Result + '|' +
        ALat + '|' +
        ALong + '|' +
        ASouth + '|' +
        AWest + '|' +
        ANorth + '|' +
        AEast + '|' +
        AName;
      Inc(ResultCount);
    end;
  until (AName = '');
  Result := IntToStr(ResultCount) + Result;
end;

procedure TVEForm.CreateAddressObject(AGeoCodeResult: TGeoCodeResult);
var
  APixel: PPixel;
  ASymbol: PSymbol;
  APoint: TDPoint;
  NewObj: PIndex;
  X, Y: Double;
begin
  X := AGeoCodeResult.Longitude;
  Y := AGeoCodeResult.Latitude;

  LongLatToXY(X, Y);

  APoint.Init(Trunc(X * 100), Trunc(Y * 100));

  NewObj := nil;

  if VEGeoCodeForm.RadioButtonSymbol.Checked then
  begin
    ASymbol := New(PSymbol, InitName(APInfo,
      APoint,
      AChild.Data^.ActualSym^.GetName,
      AChild.Data^.ActualSym^.GetLibName,
      AChild.Data^.ActSymSize,
      AChild.Data^.ActSymSizeType,
      AChild.Data^.ActSymAngle));

    if ASymbol <> nil then
    begin
      if AChild.Data^.InsertObject(ASymbol, FALSE) then
      begin
        NewObj := PIndex(ASymbol);
        AChild.Data^.RefreshSymbolMan(False);
      end;
    end;
  end
  else
  begin
    APixel := New(PPixel, Init(APoint));
    if AChild.Data^.InsertObject(APixel, FALSE) then
    begin
      NewObj := PIndex(APixel);
    end;
  end;

{$IFNDEF AXDLL}
  if (NewObj <> nil) and (DDEHandler.DoAutoIns) then
  begin
    AChild.Data^.DBEditAuto(NewObj);
  end;
{$ENDIF}
end;

procedure TVEForm.GeoCode;
begin
  if ((AvailableTransactions <= 0) and (not IsFlatRate) and (not IsOffline)) then
  begin
    ShowUserErrorMessage('RES_ZERO');
    exit;
  end;

  if AChild.Data^.ActualSym = nil then
  begin
    VEGeoCodeForm.RadioButtonPoint.Checked := True;
    VEGeoCodeForm.RadioButtonSymbol.Enabled := False;
  end
  else
  begin
    VEGeoCodeForm.RadioButtonSymbol.Enabled := True;
  end;

  VEGeoCodeForm.Show;
end;

procedure TVEForm.ShowAddressRegion(AGeoCodeResult: TGeoCodeResult);
var
  ABottom, ALeft, ATop, ARight: Double;
  ARect: TRotRect;
  AProj: PProj;
  BMFact: Double;
begin
  ABottom := AGeoCodeResult.South;
  ALeft := AGeoCodeResult.West;
  ATop := AGeoCodeResult.North;
  ARight := AGeoCodeResult.East;

  LongLatToXY(ALeft, ABottom);
  LongLatToXY(ARight, ATop);

  ARect.X := ALeft * 100;
  ARect.Y := ABottom * 100;
  ARect.Width := Abs(ARight - ALeft) * 100;
  ARect.Height := Abs(ATop - ABottom) * 100;
  ARect.Rotation := 0;

  AProj := WingisMainForm.ActualChild.Data;
  WingisMainForm.AllowDraw := False;
  SetVectorRotation;
  AProj.ZoomByRect(ARect, 0, False);
  SetVectorRotation;
  AProj.ZoomByRect(ARect, 0, False);
  WingisMainForm.AllowDraw := True;
  BMFact := VEForm.GetResFactor;
  AProj.ZoomByFactor(BMFact);
end;

procedure TVEForm.DBGeoCode;
var
  ExecDBG: function(AParet: Integer; AKey: PChar; ALanguageCode: PChar): Integer; stdcall;
  LibHandle: Integer;
  ARes: Integer;
begin
  if ((AvailableTransactions <= 0) and (not IsFlatRate) and (not IsOffline)) then
  begin
    ShowUserErrorMessage('RES_ZERO');
    exit;
  end;

  LibHandle := LoadLibrary('BMLib.dll');
  if LibHandle <> 0 then
  begin
    @ExecDBG := GetProcAddress(LibHandle, 'ExecDBG');
    if @ExecDBG <> nil then
    begin
      ARes := ExecDBG(WingisMainForm.Handle, PChar(BingMapsKey), PChar(LanguageCode));
      if ARes > 0 then
      begin
        ARes := Max(1, ARes div 50);
        UpdateUserTransactionCount(ARes, T_GEOCODER);
      end;
    end;
    FreeLibrary(LibHandle);
  end;
end;

procedure TVEForm.CreateXMLFile;
var
  ProjectToXMLFile: function(Handle: Integer; Doc: Variant; XMLFilename: PChar): HRESULT; stdcall;
  LibHandle: Integer;
  XMLFilename: AnsiString;
  s: AnsiString;
begin
{$IFNDEF AXDLL}
  LibHandle := LoadLibrary('BMLib.dll');
  if LibHandle <> 0 then
  begin
    @ProjectToXMLFile := GetProcAddress(LibHandle, 'ProjectToXMLFile');
    if @ProjectToXMLFile <> nil then
    begin
      if SaveDialog.Execute then
      begin
        XMLFilename := SaveDialog.FileName;
        if ProjectToXMLFile(Application.Handle, WinGISMainForm.GetCurrentTDocument, PChar(XMLFilename)) <> S_OK then
        begin
          s := MlgSection[38];
          MessageBox(WinGISMainForm.Handle, PChar(s), PChar(Caption), MB_ICONERROR or MB_OK or MB_APPLMODAL);
        end;
      end;
    end;
    FreeLibrary(LibHandle);
  end;
{$ENDIF}
end;

procedure TVEForm.ShowSLMap;
var
  AUrl: AnsiString;
begin
  AUrl := 'http://www.progis.com/map/';
  if ShellExecute(0, 'open', PChar(AUrl), nil, nil, SW_SHOWMAXIMIZED) > 32 then
  begin
    UpdateUserTransactionCount(1, T_BROWSER);
  end;
end;

procedure TVEForm.CheckCache;
var
  ADir: AnsiString;
  i: Integer;
begin
  if CacheDir <> '' then
  begin
    if not DirectoryExists(CacheDir) then
    begin
      CreateDir(CacheDir);
    end;
    for i := 0 to 3 do
    begin
      RemoveDir(CacheDir + IntToStr(i));
    end;
  end;
end;

procedure TVEForm.ClearCache;
begin
  if MessageBox(WinGISMainForm.Handle, PChar(MlgSection[41]), PChar(Caption), MB_ICONQUESTION or MB_YESNO or MB_APPLMODAL) = IDYES then
  begin
    Screen.Cursor := crHourGlass;
    try
      DelCacheFiles(CacheDir);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TVEForm.FillCache;
var
  Rows, Cols: Integer;
  i, k, n, p, Level: Integer;
  FullRect, CurRect: TRotRect;
  AProj: PProj;
  BMFact: Double;
  s: AnsiString;
begin
  s := MlgSection[43];
  s := Format(s, [IntToStr(Zoom), IntToStr(Min(Zoom + MAX_DOWNLOAD_LEVELS_COUNT - 1, MAX_DOWNLOAD_ZOOM_LEVEL))]);
  s := StringReplace(s, '\n', #10#13, [rfReplaceAll]);
  if MessageBox(WinGISMainForm.Handle, PChar(s), PChar(Caption), MB_ICONQUESTION or MB_YESNO or MB_APPLMODAL) <> ID_YES then
  begin
    Exit;
  end;
  AbortLoading := False;
  StatusBar.ProgressPanel := True;
  try
    AProj := WingisMainForm.ActualChild.Data;
    FullRect := AProj^.PInfo^.GetCurrentScreen;
    for Level := 0 to MAX_DOWNLOAD_LEVELS_COUNT - 1 do
    begin
      if (AbortLoading or EscPressed) or (Zoom > MAX_DOWNLOAD_ZOOM_LEVEL) then
      begin
        break;
      end;
      StatusBar.Progress := 0;
      Userinterface.Update([uiStatusBar]);
      BMFact := VEForm.GetResFactor;
      AProj.ZoomByFactor(BMFact);
      CurRect := AProj^.PInfo^.GetCurrentScreen;
      Cols := Trunc(FullRect.Width / CurRect.Width) + 2;
      Rows := Trunc(FullRect.Height / CurRect.Height) + 2;
      n := Cols * Rows;
      p := 0;
      CurRect.X := FullRect.X - CurRect.Width;
      CurRect.Y := FullRect.Y + FullRect.Height + CurRect.Height;
      for i := 0 to Rows - 1 do
      begin
        if AbortLoading or EscPressed then
        begin
          break;
        end;
        for k := 0 to Cols - 1 do
        begin
          if AbortLoading or EscPressed then
          begin
            break;
          end;
          StatusBar.ProgressText := 'Zoom Level ' + IntToStr(Zoom) + ' ' +
            '(X ' + IntToStr(k + 1) + '/' + IntToStr(Cols) + ') ' +
            '(Y ' + IntToStr(i + 1) + '/' + IntToStr(Rows) + ') ' +
            '(ESC - Stop)';
          AProj^.ZoomByRect(CurRect, 0, False);
          CurRect.X := CurRect.X + CurRect.Width;
          Inc(p);
          StatusBar.Progress := (100 * p) / n;
        end;
        CurRect.X := FullRect.X - CurRect.Width;
        CurRect.Y := CurRect.Y - CurRect.Height;
      end;
      BMFact := VEForm.GetResFactor;
      AProj^.ZoomByFactor(BMFact * 2);
    end;
    AProj^.ZoomByRect(FullRect, 0, False);
  finally
    StatusBar.ProgressPanel := False;
  end;
end;

function TVEForm.GetCurrentMetaData: Boolean;
var
  Inet, hURL: HInternet;
  s, AUrl: AnsiString;
  CenterPoint, ImagerySet: AnsiString;
  AMapStyle: AnsiString;
  p1, p2: Integer;
  ABuffer: array[0..16000] of Char;
  ReadSize: DWORD;
  AdvMetaData: Boolean;
begin
  Result := False;

  AdvMetaData := (GetKeyState(VK_CONTROL) < 0);

  if AdvMetaData then
  begin
    AUrl := BMMetaDataUrl;
  end
  else
  begin
    AUrl := BMBasicMetaDataUrl;
  end;

  CurrentMetaData.CaptureDates := '';
  CenterPoint := StringReplace(FloatToStr(CY), ',', '.', []) + ',' + StringReplace(FloatToStr(CX), ',', '.', []);
  AMapStyle := MapStyle;

  if AMapStyle = '-' then
  begin
    if Zoom > 13 then
    begin
      AMapStyle := 'h';
    end
    else
    begin
      AMapStyle := 'r';
    end;
  end;

  if AMapStyle = 'r' then
  begin
    ImagerySet := 'Road';
  end
  else
    if AMapStyle = 'a' then
    begin
      ImagerySet := 'Aerial';
    end
    else
      if AMapStyle = 'h' then
      begin
        ImagerySet := 'AerialWithLabels';
      end
      else
      begin
        exit;
      end;

  AUrl := StringReplace(AUrl, '{imageryset}', ImagerySet, []);
  AUrl := StringReplace(AUrl, '{centerpoint}', CenterPoint, []);
  AUrl := StringReplace(AUrl, '{zoomlevel}', IntToStr(Zoom), []);
  AUrl := StringReplace(AUrl, '{bingmapskey}', BingMapsKey, []);

  if AdvMetaData then
  begin
    if ShellExecute(0, 'open', PChar(AUrl), nil, nil, SW_SHOW) > 32 then
    begin
      UpdateUserTransactionCount(1, T_BROWSER);
      exit;
    end;
  end;

  Inet := InternetOpen(nil, INTERNET_OPEN_TYPE_PRECONFIG, nil, nil, 0);
  if Inet <> nil then
  begin
    hURL := InternetOpenURL(Inet, PChar(AUrl), nil, 0, INTERNET_FLAG_RELOAD, 0);
    if hURL <> nil then
    begin
      s := '';
      repeat
        InternetReadFile(hURL, @ABuffer, Sizeof(ABuffer), ReadSize);
        s := s + Copy(ABuffer, 0, ReadSize);
      until (ReadSize = 0);

      p1 := Pos('<VintageStart>', s);
      p2 := Pos('</VintageStart>', s);
      if (p1 > 0) and (p2 > 0) then
      begin
        Result := True;
        p1 := p1 + StrLen('<VintageStart>');
        CurrentMetaData.CaptureDates := Copy(s, p1, p2 - p1);
      end;
      p1 := Pos('<VintageEnd>', s);
      p2 := Pos('</VintageEnd>', s);
      if (p1 > 0) and (p2 > 0) then
      begin
        Result := True;
        p1 := p1 + StrLen('<VintageEnd>');
        if Copy(s, p1, p2 - p1) <> CurrentMetaData.CaptureDates then
        begin
          CurrentMetaData.CaptureDates := CurrentMetaData.CaptureDates + ' - ' + Copy(s, p1, p2 - p1);
        end;
      end;
    end;
    InternetCloseHandle(Inet);
  end;
end;

procedure TVEForm.TimerRefreshTimer(Sender: TObject);
begin
  if WasFullRedraw then
  begin
    TimerRefresh.Enabled := False;
  end;
  APInfo^.RedrawScreen(True);
end;

procedure TVEForm.EnableOffline;
var
  ADir: AnsiString;
  AEnable: Boolean;
  ACount: Integer;
begin
  ADir := OSInfo.CommonAppDataDir + 'bm_' + EditUser.Text;
  AEnable := DirectoryExists(ADir);
  if AEnable then
  begin
    ACount := 0;
    GetDirSize(ADir, True, ACount);
    AEnable := (AEnable and (ACount > 0));
  end;
  CheckBoxOffline.Enabled := AEnable;
end;

procedure TVEForm.DoCommand(ACmd: AnsiString);
var
  s: AnsiString;
begin
  ACmd := Trim(Uppercase(ACmd));
  if Pos('SETWGMIN', ACmd) > 0 then
  begin
    WingisLicense := Max(WingisLicense, WG_MIN);
    WingisMainForm.SetAppTitle;
  end;
  if Pos('SETWGSTD', ACmd) > 0 then
  begin
    WingisLicense := Max(WingisLicense, WG_STD);
    WingisMainForm.SetAppTitle;
  end;
  if Pos('SETWGPROF', ACmd) > 0 then
  begin
    WingisLicense := Max(WingisLicense, WG_PROF);
    WingisMainForm.SetAppTitle;
  end;
  if Pos('RESETLIC', ACmd) > 0 then
  begin
    WingisLicense := WG_MIN;
    WingisMainForm.SetAppTitle;
    DeleteFile(OSInfo.CommonAppDataDir + 'progis.id');
  end;
end;

function TVEForm.GetUTMZoneByIP: AnsiString;
var
  hURL, Inet: HInternet;
  s: AnsiString;
  LatitudeText: AnsiString;
  LongitudeText: AnsiString;
  ALongitude, ALatitude: Double;
  AList: TStringList;
  ABuffer: array[0..1024] of Char;
  ReadSize: DWORD;
begin
  Result := '';
  s := '';
  Inet := InternetOpen(nil, INTERNET_OPEN_TYPE_PRECONFIG, nil, nil, 0);
  if Inet <> nil then
  begin
    hURL := InternetOpenURL(Inet, PChar(IPLocationUrl), nil, 0, INTERNET_FLAG_RELOAD, 0);
    if hURL <> nil then
    begin
      InternetReadFile(hURL, @ABuffer, SizeOf(ABuffer), ReadSize);
      if ReadSize > 0 then
      begin
        s := Copy(ABuffer, 0, ReadSize);
      end;
      InternetCloseHandle(hURL);
    end;
  end;
  if s <> '' then
  begin
    s := StringReplace(s, ': ', '=', [rfReplaceAll]);
    AList := TStringList.Create;
    try
      AList.Text := s;
      LatitudeText := AList.Values['Latitude'];
      LongitudeText := AList.Values['Longitude'];
    finally
      AList.Free;
    end;
    if (LatitudeText <> '') and (LongitudeText <> '') then
    begin
      ALatitude := SToF(LatitudeText);
      ALongitude := SToF(LongitudeText);
      if (ALatitude <> 0) and (ALongitude <> 0) then
      begin
        Result := LatLongToUTMZone(ALatitude, ALongitude);
      end;
    end;
  end;
end;

function TVEForm.LatLongToUTMZone(ALat, ALong: Double): AnsiString;
var
  NS, WE: AnsiString;
  M1, M2, Z: Integer;
begin
  Result := '';
  if (ALat > 90) or (ALat < -90) or (ALong > 180) or (ALong < -180) then
  begin
    exit;
  end;
  if ALat < 0 then
  begin
    NS := 'S';
  end
  else
  begin
    NS := 'N';
  end;
  if ALong < 0 then
  begin
    WE := 'W';
  end
  else
  begin
    WE := 'E';
  end;
  Z := Trunc((ALong + 180) / 6) + 1;
  if WE = 'W' then
  begin
    M2 := (Z * 6) - 180;
    M2 := Abs(M2);
    M1 := M2 + 6;
  end
  else
  begin
    M2 := (Z * 6) - 180;
    M2 := Abs(M2);
    M1 := M2 - 6;
  end;
  Result := 'UTM-' + IntToStr(Z) + NS + ' (' + IntToStr(M1) + ' ' + WE + ' to ' + IntTostr(M2) + ' ' + WE + ')';
end;

procedure TVEForm.BirdsEye;
const
  BirdsEyeZoom = 18;
var
  s: AnsiString;
  TempHTML: TStringList;
  AFileName: AnsiString;
begin
  AFileName := OSInfo.TempDataDir + 'birdseye.htm';
  s := MemoHTML.Text;
  s := StringReplace(s, '{bingmapskey}', BingMapsKey_Aigner_Basic, [rfReplaceAll]);
  s := StringReplace(s, '{latitude}', StringReplace(FloatToStr(CY), ',', '.', [rfReplaceAll]), [rfReplaceAll]);
  s := StringReplace(s, '{longitude}', StringReplace(FloatToStr(CX), ',', '.', [rfReplaceAll]), [rfReplaceAll]);
  s := StringReplace(s, '{zoom}', IntToStr(BirdsEyeZoom), [rfReplaceAll]);
  TempHTML := TStringList.Create;
  try
    TempHTML.Text := s;
    TempHTML.SaveToFile(AFileName);
    if ShellExecute(0, 'open', PChar(AFileName), nil, nil, SW_SHOWMAXIMIZED) > 32 then
    begin
      UpdateUserTransactionCount(1, T_BROWSER);
    end;
  finally
    TempHTML.Free;
  end;
end;

{ TLoadThread }

procedure TLoadThread.AddUrl(AUrl: AnsiString);
begin
  ListSync.Enter;
  try
    if UrlList.IndexOf(AUrl) = -1 then
    begin
      UrlList.Add(AUrl);
      Loading := True;
    end;
  finally
    ListSync.Leave;
  end;
end;

procedure TLoadThread.LimitStack;
begin
  ListSync.Enter;
  try
    while UrlList.Count >= MAX_LOAD_COUNT do
    begin
      UrlList.Delete(0);
    end;
  finally
    ListSync.Leave;
  end;
end;

procedure TLoadThread.Execute;
var
  AUrl: AnsiString;
  AIndex: Integer;
  AFileSize: Integer;
begin
  while not Terminated do
  begin
    try
      while UrlList.Count > 0 do
      begin
        AIndex := -1;
        AUrl := '';
        if UrlList.Count > 0 then
        begin
          AIndex := UrlList.Count - 1;
          AUrl := UrlList[AIndex];
        end;
        if AUrl <> '' then
        begin
          AFileSize := GetTileFromUrl(AUrl);
          if DummyImageSize(AFileSize) then
          begin
            AUrl := AlternateUrl(AUrl);
            AFileSize := GetTileFromUrl(AUrl);
          end;
          if (AFileSize = 0) then
          begin
            DebugMsg('TLoadThread.Execute', 'Failed - ' + AUrl);
          end;
          if (AIndex > -1) and (AIndex < UrlList.Count) then
          begin
            ListSync.Enter;
            try
              UrlList.Delete(AIndex);
            finally
              ListSync.Leave;
            end;
          end;
          if (UrlList.Count = 0) then
          begin
            Loading := False;
          end;
        end;
      end;
    finally
      Sleep(THREAD_WAIT_INTERVAL);
    end;
  end;
end;

function TLoadThread.GetTileFromUrl(AUrl: AnsiString): Integer;
var
  Inet, hURL: HInternet;
  SizeBuffer: array[0..32] of Char;
  Dummy, SizeBufferSize: DWORD;
  FileSize: Integer;
  AFileName: AnsiString;
  ReadSize: DWORD;
begin
  Result := 0;
  Inet := InternetOpen(nil, INTERNET_OPEN_TYPE_PRECONFIG, nil, nil, 0);
  if Inet <> nil then
  begin
    hURL := InternetOpenURL(Inet, PChar(AUrl), nil, 0, INTERNET_FLAG_RELOAD, 0);
    if hURL <> nil then
    begin
      SizeBufferSize := SizeOf(SizeBuffer);
      Dummy := 0;
      if HttpQueryInfo(hURL, HTTP_QUERY_CONTENT_LENGTH, @SizeBuffer, SizeBufferSize, Dummy) then
      begin
        FileSize := StrToInt(Copy(SizeBuffer, 0, SizeBufferSize));
        if FileSize > 0 then
        begin
          InternetReadFile(hURL, @ImgBuffer, FileSize, ReadSize);
          if FileSize = ReadSize then
          begin
            Result := FileSize;
            if ((ImgBuffer[0] = Char($FF)) and (ImgBuffer[1] = Char($D8))) then
            begin
              AFileName := UrlToJpgFileName(AUrl);
              if not SaveJpg(@ImgBuffer, ReadSize, AFileName) then
              begin
                Result := 0;
              end;
            end
            else
              if ((ImgBuffer[0] = Char($89)) and (ImgBuffer[1] = Char('P')) and (ImgBuffer[2] = Char('N')) and (ImgBuffer[3] = Char('G'))) then
              begin
                AFileName := UrlToPngFileName(AUrl);
                if not SavePng(@ImgBuffer, ReadSize, AFileName) then
                begin
                  Result := 0;
                end;
              end;
          end;
        end;
      end;
      InternetCloseHandle(hURL);
    end;
    InternetCloseHandle(Inet);
  end;
end;

function TLoadThread.SaveJpg(ABuffer: Pointer; ABufferSize: Integer; AFilename: AnsiString): Boolean;
begin
  Result := False;
  MemStream.Clear;
  MemStream.Position := 0;
  MemStream.WriteBuffer(ABuffer^, ABufferSize);
  if CheckJpgData(MemStream) then
  begin
    FileSync.Enter;
    try
      MemStream.SaveToFile(AFilename);
      Result := FileExists(AFilename);
    finally
      FileSync.Leave;
    end;
  end;
end;

function TLoadThread.SavePng(ABuffer: Pointer; ABufferSize: Integer; AFilename: AnsiString): Boolean;
begin
  Result := False;
  MemStream.Clear;
  MemStream.Position := 0;
  MemStream.WriteBuffer(ABuffer^, ABufferSize);
  if CheckPngData(MemStream) then
  begin
    FileSync.Enter;
    try
      MemStream.SaveToFile(AFilename);
      Result := FileExists(AFilename);
    finally
      FileSync.Leave;
    end;
  end;
end;

function TLoadThread.CheckJpgData(AStream: TMemoryStream): Boolean;
var
  ABuffer: array[0..1] of Byte;
begin
  Result := False;
  try
    AStream.Position := 0;
    AStream.Read(ABuffer, 2);
    if (ABuffer[0] = $FF) and (ABuffer[1] = $D8) then
    begin
      AStream.Position := AStream.Size - 2;
      AStream.Read(ABuffer, 2);
      if (ABuffer[0] = $FF) and (ABuffer[1] = $D9) then
      begin
        Result := True;
      end;
    end;
  finally
    AStream.Position := 0;
  end;
end;

function TLoadThread.CheckPngData(AStream: TMemoryStream): Boolean;
var
  ABuffer: array[0..3] of Byte;
begin
  Result := False;
  try
    AStream.Position := 0;
    AStream.Read(ABuffer, 4);
    if (ABuffer[0] = $89) and (ABuffer[1] = $50) and (ABuffer[2] = $4E) and (ABuffer[3] = $47) then
    begin
      AStream.Position := AStream.Size - 4;
      AStream.Read(ABuffer, 4);
      if (ABuffer[0] = $AE) and (ABuffer[1] = $42) and (ABuffer[2] = $60) and (ABuffer[3] = $82) then
      begin
        Result := True;
      end;
    end;
  finally
    AStream.Position := 0;
  end;
end;

procedure TLoadThread.Initialize;
begin
  UrlList := TStringList.Create;
  MemStream := TMemoryStream.Create;
end;

procedure TLoadThread.Finalize;
begin
  UrlList.Clear;
  Terminate;
  WaitFor;
  UrlList.Free;
  MemStream.Free;
end;

end.

