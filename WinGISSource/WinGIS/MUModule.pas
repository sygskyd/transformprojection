unit MUModule;

interface

uses Windows, Forms, SysUtils, Classes, WinOSInfo, AM_Paint, AM_Layer, AM_Point, AM_Poly, AM_CPoly,
     AM_Circl, AM_Text, AM_Sym, AM_Font, AM_View, AM_Index, AM_Def, AM_Main, AM_Proj, MultiLng, WinInet, ToolsLib;

type
    TMUModule = class
    private
          LogLine: AnsiString;
          NoLog: Boolean;
          MlgSection: TMlgSection;
          MUDir: AnsiString;
          procedure CreateDirs;
          procedure ReadSettings;
          procedure WriteSettings;
          function GetProjectFileName(s: AnsiString): AnsiString;
          function StartLogLine: Boolean;
          procedure AddCol(s: AnsiString);
          procedure EndLogLine;
          procedure LogPixel(ALayer: PLayer; APixel: PPixel);
          procedure LogPolyline(ALayer: PLayer; APoly: PPoly);
          procedure LogArc(ALayer: PLayer; AArc: PEllipseArc);
          procedure LogCPoly(ALayer: PLayer; ACPoly: PCPoly);
          procedure LogCircle(ALayer: PLayer; AEllipse: PEllipse);
          procedure LogText(ALayer: PLayer; AText: PText);
          procedure LogSymbol(ALayer: PLayer; ASymbol: PSymbol);
          procedure LogObjectStyle(AItem: PIndex);
          procedure LogLineStyle(ALineStyle: PLineStyle);
          procedure LogFillStyle(AFillStyle: PFillStyle);
          procedure ProcessFile(AFileName: AnsiString; AFullSync: Boolean; ADel: Boolean);
          procedure UploadLogFile(AFileName: AnsiString);
          function UploadLogLine(s: AnsiString): Boolean;
          procedure DownloadSyncFile(AServerFileName,ALocalFileName: AnsiString);
          procedure ProcessLine(s: AnsiString; AFullSync: Boolean);
          procedure ProcessNewObject(ALayer: PLayer; AGUID: TGUID; AList: TStringList);
          procedure ProcessDelObject(ALayer: PLayer; AGUID: TGUID);
          procedure ProcessUpdObjectData(AGUID: TGUID; AList: TStringList);
          procedure ProcessUpdObjectStyle(ALayer: PLayer; AGUID: TGUID; AList: TStringList);
          procedure ProcessUpdLineStyle(ALineStyle: PLineStyle; AList: TStringList);
          procedure ProcessUpdFillStyle(AFillStyle: PFillStyle; AList: TStringList);
          procedure ProcessNewPoint(ALayer: PLayer; AGUID: TGUID; AList: TStringList);
          procedure ProcessNewLine(ALayer: PLayer; AGUID: TGUID; AList: TStringList);
          procedure ProcessNewArc(ALayer: PLayer; AGUID: TGUID; AList: TStringList);
          procedure ProcessNewArea(ALayer: PLayer; AGUID: TGUID; AList: TStringList);
          procedure ProcessNewCircle(ALayer: PLayer; AGUID: TGUID; AList: TStringList);
          procedure ProcessNewText(ALayer: PLayer; AGUID: TGUID; AList: TStringList);
          procedure ProcessNewSymbol(ALayer: PLayer; AGUID: TGUID; AList: TStringList);
          procedure ProcessUpdPointData(APoint: PPixel; AList: TStringList);
          procedure ProcessUpdLineData(APoly: PPoly; AList: TStringList);
          procedure ProcessUpdArcData(AArc: PEllipseArc; AList: TStringList);
          procedure ProcessUpdAreaData(ACPoly: PCPoly; AList: TStringList);
          procedure ProcessUpdCircleData(ACircle: PEllipse; AList: TStringList);
          procedure ProcessUpdTextData(AText: PText; AList: TStringList);
          procedure ProcessUpdSymbolData(ASymbol: PSymbol; AList: TStringList);
          procedure InsertObjectToLayer(ALayer: PLayer; AView: PView; AGUID: TGUID);
          procedure ShowError(s: AnsiString);
          function FileIsUpToDate(ALocalFileName: AnsiString): Boolean;
    public
          UserId: AnsiString;
          LastServerAddress: AnsiString;
          LoadingProjectUrl: AnsiString;
          constructor Create;
          destructor Free;
          function DownloadProject(AUrl: AnsiString; var AFileName: AnsiString): Boolean;
          procedure ApplyChanges(AFullSync: Boolean);
          procedure SendChanges;
          function CloseProject: Boolean;
          procedure LogNewObject(APInfo: PPaint; ALayer: PLayer; AItem: PIndex);
          procedure LogDelObject(APInfo: PPaint; ALayer: PLayer; AItem: PIndex);
          procedure LogUpdObjectData(APInfo: PPaint; ALayer: PLayer; AView: PView);
          procedure LogUpdObjectStyle(APInfo: PPaint; ALayer: PLayer; AItem: PIndex);
          procedure ReorganizeProject;
          procedure CheckReorganize;
    end;

var MU: TMUModule;

const LogFileExt = '.lcl';
const SyncFileExt = '.scl';

const RegKey = 'Software\PROGIS\MU';

implementation

uses ComObj, AM_Obj, AM_DPointColl, MUSyncDlg, Dialogs, FileCtrl, Registry, ActiveX, MenuFn;

const fiUserId = 0;
const fiLayerName = 1;
const fiOperation = 2;
const fiGUID = 3;
const fiStartData = 4;
const fiObjectType = 4;
const fiLineStyle = 4;
const fiLineWidth = 5;
const fiLineWidthType = 6;
const fiLineScale = 7;
const fiLineColor = 8;
const fiLineFillColor = 9;
const fiFillPattern = 10;
const fiFillForeColor = 11;
const fiFillBackColor = 12;
const fiFillScale = 13;
const fiFillScaleType = 14;
const fiPointX = 5;
const fiPointY = 6;
const fiARadius = 7;
const fiBRadius = 8;
const fiBeginAngle = 9;
const fiEndAngle = 10;
const fiIslandCount = 5;
const fiIslandInfo = 6;
const fiAreaPointCount = 6;
const fiAreaPointX = 7;
const fiAreaPointY = 8;
const fiLinePointCount = 5;
const fiLinePointX = 6;
const fiLinePointY = 7;
const fiTextAngle = 7;
const fiTextWidth = 8;
const fiTextFont = 9;
const fiTextFontStyle = 10;
const fiTextFontHeight = 11;
const fiTextText = 12;
const fiTextHeight = 13;
const fiTextNLines = 14;
const fiTextTrim = 15;
const fiTextFlags = 16;
const fiTextAdvMode = 17;
const fiTextBrkData = 18;
const fiTextPolyData = 19;
const fiTextIsAnnot = 20;
const fiTextIsOffset = 21;
const fiTextOffsetType = 22;
const fiTextAlign = 23;
const fiSymbolName = 7;
const fiSymbolLibName = 8;
const fiSymbolSize = 9;
const fiSymbolSizeType = 10;
const fiSymbolAngle = 11;

const ColumnSeparator = '|';

function SToF(s: AnsiString): Double;
begin
     try
        s:=StringReplace(s,'.',DecimalSeparator,[rfReplaceAll]);
        s:=StringReplace(s,',',DecimalSeparator,[rfReplaceAll]);
        Result:=StrToFloat(s);
     except
        Result:=0;
     end;
end;

function GetFileSize(AFileName: AnsiString): Integer;
var
FileStream: TFileStream;
begin
     if not FileExists(AFileName) then begin
        Result:=0;
        exit;
     end;
     FileStream:=TFileStream.Create(AFileName,fmOpenRead or fmShareDenyNone);
     try
        try
           Result:=FileStream.Size;
        except
           Result := 0;
        end;
     finally                                   
        FileStream.Free;
     end;
end;

function GetFileDate(AFileName: AnsiString): Double;
begin
     try
        Result:=FileDateToDateTime(FileAge(AFileName));
     except
        Result:=0;
     end;
end;

function CreateUserId: AnsiString;
var
AGUID: TGUID;
begin
     Result:='';
     CoCreateGuid(AGUID);
     Result:=IntToHex(AGUID.D2,4) + IntToHex(AGUID.D3,4);
end;

{ TMUModule }

constructor TMUModule.Create;
begin
     NoLog:=False;
     MUDir:='';
     LastServerAddress:='';
     LoadingProjectUrl:='';
     UserId:='';
     MlgSection:=TMlgSection.Create(nil);
     MlgSection.Section:='Multiuser';
     ReadSettings;
end;

destructor TMUModule.Free;
begin
     WriteSettings;
     MlgSection.Free;
end;

procedure TMUModule.ReadSettings;
var
Reg: TRegistry;
begin
     Reg:=TRegistry.Create;
     try
        Reg.RootKey:=HKEY_CURRENT_USER;
        if Reg.OpenKeyReadOnly(RegKey) then begin
           if Reg.ValueExists('ServerURL') then begin
              LastServerAddress:=Reg.ReadString('ServerURL');
           end;
           if Reg.ValueExists('ID') then begin
              UserId:=Reg.ReadString('ID');
           end;
        end;
     finally
        Reg.Free;
     end;
     if UserId = '' then begin
        UserId:=CreateUserId;
     end;
end;

procedure TMUModule.WriteSettings;
var
Reg: TRegistry;
begin
     Reg:=TRegistry.Create;
     try
        if Reg.KeyExists(RegKey) then begin
           Reg.DeleteKey(RegKey);
        end;
        if Reg.OpenKey(RegKey,True) then begin
           Reg.WriteString('ServerURL',LastServerAddress);
           Reg.WriteString('ID',UserId);
           Reg.CloseKey;
        end;
     finally
        Reg.Free;
     end;
end;

function TMUModule.CloseProject: Boolean;
var
DlgRes: Integer;
begin
     DlgRes:=MessageBox(WingisMainForm.Handle,PChar(MlgSection[9]),PChar(Application.Title),MB_ICONQUESTION or MB_APPLMODAL or MB_YESNOCANCEL);
     Result:=DlgRes <> ID_CANCEL;
     if DlgRes = ID_YES then begin
        SendChanges;
     end;
end;

function TMUModule.DownloadProject(AUrl: AnsiString; var AFileName: AnsiString): Boolean;
var
Inet,hURL: HInternet;
QueryBuffer: Array[0..31] of Char;
Dummy,QueryBufferSize: DWORD;
ProjectSize,LocalSize: Integer;
TempFileName: AnsiString;
ProjFileName: AnsiString;
AStream: TFileStream;
AFileOnServer: Boolean;
HttpRes: AnsiString;
Buffer: Array[0..4095] of Char;
BufferSize: DWORD;
begin
     Result:=False;
     CreateDirs;
     if not (Pos('http://',AUrl) = 1) then begin
        AFileName:=AUrl;
        Result:=True;
        exit;
     end;
     LocalSize:=0;
     AFileOnServer:=False;
     Inet:=InternetOpen(nil,INTERNET_OPEN_TYPE_PRECONFIG,nil,nil,0);
     if Inet <> nil then begin
        hURL:=InternetOpenURL(Inet,PChar(AUrl),nil,0,INTERNET_FLAG_RELOAD,0);
        if hURL <> nil then begin
           QueryBufferSize:=Sizeof(QueryBuffer);
           ZeroMemory(@QueryBuffer,QueryBufferSize);
           Dummy:=0;
           if HttpQueryInfo(hURL,HTTP_QUERY_STATUS_CODE,@QueryBuffer,QueryBufferSize,Dummy) then begin
              HttpRes:=StrPas(QueryBuffer);
              AFileOnServer:=(HttpRes = '200');
              if not AFileOnServer then begin
                 ShowError(MlgSection[7] + HttpRes);
              end;
           end
           else begin
                ShowError(MlgSection[7] + IntToStr(GetLastError));
           end;
           QueryBufferSize:=Sizeof(QueryBuffer);
           ZeroMemory(@QueryBuffer,QueryBufferSize);
           Dummy:=0;
           if (AFileOnServer) and (HttpQueryInfo(hURL,HTTP_QUERY_CONTENT_LENGTH,@QueryBuffer,QueryBufferSize,Dummy)) then begin
              ProjectSize:=StrToInt(StrPas(QueryBuffer));
              if ProjectSize > 0 then begin
                 ProjFileName:=GetProjectFileName(AUrl);
                 TempFileName:=MUDir + ProjFileName + '.tmp';
                 AFileName:=MUDir + ProjFileName;
                 if FileExists(TempFileName) then begin
                    DeleteFile(TempFileName);
                 end;
                 if FileExists(AFileName) then begin
                    LocalSize:=GetFileSize(AFileName);
                 end;
                 if (LocalSize = ProjectSize) and (FileIsUpToDate(AFileName)) then begin
                    Result:=True;
                 end
                 else begin
                    FileClose(FileCreate(TempFileName));
                    AStream:=TFileStream.Create(TempFileName,fmOpenWrite);
                    try
                       MUSyncForm.Display(MlgSection[13]);
                       MUSyncForm.ProgressBar.Max:=ProjectSize;
                       repeat
                             InternetReadFile(hURL,@Buffer,Sizeof(Buffer),BufferSize);
                             AStream.Write(Buffer,BufferSize);
                             MUSyncForm.ProgressBar.StepBy(BufferSize);
                       until BufferSize = 0;
                    finally
                       AStream.Free;
                       MUSyncForm.Close;
                    end;
                    if FileExists(AFileName) then begin
                       DeleteFile(AFileName);
                    end;
                    RenameFile(TempFileName,AFileName);
                    Result:=True;
                 end;
              end;
              InternetCloseHandle(hURL);
           end;
        end
        else begin
           ShowError(MlgSection[7] + IntToStr(GetLastError));
        end;
        InternetCloseHandle(Inet);
     end
     else begin
        ShowError(MlgSection[7] + IntToStr(GetLastError));
     end;
end;

procedure TMUModule.CreateDirs;
var
ADir: AnsiString;
begin
     ADir:=OSInfo.CommonAppDataDir + 'MU\';
     if not DirectoryExists(ADir) then begin
        CreateDir(ADir);
     end;
     if DirectoryExists(ADir) then begin
        MUDir:=ADir;
     end;
end;

function TMUModule.GetProjectFileName(s: AnsiString): AnsiString;
var
i,n: Integer;
APos: Integer;
begin
     APos:=1;
     n:=StrLen(PChar(s));
     for i:=1 to n do begin
         if s[i] = '/' then begin
            APos:=i+1;
         end;
     end;
     Result:=Copy(s,APos,n-APos+1);
end;

function TMUModule.StartLogLine: Boolean;
begin
     LogLine:=UserId;
     Result:=WingisMainForm.ActualChild.Data^.PInfo^.IsMU;
end;

procedure TMUModule.EndLogLine;
var
F: TextFile;
AFileName: AnsiString;
begin
     AFileName:=ChangeFileExt(WingisMainForm.ActualChild.Data^.FName,LogFileExt);
     if not FileExists(AFileName) then begin
        FileClose(FileCreate(AFileName));
     end;
     if FileExists(AFileName) then begin
        AssignFile(F,AFileName);
        Append(F);
        WriteLn(F,LogLine);
        CloseFile(F);
     end;
end;

procedure TMUModule.AddCol(s: AnsiString);
begin
     if LogLine <> '' then begin
        s:=ColumnSeparator + s;
     end;
     LogLine:=LogLine + s;
end;

procedure TMUModule.LogNewObject(APInfo: PPaint; ALayer: PLayer; AItem: PIndex);
var
AView: PView;
begin
     if APInfo = nil then begin
        APInfo:=WingisMainForm.ActualChild.Data^.PInfo;
     end;
     if ALayer = nil then begin
        ALayer:=WingisMainForm.ActualChild.Data^.Layers^.TopLayer;
     end;
     if (not APInfo^.IsMU) or (NoLog) then begin
        exit;
     end;
     AView:=PLayer(APInfo^.Objects)^.IndexObject(APInfo,AItem);
     if (AView <> nil) and (ALayer <> nil) and (StartLogLine) then begin
        AddCol(ALayer^.Text^);
        AddCol('new');
        AddCol(GuidToString(AView^.GUID));
        case AView^.GetObjType of
             ot_Pixel: LogPixel(ALayer,PPixel(AView));
             ot_Poly: LogPolyline(ALayer,PPoly(AView));
             ot_Arc: LogArc(ALayer,PEllipseArc(AView));
             ot_CPoly: LogCPoly(ALayer,PCPoly(AView));
             ot_Circle: LogCircle(ALayer,PEllipse(AView));
             ot_Text: LogText(ALayer,PText(AView));
             ot_Symbol: LogSymbol(ALayer,PSymbol(AView));
        end;
        EndLogLine;
     end;
end;

procedure TMUModule.LogDelObject(APInfo: PPaint; ALayer: PLayer; AItem: PIndex);
var
AView: PView;
begin
     if APInfo = nil then begin
        APInfo:=WingisMainForm.ActualChild.Data^.PInfo;
     end;
     if ALayer = nil then begin
        ALayer:=WingisMainForm.ActualChild.Data^.Layers^.TopLayer;
     end;
     if (not APInfo^.IsMU) or (NoLog) then begin
        exit;
     end;
     AView:=PLayer(APInfo^.Objects)^.IndexObject(APInfo,AItem);
     if (AView <> nil) and (ALayer <> nil) and (StartLogLine) then begin
        AddCol(ALayer^.Text^);
        AddCol('del');
        AddCol(GuidToString(AView^.GUID));
        EndLogLine;
     end;
end;

procedure TMUModule.LogUpdObjectData(APInfo: PPaint; ALayer: PLayer; AView: PView);
begin
     if APInfo = nil then begin
        APInfo:=WingisMainForm.ActualChild.Data^.PInfo;
     end;
     if ALayer = nil then begin
        ALayer:=WingisMainForm.ActualChild.Data^.Layers^.TopLayer;
     end;
     if (not APInfo^.IsMU) or (NoLog) then begin
        exit;
     end;
     if (AView <> nil) and (ALayer <> nil) and (StartLogLine) then begin
        AddCol(ALayer^.Text^);
        AddCol('upddata');
        AddCol(GuidToString(AView^.GUID));
        case AView^.GetObjType of
             ot_Pixel: LogPixel(ALayer,PPixel(AView));
             ot_Poly: LogPolyline(ALayer,PPoly(AView));
             ot_Arc: LogArc(ALayer,PEllipseArc(AView));
             ot_CPoly: LogCPoly(ALayer,PCPoly(AView));
             ot_Circle: LogCircle(ALayer,PEllipse(AView));
             ot_Text: LogText(ALayer,PText(AView));
             ot_Symbol: LogSymbol(ALayer,PSymbol(AView));
        end;
        EndLogLine;
     end;
end;

procedure TMUModule.LogUpdObjectStyle(APInfo: PPaint; ALayer: PLayer; AItem: PIndex);
var
AView: PView;
AIndex: PIndex;
begin
     if APInfo = nil then begin
        APInfo:=WingisMainForm.ActualChild.Data^.PInfo;
     end;
     if ALayer = nil then begin
        ALayer:=WingisMainForm.ActualChild.Data^.Layers^.TopLayer;
     end;
     if (not APInfo^.IsMU) or (NoLog) then begin
        exit;
     end;
     AIndex:=nil;
     if (AItem <> nil) and (ALayer <> nil) and (StartLogLine) then begin
        AIndex:=ALayer.HasIndexObject(AItem^.Index);
     end;
     if AIndex <> nil then begin
        AView:=PLayer(APInfo^.Objects)^.IndexObject(APInfo,AItem);
        if AView <> nil then begin
           AddCol(ALayer^.Text^);
           AddCol('updstyle');
           AddCol(GuidToString(AView^.GUID));
           LogObjectStyle(AIndex);
           EndLogLine;
        end;
     end;
end;

procedure TMUModule.LogPixel(ALayer: PLayer; APixel: PPixel);
begin
     AddCol('point');
     AddCol(IntToStr(APixel^.Position.X));
     AddCol(IntToStr(APixel^.Position.Y));
end;

procedure TMUModule.LogPolyline(ALayer: PLayer; APoly: PPoly);
var
i,n: Integer;
P: PDPoint;
begin
     AddCol('line');
     n:=APoly^.Data^.Count;
     AddCol(IntToStr(n));
     for i:=0 to n-1 do begin
         P:=APoly^.Data^.At(i);
         AddCol(IntToStr(P^.X));
         AddCol(IntToStr(P^.Y));
     end;
end;

procedure TMUModule.LogArc(ALayer: PLayer; AArc: PEllipseArc);
begin
     AddCol('arc');
     AddCol(IntToStr(AArc^.Position.X));
     AddCol(IntToStr(AArc^.Position.Y));
     AddCol(IntToStr(AArc^.PrimaryAxis));
     AddCol(IntToStr(AArc^.SecondaryAxis));
     AddCol(FloatToStr(AArc^.BeginAngle));
     AddCol(FloatToStr(AArc^.EndAngle));
end;

procedure TMUModule.LogCPoly(ALayer: PLayer; ACPoly: PCPoly);
var
i,n: Integer;
P: PDPoint;
begin
     AddCol('area');
     AddCol(IntToStr(ACPoly^.IslandCount));
     if ACPoly^.IslandCount > 0 then begin
        for i:=0 to ACPoly^.IslandCount-1 do begin
            AddCol(IntToStr(ACPoly^.IslandInfo^[i]));
        end;
     end;
     n:=ACPoly^.Data^.Count;
     AddCol(IntToStr(n));
     for i:=0 to n-1 do begin
         P:=ACPoly^.Data^.At(i);
         AddCol(IntToStr(P^.X));
         AddCol(IntToStr(P^.Y));
     end;
end;

procedure TMUModule.LogCircle(ALayer: PLayer; AEllipse: PEllipse);
begin
     AddCol('circle');
     AddCol(IntToStr(AEllipse^.Position.X));
     AddCol(IntToStr(AEllipse^.Position.Y));
     AddCol(IntToStr(AEllipse^.PrimaryAxis));
     AddCol(IntToStr(AEllipse^.SecondaryAxis));
end;

procedure TMUModule.LogText(ALayer: PLayer; AText: PText);
begin
     AddCol('text');
     AddCol(IntToStr(AText^.Pos.X));
     AddCol(IntToStr(AText^.Pos.Y));
     AddCol(IntToStr(AText^.Angle));
     AddCol(IntToStr(AText^.Width));
     AddCol(IntToStr(AText^.Font.Font));
     AddCol(IntToStr(AText^.Font.Style));
     AddCol(IntToStr(AText^.Font.Height));
     AddCol(AText^.Text^);
     AddCol(IntToStr(AText^.Height));
     AddCol(IntToStr(AText^.nLines));
     AddCol(IntToStr(AText^.fTrim));
     AddCol(IntToStr(AText^.fFlagSt));
     AddCol('0'); //fAdvMode
     AddCol('0'); //TxtBrkData
     AddCol('0'); //TxtPolyData
     AddCol(IntToStr(Ord(AText^.IsItAnnot)));
     AddCol(IntToStr(Ord(AText^.IsUseOffset)));
     AddCol(IntToStr(AText^.ObjectOffsetType));
     AddCol(IntToStr(AText^.FAlign));
end;

procedure TMUModule.LogSymbol(ALayer: PLayer; ASymbol: PSymbol);
begin
     AddCol('symbol');
     AddCol(IntToStr(ASymbol^.Position.X));
     AddCol(IntToStr(ASymbol^.Position.Y));
     AddCol(ASymbol^.SymPtr^.GetName);
     AddCol(ASymbol^.SymPtr^.GetLibName);
     AddCol(FloatToStr(ASymbol^.Size));
     AddCol(IntToStr(ASymbol^.SizeType));
     AddCol(FloatToStr(ASymbol^.Angle));
end;

procedure TMUModule.LogObjectStyle(AItem: PIndex);
begin
     if AItem^.ObjectStyle <> nil then begin
        if AItem^.HasLineStyle then begin
           LogLineStyle(@AItem^.ObjectStyle^.LineStyle);
        end;
        if AItem^.HasPattern then begin
           LogFillStyle(@AItem^.ObjectStyle^.FillStyle);
        end;
        if AItem^.GetObjType = ot_Text then begin
           LogLineStyle(@AItem^.ObjectStyle^.LineStyle);
           LogFillStyle(@AItem^.ObjectStyle^.FillStyle);
        end;
     end;
end;

procedure TMUModule.LogLineStyle(ALineStyle: PLineStyle);
begin
     AddCol(IntToStr(ALineStyle^.Style));
     AddCol(FloatToStr(ALineStyle^.Width));
     AddCol(IntToStr(ALineStyle^.WidthType));
     AddCol(FloatToStr(ALineStyle^.Scale));
     AddCol(IntToStr(ALineStyle^.Color));
     AddCol(IntToStr(ALineStyle^.FillColor));
end;

procedure TMUModule.LogFillStyle(AFillStyle: PFillStyle);
begin
     AddCol(IntToStr(AFillStyle^.Pattern));
     AddCol(IntToStr(AFillStyle^.ForeColor));
     AddCol(IntToStr(AFillStyle^.BackColor));
     AddCol(FloatToStr(AFillStyle^.Scale));
     AddCol(IntToStr(AFillStyle^.ScaleType));
end;

procedure TMUModule.ApplyChanges(AFullSync: Boolean);
var
ALocalSyncFileName: AnsiString;
AServerSyncFileName: AnsiString;
ALogFileName: AnsiString;
AOnline: Boolean;
begin
     /////
     NoLog:=True;
     try
        ALocalSyncFileName:=ChangeFileExt(WingisMainForm.ActualChild.Data^.FName,SyncFileExt);
        AServerSyncFileName:=StringReplace(WingisMainForm.ActualChild.Data^.PInfo^.MUProjectAddress,'.amp',SyncFileExt,[rfIgnoreCase]);
        ALogFileName:=ChangeFileExt(WingisMainForm.ActualChild.Data^.FName,LogFileExt);
        AOnline:=(Pos('http://',AServerSyncFileName) = 1);
        if FileExists(ALogFileName) then begin
           ProcessFile(ALogFileName,True,False);
           WingisMainForm.ActualChild.Data^.SetModified;
        end;
        if AOnline then begin
           DownloadSyncFile(AServerSyncFileName,ALocalSyncFileName);
        end;
        if FileExists(ALocalSyncFileName) then begin
           ProcessFile(ALocalSyncFileName,AFullSync,False);
        end;
     finally
        NoLog:=False;
     end;
end;

procedure TMUModule.SendChanges;
var
ALogFileName: AnsiString;
begin
     ALogFileName:=ChangeFileExt(WingisMainForm.ActualChild.Data^.FName,LogFileExt);
     UploadLogFile(ALogFileName);
end;

procedure TMUModule.DownloadSyncFile(AServerFileName,ALocalFileName: AnsiString);
var
Inet,hURL: HInternet;
QueryBuffer: Array[0..31] of Char;
QueryBufferSize: DWORD;
Dummy: DWORD;
FileSize: Integer;
TempFileName: AnsiString;
AStream: TFileStream;
AFileOnServer: Boolean;
HttpRes: AnsiString;
Buffer: Array[0..4095] of Char;
BufferSize: DWORD;
LocalFileSize: Integer;
begin
     AFileOnServer:=False;
     HttpRes:='';
     Inet:=InternetOpen(nil,INTERNET_OPEN_TYPE_PRECONFIG,nil,nil,0);
     if Inet <> nil then begin
        hURL:=InternetOpenURL(Inet,PChar(AServerFileName),nil,0,INTERNET_FLAG_RELOAD,0);
        if hURL <> nil then begin
           QueryBufferSize:=Sizeof(QueryBuffer);
           ZeroMemory(@QueryBuffer,QueryBufferSize);
           Dummy:=0;
           if HttpQueryInfo(hURL,HTTP_QUERY_STATUS_CODE,@QueryBuffer,QueryBufferSize,Dummy) then begin
              HttpRes:=StrPas(QueryBuffer);
              AFileOnServer:=(HttpRes = '200');
           end
           else begin
                ShowError(MlgSection[7] + IntToStr(GetLastError));
           end;
           QueryBufferSize:=Sizeof(QueryBuffer);
           ZeroMemory(@QueryBuffer,QueryBufferSize);
           Dummy:=0;
           if HttpRes = '404' then begin
              DeleteFile(ALocalFileName);
           end;
           if (AFileOnServer) and (HttpQueryInfo(hURL,HTTP_QUERY_CONTENT_LENGTH,@QueryBuffer,QueryBufferSize,Dummy)) then begin
              FileSize:=StrToInt(StrPas(QueryBuffer));
              LocalFileSize:=GetFileSize(ALocalFileName);
              if (FileSize > 0) and (FileSize <> LocalFileSize) then begin
                 DeleteFile(ALocalFileName);
                 TempFileName:=ALocalFileName + '.tmp';
                 if FileExists(TempFileName) then begin
                    DeleteFile(TempFileName);
                 end;
                 FileClose(FileCreate(TempFileName));
                 AStream:=TFileStream.Create(TempFileName,fmOpenWrite);
                 try
                    MUSyncForm.Display(MlgSection[10]);
                    MUSyncForm.ProgressBar.Max:=FileSize;
                    repeat
                          InternetReadFile(hURL,@Buffer,Sizeof(Buffer),BufferSize);
                          AStream.Write(Buffer,BufferSize);
                          MUSyncForm.ProgressBar.StepBy(BufferSize);
                    until BufferSize = 0;
                 finally
                    AStream.Free;
                    MUSyncForm.Close;
                 end;
                 RenameFile(TempFileName,ALocalFileName);
              end;
              InternetCloseHandle(hURL);
           end;
        end
        else begin
           ShowError(MlgSection[7] + IntToStr(GetLastError));
        end;
        InternetCloseHandle(Inet);
     end
     else begin
        ShowError(MlgSection[7] + IntToStr(GetLastError));
     end;
end;

procedure TMUModule.UploadLogFile(AFileName: AnsiString);
var
AList: TStringList;
begin
     if FileExists(AFileName) then begin
        AList:=TStringList.Create;
        try
           AList.LoadFromFile(AFileName);
           if AList.Count > 0 then begin
              try
                 MUSyncForm.Display(MlgSection[11]);
                 MUSyncForm.ProgressBar.Max:=AList.Count;
                 while (AList.Count > 0) and (UploadLogLine(AList[0])) do begin
                       AList.Delete(0);
                       MUSyncForm.ProgressBar.StepIt;
                 end;
              finally
                 MUSyncForm.Close;
              end;
           end;
           if AList.Count > 0 then begin
              AList.SaveToFile(AFileName);
           end
           else begin
              DeleteFile(AFileName);
              WingisMainForm.ActualChild.Data^.Modified:=False;
           end;
        finally
           AList.Free;
        end;
     end;
end;

function TMUModule.UploadLogLine(s: AnsiString): Boolean;
const
AAccept: Packed Array[0..1] of LPSTR = (PChar('*/*'), nil);
AHeader: AnsiString = 'Content-Type: application/x-www-form-urlencoded';
var
hInet,hCon,hReq: HInternet;
QueryBuffer: Array[0..63] of Char;
QueryBufferSize: DWORD;
AUrl,AHost,AQuery: AnsiString;
ASyncFileName: AnsiString;
i: Integer;
ARes: AnsiString;
begin
     Result:=False;
     s:=Trim(s);
     if s = '' then begin
        Result:=True;
        exit;
     end;
     AUrl:=WingisMainForm.ActualChild.Data^.PInfo^.MUServerAddress;
     if AUrl = '' then begin
        AUrl:=LastServerAddress;
     end;
     if AUrl = '' then begin
        Result:=False;
        exit;
     end;
     ASyncFileName:=ChangeFileExt(ExtractFileName(WingisMainForm.ActualChild.Data^.FName),SyncFileExt);
     hInet:=InternetOpen(nil,INTERNET_OPEN_TYPE_PRECONFIG,nil,nil,0);
     if hInet <> nil then begin
        AUrl:=StringReplace(AUrl,'http://','',[rfIgnoreCase]);
        i:=Pos('/',AUrl);
        AHost:=Copy(AUrl,1,i-1);
        AQuery:=Copy(AUrl,i,Length(AUrl)-(i-1));
        AQuery:=AQuery + '?op=newlog&syncfile=' + ASyncFileName;
        hCon:=InternetConnect(hInet,PChar(AHost),0,nil,nil,INTERNET_SERVICE_HTTP,0,0);
        if hCon <> nil then begin
           hReq:=HttpOpenRequest(hCon,'POST',PChar(AQuery),nil,nil,@AAccept,INTERNET_FLAG_RELOAD,0);
           if hReq <> nil then begin
              if HttpSendRequest(hReq,PChar(AHeader),StrLen(PChar(AHeader)),PChar(s),StrLen(PChar(s))) then begin
                 QueryBufferSize:=Sizeof(QueryBuffer);
                 ZeroMemory(@QueryBuffer,QueryBufferSize);
                 repeat
                       InternetReadFile(hReq,@QueryBuffer,Sizeof(QueryBuffer),QueryBufferSize);
                 until QueryBufferSize = 0;
                 ARes:=StrPas(QueryBuffer);
                 if ARes = 'RES_OK' then begin
                    Result:=True;            
                 end
                 else begin
                    ShowError(MlgSection[7] + ARes);
                 end;
              end
              else begin
                 ShowError(MlgSection[7] + IntToStr(GetLastError));
              end;
              InternetCloseHandle(hReq);
           end
           else begin
              ShowError(MlgSection[7] + IntToStr(GetLastError));
           end;
           InternetCloseHandle(hCon);
        end
        else begin
           ShowError(MlgSection[7] + IntToStr(GetLastError));
        end;
        InternetCloseHandle(hInet);
     end
     else begin
        ShowError(MlgSection[7] + IntToStr(GetLastError));
     end;
end;

procedure TMUModule.ProcessFile(AFileName: AnsiString; AFullSync: Boolean; ADel: Boolean);
var
i: Integer;
FullList: TStringList;
begin
     FullList:=TStringList.Create;
     try
        MUSyncForm.Display(MlgSection[12]);
        FullList.LoadFromFile(AFileName);
        MUSyncForm.ProgressBar.Max:=FullList.Count;
        for i:=0 to FullList.Count-1 do begin
            ProcessLine(FullList[i],AFullSync);
            MUSyncForm.ProgressBar.StepIt;
        end;
     finally
        FullList.Free;
        if ADel then begin
           DeleteFile(AFileName);
        end;
        MUSyncForm.Close;
     end;
     WingisMainForm.ActualChild.Data^.PInfo^.RedrawScreen(True);
end;

procedure TMUModule.ProcessLine(s: AnsiString; AFullSync: Boolean);
var
AList: TStringList;
ALayerName: AnsiString;
ALayer: PLayer;
Op: AnsiString;
GuidString: AnsiString;
GUID: TGUID;
ValuesOK: Boolean;
begin
     ValuesOK:=True;
     AList:=StringToList(s,'|');
     try
        if (AList.Count >= fiStartData) and ((AFullSync) or (AList[fiUSerId] <> UserId)) then begin
           ALayerName:=Trim(AList[fiLayerName]);
           ALayer:=WinGISMainForm.ActualChild.Data^.Layers^.NameToLayer(ALayerName);
           if ALayer = nil then begin
              ValuesOK:=False;
           end;
           Op:=Trim(AList[fiOperation]);
           if Op = '' then begin
              ValuesOK:=False;
           end;
           GuidString:=Trim(AList[fiGUID]);
           if GuidString = '' then begin
              ValuesOK:=False;
           end
           else begin
              ResetGUID(GUID);
              try
                 GUID:=StringToGuid(GuidString);
              finally
                 if GUIDIsNull(GUID) then begin
                    ValuesOK:=False;
                 end;
              end;
           end;
           if ValuesOK then begin
              if Op = 'new' then begin
                 ProcessNewObject(ALayer,GUID,AList);
              end
              else if Op = 'del' then begin
                 ProcessDelObject(ALayer,GUID);
              end
              else if Op = 'upddata' then begin
                 ProcessUpdObjectData(GUID,AList);
              end
              else if Op = 'updstyle' then begin
                 ProcessUpdObjectStyle(ALayer,GUID,AList);
              end;
           end;
        end;
     finally
        AList.Free;
     end;
end;

procedure TMUModule.ProcessNewObject(ALayer: PLayer; AGUID: TGUID; AList: TStringList);
var
ObjTypeName: AnsiString;
begin
     if ALayer^.HasGUID(WingisMainForm.ActualChild.Data^.PInfo,AGUID) then begin
        exit;
     end;
     ObjTypeName:=Trim(AList[fiObjectType]);
     if ObjTypeName = 'point' then begin
        ProcessNewPoint(ALayer,AGUID,AList);
     end
     else if ObjTypeName = 'line' then begin
        ProcessNewLine(ALayer,AGUID,AList);
     end
     else if ObjTypeName = 'arc' then begin
        ProcessNewArc(ALayer,AGUID,AList);
     end
     else if ObjTypeName = 'area' then begin
        ProcessNewArea(ALayer,AGUID,AList);
     end
     else if ObjTypeName = 'circle' then begin
        ProcessNewCircle(ALayer,AGUID,AList);
     end
     else if ObjTypeName = 'text' then begin
        ProcessNewText(ALayer,AGUID,AList);
     end
     else if ObjTypeName = 'symbol' then begin
        ProcessNewSymbol(ALayer,AGUID,AList);     
     end;
end;

procedure TMUModule.ProcessDelObject(ALayer: PLayer; AGUID: TGUID);
var
AItem: PIndex;
AView: PView;
AIndex: Integer;
AData: PProj;
begin
     AData:=WingisMainForm.ActualChild.Data;
     AIndex:=AData^.GUIDToIndex(AGUID);
     if AIndex <> 0 then begin
        AItem:=New(PIndex,Init(AIndex));
        try
           AView:=PLayer(ALayer)^.IndexObject(PProj(AData)^.PInfo,AItem);
           if AView <> nil then begin
              AView^.Invalidate(PProj(AData)^.PInfo);
              PProj(AData)^.MultiMedia^.DelMediasAtDel(AItem^.Index);
              PLayer(ALayer)^.DeleteIndex(PProj(AData)^.PInfo,AItem);
              if PProj(AData)^.Layers^.ExistsObject(AItem) = 0 then begin
                 PObjects(PProj(AData)^.PInfo^.Objects)^.DeleteIndex(PProj(AData)^.PInfo,AItem);
              end;
           end;
        finally
           Dispose(AItem,Done);
        end;
     end;
end;

procedure TMUModule.ProcessUpdObjectData(AGUID: TGUID; AList: TStringList);
var
AItem: PIndex;
AView: PView;
AIndex: Integer;
AData: PProj;
ObjTypeName: AnsiString;
begin
     AView:=nil;
     ObjTypeName:=Trim(AList[fiObjectType]);
     AData:=WingisMainForm.ActualChild.Data;
     AIndex:=AData^.GUIDToIndex(AGUID);
     if AIndex <> 0 then begin
        AItem:=New(PIndex,Init(AIndex));
        try
           AView:=PLayer(AData^.PInfo^.Objects)^.IndexObject(PProj(AData)^.PInfo,AItem);
        finally
           Dispose(AItem,Done);
        end;
     end;
     if AView <> nil then begin
        if ObjTypeName = 'point' then begin
           ProcessUpdPointData(PPixel(AView),AList);
        end
        else if ObjTypeName = 'line' then begin
           ProcessUpdLineData(PPoly(AView),AList);
        end
        else if ObjTypeName = 'arc' then begin
           ProcessUpdArcData(PEllipseArc(AView),AList);
        end
        else if ObjTypeName = 'area' then begin
           ProcessUpdAreaData(PCPoly(AView),AList);
        end
        else if ObjTypeName = 'circle' then begin
           ProcessUpdCircleData(PEllipse(AView),AList);
        end
        else if ObjTypeName = 'text' then begin
           ProcessUpdTextData(PText(AView),AList);
        end
        else if ObjTypeName = 'symbol' then begin
           ProcessUpdSymbolData(PSymbol(AView),AList);
        end;
        AData^.UpdateCliprect(AView);
     end;
end;

procedure TMUModule.ProcessUpdObjectStyle(ALayer: PLayer; AGUID: TGUID; AList: TStringList);
var
AItem: PIndex;
AIndex: Integer;
AData: PProj;
begin
     AItem:=nil;
     AData:=WingisMainForm.ActualChild.Data;
     AIndex:=AData^.GUIDToIndex(AGUID);
     if AIndex <> 0 then begin
        AItem:=ALayer^.HasIndexObject(AIndex);
     end;
     if AItem <> nil then begin
        if AList.Count > fiStartData then begin
           if AItem^.ObjectStyle = nil then begin
              AItem^.ObjectStyle:=New(PObjStyleV2);
              AItem^.ObjectStyle.SymbolFill.FillType:=symfNone;
           end;
           if AItem^.ObjectStyle <> nil then begin
              if (AItem^.HasLineStyle) and (AList.Count > fiLineFillColor) then begin
                 ProcessUpdLineStyle(@AItem^.ObjectStyle^.LineStyle,AList);
                 AItem^.SetState(sf_OtherStyle,True);
              end;
              if (AItem^.HasPattern) and (AList.Count > fiFillScaleType) then begin
                 ProcessUpdFillStyle(@AItem^.ObjectStyle^.FillStyle,AList);
                 AItem^.SetState(sf_OtherStyle,True);
              end;
              if (AItem^.GetObjType = ot_Text) and (AList.Count > fiFillScaleType) then begin
                 ProcessUpdLineStyle(@AItem^.ObjectStyle^.LineStyle,AList);
                 ProcessUpdFillStyle(@AItem^.ObjectStyle^.FillStyle,AList);
                 AItem^.SetState(sf_OtherStyle,True);
              end;
           end;
        end
        else begin
           if AItem^.ObjectStyle <> nil then begin
              Dispose(AItem^.ObjectStyle);
              AItem^.ObjectStyle:=nil;
              AItem^.SetState(sf_OtherStyle,False);
           end;
        end;
     end;
end;

procedure TMUModule.ProcessUpdLineStyle(ALineStyle: PLineStyle; AList: TStringList);
begin
     ALineStyle^.Style:=StrToInt(Trim(AList[fiLineStyle]));
     ALineStyle^.Width:=SToF(Trim(AList[fiLineWidth]));
     ALineStyle^.WidthType:=StrToInt(Trim(AList[fiLineWidthType]));
     ALineStyle^.Scale:=SToF(Trim(AList[fiLineScale]));
     ALineStyle^.Color:=StrToInt(Trim(AList[fiLineColor]));
     ALineStyle^.FillColor:=StrToInt(Trim(AList[fiLineFillColor]));
end;

procedure TMUModule.ProcessUpdFillStyle(AFillStyle: PFillStyle; AList: TStringList);
begin
     AFillStyle^.Pattern:=StrToInt(Trim(AList[fiFillPattern]));
     AFillStyle^.ForeColor:=StrToInt(Trim(AList[fiFillForeColor]));
     AFillStyle^.BackColor:=StrToInt(Trim(AList[fiFillBackColor]));
     AFillStyle^.Scale:=SToF(Trim(AList[fiFillScale]));
     AFillStyle^.ScaleType:=StrToInt(Trim(AList[fiFillScaleType]));
end;

procedure TMUModule.ProcessNewPoint(ALayer: PLayer; AGUID: TGUID; AList: TStringList);
var
APoint: PPixel;
APos: TDPoint;
X,Y: Integer;
begin
     X:=StrToInt(Trim(AList[fiPointX]));
     Y:=StrToInt(Trim(AList[fiPointY]));
     APos.Init(X,Y);
     APoint:=New(PPixel,Init(APos));
     APoint^.CalculateClipRect;
     InsertObjectToLayer(ALayer,APoint,AGUID);
end;

procedure TMUModule.ProcessNewArc(ALayer: PLayer; AGUID: TGUID; AList: TStringList);
var
AArc: PEllipseArc;
APos: TDPoint;
X,Y: Integer;
ARadius,BRadius: Integer;
BeginAngle,EndAngle: Double;
begin
     X:=StrToInt(Trim(AList[fiPointX]));
     Y:=StrToInt(Trim(AList[fiPointY]));
     ARadius:=StrToInt(Trim(AList[fiARadius]));
     BRadius:=StrToInt(Trim(AList[fiBRadius]));
     BeginAngle:=SToF(Trim(AList[fiBeginAngle]));
     EndAngle:=SToF(Trim(AList[fiEndAngle]));
     APos.Init(X,Y);
     AArc:=New(PEllipseArc,Init(APos,ARadius,BRadius,0,BeginAngle,EndAngle));
     AArc^.CalculateClipRect;
     InsertObjectToLayer(ALayer,AArc,AGUID);
end;

procedure TMUModule.ProcessNewArea(ALayer: PLayer; AGUID: TGUID; AList: TStringList);
var
ACPoly: PCPoly;
APos: TDPoint;
i,APointCount: Integer;
AIslandCount: Integer;
begin
     AIslandCount:=StrToInt(Trim(AList[fiIslandCount]));
     APos.Init(0,0);
     ACPoly:=New(PCPoly,Init);
     ACPoly.IslandCount:=AIslandCount;
     if AIslandCount > 0 then begin
        GetMem(ACPoly^.IslandInfo,AIslandCount*Sizeof(ACPoly^.IslandInfo^[0]));
     end;
     for i:=0 to AIslandCount-1 do begin
         ACPoly^.IslandInfo^[i]:=StrToInt(Trim(AList[fiIslandInfo+i]));
     end;
     APointCount:=StrToInt(Trim(AList[fiAreaPointCount+AIslandCount]));
     for i:=0 to APointCount-1 do begin
         APos.X:=StrToInt(Trim(AList[fiAreaPointX+AIslandCount+i*2]));
         APos.Y:=StrToInt(Trim(AList[fiAreaPointY+AIslandCount+i*2]));
         ACPoly^.InsertPoint(APos);
     end;
     ACPoly^.SetState(sf_IslandArea,(AIslandCount > 0));
     ACPoly^.CalculateClipRect;
     InsertObjectToLayer(ALayer,ACPoly,AGUID);
end;

procedure TMUModule.ProcessNewCircle(ALayer: PLayer; AGUID: TGUID; AList: TStringList);
var
ACircle: PEllipse;
APos: TDPoint;
X,Y: Integer;
ARadius,BRadius: Integer;
begin
     X:=StrToInt(Trim(AList[fiPointX]));
     Y:=StrToInt(Trim(AList[fiPointY]));
     ARadius:=StrToInt(Trim(AList[fiARadius]));
     BRadius:=StrToInt(Trim(AList[fiBRadius]));
     APos.Init(X,Y);
     ACircle:=New(PEllipse,Init(APos,ARadius,BRadius,0));
     ACircle^.CalculateClipRect;
     InsertObjectToLayer(ALayer,ACircle,AGUID);
end;

procedure TMUModule.ProcessNewText(ALayer: PLayer; AGUID: TGUID; AList: TStringList);
var
AText: PText;
APos: TDPoint;
X,Y: Integer;
AAngle: Integer;
ATextText: AnsiString;
AAlign: Integer;
AFontData: TFontData;
APInfo: PPaint;
begin
     X:=StrToInt(Trim(AList[fiPointX]));
     Y:=StrToInt(Trim(AList[fiPointY]));
     APos.Init(X,Y);
     AAngle:=StrToInt(Trim(AList[fiTextAngle]));
     AAlign:=StrToInt(Trim(AList[fiTextAlign]));
     ATextText:=Trim(AList[fiTextText]);
     AFontData.Init;
     AFontData.Font:=StrToInt(Trim(AList[fiTextFont]));
     AFontData.Style:=StrToInt(Trim(AList[fiTextFontStyle]));
     AFontData.Height:=StrToInt(Trim(AList[fiTextFontHeight]));
     APInfo:=WingisMainForm.ActualChild.Data^.PInfo;
     AText:=New(PText,InitCalculate(APInfo,AFontData,ATextText,APos,AAngle,AAlign));
     AText^.Width:=StrToInt(Trim(AList[fiTextWidth]));
     AText^.Height:=StrToInt(Trim(AList[fiTextHeight]));
     AText^.nLines:=StrToInt(Trim(AList[fiTextNLines]));
     AText^.fTrim:=StrToInt(Trim(AList[fiTextTrim]));
     AText^.fFlagSt:=StrToInt(Trim(AList[fiTextFlags]));
     AText^.fAdvMode:=StrToInt(Trim(AList[fiTextAdvMode]));
     AText^.IsItAnnot:=(StrToInt(Trim(AList[fiTextIsAnnot])) <> 0);
     AText^.IsUseOffset:=(StrToInt(Trim(AList[fiTextIsOffset])) <> 0);
     AText^.ObjectOffSetType:=StrToInt(Trim(AList[fiTextOffsetType]));
     InsertObjectToLayer(ALayer,AText,AGUID);
end;

procedure TMUModule.ProcessNewSymbol(ALayer: PLayer; AGUID: TGUID; AList: TStringList);
var
ASymbol: PSymbol;
APos: TDPoint;
X,Y: Integer;
AName: AnsiString;
ALibName: AnsiString;
ASize: Double;
ASizeType: Integer;
AAngle: Double;
APInfo: PPaint;
begin
     X:=StrToInt(Trim(AList[fiPointX]));
     Y:=StrToInt(Trim(AList[fiPointY]));
     APos.Init(X,Y);
     AName:=Trim(AList[fiSymbolName]);
     ALibName:=Trim(AList[fiSymbolLibName]);
     ASize:=SToF(Trim(AList[fiSymbolSize]));
     ASizeType:=StrToInt(Trim(AList[fiSymbolSizeType]));
     AAngle:=SToF(Trim(AList[fiSymbolAngle]));
     APInfo:=WingisMainForm.ActualChild.Data^.PInfo;
     ASymbol:=New(PSymbol,InitName(APInfo,APos,AName,ALibName,ASize,ASizeType,AAngle));
     InsertObjectToLayer(ALayer,ASymbol,AGUID);
end;

procedure TMUModule.ProcessNewLine(ALayer: PLayer; AGUID: TGUID; AList: TStringList);
var
APoly: PPoly;
APos: TDPoint;
i,APointCount: Integer;
begin
     APointCount:=StrToInt(Trim(AList[fiLinePointCount]));
     APos.Init(0,0);
     APoly:=New(PPoly,Init);
     for i:=0 to APointCount-1 do begin
         APos.X:=StrToInt(Trim(AList[fiLinePointX+i*2]));
         APos.Y:=StrToInt(Trim(AList[fiLinePointY+i*2]));
         APoly^.InsertPoint(APos);
     end;
     APoly^.CalculateClipRect;
     InsertObjectToLayer(ALayer,APoly,AGUID);
end;

procedure TMUModule.ProcessUpdPointData(APoint: PPixel; AList: TStringList);
begin
     APoint^.Position.X:=StrToInt(Trim(AList[fiPointX]));
     APoint^.Position.Y:=StrToInt(Trim(AList[fiPointY]));
     APoint^.CalculateClipRect;
end;

procedure TMUModule.ProcessUpdArcData(AArc: PEllipseArc; AList: TStringList);
begin
     AArc^.Position.X:=StrToInt(Trim(AList[fiPointX]));
     AArc^.Position.Y:=StrToInt(Trim(AList[fiPointY]));
     AArc^.PrimaryAxis:=StrToInt(Trim(AList[fiARadius]));
     AArc^.SecondaryAxis:=StrToInt(Trim(AList[fiBRadius]));
     AArc^.Angle:=0;
     AArc^.BeginAngle:=SToF(Trim(AList[fiBeginAngle]));
     AArc^.EndAngle:=SToF(Trim(AList[fiEndAngle]));
     AArc^.CalculateClipRect;
end;

procedure TMUModule.ProcessUpdAreaData(ACPoly: PCPoly; AList: TStringList);
var
APos: TDPoint;
i,APointCount: Integer;
AIslandCount: Integer;
begin
     if ACPoly^.SnapInfo <> nil then begin
        Dispose(ACPoly^.SnapInfo,Done);
        ACPoly^.SnapInfo:=nil;
     end;
     Dispose(ACPoly^.Data,Done);
     ACPoly^.Data:=New(PDPointCollection,Init(ciPoly,cePoly));
     ACPoly^.FreeIslandInfo;

     AIslandCount:=StrToInt(Trim(AList[fiIslandCount]));
     APos.Init(0,0);
     ACPoly.IslandCount:=AIslandCount;
     if AIslandCount > 0 then begin
        GetMem(ACPoly^.IslandInfo,AIslandCount*Sizeof(ACPoly^.IslandInfo^[0]));
     end;
     for i:=0 to AIslandCount-1 do begin
         ACPoly^.IslandInfo^[i]:=StrToInt(Trim(AList[fiIslandInfo+i]));
     end;
     APointCount:=StrToInt(Trim(AList[fiAreaPointCount+AIslandCount]));
     for i:=0 to APointCount-1 do begin
         APos.X:=StrToInt(Trim(AList[fiAreaPointX+AIslandCount+i*2]));
         APos.Y:=StrToInt(Trim(AList[fiAreaPointY+AIslandCount+i*2]));
         ACPoly^.InsertPoint(APos);
     end;
     ACPoly^.SetState(sf_IslandArea,(AIslandCount > 0));
     ACPoly^.CalculateClipRect;
end;

procedure TMUModule.ProcessUpdCircleData(ACircle: PEllipse; AList: TStringList);
begin
     ACircle^.Position.X:=StrToInt(Trim(AList[fiPointX]));
     ACircle^.Position.Y:=StrToInt(Trim(AList[fiPointY]));
     ACircle^.PrimaryAxis:=StrToInt(Trim(AList[fiARadius]));
     ACircle^.SecondaryAxis:=StrToInt(Trim(AList[fiBRadius]));
     ACircle^.CalculateClipRect;
end;

procedure TMUModule.ProcessUpdTextData(AText: PText; AList: TStringList);
var
APInfo: PPaint;
begin
     AText^.Pos.X:=StrToInt(Trim(AList[fiPointX]));
     AText^.Pos.Y:=StrToInt(Trim(AList[fiPointY]));
     AText^.Angle:=StrToInt(Trim(AList[fiTextAngle]));
     AText^.Width:=StrToInt(Trim(AList[fiTextWidth]));
     AText^.Font.Font:=StrToInt(Trim(AList[fiTextFont]));
     AText^.Font.Style:=StrToInt(Trim(AList[fiTextFontStyle]));
     AText^.Font.Height:=StrToInt(Trim(AList[fiTextFontHeight]));
     AText^.SetText(Trim(AList[fiTextText]));
     AText^.Height:=StrToInt(Trim(AList[fiTextHeight]));
     AText^.nLines:=StrToInt(Trim(AList[fiTextNLines]));
     AText^.fTrim:=StrToInt(Trim(AList[fiTextTrim]));
     AText^.fFlagSt:=StrToInt(Trim(AList[fiTextFlags]));
     AText^.fAdvMode:=0;
     AText^.IsItAnnot:=(StrToInt(Trim(AList[fiTextIsAnnot])) <> 0);
     AText^.IsUseOffset:=(StrToInt(Trim(AList[fiTextIsOffset])) <> 0);
     AText^.ObjectOffSetType:=StrToInt(Trim(AList[fiTextOffsetType]));
     AText^.FAlign:=StrToInt(Trim(AList[fiTextAlign]));
     APInfo:=WingisMainForm.ActualChild.Data^.PInfo;
     AText^.UpdateTextData(APInfo);
end;

procedure TMUModule.ProcessUpdSymbolData(ASymbol: PSymbol; AList: TStringList);
var
APInfo: PPaint;
begin
     ASymbol^.Position.X:=StrToInt(Trim(AList[fiPointX]));
     ASymbol^.Position.Y:=StrToInt(Trim(AList[fiPointY]));
     ASymbol^.Size:=SToF(Trim(AList[fiSymbolSize]));
     ASymbol^.SizeType:=StrToInt(Trim(AList[fiSymbolSizeType]));
     ASymbol^.Angle:=SToF(Trim(AList[fiSymbolAngle]));
     APInfo:=WingisMainForm.ActualChild.Data^.PInfo;
     ASymbol^.CalculateClipRect(APInfo);
end;

procedure TMUModule.ProcessUpdLineData(APoly: PPoly; AList: TStringList);
var
APos: TDPoint;
i,APointCount: Integer;
begin
     if APoly^.SnapInfo <> nil then begin
        Dispose(APoly^.SnapInfo,Done);
        APoly^.SnapInfo:=nil;
     end;
     Dispose(APoly^.Data,Done);
     APoly^.Data:=New(PDPointCollection,Init(ciPoly,cePoly));

     APointCount:=StrToInt(Trim(AList[fiLinePointCount]));
     APos.Init(0,0);
     for i:=0 to APointCount-1 do begin
         APos.X:=StrToInt(Trim(AList[fiLinePointX+i*2]));
         APos.Y:=StrToInt(Trim(AList[fiLinePointY+i*2]));
         APoly^.InsertPoint(APos);
     end;
     APoly^.CalculateClipRect;
end;

procedure TMUModule.InsertObjectToLayer(ALayer: PLayer; AView: PView; AGUID: TGUID);
var
AItem: PIndex;
AData: PProj;
begin
     AData:=WingisMainForm.ActualChild.Data;
     if AData^.Layers^.IndexObject(AData^.PInfo,AView) = nil then begin
         PLayer (AData^.PInfo^.Objects)^.InsertObject(AData^.PInfo,AView,False);
         AView^.GUID:=AGUID;
     end;
     AItem:=New(PIndex,Init(AView^.Index));
     AItem^.ClipRect.InitByRect(AView^.ClipRect);
     if AData^.Layers^.IndexObject(AData^.PInfo,AItem) <> nil then begin
         ALayer^.InsertObject(AData^.PInfo,AItem,False);
         AData^.UpdateCliprect(AView);
     end
     else begin
        Dispose(AItem,Done);
   end;
end;

procedure TMUModule.ShowError(s: AnsiString);
begin
     MessageBox(WingisMainForm.Handle,PChar(s),PChar(Application.Title),MB_ICONERROR or MB_APPLMODAL or MB_OK);
end;

procedure TMUModule.ReorganizeProject;
var
ALocalFileName: AnsiString;
begin
     NoLog:=True;
     try
        ALocalFileName:=ChangeFileExt(WingisMainForm.ActualChild.Data^.FName,SyncFileExt);
        if FileExists(ALocalFileName) then begin
           ProcessFile(ALocalFileName,True,True);
           WingisMainForm.ActualChild.Data^.SetModified;
        end;
     finally
        NoLog:=False;
     end;
end;

procedure TMUModule.CheckReorganize;
var
AFileName: AnsiString;
begin
     MenuFunctions['MUReorganize'].Visible:=False;
     if WingisMainForm.ActualChild = nil then begin
        exit;
     end;
     if WingisMainForm.ActualChild.Data^.PInfo^.IsMU then begin
        exit;
     end;
     AFileName:=ChangeFileExt(WingisMainForm.ActualChild.Data^.FName,SyncFileExt);
     MenuFunctions['MUReorganize'].Visible:=FileExists(AFileName);
     if MenuFunctions['MUReorganize'].Visible then begin
        if MessageBox(WingisMainForm.Handle,PChar(MlgSection[14]),PChar(Application.Title),MB_ICONQUESTION or MB_APPLMODAL or MB_YESNO) = ID_YES then begin
           ReorganizeProject;
           {$IFNDEF AXDLL}
           WingisMainForm.ActualChild.FileSave;
           {$ENDIF}
           MenuFunctions['MUReorganize'].Visible:=FileExists(AFileName);
        end;
     end;
end;

function TMUModule.FileIsUpToDate(ALocalFileName: AnsiString): Boolean;
var
ALocalFileDate,AServerFileDate: Double;
s: AnsiString;
AUrl: AnsiString;
Inet,hURL: HInternet;
Buffer: Array[0..31] of Char;
Dummy,BufferSize: DWORD;
AList: TStringList;
begin
     Result:=False;
     ALocalFileName:=ALocalFileName;
     AUrl:=LastServerAddress;
     if AUrl = '' then begin
        exit;
     end;
     AUrl:=AUrl + '?op=getfiledate&file=' + ExtractFileName(ALocalFileName);
     AServerFileDate:=0;
     ALocalFileDate:=GetFileDate(ALocalFileName);
     Inet:=InternetOpen(nil,INTERNET_OPEN_TYPE_PRECONFIG,nil,nil,0);
     if Inet <> nil then begin
        hURL:=InternetOpenURL(Inet,PChar(AUrl),nil,0,INTERNET_FLAG_RELOAD,0);
        if hURL <> nil then begin
           InternetReadFile(hURL,@Buffer,Sizeof(Buffer),BufferSize);
           s:=Copy(Buffer,0,BufferSize);
           if s <> '' then begin
              AList:=StringToList(s,'|');
              try
                 AList.Text:=s;
                 if AList[0] = 'RES_OK' then begin
                    AServerFileDate:=SToF(AList[1]);
                 end;
              finally
                 AList.Free;
              end;
           end;
           InternetCloseHandle(hURL);
        end;
        InternetCloseHandle(Inet);
     end;
     Result:=(ALocalFileDate >= AServerFileDate);
end;

initialization
MU:=TMUModule.Create;

finalization
MU.Free;

end.
