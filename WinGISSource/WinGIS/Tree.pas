unit Tree;

interface
uses Classes;
type
     TTreeBaseItem = class;
     TBaseTree = class
      private
       FRoot       : TTreeBaseItem;
       Function   InsertTree(var p:TTreeBaseItem;key:TTreeBaseItem):Boolean;
      public
       Constructor Create;
       Procedure   TraverseTree(p:TTreeBaseItem); virtual;
       Property    Root:TTreeBaseItem read FRoot write FRoot;
       Function    InsertInTree(key:Pointer):Boolean;
       Procedure   DisposeTree(p:TTreeBaseItem);
       Procedure   Traverse; virtual;
       Function    OnCompareItems(key1,key2:Pointer):Integer; virtual; abstract;
       Function    Key1Of(Item:Pointer):Pointer; virtual; abstract;
       Function    Key2Of(Item:Pointer):Pointer; virtual; abstract;
       Function    ResultZero(p,k:TTreeBaseItem):Boolean; virtual; abstract;
       Destructor  Destroy; override;
     end;

     TTreeBaseItem = class
      private
      public
       Left       : TTreeBaseItem;
       Right      : TTreeBaseItem;
       Procedure SaveNode; virtual; abstract;
       Constructor Init(key:Pointer); virtual; abstract;
     end;

     TTreeBaseItemRef = class of TTreeBaseItem;

implementation

Constructor TBaseTree.Create;
  begin
    inherited Create;

  end;

Procedure TBaseTree.DisposeTree                                                     {???}
   (
   p:TTreeBaseItem
   );
  var h: TTreeBaseItem;
  begin
    if p<>NIL then begin
      DisposeTree(p.Left);
      h:=p.Right;
      p.Free;
      DisposeTree(h);
    end;
  end;

Destructor TBaseTree.Destroy;
  begin
    DisposeTree(Root);
    Root:=NIL;
    inherited Destroy;
  end;

Function TBaseTree.InsertInTree
   (
   key        : Pointer
   )
   :Boolean;
  begin
    Result:=InsertTree(FRoot,key);
  end;

Function TBaseTree.InsertTree
   (
   var p          : TTreeBaseItem;
       key        : TTreeBaseItem
   )
   :Boolean;
  var Compare : Integer;
  begin
    if p<>NIL then begin
      Compare:=OnCompareItems(Key1Of(p),Key2Of(key));
      if Compare = -1 then begin
        Result:=InsertTree(p.Left,key);
      end
      else if Compare = 1 then begin
        Result:=InsertTree(p.Right,key);
      end
      else if Compare = 0 then begin
        Result:=ResultZero(p,key);
      end;
    end
    else begin
      {Ref:=TTreeBaseItemRef(p.ClassType);
      p:=Ref.Init(key);}
      p:=key;
      Result:=True;
    end;
  end;

Procedure TBaseTree.Traverse;
  begin
    TraverseTree(FRoot);
  end;

Procedure TBaseTree.TraverseTree
   (
   p:TTreeBaseItem
   );
  begin
    if p<>NIL then begin
      TraverseTree(p.left);
      p.SaveNode;
      TraverseTree(p.Right);
    end;
  end;

end.
