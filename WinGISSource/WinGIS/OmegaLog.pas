{******************************************************************************}
{ Unit OmegaLog                                                                }
{------------------------------------------------------------------------------}
{------------------------------------------------------------------------------}
{ Autor: Sygolaeff Victor, 25-DEC-2002                                         }
{******************************************************************************}
unit OmegaLog;

{$H+}

interface

uses Windows, UserUtils;

type
  TOmegaLog = class
  private
    FFileName: AnsiString;
    FDoLogging: Boolean;
    F: Text;
    procedure SetDoLogging(ASet: Boolean);
    procedure SetFileName(const AFileName: AnsiString);
  public
    property FileName: AnsiString read FFileName write SetFileName;
    property Logging: Boolean read FDoLogging write SetDoLogging;
    procedure WriteLog(const AStr: AnsiString; AddTime:Boolean = True);
    procedure WriteLogFmt(const AFormat: AnsiString; AParams: array of const; AddTime:Boolean = True);
  end;

implementation

uses SysUtils, WinDOS, WinProcs;

procedure TOmegaLog.SetDoLogging
  (
  ASet: Boolean
  );
begin
  if (FileName <> '') and (ASet <> FDoLogging) then
  begin
    Assign(F, FileName);
    if not FileExists(FFileName) then
    begin
      Rewrite(F);
      CloseFile(F);
    end;
    if ASet then
    begin
      FDoLogging := ASet;
      WriteLog('Log opened.', True);
    end
    else
    begin
      WriteLog('Log closed.', True);
      WriteLog('', False); // don't add time to last line of the log
      FDoLogging := ASet;
    end;
  end;
end;

procedure TOmegaLog.SetFileName
  (
  const AFileName: AnsiString
  );
begin
  if AnsiCompareText(FFileName, AFileName) <> 0 then
  begin
    FFileName := AFileName;
    if Logging then
    begin
      WriteLog('Log switched to ' + AFileName, True);
      FDoLogging := FALSE;
      SetDoLogging(TRUE);
    end;
  end;
end;

procedure TOmegaLog.WriteLog(const AStr: AnsiString; AddTime:Boolean  );
var
  AOutString        : AnsiString;
begin
  if Logging then
  begin
    Append(F);
    AOutString := SmartString(AStr, []);
    if AddTime then // add time stamp
       Write(F, FormatDateTime('dd.mm.yy-hh:mm:ss.ms', Now) );
    if AOutString <> '' then
      WriteLn(F, ': ', AOutString)
    else
      WriteLn(F, AOutString);
    CloseFile(F);
  end;
end;

//procedure TOmegaLog.WriteLogFmt(const AFormat: AnsiString; AParams: array of const; const AddTime: Boolean);
procedure TOmegaLog.WriteLogFmt(const AFormat: AnsiString; AParams: array of const; AddTime:Boolean );
begin
  if Logging then
    WriteLog(Format(AFormat, AParams), AddTime);
end;

end.


