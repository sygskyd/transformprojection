�
 TMUOPENFORM 0�  TPF0TMUOpenForm
MUOpenFormLeftSTop� BorderStylebsDialogCaption!1ClientHeightyClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight 	TGroupBoxGroupBoxServerURLLeftTopWidth�HeightACaption!2TabOrder  TEditEditServerAddressLeftTopWidthiHeightTabOrder OnChangeEditServerAddressChange	OnKeyDownEditServerAddressKeyDown  TButtonButtonConnectLeftxTopWidthKHeightCaption!3TabOrderOnClickButtonConnectClick   	TGroupBoxGroupBoxServerProjectsLeftTopPWidth�HeightqCaption!4TabOrder TListBoxListBoxServerProjectsLeftTopWidth�HeightQExtendedSelectFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ItemHeight
ParentFontTabOrder 
OnDblClickListBoxServerProjectsDblClick	OnKeyDownListBoxServerProjectsKeyDownOnMouseDownListBoxServerProjectsMouseDown   	TGroupBoxGroupBoxButtonsLeftTop@Width�Height1TabOrder TButtonButtonCancelLeftxTopWidthKHeightCaption!6ModalResultTabOrder OnClickButtonCancelClick  TButtonButtonOKLeft(TopWidthKHeightCaption!5ModalResultTabOrderOnClickButtonOKClick   	TGroupBoxGroupBoxLocalProjectsLeftTop� Width�HeightqCaption!15TabOrder TListBoxListBoxLocalProjectsLeftTopWidth�HeightQ
ItemHeightTabOrder 
OnDblClickListBoxLocalProjectsDblClick	OnKeyDownListBoxLocalProjectsKeyDownOnMouseDownListBoxLocalProjectsMouseDown   TMlgSection
MlgSectionSection	MultiuserLeft�Topp   