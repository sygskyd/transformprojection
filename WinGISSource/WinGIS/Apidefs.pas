{****************************************************************************}
{**                                                                        **}
{**                               Hardlock                                 **}
{**                    API-Structures and definitions                      **}
{**                                                                        **}
{**   This file contains some helpful defines to access a Hardlock using   **}
{**   the application programing interface (API) for Hardlock.             **}
{**                                                                        **}
{**                ///FAST Software Security - Group Aladdin		   **}
{**                                                                        **}
{**  Revision history                                                      **}
{**  ----------------
***  $Log: Apidefs.pas,v $
***  Revision 1.1  2005/03/24 13:31:54  AIG
***  *** empty log message ***
***
***  Revision 1.1.1.1  2000/03/14 15:55:29  strauss
***  no message
***
***  Revision 1.3  1997/02/10 17:32:13  Werner
***  Added messages and defines for LM.
***  
***  Revision 1.2  1996/08/08 15:29:21  henri
***  Added VCS log.
***  
**}
{****************************************************************************}

const
  API_INIT          =   0;     { Initialize structure        }
  API_DOWN          =   1;     { Release structure           }
  API_FORCE_DOWN    =  31;     { Force release of structure  }
  API_ABORT         =  51;     { Critical Error Abort        }

  API_AVAIL         =   6;     { Dongle available ?          }
  API_LOGIN         =   7;     { Login on module             }
  API_LOGOUT        =   8;     { Logout from module          }
  API_GET_TASKID    =  32;     { Get TaskID from API         }
  API_LOGIN_INFO    =  34;     { Get TaskID from API         }

  API_INFO          =   9;     { Informations about API      }
  API_KEYE          =  11;     { Encryption routine          }

  API_READ          =  20;     { Read Dongle Register        }
  API_WRITE         =  21;     { Write Dongle Register       }
  API_READBLOCK     =  23;     { Read Dongle Memory Block    }
  API_WRITEBLOCK    =  24;     { Write Dongle Memory Block   }

  EYE_DONGLE        =  0;      { Hardlock E-Y-E              }
  LOCAL_ACCESS      =  1;      { Local access to module      }
  LOCAL_DEVICE      =  1;      {           "                 }
  REMOTE_ACCESS     =  2;      { Remote access via HL-Server }
  NET_DEVICE        =  2;      {           "                 }

  DONT_CARE         =  3;      { First try local then remote }


 {-- LM functions --}
  API_LMINIT        =  40;     { LM compatible API_INIT replacement           }
  API_LMPING        =  41;     { checks if LM dongle and slot is available    }
  API_LMINFO        =  42;     { info about currently used LIMA               }



  {--- API Status Codes ---}

  STATUS_OK	    =	0;	{ API call was succesfull        }
  NOT_INIT	    =	1;	{ DONGLE not initialized         }
  ALREADY_INIT	    =	2;	{ Already initialized	         }
  UNKNOWN_DONGLE    =	3;	{ Device not supported	         }
  UNKNOWN_FUNCTION  =	4;	{ Function not supported         }
  HLS_FULL	    =   6;      { HL-Server is full              }
  NO_DONGLE	    =	7;	{ No device available	         }
  NETWORK_ERROR     =	8;	{ A network error occured        }
  NO_ACCESS	    =	9;	{ No device available	         }
  INVALID_PARAMETER =  10;	{ Invalid parameter passed       }
  VERSION_MISMATCH  =  11;	{ Api Version Mismatch	         }
  DOS_ALLOC_ERROR   =  12;      { Error on memory allocation     }
  CANNOT_OPEN_DRIVER=  14;      { Can not open driver            }
  INVALID_ENV       =  15;      { Error in env. search string    }
  DYNALINK_FAIL     =  16;      { Unable to get a function entry }
  INVALID_LIC       =  17;      { No valid licence info (LM)     }
  NO_LICENSE        =  18;      { Slot/licence not enabled (LM)  }
  TOO_MANY_USERS    = 256;	{ No free login slot	     }
  SELECT_DOWN	    = 257;	{ Printer not On-Line	     }


{--- API PM_Host ID's ---}

  API_XTD_DETECT    =   0;
  API_XTD_DPMI      =   1;       { QDPMI, Borland, Windows ...   }
  API_XTD_PHAR386   =   2;
  API_XTD_PHAR286   =   3;
  API_XTD_CODEBLDR  =   4;       { Intel Code Builder            }
  API_XTD_COBOLXM   =   5;


type
  API_STRUCTURE = record
    VERS_MAJOR	   : byte;	{ Major version number of API		}
    VERS_MINOR	   : byte;	{ Minor version number of API		}
    FLAGS_1	   : word;	{ Option flag one			}
    FLAGS_2	   : word;	{ Option flag two			}
    HARD_ID        : word;      { Dongle Code   (only EYE supported)    }
    EYE_MODUL      : word;      { Moduleadresse                (EYE)    }
    EYE_ADR        : word;      { Register for Read and Write  (EYE)    }
    EYE_VALUE      : word;      { Read and Write value         (EYE)    }
    RESERVE1       : Longint;   { Reserved                              }
    DATA_PTR       : Pointer;   { Pointer on data for encryption        }
    DATA_BLOCKS    : word;      { Number of 8 Byte Blocks to encrypt    }
    FUNKTION       : word;      { Function number                       }
    STATUS         : word;      { Return status                         }
    REMOTE         : word;      { Accessmode (remote and/or local)      }
    PORT           : word;      { Portaddress                           }
    SPEED          : word;      { Value for speed calibration           }
    NET_USERS      : word;      { Current Login Count                   }
    ID_REF         : Array [1..8] of char;
    ID_VERIFY      : Array [1..8] of char;
    TASK_ID        : LongInt;   { task ID for Login                     }
    MAX_USERS	   : word;	{ Maximum Logins (HL-Server)		}
    TIMEOUT	   : LongInt;	{ Login Timeout in minutes		}
    SHORTLIFE	   : word;	{ (multiple use)			}
    APPLICATION	   : word;      { Application Number                    }
    PROTOCOL	   : word;	{ Protocol Flags, Bitmap                }
    PM_HOST        : word;      { Number of DOS Extender used           }
    OSSPECIFIC     : LongInt;   { Ptr to OS specific data		}
    PORTMASK	   : word;	{ Default local search (in)		}
    PORTFLAGS      : word;	{ Default local search (out)		}
    ENVMASK	   : word;	{ Use env string search (in)		}
    ENVFLAGS	   : word;	{ Use env string search (out)		}
    RESERVE2	   : Array [1..2] of word;
    SLOT_ID        : word;      { License slot number                   }
    RESERVE3       : Array [1..168] of char;
  end;

{컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴}
{eof}
