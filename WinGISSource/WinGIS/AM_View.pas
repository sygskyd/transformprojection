{****************************************************************************}
{ Unit                                                                       }
{----------------------------------------------------------------------------}
{                                                                            }
{----------------------------------------------------------------------------}
{ Modifikationen                                                             }
{  16.02.1994 Martin Forst                                                   }
// 17.10.00 Y.Glukhov, ObjAttachment
{****************************************************************************}
unit AM_View;

interface

uses WinTypes, WinProcs, AM_Def, AM_Paint, AM_Index, Objects
  , GrTools // Glukhov Bug#468 Build#166 16.09.01
  ;

//++ Glukhov ObjAttachment 23.10.00
const
  cMaxAttachmentChain = 1024; // Maximal length of chain of attachments
// Alignment positions
  cAlignLU = 0; // left-up
  cAlignLM = 1; // left-middle
  cAlignLD = 2; // left-down
  cAlignMU = 3; // middle-up
  cAlignMM = 4; // middle-middle
  cAlignMD = 5; // middle-down
  cAlignRU = 6; // right-up
  cAlignRM = 7; // right-middle
  cAlignRD = 8; // right-down

type
  TAttData = class(TObject)
  public
      // Index of master object to which the object is attached
    IdxMaster: LongInt;
      // Pointer to master object to which the object is attached
    ObjMaster: Pointer; // do not use it directly, use GetObjMaster
      // Alignment position of the master object (see const)
    AliPosOfMaster: LongInt;
      // Alignment position of the attached object (see const)
    AliPosOfObject: LongInt;
      // Pointer to the collection of the attached objects
    PAttached: PCollection;
    constructor Create;
    destructor Destroy;
//++ Glukhov Bug#468 Build#162 01.08.01
        // Copies the source object
    procedure Assign(Src: TAttData);
//-- Glukhov Bug#468 Build#162 01.08.01
  end;
//-- Glukhov ObjAttachment 23.10.00

type
  PView = ^TView;
  TView = object(TIndex)
    GUID: TGUID;
//++ Glukhov ObjAttachment 19.10.00
     // Pointer to Data about the attached objects and the object attachment
    AttData: TAttData;
//-- Glukhov ObjAttachment 19.10.00
    constructor Init;
    constructor Load(S: TOldStream);
    destructor Done; virtual;
//++ Glukhov Bug#468 Build#162 01.08.01
       // Init by the source object
    procedure InitBy(Src: PView);
//-- Glukhov Bug#468 Build#162 01.08.01
//++ Glukhov ObjAttachment 11.10.00
// Returns the index of the master object
    function GetIdxMaster: LongInt;
// Returns the pointer to collection of the attached objects
    function GetAttachedObjs: PCollection;
// Returns the pointer to the master object, forms link data, if necessary
    function GetObjMaster(PInfo: PPaint): PView;
// Returns True when the attachment makes a loop
    function CheckLoopAttachment(PInfo: PPaint; AttachTo: PView): Boolean;
// Returns > 0, if Ok
    function AttachToObject(PInfo: PPaint; AttachTo: PView): Integer;
// Returns the index of object that was a master
    function DetachMe: Integer;
// Returns the number of the detached objects
    function DetachFromMe(PInfo: PPaint): Integer; // Glukhov Bug#468 Build#166 16.09.01
//-- Glukhov ObjAttachment 11.10.00
    function CheckMove(PInfo: PPaint; XMove, YMove: LongInt): Boolean; virtual;
    procedure Draw(PInfo: PPaint; Clipping: Boolean); virtual;
    procedure DrawTransform(PInfo: PPaint; Clipping: Boolean); virtual;
    procedure GetSize(var ABorder: TDRect); virtual;
    function GivePosX: LongInt; virtual;
    function GivePosY: LongInt; virtual;
    function GiveAPosX: LongInt; virtual;
    function GiveAPosY: LongInt; virtual;
    function GivePosXMin: LongInt; virtual;
    function GivePosYMin: LongInt; virtual;
    function GivePosXMax: LongInt; virtual;
    function GivePosYMax: LongInt; virtual;
    function GiveRadius: LongInt; virtual;
    function GiveBegAngle: Real; virtual;
    function GiveEndAngle: Real; virtual;
    procedure Invalidate(PInfo: PPaint; FromGraphic: Boolean = TRUE); virtual;
    function MakeCopy: PView; {2705F}
//++ Glukhov Bug#468 Build#166 16.09.01
// Gets the object data to edit it, using the special component.
// P1 is a left lower corner of border, p4 is a left upper one.
// bSimilar is used only when bResize is True.
    procedure InitObjEdit(PInfo: PPaint; var P1, P2, P3, P4: TGrPoint;
      var bRotate, bResize, bSimilar, bMove: Boolean); virtual;
// Modifies the object after its editing, using the special component.
    procedure ExecObjEdit(PInfo: PPaint; P1, P2, P3, P4: TGrPoint); virtual;
//-- Glukhov Bug#468 Build#166 16.09.01
    procedure MoveRel(XMove, YMove: LongInt); virtual;
    procedure Scale(XScale, YScale: Real); virtual; {2705F}
    function SelectByRect(PInfo: PPaint; Rect: TDRect; Inside: Boolean): Boolean; virtual;
    procedure Store(S: TOldStream); virtual;
    procedure UpdateData;
  end;

implementation

uses BmpImage, ActiveX, MUModule, AM_Main, SysUtils, ErrHndl
  , AM_Layer // Glukhov ObjAttachment 12.10.00
  , AM_Text // Glukhov Bug#468 Build#166 16.09.01
  ;

//++ Glukhov ObjAttachment 23.10.00

constructor TAttData.Create;
begin
  IdxMaster := 0;
  ObjMaster := nil;
  AliPosOfMaster := cAlignLU;
  AliPosOfObject := cAlignLU;
  PAttached := nil;
end;

destructor TAttData.Destroy;
begin
  ObjMaster := nil;
  if Assigned(PAttached) then
  begin
    PAttached.DeleteAll;
    Dispose(PAttached);
    PAttached := nil;
  end;
end;

//++ Glukhov Bug#468 Build#162 01.08.01
// Copies the source object

procedure TAttData.Assign(Src: TAttData);
var
  i: Integer;
  TmpItem: PIndex;
begin
  IdxMaster := Src.IdxMaster;
  ObjMaster := Src.ObjMaster;
  AliPosOfMaster := Src.AliPosOfMaster;
  AliPosOfObject := Src.AliPosOfObject;
  if Assigned(Src.PAttached) then
  begin
    if not Assigned(PAttached) then
      PAttached := New(PCollection, Init(8, 8))
    else
      PAttached.DeleteAll;
    for i := 0 to Src.PAttached.Count - 1 do
    begin
      TmpItem := PIndex(Src.PAttached.At(i));
      PAttached.Insert(TmpItem);
    end;
  end
  else
  begin
    if Assigned(PAttached) then
      PAttached.DeleteAll;
  end;
end;
//-- Glukhov Bug#468 Build#162 01.08.01

//-- Glukhov ObjAttachment 23.10.00

constructor TView.Init;
begin
  TIndex.Init(0);
  CoCreateGuid(GUID);
  ClipRect.Init;
//++ Glukhov ObjAttachment 19.10.00
// No object attachments at beginning
  AttData := nil;
//-- Glukhov ObjAttachment 19.10.00
end;

//++ Glukhov Bug#468 Build#162 01.08.01
// Init by the source object

procedure TView.InitBy(Src: PView);
begin
  TIndex.InitBy(Src);
  GUID := Src.GUID;
  if Assigned(Src.AttData) then
  begin
    if not Assigned(AttData) then
      AttData := TAttData.Create;
    AttData.Assign(Src.AttData);
  end
  else
  begin
    if Assigned(AttData) then
      AttData.Destroy;
    AttData := nil;
  end;
end;
//-- Glukhov Bug#468 Build#162 01.08.01

constructor TView.Load
  (
  S: TOldStream
  );
begin
  TIndex.Load(S);
  if ProjectVersion < 10 then
    ClipRect.Load(S);
//++ Glukhov ObjAttachment 19.10.00
  AttData := nil;
  if ProjectVersion >= 10 then
  begin
    if GetState(sf_Attached) then
    begin
      // Load index of master object and alignment data
      AttData := TAttData.Create;
      S.Read(AttData.IdxMaster, SizeOf(AttData.IdxMaster));
      S.Read(AttData.AliPosOfMaster, SizeOf(AttData.AliPosOfMaster));
      S.Read(AttData.AliPosOfObject, SizeOf(AttData.AliPosOfObject));
    end;
  end;
//-- Glukhov ObjAttachment 19.10.00
end;

procedure TView.Store
  (
  S: TOldStream
  );
begin
//++ Glukhov ObjAttachment 24.10.00
  if not Assigned(AttData) then
    SetState(sf_Attached, False);
//-- Glukhov ObjAttachment 24.10.00
  TIndex.Store(S);
{++ Bug#195 Ivanoff}
  if ProjectVersion < 10 then
    ClipRect.Store(S);
{-- Bug#195}
//++ Glukhov ObjAttachment 29.09.00
  if GetState(sf_Attached) then
  begin
    // Save index of master object and alignment data
    S.Write(AttData.IdxMaster, SizeOf(AttData.IdxMaster));
    S.Write(AttData.AliPosOfMaster, SizeOf(AttData.AliPosOfMaster));
    S.Write(AttData.AliPosOfObject, SizeOf(AttData.AliPosOfObject));
  end;
//-- Glukhov ObjAttachment 29.09.00
end;

destructor TView.Done;
begin
  ClipRect.Done;
  TIndex.Done;
//++ Glukhov ObjAttachment 19.10.00
  if Assigned(AttData) then
  begin
    AttData.Destroy;
    AttData := nil;
  end;
//-- Glukhov ObjAttachment 19.10.00
end;

//++ Glukhov ObjAttachment 11.10.00

function TView.GetIdxMaster: LongInt;
// Returns the index of the master object
begin
  if not Assigned(AttData) then
    Result := 0
  else
    Result := AttData.IdxMaster;
end;

function TView.GetAttachedObjs: PCollection;
// Returns the pointer to collection of the attached objects
begin
  if (@Self = nil) or (not Assigned(AttData)) then
    Result := nil
  else
    Result := AttData.PAttached;
end;

function TView.GetObjMaster(PInfo: PPaint): PView;
var
  TmpItem: PIndex;
begin
  Result := nil;
  try
    if not Assigned(AttData) then
    begin
      Result := nil;
      Exit;
    end;
    if Assigned(AttData.ObjMaster) then
    begin
      Result := PView(AttData.ObjMaster);
      Exit;
    end;
    TmpItem := New(PIndex, Init(AttData.IdxMaster));
    Result := PLayer(PInfo.Objects).IndexObject(PInfo, TmpItem);
    Dispose(TmpItem, Done);
    AttachToObject(PInfo, Result);
  except
    on E: Exception do
    begin
      Result := nil;
      DebugMsg('TView.GetObjMaster', E.Message);
    end;
  end;
end;

// Returns True when the attachment makes a loop

function TView.CheckLoopAttachment(PInfo: PPaint; AttachTo: PView): Boolean;
var
  Check: PView;
  Cnt: Integer;
begin
  Result := True;
  Check := AttachTo;
  if Index = Check.Index then
    Exit;
  Cnt := 1;
  while Assigned(Check) do
  begin
    if not Assigned(Check.AttData) then
      Break;
    if Index = Check.AttData.IdxMaster then
      Exit;
    Inc(Cnt);
    if Cnt > cMaxAttachmentChain then
      Exit;
    Check := Check.GetObjMaster(PInfo);
  end;
  Result := False;
end;

function TView.AttachToObject(PInfo: PPaint; AttachTo: PView): Integer;
// Returns > 0, if Ok
// 0  - AttachTo is not assigned
// -1 - attachment to itself
// -2 - loop attachment
begin
  Result := 0;
//    AttachTo:= PLayer( PInfo.Objects ).IndexObject( PInfo, AttachTo );
  if not Assigned(AttachTo) then
    Exit;
  Result := -1;
  if Index = AttachTo^.Index then
    Exit;
  Result := -2;
  if CheckLoopAttachment(PInfo, AttachTo) then
    Exit;
  // Save the data about the master object
  if not Assigned(AttData) then
    AttData := TAttData.Create;
  AttData.IdxMaster := AttachTo^.Index;
  AttData.ObjMaster := AttachTo;
  // Save the attached object in the master object collection
  if not Assigned(AttachTo.AttData) then
    AttachTo.AttData := TAttData.Create;
  if not Assigned(AttachTo.AttData.PAttached) then
    AttachTo.AttData.PAttached := New(PCollection, Init(8, 8));
  AttachTo.AttData.PAttached.Insert(@Self);
  // Save the attached state
  SetState(sf_Attached, True);
  Result := 1;
end;

function TView.DetachMe: Integer;
// Returns the index of object that was a master
var
  Item: PView;
  Attached: PCollection;
begin
  Result := 0;
  if Assigned(AttData) then
  begin
    Result := AttData.IdxMaster;
    Item := PView(AttData.ObjMaster);
    // Correct the master object collection
    if Assigned(Item) then
    begin
      Attached := Item.GetAttachedObjs;
      if Assigned(Attached) then
        Attached.Delete(@Self);
    end;
    // Reset the data about the master object
    AttData.ObjMaster := nil;
    AttData.IdxMaster := 0;
  end;
  // Save the detached state
  SetState(sf_Attached, False);
end;

//++ Glukhov Bug#468 Build#166 16.09.01

function TView.DetachFromMe(PInfo: PPaint): Integer;
// Returns the number of the detached objects
var
  Attached: PCollection;
  Item: PView;
begin
  Result := 0;
  Attached := GetAttachedObjs;
  if Assigned(Attached) then
  begin
    Result := Attached.Count;
    // For every attached object
    while Attached.Count > 0 do
    begin
      Item := Attached.At(0);
      if (PView(Item).GetObjType = ot_Text) and (PText(Item).fAdvMode = cwTxtAdvSelPoly) then
      begin
          // Convert the text that is drawn along
        PText(Item).CalcObjVertices(PInfo);
        PText(Item).fAdvMode := cwTxtAdvManPoly;
      end;
      Item.DetachMe;
    end;
  end;
end;
//-- Glukhov Bug#468 Build#166 16.09.01
//-- Glukhov ObjAttachment 11.10.00

procedure TView.Draw
  (
  PInfo: PPaint;
  Clipping: Boolean
  );
begin
  Abstract;
end;

{******************************************************************************

Proc for Transformin coords with Projection

*******************************************************************************}
procedure TView.DrawTransform
  (
  PInfo: PPaint;
  Clipping: Boolean
  );
begin
  Abstract;
end;


function TView.SelectByRect
  (
  PInfo: PPaint;
  Rect: TDRect;
  Inside: Boolean
  )
  : Boolean;
begin
  SelectByRect := ClipRect.IsInside(Rect);
end;

//++ Glukhov Bug#468 Build#166 16.09.01
// Gets the object data to edit it, using the special component.
// P1 is a left lower corner of border, p4 is a left upper one.

procedure TView.InitObjEdit(
  PInfo: PPaint; var P1, P2, P3, P4: TGrPoint;
  var bRotate, bResize, bSimilar, bMove: Boolean);
begin
  P1 := GrPoint(ClipRect.A.X, ClipRect.A.Y);
  P2 := GrPoint(ClipRect.B.X, ClipRect.A.Y);
  P3 := GrPoint(ClipRect.B.X, ClipRect.B.Y);
  P4 := GrPoint(ClipRect.A.X, ClipRect.B.Y);
  bRotate := False;
  bResize := False;
  bSimilar := True;
  bMove := True;
end;

// Modifies the object after its editing, using the special component.

procedure TView.ExecObjEdit(PInfo: PPaint; P1, P2, P3, P4: TGrPoint);
begin
end;
//-- Glukhov Bug#468 Build#166 16.09.01

procedure TView.MoveRel(XMove, YMove: LongInt);
begin
  ClipRect.A.Move(XMove, YMove);
  ClipRect.B.Move(XMove, YMove);
//++ Glukhov Bug#454 Build#161 15.06.01
  if GetObjType = ot_Image then
  begin
    PImage(@Self)^.ScaledClipRect.A.Move(XMove, YMove);
    PImage(@Self)^.ScaledClipRect.B.Move(XMove, YMove);
    PImage(@Self).MakeRotFromClip;
  end;
//-- Glukhov Bug#454 Build#161 15.06.01
end;

{**************************************************************************************}
{ Procedure TView.Invalidate                                                           }
{  Erkl�rt den Bereich des Objektes (Brgrenzungsrechteck) am Bildschirm als ung�ltig.  }
{  Beim n�chsten Bildschirm-Update wird dieser Bereich gel�scht und neu dargestellt.   }
{**************************************************************************************}

procedure TView.Invalidate
  (
  PInfo: PPaint;
  FromGraphic: Boolean
  );
var
  Rect: TRect;
  ARect: TDRect;
begin
  ARect.InitByRect(ClipRect);
  ARect.Grow(PInfo^.CalculateDraw(PInfo^.LineWidth + 10));
  PInfo^.ConvertToPhysRect(ARect, Rect);
  InflateRect(Rect, 1, 1);
  InvalidateRect(PInfo^.HWindow, @Rect, TRUE);
  ARect.Done;
  if FromGraphic then
    PInfo^.FromGraphic := TRUE;
end;

function TView.CheckMove
  (
  PInfo: PPaint;
  XMove: LongInt;
  YMove: LongInt
  )
  : Boolean;
var
  XNeu: Double;
  YNeu: Double;
begin
  CheckMove := FALSE;
  XNeu := ClipRect.A.X;
  XNeu := XNeu + XMove;
  YNeu := ClipRect.A.Y;
  YNeu := YNeu + YMove;
  if (Abs(XNeu) <= msProject) and (Abs(YNeu) <= msProject) then
  begin
    XNeu := ClipRect.B.X + XMove;
    YNeu := ClipRect.B.Y + YMove;
    if (Abs(XNeu) <= msProject) and (Abs(YNeu) <= msProject) then
      CheckMove := TRUE;
  end;
end;

procedure TView.GetSize
  (
  var ABorder: TDRect
  );
begin
  ABorder.CorrectByRect(ClipRect);
  if ClipRect.A.X < 239447088 then
  begin
    Index := Index;
  end;
  if ClipRect.A.Y < 309552 then
  begin
    Index := Index;
  end;
end;

function TView.MakeCopy
  : PView;
begin
  MakeCopy := MakeObjectCopy(@self);
end;

function TView.GivePosX: LongInt;
begin
  Abstract;
end;

function TView.GivePosY: LongInt;
begin
  Abstract;
end;

function TView.GiveAPosX: LongInt;
begin
  Abstract;
end;

function TView.GiveAPosY: LongInt;
begin
  Abstract;
end;

function TView.GivePosXMin: LongInt;
begin
  GivePosXMin := ClipRect.A.X;
end;

function TView.GivePosYMin: LongInt;
begin
  GivePosYMin := ClipRect.A.Y;
end;

function TView.GivePosXMax: LongInt;
begin
  GivePosXMax := ClipRect.B.X;
end;

function TView.GivePosYMax: LongInt;
begin
  GivePosYMax := ClipRect.B.Y;
end;

function TView.GiveRadius: LongInt;
begin
  Abstract;
end;

function TView.GiveBegAngle: Real;
begin
  Abstract;
end;

function TView.GiveEndAngle: Real;
begin
  Abstract;
end;

procedure TView.Scale {2705F}
  (
  XScale: Real;
  YScale: Real
  );
begin
  ClipRect.Scale(XScale, YScale);
end; {2705F}

procedure TView.UpdateData;
var
  APInfo: PPaint;
  ALayer: PLayer;
begin
  APInfo := WingisMainForm.ActualChild.Data^.PInfo;
  ALayer := WingisMainForm.ActualChild.Data^.Layers^.TopLayer;
  if (APInfo <> nil) and (ALayer <> nil) then
  begin
    MU.LogUpdObjectData(APInfo, ALayer, @Self);
  end;
end;

const
  RView: TStreamRec = (
    ObjType: rn_View;
    VmtLink: TypeOf(TView);
    Load: @TView.Load;
    Store: @TView.Store);

begin
  RegisterType(RView);
end.

