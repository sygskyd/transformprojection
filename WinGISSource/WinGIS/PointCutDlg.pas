unit PointCutDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  MultiLng, AM_Proj, AM_Layer, StdCtrls, AM_Def, LayerHndl;

type
  TPointCutForm = class(TForm)
    MlgSection: TMlgSection;
    GroupBoxLayer1: TGroupBox;
    GroupBoxLayer2: TGroupBox;
    GroupBoxDLayer: TGroupBox;
    ButtonOK: TButton;
    ButtonCancel: TButton;
    ComboBoxLayer1: TComboBox;
    ComboBoxLayer2: TComboBox;
    ComboBoxDLayer: TComboBox;
    CheckBoxSymbol: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ComboBoxLayerChange(Sender: TObject);
  public
    Project: PProj;
    Layer1: PLayer;
    Layer2: PLayer;
    DLayer: PLayer;
  end;

var
  PointCutForm: TPointCutForm;

implementation

{$R *.DFM}

procedure TPointCutForm.FormShow(Sender: TObject);

    Procedure DoAll(Item: PLayer);
    var
    s: AnsiString;
    begin
         if (not Item^.GetState(sf_LayerOff)) and (Item^.Text <> nil) then begin
            s:=Item^.Text^;
            ComboBoxLayer1.Items.Add(s);
            ComboBoxLayer2.Items.Add(s);
            ComboBoxDLayer.Items.Add(s);
         end;
    end;

begin
     Project^.Layers^.LData^.ForEach(@DoAll);
end;

procedure TPointCutForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     Layer1:=Project^.Layers^.NameToLayer(ComboBoxLayer1.Text);
     Layer2:=Project^.Layers^.NameToLayer(ComboBoxLayer2.Text);
     DLayer:=Project^.Layers^.NameToLayer(ComboBoxDLayer.Text);
     if (ModalResult = mrOk) and (DLayer = nil) then begin
        DLayer:=NewLayerDialog(Project,GetLangText(10154),Trim(ComboBoxDLayer.Text));
     end;
end;

procedure TPointCutForm.ComboBoxLayerChange(Sender: TObject);
begin
     if (ComboBoxLayer1.ItemIndex = -1)
     or (ComboBoxLayer2.ItemIndex = -1)
     or (ComboBoxLayer1.ItemIndex = ComboBoxLayer2.ItemIndex)
     or (ComboBoxLayer1.ItemIndex = ComboBoxDLayer.ItemIndex)
     or (ComboBoxLayer2.ItemIndex = ComboBoxDLayer.ItemIndex)
     or (Trim(ComboBoxDLayer.Text) = '') then begin
        ButtonOk.Enabled:=False;
     end
     else begin
        ButtonOk.Enabled:=True;
     end;
end;

end.
