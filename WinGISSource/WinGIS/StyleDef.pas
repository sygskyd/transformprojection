{******************************************************************************+
  Unit StyleDef
--------------------------------------------------------------------------------
  Autor: Martin Forst
--------------------------------------------------------------------------------
  Enth�lt die Definitionen der Linien- und Schraffurstile f�r die Oberf�che
  und die Dialoge.
--------------------------------------------------------------------------------
  - TSelectOption
  - TSelectOptionListType
  - TSelectOptionList
  - TCustomPropertyCollector
  - TFillProperties
  - TLineProperties
  - TSymbolFillProperties
  - TLayerProperties
--------------------------------------------------------------------------------
  Version 1.0, 15-04-1997
  Changed:
               11-JUL-2000 by Sygsky in TLayerProperties.GetGeneralize
+******************************************************************************}
unit StyleDef;

interface

uses AM_Font, Classes, Graphics, Measures, PropCollect, StdCtrls, Validate, AM_Def, AM_Paint;

const
  intNone = -1;
  intFromLayer = -2;
  intFromObject = -3;

  ptNoChange = intNoChange;
  ptNotSet = intNotSet;

  lwNoChange = dblNoChange;
  lwNotSet = dblNotSet;

  ltNoChange = intNoChange;
  ltNotSet = intNotSet;

  lt_UserDefined = 1000; { number of first user-defined linestyle }

  stNoChange = intNoChange;
  stNotSet = intNotSet;

  symNoChange = intNoChange;
  symNotSet = intNotSet;

  symfNoChange = intNoChange;
  symfNotSet = intNotSet;

  taLeft = 0;
  taCentered = 1;
  taRight = 2;
  taNotSet = intNotSet;
  taNoChange = intNoChange;
  taFromLayer = intFromLayer;
  taFromObject = intFromObject;

  sNone = #5'#%$NONE$%#';
  sFromLayer = #4'#%$FROMLAYER$%#';
  sFromObject = #3'#%$FROMOBJECT$%#';

      { Konstanten f�r TLayerProperties.Generalize                                  }
  gtNoChange = intNoChange;
  gtShowAlways = 0;
  gtSmallerScale = 1;
  gtBiggerScale = 2;
  gtScaleRange = 3;

  sotDefaultInt = 0;
  sotColor = 1;
  sotFillStyle = 2;
  sotLineStyle = 3;
  sotSize = 4;
  sotSymbolFillType = 5;
  sotSymbol = 6;
   {brovak - BUG ##83,109,110 BUILD127}
  sotText = 7;
  {brovak}
  sotGeneral=8;

  soNone = 0;
  soLayer = 1;
  soObject = 2;
  
  //brovak Attention ! It's new settings of constant BUG#39 BUILD127
  GENERALIZE_MIN: double = 1E-200; { Minimum value to mark absence of data }
  GENERALIZE_MAX: double = 1E+200; { Maximum value to mark absence of data }
  GENERALIZE_MIN_OLD: double = 1E-300; { Minimum value to mark absence of data }
  GENERALIZE_MAX_OLD: double = 1E+300; { Maximum value to mark absence of data }


type {**************************************************************************
     | Class TFillProperties
     |--------------------------------------------------------------------------
     | Einstellungen f�r den Schraffurstil.
     +*************************************************************************}
  TFillProperties = class(TPropertyCollector)
  protected
    procedure SetPropertyMode(AMode: TPropertyMode); override;
  public
    constructor Create;
    property BackColor: TColor index 0 read GetColorVal write SetColorVal;
    property BackColorOptions: TSelectOptionList index 0 read GetColorOptionList;
    property ForeColor: TColor index 1 read GetColorVal write SetColorVal;
    property ForeColorOptions: TSelectOptionList index 1 read GetColorOptionList;
    property Pattern: LongInt index 0 read GetIntVal write SetIntVal;
    property PatternOptions: TSelectOptionList index 0 read GetIntOptionList;
    property Scale: Double index 0 read GetDoubleVal write SetDoubleVal;
    property ScaleOptions: TSelectOptionList index 0 read GetDoubleOptionList;
    property ScaleType: LongInt index 1 read GetIntVal write SetIntVal;
    procedure Validate;
  end;

     {**************************************************************************
     | Class TLineProperties
     |--------------------------------------------------------------------------
     | Einstellungen f�r den Linienstil.
     +*************************************************************************}
  TLineProperties = class(TPropertyCollector)
  public
    constructor Create;
    property Color: TColor index 0 read GetColorVal write SetColorVal;
    property ColorOptions: TSelectOptionList index 0 read GetColorOptionList;
    property FillColor: TColor index 1 read GetColorVal write SetColorVal;
    property FillColorOptions: TSelectOptionList index 1 read GetColorOptionList;
    property Scale: Double index 1 read GetDoubleVal write SetDoubleVal;
    property ScaleOptions: TSelectOptionList index 1 read GetDoubleOptionList;
    property Style: LongInt index 0 read GetIntVal write SetIntVal;
    property StyleOptions: TSelectOptionList index 0 read GetIntOptionList;
    procedure Validate;
    property Width: Double index 0 read GetDoubleVal write SetDoubleVal;
    property WidthOptions: TSelectOptionList index 0 read GetDoubleOptionList;
    property WidthType: LongInt index 1 read GetIntVal write SetIntVal;
  end;

     {**************************************************************************
     | Class TSymbolFillProperties
     |--------------------------------------------------------------------------
     | Einstellungen f�r die Symbolf�llung.
     +*************************************************************************}
  TSymbolFillProperties = class(TPropertyCollector)
  private
    FFillStyle: TFillProperties;
    FLineStyle: TLineProperties;
  protected
    procedure AssignTo(Dest: TPersistent); override;
    procedure SetPropertyMode(AMode: TPropertyMode); override;
  public
    constructor Create;
    destructor Destroy; override;
    property Angle: Double index 0 read GetDoubleVal write SetDoubleVal;
    property AngleOptions: TSelectOptionList index 0 read GetDoubleOptionList;
    property Distance: Double index 1 read GetDoubleVal write SetDoubleVal;
    property DistanceOptions: TSelectOptionList index 1 read GetDoubleOptionList;
    property DistanceType: Integer index 2 read GetIntVal write SetIntVal;
    property FillType: LongInt index 0 read GetIntVal write SetIntVal;
    property FillTypeOptions: TSelectOptionList index 0 read GetIntOptionList;
    property FillStyle: TFillProperties read FFillStyle;
    property LineDistance: Double index 2 read GetDoubleVal write SetDoubleVal;
    property LineDistanceOptions: TSelectOptionList index 2 read GetDoubleOptionList;
    property LineStyle: TLineProperties read FLineStyle;
    property Offset: Double index 3 read GetDoubleVal write SetDoubleVal;
    property OffsetOptions: TSelectOptionList index 3 read GetDoubleOptionList;
    property OffsetType: Integer index 4 read GetIntVal write SetIntVal;
    property SizeType: Integer index 3 read GetIntVal write SetIntVal;
    property Size: Double index 4 read GetDoubleVal write SetDoubleVal;
    property SizeOptions: TSelectOptionList index 4 read GetDoubleOptionList;
    property Symbol: LongInt index 1 read GetIntVal write SetIntVal;
    property SymbolOptions: TSelectOptionList index 1 read GetIntOptionList;
    property SymbolAngle: Double index 5 read GetDoubleVal write SetDoubleVal;
    property SymbolAngleOptions: TSelectOptionList index 5 read GetDoubleOptionList;
    function ValidValues: Boolean; override;
  end;

     {**************************************************************************
     | Class TFillProperties
     |--------------------------------------------------------------------------
     | Einstellungen f�r den Layerstil (verwendet von TLayerDialog
     +*************************************************************************}
  TLayerProperties = class(TPropertyCollector)
  private
    FFillStyle: TFillProperties;
    FIndex: LongInt;
    FHSDLayer: Boolean;
    FLineStyle: TLineProperties;
    FName: string;
    FNumber: LongInt;
    FSymbolFill: TSymbolFillProperties;
    function GetGeneralize: Integer;
    procedure SetName(const AName: string);
  protected
    procedure AssignTo(Dest: TPersistent); override;
    procedure SetPropertyMode(AMode: TPropertyMode); override;
  public
    constructor Create;
    destructor Destroy; override;
    property Editable: ShortInt index 0 read GetBooleanVal write SetBooleanVal;
    property GeneralMax: Double index 0 read GetDoubleVal write SetDoubleVal;
    property GeneralMin: Double index 1 read GetDoubleVal write SetDoubleVal;
    property Generalize: Integer read GetGeneralize;
    property HSDVisible: ShortInt index 4 read GetBooleanVal write SetBooleanVal;
    property HSDLayer: Boolean read FHSDLayer write FHSDLayer;
    property LineStyle: TLineProperties read FLineStyle;
    property FillStyle: TFillProperties read FFillStyle;
    property Index: LongInt read FIndex write FIndex;
    property Name: string read FName write SetName;
    property Number: LongInt read FNumber write FNumber;
    property Printable: ShortInt index 1 read GetBooleanVal write SetBooleanVal;
    property SymbolFill: TSymbolFillProperties read FSymbolFill;
    property UseForSnap: ShortInt index 2 read GetBooleanVal write SetBooleanVal;
    property Visible: ShortInt index 3 read GetBooleanVal write SetBooleanVal;
    function ValidValues: Boolean; override;
  end;

  TTextProperties = class(TPropertyCollector)
  public
    constructor Create;
    property FontName: AnsiString index 0 read GetStringVal write SetStringVal;
    property FontNameOptions: TSelectOptionList index 0 read GetStringOptionList;
    property Alignment: LongInt index 0 read GetIntVal write SetIntVal;
    property AlignmentOptions: TSelectOptionList index 0 read GetIntOptionList;
    property Bold: ShortInt index 2 read GetBooleanVal write SetBooleanVal;
    property FixedHeight: ShortInt index 0 read GetBooleanVal write SetBooleanVal;
    property Height: Double index 0 read GetDoubleVal write SetDoubleVal;
    property HeightOptions: TSelectOptionList index 0 read GetDoubleOptionList;
    property Italic: ShortInt index 1 read GetBooleanVal write SetBooleanVal;
    property Rotation: Double index 1 read GetDoubleVal write SetDoubleVal;
    property Transparent: ShortInt index 4 read GetBooleanVal write SetBooleanVal;
    property Underlined: ShortInt index 3 read GetBooleanVal write SetBooleanVal;
    {+++brovak BUG83 BUILD17 }
    property AngleOptions: TSelectOptionList index 1 read GetDoubleOptionList;
    {---brovak}
    {+++brovak BUG#32 BUILD 127}
    property BackColor: TColor index 0 read GetColorVal write SetColorVal;
    property BackColorOptions: TSelectOptionList index 0 read GetColorOptionList;
    property ForeColor: TColor index 1 read GetColorVal write SetColorVal;
    property ForeColorOptions: TSelectOptionList index 1 read GetColorOptionList;
    {--brovak}

  end;

  TBitmapProperties = class(TPropertyCollector)
  public
    constructor Create;
    property ShowBitmap: ShortInt index 0 read GetBooleanVal write SetBooleanVal;
    property ShowFrame: ShortInt index 1 read GetBooleanVal write SetBooleanVal;
    property Transparency: LongInt index 0 read GetIntVal write SetIntVal;
    property TransparencyOptions: TSelectOptionList index 0 read GetIntOptionList;
    property TransparencyType: LongInt index 1 read GetIntVal write SetIntVal;
  end;

  TGeneralizeStyle = class(TPropertyCollector)
  public
    constructor Create;
    property GeneralizeType: LongInt index 0 read GetIntVal write SetIntVal;
    property GeneralizeOptions: TSelectOptionList index 0 read GetIntOptionList;
    property GeneralizeMin: Double index 0 read GetDoubleVal write SetDoubleVal;
    property GeneralizeMax: Double index 1 read GetDoubleVal write SetDoubleVal;
   {brovak BUILD127 BUG#39}
    property GeneralizeMinOptions: TSelectOptionList index 0 read GetDoubleOptionList;
    property GeneralizeMaxOptions: TSelectOptionList index 1 read GetDoubleOptionList;
   {--brovak}
  end;

  TSymbolProperties = class(TPropertyCollector)
  public
    constructor Create;
    property Size: Double index 0 read GetDoubleVal write SetDoubleVal;
    property SizeOptions: TSelectOptionList index 0 read GetDoubleOptionList;
    property SizeType: Integer index 0 read GetIntVal write SetIntVal;
    property Symbol: Integer index 1 read GetIntVal write SetIntVal;
    property Angle: Double index 1 read GetDoubleVal write SetDoubleVal;
    property AngleOptions: TSelectOptionList index 1 read GetDoubleOptionList;
  end;

procedure SetupSelectOptions(AList: TSelectOptionList; AType: Integer; AOptions: TSelectOptions);

procedure GetSelectionValues(AType, AOption: Integer; var LongName, ShortName: string;
  var Value: Double);

procedure SetDoubleField(const Value, Scale: Double; Validator: TMeasureLookupValidator;
  AUnits: TMeasureUnits; Options: TSelectOptionList);

function GetDoubleField(const Scale: Double; Validator: TMeasureLookupValidator;
  AUnits: TMeasureUnits; Options: TSelectOptionList): Double;

procedure SetIntegerField(const Value, Scale: Integer; Validator: TMeasureLookupValidator;
  AUnits: TMeasureUnits; Options: TSelectOptionList);

function GetIntegerField(const Scale: Integer; Validator: TMeasureLookupValidator;
  AUnits: TMeasureUnits; Options: TSelectOptionList): Integer;

function GetSizeField(Validator: TMeasureLookupValidator; Options: TSelectOptionList;
  Checkbox: TCheckBox): Double;

function GetSizeTypeField(Validator: TMeasureLookupValidator; Options: TSelectOptionList;
  Checkbox: TCheckBox): Integer;

procedure SetSizeField(const Value: Double; SizeType: Integer; Validator: TMeasureLookupValidator;
  Options: TSelectOptionList; CheckBox: TCheckBox);

procedure SetSymSizeField(const Value: Double; SizeType: Integer; Validator: TMeasureLookupValidator);

  procedure InitializeLineStyle(LineStyle: TLineProperties);

procedure CollectLineStyle(ObjLineStyle: PLineStyle; LineStyle: TLineProperties);

function UpdateLineStyle(ObjLineStyle: PLineStyle; LineStyle: TLineProperties): Boolean;

procedure InitializeFillStyle(FillStyle: TFillProperties);

procedure CollectFillStyle(ObjFillStyle: PFillStyle; FillStyle: TFillProperties);

function UpdateFillStyle(ObjFillStyle: PFillStyle; FillStyle: TFillProperties): Boolean;

{changed by brovak for new text properties}
procedure CollectFontStyle(const FontData: PFontData; const FontDes: PFontDes;
  TextStyle: TTextProperties; const ARotation: Double;AForegroundColor,ABackGroundColor:TColor);
{--brovak}
procedure InitializeSymbolFillStyle(SymbolFill: TSymbolFillProperties);

procedure CollectSymbolFill(ObjFillStyle: PSymbolFill; SymbolFillStyle: TSymbolFillProperties);

function UpdateSymbolFillStyle(ObjSymbolFill: PSymbolFill; SymbolFillStyle: TSymbolFillProperties): Boolean;

function GetChecked(Control: TCheckBox): ShortInt;

procedure SetChecked(Control: TCheckBox; State: ShortInt);

// calculates the line-style to use from an items-line-style and it's parent line-style
procedure CalculateLineStyle(const Style, ParentStyle: TLineStyle; var UseStyle: TLineStyle);

// calculates the fill-style to use from an items-fill-style and it's parent fill-style
procedure CalculateFillStyle(const Style, ParentStyle: TFillStyle; var UseStyle: TFillStyle);

// calculates the symbolfill-style to use from an items-symbolfill-style and it's parent symbolfill-style
procedure CalculateSymbolFill(const Style, ParentStyle: TSymbolFill; var UseStyle: TSymbolFill);

// updates the use-counts of the xstyles and symbol-fills used by the styles
procedure UpdateUseCounts(PInfo: PPaint; const LineStyle: TLineStyle;
  const FillStyle: TFillStyle; const SymbolFill: TSymbolFill; AddRef: Boolean);

implementation

uses NumTools, UserIntf, WinProcs, XLines, XFills, SysUtils;

const
  Tolerance = 1E-10;

{==============================================================================}
{ TFillProperties                                                                   }
{==============================================================================}

constructor TFillProperties.Create;
begin
  inherited Create;
  ColorProps := 2;
  DoubleProps := 1;
  IntProps := 2;
  SetupSelectOptions(BackColorOptions, sotColor, [soNone]);
  SetupSelectOptions(ForeColorOptions, sotColor, []);
  SetupSelectOptions(PatternOptions, sotFillStyle, [soNone]);
  SetupSelectOptions(ScaleOptions, sotSize, []);
  Pattern := pt_NoPattern;
  ForeColor := ColorToRGB(clBlack);
  BackColor := clNone;
  Scale := 1;
  ScaleType := stScaleIndependend;
end;

procedure TFillProperties.SetPropertyMode
  (
  AMode: TPropertyMode
  );
begin
  inherited SetPropertyMode(AMode);
  if PropertyMode = pmCollect then
  begin
    BackColorOptions.Options := [soNone];
    ForeColorOptions.Options := [];
    PatternOptions.Options := [soNone];
  end;
end;

procedure TFillProperties.Validate;
begin
  if Pattern < 0 then Pattern := 0;
  if Pattern = pt_NoPattern then
  begin
    ForeColor := clNone;
    BackColor := clNone;
  end
  else
    if Pattern = pt_Solid then BackColor := clNone;
end;

{==============================================================================}
{ TSymbolFillProperties                                                                  }
{==============================================================================}

constructor TSymbolFillProperties.Create;
begin
  inherited Create;
  OptionProps := 1;
  DoubleProps := 6;
  IntProps := 5;
  Distance := 10000;
  DistanceType := stDrawingUnits;
  SetupSelectOptions(DistanceOptions, sotSize, []);
  FillType := symfNone;
  SetupSelectOptions(FillTypeOptions, sotSymbolFillType, [soNone]);
  Offset := 0;
  OffsetType := stPercent;
  SetupSelectOptions(OffsetOptions, sotSize, []);
  LineDistance := 10000;
  SetupSelectOptions(LineDistanceOptions, sotSize, []);
  Size := dblFromObject;
  SizeType := stProjectScale;
  SetupSelectOptions(SizeOptions, sotSymbol, [soObject]);
  Symbol := symNone;
  SetupSelectOptions(SymbolOptions, sotSymbol, []);
  Angle := 0;
  SetupSelectOptions(AngleOptions, sotSize, []);
  SymbolAngle := dblFromObject;
  SetupSelectOptions(SymbolAngleOptions, sotSymbol, [soObject]);
  FLineStyle := TLineProperties.Create;
  with FLineStyle do
  begin
    Style := ltFromObject;
    Color := clFromObject;
    FillColor := clFromObject;
    Width := lwFromObject;
    Scale := dblFromObject
  end;
  FFillStyle := TFillProperties.Create;
  with FFillStyle do
  begin
    Pattern := ptFromObject;
    ForeColor := clFromObject;
    BackColor := clFromObject;
  end;
  DistanceType := stDrawingUnits;
  SizeType := stPercent;
end;

destructor TSymbolFillProperties.Destroy;
begin
  FLineStyle.Free;
  FFillStyle.Free;
  inherited Destroy;
end;

procedure TSymbolFillProperties.SetPropertyMode
  (
  AMode: TPropertyMode
  );
begin
  inherited SetPropertyMode(AMode);
  FLineStyle.PropertyMode := AMode;
  FFillStyle.PropertyMode := AMode;
end;

procedure TSymbolFillProperties.AssignTo
  (
  Dest: TPersistent
  );
begin
  inherited AssignTo(Dest);
  with Dest as TSymbolFillProperties do
  begin
    FillStyle.Assign(Self.FFillStyle);
    LineStyle.Assign(Self.FLineStyle);
  end;
end;

function TSymbolFillProperties.ValidValues
  : Boolean;
begin
  Result := inherited ValidValues and FillStyle.ValidValues and
    LineStyle.ValidValues;
end;

{==============================================================================}
{ TLineProperties                                                                   }
{==============================================================================}

constructor TLineProperties.Create;
begin
  inherited Create;
  DoubleProps := 2;
  ColorProps := 2;
  IntProps := 2;
  SetupSelectOptions(ColorOptions, sotColor, []);
  SetupSelectOptions(FillColorOptions, sotColor, []);
  SetupSelectOptions(StyleOptions, sotLineStyle, []);
  SetupSelectOptions(WidthOptions, sotSize, []);
  SetupSelectOptions(ScaleOptions, sotSize, []);
  Style := lt_Solid;
  Color := ColorToRGB(clBlack);
  FillColor := ColorToRGB(clBlack);
  Width := 0;
  WidthType := stScaleIndependend;
  Scale := 1;
end;

procedure TLineProperties.Validate;
begin
  if Style < lt_Solid then Style := lt_Solid;
  if (Style <> lt_Solid) and (Style < lt_UserDefined) then
  begin
//    Width := 0;
//    WidthType := stDrawingUnits;
  end;
end;

{===============================================================================
| TLayerProperties
+==============================================================================}

constructor TLayerProperties.Create;
begin
  inherited Create;
  DoubleProps := 2;
  BooleanProps := 5;
  FFillStyle := TFillProperties.Create;
  FLineStyle := TLineProperties.Create;
  FSymbolFill := TSymbolFillProperties.Create;
  Printable := bsYes;
  Visible := bsYes;
  Editable := bsYes;
  UseForSnap := bsYes;
  GeneralMin := GENERALIZE_MIN;
  GeneralMax := GENERALIZE_MAX;
end;

destructor TLayerProperties.Destroy;
begin
  FFillStyle.Free;
  FLineStyle.Free;
  FSymbolFill.Free;
  inherited Destroy;
end;

procedure TLayerProperties.SetPropertyMode(AMode: TPropertyMode);
begin
  inherited SetPropertyMode(AMode);
  FFillStyle.PropertyMode := AMode;
  FLineStyle.PropertyMode := AMode;
  FSymbolFill.PropertyMode := AMode;
end;

procedure TLayerProperties.SetName(const AName: string);
var
s: String;
begin
  if Editable <> 0 then begin
     s:=ValidateLayername(AName);
     if (Length(s) > 0) then
        FName := s;
  end;
end;

{++Sygsky: change to correctly handle values }
function TLayerProperties.GetGeneralize: Integer;
begin
  Result := gtShowAlways; { Always shown }
//  if (GeneralMin < 0) or (GeneralMax < 0) then
//    Exit;
  if GeneralMin > 0 then
    if GeneralMin > GENERALIZE_MIN then
      Inc(Result, gtBiggerScale);
  if GeneralMax > 0 then
    if GeneralMax < GENERALIZE_MAX then
      Inc(Result, gtSmallerScale);
end;

procedure TLayerProperties.AssignTo(Dest: TPersistent);
begin
  inherited AssignTo(Dest);
  with Dest as TLayerProperties do
  begin
    FIndex := Self.FIndex;
    FName := Self.FName;
    FNumber := Self.FNumber;
    FillStyle.Assign(Self.FFillStyle);
    LineStyle.Assign(Self.FLineStyle);
    SymbolFill.Assign(Self.FSymbolFill);
    FHSDLayer := Self.FHSDLayer;
  end;
end;

function TLayerProperties.ValidValues: Boolean;
begin
  Result := inherited ValidValues and FillStyle.ValidValues
    and LineStyle.ValidValues and SymbolFill.ValidValues;
end;

{===============================================================================
| TTextProperties
+==============================================================================}

constructor TTextProperties.Create;
begin
  inherited Create;
  StringProps := 1;
  IntProps := 1;
  DoubleProps := 2;
  BooleanProps := 5;
  ColorProps := 2;
{+++brovak BUG 83,109,110 BUILD 127}
  Rotation := 0;
  SetupSelectOptions(AngleOptions, sotText, []);
//++ Glukhov NonzeroText Build#166 27.11.01
// Height := 0;
  Height:= cMinFontHeight;
  FontName:= 'Arial';
//-- Glukhov NonzeroText Build#166 27.11.01
  SetupSelectOptions(HeightOptions,sotText, []);
  Alignment := 0;
  SetupSelectOptions(AlignmentOptions,sotText, []);
  {brovak}
{+++brovak BUG 32 BUILD 127}

  SetupSelectOptions(BackColorOptions, sotColor, [soLayer]);
  SetupSelectOptions(ForeColorOptions, sotColor, [soLayer]);
  ForeColor := ColorToRGB(clBlack);
  BackColor := clNone;
 {brovak}
end;

{===============================================================================
| TBitmapProperties
+==============================================================================}

constructor TBitmapProperties.Create;
begin
  inherited Create;
  IntProps := 2;
  BooleanProps := 2;
  SetupSelectOptions(TransparencyOptions, sotDefaultInt, []);
end;

{==============================================================================+
  TGeneralizeStyle
+==============================================================================}

constructor TGeneralizeStyle.Create;
begin
  inherited Create;
  IntProps := 1;
  DoubleProps := 2;
  {+++brovak BUG#39 BUILD 127}
  SetupSelectOptions(GeneralizeOptions,sotDefaultInt,[]);
  SetupSelectOptions(GeneralizeMinOptions,SotGeneral,[]);
  SetupSelectOptions(GeneralizeMaxOptions,SotGeneral,[]);
  {---brovak}
end;

{===============================================================================
  TSymbolProperties
+==============================================================================}

constructor TSymbolProperties.Create;
begin
  inherited Create;
  IntProps := 2;
  DoubleProps := 2;
  SetupSelectOptions(AngleOptions, sotSymbol, []);
  SetupSelectOptions(SizeOptions, sotSymbol, []);
end;

procedure SetDoubleField(const Value, Scale: Double; Validator: TMeasureLookupValidator;
  AUnits: TMeasureUnits; Options: TSelectOptionList);
begin
  if Options <> nil then
  begin
    Options.AddLongNamesToList(Validator.LookupList);
    if DblCompare(Value, dblNone) = 0 then
      Validator.LookupIndex := Options.IndexOf(soNone)
    else
      if DblCompare(Value, dblFromLayer) = 0 then
        Validator.LookupIndex := Options.IndexOf(soLayer)
      else
        if DblCompare(Value, dblFromObject) = 0 then
          Validator.LookupIndex := Options.IndexOf(soObject)
        else
          if DblCompare(Value, dblNoChange) <= 0 then
            Validator.LookupIndex := Options.IndexOf(soNoChange)
          else
            Validator.LookupIndex := -1;
  end
  else
    Validator.LookupIndex := -1;
  if Validator.LookupIndex < 0 then Validator[AUnits] := Value * Scale;
end;

procedure SetIntegerField(const Value, Scale: Integer; Validator: TMeasureLookupValidator;
  AUnits: TMeasureUnits; Options: TSelectOptionList);
begin
  if Options <> nil then
  begin
    Options.AddLongNamesToList(Validator.LookupList);
    if Value = intNone then
      Validator.LookupIndex := Options.IndexOf(soNone)
    else
      if Value = intFromLayer then
        Validator.LookupIndex := Options.IndexOf(soLayer)
      else
        if Value = intFromObject then
          Validator.LookupIndex := Options.IndexOf(soObject)
        else
          if Value <= intNoChange then
            Validator.LookupIndex := Options.IndexOf(soNoChange)
          else
            Validator.LookupIndex := -1;
  end
  else
    Validator.LookupIndex := -1;
  if Validator.LookupIndex < 0 then Validator[AUnits] := Value * Scale;
end;

function GetDoubleField(const Scale: Double; Validator: TMeasureLookupValidator;
  AUnits: TMeasureUnits; Options: TSelectOptionList): Double;
begin
  if Validator.LookupIndex >= 0 then
    Result := Options.DblValues[Validator.LookupIndex]
  else
    Result := Validator[AUnits] * Scale;
end;

function GetIntegerField(const Scale: Integer; Validator: TMeasureLookupValidator;
  AUnits: TMeasureUnits; Options: TSelectOptionList): Integer;
begin
  if Validator.LookupIndex >= 0 then
    Result := Options.Values[Validator.LookupIndex]
  else
    Result := Round(Validator[AUnits] * Scale);
end;

procedure SetSizeField
  (
  const Value: Double;
  SizeType: Integer;
  Validator: TMeasureLookupValidator;
  Options: TSelectOptionList;
  Checkbox: TCheckBox
  );
begin
  if SizeType = stPercent then
  begin
    Validator.Units := muPercent;
    SetDoubleField(Value, 100, Validator, muPercent, Options);
    if CheckBox <> nil then Checkbox.Checked := FALSE;
  end
  else
    if SizeType in [stProjectScale, stScaleIndependend] then
    begin
      SetDoubleField(Value, 1 / 10, Validator, muMillimeters, Options);
      if CheckBox <> nil then Checkbox.Checked := SizeType = stScaleIndependend;
    end
    else
    begin
      Validator.Units := muMillimeters;
      SetDoubleField(Value, 1 / 100, Validator, muMillimeters, Options);
      if CheckBox <> nil then Checkbox.Checked := FALSE;
    end;
end;

procedure SetSymSizeField
  (
  const Value: Double;
  SizeType: Integer;
  Validator: TMeasureLookupValidator
  );
begin
    if SizeType in [stProjectScale, stScaleIndependend] then begin
      Validator.Units:=muMillimeters;
      Validator.Value:=Value/10;
    end
    else begin
      Validator.Units:=muDrawingUnits;
      Validator.Value:=Value/100;
    end;
end;

function GetSizeField(Validator: TMeasureLookupValidator; Options: TSelectOptionList;
  Checkbox: TCheckBox): Double;
begin
  if Validator.LookupIndex >= 0 then
    Result := Options.DblValues[Validator.LookupIndex]
  else
    if Validator.Units = muPercent then
      Result := Validator[muPercent] / 100
    else
      if Validator.Units = muDrawingUnits then
        Result := Validator[muDrawingUnits] * 100
      else
        Result := Validator[muMillimeters] * 10;
end;

function GetSizeTypeField(Validator: TMeasureLookupValidator; Options: TSelectOptionList;
  Checkbox: TCheckBox): Integer;
begin
  if Validator.LookupIndex > 0 then
    Result := stNoChange
  else
    if Validator.Units = muPercent then
      Result := stPercent
    else
      if Validator.Units = muDrawingUnits then
        Result := stDrawingUnits
      else
        case Checkbox.State of
          cbChecked: Result := stScaleIndependend;
          cbUnChecked: Result := stProjectScale;
        else
          Result := stNoChange;
        end;
end;

procedure GetSelectionValues(AType, AOption: Integer; var LongName, ShortName: string;
  var Value: Double);
const
  Colors: array[0..3] of TColor = (clNone, clFromLayer, clFromObject, clNoChange);
  Lines: array[0..3] of Integer = (ltNone, ltFromLayer, ltFromObject, ltNoChange);
  Patterns: array[0..3] of Integer = (pt_NoPattern, ptFromLayer, ptFromObject, ptNoChange);
  Integers: array[0..3] of Integer = (intNone, intFromLayer, intFromObject, intNoChange);
  Doubles: array[0..3] of Double = (dblNone, dblFromLayer, dblFromObject, dblNoChange);
begin
  AOption := Min(AOption, 3);
  LongName := MlgStringList['SelectionNames', AOption + AType * 20 + 10];
  ShortName := MlgStringList['SelectionNames', AOption + AType * 20];
  case AType of
    sotColor: Value := Colors[AOption];
    sotLineStyle: Value := Lines[AOption];
    sotFillStyle: Value := Patterns[AOption];
    sotSize,
      sotSymbol,{++brovak}sotText,sotgeneral{--brovak}: Value := Doubles[AOption];
  else
    Value := Integers[AOption];
  end;
end;

procedure SetupSelectOptions(AList: TSelectOptionList; AType: Integer; AOptions: TSelectOptions);
begin
  AList.ListType := AType;
  AList.Options := AOptions;
  AList.GetSelectionValues := GetSelectionValues; 
end;

procedure InitializeLineStyle(LineStyle: TLineProperties);
begin
  with LineStyle do
  begin
    PropertyMode := pmCollect;
    with LineStyle do
    begin
      ColorOptions.Options := [soLayer];
      StyleOptions.Options := [soLayer];
      WidthOptions.Options := [soLayer];
      ScaleOptions.Options := [soLayer];
      FillColorOptions.Options := [soLayer];
    end;
  end;
end;

{******************************************************************************+
  Procedure CollectLineStyle
--------------------------------------------------------------------------------
+******************************************************************************}

procedure CollectLineStyle(ObjLineStyle: PLineStyle; LineStyle: TLineProperties);
begin
  if ObjLineStyle <> nil then
    with ObjLineStyle^ do
    begin
      LineStyle.Style := Style;
      LineStyle.Color := Color;
      LineStyle.Width := Width;
      LineStyle.WidthType := WidthType;
      LineStyle.Scale := Scale;
      LineStyle.FillColor := FillColor;
    end
  else
    with LineStyle do
    begin
      Style := ltFromLayer;
      Color := clFromLayer;
      Width := dblFromLayer;
      WidthType := intFromLayer;
      Scale := dblFromLayer;
      FillColor := clFromLayer;
    end;
end;

{******************************************************************************+
  Function UpdateLineStyle
--------------------------------------------------------------------------------
  Updates the line-style of an object. The properties are only changed
  if the new value is unequal to NoChange. Returns true if any property
  has been changed.
+******************************************************************************}

function UpdateLineStyle(ObjLineStyle: PLineStyle; LineStyle: TLineProperties): Boolean;
begin
  Result := FALSE;
  with LineStyle do
  begin
    if Style <> ltNoChange then Update(ObjLineStyle^.Style, Style, Result);
    if Color <> clNoChange then Update(Integer(ObjLineStyle^.Color), Color, Result);
    if Width <> dblNoChange then Update(ObjLineStyle^.Width, Width, Result);
    if WidthType <> intNoChange then Update(ObjLineStyle^.WidthType, WidthType, Result);
    if Scale <> dblNoChange then Update(ObjLineStyle^.Scale, Scale, Result);
    if FillColor <> clNoChange then Update(Integer(ObjLineStyle^.FillColor), FillColor, Result);
  end;
end;

procedure InitializeFillStyle(FillStyle: TFillProperties);
begin
  with FillStyle do
  begin
    PropertyMode := pmCollect;
    PatternOptions.Options := [soLayer];
    BackColorOptions.Options := [soLayer];
    ForeColorOptions.Options := [soLayer];
    ScaleOptions.Options := [soLayer];
  end;
end;

procedure CollectFillStyle(ObjFillStyle: PFillStyle; FillStyle: TFillProperties);
begin
  if ObjFillStyle <> nil then
    with ObjFillStyle^ do
    begin
      FillStyle.Pattern := Pattern;
      FillStyle.ForeColor := ForeColor;
      FillStyle.BackColor := BackColor;
      FillStyle.Scale := Scale;
      FillStyle.ScaleType := ScaleType;
    end
  else
    with FillStyle do
    begin
      Pattern := ptFromLayer;
      ForeColor := clFromLayer;
      BackColor := clFromLayer;
      Scale := dblFromLayer;
      ScaleType := stScaleIndependend;
    end;
end;

function UpdateFillStyle(ObjFillStyle: PFillStyle; FillStyle: TFillProperties): Boolean;
begin
  Result := FALSE;
  with FillStyle do
  begin
    if Pattern <> ptNoChange then Update(ObjFillStyle^.Pattern, Pattern, Result);
    if BackColor <> clNoChange then Update(Integer(ObjFillStyle^.BackColor), BackColor, Result);
    if ForeColor <> clNoChange then Update(Integer(ObjFillStyle^.ForeColor), ForeColor, Result);
    if Scale <> dblNoChange then Update(ObjFillStyle^.Scale, Scale, Result);
    if ScaleType <> intNoChange then
      Update(ObjFillStyle^.ScaleType, ScaleType, Result)
  end;
end;

procedure CollectFontStyle(const FontData: PFontData; const FontDes: PFontDes;
  TextStyle: TTextProperties; const ARotation: Double;AForegroundColor,ABackGroundColor:TColor);
(*nst AForeColor, ABackColor:integer)*)
begin
  with TextStyle do
  begin
    if FontDes <> nil then FontName := PToStr(FontDes^.Name);
    if FontData.Style and ts_Right <> 0 then
      Alignment := taRight
    else
      if FontData.Style and ts_Center <> 0 then
        Alignment := taCentered
      else
        Alignment := taLeft;
    Height := FontData.Height / 100.0;
    FixedHeight := BooleanStyle(FontData.Style and ts_FixHeight <> 0);
    Italic := BooleanStyle(FontData.Style and ts_Italic <> 0);
    Bold := BooleanStyle(FontData.Style and ts_Bold <> 0);
    Underlined := BooleanStyle(FontData.Style and ts_Underl <> 0);
    Transparent := BooleanStyle(FontData.Style and ts_Transparent <> 0);
    Rotation :=ARotation;
    {++brovak BUG#32 BUILD127}
    ForeColor:=AForegroundColor;
    BackColor:=ABackgroundColor;
    {--brovak}
  end;
end;

procedure InitializeSymbolFillStyle(SymbolFill: TSymbolFillProperties);
begin
  with SymbolFill do
  begin
    PropertyMode := pmCollect;
    FillTypeOptions.Options := [soLayer];
    DistanceOptions.Options := [soLayer];
    LineDistanceOptions.Options := [soLayer];
    OffsetOptions.Options := [soLayer];
    AngleOptions.Options := [soLayer];
    SymbolAngleOptions.Options := [soLayer];
    SizeOptions.Options := [soLayer];
    InitializeFillStyle(FillStyle);
    InitializeLineStyle(LineStyle);
  end;
end;

procedure CollectSymbolFill(ObjFillStyle: PSymbolFill; SymbolFillStyle: TSymbolFillProperties);
begin
  with SymbolFillStyle do begin

    if (ObjFillStyle <> nil) and (ObjFillStyle^.FillType < 0) then begin
       ObjFillStyle:=nil;
    end;

    if ObjFillStyle = nil then
    begin
      FillType := symfFromLayer;
      Angle := dblFromLayer;
      Distance := dblFromLayer;
      LineDistance := dblFromLayer;
      Offset := dblFromLayer;
      OffsetType := intFromLayer;
      SizeType := intFromLayer;
      Size := dblFromLayer;
      Symbol := intFromLayer;
      SymbolAngle := dblFromLayer;
      CollectLineStyle(nil, LineStyle);
      CollectFillStyle(nil, FillStyle);
    end
    else
    begin
      FillType := ObjFillStyle^.FillType;
      Angle := ObjFillStyle^.Angle;
      Distance := ObjFillStyle^.Distance;
      LineDistance := ObjFillStyle^.LineDistance;
      Offset := ObjFillStyle^.Offset;
      OffsetType := ObjFillStyle^.OffsetType;
      SizeType := ObjFillStyle^.SizeType;
      Size := ObjFillStyle^.Size;
      Symbol := ObjFillStyle^.Symbol;
      SymbolAngle := ObjFillStyle^.SymbolAngle;
      CollectLineStyle(@ObjFillStyle^.LineStyle, LineStyle);
      CollectFillStyle(@ObjFillStyle^.FillStyle, FillStyle);
    end;
  end;
end;

function UpdateSymbolFillStyle(ObjSymbolFill: PSymbolFill; SymbolFillStyle: TSymbolFillProperties): Boolean;
begin
  Result := FALSE;
  with SymbolFillStyle do
  begin
    if FillType <> symfNoChange then Update(ObjSymbolFill.FillType, FillType, Result);
    if Angle <> dblNoChange then Update(ObjSymbolFill.Angle, Angle, Result);
    if DistanceType <> intNoChange then Update(ObjSymbolFill.DistanceType, DistanceType, Result);
    if Distance <> dblNoChange then Update(ObjSymbolFill.Distance, Distance, Result);
    if LineDistance <> dblNoChange then Update(ObjSymbolFill.LineDistance, LineDistance, Result);
    if Offset <> dblNoChange then Update(ObjSymbolFill.Offset, Offset, Result);
    if OffsetType <> intNoChange then Update(ObjSymbolFill.OffsetType, OffsetType, Result);
    if SizeType <> intNoChange then Update(ObjSymbolFill.SizeType, SizeType, Result);
    if Size <> dblNoChange then Update(ObjSymbolFill.Size, Size, Result);
    if Symbol <> symNoChange then Update(ObjSymbolFill.Symbol, Symbol, Result);
    if SymbolAngle <> dblNoChange then Update(ObjSymbolFill.SymbolAngle, SymbolAngle, Result);
    if UpdateLineStyle(@ObjSymbolFill^.LineStyle, SymbolFillStyle.LineStyle) then
      Result := TRUE;
    if UpdateFillStyle(@ObjSymbolFill^.FillStyle, SymbolFillStyle.FillStyle) then
      Result := TRUE;
  end;
end;

function GetChecked(Control: TCheckBox): ShortInt;
begin
  case Control.State of
    cbChecked: Result := bsYes;
    cbUnchecked: Result := bsNo;
  else
    Result := bsNoChange;
  end;
end;

procedure SetChecked(Control: TCheckBox; State: ShortInt);
begin
  if State < 0 then
  begin
    Control.AllowGrayed := TRUE;
    Control.State := cbGrayed;
  end
  else
    Control.Checked := State = bsYes;
end;

procedure CalculateLineStyle(const Style, ParentStyle: TLineStyle; var UseStyle: TLineStyle);
begin
  with Style do
  begin
    if Style < 0 then
      UseStyle.Style := ParentStyle.Style
    else
      UseStyle.Style := Style;
    if DblCompare(Width, lwFromLayer) <= 0 then
    begin
      UseStyle.Width := ParentStyle.Width;
      UseStyle.WidthType := ParentStyle.WidthType;
    end
    else
    begin
      UseStyle.Width := Width;
      UseStyle.WidthType := WidthType;
    end;
    if Color < 0 then
      UseStyle.Color := ParentStyle.Color
    else
      UseStyle.Color := Color;
    if FillColor < 0 then
      UseStyle.FillColor := ParentStyle.FillColor
    else
      UseStyle.FillColor := FillColor;
    if DblCompare(Scale, dblFromLayer) <= 0 then
      UseStyle.Scale := ParentStyle.Scale
    else
      UseStyle.Scale := Scale;
  end;
end;

procedure CalculateFillStyle(const Style, ParentStyle: TFillStyle; var UseStyle: TFillStyle);
begin
  with Style do
  begin
    if Pattern < 0 then
      UseStyle.Pattern := ParentStyle.Pattern
    else
      UseStyle.Pattern := Pattern;
    if ForeColor < 0 then
      UseStyle.ForeColor := ParentStyle.ForeColor
    else
      UseStyle.ForeColor := ForeColor;
    if BackColor < 0 then
      UseStyle.BackColor := ParentStyle.BackColor
    else
      UseStyle.BackColor := BackColor;
    if DblCompare(Scale, dblFromLayer) <= 0 then
    begin
      UseStyle.Scale := ParentStyle.Scale;
      UseStyle.ScaleType := ParentStyle.ScaleType;
    end
    else
    begin
      UseStyle.Scale := Scale;
      UseStyle.ScaleType := ScaleType;
    end;
  end;
end;

procedure CalculateSymbolFill(const Style, ParentStyle: TSymbolFill; var UseStyle: TSymbolFill);
begin
  if Style.FillType <= symFromLayer then
    UseStyle := ParentStyle
  else
    UseStyle := Style;
  if Style.FillType = symfNone then exit;
  CalculateLineStyle(Style.LineStyle, ParentStyle.LineStyle, UseStyle.LineStyle);
  CalculateFillStyle(Style.FillStyle, ParentStyle.FillStyle, UseStyle.FillStyle);
end;

procedure UpdateUseCounts(PInfo: PPaint; const LineStyle: TLineStyle;
  const FillStyle: TFillStyle; const SymbolFill: TSymbolFill; AddRef: Boolean);
var
  ChangeBy: Integer;
  XLineStyle: TXLineStyle;
  XFillStyle: TCustomXFill;
begin
  if AddRef then
    ChangeBy := 1
  else
    ChangeBy := -1;
  if LineStyle.Style >= lt_UserDefined then
  begin
    XLineStyle := PInfo^.ProjStyles.XLineStyles[LineStyle.Style];
    if XLineStyle <> nil then XLineStyle.ReferenceCount := XLineStyle.ReferenceCount + ChangeBy;
  end;
  if FillStyle.Pattern >= pt_UserDefined then
  begin
    XFillStyle := PInfo^.ProjStyles.XFillStyles[FillStyle.Pattern];
    if XFillStyle <> nil then XFillStyle.ReferenceCount := XFillStyle.ReferenceCount + ChangeBy;
  end;
  with SymbolFill do
    if FillType <> symfNone then
    begin
      if LineStyle.Style >= lt_UserDefined then
      begin
        XLineStyle := PInfo^.ProjStyles.XLineStyles[LineStyle.Style];
        if XLineStyle <> nil then XLineStyle.ReferenceCount := XLineStyle.ReferenceCount + ChangeBy;
      end;
      if FillStyle.Pattern >= pt_UserDefined then
      begin
        XFillStyle := PInfo^.ProjStyles.XFillStyles[FillStyle.Pattern];
        if XFillStyle <> nil then XFillStyle.ReferenceCount := XFillStyle.ReferenceCount + ChangeBy;
      end;
    end;
end;

end.

