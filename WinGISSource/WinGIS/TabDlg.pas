unit TabDlg;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, Validate, MultiLng, Grids, TABFiles;

type
  TTabCheckDialog = class(TForm)
    Panel: TPanel;
    TabStringGrid: TStringGrid;
    Panel1: TPanel;
    OKBtn: TButton;
    CancelBtn: TButton;
    Panel2: TPanel;
    HeaderLabel: TLabel;
    MlgSection: TMlgSection;
    FloatValidator: TFloatValidator;
    StringEdit: TEdit;
    UseBtn: TButton;
    procedure TabStringGridGetEditText(Sender: TObject; ACol,
      ARow: Integer; var Value: string);
    procedure StringEditExit(Sender: TObject);
    procedure StringEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure OKBtnClick(Sender: TObject);
    procedure SaveTABClick(Sender: TObject);
    procedure TabStringGridKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TabStringGridSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
  private
    function OKRead: Boolean;
  private
    { Private declarations }
    OrigFloat: Double;
    procedure OKSet(Value: Boolean);
    property fChanged: Boolean read OKRead write OKSet;
    function MoveRows(From: Integer; MoveDown: Boolean): Boolean;
    procedure ReNumberRows;
  public
    constructor Create(Owner: TComponent);
    destructor Destroy;
    { Public declarations }
  end;

var
  TabCheckDialog: TTabCheckDialog;

//function ConfirmTABUse(Owner: TComponent; Coords: array of TRefPoint): Boolean;
function ConfirmTABUse(const Owner: TComponent; Coords: array of TRefPoint;
  const ImageName: AnsiString; ReadOnly: Boolean = FALSE;
  TABExists: Boolean = TRUE): Boolean;

implementation

uses
  TextUtils;
{$R *.DFM}

var
  Delim: TCharSet = [',', '|', ';'];

procedure ClientRectToScreen(Client: TWinControl; var SrcRect: TRect; var DstRect: TRect);
begin
  DstRect.TopLeft := Client.ClientToScreen(SrcRect.TopLeft);
  DstRect.BottomRight := Client.ClientToScreen(SrcRect.BottomRight);
end;

procedure ScreenRectToClient(Client: TWinControl; var SrcRect: TRect; var DstRect: TRect);
begin
  DstRect.TopLeft := Client.ScreenToClient(SrcRect.TopLeft);
  DstRect.BottomRight := Client.ScreenToClient(SrcRect.BottomRight);
end;
//-------------------------------------------------------------------------

function ConfirmTABUse(const Owner: TComponent; Coords: array of TRefPoint;
  const ImageName: AnsiString; ReadOnly: Boolean = FALSE;
  TABExists: Boolean = TRUE): Boolean;
var
  i, j, iSection, M, C: Integer;
  Hdr, Str: AnsiString;
  Tab: TTabFile;
  NewCoords: TRefArray;
begin
  Result := False;
  if Length(Coords) <= 0 then
    Exit;
  TabCheckDialog := TTabCheckDialog.Create(Owner);
  with TabCheckDialog do
  try
    iSection := MlgStringList.Sections[MlgSection.Section]; // Set section of LNG
    Hdr := MlgStringList.SectionStrings[iSection, 2]; // Get row titles from LNG
    with TabStringGrid do // Fill the grid
    begin
      RowCount := Length(Coords) + 1; // Add 1 more for titles
      with Rows[0] do
        for i := 0 to 4 do
        begin
          Str := Q_GetWordN(i + 1, Hdr, Delim);
          Strings[i] := Q_SpaceCompressInPlace1(Str);
        end;
      M := FloatValidator.MaxLength;
      if FloatValidator.ShowTrailingZeros then
        C := FloatValidator.Commas
      else
        C := 0;
      for j := 1 to Length(Coords) do
      begin
//        Cols[0][j] := IntToStr(j); // Title for the row...
        with Rows[j] do
        begin
          Strings[1] := FloatToStrF(Coords[j - 1].Proj.X, ffNumber, M, C);
          Strings[2] := FloatToStrF(Coords[j - 1].Proj.Y, ffNumber, M, C);
          Strings[3] := FloatToStrF(Coords[j - 1].Img.X, ffNumber, M, C);
          Strings[4] := FloatToStrF(Coords[j - 1].Img.Y, ffNumber, M, C);
        end;
      end;
      ReNumberRows;
      // Update external view
      j := (ClientWidth - ColWidths[0] - ColCount * 1 - 1) div (ColCount - 1);
      for i := 1 to ColCount - 1 do
        ColWidths[i] := j;
      HeaderLabel.Caption := Format(HeaderLabel.Caption, [ImageName]);
      if ReadOnly then // Change button names etc
      begin
        TabStringGrid.Options := TabStringGrid.Options - [goEditing];
//        OKBtn.Enabled := False;
      // Set 'OK' caption for Cancel button
        CancelBtn.Caption := MlgStringList.SectionStrings[iSection, 6];
        UseBtn.Enabled := False;
        Hint := MlgStringList.SectionStrings[iSection, 10];
      end
      else
        if not TabExists then // New file can be created
        begin
      // Set 'Create' caption for OK button
          OKBtn.Caption := MlgStringList.SectionStrings[iSection, 1];
        end;
      case ShowModal of
        mrOK: // User wants to use TAB file: Check  possible updates of TAB
          if not ReadOnly then
          begin
            if fChanged then // It is time to store the file
            begin
              SetLength(NewCoords, RowCount - 1);
              for i := 1 to RowCount - 1 do
              begin
                NewCoords[i - 1].Proj.X := StrToFloat(Q_DelChar(Cells[1, i], ','));
                NewCoords[i - 1].Proj.Y := StrToFloat(Q_DelChar(Cells[2, i], ','));
                NewCoords[i - 1].Img.X := Round(StrToFloat(Q_DelChar(Cells[3, i], ',')));
                NewCoords[i - 1].Img.Y := Round(StrToFloat(Q_DelChar(Cells[4, i], ',')));
              end;
              Result := CreateTABFile(ImageName, 0, 0, nil, NewCoords);
            end;
          end
          else
            Result := True;
        mrYes:// Simply use it without changes
          Result := True;
        mrCancel:
          if ReadOnly then
            Result := True;
      end;
    end;
  finally
    TabCheckDialog.Free;
  end;
end;
//-------------------------------------------------------------------------

procedure TTabCheckDialog.StringEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  with TabStringGrid do
    case Key of
      VK_UP:
        if (Row > 1) then
        begin
          SetFocus;
          Row := Row - 1;
        end;
      VK_DOWN:
        if (Row < (RowCount - 1)) then
        begin
          SetFocus;
          Row := Row + 1;
        end;
      VK_RETURN:
      begin
        SetFocus;
      end;
    end; // case
end;
//----------------------------------------------------

procedure TTabCheckDialog.OKBtnClick(Sender: TObject);
begin
  ModalResult := mrOK;
end;
//----------------------------------------------------

constructor TTabCheckDialog.Create(Owner: TComponent);
begin
  inherited;
  FChanged := False;
end;
//---------------------------------

destructor TTabCheckDialog.Destroy;
begin
  inherited;
end;
//------------------------------------------------------

procedure TTabCheckDialog.SaveTABClick(Sender: TObject);
begin
// Update to file
end;
//-------------------------------------------------------------

procedure TTabCheckDialog.TabStringGridKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  i, j: Integer;
begin
  with TabStringGrid do
    case Key of
      VK_INSERT: // Add record (to the end of list)
        begin
          RowCount := RowCount + 1;
          MoveRows(Row, True); // Move to bottom direction
{          with Rows[RowCount - 1] do
            for i := 1 to 4 do
              Strings[i] := '0';}
          fChanged := True;
        end;
      VK_DELETE: // Delete selected row (if selected)
        begin
          if RowCount < 4 then // Last 2 raws can't be deleted
            Exit;
          if Row < (RowCount - 1) then // Delete not last raw!
            MoveRows(Row + 1, False); // Move all rows to 1 step up
          RowCount := RowCount - 1; // Delete last raw
          fChanged := True;
        end;
    end; // Key
end;
//-----------------------------------------------------------------------

procedure TTabCheckDialog.TabStringGridGetEditText(Sender: TObject; ACol,
  ARow: Integer; var Value: string);
var
  EditRect: TRect;
  Bounds: TRect;
begin
  with StringEdit do
  begin
//    if (not Enabled) or (not Visible)  then
//      Exit;
    EditRect := TabStringGrid.CellRect(ACol, ARow);
    StringEdit.BoundsRect := EditRect {Bounds};
    Text := TabStringGrid.Cells[ACol, ARow];
    Visible := True;
    SetFocus;
    OrigFloat := FloatValidator.AsFloat;
  end;
end;
//--------------------------------------------------------

procedure TTabCheckDialog.StringEditExit(Sender: TObject);
begin
  with TabStringGrid do
  begin
    Cells[Col, Row] := StringEdit.Text;
    if FloatValidator.AsFloat <> OrigFloat then
      fChanged := True;
  end;
  with StringEdit do
  begin
    Visible := False;
  end;
end;
//--

function TTabCheckDialog.MoveRows(From: Integer; MoveDown: Boolean): Boolean;
var
  i, j: Integer;
begin
  with TabStringGrid do
    if MoveDown then // Move to bottom by 1 step
      for i := RowCount - 1 downto From + 1 do
        with Rows[i] do
          for j := 1 to 4 do
            Strings[j] := Cells[j, i - 1]
    else // Move to top by 1 step
      if From > 1 then // Can't move
        for i := From - 1 to RowCount - 2 do
          with Rows[i] do
            for j := 1 to 4 do
              Strings[j] := Cells[j, i + 1];
  ReNumberRows;
end;

procedure TTabCheckDialog.ReNumberRows;
var
  j: Integer;
begin
  with TabStringGrid do
    for j := 1 to RowCount - 1 do
      Cols[0][j] := IntToStr(j); // Title for the row...
end;

procedure TTabCheckDialog.OKSet(Value: Boolean);
begin
  if Value <> OKBtn.Enabled then
  begin
    OKBtn.Enabled := Value;
    if Value then
      UseBtn.Enabled := not Value;
  end;
end;

function TTabCheckDialog.OKRead: Boolean;
begin
  Result := OKBtn.Enabled;
end;


procedure TTabCheckDialog.TabStringGridSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  CanSelect := ACol < 3;
end;

end.

