{****************************************************************************}
{ Unit BMPImage - Nachfolger von AM_Image                                    }
{----------------------------------------------------------------------------}
{ Implementiert dei Verarbeitung von Windows- BMP�s                          }
{----------------------------------------------------------------------------}
{ Erzeugt: 01.04.1997 Bramberger                                             }
{ Modifikationen                                                             }
{        01-180298-001: In Uses-Anw. BMPError in ImgDlg umbenannt            }
{        01-106098-002: 1. Ueberarbeitung neue TrLib-Support...              }
{        Syg-10-dec-99: 1. support new TrLib32 functionality                 }
{        Syg-17-MAR-2000 Some adds to optimize img read/write/store          }
//       YG-23-MAY-2000:  Changed the rotation processing and some other things
//            New module Helmert is used a lot to calculate the rotations.
//       YG-02-JUL-2000:  Set RotRect by ClipRect for all load versions but v4
{****************************************************************************}
unit BMPImage;
{$HINTS ON}
{$WARNINGS ON}
interface

{$R-}
{$O-}

uses Img_Info, WinTypes, WinProcs, {WinDOS,} SysUtils, AM_View, AM_Def, Objects, AM_Paint,
  AM_Index, Controls, Classes, AM_Admin, AM_Ini, AM_ImagC, AM_OLE, AM_CPoly, AM_Circl,
  {++ Ivanoff BUILD#AxDll}
{$IFNDEF AXDLL} // <----------------- AXDLL
  ImgDlg,
{$ENDIF} // <----------------- AXDLL
  TrLib32, NumTools, Forms, TabFiles
  {-- Ivanoff};

type
  IDType = LongInt;
  HandleType = HGLOBAL;
  RetType = HGLOBAL;
  TFourPoints = array[0..3] of TPoint;
const
  ActualFileVersion: Integer = 1;
  MinBMPSizeToUnload: LongInt = 4096; { Min. size to keep in memry for any reason }

  opt_HideFrame = 1;
  opt_ShowBmp = 2; // It could be used in old versions (see so_ShowBMP)
  opt_HasTABFile = 4; //++Sygsky: determines if the image has corresponding TAB file

  bm_Print = 1; {Bitmap f�r Drucken laden}
  bm_Display = 2; {Bitmap f�r Bildschirm laden}

  tt_Black = 1; { Transp. }
  tt_White = 2; { Transp. }

  st_Error: LongInt = $0;
  st_Loaded: LongInt = $1;
  st_RotError: LongInt = $2;
  st_Delete: LongInt = $4;
  st_PathChg: LongInt = $8;
  st_NoLoad: LongInt = $10; { Bild nicht gefunden - �bergehen }
  st_WrongType: LongInt = $20;
  st_ReadErr: LongInt = $40;
  st_AllOK: LongWord = $80000000; { Word is more correct name than Int for the value}

  so_ShowBMP: Word = $1;

  erNoWin30 = 1;
  erCreate = 2;
  erOpen = 3;

  oe_None = 0; { Nothing to do when error open file }
  oe_ForAllBMP = 1; { Action is alredy preset for all BMP to be loaded }
type
  PTFileStream = ^TFileStream;

  PBitmapDimensions = ^BitmapDimensions;
  BitmapDimensions = record
    ReqX, ReqY: LongInt; {Screen - angeforderte Pixel}
    RealX, RealY: LongInt; {.bmp-Gr��e}
    LoadX, LoadY: LongInt; {Ladedimension - errechnete Pixel}
    LoadFactor: LongInt; {jedes x. Pixel laden}
    ImageSize: LongInt; {geladene Gr��e}
    BitCount: Word; {Farbtiefe}
  end;

  TRAttrib = record
    StartXPix, StartYPix: LongInt;
    PixelsXLoad, PixelsYLoad: LongInt;
    //++ Glukhov Bug#236 Build#144 18.12.00
    ImageSize: LongInt;
    //-- Glukhov Bug#236 Build#144 18.12.00
  end;

  TDrawAttrib = record
    Pen: HPen;
    Brush: HBrush;
    ROP2: Integer;
  end;

  PRGBTriple = ^TRGBTriple;
  TRGBArray = array[0..255] of TRGBQuad;

  //++ Glukhov HELMERT BUILD#115 23.05.00
  TLPoint = AM_Def.TLPoint;
  { Changed to make a consistency with AM_Def
        TLPoint    = Record
          X, Y : LongInt;
        End;
  }
  //-- Glukhov HELMERT BUILD#115 23.05.00

  TRPoint = record
    X, Y: Real;
  end;

  PTLPoints = ^TTLPoints;
  TTLPoints = array[0..8189] of TLPoint;

  PImgRect = ^TImgRect;
  TImgRect = record
    P1, P2,
      P3, P4: TLPoint;
  end;

  TImgDim = record
    sFNameAbs,
      sFNameRel: string;
    bUseRel: Boolean;
    Status: LongInt;
    lWidth,
      lHeight: LongInt;
    byBitCount: Byte;
    dRotation: double;
  end;

  TECWHeader = record
    Dummy: array[0..24] of Byte;
    IncX: Double;
    IncY: Double;
    OrigX: Double;
    OrigY: Double;
    Proj: array[0..31] of Char;
  end;
type
  PPImage = ^PImage; //++Sygsky

  PImage = ^TImage;
  TImage = object(TView)
    RealFileName: PString;
    // YG, file name for bitmap with the currently shown image (transformed or not)
    FileName: PString;
    //---------------
    // YG, The most common possible transformation here is the affine one.
    //     It is implemented as 2 steps-transformation.
    //     1st step:  1st rotation and X-stretch
    //     2nd step:  2nd rotation
    //---------------
    // file name for the transformed bitmap (used or not) after the 1st step
    sAffineTFileName: string;
    // file name for the transformed bitmap (used or not) after the 2nd step
    sRotatedFileName: string;
    // angle of the 1st rotation
    dStoredA: Double;
    // ratio of the X-stretch after the 1st rotation
    dStoredK: Double;
    // angle of the 2nd rotation
    dStoredR: Double;
    // width of the useful bitmap in the rotated bitmap
    iStoredW: LongInt;
    // height of the useful bitmap in the rotated bitmap
    iStoredH: LongInt;
    // original width of bitmap
    iInitialW: LongInt;
    // original height of bitmap
    iInitialH: LongInt;
    // original rotation angle of bitmap
    // (it is not used now, but maybe in future ...)
    dInitialA: Double;

    RelativePath: PString;
    AbsolutePath: PString;
    bRelPath: Boolean;
    Image: hBitmap;
    ImgData: LPVOID;
    Info: ImgInfo;
    //        Loaded            : LongInt;
    Loaded: LongWord;
    OldLoaded: LongWord;
    ShowOpt: Word;
    IsRotated: Boolean;
    Rotation: double; // YG, angle of image without project rotation
    RotSaved: Boolean;
    RotRect: TImgRect;
    Transparency: Byte;
    TransparencyType: Byte;
    Options: Word;

    ScaledClipRect: TDRect;
    VScaledClipRect: TRect; {++Sygsky: Virtual scaled cliprect}
    VClipRect: TRect; {++Sygsky: Virtual ClipRect}
    BMPAttrib: TRAttrib;
    OldLoadFactor: LongInt;
    LoadPixelsExact,
      LoadAreaExact: Boolean;
    // flag to declare that the drawn image is a symbol
    bSymbolImage: Boolean;
    // flag to declare that the image is always visible
    //    bAlwaysVisible: Boolean;
    // flag to declare that the image is big enough to be visible
    bBmpBigEnough: Boolean;
    Border: PCPoly;
    constructor Init(aFileName, aRelativePath: AnsiString; aShowOpt: Word; const BitmapSettings: TBitmapSettings);
    procedure InitFile(S: TOldStream);
    procedure InitFileEditLinks;
    function OpenInfo(var Status: Word): Boolean;
    destructor Done; virtual;
    (** Load/Store- routines **)
    constructor LoadV4(S: TOLEClientStream);
    procedure LoadFileNames(S: TOLEClientStream);
    virtual;
    constructor LoadV34(S: TOldStream);
    constructor LoadV33(S: TOldStream);
    constructor LoadV321(S: TOldStream);
    constructor LoadNew(S: TOldStream);
    constructor Load(S: TOldStream);
    constructor LoadOld(S: TOldStream);
    procedure Store(S: TOldStream); virtual;
    (* WinGIS - interface *)
    function GetObjType: Word; virtual;
    procedure SetClipRect(PInfo: PPaint); virtual;
    procedure Draw(PInfo: PPaint; CLipping: Boolean); virtual;
    procedure Invalidate(PInfo: PPaint; FroMGraphic: Boolean = TRUE); virtual;
    function AllOK: Boolean;
    procedure GetInfo(var theInfo: TImgDim);
    (* Selection - routines *)
    function SearchForPoint(PInfo: PPaint; InCircle: Pointer; Exclude: LongInt; const ObjType: TObjectTypes;
      var Point: TDPoint; var SnObje: PIndex): Boolean; virtual;
    function SearchForNearestPoint(PInfo: PPaint; var ClickPos: TDPoint; InCircle: Pointer;
      Exclude: Longint; const ObjType: TObjectTypes; var point: TDPoint; var SnObje: PIndex): Boolean; virtual;
    function SelectBetweenPolys(PInfo: PPaint; Poly1, Poly2: Pointer; const ObjType: TObjectTypes;
      Inside: Boolean): Boolean; virtual;
    function SelectByCircle(PInfo: PPaint; Middle: TDPoint; Radius: LongInt;
      const ObjType: TObjectTypes; Inside: Boolean): Boolean; virtual;
    function SelectByPoint(PInfo: PPaint; Point: TDPoint; const ObjType: TObjectTypes): PIndex; virtual;
    function SelectByPoly(PInfo: PPaint; Poly: Pointer; const ObjType: TObjectTypes; Inside: Boolean): Boolean; virtual;
    function SelectByPolyLine(PInfo: PPaint; Poly: Pointer; const ObjType: TObjectTypes): Boolean; virtual;
    procedure GetBorder(PInfo: PPaint; APoly: PCPoly);
    (* Internal- functions *)
    function LoadImage(PInfo: PPaint; Device: Integer): Word;
    function OpenImage(PInfo: PPaint; Device: Integer): Word;
    function FetchData(BMPDim: BitmapDimensions): HGLOBAL;
    function ShowImage(Show: Boolean): Boolean;
    function ReLoad(PInfo: PPaint; Device: Integer): Word;
    procedure UnLoad;
    procedure InitExactLoading; virtual;
    procedure CalculateNewImageDimensions(ImgDim: PBitmapDimensions);
    function SetPalette(DC: hDC): hPalette; virtual;
    function GetPalette(DC: hDC): Word; virtual;
    procedure ConvertTo256ColorPalette(PixelSize: LongInt; Preview: Boolean);
    procedure DrawVector(PInfo: PPaint; Clipping: Boolean);
    procedure MakeRotFromClip; {++ Sygsky }
    (* Misc. functions *)
    procedure CalcTRAttrib(var BMPDim: BitmapDimensions; PInfo: PPaint; Device: Integer);
    function SetTransparency(var New, Old: TDrawAttrib; DC: HDC): Boolean; overload;
    function SetTransparency(DC: HDC; var Points: TFourPoints): Boolean; overload;
    procedure OpenError(OEOptions: Word);
    (* Rotate functions *)
    procedure RotateBitmap(Angle: Double; PInfo: PPaint);

    function CheckPoints(Points: Pointer; PtCnt: Word): Boolean;
    procedure CorrectRotRect;
    {++ Glukhov HELMERT BUILD#115 31.05.00 }
    // YG, The new procedures
    procedure InitTransformationData;
    procedure GetTempBitmapFileName(var ouFileName: string);
    procedure InitStoredWH;
    procedure CalcStoredWH(
      iSrcW, iSrcH: LongInt; dbA1, dbK, dbA2: Double);
    procedure CalcSpecTransformationData(
      ParLB, ParRB, ParLT: TLPoint; var Angle1, kX, Angle2: Double);
    procedure CalcVClipRect4RotRect(PInfo: PPaint; var Rect: TRect);
    {-- Glukhov HELMERT BUILD#115 31.05.00 }
    function CheckAlternativeNames: Boolean;
    //++Sygsky: for Map cutting purposes
        // Copy part of image intersected with CutRect
    function Cutting(NewFileName: AnsiString; CutRect: TDRect; NewClipRect: PRect = nil): Boolean;
    //++Sygsky: to support TAB files
    function UpdateTAB(Txt: AnsiString = ''): Boolean;
    //++ Sygsky: to check cut-and-past allowance
    function IsVMSITech: Boolean;
    // To help intersect rects calculations
    function GetIRects(var CutRect, InRect: TDRect; var InImgRect: TRect): Boolean;
    //--Sygsky
    function GetCenter(PInfo: PPaint): TDPoint;
    procedure UpdateBorder(PInfo: PPaint);
    function GivePosX: LongInt; virtual;
    function GivePosY: LongInt; virtual;
    function GivePosX1: LongInt; virtual;
    function GivePosY1: LongInt; virtual;
    function GivePerimeter: LongInt; virtual;
    function GiveArea: Double; virtual;

  end;

  TRasterTrafoInfo = record
    Layers: PLayers;
    RTrafoType: Byte;
    RTrafoInfo: PTLPoints;
    RTrafoCnt: Word;
    SnapItem: Pointer;
    PInfo: PPaint;
    Parent: TWinControl;
    CallCorrectSize,
      CallCorrectSizeOverView: Boolean;
    Image: PImage;
  end;

  PSImage = ^TSImage;
  TSImage = object(TImage)
    DoEmbed: Boolean;
    lpTempFileName: PString;
    Ext: PString;

    constructor Init(aFileName: string; aShowOpt: Word; const BitmapSettings: TBitmapSettings; aDoEmbed: Boolean);
    constructor Load(S: TOLEClientStream);
    destructor Done; virtual;
    procedure LoadFileNames(S: TOLEClientStream); virtual;
    procedure Store(S: TOldStream); virtual;

    procedure InitExactLoading; virtual;
    procedure Draw(PInfo: PPaint; Clipping: Boolean); virtual;
  end;

function ProcRasterTransformation(TheRasterInfo: TRasterTrafoInfo): Boolean;

procedure ProcSearchAndDeleteBMP(Project: Pointer);

procedure ProcSetColorsPalette(AProj: Pointer);

procedure ProcSetGrayPalette(AProj: Pointer);

procedure ProcSetBmpPalette(AProj: Pointer);

procedure ProcReloadPictures(AProj: Pointer);

var
  NotLoadedImagesCounter: LongInt = 0; { How many images were not found }
  MovedImagesCounter: LongInt = 0; { How many images were found on relative path etc. }

implementation

uses AM_Main, Win32Def, AM_StdDl, AM_Layer, UserIntf,
  {++ Glukhov HELMERT BUILD#115 23.05.00 }
  Helmert,
  {-- Glukhov HELMERT BUILD#115 23.05.00 }
  AM_Proj, ResDlg, RegDB, AM_Child
{$IFDEF DEBUG}
  , ToolTips, Dialogs
{$ENDIF}
  ;
{
 For non-standard rects: Y's here are swept
}

function SpecRectsIntersected(Rect1, Rect2: TRect): Boolean;
// Take in account that Rect.Top < Rect.Bottom here
begin
  Result := False;
  if Rect1.Left > Rect2.Right then
    Exit;
  if Rect1.Right < Rect2.Left then
    Exit;
  if Rect1.Top > Rect2.Bottom then
    Exit;
  if Rect1.Bottom >= Rect2.Top then
    Result := True;
end;

procedure SpecRectsIntersection(FirstRect, SecondRect: TRect; var NewRect: TRect);
// Take in account that Rect.Top < Rect.Bottom here
begin
  NewRect := Rect(
    Max(FirstRect.Left, SecondRect.Left),
    Max(FirstRect.Top, SecondRect.Top),
    Min(FirstRect.Right, SecondRect.Right),
    Min(FirstRect.Bottom, SecondRect.Bottom)
    );
end;

procedure SetRect(SrcRect: TRect; var DstRect: TRect);
begin
  Move(SrcRect, DstRect, SizeOf(TRect));
end;

constructor TImage.Init(aFileName, aRelativePath: AnsiString; aShowOpt: Word; const BitmapSettings: TBitmapSettings);
var
  Status: Word;
  {  ImgFile: TFileStream;}
begin
  TView.Init;
  Loaded := 0;
  OldLoaded := 0;
  RealFileName := nil;
  FileName := NewStr(aFileName);
  AbsolutePath := NewStr(aFileName);
  RelativePath := NewStr(aRelativePath);
  bRelPath := False;

  if FileExists(aFileName) then
    if not FileIsImageOne(aFileName) then //++ Sygsky: Check file directly
      OpenError(0);
  //  Inc(NotLoadedImagesCounter);

  {  try
      ImgFile := TFileStream.Create(aFileName, fmOpenRead or fmShareDenyWrite);
      ImgFile.Free;
    except
      on EFOpenError do OpenError(0);
    end;}

  if (Loaded and st_NoLoad) = 0 then { Alles OK }
  begin
    OpenInfo(Status);
    if (Status <> err_OK) then
    begin
      if (Status = err_WrongFormat) then
        Loaded := st_WrongType
      else
        if (Status = err_ReadErr) then
          Loaded := st_ReadErr;
    end
    else
    begin
      Loaded := Loaded or st_AllOK;
      InitStoredWH;
    end;
  end;

  // Init the data for the image transformation
  Rotation := 0.0;
  InitTransformationData;

  // Init RotRect by ClipRect
  //  MakeRotFromClip;

  Transparency := BitmapSettings.Transparency;
  TransparencyType := BitmapSettings.TransparencyType;
  if BitmapSettings.ShowFrame then
    Options := 0
  else
    Options := opt_HideFrame;
  //++ Glukhov Bug#236 Build#144 15.12.00
  //  if BitmapSettings.ShowBitmap then Options:= Options or opt_ShowBmp;
  //-- Glukhov Bug#236 Build#144 15.12.00

  Image := 0;
  ImgData := nil;
  ShowOpt := aShowOpt;
  ScaledClipRect.InitByRect(ClipRect);
  OldLoadFactor := 0;

  Border := New(PCPoly, Init);

end;

destructor TImage.Done;
begin
  if (Loaded and st_Loaded) <> 0 then
  begin
    //++ Glukhov Bug#236 Build#144 14.12.00
    try
      Unload;
    except
      begin
      end;
    end;
    //-- Glukhov Bug#236 Build#144 14.12.00
  end;
  if sRotatedFileName <> '' then
    DeleteFile(sRotatedFileName);
  if sAffineTFileName <> '' then
    DeleteFile(sAffineTFileName);
  DisposeStr(FileName);
  Info.Free;
  Info := nil; {++ Sygsky }
  if RealFileName <> nil then
  begin
    DisposeStr(RealFileName);
    RealFileName := nil;
  end;
  //++ Glukhov Bug#236 Build#144 14.12.00
  if RelativePath <> nil then
  begin
    DisposeStr(RelativePath);
    RelativePath := nil;
  end;
  if AbsolutePath <> nil then
  begin
    DisposeStr(AbsolutePath);
    AbsolutePath := nil;
  end;
  //-- Glukhov Bug#236 Build#144 14.12.00
  TView.Done;
  Dispose(Border, Done);
end;

procedure TImage.InitExactLoading;
begin
{$IFNDEF AXDLL} // <---------- AXDLL
  LoadPixelsExact := IniFile^.BmpPixelsExact;
  LoadAreaExact := IniFile^.BmpAreaExact;
{$ELSE} // <---------- AXDLL
  LoadPixelsExact := FALSE;
  LoadAreaExact := FALSE;
{$ENDIF} // <---------- AXDLL
end;

{
Procedure TImage.InitFile(var S : TOldStream);
var GoOn : Boolean;
    Stream        : ^TOLECLIENTSTREAM;
    ImgFile       : TFileStream;
Begin
  Stream := @S;
  try
    ImgFile := TFileStream.Create(FileName^, fmOpenRead or fmShareDenyWrite);
    ImgFile.Free;
  except
    on EFOpenError do begin
      OpenError(0);
      Inc(NotLoadedImagesCounter);
    end;
  End;
End;
}

procedure TImage.InitFileEditLinks;
var
  Exists: Boolean;
begin
  Exists := FileExists(FileName^);
  if Exists then
  begin
    //if FileIsImageOne(FileName^) then   //++ Sygsky: Check file directly
    Exit;
  end
  else { Not exists }
  begin
    if CheckAlternativeNames then { Relative path's file found! }
      if FileIsImageOne(FileName^) then //++ Sygsky: Check file directly
      begin
        Inc(MovedImagesCounter); // Mark changes in pathes
        Exit;
      end;
  end;
  { Not exists or not image }
  OpenError(0);
  Inc(NotLoadedImagesCounter);
end;

procedure TImage.InitFile(S: TOldStream);
begin
  InitFileEditLinks;
  {  if FileExists(FileName^) then
      if FileIsImageOne(FileName^) then //++ Sygsky: Check file directly
        Exit;
    OpenError(0);
    Inc(NotLoadedImagesCounter); }
end;

function TImage.AllOK: Boolean;
begin
  Result := ((Loaded and st_AllOK) <> 0);
end;

procedure TImage.GetInfo(var theInfo: TImgDim);
begin
  with theInfo do
  begin
    sFNameAbs := AbsolutePath^;
    sFNameRel := RelativePath^;
    bUseRel := bRelPath;
    Status := Loaded;
    lWidth := Info.Width;
    lHeight := Info.Height;
    byBitCount := Info.BitCount;
    dRotation := Rotation;
  end;
end;

{******************************************************************************}
{ Creates an Info- Object, and checks imagetype                                }
{******************************************************************************}
{ Parameters: Status : Word    Returns status of the Create- Proc              }
{                              err_WrongFormat - Not a supported img-format    }
{                              err_ReadErr     - Error reading file            }
{                              err_OK          - All OK                        }
{******************************************************************************}
{ Return value:   Boolean      TRUE    - All OK                                }
{                              FALSE   - Error occured                         }
{******************************************************************************}

function TImage.OpenInfo(var Status: Word): Boolean;
var
  iStrNum: Integer;
begin
  OpenInfo := False;
  InitExactLoading; { Set default parameters of loading }

  if Info <> nil then
  begin
    Info.Free;
    Info := nil;
  end;

{$IFNDEF AXDLL} // <----------- AXDLL
  Info := TrLibInfo.Create(FileName^, Status, IniFile^.ModuleData^.TRLibID);
{$ELSE} // <----------- AXDLL
  Info := TrLibInfo.Create(FileName^, Status);
{$ENDIF} // <----------- AXDLL

  {01-004-040399 - Begin}
    {If Vector and can't be drawn from DLL, image can not be handled by WinGIS }
  if ((Status = err_OK) and Info.IsVector and (not Info.DrawExternal)) then
    Status := err_WrongFormat;
  {01-004-040399 - End}

  if (Status = err_WrongFormat) then
    iStrNum := 5510
  else
    iStrNum := 5512;
  //  else if(Status = err_ReadErr) then iStrNum :=  5512;

  if (Status <> err_OK) then
  begin
    Info.Free;
    Info := nil;
    {++ Ivanoff BUILD#AxDll}
    {In case of AxDll WinGisMainForm is absent. For calling message box in AxDll
     Application.Handle was used.}
    // MsgBox(WinGISMainForm.Handle, iStrNum, MB_OK Or MB_ICONEXCLAMATION, ''); { Nicht erkanntes Dateiformat }
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <------------ AXDLL

    {
    if WinGISMainForm.LastImpExpOperation = '' then
      MsgBox(Application.Handle, iStrNum, MB_OK or MB_ICONEXCLAMATION, '');
    }

{$ENDIF} // <------------ AXDLL
{$ENDIF}
    Exit;
    {-- Ivanoff}
  end;
  {01-004-040399 - Begin}
    { Vector formats can't be loaded partial }
  if (Info.IsVector) then
  begin
    LoadPixelsExact := False;
    LoadAreaExact := False;
  end;
  {01-004-040399 - End}
  OpenInfo := True;
end;

procedure TImage.Invalidate
  (
  PInfo: PPaint;
  FromGraphic: Boolean
  );
var
  Rect: TRect;
  ARect: TDRect;
begin
  ARect.InitByRect(ClipRect); {01-006-150399}
  ARect.Grow(PInfo^.CalculateDraw(PInfo^.LineWidth));
  PInfo^.ConvertToPhysRect(ARect, Rect);
  InflateRect(Rect, 1, 1);
  InvalidateRect(PInfo^.HWindow, @Rect, TRUE);
  if FromGraphic then
    PInfo^.FromGraphic := TRUE;
end;

{******************************************************************************}
{ Calculates the Paramters for the new image (loadfactor, Pixels to load ...)  }
{******************************************************************************}
{ Parameters: BMPDim :         The dimensions of the image Required actual,    }
{                              loadfactor etc.                                 }
{             PInfo            The paint structure                             }
{             Device           The device the image will be created for        }
{                                      bm_Display or bm_Print                  }
{******************************************************************************}

procedure TImage.CalcTRAttrib(var BMPDim: BitmapDimensions; PInfo: PPaint; Device: Integer);
var
  VShowRect: TRect;
  VCurrentScreen: TRect;
  ClipWidth: Integer;
  ClipHeight: Integer;
  LoadFactor: Integer;
  iX, iY: Integer;
begin
  // Refresh ScaledClipRect (it is not really used)
  ScaledClipRect.InitByRect(ClipRect);
  // Calculate the screen rectangle escribing RotRect
  CalcVClipRect4RotRect(PInfo, VClipRect);
  // Get Display bounds in logical units
  PInfo.GetDispScreen(VCurrentScreen);
  // Init BMP attributes
  with BMPAttrib do
  begin
    // First set all default values for commom case
    //      StartXPix := 0;
    //      StartYPix := 0;
    // Offset in the source bitmap
    StartXPix := (Info.Width - iStoredW) div 2;
    StartYPix := (Info.Height - iStoredH) div 2;
    // Offset in the useful bitmap
    iX := 0;
    iY := 0;

    PixelsXLoad := BMPDim.LoadX;
    PixelsYLoad := BMPDim.LoadY;
    LoadFactor := BMPDim.LoadFactor;

    SetRect(VClipRect, VScaledClipRect);
    if PInfo.HWindow <> 0 then
    begin
      // PInfo is defined
      if (LoadAreaExact) then
      begin
        // User wants to load only displayed part of image
        if Device <> bm_Print then
        begin
          if SpecRectsIntersected(VCurrentScreen, VClipRect) then
          begin
            // Some intersections detected
            SpecRectsIntersection(VClipRect, VCurrentScreen, VShowRect);
            SetRect(VShowRect, VScaledClipRect);
            ClipWidth := TrLib32.PRectWidth(@VClipRect);
            ClipHeight := TrLib32.PRectHeight(@VClipRect);

            // Set the left boundary of the loaded source bitmap
            if VShowRect.Left > VClipRect.Left then
            begin
              //++ Glukhov bug correction BUILD#121 03.07.00
{$IFDEF GLUKHOV}
              iX := Trunc(iStoredW *
                ((VShowRect.Left - VClipRect.Left + 1) / ClipWidth));
              if iX > 1 then
                StartXPix := StartXPix + iX - 1;
{$ELSE}
              iX := Round(iStoredW *
                ((VShowRect.Left - VClipRect.Left) / ClipWidth));
              if iX > 1 then
                StartXPix := StartXPix + iX;
{$ENDIF}
            end;
            // Set the width of the loading screen bitmap
            if VShowRect.Right < VClipRect.Right then
{$IFDEF GLUKHOV}
              PixelsXLoad := Trunc(iStoredW *
                (((VShowRect.Right - VShowRect.Left + 1) / ClipWidth) / LoadFactor))
{$ELSE}
              PixelsXLoad := Round(iStoredW *
                (((VShowRect.Right - VShowRect.Left) / ClipWidth) / LoadFactor))
{$ENDIF}
            else
{$IFDEF GLUKHOV}
              PixelsXLoad := Trunc((iStoredW - iX) / LoadFactor);
{$ELSE}
              PixelsXLoad := {Round} Trunc((iStoredW - iX) / LoadFactor);
{$ENDIF}
            // To ensure the correct loading
            if PixelsXLoad <= 0 then
              PixelsXLoad := 1;
            if (StartXPix + PixelsXLoad * LoadFactor) > Info.Width then
              StartXPix := Info.Width - PixelsXLoad * LoadFactor;

            // Set the upper boundary of the loaded source bitmap
            if VShowRect.Bottom < VClipRect.Bottom then
            begin
{$IFDEF GLUKHOV}
              iY := Trunc(iStoredH * ((VClipRect.Bottom - VShowRect.Bottom + 1) / ClipHeight));
              if iY > 1 then
                StartYPix := StartYPix + iY - 1;
{$ELSE}
              iY := Round(iStoredH * ((VClipRect.Bottom - VShowRect.Bottom) / ClipHeight));
              if iY > 1 then
                StartYPix := StartYPix + iY;
{$ENDIF}
            end;
            // Set the height of the loading screen bitmap
            if VShowRect.Top > VClipRect.Top then
{$IFDEF GLUKHOV}
              PixelsYLoad := Trunc(iStoredH *
                (((VShowRect.Bottom - VShowRect.Top + 1) / ClipHeight) / LoadFactor))
{$ELSE}
              PixelsYLoad := Round(iStoredH *
                (((VShowRect.Bottom - VShowRect.Top) / ClipHeight) / LoadFactor))
{$ENDIF}
            else
{$IFDEF GLUKHOV}
              PixelsYLoad := Trunc((iStoredH - iY) / LoadFactor);
{$ELSE}
              PixelsYLoad := {Round} Trunc((iStoredH - iY) / LoadFactor);
{$ENDIF}
            // To ensure the correct loading
            if PixelsYLoad <= 0 then
              PixelsYLoad := 1;
            if (StartYPix + PixelsYLoad * LoadFactor) > Info.Height then
              StartYPix := Info.Height - PixelsYLoad * LoadFactor;
            //-- Glukhov bug correction BUILD#121 03.07.00
          end
          else // Not intersected at ALL!
          begin // for example we can come here from TAB loading out of current screen
          // SO simply emulate loading!
            StartYPix := 0;
            StartXPix := 0;
            PixelsXLoad := 1;
            PixelsYLoad := 1;
            LoadFactor := 1;
          end;
        end;
      end;
    end;
    BMPDim.ImageSize := ((((PixelsXLoad * Info.BitCount) + 31) div 32) * 4) * PixelsYLoad;
    BMPAttrib.ImageSize := BMPDim.ImageSize;
  end;
end;

{******************************************************************************}
{ Checks if the image may be loaded / manages rotation or normal loading       }
{ Calls OpenImage for image- loading                                           }
{******************************************************************************}
{ Parameters: PInfo            The paint structure                             }
{             Device           The device the image will be created for        }
{                                      bm_Display or bm_Print                  }
{******************************************************************************}
{ Return value:   Word         1       - All OK                                }
{                              0       - Error occured                         }
{******************************************************************************}

function TImage.LoadImage(PInfo: PPaint; Device: Integer): Word;
var
  OpenErr: Word;
  //  NewAngle: double; // Current angle of rotation
  hcur: integer;
  VRect: TRect;
  Img: PImage;
  Idx: Integer;
begin
  LoadImage := 0; // Return = Problem
  if (Loaded and st_AllOK) = 0 then
    Exit;
  LoadImage := 1; // Return = Ok

  // Reload an image each time when it is necessary.
  //++ Glukhov Bug#236 Build#144 14.12.00
  //  if PInfo^.bMustLoad
  //  then  UnLoad
  //  else  if (Loaded and st_Loaded) <> 0 then Exit; // Already loaded, exit
  if (Loaded and st_Loaded) <> 0 then
    Exit; // Already loaded, exit
  //-- Glukhov Bug#236 Build#144 14.12.00

  // Check RotRect
  if ((RotRect.P1.X - RotRect.P4.X) = 0) and
    ((RotRect.P3.X - RotRect.P4.X) = 0) then
    MakeRotFromClip; // Init zero RotRect by ClipRect

  // Check a compression/stretching, if the project is not rotated
  if PInfo.ViewRotation = 0 then
    CorrectRotRect;

  if ((ShowOpt and so_ShowBMP) = 0) then
    Exit; // We don't need to show image

  //++ Glukhov Bug#236 Build#144 04.12.00
  if PInfo.BitmapSettings.GlobInvisiblity then
    Exit;
  //-- Glukhov Bug#236 Build#144 04.12.00

  CalcVClipRect4RotRect(PInfo, VRect);
  if ((Abs(VRect.Right - VRect.Left) <= PInfo^.BitmapSettings.ShowMinSize) and
    (Abs(VRect.Bottom - VRect.Top) <= PInfo^.BitmapSettings.ShowMinSize) and
    (not PInfo^.Printing)) then
    bBmpBigEnough := False
  else
    bBmpBigEnough := True;

  if (not bBmpBigEnough) then
    Exit; // We don't need to show image

  // Get the view angle and rotate bitmap, if necessary

  {+++Brovak}
  //  hcur := PInfo^.SetTheCursor(crHourGlass);
  hcur := Pinfo^.Cursor;
  //Screen.Cursor := crHourGlass;
  UserInterface.Update([uiCursor]);
  {--- Brovak}

  {Brovak}
  RotateBitmap(PInfo.ViewRotation, PInfo);

  LoadImage := 0;
  if IsRotated and (not RotSaved) then
  begin
    OldLoaded := Loaded;
    Loaded := st_RotError;
    {Brovak}
  //  PInfo^.SetTheCursor(hcur)
    PInfo^.Cursor := hcur;
    UserInterface.Update([uiCursor]);
    {Brovak}
    Exit;
  end;

  // File name is preset, now really open image - load it to memory as a DIB!
  OpenErr := OpenImage(PInfo, Device);
  if (OpenErr = 0) then
  begin
    // Ok
  //++ Glukhov PrintOptimization BUILD#151 21.02.01
    if (Device <> bm_Print) then
      //-- Glukhov PrintOptimization BUILD#151 21.02.01
      Loaded := Loaded or LongWord(st_Loaded);
    LoadImage := 1;
  end
  else
  begin
    if (OpenErr <> $FFFF) then
    begin
      //++ Glukhov 2 tries to create DIB BUILD#121 07.07.00
      if (OpenErr = 7) then
      begin
        OpenErr := OpenImage(PInfo, Device);
        if (OpenErr = 7) then
          OpenErr := 6;
        if (OpenErr = 0) then
        begin
          Loaded := Loaded or LongWord(st_Loaded);
          LoadImage := 1;
        end
        else
        begin
          { Vector- format: OpenImage aborted before file-load }
          MsgBox(WinGISMainForm.Handle, 5499 + OpenErr, MB_IconExclamation + MB_OK, FileName^);
          Loaded := st_Error;
          LoadImage := 0;
        end;
      end;
    end;
    //-- Glukhov 2 tries create DIB BUILD#121 07.07.00
  end;
  {+++Brovak}
//  PInfo^.SetTheCursor(hcur);
  //Screen.Cursor := hcur;
  UserInterface.Update([uiCursor]);
  {--Brovak}
//++ Glukhov Bug#236 Build#144 15.12.00
  if ((Loaded and st_Loaded) <> 0) then
  begin
    if Assigned(IniFile) and
      Assigned(IniFile.AutoBmpLoaded) and
      (IniFile.AutoBmpReload > 0) then // mode 1 or 2
    begin
      // Update buffer of the loaded images
      while IniFile.AutoBmpBfUsed >= IniFile.AutoBmpBfSize do
      begin
        Idx := IniFile.AutoBmpLoaded.Count - 1;
        if Idx < 0 then
        begin
          IniFile.AutoBmpBfUsed := 0;
          Break;
        end;
        Img := PImage(IniFile.AutoBmpLoaded.At(Idx));
        Img.Unload;
      end;
      IniFile.AutoBmpLoaded.AtInsert(0, @Self);
      IniFile.AutoBmpBfUsed := IniFile.AutoBmpBfUsed + BMPAttrib.ImageSize;
    end;
  end;
  //-- Glukhov Bug#236 Build#144 15.12.00
end;

//++ Glukhov Bug#236 Build#144 19.12.00
{
function TImage.AutoLoadImage(PInfo: PPaint; Device: Integer): Word;
var
  VRect   : TRect;
  Img     : PImage;
  Idx     : Integer;
  bToLoad : Boolean;
begin
  if (IniFile.AutoBmpReload <> 2)
  or (Device = bm_Print)
  then begin
  // regular loading
    Result:= LoadImage( PInfo, Device );
  end
  else begin
  // mode 2 (background loading)
    TmpPInfo:= PInfo;
    Result:= 0;
    bToLoad:= False;

    if ((Loaded and st_AllOK) <> 0) and       // no problem
       ((Loaded and st_Loaded) = 0) and       // not loaded
       ((ShowOpt and so_ShowBMP) <> 0) and    // to be shown
       (not PInfo.BitmapSettings.GlobInvisiblity)
    then begin
    // Check RotRect
      if ((RotRect.P1.X - RotRect.P4.X) = 0) and
         ((RotRect.P3.X - RotRect.P4.X) = 0)
      then MakeRotFromClip; // Init zero RotRect by ClipRect

    // Check a compression/stretching, if the project is not rotated
      if PInfo.ViewRotation = 0 then CorrectRotRect;

      CalcVClipRect4RotRect(PInfo, VRect);
      if (Abs(VRect.Right - VRect.Left) > PInfo^.BitmapSettings.ShowMinSize)
      or (Abs(VRect.Bottom - VRect.Top) > PInfo^.BitmapSettings.ShowMinSize)
      or (PInfo^.Printing)
      then begin
        bToLoad:= True;
      end;
    end;

  // Search the image in the list of images to be loaded
    Idx:= IniFile.AutoBmpToLoad.IndexOf( @Self );

    if bToLoad then begin
    // Put the image in the list of images to be loaded
      if Idx < 0 then begin
        IniFile.AutoBmpToLoad.Insert( @Self );
        Result:= 1;
      end
      else begin
      // 2nd query to load: force the operation
        Result:= LoadImage( PInfo, Device );
        IniFile.AutoBmpToLoad.AtDelete( Idx );
      end;
    end
    else begin
    // Remove the image from the queue
      if Idx >= 0 then IniFile.AutoBmpToLoad.AtDelete( Idx );
    end;
  end;
end;

procedure AutoLoadCheck(PInfo: PPaint);
var
  VScreen : TRect;
  VRect   : TRect;
  Img     : PImage;
  Idx     : Integer;
begin
  if IniFile.AutoBmpReload <> 2 then Exit;

// Check the list of images to be loaded
  PInfo.GetVirtScreen( VScreen );
  Idx:= 0;
  while Idx < IniFile.AutoBmpToLoad.Count do begin
    Img:= PImage( IniFile.AutoBmpToLoad.At( Idx ) );
    Img.CalcVClipRect4RotRect( PInfo, VRect );
    if SpecRectsIntersected(VScreen, VRect)
    then  Inc( Idx )
  // Delete the image which is not on the screen
    else  IniFile.AutoBmpToLoad.AtDelete( Idx );
  end;
end;
}
//-- Glukhov Bug#236 Build#144 19.12.00

{******************************************************************************}
{ Calculates the image- loading parameters (size ...) and loads image by       }
{ calling FetchData                                                            }
{******************************************************************************}
{ Parameters: PInfo            The paint structure                             }
{             Device           The device the image will be created for        }
{                                      bm_Display or bm_Print                  }
{******************************************************************************}
{ Return value:   Word         0       - All OK                                }
{                           <> 0       - Error occured                         }
{******************************************************************************}

function TImage.OpenImage(PInfo: PPaint; Device: Integer): Word;
type
  T1BITIMAGECOLORS = array[0..1] of DWORD;
var
  BMPInfo: PBITMAPINFO;
  NewHandle: hBitmap;
  VRect: TRect;
  {BitsPtr,}ImgPtr: Pointer;
  CurrentScreen: TRect;
  ScreenWidth: Integer;
  ScreenHeight: Integer;
  TrLibErrHand: HGLOBAL;
  BMPDim: BitmapDimensions;
  PColors: ^T1BITIMAGECOLORS;
begin
  OpenImage := 1;

  if Info = nil then
  begin
    exit;
  end;

  // Get screen in logical coords.
  ScreenWidth := PInfo^.ScreenWidth;
  if ScreenWidth = 0 then
    // Still not preset, do it
    LoadPixelsExact := False // It is called from SymbolEditor!
  else
    ScreenHeight := PInfo^.ScreenHeight;
  if (LoadPixelsExact) then
  begin
    // Calculate the screen rectangle escribed RotRect
    PInfo^.GetVirtScreen(CurrentScreen); { Get coords in logical units of device }
    CalcVClipRect4RotRect(PInfo, VRect); { Calc RotRect bounds in display coordinates }
    BMPDim.ReqX := Trunc(TrLib32.PRectWidth(@VRect)
      / TrLib32.PRectWidth(@CurrentScreen) * ScreenWidth);
    BMPDim.ReqY := Trunc(TrLib32.PRectHeight(@VRect)
      / TrLib32.PRectHeight(@CurrentScreen) * ScreenHeight);
    if (BMPDim.ReqX < 1) then
      BMPDim.ReqX := 1;
    if (BMPDim.ReqY < 1) then
      BMPDim.ReqY := 1;
  end
  else
  begin
    //++ Glukhov HELMERT BUILD#115 06.06.00
    BMPDim.ReqX := iStoredW;
    BMPDim.ReqY := iStoredH;
  end;
  BMPDim.RealX := iStoredW;
  BMPDim.RealY := iStoredH;
  //-- Glukhov HELMERT BUILD#115 06.06.00
  BMPDim.BitCount := Info.BitCount;

  CalculateNewImageDimensions(@BMPDim);
  CalcTRAttrib(BMPDim, PInfo, Device);
  OldLoadFactor := BMPDim.LoadFactor;

  // Vector - formats are calculated in this proc. but not loaded
  // because they are drawn by the DLL
  if (Info.IsVector and Info.DrawExternal) then
  begin
    OpenImage := $FFFF;
    exit;
  end;

  //++ Glukhov PrintOptimization BUILD#151 21.02.01
  if (Device = bm_Print) then
  begin
    OpenImage := 0;
    Exit;
  end;
  //-- Glukhov PrintOptimization BUILD#151 21.02.01

  TRLibErrHand := FetchData(BMPDim);

  if (TRLibErrHand <> 0) then
  begin
    ImgData := LPVOID(TRLibErrHand)
  end
  else
  begin
    if (Info.LastError > 0) then
    begin
      OpenImage := Info.LastError + 6;
      Exit;
    end;
  end;

  BMPinfo := nil; {01-006-110399 - Added}
  if PInfo^.Colors256 and (Info.BitCount = 8) then
  begin
    ConvertTo256ColorPalette(BMPDim.ImageSize, FALSE);
    BMPInfo := Info.BitmapInfo;
    if (BMPInfo <> nil) then
    begin
      SetWinPalForBmp(BMPInfo, PInfo^.ExtCanvas.WindowDC);
      //      FreeMem(BMPInfo, Info.LastAlloc);
    end;
  end;

  if PInfo^.Colors256 then
    SetPaletteForBmp(PInfo^.ExtCanvas.Handle);

  if (Device = bm_Display) then
  begin
    //++ Glukhov PrintOptimization BUILD#151 21.02.01
    //    if (not PInfo^.Colors256 or (BMPInfo = nil)) then BMPInfo := Info.BitmapInfo; // 01-006-110399 - Changed
    if BMPInfo = nil then
      BMPInfo := Info.BitmapInfo;
    //-- Glukhov PrintOptimization BUILD#151 21.02.01

    if (BMPInfo = nil) then
    begin
      OpenImage := 6; // 6 -> Zuwenig Speicher
      //Res := GlobalFree(ImgData, 0, MEM_RELEASE);
      Cardinal(ImgData) := GlobalFree(Cardinal(ImgData));
      ImgData := nil;
      Exit;
    end;

    // Wirkliche Ladewerte eintragen
    BMPInfo^.bmiHeader.biWidth := BMPAttrib.PixelsXLoad;
    BMPInfo^.bmiHeader.biHeight := BMPAttrib.PixelsYLoad;

    // BMPInfo^.bmiHeader.biSizeImage := BMPDim.ImageSize;
    // Note, that now ImgData points to BitMapInfo, not to pix values itself
    // to exclude excessive calls of ImgMan

    ImgPtr := GlobalLock(Cardinal(ImgData));
    NewHandle := 0;
    try
      {++Sygsky: Add painting of 1 bit images }
      if (Info.BitCount = 1) and (Info.IsVMSI) then
      begin
        PColors := @BMPInfo.bmiColors[0];
        PColors^[0] := IniFile.VMSIBWSetting.Black;
        PColors^[1] := IniFile.VMSIBWSetting.White;
      end;
      {--Sygsky}

      NewHandle := CreateDIBitmap(PInfo^.ExtCanvas.Handle,
        BMPInfo^.bmiHeader, cbm_Init,
        PChar(ImgPtr) + TrLib32.BitMapInfoSize(@(BMPInfo^.bmiHeader)),
        BMPInfo^, DIB_RGB_Colors);
    finally
      if ImgPtr <> nil then
      begin
        GlobalUnlock(Cardinal(ImgPtr));
      end;
    end;

    FreeMem(BMPInfo, Info.LastAlloc);
    BMPInfo := nil;
  end;

  if (Device = bm_Display) then
  begin
    GlobalFree(Cardinal(ImgData));
    ImgData := nil;
    if (NewHandle <> 0) then
    begin
      if (Image <> 0) then
      begin
        DeleteObject(Image);
      end;
      Image := NewHandle;
    end
    else
    begin
      OpenImage := 5;
      Exit;
    end;
  end;
  OpenImage := 0;
end;

{******************************************************************************}
{ Checks if the image must be reloaded or not / Recalculates image paramters   }
{ size.. ect. When changes to prev. paramters are detected, image is reloaded  }
{ by calling UnLoad - LoadImage                                                }
{******************************************************************************}
{ Parameters: PInfo            The paint structure                             }
{             Device           The device the image will be created for        }
{                                      bm_Display or bm_Print                  }
{******************************************************************************}
{ Return value:   Word         0       - Image not reloaded                    }
{                              1       - Image reloaded                        }
{******************************************************************************}

function TImage.ReLoad(PInfo: PPaint; Device: Integer): Word;
var
  CurrentScreen: TRect;
  ScreenWidth: Integer;
  ScreenHeight: Integer;
  BMPDim: BitmapDimensions;
  PrevAttrib: TRAttrib;
  //  OldClipRect   : TDRect;
  //  VScreenRect: TRect;
  //  OldVScaledClipRect  : TRect;
  WorldRect: TRect;
  DoLoad: Boolean;
  DoUnLoad: Boolean;
  Idx: LongInt;
begin
  DoLoad := FALSE;
  DoUnLoad := FALSE;
  ReLoad := 0;
  if Info = nil then
    Exit; // exit, if not loaded succesfully

  // Store previous parameters
//  OldClipRect.InitByRect(ScaledClipRect);
  Move(BMPAttrib, PrevAttrib, SizeOf(TRAttrib));

  // Get Display bounds in logical units
  PInfo.GetDispScreen(CurrentScreen);
  ScreenWidth := PInfo.ScreenWidth;
  ScreenHeight := PInfo.ScreenHeight;

  // Check a compression/stretching, if the project is not rotated
  if PInfo.ViewRotation = 0 then
    CorrectRotRect;

  if (LoadPixelsExact) then
  begin
    // Calculate the screen rectangle escribed RotRect
    CalcVClipRect4RotRect(PInfo, WorldRect);
    BMPDim.ReqX := Trunc(TrLib32.PRectWidth(@WorldRect)
      / TrLib32.PRectWidth(@CurrentScreen) * ScreenWidth);
    BMPDim.ReqY := Trunc(TrLib32.PRectHeight(@WorldRect)
      / TrLib32.PRectHeight(@CurrentScreen) * ScreenHeight);
    if (BMPDim.ReqX < 1) then
      BMPDim.ReqX := 1;
    if (BMPDim.ReqY < 1) then
      BMPDim.ReqY := 1;
  end
  else
  begin
    BMPDim.ReqX := iStoredW;
    BMPDim.ReqY := iStoredH;
  end;
  BMPDim.RealX := iStoredW;
  BMPDim.RealY := iStoredH;
  BMPDim.BitCount := Info.BitCount;

  CalculateNewImageDimensions(@BMPDim);
  // Renew loading parameters
  CalcTRAttrib(BMPDim, PInfo, Device);
  //++ Glukhov Bug#236 Build#144 18.12.00
  if BMPDim.LoadFactor <> OldLoadFactor then
    DoLoad := True
  else
  begin
    if not CompareMem(@PrevAttrib, @BMPAttrib, SizeOf(TRAttrib)) then
      DoLoad := True
    else
    begin
      if Assigned(IniFile) and
        Assigned(IniFile.AutoBmpLoaded) and
        (IniFile.AutoBmpReload > 0) then // mode 1 or 2
      begin
        // Update buffer of the loaded images
        Idx := IniFile.AutoBmpLoaded.IndexOf(@Self);
        if Idx >= 0 then
        begin
          IniFile.AutoBmpLoaded.AtDelete(Idx);
          IniFile.AutoBmpLoaded.AtInsert(0, @Self);
        end;
      end;
    end;
  end;
  //-- Glukhov Bug#236 Build#144 18.12.00

  //++ Glukhov Bug#236 Build#144 14.12.00
  {
    if not bAlwaysVisible then
    begin
    // Get proj window in logical units of display
      PInfo.GetVirtScreen(VScreenRect);
      if RectsAreNotIntersected(@VScreenRect, @VClipRect) then
      // Rects of Display and Image are not intersected
        if (BMPDim.ImageSize > MinBMPSizeToUnload) or LoadPixelsExact then
        begin
        // Too big to keep in mem. or no order for keeping
          DoUnLoad := TRUE;   // Simply unload non-visible  picture
          DoLoad := FALSE;    // And not reload it again
        end;
    end;

    if (LoadPixelsExact or LoadAreaExact)   // If optimization is preset
      and (PInfo^.bMustLoad and (not DoUnload)) then DoLoad := True;
    ScaledClipRect.InitByRect(OldClipRect);
    if DoUnLoad then begin
      UnLoad;
      ReLoad := 0;
      Exit;
    end;
  }
  //-- Glukhov Bug#236 Build#144 18.12.00
  if DoLoad then
  begin
    Move(PrevAttrib, BMPAttrib, SizeOf(TRAttrib));
    UnLoad;
    //    AutoLoadImage(PInfo, Device);   // Mode 2 is not implemented yet
    LoadImage(PInfo, Device);
    ReLoad := 1;
  end;
end;

{******************************************************************************}
{ Converts the image to the given Colorpalette                                 }
{******************************************************************************}
{ Parameters: PixelSize        Number  of pixels of the imnage                 }
{******************************************************************************}

procedure TImage.ConvertTo256ColorPalette(PixelSize: LongInt; Preview: Boolean);
var
  RefTable: array[0..255] of Byte;
  Size: Word;
  LogPal: ^TLogPalette;
  i: LongInt;
  RedByte: Byte;
  GreenByte: Byte;
  BlueByte: Byte;
  PalIndex: Byte;
  BitsPtr: PByteArray;
  BmpPixel: PByte;
  BMPInfo: PBITMAPINFO;
  ImgPtr: Pointer;
begin
  if (Info.BitCount <> 8) then
    Exit;
  Size := 256 * SizeOf(TPaletteEntry) + 2 * sizeof(WORD);
  GetMem(LogPal, Size);
  BMPInfo := Info.BitmapInfo;
  Move(BMPInfo.bmiColors[0], LogPal.palPalEntry[0], Size - 2 * sizeof(WORD));
  //  FreeMem( BMPInfo, Info.LastAlloc );
  LogPal^.palVersion := $300;
  LogPal^.palNumEntries := TrLib32.BitMapColorsNum(@(BMPInfo^.bmiHeader));
  for i := 0 to 255 do
  begin
    if i < LogPal^.palNumEntries then
    begin
      RedByte := LogPal^.palPalEntry[i].peRed;
      GreenByte := LogPal^.palPalEntry[i].peGreen;
      BlueByte := LogPal^.palPalEntry[i].peBlue;
      PalIndex := GetNearestPaletteIndex(g_hActualPalette, RGB(BlueByte, GreenByte, RedByte));
      RefTable[i] := PalIndex;
    end
    else
      RefTable[i] := 0;
  end;
  FreeMem(LogPal, Size);
  ImgPtr := GlobalLock(Cardinal(ImgData));
  try
    //    BitsPtr := PByteArray( PChar(ImgData) + TrLib32.BitMapInfoSize( @(BMPInfo^.bmiHeader) ) );
    BitsPtr := PByteArray(PChar(ImgPtr) + TrLib32.BitMapInfoSize(@(BMPInfo^.bmiHeader)));
    //    BMPPixel:= PByte(BitsPtr);
    if BitsPtr <> nil then
    begin
      for i := 0 to PixelSize - 1 do
      begin
{$IFDEF WIN32}
        BmpPixel := @BitsPtr[i];
{$ELSE}
        BmpPixel := Ptr(Seg(BitsPtr^) + HiWord(i) * Ofs(AhIncr), LoWord(i));
{$ENDIF}
        BmpPixel^ := RefTable[BmpPixel^]; {Pixelfarbwerte umwandeln}
        //        Inc(BmpPixel);
      end;
    end;
  except
    on EAccessViolation do
    begin
      MsgBox(WinGISMainForm.Handle, 15022, MB_OK or MB_ICONEXCLAMATION, '');
    end;
  end;
  if Assigned(ImgPtr) then
    GlobalUnlock(Cardinal(ImgPtr));
end;

{******************************************************************************}
{ Sets the paramters (pen, brush) in the DC for the selected transparency      }
{******************************************************************************}
{ Parameters: New        Structure with the new paramters (pen, brush, ROP2)   }
{             Old        Structure with the old paramters (pen, brush, ROP2)   }
{             DC         The DC where the paramters are set                    }
{******************************************************************************}

function TImage.SetTransparency(var New, Old: TDrawAttrib; DC: HDC): Boolean;
var
  Value: Byte;
const
  Percent: Single = 255 / 100;
begin
  Result := not
    (((TransparencyType = tt_White) and (Transparency = {10} 0))
    or
    ((TransparencyType = tt_Black) and (Transparency = {10} 0)));
  if not Result then
    Exit; // Nothing to do - we don't need to play with underground polygon
  if TransparencyType = tt_White then
  begin
    Value := {Trunc} Round(Percent * (100 - Transparency) + 0.4);
    New.Brush := CreateSolidBrush(RGB(Value, Value, Value));
    New.Pen := CreatePen(PS_NULL, 0, RGB(Value, Value, Value));
    New.ROP2 := R2_MERGEPEN; // OR bits
  end
  else
  begin
    Value := {Trunc} Round(Percent * Transparency + 0.4);
    New.Brush := CreateSolidBrush(RGB(Value, Value, Value));
    New.Pen := CreatePen(PS_NULL, 0, RGB(Value, Value, Value));
    New.ROP2 := R2_MASKPEN; // AND bits
  end;
  Old.Brush := SelectObject(DC, New.Brush);
  Old.Pen := SelectObject(DC, New.Pen);
  Old.ROP2 := SetROP2(DC, New.ROP2);
end;

{******************************************************************************}
{ Sets the parameters (pen, brush) in the DC for the selected transparency     }
{      and draw transparent rectangle if it will have any effects              }
{******************************************************************************}
{ Parameters: DC         The DC where the paramters are set                    }
{             Points     4 points of polygon corners                           }
{******************************************************************************}

function TImage.SetTransparency(DC: HDC; var Points: TFourPoints): Boolean;
var
  Value: Byte;
  Old, New: TDrawAttrib;
  DoIt: Boolean;
const
  Percent: Single = 255 / 100;
begin
  Result := SetTransparency(New, Old, DC);
  if not Result then
  begin
    Exit; // Nothing to do - we don't need to play with underground polygon
  end;
  Polygon(DC, Points, 4);
  SetROP2(DC, Old.ROP2);
  SelectObject(DC, Old.Brush);
  SelectObject(DC, Old.Pen);
  DeleteObject(New.Pen);
  DeleteObject(New.Brush);
end;

{******************************************************************************}
{ Loads the image by calling the functions out of the Info- Object             }
{******************************************************************************}
{ Parameters: BMPDim     The structure with the loading- paramaters            }
{******************************************************************************}
{ Return value:   HLOBAL       The memory pointer with the data in             }
{******************************************************************************}

function TImage.FetchData(BMPDim: BitmapDimensions): HGLOBAL;
begin
  with BMPAttrib do
  begin
    if (LoadAreaExact) then
    begin
      LPVOID(Result) := Info.LoadPartialImage(StartXPix, StartYPix, PixelsXLoad, PixelsYLoad, BMPDim.LoadFactor);
    end
    else
    begin
      LPVOID(Result) := Info.LoadImage(BMPDim.LoadFactor, BMPDim.LoadX, BMPDim.LoadY);
    end;
  end;
end;

{******************************************************************************}
{ Changes the showing options to on/off by the value of Show                   }
{******************************************************************************}
{ Parameters: Show             TRUE  - Display the image                       }
{                              FALSE - Do not display the image                }
{******************************************************************************}
{ Return value:   Boolean      The previous show- option                       }
{******************************************************************************}

function TImage.ShowImage(Show: Boolean): Boolean;
begin
  ShowImage := (ShowOpt and so_ShowBMP) <> 0;
  if Show then
    ShowOpt := ShowOpt or so_ShowBMP
  else
    ShowOpt := ShowOpt and not so_ShowBMP;
end;

{******************************************************************************}
{ Unloads the image from memory                                                }
{******************************************************************************}

//++ Glukhov Bug#236 Build#144 14.12.00

procedure TImage.UnLoad;
var
  Idx: LongInt;
begin
  if ((Loaded and st_Loaded) <> 0) then
  begin
    try
      if Assigned(IniFile) and
        Assigned(IniFile.AutoBmpLoaded) and
        (IniFile.AutoBmpReload > 0) then // mode 1 or 2
      begin
        // Update buffer of the loaded images
        Idx := IniFile.AutoBmpLoaded.IndexOf(@Self);
        if Idx >= 0 then
        begin
          IniFile.AutoBmpLoaded.AtDelete(Idx);
          IniFile.AutoBmpBfUsed := IniFile.AutoBmpBfUsed - BMPAttrib.ImageSize;
        end;
      end;
    except
      begin
      end;
    end;
    if Image <> 0 then
      DeleteObject(Image);
    Image := 0;
    if Assigned(ImgData) then
      {Virtual}GlobalFree(Cardinal(ImgData) {, 0, MEM_RELEASE});
    ImgData := nil;
    Loaded := Loaded and not st_Loaded;
  end;
end;
//-- Glukhov Bug#236 Build#144 14.12.00

{******************************************************************************}
{ Draws the image to display/printer                                           }
{******************************************************************************}
{ Parameters: PInfo            The paint structure                             }
{             Clipping                                                         }
{                                                                              }
{******************************************************************************}

procedure TImage.Draw(PInfo: PPaint; Clipping: Boolean);
var
  DeviceCaps: Integer;
  StretchMode, PrevStretchMode: Integer;
  New, Old: TDrawAttrib;
  MemDC: THandle;
  OldBitm: THandle;
  //  VXFact, VYFact: Double;
  Point: TDPoint;
  Points: TFourPoints {array[0..3] of TPoint};
  SrcX, SrcY: LongInt;
  SrcW, SrcH: LongInt;
  DestX, DestY: LongInt;
  DestW, DestH: LongInt;
  Rect, VDrawRect: TRect;
  dwROP: DWORD;
  bWasLoaded,
    Printable: Boolean; // Mark if an image overlaps with a paper sheet at all or not
  dbTmpAngle: Double;
  DeviceMode: Integer;
  {++Sygsky optimising PRINT}
  DestBox, { Destination box }
    ClipBox, { Output canvas clipbox }
    DrawBox: TRect; { Result draw canvas rectangle after intersection of Dest and Clip }
  PicBox: TRect; { Picture total box}
  iRes: Integer;
  NewVal: Double;
  hMem: Cardinal;
  DIBPtr: PBitMapInfo;
  {--Sygsky }
  {++ Ivanoff BUG#51 BUILD#95
  Functions for calculation extremals in array}

  function MaxValX: LongInt;
  var
    iI: Integer;
  begin
    Result := Points[0].X;
    for iI := 1 to 3 do
      if Points[iI].X > Result then
        Result := Points[iI].X;
  end;

  function MaxValY: LongInt;
  var
    iI: Integer;
  begin
    Result := Points[0].Y;
    for iI := 1 to 3 do
      if Points[iI].Y > Result then
        Result := Points[iI].Y;
  end;

  function MinValX: LongInt;
  var
    iI: Integer;
  begin
    Result := Points[0].X;
    for iI := 1 to 3 do
      if Points[iI].X < Result then
        Result := Points[iI].X;
  end;

  function MinValY: LongInt;
  var
    iI: Integer;
  begin
    Result := Points[0].Y;
    for iI := 1 to 3 do
      if Points[iI].Y < Result then
        Result := Points[iI].Y;
  end;
  {-- Ivanoff}

  procedure MakePoints; //Syg
  begin
    Point.Init(RotRect.P1.X, Rotrect.P1.Y);
    PInfo^.ConvertToDisp(Point, Points[0]);

    Point.Init(RotRect.P2.X, Rotrect.P2.Y);
    PInfo^.ConvertToDisp(Point, Points[1]);

    Point.Init(RotRect.P3.X, Rotrect.P3.Y);
    PInfo^.ConvertToDisp(Point, Points[2]);

    Point.Init(RotRect.P4.X, Rotrect.P4.Y);
    PInfo^.ConvertToDisp(Point, Points[3]);
  end;
  { Sygsky }

  function MyIntersectRect(var InterSect: TRect; var R1: TRect; var R2: TRect): Boolean;
  begin
    Result := False;
    FillMemory(Addr(InterSect), SizeOf(InterSect), 0);
    if R1.Right < R2.Left then
      Exit;
    if R1.Left > R2.Right then
      Exit;
    if R1.Top < R2.Bottom then
      Exit;
    if R1.Bottom > R2.Top then
      Exit;
    InterSect.Left := Max(R1.Left, R2.Left);
    InterSect.Right := Min(R1.Right, R2.Right);
    InterSect.Top := Min(R1.Top, R2.Top);
    InterSect.Bottom := Max(R1.Bottom, R2.Bottom);
    Result := True;
  end;
  { Sygsky }

  function RectIsInternal(var InternalRect, ExternalRect: TRect): Boolean;
  begin
    Result := False;
    if (InternalRect.Left < ExternalRect.Left) or
      (InternalRect.Right > ExternalRect.Right) then
      Exit;
    if (InternalRect.Top < ExternalRect.Top) or
      (InternalRect.Bottom > ExternalRect.Bottom) then
      Exit;
    Result := True;
  end;

begin
  //++ Glukhov Bug#236 Build#144 04.12.00

  if ((Loaded and st_RotError) <> 0) and (PInfo^.ViewRotation = 0) then
  begin
    Loaded := OldLoaded;
  end;

  if ((not (PInfo^.GetDrawMode(dm_SelDesel))) and ((Loaded and st_AllOK) <> 0))
    and ((ShowOpt and so_ShowBMP) <> 0)
    and (not PInfo.BitmapSettings.GlobInvisiblity) then
  begin //-- Glukhov Bug#236 Build#144 04.12.00 //  if bWasLoaded //  if PInfo.BitmapSettings.GlobInvisiblity then bWasLoaded:= False; //  if not ((ShowOpt and so_ShowBMP) <> 0) then bWasLoaded:= False;
{$IFDEF DRAW_VECTOR}
    // Directly now we don't use vector images( WMF DXF etc)
    // as we can't rotate them!
    if (Info.IsVector and Info.DrawExternal) then
    begin
      DrawVector(PInfo, Clipping);
      exit;
    end;
{$ENDIF}
//    PrevStretchMode := SetStretchBltMode(PInfo^.ExtCanvas.Handle, STRETCH_DELETESCANS);
    DeviceCaps := GetDeviceCaps(PInfo^.ExtCanvas.Handle, Technology);

    //++ Glukhov Bug#236 Build#144 15.12.00
    if DeviceCaps = dt_RasDisplay then
    begin
      DeviceMode := bm_Display;
      StretchMode := STRETCH_DELETESCANS; // To make it fast :o)
    end
    else
    begin
      DeviceMode := bm_Print;
      StretchMode := STRETCH_HALFTONE; // To make it good :o))
    end;
    PrevStretchMode := SetStretchBltMode(PInfo^.ExtCanvas.Handle, StretchMode);

    // Save the current view rotation
    bWasLoaded := (Loaded and st_Loaded) <> 0;
    dbTmpAngle := PInfo^.ViewRotation;

    if not bSymbolImage then
    begin
      // Check that the image to be drawn was loaded
      if not bWasLoaded then
      begin // Load image before
        //        AutoLoadImage(PInfo, DeviceMode);   // Mode 2 is not implemented yet
        LoadImage(PInfo, DeviceMode);
        Rotation := dbTmpAngle;
      end
      else
      begin
        if (Rotation = dbTmpAngle) then
          Reload(PInfo, DeviceMode)
        else
        begin
          Unload;
          //          AutoLoadImage(PInfo, DeviceMode);   // Mode 2 is not implemented yet
          LoadImage(PInfo, DeviceMode);
          Rotation := dbTmpAngle;
        end;
      end;
    end;
    //-- Glukhov Bug#236 Build#144 15.12.00

      // Calculate the screen points for RotRect
    MakePoints;
    SetRect(VScaledClipRect, VDrawRect);

    if bBMPBigEnough then
    begin
      if ((Loaded and st_Loaded) <> 0) or PInfo^.Printing then
      begin
        if PInfo^.Printing then // First, set initial values as is
        begin
          DestX := MinValX;
          DestY := MaxValY;
          DestW := Round(Abs(MaxValX - DestX));
          DestH := Round(-Abs(MinValY - DestY));

          SrcX := (Info.Width - iStoredW) div 2;
          SrcY := (Info.Height - iStoredH) div 2;
          SrcW := iStoredW;
          SrcH := iStoredH;

          {++Sygsky:PRINT}
                    // Mark that something can be printed. Just in case :o)
          Printable := True;
          { Get tightest clipping bounds box of output DC in logical units of printing DC}
          iRes := GetClipBox(PInfo.ExtCanvas.Handle, ClipBox); { Note that it is a Windows TRECT not WinGIS one }
          if (iRes <> NULLREGION) and (iRes <> ERROR) then
          begin
            { Create next Windows TRECT for our job rectangle}
            DestBox := Classes.Rect(DestX, DestY, DestX + DestW - 1, DestY + DestH - 1 {sic!});
            if not TrLib32.RectsAreEqual(ClipBox, DestBox) then
            begin { Some difference is found ! }
              // Intersect them to get really printed rectangle!
              if MyIntersectRect(DrawBox, ClipBox, DestBox) then
              begin { Intersection is detected !! }
                { Create rect of the whole bitmap pixels to be printed }
                PicBox := Classes.Rect(SrcX, SrcY, SrcX + SrcW - 1, SrcY + SrcH - 1);
                { Make intersections for every of 4 sides}

                { DrawBox.Left should be >= DestBox.Left }
                if (DrawBox.Left <> DestBox.Left) then
                begin
                  NewVal := (DrawBox.Left - DestBox.Left);
                  PicBox.Left := Round(SrcW * NewVal / DestW);
                end;
                if (DrawBox.Right <> DestBox.Right) then
                begin
                  NewVal := (DrawBox.Right - DestBox.Left);
                  PicBox.Right := Round(SrcW * NewVal / DestW);
                end;
                if (DrawBox.Top <> DestBox.Top) then
                begin
                  NewVal := (DrawBox.Top - DestBox.Top);
                  PicBox.Top := Round(SrcH * NewVal / DestH);
                end;
                if (DrawBox.Bottom <> DestBox.Bottom) then
                begin
                  NewVal := (DrawBox.Bottom - DestBox.Top);
                  PicBox.Bottom := Round(SrcH * NewVal / DestH);
                end;
                { Recalculate print/bitmap rectangles for really printed areas }
                DestX := DrawBox.Left;
                DestY := DrawBox.Top;
                DestW := Abs(TrLib32.PRectWidth(@DrawBox));
                DestH := -TrLib32.PRectHeight(@DrawBox);
                SrcX := PicBox.Left;
                SrcY := PicBox.Top;
                SrcW := TrLib32.PRectWidth(@PicBox);
                SrcH := TrLib32.PRectHeight(@PicBox);
              end
              else // Intersection wasn't detected, mark this!
                Printable := False;
            end;
          end
          else
          begin
            Printable := False;
          end;

          {--Sygsky}
        end // of printer DC specifics processing
        else // it isn't a print operation, DC doesn't belong to a printer
        begin
          //          VXFact:= BMPAttrib.PixelsXLoad / TrLib32.PRectWidth(@VScaledClipRect);
          //          VYFact:= BMPAttrib.PixelsYLoad / TrLib32.PRectHeight(@VScaledClipRect);

          DestX := VDrawRect.Left;
          DestY := VDrawRect.Bottom;
          DestW := Round(Abs(TrLib32.PRectWidth(@VDrawRect)));
          DestH := Round(-Abs(TrLib32.PRectHeight(@VDrawRect)));

          //          SrcX:= Trunc((VDrawRect.Left - VScaledClipRect.Left) * VXFact);
          //          SrcY:= Trunc((VScaledClipRect.Bottom - VDrawRect.Bottom) * VYFact);
          //          SrcW := Trunc(Abs(TrLib32.PRectWidth(@VDrawRect) * VXFact));
          //          SrcH := Trunc(Abs(TrLib32.PRectHeight(@VDrawRect) * VYFact));
          SrcX := 0;
          SrcY := 0;
          SrcW := BMPAttrib.PixelsXLoad;
          SrcH := BMPAttrib.PixelsYLoad;
        end;

        if SrcW = 0 then
          Inc(SrcW);
        if SrcH = 0 then
          Inc(SrcH);

        if (TransparencyType = tt_White) then
          dwROP := SrcAND
        else
          dwROP := SrcPAINT;
        if (DeviceCaps = dt_RasDisplay) then
        begin
          MemDC := CreateCompatibleDC(PInfo^.ExtCanvas.Handle);
          OldBitm := SelectObject(MemDC, Image);

          //++Sygsky : Create wanted background to prepare for transparency

{$IFDEF OLD}
          if SetTransparency(New, Old, PInfo^.ExtCanvas.Handle) then
          begin
            Polygon(PInfo^.ExtCanvas.Handle, Points, 4);
            SetROP2(PInfo^.ExtCanvas.Handle, Old.ROP2);
            SelectObject(PInfo^.ExtCanvas.Handle, Old.Brush);
            SelectObject(PInfo^.ExtCanvas.Handle, Old.Pen);
            DeleteObject(New.Pen);
            DeleteObject(New.Brush);
          end;
{$ELSE} // NEW VERSION HERE!
          if not SetTransparency(PInfo^.ExtCanvas.Handle, Points) then // no transparency set
          begin
            dwROP := SRCCOPY; // so use simplest/fastest mode
          end;
{$ENDIF}
//          if (ShowOpt and so_ShowBMP) <> 0 then
//          begin

          StretchBlt(PInfo^.ExtCanvas.Handle, DestX, DestY, DestW - 1, DestH + 1,
            MemDC, SrcX, SrcY, SrcW, SrcH, dwROP);
//          end;
          SelectObject(MemDC, OldBitm);
          DeleteDC(MemDC);
        end // if DeviceCaps == dt_RasDisplay
        else
        begin
          {---------- PRINTING STARTS FROM HERE ----------}
          if Printable then // Ha-ha, now it even willn't try to print beyond paper
          begin
            if not SetTransparency(PInfo^.ExtCanvas.Handle, Points) then // No any transparency used
            begin
              dwROP := SRCCOPY; // This is the outlet, use simple operation!!!
            end;
            if IsRotated then // Project is rotated
            begin
//              if (TransparencyType = tt_White) then
              Info.Print(PInfo^.ExtCanvas.Handle,
                DestX, DestY, DestW, DestH, SrcX, SrcY, SrcW, SrcH, dwROP {SrcAND});
//              else
//                Info.Print(PInfo^.ExtCanvas.Handle,
//                  DestX, DestY, DestW, DestH, SrcX, SrcY, SrcW, SrcH, SrcPaint);
            end // if IsRotated
            else
            begin // Not rotated
              if (Info.BitCount = 1) then
              begin
                if (Transparency <> 0) then // Some percentage is set
                begin
//                  if (TransparencyType = tt_White) then
                  Info.Print(PInfo^.ExtCanvas.Handle,
                    DestX, DestY, DestW, DestH, SrcX, SrcY, SrcW, SrcH, dwROP {SrcAND});
//                  else
//                    Info.Print(PInfo^.ExtCanvas.Handle,
//                      DestX, DestY, DestW, DestH, SrcX, SrcY, SrcW, SrcH, SrcPaint);
                end
                else
                  Info.Print(PInfo^.ExtCanvas.Handle,
                    DestX, DestY, DestW, DestH, SrcX, SrcY, SrcW, SrcH, SrcCopy);
              end { BitmapInfo^.bmiHeader.biBitCount == 1 }
              else
              begin
//                if (TransparencyType = tt_White) then
                Info.Print(PInfo^.ExtCanvas.Handle,
                  DestX, DestY, DestW, DestH, SrcX, SrcY, SrcW, SrcH, dwROP {SrcAND});
//                else
//                  Info.Print(PInfo^.ExtCanvas.Handle,
//                    DestX, DestY, DestW, DestH, SrcX, SrcY, SrcW, SrcH, SrcPaint);
              end; { if - else - BitmapInfo^.bmiHeader.biBitCount == 1 }
            end; { if - else - IsRotated }
          end; { if - Printable }
           //++ Glukhov PrintImage BUILD#123 17.07.00
          if (not bWasLoaded) and ((Loaded and st_Loaded) <> 0) then
            Unload;
          //-- Glukhov PrintImage BUILD#123 17.07.00
        end; { if - else - Device == dt_RasDisplay }
      end; { if(Loaded AND st_Loaded) <> 0}
    end; { IsBigEnough }
    SetStretchBltMode(PInfo^.ExtCanvas.Handle, PrevStretchMode);
  end;
  // Draw a frame around the image, using its local settings
  if ((Options and opt_HideFrame) = 0) or PInfo^.GetDrawMode(dm_SelDesel) then
  begin
    MakePoints;
    PInfo^.ExtCanvas.MoveTo(Points[0].X, Points[0].Y);
    PInfo^.ExtCanvas.LineTo(Points[1].X, Points[1].Y);
    PInfo^.ExtCanvas.LineTo(Points[2].X, Points[2].Y);
    PInfo^.ExtCanvas.LineTo(Points[3].X, Points[3].Y);
    PInfo^.ExtCanvas.LineTo(Points[0].X, Points[0].Y);
  end;
end;

{01-004-040399 - Begin}
{******************************************************************************}
{ Draws a vector image to display/printer                                      }
{******************************************************************************}
{ Parameters: PInfo            The paint structure                             }
{             Clipping                                                         }
{                                                                              }
{******************************************************************************}

procedure TImage.DrawVector(PInfo: PPaint; Clipping: Boolean);
var
  DeviceCaps: Integer;
  Screen: TDRect;
  DrawRect: TDRect;
  XFact: Real;
  YFact: Real;
  SrcX, SrcY,
    SrcW, SrcH,
    DestX, DestY,
    DestW, DestH: Integer;
  BmpBigEnough: Boolean;
  sRect, sSrc, sDest: TRect;
begin
  DeviceCaps := GetDeviceCaps(PInfo^.ExtCanvas.Handle, Technology);
  if (DeviceCaps = dt_RasDisplay) then
    PInfo^.GetProjScreen(Screen)
  else
    Screen.Assign(-MaxLongInt, -MaxLongint, MaxLongInt, MaxLongInt);

  ScaledClipRect.Intersect(Screen, DrawRect);

  PInfo^.ConvertToDisp(ClipRect.A, TRectToPoint(sRect).P1);
  PInfo^.ConvertToDisp(ClipRect.B, TRectToPoint(sRect).P2);

  if (((sRect.Right - sRect.Left) > PInfo^.BitmapSettings.ShowMinSize) and
    ((sRect.Bottom - sRect.Top) > PInfo^.BitmapSettings.ShowMinSize)) or
    (DeviceCaps <> dt_RasDisplay) or PInfo^.Printing then
    BmpBigEnough := TRUE
  else
    BmpBigEnough := FALSE; {Darstellen der Bitmaps ab Gr��e - alt:100}

  PInfo^.ConvertToDisp(DrawRect.A, TRectToPoint(sRect).P1);
  PInfo^.ConvertToDisp(DrawRect.B, TRectToPoint(sRect).P2);

  if BMPBigEnough then
  begin
    XFact := BMPAttrib.PixelsXLoad / ScaledClipRect.XSize;
    YFact := BMPAttrib.PixelsYLoad / ScaledClipRect.YSize;

    DestX := sRect.Left;
    DestY := sRect.Bottom;
    DestW := Round(Abs(sRect.Right - sRect.Left));
    DestH := Round(-Abs(sRect.Bottom - sRect.Top));

    SrcX := Trunc(DrawRect.A.XDist(ScaledClipRect.A) * XFact);
    SrcY := Trunc(ScaledClipRect.B.YDist(DrawRect.B) * YFact);
    SrcW := Trunc(DrawRect.XSize * XFact);
    SrcH := Trunc(DrawRect.YSize * YFact);

    //      SetTransparency(New, Old, PInfo^.ExtCanvas.Handle);
    //      Rectangle(PInfo^.ExtCanvas.Handle,sRect.Left,sRect.Top,sRect.Right,sRect.Bottom);

    sDest := Rect(DestX, DestY, DestX + DestW, DestY + DestH);
    sSrc := Rect(SrcX, SrcY, SrcX + SrcW, SrcY + SrcH);

    Info.Draw(PInfo^.ExtCanvas.Handle, @sDest, @sSrc, 1, SrcCopy)
  end; { IsBigEnough }
  { Zeichnen des Rahmens }

  if ((Options and opt_HideFrame) = 0) or PInfo^.GetDrawMode(dm_SelDesel) then
  begin
    PInfo^.ConvertToDisp(ClipRect.A, TRectToPoint(sRect).P1);
    PInfo^.ConvertToDisp(ClipRect.B, TRectToPoint(sRect).P2);
    PInfo^.ExtCanvas.FrameRect(sRect);
  end;
end;
{01-004-040399 - End}

{******************************************************************************}
{ Displays an Image- Open Error Dialog checks if the image should be Detached  }
{ from project or just not be loeded an sets the corresponding values in       }
{ the Loaded- variable [st_NoLoad, st_PathChg, st_Delete]                      }
{******************************************************************************}
{ Parameters: OEOptions        oe_None or oe_AllBMP                            }
{                            oe_None   - No special action                     }
{                            oe_AllBMP - Action is laready set for all BMP's   }
{******************************************************************************}

procedure TImage.OpenError(OEOptions: Word);
begin
  Loaded := Loaded or LongWord(st_NoLoad); { �bergehen .. Bild nicht lasden, kann im Eigenschaften- Dialog gel�scht werden }
end;

function TImage.GetObjType: Word;
begin
  GetObjType := ot_Image;
end;

{******************************************************************************}
{ Load/Store routines                                                          }
{******************************************************************************}

constructor TImage.LoadOld(S: TOldStream);
var
  Status: Word;
begin
  TView.Load(S);
  Image := 0;
  FileName := S.ReadStr;
  AbsolutePath := NewStr(FileName^);
  RelativePath := NewStr(ExtractRelativePath(ProjectName, FileName^));
  bRelPath := False;
  //++ Glukhov RotRect loading BUILD#121 02.07.00
  MakeRotFromClip;
  //-- Glukhov RotRect loading BUILD#121 02.07.00
  Loaded := st_Error;
  OldLoadFactor := 0;

  // Init the data for the image transformation
  InitTransformationData;

  //++ Glukhov Bug#236 Build#144 15.12.00
  if IniFile.BitmapSettings.ShowBitmap then
    ShowOpt := so_ShowBMP
  else
    ShowOpt := 0;
  if IniFile.BitmapSettings.ShowFrame then
    Options := 0
  else
    Options := opt_HideFrame;
  Transparency := IniFile.BitmapSettings.Transparency;
  TransparencyType := IniFile.BitmapSettings.TransparencyType;
  //-- Glukhov Bug#236 Build#144 15.12.00

  ScaledClipRect.InitByRect(ClipRect);
  InitFile(S);
  if (Loaded and st_Delete) <> 0 then
    Exit;
  if (Loaded and st_NoLoad) = 0 then
  begin
    OpenInfo(Status);
    if (Status <> err_OK) then
      Loaded := st_WrongType
    else
    begin
      Loaded := Loaded or st_AllOK;
      InitStoredWH;
    end;
  end;

  Border := New(PCPoly, Init);

end;

constructor TImage.Load(S: TOldStream);
var
  Status: Word;
begin
  TView.Load(S);
  Image := 0;
  FileName := S.ReadStr;
  AbsolutePath := NewStr(FileName^);
  RelativePath := NewStr(ExtractRelativePath(ProjectName, FileName^));
  bRelPath := False;
  S.Read(Rotation, sizeof(Rotation));
  S.Read(RotRect, sizeof(TImgRect));
  //++ Glukhov RotRect loading BUILD#121 02.07.00
  MakeRotFromClip;
  //-- Glukhov RotRect loading BUILD#121 02.07.00
  Loaded := st_Error;
  OldLoadFactor := 0;

  // YG, I do not understand it, flag RotSaved will be reset a bit later
  if Rotation > 7.0 then
  begin
    RotSaved := TRUE;
    Rotation := Rotation - 10.0;
  end;

  // Init the data for the image transformation
  InitTransformationData;

  //++ Glukhov Bug#236 Build#144 15.12.00
  if IniFile.BitmapSettings.ShowBitmap then
    ShowOpt := so_ShowBMP
  else
    ShowOpt := 0;
  if IniFile.BitmapSettings.ShowFrame then
    Options := 0
  else
    Options := opt_HideFrame;
  Transparency := IniFile.BitmapSettings.Transparency;
  TransparencyType := IniFile.BitmapSettings.TransparencyType;
  //-- Glukhov Bug#236 Build#144 15.12.00

  ScaledClipRect.InitByRect(ClipRect);
  InitFile(S);
  if (Loaded and st_Delete) <> 0 then
    Exit;
  if (Loaded and st_NoLoad) = 0 then
  begin
    OpenInfo(Status);
    if (Status <> err_OK) then
      Loaded := st_WrongType
    else
    begin
      Loaded := Loaded or st_AllOK;
      InitStoredWH;
    end;
  end;

  Border := New(PCPoly, Init);

end;

constructor TImage.LoadNew(S: TOldStream);
var
  ShowBitmap: Boolean;
  Status: Word;
begin
  TView.Load(S);
  Image := 0;
  FileName := S.ReadStr;
  AbsolutePath := NewStr(FileName^);
  RelativePath := NewStr(ExtractRelativePath(ProjectName, FileName^));
  bRelPath := False;
  S.Read(Rotation, sizeof(Rotation));
  S.Read(RotRect, sizeof(TImgRect));
  //++ Glukhov RotRect loading BUILD#121 02.07.00
  MakeRotFromClip;
  //-- Glukhov RotRect loading BUILD#121 02.07.00
  Loaded := st_Error;

  // YG, I do not understand it, flag RotSaved will be reset a bit later
  if Rotation > 7.0 then
  begin
    RotSaved := TRUE;
    Rotation := Rotation - 10.0;
  end;

  // Init the data for the image transformation
  InitTransformationData;

  //++ Glukhov Bug#236 Build#144 15.12.00
  S.Read(ShowBitmap, SizeOf(ShowBitmap));
  if ShowBitmap then
    ShowOpt := so_ShowBMP
  else
    ShowOpt := 0;
  if IniFile.BitmapSettings.ShowFrame then
    Options := 0
  else
    Options := opt_HideFrame;
  Transparency := IniFile.BitmapSettings.Transparency;
  TransparencyType := IniFile.BitmapSettings.TransparencyType;
  //-- Glukhov Bug#236 Build#144 15.12.00

  ScaledClipRect.InitByRect(ClipRect);
  InitFile(S);
  if (Loaded and st_Delete) <> 0 then
    Exit;
  if (Loaded and st_NoLoad) = 0 then
  begin
    OpenInfo(Status);
    if (Status <> err_OK) then
      Loaded := st_WrongType
    else
    begin
      Loaded := Loaded or st_AllOK;
      InitStoredWH;
    end;
  end;
  OldLoadFactor := 0;

  Border := New(PCPoly, Init);

end;

constructor TImage.LoadV321(S: TOldStream);
var
  ShowBitmap: Boolean;
  Status: Word;
begin
  TView.Load(S);
  Image := 0;
  FileName := S.ReadStr;
  AbsolutePath := NewStr(FileName^);
  RelativePath := NewStr(ExtractRelativePath(ProjectName, FileName^));
  bRelPath := False;
  S.Read(Rotation, sizeof(Rotation));
  S.Read(RotRect, sizeof(TImgRect));
  //++ Glukhov RotRect loading BUILD#121 02.07.00
  MakeRotFromClip;
  //-- Glukhov RotRect loading BUILD#121 02.07.00
  Loaded := st_Error;

  // YG, I do not understand it, flag RotSaved will be reset a bit later
  if Rotation > 7.0 then
  begin
    RotSaved := TRUE;
    Rotation := Rotation - 10.0;
  end;

  // Init the data for the image transformation
  InitTransformationData;

  //++ Glukhov Bug#236 Build#144 15.12.00
  S.Read(ShowBitmap, SizeOf(ShowBitmap));
  if ShowBitmap then
    ShowOpt := so_ShowBMP
  else
    ShowOpt := 0;
  if IniFile.BitmapSettings.ShowFrame then
    Options := 0
  else
    Options := opt_HideFrame;

  S.Read(Transparency, SizeOf(Transparency));
  if (Transparency > 100) then
    Transparency := 100;

  S.Read(TransparencyType, SizeOf(TransparencyType));
  if (TransparencyType <> tt_White) and (TransparencyType <> tt_Black) then
    TransparencyType := tt_White;
  //-- Glukhov Bug#236 Build#144 15.12.00

  ScaledClipRect.InitByRect(ClipRect);
  InitFile(S);
  if (Loaded and st_Delete) <> 0 then
    Exit;
  if (Loaded and st_NoLoad) = 0 then
  begin
    OpenInfo(Status);
    if (Status <> err_OK) then
      Loaded := st_WrongType
    else
    begin
      Loaded := Loaded or st_AllOK;
      InitStoredWH;
    end;
  end;
  OldLoadFactor := 0;

  Border := New(PCPoly, Init);

end;

constructor TImage.LoadV33(S: TOldStream);
var
  ShowBitmap: Boolean;
  Status: Word;
  SnapPoint: Byte; {01-010299-003 - Obsolete variable}
begin
  TView.Load(S);
  Image := 0;
  FileName := S.ReadStr;
  AbsolutePath := NewStr(FileName^);
  RelativePath := NewStr(ExtractRelativePath(ProjectName, FileName^));
  bRelPath := False;
  S.Read(Rotation, sizeof(Rotation));
  S.Read(SnapPoint, sizeof(SnapPoint));
  S.Read(RotRect, sizeof(TImgRect));
  //++ Glukhov RotRect loading BUILD#121 02.07.00
  MakeRotFromClip;
  //-- Glukhov RotRect loading BUILD#121 02.07.00
  Loaded := st_Error;

  // YG, I do not understand it, flag RotSaved will be reset a bit later
  if Rotation > 7.0 then
  begin
    RotSaved := TRUE;
    Rotation := Rotation - 10.0;
  end;

  // Init the data for the image transformation
  InitTransformationData;

  //++ Glukhov Bug#236 Build#144 15.12.00
  S.Read(ShowBitmap, SizeOf(ShowBitmap));
  if ShowBitmap then
    ShowOpt := so_ShowBMP
  else
    ShowOpt := 0;

  S.Read(Transparency, SizeOf(Transparency));
  if (Transparency > 100) then
    Transparency := 100;

  S.Read(TransparencyType, SizeOf(TransparencyType));
  if (TransparencyType <> tt_White) and (TransparencyType <> tt_Black) then
    TransparencyType := tt_White;

  S.Read(Options, SizeOf(Options)); // opt_ShowBmp is ignored now
  //-- Glukhov Bug#236 Build#144 15.12.00

  ScaledClipRect.InitByRect(ClipRect);
  InitFile(S);
  if (Loaded and st_Delete) <> 0 then
    Exit;
  if (Loaded and st_NoLoad) = 0 then
  begin
    OpenInfo(Status);
    if (Status <> err_OK) then
      Loaded := st_WrongType
    else
    begin
      Loaded := Loaded or st_AllOK;
      InitStoredWH;
    end;
  end;
  OldLoadFactor := 0;

  Border := New(PCPoly, Init);

end;

constructor TImage.LoadV34(S: TOldStream);
var
  Version: LongInt;
  Status: Word;
begin
  TView.Load(S);
  Image := 0;
  S.Read(Version, sizeof(Version));
  FileName := S.ReadStr;
  if FileName = nil then
  begin
    FileName := NewStr('');
  end;
  AbsolutePath := NewStr(FileName^);
  RelativePath := NewStr(ExtractRelativePath(ProjectName, FileName^));
  bRelPath := False;
  S.Read(IsRotated, sizeof(IsRotated));
  S.Read(Rotation, sizeof(Rotation));
  S.Read(RotSaved, sizeof(RotSaved)); // ?
  S.Read(RotRect, sizeof(TImgRect));
  //++ Glukhov RotRect loading BUILD#121 02.07.00
  MakeRotFromClip;
  //-- Glukhov RotRect loading BUILD#121 02.07.00
  Loaded := st_Error;
  OldLoadFactor := 0;

  // Init the data for the image transformation
  InitTransformationData;

  //++ Glukhov Bug#236 Build#144 15.12.00
  S.Read(ShowOpt, sizeof(ShowOpt));

  S.Read(Transparency, SizeOf(Transparency));
  if (Transparency > 100) then
    Transparency := 100;

  S.Read(TransparencyType, SizeOf(TransparencyType));
  if (TransparencyType <> tt_White) and (TransparencyType <> tt_Black) then
    TransparencyType := tt_White;

  S.Read(Options, SizeOf(Options)); // opt_ShowBmp is ignored now
  //-- Glukhov Bug#236 Build#144 15.12.00

  ScaledClipRect.InitByRect(ClipRect);
  InitFile(S);
  if (Loaded and st_Delete) <> 0 then
    Exit;
  if (Loaded and st_NoLoad) = 0 then
  begin
    OpenInfo(Status);
    if (Status <> err_OK) then
      Loaded := st_WrongType
    else
    begin
      Loaded := Loaded or st_AllOK;
      InitStoredWH;
    end;
  end;

  Border := New(PCPoly, Init);

end;

{************************** New load/store routines ******************}

procedure TImage.LoadFileNames(S: TOLEClientStream);
var
  Name, Path: string;
begin
  AbsolutePath := S.ReadStr;
  RelativePath := S.ReadStr;

  if RelativePath = nil then
  begin
    RelativePath := NewStr(ProjectName);
  end;

  Name := ExtractFileName(RelativePath^);
  Path := ExtractFilePath(ProjectName);
  if Path[Length(Path)] <> '\' then
    Path := Path + '\';
  if not FileExists(RelativePath^) then
    if FileExists(Path + Name) then
      RelativePath^ := Path + Name;
  if not FileExists(AbsolutePath^) then
    if FileExists(Path + Name) then
      AbsolutePath^ := Path + Name;
end;

constructor TImage.LoadV4(S: TOLEClientStream);
var
  Status: Word;
  bVMSI: Boolean;
begin
  TView.Load(S);
  Image := 0;
  {    AbsolutePath := S.ReadStr;
      RelativePath := S.ReadStr;}
  LoadFileNames(S);
  S.Read(bRelPath, sizeof(bRelPath));
  if (bRelPath) then
    FileName := NewStr(RelativePath^)
  else
    FileName := NewStr(AbsolutePath^);
  S.Read(IsRotated, sizeof(IsRotated));
  S.Read(Rotation, sizeof(Rotation));
  S.Read(RotRect, sizeof(TImgRect));
  Loaded := st_Error;
  OldLoadFactor := 0;

  // Init the data for the image transformation
  InitTransformationData;

  //++ Glukhov Bug#236 Build#144 15.12.00
  S.Read(ShowOpt, sizeof(ShowOpt));

  S.Read(Transparency, SizeOf(Transparency));
  if (Transparency > 100) then
    Transparency := 100;

  S.Read(TransparencyType, SizeOf(TransparencyType));
  if (TransparencyType <> tt_White) and (TransparencyType <> tt_Black) then
    TransparencyType := tt_White;

  S.Read(Options, SizeOf(Options)); // opt_ShowBmp is ignored now
  //-- Glukhov Bug#236 Build#144 15.12.00

  ScaledClipRect.InitByRect(ClipRect);
  InitFile(S);

  if (Loaded and st_Delete) <> 0 then
    Exit;

  if RelativePath <> nil then
  begin
    bVMSI := ((Uppercase(ExtractFileExt(RelativePath^)) = '.PRE')
      or (Uppercase(ExtractFileExt(RelativePath^)) = '.PRI'));
  end;

  if (Loaded and st_NoLoad) = 0 then
  begin
    if not bVMSI then
    begin
      OpenInfo(Status);
    end;
    if (Status <> err_OK) then
    begin
      if (Status = err_WrongFormat) then
        Loaded := st_WrongType
      else
        if (Status = err_ReadErr) then
          Loaded := st_ReadErr;
    end
    else
    begin
      Loaded := Loaded or st_AllOK;
      if not bVMSI then
      begin
        InitStoredWH;
      end;
    end;
  end;

  //end;

  Border := New(PCPoly, Init);

end;

procedure TImage.Store(S: TOldStream);
{var
  CurClipRect: TDRect;}
begin
  {CurClipRect.InitByRect(ClipRect);
  ClipRect.Assign(RealClipRect.A.X,RealClipRect.A.Y,RealClipRect.B.X,RealClipRect.B.Y);}
  TView.Store(S);
  {    ClipRect.Assign(CurClipRect.A.X,CurClipRect.A.Y,CurClipRect.B.X,CurClipRect.B.Y);
      CurClipRect.Done;}
  S.WriteStr(AbsolutePath);
  S.WriteStr(RelativePath);
  S.Write(bRelPath, sizeof(bRelPath));
  S.Write(IsRotated, sizeof(IsRotated));
  S.Write(Rotation, sizeof(Rotation));
  S.Write(RotRect, sizeof(TImgRect));
  S.Write(ShowOpt, SizeOf(ShowOpt));
  S.Write(Transparency, sizeof(Transparency)); { Transp. }
  S.Write(TransparencyType, sizeof(TransparencyType)); { Transp. }
  S.Write(Options, SizeOf(Options));
end;

{******************************************************************************}
{ Selection / Search routines                                                  }
{******************************************************************************}

function TImage.SelectByPoint
  (
  PInfo: PPaint;
  Point: TDPoint;
  const ObjType: TObjectTypes
  )
  : PIndex;
begin
  if (ot_Image in ObjType) and ClipRect.PointInside(Point) then
    SelectByPoint := @Self
  else
    SelectByPoint := nil;
end;

procedure TImage.SetClipRect(PInfo: PPaint);
var
  Screen: TDRect;
  XScale: Real;
  YScale: Real;
  bCoordFromHeader: Boolean;
  head: TECWHeader;
  s: TFileStream;
  x, y, w, h: Double;
  px, py: Integer;

begin
///////////
  bCoordFromHeader := False;

  if (Uppercase(ExtractFileExt(Filename^)) = '.ECW') then
  begin
    s := TFileStream.Create(Filename^, fmOpenRead or fmShareDenyNone);
    try
      s.Read(head, sizeof(head));
      x := head.OrigX * 100;
      y := head.OrigY * 100;
      px := Self.Info.Width;
      py := Self.Info.Height;
      w := (px * head.IncX) * 100;
      h := (py * head.IncY) * 100;
      if (w <> 0) and (h <> 0) and (StrPas(head.Proj) <> 'RAW') then
      begin
        ClipRect.A.X := Trunc(x);
        ClipRect.A.Y := Trunc(y) + Trunc(h);
        ClipRect.B.X := Trunc(x) + Trunc(w);
        ClipRect.B.Y := Trunc(y);
        ScaledClipRect.Assign(ClipRect.A.X, ClipRect.A.Y, ClipRect.B.X, ClipRect.B.Y);
        CorrectRotRect;
        bCoordFromHeader := True;
      end;
    finally
      s.Free;
    end;
  end;

  if (Uppercase(ExtractFileExt(Filename^)) = '.TIF') then
  begin
     ////
  end;

  if bCoordFromHeader then
    exit;

  PInfo^.GetProjScreen(Screen);
  XScale := 1.0;
  YScale := 1.0;
  if ((Loaded and st_AllOK) <> 0) then
  begin
    XScale := Screen.XSize * 2 / 3 / Info.Width;
    YScale := Screen.YSize * 2 / 3 / Info.Height;
  end;
  if YScale < XScale then
    XScale := YScale;
  if ((Loaded and st_AlloK) <> 0) then
  begin
    ClipRect.Assign(0, 0, Trunc(Info.Width * XScale), Trunc(Info.Height * XScale));
    ClipRect.MoveRel(Trunc(Screen.A.X + Screen.XSize / 6), Trunc(Screen.A.Y + Screen.YSize / 6));
  end;
  ScaledClipRect.Assign(ClipRect.A.X, ClipRect.A.Y, ClipRect.B.X, ClipRect.B.Y);
  CorrectRotRect;
end;

function TImage.SelectByCircle
  (
  PInfo: PPaint;
  Middle: TDPoint;
  Radius: LongInt;
  const ObjType: TObjectTypes;
  Inside: Boolean
  )
  : Boolean;
var
  DPoly: TCPoly;
begin
  if (ot_Image in ObjType) then
  begin
    DPoly.Init;
    GetBorder(PInfo, @DPoly);
    SelectByCircle := DPoly.SelectByCircle(PInfo, Middle, Radius, [ot_CPoly], Inside);
    DPoly.Done;
  end
  else
    SelectByCircle := FALSE
end;

procedure TImage.GetBorder
  (
  PInfo: PPaint;
  APoly: PCPoly
  );
var
  DPoint: TDPoint;
begin
  DPoint.Init(0, 0);
  with ClipRect do
  begin
    DPoint := A;
    APoly^.InsertPoint(DPoint);
    DPoint.Init(A.X, B.Y);
    APoly^.InsertPoint(DPoint);
    DPoint := B;
    APoly^.InsertPoint(DPoint);
    DPoint.Init(B.X, A.Y);
    APoly^.InsertPoint(DPoint);
  end;
  APoly^.ClosePoly;
end;

function TImage.SelectByPoly
  (
  PInfo: PPaint;
  Poly: Pointer;
  const ObjType: TObjectTypes;
  Inside: Boolean
  )
  : Boolean;
var
  DPoly: TCPoly;
begin
  SelectByPoly := FALSE;
  if (ot_Image in ObjType) then
  begin
    DPoly.Init;
    GetBorder(PInfo, @DPoly);
    SelectByPoly := DPoly.SelectByPoly(PInfo, Poly, [ot_CPoly], Inside);
    DPoly.Done;
  end;
end;

function TImage.SelectByPolyLine
  (
  PInfo: PPaint;
  Poly: Pointer;
  const ObjType: TObjectTypes
  )
  : Boolean;
var
  DPoly: TCPoly;
begin
  SelectByPolyLine := FALSE;
  if (ot_Image in ObjType) then
  begin
    DPoly.Init;
    GetBorder(PInfo, @DPoly);
    SelectByPolyLine := DPoly.SelectByPolyLine(PInfo, Poly, [ot_CPoly]);
    DPoly.Done;
  end;
end;

function TImage.SearchForPoint
  (
  PInfo: PPaint;
  InCircle: Pointer;
  Exclude: LongInt;
  const ObjType: TObjectTypes;
  var Point: TDPoint;
  var SnObje: PIndex
  )
  : Boolean;
var
  Position: TDPoint;
begin
  SearchForPoint := FALSE;
  if (GetObjType in ObjType)
    and (Exclude <> Index) then
  begin
    SearchForPoint := TRUE;
    Position.Init(ClipRect.A.X, ClipRect.B.Y);
    if PEllipse(InCircle)^.PointInside(Position) then
      Point := Position
    else
    begin
      Position.Init(ClipRect.B.X, ClipRect.B.Y);
      if PEllipse(InCircle)^.PointInside(Position) then
        Point := Position
      else
      begin
        Position.Init(ClipRect.B.X, ClipRect.A.Y);
        if PEllipse(InCircle)^.PointInside(Position) then
          Point := Position
        else
        begin
          Position.Init(ClipRect.A.X, ClipRect.A.Y);
          if PEllipse(InCircle)^.PointInside(Position) then
            Point := Position
          else
            SearchForPoint := FALSE;
        end;
      end;
    end;
  end;
end;

function TImage.SearchForNearestPoint
  (
  PInfo: PPaint;
  var ClickPos: TDPoint;
  InCircle: Pointer;
  Exclude: LongInt;
  const ObjType: TObjectTypes;
  var Point: TDPoint;
  var SnObje: PIndex
  )
  : Boolean;
var
  Position: TDPoint;
begin
  Result := FALSE;
  if (GetObjType in ObjType) and (Exclude <> Index) then
  begin
    Result := TRUE;
    Position.Init(ClipRect.A.X, ClipRect.B.Y);
    if PEllipse(InCircle)^.PointInside(Position) and
      (ClickPos.Dist(Point) > ClickPos.Dist(Position)) then
      Point := Position
    else
    begin
      Position.Init(ClipRect.B.X, ClipRect.B.Y);
      if PEllipse(InCircle)^.PointInside(Position) and
        (ClickPos.Dist(Point) > ClickPos.Dist(Position)) then
        Point := Position
      else
      begin
        Position.Init(ClipRect.A.X, ClipRect.A.Y);
        if PEllipse(InCircle)^.PointInside(Position) and
          (ClickPos.Dist(Point) > ClickPos.Dist(Position)) then
          Point := Position
        else
        begin
          Position.Init(ClipRect.B.X, ClipRect.A.Y);
          if PEllipse(InCircle)^.PointInside(Position) and
            (ClickPos.Dist(Point) > ClickPos.Dist(Position)) then
            Point := Position
          else
            Result := FALSE;
        end;
      end;
    end;
  end;
end;

function TImage.SelectBetweenPolys
  (
  PInfo: PPaint;
  Poly1: Pointer;
  Poly2: Pointer;
  const ObjType: TObjectTypes;
  Inside: Boolean
  )
  : Boolean;
var
  DPoly: TCPoly;
begin
  SelectBetweenPolys := FALSE;
  if (ot_Image in ObjType) then
  begin
    DPoly.Init;
    GetBorder(PInfo, @DPoly);
    SelectBetweenPolys := DPoly.SelectBetweenPolys(PInfo, Poly1, Poly2, [ot_CPoly], Inside);
    DPoly.Done;
  end;
end;

{******************************************************************************}
{ Checks if all imgae- points from Raster- transformation are in the image     }
{******************************************************************************}
{ Parameters: Points           Ther points                                     }
{             PtCnt            Number of points                                }
{******************************************************************************}
{ Return value:   Boolean      True    - All points are in the image           }
{                              FALSE   - At least one point is outside the img }
{******************************************************************************}

function TImage.CheckPoints(Points: Pointer; PtCnt: Word): Boolean;
var
  i: Integer;
  Pts: PTLPoints;
  Point: TDPoint;
begin
  CheckPoints := True;

  Pts := PTLPoints(Points);
  for i := 0 to PtCnt - 1 do
  begin
    Point.Init(Pts^[(i * 2) + 1].X, Pts^[(i * 2) + 1].Y);
    if ClipRect.PointInside(Point) = False then
    begin
      CheckPoints := False;
      Exit;
    end;
  end;
end;

//--------------------------------------
//++ Glukhov BugCorrection BUILD#121 30.06.00
//--------------------------------------
// Init the image variables

procedure TImage.InitStoredWH;
begin
  iInitialW := Info.Width;
  iInitialH := Info.Height;
  iStoredW := iInitialW;
  iStoredH := iInitialH;
end;
//-- Glukhov BugCorrection BUILD#121 30.06.00
//--------------------------------------

//--------------------------------------
//++ Glukhov HELMERT BUILD#115 02.06.00
// Init the data for the image transformation

procedure TImage.InitTransformationData;
begin
  IsRotated := False;
  //++ Glukhov Bug#236 Build#144 15.12.00
  //  if (Rotation <> 0.0) then IsRotated:= True;
  Rotation := 0.0;
  //-- Glukhov Bug#236 Build#144 15.12.00
  dInitialA := Rotation;
  dStoredA := 0.0;
  dStoredK := 1.0;
  dStoredR := 0.0;
  sRotatedFileName := '';
  sAffineTFileName := '';
  RotSaved := False;
  //++ Glukhov BugCorrection BUILD#121 30.06.00
  // To avoid a dividing by zero
  if iStoredW = 0 then
    iStoredW := 2;
  if iStoredH = 0 then
    iStoredH := 2;
  if iInitialW = 0 then
    iInitialW := 2;
  if iInitialH = 0 then
    iInitialH := 2;
  //-- Glukhov BugCorrection BUILD#121 30.06.00
end;
//-- Glukhov HELMERT BUILD#115 02.06.00
//--------------------------------------

//--------------------------------------
{++ Glukhov HELMERT BUILD#115 02.06.00 }
// Prepare a new temporary file name for bitmap transformation

procedure TImage.GetTempBitmapFileName(var ouFileName: string);
var
  TempPath: array[0..MAX_PATH] of Char;
  TempFileName: array[0..MAX_PATH] of Char;
begin
  ouFileName := '';
  GetTempPath(Max_Path, TempPath);
  GetTempFileName(TempPath, 'wg', 0, TempFileName);
  ouFileName := StrPas(TempFileName);
  DeleteFile(ouFileName);
  ouFileName := ExtractFilePath(ouFileName) + ExtractFileName(ouFileName) + '.bmp';
end;
{-- Glukhov HELMERT BUILD#115 02.06.00 }
//--------------------------------------

//--------------------------------------
{++ Glukhov HELMERT BUILD#115 07.06.00 }
// Prepare the size of the useful bitmap in the source image

procedure TImage.CalcStoredWH(
  iSrcW, iSrcH: LongInt;
  dbA1, dbK, dbA2: Double
  );
var
  Pnt: TDPoint;
begin
  Pnt.Init(iSrcW, iSrcH);
  Pnt.Rotate(dbA1);
  if dbA2 <> 0 then
  begin
    Pnt.X := Round(Pnt.X * dbK);
    Pnt.Y := Round(Pnt.Y / dbK);
  end;
  Pnt.Rotate(dbA2);
  iStoredW := Abs(Pnt.X);
  iStoredH := Abs(Pnt.Y);

  Pnt.Init(-iSrcW, iSrcH);
  Pnt.Rotate(dbA1);
  if dbA2 <> 0 then
  begin
    Pnt.X := Round(Pnt.X * dbK);
    Pnt.Y := Round(Pnt.Y / dbK);
  end;
  Pnt.Rotate(dbA2);
  if iStoredW < Abs(Pnt.X) then
    iStoredW := Abs(Pnt.X);
  if iStoredH < Abs(Pnt.Y) then
    iStoredH := Abs(Pnt.Y);
end;
{-- Glukhov HELMERT BUILD#115 07.06.00 }
//--------------------------------------

//--------------------------------------
{++ Glukhov HELMERT BUILD#115 31.05.00 }

procedure TImage.CalcVClipRect4RotRect(PInfo: PPaint; var Rect: TRect);
var
  x, y: Double;
  i, j: Integer;
begin
  x := RotRect.P1.X;
  y := RotRect.P1.Y;
  PInfo^.ConvertToDispDouble(x, y);
  //  i:= Round( Int(x) );  j:= Round( x );  if i > j then i:= j;
  //  Rect.Left:= i; Rect.Right:= j;
  //  i:= Round( Int(y) );  j:= Round( y );  if i > j then i:= j;
  //  Rect.Bottom:= j; Rect.Top:= i;
  i := Round(Int(x));
  j := Round(x);
  Rect.Left := j;
  Rect.Right := j;
  i := Round(Int(y));
  j := Round(y);
  Rect.Bottom := j;
  Rect.Top := j;

  x := RotRect.P2.X;
  y := RotRect.P2.Y;
  PInfo^.ConvertToDispDouble(x, y);
  i := Round(Int(x));
  j := Round(x);
  //  if Rect.Left > i  then Rect.Left:= i;
  if Rect.Left > j then
    Rect.Left := j;
  if Rect.Right < j then
    Rect.Right := j;
  i := Round(Int(y));
  j := Round(y);
  if Rect.Bottom < j then
    Rect.Bottom := j;
  //  if Rect.Top > i then Rect.Top:= i;
  if Rect.Top > j then
    Rect.Top := j;

  x := RotRect.P3.X;
  y := RotRect.P3.Y;
  PInfo^.ConvertToDispDouble(x, y);
  i := Round(Int(x));
  j := Round(x);
  if Rect.Left > j then
    Rect.Left := j;
  if Rect.Right < j then
    Rect.Right := j;
  i := Round(Int(y));
  j := Round(y);
  if Rect.Bottom < j then
    Rect.Bottom := j;
  if Rect.Top > j then
    Rect.Top := j;

  x := RotRect.P4.X;
  y := RotRect.P4.Y;
  PInfo^.ConvertToDispDouble(x, y);
  i := Round(Int(x));
  j := Round(x);
  if Rect.Left > j then
    Rect.Left := j;
  if Rect.Right < j then
    Rect.Right := j;
  i := Round(Int(y));
  j := Round(y);
  if Rect.Bottom < j then
    Rect.Bottom := j;
  if Rect.Top > j then
    Rect.Top := j;
end;
{-- Glukhov HELMERT BUILD#115 31.05.00 }
//--------------------------------------

//--------------------------------------
{++ Glukhov HELMERT BUILD#115 23.05.00 }

procedure TImage.CalcSpecTransformationData(
  ParLB, ParRB, ParLT: TLPoint;
  var Angle1, kX, Angle2: Double
  );
var
  ImgLB, ImgRB, ImgLT: TLPoint;
  dbA1, dbB1: Double;
  dbA2, dbB2: Double;
  liC1, liC2: LongInt;
  ResCode: Integer;
begin
  // Set data for image rectangle
  ImgLB.X := 0;
  ImgLB.Y := 0;
  ImgRB.X := iInitialW - 1;
  if ImgRB.X < 1 then
    ImgRB.X := 1;
  ImgRB.Y := 0;
  ImgLT.X := 0;
  ImgLT.Y := iInitialH - 1;
  if ImgLT.Y < 1 then
    ImgLT.Y := 1;
  // Calculate parameters of transformation: BitMap -> RotRect
  ProcGetAffineTransformationMatrix(
    ImgLB, ImgRB, ImgLT, ParLB, ParRB, ParLT,
    dbA1, dbB1, dbA2, dbB2, liC1, liC2, ResCode);
  // Calculate the special data of the calculated transformation
  ProcGetSpecAffineTransformationData(
    dbA1, dbB1, dbA2, dbB2, Angle1, kX, Angle2);
  Angle1 := ProcAdjustAngleSlightly(Angle1, 0, 2 * Pi);
  Angle2 := ProcAdjustAngleSlightly(Angle2, 0, 2 * Pi);
  kX := sqrt(kX); // We will use the square root of X-stretch
end;
{-- Glukhov HELMERT BUILD#115 23.05.00 }

{++ Glukhov HELMERT BUILD#115 23.05.00 }
//******************************************************************************
// Calculates the surrounding image rectangle for rotated project,
// then Calculates the summary angle of rotation,
// and rotates image and saves its name and angle of rotation
//******************************************************************************
// Parameters: Angle            The rotation angle
//             PInfo            The paint structure
//******************************************************************************
// Return value:   Byte         0       - All OK
//                              1       - general error
//                              2       - operation cancelled by the user
//******************************************************************************

procedure TImage.RotateBitmap(Angle: double; PInfo: PPaint);
var
  outFileName: string;
  Status: Word;
  Pnt1: TDPoint;
  dbA1, dbA2: Double;
  dbK, dbK2: Double;
  EpsVal: Double;
  RotLB, RotRB, RotLT: TLPoint;
begin
  if not ((Loaded and st_AllOK) <> 0) then
    Exit;

  if (ClipRect.XSize < 0) or (ClipRect.YSize < 0) then
  begin
    ClipRect.CorrectSize;
    PProj(PInfo^.AProj)^.UpdateCliprect(@Self);
    PProj(PInfo^.AProj)^.SetModified;
  end;

  // Prepare the Eps value
  EpsVal := 2 / (iInitialW + iInitialH);

  //++ Glukhov OptTransformation Build#150 29.01.01
  if (RotRect.P1.X = ClipRect.A.X) and
    (RotRect.P1.Y = ClipRect.B.Y) and
    (RotRect.P4.X = ClipRect.A.X) and
    (RotRect.P4.Y = ClipRect.A.Y) then
  begin
    // The bitmap was not transformed before
    dbA1 := 0;
    dbK := (ClipRect.XSize * iInitialH) / (iInitialW * ClipRect.YSize);
    dbK := sqrt(dbK);
    dbA2 := Angle;
  end
  else
  begin
    // Prepare the transformed RotRect
    RotLB.X := RotRect.P4.X; // left-lower vertex is not moved
    RotLB.Y := RotRect.P4.Y;
    if Angle = 0.0 then
    begin
      // Use RotRect as it is because the project was not rotated
      RotRB.X := RotRect.P3.X;
      RotRB.Y := RotRect.P3.Y;
      RotLT.X := RotRect.P1.X;
      RotLT.Y := RotRect.P1.Y;
    end
    else
    begin
      // Calculate the parallelogram that is RotRect
      // rotated by the given angle around left-lower vertex
      Pnt1.Init(RotRect.P3.X - RotRect.P4.X, RotRect.P3.Y - RotRect.P4.Y);
      Pnt1.Rotate(Angle);
      RotRB.X := Pnt1.X + RotRect.P4.X;
      RotRB.Y := Pnt1.Y + RotRect.P4.Y;
      Pnt1.Init(RotRect.P1.X - RotRect.P4.X, RotRect.P1.Y - RotRect.P4.Y);
      Pnt1.Rotate(Angle);
      RotLT.X := Pnt1.X + RotRect.P4.X;
      RotLT.Y := Pnt1.Y + RotRect.P4.Y;
    end;

    // Calculate the transformation parameters
    CalcSpecTransformationData(RotLB, RotRB, RotLT, dbA1, dbK, dbA2);
  end;
  //-- Glukhov OptTransformation Build#150 29.01.01

    // Set project sign for angles
  dbA1 := ProcAdjustAngleSlightly(-dbA1, -Pi, Pi);
  dbA2 := ProcAdjustAngleSlightly(-dbA2, -Pi, Pi);

  // Round the transformation data to minimize the number of raster operations
  Pnt1.Init(iInitialW, iInitialH);
  Pnt1.X := Round(Pnt1.X * dbK);
  Pnt1.Y := Round(Pnt1.Y / dbK);
  if (Abs(iInitialW * (1 - dbK)) < 2) and
    (Abs(iInitialH * (1 - 1 / dbK)) < 2) then
  begin
    dbK := 1.0;
    if bSymbolImage then
    begin
      // Use one summary angle
      dbA1 := dbA2 + dbA1;
      dbA2 := 0;
    end
    else
    begin
      // Distinguish image and project rotation
      dbA2 := dbA2 + dbA1;
      dbA1 := dbA2 + Angle;
      dbA2 := dbA2 - dbA1;
    end;
    dbA1 := ProcAdjustAngleSlightly(dbA1, -Pi, Pi);
    dbA2 := ProcAdjustAngleSlightly(dbA2, -Pi, Pi);
  end;

  if (Abs(dbA1) < EpsVal) then
    dbA1 := 0.0;
  if (Abs(dbA2) < EpsVal) then
    dbA2 := 0.0;

  // Prepare the transformed bitmap file
  // and Calculate the size of the useful bitmap in the source image
  DisposeStr(FileName);
  RotSaved := False;

  // For zero or changed 2nd angle the old rotated bitmap is not needed
//    if (dbA2 = 0) or (dbA2 <> dStoredR) then  begin
  if (dbA2 = 0) or (Abs(dbA2 - dStoredR) >= EpsVal) then
  begin
    // Delete the rotated file, if any
    if sRotatedFileName <> '' then
    begin
      DeleteFile(sRotatedFileName);
      sRotatedFileName := '';
      dStoredR := 0.0;
    end;
  end;

  //    if (dbA1 = 0) and (dbA2 = 0) then
  //    if ( (dbA1 = 0) and (Abs(dbA2) < EpsVal) ) or
  if ((dbA1 = 0) and (dbA2 = 0)) or
    ((dbK = 1) and (Abs(dbA1 + dbA2) < EpsVal)) then
  begin
    // Both zero angles mean that we should use the original image file
    FileName := NewStr(AbsolutePath^);
    IsRotated := False;
    OpenInfo(Status);
    // Refresh the initial size
    InitStoredWH;
    Exit;
    // We are keeping the transformed file (if any) as kind of cash
    // It should be deleted at event Done
  end;

  IsRotated := True;
  //    if (dbA1 = dStoredA) and (dbK = dStoredK) and (dbA2 = dStoredR) then
  if (Abs(dbA1 - dStoredA) < EpsVal) and
    (Abs(dbK - dStoredK) < EpsVal) and
    (Abs(dbA2 - dStoredR) < EpsVal) then
  begin
    // All data is the same
    if dbA2 <> 0 then
      FileName := NewStr(sRotatedFileName)
    else
      FileName := NewStr(sAffineTFileName);
    OpenInfo(Status);
    CalcStoredWH(iInitialW, iInitialH, dbA1, dbK, dbA2);
    RotSaved := True;
    Exit;
    // The transformed files should be deleted at event Done
  end;

  //    if (dbA1 = dStoredA) and (dbA2 = 0) then
  if (Abs(dbA1 - dStoredA) < EpsVal) and (dbA2 = 0) then
  begin
    // Use the old transformed file
    FileName := NewStr(sAffineTFileName);
    OpenInfo(Status);
    CalcStoredWH(iInitialW, iInitialH, dbA1, dStoredK, dbA2);
    RotSaved := True;
    Exit;
    // The transformed files should be deleted at event Done
  end;

  // Transformation is needed

  // Check the old transformed file
//    if (dbA1 <> dStoredA) then  begin
  if (Abs(dbA1 - dStoredA) >= EpsVal) then
  begin
    if (dbA1 <> 0.0) or (dbK <> 1.0) then
    begin
      // The old transformed file can not be used
      if sAffineTFileName <> '' then
      begin
        // Delete it, if any
        DeleteFile(sAffineTFileName);
        sAffineTFileName := '';
        dStoredA := 0.0;
        dStoredK := 1.0;
      end;
    end;
  end;

  if sAffineTFileName <> '' then
    FileName := NewStr(sAffineTFileName) // Use the transformed file
  else
    FileName := NewStr(AbsolutePath^); // Use the original file
  OpenInfo(Status);

  // Check the 1st rotation
//    if (dbA1 <> 0) and (dbA1 <> dStoredA) then
  if (dbA1 <> 0) and (Abs(dbA1 - dStoredA) >= EpsVal) then
  begin
    // Prepare a new temporary file name for the rotated image
    GetTempBitmapFileName(outFileName);
    // Rotate bitmap
    if (Info.Rotate(outFilename, dbA1, PInfo^.HWindow) <> 0) then
    begin // Error
      iStoredW := Info.Width;
      iStoredH := Info.Height;
      Exit;
    end;
    // Save the transformation
    dStoredA := dbA1;
    dStoredK := 1.0;
    sAffineTFileName := outFilename;
    DisposeStr(FileName);
    FileName := NewStr(outFilename);
    OpenInfo(Status);
  end;

  // Check the X-stretch
  if (dbK <> dStoredK) then
  begin
    // Make a stretch
    dbK2 := dbK / dStoredK;
    Pnt1.X := Round(Info.Width * dbK2);
    Pnt1.Y := Round(Info.Height / dbK2);
    if (Pnt1.X <> Info.Width) or (Pnt1.Y <> Info.Height) then
    begin
      // Prepare a new temporary file name for the stretched image
      GetTempBitmapFileName(outFileName);
      Info.Stretch(outFilename, Pnt1.X, Pnt1.Y);
      // Delete the previous file, if any
      if sAffineTFileName <> '' then
        DeleteFile(sAffineTFileName);
      sAffineTFileName := outFilename;
      // Save the transformation
      dStoredK := dbK;
      DisposeStr(FileName);
      FileName := NewStr(outFilename);
      OpenInfo(Status);
    end;
  end;

  // Check the 2nd rotation
  if (dbA2 <> 0) then
  begin
    // Prepare a new temporary file name for the rotated image
    GetTempBitmapFileName(outFileName);
    // Rotate bitmap
    if (Info.Rotate(outFilename, dbA2, PInfo^.HWindow) <> 0) then
    begin // Error
      iStoredW := Info.Width;
      iStoredH := Info.Height;
      Exit;
    end;
    // Save the transformation
    dStoredR := dbA2;
    // Delete the previous file, if any
    if sRotatedFileName <> '' then
      DeleteFile(sRotatedFileName);
    sRotatedFileName := outFilename;
    DisposeStr(FileName);
    FileName := NewStr(outFilename);
    OpenInfo(Status);
  end;

  CalcStoredWH(iInitialW, iInitialH, dStoredA, dStoredK, dStoredR);
  RotSaved := True;

  // Unload the old data  - YG, I am not sure it is actually needed
  UnLoad;
end;
{-- Glukhov HELMERT BUILD#115 23.05.00 }
//--------------------------------------

{******************************************************************************}
{ Calculates the new rotation rectangle for the chaged Clipping rectangle      }
{******************************************************************************}
// Works if there is no a rotation

procedure TImage.CorrectRotRect;
var
  XFact, YFact: Double;
  ClipA, ClipB: TDPoint;
begin
  ProcGetClipRect(
    RotRect.P1, RotRect.P2, RotRect.P3, RotRect.P4, ClipA, ClipB);
  if (ClipA.X <> ClipRect.A.X) or (ClipA.Y <> ClipRect.A.Y)
    or (ClipB.X <> ClipRect.B.X) or (ClipB.Y <> ClipRect.B.Y) then
  begin
    if (ClipA.X <> ClipB.X) and (ClipA.Y <> ClipB.Y) then
    begin
      // Make a compression for nonzero Rect
      XFact := (ClipRect.B.X - ClipRect.A.X) / (ClipB.X - ClipA.X);
      YFact := (ClipRect.B.Y - ClipRect.A.Y) / (ClipB.Y - ClipA.Y);

      RotRect.P1.X := ClipRect.A.X + Round(XFact * (RotRect.P1.X - ClipA.X));
      RotRect.P2.X := ClipRect.A.X + Round(XFact * (RotRect.P2.X - ClipA.X));
      RotRect.P3.X := ClipRect.A.X + Round(XFact * (RotRect.P3.X - ClipA.X));
      RotRect.P4.X := ClipRect.A.X + Round(XFact * (RotRect.P4.X - ClipA.X));

      RotRect.P1.Y := ClipRect.A.Y + Round(YFact * (RotRect.P1.Y - ClipA.Y));
      RotRect.P2.Y := ClipRect.A.Y + Round(YFact * (RotRect.P2.Y - ClipA.Y));
      RotRect.P3.Y := ClipRect.A.Y + Round(YFact * (RotRect.P3.Y - ClipA.Y));
      RotRect.P4.Y := ClipRect.A.Y + Round(YFact * (RotRect.P4.Y - ClipA.Y));
    end;
  end;
end;

{******************************************************************************}
{ Sets the image palette as the actual palette in the DC                       }
{ Works only on images with max,. 256 colors (BitCount <= 8)                   }
{******************************************************************************}
{ Parameters: DC               The correspnding DC                             }
{******************************************************************************}
{ Return value:   hPalette     0       -  No palette was set (BitCount > 8)    }
{                            > 0       -  The old palette                      }
{******************************************************************************}

function TImage.SetPalette(DC: HDC): hPalette;
var
  Size: Word;
  LogPal: ^TLogPalette;
  ThePalette: hPalette;
  oldPal: HPalette;
  i: Integer;
  BMPInfo: PBITMAPINFO;
begin
  if Info.BitCount > 8 then
  begin
    SetPalette := 0;
    Exit;
  end;
  Size := ((1 shl Info.BitCount) * sizeof(TRGBQuad)) + 4;
  GetMem(LogPal, Size);
  BMPInfo := Info.BitmapInfo;
  LogPal^.palVersion := $300;
  LogPal^.palNumEntries := 1 shl Info.BitCount;
  for i := 0 to (1 shl Info.BitCount) - 1 do
  begin
    LogPal^.palPalEntry[i].peRed := BMPInfo^.bmiColors[i].rgbBlue;
    LogPal^.palPalEntry[i].peBlue := BMPInfo^.bmiColors[i].rgbRed;
    LogPal^.palPalEntry[i].peGreen := BMPInfo^.bmiColors[i].rgbGreen;
    LogPal^.palPalEntry[i].peFlags := 0;
  end;
  FreeMem(BMPInfo, Info.LastAlloc);
  ThePalette := CreatePalette(LogPal^);
  FreeMem(LogPal, Size);
  OldPal := SelectPalette(DC, ThePalette, FALSE);
  RealizePalette(DC);
  SetPalette := OldPal;
end;

{******************************************************************************}
{ Fetches the BMP's palette and copies it into the ThePalette256 global        }
{ variable                                                                     }
{ Selects and realizes the palette in the DC, the old palette is stored in     }
{ the global WinGISPal variable                                                }
{******************************************************************************}
{ Parameters: DC               The window DC for the palette                   }
{******************************************************************************}
{ Return value:   Word         0 - Error occured                               }
{                              1 - No errors                                   }
{******************************************************************************}

function TImage.GetPalette(DC: HDC): Word;
var
  Size: Word;
  LogPal: PLogPalette;
  i: Integer;
  {  FirstPal: HPalette;}
  BMPInfo: PBITMAPINFO;
begin
  if (Info.BitCount > 8) then
  begin
    GetPalette := 0;
    Exit;
  end;
  Size := sizeof(TLogPalette) + ((1 shl Info.BitCount) * sizeof(TPaletteEntry));
  GetMem(LogPal, Size);
  BMPInfo := Info.BitmapInfo;
  LogPal^.palVersion := $300;
  LogPal^.palNumEntries := 1 shl Info.BitCount;
  for i := 0 to (1 shl Info.BitCount) - 1 do
  begin
    {    LogPal^.palPalEntry[i].peRed := BMPInfo^.bmiColors[i].rgbBlue;
        LogPal^.palPalEntry[i].peBlue := BMPInfo^.bmiColors[i].rgbRed;
        LogPal^.palPalEntry[i].peGreen := BMPInfo^.bmiColors[i].rgbGreen;}

    LogPal^.palPalEntry[i].peRed := BMPInfo^.bmiColors[i].rgbRed;
    LogPal^.palPalEntry[i].peBlue := BMPInfo^.bmiColors[i].rgbBlue;
    LogPal^.palPalEntry[i].peGreen := BMPInfo^.bmiColors[i].rgbGreen;

    LogPal^.palPalEntry[i].peFlags := 0;
  end;
  if (g_hBMPalette <> 0) then
    DeleteObject(g_hBMPalette);
  g_hBMPalette := CreatePalette(LogPal^);
  FreeMem(LogPal, Size);
  if g_hBMPalette <> 0 then
    Result := 1
  else
    Result := 0;
end;

{******************************************************************************}
{ Calculates loadfactor, required img. size ... for the functions for          }
{ area exact and pixels excat loading                                          }
{******************************************************************************}
{ Parameters: ImgDim           Pointer to the Image Dimensions                 }
{                               This variable will be changeds in function !   }
{******************************************************************************}

procedure TImage.CalculateNewImageDimensions(ImgDim: PBitmapDimensions);
var
  XFact, YFact: Real;
  tXFact, tYFact: LongInt;
begin
  with ImgDim^ do
  begin
    XFact := RealX / ReqX;
    YFact := RealY / ReqY;
    tXFact := Round(XFact); {It is ONLY !true! solution}
    tYFact := Round(YFact);
    if (tXFact > tYFact) then
      LoadFactor := tYFact
    else
      LoadFactor := tXFact;
    LoadFactor := LoadFactor;
    if LoadFactor = 0 then
      LoadFactor := 1;
    LoadX := RealX div LoadFactor;
    LoadY := RealY div LoadFactor;
    ImageSize := ((((LoadX * Info.BitCount) + 31) div 32) * 4) * LoadY;
  end;
end;

{******************************************************************************}
{ TSImage - Image-Type for symbols                                             }
{******************************************************************************}

constructor TSImage.Init(aFileName: string; aShowOpt: Word; const BitmapSettings: TBitmapSettings; aDoEmbed: Boolean);
begin
  inherited Init(aFileName, '', aShowOpt, BitmapSettings);
  DoEmbed := aDoEmbed;
end;

destructor TSImage.Done;
begin
  if (DoEmbed) then
    if (lpTempFileName <> nil) then
      DeleteFile(lpTempFileName^);
  TImage.Done;
end;

procedure TSImage.LoadFileNames(S: TOLEClientStream);
begin
  AbsolutePath := S.ReadStr;
  RelativePath := S.ReadStr;
  if DoEmbed then
  begin
    DisposeStr(AbsolutePath);
    DisposeStr(RelativePath);
    AbsolutePath := NewStr(lpTempFileName^);
    RelativePath := NewStr(lpTempFileName^);
  end;
end;

procedure TSImage.InitExactLoading;
begin

  LoadPixelsExact := False;
  LoadAreaExact := False;

end;

constructor TSImage.Load(S: TOLEClientStream);
var
  acTempPath, acFileName: array[0..255] of Char;
  lpImg: Pointer;
  hFile: THandle;
  SECA: TSECURITYATTRIBUTES;
  iWritten: DWord;
  iImgSize: DWORD;
  acExt: array[0..12] of Char;
  acProjN: array[0..255] of Char;
begin
  S.Read(DoEmbed, sizeof(DoEmbed));
  if DoEmbed then
  begin
    Ext := S.ReadStr;
    S.Read(iImgSize, sizeof(iImgSize));
    strpcopy(@acExt[0], Ext^);
    GetTempPath(256, acTempPath);
    strpcopy(@acProjN[0], ExtractFileName(TMDIChild(TOLEClientStream(S).ParentWindow).FName));
    strpcopy(@acExt[0], ExtractFileExt(TMDIChild(TOLEClientStream(S).ParentWindow).FName));
    acProjN[strlen(@acProjN[0]) - strlen(@acExt[0])] := #0;
    strcat(@acProjN[0], '_00');
    strpcopy(@acExt[0], Ext^);
    GetTempFileName(acTempPath, @acProjN[0], iImgSize, acFileName);
    strcopy(PChar(DWORD(@acFileName[0]) + strlen(acFileName) - 3), @acExt[0]);
    lpTempFileName := NewStr(StrPas(@acFileName[0]));
    lpImg := VirtualAlloc(nil, iImgSize, MEM_COMMIT or MEM_RESERVE, PAGE_READWRITE);
    if (lpImg = nil) then
    begin { Speicher konnte nicht alloziert werden }
    end;
    S.Read(lpImg^, iImgSize); { Bild einlesen }
    SECA.nLength := sizeof(SECA);
    SECA.lpSecurityDescriptor := nil;
    SECA.bInheritHandle := TRUE;
    hFile := CreateFile(@acFileName[0], GENERIC_READ or GENERIC_WRITE, FILE_SHARE_READ, @SECA,
      CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
    WriteFile(hFile, lpImg^, iImgSize, iWritten, nil);
    CloseHandle(hFile);
    VirtualFree(lpImg, 0, MEM_RELEASE);
  end;
  inherited LoadV4(S);
end;

procedure TSImage.Store(S: TOldStream);
var
  lpImg: Pointer;
  hFile: THandle;
  SECA: TSECURITYATTRIBUTES;
  iRead: DWord;
  iImgSize: DWORD;
  acFName: array[0..255] of Char;
begin
  S.Write(DoEmbed, sizeof(DoEmbed));
  if (DoEmbed) then
  begin
    Ext := NewStr(ExtractFileExt(AbsolutePath^));
    Delete(Ext^, 1, 1);
    S.WriteStr(Ext);
    SECA.nLength := sizeof(SECA);
    SECA.lpSecurityDescriptor := nil;
    SECA.bInheritHandle := TRUE;
    strpcopy(@acFName[0], AbsolutePath^);
    hFile := CreateFile(@acFName[0], GENERIC_READ, FILE_SHARE_READ, @SECA,
      OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
    iImgSize := GetFileSize(hFile, nil);
    lpImg := VirtualAlloc(nil, iImgSize, MEM_RESERVE or MEM_COMMIT, PAGE_READWRITE);
    ReadFile(hFile, lpImg^, iImgSize, iRead, nil);
    S.Write(iImgSize, sizeof(iImgSize));
    S.Write(lpImg^, iImgSize);
    CloseHandle(hFile);
    VirtualFree(lpImg, 0, MEM_RELEASE);
  end;
  inherited Store(S);
end;

procedure TSImage.Draw(PInfo: PPaint; Clipping: Boolean);
var
  OldCR, OldSCR: TDRect;
  DeviceCaps: Integer;
  DeviceMode: Integer;
  dpTempDPoint: TDPoint;
  dbAngle: Double;
  P1, P2, P3, P4: TDPoint;
begin
  // Store the current data for symbol
  OldCR.InitbyRect(ClipRect);
  OLDSCR.InitbyRect(ScaledClipRect);

  P1.Init(RotRect.P1.X, RotRect.P1.Y);
  P2.Init(RotRect.P2.X, RotRect.P2.Y);
  P3.Init(RotRect.P3.X, RotRect.P3.Y);
  P4.Init(RotRect.P4.X, RotRect.P4.Y);

  //++ Glukhov HELMERT BUILD#115 25.05.00
  {
    if not Assigned(PInfo^.AProj) then
      bAlwaysVisible := True
    else
    begin
      if PProj(PInfo^.AProj)^.SymbolMode = sym_SymbolMode then
        bAlwaysVisible := True
      else
        bAlwaysVisible := False;
    end;
  }
  //-- Glukhov HELMERT BUILD#115 25.05.00

  // Correct RotRect coordinates for a Symbol rotated by angle
  MakeRotFromClip;

  dpTempDPoint.X := RotRect.P1.X;
  dpTempDPoint.Y := RotRect.P1.Y;
  PInfo^.ScaleToDraw(dpTempDPoint);
  RotRect.P1.X := dpTempDPoint.X;
  RotRect.P1.Y := dpTempDPoint.Y;

  dpTempDPoint.X := RotRect.P2.X;
  dpTempDPoint.Y := RotRect.P2.Y;
  PInfo^.ScaleToDraw(dpTempDPoint);
  RotRect.P2.X := dpTempDPoint.X;
  RotRect.P2.Y := dpTempDPoint.Y;

  dpTempDPoint.X := RotRect.P3.X;
  dpTempDPoint.Y := RotRect.P3.Y;
  PInfo^.ScaleToDraw(dpTempDPoint);
  RotRect.P3.X := dpTempDPoint.X;
  RotRect.P3.Y := dpTempDPoint.Y;

  dpTempDPoint.X := RotRect.P4.X;
  dpTempDPoint.Y := RotRect.P4.Y;
  PInfo^.ScaleToDraw(dpTempDPoint);
  RotRect.P4.X := dpTempDPoint.X;
  RotRect.P4.Y := dpTempDPoint.Y;

  // Set ClipRect for current RotRect
  ProcGetClipRect(
    RotRect.P1, RotRect.P2, RotRect.P3, RotRect.P4,
    ClipRect.A, ClipRect.B);
  ScaledClipRect.InitByRect(ClipRect);

  // Set on the special flag for symbol image drawing
  bSymbolImage := True;

  DeviceCaps := GetDeviceCaps(PInfo^.ExtCanvas.Handle, Technology);
  //++ Glukhov PrintImage BUILD#123 17.07.00
  if DeviceCaps = dt_RasDisplay then
    DeviceMode := bm_Display
  else
    DeviceMode := bm_Print;

  //++ Glukhov Symbol loading BUILD#121 06.07.00
  // Compare the previous and current angles of symbol to optimize the image loading
  dbAngle := PInfo^.ViewRotation + PInfo^.Angle;
  if ((Loaded and st_Loaded) = 0) then
  begin
    LoadImage(PInfo, DeviceMode);
    Rotation := dbAngle;
  end
  else
  begin
    if (Rotation = dbAngle) then
      Reload(PInfo, DeviceMode)
    else
    begin
      Unload;
      LoadImage(PInfo, DeviceMode);
      Rotation := dbAngle;
    end;
  end;
  //-- Glukhov Symbol loading BUILD#121 06.07.00
  //-- Glukhov PrintImage BUILD#123 17.07.00

  inherited Draw(PInfo, Clipping);

  // Restore original ClipRect and RotRect
  ClipRect.InitbyRect(OldCR);
  ScaledClipRect.InitbyRect(OldSCR);

  RotRect.P1.X := P1.X;
  RotRect.P1.Y := P1.Y;
  RotRect.P2.X := P2.X;
  RotRect.P2.Y := P2.Y;
  RotRect.P3.X := P3.X;
  RotRect.P3.Y := P3.Y;
  RotRect.P4.X := P4.X;
  RotRect.P4.Y := P4.Y;
end;

{++ Glukhov HELMERT BUILD#115 23.05.00 }
// The implementation of the Helmert part was changed
{******************************************************************************}
{ ProcXXXXXXX Functions for calls from TProjXXX                                }
{******************************************************************************}

function ProcRasterTransformation
  (
  TheRasterInfo: TRasterTrafoInfo
  ): Boolean;
var
  Val1, Val2: Double;
  Val3, Val4: Double;
  YTrans: Double;
  XTrans: Double;
  Pos1, Pos2: TDPoint;
  ResCode: Integer;
begin
  Result := FALSE;
  with TheRasterInfo do
  begin // Daten aus Projekt (weil AM_Proj nicht eingebunden werden kann)
    CallCorrectSize := FALSE;
    CallCorrectSizeOverView := FALSE;
    Image := Pointer(SnapItem);
    if RTrafoType = tfLinear then
    begin
      Val1 := RTrafoInfo^[3].X - RTrafoInfo^[1].X;
      Val2 := RTrafoInfo^[3].Y - RTrafoInfo^[1].Y;
      Val3 := RTrafoInfo^[2].X - RTrafoInfo^[0].X;
      Val4 := RTrafoInfo^[2].Y - RTrafoInfo^[0].Y;
      Layers^.TopLayer^.DeSelect(PInfo, Image);
      Pos1.Init(0, 0);
      Pos2.Init(0, 0);
      YTrans := Image^.ClipRect.A.Y - RTrafoInfo^[1].Y;
      XTrans := Image^.ClipRect.A.X - RTrafoInfo^[1].X;
      Pos1.X := LimitToLong(XTrans * Val3 / Val1 + RTrafoInfo^[0].X);
      Pos1.Y := LimitToLong(YTrans * Val4 / Val2 + RTrafoInfo^[0].Y);
      YTrans := Image^.ClipRect.B.Y - RTrafoInfo^[1].Y;
      XTrans := Image^.ClipRect.B.X - RTrafoInfo^[1].X;
      Pos2.X := LimitToLong(XTrans * Val3 / Val1 + RTrafoInfo^[0].X);
      Pos2.Y := LimitToLong(YTrans * Val4 / Val2 + RTrafoInfo^[0].Y);
      if (Pos2.X < Pos1.X) or (Pos2.Y < Pos1.Y) then
        MsgBox(0, 11015, mb_Ok or mb_IconExclamation, '')
      else
      begin
        //++ Glukhov ObjAttachment 22.11.00
        //        Image^.Invalidate(PInfo);
        //-- Glukhov ObjAttachment 22.11.00
        Image^.ClipRect.Init;
        Image^.ClipRect.CorrectByPoint(Pos1);
        Image^.ClipRect.CorrectByPoint(Pos2);
        Image^.ScaledClipRect.Init;
        Image^.ScaledClipRect.CorrectByPoint(Pos1);
        Image^.ScaledClipRect.CorrectByPoint(Pos2);
        //++ Glukhov ObjAttachment 22.11.00
        //        Image^.Invalidate(PInfo);
        //-- Glukhov ObjAttachment 22.11.00
        CallCorrectSize := TRUE;
        Result := TRUE;
      end;
    end
    else
    begin // Helmert transformation
      Layers^.TopLayer^.DeSelect(PInfo, Image);
      if (Image^.Loaded and st_AllOK) = 0 then
      begin
        // Invalid bitmap
        MsgBox(0, 10223, MB_OK, '');
        Exit;
      end;
      if Image^.CheckPoints(RTrafoInfo, RTrafoCnt) = FALSE then
      begin
        // Invalid image points (YG, it seems that it is not necessary)
        MsgBox(0, 10231, MB_OK, '');
        Exit;
      end;
      PInfo^.SetTheCursor(crHourGlass);
      // Calculate the transformed parallelogram
      // for current RotRect and pairs of points (target/source)
      ProcGetHelmertTransformedRect(
        RTrafoInfo, RTrafoCnt,
        Image^.RotRect.P1, Image^.RotRect.P2,
        Image^.RotRect.P3, Image^.RotRect.P4,
        Image^.RotRect.P1, Image^.RotRect.P2,
        Image^.RotRect.P3, Image^.RotRect.P4,
        ResCode);
      // Correct its ClipRect
//++ Glukhov ObjAttachment 22.11.00
//      Image^.Invalidate(PInfo);
//-- Glukhov ObjAttachment 22.11.00
      ProcGetClipRect(
        Image^.RotRect.P1, Image^.RotRect.P2,
        Image^.RotRect.P3, Image^.RotRect.P4,
        Image^.ClipRect.A, Image^.ClipRect.B);
      // Calculate a rotation angle of BitMap
      Image^.CalcSpecTransformationData(
        Image^.RotRect.P4, Image^.RotRect.P3, Image^.RotRect.P1,
        Val1, Val2, Val3);
      //++ Glukhov Bug#236 Build#144 15.12.00
      //      Image.Rotation := ProcAdjustAngleSlightly(-Val1 - Val3, 0, 2 * Pi);
      //-- Glukhov Bug#236 Build#144 15.12.00

         // Reload image
      Image^.UnLoad;
      CallCorrectSizeOverView := TRUE;
      //++ Glukhov ObjAttachment 22.11.00
      //      Image^.Invalidate(PInfo);
      //      PInfo^.RedrawInvalidate;
      //-- Glukhov ObjAttachment 22.11.00
      Result := TRUE;
    end;
    //      StatusLine.Clear(id_Line1);
  end;
end;
{-- Glukhov HELMERT BUILD#115 23.05.00 }

procedure ProcSetColorsPalette
  (
  AProj: Pointer
  );
{var
  OldPal: HPalette;}
begin
  with PProj(AProj)^ do
  begin
    if (WhichPalette = vp_Colors) then
      Exit;
    if (g_hColorPalette <> 0) then
      InitColors256(PInfo^.ExtCanvas.Handle);
    g_hActualPalette := g_hColorPalette;
    SetPaletteForWindow(PInfo^.ExtCanvas.WindowDC);
    {    OldPal := SelectPalette(PInfo^.ExtCanvas.Handle, g_hColorPalette, FALSE);
        RealizePalette(PInfo^.ExtCanvas.Handle);
        if (OldPal <> WinGISPalette) then
          DeleteObject(OldPal);}
    ProcReloadPictures(AProj);
    WhichPalette := vp_Colors;
  end;
end;

procedure ProcSetGrayPalette
  (
  AProj: Pointer
  );
{var
  OldPal: HPalette;}
begin
  with PProj(AProj)^ do
  begin
    if (WhichPalette = vp_Gray) then
      Exit;
    if (g_hGrayPalette = 0) then
      InitColorsBlackWhite(PInfo^.ExtCanvas.Handle);
    g_hActualPalette := g_hGrayPalette;
    SetPaletteForWindow(PInfo^.ExtCanvas.WindowDC);
    {
    OldPal := SelectPalette(PInfo^.ExtCanvas.Handle, ThePalette256, FALSE);
    RealizePalette(PInfo^.ExtCanvas.Handle);
    if(OldPal <> ThePalette256) then
      DeleteObject(OldPal); }
    ProcReloadPictures(AProj);
    WhichPalette := vp_Gray;
  end;
end;

procedure ProcSetBmpPalette
  (
  AProj: Pointer
  );
var
  Image: PImage;
  {  OldPal: HPalette;}
begin
  with PProj(AProj)^ do
  begin
    Image := PImage(Layers^.IndexObject(PInfo, Layers^.SelLayer^.Data^.At(0)));
    if (Image^.GetObjType <> ot_Image) then
      Exit;
    if (Image^.GetPalette(PInfo^.ExtCanvas.WindowDC) > 0) then
    begin
      g_hActualPalette := g_hBMPalette;
      SetPaletteForWindow(PInfo^.ExtCanvas.WindowDC);
      {      OldPal := SelectPalette(PInfo^.ExtCanvas.Handle, ThePalette256, FALSE);
            RealizePalette(PInfo^.ExtCanvas.Handle);
            if (OldPal <> ThePalette256) then
              DeleteObject(OldPal);   }
      ProcReloadPictures(AProj);
      WhichPalette := vp_Bmp;
    end;
  end;
end;

//++ Glukhov Bug#236 Build#144 14.12.00

procedure ProcSearchAndDeleteBMP(Project: Pointer);
var
  LayerCnt, LayerNo: LongInt;
  CurLayer: PLayer;
  Changed: Boolean;
  PProject: PProj;

  function ForBitmap(AItem: PIndex): Boolean; far;
  var
    OImage: PImage;
  begin
    if AItem.GetObjType = ot_Image then
    begin
      OImage := PImage(CurLayer^.IndexObject(PProject^.PInfo, AItem));
      if OImage <> nil then
      begin
        Inc(PProject^.BitmapCount);
        if (OImage^.Loaded and st_Delete) <> 0 then
        begin
          CurLayer^.DeleteIndex(PProject^.PInfo, OImage);
          PLayer(PProject^.PInfo^.Objects)^.DeleteIndex(PProject^.PInfo, OImage);
          Changed := TRUE;
          PProject.SetModified;
          Dec(PProject.BitmapCount);
        end;
        if (OImage^.Loaded and st_PathChg) <> 0 then
        begin
          PProject^.SetModified;
        end;
      end
      else
      begin
        Result := False;
      end;
    end;
    Result := False;
  end;
begin
  PProject := PProj(Project);
  PProject.BitmapCount := 0;
  LayerCnt := PProject^.Layers^.LData^.Count;
  for LayerNo := 1 to LayerCnt - 1 do
  begin
    Changed := False;
    CurLayer := PProject.Layers.LData.At(LayerNo);
    CurLayer.LastObjectType(ot_Image, @ForBitmap);
    if Changed then
      PProject.Layers.LData.AtPut(LayerNo, CurLayer);
  end;
end;
//-- Glukhov Bug#236 Build#144 14.12.00

procedure ProcReloadPictures
  (
  AProj: Pointer
  );
var
  LayerCnt: Integer;
  LayerNo: Integer;
  CurLayer: PLayer;

  function ForBitmaps
      (
      AItem: PIndex
      )
      : Boolean; far;
  var
    OImage: PImage;
  begin
    with PProj(AProj)^ do
    begin
      OImage := PImage(CurLayer^.IndexObject(PInfo, AItem));
      if (OImage^.Loaded and st_Loaded) <> 0 then
      begin
        OImage^.UnLoad;
        OImage^.LoadImage(PInfo, bm_Display);
      end;
    end;
    Result := FALSE;
  end;
begin
  with PProj(AProj)^ do
  begin
    SetCursor(crHourGlass);
    LayerCnt := Layers^.LData^.Count;
    for LayerNo := 1 to LayerCnt - 1 do
    begin
      CurLayer := Layers^.LData^.At(LayerNo);
      CurLayer^.FirstObjectType(ot_Image, @ForBitmaps);
    end;
    PInfo^.RedrawScreen(TRUE);
    SetCursor(crDefault);
  end;
end;

{$IFDEF WIN32}
const
  RImage: TStreamRec = (
    ObjType: rn_Image;
    VmtLink: TypeOf(TImage);
    Load: @TImage.LoadOld;
    Store: @TImage.Store);

  RImageV31: TStreamRec = (
    ObjType: rn_ImageV31;
    VmtLink: TypeOf(TImage);
    Load: @TImage.Load;
    Store: @TImage.Store);

  RImageV32: TStreamRec = (
    ObjType: rn_ImageV32;
    VmtLink: TypeOf(TImage);
    Load: @TImage.LoadNew;
    Store: @TImage.Store);

  RImageV321: TStreamRec = (
    ObjType: rn_ImageV321;
    VmtLink: TypeOf(TImage);
    Load: @TImage.LoadV321;
    Store: @TImage.Store);

  RImageV33: TStreamRec = (
    ObjType: rn_ImageV33;
    VmtLink: TypeOf(TImage);
    Load: @TImage.LoadV33;
    Store: @TImage.Store);
  {01-010299-003 - Begin}
  RTRImage: TStreamRec = (
    ObjType: rn_TRImage;
    VmtLink: TypeOf(TImage);
    Load: @TImage.LoadV321;
    Store: @TImage.Store);

  RTRImageV2: TStreamRec = (
    ObjType: rn_TRImageV2;
    VmtLink: TypeOf(TImage);
    Load: @TImage.LoadV33;
    Store: @TImage.Store);
  {01-010299-003 - End}

  RImageV34: TStreamRec = (
    ObjType: rn_ImageV34;
    VmtLink: TypeOf(TImage);
    Load: @TImage.LoadV34;
    Store: @TImage.Store);
  RImageV4: TStreamRec = (
    ObjType: rn_ImageV4;
    VmtLink: TypeOf(TImage);
    Load: @TImage.LoadV4;
    Store: @TImage.Store);

  RSImage: TStreamRec = (
    ObjType: rn_SImage;
    VmtLink: TypeOf(TSImage);
    Load: @TSImage.LoadOld;
    Store: @TSImage.Store);

  RSImageV31: TStreamRec = (
    ObjType: rn_SImageV31;
    VmtLink: TypeOf(TSImage);
    Load: @TSImage.Load;
    Store: @TSImage.Store);

  RSImageV32: TStreamRec = (
    ObjType: rn_SImageV32;
    VmtLink: TypeOf(TSImage);
    Load: @TSImage.LoadNew;
    Store: @TSImage.Store);

  RSImageV321: TStreamRec = (
    ObjType: rn_SImageV321;
    VmtLink: TypeOf(TSImage);
    Load: @TSImage.LoadV321;
    Store: @TSImage.Store);

  RSImageV33: TStreamRec = (
    ObjType: rn_SImageV33;
    VmtLink: TypeOf(TSImage);
    Load: @TSImage.LoadV33;
    Store: @TSImage.Store);

  RSImageV34: TStreamRec = (
    ObjType: rn_SImageV34;
    VmtLink: TypeOf(TSImage);
    Load: @TSImage.LoadV34;
    Store: @TSImage.Store);

  RSImageV4: TStreamRec = (
    ObjType: rn_SImageV4;
    VmtLink: TypeOf(TSImage);
    Load: @TSImage.Load;
    Store: @TSImage.Store);

{$ELSE}
const
  RImage: TStreamRec = (
    ObjType: rn_Image;
    VmtLink: Ofs(TypeOf(TImage)^);
    Load: @TImage.LoadOld;
    Store: @TImage.Store);

  RImageV31: TStreamRec = (
    ObjType: rn_ImageV31;
    VmtLink: Ofs(TypeOf(TImage)^);
    Load: @TImage.Load;
    Store: @TImage.Store);

  RImageV32: TStreamRec = (
    ObjType: rn_ImageV32;
    VmtLink: Ofs(TypeOf(TImage)^);
    Load: @TImage.LoadNew;
    Store: @TImage.Store);

  RImageV321: TStreamRec = (
    ObjType: rn_ImageV321;
    VmtLink: Ofs(TypeOf(TImage)^);
    Load: @TImage.LoadV321;
    Store: @TImage.Store);

  RImageV33: TStreamRec = (
    ObjType: rn_ImageV33;
    VmtLink: Ofs(TypeOf(TImage)^);
    Load: @TImage.LoadV33;
    Store: @TImage.Store);

  {01-010299-003 - Begin}
  RTRImage: TStreamRec = (
    ObjType: rn_TRImage;
    VmtLink: Ofs(TypeOf(TImage)^);
    Load: @TImage.LoadV321;
    Store: @TImage.Store);

  RTRImageV2: TStreamRec = (
    ObjType: rn_TRImageV2;
    VmtLink: Ofs(TypeOf(TImage)^);
    Load: @TImage.LoadV33;
    Store: @TImage.Store);
  {01-010299-003 - End}

  RImageV34: TStreamRec = (
    ObjType: rn_ImageV34;
    VmtLink: Ofs(TypeOf(TImage)^);
    Load: @TImage.LoadV34;
    Store: @TImage.Store);
  RImageV4: TStreamRec = (
    ObjType: rn_ImageV4;
    VmtLink: Ofs(TypeOf(TImage)^);
    Load: @TImage.LoadV4;
    Store: @TImage.Store);

  { Symbolimage Streamlinks }
  RSImage: TStreamRec = (
    ObjType: rn_SImage;
    VmtLink: Ofs(TypeOf(TSImage)^);
    Load: @TSImage.LoadOld;
    Store: @TSImage.Store);

  RSImageV31: TStreamRec = (
    ObjType: rn_SImageV31;
    VmtLink: Ofs(TypeOf(TSImage)^);
    Load: @TSImage.Load;
    Store: @TSImage.Store);

  RSImageV32: TStreamRec = (
    ObjType: rn_SImageV32;
    VmtLink: Ofs(TypeOf(TSImage)^);
    Load: @TSImage.LoadNew;
    Store: @TSImage.Store);

  RSImageV321: TStreamRec = (
    ObjType: rn_SImageV321;
    VmtLink: Ofs(TypeOf(TSImage)^);
    Load: @TSImage.LoadV321;
    Store: @TSImage.Store);

  RSImageV33: TStreamRec = (
    ObjType: rn_SImageV33;
    VmtLink: Ofs(TypeOf(TSImage)^);
    Load: @TSImage.LoadV33;
    Store: @TSImage.Store);

  RSImageV34: TStreamRec = (
    ObjType: rn_SImageV34;
    VmtLink: Ofs(TypeOf(TSImage)^);
    Load: @TSImage.LoadV34;
    Store: @TSImage.Store);

  RSImageV4: TStreamRec = (
    ObjType: rn_SImageV4;
    VmtLink: Ofs(TypeOf(TSImage)^);
    Load: @TSImage.LoadV4;
    Store: @TSImage.Store);

{$ENDIF}

procedure TImage.MakeRotFromClip;
begin
  with RotRect do
  begin
    P1.X := ClipRect.A.X;
    P1.Y := ClipRect.B.Y;
    P2.X := ClipRect.B.X;
    P2.Y := ClipRect.B.Y;
    P3.X := ClipRect.B.X;
    P3.Y := ClipRect.A.Y;
    P4.X := ClipRect.A.X;
    P4.Y := ClipRect.A.Y;
  end;
end;

function TImage.GetCenter(PInfo: PPaint): TDPoint;
var
  ACPoly: TCPoly;
begin
  Result.Init(Trunc(ClipRect.CenterX), Trunc(ClipRect.CenterY));
  ACPoly.Init;
  try
    GetBorder(PInfo, @ACPoly);
    Result := ACPoly.GetCenter;
  finally
    ACPoly.Done;
  end;
end;

procedure TImage.UpdateBorder(PInfo: PPaint);
begin
  if Border <> nil then
  begin
    GetBorder(PInfo, Border);
  end;
end;

{
  This procedure should be called if  AbsolutePath name is not valid.
  It checks file name along:
  1. Relative path name according to project path (if exists)
  2. PictureDir from INI file

  Returns: True  - if file is found
           False - if wasn't found
}

function TImage.CheckAlternativeNames: Boolean;
var
  AltPath, TrueFileName: string[255];
  ProjDir: string[255];

  function CheckFileAsImageOne(Path: AnsiString): Boolean;
  begin
    Result := False;
    if FileExists(Path) then
      Result := FileIsImageOne(AltPath);
  end;

  {
    Note: AltPath can be here in 2 possible form:
    1 - absolute path with Drive:\Dir[s]\FIleName.ext
    2 - with some relative inclusions, for example:
        Drive:\Dir1\Dir2\DirN\..\..\FIleName.ext
    AltPath shoul containb existing image file name.
  }

  procedure UpdateFileName;
  var
    ActDir, TrueDir: string;
  begin
    //    TrueFileName := ExtractFileName(AltPath);
        { Convert possible relative path to a direct one }
    ActDir := GetCurrentDir; { Save original directory }
    if not SetCurrentDir(ExtractFilePath(AltPath)) then { Something is wrong! }
      Exit;
    TrueDir := GetCurrentDir; { Real directory for a file }
    if TrueDir[Length(TrueDir)] <> '\' then
      TrueDir := TrueDir + '\';
    { Conversion is complete, store true direct path to file }

    { Update absolute path }
    AltPath := TrueDir + TrueFileName;
    DisposeStr(AbsolutePath);
    AbsolutePath := NewStr(AltPath);
    DisposeStr(FileName);
    FileName := NewStr(AltPath);
    { Update relative path }
    { First try to find is it possible to calculate relative path or not? }
    TrueDir := ExtractRelativePath(ProjDir, TrueDir);
    AltPath := TrueDir + TrueFileName;
    DisposeStr(RelativePath);
    RelativePath := NewStr(AltPath);
    SetCurrentDir(ActDir); { Return to the original directory }
  end;

begin
  Result := FileExists(FileName^); { May be all is already right? }
  if Result then { Yess! Bye-bye }
    Exit;
  TrueFileName := ExtractFileName(FileName^);
  { Try to regenerate from relative path of project }
{$IFNDEF AXDLL} // <----------------- AXDLL
  ProjDir := ExtractFilePath(WinGISMainForm.FutureName);
{$ENDIF} // <----------------- AXDLL
  if RelativePath = nil then
    RelativePath := NewStr('');
  AltPath := ProjDir + RelativePath^;
  Result := FileExists(AltPath);

  if not Result then { not found }
  begin { Try to find it in "PictureDir"}
{$IFNDEF AXDLL} // <--------- AXDLL
    AltPath := IniFile.PictureDir + TrueFileName;
    if ExtractFileDrive(AltPath) = EmptyStr then { Try to add drive name}
      AltPath := IniFile.PictureDrive + AltPath;
{$ELSE} // <--------- AXDLL
    Result := FileExists(AltPath);
{$ENDIF} // <--------- AXDLL
  end;
  if Result then
    UpdateFileName;
end;
//-----------------------------------------------------------------------------

{==============================================================================
 Input:
   Clip =  intersecting rectangle/frame
 Output:
   InRect = intersected rectangle in project units
   InImgRect = intersected rectangel in image coordinate system
 ==============================================================================}

function TImage.GetIRects(var CutRect, InRect: TDRect; var InImgRect: TRect): Boolean;
var
  XCoeff, YCoeff: Double;
begin
  Result := False;
  ClipRect.Intersect(CutRect, InRect); // Put intersection to InRect
  if InRect.IsEmpty then
    Exit;
  XCoeff := ClipRect.XSize / iInitialW; // XSize of pixel in proj. coordinates
  YCoeff := ClipRect.YSize / iInitialH; // YSize of pixel in proj. coordinates
  with InImgRect do
  begin
    Left := Round(InRect.A.XDist(ClipRect.A) / XCoeff);
    Top := Round(ClipRect.B.YDist(InRect.B) / YCoeff);
    Right := Min(Round(InRect.B.XDist(ClipRect.A) / XCoeff), Pred(iInitialW));
    Bottom := Min(Round(ClipRect.B.YDist(InRect.A) / YCoeff), Pred(iInitialH));
  end;
  Result := True;
end;

function TImage.Cutting(NewFileName: AnsiString; CutRect: TDRect; NewClipRect: PRect = nil): Boolean;
var
  InRect: TDRect;
  SrcRect, NewRect: TRect;
  XCoeff, YCoeff: Double;
  A: array of TRefPoint;
begin
  Result := False;
  try
    if ClipRect.IsInside(CutRect) then // Our image is in cut rectangle, save it totally
    begin
      Result := SaveImageAs(AnsiString(AbsolutePath), NewFileName);
      Exit;
    end;
{$IFDEF OLD}
    ClipRect.Intersect(CutRect, InRect); // Put intersection to InRect
    if InRect.IsEmpty then
      Exit;
    XCoeff := ClipRect.XSize / iInitialW; // XSize of pixel in proj. coordinates
    YCoeff := ClipRect.YSize / iInitialH; // YSize of pixel in proj. coordinates
    with SrcRect do
    begin
      Left := Round(InRect.A.XDist(ClipRect.A) / XCoeff);
      Top := Round(ClipRect.B.YDist(InRect.B) / YCoeff);
      Right := Min(Round(InRect.B.XDist(ClipRect.A) / XCoeff), iInitialW - 1);
      Bottom := Min(Round(ClipRect.B.YDist(InRect.A) / YCoeff), iInitialH - 1);
    end;
{$ELSE}
    if not GetIRects(CutRect, InRect, SrcRect) then
      Exit;
{$ENDIF}
    Result := SavePartialImageAs(FileName^, NewFileName, SrcRect);
  finally
    if Result then
    begin
      with InRect do
        NewRect := Rect(A.X, A.Y, B.X, B.Y);
      if Assigned(NewClipRect) then
        NewClipRect^ := NewRect;
      SetLength(A, 0);
      CreateTABFile(NewFileName, PRectWidth(@SrcRect), PRectHeight(@SrcRect), @NewRect, A);
    end;
  end;
end;

// This procedure creates new file which will replace all the old data!

function TImage.UpdateTAB(Txt: AnsiString): Boolean;
var
  Dummy: TRefArray;
  IRect: TRect;
begin
  IRect := Rect(ClipRect.A.X, ClipRect.B.Y, ClipRect.B.X, ClipRect.A.Y);
  Dummy := nil;
  Result := CreateTABFile(FileName^, iInitialW, iInitialH, @IRect, Dummy, TabExtension, Txt);
end;

function TImage.IsVMSITech: Boolean;
begin
  //  Result := (Info.IsVMSI) or (Info.IsScaleSelf and info.IsVMSIPlus);
  Result := Info.IsVMSI;
  { // Uncomment this block if you need *.PRE also be un-cuttable in any case!
    // If block is commented, only *.PRI are UNCUTTABLE.
    if Result then        // Yes, it is ancient VMSI tech file
      Exit;
    // NO, so continue to check it.
    // first check its selfscale ability
    Result := Info.IsScaleSelf;
    if not Result then // NO, it can't be modern VMSI+ file!
      Exit;
    Result := Info.IsVMSIPlus;
  }
end;

//WAI

function TImage.GivePosX: LongInt;
begin
  GivePosX := cliprect.a.x;
end;

function TImage.GivePosY: LongInt;
begin
  GivePosY := cliprect.a.y;
end;

function TImage.GivePosX1: LongInt;
begin
  GivePosX1 := cliprect.b.x;
end;

function TImage.GivePosY1: LongInt;
begin
  GivePosY1 := cliprect.b.y;
end;

function TImage.GivePerimeter: LongInt;
begin
  GivePerimeter := round(cliprect.XSize + cliprect.Ysize);
end;

function TImage.GiveArea: Double;
var
  a, b: double;
begin
  a := Abs(cliprect.xsize);
  b := Abs(cliprect.ysize);
  GiveArea := a * b;
end;

begin
  {****** Images ... *********}

  {01-010299-003 - Begin}
  RegisterType(RTRImage);
  RegisterType(RTRImageV2);
  {01-010299-003 - End}

  RegisterType(RImage);
  RegisterType(RImageV31);
  RegisterType(RImageV32);
  RegisterType(RImageV321);
  RegisterType(RImageV33);
  RegisterType(RImageV34);
  RegisterType(RImageV4);

  {****** Symbols ...  ********}
  RegisterType(RSImage);
  RegisterType(RSImageV31);
  RegisterType(RSImageV32);
  RegisterType(RSImageV321);
  RegisterType(RSImageV33);
  RegisterType(RSImageV34);
  RegisterType(RSImageV4);
end.

