unit ProjRep;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  AM_Proj, AM_Layer, AM_Index, AM_Obj, AM_Group, MultiLng, StdCtrls, ExtCtrls, ComCtrls;

type
  TProjRepForm = class(TForm)
    MlgSection: TMlgSection;
    Bevel1: TBevel;
    ButtonClose: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    LabelInvalidObj: TLabel;
    LabelUnrefSym: TLabel;
    LabelZeroAreas: TLabel;
    LabelZeroLines: TLabel;
    ButtonRepair: TButton;
    Bevel2: TBevel;
    ProgressBar: TProgressBar;
    Label3: TLabel;
    LabelInvalidSUC: TLabel;
    Label6: TLabel;
    LabelTextErrors: TLabel;
    CheckBoxRestoreObjects: TCheckBox;
    CheckBoxRestoreSymbols: TCheckBox;
    Bevel3: TBevel;
    Label7: TLabel;
    LabelRepairedLayerItems: TLabel;
    procedure ButtonRepairClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    FInvalidObj: TList;
    FUnrefSym: TList;
    FInvalidSUC: TList;
    FZeroAreas: TList;
    FZeroLines: TList;
    FTextErrors: TList;
    procedure RepairEnabled;
    procedure RepairLayers;
    procedure GetInvalidObj;
    procedure RestoreInvalidObj;
    procedure GetUnrefSym;
    procedure RestoreUnrefSym;
    procedure GetInvalidSUC;
    procedure UpdateSUC;
    procedure GetZeroAreas;
    procedure DeleteZeroAreas;
    procedure GetZeroLines;
    procedure DeleteZeroLines;
    procedure GetTextErrors;
    procedure RepairTexts;
    procedure DeleteObject(ALayer: PLayer; AIndex: Integer);
  public
    Project: PProj;
  end;

var
  ProjRepForm: TProjRepForm;

procedure ProcExecProjRep(AProj: PProj);

implementation

uses AM_Sym, AM_Poly, AM_CPoly, AM_View, AM_Def, StyleDef, AM_Text, AM_Font,
     SymbolSelDlg, AM_Admin;

const

  pbSteps = 100;

{$R *.DFM}

procedure ProcExecProjRep(AProj: PProj);
begin
     ProjRepForm:=TProjRepForm.Create(Application);
     ProjRepForm.Project:=AProj;
     ProjRepForm.ShowModal;
     ProjRepForm.Free;
end;

{ TProjRepForm }

procedure TProjRepForm.FormCreate(Sender: TObject);
begin
    FInvalidObj:=TList.Create;
    FUnrefSym:=TList.Create;
    FInvalidSUC:=TList.Create;
    FZeroAreas:=TList.Create;
    FZeroLines:=TList.Create;
    FTextErrors:=TList.Create;
end;

procedure TProjRepForm.FormActivate(Sender: TObject);
begin
     Application.ProcessMessages;
     Screen.Cursor:=crHourGlass;
     try
        RepairLayers;
        GetInvalidObj;
        GetUnrefSym;
        GetInvalidSUC;
        GetZeroAreas;
        GetZeroLines;
        GetTextErrors;
     finally
        Screen.Cursor:=crDefault;
        RepairEnabled;
     end;
end;

procedure TProjRepForm.ButtonRepairClick(Sender: TObject);
begin
     ButtonRepair.Enabled:=False;
     Screen.Cursor:=crHourGlass;
     Application.ProcessMessages;
     try
        if FInvalidObj.Count > 0 then begin
           RestoreInvalidObj;
           GetInvalidObj;
           GetUnrefSym;
        end;
        RestoreUnrefSym;
        GetUnrefSym;
        GetInvalidSUC;
        UpdateSUC;
        GetInvalidSUC;
        GetZeroAreas;
        DeleteZeroAreas;
        GetZeroAreas;
        GetZeroLines;
        DeleteZeroLines;
        GetZeroLines;
        GetTextErrors;
        RepairTexts;
        GetTextErrors;
     finally
        Screen.Cursor:=crDefault;
        RepairEnabled;
     end;
end;

procedure TProjRepForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     FInvalidObj.Free;
     FUnrefSym.Free;
     FInvalidSUC.Free;
     FZeroAreas.Free;
     FZeroLines.Free;
     FTextErrors.Free;
end;

{------------------------------------------------------------------------------}

procedure TProjRepForm.RepairEnabled;
var
Cnt: Integer;
begin
     Cnt:=FInvalidObj.Count + FUnrefSym.Count + FInvalidSUC.Count +
          FZeroAreas.Count + FZeroLines.Count + FTextErrors.Count;
     ButtonRepair.Enabled:=Cnt > 0;
end;

{------------------------------------------------------------------------------}

procedure TProjRepForm.GetInvalidObj;
var
Cnt: Integer;

     procedure DoAll(AItem: PIndex);
     var
     i: Integer;
     ALayer: PLayer;
     Found: Boolean;
     begin
          Inc(Cnt);
          if (Cnt mod ProgressBar.Step) = 0 then begin
             ProgressBar.StepIt;
          end;
          if AItem <> nil then begin
             Found:=False;
             if AItem^.GetObjType in ot_Any then begin
                for i:=1 to Project^.Layers^.LData^.Count-1 do begin
                    ALayer:=Project^.Layers^.LData^.At(i);
                    if ALayer <> nil then begin
                       if ALayer^.HasObject(AItem) <> nil then begin
                          Found:=True;
                          break;
                       end;
                    end;
                end;
             end;
             if not Found then begin
                FInvalidObj.Add(Pointer(AItem^.Index));
             end;
          end;
     end;

begin
     FInvalidObj.Clear;

     ProgressBar.Position:=0;
     if Project <> nil then begin
        ProgressBar.Max:=PLayer(Project^.PInfo^.Objects)^.Data^.GetCount;
        ProgressBar.Step:=Max(ProgressBar.Max div pbSteps,1);
        Cnt:=0;
        PLayer(Project^.PInfo^.Objects)^.Data^.ForEach(@DoAll);
     end;

     if FInvalidObj.Count > 0 then begin
        LabelInvalidObj.Font.Color:=clRed;
     end
     else begin
        LabelInvalidObj.Font.Color:=clBlack;
     end;
     LabelInvalidObj.Caption:=IntToStr(FInvalidObj.Count);
     ProgressBar.Position:=0;
     Application.ProcessMessages;
end;

{------------------------------------------------------------------------------}

procedure TProjRepForm.RepairLayers;
var
Cnt,i: Integer;
ALayer: PLayer;
InvalidIndexCount: Integer;

     procedure DoAll(AItem: PIndex);
     begin
          Inc(Cnt);
          if (Cnt mod ProgressBar.Step) = 0 then begin
             ProgressBar.StepIt;
          end;
         try
          if AItem <> nil then begin
             if PLayer(Project^.PInfo^.Objects)^.IndexObject(Project^.PInfo,AItem) = nil then begin
                DeleteObject(ALayer,AItem^.Index);
                Inc(InvalidIndexCount);
                Project^.SetModified;
             end;
             if AItem^.Index = 0 then begin
                DeleteObject(PLayer(Project^.PInfo^.Objects),AItem^.Index);
                DeleteObject(ALayer,AItem^.Index);
                Inc(InvalidIndexCount);
                Project^.SetModified;
             end;
          end;
         except
         end;
     end;

begin

     InvalidIndexCount:=0;
     for i:=1 to Project^.Layers^.LData^.Count-1 do begin
         ProgressBar.Position:=0;
         if Project <> nil then begin
            ALayer:=Project^.Layers^.LData^.At(i);
            if ALayer <> nil then begin
               ProgressBar.Max:=ALayer^.Data^.GetCount;
               ProgressBar.Step:=Max(ProgressBar.Max div pbSteps,1);
               Cnt:=0;
               ALayer^.Data^.ForEach(@DoAll);
            end;
         end;
     end;

     LabelRepairedLayerItems.Caption:=IntToStr(InvalidIndexCount);
     ProgressBar.Position:=0;
     Application.ProcessMessages;
end;

{------------------------------------------------------------------------------}

procedure TProjRepForm.RestoreInvalidObj;
var
i: Integer;
DLayer: PLayer;
NIndex: PIndex;
begin
     DLayer:=nil;
     ProgressBar.Position:=0;
     if CheckBoxRestoreObjects.Checked then begin
        DLayer:=Project^.Layers^.InsertLayer('Restored Objects',clBlack,lt_Solid,clRed,pt_DiagCross,ilTop,DefaultSymbolFill);
     end;
     if (Project <> nil) then begin
        ProgressBar.Max:=FInvalidObj.Count;
        ProgressBar.Step:=Max(ProgressBar.Max div pbSteps,1);
        for i:=0 to FInvalidObj.Count-1 do begin
          try
            if (i mod ProgressBar.Step) = 0 then begin
               ProgressBar.StepIt;
            end;
            if DLayer <> nil then begin
                  NIndex:=New(PIndex,Init(Integer(FInvalidObj.Items[i])));
                  if NIndex <> nil then begin
                     try
                        if DLayer^.InsertObject(Project^.PInfo,NIndex,False) then begin
                           Project^.SetModified;
                        end
                        else begin
                           Dispose(NIndex,Done);
                        end;
                     except
                        Dispose(NIndex,Done);
                     end;
                  end;
            end
            else begin
               DeleteObject(PLayer(Project^.PInfo^.Objects),Integer(FInvalidObj.Items[i]));
            end;
          except
          end;
        end;
     end;
     ProgressBar.Position:=0;
end;

{------------------------------------------------------------------------------}

procedure TProjRepForm.GetUnrefSym;
var
Cnt: Integer;

     procedure DoAll(AItem: PView);
     var
     ASym: PSymbol;
     begin
          Inc(Cnt);
          if (Cnt mod ProgressBar.Step) = 0 then begin
             ProgressBar.StepIt;
          end;
         try
          if (AItem <> nil) and (AItem^.GetObjType = ot_Symbol) then begin
             ASym:=PSymbol(AItem);
             if ASym^.SymPtr = nil then begin
                ASym^.SetSymPtr(Project^.PInfo^.Symbols);
                if ASym^.SymPtr = nil then begin
                   FUnrefSym.Add(Pointer(AItem^.Index));
                end
                else begin
                   ASym^.CalculateClipRect(Project^.PInfo);
                end;
             end
             else begin
                try
                   if ASym^.SymPtr^.GetObjType <> ot_Group then begin
                      FUnrefSym.Add(Pointer(AItem^.Index));
                   end;
                except
                   FUnrefSym.Add(Pointer(AItem^.Index));
                end;
             end;
          end;
         except
         end;
     end;

begin
     FUnrefSym.Clear;
     ProgressBar.Position:=0;
     if Project <> nil then begin
        ProgressBar.Max:=PLayer(Project^.PInfo^.Objects)^.Data^.GetCount;
        ProgressBar.Step:=Max(ProgressBar.Max div pbSteps,1);
        Cnt:=0;
        PLayer(Project^.PInfo^.Objects)^.Data^.ForEach(@DoAll);
     end;
     if FUnrefSym.Count > 0 then begin
        LabelUnrefSym.Font.Color:=clRed;
     end
     else begin
        LabelUnrefSym.Font.Color:=clBlack;
     end;
     LabelUnrefSym.Caption:=IntToStr(FUnrefSym.Count);
     ProgressBar.Position:=0;
     Application.ProcessMessages;
end;

{------------------------------------------------------------------------------}

procedure TProjRepForm.RestoreUnrefSym;
var
i,k: Integer;
ASym: PSymbol;
ASGroup: PSGroup;
AItem: PIndex;
ObjIdx: Integer;
ALayer: PLayer;
begin
     ProgressBar.Position:=0;
     ASGroup:=nil;
     if Project <> nil then begin
        ProgressBar.Max:=FUnrefSym.Count;
        ProgressBar.Step:=Max(ProgressBar.Max div pbSteps,1);
        for i:=0 to FUnrefSym.Count-1 do begin
          try
            if (i mod ProgressBar.Step) = 0 then begin
               ProgressBar.StepIt;
            end;
            if CheckBoxRestoreSymbols.Checked then begin
               if (ASGroup = nil) then begin
                  ASGroup:=GetSymbol(Self,Project);
                  Application.ProcessMessages;
                  if ASGroup = nil then begin
                     break;
                  end;
               end;
               AItem:=New(PIndex,Init(Integer(FUnrefSym.Items[i])));
               if AItem <> nil then begin
                  try
                     if PLayer(Project^.PInfo^.Objects)^.Data^.Search(AItem,ObjIdx) then begin
                        ASym:=PSymbol(PLayer(Project^.PInfo^.Objects)^.Data^.At(ObjIdx));
                        if (ASym <> nil) and (ASym^.GetObjType = ot_Symbol) then begin
                           ASym^.SymIndex:=ASGroup^.Index;
                           ASym^.SetSymPtrByIndex(Project^.PInfo^.Symbols,ASGroup^.Index);
                           Inc(ASGroup^.UseCount);
                           Project^.SetModified;
                        end;
                     end;
                  finally
                     Dispose(AItem,Done);
                  end;
               end;
            end
            else begin
               for k:=1 to Project^.Layers^.LData^.Count-1 do begin
                   ALayer:=Project^.Layers^.LData^.At(k);
                   DeleteObject(ALayer,Integer(FUnrefSym.Items[i]));
               end;
               DeleteObject(PLayer(Project^.PInfo^.Objects),Integer(FUnrefSym.Items[i]));
            end;
          except
          end;
        end;
        Project^.RefreshSymbolMan(False);
     end;
     ProgressBar.Position:=0;
end;

{------------------------------------------------------------------------------}

procedure TProjRepForm.GetInvalidSUC;
var
Cnt: Integer;

     procedure DoAllGroups(ASGroup: PSGroup);
     var
     NewSUC: Integer;

          procedure DoAllObjects(AItem: PView);
          var
          ASym: PSymbol;
          begin
             if (AItem <> nil) and (AItem^.GetObjType = ot_Symbol) then begin
                ASym:=PSymbol(AItem);
                if (ASym^.SymPtr <> nil) and (ASym^.SymIndex = ASGroup^.Index) then begin
                   Inc(NewSUC);
                end;
             end;
          end;

     begin
          Inc(Cnt);
          if (Cnt mod ProgressBar.Step) = 0 then begin
             ProgressBar.StepIt;
          end;
         try
          if (ASGroup <> nil) then begin
             NewSUC:=0;
             PLayer(Project^.PInfo^.Objects)^.Data^.ForEach(@DoAllObjects);
             if ASGroup^.UseCount <> NewSUC then begin
                FInvalidSUC.Add(ASGroup);
             end;
          end;
         except
         end;
     end;

begin
     FInvalidSUC.Clear;
     ProgressBar.Position:=0;
     if Project <> nil then begin
        ProgressBar.Max:=PSymbols(Project^.PInfo^.Symbols)^.Data^.GetCount;
        ProgressBar.Step:=Max(ProgressBar.Max div pbSteps,1);
        Cnt:=0;
        PLayer(Project^.PInfo^.Symbols)^.Data^.ForEach(@DoAllGroups);
     end;
     if FInvalidSUC.Count > 0 then begin
        LabelInvalidSUC.Font.Color:=clRed;
     end
     else begin
        LabelInvalidSUC.Font.Color:=clBlack;
     end;
     LabelInvalidSUC.Caption:=IntToStr(FInvalidSUC.Count);
     ProgressBar.Position:=0;
     Application.ProcessMessages;
end;

{------------------------------------------------------------------------------}

procedure TProjRepForm.UpdateSUC;
var
i: Integer;
ASGroup: PSGroup;

     procedure DoAll(AItem: PView);
     var
     ASym: PSymbol;
     begin
          if (AItem <> nil) and (AItem^.GetObjType = ot_Symbol) then begin
             ASym:=PSymbol(AItem);
             if (ASym^.SymPtr <> nil) and (ASym^.SymIndex = ASGroup^.Index) then begin
                Inc(ASGroup^.UseCount);
             end;
          end;
     end;

begin
     ProgressBar.Position:=0;
     if Project <> nil then begin
        ProgressBar.Max:=FInvalidSUC.Count;
        ProgressBar.Step:=Max(ProgressBar.Max div pbSteps,1);
        for i:=0 to FInvalidSUC.Count-1 do begin
            if (i mod ProgressBar.Step) = 0 then begin
               ProgressBar.StepIt;
            end;
           try
            ASGroup:=PSGroup(FInvalidSUC.Items[i]);
            if ASGroup <> nil then begin
               ASGroup^.UseCount:=0;
               PLayer(Project^.PInfo^.Objects)^.Data.ForEach(@DoAll);
            end;
           except
           end;
        end;
        Project^.RefreshSymbolMan(False);
     end;
     ProgressBar.Position:=0;
end;

{------------------------------------------------------------------------------}

procedure TProjRepForm.GetZeroAreas;
var
Cnt,i: Integer;
ALayer: PLayer;

     procedure DoAll(AItem: PView);
     var
     ACPoly: PCPoly;
     begin
          Inc(Cnt);
          if (Cnt mod ProgressBar.Step) = 0 then begin
             ProgressBar.StepIt;
          end;
         try
          if (AItem <> nil) and (AItem^.GetObjType = ot_CPoly) then begin
             ACPoly:=PCPoly(AItem);
             ACPoly^.CalculateClipRect;
             if ACPoly^.Flaeche = 0 then begin
                FZeroAreas.Add(Pointer(AItem^.Index));
             end;
          end;
         except
         end;
     end;
begin
     FZeroAreas.Clear;
     ProgressBar.Position:=0;
     if Project <> nil then begin
        ProgressBar.Max:=PLayer(Project^.PInfo^.Objects)^.Data^.GetCount;
        ProgressBar.Step:=Max(ProgressBar.Max div pbSteps,1);
        Cnt:=0;
        PLayer(Project^.PInfo^.Objects)^.Data^.ForEach(@DoAll);
     end;
     if FZeroAreas.Count > 0 then begin
        LabelZeroAreas.Font.Color:=clRed;
     end
     else begin
        LabelZeroAreas.Font.Color:=clBlack;
     end;
     LabelZeroAreas.Caption:=IntToStr(FZeroAreas.Count);
     ProgressBar.Position:=0;
     Application.ProcessMessages;
end;

{------------------------------------------------------------------------------}

procedure TProjRepForm.DeleteZeroAreas;
var
i,k: Integer;
ALayer: PLayer;
begin
     ProgressBar.Position:=0;
     if Project <> nil then begin
        ProgressBar.Max:=FZeroAreas.Count;
        ProgressBar.Step:=Max(ProgressBar.Max div pbSteps,1);
        for i:=0 to FZeroAreas.Count-1 do begin
            if (i mod ProgressBar.Step) = 0 then begin
               ProgressBar.StepIt;
            end;
          try
            for k:=1 to Project^.Layers^.LData^.Count-1 do begin
                ALayer:=Project^.Layers^.LData^.At(k);
                DeleteObject(ALayer,Integer(FZeroAreas.Items[i]));
            end;
            DeleteObject(PLayer(Project^.PInfo^.Objects),Integer(FZeroAreas.Items[i]));
          except
          end;
        end;
     end;
     ProgressBar.Position:=0;
end;

{------------------------------------------------------------------------------}

procedure TProjRepForm.GetZeroLines;
var
Cnt: Integer;

     procedure DoAll(AItem: PView);
     var
     APoly: PPoly;
     begin
          Inc(Cnt);
          if (Cnt mod ProgressBar.Step) = 0 then begin
             ProgressBar.StepIt;
          end;
         try
          if (AItem <> nil) and (AItem^.GetObjType = ot_Poly) then begin
             APoly:=PPoly(AItem);
             APoly^.CalculateClipRect;
             if APoly^.Laenge = 0 then begin
                FZeroLines.Add(Pointer(AItem^.Index));
             end;
          end;
         except
         end;
     end;
begin
     FZeroLines.Clear;
     ProgressBar.Position:=0;
     if Project <> nil then begin
        ProgressBar.Max:=PLayer(Project^.PInfo^.Objects)^.Data^.GetCount;
        ProgressBar.Step:=Max(ProgressBar.Max div pbSteps,1);
        Cnt:=0;
        PLayer(Project^.PInfo^.Objects)^.Data^.ForEach(@DoAll);
     end;
     if FZeroLines.Count > 0 then begin
        LabelZeroLines.Font.Color:=clRed;
     end
     else begin
        LabelZeroLines.Font.Color:=clBlack;
     end;
     LabelZeroLines.Caption:=IntToStr(FZeroLines.Count);
     ProgressBar.Position:=0;
     Application.ProcessMessages;
end;

{------------------------------------------------------------------------------}

procedure TProjRepForm.DeleteZeroLines;
var
i,k: Integer;
ALayer: PLayer;
begin
     ProgressBar.Position:=0;
     if Project <> nil then begin
        ProgressBar.Max:=FZeroLines.Count;
        ProgressBar.Step:=Max(ProgressBar.Max div pbSteps,1);
        for i:=0 to FZeroLines.Count-1 do begin
            if (i mod ProgressBar.Step) = 0 then begin
               ProgressBar.StepIt;
            end;
          try
            for k:=1 to Project^.Layers^.LData^.Count-1 do begin
                ALayer:=Project^.Layers^.LData^.At(k);
                DeleteObject(ALayer,Integer(FZeroLines.Items[i]));
            end;
            DeleteObject(PLayer(Project^.PInfo^.Objects),Integer(FZeroLines.Items[i]));
          except
          end;
        end;
     end;
     ProgressBar.Position:=0;
end;

{------------------------------------------------------------------------------}

procedure TProjRepForm.GetTextErrors;
var
Cnt: Integer;

     procedure DoAll(AItem: PView);
     var
     AText: PText;
     begin
          Inc(Cnt);
          if (Cnt mod ProgressBar.Step) = 0 then begin
             ProgressBar.StepIt;
          end;
         try
          if (AItem <> nil) and (AItem^.GetObjType = ot_Text) then begin
             AText:=PText(AItem);
             if (AText^.Height < cMinFontHeight) or (AText^.Font.Font <= 0) or (AText^.Text = nil) or (AText^.Text^ = '') then begin
                FTextErrors.Add(AItem);
             end;
          end;
         except
         end;
     end;

begin
     FTextErrors.Clear;
     ProgressBar.Position:=0;
     if Project <> nil then begin
        ProgressBar.Max:=PLayer(Project^.PInfo^.Objects)^.Data^.GetCount;
        ProgressBar.Step:=Max(ProgressBar.Max div pbSteps,1);
        Cnt:=0;
        PLayer(Project^.PInfo^.Objects)^.Data^.ForEach(@DoAll);
     end;
     if FTextErrors.Count > 0 then begin
        LabelTextErrors.Font.Color:=clRed;
     end
     else begin
        LabelTextErrors.Font.Color:=clBlack;
     end;
     LabelTextErrors.Caption:=IntToStr(FTextErrors.Count);
     ProgressBar.Position:=0;
     Application.ProcessMessages;
end;

{------------------------------------------------------------------------------}

procedure TProjRepForm.RepairTexts;
var
i: Integer;
AText: PText;
begin
     ProgressBar.Position:=0;
     if Project <> nil then begin
        ProgressBar.Max:=FTextErrors.Count;
        ProgressBar.Step:=Max(ProgressBar.Max div pbSteps,1);
        for i:=0 to FTextErrors.Count-1 do begin
            if (i mod ProgressBar.Step) = 0 then begin
               ProgressBar.StepIt;
            end;
          try
            AText:=PText(FTextErrors[i]);
            if AText <> nil then begin
               if AText^.Height < cMinFontHeight then begin
                  AText^.Height:=cMinFontHeight;
               end;
               if AText^.Font.Font <= 0 then begin
                  AText^.Font.Font:=1;
               end;
               if ((AText^.Text = nil) or (AText^.Text^ = '')) then begin
                  AText^.SetNewText(Project^.PInfo,'_',AText^.Font);
               end;
               Project^.SetModified;
            end;
          except
          end;
        end;
     end;
     ProgressBar.Position:=0;
end;

{------------------------------------------------------------------------------}

procedure TProjRepForm.DeleteObject(ALayer: PLayer; AIndex: Integer);
var
DelIdx: Integer;
DelItem: PIndex;
AItem: PIndex;
begin
     AItem:=New(Pindex,Init(AIndex));
     if (AItem <> nil) and (ALayer <> nil) then begin
        try
           while ALayer^.Data^.Search(AItem,DelIdx) do begin
              DelItem:=ALayer^.Data^.At(DelIdx);
              if DelItem^.ObjectStyle <> nil then begin
                 with DelItem^.ObjectStyle^ do begin
                      UpdateUseCounts(Project^.PInfo,LineStyle,FillStyle,SymbolFill,False);
                 end;
              end;
              ALayer^.Data^.AtFree(DelIdx);
              Project^.SetModified;
           end;
        finally
           Dispose(AItem,Done);
        end;
     end;
end;

end.
