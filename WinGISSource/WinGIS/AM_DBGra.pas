{****************************************************************************}
{ Unit AM_DBGra                                                              }
{----------------------------------------------------------------------------}
{                                                                            }
{----------------------------------------------------------------------------}
{ Modifikationen                                                             }
{  07.10.1994 Heinz Ofner                                                    }
{  16.02.1995 Martin Forst   Fehlermeldung, wenn Layer fixiert               }
{****************************************************************************}
Unit AM_DBGra;
 
Interface

Uses WinTypes, WinProcs, Objects, AM_RText, AM_BuGra, AM_Proj, AM_Font,AM_Circl,
     AM_Text,AM_Def,AM_Layer,AM_Dlg6,AM_Admin,AM_Index,AM_View,AM_Point,AM_Paint,
     AM_StdDl,AM_Pass,AM_Ini,AM_Dlg1,AM_Sym,AM_Obj,AM_Group,Chart,TeEngine,UserIntf,AM_Cpoly,AM_Poly,
     {$IFNDEF AXDLL} // <----------------- AXDLL
     BusGraphics,
     {$ENDIF} // <----------------- AXDLL
     Forms,Controls;
                                        
Const ntCDNot      = 0;
      ntCDsNever   = 1;
      ntCDAsBD     = 2;
      ntCDsAsBDs   = 3;

Type
  PBusGraphText = ^TBusGraphText;
  TBusGraphText = Object(TOldObject)
    DC          : HDC;
    LineColl    : PStrCollection;        { Collection f�r die Datenzeilen der einzelnen Objekte }
    TextPos     : TDRect;                { linker oberer Punkt des Text- oder Diagrammeintrages }
    MaxLen      : Integer;               { L�nge der l�ngsten Datenzeile }
    DiagSize    : Integer;
    DefaultType : Integer;
    DefMarkVis  : Boolean;
    DefMarkType : Integer;
    DefMarkFont : String;
    DefMarkSize : Integer;
    Def3D       : Boolean;
    Constructor Init;
    Destructor  Done; virtual;
    Procedure   InsertString(StrLine:PChar);
    Procedure   DBText(AData:PProj);
    Procedure   DBGraphic(AData:PProj;Flag:boolean=false;IDBLayer:Player=nil);
    Procedure   SetItemColor(i: Integer;Col: AnsiString);
  end;

  Procedure ProcChangeGraphText(AData:PProj;SnaItem:PIndex);

  Procedure ProcChangeBusGraph(AData:PProj;SnaItem:PIndex);

  Procedure CalculateTextPos(SItem:PIndex;Obje:PView;var PosX:LongInt;var PosY:LongInt;Width,Height,TextHeight:LongInt);
{brovak}
  Function CalculateTextPos1(SItem:PIndex;Obje:PView;var PosX:LongInt;var PosY:LongInt;Width,Height,TextHeight:LongInt):integer;
{brovak}
  Procedure CalculateBusGraphPos(SItem:PIndex;Obje:PView;var PosX:LongInt;var PosY:LongInt;Width,Height,
                                 BeamWidth,CDiameter:LongInt;DType:Byte);

  Function PlusMinus(ValueItems:PCollection):Boolean;

Implementation

Uses ResDlg,SysUtils,Classes,GrTools,AM_ProjO,Math
     {$IFNDEF WMLT}
     ,AM_DDE
     {$ENDIF}
     ,DDEDef
{++ IDB_UNDO}
     , AM_Main, IDB_CallBacksDef
{-- IDB_UNDO}
{++ Ivanoff}
     , DK_AnnotSettings
{-- Ivanoff}
;

{**************************************** ProcChangeGraphText ****************************************************************}

Procedure ProcChangeGraphText
   (
   AData           : PProj;
   SnaItem         : PIndex
   );
  var OldFont      : TFontData;
      OldTextFont  : HFont;
      WriteTo      : PLayer;
      OldLayer     : PLayer;
      TextHeight   : LongInt;
      OldTop       : PLayer;
      TempStr      : PChar;
      NewPos       : Bool;
      TextFont     : HFont;
      TempRect     : TRect;
      Width        : LongInt;
      Height       : LongInt;
      OldWidth     : LongInt;
      OldHeight    : LongInt;
      PosX         : LongInt;
      PosY         : LongInt;
      SItem        : PIndex;
      Obje         : PView;
      TextPos      : TDRect;
      AText        : PRText;
      ExistObject  : PIndex;
      i            : Integer;
      NameColl     : PCollection;
      OldAnnot     : PChar;
      NewAnnot     : PChar;
      LastRow      : PChar;
      AFont        : PFontDes;

    Procedure CopyAll                                      { Collection kopieren und Beschriftungszeilen einf�gen }
       (
       Item           : PAnnotColl
       ); Far;
      var Annotation  : PAnnot;
          Temp        : Array[0..80] of Char;
          j           : Integer;
      begin
        Annotation:=New(PAnnot);
        Annotation^.LineName:=StrNew(Item^.Name);
        Annotation^.HasValue:=Item^.HasVal;
        StrCopy(Temp,#0);
        j:=0;
        if Item^.HasVal then begin
          while (OldAnnot[i] <> #10) and (OldAnnot[i] <> #0) do begin
            Temp[j]:=OldAnnot[i];
            Inc(i);
            Inc(j);
          end;
          Temp[j]:=#0;
          Inc(i,2);
        end;
        Annotation^.LineValue:=StrNew(Temp);
        NameColl^.Insert(Annotation);
      end;

    Procedure CopyAll1                                      { Beschriftungszeilen zur�ckschreiben }
       (
       Item1          : PAnnot
       ); Far;
      var Temp        : Array[0..80] of Char;
      begin
        if Item1^.HasValue then begin
          StrCopy(Temp,Item1^.LineValue);
          StrCat(Temp,#10#13);
          StrCat(NewAnnot,Temp);
        end;
        Inc(i);
      end;
{++ Ivanoff}
{$IFNDEF AXDLL} // <----------------- AXDLL
Var
   AForm: TDK_AnnotSettingsForm;
{$ENDIF} // <----------------- AXDLL
{-- Ivanoff}
  begin
    OldFont.Init;
    OldFont:=AData^.ActualFont;
    AData^.ActualFont:=PRText(SnaItem)^.Font;
    OldTop:=AData^.Layers^.TopLayer;
    ExistObject:=AData^.Layers^.TopLayer^.HasObject(SnaItem);
    if ExistObject = NIL then begin
      i:=0;
      while (ExistObject = NIL) and (i < AData^.Layers^.LData^.Count) do begin
        AData^.Layers^.TopLayer:=AData^.Layers^.LData^.At(i);
        if not AData^.Layers^.TopLayer^.GetState(sf_LayerOff) then
          ExistObject:=AData^.Layers^.TopLayer^.HasObject(SnaItem);
        i:=i+1;
      end;
    end;
    WriteTo:=AData^.Layers^.NameToLayer(AData^.Layers^.TopLayer^.Text^);
    OldLayer:=WriteTo;
    AData^.Layers^.TopLayer:=OldTop;
    NameColl:=New(PCollection,Init(5,5));
    OldAnnot:=PRText(SnaItem)^.RText;
    i:=0;
    PRText(SnaItem)^.LineNames^.ForEach(@CopyAll);
    NewPos:=FALSE;
    SItem:=New(PIndex,Init(0));
    SItem^.Index:=PRText(SnaItem)^.ID;
    Obje:=PLayer(AData^.PInfo^.Objects)^.IndexObject(AData^.PInfo,SItem);

{++ Ivanoff}
   {$IFNDEF WMLT}
   {$IFNDEF AXDLL} // <----------------- AXDLL
   AForm := TDK_AnnotSettingsForm.Create(Application,
                                         AData,
                                         WriteTo,
                                         NameColl);
   AForm.MakewindowAsChangeDialog(PRText(SnaItem).bKeepLinkToDBActive, WinGISMainForm.WeAreUsingTheIDB[AData]);
   If AForm.ShowModal = mrOK Then Begin
      WriteTo := AData.Layers.IndexToLayer(AForm.GetDestinationLayer.Index);
      NewPos := AForm.CalculateNewPosition;
      PRText(SnaItem).bKeepLinkToDBActive := AForm.KeepLinkToDBAlive;
(*
    if ExecDialog(TDSelFontLayer.Init(AData^.Parent,AData^.Layers,
        @WriteTo,NameColl,AData^.PInfo,@(AData^.ActualFont),
        @NewPos,'SelFontLayerChange',Obje<>NIL))=id_OK then begin
//*)
   {$ENDIF} // <----------------- AXDLL
   {$ENDIF}
{-- Ivanoff}
{++ IDB_UNDO UnChange}
      {$IFNDEF AXDLL} // <----------------- AXDLL
        {$IFNDEF WMLT}
      If (WinGisMainForm.WeAreUsingUndoFunction[AData])
         AND
         (WinGisMainForm.UndoRedo_Man.AddObjectInUndoData(AData, PView(SnaItem))) Then
            WinGisMainForm.UndoRedo_Man.SaveUndoData(AData, utUnchange);
        {$ENDIF}
      {$ENDIF}         // <----------------- AXDLL
{-- IDB_UNDO UnChange}
      AFont:=AData^.PInfo^.Fonts^.GetFont(PRText(SnaItem)^.Font.Font); {mf}
      NewAnnot:=StrAlloc(StrLen(OldAnnot)+3);
      StrCopy(NewAnnot,'');
      i:=1;
      NameColl^.ForEach(@CopyAll1);
      LastRow:=StrRScan(NewAnnot,#10);
      if LastRow <> NIL then StrCopy(LastRow,#0);
{++ Ivanoff}
      {$IFNDEF AXDLL} // <----------------- AXDLL
      If AForm.RadioButton1.Checked Then
         PRText(SnaItem)^.ChangeData(NameColl)
      Else // The old way.
         PRText(SnaItem)^.ChangeData(NameColl, NewAnnot);
      {$ENDIF}         // <----------------- AXDLL
{-- Ivanoff}
      TextHeight:=PRText(SnaItem)^.Font.Height;
      {TextHeight:=AData^.ActualFont.Height;}
      OldTop:=AData^.Layers^.TopLayer;
      TempStr:=PRText(SnaItem)^.RText;
      TextFont:=AData^.PInfo^.GetFont(PRText(SnaItem)^.Font,AData^.PInfo^.CalculateDisp(TextHeight),0);
      {TextFont:=AData^.PInfo^.GetFont(AData^.ActualFont,AData^.PInfo^.CalculateDisp(TextHeight),0);}
      if TextFont > 0 then begin
        TempRect.Left:=0;
        TempRect.Top:=0;
        OldTextFont:=SelectObject(AData^.PInfo^.ExtCanvas.Handle,TextFont);
        DrawText(AData^.PInfo^.ExtCanvas.Handle,TempStr,StrLen(TempStr),TempRect,dt_CalcRect);
        if (AFont<>NIL) and AFont^.IsVertical then begin
          Height:=AData^.PInfo^.CalculateDraw(TempRect.Right);
          Width:=AData^.PInfo^.CalculateDraw(Abs(TempRect.Bottom));
        end
        else begin
          Width:=AData^.PInfo^.CalculateDraw(TempRect.Right);
          Height:=AData^.PInfo^.CalculateDraw(Abs(TempRect.Bottom));
        end;
        SelectObject(AData^.PInfo^.ExtCanvas.Handle,OldTextFont);
        DeleteObject(TextFont);
        if NewPos and (Obje <> NIL) then begin
          CalculateTextPos(SItem,Obje,PosX,PosY,Width,Height,TextHeight);
          TextPos.Init;
          TextPos.Assign(PosX,PosY,PosX+Width,PosY-Height);
          if WriteTo <> OldLayer then begin
            AData^.Layers^.DeleteIndex(AData^.PInfo,SnaItem);
            SnaItem^.Invalidate(AData^.PInfo);
            AData^.Layers^.TopLayer:=WriteTo;
            AText:=New(PRText,InitCalculate(AData^.PInfo,AData^.ActualFont,NameColl,TempStr,TextPos,SItem^.Index));
            if AData^.InsertObject(AText,False) then {sl}
              AData^.CorrectSize(AText^.ClipRect,False);
            AData^.Layers^.TopLayer:=OldTop;
          end
          else begin
            PRText(SnaItem)^.TextRect:=TextPos;
            AFont:=AData^.PInfo^.Fonts^.GetFont(PRText(SnaItem)^.Font.Font);
            if AFont<>NIL then Dec(AFont^.UseCount);
            PRText(SnaItem)^.Font:=AData^.ActualFont;
            AFont:=AData^.PInfo^.Fonts^.GetFont(PRText(SnaItem)^.Font.Font);
            if AFont<>NIL then Inc(AFont^.UseCount);
            SnaItem^.Invalidate(AData^.PInfo);
            PRText(SnaItem)^.CalculateClipRect(AData^.PInfo);
            AData^.UpdateClipRect(PView(SnaItem));
            SnaItem^.Invalidate(AData^.PInfo);
            if not AData^.CorrectSize(PView(SnaItem)^.ClipRect,TRUE) then AData^.PInfo^.RedrawInvalidate;
            AData^.SetModified;
          end;
        end
        else begin
          TextPos.Init;
          OldWidth:=PRText(SnaItem)^.TextRect.B.X-PRText(SnaItem)^.TextRect.A.X;
          OldHeight:=PRText(SnaItem)^.TextRect.A.Y-PRText(SnaItem)^.TextRect.B.Y;
          PosX:=PRText(SnaItem)^.TextRect.A.X+Trunc(OldWidth/2-Width/2);
          PosY:=PRText(SnaItem)^.TextRect.A.Y-Trunc(OldHeight/2-Height/2);
          TextPos.Assign(PosX,PosY,PosX+Width,PosY-Height);
          if WriteTo <> OldLayer then begin
            AData^.Layers^.TopLayer:=WriteTo;
            AData^.Layers^.DeleteIndex(AData^.PInfo,SnaItem);
            SnaItem^.Invalidate(AData^.PInfo);
            AText:=New(PRText,InitCalculate(AData^.PInfo,AData^.ActualFont,NameColl,TempStr,TextPos,SItem^.Index));
            if AData^.InsertObject(AText,False) then {sl}
                AData^.CorrectSize(AText^.ClipRect,False);
            AData^.Layers^.TopLayer:=OldTop;
          end
          else begin
            PRText(SnaItem)^.TextRect:=TextPos;
            AFont:=AData^.PInfo^.Fonts^.GetFont(PRText(SnaItem)^.Font.Font);
            if AFont<>NIL then Dec(AFont^.UseCount);
            PRText(SnaItem)^.Font:=AData^.ActualFont;
            AFont:=AData^.PInfo^.Fonts^.GetFont(PRText(SnaItem)^.Font.Font);
            if AFont<>NIL then Inc(AFont^.UseCount);
            SnaItem^.Invalidate(AData^.PInfo);
            PRText(SnaItem)^.CalculateClipRect(AData^.PInfo);
            AData^.UpdateClipRect(PView(SnaItem));
            SnaItem^.Invalidate(AData^.PInfo);
            if not AData^.CorrectSize(PView(SnaItem)^.ClipRect,TRUE) then AData^.PInfo^.RedrawInvalidate;
            AData^.SetModified;
          end;
        end;
      end;
      StrDispose(NewAnnot);
    {$IFNDEF WMLT}
    {$IFNDEF AXDLL} // <----------------- AXDLL
    end;
    {$ENDIF}         // <----------------- AXDLL
    AData^.ActualFont:=OldFont;
    i:=0;
    while i < NameColl^.Count do begin
      StrDispose(PAnnot(NameColl^.At(i))^.LineName);
      StrDispose(PAnnot(NameColl^.At(i))^.LineValue);
      Inc(i);
    end;
    NameColl^.DeleteAll;
    Dispose(NameColl,Done);
    Dispose(SItem,Done);
{++ Ivanoff}
    {$IFNDEF AXDLL} // <----------------- AXDLL
    AForm.Free;
    {$ENDIF}   // <----------------- AXDLL
    {$ENDIF}
{-- Ivanoff}
  end;

{**************************************** ProcChangeBusGraph *****************************************************************}

Procedure ProcChangeBusGraph
   (
   AData                : PProj;
   SnaItem              : PIndex
   );
  var WriteTo           : PLayer;
      OldLayer          : PLayer;
      OldTop            : PLayer;
      NewPos            : Bool;
      Width             : LongInt;
      Height            : LongInt;
      OldWidth          : LongInt;
      OldHeight         : LongInt;
      PosX              : LongInt;
      PosY              : LongInt;
      SItem             : PIndex;
      Obje              : PView;
      Dest              : PCollection;
      TempColl          : PCollection;
      AType             : Byte;
      ABeamHeight       : LongInt;
      ABeamWidth        : LongInt;
      ACircleDiameter   : LongInt;
      BCircleDiameter   : LongInt;
      Cols              : Integer;
      MaxVal            : Real;
      Sum               : Real;
      GraphPos          : TDPoint;
      ExistObject       : PIndex;
      i                 : Integer;
      ACDDiamAbsRel     : Bool;
      AShowValues       : Bool;
      AAbsRel           : Bool;
      AFont             : TFontData;
      MaxTWidth         : LongInt;
      THeight           : LongInt;
      IItem             : PIndex;
      UpdateRect        : TDRect;
      DelObj            : PView;
      APLAColor         : LongInt;
      APLAPen           : Word;
      APLAPattern       : Word;
      BFont             : PFontDes;

{**************************************** gr��ten Wert feststellen ****************************************}

    Procedure DoAll
       (
       Item           : PDGraph
       ); Far;
      begin
        Inc(Cols);
        if Item^.Value > MaxVal then MaxVal:=Item^.Value;
      end;

{**************************************** gr��te Beschriftungsl�nge feststellen ****************************************}

    Procedure DoAll1
       (
       Item2          : PDGraph
       ); Far;
    var
      TText           : String;
      PercentVal      : Real;
      TWidth          : LongInt;
    begin
      if AAbsRel then Str(Item2^.Value:0:0,TText)
      else begin
        PercentVal:=100/Sum*Item2^.Value;
        Str(PercentVal:0:2,TText);
        TText:=TText+' %';
      end;
      TWidth:=Round(THeight*AData^.PInfo^.GetTextRatio(AFont,TText));
      if TWidth > MaxTWidth then MaxTWidth:=TWidth;
    end;

{**************************************** Collection kopieren ****************************************}

    Procedure CopyAll
       (
       Item           : PDGraph
       ); Far;
      var CopyItem    : PDGraph;
      begin
        CopyItem:=New(PDGraph,Init(StrNew(Item^.Name),Item^.Value,Item^.Color,Item^.Pen,Item^.Pattern));
        Dest^.Insert(CopyItem);
      end;

{**************************************** Hauptteil ****************************************}

  begin
    AType:=PBusGraph(SnaItem)^.DiagType;
    ABeamHeight:=PBusGraph(SnaItem)^.BHeight;
    ABeamWidth:=PBusGraph(SnaItem)^.BWidth;
    ACircleDiameter:=PBusGraph(SnaItem)^.NCDiameter;
    TempColl:=New(PCollection,Init(5,5));
    Dest:=TempColl;
    PBusGraph(SnaItem)^.ValItems^.ForEach(@CopyAll);
    OldTop:=AData^.Layers^.TopLayer;
    ExistObject:=AData^.Layers^.TopLayer^.HasObject(SnaItem);
    if ExistObject = NIL then begin
      i:=0;
      while (ExistObject = NIL) and (i < AData^.Layers^.LData^.Count) do begin
        AData^.Layers^.TopLayer:=AData^.Layers^.LData^.At(i);
        if not AData^.Layers^.TopLayer^.GetState(sf_LayerOff) then
          ExistObject:=AData^.Layers^.TopLayer^.HasObject(SnaItem);
        i:=i+1;
      end;
    end;
    WriteTo:=AData^.Layers^.NameToLayer(AData^.Layers^.TopLayer^.Text^);
    OldLayer:=WriteTo;
    AData^.Layers^.TopLayer:=OldTop;
    ACDDiamAbsRel:=PBusGraph(SnaItem)^.DCircAbsRel;
    AShowValues:=PBusGraph(SnaItem)^.ShowValues;
    AAbsRel:=PBusGraph(SnaItem)^.AnnotAbsRel;
    AFont.Init;
    AFont:=PBusGraph(SnaItem)^.SVFont;
    APLAColor:=PBusGraph(SnaItem)^.PLAColor;
    APLAPen:=PBusGraph(SnaItem)^.PLAPen;
    APLAPattern:=PBusGraph(SnaItem)^.PLAPattern;                             
    NewPos:=FALSE;
    if ExecDialog(TDColumnChange.Init(AData^.Parent,AData^.Layers,@WriteTo,TempColl,
        AData^.PInfo,@AFont,@AType,@ABeamHeight,@ABeamWidth,@ACircleDiameter,@ACDDiamAbsRel,
        @AShowValues,@AAbsRel,@APLAColor,@APLAPen,@APLAPattern,@NewPos,'COLUMNCHANGE'))=id_OK then begin
      if ((AType <> dtCircleDiag) and (AType <> dtAreaDiag))
          or ((AType = dtCircleDiag) and (not PlusMinus(TempColl) and (not(PBusGraph(SnaItem)^.Sum = 0))))
          or ((AType = dtAreaDiag) and (not PlusMinus(TempColl))) then begin
        if AType = dtCircleDiag then begin
          if ACDDiamAbsRel then BCircleDiameter:=ACircleDiameter
          else BCircleDiameter:=Round(ACircleDiameter*PBusGraph(SnaItem)^.Sum/100);
          AFont.Height:=Trunc(BCircleDiameter/5);
        end
        else AFont.Height:=Trunc(8*ABeamWidth/10);
        SItem:=New(PIndex,Init(0));
        SItem^.Index:=PBusGraph(SnaItem)^.ID;
        Obje:=PLayer(AData^.PInfo^.Objects)^.IndexObject(AData^.PInfo,SItem);
        if Obje<>NIL then begin
          Cols:=0;
          MaxVal:=0;
          PBusGraph(SnaItem)^.ValItems^.FreeAll;
          Dest:=PBusGraph(SnaItem)^.ValItems;
          TempColl^.ForEach(@CopyAll);
          PBusGraph(SnaItem)^.ValItems^.ForEach(@DoAll);
          if AType = dtCircleDiag then begin
            if AShowValues then begin
              MaxTWidth:=0;
              THeight:=AFont.Height;
              PBusGraph(SnaItem)^.ValItems^.ForEach(@DoAll1);
              Width:=BCircleDiameter+2*MaxTWidth;
              Height:=BCircleDiameter+2*THeight;
            end
            else begin
              Width:=BCircleDiameter;
              Height:=BCircleDiameter;
            end;
          end
          else if AType = dtBeamDiag then begin
            Width:=ABeamWidth*Cols;
            Height:=Round(ABeamHeight/100*MaxVal);
          end
          else begin
            Width:=ABeamWidth*Cols;
            Height:=Round(ABeamHeight/100*MaxVal+ABeamWidth/5);
          end;
          PBusGraph(SnaItem)^.DiagType:=AType;
          PBusGraph(SnaItem)^.BWidth:=ABeamWidth;
          PBusGraph(SnaItem)^.BHeight:=ABeamHeight;
          PBusGraph(SnaItem)^.NCDiameter:=ACircleDiameter;
          PBusGraph(SnaItem)^.GesWidth:=Width;
          PBusGraph(SnaItem)^.GesHeight:=Height;
          PBusGraph(SnaItem)^.DCircAbsRel:=ACDDiamAbsRel;
          PBusGraph(SnaItem)^.ShowValues:=AShowValues;
          PBusGraph(SnaItem)^.AnnotAbsRel:=AAbsRel;
          BFont:=AData^.PInfo^.Fonts^.GetFont(PBusGraph(SnaItem)^.SVFont.Font);
          if BFont<>NIL then Dec(BFont^.UseCount);
          PBusGraph(SnaItem)^.SVFont:=AFont;
          BFont:=AData^.PInfo^.Fonts^.GetFont(PBusGraph(SnaItem)^.SVFont.Font);
          if BFont<>NIL then Inc(BFont^.UseCount);
          PBusGraph(SnaItem)^.PLAColor:=APLAColor;
          PBusGraph(SnaItem)^.PLAPen:=APLAPen;
          PBusGraph(SnaItem)^.PLAPattern:=APLAPattern;
          if ACDDiamAbsRel then PBusGraph(SnaItem)^.CDiameter:=PBusGraph(SnaItem)^.NCDiameter
          else PBusGraph(SnaItem)^.CDiameter:=Round(PBusGraph(SnaItem)^.NCDiameter*PBusGraph(SnaItem)^.Sum/100);
          if NewPos then begin
            CalculateBusGraphPos(SItem,Obje,PosX,PosY,Width,Height,ABeamWidth,BCircleDiameter,AType);
            if (AType = dtCircleDiag) and AShowValues then begin
              PosX:=PosX+MaxTWidth;
              PosY:=PosY-THeight;
            end;
          end
          else begin
            OldWidth:=PBusGraph(SnaItem)^.GesWidth;
            OldHeight:=PBusGraph(SnaItem)^.GesHeight;
            PosX:=PBusGraph(SnaItem)^.Position.X+Trunc(OldWidth/2-Width/2);
            PosY:=PBusGraph(SnaItem)^.Position.Y-Trunc(OldHeight/2-Height/2);
          end;
          GraphPos.Init(PosX,PosY);
          PBusGraph(SnaItem)^.Position:=GraphPos;
          GraphPos.Done;
          if WriteTo <> OldLayer then begin
            UpdateRect.Init;
            IItem:=New(PIndex,Init(SnaItem^.Index));
            WriteTo^.InsertObject(AData^.PInfo,IItem,FALSE);
            AData^.Layers^.IndexObject(AData^.PInfo,SnaItem)^.Invalidate(AData^.PInfo);
            DelObj:=AData^.Layers^.IndexObject(AData^.PInfo,SnaItem);
            UpdateRect.CorrectByRect(DelObj^.ClipRect);
            OldLayer^.DeleteIndex(AData^.PInfo,SnaItem);
            AData^.Layers^.SelLayer^.DeleteIndex(AData^.PInfo,SnaItem);
{!?!?!?            UpdateRect.Grow(AData^.PInfo^.CalculateDraw(AData^.Layers^.TopLayer^.LineStyle Shr 8));}
            AData^.PInfo^.RedrawRect(UpdateRect);
          end
          else begin
            SnaItem^.Invalidate(AData^.PInfo);
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
//--                       PBusGraph(SnaItem)^.CalculateClipRect(AData^.PInfo);
                           PBusGraph(SnaItem)^.CalculateClipRect;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
            AData^.UpdateClipRect(PView(SnaItem));
            SnaItem^.Invalidate(AData^.PInfo);
            if not AData^.CorrectSize(PView(SnaItem)^.ClipRect,TRUE) then AData^.PInfo^.RedrawInvalidate;
          end;
          AData^.SetModified;
        end;
      end
      else MsgBox(AData^.Parent.Handle,10400,mb_IconExclamation or mb_OK,'');
    end;
    Dispose(TempColl,Done);
    AFont.Done;
  end;

{**************************************** PlusMinus **************************************************************************}

Function PlusMinus
   (
   ValueItems   : PCollection
   ):Boolean;
  var PlusVal  : Boolean;
      MinusVal : Boolean;

  Procedure DoAll
     (
     Item      : PDGraph
     ); Far;
    begin
      if Item^.Value > 0 then PlusVal:=TRUE;
      if Item^.Value < 0 then MinusVal:=TRUE;
    end;

  begin
    PlusVal:=FALSE;
    MinusVal:=FALSE;
    ValueItems^.ForEach(@DoAll);
    if PlusVal and MinusVal then PlusMinus:=TRUE
    else PlusMinus:=FALSE;
  end;

{**************************************** CalculateTextPos *******************************************************************}

Procedure CalculateTextPos
   (
   SItem           : PIndex;
   Obje            : PView;
   var PosX        : LongInt;
   var PosY        : LongInt;
   Width           : LongInt;
   Height          : LongInt;
   TextHeight      : LongInt
   );
  var  Rad        : LongInt;
       Circuit    : LongInt;
       BeginAngle : Real;
       EndAngle   : Real;
       Alpha      : Real;
       Beta       : Real;
       Gamma      : Real;
       BRad       : Real;
       Term       : Real;
  begin
    if SItem^.GetObjType = ot_Poly then begin
      PosX:=Obje^.GivePosX + Width Div 5;
      PosY:=Obje^.GivePosY + Height Div 2;
    end
    else if SItem^.GetObjType = ot_cPoly then begin
      PosX:=Obje^.GiveAPosX - Width Div 2;
      PosY:=Obje^.GiveAPosY + Height Div 2;
    end
    else if SItem^.GetObjType = ot_Circle then begin
      PosX:=Obje^.GivePosX;
      PosY:=Obje^.GivePosY;
      Rad:=Obje^.GiveRadius;
      Circuit:=Trunc(1.1*(Sqrt((Sqr(Width/2)+Sqr(Height/2)))));
      if Rad < Circuit then begin
        PosX:=PosX + Rad + TextHeight Div 4;
        PosY:=PosY + Height Div 2;
      end
      else if Rad > 2*Circuit then begin
        PosX:=PosX - Rad + TextHeight;
        PosY:=PosY + Height Div 2;
      end
      else begin
        PosX:=PosX - Width Div 2;
        PosY:=PosY + Height Div 2;
      end
    end
    else if SItem^.GetObjType = ot_Arc then begin
      PosX:=Obje^.GivePosX;
      PosY:=Obje^.GivePosY;
      Rad:=Obje^.GiveRadius;
      BeginAngle:=Obje^.GiveBegAngle;
      EndAngle:=Obje^.GiveEndAngle;
      Circuit:=Trunc(1.1*(Sqrt((Sqr(Width/2)+Sqr(Height/2)))));
      Alpha:=BeginAngle+(EndAngle-BeginAngle)/2;
      if BeginAngle > EndAngle then Alpha:=Alpha-Pi;
      if Alpha < 0 then Alpha:=Alpha+2*Pi;
      BRad:=Rad*0.9;
      if Rad < Circuit then begin
        if BeginAngle > EndAngle then begin
          PosX:=PosX + Rad + TextHeight Div 4;
          PosY:=PosY + Height Div 2;
        end
        else begin
          if BeginAngle < 2*Pi-EndAngle then begin
            PosX:=Round(PosX + Rad*cos(BeginAngle) + TextHeight/4);
            PosY:=Round(PosY + Rad*sin(BeginAngle) + Height/2);
          end
          else if BeginAngle > 2*Pi-EndAngle then begin
            PosX:=Round(PosX + Rad*cos(EndAngle) + TextHeight/4);
            PosY:=Round(PosY + Rad*sin(EndAngle) + Height/2);
          end
          else begin
            PosX:=Round(PosX + Rad*cos(BeginAngle) + TextHeight/4);
            PosY:=Round(PosY + Height/2);
          end;
        end;
      end
      else begin
        if (Alpha >= 0) and (Alpha < ArcTan(Height/Width)) then begin
          Term:=(0.5*(Height-Width*(Sin(Alpha)/Cos(Alpha))))/BRad*Sin(Pi/2+Alpha);
          Beta:=ArcTan(Term/(1-Sqr(Term)));
          Gamma:=Alpha+Beta;
          PosX:=Round(PosX+BRad*Cos(Gamma)-Width);
          PosY:=Round(PosY+BRad*Sin(Gamma));
        end
        else if (Alpha >= ArcTan(Height/Width)) and (Alpha < Pi/2) then begin
          Term:=(0.5*(Width*(Sin(Alpha)/Cos(Alpha))-Height))/BRad*Sin(Pi/2-Alpha);
          Beta:=ArcTan(Term/(1-Sqr(Term)));
          Gamma:=Alpha-Beta;
          PosX:=Round(PosX+BRad*Cos(Gamma)-Width);
          PosY:=Round(PosY+BRad*Sin(Gamma));
        end
        else if (Alpha >= Pi/2) and (Alpha < ArcTan(Height/-Width)+Pi) then begin
          Term:=(0.5*(Width*(Sin(Pi-Alpha)/Cos(Pi-Alpha))-Height))/BRad*Sin(-Pi/2+Alpha);
          Beta:=ArcTan(Term/(1-Sqr(Term)));
          Gamma:=Alpha+Beta;
          PosX:=Round(PosX+BRad*Cos(Gamma));
          PosY:=Round(PosY+BRad*Sin(Gamma));
        end
        else if (Alpha >= ArcTan(Height/-Width)+Pi) and (Alpha < Pi) then begin
          Term:=(0.5*(Height-Width*(Sin(Pi-Alpha)/Cos(Pi-Alpha))))/BRad*Sin(-Pi/2-Alpha);
          Beta:=ArcTan(Term/(1-Sqr(Term)));
          Gamma:=Alpha-Beta;
          PosX:=Round(PosX+BRad*Cos(Gamma));
          PosY:=Round(PosY+BRad*Sin(Gamma));
        end
        else if (Alpha >= Pi) and (Alpha < ArcTan(-Height/-Width)+Pi) then begin
          Term:=(0.5*(Height-Width*(Sin(Alpha-Pi)/Cos(Alpha-Pi))))/BRad*Sin(-Pi/2+Alpha);
          Beta:=ArcTan(Term/(1-Sqr(Term)));
          Gamma:=Alpha+Beta;
          PosX:=Round(PosX+BRad*Cos(Gamma));
          PosY:=Round(PosY+BRad*Sin(Gamma)+Height);
        end
        else if (Alpha >= ArcTan(-Height/-Width)+Pi) and (Alpha < 3/2*Pi) then begin
          Term:=(0.5*(Width*(Sin(Alpha-Pi)/Cos(Alpha-Pi))-Height))/BRad*Sin(-Pi/2-Alpha);
          Beta:=ArcTan(Term/(1-Sqr(Term)));
          Gamma:=Alpha-Beta;
          PosX:=Round(PosX+BRad*Cos(Gamma));
          PosY:=Round(PosY+BRad*Sin(Gamma)+Height);
        end
        else if (Alpha >= 3/2*Pi) and (Alpha < ArcTan(-Height/Width)+2*Pi) then begin
          Term:=(0.5*(Width*(Sin(-Alpha)/Cos(-Alpha))-Height))/BRad*Sin(Pi/2+Alpha);
          Beta:=ArcTan(Term/(1-Sqr(Term)));
          Gamma:=Alpha+Beta;
          PosX:=Round(PosX+BRad*Cos(Gamma)-Width);
          PosY:=Round(PosY+BRad*Sin(Gamma)+Height);
        end
        else if (Alpha >= ArcTan(-Height/Width)+2*Pi) and (Alpha < 2*Pi) then begin
          Term:=(0.5*(Height-Width*(Sin(-Alpha)/Cos(-Alpha))))/BRad*Sin(Pi/2-Alpha);
          Beta:=ArcTan(Term/(1-Sqr(Term)));
          Gamma:=Alpha-Beta;
          PosX:=Round(PosX+BRad*Cos(Gamma)-Width);
          PosY:=Round(PosY+BRad*Sin(Gamma)+Height);
        end
      end
    end
    else if SItem^.GetObjType = ot_Symbol then begin
      PosX:=Obje^.ClipRect.B.X + TextHeight Div 4;
      PosY:=Trunc((Obje^.ClipRect.B.Y-Obje^.ClipRect.A.Y)/2) + Obje^.ClipRect.A.Y + Height Div 2;
    end
    else if SItem^.GetObjType = ot_Pixel then begin
      PosX:=Obje^.GivePosX + TextHeight Div 4;
      PosY:=Obje^.GivePosY + Height Div 2;
    end
    else begin
      PosX:=Obje^.ClipRect.B.X + TextHeight Div 4;
      PosY:=Trunc((Obje^.ClipRect.B.Y-Obje^.ClipRect.A.Y)/2) + Obje^.ClipRect.A.Y + Height Div 2;
    end;
  end;

//Modified variant by Brovak
(* This procedure found basepoint for annotation for advanced settings *)

Function CalculateTextPos1
   (
   SItem           : PIndex;
   Obje            : PView;
   var PosX        : LongInt;
   var PosY        : LongInt;
   Width           : LongInt;
   Height          : LongInt;
   TextHeight      : LongInt
   ):integer;
  var  Rad        : LongInt;
       Circuit    : LongInt;
       BeginAngle : Real;
       EndAngle   : Real;
       Alpha      : Real;
       Beta       : Real;
       Gamma      : Real;
       BRad       : Real;
       Term       : Real;
       APoint     : TDPoint;
       Dlina,TempDlina,Rebro  : Real;
       i:integer;
       CurrentPoint,TmpPoint:Tdpoint;(*X,TmpPointY,FirstPointX,FirstPointY,
        SecondPointX,SecondPointY,AverageX,AverageY,SpecDist:longint;*)
  begin
  Result:=0;
   with Inifile.AdvAnnotationSettings do begin;
  (***********)
     if SItem^.GetObjType = ot_Poly then begin
   (*    if PolylineAdv=false then begin
      PosX:=Obje^.GivePosX + Width Div 5;
      PosY:=Obje^.GivePosY + Height Div 2;
                              end
     else
     begin; *)
     Dlina:=100*PPoly(Obje).Laenge / 2;
     TempDlina:=0;
     i:=0;
     Rebro:=0;
     with PPoly(Obje)^ do begin;
     while (TempDlina+Rebro)< Dlina do
      begin;
       TempDlina:=TempDlina+Rebro;
        inc(i);

        Rebro:=PDPoint(Data^.At(i-1))^.Dist(PDPoint(Data^.At(i))^);
      end;
      Alpha:=PDPoint(Data^.At(i-1))^.CalculateAngle(PDPoint(Data^.At(i))^);
      PosX:=Trunc(PDPoint(Data^.At(i-1))^.X+(Dlina-TempDlina)*Cos(Alpha));
      PosY:=Trunc(PDPoint(Data^.At(i-1))^.Y+(Dlina-TempDlina)*Sin(Alpha));
     if PolyLineAdvDirection=0
      then Result:= round((-Alpha*180)/pi)
      else
      if PolyLineAdvDirection=2
       then Result:=-90;

   //    else
     //  if PolyLineAdvDirection=2
    //   then Result:=pi/2
                              end;
    // end;
    end

 (***********)
    else if SItem^.GetObjType = ot_cPoly then begin
     (*if PolyGonAdv = false
      then begin;
      PosX:=Obje^.GiveAPosX - Width Div 2;
      PosY:=Obje^.GiveAPosY + Height Div 2;
           end
                 else begin; *)
       APoint.Init(0,0);          
       APoint:=Pcpoly(Obje).GetCenter;
       PosX:=APoint.X;
       PosY:=APoint.Y;
              //  end;
    end
 (***********)
    else if SItem^.GetObjType = ot_Circle then begin
      PosX:=Obje^.GivePosX;
      PosY:=Obje^.GivePosY;
      Rad:=Obje^.GiveRadius;
      Circuit:=Trunc(1.1*(Sqrt((Sqr(Width/2)+Sqr(Height/2)))));
      if Rad < Circuit then begin
        PosX:=PosX + Rad + TextHeight Div 4;
        PosY:=PosY + Height Div 2;
      end
      else if Rad > 2*Circuit then begin
        PosX:=PosX - Rad + TextHeight;
        PosY:=PosY + Height Div 2;
      end
      else begin
        PosX:=PosX - Width Div 2;
        PosY:=PosY + Height Div 2;
      end
    end
 else if SItem^.GetObjType = ot_Arc then begin
      PosX:=Obje^.GivePosX;
      PosY:=Obje^.GivePosY;
      Rad:=Obje^.GiveRadius;
      BeginAngle:=Obje^.GiveBegAngle;
      EndAngle:=Obje^.GiveEndAngle;
      Circuit:=Trunc(1.1*(Sqrt((Sqr(Width/2)+Sqr(Height/2)))));
      Alpha:=BeginAngle+(EndAngle-BeginAngle)/2;
      if BeginAngle > EndAngle then Alpha:=Alpha-Pi;
      if Alpha < 0 then Alpha:=Alpha+2*Pi;
      BRad:=Rad*0.9;
      if Rad < Circuit then begin
        if BeginAngle > EndAngle then begin
          PosX:=PosX + Rad + TextHeight Div 4;
          PosY:=PosY + Height Div 2;
        end
        else begin
          if BeginAngle < 2*Pi-EndAngle then begin
            PosX:=Round(PosX + Rad*cos(BeginAngle) + TextHeight/4);
            PosY:=Round(PosY + Rad*sin(BeginAngle) + Height/2);
          end
          else if BeginAngle > 2*Pi-EndAngle then begin
            PosX:=Round(PosX + Rad*cos(EndAngle) + TextHeight/4);
            PosY:=Round(PosY + Rad*sin(EndAngle) + Height/2);
          end
          else begin
            PosX:=Round(PosX + Rad*cos(BeginAngle) + TextHeight/4);
            PosY:=Round(PosY + Height/2);
          end;
        end;
      end
      else begin
        if (Alpha >= 0) and (Alpha < ArcTan(Height/Width)) then begin
          Term:=(0.5*(Height-Width*(Sin(Alpha)/Cos(Alpha))))/BRad*Sin(Pi/2+Alpha);
          Beta:=ArcTan(Term/(1-Sqr(Term)));
          Gamma:=Alpha+Beta;
          PosX:=Round(PosX+BRad*Cos(Gamma)-Width);
          PosY:=Round(PosY+BRad*Sin(Gamma));
        end  //
        else if (Alpha >= ArcTan(Height/Width)) and (Alpha < Pi/2) then begin
          Term:=(0.5*(Width*(Sin(Alpha)/Cos(Alpha))-Height))/BRad*Sin(Pi/2-Alpha);
          Beta:=ArcTan(Term/(1-Sqr(Term)));
          Gamma:=Alpha-Beta;
          PosX:=Round(PosX+BRad*Cos(Gamma)-Width);
          PosY:=Round(PosY+BRad*Sin(Gamma));
        end
        else if (Alpha >= Pi/2) and (Alpha < ArcTan(Height/-Width)+Pi) then begin
          Term:=(0.5*(Width*(Sin(Pi-Alpha)/Cos(Pi-Alpha))-Height))/BRad*Sin(-Pi/2+Alpha);
          Beta:=ArcTan(Term/(1-Sqr(Term)));
          Gamma:=Alpha+Beta;
          PosX:=Round(PosX+BRad*Cos(Gamma));
          PosY:=Round(PosY+BRad*Sin(Gamma));
        end
        else if (Alpha >= ArcTan(Height/-Width)+Pi) and (Alpha < Pi) then begin
          Term:=(0.5*(Height-Width*(Sin(Pi-Alpha)/Cos(Pi-Alpha))))/BRad*Sin(-Pi/2-Alpha);
          Beta:=ArcTan(Term/(1-Sqr(Term)));
          Gamma:=Alpha-Beta;
          PosX:=Round(PosX+BRad*Cos(Gamma));
          PosY:=Round(PosY+BRad*Sin(Gamma));
        end
        else if (Alpha >= Pi) and (Alpha < ArcTan(-Height/-Width)+Pi) then begin
          Term:=(0.5*(Height-Width*(Sin(Alpha-Pi)/Cos(Alpha-Pi))))/BRad*Sin(-Pi/2+Alpha);
          Beta:=ArcTan(Term/(1-Sqr(Term)));
          Gamma:=Alpha+Beta;
          PosX:=Round(PosX+BRad*Cos(Gamma));
          PosY:=Round(PosY+BRad*Sin(Gamma)+Height);
        end
        else if (Alpha >= ArcTan(-Height/-Width)+Pi) and (Alpha < 3/2*Pi) then begin
          Term:=(0.5*(Width*(Sin(Alpha-Pi)/Cos(Alpha-Pi))-Height))/BRad*Sin(-Pi/2-Alpha);
          Beta:=ArcTan(Term/(1-Sqr(Term)));
          Gamma:=Alpha-Beta;
          PosX:=Round(PosX+BRad*Cos(Gamma));
          PosY:=Round(PosY+BRad*Sin(Gamma)+Height);
        end
        else if (Alpha >= 3/2*Pi) and (Alpha < ArcTan(-Height/Width)+2*Pi) then begin
          Term:=(0.5*(Width*(Sin(-Alpha)/Cos(-Alpha))-Height))/BRad*Sin(Pi/2+Alpha);
          Beta:=ArcTan(Term/(1-Sqr(Term)));
          Gamma:=Alpha+Beta;
          PosX:=Round(PosX+BRad*Cos(Gamma)-Width);
          PosY:=Round(PosY+BRad*Sin(Gamma)+Height);
        end
        else if (Alpha >= ArcTan(-Height/Width)+2*Pi) and (Alpha < 2*Pi) then begin
          Term:=(0.5*(Height-Width*(Sin(-Alpha)/Cos(-Alpha))))/BRad*Sin(Pi/2-Alpha);
          Beta:=ArcTan(Term/(1-Sqr(Term)));
          Gamma:=Alpha-Beta;
          PosX:=Round(PosX+BRad*Cos(Gamma)-Width);
          PosY:=Round(PosY+BRad*Sin(Gamma)+Height);
        end
      end;

     if ArcAdv = true then
    begin;

       CurrentPoint.Init(PosX,PosY);
       TmpPoint.Init(Obje^.GivePosX,Obje^.GivePosY);
       Alpha:=TmpPOint.CalculateAngle(CurrentPoint);
//        if Alpha < 0 then Alpha:=Alpha+2*Pi;
       Circuit:=Round(TmpPoint.Dist(CurrentPoint));

      if ArcAdvDirection=1  //on arc
      then
      begin;
       PosX:=TmpPOint.X+round(Rad*Cos(Alpha));
       PosY:=TmpPOint.Y +round(Rad*Sin(Alpha));
      end;
         if ArcAdvDirection=2   //outside arc
      then
      begin;
       PosX:=TmpPOint.X+round((Rad+(Rad-Circuit))*(Cos(Alpha)));
       PosY:=TmpPOint.Y+round((Rad+(Rad-Circuit))*(Sin(Alpha)));
      end;
    end
    end

    else if SItem^.GetObjType = ot_Symbol then begin
      PosX:=Obje^.ClipRect.B.X + TextHeight Div 4;
      PosY:=Trunc((Obje^.ClipRect.B.Y-Obje^.ClipRect.A.Y)/2) + Obje^.ClipRect.A.Y + Height Div 2;
    end
    else if SItem^.GetObjType = ot_Pixel then begin
      PosX:=Obje^.GivePosX + TextHeight Div 4;
      PosY:=Obje^.GivePosY + Height Div 2;
    end
    else begin
      PosX:=Obje^.ClipRect.B.X + TextHeight Div 4;
      PosY:=Trunc((Obje^.ClipRect.B.Y-Obje^.ClipRect.A.Y)/2) + Obje^.ClipRect.A.Y + Height Div 2;
    end;

 if OffSetAdv=true
       then
        case  OffSetAdvDirection of
        9: begin; PosX:=PosX-Width;PosY:=PosY+Height;end;
        8: begin; PosX:=PosX-(Width div 2);PosY:=PosY+Height;end;
        7: begin; PosY:=PosY+Height; end;
        6: begin; PosX:=PosX-Width; PosY:=PosY+(Height div 2);end;
        5:  begin; PosX:=PosX-(Width div 2); PosY:=PosY+(Height div 2) end;
        4: begin; PosY:=PosY+(Height div 2) end;
        3: begin;PosX:=PosX-Width; end;
        2: begin;PosX:=PosX-(Width div 2); end;
        1: begin; end;
       end;
     
    end;
end;

{**************************************** CalculateBusGraphPos ***************************************************************}

Procedure CalculateBusGraphPos
   (
   SItem           : PIndex;
   Obje            : PView;
   var PosX        : LongInt;
   var PosY        : LongInt;
   Width           : LongInt;
   Height          : LongInt;
   BeamWidth       : LongInt;
   CDiameter       : LongInt;
   DType           : Byte
   );
  var Rad          : LongInt;
      Circuit      : LongInt;
      BeginAngle   : Real;
      EndAngle     : Real;
      Alpha        : Real;
      Beta         : Real;
      Gamma        : Real;
      BRad         : Real;
      Term         : Real;
  begin
    if SItem^.GetObjType = ot_Poly then begin
      if DType = dtCircleDiag then begin
        PosX:=Obje^.GivePosX + CDiameter Div 5;
        PosY:=Obje^.GivePosY + Height Div 2;
      end
      else begin
        PosX:=Obje^.GivePosX + BeamWidth Div 2;
        PosY:=Obje^.GivePosY + Height;
      end;
    end
    else if SItem^.GetObjType = ot_cPoly then begin
      PosX:=Obje^.GiveAPosX - Width Div 2;
      if DType = dtCircleDiag then
         PosY:=Obje^.GiveAPosY + Height Div 2
      else
         PosY:=Obje^.GiveAPosY + Height;
    end
    else if SItem^.GetObjType = ot_Circle then begin
      PosX:=Obje^.GivePosX;
      PosY:=Obje^.GivePosY;
      Rad:=Obje^.GiveRadius;
      Circuit:=Trunc(1.1*(Sqrt((Sqr(Width/2)+Sqr(Height/2)))));
      if Rad < Circuit then begin
        if DType = dtCircleDiag then begin
          PosX:=PosX + Rad + CDiameter Div 5;
          PosY:=PosY + Height Div 2;
        end
        else begin
          PosX:=PosX + Rad + BeamWidth Div 2;
          PosY:=PosY + Height;
        end;
      end
      else if Rad > 2*Circuit then begin
        if DType = dtCircleDiag then PosX:=PosX - Rad + CDiameter Div 5
        else PosX:=PosX - Rad + Width Div 5;
        PosY:=PosY + Height Div 2;
      end
      else begin
        PosX:=PosX - Width Div 2;
        PosY:=PosY + Height Div 2;
      end
    end
    else if SItem^.GetObjType = ot_Arc then begin
      PosX:=Obje^.GivePosX;
      PosY:=Obje^.GivePosY;
      Rad:=Obje^.GiveRadius;
      BeginAngle:=Obje^.GiveBegAngle;
      EndAngle:=Obje^.GiveEndAngle;
      Circuit:=Trunc(1.1*(Sqrt((Sqr(Width/2)+Sqr(Height/2)))));
      Alpha:=BeginAngle+(EndAngle-BeginAngle)/2;
      if BeginAngle > EndAngle then Alpha:=Alpha-Pi;
      if Alpha < 0 then Alpha:=Alpha+2*Pi;
      BRad:=Rad*0.9;
      if Rad < Circuit then begin
        if BeginAngle > EndAngle then begin
          PosX:=PosX + Rad + Width Div 5;
          if DType = dtCircleDiag then PosY:=PosY + Height Div 2
          else PosY:=PosY + Height;
        end
        else begin
          if BeginAngle < 2*Pi-EndAngle then begin
            PosX:=Round(PosX + Rad*cos(BeginAngle) + Width Div 5);
            if DType = dtCircleDiag then PosY:=Round(PosY + Rad*sin(BeginAngle) + Height Div 2)
            else PosY:=Round(PosY + Rad*sin(BeginAngle) + Height);
          end
          else if BeginAngle > 2*Pi-EndAngle then begin
            PosX:=Round(PosX + Rad*cos(EndAngle) + Width Div 5);
            if DType = dtCircleDiag then PosY:=Round(PosY + Rad*sin(EndAngle) + Height Div 2)
            else PosY:=Round(PosY + Rad*sin(EndAngle) + Height);
          end
          else begin
            PosX:=Round(PosX + Rad*cos(BeginAngle) + Width Div 5);
            PosY:=Round(PosY + Height Div 2);
          end;
        end;
      end
      else begin
        if (Alpha >= 0) and (Alpha < ArcTan(Height/Width)) then begin
          Term:=(0.5*(Height-Width*(Sin(Alpha)/Cos(Alpha))))/BRad*Sin(Pi/2+Alpha);
          Beta:=ArcTan(Term/(1-Sqr(Term)));
          Gamma:=Alpha+Beta;
          PosX:=Round(PosX+BRad*Cos(Gamma)-Width);
          PosY:=Round(PosY+BRad*Sin(Gamma));
        end
        else if (Alpha >= ArcTan(Height/Width)) and (Alpha < Pi/2) then begin
          Term:=(0.5*(Width*(Sin(Alpha)/Cos(Alpha))-Height))/BRad*Sin(Pi/2-Alpha);
          Beta:=ArcTan(Term/(1-Sqr(Term)));
          Gamma:=Alpha-Beta;
          PosX:=Round(PosX+BRad*Cos(Gamma)-Width);
          PosY:=Round(PosY+BRad*Sin(Gamma));
        end
        else if (Alpha >= Pi/2) and (Alpha < ArcTan(Height/-Width)+Pi) then begin
          Term:=(0.5*(Width*(Sin(Pi-Alpha)/Cos(Pi-Alpha))-Height))/BRad*Sin(-Pi/2+Alpha);
          Beta:=ArcTan(Term/(1-Sqr(Term)));
          Gamma:=Alpha+Beta;
          PosX:=Round(PosX+BRad*Cos(Gamma));
          PosY:=Round(PosY+BRad*Sin(Gamma));
        end
        else if (Alpha >= ArcTan(Height/-Width)+Pi) and (Alpha < Pi) then begin
          Term:=(0.5*(Height-Width*(Sin(Pi-Alpha)/Cos(Pi-Alpha))))/BRad*Sin(-Pi/2-Alpha);
          Beta:=ArcTan(Term/(1-Sqr(Term)));
          Gamma:=Alpha-Beta;
          PosX:=Round(PosX+BRad*Cos(Gamma));
          PosY:=Round(PosY+BRad*Sin(Gamma));
        end
        else if (Alpha >= Pi) and (Alpha < ArcTan(-Height/-Width)+Pi) then begin
          Term:=(0.5*(Height-Width*(Sin(Alpha-Pi)/Cos(Alpha-Pi))))/BRad*Sin(-Pi/2+Alpha);
          Beta:=ArcTan(Term/(1-Sqr(Term)));
          Gamma:=Alpha+Beta;
          PosX:=Round(PosX+BRad*Cos(Gamma));
          PosY:=Round(PosY+BRad*Sin(Gamma)+Height);
        end
        else if (Alpha >= ArcTan(-Height/-Width)+Pi) and (Alpha < 3/2*Pi) then begin
          Term:=(0.5*(Width*(Sin(Alpha-Pi)/Cos(Alpha-Pi))-Height))/BRad*Sin(-Pi/2-Alpha);
          Beta:=ArcTan(Term/(1-Sqr(Term)));
          Gamma:=Alpha-Beta;
          PosX:=Round(PosX+BRad*Cos(Gamma));
          PosY:=Round(PosY+BRad*Sin(Gamma)+Height);
        end
        else if (Alpha >= 3/2*Pi) and (Alpha < ArcTan(-Height/Width)+2*Pi) then begin
          Term:=(0.5*(Width*(Sin(-Alpha)/Cos(-Alpha))-Height))/BRad*Sin(Pi/2+Alpha);
          Beta:=ArcTan(Term/(1-Sqr(Term)));
          Gamma:=Alpha+Beta;
          PosX:=Round(PosX+BRad*Cos(Gamma)-Width);
          PosY:=Round(PosY+BRad*Sin(Gamma)+Height);
        end
        else if (Alpha >= ArcTan(-Height/Width)+2*Pi) and (Alpha < 2*Pi) then begin
          Term:=(0.5*(Height-Width*(Sin(-Alpha)/Cos(-Alpha))))/BRad*Sin(Pi/2-Alpha);
          Beta:=ArcTan(Term/(1-Sqr(Term)));
          Gamma:=Alpha-Beta;
          PosX:=Round(PosX+BRad*Cos(Gamma)-Width);
          PosY:=Round(PosY+BRad*Sin(Gamma)+Height);
        end
      end
    end
    else if SItem^.GetObjType = ot_Symbol then begin
      if DType = dtCircleDiag then begin
        PosX:=Obje^.ClipRect.B.X + CDiameter Div 5;
        PosY:=Trunc((Obje^.ClipRect.B.Y-Obje^.ClipRect.A.Y)/2) + Obje^.ClipRect.A.Y + Height Div 2;
      end
      else begin
        PosX:=Obje^.ClipRect.B.X + BeamWidth Div 2;
        PosY:=Trunc((Obje^.ClipRect.B.Y-Obje^.ClipRect.A.Y)/2) + Obje^.ClipRect.A.Y + Height;
      end;
    end
    else if SItem^.GetObjType = ot_Pixel then begin
      if DType = dtCircleDiag then begin
        PosX:=Obje^.GivePosX + CDiameter Div 5;
        PosY:=Obje^.GivePosY + Height Div 2;
      end
      else begin
        PosX:=Obje^.GivePosX + BeamWidth Div 2;
        PosY:=Obje^.GivePosY + Height;
      end;
    end
    else begin
      if DType = dtCircleDiag then begin
        PosX:=Obje^.ClipRect.B.X + CDiameter Div 5;
        PosY:=Trunc((Obje^.ClipRect.B.Y-Obje^.ClipRect.A.Y)/2) + Obje^.ClipRect.A.Y + Height Div 2;
      end
      else begin
        PosX:=Obje^.ClipRect.B.X + BeamWidth Div 2;
        PosY:=Trunc((Obje^.ClipRect.B.Y-Obje^.ClipRect.A.Y)/2) + Obje^.ClipRect.A.Y + Height;
      end;
    end;
  end;

{*****************************************************************************************************************************}
{**************************************** TBusGraphText **********************************************************************}
{*****************************************************************************************************************************}

Constructor TBusGraphText.Init;
begin
  inherited Init;
  LineColl:=New(PStrCollection,Init(5,5));
  TextPos.Init;
  MaxLen:=0;
  DiagSize:=0;
  DefaultType:=0;
  DefMarkVis:=False;
  DefMarkType:=Integer(smsLabelValue);
  DefMarkFont:='Arial';
  DefMarkSize:=8;
  Def3D:=False;
end;

{*****************************************************************************************************************************}

Destructor TBusGraphText.Done;
begin
  LineColl^.FreeAll;
  Dispose(LineColl,Done);
  inherited Done;
end;

{*****************************************************************************************************************************}

Procedure TBusGraphText.InsertString(StrLine:PChar);
begin
  LineColl^.Insert(StrNew(StrLine));
  if StrLen(StrLine)+1 > MaxLen then MaxLen:=StrLen(StrLine)+1;
end;


{**************************************** DBText *****************************************************************************}

Procedure TBusGraphText.DBText
   (
   AData           : PProj
   );
{$IFNDEF WMLT}
  {$IFNDEF AXDLL} // <----------------- AXDLL
  var NameColl     : PCollection;
      AFont        : TFontData;
      TextHeight   : LongInt;
      WriteTo      : PLayer;
      OldTop       : PLayer;
      TempStr      : PChar;
      NewPos       : Bool;
      Cha          : Integer;
      ResStr       : String;
      ResLInt      : LongInt;
      SFound       : Boolean;
      MStrs        : Boolean;
      StrToLong    : Boolean;
      StrOK        : Boolean;
      DoIt         : Boolean;
      TmpZoom      : Double;
{**************************************** Einzelne Texte erzeugen ****************************************}
  Procedure DoAll
     (
     Item              : PChar
     ); Far;
    var AText          : PRText;
        PosX           : LongInt;
        PosY           : LongInt;
        Cha            : Integer;
        TextRect       : TRect;
        ID             : LongInt;
        LastRow        : PChar;
        SItem          : PIndex;
        Obje           : PView;
        Err            : Integer;
        Width          : LongInt;
        Height         : LongInt;
        OldFont        : HFont;
        TextFont       : HFont;
        DispTextHeight : LongInt;
        z              : Integer;
        DispWidth      : LongInt;
        DispHeight     : LongInt;
        AFontDes       : PFontDes;
{**************************************** Textdaten erzeugen ****************************************}
    Procedure DoAll1
       (
       Item1        : PAnnot                      { Spaltenname }
       ); Far;
      var TempValue : Array[0..255] of Char;
          TempItem  : PChar;
      begin
        TempItem:=NextDDEPart(Item);
        TempItem:=NextDDEPart(TempItem);
        TempItem:=NextDDEPart(TempItem);
        if DDEHandler.FGotLInfo <> 0 then TempItem:=NextDDEPart(TempItem);
        while TempItem<>NIL do begin
          ReadDDEText(TempItem,DDEHandler.FDDESepSign,TempValue);
          if StrComp(TempValue,Item1^.LineName)=0 then begin
            ReadDDEText(TempItem,DDEHandler.FDDESepSign,TempValue);
            if StrComp(TempValue,'')=0 then Item1^.HasValue:=FALSE
            else begin
              Item1^.HasValue:=TRUE;
              StrCat(TempStr,TempValue);
              StrCat(TempStr,#10#13);
            end;
            Break;
          end
          else ReadDDEText(TempItem,DDEHandler.FDDESepSign,TempValue);
        end;
      end;
{**************************************** Texterzeugung ****************************************}
    begin
      TmpZoom:=AData^.PInfo^.Zoom;
      AData^.PInfo^.SetZoom(100);
      z:=0;
      if AData^.PInfo^.CalculateDisp(TextHeight) > 500000 then begin
        DispTextHeight:=Trunc(TextHeight/10000);
        z:=1;
      end
      else if AData^.PInfo^.CalculateDisp(TextHeight) > 5000 then begin
        DispTextHeight:=Trunc(TextHeight/100);
        z:=2;
      end
      else DispTextHeight:=TextHeight;
      {TextFont:=AData^.PInfo^.GetFont(AData^.ActualFont,AData^.PInfo^.CalculateDisp(DispTextHeight),0);}
      TextFont:=AData^.PInfo^.GetFont(AData^.PInfo^.AnnotSettings.AnnotFont,AData^.PInfo^.CalculateDisp(DispTextHeight),0);
      {TextFont:=AData^.PInfo^.GetFont(AFont,AData^.PInfo^.CalculateDisp(DispTextHeight),0);}
      if TextFont > 0 then begin
        TextRect.Left:=0;
        TextRect.Top:=0;
        if DDEHandler.FGotLInfo = 0 then Cha:=6
        else if (DDEHandler.FGotLInfo = 1) or (DDEHandler.FGotLInfo = 2) then Cha:=GetSepPos(Item,']',2,0);
        Val(StrLCopy(TempStr,Item+Cha,10),ID,Err);
        SItem:=New(PIndex,Init(ID));
        Obje:=PLayer(AData^.PInfo^.Objects)^.IndexObject(AData^.PInfo,SItem);
        if Obje <> NIL then begin
          StrCopy(TempStr,'');
          NameColl^.ForEach(@DoAll1);
          if StrComp(TempStr,'') <> 0 then begin
            LastRow:=StrRScan(TempStr,#10);
            if LastRow <> NIL then StrCopy(LastRow,#0);
            OldFont:=SelectObject(AData^.PInfo^.ExtCanvas.Handle,TextFont);
            DrawText(DC,TempStr,StrLen(TempStr),TextRect,dt_CalcRect);
            case z of
              1  : begin
                     DispWidth:=LongInt(TextRect.Right)*10000;
                     DispHeight:=LongInt(TextRect.Bottom)*10000;
                   end;
              2  : begin
                     DispWidth:=LongInt(TextRect.Right)*100;
                     DispHeight:=LongInt(TextRect.Bottom)*100;
                   end;
              else begin
                     DispWidth:=TextRect.Right;
                     DispHeight:=TextRect.Bottom;
                   end;
            end;
            {AFontDes:=AData^.PInfo^.Fonts^.GetFont(AData^.ActualFont.Font);}
            AFontDes:=AData^.PInfo^.Fonts^.GetFont(AData^.PInfo^.AnnotSettings.AnnotFont.Font);
            {AFontDes:=AData^.PInfo^.Fonts^.GetFont(AFont.Font);}
            if (AFontDes<>NIL) and AFontDes^.IsVertical then begin
              Width:=DispWidth;
              DispWidth:=DispHeight;
              DispHeight:=Width;
            end;
            if DispWidth/AData^.PInfo^.Zoom <= MaxLongInt then begin
              Width:=AData^.PInfo^.CalculateDraw(DispWidth);
              Height:=AData^.PInfo^.CalculateDraw(Abs(DispHeight));
              CalculateTextPos(SItem,Obje,PosX,PosY,Width,Height,TextHeight);
              TextPos.Assign(PosX,PosY,PosX+Width,PosY+Height);
              {AText:=New(PRText,InitCalculate(AData^.PInfo,AData^.ActualFont,NameColl,TempStr,TextPos,ID));}
              AText:=New(PRText,InitCalculate(AData^.PInfo,AData^.PInfo^.AnnotSettings.AnnotFont,NameColl,TempStr,TextPos,ID));
              {AText:=New(PRText,InitCalculate(AData^.PInfo,AFont,NameColl,TempStr,TextPos,ID));}
              if AData^.InsertObject(AText,False) then {sl}
                AData^.CorrectSize(AText^.ClipRect,False);
            end
            else MsgBox(AData^.Parent.Handle,10405,mb_IconExclamation or mb_OK,'');
            SelectObject(AData^.PInfo^.ExtCanvas.Handle,OldFont);
            DeleteObject(TextFont);
          end;
        end;
        Dispose(SItem,Done);
      end;
      AData^.PInfo^.SetZoom(TmpZoom);
    end;
{**************************************** Spaltenliste ****************************************}
  Procedure MakeColumnList
     (
     Item          : PChar
     ); Far;
    var Annotation : PAnnot;
        Value      : Array[0..255] of Char;
        FieldNr    : Integer;
    begin
      FieldNr:=0;
      if DDEHandler.FGotLInfo = 0 then Cha:=18
      else if (DDEHandler.FGotLInfo = 1) or (DDEHandler.FGotLInfo = 2) then Cha:=GetSepPos(Item,']',3,0);
      Item:=Item+Cha;
      while Item <> NIL do begin
        ReadDDEText(Item,DDEHandler.FDDESepSign,Value);
        Inc(FieldNr);
        if (StrComp(Value,'') <> 0) and (FieldNr mod 2 = 1) then begin
          Annotation:=New(PAnnot);
          Annotation^.LineName:=StrNew(Value);
          Annotation^.LineValue:=NIL;
          Annotation^.HasValue:=TRUE;
          NameColl^.Insert(Annotation);
        end;
      end;
    end;
{**************************************** Hauptteil ****************************************}
  {$ENDIF} // <----------------- AXDLL
{$ENDIF}
  begin
{$IFNDEF WMLT}
  {$IFNDEF AXDLL} // <----------------- AXDLL
    WriteTo:=NIL;
    DoIt:=FALSE;
    if DDEHandler.FGotLInfo = 0 then begin
      if AData^.PInfo^.AnnotSettings.AnnotLayer > 0 then
        WriteTo:=AData^.Layers^.IndexToLayer(AData^.PInfo^.AnnotSettings.AnnotLayer);
      if (WriteTo = NIL) or (WriteTo^.SortIndex = 0) or
         (WriteTo^.GetState(sf_LayerOff)) or (WriteTo^.GetState(sf_Fixed)) then begin
        WriteTo:=AData^.Layers^.TopLayer;
        DoIt:=TRUE;
      end;
    end
    else if DDEHandler.FGotLInfo = 1 then begin
      StrOK:=ReadDDECommandString(LineColl^.At(0),1,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ResStr,StrToLong,MStrs);
      if StrOK and (ResStr <> '') then begin
        WriteTo:=AData^.Layers^.NameToLayer(ResStr);
        if (WriteTo = NIL) or (WriteTo^.SortIndex = 0) or
           (WriteTo^.GetState(sf_LayerOff)) or (WriteTo^.GetState(sf_Fixed)) then begin
          DoIt:=TRUE;
          if AData^.PInfo^.AnnotSettings.AnnotLayer > 0 then
            WriteTo:=AData^.Layers^.IndexToLayer(AData^.PInfo^.AnnotSettings.AnnotLayer);
          if (WriteTo = NIL) or (WriteTo^.SortIndex = 0) or
             (WriteTo^.GetState(sf_LayerOff)) or (WriteTo^.GetState(sf_Fixed)) then
            WriteTo:=AData^.Layers^.TopLayer;
        end;
      end
      else begin
        if AData^.PInfo^.AnnotSettings.AnnotLayer > 0 then
          WriteTo:=AData^.Layers^.IndexToLayer(AData^.PInfo^.AnnotSettings.AnnotLayer);
        if (WriteTo = NIL) or (WriteTo^.SortIndex = 0) or
           (WriteTo^.GetState(sf_LayerOff)) or (WriteTo^.GetState(sf_Fixed)) then begin
          WriteTo:=AData^.Layers^.TopLayer;
          DoIt:=TRUE;
        end;
      end;
    end
    else if DDEHandler.FGotLInfo = 2 then begin
      if ReadDDECommandLongInt(LineColl^.At(0),1,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ResLInt,SFound,MStrs) then begin
        WriteTo:=AData^.Layers^.IndexToLayer(ResLInt);
        if (WriteTo = NIL) or (WriteTo^.SortIndex = 0) or
           (WriteTo^.GetState(sf_LayerOff)) or (WriteTo^.GetState(sf_Fixed)) then begin
          DoIt:=TRUE;
          if AData^.PInfo^.AnnotSettings.AnnotLayer > 0 then
            WriteTo:=AData^.Layers^.IndexToLayer(AData^.PInfo^.AnnotSettings.AnnotLayer);
          if (WriteTo = NIL) or (WriteTo^.SortIndex = 0) or
             (WriteTo^.GetState(sf_LayerOff)) or (WriteTo^.GetState(sf_Fixed)) then
            WriteTo:=AData^.Layers^.TopLayer;
        end;
      end
      else begin
        if AData^.PInfo^.AnnotSettings.AnnotLayer > 0 then
          WriteTo:=AData^.Layers^.IndexToLayer(AData^.PInfo^.AnnotSettings.AnnotLayer);
        if (WriteTo = NIL) or (WriteTo^.SortIndex = 0) or
           (WriteTo^.GetState(sf_LayerOff)) or (WriteTo^.GetState(sf_Fixed)) then begin
          WriteTo:=AData^.Layers^.TopLayer;
          DoIt:=TRUE;
        end;
      end;
    end;
    if WriteTo = NIL then MsgBox(AData^.Parent.Handle,11027,mb_Ok+mb_IconExclamation,'')
    else begin
      GetMem(TempStr,MaxLen);
      NameColl:=New(PCollection,Init(5,5));
      MakeColumnList(LineColl^.At(0));
      NewPos:=FALSE;                                                                               { hier nicht verwendet }
      with AData^.PInfo^.AnnotSettings do begin
        if DoIt or ShowDialog or (AData^.PInfo^.Fonts^.GetFont(AnnotFont.Font) = NIL) or
           (AnnotFont.Height <= 0) then begin
          if ExecDialog(TDSelFontLayer.Init(AData^.Parent,AData^.Layers,
              @WriteTo,NameColl,AData^.PInfo,@AnnotFont,
              @NewPos,'SelFontLayer',TRUE)) = id_OK then DoIt:=TRUE
          else DoIt:=FALSE;
        end
        else DoIt:=TRUE;
      end;
      if DoIt then begin
        with AData^ do begin

          DeselectAll(TRUE);
          PInfo^.AnnotSettings.AnnotLayer:=WriteTo^.Index;
          {AFont.Init;

          AFont:=AData^.PInfo^.AnnotSettings.AnnotFont;}

          {TextHeight:=AData^.ActualFont.Height;}
          TextHeight:=AData^.PInfo^.AnnotSettings.AnnotFont.Height;

          AData^.RedrawAfterInsert:=FALSE;
          OldTop:=AData^.Layers^.TopLayer;
          AData^.Layers^.TopLayer:=WriteTo;
          LineColl^.ForEach(@DoAll);
          AData^.Layers^.TopLayer:=OldTop;
          AData^.RedrawAfterInsert:=TRUE;
          if AData^.BorderCorrected then begin
{!?!?!?            ACursor:=AData^.ActualCur;
            if AData^.OverView then begin
              AData^.SetOverview;
              AData^.PInfo^.RedrawScreen(TRUE);
            end;
            AData^.SetCursor(ACursor);
            AData^.BorderCorrected:=FALSE;}
          end;
          {AFont.Done;}
        end;
      end;
      Cha:=0;
      while Cha < NameColl^.Count do begin
        StrDispose(PAnnot(NameColl^.At(Cha))^.LineName);
        StrDispose(PAnnot(NameColl^.At(Cha))^.LineValue);
        Inc(Cha);
      end;
      NameColl^.DeleteAll;
      Dispose(NameColl,Done);
      FreeMem(TempStr,MaxLen);
    end;
    LineColl^.FreeAll;
    MaxLen:=0;
  {$ENDIF} // <----------------- AXDLL
{$ENDIF}
  end;

{**************************************** DBGraphic **************************************************************************}

Procedure TBusGraphText.DBGraphic
   (
   AData              : PProj;
   Flag:boolean=false;
   IDBLayer:Player=nil);
{$IFNDEF WMLT}
  {$IFNDEF AXDLL} // <----------------- AXDLL
  var NameColl        : PCollection;
      AType           : Byte;
      ABeamHeight     : LongInt;
      ABeamWidth      : LongInt;
      ACircleDiameter : LongInt;
      WriteTo         : PLayer;
      OldTop          : PLayer;
      TempStr         : PChar;
      Cha             : Integer;
      ShowValues      : Bool;
      ValAbsRel       : Bool;
      CDDiamAbsRel    : Bool;
      AFont           : TFontData;
      NewType         : Byte;
//      PLAColor        : LongInt;
      PLAPen          : Word;
      PLAPattern      : Word;
      ResStr          : String;
      ResLInt         : LongInt;
      SFound          : Boolean;
      MStrs           : Boolean;
      StrToLong       : Boolean;
      StrOK           : Boolean;
      i               : Integer;
      grr             : TGrRect;
      grp             : TGrPoint;
      NewKind         : PChartKind;
      NewDiagType     : Byte;
      OldPal          : TRGBColors;
      bProp           : Boolean;
      {brovak}
       TotalCount :longint;
       NowCount   :longint;
      {brovak}


{**************************************** Summen ermitteln ****************************************}

  procedure DoAllSum(Item: PChar);
  var
  Sum: Real;
  Error: Integer;

{**************************************** Diagrammdaten erzeugen ****************************************}

    Procedure DoAll1              { Collection mit Namen, Werten, Farb- und Musterinfos erzeugen }
       (
       Item1            : PNameColl
       ); Far;
      var TempName      : Array[0..255] of Char;
          TempItem      : PChar;
          Value         : String;
          AValue        : PDGraph;
          ASize         : Real;
      begin
        TempItem:=NextDDEPart(NextDDEPart(NextDDEPart(Item)));
        if (DDEHandler.FGotLInfo <> 0) or (Flag=true) then TempItem:=NextDDEPart(TempItem);
        while TempItem <> NIL do begin
          ReadDDEString(TempItem,DDEHandler.FDDESepSign,Value);
          if Value = PNameColl(Item1)^.Name^ then begin
            ReadDDEString(TempItem,DDEHandler.FDDESepSign,Value);
            Value:=StringReplace(Value,',',DecimalSeparator,[rfReplaceAll]);
            Value:=StringReplace(Value,'.',DecimalSeparator,[rfReplaceAll]);
            if Value = '' then ASize:=0
            else Val(Value,ASize,Error);
            if Error = 0 then with PNameColl(Item1)^ do begin
              Sum:=Sum+ASize;
            end;
            Break;
          end
          else ReadDDEString(TempItem,DDEHandler.FDDESepSign,Value);
        end;
      end;
{****************************************************************************************************}

  begin
        Sum:=0;
        NameColl^.ForEach(@DoAll1);
  end;

{**************************************** Einzelne Diagramme erzeugen ****************************************}
  function DoAll
     (
     Item               : PChar
     ): Boolean;
    var Values          : PCollection;
        Error           : Integer;
        Sum             : Real;
        GraphPos        : TDPoint;
        ABusGraph       : PBusGraph;
        PosX            : LongInt;
        PosY            : LongInt;
        OldPosX         : LongInt;
        OldPosY         : LongInt;
        ID              : LongInt;
        SItem           : PIndex;
        Obje            : PView;
        Width           : LongInt;
        Height          : LongInt;
        Cols            : Integer;
        MaxVal          : Real;
        AAType          : Byte;
        BCircleDiameter : LongInt;
        CreateIt        : Boolean;
        THeight         : LongInt;
        MaxTWidth       : LongInt;
        TempStr         : array[0..255] of Char;
        ProjID          : LongInt;
        DBID            : array[0..255] of Char;
{**************************************** Diagrammdaten erzeugen ****************************************}
    Procedure DoAll1              { Collection mit Namen, Werten, Farb- und Musterinfos erzeugen }
       (
       Item1            : PNameColl
       ); Far;
      var TempName      : Array[0..255] of Char;
          TempItem      : PChar;
          Value         : String;
          AValue        : PDGraph;
          ASize         : Real;
      begin
        TempItem:=NextDDEPart(NextDDEPart(NextDDEPart(Item)));
        if (DDEHandler.FGotLInfo <> 0) or (Flag=true) then TempItem:=NextDDEPart(TempItem);
        while TempItem <> NIL do begin
          ReadDDEString(TempItem,DDEHandler.FDDESepSign,Value);
          if Value = PNameColl(Item1)^.Name^ then begin
            ReadDDEString(TempItem,DDEHandler.FDDESepSign,Value);
            Value:=StringReplace(Value,',',DecimalSeparator,[rfReplaceAll]);
            Value:=StringReplace(Value,'.',DecimalSeparator,[rfReplaceAll]);
            if Value = '' then ASize:=0
            else Val(Value,ASize,Error);
            if Error = 0 then with PNameColl(Item1)^ do begin
              Sum:=Sum+ASize;
              AValue:=New(PDGraph,Init(StrNew(StrPCopy(TempName,Name^)),ASize,BusGraphColors[Cols mod MAXBGCOL],Pen,Pattern));
              Values^.Insert(AValue);
              Inc(Cols);
              if ASize > MaxVal then MaxVal:=ASize;
            end;
            Break;
          end
          else ReadDDEString(TempItem,DDEHandler.FDDESepSign,Value);
        end;
      end;
{**************************************** gr��te Beschriftungsl�nge ****************************************}
    Procedure DoAll2
       (
       Item2            : PDGraph
       ); Far;
      var TText         : String;
          PercentVal    : Real;
          TWidth        : LongInt;
      begin
        if ValAbsRel then Str(Item2^.Value:0:0,TText)
        else begin
          PercentVal:=100/Sum*Item2^.Value;
          Str(PercentVal:0:2,TText);
          TText:=TText+' %';
        end;
        TWidth:=Round(THeight*AData^.PInfo^.GetTextRatio(AFont,TText));
        if TWidth > MaxTWidth then MaxTWidth:=TWidth;
      end;

{******************************************************************************}
    function GetPropSize: Integer;
    var
    ARes: Double;
    begin
         ARes:=Sum*NewKind^.PropFactor;
         if NewKind^.PropMode = 1 then begin
            ARes:=Log10(ARes);
         end
         else if NewKind^.PropMode = 2 then begin
            ARes:=Sqrt(ARes);
         end;
         ARes:=Max(ARes,1);
         if ARes > MaxInt then begin
            ARes:=MaxInt;
         end;
         Result:=Trunc(ARes);
    end;
{**************************************** Diagrammerzeugung ****************************************}
    begin
      Result:=False;
      Sum:=0;
      Cols:=0;
      MaxVal:=0;
      DBID:='0';
      if Flag=true then Cha:=GetSepPos(Item,']',2,0)
      else
      if DDEHandler.FGotLInfo = 0 then Cha:=6
      else if (DDEHandler.FGotLInfo = 1) or (DDEHandler.FGotLInfo = 2) then Cha:=GetSepPos(Item,']',2,0);
      val(StrLCopy(TempStr,Item+Cha,10),ID,Error);
      SItem:=New(PIndex,Init(ID));
      Obje:=PLayer(AData^.PInfo^.Objects)^.IndexObject(AData^.PInfo,SItem);
      if Obje<>NIL then begin
        Values:=New(PCollection,Init(5,5));
        NameColl^.ForEach(@DoAll1);
        AAType:=AType;
        CreateIt:=TRUE;

        {
        if ((AAType = dtCircleDiag) and PlusMinus(Values))
            or ((AAType = dtCircleDiag) and (Sum = 0)) then begin
          case NewType of
            ntCDNot,
            ntCDAsBD   : begin
                           //ExecDialog(TDColTypeDlg.Init(AData^.Parent,AType,@NewType));
                           NewType := ntCDNot;
                           if (NewType = ntCDNot) or (NewType = ntCDsNever) then begin
                             Values^.FreeAll;
                             CreateIt:=FALSE;
                           end
                           else begin
                             AAType:=dtBeamDiag;
                             ExecDialog(TDCTSizeDlg.Init(AData^.Parent,@ABeamHeight,@ABeamWidth));
                             with AData^ do begin
                               if (BeamWidth <> ABeamWidth) or (BeamHeight <> ABeamHeight) then begin
                                 BeamWidth:=ABeamWidth;
                                 BeamHeight:=ABeamHeight;
                                 IniFile^.WriteDiagOpts(DiagramType,BeamWidth,BeamHeight,CircleDiameter,
                                                        CircDiagAbsRel,DiagAnnot,DAnnotAbsRel);
                               end;
                             end;
                           end;
                         end;
            ntCDsNever : begin
                           Values^.FreeAll;
                           CreateIt:=FALSE;
                         end;
            ntCDsAsBDs : AAType:=dtBeamDiag;
          end;
        end;
        if ((AAType = dtAreaDiag) and PlusMinus(Values)) then begin
          case NewType of
            ntCDNot,
            ntCDAsBD   : begin
                           //ExecDialog(TDColTypeDlg.Init(AData^.Parent,AType,@NewType));
                           Newtype:=ntCDNot;
                           if (NewType = ntCDNot) or (NewType = ntCDsNever) then begin
                             Values^.FreeAll;
                             CreateIt:=FALSE;
                           end
                           else begin
                             AAType:=dtLineDiag;
                           end;
                         end;
            ntCDsNever : begin
                           Values^.FreeAll;
                           CreateIt:=FALSE;
                         end;
            ntCDsAsBDs : AAType:=dtLineDiag;
          end;
        end;
        }

        if Sum = 0 then CreateIt:=False;

        if CreateIt then begin
          if AAType = dtCircleDiag then begin
            if CDDiamAbsRel then BCircleDiameter:=ACircleDiameter
            else begin
              BCircleDiameter:=Round(ACircleDiameter*Sum/100);
              AFont.Height:=Trunc(BCircleDiameter/5);
            end;
          end;
          if AAType = dtCircleDiag then begin
            if ShowValues then begin
              MaxTWidth:=0;
              THeight:=AFont.Height;
              Values^.ForEach(@DoAll2);
              Width:=BCircleDiameter+2*MaxTWidth;
              Height:=BCircleDiameter+2*THeight;
            end
            else begin
              Width:=BCircleDiameter;
              Height:=BCircleDiameter;
            end;
          end
          else if AAType = dtBeamDiag then begin
            Width:=ABeamWidth*Cols;
            Height:=Round(ABeamHeight/100*MaxVal);
          end
          else begin
            Width:=ABeamWidth*Cols;
            Height:=Round(ABeamHeight/100*MaxVal+ABeamWidth/5);
          end;

          CalculateBusGraphPos(SItem,Obje,PosX,PosY,Width,Height,ABeamWidth,BCircleDiameter,AAType);

          PosX:=Trunc((Obje^.ClipRect.a.x+Obje^.ClipRect.b.x)/2);
          PosY:=Trunc((Obje^.ClipRect.a.y+Obje^.ClipRect.b.y)/2);
          Width:=Trunc(Abs(Obje^.ClipRect.b.x-Obje^.ClipRect.a.x)/2);
          Height:=Trunc(Abs(Obje^.ClipRect.b.y-Obje^.ClipRect.a.y)/2);
          Width:=Trunc((Width+Height)/2);
          Height:=Width;

          if NewKind <> nil then begin
             NewDiagType:=NewKind^.DiagType;
             if NewKind^.bProp then begin
                 DiagSize:=GetPropSize;
             end;
          end;

        OldPosX:=PosX;
        OldPosY:=PosY;

        if DiagSize <> 0 then begin
             Width:=DiagSize;
             Height:=DiagSize;
             
             PosX:=PosX-Trunc(Width/2);
             PosY:=PosY+Trunc(Height/2);
        end
        else begin
          if (Obje^.GetObjType = ot_Poly) and (PPoly(Obje)^.Data^.Count >= 2) then begin
             if PPoly(Obje)^.Data^.Count mod 2 <> 0 then begin
                i:=(PPoly(Obje)^.Data^.Count div 2);
                PosX:=TDPoint(PPoly(Obje)^.Data^.At(i)^).x;
                PosY:=TDPoint(PPoly(Obje)^.Data^.At(i)^).y;
             end
             else begin
                i:=(PPoly(Obje)^.Data^.Count div 2);
                grr.left:=TDPoint(PPoly(Obje)^.Data^.At(i-1)^).x;
                grr.bottom:=TDPoint(PPoly(Obje)^.Data^.At(i-1)^).y;
                grr.right:=TDPoint(PPoly(Obje)^.Data^.At(i)^).x;
                grr.top:=TDPoint(PPoly(Obje)^.Data^.At(i)^).y;
                grp:=RectCenter(grr);
                PosX:=Trunc(grp.x);
                PosY:=Trunc(grp.y);
             end;
          end;

          if Obje^.GetObjType <> ot_Pixel then begin
             PosX:=PosX-Trunc(Width/2);
             PosY:=PosY+Trunc(Height/2);
          end
          else begin
             Height:=Max(Height,1);
             Width:=Max(Width,1);
             Height:=Height*100;
             Width:=Width*100;
          end;
        end;

          GraphPos.Init(PosX,PosY);

          ABusGraph:=New(PBusGraph,Init(AData^.PInfo,NewDiagType,Values,Sum,GraphPos,ABeamWidth,ABeamHeight,
                                        ACircleDiameter,Width,Height,CDDiamAbsRel,ShowValues,ValAbsRel,
                                        0,PLAPen,PLAPattern,AFont,ID,FALSE));

          if ABusGraph^.ChartKindPtr = nil then
             ABusGraph^.ChartKindPtr:=New(PChartKind,
             InitKind(ABusGraph^.DiagType,100,100,Def3D,
                                   True,bvNone,bvNone,1,bsNone,0,
                                   5,5,5,5,TRUE,False,False,
                                   'Arial',DEFAULT_CHARSET,THeight,
                                   False,False,False,False,False,0,talNone,0,1,
                                   False,False,False,False,False,0,talNone,0,1,
                                   DefMarkVis,TSeriesMarksStyle(DefMarkType),DefMarkFont,ANSI_CHARSET,DefMarkSize,
                                   False,$80000018,True,1,$808080));

          if ABusGraph^.ChartHeight = 0 then
             ABusGraph^.ChartHeight:=Height;

          if ABusGraph^.ChartWidth = 0 then
             ABusGraph^.ChartWidth:=Width;

          if AData^.InsertObject(ABusGraph,False) then begin
            if NewKind = nil then begin
               NewKind:=ProcSetBusGraph(AData,PIndex(ABusGraph),nil);
               NewKind:=BusGraphics.ProcChangeBusGraph(AData,PIndex(ABusGraph),NewKind,False);
               if NewKind = nil then begin
                  try
                     AData^.Layers^.DeleteIndex(AData^.PInfo,ABusGraph);
                     try
                        ABusGraph^.Invalidate(AData^.PInfo);
                     finally
                        PLayer(AData^.PInfo^.Objects)^.DeleteIndex(AData^.PInfo,ABusGraph);
                     end;
                  finally
                     Result:=True;
                  end;
                  exit;
               end
               else if NewKind^.bProp then begin
                  DiagSize:=GetPropSize;

                  Width:=DiagSize;
                  Height:=DiagSize;

                  PosX:=OldPosX-Trunc(Width/2);
                  PosY:=OldPosY+Trunc(Height/2);

                  ABusGraph^.Position.Init(PosX,PosY);
                  ABusGraph^.GesWidth:=Width;
                  ABusGraph^.GesHeight:=Height;

                  ABusGraph^.BWidth:=ABeamWidth;
                  ABusGraph^.BHeight:=ABeamHeight;

                  ABusGraph^.ChartWidth:=Width;
                  ABusGraph^.ChartHeight:=Height;

                  ABusGraph^.CalculateClipRect;
               end;
            end
            else begin
               ProcSetBusGraph(AData,PIndex(ABusGraph),NewKind);
            end;
            {brovak}
            Application.ProcessMessages;
            {brovak}
            ProjID:=ABusGraph^.Index;
            AData^.InsertedObjects^.Insert(StrNew(@DBID));
            Str(ProjID: 10, TempStr);
            AData^.InsertedObjects^.Insert(StrNew(TempStr));
          end;
        end;
      end;
      Inc(NowCount);
    StatusBar.Progress:=(100*NowCount)/TotalCount;
    end;
{**************************************** Spaltenliste ****************************************}
  Procedure MakeColumnList
     (
     Item              : PChar
     ); Far;
    var FieldNr        : Integer;
        AColor         : Integer;
        APattern       : Word;
        APen           : Word;
        Column         : PNameColl;
        Value          : Array[0..255] of Char;
    begin
      FieldNr:=0;
      AColor:=1;
      APattern:=pt_Solid;
      APen:=lt_Solid;
      if Flag=true then Cha:=GetSepPos(Item,']',3,0)
      else
      if DDEHandler.FGotLInfo = 0 then Cha:=18
      else if (DDEHandler.FGotLInfo = 1) or (DDEHandler.FGotLInfo = 2) then Cha:=GetSepPos(Item,']',3,0);
      Item:=Item+Cha;
      while Item <> NIL do begin
        ReadDDEText(Item,DDEHandler.FDDESepSign,Value);
        Inc(FieldNr);
        if (StrComp(Value,'') <> 0) and (FieldNr mod 2 = 1) then begin
          Column:=New(PNameColl);
          Column^.Name:=NewStr(StrPas(Value));
          Column^.Pen:=APen;
          Column^.Pattern:=APattern;
          NameColl^.Insert(Column);
          Inc(AColor);
        end;
      end;
    end;
{**************************************** Hauptteil ****************************************}
  {$ENDIF} // <----------------- AXDLL
{$ENDIF}
  begin
{$IFNDEF WMLT}
  {$IFNDEF AXDLL} // <----------------- AXDLL
   NewKind:=nil;
   NewDiagType:=DefaultType;
   OldPal:=BusGraphColors;
   bProp:=False;
   WriteTo:=NIL;
   try
    AData^.ClearStatusText;

//bro

 if Flag=false
  then begin;
    if DDEHandler.FGotLInfo = 0 then WriteTo:=AData^.Layers^.TopLayer
    else if DDEHandler.FGotLInfo = 1 then begin
      StrOK:=ReadDDECommandString(LineColl^.At(0),1,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ResStr,StrToLong,MStrs);
      if StrOK and (ResStr <> '') then begin
        WriteTo:=AData^.Layers^.NameToLayer(ResStr);
        if WriteTo = NIL then begin
          if MsgBox(AData^.Parent.Handle,11025,mb_OkCancel+mb_IconExclamation,'') = id_OK then
            WriteTo:=AData^.Layers^.TopLayer;
        end
        else begin                              
          if (WriteTo^.SortIndex = 0) or (WriteTo^.GetState(sf_LayerOff)) or (WriteTo^.GetState(sf_Fixed)) then begin
            if MsgBox(AData^.Parent.Handle,11026,mb_OkCancel+mb_IconExclamation,'') = id_OK then
              WriteTo:=AData^.Layers^.TopLayer
            else WriteTo:=NIL;
          end;
        end;
      end
      else WriteTo:=AData^.Layers^.TopLayer;
    end
    else if DDEHandler.FGotLInfo = 2 then begin
      if ReadDDECommandLongInt(LineColl^.At(0),1,0,WgBlockStart,
                                           WgBlockEnd,WgBlockEnd,ResLInt,SFound,MStrs) then begin
        WriteTo:=AData^.Layers^.IndexToLayer(ResLInt);
        if WriteTo = NIL then begin
          if MsgBox(AData^.Parent.Handle,11025,mb_OkCancel+mb_IconExclamation,'') = id_OK then
            WriteTo:=AData^.Layers^.TopLayer;
        end
        else begin
          if (WriteTo^.SortIndex = 0) or (WriteTo^.GetState(sf_LayerOff)) or (WriteTo^.GetState(sf_Fixed)) then begin
            if MsgBox(AData^.Parent.Handle,11026,mb_OkCancel+mb_IconExclamation,'') = id_OK then
              WriteTo:=AData^.Layers^.TopLayer
            else WriteTo:=NIL;
          end;
        end;
      end
      else WriteTo:=AData^.Layers^.TopLayer;
    end;
  end //flag;
  else
     WriteTo:=IDBLAYER;


    if WriteTo = NIL then MsgBox(AData^.Parent.Handle,11027,mb_Ok+mb_IconExclamation,'')
    else begin
      NewType:=ntCDNot;
      GetMem(TempStr,MaxLen);
      NameColl:=New(PCollection,Init(5,5));
      MakeColumnList(LineColl^.At(0));
      AType:=AData^.DiagramType;
      ABeamWidth:=AData^.BeamWidth;
      ABeamHeight:=AData^.BeamHeight;
      ACircleDiameter:=AData^.CircleDiameter;
      CDDiamAbsRel:=AData^.CircDiagAbsRel;
      ShowValues:=AData^.DiagAnnot;
      ValAbsRel:=AData^.DAnnotAbsRel;
(*!?!?!      PLAColor:=AData^.DrawPalette^[0];*)
      PLAPen:=lt_Solid;
      PLAPattern:=pt_Solid;
{      if ExecDialog(TDColumn.Init(AData^.Parent,AData^.Layers,@WriteTo,NameColl,
          AData^.PInfo,@(AData^.DiagFont),@AType,@ABeamHeight,@ABeamWidth,@ACircleDiameter,
          @CDDiamAbsRel,@ShowValues,@ValAbsRel,@PLAColor,@PLAPen,@PLAPattern,'COLUMN'))=id_OK then begin}
        AData^.DeselectAll(TRUE);
        with AData^ do begin
          if (DiagramType <> AType) or (BeamWidth <> ABeamWidth) or (BeamHeight <> ABeamHeight)
              or (CircleDiameter <> ACircleDiameter) or (CircDiagAbsRel <> CDDiamAbsRel)
              or (DiagAnnot <> ShowValues) or (DAnnotAbsRel <> ValAbsRel) then begin
            DiagramType:=AType;
            BeamWidth:=ABeamWidth;
            BeamHeight:=ABeamHeight;
            CircleDiameter:=ACircleDiameter;
            CircDiagAbsRel:=CDDiamAbsRel;
            DiagAnnot:=ShowValues;
            DAnnotAbsRel:=ValAbsRel;
            //IniFile^.WriteDiagOpts(DiagramType,BeamWidth,BeamHeight,CircleDiameter,CircDiagAbsRel,DiagAnnot,DAnnotAbsRel);
          end;
          AFont.Init;
          AFont:=DiagFont;
          if AType = dtCircleDiag then AFont.Height:=Trunc(ACircleDiameter/5)
          else AFont.Height:=Trunc(8*ABeamWidth/10);
          AData^.RedrawAfterInsert:=FALSE;
          OldTop:=AData^.Layers^.TopLayer;
          AData^.Layers^.TopLayer:=WriteTo;
          //
          StatusBar.ProgressPanel := TRUE;
          StatusBar.ProgressText :=  GetLangText(5000);
          StatusBar.Progress:=0;
          TotalCount:=LineColl.Count;
          NowCount:=0;

//          LineColl^.ForEach(@DoAllSum);

          LineColl^.LastThat(@DoAll);

          StatusBar.ProgressPanel := false;
          //

          AData^.Layers^.TopLayer:=OldTop;
          ProcSendNewIDsToDB(AData,'GRD');

          AData^.RedrawAfterInsert:=TRUE;
          if AData^.BorderCorrected then begin
{!?!?!?            ACursor:=AData^.ActualCur;
            if AData^.OverView then begin
              AData^.SetOverview;
              AData^.PInfo^.RedrawScreen(TRUE);
            end;
            AData^.SetCursor(ACursor);
            AData^.BorderCorrected:=FALSE;}
          end;
          AFont.Done;
        end;
//      end;
      Cha:=0;
      while Cha < NameColl^.Count do begin
        Dispose(PNameColl(NameColl^.At(Cha)));
        Inc(Cha);
      end;
      NameColl^.DeleteAll;
      Dispose(NameColl,Done);
      FreeMem(TempStr,MaxLen);
    end;
   finally
    BusGraphColors:=OldPal;
    LineColl^.FreeAll;
    MaxLen:=0;
   end;
  {$ENDIF} // <----------------- AXDLL
{$ENDIF}
  end;

procedure TBusGraphText.SetItemColor(i: Integer; Col: AnsiString);
var
c: Integer;
begin
     if (i >= 0) and (i < MAXBGCOL) then begin
        c:=StrToIntDef(Col,BusGraphColors[i mod MAXBGCOL]);
        BusGraphColors[i]:=c;
     end;
end;

end.


