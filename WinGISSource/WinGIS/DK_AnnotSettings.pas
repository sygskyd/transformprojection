unit DK_AnnotSettings;
{
Annotation Objects Settings.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 25-12-2001
}
interface
{$IFNDEF AXDLL} // <----------------- AXDLL
uses
   Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
   ExtCtrls, StdCtrls, MultiLng, ComCtrls, Buttons,
   AM_Proj, FontStyleDlg, AM_Admin, AM_Layer, AM_Font, Objects, WCtrls, Am_Ini,
   CheckLst, Grids;

type
   TDK_AnnotSettingsForm = class (TForm)
      TopPanel: TPanel;
      BottomPanel: TPanel;
      OKButton: TButton;
      CancelButton: TButton;
      MlgSection1: TMlgSection;
      LayerNamesComboBox: TComboBox;
      Label1: TLabel;
      PageControl1: TPageControl;
      TabSheet1: TTabSheet;
      TabSheet2: TTabSheet;
      FieldNamesGB: TGroupBox;
      DataGB: TGroupBox;
      UpSB: TSpeedButton;
      DownSB: TSpeedButton;
      DataMemo: TMemo;
      RadioButton1: TRadioButton;
      RadioButton2: TRadioButton;
      TabSheet3: TTabSheet;
      OffSetAdvCheckBox: TCheckBox;
      PolyLineAdvDirectionComboBox: TComboBox;
      ArcAdvCheckBox: TCheckBox;
      ArcAdvDirectionComboBox: TComboBox;
      WGroupBox1: TWGroupBox;
      WGroupBox2: TWGroupBox;
      Window2: TWindow;
      Label2: TLabel;
    FieldNamesListBox: TCheckListBox;
    PrePostGrid: TStringGrid;
      procedure FormCreate (Sender: TObject);
      procedure FormCloseQuery (Sender: TObject; var CanClose: Boolean);
      procedure UpSBClick (Sender: TObject);
// ++ Commented by Cadmensky IDB Version 2.3.5
//      procedure DownSBClick (Sender: TObject);
// -- Commented by Cadmensky IDB Version 2.3.5
      procedure FormHide (Sender: TObject);
      procedure FormShow (Sender: TObject);
      procedure DataMemoChange (Sender: TObject);
      procedure RadioButton2Click (Sender: TObject);
      procedure RadioButton1Click (Sender: TObject);
      procedure UseAdvancedOptionsCheckBoxClick (Sender: TObject);
      procedure PolyLineAdvCheckBoxClick (Sender: TObject);
      procedure ArcAdvCheckBoxClick (Sender: TObject);
      procedure Window2Paint (Sender: TObject);
      procedure Window2MouseDown (Sender: TObject; Button: TMouseButton;
         Shift: TShiftState; X, Y: Integer);
      procedure Window2MouseMove (Sender: TObject; Shift: TShiftState; X,
         Y: Integer);
      procedure Window2MouseUp (Sender: TObject; Button: TMouseButton;
         Shift: TShiftState; X, Y: Integer);
      procedure OffSetAdvCheckBoxClick (Sender: TObject);
    procedure FieldNamesListBoxClick(Sender: TObject);
    procedure PrePostGridClick(Sender: TObject);
   private
    { Private declarations }
      AProject: PProj;
      ASourceLayer, ADestinationLayer: PLayer;
      AFontDialog: TFontStyleDialog;
      AnAnnotData: PCollection;
      bFieldOrderHasBeenChanged: Boolean;
      bDataHasBeenChanged: Boolean;
      function GetLayerIndex (var iLayerIndex: LongInt): Boolean;
      procedure ChangeFieldOrder;
      procedure SelectSnap;
      procedure SelectSnap1;
   public
    { Public declarations }

      constructor Create (AOwner: TComponent; AProject: PProj; ADestinationLayer: PLayer; AnAnnotData: PCollection; ASourceLayer: PLayer = nil);

      function GetDestinationLayer: PLayer;
      function KeepLinkToDBAlive: Boolean;
      function CalculateNewPosition: Boolean;

      procedure MakeWindowAsCreateDialog;
      procedure MakeWindowAsChangeDialog (bKeepLinkToDBAlive: Boolean; bWeAreUsingTheIDB: Boolean);

   end;
{$ENDIF} // <----------------- AXDLL
implementation
{$IFNDEF AXDLL} // <----------------- AXDLL
{$R *.DFM}

uses AM_Def, LayerPropertyDlg, AM_Main, WinOSInfo;

var

 {brovak}
   BasePoint: Tpoint;
   MyPoint: TPoint;
   Specrect: TRect;
   AnglePoint: Tpoint;
   OrigMouse: TPoint;
   DeltaX, DeltaY: Integer;
   IndexMinimum: integer;
    {brovak}

function PointDistance
   (
   const Point1: TPoint;
   const Point2: TPoint
   )
   : Double;
begin
   Result := Sqrt (Sqr (Point1.X - Point2.X) + Sqr (Point1.Y - Point2.Y));
end;

procedure TDK_AnnotSettingsForm.SelectSnap;
type
   Points = array[1..9] of Tpoint;

var
   Minimum: double;
   i, j: integer;
   APoints: Points;
begin;

   with Specrect do
   begin;

      APoints[1].X := SpecRect.Left;
      APoints[1].Y := SpecRect.Top;

      APoints[2].X := (SpecRect.Left + SpecRect.Right) div 2;
      APoints[2].Y := SpecRect.Top;

      APoints[3].X := SpecRect.Right;
      APoints[3].Y := SpecRect.Top;

      APoints[4].X := SpecRect.Left;
      APoints[4].Y := (SpecRect.Top + SpecRect.Bottom) div 2;

      APoints[5].X := (SpecRect.Left + SpecRect.Right) div 2;
      APoints[5].Y := (SpecRect.Top + SpecRect.Bottom) div 2;

      APoints[6].X := SpecRect.Right;
      APoints[6].Y := (SpecRect.Top + SpecRect.Bottom) div 2;

      APoints[7].X := SpecRect.Left;
      APoints[7].Y := SpecRect.Bottom;

      APoints[8].X := (SpecRect.Left + SpecRect.Right) div 2;
      APoints[8].Y := SpecRect.Bottom;

      APoints[9].X := SpecRect.Right;
      APoints[9].Y := SpecRect.Bottom;
   end;

   Minimum := PointDistance (MyPoint, APoints[1]);
   IndexMinimum := 1;

   for i := 2 to 9 do
      if PointDistance (MyPoint, APoints[i]) < Minimum then
      begin;
         Minimum := PointDistance (MyPoint, APoints[i]);
         IndexMinimum := i;
      end;
   IniFile.AdvAnnotationSettings.OffSetAdvDirection := IndexMinimum;

   AnglePoint := APoints[IndexMinimum];

   with Window2.Canvas do
   begin;
      Pen.Style := psClear;
      Pen.Color := clRed;
      Pen.Width := 2;
      Pen.Style := psSolid;
      Moveto (APoints[IndexMinimum].X - 3, APoints[IndexMinimum].y - 3);
      LineTo (APoints[IndexMinimum].X + 3, APoints[IndexMinimum].y + 3);

      Moveto (APoints[IndexMinimum].X - 3, APoints[IndexMinimum].y + 3);
      LineTo (APoints[IndexMinimum].X + 3, APoints[IndexMinimum].y - 3);
      Pen.Width := 1;
      Pen.Color := clBlue;
      Pen.Style := psDot;
      Moveto (Mypoint.X, MyPoint.y);
      LineTo (APoints[IndexMinimum].X - 1, APoints[IndexMinimum].Y - 1);

   end;

  //draw snap

end;

procedure TDK_AnnotSettingsForm.SelectSnap1;
type
   Points = array[1..9] of Tpoint;

var
   Minimum: double;
   i, j: integer;
   APoints: Points;
   Width,
      Height: integer;
begin;
   Width := SpecRect.Right - SpecRect.Left;
   Height := SpecRect.Bottom - Specrect.Top;

   case IndexMinimum of

      0, 9:
         begin;
            BasePoint.X := MyPoint.X - Width;
            Basepoint.Y := MyPoint.Y - Height;
         end;
      8:
         begin;
            BasePoint.X := MyPoint.X - Width div 2;
            Basepoint.Y := MyPoint.Y - Height;
         end;
      7:
         begin;
            BasePoint.X := MyPoint.X;
            Basepoint.Y := MyPoint.Y - Height;
         end;
      6:
         begin;
            BasePoint.X := MyPoint.X - Width;
            BasePoint.Y := MyPoint.Y - Height div 2;
         end;
      5:
         begin;
            BasePoint.X := MyPoint.X - Width div 2;
            BasePoint.Y := MyPoint.Y - Height div 2
         end;
      4:
         begin;
            BasePoint.X := MyPoint.X;
            BasePoint.Y := MyPoint.Y - Height div 2
         end;
      3:
         begin;
            BasePoint.X := MyPoint.X - Width;
            Basepoint.Y := MyPoint.Y
         end;
      2:
         begin;
            BasePoint.X := MyPoint.X - Width div 2;
            Basepoint.Y := MyPoint.Y
         end;
      1:
         begin;
            BasePoint.X := MyPoint.X;
            Basepoint.Y := MyPoint.Y
         end;

   end;
   Window2.Invalidate;
   Window2Paint (Self);
end;

constructor TDK_AnnotSettingsForm.Create (AOwner: TComponent;
   AProject: PProj;
   ADestinationLayer: PLayer;
   AnAnnotData: PCollection;
   ASourceLayer: PLayer = nil);
var
   iI, iP: Integer;
   ALayer: PLayer;
begin
   inherited Create (AOwner);
   SELF.AProject := AProject;
// ++ Cadmensky IDB Version 2.3.5
   SELF.ASourceLayer := ASourceLayer;
// -- Cadmensky IDB Version 2.3.5
   SELF.ADestinationLayer := ADestinationLayer;
   SELF.AnAnnotData := AnAnnotData;
   SELF.bFieldOrderHasBeenChanged := FALSE;
     // Add all layers in the combobox.
   for iI := 1 to AProject.Layers.LData.Count - 1 do
   begin
      ALayer := AProject.Layers.LData.At (iI);
      iP := LayerNamesComboBox.Items.Add (ALayer.Text^);
      LayerNamesComboBox.Items.Objects[iP] := Pointer (ALayer);
   end;
     // Set the destination layer name in the combobox.
   iP := -1;
   for iI := 0 to LayerNamesComboBox.Items.Count - 1 do
   begin
      ALayer := PLayer (LayerNamesComboBox.Items.Objects[iI]);
      if ALayer = ADestinationLayer then
      begin
         iP := iI;
         BREAK;
      end;
   end;
   if iP < 0 then
   begin
      iP := LayerNamesComboBox.Items.Add (ADestinationLayer^.Text^);
      LayerNamesComboBox.Items.Objects[iP] := Pointer (ADestinationLayer);
   end;
   LayerNamesComboBox.Text := LayerNamesComboBox.Items[iP];
     // Create font settings dialog.
   AFontDialog := TFontStyleDialog.Create (SELF);
   AFontDialog.Fonts := AProject.Pinfo.Fonts;
   AFontDialog.FontStyle := @AProject.ActualFont;
end;

procedure TDK_AnnotSettingsForm.FormCreate (Sender: TObject);
begin
//     AFontDialog.TextStyle.PropertyMode := pmCollect;
   AFontDialog.TextStyle.UpdatesEnabled := TRUE;
   AFontDialog.AngleEdit.Visible := FALSE;
   AFontDialog.AngleSpin.Visible := FALSE;
   AFontDialog.AngleLabel.Visible := FALSE;
     // Add font settings dialog on TabSheet2.
   AFontDialog.ControlPanel.Parent := SELF.TabSheet2;
   AFontDialog.ControlPanel.Top := 0;
   TabSheet2.Caption := AFontDialog.Caption;
     // Set TabSheet1 visible as default page.
   PageControl1.ActivePage := TabSheet1;
end;

procedure TDK_AnnotSettingsForm.FormCloseQuery (Sender: TObject; var CanClose: Boolean);
var
   iLayerIndex: LongInt;
   ALayer: PLayer;
   prepostlist : TStringList;
   i : integer;
begin
   if SELF.ModalResult = mrOK then
   begin
      CanClose := FALSE;
      ALayer := AProject.Layers.NameToLayer (LayerNamesComboBox.Text);
      if ALayer <> nil then
      begin
         SELF.ADestinationlayer := ALayer;
         CanClose := TRUE;
      end
      else
         if GetLayerIndex (iLayerIndex) then
         begin
            ALayer := AProject.Layers.IndexToLayer (iLayerIndex);
            if ALayer <> nil then
            begin
               CanClose := TRUE;
               SELF.ADestinationlayer := ALayer;
            end;
         end;
      if CanClose and (bFieldOrderHasBeenChanged or bDataHasBeenChanged) then
         ChangeFieldOrder;
   end;

   if (SELF.ModalResult = mrOK) and (CanClose = true) then
   begin;
      try
      with IniFile.AdvAnnotationSettings do
      begin;
           //    UseAdvancedOptions:=UseAdvancedOptionsCheckBox.Checked;
           //   PolygonAdv:=PolygonAdvCheckBox.Checked;
            //   PolyLineAdv:=PolyLineAdvCheckBox.Checked;
         PolyLineAdvDirection := PolyLineAdvDirectionComboBox.ItemIndex;
         ArcAdv := ArcAdvCheckBox.Checked;
         ArcAdvDirection := ArcAdvDirectionComboBox.ItemIndex;
         OffSetAdv := OffSetAdvCheckBox.Checked;
         OffSetAdvDirection := IndexMinimum;
      end;
      IniFile.WriteAdvAnnotation;
      finally
      prepostlist:=TStringList.Create;
      prepostlist.Sorted:=False;
      for i:= 0 to PrePostgrid.RowCount-1 do if Fieldnameslistbox.Checked[i] then
          begin
          if PrePostgrid.Cells[0,i]='' then PrePostgrid.Cells[0,i]:='~|';
          if PrePostgrid.Cells[1,i]='' then PrePostgrid.Cells[1,i]:='~|';
          prepostlist.Add(PrePostgrid.Cells[0,i]);
          prepostlist.Add(PrePostgrid.Cells[1,i]);
          end;
      prepostlist.SaveToFile(OSInfo.TempDataDir + 'prepost.tmp');     
      end;
   end;
end;

function TDK_AnnotSettingsForm.GetLayerIndex (var iLayerIndex: LongInt): Boolean;
var
   ADialog: TLayerPropertyDialog;
   ALayer: PLayer;
begin
   RESULT := FALSE;
   iLayerIndex := -1;
   ADialog := TLayerPropertyDialog.Create (Self);
   try
      ADialog.Caption := GetLangText (10154);
      ADialog.Project := AProject;
      ADialog.ProjectStyles := AProject.PInfo.ProjStyles;
      ADialog.LayerStyle.Name := LayerNamesComboBox.Text;
      if ADialog.ShowModal = mrOK then
      begin
// ++ Cadmensky
         ALayer := AProject.Layers.InsertLayer ('', 0, 0, 0, 0, ilTop, DefaultSymbolFill);
//           ALayer := AProject.Layers.InsertLayer('', 0, 0, 0, 0, ilSecond, DefaultSymbolFill);
// -- Cadmensky
         ALayer.SetLayerStyle (AProject.PInfo, ADialog.LayerStyle);
         iLayerIndex := ALayer.Index;
         RESULT := TRUE;
      end;
   finally
      ADialog.Free;
   end;
end;

function TDK_AnnotSettingsForm.GetDestinationLayer: PLayer;
begin
   RESULT := SELF.ADestinationLayer;
end;

function TDK_AnnotSettingsForm.CalculateNewPosition: Boolean;
begin
   //  RESULT := CheckBox1.Checked;
end;

function TDK_AnnotSettingsForm.KeepLinkToDBAlive: Boolean;
begin
 //    RESULT := CheckBox2.Checked;
end;

procedure TDK_AnnotSettingsForm.UpSBClick (Sender: TObject);
var
   sTemp: AnsiString;
// ++ Cadmensky IDB Version 2.3.5
   bChecked: Boolean;
   iNewItemIndex, iOldItemIndex: Integer;
// -- Cadmensky IDB Version 2.3.5
begin
// ++ Cadmensky IDB Version 2.3.5
      iOldItemIndex := FieldNamesListBox.ItemIndex;
      if Sender = UpSB then
      begin
         if FieldNamesListBox.ItemIndex > 0 then
            iNewItemIndex := FieldNamesListBox.ItemIndex - 1
         else
            Exit;
      end
      else
         if Sender = DownSB then
         begin
            if FieldNamesListBox.ItemIndex < FieldNamesListBox.Items.Count - 1 then
               iNewItemIndex := FieldNamesListBox.ItemIndex + 1
            else
               Exit;
         end
         else
            Exit;
// -- Cadmensky IDB Version 2.3.5
      sTemp := FieldNamesListBox.Items[iNewItemIndex];
      bChecked := FieldNamesListBox.Checked[iNewItemIndex];
      FieldNamesListBox.Items[iNewItemIndex] := FieldNamesListBox.Items[iOldItemIndex];
      FieldNamesListBox.Checked[iNewItemIndex] := FieldNamesListBox.Checked[iOldItemIndex];
      FieldNamesListBox.Items[iOldItemIndex] := sTemp;
      FieldNamesListBox.Checked[iOldItemIndex] := bChecked;
      FieldNamesListBox.ItemIndex := iNewItemIndex;

      sTemp:=PrePostgrid.Cells[0,iNewItemIndex];
      PrePostgrid.Cells[0,iNewitemIndex]:= PrePostgrid.Cells[0,iOldItemIndex];
      PrePostgrid.Cells[0,iOldItemIndex]:= sTemp;
      sTemp:=PrePostgrid.Cells[1,iNewItemIndex];
      PrePostgrid.Cells[1,iNewitemIndex]:= PrePostgrid.Cells[1,iOldItemIndex];
      PrePostgrid.Cells[1,iOldItemIndex]:= sTemp;

      bFieldOrderHasBeenChanged := TRUE;
      FieldnamesListBoxClick(Sender);
end;

// ++ Commented by Cadmensky IDB Version 2.3.5
{procedure TDK_AnnotSettingsForm.DownSBClick (Sender: TObject);
var
   sTemp: AnsiString;
// ++ Cadmensky IDB Version 2.3.5
   bSelected: Boolean;
// -- Cadmensky IDB Version 2.3.5
begin
   if FieldNamesListBox.ItemIndex < FieldNamesListBox.Items.Count - 1 then
   begin
      sTemp := FieldNamesListBox.Items[FieldNamesListBox.ItemIndex + 1];
// ++ Cadmensky IDB Version 2.3.5
      bSelected := FieldNamesListBox.Selected[FieldNamesListBox.ItemIndex + 1];
// -- Cadmensky IDB Version 2.3.5
      FieldNamesListBox.Items[FieldNamesListBox.ItemIndex + 1] := FieldNamesListBox.Items[FieldNamesListBox.ItemIndex];
// ++ Cadmensky IDB Version 2.3.5
      FieldNamesListBox.Selected[FieldNamesListBox.ItemIndex + 1] := FieldNamesListBox.Selected[FieldNamesListBox.ItemIndex];
// -- Cadmensky IDB Version 2.3.5
      FieldNamesListBox.Items[FieldNamesListBox.ItemIndex] := sTemp;
// ++ Cadmensky IDB Version 2.3.5
      FieldNamesListBox.Selected[FieldNamesListBox.ItemIndex] := bSelected;
// -- Cadmensky IDB Version 2.3.5
      FieldNamesListBox.ItemIndex := FieldNamesListBox.ItemIndex + 1;
      bFieldOrderHasBeenChanged := TRUE;
   end;
end;}
// -- Commented by Cadmensky IDB Version 2.3.5

procedure TDK_AnnotSettingsForm.ChangeFieldOrder;
var
   ANewData: PCollection;
   AnAnnot: PAnnot;
   iI, iJ: Integer;
   bF: Boolean;
   sFieldName: AnsiString;
begin
// ++ Cadmensky Version 2.3.5
   for iI := 0 to AnAnnotData.Count - 1 do
   begin;
      AnAnnot := AnAnnotData.At (iI);
      StrDispose (AnAnnot.LineName);
      StrDispose (AnAnnot.LineValue);
      Dispose (AnAnnot);
   end;
// -- Cadmensky Version 2.3.5
   AnAnnotData.DeleteAll;
   for iI := 0 to FieldNamesListBox.Items.Count - 1 do
   begin
// ++ Cadmensky IDB version 2.3.5
      if (SELF.ASourceLayer = nil) or not FieldNamesListBox.Checked[iI] then
         Continue;
// -- Cadmensky IDB version 2.3.5
      AnAnnot := New (PAnnot);
      AnAnnot.LineName := StrNew (PChar (FieldNamesListBox.Items[iI]{ + '��Testv|�Testh'}));
      try
         AnAnnot.LineValue := StrNew (PChar (DataMemo.Lines[iI]));
         AnAnnot.HasValue := DataMemo.Lines[iI] <> '';
      except
         AnAnnot.LineValue := StrNew ('');
         AnAnnot.HasValue := FALSE;
      end;
      AnAnnotData.Insert (AnAnnot);
   end;
end;

procedure TDK_AnnotSettingsForm.FormHide (Sender: TObject);
begin
   if SELF.ModalResult = mrOK then
   begin
      AFontDialog.ModalResult := mrOK;
      AFontDialog.FormHide (SENDER);
   end;
end;

procedure TDK_AnnotSettingsForm.FormShow (Sender: TObject);
var
   iI: Integer;
   AData: PAnnot;
// ++ Cadmensky IDB Version 2.3.5
   iIndex: Integer;
// -- Cadmensky IDB Version 2.3.5
begin
     // TabSheet1
   FieldNamesListBox.Items.Clear;
   DataMemo.Lines.Clear;
// ++ Cadmensky IDB Version 2.3.5       Auskommentiert von WAI
   {$IFNDEF WMLT}
   {if SELF.ASourceLayer <> nil then
      FieldNamesListBox.Items.Text := WinGISMainForm.IDB_Man.GetTableFieldsNamesList (SELF.AProject, SELF.ASourceLayer);}
// -- Cadmensky IDB Version 2.3.5
   {$ENDIF}
   ArcAdvCheckBoxClick (SELF);
// -- Cadmensky
   for iI := 0 to AnAnnotData.Count - 1 do
   begin
      AData := AnAnnotData.At (iI);
      if SELF.ASourceLayer <> nil then
      begin
         FieldNamesListBox.Items.Add (AData.LineName); //WAI
         iIndex := FieldNamesListBox.Items.IndexOf (AData.LineName);
         if iIndex >= 0 then
            FieldNamesListBox.Checked[iIndex] := true;
      end
      else
         FieldNamesListBox.Items.Add (AData.LineName);
      DataMemo.Lines.Add (AData.LineValue);
   end;
   if FieldNamesListBox.ItemIndex < 0 then
      FieldNamesListBox.ItemIndex := 0;
     // TabSheet2
   AFontDialog.FormShow (SENDER);

   IndexMinimum := 9;
   IniFile.ReadAdvAnnotation;

   with IniFile.AdvAnnotationSettings do
   begin;

    //  UseAdvancedOptionsCheckBox.Checked:=UseAdvancedOptions;
     // PolygonAdvCheckBox.Checked:=PolygonAdv;
   //   PolyLineAdvCheckBox.Checked:=PolyLineAdv;
      PolyLineAdvDirectionComboBox.ItemIndex := PolyLineAdvDirection;
      if PolyLineAdvDirectionComboBox.ItemIndex = -1 then
         PolyLineAdvDirectionComboBox.ItemIndex := 0;
      ArcAdvCheckBox.Checked := ArcAdv;
// ++ Cadmensky
      ArcAdvDirectionComboBox.Enabled := ArcAdv;
// -- Cadmensky
      ArcAdvDirectionComboBox.ItemIndex := ArcAdvDirection;
      if ArcAdvDirectionComboBox.ItemIndex = -1 then
         ArcAdvDirectionComboBox.ItemIndex := 0;
      Indexminimum := OffSetAdvDirection;
      if IndexMinimum < 0 then Indexminimum := 5;
      OffSetAdvCheckBox.Checked := OffSetAdv;
  //    if OffSetAdv=true then begin;
  //    Screen.Cursor:=crSize;
  //    Window2.OnMouseDown(Parent,mbleft,[],SpecRect.Left+3,Specrect.Top+3);
  //    Window2.OnMouseMove(Parent,[],SpecRect.Left,SpecRect.Top);
   //   Window2.OnMouseUp(Parent,mbleft,[],SpecRect.Left,SpecRect.Top);
      SelectSnap1;
     // Window2.Invalidate;
                           //  end;
   end;
     //temporary
{   if PageControl1.Pages[2].TabVisible = true then
   begin;
      UseAdvancedOptionsCheckBox.OnClick (Self);
   end;}
PrePostGrid.RowCount:= Fieldnameslistbox.items.Count;
end;

procedure TDK_AnnotSettingsForm.DataMemoChange (Sender: TObject);
var
   sText, sS: AnsiString;
   iI, iP, iSelStartPos, iStringCount: Integer;
begin
   bDataHasBeenChanged := TRUE;

   iStringCount := FieldNamesListBox.Items.Count;
   iSelStartPos := DataMemo.SelStart;

     // Delete all empty lines in Memo.
   iI := 0;
   while iI < DataMemo.Lines.Count do
      if DataMemo.Lines[iI] = '' then
         DataMemo.Lines.Delete (iI)
      else
         Inc (iI);
     // Delete all lines that exceed fields count.
   while DataMemo.Lines.Count > iStringCount do
      DataMemo.Lines.Delete (DataMemo.Lines.Count - 1);

   if DataMemo.Lines.Count = iStringCount then
   try
      sText := DataMemo.Text;
      sS := '';
        // Pass by all CRLF symbols.
      for iI := 0 to DataMemo.Lines.Count - 2 do
      begin
         iP := Pos (#13#10, sText);
         if iP > 0 then
         begin
            sS := sS + Copy (sText, 1, iP + 1);
            sText := Copy (sText, iP + 2, Length (sText));
         end;
      end;
        // Delete the last CRLF symbol.
      if Copy (sText, Length (sText) - 1, 2) = #13#10 then
         sText := Copy (sText, 1, Length (sText) - 2);
      DataMemo.Text := sS + sText;
      DataMemo.SelStart := iSelStartPos;
   except
   end;
end;

procedure TDK_AnnotSettingsForm.RadioButton1Click (Sender: TObject);
begin
   FieldNamesListBox.Enabled := TRUE;
   UpSB.Enabled := TRUE;
   DownSB.Enabled := TRUE;

   DataMemo.Enabled := FALSE;
end;

procedure TDK_AnnotSettingsForm.RadioButton2Click (Sender: TObject);
begin
   FieldNamesListBox.Enabled := FALSE;
   UpSB.Enabled := FALSE;
   DownSB.Enabled := FALSE;

   DataMemo.Enabled := TRUE;
end;

procedure TDK_AnnotSettingsForm.MakeWindowAsCreateDialog;
begin
   //DataGB.Visible := FALSE;
   RadioButton1.Visible := FALSE;
   RadioButton2.Visible := FALSE;
   DataMemo.visible:=False;
  //   CheckBox1.Visible := FALSE;
end;

procedure TDK_AnnotSettingsForm.MakeWindowAsChangeDialog (bKeepLinkToDBAlive: Boolean; bWeAreUsingTheIDB: Boolean);
begin
 (*    CheckBox2.Checked := bKeepLinkToDBAlive;
     If bKeepLinkToDBAlive AND bWeAreUsingTheIDB Then Begin
        RadioButton1.Checked := TRUE;
        RadioButton1Click(SELF);
        End
     Else Begin
        RadioButton2.Checked := TRUE;
        RadioButton2Click(SELF);
        If NOT bWeAreUsingTheIDB Then Begin
           RadioButton1.Enabled := FALSE;
           CheckBox2.Checked := FALSE;
           CheckBox2.Enabled := FALSE;
           End;
        End;
        *)
end;

procedure TDK_AnnotSettingsForm.UseAdvancedOptionsCheckBoxClick (Sender: TObject);
begin
(*
// if UseAdvancedOptionsCheckBox.Checked then
 // begin;
   WgroupBox1.Enabled:=true;
   WgroupBox2.Enabled:=true;
   PolygonAdvCheckBox.Enabled:=true;
   PolyLineAdvCheckBox.Enabled:=true;
   ArcAdvCheckBox.Enabled:=true;
   ArcAdvDirectionComboBox.Enabled:=ArcAdvCheckBox.Checked;
   OffSetAdvCheckBox.Enabled:=true;
   PolyLineAdvDirectionComboBox.Enabled:=OffSetAdvCheckBox.Checked;
   Window2.Visible:= OffSetAdvCheckBox.Checked;
   if Window2.Visible then
     SelectSnap1;

  end
  *)
(*  else
  begin;
   WgroupBox1.Enabled:=false;
   WgroupBox2.Enabled:=false;
   PolygonAdvCheckBox.Enabled:=false;
   PolyLineAdvCheckBox.Enabled:=false;
   ArcAdvCheckBox.Enabled:=false;
   ArcAdvDirectionComboBox.Enabled:=false;
   OffSetAdvCheckBox.Enabled:=false;
   PolyLineAdvDirectionComboBox.Enabled:=false;
   Window2.Visible:=false;
  end;
  *)

end;

procedure TDK_AnnotSettingsForm.PolyLineAdvCheckBoxClick (Sender: TObject);
begin
 //PolyLineAdvDirectionComboBox.Enabled:=PolyLineAdvCheckBox.Checked;
end;

procedure TDK_AnnotSettingsForm.ArcAdvCheckBoxClick (Sender: TObject);
begin
   ArcAdvDirectionComboBox.Enabled := ArcAdvCheckBox.Checked;
end;

procedure TDK_AnnotSettingsForm.Window2Paint (Sender: TObject);

var
   Arect: Trect;

   OldBrush: TBrush;
   OldPen: TPen;
   OldPenWidth: integer;
   OldStyle: TPenStyle;

begin
//if UseadvancedOptionsCheckBox.Checked=false then Exit;
   if OffsetAdvCheckBox.Checked = false then Exit;

   ARect.Left := Window2.Width div 2 - 35;
   ARect.Right := Window2.Width div 2 + 35;
   Arect.Bottom := Window2.Height div 2 + 25;
   Arect.Top := Window2.Height div 2 - 25;

   MyPoint.X := (ARect.Right + Arect.Left) div 2;
   MyPoint.Y := (Arect.Bottom + Arect.Top) div 2;

   with Window2.Canvas do
   begin;
// FillRect(Window2.Clientrect);
      OldBrush := Brush;
      OldPen := Pen;

      Brush.Style := bsBDiagonal;
      Pen.Color := ClNavy;
      Pen.Width := 1;

      Brush.Color := clNavy;

      Pen.Style := psSolid;
      Brush.Color := clNavy;

      with Arect do
         RectAngle (Left, Top, Right, Bottom);

      Pen.Style := psClear;
      Pen.Color := clRed;
      Pen.Width := 3;
      Pen.Style := psSolid;
      Moveto (MyPoint.X - 4, MyPoint.y - 1);
      LineTo (MyPoint.X + 4, MyPoint.y - 1);

      Moveto (MyPoint.X, MyPoint.y - 5);
      LineTo (MyPoint.X, MyPoint.y + 3);

{ If Radiogroup2.ItemIndex>-1 then
  begin;
   With Arect do begin;
     case Radiogroup2.ItemIndex of
      8:begin; Right:=MyPoint.X;Bottom:=MyPoint.Y;end;
      7:begin; Right:=MyPoint.X;Bottom:=MyPoint.Y+9;end;
      6:begin; Right:=MyPoint.X;Bottom:=MyPoint.Y+18;end;
      5:begin; Right:=MyPoint.X+20;Bottom:=MyPoint.Y;end;
      4:begin; Right:=MyPoint.X+20;Bottom:=MyPoint.Y+9;end;
      3:begin; Right:=MyPoint.X+20;Bottom:=MyPoint.Y+18;end;
      2:begin; Right:=MyPoint.X+40;Bottom:=MyPoint.Y;end;
      1:begin; Right:=MyPoint.X+40;Bottom:=MyPoint.Y+9;end;
      0:begin; Right:=MyPoint.X+40;Bottom:=MyPoint.Y+18;end;
     end;
   Left:=Right-40;Top:=Bottom-18;}

      Pen.Color := clBlack;
      Pen.Style := psInsideFrame;
      Pen.Width := 1;
      with Specrect do
      begin;
         Left := BasePoint.X; //+ DeltaX;
         Right := Left + 50;
         Top := BasePoint.y; // + Deltay;
         Bottom := Top + 20;
         Brush.Color := clWhite;
         Brush.Style := bsSolid;
//   Font.Style:=Font.Style+[fsBold];
 //Window3.Canvas.Pen.Color:=BaseFont.Color;
         Brush.Style := bsClear;
         RectAngle (Left, Top, Right, Bottom);
//   Brush.Style:=bssolid;
         Pen.Color := Window2.Canvas.Font.Color;
   // Brush.Color:=clBlack;
//    Pen.Style:=PsSolid;
 //!!  Window2.Canvas.Font :=BaseFont;
         Window2.Canvas.Font.Style := Font.Style + [fsBold];
         TextRect (Specrect, Left + 7, Top + 5, 'Text');

      end;

      if Screen.Cursor = crSize then SelectSnap;

   end;
// Canvas.Brush:=Oldbrush;
// Canvas.Pen:=OldPen;

end;

procedure TDK_AnnotSettingsForm.Window2MouseDown (Sender: TObject;
   Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
   clickpoint: TPoint;
begin
   ClickPoint.X := x;
   ClickPoint.y := y;

   if Button = mbLeft then

      if ptInRect (Specrect, ClickPoint) then
      begin

         if ssDouble in Shift then
         begin;
 // ChangeFont;
            Exit;
         end
         else
         begin;
            GetCursorPos (OrigMouse);
            Screen.Cursor := crSize;
            DeltaX := ClickPoint.X - BasePoint.X;
            DeltaY := ClickPoint.Y - BasePoint.Y;
         end;
      end;
end;

procedure TDK_AnnotSettingsForm.Window2MouseMove (Sender: TObject;
   Shift: TShiftState; X, Y: Integer);
var
   clickpoint: TPoint;
begin
   ClickPoint.X := x;
   ClickPoint.y := y;

   if shift = [ssleft] then
   begin;

      if not ptInRect (Window2.ClientRect, ClickPoint) then
      begin;
  //   if X> Window2.ClientRect.Right then ClickPoint.X:=Window2.ClientRect.Right-1;
//     if X< 0 then ClickPoint.X:= 1;
//     if Y> Window2.Height then ClickPoint.Y:=Window2.Height-1;
//     if Y< 0 then ClickPoint.Y:=1;
         SetCursorPos (OrigMouse.X, OrigMouse.Y);
//     Screen.Cursor:=crdefault;
         Exit;
      end;
      GetCursorPos (OrigMouse);

      if Screen.Cursor = crSize then
      begin;
 //  OldPoint.X:=X;
 //  OldPoint.Y:=y;
//   invalidate
 //   InvalidateRect(Window2.Handle,@SpecRect,false);
 //   UpdateWindow(Window2.Handle);

{     begin;
     if X<0 then x:=0;
     if x>Window2.ClientRect.Right then x:=Window2.ClientRect.Right;
     if Y<0 then x:=0;
     if y>Window2.ClientRect.Bottom then y:=Window2.ClientRect.Bottom;
    Exit;
    end;}

         BasePoint.x := x - DeltaX;
         BasePoint.y := y - Deltay;

  //  Window2.Canvas.Brush.Color:=clWhite;
         Window2.Invalidate;
// Window2.Canvas.Brush.Style:=bsSolid;
//Window2.Canvas.FillRect(Rect(0,0,Window2.Width,Window2.Height));
         Window2.OnPaint (self);

      end
   end
   else
      if ptInRect (Specrect, ClickPoint) then
         Screen.Cursor := crDrag
      else
         Screen.Cursor := crDefault;

end;

procedure TDK_AnnotSettingsForm.Window2MouseUp (Sender: TObject;
   Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
   if Screen.Cursor = crSize then
   begin;
      Screen.Cursor := crDefault;
      Basepoint.X := Mypoint.X + (BasePoint.X - AnglePoint.X);
      Basepoint.Y := Mypoint.Y + (BasePoint.Y - AnglePoint.Y);
      Window2.Invalidate;
      Window2.OnPaint (self);
   end;

   Screen.Cursor := crDefault;
   Window2.Invalidate;
end;

procedure TDK_AnnotSettingsForm.OffSetAdvCheckBoxClick (Sender: TObject);
begin
   Window2.Visible := OffSetAdvCheckBox.Checked;
   if Window2.Visible = true then
   begin;
      SelectSnap1;
      Window2.OnPaint (Self);
   end;
end;

procedure TDK_AnnotSettingsForm.FieldNamesListBoxClick(Sender: TObject);
var g : TGridRect;
begin
if FieldNamesListBox.ItemIndex< 0 then exit;
g.Left:=0;
g.Right:=0;
g.Top:=  FieldNamesListBox.ItemIndex;
g.Bottom:= FieldNamesListBox.ItemIndex;
PrePostGrid.Selection:=  g;
PrePostGrid.Row:= g.Top;
end;

procedure TDK_AnnotSettingsForm.PrePostGridClick(Sender: TObject);
begin
if PrePostGrid.Row >= 0 then
  FieldNamesListBox.ItemIndex:=  PrePostGrid.Row;
end;

{$ENDIF} // <----------------- AXDLL

end.

