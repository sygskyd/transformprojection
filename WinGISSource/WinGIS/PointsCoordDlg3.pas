{ Brovak Vladimir, Progis Russia
  Additional Dialog for Load Symbols from formatted text file with X,Y data
  and Symbol's Name (and selected library)
 Version 1  Jan 2000}

// Warning !!! Additional Mode for set pre-size/pre-rotated properties before insert not
// available yet !
// Dialog in test-mode. PLEASE, DON'T CHANGE WITHOUT NOTIFY TO brovak@progis.ru

unit PointsCoordDlg3;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,Am_obj,Multilng,
  Spin, StdCtrls, ExtCtrls,SymbolSelDlg,Am_Group, Spinbtn, WCtrls,Am_font,Am_proj,SymLib,ExtLib,
  Validate,Styledef,measures,propcollect;

type
  TPointsCoordDialog3 = class(TForm)
    MlgSection: TMlgSection;
    Button1: TButton;
    Button2: TButton;
    Label1: TLabel;
    AllSymbolsCheck: TCheckBox;
    CheckSpecial: TCheckBox;
    OpenSpecial: TEdit;
    CloseSpecial: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    ComboBox1: TComboBox;
    Label10: TLabel;
    SizeEdit: TEdit;
    AngleEdit: TEdit;
    Bevel1: TBevel;
    SymbolWindow: TWindow;
    Symbtn: TButton;
    Libname: TLabel;
    Symname: TLabel;
    ColonNumEdit: TEdit;
    SpinBtn3: TSpinBtn;
    Spin: TSpinBtn;
    SizeSpin: TSpinBtn;
    AngleSpin: TSpinBtn;
    AngleVal: TMeasureLookupValidator;
    ScaleIndependendCheck: TCheckBox;
    IntValidator1: TIntValidator;
    IntValidator2: TIntValidator;
    SymNumEdit: TEdit;
    SizeVal: TMeasureLookupValidator;
    procedure SymBtnClick(Sender: TObject);
    Procedure SymbolWindowRePaint(Sender: TObject);
    procedure SetLabels;
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure SymNumEditChange(Sender: TObject);
    procedure AllSymbolsCheckClick(Sender: TObject);
    procedure SizeEditChange(Sender: TObject);
    procedure CheckSize;
    procedure ScaleIndependendCheckClick(Sender: TObject);
    procedure CheckSpecialClick(Sender: TObject);
  private
    { Private declarations }
   FEditOptions     : TSelectOptionList;
   
  public
     ASymLibInfo:TSymLibInfo;
   ASymInfo:TSymInfo;
   ASelectedSymbol:PSGroup;
   ASymbols:PSymbols;
   FSymbol:PSGroup;
  ASymbol:PSGroup;
  AFonts       : pFonts;
  FFonts:Pfonts;
    { Public declarations }
  end;

var PointsCoordDialog3: TPointsCoordDialog3;

implementation

{$R *.DFM}

uses Am_Main;

procedure TPointsCoordDialog3.SymBtnClick(Sender: TObject);
var //SelectedSymbol : PSGroup;
   // AExtLib        : PExtLib;
  //  ProjSym: PSGroup;
 //   FFSymInfo         : TSymLibInfo;
   // d: TSymInfo;
    z:integer;
    FSymbol1:PSGroup;
    M:string;
begin

  FSymbol1:=GetSymbol(Self,WingisMainForm.ActualProj);
  if Fsymbol1<>nil then
   Fsymbol:=FSymbol1;
  SetLabels;

end;

procedure TPointsCoordDialog3.SymbolWindowRePaint;
 var ARect          : TRect;
begin
  if (FSymbol<>NIL) and (AllSymbolsCheck.Checked<>true) then  begin
    ARect:=Rect(2,2,SymbolWindow.Height-4,SymbolWindow.Height-4);
    FSymbol^.DrawSymbolInRect(SymbolWindow.Canvas.Handle,ARect,FFonts);
    end;
end;

procedure TPointsCoordDialog3.FormShow(Sender: TObject);
begin
if ASymLibInfo=nil //at first
 then begin;
  SetLabels;
  if Combobox1.ItemIndex=-1 then
    begin;
   if Combobox1.Items.Count=0
   then
    begin;
    Combobox1.Items.Add(Mlgsection[10]);
    Combobox1.Items.Add(Mlgsection[11]);
    end;
    Combobox1.ItemIndex:=0;
      end;

      FEditOptions:=TSelectOptionList.Create;
      SetupSelectOptions(FEditOptions,sotSymbol,[soObject]);

    SetSizeField(Pproj(WingisMainForm.ActualProj).ActSymSize,Pproj(WingisMainForm.ActualProj).ActSymSizeType,SizeVal,FEditOptions,
    ScaleIndependendCheck);

    SetDoubleField(Pproj(WingisMainForm.ActualProj).ActSymAngle,1,AngleVal,muRad,FEditOptions);
     CheckSize;
    end;





end;

procedure TPointsCoordDialog3.SetLabels;
    Function GetPositionOfSymbolsSet(ASymLibInfo: TSymLibInfo; sLibraryName: AnsiString): Integer;
   Var
      iI: Integer;
      m:ansistring;
    Begin
   M:=sLibraryName;
   if M='' then M:=MlgStringList['SymbolRollup',17];
        For iI:=0 To ASymLibInfo.LibCount-1 Do Begin
           if AnsiUpperCase(ASymLibInfo.GetLibName(ii))=AnsiUpperCase(M)
             then
             begin;
               RESULT := iI; EXIT;
               End;
            End; // For iI end
   End;


var M:string;
    i:integer;
 begin;
  if ASymLibInfo=nil
   then
    begin;
     FFonts:=PProj(WingisMainForm.ActualProj).PInfo^.Fonts;
     M:=PProj(WingisMainForm.ActualProj).ActualSym.GetLibName;
     if M='' then M:= MlgStringList.Strings['SymbolRollup',17];
     ASymbols:=PSymbols(PProj(WingisMainForm.ActualProj).PInfo^.Symbols);
     ASymLibInfo:=TSymLibInfo.Create(PProj(WingisMainForm.ActualProj),ASymbols,M);
     ASymInfo:=ASymLibInfo.GetSymInfo;
     SymNumEdit.Text:=IntToStr(1+AsymInfo.IndexOf(PProj(WingisMainForm.ActualProj).ActualSym));
     Symname.Caption:='( '+PProj(WingisMainForm.ActualProj).ActualSym.GetName+' )';
     Libname.Caption:=M;
     i:=ASymLibInfo.LibIndex;
     FSymbol:=PProj(WingisMainForm.ActualProj).ActualSym;
     IntValidator1.MaxValue:=ASymInfo.SymCount;
     Spin.Max:=ASymInfo.SymCount;
   end
   else
   begin;
     M:=FSymbol.GetLibName;
     if M='' then M:= MlgStringList.Strings['SymbolRollup',17];
     ASymLibInfo.LibIndex:=GetPositionOfSymbolsSet(AsymLibINfo,M);
     ASymInfo:=ASymLibInfo.GetSymInfo;
     SymNumEdit.Text:=IntToStr(1+AsymInfo.IndexOf(FSymbol));
     Symname.Caption:='( '+FSymbol.GetName +' )';
     Libname.Caption:=M;
     IntValidator1.MaxValue:=ASymInfo.SymCount;
     Spin.Max:=ASymInfo.SymCount;
   end;
     SymbolWindow.Repaint;
     SymbolWindowRepaint(Self);

   end;
procedure TPointsCoordDialog3.FormDestroy(Sender: TObject);
begin
 FEditOptions.Free;
 ASymLibInfo.Free;
 ASymInfo.Free;
 FFonts:=nil;
 ASymbols.Free;
end;

procedure TPointsCoordDialog3.SymNumEditChange(Sender: TObject);
begin
 if Trim(SymNumEdit.Text)<>''
  then
   if StrToInt(SymNumEdit.Text)<=Spin.Max
    then
     begin;
      FSymbol:=ASymInfo.GetSym(StrToInt(SymNumEdit.Text)-1);
      SetLabels;
     end;
end;


procedure TPointsCoordDialog3.AllSymbolsCheckClick(Sender: TObject);
begin
if AllSymbolsCheck.Checked
 then
  begin;
   Spin.Enabled:=false;
   SymNumEdit.Enabled:=false;
   SymNumEdit.Font.Color:=clGray;
   Symname.Font.Color:=clGray;
   Label5.Font.Color:=clGray;
   IntValidator1.Edit:=nil;
   SymbolWindow.Color:=clWhite;
   Symbtn.Enabled:=false;
  end
  else
  begin;
   IntValidator1.Edit:=SymNumEdit;
   Spin.Enabled:=true;
   SymNumEdit.Enabled:=true;
   SymNumEdit.Font.Color:=clBlack;
   Symname.Font.Color:=clBlack;
   Label5.Font.Color:=clBlack;
   Symbtn.Enabled:=true;
  end;
   SymbolWindow.Repaint;
end;

procedure TPointsCoordDialog3.SizeEditChange(Sender: TObject);
var AEnable        : Boolean;
begin
  AEnable:=SizeEdit.Enabled and (SizeVal.LookupIndex=-1)
      and not (SizeVal.Units in [muDrawingUnits,muNone]);
  if SizeVal.Value = 0 then begin
     ScaleIndependendCheck.Checked:=False;
     ScaleIndependendCheck.Enabled:=False;
  end
  else begin
     ScaleIndependendCheck.Enabled:=True;
  end;
  CheckSize;
end;

procedure TPointsCoordDialog3.CheckSize;
begin
     if ScaleIndependendCheck.Checked then begin
        if SizeVal.Value <> 0 then SizeVal.Units:=muMillimeters;
        SizeVal.UseableUnits:=32;
        SizeVal.MinValue.Value:=1;
        SizeVal.MinValue.Units:=muMillimeters;
        SizeVal.MaxValue.Units:=muMillimeters;
     end
     else begin
        if SizeVal.Value <> 0 then SizeVal.Units:=muDrawingUnits;
        SizeVal.UseableUnits:=2;
        SizeVal.MinValue.Value:=0.1;
        SizeVal.MinValue.Units:=muDrawingUnits;
        SizeVal.MaxValue.Units:=muDrawingUnits;
     end;
     if SizeVal.Value = 0 then begin
        ScaleIndependendCheck.Checked:=False;
        ScaleIndependendCheck.Enabled:=False;
     end
     else begin
        ScaleIndependendCheck.Enabled:=True;
     end;
end;

procedure TPointsCoordDialog3.ScaleIndependendCheckClick(Sender: TObject);
begin
 CheckSize;
  SizeEdit.SetFocus;
    if Sender <> nil then begin
        if TWinControl(Sender).Enabled then begin
           TWinControl(Sender).SetFocus;
        end;
     end;
end;

procedure TPointsCoordDialog3.CheckSpecialClick(Sender: TObject);
begin
 if CheckSpecial.Checked then
  begin;
    OpenSpecial.Enabled := true;
    OpenSpecial.Color := clWindow;
    CloseSpecial.Enabled := true;
    CloseSpecial.Color := clWindow;
  end
  else
  begin;
    OpenSpecial.Enabled := false;
    OpenSpecial.Color := clBtnFace;
    CloseSpecial.Enabled := false;
    CloseSpecial.Color := clBtnFace;
  end;

end;

end.
