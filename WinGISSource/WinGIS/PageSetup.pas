Unit PageSetup;

Interface

Uses Classes,GrTools,Measures,WPrinter,RegDB;

Type TPageSetupError    = (erNoError,erPrinterNotFound,erPaperFormatNotSupported);
     TPageSetupErrors   = Set of TPageSetupError;
     TPrintLayoutModel  = (plmUniquePage,plmUniformLayout,plmBook);

     TPageSetup    = Class(TPersistent)
      Private
       FCopies          : Integer;
       FSortCopies      : Boolean;
       FCurrentSize     : TPaperFormat;
       FExtent          : TGrRect;
       FHorizontalPages : Integer;
       FInterPageFrames : TGrRect;
       FAutoRepagenate  : Boolean;
       FLastError       : TPageSetupErrors;
       FList            : TStringList;
       FOrientation     : TWPrinterOrientation;
       FOverlapPageFrames : TGrRect;
       FPagesOrder      : Boolean;
       FPrintLayoutModel: TPrintLayoutModel;
       FPageFrames      : TGrRect;
       FPageWidths      : Array[0..2] of Double;
       FPageHeights     : Array[0..2] of Double;
       FPrinter         : TWPrinter;
       FPrintToWMF      : Boolean;
       FStartPageNum    : Integer;
       FVerticalPages   : Integer;
{++ Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
       FPrintCropMarks  : Boolean;
{-- Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
       FPrintPageNum    : Boolean;
       Procedure   FreePaperFormats;
       Function    GetName:String;
       Function    GetPageExtents(Horizontal,Vertical:Integer):TGrRect;
       Function    GetPaperFormat:TGrRect;
       function    GetPaperFormatExtent: TGrRect;
       Function    GetPaperFormats(AIndex:Integer):TPaperFormat;
       Function    GetSizeIndex:Integer;
       Function    GetWindowsID:Integer;
       Procedure   RecalculatePageSizes;
       Procedure   SetHorizontalPages(const Value: Integer);
       Procedure   SetInterPageFrames(const Value: TGrRect);
       Procedure   SetOverlapPageFrames(const Value: TGrRect);
       Procedure   SetOrientation(AOrientation:TWPrinterOrientation);
       Procedure   SetPageFrames(const Value: TGrRect);
       Procedure   SetPrinter(const Value: TWPrinter);
       Procedure   SetSizeIndex(const Value:Integer);
       Procedure   SetWindowsID(const Value:Integer);
       Procedure   SetVerticalPages(const Value: Integer);
       function    GetFramesInside: Boolean;
       function    GetInterFramesInside: Boolean;
      Protected
       Procedure   AssignTo(Dest:TPersistent); override;
      Public
       Constructor Create;
       Destructor  Destroy; override;
       // pointer to the currently selected page-size
       Property    ActivePaperFormat:TPaperFormat read FCurrentSize;
       Property    AutoRepagenate:Boolean read FAutoRepagenate write FAutoRepagenate;
       // number of copies and sort-copies information
       Property    Copies:Integer read FCopies write FCopies;
       Property    SortCopies:Boolean read FSortCopies write FSortCopies;
       // number of pages in x-direction
       Property    HorizontalPages:Integer read FHorizontalPages write SetHorizontalPages;
       // frames used for overlapping pages
       Property    InterPageFrames:TGrRect read FInterPageFrames write SetInterPageFrames;
       // checks if the page-frame are within the printable area of the printer
       Property    InterPageFrameInsidePrintableArea:Boolean read GetInterFramesInside;
       // last error-code
       Property    LastError:TPageSetupErrors read FLastError write FLastError;
       // loads the settings from a registry
       Procedure   LoadFromRegistry(Registry:TCustomRegistry;const KeyName:String);
       // name of the currenlty selected paper-format
       Property    Name:String read GetName;
       // orientation of the page (lasdscape of portrait)
       Property    Orientation:TWPrinterOrientation read FOrientation write SetOrientation;
       // specifies how much the pages shall overlap
       Property    OverlapPageFrames:TGrRect read FOverlapPageFrames write SetOverlapPageFrames;
       Property    PagesOrder:Boolean read FPagesOrder write FPagesOrder;
       Property    PrintLayoutModel:TPrintLayoutModel read FPrintLayoutModel write FPrintLayoutModel;
       // overall size of the useable part of the page
       Property    PageExtent:TGrRect read FExtent;
       // useable size of each page, position relative to overall-page-size
       Property    PageExtents[Horizontal,Vertical:Integer]:TGrRect read GetPageExtents;
       // frames to keep blank
       Property    PageFrames:TGrRect read FPageFrames write SetPageFrames;
       // checks if the page-frame are within the printable area of the printer
       Property    PageFrameInsidePrintableArea:Boolean read GetFramesInside;
       // overall size of all pages   
       Property    PageSize:TGrRect read GetPaperFormat;
       // name of the windows page-sizes
       Property    PaperFormatNames:TStringList read FList;
       // list of all available page-sizes
       Property    PaperFormats[AIndex:Integer]:TPaperFormat read GetPaperFormats;
       // currently selected page-size
       Property    PaperFormatIndex:Integer read GetSizeIndex write SetSizeIndex;
       Property    PaperFormatSize:TGrRect read GetPaperFormatExtent;
       // printer-object to use with the page-setup
       Property    Printer:TWPrinter read FPrinter write SetPrinter;
       Property    PrintToWMF:Boolean read FPrintToWMF write FPrintToWMF;
{++ Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
       Property    PrintCropMarks:Boolean read FPrintCropMarks write FPrintCropMarks;
{-- Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
       // copy settings from or to the printer
       Property    PrintPageNum:Boolean read FPrintPageNum write FPrintPageNum;
       Procedure   SetOptionsToPrinter;
       Procedure   GetOptionsFromPrinter;
       // saves the settings to the registry
       Procedure   SaveToRegistry(Registry:TCustomRegistry;const KeyName:String);
       // start page number
       Property    StartPageNum:Integer read FStartPageNum write FStartPageNum;
       // number of pages in y--direction
       Property    VerticalPages:Integer read FVerticalPages write SetVerticalPages;
       // windows paper-id of the currently selected page-size
       Property    WindowsID:Integer read GetWindowsID write SetWindowsID;

       Procedure   UpdatePaperFormats;
     end;

Implementation

Uses MultiLng,NumTools,SysUtils,Windows;

{ TPageSetup }

Procedure TPageSetup.AssignTo(Dest: TPersistent);
begin
  with Dest as TPageSetup do begin
    Printer:=Self.FPrinter;
    FHorizontalPages:=Self.FHorizontalPages;
    FVerticalPages:=Self.FVerticalPages;
    FOverlapPageFrames:=Self.FOverlapPageFrames;
    FPagesOrder:=Self.FPagesOrder;
    FStartPageNum:=Self.FStartPageNum;
    FPrintLayoutModel:=Self.FPrintLayoutModel;
    FPageFrames:=Self.FPageFrames;
    FInterPageFrames:=Self.FInterPageFrames;
    FOrientation:=Self.FOrientation;
    FCopies:=Self.FCopies;
    FSortCopies:=Self.FSortCopies;
    SetWindowsID(Self.GetWindowsID);
  end;
end;

Constructor TPageSetup.Create;

   Procedure NewPaperFormat(const Name:String;WindowsID:Integer;Units:TMeasureUnits;Width,Height:Double);
   var PaperFormat     : TPaperFormat;
   begin
      PaperFormat:=TPaperFormat.Create;
      PaperFormat.Name:=Name;
      PaperFormat.WindowsID:=WindowsID;
      PaperFormat.Width:=Round(Width*UnitsToBaseUnits(Units)*10);
      PaperFormat.Height:=Round(Height*UnitsToBaseUnits(Units)*10);
      FList.AddObject(Name,PaperFormat);
   end;

begin
   inherited Create;
   // create list for paper-formats
   FList:=TStringList.Create;
   FList.Sorted:=TRUE;
   FList.Duplicates:=dupAccept;
   // set default-values
   FHorizontalPages:= 1;
   FVerticalPages:= 1;
   FOverlapPageFrames:= GrRect(0,0,0,0);
   FPagesOrder:= TRUE;
   FStartPageNum:= 1;
   FPrintLayoutModel:= plmUniquePage;
   FPageFrames:= GrRect(20,20,20,20);
   FInterPageFrames:= GrRect(10,10,10,10);
{++ Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
   FPrintCropMarks:= FALSE;
{-- Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
   // set to default-printer settings
   Printer:=WPrinters.DefaultPrinter;
   GetOptionsFromPrinter;
   // setup with default-values if no printer installed on the system
   if not Assigned(FPrinter) or (WindowsId = -1) then
   begin
     // fill the page-size list
     NewPaperFormat(MlgStringList['PageSetup',1],DMPAPER_LETTER,muInches,8.5,11);
     NewPaperFormat(MlgStringList['PageSetup',2],DMPAPER_LEGAL,muInches,8.5,14);
     NewPaperFormat(MlgStringList['PageSetup',3],DMPAPER_A4,muMillimeters,210,297);
     NewPaperFormat(MlgStringList['PageSetup',4],DMPAPER_CSHEET,muInches,17,22);
     NewPaperFormat(MlgStringList['PageSetup',5],DMPAPER_DSHEET,muInches,22,34);
     NewPaperFormat(MlgStringList['PageSetup',6],DMPAPER_ESHEET,muInches,34,44);
     NewPaperFormat(MlgStringList['PageSetup',7],DMPAPER_TABLOID,muInches,11,17);
     NewPaperFormat(MlgStringList['PageSetup',8],DMPAPER_LEDGER,muInches,17,11);
     NewPaperFormat(MlgStringList['PageSetup',9],DMPAPER_STATEMENT,muInches,5.5,8.5);
     NewPaperFormat(MlgStringList['PageSetup',10],DMPAPER_EXECUTIVE,muInches,7.25,10.5);
     NewPaperFormat(MlgStringList['PageSetup',11],DMPAPER_A3,muMillimeters,297,420);
     NewPaperFormat(MlgStringList['PageSetup',12],DMPAPER_A5,muMillimeters,148,210);
     NewPaperFormat(MlgStringList['PageSetup',13],DMPAPER_B4,muMillimeters,250,354);
     NewPaperFormat(MlgStringList['PageSetup',14],DMPAPER_B5,muMillimeters,182,257);
     NewPaperFormat(MlgStringList['PageSetup',15],DMPAPER_FOLIO,muInches,8.5,13);
     NewPaperFormat(MlgStringList['PageSetup',16],DMPAPER_QUARTO,muMillimeters,215,275);
     NewPaperFormat(MlgStringList['PageSetup',17],DMPAPER_10X14,muInches,10,14);
     NewPaperFormat(MlgStringList['PageSetup',18],DMPAPER_11X17,muInches,11,17);
     NewPaperFormat(MlgStringList['PageSetup',19],DMPAPER_ENV_9,muInches,3+7/8,8+7/8);
     NewPaperFormat(MlgStringList['PageSetup',20],DMPAPER_ENV_10,muInches,4+1/8,9.5);
     NewPaperFormat(MlgStringList['PageSetup',21],DMPAPER_ENV_11,muInches,4.5,10+3/8);
     NewPaperFormat(MlgStringList['PageSetup',22],DMPAPER_ENV_12,muInches,4+3/4,11);
     NewPaperFormat(MlgStringList['PageSetup',23],DMPAPER_ENV_14,muInches,5,11+1/2);
     NewPaperFormat(MlgStringList['PageSetup',24],DMPAPER_ENV_DL,muMillimeters,110,220);
     NewPaperFormat(MlgStringList['PageSetup',25],DMPAPER_ENV_C5,muMillimeters,162,229);
     NewPaperFormat(MlgStringList['PageSetup',26],DMPAPER_ENV_C3,muMillimeters,324,458);
     NewPaperFormat(MlgStringList['PageSetup',27],DMPAPER_ENV_C4,muMillimeters,229,324);
     NewPaperFormat(MlgStringList['PageSetup',28],DMPAPER_ENV_C5,muMillimeters,114,162);
     NewPaperFormat(MlgStringList['PageSetup',29],DMPAPER_ENV_C65,muMillimeters,114,229);
     NewPaperFormat(MlgStringList['PageSetup',30],DMPAPER_ENV_B4,muMillimeters,250,353);
     NewPaperFormat(MlgStringList['PageSetup',31],DMPAPER_ENV_B5,muMillimeters,176,250);
     NewPaperFormat(MlgStringList['PageSetup',32],DMPAPER_ENV_B6,muMillimeters,166,125);
     NewPaperFormat(MlgStringList['PageSetup',33],DMPAPER_ENV_ITALY,muMillimeters,110,230);
     NewPaperFormat(MlgStringList['PageSetup',34],DMPAPER_ENV_MONARCH,muInches,3+3/8,7+1/2);
     NewPaperFormat(MlgStringList['PageSetup',35],DMPAPER_ENV_PERSONAL,muInches,3+5/6,6+1/2);
     NewPaperFormat(MlgStringList['PageSetup',36],DMPAPER_FANFOLD_US,muInches,14+7/8,11);
     NewPaperFormat(MlgStringList['PageSetup',37],DMPAPER_FANFOLD_STD_GERMAN,muInches,8+1/2,12);
     NewPaperFormat(MlgStringList['PageSetup',38],DMPAPER_FANFOLD_LGL_GERMAN,muInches,8+1/2,7+13);
     // setup with default-values
     if GetLocaleStr(GetSystemDefaultLCID,LOCALE_IMEASURE,'0')='1'
     then  SetWindowsID(DMPAPER_LETTER)
     else  SetWindowsID(DMPAPER_A4);
   end;
   FLastError:=[];
end;

Destructor TPageSetup.Destroy;
var Cnt            : Integer;
begin
   for Cnt:=0 to FList.Count-1 do
       TPaperFormat(FList.Objects[Cnt]).Free;
   FList.Free;
   inherited Destroy;
end;

Function TPageSetup.GetName:String;
begin
   Result:=FCurrentSize.Name;
end;

Function TPageSetup.GetPageExtents(Horizontal,Vertical:Integer):TGrRect;
var LeftFrame      : Double;
    PageWidth      : Double;
    BottomFrame    : Double;
    PageHeight     : Double;
begin
  LeftFrame:=0;
  if Horizontal=0 then
     PageWidth:=FPageWidths[0]
  else
     begin
        LeftFrame:=LeftFrame+FPageWidths[0];
        if Horizontal>1 then
           LeftFrame:=LeftFrame+(Horizontal-1)*FPageWidths[1];
        if Horizontal=FHorizontalPages-1 then
           PageWidth:=FPageWidths[2]
        else
           PageWidth:=FPageWidths[1];
     end;
  BottomFrame:=0;
  if Vertical=0 then
     PageHeight:=FPageHeights[0]
  else
     begin
        BottomFrame:=BottomFrame+FPageHeights[0];
        if Vertical>1 then
           BottomFrame:=BottomFrame+(Vertical-1)*FPageHeights[1];
        if Vertical=FVerticalPages-1 then
           PageHeight:=FPageHeights[2]
        else
           PageHeight:=FPageHeights[1];
     end;
  Result:=GrBounds(LeftFrame,BottomFrame,PageWidth,PageHeight);
end;

Function TPageSetup.GetPaperFormat:TGrRect;
begin
  Result:=GrRect(-FPageFrames.Left,-FPageFrames.Bottom,
      FExtent.Right+FPageFrames.Right,FExtent.Top+FPageFrames.Top);
end;

Function TPageSetup.GetPaperFormats(AIndex:Integer):TPaperFormat;
begin
  Result:=TPaperFormat(FList.Objects[AIndex]);
end;

Function TPageSetup.GetSizeIndex:Integer;
begin
  Result:=FList.IndexOfObject(FCurrentSize);
end;

Function TPageSetup.GetWindowsID:Integer;
begin                        
   if FCurrentSize=NIL then
      Result:=-1
   else
      Result:=FCurrentSize.WindowsID;
end;

{******************************************************************************+
  Procedure TPageSetup.LoadFromRegistry
--------------------------------------------------------------------------------
  Loads the setting from the registry-database. 
+******************************************************************************}
Procedure TPageSetup.LoadFromRegistry(Registry: TCustomRegistry;
                                      const KeyName: String);
var AValue         : Integer;
    BValue         : Double;
    CValue         : Boolean;
    DValue         : String;
    Index          : Integer;
    AFormat        : Integer;
begin
  with Registry do
    begin
      if FCurrentSize<>NIL then AFormat:=FCurrentSize.WindowsID;
      AValue:=ReadInteger(KeyName+'WindowsPageID');
      if LastError=rdbeNoError then AFormat:=AValue;
      AValue:=ReadInteger(KeyName+'HorizontalPages');
      if LastError=rdbeNoError then FHorizontalPages:=AValue;
      AValue:=ReadInteger(KeyName+'VerticalPages');
      if LastError=rdbeNoError then FVerticalPages:=AValue;
      AValue:=ReadInteger(KeyName+'Copies');
      if LastError=rdbeNoError then FCopies:=AValue;
      CValue:=ReadBool(KeyName+'SortCopies');
      if LastError=rdbeNoError then FSortCopies:=CValue;
      BValue:=ReadFloat(KeyName+'OverlapPageFrames\Left');
      if LastError=rdbeNoError then FOverlapPageFrames.Left:=BValue;
      BValue:=ReadFloat(KeyName+'OverlapPageFrames\Right');
      if LastError=rdbeNoError then FOverlapPageFrames.Right:=BValue;
      BValue:=ReadFloat(KeyName+'OverlapPageFrames\Bottom');
      if LastError=rdbeNoError then FOverlapPageFrames.Bottom:=BValue;
      BValue:=ReadFloat(KeyName+'OverlapPageFrames\Top');
      if LastError=rdbeNoError then FOverlapPageFrames.Top:=BValue;
      AValue:=ReadInteger(KeyName+'LayoutModel');
      if LastError=rdbeNoError then FPrintLayoutModel:=TPrintLayoutModel(AValue);
      CValue:=ReadBool(KeyName+'AutoRepagenate');
      if LastError=rdbeNoError then FAutoRepagenate:=CValue;
      CValue:=ReadBool(KeyName+'PagesOrder');
      if LastError=rdbeNoError then FPagesOrder:=CValue;
      CValue:=ReadBool(KeyName+'PrintToWMF');
      if LastError=rdbeNoError then FPrintToWMF:=CValue;
{++ Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
      CValue:=ReadBool(KeyName+'PrintCropMarks');
      if LastError=rdbeNoError then FPrintCropMarks:=CValue;
{-- Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
      AValue:=ReadInteger(KeyName+'StartPageNumber');
      if LastError=rdbeNoError then FStartPageNum:=AValue;
      AValue:=ReadInteger(KeyName+'Orientation');
      if LastError=rdbeNoError then FOrientation:=TWPrinterOrientation(AValue);
      BValue:=ReadFloat(KeyName+'PageFrames\Left');
      if LastError=rdbeNoError then FPageFrames.Left:=BValue;
      BValue:=ReadFloat(KeyName+'PageFrames\Right');
      if LastError=rdbeNoError then FPageFrames.Right:=BValue;
      BValue:=ReadFloat(KeyName+'PageFrames\Bottom');
      if LastError=rdbeNoError then FPageFrames.Bottom:=BValue;
      BValue:=ReadFloat(KeyName+'PageFrames\Top');
      if LastError=rdbeNoError then FPageFrames.Top:=BValue;
      BValue:=ReadFloat(KeyName+'InterPageFrames\Left');
      if LastError=rdbeNoError then FInterPageFrames.Left:=BValue;
      BValue:=ReadFloat(KeyName+'InterPageFrames\Right');
      if LastError=rdbeNoError then FInterPageFrames.Right:=BValue;
      BValue:=ReadFloat(KeyName+'InterPageFrames\Bottom');
      if LastError=rdbeNoError then FInterPageFrames.Bottom:=BValue;
      BValue:=ReadFloat(KeyName+'InterPageFrames\Top');
      if LastError=rdbeNoError then FInterPageFrames.Top:=BValue;
      DValue:=ReadString(KeyName+'PrinterName');
      // set the printer, set to default if printer does not exist
      Index:=WPrinters.Printers.IndexOf(DValue);
      if Index>=0  then FPrinter:=WPrinters[Index]
      else  begin
        FPrinter:=WPrinters.DefaultPrinter;
        Include(FLastError,erPrinterNotFound);
      end;
      UpdatePaperFormats;
      SetWindowsID(AFormat);
    end;
end;

Procedure TPageSetup.SaveToRegistry(Registry: TCustomRegistry;
                                    const KeyName: String);
begin
  with Registry do
    begin
      WriteInteger(KeyName+'WindowsPageID',GetWindowsID);
      WriteInteger(KeyName+'HorizontalPages',FHorizontalPages);
      WriteInteger(KeyName+'VerticalPages',FVerticalPages);
      WriteFloat(KeyName+'OverlapPageFrames\Left',FOverlapPageFrames.Left);
      WriteFloat(KeyName+'OverlapPageFrames\Right',FOverlapPageFrames.Right);
      WriteFloat(KeyName+'OverlapPageFrames\Bottom',FOverlapPageFrames.Bottom);
      WriteFloat(KeyName+'OverlapPageFrames\Top',FOverlapPageFrames.Top);
      WriteInteger(KeyName+'LayoutModel',Integer(FPrintLayoutModel));
      WriteBool(KeyName+'AutoRepagenate',FAutoRepagenate);
      WriteBool(KeyName+'PagesOrder',FPagesOrder);
      WriteBool(KeyName+'PrintToWMF',FPrintToWMF);
{++ Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
      WriteBool(KeyName+'PrintCropMarks',FPrintCropMarks);
{-- Moskaliov BUGS##202b,293 BUILD#152 19.01.01}
      WriteInteger(KeyName+'StartPageNumber',FStartPageNum);
      WriteInteger(KeyName+'Orientation',Integer(FOrientation));
      WriteFloat(KeyName+'PageFrames\Left',FPageFrames.Left);
      WriteFloat(KeyName+'PageFrames\Right',FPageFrames.Right);
      WriteFloat(KeyName+'PageFrames\Bottom',FPageFrames.Bottom);
      WriteFloat(KeyName+'PageFrames\Top',FPageFrames.Top);
      WriteFloat(KeyName+'InterPageFrames\Left',FInterPageFrames.Left);
      WriteFloat(KeyName+'InterPageFrames\Right',FInterPageFrames.Right);
      WriteFloat(KeyName+'InterPageFrames\Bottom',FInterPageFrames.Bottom);
      WriteFloat(KeyName+'InterPageFrames\Top',FInterPageFrames.Top);
      WriteString(KeyName+'PrinterName',FPrinter.Name);
    end;
end;

Procedure TPageSetup.SetInterPageFrames(const Value: TGrRect);
begin
   FInterPageFrames:=Value;
   RecalculatePageSizes;
end;

Procedure TPageSetup.SetOverlapPageFrames(const Value: TGrRect);
begin
   FOverlapPageFrames:=Value;
   RecalculatePageSizes;
end;

Procedure TPageSetup.SetPageFrames(const Value:TGrRect);
begin
   FPageFrames:=Value;
   RecalculatePageSizes;
end;

Procedure TPageSetup.SetSizeIndex(const Value:Integer);
begin
   FCurrentSize:=TPaperFormat(FList.Objects[Value]);
   RecalculatePageSizes;
end;

Procedure TPageSetup.SetWindowsID(const Value:Integer);
  var
    Cnt   : Integer;
  begin
    for Cnt:=FList.Count-1 downto 0 do  begin
      FCurrentSize:=TPaperFormat(FList.Objects[Cnt]);
      if FCurrentSize.WindowsID=Value then
      begin
        RecalculatePageSizes;
        Exit;
      end;
    end;

    // paper-format is not supported, use current printer-setting
    Include(FLastError,erPaperFormatNotSupported);
    if Assigned(FPrinter) then
      for Cnt:=FList.Count-1 downto 0 do  begin
        FCurrentSize:=TPaperFormat(FList.Objects[Cnt]);
        if FCurrentSize.WindowsID=FPrinter.PaperFormat then
        begin
          RecalculatePageSizes;
          Exit;
        end;
      end;
    if FList.Count>0
    then
       FCurrentSize:= TPaperFormat(FList.Objects[0]);
  end;

Procedure TPageSetup.SetOrientation(AOrientation:TWPrinterOrientation);
begin
  FOrientation:=AOrientation;
  RecalculatePageSizes;
end;

Procedure TPageSetup.SetHorizontalPages(const Value:Integer);
begin
  FHorizontalPages:=Value;
  RecalculatePageSizes;
end;

Procedure TPageSetup.SetVerticalPages(const Value:Integer);
begin
  FVerticalPages:=Value;
  RecalculatePageSizes;
end;

{ TPaperFormat }

Procedure TPageSetup.RecalculatePageSizes;
var NeededWidth    : Double;
    NeededHeight   : Double;
    PaperSize      : TGrRect;
begin
  // get the paper-size
  PaperSize:=GetPaperFormatExtent;
  // calculate the width of the pages
  FillChar(FPageWidths,SizeOf(FPageWidths),#0);
  if FHorizontalPages=1 then
     begin
        FPageWidths[0]:=RectWidth(PaperSize)-FPageFrames.Left-FPageFrames.Right;
        NeededWidth:=FPageWidths[0];
     end
  else
     begin
        if FPrintLayoutModel = plmUniformLayout then
           begin
              FPageWidths[0]:=RectWidth(PaperSize)-FPageFrames.Left-FOverlapPageFrames.Right
                              -FInterPageFrames.Right;
              FPageWidths[1]:=RectWidth(PaperSize)-FInterPageFrames.Left-FOverlapPageFrames.Left
                              -FOverlapPageFrames.Right-FInterPageFrames.Right;
              FPageWidths[2]:=RectWidth(PaperSize)-FInterPageFrames.Left-FOverlapPageFrames.Left
                              -FPageFrames.Right;
           end
        else   // FPrintLayoutModel = plmBook
           begin
              FPageWidths[0]:=RectWidth(PaperSize)-FPageFrames.Left-FOverlapPageFrames.Right
                              -FPageFrames.Right;
              FPageWidths[1]:=RectWidth(PaperSize)-FPageFrames.Left-FOverlapPageFrames.Left
                              -FOverlapPageFrames.Right-FPageFrames.Right;
              FPageWidths[2]:=RectWidth(PaperSize)-FPageFrames.Left-FOverlapPageFrames.Left
                              -FPageFrames.Right;
           end;
        NeededWidth:=FPageWidths[0]+FPageWidths[2];
        if FHorizontalPages>2 then
           NeededWidth:=NeededWidth+(FHorizontalPages-2)*FPageWidths[1];
     end;
  // calculate the height of the pages
  FillChar(FPageHeights,SizeOf(FPageHeights),#0);
  if FVerticalPages=1 then
     begin
        FPageHeights[0]:=RectHeight(PaperSize)-FPageFrames.Top-FPageFrames.Bottom;
        NeededHeight:=FPageHeights[0];
     end
  else
     begin
        if FPrintLayoutModel = plmUniformLayout then
           begin
              FPageHeights[0]:=RectHeight(PaperSize)-FPageFrames.Top-FOverlapPageFrames.Bottom
                               -FInterPageFrames.Bottom;
              FPageHeights[1]:=RectHeight(PaperSize)-FInterPageFrames.Top-FOverlapPageFrames.Top
                               -FOverlapPageFrames.Bottom-FInterPageFrames.Bottom;
              FPageHeights[2]:=RectHeight(PaperSize)-FInterPageFrames.Top
                               -FOverlapPageFrames.Top-FPageFrames.Bottom;
           end
        else   // FPrintLayoutModel = plmBook
           begin
              FPageHeights[0]:=RectHeight(PaperSize)-FPageFrames.Top-FOverlapPageFrames.Bottom
                               -FPageFrames.Bottom;
              FPageHeights[1]:=RectHeight(PaperSize)-FPageFrames.Top-FOverlapPageFrames.Top
                               -FOverlapPageFrames.Bottom-FPageFrames.Bottom;
              FPageHeights[2]:=RectHeight(PaperSize)-FPageFrames.Top
                               -FOverlapPageFrames.Top-FPageFrames.Bottom;
           end;
        NeededHeight:=FPageHeights[0]+FPageHeights[2];
        if FVerticalPages>2 then
           NeededHeight:=NeededHeight+(FVerticalPages-2)*FPageHeights[1];
     end;
  FExtent:=GrBounds(0,0,NeededWidth,NeededHeight);
end;

function TPageSetup.GetPaperFormatExtent:TGrRect;
begin
  if FOrientation=wpoPortrait then
     Result:=GrBounds(0,0,FCurrentSize.Width/10,FCurrentSize.Height/10)
  else
     Result:=GrBounds(0,0,FCurrentSize.Height/10,FCurrentSize.Width/10);
end;

Procedure TPageSetup.FreePaperFormats;
var Cnt            : Integer;
begin
   for Cnt:=0 to FList.Count-1 do
       TPaperFormat(FList.Objects[Cnt]).Free;
   FList.Clear;
end;

Procedure TPageSetup.UpdatePaperFormats;
  var
    PaperFormat      : TPaperFormat;
    Cnt              : Integer;
    AIndex           : Integer;
  begin
    if FPrinter<>NIL then begin
         // free the current paper-format list
         AIndex:=GetSizeIndex;
         if FPrinter.PaperFormatCount > 0 then begin
            FreePaperFormats;
         end;
         // copy the paper-formats of the printer
         for Cnt:=0 to FPrinter.PaperFormatCount-1 do begin
             PaperFormat:=TPaperFormat.Create;
             PaperFormat.Assign(FPrinter.PaperFormats[Cnt]);
             FList.AddObject(PaperFormat.Name,PaperFormat);
         end;
         if AIndex >= 0 then begin
            SetSizeIndex(AIndex);
         end;
    end;
  end;

//++ Glukhov PageSetupCorrection Build#175 25.01.02
Procedure TPageSetup.GetOptionsFromPrinter;
begin
  if Assigned(FPrinter) then begin
    // reactivate the previous paper-format
    SetWindowsID(FPrinter.PaperFormat);
    // copy other printer-settings
    FOrientation:= FPrinter.Orientation;
    FCopies:= FPrinter.Copies;
    // Correct FCopies (it can be zero,f.i., for a set off printer)
    if FCopies < 1 then FCopies:= 1;
    FSortCopies:= FPrinter.Collate;
    if WindowsID <> -1 then begin
       RecalculatePageSizes;
    end;
  end;
end;

Procedure TPageSetup.SetOptionsToPrinter;
begin
  if (FPrinter<>NIL) and not FPrinter.Printing then
  begin
    FPrinter.Copies:= FCopies;
    FPrinter.Collate:= FSortCopies;
    FPrinter.PaperFormat:= FCurrentSize.WindowsID;
    FPrinter.Orientation:= FOrientation;
  end;
end;
//-- Glukhov PageSetupCorrection Build#175 25.01.02

procedure TPageSetup.SetPrinter(const Value: TWPrinter);
var LastFormat     : Integer;
begin
   LastFormat:= WindowsID;
   FPrinter:= Value;
   UpdatePaperFormats;
   WindowsID:= LastFormat;
end;

function TPageSetup.GetFramesInside: Boolean;
var ARect          : TRect;
begin
   if FPrinter=NIL then
      Result:=TRUE
   else
      begin
        with FPageFrames do
           ARect:=Rect(Round(Left*10),Round(Top*10),
                       Round(FPrinter.PageSize.Right-Right*10),
                       Round(FPrinter.PageSize.Bottom-Bottom*10));
        Result:=RectInside(ARect,Printer.PrintableArea);
      end;
end;

function TPageSetup.GetInterFramesInside: Boolean;
begin
   Result:=TRUE;
end;

end.
