Unit SelectOptionsDlg;

Interface

Uses Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     ExtCtrls,StdCtrls,WCtrls,AM_Def,Palette, ProjStyle, MultiLng,PropCollect,
  Validate;

Type TSelectOptionsDialog = class(TWForm)
       ControlPanel1    : TPanel;
       ControlPanel2    : TPanel;
       Group5           : TWGroupBox;
       FillStyleBtn     : TButton;
       LineStyleBtn     : TButton;
       Group6           : TWGroupBox;
       DBWorkingLayerRadio: TRadioButton;
       DBAllLayerRadio  : TRadioButton;
       Group7           : TWGroupBox;
       ObjectCopiesCheck: TCheckBox;
       OneLayerCheck    : TCheckBox;
       OneObjectCheck   : TCheckBox;
       UseGlobalCheck   : TCheckBox;
       Bevel1           : TBevel;
       OKBtn            : TButton;
       CancelBtn        : TButton;
       MlgSection       : TMlgSection;
       Procedure FormCreate(Sender: TObject);
       Procedure FormDestroy(Sender: TObject);
       Procedure FormHide(Sender: TObject);
       Procedure FormShow(Sender: TObject);
       Procedure LineStyleBtnClick(Sender: TObject);
       Procedure FillStyleBtnClick(Sender: TObject);
       Procedure OneLayerCheckClick(Sender: TObject);
       Procedure OneObjectCheckClick(Sender: TObject);
       Procedure UseGlobalCheckClick(Sender: TObject);
      Private
       FAllowExtendedStyles  : Boolean;
       FOptions         : TSelectionSettings;
       FOrgOptions      : PSelectionSettings;
       FOrgProjStyles   : TProjectStyles;
       FProjStyles      : TProjectStyles;
       Function    GetOptions:PSelectionSettings;
      Public
       Property    AllowExtendedStyles:Boolean read FAllowExtendedStyles write FAllowExtendedStyles default TRUE;
       Property    SelectionSettings:PSelectionSettings read GetOptions write FOrgOptions;
       Property    ProjectStyles:TProjectStyles read FProjStyles write FOrgProjStyles;
     end;

Implementation

{$R *.DFM}

Uses FillDlg,LineDlg,StyleDef;

Procedure TSelectOptionsDialog.FormCreate(Sender: TObject);
begin
  FAllowExtendedStyles:=TRUE;
  FOptions.LineStyle:=DefaultLineStyle;
  FOptions.FillStyle:=DefaultFillStyle;
  FProjStyles:=TProjectStyles.Create;
end;

Procedure TSelectOptionsDialog.FormDestroy(Sender: TObject);
begin
  FProjStyles.Free;
end;

Procedure TSelectOptionsDialog.LineStyleBtnClick(Sender: TObject);
  var Dialog       : TLineStyleDialog;
  begin
    Dialog:=TLineStyleDialog.Create(Self);
    try
      Dialog.AllowXLineStyles:=FAllowExtendedStyles;
      Dialog.ProjectStyles:=FProjStyles;
      with Dialog.LineStyle do begin
        PropertyMode:=pmSet;
        Style:=FOptions.LineStyle.Style;
        Color:=FOptions.LineStyle.Color;
        FillColor:=FOptions.LineStyle.FillColor;
        Width:=FOptions.LineStyle.Width;
        WidthType:=FOptions.LineStyle.WidthType;
      end;
      if Dialog.ShowModal=mrOK then with Dialog.LineStyle do begin
        FOptions.LineStyle.Style:=Style;
        FOptions.LineStyle.Color:=Color;
        FOptions.LineStyle.FillColor:=FillColor;
        FOptions.LineStyle.Width:=Width;
        FOptions.LineStyle.WidthType:=WidthType;
      end;
    finally
      Dialog.Free;
    end;
  end;

Procedure TSelectOptionsDialog.FillStyleBtnClick(Sender: TObject);
  var Dialog   : TPatternDialog;
  begin
    Dialog:=TPatternDialog.Create(Self);
    try
      Dialog.AllowXFillStyles:=FAllowExtendedStyles;
      Dialog.ProjectStyles:=FProjStyles;
      with Dialog.FillStyle do begin
        Pattern:=FOptions.Fillstyle.Pattern;
        ForeColor:=FOptions.Fillstyle.ForeColor;
        BackColor:=FOptions.Fillstyle.BackColor;
      end;
      if Dialog.ShowModal=mrOK then with Dialog.FillStyle do begin
        FOptions.Fillstyle.Pattern:=Pattern;
        FOptions.Fillstyle.BackColor:=BackColor;
        FOptions.Fillstyle.ForeColor:=ForeColor;
      end;
    finally
      Dialog.Free;
    end;    
  end;

Procedure TSelectOptionsDialog.UseGlobalCheckClick(Sender: TObject);
  begin
    SetControlGroupEnabled(ControlPanel2,not UseGlobalCheck.Checked);
  end;

Procedure TSelectOptionsDialog.FormShow(Sender: TObject);
  begin
    if FOrgProjStyles<>NIL then FProjStyles.Assign(FOrgProjStyles);
    if FOrgOptions<>NIL then FOptions:=FOrgOptions^;
    with FOptions do begin
      UseGlobalCheck.Checked:=UseGlobals;
      if UseGlobals then UseGlobalCheckClick(UseGlobalCheck);
      if DBSelectionMode then DBAllLayerRadio.Checked:=TRUE
      else DBWorkingLayerRadio.Checked:=TRUE;
      ObjectCopiesCheck.Checked:=TransparentMode and $80 <> 0;
      OneObjectCheck.Checked:=TransparentMode and $1 <> 0;
      OneLayerCheck.Checked:=TransparentMode and $2 <> 0;
      OneLayerCheckClick(Self);
      OneObjectCheckClick(Self);
    end;
  end;

Function TSelectOptionsDialog.GetOptions
   : PSelectionSettings;
  begin
    Result:=@FOptions;
  end;

Procedure TSelectOptionsDialog.FormHide(Sender: TObject);
begin
  if ModalResult=mrOK then with FOptions do begin
    UseGlobals:=UseGlobalCheck.Checked;
    DBSelectionMode:=DBAllLayerRadio.Checked;
    if OneObjectCheck.Checked then TransparentMode:=$1
    else if OneLayerCheck.Checked then TransparentMode:=$2
    else TransparentMode:=$4;
    if ObjectCopiesCheck.Checked then TransparentMode:=TransparentMode or $80
    else TransparentMode:=TransparentMode or $40;
    if FOrgProjStyles<>NIL then FOrgProjStyles.Assign(FProjStyles);
    if FOrgOptions<>NIL then FOrgOptions^:=FOptions;
  end;
end;
 
Procedure TSelectOptionsDialog.OneLayerCheckClick(Sender: TObject);
begin
  if OneLayerCheck.Checked then begin
    ObjectCopiesCheck.Checked:=FALSE;
    ObjectCopiesCheck.Enabled:=FALSE;
  end
  else ObjectCopiesCheck.Enabled:=not UseGlobalCheck.Checked;
end;

Procedure TSelectOptionsDialog.OneObjectCheckClick(Sender: TObject);
begin
  if OneObjectCheck.Checked then begin
    OneLayerCheck.Checked:=TRUE;
    OneLayerCheck.Enabled:=FALSE;
  end
  else OneLayerCheck.Enabled:=not UseGlobalCheck.Checked;
  OneLayerCheckClick(Sender);
end;

end.
