Unit Clipping;

Interface

Uses WinProcs,WinTypes,Classes,GrTools;

Type TClippingInfo      = Record
       FirstVisible     : Boolean;               
       Points           : Array[0..8] of TGrPoint;
       EdgeInfo         : Array[0..8] of Boolean
     end;

     TClipPointList     = TGrPointList;
 
Function ClipCircle(Const ACenter:TGrPoint;ARadius:Double;Const ClipRect:TRect;
    var ClippingInfo:TClippingInfo):Integer;

Function ClipArc(Const ACenter:TGrPoint;Const ARadius:Double;ABeginAngle:Double;
   Const AEndAngle:Double;Const ClipRect:TRect;var ClippingInfo:TClippingInfo):Integer;

Procedure ClipPolyline(var ClipPoints:TClipPointList;const ClipArea:TRect);

Procedure ClipPolygon(var ClipPoints:TClipPointList;const ClipArea:TRect);

Implementation

Procedure SwapLists
   (
   var List1       : TClipPointList;
   var List2       : TClipPointList
   );
  var Swap         : TClipPointList;
  begin
    Swap:=List1;
    List1:=List2;
    List2:=Swap;
  end;

Procedure ClipPolyline(var ClipPoints:TClipPointList;const ClipArea:TRect);
  var Cnt          : Integer;
      OutList      : TClipPointList;
      d,k          : Double;
      hx           : Double;
      hy           : Double;
      v1           : TGrPoint;
      v2           : TGrPoint;
  begin
    { create list for output-points }
    OutList:=TClipPointList.Create;
    OutList.Capacity:=ClipPoints.Capacity;

    with ClipArea do begin
      { clip to left border }
      if ClipPoints.Count>0 then begin
        v2:=ClipPoints[0];
        for Cnt:=1 to ClipPoints.Count-1 do begin
          v1:=v2;
          v2:=ClipPoints[Cnt];
          if v1.x>=Left then begin
            if v2.x>=Left then begin
              if OutList.Count=0 then OutList.Add(v1);
              OutList.Add(v2);
            end
            else if v2.x<Left then begin
              if (v1.y<>v2.y) then begin
                k:=(v2.y-v1.y)/(v2.x-v1.x);
                d:=v1.y-k*v1.x;
                hy:=k*Left+d
              end  
              else hy:=v1.y;
              if OutList.Count=0 then OutList.Add(v1);
              OutList.Add(GrPoint(Left,hy));
            end;
          end
          else if v2.x>Left then begin
            if (v1.y<>v2.y) then begin
              k:=(v2.y-v1.y)/(v2.x-v1.x);
              d:=v1.y-k*v1.x;
              hy:=k*Left+d
            end  
            else hy:=v1.y;
            OutList.Add(GrPoint(Left,hy));
            OutList.Add(v2);
          end;
        end;
        SwapLists(ClipPoints,OutList);
        OutList.Clear;
      end;
      { clip to the right border }
      if ClipPoints.Count>0 then begin
        v2:=ClipPoints[0];
        for Cnt:=1 to ClipPoints.Count-1 do begin
          v1:=v2;
          v2:=ClipPoints[Cnt];
          if v1.x<=Right then begin
            if v2.x<=Right then begin
              if OutList.Count=0 then OutList.Add(v1);
              OutList.Add(v2);
            end
            else if v2.x>Right then begin
              if (v1.y<>v2.y) then begin
                k:=(v2.y-v1.y)/(v2.x-v1.x);
                d:=v1.y-k*v1.x;
                hy:=k*Right+d
              end  
              else hy:=v1.y;
              if OutList.Count=0 then OutList.Add(v1);
              OutList.Add(GrPoint(Right,hy));
            end;
          end  
          else if v2.x<Right then begin
            if (v1.y<>v2.y) then begin
              k:=(v2.y-v1.y)/(v2.x-v1.x);
              d:=v1.y-k*v1.x;
              hy:=k*Right+d
            end  
            else hy:=v1.y;
            OutList.Add(GrPoint(Right,hy));
            OutList.Add(v2);
          end;
        end;  
        SwapLists(ClipPoints,OutList);
        OutList.Clear;
      end;
      { clip to bottom border }
      if ClipPoints.Count>0 then begin
        v2:=ClipPoints[0];
        for Cnt:=1 to ClipPoints.Count-1 do begin
          v1:=v2;
          v2:=ClipPoints[Cnt];
          if v1.y>=Bottom then begin
            if v2.y>=Bottom then begin
              if OutList.Count=0 then OutList.Add(v1);
              OutList.Add(v2);
            end
            else if v2.y<Bottom then begin
              if (v1.x<>v2.x) then begin
                k:=(v2.y-v1.y)/(v2.x-v1.x);
                d:=v1.y-k*v1.x;
                hx:=(Bottom-d)/k
              end  
              else hx:=v1.x;
              OutList.Add(v1);
              OutList.Add(GrPoint(hx,Bottom));
            end;
          end
          else if v2.y>Bottom then begin
            if (v1.x<>v2.x) then begin
              k:=(v2.y-v1.y)/(v2.x-v1.x);
              d:=v1.y-k*v1.x;
              hx:=(Bottom-d)/k
            end  
            else hx:=v1.x;
            OutList.Add(GrPoint(hx,Bottom));
            OutList.Add(v2);
          end;
        end;
        SwapLists(ClipPoints,OutList);
        OutList.Clear;
      end;
      { clip to top border }
      if ClipPoints.Count>0 then begin
        v2:=ClipPoints[0];
        for Cnt:=1 to ClipPoints.Count-1 do begin
          v1:=v2;
          v2:=ClipPoints[Cnt];
          if v1.y<=Top then begin
            if v2.y<=Top then begin
              if OutList.Count=0 then OutList.Add(v1);
              OutList.Add(v2);
            end
            else if v2.y>top then begin
              if (v1.x<>v2.x) then begin
                k:=(v2.y-v1.y)/(v2.x-v1.x);
                d:=v1.y-k*v1.x;
                hx:=(Top-d)/k
              end  
              else hx:=v1.x;
              OutList.Add(v1);
              OutList.Add(GrPoint(hx,Top));
            end;
          end
          else if (v1.y>Top) and (v2.y<Top) then begin
            if (v1.x<>v2.x) then begin
              k:=(v2.y-v1.y)/(v2.x-v1.x);
              d:=v1.y-k*v1.x;
              hx:=(Top-d)/k
            end
            else hx:=v1.x;
            OutList.Add(GrPoint(hx,Top));
            OutList.Add(v2);
          end;
        end;
        SwapLists(ClipPoints,OutList);
        OutList.Clear;
      end;
    end;  
    OutList.Free;
  end;

Procedure ClipPolygon(var ClipPoints:TClipPointList;const ClipArea:TRect);
  var Cnt          : Integer;
      OutList      : TClipPointList;
      d,k          : Double;
      hx           : Double;
      hy           : Double;
      v1           : TGrPoint;
      v2           : TGrPoint;
  begin
    { create list for output-points }
    OutList:=TClipPointList.Create;
    OutList.Capacity:=ClipPoints.Capacity;

    with ClipArea do begin
      { clip to left border }
      if ClipPoints.Count>0 then begin
        v2:=ClipPoints[ClipPoints.Count-1];
        for Cnt:=0 to ClipPoints.Count-1 do begin
          v1:=v2;
          v2:=ClipPoints[Cnt];
          if v1.x>=Left then begin
            if v2.x>=Left then OutList.Add(v2)
            else if v2.x<Left then begin
              if (v1.y<>v2.y) then begin
                k:=(v2.y-v1.y)/(v2.x-v1.x);
                d:=v1.y-k*v1.x;
                hy:=k*Left+d
              end
              else hy:=v1.y;
              OutList.Add(GrPoint(Left,hy));
            end;
          end
          else if v2.x>Left then begin
            if (v1.y<>v2.y) then begin
              k:=(v2.y-v1.y)/(v2.x-v1.x);
              d:=v1.y-k*v1.x;
              hy:=k*Left+d
            end
            else hy:=v1.y;
            OutList.Add(GrPoint(Left,hy));
            OutList.Add(v2);
          end;
        end;
        SwapLists(ClipPoints,OutList);
        OutList.Clear;
      end;
      { clip to the right border }
      if ClipPoints.Count>0 then begin
        v2:=ClipPoints[ClipPoints.Count-1];
        for Cnt:=0 to ClipPoints.Count-1 do begin
          v1:=v2;
          v2:=ClipPoints[Cnt];
          if v1.x<=Right then begin
            if v2.x<=Right then OutList.Add(v2)
            else if v2.x>Right then begin
              if (v1.y<>v2.y) then begin
                k:=(v2.y-v1.y)/(v2.x-v1.x);
                d:=v1.y-k*v1.x;
                hy:=k*Right+d
              end
              else hy:=v1.y;
              OutList.Add(GrPoint(Right,hy));
            end;
          end
          else if v2.x<Right then begin
            if (v1.y<>v2.y) then begin
              k:=(v2.y-v1.y)/(v2.x-v1.x);
              d:=v1.y-k*v1.x;
              hy:=k*Right+d
            end
            else hy:=v1.y;
            OutList.Add(GrPoint(Right,hy));
            OutList.Add(v2);
          end;
        end;
        SwapLists(ClipPoints,OutList);
        OutList.Clear;
      end;
      { clip to bottom border }
      if ClipPoints.Count>0 then begin
        v2:=ClipPoints[ClipPoints.Count-1];
        for Cnt:=0 to ClipPoints.Count-1 do begin
          v1:=v2;
          v2:=ClipPoints[Cnt];
          if v1.y>=Bottom then begin
            if v2.y>=Bottom then OutList.Add(v2)
            else if v2.y<Bottom then begin
              if (v1.x<>v2.x) then begin
                k:=(v2.y-v1.y)/(v2.x-v1.x);
                d:=v1.y-k*v1.x;
                hx:=(Bottom-d)/k
              end
              else hx:=v1.x;
              OutList.Add(GrPoint(hx,Bottom));
            end;
          end
          else if v2.y>Bottom then begin
            if (v1.x<>v2.x) then begin
              k:=(v2.y-v1.y)/(v2.x-v1.x);
              d:=v1.y-k*v1.x;
              hx:=(Bottom-d)/k
            end
            else hx:=v1.x;
            OutList.Add(GrPoint(hx,Bottom));
            OutList.Add(v2);
          end;
        end;
        SwapLists(ClipPoints,OutList);
        OutList.Clear;
      end;
      { clip to top border }
      if ClipPoints.Count>0 then begin
        v2:=ClipPoints[ClipPoints.Count-1];
        for Cnt:=0 to ClipPoints.Count-1 do begin
          v1:=v2;
          v2:=ClipPoints[Cnt];
          if v1.y<=Top then begin
            if v2.y<=Top then OutList.Add(v2)
            else if v2.y>top then begin
              if (v1.x<>v2.x) then begin
                k:=(v2.y-v1.y)/(v2.x-v1.x);
                d:=v1.y-k*v1.x;
                hx:=(Top-d)/k
              end
              else hx:=v1.x;
              OutList.Add(GrPoint(hx,Top));
            end;
          end
          else if (v1.y>Top) and (v2.y<Top) then begin
            if (v1.x<>v2.x) then begin
              k:=(v2.y-v1.y)/(v2.x-v1.x);
              d:=v1.y-k*v1.x;
              hx:=(Top-d)/k
            end
            else hx:=v1.x;
            OutList.Add(GrPoint(hx,Top));
            OutList.Add(v2);
          end;
        end;
        SwapLists(ClipPoints,OutList);
        OutList.Clear;
      end;
    end;
    { close the polygon }
    if ClipPoints.Count>0 then ClipPoints.Add(ClipPoints[0]);  
    OutList.Free;
  end;

Function ClipCircle
   (
   Const ACenter   : TGrPoint;
   ARadius         : Double;
   Const ClipRect  : TRect;
   var ClippingInfo: TClippingInfo
   )
   : Integer;
  var P1           : TGrPoint;
      P2           : TGrPoint;
      CutCnt       : Integer;
      Cut1         : TGrPoint;
      Cut2         : TGrPoint;
  Procedure AddPoint
     (
     Const Point   : TGrPoint;
     IsEdge        : Boolean;
     Visible       : Boolean
     );
    begin
      if (Result=0) or not GrPointsEqual(Point,ClippingInfo.Points[Result-1]) then begin
        ClippingInfo.Points[Result]:=Point;
        ClippingInfo.EdgeInfo[Result]:=IsEdge;
        if Result=0 then ClippingInfo.FirstVisible:=Visible;  
        Inc(Result);
      end;
    end;
  begin
    Result:=0;
    { clip with bottom-line }
    P1:=GrPoint(ClipRect.Left,ClipRect.Bottom);
    P2:=GrPoint(ClipRect.Right,ClipRect.Bottom);
    CutCnt:=CircleVectorIntersection(ACenter,ARadius,P1,P2,Cut1,Cut2);
    if CutCnt>1 then begin
      if Cut1.X<=ClipRect.Right then if Cut2.X>=ClipRect.Left then begin
        if Cut1.X<=ClipRect.Left then AddPoint(GrPoint(ClipRect.Left,ClipRect.Bottom),TRUE,TRUE)
        else AddPoint(Cut1,FALSE,FALSE);
        if Cut2.X>=ClipRect.Right then AddPoint(GrPoint(ClipRect.Right,ClipRect.Bottom),TRUE,TRUE)
        else AddPoint(Cut2,FALSE,TRUE);
      end;
    end;
    { clip with right-line }
    P1:=GrPoint(ClipRect.Right,ClipRect.Bottom);
    P2:=GrPoint(ClipRect.Right,ClipRect.Top);
    CutCnt:=CircleVectorIntersection(ACenter,ARadius,P1,P2,Cut1,Cut2);
    if CutCnt>1 then begin
      if Cut1.Y<=ClipRect.Top then if Cut2.Y>=ClipRect.Bottom then begin
        if Cut1.Y<=ClipRect.Bottom then AddPoint(GrPoint(ClipRect.Right,ClipRect.Bottom),TRUE,TRUE)
        else AddPoint(Cut1,FALSE,FALSE);
        if Cut2.Y>=ClipRect.Top then AddPoint(GrPoint(ClipRect.Right,ClipRect.Top),TRUE,TRUE)
        else AddPoint(Cut2,FALSE,TRUE);
      end;
    end;
    { clip with top-line }
    P1:=GrPoint(ClipRect.Right,ClipRect.Top);
    P2:=GrPoint(ClipRect.Left,ClipRect.Top);
    CutCnt:=CircleVectorIntersection(ACenter,ARadius,P1,P2,Cut1,Cut2);
    if CutCnt>1 then begin
      if Cut1.X>=ClipRect.Left then if Cut2.X<=ClipRect.Right then begin
        if Cut1.X>=ClipRect.Right then AddPoint(GrPoint(ClipRect.Right,ClipRect.Top),TRUE,TRUE)
        else AddPoint(Cut1,FALSE,FALSE);
        if Cut2.X<=ClipRect.Left then AddPoint(GrPoint(ClipRect.Left,ClipRect.Top),TRUE,TRUE)
        else AddPoint(Cut2,FALSE,TRUE);
      end;  
    end;
    { clip with left-line }
    P1:=GrPoint(ClipRect.Left,ClipRect.Top);
    P2:=GrPoint(ClipRect.Left,ClipRect.Bottom);
    CutCnt:=CircleVectorIntersection(ACenter,ARadius,P1,P2,Cut1,Cut2);
    if CutCnt>1 then begin
      if Cut1.Y>=ClipRect.Bottom then if Cut2.Y<=ClipRect.Top then begin
        if Cut1.Y>=ClipRect.Top then AddPoint(GrPoint(ClipRect.Left,ClipRect.Top),TRUE,TRUE)
        else AddPoint(Cut1,FALSE,FALSE);
        if Cut2.Y<=ClipRect.Bottom then AddPoint(GrPoint(ClipRect.Left,ClipRect.Bottom),TRUE,TRUE)
        else AddPoint(Cut2,FALSE,TRUE);
      end;
    end;
  end;

Function ClipArc
   (
   Const ACenter        : TGrPoint;
   Const ARadius        : Double;
   ABeginAngle          : Double;
   Const AEndAngle      : Double;
   Const ClipRect       : TRect;
   var ClippingInfo     : TClippingInfo
   )
   : Integer;
  var P1           : TGrPoint;
      P2           : TGrPoint;
      CutCnt       : Integer;
      Cut1         : TGrPoint;
      Cut2         : TGrPoint;
      SegmentInfo  : TClippingInfo;
      SegmentCount : Integer;
  Function PointOnEllipseArc
     (
     Const Point   : TGrPoint
     )
     : Boolean;
    var Angle      : Double;
    begin
      Angle:=LineAngle(ACenter,Point); if Angle<0 then Angle:=Angle+2*Pi;
      if Angle>AEndAngle then Angle:=Angle-2*Pi;
      Result:=(Angle>=ABeginAngle) and (Angle<=AEndAngle);
    end;
  Function PointOnArc
    (
    Const BeginPoint    : TGrPoint;
    Const EndPoint      : TGrPoint;
    Angle3              : Double
    )
    : Boolean;
   var Angle1           : Double;
       Angle2           : Double;
   begin
      Angle1:=LineAngle(ACenter,BeginPoint); if Angle1<0 then Angle1:=Angle1+2*Pi;
      Angle2:=LineAngle(ACenter,EndPoint); if Angle2<0 then Angle2:=Angle2+2*Pi;
      if Angle3<0 then Angle3:=Angle3+2*Pi;
      if Angle1>Angle2 then Angle1:=Angle1-2*Pi;
      if Angle3>Angle2 then Angle3:=Angle3-2*Pi;
      Result:=(Angle3>=Angle1) and (Angle3<=Angle2);
   end;
  Procedure AddPoint
     (
     Const Point   : TGrPoint;
     IsEdge        : Boolean;
     Visible       : Boolean
     );
    begin
      if (Result=0) or not GrPointsEqual(Point,ClippingInfo.Points[Result-1]) then begin
        ClippingInfo.Points[Result]:=Point;
        ClippingInfo.EdgeInfo[Result]:=IsEdge;
        if Result=0 then ClippingInfo.FirstVisible:=Visible;
        Inc(Result);
      end;
    end;
  begin
    Result:=0;
    { first create list of visible segments } 
    if ABeginAngle>AEndAngle then ABeginAngle:=ABeginAngle-2*Pi;
    { clip with bottom-line }
    P1:=GrPoint(ClipRect.Left,ClipRect.Bottom);
    P2:=GrPoint(ClipRect.Right,ClipRect.Bottom);
    CutCnt:=CircleVectorIntersection(ACenter,ARadius,P1,P2,Cut1,Cut2);
    if CutCnt>1 then begin
      if Cut1.X<=ClipRect.Right then if Cut2.X>=ClipRect.Left then begin
        if Cut1.X>ClipRect.Left then AddPoint(Cut1,FALSE,FALSE);
        if Cut2.X<ClipRect.Right then AddPoint(Cut2,FALSE,TRUE);
      end;
    end;
    { clip with right-line }
    P1:=GrPoint(ClipRect.Right,ClipRect.Bottom);
    P2:=GrPoint(ClipRect.Right,ClipRect.Top);
    CutCnt:=CircleVectorIntersection(ACenter,ARadius,P1,P2,Cut1,Cut2);
    if CutCnt>1 then begin
      if Cut1.Y<=ClipRect.Top then if Cut2.Y>=ClipRect.Bottom then begin
        if Cut1.Y>ClipRect.Bottom then AddPoint(Cut1,FALSE,FALSE);
        if Cut2.Y<ClipRect.Top then AddPoint(Cut2,FALSE,TRUE);
      end;
    end;
    { clip with top-line }
    P1:=GrPoint(ClipRect.Right,ClipRect.Top);
    P2:=GrPoint(ClipRect.Left,ClipRect.Top);
    CutCnt:=CircleVectorIntersection(ACenter,ARadius,P1,P2,Cut1,Cut2);
    if CutCnt>1 then begin
      if Cut1.X>=ClipRect.Left then if Cut2.X<=ClipRect.Right then begin
        if Cut1.X<ClipRect.Right then AddPoint(Cut1,FALSE,FALSE);
        if Cut2.X>ClipRect.Left then AddPoint(Cut2,FALSE,TRUE);
      end;
    end;
    { clip with left-line }
    P1:=GrPoint(ClipRect.Left,ClipRect.Top);
    P2:=GrPoint(ClipRect.Left,ClipRect.Bottom);
    CutCnt:=CircleVectorIntersection(ACenter,ARadius,P1,P2,Cut1,Cut2);
    if CutCnt>1 then begin
      if Cut1.Y>=ClipRect.Bottom then if Cut2.Y<=ClipRect.Top then begin
        if Cut1.Y<ClipRect.Top then AddPoint(Cut1,FALSE,FALSE);
        if Cut2.Y>ClipRect.Bottom then AddPoint(Cut2,FALSE,TRUE);
      end;
    end;
    { delete segments not belonging to the arc and correct begin and end-point }
    SegmentInfo:=ClippingInfo;
    SegmentCount:=Result;
    { if not starting with visible segment copy last point to first }
    if SegmentCount>0 then with SegmentInfo do begin
      if not FirstVisible then begin
        Move(Points[0],Points[1],(SegmentCount-1)*SizeOf(TGrPoint));
        Points[0]:=ClippingInfo.Points[SegmentCount-1];
      end;
      Result:=0;
      for CutCnt:=0 to SegmentCount Div 2-1 do begin
        if PointOnEllipseArc(Points[CutCnt*2]) then
            AddPoint(Points[CutCnt*2],FALSE,TRUE);
        if PointOnArc(Points[CutCnt*2],Points[CutCnt*2+1],AEndAngle) then
            AddPoint(GrPoint(ACenter.X+ARadius*Cos(AEndAngle),
            ACenter.Y+ARadius*Sin(AEndAngle)),FALSE,FALSE);
        if PointOnArc(Points[CutCnt*2],Points[CutCnt*2+1],ABeginAngle) then
            AddPoint(GrPoint(ACenter.X+ARadius*Cos(ABeginAngle),
            ACenter.Y+ARadius*Sin(ABeginAngle)),FALSE,TRUE);
        if PointOnEllipseArc(Points[CutCnt*2+1]) then
            AddPoint(Points[CutCnt*2+1],FALSE,FALSE);
      end;
      if Result>0 then with ClippingInfo do if not FirstVisible then begin
        Cut1:=Points[Result-1];
        Move(Points[0],Points[1],(Result-1)*SizeOf(TGrPoint));
        Points[0]:=Cut1;
      end;
    end;
  end;

end.
