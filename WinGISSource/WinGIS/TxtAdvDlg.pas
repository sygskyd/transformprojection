//****************************************************************************
// Unit TxtAdvDlg
//----------------------------------------------------------------------------
// Provides a dialog that allows to set some additional text properties.
//----------------------------------------------------------------------------
// Modifications
//  01.07.2001, Y.Glukhov, creation
//****************************************************************************
Unit TxtAdvDlg;

Interface

Uses
  Windows, Messages, Classes, Controls, StdCtrls,
  ExtCtrls, WCtrls, Spinbtn, Validate, MultiLng,
  ProjHndl, AM_Def, AM_Proj, AM_Text, AM_Poly, AM_View;

// The procedure can be called either from the Menu handler or directly
Procedure ProcForTxtAdvancedHandler( Project: PProj; bOldDialog: Boolean );

Type TTxtAdvancedHandler = class(TProjMenuHandler)
    Procedure   OnStart; override;
end;

Type
  TTxtAdvDialog   = Class(TWForm)
    Bevel1        : TBevel;
    ControlPanel  : TPanel;
    ControlPanel1 : TPanel;
    MlgSection    : TMlgSection;
    grpSpace      : TWGroupBox;
    grpPoly       : TWGroupBox;
    valHSpace     : TMeasureLookupValidator;
    valVSpace     : TMeasureLookupValidator;
    CancelBtn     : TButton;
    OKBtn         : TButton;
    lblState      : TWLabel;
    edtHSpace     : TEdit;
    edtVSpace     : TEdit;
    spinHSpace    : TSpinBtn;
    spinVSpace    : TSpinBtn;
    WLabel1       : TWLabel;
    WLabel2       : TWLabel;
    WLabel3       : TWLabel;
    WLabel4       : TWLabel;
    WLabel5       : TWLabel;
    WLabel6       : TWLabel;
    WLabel7       : TWLabel;
    edtHShift     : TEdit;
    edtVShift     : TEdit;
    edtOffset     : TEdit;
    spinVShift    : TSpinBtn;
    spinHShift    : TSpinBtn;
    spinOffset    : TSpinBtn;
    valOffset     : TMeasureLookupValidator;
    valHShift     : TMeasureLookupValidator;
    valVShift     : TMeasureLookupValidator;

    Panel1        : TPanel;
    btnOriginal   : TButton;
    btnSpaced     : TButton;
    btnPolyMode   : TButton;
    chkUnder      : TCheckBox;
    chkRedir      : TCheckBox;
    btnSelPoly    : TButton;
    btnEntPoly: TButton;

    procedure FormShow(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnOriginalClick(Sender: TObject);
    procedure btnSpacedClick(Sender: TObject);
    procedure btnPolyModeClick(Sender: TObject);
    procedure DataChanged(Sender: TObject);
    procedure btnSelPolyClick(Sender: TObject);
    procedure btnEntPolyClick(Sender: TObject);

  Private
    bInitialized    : Boolean;
    bModified       : Boolean;
    bNeedRecalc     : Boolean;
    oldClipRect     : TDRect;

    function    CheckSpaceValues: Boolean;
    function    CheckPolyValues: Boolean;
    procedure   RefreshFormData;
    procedure   RedrawObject;
    procedure   RecalcObject(Sender: TObject);
    procedure   SetSelPolyMode;
    procedure   SetManPolyMode;
    procedure   ProcessNewMode;

  Public
    // The edited (external) text object
    oExtText        : PText;
    // Internal text object to save the original external data
    oIntText        : PText;
    Project         : PProj;
    oSelLine        : PView;
    bNeedRedraw     : Boolean;

    Procedure   SetNewCaption(ANewCaption:Integer);
  end;

Implementation

{$R *.DFM}

Uses
  Measures, NumTools, StrTools,
  AttachHndl;

//=============================================================================
//  TTxtAdvancedHandler
//=============================================================================

Procedure TTxtAdvancedHandler.OnStart;
begin
  ProcForTxtAdvancedHandler( Project, False );
end;

Procedure ProcForTxtAdvancedHandler( Project: PProj; bOldDialog: Boolean );
var
  SrcDialog   : TTxtAdvDialog;
  PAdvText    : PText;
  i           : Integer;
begin
  if bOldDialog then begin
  // Use the kept (Old) dialog
    SrcDialog:= TTxtAdvDialog( Project^.ActualData );
    Project^.ActualData:= nil;
    SrcDialog.bNeedRedraw:= True;
    Project^.SetActualMenu( mn_Select );
  end
  else begin
  // Create the New dialog
    // Store the text object to be edited
    PAdvText:= PText( Project.Layers.IndexObject( Project.PInfo, Project.SelList.At(0) ) );
    // Create dialog to edit the advanced text data
    SrcDialog:= TTxtAdvDialog.Create( Project.Parent );
    SrcDialog.Project:= Project;
    SrcDialog.oExtText:= PAdvText;
    SrcDialog.bNeedRedraw:= False;
  end;

  i:= SrcDialog.ShowModal;
  // Set the necessary mode
  if i = mrYes then begin
    // Enter a polyline
    SrcDialog.oSelLine:= nil;
    Project^.ActualData:= SrcDialog;
    Project^.SetActualMenu( mn_EntToDrawAlong );
    Exit;
  end;
  if i = mrNo then begin
    // Select a polyline
    SrcDialog.oSelLine:= nil;
    Project^.ActualData:= SrcDialog;
    Project^.SetActualMenu( mn_SelToDrawAlong );
    Exit;
  end;
  SrcDialog.Free;
  Project^.ActualData:= nil;
  Project^.SetActualMenu( mn_Select );
end;

//=============================================================================
//  TTxtAdvDialog
//=============================================================================

Procedure TTxtAdvDialog.FormCreate(Sender: TObject);
begin
  bInitialized:= False;
  bNeedRedraw:= False;
  oIntText:= nil;
  oSelLine:= nil;
end;

Procedure TTxtAdvDialog.FormDestroy(Sender: TObject);
begin
  oExtText:= nil;
  if Assigned(oIntText) then begin
    Dispose( oIntText, Done );
    oIntText:= nil;
  end;
  bInitialized:= False;
end;

Procedure TTxtAdvDialog.SetNewCaption(ANewCaption: Integer);
begin
  Caption:=MlgSection[ANewCaption];
end;

Procedure TTxtAdvDialog.RefreshFormData;
  begin
    lblState.Caption:= MlgStringList.Strings['TxtAdvDialog',4];
    lblState.Caption:= lblState.Caption + '  ' +
        MlgStringList.Strings['TxtAdvDialog',5+oExtText.fAdvMode];
//    WLblAttNum.Caption:= Format(
//      MlgStringList.Strings['AttPropertyDialog',7], [NumOfAttached] );

    btnSpaced.Caption:= MlgStringList.Strings['TxtAdvDialog',22];
    btnPolyMode.Caption:= MlgStringList.Strings['TxtAdvDialog',23];
    case oExtText.fAdvMode of
    cwTxtAdvSpaced:
      begin
        btnSpaced.Caption:= MlgStringList.Strings['TxtAdvDialog',20];
        btnOriginal.Enabled:= True;
        if bNeedRecalc
        then  btnSpaced.Enabled:= True
        else  btnSpaced.Enabled:= False;
        btnPolyMode.Enabled:= True;
        SetControlGroupEnabled( grpSpace, True );
        SetControlGroupEnabled( grpPoly, False );
      end;
    cwTxtAdvSelPoly,
    cwTxtAdvManPoly:
      begin
        btnPolyMode.Caption:= MlgStringList.Strings['TxtAdvDialog',20];
        btnOriginal.Enabled:= True;
        btnSpaced.Enabled:= True;
        if bNeedRecalc
        then  btnPolyMode.Enabled:= True
        else  btnPolyMode.Enabled:= False;
        SetControlGroupEnabled( grpSpace, True );
        SetControlGroupEnabled( grpPoly, True );
      end;
    else  // cwTxtAdvRegular
      begin
        btnOriginal.Enabled:= False;
        btnSpaced.Enabled:= True;
        btnPolyMode.Enabled:= True;
        SetControlGroupEnabled( grpSpace, False );
        SetControlGroupEnabled( grpPoly, False );
      end;
    end;
  end;

Procedure TTxtAdvDialog.FormShow(Sender: TObject);
var
  Cnt          : Integer;
begin
  if not bInitialized then begin
    // Save the external object into the internal one
    if Assigned(oIntText) then Dispose( oIntText, Done );
    oExtText.CheckTextState( Project.PInfo );
    oIntText:= New( PText, InitBy( oExtText ) );
    OldClipRect.InitByRect( oExtText.ClipRect );
    // Init Space values
    if Assigned( oExtText.TxtBrkData ) then begin
      valHSpace.Value:= oExtText.TxtBrkData.hSpace * 100;
      valVSpace.Value:= oExtText.TxtBrkData.vSpace * 100;
    end
    else begin
      valHSpace.Value:= 0;
      valVSpace.Value:= 0;
    end;
    if Assigned( oExtText.TxtPolyData ) then begin
      valOffset.Value:= Round( oExtText.TxtPolyData.Offset * 100 );
      valHShift[muCentimeters]:= oExtText.TxtPolyData.hShift;
      valVShift[muCentimeters]:= oExtText.TxtPolyData.vShift;
      chkUnder.Checked:= (oExtText.TxtPolyData.fVerAl = cwTxtAdvFVertAD);
      chkRedir.Checked:= oExtText.TxtPolyData.bRedir;
    end
    else begin
      valOffset.Value:= 0;
      valHShift[muCentimeters]:= 0;
      valVShift[muCentimeters]:= 0;
      chkUnder.Checked:= False;
      chkRedir.Checked:= False;
    end;
    // Init flags
    bModified:= False;
    bNeedRecalc:= False;
    bInitialized:= True;
  end;
  // Check option Redraw
  if bNeedRedraw then begin
    if Assigned( oSelLine ) then begin
      if oSelLine.Index <> 0
      then  SetSelPolyMode
      else  SetManPolyMode;
    end;
  end;
  RefreshFormData;
end;

Procedure TTxtAdvDialog.RecalcObject(Sender: TObject);
begin
  case oExtText.fAdvMode of
    cwTxtAdvSpaced:
      btnSpacedClick( Sender );
    cwTxtAdvSelPoly:
      SetSelPolyMode;
    cwTxtAdvManPoly:
      SetManPolyMode;
    else  // cwTxtAdvRegular
      btnOriginalClick( Sender );
  end;
end;

Procedure TTxtAdvDialog.FormHide(Sender: TObject);
begin
  case ModalResult of
  mrOK:
    begin
      if bNeedRecalc then RecalcObject( Sender );
    end;
  mrCancel:
    begin
      if bModified then begin
        oExtText.Assign( oIntText );
        RedrawObject;
      end;
    end;
  end;
//  bInitialized:= False;
end;

procedure TTxtAdvDialog.FormCloseQuery( Sender: TObject; var CanClose: Boolean );
begin
  CanClose:= CheckValidators(Self);
end;

procedure TTxtAdvDialog.ProcessNewMode;
begin
  bModified:= True;
  bNeedRecalc:= False;
  RedrawObject;
  RefreshFormData;
end;

Procedure TTxtAdvDialog.RedrawObject;
begin
  RedrawWithAllAttached( Project, oExtText, OldClipRect );
  OldClipRect.InitByRect( oExtText.ClipRect );
  bNeedRedraw:= False;
end;

procedure TTxtAdvDialog.btnOriginalClick(Sender: TObject);
begin
  oExtText.SetModeRegular;
  oExtText.CheckTextState( Project.PInfo );
  ProcessNewMode;
end;

function TTxtAdvDialog.CheckSpaceValues: Boolean;
begin
  Result:= False;
  if not valHSpace.ValidateInput then begin
    edtHSpace.SetFocus;
    Exit;
  end;
  if not valVSpace.ValidateInput then begin
    edtVSpace.SetFocus;
    Exit;
  end;
  Result:= True;
end;

function TTxtAdvDialog.CheckPolyValues: Boolean;
begin
  Result:= False;
  if not valOffset.ValidateInput then begin
    edtOffset.SetFocus;
    Exit;
  end;
  if not valHShift.ValidateInput then begin
    edtHShift.SetFocus;
    Exit;
  end;
  if not valVShift.ValidateInput then begin
    edtVShift.SetFocus;
    Exit;
  end;
  Result:= True;
end;

procedure TTxtAdvDialog.btnSpacedClick(Sender: TObject);
begin
  if not CheckSpaceValues then Exit;
  oExtText.SetModeSpaced( valHSpace.Value / 100, valVSpace.Value / 100 );
  oExtText.CheckTextState( Project.PInfo );
  ProcessNewMode;
end;

procedure TTxtAdvDialog.SetSelPolyMode;
begin
  if not CheckSpaceValues then Exit;
  if not CheckPolyValues then Exit;

  oExtText.SetModeSpaced( valHSpace.Value / 100, valVSpace.Value / 100 );
  if Assigned( oSelLine )
  then  oExtText.SetModeSelPoly( oSelLine.Index, oSelLine, valOffset.Value / 100 )
  else  oExtText.SetModeSelPoly( oExtText.TxtPolyData.IdxPoly, oExtText.TxtPolyData.ObjPoly, valOffset.Value / 100 );
  oExtText.TxtPolyData.dConvA:= 0;
  with Project.PInfo.VariousSettings do begin
    oExtText.TxtPolyData.bConvA:= bConvRelMistake;
    if bConvRelMistake
    then  oExtText.TxtPolyData.dConvA:= dbConvRelMistake
    else  oExtText.TxtPolyData.dConvA:= dbConvAbsMistake;
  end;
  oExtText.TxtPolyData.hShift:= Round( valHShift[muCentimeters] );
  oExtText.TxtPolyData.vShift:= Round( valVShift[muCentimeters] );
  oExtText.TxtPolyData.fVerAl:= cwTxtAdvFVertAU;
  if chkUnder.Checked then oExtText.TxtPolyData.fVerAl:= cwTxtAdvFVertAD;
  oExtText.TxtPolyData.bReDir:= chkRedir.Checked;
  oExtText.CheckTextState( Project.PInfo );
  ProcessNewMode;
  oSelLine:= nil;
end;

procedure TTxtAdvDialog.SetManPolyMode;
begin
  if not CheckSpaceValues then Exit;
  if not CheckPolyValues then Exit;

  oExtText.SetModeSpaced( valHSpace.Value / 100, valVSpace.Value / 100 );
  if Assigned( oSelLine )
  then  oExtText.SetModeManPoly( PPoly(oSelLine).Data, valOffset.Value / 100 )
  else  oExtText.SetModeManPoly( nil, valOffset.Value / 100 );
  oExtText.TxtPolyData.hShift:= Round( valHShift[muCentimeters] );
  oExtText.TxtPolyData.vShift:= Round( valVShift[muCentimeters] );
  oExtText.TxtPolyData.fVerAl:= cwTxtAdvFVertAU;
  if chkUnder.Checked then oExtText.TxtPolyData.fVerAl:= cwTxtAdvFVertAD;
  oExtText.TxtPolyData.bReDir:= chkRedir.Checked;
  oExtText.CheckTextState( Project.PInfo );
  ProcessNewMode;
  oSelLine:= nil;
end;

procedure TTxtAdvDialog.btnPolyModeClick(Sender: TObject);
begin
  if not Assigned( oExtText.TxtPolyData ) then begin
    SetManPolyMode;
    Exit;
  end;
  if oExtText.TxtPolyData.IdxPoly <> 0
  then  SetSelPolyMode
  else  SetManPolyMode;
end;

procedure TTxtAdvDialog.btnSelPolyClick(Sender: TObject);
begin
  valHShift[muCentimeters]:= 0;
  valVShift[muCentimeters]:= 0;
  ModalResult:= mrNo;
  Hide;
end;

procedure TTxtAdvDialog.btnEntPolyClick(Sender: TObject);
begin
  valHShift[muCentimeters]:= 0;
  valVShift[muCentimeters]:= 0;
  ModalResult:= mrYes;
  Hide;
end;

procedure TTxtAdvDialog.DataChanged(Sender: TObject);
begin
  bNeedRecalc:= True;
  RefreshFormData;
end;

//=============================================================================
//  Initialization
//=============================================================================
initialization
  begin
    TTxtAdvancedHandler.Registrate('EditTxtAdvanced');
  end;
end.
