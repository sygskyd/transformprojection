Unit AttachmentDlg;

Interface
                              
Uses WinProcs,WinTypes,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
  StdCtrls,FileTree,AM_MMedi,Objects,UserIntf,WCtrls,TreeView,SelectFolderDlg,Menus,
  MultiLng,RegDB;

{$H+}

Type TAttachmentDialog = Class(TWForm)
       AbsolutePathsMenu: TMenuItem;
       AddBtn           : TButton;
       AddMenu          : TMenuItem;
       CancelBtn        : TButton;
       ChangeSourceBtn  : TButton;
       ChangeSourceMenu : TMenuItem;
       DeleteBtn        : TButton;
       DeleteMenu       : TMenuItem;
       DirectorySelectDialog: TSelectFolderDialog;
       FilesList        : TFileTreeView;
       MlgSection       : TMlgSection;
       ModeMenu         : TMenuItem;
       N1               : TMenuItem;
       OkBtn            : TButton;
       OpenBtn: TButton;
       OpenDialog       : TOpenDialog;
       PopupMenu        : TPopupMenu;
       RelativePathsMenu: TMenuItem;
       N2: TMenuItem;
       OpenMenu: TMenuItem;
       Procedure   AbsolutePathsMenuClick(Sender: TObject);
       Procedure   AddBtnClick(Sender: TObject);
       Procedure   ChangeSourceBtnClick(Sender: TObject);
       Procedure   DeleteBtnClick(Sender: TObject);
       Procedure   FilesListClick(Sender: TObject);
       Procedure   FilesListDblClick(Sender: TObject);
       Procedure   FormCreate(Sender: TObject);
       Procedure   FormDestroy(Sender: TObject);
       Procedure   FormHide(Sender: TObject);
       Procedure   FormShow(Sender: TObject);
       Procedure   OpenBtnClick(Sender: TObject);
       Procedure   RelativePathsMenuClick(Sender: TObject);
      Private
       FDirectories     : Integer;
       FFiles           : Integer;
       FMMFiles         : PCollection;
       FModified        : Boolean;
       FObjectID        : LongInt;
       FRelativeTo      : String;
       FRegistry        : TCustomRegistry;
       FToDelete        : TWList;
       Procedure   UpdateEnabled;
       Procedure   UpdateSelectionInfo;
    procedure SetRelativeTo(const Value: String);
      Public
       Property    MMFiles:PCollection read FMMFiles write FMMFiles;
       Property    Modified:Boolean read FModified;
       Property    ObjectID:LongInt read FObjectID write FObjectID;
       Property    RelativeToPath:String read FRelativeTo write SetRelativeTo;
       Property    Registry:TCustomRegistry read FRegistry write FRegistry;
     end;

Implementation

{$R *.DFM}

Uses FileCtrl,ShellAPI,StrTools;

{******************************************************************************+
  Procedure TMMFileDialog.AddBtnClick
--------------------------------------------------------------------------------
  Called if the user wants to add files. Opens the dialog and creates new
  entries in the multimedia-files-list (dialog allows to select more than one
  file). Sets the modified-flag.
+******************************************************************************}
Procedure TAttachmentDialog.AddBtnClick(Sender: TObject);
var Cnt          : Integer;
    MMEntry      : PMMEntry;
    Buffer       : Array[0..255] of Char;
    Buffer1      : Array[0..255] of Char;
begin
  OpenDialog.Title:=MlgSection[11];
  if OpenDialog.Execute then with OpenDialog do begin
    FilesList.BeginUpdate;
    // create new entries in the multimedia-files-list
    for Cnt:=0 to Files.Count-1 do begin
      MMEntry:=New(PMMEntry,Init(ObjectID,StrNew(StrPCopy(Buffer,Files[Cnt])),
          StrNew(StrPCopy(Buffer1,ExtractRelativePath(FRelativeTo+'\',Files[Cnt])))));
      FilesList.Add(MMEntry^.FileName,'',MMEntry^.RelativeFileName,'',FMMFiles^.Count);
      FMMFiles^.Insert(MMEntry);
    end;
    // update the list-view
    FilesList.EndUpdate;
    // set the modified-flag
    FModified:=True;
  end;
end;                                                           

Procedure TAttachmentDialog.FormShow(Sender: TObject);
var Cnt          : Integer;
    MMEntry      : PMMEntry;
begin
  Screen.Cursor:=crHourGlass;
  FilesList.RelativeToPath:=FRelativeTo;
  try
    FilesList.BeginUpdate;
    FilesList.Clear;
    for Cnt:=0 to FMMFiles^.Count-1 do begin
      MMEntry:=FMMFiles^.At(Cnt);
      if MMEntry^.ID=ObjectID then FilesList.Add(StrPas(MMEntry^.FileName),
          '',StrPas(MMEntry^.RelativeFileName),'',Cnt);
    end;
    FilesList.EndUpdate;
  finally
    Screen.Cursor:=crDefault;
  end;
  // load dialog-settings from the registry
  with FRegistry do begin
    CurrentPath:='\User\WinGISUI\MultimediaFileDialog';
    FilesList.RelativePaths:=ReadBool('ShowRelativePaths');
    if LastError<>rdbeNoError then FilesList.RelativePaths:=TRUE;
    if FilesList.RelativePaths then RelativePathsMenu.Checked:=TRUE
    else AbsolutePathsMenu.Checked:=TRUE;
  end;  
  // update enable-disable-status of controls
  UpdateEnabled;
end;

Procedure TAttachmentDialog.FilesListClick(Sender: TObject);
begin
  UpdateEnabled;
end;

Procedure TAttachmentDialog.UpdateEnabled;
var AEnable      : Boolean;
begin
  with FilesList do begin
    UpdateSelectionInfo;
    AEnable:=(Tree.AllCount>0) and (SelCount>0) and ((FDirectories=0) or (FFiles=0));
    DeleteBtn.Enabled:=AEnable;
    DeleteMenu.Enabled:=AEnable;
    AEnable:=AEnable and (SelCount=1);
    ChangeSourceBtn.Enabled:=AEnable;
    ChangeSourceMenu.Enabled:=AEnable;
    OpenBtn.Enabled:=AEnable and (FDirectories=0);
    OpenMenu.Enabled:=AEnable and (FDirectories=0);
  end;
end;

Procedure TAttachmentDialog.UpdateSelectionInfo;
var Cnt          : Integer;
    FileTree     : TFileTree;
begin
  FFiles:=0;                                        
  FDirectories:=0;
  with FilesList do for Cnt:=0 to Items.Count-1 do if Selected[Cnt] then begin
    FileTree:=TFileTree(Tree.ExpandedItems[Cnt]);
    if FileTree.IsDirectory then Inc(FDirectories)
    else Inc(FFiles);
  end;
end;

Procedure TAttachmentDialog.DeleteBtnClick(Sender: TObject);
var MsgNumber    : Integer;
    Cnt          : Integer;
Procedure CopyTree
   (
   ATree         : TFileTree
   );
  var Cnt        : Integer;
  begin
    if ATree.IsDirectory then for Cnt:=0 to ATree.Count-1 do CopyTree(TFileTree(ATree.Items[Cnt]))
    else begin
      FToDelete.Add(ATree.FileInfo);
      FilesList.Files.Remove(ATree.FileInfo);
    end;
  end;
begin
  // update information about the selected files/directories
  UpdateSelectionInfo;
  // determine the number of the message to display
  if FFiles=1 then MsgNumber:=7
  else if FFiles>1 then MsgNumber:=8
  else if FDirectories=1 then MsgNumber:=9
  else MsgNumber:=10;
  if MessageDialog(MlgSection[MsgNumber],mtConfirmation,[mbYes,mbNo,mbCancel],0)=mrYes then begin
    FilesList.BeginUpdate;
    // copy all selected entries to the ToDelete-list
    for Cnt:=0 to FilesList.Tree.ExpandedCount-1 do if FilesList.Selected[Cnt] then
        CopyTree(TFileTree(FilesList.Tree.ExpandedItems[Cnt]));
    // update the list-view
    FilesList.EndUpdate;
    // set the modified-flag
    FModified:=True;
  end;
end;

Procedure TAttachmentDialog.FormCreate(Sender: TObject);
begin
  FToDelete:=TWList.Create;
end;

Procedure TAttachmentDialog.FormDestroy(Sender: TObject);
var Cnt          : Integer;
begin
  for Cnt:=0 to FToDelete.Count-1 do TFileInfo(FToDelete[Cnt]).Free;
  FToDelete.Free;
end;

Function SortReference(Item1,Item2:Pointer):Integer; Far;
begin
  if TFileInfo(Item1).Reference<TFileInfo(Item2).Reference then Result:=-1
  else if TFileInfo(Item1).Reference>TFileInfo(Item2).Reference then Result:=1
  else Result:=0;
end;

Procedure TAttachmentDialog.FormHide(Sender: TObject);
var AItem        : TFileInfo;
    Cnt          : Integer;
    MMEntry      : PMMEntry;
begin
  if ModalResult=mrOK then begin
    // update filenames of multimedia-entries from the tree
    for Cnt:=0 to FilesList.Files.Count-1 do begin
      AItem:=TFileInfo(FilesList.Files[Cnt]);
      MMEntry:=FMMFiles^.At(AItem.Reference);
      StrDispose(MMEntry^.FileName);
      StrDispose(MMEntry^.RelativeFileName);
      MMEntry^.FileName:=StrAlloc(Length(AItem.LongName)+1);
      MMEntry^.RelativeFileName:=StrAlloc(Length(AItem.RelativeLongName)+1);
      StrPCopy(MMEntry^.FileName,AItem.LongName);
      StrPCopy(MMEntry^.RelativeFileName,AItem.RelativeLongName);
    end;
    // remove deleted multimedia-entries from the list
    FToDelete.Sort(SortReference);
    for Cnt:=FToDelete.Count-1 downto 0 do begin
      AItem:=FToDelete[Cnt];
      FMMFiles^.AtFree(AItem.Reference)
    end;
    // store dialog-settings to the registry
    with FRegistry do begin
      CurrentPath:='\User\WinGISUI\MultimediaFileDialog';
      WriteBool('ShowRelativePaths',FilesList.RelativePaths);
    end;
  end;
end;

Procedure TAttachmentDialog.ChangeSourceBtnClick(Sender: TObject);
  var FileTree     : TFileTree;
  {****************************************************************************+
    ChangePathNames
  ------------------------------------------------------------------------------
    Replaces the path-names of all sub-entries of Tree with the newly selected
    path-name. The function is recursively called for all sub-directories of
    the tree.
  +****************************************************************************}
  Procedure ChangePathNames(Replace:Integer;Const ReplaceBy:String;Tree:TFileTree);
  var Cnt        : Integer;
      TreeItem   : TFileTree;
  begin
    for Cnt:=0 to Tree.Count-1 do begin
      TreeItem:=TFileTree(Tree[Cnt]);
      if TreeItem.IsDirectory then ChangePathNames(Replace,ReplaceBy,TreeItem)
      else begin
        TreeItem.FileInfo.LongName:=ReplaceBy+Copy(TreeItem.FileInfo.LongName,
            Replace+1,Length(TreeItem.FileInfo.LongName)-Replace);
        TreeItem.FileInfo.RelativeLongName:=ExtractRelativePath(FRelativeTo+'\',
            TreeItem.FileInfo.LongName);
      end;
    end;
  end;
  var ReplaceLength     : Integer;
      ReplaceWith       : String;
      FileInfo          : TFileInfo;
  begin
    with FilesList do if SelCount=1 then begin
      FileTree:=TFileTree(Tree.ExpandedItems[ItemIndex]);
      if FileTree.IsDirectory then begin
        ReplaceWith:=PathOfTree(FileTree);
        DirectorySelectDialog.FileName:='';
        DirectorySelectDialog.InitialDir:=ReplaceWith;
        if DirectorySelectDialog.Execute then begin
          BeginUpdate;
          ReplaceLength:=Length(ReplaceWith);
          if not IsSuffix('\',ReplaceWith,False) then Inc(ReplaceLength);
          ReplaceWith:=DirectorySelectDialog.FileName;
          if not IsSuffix('\',ReplaceWith,False) then ReplaceWith:=ReplaceWith+'\';
          ChangePathNames(ReplaceLength,ReplaceWith,FileTree);
          // update the list-view
          EndUpdate;
          // set the modified-flag
          FModified:=True;
        end;
      end
      else begin
        OpenDialog.Title:=MlgSection[12];
        FileInfo:=FileTree.FileInfo;
        OpenDialog.FileName:=FileInfo.Name(FALSE,FALSE);
        if OpenDialog.Execute then begin
          BeginUpdate;
          FileInfo.LongName:=OpenDialog.FileName;
          FileInfo.RelativeLongName:=ExtractRelativePath(FRelativeTo+'\',
              OpenDialog.FileName);
          // update the list-view
          EndUpdate;
          // set the modified-flag
          FModified:=True;
          // select the updated item
          FileTree:=FileInfoToTree(FileInfo);
          ItemIndex:=Tree.ExpandedIndexOf(FileTree);
        end;
      end;
    end;
  end;

Procedure TAttachmentDialog.AbsolutePathsMenuClick(Sender: TObject);
  begin
    FilesList.RelativePaths:=FALSE;
    AbsolutePathsMenu.Checked:=TRUE;
  end;

Procedure TAttachmentDialog.RelativePathsMenuClick(Sender: TObject);
  begin
    FilesList.RelativePaths:=TRUE;
    RelativePathsMenu.Checked:=TRUE;
  end;

Procedure TAttachmentDialog.OpenBtnClick(Sender: TObject);
var FileTree      : TFileTree;
    FileName      : String;
begin
  FileTree:=TFileTree(FilesList.Tree.ExpandedItems[FilesList.ItemIndex]);
  if not FileTree.IsDirectory then with FileTree.FileInfo do begin
    // look if file in relative path exists
    FileName:=FRelativeTo+'\'+FileTree.FileInfo.RelativeLongName;
    if not FileExists(FileName) then begin
      // look if file in absolute path exists
      FileName:=LongName;
      if not FileExists(FileName) then begin
        // file does not exist in relative or absolute path, show message
        FileName:='';
        MessageDialog(Format(MlgSection[19],[LongName,RelativeLongName]),
            mtInformation,[mbOK],0);
      end;  
    end;
    // if file found call default-viewer for the file-type
    if FileName<>'' then ShellExecute(Handle,'open',PChar(FileName),'','',SW_SHOWDEFAULT);
  end;
end;

procedure TAttachmentDialog.FilesListDblClick(Sender: TObject);
begin
  if OpenBtn.Enabled then OpenBtnClick(Sender);
end;

procedure TAttachmentDialog.SetRelativeTo(const Value: String);
begin
  FRelativeTo:=Value;                    
  if IsSuffix('\',FRelativeTo,False) then
    FRelativeTo:=Copy(FRelativeTo,1,Length(FRelativeTo)-1);
end;

end.
