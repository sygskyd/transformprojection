{*******************************************************************************
 Unit ViewHndl
--------------------------------------------------------------------------------
  Autor: Martin Forst
--------------------------------------------------------------------------------
  - Men�handler f�r die Zoom-Men�punkte.
--------------------------------------------------------------------------------
  Version 1.0 10-04-1997
+******************************************************************************}
Unit ViewHndl;

Interface
{$IFNDEF AXDLL} // <----------------- AXDLL

Uses AM_Proj,Classes,Controls,InpHndl,MenuHndl,ProjHndl;

Type TZoomHandler       = Class(TProjMenuHandler)
      Public
       Class Function HandlerType:Word; override;
       Procedure   OnKeyDown(var Key:Word;Shift:TShiftState); override;
       Function    OnMouseDown(Const X,Y:Double;Button:TMouseButton;Shift:TShiftState):Boolean; override;
       Function    OnMouseUp(Const X,Y:Double;Button:TMouseButton;Shift:TShiftState):Boolean; override;
       Procedure   OnStart; override;
       Procedure   OnReinvoke; override;
     end;

     TZoomScaleHandler  = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;

     TZoomInHandler     = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;

     TZoomOutHandler    = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;

     TZoomAllHandler    = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;

     TZoomSelectedHandler = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;

     TZoomNextSelectedHandler= Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;

     TSaveViewToFileHandler = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;

     TOpenAttachedDocumentHandler = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;

     TRotateViewHandler = Class(TProjMenuHandler)
      Private
       FDirection  : Boolean;
       {++Brovak}
       OldActualMen:integer;
       {--Brovak}
       Procedure   DoFinishedInput(Sender:TObject);
      Protected
       Procedure   DoRotateDialog(Const Rotation:Double);
      Public
       Class Function HandlerType:Word; override;
       Procedure   OnKeyDown(var Key:Word;Shift:TShiftState); override;
       Procedure   OnStart; override;
       {++Brovak}
       Function    OnMouseDown(Const X,Y:Double;Button:TMouseButton;Shift:TShiftState):Boolean; override;
       {--Brovak}
     end;

     TViewPanHandler    = Class(TProjMenuHandler)
      Private
//++ Glukhov DragView Build#150 30.01.01
        FActive        : Boolean;
        FStartX        : Double;
        FStartY        : Double;
        FEndX          : Double;
        FEndY          : Double;
        Procedure   DoFinishedInput;
      Protected
        Procedure   DrawBitmap;
        Procedure   DrawFrame;
//-- Glukhov DragView Build#150 30.01.01
      Public
        Class Function HandlerType:Word; override;
        Procedure   OnKeyDown(var Key:Word;Shift:TShiftState); override;
        Procedure   OnStart; override;
//++ Glukhov DragView Build#150 30.01.01
        Function    OnMouseDown(Const X,Y:Double;Button:TMouseButton;Shift:TShiftState):Boolean; override;
        Function    OnMouseUp  (Const X,Y:Double;Button:TMouseButton;Shift:TShiftState):Boolean; override;
        Function    OnMouseMove(Const X,Y:Double;Shift:TShiftState):Boolean; override;
      Public
        Property    Active: Boolean read FActive;
        Property    XStart: Double  read FStartX;
        Property    XEnd:   Double  read FEndX;
        Property    YStart: Double  read FStartY;
        Property    YEnd:   Double  read FEndY;
//-- Glukhov DragView Build#150 30.01.01
     end;

     TViewShowManagerHandler = Class(TMenuHandler)
      Public
       Procedure   OnStart; override;
     end;

     TLegendShowManagerHandler = Class(TMenuHandler)
      Public
       Procedure   OnStart; override;
     end;

Procedure UpdateLegends;

Procedure UpdateViews;

{$ENDIF} // <----------------- AXDLL
Implementation
{$IFNDEF AXDLL} // <----------------- AXDLL
Uses AM_Def,AM_ProjO,AM_ProjP,AM_Sight,Forms,GrTools,MenuFn,NumTools
     {$IFNDEF AXDLL} // <----------------- AXDLL
     ,ResDlg,RotDlg
     {$ENDIF} // <----------------- AXDLL
     ,Rollup
     {$IFNDEF AXDLL} // <----------------- AXDLL
     ,ScaleDlg
     {$ENDIF} // <----------------- AXDLL
     ,SysUtils,UserIntf,WCtrls,Windows
     {$IFNDEF WMLT}
     ,ViewManager
     ,LegendManager
     {$ENDIF}
     ,AM_Main,Graphics,Jpeg,Clipbrd,VEModule;

{$R *.R32}

{$R *.mfn}

{==============================================================================+
  Globale Variablen
+==============================================================================}

var FZoomInCursor   : THandle;
//++ Glukhov DragView Build#150 30.01.01
var FHandFlatCursor  : THandle;
var FHandGrabCursor  : THandle;
//-- Glukhov DragView Build#150 30.01.01

{==============================================================================+
  TZoomHandler
+==============================================================================}

Procedure TZoomHandler.OnReinvoke;
begin
  Status:=mhsReturnToPrevious;
end;

Procedure TZoomHandler.OnStart;
begin
  SubHandler:=TRubberBox.Create(Self);
  with SubHandler as TRubberBox do begin
    ActiveCursor:=FZoomInCursor;
    InActiveCursor:=FZoomInCursor;
    MinSizeReachedCursor:=FZoomInCursor;
    PositiveDimensions:=TRUE;
  end;
  Status:=mhsNotFinished;
  {$IFNDEF WMLT}
  StatusText:=MlgStringList['ViewHndl',10];
  {$ENDIF}
  Visible:=TRUE;
end;

Class Function TZoomHandler.HandlerType:Word;
begin
  Result:=mhtPushActive;
end;

Function TZoomHandler.OnMouseDown
   (
   Const X         : Double;
   Const Y         : Double;
   Button          : TMouseButton;
   Shift           : TShiftState
   )
   : Boolean;
  begin
    inherited OnMouseDown(X,Y,Button,Shift);
    with Project^ do if Button=mbRight then begin
      if ssShift in Shift then DoPopup(X,Y,'ZoomPopupMenu')
//++ Glukhov Bug#436 Build#166 09.10.01
{
      else if PreviousZooms.Count>0 then begin
        ZoomByRect(PRotRect(PreviousZooms[PreviousZooms.Count-1])^,0,TRUE);
        PreviousZooms.Delete(PreviousZooms.Count-1);
      end
}      
//-- Glukhov Bug#436 Build#166 09.10.01
      else ZoomByFactorWithCenter(X,Y,0.5);
    end
    else if Button=mbLeft then with SubHandler as TRubberBox do begin
      ViewRotation:=PInfo^.ViewRotation;
      MinSize:=10;
    end;
    Result:=TRUE;
  end;

Function TZoomHandler.OnMouseUp
   (
   Const X         : Double;
   Const Y         : Double;
   Button          : TMouseButton;
   Shift           : TShiftState
   )
   : Boolean;
  var Factor       : Integer;
  begin
    inherited OnMouseUp(X,Y,Button,Shift);
    if Button=mbLeft then with SubHandler as TRubberBox do begin
      if MinSizeReached then begin
//        Project^.PreviousZooms.Add(Project^.CurrentViewRect);
        Project^.ZoomByRect(RotRect(Position,Width,Height,-ViewRotation),0,TRUE);
      end
      else begin
        Factor:=2;
        if ssShift in Shift then Factor:=Factor*2;
        if ssCtrl in Shift then Factor:=Factor*4;
//        Project^.PreviousZooms.Add(Project^.CurrentViewRect);
        Project^.ZoomByFactorWithCenter(X,Y,Factor);
      end;
    end;
    Result:=TRUE;
  end;

Procedure TZoomHandler.OnKeyDown(var Key:Word;Shift:TShiftState);
begin
  {$IFNDEF WMLT}
  if Key=vk_Escape then Status:=mhsReturnToPrevious;
  {$ENDIF}
end;

{==============================================================================+
  TZoomScaleHandler
+==============================================================================}

Procedure TZoomScaleHandler.OnStart;
{$IFNDEF AXDLL} // <----------------- AXDLL
  var Dialog       : TScaleDialog;
{$ENDIF} // <----------------- AXDLL
  begin
{$IFNDEF AXDLL} // <----------------- AXDLL
    with Project^ do begin
      Dialog:=TScaleDialog.Create(Parent);
      try
        Dialog.Scale:=PInfo^.ZoomToScale(PInfo^.Zoom);
        Dialog.Registry:=Registry;
//++ Glukhov PreciseScale Build#161 08.06.01
        Dialog.dZoomLimit:= GetZoomLimit;
//-- Glukhov PreciseScale Build#161 08.06.01
        if Dialog.ShowModal=mrOK then begin
//          Project^.PreviousZooms.Add(CurrentViewRect);
          Project^.ZoomByFactor(Dialog.Scale/PInfo^.ZoomToScale(PInfo^.Zoom));
        end;
      finally
        Dialog.Free;
      end;
    end;
{$ENDIF} // <----------------- AXDLL
  end;

{==============================================================================+
  TZoomInHandler
+==============================================================================}

Procedure TZoomInHandler.OnStart;
var
BMFact: Double;
begin
//  Project^.PreviousZooms.Add(Project^.CurrentViewRect);
  BMFact:=VEForm.GetResFactor;
  Project^.ZoomByFactor(2.0*BMFact);
end;

{==============================================================================+
  TZoomOutHandler
+==============================================================================}

Procedure TZoomOutHandler.OnStart;
var
BMFact: Double;
begin
//++ Glukhov Bug#436 Build#166 09.10.01
{
  with Project^ do if PreviousZooms.Count>0 then begin
    ZoomByRect(PRotRect(PreviousZooms[PreviousZooms.Count-1])^,0,TRUE);
    PreviousZooms.Delete(PreviousZooms.Count-1);
  end
  else ZoomByFactor(0.5);
}
  BMFact:=VEForm.GetResFactor;
  Project.ZoomByFactor(0.5*BMFact);
//-- Glukhov Bug#436 Build#166 09.10.01
end;

{==============================================================================+
  TZoomAllHandler
+==============================================================================}

Procedure TZoomAllHandler.OnStart;
begin
  Project^.SetOverview;
end;

{==============================================================================+
  TZoomSelectedHandler
+==============================================================================}

Procedure TZoomSelectedHandler.OnStart;
begin
  {$IFNDEF WMLT}
  ProcShowAllSel(Project);
  {$ENDIF}
end;

{==============================================================================+
  TZoomNextSelectedHandler
+==============================================================================}

Procedure TZoomNextSelectedHandler.OnStart;
begin
  {$IFNDEF WMLT}
  ProcShowNextSel(Project);
  {$ENDIF}
end;

{==============================================================================+
  TViewPanHandler
+==============================================================================}

Procedure TViewPanHandler.OnStart;
  begin
//++ Glukhov DragView Build#150 30.01.01
    FActive:= False;
    Cursor:= FHandFlatCursor;
//-- Glukhov DragView Build#150 30.01.01
    Status:= mhsNotFinished;
{+++ Brovak BUG 524,539 BUILD 167}
     FAllowDblclick:=true;
 {-- Brovak }
    {$IFNDEF WMLT}
    StatusText:= MlgStringList['ViewHndl',10];
    {$ENDIF}
  end;

Class Function TViewPanHandler.HandlerType : Word;
  begin
    Result:=mhtPushActive or mhtDontPush;
  end;

Procedure TViewPanHandler.OnKeyDown (
   var Key         : Word;
   Shift           : TShiftState
   );
  begin
    if Key=vk_Escape then begin
//++ Glukhov DragView Build#150 30.01.01
        FEndX:= FStartX;
        FEndY:= FStartY;
        DrawBitmap;
//-- Glukhov DragView Build#150 30.01.01
{$IFNDEF WMLT}
        Status:=mhsReturnToPrevious;
{$ELSE}
        FActive:= False;
        Cursor:= FHandFlatCursor;
        FCursor:=Cursor;
{$ENDIF}
    end;
  end;

{
Procedure TViewPanHandler.DoFinishedInput;
var
DX,DY: Double;
begin
     DX:=FLastX-FStartX;
     DY:=FLastY-FStartY;

     MoveRect(Project.CurrentViewRect,-DX,-DY);
     Project.ZoomByRectWithoutCheck(Project.CurrentViewRect,0,False);

end;
}

//++ Glukhov DragView Build#150 06.02.01
Procedure TViewPanHandler.DoFinishedInput;
var
dx,dy: Double;
bAction: Boolean;
begin
     dx:= XStart-XEnd;
     dy:= YStart-YEnd;
     bAction:=(dx <> 0) or (dy <> 0);
     if bAction  then begin
        MoveRect(Project.CurrentViewRect,dx,dy);
        Project.ZoomByFactor(1.0);
     end
     else begin
        FEndX:=FStartX;
        FEndY:=FStartY;
        DrawBitmap;
     end;
end;
//-- Glukhov DragView Build#150 06.02.01

//++ Glukhov DragView Build#150 30.01.01
Function TViewPanHandler.OnMouseDown (
      Const X     : Double;
      Const Y     : Double;
      Button      : TMouseButton;
      Shift       : TShiftState
) : Boolean;
  begin
    Result:= False;
    if Button = mbLeft then begin
      if not FActive then begin
        FStartX:= X;
        FStartY:= Y;
        FEndX:= X;
        FEndY:= Y;
        FActive:= True;
        Cursor:= FHandGrabCursor;
        {+++ Brovak BUG 524 BUILD 167}
        FAllowDblclick:=false;
        {-- Brovak }
      end;
      Result:= True;
    end;
  end;

Function TViewPanHandler.OnMouseUp (
      Const X     : Double;
      Const Y     : Double;
      Button      : TMouseButton;
      Shift       : TShiftState
) : Boolean;
  begin
    Result:= False;
    if Button = mbLeft then begin
      if FActive then begin
        FEndX:= X;
        FEndY:= Y;
        FActive:= False;
        Cursor:= FHandFlatCursor;
        {+++ Brovak BUG 524 BUILD 167}
        FAllowDblclick:=true;
        {-- Brovak }
        DoFinishedInput;
      end;
      Result:= True;
    end;
  end;

Function TViewPanHandler.OnMouseMove (
    Const X         : Double;
    Const Y         : Double;
    Shift           : TShiftState
) : Boolean;
  begin
    Result:= False;
    if FActive then begin
      FEndX:= X;
      FEndY:= Y;
      DrawBitmap;
      DrawFrame;
      Result:= True;
    end;
  end;

Procedure TViewPanHandler.DrawBitmap;
{$IFNDEF AXDLL} // <----------------- AXDLL
  var
    dx1, dy1  : Double;
    dx2, dy2  : Double;
    DstRect   : TRect;
{$ENDIF} // <----------------- AXDLL
  begin
{$IFNDEF AXDLL} // <----------------- AXDLL
      InternalToWindowPoint( FStartX, FStartY, dx1, dy1 );
      InternalToWindowPoint( FEndX, FEndY, dx2, dy2 );
      Project.PInfo.ExtCanvas.DrawFromCacheWithOffset(
            Project.PInfo.ExtCanvas.LogicalSize,
            Round(dx2-dx1),
            Round(dy2-dy1) );
{$ENDIF} // <----------------- AXDLL
  end;

Procedure TViewPanHandler.DrawFrame;
  var
    dx1, dy1  : Double;
    dx2, dy2  : Double;
    DstRect   : TRect;
  begin
      InternalToWindowPoint( FStartX, FStartY, dx1, dy1 );
      InternalToWindowPoint( FEndX, FEndY, dx2, dy2 );
      DstRect:= Project.PInfo.ExtCanvas.LogicalSize;
      DstRect.Left:= DstRect.Left + Round(dx2-dx1);
      DstRect.Top:= DstRect.Top + Round(dy2-dy1);
      DstRect.Right:= DstRect.Right + Round(dx2-dx1);
      DstRect.Bottom:= DstRect.Bottom + Round(dy2-dy1);
      ActivateDrawingTools;
      Canvas.MoveTo(
            DstRect.Left, DstRect.Top );
      Canvas.LineTo(
            DstRect.Left, DstRect.Bottom );
      Canvas.LineTo(
            DstRect.Right, DstRect.Bottom );
      Canvas.LineTo(
            DstRect.Right, DstRect.Top );
      Canvas.LineTo(
            DstRect.Left, DstRect.Top );
      RestoreDrawingTools;
  end;

//-- Glukhov DragView Build#150 30.01.01

{==============================================================================+
  TRotateViewHandler
+==============================================================================}

Procedure TRotateViewHandler.DoRotateDialog
   (
   Const Rotation   : Double
   );
{$IFNDEF AXDLL} // <----------------- AXDLL
  var Dialog        : TRotateViewDialog;
      ModalResult   : TModalResult;
{$ENDIF} // <----------------- AXDLL
  begin
{$IFNDEF AXDLL} // <----------------- AXDLL
    Dialog:=TRotateViewDialog.Create(Parent);
    try
      Dialog.Rotation:=-Rotation;
      Dialog.Registry:=Project^.Registry;  
      ModalResult:=Dialog.ShowModal;
      FDirection:=Dialog.Direction;
      if ModalResult=mrOK then begin
{++ Ivanoff BUG#51 BUILD#95}
{If a angle of rotation equals 360 degrees, the angle should be equal zero.
Because in WinGIS ONLY sin360 not equals zero, it equals 2e-16!.}
        If DblCompare(abs(Dialog.Rotation), (2*PI)) = 0 Then Dialog.Rotation := 0;
{-- Ivanoff}
        if DblCompare(Project^.PInfo^.ViewRotation,Dialog.Rotation)<>0 then begin
          Project^.PInfo^.SetViewRotation(-Dialog.Rotation);
          Project^.ZoomByRect(Project^.CurrentViewRect,0,TRUE);
        end;
        Status:=mhsReturnToPrevious;
      end
      else if ModalResult=mrCancel then Status:=mhsReturnToPrevious
      else if ModalResult=mrYes then if SubHandler=NIL then begin
        SubHandler:=TRubberLine.Create(Self);
        with SubHandler as TRubberLine do begin
          InactiveCursor:=crArrow;
          ActiveCursor:=crArrow;
          TwoClicks:=TRUE;
          {++Brovak}
           ForRotate:=true;
           Statustext:=MlgStringList['Main', 79000];
          {--brovak}
          OnFinishedInput:=DoFinishedInput;
        end;
        Status:=mhsNotFinished;
      end;
    finally
      Dialog.Free;
    end;
{$ENDIF} // <----------------- AXDLL
  end;

Procedure TRotateViewHandler.OnStart;
  begin
  {++Brovak - for Snap}
    OldActualMen:=Project^.ActualMen;
    //Project^.ActualMen:=-99999; //any number<>MN_
  {--Brovak}
    DoRotateDialog(Project^.PInfo^.ViewRotation);
  end;

Procedure TRotateViewHandler.DoFinishedInput
  (
  Sender           : TObject
  );
  var Angle        : Double;
  begin
    with SubHandler as TRubberLine do begin
      Angle:=LineAngle(GrPoint(XStart,YStart),GrPoint(XEnd,YEnd));
      if FDirection then DoRotateDialog(-Angle+Pi/2)
      else DoRotateDialog(-Angle);
      {++Brovak - for Snap}
      Statustext:='';
      Project^.ActualMen:=OldActualMen;
      {--Brovak - for Snap}
    end;
  end;

Procedure TRotateViewHandler.OnKeyDown
   (
   var Key         : Word;
   Shift           : TShiftState
   );
  begin
    if Key=vk_Escape then begin
      end;
  end;
 {++Brovak}
Function TRotateViewHandler.OnMouseDown(Const X,Y:Double;Button:TMouseButton;Shift:TShiftState):Boolean;
begin
  inherited OnMouseDown(X,Y,Button,Shift);
 end;
 {--Brovak}
Class Function TRotateViewHandler.HandlerType:Word;
begin
  Result:=mhtPushActive;
end;

{==============================================================================+
  TViewShowManagerHandler
+==============================================================================}

Procedure TViewShowManagerHandler.OnStart;
var Rollup         : TRollupForm;
begin
  Rollup:=UserInterface.RollupNamed['ViewManagerRollupForm'];
  if Rollup<>NIL then Rollup.RollupToolWindow.Visible:=
      not Rollup.RollupToolWindow.Visible;
end;

{==================================================================================+
  TSaveViewToFileHandler  - Saves the current View as an image file or to Clipboard
+==================================================================================}

Procedure TSaveViewToFileHandler.OnStart;
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
   ProcSaveViewAsImage(Project,'');
{$ENDIF} // <----------------- AXDLL
end;

{==============================================================================+
  TLegendShowManagerHandler
+==============================================================================}

Procedure TLegendShowManagerHandler.OnStart;
var Rollup         : TRollupForm;
begin
  Rollup:=UserInterface.RollupNamed['LegendManagerRollupForm'];
  if Rollup<>NIL then Rollup.RollupToolWindow.Visible:=
      not Rollup.RollupToolWindow.Visible;
end;

Procedure UpdateViews;
{$IFNDEF WMLT}
var Rollup         : TViewManagerRollupForm;
{$ENDIF}
begin
{$IFNDEF WMLT}
  Rollup:=TViewManagerRollupForm(UserInterface.RollupNamed['ViewManagerRollupForm']);
  if Rollup<>NIL then Rollup.UpdateViews;
{$ENDIF}
end;

Procedure UpdateLegends;
{$IFNDEF WMLT}
var Rollup         : TLegendManagerRollupForm;
{$ENDIF}
begin
{$IFNDEF WMLT}
  Rollup:=TLegendManagerRollupForm(UserInterface.RollupNamed['LegendManagerRollupForm']);
  if Rollup<>NIL then Rollup.UpdateLegends;
{$ENDIF}
end;

{ TOpenAttachedDocumentHandler }

procedure TOpenAttachedDocumentHandler.OnStart;
begin
     ProcOpenAttachedDocument(Project);
end;

{==============================================================================+
  Initialisierungs- und Terminierungscode
+==============================================================================}

Initialization;
  MenuFunctions.RegisterFromResource(HInstance,'ViewHndl','ViewHndl');
  TOpenAttachedDocumentHandler.Registrate('OpenAttachedDocument');
  TZoomAllHandler.Registrate('ViewZoomAll');
  TZoomSelectedHandler.Registrate('ViewZoomSelected');
  TZoomNextSelectedHandler.Registrate('ViewZoomNextSelected');
  TZoomHandler.Registrate('ViewZoom');
  TZoomScaleHandler.Registrate('ViewZoomScale');
  TZoomInHandler.Registrate('ViewZoomIn');
  TZoomOutHandler.Registrate('ViewZoomOut');
  TViewPanHandler.Registrate('ViewPan');
  TRotateViewHandler.Registrate('ViewRotate');
  TSaveViewToFileHandler.Registrate('ViewSaveAsImage');
  TViewShowManagerHandler.Registrate('ViewShowViewManager');
  TLegendShowManagerHandler.Registrate('ViewShowLegendManager');
  FZoomInCursor:=AddCursor(LoadCursor(hInstance,'ZOOMIN'));
//++ Glukhov DragView Build#150 30.01.01
  FHandFlatCursor:=AddCursor(LoadCursor(hInstance,'HANDFLAT'));
  FHandGrabCursor:=AddCursor(LoadCursor(hInstance,'HANDGRAB'));
//-- Glukhov DragView Build#150 30.01.01
{$IFNDEF WMLT}
  UserInterface.AddRollup(TViewManagerRollupForm);
  UserInterface.AddRollup(TLegendManagerRollupForm);
{$ENDIF}
{$ENDIF} // <----------------- AXDLL
end.
