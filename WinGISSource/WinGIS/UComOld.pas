unit Ucomold;
{Altes Flaechen Datenbank verbinden}
Interface

Uses SysUtils,Messages,Objects,AM_Admin,AM_Def,AM_Index,AM_Point
     {$IFNDEF WMLT}
     ,AM_DDE
     {$ENDIF}
     ,AM_Layer,AM_Poly,AM_CPoly,AM_StdDl,AM_Dlg1,AM_Coll,WinProcs,AM_Proj,
     AM_Sym,AM_Text,AVLTree,AM_Paint,WinTypes,Classes,
     ulbase,ResDlg,Controls,ProjStyle;

const id_SourceList     = 101;
      id_SourcePoint    = 103;
      id_ObjectTypes    = 104;
      maxTypes          = 3;

      ObjTypes          : Array[0..maxTypes-1] of Word = (ot_Pixel,ot_symbol,ot_Text);

type

{***********************************************************************}
     { Record TNode                                                          }
     {-----------------------------------------------------------------------}
     {***********************************************************************}
     PNode              = ^TNode;
     TNode              = Record
       Position         : PDPoint;
       ObjNumber        : LongInt;
     end;

     {***********************************************************************}
     { Record TListNode                                                      }
     {-----------------------------------------------------------------------}
     {***********************************************************************}
     PListNode          = ^TListNode;
     TListNode          = Record
       ObjNumber        : LongInt;
       Next             : PListNode;
     end;


     PPointOldData      = ^TPointOldData;
     TPointOldData      = Record
       LayersAreas      : PLongColl;
       LayersPoints     : PLongColl;
       ObjectTypes      : TObjectTypes;
       Invers           : Boolean;
     end;

     PPointOldDlg       = ^TPOintOldDlg;
     TPointOldDlg       = Class(TLayerDlg)
      Public
       Data             : PPointOldData;
       TypesList        : TRListBox;
       Constructor Init(AParent:TWinControl;AData:PPointOldData;
           ALayers:PLayers;AProjStyle:TProjectStyles);
       Function    CanClose:Boolean; override;
       Procedure   SetupWindow; override;
     end;


    {***********************************************************************}
     { Object TTree                                                          }
     {-----------------------------------------------------------------------}
     { Felder:                                                               }
     {***********************************************************************}
     PTree              = ^TTree;
     TTree              = Object(TAvlTree)
       Function    Compare(Key1,Key2:Pointer):Integer; virtual;
       Procedure   FreeItem(Item:Pointer); virtual;
     end;


     TOldCombine           = class
       Proj             : PProj;
       Parent           : TWinControl;
       InsertData       : TPointOldData;
       Constructor Create(AProj:PProj;AParent:TWinControl);
       Procedure DoAll;
     end;

var OldCombine : TOldCombine;
implementation

Uses StateBar,UserIntf,AM_View,AM_Group,AM_Obj;

{******************** Dialog *******************************}

Procedure TOldCombine.DoAll;
{$IFNDEF WMLT}
  var
      AvlTree          : PTree;
      Cnt              : Integer;
      ALayer           : PLayer;
      AllCount         : LongInt;
      CurObject        : LongInt;
      Aborted          : Boolean;
  Function InsertPoints
     (
     Item              : PIndex
     )
     : Boolean; Far;
    var Node           : PNode;
        AItem          : PPixel;
    begin
      New(Node);
      AItem:=Pointer(PLayer(Proj^.PInfo^.Objects)^.IndexObject(Proj^.PInfo,Item));
      with Node^ do begin
        Position:=@AItem^.Position;
        ObjNumber:=AItem^.Index;
      end;
      if not AvlTree^.Insert(Node) then Dispose(Node);
      Inc(CurObject);
      StatusBar.Progress:=100*CurObject/AllCount;
      InsertPoints:=StatusBar.QueryAbort;
    end;


  Function InsertSymbols
     (
     Item              : PIndex
     )
     : Boolean; Far;
    var Node           : PNode;
        AItem          : PSymbol;
    begin
      New(Node);
      AItem:=Pointer(PLayer(Proj^.PInfo^.Objects)^.IndexObject(Proj^.PInfo,Item));
      with Node^ do begin
        Position:=@AItem^.Position;
        ObjNumber:=AItem^.Index;
      end;
      if not AvlTree^.Insert(Node) then Dispose(Node);
      Inc(CurObject);
      StatusBar.Progress:=100*CurObject/AllCount;
      InsertSymbols:=StatusBar.QueryAbort;
    end;

  Function InsertText
     (
     Item              : PIndex
     )
     : Boolean; Far;
    var Node           : PNode;
        AItem          : PText;
    begin
      New(Node);
      AItem:=Pointer(PLayer(Proj^.PInfo^.Objects)^.IndexObject(Proj^.PInfo,Item));
      with Node^ do begin
        Position:=@AItem^.Pos;
        ObjNumber:=AItem^.Index;
      end;
      if not AvlTree^.Insert(Node) then Dispose(Node);
      Inc(CurObject);
      StatusBar.Progress:=100*CurObject/AllCount;
      InsertText:=StatusBar.QueryAbort;
    end;
  Function SearchNode
     (
     SData           : Pointer;
     Tree            : PAVLNode
     )
     : PAVLNode;
    begin
      if Tree=NIL then SearchNode:=NIL
      else begin
        repeat
          case AvlTree^.Compare(SData,Tree^.Data) of
            CPEQUAL   : begin
              SearchNode:=Tree;
              Exit;
            end;
            CPSMALLER : if Tree^.Left=NIL then begin
                  SearchNode:=Tree;
                  Exit;
                end
                else Tree:=Tree^.Left;
            CPBIGGER  : if Tree^.Right=NIL then begin
                  SearchNode:=Tree;
                  Exit;
                end
                else Tree:=Tree^.Right;
          end;
        until Tree=NIL;
        SearchNode:=NIL;
      end;
    end;
  Function DoCPolys
     (
     Item          : PCPoly
     )
     : Boolean; Far;
    var Search     : TNode;
        SearchPos  : TDPoint;
        Node1      : PAVLNode;
        Node2      : PAvlNode;
        List       : PListNode;
        NewNode    : PListNode;
        AStr       : String;
        AItem      : PCPoly;
        AObjStr    : String;
        AIndex     : PIndex;
        AView      : PView;
        ASymbol    : PSGroup;
        TmpInd     : LongInt;
    Function SearchSym
       (
       Item        : PSGroup
       )
       : Boolean; Far;
      begin
        Result:=Item^.Index = PSymbol(AView)^.SymIndex;
      end;
    begin
      AItem:=Pointer(PLayer(Proj^.PInfo^.Objects)^.IndexObject(Proj^.PInfo,Item));
      Search.Position:=@SearchPos;
      SearchPos.Init(AItem^.ClipRect.A.X,-MaxLongInt);
      Node1:=SearchNode(@Search,AvlTree^.AvlRoot);
      SearchPos.Init(AItem^.ClipRect.B.X,MaxLongInt);
      Node2:=SearchNode(@Search,AvlTree^.AvlRoot);
      List:=NIL;
      if (PNode(Node1^.Data)^.Position^.Y>=AItem^.ClipRect.A.Y)
          and (PNode(Node1^.Data)^.Position^.Y<=AItem^.ClipRect.B.Y)
          and (AItem^.PointInside(Proj^.PInfo,PNode(Node1^.Data)^.Position^)) then begin
        New(NewNode);
        NewNode^.ObjNumber:=PNode(Node1^.Data)^.ObjNumber;
        NewNode^.Next:=List;
        List:=NewNode;
      end;
      if Node1<>Node2 then repeat
        Node1:=AvlTree^.NextNode(Node1);
        if (PNode(Node1^.Data)^.Position^.Y>=AItem^.ClipRect.A.Y)
            and (PNode(Node1^.Data)^.Position^.Y<=AItem^.ClipRect.B.Y)
            and (AItem^.PointInside(Proj^.PInfo,PNode(Node1^.Data)^.Position^)) then begin
          New(NewNode);
          NewNode^.ObjNumber:=PNode(Node1^.Data)^.ObjNumber;
          NewNode^.Next:=List;
          List:=NewNode;
        end;
      until Node1=Node2;
      if List<>NIL then begin
        AStr:=Format('[CHG][%10d][%10d]',[List^.ObjNumber,AItem^.Index]);
        Str(AItem^.Flaeche:15:2,AObjStr);
        AStr:=AStr+'['+AObjStr+']';
        AIndex:=New(PIndex,Init(0));
        AIndex^.Index:=List^.ObjNumber;
        AView:=Pointer(PLayer(Proj^.PInfo^.Objects)^.IndexObject(Proj^.PInfo,AIndex));
        Dispose(AIndex,Done);
        if (AView^.GetObjType=ot_Pixel) then begin
          Str(PPixel(AView)^.Position.X/100:12:2,AObjStr);
          AStr:=AStr+'['+AObjStr+']';
          Str(PPixel(AView)^.Position.Y/100:12:2,AObjStr);
          AStr:=AStr+'['+AObjStr+']';
        end
        else if (AView^.GetObjType=ot_Symbol) then begin
          Str(PSymbol(AView)^.Position.X/100:12:2,AObjStr);
          AStr:=AStr+'['+AObjStr+']';
          Str(PSymbol(AView)^.Position.Y/100:12:2,AObjStr);
          AStr:=AStr+'['+AObjStr+']';
          TmpInd:=PSymbol(AView)^.SymIndex-600000000;
          Str(TmpInd:10,AObjStr);
          AStr:=AStr+'['+AObjStr+']';
          AIndex:=PSymbols(Proj^.PInfo^.Symbols)^.Data^.FirstThat(@SearchSym);
          AStr:=AStr+'['+PSGroup(AIndex)^.Name^+']';
        end
        else if (AView^.GetObjType=ot_Text) then begin
          Str(PText(AView)^.Pos.X/100:12:2,AObjStr);
          AStr:=AStr+'['+AObjStr+']';
          Str(PText(AView)^.Pos.Y/100:12:2,AObjStr);
          AStr:=AStr+'['+AObjStr+']';
          AStr:=AStr+'['+PText(AView)^.Text^+']';
        end;
        DDEHandler.SendString(AStr);
      end;
      while List<>NIL do begin
        NewNode:=List;
        List:=List^.Next;
        Dispose(NewNode);
      end;
      Inc(CurObject,100);
      StatusBar.Progress:=100*CurObject/AllCount;
      DoCPolys:=StatusBar.QueryAbort;
    end;
  Function DoCount
     (
     Item          : Pointer
     )
     : Boolean; Far;
    begin
      Inc(AllCount);
      DoCount:=FALSE;
    end;
{$ENDIF}
  begin
{$IFNDEF WMLT}
    if ExecDialog(TPointOldDlg.Init((Proj)^.Parent,
        @InsertData,PProj(Proj)^.Layers,PProj(Proj)^.PInfo^.ProjStyles))=id_OK then begin
      StatusBar.ProgressPanel:=TRUE;
      try
        StatusBar.ProgressText:=GetLangText(11005);
        StatusBar.AbortText:=GetLangText(3803);
        AllCount:=0;
        for Cnt:=0 to InsertData.LayersAreas^.Count-1 do begin
          ALayer:=PProj(Proj)^.Layers^.IndexToLayer(LongInt(InsertData.LayersAreas^.At(Cnt)));
          ALayer^.FirstObjectType(ot_CPoly,@DoCount);
        end;
        AllCount:=AllCount*100;
        for Cnt:=0 to InsertData.LayersPoints^.Count-1 do begin
          ALayer:=PProj(Proj)^.Layers^.IndexToLayer(LongInt(InsertData.LayersPoints^.At(Cnt)));
          if ot_Pixel in InsertData.ObjectTypes then
             ALayer^.FirstObjectType(ot_Pixel,@DoCount);
          if ot_Symbol in InsertData.ObjectTypes then
             ALayer^.FirstObjectType(ot_Symbol,@DoCount);
          if ot_Text in InsertData.ObjectTypes then
             ALayer^.FirstObjectType(ot_Text,@DoCount);
        end;
        AvlTree:=New(PTree,Init);
        Aborted:=FALSE;
        CurObject:=0;
        for Cnt:=0 to InsertData.LayersPoints^.Count-1 do begin
          ALayer:=PProj(Proj)^.Layers^.IndexToLayer(LongInt(InsertData.LayersPoints^.At(Cnt)));
          if ot_Pixel in InsertData.ObjectTypes then
             Aborted:=ALayer^.FirstObjectType(ot_Pixel,@InsertPoints)<>NIL;
          if ot_Symbol in InsertData.ObjectTypes then
             Aborted:=Aborted or (ALayer^.FirstObjectType(ot_Symbol,@InsertSymbols)<>NIL);
          if ot_Text in InsertData.ObjectTypes then
             Aborted:=Aborted or (ALayer^.FirstObjectType(ot_Text,@InsertText)<>NIL);
        end;
        if (AvlTree^.AvlRoot<>NIL)
            and not Aborted then for Cnt:=0 to InsertData.LayersAreas^.Count-1 do begin
          ALayer:=PProj(Proj)^.Layers^.IndexToLayer(LongInt(InsertData.LayersAreas^.At(Cnt)));
          ALayer^.FirstObjectType(ot_CPoly,@DoCPolys);
          DDEHandler.SendString('[END][]');
        end;
        Dispose(AvlTree,Done);
      finally
        StatusBar.ProgressPanel:=FALSE;
      end;
    end;
{$ENDIF}
  end;


{******************** Dialog *******************************}
Constructor TPointOlddlg.Init
   (
   AParent         : TWinControl;
   AData           : PPointOldData;
   ALayers         : PLayers;
   AProjStyle      : TProjectStyles
   );
  begin
    inherited Init(AParent,'D_Combine',ALayers,AProjStyle);
    Data:=AData;
    Typeslist:=TRListBox.InitResource(Self,id_ObjectTypes);
    HelpContext:=6000;
  end;

Procedure TPointOldDlg.SetupWindow;
  var Cnt          : Integer;
      AText        : Array[0..255] of Char;
  begin
    inherited SetupWindow;
    FillLayerList(id_SourceList,TRUE,FALSE);
    FillLayerList(id_SourcePoint,TRUE,FALSE);
    SetMultiLayerSelection(id_SourceList,Data^.LayersAreas,TRUE);
    SetMultiLayerSelection(id_SourcePoint,Data^.LayersPoints,TRUE);
    for Cnt:=0 to maxTypes-1 do begin
      GetLangPChar(11073+Cnt,AText,SizeOf(AText));
      TypesList.AddString(AText);
      if ObjTypes[Cnt] in Data^.ObjectTypes then
          SendDlgItemMsg(id_ObjectTypes,lb_SetCurSel,Cnt,0);
    end;
  end;

Function TPointOldDlg.CanClose
   : Boolean;
  var Cnt          : Integer;
  begin
    CanClose:=FALSE;
    if SendDlgItemMsg(id_SourceList,lb_GetSelCount,0,0)=0 then begin
      MsgBox(Handle,841,mb_IconExclamation or mb_OK,'');
      SetFocus(GetItemHandle(id_SourceList));
    end
    else if SendDlgItemMsg(id_SourcePoint,lb_GetSelCount,0,0)=0 then begin
      MsgBox(Handle,841,mb_IconExclamation or mb_OK,'');
      SetFocus(GetItemHandle(id_SourcePoint));
    end
    else with Data^ do begin
      if LayersAreas<>NIL then Dispose(LayersAreas,Done);
      if LayersPoints<>NIL then Dispose(LayersPoints,Done);
      LayersAreas:=New(PLongColl,Init(5,5));
      LayersPoints:=New(PLongColl,Init(5,5));
      GetMultiLayerSelection(id_SourceList,TRUE,LayersAreas);
      GetMultiLayerSelection(id_SourcePoint,TRUE,LayersPoints);
      ObjectTypes:=[];
      for Cnt:=0 to maxTypes-1 do
          if SendDlgItemMsg(id_ObjectTypes,lb_GetSel,Cnt,0)<>0 then
        ObjectTypes:=ObjectTypes+[ObjTypes[Cnt]];
      CanClose:=TRUE;
    end;
  end;

Function TTree.Compare
   (
   Key1            : Pointer;
   Key2            : Pointer
   )
   : Integer;
  begin
    if PNode(Key1)^.Position^.X>PNode(Key2)^.Position^.X then Compare:=cpBigger
    else if PNode(Key1)^.Position^.X<PNode(Key2)^.Position^.X then Compare:=cpSmaller
    else begin
      if PNode(Key1)^.Position^.Y>PNode(Key2)^.Position^.Y then Compare:=cpBigger
      else if PNode(Key1)^.Position^.Y<PNode(Key2)^.Position^.Y then Compare:=cpSmaller
      else Compare:=cpEqual;
    end;
  end;

Procedure TTree.FreeItem
   (
   Item            : Pointer
   );
  begin
    Dispose(PNode(Item));
  end;


Constructor TOldCombine.Create;
  Begin
    Proj:=AProj;
    Parent:=AParent;
  end;

end.

