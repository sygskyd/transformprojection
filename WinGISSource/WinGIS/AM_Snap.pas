{****************************************************************************}
{ Unit                                                                       }
{----------------------------------------------------------------------------}
{                                                                            }
{----------------------------------------------------------------------------}
{ Modifikationen                                                             }
{  16.02.1994 Martin Forst                                                   }
{****************************************************************************}
unit AM_Snap;

interface

uses Messages, WinDos, WinProcs, WinTypes, AM_Def, AM_Admin, AM_Coll, AM_Layer, SysUtils,
  AM_Paint, AM_Index, AM_Poly, AM_Sym, AM_Point, ResDlg, Classes, Objects;

const
  lsNone = 0;
  lsMovePoint = 1;
  lsInsPoint = 2;

  ciSnapItems = 2;
  ceSnapItems = 2;

  ciSnapLayers = 2;
  ceSnapLayers = 2;

  ciSnapOpt = 2;
  ceSnapOpt = 2;

  id_LineSnap = 101;
  id_Radius = 100;
  id_ListSnap = 104;
  id_ListFix = 105;
  id_Undo = 509;

  id_SnapRadius = 102;
  id_LimitSnap = 103;
  id_SearchNearest = 104;

  id_List = 101;

type
  PSnapData = ^TSnapData;
  TSnapData = object(TOldObject)
    Radius: LongInt;
    LineSnap: Integer16;
    SnapLayers: PLongColl;
    FixLayers: PLongColl;
    constructor Init;
    destructor Done; virtual;
    constructor Load(S: TOldStream);
    procedure Store(S: TOldStream);
  end;

  PSnapOpt = ^TSnapOpt;
  TSnapOpt = object(TOldObject)
    SnapRadius: LongInt;
    SnapRadiusType: Integer;
    SearchNearest: Boolean;
    constructor Init;
    destructor Done; virtual;
    constructor Load(S: TOldStream);
    procedure Store(S: TOldStream);
  end;

  TSnapDialog = class(TGrayDialog)
  public
    Layers: PPLayers;
    constructor Init(AParent: TComponent; AName: PChar; ALayers: PPLayers);
    procedure GetLayerList(AList: Integer; Fixed: Boolean; ALayers: PLongColl);
    function IndexOfLayer(Layer: LongInt; Fixed: Boolean): Integer;
    function LayerOfIndex(AIndex: Integer; Fixed: Boolean): PLayer;
    procedure SetLayers(AList: Integer; Fixed: Boolean; ALayers: PLongColl);
  end;

  TSnapDlg = class(TSnapDialog)
  public
    Data: PSnapData;
    Proj: Pointer;
    FName: PChar;
    DidUndo: Boolean;
    constructor Init(AParent: TComponent; AData: PSnapData; ALayers: PPLayers; AFName: PChar; AProj: Pointer);
    procedure Cancel(var Msg: TMessage); message id_First + id_Cancel;
    function CanClose: Boolean; override;
    procedure Undo(var Msg: TMessage); message id_First + id_Undo;
    procedure SetupWindow; override;
  end;

  PSnapInfo = ^TSnapInfo;
  TSnapInfo = object(TCollection)
    Position: TDPoint;
    Objects: TSortedLong;
    constructor Init;
    destructor Done; virtual;
    procedure CalculatePosition;
    function HasObject(AObject: LongInt): Boolean;
    function HasPoint(APoint: PDPoint): Boolean;
    function HasPosition(APoint: PDPoint): Boolean;
    procedure InsertObject(AObject: LongInt);
    procedure InsertPoint(APoint: PDPoint);
    procedure SetPointsPosition;
  end;

  PSnapped = ^TSnapped;
  TSnapped = object(TViewColl)
    function HasPoint(APoint: PDPoint): Boolean;
  end;

procedure DoSnap(Layers: PLayers; Proj: Pointer; Options: PSnapData);

implementation

uses AM_Circl, AM_Proj, UserIntf;

constructor TSnapDlg.Init
  (
  AParent: TComponent;
  AData: PSnapData;
  ALayers: PPLayers;
  AFName: PChar;
  AProj: Pointer
  );
begin
  inherited Init(AParent, 'ASNAP', ALayers);
  Data := AData;
  Layers := ALayers;
  FName := AFName;
  DidUndo := FALSE;
  Proj := AProj;
  HelpContext := 5040;
end;

procedure TSnapDialog.SetLayers
  (
  AList: Integer;
  Fixed: Boolean;
  ALayers: PLongColl
  );

  procedure DoAll
      (
      Item: PLayer
      ); far;
  var
    AText: array[0..255] of Char;
  begin
    if (Item^.Index > 0) and (Fixed or not (Item^.GetState(sf_Fixed))) then
    begin
      StrPCopy(AText, PToStr(Item^.Text));
      SendDlgItemMsg(AList, lb_AddString, 0, LongInt(@AText));
    end;
  end;

  procedure DoAll2
      (
      Item: LongInt
      ); far;
  var
    AIndex: Integer;
  begin
    AIndex := IndexOfLayer(Item, Fixed);
    if AIndex <> lb_Err then
      SendDlgItemMsg(AList, lb_SetSel, 1, MakeLong(AIndex, 0));
  end;
begin
  Layers^^.LData^.ForEach(@DoAll);
  ALayers^.ForEach(@DoAll2);
end;

procedure TSnapDlg.SetupWindow;
begin
  inherited SetupWindow;
  with Data^ do
  begin
    SetFieldLong(id_Radius, Radius);
    CheckRadioButton(Handle, id_LineSnap, id_LineSnap + 2, id_LineSnap + LineSnap);
    SetLayers(id_ListSnap, FALSE, SnapLayers);
    SetLayers(id_ListFix, TRUE, FixLayers);
  end;
end;

constructor TSnapDialog.Init
  (
  AParent: TComponent;
  AName: PChar;
  ALayers: PPLayers
  );
begin
  inherited Init(AParent, AName);
  Layers := ALayers;
end;

function TSnapDialog.IndexOfLayer
  (
  Layer: LongInt;
  Fixed: Boolean
  )
  : Integer;
var
  Cnt: Integer;

  function DoAll
      (
      Item: PLayer
      )
      : Boolean; far;
  begin
    if Fixed or not (Item^.GetState(sf_Fixed)) then
    begin
      DoAll := Item^.Index = Layer;
      Inc(Cnt);
    end
    else
      DoAll := FALSE;
  end;
begin
  Cnt := -2;
  if Layers^^.LData^.FirstThat(@DoAll) <> nil then
    IndexOfLayer := Cnt
  else
    IndexOfLayer := lb_Err;
end;

function TSnapDialog.LayerOfIndex
  (
  AIndex: Integer;
  Fixed: Boolean
  )
  : PLayer;
var
  Cnt: Integer;

  function DoAll
      (
      Item: PLayer
      )
      : Boolean; far;
  begin
    if Fixed or not (Item^.GetState(sf_Fixed)) then
      Inc(Cnt);
    DoAll := Cnt = AIndex;
  end;
begin
  Cnt := -2;
  LayerOfIndex := Layers^.LData^.FirstThat(@DoAll);
end;

function TSnapDlg.CanClose
  : Boolean;
var
  ARadius: LongInt;
  ASnap: PLongColl;
  AFix: PLongColl;

  function DoAll
      (
      Item: Pointer
      )
      : Boolean; far;
  var
    ALayer: PLayer;
  begin
    if AFix^.IndexOf(Item) <> -1 then
    begin
      ALayer := Layers^^.IndexToLayer(LongInt(Item));
      if ALayer <> nil then
        MsgBox(Handle, 842, mb_IconExclamation or mb_OK, ALayer^.Text^);
      DoAll := TRUE;
    end
    else
      DoAll := FALSE;
  end;
begin
  CanClose := FALSE;
  if not GetFieldLong(id_Radius, ARadius) then
  begin
    MsgBox(Handle, 840, mb_IconExclamation or mb_OK, '');
    SetFocus(GetItemHandle(id_Radius));
  end
  else
  begin
    ASnap := New(PLongColl, Init(5, 5));
    GetLayerList(id_ListSnap, FALSE, ASnap);
    if ASnap^.Count = 0 then
    begin
      MsgBox(Handle, 841, mb_IconExclamation or mb_OK, '');
      SetFocus(GetItemHandle(id_ListSnap));
      Dispose(ASnap, Done);
    end
    else
    begin
      AFix := New(PLongColl, Init(5, 5));
      GetLayerList(id_ListFix, TRUE, AFix);
      if ASnap^.FirstThat(@DoAll) = nil then
      begin
        with Data^ do
        begin
          Radius := ARadius;
          if IsDlgButtonChecked(Handle, id_LineSnap) = 1 then
            LineSnap := 0
          else
            if IsDlgButtonChecked(Handle, id_LineSnap + 1) = 1 then
              LineSnap := 1
            else
              if IsDlgButtonChecked(Handle, id_LineSnap + 2) = 1 then
                LineSnap := 2;
          Dispose(SnapLayers, Done);
          Dispose(FixLayers, Done);
          SnapLayers := ASnap;
          FixLayers := AFix;
        end;
        CanClose := TRUE;
      end
      else
      begin
        Dispose(ASnap, Done);
        Dispose(AFix, Done);
      end;
    end;
  end;
end;

procedure TSnapDialog.GetLayerList
  (
  AList: Integer;
  Fixed: Boolean;
  ALayers: PLongColl
  );
var
  Puffer: array[0..bsSelLayer] of Integer;
  Items: Integer;
  ALayer: PLayer;
  Cnt: Integer;
begin
  Items := SendDlgItemMsg(AList, lb_GetSelItems, bsSelLayer, LongInt(@Puffer));
  for Cnt := 0 to Items - 1 do
  begin
    ALayer := LayerOfIndex(Puffer[Cnt], Fixed);
    if ALayer <> nil then
      ALayers^.Insert(Pointer(ALayer^.Index));
  end;
end;

procedure DoSnap
  (
  Layers: PLayers;
  Proj: Pointer;
  Options: PSnapData
  );
var
  OldCursor: Integer;
  SnapLayer: PLayer;
  FixLayer: PLayer;
  SnapRadius: LongInt;
  Snapped: PSnapped;
  CopyFixed: Boolean;

  procedure SnapFixSnapped
      (
      BItem: PSnapInfo
      ); far;
  var
    SelectCircle: PEllipse;
    SnObje: PIndex;
  begin
    with BItem^, PProj(Proj)^ do
    begin
      SelectCircle := New(PEllipse, Init(Position, SnapRadius, SnapRadius, 0));
      SnObje := nil;
      if FixLayer^.SearchForPoint(PInfo, SelectCircle, 0, [ot_Symbol, ot_Pixel, ot_Poly, ot_CPoly], Position, SnObje) then
        SetPointsPosition;
      Dispose(SelectCircle, Done);
    end;
  end;

  procedure SnapFix
      (
      AItem: PPoly
      ); far;
  var
    Cnt: Integer;
    AIndex: Integer;
    BItem: PDPoint;
    SelectCircle: PEllipse;
    SnObje: PIndex;
  begin
    with AItem^, PProj(Proj)^ do
      if GetObjType in [ot_Poly, ot_CPoly] then
      begin
{!?!?!        StatusLine.SetFieldValue(id_Field2,Index Mod 100000000,0);}
        for Cnt := 0 to Data^.Count - 1 do
        begin
          if (SnapInfo = nil) or not (SnapInfo^.Search(Cnt, AIndex)) then
          begin
            BItem := Data^.At(Cnt);
            if not Snapped^.HasPoint(BItem) then
            begin
              SelectCircle := New(PEllipse, Init(BItem^, SnapRadius, SnapRadius, 0));
              SnObje := nil;
              FixLayer^.SearchForPoint(PInfo, SelectCircle, AItem^.Index, [ot_Symbol, ot_Pixel, ot_Poly, ot_CPoly], BItem^, SnObje);
              Dispose(SelectCircle, Done);
            end;
          end;
        end;
      end
      else
        if GetObjType = ot_Symbol then
        begin
          BItem := @PSymbol(AItem)^.Position;
          SelectCircle := New(PEllipse, Init(BItem^, SnapRadius, SnapRadius, 0));
          SnObje := nil;
          FixLayer^.SearchForPoint(PInfo, SelectCircle, AItem^.Index, [ot_Symbol, ot_Pixel, ot_Poly, ot_CPoly], BItem^, SnObje);
          Dispose(SelectCircle, Done);
        end
        else
          if (GetObjType = ot_Pixel) and not GetState(sf_NoSnap) then
          begin
            BItem := @PPixel(AItem)^.Position;
            SelectCircle := New(PEllipse, Init(BItem^, SnapRadius, SnapRadius, 0));
            SnObje := nil;
            FixLayer^.SearchForPoint(PInfo, SelectCircle, AItem^.Index, [ot_Symbol, ot_Pixel, ot_Poly, ot_CPoly], BItem^, SnObje);
            Dispose(SelectCircle, Done);
          end;
  end;

  procedure EliminatePoints
      (
      AItem: PPoly
      ); far;
  begin
    if AItem^.GetObjType in [ot_Poly, ot_CPoly] then
    begin
{!?!?!        StatusLine.SetFieldValue(id_Field2,AItem^.Index Mod 100000000,0);}
      AItem^.EliminatePoints(SnapRadius);
    end;
  end;

  procedure SnapPoints
      (
      Item: PPoly
      ); far;

    procedure DoPoints
        (
        AItem: PDPoint
        ); far;
    var
      SelRect: TDRect;
      Points: PSnapInfo;

      procedure DoItems
          (
          BItem: PPoly
          ); far;
      var
        CPoint: PDPoint;
        Distance: Double;
        Cnt: Integer;
        AIndex: Integer;
        CItem: PDPoint;
        ADistance: Double;
      begin
        if (BItem^.Index <> Item^.Index) and (BItem^.GetObjType in [ot_Poly, ot_CPoly]) then
          with BItem^ do
          begin
            Distance := MaxLongInt;
            CPoint := nil;
            for Cnt := 0 to Data^.Count - 1 do
            begin
              if (SnapInfo = nil) or not (SnapInfo^.Search(Cnt, AIndex)) then
              begin
                CItem := Data^.At(Cnt);
                if SelRect.PointInside(CItem^) then
                begin
                  ADistance := AItem^.XDist(CItem^) + AItem^.YDist(CItem^);
                  if ADistance < Distance then
                  begin
                    Distance := ADistance;
                    CPoint := CItem;
                  end;
                end;
              end;
            end;
            if CPoint <> nil then
            begin
              if GetObjType = ot_CPoly then
              begin
                if (Data^.IndexOf(CPoint) = 0) then
                  Points^.InsertPoint(Data^.At(Data^.Count - 1))
                else
                  if Data^.IndexOf(CPoint) = Data^.Count - 1 then
                    Points^.InsertPoint(Data^.At(0));
              end;
              Points^.InsertPoint(CPoint);
              Points^.InsertObject(Index);
            end;
          end
        else
          if BItem^.GetObjType = ot_Symbol then
          begin
            if PSymbol(BItem)^.Position.Dist(AItem^) < SnapRadius then
              Points^.InsertPoint(@PSymbol(BItem)^.Position);
          end
          else
            if (BItem^.GetObjType = ot_Pixel) and not BItem^.GetState(sf_NoSnap) then
            begin
              if PPixel(BItem)^.Position.Dist(AItem^) < SnapRadius then
                Points^.InsertPoint(@PPixel(BItem)^.Position);
            end;
      end;
    begin
      SelRect.Init;
      SelRect.AssignByPoint(AItem^, SnapRadius);
      Points := New(PSnapInfo, Init);
      Points^.InsertPoint(AItem);
      SnapLayer^.Data^.ForEach(@DoItems);
      if Points^.Count > 1 then
      begin
        Points^.CalculatePosition;
        Snapped^.AtInsert(Snapped^.GetCount, Points);
      end
      else
        Dispose(Points, Done);
    end;
  var
    AIndex: Integer;
    Cnt: Integer;
    APoint: PDPoint;
  begin
    if Item^.GetObjType in [ot_Poly, ot_CPoly] then
      with Item^ do
      begin
{!?!?!        StatusLine.SetFieldValue(id_Field2,Index Mod 100000000,0);}
        for Cnt := 0 to Data^.Count - 1 do
        begin
          if (SnapInfo = nil) or not (SnapInfo^.Search(Cnt, AIndex)) then
          begin
            APoint := Data^.At(Cnt);
            if not Snapped^.HasPoint(APoint) then
              DoPoints(APoint);
          end;
        end;
      end;
  end;

  procedure CopyLayer
      (
      Item: LongInt
      ); far;

    procedure CopyItems
        (
        AItem: PIndex
        ); far;
    var
      AObject: PIndex;
    begin
      AObject := Layers^.IndexObject(PProj(Proj)^.PInfo, AItem);
      if AObject <> nil then
        if CopyFixed or not Layers^.IsObjectFixed(AObject) then
          SnapLayer^.InsertObject(PProj(Proj)^.PInfo, AObject, FALSE)
    end;
  var
    ALayer: PLayer;
  begin
    ALayer := Layers^.IndexToLayer(Item);
    if ALayer <> nil then
      ALayer^.Data^.ForEach(@CopyItems);
  end;

  procedure SnapLineSnapped
      (
      Item: PSnapInfo
      ); far;
  var
    DoInsert: Boolean;

    function SnapToLine
        (
        BItem: PPoly
        )
        : Boolean; far;
    var
      BIndex: Integer;
      APosition: TDPoint;
    begin
      SnapToLine := FALSE;
      with BItem^, PProj(Proj)^ do
      begin
        if not (Item^.HasObject(Index)) and (GetObjType in [ot_Poly, ot_CPoly]) then
        begin
          APosition.Init(Item^.Position.X, Item^.Position.Y);
          if SearchForLine(PInfo, APosition, [ot_Poly, ot_CPoly], SnapRadius, BIndex) <> nil then
          begin
            Inc(BIndex);
            if DoInsert then
            begin
              if BItem^.FindIndexOfPoint(APosition) = -1 then
              begin
                InsertPointByIndex(BIndex, APosition);
                Item^.InsertPoint(Data^.At(BIndex));
              end;
            end;
            Item^.Position := APosition;
            if not DoInsert then
              Item^.SetPointsPosition; /////
          end;
        end;
      end;
    end;
  begin
    if Options^.LineSnap = lsMovePoint then
    begin
      DoInsert := FALSE;
      if SnapLayer^.Data^.FirstThat(@SnapToLine) = nil then
        FixLayer^.Data^.FirstThat(@SnapToLine);
    end
    else
    begin
      DoInsert := TRUE;
      SnapLayer^.Data^.FirstThat(@SnapToLine);
    end;
  end;

  procedure SnapLine
      (
      AItem: PPoly
      ); far;
  var
    AIndex: Integer;
    Cnt: Integer;
    APoint: PDPoint;
    DoInsert: Boolean;

    function SnapToLine
        (
        BItem: PPoly
        )
        : Boolean; far;
    var
      BIndex: Integer;
    begin
      SnapToLine := FALSE;
      if (BItem^.Index <> AItem^.Index) and (BItem^.GetObjType in [ot_Poly, ot_CPoly]) then
        with BItem^, PProj(Proj)^ do
        begin
          if SearchForLine(PInfo, APoint^, [ot_Poly, ot_CPoly], SnapRadius, BIndex) <> nil then
          begin
            Inc(BIndex);
            if DoInsert then
            begin
              if BItem^.FindIndexOfPoint(APoint^) = -1 then
              begin
                InsertPointByIndex(BIndex, APoint^);
              end;
            end;
          end;
        end;
    end;
  begin
    if AItem^.GetObjType in [ot_Poly, ot_CPoly] then
      with AItem^ do
      begin
{!?!?!        StatusLine.SetFieldValue(id_Field2,Index Mod 100000000,0);}
        for Cnt := 0 to Data^.Count - 1 do
        begin
          if (SnapInfo = nil) or not (SnapInfo^.Search(Cnt, AIndex)) then
          begin
            APoint := Data^.At(Cnt);
            if not Snapped^.HasPoint(APoint) then
            begin
              if Options^.LineSnap = lsMovePoint then
              begin
                DoInsert := FALSE;
                if SnapLayer^.Data^.FirstThat(@SnapToLine) = nil then
                  FixLayer^.Data^.FirstThat(@SnapToLine);
              end
              else
              begin
                DoInsert := TRUE;
                SnapLayer^.Data^.FirstThat(@SnapToLine);
                DoInsert := FALSE;
                FixLayer^.Data^.FirstThat(@SnapToLine);
              end;
            end;
          end;
        end;
      end
    else
      if AItem^.GetObjType = ot_Symbol then
      begin
        if not Snapped^.HasPoint(@PSymbol(AItem)^.Position) then
        begin
          APoint := @PSymbol(AItem)^.Position;
          DoInsert := FALSE;
          if SnapLayer^.Data^.FirstThat(@SnapToLine) = nil then
            FixLayer^.Data^.FirstThat(@SnapToLine);
        end;
      end
      else
        if (AItem^.GetObjType = ot_Pixel) and not AItem^.GetState(sf_NoSnap) then
        begin
          if not Snapped^.HasPoint(@PPixel(AItem)^.Position) then
          begin
            APoint := @PPixel(AItem)^.Position;
            DoInsert := FALSE;
            if SnapLayer^.Data^.FirstThat(@SnapToLine) = nil then
              FixLayer^.Data^.FirstThat(@SnapToLine);
          end;
        end;
  end;

  procedure CalculateClips
      (
      Item: PPoly
      ); far;

    procedure UpdateClipRect
        (
        AView: PIndex
        );
    var
      AItem: PIndex;
      Cnt: Integer;
      ALayer: PLayer;
    begin
      for Cnt := 0 to Layers^.LData^.Count - 1 do
      begin
        ALayer := Layers^.LData^.At(Cnt);
        AItem := ALayer^.HasObject(AView);
        if AItem <> nil then
          AItem^.ClipRect.Assign(AView^.ClipRect.A.X,
            AView^.ClipRect.A.Y, AView^.ClipRect.B.X, AView^.ClipRect.B.Y);
      end;
    end;
  begin
    if Item^.GetObjType in [ot_Poly, ot_CPoly] then
    begin
      Item^.CalculateClipRect;
      UpdateClipRect(Item);
    end
    else
      if Item^.GetObjType = ot_Symbol then
      begin
        PSymbol(Item)^.CalculateClipRect(PProj(Proj)^.PInfo);
        UpdateClipRect(Item);
      end
      else
        if (Item^.GetObjType = ot_Pixel) and not Item^.GetState(sf_NoSnap) then
        begin
          PPixel(Item)^.CalculateClipRect;
          UpdateClipRect(Item);
        end;
  end;

begin
  OldCursor := SetCursor(LoadCursor(0, idc_Wait));
  with Options^ do
  begin
    PProj(Proj)^.SetStatusText(GetLangText(849), TRUE);
    Snapped := New(PSnapped, Init);
    SnapLayer := New(PLayer, Init(nil));
    CopyFixed := TRUE;
    FixLayers^.ForEach(@CopyLayer);
    FixLayer := SnapLayer;
    SnapLayer := New(PLayer, Init(nil));
    CopyFixed := FALSE;
    SnapLayers^.ForEach(@CopyLayer);

    SnapRadius := Radius;
    PProj(Proj)^.SetStatusText(GetLangText(846), FALSE);
    SnapLayer^.Data^.ForEach(@EliminatePoints);

    PProj(Proj)^.SetStatusText(GetLangText(847), FALSE);
    SnapRadius := Radius;
    SnapLayer^.Data^.ForEach(@SnapPoints);

    SnapRadius := Radius;
    if FixLayers^.Count <> 0 then
    begin
      PProj(Proj)^.SetStatusText(GetLangText(844), FALSE);
      SnapLayer^.Data^.ForEach(@SnapFix);
      Snapped^.ForEach(@SnapFixSnapped);
    end;
    if LineSnap <> lsNone then
    begin
      PProj(Proj)^.SetStatusText(GetLangText(848), FALSE);
      SnapLayer^.Data^.ForEach(@SnapLine);
      Snapped^.ForEach(@SnapLineSnapped);
    end;

    PProj(Proj)^.SetStatusText(GetLangText(839), FALSE);
    SnapLayer^.Data^.ForEach(@CalculateClips);
    PProj(Proj)^.ClearStatusText;
  end;
  Dispose(Snapped, Done);
  SnapLayer^.Data^.DeleteAll;
  Dispose(SnapLayer, Done);
  FixLayer^.Data^.DeleteAll;
  Dispose(FixLayer, Done);
  SetCursor(OldCursor);
  PProj(Proj)^.PInfo^.RedrawScreen(TRUE);
end;

constructor TSnapInfo.Init;
begin
  TCollection.Init(ciSnapItems, ceSnapItems);
  Position.Init(0, 0);
  Objects.Init(ciSnapInfo, ciSnapItems);
end;

destructor TSnapInfo.Done;
begin
  DeleteAll;
  Position.Done;
  Objects.Done;
  TCollection.Done;
end;

procedure TSnapInfo.InsertPoint
  (
  APoint: PDPoint
  );
begin
  if Count < MaxCollectionSize then
    Insert(APoint);
end;

function TSnapInfo.HasPoint
  (
  APoint: PDPoint
  )
  : Boolean;

  function DoAll
      (
      Item: PDPoint
      )
      : Boolean; far;
  begin
    DoAll := Item = APoint;
  end;
begin
  HasPoint := FirstThat(@DoAll) <> nil;
end;

function TSnapped.HasPoint
  (
  APoint: PDPoint
  )
  : Boolean;

  function DoAll
      (
      Item: PSnapInfo
      )
      : Boolean; far;
  begin
    DoAll := Item^.HasPoint(APoint);
  end;
begin
  HasPoint := FirstThat(@DoAll) <> nil;
end;

procedure TSnapInfo.CalculatePosition;
var
  NewX: Extended;
  NewY: Extended;

  procedure SumPoints
      (
      DItem: PDPoint
      ); far;
  begin
    NewX := NewX + DItem^.X;
    NewY := NewY + DItem^.Y;
  end;
begin
  NewX := 0;
  NewY := 0;
  ForEach(@SumPoints);
  Position.Init(Trunc(NewX / Count), Trunc(NewY / Count));
  SetPointsPosition;
end;

procedure TSnapInfo.SetPointsPosition;

  procedure DoAll
      (
      Item: PDPoint
      ); far;
  begin
    Item^ := Position;
  end;
begin
  ForEach(@DoAll);
end;

function TSnapInfo.HasPosition
  (
  APoint: PDPoint
  )
  : Boolean;

  function DoAll
      (
      Item: PDPoint
      )
      : Boolean; far;
  begin
    DoAll := not Item^.IsDiff(APoint^);
  end;
begin
  HasPosition := FirstThat(@DoAll) <> nil;
end;

procedure TSnapInfo.InsertObject
  (
  AObject: LongInt
  );
begin
  if Objects.Count < MaxCollectionSize then
    Objects.Insert(Pointer(AObject));
end;

function TSnapInfo.HasObject
  (
  AObject: LongInt
  )
  : Boolean;
var
  AIndex: Integer;
begin
  HasObject := Objects.Search(Pointer(AObject), AIndex);
end;

constructor TSnapData.Init;
begin
  Radius := 0;
  LineSnap := 0;
  SnapLayers := New(PLongColl, Init(ciSnapLayers, ceSnapLayers));
  FixLayers := New(PLongColl, Init(ciSnapLayers, ceSnapLayers));
end;

constructor TSnapData.Load
  (
  S: TOldStream
  );
var
  Version: Word;
begin
  TOldObject.Init;
  S.Read(Version, SizeOf(Version));
  S.Read(Radius, SizeOf(Radius));
  S.Read(LineSnap, SizeOf(LineSnap));
  SnapLayers := Pointer(S.Get);
  FixLayers := Pointer(S.Get);
end;

procedure TSnapData.Store
  (
  S: TOldStream
  );
var
  Version: Word;
begin
  Version := 0;
  S.Write(Version, SizeOf(Version));
  S.Write(Radius, SizeOf(Radius));
  S.Write(LineSnap, SizeOf(LineSnap));
  S.Put(SnapLayers);
  S.Put(FixLayers);
end;

destructor TSnapData.Done;
begin
  if SnapLayers <> nil then
    Dispose(SnapLayers, Done);
  if FixLayers <> nil then
    Dispose(FixLayers, Done);
  TOldObject.Done;
end;

procedure TSnapDlg.Undo
  (
  var Msg: TMessage
  );
var
  Dir: array[0..fsPathName] of Char;
  DName: array[0..fsFileName + fsExtension] of Char;
  Ext: array[0..fsExtension] of Char;
  S: TOldFileStream;
  AData: PLayers;
  AObjects: PLayer;
  ASymbols: PLayer;
begin
  if MsgBox(Handle, 826, mb_IconQuestion or mb_YesNo, '') = idYes then
  begin
    FileSplit(FName, Dir, DName, Ext);
    StrCat(DName, '.SNP');
    StrCat(Dir, DName);
    if CheckFileExist(Dir) then
    begin
      SetCursor(LoadCursor(0, idc_Wait));
      PProj(Proj)^.SetStatusText(GetLangText(4000), TRUE);
      S := TOldFileStream.Create(Dir, stOpenRead);
      AData := Pointer(S.Get);
      AObjects := Pointer(S.Get);
      ASymbols := Pointer(S.Get);
      if S.Status <> stOK then
      begin
        MsgBox(Handle, 828, mb_IconExclamation or mb_OK, StrPas(DName));
        if AData <> nil then
          Dispose(AData, Done);
        if AObjects <> nil then
          Dispose(AObjects, Done);
        if ASymbols <> nil then
          Dispose(ASymbols, Done);
      end
      else
      begin
        Dispose(Layers^, Done);
        Layers^ := AData;
        Dispose(PProj(Proj)^.PInfo^.Objects, Done);
        PProj(Proj)^.PInfo^.Objects := AObjects;
        Dispose(PProj(Proj)^.PInfo^.Symbols, Done);
        PProj(Proj)^.PInfo^.Symbols := ASymbols;
        DidUndo := TRUE;
      end;
      S.Free;
      PProj(Proj)^.ClearStatusText;
      SetCursor(LoadCursor(0, idc_Arrow));
    end
    else
      MsgBox(Handle, 829, mb_IconExclamation or mb_OK, StrPas(DName));
  end;
end;

procedure TSnapDlg.Cancel
  (
  var Msg: TMessage
  );
begin
  if DidUndo then
    EndDlg(id_Yes)
  else
    EndDlg(id_Cancel);
end;

constructor TSnapOpt.Init;
begin
  TOldObject.Init;
  SearchNearest := FALSE;
end;

destructor TSnapOpt.Done;
begin
end;

constructor TSnapOpt.Load
  (
  S: TOldStream
  );
var
  Version: Word;
  SnapLayers: PLongColl;
begin
  S.Read(Version, SizeOf(Version));
  SnapLayers := Pointer(S.Get);
  if SnapLayers = nil then
    SnapLayers := New(PLongColl, Init(ciSnapOpt, ceSnapOpt));
  Dispose(SnapLayers, Done);
  SearchNearest := FALSE;
end;

procedure TSnapOpt.Store
  (
  S: TOldStream
  );
var
  Version: Word;
begin
  Version := 1;
  S.Write(Version, SizeOf(Version));
end;

end.

