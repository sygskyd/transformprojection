//*****************************************************************************
// Unit AttachHndl
//-----------------------------------------------------------------------------
// Author: Yuriy Glukhov
//-----------------------------------------------------------------------------
// Procedures und MenuHandlers for the Object Attachment Interface
//*****************************************************************************
//++ Glukhov ObjAttachment 06.10.00
unit AttachHndl;

interface

uses ProjHndl, AM_View, Objects, AM_Def;

  Type TAttachToHandler = class(TProjMenuHandler)
      Procedure   OnStart; override;
  end;

  Type TDetachFromHandler = class(TProjMenuHandler)
      Procedure   OnStart; override;
  end;

//-----------------
// Fills up the the given Collection by the objects that are attached to the given one.
Procedure FillUpCollectionByAttachedTo(
  Project: Pointer; Item: PView; AddList: PCollection; Recursive: Boolean );
  
// Fills up the Selection List by the attached objects that are not selected
// The additional objects are stored in the AddList also
Procedure FillUpSelectionByAttached(
  Project: Pointer; AddList: PCollection; Recursive: Boolean );

// Fills up the Selection List by the objects
// that are attached to the given one and not selected
// The additional objects are stored in the AddList also
Procedure FillUpSelectionByAttachedTo(
  Project: Pointer; Item: PView; AddList: PCollection; Recursive: Boolean );

// Removes the additional (attached) objects from the Selection List
Procedure ClearSelectionFromAdded( Project: Pointer; AddList: PCollection );

// Corrects all the attached objects for the given changed object
Procedure UpdateAllAttached(
  Project: Pointer; Item: PView; OldClipRect: TDRect; var RedrawRect: TDRect );

// Updates the given changed object and Corrects all the attached objects
Procedure UpdateWithAllAttached(
  Project: Pointer; Item: PView; OldClipRect: TDRect; var RedrawRect: TDRect );

// Redraws (and updates) the given changed object
// and Redraws (and updates) all the attached objects
Procedure RedrawWithAllAttached(
  Project: Pointer; Item: PView; OldClipRect: TDRect );

// Calculates the offset that corrects the attached object
// for the change of master ClipRect
Procedure CalcAttMove4ChangedMaster(
    AliMaster: LongInt;  OldClipRect, NewClipRect: TDRect;
    var XMove, YMove: LongInt );

implementation

{$R *.mfn}

uses  MenuFn, Windows,
      AM_Index, AM_Layer, AM_Proj, AM_ProjO
{++ IDB}
      , Am_Main, IDB_CallBacksDef
{-- IDB}
      , AM_Text   // Glukhov Bug#468 Build#162 01.08.01
;

//=============================================================================
//  TAttachToHandler
//=============================================================================

Procedure TAttachToHandler.OnStart;
begin
// Store the object to be attached
  Project^.ActualData:=
      Project.Layers.IndexObject( Project.PInfo, Project.SelList.At(0) );
//   Project^.ActualData:= PLayer( Project.PInfo.Objects ).IndexObject( Project.PInfo, Project.SelList.At(0) );
// Set the necessary mode
  Project^.SetActualMenu( mn_AttachObject );
end;


//=============================================================================
//  TDetachFromHandler
//=============================================================================

Procedure TDetachFromHandler.OnStart;
var
  DetItem     : PView;
  MstItem     : PView;
  TmpItem     : PIndex;
  i,n         : Integer;
begin
 n:=Project.SelList.GetCount;
 for i:=0 to n-1 do begin
// Get the object to be detached
  DetItem:=
      Project.Layers.IndexObject( Project.PInfo, Project.SelList.At(i) );
//   DetItem:= PLayer( Project.PInfo.Objects ).IndexObject( Project.PInfo, Project.SelList.At(0) );
// Get the master object for the selected one
//  TmpItem:= DetItem.GetObjMaster( Project.PInfo );
  MstItem:= DetItem.GetObjMaster( Project.PInfo );
  if (n = 1) and (not Assigned( MstItem )) then
  begin
  // There is no master object for the selected one
    MsgBox( Project.PInfo.HWindow, 11774, Mb_IconExclamation or Mb_OK, '' );
    Exit;
  end;
// Select the master object
  {$IFNDEF WMLT}
  ProcSelDeselIndex( Project, MstItem );
  {$ENDIF}
// Ask an acknowledgement
  if (i > 0) then begin
    DetItem.DetachMe;
    Project.SetModified;
  end
  else if MsgBox( Project.PInfo.HWindow, 11775, mb_IconQuestion or mb_YesNo or mb_DefButton1, '' ) = id_Yes then
  begin
    DetItem.DetachMe;
    Project.SetModified;
  end
  else begin
    exit;
  end;
// DeSelect the master object
  {$IFNDEF WMLT}
  ProcSelDeselIndex( Project, MstItem );
  {$ENDIF}
 end;
 Project.SetActualMenu( mn_Select );
end;

//=============================================================================
//  Procedures and Functions
//=============================================================================

// Fills up the Selection List by the attached objects that are not selected.
// The additional objects are stored in the AddList also.
Procedure FillUpSelectionByAttached(
  Project: Pointer; AddList: PCollection; Recursive: Boolean );
var
  i         : Integer;
  TstItem   : PView;
begin
  for i:=0 to PProj(Project).SelList.GetCount-1 do begin
    TstItem:=
    PProj(Project).Layers.IndexObject( PProj(Project).PInfo, PProj(Project).SelList.At( i ) );
    FillUpSelectionByAttachedTo( Project, TstItem, AddList, Recursive );
  end;
end;

// Fills up the the given Collection by the objects that are attached to the given one.
Procedure FillUpCollectionByAttachedTo(
  Project: Pointer; Item: PView; AddList: PCollection; Recursive: Boolean );
var
  Attached  : PCollection;
  i         : Integer;
  TstItem   : PView;
begin
  Attached:= Item.GetAttachedObjs;
  if Assigned( Attached ) then begin
    for i:=0 to Attached.Count-1 do begin
      TstItem:= Attached.At( i );
    // Add the object to the list
      if Assigned( AddList ) then AddList.Insert( TstItem );
      if Recursive
      then FillUpSelectionByAttachedTo( Project, TstItem, AddList, Recursive );
    end;
  end;
end;

// Fills up the Selection List by the objects that are attached
// to the given one and not selected.
// The additional objects are stored in the AddList also.
Procedure FillUpSelectionByAttachedTo(
  Project: Pointer; Item: PView; AddList: PCollection; Recursive: Boolean );
var
  Attached  : PCollection;
  i         : Integer;
  TstItem   : PView;
begin
  Attached:= Item.GetAttachedObjs;
  if Assigned( Attached ) then begin
    for i:=0 to Attached.Count-1 do begin
      TstItem:= Attached.At( i );
    // Check the attached object for selection
//      if not TstItem.GetState( sf_Selected ) then begin
      if not Assigned( PProj(Project).Layers.SelLayer.HasObject( TstItem ) ) then begin
      // Add the object to the list
        if Assigned( AddList ) then AddList.Insert( TstItem );
      // Add the object to the selected ones
//        ProcSelDeselIndex( PProj(Project), TstItem );
        {$IFNDEF WMLT}
        SelectIndexOnAllLayers( PProj(Project), TstItem );
        {$ENDIF}
        if Recursive
        then FillUpSelectionByAttachedTo( Project, TstItem, AddList, Recursive );
      end;
    end;
  end;
end;

// Removes the additional (attached) objects from the Selection List
Procedure ClearSelectionFromAdded( Project: Pointer; AddList: PCollection );
var
  i         : Integer;
  TstItem   : PIndex;
begin
  for i:=0 to AddList.Count-1 do begin
    TstItem:= AddList.At( i );
//++ Glukhov Bug#205 Build#128 28.11.00
//    ProcSelDeselIndex( PProj(Project), TstItem );
    {$IFNDEF WMLT}
    DeSelectIndexOnAllLayers( PProj(Project), TstItem );
    {$ENDIF}
//-- Glukhov Bug#205 Build#128 28.11.00
  end;
end;

//-----------------
// Calculates the offset that corrects the attached object
// for the change of master ClipRect
Procedure CalcAttMove4ChangedMaster(
  AliMaster: LongInt;
  OldClipRect, NewClipRect: TDRect;
  var XMove, YMove: LongInt );
begin
  case (AliMaster div 3) of
  0:  XMove:= NewClipRect.A.X - OldClipRect.A.X;
  1:  XMove:= (NewClipRect.A.X + NewClipRect.B.X) div 2
            - (OldClipRect.A.X + OldClipRect.B.X) div 2;
  2:  XMove:= NewClipRect.B.X - OldClipRect.B.X;
  end;
  case (AliMaster mod 3) of
  0:  YMove:= NewClipRect.B.Y - OldClipRect.B.Y;
  1:  YMove:= (NewClipRect.A.Y + NewClipRect.B.Y) div 2
            - (OldClipRect.A.Y + OldClipRect.B.Y) div 2;
  2:  YMove:= NewClipRect.A.Y - OldClipRect.A.Y;
  end;
end;

// Corrects all the attached objects for the given changed object
Procedure UpdateAllAttached(
  Project: Pointer; Item: PView; OldClipRect: TDRect; var RedrawRect: TDRect );
var
  i           : Integer;
  Attached    : PCollection;
  TstItem     : PView;
  XMove       : LongInt;
  YMove       : LongInt;
  CurClipRect : TDRect;
{++ IDB_UNDO}
  bDataMustBeStored: Boolean;
{-- IDB_UNDO}
begin
  Attached:= Item.GetAttachedObjs;
{++ IDB_UNDO}
  bDataMustBeStored := FALSE;
{-- IDB_UNDO}
  if Assigned( Attached ) then begin
  // For every attached object
    for i:=0 to Attached.Count-1 do begin
      TstItem:= Attached.At( i );
    // Calculates the necessary offset
      CalcAttMove4ChangedMaster(
          TstItem.AttData.AliPosOfMaster, OldClipRect, Item.ClipRect,  XMove, YMove );
    // Save the current ClipRect
      CurClipRect.InitByRect( TstItem.ClipRect );
{++ IDB_UNDO}
      {$IFNDEF WMLT}
        {$IFNDEF AXDLL} // <----------------- AXDLL
      If WinGisMainForm.WeAreUsingUndoFunction[Project] Then
         If WinGisMAinForm.UndoRedo_Man.AddObjectInUndoData(Project, TstItem) Then
            bDataMustBeStored := TRUE;
        {$ENDIF} // <----------------- AXDLL
      {$ENDIF}
{-- IDB_UNDO}
    // Correct the object ClipRect
      TstItem.MoveRel( XMove, YMove );            // IDB_Warning!!!
//++ Glukhov Bug#468 Build#162 01.08.01
      if (TstItem.GetObjType = ot_Text) and
         (PText(TstItem).fAdvMode = cwTxtAdvSelPoly) then begin
        PText(TstItem).TxtPolyData.hShift:= PText(TstItem).TxtPolyData.hShift - XMove;
        PText(TstItem).TxtPolyData.vShift:= PText(TstItem).TxtPolyData.vShift - YMove;
      end;
//-- Glukhov Bug#468 Build#162 01.08.01
      PProj(Project).UpdateClipRect( TstItem );   // IDB_Warning!!!
      PProj(Project).CorrectSize( TstItem.ClipRect, False );   // IDB_Warning!!!
      PProj(Project).SetModified;
    // Correct the rectangle for redrawing
      RedrawRect.CorrectByRect( CurClipRect );
      RedrawRect.CorrectByRect( TstItem.ClipRect );
    // Process the attached objects recurrently
      UpdateAllAttached( Project, TstItem, CurClipRect, RedrawRect );
    end;
{++ IDB_UNDO}
    {$IFNDEF WMLT}
      {$IFNDEF AXDLL} // <----------------- AXDLL
    if bDataMustBeStored Then
       WinGisMainForm.UndoRedo_Man.SaveUndoData(Project, utUnChange);
      {$ENDIF} // <----------------- AXDLL
    {$ENDIF}
{-- IDB_UNDO}
  end;
end;

// Updates the given changed object and Corrects all the attached objects
Procedure UpdateWithAllAttached(
  Project: Pointer; Item: PView; OldClipRect: TDRect; var RedrawRect: TDRect );
begin
  RedrawRect.CorrectByRect( OldClipRect );
  RedrawRect.CorrectByRect( Item.ClipRect );
  PProj(Project).UpdateClipRect( Item );
  PProj(Project).CorrectSize( Item.ClipRect, True );
  PProj(Project).SetModified;
  UpdateAllAttached( Project, Item, OldClipRect, RedrawRect );
end;

// Redraws (and updates) the given changed object
// and Redraws (and updates) all the attached objects
Procedure RedrawWithAllAttached(
  Project: Pointer; Item: PView; OldClipRect: TDRect );
var
  RedrawRect  : TDRect;
begin
  RedrawRect.InitByRect( OldClipRect );
  RedrawRect.CorrectByRect( Item.ClipRect );
  PProj(Project).UpdateClipRect( Item );
  PProj(Project).CorrectSize( Item.ClipRect, True );
  PProj(Project).SetModified;
  UpdateAllAttached( Project, Item, OldClipRect, RedrawRect );
  PProj(Project).PInfo.FromGraphic:= True;
  PProj(Project).PInfo.RedrawRect( RedrawRect );
end;





//=============================================================================
//  Initialization
//=============================================================================
initialization
  begin
    MenuFunctions.RegisterFromResource(HInstance, 'AttachHndl', 'AttachHndl');
    TAttachToHandler.Registrate('EditAttachTo');
    TDetachFromHandler.Registrate('EditDetachFrom');
  end;

end.
