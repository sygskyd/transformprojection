Unit OrganizeCopyDlg;

Interface

Uses Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,StdCtrls,
     WCtrls,MultiLng;

Type TOrganizeCopyDialog = Class(TWForm)
       AddBtn            : TButton;
       CancelBtn         : TButton;
       ItemList          : TListBox;
       MlgSection        : TMlgSection;
       ReplaceBtn        : TButton;
       WLabel1           : TWLabel;
    procedure FormShow(Sender: TObject);
      Private
       FReferences       : Boolean;
      Public
       Property    References:Boolean read FReferences write FReferences;  
     end;

Implementation

{$R *.DFM}

procedure TOrganizeCopyDialog.FormShow(Sender: TObject);
begin
  if FReferences then begin
    WLabel1.Caption:=MlgSection[6];
    ReplaceBtn.Caption:=MlgSection[7];
    AddBtn.Caption:=MlgSection[8]
  end;
end;

end.
