{****************************************************************************************}
{ Unit AM_Imp2                                                                           }
{----------------------------------------------------------------------------------------}
{ Diverse Methoden f�r ASCII-Import und Export.                                          }
{----------------------------------------------------------------------------------------}
{ Modifikationen                                                                         }
{ 13.01.1998 Heinz Ofner    wegen Platzmangel aus am_imp ausgelagert                     }
{****************************************************************************************}
Unit Am_imp2;

Interface

Uses Objects,WinProcs,WinTypes,AM_Def,AM_Ini,AM_Imp;

Function TextToRGBColor(Const AText:String;var Color:TColorRef):Boolean;

Function RGBColorToText(Color:TColorRef):String;

Function DeleteBlanks(AString:String):String;

Function ReadDDEItem(Source:PChar;var Item:String):PChar;

Function ReadTableFromIni(HWindow:THandle;Const Name:String;Options:Word;var ATable:PTable):Boolean;

Function WriteTableToIni(HWindow:THandle;Const Name:String;Options:Word;ATable:PTable):Boolean;

Implementation

Uses SysUtils;

{========================================================================================}
{ Diverses                                                                               }
{========================================================================================}

{****************************************************************************************}
{ Function TextToRGBColor                                                                }
{----------------------------------------------------------------------------------------}
{ Versucht den angegebenen Text in einen RGB-Farbwert umzuwandeln. Der Text mu� dazu mit }
{ einen $-Zeichen beginnen und mu� dann die RGB-Werte hexadezimal enthalten.             }
{----------------------------------------------------------------------------------------}
{ AText          i = umzuwandelnder Text                                                 }
{ Color          o = RBG-Farbwert                                                        }
{----------------------------------------------------------------------------------------}
{ Ergebnis: TRUE, wenn Umwandlung OK, FALSE bei Formatfehler                             }
{****************************************************************************************}
Function TextToRGBColor
   (
   Const AText     : String;
   var Color       : TColorRef
   )
   : Boolean;
  var Cnt          : Integer;
  begin
    TextToRGBColor:=FALSE;
    if AText[1]='$' then begin
      LongInt(Color):=0;
      for Cnt:=2 to Length(AText) do case UpCase(AText[Cnt]) of
        '0'..'9' : LongInt(Color):=LongInt(Color)*16+Ord(AText[Cnt])-Ord('0');
        'A'..'F' : LongInt(Color):=LongInt(Color)*16+10+Ord(UpCase(AText[Cnt]))-Ord('A');
        else Break;
      end;
      if Cnt<Length(AText) then Color:=0
      else TextToRGBColor:=TRUE;
    end;
  end;

{****************************************************************************************}
{ Function RGBColorToText                                                                }
{----------------------------------------------------------------------------------------}
{ Wandelt die angegebene Farbe in einen String um.                                       }
{----------------------------------------------------------------------------------------}
{ Color          i = RBG-Farbwert                                                        }
{----------------------------------------------------------------------------------------}
{ Ergebnis: Farbe als ASCII-Text.                                                        }
{****************************************************************************************}
Function RGBColorToText
   (
   Color           : TColorRef
   )
   : String;
  begin
    Result:=Format('$%.6X',[Color]);
  end;

{****************************************************************************************}
{ Function DeleteBlanks                                                                  }
{----------------------------------------------------------------------------------------}
{ L�scht Leerzeichen vom Anfang und vom Ende des Strings.                                }
{****************************************************************************************}
Function DeleteBlanks
   (
   AString         : String
   )
   : String;
  var Cnt          : Integer;
  begin
    Cnt:=1;
    while (Cnt<=Length(AString)) and (AString[Cnt]=' ') do Inc(Cnt);
    Delete(AString,1,Cnt-1);
    Cnt:=Length(AString);
    while (Cnt>0) and (AString[Cnt]=' ') do Dec(Cnt);
    Delete(AString,Cnt+1,Length(AString)-Cnt);
    DeleteBlanks:=AString;
  end;

{****************************************************************************************}
{ Function ReadDDEItem                                                                   }
{----------------------------------------------------------------------------------------}
{ Liest einen Eintrag aus einem DDE-String. Ausgehend von Source wird ein                }
{ in eckige Klammern eingeschlossener Eintrag gesucht. Der Text zwischen                 }
{ den Klammern wird in Item kopiert. Die Funktion liefert einen Zeiger auf               }
{ das Zeichen hinter der schlie�enden eckigen Klammer, bzw. NIL, wenn kein Eintrag       }
{ gefunden wurde.                                                                        }
{****************************************************************************************}
Function ReadDDEItem
   (
   Source          : PChar;
   var Item        : String
   )
   : PChar;
  var EndPos       : PChar;
  begin
    Item:='';
    EndPos:=NIL;
    Source:=AnsiStrScan(Source,'[');
    if Source<>NIL then begin
      EndPos:=AnsiStrScan(Source,']');
      if EndPos<>NIL then begin
        Inc(Source);
        Move(Source^,Item[1],EndPos-Source);
        Item[0]:=Char(EndPos-Source);
        EndPos:=EndPos+1;
        if EndPos^=#0 then EndPos:=NIL;
      end;
    end;
    ReadDDEItem:=EndPos;
  end;

{****************************************************************************************}
{ Function ReadTableFromIni                                                              }
{----------------------------------------------------------------------------------------}
{ Liest eine Umsetzungstabelle aus dem Ini-File.                                         }
{----------------------------------------------------------------------------------------}
{ HWindow          i = Handle des Fensters f�r MessageBox-Ausgaben                       }
{ Name             i = Name der Tabelle im Ini-File                                      }
{ Options          i = Tabellenoptionen (tblXXXX)                                        }
{ ATable           o = eingelesene Tabelle                                               }
{----------------------------------------------------------------------------------------}
{ Ergebnis: FALSE, wenn ein Fehler aufgetreten ist, TRUE sonst                           }
{****************************************************************************************}
Function ReadTableFromIni
   (
   HWindow         : THandle;
   Const Name      : String;
   Options         : Word;
   var ATable      : PTable
   )
   : Boolean;
  var Cnt          : Integer;
      AName        : String;
      AType        : Integer;
      ErrorPos     : Integer;
      ReadStr      : Array[0..255] of Char;
      Source       : PChar;
      Next         : PChar;
      Error        : Integer;
      AEntry       : PTableEntry;
      ADefault     : Integer;
      Entries      : Integer;
  Const erEntryMissing  = 1;           { gesamter Eintrag fehlt                          }
        erTypeMissing   = 2;           { Typangabe fehlt                                 }
        erInvalidChar   = 3;           { Ung�ltiges Zeichen in der Typdefinition         }
        erBlankEntry    = 4;           { Leerer Eintrag in der Namensliste               }
  begin
    ATable:=New(PTable,Init);
    Error:=0;
    Cnt:=0;
    AName:=Name+'Length'+#0;                               { L�nge der Tabelle einlesen  }
    GetPrivateProfileString(IniAppName,@AName[1],'',
        ReadStr,SizeOf(ReadStr),IniFile^.IniFile);
    if ReadStr[0]=#0 then Error:=erEntryMissing
    else begin
      Val(DeleteBlanks(StrPas(ReadStr)),Entries,ErrorPos);
      if ErrorPos<>0 then Error:=erInvalidChar;
    end;
    if Error=0 then while (Cnt<Entries)                    { alle Eintr�ge einlesen      }
        and (Error=0) do begin
      Str(Cnt,AName);
      AName:=Name+AName+#0;
      GetPrivateProfileString(IniAppName,@AName[1],'',
          ReadStr,SizeOf(ReadStr),IniFile^.IniFile);
      if ReadStr[0]=#0 then Error:=erEntryMissing
      else begin
        Source:=ReadStr;                                   { WinGIS-Typ einlesen         }
        Next:=StrScan(Source,',');
        if Next=NIL then Error:=erTypeMissing
        else begin
          Next^:=#0;
          AName:=DeleteBlanks(StrPas(Source));
          if Options and tblHasStrings<>0 then             { als String speichern oder in}
              AEntry:=New(PTableEntry,InitName(AName))     { Zahl umwandeln              }
          else begin
            Val(AName,AType,ErrorPos);
            AEntry:=New(PTableEntry,Init(AType));
            if ErrorPos<>0 then Error:=erInvalidChar;
          end;
          if Error=0 then repeat                           { Namensliste einlesen        }
            Source:=Next+1;
            Next:=StrScan(Source,',');
            if Next<>NIL then Next^:=#0;
            AName:=DeleteBlanks(StrPas(Source));
            if AName='' then Error:=erBlankEntry
            else AEntry^.InsertName(AName);
          until Next=NIL;
          ATable^.InsertEntry(AEntry);                     { Eintrag in Tabelle einf�gen }
        end;
      end;
      Inc(Cnt);
    end;
    if (Error=0)                                           { Defaultwert der Tabelle     }
        and (Options and tblHasDefault<>0) then begin      { einlesen                    }
      AName:=Name+'Default'+#0;
      GetPrivateProfileString(IniAppName,@AName[1],'',
          ReadStr,SizeOf(ReadStr),IniFile^.IniFile);
      if ReadStr[0]=#0 then Error:=erEntryMissing
      else begin
        if Options and tblHasStrings<>0 then ATable^.SetDefaultName(StrPas(ReadStr))
        else begin
          Val(DeleteBlanks(StrPas(ReadStr)),ADefault,ErrorPos);
          if ErrorPos<>0 then Error:=erInvalidChar
          else ATable^.SetDefault(ADefault);
        end;
      end;
    end;
    if Error<>0 then begin                                 { falls Fehler->Fehlermeldung }
      Dec(AName[0]);
      MsgBox(HWindow,3060+Error,mb_IconExclamation or mb_OK,AName);
      Dispose(ATable,Done);
      ATable:=NIL;
      ReadTableFromIni:=FALSE;
    end
    else ReadTableFromIni:=TRUE;
  end;

{****************************************************************************************}
{ Function WriteTableToIni                                                               }
{----------------------------------------------------------------------------------------}
{ Schreibt eine Umsetzungstabelle ins Ini-File.                                          }
{----------------------------------------------------------------------------------------}
{ HWindow          i = Handle des Fensters f�r MessageBox-Ausgaben                       }
{ Name             i = Name der Tabelle im Ini-File                                      }
{ Options          i = Tabellenoptionen (tblXXXX)                                        }
{ ATable           i = auszugebende Tabelle                                              }
{----------------------------------------------------------------------------------------}
{ Ergebnis: FALSE, wenn ein Fehler aufgetreten ist, TRUE sonst                           }
{****************************************************************************************}
Function WriteTableToIni
   (
   HWindow         : THandle;
   Const Name      : String;
   Options         : Word;
   ATable          : PTable
   )
   : Boolean;
  var Cnt          : Integer;
      Cnt2         : Integer;
      AEntry       : PTableEntry;
      AText        : Array[0..255] of Char;
      BText        : Array[0..255] of Char;
      Text         : String;
      AStr         : PString;
  begin
    if ATable<>NIL then begin
      IniFile^.WriteInteger(IniAppName,                    { L�nge der Tabelle schreiben }
          StrPCopy(AText,Name+'Length'),ATable^.GetEntryCount);
      if Options and tblHasDefault<>0 then begin           { Defaultwert schreiben       }
        if Options and tblHasStrings=0 then
            IniFile^.WriteInteger(IniAppName,
                StrPCopy(AText,Name+'Default'),ATable^.GetDefault)
        else WritePrivateProfileString(IniAppName,
            StrPCopy(AText,Name+'Default'),
            StrPCopy(BText,ATable^.GetDefaultName),IniFile^.IniFile);
      end;
      for Cnt:=0 to ATable^.GetEntryCount-1 do begin       { Tabelleneintr�ge            }
        AEntry:=ATable^.GetEntryAt(Cnt);
        if Options and tblHasStrings<>0 then               { Typeintrag                  }
            StrPCopy(AText,AEntry^.GetTypeName)
        else begin
          Str(AEntry^.GetType,Text);
          StrPCopy(AText,Text);
        end;
        StrCat(AText,',');
        StrPCopy(@AText[StrLen(AText)],                    { Exportname                  }
            PToStr(AEntry^.teExportName));
        for Cnt2:=0 to AEntry^.teNames.Count-1 do begin    { Rest der Namensliste        }
          AStr:=AEntry^.teNames.At(Cnt2);
          if AStr<>AEntry^.teExportName then begin
            StrCat(AText,',');
            StrPCopy(@AText[StrLen(AText)],PToStr(AStr));
          end;
        end;
        Str(Cnt,Text);
        Text:=Name+Text+#0;
        WritePrivateProfileString(IniAppName,@Text[1],AText,IniFile^.IniFile);
      end;
    end;
    WriteTableToIni:=TRUE;
  end;

end.