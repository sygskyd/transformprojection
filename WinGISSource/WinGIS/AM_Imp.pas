{****************************************************************************************}
{ Unit AM_Imp                                                                            }
{----------------------------------------------------------------------------------------}
{ ASCII-Import und Export.                                                               }
{----------------------------------------------------------------------------------------}
{ Autor: Martin Forst, 2. September 1994                                                 }
{----------------------------------------------------------------------------------------}
{ Modifikationen                                                                         }
{ 28.08.1995 Martin Forst    Offset und Zoom hinzugef�gt, Zeilenl�nge auf 3000 gesetzt,  }
{                            Positionen im ADF-File<3000                                 }
{****************************************************************************************}
Unit AM_Imp;

{$I-,O-}

Interface

Uses Graphics,WinProcs,WinTypes,Messages,SysUtils,WinDos,AM_Admin,AM_Circl,
     AM_Coll,AM_CPoly
     {$IFNDEF WMLT}
     ,AM_DDE
     {$ENDIF}
     ,AM_Def,AM_Dlg1,AM_Font,BmpImage,AM_Index,
     AM_Ini,AM_Layer,AM_Paint,AM_Point,AM_Poly,AM_Proj,AM_Splin,AM_StdDl,
     AM_Text,AM_View,AM_Sym,AM_Pass,ResDlg,Classes,Controls,Objects,
     UserIntf,ProjStyle;

Const maxReadBufferLen  = 65500;       { Gr��e des Lesebuffers                           }
      maxColumnNameLen  = 17;          { maximale L�nge eines Spaltennamens              }
                                       { im ADF-File                                     }
      maxLineLength     = 10000;       { maximale L�nge einer Zeile                      }
      maxColumnLength   = 255;         { maximale L�nge einer Spalte                     }
      maxDDEMemorySize  = 32000;       { Speichergr��e f�r die DDE-Kommunikation         }
      maxWriteBufferLen = 65500;       { Gr��e des Schreibbuffers                        }
      maxTableCount     = 8;           { Anzahl Tabellen                                 }
      maxTableNameLen   = 7;           { gr��te L�nge der Tabellennamen (in TableNames)  }
      maxWarnings       = 200;         { Abbruch des Imports nach dieser Warnungsanzahl  }

      { Initialisierungsgr��en der Collections                                           }
      ciColumnList      = 20;          { TColumnList                                     }
      ceColumnList      = 5;
      ciTableEntries    = 5;           { TTable.tblEntries                               }
      ceTableEntries    = 5;
      ciTableNames      = 5;           { TTableEntry.teNameList                          }
      ceTableNames      = 5;

      { Konstanten f�r die Spaltenart                                                    }
      ctScaleFactor     = -1;
      ctXPosition       = 0;
      ctYPosition       = 1;
      ctParamA          = 2;
      ctParamB          = 3;
      ctParamC          = 4;
      ctLayerName       = 5;
      ctObjectType      = 6;
      ctObjectNumber    = 7;
      ctTextLayer       = 8;
      ctBlockNumber     = 9;
      ctPrevBlockNumber = 10;

      ctImagePath       = 11;
      ctGraphic         = 12;
      ctDatabase        = 13;
      ctID              = 14;
      ctLast            = 14;

      { Objektnummern im ASCII-File                                                      }
      iotPoint          = 0;
      iotPoly           = 1;
      iotCPoly          = 2;
      iotSymbol         = 3;
      iotCircle         = 4;
      iotArc            = 5;
      iotText           = 6;
      iot2Spline        = 7;
      iot3Spline        = 8;
      iotAttribute      = 9;
      iotLayer          = 10;
      iotImage          = 11;
      iotInsert         = 12;
      iotComment        = 13;
      iotRText          = 17;

      { Konstanten f�r Type einer Datenbankspalte TColumn.coType                         }
      dbtNumeric        = 0;           { numerische Spalte                               }
      dbtChar           = 1;           { Zeichenspalte                                   }

      { IDs der Dialogelemente                                                           }
      idASCIIFile       = 100;         { TImportDlg, TExportDlg                          }
      idADFFile         = 101;
      idSymbolFile      = 102;
      idSkipGraphic     = 103;
      idGraphicObject   = 104;
      idGraphicLayer    = 105;
      idSkipObjects     = 106;
      idObjectLayer     = 107;
      idGraphicCombo    = 108;
      idObjectCombo     = 109;
      idFontSample      = 110;
      idFontHeight      = 111;
      idFileButton      = 54;
      idFontButton      = 507;
      idOptions         = 8;
      idOffsets         = 660;
      idLayerStyle      = 112;

      idProject         = 200;         { TExportDlg                                      }
      idLayers          = 201;
      idSelected        = 202;
      idLayerList       = 210;

      idList1           = 104;         { TOptionsDlg                                     }
      idList2           = 105;
      idDefault         = 106;
      idExport          = 107;
      idTableList       = 108;
      idText            = 109;
      idFontList        = 200;
      idInsertWinGIS    = 901;
      idDeleteWinGIS    = 902;
      idInsertASCII     = 903;
      idDeleteASCII     = 904;
      idList3           = 205;

      idLogList         = 110;         { TLogDlg                                         }
      idSave            = 54;

      idScale           = 100;
      idXOffset         = 101;
      idYOffset         = 102;
      idXOfs            = 104;
      idYOfs            = 105;

      { Konstanten f�r ReadField AOptions, bitcodiert                                    }
      rcBlankWarning    = $0001;       { bei Leerstring->Fehlermeldung                   }
      rcNotDefWarning   = $0002;       { wenn Spalte nicht definiert->Warnung            }
      rcNotDefError     = $0004;       { wenn Spalte nicht definiert->Fehlermeldung      }

      rcDefinedNotBlank = rcBlankWarning or rcNotDefWarning;

      { Konstanten f�r TASCIIData.ExportWhat                                             }
      exProject         = 0;           { gesamtes Projekt exportieren                    }
      exLayers          = 1;           { ausgew�hlte Layer exportieren                   }
      exSelected        = 2;           { selektierte Objekte exportieren                 }

      { Konstanten f�r ReadField AType                                                   }
      rwtCoord          = 1;           { Koordinate  (Skalierung)                        }
      rwtXCoord         = 2;           { X-Koordinate (Skalierung und XOffset)           }
      rwtYCoord         = 3;           { Y-Koordinate (Skalierung und XOffset)           }
      rwtLongInt        = 4;           { Longint                                         }
      rwtReal           = 5;           { Real                                            }
      rwtString         = 6;           { String                                          }

      { Konstanten f�r die Dateierweiterungen                                            }
      fexASCIIFile      = '.asc';
      fexADFFile        = '.adf';
      fexSymbolFile     = '.sym';

      { Indizes f�r TableNames und TableDefault                                          }
      tnColumns         = 0;
      tnColor           = 1;
      tnAlign           = 2;
      tnStyle           = 3;
      tnLine            = 4;
      tnPattern         = 5;
      tnDBType          = 6;
      tnFont            = 7;

      { Namen der Tabellen im .Ini-File                                                  }
      TableNames        : Array[0..maxTableCount-1] of String[maxTableNameLen] =
          ('Column','Color','Align','Style','Line','Pattern','DBType','Font');

      { Konstanten f�r TableOptions                                                      }
      tblHasDefault     = $0001;       { Tabelle besitzt einen Defaultwert               }
      tblHasStrings     = $0002;       { Typeintr�ge sind Strings                        }

      { besitzen die Tabellen Defaultwerte?                                              }
      TableOptions      : Array[0..maxTableCount-1] of Word =
                              (0,tblHasDefault,tblHasDefault,tblHasDefault,tblHasDefault,
                               tblHasDefault,0,tblHasDefault+tblHasStrings);

      { Konstanten f�r TOptionsDlg.TableNumber                                           }
      otnFont           = 0;
      otnLine           = 1;
      otnPattern        = 2;
      otnColor          = 3;

      otnLast           = 3;

      IniAppName        = 'ASCII Import';   { Name des Application-Abschnitts im Ini-File}

Type {***********************************************************************************}
     { Object TColumn                                                                    }
     {-----------------------------------------------------------------------------------}
     { Definition einer Spalte                                                           }
     {-----------------------------------------------------------------------------------}
     { coName           = Name der Spalte                                                }
     { coStartPosition  = Anfangsposition in der Zeile                                   }
     { coEndPosition    = Endposition in der Zeile                                       }
     { coDatabaseType   = Spaltentyp in der Datenbank (dbtXXXX)                          }
     { coDatabaseLength = L�nge der Spalte in der Datenbank                              }
     { coNext           = n�chste Spalte (f�r verkettete Liste)                          }
     {***********************************************************************************}
     PColumn            = ^TColumn;
     TColumn            = Object(TOldObject)
       coState          : Word;
       coName           : String;
       coStartPosition  : Integer;
       coEndPosition    : Integer;
       coDatabaseType   : Integer;
       coDatabaseLength : Integer;
       coNext           : PColumn;
       Constructor Init(Const AName:String;AStart,AEnd,AType,ALen:Integer;ANext:PColumn);
       Destructor  Done; virtual;
     end;

     POffsetData        = ^TOffsetData;
     TOffsetData        = Record
       XOffset          : LongInt;
       YOffset          : LongInt;
       Scale            : Real;
     end;

     {***********************************************************************************}
     { Object TASCIIData                                                                 }
     {-----------------------------------------------------------------------------------}
     { Daten f�r Import- und Exportdialog.                                               }
     {-----------------------------------------------------------------------------------}
     { ASCIIFile        = Name der ASCII-Datei                                           }
     { ADFFile          = Name der ASCII-Definitionsdatei                                }
     { SymbolFile       = Name der Symbolzuordnungsdatei                                 }
     { GraphicFont      = Font f�r Grafiktexte                                           }
     { GraphicLayer     = Layer f�r Grafiktexte ohne Layerangabe oder 0, wenn solche     }
     {                    Texte nicht erzeugt werden sollen                              }
     { ObjectLayer      = Layer f�r Objekte ohne Layerangabe oder 0, wenn solche Objekte }
     {                    nicht importiert werden sollen                                 }
     { ExportWhat       = was soll exportiert werden (exXXXX)                            }
     { ExportLayers     = Indizes der zu exportierenden Layer (wenn ExportWhat=exLayers) }
     { NewLayerStyle    = soll Layerstil der ASCII-Datei genommen werden                 }
     {***********************************************************************************}
     PASCIIData        = ^TASCIIData;
     TASCIIData        = Object(TOldObject)
       ASCIIFile        : String;
       ADFFile          : String;
       SymbolFile       : String;
       GraphicFont      : TFontData;
       GraphicFontVert  : Boolean; {mf}
       GraphicLayer     : LongInt;
       ObjectLayer      : LongInt;
       ExportWhat       : Integer;
       ExportLayers     : PLongColl;
       ImpOffsets       : TOffsetData;
       ExpOffsets       : TOffsetData;
       NewLayerStyle    : Word;
       Constructor Init;
       Destructor  Done; virtual;
     end;

     {***********************************************************************************}
     { Object TStringColl                                                                }
     {-----------------------------------------------------------------------------------}
     { Liste von PStrings, alphabetisch sortiert, Gro�schreibung nicht ber�cksichtigt.   }
     {***********************************************************************************}
     PStringColl        = ^TStringColl;
     TStringColl        = Object(TStringCollection)
       Function         Compare(Key1,Key2:Pointer):Integer; virtual;
     end;

     {***********************************************************************************}
     { Object TTableEntry                                                                }
     {-----------------------------------------------------------------------------------}
     { teType             = Typ (interne Wingis-Nummer)                                  }
     { teTypeName         = Typ, wenn f�r die Tabelle TableOptions=tboHasStrings ist.    }
     { teNames            = Liste von Namen im ASCII-File                                }
     { teEportName        = Zeiger auf den Namen, der beim Export f�r diesen Type ver-   }
     {                      wendet wird. Zeigt in teNames, darf nicht freigegeben werden.}
     {***********************************************************************************}
     PTableEntry        = ^TTableEntry;
     TTableEntry        = Object(TOldObject)
       teType           : LongInt;
       teTypeName       : PString;
       teNames          : TStringColl;
       teExportName     : PString;
       Constructor Init(AType:LongInt);
       Constructor InitName(Const AName:String);
       Destructor  Done; virtual;
       Function    GetExportName:String;
       Function    GetType:LongInt;
       Function    GetTypeName:String;
       Function    SearchName(Const AName:String):Integer;
       Procedure   SetExportName(AIndex:Integer);
       Function    InsertName(Const AName:String):Integer;
       Function    IsValid:Integer;
     end;

     {***********************************************************************************}
     { Object TTable                                                                     }
     {-----------------------------------------------------------------------------------}
     { Eine Zuordnungstabelle ASCII->WinGIS.                                             }
     {-----------------------------------------------------------------------------------}
     { tblDefault         = Vorgabewert, wenn ein Name nicht in der Typliste ist.        }
     { tblDefaultName     = Vorgabewert f�r Tabellen mit tblHasStrings                   }
     { tblEntries         = Liste von TTabelEntry                                        }
     {***********************************************************************************}
     PTable             = ^TTable;
     TTable             = Object(TOldObject)
         Private
       tblDefault       : LongInt;
       tblDefaultName   : PString;
       tblEntries       : TCollection;
         Public
       Constructor Init;
       Destructor  Done; virtual;
       Function    GetDefault:Integer;
       Function    GetDefaultName:String;
       Function    GetEntry(AType:Integer):PTableEntry;
       Function    GetEntryAt(At:Integer):PTableEntry;
       Function    GetEntryCount:Integer;
       Procedure   InsertEntry(AEntry:PTableEntry);
       Function    IsValid:Integer;
       Function    NameToType(Const AName:String;var Found:Boolean):LongInt;
       Function    NameToTypeName(Const AName:String;var Found:Boolean):String;
       Function    SearchTypeName(Const AName:String):Integer;
       Procedure   SetDefault(ADefault:LongInt);
       Procedure   SetDefaultName(Const AName:String);
       Function    TypeToName(AType:LongInt):String;
       Function    TypeNameToName(Const AType:String):String;
     end;

     PTables            = ^TTables;
     TTables            = Array[0..maxTableCount-1] of PTable;

     {***********************************************************************************}
     { Dialog TLogDlg                                                                    }
     {-----------------------------------------------------------------------------------}
     { Report- und Abbruchfenster beim Import bzw. Export.                               }
     {***********************************************************************************}
     TLogDlg        = Class(TAbortDlg)
      Public
       Title        : PChar;
       Constructor Init(AParent:TComponent;ATitle:PChar);
       Procedure   AddBlankLine;
       Procedure   AddString(Const AText:String;AddLine:Boolean);
       Procedure   AddText(AText:Integer;myParams:Array of Const;AddLine:Boolean);
       Procedure   ISave(var Msg:TMessage); message id_First+idSave;
       Procedure   SetupWindow; override;
     end;

     {***********************************************************************************}
     { Object TSymConvert                                                                }
     {-----------------------------------------------------------------------------------}
     { Wird vom Import und Export verwendet, um f�r Objekt Inserts die Objektnummer in   }
     { ASCII-File der Objektnummer von WinGIS zuzuordnen.                                }
     {-----------------------------------------------------------------------------------}
     { Index            i = Nummer des Objekts im ASCII-File                             }
     { WinGISNumber       = Nummer des Objekts in WinGIS     (bzw. umgekehrt bei Export) }
     {***********************************************************************************}
     PSymConvert        = ^TSymConvert;
     TSymConvert        = Object(TIndex)
       WinGISNumber     : LongInt;
       Constructor Init(AASCII,AWinGIS:LongInt);
     end;

     {***********************************************************************************}
     { Object TSymConvertList                                                            }
     {-----------------------------------------------------------------------------------}
     { Liste von PSymConvert, sortiert nach TSymConvert.Index.                           }
     {***********************************************************************************}
     PConvertList       = ^TSymConvertList;
     TSymConvertList    = Object(TSortedCollection)
       Function    Compare(Key1,Key2:Pointer):Integer; virtual;
       Function    KeyOf(Item:Pointer):Pointer; virtual;
     end;

     TInProgress        = (ipNone,ipASCIIImport,ipASCIIExport,ipDXFImport,
                           ipDXFExport,ipDBASCIIImport,ipDBASCIIWaiting);

     {***********************************************************************************}
     { Object TImport                                                                    }
     {-----------------------------------------------------------------------------------}
     { imReadBuffer     = Puffer f�r das Lesen vom ASCII-File                            }
     { imScaleFactor    = Skalierungsfaktor f�r Symbole                                  }
     { imColumns        = Liste aller Spalten                                            }
     { imProject        = Projektdaten                                                   }
     { imOwner          = Projektfenster                                                 }
     { imTables         = Umsetzungstabellen                                             }
     { imLogDlg         = Reportdialog                                                   }
     { imWarnings       = Anzahl der aufgetretenen Warnungen                             }
     { imSymConvert     = Symbolzuordnungstabelle                                        }
     { imDBEnd          = TRUE, wenn die Datenbank ein [END][] gesendet hat              }
     { imDBData         = Zwischenspeicher f�r Daten von der Datenbank                   }
     { imExportedCnt    = Anzahl exportierter Objekte                                    }
     { imAllObjects     = Gesamtanzahl zu exportierender Objekte                         }
     {***********************************************************************************}
     PImport            = ^TImport;
     TImport            = Object(TOldObject)
       imInProgress     : TInProgress;
       imLogDlg         : TLogDlg;
       imDDELines       : PStrCollection;
       imErrString      : PChar;
      Private
       imScaleFactor    : Real;
       imColumns        : Array[ctXPosition..ctLast] of PColumn;
       imProject        : PProj;
       imOwner          : TWinControl;
       imOptions        : TASCIIData;
       imTables         : TTables;
       imWarnings       : LongInt;
       imSymConvert     : PConvertList;
       imDBEnd          : Boolean;
       imDBData         : PIndexColl;
       imExportedCnt    : LongInt;
       imAllObjects     : LongInt;
       imError          : Boolean;
       AscFileName      : Array[0..fsPathName] of Char;
       imDoDatabase     : Boolean;
       imDBEndSend      : Boolean;
      Public
       Constructor Init(AOwner:TWinControl;AProject:PProj);
       Destructor  Done; virtual;
       Procedure   DBEnd;
       Procedure   DBError;
       Procedure   DBExportData;
       Procedure   ExportASCII;
       Procedure   ExportDXF;
       Procedure   ImportASCII;
       Procedure   ImportASCIIFromDB;
       Procedure   InsertDDEData(AData:PChar);
       Procedure   ExecuteDXFIn;
       Procedure   ImportDXF(AData:PProj);
       Procedure   InsertObjectAndLayer(Item: PView;LayerName:String);
      Private
       Function    CheckColumnOverlap:Boolean;
       Function    ExportDialog:Boolean;
       Function    ExportASCIIFile:Boolean;
       Procedure   FreeTables;
       Function    ImportDialog:Boolean;
       Function    ImportASCIIFile:Boolean;
       Function    ReadADFFile:Boolean;
       Function    ReadSymFile(ForExport:Boolean):Boolean;
       Function    ReadTables:Boolean;
       Procedure   SendImportDBDefinition;
       Procedure   SendExportDBDefinition;
     end;

     {***********************************************************************************}
     { Dialog TImportDlg                                                                 }
     {-----------------------------------------------------------------------------------}
     { Dialog zur Eingabe der Dateinamen und diverser Einstellungen f�r Import.          }
     {***********************************************************************************}
     TImportDlg         = Class(TLayerDlg)
      Public
       Data             : PASCIIData;
       FileField        : Integer;
       PInfo            : PPaint;
       AFont            : TFontData;
       IsImport         : Boolean;
       Constructor Init(AParent:TComponent;AName:PChar;AData:PASCIIData;ALayers:PLayers;
                       APInfo:PPaint);
       Function    CanClose:Boolean; override;
       Function    CheckFile(AId:Integer;AText:PChar;ForWrite:Boolean):Boolean;
       Procedure   OffsetsDlg(var Msg:TMessage); message id_First+idOffsets;
       Procedure   OptionsDlg(var Msg:TMessage); message id_First+idOptions;
       Procedure   ExpandFileName(AId:Integer);
       Procedure   SelectFile(var Msg:TMessage); message id_First+idFileButton;
       Procedure   SelectFont(var Msg:TMessage); message id_First+idFontButton;
       Procedure   SetFont;
       Procedure   SetupWindow; override;
       Procedure   WMCommand(var Msg:TWMCommand); message wm_First+wm_Command;
       Procedure   WMDrawObject(var Msg:TMessage); message wm_First+wm_DrawObject;
     end;

     {***********************************************************************************}
     { Dialog TExportDlg                                                                 }
     {-----------------------------------------------------------------------------------}
     { Dialog zur Eingabe der Dateinamen und diverser Einstellungen f�r Export.          }
     {***********************************************************************************}
     PExportDlg         = ^TExportDlg;
     TExportDlg         = Class(TImportDlg)
      Public
       Constructor Init(AParent:TComponent;AName:PChar;AData:PASCIIData;ALayers:PLayers;APInfo:PPaint);
       Function    CanClose:Boolean; override;
       Procedure   SetupWindow; override;
       Procedure   WMCommand(var Msg:TWMCommand); message wm_First+wm_Command;
     end;

     {***********************************************************************************}
     { Dialog TOptionsDlg                                                                }
     {-----------------------------------------------------------------------------------}
     { Dialog zum Setzen der Import-Optionen, Wartung der Umwandlungstabellen.           }
     {***********************************************************************************}
     POptionsDlg        = ^TOptionsDlg;
     TOptionsDlg        = Class(TGrayDialog)
      Public
       Tables           : Array[0..otnLast] of PTable;
       CurTableNumber   : Integer;
       CurEntry         : PTableEntry;
       WinGISList       : Integer;
       ASCIIText        : TREdit;
       WingisDefault    : TRCheckBox;
       CurTableOption   : Word;
       Constructor Init(AParent:TComponent);
       Function    CanClose:Boolean; override;
       Function    CheckTable(ATable:Integer):Boolean;
       Procedure   DrawWinGISListItem(Info:PDrawItemStruct);
       Procedure   FillASCIIList;
       Procedure   IDefault(var Msg:TWMCommand); message id_First+idDefault;
       Procedure   IDeleteASCII(var Msg:TWMCommand); message id_First+idDeleteASCII;
       Procedure   IDeleteWinGIS(var Msg:TWMCommand); message id_First+idDeleteWinGIS;
       Procedure   IExport(var Msg:TWMCommand); message id_First+idExport;
       Procedure   IInsertASCII(var Msg:TWMCommand); message id_First+idInsertASCII;
       Procedure   IInsertWinGIS(var Msg:TWMCommand); message id_First+idInsertWinGIS;
       Procedure   IList1(var Msg:TWMCommand); message id_First+idList1;
       Procedure   IList2(var Msg:TWMCommand); message id_First+idList2;
       Procedure   IList3(var Msg:TWMCommand); message id_First+idList3;
       Procedure   ITableList(var Msg:TWMCommand); message id_First+idTableList;
       Procedure   IText(var Msg:TWMCommand); message id_First+idText;
       Procedure   SendFocusChange(AId:Integer);
       Function    SetTable(TableNumber:Integer):Boolean;
       Procedure   SetupWindow; override;
       Procedure   WMDrawItem(var Msg:TWMDrawItem); message wm_First+wm_DrawItem;
       Function    WriteTables:Boolean;
     end;

     TOffsetDlg         = Class(TGrayDialog)
      Public
       Data             : POffsetData;
       CoordinateSystem : Integer;
       Constructor Init(AParent:TComponent;AData:POffsetData;ASystem:Integer);
       Procedure   SetupWindow; override;
       Function    CanClose:Boolean; override;
     end;

Implementation

Uses AM_Child,Win32Def,Dialogs,AM_RText,FontStyleDlg,AM_Imp2,LayerHndl,
     CoordinateSystem, ExtCanvas;

Type TAction            = (doInitialize,    { neues Objekt initialisieren                }
                           doExpand,        { Objekt um eine Daten einer Zeile erweitern }
                           doFinish);       { Objekt abschlie�en und einf�gen            }

     {***********************************************************************************}
     { Record TLine                                                                      }
     {-----------------------------------------------------------------------------------}
     { Beschreibt die Lesequelle f�r die Routinen des Imports un zeigt entweder auf die  }
     { aktuelle Zeile im Lesebuffer oder auf die gesicherte Zeile.                       }
     {-----------------------------------------------------------------------------------}
     { LineStart          = Zeiger auf den Zeilenanfang                                  }
     { LineEnd            = Zeiger auf das Zeilenende                                    }
     { LineLength         = LineEnd-LineStart                                            }
     {***********************************************************************************}
     PLine              = ^TLine;
     TLine              = Record
       LineStart        : PChar;
       LineEnd          : PChar;
       LineLength       : Word;
     end;


     {***********************************************************************************}
     { Object TDBListItem                                                                }
     {-----------------------------------------------------------------------------------}
     PDBListItem        = ^TDBListItem;
     TDBListItem        = Object(TIndex)
       dbData           : PChar;
       Constructor Init(AData:PChar);
       Destructor  Done; virtual;
     end;

{========================================================================================}
{ Diverses                                                                               }
{========================================================================================}

{========================================================================================}
{ TColumn                                                                                }
{========================================================================================}

Constructor TColumn.Init
   (
   Const AName     : String;
   AStart          : Integer;
   AEnd            : Integer;
   AType           : Integer;
   ALen            : Integer;
   ANext           : PColumn
   );
  begin
    inherited Init;
    coName:=AName;
    coStartPosition:=AStart;
    coEndPosition:=AEnd;
    coDatabaseType:=AType;
    coDatabaseLength:=ALen;
    coNext:=ANext;
    coState:=0;
  end;

Destructor TColumn.Done;
  begin
    if coNext<>NIL then begin
      Dispose(coNext,Done);
      coNext:=NIL;
    end;
    inherited Done;
  end;

{========================================================================================}
{ TImport                                                                                }
{========================================================================================}

Constructor TImport.Init
   (
   AOwner          : TWinControl;
   AProject        : PProj
   );
  begin
    inherited Init;
    FillChar(imColumns,SizeOf(imColumns),#0);
    imOwner:=AOwner;
    imProject:=AProject;
    FillChar(imTables,SizeOf(imTables),#0);
    imOptions.Init;
    imSymConvert:=NIL;
    imDBData:=NIL;
    imInProgress:=ipNone;
    imLogDlg:=NIL;
    imDDELines:=New(PStrCollection,Init(5,5));
  end;

{****************************************************************************************}
{ Function TImport.ReadADFFile                                                           }
{----------------------------------------------------------------------------------------}
{ Liest das ADF-File ein. Fehlermeldungen werden in imLogDlg ausgegeben.                 }
{----------------------------------------------------------------------------------------}
{ Ergebnis: TRUE, wenn OK, FALSE, wenn ein Fehler aufgetreten ist.                       }
{****************************************************************************************}
Function TImport.ReadADFFile
   : Boolean;
  var ADFFile           : Text;        { ADF-Datei                                       }
      ReadStr           : String;      { Lesebuffer                                      }
      Error             : Boolean;     { Fehler beim Lesen aufgetreten                   }
      InDefinition      : Boolean;     { im Definitionsabschnitt der Datei?              }
      CurLine           : Integer;     { aktuelle Zeile in der Datei                     }
      ColumnType        : Integer;     { Typ der aktuellen Spalte                        }
      ColumnName        : String;      { Name der aktuellen Spalte                       }
      NameFound         : Boolean;
{$IFDEF MRING}
      Size              : Integer;     { L�nge des adf-file-Eintrages in der ini         }
      ADFName           : Array [1..255] of Char;  { adf-file-Eintrag in der ini         }
{$ENDIF}
  Const erOpenFile           = 0;      { Fehler beim �ffnen der Datei                    }
        erInvalidChar        = 1;      { ung�ltiges Zeichen in Zahlenangabe              }
        erColumnDefMissing   = 2;      { Spaltendefinition fehlt                         }
        erScaleFactMissing   = 3;      { Skalierungsfaktor fehlt                         }
        erInvalidKeyWord     = 4;      { ung�ltiges Schl�sselwort                        }
        erDoubleKeyWord      = 5;      { Schl�sselwort doppelt verwendet                 }
        erInvalidPositions   = 6;      { StartPos > EndPos                               }
        erColumnTooLong      = 7;      { Spaltenl�nge > maxColumnLength                  }
        erInvalidDBLength    = 8;      { Ung�ltiges Zeichen in Breitenangabe der DBSpalte}
        erInvalidDBType      = 9;      { unbekannter Typ f�r Datenbanlspalte             }
        erReadFile           = 10;     { Fehler beim Lesen der Datei                     }
        erInvalidStartPos    = 11;     { Startposition ist 0                             }
        erPositionTooLarge   = 12;     { Position > maximale Zeilenl�nge                 }
  {**************************************************************************************}
  { Procedure ErrorMessage                                                               }
  {--------------------------------------------------------------------------------------}
  { Gibt eine WhatError entsprechende Fehlermeldung aus. Error wird auf TRUE gesetzt.    }
  {--------------------------------------------------------------------------------------}
  { WhatError      = Nummer des Fehlers (erXXXX)                                         }
  {--------------------------------------------------------------------------------------}
  { Seiteneffekte: Error=TRUE                                                            }
  {**************************************************************************************}
  Procedure ErrorMessage
     (
     WhatError          : Integer
     );
    begin
      case WhatError of
        erOpenFile,
        erReadFile         : imLogDlg.AddText(3000+WhatError,[imOptions.ADFFile,ColumnName],FALSE);
        erColumnTooLong    : imLogDlg.AddText(3000+WhatError,[imOptions.ADFFile,ColumnName,maxColumnLength],FALSE);
        erPositionTooLarge : imLogDlg.AddText(3000+WhatError,[imOptions.ADFFile,ColumnName,maxLineLength],FALSE);
        else imLogDlg.AddText(3000+WhatError,[curLine,ColumnName],FALSE);
      end;
      Error:=TRUE;
    end;
  {**************************************************************************************}
  { Procedure ReadInsertColumn                                                           }
  {--------------------------------------------------------------------------------------}
  { Liest die n�chste Zeile aus dem ADF-File und wandelt den Text in die Spaltenanfangs- }
  { -endposition um. Tritt dabei kein Fehler auf, wird in imColumns eine neue            }
  { Spaltendefinition angeh�ngt.                                                         }
  {--------------------------------------------------------------------------------------}
  { ColumnType     i = Typ der Spalte (an welche Liste anh�ngen)                         }
  {--------------------------------------------------------------------------------------}
  { Seiteneffekte: ReadStr                                                               }
  {**************************************************************************************}
  Procedure ReadInsertColumn
     (
     ColumnType    : Integer
     );
    var APos       : Integer;
        StartStr   : String;
        EndStr     : String;
        LengthStr  : String;
        TypeStr    : String;
        StartPos   : Integer;
        EndPos     : Integer;
        ErrorPos   : Integer;
        DBLength   : Integer;
        DBType     : Integer;
        NameFound  : Boolean;
    begin
      ReadLn(ADFFile,ReadStr);                             { Definition einlesen         }
      if IoResult<>0 then ErrorMessage(erReadFile)
      else begin
        Inc(CurLine);
        ReadStr:=DeleteBlanks(ReadStr);
        if ReadStr='' then ErrorMessage(erColumnDefMissing)
        else begin
          DBLength:=0;
          DBType:=-1;
          TypeStr:='';
          LengthStr:='';
          APos:=Pos(' ',ReadStr);
          StartStr:=DeleteBlanks(Copy(ReadStr,1,APos));    { Spaltenanfang               }
          Delete(ReadStr,1,APos);
          APos:=Pos(' ',ReadStr);
          if APos=0 then EndStr:=ReadStr
          else begin
            EndStr:=DeleteBlanks(Copy(ReadStr,1,APos));    { Spaltenende                 }
            Delete(ReadStr,1,APos);
            APos:=Pos(' ',ReadStr);
            if APos=0 then TypeStr:=ReadStr                { Datenbanktyp                }
            else begin
              TypeStr:=DeleteBlanks(Copy(ReadStr,1,APos));
              Delete(ReadStr,1,APos);
              APos:=Pos(' ',ReadStr);
              if APos=0 then LengthStr:=ReadStr            { L�nge der Datenbankspalte   }
              else LengthStr:=DeleteBlanks(Copy(ReadStr,1,APos));
            end;
          end;
          Val(StartStr,StartPos,ErrorPos);
          if ErrorPos=0 then Val(EndStr,EndPos,ErrorPos);
          if ErrorPos<>0 then ErrorMessage(erInvalidPositions)
          else if (TypeStr<>'')
              and (ColumnType=ctDatabase) then begin
            DBType:=imTables[tnDBType]^.NameToType(TypeStr,NameFound);
            if not NameFound then ErrorMessage(erInvalidDBType)
            else begin
              Val(LengthStr,DBLength,ErrorPos);
              if ErrorPos<>0 then ErrorMessage(erInvalidDBLength);
            end;
          end;
          if not Error then begin
            if StartPos>EndPos then ErrorMessage(erInvalidPositions)
            else if EndPos-StartPos>maxColumnLength then ErrorMessage(erColumnTooLong)
            else if EndPos>maxLineLength then ErrorMessage(erPositionTooLarge)
            else if StartPos+EndPos<>0 then begin
              if StartPos=0 then ErrorMessage(erInvalidStartPos)
              else imColumns[ColumnType]:=
                New(PColumn,Init(ColumnName,StartPos,EndPos,DBType,DBLength,imColumns[ColumnType]));
            end;
          end;
        end;
      end;
    end;
  {**************************************************************************************}
  { Procedure ReadScaleFactor                                                            }
  {--------------------------------------------------------------------------------------}
  { Liest die n�chste Zeile aus dem ADF-File und wandelt den Text in den Ma�stabsfaktor  }
  { um. Tritt dabei ein Fehler auf, wird ErrorMessage aufgerufen.                        }
  {--------------------------------------------------------------------------------------}
  { Seiteneffekte: ReadStr                                                               }
  {**************************************************************************************}
  Procedure ReadScaleFactor;
    var ErrorPos   : Integer;
    begin
      ReadLn(ADFFile,ReadStr);
      if IoResult<>0 then ErrorMessage(erReadFile)
      else begin
        Inc(CurLine);
        DeleteBlanks(ReadStr);
        if ReadStr='' then ErrorMessage(erScaleFactMissing)
        else begin
          Val(ReadStr,imScaleFactor,ErrorPos);
          if ErrorPos<>0 then ErrorMessage(erInvalidChar)
        end;
      end;
    end;
  begin
    Error:=FALSE;
{$IFNDEF MRING}
    imLogDlg.AddText(3600,[imOptions.ADFFile],TRUE);
    Assign(ADFFile,imOptions.ADFFile);
{$ENDIF}
{$IFDEF MRING}
    GetPrivateProfileString('ASCII Import','ADFFILE','',@ADFName,Size,AM_Ini.IniFile^.IniFile);
    Assign(ADFFile,ADFName);
{$ENDIF}
    Reset(ADFFile);
    if IOResult<>0 then ErrorMessage(erOpenFile)
    else begin
      CurLine:=0;
      InDefinition:=TRUE;
      while not(Eof(ADFFile))
          and not(Error) do begin
        ReadLn(ADFFile,ColumnName);
        if IoResult<>0 then ErrorMessage(erReadFile)
        else begin
          Inc(CurLine);
          DeleteBlanks(ColumnName);
          if ColumnName<>'' then begin
            ColumnType:=imTables[tnColumns]^.NameToType(ColumnName,NameFound);
            if InDefinition then begin
              if not NameFound then ErrorMessage(erInvalidKeyWord)
              else case ColumnType of
                ctScaleFactor  : ReadScaleFactor;
                ctGraphic,
                ctDataBase     : ErrorMessage(erInvalidKeyWord)
                else if imColumns[ColumnType]<>NIL then ErrorMessage(erDoubleKeyWord)
                     else ReadInsertColumn(ColumnType);
              end;
            end
            else if ColumnType=ctGraphic then ReadInsertColumn(ctGraphic)
            else ReadInsertColumn(ctDataBase);
          end
          else if InDefinition then InDefinition:=FALSE;
        end;
      end;
      Close(ADFFile);
    end;
    if not Error then if CheckColumnOverlap then Error:=TRUE;
    if Error then imLogDlg.AddBlankLine;
    ReadADFFile:=not Error;
  end;

Function TImport.ReadSymFile
   (
   ForExport            : Boolean
   )
   : Boolean;
  var Error             : Boolean;
      SymFile           : Text;
      ReadStr           : String;
      CurLine           : Integer;
      Number1           : LongInt;
      Number2           : LongInt;
      SConvert          : PSymConvert;
      APos              : Integer;
      ErrorPos          : Integer;
{$IFDEF MRING}
      Size              : Integer;
      SYMName           : Array [1..255] of Char;
{$ENDIF}
  Const erOpenFile      = 0;
        erReadFile      = 1;
        erInvalidChar   = 2;
        erFormatError   = 3;

        erWarningFirst  = 20;
        erDuplicate     = 20;
  {**************************************************************************************}
  { Procedure ErrorMessage                                                               }
  {--------------------------------------------------------------------------------------}
  { Gibt eine WhatError entsprechende Fehlermeldung aus. Error wird auf TRUE gesetzt.    }
  {--------------------------------------------------------------------------------------}
  { WhatError      = Nummer des Fehlers (erXXXX)                                         }
  {--------------------------------------------------------------------------------------}
  { Seiteneffekte: Error=TRUE                                                            }
  {**************************************************************************************}
  Procedure ErrorMessage
     (
     WhatError     : Integer
     );
    var Params     : Array[0..1] of Pointer;
    begin
      Params[0]:=Pointer(CurLine);
      case WhatError of
        erOpenFile,
        erReadFile       : imLogDlg.AddText(3200+WhatError,[imOptions.SymbolFile],FALSE);
        erDuplicate      : imLogDlg.AddText(3200+WhatError,[CurLine,Number1],FALSE);
        else imLogDlg.AddText(3200+WhatError,[CurLine],FALSE);
      end;
      if WhatError<erWarningFirst then Error:=TRUE;
    end;
  begin
    Error:=FALSE;
{$IFNDEF MRING}
    imLogDlg.AddText(3612,[imOptions.SymbolFile],TRUE);
{$ENDIF}
    CurLine:=0;
    imSymConvert:=New(PConvertList,Init(20,20));
{$IFNDEF MRING}
    Assign(SymFile,imOptions.SymbolFile);
{$ENDIF}
{$IFDEF MRING}
    GetPrivateProfileString('ASCII Import','SYMFILE','',@SYMName,Size,AM_Ini.IniFile^.IniFile);
    Assign(SymFile,StrPAS(@SYMName));
{$ENDIF}
    Reset(SymFile);
    if IOResult<>0 then ErrorMessage(erOpenFile)
    else begin
      while not Eof(SymFile)
          and not Error do begin
        Inc(CurLine);
        Readln(SymFile,ReadStr);
        if IOResult<>0 then ErrorMessage(erReadFile)
        else begin
          DeleteBlanks(ReadStr);
          if ReadStr<>'' then begin
            APos:=Pos(' ',ReadStr);
            if APos=0 then ErrorMessage(erFormatError)
            else begin
              Val(Copy(ReadStr,1,APos-1),Number1,ErrorPos);
              if ErrorPos=0 then Val(DeleteBlanks(Copy(ReadStr,APos+1,Length(ReadStr)-APos)),Number2,ErrorPos);
              if ErrorPos<>0 then ErrorMessage(erInvalidChar)
              else begin
                if ForExport then SConvert:=New(PSymConvert,Init(Number1,Number2))
                else SConvert:=New(PSymConvert,Init(Number2,Number1));
                if imSymConvert^.Search(PLongInt(@SConvert^.Index),APos) then begin
                  ErrorMessage(erDuplicate);
                  Dispose(SConvert,Done);
                end
                else imSymConvert^.AtInsert(APos,SConvert);
              end;
            end;
          end;
        end;
      end;
      Close(SymFile);
    end;
    if Error then imLogDlg.AddBlankLine;
    ReadSymFile:=not Error;
  end;

Function TImport.ImportASCIIFile
   : Boolean;
  Const LineEndChar     = #$0A;
        erOpenFile      = 0;                { Fehler beim �ffnen Der ASCII-Datei         }
        erLineTooLong   = 1;                { die aktuelle Zeile ist zu lang             }
        erNoDDEMemory   = 3;                { DDE-Speicher konnte nicht allokiert werden }
        erReadError     = 4;                { Fehler beim Lesen der Datei                }
        erWarningCount  = 5;                { zu  viele Warnungen aufgetreten            }
        erNotDefError   = 6;                { Spalte wurde nicht definiert->Abbruch      }
        erDemoMode      = 7;                { Applikation im Demo-Modus                  }

        erSuspendFirst  = 15;
        erBlankColumn   = 15;               { Spalte darf nicht leer bleiben             }
        erNotDefined    = 16;               { Spalte wurde nicht definiert               }
        erObjNotDefined = 17;               { Objekt bei Insert noch nicht definiert     }
        erTooManyPoints = 18;               { Polygon mit zuvielen Eckpunkten            }
        erTooLongSpline = 19;               { Spline mit zuvielen Punkten                }
        erLessLines     = 20;               { Objektdefinition hat zuwenig Zeilen        }
        erTooManyLines  = 21;               { Objektdefinition hat zuviele Zeilen        }
        erUnknownObject = 22;               { unbekannter Objekttyp                      }
        erInvalidChar   = 23;               { ung�ltiges Zeichen in Zahl                 }
        erInvScale      = 24;               { ung�ltiger Skalierungsfaktor               }
        erInvSymNumber  = 25;               { Symbolnummer nicht im Symbolfile           }
        erSymNotExists  = 26;               { Symbol existiert nicht in WinGIS           }
        erTooBigCoords  = 27;               { Koordinaten �berschreiten msProject        }

        erWarningFirst  = 30;               { ab hier folgen Warnings                    }
        erInvAlign      = 30;               { Ung�ltige Textausrichtung                  }
        erInvStyle      = 31;               { Ung�ltiger Textstil                        }
        erInvLineType   = 32;               { Ung�ltiger LinienTyp                       }
        erInvLineColor  = 33;               { Ung�ltige Linienfarbe                      }
        erInvPattern    = 34;               { Ung�ltiger Schraffurstil                   }
        erInvPatColor   = 35;               { Ung�ltige Schraffurfarbe                   }
        erInvFontName   = 36;               { Font nicht installiert                     }

  var ReadBuffer        : PChar;            { Zeiger auf den Lesebuffer                  }
      ReadCnt           : Integer;          { Anzahl gelesener Zeichen                   }
      CurrentLine       : TLine;            { aktuelle Zeile                             }
      SavedLine         : TLine;            { von SaveCurrentLine gesicherte Zeile       }
      BufferEnd         : PChar;            { Zeiger auf das letzte g�ltige Zeichen      }
                                            { im Buffer                                  }
      AsciiFile         : File;             { ASCII-Datei                                }
      OldObjectType     : LongInt;          { vorheriger Objekttyp                       }
      ObjectType        : LongInt;          { aktuelle Objekttyp                         }
      OldObjectNumber   : LongInt;          { vorherige Objektnummer                     }
      ObjectNumber      : LongInt;          { aktuelle Objektnummer                      }
      LineCount         : LongInt;          { aktuelle Zeilennummer im ASCII-File        }
      CurObject         : PView;            { Zeiger auf aktuelles Objekt                }
      CurPosition       : TDPoint;          { aktuelle Position                          }
      ObjectLayer       : PLayer;           { Layer f�r Objekte ohne Layerangabe         }
      GraphicLayer      : PLayer;           { Layer f�r Grafiktexte ohne Layerang.       }
      CurObjectLine     : LongInt;          { Anzahl Zeilen f�r aktuelles Objekt         }
      Convert           : TViewColl;        { Zuordnungsliste ASCII-Nummern->WinGIS      }
      NameFound         : Boolean;          { Name in Umwandlunstabelle gefunden?        }
      WorkStr           : String;
      Suspend           : Boolean;          { TRUE, wenn aktuelles Objekt �berlesen wird }
      CurFilePos        : LongInt;          { Fileposition des aktuellen Puffers         }
      XPosition         : Integer;          { Spaltennummer f�r X-Position               }
      YPosition         : Integer;          { Spaltennummer f�r Y-Position               }
  {**************************************************************************************}
  { Procedure ErrorMessage                                                               }
  {--------------------------------------------------------------------------------------}
  { Gibt eine WhatError entsprechende Fehlermeldung aus. Error wird auf TRUE gesetzt,    }
  { wenn WhatError<erSuspendFirst ist. Ist WhatError <erWarningFirst, so wird Suspend    }
  { TRUE gesetzt um das aktuelle Objekt nicht zu importieren.                            }
  {--------------------------------------------------------------------------------------}
  { WhatError      = Nummer des Fehlers (erXXXX)                                         }
  { AParam         = ein Parameter                                                       }
  {--------------------------------------------------------------------------------------}
  { Seiteneffekte: Error=TRUE                                                            }
  {**************************************************************************************}
  Procedure ErrorMessage
     (
     WhatError         : Integer;
     Const AParam      : String
     );
    var BStr           : String;
    begin
      case WhatError of
        erOpenFile,
        erReadError     : imLogDlg.AddText(3020+WhatError,[imOptions.ASCIIFile,AParam],FALSE);
        erTooManyPoints,
        erTooLongSpline : imLogDlg.AddText(3020+WhatError,[LineCount,MaxCollectionSize],FALSE);
        erTooBigCoords  : begin
            BStr:=FormatStr(msProject,2);
            imLogDlg.AddText(3020+WhatError,[LineCount,AParam,BStr],FALSE);
          end;
        erNotDefined,
        erNotDefError   : imLogDlg.AddText(3020+WhatError,[AParam,AParam],FALSE);
        erLineTooLong   : imLogDlg.AddText(3020+WhatError,[LineCount,maxLineLength],FALSE);
        else imLogDlg.AddText(3020+WhatError,[LineCount,AParam],FALSE);
      end;
      if WhatError<erSuspendFirst then imError:=TRUE
      else begin
        if WhatError<erWarningFirst then Suspend:=TRUE;
        Inc(imWarnings);
        if imWarnings>=maxWarnings then begin
          imLogDlg.AddBlankLine;
          ErrorMessage(erWarningCount,IntToStr(imWarnings));
        end;
      end;
    end;
  {**************************************************************************************}
  { Function StrScan                                                                     }
  {--------------------------------------------------------------------------------------}
  { Sucht beginnend mit Str das n�chste Auftreten des Zeilenende-Zeichens. Es werden     }
  { maximal MaxLen Bytes durchsucht. Ein eventuelles Stringendezeichen wird ignoriert.   }
  {--------------------------------------------------------------------------------------}
  { Str                 i = zu durchsuchender String                                     }
  { MaxLen              i = max. Anzahl Bytes die zu durchsuchen sind                    }
  {--------------------------------------------------------------------------------------}
  { Ergebnis: Zeiger auf ein gefundenes Zeilenende oder NIL, wenn keines gefunden.       }
  {**************************************************************************************}
  Function StrScan
      (
      Str          : PChar;
      MaxLen       : Integer
      )
      : PChar;
    ASM
        PUSH    EDI
        MOV     EDI,Str
        MOV     ECX,MaxLen
        XOR     EAX,EAX
        JCXZ @@1
        MOV     AL,LineEndChar
        REPNE   SCASB
        MOV     EAX,0
        JNE     @@1
        MOV     EAX,EDI
        DEC     EAX
        DEC     EAX
  @@1:  POP     EDI
  end;
  {**************************************************************************************}
  { Procedure ReadColumn                                                                 }
  {--------------------------------------------------------------------------------------}
  { Liest die angegebene Spalte aus dem Lesebuffer. Liegt die Spalte au�erhalb der       }
  { Zeile, so wird eine Leerstring zur�ckgegeben.                                        }
  {--------------------------------------------------------------------------------------}
  { Source         i = Zeile von der gelesen werden soll                                 }
  { AColumn        i = einzulesende Spalte                                               }
  { DelBlanks      i = Leerzeichen l�schen (TRUE) oder nicht (FALSE)                     }
  { BResult         o = Text in der Spalte, bzw. Leerstring wenn Spalte au�erhalb der     }
  {                    Zeile oder kein Text in der Spalte angegeben oder AColumn NIL.    }
  {**************************************************************************************}
  Procedure ReadColumn
     (
     var Source         : TLine;
     AColumn            : PColumn;
     DelBlanks          : Boolean;
     var BResult         : String
     );
    var Length          : Word;
        ColumnStart     : PChar;                      { Zeiger auf Spaltenanfang         }
        ColumnEnd       : PChar;                      { Zeiger auf Spaltenende           }
    begin
      if AColumn=NIL then BResult:=''
      else with AColumn^,Source do begin
        Length:=LineLength;
        if Length>=coStartPosition then begin         { Liegt Spalte noch in der Zeile   }
          if Length>coEndPosition then                { Begrenzung aufs Spaltenende      }
              Length:=coEndPosition;
          ColumnStart:=LineStart+coStartPosition-1;   { Spaltenanfang berechnen          }
          ColumnEnd:=LineStart+Length-1;              { Spaltenende berechnen            }
          if DelBlanks then begin
            while (ColumnStart<=ColumnEnd)            { Leerzeichen am Anfang l�schen    }
                and (ColumnStart^=' ') do
              ColumnStart:=ColumnStart+1;
            while (ColumnEnd>ColumnStart)             { Leerzeichen am Ende l�schen      }
                and (ColumnEnd^=' ') do
              ColumnEnd:=ColumnEnd-1;
          end;
          Length:=ColumnEnd-ColumnStart+1;
          BResult[0]:=Char(Length);                    { Text kopieren                    }
          Move(ColumnStart^,BResult[1],Length);
        end
        else BResult:='';
      end;
    end;
  {**************************************************************************************}
  { Function ReadField                                                                   }
  {--------------------------------------------------------------------------------------}
  { Liest die angegebene Spalte ein und wandelt den Text in den angegebenen Typ um       }
  { Tritt dabei ein Fehler auf, so wird eine Fehlermeldung ausgegeben.                   }
  {--------------------------------------------------------------------------------------}
  { Source         i = Quellbuffer                                                       }
  { AColumn        i = einzulesende Spalte                                               }
  { AOptions       i = rcXXXX                                                            }
  { AType          i = Typ der einzulesenden Zahl, rwtXXXX                               }
  { BResult         o = Inhalt der Spalte im Format AType                                 }
  {--------------------------------------------------------------------------------------}
  { Ergebnis: FALSE, wenn Fehler, TRUE sonst.                                            }
  {**************************************************************************************}
  Function ReadField
     (
     var Source    : TLine;
     AColumn       : Integer;
     AOptions      : Word;
     AType         : Integer;
     var BResult
     )
     : Boolean;
    var AText      : String;
        ErrorPos   : Integer;
        ADouble    : Double;
    begin
      ReadField:=FALSE;
      if (imColumns[AColumn]=NIL)
          and (AOptions and (rcNotDefWarning or rcNotDefError)<>0) then begin
        AText:=imTables[tnColumns]^.TypeToName(AColumn);
        if AOptions and rcNotDefError<>0 then ErrorMessage(erNotDefError,AText)
        else ErrorMessage(erNotDefined,AText)
      end
      else begin
        ErrorPos:=0;
        ReadColumn(Source,imColumns[AColumn],TRUE,AText);
        if AText='' then begin
          if AOptions and rcBlankWarning<>0 then begin
            ErrorMessage(erBlankColumn,imColumns[AColumn]^.coName);
            ErrorPos:=1;
          end
          else if AType<>rwtString then AText:='0';
        end;
        if ErrorPos=0 then begin
          case AType of
            rwtCoord,
            rwtXCoord,
            rwtYCoord : begin
                Val(AText,ADouble,ErrorPos);
                ADouble:=ADouble*100.0*imOptions.ImpOffsets.Scale;
                if AType=rwtXCoord then ADouble:=ADouble+imOptions.ImpOffsets.XOffset
                else if AType=rwtYCoord then ADouble:=ADouble+imOptions.ImpOffsets.YOffset;
                if Abs(ADouble)>msProject then ErrorMessage(erTooBigCoords,imColumns[AColumn]^.coName)
                else LongInt(BResult):=LimitToLong(ADouble);
              end;
            rwtLongInt    : Val(AText,LongInt(BResult),ErrorPos);
            rwtReal       : Val(AText,Real(BResult),ErrorPos);
            rwtString     : begin
                OemToAnsiBuff(@AText[1],@String(BResult)[1],Length(AText));
                String(BResult)[0]:=AText[0];
              end;
          end;
          if ErrorPos<>0 then ErrorMessage(erInvalidChar,imColumns[AColumn]^.coName)
          else ReadField:=TRUE;
        end;
      end;
    end;
  {**************************************************************************************}
  { Function SaveCurrentLine                                                             }
  {--------------------------------------------------------------------------------------}
  { Sichert die aktuelle Zeile des Readbuffers im Savedline-Buffer.                      }
  {--------------------------------------------------------------------------------------}
  { Seiteneffekte: SavedLine                                                             }
  {**************************************************************************************}
  Procedure SaveCurrentLine;
    begin
      SavedLine.LineLength:=CurrentLine.LineLength;
      SavedLine.LineEnd:=SavedLine.LineStart+SavedLine.LineLength;
      Move(CurrentLine.LineStart^,SavedLine.LineStart^,SavedLine.LineLength);
    end;
  {**************************************************************************************}
  { Function GetInsertLayer                                                              }
  {--------------------------------------------------------------------------------------}
  { Ermittelt den Layer, auf dem das Objekt einzuf�gen ist. Zuerst wird aus Source der   }
  { Layername gelesen. Existiert der Layer nicht, so wird er angelegt. Ist der Layer-    }
  { name leer, so wird der Objektlayer zur�ckgegeben (dieser kann NIL sein).             }
  {--------------------------------------------------------------------------------------}
  { Source         i = Puffer aus dem die Werte zu lesen sind                            }
  {--------------------------------------------------------------------------------------}
   Function GetInsertLayer
      (
      var Source        : TLine
      )
      : PLayer;
    var CurLayerName    : String;
        Layer           : PLayer;
    begin
      ReadField(Source,ctLayerName,0,rwtString,CurLayerName);
      Layer:=imProject^.Layers^.NameToLayer(CurLayerName);
      if Layer=NIL then begin
        if CurLayerName<>'' then Layer:=imProject^.Layers^.InsertLayer(CurLayerName,
            c_Black,lt_Solid,c_Black,pt_NoPattern,ilLast,DefaultSymbolFill)
        else Layer:=ObjectLayer;
      end;
      GetInsertLayer:=Layer;
    end;
  {**************************************************************************************}
  { Function InsertGraphicText                                                           }
  {--------------------------------------------------------------------------------------}
  { F�gt alle Grafiktexte in das Projekt ein, sofern es sich nicht um einen Leerstring   }
  { handelt. Ist ein TextLayer im ASCII-File angegeben, so wird der Text auf diesem      }
  { eingef�gt (Layer wird ggf. angelegt). Ist kein Layer angegeben und wurde im Dialog   }
  { ein Layer f�r Grafiktexte ausgew�hlt, so wird der Text dort eingef�gt. Die Daten     }
  { werden aus CurrentLine gelesen.                                                      }
  {**************************************************************************************}
  Procedure InsertGraphicText
     (
     var CurrentLine : TLine;
     var LayerLine   : TLine;
     SkipFirst       : Boolean
     );
    var CurColumn  : PColumn;
        Text       : String;
        TextLayer  : String;
        Layer      : PLayer;
        TextPos    : TDPoint;
        AText      : PText;
    begin
      try
        if (imColumns[ctTextLayer]<>NIL)
            or (GraphicLayer<>NIL)
            or (imOptions.GraphicLayer=-1) then begin
          TextPos.Init(CurPosition.X+imOptions.GraphicFont.Height Div 2,
              CurPosition.Y+imOptions.GraphicFont.Height Div 2);
          CurColumn:=imColumns[ctGraphic];
          if (CurColumn<>NIL)
              and SkipFirst then CurColumn:=CurColumn^.coNext;
          while CurColumn<>NIL do begin
            ReadColumn(CurrentLine,CurColumn,TRUE,Text);
            if Text<>'' then begin
              ReadField(CurrentLine,ctTextLayer,0,rwtString,TextLayer);
              if TextLayer<>'' then begin
                Layer:=imProject^.Layers^.NameToLayer(TextLayer);
                if Layer=NIL then Layer:=imProject^.Layers^.InsertLayer(TextLayer,
                    c_Black,lt_Solid,c_Black,pt_NoPattern,ilLast,DefaultSymbolFill);
              end
              else if imOptions.GraphicLayer=-1 then Layer:=GetInsertLayer(LayerLine)
              else Layer:=GraphicLayer;
            end
            else Layer:=NIL;
            if Layer<>NIL then begin
              OemToAnsiBuff(@Text[1],@Text[1],Length(Text));
              AText:=New(PText,InitCalculate(imProject^.PInfo,imOptions.GraphicFont,
                  Text,TextPos,0,0));
              if imOptions.GraphicFontVert then begin  {mf}
                AText^.Angle:=90;
                AText^.CalculateClipRect(imProject^.PInfo);
              end;
              if PLayer(imProject^.PInfo^.Objects)^.InsertObject(imProject^.PInfo,AText,FALSE) then
                  Layer^.InsertObject(imProject^.PInfo,New(PIndex,Init(AText^.Index)),FALSE);
              Dec(TextPos.Y,Trunc(imOptions.GraphicFont.Height*1.2));
            end;
            CurColumn:=CurColumn^.coNext;
          end;
        end;
      except {sl}
      end;
    end;
  {**************************************************************************************}
  { Procedure InsertDatabase                                                             }
  {--------------------------------------------------------------------------------------}
  { Schickt die Daten des aktuellen Objekts an die Datenbank, sofern DDE Ok. Der DDE-    }
  { Speicher wird nicht freigegeben.                                                     }
  {--------------------------------------------------------------------------------------}
  Procedure InsertDatabase
     (
     var Source    : TLine;
     Item          : PView;
     Layer         : PLayer
     );
    var CurColumn  : PColumn;
        AText      : String;
        ColLength  : Integer;
    begin
      if imDoDatabase
          and (DDEHandler.DDEOk)
          and (imColumns[ctDatabase]<>NIL) then begin
        DDEHandler.Reset;
        DDEHandler.Append('[IMP]');
        if DDEHandler.FLayerInfo=1 then DDEHandler.Append('['+PToStr(Layer^.Text)+']')
        else if DDEHandler.FLayerInfo=2 then begin
          Str(Layer^.Index,AText);
          DDEHandler.Append('['+AText+']');
        end;
        Str(Item^.Index:10,AText);
        DDEHandler.Append('['+AText+']');
        if (Item^.GetObjType=ot_CPoly) then begin
          Str(PCPoly(Item)^.Flaeche:15:2,AText);
          DDEHandler.Append('['+AText+']');
        end
        else DDEHandler.Append('[              0]');
        CurColumn:=imColumns[ctDataBase];
        while CurColumn<>NIL do begin
          ReadColumn(Source,CurColumn,FALSE,AText);
          OemToAnsiBuff(@AText[1],@AText[1],Length(AText));
          ColLength:=CurColumn^.coEndPosition-CurColumn^.coStartPosition+1;
          FillChar(AText[Length(AText)+1],ColLength-Length(AText),' ');
          AText[0]:=Char(ColLength);
          DDEHandler.Append('['+AText+']');
          CurColumn:=CurColumn^.coNext;
        end;
        DDEHandler.Append(#0);
        DDEHandler.Execute;
      end;
    end;
  {**************************************************************************************}
  { Procedure InsertCurObject                                                            }
  {--------------------------------------------------------------------------------------}
  { F�gt das aktuelle Objekt in die Grafik ein, f�gt die Grafiktexte ein und tr�gt die   }
  { Attribute in die Datenbank ein. Ist f�r das Objekt kein Layer angegeben (Layername   }
  { = '' und ObjectLayer=''), dann wird der Objektspeicher freigegeben.                  }
  {--------------------------------------------------------------------------------------}
  { Source         i = Puffer aus dem die Werte zu lesen sind                            }
  {**************************************************************************************}
  Function InsertCurObject
     (
     var Source         : TLine
     )
     : Boolean;
    var Layer           : PLayer;
    begin
      InsertCurObject:=FALSE;
      try
        Layer:=GetInsertLayer(Source);
        if Layer<>NIL then begin
          PLayer(imProject^.PInfo^.Objects)^.InsertObject(imProject^.PInfo,CurObject,FALSE);
          Convert.Insert(New(PSymConvert,Init(OldObjectNumber,CurObject^.Index)));
          Layer^.InsertObject(imProject^.PInfo,New(PIndex,Init(CurObject^.Index)),FALSE);
          InsertDatabase(Source,CurObject,Layer);
          InsertCurObject:=TRUE;
        end
        else Dispose(CurObject,Done);
      except
      end;
    end;
  Procedure DoPoly
     (
     Action        : TAction
     );
    begin
      if Action=doExpand then begin
        if ReadField(CurrentLine,XPosition,rcDefinedNotBlank,rwtXCoord,CurPosition.X)
            and ReadField(CurrentLine,YPosition,rcDefinedNotBlank,rwtYCoord,CurPosition.Y) then begin
          if not PPoly(CurObject)^.InsertPoint(CurPosition) then ErrorMessage(erTooManyPoints,'');
          InsertGraphicText(CurrentLine,SavedLine,FALSE);
        end;
      end
      else if Action=doInitialize then begin
        CurObject:=New(PPoly,Init);
        SaveCurrentLine;
      end
      else if Action=doFinish then begin
        if PPoly(CurObject)^.Data^.Count<2 then Dispose(CurObject,Done)
        else InsertCurObject(SavedLine);
      end;
    end;
  Procedure DoCPoly
     (
     Action        : TAction
     );
    begin
      if Action=doExpand then begin
        if ReadField(CurrentLine,XPosition,rcDefinedNotBlank,rwtXCoord,CurPosition.X)
            and ReadField(CurrentLine,YPosition,rcDefinedNotBlank,rwtYCoord,CurPosition.Y) then begin
          if not PCPoly(CurObject)^.InsertPoint(CurPosition) then ErrorMessage(erTooManyPoints,'');
          InsertGraphicText(CurrentLine,SavedLine,FALSE);
        end;
      end
      else if Action=doInitialize then begin
        CurObject:=New(PCPoly,Init);
        SaveCurrentLine;
      end
      else if Action=doFinish then begin
        if PCPoly(CurObject)^.Data^.Count<3 then Dispose(CurObject,Done)
        else begin
          PCPoly(CurObject)^.ClosePoly;
          InsertCurObject(SavedLine);
        end;
      end;
    end;
  Procedure DoPoint
     (
     Action             : TAction
     );
    begin
      if (Action=DoExpand)
          and ReadField(CurrentLine,XPosition,rcDefinedNotBlank,rwtXCoord,CurPosition.X)
          and ReadField(CurrentLine,YPosition,rcDefinedNotBlank,rwtYCoord,CurPosition.Y) then begin
        CurObject:=New(PPixel,Init(CurPosition));
        InsertCurObject(CurrentLine);
        InsertGraphicText(CurrentLine,CurrentLine,FALSE);
      end;
    end;
  Procedure DoSpline
     (
     Action        : TAction
     );
    begin
      if Action=doExpand then begin
        if ReadField(CurrentLine,XPosition,rcDefinedNotBlank,rwtXCoord,CurPosition.X)
            and ReadField(CurrentLine,YPosition,rcDefinedNotBlank,rwtYCoord,CurPosition.Y) then begin
          if not PSpline(CurObject)^.InsertPoint(CurPosition) then ErrorMessage(erTooLongSpline,'')
          else if (ObjectType=iot2Spline) then
              if not PSpline(CurObject)^.InsertPoint(CurPosition) then ErrorMessage(erTooLongSpline,'');
          InsertGraphicText(CurrentLine,SavedLine,FALSE);
        end;
      end
      else if Action=doInitialize then begin
        CurObject:=New(PSpline,Init);
        SaveCurrentLine;
      end
      else if Action=doFinish then begin
        if PSpline(CurObject)^.Data^.Count<4 then Dispose(CurObject,Done)
        else if (PSpline(CurObject)^.Data^.Count Mod 3)<>1 then Dispose(CurObject,Done)
        else InsertCurObject(SavedLine);
      end;
    end;
  Procedure DoCircle
     (
     Action             : TAction
     );
    var PrimaryAxis     : LongInt;
        SecondaryAxis   : LongInt;
        Angle           : Real;
    begin
      if Action=doExpand then begin
        if CurObjectLine=2 then begin
          if ReadField(SavedLine,XPosition,rcDefinedNotBlank,rwtXCoord,CurPosition.X)
              and ReadField(SavedLine,YPosition,rcDefinedNotBlank,rwtYCoord,CurPosition.Y)
              and ReadField(SavedLine,ctParamA,rcDefinedNotBlank,rwtCoord,PrimaryAxis)
              and ReadField(CurrentLine,ctParamA,rcDefinedNotBlank,rwtCoord,SecondaryAxis)
              and ReadField(SavedLine,ctParamC,0,rwtReal,Angle) then begin
            CurObject:=New(PEllipse,Init(CurPosition,PrimaryAxis,SecondaryAxis,Angle*Pi/180));
            InsertCurObject(SavedLine);
            InsertGraphicText(SavedLine,SavedLine,FALSE);
          end;
        end
        else if CurObjectLine>2 then ErrorMessage(erTooManyLines,'');
      end
      else if Action=doInitialize then SaveCurrentLine
      else if (Action=doFinish)
          and (CurObjectLine<2) then ErrorMessage(erLessLines,'');
    end;
  Procedure DoArc
     (
     Action             : TAction
     );
    var PrimaryAxis     : LongInt;
        SecondaryAxis   : LongInt;
        Angle           : Real;
        StartAngle      : Real;
        EndAngle        : Real;
    begin
      if Action=doExpand then begin
        if CurObjectLine=2 then begin
          if ReadField(SavedLine,XPosition,rcDefinedNotBlank,rwtXCoord,CurPosition.X)
              and ReadField(SavedLine,YPosition,rcDefinedNotBlank,rwtYCoord,CurPosition.Y)
              and ReadField(SavedLine,ctParamA,rcDefinedNotBlank,rwtCoord,PrimaryAxis)
              and ReadField(CurrentLine,ctParamA,rcDefinedNotBlank,rwtCoord,SecondaryAxis)
              and ReadField(SavedLine,ctParamB,0,rwtReal,StartAngle)
              and ReadField(CurrentLine,ctParamB,0,rwtReal,EndAngle)
              and ReadField(SavedLine,ctParamC,0,rwtReal,Angle) then begin
            CurObject:=New(PEllipseArc,Init(CurPosition,PrimaryAxis,
                SecondaryAxis,Angle*Pi/180,StartAngle*Pi/180,EndAngle*Pi/180));
            InsertCurObject(SavedLine);
            InsertGraphicText(SavedLine,SavedLine,FALSE);
          end;
        end
        else if CurObjectLine>2 then ErrorMessage(erTooManyLines,'');
      end
      else if Action=doInitialize then SaveCurrentLine
      else if (Action=doFinish)
          and (CurObjectLine<2) then ErrorMessage(erLessLines,'')
    end;
  Procedure DoText
     (
     Action             : TAction
     );
    var EndPos          : TDPoint;
        Height          : LongInt;
        AText           : String;
        Font            : String;
        Style           : String;
        Align           : String;
        AFont           : TFontData;
        AAngle          : Real;
        APos            : Integer;
        FontName        : String;
        BFont           : Integer;
        CFont           : Integer;
    begin
      if Action=doExpand then begin
        if CurObjectLine=2 then begin
          EndPos.Init(0,0);
          if ReadField(CurrentLine,XPosition,rcDefinedNotBlank,rwtXCoord,EndPos.X)
              and ReadField(CurrentLine,YPosition,rcDefinedNotBlank,rwtYCoord,EndPos.Y)
              and ReadField(CurrentLine,ctParamA,rcNotDefWarning,rwtString,Style)
              and ReadField(CurrentLine,ctParamB,rcNotDefWarning,rwtString,Align)
              and ReadField(SavedLine,XPosition,rcDefinedNotBlank,rwtXCoord,CurPosition.X)
              and ReadField(SavedLine,YPosition,rcDefinedNotBlank,rwtYCoord,CurPosition.Y)
              and ReadField(SavedLine,ctParamA,rcNotDefWarning,rwtString,Font)
              and ReadField(SavedLine,ctParamB,rcDefinedNotBlank,rwtCoord,Height)
              and ReadField(SavedLine,ctParamC,0,rwtReal,AAngle)
              and ReadField(SavedLine,ctGraphic,rcDefinedNotBlank,rwtString,AText) then begin
            AFont.Init;
            FontName:=imTables[tnFont]^.NameToTypeName(Font,NameFound);
            if NameFound then Font:=FontName;
            BFont:=imProject^.PInfo^.Fonts^.IDFromName(Font);
            if BFont=0 then begin
              ErrorMessage(erInvFontName,Font);
              BFont:=1;
            end;
            AFont.Style:=imTables[tnAlign]^.NameToType(Align,NameFound);
            if not NameFound then ErrorMessage(erInvAlign,Align);
            Style:=Style+',';
            APos:=Pos(',',Style);
            while APos<>0 do begin
              WorkStr:=Copy(Style,1,APos-1);
              AFont.Style:=AFont.Style or imTables[tnStyle]^.NameToType(WorkStr,NameFound);
              if not NameFound then ErrorMessage(erInvStyle,WorkStr);
              Delete(Style,1,APos);
              APos:=Pos(',',Style);
            end;
            AFont.Height:=Height;
            if (AFont.Style and $80<>0)
                and (Font[1]<>'@') then begin
              Font:='@'+Font;
              CFont:=imProject^.PInfo^.Fonts^.IDFromName(Font);
              if CFont=0 then ErrorMessage(erInvFontName,Font)
              else BFont:=CFont;
            end;
            AFont.Font:=BFont;
            CurObject:=New(PText,Init(AFont,AText,0));
            with PText(CurObject)^ do begin
              Font.Style:=Font.Style and $7F;
              if CurPosition.IsDiff(EndPos) then begin
                Angle:=360-Round(CurPosition.CalculateAngle(EndPos)*180/Pi);
                Width:=LimitToLong(CurPosition.Dist(EndPos));
              end
              else begin
                if AFont.Style and $80<>0 then Angle:=360-Round(AAngle)+90
                else Angle:=360-Round(AAngle);
                Width:=Round(Font.Height*imProject^.PInfo^.GetTextRatio(Font,AText));
              end;
              while Angle>360 do Angle:=Angle-360;
              while Angle<-360 do Angle:=Angle+360;
              Pos:=CurPosition;
              AAngle:=Angle*Pi/180;
              if AFont.Style and $80<>0 then Pos.Move(Round(Sin(AAngle)*Font.Height/2.0),Round(Cos(AAngle)*Font.Height/2.0))
              else Pos.Move(Round(Sin(AAngle)*Font.Height),Round(Cos(AAngle)*Font.Height));
              CalculateClipRect(imProject^.PInfo);
            end;
            InsertCurObject(SavedLine);
            InsertGraphicText(SavedLine,SavedLine,TRUE);
          end;
          EndPos.Done;
        end
        else if CurObjectLine>2 then ErrorMessage(erTooManyLines,'');
      end
      else if Action=doInitialize then SaveCurrentLine
      else if (Action=doFinish)
          and (CurObjectLine<2) then ErrorMessage(erLessLines,'');
    end;
  Procedure DoRText
     (
     Action             : TAction
     );
    var Height          : LongInt;
        AText           : String;
        AFont           : String;
        Style           : String;
        Align           : String;
        APos            : Integer;
        FontName        : String;
        BFont           : Integer;
        CFont           : TFOntData;
        Temp            : PChar;
    begin
      if Action=doExpand then with PRText(CurObject)^ do begin
        ReadField(CurrentLine,ctGraphic,rcNotDefWarning,rwtString,AText);
        if AText<>'' then begin
          APos:=Pos('|',AText);
          if APos<>0 then begin
            Align:=Copy(AText,1,APos-1)+#0;
            Style:=Copy(AText,APos+1,255)+#0;
          end
          else begin
            Align:=AText+#0;
            Style:=''+#0;
          end;
          if RText=NIL then begin
            GetMem(Temp,2+Length(Style)+1);
            StrCopy(Temp,'');
          end
          else begin
            GetMem(Temp,StrLen(RText)+2+Length(Style));
            StrCopy(Temp,RText);
            StrCat(Temp,#10#13);
          end;
          StrCat(Temp,@Style[1]);
          StrDispose(RText);
          RText:=Temp;
          LineNames^.Insert(New(PAnnotColl,Init(StrNew(@Align[1]),TRUE)));
        end;
        if CurObjectLine=2 then begin
          if ReadField(CurrentLine,ctParamA,rcNotDefWarning,rwtString,Style)
              and ReadField(CurrentLine,ctParamB,rcNotDefWarning,rwtString,Align)
              and ReadField(SavedLine,ctParamA,rcNotDefWarning,rwtString,AFont)
              and ReadField(SavedLine,ctParamB,rcDefinedNotBlank,rwtCoord,Height) then begin
            FontName:=imTables[tnFont]^.NameToTypeName(AFont,NameFound);
            if NameFound then AFont:=FontName;
            BFont:=imProject^.PInfo^.Fonts^.IDFromName(AFont);
            if BFont=0 then begin
              ErrorMessage(erInvFontName,AFont);
              BFont:=1;
            end;
            Font.Style:=imTables[tnAlign]^.NameToType(Align,NameFound);
            if not NameFound then ErrorMessage(erInvAlign,Align);
            Style:=Style+',';
            APos:=Pos(',',Style);
            while APos<>0 do begin
              WorkStr:=Copy(Style,1,APos-1);
              Font.Style:=Font.Style or imTables[tnStyle]^.NameToType(WorkStr,NameFound);
              if not NameFound then ErrorMessage(erInvStyle,WorkStr);
              Delete(Style,1,APos);
              APos:=Pos(',',Style);
            end;
            Font.Height:=Height;
            Font.Font:=BFont;
            Font.Style:=Font.Style and $7F;
          end;
        end;
      end
      else if Action=doInitialize then begin
        CFont.Init;
        CurObject:=New(PRText,Init(CFont,''));
        SaveCurrentLine;
      end
      else if (Action=doFinish)
          and ReadField(SavedLine,XPosition,rcDefinedNotBlank,rwtXCoord,CurPosition.X)
          and ReadField(SavedLine,YPosition,rcDefinedNotBlank,rwtYCoord,CurPosition.Y) then
          with PRText(CurObject)^ do begin
        TextRect.A:=CurPosition;
        CalculateClipRect(imProject^.PInfo);
        InsertCurObject(SavedLine);
      end;
    end;
  Procedure DoLayer
     (                 
     Action             : TAction
     );
    var LayerName       : String;
        LType           : String;
        LColor          : String;
        Pattern         : String;
        PatColor        : String;
        LineType        : Integer;
        LineColor       : TColorRef;
        PatternType     : Integer;
        PatternColor    : TColorRef;
        ALayer          : PLayer;
        NameFound       : Boolean;
    begin
      if Action=doExpand then begin
        if CurObjectLine=2 then begin
          if ReadField(SavedLine,ctParamA,rcNotDefWarning,rwtString,LType)
              and ReadField(SavedLine,ctParamB,rcNotDefWarning,rwtString,LColor)
              and ReadField(CurrentLine,ctParamA,rcNotDefWarning,rwtString,Pattern)
              and ReadField(CurrentLine,ctParamB,rcNotDefWarning,rwtString,PatColor)
              and ReadField(SavedLine,ctLayerName,rcDefinedNotBlank,rwtString,LayerName) then begin
            LineType:=imTables[tnLine]^.NameToType(LType,NameFound);
            if not NameFound then ErrorMessage(erInvLineType,LType);
            if not TextToRGBColor(LColor,LineColor) then begin
              LineColor:=RGBColors[imTables[tnColor]^.NameToType(LColor,NameFound)];
              if not NameFound then ErrorMessage(erInvLineColor,LColor);
            end;
            PatternType:=imTables[tnPattern]^.NameToType(Pattern,NameFound);
            if not NameFound then ErrorMessage(erInvPattern,Pattern);
            if not TextToRGBColor(PatColor,PatternColor) then begin
              PatternColor:=imTables[tnColor]^.NameToType(PatColor,NameFound);
              if not NameFound then ErrorMessage(erInvPatColor,PatColor);
            end;
            ALayer:=imProject^.Layers^.NameToLayer(LayerName);
            if ALayer=NIL then imProject^.Layers^.InsertLayer(LayerName,LineColor,
                LineType,PatternColor,PatternType,ilLast,DefaultSymbolFill)
            else if imOptions.NewLayerStyle = 1 then ALayer^.SetStyle(LineColor,LineType,
                PatternColor,clNone,PatternType,NIL,DefaultSymbolFill);
          end;
        end
        else if CurObjectLine>2 then ErrorMessage(erTooManyLines,'');
      end
      else if Action=doInitialize then SaveCurrentLine
      else if (Action=doFinish)
          and (CurObjectLine<2) then ErrorMessage(erLessLines,'');
    end;
  Procedure DoAttribute
     (
     Action             : TAction
     );
    begin
      if (Action=doExpand)
          and ReadField(CurrentLine,XPosition,rcDefinedNotBlank,rwtXCoord,CurPosition.X)
          and ReadField(CurrentLine,YPosition,rcDefinedNotBlank,rwtYCoord,CurPosition.Y) then
        InsertGraphicText(CurrentLine,CurrentLine,FALSE);
    end;
  Procedure DoImage
     (
     Action             : TAction
     );
    var Width           : LongInt;
        Height          : LongInt;
        Attributes      : String;
        Transp          : Byte;
        Error           : Integer;
        APath           : String;
    begin
      if Action=doExpand then begin
        if CurObjectLine=2 then begin
          if ReadField(SavedLine,XPosition,rcDefinedNotBlank,rwtXCoord,CurPosition.X)
              and ReadField(SavedLine,YPosition,rcDefinedNotBlank,rwtYCoord,CurPosition.Y)
              and ReadField(SavedLine,ctParamA,rcDefinedNotBlank,rwtCoord,Width)
              and ReadField(SavedLine,ctParamB,rcDefinedNotBlank,rwtCoord,Height)
              and ReadField(SavedLine,ctParamC,rcDefinedNotBlank,rwtString,Attributes)
              and ReadField(CurrentLine,ctImagePath,rcDefinedNotBlank,rwtString,APath) then begin
            CurObject:=New(PImage,Init(APath,ExtractRelativePath(imProject^.FName,APath),
                so_ShowBMP, imProject^.PInfo^.BitmapSettings));
            CurObject^.ClipRect.A.Init(CurPosition.X,CurPosition.Y);
            CurObject^.ClipRect.B.Init(CurPosition.X+Width,CurPosition.Y+Height);
            PImage(CurObject)^.ScaledClipRect.Assign(CurPosition.X,CurPosition.Y,CurPosition.X+Width,CurPosition.Y+Height);
            if Attributes[1] = '1' then PImage(CurObject)^.ShowOpt := PImage(CurObject)^.ShowOpt OR so_ShowBMP
            else PImage(CurObject)^.ShowOpt := PImage(CurObject)^.ShowOpt AND NOT so_ShowBMP;
            Delete(Attributes,1,1);
            if Attributes[1] = '1' then PImage(CurObject)^.Options:=PImage(CurObject)^.Options or opt_HideFrame
            else PImage(CurObject)^.Options:=PImage(CurObject)^.Options and not opt_HideFrame;
            Delete(Attributes,1,1);
            if Attributes[1] = '2' then PImage(CurObject)^.TransparencyType:=tt_White
            else PImage(CurObject)^.TransparencyType:=tt_Black;
            Delete(Attributes,1,1);
            Val(Attributes,Transp,Error);
            if (Error = 0) and (Transp >= 0) and (Transp <= 100) then PImage(CurObject)^.Transparency:=Transp
            else PImage(CurObject)^.Transparency:=0;
            TMDIChild(imOwner).FirstDraw:=TRUE;
            InsertCurObject(SavedLine);
            InsertGraphicText(SavedLine,SavedLine,FALSE);
          end;
        end
        else if CurObjectLine>2 then ErrorMessage(erTooManyLines,'');
      end
      else if Action=doInitialize then SaveCurrentLine
      else if (Action=doFinish)
          and (CurObjectLine<2) then ErrorMessage(erLessLines,'')
    end;
  Procedure DoInsert
     (
     Action             : TAction
     );
    var ANumber         : LongInt;
        AConvert        : PSymConvert;
        Layer           : PLayer;
        AIndex          : PIndex;
        BIndex          : LongInt;
    begin
      if Action=doExpand then begin
        if ReadField(CurrentLine,ctParamA,rcDefinedNotBlank,rwtLongInt,ANumber) then begin
          AIndex:=New(PIndex,Init(ANumber));
          if Convert.Search(AIndex,BIndex) then begin
            Layer:=GetInsertLayer(CurrentLine);
            AConvert:=Pointer(Convert.At(BIndex));
            AIndex^.Index:=AConvert^.WinGISNumber;
            if (Layer<>NIL) and (Layer^.HasObject(AIndex)=NIL) then begin
              Layer^.InsertObject(imProject^.PInfo,AIndex,FALSE);
              if DDEHandler.FLayerInfo<>0 then InsertDatabase(CurrentLine,Layer^.IndexObject(imProject^.PInfo,AIndex),Layer);
            end
            else Dispose(AIndex,Done);
          end
          else begin
            ErrorMessage(erObjNotDefined,IntToStr(ANumber));
            Dispose(AIndex,Done);
          end;
        end;
      end;
    end;
  Procedure DoSymbol
     (
     Action             : TAction
     );
    var SymNumber       : LongInt;
        ScaleFactor     : Real;
        Rotation        : Real;
        Convert         : PSymConvert;
        AIndex          : Integer;
        BIndex          : TIndex;
    begin
      if (Action=DoExpand)
          and ReadField(CurrentLine,XPosition,rcDefinedNotBlank,rwtXCoord,CurPosition.X)
          and ReadField(CurrentLine,YPosition,rcDefinedNotBlank,rwtYCoord,CurPosition.Y)
          and ReadField(CurrentLine,ctParamA,rcDefinedNotBlank,rwtLongint,SymNumber)
          and ReadField(CurrentLine,ctParamB,rcNotDefWarning,rwtReal,ScaleFactor)
          and ReadField(CurrentLine,ctParamC,rcNotDefWarning,rwtReal,Rotation) then begin
        if ScaleFactor=0 then ScaleFactor:=imScaleFactor;
        if ScaleFactor=0 then ErrorMessage(erInvScale,'')
        else begin
          if not imSymConvert^.Search(PLongInt(@SymNumber),AIndex) then ErrorMessage(erInvSymNumber,IntToStr(SymNumber))
          else begin
            Convert:=imSymConvert^.At(AIndex);
            BIndex.Init(Convert^.WinGISNumber+mi_Group);
            if PLayer(imProject^.PInfo^.Symbols)^.IndexObject(imProject^.PInfo,@BIndex)=NIL then
                ErrorMessage(erSymNotExists,inttostr(Convert^.WinGISNumber))
            else begin
              CurObject:=New(PSymbol,Init(imProject^.PInfo,CurPosition,BIndex.Index,ScaleFactor,Rotation*Pi/180));
              InsertCurObject(CurrentLine);  {1907F}
              InsertGraphicText(CurrentLine,CurrentLine,FALSE);
            end;
            BIndex.Done;
          end;
        end;
      end;
    end;
  Procedure DoObjects
     (
     ObjectType    : Integer;
     Action        : TAction
     );
    begin
      case ObjectType of
        iotPoint     : DoPoint(Action);
        iotPoly      : DoPoly(Action);
        iotCPoly     : DoCPoly(Action);
        iotCircle    : DoCircle(Action);
        iotArc       : DoArc(Action);
        iotText      : DoText(Action);
        iotRText     : DoRText(Action);
        iot2Spline,
        iot3Spline   : DoSpline(Action);
        iotAttribute : DoAttribute(Action);
        iotLayer     : DoLayer(Action);
        iotImage     : DoImage(Action);
        iotInsert    : DoInsert(Action);
        iotComment   : ;
        iotSymbol    : DoSymbol(Action);
        else ErrorMessage(erUnknownObject,IntToStr(ObjectType));
      end;
    end;
  begin
    imError:=FALSE;
    imLogDlg.AddText(3602,[imOptions.ASCIIFile],TRUE);
    Assign(AsciiFile,imOptions.ASCIIFile);
    Reset(AsciiFile,1);
    if IOResult<>0 then ErrorMessage(erOpenFile,'')
    else begin
      if DDEHandler.DDEOk
          and not DDEHandler.GetGlobal(maxDDEMemorySize) then
        ErrorMessage(erNoDDEMemory,'');
      if not imError then with CurrentLine do begin
        SendImportDBDefinition;
        if imOptions.ObjectLayer=0 then ObjectLayer:=NIL
        else ObjectLayer:=imProject^.Layers^.IndexToLayer(imOptions.ObjectLayer);
        if imOptions.GraphicLayer<=0 then GraphicLayer:=NIL
        else GraphicLayer:=imProject^.Layers^.IndexToLayer(imOptions.GraphicLayer);
        GetMem(ReadBuffer,maxReadBufferLen);
        LineStart:=ReadBuffer;
        GetMem(SavedLine.LineStart,maxLineLength);
        BufferEnd:=ReadBuffer;
        OldObjectNumber:=-1;
        ObjectNumber:=-1;
        OldObjectType:=-1;
        ObjectType:=-1;
        LineCount:=0;
        CurObject:=NIL;
        CurPosition.Init(0,0);
        CurObjectLine:=0;
        Convert.Init;
        Suspend:=FALSE;
        if imProject^.PInfo^.CoordinateSystem.CoordinateType=ctGeodatic then begin
          XPosition:=ctYPosition;
          YPosition:=ctXPosition;
        end
        else begin
          XPosition:=ctXPosition;
          YPosition:=ctYPosition;
        end;
        repeat
          LineEnd:=StrScan(LineStart,BufferEnd-LineStart);
          if LineEnd=NIL then begin
            CurFilePos:=FilePos(AsciiFile);
            Move(LineStart^,ReadBuffer^,BufferEnd-LineStart);
            BufferEnd:=@ReadBuffer[BufferEnd-LineStart];
            BlockRead(AsciiFile,BufferEnd^,ReadBuffer-BufferEnd+maxReadBufferLen,ReadCnt);
            if IOResult<>0 then ErrorMessage(erReadError,'');
            BufferEnd:=BufferEnd+ReadCnt;
            LineStart:=ReadBuffer;
          end
          else if LineEnd-LineStart>maxLineLength then ErrorMessage(erLineTooLong,'')
          else begin
            imLogDlg.SetPercent(100.0*(LineStart-ReadBuffer+CurFilePos)/FileSize(AsciiFile));
            Inc(LineCount);
            LineLength:=LineEnd-LineStart;
            if ReadField(CurrentLine,ctObjectType,rcNotDefError,rwtLongInt,ObjectType)
                and ReadField(CurrentLine,ctObjectNumber,rcNotDefError,rwtLongInt,ObjectNumber) then begin
              if (OldObjectType<>ObjectType)
                  or (OldObjectNumber<>ObjectNumber) then begin
                if (OldObjectType<>-1)
                    and not Suspend then DoObjects(OldObjectType,doFinish);
                if Suspend then begin
                  imLogDlg.AddText(3578,[OldObjectNumber],FALSE);
                  Suspend:=FALSE;
                end;
                CurObject:=NIL;
                CurObjectLine:=0;
                DoObjects(ObjectType,doInitialize);
                OldObjectType:=ObjectType;
                OldObjectNumber:=ObjectNumber;
              end;
              Inc(CurObjectLine);
              if not Suspend then DoObjects(ObjectType,doExpand);
            end;
            LineStart:=LineEnd+2;
          end;
          imError:=imError or imLogDlg.CheckAskForAbort(3562);
        until (ReadCnt=0) or imError;
        if not imError then begin
          DoObjects(ObjectType,doFinish);
          imLogDlg.SetPercent(100);
        end;
        Convert.Done;
        FreeMem(ReadBuffer,maxReadBufferLen);
        FreeMem(SavedLine.LineStart,maxLineLength);
        CurPosition.Done;
        if DDEHandler.DDEOk then begin
          DDEHandler.FreeGlobal;
          DDEHandler.SendString('[END][]');
          imDBEndSend:=TRUE;
        end;
      end;
      Close(AsciiFile);
    end;
    if imError then imLogDlg.AddBlankLine;
    ImportASCIIFile:=not imError;
  end;

Destructor TImport.Done;
  begin
    Dispose(imDDELines,Done);
    inherited Done;
  end;

{****************************************************************************************}
{ Procedure TImport.FreeTables                                                           }
{----------------------------------------------------------------------------------------}
{ Gibt den von den Tabellen und den Spaltendefinitionen belegten Speicher frei.          }
{****************************************************************************************}
Procedure TImport.FreeTables;
  var Cnt          : Integer;
  begin
    for Cnt:=ctXPosition to ctLast do
        if imColumns[Cnt]<>NIL then begin
      Dispose(imColumns[Cnt],Done);
      imColumns[Cnt]:=NIL;
    end;
    for Cnt:=0 to maxTableCount-1 do begin
      if imTables[Cnt]<>NIL then Dispose(imTables[Cnt],Done);
      imTables[Cnt]:=NIL;
    end;
    if imSymConvert<>NIL then begin
      Dispose(imSymConvert,Done);
      imSymConvert:=NIL;
    end;
  end;

{****************************************************************************************}
{ Function TImport.DBExportData                                                          }
{----------------------------------------------------------------------------------------}
{ Wird aufgerufen, um die von der Datenbank gesendeten und in einer StringCollection     }
{ gepufferten Daten f�r den Export vorzubereiten.                                        }
{ Format von Item: [EXP][Daten][Daten][...                                               }
{----------------------------------------------------------------------------------------}
{ AData            i = Daten zum Objekt                                                  }
{****************************************************************************************}
Procedure TImport.DBExportData;
  Procedure DoAll
     (
     Item          : PChar
     ); Far;
    var AItem      : PDBListItem;
    begin
      if (imDBData<>NIL)
          and (imLogDlg<>NIL) then begin
        AItem:=New(PDBListItem,Init(Item));
        if AItem<>NIL then imDBData^.Insert(AItem);
        Inc(imExportedCnt);
        imLogDlg.SetPercent(100.0*imExportedCnt/imAllObjects);
      end;
      StrDispose(Item);
    end;
  begin
    imDDELines^.ForEach(@DoAll);
    imDDELines^.DeleteAll;
  end;

{****************************************************************************************}
{ Function TImport.InsertDDEData                                                         }
{----------------------------------------------------------------------------------------}
{ Wird aufgerufen, wenn die Datenbank die Daten f�r ein zu exportierendes Objekt mittels }
{ DDE an den Grafikeditor schickt. Die Daten werden in einer StringCollection gepuffert. }
{****************************************************************************************}
Procedure TImport.InsertDDEData
   (
   AData           : PChar
   );
  begin
    imDDELines^.Insert(StrNew(AData));
  end;

{****************************************************************************************}
{ Function TImport.DBError                                                               }
{----------------------------------------------------------------------------------------}
{ Wird aufgerufen, wenn beim Export definierte Datenbankspalten in der Datenbank nicht   }
{ existieren. Format von ErrorStr:[ERR][Anz][Name][Name][...                             }
{----------------------------------------------------------------------------------------}
{ ErrorStr         i = Zeiger auf den DDE-Speicher der Datenbank                         }
{****************************************************************************************}
Procedure TImport.DBError;
  var Cnt          : Integer;
      AStr         : String;
      ErrorText    : String;
      Source       : PChar;
      Error        : Integer;
      ErrorStr     : Array[0..255] of Char;
      BStr         : Array[0..255] of Char;
  begin
    {*************************************************************************************}
    { Fehler im DDE-String von der Datenbank (wingisdb.exe vom 17.3.97)!                  }
    { Das Kommando soll richtig lauten: [ERR][Anzahl][Spaltenname][Spaltenname]...        }
    { Das Kommando lautet:              [ERR][Anzahl]Spaltenname,Spaltenname...           }
    { Wenn dieser Fehler bereinigt ist, m�ssen die anschlie�enden Zeilen wieder gegen die }
    { darunter ausgeklammerten ersetzt werden!                                            }
    {*************************************************************************************}
    if imLogDlg<>NIL then begin
      Source:=ReadDDEItem(imErrString,AStr);                  { [ERR] �berlesen             }
      Source:=ReadDDEItem(Source,AStr);                       { Anzahl �berlesen            }
      Val(AStr,Cnt,Error);
      if Error = 0 then begin
        StrCopy(ErrorStr,Source);
        StrCat(ErrorStr,']');
        Source:=ErrorStr;
        ErrorText:='';
        while Source <> NIL do begin
          ReadDDEText(Source,DDEHandler.FDDESepSign,BStr);
          ErrorText:=ErrorText+', '+StrPas(BStr);
        end;
        Delete(ErrorText,1,2);
        if Cnt=1 then begin
          imLogDlg.AddText(3583,[ErrorText],FALSE);
        end
        else if Cnt>1 then begin
          imLogDlg.AddText(3582,[' '],FALSE);
          imLogDlg.AddString('  '+ErrorText,FALSE);
        end;
        if imInProgress=ipASCIIImport then begin
          if MsgBox(imOwner.Handle,11264,mb_IconQuestion or mb_YesNo,'')=id_Yes then
            imDoDatabase:=FALSE
          else imError:=TRUE;
        end
        else if Cnt>0 then imError:=TRUE;
      end;
      ErrorText:='';
    end;
    StrDispose(imErrString);
  end;

{****************************************************************************************}
{ Function TImport.DBEnd                                                                 }
{----------------------------------------------------------------------------------------}
{ Wird aufgerufen, wenn die Datenbank einen [END][]-Befehl schickt.                      }
{****************************************************************************************}
Procedure TImport.DBEnd;
  begin
    imDBEnd:=TRUE;
  end;

{****************************************************************************************}
{ Function TImport.ImportDialog                                                          }
{----------------------------------------------------------------------------------------}
{ �ffnet den Import-Dialog. Die Daten f�r den Dialog werden aus imOptions genommen.      }
{----------------------------------------------------------------------------------------}
{ Ergebnis: TRUE, wenn alle Eingaben OK, FALSE sonst                                     }
{****************************************************************************************}
Function TImport.ImportDialog
   : Boolean;
  begin
    ImportDialog:=ExecDialog(TImportDlg.
        Init(imOwner,'ASCIIIMPORT',@imOptions,imProject^.Layers,imProject^.PInfo))=idOK;
  end;

{****************************************************************************************}
{ Procedure TImport.ImportASCII                                                          }
{----------------------------------------------------------------------------------------}
{ Liest die Tabelle aus dem INI-File, �ffnet den Importdialog, liest das ADF-File und    }
{ importiert das ASCII-File. Anschlie�end werden die Projektgrenzen neu berechnet. Der   }
{ Speicher f�r die Tabellen wird freigegeben.                                            }
{****************************************************************************************}
Procedure TImport.ImportASCII;
  var DoneOK       : Boolean;
      AText        : Array[0..50] of Char;
  begin
    if ImportDialog then begin
      imWarnings:=0;
      imLogDlg:=TLogDlg.Init(imOwner,GetLangPChar(3573,AText,SizeOf(AText)));
      imInProgress:=ipASCIIImport;
      imDoDatabase:=TRUE;
      DoneOK:=ReadTables
          and ReadAdfFile
          and ReadSymFile(FALSE)
          and ImportASCIIFile;
      if not DoneOK then imLogDlg.AddText(3601,[NIL],TRUE)
      else if imWarnings=0 then imLogDlg.AddText(3603,[NIL],TRUE)
      else begin
        imLogDlg.AddBlankLine;
        if imWarnings=1 then imLogDlg.AddText(3609,[imWarnings],TRUE)
        else imLogDlg.AddText(3608,[imWarnings],TRUE);
      end;
      ShowWindow(imLogDlg.GetItemHandle(idSave),sw_Show);
      SetWindowWord(imLogDlg.GetItemHandle(id_Cancel),gww_ID,id_OK);
      SetWindowText(imLogDlg.GetItemHandle(id_OK),GetLangPChar(0,AText,SizeOf(AText)));
      imLogDlg.ClearAbort;
      repeat until imLogDlg.HandleMessages;
      imLogDlg.Free;
      imLogDlg:=NIL;
      imProject^.CalculateSize;
      imProject^.Layers^.DetermineTopLayer;
      imProject^.SetModified;
      FreeTables;
      imInProgress:=ipNone;
      DDEHandler.SendString('[IDR][]');
    end;
  end;

{****************************************************************************************}
{ Procedure TImport.ImportASCIIFromDB                                                    }
{----------------------------------------------------------------------------------------}
{ Liest die Tabelle aus dem INI-File, wertet den DDE-String aus, liest das ADF-File und  }
{ importiert das ASCII-File. Anschlie�end werden die Projektgrenzen neu berechnet. Der   }
{ Speicher f�r die Tabellen wird freigegeben.                                            }
{****************************************************************************************}
Procedure TImport.ImportASCIIFromDB;
  var DoneOK       : Boolean;
      TempStr      : Array[0..255] of Char;
      AllDatas     : Boolean;
      ASCIIFName   : Array[0..fsPathName] of Char;
      ADFFName     : Array[0..fsPathName] of Char;
      SymbolFName  : Array[0..fsPathName] of Char;
      AFont        : TFontData;
      LayerName    : Array[0..255] of Char;
      FStyle       : Integer;
      FHeight      : Real;
      Error        : Integer;
      TextLayer    : LongInt;
      ObjLayer     : LongInt;
      TempOffset   : TOffsetData;
      TempPointer  : PChar;
      CreateOnLayer: PLayer;
      IsOK         : Boolean;
      UseExistingL : Boolean;
      DDEStrEnd    : Boolean;
      Msg          : TMsg;
      DoAbort      : Boolean;
      DDELine      : PChar;
      BFont        : PFontDes;
  Function SearchLName
     (
     Item          : PDGRLayerData
     )
     : Boolean; Far;
    begin
      SearchLName:=StrIComp(Item^.LayerName,LayerName) = 0;
    end;
  Procedure ExpandFileName
     (
     AText           : PChar;
     FileKind        : Byte
     );
    var Dir          : Array[0..fsDirectory] of Char;
        DName        : Array[0..fsFileName] of Char;
        Ext          : Array[0..fsExtension] of Char;
    begin
      FileSplit(AText,Dir,DName,Ext);
      if (StrComp(DName,'')<>0)
          and (StrComp(Ext,'')=0) then begin
        case FileKind of
          1 : StrCat(AText,fexASCIIFile);
          2 : StrCat(AText,fexADFFile);
          3 : StrCat(AText,fexSymbolFile);
        end;
      end;
    end;
  Function CheckFile
     (
     AText           : PChar;
     FileKind        : Byte
     )
     : Boolean;
    var BResult       : Boolean;
    begin
      BResult:=FALSE;
      if not CheckFileExist(AText) then MsgBox(imOwner.Handle,3519,mb_IconExclamation or mb_OK,StrPas(AText))
      else BResult:=TRUE;
      CheckFile:=BResult;
    end;
  begin
    imLogDlg:=TLogDlg.Init(imOwner,GetLangPChar(3573,TempStr,SizeOf(TempStr)));
    imDBEndSend:=FALSE;
    imInProgress:=ipDBASCIIImport;
    repeat
      DDELine:=imDDELines^.At(0);
      imDDELines^.AtDelete(0);
      DoAbort:=FALSE;
      AllDatas:=TRUE;
      TempPointer:=NextDDEPart(NextDDEPart(DDELine));
      DDEStrEnd:=ReadDDEText(TempPointer,DDEHandler.FDDESepSign,ASCIIFName);
      ExpandFileName(ASCIIFName,1);
      if StrComp(ASCIIFName,'')=0 then begin
        MsgBox(imOwner.Handle,3534,mb_IconExclamation or mb_OK,'');
        AllDatas:=FALSE;
      end;
      DDEStrEnd:=ReadDDEText(TempPointer,DDEHandler.FDDESepSign,ADFFName);
      ExpandFileName(ADFFName,2);
      if StrComp(ADFFName,'')=0 then begin
        MsgBox(imOwner.Handle,3535,mb_IconExclamation or mb_OK,'');
        AllDatas:=FALSE;
      end;
      DDEStrEnd:=ReadDDEText(TempPointer,DDEHandler.FDDESepSign,SymbolFName);
      ExpandFileName(SymbolFName,3);
      if StrComp(SymbolFName,'')=0 then begin
        MsgBox(imOwner.Handle,3536,mb_IconExclamation or mb_OK,'');
        AllDatas:=FALSE;
      end;
      AFont.Init;                                                                            { FontName }
      AFont:=imOptions.GraphicFont;
      if not DDEStrEnd then begin
        DDEStrEnd:=ReadDDEText(TempPointer,DDEHandler.FDDESepSign,TempStr);
        if StrComp(TempStr,'') <> 0 then begin
          with AFont do begin
            Font:=imProject^.PInfo^.Fonts^.IDFromName(StrPas(TempStr));
            if Font = 0 then begin
              MsgBox(imOwner.Handle,3537,mb_IconExclamation or mb_OK,StrPas(TempStr));
              Font:=1;
            end;
          end;
        end;
      end;
      if not DDEStrEnd then begin                                                     { FontStyle }
        DDEStrEnd:=ReadDDEText(TempPointer,DDEHandler.FDDESepSign,TempStr);
        Val(TempStr,FStyle,Error);
        if Error = 0 then AFont.Style:=FStyle
        else AFont.Style:=0;
      end;
      if not DDEStrEnd then begin                                                     { FontHeight }
        DDEStrEnd:=ReadDDEText(TempPointer,DDEHandler.FDDESepSign,TempStr);
        Val(TempStr,FHeight,Error);
        if Error = 0 then AFont.Height:=Round(FHeight*100);
      end;
      if not DDEStrEnd then begin                                                     { Graphiktexte ohne Layerangabe }
        DDEStrEnd:=ReadDDEText(TempPointer,DDEHandler.FDDESepSign,TempStr);
        if StrComp(TempStr,'') <> 0 then begin
          if StrComp(TempStr,'-1') = 0 then TextLayer:=-1
          else begin
            UseExistingL:=FALSE;
            CreateOnLayer:=imProject^.Layers^.NameToLayer(StrPas(TempStr));
            if CreateOnLayer <> NIL then begin
              if CreateOnLayer^.GetState(sf_Fixed) then begin
                if MsgBox(imOwner.Handle,3540,mb_IconQuestion or mb_YesNo,StrPas(TempStr)) = id_Yes then begin
                  if InputCheckPassword(imOwner.Parent) then CreateOnLayer^.SetState(sf_Fixed,FALSE)
                  else begin
                    MsgBox(imOwner.Handle,3541,mb_IconExclamation or mb_OK,'');
                    CreateOnLayer:=NIL;
                    UseExistingL:=TRUE;
                  end;
                end
                else begin
                  CreateOnLayer:=NIL;
                  UseExistingL:=TRUE;
                end;
              end;
              if (CreateOnLayer <> NIL) and (CreateOnLayer^.GetState(sf_LayerOff)) then begin
                if MsgBox(imOwner.Handle,3544,mb_IconQuestion or mb_YesNo,StrPas(TempStr)) = id_Yes then
                  CreateOnLayer^.SetState(sf_LayerOff,FALSE)
                else begin
                  CreateOnLayer:=NIL;
                  UseExistingL:=TRUE;
                end;
              end;
            end;
            if CreateOnLayer = NIL then begin
              if not UseExistingL then begin
                CreateOnLayer:=NewLayerDialog(imProject,GetLangText(10154),StrPas(TempStr));
                if CreateOnLayer<>NIL then TextLayer:=CreateOnLayer^.Index
                else UseExistingL:=TRUE;
              end;
              if UseExistingL then repeat
                IsOK:=FALSE;
                CreateOnLayer:=SelectLayerDialog(imProject,[sloNewLayer]);
                if CreateOnLayer<>NIL then begin
                  TextLayer:=CreateOnLayer^.Index;
                  IsOK:=TRUE;
                end
                else begin
                  if MsgBox(imOwner.Handle,3538,mb_IconExclamation or mb_RetryCancel,'') = idCancel then begin
                    TextLayer:=0;
                    IsOK:=TRUE;
                  end;
                end;
              until IsOK;
            end
            else TextLayer:=CreateOnLayer^.Index;
          end;
        end
        else TextLayer:=0;
      end
      else TextLayer:=0;
      if not DDEStrEnd then begin                                                     { Objekte ohne Layerangabe }
        DDEStrEnd:=ReadDDEText(TempPointer,DDEHandler.FDDESepSign,TempStr);
        if StrComp(TempStr,'') <> 0 then begin
          UseExistingL:=FALSE;
          CreateOnLayer:=imProject^.Layers^.NameToLayer(StrPas(TempStr));
          if CreateOnLayer <> NIL then begin
            if CreateOnLayer^.GetState(sf_Fixed) then begin
              if MsgBox(imOwner.Handle,3540,mb_IconQuestion or mb_YesNo,StrPas(TempStr)) = id_Yes then begin
                if InputCheckPassword(imOwner.Parent) then CreateOnLayer^.SetState(sf_Fixed,FALSE)
                else begin
                  MsgBox(imOwner.Handle,3541,mb_IconExclamation or mb_OK,'');
                  CreateOnLayer:=NIL;
                  UseExistingL:=TRUE;
                end;
              end
              else begin
                CreateOnLayer:=NIL;
                UseExistingL:=TRUE;
              end;
            end;
            if (CreateOnLayer <> NIL) and (CreateOnLayer^.GetState(sf_LayerOff)) then begin
              if MsgBox(imOwner.Handle,3544,mb_IconQuestion or mb_YesNo,StrPas(TempStr)) = id_Yes then
                CreateOnLayer^.SetState(sf_LayerOff,FALSE)
              else begin
                CreateOnLayer:=NIL;
                UseExistingL:=TRUE;
              end;
            end;
          end;
          if CreateOnLayer = NIL then begin
            if not UseExistingL then begin
              CreateOnLayer:=NewLayerDialog(imProject,GetLangText(10154),StrPas(TempStr));
              if CreateOnLayer<>NIL then ObjLayer:=CreateOnLayer^.Index
              else UseExistingL:=TRUE;
            end;
            if UseExistingL then repeat
              IsOK:=FALSE;
              CreateOnLayer:=SelectLayerDialog(imProject,[sloNewLayer]);
              if CreateOnLayer<>NIL then begin
                ObjLayer:=CreateOnLayer^.Index;
                IsOK:=TRUE;
              end
              else begin
                if MsgBox(imOwner.Handle,3539,mb_IconExclamation or mb_RetryCancel,'') = idCancel then begin
                  ObjLayer:=0;
                  IsOK:=TRUE;
                end;
              end;
            until IsOK;
          end
          else ObjLayer:=CreateOnLayer^.Index;
        end
        else ObjLayer:=0;
      end
      else ObjLayer:=0;
      if not DDEStrEnd then begin                                                     { X-Offset }
        DDEStrEnd:=ReadDDEText(TempPointer,DDEHandler.FDDESepSign,TempStr);
        Val(TempStr,FHeight,Error);
        if Error = 0 then TempOffset.XOffset:=Round(FHeight*100)
        else TempOffset.XOffset:=0;
      end
      else TempOffset.XOffset:=0;
      if not DDEStrEnd then begin                                                     { Y-Offset }
        DDEStrEnd:=ReadDDEText(TempPointer,DDEHandler.FDDESepSign,TempStr);
        Val(TempStr,FHeight,Error);
        if Error = 0 then TempOffset.YOffset:=Round(FHeight*100)
        else TempOffset.YOffset:=0;
      end
      else TempOffset.YOffset:=0;
      if not DDEStrEnd then begin                                                     { Skalierung }
        DDEStrEnd:=ReadDDEText(TempPointer,DDEHandler.FDDESepSign,TempStr);
        Val(TempStr,FHeight,Error);
        if Error = 0 then TempOffset.Scale:=FHeight
        else TempOffset.Scale:=1;
      end
      else TempOffset.Scale:=1;
      if AllDatas then begin
        if CheckFile(ASCIIFName,1)
            and CheckFile(ADFFName,2)
            and CheckFile(SymbolFName,3) then with imOptions do begin
          ASCIIFile:=StrPas(ASCIIFName);
          ADFFile:=StrPas(ADFFName);
          SymbolFile:=StrPas(SymbolFName);
          GraphicFont:=AFont;
          BFont:=imProject^.PInfo^.Fonts^.GetFont(AFont.Font);
          if (BFont<>NIL) and BFont^.IsVertical then GraphicFontVert:=TRUE {mf}
          else GraphicFontVert:=FALSE;
          GraphicLayer:=TextLayer;
          ObjectLayer:=ObjLayer;
          ImpOffsets:=TempOffset;
          imWarnings:=0;
          imDoDatabase:=TRUE;
          DoneOK:=ReadTables
              and ReadAdfFile
              and ReadSymFile(FALSE)
              and ImportASCIIFile;
          if not DoneOK then imLogDlg.AddText(3601,[NIL],TRUE)
          else if imWarnings=0 then imLogDlg.AddText(3603,[NIL],TRUE)
          else begin
            imLogDlg.AddBlankLine;
            if imWarnings=1 then imLogDlg.AddText(3609,[imWarnings],TRUE)
            else imLogDlg.AddText(3608,[imWarnings],TRUE);
          end;
          ShowWindow(imLogDlg.GetItemHandle(idSave),sw_Show);
          SetWindowWord(imLogDlg.GetItemHandle(id_Cancel),gww_ID,id_OK);
          SetWindowText(imLogDlg.GetItemHandle(id_OK),GetLangPChar(0,TempStr,SizeOf(TempStr)));
          FreeTables;
          imProject^.CalculateSize;
          imProject^.Layers^.DetermineTopLayer;
          imProject^.SetModified;
        end
        else AFont.Done;
      end
      else AFont.Done;
      StrDispose(DDELine);
      if not imDBEndSend then DDEHandler.SendString('[END][]');
      repeat
        if PeekMessage(Msg,0,0,0,pm_Remove) then begin
          if (Msg.Message=wm_KeyUp)
              and (Msg.wParam=vk_Escape) then DoAbort:=TRUE;
          if imLogDlg.DoAbort then DoAbort:=TRUE;
          if not IsDialogMessage(imLogDlg.Handle,Msg) then begin
            TranslateMessage(Msg);
            DispatchMessage(Msg);
          end;
        end;
      until DoAbort
          or (imDDELines^.Count<>0);
    until imDDELines^.Count=0;
    imInProgress:=ipNone;
    imLogDlg.Free;
    imLogDlg:=NIL;
  end;

{****************************************************************************************}
{ Function TImport.ExportDialog                                                          }
{----------------------------------------------------------------------------------------}
{ �ffnet den Export-Dialog. Die Daten f�r den Dialog werden aus imOptions genommen.      }
{----------------------------------------------------------------------------------------}
{ Ergebnis: TRUE, wenn alle Eingaben OK, FALSE sonst                                     }
{****************************************************************************************}
Function TImport.ExportDialog
   : Boolean;
  begin
    ExportDialog:=ExecDialog(TExportDlg.
        Init(imOwner,'ASCIIEXPORT',@imOptions,imProject^.Layers,
            imProject^.PInfo))=idOK;
  end;

{****************************************************************************************}
{ Procedure TImport.ExportASCII                                                          }
{----------------------------------------------------------------------------------------}
{ Liest die Tabelle aus dem INI-File, �ffnet den Exportdialog, liest das ADF-File und    }
{ exportiert das ASCII-File. Der Speicher f�r die Tabellen wird freigegeben.             }
{****************************************************************************************}
Procedure TImport.ExportASCII;
  var DoneOK       : Boolean;
      AText        : Array[0..50] of Char;
  begin
    if ExportDialog then begin
      imWarnings:=0;
      imLogDlg:=TLogDlg.Init(imOwner,GetLangPChar(3580,AText,SizeOf(AText)));
      imInProgress:=ipASCIIExport;
      imDoDatabase:=TRUE;
      DoneOK:=ReadTables
          and ReadAdfFile
          and ReadSymFile(TRUE)
          and ExportASCIIFile;
      if not DoneOK then imLogDlg.AddText(3607,[NIL],TRUE)
      else if imWarnings=0 then imLogDlg.AddText(3605,[NIL],TRUE)
      else begin
        imLogDlg.AddBlankLine;
        if imWarnings=1 then imLogDlg.AddText(3611,[imWarnings],TRUE)
        else imLogDlg.AddText(3610,[imWarnings],TRUE);
      end;
      ShowWindow(imLogDlg.GetItemHandle(idSave),sw_Show);
      SetWindowWord(imLogDlg.GetItemHandle(id_Cancel),gww_ID,id_OK);
      SetWindowText(imLogDlg.GetItemHandle(id_OK),GetLangPChar(0,AText,SizeOf(AText)));
      imLogDlg.ClearAbort;
      repeat until imLogDlg.HandleMessages;
      imLogDlg.Free;
      imLogDlg:=NIL;
      FreeTables;
      imInProgress:=ipNone;
    end;
  end;

{****************************************************************************************}
{ Procedure TImport.SendImportDBDefinition                                               }
{----------------------------------------------------------------------------------------}
{ Schickt die Definition der Datenbankspalten mittels DDE-Befehl and die Datenbank. Der  }
{ DDE-Speicher mu� bereits reserviert sein. Die Formatbeschreibung siehe AMDDE.DOC.      }
{****************************************************************************************}
Procedure TImport.SendImportDBDefinition;
  var CurColumn         : PColumn;
      ColumnCount       : Integer;
      WorkStr           : String;
      ColumnLen         : Integer;
      ColumnType        : Char;
  begin
    if DDEHandler.DDEOk then begin
      CurColumn:=imColumns[ctDatabase];
      ColumnCount:=0;
      DDEHandler.Reset;
      DDEHandler.Append('[DEF][   ]');
      while CurColumn<>NIL do with CurColumn^ do begin
        case coDatabaseType of
          dbtNumeric : begin
              ColumnType:='N';
              ColumnLen:=coDatabaseLength;
            end;
          dbtChar    : begin
              ColumnType:='C';
              ColumnLen:=coDatabaseLength;
            end;
          else begin
            ColumnType:=' ';
            ColumnLen:=coEndPosition-coStartPosition+1;
          end;
        end;
        WorkStr:=Format('[%-15s][%3d]',[coName,ColumnLen]);
  {      WorkStr:=Format('[%-15s][%c][%3d]',[coName,ColumnType,ColumnLen]);}
        DDEHandler.Append(WorkStr);
        CurColumn:=CurColumn^.coNext;
        Inc(ColumnCount);
      end;
      if ColumnCount<>0 then begin
        Str(ColumnCount:3,WorkStr);
        DDEHandler.Write(6,WorkStr);
        DDEHandler.Append(#0);
        DDEHandler.Execute;
      end;
    end;
  end;

{****************************************************************************************}
{ Procedure TImport.SendExportDBDefinition                                               }
{----------------------------------------------------------------------------------------}
{ Schickt die Definition der Datenbankspalten mittels DDE-Befehl and die Datenbank.      }
{****************************************************************************************}
Procedure TImport.SendExportDBDefinition;
  var CurColumn         : PColumn;
      ColumnCount       : Integer;
      WorkStr           : String;
  begin
    CurColumn:=imColumns[ctDatabase];
    ColumnCount:=0;
    DDEHandler.GetGlobal(maxDDEMemorySize);
    DDEHandler.Append('[COL][   ]');
    while CurColumn<>NIL do with CurColumn^ do begin
      DDEHandler.Append('['+coName+']');
      CurColumn:=CurColumn^.coNext;
      Inc(ColumnCount);
    end;
    if ColumnCount<>0 then begin
      Str(ColumnCount:3,WorkStr);
      DDEHandler.Write(6,WorkStr);
      DDEHandler.Append(#0);
      DDEHandler.Execute;
    end;
    DDEHandler.FreeGlobal;
  end;

{****************************************************************************************}
{ Procedure TImport.ExportASCIIFile                                                      }
{----------------------------------------------------------------------------------------}
{ Exportiert das Projekt in ein ASCII-File.                                              }
{****************************************************************************************}
Function TImport.ExportASCIIFile
   : Boolean;
  var AsciiFile         : File;             { ASCII-Datei                                }
      WriteBuffer       : PChar;            { Puffer f�r Dateiausgabe                    }
      LineStart         : PChar;            { Anfang der aktuellen Zeile                 }
      CurObjectNumber   : LongInt;          { Nummer des aktuellen Objekts               }
      LineEnd           : Integer;          { L�nge der aktuellen Zeile                  }
      Cnt               : LongInt;
      CurLayerName      : String;           { aktueller Layername                        }
      ExportedList      : TViewColl;        { Liste der berits exportierten Objekte      }
      ToExport          : TViewColl;        { Liste mit zu exportierenden Objekten       }
      Marker            : LongInt;
      InsertedList      : TIndexColl;
      ExpItem           : PIndex;
      AIndex            : LongInt;
      BIndex            : Integer;
      XPosition         : Integer;
      YPosition         : Integer;
      TempStr           : String;
  Const erOpenFile           = 0;           { Datei konnte nicht erzeugt werden          }
        erColumnNotDefined   = 1;           { Spalte wurde nicht definiert               }
        erDiskFull           = 2;           { Diskette ist voll                          }
        erWriteError         = 3;           { Fehler beim Schreiben                      }
        erWarningCount       = 4;           { zu viele Warnings aufgetreten              }
        erWarningFirst       = 10;
        erColumnTooSmall     = 10;          { Spalte ist zu schmal                       }
        erInvSymNumber       = 11;          { Symbolnummer nicht in der Zuordnungsdatei  }
  {**************************************************************************************}
  { Procedure ErrorMessage                                                               }
  {--------------------------------------------------------------------------------------}
  { Gibt eine WhatError entsprechende Fehlermeldung aus. Error wird auf TRUE gesetzt.    }
  {--------------------------------------------------------------------------------------}
  { WhatError      = Nummer des Fehlers (erXXXX)                                         }
  {--------------------------------------------------------------------------------------}
  { Seiteneffekte: Error=TRUE                                                            }
  {**************************************************************************************}
  Procedure ErrorMessage
     (
     WhatError          : Integer;
     Param              : Array of Const
     );
    begin
      imLogDlg.AddText(3100+WhatError,Param,FALSE);
      if WhatError<erWarningFirst then imError:=TRUE
      else begin
        Inc(imWarnings);
        if imWarnings>=maxWarnings then begin
          imLogDlg.AddBlankLine;
          ErrorMessage(erWarningCount,[imWarnings]);
        end;
      end;
    end;
  {**************************************************************************************}
  { Procedure FlushBuffer                                                                }
  {--------------------------------------------------------------------------------------}
  { Schreibt den Schreibpuffer bis zum Anfang der aktuellen Zeile auf die Platte. Die    }
  { aktuelle Zeile wird an den Anfang des Puffers kopiert. Der restliche Puffer wird mit }
  { Leerzeichen aufgef�llt.                                                              }
  {**************************************************************************************}
  Procedure FlushBuffer;
    var Written         : Integer;
        WriteCount      : Word;
    begin
      if not imError then begin
        WriteCount:=LineStart-WriteBuffer;
        BlockWrite(AsciiFile,WriteBuffer^,WriteCount,Written);
        if IOResult<>0 then ErrorMessage(erWriteError,[imOptions.ASCIIFile])
        else if WriteCount<>Written then ErrorMessage(erDiskFull,['']);
        if LineEnd=0 then FillChar(WriteBuffer^,maxWriteBufferLen,' ')
        else begin
          Move(LineStart^,WriteBuffer^,LineEnd);
          FillChar(WriteBuffer[LineEnd+1],maxWriteBufferLen-LineEnd-1,' ');
        end;
        LineStart:=WriteBuffer;
      end;
    end;
  {**************************************************************************************}
  { Procedure WriteColumn                                                                }
  {--------------------------------------------------------------------------------------}
  { Schreibt eine Spalte in den Schribebuffer. Ist die Spalte zu schmal, so wird eine    }
  { Fehlermeldung ausgegeben und der Text gek�rzt. Ist der Buffer voll, so wird der      }
  { Buffer auf die Platte geschrieben und neu initialisiert.                             }
  {--------------------------------------------------------------------------------------}
  { AColumn             i = Nummer der Spalte (ctXXXX)                                   }
  { AText               i = auszugebender Text                                           }
  { Left                i = TRUE->linksb�ndig, FALSE->rechtsb�ndig                       }
  {**************************************************************************************}
  Procedure WriteColumn
     (
     AColumn            : PColumn;
     Const AText        : String;
     Left               : Boolean
     );
    var WriteLength     : Integer;
    begin
      with AColumn^ do begin
        if Length(AText)>coEndPosition-coStartPosition+1 then ErrorMessage(erColumnTooSmall,[coName]);
        WriteLength:=Min(Length(AText),coEndPosition-coStartPosition+1);
        if Left then begin
          if LineStart-WriteBuffer+coStartPosition+WriteLength>maxWriteBufferLen then
              FlushBuffer;
          Move(AText[1],LineStart[coStartPosition-1],WriteLength);
          LineEnd:=Max(LineEnd,coStartPosition+WriteLength);
        end
        else begin
          if LineStart-WriteBuffer+coEndPosition>maxWriteBufferLen then
              FlushBuffer;
          Move(AText[1],LineStart[coEndPosition-WriteLength],WriteLength);
          LineEnd:=Max(LineEnd,coEndPosition);
        end;
      end;
    end;
  {**************************************************************************************}
  { Procedure WriteString                                                                }
  {--------------------------------------------------------------------------------------}
  { Schreibt einen String in eine Spalte. Der Text wird in das OEM-Format konvertiert.   }
  {--------------------------------------------------------------------------------------}
  { AColumn             i = Nummer der Spalte (ctXXXX)                                   }
  { Text                i = auszugebender String                                         }
  {**************************************************************************************}
  Procedure WriteString
     (
     AColumn       : Integer;
     Const Text    : String
     );
    var AText      : String;
    begin
      AnsiToOemBuff(@Text[1],@AText[1],Length(Text));
      AText[0]:=Text[0];
      if imColumns[AColumn]=NIL then begin
        AText:=imTables[tnColumns]^.TypeToName(AColumn);
        ErrorMessage(erColumnNotDefined,[AText])
      end
      else WriteColumn(imColumns[AColumn],AText,TRUE);
    end;
  Procedure WriteField
     (
     AColumn       : Integer;
     AType         : Integer;
     var Number
     );
    var AText      : String;
        ANumber    : Double;
    begin
      case AType of
        rwtCoord,
        rwtXCoord,
        rwtYCoord  : begin
            ANumber:=LongInt(Number);
            if AType=rwtXCoord then ANumber:=ANumber+imOptions.ExpOffsets.XOffset
            else if AType=rwtYCoord then ANumber:=ANumber+imOptions.ExpOffsets.YOffset;
            ANumber:=ANumber*imOptions.ExpOffsets.Scale/100;
            Str(ANumber:0:2,AText);
          end;
        rwtLongInt : Str(LongInt(Number),AText);
        rwtReal    : Str(Real(Number):0:2,AText);
        else AText:='';
      end;
      if imColumns[AColumn]=NIL then begin
        AText:=imTables[tnColumns]^.TypeToName(AColumn);
        ErrorMessage(erColumnNotDefined,[AText])
      end
      else WriteColumn(imColumns[AColumn],AText,FALSE);
    end;
  {**************************************************************************************}
  { Procedure WriteObjectTypeAndNumber                                                   }
  {--------------------------------------------------------------------------------------}
  { Schreibt den Objekttyp und die aktuelle Objektnummer in den Schreibbuffer.           }
  {**************************************************************************************}
  Procedure WriteObjectTypeAndNumber
     (
     AType         : LongInt;
     AId           : LongInt
     );
    begin
      if (AId<>0)
          and (imColumns[ctID]<>NIL) then WriteField(ctID,rwtLongInt,AId);
      WriteField(ctObjectType,rwtLongInt,AType);
      WriteField(ctObjectNumber,rwtLongInt,CurObjectNumber);
    end;
  Procedure WriteDatabase
     (
     AItem         : PIndex
     );
    var AColumn    : PColumn;
        AIndex     : Integer;
        Item       : PDBListItem;
        Source     : PChar;
        AText      : String;
    begin
      if imDBData<>NIL then begin
        if imDBData^.Search(AItem,AIndex) then begin
          Item:=imDBData^.At(AIndex);
          Source:=Item^.DBData;
          AColumn:=imColumns[ctDatabase];
          while (AColumn<>NIL)
              and (Source<>NIL) do begin
            Source:=ReadDDEItem(Source,AText);
            WriteColumn(AColumn,AText,TRUE);
            AColumn:=AColumn^.coNext;
          end;
        end;
      end;
    end;
  {**************************************************************************************}
  { Procedure NewLine                                                                    }
  {--------------------------------------------------------------------------------------}
  { Schreibt das Zeilenende in den Schreibbuffer. LineStart und LineEnd werden neu       }
  { berechnet.                                                                           }
  {**************************************************************************************}
  Procedure NewLine;
    begin
      if LineStart-WriteBuffer+LineEnd+2>maxWriteBufferLen then FlushBuffer;
      LineStart[LineEnd]:=#$0D;
      LineStart[LineEnd+1]:=#$0A;
      LineStart:=@LineStart[LineEnd+2];
      LineEnd:=0;
    end;
  {**************************************************************************************}
  { es folgen die Exportroutinen f�r die einzelnen Objekte ...                           }
  {**************************************************************************************}
  Procedure DoPolyAndSpline
     (
     Item          : PPoly;
     ObjType       : Integer
     );
    var Cnt          : Integer;
        IslandCount  : Integer;
        IslandInfo   : PLongArray;
        IslandInfoIdx: Integer;
        IsIsland     : boolean;
        NextIslandIdx: integer;
        PointCount   : integer;
        SkipIndex    : Integer;
        FirstIsland  : boolean;
    begin
       with Item^ do
       begin
          PointCount:=0; SkipIndex:=0; FirstIsland:=TRUE; IsIsland:=FALSE;
          if ObjType = iotCPoly then
          begin
             IslandCount:=PCPoly(Item)^.IslandCount;
             if IslandCount > 1 then
             begin
                Showmessage('its an island');
                IslandInfo:=PCPoly(Item)^.IslandInfo;
                IsIsland:=true; IslandInfoIdx:=0;
                NextIslandIdx:=IslandInfo^[IslandInfoIdx];
             end;
          end;

          Inc(CurObjectNumber);
          WriteString(ctLayerName,CurLayerName);
          WriteDatabase(Item);
          WriteObjectTypeAndNumber(ObjType,Item^.Index);
          WriteField(XPosition,rwtXCoord,PDPoint(Data^.At(0))^.X);
          WriteField(YPosition,rwtYCoord,PDPoint(Data^.At(0))^.Y);
          PointCount:=PointCount+1;
          NewLine;

          for Cnt:=1 to Data^.Count-1 do
          begin
             if SkipIndex = 0 then PointCount:=PointCount + 1;
             if (IsIsland) and (PointCount = NextIslandIdx) then // next island area will be created
             begin
                IslandInfoIdx:=IslandInfoIdx+1;
                if FirstIsland then SkipIndex:=1 else SkipIndex:=2;
                FirstIsland:=FALSE;

                if IslandInfoIdx >= IslandCount then
                begin
                   NextIslandIdx:=0;
                   IslandInfoIdx:=IslandCount;
                end
                else
                begin
                   NextIslandIdx:=IslandInfo^[IslandInfoIdx];
                   Inc(CurObjectNumber);
                   WriteString(ctLayerName,CurLayerName);
                   WriteDatabase(Item);
                   WriteObjectTypeAndNumber(ObjType,Item^.Index);
                   WriteField(XPosition,rwtXCoord,PDPoint(Data^.At(Cnt+SkipIndex))^.X);
                   WriteField(YPosition,rwtYCoord,PDPoint(Data^.At(Cnt+SkipIndex))^.Y);
                   PointCount:=1;
                   NewLine;
                end;
             end
             else
             begin
                if (SkipIndex=0) then
                begin
                   if (IsIsland) and (Cnt = Data^.Count-2) then //avoid exporting last connectionline
                      continue;
                   WriteObjectTypeAndNumber(ObjType,0);
                   WriteField(XPosition,rwtXCoord,PDPoint(Data^.At(Cnt))^.X);
                   WriteField(YPosition,rwtYCoord,PDPoint(Data^.At(Cnt))^.Y);
                   NewLine;
                end
                else
                   SkipIndex:=SkipIndex-1;
             end;
          end;
       end;
    end;
  Procedure DoPoint
     (
     Item          : PPixel
     );
    begin
      Inc(CurObjectNumber);
      WriteObjectTypeAndNumber(iotPoint,Item^.Index);
      WriteDatabase(Item);
      WriteString(ctLayerName,CurLayerName);
      WriteField(XPosition,rwtXCoord,Item^.Position.X);
      WriteField(YPosition,rwtYCoord,Item^.Position.Y);
      NewLine;
    end;
  Procedure DoCircle
     (
     Item          : PEllipse
     );
    var Angle      : Real;
    begin
      Inc(CurObjectNumber);
      WriteObjectTypeAndNumber(iotCircle,Item^.Index);
      WriteDatabase(Item);
      WriteString(ctLayerName,CurLayerName);
      WriteField(XPosition,rwtXCoord,Item^.Position.X);
      WriteField(YPosition,rwtYCoord,Item^.Position.Y);
      WriteField(ctParamA,rwtCoord,Item^.PrimaryAxis);
      Angle:=Item^.Angle*180/Pi;
      WriteField(ctParamC,rwtReal,Angle);
      NewLine;
      WriteObjectTypeAndNumber(iotCircle,0);
      WriteField(XPosition,rwtXCoord,Item^.Position.X);
      WriteField(YPosition,rwtYCoord,Item^.Position.Y);
      WriteField(ctParamA,rwtCoord,Item^.SecondaryAxis);
      NewLine;
    end;
  Procedure DoArc
     (
     Item          : PEllipseArc
     );
    var Angle      : Real;
    begin
      Inc(CurObjectNumber);
      WriteObjectTypeAndNumber(iotArc,Item^.Index);
      WriteDatabase(Item);
      WriteString(ctLayerName,CurLayerName);
      WriteField(XPosition,rwtXCoord,Item^.Position.X);
      WriteField(YPosition,rwtYCoord,Item^.Position.Y);
      WriteField(ctParamA,rwtCoord,Item^.PrimaryAxis);
      Angle:=Item^.Angle*180/Pi;
      WriteField(ctParamC,rwtReal,Angle);
      Angle:=Item^.BeginAngle*180/Pi;
      WriteField(ctParamB,rwtReal,Angle);
      NewLine;
      WriteObjectTypeAndNumber(iotArc,0);
      WriteField(XPosition,rwtXCoord,Item^.Position.X);
      WriteField(YPosition,rwtYCoord,Item^.Position.Y);
      WriteField(ctParamA,rwtCoord,Item^.SecondaryAxis);
      Angle:=Item^.EndAngle*180/Pi;
      WriteField(ctParamB,rwtReal,Angle);
      NewLine;
    end;
  Procedure DoText
     (
     Item          : PText
     );
    var Style      : String;
        FontDes    : PFontDes;
        Border     : PCPoly;
    begin
      with Item^ do begin
        Inc(CurObjectNumber);
        WriteObjectTypeAndNumber(iotText,Item^.Index);
        WriteDatabase(Item);
        WriteString(ctLayerName,CurLayerName);
        Border:=GetBorder;
        WriteField(XPosition,rwtXCoord,PDPoint(Border^.Data^.At(3))^.X);
        WriteField(YPosition,rwtYCoord,PDPoint(Border^.Data^.At(3))^.Y);
        FontDes:=imProject^.PInfo^.Fonts^.GetFont(Font.Font);
        if FontDes<>NIL then begin                     {mf}
          Style:=PToStr(FontDes^.Name);
          if Style[1]='@' then Delete(Style,1,1);
          Style:=imTables[tnFont]^.TypeNameToName(Style)
        end
        else Style:=imTables[tnFont]^.TypeNameToName('');
        if Style<>'' then WriteString(ctParamA,Style)
        else WriteString(ctParamA,PToStr(FontDes^.Name));
        WriteField(ctParamB,rwtCoord,Font.Height);
        WriteString(ctGraphic,PToStr(Text));
        NewLine;
        WriteObjectTypeAndNumber(iotText,0);
        WriteField(XPosition,rwtXCoord,PDPoint(Border^.Data^.At(2))^.X);
        WriteField(YPosition,rwtYCoord,PDPoint(Border^.Data^.At(2))^.Y);
        if Font.Style and ts_Italic<>0 then Style:=imTables[tnStyle]^.TypeToName(ts_Italic)
        else Style:='';
        if Font.Style and ts_Bold<>0 then Style:=Style+','+imTables[tnStyle]^.TypeToName(ts_Bold);
        if Font.Style and ts_Underl<>0 then Style:=Style+','+imTables[tnStyle]^.TypeToName(ts_Underl);
        if Font.Style and ts_FixHeight<>0 then Style:=Style+','+imTables[tnStyle]^.TypeToName(ts_FixHeight);
        if Font.Style and ts_Transparent<>0 then Style:=Style+','+imTables[tnStyle]^.TypeToName(ts_Transparent);
        if (FontDes<>NIL) and FontDes^.IsVertical then Style:=Style+','+imTables[tnStyle]^.TypeToName(128); {mf}
        if Style='' then Style:=imTables[tnStyle]^.TypeToName(0)
        else if Style[1]=',' then Delete(Style,1,1);
        WriteString(ctParamA,Style);
        WriteString(ctParamB,imTables[tnAlign]^.TypeToName(Item^.Font.Style and ts_Align));
        NewLine;
        Dispose(Border,Done);
      end;
    end;
  Procedure DoRText
     (
     Item          : PRText
     );
    var Style      : String;
        FontDes    : PFontDes;
        Border     : PCPoly;
        Cnt        : Integer;
        CurText    : PChar;
    Function ColumnText
       (
       AIndex      : Integer
       )
       : String;
      var CurEnd   : PChar;
      begin
        if (AIndex>=Item^.LineNames^.Count)
            or (CurText=NIL) then Result:=''
        else begin
          CurEnd:=StrPos(CurText,#10);
          if CurEnd=NIL then CurEnd:=StrEnd(CurText);
          Result:=StrPas(PAnnotColl(Item^.LineNames^.At(AIndex))^.Name)+'|'
              +Copy(StrPas(CurText),1,CurEnd-CurText);
          if CurEnd^=#0 then CurText:=NIL
          else CurText:=CurEnd+2;
        end;
      end;
    begin
      with Item^ do begin
        CurText:=RText;
        Inc(CurObjectNumber);
        WriteObjectTypeAndNumber(iotRText,Item^.Index);
        WriteDatabase(Item);
        WriteString(ctLayerName,CurLayerName);
        Border:=GetBorder(imProject^.PInfo);
        WriteField(XPosition,rwtXCoord,PDPoint(Border^.Data^.At(0))^.X);
        WriteField(YPosition,rwtYCoord,PDPoint(Border^.Data^.At(0))^.Y);
        FontDes:=imProject^.PInfo^.Fonts^.GetFont(Font.Font);
        if FontDes<>NIL then begin                     {mf}
          Style:=PToStr(FontDes^.Name);
          if Style[1]='@' then Delete(Style,1,1);
          Style:=imTables[tnFont]^.TypeNameToName(Style)
        end
        else Style:=imTables[tnFont]^.TypeNameToName('');
        if Style<>'' then WriteString(ctParamA,Style)
        else WriteString(ctParamA,PToStr(FontDes^.Name));
        WriteField(ctParamB,rwtCoord,Font.Height);
        WriteString(ctGraphic,ColumnText(0));
        NewLine;
        WriteObjectTypeAndNumber(iotRText,0);
        if Font.Style and ts_Italic<>0 then Style:=imTables[tnStyle]^.TypeToName(ts_Italic)
        else Style:='';
        if Font.Style and ts_Bold<>0 then Style:=Style+','+imTables[tnStyle]^.TypeToName(ts_Bold);
        if Font.Style and ts_Underl<>0 then Style:=Style+','+imTables[tnStyle]^.TypeToName(ts_Underl);
        if Font.Style and ts_FixHeight<>0 then Style:=Style+','+imTables[tnStyle]^.TypeToName(ts_FixHeight);
        if Font.Style and ts_Transparent<>0 then Style:=Style+','+imTables[tnStyle]^.TypeToName(ts_Transparent);
        if Style='' then Style:=imTables[tnStyle]^.TypeToName(0)
        else if Style[1]=',' then Delete(Style,1,1);
        WriteString(ctParamA,Style);
        WriteString(ctParamB,imTables[tnAlign]^.TypeToName(Item^.Font.Style and ts_Align));
        WriteString(ctGraphic,ColumnText(1));
        NewLine;
        for Cnt:=2 to LineNames^.Count-1 do begin
          WriteObjectTypeAndNumber(iotRText,Item^.Index);
          WriteString(ctGraphic,ColumnText(Cnt));
          NewLine;
        end;
        Dispose(Border,Done);
      end;
    end;
  Procedure DoImage
     (
     Item          : PImage
     );
    var Param      : LongInt;
        FileName   : String;
        TempStr    : String;
        TmpStr     : String;
    begin
      Inc(CurObjectNumber);
      WriteObjectTypeAndNumber(iotImage,Item^.Index);
      WriteDatabase(Item);
      WriteString(ctLayerName,CurLayerName);
      WriteField(XPosition,rwtXCoord,Item^.ClipRect.A.X);
      WriteField(YPosition,rwtYCoord,Item^.ClipRect.A.Y);
      Param:=LimitToLong(Item^.ClipRect.XSize);
      WriteField(ctParamA,rwtCoord,Param);
      Param:=LimitToLong(Item^.ClipRect.YSize);
      WriteField(ctParamB,rwtCoord,Param);
      if(Item^.ShowOpt AND so_ShowBMP) <> 0 then TempStr:='1'
      else TempStr:='0';
      if Item^.Options AND opt_HideFrame = 1 then TempStr:=TempStr+'1'
      else TempStr:=TempStr+'0';
      if Item^.TransparencyType = 1 then TempStr:=TempStr+'1'
      else TempStr:=TempStr+'2';
      Str(Item^.Transparency:3,TmpStr);
      TempStr:=TempStr+TmpStr;
      WriteString(ctParamC,TempStr);
      NewLine;
      WriteObjectTypeAndNumber(iotImage,0);
      FileName:=PToStr(Item^.FileName);
      WriteString(ctImagePath,FileName);
      NewLine;
    end;
  Procedure DoInsert
     (
     Item          : PSymConvert
     );
    begin
      Inc(CurObjectNumber);
      WriteObjectTypeAndNumber(iotInsert,Item^.Index);
      WriteString(ctLayerName,CurLayerName);
      WriteField(ctParamA,rwtLongInt,Item^.WinGISNumber);
      NewLine;
    end;
  Procedure DoSymbol
     (
     Item          : PSymbol
     );
    var AIndex     : Integer;
        Convert    : PSymConvert;
        Number     : LongInt;
        AAngle     : Real;
    begin
      Number:=Item^.SymIndex-mi_Group;
      if not imSymConvert^.Search(PLongInt(@Number),AIndex) then
        ErrorMessage(erInvSymNumber,[Number])
      else begin
        Inc(CurObjectNumber);
        WriteObjectTypeAndNumber(iotSymbol,Item^.Index);
        WriteDatabase(Item);
        WriteString(ctLayerName,CurLayerName);
        WriteField(XPosition,rwtXCoord,Item^.Position.X);
        WriteField(YPosition,rwtYCoord,Item^.Position.Y);
        Convert:=imSymConvert^.At(AIndex);
        WriteField(ctParamA,rwtLongInt,Convert^.WinGisNumber);
        WriteField(ctParamB,rwtReal,Item^.Size);
        AAngle:=360+Item^.Angle*180/Pi;             {1907F}
        if AAngle>=360.0 then AAngle:=AAngle-360.0;  {1907F}
        WriteField(ctParamC,rwtReal,AAngle);
        NewLine;
      end;
    end;
  Procedure DoLayer
     (
     Layer         : PLayer
     );
    begin
      CurLayerName:=PToStr(Layer^.Text);
      if imOptions.ExportWhat<>exSelected then begin
        Inc(CurObjectNumber);
        WriteObjectTypeAndNumber(iotLayer,0);
        WriteString(ctLayerName,CurLayerName);
        with Layer^.LineStyle do begin
          WriteString(ctParamA,imTables[tnLine]^.TypeToName(Style));
          WriteString(ctParamB,RGBColorToText(Color));
        end;
        NewLine;
        WriteObjectTypeAndNumber(iotLayer,0);
        with Layer^.FillStyle do begin
          WriteString(ctParamA,imTables[tnPattern]^.TypeToName(Pattern));
          WriteString(ctParamB,RGBColorToText(ForeColor));
        end;
        NewLine;
      end;
    end;
  {**************************************************************************************}
  { Function DoObjects                                                                   }
  {--------------------------------------------------------------------------------------}
  { Ermittelt, ob das angegebene Objekt bereits exportiert wurde und schreibt in diesem  }
  { Fall ein Object-Insert in das ASCII-File. Andernfalls wird die den Objekt            }
  { entsprechende Exportroutine aufgerufen und das Objekt in die Liste der bereits       }
  { exporierten Objekte eingef�gt. Dann wird die Prozentanzeige aktualisiert und der     }
  { Eventhandler des Abbruchdialogs aufgerufen.                                          }
  {--------------------------------------------------------------------------------------}
  { Ergebnis: TRUE, wenn der Export abzubrechen ist (ein Fehler beim Export aufgetreten  }
  { ist, oder der Anwender den Abbruch w�nscht.                                          }
  {**************************************************************************************}
  Function DoObjects
     (
     Item        : PIndex
     )
     : Boolean; Far;
    var AObject  : Pointer;
        AIndex   : LongInt;
        Exported : Boolean;
    begin
      if Item^.GetObjType=ot_Layer then DoLayer(Pointer(Item))
      else if ExportedList.Search(Item,AIndex) then DoInsert(Pointer(ExportedList.At(AIndex)))
      else begin
        AObject:=imProject^.Layers^.IndexObject(imProject^.PInfo,Item);
        Exported:=TRUE;
        case PIndex(AObject)^.GetObjType of
          ot_Pixel  : DoPoint(AObject);
          ot_Poly   : DoPolyAndSpline(AObject,iotPoly);
          ot_CPoly  : DoPolyAndSpline(AObject,iotCPoly);
          ot_Spline : DoPolyAndSpline(AObject,iot3Spline);
          ot_Text   : DoText(AObject);
          ot_RText  : DoRText(AObject);
          ot_Circle : DoCircle(AObject);
          ot_Arc    : DoArc(AObject);
          ot_Image  : DoImage(AObject);
          ot_Symbol : DoSymbol(AObject);
          else Exported:=FALSE;
        end;
        if Exported then ExportedList.AtInsert(AIndex,New(PSymConvert,Init(Item^.Index,CurObjectNumber)));
      end;
      Inc(imExportedCnt);
      imLogDlg.SetPercent(100.0*imExportedCnt/imAllObjects);
      imError:=imError or imLogDlg.CheckAskForAbort(3579);
      DoObjects:=imError;
    end;
  Function DoCollectObjects
     (
     Item        : PIndex
     )
     : Boolean;
    begin
      ToExport.AtInsert(ToExport.GetCount,Item);
      DoCollectObjects:=FALSE;
    end;
  Function DoCollectLayer
     (
     Layer         : PLayer
     )
     : Boolean;
    var Cnt        : LongInt;
    begin
      with Layer^ do if (PToStr(Text)<>'') then begin
        ToExport.AtInsert(ToExport.GetCount,Layer);
        for Cnt:=0 to Layer^.Data^.GetCount-1 do
          if DoCollectObjects(Layer^.Data^.At(Cnt)) then Break;
      end;
      DoCollectLayer:=FALSE;
    end;
  Procedure DoTheObjects
     (
     WaitForDB          : Boolean
     );
    var Cnt1            : LongInt;
    begin
      if WaitForDB then while not imDBEnd
          and not imError do
        imError:=imError or imLogDlg.CheckAskForAbort(3579);
      DBExportData;
      imExportedCnt:=Marker+Cnt+1;
      if not imError then begin
        for Cnt1:=Marker to Cnt do
            if DoObjects(ToExport.At(Cnt1)) then Break;
        Marker:=Cnt+1;
      end;
      imExportedCnt:=Marker*2;
      InsertedList.DeleteAll;
      imDBData^.FreeAll;
      imDBEnd:=FALSE;
    end;
  begin
    imError:=FALSE;
    imLogDlg.AddText(3606,[imOptions.ASCIIFile],TRUE);
    Assign(AsciiFile,imOptions.ASCIIFile);
    Rewrite(AsciiFile,1);
    if IOResult<>0 then ErrorMessage(erOpenFile,[imOptions.ASCIIFile])
    else begin
      ToExport.Init;
      with imOptions do case ExportWhat of
        exProject  : begin
            for Cnt:=0 to imProject^.Layers^.LData^.Count-1 do
                if DoCollectLayer(imProject^.Layers^.LData^.At(Cnt)) then Break;
          end;
        exLayers   : begin
            for Cnt:=0 to ExportLayers^.Count-1 do
              if DoCollectLayer(imProject^.Layers^.IndexToLayer(LongInt(ExportLayers^.At(Cnt)))) then Break;
          end;
        exSelected : begin
            ToExport.AtInsert(ToExport.GetCount,imProject^.Layers^.TopLayer);
            for Cnt:=0 to imProject^.Layers^.SelLayer^.Data^.GetCount-1 do
              if DoCollectObjects(imProject^.Layers^.SelLayer^.Data^.At(Cnt)) then Break;
          end;
      end;
      GetMem(WriteBuffer,maxWriteBufferLen);
      FillChar(WriteBuffer^,maxWriteBufferLen,' ');
      LineStart:=WriteBuffer;
      LineEnd:=0;
      ExportedList.Init;
      CurObjectNumber:=0;
      imWarnings:=0;
      Marker:=0;
      if imProject^.PInfo^.CoordinateSystem.CoordinateType=ctGeodatic then begin
        XPosition:=ctYPosition;
        YPosition:=ctXPosition;
      end
      else begin
        XPosition:=ctXPosition;
        YPosition:=ctYPosition;
      end;
      DBLoaded:=DDEHandler.GetDBLoaded;
      DDEMLLoaded:=DDEHandler.GetDDEMLLoaded;
      if (imColumns[ctDatabase]=NIL)
          or not DDEHandler.DDEOk
          {or (DBLoaded<>1) then begin}
          or ((DBLoaded<>1) and (DDEMLLoaded<>1)) then begin      {?}
        imAllObjects:=ToExport.GetCount;
        imExportedCnt:=Round(imAllObjects/100);
        Inc(imAllObjects,imExportedCnt);
        imLogDlg.SetPercent(100.0*imExportedCnt/imAllObjects);
        for Cnt:=0 to ToExport.GetCount-1 do
            if DoObjects(ToExport.At(Cnt)) then Break;
      end
      else begin
        SendExportDBDefinition;
        InsertedList.Init(200,1);
        imDBEnd:=FALSE;
        imDBData:=New(PIndexColl,Init(200,1));
        imAllObjects:=ToExport.GetCount*2;
        imExportedCnt:=Round(imAllObjects/100);
        Inc(imAllObjects,imExportedCnt);
        imLogDlg.SetPercent(100.0*imExportedCnt/imAllObjects);
        DDEHandler.StartList('[EXP][   ]');
        { get database information for the objects and export them. Be patient,
        + the layer must be contained in the ToExport-List before the objects }
        for Cnt:=0 to ToExport.GetCount-1 do begin
          ExpItem:=ToExport.At(Cnt);
          if ExpItem^.GetObjType=ot_Layer then begin
            if DDEHandler.FLayerInfo<>0 then begin
              { send current list to the database and process objects }
              if DDEHandler.QuestLCnt>0 then begin
                DDEHandler.EndList(FALSE);
                DoTheObjects(TRUE);
              end;
              { create startlist-command for the current layer }
              if DDEHandler.FLayerInfo=1 then DDEHandler.StartList('[EXP]['+PToStr(PLayer(ExpItem)^.Text)+'][   ]')
              else if DDEHandler.FLayerInfo=2 then begin
                Str(ExpItem^.Index,TempStr);
                DDEHandler.StartList('[EXP]['+TempStr+'][   ]');
              end;
            end;
          end
          else if (DDEHandler.FLayerInfo<>0) or not ExportedList.Search(ExpItem,AIndex) then begin
            if not InsertedList.Search(ExpItem,BIndex) then begin
              InsertedList.Insert(ExpItem);
              DDEHandler.AppendList(ExpItem^.Index);
              if DDEHandler.QuestLCnt=0 then DoTheObjects(TRUE);
            end;
          end;
          if imError then Break;
        end;
        if not imError
            and (Marker<=Cnt) then begin
          if DDEHandler.QuestLCnt>0 then DDEHandler.EndList(FALSE);
          DoTheObjects(DDEHandler.QuestLCnt>0);
        end;
        InsertedList.DeleteAll;
        InsertedList.Done;
        Dispose(imDBData,Done);
        imDBData:=NIL;
        DDEHandler.SendString('[END][]');
      end;
      if not imError then FlushBuffer;
      FreeMem(Pointer(WriteBuffer),maxWriteBufferLen);
      Close(ASCIIFile);
      ExportedList.DeleteAll;
      ExportedList.Done;
      ToExport.DeleteAll;
      ToExport.Done;
      if imError then Erase(ASCIIFile);
    end;
    if imError then imLogDlg.AddBlankLine;
    ExportASCIIFile:=not imError;
  end;

{****************************************************************************************}
{ Function TImport.ReadTables                                                            }
{----------------------------------------------------------------------------------------}
{ Liest alle ben�tigten Tabellen aus dem INI-File ein.                                   }
{----------------------------------------------------------------------------------------}
{ Ergebnis: FALSE, wenn ein Fehler aufgetreten ist, TRUE sonst.                          }
{****************************************************************************************}
Function TImport.ReadTables
   : Boolean;
  var Cnt          : Integer;
  begin
    Cnt:=0;
    while Cnt<maxTableCount do begin
      if not ReadTableFromIni(imOwner.Handle,TableNames[Cnt],TableOptions[Cnt],imTables[Cnt]) then
          Cnt:=maxTableCount;
      Inc(Cnt);
    end;
    ReadTables:=Cnt=maxTableCount;
  end;

{****************************************************************************************}
{ Function TImport.CheckColumnOverlap                                                    }
{----------------------------------------------------------------------------------------}
{ Pr�ft, ob sich Spalten �berlappen.                                                     }
{----------------------------------------------------------------------------------------}
{ Ergebnis: TRUE, wenn sich Spalten �berlappen, FALSE sonst.                             }
{****************************************************************************************}
Function TImport.CheckColumnOverlap
   : Boolean;
  var Cnt               : Integer;
      Cnt1              : Integer;
      DoOverlap         : Boolean;
  Procedure DoCheckOverlap
     (
     Column1       : PColumn;
     Column2       : PColumn
     );
    begin
      if (Column1<>NIL)
          and (Column2<>NIL)
          and (Column1^.coStartPosition<=Column2^.coEndPosition)
          and (Column1^.coEndPosition>=Column2^.coStartPosition) then begin
        imLogDlg.AddText(3015,[Column1^.coName,Column2^.coName],FALSE);
        DoOverlap:=TRUE;
      end;
    end;
  const MayNotOverlap   = [ctXPosition,ctYPosition,ctParamA,ctParamB,ctParamC,
                           ctObjectNumber,ctBlockNumber,ctPrevBlockNumber,ctID];
  begin
    DoOverlap:=FALSE;
    for Cnt:=0 to ctLast-1 do
        for Cnt1:=Cnt+1 to ctLast do if (Cnt in MayNotOverlap)
            and (Cnt1 in MayNotOverlap) then
      DoCheckOverlap(imColumns[Cnt],imColumns[Cnt1]);
    for Cnt:=0 to ctLast do if (Cnt in MayNotOverlap) then
      DoCheckOverlap(imColumns[ctLayerName],imColumns[Cnt]);
    for Cnt:=0 to ctLast do if (Cnt in MayNotOverlap) then
      DoCheckOverlap(imColumns[ctTextLayer],imColumns[Cnt]);
    DoCheckOverlap(imColumns[ctObjectNumber],imColumns[ctImagePath]);
    DoCheckOverlap(imColumns[ctObjectType],imColumns[ctImagePath]);
    CheckColumnOverlap:=DoOverlap;
  end;

{***********************************************************************************************}
{ Procedure RemoveGerman                                                                        }
{   Ersetzt Umlaute, � und Leerzeichen in AStr (ACAD-DXFIn erlaubt diese Zeichen nicht).        }
{***********************************************************************************************}
Procedure RemoveGerman
   (
   var AStr        : String
   );
  var Cnt          : Integer;
      BStr         : String;
  begin
    BStr:='';
    for Cnt:=1 to Length(AStr) do begin
      Case AStr[Cnt] of
        '�'  : BStr:=BStr+'Ae';
        '�'  : BStr:=BStr+'ae';
        '�'  : BStr:=BStr+'Oe';
        '�'  : BStr:=BStr+'oe';
        '�'  : BStr:=BStr+'Ue';
        '�'  : BStr:=BStr+'ue';
        '�'  : BStr:=BStr+'ss';
        ' '  : BStr:=BStr+'-';
        else BStr:=BStr+AStr[Cnt];
      end;
    end;
    AStr:=BStr;
  end;

{***********************************************************************************************}
{ Procedure TImport.InsertObject                                                                }
{   F�gt das Objekt Item in die Objektliste und in den Layer LayerName ein. Existiert der Layer }
{   nicht, so wird ein neuer Layer erzeugt. Item wird nicht in die Datenbank eingef�gt.         }
{***********************************************************************************************}
Procedure TImport.InsertObjectAndLayer
   (
   Item            : PView;
   LayerName       : String
   );
  var Layer        : PLayer;
  begin
    Layer:=imProject^.Layers^.NameToLayer(LayerName);
    if Layer=NIL then Layer:=imProject^.Layers^.InsertLayer(LayerName,nl_Color,
                       nl_Line,nl_PatCol,nl_Pat,ilTop,DefaultSymbolFill);
    try
      begin
        PLayer(imProject^.PInfo^.Objects)^.InsertObject(imProject^.PInfo,Item,TRUE);
        Layer^.InsertObject(imProject^.PInfo,New(PIndex,Init(Item^.Index)),TRUE);
        imProject^.CorrectSize(Item^.ClipRect,Item^.Index Mod 40=0);
      end;  
    except
    end;
  end;

Procedure TImport.ImportDXF
   (
   AData           : PProj
   );
  var Dialog       : TOpenDialog; 
  begin
    imProject:=AData;
    Dialog:=TOpenDialog.Create(imProject^.Parent);
    Dialog.Title:=GetLangText(111);
    Dialog.Filter:=GetLangText(3553);
    if Dialog.Execute then begin
      StrPCopy(AscFileName,Dialog.FileName);  
      if not CheckFileExist(AscFileName) then
          MsgBox(imProject^.Parent.Handle,1013,mb_IconExclamation or mb_OK,StrPas(AscFileName))
      else begin
        imProject^.SetCursor(crHourGlass);
        imProject^.SetStatusText(GetLangText(3012),TRUE);
        imInProgress:=ipDXFImport;
        ExecuteDXFIn;
        imInProgress:=ipNone;
        imProject^.SetCursor(crDefault);
        imProject^.ClearStatusText;
      end;
    end;
    Dialog.Free;
  end;

{***********************************************************************************************}
{ Procedure RemoveSpaces                                                                        }
{   L�scht Leerzeichen aus AString.                                                             }
{***********************************************************************************************}
Function RemoveSpace
   (
   AString         : String
   )
   : String;
  var Count        : Byte;
  begin
    Count:=1;
    while Count<=byte(AString[0]) do begin
      if AString[Count]=#32 then begin
        Move(AString[Count+1], AString[Count], byte(AString[0])-Count);
        Dec(byte(AString[0]));
      end
      else Inc(Count);
    end;
    RemoveSpace:=AString;
  end;

{***********************************************************************************************}
{ Procedure RemoveSpaceBack                                                                     }
{   L�scht Leerzeichen vom Ende von AString.                                                    }
{***********************************************************************************************}
Function RemoveSpaceBack
   (
   AString         : String
   )
   : String;
  var Cnt          : Integer;
  begin
    Cnt:=Length(AString);
    while (Cnt>0) and (AString[Cnt]=' ') do Dec(Cnt);
    Delete(AString,Cnt+1,Length(AString)-Cnt);
    RemoveSpaceBack:=AString;
  end;

{***********************************************************************************************}
{ Procedure RemoveSpaceFront                                                                    }
{   L�scht Leerzeichen vom Anfang von AString.                                                  }
{***********************************************************************************************}
Function RemoveSpaceFront
   (
   AString       : String
   )
   : String;
  var Cnt        : Integer;
  begin
    Cnt:=1;
    while (Cnt<=Length(AString)) and (AString[Cnt]=' ') do Inc(Cnt);
    Delete(AString,1,Cnt-1);
    RemoveSpaceFront:=AString;
  end;

Procedure TImport.ExecuteDXFIn;

var AscFile      : text;
    Line1        : String;
    Line2        : String;
    PObj         : PView;
    Closed       : Boolean;
    X            : Boolean;
    Y            : Boolean;
    X2           : Boolean;
    Y2           : Boolean;
    First        : Boolean;
    Pos          : TDPoint;
    Pos2         : TDPoint;
    Err          : Integer;
    Dummy        : real;
    TempCount    : LongInt;
    LineCount    : LongInt;
    Layerbez     : String;

  Function ReadLines
    : Boolean;

  begin
    ReadLines:=TRUE;
    ReadLn(AscFile, Line1);
    inc(LineCount);
    Line1:=UpString(RemoveSpaceFront(RemoveSpaceBack(Line1)));
    ReadLn(AscFile, Line2);
    inc(LineCount);
    Line2:=UpString(RemoveSpaceFront(RemoveSpaceBack(Line2)));
    if IOResult<>0 then begin
      MsgBox(imProject^.Parent.Handle, 3003, mb_Ok, '');
      ReadLines:=FALSE;
    end;
  end;

 Const  BlockMax  = 1000;
 type ArBlkName   = array[1..BlockMax] of String[40];
     ArBlkLayer   = array[1..BlockMax] of String[32];
     ArBlkReal    = array[1..BlockMax] of real;


 var bNoError     : Boolean;
     BlkCount     : Word;
     BlkI         : Word;
     BlkName      : ^ArBlkName;
     BlkLayer     : ^ArBlkLayer;
     BlkXoff      : ^ArBlkReal;
     BlkYoff      : ^ArBlkReal;
     BlkXScale    : ^ArBlkReal;
     BlkYScale    : ^ArBlkReal;
     BlkMerk      : LongInt;
     BlockName    : String[40];
     Kompatibel   : Boolean;

begin
  TempCount:=0;
  Pos.Init(0, 0);
  Pos2.Init(0, 0);
{$I-}

  (* Blk-Felder initialisieren *)
  new(BlkName);
  new(BlkLayer);
  new(BlkXoff);
  new(BlkYoff);
  new(BlkXScale);
  new(BlkYScale);

  for BlkCount:=1 to BlockMax do
  begin
    BlkName^[BlkCount]:='';
    BlkLayer^[BlkCount]:='';
    BlkXoff^[BlkCount]:=0.0;
    BlkYoff^[BlkCount]:=0.0;
    BlkXScale^[BlkCount]:=1.0;
    BlkYScale^[BlkCount]:=1.0;
  end;
  BlkCount:=0;

  (* erster Durchlauf : lies die INSERT Eintraege in der SECTION ENTITIES *)

  Assign(AscFile, AscFileName);
  Reset(AscFile);
  LineCount:=0;
  while Readlines and (Line2<>'EOF') do
  begin
      if (Line2='SECTION') then
      begin
         if ReadLines and (Line2='ENTITIES') then
         begin
           (* wir befinden uns in der ENTITIES SECTION : suchen von INSERT *)
           bNoError:=ReadLines;
           repeat
             if bNoError and (Line2='INSERT') then
             begin
                (* es wird also ein Block eingefuegt ! *)
                if BlkCount=BlockMax then
                  MsgBox(imProject^.Parent.Handle, 3010, mb_Ok, '')
                else
                  inc(BlkCount);
                repeat
                   bNoError:=ReadLines;
                   if bNoError and (Line1='8') then
                   begin
                     (* Layer auf dem der Block eingefuegt werden soll *)
                     BlkLayer^[BlkCount]:=Line2;
                   end;
                   if bNoError and (Line1='2') then
                   begin
                      (* Blockname *)
                      BlkName^[BlkCount]:=Line2;
                   end;
                   if bNoError and (Line1='10') then
                   begin
                      (* X Offset bezogen auf Einf�gepunkt *)
                      Val(Line2, Dummy, Err);
                      BlkXoff^[BlkCount]:=Dummy;
                   end;
                   if bNoError and (Line1='20') then
                   begin
                      (* Y Offset bezogen auf Einf�gepunkt *)
                      Val(Line2, Dummy, Err);
                      BlkYoff^[BlkCount]:=Dummy;
                   end;
                   if bNoError and (Line1='41') then
                   begin
                      (* X Scale Faktor *)
                      Val(Line2, Dummy, Err);
                      BlkXScale^[BlkCount]:=Dummy;
                   end;
                   if bNoError and (Line1='42') then
                   begin
                      (* Y Scale Faktor *)
                      Val(Line2, Dummy, Err);
                      BlkYScale^[BlkCount]:=Dummy;
                   end;
                until (not bNoError) or (Line1='0');
             end
             else
             begin
                bNoError:=ReadLines;
             end;
           until (not bNoError) or (Line2='ENDSEC') or (Line2='EOF');
         end;
      end;
  end;
  Close(AscFile);

  (* 2. Durchlauf *)

  Reset(AscFile);
  LineCount:=0;
  while ReadLines and (Line2<>'EOF') do begin
    Layerbez:='';
    if (Line2='SECTION') then
    begin
       if ReadLines then
       begin
         if (Line2='BLOCKS') then
         begin
            repeat
              bNoError:=Readlines;
              if bNoError and (Line2='BLOCK') then
              begin
                 (* als erstes wird der Blockname bestimmt ! *)
                 repeat
                    bNoError:=ReadLines;
                    if bNoError and (Line1='2') then
                    begin
                       (* Blockname suchen und zuweisen *)
                       BlkMerk:=LineCount;
                       BlkI:=1;
                       BlockName:=line2;
                       while (BlkName^[BlkI]<>BlockName) and (BlkI<=BlkCount) do
                          inc(BlkI);
                       repeat
                          if (BlkI<=BlkCount) and (BlkName^[BlkI]<>'') then
                          begin
                             (* es wird dieser Block eingef�gt : *)
                              if (BlkMerk<>LineCount) then
                              begin
                                (* Block wird nochmals eingef�gt : Dateiposition zur�cksetzen *)
                                 reset(AscFile);

                                 LineCount:=0;
                                 while LineCount<BlkMerk do
                                 begin
                                    readln(AscFile);
                                    inc(LineCount);
                                 end;
                                 (*LineCount:=0;
                                 while LineCount<BlkMerk do
                                    readlines; *)
                              end;
                              Kompatibel:=FALSE;
                              repeat
                                 (* f�ge die Objekte dieses Blocks ein : *)

                                 bNoError:=ReadLines;
                                 while ((Line2='POLYLINE') or (Line2='POINT') or (Line2='LINE')) do begin
                                    Closed:=FALSE;
                                    Kompatibel:=TRUE;
                                    if Line2='POLYLINE' then begin
                                      Inc(TempCount);
                                      First:=TRUE;
                                      while ReadLines and (Line2<>'SEQEND') and (Line1<>'SEQEND') do begin
                                      (* Layer von INSERT �bernehmen : *)
                                          layerbez:=BlkLayer^[BlkI];
                                          OemToAnsiBuff(@layerbez[1],@layerbez[1],Length(Layerbez));
                                        if (Line1='70') and (Line2='1') then
                                          Closed:=TRUE;
                                        if Line2='VERTEX' then begin
                                          if Closed and First then
                                            PObj:=New(PCPoly, Init)
                                          else
                                            if  not(Closed) and First then
                                              PObj:=New(PPoly, Init);
                                          First:=FALSE;
                                          X:=FALSE;
                                          Y:=FALSE;
                                          while ReadLines and not(X and Y) do begin
                                            if Line1='10' then begin
                                              X:=TRUE;
                                              Val(Line2, Dummy, Err);
                                              Pos.X:=Round((Dummy*BlkXScale^[BlkI]+BlkXoff^[BlkI])*100.0);
                                            end;
                                            if Line1='20' then begin
                                              Y:=TRUE;
                                              Val(Line2, Dummy, Err);
                                              Pos.Y:=Round((Dummy*BlkYScale^[BlkI]+BlkYoff^[BlkI])*100.0);
                                            end;
                                          end;
                                          if not(PPoly(PObj)^.InsertPoint(Pos)) then begin
                                            MsgBox(imProject^.Parent.Handle, 3000, mb_Ok or mb_IconExclamation, '');
                                            Exit;
                                          end;
                                        end;
                                      end;
                                      if Closed then
                                        PCPoly(PObj)^.ClosePoly;
                                      if layerbez<>'' then
                                         InsertObjectAndLayer(PObj,layerbez)
                                      else
                                      begin
                                            MsgBox(imProject^.Parent.Handle, 3003, mb_Ok or mb_IconExclamation, '');
                                            Exit;
                                      end;
                                    end;
                                    if Line2='POINT' then begin
                                      Inc(TempCount);
                                      X:=FALSE;
                                      Y:=FALSE;
                                      while ReadLines and not(X and Y) do begin
                                        (* Layer von INSERT �bernehmen : *)
                                          layerbez:=BlkLayer^[BlkI];
                                          OemToAnsiBuff(@layerbez[1],@layerbez[1],Length(Layerbez));
                                        if Line1='10' then begin
                                          X:=TRUE;
                                          Val(Line2, Dummy, Err);
                                          Pos.X:=Round((Dummy*BlkXScale^[BlkI]+BlkXoff^[BlkI])*100.0);
                                        end;
                                        if Line1='20' then begin
                                          Y:=TRUE;
                                          Val(Line2, Dummy, Err);
                                          Pos.Y:=Round((Dummy*BlkYScale^[BlkI]+BlkYoff^[BlkI])*100.0);
                                        end;
                                      end;
                                      PObj:=New(PPixel, Init(Pos));
                                      if layerbez<>'' then
                                         InsertObjectandLayer(PObj,layerbez)
                                      else
                                      begin
                                            MsgBox(imProject^.Parent.Handle, 3003, mb_Ok or mb_IconExclamation, '');
                                            Exit;
                                      end;

                                    end;
                                    if Line2='LINE' then begin
                                      Inc(TempCount);
                                      X:=FALSE;
                                      Y:=FALSE;
                                      X2:=FALSE;
                                      Y2:=FALSE;
                                      while ReadLines and not(X and Y and X2 and Y2) do begin
                                      (* Layer von INSERT �bernehmen : *)
                                          layerbez:=BlkLayer^[BlkI];
                                          OemToAnsiBuff(@layerbez[1],@layerbez[1],Length(Layerbez));
                                        if Line1='10' then begin
                                          X:=TRUE;
                                          Val(Line2, Dummy, Err);
                                          Pos.X:=Round((Dummy*BlkXScale^[BlkI]+BlkXoff^[BlkI])*100.0);
                                        end;
                                        if Line1='20' then begin
                                          Y:=TRUE;
                                          Val(Line2, Dummy, Err);
                                          Pos.Y:=Round((Dummy*BlkYScale^[BlkI]+BlkYoff^[BlkI])*100.0);
                                        end;
                                        if Line1='11' then begin
                                          X2:=TRUE;
                                          Val(Line2, Dummy, Err);
                                          Pos2.X:=Round((Dummy*BlkXScale^[BlkI]+BlkXoff^[BlkI])*100.0);
                                        end;
                                        if Line1='21' then begin
                                          Y2:=TRUE;
                                          Val(Line2, Dummy, Err);
                                          Pos2.Y:=Round((Dummy*BlkYScale^[BlkI]+BlkYoff^[BlkI])*100.0);
                                        end;
                                      end;
                                      PObj:=New(PPoly, Init);
                                      PPoly(PObj)^.InsertPoint(Pos);
                                      PPoly(PObj)^.InsertPoint(Pos2);
                                      if layerbez<>'' then
                                         InsertObjectandLayer(PObj,layerbez)
                                      else
                                      begin
                                            MsgBox(imProject^.Parent.Handle, 3003, mb_Ok or mb_IconExclamation, '');
                                            Exit;
                                      end;

                                   end;
                                 end;
                              until (not bNoError) or (Line2='ENDBLK') or (Line2='ENDSEC');
                              if not Kompatibel then
                              begin
                                  (* der Block enth�lt also ausschlie�lich Elemente,
                                     die von Austromap nicht unterst�tzt werden ! *)
                                  BlkI:=BlKCount+1;
                              end;
                          end
                          else
                          begin
                            (* Block wird also nicht eingef�gt! Lese daher bis Blockende *)
                            repeat
                                bNoError:=Readlines;
                            until (not bNoError) or (Line2='ENDBLK') or (Line2='ENDSEC');
                            BlkI:=BlkCount;
                          end;
                          inc(BlkI);
                          while (BlkI<=BlkCount) and (BlkName^[BlkI]<>BlockName) do
                             inc(BlkI);
                       until BlkI>BlkCount;
                    end;

                 until (not bNoError) or (Line2='ENDSEC') or (Line2='EOF');
              end;
            until (not bNoError) or (Line2='ENDSEC') or (Line2='EOF');
         end
         else
         begin
           if (Line2='ENTITIES') then
           begin
              repeat
                (* Bearbeiten der ENTITIES SECTION *)

                bNoError:=Readlines;
                while ((Line2='POLYLINE') or (Line2='POINT') or (Line2='LINE')) do begin
                  Closed:=FALSE;
                  if Line2='POLYLINE' then begin
                    Inc(TempCount);
                    First:=TRUE;
                    while ReadLines and (Line2<>'SEQEND') and (Line1<>'SEQEND') do begin
                      if (layerbez='') and (line1='8') then begin
                        layerbez:=line2;
                        OemToAnsiBuff(@layerbez[1],@layerbez[1],Length(Layerbez));
                      end;
                      if (Line1='70') and (Line2='1') then
                        Closed:=TRUE;
                      if Line2='VERTEX' then begin
                        if Closed and First then
                          PObj:=New(PCPoly, Init)
                        else
                          if  not(Closed) and First then
                            PObj:=New(PPoly, Init);
                        First:=FALSE;
                        X:=FALSE;
                        Y:=FALSE;
                        while ReadLines and not(X and Y) do begin
                          if Line1='10' then begin
                            X:=TRUE;
                            Val(Line2, Dummy, Err);
                            Pos.X:=Round(Dummy*100);
                          end;
                          if Line1='20' then begin
                            Y:=TRUE;
                            Val(Line2, Dummy, Err);
                            Pos.Y:=Round(Dummy*100);
                          end;
                        end;
                        if not(PPoly(PObj)^.InsertPoint(Pos)) then begin
                          MsgBox(imProject^.Parent.Handle, 3000, mb_Ok or mb_IconExclamation, '');
                          Exit;
                        end;
                      end;
                    end;
                    if Closed then
                      PCPoly(PObj)^.ClosePoly;
            (*   Data^.InsertObject(PObj,FALSE) *)
                    if layerbez<>'' then
                       InsertObjectAndLayer(PObj,layerbez)
                    else
                    begin
                          MsgBox(imProject^.Parent.Handle, 3003, mb_Ok or mb_IconExclamation, '');
                          Exit;
                    end;
                  end;
                  if Line2='POINT' then begin
                    Inc(TempCount);
                    X:=FALSE;
                    Y:=FALSE;
                    while ReadLines and not(X and Y) do begin
                      if (layerbez='') and (line1='8') then begin
                        layerbez:=line2;
                        OemToAnsiBuff(@layerbez[1],@layerbez[1],Length(Layerbez));
                      end;
                      if Line1='10' then begin
                        X:=TRUE;
                        Val(Line2, Dummy, Err);
                        Pos.X:=Round(Dummy*100);
                      end;
                      if Line1='20' then begin
                        Y:=TRUE;
                        Val(Line2, Dummy, Err);
                        Pos.Y:=Round(Dummy*100);
                      end;
                    end;
                    PObj:=New(PPixel, Init(Pos));
            (*        Data^.InsertObject(PObj,FALSE); *)
                    if layerbez<>'' then
                       InsertObjectandLayer(PObj,layerbez)
                    else
                    begin
                          MsgBox(imProject^.Parent.Handle, 3003, mb_Ok or mb_IconExclamation, '');
                          Exit;
                    end;

                  end;
                  if Line2='LINE' then begin
                    Inc(TempCount);
                    X:=FALSE;
                    Y:=FALSE;
                    X2:=FALSE;
                    Y2:=FALSE;
                    while ReadLines and not(X and Y and X2 and Y2) do begin
                      if (layerbez='') and (line1='8') then begin
                        layerbez:=line2;
                        OemToAnsiBuff(@layerbez[1],@layerbez[1],Length(Layerbez));
                      end;
                      if Line1='10' then begin
                        X:=TRUE;
                        Val(Line2, Dummy, Err);
                        Pos.X:=Round(Dummy*100);
                      end;
                      if Line1='20' then begin
                        Y:=TRUE;
                        Val(Line2, Dummy, Err);
                        Pos.Y:=Round(Dummy*100);
                      end;
                      if Line1='11' then begin
                        X2:=TRUE;
                        Val(Line2, Dummy, Err);
                        Pos2.X:=Round(Dummy*100);
                      end;
                      if Line1='21' then begin
                        Y2:=TRUE;
                        Val(Line2, Dummy, Err);
                        Pos2.Y:=Round(Dummy*100);
                      end;
                    end;
                    PObj:=New(PPoly, Init);
                    PPoly(PObj)^.InsertPoint(Pos);
                    PPoly(PObj)^.InsertPoint(Pos2);
            (*       Data^.InsertObject(PObj,FALSE); *)
                    if layerbez<>'' then
                       InsertObjectandLayer(PObj,layerbez)
                    else
                    begin
                          MsgBox(imProject^.Parent.Handle, 3003, mb_Ok or mb_IconExclamation, '');
                          bNoError:=FALSE;
                    end;

                  end;
                end;

              until (not bNoError) or (Line2='ENDSEC') or (Line2='EOF');
           end;
         end;
       end;
    end;
  end;
  Close(AscFile);

  (* Blk-Felder freigeben *)
  dispose(BlkName);
  dispose(BlkLayer);
  dispose(BlkXoff);
  dispose(BlkYoff);
  dispose(BlkXScale);
  dispose(BlkYScale);

{$I+}
end;

Procedure TImport.ExportDXF;

const Tab          = '  ';
      Tab0         = '  0';
      Tab2         = '  2';

var AscFile      : text;
    ViewObj      : PIndex;
    TempCount    : LongInt;
    AStr         : String;
    LayerName    : String;

  Procedure DoAll
    (
    Item         : PLayer
    ); far;

    Procedure DoAll2
      (
      Item2        : PIndex
      ); far;

      Procedure DoAll3
        (
        Item3        : PDPoint
        ); far;

      begin
        WriteLn(AscFile, Tab0);
        WriteLn(AscFile, 'VERTEX');
        WriteLn(AscFile, Tab, '8');
        WriteLn(AscFile, LayerName);
        WriteLn(AscFile, ' 10');
        Str(Item3^.X/100:0:2,AStr);
        WriteLn(AscFile, AStr);
        WriteLn(AscFile, ' 20');
        Str(Item3^.Y/100:0:2,AStr);
        WriteLn(AscFile, AStr);
        WriteLn(AscFile, ' 30');
        WriteLn(AscFile, '0');
      end;

    begin
      ViewObj:=PLayer(Item)^.IndexObject(imProject^.PInfo, Item2);
      if (Item2^.GetObjType=ot_Pixel) then begin
        Inc(TempCount);
        WriteLn(AscFile, Tab0);
        WriteLn(AscFile, 'POINT');
        WriteLn(AscFile, Tab, '8');
        WriteLn(AscFile, LayerName);
        WriteLn(AscFile, ' 10');
        Str(PPixel(ViewObj)^.Position.X/100:0:2,AStr);
        WriteLn(AscFile, AStr);
        WriteLn(AscFile, ' 20');
        Str(PPixel(ViewObj)^.Position.Y/100:0:2,AStr);
        WriteLn(AscFile, AStr);
        WriteLn(AscFile, ' 30');
        WriteLn(AscFile, '0');
      end
      else begin
        if (Item2^.GetObjType=ot_Poly)
            or (Item2^.GetObjType=ot_CPoly) then begin
          Inc(TempCount);
          WriteLn(AscFile, Tab0);
          WriteLn(AscFile, 'POLYLINE');
          WriteLn(AscFile, Tab, '8');
          WriteLn(AscFile, LayerName);
          WriteLn(AscFile, ' 66');
          WriteLn(AscFile, '     1');
          WriteLn(AscFile, ' 10');
          WriteLn(AscFile, '0');
          WriteLn(AscFile, ' 20');
          WriteLn(AscFile, '0');
          WriteLn(AscFile, ' 30');
          WriteLn(AscFile, '0');
          if Item2^.GetObjType=ot_CPoly then begin
            WriteLn(AscFile, ' 70');
            WriteLn(AscFile, '     1');
          end;
          PPoly(ViewObj)^.Data^.ForEach(@DoAll3);
          WriteLn(AscFile, Tab0);
          WriteLn(AscFile, 'SEQEND');
        end;
      end;
    end;

  begin
    if (Item^.Index<>0)
        and not(Item^.GetState(sf_LayerOff)) then begin
      LayerName:=PToStr(Item^.Text);
      RemoveGerman(LayerName);
      AnsiToOemBuff(@LayerName[1],@LayerName[1],Length(LayerName));
      Item^.Data^.ForEach(@DoAll2);
    end;
  end;

var Abort          : Boolean;
    Dialog         : TOpenDialog;
begin
  Dialog:=TSaveDialog.Create(imProject^.Parent);
  Dialog.Filter:=GetLangText(3553);
  Dialog.Title:=GetLangText(105);
  if Dialog.Execute then begin
    StrPCopy(AscFileName,Dialog.FileName);
    imInProgress:=ipDXFExport;
    Abort:=FALSE;
    TempCount:=0;
    Assign(AscFile, AscFileName);
  {$I-}
    Reset(AscFile);
  {$I+}
    if IOResult=0 then begin
      if id_No=MsgBox(imProject^.Parent.Handle, 1006,
          mb_YesNo or mb_DefButton2 or mb_IconExclamation,StrPas(ASCFileName)) then
        Abort:=TRUE;
      Close(AscFile);
    end;
    if not Abort then begin
      imProject^.SetStatusText(GetLangText(3616),TRUE);
      Rewrite(AscFile);
      WriteLn(AscFile, Tab0);
      WriteLn(AscFile, 'SECTION');
      WriteLn(AscFile, Tab2);
      WriteLn(AscFile, 'ENTITIES');
      imProject^.Layers^.LData^.ForEach(@DoAll);
      WriteLn(AscFile, Tab0);
      WriteLn(AscFile, 'ENDSEC');
      WriteLn(AscFile, Tab0);
      WriteLn(AscFile, 'EOF');
      Close(AscFile);
      imProject^.ClearStatusText;
    end;
    imInProgress:=ipNone;
  end;
  Dialog.Free;
end;

{========================================================================================}
{ TImportDlg                                                                             }
{========================================================================================}

Constructor TImportDlg.Init
   (
   AParent              : TComponent;
   AName                : PChar;
   AData                : PASCIIData;
   ALayers              : PLayers;
   APInfo               : PPaint
   );
  begin
    inherited Init(AParent,AName,ALayers,APInfo^.ProjStyles);
    IsImport:=TRUE;
    Data:=AData;
    PInfo:=APInfo;
    AFont.Init;
    AFont:=Data^.GraphicFont;
    HelpContext:=1020;
  end;

Function TImportDlg.CanClose
   : Boolean;
  var ASCIIName    : Array[0..255] of Char;
      ADFName      : Array[0..255] of Char;
      SymName      : Array[0..255] of Char;
      Graphic      : LongInt;
      Objects      : LongInt;
      BFont        : PFontDes;
  Function GetFontHeight
     : Boolean;
    begin
      GetFontHeight:=FALSE;
      if not GetFieldLong(idFontHeight,AFont.Height) then begin
        MsgBox(Handle,3522,mb_IconExclamation or mb_OK,'');
        SetFocus(GetItemHandle(idFontHeight));
      end
      else if AFont.Height=0 then begin
          MsgBox(Handle,3521,mb_IconExclamation or mb_OK,'');
          SetFocus(GetItemHandle(idFontHeight));
        end
        else GetFontHeight:=TRUE;
    end;
  Function DoGetLayer
     (
     AButton       : Integer;
     AId           : Integer;
     var ALayer    : LongInt
     )
     : Boolean;
    begin
      ALayer:=0;
      if IsDlgButtonChecked(Handle,AButton)=0 then DoGetLayer:=TRUE
      else DoGetLayer:=GetLayer(AId,ALayer,959,10125,10128,ilTop);
    end;
  begin
    CanClose:=FALSE;
    if CheckFile(idASCIIFile,ASCIIName,FALSE)
        and CheckFile(idADFFile,ADFName,FALSE)
        and CheckFile(idSymbolFile,SymName,FALSE)
        and DoGetLayer(idGraphicLayer,idGraphicCombo,Graphic)
        and DoGetLayer(idObjectLayer,idObjectCombo,Objects)
        and GetFontHeight then with Data^ do begin
      ASCIIFile:=StrPas(ASCIIName);
      ADFFile:=StrPas(ADFName);
      SymbolFile:=StrPas(SymName);
      if IsDlgButtonChecked(Handle,idGraphicObject)<>0 then GraphicLayer:=-1
      else GraphicLayer:=Graphic;
      ObjectLayer:=Objects;
      GraphicFont:=AFont;
      BFont:=PInfo^.Fonts^.GetFont(AFont.Font);            {mf}
      if (BFont<>NIL) and BFont^.IsVertical then GraphicFontVert:=TRUE
      else GraphicFontVert:=FALSE;
      if IsDlgButtonChecked(Handle,idLayerStyle)<>0 then NewLayerStyle:=1
      else NewLayerStyle:=0;
      CanClose:=TRUE;
    end;
  end;

Procedure TImportDlg.SetupWindow;
  var AId          : Integer;
      ALayer       : PLayer;
  begin
    inherited SetupWindow;
    if IsImport then with Data^ do begin
      SetFieldText(idASCIIFile,ASCIIFile);
      SetFieldText(idADFFile,ADFFile);
      SetFieldText(idSymbolFile,SymbolFile);
      if GraphicLayer=0 then AId:=idSkipGraphic
      else if GraphicLayer=-1 then AId:=idGraphicObject
      else begin
        AId:=idGraphicLayer;
        ALayer:=Layers^.IndexToLayer(GraphicLayer);
        if ALayer<>NIL then SetFieldText(idGraphicCombo,PToStr(ALayer^.Text));
      end;
      CheckRadioButton(Handle,idSkipGraphic,idGraphicLayer,AId);
      EnableWindow(GetItemHandle(idGraphicCombo),GraphicLayer<>0);
      if ObjectLayer=0 then AId:=idSkipObjects
      else begin
        AId:=idObjectLayer;
        ALayer:=Layers^.IndexToLayer(ObjectLayer);
        if ALayer<>NIL then SetFieldText(idObjectCombo,PToStr(ALayer^.Text));
      end;
      CheckRadioButton(Handle,idSkipObjects,idObjectLayer,AId);
      EnableWindow(GetItemHandle(idObjectCombo),ObjectLayer<>0);
      FillLayerList(idGraphicCombo,FALSE,TRUE);
      FillLayerList(idObjectCombo,FALSE,TRUE);
      SetFont;
      CheckDlgButton(Handle,idLayerStyle,NewLayerStyle);
    end;
  end;

Procedure TImportDlg.SelectFile
   (
   var Msg         : TMessage
   );
  var AFName       : Array[0..255] of Char;
      NxName       : String;
      NxFName      : String;
      Dialog       : TOpenDialog;
  begin
    Dialog:=TOpenDialog.Create(Self);
    Case FileField of
      idAsciiFile   : begin
          Dialog.Filter:=GetLangText(3550);
          Dialog.Title:=GetLangText(3515);
        end;
      idADFFile    : begin
          Dialog.Filter:=GetLangText(3551);
          Dialog.Title:=GetLangText(3516);
        end;
      idSymbolFile : begin
          Dialog.Filter:=GetLangText(3552);
          Dialog.Title:=GetLangText(3517);
        end;
    end;
    if Dialog.Execute then begin
      StrPCopy(AFName,Dialog.FileName);
      SetDlgItemText(Handle,FileField,AFName);
      if FileField=idAsciiFile then begin
        NxName:=StrPas(AFName);
        Delete(NxName,length(NxName)-3,4);
        NxFName:=NxName+fexADFFile+#0;
        if CheckFileExist(@NxFName[1]) or (not IsImport) then SetDlgItemText(Handle,idADFFile,@NxFName[1]);
        NxFName:=NxName+fexSymbolFile;
        if CheckFileExist(@NxFName[1]) or (not IsImport) then SetDlgItemText(Handle,idSymbolFile,@NxFName[1]);
      end;
    end;
    Dialog.Free;
    SetFocus(GetItemHandle(FileField));
  end;

Procedure TImportDlg.ExpandFileName
   (
   AId             : Integer
   );
  var AText        : Array[0..255] of Char;
      Dir          : Array[0..fsDirectory] of Char;
      DName        : Array[0..fsFileName] of Char;
      Ext          : Array[0..fsExtension] of Char;
  begin
    GetDlgItemText(Handle,AId,AText,SizeOf(AText));
    FileSplit(AText,Dir,DName,Ext);
    if (StrComp(DName,'')<>0)
        and (StrComp(Ext,'')=0) then begin
      case AId of
        idASCIIFile : StrCat(AText,fexASCIIFile);
        idADFFile   : StrCat(AText,fexADFFile);
        idSymbolFile: StrCat(AText,fexSymbolFile);
      end;
      SetDlgItemText(Handle,AId,AText);
    end;
  end;

{****************************************************************************************}
{ Procedure TImportDlg.WMCommand                                                         }
{----------------------------------------------------------------------------------------}
{ Behandelt folgende Commands:                                                           }
{ en_SetFocus, en_KillFocus: Der Button zur Dateiauswahl wird disabled, wenn der Focus   }
{   nicht auf einem der Datei-Eingabefelder oder dem Button selbst steht                 }
{ bn_Clicked: Die Comboboxen f�r die Layerauswahl werden entsprechend dem angew�hlten    }
{   Radio-Button enabled oder disabled.                                                  }
{****************************************************************************************}
Procedure TImportDlg.WMCommand
   (
   var Msg         : TWMCommand
   );
  var AId          : Integer;
  begin
    inherited ;
    case Msg.NotifyCode of
      en_SetFocus,
      en_KillFocus : begin
          AId:=GetDlgCtrlId(GetFocus);
          if AId<>idFileButton then begin
            if AId=idASCIIFile then FileField:=idASCIIFile
            else if AId=idADFFile then FileField:=idADFFile
            else if AId=idSymbolFile then FileField:=idSymbolFile
            else FileField:=0;
            EnableWindow(GetItemHandle(idFileButton),FileField<>0);
          end;
          if Msg.NotifyCode=en_KillFocus then
              ExpandFileName(Msg.ItemID);
        end;
      bn_Clicked   : begin
        if (Msg.ItemID=idSkipGraphic)
            or (Msg.ItemID=idGraphicLayer) then
          EnableWindow(GetItemHandle(idGraphicCombo),IsDlgButtonChecked(Handle,idGraphicLayer)=1);
        if (Msg.ItemID=idSkipObjects)
            or (Msg.ItemID=idObjectLayer) then
          EnableWindow(GetItemHandle(idObjectCombo),IsDlgButtonChecked(Handle,idObjectLayer)=1);
      end;
    end;
  end;

{****************************************************************************************}
{ Function TImportDlg.CheckFile                                                          }
{----------------------------------------------------------------------------------------}
{ Pr�ft, ob die in der Eingabezeile AId ein Name eingegeben wurde und ob die Datei       }
{ existiert.                                                                             }
{----------------------------------------------------------------------------------------}
{ AId              i = Name des Eingabefeldes                                            }
{ AText            o = wird auf den neuen Dateinamen gesetzt                             }
{ ForWrite         i = TRUE->�berschreibwarnung, FALSE->Datei mu� existieren             }
{----------------------------------------------------------------------------------------}
{ Ergebnis: TRUE, wenn Eingabe OK und Datei existiert, FALSE sonst                       }
{****************************************************************************************}
Function TImportDlg.CheckFile
   (
   AId             : Integer;
   AText           : PChar;
   ForWrite        : Boolean
   )
   : Boolean;
  var BResult       : Boolean;
  begin
    BResult:=FALSE;
    ExpandFileName(AId);
    if GetDlgItemText(Handle,AId,AText,255)=0 then MsgBox(Handle,3518,mb_IconExclamation or mb_OK,'')
    else begin
      if ForWrite then begin
        if CheckFileExist(AText) then
            BResult:=MsgBox(Handle,1006,mb_IconQuestion+mb_YesNo,StrPas(AText))=id_Yes
        else BResult:=TRUE;
      end
      else begin
        if not CheckFileExist(AText) then MsgBox(Handle,3519,mb_IconExclamation or mb_OK,StrPas(AText))
        else BResult:=TRUE;
      end;
    end;
    if not BResult then SetFocus(GetItemHandle(AId));
    CheckFile:=BResult;
  end;

{****************************************************************************************}
{ Procedure TImportDlg.SelectFont                                                        }
{----------------------------------------------------------------------------------------}
{ Stellt den Fontauswahl-Dialog dar. Dr�ckt der Anwender OK, so werden die Font-Felder   }
{ mittels SetFont upgedatet. Die Daten werden aus AFont genommen und auch dort           }
{ gespeichert.                                                                           }
{****************************************************************************************}
Procedure TImportDlg.SelectFont
   (
   var Msg         : TMessage
   );
  var Dialog       : TFontStyleDialog; 
  begin
    Dialog:=TFontStyleDialog.Create(Self);
    try
      Dialog.Fonts:=PInfo^.Fonts;
      Dialog.FontStyle:=@AFont;
      Dialog.ShowModal;
    finally
      Dialog.Free;
    end;
  end;

{****************************************************************************************}
{ Procedure TImportDlg.WMDrawObject                                                      }
{----------------------------------------------------------------------------------------}
{ Zeichnet das FontSmaple-Feld.                                                          }
{****************************************************************************************}
Procedure TImportDlg.WMDrawObject
   (
   var Msg         : TMessage
   );
  var Font         : HFont;
      OldFont      : HFont;
      Rect         : TRect;
      TAlign       : Word;
  begin
    if Msg.wParam=idFontSample then with Msg do begin
      GetClientRect(GetItemHandle(wParam),Rect);
      Font:=PInfo^.GetFont(AFont,20,0);
      OldFont:=SelectObject(lParamLo,Font);
      Case AFont.Style and $03 of
        0 : TAlign:=dt_Left;
        1 : TAlign:=dt_Center;
        else TAlign:=dt_Right
      end;
      DrawText(lParamLo,'AaBbCcDdEeFf',12,Rect,TAlign or dt_VCenter);
      SelectObject(lParamLo,OldFont);
      DeleteObject(Font);
    end;
  end;

{****************************************************************************************}
{ Procedure TImportDlg.SetFont                                                           }
{----------------------------------------------------------------------------------------}
{ Das FontSample wird neu gezeichnet und die FontH�he in das Eingabefeld geschrieben.    }
{ Ist eine fixe Fonth�he gew�hlt, so wird das Eingabefeld disabled, sonst enabled.       }
{****************************************************************************************}
Procedure TImportDlg.SetFont;
  begin
    InvalidateRect(GetItemHandle(idFontSample),NIL,FALSE);
    SetFieldLongZero(idFontHeight,AFont.Height);
  end;

Procedure TImportDlg.OptionsDlg;
  begin
    ExecDialog(TOptionsDlg.Init(Self));
  end;

Procedure TImportDlg.OffsetsDlg;
  begin
    if IsImport then ExecDialog(TOffsetDlg.Init(Self,@Data^.ImpOffsets,
        Integer(PInfo^.CoordinateSystem.CoordinateType)))
    else ExecDialog(TOffsetDlg.Init(Self,@Data^.ExpOffsets,
        Integer(PInfo^.CoordinateSystem.CoordinateType)))
  end;

{========================================================================================}
{ TASCIIData                                                                             }
{========================================================================================}

Constructor TASCIIData.Init;
  begin
    inherited Init;
    ASCIIFile:='';
    ADFFile:='';
    SymbolFile:='';
    GraphicFont.Init;
    GraphicFont.Height:=500;
    GraphicFontVert:=FALSE; {mf}
    GraphicLayer:=0;
    ObjectLayer:=0;
    ExportWhat:=exProject;
    ExportLayers:=NIL;
    ImpOffsets.XOffset:=0;
    ImpOffsets.YOffset:=0;
    ImpOffsets.Scale:=1;
    ExpOffsets.XOffset:=0;
    ExpOffsets.YOffset:=0;
    ExpOffsets.Scale:=1;
    NewLayerStyle:=0;
  end;

Destructor TASCIIData.Done;
  begin
    if ExportLayers<>NIL then Dispose(ExportLayers,Done);
    inherited Done;
  end;

{========================================================================================}
{ TTableEntry                                                                            }
{========================================================================================}

Constructor TTableEntry.Init
   (
   AType           : LongInt
   );
  begin
    inherited Init;
    teType:=AType;
    teNames.Init(ceTableNames,ciTableNames);
    teExportName:=NIL;
    teTypeName:=NIL;
  end;

Constructor TTableEntry.InitName
   (
   Const AName     : String
   );
  begin
    Init(0);
    teTypeName:=NewStr(AName);
  end;

Destructor TTableEntry.Done;
  begin
    teNames.Done;
    DisposeStr(teTypeName);
    inherited Done;
  end;

Function TTableEntry.GetType
   : LongInt;
  begin
    GetType:=teType;
  end;

Function TTableEntry.GetTypeName
   : String;
  begin
    GetTypeName:=PToStr(teTypeName);
  end;

{****************************************************************************************}
{ Function TTableEntry.InsertName                                                        }
{----------------------------------------------------------------------------------------}
{ F�gt einen Namen in die Namensliste ein. Ist es der erste Eintrag, so wird dieser      }
{ als ExportName gesetzt.                                                                }
{----------------------------------------------------------------------------------------}
{ AName            i = einzuf�gender Name                                                }
{----------------------------------------------------------------------------------------}
{ Ergebis: Position des eingef�gten Strings.                                             }
{****************************************************************************************}
Function TTableEntry.InsertName
   (
   Const AName     : String
   )
   : Integer;
  var AStr         : PString;
      APos         : Integer;
  begin
    AStr:=NewStr(AName);
    if not teNames.Search(AStr,APos) then
      teNames.AtInsert(APos,AStr);
    if teNames.Count=1 then teExportName:=AStr;
    InsertName:=APos;
  end;

{****************************************************************************************}
{ Function TTableEntry.GetExportName                                                     }
{----------------------------------------------------------------------------------------}
{ Liefert den Spaltennamen f�r ASCII-Export.                                             }
{----------------------------------------------------------------------------------------}
{ Ergebnis: teExportName                                                                 }
{****************************************************************************************}
Function TTableEntry.GetExportName
   : String;
  begin
    GetExportName:=PToStr(teExportName);
  end;

{****************************************************************************************}
{ Function TTableEntry.SearchName                                                        }
{----------------------------------------------------------------------------------------}
{ Pr�ft, ob AName in der Namensliste enthalten ist.                                      }
{----------------------------------------------------------------------------------------}
{ AName            i = zu suchender Name                                                 }
{----------------------------------------------------------------------------------------}
{ Ergebnis: Position in der Liste, wenn der Name in der Liste enthalten ist, -2 sonst.   }
{****************************************************************************************}
Function TTableEntry.SearchName
   (
   Const AName     : String
   )
   : Integer;
  var AIndex       : Integer;
      BName        : PString;
  begin
    BName:=NewStr(AName);
    if teNames.Search(BName,AIndex) then SearchName:=AIndex
    else SearchName:=-2;
    DisposeStr(BName);
  end;

{****************************************************************************************}
{ Function TTableEntry.IsValid                                                           }
{----------------------------------------------------------------------------------------}
{ Pr�ft, ob kein Leerstring in der Namensliste enthalten ist.                            }
{----------------------------------------------------------------------------------------}
{ Ergebnis: Die Position des Leerstrings in der Namensliste, -1, wenn kein Leerstring    }
{           enthalten.                                                                   }
{****************************************************************************************}
Function TTableEntry.IsValid
   : Integer;
  var Cnt          : Integer;
  begin
    if teNames.Count=0 then IsValid:=0
    else begin
      Cnt:=0;
      while Cnt<teNames.Count do begin
        if DeleteBlanks(PToStr(teNames.At(Cnt)))='' then Break;
        Inc(Cnt);
      end;
      if Cnt=teNames.Count then IsValid:=-1
      else IsValid:=Cnt;
    end;
  end;

{****************************************************************************************}
{ Procedure TTableEntry.SetExportName                                                    }
{----------------------------------------------------------------------------------------}
{ Setzt den ExportNamen auf den Eintrag mit der Nummer AIndex.                           }
{****************************************************************************************}
Procedure TTableEntry.SetExportName
   (
   AIndex          : Integer
   );
  begin
    teExportName:=teNames.At(AIndex);
  end;

{========================================================================================}
{ TTable                                                                                 }
{========================================================================================}

Constructor TTable.Init;
  begin
    inherited Init;
    tblEntries.Init(ciTableEntries,ceTableEntries);
    tblDefault:=0;
    tblDefaultName:=NIL;
  end;

Destructor TTable.Done;
  begin
    tblEntries.Done;
    DisposeStr(tblDefaultName);
    inherited Done;
  end;

{****************************************************************************************}
{ Function TTable.NameToType                                                             }
{----------------------------------------------------------------------------------------}
{ Sucht in ATable nach AName. Wird der Eintrag gefunden, wird der entsprechende          }
{ Spaltentyp zur�ckgegeben, sonst der Defaultwert der Tabelle. Die Gro�-/Klein-          }
{ schreibung von AName wird nicht ber�cksichtigt.                                        }
{----------------------------------------------------------------------------------------}
{ AName            i = gesuchter Spaltenname                                             }
{ Found            o = TRUE, wenn gefunden, FALSE sonst                                  }
{----------------------------------------------------------------------------------------}
{ Ergebnis: zugeh�riger Spaltentyp oder der Defaultwert, wenn nicht in der Liste.        }
{****************************************************************************************}
Function TTable.NameToType
   (
   Const AName     : String;
   var Found       : Boolean
   )
   : LongInt;
  Function DoSearch
     (
     Item          : PTableEntry
     )
     : Boolean; Far;
    begin
      DoSearch:=Item^.SearchName(AName)<>-2;
    end;
  var AEntry       : PTableEntry;
  begin
    AEntry:=tblEntries.FirstThat(@DoSearch);
    if AEntry<>NIL then NameToType:=AEntry^.GetType
    else NameToType:=GetDefault;
    Found:=AEntry<>NIL;
  end;

{****************************************************************************************}
{ Function TTable.NameToTypeName                                                         }
{----------------------------------------------------------------------------------------}
{ Sucht in ATable nach AName. Wird der Eintrag gefunden, wird der entsprechende          }
{ Spaltentyp zur�ckgegeben, sonst der Defaultwert der Tabelle. Die Gro�-/Klein-          }
{ schreibung von AName wird nicht ber�cksichtigt.                                        }
{----------------------------------------------------------------------------------------}
{ AName            i = gesuchter Spaltenname                                             }
{ Found            o = TRUE, wenn gefunden, FALSE sonst                                  }
{----------------------------------------------------------------------------------------}
{ Ergebnis: zugeh�riger Spaltentyp oder der Defaultwert, wenn nicht in der Liste.        }
{****************************************************************************************}
Function TTable.NameToTypeName
   (
   Const AName     : String;
   var Found       : Boolean
   )
   : String;
  Function DoSearch
     (
     Item          : PTableEntry
     )
     : Boolean; Far;
    begin
      DoSearch:=Item^.SearchName(AName)<>-2;
    end;
  var AEntry       : PTableEntry;
  begin
    AEntry:=tblEntries.FirstThat(@DoSearch);
    if AEntry<>NIL then NameToTypeName:=AEntry^.GetTypeName
    else NameToTypeName:=GetDefaultName;
    Found:=AEntry<>NIL;
  end;

{****************************************************************************************}
{ Function TTable.TypeToName                                                             }
{----------------------------------------------------------------------------------------}
{ Liefert den Exportnamen des angegebenen Typs.                                          }
{----------------------------------------------------------------------------------------}
{ AType            i = gesuchter Typ                                                     }
{----------------------------------------------------------------------------------------}
{ Ergebnis: zugeh�riger Exportname, oder Leerstring, wenn AType nicht in der Tabelle.    }
{****************************************************************************************}
Function TTable.TypeToName
   (
   AType           : LongInt
   )
   : String;
  Function DoSearch
     (
     Item          : PTableEntry
     )
     : Boolean; Far;
    begin
      DoSearch:=Item^.GetType=AType;
    end;
  var AEntry       : PTableEntry;
  begin
    AEntry:=tblEntries.FirstThat(@DoSearch);
    if AEntry<>NIL then TypeToName:=AEntry^.GetExportName
    else TypeToName:='';
  end;

{****************************************************************************************}
{ Function TTable.TypeNameToName                                                         }
{----------------------------------------------------------------------------------------}
{ Liefert den Exportnamen des angegebenen Typs.                                          }
{----------------------------------------------------------------------------------------}
{ AType            i = gesuchter Typ                                                     }
{----------------------------------------------------------------------------------------}
{ Ergebnis: zugeh�riger Exportname, oder Leerstring, wenn AType nicht in der Tabelle.    }
{****************************************************************************************}
Function TTable.TypeNameToName
   (
   Const AType     : String
   )
   : String;
  Function DoSearch
     (
     Item          : PTableEntry
     )
     : Boolean; Far;
    begin
      DoSearch:=Item^.GetTypeName=AType;
    end;
  var AEntry       : PTableEntry;
  begin
    AEntry:=tblEntries.FirstThat(@DoSearch);
    if AEntry<>NIL then TypeNameToName:=AEntry^.GetExportName
    else TypeNameToName:='';
  end;

Procedure TTable.InsertEntry
   (
   AEntry          : PTableEntry
   );
  begin
    tblEntries.Insert(AEntry);
  end;

Function TTable.GetDefault
   : Integer;
  begin
    GetDefault:=tblDefault;
  end;

Function TTable.GetDefaultName
   : String;
  begin
    GetDefaultName:=PToStr(tblDefaultName);
  end;

Procedure TTable.SetDefault
   (
   ADefault        : LongInt
   );
  begin
    tblDefault:=ADefault;
  end;

Procedure TTable.SetDefaultName
   (
   Const AName     : String
   );
  begin
    DisposeStr(tblDefaultName);
    tblDefaultName:=NewStr(AName);
  end;

Function TTable.GetEntry
   (
   AType           : Integer
   )
   : PTableEntry;
  Function Search
     (
     Item          : PTableEntry
     )
     : Boolean; Far;
    begin
      Search:=Item^.teType=AType;
    end;
  begin
    GetEntry:=tblEntries.FirstThat(@Search);
  end;

Function TTable.GetEntryAt
   (
   At              : Integer
   )
   : PTableEntry;
  begin
    if (At>-1)
       and (At<tblEntries.Count) then GetEntryAt:=tblEntries.At(At)
    else GetEntryAt:=NIL;
  end;

Function TTable.GetEntryCount
   : Integer;
  begin
    GetEntryCount:=tblEntries.Count;
  end;

{****************************************************************************************}
{ Function TTable.IsValid                                                                }
{----------------------------------------------------------------------------------------}
{ Pr�ft, ob alle Eintr�ge mindestens einen Namen enthalten und kein Leerstring in den    }
{ Tabelleneintr�gen enthalten ist                                                        }
{----------------------------------------------------------------------------------------}
{ Ergebnis: Nummer des Tabelleneintrags der einen Leerstring enth�lt, oder -1,           }
{           wenn keine Leerstrings in der Tabelle.                                       }
{****************************************************************************************}
Function TTable.IsValid
   : Integer;
  var Cnt          : Integer;
  begin
    Cnt:=0;
    while Cnt<GetEntryCount do begin
      if GetEntryAt(Cnt)^.IsValid<>-1 then Break;
      Inc(Cnt);
    end;
    if Cnt=GetEntryCount then IsValid:=-1
    else IsValid:=Cnt;
  end;

Function TTable.SearchTypeName
   (
   Const AName     : String
   )
   : Integer;
  var Cnt          : Integer;
  Function DoSearch
     (
     Item          : PTableEntry
     )
     : Boolean; Far;
    begin
      Inc(Cnt);
      DoSearch:=CompareText(Item^.teTypeName^,AName)=0;
    end;
  begin
    Cnt:=-1;
    if tblEntries.FirstThat(@DoSearch)=NIL then SearchTypeName:=-1
    else SearchTypeName:=Cnt;
  end;

{========================================================================================}
{ TOptionsDlg                                                                            }
{========================================================================================}

Constructor TOptionsDlg.Init
   (
   AParent         : TComponent
   );
  begin
    inherited Init(AParent,'ASCIIOPTIONS');
    FillChar(Tables,SizeOf(Tables),#0);
    CurTableNumber:=-1;
    CurEntry:=NIL;
    ASCIIText:=TREdit.InitResource(Self,idText,255);
    WingisDefault:=TRCheckBox.InitResource(Self,idDefault);
  end;

Function EnumerateFonts
   (
   var LFont       : TLogFont;
   TMetric         : PTextMetric;
   FType           : Integer;
   Data            : PFonts
   )
   : Integer stdcall;
  begin
    if FType or Raster_FontType<>FType then
        SendDlgItemMessage(THandle(Data),idFontList,
            cb_AddString,0,LongInt(@LFont.lfFaceName));
    EnumerateFonts:=1;
  end;

Procedure TOptionsDlg.SetupWindow;
  var AText        : Array[0..255] of Char;
      Cnt          : Integer;
      EnumProc     : TFarProc;
      DC           : THandle;
  begin
    inherited SetupWindow;
    for Cnt:=3554 to 3557 do begin
      GetLangPChar(Cnt,AText,SizeOf(AText));
      SendDlgItemMsg(idTableList,cb_AddString,0,LongInt(@AText));
    end;
    SendDlgItemMsg(idTableList,cb_SetCurSel,0,0);
    EnumProc:=MakeProcInstance(@EnumerateFonts,hInstance);
    DC:=GetDC(0);
    EnumFonts(DC,NIL,EnumProc,Pointer(Handle));
    SendDlgItemMsg(idFontList,cb_SetCurSel,0,0);
    ReleaseDC(0,DC);
    FreeProcInstance(EnumProc);
    SetTable(otnFont);
  end;                        

Function TOptionsDlg.SetTable
    (
    TableNumber    : Integer
    )
    : Boolean;
  var AEnable      : Boolean;
      AEntry       : PTableEntry;
      Cnt          : Integer;
      AText        : Array[0..255] of Char;
  begin
    if CurTableNumber=TableNumber then SetTable:=TRUE
    else begin
      SetTable:=FALSE;
      if Tables[TableNumber]=NIL then begin
        case TableNumber of
          otnFont    : SetTable:=ReadTableFromIni(Handle,TableNames[tnFont],TableOptions[tnFont],Tables[otnFont]);
          otnLine    : SetTable:=ReadTableFromIni(Handle,TableNames[tnLine],TableOptions[tnLine],Tables[otnLine]);
          otnPattern : SetTable:=ReadTableFromIni(Handle,TableNames[tnPattern],TableOptions[tnPattern],Tables[otnPattern]);
          otnColor   : SetTable:=ReadTableFromIni(Handle,TableNames[tnColor],TableOptions[tnColor],Tables[otnColor]);
        end;
      end;
      if Tables[TableNumber]<>NIL then begin
        case TableNumber of
          otnFont    : CurTableOption:=TableOptions[tnFont];
          otnLine    : CurTableOption:=TableOptions[tnLine];
          otnPattern : CurTableOption:=TableOptions[tnPattern];
          otnColor   : CurTableOption:=TableOptions[tnColor];
        end;
        CurTableNumber:=TableNumber;
        CurEntry:=Tables[CurTableNumber]^.GetEntryAt(0);
        if CurTableNumber=otnFont then begin
          WinGISList:=idList3;
          ShowWindow(GetItemHandle(idList1),sw_Hide);
        end
        else begin
          WinGISList:=idList1;              
          ShowWindow(GetItemHandle(idList3),sw_Hide);
        end;
        ShowWindow(GetItemHandle(WinGISList),sw_Show);
        SendDlgItemMsg(WinGISList,lb_ResetContent,0,0); 
        AEnable:=Tables[CurTableNumber]^.GetEntryCount>0;
        if not AEnable then begin
          WingisDefault.UnCheck;
          CheckDlgButton(Handle,idExport,0);
        end;                                    
        EnableWindow(GetItemHandle(idDefault),AEnable);
        EnableWindow(GetItemHandle(idDeleteWinGIS),AEnable and (CurTableNumber=otnFont));
        AEnable:=CurTableNumber=otnFont;
        EnableWindow(GetItemHandle(idFontList),AEnable);
        EnableWindow(GetItemHandle(idInsertWinGIS),AEnable);
        for Cnt:=0 to Tables[CurTableNumber]^.GetEntryCount-1 do begin
          AEntry:=Tables[CurTableNumber]^.GetEntryAt(Cnt);
          if CurTableNumber=otnFont then begin
            StrPCopy(AText,AEntry^.GetTypeName);
            SendDlgItemMsg(WinGISList,lb_AddString,0,LongInt(@AText));
          end
          else SendDlgItemMsg(WinGISList,lb_AddString,0,AEntry^.GetType);
        end;
        SendDlgItemMsg(WinGISList,lb_SetCurSel,0,0);
        SendFocusChange(WinGISList);
        FillASCIIList;
      end;
    end;
  end;

Procedure TOptionsDlg.IDefault
   (
   var Msg         : TWMCommand
   );
  begin
    if (Msg.NotifyCode=bn_Clicked)
        and (CurEntry<>NIL)
        and (WinGisDefault.GetCheck=bf_UnChecked) then begin
      WinGISDefault.Check;
      if CurTableOption and tblHasStrings=0 then Tables[CurTableNumber]^.SetDefault(CurEntry^.GetType)
      else Tables[CurTableNumber]^.SetDefaultName(CurEntry^.GetTypeName)
    end;
  end;

Procedure TOptionsDlg.IExport
   (
   var Msg         : TWMCommand
   );
  begin
    if (Msg.NotifyCode=bn_Clicked)
        and (CurEntry<>NIL)
        and (IsDlgButtonChecked(Handle,idExport)=0) then begin
      CheckDlgButton(Handle,idExport,1);
      CurEntry^.SetExportName(SendDlgItemMsg(idList2,lb_GetCurSel,0,0));
    end;
  end;

Procedure TOptionsDlg.IList1
   (
   var Msg         : TWMCommand
   );
  var AItem        : Integer;
  begin
    if Msg.NotifyCode=lbn_SelChange then begin
      AItem:=SendDlgItemMsg(WinGISList,lb_GetCurSel,0,0);
      CurEntry:=Tables[CurTableNumber]^.GetEntryAt(AItem);
      if CurEntry<>NIL then begin
        if CurTableOption and tblHasStrings=0 then begin
          if CurEntry^.GetType=Tables[CurTableNumber]^.GetDefault then
              WinGisDefault.Check
          else WinGisDefault.UnCheck;
        end
        else begin
          if CurEntry^.GetTypeName=Tables[CurTableNumber]^.GetDefaultName then
              WinGisDefault.Check
          else WinGisDefault.UnCheck;
        end
      end;
      FillASCIIList;
    end;
    DefaultHandler(Msg);
  end;

Procedure TOptionsDlg.IList2
   (
   var Msg         : TWMCommand
   );
  var Text         : PString;
      AItem        : Integer;
      AEnable      : Boolean;
  begin                                     
    if Msg.NotifyCode=lbn_SelChange then begin
      AItem:=SendDlgItemMsg(idList2,lb_GetCurSel,0,0);
      AEnable:=AItem<>-1;
      EnableWindow(GetItemHandle(idDeleteASCII),AEnable);
      EnableWindow(GetItemHandle(idExport),AEnable);
      if AItem<>-1 then begin
        Text:=CurEntry^.teNames.At(AItem);
        if CurEntry^.teExportName=Text then
            CheckDlgButton(Handle,idExport,1)
        else CheckDlgButton(Handle,idExport,0);
      end;
    end;
    DefaultHandler(Msg);
  end;

Procedure TOptionsDlg.IList3
   (
   var Msg         : TWMCommand
   );
  begin
    IList1(Msg);
  end;

Procedure TOptionsDlg.ITableList
   (
   var Msg         : TWMCommand
   );
  begin
    if Msg.NotifyCode=cbn_SelChange then
        SetTable(SendDlgItemMsg(idTableList,cb_GetCurSel,0,0));
    DefaultHandler(Msg);
  end;

Procedure TOptionsDlg.IDeleteASCII
   (
   var Msg         : TWMCommand
   );
  var AItem        : Integer;
      AText        : Array[0..255] of Char;
      AStr         : PString;
  begin
    with CurEntry^ do begin
      AItem:=SendDlgItemMsg(idList2,lb_GetCurSel,0,0);
      AStr:=teNames.At(AItem);
      ASCIIText.SetText(StrPCopy(AText,PToStr(AStr)));
      SendDlgItemMsg(idList2,lb_DeleteString,0,AItem);
      teNames.AtFree(AItem);
      if AItem>=teNames.Count then Dec(AItem);
      if teExportName=AStr then begin
        if AItem=-1 then teExportName:=NIL
        else teExportName:=teNames.At(AItem);
      end;
      SendDlgItemMsg(idList2,lb_SetCurSel,AItem,0);
      SendFocusChange(idList2);
    end;
  end;

Procedure TOptionsDlg.IInsertASCII
   (
   var Msg         : TWMCommand
   );
  var AItem        : Integer;
      AText        : Array[0..255] of Char;
  begin
    if CurEntry<>NIL then begin
      ASCIIText.GetText(AText,SizeOf(AText));
      if CurEntry^.SearchName(StrPas(AText))=-2 then begin
        AItem:=CurEntry^.InsertName(StrPas(AText));
        SendDlgItemMsg(idList2,lb_InsertString,AItem,LongInt(@AText));
        SendDlgItemMsg(idList2,lb_SetCurSel,AItem,0);
        SendFocusChange(idList2);
        ASCIIText.SetText('');
      end;
    end;
  end;

Procedure TOptionsDlg.IDeleteWinGIS
   (
   var Msg         : TWMCommand
   );
  var AItem        : Integer;
  begin
    if (MsgBox(Handle,3559,mb_IconQuestion or mb_YesNo,'')=id_Yes) then begin
      AItem:=SendDlgItemMsg(WinGISList,lb_GetCurSel,0,0);
      Tables[CurTableNumber]^.tblEntries.AtFree(AItem);
      SendDlgItemMsg(WinGISList,lb_DeleteString,AItem,0);
      if AItem>=Tables[CurTableNumber]^.tblEntries.Count then Dec(AItem);
      SendDlgItemMsg(WinGISList,lb_SetCurSel,AItem,0);
      SendFocusChange(WinGISList);
      if AItem=-1 then begin
        EnableWindow(GetItemHandle(idDeleteWinGIS),FALSE);
        WinGisDefault.Disable;
      end;
    end;
  end;

Procedure TOptionsDlg.IInsertWinGIS
   (
   var Msg         : TWMCommand
   );
  var Pos          : Integer;
      AText        : Array[0..255] of Char;
  begin
    GetDlgItemText(Handle,idFontList,@AText,SizeOf(AText));
    Pos:=Tables[CurTableNumber]^.SearchTypeName(StrPas(AText));
    if Pos=-1 then begin
      ASCIIText.Enable;
      Pos:=SendDlgItemMsg(WinGISList,lb_AddString,0,LongInt(@AText));
      Tables[CurTableNumber]^.tblEntries.AtInsert(Pos,New(PTableEntry,InitName(StrPas(AText))));
      if Tables[CurTableNumber]^.tblEntries.Count=1 then begin
        EnableWindow(GetItemHandle(idDeleteWinGIS),TRUE);
        WinGisDefault.Enable;
      end;
    end;
    SendDlgItemMsg(WinGISList,lb_SetCurSel,Pos,0);
    SendFocusChange(WinGISList);
  end;

Procedure TOptionsDlg.IText
   (
   var Msg         : TWMCommand
   );
  begin
    if Msg.NotifyCode=en_Change then
        EnableWindow(GetItemHandle(idInsertAscii),ASCIIText.GetTextLen>0);
  end;

Procedure TOptionsDlg.WMDrawItem
   (
   var Msg         : TWMDrawItem
   );
  begin
    if Msg.DrawItemStruct^.CtlID=idList1 then
        DrawWinGISListItem(Msg.DrawItemStruct);
    DefaultHandler(Msg);
  end;

Procedure TOptionsDlg.DrawWinGISListItem
   (
   Info            : PDrawItemStruct
   );
  var OldObj       : THandle;
      OldBitmap    : THandle;
      BitHandle    : THandle;
      MemDC        : THandle;
      BitMap       : TBitmap;
      YPos         : Integer;
  begin
    with Info^ do begin
      if ItemAction and (oda_Select or oda_DrawEntire)<>0 then begin
        PatBlt(Info^.hDC,rcItem.Left,rcItem.Top,
            18,rcItem.Bottom-rcItem.Top-2,PatCopy);
        if ItemState and ods_Selected<>0 then begin
          BitHandle:=LoadBitmap(ResourceModule,PChar(6004));
          MemDC:=CreateCompatibleDC(hDC);
          OldBitmap:=SelectObject(MemDC,BitHandle);
          GetObject(BitHandle,Sizeof(Bitmap),@Bitmap);
          YPos:=rcItem.Top+(rcItem.Bottom-rcItem.Top-Bitmap.bmHeight) Div 2;
          BitBlt(hDC,3,YPos,Bitmap.bmWidth,Bitmap.bmHeight,MemDC,0,0,srcCopy);
          SelectObject(MemDC,OldBitmap);
          DeleteObject(BitHandle);
          DeleteDC(MemDC);
        end
      end;
      if ItemAction and oda_DrawEntire<>0 then begin
        case CurTableNumber of
          otnLine    : begin
              OldObj:=SelectObject(hDC,GetPen(RGBColors[c_Black],Info^.ItemData,0));
              YPos:=rcItem.Top-1+(rcItem.Bottom-rcItem.Top) Div 2;
              MoveTo(hDC,rcItem.Left+18,YPos);
              LineTo(hDC,rcItem.Right-5,YPos);
              MoveTo(hDC,rcItem.Left+18,YPos+1);
              LineTo(hDC,rcItem.Right-5,YPos+1);
              MoveTo(hDC,rcItem.Left+18,YPos+2);
              LineTo(hDC,rcItem.Right-5,YPos+2);
              DeleteObject(SelectObject(hDC,OldObj));
            end;
          otnColor   : begin
              OldObj:=SelectObject(hDC,GetBrush(RGBColors[ItemData],pt_Solid,NIL));
              PatBlt(Info^.hDC,rcItem.Left+18,rcItem.Top+1,
                  rcItem.Right-rcItem.Left-26,rcItem.Bottom-rcItem.Top-2,PatCopy);
              DeleteObject(SelectObject(hDC,OldObj));
            end;
          otnPattern : begin
              OldObj:=SelectObject(hDC,GetBrush(RGBColors[c_Black],ItemData,NIL));
              PatBlt(Info^.hDC,rcItem.Left+18,rcItem.Top+1,
                  rcItem.Right-rcItem.Left-26,rcItem.Bottom-rcItem.Top-2,PatCopy);
              DeleteObject(SelectObject(hDC,OldObj));
            end;
        end;
      end;
    end;
  end;

Procedure TOptionsDlg.FillASCIIList;
  Procedure DoAll
     (
     Item          : PString
     ); Far;
    var AText      : Array[0..255] of Char;
    begin
      StrPCopy(AText,PToStr(Item));
      SendDlgItemMsg(idList2,lb_AddString,0,LongInt(@AText));
    end;
  var AEnable      : Boolean;
  begin
    SendDlgItemMsg(idList2,lb_ResetContent,0,0);
    if CurEntry<>NIL then begin
      CurEntry^.teNames.ForEach(@DoAll);
      AEnable:=CurEntry^.teNames.Count>0;
    end
    else AEnable:=FALSE;
    EnableWindow(GetItemHandle(idDeleteASCII),AEnable);
    EnableWindow(GetItemHandle(idExport),AEnable);
    AEnable:=CurEntry<>NIL;
    EnableWindow(GetItemHandle(idText),AEnable);
    EnableWindow(GetItemHandle(idInsertASCII),AEnable and (ASCIIText.GetTextLen>0));
    SendDlgItemMsg(idList2,lb_SetCurSel,0,0);
    SendFocusChange(idList2);
  end;

Function TOptionsDlg.WriteTables
   : Boolean;
  begin
    WriteTables:=WriteTableToIni(Handle,TableNames[tnFont],TableOptions[tnFont],Tables[otnFont])
        and WriteTableToIni(Handle,TableNames[tnLine],TableOptions[tnLine],Tables[otnLine])
        and WriteTableToIni(Handle,TableNames[tnPattern],TableOptions[tnPattern],Tables[otnPattern])
        and WriteTableToIni(Handle,TableNames[tnColor],TableOptions[tnColor],Tables[otnColor]);
  end;

Function TOptionsDlg.CheckTable
   (
   ATable          : Integer
   )
   : Boolean;
  var ErrorItem    : Integer;
      AEntry       : PTableEntry;
  begin
    CheckTable:=FALSE;
    if Tables[ATable]=NIL then CheckTable:=TRUE
    else begin
      ErrorItem:=Tables[ATable]^.IsValid;
      if ErrorItem<>-1 then begin
        AEntry:=Tables[ATable]^.GetEntryAt(ErrorItem);
        if AEntry^.teNames.Count=0 then MsgBox(Handle,3561,mb_IconExclamation or mb_OK,'')
        else MsgBox(Handle,3560,mb_IconExclamation or mb_OK,'');
        SetTable(ATable);
        SendDlgItemMsg(WinGISList,lb_SetCurSel,ErrorItem,0);
        SendFocusChange(idList1);
        SendDlgItemMsg(idList2,lb_SetCurSel,AEntry^.IsValid,0);
      end
      else CheckTable:=TRUE;
    end;
  end;                            

Function TOptionsDlg.CanClose
   : Boolean;
  begin
    CanClose:=FALSE;
    if CheckTable(otnFont)
        and CheckTable(otnLine)
        and CheckTable(otnPattern)
        and CheckTable(otnColor) then begin
      WriteTables;
      CanClose:=TRUE;
    end;
  end;

Procedure TOptionsDlg.SendFocusChange
   (
   AId             : Integer
   );
  begin
    SendMessage(Handle,wm_Command,MakeLong(AId,lbn_SelChange),GetItemHandle(AId));
  end;

{========================================================================================}
{ TSymConvert                                                                               }
{========================================================================================}

Constructor TSymConvert.Init
   (
   AASCII          : LongInt;
   AWinGIS         : LongInt
   );
  begin
    inherited Init(AASCII);
    WinGisNumber:=AWinGIS;
  end;

{========================================================================================}
{ TSymConvertList                                                                           }
{========================================================================================}

Function TSymConvertList.KeyOf
   (
   Item            : Pointer
   )
   : Pointer;
  begin
    KeyOf:=Pointer(PSymConvert(Item)^.Index);
  end;

Function TSymConvertList.Compare
   (
   Key1            : Pointer;
   Key2            : Pointer
   )
   : Integer;
  begin
    if LongInt(Key1) < PLongInt(Key2)^ then Compare:=-1
    else if LongInt(Key1) > PLongInt(Key2)^ then Compare:=1
    else Compare:=0;
  end;

{========================================================================================}
{ TExportDlg                                                                             }
{========================================================================================}

Constructor TExportDlg.Init
   (
   AParent              : TComponent;
   AName                : PChar;
   AData                : PASCIIData;
   ALayers              : PLayers;
   APInfo               : PPaint
   );
  begin
    inherited Init(AParent,AName,AData,ALayers,APInfo);
    IsImport:=FALSE;
    HelpContext:=1030;
  end;

Procedure TExportDlg.SetupWindow;
  begin
    inherited SetupWindow;
    with Data^ do begin
      SetFieldText(idASCIIFile,ASCIIFile);
      SetFieldText(idADFFile,ADFFile);
      SetFieldText(idSymbolFile,SymbolFile);
      FillLayerList(idLayerList,TRUE,FALSE);
      if (ExportWhat=exSelected)
          and (Layers^.SelLayer^.Data^.GetCount=0) then ExportWhat:=exProject;
{$IFNDEF MRING}
      CheckRadioButton(Handle,idProject,idSelected,idProject+ExportWhat);
{$ENDIF}
      if ExportLayers<>NIL then SetMultiLayerSelection(idLayerList,ExportLayers,TRUE);
      if ExportWhat<>exLayers then EnableWindow(GetItemHandle(idLayerList),FALSE);
      if Layers^.SelLayer^.Data^.GetCount=0 then EnableWindow(GetItemHandle(idSelected),FALSE);
{$IFDEF MRING}
      if SendDlgItemMsg(idLayers,bm_GetCheck,0,0)=0 then SendDlgItemMsg(idLayers,bm_SetCheck,1,0);
      EnableWindow(GetItemHandle(idLayerList),TRUE);
      SendDlgItemMsg(idLayerList,CB_SETCURSEL,0,0);
{$ENDIF}
    end;
  end;

Procedure TExportDlg.WMCommand
   (
   var Msg         : TWMCommand
   );
  begin
    inherited WMCommand(Msg);
    case Msg.NotifyCode of
      bn_Clicked   : case Msg.ItemID of
          idProject,
          idSelected  : EnableWindow(GetItemHandle(idLayerList),FALSE);
          idLayers    : EnableWindow(GetItemHandle(idLayerList),TRUE);
        end;
    end;
  end;

Function TExportDlg.CanClose
   : Boolean;
  var ASCIIName    : Array[0..255] of Char;
      ADFName      : Array[0..255] of Char;
      SymName      : Array[0..255] of Char;
  Function CheckSelectedLayers
     : Boolean;
    begin
      CheckSelectedLayers:=TRUE;
      if IsDlgButtonChecked(Handle,idLayers)=1 then begin
        if SendDlgItemMsg(idLayerList,lb_GetSelCount,0,0)=0 then begin
          MsgBox(Handle,3571,mb_IconExclamation or mb_OK,'');
          SetFocus(GetItemHandle(idLayerList));
          CheckSelectedLayers:=FALSE;
        end;
      end;
    end;
  begin
    CanClose:=FALSE;
{$IFNDEF MRING}
    if CheckFile(idASCIIFile,ASCIIName,TRUE)
        and CheckFile(idADFFile,ADFName,FALSE)
        and CheckFile(idSymbolFile,SymName,FALSE)
        and CheckSelectedLayers then with Data^ do begin
{$ENDIF}
{$IFDEF MRING}
     if CheckFile(idASCIIFile,ASCIIName,TRUE)
        and CheckSelectedLayers then with Data^ do begin
{$ENDIF}
      ASCIIFile:=StrPas(ASCIIName);
      ADFFile:=StrPas(ADFName);
      SymbolFile:=StrPas(SymName);
      if IsDlgButtonChecked(Handle,idSelected)=1 then ExportWhat:=exSelected
      else if IsDlgButtonChecked(Handle,idLayers)=1 then begin
        if ExportLayers<>NIL then Dispose(ExportLayers,Done);
        ExportLayers:=New(PLongColl,Init(5,5));
        GetMultiLayerSelection(idLayerList,TRUE,ExportLayers);
        ExportWhat:=exLayers;
      end
      else ExportWhat:=exProject;
      CanClose:=TRUE;
    end;
  end;

{========================================================================================}
{ TLogDlg                                                                                }
{========================================================================================}

Procedure TLogDlg.AddString
   (
   Const AText     : String;
   AddLine         : Boolean
   );
  var AStr         : String;
  begin
    AStr:=AText+#0;
    SendDlgItemMsg(idLogList,lb_AddString,0,LongInt(@AStr[1]));
    if AddLine then AddBlankLine
    else SendDlgItemMsg(idLogList,lb_SetCurSel,SendDlgItemMsg(idLogList,lb_GetCount,0,0)-1,0);
    UpdateWindow(GetItemHandle(idLogList));
  end;

Procedure TLogDlg.AddText
   (
   AText           : Integer;
   myParams          : Array of Const;
   AddLine         : Boolean
   );
  var AStr         : String;
  begin
    try
      AStr:=Format(GetLangText(AText),myParams)+#0;
    except
      AStr:=GetLangText(AText);
    end;
    SendDlgItemMsg(idLogList,lb_AddString,0,LongInt(@AStr[1]));
    if AddLine then AddBlankLine
    else SendDlgItemMsg(idLogList,lb_SetCurSel,SendDlgItemMsg(idLogList,lb_GetCount,0,0)-1,0);
    UpdateWindow(GetItemHandle(idLogList));
  end;

Procedure TLogDlg.AddBlankLine;
  var AChar        : Char;
  begin
    AChar:=#0;
    SendDlgItemMsg(idLogList,lb_AddString,0,LongInt(@AChar));
    SendDlgItemMsg(idLogList,lb_SetCurSel,SendDlgItemMsg(idLogList,lb_GetCount,0,0)-1,0);
  end;

Procedure TLogDlg.SetupWindow;
  begin
    SetWindowText(Handle,Title);
    inherited SetupWindow;
{!?!?    SendDlgItemMsg(idLogList,wm_SetFont,SmallFont,1);}
  end;

Procedure TLogDlg.ISave
   (
   var Msg         : TMessage
   );
  var AFName       : Array[0..255] of Char;
      OutFile      : TextFile;
      Cnt          : LongInt;
      Dialog       : TSaveDialog;
  begin
    Dialog:=TSaveDialog.Create(Self);
    Dialog.Title:=GetLangText(3575);
    Dialog.Filter:=GetLangText(3549);
    if Dialog.Execute then begin
      StrPCopy(AFName,Dialog.FileName);  
      if not CheckFileExist(AFName)
          or (MsgBox(Handle,1006,mb_IconQuestion or mb_YesNo,StrPas(AFName))=idYes) then begin
        AssignFile(OutFile,AFName);
        Rewrite(OutFile);
        if IOResult<>0 then MsgBox(Handle,3576,mb_IconExclamation or mb_OK,StrPas(AFName))
        else begin
          for Cnt:=0 to SendDlgItemMsg(idLogList,lb_GetCount,0,0)-1 do begin
            SendDlgItemMsg(idLogList,lb_GetText,Cnt,LongInt(@AFName));
            Writeln(OutFile,AFName);
            if IOResult<>0 then MsgBox(Handle,3577,mb_IconExclamation or mb_OK,StrPas(AFName));
          end;
          CloseFile(OutFile);
        end;
      end;
    end;
    Dialog.Free;
  end;

Constructor TLogDlg.Init
   (
   AParent         : TComponent;
   ATitle          : PChar
   );
  begin
    inherited Init(AParent,'ASCIILOG',0);
    Title:=ATitle;
  end;

{========================================================================================}
{ TStringColl                                                                            }
{========================================================================================}

Function TStringColl.Compare
   (
   Key1            : Pointer;
   Key2            : Pointer
   )
   : Integer;
  begin
    Compare:=CompareText(PString(Key1)^,PString(Key2)^);
  end;

{========================================================================================}
{ TDBDataItem                                                                            }
{========================================================================================}

Constructor TDBListItem.Init
   (
   AData           : PChar
   );
  var Source       : PChar;
      IndexStr     : String;
      Error        : Integer;
  begin
    inherited Init(0);
    Source:=ReadDDEItem(AData,IndexStr);
    if Source=NIL then Fail
    else begin
      Source:=ReadDDEItem(Source,IndexStr);
      if Source=NIL then Fail
      else begin
        Val(DeleteBlanks(IndexStr),Index,Error);
        if (Error<>0)
            or (Index=0) then Fail
        else dbData:=StrNew(Source);
      end;
    end;
  end;

Destructor TDBListItem.Done;
  begin
    StrDispose(dbData);
    inherited Done;
  end;

{========================================================================================}
{ TOffsetDlg                                                                             }
{========================================================================================}

Constructor TOffsetDlg.Init
   (
   AParent         : TComponent;
   AData           : POffsetData;
   ASystem         : Integer
   );
  begin
    inherited Init(AParent,'ASCIIOFFSET');
    Data:=AData;
    CoordinateSystem:=ASystem;
  end;

Procedure TOffsetDlg.SetupWindow;
  var AStr         : String;
      APos         : Integer;
      Cnt          : Integer;
  begin
    inherited SetupWindow;
    Str(Data^.Scale:0:10,AStr);
    APos:=Pos('.',AStr)+2;
    Cnt:=Length(AStr);
    while (Cnt>APos)
        and (AStr[Cnt]='0') do Dec(Cnt);
    Delete(AStr,Cnt+1,Length(AStr)-Cnt);     
    AStr:=AStr+#0;
    SetDlgItemText(Handle,idScale,@AStr[1]);
    SetFieldLongZero(idXOffset,Data^.XOffset);
    SetFieldLongZero(idYOffset,Data^.YOffset);
    if CoordinateSystem=Integer(ctGeodatic) then begin
      SetDlgItemText(Handle,idXOfs,GetLangPChar(3704,@AStr,SizeOf(AStr)));
      SetDlgItemText(Handle,idYOfs,GetLangPChar(3703,@AStr,SizeOf(AStr)));
    end
    else begin
      SetDlgItemText(Handle,idXOfs,GetLangPChar(3703,@AStr,SizeOf(AStr)));
      SetDlgItemText(Handle,idYOfs,GetLangPChar(3704,@AStr,SizeOf(AStr)));
    end;
  end;

Function TOffsetDlg.CanClose
   : Boolean;
  var AX           : LongInt;
      AY           : LongInt;
      AStr         : Array[0..50] of Char;
      Error        : Integer;
      AScale       : Real;
  begin
    CanClose:=FALSE;
    GetDlgItemText(Handle,idScale,AStr,SizeOf(AStr));
    Val(StrPas(AStr),AScale,Error);
    if (Error<>0)
        or (AScale<=0) then begin
      MsgBox(Handle,3705,mb_IconExclamation or mb_OK,'');
      SetFocus(GetItemHandle(idScale));
    end
    else if GetFieldLongZero(idXOffset,AX)
        and GetFieldLongZero(idYOffset,AY) then begin
      Data^.XOffset:=AX;
      Data^.YOffset:=AY;
      Data^.Scale:=AScale;
      CanClose:=TRUE;
    end;
  end;

end.