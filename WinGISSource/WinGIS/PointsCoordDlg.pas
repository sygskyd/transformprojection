{******************************************************************************+
  Unit PointsCoordDialog
--------------------------------------------------------------------------------
  Autor: Vladimir Brovak, Progis Russia
--------------------------------------------------------------------------------
  Dialog to edit a single Object's construct point (poly,arc,ellipses)
--------------------------------------------------------------------------------
  Release: 1

March-April 2001. Dialog working ONLY for Input (Drawing ) /Edit Polylines and
            Polygons (Insert,Move Point)
            Also, it work in Object Properties for Poly, when SINGLE one Poly selected
            (or Snapped thru DoubleClick)
--------------------------------------------------------------------------------
  Release: 2

May  2001.  Now Dialog working also with  for Input (Drawing ) /Edit Circles
            Also, it work in Object Properties for Circle, when SINGLE one Circle selected
--------------------------------------------------------------------------------
  Release: 3

Jun  2001.  Now Dialog working also with  for Input (Drawing )/Edit Arcs,Points,Texts,Symbols
            Also, it work in Object Properties, when SINGLE object selected

            Points and Symbols can multiplie insert to project

            Data in PointStringGrid right-aligned now
--------------------------------------------------------------------------------
 Release: 4

 Jul 2001. Now Dialog include support both WinGIS coordinate system
 --------------------------------------------------------------------------------

 Release: 5

 September-October 2001. Now Dialog include Loading coordinates from delimeted file;
 Also erase all points thru one button possible
 --------------------------------------------------------------------------------
 Release: 6

 October 2001. Now special mode added to Dialog - insert points with "ortho" mode
 --------------------------------------------------------------------------------

 Release: 7

 October-November 2001. Now added switch meters/geograph coordinate.
   We will use Coord32.dll for Calculate It.
   This mode work for the time present ONLY for coordinates in stringgrid

   User can load geographical coordinates from file too. Format as Float -
   before "," Degree; after it - Minute
 ----------------------------------------------------------------------------------------------
Release: 8

 December 2001 - Jan 2001. Now added Load _various_ Symbols from text-file from _various_ libraryes
 ----------------------------------------------------------------------------------------------
Release: 9

 Jan 2001. Now change mode for work with coordinates - it will use AxGisPro.ocx
 ----------------------------------------------------------------------------------------------



*WARNING !*   Dialog now in hard-work-test mode, please don't change source code
             it without notify of me. Comments welcome. Thanks
*ATTENTION!*  e-mail: brovak@progis.ru

+******************************************************************************}

unit PointsCoordDlg;

interface

uses AM_Def, MultiLng, StdCtrls, Grids, Controls, ExtCtrls, Am_index, PointsCoordDlg1, PointsCoordDlg2,
  Validate, ComCtrls, Buttons, Classes, WCtrls, MenuHndl, InpHndl, AM_Input, Sysutils, Windows, Am_Ini,
  dialogs, graphics, grtools, Am_poly, am_cpoly, forms, am_View, am_circl, Am_Point, am_sym, Am_text, OleCtrls, AxGisPro_TLB, Math, Lists,
  Menus;

type TInputCoordinateType = (ictMetric, ictdegree);

type
  TPointsCoordDialog = class(TWForm)
    Bevel1: TBevel;
    OKBtn: TButton;
    CancelBtn: TButton;
    ControlsPanel: TPanel;
    TypeCombo: TComboBox;
    MlgSection: TMlgSection;
    Label1: TLabel;
    SpecificCheckBox: TCheckBox;
    SwitchBtn: TButton;
    Label5: TLabel;
    ControlsPanel1: TPanel;
    GroupBox1: TGroupBox;
    Edit4: TEdit;
    Edit5: TEdit;
    Label6: TLabel;
    Label7: TLabel;
    InsBtn: TButton;
    AddBtn: TButton;
    DelBtn: TButton;
    Panel1: TPanel;
    CheckBox1: TCheckBox;
    PointCombo: TComboBox;
    Label2: TLabel;
    ModeCombo: TComboBox;
    ApplyBtn: TButton;
    Label3: TLabel;
    Edit1: TEdit;
    Label4: TLabel;
    Label8: TLabel;
    Edit2: TEdit;
    Edit3: TEdit;
    HeaderControl1: THeaderControl;
    Panel2: TPanel;
    PointsStringGrid: TStringGrid;
    AngleValidator: TFloatValidator;
    AltEdit1: TEdit;
    AltEdit2: TEdit;
    AltLabel1: TLabel;
    AltLabel2: TLabel;
    FloatValidator1: TFloatValidator;
    FloatValidator2: TFloatValidator;
    AngleValidator2: TFloatValidator;
    ComboBox1: TComboBox;
    AltEdit3: TEdit;
    AngleValidator3: TFloatValidator;
    Label9: TLabel;
    AltLabel3: TLabel;
    ALtlabel3a: TLabel;
    Label10: TLabel;
    DelAllBtn: TSpeedButton;
    FileButton: TSpeedButton;
    OrtoBtn: TSpeedButton;
    RadioGroup1: TRadioGroup;
    CoordTypeBtn: TSpeedButton;
    StringEdit: TEdit;
    GradusEdit: TEdit;
    SecondEdit: TEdit;
    MinuteEdit: TEdit;
    CoordBtn2: TButton;
    GradusEdit1: TEdit;
    MinuteEdit1: TEdit;
    SecondEdit1: TEdit;
    StringEditValid: TFloatValidator;
    IgnoreGrid: TStringGrid;
    ReplaceGrid: TStringGrid;
    PopupMenu: TPopupMenu;
    PMSave: TMenuItem;
    SaveDialog: TSaveDialog;

    procedure FormShow(Sender: TObject);
    procedure SwitchMode; //switch Simply/Advance mode
    procedure SwitchBtnClick(Sender: TObject);
    procedure Clear;
    function FormatIt(s: double): string;
    function FormatIt1(s: double): string;
    function FormatIt2(s: double): string;
    procedure PrepareGrid(AProj: Pointer; intObjecttype: word; AFixedPointsRow: integer);
    procedure SpecificCheckBoxClick(Sender: TObject);
    procedure PointsStringGridDrawCell(Sender: TObject; ACol,
      ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure PointsStringGridSelectCell(Sender: TObject; ACol,
      ARow: Integer; var CanSelect: Boolean);
    function CheckEdit(StringText: string; Sender: TObject): boolean;
    procedure PreparePointsGrid;
    procedure InsBtnClick(Sender: TObject);

    procedure UpdateTitle;
    procedure AddBtnClick(Sender: TObject);
    procedure DelBtnClick(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure ModeComboChange(Sender: TObject);
    procedure ChangeCombo;
    procedure CheckGridCount;
    procedure SetGridAndEditOptions(GridMode: integer; FirstTitle, SecondTitle, THirdTitle: string);
    procedure PointComboChange(Sender: TObject);
    procedure TypeComboChange(Sender: TObject);
    procedure CalculateAdditional;
    procedure SetButton(InsBut, AddBut, DelBut: boolean);
    procedure PointComboClick(Sender: TObject);
    procedure Edit2Exit(Sender: TObject);
    procedure EditExit(Sender: TObject);
    procedure EditExit1(Sender: TObject);
    procedure PointsStringGridSetEditText(Sender: TObject; ACol,
      ARow: Integer; const Value: string);
    procedure PointsStringGridGetEditText(Sender: TObject; ACol,
      ARow: Integer; var Value: string);
    procedure PointsStringGridExit(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure ResetCell(OldCol, OldRow: integer);
    procedure ApplyBtnClick(Sender: TObject);
    procedure SetOK;
    function CheckArrayPoints: boolean;
    function SetNewCoordinates: boolean;
    procedure SetNewCoordForEdit;

    function DetectFramePoint: integer;
    function CheckAvMode: boolean;
    procedure SetCutGrid(EditCount: integer);

    procedure SetInputCircleModeWithInputPointCounts(InputCount: integer);
    procedure SetInputArcModeWithInputPointCounts(InputCount: integer);
    procedure SetInputTExtMode(InputCount: integer);

    procedure FillCircleDataToGrid;
    procedure FillArcDataToGrid;
    procedure FloatValidator1Error(Sender: TObject; ErrorCode: Word);
    procedure TypeComboClick(Sender: TObject);
    procedure FileButtonClick(Sender: TObject);
    function LoadDataFromFile: boolean;
    procedure DelAllBtnClick(Sender: TObject);
    procedure PointsStringGridMouseUp(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure OrtoBtnClick(Sender: TObject);
    procedure CoordTypeBtnClick(Sender: TObject);
    procedure StringEditExit(Sender: TObject);
    procedure PointsStringGridClick(Sender: TObject);
    procedure ResetPoints(Flag: boolean);
    function InputDegreeMode(ADataString: string; ADegreeMode: integer): double;
    procedure InputDegreeMode1(ADataString: string; var Gradus, Minute, Second: string);
    function OutputDegreeMode(ADataFloat: double): string;
    function OutputDegreeMode1: string;
    function OutputDegreeMode2: string;
    procedure FormDestroy(Sender: TObject);
    procedure MinuteEditExit(Sender: TObject);
    procedure SecondEditExit(Sender: TObject);
    procedure InvisibleGradus;
    procedure InvisibleGradus1;
    procedure RadioGroup1Click(Sender: TObject);
    procedure ModeComboClick(Sender: TObject);
    procedure Edit1Click(Sender: TObject);
    procedure Edit2Click(Sender: TObject);
    procedure Edit3Click(Sender: TObject);
    procedure Edit4Click(Sender: TObject);
    procedure Edit5Click(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure PointsStringGridRowMoved(Sender: TObject; FromIndex,
      ToIndex: Integer);
    procedure PointsStringGridColumnMoved(Sender: TObject; FromIndex,
      ToIndex: Integer);
    procedure PointsStringGridTopLeftChanged(Sender: TObject);
    procedure GradusEditExit(Sender: TObject);
    procedure GradusEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MinuteEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SecondEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CoordBtn2Click(Sender: TObject);
    procedure GradusEdit1Exit(Sender: TObject);
    procedure GradusEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MinuteEdit1Exit(Sender: TObject);
    procedure MinuteEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SecondEdit1Exit(Sender: TObject);
    procedure SecondEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edit4MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Edit5MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure PMSaveClick(Sender: TObject);
  private
    TmpInputMode, TmpDegreeMode, NowEdit: integer;
    function PrepareCircleForInsert: integer;
    function PrepareArcForInsert: integer;
    procedure SetText(AltLabel1Text, AltLabel2Text, PointsCoordGridFirst: string);

    procedure SetTmpArray(i: integer);
    procedure SetTmpArray1(i: integer);

    procedure RecalculateCirlcle(i: integer);
    function GetXYFromGrid(i: integer): TDpoint;
    function GetXYFromGrid1(i: integer): TGrpoint;
    procedure SetCircleParamsForFirstSecond;
    function CheckParamsOfEditCircleBeforeApplyIt: boolean;
    function ReturnCorrected(D: double): integer;
    procedure Deleteprocess;
    procedure Deleteprocess1;

    procedure DegressToMetric;
    procedure MetricToDegress;
    procedure SetProjection(var X, Y: double);
    procedure SimplyModeGradusEdit(Sender: TOBject);
    procedure TmpClosed;
    procedure TmpClosed1;
  public
    Project: pointer;
    SimplyMode: boolean; // true  - only x,y input
    ObjectType: longword; // type of editable object
    EditMode: boolean; // Input or Edit Mode of object
    SetPreset: boolean; // if true then existing points presented
    ActiveMode: integer; // Active  mode from combobox                      set
    FixedPointsRow: integer; //if -1 then not fixed
    MenuHandler: TMenuHandler;
    OldRow, OLdCol: integer; //previous pos
    LastApplyPoint: string; //previous apply point
    LastInputString: integer; //last input string with coords
    PrevPoint, NextPoint: integer; //row frames of current row
    FirstSwitch: boolean;
    CircleType: integer; //for circle only
    ArcType: integer; //for arc only
    MaxPointsInThisMode: integer; //max count of point in curren mode
    CircleCenter: TDPoint;
    CircleRadius: longint;
    OldCircleModeType, OldArcModeType: integer;
    TmpOld: Pindex;
    MlgSection24, MlgSection8, MlgSection71, MlgSection76: string;
    FileName: AnsiString;
    CoordInputType: TInputCoordinateType;
    transform_pointer: pointer;
    EditBuffer: string;
    SetNewData: boolean;
    BadFlag: string;
    IsFormShow: boolean;
    SymlibList: TIntList;
    IsLoadingNow: boolean;
    StartLibIndex: integer;
    AxGISProjection1: TAxGisProjection;

    SpecialOrthoActive: boolean;
    ZBase: TDPoint;
  end;
var PointsCoordDialog: TPointsCoordDialog;

function SToI(s: AnsiString): Integer;

implementation

{$R *.DFM}

uses AM_Proj, attachhndl, am_main, am_projp, CoordinateSystem, userIntf,WinOSInfo,
  TextUtils, SymLib, extLib, am_obj, am_font, Am_Group, SymbolSelDlg, SymRol, MenuFn;

var
  TmpArray: TVladType;
  TmpArray1: TVladType1;
  Koeff: double;
  Z1: TPointsCoordDialog1;
  Z2: TPointsCoordDialog2;
  SetFlagZ: boolean;

  {+}
  ProjectActSymbolLibrary: string;
  ProjectActSymbol: string;
  KolonSymbolName: integer;
//  ProjectSymbolActualSize
//  ProjectSymbolSizeUNits
//  ProjectSymbolArray
  {-}

  ASymLibInfo: TSymLibInfo;
  ASymInfo: TSymInfo;
  ASymbols: PSymbols;
  FFonts: Pfonts;
  InsertSym: PSGroup;
 //clear and prepare basic elements of Dialog


function SToI(s: AnsiString): Integer;
var
d: Extended;
i: Int64;
begin
     d:=StrToFloat(s);
     d:=d*10000.0;
     i:=Trunc(d);
     i:=i div 100;
     Result:=i;
end;

function GetPositionOfSymbolsSet(ASymLibInfo: TSymLibInfo; sLibraryName: AnsiString): Integer;
var
  iI: Integer;
  m: ansistring;

begin
  M := sLibraryName;
  if M = '' then M := MlgStringList['SymbolRollup', 17];

  for iI := 0 to ASymLibInfo.LibCount - 1 do begin
    if AnsiUpperCase(ASymLibInfo.GetLibName(ii)) = AnsiUpperCase(M)
      then
    begin;
      RESULT := iI;
      EXIT;
    end;
  end; // For iI end
end;



procedure TPointsCoordDialog.Clear;
begin;
  PointsStringGrid.RowCount := 1;
  PointsStringGrid.ColCount := 3;
  PointCombo.Items.Clear;
  ModeCombo.Items.Clear;
  Edit1.Text := '';
  Edit2.Text := '';
  Edit3.Text := '';
  SpecificCheckBox.Checked := false;
  Panel1.Visible := false;
  PointsStringGrid.EditorMode := true;
  TypeCombo.Items.Clear;
  if (not (ObjectType = mn_New_Circle)) and
    (not (ObjectType = mn_Edit_Circle)) and
    (not (ObjectType = mn_Edit_Arc)) and
    (not (ObjectType = mn_New_Arc)) then
    TypeCombo.Items.Add(MlgSection[27]);
end;

//filling grid by Array of Points

procedure TPointsCoordDialog.PrepareGrid(AProj: Pointer; IntObjecttype: word; AFixedPointsRow: integer);
var
  i, j, j1: integer;
  A: double;
  RealCount: integer;
  key: word;
//  ADatum:PDatum;
//  AProjection:PProjection;
  M: string;
begin
  Project := Aproj;
  IsFormShow := false;
  TmpClosed; //!!!!!!!!!!!!!!
  MaxPointsInThisMode := 32000;
  PointCombo.Font.Color := clGray;
  FileName := '';
  CoordInputType := ictMetric;
  StringEdit.Font := PointsStringGrid.Font;
  GradusEdit.Font := StringEdit.Font;
  MinuteEdit.Font := StringEdit.Font;
  SecondEdit.Font := StringEdit.Font;
  IsLoadingNow := false;
  SetNewData := true;
 // TmpClosed1;
  if Inifile.LoadPointsCoordDataFromFile.InputMode = 1
    then TmpInputMode := 0;
//  Inifile.LoadPointsCoordDataFromFile.InputMode:=1;
  TmpDegreeMode := Inifile.LoadPointsCoordDataFromFile.DegreeMode;
//     create_transform(transform_pointer);
   {++Brovak}
  if AxGisProjection1 = nil then
  begin
    AxGisProjection1 := TAxGisProjection.Create(nil);
    AxGisProjection1.WorkingPath := OSInfo.WingisDir;
    AxGisProjection1.LngCode := '044';
  end;
  {--Brovak}

  Screen.Cursor := crHourGlass;
  MlgSection24 := MlgSection[24]; //Pnt. N
  MlgSection8 := MlgSection[8];
  MlgSection71 := MlgSection[71];
  MlgSection76 := MlgSection[76];

  TmpOld := PProj(AProj)^.OldSnapItem;
  ObjectType := intObjectType;
  FixedPointsRow := AFixedPointsRow;
  Clear;

  PointsStringGrid.Col := 1;

 //prepare grid and related edit
  PointsStringGrid.Cells[1, 0] := FormatIt(0);
  PointsStringGrid.Cells[2, 0] := FormatIt(0);
  Edit1.Text := FormatIt(0);
  Edit2.Text := FormatIt(0);
  Edit4.Text := FormatIt(0);
  Edit5.Text := FormatIt(0);
  Koeff := PixelsPerInch / 120;
 //ObjectType NOT types from AM_DEF (mn_ or ot_), it is variable for calling this function
 //  with special parameters for current mode! It is may be AM_DEF mode, but can others

  if PProj(Project).PInfo.CoordinateSystem.CoordinateType = ctGeodatic then
  begin;
    HeaderControl1.Sections[1].Text := MlgSection[21];
    HeaderControl1.Sections[2].Text := MlgSection[20];
    Label6.Caption := MlgSection[44];
    Label7.Caption := MlgSection[43];
  end;

  case ObjectType of

    ot_Pixel: //Point object input
      begin;
        Self.Caption := MlgSection[81];
        PointsStringGrid.Cells[0, 0] := MlgSection24 + ' 1';
        TypeCombo.Enabled := false;
        Label1.Font.Color := clGray;
        ModeCombo.Items.Add(MlgSection[12]);
        ModeCombo.Items.Add(MlgSection[13]);
        ModeCombo.ItemIndex := 0;
        SetButton(true, true, false);
        UpdateTitle;
      end;

    ot_Symbol: //Symbol object input
      begin;
        TypeCombo.Enabled := false;
        ProjectActSymbol := PProj(WingisMainForm.ActualProj).ActualSym.GetName;
        M := PProj(Project).ActualSym.GetLibName;
        if M = '' then M := MlgStringList.Strings['SymbolRollup', 17];
        ProjectActSymbolLibrary := M;
        FFonts := PProj(Project).PInfo^.Fonts;
        ASymbols := PSymbols(PProj(Project).PInfo^.Symbols);
        ASymLibInfo := TSymLibInfo.Create(PProj(Project), ASymbols, M);
        SymlibList := TIntList.Create;
        StartLibIndex := GetPositionOfSymbolsSet(ASymLibInfo, ProjectActSymbolLibrary);
        SymlibList.Add(StartLibIndex);
        Self.Caption := MlgSection[82];
        PointsStringGrid.Cells[0, 0] := ProjectActSymbol;
        Label1.Font.Color := clGray;
        ModeCombo.Items.Add(MlgSection[12]);
        ModeCombo.Items.Add(MlgSection[13]);
        ModeCombo.ItemIndex := 0;
        SetButton(true, true, false);
        UpdateTitle;




      end;

    ot_Text: // Text object input
      begin;
        Self.Caption := MlgSection[80];
        PointsStringGrid.Cells[0, 0] := MlgSection24 + ' 1';
        MaxPointsInThisMode := 2;
        TypeCombo.Enabled := false;
        Label1.Font.Color := clGray;
        FloatValidator1.MinValue := -999999999;
        FloatValidator2.MinValue := -999999999;
        ModeCombo.Items.Add(MlgSection[12]);
        ModeCombo.Items.Add(MlgSection[13]);
        ModeCombo.ItemIndex := 0;
        SetButton(true, true, false);
        if PProj(Project).PInfo.CoordinateSystem.CoordinateType = ctGeodatic then
          SetText(MlgSection[42], MlgSection[41], '')
        else
          SetText(MlgSection[41], MlgSection[42], '');

        SetCutGrid(1);
        if PProj(AProj)^.TextRect.IsOn = true then
          with PProj(AProj)^.TextRect^ do
          begin;
            PointsStringGrid.Cells[1, 0] := FormatIt(StartPos.X / 100);
            PointsStringGrid.Cells[2, 0] := FormatIt(StartPos.Y / 100);
          end;
        UpdateTitle;
      end;

    mn_Edit_Point: // Edit Point's coordinates
      begin;
        SetButton(false, false, false);
        TypeCombo.Enabled := false;
        SpecificCheckBox.Enabled := false;
        Label1.Font.Color := clGray;
        Label5.Caption := MlgSection[69];
        with PProj(AProj)^ do
          with PPixel(OldSnapItem.mptr)^ do
          begin;
            PointsStringGrid.Cells[0, 0] := MlgSection[70];
            PointsStringGrid.Cells[1, 0] := FormatIt(Position.X / 100);
            PointsStringGrid.Cells[2, 0] := FormatIt(Position.Y / 100);
       // AltEdit2.Text:=FormatIt(GiveRadius/100);
          end;
      end;

    mn_Edit_Symbol: // Edit Symbol's coordinates
      begin;
        SetButton(false, false, false);
        TypeCombo.Enabled := false;
        SpecificCheckBox.Enabled := false;
        Label1.Font.Color := clGray;
        Label5.Caption := MlgSection[78];
        with PProj(AProj)^ do
          with PSymbol(OldSnapItem.mptr)^ do
          begin;
            PointsStringGrid.Cells[0, 0] := MlgSection[77] + MlgSection[70];
            PointsStringGrid.Cells[1, 0] := FormatIt(Position.X / 100);
            PointsStringGrid.Cells[2, 0] := FormatIt(Position.Y / 100);
          end;
      end;

    mn_Edit_Text: // Edit Text's coordinates
      begin;
        SetButton(false, false, false);
        TypeCombo.Enabled := false;
        SpecificCheckBox.Enabled := false;
        Label1.Font.Color := clGray;
        Label5.Caption := MlgSection[79];
        with PProj(AProj)^ do
          with PTExt(OldSnapItem.mptr)^ do
          begin;
            PointsStringGrid.Cells[0, 0] := MlgSection[77] + MlgSection[70];
            PointsStringGrid.Cells[1, 0] := FormatIt(Pos.X / 100);
            PointsStringGrid.Cells[2, 0] := FormatIt(Pos.Y / 100);
          end;
      end;

    mn_Edit_Circle: // Edit Circle's coordinates
      begin;
        SetButton(false, true, true);
        CircleCenter.Init(0, 0);
        J := MlgStringList.Sections['DrawHndl'];
        TypeCombo.Items.Clear;
        TypeCombo.Items.Add(GetShortHint(MlgStringList.SectionStrings[j, 8]));
        TypeCombo.Items.Add(GetShortHint(MlgStringList.SectionStrings[j, 5]));
        TypeCombo.Items.Add(GetShortHint(MlgStringList.SectionStrings[j, 9]));
        TypeCombo.ItemIndex := 0;
        MaxPointsInThisMode := 2;
        CircleType := 1;
        OldCircleModeType := 0;
        SetInputCircleModeWithInputPointCounts(1);
        Label8.Visible := false;
        Edit3.Visible := false;
        ModeCombo.Items.Add(MlgSection[12]);
        ModeCombo.Items.Add(MlgSection[13]);
        ModeCombo.ItemIndex := 0;

        with PProj(AProj)^ do
          with PEllipse(OldSnapItem.mptr)^ do
          begin;
            PointsStringGrid.Cells[1, 0] := FormatIt(Position.X / 100);
            PointsStringGrid.Cells[2, 0] := FormatIt(Position.Y / 100);
            AltEdit2.Text := FormatIt(GiveRadius / 100);
          end;

        UpdateTitle;

      end;

    mn_Edit_Arc: // Edit Arc's coordinates
      begin;
        SetButton(false, false, false);
        CircleCenter.Init(0, 0);
        TypeCombo.Items.Clear;
        TypeCombo.Enabled := false;
        Label1.Font.Color := clGray;
        MaxPointsInThisMode := 1;
        ArcType := 1;
        OldArcModeType := 0;
        SetInputArcModeWithInputPointCounts(1);
        SpecificCheckBox.Enabled := false;
        Label8.Visible := false;
        Edit3.Visible := false;

        with PProj(AProj)^ do
          with PEllipseArc(OldSnapItem.mptr)^ do
          begin;
            PointsStringGrid.Cells[1, 0] := FormatIt(Position.X / 100);
            PointsStringGrid.Cells[2, 0] := FormatIt(Position.Y / 100);
            AltEdit1.Text := FormatIt(GiveRadius / 100);
            AltEdit2.Text := FormatIt(180 * BeginAngle / PI);
            AltEdit3.Text := FormatIt(180 * EndAngle / PI);
          end;
        UpdateTitle;
      end;

    mn_New_Arc: // New Arc input
      begin;
        Self.Caption := MlgSection[72];
        Label1.Caption := MlgSection[54];
        J := MlgStringList.Sections['DrawHndl'];
        Label8.Visible := false;
        Edit3.Visible := false;
        TypeCombo.Items.Add(GetShortHint(MlgStringList.SectionStrings[j, 3]));
        TypeCombo.Items.Add(GetShortHint(MlgStringList.SectionStrings[j, 1]));
        TypeCombo.Items.Add(GetShortHint(MlgStringList.SectionStrings[j, 2]));
        TypeCombo.ItemIndex := ArcType - 1;
        ModeCombo.Items.Add(MlgSection[12]);
        ModeCombo.Items.Add(MlgSection[13]);
        ModeCombo.ItemIndex := 0;
        TypeCombo.Enabled := false;

        case ArcType of
          1:
            begin;
              MaxPointsInThisMode := 3;
              SetInputArcModeWithInputPointCounts(PointsStringgrid.RowCount);
            end;
          2:
            begin;
              MaxPointsInThisMode := 2;
              SetInputArcModeWithInputPointCounts(PointsStringgrid.RowCount);
              Combobox1.Items.Add(MlgSection[62]);
              Combobox1.Items.Add(MlgSection[63]);
              FloatValidator2.Edit := nil;
              AngleValidator2.Edit := AltEdit2;
            end;
          3:
            begin;
              MaxPointsInThisMode := 3;
              SetInputArcModeWithInputPointCounts(PointsStringgrid.RowCount);
            end;
        end;
        if ArcType > 1 then
          FillArcDataToGrid
        else
        begin;
          with PProj(Project)^.MenuHandler.SubHandler as TRubberArc do
            if PointsCount <= 1 then
              FillArcDataToGrid
            else
            begin;
         //Center
              PointsStringGrid.Cells[1, 0] := FormatIt(ArrayOfPoints[0].X / 100);
              PointsStringGrid.Cells[2, 0] := FormatIt(ArrayOfPoints[0].Y / 100);
         //Radius
              AltEdit1.Text := FormatIt(GrPointDistance(ArrayOfPoints[0], ArrayOfPoints[1]) / 100);
              if PointsCount > 2 then
              begin;
                AddBtn.OnClick(Self);
                PointsStringGrid.Cells[1, 1] := FormatIt(ArrayOfPoints[2].X / 100);
                PointsStringGrid.Cells[2, 1] := FormatIt(ArrayOfPoints[2].Y / 100);
                AltEdit2.Text := AltEdit1.Text;
              end;
            end;
        end;
        UpdateTitle;
      end;

    mn_New_Circle: //Input New Circle
      begin;
        Self.Caption := MlgSection[49];
        Label1.Caption := MlgSection[54];
        J := MlgStringList.Sections['DrawHndl'];
        Label8.Visible := false;
        Edit3.Visible := false;
        TypeCombo.Items.Add(GetShortHint(MlgStringList.SectionStrings[j, 8]));
        TypeCombo.Items.Add(GetShortHint(MlgStringList.SectionStrings[j, 5]));
        TypeCombo.Items.Add(GetShortHint(MlgStringList.SectionStrings[j, 7]));
        TypeCombo.Items.Add(GetShortHint(MlgStringList.SectionStrings[j, 6]));
        TypeCombo.Items.Add(GetShortHint(MlgStringList.SectionStrings[j, 9]));
        TypeCombo.ItemIndex := CircleType - 1;
        ModeCombo.Items.Add(MlgSection[12]);
        ModeCombo.Items.Add(MlgSection[13]);
        ModeCombo.ItemIndex := 0;
        TypeCombo.Enabled := false;
        Koeff := PixelsPerInch / 120;
        case CircleType of
          1:
            begin;
              MaxPointsInThisMode := 2;
              SetInputCircleModeWithInputPointCounts(PointsStringgrid.RowCount);
            end;
          2:
            begin;
              MaxPointsInThisMode := 2;
              SetInputCircleModeWithInputPointCounts(PointsStringgrid.RowCount);
              FloatValidator2.Edit := nil;
              AngleValidator2.Edit := AltEdit2;
            end;
          3:
            begin;
              MaxPointsInThisMode := 3;
              SetInputCircleModeWithInputPointCounts(PointsStringgrid.RowCount);
            end;
          4:
            begin;
              MaxPointsInThisMode := 3;
              SetInputCircleModeWithInputPointCounts(PointsStringgrid.RowCount);
              Combobox1.Items.Add(MlgSection[62]);
              Combobox1.Items.Add(MlgSection[63]);
            end;
          5:
            begin;
              MaxPointsInThisMode := 2;
              SetInputCircleModeWithInputPointCounts(PointsStringgrid.RowCount);
              FloatValidator1.MinValue := -9999999999;
              FloatValidator2.MinValue := -9999999999;
            end;
        end;
        FillCircleDataToGrid;
        UpdateTitle;
      end;

    Ot_cPoly, Ot_POly: //Input Poly
      begin;
        Self.Caption := MlgSection[26];
        J := MlgStringList.Sections['DrawOrthoHndl'];
        if PProj(AProj)^.ActualMen = mn_cPoly then
          RealCount := 3 //Polygon
        else
          RealCount := 1; //Polyline
        for i := 1 to RealCount do //Filling Post-Processing combobox
          TypeCombo.Items.Add(MlgStringList.SectionStrings[j, i]);
        ModeCombo.Items.Add(MlgSection[12]);
        ModeCombo.Items.Add(MlgSection[13]);
        ModeCombo.ItemIndex := 0;
        PointsStringGrid.Cells[0, 0] := MlgSection24 + ' 1';
    //filling coordinates
        if PProj(AProj)^.bDrawRubberBox = false then
     //rubber
        begin;
          with PProj(AProj)^.NewPoly^.Data^ do
          begin;
            PointsStringGrid.Visible := false;
            for i := 0 to Count - 1 do
            begin;
              PointsStringGrid.Cells[0, i] := MlgSection24 + ' ' + IntToStr(i + 1);
              PointsStringGrid.Cells[1, i] := FormatIt(PDPoint(At(i))^.X / 100); //    Data.        //x
              PointsStringGrid.Cells[2, i] := FormatIt(PDPoint(At(i))^.Y / 100); //y
              if i <> Count then PointsStringGrid.RowCount := PointsStringGrid.RowCount + 1;
            end;
            PointsStringGrid.Visible := true;
            RealCount := PointsStringGrid.RowCount;
          end; //with
        end
        else
        begin; //rubber
          RealCount := 0;
          with PProj(AProj)^.SelRect^ do
            if IsOn then
            begin;
              PointsStringGrid.Cells[0, 0] := MlgSection24 + ' 1';
              PointsStringGrid.Cells[1, 0] := FormatIt(StartPos.X / 100);
              PointsStringGrid.Cells[2, 0] := FormatIt(StartPos.Y / 100);
              PointsStringGrid.RowCount := 2;
              RealCount := 2;
            end;
        end;
    //adding point for input new point
        if Realcount >= 1 then
        begin;
          PointsStringGrid.Cells[0, Realcount - 1] := MlgSection24 + ' ' + IntToStr(RealCount);
//          SetNewData:=false;
          PointsStringGrid.Cells[1, RealCount - 1] := FormatIt(0);
          PointsStringGrid.Cells[2, Realcount - 1] := FormatIt(0);
          PointsStringGrid.Row := RealCount - 1;
        end;

        UpdateTitle;
        LastInputString := PointsStringGrid.RowCount - 1;
        PointCombo.Hint := MlgSection[36] + IntToStr(PointsStringGrid.RowCount);

        if PointsStringGrid.RowCount = 1 then
          DelBtn.Enabled := false
        else
          DelBtn.Enabled := true;

    //Set post-processing combobox to current mode
        TypeCombo.ItemIndex := 0;
        if PProj(AProj)^.NewPoly.bDrawOrtho = true
          then
        begin;
          TypeCombo.ItemIndex := 1;
          if (PProj(Project)^.Registry.ReadBool('\User\WinGISUI\DrawingTools\Ortho') = true)
            and (PointsStringGrid.RowCount > 2)
            then
          begin;
            SpecificCheckBox.Checked := true;
            CheckBox1.OnClick(Self);
            OrtoBtn.Down := true;
            OrtoBtn.OnClick(Self);

         //    Edit2.SetFocus;
          end;

        end;
        if PProj(AProj)^.bDraw3Points = true then TypeCombo.ItemIndex := 2;
        if PProj(AProj)^.bDrawRubberBox = true then TypeCombo.ItemIndex := 3;

        if PointsStringGrid.Row > PointsStringGrid.VisibleRowCount then
          PointSStringGrid.TopRow := PointSStringGrid.Row - PointSStringGrid.VisibleRowCount + 1;

      end;
  //editing existing poly. Mn_snap - for Object properties Dialog
    mn_InsPOk, mn_SnapPoly, mn_Snap: //Poly
      begin;
    //user can't change  mode for special input (as orthogonal and etc)
        Label1.Font.Color := clGray;
        TypeCombo.ItemIndex := 0;
        TypeCombo.Enabled := false;
        Self.Caption := MlgSection[26];
        if ObjectType <> mn_Snap then
          SetButton(false, false, false);
        J := MlgStringList.Sections['DrawOrthoHndl'];
        for i := 1 to 3 do
          TypeCombo.Items.Add(MlgStringList.SectionStrings[j, i]);
        ModeCombo.Items.Add(MlgSection[12]);
        ModeCombo.Items.Add(MlgSection[13]);
        ModeCombo.ItemIndex := 0;
        with PProj(AProj)^ do
          with Ppoly(OldSnapItem.mptr)^.Data^ do
          begin;
            RealCount := Count - 1;
       //Polygon have same points as first and end . So
       // I don't add last point to table
            if Ppoly(OldSnapItem.mptr)^.GetObjType = ot_CPoly then RealCount := RealCount - 1;
       //filling data
            PointsStringGrid.Visible := false;
            PointsStringGrid.OnDrawCell := nil;
            PointsStringGrid.OnSelectCell := nil;

            for i := 0 to RealCount do
            begin;
              PointsStringGrid.Cells[0, i] := MlgSection24 + ' ' + IntToStr(i + 1);
              PointsStringGrid.Cells[1, i] := FormatIt(PDPoint(At(i))^.X / 100);
              PointsStringGrid.Cells[2, i] := FormatIt(PDPoint(At(i))^.Y / 100);
              PointsStringGrid.RowCount := PointsStringGrid.RowCount + 1;
            end;
            PointsStringGrid.OnDrawCell := PointsStringGridDrawCell;
            PointsStringGrid.OnSelectCell := PointsStringGridSelectCell;
            PointsStringGrid.Visible := true;
            if PointsStringGrid.RowCount > 1 then
            begin;
              PointsStringGrid.RowCount := PointsStringGrid.RowCount - 1;
            end;
          end;
        UpdateTitle;
      end;
  end;

 //IF Layer Fixed, then all elements set to RedOnly mode
  if Pproj(WinGisMainform.ActualProj).Layers^.TopLayer^.GetState(sf_Fixed) then
  begin;
    SetButton(false, false, false);
    FileButton.Enabled := false;
    TypeCombo.Enabled := false;
    DelAllBtn.Enabled := false;
    SpecificCheckBox.Enabled := false;
    PointsStringGrid.Options := PointsStringGrid.Options - [goEditing];
    Edit4.Readonly := true;
    Edit5.Readonly := true;

    FloatValidator1.Edit := nil;
    FloatValidator2.Edit := nil;
    AngleValidator2.Edit := nil;
    AngleValidator3.Edit := nil;

    AltEdit1.ReadOnly := true;
    AltEdit2.ReadOnly := true;
    AltEdit3.ReadOnly := true;
    CoordTypeBtn.Enabled := false;
    FileButton.Enabled := false;

  end;
  OrtoBtn.Enabled := (SpecificCheckbox.Checked = true) and (PointsStringGrid.Rowcount > 2);



 { if Inifile.LoadPointsCoordDataFromFile.InputMode = 0
    then begin;
     AProjection:=Get_projection(PProj(Project).PInfo.ProjectionSettings.ProjectProjection);
     ADatum:=Get_datum_params(PProj(Project).PInfo.ProjectionSettings.ProjectDate);
     set_FromSystem(transform_pointer,AProjection,ADatum);

     AProjection:=Get_projection('Phi/Lamda values');
     ADatum:=Get_datum_params('WGS84');
     set_ToSytem(transform_pointer,AProjection,ADatum);


          end;}
  Screen.Cursor := crDefault;
  SetNewData := false;
end;

procedure TPointsCoordDialog.FormShow(Sender: TObject);
var
  z: boolean;
  i: integer;
  key: word;
begin
  FileButton.Enabled:=MenuFunctions['FileImportTXT'].Enabled;
  if IsFormShow = false
    then begin;
    ISFormShow := true;
    TmpClosed; //
//if  AxGISProjection1=nil
// then
//  begin;
//   AxGISProjection1.Create(nil);
//   AxGisProjection1.WorkingPath:=WinGISMainForm.CurrentDirectory;
//  end;

    FirstSwitch := true;
    z := true;
    OldRow := 0;
    OldCol := 1;
    LastApplyPoint := MlgSection[29];
    SwitchMode;

    PreparePointsgrid;
    CalculateAdditional;
    ChangeCombo;

    if (FixedPointsRow <> -1) or (ObjectType = mn_insPOk) then
    begin;
      if ObjectType = mn_insPOk then
      begin;
     // Set row to req. string
        i := DetectFramePoint;
        if i = -1 then
        begin;
          FixedPointsRow := PointsStringGrid.RowCount;
          AddBtn.Click;
          PointsStringGrid.Row := PointsStringGrid.RowCount - 1;
        end
        else
        begin;
          FixedPointsRow := i;
          PointsStringGrid.Row := i;
          InsBtn.Click;
        end;
        OldRow := FixedPointsRow;
        DelBtn.Enabled := false;
        Exit;
      end;
      PointsStringGrid.Row := FixedPointsRow;
      OldRow := FixedPointsRow;
    end;

    SetOk;
 //copy data of active points to Simply Mode "classic" F4-Dialog
    Edit4.Text := PointsStringGrid.Cells[1, PointsStringGrid.Row];
    Edit5.Text := PointsStringGrid.Cells[2, PointsStringGrid.Row];

    if ObjectType = Ot_text then SetInputTExtMode(PointsStringGrid.RowCount);
// UpdateTitle;
// if PProj(Project).PInfo.ProjectionSettings.ShowCoordInDegrees then
//  begin;
 //            TmpInputMode:=Inifile.LoadPointsCoordDataFromFile.InputMode;
//  Inifile.LoadPointsCoordDataFromFile.InputMode:=1;
//           TmpDegreeMode:=Inifile.LoadPointsCoordDataFromFile.DegreeMode;
    if (Inifile.LoadPointsCoordDataFromFile.InputMode = 1) and (Edit4.Text <> '')
      then
    begin;
      ResetPoints(false);
    end;

//  end
//  else
//  Inifile.LoadPointsCoordDataFromFile.InputMode:=0;
    Screen.Cursor := crDefault;
//if objecttype=ot_poly or objecttype=ot_cpoly objecttype=ot_poly objecttype=ot_poly objecttype=ot_poly
    if Inifile.LoadPointsCoordDataFromFile.InputMode = 1 then
      if (ObjectType = ot_CPoly) or (ObjectType = ot_Poly) or
        (ObjectType = mn_New_Circle) or (ObjectType = mn_New_Arc)
        or (ObjectType = ot_Pixel) or (ObjectType = ot_Text)
        or (ObjectType = ot_Symbol)
        then
      begin;
        PointsStringGrid.Cells[1, PointsStringGrid.RowCount - 1] := OutputDegreeMode(0);
        PointsStringGrid.Cells[2, PointsStringGrid.RowCount - 1] := OutputDegreeMode(0);
      end;

    if (ObjectType = mn_Snap) or (ObjectType = mn_Edit_Point) or
      (ObjectType = mn_Edit_Arc) or (ObjectType = mn_Edit_Symbol)
      or (ObjectType = mn_Edit_Circle) or (ObjectType = mn_Edit_Text)
      then
      Inifile.LoadPointsCoordDataFromFile.InputMode := 0;


    if (OrtoBtn.Visible) and (OrtoBtn.Enabled) then
      if OrtoBtn.Down then
      begin;
        Edit1.SetFocus;
        ActiveControl := Edit1;
      end;
  end;
end;

//Organize form of Dialog with SimplyMode ("classic" F4-Dialog) or Advanced

procedure TPointsCoordDialog.SetOk;
begin;
  if ((ObjectType = ot_CPoly) or
    (ObjectType = ot_Poly))
    then ActiveControl := OkBtn;

end;

procedure TPointsCoordDialog.SwitchMode;

begin;
 //Koeff will using with ReturnCorrected function.
 //it need for correct positioning for small/big fonts
  Koeff := PixelsPerInch / 120;
  BadFlag := '';
  if SimplyMode = false then
  begin; //Advanced mode
    ControlsPanel.Visible := true;
    ControlsPanel.Top := 0;
    ControlsPanel1.Visible := false;
    Bevel1.Top := ControlsPanel.Height + ReturnCorrected(-1);
    OkBtn.Top := Bevel1.Top + ReturnCorrected(6);
    CancelBtn.Top := OkBtn.Top;
    SwitchBtn.Top := OkBtn.Top;
    ControlsPanel.Top := 0;
    Self.ClientHeight := {ReturnCorrected(} trunc(ControlsPanel.Height + OkBtn.Height * 1.5);
    Self.ClientWidth := ReturnCorrected(490); //ReturnCorrected(492);
//   FileButton.Top:=OkBtn.Top;
//   FileButton.Left:= SwitchBtn.Left+SwitchBtn.Width+ReturnCorrected(10);
//    OkBtn.Left := trunc(Self.ClientWidth / 2 + 6);
//    CancelBtn.Left := OkBtn.Left + OkBtn.Width + ReturnCorrected(5);
    SwitchBtn.Caption := Mlgsection[6];
//    SetOk;
    CoordBtn2.Visible := false;
  end
  else
  begin; //Classic F4
    ControlsPanel1.Visible := true;
    ControlsPanel1.Top := 0;
    ControlsPanel.Visible := false;
    Bevel1.Top := ControlsPanel1.Height;// - 1 ;
    OkBtn.Top := Bevel1.Top + 6;
    CancelBtn.Top := OkBtn.Top;
    SwitchBtn.Top := OkBtn.Top;
//   FileButton.Top:=OkBtn.Top;
//   FileButton.Left:= SwitchBtn.Left+SwitchBtn.Width+ReturnCorrected(8);
    ControlsPanel.Top := 0;
    Self.ClientHeight := ControlsPanel1.Height + 40;
//    OkBtn.Left := ReturnCorrected(230);
//    CancelBtn.Left := ReturnCorrected(348);
    Self.ClientWidth := 370;
    SwitchBtn.Caption := Mlgsection[5];
    Edit4.SetFocus;
    CoordBtn2.Top := OkBtn.Top;
//    CoordBtn2.Left := SwitchBtn.Left + SwitchBtn.Width + 3;
    CoordBtn2.Visible := true;
  end;
 //Set Dialog to Center of DeskTop
  if (Screen.DesktopWidth - Width) div 2 > 0 then
    Left := (Screen.DesktopWidth - Width) div 2
  else
    Left := 0;

  if (Screen.DesktopHeight - Height) div 2 > 0 then
    Top := (Screen.DesktopHeight - Height) div 2
  else
    Top := 0;
end;
//Switch Classic/Advaced mode of Dialog

procedure TPointsCoordDialog.SwitchBtnClick(Sender: TObject);
var
  Key: word;
begin
  InvisibleGradus;
  InvisibleGradus1;
  SimplyMode := not SimplyMode;
  SwitchMode;
  if SimplyMode = false then
  begin;
    PointsStringGrid.Cells[1, PointsStringGrid.Row] := Edit4.Text;
    PointsStringGrid.Cells[2, PointsStringGrid.Row] := Edit5.Text;
    if FirstSwitch = true then
      if ObjectType in [mn_poly, mn_cpoly] then
      begin;
        if PointsStringGrid.Row > PointsStringGrid.VisibleRowCount then
          PointSStringGrid.TopRow := PointSStringGrid.Row - PointSStringGrid.VisibleRowCount + 1;
      end;

  end
  else
  begin;
   //if IniFile.LoadPointsCoordDataFromFile.InputMode =0 then
//    begin;
    Edit4.Text := PointsStringGrid.Cells[1, PointsStringGrid.Row];
    Edit5.Text := PointsStringGrid.Cells[2, PointsStringGrid.Row];
  //  end
   // else begin;


   // end;

  end;
  FirstSwitch := false;
end;

//Set on/off Additional panel for editing of points
// When Panel visible, user can input/edit coordinates using
// other points coordinates

procedure TPointsCoordDialog.SpecificCheckBoxClick(Sender: TObject);
var
  i: integer;
begin
  InvisibleGradus;
  if Inifile.LoadPointsCoordDataFromFile.InputMode = 1
    then
  begin;
    if TObject(SpecificCheckBox) = Sender // <> nil) and (Sender <>'')
      then MessageDlg(Mlgsection[96], mtInformation, [mbOk], 0);
    SpecificCheckBox.Checked := false;
    Panel1.Visible := false;
    Exit;
  end;

  if SpecificCheckBox.Checked = false then
  begin;
    CheckBox1.Checked := false;
    Panel1.Visible := false;
  end
  else
  begin;
    case ObjectType of
      Ot_Poly, Ot_Cpoly, mn_InsPOk, mn_SnapPoly, Mn_Snap, mn_Edit_Circle, mn_New_Circle, mn_New_Arc, mn_Edit_Arc, ot_Pixel, ot_Symbol, ot_text:
        begin;
          Edit3.Visible := false;
          Label8.Visible := false;
        end;
    end; //case
//    Panel1.Visible := true;
  end; //else
  Screen.Cursor := crHourGlass;
  ChangeCombo;
  CheckGridCount;
  Screen.Cursor := crDefault;
  OrtoBtn.Enabled := (SpecificCheckbox.Checked = true) and (PointsStringGrid.Rowcount > 2);
end;

procedure TPointsCoordDialog.PointsStringGridDrawCell(Sender: TObject;
  ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  MyRect: TRect;
  TmpFontColor: integer;
  TmpFontStyle: TFontStyle;

  sCad: string;
  i: integer;
  bEsNum: Bool;
begin


  if not (gdSelected in state) then
  begin;


    if PointsStringGrid.Cells[ACol, ARow] = ''
      then PointsStringGrid.Cells[ACol, ARow] := FormatIt(0);
//CheckGridCount;
    CalculateAdditional;
  end;
 //It is Right-aligned text in PointsStringGrid
 //Thanks to Sigolaeff (He found base source code for it)
  if Acol > 0 then
    if (PointsStringGrid.Cells[ACol, ARow] > '') and (Inifile.LoadPointsCoordDataFromFile.InputMode = 0) then
    begin
      bEsNum := True;
      sCad := PointsStringGrid.Cells[ACol, ARow];
      for i := 1 to length(sCad) do
        if not (sCad[i] in ['0'..'9', '-', '.', ',']) then bEsNum := False;
      if bEsNum then
      begin

        sCad := Format('%.2f', [StrToFloat(PointsStringGrid.Cells[ACol, ARow])]);
        with PointSStringGrid do
          with Canvas, Rect do
          begin
            i := Right - TextWidth(sCad + ' ');
            Canvas.FillRect(Rect);
            Canvas.TextOut(i, Top + 2, sCad);
          end;
      end;
    end;

end;

//Select Row

procedure TPointsCoordDialog.PointsStringGridSelectCell(Sender: TObject;
  ACol, ARow: Integer; var CanSelect: Boolean);
var
s: String;
begin;
  InvisibleGradus;
  if FixedPointsRow <> -1 then
  //Check - user can't select another row, if mode is Mn_snapOk or mn_InsPok
    if FixedPointsRow <> ARow then
    begin; //Say to user - "Astalavista,baby" :))
      MessageDlg(Mlgsection[40], mtError, [mbOk], 0);
      PointsStringGrid.Row := FixedPointsRow;
      PointsStringGridSelectCell(Self, ACol, FixedPointsRow, CanSelect);
      CanSelect := false;
      Exit;
    end;

 //   If user forgot fill cells - he will have "0.00"
  if (OldRow <= PointsStringGrid.RowCount - 1) then
  begin;
 //   if PointsStringGrid.EditorMode = false then
 //begin;
    if PointsStringGrid.Cells[OldCol, Oldrow] = '' then
      PointsStringGrid.Cells[OldCol, Oldrow] := FormatIt(0);
 //end;
    ResetCell(OldCol, OldRow);
// end;
  end;
end;

//Prepare visual properties of Grid - headers, width and etc

procedure TPointsCoordDialog.PreparePointsGrid;

var
  GridWith: Integer;
  VertScroll: boolean;
  z: boolean;
begin
  VertScroll := PointsStringGrid.RowCount > PointsStringGrid.VisibleRowCount;

{  if VertScroll = false then
    PointsStringGrid.DefaultColWidth := (PointsStringGrid.Width div 3) - 2
  else}
  if VertScroll = true then
    PointsStringGrid.DefaultColWidth := (PointsStringGrid.ClientWidth div 3) - 3
  else PointsStringGrid.DefaultColWidth := (PointsStringGrid.ClientWidth div 3) - 2;

  PointsStringGrid.ColWidths[0] := PointsStringGrid.DefaultColWidth - 10;
  PointsStringGrid.ColWidths[1] := PointsStringGrid.DefaultColWidth + 5;
  PointsStringGrid.ColWidths[2] := PointsStringGrid.DefaultColWidth + 8;

  if VertScroll = true then PointsStringGrid.ColWidths[2] := PointsStringGrid.ColWidths[2] + 1;
  if VertScroll = true then PointsStringGrid.ColWidths[1] := PointsStringGrid.ColWidths[1] + 1;
 //   PointsStringGrid.ColWidths[2]:= PointsStringGrid.ColWidths[1];

  HeaderControl1.Left := Panel2.Left;// + 1;
  HeaderControl1.Width := panel2.Width;// - 1;

  HeaderControl1.Sections[0].Width := PointsStringGrid.ColWidths[0] + 2;
  HeaderControl1.Sections[1].Width := PointsStringGrid.ColWidths[1] + 1;
  HeaderControl1.Sections[2].Width := PointsStringGrid.ColWidths[2] + 4; // HeaderControl1.Width - (2 * PointsStringGrid.DefaultColWidth + 3); //+2;
end;

//checking correct input coordinate

function TPointsCoordDialog.CheckEdit(StringText: string; Sender: TObject): boolean;
var
  R: single;
begin
  Result := false;
  if Inifile.LoadPointsCoordDataFromFile.InputMode = 0 then begin;
    try
      R := StrToFloat(Trim(StringText));
    except
      MessageDlg('"' + StringText + '" ' + Mlgsection[34], mtError, [mbOk], 0);
      (Sender as TWinControl).SetFocus;
      Exit;
    end;
    Result := true;
  end

  else
  begin;
    //checking for format


  end;

end;

//Insert new points

procedure TPointsCoordDialog.InsBtnClick(Sender: TObject);
var
  R, i: Integer;
  z: boolean;
begin
  InvisibleGradus;
  if CheckAvMode = false then Exit;
  R := PointsStringGrid.Row;
  PointsStringGrid.RowCount := PointsStringGrid.RowCount + 1;
  for i := PointsStringGrid.RowCount - 1 downto R + 1 do
  begin;
    if ObjectType = ot_Symbol then
      PointsStringGrid.Cells[0, i] := PointsStringGrid.Cells[0, i - 1];
    PointsStringGrid.Cells[1, i] := PointsStringGrid.Cells[1, i - 1];
    PointsStringGrid.Cells[2, i] := PointsStringGrid.Cells[2, i - 1]
  end;
  if ObjectType <> ot_Symbol then
    PointsStringGrid.Cells[0, PointsStringGrid.RowCount - 1] := MlgSection[24] + ' ' + IntToStr(PointsStringGrid.RowCount);


  if ObjectType = ot_Symbol then
  begin;
    if IsLoadingNow = false
      then
    begin;
      PointsStringGrid.Cells[0, R] := PProj(Project).ActualSym.GetName;
      SymlibList.Insert(R, GetPositionOfSymbolsSet(ASymLibInfo, PProj(Project).ActualSym.GetLibName));
    end
  end
  else
    ; //    PointsStringGrid.Cells[0,R]:=MlgSection[24] + ' ' + IntToStr(R+1);


  PointsStringGrid.Cells[1, R] := FormatIt(0);
  PointsStringGrid.Cells[2, R] := FormatIt(0);

  PointsStringGrid.Row := R;
  PreparePointsGrid;
  if label5.Visible = true then UpdateTitle;
  PointsStringGrid.OnSelectCell(Self, PointsStringGrid.Col, R, z);
  CheckGridCount;

  if PointsStringGrid.RowCount > 1 then
  begin;
    LastInputString := PointsStringGrid.Row - 1;
    PointCombo.Hint := MlgSection[36] + IntToStr(PointsStringGrid.Row);
  end
  else
  begin;
    PointCombo.Hint := MlgSection[36] + IntToStr(1);
    LastInputString := 0;
  end;

  if PointsStringGrid.RowCount = MaxPointsInThisMode then
  begin;
    InsBtn.Enabled := false;
    Addbtn.Enabled := false;
  end;

  DelBtn.Enabled := true;

  if (ObjectType = mn_New_Circle) or (ObjectType = mn_Edit_Circle) then SetInputCircleModeWithInputPointCounts(PointsStringGrid.RowCount);

  if (ObjectType = mn_New_Arc) or (ObjectType = mn_Edit_Arc) then SetInputArcModeWithInputPointCounts(PointsStringGrid.RowCount);

  if ObjectType = ot_Text then
    SetInputTExtMode(PointsStringGrid.RowCount);

  OrtoBtn.Enabled := (SpecificCheckbox.Checked = true) and (PointsStringGrid.Rowcount > 2);
end;
//Show count of Points of Object

procedure TPointsCoordDialog.UpdateTitle;
begin;
  if (ObjectType <> ot_Pixel) and (ObjectType <> ot_Symbol) then
    Label5.Caption := MlgSection8 + ' (' + IntToStr(PointsStringGrid.RowCount) + ')'
  else
  begin;
    if ObjectType = ot_Pixel then
      Label5.Caption := MlgSection71 + ' (' + IntToStr(PointsStringGrid.RowCount) + ')';
    if ObjectType = ot_Symbol then
      Label5.Caption := MlgSection76 + ' (' + IntToStr(PointsStringGrid.RowCount) + ')'
  end;
end;

//adding point

procedure TPointsCoordDialog.AddBtnClick(Sender: TObject);
begin
  if CheckAvMode = false then Exit;
  InvisibleGradus;
  if ObjectType = mn_Edit_Circle then
  //move
    RecalculateCirlcle(OldCircleModeType);

  PointsStringGrid.RowCount := PointsStringGrid.RowCount + 1;

  PointsStringGrid.Cells[1, PointsStringGrid.RowCount - 1] := FormatIt(0);
  PointsStringGrid.Cells[2, PointsStringGrid.RowCount - 1] := FormatIt(0);
  if ObjectType = ot_Symbol
    then
  begin;
    if IsLoadingNow = false
      then
    begin;
      PointsStringGrid.Cells[0, PointsStringGrid.RowCount - 1] := PProj(Project).ActualSym.GetName;
      SymlibList.Add(GetPositionOfSymbolsSet(ASymLibInfo, PProj(Project).ActualSym.GetLibName));
    end
  end
  else
    PointsStringGrid.Cells[0, PointsStringGrid.RowCount - 1] := MlgSection[24] + ' ' + IntToStr(PointsStringGrid.RowCount);

  PreparePointsGrid;
  CheckGridCount;
  if label5.Visible = true then UpdateTitle;
  PointsStringGrid.Row := PointsStringGrid.RowCount - 1;

  if PointsStringGrid.RowCount > 1 then
  begin;
    LastInputString := PointsStringGrid.Row - 1;
    PointCombo.Hint := MlgSection[36] + IntToStr(PointsStringGrid.Row);
  end
  else
  begin;
    PointCombo.Hint := MlgSection[36] + IntToStr(1);
    LastInputString := 0;
  end;

  if PointsStringGrid.RowCount = MaxPointsInThisMode then
  begin;
    InsBtn.Enabled := false;
    Addbtn.Enabled := false;
  end;

  DelBtn.Enabled := true;
  if (ObjectType = mn_New_Circle) or (ObjectType = mn_Edit_Circle) then SetInputCircleModeWithInputPointCounts(PointsStringGrid.RowCount);

  if (ObjectType = mn_New_Arc) or (ObjectType = mn_Edit_Arc) then SetInputArcModeWithInputPointCounts(PointsStringGrid.RowCount);

  if ObjectType = mn_Edit_Circle then
  //move
    SetCircleParamsForFirstSecond;

  if ObjectType = ot_Text then
    SetInputTExtMode(PointsStringGrid.RowCount);

  OrtoBtn.Enabled := (SpecificCheckbox.Checked = true) and (PointsStringGrid.Rowcount > 2);
end;
//delete point

procedure TPointsCoordDialog.DeleteProcess;
var
  i: integer;
begin;

  if ObjectType = mn_Edit_Circle then
     //move
    RecalculateCirlcle(OldCircleModeType);

  if ObjectType = ot_Symbol
    then SymlibList.Delete(i);

  for i := PointsStringGrid.Row to PointsStringGrid.RowCount - 1 do
  begin;
    if ObjectType = ot_Symbol
      then
    begin;
      PointsStringGrid.Cells[0, i] := PointsStringGrid.Cells[0, i + 1];
    end;
    PointsStringGrid.Cells[1, i] := PointsStringGrid.Cells[1, i + 1];
    PointsStringGrid.Cells[2, i] := PointsStringGrid.Cells[2, i + 1]
  end;

  PointsStringGrid.RowCount := PointsStringGrid.RowCount - 1;
  PreparePointsGrid;
  CheckGridCount;
  UpdateTitle;

  if PointsStringGrid.RowCount > 1 then
  begin;
    LastInputString := PointsStringGrid.Row - 1;
    PointCombo.Hint := MlgSection[36] + IntToStr(PointsStringGrid.Row);
  end
  else
  begin;
    PointCombo.Hint := MlgSection[36] + IntToStr(1);
    LastInputString := 0;
    PointCombo.Enabled := false;
    PointCombo.Font.Color := clGray;
    CheckBox1.Checked := false;
  end;
end;

procedure TPointsCoordDialog.DeleteProcess1;
begin;
  if PointsStringGrid.RowCount = 1 then
    DelBtn.Enabled := false
  else
    DelBtn.Enabled := true;

  if PointsStringGrid.RowCount < MaxPointsInThisMode then
  begin;
    InsBtn.Enabled := true;
    Addbtn.Enabled := true;
  end;

  if (ObjectType = mn_New_Circle) or (ObjectType = mn_Edit_Circle) then SetInputCircleModeWithInputPointCounts(PointsStringGrid.RowCount);

  if (ObjectType = mn_New_Arc) or (ObjectType = mn_Edit_Arc) then SetInputArcModeWithInputPointCounts(PointsStringGrid.RowCount);

  if ObjectType = mn_Edit_Circle then
  //move
    SetCircleParamsForFirstSecond;

  if ObjectType = ot_Text then
    SetInputTExtMode(PointsStringGrid.RowCount);
  if OrtoBtn.Down then
    if PointsStringGrid.RowCount < 3
      then
    begin;
      OrtoBtn.Down := false;
      OrtoBtn.OnClick(Self);
    end;
  OrtoBtn.Enabled := (SpecificCheckbox.Checked = true) and (PointsStringGrid.RowCount > 2);
end;

procedure TPointsCoordDialog.DelBtnClick(Sender: TObject);
var
  I: integer;
begin
  if PointsStringGrid.RowCount > 1 then
    if MessageDlg(Mlgsection[28] + ' ' + IntToStr(PointsStringGrid.Row + 1) + ' ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      DeleteProcess;
  DeleteProcess1;
  InvisibleGradus;
end;

//Switch in/off  Apply For current point coord anothers points

procedure TPointsCoordDialog.CheckBox1Click(Sender: TObject);
begin
  InvisibleGradus;
  if CheckBox1.Checked = true then
  begin;
    PointCombo.Enabled := true;
    PointCombo.Font.Color := clBlack;

  // if  (ObjectType <> Mn_Edit_Circle) and (ObjectType <> mn_Edit_Point)
//     and (ObjectType <> mn_Edit_Arc) and (ObjectType <> mn_Edit_Symbol)
//     and (ObjectType <> mn_Snap)
   //, mn_Edit_Circle]; // (*, mn_Edit_Point, mn_Edit_Arc, mn_Edit_Symbol, mn_Edit_Arc*)])
//    then
    if (PointsStringGrid.RowCount > 1) then
    begin;
      PointCombo.Hint := MlgSection[36] + IntToStr(PointsStringGrid.RowCount - 1);
      LastInputString := PointsStringGrid.RowCount - 2;
      LastApplyPoint := IntToStr(PointsStringGrid.RowCount - 1);
//     PointCombo.Text:=IntToStr(PointsStringGrid.RowCount-1
      CalculateAdditional;
    end;
  end
  else
  begin;
    PointCombo.Enabled := false;
    PointCombo.Font.Color := clGray;
  end;

end;
//Change for Apply - Angle or Delta

procedure TPointsCoordDialog.ModeComboChange(Sender: TObject);
begin
  ChangeCombo;
  CalculateAdditional;
end;

//Change Combo with variuos modes of Additional edit of Points

procedure TPointsCoordDialog.ChangeCombo;
begin;

  if ModeCombo.ItemIndex = 0 then
    case ObjectType of
      Ot_Poly, Ot_Cpoly, mn_InsPOk, mn_SnapPoly, mn_Snap, mn_New_Circle, mn_Edit_Circle, Mn_New_Arc, mn_Edit_Arc, ot_Pixel, ot_Symbol, ot_text:
        begin;
          if PProj(Project).PInfo.CoordinateSystem.CoordinateType <> ctGeodatic then
          begin;
            Label3.Caption := Mlgsection[41];
            Label4.Caption := Mlgsection[42]
          end
          else
          begin;
            Label3.Caption := Mlgsection[42];
            Label4.Caption := Mlgsection[41]
          end;

          AngleValidator.Edit := nil;
        end;
    end;

  if ModeCombo.ItemIndex = 1 then
    case ObjectType of
      Ot_Poly, Ot_Cpoly, mn_InsPOk, mn_SnapPoly, mn_Snap, mn_New_Circle, mn_Edit_Circle, mn_New_Arc, mn_Edit_Arc, ot_pixel, ot_Symbol, ot_Text:
        begin;
          Label3.Caption := Mlgsection[33];
          Label4.Caption := Mlgsection[32];
          AngleValidator.Edit := Edit2;
        end;
    end;
end;

//check and prepare Point's combobox for additional mode editing

procedure TPointsCoordDialog.CheckGridCount;
var
  i, j: integer;
begin;
  if PointsStringGrid.RowCount = 1 then
  begin;
    PointCombo.Enabled := false;
    CheckBox1.Enabled := false;
  end
  else
  begin;
    CheckBox1.Enabled := true;
  end;

  case ObjectType of
    Ot_Poly, Ot_CPoly, mn_InsPOk, mn_SnapPoly, mn_Snap, mn_New_Circle, mn_New_Arc, mn_Edit_Arc, mn_Edit_Circle, ot_pixel, ot_Symbol, ot_Text:
      begin;
        if SpecificCheckBox.Checked = true then
        begin;
          PointCombo.Clear;
          if PointsStringGrid.RowCount > 1 then
          begin;
            for i := 0 to PointsStringGrid.RowCount - 1 do
     //    if i <> PointsStringGrid.Row then
              PointCombo.Items.Add(IntToStr(i + 1));
            if PointCombo.Items.Count > 0 then

//         if (ObjectType <> mn_InsPok) and (ObjectType <> mn_SnapPoly) and (ObjectType <> mn_Snap) and (ObjectType <> mn_Edit_Circle) then
            begin;
              PointCombo.Items.Add(MlgSection[29]);
              j := PointCombo.Items.IndexOf(LastApplyPoint);
              if j = -1 then
                PointCombo.Itemindex := PointCombo.Items.IndexOf(MlgSection[29])
              else
                PointCombo.Itemindex := j;

              if (ObjectType = mn_InsPok) or (ObjectType = mn_SnapPoly) or (ObjectType = mn_Snap) or (ObjectType = mn_Edit_Circle) then
              begin;
                LastInputString := PointsStringGrid.RowCount - 1;
                PointCombo.Hint := MlgSection[36] + IntToStr(PointsStringGrid.RowCount - 1);
              end;

            end
            else
              PointCombo.Itemindex := 0;

          end;
        end; //rowcountcol
      end; //checked

  end; //poly

end; //case

procedure TPointsCoordDialog.PointComboChange(Sender: TObject);
begin
  LastApplyPoint := PointCombo.Items[PointCombo.ItemIndex];
end;

//this procedure calculate increment for selected point relative
//selected on Point's combobox

procedure TPointsCoordDialog.CalculateAdditional;

var
  MyTmp: double;

  function GetDistance(X1, Y1, X2, Y2: double): double;
  var
    Point1, Point2: TGrPoint;
    TmpVar: double;
  begin;
    Point1.X := X1 * 100;
    Point1.Y := Y1 * 100;
    Point2.X := X2 * 100;
    Point2.Y := Y2 * 100;
    TmpVar := GrPointDistance(Point1, Point2) / 100;
    Result := Trunc(TmpVar) + Trunc(Frac(TmpVar) * 100) / 100;
    Edit2.Hint := '';
  end;

  function GetAngle(X1, Y1, X2, Y2: double): double;
  var
    Point1, Point2: TGrPoint;
    TmpVar: double;
  begin;

    Point1.X := X1 * 100;
    Point1.Y := Y1 * 100;
    Point2.X := X2 * 100;
    Point2.Y := Y2 * 100;

    if (Point1.X = Point2.X) and (Point1.Y = Point2.Y) then
   //if two points are equal
    begin;
      Result := 0;
      Edit2.Hint := MlgSection[37];
      Exit;
    end;
    Edit2.Hint := '';

    TmpVar := LineAngle(Point1, Point2) * 180 / PI;
    Result := Trunc(TmpVar) + Trunc(Frac(TmpVar) * 100) / 100;
  end;

begin;
  if IniFile.LoadPointsCoordDataFromFile.InputMode = 1 then
  begin;
    CheckBox1.Checked := false;
    Exit;
  end;
 // if PointsStringGrid.EditorMode=true then Exit;
  try
    case ObjectType of
      ot_Poly, ot_CPoly, mn_InsPOk, mn_SnapPoly, mn_Snap, mn_New_Circle, mn_New_Arc, mn_Edit_Arc, mn_Edit_Circle, ot_pixel, ot_Symbol, ot_text:
        if CheckBox1.Checked = true then
     //Apply to point
          if ModeCombo.ItemIndex = 0 then //X,Y delta
          begin; //ModeCombo
       //   if Items.IndexOf
            if PointCombo.ItemIndex = PointCombo.Items.Count - 1 then
        //"Previous" in point's list//calculate for view in edit current
            begin;
              Edit1.Text := FormatIt(StrToFloat(PointsStringGrid.Cells[1, PointsStringGrid.Row]) - StrToFloat(PointsStringGrid.Cells[1, LastInputString]));
              Edit2.Text := FormatIt(StrToFloat(PointsStringGrid.Cells[2, PointsStringGrid.Row]) - StrToFloat(PointsStringGrid.Cells[2, LastInputString]))
            end
            else
            begin;
              Edit1.Text := FormatIt(StrToFloat(PointsStringGrid.Cells[1, PointsStringGrid.Row]) - StrToFloat(PointsStringGrid.Cells[1, StrToInt(PointCombo.Text) - 1]));
              Edit2.Text := FormatIt(StrToFloat(PointsStringGrid.Cells[2, PointsStringGrid.Row]) - StrToFloat(PointsStringGrid.Cells[2, StrToInt(PointCombo.Text) - 1]));
            end;
          end
          else
          begin; //Distance,Angle
            if PointCombo.ItemIndex = PointCombo.Items.Count - 1 then
        //"Previous" in point's list//calculate for view in edit current
            begin;
              MyTmp := GetDistance(StrToFloat(PointsStringGrid.Cells[1, PointsStringGrid.Row]), StrToFloat(PointsStringGrid.Cells[2, PointsStringGrid.Row]), StrToFloat(PointsStringGrid.Cells[1, LastInputString]), StrToFloat(PointsStringGrid.Cells[2, LastInputString]));
              Edit1.Text := FormatIt(MyTmp);
              MyTmp := GetAngle(StrToFloat(PointsStringGrid.Cells[1, PointsStringGrid.Row]), StrToFloat(PointsStringGrid.Cells[2, PointsStringGrid.Row]), StrToFloat(PointsStringGrid.Cells[1, LastInputString]), StrToFloat(PointsStringGrid.Cells[2, LastInputString]));
              Edit2.Text := FormatIt(MyTmp);
            end
            else
            begin;

              MyTmp := GetDistance(StrToFloat(PointsStringGrid.Cells[1, PointsStringGrid.Row]), StrToFloat(PointsStringGrid.Cells[2, PointsStringGrid.Row]), StrToFloat(PointsStringGrid.Cells[1, StrToInt(PointCombo.Text) - 1]), StrToFloat(PointsStringGrid.Cells[2, StrToInt(PointCombo.Text) - 1]));
              Edit1.Text := FormatIt(MyTmp);
              MyTmp := GetAngle(StrToFloat(PointsStringGrid.Cells[1, PointsStringGrid.Row]), StrToFloat(PointsStringGrid.Cells[2, PointsStringGrid.Row]), StrToFloat(PointsStringGrid.Cells[1, StrToInt(PointCombo.Text) - 1]), StrToFloat(PointsStringGrid.Cells[2, StrToInt(PointCombo.Text) - 1]));
              Edit2.Text := FormatIt(MyTmp);

            end;

          end
        else //"Apply" not checked

        begin;
          if PointsStringGrid.Row <> OldRow then
          begin;
            Edit1.Text := FormatIt(0);
            Edit2.Text := FormatIt(0);
          end;
        end;

   // end; //Poly

    end; //Case
  except;
    MessageDlg('Uknown error ! Please write about it to support@progis.ru', mtError, [mbOk], 0);
  end;

end;

//change post-processing mode. If available

procedure TPointsCoordDialog.TypeComboChange(Sender: TObject);
begin
  case ObjectType of
    Ot_Poly, Ot_Cpoly, Mn_Snap:
      case TypeCombo.ItemIndex of
        0, 1: MaxPointsInThisMode := 17000; //standart and orthogonal
        2:
          if PointsStringGrid.RowCount > 3 then
          begin;
            MessageDlg(Mlgsection[30], mtError, [mbOk], 0); //3 points
            TypeCombo.ItemIndex := 0;
          end;
    //by 2 points
        3:
          if PointsStringGrid.RowCount > 2 then
          begin;
            MessageDlg(Mlgsection[30], mtError, [mbOk], 0); //rubber (thru diagonal)
            TypeCombo.ItemIndex := 0;
          end;
      end; //Poly

    mn_Edit_Circle:
      begin;
        case TypeCombo.ItemIndex of
          0: CircleType := 1;
          1: CircleType := 2; //
          2: CircleType := 5; //
        end;

        RecalculateCirlcle(OldCircleModeType);
        SetInputCircleModeWithInputPointCounts(PointsStringGrid.RowCount);
        SetCircleParamsForFirstSecond;
        OldCircleModeType := TypeCombo.ItemIndex;

      end;

  end; //Main case
end;

//Set keys for PointStringGrid

procedure TPointsCoordDialog.SetButton(InsBut, AddBut, DelBut: boolean);
begin;
  InsBtn.Enabled := InsBut;
  AddBtn.Enabled := AddBut;
  DelBtn.Enabled := DelBut;
end;

//Click on Combo with Points

procedure TPointsCoordDialog.PointComboClick(Sender: TObject);
begin
  CalculateAdditional;
end;

procedure TPointsCoordDialog.Edit2Exit(Sender: TObject);
var
  TmpString: string;
begin
  TmpString := Edit2.Text;
  case ObjectType of

    Ot_Poly, Ot_Cpoly, mn_InsPOk, mn_SnapPoly, mn_Snap, ot_Pixel, ot_Symbol:
      begin;
        if ModeCombo.ItemIndex = 0 then
          if CheckEdit(Edit2.Text, Edit2) = true then Edit2.Text := FormatIt(StrToFloat(TmpString));
        if ModeCombo.ItemIndex = 1 then //if angle and Distance
          if AngleValidator.ValidateInput = false then Edit2.SetFocus;
      end;
  end;
end;

procedure TPointsCoordDialog.PointsStringGridSetEditText(Sender: TObject;
  ACol, ARow: Integer; const Value: string);
begin;
//
//end;

end;

procedure TPointsCoordDialog.PointsStringGridGetEditText(Sender: TObject;
  ACol, ARow: Integer; var Value: string);

var
  EditRect: TRect;
  Bounds: TRect;
  Agr, Ami, Ase: string;
begin
  EditBuffer := PointsStringGrid.Cells[PointsStringGrid.Col, PointsStringGrid.Row];
  EditRect := PointsStringGrid.CellRect(PointsStringGrid.Col, PointsStringGrid.Row);
  EditRect.Bottom := EditRect.Bottom + Panel2.Top + PointsStringGrid.Top + 1;
  EditRect.Top := EditRect.Top + Panel2.Top + PointsStringGrid.Top + 3;
  EditRect.Left := EditRect.Left + Panel2.Left + PointsStringGrid.Left + 1;
  EditRect.Right := EditRect.Right + Panel2.Left + PointsStringGrid.Left + 2;

 //:=PointsStringGrid.Font;

  if Inifile.LoadPointsCoordDataFromFile.InputMode = 0
    then
  begin;
    with StringEdit do
    begin
      StringEdit.BoundsRect := EditRect {Bounds};
      Text := PointsStringGrid.Cells[ACol, ARow];
      Visible := True;
      StringEditValid.Edit := StringEdit;
      SetFocus;
    end
  end
  else
  begin;
    StringEditValid.Edit := nil;
    EditRect.Right := EditRect.Right - 1;
    EditRect.Top := EditRect.Top - 1;
    EditRect.Bottom := EditRect.Bottom + 2;
    if Inifile.LoadPointsCoordDataFromFile.DegreeMode = 1
      then begin; //gradus+minute only !
      GradusEdit.BoundsRect := EditRect;
      MinuteEdit.BoundsRect := EditRect;
      GradusEdit.Width := trunc(GradusEdit.Width / 3);
      MinuteEdit.Width := MinuteEdit.Width - GradusEdit.Width + 2;
      MinuteEdit.Left := GradusEdit.Width + GradusEdit.Left - 2;
      GradusEdit.Visible := true;
      MinuteEdit.Visible := true;
      MinuteEdit.MaxLength := 10;
    end
    else
    begin;
      GradusEdit.BoundsRect := EditRect;
      MinuteEdit.BoundsRect := EditRect;
      SecondEdit.BoundsRect := EditRect;
      GradusEdit.Width := trunc(GradusEdit.Width / 3);
      MinuteEdit.Width := trunc(MinuteEdit.Width / 4.5) + 2;
      SecondEdit.Width := SecondEdit.Width - GradusEdit.Width - MinuteEdit.Width + 5;
      MinuteEdit.Left := GradusEdit.Width + GradusEdit.Left - 2;
      SecondEdit.Left := MinuteEdit.Left + MinuteEdit.Width - 3;
      GradusEdit.Visible := true;
      MinuteEdit.Visible := true;
      SecondEdit.Visible := true;
      MinuteEdit.MaxLength := 2;
      SecondEdit.MaxLength := 9;
    end;
    InputDegreeMode1(PointsStringGrid.Cells[PointsStringGrid.Col, PointsStringGrid.Row], Agr, Ami, ASe);
    GradusEdit.Text := Agr;
    MinuteEdit.Text := Ami;
    SecondEdit.Text := Ase;
    GradusEdit.SetFocus;

  end; //if


//    OrigFloat := FloatValidator.AsFloat;
//  end;
(*if (OldRow<>Arow) or (oldCol<>Arow)
then
if StrToFloat(PointsStringGrid.Cells[ACol,Arow])=0
  then
  begin;
  // SetflagZ:=true;
   PointsStringGrid.Cells[ACol,Arow]:='';
  end;
  *)
  OldRow := Arow;
  oldCol := Acol;


end;

procedure TPointsCoordDialog.PointsStringGridExit(Sender: TObject);
begin
  if (OldRow <= PointsStringGrid.RowCount - 1) then
  begin;
    if (PointsStringGrid.Cells[OldCol, Oldrow] = '')
      or (PointsStringGrid.Cells[OldCol, Oldrow] = '0') then PointsStringGrid.Cells[OldCol, Oldrow] := FormatIt(0);
    ResetCell(OldCol, OldRow);
  end;
end;

procedure TPointsCoordDialog.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
var
  ss: string;
begin
 //if Layer Fixed
  if Pproj(WinGisMainform.ActualProj).Layers^.TopLayer^.GetState(sf_Fixed) then
  begin;
    CanClose := true;
    Exit;
  end;

  if SimplyMode = true then
  begin;
    PointsStringGrid.Cells[1, PointsStringGrid.Row] := Edit4.Text;
    PointsStringGrid.Cells[2, PointsStringGrid.Row] := Edit5.Text;
  end;

  if ModalResult = mrOk then
  begin;
    CanClose := CheckArrayPoints; //if Points All Right
    if CanClose = false then Exit;

    if SetNewCoordinates = false then
    begin;
      CanClose := false;
    end;
  end;

end;

procedure TPointsCoordDialog.ResetCell(OldCol, OldRow: integer);
var
  Tmpvalue: double;
begin;
  if (OldCol <> 0) and (Inifile.LoadPointsCoordDataFromFile.InputMode = 0) then
  begin;
    TmpValue := StrToFloat(PointsStringGrid.Cells[OldCol, OldRow]);
   // TmpValue := Trunc(TmpValue) + Round(Frac(TmpValue) * 100) / 100;
    PointsStringGrid.Cells[OldCol, Oldrow] := FormatIt(TmpValue);
  end;
end;

// Set new data for selected point after "Apply" click

procedure TPointsCoordDialog.ApplyBtnClick(Sender: TObject);

var
  MyTmp, Angle: double;
  TmpPoint, Point1, Point2: TGrpoint;

  function MoveWithDistanceAngleX(var X: double; Distance, Angle: double): string;
  begin;
    X := X + Distance * Cos(Angle * PI / 180);
    Result := FormatIt(Trunc(X) + Round(Frac(X) * 100) / 100);
  end;

  function MoveWithDistanceAngleY(var Y: double; Distance, Angle: double): string;
  begin;
    Y := Y + Distance * Sin(Angle * PI / 180);
    Result := FormatIt(Trunc(Y) + Round(Frac(Y) * 100) / 100);
  end;

begin;
  InvisibleGradus; ;

  if OrtoBtn.Down = true
    then begin;
     //Change
    if PointsStringGrid.Row < 2
      then
    begin;
      MessageDlg(Mlgsection[89], mtError, [mbOk], 0);
      Exit;
    end;

    Point1 := GrPoint(StrToFloat(PointsStringgrid.Cells[1, PointsStringgrid.Row - 2]), StrToFloat(PointsStringgrid.Cells[2, PointsStringgrid.Row - 2]));
    Point2 := GrPoint(StrToFloat(PointsStringgrid.Cells[1, PointsStringgrid.Row - 1]), StrToFloat(PointsStringgrid.Cells[2, PointsStringgrid.Row - 1]));
    Angle := LineAngle(Point1, Point2);

    case Radiogroup1.ItemIndex of
      0: begin;
          if Angle + pi / 2 > 2 * pi then Angle := Angle - pi / 2;
          MovePointAngle(Point2, StrToFloat(Edit1.Text), Angle + pi / 2);
        end;
      1: begin;
          if Angle - pi / 2 < -2 * pi then Angle := Angle + pi / 2;
          MovePointAngle(Point2, StrToFloat(Edit1.Text), Angle - pi / 2);
        end;
    end;
    PointsStringGrid.Cells[1, PointsStringGrid.Row] := FormatIt(Point2.X);
    PointsStringGrid.Cells[2, PointsStringGrid.Row] := FormatIt(Point2.Y);

  end
  else
  begin;

    case ObjectType of
      ot_Poly, ot_CPoly, mn_InsPOk, mn_SnapPoly, mn_Snap, mn_New_Circle, mn_New_Arc, mn_Edit_Arc, mn_Edit_Circle, ot_Pixel, ot_Symbol, ot_Text:
        if CheckBox1.Checked = true then
    //Apply to point
          if ModeCombo.ItemIndex = 0 then //X,Y delta
          begin; //ModeCombo

            if PointCombo.ItemIndex = PointCombo.Items.Count - 1 then
       //"Previous" in point's list//calculate for view in edit current
            begin;
              POintsStringGrid.Cells[1, POintsStringGrid.Row] := FormatIt(StrToFloat(PointsStringGrid.Cells[1, LastInputString]) + StrToFloat(Edit1.Text));
              POintsStringGrid.Cells[2, POintsStringGrid.Row] := FormatIt(StrToFloat(PointsStringGrid.Cells[2, LastInputString]) + StrToFloat(Edit2.Text));
            end
            else
            begin;
              POintsStringGrid.Cells[1, POintsStringGrid.Row] := FormatIt(StrToFloat(PointsStringGrid.Cells[1, StrToInt(PointCombo.Text) - 1]) + StrToFloat(Edit1.Text));
              POintsStringGrid.Cells[2, POintsStringGrid.Row] := FormatIt(StrToFloat(PointsStringGrid.Cells[2, StrToInt(PointCombo.Text) - 1]) + StrToFloat(Edit2.Text));
            end;
          end
          else
          begin; //Distance,Angle

            if PointCombo.ItemIndex = PointCombo.Items.Count - 1 then
       //"Previous" in point's list//calculate for view in edit current
            begin;
              MyTmp := StrToFloat(PointsStringGrid.Cells[1, LastInputString]);
              POintsStringGrid.Cells[1, POintsStringGrid.Row] := MoveWithDistanceAngleX(MyTmp, StrToFloat(Edit1.Text), StrToFloat(Edit2.Text));
              MyTmp := StrToFloat(PointsStringGrid.Cells[2, LastInputString]);
              POintsStringGrid.Cells[2, POintsStringGrid.Row] := MoveWithDistanceAngleY(MyTmp, StrToFloat(Edit1.Text), StrToFloat(Edit2.Text));
            end
            else
            begin;
              MyTmp := StrToFloat(PointsStringGrid.Cells[1, StrToInt(PointCombo.Text) - 1]);
              POintsStringGrid.Cells[1, POintsStringGrid.Row] := MoveWithDistanceAngleX(MyTmp, StrToFloat(Edit1.Text), StrToFloat(Edit2.Text));
              MyTmp := StrToFloat(PointsStringGrid.Cells[2, StrToInt(PointCombo.Text) - 1]);
              POintsStringGrid.Cells[2, POintsStringGrid.Row] := MoveWithDistanceAngleY(MyTmp, StrToFloat(Edit1.Text), StrToFloat(Edit2.Text));
            end;

          end
        else //Not "Apply"
        begin;
          if ModeCombo.ItemIndex = 0 then
          begin;
            POintsStringGrid.Cells[1, POintsStringGrid.Row] := FormatIt(StrToFloat(PointsStringGrid.Cells[1, PointsStringGrid.Row]) + StrToFloat(Edit1.Text));
            POintsStringGrid.Cells[2, POintsStringGrid.Row] := FormatIt(StrToFloat(PointsStringGrid.Cells[2, PointsStringGrid.Row]) + StrToFloat(Edit2.Text));
          end
          else
          begin; //Distance,Angle
            MyTmp := StrToFloat(PointsStringGrid.Cells[1, POintsStringGrid.Row]);
            POintsStringGrid.Cells[1, POintsStringGrid.Row] := MoveWithDistanceAngleX(MyTmp, StrToFloat(Edit1.Text), StrToFloat(Edit2.Text));
            MyTmp := StrToFloat(PointsStringGrid.Cells[2, POintsStringGrid.Row]);
            POintsStringGrid.Cells[2, POintsStringGrid.Row] := MoveWithDistanceAngleY(MyTmp, StrToFloat(Edit1.Text), StrToFloat(Edit2.Text));
          end
        end;

    end; //Case

  end; //if Ortho<>Down
end;

//check points

function TPointsCoordDialog.CheckArrayPoints: boolean;
var
  i: integer;
  X1, X2, Y1, Y2: double;
begin;

  Result := false;


  if (ObjectType = ot_poly) or (ObjectType = ot_Cpoly)
    then
 //At first, 2 neighborad points can't be same
    if IniFile.LoadPointsCoordDataFromFile.InputMode = 0 then
    begin;
      for i := 1 to PointsStringGrid.RowCount - 1 do
        if (PointsStringGrid.Cells[1, i - 1] = PointsStringGrid.Cells[1, i])
          and
          (PointsStringGrid.Cells[2, i - 1] = PointsStringGrid.Cells[2, i]) then
        begin;
          MessageDlg(MlgSection[38] + #13 + Format(MlgSection[39], [i, i + 1]), mtError, [mbOk], 0);
          Exit;
        end;
    end
    else
    begin;
      for i := 1 to PointsStringGrid.RowCount - 1 do
      begin;
        X1 := InputDegreeMode(PointsStringGrid.Cells[1, i], Inifile.LoadPointsCoordDataFromFile.DegreeMode);
        X2 := InputDegreeMode(PointsStringGrid.Cells[1, i - 1], Inifile.LoadPointsCoordDataFromFile.DegreeMode);
        Y1 := InputDegreeMode(PointsStringGrid.Cells[2, i], Inifile.LoadPointsCoordDataFromFile.DegreeMode);
        Y2 := InputDegreeMode(PointsStringGrid.Cells[2, i - 1], Inifile.LoadPointsCoordDataFromFile.DegreeMode);

        if (X1 = X2) and (Y1 = Y2) then
        begin;
          MessageDlg(MlgSection[38] + #13 + Format(MlgSection[39], [i, i + 1]), mtError, [mbOk], 0);
          Exit;
        end;
      end;
    end;

  case ObjectType of

    mn_Snap: //2a,2b
      with PProj(Project)^ do
        if ((PointsStringGrid.RowCount < 3) and (Ppoly(OldSnapItem.mptr)^.GetObjType = ot_CPoly))
          or
          ((PointsStringGrid.RowCount < 2) and (Ppoly(OldSnapItem.mptr)^.GetObjType = ot_Poly)) then
        begin;
          MessageDlg(MlgSection[47], mtError, [mbOk], 0);
          Exit;
        end;

  end;
  Result := true;
//Screen.Cursor:=crDefault;
end;
//special array for circle

procedure TPointsCoordDialog.SetTmpArray(i: integer);
begin;
  TmpArray[i].X := Trunc(StrToFloat(PointsStringGrid.Cells[1, i]) * 100);
  TmpArray[i].Y := Trunc(StrToFloat(PointsStringGrid.Cells[2, i]) * 100);
end;
//special array for arcs

procedure TPointsCoordDialog.SetTmpArray1(i: integer);
begin;
  TmpArray1[i].X := Trunc(StrToFloat(PointsStringGrid.Cells[1, i]) * 100);
  TmpArray1[i].Y := Trunc(StrToFloat(PointsStringGrid.Cells[2, i]) * 100);
end;

//prepare/set new coordinates for edit - save will call from Prophndl

procedure TPointsCoordDialog.SetNewCoordForEdit;
var
  i, j: integer;
  TmpPoint: TDPoint;
  Rect: TDRECT;
  RealCount, TmpMode: integer;
  Radius: longint;
  AEllipse: PEllipse;
begin;

  Screen.Cursor := crHourGlass;
{TmpMode:= Inifile.LoadPointsCoordDataFromFile.InputMode;
if Inifile.LoadPointsCoordDataFromFile.InputMode=1 then begin;
 Inifile.LoadPointsCoordDataFromFile.InputMode:=0;
 TmpInputMode:=1;
 TmpDegreeMode:= Inifile.LoadPointsCoordDataFromFile.DegreeMode;
 ResetPoints(false);
                                              end;
 }
// PProj(Project)^.OldSnapItem:=TmpOld;
//  PProj(Project)^.OldSnapItem:= PProj(Project)^.FindOLdSnapItem;
  if not (Pproj(WinGisMainform.ActualProj).Layers^.TopLayer^.GetState(sf_Fixed)) then
    case ObjectType of
  {Move Existing Point}
      mn_Edit_Point:
        begin;
          TmpPoint.Init(0, 0);
          Rect.InitByRect(PProj(Project)^.OldSnapItem.ClipRect);
          with PProj(Project)^ do
            with PPixel(OldSnapItem.mptr)^ do
            begin;
              TmpPoint.X := Trunc(StrToFloat(PointsStringGrid.Cells[1, 0]) * 100);
              TmpPOint.Y := Trunc(StrToFloat(PointsStringGrid.Cells[2, 0]) * 100);
              MoveRel(TmpPoint.X - Position.X, TmpPoint.Y - Position.Y);
            end;
          RedrawWithAllAttached(PProj(Project), PView(PProj(Project)^.OldSnapItem.mptr), Rect);
        end;

      mn_Edit_Symbol: {Move symbol}
        begin;
          TmpPoint.Init(0, 0);
          Rect.InitByRect(PProj(Project)^.OldSnapItem.ClipRect);
          with PProj(Project)^ do
            with PSymbol(OldSnapItem.mptr)^ do
            begin;
              TmpPoint.X := Trunc(StrToFloat(PointsStringGrid.Cells[1, 0]) * 100);
              TmpPOint.Y := Trunc(StrToFloat(PointsStringGrid.Cells[2, 0]) * 100);
              MoveRel(TmpPoint.X - Position.X, TmpPoint.Y - Position.Y);
            end;
          RedrawWithAllAttached(PProj(Project), PView(PProj(Project)^.OldSnapItem.mptr), Rect);
        end;

      mn_Edit_Text: {Move Text}
        begin;
          TmpPoint.Init(0, 0);
          Rect.InitByRect(PProj(Project)^.OldSnapItem.ClipRect);
          with PProj(Project)^ do
            with PText(OldSnapItem.mptr)^ do
            begin;
              TmpPoint.X := Trunc(StrToFloat(PointsStringGrid.Cells[1, 0]) * 100);
              TmpPOint.Y := Trunc(StrToFloat(PointsStringGrid.Cells[2, 0]) * 100);
              MoveRel(TmpPoint.X - Pos.X, TmpPoint.Y - Pos.Y);
            end;
          RedrawWithAllAttached(PProj(Project), PView(PProj(Project)^.OldSnapItem.mptr), Rect);
        end;

      mn_Snap: //Change poly
        begin;
          TmpPoint.Init(0, 0);
          Rect.InitByRect(PProj(Project)^.OldSnapItem.ClipRect);
          with PPoly(PProj(Project)^.OldSnapItem.mptr)^ do
            Data^.DeleteAll;
          for i := 0 to PointsStringGrid.RowCount - 1 do
          begin;
            TmpPoint.X := Trunc(StrToFloat(PointsStringGrid.Cells[1, i]) * 100);
            TmpPoint.Y := Trunc(StrToFloat(PointsStringGrid.Cells[2, i]) * 100);
            PPoly(PProj(Project)^.OldSnapItem.mptr)^.InsertPoint(TmpPoint)
          end;
          if PProj(Project)^.OldSnapItem.GetObjType = ot_Cpoly then
          begin;
            TmpPoint.X := Trunc(StrToFloat(PointsStringGrid.Cells[1, 0]) * 100);
            TmpPoint.Y := Trunc(StrToFloat(PointsStringGrid.Cells[2, 0]) * 100);
            PPoly(PProj(Project)^.OldSnapItem.mptr)^.InsertPoint(TmpPoint)
          end;

          RedrawWithAllAttached(PProj(Project), PView(PProj(Project)^.OldSnapItem^.mptr), Rect);
        end; //mn_Snap

      mn_Edit_Arc: //Edit Arc
        begin;
  (*  if (FloatValidator1.asFloat <= 0) and (PointsStringGrid.RowCount = 1) then
     begin;
      MessageDlg(Mlgsection[58] + Mlgsection[57],mtError, [mbOk],0);
      ActiveControl := AltEdit1;
      Result := false;
      Exit;
     end;
    if AngleValidator2.ValidInput = false then
     begin;
      MessageDlg(Mlgsection[59],mtError, [mbOk],0);
      ActiveControl := AltEdit2;
      Result := false;
      Exit;
     end;
    if AngleValidator3.ValidInput = false then
     begin;
      MessageDlg(Mlgsection[59],mtError, [mbOk],0);
      ActiveControl := AltEdit3;
      Result := false;
      Exit;
     end;
    *)
          Rect.InitByRect(PProj(Project)^.OldSnapItem.ClipRect);

          with PProj(Project)^ do
            with PEllipseArc(OldSnapItem.mptr)^ do
            begin;
              Position.X := Trunc(StrToFloat(PointsStringGrid.Cells[1, 0]) * 100);
              Position.Y := Trunc(StrToFloat(PointsStringGrid.Cells[2, 0]) * 100);
              PrimaryAxis := Trunc(FloatValidator1.AsFloat * 100);
              SecondaryAxis := Trunc(FloatValidator1.AsFloat * 100);
              BeginAngle := AngleValidator2.AsFloat * PI / 180;
              EndAngle := AngleValidator3.AsFloat * PI / 180;
              CalculateCliprect;

            end;
          RedrawWithAllAttached(PProj(Project), PView(PProj(Project)^.OldSnapItem.mptr), Rect);
        end;

      mn_Edit_Circle: //Edit Circle
        begin;
   (* if CheckParamsOfEditCircleBeforeApplyIt = false then
     begin;
      Result := false;
      Exit;
     end;
     *)
{$IFNDEF WMLT}
          AEllipse := PEllipse(PProj(Project)^.OldSnapItem.mptr);
          RecalculateCirlcle(OldCircleModeType);
          FuncChangeEllipse(AEllipse, Project, CircleCenter, CircleRadius);
{$ENDIF}
        end;

    end; //case
  Inifile.LoadPointsCoordDataFromFile.InputMode := TmpMode;
  Screen.Cursor := crDefault;
end;

 //prepare/set new coordinates

function TPointsCoordDialog.SetNewCoordinates: boolean;

var
  i, j: integer;
  TmpPoint: TDPoint;
  Rect: TDRECT;
  RealCount: integer;
  Radius: longint;
  AEllipse: PEllipse;
  TmpMode: integer;
  T_Lib, T_Sym: AnsiString;
  SymRollUp: TSymbolRollupForm;

 //set refpoint for text
  function ForStartText: boolean;
  begin;
    TmpPoint.X := Trunc(SToI(PointsStringGrid.Cells[1, 0]));
    TmpPoint.Y := Trunc(SToI(PointsStringGrid.Cells[2, 0]));
    with PProj(Project)^.TextRect^ do
      if IsOn = true then
      begin;
        IsOn := false;
        StartPos.Move(TmpPoint.X - StartPos.X, TmpPoint.Y - StartPos.Y);
        Result := true;
      end
      else
      begin;
        PProj(Project)^.MsgLeftClick(TmpPOint);
        Result := false;
      end;

  end;

begin;
  Result := true;
  Screen.Cursor := crHourGlass;
  TmpMode := Inifile.LoadPointsCoordDataFromFile.InputMode;
  if Inifile.LoadPointsCoordDataFromFile.InputMode = 1 then begin;
    Inifile.LoadPointsCoordDataFromFile.InputMode := 0;
    TmpInputMode := 1;
    ResetPoints(false);
  end;

  case ObjectType of

    ot_Text: //set new text coord
      begin;
        Rect.Init;
        Rect.A := PProj(Project)^.TextRect^.StartPos;
        Rect.B := PProj(Project)^.AOldMousePos;
        if PointsStringGrid.RowCount = 1 then
        begin;
          if FloatValidator1.ValidInput = false then
          begin;
            MessageDlg(Mlgsection[59], mtError, [mbOk], 0);
            Screen.Cursor := crDefault;
            AltEdit2.SetFocus;
            Result := false;
            Exit;
          end;
          if FloatValidator2.ValidInput = false then
          begin;
            MessageDlg(Mlgsection[59], mtError, [mbOk], 0);
            Screen.Cursor := crDefault;
            AltEdit3.SetFocus;
            Result := false;
            Exit;
          end;
          if (FloatValidator1.AsFloat = 0) and (FloatValidator2.AsFloat = 0) and (SimplyMode = false) then
          begin;
            MessageDlg(Mlgsection[68], mtError, [mbOk], 0);
            Screen.Cursor := crDefault;
            AltEdit1.SetFocus;
            Result := false;
            Exit;
          end;

          if ForStartText = false then
          begin;
            PProj(Project).PInfo.RedrawRect(Rect);
            Exit;
          end;

          TmpPoint.X := Trunc(TmpPoint.X + FloatValidator1.AsFloat * 100);
          TmpPoint.Y := Trunc(TmpPoint.Y + FloatValidator2.AsFloat * 100);

          PProj(Project)^.MsgMouseMove(TmpPOint);
          PProj(Project)^.MsgLeftClick(TmpPOint);

        end
        else
        begin;
          ForStartText;
          TmpPoint.X := Trunc(SToI(PointsStringGrid.Cells[1, 1]));
          TmpPoint.Y := Trunc(SToI(PointsStringGrid.Cells[2, 1]));
          PProj(Project)^.MsgMouseMove(TmpPOint);
          PProj(Project)^.MsgLeftClick(TmpPOint);
        end;
        PProj(Project).PInfo.RedrawRect(Rect);
      end;

    ot_Pixel: //set new Point(s) coord
      begin;
        for i := 0 to PointsStringGrid.RowCount - 1 do
        begin;
          TmpPoint.X := Trunc(SToI(PointsStringGrid.Cells[1, i]));
          TmpPoint.Y := Trunc(SToI(PointsStringGrid.Cells[2, i]));
          PProj(Project)^.MsgLeftClick(TmpPOint);
        end;
      end;

    ot_Symbol: //set new symbol(s) coord
      begin;
        ProjectActSymbol := PProj(WingisMainForm.ActualProj).ActualSym.GetName;
        ProjectActSymbolLibrary := PProj(WingisMainForm.ActualProj).ActSymLib;
        SymRollUp := TSymbolRollupForm(UserInterface.RollupNamed['SymbolRollupForm']);
        for i := 0 to PointsStringGrid.RowCount - 1 do
        begin;
          ASymLibInfo.LibIndex := SymLibLIst.Items[i];
          ASymInfo := ASymLibInfo.GetSymInfo;
          if ASymLibInfo.LibIndex = 0 then T_Lib := '' else
            T_Lib := ASymLibInfo.GetLibName(SymLibLIst.Items[i]);
          T_Sym := PointsStringGrid.Cells[0, i];
          if T_Lib <> PProj(WingisMainForm.ActualProj).ActSymLib then
          begin;
            SymRollUp.SetActLib(ExtractFileName(T_Lib));
            SymRollUp.SetActSymbol(ASymInfo.GetSym(ASymINfo.IndexOf(PointsStringGrid.Cells[0, i])));
          end
          else
          begin;
            if T_Sym <> PProj(WingisMainForm.ActualProj).ActualSym.GetName
              then
              SymRollUp.SetActSymbol(ASymInfo.GetSym(ASymINfo.IndexOf(PointsStringGrid.Cells[0, i])));
          end;

          TmpPoint.X := Trunc(SToI(PointsStringGrid.Cells[1, i]));
          TmpPoint.Y := Trunc(SToI(PointsStringGrid.Cells[2, i]));
          PProj(Project)^.MsgLeftClick(TmpPOint);
        end;

        ASymLibInfo.LibIndex := GetPositionOfSymbolsSet(AsymLibInfo, ProjectActSymbolLibrary);
        SymRollUp.SetActLib(ProjectActSymbolLibrary);
        ASymInfo := ASymLibInfo.GetSymInfo;
        SymRollUp.SetActSymbol(ASymInfo.GetSym(ASymINfo.IndexOf(ProjectActSymbol)));



(*  {Move Existing Point}
  mn_Edit_Point:
   begin;
    TmpPoint.Init(0,0);
    Rect.InitByRect(PProj(Project)^.OldSnapItem.ClipRect);
    with PProj(Project)^ do
     with PPixel(OldSnapItem.mptr)^ do
      begin;
       TmpPoint.X := Trunc(SToF(PointsStringGrid.Cells[1,0]) * 100);
       TmpPOint.Y := Trunc(SToF(PointsStringGrid.Cells[2,0]) * 100);
       MoveRel(TmpPoint.X - Position.X,TmpPoint.Y - Position.Y);
      end;
    RedrawWithAllAttached(PProj(Project),PView(PProj(Project)^.OldSnapItem.mptr),Rect);
   end;

  mn_Edit_Symbol: {Move symbol}
   begin;
    TmpPoint.Init(0,0);
    Rect.InitByRect(PProj(Project)^.OldSnapItem.ClipRect);
    with PProj(Project)^ do
     with PSymbol(OldSnapItem.mptr)^ do
      begin;
       TmpPoint.X := Trunc(SToF(PointsStringGrid.Cells[1,0]) * 100);
       TmpPOint.Y := Trunc(SToF(PointsStringGrid.Cells[2,0]) * 100);
       MoveRel(TmpPoint.X - Position.X,TmpPoint.Y - Position.Y);
      end;
    RedrawWithAllAttached(PProj(Project),PView(PProj(Project)^.OldSnapItem.mptr),Rect);
   end;
                    *)
(*  mn_Edit_Text: {Move Text}
   begin;
    TmpPoint.Init(0,0);
    Rect.InitByRect(PProj(Project)^.OldSnapItem.ClipRect);
    with PProj(Project)^ do
     with PText(OldSnapItem.mptr)^ do
      begin;
       TmpPoint.X := Trunc(SToF(PointsStringGrid.Cells[1,0]) * 100);
       TmpPOint.Y := Trunc(SToF(PointsStringGrid.Cells[2,0]) * 100);
       MoveRel(TmpPoint.X - Pos.X,TmpPoint.Y - Pos.Y);
      end;
    RedrawWithAllAttached(PProj(Project),PView(PProj(Project)^.OldSnapItem.mptr),Rect);
   end;
     *)
      end;
    ot_Cpoly, ot_Poly: {New Poly}
      begin;
        TmpPoint.Init(0, 0);
        PProj(Project)^.SelRect.EndIt(PProj(Project)^.Pinfo);
        with PProj(Project)^.NewPoly^ do
        begin;
          EndIt(PProj(Project)^.Pinfo);

          case TypeCombo.ItemIndex of
       // 0:
            1:
              begin; //Ortogonal
                PProj(Project)^.NewPoly.bDrawOrtho := false;
                WingisMainForm.ActualChild.DrawOrthoOnOff;
              end;
            2: //3 point
              begin;
                PProj(Project)^.bDraw3Points := false;
                WingisMainForm.ActualChild.DrawRectBy3Points;
              end;
       //rubber
            3:
              begin;
                PProj(Project)^.bDrawRubberBox := false;
                WingisMainForm.ActualChild.DrawRectByRubberBox;

              end;
          end;

          for i := 0 to PointsStringGrid.RowCount - 1 do
          begin;
            TmpPoint.X := Trunc(SToI(PointsStringGrid.Cells[1, i]));
            TmpPoint.Y := Trunc(SToI(PointsStringGrid.Cells[2, i]));
            PProj(Project)^.MsgLeftClick(TmpPOint);
            if TypeCombo.ItemIndex <> 3 then LineOn := True;
          end;

          if TypeCombo.ItemIndex = 3 then EndIt(PProj(Project)^.Pinfo);

        end; //with
      end; //ot_Poly
    mn_InsPOk, mn_SnapPoly:
      begin;

      end;

(*  mn_Snap: //Change poly
   begin;
    TmpPoint.Init(0,0);
    Rect.InitByRect(PProj(Project)^.OldSnapItem.ClipRect);
    with PPoly(PProj(Project)^.OldSnapItem.mptr)^ do
     Data^.DeleteAll;
    for i := 0 to PointsStringGrid.RowCount - 1 do
     begin;
      TmpPoint.X := Trunc(SToF(PointsStringGrid.Cells[1,i]) * 100);
      TmpPoint.Y := Trunc(SToF(PointsStringGrid.Cells[2,i]) * 100);
      PPoly(PProj(Project)^.OldSnapItem.mptr).InsertPoint(TmpPoint)
     end;
    if PProj(Project)^.OldSnapItem.GetObjType = ot_Cpoly then
     begin;
      TmpPoint.X := Trunc(SToF(PointsStringGrid.Cells[1,0]) * 100);
      TmpPoint.Y := Trunc(SToF(PointsStringGrid.Cells[2,0]) * 100);
      PPoly(PProj(Project)^.OldSnapItem.mptr).InsertPoint(TmpPoint)
     end;

    RedrawWithAllAttached(PProj(Project),PView(PProj(Project)^.OldSnapItem.mptr),Rect);
   end; //mn_Snap
  *)
    mn_New_Circle: //Input new Circle
      with PProj(Project)^.MenuHandler.SubHandler as TRubberEllipse do
      begin;

        if SimplyMode = false then //from grid
        begin;

          SetTmpArray(0);
          RealCount := PrepareCircleForInsert;
          if RealCount = -1 then
          begin;
            Result := false;
            Exit;
          end;
          for i := 1 to RealCount - 2 do
            SetTmpArray(i);
          OnUndo(0);
          SetArray(TmpArray);
          if (CircleType = 4) then
            if (PointsStringGrid.RowCount = 2) then
              RealCount := 3
            else
              RealCount := PointsStringGrid.RowCount;
        end
        else
        begin;
          RealCount := PointsStringGrid.RowCount;
          Onundo(0);
          for i := 0 to RealCount - 2 do
            SetTmpArray(i);
          TmpArray[RealCount - 1].X := 100 * StrToFloat(FormatIt(StrToFloat(Edit4.Text)));
          TmpArray[RealCount - 1].Y := 100 * StrToFloat(FormatIt(StrToFloat(Edit5.Text)));
          SetArray(TmpArray);
        end;

        ImportNewCoords(RealCount);
      end;

    mn_New_Arc: //input new Arc
      with PProj(Project)^.MenuHandler.SubHandler as TRubberArc do
      begin;

        if SimplyMode = false then //from grid
        begin;

          SetTmpArray1(0);
          RealCount := PrepareArcForInsert;
          if RealCount = -1 then
          begin;
            Result := false;
            Exit;
          end;
          for i := 1 to RealCount - 2 do
            SetTmpArray1(i);
          OnUndo(0);
          SetArray(TmpArray1);
          if ArcType = 1 then
            if PointsStringGrid.RowCount = 1 then
              RealCount := 4
            else
              RealCount := PointsStringGrid.RowCount + 1;

          if ArcType = 2 then
            if (PointsStringGrid.RowCount = 2) then
              RealCount := 3
            else
              RealCount := PointsStringGrid.RowCount;

          if ArcType = 3 then
            RealCount := PointsStringGrid.RowCount;
        end
        else
        begin;
          RealCount := PointsStringGrid.RowCount;
          Onundo(0);
          for i := 0 to RealCount - 2 do
            SetTmpArray1(i);
          TmpArray1[RealCount - 1].X := 100 * StrToFloat(FormatIt(StrToFloat(Edit4.Text)));
          TmpArray1[RealCount - 1].Y := 100 * StrToFloat(FormatIt(StrToFloat(Edit5.Text)));
          SetArray(TmpArray1);
        end;

        ImportNewCoords(RealCount);
      end;

    mn_Edit_Arc: //Edit Arc
      begin;
        if (FloatValidator1.asFloat <= 0) and (PointsStringGrid.RowCount = 1) then
        begin;
          MessageDlg(Mlgsection[58] + Mlgsection[57], mtError, [mbOk], 0);
          AltEdit1.SetFocus;
          Result := false;
          Exit;
        end;
        if AngleValidator2.ValidInput = false then
        begin;
          MessageDlg(Mlgsection[59], mtError, [mbOk], 0);
          AltEdit2.SetFocus;
          Result := false;
          Exit;
        end;
        if AngleValidator3.ValidInput = false then
        begin;
          MessageDlg(Mlgsection[59], mtError, [mbOk], 0);
          AltEdit3.SetFocus;
          Result := false;
          Exit;
        end;

(*    Rect.InitByRect(PProj(Project)^.OldSnapItem.ClipRect);

    with PProj(Project)^ do
     with PEllipseArc(OldSnapItem.mptr)^ do
      begin;
       Position.X := Trunc(SToF(PointsStringGrid.Cells[1,0]) * 100);
       Position.Y := Trunc(SToF(PointsStringGrid.Cells[2,0]) * 100);
       PrimaryAxis := Trunc(FloatValidator1.AsFloat * 100);
       SecondaryAxis := Trunc(FloatValidator1.AsFloat * 100);
       BeginAngle := AngleValidator2.AsFloat * PI / 180;
       EndAngle := AngleValidator3.AsFloat * PI / 180;
       CalculateCliprect;

      end;
    RedrawWithAllAttached(PProj(Project),PView(PProj(Project)^.OldSnapItem.mptr),Rect);
  *)
      end;

    mn_Edit_Circle: //Edit Circle
      begin;
        if CheckParamsOfEditCircleBeforeApplyIt = false then
        begin;
          Result := false;
          Exit;
        end;
  (*
    AEllipse := PEllipse(PProj(Project)^.OldSnapItem.mptr);
    RecalculateCirlcle(OldCircleModeType);
    FuncChangeEllipse(AEllipse,Project,CircleCenter,CircleRadius);
   *)
      end;

  end; //Case

  IniFile.SimplyMode := SimplyMode;
  IniFile.WriteF4SimplyMode;
  Inifile.LoadPointsCoordDataFromFile.InputMode := TmpMode;
  Screen.Cursor := crDefault;
end;

//Show value as "x.xx" mode (two digits after point)

function TPointsCoordDialog.FormatIt(s: double): string;
begin;
//TmpInputMode:=Inifile.LoadPointsCoordDataFromFile.InputMode;
//TmpDegreeMode:=Inifile.LoadPointsCoordDataFromFile.DegreeMode;
  if (Inifile.LoadPointsCoordDataFromFile.InputMode = 0) or (SetNewData = true)
    then
    Result := Format('%.2f', [s])
  else
    Result := OutputDegreeMode(s);
end;

function TPointsCoordDialog.FormatIt1(s: double): string;
begin;
  Result := Format('%.2f', [s]);
end;

function TPointsCoordDialog.FormatIt2(s: double): string;
begin;
  Result := Format('%.2f', [s]);
end;
//Detect Previous and Next point. It very important for some mode,
//for mn_inspok, at example

function TPointsCoordDialog.DetectFramePoint: integer;
begin;
  Result := -1;
  if ((PrevPoint = 0) and (NextPoint = PointsStringGrid.RowCount - 1))
    or
    ((NextPoint = 0) and (PrevPoint = PointsStringGrid.RowCount - 1)) then Exit;
  if PrevPoint > NextPoint then
    Result := PrevPoint
  else
    Result := NextPoint
end;

//Exit from Edit and Check

procedure TPointsCoordDialog.EditExit(Sender: TObject);
var
  TmpString: string;
begin
  TmpString := (Sender as TEdit).Text;

  case ObjectType of

    Ot_Poly, Ot_Cpoly, mn_InsPOk, mn_SnapPoly, mn_Snap:
      if CheckEdit((Sender as TEdit).Text, (Sender as TEdit)) = true then (Sender as TEdit).Text := FormatIt(StrToFloat(TmpString));

  end;

end;

procedure TPointsCoordDialog.EditExit1(Sender: TObject);
var
  TmpString: string;
  Number: double;
begin
  if Inifile.LoadPointsCoordDataFromFile.InputMode = 0
    then
  begin;
    TmpString := Trim((Sender as TEdit).Text);
    if TmpString = '' then
    begin;
      (Sender as TEdit).SetFocus;
      Exit;
    end;
    try
      Number := StrToFloat(TmpString);
    except
      (Sender as TEdit).SetFocus;
      Exit;
    end;
    (Sender as TEdit).Text := FormatIt(StrToFloat(TmpString));
  end;
end;


//Can user change mode for poly ?

function TPointsCoordDialog.CheckAvMode: boolean;
begin;
  Result := true;

  case ObjectType of
    mn_Poly, mn_CPoly, mn_snap:
      begin;
        if TypeCombo.ItemIndex in [0, 1] then Exit;
        if ((TypeCombo.ItemIndex = 2) and (PointsStringGrid.RowCount = 3)) //3point
          or
          ((TypeCombo.ItemIndex = 3) and (PointsStringGrid.RowCount = 2)) then
     //Rubber
        begin;
          MessageDlg(MlgSection[48], mtError, [mbOk], 0);
          Result := false;
          Exit;
        end;
      end;

  end;

end;



//procedure set

procedure TPointsCoordDialog.SetText(AltLabel1Text, AltLabel2Text, PointsCoordGridFirst: string);
begin;
  AltLabel1.Caption := AltLabel1Text;
  AltLabel2.Caption := AltLabel2Text;
  POintsStringGrid.Cells[0, 0] := PointsCoordGridFirst;
end;

//Set Grid height, also additional Edit show or now

procedure TPointsCoordDialog.SetCutGrid(EditCount: integer);
  procedure SetOptions(Height: integer; FirstVisible, SecondVisible: boolean);
  begin;
    Panel2.Height := ReturnCorrected(Height);

    if FirstVisible = true then
    begin;
      AltEdit1.Visible := true;
      AltLabel1.Visible := true;
      AltEdit1.Width := round(Panel2.Width / 2) + 4;
      AltLabel1.Left := Panel2.Left + ReturnCorrected(8);
      AltEdit1.Top := Panel2.Top + Panel2.Height + InsBtn.Height + ReturnCorrected(6);
      AltLabel1.Top := AltEdit1.Top + trunc(AltEdit1.Height / 2 - AltLabel1.Height / 2);
      AltEdit1.Left := Panel2.Left + trunc(Panel2.Width / 2) - ReturnCorrected(8);
    end
    else
    begin;
      AltEdit1.Visible := false;
      AltLabel1.Visible := false;
    end;

    if SecondVisible = true then
    begin;
      AltEdit2.Visible := true;
      AltLabel2.Visible := true;
      AltEdit2.Width := round(Panel2.Width / 2) + 4;
      AltLabel2.Left := Panel2.Left + ReturnCorrected(8);
      AltEdit2.Top := Panel2.Top + Panel2.Height + InsBtn.Height + ReturnCorrected(2);
      if FirstVisible = true then
        AltEdit2.Top := AltEdit2.Top + AltEdit2.Height + ReturnCorrected(3)
      else
        AltEdit2.Top := AltEdit2.Top + ReturnCorrected(8);
      AltLabel2.Top := AltEdit2.Top + trunc(AltEdit2.Height / 2 - AltLabel2.Height / 2);
      AltEdit2.Left := Panel2.Left + trunc(Panel2.Width / 2) - ReturnCorrected(8);
    end
    else
    begin;
      AltEdit2.Visible := false;
      AltLabel2.Visible := false;
    end;

    InsBtn.Top := Panel2.Top + Panel2.Height + 2;
    AddBtn.Top := InsBtn.Top;
    DelBtn.Top := InsBtn.Top;

  end;

begin;
  case EditCount of
    0: SetOptions(107, false, false);
    1: SetOptions(76, false, true);
    2: SetOptions(53, true, true);
  end;

  if (ObjectType = mn_Edit_Circle) or (ObjectType = mn_New_Circle) then
    case CircleType of
      1: SetText('', MlgSection[55], MlgSection[22]);
      2: SetText(MlgSection[23], MlgSection[32], MlgSection[24] + '1');
      3: SetText('', '', MlgSection[24] + '1');
      4:
        begin;
          SetText(MlgSection[55], MlgSection[61], MlgSection[24] + '1');
          if PointsStringGrid.RowCount = 2 then
          begin;
            AltEdit2.Visible := false;
            Combobox1.Visible := true;
            Combobox1.Top := AltEdit2.Top;
            Combobox1.Left := AltEdit2.Left;
            Combobox1.Width := AltEdit2.Width;
            Combobox1.Height := AltEdit2.Height;
            Combobox1.ItemIndex := 0;
          end
          else
            Combobox1.Visible := false;

        end;
      5:
        if PProj(Project).PInfo.CoordinateSystem.CoordinateType <> ctGeodatic then
          SetText(MlgSection[41], MlgSection[42], MlgSection[24] + '1')
        else
          SetText(MlgSection[42], MlgSection[41], MlgSection[24] + '1')

    end;

  if ObjectType = ot_Text then
  begin;
    if PointsStringGrid.RowCount = 2 then
    //      SetText(MlgSection[55],MlgSection[75],MlgSection[22])
      //                   else
      if ObjectType = ot_Text then
      begin;
        if PointsStringGrid.RowCount = 2 then
          if PProj(Project).PInfo.CoordinateSystem.CoordinateType <> ctGeodatic then
            SetText(MlgSection[41], MlgSection[42], MlgSection[24] + '1')
          else
            SetText(MlgSection[42], MlgSection[41], MlgSection[24] + '1')
      end;

  end;

  if (ObjectType = mn_Edit_Arc) or (ObjectType = mn_New_Arc) then
    case ArcType of
      1:
        if PointsStringGrid.RowCount = 1 then
          SetText(MlgSection[55], MlgSection[75], MlgSection[22])
        else
          SetText('', MlgSection[55], MlgSection[22]);
      2:
        begin;
          SetText(MlgSection[55], MlgSection[61], MlgSection[24] + '1');
          if PointsStringGrid.RowCount = 2 then
          begin;
            AltEdit2.Visible := false;
            Combobox1.Visible := true;
            Combobox1.Top := AltEdit2.Top;
            Combobox1.Left := AltEdit2.Left;
            Combobox1.Width := AltEdit2.Width;
            Combobox1.Height := AltEdit2.Height;
            Combobox1.ItemIndex := 0;
          end
          else
            Combobox1.Visible := false;
        end;
   //  3:

    end;

end;

//prepare for circle

procedure TPointsCoordDialog.SetInputCircleModeWithInputPointCounts(InputCount: integer);

begin;

  case CircleType of
    1: // two points

      case InputCount of
        1: SetGridAndEditOptions(1, MlgSection[22], '', '');
        2: SetGridAndEditOptions(0, MlgSection[22], MlgSection[24] + ' 1', '');
      end;
    2: // Diameter
      case InputCount of
        1: SetGridAndEditOptions(2, MlgSection[24] + ' 1', '', '');
        2: SetGridAndEditOptions(0, MlgSection[24] + ' 1', MlgSection[56], '');
      end;

    3: // Tri points on a circle
      SetGridAndEditOptions(0, MlgSection[24] + ' 1', MlgSection[24] + ' 2', MlgSection[24] + ' 3');

    4: //Two points and radius
      case InputCount of
        1: SetGridAndEditOptions(0, MlgSection[24] + ' 1', '', '');
        2: SetGridAndEditOptions(2, MlgSection[24] + ' 1', MlgSection[24] + ' 2', MlgSection[61]);
        3: SetGridAndEditOptions(0, MlgSection[24] + ' 1', MlgSection[24] + ' 2', MlgSection[22]);
      end;

    5: //Enclosing rectangle
      case InputCount of
        1: SetGridAndEditOptions(2, MlgSection[24] + ' 1', '', '');
        2: SetGridAndEditOptions(0, MlgSection[24] + ' 1', MlgSection[56], '');
      end;

  end;
  if PointsStringGrid.RowCount = 1 then DelBtn.Enabled := false;
end;

//prepare for text

procedure TPointsCoordDialog.SetInputTExtMode(InputCount: integer);
begin;
  case InputCount of
    1: SetGridAndEditOptions(2, MlgSection[24] + ' 1', '', '');
    2: SetGridAndEditOptions(0, MlgSection[24] + ' 1', MlgSection[56], '');
  end;
end;

procedure TPointsCoordDialog.SetGridAndEditOptions(GridMode: integer; FirstTitle, SecondTitle, THirdTitle: string);
begin;
  SetCutGrid(GridMode);
  with PointsStringGrid do
  begin;
    Cells[0, 0] := FirstTitle;
    if RowCount > 1 then
      Cells[0, 1] := SecondTitle;
    if RowCount > 2 then
      Cells[0, 2] := ThirdTitle;
  end;
end;

procedure TPointsCoordDialog.SetInputArcModeWithInputPointCounts(InputCount: integer);

  procedure ResetValidators;
  begin;

    case PointsStringGrid.RowCount of
      1:
        begin;
          FloatValidator2.Edit := nil;
          FloatValidator1.Edit := AltEdit1;
          AngleValidator2.Edit := AltEdit2;
          AngleValidator3.Edit := AltEdit3;
        end;

      2:
        begin;
          AngleValidator3.Edit := nil;
          AngleValidator2.Edit := nil;
          FloatValidator2.Edit := AltEdit2;
        end;

      3:
        begin;
          AngleValidator2.Edit := nil;
          AngleValidator3.Edit := nil;
          FloatValidator2.Edit := AltEdit2;
        end;

    end;

  end;
 //visible AltEdit3 or not
  procedure SetThird(Flag: boolean);
  begin;
    if Flag = false then
    begin;
      AltEdit3.Visible := false;
      AngleValidator3.Edit := nil;
      Altlabel3.Visible := false;
      AltLabel3a.Visible := false;
    end
    else
    begin;
      FloatValidator2.Edit := nil;
      AngleValidator2.Edit := AltEdit2;
      Altlabel3.Caption := Mlgsection[74];
      AltLabel3a.Caption := Mlgsection[73];
      AltLabel2.Top := AltLabel2.Top + 2;
      AltEdit2.Top := AltEdit2.Top + 2;
      AltLabel3.Top := AltLabel2.Top;
      AltEdit3.Top := AltEdit2.Top;
      AltLabel3a.Top := AltLabel2.Top;

      AngleValidator3.Edit := AltEdit3;
      AltEdit3.Width := trunc(AltEdit2.Width / 2) - 8;
      AltEdit3.Left := (AltEdit1.Left + AltEdit1.Width) - AltEdit3.Width;
      AltLabel3.Left := AltEdit3.Left - AltLabel3.Width - 4;

      AlTEdit2.Width := AltEdit3.Width;
      AltEdit2.Left := AltLabel3.Left - 4 - AltEdit2.Width;

      AltLabel3a.Left := AltEdit2.Left - AltLabel3a.Width - 4;

      AltLabel3.Visible := true;
      AltLabel3a.Visible := true;
      AltEdit3.Visible := true;

    end;
  end;

begin;

  case ArcType of
    1: // Center,radius, two points

      begin;

    {�����,������,������ ����,������ ����}
        case InputCount of
          1:
            begin;
              SetGridAndEditOptions(2, MlgSection[22], '', '');

            end;
          2:
            begin;
              SetGridAndEditOptions(1, MlgSection[22], MlgSection[24] + ' 1', '');
            end;
          3:
            begin;
              SetGridAndEditOptions(1, MlgSection[22], MlgSection[24] + ' 1', MlgSection[24] + ' 2');
            end;
        end;
        ResetValidators;
        if InputCount = 1 then
          SetThird(true)
        else
        begin;
          SetThird(false); {AltLabel2.Caption:=MlgSection[55];}
        end;
      end;
    2: // 2 ����� � ������
      begin;
        SetThird(false);
        case InputCount of
          1: SetGridAndEditOptions(0, MlgSection[24] + ' 1', '', '');
          2: SetGridAndEditOptions(2, MlgSection[24] + ' 1', MlgSection[24] + ' 2', '');
        end;

      end;

    3: // Tri points on a circle
      begin;
        SetGridAndEditOptions(0, MlgSection[24] + ' 1', MlgSection[24] + ' 2', MlgSection[24] + ' 3');
        SetThird(false);
      end;
  end;
  if PointsStringGrid.RowCount = 1 then DelBtn.Enabled := false;
end;

//before Real insert - calculate need coordinates

function TPointsCoordDialog.PrepareCircleForInsert: integer;
var
  Length1, Length2: double;
  Middle: TGrPoint;
  function Check: boolean;
  begin;
    Result := false;
    Middle := GrPoint((TmpArray[1].X + TmpArray[0].X) / 200, (TmpArray[1].Y + TmpArray[0].Y) / 200);
    Length1 := GrPointDistance(GrPOint(TmpArray[0].X / 100, TmpArray[0].Y / 100), Middle); //katet
    Length2 := FloatValidator1.AsFloat;
    if Length2 <= Length1 then
    begin;
      MessageDlg(Mlgsection[64] + #13 + Mlgsection[65] + FormatIt(Length1), mtError, [mbOk], 0);
      AltEdit1.SetFocus;
      Exit;
    end;
    Result := true;
  end;

  function CenterFor2PlusRadiusMode: TGrpoint;

  begin;

    Length2 := StrToFloat(FormatIt(Sqrt(Length2 * Length2 - Length1 * Length1)));

    if Combobox1.ItemIndex = 0 then
      MovePointAngle(Middle, Length2 * 100, LineAngle(TmpArray[0], TmpArray[1]) - Pi / 2)
    else
      MovePointAngle(Middle, Length2 * 100, LineAngle(TmpArray[0], TmpArray[1]) + Pi / 2);

    Result := Middle;

  end;

begin;
  Result := PointsStringGrid.RowCount;

  case CircleType of

    1:
      if PointsStringGrid.RowCount = 1 then
      begin;
        if FloatValidator2.asFloat <= 0 then
        begin;
          MessageDlg(Mlgsection[58] + Mlgsection[57], mtError, [mbOk], 0);
          AltEdit2.SetFocus;
          Result := -1;
        end
        else
        begin;

          TmpArray[1] := MovedPointAngle(TmpArray[0], FloatValidator2.asFloat * 100, 0);
          Result := 2;
        end;
      end
      else
        Result := 3;
    2:
      if PointsStringGrid.RowCount = 1 then
      begin;
        if FloatValidator1.asFloat <= 0 then
        begin;
          MessageDlg(Mlgsection[60] + Mlgsection[57], mtError, [mbOk], 0);
          AltEdit1.SetFocus;
          Result := -1;
          Exit;
        end;

        if AngleValidator2.ValidInput = false then
        begin;
          MessageDlg(Mlgsection[59], mtError, [mbOk], 0);
          AltEdit2.SetFocus;
          Result := -1;
          Exit;
        end
        else
        begin;
          TmpArray[1] := MovedPointAngle(TmpArray[0], FloatValidator1.asFloat * 100, AngleValidator2.AsFloat);
          Result := 2;
        end;
      end
      else
        Result := 3;
    3: ;
    4:
      begin;
        if FloatValidator1.ValidInput = false then
        begin;
          MessageDlg(Mlgsection[58] + MlgSection[66], mtError, [mbOk], 0);
          AltEdit1.SetFocus;
          Result := -1;
          Exit;
        end;

        if PointsStringGrid.RowCount = 2 then
        begin;
          if FloatValidator1.asFloat <= 0 then
          begin;
            MessageDlg(Mlgsection[58] + Mlgsection[57], mtError, [mbOk], 0);
            AltEdit1.SetFocus;
            Result := -1;
          end
          else
          begin;
            SetTmpArray(1);
            if Check = true then
            begin;
              TmpArray[2] := CenterFor2PlusRadiusMode
            end
            else
              Result := -1;
          end;
        end
        else
          Result := Result + 1;
      end;
    5:
      if PointsStringGrid.RowCount = 1 then
      begin;
        if (FloatValidator1.AsFloat = 0) and (FloatValidator2.AsFloat = 0) then
        begin;
          MessageDlg(Mlgsection[68], mtError, [mbOk], 0);
          AltEdit1.SetFocus;
          Result := -1;
        end;

        TmpArray[1].X := TmpArray[0].X + FloatValidator1.AsFloat * 100;
        TmpArray[1].Y := TmpArray[0].Y + FloatValidator2.AsFloat * 100;
        Result := 2;
      end
      else
        SetTmpArray(1);
  end;

end;
//before real insert

function TPointsCoordDialog.PrepareArcForInsert: integer;
var
  Length1, Length2: double;
  Middle: TGrPoint;

  function Check: boolean;
  begin;
    Result := false;
    Middle := GrPoint((TmpArray1[1].X + TmpArray1[0].X) / 200, (TmpArray1[1].Y + TmpArray1[0].Y) / 200);
    Length1 := GrPointDistance(GrPOint(TmpArray1[0].X / 100, TmpArray1[0].Y / 100), Middle); //katet
    Length2 := FloatValidator1.AsFloat;
    if Length2 <= Length1 then
    begin;
      MessageDlg(Mlgsection[64] + #13 + Mlgsection[65] + FormatIt(Length1), mtError, [mbOk], 0);
      AltEdit1.SetFocus;
      Exit;
    end;
    Result := true;
  end;

  function CenterFor2PlusRadiusMode: TGrpoint;

  begin;

    Length2 := StrToFloat(FormatIt(Sqrt(Length2 * Length2 - Length1 * Length1)));

    if Combobox1.ItemIndex = 0 then
      MovePointAngle(Middle, Length2 * 100, LineAngle(TmpArray1[0], TmpArray1[1]) - Pi / 2)
    else
      MovePointAngle(Middle, Length2 * 100, LineAngle(TmpArray1[0], TmpArray1[1]) + Pi / 2);

    Result := Middle;

  end;

begin;
  Result := PointsStringGrid.RowCount;

  case ArcType of

    1:
      begin;
        if (FloatValidator1.asFloat <= 0) and (PointsStringGrid.RowCount = 1) then
        begin;
          MessageDlg(Mlgsection[58] + Mlgsection[57], mtError, [mbOk], 0);

          AltEdit1.SetFocus;
          Result := -1;
          Exit;
        end;
        if PointsStringGrid.RowCount = 1 then //with Angle
        begin;
          if AngleValidator2.ValidInput = false then
          begin;
            MessageDlg(Mlgsection[59], mtError, [mbOk], 0);
            AltEdit2.SetFocus;
            Result := -1;
            Exit;
          end;
          if AngleValidator3.ValidInput = false then
          begin;
            MessageDlg(Mlgsection[59], mtError, [mbOk], 0);
            AltEdit3.SetFocus;
            Result := -1;
            Exit;
          end;

      //start preapre points o
          TmpArray1[1] := MovedPointAngle(TmpArray1[0], FloatValidator1.asFloat * 100, 0);
          TmpArray1[2] := MovedPointAngle(TmpArray1[0], FloatValidator1.AsFloat * 100, AngleValidator2.AsFloat * PI / 180);
          TmpArray1[3] := MovedPointAngle(TmpArray1[0], FloatValidator1.AsFloat * 100, AngleValidator3.AsFloat * PI / 180);
        end
        else //more then 1 point;
        begin;
          if FloatValidator2.asFloat <= 0 then
          begin;
            MessageDlg(Mlgsection[58] + Mlgsection[57], mtError, [mbOk], 0);
            AltEdit2.SetFocus;
            Result := -1;
            Exit;
          end;
          SetTmpArray1(1);
          if PointsStringGrid.RowCount = 2 then
            TmpArray1[2] := TmpArray1[1]
          else
          begin;
            SetTmpArray1(2);
            TmpArray1[3] := TmpArray1[2];
            TmpArray1[2] := TmpArray1[1];
          end;
          TmpArray1[1] := MovedPointAngle(TmpArray1[0], FloatValidator2.asFloat * 100, 0);
        end;
        Result := 2;
      end;
    3:
      begin;
        SetTmpArray1(0);
        Result := PointsStringGrid.RowCount + 1;
      end;
    2:
      begin;
        if FloatValidator1.ValidInput = false then
        begin;
          MessageDlg(Mlgsection[58] + MlgSection[66], mtError, [mbOk], 0);
          AltEdit1.SetFocus;
          Result := -1;
          Exit;
        end;

        if PointsStringGrid.RowCount = 2 then
        begin;
          if FloatValidator1.asFloat <= 0 then
          begin;
            MessageDlg(Mlgsection[58] + Mlgsection[57], mtError, [mbOk], 0);
            AltEdit1.SetFocus;
            Result := -1;
          end
          else
          begin;
            SetTmpArray1(1);
            if Check = true then
            begin;
              TmpArray1[2] := CenterFor2PlusRadiusMode
            end
            else
              Result := -1;
          end;
        end
        else
          Result := Result + 1;
      end;

  end;

end;

procedure TPointsCoordDialog.FillCircleDataToGrid;
var
  PCount, i: integer;

  procedure Insert;
  var
    i: integer;
  begin;
    with PProj(Project)^.MenuHandler.SubHandler as TRubberEllipse do
      for i := 0 to PCount - 1 do
      begin;
        PointsStringGrid.Cells[1, i] := FormatIt(ArrayOfPoints[i].X / 100);
        PointsStringGrid.Cells[2, i] := FormatIt(ArrayOfPoints[i].Y / 100);
        if i <> PCount - 1 then AddBtn.Click;
      end;
  end;

begin;
  with PProj(Project)^.MenuHandler.SubHandler as TRubberEllipse do
  begin;
    PCount := PointsCount;
    Insert;
  end;
end;

procedure TPointsCoordDialog.FillArcDataToGrid;
var
  i: integer;
begin;
  begin;
    with PProj(Project)^.MenuHandler.SubHandler as TRubberArc do
      for i := 0 to PointsCount - 1 do
      begin;
        PointsStringGrid.Cells[1, i] := FormatIt(ArrayOfPoints[i].X / 100);
        PointsStringGrid.Cells[2, i] := FormatIt(ArrayOfPoints[i].Y / 100);
        if i <> PointsCount - 1 then AddBtn.Click;
      end;
  end;
end;

procedure TPointsCoordDialog.FloatValidator1Error(Sender: TObject;
  ErrorCode: Word);
begin
 {ActiveControl :=}(Sender as TFloatValidator).Edit.SetFocus;
end;

procedure TPointsCoordDialog.RecalculateCirlcle(i: integer);
var
  TmpPoint, TmPoint1: TDPoint;
  TmpGrPoint, TmpGrPoint1, TmpGrPoint2: TGrPoint;
  TmpVar: double;
  TmpCenter: TGrpoint;
begin;
  TmpPoint.Init(0, 0);
  CircleCenter.Init(0, 0);
  case I of
    0:
      begin; //Center+Radius
        CircleCenter := GetXYFromGrid(0);

        if PointsStringGrid.RowCount = 1 then
          CircleRadius := Trunc(FloatValidator2.AsFloat * 100)
        else
        begin;
          TmpGrPoint := GetXYFromGrid1(0);
          TmpGrPoint1 := GetXYFromGrid1(1);
          TmpVar := GrPointDistance(TmpGrPoint, TmpGrPoint1);
          CircleRadius := Trunc(TmpVar);
        end;
      end;

    1: //diametr
      begin;
        TmpGrPoint := GetXYFromGrid1(0);
        if PointsStringGrid.RowCount = 1 then
          TmpGrPoint1 := MovedPointAngle(TmpGrPoint, Trunc(FloatValidator1.AsFloat * 100), AngleValidator2.AsFloat * Pi / 180)
        else
          TmpGrPoint1 := GetXYFromGrid1(1);

        CircleRadius := Trunc(GrPointDistance(TmpGrPoint, TmpGrPoint1) / 2);
        CircleCenter.X := Trunc((TmpGrPoint.X + TmpGrPoint1.X) / 2);
        CircleCenter.Y := Trunc((TmpGrPoint.Y + TmpGrPoint1.Y) / 2);
      end;

    2:
      begin;

        TmpGrPoint := GetXYFromGrid1(0);
        if PointsStringGrid.RowCount <> 1 then
          TmpGrPoint1 := GetXYFromGrid1(1)
        else
          TmpGrPoint1 := GrPoint((TmpGrPoint.X + FloatValidator1.AsFloat * 100), (TmpGrPoint.Y + FloatValidator2.AsFloat * 100));

        TmpGrPOint2 := GrPoint(TmpGrPoint.X, TmpGrPoint1.Y);

        TmpCenter := GrPoint(Trunc((TmpGrPOint.X + TmpGrPOint1.X) / 2), Trunc((TmpGrPOint.Y + TmpGrPOint1.Y) / 2));
        CircleCenter.X := Trunc(TmpCenter.X);
        CircleCenter.Y := Trunc(TmpCenter.Y);

        CircleRadius := min(trunc(GrPointDistance(TmpGrPoint2, TmpGrPoint) / 2), trunc(GrPointDistance(TmpGrPoint1, TmpGrPoint) / 2));

      end;

  end; //case

end;

function TPointsCoordDialog.GetXYFromGrid(i: integer): TDpoint;
begin;
  Result.Init(0, 0);
  Result.X := Trunc(StrToFloat(PointsStringGrid.Cells[1, i]) * 100);
  Result.Y := Trunc(StrToFloat(PointsStringGrid.Cells[2, i]) * 100);
end;

function TPointsCoordDialog.GetXYFromGrid1(i: integer): TGrPoint;
begin;

  Result.X := Trunc(StrToFloat(PointsStringGrid.Cells[1, i]) * 100);
  Result.Y := Trunc(StrToFloat(PointsStringGrid.Cells[2, i]) * 100);
end;

procedure TPointsCoordDialog.SetCircleParamsForFirstSecond;
var
  TmpPOint, TmpPoint1, TmpPoint3: TGrPoint;
  OnePoint: TDpoint;

  procedure SetFirstRow(Z: TDPoint);
  begin;
    PointsStringGrid.Cells[1, 0] := FormatIt(Z.X / 100);
    PointsStringGrid.Cells[2, 0] := FormatIt(Z.Y / 100);
  end;
  procedure SetSecondRow(W: TGrPoint);
  begin;
    PointsStringGrid.Cells[1, 1] := FormatIt(W.X / 100);
    PointsStringGrid.Cells[2, 1] := FormatIt(W.Y / 100);
  end;

begin;
  OnePoint.Init(0, 0);
  case TypeCombo.ItemIndex of
    0: //Center+Radius
      begin;
        AngleValidator2.Edit := nil;
        FloatValidator2.MinValue := 0;
        FloatValidator2.Edit := AltEdit2;

        SetFirstRow(CircleCenter);

        if PointsStringGrid.RowCount <> 1 then
        begin;
          TmpPoint := GetXYFromGrid1(0);
          TmpPoint1 := MovedPointAngle(TmpPoint, CircleRadius, 0);
          SetSecondRow(TmpPoint1);
        end
        else
          AltEdit2.Text := FormatIt(CircleRadius / 100);

      end;
    1:
      begin; //Diametr

        FloatValidator2.Edit := nil;
        FloatValidator1.MinValue := 0;
        AngleValidator2.Edit := AltEdit2;

        TmpPoint := Grpoint(CircleCenter.X, CircleCenter.Y);
        TmpPoint1 := MovedPointAngle(TmpPoint, -CircleRadius, 0);
        OnePoint.X := Trunc(TmpPOint1.X);
        OnePoint.Y := Trunc(TmpPOint1.Y);
        SetFirstRow(OnePoint);

        if PointsStringGrid.RowCount <> 1 then
        begin;
          TmpPoint1 := MovedPointAngle(TmpPoint, CircleRadius, 0);
          SetSecondRow(TmpPoint1);
        end
        else
        begin;
          AltEdit1.Text := FormatIt((CircleRadius * 2) / 100);
          AltEdit2.Text := FormatIt(0);
        end;

      end;
    2:
      begin;

        AngleValidator2.Edit := nil;
        FloatValidator2.Edit := AltEdit2;
        FloatValidator1.MinValue := -99999999999;
        FloatValidator2.MinValue := -99999999999;
        TmpPoint3 := Grpoint(CircleCenter.X, CircleCenter.Y);
        TmpPoint1 := MovedPointAngle(TmpPoint3, CircleRadius, pi);
        TmpPoint := MovedPointAngle(TmpPoint3, CircleRadius, pi * 3 / 2);
        OnePoint.X := Trunc(TmpPoint1.X);
        OnePoint.Y := Trunc(TmpPoint.Y);
        SetFirstRow(OnePoint);

        if PointsStringGrid.RowCount <> 1 then
        begin;
          TmpPoint1 := MovedPointAngle(TmpPoint3, CircleRadius, 0);
          TmpPoint := MovedPointAngle(TmpPoint3, CircleRadius, pi / 2);
          TmpPoint3.X := TmpPoint1.X;
          TmpPoint3.Y := TmpPoint.Y;
          SetSecondRow(TmpPoint3);
        end
        else
        begin;
          AltEdit1.Text := FormatIt(2 * (TmpPoint3.X - OnePoint.X) / 100);
          AltEdit2.Text := FormatIt(2 * (TmpPoint3.Y - OnePoint.Y) / 100);
        end;

      end;

  end; //case

end;

function TPointsCoordDialog.CheckParamsOfEditCircleBeforeApplyIt: boolean;
begin;
  Result := false;
  case TypeCombo.ItemIndex of
    0:
      if PointsStringGrid.RowCount = 1 then
        if FloatValidator2.asFloat <= 0 then
        begin;
          FloatValidator2.Edit := nil;
          MessageDlg(Mlgsection[58] + Mlgsection[57], mtError, [mbOk], 0);
          AltEdit2.SetFocus;
          Exit;
        end;

    1:
      if PointsStringGrid.RowCount = 1 then
      begin;
        if FloatValidator1.asFloat <= 0 then
        begin;
          MessageDlg(Mlgsection[60] + Mlgsection[57], mtError, [mbOk], 0);
          AltEdit1.SetFocus;
          Exit;
        end;

        if AngleValidator2.ValidInput = false then
        begin;
          MessageDlg(Mlgsection[59], mtError, [mbOk], 0);
          AltEdit2.SetFocus;
          Exit;
        end
      end;
    2:
      if PointsStringGrid.RowCount = 1 then
      begin;
        if (FloatValidator1.AsFloat = 0) and (FloatValidator2.AsFloat = 0) then
        begin;
          MessageDlg(Mlgsection[68], mtError, [mbOk], 0);
          AltEdit1.SetFocus;
          Exit;
        end;
      end
  end; //case
  Result := true;
end;

function TPointsCoordDialog.ReturnCorrected(D: double): integer;
begin;
  Result := Trunc(D * Koeff);
end;

procedure TPointsCoordDialog.TypeComboClick(Sender: TObject);
begin
  InvisibleGradus;
 //if ObjectType=mn_Edit_Circle
 //  then RecalculateCirlcle;
end;

//Call procedure for load from file

procedure TPointsCoordDialog.FileButtonClick(Sender: TObject);
var XSymLib: ansistring;
  XSymbol: PSGroup;
  IndexOld: integer;
begin
  {InvisibleGradus;
  if ObjectType = ot_Symbol
    then begin;
    XSymLib := PProj(WingisMainForm.ActualProj).ActSymLib;
    XSymbol := PProj(WingisMainForm.ActualProj).ActualSym;
    IndexOld := ASymLibInfo.LibIndex;
  end;

  Z1 := TPointsCoordDialog1.Create(Self);
  Z1.SLibIndex := StartLibIndex;
  if FileName <> '' then Z1.FileNameEdit.Text := FileName;
  if ObjectType = ot_Symbol then
    Z1.CheckBox1.Visible := true;

  if Z1.ShowModal = mrOk then
  begin;
    KolonSymbolName := Z1.SSymColumn;
    Z1.ChangeData;
    LoadDataFromFile;
    FileName := Z1.FileNameEdit.Text;

    if ObjectType = ot_Symbol then begin;
      ASymLibInfo.LibIndex := Indexold;
      PProj(WingisMainForm.ActualProj).ActSymLib := XSymLib;
      PProj(WingisMainForm.ActualProj).ActualSym := XSymbol;
    end;
  end;
  Z1.Free;
  OrtoBtn.Enabled := (SpecificCheckbox.Checked = true) and (PointsStringGrid.RowCount > 2);}

  DelAllBtnClick(nil);
  if PointsStringGrid.RowCount <= 1 then begin
     if WingisMainForm.ActualChild.ExtrasImportCoordinates('','',-1,0,'','') then begin
        Close;
     end;
  end;
end;

function TPointsCoordDialog.LoadDataFromFile: boolean;

var
  DataFile: TextFile;
  RecordCount, i, j, k, mm, mm1, KolonX, KolonY: integer;
  Stroka: ANSIstring;
  fX, fY: double;
  Maximum, Minimum, Middle: integer;
  Liblist: TList;
  TmpSymbolName, TmpLibName: string;

  IgnoreAll, CancelLoad, StopExpandReplace: boolean;

  procedure PrepareSpecGrid;
  var i: integer;
  begin;
    IgnoreGrid.ColCount := 2;
    IgnoreGrid.RowCount := 1;

    ReplaceGrid.ColCount := 4;
    ReplaceGrid.RowCount := 1;

    for i := 0 to 3 do
    begin;
      IgnoreGrid.Cells[0, i] := '';
      ReplaceGrid.Cells[0, i] := '';
    end;

  end;

  function CheckIgnore(SymName, LibName: string): boolean;
  var i: integer;
  begin;
    Result := false;
    for i := 0 to IgnoreGrid.RowCount - 1 do
      if (SymName = IgnoreGrid.Cells[0, 1])
        and (LibName = IgnoreGrid.Cells[1, 1])
        then begin;
        Result := true;
        Exit;
      end;
  end;

  function CheckReplace(var SymName, LibName: string): boolean;
  var i: integer;
  begin;
    Result := false;
    for i := 0 to ReplaceGrid.RowCount - 1 do
      if (SymName = ReplaceGrid.Cells[0, 1])
        and (LibName = ReplaceGrid.Cells[1, 1])
        then begin;
        SymName := ReplaceGrid.Cells[2, 1];
        LibName := ReplaceGrid.Cells[3, 1];
        ASymLibInfo.LibIndex := GetPositionOfSymbolsSet(ASymLibInfo, LibName); //SLibIndex;
        ASymInfo := ASymLibInfo.GetSymInfo;

        Result := true;
        Exit;
      end;
  end;




  procedure GetMMMA(aKolonX, aKolonY, aKolonSymbolName: integer);
  var CheckArray: array[1..3] of integer;
    i: integer;
  begin;
    CheckArray[1] := aKolonX;
    CheckArray[2] := aKolonX;
    CheckArray[3] := aKolonSymbolName;

    Minimum := MinIntValue(CheckArray);
    Maximum := MaxIntValue(CheckArray);

    i := 1;
    while (CheckArray[i] = Minimum) or (CheckArray[i] = Maximum) do
      inc(i);

    Middle := CheckArray[i];

  end;


  function GetSymbolName: boolean;
  label QResultmrNo;
  var
    TmpStroka, EndStroka, MeStroka: Ansistring;
    QFound: integer;
    TmpNumber: integer;
    TmpSymbol: PSGroup;
    QResult: byte;
    QResult1: byte;

  begin;
    with Z1 do
    begin;
      Result := false;
     //if file not contain symbol's names
      if not CheckBox1.Checked then Exit;

      Q_SpaceCompressInPlace(Stroka);
    {--Brovak}
      if pos(DelimEdit.Text, Stroka) = 0 then //end of line
        TmpStroka := Stroka
      else
        TmpStroka := copy(Stroka, 1, pos(DelimEdit.Text, Stroka));
      Delete(Stroka, 1, Length(TmpStroka));

      if Z1.CheckSpecial.Checked = true then
      begin;
        if OpenSpecial.Text <> '' then Delete(TmpStroka, 1, length(OpenSpecial.Text));

        EndStroka := '';
        if CloseSpecial.Text <> '' then
          EndStroka := CloseSpecial.Text
        else
          if Stroka <> '' then EndStroka := DelimEdit.Text;
      end
      else
        EndStroka := DelimEdit.Text;

      QFound := pos(EndStroka, TmpStroka);
      Delete(Tmpstroka, QFound, Length(TmpStroka) - Qfound + 1);

// check specials
    //if Symbol's name have additional frame
      if SUsespec = true
        then
        if SOpenspec <> ''
          then
          Delete(TmpStroka, 1, length(SOpenspec));

      if SClosespec <> ''
        then
        Delete(TmpStroka, length(TmpStroka) - Length(SClosespec) + 1, length(SClosespec));

   //if it is number - convert to name
      ASymLibInfo.LibIndex := GetPositionOfSymbolsSet(ASymLibInfo, TmpLibName); //SLibIndex;
      ASymInfo := ASymLibInfo.GetSymInfo;
      if not (ScontainName) then
      begin;
        try
          TmpNumber := StrToInt(TmpStroka);
        except
          Result := false
        end;
        if Result = false then TmpNumber := TmpNumber div (TmpNumber - TmpNumber);
        TmpStroka := ASymInfo.GetSym(TmpNumber).GetName;
      end;

    //if it is another symbol then Exit with false Flag

      TmpSymbolName := TmpStroka;
      TmpLibName := SsLIbName;

      if SAllSymCheck = false then
        if (SSymbolName <> TmpStroka)
          then Exit;


      MeStroka := format(Mlgsection[97], [TmpStroka]);



      if ASymInfo.IndexOf(TmpStroka) = -1 //not found
        then
      begin;

     //�������� �� �����
        if IgnoreAll then Exit;
        if CheckIgnore(TmpStroka, SSLibName) = true
          then Exit;

        if StopExpandReplace then
        begin;
          TmpSymbolName := ReplaceGrid.Cells[2, 0];
          TmpLibName := ReplaceGrid.Cells[3, 0];
          ASymLibInfo.LibIndex := GetPositionOfSymbolsSet(ASymLibInfo, TmpLibName); //SLibIndex;
          ASymInfo := ASymLibInfo.GetSymInfo;
          Result := true;
          Exit;
        end;
        if CheckReplace(TmpSymbolName, TmpLibName) = true
          then
        begin;
          Result := true;
          Exit;
        end;

        QResult := MessageDlg(MeStroka + chr(13) + Mlgsection[98] + chr(13) + Mlgsection[103], mtWarning, [mbYes, mbNo, mbCancel], 0);
        case QResult of
          mrCancel: begin;
              CancelLoad := true;
              Exit;
            end;
          mrYes: begin;
              TmpSymbol := GetSymbol(Self, WingisMainForm.ActualProj);

              if TmpSymbol = nil then goto QResultmrNo
              else
              begin;
                TmpSymbolName := TmpSymbol.GetName;
                TmpLibName := TmpSymbol.GetLibName;
                ASymLibInfo.LibIndex := GetPositionOfSymbolsSet(ASymLibInfo, TmpLibName); //SLibIndex;
                ASymInfo := ASymLibInfo.GetSymInfo;

                QResult1 := MessageDlg(format(Mlgsection[99], [TmpStroka]) + chr(13) + Mlgsection[101], mtConfirmation, [mbYes, mbNo, mbAll], 0);

                case QResult1 of
                  mrCancel: begin; CancelLoad := true; Exit; end;
                  MrYes: ReplaceGrid.RowCount := ReplaceGrid.RowCount + 1;
                  MrAll: begin; ReplaceGrid.RowCount := 1; StopExpandReplace := true; end;
                end; //case

                if QResult <> mrNo then
                begin;
                  ReplaceGrid.Cells[0, ReplaceGrid.RowCount - 1] := TmpStroka;
                  ReplaceGrid.Cells[1, ReplaceGrid.RowCount - 1] := SsLIbName;
                  ReplaceGrid.Cells[2, ReplaceGrid.RowCount - 1] := TmpSymbolName;
                  ReplaceGrid.Cells[3, ReplaceGrid.RowCount - 1] := TmpLibName;
                end;

              end; //else
            end; //mrYes

          mrNo:
            begin;
              QResultmrNo: QResult1 := MessageDlg(format(Mlgsection[100], [TmpStroka]) + chr(13) + Mlgsection[104] + chr(13) + Mlgsection[102], mtConfirmation, [mbYes, mbNo, mbAll, mbCancel], 0);

              case QResult1 of

                mrCancel: CancelLoad := true;

                mrYes: begin;
                    IgnoreGrid.RowCount := IgnoreGrid.RowCount + 1;
                    IgnoreGrid.Cells[0, IgnoreGrid.RowCount - 1] := TmpStroka;
                    IgnoreGrid.Cells[1, IgnoreGrid.RowCount - 1] := SsLIbName;
                  end;
                mrAll: IgnoreAll := true;

              end; //case
              Exit;
            end; //Mr No

        end; //big case
      end; //-1

      Result := true;
    end;

  end;



   //Extract X or Y coordinate from substring
  function GetNumber: double;
  var
    TmpStroka, EndStroka: Ansistring;
    QFound: integer;

  begin;
    with Z1 do
    begin;
    { +++ BRovak Bug noted by Wilhelm Oremus}
      Q_SpaceCompressInPlace(Stroka);
    {--Brovak}
      if pos(DelimEdit.Text, Stroka) = 0 then //end of line
        TmpStroka := Stroka
      else
        TmpStroka := copy(Stroka, 1, pos(DelimEdit.Text, Stroka));
      Delete(Stroka, 1, Length(TmpStroka));

      if Z1.CheckSpecial.Checked = true then
      begin;
        if OpenSpecial.Text <> '' then Delete(TmpStroka, 1, length(OpenSpecial.Text));

        EndStroka := '';
        if CloseSpecial.Text <> '' then
          EndStroka := CloseSpecial.Text
        else
          if Stroka <> '' then EndStroka := DelimEdit.Text;
      end
      else
        EndStroka := DelimEdit.Text;

      QFound := pos(EndStroka, TmpStroka);
      Delete(Tmpstroka, QFound, Length(TmpStroka) - Qfound + 1);

      QFound := Pos(',', TmpStroka);
      if QFound > 0 then TmpStroka[Qfound] := '.';
      Result := StrToFloat(TmpStroka);
    end;
  end; //function

begin;
  if Z1.ComboBox1.ItemIndex = 0 then
  begin;
    PointsStringGrid.RowCount := 1;
    PointsStringGrid.Row := 0;
  end;
  PointsStringGrid.OnDrawCell := nil;
  if Z1.Combobox1.ItemIndex <> 2 then
    PointsStringGrid.OnSelectCell := nil;
  PointsStringGrid.Visible := false;
  Screen.Cursor := crHourGlass;
  IsLoadingNow := true;
  Label10.Visible := true;
  AltLabel1.Visible := false;
  AltLabel2.Visible := false;
  Combobox1.Visible := false;
  AltEdit1.Visible := false;
  AltEdit2.Visible := false;
  AltEdit3.Visible := false;
  CheckBox1.Checked := false;
  SpecificCheckBox.Checked := false;
  Label5.Visible := false;


{ then
  begin;
   AProjection:=Get_projection(PProj(Project).PInfo.ProjectionSettings.ProjectProjection);
   ADatum:=Get_datum_params(PProj(Project).PInfo.ProjectionSettings.ProjectDate);
   set_FromSystem(transform_pointer,AProjection,ADatum);

   AProjection:=Get_projection('Phi/Lamda values');
   ADatum:=Get_datum_params('WGS84');
   set_ToSytem(transform_pointer,AProjection,ADatum);
  end
 }


  with Z1 do
  begin;
    try

      IgnoreAll := false;
      CancelLoad := false;
      StopExpandReplace := false;

      if ObjectType = Ot_Symbol then begin;
        TmpSymbolName := PProj(Project).ActualSym.GetName;
        TmpLibName := PProj(Project).ActualSym.GetLibName;
      end;

      AssignFile(DataFile, FileNameEdit.Text);
      Reset(DataFile);
      StatusBar.ProgressPanel := TRUE;
      StatusBar.ProgressText := Z1.MlgSection[23];
      UserInterface.Update([uiStatusBar]);
      RecordCount := 0;
      Application.ProcessMessages;


     // calculating count of points
      while not eof(DataFile) do
      begin;
        Readln(DataFile, Stroka);
        Inc(RecordCount);
      end;
      StatusBar.ProgressText := Z1.MlgSection[24]; ;
      UserInterface.Update([uiStatusBar]);
      RecordCount := RecordCount - StrToInt(ReadFromEdit.Text) + 1;

      Reset(DataFile);

      if ((Z1.Combobox1.ItemIndex in [1, 2]) and (RecordCount > MaxPointsInThisMode - PointsStringgrid.RowCount))
        or ((Z1.Combobox1.ItemIndex = 0) and (RecordCount > MaxPointsInThisMode - PointsStringgrid.RowCount + 1))
        then
      begin;
        MsgBox(Self.Handle, 2501, MB_OK + MB_ICONINFORMATION + MB_DEFBUTTON1, '');
        RecordCount := MaxPointsInThisMode - PointsStringgrid.RowCount;
        if Z1.Combobox1.ItemIndex = 0 then Inc(RecordCount);
      end;

      mm1 := 1;
      for mm := 1 to StrToInt(ReadFromEdit.Text) - 1 do
        Readln(DataFile, Stroka);

      KolonX := StrToInt(KolonkaX.Text);
      KolonY := StrToInt(KolonkaY.Text);
      if Z1.AddBtn.Visible then
      begin;
        GetMMMA(KolonX, KolonY, KolonSymbolName);
        PrepareSpecGrid;
      end
      else
      begin;
        Maximum := Max(StrToInt(KolonkaX.Text), StrToInt(KolonkaY.Text));
        Minimum := Min(StrToInt(KolonkaX.Text), StrToInt(KolonkaY.Text));
      end;

      Result := false;
      Application.ProcessMessages;

      for mm := 1 to RecordCount do
      begin;

        if CancelLoad then Break;

        J := round(100 * mm / RecordCount);

        StatusBar.Progress := j;
        UserInterface.Update([uiStatusBar]);
        Readln(DataFile, Stroka);

        j := 1;
        // while not found x or y colon - erase others colon
        for i := 1 to Minimum - 1 do
        begin;
          k := pos(DelimEdit.Text, Stroka);
          if k = 0 then J := j div (j - j);
          Delete(Stroka, 1, k + Length(DelimEdit.Text) - 1);
          Inc(j);
          if Stroka = '' then J := j div (j - j); //Create Exception - error format of file data
        end;
        if Z1.AddBtn.Visible then
          if J = KolonSymbolName then
            if GetSymbolName = false then Continue;
        if J = KolonX then fX := GetNumber;
        if J = KolonY then fY := GetNumber;
        Inc(j);


          // while not found other need coord - erase others colon
        if Z1.AddBtn.Visible then
        begin;
          for i := 1 to Middle - Minimum - 1 do
          begin;
            k := pos(DelimEdit.Text, Stroka);
            if k = 0 then J := j div (j - j);
            Delete(Stroka, 1, k + Length(DelimEdit.Text) - 1);
            inc(j)
          end;
          if Stroka = '' then J := j div (j - j); //Create Exception - error at start data

          if Z1.AddBtn.Visible then
            if J = KolonSymbolName then
              if GetSymbolName = false then Continue;
          if J = KolonX then fX := GetNumber;
          if J = KolonY then fY := GetNumber;
          Inc(j);
        end
        else
        begin;
          Middle := Minimum;
        end;

       // while not found other need coord - erase others colon
        for i := 1 to Maximum - Middle - 1 do
        begin;
          k := pos(DelimEdit.Text, Stroka);
          if k = 0 then J := j div (j - j);
          Delete(Stroka, 1, k + Length(DelimEdit.Text) - 1);
          inc(j)
        end;
        if Stroka = '' then J := j div (j - j); //Create Exception - error at start data

        if Z1.AddBtn.Visible then
          if J = KolonSymbolName then
            if GetSymbolName = false then Continue;
        if J = KolonX then fX := GetNumber;
        if J = KolonY then fY := GetNumber;

        case Z1.Combobox1.ItemIndex of
          0: begin;
              if mm > 1 then Self.AddBtn.Click; //Rewrite
              if ObjectTYpe = ot_Symbol then begin;
                PointsStringGrid.Cells[0, PointsStringGrid.Row] := TmpSymbolName;
                SymLibList.Add(GetPositionOfSymbolsSet(ASymLibInfo, TmpLibName));
              end;
            end;

          1: begin; Self.AddBtn.Click; //Add
              if ObjectTYpe = ot_Symbol then begin;
                PointsStringGrid.Cells[0, PointsStringGrid.Row] := TmpSymbolName;
                SymLibList.Add(GetPositionOfSymbolsSet(ASymLibInfo, TmpLibName));
              end;
            end;
          2: begin; Self.InsBtn.Click; //Ins
              if ObjectTYpe = ot_Symbol then begin;
                PointsStringGrid.Cells[0, PointsStringGrid.Row] := TmpSymbolName;
                SymLibList.Insert(PointsStringGrid.Row, GetPositionOfSymbolsSet(ASymLibInfo, TmpLibName));
              end;
            end;
        end;
        //update data with Koefficients
        if CheckX.Checked then Fx := Fx * FloatValidator1.AsFloat;
        if CheckY.Checked then Fy := Fy * FloatValidator2.AsFloat;

//        write decoded data to colons

       //Output(  SetProjection(FX,FY);
        if PProj(Project).PInfo.CoordinateSystem.CoordinateType = ctGeodatic then
        begin;
          PointsStringGrid.Cells[1, PointsStringGrid.Row] := FormatIt(Fy);
          PointsStringGrid.Cells[2, PointsStringGrid.Row] := FormatIt(Fx);
        end
        else
        begin;
          PointsStringGrid.Cells[1, PointsStringGrid.Row] := FormatIt(Fx);
          PointsStringGrid.Cells[2, PointsStringGrid.Row] := FormatIt(Fy);
        end;


        inc(mm1);

        if Z1.ComboBox1.ItemIndex = 2 then PointsStringGrid.Row := PointsStringGrid.Row + 1;
      end; //for
      StatusBar.ProgressPanel := FALSE;
       //Message at finish of loading
      if mm > 1 then
        ShowMessage(Z1.Mlgsection[27] + IntToStr(mm1 - 1));

    except; //error of reading data
      ShowMessage(Z1.Mlgsection[28] + IntToStr(mm + IntValidator1.AsInteger - 1));
    end;
    SetButton(false, false, true);
    IsLoadingNow := false;
    CloseFile(DataFile);
    Label10.Visible := false;
    StatusBar.ProgressPanel := FALSE;

  end; //with
  Label5.Visible := true;
  PointsStringGrid.OnDrawCell := PointsStringGridDrawCell;
  if Z1.Combobox1.ItemIndex <> 2 then
    PointsStringGrid.OnSelectCell := PointsStringGridSelectCell;
  PointsStringGrid.Visible := true;
  UpdateTitle;
  Screen.Cursor := crDefault;
//   if Symbolslist<>nil then
//  SymbolsList.Free;
end; //proc

procedure TPointsCoordDialog.DelAllBtnClick(Sender: TObject);
begin
  InvisibleGradus;
// Delete all points of object
  if (PointsStringGrid.RowCount > 1) and (MessageDlg(Mlgsection[85], mtConfirmation, [mbOk, mbCancel], 0) = mrOk) then
  begin;
    PointsStringGrid.RowCount := 1;
    PointsStringGrid.Row := 0;
    Deleteprocess;
    Deleteprocess1;
    PointsStringGrid.Cells[1, 0] := FormatIt(0);
    PointsStringGrid.Cells[2, 0] := FormatIt(0);
    CalculateAdditional;
    Edit1.Text := FormatIt(0);
    Edit2.Text := FormatIt(0);
    Edit3.Text := FormatIt(0);
  end;
  OrtoBtn.Enabled := (SpecificCheckbox.Checked = true) and (PointsStringGrid.RowCount > 2);
end;

procedure TPointsCoordDialog.PointsStringGridMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  CalculateAdditional;
end;

procedure TPointsCoordDialog.OrtoBtnClick(Sender: TObject);
begin
//OrtoBtn.Down:=not OrtoBtn.Down;
  Label4.Visible := not OrtoBtn.Down;
//   Edit3.Visible:= not  OrtoBtn.Down;
  Edit2.Visible := not OrtoBtn.Down;
 //  Label8.Visible:=not OrtoBtn.Down;
  CheckBox1.Visible := not OrtoBtn.Down;
  PointCombo.Visible := not OrtoBtn.Down;
  ModeCombo.Visible := not OrtoBtn.Down;
  Label2.Visible := not OrtoBtn.Down;
  Radiogroup1.Visible := OrtoBtn.Down;

  if OrtoBtn.Down = false
    then ChangeCombo
  else Label3.Caption := MlgSection[33];
  if OrtoBtn.Down
    then
    ActiveControl := Edit1;


end;

procedure TPointsCoordDialog.DegressToMetric;
begin;
    //
end;

procedure TPointsCoordDialog.MetricToDegress;
begin;
   //
end;

procedure TPointsCoordDialog.CoordTypeBtnClick(Sender: TObject);
begin
  InvisibleGradus;
//if CoordTypeBtn.Down=true
//then CoordInputType:=ictdegree
//else CoordInputType:=ictMetric;
  Z2 := PointsCoordDlg2.TPointsCoordDialog2.Create(Parent);
  TmpInputMode := Inifile.LoadPointsCoordDataFromFile.InputMode;
  TmpDegreeMode := Inifile.LoadPointsCoordDataFromFile.DegreeMode;

  if Z2.ShowModal = mrOk
    then
  begin;
    TmpClosed1;
    Z2.ChangeData;
    if TmpInputMode <> Inifile.LoadPointsCoordDataFromFile.InputMode
      then ResetPoints(false)
    else //degree
      if (Inifile.LoadPointsCoordDataFromFile.InputMode = 1) and
        (TmpDegreeMode <> Inifile.LoadPointsCoordDataFromFile.DegreeMode)
        then ResetPoints(false);
  end;
  Z2.Free;
end;

procedure TPointsCoordDialog.StringEditExit(Sender: TObject);
var Flag: boolean;
begin
  Flag := true;
  PointsStringGrid.Cells[PointsStringGrid.Col, PointsStringGrid.Row] := StringEdit.Text;
  PointsStringGrid.OnSelectCell(Self, PointsStringGrid.Col, PointsStringGrid.Row, Flag);
  StringEdit.Visible := false;
  StringEditValid.Edit := nil;
end;

procedure TPointsCoordDialog.PointsStringGridClick(Sender: TObject);
begin
//InvisibleGradus;
end;

procedure TPointsCoordDialog.ResetPoints(Flag: boolean);
var i, j, k, TRow, TCol: integer;
  DataArray: array[1..2] of double;
  DataString: string;
  DataFloat: double;

begin;
  if PProj(Project).PInfo.ProjectionSettings.ProjectProjSettings = 'NONE' then Exit;
  Screen.Cursor := crHourGlass;

  if Inifile.LoadPointsCoordDataFromFile.InputMode = 1 then
  begin
      // source projection is current project projection
    if PProj(Project).PInfo.CoordinateSystem.CoordinateType = ctGeodatic then
      AxGISProjection1.SourceCoordinateSystem := cs_Geodetic
    else
      AxGISProjection1.SourceCoordinateSystem := cs_Carthesic;
    AxGISProjection1.SourceProjSettings := PProj(Project).PInfo.ProjectionSettings.ProjectProjSettings;
    AxGISProjection1.SourceProjection := PProj(Project).PInfo.ProjectionSettings.ProjectProjection;
    AxGISProjection1.SourceDate := PProj(Project).PInfo.ProjectionSettings.ProjectDate;
    AxGISProjection1.SourceXOffset := PProj(Project).PInfo.ProjectionSettings.ProjectionXOffset;
    AxGISProjection1.SourceYOffset := PProj(Project).PInfo.ProjectionSettings.ProjectionYOffset;
    AxGISProjection1.SourceScale := PProj(Project).PInfo.ProjectionSettings.ProjectionScale;
    AxGISProjection1.UsedInputCoordSystem := cs_Carthesic; // due to WinGIS coordinates are always stored carthesic

      // target projection is phi/lambda
    AxGISProjection1.TargetCoordinateSystem := cs_Geodetic; // phi/lamda values are always geodetic
    AxGISProjection1.TargetProjSettings := 'Geodetic Phi/Lamda';
    AxGISProjection1.TargetProjection := 'Phi/Lamda values';
    AxGISProjection1.TargetDate := 'WGS84';
    AxGISProjection1.TargetXOffset := 0; // no offsets
    AxGISProjection1.TargetYOffset := 0;
    AxGISProjection1.TargetScale := 1.0; // no scale
    AxGISProjection1.UsedOutputCoordSystem := cs_Carthesic; // due to WinGIS coordinates are always stored carthesic
  end
  else
  begin
      // source projection is phi/lamda
    AxGISProjection1.SourceCoordinateSystem := cs_Geodetic; // phi/lamda values are always geodetic
    AxGISProjection1.SourceProjSettings := 'Geodetic Phi/Lamda';
    AxGISProjection1.SourceProjection := 'Phi/Lamda values';
    AxGISProjection1.SourceDate := 'WGS84';
    AxGISProjection1.SourceXOffset := 0; // no offsets
    AxGISProjection1.SourceYOffset := 0;
    AxGISProjection1.SourceScale := 1.0; // no scale
    AxGISProjection1.UsedInputCoordSystem := cs_Carthesic; // due to WinGIS coordinates are always stored carthesic

      // target projection is current project projection
    if PProj(Project).PInfo.CoordinateSystem.CoordinateType = ctGeodatic then
      AxGISProjection1.TargetCoordinateSystem := cs_Geodetic
    else
      AxGISProjection1.TargetCoordinateSystem := cs_Carthesic;
    AxGISProjection1.TargetProjSettings := PProj(Project).PInfo.ProjectionSettings.ProjectProjSettings;
    AxGISProjection1.TargetProjection := PProj(Project).PInfo.ProjectionSettings.ProjectProjection;
    AxGISProjection1.TargetDate := PProj(Project).PInfo.ProjectionSettings.ProjectDate;
    AxGISProjection1.TargetXOffset := PProj(Project).PInfo.ProjectionSettings.ProjectionXOffset;
    AxGISProjection1.TargetYOffset := PProj(Project).PInfo.ProjectionSettings.ProjectionYOffset;
    AxGISProjection1.TargetScale := PProj(Project).PInfo.ProjectionSettings.ProjectionScale;
    AxGISProjection1.UsedOutputCoordSystem := cs_Carthesic; // due to WinGIS coordinates are always stored carthesic
  end;


  TRow := PointsStringgrid.Row;
  TCol := PointsStringgrid.Col;

//  StatusBar.ProgressPanel := TRUE;
//  StatusBar.ProgressText := Z1.MlgSection[91];
 // UserInterface.Update([uiStatusBar]);
 // Application.ProcessMessages;
// PointsStringGrid.OnDrawCell := nil;
  with PointsStringgrid do
    for i := 0 to RowCount - 1 do
    begin;
//    StatusBar.Progress:=100*i/(RowCount-1);
      for j := 1 to 2 do
      begin;
        DataString := Cells[j, i];
        if Flag = false then begin;
          if TmpInputMode = 0
            then DataArray[j] := StrToFloat(DataString)
          else DataArray[j] := InputDegreeMode(DataString, TmpDegreeMode);
        end
        else
          DataArray[j] := StrToFloat(DataString);
       // proj
//       SetProjection;
      end;


   //for change datas we need use change projecions
      if Inifile.LoadPointsCoordDataFromFile.InputMode <> TmpInputMode
        then SetProjection(DataArray[1], DataArray[2]);
{
        //fvfv
     AProjection:=Get_projection('Phi/Lamda values');
     ADatum:=Get_datum_params('WGS84');
     set_FromSystem(transform_pointer,AProjection,ADatum);

     AProjection:=Get_projection(PProj(Project).PInfo.ProjectionSettings.ProjectProjection);
     ADatum:=Get_datum_params(PProj(Project).PInfo.ProjectionSettings.ProjectDate);
     set_ToSytem(transform_pointer,AProjection,ADatum);

       SetProjection(DataArray[1],DataArray[2]);
                                                       }
        //sdfsdf

      for j := 1 to 2 do
      begin;
             // ������� ����� � ������ ������ ���� �����
        if (Inifile.LoadPointsCoordDataFromFile.InputMode = 0) and (TmpInputMode = 1)
          then DataString := FormatIt(DataArray[j])
        else DataString := OutputDegreeMode(DataArray[j]);
        Cells[j, i] := DataString;
      end;
        //������
    end;
//  PointsStringGrid.OnDrawCell := PointsStringGridDrawCell;
  PointsStringgrid.Row := TRow;
  PointsStringgrid.Col := TCol;
//  StatusBar.ProgressPanel := false;
  Screen.Cursor := crDefault;
end;

function TPointsCoordDialog.InputDegreeMode(ADataString: string; ADegreeMode: integer): double;
const Minut = '''';
var Position: integer;
  Gradus, Minute, PartMinute, Second, PartSecond: double;

begin;

    //Gradus
  Position := Pos(chr(176), AdataString);
  Gradus := StrToFloat(copy(AdataString, 1, Position - 1));
  AdataString := copy(ADataString, Position + 1, Length(AdataString) - Position);
   //Minute
  Position := Pos(Minut, AdataString);
  Minute := StrToFloat(copy(AdataString, 1, Position - 1));
  AdataString := copy(ADataString, Position + 1, Length(AdataString) - Position);

  if ADegreeMode = 0
    then
  begin;
      //Seconds+subseconds
    Position := Pos('"', AdataString);
    Second := StrToFloat(copy(AdataString, 1, Position - 1));
    Second := Second / 60;
    Minute := (Minute + Second) / 60;
  end
  else
    Minute := Minute / 60;
  {   begin;
      //subseconds
      PartMinute:=StrToFloat(copy(ADataString,Position+1,Length(AdataString)-Position));
       Minute:=Minute/60+PartMinute;
     end;}
  Gradus := Gradus + Minute;
  Result := Gradus;

end;

procedure TPointsCoordDialog.InputDegreeMode1(ADataString: string; var Gradus, Minute, Second: string);
const Minut = '''';
var Position: integer;
  AGradus, AMinute, ASecond: double;

begin;

    //Gradus
  Position := Pos(chr(176), AdataString);
  Gradus := copy(AdataString, 1, Position - 1);
  AdataString := copy(ADataString, Position + 1, Length(AdataString) - Position);
   //Minute
  Position := Pos(Minut, AdataString);
  Minute := copy(AdataString, 1, Position - 1);
  AdataString := copy(ADataString, Position + 1, Length(AdataString) - Position);

  if IniFile.LoadPointsCoordDataFromFile.DegreeMode = 0
    then
  begin;
      //Seconds+subseconds
    Position := Pos('"', AdataString);
    Second := copy(AdataString, 1, Position - 1);
  end
end;

function TPointsCoordDialog.OutputDegreeMode(ADataFloat: double): string;
const Minut = '''';
var Position: integer;
  Gradus, Minute, PartMinute, Second, PartSecond: double;

begin;
  Gradus := Trunc(ADataFloat);
  Minute := Abs(Frac(ADataFloat) * 60);

  if Inifile.LoadPointsCoordDataFromFile.DegreeMode = 0
    then begin;
    Result := FormatIt1(Frac(Minute) * 60);
    Minute := Trunc(Minute);
    Result := FormatFloat('00', Gradus) + chr(176) + FormatFloat('00', Minute) + '''' + Result + '"';
  end
  else
  begin;
    Result := FormatFloat('00', Gradus) + chr(176) + FormatIt1(Minute) + '''';
  end;
end;

function TPointsCoordDialog.OutputDegreeMode1: string;
const Minut = '''';
var i: integer;
var Position: integer;
  Gradus, Minute, PartMinute, Second, PartSecond: double;

begin;
 {  if Length(GradusEdit.Text)=1
    then GradusEdit.Text:='0'+GradusEdit.Text;

     if Length(MinutEdit.Text)=1
    then MinutEdit.Text:='0'+MinutEdit.Text;
  }
  Result := GradusEdit.Text + chr(176) + MinuteEdit.Text + Minut;

  if Inifile.LoadPointsCoordDataFromFile.DegreeMode = 0
    then
    Result := Result + SecondEdit.Text + '"';

end;

function TPointsCoordDialog.OutputDegreeMode2: string;
const Minut = '''';
var Position: integer;
  Gradus, Minute, PartMinute, Second, PartSecond: double;

begin;

  Result := GradusEdit1.Text + chr(176) + MinuteEdit1.Text + Minut;

  if Inifile.LoadPointsCoordDataFromFile.DegreeMode = 0
    then
    Result := Result + SecondEdit1.Text + '"'
end;

procedure TPointsCoordDialog.SetProjection(var X, Y: double);
begin;
  AxGISProjection1.Calculate(X, Y);
end;


procedure TPointsCoordDialog.FormDestroy(Sender: TObject);
begin

  if (OrtoBtn <> nil) and (Project <> nil) then begin
     if (OrtoBtn.Visible) and (OrtoBtn.Enabled) then
        PProj(Project)^.Registry.WriteBool('\User\WinGISUI\DrawingTools\Ortho', OrtoBtn.Down)
     else
        PProj(Project)^.Registry.WriteBool('\User\WinGISUI\DrawingTools\Ortho', false);
  end;

  if AXGisProjection1 <> nil then
     AxGISProjection1.Destroy;
  AxGISProjection1 := nil;
  if ObjectType = ot_symbol then begin
    ASymLibInfo.Free;
    FFonts := nil;
    ASymInfo.Free;
    SymlibList.Free;
  end;
end;

procedure TPointsCoordDialog.MinuteEditExit(Sender: TObject);
var s: integer;
  v: double;
begin
  if (Trim(MinuteEdit.Text) = '')
    or (MinuteEdit.Text = '0')
    then MinuteEdit.Text := '00';

  if IniFile.LoadPointsCoordDataFromFile.DegreeMode = 0
    then
  begin;
    try
      s := StrToInt(MinuteEdit.Text);
      if (s >= 60) or (s < 0) then
        s := s div (s - s);
      PointsStringGrid.Cells[PointsStringGrid.Col, PointsStringGrid.Row] := OutputDegreeMode1;
    except
      MessageBeep(MB_ICONEXCLAMATION);
      MinuteEdit.SetFocus;
    end;
  end
  else
  begin;
    try
      V := StrToFloat(MinuteEdit.Text);
      if (v >= 60) or (v < 0) then
        V := v / (v - v);
      PointsStringGrid.Cells[PointsStringGrid.Col, PointsStringGrid.Row] := OutputDegreeMode1;
    except
      MessageBeep(MB_ICONEXCLAMATION);
      MinuteEdit.SetFocus;
    end;
  end; //else
end;



procedure TPointsCoordDialog.SecondEditExit(Sender: TObject);
var v: double;
begin
  try
    if (Trim(SecondEdit.Text) = '')
      or (SecondEdit.Text = '0')
      then SecondEdit.Text := '00';

    V := StrToFloat(SecondEdit.Text);
    if (v >= 60) or (v < 0) then
      V := v / (v - v);
    PointsStringGrid.Cells[PointsStringGrid.Col, PointsStringGrid.Row] := OutputDegreeMode1;
  except
    MessageBeep(MB_ICONEXCLAMATION);
    SecondEdit.SetFocus;
  end;

end;

procedure TPointsCoordDialog.InvisibleGradus;
var Flag: boolean;
begin;
  GradusEdit.Visible := false;
  MinuteEdit.Visible := false;
  SecondEdit.Visible := false;
  if StringEdit.Visible = true
    then
  begin;
    PointsStringGrid.Cells[PointsStringGrid.Col, PointsStringGrid.Row] := StringEdit.Text;
  // PointsStringGrid.OnSelectCell(Self,PointsStringGrid.Col,PointsStringGrid.Row,Flag);
    // StringEdit.OnExit(StringEdit);
    StringEditValid.Edit := nil;
    StringEdit.Visible := false;
  end;
end;

procedure TPointsCoordDialog.InvisibleGradus1;
var Flag: boolean;
begin;
  GradusEdit1.Visible := false;
  MinuteEdit1.Visible := false;
  SecondEdit1.Visible := false;
end;

procedure TPointsCoordDialog.RadioGroup1Click(Sender: TObject);
begin
  InvisibleGradus;
end;

procedure TPointsCoordDialog.ModeComboClick(Sender: TObject);
begin
  InvisibleGradus;
end;

procedure TPointsCoordDialog.Edit1Click(Sender: TObject);
begin
  InvisibleGradus;
end;

procedure TPointsCoordDialog.Edit2Click(Sender: TObject);
begin
  InvisibleGradus;
end;

procedure TPointsCoordDialog.Edit3Click(Sender: TObject);
begin
  InvisibleGradus;
end;

procedure TPointsCoordDialog.Edit4Click(Sender: TObject);
begin
  if BadFlag <> ''
    then
    Exit;
//SetFocus(BadFlag);
  InvisibleGradus;
  NowEdit := 1;
  SimplyModeGradusEdit(Edit4);
end;

procedure TPointsCoordDialog.Edit5Click(Sender: TObject);
begin
  if BadFlag <> '' then
//Edit4.SetFocus;
    Exit;
  InvisibleGradus;
  NowEdit := 2;
  SimplyModeGradusEdit(Edit5);
end;

procedure TPointsCoordDialog.OKBtnClick(Sender: TObject);
begin
  InvisibleGradus;
end;

procedure TPointsCoordDialog.PointsStringGridRowMoved(Sender: TObject;
  FromIndex, ToIndex: Integer);
begin
  InvisibleGradus;
end;

procedure TPointsCoordDialog.PointsStringGridColumnMoved(Sender: TObject;
  FromIndex, ToIndex: Integer);
begin
  InvisibleGradus;
end;

procedure TPointsCoordDialog.PointsStringGridTopLeftChanged(
  Sender: TObject);
begin
  InvisibleGradus;
end;


procedure TPointsCoordDialog.GradusEditExit(Sender: TObject);
var s: integer;
begin
  try
    if (Trim(GradusEdit.Text) = '')
      or (GradusEdit.Text = '0')
      then GradusEdit.Text := '00';
    s := StrToInt(GradusEdit.Text);
//    if (s > 180) or (s < -180) then
//      s := s div (s - s);
    PointsStringGrid.Cells[PointsStringGrid.Col, PointsStringGrid.Row] := OutputDegreeMode1;
  except
    MessageBeep(MB_ICONEXCLAMATION);
    GradusEdit.SetFocus;
  end;

end;

procedure TPointsCoordDialog.GradusEditKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var Flag: boolean;
begin
  Flag := true;
  if (Key = VK_return) or (Key = VK_space) or (Key = VK_tab)
    then MinuteEdit.SetFocus
  else if Key = vk_Escape then
  begin;
    InvisibleGradus;
    PointsStringGrid.Cells[PointsStringGrid.Col, PointsStringGrid.Row] := EditBuffer;
    PointsStringGrid.OnSelectCell(Self, PointsStringGrid.Col, PointsStringGrid.Row, Flag);
  end;
end;

procedure TPointsCoordDialog.MinuteEditKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var Flag: boolean;
begin
  if ((Key = VK_return) or (Key = VK_space) or (Key = VK_tab)) and (SecondEdit.Visible = true)
    then SecondEdit.SetFocus
  else if Key = vk_Escape then
  begin;
    InvisibleGradus;
    PointsStringGrid.Cells[PointsStringGrid.Col, PointsStringGrid.Row] := EditBuffer;
    PointsStringGrid.OnSelectCell(Self, PointsStringGrid.Col, PointsStringGrid.Row, Flag);
  end
  else if (Key = VK_BACK) and (Length(MinuteEdit.Text) = 0) then GradusEdit.SetFocus;
end;

procedure TPointsCoordDialog.SecondEditKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var Flag: boolean;
begin
  if Key = vk_Escape then
  begin;
    InvisibleGradus;
    PointsStringGrid.Cells[PointsStringGrid.Col, PointsStringGrid.Row] := EditBuffer;
    PointsStringGrid.OnSelectCell(Self, PointsStringGrid.Col, PointsStringGrid.Row, Flag);
  end
  else if (Key = VK_BACK) and (Length(SecondEdit.Text) = 0) then MinuteEdit.SetFocus;
end;




procedure TPointsCoordDialog.CoordBtn2Click(Sender: TObject);
begin
  InvisibleGradus;
  InvisibleGradus1;
  Z2 := PointsCoordDlg2.TPointsCoordDialog2.Create(Parent);
  TmpInputMode := Inifile.LoadPointsCoordDataFromFile.InputMode;
  TmpDegreeMode := Inifile.LoadPointsCoordDataFromFile.DegreeMode;

  if Z2.ShowModal = mrOk
    then
  begin;
    Z2.ChangeData;
    if TmpInputMode <> Inifile.LoadPointsCoordDataFromFile.InputMode
      then
    begin;
      PointsStringGrid.Cells[1, PointsStringGrid.Row] := Edit4.Text;
      PointsStringGrid.Cells[2, PointsStringGrid.Row] := Edit5.Text;
      ResetPoints(false);
      Edit4.Text := PointsStringGrid.Cells[1, PointsStringGrid.Row];
      Edit5.Text := PointsStringGrid.Cells[2, PointsStringGrid.Row];
    end
    else //degree
      if (Inifile.LoadPointsCoordDataFromFile.InputMode = 1) and
        (TmpDegreeMode <> Inifile.LoadPointsCoordDataFromFile.DegreeMode)
        then
        ; //ResetPoints(false);
  end;
  Z2.Free;

end;


procedure TPointsCoordDialog.SimplyModeGradusEdit(Sender: TOBject);
var
  EditRect: TRect;
  Bounds: TRect;
  Agr, Ami, Ase: string;
begin

  if PProj(Project).PInfo.ProjectionSettings.ProjectProjection = 'NONE'
    then
    Inifile.LoadPointsCoordDataFromFile.InputMode := 0;

  if Inifile.LoadPointsCoordDataFromFile.InputMode = 0 then Exit;

  EditBuffer := (Sender as TEdit).Text;
  EditRect := (Sender as TEdit).BoundsRect;
  EditRect.Bottom := EditRect.Bottom + GroupBox1.Top + PointsStringGrid.Top + 1;
  EditRect.Top := EditRect.Top + GroupBox1.Top + PointsStringGrid.Top;
  EditRect.Left := EditRect.Left + GroupBox1.Left + PointsStringGrid.Left;
  EditRect.Right := EditRect.Right + GroupBox1.Left + PointsStringGrid.Left + 1;

// if Inifile.LoadPointsCoordDataFromFile.InputMode=0 then
//  begin;
//  Exit


 //:=PointsStringGrid.Font;


//     EditRect.Right:=EditRect.Right-1;
  //   EditRect.Top:=EditRect.Top-1;
    // EditRect.Bottom:=EditRect.Bottom+2;



  if Inifile.LoadPointsCoordDataFromFile.DegreeMode = 1
    then begin; //gradus+minute only !
    GradusEdit1.BoundsRect := EditRect;
    MinuteEdit1.BoundsRect := EditRect;
    GradusEdit1.Width := trunc(GradusEdit1.Width / 6);
    MinuteEdit1.Width := MinuteEdit1.Width - GradusEdit1.Width + 2;
    MinuteEdit1.Left := GradusEdit1.Width + GradusEdit1.Left - 2;
    GradusEdit1.Visible := true;
    MinuteEdit1.Visible := true;
    MinuteEdit1.MaxLength := 10;
  end
  else
  begin;
    GradusEdit1.BoundsRect := EditRect;
    MinuteEdit1.BoundsRect := EditRect;
    SecondEdit1.BoundsRect := EditRect;
    GradusEdit1.Width := trunc(GradusEdit1.Width / 6);
    MinuteEdit1.Width := trunc(MinuteEdit1.Width / 8.5) + 2;
    SecondEdit1.Width := SecondEdit1.Width - GradusEdit1.Width - MinuteEdit1.Width + 5;
    MinuteEdit1.Left := GradusEdit1.Width + GradusEdit1.Left - 2;
    SecondEdit1.Left := MinuteEdit1.Left + MinuteEdit1.Width - 3;
    GradusEdit1.Visible := true;
    MinuteEdit1.Visible := true;
    SecondEdit1.Visible := true;
    MinuteEdit1.MaxLength := 2;
    SecondEdit1.MaxLength := 9;
  end;
  InputDegreeMode1((Sender as TEdit).Text, Agr, Ami, ASe);
  GradusEdit1.Text := Agr;
  MinuteEdit1.Text := Ami;
  SecondEdit1.Text := Ase;
  GradusEdit1.SetFocus;

end; //if

procedure TPointsCoordDialog.GradusEdit1Exit(Sender: TObject);
var s: integer;
begin
  BadFlag := '';
  try
    if (Trim(GradusEdit1.Text) = '')
      or (GradusEdit1.Text = '0')
      then GradusEdit1.Text := '00';
    s := StrToInt(GradusEdit1.Text);
    if (s > 180) or (s < -180) then
      s := s div (s - s);
    if NowEdit = 1 then
      Edit4.Text := OutputDegreeMode2
    else
      Edit5.Text := OutputDegreeMode2;
  except
    MessageBeep(MB_ICONEXCLAMATION);
    BadFlag := 'GradusEdit1';
    GradusEdit1.SetFocus;
  end;


end;

procedure TPointsCoordDialog.GradusEdit1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var Flag: boolean;
begin
  Flag := true;
  if (Key = VK_return) or (Key = VK_space) or (Key = VK_tab)
    then MinuteEdit1.SetFocus
  else if Key = vk_Escape then
  begin;
    InvisibleGradus1;
    if NowEdit = 1 then
      Edit4.Text := EditBuffer
    else
      Edit5.Text := EditBuffer;
  end;

end;


procedure TPointsCoordDialog.MinuteEdit1Exit(Sender: TObject);
var s, oldedit: integer;
  v: double;
begin
  BadFlag := '';
  if (Trim(MinuteEdit1.Text) = '')
    or (MinuteEdit1.Text = '0')
    then MinuteEdit1.Text := '00';
  OLdEdit := NowEdit;
  if IniFile.LoadPointsCoordDataFromFile.DegreeMode = 0
    then
  begin;
    try
      s := StrToInt(MinuteEdit1.Text);
      if (s >= 60) or (s < 0) then
        s := s div (s - s);
      if NowEdit = 1 then
        Edit4.Text := OutputDegreeMode2
      else
        Edit5.Text := OutputDegreeMode2;
    except
      MessageBeep(MB_ICONEXCLAMATION);
      BadFlag := 'MinuteEdit1';
      MinuteEdit1.SetFocus;
    end;
  end
  else
  begin;
    try

      V := StrToFloat(MinuteEdit1.Text);
      if (v >= 60) or (v < 0) then
        V := v / (v - v);
      if NowEdit = 1 then
        Edit4.Text := OutputDegreeMode2
      else
        Edit5.Text := OutputDegreeMode2;
    except
      MessageBeep(MB_ICONEXCLAMATION);
      BadFlag := 'MinuteEdit1';
      MinuteEdit1.SetFocus;
    end;

  end;
end;


procedure TPointsCoordDialog.MinuteEdit1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var Flag: boolean;
begin
  if ((Key = VK_return) or (Key = VK_space) or (Key = VK_tab)) and (SecondEdit1.Visible = true)
    then SecondEdit1.SetFocus
  else if Key = vk_Escape then
  begin;
    InvisibleGradus;
    if NowEdit = 1 then
      Edit4.Text := EditBuffer
    else
      Edit5.Text := EditBuffer;
  end
  else if (Key = VK_BACK) and (Length(MinuteEdit1.Text) = 0) then GradusEdit1.SetFocus;
end;



procedure TPointsCoordDialog.SecondEdit1Exit(Sender: TObject);
var v: double;
begin
  BadFlag := '';
  try
    if (Trim(SecondEdit1.Text) = '')
      or (SecondEdit1.Text = '0')
      then SecondEdit1.Text := '00';

    V := StrToFloat(SecondEdit1.Text);
    if (v >= 60) or (v < 0) then
      V := v / (v - v);
    if NowEdit = 1 then
      Edit4.Text := OutputDegreeMode2
    else
      Edit5.Text := OutputDegreeMode2;
  except
    MessageBeep(MB_ICONEXCLAMATION);
    BadFlag := 'SecondEdit1';
    SecondEdit1.SetFocus;
  end;

end;

procedure TPointsCoordDialog.SecondEdit1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var Flag: boolean;
begin
  if Key = vk_Escape then
  begin;
    InvisibleGradus;
    if NowEdit = 1 then
      Edit4.Text := EditBuffer
    else
      Edit5.Text := EditBuffer;
  end
  else if (Key = VK_BACK) and (Length(SecondEdit1.Text) = 0) then MinuteEdit1.SetFocus;

end;

procedure TPointsCoordDialog.TmpClosed;
begin;
  if (PProj(Project).PInfo.ProjectionSettings.ProjectProjSettings = 'NONE')
    or (ObjectType = mn_New_Circle) or (ObjectType = mn_New_Arc)
    or (ObjectType = mn_Edit_Circle) or (ObjectType = mn_Edit_Arc)
    then
  begin;
    Inifile.LoadPointsCoordDataFromFile.InputMode := 0;
    CoordTypeBtn.Enabled := false;
    CoordBtn2.Enabled := false;
  end;
end;

procedure TPointsCoordDialog.TmpClosed1;
begin;
  if Inifile.LoadPointsCoordDataFromFile.InputMode = 1
    then
  begin;
       // PointsStringGrid.SetFocus;
    SpecificCheckBox.Checked := false;
    SpecificCheckBox.OnClick(Self);
    SpecificCheckBox.Enabled := false;
    OrtoBtn.Enabled := false;
  end
  else
    SpecificCheckBox.Enabled := true;

end;

procedure TPointsCoordDialog.Edit4MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Inifile.LoadPointsCoordDataFromFile.InputMode = 1
    then
    if GradusEdit1.Visible = false then
      Edit4.OnClick(Self);
end;

procedure TPointsCoordDialog.Edit5MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Inifile.LoadPointsCoordDataFromFile.InputMode = 1
    then
    if GradusEdit1.Visible = false then
      Edit5.OnClick(Self);
end;

procedure TPointsCoordDialog.PMSaveClick(Sender: TObject);
var
i: Integer;
List: TStringList;
s: String;
begin
     if SaveDialog.Execute then begin
        List:=TStringList.Create;
        try
           for i:=0 to PointsStringGrid.RowCount-1 do begin
               s:=PointsStringGrid.Cells[1,i] + ';' + PointsStringGrid.Cells[2,i]; 
               List.Add(s);
           end;
           List.SaveToFile(SaveDialog.Filename);
        finally
               List.Free;
        end;
     end;
end;

end.

