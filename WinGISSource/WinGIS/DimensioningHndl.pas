Unit DimensioningHndl;

Interface

Uses ProjHndl,RegDB;

Type TDrawDimensioningHandler = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;

     TCustomDimensioningHandler = Class(TProjMenuHandler)
       Procedure   OnActivate; override;
       Procedure   OnDeActivate; override;
       Procedure   OnStart; override;
     end;

     TDimensioningPointToPointHandler = Class(TCustomDimensioningHandler)
       Procedure   OnStart; override;
     end;

     TDimensioningOrthogonalHandler = Class(TCustomDimensioningHandler)
       Procedure   OnStart; override;
     end;

     TDimensioningIntersectionHandler = Class(TCustomDimensioningHandler)
       Procedure   OnStart; override;
     end;

     TDimensioningOptionsHandler = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;

Implementation

{$R *.mfn}

Uses AM_Def,MenuFn,Am_ProjP;

{===============================================================================
  TDrawDimensioningHandler
+==============================================================================}

Procedure TDrawDimensioningHandler.OnStart;
var CurrentFn      : String;
begin                                                     
  with Project^ do begin
    CurrentFn:=Registry.ReadString('\User\WinGISUI\DrawingTools\Dimensioning\InputType');
    if Registry.LastError<>rdbeNoError then CurrentFn:='DimensioningPointToPoint';
    MenuFunctions[CurrentFn].Execute;
  end;
end;

{===============================================================================
  TCustomDimensioningHandler
+==============================================================================}

Procedure TCustomDimensioningHandler.OnStart;
begin
  Project^.Registry.WriteString('\User\WinGISUI\DrawingTools\Dimensioning\InputType',MenuName);
end;

Procedure TCustomDimensioningHandler.OnActivate;
begin
  MenuFunctions['DrawDimensioning'].Checked:=TRUE;
end;

Procedure TCustomDimensioningHandler.OnDeActivate;
begin
  MenuFunctions['DrawDimensioning'].Checked:=FALSE;
end;

{===============================================================================
  TDimensioningPointToPointHandler
+==============================================================================}

Procedure TDimensioningPointToPointHandler.OnStart;
begin
  inherited OnStart;
  Project^.SetActualMenu(mn_MesPoint);
end;

{===============================================================================
  TDimensioningOrthogonalHandler
+==============================================================================}

Procedure TDimensioningOrthogonalHandler.OnStart;
begin
  inherited OnStart;
  Project^.SetActualMenu(mn_MesOrtho);
end;

{===============================================================================
  TDimensioningIntersectionHandler
+==============================================================================}

Procedure TDimensioningIntersectionHandler.OnStart;
begin
  inherited OnStart;
  Project^.SetActualMenu(mn_MesCut);
end;

{===============================================================================
  TDimensioningOptionsHandler
+==============================================================================}

Procedure TDimensioningOptionsHandler.OnStart;
begin
  {$IFNDEF AXDLL} // <----------------- AXDLL
    {$IFNDEF WMLT}
  ProcMeasureOptions(Project);
    {$ENDIF}
  {$ENDIF} // <----------------- AXDLL
end;

Initialization;
  MenuFunctions.RegisterFromResource(HInstance,'DimensioningHndl','DimensioningHndl');
  TDrawDimensioningHandler.Registrate('DrawDimensioning');
  TDimensioningPointToPointHandler.Registrate('DimensioningPointToPoint');
  TDimensioningOrthogonalHandler.Registrate('DimensioningOrthogonal');
  TDimensioningIntersectionHandler.Registrate('DimensioningIntersection');
  TDimensioningOptionsHandler.Registrate('DimensioningOptions');

end.
