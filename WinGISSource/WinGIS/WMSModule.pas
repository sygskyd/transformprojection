unit WMSModule;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, JPEG, OleCtrls, AxGisPro_TLB, AM_Def, WinOSInfo, AM_Paint,
  MultiLng, ExtCtrls, PNGImage, Buttons;

type
  TWMSForm = class(TForm)
    AxGisProjection: TAxGisProjection;
    MlgSection: TMlgSection;
    GroupBoxSettings: TGroupBox;
    LabelHost: TLabel;
    EditHost: TEdit;
    LabelLayers: TLabel;
    EditLayers: TEdit;
    ComboBoxFormat: TComboBox;
    LabelInfo: TLabel;
    LabelLegend: TLabel;
    EditEPSG: TEdit;
    LabelEPSG: TLabel;
    LabelFormat: TLabel;
    OpenDialog: TOpenDialog;
    SaveDialog: TSaveDialog;
    GroupBoxButtons: TGroupBox;
    ButtonApply: TButton;
    ButtonClose: TButton;
    CheckBoxOnOff: TCheckBox;
    ButtonOpen: TSpeedButton;
    ButtonSave: TSpeedButton;
    LabelEPSGLink: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CheckBoxOnOffClick(Sender: TObject);
    procedure ButtonGetCapabilitiesClick(Sender: TObject);
    procedure ButtonCloseClick(Sender: TObject);
    procedure ButtonApplyClick(Sender: TObject);
    procedure LabelInfoClick(Sender: TObject);
    procedure LabelLegendClick(Sender: TObject);
    procedure ButtonOpenClick(Sender: TObject);
    procedure ButtonSaveClick(Sender: TObject);
    procedure EditChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure LabelEPSGLinkClick(Sender: TObject);
  private
     LLX,LLY: Double;
     URX,URY: Double;
     Jpg: TJPEGImage;
     Png: TPngObject;
     GetMapUrl: AnsiString;
     GetCapUrl: AnsiString;
     GetLegUrl: AnsiString;
     ProjRefPoint: TDPoint;
     LanguageCode: AnsiString;
     CanvasRect: TRect;
     MemStream: TMemoryStream;
     LastUrl: AnsiString;
     ImageFormat: AnsiString;
     procedure BuildGetMapUrl;
     procedure BuildGetCapUrl;
     procedure BuildGetLegUrl(Layer: AnsiString);
     function ReadLanguage: AnsiString;
     function GetCanvasRect: Boolean;
     procedure GetViewRect;
     procedure DrawImage;
     function GetImage: Boolean;
     procedure GetCap;
     procedure GetLeg(Layer: AnsiString);
     procedure EnableControls;
     procedure GetAllLegends;
     procedure XYToLongLat(var ALongitude,ALatitude: Double);
     procedure OpenBrowser(AFilename: AnsiString);
     procedure OpenText(AFilename: AnsiString);
     procedure Redraw;
     procedure EnumLayers;
  public
     APInfo: PPaint;
     Transparent: Boolean;
     procedure DrawMap;
     procedure OpenWWCFile(AFilename: AnsiString);
  end;

var
  WMSForm: TWMSForm;

const GetMapUrlTemplate = '{host}SERVICE=WMS&VERSION=1.1.1&BBOX={bbox}&WIDTH={width}&HEIGHT={height}&FORMAT={format}&SRS=EPSG:{epsg}&REQUEST=GetMap&LAYERS={layers}&STYLES=';
const GetCapUrlTemplate = '{host}SERVICE=WMS&VERSION=1.1.1&REQUEST=GetCapabilities';
const GetLegUrlTemplate = '{host}SERVICE=WMS&VERSION=1.1.1&SERVICE=WMS&REQUEST=GetLegendGraphic&layer={layer}&FORMAT=image/png&STYLE=';

//const GetMapUrlTemplate = '{host}SERVICE=WMS&VERSION=1.1.1&BBOX={bbox}&WIDTH={width}&HEIGHT={height}&FORMAT={format}&SRS=EPSG:{epsg}&REQUEST=GetMap&LAYERS={layers}&STYLES=';
//const GetCapUrlTemplate = '{host}SERVICE=WMS&VERSION=1.1.1&REQUEST=GetCapabilities';
//const GetLegUrlTemplate = '{host}SERVICE=WMS&version=1.1.1&SERVICE=WMS&REQUEST=GetLegendGraphic&layer={layer}&FORMAT=image/png&STYLE=';

const JPG_TEMP_FILE = 'wms.jpg';
const PNG_TEMP_FILE = 'wms.png';
const HTM_TEMP_FILE = 'wms.htm';
const TXT_TEMP_FILE = 'wms.txt';                
const CAP_TEMP_FILE = 'wms.xml';
const LEG_TEMP_FILE = 'wms_legend.htm';

const EPSG_WGS84 = '4326';

implementation

uses AM_Main, IniFiles, ErrHndl, WinInet, ShellApi, ToolsLib, MenuFn, Registry;

{$R *.DFM}

procedure TWMSForm.FormCreate(Sender: TObject);
begin
     APInfo:=nil;
     Jpg:=TJPEGImage.Create;
     Png:=TPngObject.Create;
     MemStream:=TMemoryStream.Create;
     AxGisProjection.Top:=-100;
     AxGisProjection.WorkingPath:=OSInfo.WingisDir;
     LanguageCode:=ReadLanguage;
     AxGisProjection.LngCode:=LanguageCode;
     AXGisProjection.ShowOnlySource:=True;
     ProjRefPoint.Init(0,0);
     CanvasRect:=Rect(0,0,0,0);
     LastUrl:='';
     ImageFormat:='';
end;

procedure TWMSForm.FormShow(Sender: TObject);
begin
     EnableControls;
end;

procedure TWMSForm.FormDestroy(Sender: TObject);
begin
     Jpg.Free;
     Png.Free;
     MemStream.Free;
end;

procedure TWMSForm.EnableControls;
begin
     EditHost.Enabled:=CheckBoxOnOff.Checked;
     LabelInfo.Enabled:=(CheckBoxOnOff.Checked) and (Trim(EditHost.Text) <> '');
     EditLayers.Enabled:=CheckBoxOnOff.Checked;
     LabelLegend.Enabled:=(CheckBoxOnOff.Checked) and (Trim(EditLayers.Text) <> '');
     EditEPSG.Enabled:=CheckBoxOnOff.Checked;
     LabelEPSGLink.Enabled:=CheckBoxOnOff.Checked;
     ComboBoxFormat.Enabled:=CheckBoxOnOff.Checked;
     ButtonSave.Enabled:=(CheckBoxOnOff.Checked) and (Trim(EditHost.Text) <> '');
     if CheckBoxOnOff.Checked then begin
        EditHost.Color:=clWindow;
        EditLayers.Color:=clWindow;
        EditEPSG.Color:=clWindow;
        ComboBoxFormat.Color:=clWindow;
    end
     else begin
        EditHost.Color:=clBtnFace;
        EditLayers.Color:=clBtnFace;
        EditEPSG.Color:=clBtnFace;
        ComboBoxFormat.Color:=clBtnFace;
     end;
     if ComboBoxFormat.ItemIndex = -1 then begin
        ComboBoxFormat.ItemIndex:=0;
     end;
     Transparent:=(ComboBoxFormat.Text = 'image/png');
end;

procedure TWMSForm.CheckBoxOnOffClick(Sender: TObject);
begin
     EnableControls;
end;

procedure TWMSForm.EditChange(Sender: TObject);
begin
     EnableControls;
end;                            

procedure TWMSForm.DrawMap;
begin
     if APInfo = nil then begin
        exit;
     end;
     if (Trim(EditHost.Text) = '') or (Trim(EditLayers.Text) = '') then begin
        exit;
     end;
     if Trim(EditEPSG.Text) = '' then begin
        EditEPSG.Text:=EPSG_WGS84;
     end;
     GetCanvasRect;
     GetViewRect;
     BuildGetMapUrl;
     if GetImage then begin
        DrawImage;
     end;
end;

procedure TWMSForm.XYToLongLat(var ALongitude,ALatitude: Double);
begin
     try
        AxGISProjection.SourceProjSettings:=APInfo^.ProjectionSettings.ProjectProjSettings;
        AxGISProjection.SourceProjection:=APInfo^.ProjectionSettings.ProjectProjection;
        AxGISProjection.SourceDate:=APInfo^.ProjectionSettings.ProjectDate;
        AxGISProjection.SourceXOffset:=APInfo^.ProjectionSettings.ProjectionXOffset;
        AxGISProjection.SourceYOffset:=APInfo^.ProjectionSettings.ProjectionYOffset;
        AxGisProjection.SourceScale:=APInfo^.ProjectionSettings.ProjectionScale;

        AxGISProjection.TargetProjSettings:='Geodetic Phi/Lamda';
        AxGISProjection.TargetProjection:='Phi/Lamda values';
        AxGISProjection.TargetDate:='WGS84';
        AxGISProjection.TargetXOffset:=0;
        AxGISProjection.TargetYOffset:=0;
        AxGisProjection.TargetScale:=1;

        AXGisProjection.Calculate(ALongitude,ALatitude);
     except
        on E: Exception do begin
           DebugMsg('TVEForm.XYToLongLat',E.Message);
        end;
     end;
end;

procedure TWMSForm.BuildGetMapUrl;
var
s,BBox: AnsiString;
w,h: Integer;
l,t,r,b: AnsiString;
begin
     s:=GetMapUrlTemplate;

     EditHost.Text:=Trim(EditHost.Text);
     if Pos('?',EditHost.Text) = 0 then begin
        EditHost.Text:=EditHost.Text + '?';
     end;

     s:=StringReplace(s,'{host}',Trim(EditHost.Text),[]);
     s:=StringReplace(s,'{format}',Trim(ComboBoxFormat.Text),[]);

     EditLayers.Text:=StringReplace(EditLayers.Text,' ','',[rfReplaceAll]);
     EnumLayers;
     s:=StringReplace(s,'{layers}',EditLayers.Text,[]);

     EditEPSG.Text:=Trim(EditEPSG.Text);
     s:=StringReplace(s,'{epsg}',EditEPSG.Text,[]);

     w:=CanvasRect.Right - CanvasRect.Left;
     h:=CanvasRect.Bottom - CanvasRect.Top;

     s:=StringReplace(s,'{width}',IntToStr(w),[]);
     s:=StringReplace(s,'{height}',IntToStr(h),[]);

     l:=StringReplace(FloatToStr(LLX),',','.',[]);
     b:=StringReplace(FloatToStr(LLY),',','.',[]);
     r:=StringReplace(FloatToStr(URX),',','.',[]);
     t:=StringReplace(FloatToStr(URY),',','.',[]);

     BBox:=l + ',' + b + ',' + r + ',' + t;

     s:=StringReplace(s,'{bbox}',BBox,[]);
     GetMapUrl:=s;
end;

procedure TWMSForm.DrawImage;
var
AXPos,AYPos: Double;
ARect: TRect;
AOldStretchMode: Integer;
AOldMapMode: Integer;
AFileName: AnsiString;
ACanvas: TCanvas;
begin
     if ImageFormat = 'image/jpeg' then begin
        AFileName:=OSInfo.TempDataDir + JPG_TEMP_FILE;
     end
     else if ImageFormat = 'image/png' then begin
        AFileName:=OSInfo.TempDataDir + PNG_TEMP_FILE;
     end
     else begin
        Exit;
     end;

     if not FileExists(AFileName) then begin
        Exit;                            
     end;

     ACanvas:=TCanvas.Create;
     ACanvas.Handle:=APInfo^.ExtCanvas.Handle;
     AOldMapMode:=SetMapMode(APInfo^.ExtCanvas.Handle,MM_TEXT);
     ARect:=Rect(CanvasRect.Left,CanvasRect.Top,CanvasRect.Right,CanvasRect.Bottom);
     try
        if ImageFormat = 'image/jpeg' then begin
           Jpg.LoadFromFile(AFileName);
           ACanvas.Draw(ARect.Left,ARect.Top,Jpg);
        end
        else if ImageFormat = 'image/png' then begin
           Png.LoadFromFile(AFileName);
           Transparent:=Png.Transparent;
           Png.Draw(ACanvas,ARect);
        end;
     finally
        SetMapMode(APInfo^.ExtCanvas.Handle,AOldMapMode);
        ACanvas.Free;
     end;
end;

procedure TWMSForm.ButtonGetCapabilitiesClick(Sender: TObject);
begin
     Screen.Cursor:=crHourGlass;
     try
        BuildGetCapUrl;
        GetCap;
     finally
        Screen.Cursor:=crDefault;
     end;
end;

procedure TWMSForm.BuildGetCapUrl;
var
s: AnsiString;
begin
     s:=GetCapUrlTemplate;
     EditHost.Text:=Trim(EditHost.Text);
     if Pos('?',EditHost.Text) = 0 then begin
        EditHost.Text:=EditHost.Text + '?';
     end;
     s:=StringReplace(s,'{host}',EditHost.Text,[]);
    GetCapUrl:=s;
end;

procedure TWMSForm.GetCap;
const BUFFER_SIZE = 10000;
var
Inet,hURL: HInternet;
ReadSize: DWORD;
Buffer: Array[0..BUFFER_SIZE-1] of Char;
CapFilename: AnsiString;
begin
     CapFilename:=OSInfo.TempDataDir + CAP_TEMP_FILE;
     DeleteFile(CapFilename);
     Inet:=InternetOpen(nil,INTERNET_OPEN_TYPE_PRECONFIG,nil,nil,0);
     if Inet <> nil then begin
        hURL:=InternetOpenURL(Inet,PChar(GetCapUrl),nil,0,INTERNET_FLAG_RELOAD,0);
        if hURL <> nil then begin
           MemStream.Clear;
           MemStream.Position:=0;
           repeat
              InternetReadFile(hURL,@Buffer,Buffer_Size,ReadSize);
              MemStream.WriteBuffer(Buffer,ReadSize);
           until ReadSize = 0;
           MemStream.SaveToFile(CapFilename);
           if FileExists(CapFilename) then begin
              OpenBrowser(CapFilename);
           end;
           InternetCloseHandle(hURL);
        end;
        InternetCloseHandle(Inet);
      end;
end;

function TWMSForm.GetCanvasRect: Boolean;
begin
     Result:=True;
     if IsRectEmpty(APInfo^.ExtCanvas.PhysicalSize) then begin
        CanvasRect:=APInfo^.ExtCanvas.ClipRect;
        LPToDP(APInfo^.ExtCanvas.Handle,CanvasRect,2);
     end
     else begin
        CanvasRect:=APInfo^.ExtCanvas.PhysicalSize;
     end;
     if IsRectEmpty(CanvasRect) then begin
        Result:=False;
     end;
end;

procedure TWMSForm.GetViewRect;
begin
     if not GetCanvasRect then begin
        exit;
     end;

     APInfo^.MsgToDrawPos(CanvasRect.Left,CanvasRect.Bottom,ProjRefPoint);
     LLX:=ProjRefPoint.X / 100;
     LLY:=ProjRefPoint.Y / 100;

     APInfo^.MsgToDrawPos(CanvasRect.Right,CanvasRect.Top,ProjRefPoint);
     URX:=ProjRefPoint.X / 100;
     URY:=ProjRefPoint.Y / 100;

     if EditEPSG.Text = EPSG_WGS84 then begin
        XYToLongLat(LLX,LLY);
        XYToLongLat(URX,URY);
     end;
end;

function TWMSForm.ReadLanguage: AnsiString;
var
Ini: TIniFile;
begin
     Ini:=TIniFile.Create(OSInfo.WingisIniFileName);
     try
        Result:=Ini.ReadString('Settings','Language','044');
     finally
        Ini.Free;
     end;
end;

function TWMSForm.GetImage: Boolean;
const BUFFER_SIZE = 10000;
var
Inet,hURL: HInternet;
ReadSize: DWORD;
Buffer: Array[0..BUFFER_SIZE-1] of Char;
JpgFilename,PngFilename,TextFilename,HtmFilename: AnsiString;
CheckHeader: Boolean;
IsJpg: Boolean;
IsPng: Boolean;
IsHtm: Boolean;
IsText: Boolean;
begin
     Result:=False;
     if GetMapUrl = LastUrl then begin
        Result:=True;
        exit;
     end;
     JpgFilename:=OSInfo.TempDataDir + JPG_TEMP_FILE;
     DeleteFile(JpgFilename);
     PngFilename:=OSInfo.TempDataDir + PNG_TEMP_FILE;
     DeleteFile(PngFilename);
     HtmFilename:=OSInfo.TempDataDir + HTM_TEMP_FILE;
     DeleteFile(HtmFilename);
     TextFilename:=OSInfo.TempDataDir + TXT_TEMP_FILE;
     DeleteFile(TextFilename);
     CheckHeader:=True;
     IsJpg:=False;
     IsPng:=False;
     IsHtm:=False;
     IsText:=False;
     Inet:=InternetOpen(nil,INTERNET_OPEN_TYPE_PRECONFIG,nil,nil,0);
     if Inet <> nil then begin
        hURL:=InternetOpenURL(Inet,PChar(GetMapUrl),nil,0,INTERNET_FLAG_RELOAD,0);
        //ShellExecute(0,'open',PChar(GetMapUrl),nil,nil,SW_SHOW);
        if hURL <> nil then begin
           MemStream.Clear;
           MemStream.Position:=0;
           repeat
              InternetReadFile(hURL,@Buffer,Buffer_Size,ReadSize);
              if CheckHeader then begin
                 if ((Buffer[0] = Char($FF)) and (Buffer[1] = Char($D8))) then begin
                    IsJpg:=True;
                 end
                 else if ((Buffer[0] = Char($89)) and (Buffer[1] = Char('P')) and (Buffer[2] = Char('N'))and (Buffer[3] = Char('G'))) then begin
                    IsPng:=True;
                 end
                 else if (Buffer[0] = '<') then begin
                    IsHtm:=True;
                 end
                 else begin
                    IsText:=True;
                 end;
                 CheckHeader:=False;
              end;
              MemStream.WriteBuffer(Buffer,ReadSize);
           until ReadSize = 0;
           if IsJpg then begin
             MemStream.SaveToFile(JpgFilename);
             LastUrl:=GetMapUrl;
             ImageFormat:='image/jpeg';
             Result:=True;
           end
           else if IsPng then begin
             MemStream.SaveToFile(PngFilename);
             LastUrl:=GetMapUrl;
             ImageFormat:='image/png';
             Result:=True;
           end
           else if IsHtm then begin
             MemStream.SaveToFile(HtmFilename);
             OpenBrowser(HtmFilename);
             CheckBoxOnOff.Checked:=False;
           end
           else if IsText then begin
             MemStream.SaveToFile(TextFilename);
             OpenText(TextFilename);
             CheckBoxOnOff.Checked:=False;
           end;
           InternetCloseHandle(hURL);
        end;
        InternetCloseHandle(Inet);
      end;
end;

procedure TWMSForm.GetAllLegends;
var
Layername: AnsiString;
LayerList: TStringList;
HTML: TStringList;
i: Integer;
LegFilename,HTMLFilename: AnsiString;
begin
     if Trim(EditLayers.Text) = '' then begin
        exit;
     end;

     LayerList:=TStringList.Create;
     HTML:=TStringList.Create;
     HTML.Add('<html><body>');
     try
        LayerList.Text:=StringReplace(EditLayers.Text,',',#13#10,[rfReplaceAll]);
        for i:=0 to LayerList.Count-1 do begin
            Layername:=Trim(LayerList[i]);
            BuildGetLegUrl(Layername);
            GetLeg(Layername);
            HTML.Add('<p><hr><font face="Arial" size="3">' + Layername + '<br><hr>');
            LegFilename:=OSInfo.TempDataDir + Layername + '.png';
            if FileExists(LegFilename) then begin
               HTML.Add('<img src="' + LegFilename + '">');
            end;
            HTML.Add('</font></p>');
        end;
        HTML.Add('</body></html>');
        HTMLFilename:=OSInfo.TempDataDir + LEG_TEMP_FILE;
        DeleteFile(HTMLFilename);
        HTML.SaveToFile(HTMLFilename);
        if FileExists(HTMLFilename) then begin
           OpenBrowser(HTMLFilename);
        end;
     finally
        LayerList.Free;
        HTML.Free;
     end;
end;

procedure TWMSForm.BuildGetLegUrl(Layer: AnsiString);
var
s: AnsiString;
begin
     s:=GetLegUrlTemplate;
     EditHost.Text:=Trim(EditHost.Text);
     if Pos('?',EditHost.Text) = 0 then begin
        EditHost.Text:=EditHost.Text + '?';
     end;
     s:=StringReplace(s,'{host}',EditHost.Text,[]);
     s:=StringReplace(s,'{layer}',Layer,[]);
     GetLegUrl:=s;
end;

procedure TWMSForm.GetLeg(Layer: AnsiString);
const BUFFER_SIZE = 10000;
var
Inet,hURL: HInternet;
ReadSize: DWORD;
Buffer: Array[0..BUFFER_SIZE-1] of Char;
LegFilename: AnsiString;
CheckHeader: Boolean;
IsPng: Boolean;
begin
     LegFilename:=OSInfo.TempDataDir + Layer + '.png';
     DeleteFile(LegFilename);
     CheckHeader:=True;
     IsPng:=False;
     Inet:=InternetOpen(nil,INTERNET_OPEN_TYPE_PRECONFIG,nil,nil,0);
     if Inet <> nil then begin
        hURL:=InternetOpenURL(Inet,PChar(GetLegUrl),nil,0,INTERNET_FLAG_RELOAD,0);
        if hURL <> nil then begin
           MemStream.Clear;
           MemStream.Position:=0;
           repeat
              InternetReadFile(hURL,@Buffer,Buffer_Size,ReadSize);
              if CheckHeader then begin
                 if ((Buffer[0] = Char($89)) and (Buffer[1] = Char('P')) and (Buffer[2] = Char('N'))and (Buffer[3] = Char('G'))) then begin
                    IsPng:=True;
                 end;
                 CheckHeader:=False;
              end;
              MemStream.WriteBuffer(Buffer,ReadSize);
           until ReadSize = 0;
           if IsPng then begin
              MemStream.SaveToFile(LegFilename);
           end;
           InternetCloseHandle(hURL);
        end;
        InternetCloseHandle(Inet);
      end;
end;

procedure TWMSForm.ButtonCloseClick(Sender: TObject);
begin
     Close;
end;

procedure TWMSForm.ButtonApplyClick(Sender: TObject);
begin
     Redraw;
end;

procedure TWMSForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     Redraw;
end;

procedure TWMSForm.Redraw;
begin
     if WingisMainForm.ActualChild <> nil then begin
        WingisMainForm.ActualChild.Data^.PInfo^.RedrawScreen(False);
     end;
end;

procedure TWMSForm.LabelInfoClick(Sender: TObject);
begin
     if (Trim(EditHost.Text) = '') then begin
        exit;
     end;

     Screen.Cursor:=crHourGlass;
     try
        BuildGetCapUrl;
        GetCap;
     finally
        Screen.Cursor:=crDefault;
     end;
end;

procedure TWMSForm.LabelLegendClick(Sender: TObject);
begin
     if (Trim(EditHost.Text) = '') or (Trim(EditLayers.Text) = '') then begin
        exit;
     end;

     Screen.Cursor:=crHourGlass;
     try
        GetAllLegends;
     finally
        Screen.Cursor:=crDefault;
     end;
end;

procedure TWMSForm.ButtonOpenClick(Sender: TObject);
begin
     if OpenDialog.Execute then begin
        OpenWWCFile(OpenDialog.Filename);
     end;
end;

procedure TWMSForm.ButtonSaveClick(Sender: TObject);
var
Settings: TStringList;
begin
     if SaveDialog.Execute then begin
        Settings:=TStringList.Create;
        try
           Settings.Add('Host=' + Trim(EditHost.Text));
           Settings.Add('Layers=' + Trim(EditLayers.Text));
           Settings.Add('EPSG=' + Trim(EditEPSG.Text));
           Settings.Add('Format=' + Trim(ComboBoxFormat.Text));
           Settings.SaveToFile(SaveDialog.FileName);
        finally
           Settings.Free;
        end;
     end;
end;

procedure TWMSForm.OpenBrowser(AFilename: AnsiString);
var
StartupInfo: TStartupInfo;
ProcessInfo: TProcessInformation;
Reg: TRegistry;
StdBrowser: AnsiString;
CmdLine: AnsiString;

   function EnumWindowsProc(AWND: HWND; Param: DWORD):Boolean; stdcall;
   var
   PID: DWORD;
   begin
        Result:=True;
        PID:=0;
        GetWindowThreadProcessID(AWND,@PID);
        if PID = Param then begin
           Result:=False;
           SetForeGroundWindow(AWND);
        end;
 end;

begin
     StdBrowser:='';
     Reg:=TRegistry.Create;
     try
        Reg.RootKey:=HKEY_CLASSES_ROOT;
        if Reg.OpenKeyReadOnly('http\shell\open\command') then begin
           StdBrowser:=Reg.ReadString('');
        end;
     finally
        Reg.Free;
     end;

     CmdLine:=StdBrowser + ' "' + AFilename + '"';

     ZeroMemory(@ProcessInfo,Sizeof(ProcessInfo));
     ZeroMemory(@StartupInfo,Sizeof(StartupInfo));
     StartupInfo.cb:=Sizeof(StartupInfo);
     StartupInfo.wShowWindow:=SW_SHOW;
     StartupInfo.dwFlags:=STARTF_USESHOWWINDOW;

     if CreateProcess(nil,
                      PChar(CmdLine),
                      nil,
                      nil,
                      False,
                      NORMAL_PRIORITY_CLASS,
                      nil,
                      nil,
                      StartupInfo,
                      ProcessInfo) then begin

        WaitForInputIdle(ProcessInfo.hProcess,INFINITE);
        EnumWindows(@EnumWindowsProc,ProcessInfo.dwProcessId);
     end;
end;

procedure TWMSForm.OpenText(AFilename: AnsiString);
var
AStringList: TStringList;
s: AnsiString;
begin
     AStringList:=TStringList.Create;
     try
        AStringList.LoadFromFile(AFilename);
        s:=AStringList.Text;
        MessageBox(Handle,PChar(s),PChar(Caption),MB_ICONINFORMATION or MB_OK or MB_APPLMODAL);
     finally
        AStringList.Free;
     end;
end;

procedure TWMSForm.LabelEPSGLinkClick(Sender: TObject);
begin
     OpenBrowser('http://www.epsg-registry.org');
end;

procedure TWMSForm.EnumLayers;
var
s: AnsiString;
i,start,stop: Integer;
List: TStringList;
begin
     s:=EditLayers.Text;
     if Pos('-',s) <= 0 then begin
        exit;
     end;
     List:=TStringList.Create;
     try
        s:=StringReplace(s,'-',#13#10,[]);
        List.Text:=s;
        if List.Count = 2 then begin
           start:=StrToIntDef(List[0],-1);
           stop:=StrToIntDef(List[1],-1);
           if (start >= 0) and (stop >=1) and (stop > start) and (stop < 256) then begin
              s:='';
              for i:=start to stop do begin
                  s:=s + IntToStr(i);
                  if i < stop then begin
                     s:=s + ',';
                  end;
              end;
              EditLayers.Text:=s;
           end;
        end;
     finally
        List.Free;
     end;
end;

procedure TWMSForm.OpenWWCFile(AFilename: AnsiString);
var
Settings: TStringList;
s: AnsiString;
begin
        Settings:=TStringList.Create;
        try
           Settings.LoadFromFile(AFilename);
           EditHost.Text:=Settings.Values['Host'];
           EditLayers.Text:=Settings.Values['Layers'];
           EditEPSG.Text:=Settings.Values['EPSG'];
           s:=Settings.Values['Format'];
           if s <> '' then begin
              ComboBoxFormat.ItemIndex:=ComboBoxFormat.Items.IndexOf(s);
           end;
           if Trim(EditHost.Text) <> '' then begin
              CheckBoxOnOff.Checked:=True;
           end;
           EnableControls;
        finally
           Settings.Free;
        end;
end;

end.

