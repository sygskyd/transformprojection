{******************************************************************************+
  Module ListHndl
--------------------------------------------------------------------------------
  Author: Martin Forst
--------------------------------------------------------------------------------
  Contains the menu-handlers for the layer- and the combined layers/views/
  legends-lists
+******************************************************************************}
Unit ListHndl;

Interface

Uses AM_Proj,Classes,Controls,MenuFn,ProjHndl,StdCtrls,Windows,Am_main;

Type TListContents = (lcLayers,lcViews,lcLegends);

     TListLayersHandler = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;

     TListViewsHandler = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;

     TListLegendsHandler = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;

     TListLayersViewsLegendsHandler = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;

     TListLayersSelectHandler = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;

     TListMenuFunction = Class(TMenuFunction)
      Private
       Procedure   DoPaintColumn0(Control: TWinControl;Index:Integer;
                      Rect:TRect;State:TOwnerDrawState);
       Procedure   DoPaintColumn1(Control: TWinControl;Index:Integer;
                      Rect:TRect;State:TOwnerDrawState);
       Procedure   DoKeyPress(Sender:TObject;var Key:Char);               
      Protected
      Public
       Function    CreateToolbarControl(Owner:TComponent):TControl; override;
     end;

Procedure UpdateLayerLists(Project:PProj);

Procedure UpdateLayersViewsLegendsLists(Project:PProj);

Procedure UpdateWorkingLayer(Project:PProj);

Implementation

{$R *.mfn}

Uses AM_Def,AM_Layer,AM_ProjO,AM_Sight,ColumnList,CommonResources,Graphics,
     GrTools,NumTools,WCtrls,UserIntf
     {$IFNDEF WMLT}
     ,AM_DDE
     {$ENDIF}
     ,DDEDef;

{==============================================================================+
  TListLayersHandler
+==============================================================================}

Procedure TListLayersHandler.OnStart;
begin
  Project^.Registry.WriteInteger('\User\WinGISUI\DrawingTools\LayersViewsLegendsList\Contents',
      Integer(lcLayers));
  UpdateLayersViewsLegendsLists(Project);
end;

{==============================================================================+
  TListViewsHandler
+==============================================================================}

Procedure TListViewsHandler.OnStart;
begin
  Project^.Registry.WriteInteger('\User\WinGISUI\DrawingTools\LayersViewsLegendsList\Contents',
      Integer(lcViews));
  UpdateLayersViewsLegendsLists(Project);
end;

{==============================================================================+
  TListLegendsHandler
+==============================================================================}

Procedure TListLegendsHandler.OnStart;
begin
  Project^.Registry.WriteInteger('\User\WinGISUI\DrawingTools\LayersViewsLegendsList\Contents',
      Integer(lcLegends));
  UpdateLayersViewsLegendsLists(Project);
end;

{==============================================================================+
  TListLayersViewsLegendsHandler
+==============================================================================}

Procedure TListLayersViewsLegendsHandler.OnStart;
var ListContents   : TListContents;
    ALayer         : PLayer;
    TmpStr         : String;
begin
  ListContents:=TListContents(Project^.Registry.ReadInteger('\User\WinGISUI\DrawingTools\LayersViewsLegendsList\Contents'));
  case ListContents of
    lcLayers  : begin
        with MenuFunctions['ListLayersViewsLegends'] do ALayer:=Project^.Layers^.
            NameToLayer(Copy(Items[ItemIndex],2,Length(Items[ItemIndex])-1));
        if ALayer<>NIL then begin
          Project^.DeselectAll(True);
          Project^.Layers^.SetTopLayer(ALayer);
          {$IFNDEF WMLT}
            {$IFNDEF AXDLL} // <----------------- AXDLL
          if (DDEHandler.FSendLMSets) or (CoreConnected) then begin
            DDEHandler.SetDBSendStrings(DDECommLMI,NIL);
            if Project^.Layers^.TranspLayer then DDEHandler.SendString('[ALA][]')
            else begin
              if DDEHandler.FLayerInfo <> 2 then DDEHandler.SendString('[ALA]['+Project^.Layers^.TopLayer^.Text^+']')
              else begin
                Str(Project^.Layers^.TopLayer^.Index:10,TmpStr);
                DDEHandler.SendString('[ALA]['+TmpStr+']');
              end;
            end;
            DDEHandler.SetDBSendStrings(DDECommAll,NIL);
          end;
            {$ENDIF} // <----------------- AXDLL
          {$ENDIF}
        end;
      end;
    lcViews   : ProcSetSight(Project,PSight(Project^.Sights^.At(
        MenuFunctions['ListLayersViewsLegends'].ItemIndex)));
    lcLegends : begin
        // set current legend index
        Project^.Legends.CurrentLegendIndex:=MenuFunctions['ListLayersViewsLegends'].ItemIndex;
        // update curent legend view
        UserInterface.Update([uiLegends]);
      end;
  end;
end; 

{==============================================================================+
  TListLayersSelectHandler
+==============================================================================}

{******************************************************************************+
  Procedure TListLayersSelectHandler.OnStart
--------------------------------------------------------------------------------
  Called if the user selects a layer from the layer-combo-box in the toolbar.
  Sets the selected layer as the current top-layer.
+******************************************************************************}
Procedure TListLayersSelectHandler.OnStart;
var ALayer       : PLayer;
begin
  with MenuFunctions['ListLayers'] do ALayer:=Project^.Layers^.NameToLayer(Items[ItemIndex]);
  if ALayer<>NIL then begin
    Project^.DeselectAll(True);
    Project^.Layers^.SetTopLayer(ALayer);
  end;
end;

{==============================================================================+
  Functions
+==============================================================================}

{******************************************************************************+
  Procedure FillWithLayers
--------------------------------------------------------------------------------
  Fills the list of the menu-function with the currently visible layers.
+******************************************************************************}
Procedure FillWithLayers(MenuFunction:TMenuFunction;Project:PProj);
var Cnt          : Integer;
    ALayer       : PLayer;
begin
  with Project^,MenuFunction do begin
    Items.BeginUpdate;
    Items.Clear;
    for Cnt:=1 to Layers^.LData^.Count-1 do begin
      ALayer:=Layers^.LData^.At(Cnt);
      if not ALayer^.GetState(sf_LayerOff) then begin
        if ALayer^.GetState(sf_Fixed) then Items.Add('F'+PToStr(ALayer^.Text))
        else if ALayer^.GetState(sf_LayerOffAtGen) then Items.Add('U'+PToStr(ALayer^.Text))
        else Items.Add('L'+PToStr(ALayer^.Text));
        if ALayer=Layers^.TopLayer then ItemIndex:=Items.Count-1;
      end;
    end;
    Items.EndUpdate;
  end;                                                       
end;

Procedure FillWithViews(MenuFunction:TMenuFunction;Project:PProj);
var Cnt            : Integer;
    Sight          : PSight;
    Prefix         : Char;
begin
  with Project^,MenuFunction do begin
    Items.BeginUpdate;
    Items.Clear;
    for Cnt:=0 to Sights^.Count-1 do begin
      Sight:=Sights^.At(Cnt);

      if Cnt = Project^.Sights^.Default then begin
         if not RectEmpty(Sight^.ViewRect) and (Sight^.LayersData<>NIL) then Prefix:='Z'
         else if not RectEmpty(Sight^.ViewRect) then Prefix:='Y'
         else if Sight^.LayersData<>NIL then Prefix:='X'
         else Prefix:=' ';
      end
      else begin
         if not RectEmpty(Sight^.ViewRect) and (Sight^.LayersData<>NIL) then Prefix:='B'
         else if not RectEmpty(Sight^.ViewRect) then Prefix:='R'
         else if Sight^.LayersData<>NIL then Prefix:='L'
         else Prefix:=' ';
      end;

      Items.Add(Prefix+PToStr(Sight^.Name));
    end;  
    Items.EndUpdate;
  end;
end;

Procedure FillWithLegends(MenuFunction:TMenuFunction;Project:PProj);
var Cnt            : Integer;
begin
  with Project^,MenuFunction do begin
    Items.BeginUpdate;
    try
      Items.Clear;
      for Cnt:=0 to Project^.Legends.Count-1 do
          Items.Add('G'+Project^.Legends.Legends[Cnt].Name);
      ItemIndex:=Project^.Legends.CurrentLegendIndex;
    finally
      Items.EndUpdate;
    end;  
  end;
end;

{******************************************************************************+
  Procedure UpdateLayerLists
--------------------------------------------------------------------------------
  Updates the layer-lists in the status-bars. 
+******************************************************************************}
Procedure UpdateLayerLists(Project:PProj);
begin
  if Project=NIL then MenuFunctions['ListLayers'].Items.Clear
  else FillWithLayers(MenuFunctions['ListLayers'],Project);
end;

{******************************************************************************+
  Procedure UpdateWorkingLayer
--------------------------------------------------------------------------------
  Called if the current working-layer has changed. Updates the currently
  selected layer of the layer-lists and the combined lists.
+******************************************************************************}
Procedure UpdateWorkingLayer(Project:PProj);
var ListContents   : TListContents;
    iOpenWindows: integer;
    IsAttributeTableOpen, IsMonitoringOpen: boolean;
//    ALayer: Pointer;
begin

   if Project <> nil then
   begin

{brovak}
{$IFNDEF WMLT}
{$IFNDEF AXDLL}
      if WinGISMainForm.WeAreUsingTheIDB[Project] then
      begin
//         ALayer := Project^.Layers^.IndexToLayer(MenuFunctions['ListLayers'].ItemIndex);
         iOpenWindows := WinGISMainForm.IDB_Man.IsOpenedWindow(PProj(WinGisMainForm.ActualProj));
         IsAttributeTableOpen := false;
         IsAttributeTableOpen := (iOpenWindows mod 2) > 0;
         IsMonitoringOpen := false;
         IsMonitoringOpen := (iOpenWindows div 2) > 0;
//         WinGISMainForm.IDB_Man.CloseAllWindowsOfAProject(PProj(WinGisMainForm.ActualProj));
      end;
{$ENDIF}
{$ENDIF}
{brovak}


      MenuFunctions['ListLayers'].ItemIndex := Project^.Layers^.IndexOfLayer(Project^.Layers^.TopLayer,TRUE);
      ListContents := TListContents(Project^.Registry.ReadInteger('\User\WinGISUI\DrawingTools\LayersViewsLegendsList\Contents'));
      if ListContents = lcLayers then
         MenuFunctions['ListLayersViewsLegends'].ItemIndex := Project^.Layers^.IndexOfLayer(Project^.Layers^.TopLayer,TRUE);

{brovak}
{$IFNDEF WMLT}
{$IFNDEF AXDLL}
      if WinGISMainForm.WeAreUsingTheIDB[Project]=true then
      begin
            if IsMonitoringOpen then
            begin
               if PProj(WinGisMainForm.ActualProj).Layers.SelLayer.Data.GetCount > 0 then
                  WinGISMainForm.IDB_Man.ShowMonitoringTable (false)
               else
                  WinGISMainForm.IDB_Man.CloseAllWindowsOfAProject(PProj(WinGisMainForm.ActualProj));
            end;

            if IsAttributeTableOpen then
               WinGISMainForm.IDB_Man.ShowDataTable;

//        MenuFunctions['IDB_ShowLayerTable'].Execute;
// -- Cadmensky
      end;
{$ENDIF}
{$ENDIF}
{brovak}

   end;
end;

{******************************************************************************+
  Procedure UpdateLayersViewsLegendsList
--------------------------------------------------------------------------------
  Updates the combined layers/views/legends list with the appropriate
  information.
+******************************************************************************}

Procedure UpdateLayersViewsLegendsLists(Project:PProj);
var ListContents   : TListContents;
begin
  if (Project=NIL) or (Project^.SymbolMode<>sym_Project) then MenuFunctions['ListLayersViewsLegends'].Items.Clear
  else begin
    ListContents:=TListContents(Project^.Registry.ReadInteger('\User\WinGISUI\DrawingTools\LayersViewsLegendsList\Contents'));
    case ListContents of
      lcLayers  : FillWithLayers(MenuFunctions['ListLayersViewsLegends'],Project);
      lcViews   : FillWithViews(MenuFunctions['ListLayersViewsLegends'],Project);
      lcLegends : FillWithLegends(MenuFunctions['ListLayersViewsLegends'],Project);
    end;
  end;  
end;

{==============================================================================+
  TListMenuFunction
+==============================================================================}

Function TListMenuFunction.CreateToolbarControl(Owner:TComponent):TControl;
var Column         : TColumn;
begin
  Result:=TColumnComboBox.Create(Owner);
  with Result as TColumnComboBox do begin
    Width:=200;
    OnChange:=DoListChange;
    OnClick:=DoListClick;
    Column:=NewColumn(Columns,22);
    Column.OnPaint:=DoPaintColumn0;
    Column:=NewColumn(Columns,50,50,TRUE);
    Column.OnPaint:=DoPaintColumn1;
    OnKeyPress:=DoKeyPress;
  end;
end;

procedure TListMenuFunction.DoKeyPress(Sender:TObject;var Key:Char);
var Cnt            : Integer;
begin
  with Sender as TColumnComboBox do begin
    Cnt:=(ItemIndex+1) Mod Items.Count;
    while Cnt<>ItemIndex do begin
      if (Length(Items[Cnt])>2) and (UpCase(Items[Cnt][2])=UpCase(Key)) then begin
        ItemIndex:=Cnt;
        DoListClick(Sender);
        break;
      end;
      Cnt:=(Cnt+1) Mod Items.Count;
    end;
    Key:=#0;
  end;
end;

Procedure TListMenuFunction.DoPaintColumn0(Control: TWinControl;Index:Integer;
    Rect:TRect;State:TOwnerDrawState);
begin
  // draw the bitmap
  with Control as TColumnComboBox do case Items[Index][1] of
    'Z',
    'B' : CommonListBitmaps.Draw(Canvas,Rect.Left+4,CenterHeightInRect(16,Rect),clbLayerZoom);
    'F',
    'U',
    'X',
    'L' : CommonListBitmaps.Draw(Canvas,Rect.Left+4,CenterHeightInRect(16,Rect),clbLayerGray);
    'Y',
    'R' : CommonListBitmaps.Draw(Canvas,Rect.Left+4,CenterHeightInRect(16,Rect),clbZoom);
    'G' : CommonListBitmaps.Draw(Canvas,Rect.Left+4,CenterHeightInRect(16,Rect),clbLegend);
  end;
end;

Procedure TListMenuFunction.DoPaintColumn1(Control: TWinControl;Index:Integer;
    Rect:TRect;State:TOwnerDrawState);
begin
  with Control as TColumnComboBox do begin
    // draw the text
    if Items[Index][1]='F' then Canvas.Font.Color:=clRed
    else if Items[Index][1]='U' then Canvas.Font.Color:=clGray;

    if (Items[Index][1]='X') or (Items[Index][1]='Y') or (Items[Index][1]='Z') then Canvas.Font.Style:=[fsBold]
    else Canvas.Font.Style:=[];

    TextRectEx(Canvas,Rect,Copy(Items[Index],2,Length(Items[Index])-1),clWindow,State);
  end;
end;

{==============================================================================+
  Initialization- and termination-code
+==============================================================================}

Initialization
  MenuFunctions.RegisterAlternativeHandler('ListLayersViewsLegends',
      TListMenuFunction);
  MenuFunctions.RegisterFromResource(HInstance,'ListHndl','ListHndl');
  TListLayersHandler.Registrate('ListShowLayers');
  TListViewsHandler.Registrate('ListShowViews');
  TListLegendsHandler.Registrate('ListShowLegends');
  TListLayersSelectHandler.Registrate('ListLayers');
  TListLayersViewsLegendsHandler.Registrate('ListLayersViewsLegends');

end.
