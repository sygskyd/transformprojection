Unit ConvertHndl;

Interface

Uses MenuFn,ProjHndl;

Type TConvertToCurveHandler = Class(TProjMenuHandler)
      Public
       Procedure   OnStart; override;
     end;

     TConvertToPolylineHandler = Class(TProjMenuHandler)
      Public
       Procedure   OnStart; override;
     end;

     TConvertToPolygonHandler = Class(TProjMenuHandler)
      Public
       Procedure   OnStart; override;
     end;

     TConvertToLinesHandler = Class(TProjMenuHandler)
      Public
       Procedure   OnStart; override;
     end;

     TConvertToIslandHandler = Class(TProjMenuHandler)
      Public
       Procedure   OnStart; override;
     end;

Implementation

{$R *.MFN}

Uses AM_CPoly,AM_ProjO,Dialogs,MultiLng;

Procedure TConvertToCurveHandler.OnStart;
begin
  {$IFNDEF WMLT}
  ProcToCurve(Project);
  {$ENDIF}
end;

Procedure TConvertToPolylineHandler.OnStart;
begin
//++ Glukhov Bug#192 Build#161 29.05.01
//  MessageDialog('Sorry, not yet implemented.',mtInformation,[mbOK],0);
  {$IFNDEF WMLT}
  ProcConvToPLine( Project );
  {$ENDIF}
//-- Glukhov Bug#192 Build#161 29.05.01
end;

Procedure TConvertToPolygonHandler.OnStart;
begin
//++ Glukhov Bug#192 Build#161 10.10.01
//  MessageDialog('Sorry, not yet implemented.',mtInformation,[mbOK],0);
  {$IFNDEF WMLT}
  ProcConvToPolygon( Project );
  {$ENDIF}
//-- Glukhov Bug#192 Build#161 29.05.01
end;

Procedure TConvertToLinesHandler.OnStart;
begin
  MessageDialog('Sorry, not yet implemented.',mtInformation,[mbOK],0);
end;
                                                  
{ TConvertToIslandHandler }

procedure TConvertToIslandHandler.OnStart;
begin
  ProcCreateIslandArea(Project);
end;

Initialization
  MenuFunctions.RegisterFromResource(HInstance,'ConvertHndl','ConvertHndl');
  TConvertToCurveHandler.Registrate('ConvertToCurve');
  TConvertToPolylineHandler.Registrate('ConvertToPolyline');
  TConvertToPolygonHandler.Registrate('ConvertToPolygon');
  TConvertToLinesHandler.Registrate('ConvertToLines');
  TConvertToIslandHandler.Registrate('ConvertToIsland');

end.
 