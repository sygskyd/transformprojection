unit Img_Info;

interface

uses WinTypes, WinProcs, {WinDOS,} Classes, TrLib32;

const
  err_OK: Word = 0;
  err_WrongFormat: Word = 1;
  err_ReadErr: Word = 2;
  err_LowMem: Word = 3;
  {01-004-040399 - Begin}
  ifNone: LongInt = $0;
  ifLoadable: LongInt = $1;
  ifVector: LongInt = $2;
  ifDLLDraw: LongInt = $4;
  ifIntLoad: LongInt = $8;
  ifCoded: LongInt = $10;
  ifScaleSelf: LongInt = $20;
  {01-004-040399 - End}

type
  TVMSITechImage = (vtiUnknown, vtiNo, vtiYes);

  ImgInfo = class(TObject)
  private
    sFileName: AnsiString;
    iPage: LongInt;
  protected
    function getWidth: LongInt; virtual; abstract;
    function getHeight: LongInt; virtual; abstract;
    function getWidthMM: Double; virtual; abstract;
    function getHeightMM: Double; virtual; abstract;
    function getBitCount: Word; virtual; abstract;
    function getBounds: TRect; virtual;
    function getSize: LongInt; virtual; abstract;
    function getLineLength: LongInt; virtual; abstract;
    function getBitmapInfo: PBITMAPINFO; virtual; abstract;
    function getLastAlloc: Word; virtual; abstract;
    {01-004-040399 - Begin}
    procedure setPage(thePage: LongInt);
    {01-004-040399 - End}
  public
    constructor Create(FileName: string; var Status: Word);
    property Width: LongInt read getWidth;
    property WidthMM: Double read getWidthMM;
    property Height: LongInt read getHeight;
    property HeightMM: Double read getHeightMM;
    property BitCount: Word read getBitCount;
    property Bounds: TRect read getBounds;
    property Size: LongInt read getSize;
    property LineLength: LongInt read getLineLength;
    property BitmapInfo: PBITMAPINFO read getBitmapInfo;
    property LastAlloc: Word read getLastAlloc;
    {01-004-040399 - Begin}
    property Page: LongInt write setPage;
    {01-004-040399 - End}
    property FileName: AnsiString read sFileName;
    function LoadPartialImage(UpperLeftX, UpperLeftY, Width, Height, Factor: LongInt): LPVOID; virtual; abstract;
    function LoadImage(Factor, Width, Height: LongInt): LPVOID; virtual; abstract;
    function Rotate(outFileName: string; dAngle: double; HWindow: Hwnd): LongInt; virtual; abstract;
    function Stretch(outFileName: AnsiString; Width, Height: Integer): Boolean; virtual; abstract;
    function LastError: LongInt; virtual; abstract;
    {01-004-040399 - Begin}
    function Draw(hDC: HDC; lpDestRect, lpSrcRect: PRECT; LoadFactor: LongInt;
      dwROP: DWORD): LongInt; virtual; abstract;
    function IsVector: Boolean; virtual; abstract;
    function IsVMSI: Boolean; virtual; abstract;
    function IsVMSIPlus: Boolean; virtual; abstract;
    function IsScaleSelf: Boolean; virtual; abstract;
    function DrawExternal: Boolean; virtual; abstract;
    function GetPageCount(sFileName: LPSTR): LongInt; virtual; abstract;
    {01-004-040399 - End}
    {01-007-020599 - Begin}
    function Print(hDC: HDC; DestX, DestY, DestW, DestH: Integer;
      SrcX, SrcY, SrcW, SrcH: Integer;
      dwROP: DWORD): LongInt; virtual; abstract;
    {01-007-020599 - End}
  end;

  TrLibInfo = class(ImgInfo)
  private
    BMPInfo: PBITMAPINFO;
    lLineLength: LongInt;
    LastAlloc: Word;
    FPageCount: Integer;
    FIsVMSI: Boolean;
    FVMSITech: TVMSITechImage;
    uID: LongInt;
    {01-004-040399 - Begin}
    dwStatus: DWORD;
    {01-004-040399 - End}
  public
    //       Constructor Create(FileName : String; var Status : Word; ID : Integer);
    constructor Create(FileName: AnsiString; var Status: Word; ID: Integer = -1);
    destructor Destroy; override;

    function LoadPartialImage(UpperLeftX, UpperLeftY, Width, Height, Factor: LongInt): LPVOID; override;
    function LoadImage(Factor, Width, Height: LongInt): LPVOID; override;
    function LastError: LongInt; override;
    function Rotate(soutFileName: string; dAngle: double; HWindow: Hwnd): LongInt; override;
    function Stretch(outFileName: AnsiString; Width, Height: Integer): Boolean; override;

    {01-004-040399 - Begin}
    function Draw(hDC: HDC; lpDestRect, lpSrcRect: PRECT; LoadFactor: LongInt;
      dwROP: DWORD): LongInt; override;
    function IsVector: Boolean; override;
    function IsVMSI: Boolean; override; //++Sygsky
    function IsVMSIPlus: Boolean; override; //++Sygsky
    function IsScaleSelf: Boolean; override;
    function DrawExternal: Boolean; override;
    function GetPageCount(sFileName: LPSTR): LongInt; override;
    function FormatIsSupported: Boolean;
    {01-004-040399 - End}

    {01-007-020599 - Begin}
    function Print(hDC: HDC; DestX, DestY, DestW, DestH: Integer;
      SrcX, SrcY, SrcW, SrcH: Integer;
      dwROP: DWORD): LongInt; override;
    {01-007-020599 - End}
    function ImageSize: DWORD;
    function ImageVolume: DWORD;
    property PageCount: Integer read FPageCount;

  protected
    function getWidth: LongInt; override;
    function getHeight: LongInt; override;
    function getWidthMM: Double; override;
    function getHeightMM: Double; override;
    function getBitCount: Word; override;
    function getSize: LongInt; override;
    function getLineLength: LongInt; override;
    function getBitmapInfo: PBITMAPINFO; override;
    function getLastAlloc: Word; override;
  end;

implementation

uses Math, UserUtils;
{******************************************************************************}
{* ImgInfo: Basis klasse f. Information                                        }
{******************************************************************************}

constructor ImgInfo.Create(FileName: string; var Status: Word);
begin
  inherited Create;
  sFileName := FileName + #0;
  Status := err_OK;
end;

{01-004-040399 - Begin}

procedure ImgInfo.setPage(thePage: LongInt);
begin
  iPage := thePage;
end;
{01-004-040399 - End}

{******************************************************************************}
{* TrLibInfo: Information f. TrLib32 Implementierung                           }
{******************************************************************************}

constructor TrLibInfo.Create(FileName: AnsiString; var Status: Word; ID: Integer = -1);
begin
  inherited Create(FileName, Status);
  {01-005-100399 - Begin}
  uID := ID;
  {01-005-100399 - End}
  try
    BMPInfo := TrLib.GetHeader(PChar(sFileName), FPageCount, FIsVMSI);
    if (BMPInfo = nil) then
    begin
      if (LastError = ec_NOSUPPORT) then
        Status := err_WrongFormat
      else
        Status := err_ReadErr;
      Exit;
    end;
  except
    Status := err_WrongFormat;
    Exit;
  end;
  Status := 0; // ++ Sygsky BUG#OmegaPhoto
  lLineLength := (((BMPInfo^.bmiHeader.biWidth * BMPInfo^.bmiHeader.biBitCount) + 31) div 32) * 4;
  {01-004-040399 - Begin}
  dwStatus := TrLib.GetImgFlags(PChar(sFileName), uID);
  {01-004-040399 - End}
end;

destructor TrLibInfo.Destroy;
begin
  if (BMPInfo <> nil) then
    VirtualFree(BMPInfo, 0, MEM_RELEASE);
end;

function TrLibInfo.LoadPartialImage(UpperLeftX, UpperLeftY, Width, Height, Factor: LongInt): LPVOID;
var
  sRect: TRECT;
  sDest: TPOINT;
  sDestRect: TRECT;
begin
  LoadPartialImage := nil;
  {01-004-040399 - Begin}
  if (IsVector) then
    exit;
  {01-004-040399 - End}
  sRect.left := UpperLeftX;
  sRect.top := UpperLeftY;
  sRect.right := (UpperLeftX + Width * Factor) - 1;
  sRect.bottom := (UpperLeftY + Height * Factor) - 1;

  sDest.X := Width;
  sDest.Y := Height;

  LoadPartialImage := TrLib.LoadImg(sRect, sDest, Factor, PChar(sFileName), uID, @sDestRect);
end;

function TrLibInfo.LoadImage(Factor, Width, Height: LongInt): LPVOID;
begin
  LoadImage := LoadPartialImage(0, 0, Width, Height, Factor);
end;

function TrLibInfo.Rotate(soutFileName: string; dAngle: double; HWindow: Hwnd): LongInt;
begin
  Rotate := 0;
  if (IsVector) then
    exit;
  soutFileName := soutFileName + #0;
  Rotate := TrLib.Rotate(PChar(sFilename), @soutFilename[1], dAngle, uID, Hwindow);
end;

{++Sygsky: for Yura specially. Ask him why :-}

function TrLibInfo.Stretch(outFileName: AnsiString; Width, Height: Integer): Boolean;
begin
  REsult := False;
  if IsVector then
    Exit;
  Result := TrLib.Stretch(sFileName, outFileName, Width, Height);
end;

function TrLibInfo.LastError: LongInt;
begin
  LastError := TrLib.GetLastError(uID);
end;

function TrLibInfo.getWidth: LongInt;
begin
  getWidth := BMPInfo^.bmiHeader.biWidth;
end;

function TrLibInfo.getHeight: LongInt;
begin
  getHeight := BMPInfo^.bmiHeader.biHeight;
end;

function TrLibInfo.getBitCount: Word;
begin
  getBitCount := BMPInfo^.bmiHeader.biBitCount;
end;

function TrLibInfo.getSize: LongInt;
begin
  getSize := BMPInfo^.bmiHeader.biSizeImage;
end;

function TrLibInfo.getLineLength: LongInt;
begin
  getLineLength := lLineLength;
end;

function TrLibInfo.getBitmapInfo: PBITMAPINFO;
var
  Data: PBITMAPINFO;
  Size: LongInt;
begin
  getBitmapInfo := nil;
  if BitCount < 24 then
    Size := sizeof(TBITMAPINFOHEADER) + ((1 shl BitCount) * sizeof(TRGBQuad))
  else
    Size := sizeof(TBITMAPINFOHEADER);
  GetMem(Data, Size);
  if (Data = nil) then
    exit;
  Move(BMPInfo^, Data^, Size);
  getBitmapInfo := Data;
  LastAlloc := Size;
end;

function TrLibInfo.getLastAlloc: Word;
begin
  getLastAlloc := LastAlloc;
end;

{01-004-040399 - Begin}

function TrLibInfo.Draw(hDC: HDC; lpDestRect, lpSrcRect: PRECT; LoadFactor: LongInt;
  dwROP: DWORD): LongInt;
begin
  Draw := 0;
  if (IsVector) then
    LoadFactor := 1;
  if (DrawExternal) then
    Draw := TrLib.DrawImage(PChar(sFileName), hDC, lpDestRect, lpSrcRect, LoadFactor, dwROP, uID);
end;

function TrLibInfo.IsVector: Boolean;
begin
  Result := ((dwStatus and ifVector) > 0);
end;

function TrLibInfo.IsVMSI: Boolean;
begin
  Result := FIsVMSI <> False;
end;

function TrLibInfo.IsVMSIPlus: Boolean;
begin
  case FVMSITech of
    vtiUnknown: // Detect it first time
      begin
        if FileIsVMSIPlus(sFileName) then
          FVMSITech := vtiYes
        else
          FVMSITech := vtiNo;
      end;
    vtiNo..vtiYes: ;
  end;
  Result := FVMSITech = vtiYes;
end;

function TrLibInfo.IsScaleSelf: Boolean;
begin
  Result := dwStatus and ifScaleSelf <> 0;
end;

function TrLibInfo.DrawExternal: Boolean;
begin
  Result := ((dwStatus and ifDLLDraw) > 0);
end;

function TrLibInfo.GetPageCount(sFileName: LPSTR): LongInt;
begin
  GetPageCount := TrLib.GetPageCount(PChar(sFileName), uID);
end;

{01-004-040399 - End}

function ImgInfo.getBounds: TRect;
begin
  Result := Rect(0, 0, Width - 1, Height - 1);
end;

function TrLibInfo.getHeightMM: Double;
begin
  if BMPInfo.bmiHeader.biXPelsPerMeter <= 0 then
    Result := 0
      {++ Moskaliov BUG#268 BUILD#106 22.03.00}
  //  else Result:=BMPInfo.bmiHeader.biWidth*1000/BMPInfo.bmiHeader.biXPelsPerMeter;
  else
    Result := BMPInfo.bmiHeader.biHeight * 1000 / BMPInfo.bmiHeader.biYPelsPerMeter;
  {-- Moskaliov BUG#268 BUILD#106 22.03.00}
end;

function TrLibInfo.getWidthMM: Double;
begin
  if BMPInfo.bmiHeader.biYPelsPerMeter <= 0 then
    Result := 0
      {++ Moskaliov BUG#268 BUILD#106 22.03.00}
  //  else Result:=BMPInfo.bmiHeader.biHeight*1000/BMPInfo.bmiHeader.biYPelsPerMeter;
  else
    Result := BMPInfo.bmiHeader.biWidth * 1000 / BMPInfo.bmiHeader.biXPelsPerMeter;
  {-- Moskaliov BUG#268 BUILD#106 22.03.00}
end;

{01-007-020599 - Begin}

function TrLibInfo.Print(hDC: HDC; DestX, DestY, DestW, DestH: Integer;
  SrcX, SrcY, SrcW, SrcH: Integer;
  dwROP: DWORD): LongInt;
var
  SrcRect, DestRect: TRect;
  LoadFactor: LongInt;
  iSign: Integer;
begin
  // SrcRect if rectangle of image to print of!
  SrcRect := Classes.Bounds(SrcX, SrcY, SrcW - Sign(SrcW), SrcH - Sign(SrcH));
  // DestRect is rectangle in device DC to print to
  DestRect := Classes.Bounds(DestX, DestY, DestW - Sign(DestW), DestH - Sign(DestH));
  // We need load factor to use PRI oadvantages

  //++ Sygsky: big images print problem
  LoadFactor := Max(Min(ceil(Abs(SrcW / DestW)), ceil(Abs(SrcH / DestH))), 1);
  Result := Integer(TrLib.PrintImage(PChar(sFileName), hDC, @DestRect, @SrcRect, LoadFactor, dwROP, uID) = 0);
  { We have to return 0 in success }
end;
{01-007-020599 - End}

function TrLibInfo.FormatIsSupported: Boolean;
begin
  Result := BMPInfo <> nil;
end;

function TrLibInfo.ImageSize: DWORD;
begin
  Result := getLinelength * getHeight;
end;

function TrLibInfo.ImageVolume: DWORD;
begin
  Result := ImageSize * ((Self.getBitCount + 7) div 8);
end;

end.

