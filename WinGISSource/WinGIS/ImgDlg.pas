{****************************************************************************}
{ Unit ImgDlg                                                                }
{----------------------------------------------------------------------------}
{ Bilder Lade sowie Speicherdialog                                           }
{----------------------------------------------------------------------------}
{ Modifikationen                                                             }
{  18.02.1998 Bramberger Michael 01-180298-002                               }
{              In OpenImageFile C-String in Pascal-String unwandeln          }
{  08-DEC-1999 Sigolaeff Victor: totally rewritten to adopt new TrLib32.     }
{              Added ExtractFilterExt function                               }
{****************************************************************************}
unit ImgDlg;

interface
{$IFNDEF AXDLL} // <----------------- AXDLL
uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, {Graphics,} Controls,
  Forms, Dialogs, StdCtrls, CommDlg, {AM_Ini,} UserIntf, WCtrls, WOpenPicDlg,
  TabFiles;

function OpenImageFile(var Dlg: TWOpenPictureDialog): Boolean; // Syg
function SaveImageFile(var newFileName: AnsiString; sFilters: AnsiString = ''): Boolean;

{$ENDIF} // <----------------- AXDLL

implementation

{$IFNDEF AXDLL} // <----------------- AXDLL

uses AM_Main, TrLib32, MultiLng, AM_Def, UserUtils;

{---------------------------------------------------------}
var
  TransformationOn: Boolean = FALSE; { Previous state of check box in open dialog }

function OpenImageFile(var Dlg: TWOpenPictureDialog): Boolean; // Syg
var
  Filters: AnsiString;
begin
  OpenImageFile := True;
  Filters := TrLib.ImageGetImpFormats;
  if (Filters = EmptyStr) then
  begin
    SmartMsgBox(0, 'Main', 4651, 4649, MB_OK + MB_ICONERROR, []);
    Result := False;
    Exit;
  end;
  try
    Filters := Filters + TabFilter; 
    Dlg.Filter := Filters;
    if TransformationOn then
      Dlg.SetOmegaOn; { Restore prev. state }
    if not Dlg.Execute then
      Result := False;
  finally
    TransformationOn := Dlg.GetOmegaOn; { Store actual state }
  end;
end;

function SaveImageFile(var newFileName: AnsiString; sFilters: AnsiString = ''): Boolean;
var
  Filters: LPSTR;
  Ext: AnsiString;
  EIndex: Integer;
  Opts: set of TOpenOption;
begin
  Result := False;
  if sFilters <> '' then
     Filters := PChar (sFilters)
  else
     Filters := TrLib.EnumExpFormats;
  if Filters = nil then
  begin
    SmartMsgBox(0, 'Main', 4650, 4649, MB_OK + MB_ICONERROR, []);
    Exit;
  end;
  try
    with WinGISMainForm.ImgSaveDlg do
    begin
      Filter := Filters;
      { Is it possible to store with the same ext as source file has? }
      EIndex := GetFilterIndex(Filters, NewFileName);
      if EIndex > 0 then { Yesss }
        FilterIndex := EIndex; { Set old ext as default one for user }
      DefaultExt := ExtractFilterExt(Filters, EIndex);
      Opts := Options; { Save old options }
      Options := [ofOverwritePrompt, ofHideReadOnly, ofShowHelp, ofPathMustExist,
        ofNoReadOnlyReturn, ofEnableSizing];
      try
        while True do
          if Execute then
            break
          else
            Exit;
      finally
        Options := Opts; { Restore old options }
      end;
      NewFileName := FileName;
      Ext := ExtractFileExt(FileName);
      if Ext = '' then { User not pointed wanted extension, use default one }
      begin
        Ext := ExtractFilterExt(Filters, FilterIndex);
        NewFileName := Concat(FileName, '.' + Ext);
      end;
      Result := True;
    end;
  finally
    VirtualFree(Filters, 0, MEM_RELEASE);
  end;
end;

{$ENDIF} // <----------------- AXDLL

end.

