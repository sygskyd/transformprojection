{******************************************************************************+
  Unit TooltipShow
--------------------------------------------------------------------------------
  Author : Raimund Leitner
--------------------------------------------------------------------------------
  implements the small window for displaying floating texts. Also gives
  the possibility to cahnge the floating text and show all floating
  texts on the active layer.

    Changed by Brovak , Progis Moscow, Russia 3.10.2000
{******************************************************************************}

unit TooltipShow;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Menus, MultiLng;

type
  TTooltipShowForm = class(TForm)
    Tooltip: TLabel;
    Bevel1: TBevel;
    PopupMenu1: TPopupMenu;
    EditFloatingText1: TMenuItem;
    ShowAllFloatingTexts1: TMenuItem;
    ShowselectedFloatingTexts1: TMenuItem;
    Tooltips: TMlgSection;
    EditFloatingText3: TMenuItem;
    N1: TMenuItem;
    procedure TooltipMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure TooltipClick(Sender: TObject);
    procedure EditFloatingText1Click(Sender: TObject);
    procedure ShowAllFloatingTexts1Click(Sender: TObject);
    procedure ShowselectedFloatingTexts1Click(Sender: TObject);
    procedure EditFloatingText3Click(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
  private
    { Private-Deklarationen}
    Function  GetTooltipText:String;
  public
    { Public-Deklarationen}
    // re-read strings from string-file
    Procedure SetPopupMenuStrings;
    // set tooltip and arrange window apropriate
    Procedure SetTooltipText(aText:String);
    // move from to x,y
    Procedure MoveTo(x:Integer;y:Integer);
    // direct access via property
    property  TooltipText:String read GetTooltipText write SetTooltipText;

  end;

var
  TooltipShowForm: TTooltipShowForm;

implementation

{$R *.DFM}

Uses Tooltips;

Procedure TTooltipShowForm.SetTooltipText(aText:String);
begin
  Tooltip.Caption:=aText;
  Bevel1.Width:=Tooltip.Width+5;
  Bevel1.Height:=Tooltip.Height+5;
  Width:=Bevel1.Width-1;
  Height:=Bevel1.Height-1;
end;

Function TTooltipShowForm.GetTooltipText:String;
begin
  Result:=Tooltip.Caption;
end;

Procedure TTooltipShowForm.MoveTo(x:Integer;y:Integer);
begin
  // show form 2 pixel above and right to mouse position
  Top:=y-Height-2;
  Left:=x+2;
end;

procedure TTooltipShowForm.TooltipMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  // if mouse cursor over tooltip move tooltip aside...
{  Top:=Top+Y-Height-2;
  Left:=Left+X+2;        for testing}
end;

procedure TTooltipShowForm.TooltipClick(Sender: TObject);
begin
  TooltipThread.FixFloatingText;
end;

procedure TTooltipShowForm.EditFloatingText1Click(Sender: TObject);
begin
  TooltipThread.ChangeCurrentFloatingText;
end;

  {++ Changed and added by Brovak Bug 106 BUILD 135}
procedure TTooltipShowForm.ShowAllFloatingTexts1Click(Sender: TObject);
begin
  TooltipThread.ShowAllFlag:=not TooltipThread.ShowAllFlag;
  TooltipThread.ShowAllFloatingTexts(TooltipThread.ShowAllFlag);
end;

procedure TTooltipShowForm.ShowselectedFloatingTexts1Click(
  Sender: TObject);
begin
   TooltipThread.ShowSelectedFlag:=not TooltipThread.ShowSelectedFlag;
   TooltipThread.ShowSelectedFloatingTexts(TooltipThread.ShowSelectedFlag);
end;

procedure TTooltipShowForm.EditFloatingText3Click(Sender: TObject);
begin
  TooltipThread.ChangeCurrentFloatingTextForSelected;
end;
 {--brovak}

procedure TTooltipShowForm.SetPopupMenuStrings;
begin
  EditFloatingText1.Caption:=MlgStringList['Tooltips',2];
   {++Brovak Bug 106 BUILD 135}
  EditFloatingText3.Caption:=MlgStringList['Tooltips',7]; //Edit Selected
   {--Brovak}
  ShowAllFloatingTexts1.Caption:=MlgStringList['Tooltips',4];
  ShowselectedFloatingTexts1.Caption:=MlgStringList['Tooltips',5];
end;

 {++Brovak Bug 106 BUILD 135}
procedure TTooltipShowForm.PopupMenu1Popup(Sender: TObject);
begin
 SetPopupMenuStrings;

 EditFloatingText3.Enabled:= TooltipThread.IsExistSelected;
 ShowselectedFloatingTexts1.Enabled:= TooltipThread.IsExistSelected;
end;
 {--Brovak}
end.
