{******************************************************************************+
  Unit SymbolFillDialog
--------------------------------------------------------------------------------
  Autor: Martin Forst
--------------------------------------------------------------------------------
  Dialog to edit a symboll-fill.
--------------------------------------------------------------------------------
  Release: 1
+******************************************************************************}

Unit SymbolFillDlg;              

Interface
{$IFNDEF AXDLL} // <----------------- AXDLL
Uses AM_Def,WinProcs,WinTypes,Messages,SysUtils,Classes,Graphics,Controls,Forms,
     Dialogs,StdCtrls,ExtCtrls,WCtrls,Spinbtn,Validate,AM_Proj,AM_Group,StyleDef,
     Palette,SymbolSelDlg,DipGrid,ProjStyle,MultiLng,PropCollect;

Type TSymbolFillDialog  = Class(TWForm)
       Bevel1           : TBevel;
       OKBtn            : TButton;
       CancelBtn        : TButton;
       WidthVal         : TMeasureLookupValidator;
       SymAngleVal      : TMeasureLookupValidator;
       DistanceVal      : TMeasureLookupValidator;
       LineDistVal      : TMeasureLookupValidator;
       OffsetVal        : TMeasureLookupValidator;
       AngleVal         : TMeasureLookupValidator;
       ControlsPanel    : TPanel;
       TypeCombo        : TComboBox;
       Label3           : TWLabel;
       DistanceEdit     : TEdit;
       LineDistEdit     : TEdit;
       Label4           : TWLabel;
       Label5           : TWLabel;
       OffsetEdit       : TEdit;
       OffsetSpin       : TSpinBtn;
       LineDistSpin     : TSpinBtn;
       DistanceSpin     : TSpinBtn;
       LineBtn          : TButton;
       FillBtn          : TButton;
       WidthEdit        : TEdit;
       WidthSpin        : TSpinBtn;
       Label8           : TWLabel;
       SymScaleDependCheck: TCheckBox;
       Label7           : TWLabel;
       SymAngleEdit     : TEdit;
       SymAngleSpin     : TSpinBtn;
       AngleEdit        : TEdit;
       AngleSpin        : TSpinBtn;
       ScaleDependCheck : TCheckBox;
       Label6           : TWLabel;
       MlgSection       : TMlgSection;
       SymbolWindow     : TWindow;
       SymBtn           : TButton;
       Procedure   DistanceEditChange(Sender: TObject);
       Procedure   FillBtnClick(Sender: TObject);
       Procedure   LineBtnClick(Sender: TObject);
       Procedure   WidthEditChange(Sender: TObject);
       Procedure   FormHide(Sender: TObject);
       Procedure   FormShow(Sender: TObject);
       Procedure   FormCreate(Sender: TObject);
       Procedure   FormDestroy(Sender: TObject);
       Procedure   FormCloseQuery(Sender: TObject; var CanClose: Boolean);
       Procedure   SymBtnClick(Sender: TObject);
       Procedure   SymbolWindowPaint(Sender: TObject);
       Procedure   TypeComboClick(Sender: TObject);
      Private

       ItIsNew:boolean; //Brovak Bug 164
       FOrgProjStyles   : TProjectStyles;
       FOrgSymbolFill   : TSymbolFillProperties;
       FProjectStyles   : TProjectStyles;
       FProject         : PProj;
       FSymbol          : PSGroup;
       FSymbolFill      : TSymbolFillProperties;
       Function    GetFillType:Integer;
       Procedure   UpdateEnabled;
       Procedure   UpdateWidthSpin;
      Public
       Property    SymbolFill:TSymbolFillProperties read FSymbolFill write FOrgSymbolFill;
       Property    Project:PProj read FProject write FProject;
       Property    ProjectStyles:TProjectStyles read FProjectStyles write FOrgProjStyles;
     end;

{$ENDIF} // <----------------- AXDLL
Implementation
{$IFNDEF AXDLL} // <----------------- AXDLL

{$R *.DFM}

Uses AM_Index,AM_Obj,AM_Paint,FillDlg,LineDlg,Measures,NumTools,
     UserIntf,Win32Def;

Procedure TSymbolFillDialog.FillBtnClick(Sender: TObject);
  var Dialog       : TPatternDialog;
  begin
    Dialog:=TPatternDialog.Create(Self);
    try
      Dialog.ProjectStyles:=FProjectStyles;
      with Dialog.FillStyle do begin
        PropertyMode:=pmCollect;
        Assign(SymbolFill.FillStyle);
        BackColorOptions.Options:=[soObject];
        ForeColorOptions.Options:=[soObject];
        PatternOptions.Options:=[soObject];
        ScaleOptions.Options:=[soObject];
      end;
      if Dialog.ShowModal=mrOK then begin
        with SymbolFill.FillStyle do begin
          PropertyMode:=pmUpdate;
          Assign(Dialog.FillStyle);
        end;
      end;
    finally
      Dialog.Free;
    end;
  end;

Procedure TSymbolFillDialog.UpdateEnabled;
var AEnable        : Boolean;
begin
  AEnable:=GetFillType=symfRegular;
  SetControlEnabled(DistanceEdit,AEnable);
  SetControlEnabled(LineDistEdit,AEnable);
  SetControlEnabled(OffsetEdit,AEnable);
  SetControlEnabled(AngleEdit,AEnable);
  AEnable:=not((GetFillType=symfNone) or (GetFillType=symfFromLayer));
  SetControlEnabled(LineBtn,AEnable);
  SetControlEnabled(FillBtn,AEnable);
  SetControlEnabled(WidthEdit,AEnable);
  SetControlEnabled(SymScaleDependCheck,AEnable);
  SetControlEnabled(SymAngleEdit,AEnable);
  SetControlEnabled(SymBtn,AEnable);               { 100298-1Ray }
  SymbolWindow.Enabled:=AEnable;
  SetControlEnabled(SymbolWindow,False);
  AEnable:=DistanceEdit.Enabled and (DistanceVal.LookupIndex=-1)
      and not (DistanceVal.Units in [muDrawingUnits,muPercent,muNone]);
  if not AEnable then ScaleDependCheck.Checked:=FALSE; 
  ScaleDependCheck.Enabled:=AEnable;
  AEnable:=WidthEdit.Enabled and (WidthVal.LookupIndex=-1)
      and not (WidthVal.Units in [muDrawingUnits,muPercent,muNone]);
  if not AEnable then SymScaleDependCheck.Checked:=FALSE;
  SymScaleDependCheck.Enabled:=AEnable;
  // enable/disable all controls depending on the updates-enabled flag
  if not FSymbolFill.UpdatesEnabled then SetControlGroupEnabled(ControlsPanel,FALSE);
  {+++Brovak Bug 164}
  if (Self.Owner.ClassName='TEditObjectStyleHandler') and (TypeCombo.Text=Mlgsection[15]) then
 begin;
 ScaleDependCheck.Enabled:=true;
 SymScaleDependCheck.Enabled:=true;
 end;
   {--Brovak}
end;

Procedure TSymbolFillDialog.LineBtnClick(Sender: TObject);
var Dialog         : TLineStyleDialog;
begin
  Dialog:=TLineStyleDialog.Create(Self);
  try
    with Dialog do begin
      ProjectStyles:=FProjectStyles;
      with LineStyle do begin
        Assign(FSymbolFill.LineStyle);
        StyleOptions.Options:=[soObject];
        ColorOptions.Options:=[soObject];
        WidthOptions.Options:=[soObject];
        ScaleOptions.Options:=[soObject];
      end;
      if ShowModal=mrOK then with FSymbolFill.LineStyle do begin
        PropertyMode:=pmUpdate;
        Assign(LineStyle);
      end;
    end;
  finally
    Dialog.Free;
  end;
end;
          
Procedure TSymbolFillDialog.FormHide(Sender: TObject);
begin
  if ModalResult=mrOK then with FSymbolFill do begin
    FillType:=GetFillType;
    if (FillType>symfNone) and (FSymbol<>NIL) then begin
      if PToStr(FSymbol^.LibName)<>'' then begin
        PSymbols(Project^.PInfo^.Symbols)^.CopySymIntoProject(Project^.PInfo,FSymbol);
        FSymbol:=PSymbols(Project^.PInfo^.Symbols)^.GetSymbol(FSymbol^.GetName,
            FSymbol^.GetLibName);
        Symbol:=FSymbol^.Index;
      end;
      Size:=GetSizeField(WidthVal,SizeOptions,SymScaleDependCheck);
      SizeType:=GetSizeTypeField(WidthVal,SizeOptions,SymScaleDependCheck);
      Distance:=GetSizeField(DistanceVal,DistanceOptions,ScaleDependCheck);
      DistanceType:=GetSizeTypeField(DistanceVal,DistanceOptions,ScaleDependCheck);
      LineDistance:=GetSizeField(LineDistVal,DistanceOptions,ScaleDependCheck);
      Offset:=GetSizeField(OffsetVal,OffsetOptions,ScaleDependCheck);
      OffsetType:=GetSizeTypeField(OffsetVal,OffsetOptions,ScaleDependCheck);
      Angle:=GetDoubleField(1,AngleVal,muRad,AngleOptions);
      SymbolAngle:=GetDoubleField(1,SymAngleVal,muRad,SymbolAngleOptions);
    end;
    if FOrgSymbolFill<>NIL then FOrgSymbolFill.Assign(FSymbolFill);
    if FOrgProjStyles<>NIL then FOrgProjStyles.Assign(FProjectStyles);
  end;
end;

Procedure TSymbolFillDialog.FormShow(Sender: TObject);
  var AIndex       : Integer;
      BIndex       : TIndex;
  begin
  {+++Brovak Bug 164}
    if ItIsNew= true then
begin;
  {--Brovak}
   // copy original styles to internal styles
    if FOrgSymbolFill<>NIL then FSymbolFill.Assign(FOrgSymbolFill);
    if FOrgProjStyles<>NIL then FProjectStyles.Assign(FOrgProjStyles);
    with FSymbolFill do begin
      PropertyMode:=pmSet;
      // add special values to type-combobox, select right item
      TypeCombo.Clear;
      FillTypeOptions.AddLongNamesToList(TypeCombo.Items);
      TypeCombo.Items.Add(MlgSection[15]);
//      TypeCombo.Items.Add(MlgSection[14]);
      case FillType of
        symfRegular    : AIndex:=FillTypeOptions.Count;
//        symfSingle     : AIndex:=FillTypeOptions.Count+1;
        else AIndex:=FillTypeOptions.IndexOfValue(FillType);
      end;
      TypeCombo.ItemIndex:=AIndex;
       {+++Brovak Bug 164}
//        if It is object properties dialog - set Types
      if Self.Owner.ClassName='TEditObjectStyleHandler' then begin;
      SizeType:=GetSizeTypeField(WidthVal,SizeOptions,SymScaleDependCheck);
      DistanceType:=GetSizeTypeField(DistanceVal,DistanceOptions,ScaleDependCheck);
                                                            end;
       {--Brovak}

      SetSizeField(Distance,DistanceType,DistanceVal,DistanceOptions,ScaleDependCheck);
      SetSizeField(LineDistance,DistanceType,LineDistVal,LineDistanceOptions,NIL);
      SetSizeField(Offset,OffsetType,OffsetVal,OffsetOptions,NIL);
      SetDoubleField(Angle,1.0,AngleVal,muRad,AngleOptions);
      SetDoubleField(SymbolAngle,1.0,SymAngleVal,muRad,SymbolAngleOptions);
      SetSizeField(Size,SizeType,WidthVal,SizeOptions,SymScaleDependCheck);
      UpdateWidthSpin;
      BIndex.Init(FSymbolFill.Symbol);
      FSymbol:=PSGroup(PSymbols(FProject^.PInfo^.Symbols)^.IndexObject(FProject^.PInfo,@BIndex));
      UpdateEnabled;
    end;
    ItIsNew:=false;
  end;
  end;

{******************************************************************************+
  Procedure TSymbolFillDialog.UpdateWidthSpin
--------------------------------------------------------------------------------
  Sets the width-spin options depending on the type of the symbol-width.
+******************************************************************************}
Procedure TSymbolFillDialog.UpdateWidthSpin;
begin
  if WidthVal.Units=muPercent then begin
    WidthSpin.Multiply:=TRUE;
    WidthSpin.Increment:=2;
    WidthSpin.ShiftIncrement:=5;
    WidthSpin.CtrlIncrement:=10;
  end
  else begin
    WidthSpin.Multiply:=FALSE;
{!?!?!    WidthSpin.Increment:=2;
    WidthSpin.ShiftIncrement:=5;
    WidthSpin.CtrlIncrement:=10;}
  end;  
end;

Procedure TSymbolFillDialog.FormCreate(Sender: TObject);
begin
  FProjectStyles:=TProjectStyles.Create;
  FSymbolFill:=TSymbolFillProperties.Create;
  {++Brovak Bug 164}
  ItIsNew:=true;
  {--Brovak}
end;

Procedure TSymbolFillDialog.FormDestroy(Sender: TObject);
begin
  FSymbolFill.Free;
  FProjectStyles.Free;
end;

Procedure TSymbolFillDialog.WidthEditChange(Sender: TObject);
begin
  if (WidthVal.LookupIndex<>-1) or (WidthVal.Units in [muDrawingUnits,muPercent,muNone]) then
    SymScaleDependCheck.Checked:=FALSE;
  UpdateEnabled;
  UpdateWidthSpin;
end;

Procedure TSymbolFillDialog.DistanceEditChange(Sender: TObject);
  var Validator    : TMeasureLookupValidator;
  begin
    if Sender=DistanceEdit then Validator:=DistanceVal
    else if Sender=LineDistEdit then Validator:=LineDistVal
    else if Sender=OffsetEdit then Validator:=OffsetVal
    else Validator:=NIL;
    if Validator<>NIL then begin
      if (Validator.LookupIndex<>-1) or (Validator.Units in [muDrawingUnits,muPercent,muNone]) then
          SymScaleDependCheck.Checked:=FALSE;
      if Validator.Units<>muPercent then begin
        if (DistanceVal.Units in [muDrawingUnits])
            XOR (Validator.Units in [muDrawingUnits]) then
          DistanceVal.Units:=Validator.Units;
        if (LineDistVal.Units in [muDrawingUnits])
            XOR (Validator.Units in [muDrawingUnits]) then
          LineDistVal.Units:=Validator.Units;
        if (OffsetVal.Units<>muPercent) and ((OffsetVal.Units in [muDrawingUnits])
            XOR (Validator.Units in [muDrawingUnits])) then
          OffsetVal.Units:=Validator.Units;
      end;    
    end;
    UpdateEnabled;
  end;

Procedure TSymbolFillDialog.FormCloseQuery(Sender: TObject;
    var CanClose: Boolean);
  begin
    CanClose:=CheckValidators(Self);
    if CanClose and (ModalResult=mrOK) then begin
      CanClose:=FALSE;
      if (GetFillType=symfNone) or (GetFillType=symfFromLayer) then CanClose:=TRUE
      else begin
        {if FSymbol=NIL then MessageDialog(MlgSection[13],mtInformation,[mbOK],0)
        else} CanClose:=TRUE;
      end;
    end;  
  end;

Function TSymbolFillDialog.GetFillType
   : Integer;
  begin                       
    case TypeCombo.ItemIndex-FSymbolFill.FillTypeOptions.Count of
      0 : Result:=symfRegular;
//      1 : Result:=symfSingle;
      else Result:=FSymbolFill.FillTypeOptions.Values[TypeCombo.ItemIndex];
    end;
  end;

Procedure TSymbolFillDialog.SymBtnClick(Sender: TObject); 
var SelectedSymbol : PSGroup;
begin
  SelectedSymbol:=GetSymbol(Self,FProject);
  if SelectedSymbol<>NIL then with FSymbolFill do begin
    FSymbol:=SelectedSymbol;
    Symbol:=FSymbol^.Index;
    SymbolWindow.Repaint;
  end;
end;

Procedure TSymbolFillDialog.TypeComboClick(Sender: TObject);
begin
  UpdateEnabled;
end;

Procedure TSymbolFillDialog.SymbolWindowPaint(Sender: TObject);
var ARect          : TRect;
begin
  with SymbolWindow do begin
    if Enabled then begin
      Canvas.Font.Color:=clWindowText;
      Canvas.Pen.Color:=ColorToRGB(clBlack);
    end
    else begin
      Canvas.Font.Color:=clGrayText;
      Canvas.Pen.Color:=ColorToRGB(clSilver);
    end;
    if FSymbol<>NIL then begin
      Canvas.TextOut(Height,23,PToStr(FSymbol^.Name));
      Canvas.TextOut(Height,25+Abs(Font.Height),PToStr(FSymbol^.LibName));
      ARect:=Rect(2,2,Height-4,Height-4);
      FSymbol^.DrawSymbolInRect(Canvas.Handle,ARect,Project^.PInfo^.Fonts);
    end
    else with FSymbolFill do if Symbol<0 then begin
      Canvas.TextOut(5,23,SymbolOptions.LongNames[SymbolOptions.IndexOfValue(Symbol)]);
    end;
  end;
end;
{$ENDIF} // <----------------- AXDLL
end.

