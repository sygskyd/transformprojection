unit MTextInput;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  MultiLng, StdCtrls;

type
  TMTextInput = class(TForm)
    MlgSection: TMlgSection;
    OKBtn: TButton;
    CancelBtn: TButton;
    Label1: TLabel;
    memText: TMemo;
    procedure FormCreate(Sender: TObject);
    procedure memTextChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

Uses AM_Text;

procedure TMTextInput.FormCreate(Sender: TObject);
begin
  memText.Text:= '';
end;

procedure TMTextInput.memTextChange(Sender: TObject);
var
  len   : Integer;
begin
  len:= Length( Trim( memText.Text ) );
  if len >= cMaxTxtLen
  then  memText.Font.Color:= clGrayText	
  else  memText.Font.Color:= clWindowText;
end;

end.
