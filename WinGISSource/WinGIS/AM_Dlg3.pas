{****************************************************************************}
{ Unit                                                                       }
{----------------------------------------------------------------------------}
{                                                                            }
{----------------------------------------------------------------------------}
{ Modifikationen                                                             }
{  16.02.1994 Martin Forst                                                   }
{****************************************************************************}
Unit AM_Dlg3;

Interface

Uses Messages,SysUtils,WinTypes,WinProcs,AM_Def,AM_Text,AM_Font,AM_Paint,
     AM_Ini,Objects,AM_StdDl,Classes,ResDlg;

Const id_TField         = 100;
      id_TSample        = 102;
      id_THeight        = 103;

      id_Font           = 507;

      id_Italic         = 102;
      id_Underl         = 103;
      id_Bold           = 104;

      id_Left           = 130;
      id_Center         = 131;
      id_Right          = 132;
      id_FontList       = 126;
      id_FixHeight      = 127;
      id_FixList        = 134;

      id_Opaque         = 390;
      id_Transparent    = 391;

Type TDInpText     = Class(TGrayDialog)
      Public
       Text        : PShortString;
       Constructor Init(AParent:TComponent;AText:PShortString);
       Procedure   SetupWindow; override;
       Function    CanClose:Boolean; override;
     end;

     TDFont        = Class(TLayerDlg)
      Public
       Data        : PFontData;
       PInfo       : PPaint;
       NeedHeight  : Boolean;
       Constructor Init(AParent:TComponent;APInfo:PPaint;AData:PFontData;Name:PChar;AHeight:Boolean);
       Function    CanClose:Boolean; override;
       Function    GetFont:HFont;
       Procedure   GetFontStyle(var AFont:TFontData);
       Procedure   SetupWindow; override;
       Procedure   WMCommand(var Msg:TWMCommand); message wm_First+wm_command;
       Procedure   WMDrawObject(var Msg:TMessage); message wm_First+wm_DrawObject;
     end;
 
Implementation

Function TDFont.GetFont
   : HFont;
  var FStyle       : TFontData;
  begin
    GetFontStyle(FStyle);
    if FStyle.Font<>0 then GetFont:=PInfo^.GetFont(FStyle,35,0)
    else GetFont:=0;
  end;

Procedure TDFont.WMDrawObject
   (
   var Msg         : TMessage
   );
  var Font         : HFont;
      OldFont      : HFont;
      Rect         : TRect;
  begin
    if Msg.wParam=125 then with Msg do begin
      GetClientRect(GetItemHandle(wParam),Rect);
      Font:=GetFont;
      if Font<>0 then begin
        OldFont:=SelectObject(lParamlo,Font);
        DrawText(lParamlo,'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPp',32,Rect,dt_Left or dt_VCenter);
        SelectObject(lParamlo,OldFont);
        DeleteObject(Font);
      end;
    end;
  end;

Constructor TDFont.Init
   (
   AParent         : TComponent;
   APInfo          : PPaint;
   AData           : PFontData;
   Name            : PChar;
   AHeight         : Boolean
   );
  begin
    inherited Init(AParent,Name,NIL,APInfo^.ProjStyles);
    Data:=AData;
    PInfo:=APInfo;
    NeedHeight:=AHeight;
  end;

Procedure TDFont.SetupWindow;
  Procedure DoAll
     (
     Item          : PFontDes
     ); Far;
    var AName      : String;
    begin
      AName:=Item^.Name^+#0;
      SendDlgItemMsg(id_FontList,cb_AddString,0,LongInt(@AName[1]));
    end;
  var BText        : String;
      AFont        : PFontDes;
  begin
    inherited SetupWindow;
    PInfo^.Fonts^.ForEach(@DoAll);
    AFont:=PInfo^.Fonts^.GetFont(Data^.Font);
    if AFont<>NIL then begin
      BText:=AFont^.Name^+#0;
      SendDlgItemMsg(id_FontList,cb_SelectString,-1,LongInt(@BText[1]));
    end;
    SetFieldLong(id_FixList,Data^.Height);
    CheckDlgButton(Handle,id_FixHeight,Byte(Data^.Style and ts_FixHeight<>0));
    CheckDlgButton(Handle,id_Bold,Byte(Data^.Style and ts_Bold<>0));
    CheckDlgButton(Handle,id_Underl,Byte(Data^.Style and ts_Underl<>0));
    CheckDlgButton(Handle,id_Italic,Byte(Data^.Style and ts_Italic<>0));
    CheckRadioButton(Handle,id_Left,id_Right,id_Left+Byte(Data^.Style and ts_Align));
    CheckRadioButton(Handle,id_Opaque,id_Transparent,id_Opaque+Byte(Data^.Style and ts_Transparent<>0));
  end;

Procedure TDFont.WMCommand
   (
   var Msg         : TWMCommand
   );
  begin
    inherited ;
    if (Msg.ItemID<=126)
        and (Msg.ItemID>=102)
        and ((Msg.NotifyCode=bn_Clicked)
            or (Msg.NotifyCode=cbn_SelChange)
            or (Msg.NotifyCode=cbn_KillFocus)) then
      InvalidateRect(GetItemHandle(125),NIL,FALSE);
  end;

Procedure TDFont.GetFontStyle
   (
   var AFont       : TFontData
   );
  var Cnt          : Integer;
      AHeight      : Array[0..255] of Char;
  begin
    with AFont do begin
      Style:=0;
      if IsDlgButtonChecked(Handle,id_Bold)=1 then Style:=Style or ts_Bold;
      if IsDlgButtonChecked(Handle,id_Underl)=1 then Style:=Style or ts_Underl;
      if IsDlgButtonChecked(Handle,id_Italic)=1 then Style:=Style or ts_Italic;
    if IsDlgButtonChecked(Handle,id_Transparent)=1 then Style:=Style or ts_Transparent;
      Cnt:=SendDlgItemMsg(id_FontList,cb_GetCurSel,0,0);
      if Cnt>=0 then begin
        SendDlgItemMsg(id_FontList,cb_GetLBText,Cnt,LongInt(@AHeight));
        Font:=PInfo^.Fonts^.IDFromName(StrPas(AHeight));
      end
      else Font:=0;
      if IsDlgButtonChecked(Handle,id_FixHeight)=1 then Style:=Style or ts_FixHeight;
      GetFieldLong(id_FixList,Height);
      if IsDlgButtonChecked(Handle,id_Left)=1 then Style:=Style or ts_Left
      else if IsDlgButtonChecked(Handle,id_Right)=1 then Style:=Style or ts_Right
      else if IsDlgButtonChecked(Handle,id_Center)=1 then Style:=Style or ts_Center
    end;
  end;

Function TDFont.CanClose
   : Boolean;
  var AHeight      : LongInt;
  begin
    CanClose:=FALSE;
    if (not NeedHeight
        and (IsDlgButtonChecked(Handle,id_FixHeight)=0))
        or (GetFieldLong(id_FixList,AHeight)
        and (AHeight>0)) then begin
      GetFontStyle(Data^);
      CanClose:=TRUE;
    end
    else begin
      MsgBox(Handle,210,mb_IconExclamation or mb_Ok,'');
      SetFocus(GetItemHandle(id_FixList));
    end;
  end;

Constructor TDInpText.Init
   (
   AParent         : TComponent;
   AText           : PShortString
   );
  begin
    inherited Init(AParent,'TEXTINPUT');
    Text:=AText;
  end;

Procedure TDInpText.SetupWindow;
  var AText        : Array[0..255] of Char;
  begin
    inherited SetupWindow;
    SetDlgItemText(Handle,100,StrPCopy(AText,Text^));
  end;

Function TDInpText.CanClose
   : Boolean;
  var AText        : Array[0..255] of Char;
  begin
    GetDlgItemText(Handle,100,AText,255);
    if StrComp(AText,'')=0 then begin
      MsgBox(Handle,2010,mb_OK or mb_IconExclamation,'');
      CanClose:=FALSE;
    end
    else begin
      CanClose:=TRUE;
      Text^:=StrPas(AText);
    end;
  end;

end.
