{****************************************************************************}
{ Unit AM_Circle                                                             }
{----------------------------------------------------------------------------}
{ Kreise, Kreisb�gen                                                         }
{----------------------------------------------------------------------------}
{ Modifikationen                                                             }
{  16.02.1994 Martin Forst                                                   }
{  05.04.1994 Martin Forst   Einf�gen ins Projekt, Selektieren               }
{  12.04.1994 Martin Forst   Selektieren mit Kreis und Poly                  }
{  19.07.1994 Martin Forst   MapToCircle->Absturz wenn Punkt=Mittelpunkt     }
{                            beseitigt                                       }
{****************************************************************************}
unit AM_Circl;

interface

uses WinTypes, WinProcs, AM_Def, AM_View, AM_Paint, Objects, AM_Index;

const { Konstanten f�r TCircle und TArc .Status                              }
  ciMaxDist = 10; { Maximaler Abstand f�r SelectByPoint }
                                       { in 1/10 mm                          }
  Tolerance = 1E-4;

type {***********************************************************************}
     { Object TEllipse                                                       }
     {-----------------------------------------------------------------------}
     { Position              = Mittelpunkt der Ellipse                       }
     { PrimaryAxis           = L�nge der Hauptachse                          }
     { SecondaryAxis         = L�nge der Nebenachse                          }
     { Angle                 = Drehwinkel der Hauptachse zur Horizontalen    }
     {***********************************************************************}
  PEllipse = ^TEllipse;
  TEllipse = object(TView)
    Position: TDPoint;
    PrimaryAxis: LongInt;
    SecondaryAxis: LongInt;
    Angle: Real48;
    constructor Init(APosition: TDPoint; Primary, Secondary: LongInt; AAngle: Real);
    constructor Load(S: TOldStream);
    procedure CalculateClipRect; virtual;
    function GetObjType: Word; virtual;
    function GivePosX: LongInt; virtual;
    function GivePosY: LongInt; virtual;
    function GiveRadius: LongInt; virtual;
    procedure MoveRel(XMove, YMove: LongInt); virtual;
    function PointInside(ItemPos: TDPoint): Boolean;
    procedure Scale(XScale, YScale: Real); virtual; {2705F}
    function SelectBetweenPolys(PInfo: PPaint; Poly1, Poly2: Pointer; const ObjType: TObjectTypes;
      Inside: Boolean): Boolean; virtual;
    function SelectByCircle(PInfo: PPaint; Middle: TDPoint; ARadius: LongInt;
      const ObjType: TObjectTypes; Inside: Boolean): Boolean; virtual;
    function SelectByPoint(PInfo: PPaint; Point: TDPoint; const ObjType: TObjectTypes): PIndex; virtual;
    function SelectByPoly(PInfo: PPaint; Poly: Pointer; const ObjType: TObjectTypes; Inside: Boolean): Boolean; virtual;
    function SelectByPolyLine(PInfo: PPaint; Poly: Pointer; const ObjType: TObjectTypes): Boolean; virtual;
    procedure Store(S: TOldStream); virtual;
    procedure Draw(PInfo: PPaint; Clipped: Boolean); virtual;
    function Flaeche: Double;
    function Laenge: Double;
//++ Glukhov Bug#199 Build#157 27.04.01
      // Calculates a number of points to approximate the circle with the given inaccuracy
    function ApproximatePointsNumber(Delta: Double = 0.0; bRel: Boolean = True): LongInt; virtual;
      // Calculates a point of the circle approximation with the given inaccuracy
      // bRel defines Relative/Absolute inaccuracy
      // Num >= 4; Idx should be 0..Num-1
      // First and last Points are Position+(PrimaryAxis,0)
      // Sign>=0 means counterclockwise direction
    function ApproximatePoint(Idx, Num: LongInt; Sign: Integer = 1): TDPoint; virtual;
//-- Glukhov Bug#199 Build#157 27.04.01
  end;

     {***********************************************************************}
     { Object TSEllipse                                                      }
     {-----------------------------------------------------------------------}
     {***********************************************************************}
  PSEllipse = ^TSEllipse;
  TSEllipse = object(TEllipse)
    constructor Init(APosition: TDPoint; Primary, Secondary: LongInt; AAngle: Real);
    constructor Load(S: TOldStream);
    constructor LoadOld(S: TOldStream);
    procedure Draw(PInfo: PPaint; Clipped: Boolean); virtual;
    procedure Store(S: TOldStream); virtual;
  end;

     {***********************************************************************}
     { Object TArc                                                           }
     {-----------------------------------------------------------------------}
     {***********************************************************************}
  PEllipseArc = ^TEllipseArc;
  TEllipseArc = object(TEllipse)
    BeginAngle: Real48;
    EndAngle: Real48;
    constructor Init(APosition: TDPoint; Primary, Secondary: LongInt; AAngle, ABegin, AEnd: Real);
    constructor Load(S: TOldStream);
    procedure CalculateClipRect; virtual;
    function GetObjType: Word; virtual;
    function GiveBegAngle: Real; virtual;
    function GiveEndAngle: Real; virtual;
    function PointOnArc(Point: TDPoint): Boolean;
    function SelectBetweenPolys(PInfo: PPaint; Poly1, Poly2: Pointer; const ObjType: TObjectTypes;
      Inside: Boolean): Boolean; virtual;
    function SelectByCircle(PInfo: PPaint; Middle: TDPoint; ARadius: LongInt;
      const ObjType: TObjectTypes; Inside: Boolean): Boolean; virtual;
    function SelectByPoint(PInfo: PPaint; Point: TDPoint; const ObjType: TObjectTypes): PIndex; virtual;
    function SelectByPoly(PInfo: PPaint; Poly: Pointer; const ObjType: TObjectTypes; Inside: Boolean): Boolean; virtual;
    function SelectByPolyLine(PInfo: PPaint; Poly: Pointer; const ObjType: TObjectTypes): Boolean; virtual;
    function SearchForPoint(PInfo: PPaint; InCircle: Pointer; Exclude: LongInt; const ObjType: TObjectTypes;
      var Point: TDPoint; var SnObje: PIndex): Boolean; virtual;
    procedure Store(S: TOldStream); virtual;
    procedure Draw(PInfo: PPaint; Clipped: Boolean); virtual;
    function SearchForNearestPoint(PInfo: PPaint; var ClickPos: TDPoint; InCircle: Pointer;
      Exclude: Longint; const ObjType: TObjectTypes; var point: TDPoint; var SnObje: PIndex): Boolean; virtual;
    function Laenge: Double;
//++ Glukhov Bug#199 Build#157 27.04.01
      // Calculates a number of points to approximate the arc with the given inaccuracy
    function ApproximatePointsNumber(Delta: Double = 0.0; bRel: Boolean = True): LongInt; virtual;
      // Calculates a point of the arc approximation with the given inaccuracy
      // bRel defines Relative/Absolute inaccuracy
      // Num >= 2; Idx should be 0..Num-1
      // First and last Points correspond to BeginAngle and EndAngle
      // Sign>=0 means Begin-End direction
    function ApproximatePoint(Idx, Num: LongInt; Sign: Integer = 1): TDPoint; virtual;
//-- Glukhov Bug#199 Build#157 27.04.01
  end;

     {***********************************************************************}
     { Object TSEllipseArc                                                   }
     {-----------------------------------------------------------------------}
     {***********************************************************************}
  PSEllipseArc = ^TSEllipseArc;
  TSEllipseArc = object(TEllipseArc)
    constructor LoadOld(S: TOldStream);
    procedure Draw(PInfo: PPaint; Clipped: Boolean); virtual;
  end;

implementation

uses AM_CPoly, AM_Poly, Classes, GrTools, Clipping, XLines, XStyles, SymbolFill
  , Math // Glukhov Bug#199 Build#157 27.04.01
  ;

{========================================================================================}
{ TEllipse                                                                               }
{========================================================================================}

constructor TEllipse.Init
  (
  APosition: TDPoint;
  Primary: LongInt;
  Secondary: LongInt;
  AAngle: Real
  );
begin
  inherited Init;
  Position.Init(APosition.X, APosition.Y);
  PrimaryAxis := Primary;
  SecondaryAxis := Secondary;
  Angle := AAngle;
  CalculateClipRect;
end;

constructor TEllipse.Load
  (
  S: TOldStream
  );
begin
  inherited Load(S);
  Position.Load(S);
  S.Read(PrimaryAxis, SizeOf(PrimaryAxis));
  S.Read(SecondaryAxis, SizeOf(SecondaryAxis));
  S.Read(Angle, SizeOf(Angle));
end;

function TEllipse.GetObjType
  : Word;
begin
  GetObjType := ot_Circle;
end;

procedure TEllipse.MoveRel
  (
  XMove: LongInt;
  YMove: LongInt
  );
begin
  inherited MoveRel(XMove, YMove);
  Position.Move(XMove, YMove);
end;

function TEllipse.SelectByCircle
  (
  PInfo: PPaint;
  Middle: TDPoint;
  ARadius: LongInt;
  const ObjType: TObjectTypes;
  Inside: Boolean
  )
  : Boolean;
var
  D: Double;
begin
  if ot_Circle in ObjType then
  begin
    D := Middle.Dist(Position);
    if Inside then
      SelectByCircle := PrimaryAxis <= ARadius - D
    else
      SelectByCircle := ARadius >= Abs(D - PrimaryAxis)
  end
  else
    SelectByCircle := FALSE;
end;

function TEllipse.SelectByPoint
  (
  PInfo: PPaint;
  Point: TDPoint;
  const ObjType: TObjectTypes
  )
  : PIndex;
var
  Dist: Double;
  MDist: Double;
begin
  SelectByPoint := nil;
  if ot_Circle in ObjType then
  begin
    Dist := Position.Dist(Point);
    MDist := PInfo^.CalculateDraw(ciMaxDist);
    if (Abs(Dist - PrimaryAxis) < MDist) or (Dist < PrimaryAxis) then
      SelectByPoint := @Self;
  end;
end;

function TEllipse.PointInside
  (
  ItemPos: TDPoint
  )
  : Boolean;
begin
  Result := Position.Dist(ItemPos) < PrimaryAxis;
end;

function TEllipse.SelectByPoly
  (
  PInfo: PPaint;
  Poly: Pointer;
  const ObjType: TObjectTypes;
  Inside: Boolean
  )
  : Boolean;
begin
  SelectByPoly := FALSE;
  if (ot_Circle in ObjType)
    and ClipRect.IsVisible(PView(Poly)^.ClipRect) then
  begin
    if Inside then
      SelectByPoly := ClipRect.IsInside(PView(Poly)^.ClipRect)
        and not PCPoly(Poly)^.CheckNormalDist(PInfo, @Position, PrimaryAxis)
    else
      SelectByPoly := PCPoly(Poly)^.CheckPointDist(@Position, PrimaryAxis)
        or PCPoly(Poly)^.CheckNormalDist(PInfo, @Position, PrimaryAxis)
        or PCPoly(Poly)^.PointInside(PInfo, Position);
  end;
end;

function TEllipse.SelectBetweenPolys
  (
  PInfo: PPaint;
  Poly1: Pointer;
  Poly2: Pointer;
  const ObjType: TObjectTypes;
  Inside: Boolean
  )
  : Boolean;
begin
  SelectBetweenPolys := FALSE;
  if (GetObjType in ObjType)
    and (ClipRect.IsVisible(PView(Poly1)^.ClipRect) or ClipRect.IsVisible(PView(Poly2)^.ClipRect)) then
  begin
    if Inside then
      SelectBetweenPolys := (ClipRect.IsInside(PView(Poly1)^.ClipRect) xor ClipRect.IsInside(PView(Poly2)^.ClipRect))
        and not PCPoly(Poly1)^.CheckNormalDist(PInfo, @Position, PrimaryAxis)
        and not PCPoly(Poly2)^.CheckNormalDist(PInfo, @Position, PrimaryAxis)
    else
      SelectBetweenPolys := (PCPoly(Poly1)^.CheckPointDist(@Position, PrimaryAxis)
        xor PCPoly(Poly2)^.CheckPointDist(@Position, PrimaryAxis))
        or (PCPoly(Poly1)^.CheckNormalDist(PInfo, @Position, PrimaryAxis)
        xor PCPoly(Poly2)^.CheckNormalDist(PInfo, @Position, PrimaryAxis))
        or (PCPoly(Poly1)^.PointInside(PInfo, Position)
        xor PCPoly(Poly2)^.PointInside(PInfo, Position));
  end;
end;

function TEllipse.SelectByPolyLine
  (
  PInfo: PPaint;
  Poly: Pointer;
  const ObjType: TObjectTypes
  )
  : Boolean;
begin
  SelectByPolyLine := FALSE;
  if (ot_Circle in ObjType)
    and ClipRect.IsVisible(PView(Poly)^.ClipRect) then
    SelectByPolyLine := PPoly(Poly)^.CheckPointDist(@Position, PrimaryAxis)
      or PPoly(Poly)^.CheckNormalDist(PInfo, @Position, PrimaryAxis);
end;

procedure TEllipse.Store
  (
  S: TOldStream
  );
begin
  inherited Store(S);
  Position.Store(S);
  S.Write(PrimaryAxis, SizeOf(PrimaryAxis));
  S.Write(SecondaryAxis, SizeOf(SecondaryAxis));
  S.Write(Angle, SizeOf(Angle));
end;

procedure TEllipse.Draw
  (
  PInfo: PPaint;
  Clipped: Boolean
  );
var
  AMiddle: TPoint;
  XMiddle: Double;
  YMiddle: Double;
begin
  if true or Clipped then
  begin
    PInfo^.ConvertToDispDouble(Position, XMiddle, YMiddle);
    if PInfo^.ActSymbolFill.FillType = symfNone then
      PInfo^.ExtCanvas.ClippedCircle(
        GrPoint(XMiddle, YMiddle), PInfo^.CalculateDispDouble(PrimaryAxis))
    else
      PInfo^.ExtCanvas.ClippedCircle(GrPoint(XMiddle, YMiddle),
        PInfo^.CalculateDispDouble(PrimaryAxis), @SymbolFillRectangle,
        LongInt(PInfo), Integer(@ClipRect), 0);
  end
  else
  begin
    PInfo^.ConvertToDisp(Position, AMiddle);
    if PInfo^.ActSymbolFill.FillType = symfNone then
      PInfo^.ExtCanvas.Circle(
        AMiddle, PInfo^.CalculateDisp(PrimaryAxis))
    else
      PInfo^.ExtCanvas.Circle(AMiddle, PInfo^.CalculateDisp(PrimaryAxis),
        @SymbolFillRectangle, LongInt(PInfo), Integer(@ClipRect), 0);
  end;
end;

procedure TEllipse.CalculateClipRect;
begin
  ClipRect.AssignByPoint(Position, PrimaryAxis);
end;

function TEllipse.GivePosX: LongInt;
begin
  GivePosX := Position.X;
end;

function TEllipse.GivePosY: LongInt;
begin
  GivePosY := Position.Y;
end;

function TEllipse.GiveRadius: LongInt;
begin
  GiveRadius := PrimaryAxis;
end;

function TEllipse.Flaeche: Double;
begin
  Flaeche := Sqr(PrimaryAxis / 100) * Pi;
end;

function TEllipse.Laenge: Double;
begin
  Laenge := 2 * (PrimaryAxis / 100) * Pi;
end;

procedure TEllipse.Scale {2705F}
  (
  XScale: Real;
  YScale: Real
  );
begin
  inherited Scale(XScale, YScale);
  Position.Scale(XScale, YScale);
  PrimaryAxis := LimitToLong(PrimaryAxis * XScale);
end; {2705F}

//++ Glukhov Bug#199 Build#157 27.04.01
// Calculates a number of points to approximate the circle with the given inaccuracy

function TEllipse.ApproximatePointsNumber(Delta: Double = 0.0; bRel: Boolean = True): LongInt;
var
  dAlp, dNum: Double;
begin
  Result := 4;
  if bRel then
    Delta := Delta * PrimaryAxis;
    // Inaccuracy has to be less than radius
  if Delta <= 0.0 then
    Delta := PrimaryAxis / 256.0;
  if Delta > PrimaryAxis then
    Delta := PrimaryAxis;
  if Delta <= 1.0 then
    Delta := 1.0;
    // Calculates the half of the sector angle
  dAlp := arccos(1 - Delta / PrimaryAxis);
  dNum := Pi / dAlp;
  Result := Trunc(dNum);
  if Result < dNum then
    Inc(Result);
  if Result < 4 then
    Result := 4;
end;

// Calculates a point of approximation the circle with the given inaccuracy
// Num >= 4; Idx should be 0..Num-1
// First and last Points are Position+(PrimaryAxis,0)
// Sign>=0 means counterclockwise direction

function TEllipse.ApproximatePoint(Idx, Num: LongInt; Sign: Integer = 1): TDPoint;
var
  dAlp: Double;
begin
  Result.Init(Position.X, Position.Y);
  if Num < 4 then
    Num := 4;
  if Idx < 0 then
    Idx := 0;
  if Idx >= Num then
    Idx := Num - 1;
  if Sign < 0 then
    Idx := Num - Idx - 1;
  dAlp := 2 * Pi * Idx / (Num - 1);
  Result.MoveAngle(dAlp, PrimaryAxis);
end;
//-- Glukhov Bug#199 Build#157 27.04.01

{========================================================================================}
{ TSEllipse                                                                              }
{========================================================================================}

constructor TSEllipse.Init
  (
  APosition: TDPoint;
  Primary: LongInt;
  Secondary: LongInt;
  AAngle: Real
  );
begin
  inherited Init(APosition, Primary, Secondary, AAngle);
end;

constructor TSEllipse.LoadOld
  (
  S: TOldStream
  );
var
  Dummy: Boolean;
begin
  TView.Load(S);
  S.Read(PrimaryAxis, SizeOf(PrimaryAxis));
  SecondaryAxis := PrimaryAxis;
  Angle := 0;
  Position.Load(S);
  S.Read(Dummy, SizeOf(Dummy));
end;

constructor TSEllipse.Load
  (
  S: TOldStream
  );
var
  Dummy: Boolean;
begin
  inherited Load(S);
  S.Read(Dummy, SizeOf(Dummy));
end;

procedure TSEllipse.Store
  (
  S: TOldStream
  );
var
  Dummy: Boolean;
begin
  inherited Store(S);
  S.Write(Dummy, SizeOf(Dummy));
end;

procedure TSEllipse.Draw
  (
  PInfo: PPaint;
  Clipped: Boolean
  );
var
  AMiddle: TPoint;
  XMiddle: Double;
  YMiddle: Double;
begin
  with PInfo^ do
    if Clipped then
    begin
      ScaleToDispDouble(Position, XMiddle, YMiddle);
      ExtCanvas.ClippedCircle(GrPoint(XMiddle, YMiddle),
        ScaleDispDouble(PrimaryAxis));
    end
    else
    begin
      ScaleToDisp(Position, AMiddle);
      ExtCanvas.Circle(AMiddle, ScaleDisp(PrimaryAxis));
    end;
end;

{========================================================================================}
{ TEllipseArc                                                                            }
{========================================================================================}

constructor TEllipseArc.Init
  (
  APosition: TDPoint;
  Primary: LongInt;
  Secondary: LongInt;
  AAngle: Real;
  ABegin: Real;
  AEnd: Real
  );
begin
  BeginAngle := 0;
  EndAngle := 0;
  inherited Init(APosition, Primary, Secondary, AAngle);
  BeginAngle := ABegin;
  EndAngle := AEnd;
  CalculateClipRect;
end;

constructor TEllipseArc.Load
  (
  S: TOldStream
  );
begin
  inherited Load(S);
  S.Read(BeginAngle, SizeOf(BeginAngle));
  S.Read(EndAngle, SizeOf(EndAngle));
end;

function TEllipseArc.GetObjType
  : Word;
begin
  GetObjType := ot_Arc;
end;

function TEllipseArc.PointOnArc(Point: TDPoint): Boolean;
var
  Angle1: Real;
  Angle2: Real;
  Angle3: Real;
begin
  Angle1 := BeginAngle + Angle;
  Angle2 := EndAngle + Angle;
  Angle3 := Position.CalculateAngle(Point);
  if Angle1 > Angle2 then
    Angle1 := Angle1 - 2 * Pi;
  if Angle3 > Angle2 then
    Angle3 := Angle3 - 2 * Pi;
  PointOnArc := (Angle3 >= Angle1) and (Angle3 <= Angle2);
end;

procedure TEllipseArc.CalculateClipRect;
var
  Pos: TDPoint;
begin
  ClipRect.Init;
  Pos.Init(PrimaryAxis, 0);
  Pos.Rotate(BeginAngle);
  Pos.Move(Position.X, Position.Y);
  ClipRect.CorrectByPoint(Pos);
  Pos.Init(PrimaryAxis, 0);
  Pos.Rotate(EndAngle);
  Pos.Move(Position.X, Position.Y);
  ClipRect.CorrectByPoint(Pos);
  Pos.Init(Position.X - PrimaryAxis, Position.Y);
  if PointOnArc(Pos) then
    ClipRect.CorrectByPoint(Pos);
  Pos.Init(Position.X + PrimaryAxis, Position.Y);
  if PointOnArc(Pos) then
    ClipRect.CorrectByPoint(Pos);
  Pos.Init(Position.X, Position.Y - PrimaryAxis);
  if PointOnArc(Pos) then
    ClipRect.CorrectByPoint(Pos);
  Pos.Init(Position.X, Position.Y + PrimaryAxis);
  if PointOnArc(Pos) then
    ClipRect.CorrectByPoint(Pos);
end;

function TEllipseArc.SelectByCircle
  (
  PInfo: PPaint;
  Middle: TDPoint;
  ARadius: LongInt;
  const ObjType: TObjectTypes;
  Inside: Boolean
  )
  : Boolean;
var
  Angle1: Real;
  Point: TDPoint;
begin
  SelectByCircle := FALSE;
  if ot_Arc in ObjType then
  begin
    Point.Init(Position.X, Position.Y);
    Point.MoveAngle(BeginAngle, PrimaryAxis);
    if Point.Dist(Middle) <= ARadius then
      SelectByCircle := TRUE
    else
    begin
      Point.Init(Position.X, Position.Y);
      Point.MoveAngle(EndAngle, PrimaryAxis);
      if Point.Dist(Middle) <= ARadius then
        SelectByCircle := TRUE
      else
      begin
        Angle1 := Position.CalculateAngle(Middle);
        Point.Init(Position.X, Position.Y);
        Point.MoveAngle(Angle1, PrimaryAxis);
        SelectByCircle := PointOnArc(Point)
          and (Point.Dist(Middle) <= ARadius);
      end;
    end;
  end;
end;

function TEllipseArc.SelectByPoint
  (
  PInfo: PPaint;
  Point: TDPoint;
  const ObjType: TObjectTypes
  )
  : PIndex;
var
  Dist: Double;
  MDist: Double;
begin
  SelectByPoint := nil;
  if ot_Arc in ObjType then
  begin
    Dist := Position.Dist(Point);
    MDist := PInfo^.CalculateDraw(ciMaxDist);
    if (Abs(Dist - PrimaryAxis) < MDist)
      and PointOnArc(Point) then
      SelectByPoint := @Self;
  end;
end;

function TEllipseArc.SelectByPoly
  (
  PInfo: PPaint;
  Poly: Pointer;
  const ObjType: TObjectTypes;
  Inside: Boolean
  )
  : Boolean;
var
  Cnt: Integer;
  Point: TDPoint;

  function CalculateCuts
      (
      L1: PDPoint;
      L2: PDPoint
      )
      : Boolean; far;
  var
    D: Double;
    DX: Double;
    DY: Double;
    DXK: Double;
    DYK: Double;
    N: Double;
    P: Double;
    Point: TDPoint;
    T: Double;
    BResult: Boolean;
  begin
    BResult := FALSE;
    DX := L2^.X - L1^.X;
    DY := L2^.Y - L1^.Y;
    DXK := L1^.X - Position.X;
    DYK := L1^.Y - Position.Y;
    N := DX * DX + DY * DY;
    P := (DX * DXK + DY * DYK) / N;
    D := P * P + (1.0 * PrimaryAxis * PrimaryAxis - DXK * DXK - DYK * DYK) / N;
    if D >= 0 then
    begin
      D := Sqrt(D);
      T := -P + D;
      if (T > -Tolerance)
        and (T < 1.0 + Tolerance) then
      begin
        Point.Init(LimitToLong(L1^.X + T * DX), LimitToLong(L1^.Y + T * DY));
        if PointOnArc(Point) then
          BResult := TRUE;
      end;
      if not BResult then
      begin
        T := -P - D;
        if (T > -Tolerance)
          and (T < 1.0 + Tolerance) then
        begin
          Point.Init(LimitToLong(L1^.X + T * DX), LimitToLong(L1^.Y + T * DY));
          if PointOnArc(Point) then
            BResult := TRUE;
        end
      end;
    end;
    CalculateCuts := BResult;
  end;
begin
  SelectByPoly := FALSE;
  if (ot_Arc in ObjType)
    and ClipRect.IsVisible(PCPoly(Poly)^.ClipRect) then
  begin
    if Inside then
    begin
      if ClipRect.IsInside(PView(Poly)^.ClipRect) then
      begin
        Point.Init(Position.X, Position.Y);
        Point.MoveAngle(BeginAngle, PrimaryAxis);
        if PCPoly(Poly)^.PointInside(PInfo, Point) then
        begin
          Cnt := 0;
          while Cnt <= PCPoly(Poly)^.Data^.Count - 2 do
          begin
            if CalculateCuts(PCPoly(Poly)^.Data^.At(Cnt), PCPoly(Poly)^.Data^.At(Cnt + 1)) then
              Cnt := PCPoly(Poly)^.Data^.Count;
            Inc(Cnt);
          end;
          if Cnt <= PCPoly(Poly)^.Data^.Count then
            SelectByPoly := TRUE;
        end;
      end;
    end
    else
    begin
      Cnt := 0;
      while Cnt <= PCPoly(Poly)^.Data^.Count - 2 do
      begin
        if CalculateCuts(PCPoly(Poly)^.Data^.At(Cnt), PCPoly(Poly)^.Data^.At(Cnt + 1)) then
          Cnt := PCPoly(Poly)^.Data^.Count;
        Inc(Cnt);
      end;
      if Cnt > PCPoly(Poly)^.Data^.Count then
        SelectByPoly := TRUE
      else
      begin
        Point.Init(Position.X, Position.Y);
        Point.MoveAngle(BeginAngle, PrimaryAxis);
        if PCPoly(Poly)^.PointInside(PInfo, Point) then
          SelectByPoly := TRUE;
      end;
    end;
  end;
end;

function TEllipseArc.SelectBetweenPolys
  (
  PInfo: PPaint;
  Poly1: Pointer;
  Poly2: Pointer;
  const ObjType: TObjectTypes;
  Inside: Boolean
  )
  : Boolean;
var
  Cnt: Integer;
  Point: TDPoint;

  function CalculateCuts
      (
      L1: PDPoint;
      L2: PDPoint
      )
      : Boolean; far;
  var
    D: Double;
    DX: Double;
    DY: Double;
    DXK: Double;
    DYK: Double;
    N: Double;
    P: Double;
    Point: TDPoint;
    T: Double;
    BResult: Boolean;
  begin
    BResult := FALSE;
    DX := L2^.X - L1^.X;
    DY := L2^.Y - L1^.Y;
    DXK := L1^.X - Position.X;
    DYK := L1^.Y - Position.Y;
    N := DX * DX + DY * DY;
    P := (DX * DXK + DY * DYK) / N;
    D := P * P + (1.0 * PrimaryAxis * PrimaryAxis - DXK * DXK - DYK * DYK) / N;
    if D >= 0 then
    begin
      D := Sqrt(D);
      T := -P + D;
      if (T > -Tolerance)
        and (T < 1.0 + Tolerance) then
      begin
        Point.Init(LimitToLong(L1^.X + T * DX), LimitToLong(L1^.Y + T * DY));
        if PointOnArc(Point) then
          BResult := TRUE;
      end;
      if not BResult then
      begin
        T := -P - D;
        if (T > -Tolerance)
          and (T < 1.0 + Tolerance) then
        begin
          Point.Init(LimitToLong(L1^.X + T * DX), LimitToLong(L1^.Y + T * DY));
          if PointOnArc(Point) then
            BResult := TRUE;
        end
      end;
    end;
    CalculateCuts := BResult;
  end;
begin
  SelectBetweenPolys := FALSE;
  if (GetObjType in ObjType)
    and (ClipRect.IsVisible(PView(Poly1)^.ClipRect) or ClipRect.IsVisible(PView(Poly2)^.ClipRect)) then
  begin
    if Inside then
    begin
      if (ClipRect.IsInside(PView(Poly1)^.ClipRect) or ClipRect.IsInside(PView(Poly2)^.ClipRect)) then
      begin
        Point.Init(Position.X, Position.Y);
        Point.MoveAngle(BeginAngle, PrimaryAxis);
        if (PCPoly(Poly1)^.PointInside(PInfo, Point) xor PCPoly(Poly2)^.PointInside(PInfo, Point)) then
        begin
          Cnt := 0;
          while Cnt <= PCPoly(Poly1)^.Data^.Count - 2 do
          begin
            if CalculateCuts(PCPoly(Poly1)^.Data^.At(Cnt), PCPoly(Poly1)^.Data^.At(Cnt + 1)) then
              Cnt := PCPoly(Poly1)^.Data^.Count;
            Inc(Cnt);
          end;
          if Cnt <= PCPoly(Poly1)^.Data^.Count then
          begin
            Cnt := 0;
            while Cnt <= PCPoly(Poly2)^.Data^.Count - 2 do
            begin
              if CalculateCuts(PCPoly(Poly2)^.Data^.At(Cnt), PCPoly(Poly2)^.Data^.At(Cnt + 1)) then
                Cnt := PCPoly(Poly2)^.Data^.Count;
              Inc(Cnt);
            end;
            if Cnt <= PCPoly(Poly2)^.Data^.Count then
              SelectBetweenPolys := TRUE;
          end;
        end;
      end;
    end
    else
    begin
      Cnt := 0;
      while Cnt <= PCPoly(Poly1)^.Data^.Count - 2 do
      begin
        if CalculateCuts(PCPoly(Poly1)^.Data^.At(Cnt), PCPoly(Poly1)^.Data^.At(Cnt + 1)) then
          Cnt := PCPoly(Poly1)^.Data^.Count;
        Inc(Cnt);
      end;
      if Cnt > PCPoly(Poly1)^.Data^.Count then
        SelectBetweenPolys := TRUE
      else
      begin
        Cnt := 0;
        while Cnt <= PCPoly(Poly2)^.Data^.Count - 2 do
        begin
          if CalculateCuts(PCPoly(Poly2)^.Data^.At(Cnt), PCPoly(Poly2)^.Data^.At(Cnt + 1)) then
            Cnt := PCPoly(Poly2)^.Data^.Count;
          Inc(Cnt);
        end;
        if Cnt > PCPoly(Poly2)^.Data^.Count then
          SelectBetweenPolys := TRUE
        else
        begin
          Point.Init(Position.X, Position.Y);
          Point.MoveAngle(BeginAngle, PrimaryAxis);
          if (PCPoly(Poly1)^.PointInside(PInfo, Point) xor PCPoly(Poly2)^.PointInside(PInfo, Point)) then
            SelectBetweenPolys := TRUE;
        end;
      end;
    end;
  end;
end;

function TEllipseArc.SelectByPolyLine
  (
  PInfo: PPaint;
  Poly: Pointer;
  const ObjType: TObjectTypes
  )
  : Boolean;
var
  Cnt: Integer;

  function CalculateCuts
      (
      L1: PDPoint;
      L2: PDPoint
      )
      : Boolean; far;
  var
    D: Double;
    DX: Double;
    DY: Double;
    DXK: Double;
    DYK: Double;
    N: Double;
    P: Double;
    Point: TDPoint;
    T: Double;
    BResult: Boolean;
  begin
    BResult := FALSE;
    DX := L2^.X - L1^.X;
    DY := L2^.Y - L1^.Y;
    DXK := L1^.X - Position.X;
    DYK := L1^.Y - Position.Y;
    N := DX * DX + DY * DY;
    P := (DX * DXK + DY * DYK) / N;
    D := P * P + (1.0 * PrimaryAxis * PrimaryAxis - DXK * DXK - DYK * DYK) / N;
    if D >= 0 then
    begin
      D := Sqrt(D);
      T := -P + D;
      if (T > -Tolerance)
        and (T < 1.0 + Tolerance) then
      begin
        Point.Init(LimitToLong(L1^.X + T * DX), LimitToLong(L1^.Y + T * DY));
        if PointOnArc(Point) then
          BResult := TRUE;
      end;
      if not BResult then
      begin
        T := -P - D;
        if (T > -Tolerance)
          and (T < 1.0 + Tolerance) then
        begin
          Point.Init(LimitToLong(L1^.X + T * DX), LimitToLong(L1^.Y + T * DY));
          if PointOnArc(Point) then
            BResult := TRUE;
        end
      end;
    end;
    CalculateCuts := BResult;
  end;
begin
  SelectByPolyLine := FALSE;
  if (ot_Arc in ObjType)
    and ClipRect.IsVisible(PPoly(Poly)^.ClipRect) then
  begin
    Cnt := 0;
    while Cnt <= PPoly(Poly)^.Data^.Count - 2 do
    begin
      if CalculateCuts(PPoly(Poly)^.Data^.At(Cnt), PPoly(Poly)^.Data^.At(Cnt + 1)) then
        Cnt := PPoly(Poly)^.Data^.Count;
      Inc(Cnt);
    end;
    if Cnt > PPoly(Poly)^.Data^.Count then
      SelectByPolyLine := TRUE;
  end;
end;

procedure TEllipseArc.Store
  (
  S: TOldStream
  );
begin
  inherited Store(S);
  S.Write(BeginAngle, SizeOf(BeginAngle));
  S.Write(EndAngle, SizeOf(EndAngle));
end;

{****************************************************************************}
{ Function AngleDifference                                                   }
{----------------------------------------------------------------------------}
{ Berechnet die WinkelDifferenz zwischen Angle1 und Angle2 (Angle2-Angle1).  }
{****************************************************************************}

function AngleDifference
  (
  Angle1: Double;
  Angle2: Double
  )
  : Double;
begin
  if Angle2 < Angle1 then
    Angle2 := Angle2 + 2 * Pi;
  Angle1 := Angle2 - Angle1;
  if Angle1 < 0 then
    AngleDifference := Angle1 + 2 * Pi
  else
    AngleDifference := Angle1;
end;

procedure TEllipseArc.Draw
  (
  PInfo: PPaint;
  Clipped: Boolean
  );
var
  BPrimaryAxis: Double;
  XMiddle: Double;
  YMiddle: Double;
begin
  BPrimaryAxis := PInfo^.CalculateDispDouble(PrimaryAxis);
  PInfo^.ConvertToDispDouble(Position, XMiddle, YMiddle);
  if Clipped then
    PInfo^.ExtCanvas.ClippedArc(GrPoint(XMiddle, YMiddle), BPrimaryAxis,
      BeginAngle + PInfo^.ViewRotation, EndAngle + PInfo^.ViewRotation)
  else
    PInfo^.ExtCanvas.Arc(GrPoint(XMiddle, YMiddle), BPrimaryAxis,
      BeginAngle + PInfo^.ViewRotation, EndAngle + PInfo^.ViewRotation);
end;

function TEllipseArc.GiveBegAngle: Real;
begin
  GiveBegAngle := BeginAngle;
end;

function TEllipseArc.GiveEndAngle: Real;
begin
  GiveEndAngle := EndAngle;
end;

function TEllipseArc.Laenge: Double;
var
  DrawAngle: Real;
begin
  if BeginAngle < EndAngle then
    DrawAngle := EndAngle - BeginAngle
  else
    DrawAngle := 2 * Pi - BeginAngle + EndAngle;
  Laenge := PrimaryAxis / 100 * DrawAngle;
end;

//++ Glukhov Bug#199 Build#157 27.04.01
// Calculates a number of points to approximate the arc with the given inaccuracy

function TEllipseArc.ApproximatePointsNumber(Delta: Double = 0.0; bRel: Boolean = True): LongInt;
var
  dAlp, dNum: Double;
begin
  Result := 2;
  if bRel then
    Delta := Delta * PrimaryAxis;
    // Inaccuracy has to be less than radius
  if Delta <= 0.0 then
    Delta := PrimaryAxis / 256.0;
  if Delta > PrimaryAxis then
    Delta := PrimaryAxis;
  if Delta <= 1.0 then
    Delta := 1.0;
    // Calculates the sector angle

  if PrimaryAxis = 0 then
    exit;

  dNum := 1.0 - Delta / PrimaryAxis;
  dAlp := 2.0 * arccos(dNum);
  dNum := AngleDifference(BeginAngle, EndAngle);
  dNum := dNum / dAlp;
  Result := Trunc(dNum);
  if Result < dNum then
    Inc(Result);
  if Result < 2 then
    Result := 2;
end;

//------------
// Calculates a point of the arc approximation with the given inaccuracy
// Num >= 2; Idx should be 0..Num-1
// First and last Points correspond to BeginAngle and EndAngle
// Sign>=0 means Begin-End direction

function TEllipseArc.ApproximatePoint(Idx, Num: LongInt; Sign: Integer = 1): TDPoint;
var
  dAlp, dArc: Double;
begin
  Result.Init(Position.X, Position.Y);
  if Num < 2 then
    Num := 2;
  if Idx < 0 then
    Idx := 0;
  if Idx >= Num then
    Idx := Num - 1;
  if Sign < 0 then
    Idx := Num - Idx - 1;
  dArc := AngleDifference(BeginAngle, EndAngle);
  dAlp := BeginAngle + dArc * Idx / (Num - 1);
  Result.MoveAngle(dAlp, PrimaryAxis);
end;
//-- Glukhov Bug#199 Build#157 27.04.01

{========================================================================================}
{ TEllipseArc                                                                            }
{========================================================================================}

function TEllipseArc.SearchForPoint
  (
  PInfo: PPaint;
  InCircle: Pointer;
  Exclude: LongInt;
  const ObjType: TObjectTypes;
  var Point: TDPoint;
  var SnObje: PIndex
  )
  : Boolean;
var
  Pos: TDPoint;
begin
  SearchForPoint := FALSE;
  if (ot_Arc in ObjType) and ClipRect.IsVisible(PEllipse(InCircle)^.ClipRect) and (Index <> Exclude) then
  begin
    Pos.Init(PrimaryAxis, 0);
    Pos.Rotate(BeginAngle);
    Pos.Move(Position.X, Position.Y);
    if PEllipse(InCircle)^.PointInside(Pos) then
    begin
      Point := Pos;
      SearchForPoint := TRUE;
    end
    else
    begin
      Pos.Init(PrimaryAxis, 0);
      Pos.Rotate(EndAngle);
      Pos.Move(Position.X, Position.Y);
      if PEllipse(InCircle)^.PointInside(Pos) then
      begin
        Point := Pos;
        SearchForPoint := TRUE;
      end;
    end;
  end;
end;

procedure TSEllipseArc.Draw
  (
  PInfo: PPaint;
  Clipped: Boolean
  );
var
  BPrimaryAxis: Double;
  XMiddle: Double;
  YMiddle: Double;
begin
  BPrimaryAxis := PInfo^.ScaleDispDouble(PrimaryAxis);
  PInfo^.ScaleToDispDouble(Position, XMiddle, YMiddle);
  if Clipped then
    PInfo^.ExtCanvas.ClippedArc(GrPoint(XMiddle, YMiddle), BPrimaryAxis,
      BeginAngle + PInfo^.Angle + PInfo^.ViewRotation, EndAngle + PInfo^.Angle + PInfo^.ViewRotation)
  else
    PInfo^.ExtCanvas.Arc(GrPoint(XMiddle, YMiddle), BPrimaryAxis,
      BeginAngle + PInfo^.Angle + PInfo^.ViewRotation, EndAngle + PInfo^.Angle + PInfo^.ViewRotation);
end;

function TEllipseArc.SearchForNearestPoint
  (
  PInfo: PPaint;
  var ClickPos: TDPoint;
  InCircle: Pointer;
  Exclude: LongInt;
  const ObjType: TObjectTypes;
  var Point: TDPoint;
  var SnObje: PIndex
  )
  : Boolean;
var
  Pos: TDPoint;
begin
  Result := FALSE;
  if (ot_Arc in ObjType)
    and ClipRect.IsVisible(PEllipse(InCircle)^.ClipRect)
    and (Index <> Exclude) then
  begin
    Pos.Init(PrimaryAxis, 0);
       {Pos.Rotate(BeginAngle);}
    Pos.Move(Position.X, Position.Y);
    if PEllipse(InCircle)^.PointInside(Pos)
      and (ClickPos.Dist(Point) > ClickPos.Dist(Pos)) then
    begin
      Point := Pos;
      Result := TRUE;
    end
    else
    begin
      Pos.Init(PrimaryAxis, 0);
      Pos.Rotate(EndAngle);
      Pos.Move(Position.X, Position.Y);
      if PEllipse(InCircle)^.PointInside(Pos)
        and (ClickPos.Dist(Point) > ClickPos.Dist(Pos)) then
      begin
        Point := Pos;
        Result := TRUE;
      end;
    end;
  end;
end;

constructor TSEllipseArc.LoadOld   (  S: TOldStream  );
var
  Pos: TDPoint;
  Filled: Boolean;
begin
  TView.Load(S);
  S.Read(PrimaryAxis, SizeOf(PrimaryAxis));
  SecondaryAxis := PrimaryAxis;
  Angle := 0;
  Position.Load(S);
  S.Read(Filled, SizeOf(Filled));
  Pos.Load(S);
  BeginAngle := Position.CalculateAngle(Pos);
  Pos.Load(S);
  EndAngle := Position.CalculateAngle(Pos);
end;

const
  REllipse: TStreamRec = (
    ObjType: rn_Ellipse;
    VmtLink: TypeOf(TEllipse);
    Load: @TEllipse.Load;
    Store: @TEllipse.Store);

  RSEllipseOld: TStreamRec = (
    ObjType: rn_Circle;
    VmtLink: TypeOf(TSEllipse);
    Load: @TSEllipse.LoadOld;
    Store: @TSEllipse.Store);

  RSEllipse: TStreamRec = (
    ObjType: rn_SEllipse;
    VmtLink: TypeOf(TSEllipse);
    Load: @TSEllipse.Load;
    Store: @TSEllipse.Store);

  REllipseArc: TStreamRec = (
    ObjType: rn_EllipseArc;
    VmtLink: TypeOf(TEllipseArc);
    Load: @TEllipseArc.Load;
    Store: @TEllipseArc.Store);

  RSEllipseArcOld: TStreamRec = (
    ObjType: rn_Arc;
    VmtLink: TypeOf(TSEllipseArc);
    Load: @TSEllipseArc.LoadOld;
    Store: @TSEllipseArc.Store);

  RSEllipseArc: TStreamRec = (
    ObjType: rn_SEllipseArc;
    VmtLink: TypeOf(TSEllipseArc);
    Load: @TSEllipseArc.Load;
    Store: @TSEllipseArc.Store);
begin
  RegisterType(REllipse);
  RegisterType(RSEllipseOld);
  RegisterType(RSEllipse);
  RegisterType(REllipseArc);
  RegisterType(RSEllipseArcOld);
  RegisterType(RSEllipseArc);
end.

