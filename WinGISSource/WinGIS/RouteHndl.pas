Unit RouteHndl;

Interface

Type TRouteNewHandler   = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;

     TRouteLoadHandler  = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;

     TRouteSaveHandler  = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;

     TRouteQuitHandler  = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;

     TRouteSetVLTypeHandler = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;

     TCustomRouteHandler = Class(TProjMenuHandler)
      Private
       Function    GetRouteObject:TRouteObject;
       Procedure   FreeInspect;
      Public
       Property    RouteObject:TRouteObject read GetRouteObject;
     end;  

     TRouteUndoHandler = Class(TCustomRouteHandler)
       Procedure   OnStart; override;
     end;

     TRouteStartHandler = Class(TCustomRouteHandler)
       Procedure   OnStart; override;
     end;

     TRouteDestHandler = Class(TCustomRouteHandler)
       Procedure   OnStart; override;
     end;

     TRouteGoHandler = Class(TCustomRouteHandler)
       Procedure   OnStart; override;
     end;

     TRouteStopHandler = Class(TCustomRouteHandler)
       Procedure   OnStart; override;
     end;

     TRouteOneWayHandler = Class(TCustomRouteHandler)
       Procedure   OnStart; override;
     end;

     TRouteToggleHandler = Class(TCustomRouteHandler)
       Procedure   OnStart; override;
     end;

     TRouteClearHandler = Class(TCustomRouteHandler)
       Procedure   OnStart; override;
     end;

     TRouteSelectHandler = Class(TCustomRouteHandler)
       Procedure   OnStart; override;
     end;

     TRouteShowFormHandler = Class(TCustomRouteHandler)
       Procedure   OnStart; override;
     end;

     TRouteTimeHandler = Class(TCustomRouteHandler)
       Procedure   OnStart; override;
     end;

     TRouteWayHandler = Class(TCustomRouteHandler)
       Procedure   OnStart; override;
     end;

     TRouteOpenHandler = Class(TCustomRouteHandler)
       Procedure   OnStart; override;
     end;

     TRouteSaveHandler = Class(TCustomRouteHandler)
       Procedure   OnStart; override;
     end;

     TRouteMoneyHandler = Class(TCustomRouteHandler)
       Procedure   OnStart; override;
     end;

     TRouteInspectHandler = Class(TCustomRouteHandler)
       Procedure   OnStart; override;
     end;

     TRouteVlHandler = Class(TCustomRouteHandler)
       Procedure   OnStart; override;
     end;

     TRouteKLHandler = Class(TCustomRouteHandler)
       Procedure   OnStart; override;
     end;

     TRoutePontiHandler = Class(TCustomRouteHandler)
       Procedure   OnStart; override;
     end;

Implementation

{===============================================================================
| TRouteNewHandler
+==============================================================================}

Procedure TRouteNewHandler.OnStart;
  begin
    Project^.RouteMenu.OnNew;
  end;

{===============================================================================
| TRouteLoadHandler
+==============================================================================}

Procedure TRouteLoadHandler.OnStart;
  begin
    Project^.RouteMenu.OnLoad;
  end;

{===============================================================================
| TRouteSaveHandler
+==============================================================================}

Procedure TRouteSaveHandler.OnStart;
  begin
    Project^.RouteMenu.OnSave;
  end;

{===============================================================================
| TRouteQuitHandler
+==============================================================================}

Procedure TRouteQuitHandler.OnStart;
  begin
    Project^.RouteMenu.OnQuit;
  end;

{===============================================================================
| TRouteSetVLTypeHandler
+==============================================================================}

Procedure TRouteSetVLTypeHandler.OnStart;
  begin
    Project^.RouteMenu.OnSetVLType;
  end;

Function TCustomRouteHandler.GetRouteObject
   : TRouteObject;
  begin
    Result:=Project^.RouteMenu.RouteObject;
  end;    

Procedure TRouteUndoHandler.OnStart;
  begin
    if RouteObject<>NIL then begin
      FreeInspect;
      RouteObject.DeSelect;
    end;
  end;

Procedure TRouteStartHandler.OnStart;
  begin
    if RouteObject<>NIL then begin
      Project.SetActualMenu(mn_SelStart);
      RouteObject.Start:=-1;
    end;
  end;

Procedure TRouteDestHandler.OnStart;
  begin
    if RouteObject<>NIL then begin
      FreeInspect;
      Project^.SetActualMenu(mn_SelDest);
      RouteObject.Dest:=-1;
    end;
  end;

Procedure TRouteGoHandler.OnStart;
  begin
    if RouteObject<>NIL then begin
      FreeInspect;
      RouteObject.RunShortestPath;
    end;
  end;

Procedure TRouteStopHandler.OnStart;
  begin
    if RouteObject<>NIL then begin
      FreeInspect;
      RouteObject.SelectStop;
    end;
  end;

Procedure TRouteOneWayHandler.OnStart;
  begin
    if RouteObject<>NIL then begin
      FreeInspect;
      RouteObject.SelOneWay;
    end;
  end;

Procedure TRouteToggleHandler.OnStart;
  begin
    if RouteObject<>NIL then begin
      FreeInspect;
      RouteObject.ToggleOneWay;
    end;
  end;

Procedure TRouteClearHandler.OnStart;
  begin
    if RouteObject<> NIL then begin
      FreeInspect;
      Project^.DeselectAll(TRUE);
      RouteObject.Restore;
      RouteObject.DeleteLayer(TRouteObject(FRouteObj).Id_SelLayer);
      RouteObject.DeleteLayer(TRouteObject(FRouteObj).Id_RouteOnLayer);
      RouteObject.DeleteLayer(TRouteObject(FRouteObj).Id_ClipLayer);
      RouteObject.LastStartIndex:=0;
      RouteObject.LastDestIndex:=0;
      RouteObject.Start:=-1;
      RouteObject.Dest:=-1;
      Project^.Flag:=0;
      Project^.PInfo^.RedrawScreen(TRUE);
    end;
  end;

Procedure TRouteSelectHandler.OnStart;
  begin
    if TRouteObject(FRouteObj)<>NIL then begin
      FreeInspect;
      SelectOnRouteLayer;
    end;
  end;

Procedure TRouteShowFormHandler.OnStart;
  begin
    Project^.RouteMenu.OnSetVLType;
  end;

Procedure TRouteTimeHandler.OnStart;
  begin
    Project^.RouteMenu.OnSetVLType;
  end;

Procedure TRouteWayHandler.OnStart;
  begin
    Project^.RouteMenu.OnSetVLType;
  end;

Procedure TRouteOpenHandler.OnStart;
  begin
    Project^.RouteMenu.OnSetVLType;
  end;

Procedure TRouteSaveHandler.OnStart;
  begin
    Project^.RouteMenu.OnSetVLType;
  end;

Procedure TRouteMoneyHandler.OnStart;
  begin
    Project^.RouteMenu.OnSetVLType;
  end;

Procedure TRouteInspectHandler.OnStart;
  begin
    Project^.RouteMenu.OnSetVLType;
  end;

Procedure TRouteVlHandler.OnStart;
  begin
    Project^.RouteMenu.OnSetVLType;
  end;

Procedure TRouteKLHandler.OnStart;
  begin
    Project^.RouteMenu.OnSetVLType;
  end;

Procedure TRoutePontiHandler.OnStart;
  begin
    Project^.RouteMenu.OnSetVLType;
  end;

{===============================================================================
| Initialisierungs- und Terminierungscode
+==============================================================================}

Initialization;
  begin
    MenuFunctions.RegisterFromResource('RouteHndl','RouteHndl');
    TRouteNewHandler.Registrate('RouteNew');
    TRouteLoadHandler.Registrate('RouteLoad');
    TRouteSaveHandler.Registrate('RouteSave');
    TRouteQuitHandler.Registrate('RouteQuit');
    TRouteSetVLTypeHandler.Registrate('RouteSetVLType')
  end;

end.

