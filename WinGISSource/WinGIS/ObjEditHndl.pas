{******************************************************************************+
  Unit ObjEditHndl
--------------------------------------------------------------------------------
  Autor: Martin Forst
--------------------------------------------------------------------------------
  Menu-Handler for the edit-objects menu-function.

------------------------------------
  Unit hardly modified for polylines and polygones  by Brovak    23.01.2000
-------------------------------------
  Jun 2001 Added support for Arcs and Circles by Brovak
-------------------------------------
  Jul 2001 Added support for Points (ot_Pixel) by Brovak
-------------------------------------
  August 2001 Added support for Symbols, Texts ; added support for
  transparent select object and fixed layers       by Brovak
-------------------------------------
 Sepptember 2001   Some  modifications for support Attached
 and Drawn Along Texts     by Brovak
---------------------------------------
 October 2001   Support for ZoomOut with right doubleclick     by Brovak
+******************************************************************************}
Unit ObjEditHndl;

Interface

Uses AM_Def,AM_Proj,Classes,Controls,Graphics,MenuFn,MenuHndl,ProjHndl,Dialogs,GrTools,AM_Layer,Math;

Type TObjectEditMenuHandler  = Class(TProjMenuHandler)
      Private
       Procedure   CreateEditHandler;
      Public
       PolyList       : TGrPointList;
       LayerofSnapObject : Player;
       ObjectType:word;
       procedure Clear;
       Class Function HandlerType:Word; override;
       Function    OnMouseMove(Const X,Y:Double;Shift:TShiftState):Boolean; override;
       Function    OnMouseDown(Const X,Y:Double;Button:TMouseButton;Shift:TShiftState):Boolean; override;
       Function    OnMouseUp(Const X,Y:Double;Button:TMouseButton;Shift:TShiftState):Boolean; override;
       Procedure   OnStart; override;
       Procedure   OnKeyDown(var Key:Word;Shift:TShiftState); override;

     end;

Implementation

Uses AM_Child,AM_Index,AM_Poly,AM_Text,EditHndl1,PolyEdit1, am_cpoly,Am_Font,MultiLng,MUModule,
SysUtils,AM_Main,AM_Projo,Attachhndl,AM_View,AM_Circl,CircleEdit,ArcEdit,Am_Point,Am_Sym,SymbolEdit,Numtools,Windows
{++ IDB_UNDO}
     {$IFNDEF AXDLL} // <----------------- AXDLL
     , IDB_CallBacksDef
     {$ENDIF} // <----------------- AXDLL
{-- IDB_UNDO}

;

var TmpSize,TmpAngle:double;
    FlagFixed:boolean;
{==============================================================================}
{ TObjectEditHandler                                                           }
{==============================================================================}

Procedure TObjectEditMenuHandler.OnStart;
var TmpSnap:Pindex;
begin
with Project^ do //if pixel
begin;
  EscPressedTwo:=false;
If SnapItem<>nil then
 if SnapItem.GetObjType=ot_pixel
  then
   begin;
   MenuFunctions['EditObjects'].Checked:=false;
   MenuFunctions['EditPointMove'].Checked:=true;
   Exit;
   end;


end;
 FlagFixed:=false;

With Project^ do
 begin;
    Pinfo.EditPoly:=true;
    if OldSnapitem<>nil then
     begin;
//     TmpSnap:=OldSnapItem;
     DeselectAll(true);
     Layers^.TopLayer^.Select(PInfo,OldsnapItem);

     end;
 end;

 inherited OnStart;
  Status:=mhsNotFinished;

  with Project^ do
    begin;


      if OldSnapItem=nil then
          FindOldSnapItem;
      if OldsnapItem<>nil then
          SnapItem:=Layers^.IndexObject(PInfo, OldsnapItem);

      if SnapItem<>NIL then
         begin;

          //check all
        if CheckFixedOrNot=true then Exit;


         CreateEditHandler;
         ObjectType:=SnapItem.GetObjType;


         if (ObjectType=ot_poly) or (ObjectType=ot_cpoly)
          then
           begin;
            Pinfo.EditPoly:=true;
            EditVertexMode:=4;
            ActualMen:=mn_Select;
           end;
         end
         else SubHandler:=NIL;
    end;
  Visible:=TRUE;
  Project^.OldMenu:=-1000;
end;

Class Function TObjectEditMenuHandler.HandlerType:Word;
begin
  Result:=mhtReturnOrClear;
end;


Procedure  TObjectEditMenuHandler.OnKeyDown(var Key:Word;Shift:TShiftState);
begin;
{$IFNDEF AXDLL} // <----------------- AXDLL
 if Key=27   //Esc pressed
 then begin;
   if SubHandler<>nil then begin; Project^.Pinfo.EditPoly:=false; SubHandler:=nil end
    else begin;
     Project^.PInfo^.EditPoly:=false;
     Project^.SetActualMenu(mn_Select,true);
     end;
     end;
  inherited OnKeyDown(Key,Shift) ;
{$ENDIF} // <----------------- AXDLL
end;

Function TObjectEditMenuHandler.OnMouseMove(Const X,Y:Double;Shift:TShiftState):Boolean;
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
   if LayerofSnapObject<> Project^.Layers.TopLayer then
   Clear;
   Result:=inherited OnMouseMove(X,Y,Shift);
{$ENDIF} // <----------------- AXDLL
end;

Function TObjectEditMenuHandler.OnMouseDown(Const X,Y:Double;Button:TMouseButton;Shift:TShiftState):Boolean;
{$IFNDEF AXDLL} // <----------------- AXDLL
  Label GotoBack;
var Pos               : TDPoint;
    EditHandlerClass  : TMenuHandlerClass;
    Cnt : integer;
    Z:PPoly;
    Circle:PEllipse;
    Arc:PEllipseArc;
    Symbol:PSymbol;
    APoint:PGrPOint;
    APoint1:TDpoint;
    AText:PText;
    X1,Y1:double;
    Rect,Rect1:TDRECT;
    FlagTransp:boolean;
    OLdMen:integer;
    OldOldSnapitem:Pindex;
    OldOldTopLayer:PLayer;
    Point1,Point2,Point3,Point4:TGrPoint;
{$ENDIF} // <----------------- AXDLL
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  {$IFNDEF WMLT}
if Button=mbLeft then begin;

Project^.Pinfo.Editpoly:=true;
Pos.Init(LimitToLong(X),LimitToLong(Y));
if LayerofSnapObject<> Project^.Layers.TopLayer then
 Clear;
FlagTransp:= MenuFunctions['SelectTransparent'].Checked;

if ssDouble in Shift then //DoubleClick - Change object !
 begin;
 FlagFixed:=true;
  if SubHandler<>nil then //if Special frame exists
   begin;
    Apoint1.Init(0,0);
   Rect.InitByRect(Project^.SnapItem.ClipRect);
     Cnt:= Project^.Layers^.SelLayer.Data.GetCount;
     if Cnt=0 then
      begin;
      with Project^ do begin;
       if OldsnapItem<>nil then
          SnapItem:=Layers^.IndexObject(PInfo, OldsnapItem);
 //           Rect.InitByRect(Project^.SnapItem.ClipRect);

          ObjectType:=Snapitem.GetOBjType;
      if (ObjectType=ot_poly) or  (ObjectType=ot_cpoly) then z:=Ppoly(SnapItem);
      if (ObjectType=ot_circle) then Circle:= Pellipse(SnapItem);
      if (ObjectType=ot_arc) then Arc:= PellipseArc(SnapItem);
      if (ObjectType=ot_symbol) then Symbol:= PSymbol(SnapItem);
      if (ObjectType=ot_text) then AText:= PText(SnapItem);
                        end;
     end
     else


      With Project^ do
      begin;
     SnapItem:=Project^.GiveMeSourceObjectFromSelect(Project^.Layers^.SelLayer.Data.At(0)).Mptr;
     ObjectType:=Snapitem.GetOBjType;
      if (ObjectType=ot_poly) or  (ObjectType=ot_cpoly) then z:=Ppoly(SnapItem);
      if (ObjectType=ot_circle) then Circle:= Pellipse(SnapItem);
      if (ObjectType=ot_arc) then Arc:= PellipseArc(SnapItem);
      if (ObjectType=ot_symbol) then Symbol:= PSymbol(SnapItem);
      if (ObjectType=ot_text) then AText:= PText(SnapItem);
     end;

       If (WinGisMainForm.WeAreUsingUndoFunction[Project])
           AND
          (WinGisMainForm.UndoRedo_Man.AddObjectInUndoData(Pproj(Project),PView(Project^.SnapItem))) Then
          WinGisMainForm.UndoRedo_Man.SaveUndoData(PProj(Project), utUnChange);
        visible:=false;

      case ObjectType of

      ot_Poly,ot_Cpoly:
       begin;
        with SubHandler as TPolyEditor1 do
        for Cnt:=0 to Polygon.Count-1 do
         begin;
          Apoint1.X:=round(Polygon.PointPtrs[Cnt].X);
          Apoint1.Y:=round(Polygon.PointPtrs[Cnt].Y);
          Z.MovePoint(Cnt,Apoint1,false);
         end;
         z.CalculateClipRect;
         z.UpdateData;
         with Project^ do begin;
         DeselectAll(true);
         RedrawWithAllAttached(PProj(Project), PView(z), Rect );
         Project^.Pinfo.Editpoly:=false;
         end;
       end;


      ot_text:
        begin;
        with SubHandler as TSymbolEditor do
        begin;

         Point1:=SRefPosition;
         Point2:=MovedPointAngle(Point1,Width,Rotation);
         Point4:=MovedPointAngle(Point1,Height,Rotation+Pi/2);
         Point3:=MovedPointAngle(Point4,Width,Rotation);
           with Project^ do begin;
         AText.ExecObjEdit(Pinfo,Point1,Point2,Point3,Point4);
         DeselectAll(true);
         RedrawWithAllAttached(PProj(Project), PView(Atext), Rect );
                          end;
       end;
       end;

     ot_symbol:
        begin;
        with SubHandler as TSymbolEditor do
        begin;

         Symbol.Position.X:=Round(SRefPosition.X);
         Symbol.Position.Y:=Round(SRefPosition.Y);

           if DblCompare(TmpAngle,dblFromObject)<=0
            then Symbol.Angle:= TmpAngle
            else
                 Symbol.Angle:=Rotation;

         if  DblCompare(Symbol.Size,dblFromLayer)>=0
           then

             Symbol.Size:= Symbol.Size*(Width/TmpSize);
         Symbol.CalculateClipRect( Project^.Pinfo);
         with Project^ do begin;
            Project^.DeselectAll(true);
            RedrawWithAllAttached(PProj(Project), PView(symbol), Rect );
         end;
         if PProj(Project)^.PInfo^.IsMU then begin
            MU.LogUpdObjectData(PProj(Project)^.PInfo,PProj(Project)^.Layers^.TopLayer,Symbol);
         end;
       end;
       end;
      ot_circle:
        begin;
        with SubHandler as TCircleEditor do
        begin;
         Circle.Position.X:=Round(Middle.X);
         Circle.Position.Y:=Round(Middle.Y);
         Circle.PrimaryAxis:=Round(Radius);
         Circle.SecondaryAxis:=Round(Radius);
         Circle.CalculateClipRect;
         Circle.UpdateData;
        end;
         Project^.DeselectAll(true);
         RedrawWithAllAttached(PProj(Project), PView(Circle), Rect );
         end;

          ot_arc:
        begin;
        with SubHandler as TArcEditor do
        begin;
         Arc.Position.X:=Round(Middle.X);
         Arc.Position.Y:=Round(Middle.Y);
         Arc.PrimaryAxis:=Round(Radius);
         Arc.SecondaryAxis:=Round(Radius);
         Arc.BeginAngle:=BegAng;
         Arc.EndAngle:=EndAng;
         Arc.CalculateClipRect;
         Arc.UpdateData;
        end;
         Project^.DeselectAll(true);
         RedrawWithAllAttached(PProj(Project), PView(Arc), Rect );
         end;

       end;  //case

        ProcDBSendObject(PProj(Project),Project^.SnapItem,'UPD');

        if WinGisMainForm.WeAreUsingTheIDB[Project] then begin
           WinGISMainForm.IDB_Man.UpdateCalcFields(Project,Project^.Layers^.TopLayer,Project^.SnapItem^.Index);
        end;

    SubHandler:=Nil;
    Exit;
   end //SubHandler<>nil
  end; //Double pressed

 with Project^ do begin;
  OldOldSnapItem:=OldSnapItem;//:=SelectByPoint(Pos,[ot_Poly,ot_CPoly,ot_Circle,ot_arc,ot_pixel,ot_symbol,ot_text]);
  OldOldTopLayer:=Project^.Layers.TopLayer;//  GiveMeSourceObjectFromSelect(Layers^.SelLayer.Data.At(0));
                  end;

  Result:=inherited OnMouseDown(X,Y,Button,Shift);

  if not Result then with Project^ do
    begin
     DeselectAll(true);
      if Project^.Layers.TranspLayer=true then
       begin;
        OldMen:=ActualMen;
        ActualMen:=mn_Select;
       end;
         if Project^.Layers.TranspLayer=true then
        SnapItem:=SelectByPoint(Pos,[ot_Poly,ot_CPoly,ot_Circle,ot_arc,ot_pixel,ot_symbol,ot_text],true)
         else
        SnapItem:=SelectByPoint(Pos,[ot_Poly,ot_CPoly,ot_Circle,ot_arc,ot_pixel,ot_symbol,ot_text]);

        OldSnapItem := GiveMeSourceObjectFromSelect(Layers^.SelLayer.Data.At(0));
//        exit;


     if Project^.Layers.TranspLayer=true then
        begin;
         ActualMen:=OldMen;
         LayerofSnapObject:=Project^.Layers.TopLayer;
        end;
    if not (Layers^.TopLayer^.GetState(sf_Fixed)) then
       begin
//        FindOldSnapItem;
  //      DeselectAll(true);

GotoBack:  if OldSnapItem<>NIL then
           begin;
           SnapItem:=Layers^.IndexObject(PInfo, OldsnapItem);
            if CheckFixedOrNot=true then Exit;
            EscPressedTwo:=false;
             if (SnapItem.GetObjType<>ot_Pixel)
              then CreateEditHandler
              else begin;
               if SnapItem.GetObjType=ot_Pixel then
                begin;
          //if it is Point - edit => EditPointMove Function
                DeselectAll(true);
                MenuFunctions['EditPointMove'].Execute;
                MsgLeftClick(PPixel(SnapItem)^.Position);
                Exit;
               end;
                  end;
      end
       else
        begin;
        Project^.Pinfo.Editpoly:=false;
        SubHandler:=NIL;
        ActualMen:=mn_Select;
        end;
       end

      else  //Fixed
        begin;
        Project^.Layers.TopLayer:=OldOldTopLayer;
        OldSnapItem:=OldOLdSnapItem;
        
        MessageBeep(MB_ICONEXCLAMATION);
        if FlagFixed=false
         then
          MessageDlg(MlgStringList['Main',1300],mtWarning,[mbOk],0);
        FlagFixed:=true;
        Goto GotoBack;
        end;

        DeselectAll(true);



    Result:=TRUE;
  end;

                                    end; //buttonleft
  {$ENDIF}
{$ENDIF} // <----------------- AXDLL
end;

Function TObjectEditMenuHandler.OnMouseUp(Const X,Y:Double;Button:TMouseButton;Shift:TShiftState):Boolean;
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  if LayerofSnapObject<> Project^.Layers.TopLayer then
  Clear;
  Result:=inherited OnMouseUp(X,Y,Button,Shift);
{$ENDIF} // <----------------- AXDLL
end;

Procedure TObjectEditMenuHandler.CreateEditHandler;
var Cnt            : Integer;
    Last           : Integer;
    TmpPOint       : TGrPOint;
    A,B            : TGrPoint;
    DX,DY,SinAngle,CoSangle:extended;
    Point1,POint2,Point3,Point4:TGrPoint;
    APoint:PGrPOint ;
    APoly           : PCPoly;
    bRotate, bResize, bSimilar, bMove:boolean;
 begin

  with Project^ do if SnapItem<>NIL then case SnapItem^.GetObjType of
    ot_Text  : begin
      SubHandler:=TSymbolEditor.Create(Self);
      with SubHandler as TSymbolEditor do begin
       PText(SnapItem).InitObjEdit(Pinfo,Point1,Point2,Point3,Point4,bRotate, bResize, bSimilar, bMove);
       Options:=[];
        if bRotate then Options:=Options+[ehoRotate];
        if bResize then Options:=Options+[ehoSize]+[ehoKeepAspect];
        if bMove then Options:=Options+[ehoMoveInside];

          XPosition:=Point1.X;
          YPosition:=Point1.Y;
          Width:= GrPointDistance(Point1,Point2);
          Height:=GrPointDistance(Point1,Point4);
          Rotation:=LineAngle(Point1,Point2);

          TmpAngle:=PText(SnapItem)^.Angle;
          ObjectTypeForEdit:=0;
          //center
          LineIntersection(Point1,Point3,Point2,Point4,Center,TmpPOint);

          with PText(SnapItem)^  do
           begin;
          SRefPosition.X:= Point1.X;//Pos.X;
          SRefPosition.Y:= Point1.Y;//Pos.Y;
             end;
           PolyList:=TGrPointList.Create;            
           PolyList.Capacity:=3;
           PolyList.Clear;
           APoly:=New(PCPoly, Init);
         //create triangle - for show rotation of text
           with Apoly^ do
           begin;
              PolyList.Add(Point1);
              TmpPoint:=MovedPointAngle(Point1,Height,Rotation+PI/2);
              TmpPoint:=MovedPointAngle(TmpPoint,Width/2,Rotation);
              PolyList.Add(TmpPoint);
              PolyList.Add(Point2);
              end;
         Polygon:=PolyList; Dispose(APoly, Done);
        Visible:=TRUE;
        end;
      end;
    ot_Image: begin
      end;

    ot_Symbol: begin;

        SubHandler:=TSymbolEditor.Create(Self);
         with SubHandler as TSymbolEditor do
          begin;
        PInfo.EditPoly:=false;
          Options:=[ehoMoveInside,ehoKeepAspect];
            with PSymbol(SnapItem)^  do begin
               ObjectTypeForEdit:=1;
               TmpAngle:=PSymbol(SnapItem)^.Angle;

               if DblCompare(Size,dblFromLayer)>=0 then begin
                  Options:=Options+[ehoSize, ehoOnlySymmetric];
               end;

               APoly:=New(PCPoly, Init);
               GetBorder(PInfo, APoly);
               PSymbol(SnapItem)^.Angle:=0;
               PSymbol(SnapItem)^.CalculateClipRect(Pinfo);
               //center
               Center.X:=(ClipRect.B.X+ClipRect.A.X)/2;
               Center.Y:=(ClipRect.B.Y+ClipRect.A.Y)/2;

               Width:= abs(ClipRect.B.X-ClipRect.A.X);
               TmpSize:=Width;
               Height:=abs(ClipRect.B.Y-ClipRect.A.Y);

               TmpPoint.X:=ClipRect.A.X;
               TmpPoint.Y:=ClipRect.A.Y;
                PSymbol(SnapItem)^.Angle:=TmpAngle;

             SRefPosition.X:=Position.X;
             SRefPosition.Y:=Position.Y;
               end;


            with TmpPOint do begin;
        Point1.X:= PDPoint(APoly.Data.At(0)).X;
        Point1.Y:= PDPoint(APoly.Data.At(0)).Y;

        XPosition:= Point1.X;
        YPOsition:= Point1.Y;
           end;

             if DblCompare(TmpAngle,dblFromObject)<=0 then begin
                Rotation:=0;
                TmpAngle:=0;
             end
             else
                Rotation:=TmpAngle;

       Options:=Options+[ehoRotate];
       PolyList:=TGrPointList.Create;

       PolyList.Capacity:=3;
       PolyList.Clear;

        //create triangle - for show rotation of symbol
           with Apoly^ do
           begin;

              PolyList.Add(Point1);

              Point2:=MovedPointAngle(Point1,Height,TmpAngle+PI/2);
              Point2:=MovedPointAngle(Point2,Width/2,TmpAngle);
              PolyList.Add(Point2);
              Point2:=MovedPointAngle(Point1,Width,TmpAngle);
              PolyList.Add(Point2);
              end;

         Polygon:=PolyList;


       Dispose(APoly, Done);

    VISIBLE:=true;
         end;
      end;
    ot_Circle: begin
          SubHandler:=TCircleEditor.Create(Self);
            with SubHandler as TCircleEditor do
             begin;
               Options:=[ehoSize,ehoMoveInside, ehoKeepAspect,ehoOnlySymmetric];
                with PEllipse(SnapItem)^  do
                begin;
                Middle:=GrPoint(Position.X,Position.Y);
                Radius:=PrimaryAxis;
              //  DeselectAll(true);
                end;
                XPosition:=Middle.X-Radius;
                YPosition:=Middle.Y-Radius;
                Width:=Radius*2;
                Height:=Radius*2;
                Visible:=TRUE;
                end;

      end;
       ot_arc: begin
          SubHandler:=TArcEditor.Create(Self);
            with SubHandler as TArcEditor do
             begin;
               Options:=[ehoSize,ehoKeepAspect,ehoRotate,ehoMoveInside,ehoOnlySymmetric];
                with PEllipseArc(SnapItem)^  do
                begin;
                Middle:=GrPoint(Position.X,Position.Y);
                Radius:=PrimaryAxis;
                BegAng:=BeginAngle;
                EndAng:=EndAngle;
                FViewRotation:=Project^.PInfo.ViewRotation;
                DeselectAll(true);
                end;
                XPosition:=Middle.X-Radius;
                YPosition:=Middle.Y-Radius;
                Width:=Radius*2;
                Height:=Radius*2;

                Visible:=TRUE;
                 // MenuFunctions.GroupEnabled[1017]:=false;
                end;

      end;
    ot_Poly,
    ot_CPoly : begin

        SubHandler:=TPolyEditor1.Create(Self);
        PolyList:=TGrPointList.Create;
        with PPoly(SnapItem)^ do begin;
          with SubHandler as TEditHandler1 do  Options:=[ehoSize, ehoSizeMultiple,  {ehoSizeSymmetric ,} ehoRotate,ehoMoveInside {,EhoMoveBorder}];
          if SnapItem^.GetObjType=ot_CPoly then Last:=Data^.Count-1
          else Last:=Data^.Count;
          PolyList.Clear;
          PolyList.Capacity:=Last;
          for Cnt:=0 to Last-1 do PolyList.Add(GrPoint(PDPoint(Data^.At(Cnt))^.X,
              PDPoint(Data^.At(Cnt))^.Y));
        end;
        Pinfo.EditPoly:=true;
        with SubHandler as TPolyEditor1 do begin
          Closed:=SnapItem^.GetObjType=ot_CPoly;
          Polygon:=PolyList;
          Visible:=TRUE;
        end;
      end;
  end;
  LayerOfSnapObject:=Project^.Layers.TopLayer;
  {$IFNDEF AXDLL} // <----------------- AXDLL
  if SubHandler<>nil then Self.FAllowDblclick:=true;
  {$ENDIF} // <----------------- AXDLL
end;

Procedure TObjectEditMenuHandler.Clear;
 begin;
 // visible:=false;
 // with SubHandler as TEditHandler1 do
 // FrameVisible:=false;
  SubHandler:=nil;
  Project^.Snapitem:=nil;
  Project^.OldSnapItem:=nil;
 //  visible:=false;
 end;


{==============================================================================+
  Initialization
+==============================================================================}

Initialization
  // register menu-handler
  MenuHandlers.RegisterMenuHandler('EditObjects',TObjectEditMenuHandler);

end.

