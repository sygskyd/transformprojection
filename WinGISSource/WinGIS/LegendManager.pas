Unit LegendManager;

Interface

Uses Windows,Messages,SysUtils,Classes,Graphics,Controls,Dialogs,Forms,Menus,
     ProjHndl,RollUp,StdCtrls,TreeView,WCtrls,AM_Proj,AM_Sight,ExtCtrls,ActnList,
     ComCtrls, ColumnList, ImgList, MultiLng, InplEdit, Validate,AM_Paint,
     AM_Layer,AM_Group, Combobtn;

Type TLegendManagerRollupForm  = Class(TProjRollupForm)
       ActionList       : TActionList;
       ActivateAction   : TAction;
       LegendsSheet     : TTabSheet;
       MlgSection       : TMlgSection;
       NewViewAction    : TAction;
       PageControl      : TPageControl;
       PropertiesAction : TAction;
       RemoveViewAction : TAction;
       Rollup           : TRollup;
       UpdateLayersAction: TAction;
       UpdateViewAction : TAction;
       UpdateAllAction  : TAction;
       LegendeSymbols   : TImage;
       LegendsListbox   : TColumnListbox;
       ImageList        : TImageList;
       Label1           : TWLabel;

       MenuBtn          : TPopupMenuBtn;
       LegendsPopupMenu : TPopupMenu;
       NewLegendAction  : TAction;
       RenameLegendAction: TAction;
       CopyLegendAction : TAction;
       DeleteLegendAction: TAction;
       PropertiesLegendAction: TAction;
       NewLegendMenu    : TMenuItem;
       RenameLegendMenu : TMenuItem;
       CopyLegendMenu   : TMenuItem;
       DeleteLegendMenu : TMenuItem;
       PropertiesLegendMenu: TMenuItem;
       N4               : TMenuItem;
       N5               : TMenuItem;
       TitleCombo       : TComboBox;
       LegNameLabel     : TWLabel;
       Procedure   ActivateActionExecute(Sender: TObject);
       Procedure   FormResize(Sender: TObject);
       Procedure   FormShow(Sender: TObject);
       Procedure   LegendsSheetResize(Sender: TObject);
       Procedure   NewViewActionExecute(Sender: TObject);
       Procedure   PropertiesActionExecute(Sender: TObject);
       Procedure   PropertiesMenuClick(Sender: TObject);
       Procedure   RemoveViewActionExecute(Sender: TObject);
       Procedure   UpdateLayersActionExecute(Sender: TObject);
       Procedure   UpdateAllActionExecute(Sender: TObject);
       Procedure   UpdateViewActionExecute(Sender: TObject);
       Procedure   UpdateViewMenuClick(Sender: TObject);
       Procedure   ViewsListboxClick(Sender: TObject);
       Procedure   ViewsListboxDblClick(Sender: TObject);
       Procedure   ViewsInplaceEditorEditing(Sender: TObject; Index: Integer;
                       var AllowEdit: Boolean; var Text: String);
       Procedure   ViewsInplaceEditorValidate(Sender: TObject; Index: Integer;
                       const Text: String; var Valid: Boolean);
       Procedure LegendsListboxColumns1Paint(Control: TWinControl;
                       Index: Integer; Rect: TRect; State: TOwnerDrawState);
       Procedure LegendsListboxColumns0Paint(Control: TWinControl;
                       Index: Integer; Rect: TRect; State: TOwnerDrawState);
       Procedure FormCreate(Sender: TObject);
       Procedure FormDestroy(Sender: TObject);
    procedure CopyLegendMenuClick(Sender: TObject);
    procedure TitleComboClick(Sender: TObject);
    procedure NewLegendActionExecute(Sender: TObject);
    procedure RenameLegendActionExecute(Sender: TObject);
    procedure CopyLegendActionExecute(Sender: TObject);
    procedure DeleteLegendActionExecute(Sender: TObject);
    procedure PropertiesLegendActionExecute(Sender: TObject);
      Private
       FImages     : TWImageList;
       // for symbol drawing...
       FPInfo      : PPaint;
       Function    AddTreeItem(Parent:TTreeEntry;const ACaption:String):TTreeEntry;
       Procedure   CreateViewSubtree(Parent:TTreeEntry;Sight:PSight);
       Function    GetSelectedView:PSight;
       Procedure   UpdateEnabled;
       Procedure   UpdateViewsList;
       // legend functions start
       Procedure   DrawSymbol(AIndex:Integer;ARect:TRect);
       Procedure   DrawColumnText(Rect:TRect;State:TOwnerDrawState;Text:String);
       procedure   DrawDlgLine(Rect:TRect;Layer:pLayer);
       procedure   DrawDlgFilledRect(Rect:TRect;Layer:pLayer);
       procedure   DrawDlgSymbol(Symbol:pSGroup;Rect:TRect;Layer:pLayer);
       // legends functions end
      Protected
       Procedure   SetProject(AProject:PProj); override;
      Public
       Procedure   UpdateLegends;
       Procedure   UpdateViews;
     end;

Implementation

{$R *.DFM}

Uses AM_Def,AM_ProjO,CommonResources,GrTools
     {$IFNDEF WMLT}
     ,NewViewDlg,ViewPropertiesDlg
     {$ENDIF}
     ,NumTools,TreeList,
     UserIntf,Legend,Styledef,XStyles,XLines,XFills,AM_Obj,
     ListHndl,LegendDlg;

{==============================================================================+
  TViewManagerRollup
+==============================================================================}

Function TLegendManagerRollupForm.AddTreeItem(Parent:TTreeEntry;const ACaption:String):TTreeEntry;
begin
  Result:=TTreeEntry.Create;
  Result.Caption:=ACaption;
  Result.Bitmap:=-3;
  Parent.Add(Result);
end;

{******************************************************************************+
  TViewManagerRollup.CreateViewSubtree
--------------------------------------------------------------------------------
  Creates the sub-entries for a view-tree.
+******************************************************************************}
Procedure TLegendManagerRollupForm.CreateViewSubtree(Parent:TTreeEntry;Sight:PSight);
var Item           : TTreeEntry;
    Cnt            : Integer;
    SightData      : PSightLData;
    Layer          : PLayer;
begin
  if not RectEmpty(Sight^.ViewRect) then begin
    Item:=AddTreeItem(Parent,'Range');
    Item.Expanded:=FALSE;
    AddTreeItem(Item,'X');
    AddTreeItem(Item,'Y');
    AddTreeItem(Item,'Width');
    AddTreeItem(Item,'Height');
    AddTreeItem(Item,'Rotation');
  end;
  if Sight^.LayersData<>NIL then begin
    Item:=AddTreeItem(Parent,'Layer');
    Item.Expanded:=FALSE;
    for Cnt:=0 to Sight^.LayersData^.Count-1 do begin
      SightData:=Sight^.LayersData^.At(Cnt);
      if SightData^.LIndex<>0 then begin
        Layer:=Project^.Layers^.IndexToLayer(SightData^.LIndex);
        if Layer<>NIL then AddTreeItem(Item,PToStr(Layer^.Text));
      end;
    end;
  end;
end;

Procedure TLegendManagerRollupForm.UpdateViewsList;
begin
end;

Procedure TLegendManagerRollupForm.UpdateViews;
begin
end;

{******************************************************************************+
  Procedure TViewManagerRollup.SetProject
--------------------------------------------------------------------------------
  Called by the user-interface if the active project changes.
+******************************************************************************}
Procedure TLegendManagerRollupForm.SetProject(AProject:PProj);
begin
  inherited SetProject(AProject);
  UpdateViewsList;
  UpdateLegends;
end;

{==============================================================================+
  Initialisation- and termination-code
+==============================================================================}

Procedure TLegendManagerRollupForm.FormResize(Sender: TObject);
begin
  // show tabs
  {if PageControl.Parent<>NIL then PageControl.BoundsRect:=
    InflatedRect(PageControl.Parent.ClientRect,-2,-2);}
  // hide tabs
  if PageControl.Parent<>NIL then PageControl.BoundsRect:=
    InflatedWidthHeight(PageControl.Parent.ClientRect,6,6,25,6);
end;

{******************************************************************************+
  TViewManager.SelectedView
--------------------------------------------------------------------------------
  Returns the tree-entry representing the currently selected view. If no
  view selected NIL is returned.
+******************************************************************************}
Function TLegendManagerRollupForm.GetSelectedView:PSight;
begin
end;

Procedure TLegendManagerRollupForm.UpdateViewMenuClick(Sender: TObject);
begin
end;

{******************************************************************************+
  Procedure TViewManagerRollup.UpdateEnabled
--------------------------------------------------------------------------------
  Updates the enabled-status of the manager-controls.
+******************************************************************************}
Procedure TLegendManagerRollupForm.UpdateEnabled;
var AEnable        : Boolean;
begin
  AEnable:=Project<>NIL;
  SetControlGroupEnabled(LegendsListbox,AEnable);
  MenuBtn.Enabled:=AEnable;
  if (Project <> NIL) and (Project^.Legends.Count > 0) and (Project^.Legends.CurrentLegendIndex <> -1) then AEnable:=TRUE
  else AEnable:=FALSE;
  RenameLegendAction.Enabled:=AEnable;
  CopyLegendAction.Enabled:=AEnable;
  DeleteLegendAction.Enabled:=AEnable;
  ActivateAction.Enabled:=AEnable;
  UpdateViewAction.Enabled:=AEnable;
  UpdateAllAction.Enabled:=AEnable;
  UpdateLayersAction.Enabled:=AEnable;
//  RemoveMenu.Enabled:=AEnable;
end;

Procedure TLegendManagerRollupForm.ViewsListboxClick(Sender: TObject);
begin
  UpdateEnabled;
end;

Procedure TLegendManagerRollupForm.PropertiesMenuClick(Sender: TObject);
begin
end;

{******************************************************************************+
  TViewManagerRollup.ActivateActionExecute
--------------------------------------------------------------------------------
  Activates the currently selected view. Called by the corresponding menu.
+******************************************************************************}
Procedure TLegendManagerRollupForm.ActivateActionExecute(Sender: TObject);
var Sight          : PSight;
begin
  // get the currently selected view
  Sight:=GetSelectedView;
  if Sight<>NIL then ProcSetSight(Project,Sight);
end;

Procedure TLegendManagerRollupForm.ViewsListboxDblClick(Sender: TObject);
begin
  ActivateAction.Execute;
end;

{******************************************************************************+
  Procedure TViewManagerRollup.UpdateLayersActionExecute
--------------------------------------------------------------------------------
  Updates the layer-setting of the currently selected view with the
  current layer-priority.
+******************************************************************************}
Procedure TLegendManagerRollupForm.UpdateLayersActionExecute(Sender: TObject);
var Sight          : PSight;      
begin
  Sight:=GetSelectedView;
  if Sight<>NIL then begin
    Sight^.SetLayers(Project^.Layers);
    Project^.SetModified;
    UpdateViewsList;
  end;
end;

{******************************************************************************+
  TViewManagerRollup.UpdateViewMenuClick
--------------------------------------------------------------------------------
  Updates the view-rectangle stored in the currently selected view with the
  current display-view.
+******************************************************************************}
Procedure TLegendManagerRollupForm.UpdateViewActionExecute(Sender: TObject);
var Sight          : PSight;
begin
  Sight:=GetSelectedView;
  if Sight<>NIL then begin
    Sight^.ViewRect:=Project^.PInfo^.GetCurrentScreen;
    Project^.SetModified;
    UpdateViewsList;
  end;
end;

{******************************************************************************+
  TViewManagerRollup.UpdateAllActionExecute
--------------------------------------------------------------------------------
  Updates the view-rectangle and the layer-information stored in the currently
  selected view.
+******************************************************************************}
Procedure TLegendManagerRollupForm.UpdateAllActionExecute(Sender: TObject);
var Sight          : PSight;
begin
  Sight:=GetSelectedView;
  if Sight<>NIL then begin
    Sight^.SetLayers(Project^.Layers);
    Sight^.ViewRect:=Project^.PInfo^.GetCurrentScreen;
    Project^.SetModified;
    UpdateViewsList;
  end;
end;

{******************************************************************************+
  TViewManagerRollupForm.PropertiesActionExecute
--------------------------------------------------------------------------------
  Shows the properties-dialog for the currently selected view.
+******************************************************************************}
Procedure TLegendManagerRollupForm.PropertiesActionExecute(Sender: TObject);
{$IFNDEF WMLT}
var Dialog         : TViewPropertiesDialog;
    Sight          : PSight;
    WasDefault     : Boolean;
    DefaultView    : PSight;
    Index          : Integer;
{$ENDIF}
begin
{$IFNDEF WMLT}
  Sight:=GetSelectedView;
  if Sight<>NIL then begin
    Dialog:=TViewPropertiesDialog.Create(Self);
    try
      Dialog.Project:=Project;
      Dialog.Sight:=Sight;
      Index:=Project^.Sights^.IndexOf(Sight);
      if Project^.Sights^.Default<0 then DefaultView:=NIL
      else DefaultView:=Project^.Sights^.At(Project^.Sights^.Default);
      WasDefault:=Project^.Sights^.IndexOf(Sight)=Project^.Sights^.Default;
      Dialog.DefaultView:=WasDefault;
      if Dialog.ShowModal=mrOK then begin
        if Dialog.NameChanged then begin
          Project^.Sights^.AtDelete(Index);
          Project^.Sights^.Insert(Sight);
          Index:=Project^.Sights^.IndexOf(Sight);
          {$IFNDEF WMLT}
          ProcDBSendViews(Project);
          {$ENDIF}
        end;
        if Dialog.DefaultView then Project^.Sights^.Default:=Index
        else if WasDefault then Project^.Sights^.Default:=-1
        else if DefaultView<>NIL then Project^.Sights^.Default:=
            Project^.Sights^.IndexOf(DefaultView);
        if Dialog.Modified then Project^.SetModified;
        UpdateViewsList;
      end;
    finally
      Dialog.Free;
    end;
  end;
{$ENDIF}
end;

{******************************************************************************+
  TViewManagerRollupForm.NewViewActionExecute
--------------------------------------------------------------------------------
  Creates a new view. Displays a dialog-box to enter the name and the
  options for the new view.
+******************************************************************************}
Procedure TLegendManagerRollupForm.NewViewActionExecute(Sender: TObject);
{$IFNDEF WMLT}
var Dialog         : TNewViewDialog;
{$ENDIF}
begin
{$IFNDEF WMLT}
  Dialog:=TNewViewDialog.Create(Self);
  try
    Dialog.Project:=Project;
    if Dialog.ShowModal=mrOK then begin
      UpdateViewsList;
      Project^.SetModified;
    end;
  finally
    Dialog.Free;
  end;
{$ENDIF}  
end;

Procedure TLegendManagerRollupForm.LegendsSheetResize(Sender: TObject);
begin
  // adjust width
  TitleCombo.Width:=Max(0,LegendsSheet.Width-16);
  // adjust width and heigth
  LegendsListbox.Width:=Max(0,LegendsSheet.Width-16);
  LegendsListbox.Top:=32;
  LegendsListbox.Height:=Max(0,LegendsSheet.Height-40);
  // if hide tabs
  TitleCombo.Top:=23;
  LegendsListbox.Top:=LegendsListbox.Top+15;
  LegendsListbox.Height:=LegendsListbox.Height-15;
  MenuBtn.Left:=LegendsSheet.Width-26;
  // end if hide tabs
end;

Procedure TLegendManagerRollupForm.RemoveViewActionExecute(Sender: TObject);
var Sight          : PSight;
    Index          : Integer;
begin
  Sight:=GetSelectedView;                    
  if (Sight<>NIL) and (MessageDialog(Format(MlgSection[12],[PToStr(Sight^.Name)]),
      mtConfirmation,[mbYes,mbNo],0)=mrYes) then begin
    Index:=Project^.Sights^.IndexOf(Sight);
    Project^.Sights^.AtFree(Index);
    if Index=Project.Sights.Default then Project.Sights.Default:=-1
    else if Index<Project.Sights.Default then Dec(Project^.Sights^.Default);
    Project^.SetModified;
    UpdateViewsList;
  end;
end;

Procedure TLegendManagerRollupForm.FormShow(Sender: TObject);
begin
//  ViewsInplaceEditor.Rehook;
  UpdateEnabled;
end;

Procedure TLegendManagerRollupForm.ViewsInplaceEditorEditing(Sender:TObject;
  Index:Integer; var AllowEdit:Boolean; var Text:String);
begin
  Text:=PToStr(PSight(Project^.Sights^.At(Index))^.Name);
end;

Procedure TLegendManagerRollupForm.ViewsInplaceEditorValidate(
  Sender: TObject; Index: Integer; const Text: String; var Valid: Boolean);
var Sight          : PSight;
begin
  Sight:=Project^.Sights^.At(Index);
  Valid:=TRUE;
  if (AnsiCompareText(PToStr(Sight^.Name),Text)<>0)
      and (Project^.Sights^.NamedSight(Text)<>NIL) then begin
    MessageDialog(MlgSection[13],mtError,[mbOK],0);  
    Valid:=FALSE;
  end;
end;

Procedure TLegendManagerRollupForm.LegendsListboxColumns1Paint(
  Control: TWinControl; Index: Integer; Rect: TRect;
  State: TOwnerDrawState);
var Entry     : TLegendEntry;
    Layer     : pLayer;
    FLegend   : TLegend;

  Function GetLayerForEntry(Entry:TLegendEntry):pLayer;
  var j       : LongInt;
      aLayer  : pLayer;
  begin
    Result:=nil;
    j:=0;
    while j<Project^.Layers^.LData^.Count do begin
      aLayer:=pLayer(Project^.Layers^.LData^.At(j));
      if aLayer<>nil then
        if aLayer.Index=Entry.FLayerNumber then begin
          Result:=aLayer;
          Exit;
          end;
      j:=j+1;
      end;
  end;

begin
  if Project=nil then begin
    LegendsListBox.Count:=0;
    Exit;
    end;
  with Project^ do begin
    if (Legends.Count>0) and
       (Legends.CurrentLegendIndex<>-1) and
       (Legends.Count>Legends.CurrentLegendIndex) then begin
      // later legend of current view
      FLegend:=Legends.Legends[Legends.CurrentLegendIndex];
      if (FLegend.Count>Index) and (Index>-1) then begin
        Entry:=FLegend.Items[Index];
        if Assigned(Entry) then
          with Entry do begin
            // sync layername change
            if FNameFromLayer then begin
              Layer:=GetLayerForEntry(Entry);
              if Assigned(Layer) then begin
                FText:=Layer^.Text^;
                FLegend.Items[Index]:=Entry;
                end;
              end;
            DrawColumnText(Rect,State,FText)
            end;
        end;
      end;
    end;
end;


  {*****************************************************************************
  | Procedure DrawSymbol
  |-----------------------------------------------------------------------------
  | Zeichnet das einem Feld zugeordnete Symbol in das angegebene Rechteck.
  |-----------------------------------------------------------------------------
  | in AIndex = Index der Bitmap Ord(leXXXX)
  | in ARect = Rechteck in das das Symbol gezeichnet werden soll
  +****************************************************************************}
  Procedure TLegendManagerRollupForm.DrawSymbol
     (
     AIndex          : Integer;
     ARect           : TRect
     );
    var X            : Integer;
        Y            : Integer;
    begin
      X:=ARect.Left+(ARect.Right-ARect.Left-16) Div 2;
      Y:=ARect.Top+(ARect.Bottom-ARect.Top-16) Div 2;
      FImages.Draw(LegendsListbox.Canvas,X,Y,Ord(AIndex));
    end;

procedure TLegendManagerRollupForm.LegendsListboxColumns0Paint(
  Control: TWinControl; Index: Integer; Rect: TRect;
  State: TOwnerDrawState);
var Entry     : TLegendEntry;
    i         : TBitmapNr;
    FLegend   : TLegend;
    Layer     : pLayer;
    Symbol    : pSGroup;
begin
  if Project=nil then begin
    LegendsListBox.Count:=0;
    Exit;
    end;
  with Project^ do begin
    if (Legends.Count>0) and
       (Legends.CurrentLegendIndex<>-1) and
       (Legends.Count>Legends.CurrentLegendIndex) then begin
      // later legend of current view
      FLegend:=Legends.Legends[Legends.CurrentLegendIndex];
      if (FLegend.Count>Index) and (Index>-1) then begin
        Entry:=FLegend.Items[Index];
        if Assigned(Entry) then begin
          i:=biNone;
          Layer:=Project^.Layers^.IndexToLayer(Entry.FLayerNumber);
          case Entry.FType of
            leLine       : DrawDlgLine(Rect,Layer); // i:=biLine;
            leFilledRect : DrawDlgFilledRect(Rect,Layer); //i:=biFilledRect;
            leSymbol     : begin
                           Symbol:=pSymbols(Project^.PInfo^.Symbols)^.GetSymbol(Entry.FSymName,
                                    Entry.FLibName);
                           DrawDlgSymbol(Symbol,Rect,Layer);
                           end;
            end;
          end;
        end;
      end;
    end;
end;

{******************************************************************************+
I Procedure DrawDlgLine                                                        I
I draws a diagonal line inside Rect in the style of Layer                      I
I (the same as in LegendDlg.Pas except that Layer is TLayerProperty)           I
I (Delphi has no templates grrr)
+******************************************************************************}
procedure TLegendManagerRollupForm.DrawDlgLine
  (
  Rect     : TRect;
  Layer    : pLayer
  );
var ClipBox      : TRect;
    Region       : THandle;
    XLineStyle   : TXLineStyle;
    XFillStyle   : TCustomXFill;
    Points       : Array[0..1] of TPoint;
begin
  if Layer=nil then Exit;
  InflateRect(Rect,-3,-3);
  with LegendsListBox,Canvas,Layer^ do begin
    with LineStyle do {if ValidValues then} begin
      Brush.Color:=clWindow;
      if (Style>=lt_UserDefined) and
         (Style<Project^.PInfo^.ProjStyles.XLineStyles.Count) then begin
        { draw user-defined type, set scale that line covers 4/5 of the item-height }
        // [!!!]
        // if userdefined style doesn't exists already draw symbolic line
        // just new created user-defines styles are only known in layerdialog (cannot
        // be accessed here -> later correction ray
          XLineStyle:=Project^.PInfo^.ProjStyles.XLineStyles[Style];
          with Rect do begin
            Points[0]:=Point(Left,Bottom);
            Points[1]:=Point(Right,Top);
            XPolyline(Handle,Points,2,XLineStyle.StyleDef^.XLineDefs,
                  XLineStyle.StyleDef^.Count,LineStyle.Color,
                  LineStyle.FillColor,FALSE,
                  (ItemHeight-2)/RectHeight(XLineStyle.ClipRect)/5);
            end;
        end else begin
        { draw windows line-style }
        Pen.Handle:=GetPen(Color,Style,Round(Width));
        GetClipBox(Handle,ClipBox);
        with Rect do IntersectClipRect(Handle,Left,Top,Right,Bottom);
        MoveTo(Rect.Left,Rect.Bottom);
        LineTo(Rect.Right,Rect.Top+1);
        MoveTo(Rect.Left,Rect.Bottom-1);
        LineTo(Rect.Right,Rect.Top);
        with ClipBox do Region:=CreateRectRgn(Left,Top,Right,Bottom);
        SelectClipRgn(Handle,Region);
        DeleteObject(Region);
        end;
      end
    end;
end;

{******************************************************************************+
I Procedure DrawDlgFilledRect                                                  I
I draws a filled rect inside Rect in the style of Layer                        I
I (the same as in LegendDlg.Pas except that Layer is TLayerProperty)           I
I (Delphi has no templates grrr)
+******************************************************************************}
procedure TLegendManagerRollupForm.DrawDlgFilledRect
  (
  Rect     : TRect;
  Layer    : pLayer
  );
var ClipBox      : TRect;
    Region       : THandle;
    XLineStyle   : TXLineStyle;
    XFillStyle   : TCustomXFill;
    Points       : Array[0..4] of TPoint;
begin
  if (Layer=nil) or (Project=nil) then Exit;
  InflateRect(Rect,-3,-1);
  with LegendsListBox,Canvas,Layer^ do begin
    with FillStyle do {if ValidValues then} begin
    with LineStyle do {if ValidValues then} begin
      Pen.Handle:=0;
      Brush.Handle:=0;
      // build drawing rectangle
      InflateRect(Rect,-1,-1);
      with Rect do begin
        Points[0]:=Point(Left,Top);
        Points[1]:=Point(Right,Top);
        Points[2]:=Point(Right,Bottom);
        Points[3]:=Point(Left,Bottom);
        Points[4]:=Point(Left,Top);
        end;
      // draw windows or draw userdefined fill styles
      if Pattern>=pt_UserDefined then begin
        // style present ?(if layer style is changed, style isn't saved immediately)
        if Pattern<Project^.PInfo^.ProjStyles.XFillStyles.Count then begin
            { draw user-defined fill-style }
            XFillStyle:=Project^.PInfo^.ProjStyles.XFillStyles[Pattern];
            if XFillStyle is TVectorXFill then with XFillStyle as TVectorXFill do begin
              XLFillPolygon(Handle,Points,4,StyleDef^.XFillDefs,StyleDef^.Count,
                  0,0,5/MinDistance,0);
              end
            else if XFillStyle is TBitmapXFill then with XFillStyle as TBitmapXFill do
                BMFillPolygon(Handle,Points,4,StyleDef(Handle)^.BitmapHandle,srcCopy,1);
          end;
          Brush.Handle:=0;
        end else begin
        if Pattern<>0 then begin
          // setup windows-fill-type
          Pen.Style:=psClear;
          Brush.Handle:=GetBrush(ForeColor,Pattern,Project^.PInfo^.ProjStyles.XFillStyles);
          if BackColor=clNone then SetBKMode(Handle,Transparent)
                              else SetBKColor(Handle,BackColor);
          with Rect do Rectangle(Left,Top,Right+1,Bottom+1);
          end
        end;
      Pen.Style:=psClear;
      // setup pen style
      if (Style>=lt_UserDefined) and
         (Project^.PInfo^.ProjStyles.XLineStyles.Count<=Style) then begin
        XLineStyle:=Project^.PInfo^.ProjStyles.XLineStyles[Style];
        InflateRect(Rect,-1,-1);
        with Rect do begin
          Points[0]:=Point(Left,Top);
          Points[1]:=Point(Right,Top);
          Points[2]:=Point(Right,Bottom);
          Points[3]:=Point(Left,Bottom);
          Points[4]:=Point(Left,Top);
          XPolygon(Handle,Points,4,XLineStyle.StyleDef^.XLineDefs,
                  XLineStyle.StyleDef^.Count,LineStyle.Color,
                  LineStyle.FillColor,FALSE,
                  (ItemHeight-2)/RectHeight(XLineStyle.ClipRect)/5);
            end;
        end else begin
        Pen.Handle:=GetPen(Color,Style,Round(Width));
        with Rect do Rectangle(Left,Top,Right,Bottom);
        Brush.Handle:=0;
        Pen.Handle:=0;
        end;
      end;
    end;  { with linestyle }
    end;  { with fillstyle }
end;

{******************************************************************************+
I Procedure DrawDlgSymbol                                                      I
I draws a symbol inside Rect in the style of Layer                             I
I (the same as in LegendDlg.Pas except that Layer is TLayerProperty)           I
I (Delphi has no templates grrr)
+******************************************************************************}
procedure TLegendManagerRollupForm.DrawDlgSymbol
  (
  Symbol    : pSGroup;
  Rect      : TRect;
  Layer     : pLayer
  );
var ClipBox      : TRect;
    Region       : THandle;
    XLineStyle   : TXLineStyle;
    XFillStyle   : TCustomXFill;
    Points       : Array[0..1] of TPoint;

  procedure IDrawSymbol;
  begin
    with LegendsListBox,Canvas do begin
      if Symbol=nil then DrawSymbol(Ord(biSymbol),Rect)
        else begin
        FPInfo^.ExtCanvas.Handle:=Canvas.Handle;
        Symbol^.DrawSymbolInRect(FPInfo,Rect);
        end;
      end;
  end;

begin
  if (Layer=nil) or (Project=nil) then Exit;
  InflateRect(Rect,-3,0);
  with LegendsListBox,Canvas,Layer^ do begin
    with FillStyle do {if ValidValues then} begin
    with LineStyle do {if ValidValues then} begin
      // clear background
      Pen.Style:=psClear;
      Pen.Color:=clWindow;
      Brush.Color:=clWindow;
      Canvas.FillRect(Rect);
      // "default style"
      Pen.Handle:=GetPen(Color,Style,Round(Width));
      Brush.Handle:=0;
      if Pattern>=pt_UserDefined then begin
        { draw user-defined fill-style }
        // only if style is present
        // setup user defined style [!!!]
{        if Project^.PInfo^.ProjStyles.XFillStyles.Count>Pattern then begin
          XFillStyle:=FProject^.PInfo^.ProjStyles.XFillStyles[Pattern];
          if XFillStyle.FillType=xftBitmap then begin
            end else begin
            end;
          end else begin}
        end else begin
        if Pattern<>0 then begin
          // setup windows-fill-type and linetype
          Brush.Handle:=GetBrush(ForeColor,Pattern,Project^.PInfo^.ProjStyles.XFillStyles);
          if BackColor=clNone then SetBKMode(Handle,Transparent)
                              else SetBKColor(Handle,BackColor);
          end;
        end;
// draw symbol (or if impossible a symbol bitmap)
      IDrawSymbol;
// reset brush'n pen
      Brush.Handle:=0;
      Pen.Handle:=0;
      end;
    end;  { with linestyle }
    end;  { with fillstyle }
end;

Procedure TLegendManagerRollupForm.DrawColumnText
  (
  Rect   : TRect;
  State  : TOwnerDrawState;
  Text   : String
  );
begin
  with LegendsListBox,Canvas do begin
    Brush.Color:=clWindow;
    Font.Color:=clWindowText;
    if odSelected in State then begin
      Brush.Color:=clHighlight;
      Font.Color:=clHighlightText
      end
      else if odGrayed in State then begin
        Brush.Color:=clBtnFace;
        Font.Color:=clBtnText
        end;
    InflateRect(Rect,-3,0);
    TextRect(Rect,Rect.Left+2,Rect.Top,Text);
    if odFocused in State then DrawFocusRect(Rect);
  end;
end;

Procedure TLegendManagerRollupForm.FormCreate(Sender: TObject);
begin
  FImages:=TWImageList.CreateSize(16,16);
  FImages.AddBitmaps(LegendeSymbols.Picture.Bitmap);
  // setup pinfo (for drawing symbols)
  FPInfo:=New(PPaint,Init);
  FPInfo^.ExtCanvas.Cached:=FALSE;
  Dispose(FPInfo^.Fonts,Done);
  FPinfo^.Fonts:=nil;
  // hide Legends-tab because of own rollup
  PageControl.ActivePage:=LegendsSheet;
  //Scaleby(Trunc(9600000/(PixelsPerInch)),100000);
end;

Procedure TLegendManagerRollupForm.FormDestroy(Sender: TObject);
begin
  FImages.Free;
  Dispose(FPInfo,Done);
end;

Procedure TLegendManagerRollupForm.UpdateLegends;
var FLegend : TLegend;
    i       : Integer;
begin
  if Project=NIL then begin
    LegendsListBox.Count:=0;
    LegNameLabel.Caption:='';
    TitleCombo.Items.Clear;
    TitleCombo.Text:='';
    TitleCombo.ItemIndex:=-1;
    TitleCombo.Enabled:=FALSE;
    Exit;
  end;
  with Project^ do begin
    if Legends.Count>0 then begin
      LegNameLabel.Caption:='';
      TitleCombo.Enabled:=TRUE;
      TitleCombo.Items.Clear;
      TitleCombo.Text:='';
      TitleCombo.ItemIndex:=-1;
      for i:=0 to Legends.Count-1 do TitleCombo.Items.Add(Legends[i].Title);
      // later legend of current view [!!!]
      if (Legends.CurrentLegendIndex<>-1) and (Legends.Count>Legends.CurrentLegendIndex) then begin
        FLegend:=Legends.Legends[Legends.CurrentLegendIndex];
        LegendsListBox.Count:=FLegend.Count;
        LegNameLabel.Caption:=FLegend.Name;
        TitleCombo.Text:=FLegend.Title;
        TitleCombo.ItemIndex:=Legends.CurrentLegendIndex;
        LegendsListBox.Invalidate;
      end
      else begin
        Legends.CurrentLegendIndex:=0;
        LegendsListBox.Count:=0;
        LegNameLabel.Caption:='';
        TitleCombo.Text:='';
        TitleCombo.ItemIndex:=0;
        TitleComboClick(nil);
        if Legends.Count > 0 then TitleCombo.Enabled:=TRUE
        else TitleCombo.Enabled:=FALSE;
      end;
    end
    else begin
      Legends.CurrentLegendIndex:=-1;
      LegendsListBox.Count:=0;
      LegNameLabel.Caption:='';
      TitleCombo.Text:='';
      TitleCombo.ItemIndex:=-1;
      TitleCombo.Enabled:=FALSE;
    end;
  end;
  UpdateEnabled;
end;

procedure TLegendManagerRollupForm.TitleComboClick(Sender: TObject);
begin
  if Project = NIL then Exit;
  Project^.Legends.CurrentLegendIndex:=TitleCombo.ItemIndex;
  UpdateLegends;
  UpdateLayersViewsLegendsLists(Project);
end;

procedure TLegendManagerRollupForm.CopyLegendMenuClick(Sender: TObject);
begin
  {}
end;

procedure TLegendManagerRollupForm.NewLegendActionExecute(Sender: TObject);
  var FLegendDlg   : TLegendDialog;
      LayerList    : TList;
      Cnt          : Integer;
      NewResult    : Boolean;
  Procedure DoAll
     (
     Item          : PLayer
     ); Far;
    begin
      if Item^.SortIndex>0 then with Item^ do LayerList.Add(Item^.GetLayerStyle);
    end;
  begin
    FLegendDlg:=TLegendDialog.Create(Self);
    try
      FLegendDlg.Project:=Project;
      LayerList:=TList.Create;
      Project^.Layers^.LData^.ForEach(@DoAll);
      FLegendDlg.LayerList:=LayerList;
      FLegendDlg.SetNewCaption(43);
      FLegendDlg.Show;
      NewResult:=FLegendDlg.AddLegend;
      FLegendDlg.Hide;
      if NewResult then FLegendDlg.ShowModal;
    finally
      for Cnt:=0 to LayerList.Count-1 do TLayerProperties(LayerList[Cnt]).Free;
      LayerList.Free;
      FLegendDlg.Destroy;
      UpdateLegends;
      UpdateLayersViewsLegendsLists(Project);
    end;
  end;

procedure TLegendManagerRollupForm.RenameLegendActionExecute(Sender: TObject);
  var FLegendDlg   : TLegendDialog;
      LayerList    : TList;
      Cnt          : Integer;
      RenameResult : Boolean;
  Procedure DoAll
     (
     Item          : PLayer
     ); Far;
    begin
      if Item^.SortIndex>0 then with Item^ do LayerList.Add(Item^.GetLayerStyle);
    end;
  begin
    FLegendDlg:=TLegendDialog.Create(Self);
    try
      FLegendDlg.Project:=Project;
      LayerList:=TList.Create;
      Project^.Layers^.LData^.ForEach(@DoAll);
      FLegendDlg.LayerList:=LayerList;
      FLegendDlg.SetNewCaption(43);
      FLegendDlg.Show;
      RenameResult:=FLegendDlg.RenameLegend;
      FLegendDlg.Hide;
      if RenameResult then FLegendDlg.ShowModal;
    finally
      for Cnt:=0 to LayerList.Count-1 do TLayerProperties(LayerList[Cnt]).Free;
      LayerList.Free;
      FLegendDlg.Destroy;
      UpdateLegends;
      UpdateLayersViewsLegendsLists(Project);
    end;
  end;

procedure TLegendManagerRollupForm.CopyLegendActionExecute(Sender: TObject);
  var FLegendDlg   : TLegendDialog;
      LayerList    : TList;
      Cnt          : Integer;
      CopyResult   : Boolean;
  Procedure DoAll
     (
     Item          : PLayer
     ); Far;
    begin
      if Item^.SortIndex>0 then with Item^ do LayerList.Add(Item^.GetLayerStyle);
    end;
  begin
    FLegendDlg:=TLegendDialog.Create(Self);
    try
      FLegendDlg.Project:=Project;
      LayerList:=TList.Create;
      Project^.Layers^.LData^.ForEach(@DoAll);
      FLegendDlg.LayerList:=LayerList;
      FLegendDlg.SetNewCaption(43);
      FLegendDlg.Show;
      CopyResult:=FLegendDlg.CopyLegend;
      FLegendDlg.Hide;
      if CopyResult then FLegendDlg.ShowModal;
    finally
      for Cnt:=0 to LayerList.Count-1 do TLayerProperties(LayerList[Cnt]).Free;
      LayerList.Free;
      FLegendDlg.Destroy;
      UpdateLegends;
      UpdateLayersViewsLegendsLists(Project);
    end;
  end;

procedure TLegendManagerRollupForm.DeleteLegendActionExecute(Sender: TObject);
  var FLegendDlg   : TLegendDialog;
      LayerList    : TList;
      Cnt          : Integer;
      DeleteResult : Boolean;
  Procedure DoAll
     (
     Item          : PLayer
     ); Far;
    begin
      if Item^.SortIndex>0 then with Item^ do LayerList.Add(Item^.GetLayerStyle);
    end;
  begin
    FLegendDlg:=TLegendDialog.Create(Self);
    try
      FLegendDlg.Project:=Project;
      LayerList:=TList.Create;
      Project^.Layers^.LData^.ForEach(@DoAll);
      FLegendDlg.LayerList:=LayerList;
      FLegendDlg.SetNewCaption(43);
      FLegendDlg.Show;
      DeleteResult:=FLegendDlg.RemoveLegend;
      FLegendDlg.Hide;
      if DeleteResult then FLegendDlg.ShowModal;
    finally
      for Cnt:=0 to LayerList.Count-1 do TLayerProperties(LayerList[Cnt]).Free;
      LayerList.Free;
      FLegendDlg.Destroy;
      UpdateLegends;
      UpdateLayersViewsLegendsLists(Project);
    end;
  end;

Procedure TLegendManagerRollupForm.PropertiesLegendActionExecute(Sender: TObject);
  var FLegendDlg   : TLegendDialog;
      LayerList    : TList;
      Cnt          : Integer;
  Procedure DoAll
     (
     Item          : PLayer
     ); Far;
    begin
      if Item^.SortIndex>0 then with Item^ do LayerList.Add(Item^.GetLayerStyle);
    end;
  begin
    FLegendDlg:=TLegendDialog.Create(Self);
    try
      FLegendDlg.Project:=Project;
      LayerList:=TList.Create;
      Project^.Layers^.LData^.ForEach(@DoAll);
      FLegendDlg.LayerList:=LayerList;
      FLegendDlg.SetNewCaption(43);
      FLegendDlg.ShowModal;
    finally
      for Cnt:=0 to LayerList.Count-1 do TLayerProperties(LayerList[Cnt]).Free;
      LayerList.Free;
      FLegendDlg.Destroy;
      UpdateLegends;
      UpdateLayersViewsLegendsLists(Project);
    end;
  end;

end.
