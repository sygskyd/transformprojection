unit offsetdlg;

interface

uses
   Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
   StdCtrls, WCtrls, ExtCtrls, MultiLng, AM_Text, AM_Proj, AM_view, Am_index, am_def,
   am_ini, am_dbgra, am_font, am_layer;

type
   TOffsetDialog = class (TForm)
      OKBtn: TButton;
      CancelBtn: TButton;
      ControlPanel: TPanel;
      BackColorLabel: TWLabel;
      Window2: TWindow;
      OffSetAdvCheckBox: TCheckBox;
      Label2: TLabel;
      PolyLineAdvDirectionComboBox: TComboBox;
      ArcAdvCheckBox: TCheckBox;
      ArcAdvDirectionComboBox: TComboBox;
      WGroupBox1: TWGroupBox;
      WGroupBox2: TWGroupBox;
      MlgSection: TMlgSection;
      Button1: TButton;
      procedure Button1Click (Sender: TObject);
      procedure FormCreate (Sender: TObject);
      procedure Window2Paint (Sender: TObject);
      procedure Window2MouseDown (Sender: TObject; Button: TMouseButton;
         Shift: TShiftState; X, Y: Integer);
      procedure Window2MouseMove (Sender: TObject; Shift: TShiftState; X,
         Y: Integer);
      procedure Window2MouseUp (Sender: TObject; Button: TMouseButton;
         Shift: TShiftState; X, Y: Integer);
      procedure OffSetAdvCheckBoxClick (Sender: TObject);
      procedure FormShow (Sender: TObject);
      procedure ChangePositionObjprop;

   private
    { Private declarations }
   public
      CurrentText: PText;
      CurrentProject: PProj;
      MasterItem: PIndex; //master of label
      IndexMinimum: Integer;
  // Attached: PCollection;
      procedure PropertiesInit;

      function ConvertToTextOffsetOptions (i: integer): integer;
      function ConvertFromTextOffsetOptions (i: integer): integer;
      procedure SelectSnap;
      procedure SelectSnap1;

    { Public declarations }
   end;

var
   OffsetDialog: TOffsetDialog;

implementation

{$R *.DFM}

var
 {brovak}
   BasePoint: Tpoint;
   MyPoint: TPoint;
   Specrect: TRect;
   AnglePoint: Tpoint;
   OrigMouse: TPoint;
   DeltaX, DeltaY: Integer;
    {brovak}

function PointDistance
   (
   const Point1: TPoint;
   const Point2: TPoint
   )
   : Double;
begin
   Result := Sqrt (Sqr (Point1.X - Point2.X) + Sqr (Point1.Y - Point2.Y));
end;

procedure TOffsetDialog.SelectSnap;
type
   Points = array[1..9] of Tpoint;

var
   Minimum: double;
   i, j: integer;
   APoints: Points;
begin;

   with Specrect do
   begin;

      APoints[1].X := SpecRect.Left;
      APoints[1].Y := SpecRect.Top;

      APoints[2].X := (SpecRect.Left + SpecRect.Right) div 2;
      APoints[2].Y := SpecRect.Top;

      APoints[3].X := SpecRect.Right;
      APoints[3].Y := SpecRect.Top;

      APoints[4].X := SpecRect.Left;
      APoints[4].Y := (SpecRect.Top + SpecRect.Bottom) div 2;

      APoints[5].X := (SpecRect.Left + SpecRect.Right) div 2;
      APoints[5].Y := (SpecRect.Top + SpecRect.Bottom) div 2;

      APoints[6].X := SpecRect.Right;
      APoints[6].Y := (SpecRect.Top + SpecRect.Bottom) div 2;

      APoints[7].X := SpecRect.Left;
      APoints[7].Y := SpecRect.Bottom;

      APoints[8].X := (SpecRect.Left + SpecRect.Right) div 2;
      APoints[8].Y := SpecRect.Bottom;

      APoints[9].X := SpecRect.Right;
      APoints[9].Y := SpecRect.Bottom;
   end;

   Minimum := PointDistance (MyPoint, APoints[1]);
   IndexMinimum := 1;

   for i := 2 to 9 do
      if PointDistance (MyPoint, APoints[i]) < Minimum then
      begin;
         Minimum := PointDistance (MyPoint, APoints[i]);
         IndexMinimum := i;
      end;
// IniFile.AdvAnnotationSettings.OffSetAdvDirection:=IndexMinimum;

   AnglePoint := APoints[IndexMinimum];

   with Window2.Canvas do
   begin;
      Pen.Style := psClear;
      Pen.Color := clRed;
      Pen.Width := 2;
      Pen.Style := psSolid;
      Moveto (APoints[IndexMinimum].X - 3, APoints[IndexMinimum].y - 3);
      LineTo (APoints[IndexMinimum].X + 3, APoints[IndexMinimum].y + 3);

      Moveto (APoints[IndexMinimum].X - 3, APoints[IndexMinimum].y + 3);
      LineTo (APoints[IndexMinimum].X + 3, APoints[IndexMinimum].y - 3);
      Pen.Width := 1;
      Pen.Color := clBlue;
      Pen.Style := psDot;
      Moveto (Mypoint.X, MyPoint.y);
      LineTo (APoints[IndexMinimum].X - 1, APoints[IndexMinimum].Y - 1);

   end;

  //draw snap

end;

procedure TOffsetDialog.SelectSnap1;
type
   Points = array[1..9] of Tpoint;

var
   Minimum: double;
   i, j: integer;
   APoints: Points;
   Width,
      Height: integer;
begin;
   Width := SpecRect.Right - SpecRect.Left;
   Height := SpecRect.Bottom - Specrect.Top;

   case IndexMinimum of

      0, 9:
         begin;
            BasePoint.X := MyPoint.X - Width;
            Basepoint.Y := MyPoint.Y - Height;
         end;
      8:
         begin;
            BasePoint.X := MyPoint.X - Width div 2;
            Basepoint.Y := MyPoint.Y - Height;
         end;
      7:
         begin;
            BasePoint.X := MyPoint.X;
            Basepoint.Y := MyPoint.Y - Height;
         end;
      6:
         begin;
            BasePoint.X := MyPoint.X - Width;
            BasePoint.Y := MyPoint.Y - Height div 2;
         end;
      5:
         begin;
            BasePoint.X := MyPoint.X - Width div 2;
            BasePoint.Y := MyPoint.Y - Height div 2
         end;
      4:
         begin;
            BasePoint.X := MyPoint.X;
            BasePoint.Y := MyPoint.Y - Height div 2
         end;
      3:
         begin;
            BasePoint.X := MyPoint.X - Width;
            Basepoint.Y := MyPoint.Y
         end;
      2:
         begin;
            BasePoint.X := MyPoint.X - Width div 2;
            Basepoint.Y := MyPoint.Y
         end;
      1:
         begin;
            BasePoint.X := MyPoint.X;
            Basepoint.Y := MyPoint.Y
         end;

   end;
   Window2.Invalidate;
   Window2Paint (Self);
end;

procedure TOffsetDialog.Button1Click (Sender: TObject);
begin
//Show advanced text dialog
end;

procedure TOffsetDialog.PropertiesInit;
var
   Realangle: double;
begin;
//  Attached CurrentText.GetAttachedObjs
//  MasterItem := CurrentText.GetObjMaster(Pproj(CurrentProject).PInfo);
   Label2.Enabled := false;
   ArcAdvCheckBox.Checked := false;
   ArcAdvDirectionCombobox.Enabled := false;
   PolyLineAdvDirectionComboBox.Enabled := false;
  // OffSetAdvCheckBox.Enabled:=false;
   Button1.Enabled := false;
  // Window2.Visible:=false;

   if MasterItem.GetObjType = ot_Poly then
   begin;
      PolyLineAdvDirectionComboBox.Items.Add (MlgSection[5]);
      PolyLineAdvDirectionComboBox.Items.Add (MlgSection[6]);
      PolyLineAdvDirectionComboBox.Items.Add (MlgSection[7]);
      PolyLineAdvDirectionComboBox.Enabled := true;

      if CurrentText.ObjectOffSetType > -1 then
      begin;
         Label2.Enabled := true;
         PolyLineAdvDirectionComboBox.ItemIndex := CurrentText.ObjectOffSetType;
      end;
   end;
   if MasterItem.GetObjType = ot_Arc then
   begin;
      ArcAdvDirectionComboBox.Items.Add (MlgSection[9]);
      ArcAdvDirectionComboBox.Items.Add (MlgSection[10]);
      ArcAdvDirectionComboBox.Items.Add (MlgSection[11]);
      ArcAdvDirectionComboBox.Enabled := true;
      if CurrentText.ObjectOffSetType > -1 then
      begin;
         ArcAdvCheckBox.Checked := true;
         ArcAdvCheckBox.Enabled := true;
         ArcAdvDirectionComboBox.ItemIndex := CurrentText.ObjectOffSetType;
      end;
   end;

   if CurrentText.IsUseOffset = true then
      OffSetAdvCheckBox.Checked := true;
end;

function TOffsetDialog.ConvertToTextOffsetOptions (i: integer): integer;
begin;
   case I of
      1: Result := 0;
      2: Result := 3;
      3: Result := 6;
      4: Result := 1;
      5: Result := 4;
      6: Result := 7;
      7: Result := 2;
      8: Result := 5;
      9: Result := 8;
   end;
end;

function TOffsetDialog.ConvertFromTextOffsetOptions (i: integer): integer;
begin;
   case I of
      0: Result := 1;
      1: Result := 4;
      2: Result := 7;
      3: Result := 2;
      4: Result := 5;
      5: Result := 8;
      6: Result := 3;
      7: Result := 6;
      8: Result := 9;
   end;
end;

procedure TOffsetDialog.Window2Paint (Sender: TObject);

var
   Arect: Trect;

   OldBrush: TBrush;
   OldPen: TPen;
   OldPenWidth: integer;
   OldStyle: TPenStyle;

begin
//if UseadvancedOptionsCheckBox.Checked=false then Exit;
   if OffsetAdvCheckBox.Checked = false then Exit;

   ARect.Left := Window2.Width div 2 - 35;
   ARect.Right := Window2.Width div 2 + 35;
   Arect.Bottom := Window2.Height div 2 + 25;
   Arect.Top := Window2.Height div 2 - 25;

   MyPoint.X := (ARect.Right + Arect.Left) div 2;
   MyPoint.Y := (Arect.Bottom + Arect.Top) div 2;

   with Window2.Canvas do
   begin;
// FillRect(Window2.Clientrect);
      OldBrush := Brush;
      OldPen := Pen;

      Brush.Style := bsBDiagonal;
      Pen.Color := ClNavy;
      Pen.Width := 1;

      Brush.Color := clNavy;

      Pen.Style := psSolid;
      Brush.Color := clNavy;

      with Arect do
         RectAngle (Left, Top, Right, Bottom);

      Pen.Style := psClear;
      Pen.Color := clRed;
      Pen.Width := 3;
      Pen.Style := psSolid;
      Moveto (MyPoint.X - 4, MyPoint.y - 1);
      LineTo (MyPoint.X + 4, MyPoint.y - 1);

      Moveto (MyPoint.X, MyPoint.y - 5);
      LineTo (MyPoint.X, MyPoint.y + 3);

{ If Radiogroup2.ItemIndex>-1 then
  begin;
   With Arect do begin;
     case Radiogroup2.ItemIndex of
      8:begin; Right:=MyPoint.X;Bottom:=MyPoint.Y;end;
      7:begin; Right:=MyPoint.X;Bottom:=MyPoint.Y+9;end;
      6:begin; Right:=MyPoint.X;Bottom:=MyPoint.Y+18;end;
      5:begin; Right:=MyPoint.X+20;Bottom:=MyPoint.Y;end;
      4:begin; Right:=MyPoint.X+20;Bottom:=MyPoint.Y+9;end;
      3:begin; Right:=MyPoint.X+20;Bottom:=MyPoint.Y+18;end;
      2:begin; Right:=MyPoint.X+40;Bottom:=MyPoint.Y;end;
      1:begin; Right:=MyPoint.X+40;Bottom:=MyPoint.Y+9;end;
      0:begin; Right:=MyPoint.X+40;Bottom:=MyPoint.Y+18;end;
     end;
   Left:=Right-40;Top:=Bottom-18;}

      Pen.Color := clBlack;
      Pen.Style := psInsideFrame;
      Pen.Width := 1;
      with Specrect do
      begin;
         Left := BasePoint.X; //+ DeltaX;
         Right := Left + 50;
         Top := BasePoint.y; // + Deltay;
         Bottom := Top + 20;
         Brush.Color := clWhite;
         Brush.Style := bsSolid;
//   Font.Style:=Font.Style+[fsBold];
 //Window3.Canvas.Pen.Color:=BaseFont.Color;
         Brush.Style := bsClear;
         RectAngle (Left, Top, Right, Bottom);
//   Brush.Style:=bssolid;
         Pen.Color := Window2.Canvas.Font.Color;
   // Brush.Color:=clBlack;
//    Pen.Style:=PsSolid;
 //!!  Window2.Canvas.Font :=BaseFont;
         Window2.Canvas.Font.Style := Font.Style + [fsBold];
         TextRect (Specrect, Left + 7, Top + 5, 'Text');

      end;

      if Screen.Cursor = crSize then SelectSnap;

   end;
// Canvas.Brush:=Oldbrush;
// Canvas.Pen:=OldPen;

end;

procedure TOffsetDialog.Window2MouseDown (Sender: TObject;
   Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
   clickpoint: TPoint;
begin
   ClickPoint.X := x;
   ClickPoint.y := y;

   if Button = mbLeft then

      if ptInRect (Specrect, ClickPoint) then
      begin

         if ssDouble in Shift then
         begin;
 // ChangeFont;
            Exit;
         end
         else
         begin;
            GetCursorPos (OrigMouse);
            Screen.Cursor := crSize;
            DeltaX := ClickPoint.X - BasePoint.X;
            DeltaY := ClickPoint.Y - BasePoint.Y;
         end;
      end;
end;

procedure TOffsetDialog.Window2MouseMove (Sender: TObject;
   Shift: TShiftState; X, Y: Integer);
var
   clickpoint: TPoint;
begin
   ClickPoint.X := x;
   ClickPoint.y := y;

   if shift = [ssleft] then
   begin;

      if not ptInRect (Window2.ClientRect, ClickPoint) then
      begin;
  //   if X> Window2.ClientRect.Right then ClickPoint.X:=Window2.ClientRect.Right-1;
//     if X< 0 then ClickPoint.X:= 1;
//     if Y> Window2.Height then ClickPoint.Y:=Window2.Height-1;
//     if Y< 0 then ClickPoint.Y:=1;
         SetCursorPos (OrigMouse.X, OrigMouse.Y);
//     Screen.Cursor:=crdefault;
         Exit;
      end;
      GetCursorPos (OrigMouse);

      if Screen.Cursor = crSize then
      begin;
 //  OldPoint.X:=X;
 //  OldPoint.Y:=y;
//   invalidate
 //   InvalidateRect(Window2.Handle,@SpecRect,false);
 //   UpdateWindow(Window2.Handle);

{     begin;
     if X<0 then x:=0;
     if x>Window2.ClientRect.Right then x:=Window2.ClientRect.Right;
     if Y<0 then x:=0;
     if y>Window2.ClientRect.Bottom then y:=Window2.ClientRect.Bottom;
    Exit;
    end;}

         BasePoint.x := x - DeltaX;
         BasePoint.y := y - Deltay;

  //  Window2.Canvas.Brush.Color:=clWhite;
         Window2.Invalidate;
// Window2.Canvas.Brush.Style:=bsSolid;
//Window2.Canvas.FillRect(Rect(0,0,Window2.Width,Window2.Height));
         Window2.OnPaint (self);

      end
   end
   else
      if ptInRect (Specrect, ClickPoint) then
         Screen.Cursor := crDrag
      else
         Screen.Cursor := crDefault;

end;

procedure TOffsetDialog.Window2MouseUp (Sender: TObject;
   Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
   if Screen.Cursor = crSize then
   begin;
      Screen.Cursor := crDefault;
      Basepoint.X := Mypoint.X + (BasePoint.X - AnglePoint.X);
      Basepoint.Y := Mypoint.Y + (BasePoint.Y - AnglePoint.Y);
      Window2.Invalidate;
      Window2.OnPaint (self);
   end;

   Screen.Cursor := crDefault;
   Window2.Invalidate;
end;

procedure TOffsetDialog.FormCreate (Sender: TObject);
var
   i: integer;
begin
   i := 5;

end;

procedure TOffsetDialog.OffSetAdvCheckBoxClick (Sender: TObject);
begin
   Window2.Visible := OffSetAdvCheckBox.Checked;
   if Window2.Visible = true then
   begin;
      SelectSnap1;
      Window2.OnPaint (Self);
   end;
end;

procedure TOffsetDialog.FormShow (Sender: TObject);
begin
   SelectSnap1;
end;

procedure TOffsetDialog.ChangePositionObjprop;
var
   TmpAngle: integer;
   ilPosx, ilPosy, ilTextHeight, ilHeight, ilWidth: longint;
   ATextFont, AOldFont: HFont;
   AFontDes: PFontDes;
   Stroka: PChar;
   ATextRect: TRect;
   AITem: Pindex;
   AObj: PView;
begin;
      with IniFile.AdvAnnotationSettings do
   begin;
           //    UseAdvancedOptions:=UseAdvancedOptionsCheckBox.Checked;
           //   PolygonAdv:=PolygonAdvCheckBox.Checked;
            //   PolyLineAdv:=PolyLineAdvCheckBox.Checked;
      PolyLineAdvDirection := PolyLineAdvDirectionComboBox.ItemIndex;
      if PolyLineAdvDirection < 0 then PolyLineAdvDirection := 0;
      ArcAdv := ArcAdvCheckBox.Checked;
      ArcAdvDirection := ArcAdvDirectionComboBox.ItemIndex;
      OffSetAdv := OffSetAdvCheckBox.Checked;
      OffSetAdvDirection := IndexMinimum;
   end;
   IniFile.WriteAdvAnnotation;

   ilTextHeight := CurrentText.Font.Height; // Project. PInfo.AnnotSettings.AnnotFont.Height;

   ATextRect.Left := 0;
   ATextRect.Top := 0;
 // CurrentProject.PInfo.AnnotSettings.AnnotFont.Font
   AFontDes := CurrentProject.PInfo.Fonts.GetFont (CurrentText.Font.Font); //CurrentProject.PInfo^.Fonts.IDFromName(CurrentText.Font.));//CurrentProject.PInfo.AnnotSettings.AnnotFont.Font);
   ATextFont := CurrentProject.PInfo.GetFont (CurrentText.Font (*CurrentProject.PInfo.AnnotSettings.AnnotFont*), CurrentProject.PInfo.CalculateDisp (ilTextHeight), 0);

//   AItem := CurrentProject.Layers.TopLayer.HasIndexObject(MasterItem);

   AObj := PLayer (CurrentProject.PInfo.Objects).IndexObject (CurrentProject.PInfo, MasterItem);

   AOldFont := SelectObject (CurrentProject.PInfo.ExtCanvas.Handle, ATextFont);

   Stroka := StrAlloc (Length (PtoStr (CurrentText.Text)) + 1);
   PStrToPChar (Stroka, CurrentText.Text);
         // The following command does not draws text but measures only it's length.
   DrawText (CurrentProject.PInfo.ExtCanvas.Handle, Stroka, StrLen (Stroka), ATextRect, DT_CalcRect);
//  strdispose(Stroka);
   if (AFontDes <> nil) and (AFontDes.IsVertical) then
   begin
      ilHeight := CurrentProject.PInfo.CalculateDraw (ATextRect.Right);
      ilWidth := CurrentProject.PInfo.CalculateDraw (Abs (ATextRect.Bottom));
   end
   else
   begin
      ilWidth := CurrentProject.PInfo.CalculateDraw (ATextRect.Right);
      ilHeight := CurrentProject.PInfo.CalculateDraw (Abs (ATextRect.Bottom));
   end;

   TmpAngle := CalculateTextPos1 (MasterItem, AObj, ilPosX, ilPosY, ilWidth, ilHeight, ilTextHeight);
   if PView (MasterItem).GetObjType = ot_Symbol then
      CurrentText.Font.Style := CurrentText.Font.Style or ts_Transparent;

   CurrentText.Angle := TmpAngle;
   CurrentText.Pos.X := ilPosX;
   CurrentText.Pos.Y := ilPosY;

   CurrentText.IsUseOffset := IniFile.AdvAnnotationSettings.OffSetAdv;
   if IniFile.AdvAnnotationSettings.OffSetAdv = true then
      CurrentText.AttData.AliPosOfObject := ConvertToTextOffsetOptions (IndexMinimum);

   if CurrentText.GetObjMaster (CurrentProject.PInfo).GetObjType = ot_Poly then
   begin;
      CurrentText.ObjectOffSetType := IniFile.AdvAnnotationSettings.PolyLineAdvDirection;
   end
   else
   begin;
      if CurrentText.GetObjMaster (CurrentProject.PInfo).GetObjType = ot_Arc then
         if IniFile.AdvAnnotationSettings.ArcAdv = true then
            CurrentText.ObjectOffSetType := IniFile.AdvAnnotationSettings.ArcAdvDirection;
   end;

   SelectObject (Currentproject.PInfo.ExtCanvas.Handle, AOldFont);
   DeleteObject (ATextFont);

   CurrentText.UpdateTextData (CurrentProject.PInfo);
   CurrentText.CalculateClipRect (CurrentProject.Pinfo);
end;

end.

