Unit PrnSelRange;

Interface
{$IFNDEF AXDLL} // <----------------- AXDLL
Uses Classes,InpHndl,GrTools
     ,PrintDlg
     ,PrintTemplates,AM_Coord
{++ Ivanoff BUG#91}
     , AM_Def, Controls
{-- Ivanoff BUG#91}
{++ Ivanoff BUG#372}
     , Graphics, Windows
{-- Ivanoff BUG#372}
     ;

Type TSelectPrintRangeHandler = Class(TRubberGrid)
      Private
       FDialog     : TPrintingDialog;
{++ Ivanoff BUG#91}
       dLastMouseX,
       dLastMouseY,
       dSnapSencePointX,
       dSnapSencePointY: Double;
       bIAmUnderMoving: Boolean;
{-- Ivanoff BUG#91}
       Procedure   SetDialog(ADialog:TPrintingDialog);
      Public
       Constructor Create(AOwner:TComponent); override;
{++ Moskaliov BUG#90 BUILD#150 05.01.01}
       Procedure   DispFrameStatusText(Sender: TObject);
       Procedure   InputCoordDlgSetup(var AData: TCoordData);
       Procedure   InputCoordDlgSetValues(AData: TCoordData);
{-- Moskaliov BUG#90 BUILD#150 05.01.01}
       Procedure   OnActivate; override;
       Procedure   OnDeactivate; override;

       Property    PrintDialog:TPrintingDialog read FDialog write SetDialog;

{++ Ivanoff BUG#91}
       Function    OnMouseDown(Const X,Y:Double;Button:TMouseButton;Shift:TShiftState):Boolean; override;
       Function    OnMouseMove(Const X,Y:Double;Shift:TShiftState):Boolean; override;
       Function    OnMouseUp(Const X,Y:Double;Button:TMouseButton;Shift:TShiftState):Boolean; override;

       Property    SnapSencePointX: Double Read dSnapSencePointX;
       Property    SnapSebcePointY: Double Read dSnapSencePointY;
{-- Ivanoff BUG#91}
{++ Ivanoff BUG#372}
       Procedure   OnKeyUp(var Key: Word; Shift:TShiftState); override;
{-- Ivanoff BUG#372}
     end;
{$ENDIF} // <----------------- AXDLL
Implementation
{$IFNDEF AXDLL} // <----------------- AXDLL

Uses EditHndl,SysUtils, Messages;

{===============================================================================
  TSelectPrintRangeHandler
+==============================================================================}

Constructor TSelectPrintRangeHandler.Create(AOwner:TComponent);
begin
   inherited Create(AOwner);
   Options:=[ehoMoveInside,ehoSize,ehoSizeMultiple,ehoSizeSymmetric,ehoRotate];
   ReferencePointIndex:=0;
{++ Ivanoff BUG#91}
   bIAmUnderMoving := FALSE;
   dSnapSencePointX := 0;
   dSnapSencePointY := 0;
{-- Ivanoff BUG#91}
{+++ Brovak BUG 524,539 BUILD 167}
     FAllowDblclick:=true;
{-- Brovak}     
end;

//++ Glukhov PrintLayout Build#166 24.12.01
Procedure TSelectPrintRangeHandler.SetDialog(ADialog:TPrintingDialog);
var
  Graphic   : TProjectPlaceholder;
  ARect     : TDRect;
  ARotRect  : TRotRect;
  dblValue  : Double;
begin
  // detect the project placeholder
  Graphic:= ADialog.GetProjGraph;
  if Graphic = NIL  then Exit;

  FDialog:= ADialog;
{++ Ivanoff BUG#353}
  // Get parameters of current project window.
  FDialog.Project.PInfo.GetProjScreen(ARect);

  // Define bigger window.
  If FDialog.PrintOptions.PrintRect.X < ARect.A.X Then ARect.A.X:= Round(FDialog.PrintOptions.PrintRect.X);
  If FDialog.PrintOptions.PrintRect.Y < ARect.A.Y Then ARect.A.Y:= Round(FDialog.PrintOptions.PrintRect.Y);
  dblValue := FDialog.PrintOptions.PrintRect.X + FDialog.PrintOptions.PrintRect.Width;
  If dblValue > ARect.B.X Then ARect.B.X := Round(dblValue);
  dblValue := FDialog.PrintOptions.PrintRect.Y + FDialog.PrintOptions.PrintRect.Height;
  If dblValue > ARect.B.Y Then ARect.B.Y := Round(dblValue);
  // Create a TRotRect that is needed to Zoom operation.
  ARotRect.X := ARect.A.X;
  ARotRect.Y := ARect.A.Y;
  ARotRect.Width := ARect.B.X - ARect.A.X;
  ARotRect.Height := ARect.B.Y - ARect.A.Y;
  ARotRect.Rotation := FDialog.Project^.PInfo^.ViewRotation;
  // Zoom the project.

  FDialog.Project.ZoomByRect(ARotRect, 0, TRUE);

{-- Ivanoff BUG#353}

  XPosition:= FDialog.PrintOptions.PrintRect.X;
  YPosition:= FDialog.PrintOptions.PrintRect.Y;
  Options:= Options+[ehoSize];
  // YG: I changed a bit the interpretation of property KeepScale to make it
  // more clear (it did not mean the scale keeping at all)
  // Now it means that the form of printing rectangle is set in the project,
  // and it is set in the Print dialog, if else
  if not FDialog.PrintOptions.KeepScale then begin
  // The rectangle shape is set in the dialog (not in the project)
    with FDialog.Project^.PInfo^ do
    begin
      Width:= RectWidth(Graphic.BoundsRect)*100.0/Graphic.PrintScale/UnitsToMM;
      Height:= RectHeight(Graphic.BoundsRect)*100.0/Graphic.PrintScale/UnitsToMM;
    end;
    Options:= Options+[ehoKeepAspect];
    if (Graphic.PrintSize = psScale)
    or (Graphic.PrintSize = psScaleAndFit) then Options:= Options-[ehoSize];
  end
  else begin
  // The rectangle shape is set in the project
    Width:= FDialog.PrintOptions.PrintRect.Width;
    Height:= FDialog.PrintOptions.PrintRect.Height;
    Options:= Options-[ehoKeepAspect];
  end;
  Rotation:= FDialog.PrintOptions.PrintRect.Rotation;
end;
//-- Glukhov PrintLayout Build#166 24.12.01

{++ Moskaliov BUG#90 BUILD#150 05.01.01}
Procedure TSelectPrintRangeHandler.DispFrameStatusText(Sender: TObject);
var TempText     : String;
    dblX2        : Double;
    dblY2        : Double;
    dblAngle     : Double;
    dblSin       : Double;
    dblCos       : Double;
begin
   dblSin:=sin(Rotation);
   dblCos:=cos(Rotation);
   dblX2:=XPosition-Height*dblSin+Width*dblCos;
   dblY2:=YPosition+Height*dblCos+Width*dblSin;
   TempText:=FDialog.Project^.PInfo^.CoordinateSystem.FormatCoordinates(XPosition,YPosition,dblX2,dblY2);
   dblAngle:=(Rotation*180)/Pi;
   TempText:=TempText+' '+FDialog.MlgSection[126]+':'+FormatFloat('#,##0.00',dblAngle);
   StatusText:=TempText;
end;

Procedure TSelectPrintRangeHandler.InputCoordDlgSetup(var AData: TCoordData);
begin
   with AData do
      begin
         Lines:=5;
         Text3:=15103;
         Text4:=15104;
         Text5:=15101;
         ValuesPredefined[0]:=XPosition;
         ValuesPredefined[1]:=YPosition;
         ValuesPredefined[2]:=Height;
         ValuesPredefined[3]:=Width;
         ValuesPredefined[4]:=(Rotation*180)/Pi;
         CoordSystem:=Integer(FDialog.Project^.PInfo^.CoordinateSystem.CoordinateType)
                      or Integer(PresetBit); { Mark values to be predefined }
      end;
end;

Procedure TSelectPrintRangeHandler.InputCoordDlgSetValues(AData: TCoordData);
var dblScale     : Double;
    dblRest      : Double;
    intPart      : Integer;
    i            : Integer;
begin
   with AData do
      begin
         for i:=0 to 3 do
             ValuesPredefined[i]:=ValuesPredefined[i]*100;
         XPosition:=ValuesPredefined[0];
         YPosition:=ValuesPredefined[1];
         if ehoKeepAspect in Options then
            begin
               dblScale:=(ValuesPredefined[2]/Height+ValuesPredefined[3]/Width)*0.5;
               Height:=Height*dblScale;
               Width:=Width*dblScale;
            end
         else
            begin
               Height:=ValuesPredefined[2];
               Width:=ValuesPredefined[3];
            end;
         if ValuesPredefined[4] = 0 then
            Rotation:=0
         else
            begin
               if ValuesPredefined[4] > 0 then
                  begin
                     while ValuesPredefined[4] > MaxLongInt do
                        ValuesPredefined[4]:=ValuesPredefined[4]-360;
                  end
               else   // ValuesPredefined[4] < 0
                  begin
                     while ValuesPredefined[4] < -MaxLongInt do
                        ValuesPredefined[4]:=ValuesPredefined[4]+360;
                  end;
               intPart:=Trunc(ValuesPredefined[4]);
               dblRest:=Frac(ValuesPredefined[4]);
               intPart:=intPart Mod 360;
               Rotation:=((intPart+dblRest)*Pi)/180;
            end;
      end;
   DispFrameStatusText(Self);
end;
{-- Moskaliov BUG#90 BUILD#150 05.01.01}

Procedure TSelectPrintRangeHandler.OnActivate;
begin
  inherited;
  // set view-rotation to handler
  ViewRotation:=FDialog.Project^.PInfo^.ViewRotation;
end;

Procedure TSelectPrintRangeHandler.OnDeactivate;
begin
  inherited;
end;

{++ Ivanoff BUG#91}
Function TSelectPrintRangeHandler.OnMouseDown(Const X,Y:Double;Button:TMouseButton;Shift:TShiftState):Boolean;
Begin
     RESULT := Inherited OnMOuseDown(X, Y, Button, Shift);
     SELF.dSnapSencePointX := SELF.XPosition;
     SELF.dSnapSencePointY := SELF.YPosition;
     SELF.dLastMouseX := X;
     SELF.dLastMouseY := y;
     SELF.bIAmUnderMoving := TRUE;
End;

Function TSelectPrintRangeHandler.OnMouseMove(Const X,Y:Double;Shift:TShiftState):Boolean;
Begin
     RESULT := Inherited OnMouseMove(X, Y, Shift);
     If SELF.bIAmUnderMoving Then Begin
        SELF.dSnapSencePointX := SELF.dSnapSencePointX + (X - dLastMouseX);
        SELF.dSnapSencePointY := SELF.dSnapSencePointY + (Y - dLastMouseY);
        dLastMouseX := X;
        dLastMouseY := Y;
        End;
End;

Function TSelectPrintRangeHandler.OnMouseUp(Const X,Y:Double;Button:TMouseButton;Shift:TShiftState):Boolean;
Begin
     Inherited OnMouseUp(X, Y, Button, Shift);
     SELF.bIAmUnderMoving := FALSE;
End;
{-- Ivanoff BUG#91}
{++ Ivanoff BUG#372}
Procedure TSelectPrintRangeHandler.OnKeyUp(var Key: Word; Shift:TShiftState);
Begin
     If Key = vk_Tab Then Begin
        SELF.Paint;
        If SELF.Pen.Color = clBlue Then
           SELF.Pen.Color := $00FFFFCC
        Else
           SELF.Pen.Color := clBlue;
        SELF.Paint;
     End;
     Inherited OnKeyUp(Key, Shift);
End;
{-- Ivanoff BUG#372}
{$ENDIF} // <----------------- AXDLL
end.
