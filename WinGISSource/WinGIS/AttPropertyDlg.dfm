�
 TATTPROPERTYDIALOG 0�  TPF0TAttPropertyDialogAttPropertyDialogLeftBTop� BorderStylebsDialogCaption!1ClientHeight� ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrder	PositionpoScreenCenterOnCreate
FormCreateOnHideFormHideOnShowFormShowPixelsPerInch`
TextHeight TBevelBevel1LeftTop� WidthyHeightAnchorsakLeftakBottom ShapebsBottomLine  TButton	CancelBtnLeft(Top� WidthYHeightAnchorsakLeftakBottom Cancel	Caption!3ModalResultTabOrder  TButtonOKBtnLeft� Top� WidthYHeightAnchorsakLeftakBottom Caption!2Default	ModalResultTabOrder   TPanelControlPanel1Left Top�Width�Height� AnchorsakLeftakTopakBottom 
BevelOuterbvNoneTabOrder TPanelControlPanelLeft TopWidth�Height� AnchorsakLeftakTopakBottom 
BevelOuterbvNoneTabOrder  TWLabelWLabel4Left}TopWidth\HeightAutoSizeCaption!6FocusControl
VerOffEdit  TWLabelWLabel3LeftTopWidth\HeightAutoSizeCaption!5FocusControl
HorOffEdit  TWLabelWLabel1LeftTopWidth� HeightAutoSizeCaption!4  TWLabel
WLblAttNumLeft	TopOWidth� HeightAutoSizeCaption!7  TWLabelWLabel2Left� TopWidth� Height	AlignmenttaRightJustifyAutoSizeCaption!13  TWLabelWLabel5Left� ToplWidthXHeight	AlignmenttaRightJustifyAutoSizeCaption!15FocusControlRadioGrpAliObject  TWLabelWLabel6Left� TopWidthXHeight	AlignmenttaRightJustifyAutoSizeCaption!14FocusControlRadioGrpAliMaster  
TWGroupBoxGroup1LeftTopaWidth� HeightaCaption!8TabOrderBoxStyle	bsTopLine 	TCheckBoxChkSelAttachedLeftTopMWidth� HeightCaption!12TabOrder  	TCheckBoxChkDeselObjectLeftTop:Width� HeightCaption!11TabOrder  	TCheckBoxChkSelMasterLeftTop&Width� HeightCaption!10TabOrder  	TCheckBoxChkMoveLeftTopWidth� HeightCaption!9TabOrder OnClickChkMoveClick   TSpinBtn
VerOffSpinLeft� Top(WidthHeight	ArrowkeysCtrlIncrement       ��?	Increment       ��?Max     (k�@Min     (k��ShiftIncrement      @�@	Validator	valVShift  TEdit
VerOffEditLeft}Top(WidthLHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderOnChangeVerOffEditChangeOnExitVerOffEditExit  TEdit
HorOffEditLeftTop(WidthLHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder OnChangeHorOffEditChangeOnExitHorOffEditExit  TSpinBtn
HorOffSpinLeftUTop(WidthHeight	ArrowkeysCtrlIncrement       ��?	Increment       ��?Max     (k�@Min     (k��ShiftIncrement      @�@	Validator	valHShift  TRadioGroupRadioGrpAliMasterLeftDTopWidthBHeightJColumnsCtl3D	Items.Strings          ParentCtl3DTabOrderOnClickRadioGrpAliMasterClick  TRadioGroupRadioGrpAliObjectLeftDToplWidthBHeightJColumnsCtl3D	Items.Strings          ParentCtl3DTabOrderOnClickRadioGrpAliObjectClick  TButtonBtnHor0LeftfTop(WidthHeightCaption0TabOrderTabStopOnClickBtnHor0Click  TButtonBtnVer0Left� Top(WidthHeightCaption0TabOrderTabStopOnClickBtnVer0Click    TMlgSection
MlgSectionSectionAttPropertyDialogLeftTop�   TMeasureLookupValidator	valHShiftEdit
HorOffEditUseableUnitsCommasDefaultValue.Units	muPercentMinValue.UnitsmuCentimetersMinValue.Value     (k��MaxValue.UnitsmuCentimetersMaxValue.Value     (k�@UnitsmuDrawingUnitsLeftTop�   TMeasureLookupValidator	valVShiftEdit
VerOffEditUseableUnitsCommasDefaultValue.Units	muPercentMinValue.UnitsmuCentimetersMinValue.Value     (k��MaxValue.UnitsmuCentimetersMaxValue.Value     (k�@UnitsmuDrawingUnitsLeft*Top�    