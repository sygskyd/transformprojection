{++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  Created: 14-DEC-2000 ! By Sigolaeff Victor (aka Sygsky)
     mailto:Sygsky@yandex|mail|rambler|pochtamt.ru
********************************************************************************
  Unit contains object and utils for TAB file format support.
  Parser of TAB is included in private method fParseTAB
********************************************************************************
  Changed:
  Sygsky: 27-DEC-2000, start release.
  Sygsky: 12-FEB-2001, WriteTabFile proc. added.
  Sygsky: 04-SEP-2001, MultiImageTab support added. -
                       you can use names in form "image.*" for example
  Sygsky: 26-DEC-2002, Comment to OMG file and a log reports were added by
                       Almir Karabegovic(from Bosnia) request through Freddie.
                       It could be my LAST change for WinGIS in my life
                       due to my discharge by Mayer on 16-DEC-2002 :o)
-------------------------------------------------------------------------------
Example of a TAB file contents:
...
!This is a sample TAB-file for ProGIS
!A comment is inserted using a '!'

Definition Table
File "yosemite.jpg" ! You can omit this syntax line now. File "yosemite.*" will be used except
(-400000.0, 300000.0) (   0.0, 767.0) Label "Lower Left Point"
(0000000.0, 600000.0) (1023.0,   0.0) Label "Upper Right Point",
Units "cm"
...
}

unit TABFiles;

interface
uses
  Classes, FileCtrl, Windows, SysUtils
{$IFDEF VMSI}
  , QStrings
{$ENDIF}
  ;

type
 { Actual type of object: not loaded, dir loaded, file loaded}
  ExecType = (etNone, etDir, etFile);

  TTabError = (teNone, teUnknown, teFileNotExists,
    teFileReadError, teSyntaxCheck, teSomeSyntaxAbsent,
    teBadCoords, teNoDefinition, teNoImageName,
    teNoCoord, teNoUnits, teImageNameDifferent,
    teCancelled, teDuplicatedOnLayer);

  SyntaxType = (stDefinition, stFileName, stCoord,
    stUnits, stType, stBlank, stComment, stUnknown);

  { Millimeters, centimeters, meters, kilometers, inches, pixels - for VMSI}
  TabUnits = (tuMM, tuCM, tuM, tuKM
{$IFDEF VMSI}
    , tuPX
{$ENDIF}
    , tuIN
    , tuUnknown
    );

  UnitsDef = record
    SName, LName: AnsiString;
    Mult: Double;
  end;

  TFPoint = record
    X: Double;
    Y: Double;
  end;

  TFRect = record
    case Integer of
      0: (Left, Top, Right, Bottom: Double);
      1: (TopLeft, BottomRight: TFPoint);
  end;

  PRefPoint = ^TRefPoint;
  TRefPoint = record
    Proj: TFPoint;
    Img: TFPoint;
//    Img: TPoint;
  end;
  TRefArray = array of TRefPoint;

  // Point types:
  // ptNormal - usual point with coordinates of pixel center
  // ptA..ptB - point for rectangle (created by WinGIS only) boundaries envelope
  TPointType = (ptNormal, ptA, ptB);

  PTABFile = ^TTabFile;

  TTABFile = class(TObject)
  private
//    fCount: Integer;
    fNameList: TStringList;
    fMultiply: Double;
    fFileName: AnsiString;
    fImageName: AnsiString;
    fClipRect: TRect;
    TABFile: TextFile;
    fWidth,
      fHeight,
      PointsCnt,
      LineCnt: Integer;
    fErrorCode: TTabError;
    fExecType: ExecType;
    ImagePoints: array of TFPoint;
//    ImagePoints: array of TPoint;
    ProjPoints: array of TFPoint;
    PointType: array of TPointType;
    fAPoint, fBPoint: TFPoint;
    fAPointI, fBPointI : TFPoint;
    function fParseTAB: Boolean;
    function fProcessTAB: Boolean;
    function fGetFileName: AnsiString;
    function fGetDirName: AnsiString;
    function fGetImageName: AnsiString;
    function fGetFullImageName: AnsiString;
    function fCheckBestPoints: Boolean;
    function fIsValid: Boolean; // Information only about TAB, not image itself!
    function fGetProjPoint(Index: Integer): TFPoint;
    function fGetImagePoint(Index: Integer): TFPoint;
    function fGetClipRect: TRect;
    function fGetProjRect: TFRect;
    function fGetPixRect: TRect;
    function HasWildCards: Boolean;
    function GetUserName: AnsiString;
    function GetEntry(Index: Integer): AnsiString;
    function fGetImageCount: Integer;
    procedure fClrError;
    procedure UpdateNames;
  protected
  public
    constructor Create(FileName: AnsiString = '');
    destructor Destroy; override;

    property Mode: ExecType read fExecType;
    property FileName: AnsiString read fGetFileName;
    property MultiNames[Index: Integer]: AnsiString read GetEntry;
    property ImageName: AnsiString read fGetImageName;
    property FullImageName: AnsiString read fGetFullImageName;
    property DirName: AnsiString read fGetDirName;
    property IsValid: Boolean read fIsValid;
    property LastError: TTabError read fErrorCode write fErrorCode;
    property ProjPoint[Index: Integer]: TFPoint read fGetProjPoint;
    property ImagePoint[Index: Integer]: TFPoint read fGetImagePoint;
    property ProjClipRect: TRect read fGetClipRect;
    property ProjPixRect: TRect read fGetPixRect;
    property FProjClipRect: TFRect read fGetProjRect;
    property LastReadLine: Integer read LineCnt;
    property Cnt: Integer read PointsCnt;
    property ImageCount: Integer read fGetImageCount;
    property WildCardsUsed: Boolean read HasWildCards;
    property RealImageNameUsed: AnsiString read GetUserName;
    property APoint: TFPoint read fAPoint;
    property BPoint: TFPoint read fBPoint;
{    property ImageWidth: Integer read fWidth;
    property ImageHeight: Integer read fHeight;}

    function Execute: Boolean;
    function LoadTAB(FileName: AnsiString): Boolean; { It can be a directory -  but not NOWWWW :o}
    function FillRefPoints(var RefPoints: TRefArray): Boolean;
//    function FillRefPoints(RefPoints: TRefArray): Boolean;
    function NameCoincides(ImageFileName: AnsiString): Boolean;
    function LastErrorText: AnsiString;
  end;

const
  SyntaxName: array[stDefinition..stComment] of AnsiString =
    (
    'Definition Table', 'File', '(',
    'Units', 'Type', '', '!'
    );

  CoordFormat = '(%.10f,%.10f) (%.10f,%.10f)%s'; // Format string for coordinates
  ICoordFormat = '(%.10d,%.10d) (%.8d,%.8d)%s'; // Format string for coordinates

  LabelA: AnsiString = 'Label "Lower Left Corner"';
  LabelB: AnsiString = 'Label "Upper Right Corner"';
  UnitsName: array[tuMM..tuIN] of UnitsDef =
    (
    (SName: 'MM'; LName: 'Millimet'; Mult: 10.0),
    (SName: 'CM'; LName: 'Centimet'; Mult: 1.0),
    (SName: 'M'; LName: 'Met'; Mult: 100.0),
    (SName: 'KM'; LName: 'Kilomet'; Mult: 0.00001),
{$IFDEF VMSI}
    (SName: 'PX'; LName: 'Pix'; Mult: 1.0),
{$ENDIF}
    (SName: 'IN'; LName: 'Inch'; Mult: 2.54)
    );

  TabFilter = '|PROGIS TAB files(*.tab)|*.TAB';

  TabExtension = '.tab';
  TabExt = TabExtension;

  BAKExtension = '.bak';
  BAKExt = BAKExtension;

  TabVersion = '1.1';

// Output function of tab file!

function FPoint(X: Double; Y: Double): TFPoint;
function FRect(Left: Double; Top: Double; Right: Double; Bottom: Double): TFRect;

function CreateTABFile(FileName: AnsiString; Width, Height: Integer;
  ClipRect: PRect; RefPoints: array of TRefPoint;
  NewExt: AnsiString = TabExtension;
  LastComment: AnsiString = ''): Boolean;

function StrToUnits(Str: AnsiString): TabUnits;
function UnitsMultFactor(Units: TabUnits): double;
function MakeImageList(FileMask: AnsiString): TStringList;
function WildCardExists(FileName: AnsiString): Boolean;

implementation
{ TTABFile }

uses
  UserUtils, TextUtils, TrLib32, Adler
{$IFNDEF VMSI}
  ,
  SelectFromWildCards
{$ENDIF}
  ;

//-----------------------------------------------------

function WildCardExists(FileName: AnsiString): Boolean;
begin
  Result := (Pos('*', FileName) + Pos('?', FileName)) > 0;
end;

//-----------------------------------------------------

function FPoint(X: Double; Y: Double): TFPoint;
begin
  Result.X := X;
  Result.Y := Y;
end;

function FRect(Left, Top, Right, Bottom: Double): TFRect;
begin
  Result.Left := Left;
  Result.Top := Top;
  Result.Right := Right;
  Result.Bottom := Bottom;
end;

function StrToUnits(Str: AnsiString): TabUnits;
var
  i: TabUnits;
  L: integer;
begin
  Result := tuUnknown;
  for i := tuMM to tuIN do
  begin
    L := Length(UnitsName[i].LName);
          // Now it accepts mm, cm, meters, km, inches  ONLY! Note it...
    if (CompareText(UnitsName[i].SName, Str) = 0) // Short name coincidence!
      or
      (StrLIComp(PChar(UnitsName[i].LName), PChar(Str), L) = 0) then // Long name coincidence!
    begin
      Result := i;
      break;
    end;
  end;
end;

function UnitsMultFactor(Units: TabUnits): double;
begin
  if Units <> tuUnknown then
    Result := UnitsName[Units].Mult
  else
    Result := 0.0; // Just in inform about error
end;

function FileNameType(FileName: AnsiString): ExecType;
begin
  Result := etNone;
  if FileName = EmptyStr then { No name - no type}
    Exit;
  if DirectoryExists(FileName) then
  begin
    Result := etDir;
  end
  else
    if FileExists(FileName) then
    begin
      Result := etFile;
    end
    else
      Exit;
end;

constructor TTABFile.Create(FileName: AnsiString = '');
begin
  inherited Create;
  fMultiply := 1.0; // for "CentiM"eters convert to other units (in future :)
  fWidth := 0;
  fHeight := 0;
  PointsCnt := 0;
  SetLength(ImagePoints, 0);
  SetLength(ProjPoints, 0);
  fNameList := TStringList.Create;
  LoadTab(FileName);
end;

destructor TTABFile.Destroy;
begin
  { Clear arrays just in case }
  SetLength(ImagePoints, 0);
  SetLength(ProjPoints, 0);
  fNameList.Destroy;
  inherited Destroy;
end;

// For future developer[s]

function TTABFile.Execute: Boolean;
begin
  Result := False;
  try
    case fExecType of
      etNone: Exit;
      etDir: ;
      etFile: ;
    end;
  finally
    fExecType := etNone;
  end;
end;

function TTABFile.fProcessTAB: Boolean;
var
  i: Integer;
begin
  Result := False;
  try
    if not FileExists(fFileName) then
    begin
      fErrorCode := teFileNotExists;
      Exit;
    end;
{$I-}
    AssignFile(TabFile, fFileName); { File selected in dialog box }
    Reset(TabFile);
{$I+}
    if IoResult <> 0 then
    begin
      fErrorCode := teFileReadError;
      Exit;
    end;
    try
    { Read TAB file content }
      LineCnt := 0; { Reset read line counter }
      Result := fParseTAB;
    finally
      CloseFile(TabFile);
    end;
  finally
    if not Result then
      fExecType := etNone
    else
      for i := 0 to PointsCnt - 1 do
      begin
        ProjPoints[i].X := ProjPoints[i].X * fMultiply;
        ProjPoints[i].Y := ProjPoints[i].Y * fMultiply;
      end;
  end;
end;

{ ----------- Main parser procedure -------------- }

function TTABFile.fParseTAB: Boolean;
var
  Line, Tmp, Body: AnsiString;
  Word: AnsiString;
  P, P1: Integer;
  i: TabUnits;
  Pt1: TFPoint;
  Pt2: TFPoint;
  SyntaxSum: set of SyntaxType;
  SType: SyntaxType;
  Sep: AnsiString;
  {----------------------------------}
  function WhatSyntaxType: SyntaxType;
  begin
    for Result := stDefinition to stComment do
      if StrLIComp(PChar(Line), PChar(SyntaxName[Result]),
        Length(SyntaxName[Result])) = 0 then { Found }
        Exit;
    Result := stUnknown;
  end;
  {---------------------}
  procedure Whites2Spaces;
  begin
{    for I := 1 to Length(Line) do
      if Ord(Line[I]) < Ord(#32) then
        Line[I] := #32; // Replace whites with spaces! }
    Q_SpaceCompressInPlace(Line);
  end;
  {-------------------------}
  function ReadLine: Boolean;
  begin
    Result := False;
    if Eof(TabFile) then
      Exit;
{$I-}
    ReadLn(TabFile, Line);
{$I+}
    Result := IOResult = 0;
    if Result then
    begin
      Line := AdjustLineBreaks(Line);
      Inc(LineCnt);
    end;
  end;
{++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++}
begin
  Result := False;
  SyntaxSum := []; // Clear syntax checker
  while ReadLine do
  begin
    Whites2Spaces; // reduce consequent spaces and trim string
//    Line := Trim(Line);
    if Line = EmptyStr then { skip blank line }
      Continue;
    // Detect line type
    SType := WhatSyntaxType;
    // Count used type
    Include(SyntaxSum, SType);
    case SType of
      stType, stBlank, stComment, stUnknown: ; { skip non parsed line }
      stDefinition:
        begin
          PointsCnt := 0; { Reset counter}
        end;
      stFileName:
        begin
          P := Pos('"', Line);
          if P = 0 then // Check another separator
          begin
            P := Pos('''', Line);
            if P = 0 then
            begin
              fErrorCode := teNoImageName;
              Exit;
            end;
            Sep := '''';
          end
          else
            Sep := '"';
          P1 := Pos(Sep, Copy(Line, P + 1, MaxInt));
          if (P = 0) or (P1 = 0) then { No "" pair was found }
            Exit;
          fImageName := Trim(Copy(Line, P + 1, P1 - 1));
        end;

      stCoord: { Coordinate line is detected }
// Example line to parse: (4470000,7010000) (0,0) Label "Upper left Point",
        begin
          try
            Tmp := Copy(Line, 2, MaxInt);
            Tmp := Trim(Tmp);
            // Seek 1st comma
            P := Pos(',', Tmp);
            // and  1st closing bracket
            P1 := Pos(')', Tmp);
            if (P = 0) or (P1 = 0) then
              Exit;
            // Fill project coordinates
            try
              Pt1.x := StrToFloat(Copy(Tmp, 1, P - 1));
              Pt1.y := StrToFloat(Copy(Tmp, P + 1, P1 - P - 1));
            except // Error during conversion
              fErrorCode := teBadCoords;
              Exit;
            end;
            // Go to the next coordinate pair...
            P := Pos('(', Tmp);
            Tmp := Copy(Tmp, P + 1, MaxInt);
            Tmp := Trim(Tmp);
            // Seek 1st comma
            P := Pos(',', Tmp);
            // and  1st closing bracket
            P1 := Pos(')', Tmp);
            if (P = 0) or (P1 = 0) then
              Exit;
            // Fill image coordinates
            try
              Pt2 := FPoint(StrToFloat(Copy(Tmp, 1, P - 1)), StrToFloat(Copy(Tmp, P + 1, P1 - P - 1)));
            except
              fErrorCode := teBadCoords;
              Exit;
            end;
          except
            fErrorCode := teSyntaxCheck;
            Exit;
          end;
          { Expand arrays }
          if Length(ImagePoints) <= PointsCnt then
          begin
            SetLength(ImagePoints, Length(ImagePoints) + 4);
            SetLength(ProjPoints, Length(ProjPoints) + 4);
            SetLength(PointType, Length(ProjPoints) + 4);
          end;
          ProjPoints[PointsCnt] := Pt1;
          ImagePoints[PointsCnt] := Pt2;
          // Check for Adler checksum presented at end of line
          PointType[PointsCnt] := ptNormal;
          P := Pos('$', Line);
          if P > 0 then
          begin
            try
              P1 := Integer(HexToUint(Copy(Line, P + 1, MaxInt)));
              if DWORD(P1) = Adler32(0, PByte(Copy(Line, 1, P - 1)), P - 1) then
              begin
                if Pos(LabelA, Line) > 0 then
                  PointType[PointsCnt] := ptA
                else
                  if Pos(LabelB, Line) > 0 then
                    PointType[PointsCnt] := ptB;
              end;
            except
            end;
          end;
          Inc(PointsCnt);
        end;
      stUnits: { Units type was detected }
        begin
          Word := Q_GetWordN(2, Line); // Get second word
          i := StrToUnits(Word);
          if i = tuUnknown then // Illegal units used
          begin
            fErrorCode := teSyntaxCheck;
            Exit;
          end
          else
            FMultiply := UnitsMultFactor(i);
        end;
    end;
  end;

  // Check all needed syntax elements to be present

  if not (stDefinition in SyntaxSum) then
  begin
    fErrorCode := teNoDefinition;
    Exit;
  end;

  //++ Sygsky 31-AUGUST-2001 by Freddie request I add follow:
  //
  // Lower I we will check the file name.
  // Please note that there are follow options:
  // 1.If  a  file  name  not  found in TAB file, I will assign new name in a
  // "*.*" form.
  // 2. If file name contains "wild cards" then I will find all consequence
  // files in the TAB directory.
  // 3. If only ONE file was found from previous steps, I will use it.
  // 4. If multiple file were found, user should to point what a file he will
  // prefer in this case.
  //-- Sygsky

  if not (stFileName in SyntaxSum) then // No "File 'Disk:\Dir[s]\FIleName.Ext'" item was detected
    fImageName := ExtractFileNameWithNoExt(fFileName) + '.*'; // So find any available files (images of course!)
  if not (stCoord in SyntaxSum) then
  begin
    fErrorCode := teNoCoord;
    Exit;
  end;
  if not (stUnits in SyntaxSum) then
  begin
    fErrorCode := teNoUnits;
    Exit;
  end;

  // Check wild cards in extension ONLY
  Body := ExtractFileNameWithNoExt(fImageName);
  if WildCardExists(Body) then // Wild cards in BODY!
  begin
    fErrorCode := teImageNameDifferent;
    Exit;
  end;

  // TAB and image has equal names?
  if (CompareText(Body, ExtractFileNameWithNoExt(fFileName)) = 0) then
  begin
    Tmp := ExtractFileExt(fImageName);
    if not WildCardExists(Tmp) then // Simple file name used
    begin
      if not FileExists(FullImageName) then // And file not exists :o(
      begin
        fErrorCode := teNoImageName;
        Exit;
      end;
    end
    else // Wildcards used in file name
    begin
      fErrorCode := teCancelled; // Put this error code just in case
                                 // if all will be OK, nobody will see it
                                 // but if user will cancel, he will :o)
    end;
    UpdateNames; // Fill names if exists
  end
  else // Not the same name as for
  begin
    fErrorCode := teImageNameDifferent;
    Exit;
  end;

  if Eof(TabFile) then
    Result := True;
end;

// ----- Use this function to init process ------

function TTABFile.LoadTAB(FileName: AnsiString): Boolean;
begin
  Result := False;
  fClrError;
  fExecType := FileNameType(FileName);
  case fExecType of
    etNone:
      begin
        fErrorCode := teFileNotExists;
        Exit;
      end;
    { Directory with all TAB files in it }
    etDir: fFileName := FileName;
    { Single TAB file }
    etFile:
      begin
        fFileName := FileName;
        if not fProcessTAB then { Not TAB file }
          Result := False
        else
          Result := fCheckBestPoints; { All points are good ones? }
        if not Result then
        begin
          fExecType := etNone;
          Exit;
        end;
      end;
  end;
  Result := True;
end;

{function TTABFile.fExecTAB: Boolean;
begin
  Result := False;
end;}

function TTABFile.fGetDirName: AnsiString;
begin
  Result := EmptyStr;
  if fExecType = etDir then
    Result := fFileName
  else
    Result := ExtractFilePath(fFileName);
end;

function TTABFile.fGetFileName: AnsiString;
begin
  Result := EmptyStr;
  if fExecType = etFile then
    Result := fFileName;
end;

function TTABFile.fGetImageName: AnsiString;
begin
  Result := EmptyStr;
  if fExecType = etFile then
    Result := fImageName;
end;

function TTABFile.fGetFullImageName: AnsiString;
begin
  Result := EmptyStr;
  if fExecType = etFile then
    Result := ChangeFileExt(fFileName, ExtractFileExt(fImageName));
end;

function TTABFile.fIsValid: Boolean;
begin
  Result := fExecType <> etNone;
end;

{------------------------------------}

function TTABFile.fCheckBestPoints: Boolean;
var
{$ifdef FUTURE} // Here I will try to find best points from set
  I, J, MinI, MinJ: Integer;
  Res, MinDiff, Diff: Double;
{$else}
  I, IA, IB: Integer;
  W, H: Double;
  PW, PH : Double;
{$endif}

begin
  Result := False;
{$ifdef FUTURE}
  if fExecType <> etFile then
    Exit;
  MinI := -1;
  MinDiff := 10E+30;
  for I := 0 to PointsCnt - 2 do
    for J := I + 1 to PointsCnt - 1 do
    begin
      Res := (ProjPoints[I].x - ProjPoints[J].x);
      Diff := (ImagePoints[I].y - ImagePoints[J].y);
      if (Diff = 0) or (Res = 0) then
        Continue;
      Diff := Res / Diff;
      if (Abs(Diff) < MinDiff) then { Smallest dispersion detected }
      begin
        MinDiff := Diff;
        MinI := I;
        MinJ := J;
      end;
    end;
  if MinI = -1 then
  begin
    fErrorCode := teBadCoords;
    Exit;
  end;
  { Ok, I found the best conseguent points, now copy them to the start of
    array and hide all others! }
  ProjPoints[0] := ProjPoints[MinI];
  ImagePoints[0] := ImagePoints[MinI];
  ProjPoints[1] := ProjPoints[MinJ];
  ImagePoints[1] := ImagePoints[MinJ];
  PointsCnt := 2;
{$else}
  if PointsCnt < 2 then
  begin
    fErrorCode := teBadCoords;
    Exit;
  end;
  // First try to find directly set points A&B (if TAB was created by WinGIS :o)
  IA := -1;
  IB := -1;
  for I := 0 to Pred(PointsCnt) do
    case PointType[I] of
      ptNormal: ;
      ptA: IA := I;
      ptB: IB := I;
    end;
  if (IA = -1) or (IB = -1) then // So don't pay attention on TAB comments
  begin
  { Set from last point to reduce comparison counts }
    fAPoint := ProjPoints[PointsCnt - 1];
    fBPoint := fAPoint;
    fAPointI := ImagePoints[PointsCnt - 1];
    fBPointI := fAPointI;
    for I := 0 to PointsCnt - 2 do
    begin
      if fAPoint.X > ProjPoints[I].X then
      begin
        fAPoint.X := ProjPoints[I].X;
        fAPointI.X := ImagePoints[I].X;
      end;
      if fAPoint.Y > ProjPoints[I].Y then
      begin
        fAPoint.Y := ProjPoints[I].Y;
        fAPointI.Y := ImagePoints[I].Y;
      end;
      if fBPoint.X < ProjPoints[I].X then
      begin
        fBPoint.X := ProjPoints[I].X;
        fBPointI.X := ImagePoints[I].X;
      end;
      if fBPoint.Y < ProjPoints[I].Y then
      begin
        fBPoint.Y := ProjPoints[I].Y;
        fBPointI.Y := ImagePoints[I].Y;
      end;
    end;
    W := Abs(fBPoint.X - fAPoint.X); // Width of project
    PW := Abs(W / (fBPointI.X - fAPointI.X)); // Width of pixel image
    H := Abs(fBPoint.Y - fAPoint.Y); // height of project
    PH := Abs(H / (fBPointI.Y - fAPointI.Y)); // Height of image
    fClipRect := Rect(Round(APoint.X - PW/2), Round(APoint.Y - PH/2),
                      Round(BPoint.X + PW/2), Round(BPoint.Y + PH/2));
  end
  else // A and B were found from TAB, so use them as output if needed
  begin
    fAPoint := ProjPoints[IA];
    fBPoint := ProjPoints[IB];
    // Don't round the coordinates as they are already generated from project units
    fClipRect := Rect(Round(APoint.X), Round(APoint.Y), Round(BPoint.X), Round(BPoint.Y));
  end;
{$endif}
  Result := True;
end;

function TTABFile.fGetProjPoint(Index: Integer): TFPoint;
begin
  Result.X := -1;
  Result.Y := -1;
  if (fExecType = etFile) and (Index < PointsCnt) and (Index >= 0) then
  begin
    Result.X := {Round}(ProjPoints[Index].X);
    Result.Y := {Round}(ProjPoints[Index].Y);
  end;
end;

function TTABFile.fGetClipRect: TRect;
begin
  Result := fClipRect;
end;

function TTABFile.fGetProjRect: TFRect;
begin
  Result := FRect(APoint.X, APoint.Y, BPoint.X, BPoint.Y);
end;

function  TTABFile.fGetPixRect: TRect;
begin
  Result :=  Rect(Round(APoint.X), Round(APoint.Y), Round(BPoint.X), Round(BPoint.Y));
end;

function TTABFile.fGetImagePoint(Index: Integer): TFPoint;
begin
  Result.x := -1;
  Result.y := -1;
  if (fExecType = etFile) and (Index < PointsCnt) and (Index >= 0) then
  begin
    Result := ImagePoints[Index];
  end;
end;

procedure TTABFile.fClrError;
begin
  fErrorCode := teNone;
end;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Parameters:
//         FileName: Image file name with full path.
//         Width:    Image width in pixels
//         Height:   Image height iin pixels
//         ClipRect: Clipped rectangle of image in project units - you can
//                   leave it anassigned and not useable
//         RefPoints: array of reference points for image adjustment (i.e. Omega)
//         NewExt:   extension for newly created TAB
//         LastComment: if non-empty, will be added as a last comment line[s].
//                       Note that you can use [CR+]LF to embed multiple line here :o)
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

function CreateTABFile(FileName: AnsiString; Width, Height: Integer;
  ClipRect: PRect; RefPoints: array of TRefPoint;
  NewExt: AnsiString = TabExtension;
  LastComment: AnsiString = ''): Boolean;
var
  TABFile: TextFile;
  TabFileName, BAKName, Str: AnsiString;
  DT: TDateTime;
  I: Integer;
  Year, Month, Day: Word;
begin
  Result := False;
  TabFileName := ChangeFileExt(FileName, NewExt);
  if FileExists(TabFileName) then // Try to bak it
  begin
    BAKName := TabFileName + BAKExtension;
    DeleteFile(BAKName); // Just in case
    if not RenameFile(TabFileName, BAKName) then // Unsuccessfull to rename, try to delete
      if not DeleteFile(TabFileName) then // Not deleted!!
        Exit; // :(((
  end;
  AssignFile(TABFile, TabFileName); { TAB file for image }
{$I-}
  ReWrite(TABFile);
{$I+}
  if IoResult <> 0 then
    Exit;
  try
    DT := Now;
    WriteLn(TabFile, '! This is a TAB file for WinGIS image v. ' + TabVersion);
    DecodeDate(DT, Year, Month, Day);
    WriteLn(TabFile, Format('! Created at %s %.2d-%s-%.4d, %s', [
      TimeToStr(DT),
        Integer(Day), UpperCase(Q_SMonthsEng[Month]), Integer(Year),
        Q_WeekDaysEng[DayOfWeek(DT)]
        ]));
    WriteLn(TabFile, '! A comment is inserted using a ''!''');
    Writeln(TabFile, '! Coordinates are represented in a follow order: Project X, Y, Image X, Y');
    Writeln(TabFile, '! WinGIS uses centimeters ("' + UnitsName[tuCM].SName + '") as default units!');
    WriteLn(TabFile);
    WriteLn(TabFile, 'Definition Table');
    WriteLn(TabFile, 'File "' + ExtractFileName(FileName) + '"');
    if Assigned(ClipRect) then
      with ClipRect^ do
      begin
        if Top < Bottom then
          XChgDWORD(DWORD(Top), DWORD(Bottom));
        if Left > Right then
          XChgDWORD(DWORD(Left), DWORD(Right));
        Str := Format(ICoordFormat,
          [Left, Bottom, 0, Height - 1, ' ' + LabelA]);
        WriteLn(TabFile, Str + '$' + IntToHex(Adler32(0, PByte(Str), Length(Str)), 8));
        Str := Format(ICoordFormat,
          [Right, Top, Width - 1, 0, ' ' + LabelB]);
        WriteLn(TabFile, Str + '$' + IntToHex(Adler32(0, PByte(Str), Length(Str)), 8));
      end;
    if Length(RefPoints) > 0 then
      for i := Low(RefPoints) to High(RefPoints) do
        WriteLn(TabFile, Format(CoordFormat,
          [RefPoints[i].Proj.X, RefPoints[i].Proj.Y,
           RefPoints[i].Img.X, RefPoints[i].Img.Y,
            ' Label "Ref. point #' + IntToStr(i + 1) + '"']));
    WriteLn(TabFile, Format('Units "%s"', [UnitsName[tuCM].SName]));
    LastComment := Trim(LastComment);
    // Append any user defined comment (if defined)
    if LastComment <> EmptyStr then // Add user define comment
    begin
      WriteLn(TabFile, ''); // Skip one line
      WriteLn(TabFile, '! All following lines were added from external module:'); // Skip one line
      LastComment := Q_DelChar(LastComment, #10); // remove LF
      for I := 1 to MaxInt do
      begin
        BAKName := Q_GetWordN(I, LastComment, [#13]); // Get next line
        if BAKName = EmptyStr then // No more lines, exit
          break; // No more comments
//        BAKName := Trim(BAKName);
        WriteLn(TabFile, '! ' + BAKName);
      end;
    end;
  finally
    CloseFile(TabFile);
  end;
  Result := True;
end;

function TTABFile.FillRefPoints(var RefPoints: TRefArray): Boolean;
var
  i: Integer;
begin
  Result := False;
  if not fIsValid then
    Exit;
  SetLength(RefPoints, PointsCnt);
  for i := 0 to PointsCnt - 1 do
  begin
    RefPoints[i].Proj := ProjPoints[i];
    RefPoints[i].Img := ImagePoints[i];
  end;
  Result := True;
end;

function TTABFile.NameCoincides(ImageFileName: AnsiString): Boolean;
begin
  Result := False;
  if not fIsValid then
    Exit;
  Result := CompareText(ExtractFileName(ImageFileName), ImageName) = 0;
end;

function TTABFile.LastErrorText: AnsiString;
const
  ErrorTexts: array[teNone..teDuplicatedOnLayer] of AnsiString =
    (
    'No errors', 'Unknown error, ask Sygsky@yandex|rambler|mail.ru',
    'Pointed image file not found', 'Error read TAB file',
    'Syntax error of TAB', 'Some mandatory syntax is absent',
    'Bad coordinate data', 'No "Definition Table" field',
    'No "File" field', 'No one coord detected',
    'No "Units" field', 'Image name doesn''t coincide with TAB name',
    'Cancelled by user',
    'Already present on the layer'
    );
begin
  Result := ErrorTexts[fErrorCode];
end;

//---------------------------------------

function TTABFile.HasWildCards: Boolean;
begin
  Result := WildCardExists(fImageName);
end;

//---------------------------------------
// Gets user selection in case of multiple file used

function TTABFile.GetUserName: AnsiString;
begin
{$IFNDEF VMSI}
  if HasWildCards then
    Result := NameFromWildCards(FullImageName, fFileName)
  else
{$ENDIF}
    Result := FullImageName;
end;

//---------------------------------------

function TTABFile.GetEntry(Index: Integer): AnsiString;
begin
  if HasWildCards then
    if (Index >= 0) and (Index < fNameList.Count) then
      Result := fNameList.Strings[Index]
    else
      Result := EmptyStr;
end;

//---------------------------------------

// This procedure

function MakeImageList(FileMask: AnsiString): TStringList;
var
  SearchRec: TSearchRec;
  ErrCode: Integer;
  Ext: AnsiString;
const
  faOrdinaryFile = faReadOnly + faArchive;
begin
  Result := TStringList.Create; // Now count of images found = 0
  ErrCode := FindFirst(FIleMask, faOrdinaryFile, SearchRec);
  try
    if ErrCode <> 0 then
      Exit;
{(*}
    repeat
      Ext := ExtractFileExt(SearchRec.Name);
      if CompareText(Ext, TABExtension) <> 0 then
        if CompareText(Ext, BAKExtension) <> 0 then
          if  FileIsImageOne(SearchRec.Name) then
            Result.Add(ExtractFileName(SearchRec.Name)); // Bump the counter
    until (FindNext(SearchRec) <> 0);  // while not last file found
{*)}
  finally
    FindClose(SearchRec);
  end;
end;

procedure TTABFile.UpdateNames;
var
  NewNames: TStringList;
begin
  NewNames := MakeImageList(fGetFullImageName);
  fNameList.Assign(NewNames); // Clear old contents and load new from mask
  NewNames.Free;
end;

function TTABFile.fGetImageCount: Integer;
begin
  Result := fNameList.Count;
end;

end.

