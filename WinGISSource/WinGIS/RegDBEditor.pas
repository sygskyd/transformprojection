Unit RegDBEditor;

Interface

Uses Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,StdCtrls,
     WCtrls,TreeView,RegDB, ColumnList, ExtCtrls;

Type TRegDBEditorForm = class(TForm)
       Panel1: TPanel;
       TreeView: TTreeListView;
       ValuesList: TColumnListbox;
    Splitter1: TSplitter;
    Panel2: TPanel;
    Button1: TButton;
       Procedure TreeViewClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ValuesListColumns1Paint(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure ValuesListColumns2Paint(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Button1Click(Sender: TObject);
      private
       FRegistry   : TRegistryDatabase;
       FValues     : TStringList;
       Procedure   FreeValues;
       Procedure   SetRegistry(ARegistry:TRegistryDatabase);
       Procedure   UpdateValueList(const KeyName:String);
       Procedure   UpdateTree;
      public
       Property    Registry:TRegistryDatabase write SetRegistry;
     end;

var RegDBEditorForm : TRegDBEditorForm;

Implementation

{$R *.DFM}

Procedure TRegDBEditorForm.SetRegistry(ARegistry:TRegistryDatabase);
begin
  FRegistry:=ARegistry;
  UpdateTree;
end;

Procedure TRegDBEditorForm.UpdateTree;
  Procedure AddTree(Parent:TTreeEntry;const Name:String);
  var Item         : TTreeEntry;
      Items        : TStringList;
      Cnt          : Integer;
  begin
    Items:=TStringList.Create;
    FRegistry.GetSubKeys(Name,Items);
    for Cnt:=0 to Items.Count-1 do begin
      Item:=TTreeEntry.Create;
      Item.Caption:=Items[Cnt];
      Parent.Add(Item);
      AddTree(Item,Name+'\'+Items[Cnt]);
    end;
  end;
begin
  TreeView.BeginUpdate;
  TreeView.Tree.Clear;
  AddTree(TreeView.Tree,'\');
  TreeView.EndUpdate;
  Visible:=TRUE;
end;

Procedure TRegDBEditorForm.TreeViewClick(Sender: TObject);
var Entry          : TTreeEntry;
    KeyName        : String;
begin
  Entry:=TTreeEntry(TreeView.Tree.ExpandedItems[TreeView.ItemIndex]);
  KeyName:='';
  while Entry<>NIL do begin
    KeyName:=Entry.Caption+'\'+KeyName;
    Entry:=TTreeEntry(Entry.Parent);
  end;  
  UpdateValueList(KeyName);
end;

Procedure TRegDBEditorForm.UpdateValueList(const KeyName:String);
var Items          : TStringList;
    Cnt            : Integer;
begin
  Items:=TStringList.Create;
  FRegistry.OpenKey(KeyName,FALSE);
  FRegistry.GetValues('',Items);
  FreeValues;
  for Cnt:=0 to Items.Count-1 do FValues.AddObject(Items[Cnt],FRegistry.GetEntryInfo(Items[Cnt]));
  Items.Free;
  ValuesList.Items.BeginUpdate;
  ValuesList.Count:=FValues.Count;
  ValuesList.Items.EndUpdate;
end;

procedure TRegDBEditorForm.FormCreate(Sender: TObject);
begin
  FValues:=TStringList.Create;
end;

procedure TRegDBEditorForm.FormDestroy(Sender: TObject);
begin
  FValues.Free;
end;

Procedure TRegDBEditorForm.FreeValues;
var Cnt            : Integer;
begin
  for Cnt:=0 to FValues.Count-1 do FValues.Objects[Cnt].Free;
  FValues.Clear;
end;

procedure TRegDBEditorForm.ValuesListColumns1Paint(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  TextRectEx(ValuesList.Canvas,Rect,FValues[Index],ValuesList.Color,State);
end;

procedure TRegDBEditorForm.ValuesListColumns2Paint(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
var Item           : TRegistryEntryInfo;
    Text           : String;
begin
  Item:=TRegistryEntryInfo(FValues.Objects[Index]);
  case Item.DataType of
    rtText: Text:=FRegistry.ReadString(Item.Name);
    rtInteger: Text:=IntToStr(FRegistry.ReadInteger(Item.Name));
    rtBool: if FRegistry.ReadBool(Item.Name) then Text:='TRUE'
            else Text:='FALSE';
    rtFloat: Text:=FloatToStr(FRegistry.ReadFloat(Item.Name));
    rtBinary: Text:=IntToStr(Item.DataSize);
    rtDateTime : Text:=DateTimeToStr(FRegistry.ReadDateTime(Item.Name));
    else Text:='';
  end;
  ValuesList.Canvas.Font.Color:=clWindowText;
  TextRectEx(ValuesList.Canvas,Rect,Text,ValuesList.Color,[]);
end;

procedure TRegDBEditorForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action:=caHide;
end;

procedure TRegDBEditorForm.Button1Click(Sender: TObject);
begin
  UpdateTree;
end;

Initialization
  RegDBEditorForm:=TRegDBEditorForm.Create(Application);

end.
