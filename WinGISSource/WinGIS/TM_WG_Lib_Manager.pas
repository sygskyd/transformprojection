unit TM_WG_Lib_Manager;

interface

Uses Windows, Graphics, Forms, Gauges,
     IDB_CallbacksDef;

Type
    TInitLibrary = Function(AnApplication: TApplication;
                            AMainForm: TForm;
                            pcLanguageFileExtension: PChar;
                            pcMainWindowTitle: PChar): Boolean; stdcall;
    TFinLibrary = Procedure; stdcall;
    TGetVersion = Function: PChar; stdcall;
    TStartTM = Function(AProject: Pointer;                           // Pointer to a project of WinGIS
                        ALayer: Pointer;
                        iDatabaseType: Integer;                      // Type of attribute database
                        pcDatabaseFileName: PChar;                   // File of attribute database
                        pcUserName: PChar;                           // User name in the attribute database
                        pcUserPassword: PChar;                       // Password of the user of the attribute database

                        pcAttributeTableName: PChar;                 // Name of attribute tabel
                        pcProgisIDFieldName: PChar;                  // The field that contains ProgisID data
                        pcDefaultDataSourceField: PChar;             // This field will be seleceted as data source by default                        
                        pcListOfFieldsThatAreNotToBeVisible: PChar;  // The list of internal usage fields. Names of these
                                                                     //     have not to be visible for the user.
                        iSchemeSourceType: Integer;                  // Type of source of thematic map scheme
                        pcSchemeDatabaseFileName: PChar;             // Name of file in which the thematic map schemes
                                                                     //      are storaged
                        pcSchemeUserName: PChar;                     // User name (if the schemes are storgaed in database file)
                        pcSchemePassword: PChar;                     // Password (if the schemes are storgaed in database file)
                        pcSchemeStorageTableName: PChar;             // Table name (if the schemes are storgaed in database file)
                        pcSchemeStorageFieldName: PChar;             // Field name (if the schemes are storgaed in database file)
                        AGauge: TGauge): Boolean;
                        stdcall;

    TGetStylesCount = Function(AProject: Pointer): Integer; stdcall;
    TDrawStyle = Procedure(AProject: Pointer; iStyleMode: Integer; iStyle: Integer; ACanvas: TCanvas; ARect: TRect); stdcall;
// >>     TSetObjectStyle = Function(AProject, ALayer: Pointer; iItemID, iLineColor, iLineStyle, iLineWidth, iPatternColor, iPatternStyle, iTransparency: Integer; iObjectSize: Integer): Boolean; stdcall;

    TSetGetStylesCountCallback = Procedure(AFunction: TGetStylesCount); stdcall;
    TSetDrawStyleCallback = Procedure(AProcedure: TDrawStyle); stdcall;
    TSetSetObjectStyleCallback = Procedure(AFunction: TSetObjectStyle); stdcall;

    TTMLib = Class
       Private
          bTheLibraryWasInitialized: Boolean;
          hTMLibrary: THandle;

          InitLibraryFunc: TInitLibrary;
          FinLibraryProc: TFinLibrary;

          GetVersionFunc: TGetVersion;
          StartTMFunc: TStartTM;

          SetGetFillStylesCountCallbackProc: TSetGetStylesCountCallback;
          SetGetLineStylesCountCallbackProc: TSetGetStylesCountCallback;
          SetDrawStyleCallbackProc: TSetDrawStyleCallback;
          SetSetObjectStyleCallbackProc: TSetSetObjectStyleCallback;
       Public
          Constructor Create(AnApplication: TApplication; sTMLibFileName, sLanguageFileExtension, sMainWindowTitle: AnsiString);
          Destructor Free;

          Function GetLibraryVersion: AnsiString;
          Function LibraryWasLoaded: Boolean;

          Procedure SetSetObjectStyleCallback(AFunction: TSetObjectStyle);

          Function StartTM(AProject: Pointer;
                           ALayer: Pointer;
                           iDatabaseType: Integer;
                           sDatabaseFileName,
                           sUserName,
                           sUserPassword,
                           sAttributeTableName,
                           sProgisIDFieldName,
                           sDefaultDataSourceField,
                           sListOfFieldsThatAreNotToBeVisible: AnsiString;
                           iSchemeSourceType: Integer;
                           sSchemeDatabaseFileName,
                           sSchemeUserName,
                           sSchemePassword,
                           sSchemeStorageTableName,
                           sSchemeStorageFieldName: AnsiString;
                           AGauge: TGauge): Boolean;
       End;

implementation

Uses SysUtils, Classes,
     MultiLng,
     AM_Proj, AM_Def, XFills, XLines, NumTools, XStyles, IDB_Man, AM_Main;

Function GetXLineStylesCount(AProject: Pointer): Integer; stdcall;
Begin
     RESULT := PProj(AProject).PInfo.ProjStyles.XLineStyles.Count;
End;

Function GetXFillStylesCount(AProject: Pointer): Integer; stdcall;
Begin
     RESULT := PProj(AProject).PInfo.ProjStyles.XFillStyles.Count;
End;

{ This procedure draws a fill or line style in a rect of a canvas.
  iStyleMode: 0 = fill style will be drawn;
              1 = line style will be drawn.
  iStyle - number of the style in the style list. }
Procedure DrawStyle(AProject: Pointer; iStyleMode: Integer; iStyle: Integer; ACanvas: TCanvas; ARect: TRect); stdcall;
Var
   AXFillItem: TCustomXFill;
   AXLineItem: TXLineStyle;

   Points: TRectCorners;
   X, Y: Integer;
   LinePoints: Array[0..1] of TPoint;
   AZoom: Double;

   sText: AnsiString;
Const
     SystemFillStyles = 8;
     SystemLineStyles = 5;
Begin
     Case iStyleMode Of
     0: Begin // Fill style
        If iStyle < SystemFillStyles-1 Then
           ACanvas.Rectangle(ARect.Left, ARect.Top, ARect.Right, ARect.Bottom)
        Else Begin
           AXFillItem := PProj(AProject).PInfo.ProjStyles.XFillStyles.Styles[iStyle-SystemFillStyles+1];
           RectCorners(InflatedRect(ARect, 0, -1), Points);
           If AXFillItem IS TVectorXFill Then
              With AXFillItem AS TVectorXFill Do
                   XLFillPolygon(ACanvas.Handle, Points, 4, StyleDef^.XFillDefs, StyleDef^.Count, 0, 0, 5/MinDistance, 0)
           Else
              If AXFillItem IS TBitmapXFill Then
                 With AXFillItem AS TBitmapXFill Do BMFillPolygon(ACanvas.Handle, Points, 4, StyleDef(ACanvas.Handle)^.BitmapHandle, srcCopy, 1);
           End;
        End;
     1: Begin // Line style
        Y := CenterHeightInRect(0, ARect);
        If iStyle < SystemLineStyles Then Begin
           ACanvas.Polyline([Point(ARect.Left, Y-1), Point(ARect.Right, Y-1)]);
           ACanvas.Polyline([Point(ARect.Left, Y), Point(ARect.Right, Y)]);
           End
        Else Begin
           AXLineItem := PProj(Aproject).PInfo.ProjStyles.XLineStyles.Styles[iStyle-SystemLineStyles];
           LinePoints[0] := Point(ARect.Left+1, Y);
           LinePoints[1] := Point(ARect.Right-1, Y);
           If RectHeight(AXLineItem.ClipRect) = 0 Then
              AZoom := (ARect.Right - ARect.Left) / AXLineItem.Items[0].Cycle / 5
           Else
              AZoom := (ARect.Bottom - ARect.Top - 2)/RectHeight(AXLineItem.ClipRect) / 2;
           XPolyline(ACanvas.Handle, LinePoints, 2, AXLineItem.StyleDef^.XLineDefs, AXLineItem.StyleDef^.Count, clBlack, clBlack, True, AZoom);
           End;
        End;
     End; // Case end
End;

Constructor TTMLib.Create(AnApplication: TApplication; sTMLibFileName, sLanguageFileExtension, sMainWindowTitle: AnsiString);
Var
   iLength: Integer;
   pcLangFileExt: PChar;
Begin
      bTheLibraryWasInitialized := FALSE;
      // Load TM dll.
      //hTMLibrary := LoadLibrary(PChar(sTMLibFileName));
      If hTMLibrary = 0 Then EXIT;
      // Detect all function from TM dll.
      //********     Initialization and finalization routnes of the DLL.  **********//
      InitLibraryFunc := GetProcAddress(hTMLibrary, 'InitLibrary');
      If @InitLibraryFunc = NIL Then EXIT;

      FinLibraryProc := GetProcAddress(hTMLibrary, 'FinLibrary');
      If @FinLibraryProc = NIL Then EXIT;

      GetVersionFunc := GetProcAddress(hTMLibrary, 'GetVersion');
      If @GetVersionFunc = NIL Then EXIT;

      StartTMFunc := GetProcAddress(hTMLibrary, 'StartTM');
      If @StartTMFunc = NIL Then EXIT;

      SetGetFillStylesCountCallbackProc := GetProcAddress(hTMLibrary, 'SetGetFillStylesCountCallback');
      If @SetGetFillStylesCountCallbackProc = NIL Then EXIT;

      SetGetLineStylesCountCallbackProc := GetProcAddress(hTMLibrary, 'SetGetLineStylesCountCallback');
      If @SetGetLineStylesCountCallbackProc = NIL Then EXIT;

      SetDrawStyleCallbackProc := GetProcAddress(hTMLibrary, 'SetDrawStyleCallback');
      If @SetDrawStyleCallbackProc = NIL Then EXIT;

      SetSetObjectStyleCallbackProc := GetProcAddress(hTMLibrary, 'SetSetObjectStyleCallback');
      If @SetSetObjectStyleCallbackProc = NIL Then EXIT;

      // Init TM library.
      iLength := Length(sLanguageFileExtension) + 1;
      GetMem(pcLangFileExt, iLength);
      bTheLibraryWasInitialized := FALSE;
      TRY
         StrPCopy(pcLangFileExt, SLanguageFileExtension);
         If @InitLibraryFunc <> NIL Then
            bTheLibraryWasInitialized := InitLibraryFunc(AnApplication, WinGISMainForm, pcLangFileExt, PChar(sMainWindowTitle));
      FINALLY
         FreeMem(pcLangFileExt, iLength);
      END;

      If bTheLibraryWasInitialized Then Begin
         SetGetFillStylesCountCallbackProc(GetXFillStylesCount);
         SetGetLineStylesCountCallbackProc(GetXLineStylesCount);
         SetDrawStyleCallbackProc(DrawStyle);
         End;
End;

Destructor TTMLib.Free;
Begin
     // Get ready to unload TM dll.
     If @FinLibraryProc <> NIL Then
        FinLibraryProc;
     If hTMLibrary <> 0 Then
        FreeLibrary(hTMLibrary);
End;

Function TTMLib.GetLibraryVersion: AnsiString;
Begin
     If @GetVersionFunc <> NIL Then
        RESULT := GetVersionFunc
     Else
        RESULT := 'Unknown'; 
End;

Function TTMLib.LibraryWasLoaded: Boolean;
Begin
     RESULT := SELF.bTheLibraryWasInitialized;
End;

Procedure TTMLib.SetSetObjectStyleCallback(AFunction: TSetObjectStyle);
Begin
     If @SetSetObjectStyleCallbackProc <> NIL Then
        SetSetObjectStyleCallbackProc(AFunction);
End;

Function TTMLib.StartTM(AProject: Pointer; ALayer: Pointer; iDatabaseType: Integer; sDatabaseFileName, sUserName, sUserPassword, sAttributeTableName, sProgisIDFieldName, sDefaultDataSourceField, sListOfFieldsThatAreNotToBeVisible: AnsiString; iSchemeSourceType: Integer; sSchemeDatabaseFileName, sSchemeUserName, sSchemePassword, sSchemeStorageTableName, sSchemeStorageFieldName: AnsiString; AGauge: TGauge): Boolean;
Begin
     If @StartTMFunc <> NIL Then
        RESULT := StartTMFunc(AProject,
                              ALayer,
                              iDatabaseType,
                              PChar(sDatabaseFileName),
                              PChar(sUserName),
                              PChar(sUserPassword),
                              PChar(sAttributeTableName),
                              PChar(sProgisIDFieldName),
                              PChar(sDefaultDataSourceField),
                              PChar(sListOfFieldsThatAreNotToBeVisible),
                              iSchemeSourceType,
                              PChar(sSchemeDatabaseFileName),
                              PChar(sSchemeUserName),
                              PChar(sSchemePassword),
                              PChar(sSchemeStorageTableName),
                              PChar(sSchemeStorageFieldName),
                              AGauge)
     Else
        RESULT := FALSE;
End;

end.
