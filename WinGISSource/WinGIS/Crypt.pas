unit Crypt;

interface

function CryptString(s: AnsiString): AnsiString;
function DecryptString(s: AnsiString): AnsiString;

implementation

uses Windows,SysUtils,Dialogs;

const CryptKey = '\r2vS/"�J8n|6H?P$T^�aO,zL>Z�5qBR�{40<@�cm3 j1VuX%&)M*�IsgN�k!�xA79DK�ifFQ_#.-WlepwtCEbY(y}h;:+U~do=G';

function CryptString(s: AnsiString): AnsiString;
var
i,n,p: Integer;
c: Char;
begin
     Result:='';
     i:=1;
     n:=Strlen(PChar(s));
     while i <= n do begin
         c:=s[i];
         p:=Pos(c,CryptKey);
         if p > 0 then begin
            Result:=Result + IntToHex(p,2);
         end
         else begin
            Result:=Result + 'FF' + c;
         end;
         Inc(i);
     end;
end;

function DecryptString(s: AnsiString): AnsiString;
var
i,n,p: Integer;
v: AnsiString;
c: Char;
begin
     Result:='';
     i:=1;
     n:=Strlen(PChar(s));
     while i <= n do begin
         v:=s[i]+s[i+1];
         if v = 'FF' then begin
              c:=s[i+2];
              Inc(i);
         end
         else begin
              p:=StrToInt('$' + v);
              c:=CryptKey[p];
         end;
         Result:=Result + c;
         Inc(i,2);
     end;
end;

end.
