{*************************************************************************}
{ AM_TRDDE - TurboRaster DDE - Unterst�tzung                              }
{ Unit f�r die DDE Unterst�tzte Turbo Raster Ausf�hrung                   }
{ Autor : Michael Bramberger                                              }
{ Datum : 08.08.1995                                                      }
{*************************************************************************}
{$D+,L+}
Unit AM_trDDE;

Interface

Uses Objects,WinTypes,WinProcs,AM_Def,
     AM_View,Am_Point,AM_Proj,AM_Sym,AM_Layer,AM_Index,AM_Poly,
     AM_Ini, WinDOS,Controls,Messages,DDEDef;

Type PLPoint = ^LPoint;
     LPoint = record
       x,
       y        : LongInt;
     End;

     LPA = Array[0..3] of lpoint;
     PMyRefDDEData = ^TMyRefDDEData;
     TMyRefDDEData = record
       Flags    : WORD;
       cfFormat : Integer;
       Data     : Array[0..1] of LPOINT;
     End;

     PProofData = ^TProofData;
     TProofData = record
       Flags    : WORD;
       cfFormat : WORD;
       Data     : Array[0..30] of Char;      {[0..0]}
     End;

Type PTurboRasterDDE = ^TTurboRasterDDE;
     TTurboRasterDDE = object(TOldObject)

       Parent                         : TWinControl;
       hWindow                        : hWnd;
       hClient                        : hWnd;
       WaitingAcknowledge             : WORD;
       Terminating                    : Boolean;
       IsConnected                    : Boolean;
       CommVersion                    : Integer;

       Constructor Init(AParent : TWinControl);
       Destructor Done;                            virtual;

       Procedure DDEInitiate(var Msg : TMessage);
       Procedure DDEAcknowledge(var Msg : TMessage);
       Procedure DDETerminate(var Msg : TMessage);
       Procedure DDERequest(var Msg : TMessage);
       Procedure Acknowledge(Flags : LongInt);
       Procedure SendRefPoints(Points : PLPointArray);
     End;

       Procedure GetSerial(var Serial : PChar);
       Procedure GetModuleCode(var Code : PChar);
       Procedure GetLicenceName(var Licence : PChar);
       
Function DeCodeText(szText : PChar) : Integer;

procedure GenerateCode(
  LizNr       : String;
  Modul       : String;
  var ActCode : String);

Implementation

Uses SysUtils, WinOSInfo;

Constructor TTurboRasterDDE.Init(AParent : TWinControl);
Begin
  inherited Init;
  Parent := AParent;
  hWindow := Parent.Handle;
  hClient := 0;
  WaitingAcknowledge := 0;
  IsConnected := FALSE;
  Terminating := FALSE;
End;

Destructor TTurboRasterDDE.Done;
Begin
End;

Procedure TTurboRasterDDE.DDEInitiate(var Msg : TMessage);
var App          : Array[0..255] of Char;
    Topic        : Array[0..255] of Char;
Begin
  GlobalGetAtomName(Msg.lParamLo,App,SizeOf(App));
  GlobalGetAtomName(Msg.lParamHi,Topic,SizeOf(Topic));
  hClient := Msg.wParam;
  IsConnected := TRUE;
  if StrIComp(Topic, 'TurboRaster100') = 0 then
    CommVersion := 1
  else
    if StrIComp(Topic, 'TurboRaster310') = 0 then
      CommVersion := 2;
  PostMessage(hClient, WM_DDE_ACK, hWindow, Msg.lParam);
End;


Procedure TTurboRasterDDE.DDEAcknowledge(var Msg : TMessage);
Begin
  if(not Terminating) then
    case WaitingAcknowledge of
          WM_DDE_DATA : WaitingAcknowledge := 0;
    End;
End;

Procedure TTurboRasterDDE.DDETerminate(var Msg : TMessage);
Begin
  if(Terminating = FALSE) then
  Begin
    Terminating := TRUE;
    PostMessage(hClient, WM_DDE_TERMINATE, WORD(hWindow), 0);
    IsConnected := FALSE;
    hClient := 0;
  End;
End;

Procedure TTurboRasterDDE.Acknowledge(Flags : LongInt);
Begin
  PostMessage(hClient, WM_DDE_ACK, hWindow, Flags);
End;

Procedure TTurboRasterDDE.SendRefPoints(Points : PLPointArray);
var  ParamPtr : PMyRefDDEData;
     hParam   : THANDLE;
     LParam   : LongInt;
     MaxPts, i,
     Add      : Integer;
Begin
  if CommVersion = 1 then
  Begin
    MaxPts := 2;
    LParam := LongInt(dd_RefPoints);
    Add := 1;
  End
  else
  Begin
    LParam := LongInt(dd_RefPointsV2);
    MaxPts := Points^[0].x + 1;       { +1, da auch in dd_RefPointsV2 als erste x-Koordinate die Anzahl der Punkte gesp. sind }
    Add := 0;
  End;

  hParam := GlobalAlloc(GMEM_DDESHARE, sizeof(TMyRefDDEDATA) + (MaxPts-2)*sizeof(LPOINT));
                                             { MaxPts -2, da in TMyRefData schon 2 "drin" sind. }

  ParamPtr := GlobalLock(hParam);
  ParamPtr^.Flags := $8000 or $2000;
  for i:=0 to MaxPts-Add do
  Begin
    ParamPtr^.Data[i].x := Points^[i+Add].x;
    ParamPtr^.Data[i].y := Points^[i+Add].y;
  End;
  GlobalUnlock(hParam);
  LParam := LParam shl 16;
  LParam := LParam or LongInt(hParam);
  PostMessage(hClient, WM_DDE_DATA, hWindow, LParam);
End;


Function DeCodeText(szText : PChar) : Integer;
var CheckSum2    : Word;
    CheckSum     : Word;
    Cnt          : Integer;
    Coded        : Array[0..130] of Char;
    Str          : Array[0..10] of Char;
    AStr         : String[10];
    Source       : PChar;

const MaxLen     = 60;

Begin
  if strlen(szText)<2*MaxLen then
  Begin
    DeCodeText := 0;
    Exit;
  End;
  Source := szText;
  StrCopy(Coded, '');
  for Cnt := 0 to MaxLen-1 do
  Begin
    AStr := StrPas(StrLCopy(Str, Source, 2));
    Coded[Cnt] := Char(HexToLong(AStr));
    Source := Source+2;
  End;
  Byte(Coded[0]) := Byte(Coded[0]) XOr $1C;
  for Cnt := 1 to MaxLen-1 do
    Byte(Coded[Cnt]) := Byte(Coded[Cnt]) XOr Byte(Coded[Cnt-1]);
  CheckSum := 0;
  for Cnt := 0 to MaxLen-5 do
    CheckSum := CheckSum+Byte(Coded[Cnt]);

  Source := @Coded[MaxLen-4];
  AStr := StrPas(StrLCopy(Str, Source, 4));
  CheckSum2 := HexToLong(AStr);
  if (CheckSum<>CheckSum2) then
  Begin
    DeCodeText := 0;
    Exit;
  End;
  Source := StrScan(Coded, #$FF);
  if (Source = NIL) then
  Begin
    DeCodeText := 0;
    Exit;
  End;
  Source^ := #0;
  StrCopy(szText, Coded);
  DeCodeText := 1;
End;


Procedure {TTurboRasterDDE.}GetSerial(var Serial : PChar);
var s            : String;
    s1           : String;
    ResHandle    : THandle;
    ResData      : PChar;
    Uncode       : Array[0..255] of Char;
    Temp         : Array[0..32] of Char;
begin
  ResHandle:=LoadResource(HInstance,FindResource(HInstance,PChar(9),PChar(RT_RCDATA)));
  if ResHandle<>0 then
  Begin
    ResData := LockResource(ResHandle);
    StrCopy(UnCode, ResData);
    DeCodeText(UnCode);
    { F�r Lizensversionen }
    UnCode[4] := '.';
    {UnCode[StrLen(UnCode)-12] := '.';  }
    Serial := StrNew(UnCode);
    FreeResource(ResHandle);
  end;
end;

{***********************************************************************************************}
{ Function Code                                                                                 }
{-----------------------------------------------------------------------------------------------}
{ Codiert AStr. Das erste Zeichen wird mit $3F XOr-verkn�pft. Jedes weitere Zeichen             }
{ mit dem uncodierten Zeichen davor.                                                            }
{-----------------------------------------------------------------------------------------------}
{ Parameter:                                                                                    }
{  AStr            i = zu codierender String                                                    }
{-----------------------------------------------------------------------------------------------}
{ Ergebnis: codierter String                                                                    }
{***********************************************************************************************}
Function Code
   (
   AStr            : String
   )
   : String;
  var Cnt          : Integer;
  begin
    AStr[1]:=Char(Byte(AStr[1]) XOr $3F);
    for Cnt:=2 to Length(AStr) do AStr[Cnt]:=Char(Byte(AStr[Cnt]) XOr Byte(AStr[Cnt-1]));
    Code:=AStr;
  end;

procedure GenerateCode(
  LizNr       : String;
  Modul       : String;
  var ActCode : String);
var
    Modul1   : String;
    Erg      : String;
    R1,R2    : String;
    i        : Integer;
    Feld,
    Feld1    : String;
    FeldHi,
    FeldLo,
    FeldB,
    BModul   : Byte;
    Test     : Real;
    Tests,
    StrCode  : String;
begin
  if Modul='Route Explorer' then Modul:='Route';
  if Modul='GPS Modul' then Modul:='GPSMo';
  if Modul='Turbo Raster' then Modul:='TrbRa';
  for i:=1 to 5 do
    begin
    FeldHi:=ord(LizNr[2*i])-ord('0')+i+1;
    FeldLo:=ord(LizNr[2*i-1])-ord('0')+6-i;
    FeldB:= (Byte(FeldHi shl 4  ) ) +
            (Byte(FeldLo and $0F) ) ;
    BModul:=ord(Modul[i]);
    Feld[6-i]:=chr(FeldB XOr BModul);
    end;
  SetLength(Feld,5);
  Feld:=Code(Feld);
  for i:= 1 to 5 do
    Feld1[6-i]:=Feld[i];
  SetLength(Feld1,5);
  Feld:=Code(Feld1);
  ActCode:='';
  for i:=1 to 5 do
    begin
    if Ord(Feld[i])<1 then ActCode:=ActCode+'0';
    if Ord(Feld[i])<10 then ActCode:=ActCode+'0';
    if Ord(Feld[i])<100 then ActCode:=ActCode+'0';
    Str(Ord(Feld[i]),StrCode);
    ActCode:=ActCode+StrCode;
    end;
  SetLength(Erg,10);
  Feld1:=UnCode(Feld);
  for i:=1 to 5 do
    Feld[6-i]:=Feld1[i];
  SetLength(Feld,5);
  Feld:=UnCode(Feld);
  for i:=1 to 5 do
    begin
    FeldHi:=ord(LizNr[2*i])-ord('0')+i+1;
    FeldLo:=ord(LizNr[2*i-1])-ord('0')+6-i;
    FeldB:= (Byte(FeldHi shl 4  ) ) +
            (Byte(FeldLo and $0F) ) ;
    Modul1[i]:=chr(ord(Feld[6-i]) XOr FeldB);
    end;
  SetLength(Modul1,5);
end;

Procedure {TTurboRasterDDE.}GetModuleCode(var Code : PChar);
var Buffer  : Array[0..512] of Char;
    Default : Array[0..32] of Char;
    IniFile,
    ProgName: Array[0..512] of Char;
    Path,
    AName,
    Ext     : Array[0..64] of Char;
    Point   : PChar;
    S       : String;
    T : String;
Const ValidCode1 : LongInt = 0220780;
      ValidCode2 : LongInt = 80111080;
Begin
  strcopy(Default, '1,123456789abcdef');
  GetModuleFileName(HInstance, ProgName, SizeOf(ProgName));
  FileSplit(ProgName, Path, AName, Ext);
  StrCopy(IniFile,PChar(OSInfo.WingisIniFileName));
  GetPrivateProfileString('MODULES', 'TurboRaster', Default, Buffer, 511, IniFile);
  Point := strscan(Buffer, ',');
  if Point<>NIL then begin
    Inc(Point);
    Code := strnew(Point);
  end
  else Code:=StrNew('0123456789');
End;

Procedure {TTurboRasterDDE.}GetLicenceName(var Licence : PChar);
var ResHandle, ResHandle1 : THandle;
    ResData               : PChar;
    UnCoded, UnCoded1     : Array[0..255] of Char;
Begin
    ResHandle := LoadResource(HInstance,FindResource(HInstance,PChar(10),PChar(RT_RCDATA)));
    ResHandle1 := LoadResource(HInstance,FindResource(HInstance,PChar(11),PChar(RT_RCDATA)));
    if(ResHandle<>0) and (ResHandle1<>0) then
    Begin
      ResData:=LockResource(ResHandle);
      StrCopy(UnCoded, ResData);
      ResData:=LockResource(ResHandle1);
      StrCopy(UnCoded1, ResData);
      DeCodeText(@UnCoded[0]);
      DeCodeText(@UnCoded1[0]);
      StrCat(UnCoded,', ');
      StrCat(UnCoded, UnCoded1);
    End;
    FreeResource(ResHandle);
    FreeResource(ResHandle1);
    Licence := strnew(UnCoded);
End;

Function UnCode(AStr : PChar) : PChar;
var Code         : Byte;
    NewChar      : Char;
    Cnt          : Integer;
Begin
  Code := $3F;
  for Cnt := 0 to strlen(AStr) do
  Begin
    NewChar := Char(Byte(AStr[Cnt]) XOr Code);
    Code := Byte(AStr[Cnt]);
    AStr[Cnt] := NewChar;
  end;
  UnCode := AStr;
End;


Procedure TTurboRasterDDE.DDERequest(var Msg : TMessage);
var Serial : PChar;
    hParam : THandle;
    pParam : Pointer;
    LParam : LongInt;
    Tmp    : Array[0..12] of Char;
    ProofData : PProofData;
Begin
  if(Msg.wParam <> hClient) then exit;

  case Msg.lParamHi of
    dd_SerialNumber : Begin
                        GetSerial(Serial);
                        LParam := dd_SerialNumber;
                      End;
    dd_ModuleCode   : Begin
                        GetModuleCode(Serial);
                        LParam := dd_ModuleCode;
                      End;
    dd_ModuleName   : Begin
                        LParam := dd_ModuleName;
                        Serial := strnew(StrPCopy(Tmp, im_TurboRaster));
                      End;
    dd_LicenceName  : Begin
                        GetLicenceName(Serial);
                        LParam := dd_LicenceName;
                      End;
  End;

  hParam := GlobalAlloc(GMEM_DDESHARE, strlen(Serial) + 1 + sizeof(TProofData));
  ProofData := PProofData(GlobalLock(hParam));
  strcopy(ProofData^.Data, Serial);

  ProofData^.cfFormat := 0;
  ProofData^.Flags := $8000 or $2000;
  GlobalUnlock(hParam);

  LParam := LParam shl 16;
  LParam := LParam or LongInt(hParam);
  PostMessage(hClient, wm_dde_data, hWindow, LParam);
End;

End.
