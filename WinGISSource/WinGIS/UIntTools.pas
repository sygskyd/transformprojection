{******************************************************************************+
  Module UIntTools
--------------------------------------------------------------------------------
  Contains some common functions used by the WinGIS user-interface. 
--------------------------------------------------------------------------------
  ReadHistory
  WriteHistory
--------------------------------------------------------------------------------
  Author: Martin Forst
+******************************************************************************}
Unit UIntTools;

Interface

Uses RegDB,Validate;

{******************************************************************************+
  Procedure ReadHistory
--------------------------------------------------------------------------------
  Reads a historry-list for a validator from the registry-database.
+******************************************************************************}
Procedure ReadHistory(Registry:TRegistryDatabase;const KeyName:String;Validator:TCustomValidator);

{******************************************************************************+
  Procedure WriteHistory
--------------------------------------------------------------------------------
  Writes a historry-list from a validator to the registry-database.
+******************************************************************************}
Procedure WriteHistory(Registry:TRegistryDatabase;const KeyName:String;Validator:TCustomValidator);

Implementation

Uses SysUtils;

Procedure ReadHistory(Registry:TRegistryDatabase;const KeyName:String;Validator:TCustomValidator);
var Cnt            : Integer;
    Count          : Integer;
begin
  Count:=Registry.ReadInteger(KeyName+'\Count');
  for Cnt:=0 to Count-1 do Validator.AddHistory(
      Registry.ReadString(Format('%s\Item%d',[KeyName,Cnt])));
end;

Procedure WriteHistory(Registry:TRegistryDatabase;const KeyName:String;Validator:TCustomValidator);
var Cnt            : Integer;
begin
  Registry.WriteInteger(KeyName+'\Count',Validator.HistoryCount);
  for Cnt:=0 to Validator.HistoryCount-1 do Registry.WriteString(Format('%s\Item%d',
      [KeyName,Cnt]),Validator.HistoryStrings[Cnt]);
end;

end.
 