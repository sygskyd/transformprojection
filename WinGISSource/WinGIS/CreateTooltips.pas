{******************************************************************************+
  Unit CreateTooltip
--------------------------------------------------------------------------------
  Author : Raimund Leitner
--------------------------------------------------------------------------------
  handles the complete floating text creation routines and dialogs.
  with property ModifyDlg can be decided, if a create or modify floating
  text dialog should be displayed.

27.03.2000
  Dennis Ivanoff, Progis Moscow, Russia
  xBase files as a source of tooltip's information were added. (xBase founded on dbf format).
  External source for tooltips after loading still Paradox type.
{******************************************************************************}

unit CreateTooltips;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, MultiLng,
  WCtrls, Combobtn, ExtCtrls, Am_Layer, DTI_LibMan;

const
  MlgSectionName = 'CreateTooltipDialog';

type
  TCreateTooltipsForm = class(TForm)
    OkBtn: TButton;
    CancelBtn: TButton;
    GroupBox1: TWGroupBox;
    ExtDestBtn: TRadioButton;
    ProjBtn: TRadioButton;
    GroupBox2: TWGroupBox;
    ExtSourceBtn: TRadioButton;
    ManEditBtn: TRadioButton;
    ExtSourceFileBtn: TWButton;
    ExtSourceEdit: TEdit;
    WLabel1: TWLabel;
    WLabel2: TWLabel;
    ExtDestEdit: TEdit;
    ExtDestFileBtn: TWButton;
    WLabel3: TWLabel;
    WLabel4: TWLabel;
    ExtSourceIDCombo: TComboBox;
    UseExtSourceBtn: TRadioButton;
    ExtSourceFTCombo: TComboBox;
    ExtDestIDCombo: TComboBox;
    ExtDestFTCombo: TComboBox;
    Bevel1: TBevel;
    DDEBtn: TRadioButton;
    MlgSection1: TMlgSection;
    AutoFillCombo: TComboBox;
    Panel1: TPanel;
    FillAutoBtn: TRadioButton;
    FillSelectedBtn: TRadioButton;
    ShowDefaultBtn: TRadioButton;
    IDBSourceRadioButton: TRadioButton;
    IDBDestinationRadioButton: TRadioButton;
    procedure SetProject(AProj: Pointer);
{++ IDB}
    procedure SetLayer(ALayer: PLayer);
{-- IDB}
    procedure SetModifyDlg(aMod: Boolean);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure OkBtnClick(Sender: TObject);
    // Source data
    procedure ManEditBtnClick(Sender: TObject);
    procedure DDEBtnClick(Sender: TObject);
    procedure ExtSourceBtnClick(Sender: TObject);
    procedure ExtSourceFileBtnClick(Sender: TObject);
    procedure ExtSourceEditExit(Sender: TObject);
    // Destination data
    procedure ProjBtnClick(Sender: TObject);
    procedure UseExtSourceBtnClick(Sender: TObject);
    procedure ExtDestBtnClick(Sender: TObject);
    procedure ExtDestFileBtnClick(Sender: TObject);
    procedure FillAutoBtnClick(Sender: TObject);
    procedure ShowDefaultBtnClick(Sender: TObject);
    procedure AutoFillComboChange(Sender: TObject);
    procedure FillSelectedBtnClick(Sender: TObject);
    procedure IDBSourceRadioButtonClick(Sender: TObject);
    procedure IDBDestinationRadioButtonClick(Sender: TObject);
  private
    { Private-Deklarationen}
    FProject: Pointer;
    FSourceDB: string;
    FDestDB: string;
{++ IDB}
    FLayer: PLayer; { This property is required for a case when an user wants to work
                      with the Geotext (TTooltip information) that is located in
                      the Internal database. Ivanoff.}
{-- IDB}
    FLayername: string;
    FModifyDlg: Boolean;
    FWasRunning: Boolean;

    ASourceMode: TWorkMode;
    ADestMode: TWorkMode;

    // fill IDCombo,FTCombo with according column names from aTable
    procedure FillCombos(aTable: string; IDCombo: TCombobox; FTCombo: TComboBox);
    // select appropriate entry of aCombo
    procedure SelectEntry(aCombo: TCombobox; aStr: string);

    function CopyData(ALayer: Pointer; sSourceTableName: AnsiString; sSrcIDCol: AnsiString; sSrcSFTCol: AnsiString;
      sDestTable: AnsiString; sDestIDCol: AnsiString; sDestFTCol: AnsiString): Boolean;

    // enable buttons depending on radio buttons
    procedure EnableDBButtons;
    // AutoFill
    procedure DoFillTooltipTexts(aLayer: Pointer; FillAll: Boolean; FillWith: Integer; DeleteOld: Boolean);
{++ IDB}
    procedure FillTComboWithFieldNames(ACombo: TComboBox);
{-- IDB}
  public
    { Public-Deklarationen}
    constructor Create(AOwner: TComponent);
    property Project: Pointer read FProject write SetProject;
{++ IDB}
    property Layer: PLayer read FLayer write SetLayer;
{-- IDB}
    property Layername: string read FLayername write FLayername;
    property ModifyDlg: Boolean read FModifyDlg write SetModifyDlg;
  end;

var
  CreateTooltipsForm: TCreateTooltipsForm;

implementation

{$R *.DFM}

uses AM_Def, AM_Proj, AM_Index, AM_View, Tooltips, AM_Obj, AM_Paint, AM_Point, AM_Poly, AM_CPoly,
  AM_Circl, AM_Text, AM_RText, AM_BuGra, AM_Splin, BmpImage, AM_Sym
{$IFNDEF WMLT}
{++ IDB}
  , Am_Main, MenuFn, IDB_Consts
{-- IDB}
{$ENDIF}
  ;

constructor TCreateTooltipsForm.Create
  (
  AOwner: TComponent
  );
begin
  inherited Create(AOwner);
  ModifyDlg := FALSE;

  ASourceMode := wmBDE_Or_Local;
  ADestMode := wmBDE_OR_Local;

    // stop tooltipthread while manipulating data
    {
    FWasRunning:=FALSE;
    if not TooltipThread.Suspended then begin
      TooltipThread.SureSuspend;
      FWasRunning:=TRUE;
    end;
    }
end;

procedure TCreateTooltipsForm.SetProject
  (
  AProj: Pointer
  );
begin
  FProject := AProj;
end;

{++ IDB}

procedure TCreateTooltipsForm.SetLayer(ALayer: PLayer);
begin
  SELF.FLayer := ALayer;
end;
{-- IDB}

procedure TCreateTooltipsForm.SetModifyDlg
  (
  aMod: Boolean
  );
begin
  FModifyDlg := aMod;
  if FModifyDlg then
    ManEditBtn.Caption := MlgStringList[MlgSectionName, 13]
  else
    ManEditBtn.Caption := MlgStringList[MlgSectionName, 3];
end;

procedure TCreateTooltipsForm.FormShow
  (
  Sender: TObject
  );
var
  aLayer: pLayer;
  aBool: Boolean;
{++ IDB}
  iP: Integer;
{-- IDB}
  sTemp: AnsiString;
begin
{++ IDB}
    // If not define work with the Internal Database, a button linked with it must be disabled.
{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}
  if not WinGISMainForm.WeAreUsingTheIDB[FPRoject] then
  begin
    IDBSourceRadioButton.Visible := FALSE;
    IDBDestinationRadioButton.Visible := FALSE;
  end;
{$ENDIF}
{$ENDIF}
{-- IDB}
    // if Dlg is ModifyDlg set to actual floating text settings
  if FModifyDlg then
  begin
    with PProj(FProject)^ do
      aLayer := PLayer(Layers.NameToLayer(FLayername));
    with aLayer.TooltipInfo do
    begin
      case AutoText of
        int_Manu: AutoFillCombo.ItemIndex := 0;
        int_ID: AutoFillCombo.ItemIndex := 1;
        int_Area: AutoFillCombo.ItemIndex := 2;
        int_Peri: AutoFillCombo.ItemIndex := 3;
        int_Coord: AutoFillCombo.ItemIndex := 4;
        int_SymNr: AutoFillCombo.ItemIndex := 5;
        int_SymNa: AutoFillCombo.ItemIndex := 6;
      end;
      case BDEOrLocal of
        int_Local:
          begin
            ManEditBtn.Checked := TRUE;
            ExtSourceBtn.Enabled := FALSE;
            AutoFillCombo.Enabled := TRUE;
            if FillAuto = int_Auto then
              FillAutoBtn.Checked := TRUE
            else
              if FillAuto = int_Sel then
                FillSelectedBtn.Checked := TRUE
              else
                ShowDefaultBtn.Checked := TRUE;
            aBool := AutoFillCombo.ItemIndex > 0;
            FillAutoBtn.Enabled := aBool;
            FillSelectedBtn.Enabled := aBool and (PProj(FProject).Layers.SelLayer.Data.GetCount > 0);
            ShowDefaultBtn.Enabled := aBool;
            if aBool and (FillAutoBtn.Checked or FillSelectedBtn.Checked) then
              OKBtn.Caption := MlgStringList[MlgSectionName, 19]
            else
              OKBtn.Caption := MlgStringList[MlgSectionName, 11];
          end;
        int_BDE:
          begin
            ExtSourceBtn.Checked := TRUE;
            UseExtSourceBtn.Checked := TRUE;
            ExtSourceEdit.Text := BDETable;
            FSourceDB := BDETable;
            FillCombos(BDETable, ExtSourceIDCombo, ExtSourceFTCombo);
            SelectEntry(ExtSourceIDCombo, IDColName);
            SelectEntry(ExtSourceFTCombo, FTColName);
            ManEditBtn.Enabled := FALSE;
          end;
        int_DDE:
          begin
            DDEBtn.Checked := TRUE;
            UseExtSourceBtn.Checked := TRUE;
            ProjBtn.Enabled := FALSE;
            ExtDestBtn.Enabled := FALSE;
            ManEditBtn.Enabled := FALSE;
            ExtSourceBtn.Enabled := FALSE;
          end;
{++ IDB}
        int_IDB:
          begin
            SELF.AutoFillCombo.Enabled := FALSE;
            SELF.FillAutoBtn.Enabled := FALSE;
            SELF.FillSelectedBtn.Enabled := FALSE;
            SELF.ShowDefaultBtn.Enabled := FALSE;
            SELF.DDEBtn.Enabled := FALSE;
            SELF.ExtSourceBtn.Enabled := FALSE;

            IDBSourceRadioButton.Checked := TRUE;
                   // One this call instead of all lines below. Ivanoff. 25.09.2000
            SELF.IDBSourceRadioButtonClick(SENDER);
          end;
{-- IDB}
        int_ADO:
          begin
            ASourceMode := wmADO;

            ExtSourceBtn.Checked := TRUE;
            UseExtSourceBtn.Checked := TRUE;

            sTemp := BDETable;
            iP := Pos(cs_ParameterSeparator, sTemp);
            sTemp := Copy(sTemp, iP + 1, Length(sTemp));
            iP := Pos(cs_ParameterSeparator, sTemp);

                   // 35='''%s'' in ''%s''
            ExtSourceEdit.Text := Format(MlgStringList[MlgSectionName, 35], [Copy(sTemp, iP + 1, Length(sTemp)), Copy(sTemp, 1, iP - 1)]);
            FSourceDB := BDETable;

            FillCombos(BDETable, ExtSourceIDCombo, ExtSourceFTCombo);

            SelectEntry(ExtSourceIDCombo, IDColName);
            SelectEntry(ExtSourceFTCombo, FTColName);
            ManEditBtn.Enabled := FALSE;
          end;
      end;
    end;
  end
  else
  begin
    AutoFillCombo.ItemIndex := 0;
    FillAutoBtn.Checked := TRUE;
    FillSelectedBtn.Enabled := PProj(FProject).Layers.SelLayer.Data.GetCount > 0;
  end;
  EnableDBButtons;
end;

procedure TCreateTooltipsForm.FormClose
  (
  Sender: TObject;
  var Action: TCloseAction
  );
begin
    // is done by TooltipDlg
    // resume thread only if has been running before!!!
    // if FWasRunning then TooltipThread.Resume;
end;

procedure TCreateTooltipsForm.OkBtnClick
  (
  Sender: TObject
  );
var
  aLayer: PLayer;
  Success: Boolean;
  SrcIDCol: string;
  SrcFTCol: string;
  DstIDCol: string;
  DstFTCol: string;
  AIndex: PIndex;
  AOldCursor: TCursor;

  function GetColName
      (
      Combo: TCombobox
      )
      : string;
  begin
    RESULT := Combo.Text;
  end;

  function SearchTooltip
      (
      Item: PIndex
      )
      : Boolean; far;
  var
    aView: PView;
  begin
    RESULT := FALSE;
    aView := PObjects(PProj(FProject).PInfo.Objects).IndexObject(PProj(FProject).PInfo, Item);
    if aView <> nil then
      RESULT := aView.Tooltip <> nil;
  end;

begin
{$IFNDEF WMLT}
  AOldCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
{++ IDB}
  MenuFunctions['EditTooltip'].Enabled := TRUE;
{-- IDB}
  try
    ModalResult := mrNone;
    Success := FALSE;
    with PProj(FProject)^ do
      aLayer := PLayer(Layers^.NameToLayer(FLayername));

    if DDEBtn.Checked then
    begin
      aLayer^.TooltipInfo.SetDDE;
      Success := TRUE;
    end
{++ IDB}
    else
      if IDBSourceRadioButton.Checked then
      begin
{$IFNDEF AXDLL} // <----------------- AXDLL
        if WinGisMainForm.WeAreUsingTheIDB[FPRoject] then
          WinGisMainForm.IDB_Man.SetGeoTextFieldNames(SELF.FProject, SELF.FLayer, ExtSourceIDCombo.Items[ExtSourceIDCombo.ItemIndex], ExtSourceFTCombo.Items[ExtSourceFTCombo.ItemIndex]);
{$ENDIF}
        ALayer.ToolTipInfo.SetIDB(cs_AttributeTableNamePrefix + IntToStr(SELF.FLayer.Index), ExtSourceIDCombo.Items[ExtSourceIDCombo.ItemIndex], ExtSourceFTCombo.Items[ExtSourceFTCombo.ItemIndex]);
              // Tnis line is required to avoid a problem when the user can modify ProgisID field in the IDB attribute table.
        if ALayer.TooltipInfo.FTColName = ALayer.TooltipInfo.IDColName then
          MenuFunctions['EditTooltip'].Enabled := FALSE;
        Success := TRUE;
      end
      else
        if (IDBDestinationRadioButton.Checked)
          and
          (ManEditBtn.Checked)
          and
          (AutoFillCombo.ItemIndex < 1) then
        begin
{$IFNDEF AXDLL} // <----------------- AXDLL
          if WinGisMainForm.WeAreUsingTheIDB[FPRoject] then
            WinGisMainForm.IDB_Man.SetGeoTextFieldNames(SELF.FProject, SELF.FLayer, ExtDestIDCombo.Items[ExtDestIDCombo.ItemIndex], ExtDestFTCombo.Items[ExtDestFTCombo.ItemIndex]);
{$ENDIF}
          ALayer.ToolTipInfo.SetIDB(cs_AttributeTableNamePrefix + IntToStr(SELF.FLayer.Index), ExtDestIDCombo.Items[ExtDestIDCombo.ItemIndex], ExtDestFTCombo.Items[ExtDestFTCombo.ItemIndex]);
          Success := TRUE;
        end
{-- IDB}
        else
        begin
          if aLayer <> nil then
          begin
            SrcIDCol := GetColName(ExtSourceIDCombo);
            SrcFTCol := GetColName(ExtSourceFTCombo);
            DstIDCol := GetColName(ExtDestIDCombo);
            DstFTCol := GetColName(ExtDestFTCombo);
                    // texts for floating text will be created manualy or automatically
            if ManEditBtn.Checked then
            begin
              if AutoFillCombo.ItemIndex > 0 then
              begin
                if FillAutoBtn.Checked then
                begin
                  AIndex := aLayer^.Data^.FirstThat(@SearchTooltip);
                  if AIndex <> nil then
                    if MessageDialog(MlgStringList[MlgSectionName, 30], mtConfirmation, [mbYes, mbNo], 0) <> mrYes then
                      Exit;
                  DoFillTooltipTexts(aLayer, TRUE, AutoFillCombo.ItemIndex, TRUE);
                  aLayer^.TooltipInfo.SetFillAuto;
                end
                else
                  if FillSelectedBtn.Checked then
                  begin
                    AIndex := PProj(FProject)^.Layers^.SelLayer^.Data^.FirstThat(@SearchTooltip);
                    if AIndex <> nil then
                      if MessageDialog(MlgStringList[MlgSectionName, 30], mtConfirmation, [mbYes, mbNo], 0) <> mrYes then
                        Exit;
                    DoFillTooltipTexts(aLayer, FALSE, AutoFillCombo.ItemIndex, TRUE);
                    aLayer^.TooltipInfo.SetFillAutoSelected;
                  end
                  else
                    if ShowDefaultBtn.Checked then
                    begin
                      aLayer^.TooltipInfo.SetShowDefault;
                    end;
              end
{++ Ivanoff 25.07.2000}
{ You can't create Geotext when you have selected "Manual input" in dialog. Cause of this error is that
  the procedure doesn't work with AutoFillCombo with its ItemIndex = 0. In this case we need to set a source
  of the Geotext as LOCAL (data are saved in the project).}
              else
                if AutoFillCombo.ItemIndex = 0 then
                  ALayer.TooltipInfo.SetLocal
{-- Ivanoff}
                else
                  aLayer^.TooltipInfo.SetFillNone;
              aLayer^.TooltipInfo.SetFillNone;
              aLayer^.TooltipInfo.AutoText := AutoFillCombo.ItemIndex;
                       //////
              aLayer^.TooltipInfo.BDEOrLocal := int_Local;

              Success := TRUE;
            end
{++ IDB}
            else
            begin
{-- IDB}
                       // texts for floating text come from extern db
              if ExtSourceBtn.Checked then
              begin
                if (SrcIDCol = '') or (SrcFTCol = '') then
                begin
                  MessageDialog(MlgStringList[MlgSectionName, 15], mtError, [mbOk], 0);
                  Exit;
                end;
              end;
                       // if manual edit no source must be prepared...

                       // prepare destination for floating text data
              if ExtDestBtn.Checked then
              begin
                if (DstIDCol = '') or (DstFTCol = '') then
                begin
                  MessageDialog(MlgStringList[MlgSectionName, 15], mtError, [mbOk], 0);
                  Exit;
                end;
                if FileExists(FDestDB) then
                begin
                  if MessageDialog(MlgStringList[MlgSectionName, 16], mtConfirmation, [mbOK, mbCancel], 0) <> mrOK then
                  begin
                    Exit;
                  end;
                end;
                if ADestMode = wmADO then
                begin
                end
                else
                begin
                  if not WinGISMainForm.DTI_Man.CreateTable(FDestDB, DstIDCol, DstFTCol) then
                  begin
                    MessageDialog(MlgStringList[MlgSectionName, 17], mtError, [mbOK], 0);
                    Exit; // any error => exit
                  end;
                end;
                aLayer^.TooltipInfo.SetBDE(FDestDB, DstIDCol, DstFTCol);
              end;
              if ProjBtn.Checked then
              begin
                aLayer^.TooltipInfo.SetLocal;
              end;

              if UseExtSourceBtn.Checked then
              begin // "recycle source"
                if ASourceMode = wmADO then
                begin
                  if not WinGISMainForm.DTI_Man.ADO_TableExists(FSourceDB) then
                    if not WinGISMainForm.DTI_Man.ADO_CreateTable(FSourceDB, SrcIDCol, SrcFTCol) then
                    begin
                      MessageDialog(MlgStringList[MlgSectionName, 17], mtError, [mbOK], 0);
                      EXIT;
                    end;
                  aLayer.TooltipInfo.SetADO(FSourceDB, SrcIDCol, SrcFTCol);
                end
                else
                  aLayer^.TooltipInfo.SetBDE(FSourceDB, SrcIDCol, SrcFTCol);
                Success := TRUE;
              end
              else
                if not IDBDestinationRadioButton.Checked then
{ Copy (if it is necessary) text data to project or extern db
  It is a reloading data from one external database file to an another one.
  This way was exist before me. (08.06.2000. Ivanoff.}
                  Success := CopyData(aLayer, FSourceDB, SrcIDCol, SrcFTCol, FDestDB, DstIDCol, DstFTCol)
                else
                begin
{++ IDB}
{$IFNDEF AXDLL} // <----------------- AXDLL
                  if WinGisMainForm.WeAreUsingTheIDB[FPRoject] then
                  begin
                    if Trim(ExtSourceEdit.Text) = '' then
                    begin
                      MessageBox(Application.Handle, PChar(Format(MlgStringList['IDB_Mans', 5], [ExtSourceEdit.Text])), 'WinGIS', MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
                      EXIT;
                    end;
                                // Here a loading from external DB to the Intenal Database was realized by Dennis Ivanoff.
                    Success := WinGISMainForm.IDB_Man.LoadGeoTextData(SELF.FProject,
                      SELF.FLayer,
                      ExtSourceEdit.Text,
                      ExtSourceFTCombo.Items[ExtSourceFTCombo.ItemIndex],
                      ExtDestFTCombo.Items[ExtDestFTCombo.ItemIndex],
                      ExtSourceIDCombo.Items[ExtSourceIDCombo.ItemIndex]);
                    if Success then
                    begin
                      WinGisMainForm.IDB_Man.SetGeoTextFieldNames(SELF.FProject, SELF.FLayer, ExtDestIDCombo.Items[ExtDestIDCombo.ItemIndex], ExtDestFTCombo.Items[ExtDestFTCombo.ItemIndex]);
                      ALayer.ToolTipInfo.SetIDB(cs_AttributeTableNamePrefix + IntToStr(SELF.FLayer.Index), ExtDestIDCombo.Items[ExtDestIDCombo.ItemIndex], ExtDestFTCombo.Items[ExtDestFTCombo.ItemIndex]);
                    end;
                  end;
{$ENDIF}
{-- IDB}
                end;
{++ IDB}
            end;
{-- IDB}
          end;
        end; // if DDEBtn.Checked

    if Success then
    begin
      // project has been modified (tooltipinfo is saved)
      PProj(FProject)^.SetModified;
      // continue tooltipthread
      Tooltipthread.StartFloatingText(FProject);
      FWasRunning := TRUE;
      ModalResult := mrOk;
    end;
  finally
    Screen.Cursor := AOldCursor;
  end
{$ENDIF}
end;

function TCreateTooltipsForm.CopyData(ALayer: Pointer; sSourceTableName: AnsiString; sSrcIDCol: AnsiString; sSrcSFTCol: AnsiString;
  sDestTable: AnsiString; sDestIDCol: AnsiString; sDestFTCol: AnsiString): Boolean;
var
  aItem: PIndex;
  i: Integer;
  NewText: string;

  procedure StoreInProject
      (
      aItem: PIndex;
      FloatingText: string
      );
  begin
      // �ber indexobject das richtige holen...
    with PProj(FProject)^ do
      aItem := PObjects(PInfo.Objects).IndexObject(PInfo, aItem);
    if aItem <> nil then
    begin
      DisposeStr(aItem.Tooltip);
      aItem.Tooltip := NewStr(FloatingText);
    end;
  end;

begin
{$IFNDEF WMLT}
  Result := FALSE;

    // copy data only when different Tables...

  with PLayer(aLayer)^ do
  begin
      // get tooltipinfo for each layer object
    for i := 0 to Data.GetCount - 1 do
    begin
      aItem := PIndex(Data.At(i));
      NewText := '';

      if sSourceTableName = '' then
      begin
        aItem := IndexObject(PProj(FProject).PInfo, aItem);
        if aItem <> nil then
          NewText := aItem.ToolTip^;
      end
      else
        NewText := WinGISMainForm.DTI_Man.GetGeoText(sSourceTableName, aItem.Index, sSrcIDCol, sSrcSFTCol);
        // get source record

      if NewText <> '' then
      begin
          // local or extern storage ?
        if TooltipInfo.BDEOrLocal = int_Local then
          StoreInProject(aItem, NewText)
        else // Store in DB
          WinGISMainForm.DTI_Man.SetGeoText(sDestTable, aItem.Index, sDestIDCol, sDestFTCol, NewText);
      end;
    end;
  end;
  Result := TRUE;
{$ENDIF}
end;

procedure TCreateTooltipsForm.EnableDBButtons;
var
  aBool: Boolean;
begin
  // enable buttons if external source or dest db checked
  aBool := ExtSourceBtn.Checked;
  //  ExtSourceEdit.Enabled:=aBool;
  ExtSourceFileBtn.Enabled := aBool;
  ExtSourceIDCombo.Enabled := aBool;
  ExtSourceFTCombo.Enabled := aBool;
  WLabel1.Enabled := aBool;
  WLabel2.Enabled := aBool;
  UseExtSourceBtn.Enabled := aBool;

  aBool := ExtDestBtn.Checked;
  //  ExtDestEdit.Enabled:=aBool;
  ExtDestFileBtn.Enabled := aBool;
  ExtDestIDCombo.Enabled := aBool;
  ExtDestFTCombo.Enabled := aBool;
  WLabel3.Enabled := aBool;
  WLabel4.Enabled := aBool;

  aBool := DDEBtn.Checked;
  ProjBtn.Enabled := not aBool;
  ExtDestBtn.Enabled := not aBool;
  if aBool then
    UseExtSourceBtn.Enabled := aBool;

  aBool := ManEditBtn.Checked;
  AutoFillCombo.Enabled := aBool;
  ProjBtn.Enabled := aBool;
  IDBDestinationRadioButton.Enabled := aBool or ExtSourceBtn.Checked;
  aBool := aBool and (AutoFillCombo.ItemIndex > 0);
  FillAutoBtn.Enabled := aBool;
  FillSelectedBtn.Enabled := aBool and (PProj(FProject).Layers.SelLayer.Data.GetCount > 0);
  ShowDefaultBtn.Enabled := aBool;
  if aBool and (FillAutoBtn.Checked or FillSelectedBtn.Checked) then
    OKBtn.Caption := MlgStringList[MlgSectionName, 19]
  else
    OKBtn.Caption := MlgStringList[MlgSectionName, 11];

{++ IDB}
  // enable buttons if external source or dest db checked
  if IDBSourceRadioButton.Checked then
  begin
       // An upper part of forms.
    ExtSourceFileBtn.Enabled := FALSE;
    ExtSourceEdit.Enabled := FALSE;
    ExtSourceIDCombo.Enabled := TRUE;
    ExtSourceFTCombo.Enabled := TRUE;
    WLabel1.Enabled := TRUE;
    WLabel2.Enabled := TRUE;
       // A lower part of form.
    UseExtSourceBtn.Enabled := FALSE;
    ProjBtn.Enabled := FALSE;
    UseExtSourceBtn.Enabled := TRUE;
    UseExtSourceBtn.Checked := TRUE;
    ExtDestBtn.Enabled := FALSE;
    ExtDestEdit.Enabled := FALSE;
    ExtDestFileBtn.Enabled := FALSE;
    ExtDestIDCombo.Enabled := FALSE;
    ExtDestFTCombo.Enabled := FALSE;
    IDBDestinationRadioButton.Enabled := FALSE;
  end;
  if IDBDestinationRadioButton.Checked then
  begin
    ExtDestEdit.Enabled := FALSE;
    ExtDestIDCombo.Enabled := TRUE;
    ExtDestFTCombo.Enabled := TRUE;
    WLabel3.Enabled := TRUE;
    WLabel4.Enabled := TRUE;
  end;
{-- IDB}
end;

procedure TCreateTooltipsForm.ManEditBtnClick
  (
  Sender: TObject
  );
begin
  EnableDBButtons;
end;

procedure TCreateTooltipsForm.ExtSourceBtnClick
  (
  Sender: TObject
  );
begin
  EnableDBButtons;
  ExtSourceEdit.Enabled := TRUE;
  ExtSourceEdit.Text := '';
  UseExtSourceBtn.Checked := TRUE;
{++ IDB}
  ExtSourceIDCombo.Items.Clear;
  ExtSourceFTCombo.Items.Clear;
{-- IDB}
end;

procedure TCreateTooltipsForm.ProjBtnClick
  (
  Sender: TObject
  );
begin
  EnableDBButtons;
end;

procedure TCreateTooltipsForm.ExtDestBtnClick
  (
  Sender: TObject
  );
begin
  EnableDBButtons;
{++ IDB}
  ExtDestEdit.Text := '';
  ExtDestIDCombo.Items.Clear;
  ExtDestFTCombo.Items.Clear;
  ExtDestIDCombo.Style := csDropDown;
  ExtDestFTCombo.Style := csDropDown;
{-- IDB}
end;

procedure TCreateTooltipsForm.UseExtSourceBtnClick
  (
  Sender: TObject
  );
begin
  EnableDBButtons;
end;

procedure TCreateTooltipsForm.ExtSourceFileBtnClick
  (
  Sender: TObject
  );
var
  aDlg: TOpenDialog;
  sTemp,
    sTableName,
    sProgisIDFieldName,
    sGeoTextFieldName: AnsiString;
  iP: Integer;
begin
{$IFNDEF WMLT}
  aDlg := TOpenDialog.Create(self);
  try
      // Paradox files
    aDlg.DefaultExt := '*.db';
{++ Ivanoff 27.03.2000}
{xBase files were added as a source of tooltip information.}
{21.09.2001 - the same about MS Access format file support.}
    aDlg.Filter := MlgStringList[MlgSectionName, 18] + ' (*.db)|*.db' + // 18='Paradox-Files'
      '|' +
      MlgStringList[MlgSectionName, 31] + ' (*.dbf)|*.dbf' + // 31='xBase-Files'
      '|' +
      MlgStringList[MlgSectionName, 34] + ' (*.mdb)|*.mdb' // 34='MS Access files'
      ;
{-- Ivanoff}

    if ASourceMode = wmADO then
    begin
      aDlg.FilterIndex := 3;
         // Split parameter.
         // For example: 'MSACCESS|c:\required database file name.mdb|required table name'
      sTemp := FSourceDB;
      iP := Pos(cs_ParameterSeparator, sTemp);
      sTemp := Copy(sTemp, iP + 1, Length(sTemp));
      iP := Pos(cs_ParameterSeparator, sTemp);
      sTemp := Copy(sTemp, 1, iP - 1);
      aDlg.InitialDir := ExtractFilePath(sTemp);
      aDlg.FileName := ExtractFileName(sTemp);
    end
    else
      aDlg.FilterIndex := 1;

    aDlg.Options := [ofFileMustExist, ofHideReadOnly];
    if aDlg.Execute then
    begin

      case aDlg.FilterIndex of
        3:
          begin
            sTableName := cs_MSACCESS_TypeSign + cs_ParameterSeparator +
              aDlg.FileName + cs_ParameterSeparator +
              '';
            sProgisIDFieldName := '';
            sGeoTextFieldName := '';
            sTableName := Trim(WinGISMainForm.DTI_Man.ADO_ChooseTableName(sTableName));
            if sTableName <> '' then
            begin
              iP := Pos(']', sTableName); // Use of ']' is forbidden.
              if iP > 0 then
              begin
                sTemp := Copy(sTableName, 1, iP - 1);
                sTableName := Copy(sTableName, iP + 1, Length(sTableName));
                iP := Pos(']', sTableName); // Use of ']' is forbidden.
                sProgisIDFieldName := Copy(sTableName, 1, iP - 1);
                sGeoTextFieldName := Copy(sTableName, iP + 1, Length(sTableName));
                sTableName := sTemp;
              end;

              SELF.ASourceMode := wmADO;
               // For example: 'MSACCESS|c:\required database file name.mdb|required table name'
              FSourceDB := cs_MSACCESS_TypeSign + cs_ParameterSeparator +
                aDlg.FileName + cs_ParameterSeparator +
                sTableName;
               // 35='''%s'' in ''%s''
              ExtSourceEdit.Text := Format(MlgStringList[MlgSectionName, 35], [sTableName, aDlg.FileName]);
              if sProgisIDFieldName <> '' then
              begin
                ExtSourceIDCombo.Items.Text := sProgisIDFieldName;
                ExtSourceIDCombo.ItemIndex := 0;
                ExtSourceFTCombo.Items.Text := sGeoTextFieldName;
                ExtSourceFTCombo.ItemIndex := 0;
              end
              else
                FillCombos(FSourceDB, ExtSourceIDCombo, ExtSourceFTCombo);
            end;
          end;
      else
        begin // Case else
          SELF.ASourceMode := wmBDE_Or_Local;

          FSourceDB := aDlg.FileName;
          ExtSourceEdit.Text := FSourceDB;
          FillCombos(FSourceDB, ExtSourceIDCombo, ExtSourceFTCombo);
        end;
      end; // Case end
    end;
  finally
    aDlg.Free;
  end;
{$ENDIF}
end;

procedure TCreateTooltipsForm.FillCombos
  (
  aTable: string;
  IDCombo: TCombobox;
  FTCombo: TCombobox
  );
type
  TrecFieldDesc = record
    sFieldName: AnsiString;
    iFieldType: Integer;
  end;
    // This declaration was gotten from db.pas.
  TFieldType = (ftUnknown, ftString, ftSmallint, ftInteger, ftWord,
    ftBoolean, ftFloat, ftCurrency, ftBCD, ftDate, ftTime, ftDateTime,
    ftBytes, ftVarBytes, ftAutoInc, ftBlob, ftMemo, ftGraphic, ftFmtMemo,
    ftParadoxOle, ftDBaseOle, ftTypedBinary, ftCursor, ftFixedChar, ftWideString,
    ftLargeint, ftADT, ftArray, ftReference, ftDataSet);
const
  IndexFieldDataTypes = [ftInteger, ftWord, ftLargeInt, ftFloat];
  FloatingTextFieldDataTypes = [ftString, ftWideString];
var
  AList: TList;
  pFD: ^TrecFieldDesc;
  iI: Integer;
begin
{$IFNDEF WMLT}
  IDCombo.Clear;
  FTCombo.Clear;

  if ASourceMode = wmADO then
    AList := WinGISMainForm.DTI_Man.ADO_GetFieldDescs(ATable)
  else
    AList := WinGISMainForm.DTI_Man.GetFieldDescs(aTable);

  for iI := 0 to AList.Count - 1 do
  begin
    pFD := AList[iI];
    if TFieldType(pFD.iFieldType) in IndexFieldDataTypes then
      IDCombo.Items.Add(pFD.sFieldName);
// ++ Cadmensky
//        If TFieldType(pFD.iFieldType) IN FloatingTextFieldDataTypes Then
// -- Cadmensky
    FTCombo.Items.Add(pFD.sFieldName);
    Dispose(pFD);
  end; // For iI end
  AList.Clear;
  AList.Free;

  if IDCombo.Items.Count > 0 then
    IDCombo.ItemIndex := 0;
  if FTCombo.Items.Count > 0 then
    FTCombo.ItemIndex := 0;
{$ENDIF}
end;

procedure TCreateTooltipsForm.ExtSourceEditExit
  (
  Sender: TObject
  );
begin
    {
    aDBName:=ExtSourceEdit.Text;
    if not FileExists(aDBName) then begin
      MessageDialog(Format('Datenbank %s nicht gefunden',[aDBName]),mtError,[mbOk],0);
      ExtSourceEdit.Text:=FSourceDB;
      Exit;
    end;
    FSourceDB:=aDBName;
    FillCombos(FSourceDB,ExtSourceIDCombo,ExtSourceFTCombo);
    }
end;

procedure TCreateTooltipsForm.ExtDestFileBtnClick
  (
  Sender: TObject
  );
var
  aDlg: TOpenDialog;
begin
  aDlg := TOpenDialog.Create(Self);
  try
      // Paradox files
    aDlg.DefaultExt := '*.db';
    aDlg.Filter := MlgStringList[MlgSectionName, 18] + ' (*.db)|*.db';
    aDlg.FilterIndex := 1;
    aDlg.Options := [ofCreatePrompt, ofHideReadOnly];
    if aDlg.Execute then
    begin
      FDestDB := aDlg.FileName;
      ExtDestEdit.Text := FDestDB;
      FillCombos(FDestDB, ExtDestIDCombo, ExtDestFTCombo);
      if FileExists(FDestDB) then
      begin
        ExtDestIDCombo.Style := csDropDownList;
        ExtDestIDCombo.Style := csDropDownList;
      end
      else
      begin
        ExtDestIDCombo.Style := csDropDown;
        ExtDestIDCombo.Style := csDropDown;
      end;
    end;
  finally
    aDlg.Free
  end;
end;

procedure TCreateTooltipsForm.SelectEntry
  (
  aCombo: TCombobox;
  aStr: string
  );
var
  i: Integer;
begin
  with aCombo do
  begin
    for i := 0 to Items.Count - 1 do
    begin
      if Items[i] = aStr then
      begin
        ItemIndex := i;
        Exit;
      end;
    end;
  end;
end;

procedure TCreateTooltipsForm.DDEBtnClick
  (
  Sender: TObject
  );
begin
  UseExtSourceBtn.Checked := TRUE;
  EnableDBButtons;
end;

procedure TCreateTooltipsForm.FillAutoBtnClick
  (
  Sender: TObject
  );
var
  aLayer: pLayer;
begin
  with PProj(FProject)^ do
    aLayer := PLayer(Layers.NameToLayer(FLayername));
  aLayer.TooltipInfo.SetFillAuto;
  EnableDBButtons;
end;

procedure TCreateTooltipsForm.FillSelectedBtnClick
  (
  Sender: TObject
  );
var
  aLayer: pLayer;
begin
  EnableDBButtons;
end;

procedure TCreateTooltipsForm.ShowDefaultBtnClick
  (
  Sender: TObject
  );
var
  aLayer: pLayer;
begin
  with PProj(FProject)^ do
    aLayer := PLayer(Layers.NameToLayer(FLayername));
  aLayer.TooltipInfo.SetShowDefault;
  EnableDBButtons;
end;

procedure TCreateTooltipsForm.AutoFillComboChange
  (
  Sender: TObject
  );
var
  aLayer: pLayer;
begin
  EnableDBButtons;
end;

procedure TCreateTooltipsForm.DoFillTooltipTexts
  (
  aLayer: Pointer;
  FillAll: Boolean;
  FillWith: Integer;
  DeleteOld: Boolean
  );
var
  bLayer: PLayer;

  procedure FillText
      (
      Item: PIndex
      ); far
  begin
{++ IDB}
{$IFNDEF WMLT}
    if IDBDestinationRadioButton.Checked then
      bLayer.TooltipInfo.SetIDB(cs_AttributeTableNamePrefix + IntToStr(bLayer.Index), ExtDestIDCombo.Text, ExtDestFTCombo.Text);
    PProj(FProject).DoFillTooltipText(bLayer, IDBDestinationRadioButton.Checked, Item, AutoFillCombo.ItemIndex, TRUE);
{$ENDIF}
{-- IDB}
  end;
begin
    // If I have to fill TToolTipInfo for objects on the same layer, the bLayer variable is
    // important but if I have to do it on different layers, this variable isn't important.
  bLayer := PLayer(aLayer);
  if FillAll then
    bLayer.Data.ForEach(@FillText)
  else
    PProj(FProject).Layers.SelLayer.Data.ForEach(@FillText);
end;

{++ IDB}

procedure TCreateTooltipsForm.FillTComboWithFieldNames(ACombo: TComboBox);
var
  iP: Integer;
  AOldCursor: TCursor;
begin
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
  if not WinGisMainForm.WeAreUsingTheIDB[FPRoject] then
    EXIT;
  AOldCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    ACombo.Items.Text := WinGisMainForm.IDB_Man.GetTableFieldsNamesList(FProject, FLayer);
    iP := ACombo.Items.IndexOf(WinGisMainForm.IDB_Man.GetGeoTextFieldName(SELF.FProject, SELF.FLayer));
    if iP < 0 then
      iP := 0;
    ACombo.ItemIndex := iP;
  finally
    Screen.Cursor := AOldCursor;
  end;
{$ENDIF}
{$ENDIF}
end;
{-- IDB}

procedure TCreateTooltipsForm.IDBSourceRadioButtonClick(Sender: TObject);
begin
  EnableDBButtons;
  ExtSourceEdit.Enabled := FALSE;
  ExtSourceEdit.Text := Format(SELF.MlgSection1[33], [SELF.FLayername]); // 'A table of layer ''' + SELF.FLayername + '''';
  ExtSourceIDCombo.Items.Clear;
  ExtSourceIDCombo.Items.Add('ProgisID');
  ExtSourceIDCombo.ItemIndex := 0;
{++ IDB}
  FillTComboWithFieldNames(ExtSourceFTCombo);
{-- IDB}
end;

procedure TCreateTooltipsForm.IDBDestinationRadioButtonClick(Sender: TObject);
var
  iI: Integer;
begin
  EnableDBButtons;
  ExtDestEdit.Enabled := FALSE;
  ExtDestEdit.Text := Format(SELF.MlgSection1[33], [SELF.FLayername]); // 'A table of layer ''' + SELF.FLayername + '''';
  ExtDestIDCombo.Style := csDropDownList;
  ExtDestFTCombo.Style := csDropDownList;
  Application.ProcessMessages;
  ExtDestIDCombo.Items.Clear;
  ExtDestIDCombo.Items.Add('ProgisID');
  ExtDestIDCombo.ItemIndex := 0;
{++ IDB}
  SELF.FillTComboWithFieldNames(ExtDestFTCombo);
  iI := 0;
  while iI < ExtDestFTCombo.Items.Count do
    if AnsiUpperCase(ExtDestFTCombo.Items[iI]) = 'PROGISID' then
      ExtDestFTCombo.Items.Delete(iI)
    else
      Inc(iI);
{-- IDB}
end;

end.

