unit AddOnsDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ShellAPI, WinInet;

type
  TAddOnsForm = class(TForm)
    GroupBoxList: TGroupBox;
    GroupBoxButtons: TGroupBox;
    ButtonClose: TButton;
    ListBox: TListBox;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ListBoxMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ListBoxMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    AddOnsList: TStringList;
    procedure GetAddOnsList;
  public
  end;

var
  AddOnsForm: TAddOnsForm;

procedure RunAddOnsDlg;

implementation

{$R *.DFM}

procedure RunAddOnsDlg;
begin
     AddOnsForm:=TAddOnsForm.Create(Application);
     try
        AddOnsForm.ShowModal;
     finally
        AddOnsForm.Free;
        AddOnsForm:=nil;
     end;
end;

{ TAddOnsForm }

procedure TAddOnsForm.FormShow(Sender: TObject);
begin
     AddOnsList:=TStringList.Create;
     GetAddOnsList;
end;

procedure TAddOnsForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     AddOnsList.Free;
end;

procedure TAddOnsForm.GetAddOnsList;
var
s: AnsiString;
hSession,hURL: HInternet;
Buffer: Array[0..1024*256] of Char;
BufferSize: DWord;
AConnected: Boolean;
i: Integer;
begin
        s:='';
        hSession:=nil;
        hURL:=nil;
        AConnected:=False;

        hSession:=InternetOpen(nil,INTERNET_OPEN_TYPE_PRECONFIG, nil, nil, INTERNET_SERVICE_URL);
        if hSession <> nil then begin
           hURL:=InternetOpenURL(hSession,'http://www.progis.com/wingis_addons.txt',nil,0,INTERNET_FLAG_RELOAD,0);
           if hURL <> nil then begin
              try
                 InternetReadFile(hURL,@Buffer,Sizeof(Buffer),BufferSize);
                 if BufferSize > 0 then begin
                    s:=Trim(Copy(Buffer,0,BufferSize));
                    if s <> '' then begin
                       AddOnsList.Text:=s;
                       for i:=0 to AddOnsList.Count-1 do begin
                           ListBox.Items.Add(Trim(AddOnsList.Names[i]));
                       end;
                    end;
                 end;
              finally
                 InternetCloseHandle(hURL);
              end;
           end;
        end;
        InternetCloseHandle(hSession);
end;

procedure TAddOnsForm.ListBoxMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
     if ListBox.ItemAtPos(Point(X,Y),True) > -1 then begin
        ListBox.Cursor:=crHandPoint;
     end
     else begin
        ListBox.Cursor:=crDefault;
     end;
end;

procedure TAddOnsForm.ListBoxMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
ALink: AnsiString;
AName: AnsiString;
begin
     if (ListBox.ItemAtPos(Point(X,Y),True) > -1) and (ListBox.ItemIndex > -1) then begin
        AName:=ListBox.Items[ListBox.ItemIndex];
        if AName <> '' then begin
           ALink:=Trim(AddOnsList.Values[AName]);
           if ALink <> '' then begin
              ShellExecute(0,'open',PChar(ALink),nil,nil,SW_SHOW);
           end;
        end;
     end;
end;

end.
