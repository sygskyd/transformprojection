unit ExtLib;

interface

uses Objects, SysUtils, Dialogs, WinTypes, Classes, Forms,
  AM_Group, AM_Obj, AM_Font, AM_Layer, AM_Paint, AM_Def, AM_Ole, AM_Ini,
  UserIntF;

const
  ExtLibProjVersion = 3; { Version f�r im Projekt gespeicherten Teil }
  ExtLibFileVersion = 2; { Version f�r externe Symbolbibliotheken }
  ExtLibFileHeader = 'WSL WinGIS Symbol Library';
  el_Loaded = 1; { ext. Bib. ist geladen }
  el_UnAvailable = 2; { ext. Bib. ist nicht verf�gbar (Datei nicht vorhanden) }
  el_Modified = 4; { ext. Bib. wurde ge�ndert }

type
  PExtLib = ^TExtLib;
  TExtLib = object(TOldObject)
    Symbols: PSymbols;
    Fonts: PFonts;
    LibName,
      LibFileName,
      LibRelName: PString;
    LibState: LongInt; { Statusbits siehe el_xxx }
    Owner: PLayer;
    UseCount: Integer;
    constructor Init(AOwner: PLayer; ALibName: string);
    constructor Load(S: TOldStream);
    function AlreadyLoaded: pExtLib;
    function CheckName(AName: string; BSym: PSGroup): Boolean;
    procedure CheckSymTexts(aFonts: pFonts; ASym: pSGroup);
    function InsertSymbol(PInfo: PPaint; ASym: PSGroup; Paint: Boolean): Boolean;
{++ IDB}
    procedure InsertSymbolAfterUndo(ASym: PSGroup);
{-- IDB}
    function DeleteSymbol(ASym: PSGroup): Boolean;
    function GetSymbol(SymName: PString): PSGroup;
    procedure LoadExtLib;
    function Loaded: Boolean;
    function UnAvailable: Boolean;
    function Modified: Boolean;
    procedure NewExtLib(AddSymbol: Boolean = TRUE);
    procedure SetModified(ASet: Boolean);
    procedure ReLoad;
    procedure Store(S: TOldStream); virtual;
    function SaveExtLib: Boolean;
    procedure SetUseCounts;
    procedure UnLoad;
    destructor Done; virtual;
    procedure RemoveLastInstance;
  end;

  TSymEditInfo = class
  public
    SymEditChild: Pointer; { Child-Fenster, das nach Symbleditor }
                                 { geschlossen werden mu� (TMDIChild)}
    SymParent: Pointer; { "Vaterprojekt" des Symboleditors (TMDIChild)}
    SymEditor: Pointer; { ev. aktiver Symboleditor (TSymEditor) }
    SymbolToEdit: PSGroup; { Symbol f�r Editor }
    SymExtLib: pExtLib; { editierte Ext Bib (pExtLib) }
    function CanCloseSymEditor: Boolean; { versucht Symboleditor zu beenden }
    function CloseSymEditor: Boolean; { Beendet Symboleditor eines Vaterprojektes }
    function EndSymEditor: Boolean; { Beendet einen Symboleditor und updatet das Vaterprojekt }
    procedure ResetEditChildPtr; { setzt SymEditChild auf NIL }
  end;

function ExtLibAlreadyOpened(const AName: string): pExtLib;

var
  LibNameLoading: AnsiString = '';

implementation

uses AM_Text, AM_Index, AM_Child, SymEdit, AM_Main, MultiLng;

{******************************************************************************}
{ Methoden Objekt TExtLib                                                      }
{ Verwaltung externer Symbolbibliotheken                                       }
{******************************************************************************}

constructor TExtLib.Init
  (
  AOwner: PLayer;
  ALibName: string
  );
begin
  LibFileName := NewStr(ALibName);
  LibRelName := NewStr(ExtractRelativePath(GetCurrentDir, ALibName));
  LibName := NewStr(ExtractFileName(ALibName));
  Owner := AOwner;
  LibState := 0;
  Symbols := nil;
  UseCount := 0;
end;

{******************************************************************************}
{ Construktor Load                                                             }
{ L�dt den Namen der ext. Bibl. aus AMP-File                                   }

constructor TExtLib.Load
  (
  S: TOldStream
  );
var
  Version: Byte;
begin
  S.Read(Version, sizeof(Version));
  LibName := S.ReadStr;
  LibName^ := Lowercase(LibName^);
  if (Version > 2) then
  begin
    LibFileName := S.ReadStr;
    LibFileName^ := Lowercase(LibFileName^);
    LibRelName := S.ReadStr;
    LibRelName^ := Lowercase(LibRelName^);
  end
  else
  begin
    LibFileName := NewStr(LibName^);
    LibRelName := NewStr(LibName^);
  end;
  LibState := 0;
  Symbols := nil;
  UseCount := 0;
end;

{******************************************************************************}
{ Methode Store                                                                }
{ speichert den Namen der ext. Bibliothek im AMP-File                          }

procedure TExtLib.Store
  (
  S: TOldStream
  );
var
  Version: Byte;
begin
  Version := ExtLibProjVersion;
  S.Write(Version, sizeof(Version));
  S.WriteStr(LibName);
  S.WriteStr(LibFileName);
  S.WriteStr(LibRelName);
end;

{******************************************************************************}
{ Methode NewExtLib                                                            }
{ erzeugt eine neue Symbolbibliothek                                           }

procedure TExtLib.NewExtLib(AddSymbol: Boolean);
var
  aSym: pSGroup;
  SymName: string;
begin
  UnLoad;
  Fonts := New(pFonts, Init);
  Symbols := New(pSymbols, Init(Owner));
  LibState := el_Loaded or el_Modified;
  if AddSymbol then
  begin
    SymName := Symbols^.GetNewSymName(MlgStringList['SymbolMsg', 3],
      LibName^); {Name f�r neues Symbol }
    ASym := New(PSGroup, InitName(SymName, PToStr(LibName)));
    Symbols^.InsertSymbol(nil, ASym, False); { Leeres Symbol einf�gen (PInfo wird nicht gebraucht)}
  end;
end;

{******************************************************************************}
{ Methode LoadExtLib                                                           }
{ L�dt externe Sym-Bib aus externer Datei                                      }

procedure TExtLib.LoadExtLib;
var
  AText: string;
  Version: Byte;
  S: TOleClientStream;
  TempFileName,
    BakFileName,
    FileName: string;
  i: Integer;

begin
  UnLoad;

  FileName := '';
  if (FileExists(LibFileName^)) then
    FileName := LibFileName^ + #0;
  if (FileExists(LibRelName^)) then
    FileName := LibRelName^ + #0;
  if WingisMainForm.ActualChild <> nil then
    TempFileName := ExtractFilePath(WingisMainForm.ActualChild.FName);
  if Copy(TempFileName, Length(TempFileName), 1) <> '\' then
  begin
    TempFileName := TempFileName + '\';
  end;

  if FileName = '' then
  begin
    TempFileName := TempFileName + ExtractFileName(LibFileName^);
    if (FileExists(TempFileName)) then
    begin
      FileName := TempFileName + #0;
      StrCopy(PChar(AnsiString(LibFileName^)), PChar(AnsiString(FileName)));
    end;
  end;

  if (FileName = '') then
  begin
    LibState := el_UnAvailable; { SymLib nicht verf�gbar }
    Exit;
  end;

  S := TOleClientStream.Create(FileName, stOpenRead);
//    S.Init(@FileName[1],stOpenRead,AM_Def.StrBufSize);
  AText := S.ReadString;
  if (AText <> ExtLibFileHeader) then
  begin
    MessageDialog(Format(MlgStringList['SymbolMsg', 1], [FileName]), mtInformation, [mbOk], 0);
    LibState := el_UnAvailable;
    S.Free;
    exit;
  end;
  S.Read(Version, sizeof(Version));

  LibNameLoading := Lowercase(ExtractFileName(Filename));
  Symbols := Pointer(S.Get);
  LibNameLoading := '';

  if (Version = 1) then
    Fonts := New(PFonts, Init)
  else
    Fonts := Pointer(S.Get);
  S.Free;
  SetUseCounts;
  WinGISMainForm.LoadedLibs.Add(@self);
  LibState := (LibState or el_Loaded) and (not el_Modified);
end;

{******************************************************************************}
{ Methode SaveExtLib                                                           }
{ speichert ev. �nderungen in der externen Symbolbibliothek                    }

function TExtLib.SaveExtLib: Boolean;
var
  Version: Byte;
  AText: string;
  S: TOleClientStream;
  TempFileName,
    BakFileName,
    FileName: string;
  ABuffer: array[0..255] of Char;
begin
  Result := False;
  if (LibState and el_UnAvailable) <> 0 then
    Exit; { nicht verf�gbare Bib nich speichern! }
  if (Loaded) and (Assigned(Symbols)) then
  begin
    LibState := LibState and (not el_Modified);
    SetUseCounts;
    if (not FileExists(LibFileName^)) and (FileExists(LibRelName^)) then
      FileName := LibRelName^ + #0
    else
      FileName := LibFileName^ + #0;

    TempFileName := ChangeFileExt(FileName, '.$$$');
    BakFileName := ChangeFileExt(FileName, '.~ws');
    S := TOleClientStream.Create(TempFileName, stCreate);
//    S.Init(StrPCopy(ABuffer, TempFileName), stCreate, AM_Def.StrBufSize);
    AText := ExtLibFileHeader;
    S.WriteString(AText);
    Version := ExtLibFileVersion;
    S.Write(Version, sizeof(Version));
    S.Put(Symbols);
    S.Put(Fonts);
    S.Free;
    if S.Status = stOK then
      Result := TRUE;
    if Result then
    begin
      if IniFile^.Options.BackupFile then
      begin
        if FileExists(BakFileName) then
          DeleteFile(StrPCopy(ABuffer, BakFileName));
        if FileExists(FileName) then
          Result := RenameFile(FileName, BakFileName);
      end
      else
        if FileExists(FileName) then
        begin
          Result := DeleteFile(StrPCopy(ABuffer, FileName));
        end;
      if Result then
      begin
        Result := RenameFile(TempFileName, FileName);
      end;
    end
    else
    begin
      DeleteFile(StrPCopy(ABuffer, TempFileName));
      LibState := LibState or el_Modified; { Speichern war nicht erfolgreich }
      MessageDialog(Format(MlgStringList['SymbolMsg', 2], [FileName]), mtError, [mbOK], 0);
    end
  end;
end;

procedure TExtLib.UnLoad;
begin
  if (Loaded) then
  begin
    if (Assigned(Symbols)) then
      Dispose(Symbols, Done);
    if (Assigned(Fonts)) then
      Dispose(Fonts, Done);
    Fonts := nil;
    Symbols := nil;
    LibState := LibState and (not el_Loaded);
  end;
end;

procedure TExtLib.ReLoad;
var
  ASymbols: PSymbols;
begin
  LoadExtLib; { neu aus Datei laden }
end;

function TExtLib.AlreadyLoaded: pExtLib;
var
  i: Integer;
  aExtLib: pExtLib;
  Libs: TList;
begin
  aExtLib := nil;
  Result := nil;
  Libs := WinGISMainForm.LoadedLibs;
  if Libs = nil then
    exit;
  i := 0;
  while ((i < Libs.Count) and (Result = nil)) do
  begin
    aExtLib := pExtLib(Libs.Items[i]);
    if aExtLib^.Loaded then
      if (aExtLib^.LibName^ = LibName^) then
        Result := aExtLib;
    Inc(i);
  end;
end;

function ExtLibAlreadyOpened(const AName: string): pExtLib;
var
  i: Integer;
  aExtLib: pExtLib;
  Libs: TList;
begin
  Result := nil;
  Libs := WinGISMainForm.LoadedLibs;
  i := 0;
  while ((i < Libs.Count) and (Result = nil)) do
  begin
    aExtLib := pExtLib(Libs.Items[i]);
    if (aExtLib^.Loaded) then
      if (aExtLib^.LibName^ = AName) then
        Result := aExtLib;
    Inc(i);
  end;
end;

function TExtLib.CheckName
  (
  AName: string;
  BSym: PSGroup
  ): Boolean;
begin
  CheckName := False;
  if (Loaded) then
    CheckName := Symbols^.CheckName(AName, LibName^, BSym);
end;

{*******************************************************************************
| TExtLib.InsertSymbol
|-------------------------------------------------------------------------------
| InsertSymbol f�gt ein Symbol in die ext. Bib ein, passt die Texte im Symbol
|   an die Fonts in der ext. Bib an und erh�ht die verw. Font-UseCounts
+******************************************************************************}

function TExtLib.InsertSymbol
  (
  PInfo: PPaint;
  ASym: PSGroup;
  Paint: Boolean
  )
  : Boolean;
begin
  Result := False;
  if (Loaded) then
  begin
    LibState := LibState or el_Modified;
    ASym^.SetLibName(LibName^);
    Result := Symbols^.InsertSymbol(PInfo, ASym, Paint);
    if (Result) and (PInfo <> nil) then
      CheckSymTexts(PInfo^.Fonts, ASym);
  end;
end;

{++ IDB}
{ I have to use my own function InsertAfterUndo 'cause ordinary functions add object with
  new index. But I need to restore object with OLD index. Ivanoff. 11.09.2000.}

procedure TExtLib.InsertSymbolAfterUndo(ASym: PSGroup);
begin
  if not SELF.Loaded then
    EXIT;
  LibState := LibState or el_Modified;
  ASym.SetLibName(SELF.LibName^);
  Symbols.InsertObjectAfterUndo(ASym);
end;
{-- IDB}

{*******************************************************************************
| TExtLib.DeleteSymbol
|-------------------------------------------------------------------------------
| DeleteSymbol l�scht ein Symbol aus der ext. Bib und �berpr�ft die
|   UseCounts der Fonts
+******************************************************************************}

function TExtLib.DeleteSymbol
  (
  ASym: pSGroup
  ): Boolean;
var
  DummyPInfo: pPaint;
  DelIndRes: Boolean;
  i: Integer;
  aText: pText;
  aFontDes: pFontDes;
begin
  DelIndRes := Symbols^.DeleteSymbol(ASym);
  if (DelIndRes) then
  begin
    for i := 0 to ASym^.Data^.Count - 1 do
    begin { Text-UseCount abchecken }
      aText := pText(ASym^.Data^.At(i));
      if (aText^.GetObjType = ot_Text) then
      begin
        aFontDes := Fonts^.GetFont(aText^.Font.Font);
        if (aFontDes <> nil) then
          Dec(aFontDes^.UseCount);
      end;
    end;
  end;
  Result := DelIndRes;
end;

{******************************************************************************}
{ CheckSymTexts passt ev. Texte im Symbol an die Font-Nr in der Ext. Bib an    }
{    Nicht vorhandene Fonts, werden aus aFonts kopiert (<>TSymbols.CheckTexts) }
{******************************************************************************}

procedure TExtLib.CheckSymTexts
  (
  aFonts: pFonts;
  ASym: pSGroup
  );
var
  i: LongInt;
  aObj: pIndex;
  aText: pText;
  aFontDes,
    ExtFontDes: pFontDes;
  FontID: Integer16;
begin
  if (aFonts = nil) then
    exit;
  i := 0;
  while (i < ASym^.Data^.Count) do
  begin
    aObj := PIndex(ASym^.Data^.At(i));
    if (aObj^.GetObjType = ot_Text) then
    begin { Texte anpassen }
      aText := pText(aObj);
      aFontDes := aFonts^.GetFont(aText^.Font.Font);
      if (aFontDes <> nil) then
      begin
        FontID := Fonts^.IDFromName(aFontDes^.Name^);
        if (FontID > 0) then
          aText^.Font.Font := FontID { Font exist., ID anpassen }
        else
        begin { Font exist nicht, kopieren }
          ExtFontDes := MakeObjectCopy(aFontDes); { Kopie erstellen }
          Inc(Fonts^.MaxNum);
          FontID := Fonts^.MaxNum;
          ExtFontDes^.FNum := FontID;
          Fonts^.Insert(ExtFontDes); { zu ext. Font hinzuf�gen }
          aText^.Font.Font := FontID;
        end;
      end
      else
        aText^.Font.Font := 1; { default : Text mit Nr 1 }
      ExtFontDes := Fonts^.GetFont(aText^.Font.Font);
      if (ExtFontDes <> nil) then
        Inc(ExtFontDes^.UseCount);
    end;
    Inc(i);
  end;
end;

procedure TExtLib.SetUseCounts;

  procedure DoAll(AItem: PSGroup); far;
  begin
    AItem^.UseCount := 0;
  end;
begin
  if (Loaded) and (Assigned(Symbols)) then
    Symbols^.Data^.ForEach(@DoAll);
end;

function TExtLib.GetSymbol
  (
  SymName: PString
  ): PSGroup;
var
  ASym,
    Found: PSGroup;
begin
  if (Loaded) then
  begin { nur wenn geladen! }
    ASym := new(PSGroup, InitName(PToStr(SymName), PToStr(LibName)));
    Found := PSGroup(Symbols^.SymTable.Find(ASym)); { Projektsym }
    Dispose(ASym, Done);
    GetSymbol := Found;
  end
  else
    GetSymbol := nil;
end;

function TExtLib.Loaded: Boolean;
begin
  Loaded := (LibState and el_Loaded) <> 0;
end;

function TExtLib.UnAvailable: Boolean;
begin
  UnAvailable := (LibState and el_UnAvailable) <> 0;
end;

function TExtLib.Modified: Boolean;
begin
  Modified := (LibState and el_Modified) <> 0;
end;

procedure TExtLib.SetModified(ASet: Boolean);
begin
  if (ASet) then
    LibState := LibState or el_Modified
  else
    LibState := LibState and (not el_Modified);
end;

destructor TExtLib.Done;
begin
  DisposeStr(LibName);
  DisposeStr(LibFileName);
  DisposeStr(LibRelName);
  if (Loaded) then
  begin
    if (Assigned(Symbols)) then
      Dispose(Symbols, Done);
    if (Assigned(Fonts)) then
      Dispose(Fonts, Done);
  end;
  inherited Done;
end;

procedure TExtLib.RemoveLastInstance;
var
  i: Integer;
  aExtLib: pExtLib;
  Libs: TList;
  Result: pExtLib;
begin
  if UseCount > 0 then
    Exit; { nothing to do !}
  Result := nil;
  if (Application = nil) then
    Exit;
  if (Application.MainForm = nil) then
    Exit;
  Libs := WinGISMainForm.LoadedLibs;
  if Libs = nil then
    Exit;
  i := 0;
  while ((i < Libs.Count) and (Result = nil)) do
  begin
    aExtLib := pExtLib(Libs.Items[i]);
    if (aExtLib^.Loaded) then
      if (aExtLib^.LibName^ = LibName^) then
        Libs.Delete(i);
    Inc(i);
  end;
end;

{******************************************************************************}
{ Methoden f�r TSymEditInfo                                                    }
{******************************************************************************}

function TSymEditInfo.CanCloseSymEditor: Boolean;
var
  ASymParent: Pointer;
begin
  if TMDIChild(SymEditChild).Data.Modified then
    Result := TSymEditor(SymEditor).CanEndEditor
  else
    Result := True;
//  CanCloseSymEditor:=False;
  ASymParent := SymParent; { zu aktivierendes Projekt merken }
//  if (TMDIChild(SymEditChild).CanClose) then begin
  if Result = True then
  begin
    TMDIChild(SymEditChild).Release; { Symboleditor-Childfenster-Tsch�ss }
//    CanCloseSymEditor:=True;
    SymbolToEdit^.InEditor := False; { Symbol wird nicht l�nger bearbeitet }
    if (ASymParent <> nil) and (TMDIChild(ASymParent).Data <> nil) then
    begin
{      if (SymExtLib=nil) then begin    { Symbib wird "hinter" Projekt bearbeitet }
      TMDIChild(ASymParent).Data^.SymEditInfo.SymEditChild := nil;
                                         { Projekt hat keinen Symboleditor mehr }
      TMDIChild(ASymParent).Show; { damit Symbolbib korrekt }
    end;
  end;
end;

function TSymEditInfo.CloseSymEditor: Boolean;
var
  AChild: TMDIChild;
begin
  CloseSymEditor := True;
  if SymEditChild <> nil then
  begin
    AChild := SymEditChild;
    AChild.Release;
    exit;
  end;

{$IFDEF RUSSIA}
  if (SymEditChild <> nil) and (TMDIChild(SymEditChild).Data.Modified) and (not SELF.EndSymEditor) then
  begin
    RESULT := FALSE;
    EXIT;
  end;
{$ENDIF}

  if (SymEditChild <> nil) and
    (SymParent = nil) then
  begin { Symboleditor ist noch ge�ffnet }
    AChild := SymEditChild;
    AChild.Show; { Symboleditor f�r Benutzer anzeigen }
    if (AChild.CanClose) then
    begin { ev. Speichermeldung }
      SymEditChild := nil;
      AChild.Release; { Symboleditor-Childfenster-Tsch�ss }
    end
    else
      CloseSymEditor := False; { Abbruch, nichts passiert }
  end;
end;

function TSymEditInfo.EndSymEditor: Boolean;
begin
  EndSymEditor := True;
{$IFNDEF TESTING}
  if (TSymEditor(SymEditor).CanEndEditor) then
  begin { �nderungen zu speichern ? }
{$ENDIF}
    if (SymExtLib = nil) then
      TMDIChild(SymParent).Data^.SymEditInfo.SymEditChild := nil { Projekt ist kein Symboleditor mehr }
{$IFNDEF TESTING}
  end
  else
    EndSymEditor := False; { Abbruch nichts tun }
{$ENDIF}
end;

procedure TSymEditInfo.ResetEditChildPtr;
begin
  if (SymParent <> nil) then
    if (SymExtLib = nil) then
      TMDIChild(SymParent).Data^.SymEditInfo.SymEditChild := nil; { "Vaterprojekt" hat keinen Symboleditor mehr }
end;

{******************************************************************************}
{ Registrierungsteil                                                           }
{******************************************************************************}

const
  RExtLib: TStreamRec = (
    ObjType: rn_ExtLib;
    VmtLink: TypeOf(TExtLib);
    Load: @TExtLib.Load;
    Store: @TExtLib.Store);
begin
  RegisterType(RExtLib);
end.

