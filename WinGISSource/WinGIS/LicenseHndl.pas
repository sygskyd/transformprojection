unit LicenseHndl;

interface

uses Windows;

var
WingisLicense: Integer;

{$IFNDEF AXDLL}
function GetUserId: DWord; external 'GisReg.dll';
function GetWingisLicense: Integer; external 'GisReg.dll';
procedure AddLicense; external 'GisReg.dll';
procedure ShowInfoDialog(s: AnsiString; Info: AnsiString); external 'GisReg.dll';
{$ENDIF}

implementation

end.
