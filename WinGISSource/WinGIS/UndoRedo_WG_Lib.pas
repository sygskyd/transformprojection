unit UndoRedo_WG_Lib;

interface

uses Windows, Forms, Messages, Classes, Objects,
   IDB_CallbacksDef, Lists;

type
   { Types to connect to the library's functions and procedures. }
   //********     Initialization and finalization routnes of the DLL.  ************
   TInitLibrary = function (AnApplicationHandle: Integer; ALanguageFileExtension: PChar): Boolean; stdcall;
   TFinLibrary = procedure; stdcall;

   TGetVersion = function: PChar; stdcall;
   //****************            IDB core routines.           *********************
   TAddProject = function (AProjectFormHandle: Integer; AProject: Pointer; bProjectFileHasReadOnlyState: Boolean): Boolean; stdcall;
   TDeleteProject = procedure (AProject: Pointer); stdcall;

   TSet_DoNotAddAttributeRecords_Sign = procedure (AProject: Pointer; bValue: Boolean); stdcall;
   TDeleteLayerTables = procedure (AProject, ALayer: Pointer); stdcall;
   TLoadGeoTextData = function (AProject, ALayer: Pointer; pcSourceDBFileName, pcSourceFieldName, pcDestinationFieldName, pcProgisIDFieldName: PChar): Boolean; stdcall;
   TGetActiveWindowProject = function: Pointer; stdcall;

   //****************   WinGIS <-> IDB interaction routines    ********************
   { Callback functions }

   TGetProjectFileSizeCallback = procedure (AProcedure: TGetProjectFileSize); stdcall;
   TGetProjectUndoStepsCountCallback = procedure (AProcedure: TGetProjectUndoStepsCount); stdcall;

   TClearUndoData = procedure (AProject: Pointer); stdcall;
   TGetReadyToReceiveUndoData = procedure (AProject: Pointer); stdcall;
   TSet_ThereIsNoNecessityToPutDataInUndo_Sign = procedure (AProject: Pointer; bValue: Boolean); stdcall;
   TSet_AnExternalDBIsWorking_Sign = procedure (AProject: Pointer; bValue: Boolean); stdcall;
   TPutDataInUndo = function (AProject: Pointer; Adata: Pointer; iSize: Integer): Boolean; stdcall;
   TSaveUndoData = procedure (AProject: Pointer; AnUndoType: TUndoType; bClearRedoData: Boolean); stdcall;
   TThereAreDataForRestoring = function (AProject: Pointer): Boolean; stdcall;
   TGetNextUndoDescription = function (AProject: Pointer): PChar; stdcall;
   TGetUndoData = function (AProject: Pointer; var pBuff: Pointer; var iSize: Integer; var AnUndoType: TUndoType): Boolean; stdcall;

   TOnChangeUndoStepsCount = procedure (AProject: Pointer; iNewUndoStepsCount: integer); stdcall;
   TAddSymbolMappingEntry = procedure (AProject: Pointer; pcSymLibName, pcSymName, pcNewSymName: PChar); stdcall;
   TSymbolNameExists = function (AProject: Pointer; pcSymbolLibraryName, pcSymbolName: PChar): Boolean; stdcall;
   TGetSymbolActualName = function (AProject: Pointer; pcSymbolLibraryName, pcSymbolName: PChar): PChar; stdcall;

   TClearRedoData = procedure (AProject: Pointer); stdcall;
   TGetReadyToReceiveRedoData = procedure (AProject: Pointer); stdcall;
   TPutDataInRedo = function (AProject: Pointer; AData: Pointer; iSize: Integer; bNeedToIncObjCount: Boolean): Boolean; stdcall;
   TSaveRedoData = procedure (AProject: Pointer; ARedoType: TRedoType); stdcall;
   TThereAreDataForRedoing = function (AProject: Pointer): Boolean; stdcall;
   TGetNextRedoDescription = function (AProject: Pointer): PChar; stdcall;
   TGetRedoData = function (AProject: Pointer; var ABuffer: Pointer; var ASize: Integer; var ARedoType: TRedoType): Boolean; stdcall;

   TGetReadyToReceiveIDsOfObjectsToBeRestored = function (AProject: Pointer): Boolean; stdcall;
   TReceiveIDOfObjectToBeRestored = procedure (AProject: Pointer; ALayer: Pointer; iObjectID: Integer); stdcall;
   TExecuteRestoring = procedure (AProject: Pointer); stdcall;

   { Class that implements the access to IDB. }
   TUndoRedoLib = class
   private
      { IDB library handle }
      hURLibrary: THandle;
      bTheLibraryWasInitialized: Boolean;
      { Initialization and finalization routnes of the DLL }
      InitLibraryFunc: TInitLibrary;
      FinLibraryProc: TFinLibrary;
      GetVersionFunc: TGetVersion;
      { IDB core routines }
      AddProjectFunc: TAddProject;
      DeleteProjectProc: TDeleteProject;
      { Callback functions }
      SetGetProjectUndoStepsCountCallbackProc: TGetProjectUndoStepsCountCallback;
      SetGetProjectFileSizeCallbackProc: TGetProjectFileSizeCallback;
      { Undo routines }
      ClearUndoDataProc: TClearUndoData;
      GetReadyToReceiveUndoDataProc: TGetReadyToReceiveUndoData;
      Set_ThereIsNoNecessityToPutDataInUndo_SignProc: TSet_ThereIsNoNecessityToPutDataInUndo_Sign;
      Set_AnExternalDBIsWorking_SignProc: TSet_AnExternalDBIsWorking_Sign;
      PutDataInUndoFunc: TPutDataInUndo;
      SaveUndoDataProc: TSaveUndoData;
      ThereAreDataForRestoringFunc: TThereAreDataForRestoring;
      GetNextUndoDescriptionFunc: TGetNextUndoDescription;
      GetUndoDataFunc: TGetUndoData;
      OnChangeUndoStepsCountProc: TOnChangeUndoStepsCount;
      AddSymbolMappingEntryProc: TAddSymbolMappingEntry;
      SymbolNameExistsFunc: TSymbolNameExists;
      GetSymbolActualNameFunc: TGetSymbolActualName;

      { Redo routines }
      ClearRedoDataProc: TClearRedoData;
      GetReadyToReceiveRedoDataProc: TGetReadyToReceiveRedoData;
      PutDataInRedoFunc: TPutDataInRedo;
      SaveRedoDataProc: TSaveRedoData;
      ThereAreDataForRedoingFunc: TThereAreDataForRedoing;
      GetNextRedoDescriptionFunc: TGetNextRedoDescription;
      GetRedoDataFunc: TGetRedoData;

   public
      constructor Create (AMainForm: TForm; sIDBLibFileName, sLanguageFileExtension: AnsiString);
      destructor Free;

      function LibraryWasLoaded: Boolean;
      function GetLibraryVersion: AnsiString;

      function AddProject (AProjectFormHandle: Integer; AProject: Pointer; bProjectFileHasReadOnlyState: Boolean): Boolean;
      procedure DeleteProject (AProject: Pointer);

      procedure SetGetProjectUndoStepsCountCallback (AFunction: TGetProjectUndoStepsCount);
      procedure SetGetProjectFileSizeCallback (AFunction: TGetProjectFileSize);
      { Undo routines }
      procedure ClearUndoData (AProject: Pointer);
      procedure GetReadyToReceiveUndoData (AProject: Pointer);
      procedure Set_ThereIsNoNecessityToPutDataInUndo_Sign (AProject: Pointer; bValue: Boolean);

      function PutDataInUndo (AProject: Pointer; AData: Pointer; iSize: Integer): Boolean;
      procedure SaveUndoData (AProject: Pointer; AnUndoType: TUndoType = utUnknownUNDO; bClearRedoData: Boolean = TRUE);
      function ThereAreDataForRestoring (AProject: Pointer): Boolean;
      function GetNextUndoDescription (AProject: Pointer): AnsiString;
      function GetUndoData (AProject: Pointer; var pBuff: Pointer; var iSize: Integer; var AnUndoType: TUndoType): Boolean; // AnsiString;
      procedure OnChangeUndoStepsCount (AProject: Pointer; iNewUndoStepsCount: integer);
      procedure AddSymbolMappingEntry (AProject: Pointer; sSymLibName, sSymName, sNewSymName: AnsiString);
      function SymbolNameExists (AProject: Pointer; sSymbolLibraryName, sSymbolName: AnsiString): Boolean;
      function GetSymbolActualName (AProject: Pointer; sSymbolLibraryName, sSymbolName: AnsiString): AnsiString;
      { Redo routines }
      procedure ClearRedoData (AProject: Pointer);
      procedure GetReadyToReceiveRedoData (AProject: Pointer);
      function PutDataInRedo (AProject: Pointer; AData: Pointer; iSize: Integer; bNeedToIncObjCount: Boolean): Boolean;
      procedure SaveRedoData (AProject: Pointer; ARedoType: TRedoType);
      function ThereAreDataForRedoing (AProject: Pointer): Boolean;
      function GetNextRedoDescription (AProject: Pointer): AnsiString;
      function GetRedoData (AProject: Pointer; var ABuffer: Pointer; var ASize: Integer; var ARedoType: TRedoType): Boolean;

   end;

implementation

uses SysUtils, AM_Main,
   MultiLng;

constructor TUndoRedoLib.Create (AMainForm: TForm; sIDBLibFileName, sLanguageFileExtension: AnsiString);
var
   pcLangFileExt: PChar;
   iLength: Integer;
begin
   bTheLibraryWasInitialized := FALSE;
   // Load IDB dll.
   hURLibrary := LoadLibrary (PChar (sIDBLibFileName));
   WingisMainForm.CheckLibStatus (hURLibrary, ExtractFileName (sIDBLibFileName), 'Undo/Redo');
   if hURLibrary = 0 then EXIT;
   // Detect all function from IDB dll.
   //********     Initialization and finalization routnes of the DLL.  **********//
   InitLibraryFunc := GetProcAddress (hURLibrary, 'InitLibrary');
   if @InitLibraryFunc = nil then EXIT;

   FinLibraryProc := GetProcAddress (hURLibrary, 'FinLibrary');
   if @FinLibraryProc = nil then EXIT;

   GetVersionFunc := GetProcAddress (hURLibrary, 'GetVersion');

   //****************            IDB core routines            *******************//
   AddProjectFunc := GetProcAddress (hURLibrary, 'AddProject');
   if @AddProjectFunc = nil then EXIT;

   DeleteProjectProc := GetProcAddress (hURLibrary, 'DeleteProject');
   if @DeleteProjectProc = nil then EXIT;

   { Callback functions }
   SetGetProjectUndoStepsCountCallbackProc := GetProcAddress (hURLibrary, 'GetProjectUndoStepsCountCallback');
   if @SetGetProjectUndoStepsCountCallbackProc = nil then EXIT;

   SetGetProjectFileSizeCallbackProc := GetProcAddress (hURLibrary, 'GetProjectFileSizeCallback');
   if @SetGetProjectFileSizeCallbackProc = nil then EXIT;

   { Undo routines }
   ClearUndoDataProc := GetProcAddress (hURLibrary, 'ClearUndoData');
   if @ClearUndoDataProc = nil then EXIT;

   GetReadyToReceiveUndoDataProc := GetProcAddress (hURLibrary, 'GetReadyToReceiveUndoData');
   if @GetReadyToReceiveUndoDataProc = nil then EXIT;

   Set_ThereIsNoNecessityToPutDataInUndo_SignProc := GetProcAddress (hURLibrary, 'Set_ThereIsNoNecessityToPutDataInUndo_Sign');
   if @Set_ThereIsNoNecessityToPutDataInUndo_SignProc = nil then EXIT;

   PutDataInUndoFunc := GetProcAddress (hURLibrary, 'PutDataInUndo');
   if @PutDataInUndoFunc = nil then EXIT;

   SaveUndoDataProc := GetProcAddress (hURLibrary, 'SaveUndoData');
   if @SaveUndoDataProc = nil then EXIT;

   ThereAreDataForRestoringFunc := GetProcAddress (hURLibrary, 'ThereAreDataForUndoing');
   if @ThereAreDataForRestoringFunc = nil then EXIT;

   GetNextUndoDescriptionFunc := GetProcAddress (hURLibrary, 'GetNextUndoDescription');
   if @GetNextUndoDescriptionFunc = nil then EXIT;

   GetUndoDataFunc := GetProcAddress (hURLibrary, 'GetUndoData');
   if @GetUndoDataFunc = nil then EXIT;

   OnChangeUndoStepsCountProc := GetProcAddress (hURLibrary, 'OnChangeUndoStepsCount');
   if @OnChangeUndoStepsCountProc = nil then EXIT;

   AddSymbolMappingEntryProc := GetProcAddress (hURLibrary, 'AddSymbolMappingEntry');
   if @AddSymbolMappingEntryProc = nil then EXIT;

   SymbolNameExistsFunc := GetProcAddress (hURLibrary, 'SymbolNameExists');
   if @SymbolNameExistsFunc = nil then EXIT;

   GetSymbolActualNameFunc := GetprocAddress (hURLibrary, 'GetSymbolActualName');
   if @GetSymbolActualNameFunc = nil then EXIT;

   { Redo routines }
   ClearRedoDataProc := GetProcAddress (hURLibrary, 'ClearRedoData');
   if @ClearRedoDataProc = nil then EXIT;

   GetReadyToReceiveRedoDataProc := GetProcAddress (hURLibrary, 'GetReadyToReceiveRedoData');
   if @GetReadyToReceiveRedoDataProc = nil then EXIT;

   PutDataInRedoFunc := GetProcAddress (hURLibrary, 'PutDataInRedo');
   if @PutDataInRedoFunc = nil then EXIT;

   SaveRedoDataProc := GetProcAddress (hURLibrary, 'SaveRedoData');
   if @SaveRedoDataProc = nil then EXIT;

   ThereAreDataForRedoingFunc := GetProcAddress (hURLibrary, 'ThereAreDataForRedoing');
   if @ThereAreDataForRedoingFunc = nil then EXIT;

   GetNextRedoDescriptionFunc := GetProcAddress (hURLibrary, 'GetNextRedoDescription');
   if @GetNextRedoDescriptionFunc = nil then EXIT;

   GetRedoDataFunc := GetProcAddress (hURLibrary, 'GetRedoData');
   if @GetRedoDataFunc = nil then EXIT;

   // Init IDB library.
   iLength := Length (sLanguageFileExtension) + 1;
   GetMem (pcLangFileExt, iLength);
   try
      StrPCopy (pcLangFileExt, sLanguageFileExtension);
      if @InitLibraryFunc <> nil then
         bTheLibraryWasInitialized := InitLibraryFunc (Integer (Application.Handle), pcLangFileExt);
   finally
      FreeMem (pcLangFileExt, iLength);
   end;
end;

destructor TUndoRedoLib.Free;
begin
   // Get ready to unload IDB dll.
   if @FinLibraryProc <> nil then FinLibraryProc;
   if hURLibrary <> 0 then FreeLibrary (hURLibrary);
end;

function TUndoRedoLib.LibraryWasLoaded: Boolean;
begin
   RESULT := SELF.bTheLibraryWasInitialized;
end;

function TUndoRedoLib.GetLibraryVersion: AnsiString;
begin
   if @GetVersionFunc <> nil then
      RESULT := GetVersionFunc
   else
      RESULT := MlgStringList['IDB', 12]; // Unknown
end;

//****************            IDB core routines.           *******************//

function TUndoRedoLib.AddProject (AProjectFormHandle: Integer; AProject: Pointer; bProjectFileHasReadOnlyState: Boolean): Boolean;
begin
   if @AddProjectFunc <> nil then
      RESULT := AddProjectFunc (AProjectFormHandle, AProject, bProjectFileHasReadOnlyState)
   else
      RESULT := FALSE;
end;

procedure TUndoRedoLib.DeleteProject (AProject: Pointer);
begin
   if (@DeleteProjectProc <> nil) then
      DeleteProjectProc (AProject);
end;

procedure TUndoRedoLib.SetGetProjectUndoStepsCountCallback (AFunction: TGetProjectUndoStepsCount);
begin
   if @SetGetProjectUndoStepsCountCallbackProc <> nil then
      SetGetProjectUndoStepsCountCallbackProc (AFunction);
end;

procedure TUndoRedoLib.SetGetProjectFileSizeCallback (AFunction: TGetProjectFileSize);
begin
   if @SetGetProjectFileSizeCallbackProc <> nil then
      SetGetProjectFileSizeCallbackProc (AFunction);
end;

procedure TUndoRedoLib.ClearUndoData (AProject: Pointer);
begin
   if @ClearUndoDataProc <> nil then
      ClearUndoDataProc (AProject);
end;

procedure TUndoRedoLib.GetReadyToReceiveUndoData (AProject: Pointer);
begin
   if @GetReadyToReceiveUndoDataProc <> nil then
      GetReadyToReceiveUndoDataProc (AProject);
end;

function TUndoRedoLib.PutDataInUndo (AProject: Pointer; AData: Pointer; iSize: Integer): Boolean;
begin
   if @PutDataInUndoFunc <> nil then
      RESULT := PutDataInUndoFunc (AProject, AData, iSize)
   else
      RESULT := FALSE;
end;

procedure TUndoRedoLib.SaveUndoData (AProject: Pointer; AnUndoType: TUndoType = utUnknownUNDO; bClearRedoData: Boolean = TRUE);
begin
   if @SaveUndoDataProc <> nil then
      SaveUndoDataProc (AProject, AnUndoType, bClearRedoData);
end;

function TUndoRedoLib.ThereAreDataForRestoring (AProject: Pointer): Boolean;
begin
   if @ThereAreDataForRestoringFunc <> nil then
      RESULT := ThereAreDataForRestoringFunc (AProject)
   else
      RESULT := FALSE;
end;

function TUndoRedoLib.GetNextUndoDescription (AProject: Pointer): AnsiString;
var
   pcData: PChar;
begin
   if @GetNextUndoDescriptionFunc <> nil then
   begin
      pcData := GetNextUndoDescriptionFunc (AProject);
      RESULT := pcData;
      FreeMem (pcData);
   end
   else
      RESULT := '';
end;

function TUndoRedoLib.GetUndoData (AProject: Pointer; var pBuff: Pointer; var iSize: Integer; var AnUndoType: TUndoType): Boolean; //AnsiString;
begin
   if @GetUndoDataFunc <> nil then
      RESULT := GetUndoDataFunc (AProject, pBuff, iSize, AnUndoType)
   else
      RESULT := FALSE;
end;

procedure TUndoRedoLib.OnChangeUndoStepsCount (AProject: Pointer; iNewUndoStepsCount: integer);
begin
   if @OnChangeUndoStepsCountProc <> nil then
      OnChangeUndoStepsCountProc (AProject, iNewUndoStepsCount);
end;

procedure TUndoRedoLib.AddSymbolMappingEntry (AProject: Pointer; sSymLibName, sSymName, sNewSymName: AnsiString);
begin
   if @AddSymbolMappingEntryProc <> nil then
      AddSymbolMappingEntryProc (AProject, PChar (sSymLibName), PChar (sSymName), PChar (sNewSymName));
end;

function TUndoRedoLib.SymbolNameExists (AProject: Pointer; sSymbolLibraryName, sSymbolName: AnsiString): Boolean;
begin
   if @SymbolNameExistsFunc <> nil then
      RESULT := SymbolNameExistsFunc (AProject, PChar (sSymbolLibraryName), PChar (sSymbolName))
   else
      RESULT := FALSE;
end;

function TUndoRedoLib.GetSymbolActualName (AProject: Pointer; sSymbolLibraryName, sSymbolName: AnsiString): AnsiString;
begin
   if @GetSymbolActualNameFunc <> nil then
      RESULT := GetSymbolActualNameFunc (AProject, PChar (sSymbolLibraryName), PChar (sSymbolName))
   else
      RESULT := '';
end;

procedure TUndoRedoLib.Set_ThereIsNoNecessityToPutDataInUndo_Sign (AProject: Pointer; bValue: Boolean);
begin
   if @Set_ThereIsNoNecessityToPutDataInUndo_SignProc <> nil then
      Set_ThereIsNoNecessityToPutDataInUndo_SignProc (AProject, bValue);
end;

procedure TUndoRedoLib.ClearRedoData (AProject: Pointer);
begin
   if @ClearRedoDataProc <> nil then
      ClearRedoDataProc (AProject);
end;

procedure TUndoRedoLib.GetReadyToReceiveRedoData (AProject: Pointer);
begin
   if @GetReadyToReceiveRedoDataProc <> nil then
      GetReadyToReceiveRedoDataProc (AProject);
end;

function TUndoRedoLib.PutDataInRedo (AProject: Pointer; AData: Pointer; iSize: Integer; bNeedToIncObjCount: Boolean): Boolean;
begin
   if @PutDataInRedoFunc <> nil then
      RESULT := PutDataInRedoFunc (AProject, AData, iSize, bNeedToIncObjCount)
   else
      RESULT := FALSE;
end;

procedure TUndoRedoLib.SaveRedoData (AProject: Pointer; ARedoType: TRedoType);
begin
   if @SaveRedoDataProc <> nil then
      SaveRedoDataProc (AProject, ARedoType);
end;

function TUndoRedoLib.ThereAreDataForRedoing (AProject: Pointer): Boolean;
begin
   if @ThereAreDataForRedoingFunc <> nil then
      RESULT := ThereAreDataForRedoingFunc (AProject)
   else
      RESULT := FALSE;
end;

function TUndoRedoLib.GetNextRedoDescription (AProject: Pointer): AnsiString;
var
   pcData: PChar;
begin
   if @GetNextRedoDescriptionFunc <> nil then
   begin
      pcData := GetNextRedoDescriptionFunc (AProject);
      RESULT := pcData;
      FreeMem (pcData);
   end
   else
      RESULT := '';
end;

function TUndoRedoLib.GetRedoData (AProject: Pointer; var ABuffer: Pointer; var ASize: Integer; var ARedoType: TRedoType): Boolean;
begin
   if @GetRedoDataFunc <> nil then
      RESULT := GetRedoDataFunc (AProject, ABuffer, ASize, ARedoType)
   else
      RESULT := FALSE;
end;

end.

