Unit ConstructHndl;

Interface

Uses ProjHndl,RegDB;

Type TDrawConstructHandler = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;

     TCustomConstructHandler = Class(TProjMenuHandler)
       Procedure   OnActivate; override;
       Procedure   OnDeActivate; override;
       Procedure   OnStart; override;
     end;

     TConstructLinesHandler = Class(TCustomConstructHandler)
       Procedure   OnStart; override;
     end;

     TConstructParaHandler = Class(TCustomConstructHandler)
       Procedure   OnStart; override;
     end;

     TConstructDistanceHandler = Class(TCustomConstructHandler)
       Procedure   OnStart; override;
     end;

     TConstructOrthogonalHandler = Class(TCustomConstructHandler)
       Procedure   OnStart; override;
     end;
                                           
     TConstructTwoDistancesHandler = Class(TCustomConstructHandler)
       Procedure   OnStart; override;
     end;

     TConstructTwoAnglesHandler = Class(TCustomConstructHandler)
       Procedure   OnStart; override;
     end;

     TConstructAngleHandler = Class(TCustomConstructHandler)
       Procedure   OnStart; override;
     end;

     TConstructOptionsHandler = Class(TProjMenuHandler)
       Procedure   OnStart; override;
     end;

Implementation

{$R *.mfn}

Uses AM_Def,AM_ProjM,MenuFn,AM_ProjP;

{===============================================================================
  TDrawConstructHandler
+==============================================================================}

Procedure TDrawConstructHandler.OnStart;
var CurrentFn      : String;
begin                                                     
  with Project^ do begin
    CurrentFn:=Registry.ReadString('\User\WinGISUI\DrawingTools\Construct\InputType');
    if Registry.LastError<>rdbeNoError then CurrentFn:='ConstructDistance';
    MenuFunctions[CurrentFn].Execute;
  end;
end;

{===============================================================================
  TCustomConstructHandler
+==============================================================================}

Procedure TCustomConstructHandler.OnStart;
begin
  Project^.Registry.WriteString('\User\WinGISUI\DrawingTools\Construct\InputType',MenuName);
end;

Procedure TCustomConstructHandler.OnActivate;
begin
  MenuFunctions['DrawConstructions'].Checked:=TRUE;
end;

Procedure TCustomConstructHandler.OnDeActivate;
begin
  MenuFunctions['DrawConstructions'].Checked:=FALSE;
end;

Procedure TConstructDistanceHandler.OnStart;
begin
  inherited OnStart;
  Project^.SetActualMenu(mn_DistMark);
end;

procedure TConstructLinesHandler.OnStart;
begin
  inherited OnStart;
  Project^.SetActualMenu(mn_LineMark);
end;

procedure TConstructParaHandler.OnStart;
begin
  inherited OnStart;
  {$IFNDEF WMLT}
  {$IFNDEF AXDLL} // <----------------- AXDLL
  ProcMakeOffset2(Project);
  {$ENDIF} // <----------------- AXDLL
  {$ENDIF}
end;

{===============================================================================
  TConstructOrthogonalHandler
+==============================================================================}

Procedure TConstructOrthogonalHandler.OnStart;
begin
  inherited OnStart;
  Project^.SetActualMenu(mn_OrthoMark);
end;

{===============================================================================
  TConstructTwoDistancesHandler
+==============================================================================}

Procedure TConstructTwoDistancesHandler.OnStart;
begin
  inherited OnStart;
  Project^.SetActualMenu(mn_CircleCutMark);
end;

{===============================================================================
  TConstructTwoDistancesHandler
+==============================================================================}

Procedure TConstructTwoAnglesHandler.OnStart;
begin
  inherited OnStart;
  Project^.SetActualMenu(mn_AngleMark);
end;

{===============================================================================
  TConstructAngleHandler
+==============================================================================}

procedure TConstructAngleHandler.OnStart;
begin
  inherited OnStart;
  Project^.SetActualMenu(mn_CircleMark);
end;

{===============================================================================
  TConstructOptionsHandler
+==============================================================================}

Procedure TConstructOptionsHandler.OnStart;
begin
  SetConstructOptions(Project);
end;

{ TConstructLineHandler }

Initialization;
  MenuFunctions.RegisterFromResource(HInstance,'ConstructHndl','ConstructHndl');
  TDrawConstructHandler.Registrate('DrawConstructions');
  TConstructDistanceHandler.Registrate('ConstructDistance');
  TConstructLinesHandler.Registrate('ConstructLines');
  TConstructParaHandler.Registrate('ConstructPara');
  TConstructOrthogonalHandler.Registrate('ConstructOrthogonal');
  TConstructTwoDistancesHandler.Registrate('ConstructTwoDistances');
  TConstructTwoAnglesHandler.Registrate('ConstructTwoAngles');
  TConstructAngleHandler.Registrate('ConstructAngle');
  TConstructOptionsHandler.Registrate('ConstructOptions');

end.
