unit BitmapPropertiesDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, {Graphics,} Controls, Forms, Dialogs,
  StdCtrls, WCtrls, ExtCtrls, Spinbtn, Validate, StyleDef, PropCollect,
  MultiLng,BMPImage;

type
  TTABAction = (taNone, taFresh, taUpdateAdd, taAdd);
  TBitmapPropertiesDialog = class(TWForm)
    Bevel1: TBevel;
    BlackTransparencyBtn: TRadioButton;
    CancelBtn: TButton;
    ControlPanel: TPanel;
    Group1: TWGroupBox;
    Group2: TWGroupBox;
    MlgSection: TMlgSection;
    NoChangeTransparencyBtn: TRadioButton;
    OKBtn: TButton;
    ShowFrameCheck: TCheckBox;
    ShowPictureCheck: TCheckBox;
    TransparencyEdit: TEdit;
    TransparencySpin: TSpinBtn;
    TransparencyVal: TMeasureLookupValidator;
    WLabel1: TWLabel;
    WhiteTransparencyBtn: TRadioButton;
    Group3: TWGroupBox;
    FreshTABButton: TRadioButton;
    UpdateTABButton: TRadioButton;
    AddTABButton: TRadioButton;
    EditTABButton: TButton;
    DoNothing: TRadioButton;
    procedure FormShow(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure EditTABButtonClick(Sender: TObject);
  private
    FOrgStyle: TBitmapProperties;
    FStyle: TBitmapProperties;
    procedure ResetTABService;
  public
//++Sygsky: TAB service
    Amount: Integer;
    WithTAB: Integer;
    ABitmap: BMPImage.PImage;
    TABAction: TTABAction;
//--Sygsky
    property BitmapStyle: TBitmapProperties read FStyle write FOrgStyle;
  end;

implementation

{$R *.DFM}

uses Measures, TABFiles, TabDlg;

procedure TBitmapPropertiesDialog.FormShow(Sender: TObject);
  procedure SetChecked
      (
      Control: TCheckBox;
      State: ShortInt
      );
  begin
    if State < 0 then
    begin
      Control.AllowGrayed := TRUE;
      Control.State := cbGrayed;
    end
    else
      Control.Checked := State = bsYes;
  end;
begin
  if FOrgStyle <> nil then FStyle.Assign(FOrgStyle);
  with FStyle do
  begin
    SetChecked(ShowPictureCheck, ShowBitmap);
    SetChecked(ShowFrameCheck, ShowFrame);
    TransparencyVal.LookupList.Clear;
    SetIntegerField(Transparency, 1, TransparencyVal, muPercent, TransparencyOptions);
    if TransparencyType < 0 then
    begin
      NoChangeTransparencyBtn.Visible := TRUE;
      NoChangeTransparencyBtn.Checked := TRUE;
    end
    else
      if TransparencyType = tt_Black then
        BlackTransparencyBtn.Checked := TRUE
      else
        WhiteTransparencyBtn.Checked := TRUE;
    if not UpdatesEnabled then SetControlGroupEnabled(ControlPanel, FALSE);
  end;
//++Sygsky: TAB service
  ResetTABService;
//--Sygsky
end;

procedure TBitmapPropertiesDialog.FormHide(Sender: TObject);
  function GetChecked
      (
      Control: TCheckBox
      )
      : ShortInt;
  begin
    case Control.State of
      cbChecked: Result := bsYes;
      cbUnchecked: Result := bsNo;
    else
      Result := bsNoChange;
    end;
  end;
begin
  with FStyle do
  begin
    PropertyMode := pmSet;
    ShowBitmap := GetChecked(ShowPictureCheck);
    ShowFrame := GetChecked(ShowFrameCheck);
    Transparency := GetIntegerField(1, TransparencyVal, muPercent, TransparencyOptions);
    if NoChangeTransparencyBtn.Checked then
      TransparencyType := intNoChange
    else
      if BlackTransparencyBtn.Checked then
        TransparencyType := tt_Black
      else
        TransparencyType := tt_White
  end;
  if FOrgStyle <> nil then FOrgStyle.Assign(FStyle);

  if FreshTABButton.Checked then
    TABAction := taFresh
  else
    if UpdateTABButton.Checked then
      TABAction := taUpdateAdd
    else
      if AddTABButton.Checked then
        TABAction := taAdd;
end;

procedure TBitmapPropertiesDialog.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  CanClose := CheckValidators(Self);
end;

procedure TBitmapPropertiesDialog.FormCreate(Sender: TObject);
begin
  FStyle := TBitmapProperties.Create;
  Amount := 0;
  WithTAB := 0;
  TabAction := taNone;
end;

procedure TBitmapPropertiesDialog.FormDestroy(Sender: TObject);
begin
  FStyle.Free;
end;

procedure TBitmapPropertiesDialog.ResetTABService;
begin

  // None is always enabled
  // DoNothing.Enabled := True;

  //Freshen action is possible only if some TAB exists
  FreshTAbButton.Enabled := WithTAB > 0;

  // If not all images have TABs, button should be enabled
  AddTABButton.Enabled := WithTAB < Amount;

  // Update and /or create button has the same ability as previous one
   UpdateTABButton.Enabled :=  AddTABButton.Enabled and (WithTAB > 0);


  // Edit is possible only for single image TAB
  EditTABButton.Enabled := (Amount = 1) and (WithTAB = 1);
end;

procedure TBitmapPropertiesDialog.EditTABButtonClick(Sender: TObject);
var
  RefArray: TRefArray;
  Name: AnsiString;
  Tab: TTabFile;
begin
  Name := ChangeFileExt(ABitmap.FileName^, TabExtension); // Name for the TAB file
  Tab := TTabFile.Create(Name);
  try
    // Fill Reference points Array
    if Tab.IsValid  and Tab.NameCoincides(ABitMap.FileName^)then
      if Tab.FillRefPoints(RefArray) then
        // Use them
        ConfirmTABUse(Self, RefArray, ABitMap.FileName^, TRUE); // Don't edit it
  finally
    TAb.Free;
  end;
end;

end.

