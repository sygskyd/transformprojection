�
 TPRNFRAMEDIALOG 0x  TPF0TPrnFramedialogPrnFramedialogLeft� Top� BorderIconsbiSystemMenu BorderStylebsDialogCaption!1ClientHeight+ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoDesktopCenter
PrintScalepoNoneOnClose	FormCloseOnCloseQueryFormCloseQueryOnCreate
FormCreateOnShowFormShowPixelsPerInch`
TextHeight 
TWGroupBox
WGroupBox1Left Top Width�HeightTabOrder  TLabelLabel6Left� TopWidth	HeightCaption!4  TLabelLabel8Left� TopWidth	HeightCaption!3  TLabelDimLabelLeft8TopWidth� HeightAutoSizeCaptionDimLabel  TLabel
ScalelabelLeft8TopWidth� HeightAutoSizeCaption
ScaleLabel  	TCheckBoxAddModeCheckLeftTopWidth� HeightCaption!2TabOrder OnClickAddModeCheckClick  
TWGroupBox
WGroupBox2Left	Top� Width� HeightfCaption!15TabOrderBoxStyle	bsTopLine TLabelLabel1LeftLTopOWidthHeightCaption!19  TLabelLabel2LeftLTop.WidthHeight	AlignmenttaRightJustifyCaption!18  	TCheckBoxUsecrosscheckLeftTopWidth� HeightCaption!17TabOrder OnClickUsecrosscheckClick  TPanel
CrosspanelLeftxTopKWidthkHeightColorclBlackTabOrderOnClickCrosspanelClick  TEdit	CrossEditLeftxTop*WidthYHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderOnChangeCrossEditChange  TSpinBtnSpinBtn2Left� Top*WidthHeight	Arrowkeys	Increment       ��?Max �P]���C	Validator
CrossValid   
TWGroupBox
WGroupBox3LeftTop,Width�HeightoCaption!5TabOrder TLabelSteplenghtlabelLeft`TopWidthHeight	AlignmenttaRightJustifyCaption!12  TLabel	MarklabelLeft`Top6WidthHeight	AlignmenttaRightJustifyCaption!13  TLabelLabel7Left$TopVWidthJHeight	AlignmenttaRightJustifyAutoSizeCaption!14  	TCheckBoxAddmarkcheckLeftTopWidth� HeightCaption!6TabOrder OnClickAddmarkcheckClick  	TCheckBox
RightCheckLeft� Top%WidthOHeightCaption!9TabOrderOnClickRightCheckClick  	TCheckBoxTopCheckLeft� Top=WidthUHeightCaption!10TabOrderOnClickTopCheckClick  	TCheckBox	LeftCheckLeft� TopWidthEHeightCaption!8TabOrderOnClickLeftCheckClick  	TCheckBoxBottomCheckLeft� TopTWidthSHeightCaption!11TabOrderOnClickBottomCheckClick  TSpinBtnSpinBtn1Left�Top2Height	Arrowkeys	Increment       ��?Max �P]���CMin �P]����	Validator	MarkValid  TEditDipEditLeftrTopWidthbHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderOnChangeDipEditChange  TEditMarkEditLefttTop2Width`HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderOnChangeMarkEditChange  TButtonFontBtnLeftTop3Width^HeightCaption!7Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderOnClickFontBtnClick  TSpinBtnSpinBtn3Left�TopHeight	Arrowkeys	Increment       ��?Max �P]���CMin �P]����	Validator	StepValid  TPanel	GridpanelLefttTopQWidthmHeightColorclBlackTabOrder
OnClickGridpanelClick  	TCheckBoxCheckBoxOutsideLeftTopPWidth� HeightCaption!29TabOrderVisible   
TWGroupBox
WGroupBox4Left Top� Width� HeightgCaption!16TabOrderBoxStyle	bsTopLine TLabelLabel3Left"Top3WidthHeightCaption!21  TLabelLabel5Left"TopNWidthHeightCaption!23  TWLabelWLabel2Left"TopWidthHeightCaption!20  TLabelAxLefttTopWidth7Height	AlignmenttaRightJustifyAutoSizeCaption0Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelMaLefttTop3Width7Height	AlignmenttaRightJustifyAutoSizeCaption0Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelCrLefttTopNWidth7Height	AlignmenttaRightJustifyAutoSizeCaption0Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TWindowWindow1Left\Top    TButtonButton1Left;TopWidthZHeightCaption!27ModalResultTabOrder  TButtonButton2Left�TopWidthSHeightCaption!28ModalResultTabOrder  TButtonLoadBtnLeftTopWidth^HeightCaption!25TabOrderOnClickLoadBtnClick  TButtonSaveBtnLeftqTopWidtheHeightCaption!26TabOrderOnClickSaveBtnClick  TColorDialogColorDialog1Ctl3D	LeftTop�   TMeasureLookupValidator
CrossValidEdit	CrossEditUseableUnitsCommasDefaultValue.UnitsmuCentimetersDefaultValue.Value       � @	MaxLengthMinValue.UnitsmuDrawingUnitsMaxValue.UnitsmuDrawingUnitsMaxValue.Value �P]���CUnitsmuDrawingUnitsLeft� Top�   TMeasureLookupValidator	MarkValidEditMarkEditUseableUnitsCommasDefaultValue.UnitsmuCentimetersDefaultValue.Value       ��?	MaxLengthMinValue.UnitsmuDrawingUnitsMaxValue.UnitsmuDrawingUnitsMaxValue.Value �P]���CUnitsmuDrawingUnitsLeft1Topc  TMeasureLookupValidator	StepValidEditDipEditUseableUnitsCommasDefaultValue.UnitsmuDrawingUnits	MaxLengthMinValue.UnitsmuDrawingUnitsMaxValue.UnitsmuDrawingUnitsMaxValue.Value �P]���CUnitsmuDrawingUnitsOnChangeUnits
StepChangeLeft5TopC  TMeasureValidatorDimValidUseableUnitsCommasMinValue.UnitsmuDrawingUnitsMinValue.Value �P]����MaxValue.UnitsmuDrawingUnitsMaxValue.Value �P]���CUnitsmuDrawingUnitsLeft�Top  TMlgSection
MlgSectionSectionAdvMarkingDlgLeft� Top   