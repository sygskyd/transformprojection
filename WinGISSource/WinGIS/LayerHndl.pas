{******************************************************************************+
  Unit LayerHndl
--------------------------------------------------------------------------------
  Autor: Martin Forst
--------------------------------------------------------------------------------
  Contains the menu-handler for the layer-priority-dialog, the function to
  select a layer and the function to create a new layer.
--------------------------------------------------------------------------------
  Release: 1
--------------------------------------------------------------------------------
+******************************************************************************}
Unit LayerHndl;

Interface
                                                                  
Uses AM_Layer,AM_Proj,MenuFn,ProjHndl;


Type // options for selecting layers
     TSelectLayerOption   = (sloLockedLayers,sloInvisibleLayers,sloNewLayer);
     TSelectLayerOptions  = Set of TSelectLayerOption;

Type TLayerManagerHandler = Class(TProjMenuHandler)
       Public
        Procedure  OnStart; override;
     end;

Function SelectLayerDialog(Project:PProj;Options:TSelectLayerOptions;HelpContext:Integer=0):PLayer;

Function NewLayerDialog(Project:PProj;Const Caption,InitialName:String):PLayer;

Function NewLayerDialog1(Project:PProj;Const Caption,InitialName:String):PLayer;

Implementation

{$R *.mfn}

Uses AM_Def,Controls,LayerPropertyDlg,SelectLayerDlg,UserIntf;

{===============================================================================
  TLayerManagerHandler
+==============================================================================}

Procedure TLayerManagerHandler.OnStart;
begin
  Project^.SetLayerPriority;
end;

{===============================================================================
  Other
+==============================================================================}

Function SelectLayerDialog(Project:PProj;Options:TSelectLayerOptions;HelpContext:Integer):PLayer;
{$IFNDEF AXDLL} // <----------------- AXDLL
var Dialog       : TSelectLayerDialog;
{$ENDIF} // <----------------- AXDLL
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  Result:=NIL;
  Dialog:=TSelectLayerDialog.Create(Project^.Parent);
  try
    Dialog.Project:=Project;
    Dialog.ShowNewLayerButton:=sloNewLayer in Options;
    Dialog.ShowLockedLayers:=sloLockedLayers in Options;
    Dialog.ShowInvisibleLayers:=sloInvisibleLayers in Options;
    Dialog.HelpContext:=HelpContext;
    if Dialog.ShowModal in [mrOK,mrYES] then
        Result:=Project^.Layers^.IndexToLayer(Dialog.SelectedLayer);
  finally
    Dialog.Free;
  end;
{$ENDIF} // <----------------- AXDLL
end;

Function NewLayerDialog(Project:PProj;Const Caption,InitialName:String):PLayer;
{$IFNDEF AXDLL} // <----------------- AXDLL
var Dialog       : TLayerPropertyDialog;
{$ENDIF} // <----------------- AXDLL
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  Result:=NIL;
  Dialog:=TLayerPropertyDialog.Create(Project^.Parent);
  try
    Dialog.Caption:=Caption;
    Dialog.Project:=Project;
    Dialog.ProjectStyles:=Project^.PInfo^.ProjStyles;
    Dialog.LayerStyle.Name:=InitialName;
    if Dialog.ShowModal=mrOK then begin
      Result:=Project^.Layers^.InsertLayer('',0,0,0,0,ilSecond,DefaultSymbolFill);
      Result^.SetLayerStyle(Project^.PInfo,Dialog.LayerStyle);
    end;
  finally
    Dialog.Free;
  end;
{$ENDIF} // <----------------- AXDLL
end;

Function NewLayerDialog1(Project:PProj;Const Caption,InitialName:String):PLayer;
{$IFNDEF AXDLL} // <----------------- AXDLL
var Dialog       : TLayerPropertyDialog;
    TestLayer    : PLayer;
{$ENDIF} // <----------------- AXDLL
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  Result:=NIL;
  Dialog:=TLayerPropertyDialog.Create(Project^.Parent);
  try
    Dialog.Caption:=Caption;
    Dialog.Project:=Project;
    Dialog.ProjectStyles:=Project^.PInfo^.ProjStyles;
    Dialog.LayerStyle.Name:=InitialName;
    if Dialog.ShowModal=mrOK then begin

      TestLayer:=Project^.Layers^.NameToLayer(Dialog.LayerStyle.Name);

      if TestLayer <> NIL then Result:=TestLayer
      else begin
        Result:=Project^.Layers^.InsertLayer('',0,0,0,0,ilSecond,DefaultSymbolFill);
        Result^.SetLayerStyle(Project^.PInfo,Dialog.LayerStyle);
      end;
    end;
  finally
    Dialog.Free;
  end;
{$ENDIF} // <----------------- AXDLL
end;

{===============================================================================
  Initialisation and termination-code
+==============================================================================}

Initialization
  MenuFunctions.RegisterFromResource(HInstance,'LayerHndl','LayerHndl');
  TLayerManagerHandler.Registrate('ViewLayerManager');

end.
 