UNIT ubtree;

INTERFACE
uses uclass, ubtreep, ualock;

CONST Size = 255;
TYPE Item  = record
       f: Longint;
       APtr  : TExtPtr;
     end;
     BNode = class;
     BLeaf = class;
     BTree = class
      public
       Root : BNode;
       Head : BLeaf;
       Constructor Create;
       Function Find (Key: Item): BLeaf;
       Procedure Insert (Key: Item);
       Procedure Delete (Key: Item);
       Destructor Destroy; override;
       procedure Print;
       function TreeCopy:BNode;
       function TreeCopyP:TTraversePtr;
       Procedure SetHead;
       function GetRoot:BNode;
       PRocedure PutRoot(ARoot:Bnode);
     end;

     BNode = class
       KeyNum : Integer;
       Parent : BNode;
       Keys   : Array[0..Size] of Item;
       Constructor Create;
       Procedure Insert (Key: Item; Pos: Integer; Node: BNode); virtual;
       Procedure Delete (Pos: Integer); virtual;
       Function IndexSeek (Key: Item): Integer;
       Destructor Destroy; override;
     end;

     BKnot = class(BNode)
       Sons : Array[0..Size+1] of BNode;
       Constructor Create;
       Procedure Insert (Key: Item; Pos: Integer; Node: BNode); override;
       Procedure Delete (Pos: Integer); override;
       Destructor Destroy; override;
     end;

     BLeaf = class(BNode)
       Prev : BLeaf;
       Next : BLeaf;
       Constructor Create (Node: BLeaf);
       Destructor Destroy; override;
     end;

IMPLEMENTATION

Function LeafSeek (Key: Item; Node: BNode): BNode;
  Begin
    LeafSeek:=Node;
    if Node is BKnot then with BKnot(Node)
    do LeafSeek:=LeafSeek(Key,Sons[IndexSeek(Key)]);
  End;

Constructor BTree.Create;
  Begin
    inherited Create;
    Head:=BLeaf.Create(Nil);
    Root:=Head;
  End;

Function BTree.Find (Key: Item): BLeaf;
  Var Leaf : BLeaf;
      ind  : integer;
  Begin
    Find:=Nil;
    Leaf:=BLeaf(LeafSeek(Key,Root));
    if (Leaf<>Nil) then begin
      ind:=Leaf.IndexSeek(Key);
      if (ind>0) and (Leaf.Keys[ind-1].f=Key.f) then Find:=Leaf;
    end;
  End;

Procedure BTree.Insert (Key: Item);
  Const Half = (Size+1) div 2;
  Var Node,Nod2 : BNode;
      ind,i     : integer;
  Begin
    Node:=LeafSeek(Key,Root);
    while TRUE do begin
      ind:=Node.IndexSeek(Key);
      if (ind>0) and (Node.Keys[ind-1].f=Key.f) then break;
      Node.Insert(Key,ind,Nod2);
      if Node.KeyNum<=Size then break;
      if Node is BKnot then Nod2:=BKnot.Create
      else Nod2:=BLeaf.Create(BLeaf(Node));
      Node.KeyNum:=Size-Half+1;
      Nod2.KeyNum:=Half;
      move(Node.Keys[Node.KeyNum],Nod2.Keys[0],Half*sizeof(Item));
      if Nod2 is BKnot then with BKnot(Nod2) do begin
        move(BKnot(Node).Sons[Node.KeyNum],Sons[0],(Half+1)*sizeof(BNode));
        for i:=0 to Half do Sons[i].Parent:=Nod2;
        dec(Node.KeyNum);
        Key:=Node.Keys[Node.KeyNum];
      end else Key:=Nod2.Keys[0];
      if Node=Root then begin
        Root:=BKnot.Create;
        Root.KeyNum:=1;
        Root.Keys[0]:=Key;
        BKnot(Root).Sons[0]:=Node;
        BKnot(Root).Sons[1]:=Nod2;
        Node.Parent:=Root;
        Nod2.Parent:=Root;
        break;
      end;
      Node:=Node.Parent;
    end;
  End;

Procedure BTree.Delete (Key: Item);
  Const Half = (Size) div 2;
  Var Node,Nod2 : BNode;
      Par       : BKnot;
      ind,i     : integer;
  Begin
    Node:=Find(Key);
    if Node<>Nil then while TRUE do begin
      ind:=Node.IndexSeek(Key);
      Node.Delete(ind);
      if (Node.KeyNum>=Half) or (Node=Root) then break;
      Par:=BKnot(Node.Parent);
      ind:=Par.Indexseek(Key);
      if (ind>0) then begin
        Nod2:=Par.Sons[ind-1];
        if Nod2.KeyNum>Half then with Nod2 do begin
          if Node is BLeaf then Node.Insert(Keys[KeyNum-1],0,Nil)
          else with Nod2 as BKnot
          do Node.Insert(Par.Keys[ind-1],-1,Sons[KeyNum]);
          Par.Keys[ind-1]:=Keys[KeyNum-1];
          Delete(KeyNum);
          break;
        end;
      end;
      if (ind<Par.KeyNum) then begin
        Nod2:=Par.Sons[ind+1];
        if Nod2.KeyNum>Half then with Nod2 do begin
          if Node is BLeaf then begin
            Node.Insert(Keys[0],Node.KeyNum,Nil);
            Par.Keys[ind]:=Keys[1];
          end else with Nod2 as BKnot do begin
            Node.Insert(Par.Keys[ind],Node.KeyNum,Sons[0]);
            Par.Keys[ind]:=Keys[0];
          end;
          Delete(0);
          break;
        end;
      end;
      if (ind=Par.KeyNum) then begin
        Node:=Nod2;
        Nod2:=Par.Sons[ind];
        dec(ind);
      end;
      if Node is BKnot then with BKnot(Node) do begin
        Insert(Par.Keys[ind],KeyNum,BKnot(Nod2).Sons[0]);
        move(BKnot(Nod2).Sons[1],Sons[KeyNum+1],Nod2.KeyNum*sizeof(BNode));
        for i:=1 to Nod2.KeyNum do Sons[i+KeyNum].Parent:=Node;
      end;
      move(Nod2.Keys[0],Node.Keys[Node.KeyNum],Nod2.KeyNum*sizeof(Item));
      Node.KeyNum:=Node.KeyNum+Nod2.KeyNum;
      if Node is BLeaf then with BLeaf(Node) do begin
        Next:=BLeaf(Nod2).Next;
        if Next<>Nil then Next.Prev:=BLeaf(Node);
      end;
      Nod2.Destroy;
      Key:=Par.Keys[ind];
      if (Par=Root) and (Par.KeyNum=1) then begin
        Root.Destroy;
        Root:=Node;
        break;
      end;
      Node:=Par;
    end;
  End;

Destructor BTree.Destroy;
  procedure kill(Node: BNode);
    var i : integer;
    begin
      if Node is BKnot then
      for i:=0 to Node.KeyNum do kill(BKnot(Node).Sons[i]);
      Node.Destroy;
    end;
  Begin
    kill(Root);
    inherited Destroy;
  End;

Constructor BNode.Create;
  Begin
    inherited Create;
    KeyNum:=0;
  End;

Procedure BNode.Insert (Key: Item; Pos: Integer; Node: BNode);
  Begin
    if Pos<0 then Pos:=0;
    move(Keys[Pos],Keys[Pos+1],(KeyNum-Pos)*sizeof(Item));
    Keys[Pos]:=Key;
    inc(KeyNum);
  End;

Procedure BNode.Delete (Pos: Integer);
  Begin
    if Pos<1 then Pos:=1;
    move(Keys[Pos],Keys[Pos-1],(KeyNum-Pos)*sizeof(Item));
    dec(KeyNum);
  End;

Function BNode.IndexSeek (Key: Item): Integer;
  function binseek(i,j: integer; Key: Item): integer;
    var m : integer;
    begin
      while i<>j do begin
        m:=(i+j) div 2; 
        if Keys[m].f<=Key.f then i:=m+1 else j:=m;
      end;
      binseek:=i;
    end;
  Begin
    IndexSeek:=binseek(0,KeyNum,Key);
  End;

Destructor BNode.Destroy;
  Begin
    inherited Destroy;
  End;

Constructor BKnot.Create;
  Begin
    inherited Create;
  End;

Procedure BKnot.Insert (Key: Item; Pos: Integer; Node: BNode);
  Begin
    inherited Insert(Key,Pos,Node);
    move(Sons[Pos+1],Sons[Pos+2],(KeyNum-Pos-1)*sizeof(BNode));
    Sons[Pos+1]:=Node;
    Node.Parent:=self;
  End;

Procedure BKnot.Delete (Pos: Integer);
  Begin
    inherited Delete(Pos);
    move(Sons[Pos+1],Sons[Pos],(KeyNum-Pos+1)*sizeof(BNode));
  End;

Destructor BKnot.Destroy;
  Begin
    inherited Destroy;
  End;

Constructor BLeaf.Create (Node: BLeaf);
  Begin
    inherited Create;
    if Node<>Nil then begin
      Next:=Node.Next;
      if Next<>Nil then Next.Prev:=self;
      Prev:=Node;
      Node.Next:=self;
    end;
  End;

Destructor BLeaf.Destroy;
  Begin
    inherited Destroy;
  End;

function Btree.TreeCopy:BNode;
  var NewRoot  : BNode;
      PrevLeaf : BLeaf;
  Procedure ConnectLeaves(p:BNode);
    var      i : integer;
    begin
      if p is BKnot then begin
        for i:=0 to p.keynum do ConnectLeaves(BKnot(p).sons[i]);
      end else begin
        BLeaf(p).next:=NIL;
        Bleaf(p).prev:=PrevLeaf;
        if PrevLeaf<>nil then BLeaf(BLeaf(p).prev).next:=BLeaf(p);
        PrevLeaf:=BLeaf(p);
      end;
    end;
  procedure Traverse(var p:BNode;var q:BNode;AParent:BNOde);
    var i : integer;
        h : BNode;
    begin
      if p is BKnot then begin
        q:=BKnot.Create;
        q.Parent:=AParent;
        q.keynum:=p.keynum;
        System.Move(p.Keys[0],q.Keys[0],size*sizeof(Item));
        {InitKnot}
        for i:= 0 to p.keynum do begin
          h:=BKnot(p).sons[i];
          Traverse(h,BKnot(q).sons[i],q);
        end;
      end else begin
        q:=BLeaf.Create(NIL);
        q.Parent:=AParent;
        q.keynum:=p.Keynum;
        System.Move(p.Keys[0],q.Keys[0],size*sizeof(Item));
        {INitLeaf}
      end;
    end;
  begin
    NewRoot:=NIL;
    PrevLeaf:=NIL;
    Traverse(Root,NewRoot,NIL);
    ConnectLeaves(NewRoot);
    Result:=newRoot;
  end;
(*
 function Btree.TreeCopyP:BPNode;
  var NewRoot  : BPNode;
      PrevLeaf : BPLeaf;

  Procedure ConnectLeaves(p:BPNode);
    var      i : integer;
    begin
      if p is BPKnot then begin
        for i:=0 to p.keynum do ConnectLeaves(BPKnot(p).sons[i]);
      end else begin
        BPLeaf(p).next:=NIL;
        BPleaf(p).prev:=PrevLeaf;
        if PrevLeaf<>nil then BPLeaf(BPLeaf(p).prev).next:=BPLeaf(p);
        PrevLeaf:=BPLeaf(p);
      end;
    end;
  procedure Traverse(var p:BNode;var q:BPNode;AParent:BPNode);
    var i : integer;
        h : BNode;
    begin
      if p is BKnot then begin
        q:=BPKnot.Create;
        q.Parent:=AParent;
        q.keynum:=p.keynum;
        System.Move(p.Keys[0],q.Keys[0],size*sizeof(Item));
        {InitKnot}
        for i:= 0 to p.keynum do begin
          h:=BKnot(p).sons[i];
          Traverse(h,BPKnot(q).sons[i],q);
        end;
      end else begin
        q:=BPLeaf.Create(NIL);
        q.Parent:=AParent;
        q.keynum:=p.Keynum;
        System.Move(p.Keys[0],q.Keys[0],size*sizeof(Item));
        {INitLeaf}
      end;
    end;
  begin
    NewRoot:=NIL;
    PrevLeaf:=NIL;
    Traverse(Root,NewRoot,NIL);
    ConnectLeaves(NewRoot);
    Result:=newRoot;
  end;
*)

function Btree.TreeCopyP:TTraversePtr;
  var {NewRoot  : BPNode;}
      NewRoot  : TTraversePtr;
      {PrevLeaf : BPLeaf;}
      PrevLeaf : TTraversePtr;

  Procedure ConnectLeaves(pt:TTraversePtr);
    var      i : integer;
             p : BPNode;
             pt1 : TTraversePtr;

             ph  : BPLeaf;
    begin
      p:=pt.Item;
      if p is BPKnot then begin
        for i:=0 to p.keynum do begin
          pt1:=TTraversePtr.create;
          pt1.Item:=BPKnot(p).sons[i];
          ConnectLeaves(pt1);
          pt1.free;
        end;
      end else begin
        BPLeaf(p).next:=NIL;
        BPleaf(p).prev:=PrevLeaf.Item;
        if PrevLeaf.Item<>nil then begin
          BPLeaf(BPLeaf(p).prev).next:=BPLeaf(p);
        end;
        PrevLeaf.Item:=BPLeaf(p);
      end;
    end;

  procedure Traverse(var p:BNode;var q:TTraversePtr;AParent:BPNode);
    var i : integer;
        h : BNode;
        pt1 : TTraversePtr;
    begin
      if p is BKnot then begin
        q.Item:=BPKnot.Create;
        {BPKnot(q.Item).Parent:=AParent;}
        BPKnot(q.Item).keynum:=p.keynum;
        System.Move(p.Keys[0],BPKnot(q.Item).Keys[0],size*sizeof(Item));
        {InitKnot}
        for i:= 0 to p.keynum do begin
          h:=BKnot(p).sons[i];
          pt1:=TTraversePtr.create;
          {pt1.Item:=BPKnot(q.Item).sons[i];}
          Traverse(h,pt1,q.Item);
          BPKnot(q.Item).sons[i]:=pt1.Item;
          pt1.free;
        end;
      end else begin
        q.Item:=BPLeaf.Create(NIL);
        {BPLeaf(q.Item).Parent:=AParent;}
        BPLeaf(q.Item).keynum:=p.Keynum;
        System.Move(p.Keys[0],BPLeaf(q.Item).Keys[0],size*sizeof(Item));
        {INitLeaf}
      end;
    end;
  begin
    NewRoot:=TTraversePtr.create;
    PrevLeaf:=TTraversePtr.create;
    NewRoot.Item:=Nil;
    PrevLeaf.Item:=NIL;
    Traverse(Root,NewRoot,NIL);
    ConnectLeaves(NewRoot);
    PrevLeaf.free;
    Result:=newRoot;
    {NewRoot.free;}
  end;
Procedure Btree.print;
 var p           : BLeaf;
      ATTItem       :Item;
      i           : integer;
      f           : textfile;
  begin
  assignfile(f,'d:\test.dat');
    rewrite(f);
    p:=head;
    while p<>NIL do begin

      for i:=0 to p.KeyNum-1 do begin
        ATTItem:=p.Keys[i];
        if Attitem.f<>0 then writeln(f,ATTItem.f);
      end;


      p:=p.next;
    end;
    closefile(f);
  end;

function Btree.getroot:BNode;
  begin
    Result:=Root;
  end;

PRocedure Btree.putRoot(ARoot:BNode);
  begin
    Root:=ARoot;
  end;

PRocedure Btree.SetHead;
  var p : BNode;
  begin
    p:=Root;
    while p is BKnot do begin
      p:=BKnot(p).Sons[0];
    end;
    Head:=BLeaf(p);
  end;

END.
