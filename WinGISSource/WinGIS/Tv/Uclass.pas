unit Uclass;
{Clfs.asm}
interface
uses SysUtils,Uheap,Utils,Classes,Wintypes,ualock;

const offs = 0;
      segm = 2;

Type
     PPtr       = ^TPtr;
     TPtr       = record
       Ptr      : Pointer;        {Ptr=NIL und Offset=0 -> Pointer = NULL; PageNum=0 ist gueltiger Wert}
       PageNum  : word;        {max 2GByte; sonst lonint-> ExtList muss dann vom Type TLongList sein }
       Offset   : word;           {Ptr=-1, Offset>=28,PageNum>=0 : Page ausgelagert}
     end;

     TMainClass = class;
     PExtPtr = ^TExtPtr;
     TExtPtr = Object             {Ab jetzt Schnittstelle fuer externe Pointer!!!}
       {PTr     : Pointer;}
       Pn      : Word;
       Offset  : Word;
       Function Get:Pointer;
       Function GetAuto:TTraversePtr;
       PRocedure Put(Item:Pointer);
        PRocedure PutToVar(Item:Pointer);   {Put to lokal Variable  -> not mdified}
       Procedure PutMemPtr(Item:Pointer);
       Function GetMemPtr:POinter;
       Function GetPn:Word;
       Function GetOfs:Word;
     end;
                    (*
      PExtPtr2 = ^TExtPtr2;
     TExtPtr2 = Object             {Ab jetzt Schnittstelle fuer externe Pointer!!!}
       {PTr     : Pointer;}
       Pn      : Word;
       Offset  : Word;
       Function Get:Pointer;
       PRocedure Put(Item:Pointer);
       Procedure PutMemPtr(Item:Pointer);
       Function GetMemPtr:POinter;
       Function GetPn:Word;
       Function GetOfs:Word;
     end;
                      *)
     TIncPtr     = record
       PN     : word;
       Offset : word;
     end;

     TMainClass = Class
       FType:word;
    
       class Function NewInstance:TObject; override;
       procedure      FreeInstance; override;

       function       ReadPointer(FPtr:Pointer):Pointer;
       procedure      WritePointer(FPtr:Pointer;APtr:Pointer);
       Function       SwappedOut(APtr:PPtr):Boolean;

       function       ReadExtPointer(FPtr:Pointer):Pointer;
      { function       ReadExtPointer2(FPtr:Pointer):Pointer;}
       procedure      WriteExtPointer(FPtr:Pointer;APtr:Pointer);
        procedure      WriteExtPointerWithoutSetModified(FPtr:Pointer;APtr:Pointer);
       procedure      AddPointers; virtual; abstract;
       procedure      DelPointers; virtual; abstract;
       function       MakePointer(PageNum,Offset:Word):Pointer;
       Function       GetPageNum(AAdress:POinter):Longint;
       function       ExtPointerValid(Pn,Offset:Word):Boolean;
       Procedure      LockPage(AAdress:Pointer);
       Procedure      SavePage(AAdress:Pointer);
       Procedure      UnLockPage(AAdress:Pointer);
       Procedure      UnLockAll;
       Function       SwappedOutExt(APtr:PExtPtr):Boolean;
       Procedure      Modified(AAdress:Pointer);
       function       getLockCount:byte;
       Function       islocked:Boolean;
     end;


     TMainClass2 = class(TMainClass)
       class Function NewInstance:TObject; override;
       procedure      FreeInstance; override;    
     end;


     TExtPtrLst = class
      public
       Count     : longint;
       Items     : array[0..16000] of TExtptr;
       Procedure Add(AItem : TExtPtr);
       Procedure Clear;
     end;

implementation

uses Upage,FrameTab,Process,wlist;


{Wird in Newinstance aufgerufen}
{gleich wie Modified (laesst sich in newinstance nicht aufrufen)}
{Objekte werden beim create Aufruf auch schon als modified automatisch gekennzeichnet}
procedure SetModified(AAdress:Pointer);
   var  aSel       : Word;
        APage      : PPage;
        FrameTable : TFrameTable;
        h          : pointer;
  begin
    if not GetHeapman.ReadOnly then begin
      if AAdress<>NIL then begin
       aSel:=GetSelector(aAdress);
       APage:=MakeLongPointer(0,ASel);
       h:=getHeapMan.FKernel;
       if h<> nil then begin
         FrameTable:=TKernel(GetHeapMan.FKernel).FrameManager;
         FrameTable.FrameTable^[APage^.FrameId].Modified:=1;
         GetHeapMan.ProjModified:=true;
       end;
      end;
    end;
  end;

Class Function TMainClass.NewInstance
   : TObject;
  var hPage : PPage;
      p     : pointer;
      size  : word;
      p_ofs : word;
      p_seg : word;
      hofs  : word;
      hseg  : word;
      h2    : word;
  begin
    p:=GetHeapMan.MAllocate(GetHeapMan.ActualPage,InstanceSize);
    Result:=p;
    if p<> nil then begin
      SetModified(p);
      InitInstance(p);
      if GEtHeapMan.ActualPage^.VmtPointerList<>NIL then GetHeapMan.AddVmtPtr(p);
      h2:= GetHeapMan.GetFtype(Longint(self));
      if h2<>$FFFF then begin
        PWord(longint(p)+4)^:=h2;
      end;
    end;
  (*
    {p:=GetHeapMan.MAllocate(GetHeapMan.ActualPage,InstanceSize);}
    p:=MemAllocate(InstanceSize);
    if p<>NIL then begin
      SetModified(p);
      {FillChar(PChar(p)^,InstanceSize,#0);}
      if GEtHeapMan.ActualPage^.VmtPointerList<>NIL then GetHeapMan.AddVmtPtr(p);
      size:=InstanceSize;
      p_ofs:=GetOffset(p);
      p_seg:=GetSelector(p);
      asm
        MOV     AX,[BP+6].offs
        MOV     hofs,AX
        MOV	AX,[BP+6].segm
        MOV     hseg,AX
      end;
      h2:= GetHeapMan.GetFtype(hofs,hseg);
      asm
          MOV     CX,Size
          MOV	BX,p_ofs
          MOV	DI,p_ofs
          MOV	ES,p_seg
          CLD
          MOV	AX,[BP+6].offs
          STOSW
          MOV	AX,[BP+6].segm
          STOSW
          XOR	AX,AX
          SUB	CX,4
          SHR	CX,1
          REP	STOSW
          ADC	CX,AX
          REP	STOSB
          MOV	AX,BX
      end;

      if h2<>$FFFF then begin
        PWord(longint(p)+4)^:=h2;
      end;

    end;
    Result:=p;
    *)
  end;


Procedure TMainClass.FreeInstance;
  begin
    if Self<>GetHeapMan.NullPointer then GetHeapMan.DeAllocate(Self,InstanceSize);
  end;

Procedure TMainClass2.FreeInstance;
  begin
    if Self<>GetHeapMan.NullPointer then GetHeapMan.DeAllocate(Self,InstanceSize);
  end;


function TMainClass.ReadPointer
   (
   FPtr   : Pointer
   )
   : Pointer;
  var helpPtr    : PPtr;
      Info       : word;
      SelLockPage   : word;
      LockPageIndex : Longint;
      LockPage   : Pointer;
      a : word;
      pt : pointer;
      b  : boolean;

  begin
    helpPtr:=PPtr(FPtr);
     if helpPtr^.pageNum>20 then begin
        a:=0;
      end;
    if SwappedOut(FPtr) then begin

      LockPage:=MakeLongPointer(0,GetSelector(FPtr));
      LockPageIndex:=PPage(LockPage)^.PageNumber;
      If helpPtr^.PageNum > TProcess(GetHeapMan.ActualProcess).PageTable.count then
        raise ElistError.create(' TPtr ung�ltig ');

      TProcess(GetHeapMan.ActualProcess).SwapIn(helpPtr^.PageNum,{LockPageIndex}Longint(LockPage));

      PPtr(FPtr)^.Ptr:=MakePointer(HelpPtr^.PageNum,HelpPtr^.Offset);
      Result:=helpPtr^.Ptr;
      GetHeapMan.FromPage:=Lockpage;
      GetHeapMan.ToPage:=result;

    end else begin

    Result:=MakePointer(HelpPtr^.PageNum,HelpPtr^.Offset);

    end;
  end;

Procedure TMainClass.WritePointer
   (
   FPtr   : Pointer;          {Adresse Basis}  {ZB. @FPointerA von TPolylistItem - zeigt auf GraphItem}
   APtr   : Pointer           {Inhalt  zb. GraphItem}
   );
  var helpPtr   : PPtr;
      FrameNum  : Word;
      SelAPtr   : Word;
      SelFPtr   : Word;
      Incptr       : TIncPtr;
      PointToPage  : PPage;
  begin
    helpPtr:=PPtr(FPtr);
    helpPtr^.Ptr:=APtr;

    SelAPtr:=GetSelector(APtr);
    SelFPtr:=GetSelector(FPtr);
    if SelAPtr=0 then begin
      helpPtr^.Offset:=0;
      helpPtr^.PageNum:=0;
      HelpPtr^.Ptr:=NIL;
    end
    else begin
      FrameNum:=GetHeapMan.SeekFrameNum(SelAPtr);
      HelpPtr^.PageNum:=PFrameArray(GetHeapMan.FrameTable)^[FrameNum].PageIndex;
     { PFrameArray(GetHeapMan.FrameTable)^[FrameNum].Modified:=1;                {new}
      if helpPtr^.PageNum = -1 then raise ElistError.Create('Page nicht eingelagert ');
      helpPtr^.Offset:=GetOffset(APtr);

      {PointToPage:=MakeLongPointer(0,SelAptr);}

      FrameNum:=GetHeapMan.SeekFrameNum(SelFPtr);
      PFrameArray(GetHeapMan.FrameTable)^[FrameNum].Modified:=1;
    end;
  end;

function TMainClass.ReadExtPointer
   (
   FPtr   : Pointer
   )
   : Pointer;
  var helpPtr    : PExtPtr;
      Info       : word;
      SelLockPage   : word;
      LockPageIndex : Longint;
      LockPage   : Pointer;
      a : word;
      pt : pointer;
      b  : boolean;
      fault : boolean;

  begin
    fault:=false;
    helpPtr:=PExtPtr(FPtr);
    if SwappedOutext(FPtr) then begin

      //LockPage:=MakeLongPointer(0,GetSelector(FPtr));
      //LockPageIndex:=PPage(LockPage)^.PageNumber;


        If helpPtr^.PN > TProcess(GetHeapMan.ActualProcess).PageTable.count then begin
          raise ElistError.create(' TExtPtr ung�ltig ');
        end;

        TProcess(GetHeapMan.ActualProcess).SwapIn(helpPtr^.PN,0);

        {PExtPtr(FPtr)^.Ptr:=MakePointer(HelpPtr^.PageNum,HelpPtr^.Offset);
        Result:=helpPtr^.Ptr;}
        Result:=MakePointer(HelpPtr^.Pn,HelpPtr^.Offset);
        GetHeapMan.FromPage:=Lockpage;
        GetHeapMan.ToPage:=result;

    end else begin

    Result:=MakePointer(HelpPtr^.PN,HelpPtr^.Offset);

    end;
  end;

Procedure TMainClass.WriteExtPointer
   (
   FPtr   : Pointer;          {Adresse Basis}  {ZB. @FPointerA von TPolylistItem - zeigt auf GraphItem}
   APtr   : Pointer           {Inhalt  zb. GraphItem}
   );
  var helpPtr   : PExtPtr;
      FrameNum  : Word;
      SelAPtr   : Word;
      SelFPtr   : Word;
      Incptr       : TIncPtr;
      PointToPage  : PPage;
  begin
    helpPtr:=PExtPtr(FPtr);
    {helpPtr^.Ptr:=APtr;}

    SelAPtr:=GetSelector(APtr);
    SelFPtr:=GetSelector(FPtr);
    if SelAPtr=0 then begin
      helpPtr^.Offset:=0;
      helpPtr^.PN:=0;
     { HelpPtr^.Ptr:=NIL;}
    end
    else begin
      FrameNum:=GetHeapMan.SeekFrameNum(SelAPtr);
      HelpPtr^.PN:=PFrameArray(GetHeapMan.FrameTable)^[FrameNum].PageIndex;
     { PFrameArray(GetHeapMan.FrameTable)^[FrameNum].Modified:=1;                {new}
      if helpPtr^.PN = -1 then raise ElistError.Create('Page nicht eingelagert ');
      helpPtr^.Offset:=GetOffset(APtr);

      {PointToPage:=MakeLongPointer(0,SelAptr);}

      FrameNum:=GetHeapMan.SeekFrameNum(SelFPtr);
      PFrameArray(GetHeapMan.FrameTable)^[FrameNum].Modified:=1;
    end;
  end;


{return true wenn externer Pointer zeigt auf eingelagerte Seite, sonst false}
Function TMainClass.ExtPointerValid(Pn,Offset:word):Boolean;
 var PageTable  : TPageTable;
      FrameTable : TFrameTable;
      FrameIndex:Longint;
  begin
    Result:=false;
    Pagetable:=TProcess(GetHeapMan.ActualProcess).PageTable;
    FrameTable:=TKernel(GetHeapMan.FKernel).FrameManager;
    FrameIndex:= PageTable[PN];
    if FrameIndex<>-1 then Result:=true;
  end;

{retourniert die Adresse des externen Pointers, ist die Seite ausgelagert so ist der Rueckgabewert NIL(oder -1)?}
Function TMainClass.MakePointer
   (
   PageNum : Word;
   Offset  : Word
   )
   :Pointer;
  var PageTable  : TPageTable;
      FrameTable : TFrameTable;
      PointerToUpdate : Pointer;
      FrameIndex:Longint;
      PageToUpdate:word;
  begin
    if Offset<>0 then begin
      Result:=NIL;
      Pagetable:=TProcess(GetHeapMan.ActualProcess).PageTable;
      FrameTable:=TKernel(GetHeapMan.FKernel).FrameManager;
      FrameIndex:= PageTable[PageNum];
      {if FrameIndex=-1 then raise ElistError.create('Seite ausgelagert');}
      if FrameIndex<>-1 then begin
        {PageToUpdate:=(FrameTable.Frametable^[FrameIndex].Handle;}
        PageToUpdate:=GetSelector(FrameTable.Frametable^[FrameIndex].Address);
        if PageToUpdate<>0 then begin
          {PointerToUpdate:=MakeLongPointer(Offset,PageToUpdate);}
          {Result:=PPtr(PointerToUpdate)^.Ptr;}
          Result:=MakeLongPointer(Offset,PageToUpdate);
        end;     {else Result=NIL}
      end;
    end else Result:=NIL;
  end;

  Function TMainClass.GetPageNUm(AAdress:Pointer):Longint;
    var aSel       : Word;
        APage      : PPage;
        FrameTable : TFrameTable;
    begin
      aSel:=GetSelector(aAdress);
      APage:=MakeLongPointer(0,ASel);
      FrameTable:=TKernel(GetHeapMan.FKernel).FrameManager;
      REsult:=FrameTable.FrameTable^[APage^.FrameId].PageIndex;
    end;

 Procedure TMainClass.LockPage(AAdress:Pointer);
    var aSel       : Word;
        APage      : PPage;
        FrameTable : TFrameTable;
   begin
     if AAdress<>NIL then begin
     aSel:=GetSelector(aAdress);
     APage:=MakeLongPointer(0,ASel);
     FrameTable:=TKernel(GetHeapMan.FKernel).FrameManager;
     {if FrameTable.FrameTable^[APage^.FrameId].Locked=0 then Inc(GetHeapMan.Lockcount);}
     FrameTable.FrameTable^[APage^.FrameId].Locked:=1;
     inc(FrameTable.FrameTable^[APage^.FrameId].LockCount);
     if FrameTable.FrameTable^[APage^.FrameId].LockCount > 65000 then begin
       {FrameTable.FrameTable^[APage^.FrameId].LockCount:=1;}
       raise Elisterror.create('lock count');
     end;
     inc(GetHeapMan.CountLockedPages);
     end;
   end;


 Procedure TMainClass.UnLockPage(AAdress:Pointer);
    var aSel       : Word;
        APage      : PPage;
        FrameTable : TFrameTable;
   begin
     if AADress<>NIL then begin
       aSel:=GetSelector(aAdress);
       APage:=MakeLongPointer(0,ASel);
       FrameTable:=TKernel(GetHeapMan.FKernel).FrameManager;
       {if FrameTable.FrameTable^[APage^.FrameId].Locked=1 then dec(GetHeapMan.Lockcount);}

       if FrameTable.FrameTable^[APage^.FrameId].LockCount=1 then
         FrameTable.FrameTable^[APage^.FrameId].Locked:=0;

       if FrameTable.FrameTable^[APage^.FrameId].LockCount>0 then
         dec(FrameTable.FrameTable^[APage^.FrameId].LockCount);

       dec(GetHeapMan.CountLockedPages);
     end;
   end;



function TMainclass.getLockCount:byte;
 var aSel       : Word;
        APage      : PPage;
        FrameTable : TFrameTable;
        AAdress    : Pointer;
  begin
     AAdress:=Self;
     if AAdress<>NIL then begin
     aSel:=GetSelector(aAdress);
     APage:=MakeLongPointer(0,ASel);
     FrameTable:=TKernel(GetHeapMan.FKernel).FrameManager;
    Result:=FrameTable.FrameTable^[APage^.FrameId].LockCount;
    end;
  end;

Function TMainClass.islocked:Boolean;
 var aSel       : Word;
        APage      : PPage;
        FrameTable : TFrameTable;
        AAdress    : POinter;
  begin
     AAdress:=Self;
     if AAdress<>NIL then begin
     aSel:=GetSelector(aAdress);
     APage:=MakeLongPointer(0,ASel);
     FrameTable:=TKernel(GetHeapMan.FKernel).FrameManager;
    if FrameTable.FrameTable^[APage^.FrameId].Locked=0 then  Result:=false else
    Result:=true;
    end;
  end;




PRocedure TMainClass.UnlockAll;
  var cnt        : longint;
      FrameTable : TFrameTable;
      i          : longint;
  begin
    FrameTable:=TKernel(GetHeapMan.FKernel).FrameManager;
    for cnt:=0 to FrameTAble.Capacity-1 do begin
        FrameTable.FrameTable^[cnt].Locked:=0;
        FrameTable.FrameTable^[cnt].LockCount:=0;
    end;
    GetHeapMan.CountLockedPages:=0;
  end;


Function TMainClass.SwappedOut(APtr:PPtr):Boolean;
  begin
    Result:=false;
    if TProcess(GetHeapMan.ActualProcess).PageTAble[APtr^.PageNum]=-1 then Result:=True;
  end;


Function TMainClass.SwappedOutExt(APtr:PExtPtr):Boolean;
  begin
    Result:=false;
    if TProcess(GetHeapMan.ActualProcess).PageTAble[APtr^.PN]=-1 then Result:=True;
  end;

Procedure TMainClass.SavePage(AAdress:Pointer);
   var aSel       : Word;
        APage      : PPage;
        FrameTable : TFrameTable;
        Aindex     : Longint;
        APageNum   : longint;
   begin
     if AADress<>NIL then begin
     aSel:=GetSelector(aAdress);
     APage:=MakeLongPointer(0,ASel);
     FrameTable:=TKernel(GetHeapMan.FKernel).FrameManager;
     APageNum:=FrameTable.FrameTable^[APage^.FrameId].PageIndex;
     AIndex:=TProcess(GetHeapMan.ActualProcess).AddrTable[APageNum];
     if AIndex=-1 then raise ElistError.Create('Fehler bei swap in; diese Seite wurde noch nie ausgelagert!');

     {APage:=FrameTable.FrameTable^[APage^.FrameId].Address;}
     TProcess(GetHeapMan.ActualProcess).FileMan.WritePosition(AIndex,Pointer(APage));
     end;
   end;

Procedure TMainClass.Modified(AAdress:Pointer);
   var  aSel       : Word;
        APage      : PPage;
        FrameTable : TFrameTable;
  begin
    if not GetHeapman.ReadOnly then begin
      if AAdress<>NIL then begin
       aSel:=GetSelector(aAdress);
       APage:=MakeLongPointer(0,ASel);
       FrameTable:=TKernel(GetHeapMan.FKernel).FrameManager;
       FrameTable.FrameTable^[APage^.FrameId].Modified:=1;
       GetHeapMan.ProjModified:=true;
      end;
    end;
  end;

Function TExtPtr.Get:Pointer;
  begin
    Result:=Pointer(TKernel(GetHeapMan.FKernel).MClass.ReadExtPointer(@Self));
  end;

Procedure TExtPtr.Put
   (
   Item   : Pointer
   );
   var  aSel       : Word;
        APage      : PPage;
        FrameTable : TFrameTable;
  begin
    TKernel(GetHeapMan.FKernel).MClass.WriteExtPointer(@Self,Item);

      if not GetHeapman.ReadOnly then begin
       aSel:=GetSelector(@Self);
       APage:=MakeLongPointer(0,ASel);
       FrameTable:=TKernel(GetHeapMan.FKernel).FrameManager;
       FrameTable.FrameTable^[APage^.FrameId].Modified:=1;
       GetHeapMan.ProjModified:=true;
    end;
  end;

Function TExtPtr.GetPn:Word;
  begin
    Result:=Pn;
  end;

Function TExtPtr.GetOfs:Word;
  begin
    Result:=Offset;
  end;


Procedure TExtPtr.PutMemPtr(Item:Pointer);
  begin
    Offset:=GetOffset(Item);
    Pn:=GetSelector(Item);
  end;


Function TExtPtr.GetMemPtr:POinter;
  begin
    Result:=MakeLongPointer(offset,Pn);
  end;


{==============================================================================}
(*
function TMainClass.ReadExtPointer2
   (
   FPtr   : Pointer
   )
   : Pointer;
  var helpPtr    : PExtPtr2;
      Info       : word;
      SelLockPage   : word;
      LockPageIndex : Longint;
      LockPage   : Pointer;
      a : word;
      pt : pointer;
      b  : boolean;

  begin
    helpPtr:=Pextptr2(FPtr);
    if SwappedOutext(FPtr) then begin

      LockPage:=MakeLongPointer(0,GetSelector(FPtr));
      LockPageIndex:=PPage(LockPage)^.PageNumber;

        If helpPtr^.PN > TProcess(GetHeapMan.ActualProcess).PageTable.count then
        raise ElistError.create(' Textptr2 ung�ltig ');

      TProcess(GetHeapMan.ActualProcess).SwapIn(helpPtr^.PN,{LockPageIndex}Longint(LockPage));

      {PextPtr2(FPtr)^.Ptr:=MakePointer(HelpPtr^.PageNum,HelpPtr^.Offset);
      Result:=helpPtr^.Ptr;}
      Result:=MakePointer(HelpPtr^.Pn,HelpPtr^.Offset);
      GetHeapMan.FromPage:=Lockpage;
      GetHeapMan.ToPage:=result;

    end else begin

    Result:=MakePointer(HelpPtr^.PN,HelpPtr^.Offset);

    end;
  end;

Function TextPtr2.Get:Pointer;
  begin
    Result:=Pointer(TKernel(GetHeapMan.FKernel).MClass.ReadExtPointer2(@Self));
  end;

Procedure TextPtr2.Put
   (
   Item   : Pointer
   );
  begin
    TKernel(GetHeapMan.FKernel).MClass.WriteExtPointer(@Self,Item);
  end;

Function TextPtr2.GetPn:Word;
  begin
    Result:=Pn;
  end;

Function TextPtr2.GetOfs:Word;
  begin
    Result:=Offset;
  end;


Procedure TextPtr2.PutMemPtr(Item:Pointer);
  begin
    Offset:=GetOffset(Item);
    Pn:=GetSelector(Item);
  end;


Function TextPtr2.GetMemPtr:POinter;
  begin
    Result:=MakeLongPointer(offset,Pn);
  end;
 *)
Class Function TMainClass2.NewInstance
   : TObject;
  var hPage : PPage;
      p     : pointer;
      size  : word;
      p_ofs : word;
      p_seg : word;
      h     : word;
      h2    : word;
  begin
   p:=GetHeapMan.MAllocate(GetHeapMan.ActualPage,InstanceSize);
   Result:=p;
   if p<> NIL then begin
     InitInstance(p);
   end;
  (*
    {p:=GetHeapMan.MAllocate(GetHeapMan.ActualPage,InstanceSize);}
    p:=MemAllocate(InstanceSize);
    if p<>NIL then begin
      size:=InstanceSize;
      p_ofs:=GetOffset(p);
      p_seg:=GetSelector(p);

      asm
          MOV     CX,Size
          MOV	BX,p_ofs
          MOV	DI,p_ofs
          MOV	ES,p_seg
          CLD
          MOV	AX,[BP+6].offs
          STOSW
          MOV	AX,[BP+6].segm
          STOSW
          XOR	AX,AX
          SUB	CX,4
          SHR	CX,1
          REP	STOSW
          ADC	CX,AX
          REP	STOSB
          MOV	AX,BX
      end;



    end;
    Result:=p;
    *)
  end;

Procedure TExtPtr.PutToVar
   (
   Item   : Pointer
   );
  begin
    TKernel(GetHeapMan.FKernel).MClass.WriteExtPointerWithOutSetModified(@Self,Item);
  end;


Procedure TMainClass.WriteExtPointerWithoutSetModified
   (
   FPtr   : Pointer;          {Adresse Basis}  {ZB. @FPointerA von TPolylistItem - zeigt auf GraphItem}
   APtr   : Pointer           {Inhalt  zb. GraphItem}
   );
  var helpPtr   : PExtPtr;
      FrameNum  : Word;
      SelAPtr   : Word;
      SelFPtr   : Word;
      Incptr       : TIncPtr;
      PointToPage  : PPage;
  begin
    helpPtr:=PExtPtr(FPtr);
    {helpPtr^.Ptr:=APtr;}

    SelAPtr:=getselector(APtr);
    SelFPtr:=getselector(FPtr);
    if SelAPtr=0 then begin
      helpPtr^.Offset:=0;
      helpPtr^.PN:=0;
     { HelpPtr^.Ptr:=NIL;}
    end
    else begin
      FrameNum:=GetHeapMan.SeekFrameNum(SelAPtr);
      HelpPtr^.PN:=PFrameArray(GetHeapMan.FrameTable)^[FrameNum].PageIndex;
     { PFrameArray(GetHeapMan.FrameTable)^[FrameNum].Modified:=1;                {new}
      if helpPtr^.PN = -1 then raise ElistError.Create('Page nicht eingelagert ');
      helpPtr^.Offset:=GetOffset(APtr);


    end;
  end;


 Procedure TExtPtrLst.Add(AItem : TExtPtr);
   begin
     if Count<16000 then
     Items[Count]:=AItem
     else Elisterror.create('extptrlst full');
     inc(Count);
   end;

 Procedure TExtPTrLst.Clear;
   begin
     Count:=0;
   end;

Function TExtPtr.GetAuto:TTraversePtr;
  var ptr   : TTraversePtr;
  begin
    ptr:= TTraversePtr.Create;
    Ptr.Item:=Get;
    Result:=ptr;
  end;

end.
