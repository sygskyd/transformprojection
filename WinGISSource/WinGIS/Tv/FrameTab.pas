unit Frametab;

 {FrameIndex auf -1 initialwert 150}
interface
Uses Classes, UClass, WinTypes,UlBase,NLBase,udef,utils;

const  BlockSize     = 64;
       initblocks    = 2;     {gibt an wieviele 64k-Seiten in uheap privat gbraucht werden}
       {der Eingentliche cache startet dann ab SeitenNr 'initblocks'}

Type TFreeListItem = class(TNLBaseItem)
       Index   : Longint;
       Constructor Create(AIndex:longint);
       Destructor Destroy; override;
     end;


     PFRameTableEntry  = ^TFrameTableEntry;
     TFrameTableEntry  = record
       Address           : Pointer;
      { Handle            : Word;}
       PageIndex         : integer;
       Info              : Word;         {Priority}
       modified          : Byte;
       free              : Byte;         {1=frei; 0=besetzt}
       locked            : Byte;
       LockCount         : word;
       ProcessID         : integer;   {Neu  ProcessID und PageIndex ergeben eindeutige PageNum}
     end;

     PFrameArray  =^TFrameArray;
     TFrameArray  = array[0..2000] of TFrameTableEntry;

     TFrameTable = class{(TMainClass)}
       FCapacity    : integer;
       FCount       : integer;
       FFrameTable  : PFrameArray;
       {FPageNumber  : LongInt;}
       Function    GetAddress(Aindex:word):Pointer;
       Procedure  SetCapacity(ACapacity:integer);
      public
       FreeList     : TNLBase;
       TraversePtr  : Longint;
       constructor Create(ACapacity:integer;AList:TNLBase);
       Function    DetectInitialFrameCapacity:word;
       {Function    CreatePage(AFrameIndex:Longint):Pointer;
       Function    FreePage(Page:Pointer):word;
       Function    CreatePrivateHeap:Pointer;}
       Procedure   InitGlobalPage(Page:Pointer;AFrameIndex:longint);
       {Procedure   FreePrivateHeap;}
       Procedure   Expand;
       Destructor  Destroy; override;
       Property    Count:integer read FCount write FCount;
       Property    Capacity:integer read FCapacity write SetCapacity;
       Property    FrameTable:PFrameArray read FFrameTable write FFrameTable;
  {     Property    PageNumber:LongInt read FPageNumber write FPageNumber;}
       property    Items[Aindex:Word]:Pointer  read GetAddress; default;
       Procedure   Clear(AList:TNLBase);
      { Property    Items[AIndex:Word]:Word read GetItem write SetItem; default;}
     end;

implementation

Uses Uheap,Process;

Function TFrameTable.GetAddress(Aindex:word):Pointer;
  begin
    Result:=FFrameTable^[Aindex].Address;
  end;

Constructor TFrameTable.Create;
  begin
    inherited Create;
    {PLongInt(Self)^:=Longint(TFrameTable);}
    GetHeapMan.FPageNumber:=0;
    FreeList:=AList;
    Capacity:=ACapacity;
    TraversePtr:=2;       {Seite 0(entspricth frametable[1]) darf nicht ausgelagert werden wegen startpointer}
  end;
{
Destructor TFrameTable.Destroy;
  var cnt   : integer;
  begin
    for cnt:=0 to FCapacity-1 do if FrameTable^[cnt].Address<>NIL then FreePage(FrameTable^[Cnt].Address);
    if FCapacity > 0 then GetHeapManager.DeAllocate(FrameTable,FCapacity * sizeof(TFrameTableEntry));
    inherited Destroy;
  end;
 }
Destructor TFrameTable.Destroy;
  var cnt   : integer;
  begin

  {  for cnt:=0 to FCapacity-1 do if FrameTable^[cnt].Address<>NIL then FreePage(FrameTable^[Cnt].Address);}
    if FCapacity > 0 then FreeMem(FrameTable,FCapacity * sizeof(TFrameTableEntry));
    inherited Destroy;
  end;

 Constructor TFreeListItem.Create
    (
    AIndex:Longint
    );
   begin
     inherited Create;
     {PLongint(Self)^:=Longint(TFreeListItem);}
     Index:=AIndex;
   end;

Destructor TFreeListItem.Destroy;
  begin
    inherited Destroy;
  end;

Function TFrameTable.DetectInitialFrameCapacity : word;
  begin
    Result:=128;
  end;

{
 Function  TFrameTable.CreatePage(AFrameIndex:longint) : Pointer;
   var hgm      : THandle;
       pb       : Pointer;
       temp     : word;
   begin
     hgm:=GlobalAlloc(GMEM_MOVEABLE,BlockSize*1024);
     if hgm<>0 then begin
       pb:=GlobalLock(hgm);
       InitGlobalPage(pb,AFrameIndex);
       inc(GetHeapManager.FPageNumber);
       Result:=pb;
     end else Result:=NIL;
   end;


 Function TFrameTable.FreePage
    (
    Page       : Pointer
    )
    : word;
   begin
     GlobalUnLock(GetSelector(Page));
     Result:=GlobalFree(GetSelector(Page));
     if Result<>0 then raise ElistError.Create('Speicher erfolglos freigegeben');
   end;

 }

 Procedure TFrameTable.InitGlobalPage
    (
    Page   : Pointer;
    AFrameIndex : longint
    );
   begin
     FillChar(Page^,65535,#0);
     PPage(Page)^.Partition:=0;
     PPage(Page)^.Signature:=2801;
     PPage(Page)^.Capacity:=BlockSize;
     PPage(Page)^.FreeSpace:=BlockSize*1024 - sizeof(TPage) + 2*sizeof(HeapCoord);
     PPage(Page)^.PageNumber:=GetHeapMan.FPageNumber;
     PPage(Page)^.IntPointerList:=NIL;
     PPage(Page)^.ExtPointerList:=NIL;
     PPage(Page)^.VmtPointerList:=NIL;
     PPage(Page)^.FreeList:=SizeOf(TPage) - 2*sizeof(HeapCoord);
     PPage(Page)^.heapBlockNext:=0;
     PPage(Page)^.HeapBlockSize:=PPage(Page)^.FreeSpace;
     PPage(Page)^.FrameId:=AFrameIndex;;
   end;

   {
 Procedure TFrameTable.FreePrivateHeap;
   var handle : THandle;
       cnt    : integer;
   begin
     for cnt:= 0 to FCapacity-1 do begin
       FreePage(PFrameArray(FrameTable)^[cnt].Address);
     end;
   end;
 }

 Procedure TFrameTable.SetCapacity
    (
    ACapacity  : integer
    );
  var HelpPtr      : PFrameArray;
      OldCap       : integer;
      Cnt          : integer;
      AItem        : TFreeListITem;
      helpPage     : PPage;
      Asize        : word;
      p            : pointer;
      BaseSel      : Word;
  begin
    OldCap:=FCapacity;
    if ACapacity>FCapacity then begin
      if FCapacity>0 then begin
        raise ElistError.Create('Darf nicht erweitert werden');
        GetMem(HelpPtr,ACapacity*sizeof(TFrameTableEntry));
        if HelpPtr<>NIL then begin
          System.Move(FrameTable^[0],HelpPtr^[0],FCapacity*sizeof(TFrameTableEntry));
          FreeMem(FrameTable,FCapacity*sizeof(TFrameTableEntry));
          FrameTable:=HelpPtr;
        end;
      end
      else begin
        GetMem(FFrameTable,ACapacity*sizeof(TFrameTableEntry));
      end;
      FCapacity:=ACapacity;
    end;


    p:=GetHeapMan.PrivateHeap;     {Speicher wird in Uheap allokiert}

    BaseSel:=GetSelector(p);

    for cnt:=0 to Capacity-1 do begin
      {FrameTable^[cnt].Address:=CreatePage(cnt);}
      PFrameArray(FrameTable)^[cnt].Address:=MakeLongPointer(0,(BaseSel+(cnt+initblocks)*MSelectorInc));
      InitGlobalPage(PFrameArray(FrameTable)^[cnt].Address,cnt);
      FrameTAble^[cnt].Free:=1;
      FrameTAble^[cnt].Modified:=0;
      FrameTable^[cnt].PageIndex:=-1;
      FrameTable^[cnt].Locked:=0;
      FrameTable^[cnt].LockCount:=0;

      if FrameTable^[cnt].Address=NIL then break;

      AItem:=TFreeLIstItem.Create(cnt);
      FreeList.Add(AItem);

    end;
   end;

   (*
 Function TFrameTable.CreatePrivateHeap:Pointer;
   var hgm      : THandle;
       size     : longint;
       hpar     : PHeapAr;
       BaseSel  : word;
       fValid   : Boolean;
       cnt      : longint;
       p        : Pointer;
   begin
     size:=BlockSize*1024*FCapacity;
     if size > MaxAllocSize then raise ElistError.Create('Block too large');
     hgm:=GlobalAlloc(GMEM_MOVEABLE,SIZE);
     if hgm = 0 then raise ElistError.Create('not enough Memory available');

     p:=GlobalLock(hgm);

     BaseSel:=GetSelector(p);


     Result:=p;
   end;

*)

Procedure TFrameTable.Expand;
  begin
    raise Elisterror.create('---');
    Capacity:=Capacity+16;
  end;


Procedure TFrameTable.Clear(AList:TNLBase);
  var cnt      : longint;
      AItem    : TFreeListITem;
  begin
   {
   for cnt:=0 to FCapacity-1 do if FrameTable^[cnt].Address<>NIL then FreePage(FrameTable^[Cnt].Address);
    if FCapacity > 0 then FreeMem(FrameTable,FCapacity * sizeof(TFrameTableEntry));
   FCapacity:=0;
   FCount:=0;
   FPageNumber:=0;
   FFrameTable:=NIL;
    }
  { Freelist:=Alist;    {<--}
   TraversePtr:=2;

   {Capacity:=125;}

   for cnt:=1 to Capacity-1 do begin
      FrameTAble^[cnt].Free:=1;
      FrameTAble^[cnt].Modified:=0;
      FrameTable^[cnt].PageIndex:=-1;
      FrameTable^[cnt].Locked:=0;
      FrameTable^[cnt].LockCount:=0;

      {AItem:=TFreeLIstItem.Create(cnt);
      FreeList.Add(AItem);}

      InitGlobalPage(FrameTable^[cnt].Address,-1);
    end;

  end;

end.
{
 Procedure TFrameTable.CombinePageTableFrameTable;
   var cnt:word;
   begin
     for cnt:=0 to FrameCapacity-1 do begin
       TPageTable(PageTable)[cnt]:=cnt;
       PFrameArray(FrameTable)^[cnt].PageIndex:=cnt;
     end;
   end;
 }