unit uprojstyle;

interface
uses Projstyle, classes;

type
  TCProjStyle = class
    XStyle: TProjectStyles;
    CopyTo: TProjectStyles;
    constructor Create(AStyle: TProjectStyles);
    procedure CopyProjectStyle;
    procedure WriteToSTream(AStyle: TProjectStyles; AStream: TStream);
    function REadfromSTream(var AStyle: TProjectStyles; ASTream: TSTream): TProjectSTyles;
    destructor Destroy; override;
    procedure RestoreRegistry(var Areg: POinter);
  end;

implementation
uses XFills, XLines, regDB, objects, palette;

constructor TCProjStyle.Create(AStyle: TProjectStyles);
begin
  inherited Create;
end;

destructor TCProjStyle.Destroy;
begin

  inherited Destroy;
end;

procedure TCProjStyle.CopyProjectStyle;
begin
end;

procedure TCProjStyle.WriteToSTream(AStyle: TProjectStyles; AStream: TStream);
var
  i: integer;
  Afill: TCustomXFill;
  ALine: TXLineStyle;
begin
  ASTream.WRite(AStyle.XFillStyles.Count, sizeof(Integer));
    //AFill:=TCustomXFill.Create;
  for i := 0 to AStyle.XFillStyles.Capacity - 1 do
  begin
    AFill := (AStyle.XFillStyles.Items[i]);
    if Afill <> nil then
      AStream.WRite(Pointer(AFill)^, TCustomXFill.InstanceSize);
  end;
  ASTream.WRite(AStyle.XLineStyles.Count, sizeof(Integer));
  for i := 0 to AStyle.XLineStyles.Capacity - 1 do
  begin
    ALIne := (AStyle.XLIneStyles.Items[i]);
    if ALine <> nil then
      AStream.WRite(Pointer(ALine)^, TXLineStyle.InstanceSize);
  end;
  i := Astream.Size;
  i := TXLIneStyle.InstanceSize;
end;

function TCProjStyle.REadfromSTream(var AStyle: TProjectStyles; ASTream: TSTream): TProjectStyles;
var
  Cnt: Integer;
  i: integer;
  Afill: TCustomXFill;
  ALine: TXLineStyle;
begin

    //Astyle.Free;
    //AStyle:=TPRojectstyles.create;

  AStream.Seek(0, 0);
  Astream.REad(Cnt, Sizeof(integer));
  AFill := TCustomXfill.Create;
  ALine := TXlineStyle.Create;

  for i := 0 to cnt - 1 do
  begin
    Fillchar(Pointer(Afill)^, TCustomXFill.InstanceSize, #0);
    AStream.REad(Pointer(AFill)^, TCustomXFill.InstanceSize);
      //AStyle.XFillStyles.Add(AFill);
  end;
  Astream.REad(Cnt, Sizeof(integer));
  for i := 0 to cnt - 1 do
  begin
    Fillchar(Pointer(ALine)^, TXLineStyle.InstanceSize, #0);
    AStream.REad(Pointer(ALIne)^, TXLineStyle.InstanceSize);
      //AStyle.XFillStyles.Add(AFill);
  end;

end;

procedure TCProjSTyle.RestoreRegistry(var Areg: POinter);
var
  REgData, b: TRegistryDAtaBase;
  s: TOldMemorySTream;
//  d: POldSTream;
  APal: TPalette;
  bPal: TPalette;
  xL: TXLineStyleList;
  i: integer;
  j: integer;
  ALine: TXLineStyle;
  Number: integer;
begin
  RegData := TRegistryDataBase(AReg);
  bPal := TPalette.create;
  aPal := TPalette.create;
  xl := TXLineStyleList.Create;
  b := TRegistryDataBase.CReate;
  s := TOldMemorySTream.Create;// .init(1024, 1024);
  s.seek(0, soFromBeginning);
//  d := @s;
  RegDAta.StoreToStream(s);
//  RegDAta.StoreToStream(POldSTream(d)^);
  s.seek(0, soFromBeginning);
  b.LoadFromStream(s);
//  b.LoadFromStream(d);
  aPal.ReadFromRegistry(b, '\Project\Palette');
  bPal.ReadFromRegistry(REgdata, '\Project\Palette');
  XL.ReadFromRegistry(b, '\Project\XLineStyles');
  for i := 0 to xl.Capacity - 1 do
  begin
    ALIne := (xl.Items[i]);
    if Aline <> nil then
    begin
      Number := ALine.Number;
      j := 0;
    end;
  end;
    //RegDAta.Free;
    //Areg:=b;
  s.Free;
//  s.done;
end;

end.

