unit Upixel;

interface
uses uheap,uview,uclass,Ulbase,wintypes,WinProcs,udef,am_paint,am_def,am_font,classes,sysutils;

Const MaxDist      = 7;
      LineLen      = 7;

Type CPixel = class(CView)
      private
       Procedure   AddPointers; override;
       Procedure   DelPointers; override;
      public
       Position  : TDPoint;
       procedure   CalculateClipRect;
       constructor Create(APos : TDPoint);
       Function    GetObjType:Word; override;
       Function    SelectByPoint(PInfo:PPaint;Point:TDPoint;ObjType:TObjectTypes):boolean; override;
       Function    SelectByRect(PInfo:PPaint;Rect:TDRect;Inside:Boolean):Boolean; override;
       Procedure   Draw(PInfo:PPaint); override;
       Procedure   Dra;override;
       Function    GivePosX:longint;
       Function    GivePosY:Longint;
      { Procedure   GetClipRect(var AClipRect:TClipRect); override;}
     end;

implementation

Constructor CPixel.Create(APos : TDPoint);
  begin
    
    Position.Init(APos.X,APos.Y);
  end;

Procedure CPixel.AddPointers;
  begin
    GetHeapMan.AddVmtPtr(Self);
  end;

Procedure CPixel.DelPointers;
  begin
    GetHeapMan.DelVmtPtr(Self);
  end;

Procedure CPixel.CalculateClipRect;
  begin
    with Position do ClipRect.Assign(X-LineLen,Y-LineLen,X+LineLen,Y+LineLen);
  end;

Procedure CPixel.Draw
   (
   PInfo : PPaint
   );
  var APoint        : TPoint;               
  begin
    if Visible(PInfo) then begin
      PInfo^.ConvertToDisp(Position,APoint);
      with PInfo^.ExtCanvas do begin
        MoveTo(APoint.X-LineLen,APoint.Y);
        LineTo(APoint.X+LineLen+1,APoint.Y);
        MoveTo(APoint.X,APoint.Y-LineLen+1);
        LineTo(APoint.X,APoint.Y+LineLen+1);
      end;
    end;
  end;


 Function  CPixel.SelectByPoint(PInfo:PPaint;Point:TDPoint;ObjType:TObjectTypes):boolean;
   var Rect         : TDRect;
      MDist        : LongInt;
  begin
    if (ot_PIxel in ObjType) then begin
      MDist:=PInfo^.CalculateDraw(MaxDist);
      Rect.Init;
      Rect.A:=Position;
      Rect.B:=Position;
      Rect.A.Move(-MDist,-MDist);
      Rect.B.Move(MDist,MDist);
      if Rect.PointInside(Point) then Result:=true
      else Result:=false;
    end
    else Result:=false;
  end;

 Function  CPixel.SelectByRect(PInfo:PPaint;Rect:TDRect;Inside:Boolean):Boolean;
   begin
   end;

Procedure CPixel.Dra;
var a : longint;
  begin
    a:=Index;
  end;

Function    CPixel.GetObjType:Word;
  begin
    Result:=ot_Pixel;
  end;

Function    Cpixel.GivePosX:longint;
  begin
    Result:=Position.x;
  end;

Function    CPixel.GivePosY:Longint;
  begin
    Result:=Position.y;
  end;

end.
