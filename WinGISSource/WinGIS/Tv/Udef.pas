unit Udef;

interface
uses wintypes,am_def;
const
      ObjectTypes       = 2;           { Anzahl verschiedener Objekttypen      }
      maxVertexEntries  = 255;         { Anzahl Vertices pro Kachel            }
      clFromLayer       = $80000000;   { Farbe ist Farbe des Layers            }
      Epsilon           = 1E-5;        { Toleranz f�r Float-Vergleiche         }
      MaxInt128         = 128000;
type
     CLong              = longint;
     Str80              = String[80];
     TCoord             = Double;

     CPoint = record
       X      : Clong;
       Y      : Clong;
     end;

     CRect = record
       l,u,r,b : CLong;
     end;

     TClipRect          = object
       Left             : TCoord;
       Right            : TCoord;
       Bottom           : TCoord;
       Top              : TCoord;
       Function   isempty : boolean;
     end;
             (*
     TPointItem = class(TSLBaseItem)
      private
       FX       : CLong;
       FY       : CLong;
       {Procedure AddPointers; override;
       Procedure DelPointers; override; }
      public
       property X : CLong read FX write FX;
       property Y : CLong read FY write FY;
       Constructor Create(Ax,Ay:CLong);
       Destructor Destroy; override;

     end;
               *)

Procedure SetClipRect(var ARect:TClipRect;ALeft,ARight,ABottom,ATop:TCoord);
Function IsRectInside(Const Rect1,Rect2:TClipRect):Boolean;
Function IsPointInside(APoint:CPOint; ARect:TClipREct):Boolean;
Function GetCenter(ARect:TClipRect):CPoint;        {TCliprect}
Function GetCenterTD(ARect:TDRect):CPoint;         {TDREct}
Procedure InitRectTD(var AREct:TClipRect;BRect:TDRect);
implementation
                 (*
Constructor TPointItem.Create(Ax,Ay:CLong);
  begin
    PLongInt(Self)^:=longint(TPointItem);
    AddPointers;
    X:=Ax;
    Y:=Ay;
  end;

Destructor TPointItem.Destroy;
  begin
    DelPointers;
    inherited Destroy;
  end;
{
Procedure TPointItem.AddPointers;
  begin
    HeapManager.AddVmtPtr(Self);
  end;

Procedure TPointItem.DelPointers;
  begin
    HeapManager.DelVmtPtr(Self);
  end;
}
                   *)

Procedure SetClipRect
   (
   var ARect       : TClipRect;
   ALeft           : TCoord;
   ARight          : TCoord;
   ABottom         : TCoord;
   ATop            : TCoord
   );
  begin
    with ARect do begin
      Left:=ALeft;
      Right:=ARight;
      Bottom:=ABottom;
      Top:=ATop;
    end;
  end;

 {ist Rect1 in Rect2 enthalten?}
Function IsRectInside
  (
  Const Rect1      : TClipRect;
  Const Rect2      : TClipRect
  )
  : Boolean;
 begin
   Result:=FALSE;
   if Rect1.Top<=Rect2.Top then
   if Rect1.Bottom>=Rect2.Bottom then
   if Rect1.Left>=Rect2.Left then
   if Rect1.Right<=Rect2.Right then Result:=TRUE
 end;

Function IsPointInside(APoint:CPOint; ARect:TClipREct):Boolean;
  begin
    Result:=false;
    if (APoint.x >= Arect.left)and
       (APoint.x <  Arect.right)and
       (APOint.y >= Arect.bottom) and
       (APOint.y <  Arect.top) then REsult:=true;
  end;


Function GetCenter(ARect:TClipRect):CPoint;
 var l,r,b,t: double;

  begin
    l:=ARect.Left;
    r:=ARect.Right;
    b:=Arect.Bottom;
    t:=Arect.Top;
    Result.x:=round(l+(r-l)/2);
    Result.y:=round(b+(t-b)/2);
  end;

Function GetCenterTD(ARect:TDRect):CPoint;
 var l,r,b,t: double;

  begin
    l:=ARect.a.x;
    r:=ARect.b.x;
    b:=Arect.a.y;
    t:=Arect.b.y;
    Result.x:=round(l+(r-l)/2);
    Result.y:=round(b+(t-b)/2);
  end;

Procedure InitRectTD(var AREct:TClipRect;BRect:TDRect);
  begin
    ARect.Left:=BREct.A.x;
    AREct.Right:=BRect.B.x;
    ARect.bottom:=BRect.A.y;
    ARect.top:=BRect.B.y;
  end;

Function TCliprect.isempty:Boolean;
  begin
    Result:=false;
    if (right=0)and(left=0)and(bottom=0)and(top=0) then result:=true;
  end;

begin
end.


