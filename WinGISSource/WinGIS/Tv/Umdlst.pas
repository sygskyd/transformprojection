unit Umdlst;
{unit MDataLst}
{stellt eine List zur Verfuegung, welche Pointer auf MEMDATA in CLayer speichert}
{diese Liste dient zur Erstellung des Index Arrays fuer die Datenbankabfrage}

interface
uses classes,sysutils,uclass,umemlst,uview,udbindex,uptlst,winprocs,uglob,ubtree,ubtreep,ualock;

type

  TMemlist = class(TList)
     Destructor Destroy; override;
     Procedure Clear;
     Procedure AddItem(aItem:TmemLst;AName:string);
     Procedure CreateDBIndizes(ALayers:Pointer);
     Procedure Print;
  end;

  TMemDataLstItem = class
   public
    MemLst     : TMemLst;
    TExt       : string;
    constructor create(AItem:TMemLst;AName:string);
    destructor destroy;
  end;

{var MemDataLst : TMemList;}

implementation

uses uLayer,uadmin;

Destructor TMemDatalstItem.destroy;
  begin
    MemLst.free;
    inherited Destroy;
  end;

Destructor TMemList.Destroy;
  var Aitem : TMemDataLstItem;
      i     : integer;
  begin
    for i:=0 to Count-1 do begin
      Aitem:=Items[i];
      Aitem.Free;
    end;
    inherited Destroy;
  end;

Procedure TMemList.Clear;
 var Aitem : TMemDataLstItem;
      i     : integer;
  begin
    for i:=0 to Count-1 do begin
      Aitem:=Items[i];
      Aitem.Free;
    end;
    if Count>0 then inherited Clear;
  end;


Constructor TMemDataLstItem.create(AItem:TMemlst;AName:string);
  begin
    inherited Create;
    MemLst:=AItem;
    Text:=AName;
  end;

Procedure TMemList.AddItem(AItem:TMemLst;AName:string);
  var BItem : TMemDataLstItem;
  begin
    BItem:=TMemDataLstItem.create(AItem,Aname);
    Add(BItem);
  end;

Procedure TMemList.Print;
  var
      AItem    : TMemDataLstItem;
      Memdata  : TMemLst;
      f        : TextFile;
      p        : TMemLstItem;
      i        : longint;
      j        : longint;
      AViewitem : TExtptr;
      AView     : CView;
  begin
    Assignfile(f,'d:\test.dat');
    rewrite(f);
    for i:=0 to count- 1 do begin
      AItem:=Items[i];
      MemData:=Aitem.MemLst;
      p:=MemData.Head;
      writeln(f,MemData.count,'All: ',MemData.Allcount);
      while p<>NIL do begin
        writeln(f,p.count);
        for j:=0 to p.Count-1 do begin
          AviewItem:=p.Items[j];
          AView:=AViewItem.get;
          writeln(f,'Pn:  ',AviewItem.pn,'   Ofs: ',AViewitem.offset,' DB: ',AView.Index,' Index: ',AView.Index2);
        end;
        p:=p.next;
      end;
    end;
    closefile(f);
  end;

Procedure TMemList.CreateDBIndizes(ALayers:POinter);
  var
      AItem    : TMemDataLstItem;
      Memdata  : TMemLst;
      p        : TMemLstItem;
      i        : longint;
      j        : longint;
      AViewitem : TExtptr;
      AView     : CView;
      c         : longint;
      help      : TDBLst;
      APtr      : TExtPtr;
      BPtr      : TMRec;
      BView     : CView;
      ptLayer    : TExtPtr;
      ALayer     : CLayer;
      aBtree    : Btree;
      bBtree    : Btree;
      pTree     : BPTree;
      mItem     : Item;
      pt1       : TTraversePtr;
      ptRoot    : TTraversePtr;
      {u,v       : TDBLstItem;}
  begin

   for i:=0 to count- 1 do begin
      AItem:=Items[i];
      MemData:=Aitem.MemLst;
      ptLayer:=CLayers(ALayers).GetLayerExtptr(AItem.Text);
      ALayer:=ptLayer.Get;
      ALayer.LockPage(ALayer);

      p:=MemData.Head;

      c:=Memdata.allcount;
      {help:=TDBLst.Create(c);}

      aBtree:=Btree.create;
      {bBtree:=Btree.create;}
      pt1:=TTraversePtr.create;
      pTree:=BPtree.create;
      pt1.item:=ptree;  {lock ptree}
      pTree.Count:=c;

      {help.LockPage(Help);}

      {ALayer.DBindex.Put(help);}
      ALayer.DBindex.Put(ptree);

      ALayer.Modified(ALayer);
      ALayer.UnlockPage(ALayer);

      p:=MemData.Head;

      while p<>NIL do begin
        for j:=0 to p.Count-1 do begin

          AviewItem:=p.Items[j];
          BView:=AViewItem.Get;
          {BPtr.DbIndex:=BView.Index;
          BPtr.APtr:=AViewItem;
          help.Add(BPtr);
          }
          MItem.F:=BView.INdex;
          MItem.APtr:=AViewItem;
          aBtree.Insert(MItem);

        end;
        p:=p.next;
      end;
      {help.unlockpage(help);}
      {bBtree.Root:=abtree.treecopy;}
      ptRoot:=abtree.treecopyp;
      pTree.Root:=ptRoot.Item;
      ptRoot.free;
      ptree.setHead;
      {ptree.Print;}

      {bBtree.SetHead;}
      {bBtree.print;}
      abtree.free;
      pt1.free;       {unlock ptree}
      {bbtree.free;}

    end;
  end;

begin
  {MemDAtaLst:=NIL;}

end.



 {
      u:=help.head;

      while u<>NIL do begin
        v:=u;
        v.lockpage(v);
        u:=u.next;
        v.unlockpage(v);
      end;  }
