unit fileman;
interface
uses sysutils,classes,DLbase;
const
      StartIndex = 0;                   {Nummer des Blockes ab dem ins File gespeichert wird}
      {isFreeTAble=500;                  {WICHTIG!!!}
      BlockSize  = 64;             {64}
      StringSize = 80;                   {for constructor load}

      ClusterSize =1024;                       {1024}


Type
     TFileInfo     = record             {Am Beginn jedes files}
       Version     : longint;
       FileManPos  : longint;
       Count       : longint;
       Capacity    : longint;
       FreeListCnt : Longint;
       AddrTabPos  : longint;
       CheckId     : longint;
       ObjCount    : longint;
     end;

     TFName        = string[80];
     CFreeListItem = class(TDLBaseItem)
       Index  : Longint;
     end;

     TFileMan = class
      private
       Count      : longint;         {Index}
       Capacity   : longint;
       ItemSize   : Longint;
       FreeList   : TDLBase;
       MStream    : TFileStream;
       FileManPos : longint;
       Offset     : Longint;
      public
       Info       : TFileInfo;
       Constructor Create;
       Destructor  Destroy; override;
       Function    Getstream : TFileStream;

       Procedure   CreateFile(AFileName:TFName);
       Procedure   OpenFile(AFileName:TFName);              {:TStream; }
       Procedure   CloseFile;
       PRocedure   Append;

       Procedure   WritePosition(BlockIndex:Longint;APage:Pointer);
       Procedure   ReadPosition(BlockIndex:LongInt;var APage:Pointer);
       Function    GetFreeBlockIndex:Longint;
       Procedure   DeleteBlockIndex(AIndex:Longint);
       Function    StreamValid:Boolean;
       Function    GetFileManPos:Longint;
       Procedure   PutAddrTab;
       Procedure   SetAddrTab;

       Procedure   Load;
       Procedure   STore;
       Function    GetFileName : String;
       PRocedure   PutFileName(astr : String);
       PRocedure   PutCheckId(AId:Longint);
       Procedure   PutObjCount(ACount:Longint);
       Function    GetObjCount:Longint;
     end;

implementation
uses uheap,am_def,winprocs,wintypes;

Procedure TFileMan.PutFileName(astr : sTring);
  begin
  end;

Function  TFileMan.GetFileName : String;
  begin
  end;

constructor TFileMan.Create;
  begin
    inherited Create;
    ItemSize:=Clustersize*BlockSize;
    FreeList:=TDLBase.Create;
    Offset:=SizeOf(TFileInfo);
  end;

Destructor TFileMan.Destroy;
  begin
    Freelist.Free;
    MStream.Free;
    inherited Destroy;
  end;

Function TFileMan.StreamValid:Boolean;
  begin
    Result:=False;
    if MStream<>NIL then REsult:=True;
  end;

Function TFileMan.GetFreeBlockIndex:Longint;
  var p : TDLBaseItem;
  begin
    if MStream<>NIL then begin
      if Freelist.Count>0 then begin
        p:=FreeList.GetHead;
        Result:=CFreeListItem(p).Index;
        FreeList.DeleteHead;
      end else begin
        Result:=Count;
        Inc(Capacity);
      end;
      inc(count);
    end else
      {MessageDlg('ERROR Stream NIL.', mtInformation,[mbOk], 0);}
      raise ElistError.Create('ERROR Stream NIL.');
  end;

Procedure TFileMan.DeleteBlockIndex
   (
   AIndex : Longint
   );
  var AItem    : CFreeListItem;
  begin
    AItem:=CFreeListItem.Create;
    AItem.Index:=AIndex;
    FreeList.Add(AItem);
    dec(count);
  end;

Procedure TFileMan.CreateFile
   (
   AFileName   : TFName
   );
  begin
    MStream:=TFileStream.Create(AfileName,fmCreate);
  end;

Procedure TFileMan.OpenFile
   (
   AFileName   : TFName
   );{:TStream;}
  var FileAttr: Integer;
  begin
    fileAttr:=filegetattr(AFileName);
    if (FileAttr and FaReadOnly)=faReadOnly then begin
      MStream:=TFileStream.Create(AfileName,fmOpenRead);
      GetHeapMan.REadOnly:=True;
    end else begin
      MStream:=TFileStream.Create(AfileName,fmOpenReadWrite);
      GetHeapMan.REadOnly:=false;
    end;
  end;

Procedure TFileMan.CloseFile;
  begin
    MStream.Free;
  end;

Procedure TFileMan.WritePosition
   (
   BlockIndex : Longint;
   APage      : Pointer
   );
  var
  Position : longint;
  begin
  if blockindex=-1 then raise Elisterror.create('BlockIndex=-1');
   Position:=ItemSize*BlockIndex+Offset;
 { if blockindex=100  then begin
    a:=0;                dd
  end; }
    if BlockIndex>=Count then raise ElistError.Create('Capacity');
    if (Mstream<>NIL)and(BlockIndex<Count) then begin
      MStream.seek(Position,0);
      try
      MStream.WriteBuffer((APage)^,ItemSize);
      except
        //MsgBox(Statusline^.hwindow,3102,mb_OK or mb_IconHand,'');
        raise ElistError.create('');
      end;
    end;
  end;


Procedure TFileMan.ReadPosition
   (
   BlockIndex: Longint;
   var APage : Pointer
   );
  var Position : longint;
  begin
    {if blockindex=32  then begin
      a:=0;
    end;}
    Position:=ItemSize*BlockIndex+Offset;
     if BlockIndex>=Count then raise ElistError.Create('Capacity');
    if (MStream<>NIL) and (BlockIndex<Count)then begin
      MStream.seek(Position,0);
      MStream.REad((APage)^,ItemSize);
      {writeln(Heapmanager.fout,'swap in: ',BlockIndex);}
    end;
  end;

  PRocedure TFileMan.Load;
    var cnt   : longint;
        AItem : CFreeListITem;
        AIndex : longint;
        i      : longint;
    begin
      if MStream=NIl then raise ElistError.Create('Mstream nil');
      {MStream.read(count,sizeof(longint));
      MStream.read(Itemsize,sizeof(longint));
      MStream.Read(cnt,sizeof(longint));
      {MStream:=Files;
      FreeList:=TDLBase.Create;}

      MStream.Seek(Info.FileManPos,0);         {von FileEnde lesen}
      MStream.Read(Info,SizeOf(TFileInfo));

      Count:=Info.Count;
      Capacity:=Info.Capacity;
      ItemSize:=clustersize*BlockSize;
      FileManPos:=Info.FileManPos;
      Offset:=sizeof(TFileInfo);

      cnt:=Info.FreeListcnt;
      if cnt>0 then begin
        for i:=1 to cnt do begin
          MStream.Read(AIndex,Sizeof(longint));
          AItem:=CFreeListItem.Create;
          AITem.Index:=AIndex;
          Freelist.Add(AITem);
        end;
      end;
      {FreeList!!!}
    end;

  Procedure TFileMan.Store;
   var  help  : longint;
        p     : TDLBaseITem;
        cnt   : longint;
   begin
     {MStream.write(count,Sizeof(longint));
     MStream.write(Itemsize,Sizeof(longint));
     cnt:=Freelist.count;
     MStream.Write(cnt,sizeof(longint));  }
     Info.Version:=280171;
     Info.FileManPos:=Itemsize*capacity+offset;    {ist gleich wie  MStream.Size!!!  }
     Info.Capacity:=Capacity;
     Info.Count:=Count;
     Info.FreeListCnt:=FreeList.count;

     MStream.Seek(0,0);
     MStream.Write(Info,SizeOf(TFileInfo));
     MStream.Seek(Info.FileManPos,0);

     if Freelist.Count>0 then begin       {FreeList ans FileEnde anhaengen}
       p:=Freelist.GetHead;
       while p<>NIL do begin
         MStream.write(CFreeListItem(p).Index,sizeof(longint));
         p:=p.next;
       end;
     end;
     {FreeList!!!}
   end;

PRocedure TfileMan.Append;
  begin
    MStream.Seek(MStream.Size,0);
  end;

Function TFileMan.GetSTream:TFileSTream;
  begin
    Result:=Mstream;
  end;

Function TFileMan.GetFileManPos:Longint;
  begin
    Result:=Info.FileManPos;
  end;

Procedure   TFileMan.PutAddrTab;
  begin
    Info.AddrTabPos:=MStream.Position;
  end;

Procedure TfileMan.SetAddrTab;
  begin
    MStream.Seek(Info.AddrTabPos,0);
  end;

PRocedure TFileMan.PutCheckId(AId:Longint);
  begin
    Info.CheckId:=AId;
  end;


Procedure TFileMan.PutObjCount(ACount:Longint);
  begin
    Info.ObjCount:=ACount;
  end;

Function  TFileMan.GetObjCount:Longint;
  begin
    Result:=Info.ObjCount;
  end;
end.


