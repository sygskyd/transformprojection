{deselectall}
{monitoring}
{cnt5:=TGlobal(GetheapMan.GlobalObj).SelLst.SelectCount;}
{immer die Anzahl der Selektieren Objekte}
{SelRect... selektierter Bereich... 1..mehrere Objekte}
unit Udraw;
{= altes am_Proj}
{Daten Import, Daten-Haltung , Drawroutine fuer alle Layer}
interface
uses Uheap,Am_Paint,ulayer,uadmin,process,upoly,ucpoly,udef,ulbase,am_Layer,am_Index,am_view,am_def,am_Poly,
     classes,am_CPoly,sysutils,utext,am_text,am_sym,usymbol,upixel,am_point,UQTree,UPTree,uview,Uclass,
     ubucketl,Am_stddl,usellst
     {$IFNDEF WMLT}
     ,am_dde
     {$ENDIF}
     ,utdrlst, uLaylst, udrwsel, uglob, ulayref, uproj ,upaint,umdlst,
     udbase, am_meas, umeas, am_circl, ucirc, am_splin, uspline,ulaysort, am_coll,objects;

Type TDocument = class
      private
       ConvertProj      : pointer;
       ConvertInfo      : PPaint;
       OldProj          : Pointer;
       PInfo            : PPaint;
       {FLayer           : TExtPtr;}
       Layers           : TExtPtr;
       PRoject          : CProj;      {SetSTart}
       FLayer            : CLayer;
       AProcess         : TProcess;
       BProcess         : TProcess;
       FObjectNumber    : longint;
       {FLeafNumber     : LongInt;}
       QTLayer          : PLayer;
       QTLayerIndex     : Longint;
       XOffset          : Longint;
       YOffset          : Longint;
       MBucketList      : TList;
       a                : longint;
       b                : longint;
       heigth           : longint;
       width            : longint;
       DelLayerMemData  : Boolean;
       DrawLst          : TbitLst;
       SElLst           : TBitLst;

       lfile            : TextFile;

       LastText    : LongInt;
       LastSymbol  : LongInt;
       LastPixel   : LongInt;
       LastPoly    : LongInt;
       LastCPoly   : LongInt;
       LastSpline  : LongInt;
       LastMesLine : LongInt;
       LastCircle  : LongInt;
       LastArc     : LongInt;
       LastGroup   : Longint;

       MAXLastText    : LongInt;
       MaxLastSymbol  : LongInt;
       MaxLastPixel   : LongInt;
       MaxLastPoly    : LongInt;
       MaxLastCPoly   : LongInt;
       MaxLastSpline  : LongInt;
       MaxLastMesLine : LongInt;
       MaxLastCircle  : LongInt;
       MaxLastArc     : LongInt;
       MaxLastGroup   : longint;

       {conversion}
       RectItem    : TrectItem;
       Allcount    : longint;
       Docount     : longint;
       cntxx       : longint;
       OldHeapMan  : THeap;
       InsertLayer : cLayer;
       DefStr      : STring;
       IsSerialNum : Boolean;

       Function    GetFLayer : CLayer;
       PRocedure   SetFLayer(Item:CLayer);
       Function    GetExtmap:Boolean;
       Function    GetBasemap:Boolean;
       Procedure   SetBaseMap(AVal:Boolean);
       Procedure   SetExtMap(AVal:Boolean);
      public
       Proj             : Pointer;
       Flag             : Boolean;
       ActualLayer      : str80;
       loaded           : boolean;
       SelREct          : TDrect;
       LastSelRect      : TDRect;
       SelCount         : Longint;
       DiffObjects      : Boolean;
       SelectedObjectType : Longint;
       InputFileName    : string;
       FirstLayerVisited : Boolean;
       JustCreated       : Boolean;

       {ClipRect         : TClipRect;}
       constructor Create(AProj:Pointer;AInfo:PPaint);
       Procedure   Init(AProj:Pointer;AInfo:PPaint);
       Destructor  Destroy; override;
       Procedure   ImportFromSelLayer;
       Procedure   OnNew(AfileName:string);
       Function    Import:TRectItem;
       Function    ImportProject(Ax,Ay:Longint):TRectItem;
       Function    ImportWithLayers(ALayers:TSTringList):TREctItem;
       PRocedure   CreateIndexTree;                                   {}
       Function    OnLoad(AFileName:string):Boolean;
       Procedure   Onstore(AFileName:string);
       Procedure   OnUpdate(AFileName:string);
       Procedure   Draw;
       Procedure   DrawByQuery;
       {Procedure   InsertObject(AObject:CView;LayerNumber:Word);
       Procedure   AddBucketLeaf(ALeaf:TBucketLeaf);
       Procedure   AddPBucketLeaf(ALeaf:TPBucketLeaf);
       Procedure   DeleteBucketLeaf(ALeaf:TBucketLeaf);                   }
       Procedure   CreateBaumLayer(AProj:Pointer;Layername:string);
       {PRocedure   DoTree(tree:TPBucketBase);
       PRocedure   DrawBaum(tree:TPBucketBase);}
       PRocedure   ConvertFTreeToFPTree;
       Procedure   BaumConvert(ANode:TBucketBase;AInsNode:TPBucketBase;AInsParent:TPBucketBase);
       Procedure   TreeSkeletonInsertData(ARefNode:TBucketBase;AInsNode:TPBucketBase);
       Procedure   CopyTreeDAta(ALeaf:TBucketLeaf;AInsLeaf:TPBucketLeaf);
       {Property    ALayer:Clayer read GetFLayer write SetFLayer;  {ActualLayer}
       Property    ALayer : CLayer read FLayer write FLayer;
       Function    GetLayer:Clayer;
       Function    GetLayers:CLayers;

       Procedure   DeSelectAll(ATextOut:Boolean);
       Procedure   Monitoring;
       Function    SelectByPoint(Point:TDPoint;ObjType:TObjectTypes):Pointer;
       Function    SelectByRect(Rect:TDRect;Inside:Boolean):Boolean;
       PRocedure   PutCheckId(AId:Longint);
       Procedure   ClearDrawbit;
       Procedure   PutObjCount(ACount:Longint);
       Function    GetObjCount:Longint;
       Procedure   Resetmemlayers;
       Function    GetLayerExtPtr(Atext:string):TExtPtr2;
       Function    GetLayerbyName(Atext:string):POinter;
       procedure   DoDbIndizes;
       Procedure   ReAssignMemdata;
       {$IFNDEF AXDLL} // <----------------- AXDLL
       Procedure   Convert(MainWindow:Pointer;OutFileName,wgpFileName:string;FileList:TStringList);
       Procedure   ConvertWithLayers(InputChild:Pointer;MainWindow:Pointer;InpFileName,HspFileName,
                   HsdFileName:string;HspList,hsdList,OutFileLIst:TStringList);
       {$ENDIF} // <----------------- AXDLL
       Procedure   InsertLayers(Bdata,BNewData:Pointer);
       Procedure   InsertViews(Data,NewData:Pointer);
       PRocedure   INsertLayersHspHsd(BData,BNewData:POinter;hspList,hsdlist:TStringList);
       PRocedure   INsertRefLayersHsd(BNewData:POinter;hsdlist:TStringList);
       PRocedure   INsertRefLayers(bData,BNewData:POinter);
       Property    IsBaseMap:boolean read GetBaseMap write SetBaseMap;
       Property    IsExtMap:boolean read GetExtMap write SetExtMap;
       Function    InsertLayerByName(Statusdlg:Pointer;ALayerName:string):Boolean;
       PRocedure   ConverttoWGP(ALayer:CLayer);
       Procedure   DTree(tree:TPBucketBase);
       Procedure   TraverseTree(tree:TPBucketBase);
       Procedure   AddIndex;
       Procedure   AddIndex1(ALength:integer);
       PRocedure   UpdateLastIndizes(AData:Pointer);
       PRocedure   UpdateLastIndizesMAX(AData:Pointer);
       PRocedure   UpdateLastIndizesWithOldIndizes(AData:Pointer);
       PRocedure   SetSerialNumber(ANumber:string);
       Procedure   REsetSerialNumber;
       PRocedure   IncStatus(AView:CView);
       Procedure   DecStatus(AView:CView);
       Procedure   ClearStatus;
       PRocedure   UpdateDiffObjects;
       Function    GetSelObjectType:longint;
       Procedure   ChangeObjectStyle(AColl:PViewColl;LayerName:string);
       Function    GetProj:CProj;
       Procedure   PrintDBIndizes;
       Procedure   TestSearch(AProj:Pointer;LayerName:string; Aindex:longint);
       Procedure   InsertBitMapOnRefLayer(AIndex:PIndex;ALayer:PIndex);
       Procedure   InsertBitMaps;
       PRocedure   InsertProjStyles(AProj,AData:Pointer);
       PRocedure   UpdateLastIndizesByIndex(AData:Pointer;AIndex:longint);
       Procedure   DrawSelection(AInfo:PPaint);


       {ueberprueft selection mit ProgisID ind Btree}
     end;

function GetConSym: Pointer;


var AConSym          : Pointer;

const
HSP_LOGFILE          = 'Hsp.log';
HSP_LOGHEADER        = 'Filename;AmpProgisId;HspProgisId';
HSP_LOGSEPERATOR     = ';';

implementation

uses am_Main,am_child,am_proj,uconsym,am_obj,umemlst,am_Sight,ubtreep,am_admin,
     wintypes,winprocs,forms,statebar,userintf,am_rtext,am_dbgra;

function GetConSym: POInter;
  begin
    Result:=AConSym;
  end;

Constructor TDocument.Create
   (
   AProj  : Pointer;
   AInfo  : PPaint
   );
  var APoint : CPoint;
      p      : TSLBaseItem;
  begin
    inherited Create;
      PInfo:=AInfo;
      Proj:=AProj;

      LastText:=mi_text;
      LastSymbol:=mi_symbol;
      LastPixel:=mi_Pixel;
      LastPoly:=mi_Poly;
      LastCPoly:=mi_CPoly;
      LastSpline:=mi_Spline;
      LastMesLine:=mi_MesLine;
      LastCircle:=Mi_Circle;
      LastArc:=Mi_Arc;
      LastGroup:=MI_Group;

      MaxLastText:=mi_text;
      MaxLastSymbol:=mi_symbol;
      MaxLastPixel:=mi_Pixel;
      MaxLastPoly:=mi_Poly;
      MaxLastCPoly:=mi_CPoly;
      MaxLastSpline:=mi_Spline;
      MaxLastMesLine:=mi_MesLine;
      MaxLastCircle:=Mi_Circle;
      MaxLastArc:=Mi_Arc;
      MaxLastGroup:=Mi_Group;

      Defstr:='0000100100';
  end;

Procedure TDocument.Init
   (
   AProj  : Pointer;
   AInfo  : PPaint
   );
  begin
      PInfo:=AInfo;
      Proj:=AProj;
  end;

Destructor TDocument.Destroy;
  begin
    if AProcess<>nil then begin
      if DelLayerMemData then begin
        {REsetmemLayers;}
        DelLayermemdata:=false;
      end;
      GetHeapMan.SetKernel;
      AProcess.Free;
      AProcess:=NIL;
    end;
    if SelLst <> NIL then Sellst.Free;
    if DrawLst <> NIL then Drawlst.Free;
    {ALayer.Free;}
    if bProcess<>NIL then bProcess.CloseSwapFile;
    inherited Destroy;
  end;

Procedure TDocument.SetFLayer(Item:CLayer);
  begin
    {FLayer.Put(Item);}
  end;

Function TDocument.GetFLayer:Clayer;
  begin
    {Result:=FLayer.Get;}
  end;
                                 {
Procedure TDocument.AddBucketLeaf
   (
   ALeaf           : TBucketLeaf
   );
  begin
    Inc(FLeafNumber);
    ALeaf.BucketNumber:=FLeafNumber;
  end;

Procedure TDocument.AddPBucketLeaf
   (
   ALeaf           : TPBucketLeaf
   );
  begin
    Inc(FLeafNumber);
    ALeaf.BucketNumber:=FLeafNumber;
  end;

Procedure TDocument.DeleteBucketLeaf
   (
   ALeaf           : TBucketLeaf
   );
  begin
  end;
                                  }

PRocedure TDocument.CreateIndexTree;
  var p               : TSLBaseITem;
      AView           : CView;
      i               : longint;
      j               : longint;
      AInsNode        : TInsnode;
      VertexList      : TList;
      InsList         : TSLBase;
      VItem           : TVertexITem;
      VertexItem      : TPVertexITem;
      StatusDlg       : Pointer;
  begin
    Clayers(Layers.Get).CreateIndexTree(Proj,DefStr,isSerialNum);
    {Layers.Saveoutall;}
    GetHeapMan.doit:=true;
    {GetHeapMan.createf;}
    {Layers.TraverseAllLayers;}
    GetHeapMan.doit:=false;
    {GetHeapMan.Closef;}
  (*
    StatusDlg:=CreateAbortDlg(PProj(Proj)^.Parent);
    SetClipRect(ClipRect,PProj(Proj)^.Size.A.x,PProj(Proj)^.Size.B.x,
    PProj(Proj)^.Size.A.y,PProj(Proj)^.Size.B.y);

    i:=0;
    j:=ALayer.Data.Count;
    p:=ALayer.Data.Head;
    while p<>NIL do begin
      AView:=TLayerItem(p).Mptr;
      InsertObject(AView,0);
      StatusDlg^.SetPercent(round(100*(i/j)));
      inc(i);
      p:=p.next;
    end;


    MBucketList:=TList.create;
    Mbucketlist.capacity:=500;

    GetHeapMan.SetProcess(AProcess);
    ALayer.InitPTree;    {erzeugt lediglich die Wurzel!!!}

    {Erzeugt Persitent-QuadTree}
    ALayer.TreeSkeleton(ALayer.Tree,ALayer.pTree,NIL);

    {Schneide Objekte in Alayer mit P-Quadtree}
    ALayer.IntersectLayerPQuadTree(StatusDlg);

    Dispose(StatusDlg,Done);
    TKernel(GetHeapMan.FKernel).MClass.UnlockAll;
    *)
  end;
  (*
Procedure TDocument.InsertObject
   (
   AObject         : CView;
   LayerNumber     : Word
   );
  var Bucket       : TBucketBase;
      Node         : TBucketNode;
      AClipRect    : TClipRect;
      Rect         : TClipRect;
      Width        : TCoord;
      Height       : TCoord;
      ALeaf        : TBucketLeaf;
      Offsets      : Array[0..3,0..1] of TCoord;
  begin
    Inc(FObjectNumber);
    AObject.SetIndex(FObjectNumber);
    AObject.GetClipRect(AClipRect);
    while not IsRectInside(AClipRect,ALayer.Tree.ClipRect) do with ALayer.Tree.ClipRect do begin
      Width:=Right-Left;
      Height:=Top-Bottom;
      if AClipRect.Top>Top then begin
        Offsets[0,1]:=Height;  Offsets[1,1]:=Height;
        Offsets[2,1]:=0.0;     Offsets[3,1]:=0.0;
      end
      else begin
        Offsets[0,1]:=0.0;     Offsets[1,1]:=0.0;
        Offsets[2,1]:=-Height; Offsets[3,1]:=-Height;
      end;
      if AClipRect.Left<Left then begin
        Offsets[0,0]:=-Width; Offsets[1,0]:=0.0;
        Offsets[2,0]:=-Width; Offsets[3,0]:=0.0;
      end
      else begin
        Offsets[0,0]:=0.0;    Offsets[1,0]:=Width;
        Offsets[2,0]:=0.0;    Offsets[3,0]:=Width;
      end;
      SetClipRect(Rect,Left+Offsets[0,0],Right+Offsets[1,0],
          Bottom+Offsets[2,1],Top+Offsets[0,1]);
      Node:=TBucketNode.Create(Rect,NIL,Self);
      for Cnt:=0 to 3 do begin
        if (Abs(Offsets[Cnt,0])<Epsilon)
            and (Abs(Offsets[Cnt,1])<Epsilon) then Node[Cnt]:=ALayer.Tree
        else begin
          SetClipRect(Rect,Left+Offsets[Cnt,0],Right+Offsets[Cnt,0],
              Bottom+Offsets[Cnt,1],Top+Offsets[Cnt,1]);
          ALeaf:=TBucketLeaf.Create(Rect,Node,Self);
          AddBucketLeaf(ALeaf);
          Node[Cnt]:=ALeaf;
        end;
      end;
      ALayer.Tree:=Node;
    end;

    AObject.Insert(Self,LayerNumber);           {*********************************************************************}

  end;

*)
PRocedure TDocument.OnNew(AfileName:string);
  var   h : TExtPtr;
  begin
    GetHeapMan.SetKernel;
    AProcess:=TProcess.Create(GetHeapMan.FreeList);
    GetHeapMan.SetProcess(AProcess);
    AProcess.OnNew(AfileName);

    Project:=CProj.Create;
    Layers:=Project.Layers;
    CLayers(Layers.Get).Init(Proj,Self);
    {Layers:=CLayers.Create(Proj,Self);}

    a:=0;
    b:=0;              {spalten, width}
    width:=PProj(Proj)^.Size.b.x-PProj(Proj)^.Size.a.x;
    heigth:=PProj(Proj)^.Size.b.y - PProj(Proj)^.Size.a.y;
    PProj(Proj)^.Size.b.x:=PProj(Proj)^.Size.a.x + (b+1)*width+100;
    PProj(Proj)^.Size.b.y:=PProj(Proj)^.Size.a.y + (a+1)*heigth+100;

    {Hier wird das ClipRect des amp-files gesetzt!}
    {und wird nach GetHeapMan.size geschrieben}
    SetClipRect(CLayers(Layers.Get).ClipRect,PProj(Proj)^.Size.A.x,PProj(Proj)^.Size.B.x,
    PProj(Proj)^.Size.A.y,PProj(Proj)^.Size.B.y);
    {SetclipRect(Layers.Cliprect,1364910,1510008,1863991,18710052);}
    {Austria:}
    {SetClipRect(Layers.Cliprect,-30000000,30000000,15000000,45000000);}

    {AProcess.SetSTart(CLayers(Layers.Get));}
    AProcess.SetStart(Project);
  end;

Function TDocument.OnLoad(AfileName:string):Boolean;
var ObjCnt: longint;
    APaint : CPaint;
    hstr   : string;
    lstr   : string;
    isSnum : Boolean;
    hhh    : pointer;
  begin
   Result:=true;
   isSNum:=false;
   GetHeapMan.Proj:=Proj;
   GetHeapMan.PInfo:=PProj(Proj)^.Pinfo;
   AProcess:=TProcess.Create(GetHeapMan.FreeList);
   AProcess.OnOpen(AfileName);
   AProcess.LoadAll;         {new}
   ObjCnt:=GetObjCount;

   Project:=AProcess.GetStart;
   Layers:=Project.Layers;
   APaint:=Project.PInfo.Get;
   {
   PProj(Proj)^.BaseMap:=isBaseMap;
   PProj(Proj)^.ExtMap:=isExtMap;

   PProj(Proj)^.BaseMap:=false;
   PProj(Proj)^.ExtMap:=true;
   PProj(Proj)^.AttribFix:=false;
   }
   PProj(Proj)^.AHeapManager.isAttribFix:=PProj(Proj)^.AttribFix;

   if DrawLst<> NIl then Drawlst.free;
   if SelLst<>NIl then SEllst.free;
   DrawLst:=TBitLst.Create(ObjCnt);
   SelLst:=TBitLst.Create(ObjCnt);
   TGlobal(GetheapMan.GlobalObj).SelLst:=SelLst;
   TGlobal(GetheapMan.GlobalObj).DrawLst:=DrawLst;

   SetClipRect(CLayers(Layers.Get).ClipRect,PProj(Proj)^.Size.A.x,PProj(Proj)^.Size.B.x,
   PProj(Proj)^.Size.A.y,PProj(Proj)^.Size.B.y);

   {Layers.Init(Proj,Self);}
   {PProj(Proj)^.Pinfo^.RedrawScreen(true);}
   GetHeapMan.SetProcess(AProcess);
   CLayers(Layers.Get).InitLayers;
   {Getlayers.TraverseDBIndizes;}
   {ClearDrawBit;}

   Result:=true;
   Loaded:=true;

   hhh:=GetLayerByName('GN');
  end;


Function TDocument.InsertLayerByName(Statusdlg:Pointer;ALayerName:string):Boolean;
 var BLayer    : PLayer;
      cnt      : Longint;
      cnt2     : Longint;
      cnt3     : longint;
      AIndex   : PIndex;
      AView    : PView;
      APoly    : PPoly;
      ACPoly   : PCPoly;
      APoint   : PDPoint;
      APixel   : PPixel;
      AText    : PText;
      ARTExt   : PRText;
      ASym     : PSymbol;
      AMesLine : PMeasureLine;
      ASpline  : PSpline;

      InsertPoly  : CPoly;
      InsertCPoly : CCPoly;
      InsertText  : CText;
      InsertSym   : CSymbol;
      InsertPOint : CPixel;
      InsertMes   : CMeasureLine;
      InsertCirc  : CEllipse;
      InsertArc   : CArc;
      ACirc       : PEllipse;
      Arc         : PEllipseArc;
      InsertSpline: CSpline;
      NewPoint : CPoint;

      i        : longint;
      J        : longint;
      AClipRect : TDREct;
      Pos       : TDPoint;
      helpText  : string;
      hSymIndex : longint;
      hFontIndex: longint;
      AProj     : PProj;
      Pmemlstitem : TMemlstItem;
      AMemLst   : TMemlst;
      cnt4      : longint;
      ObjIndex  : longint;
      AExtPtr   : TEXTPtr;
      NewIndex  : TIndex;
      xxx       : word;

  begin



    AProj:=ConvertProj;

    XOffset:=0;  {0 fuer  einfach ohne Daten duplizieren}
    YOffset:=0;  {0 fuer  einfach}


    for i:=1 to AProj^.Layers^.Ldata^.count-1 do begin


        BLayer:=AProj^.Layers^.Ldata^.at(i);

        if BLayer<>NIL then begin
          if BLayer^.Text^=ALayerName then begin
            if Blayer=NIL then raise Elisterror.create('layer nil');
            {create layer; alayer = actueller Arbeitslayer}
            {ALayer:=CLayers(Layers.Get).InsertLayer(Self,ClipRect,BLayer^.Text^,$000000FF,lt_Solid,$00FF0000,pt_Solid,iltop);}
            ALayer:=CLayers(Layers.Get).NameToLayer(BLayer^.Text^);

            if ALayer=NIL then begin
              raise Elisterror.create('layer nil');
            {Layers.Cliprect bestimmt den Rahmen des Q-Trees!}
              {
              ALayer:=CLayers(Layers.Get).InsertLayer(Self,CLayers(Layers.Get).ClipRect,BLayer^.Text^,BLayer^.color,
              BLayer^.LineStyle,
                BLayer^.Patcolor,BLayer^.Pattern,iltop,BLayer^.Index);
              TKernel(GetHeapMan.FKernel).MClass.Modified(CLayers(Layers.Get));
              TKernel(GetHeapMan.FKernel).MClass.Modified(ALayer);
              ALayer.SetGeneralMin(BLayer^.GeneralMin);
              ALayer.SetGeneralMax(BLayer^.GeneralMax);
              if BLayer^.Getstate(sf_general) then Alayer.SetState(sf_general,true);
              if BLayer^.Getstate(sf_LayerOff) then Alayer.SetState(sf_LayerOff,true);
              if not BLayer^.Getstate(sf_LayerOff) then begin
                Alayer.SetState(sf_LayerOff,false);
                ALayer.SetState(sf_HSDVisible,TRUE);
              end;  }
            end;

            TKernel(GetHeapMan.FKernel).MClass.LockPage(ALayer);
            {MemDataLst.AddItem(ALayer.Memdata);}


            AMemlst:=TMemlst.create;
            SortLayer(AProj,BLayer,AMemlst);

            {for cnt:=0 to BLayer^.Data^.GetCount-1 do begin
              AIndex:=BLayer^.Data^.At(cnt);}

            PMemLstItem:=AMemLst.Head;
            While pMemlstItem<>NIL do begin
              for cnt4:=0 to pMemlstItem.count-1 do begin
                ObjIndex:=longint(PMemLstItem.items[cnt4]);
                NewIndex.Index:=ObjIndex;
                AIndex:=BLayer^.HasObject(@NewIndex);
                if AIndex<>NIL then begin
                  if AIndex<>NIL then AView:=BLayer^.IndexObject(ConvertInfo,AIndex);
                  if AView<>NIL then begin
                    if cntxx=0 then begin
                      RectItem.InitbyTDRect(AView^.Cliprect);
                    end else RectItem.CorrectByTDRect(AView^.Cliprect);
                    inc(cntxx);
                    AclipREct.InitByRect(AView^.ClipRect);

                    AclipREct.a.x:=AView^.ClipRect.a.x+XOffset;
                    AclipREct.a.y:=AView^.ClipRect.a.y+YOffset;
                    AclipREct.b.x:=AView^.ClipRect.b.x+XOffset;
                    AclipREct.b.y:=AView^.ClipRect.b.y+YOffset;


                        if Docount = 32451 then begin
                       xxx:=0;
                    end;

                    inc(Docount);
                    //StatusDlg^.SetPercent(100*(docount/Allcount));
                    Statusbar.Progress:=100*(docount/Allcount);

                    if AView^.Getobjtype=ot_poly then begin
                      APoly:=PPoly(AView);
                      InsertPoly:=CPoly.Create(APoly^.Data^.count);
                      TKernel(GetHeapMan.FKernel).MClass.LockPage(InsertPoly);
                      TKernel(GetHeapMan.FKernel).MClass.Modified(InsertPoly);
                      InsertPoly.Index:=AView^.Index;
                      InsertPoly.InitObjectStyle(AIndex^.Objectstyle);
                      {InsertPoly.Index:=LastPoly;}
                      Inc(LastPoly);
                      if PProj(Proj)^.LogFile then writeln(LFile,InputFileName,HSP_LOGSEPERATOR,AView^.Index,HSP_LOGSEPERATOR,LastPoly);
                      for cnt2:=0 to APoly^.Data^.Count-1 do begin
                        APoint:=APoly^.Data^.At(cnt2);
                        NewPoint.x:=APoint^.x + XOffset;
                        NewPoint.y:=APoint^.y + YOffset;
                        InsertPoly.InsertPoint(NewPoint);
                      end;
                      InsertPOly.ClipRect.InitByRect(AClipRect);
                      InsertPoly.Length:=APoly^.Laenge;
                       {ALayer.InsertObject(InsertPoly);}
                      ALayer.InsertObject2(InsertPoly);
                      TKernel(GetHeapMan.FKernel).MClass.UnLockPage(InsertPoly);
                    end  else

                    if AView^.Getobjtype=ot_Cpoly then begin
                      ACPoly:=PCPoly(AView);
                      InsertCPoly:=CCPoly.Create(ACPoly^.Data^.Count);
                      InsertCPoly.LockPage(InsertCPoly);
                      InsertCPoly.Modified(InsertCPoly);
                      {
                      TKernel(GetHeapMan.FKernel).MClass.LockPage(InsertCPoly);
                      TKernel(GetHeapMan.FKernel).MClass.Modified(InsertCPoly);
                      }
                      InsertCPoly.Index:=AView^.Index;
                      InsertCPoly.Area:=ACPoly^.Flaeche;
                      InsertCPoly.Length:=ACPoly^.Laenge;
                      InsertCPoly.InitObjectStyle(AIndex^.Objectstyle);
                      {nsertCPoly.Index:=LastCPoly;}
                      Inc(LastCPoly);
                      if PProj(Proj)^.LogFile then writeln(LFile,InputFileName,HSP_LOGSEPERATOR,AView^.Index,HSP_LOGSEPERATOR,LastCPoly);
                      for cnt2:=0 to ACPoly^.Data^.Count-1 do begin
                        APoint:=ACPoly^.Data^.At(cnt2);
                        NewPoint.x:=APoint^.x + XOffset;
                        NewPoint.y:=APoint^.y + YOffset;
                        InsertCPoly.InsertPoint(NewPoint);
                      end;
                      InsertCPOly.ClipRect.InitByRect(AClipRect);
                      InsertCPoly.SetSTate(ACpoly^.State,true);
                      if ACPoly^.GetState(sf_IslandArea) then begin

                        InsertCPoly.FIslandInfo.Put(
                        GetHeapMan.MAllocate(GetHeapMan.ActualPage,ACPoly^.IslandCount*sizeof(longint)));
                        InsertCPoly.IslandCount:=ACPoly^.IslandCount;
                        for cnt3:= 0 to InsertCPoly.Islandcount-1 do begin
                          InsertCPoly.IslandInfo^[cnt3]:=ACPoly^.IslandInfo^[cnt3];
                        end;
                      end;
                      {ALayer.InsertObject(InsertCPoly);}
                      ALayer.InsertObject2(InsertCPoly);
                      TKernel(GetHeapMan.FKernel).MClass.UnLockPage(InsertCPoly);
                    end else
                    if AView^.Getobjtype=ot_Text then begin
                      AText:=PText(AView);

                      if Atext^.Text<>NIL then begin
                        helpText:=Atext^.Text^;
                        {
                        if length(Atext^.Text^)>79 then begin
                          helpText:=copy(ATExt^.Text^,0,79);
                          helpa:=length(helptext);
                        end;
                         }
                        Pos.x:=Atext^.pos.x;
                        Pos.y:=Atext^.pos.y;
                        Pos.x:=Pos.x + XOffset;
                        Pos.y:=Pos.y + YOffset;
                        hFontIndex:=TConSym(GetConSym).GetNewFont(AText^.Font.Font);
                        AText^.Font.font:=hFontIndex;
                        InsertText:=CText.Create(Pos,Atext^.Angle,Atext^.Width,Atext^.Font,helpText);
                        TKernel(GetHeapMan.FKernel).MClass.LockPage(InsertText);
                        TKernel(GetHeapMan.FKernel).MClass.Modified(InsertText);
                        InsertText.Index:=AView^.Index;
                        InsertText.InitObjectStyle(AIndex^.Objectstyle);
                        {InsertText.Index:=LastText;}
                        Inc(LastText);
                        if PProj(Proj)^.LogFile then writeln(LFile,InputFileName,HSP_LOGSEPERATOR,AView^.Index,HSP_LOGSEPERATOR,LastText);
                        InsertText.ClipRect.InitByRect(AClipRect);
                        {ALayer.InsertObject(InsertText);}
                        ALayer.InsertObject2(InsertText);
                        TKernel(GetHeapMan.FKernel).MClass.UnLockPage(InsertText);
                      end;
                    end
                    else if (AView^.Getobjtype=ot_Symbol) then begin
                      ASym:=PSymbol(AView);
                      Pos.x:=Asym^.Position.x;
                      Pos.y:=Asym^.Position.y;
                      Pos.x:=Pos.x + XOffset;
                      Pos.y:=Pos.y + YOffset;
                      hSymIndex:=TConSym(GetConSym).GetNewSym(Asym^.SymIndex);
                      InsertSym:=CSymbol.Create(ConvertInfo,Pos,hSymIndex,Asym^.Size,ASym^.Angle,ASym^.SizeType);
                      TKernel(GetHeapMan.FKernel).MClass.LockPage(InsertSym);
                      TKernel(GetHeapMan.FKernel).MClass.Modified(InsertSym);
                      InsertSym.Index:=AView^.Index;
                      InsertSym.InitObjectStyle(AIndex^.Objectstyle);
                      {InsertSym.Index:=LastSymbol;}
                      Inc(LastSymbol);
                      if PProj(Proj)^.LogFile then writeln(LFile,InputFileName,HSP_LOGSEPERATOR,AView^.Index,HSP_LOGSEPERATOR,LastSymbol);
                      InsertSym.ClipRect.InitByRect(AClipRect);
                      {ALayer.InsertObject(InsertSym);}
                      ALayer.InsertObject2(InsertSym);
                      TKernel(GetHeapMan.FKernel).MClass.UnLockPage(InsertSym);

                    end else
                    if AView^.Getobjtype=ot_Pixel then begin
                      APixel:=PPixel(AView);
                      Pos.x:=APixel^.Position.x;
                      Pos.y:=APixel^.Position.y;
                      Pos.x:=Pos.x + XOffset;
                      Pos.y:=Pos.y + YOffset;
                      InsertPoint:=CPixel.Create(Pos);
                      InsertPoint.Index:=AView^.Index;
                      InsertPoint.InitObjectStyle(AIndex^.Objectstyle);
                      {InsertPoint.Index:=LastPixel;}
                      Inc(LastPixel);
                      if PProj(Proj)^.LogFile then writeln(LFile,InputFileName,HSP_LOGSEPERATOR,AView^.Index,HSP_LOGSEPERATOR,LastPixel);
                      InsertPoint.ClipRect.InitByRect(AClipRect);
                      {ALayer.InsertObject(InsertPoint);}
                      ALayer.InsertObject2(InsertPoint);
                    end else
                    if AView^.Getobjtype=ot_Circle then begin
                      ACirc:=PEllipse(AView);
                      InsertCirc:=CEllipse.Create(ACirc^.Position,ACirc^.PrimaryAxis,ACirc^.SecondaryAxis,ACirc^.Angle);
                      InsertCirc.ClipRect.InitByRect(AClipRect);
                      InsertCirc.Index:=AView^.Index;
                      InsertCirc.InitObjectStyle(AIndex^.Objectstyle);
                      {InsertCirc.Index:=LastCircle;}
                      Inc(LastCircle);
                      if PProj(Proj)^.LogFile then writeln(LFile,InputFileName,HSP_LOGSEPERATOR,AView^.Index,HSP_LOGSEPERATOR,LastCircle);
                      ALayer.InsertObject2(Insertcirc);
                    end else
                    if AView^.Getobjtype=ot_Arc then begin
                      Arc:=PEllipseArc(AView);
                      InsertArc:=CArc.Create(Arc^.Position,Arc^.PrimaryAxis,Arc^.SecondaryAxis,Arc^.Angle,
                        Arc^.BeginAngle,Arc^.EndAngle);
                      InsertArc.ClipRect.InitByRect(AClipRect);
                      InsertArc.Index:=AView^.Index;
                      InsertArc.InitObjectStyle(AIndex^.Objectstyle);
                      {InsertARc.Index:=LastArc;}
                      Inc(LastArc);
                      if PProj(Proj)^.LogFile then writeln(LFile,InputFileName,HSP_LOGSEPERATOR,AView^.Index,HSP_LOGSEPERATOR,LastArc);
                      ALayer.InsertObject2(InsertArc);
                    end else
                    if AView^.Getobjtype=ot_Mesline then begin
                      AMesLine:=PMeasureLine(AView);
                      InsertMes:=CMeasureLine.Create(AMesLine^.StartPoint,AMesLine^.EndPoint);
                      InsertMes.SetSTate(AMesLine^.State,true);
                      InsertMes.ClipRect.InitByRect(AClipRect);
                      InsertMes.Index:=AView^.Index;
                      InsertMes.InitObjectStyle(AIndex^.Objectstyle);
                      {InsertMes.Index:=LastMesLine;}
                      Inc(LastMesLine);
                      if PProj(Proj)^.LogFile then writeln(LFile,InputFileName,HSP_LOGSEPERATOR,AView^.Index,HSP_LOGSEPERATOR,LastMesLine);
                      ALayer.InsertObject2(InsertMes);
                    end else
                    if AView^.Getobjtype=ot_spline then begin

                      ASpline:=PSpline(AView);
                      InsertSpline:=CSpline.Create(ASpline^.Data^.count);
                      TKernel(GetHeapMan.FKernel).MClass.LockPage(InsertSpline);
                      TKernel(GetHeapMan.FKernel).MClass.Modified(InsertSpline);
                      InsertSpline.Index:=AView^.Index;
                      InsertSpline.InitObjectStyle(AIndex^.Objectstyle);
                      {InsertSpline.Index:=LastSpline;}
                      Inc(LastSpline);
                      if PProj(Proj)^.LogFile then writeln(LFile,InputFileName,HSP_LOGSEPERATOR,AView^.Index,HSP_LOGSEPERATOR,LastSpline);
                      for cnt2:=0 to ASpline^.Data^.Count-1 do begin
                        APoint:=ASpline^.Data^.At(cnt2);
                        NewPoint.x:=APoint^.x + XOffset;
                        NewPoint.y:=APoint^.y + YOffset;
                        InsertSpline.InsertPoint(NewPoint);
                      end;
                      InsertSpline.ClipRect.InitByRect(AClipRect);
                      ALayer.InsertObject2(InsertSpline);
                      TKernel(GetHeapMan.FKernel).MClass.UnLockPage(InsertSpline);
                    end else if Aview^.Getobjtype=(ot_image) then begin
                      InsertBitMapOnRefLayer(AView,BLayer);
                    end
                    else if Aview^.Getobjtype=(ot_busgraph) then begin
                      InsertBitMapOnRefLayer(AView,BLayer);
                    end
                    else if Aview^.Getobjtype=(ot_RText) then begin
                      InsertBitMapOnRefLayer(AView,BLayer);
                    end;

                    begin
                    end;
                  end; {Aview<>NIL}
                end; {Aindex <>NIL}
              end;  {for cnt4:=0 to pmemlstitem.coount-1}
              pMemLstItem:=PMemLstItem.next;
            end;   {all layeritems}
            TKernel(GetHeapMan.FKernel).MClass.UnLockPage(ALayer);
            AMemLst.Free;
          end; {bLayer^.text^=ALayername}
        end; {BLayer<>NIL}


    end; {for each layer}


   Result:=true;
  end;



 Function    TDocument.ImportWithLayers(ALayers:TSTringList):TREctItem;
 var BLayer    : PLayer;
      cnt      : Longint;
      cnt2     : Longint;
      cnt3     : longint;
      AIndex   : PIndex;
      AView    : PView;
      APoly    : PPoly;
      ACPoly   : PCPoly;
      APoint   : PDPoint;
      APixel   : PPixel;
      AText    : PText;
      ASym     : PSymbol;
      AMesLine : PMeasureLine;
      ASpline  : PSpline;

      InsertPoly  : CPoly;
      InsertCPoly : CCPoly;
      InsertText  : CText;
      InsertSym   : CSymbol;
      InsertPOint : CPixel;
      InsertMes   : CMeasureLine;
      InsertCirc  : CEllipse;
      InsertArc   : CArc;
      ACirc       : PEllipse;
      Arc         : PEllipseArc;
      InsertSpline: CSpline;
      NewPoint : CPoint;

      i        : longint;
      J        : longint;
      Statusdlg : Pointer;
      AClipRect : TDREct;
      Pos       : TDPoint;
      helpText  : string;
      hSymIndex : longint;
      hFontIndex: longint;
      AProj     : PProj;
  begin
    AProj:=ConvertProj;
    StatusDlg:=CreateAbortDlg(AProj^.Parent);
    RectItem := TRectItem.Create;

    XOffset:=0;  {0 fuer  einfach ohne Daten duplizieren}
    YOffset:=0;  {0 fuer  einfach}

    Allcount:=0;
    Docount:=0;
    cntxx:=0;
    if AProcess<>NIL then begin

     {1!!! kein Ahnung was Layer 0 soll}
     {
      for i:=1 to AProj^.Layers^.Ldata^.count-1 do begin
        BLayer:=AProj^.Layers^.Ldata^.at(i);
        if BLayer<>NIL then Allcount:=Allcount+ BLayer^.Data^.GetCount;
      end;
      }

      Allcount:=0;
      {
      for i:=0 to ALayers.count-1 do begin
        BLayer:=AProj^.Layers^.NameToLayer(ALayers.strings[i]);
        if BLayer<> NIl then Allcount:=AllCount+BLayer^.Data^.GetCount;
      end;
      }
      for i:=0 to ALayers.count-1 do begin
        BLayer:=AProj^.Layers^.NameToLayer(ALayers.strings[i]);
        if BLayer<> NIl then begin
          Allcount:=AllCount+BLayer^.Data^.GetCount;
          ALayer:=CLayers(Layers.Get).NameToLayer(BLayer^.Text^);
          if ALayer=NIL then begin
            ALayer:=CLayers(Layers.Get).InsertLayer(Self,CLayers(Layers.Get).ClipRect,BLayer^.Text^,BLayer^.Linestyle.color,
              BLayer^.LineStyle.Style,0,0,iltop,BLayer^.Index);
            TKernel(GetHeapMan.FKernel).MClass.Modified(CLayers(Layers.Get));
            TKernel(GetHeapMan.FKernel).MClass.Modified(ALayer);
            //if BLayer^.Pattern > 7 then ALayer.Pattern:=TConSym(GetConSym).GetNewPat(BLayer^.Pattern);
            ALayer.SetGeneralMin(BLayer^.GeneralMin);
            ALayer.SetGeneralMax(BLayer^.GeneralMax);
            //if BLayer^.Getstate(sf_general) then Alayer.SetState(sf_general,true);
            if BLayer^.Getstate(sf_LayerOff) then Alayer.SetState(sf_LayerOff,true);
            if not BLayer^.Getstate(sf_LayerOff) then Alayer.SetState(sf_LayerOff,false);
            //ALayer.SetState(sf_HSDVisible,TRUE);
          end;
        end;
      end;





      for i:=0 to ALayers.count-1 do begin
        INsertLayerbyname(Statusdlg,Alayers.strings[i]);
      end;


    end;  {Aprocess<>NIL}
    //Dispose(StatusDlg,Done);
    REsult:=RectItem;
  end;


PRocedure TDocument.ConvertToWGP(ALayer: CLayer);
  begin
    DTree(ALayer.ptree);
    ALayer.unlockall;
  end;


Procedure Tdocument.DTree(tree:TPBucketBase);
  begin
    tree.LockPage(tree);
    if tree is TPBucketNode then with tree as tPbucketnode do begin
      dTree(sons[0]);
      dTree(sons[1]);
      dTree(sons[2]);
      dTree(sons[3]);
    end
    else begin
      TraverseTRee(Tree);
    end;
  end;

Procedure TDocument.TraverseTree(tree:TPBucketBase);
 var cnt         : longint;
      AvertexItem : TSLBaseITem;
      Help        : TSLBaseITem;
      AView       : CView;
      d           : longint;
      ap          : POinter;
      InsNode     : TInsNode;

      Rect         : TDRect;
      Width        : Double;
      DrawIt       : Boolean;
      VList        : TlayReflst;
      i            : Longint;
      AViewItem    : TExtPtr;
      AviewItem2   : TExtPtr2;
      OText        : TText;
      AText        : CText;
      InsTExt      : string;
      h            : Pstring;
      doinsert     : longint;
      InsertTExt   : CText;
      hFOntIndex   : longint;
      helptext     : string;
      Pos          : TDPoint;
      ACRect       : TDREct;
  begin
      Vlist:=TPBucketLeaf(tree).VertexList;
      Vlist.LockPage(Vlist);
      if Vlist.count>0 then
      for i:=Vlist.count-1 downto 0 do begin

        AViewItem:=Vlist.Items(i);
        AView:=AViewItem.Get;
        if AView<>NIL then begin
          if Aview.GetObjtype=ot_Text then begin
            ATExt:=CTExt(AView);
            OTExt.Pos.x:=AText.Position.x;
            OText.Pos.y:=Atext.Position.y;
            OText.Angle:=ATExt.Angle;
            Otext.Width:=ATExt.Width;
            Otext.Font.Font:=AText.font.font;
            Otext.Font.Style:=Atext.font.style;
            Otext.Font.Height:=Atext.font.height;
            ACrect.InitByRect(AText.Cliprect);
            Instext:=ATExt.GetText;
            doinsert:=ot_text;
            {Otext.settext(Instext);}
          end;
        end;

        OldHeapMan:=BHeapManager;
        BHeapManager:=PProj(ConvertProj)^.AHeapManager;
        {Objekte einfuegen}
        INsertLayer.LockPage(InsertLayer);


        if doinsert=ot_text then begin
          helpText:='hallo';
          Pos.x:=Otext.pos.x;
          Pos.y:=Otext.pos.y;
          hFontIndex:=TConSym(GetConSym).GetNewFont(OText.Font.Font);
          OText.Font.font:=hFontIndex;
          InsertText:=CText.Create(Pos,Otext.Angle,Otext.Width,Otext.Font,helpText);
          TKernel(GetHeapMan.FKernel).MClass.LockPage(InsertText);
          TKernel(GetHeapMan.FKernel).MClass.Modified(InsertText);
          InsertText.Index:=LastText;
          Inc(LastText);
          if PProj(Proj)^.LogFile then writeln(LFile,InputFileName,HSP_LOGSEPERATOR,OText.Index,HSP_LOGSEPERATOR,LastText);
          InsertText.ClipRect.InitByRect(ACRect);
          helpTExt:=inserttext.gettext;
          InsertLayer.InsertObject2(InsertText);
          TKernel(GetHeapMan.FKernel).MClass.UnLockPage(InsertText);
        end;




        INsertLayer.UnLockPage(InsertLayer);

        BHeapManager:=OldHeapMan;

      end;

      Vlist.Unlockpage(Vlist);

  end;



Function TDocument.ImportProject(AX,AY:Longint):TRectItem;
 var BLayer    : PLayer;
      cnt      : Longint;
      cnt2     : Longint;
      cnt3     : longint;
      cnt4     : Longint;
      cnt5     : longint;
      AIndex   : PIndex;
      AView    : PView;
      APoly    : PPoly;
      ACPoly   : PCPoly;
      APoint   : PDPoint;
      APixel   : PPixel;
      AText    : PText;
      ARTExt   : PRText;
      ASym     : PSymbol;
      AMesLine : PMeasureLine;
      ASpline  : PSpline;

      InsertPoly  : CPoly;
      InsertCPoly : CCPoly;
      InsertText  : CText;
      InsertSym   : CSymbol;
      InsertPOint : CPixel;
      InsertMes   : CMeasureLine;
      InsertCirc  : CEllipse;
      InsertArc   : CArc;
      ACirc       : PEllipse;
      Arc         : PEllipseArc;
      InsertSpline: CSpline;
      NewPoint : CPoint;

      i        : longint;
      J        : longint;
      Statusdlg : Pointer;
      AClipRect : TDREct;
      Pos       : TDPoint;
      helpText  : string;
      hSymIndex : longint;
      hFontIndex: longint;
      AProj     : PProj;
      BLayers   : CLayers;

      p         : TSLBaseItem;
      help      : TSLBaseItem;
      ABLayer   : CLayer;
      DLayer    : CLayer;
      LayerName : string;
      AMemLst   : TMemlst;
      NewIndex  : TIndex;
      ObjIndex  : longint;
      Pmemlstitem : TMemlstItem;
      x12       : integer;
      AExtPtr   : TEXTPtr;

  begin
    AProj:=ConvertProj;
    {ConvertPRoj ... einzulesendes Projekt}
    {Proj        ... aktuelles Projekt}
    StatusDlg:=CreateAbortDlg(AProj^.Parent);
    RectItem := TRectItem.Create;

    XOffset:=Ax;  {0 fuer  einfach ohne Daten duplizieren}
    YOffset:=Ay;  {0 fuer  einfach}

    Allcount:=0;
    Docount:=0;
    cntxx:=0;
    if AProcess<>NIL then begin


      if  AProj^.Document.loaded then begin
        i:=0;
        BHeapManager:=AProj^.AHeapManager;
        BLayers:=AProj^.Document.Layers.Get;
        p:=BLayers.LayerList.GetHead;
        while p<>NIL do begin
          p.LockPage(p);
          ABLayer:=TLayerListItem(p).Layer;
          p.Lockpage(ABLayer);
          if ABLayer<>nil then begin

            LayerName:=ABLayer.GetTExt;
            OldHeapMan:=BHeapManager;
            BHeapManager:=Aproj^.Aheapmanager;

            DLayer:=CLayers(Layers.Get).NameToLayer(LayerName);
            if DLayer=NIL then begin
              CLayers(Layers.Get).Init(Proj,self);
              DLayer:=CLayers(Layers.Get).InsertLayer(Self,CLayers(Layers.Get).ClipRect,LayerName,ABLayer.color,
                ABLayer.LineStyle,ABLayer.Patcolor,ABLayer.Pattern,iltop,ABLayer.Index);
              TKernel(GetHeapMan.FKernel).MClass.Modified(CLayers(Layers.Get));
              TKernel(GetHeapMan.FKernel).MClass.Modified(DLayer);
              dLayer.SetGeneralMin(ABLayer.GeneralMin);
              dLayer.SetGeneralMax(ABLayer.GeneralMax);
              //if aBLayer.Getstate(sf_general) then dlayer.SetState(sf_general,true);
              if aBLayer.Getstate(sf_LayerOff) then dlayer.SetState(sf_LayerOff,true);
              if not aBLayer.Getstate(sf_LayerOff) then begin
                dlayer.SetState(sf_LayerOff,false);
                //dlayer.SetState(sf_HSDVisible,TRUE);
              end;
            end;
            InsertLayer:=DLayer;

            BHeapManager:=OldHeapMan;

            ConvertToWGP(ABLayer);

          end;
          p.Unlockpage(ABLayer);
          help:=p;
          p:=p.next;
          p.UnlockPage(help);
        end;
      end;

    BHeapManager:=PProj(Proj)^.AHeapManager;


      for i:=1 to AProj^.Layers^.Ldata^.count-1 do begin
        BLayer:=AProj^.Layers^.Ldata^.at(i);
        if BLayer<>NIL then begin
          Allcount:=Allcount+ BLayer^.Data^.GetCount;
          ALayer:=CLayers(Layers.Get).NameToLayer(BLayer^.Text^);
          if ALayer=NIL then begin
            ALayer:=CLayers(Layers.Get).InsertLayer(Self,CLayers(Layers.Get).ClipRect,BLayer^.Text^,0,
              0,0,0,iltop,BLayer^.Index);
            TKernel(GetHeapMan.FKernel).MClass.Modified(CLayers(Layers.Get));
            TKernel(GetHeapMan.FKernel).MClass.Modified(ALayer);
            //if BLayer^.Pattern > 7 then ALayer.Pattern:=TConSym(GetConSym).GetNewPat(BLayer^.Pattern);
            ALayer.SetGeneralMin(BLayer^.GeneralMin);
            ALayer.SetGeneralMax(BLayer^.GeneralMax);
            //if BLayer^.Getstate(sf_general) then Alayer.SetState(sf_general,true);
            if BLayer^.Getstate(sf_LayerOff) then Alayer.SetState(sf_LayerOff,true);
            if not BLayer^.Getstate(sf_LayerOff) then Alayer.SetState(sf_LayerOff,false);
            //ALayer.SetState(sf_HSDVisible,TRUE);
          end;
        end;
      end;

      {four each layer}
      {1!!! kein Ahnung was Layer 0 soll}
      for i:=1 to AProj^.Layers^.Ldata^.count-1 do begin
        BLayer:=AProj^.Layers^.Ldata^.at(i);
        if BLayer<>NIL then begin

          ALayer:=CLayers(Layers.Get).NameToLayer(BLayer^.Text^);
          if ALayer=NIL then begin
            raise elisterror.create('Layer NIL');
          {
            ALayer:=CLayers(Layers.Get).InsertLayer(Self,CLayers(Layers.Get).ClipRect,BLayer^.Text^,BLayer^.color,
            BLayer^.LineStyle,
              BLayer^.Patcolor,BLayer^.Pattern,iltop,BLayer^.Index);
            TKernel(GetHeapMan.FKernel).MClass.Modified(CLayers(Layers.Get));
            TKernel(GetHeapMan.FKernel).MClass.Modified(ALayer);
            if BLayer^.Pattern > 7 then ALayer.Pattern:=TConSym(GetConSym).GetNewPat(BLayer^.Pattern);
            ALayer.SetGeneralMin(BLayer^.GeneralMin);
            ALayer.SetGeneralMax(BLayer^.GeneralMax);
            if BLayer^.Getstate(sf_general) then Alayer.SetState(sf_general,true);
            if BLayer^.Getstate(sf_LayerOff) then Alayer.SetState(sf_LayerOff,true);
            if not BLayer^.Getstate(sf_LayerOff) then Alayer.SetState(sf_LayerOff,false);
            ALayer.SetState(sf_HSDVisible,TRUE);
            }
          end;

          TKernel(GetHeapMan.FKernel).MClass.LockPage(ALayer);
          {MemDataLst.AddItem(ALayer.Memdata);}


          AMemlst:=TMemlst.create;
          SortLayer(AProj,BLayer,AMemlst);
          {}
          {for cnt5:=0 to BLayer^.Data^.getcount-1 do begin
            AExtPtr:=TExtPtr(BLayer^.Data^.at(cnt5)^.Index);
            AMemLst.Add(AExtPtr);
          end; }
          {}

          {for cnt:=0 to BLayer^.Data^.GetCount-1 do begin}
          PMemLstItem:=AMemLst.Head;
          While pMemlstItem<>NIL do begin
            for cnt4:=0 to pMemlstItem.count-1 do begin
              {AIndex:=BLayer^.Data^.At(cnt);}
              ObjIndex:=longint(PMemLstItem.items[cnt4]);
              NewIndex.Index:=ObjIndex;
              AIndex:=BLayer^.HasObject(@NewIndex);
              if AIndex<>NIL then begin
                if AIndex<>NIL then AView:=BLayer^.IndexObject(ConvertInfo,AIndex);
                if AView<>NIL then begin
                  if cntxx=0 then begin
                    RectItem.InitbyTDRect(AView^.Cliprect);
                  end else RectItem.CorrectByTDRect(AView^.Cliprect);
                  inc(cntxx);
                  AclipREct.InitByRect(AView^.ClipRect);

                  AclipREct.a.x:=AView^.ClipRect.a.x+XOffset;
                  AclipREct.a.y:=AView^.ClipRect.a.y+YOffset;
                  AclipREct.b.x:=AView^.ClipRect.b.x+XOffset;
                  AclipREct.b.y:=AView^.ClipRect.b.y+YOffset;

                  inc(Docount);
                  //StatusDlg^.SetPercent(100*(docount/Allcount));
                  Statusbar.Progress:=100*(docount/Allcount);

                  if AView^.Getobjtype=ot_poly then begin
                    APoly:=PPoly(AView);
                    InsertPoly:=CPoly.Create(APoly^.Data^.count);
                    TKernel(GetHeapMan.FKernel).MClass.LockPage(InsertPoly);
                    TKernel(GetHeapMan.FKernel).MClass.Modified(InsertPoly);
                    {InsertPoly.Index:=AView^.Index;}

                    InsertPoly.Length:=APoly^.Laenge;
                    InsertPoly.InitObjectStyle(AIndex^.Objectstyle);

                    if PProj(Proj)^.DBUpdate then InsertPoly.Index:=LastPoly else InsertPoly.Index:=AView^.Index;
                    if AView^.Index>MaxLastPoly then MaxLastPoly:=Aview^.Index;   {without DBUpdate,  }
                    Inc(LastPoly);                                 {for DBUpdate, indizes new, projectindex =lastindex}
                    if PProj(Proj)^.LogFile then writeln(LFile,InputFileName,HSP_LOGSEPERATOR,AView^.Index,HSP_LOGSEPERATOR,InsertPoly.Index);
                    for cnt2:=0 to APoly^.Data^.Count-1 do begin
                      APoint:=APoly^.Data^.At(cnt2);
                      NewPoint.x:=APoint^.x + XOffset;
                      NewPoint.y:=APoint^.y + YOffset;
                      InsertPoly.InsertPoint(NewPoint);
                    end;
                    InsertPOly.ClipRect.InitByRect(AClipRect);
                    {ALayer.InsertObject(InsertPoly);}
                    ALayer.InsertObject2(InsertPoly);
                    TKernel(GetHeapMan.FKernel).MClass.UnLockPage(InsertPoly);
                  end  else

                  if AView^.Getobjtype=ot_Cpoly then begin
                    ACPoly:=PCPoly(AView);
                    InsertCPoly:=CCPoly.Create(ACPoly^.Data^.Count);
                    InsertCPoly.LockPage(InsertCPoly);
                    InsertCPoly.Modified(InsertCPoly);

                    {
                    TKernel(GetHeapMan.FKernel).MClass.LockPage(InsertCPoly);
                    TKernel(GetHeapMan.FKernel).MClass.Modified(InsertCPoly);
                    }
                    {InsertCPoly.Index:=AView^.Index;}
                    InsertCPoly.Area:=ACPoly^.Flaeche;
                    InsertCPoly.Length:=ACPoly^.Laenge;
                    InsertCPoly.InitObjectStyle(AIndex^.Objectstyle);

                    if PProj(Proj)^.DBUpdate then InsertCPoly.Index:=LastCPoly else InsertCPoly.Index:=AView^.Index;
                    if AView^.Index>MaxLastCPoly then MaxLastCPoly:=Aview^.Index;   {without DBUpdate,  }
                    Inc(LastCPoly);                                 {for DBUpdate, indizes new, projectindex =lastindex}

                    if PProj(Proj)^.LogFile then writeln(LFile,InputFileName,HSP_LOGSEPERATOR,AView^.Index,HSP_LOGSEPERATOR,InsertCPoly.Index);
                    for cnt2:=0 to ACPoly^.Data^.Count-1 do begin
                      APoint:=ACPoly^.Data^.At(cnt2);
                      NewPoint.x:=APoint^.x + XOffset;
                      NewPoint.y:=APoint^.y + YOffset;
                      InsertCPoly.InsertPoint(NewPoint);
                    end;
                    InsertCPOly.ClipRect.InitByRect(AClipRect);
                    InsertCPoly.SetSTate(ACpoly^.State,true);
                    if ACPoly^.GetState(sf_IslandArea) then begin

                      InsertCPoly.FIslandInfo.Put(
                      GetHeapMan.MAllocate(GetHeapMan.ActualPage,ACPoly^.IslandCount*sizeof(longint)));
                      InsertCPoly.IslandCount:=ACPoly^.IslandCount;
                      for cnt3:= 0 to InsertCPoly.Islandcount-1 do begin
                        InsertCPoly.IslandInfo^[cnt3]:=ACPoly^.IslandInfo^[cnt3];
                      end;
                    end;
                    {ALayer.InsertObject(InsertCPoly);}
                    ALayer.InsertObject2(InsertCPoly);
                    TKernel(GetHeapMan.FKernel).MClass.UnLockPage(InsertCPoly);
                  end else
                  if AView^.Getobjtype=ot_Text then begin
                    AText:=PText(AView);
                    if Atext^.TExt<>NIL then begin
                      helpText:=Atext^.Text^;

                      (*
                      if length(Atext^.Text^)>79 then {raise ElistError.create('dddd');} begin
                        helpText:=copy(ATExt^.Text^,0,79);
                        helpa:=length(helptext);
                      end;
                      *)
                      Pos.x:=Atext^.pos.x;
                      Pos.y:=Atext^.pos.y;
                      Pos.x:=Pos.x + XOffset;
                      Pos.y:=Pos.y + YOffset;
                    hFontIndex:=TConSym(GetConSym).GetNewFont(AText^.Font.Font);
                      AText^.Font.font:=hFontIndex;
                      InsertText:=CText.Create(Pos,Atext^.Angle,Atext^.Width,Atext^.Font,helpText);
                      TKernel(GetHeapMan.FKernel).MClass.LockPage(InsertText);
                      TKernel(GetHeapMan.FKernel).MClass.Modified(InsertText);
                      {InsertText.Index:=AView^.Index;}

                      if PProj(Proj)^.DBUpdate then InsertText.Index:=LastText else InsertText.Index:=AView^.Index;
                      if AView^.Index>MaxLastText then MaxLastText:=Aview^.Index;   {without DBUpdate,  }
                      Inc(LastText);                                 {for DBUpdate, indizes new, projectindex =lastindex}

                      if PProj(Proj)^.LogFile then writeln(LFile,InputFileName,HSP_LOGSEPERATOR,AView^.Index,HSP_LOGSEPERATOR,InsertText.Index);
                      InsertText.ClipRect.InitByRect(AClipRect);
                      InsertText.InitObjectStyle(AIndex^.Objectstyle);
                      {ALayer.InsertObject(InsertText);}
                      ALayer.InsertObject2(InsertText);
                      TKernel(GetHeapMan.FKernel).MClass.UnLockPage(InsertText);
                    end;
                  end else
                  if (AView^.Getobjtype=ot_Symbol) then begin
                    ASym:=PSymbol(AView);
                    Pos.x:=Asym^.Position.x;
                    Pos.y:=Asym^.Position.y;
                    Pos.x:=Pos.x + XOffset;
                    Pos.y:=Pos.y + YOffset;
                    hSymIndex:=TConSym(GetConSym).GetNewSym(Asym^.SymIndex);
                    InsertSym:=CSymbol.Create(ConvertInfo,Pos,hSymIndex,Asym^.Size,ASym^.Angle,ASym^.SizeType);
                    TKernel(GetHeapMan.FKernel).MClass.LockPage(InsertSym);
                    TKernel(GetHeapMan.FKernel).MClass.Modified(InsertSym);
                    {InsertSym.Index:=AView^.Index;}

                    if PProj(Proj)^.DBUpdate then InsertSym.Index:=LastSymbol else InsertSym.Index:=AView^.Index;
                    if AView^.Index>MaxLastSymbol then MaxLastSymbol:=Aview^.Index;   {without DBUpdate,  }
                    Inc(LastSymbol);                                 {for DBUpdate, indizes new, projectindex =lastindex}
                    InsertSym.InitObjectStyle(AIndex^.Objectstyle);


                    if PProj(Proj)^.LogFile then writeln(LFile,InputFileName,HSP_LOGSEPERATOR,AView^.Index,HSP_LOGSEPERATOR,Insertsym.Index);
                    InsertSym.ClipRect.InitByRect(AClipRect);
                    {ALayer.InsertObject(InsertSym);}
                    ALayer.InsertObject2(InsertSym);
                    TKernel(GetHeapMan.FKernel).MClass.UnLockPage(InsertSym);

                  end else
                  if AView^.Getobjtype=ot_Pixel then begin
                    APixel:=PPixel(AView);
                    Pos.x:=APixel^.Position.x;
                    Pos.y:=APixel^.Position.y;
                    Pos.x:=Pos.x + XOffset;
                    Pos.y:=Pos.y + YOffset;
                    InsertPoint:=CPixel.Create(Pos);
                    InsertPoint.InitObjectStyle(AIndex^.Objectstyle);
                    {InsertPoint.Index:=AView^.Index;}

                    if PProj(Proj)^.DBUpdate then InsertPoint.Index:=LastPixel else InsertPoint.Index:=AView^.Index;
                    if AView^.Index>MaxLastPixel then MaxLastPixel:=Aview^.Index;   {without DBUpdate,  }
                    Inc(LastPixel);                                 {for DBUpdate, indizes new, projectindex =lastindex}

                    if PProj(Proj)^.LogFile then writeln(LFile,InputFileName,HSP_LOGSEPERATOR,AView^.Index,HSP_LOGSEPERATOR,InsertPoint.Index);
                    InsertPoint.ClipRect.InitByRect(AClipRect);
                    {ALayer.InsertObject(InsertPoint);}
                    ALayer.InsertObject2(InsertPoint);
                  end else
                  if AView^.Getobjtype=ot_Circle then begin
                    ACirc:=PEllipse(AView);
                    InsertCirc:=CEllipse.Create(ACirc^.Position,ACirc^.PrimaryAxis,ACirc^.SecondaryAxis,ACirc^.Angle);
                    InsertCirc.ClipRect.InitByRect(AClipRect);
                    InsertCirc.InitObjectStyle(AIndex^.Objectstyle);
                    {InsertCirc.Index:=AView^.Index;}

                    if PProj(Proj)^.DBUpdate then Insertcirc.Index:=LastCircle else InsertCirc.Index:=AView^.Index;
                    if AView^.Index>MaxLastCircle then MaxLastCircle:=Aview^.Index;   {without DBUpdate,  }
                    Inc(LastCircle);                                 {for DBUpdate, indizes new, projectindex =lastindex}

                    if PProj(Proj)^.LogFile then writeln(LFile,InputFileName,HSP_LOGSEPERATOR,AView^.Index,HSP_LOGSEPERATOR,InsertCirc.Index);
                    ALayer.InsertObject2(Insertcirc);
                  end else
                  if AView^.Getobjtype=ot_Arc then begin
                    Arc:=PEllipseArc(AView);
                    InsertArc:=CArc.Create(Arc^.Position,Arc^.PrimaryAxis,Arc^.SecondaryAxis,Arc^.Angle,
                      Arc^.BeginAngle,Arc^.EndAngle);
                    InsertArc.ClipRect.InitByRect(AClipRect);
                    {InsertArc.Index:=AView^.Index;}
                    InsertArc.InitObjectStyle(AIndex^.Objectstyle);
                    if PProj(Proj)^.DBUpdate then InsertArc.Index:=LastARc else InsertArc.Index:=AView^.Index;
                    if AView^.Index>MaxLastARc then MaxLastArc:=Aview^.Index;   {without DBUpdate,  }
                    Inc(LastArc);                                 {for DBUpdate, indizes new, projectindex =lastindex}

                    if PProj(Proj)^.LogFile then writeln(LFile,InputFileName,HSP_LOGSEPERATOR,AView^.Index,HSP_LOGSEPERATOR,InsertArc.Index);
                    ALayer.InsertObject2(InsertArc);
                  end else
                  if AView^.Getobjtype=ot_Mesline then begin
                    AMesLine:=PMeasureLine(AView);
                    InsertMes:=CMeasureLine.Create(AMesLine^.StartPoint,AMesLine^.EndPoint);
                    InsertMes.SetSTate(AMesLine^.State,true);
                    InsertMes.ClipRect.InitByRect(AClipRect);
                    InsertMes.InitObjectStyle(AIndex^.Objectstyle);
                    {InsertMes.Index:=AView^.Index;}

                    if PProj(Proj)^.DBUpdate then InsertMes.Index:=LastMesLine else InsertMes.Index:=AView^.Index;
                    if AView^.Index>MaxLastMesLine then MaxLastMesLine:=Aview^.Index;   {without DBUpdate,  }
                    Inc(LastMesLine);                                 {for DBUpdate, indizes new, projectindex =lastindex}

                    if PProj(Proj)^.LogFile then writeln(LFile,InputFileName,HSP_LOGSEPERATOR,AView^.Index,HSP_LOGSEPERATOR,InsertMes.Index);
                    ALayer.InsertObject2(InsertMes);
                  end else
                  if AView^.Getobjtype=ot_spline then begin

                    ASpline:=PSpline(AView);
                    InsertSpline:=CSpline.Create(ASpline^.Data^.count);
                    TKernel(GetHeapMan.FKernel).MClass.LockPage(InsertSpline);
                    TKernel(GetHeapMan.FKernel).MClass.Modified(InsertSpline);
                    {InsertSpline.Index:=AView^.Index;}
                    InsertSpline.InitObjectStyle(AIndex^.Objectstyle);

                     if PProj(Proj)^.DBUpdate then InsertSpline.Index:=LastSpline else InsertSpline.Index:=AView^.Index;
                    if AView^.Index>MaxLastSpline then MaxLastSpline:=Aview^.Index;   {without DBUpdate,  }
                    Inc(LastSpline);                                 {for DBUpdate, indizes new, projectindex =lastindex}

                    if PProj(Proj)^.LogFile then writeln(LFile,InputFileName,HSP_LOGSEPERATOR,AView^.Index,HSP_LOGSEPERATOR,InsertSpline.Index);
                    for cnt2:=0 to ASpline^.Data^.Count-1 do begin
                      APoint:=ASpline^.Data^.At(cnt2);
                      NewPoint.x:=APoint^.x + XOffset;
                      NewPoint.y:=APoint^.y + YOffset;
                      InsertSpline.InsertPoint(NewPoint);
                    end;
                    InsertSpline.ClipRect.InitByRect(AClipRect);
                    ALayer.InsertObject2(InsertSpline);
                    TKernel(GetHeapMan.FKernel).MClass.UnLockPage(InsertSpline);
                  end else if Aview^.Getobjtype=(ot_image) then begin
                      InsertBitMapOnRefLayer(AView,BLayer);
                    end
                    else if Aview^.Getobjtype=(ot_busgraph) then begin
                      InsertBitMapOnRefLayer(AView,BLayer);
                    end
                    else if Aview^.Getobjtype=(ot_RText) then begin
                      InsertBitMapOnRefLayer(AView,BLayer);
                    end;

                  begin
                  end;
                end; {Aview<>NIL}
              end; {AINdex <>NIL}
            end;  {for cnt4:=0 to pmemlstitem.coount-1}
            pMemLstItem:=PMemLstItem.next;
          end;   {all layeritems}
          TKernel(GetHeapMan.FKernel).MClass.UnLockPage(ALayer);
          AMemLst.Free;
        end; {BLayer<>NIL}
      end;  {for each layer}
    end;  {Aprocess<>NIL}
    //Dispose(StatusDlg,Done);
    REsult:=RectItem;
  end;

Procedure TDocument.ImportFromSelLayer;
  var BLayer : PLayer;
      cnt    : Longint;
      cnt2   : Longint;
      AIndex : PIndex;
      AView  : PView;
      APoly  : PPoly;
      ACPoly : PCPoly;
      APoint : PDPoint;
      APixel : PPixel;
      AText  : PText;
      ASym   : PSymbol;

      InsertPoly  : CPoly;
      InsertCPoly : CCPoly;
      InsertText  : CText;
      InsertSym   : CSymbol;
      InsertPOint : CPixel;

      NewPoint : CPoint;
  begin
 (*  SetClipRect(CLayers(Layers.Get).ClipRect,AProj^.Size.A.x,AProj^.Size.B.x,
    AProj^.Size.A.y,AProj^.Size.B.y);
    ALayer:=CLayer.Create(CLayers(Layers.Get).clipRect,Self);
    AProcess.SetSTart(ALayer);

    BLayer:=AProj^.CLayers(Layers.Get)^.SelLayer;
    if BLayer^.Data^.Getcount>0 then
    for cnt:=0 to BLayer^.Data^.Getcount-1 do begin
      AIndex:=BLayer^.Data^.At(cnt);
      if AIndex<>NIL then AView:=BLayer^.IndexObject(PInfo,AIndex);
      if AView<>NIL then begin
        if AView^.Getobjtype=ot_poly then begin
          APoly:=PPoly(AView);
          InsertPoly:=CPoly.Create(APoly^.Data^.count);
          for cnt2:=0 to APoly^.Data^.Count-1 do begin
            APoint:=APoly^.Data^.At(cnt2);
            NewPoint.x:=APoint^.x;
            NewPoint.y:=APoint^.y;
            InsertPoly.InsertPoint(NewPoint);
          end;
          {initcliprect}
          ALayer.InsertObject(InsertPoly);
        end else
        if AView^.Getobjtype=ot_Cpoly then begin
          ACPoly:=PCPoly(AView);
          InsertCPoly:=CCPoly.Create(ACPoly^.Data^.count);
          for cnt2:=0 to ACPoly^.Data^.Count-1 do begin
            APoint:=ACPoly^.Data^.At(cnt2);
            NewPoint.x:=APoint^.x;
            NewPoint.y:=APoint^.y;
            InsertCPoly.InsertPoint(NewPoint);
          end;
          {initcliprect}
          ALayer.InsertObject(InsertCPoly);
        end else
        if AView^.Getobjtype=ot_Text then begin
          AText:=PText(AView);
          InsertText:=CText.Create(Atext^.Pos,Atext^.Angle,Atext^.Width,Atext^.Font,Atext^.Text^);
          ALayer.InsertObject(InsertText);
        end else
        if AView^.Getobjtype=ot_Symbol then begin
          ASym:=PSymbol(AView);
          InsertSym:=CSymbol.Create(PInfo,Asym^.Position,Asym^.SymIndex,Asym^.ScaleFact,ASym^.Angle);
          ALayer.InsertObject(InsertSym);
        end else
        if AView^.Getobjtype=ot_Pixel then begin
          APixel:=PPixel(AView);
          InsertPoint:=CPixel.Create(APixel^.Position);
          ALayer.InsertObject(InsertPoint);
        end else
        if AView^.Getobjtype=ot_Circle then begin
        end;
      end;
    end;
   *)
  end;



Procedure TDocument.OnStore(AfileName:string);
  var MInfo : CPaint;
      AID   : LongInt;
  begin
 {  GetHeapMan.SetKernel;}
   {AProcess.OnStore(AfileName,1);}
   {AProcess.OnNew(AFileName);}
   AId:=Random(64000);
   PutCheckID(AID);
   PPRoj(Proj)^.CheckID:=AID;

   isBaseMap:=false;
   isextmap:=true;
   PProj(Proj)^.BaseMap:=isBaseMap;
   PProj(Proj)^.ExtMap:=isExtMap;
   CProj(Project).Size.a.x:=round(GetHeapMan.TCRList.Allsize.left);
   CProj(Project).Size.b.x:=round(GetHeapMan.TCRList.Allsize.right);
   CProj(Project).Size.a.y:=round(GetHeapMan.TCRList.Allsize.bottom);
   CProj(Project).Size.b.y:=round(GetHeapMan.TCRList.Allsize.top);

   {PInfo:=GetHeapMan.PInfo;}

   MInfo:=CProj(Project).PInfo.Get;

   MInfo.Drawmode:=pinfo^.Drawmode;
   MInfo.ClipRect:=pinfo^.clipRect;
   MInfo.PreViewing:=pinfo^.PreViewing;
   MInfo.ScreenWidth:=pinfo^.ScreenWidth;
   MInfo.Screenheight:=pinfo^.ScreenHeight;
   MInfo.Winwidth:=pinfo^.WinWidth;
   //MInfo.REPaintArea:=pinfo^.RePaintArea;


   PutObjCount(GetHeapMan.LastIndex);
   if AProcess.FileMan<>NIL then begin
     AProcess.OnClose;
   end;
   GetHeapMan.SetProcess(AProcess);
   {
   if AProcess<>nil then begin
      GetHeapMan.SetKernel;
      AProcess.Free;
      AProcess:=NIL;
      ALayer:=NIL;
    end;
   }
  end;


Procedure TDocument.OnUpdate(AfileName:string);
  var MInfo : CPaint;

  begin

   if AProcess.FileMan<>NIL then begin
     AProcess.OnUpdate;
   end;
   GetHeapMan.SetProcess(AProcess);

  end;

PRocedure TDocument.CreateBaumLayer(AProj:POinter;Layername:string);
 var
      LayerIndex   : Longint;
      ALayer       : PLayer;
      BLayer       : CLayer;
      APtr         : TExtPtr;
Procedure DrawBaum
   (
   tree            : TPBucketBase
   );
  var APoly   : PPoly;
      APOint  : TDPoint;
      NText   : PText;
      BProj   : PProj;
      VListCount : Integer;
      bnum    : integer;
      vlcount : integer;
  begin
    BProj:=PProj(AProj);
    APoly:=new(PPoly,Init);
    VLCount:=TPBucketLeaf(Tree).Vertexlist.Count;  {Layeref, tats. Referenzen}
    VListCount:=TPBucketLeaf(Tree).VertexCount;    {durch split}
    bnum:=TPBucketLeaf(Tree).BucketNumber;
    with tree.ClipRect do begin
      APoint.Init(trunc(Left),trunc(Bottom));
      APoly^.InsertPoint(APOint);
      APoint.Init(trunc(Right),trunc(Bottom));
      APoly^.InsertPoint(APOint);
      APoint.Init(trunc(Right),trunc(Top));
      APoly^.InsertPoint(APOint);
      APoint.Init(trunc(Left),trunc(Top));
      APoly^.InsertPoint(APOint);
      APoint.Init(trunc(Left),trunc(Bottom));
      APoly^.InsertPoint(APOint);
      NText:=New(PText,Init(Bproj^.ActualFont,inttostr(VListCount)+'; '+inttostr(VLCount),0));
      NText^.width:=(round((right-left)/4));
      NText^.font.height:=(round((top-bottom)/4));
      Ntext^.pos.init(round(left+(right-left)/2),round(bottom+(top-bottom)/2));
      Ntext^.angle:=360;
      NTExt^.calculateCliprect(BProj^.PInfo);
    end;
    PProj(APRoj)^.InsertObjectOnLayer(APoly,ALayer^.Index);
    PProj(APRoj)^.InsertObjectOnLayer(NText,ALayer^.Index);
  end;


PRocedure DoTree
   (
   tree            : TPBucketBase
   );
  var acount : integer;
  begin
    if tree is TPBucketNode then with tree as tPbucketnode do begin
      doTree(sons[0]);
      doTree(sons[1]);
      doTree(sons[2]);
      doTree(sons[3]);
    end
    else begin
      acount:=TPBucketLeaf(Tree).VertexCount;
      DrawBaum(Tree);
    end;
  end;

  begin
    ALayer:=PProj(AProj)^.Layers^.NameToLayer('QuadTree'+LayerName);
    {if ALayer=NIL then ALayer:=PProj(AProj)^.Layers^.InsertLayer('QuadTree'+LayerName,
    $00FF0000,lt_Solid,$000000FF,pt_Solid,ilTop);}
    APtr:=CLayers(PProj(AProj)^.Document.Getlayers).GetlayerExtPtr(LayerName);
    BLayer:=APtr.Get;
    DoTree(BLayer.pTree);
  end;





Procedure TDocument.TreeSkeletonInsertData
   (
   ARefNode      : TBucketBase;
   AInsNode      : TPBucketBase
   );
  var cnt        : longint;
      help       : TClipRect;
  begin
    TKernel(GetHeapMan.FKernel).MClass.LockPage(AInsNode);
    for cnt:=0 to 3 do begin
      if TBucketNode(ARefNode).IsNode[Cnt] then begin
        {help:=TBucketNode(ARefNode).sons[Cnt].ClipRect;
        {TPBucketNode(AInsNode).Sons[cnt]:=TPBucketNode.Create(help,TPBucketNode(AInsParent),NIL);}
        TreeSkeletonInsertData(TBucketNode(ARefNode).Sons[cnt],TPBucketNode(AInsNode).Sons[cnt]);
      end else begin
        {help:=TBucketNode(ARefNode).sons[Cnt].ClipRect;
        {TPBucketNode(AInsNode).Sons[Cnt]:=TPBucketLeaf.Create(help,TPBucketNode(AInsParent),NIL);}
        CopyTreeData(TBucketLeaf(TBucketNode(ARefNode).sons[Cnt]),TPBucketLeaf(TPBucketNode(AInsNode).Sons[Cnt]));
      end;
    end;
  end;

Procedure TDocument.BaumConvert
   (
   ANode        : TBucketBase;
   AInsNode     : TPBucketBase;
   AInsParent   : TPBucketBase
   );
  var cnt        : Longint;
      help       : TClipRect;
  begin
    for cnt:=0 to 3 do begin
      if TBucketNode(ANode).IsNode[Cnt] then begin
        help:=TBucketNode(ANode).sons[Cnt].ClipRect;
        TPBucketNode(AInsNode).Sons[cnt]:=TPBucketNode.Create(help,TPBucketNode(AInsParent),NIL);
        BaumConvert(TBucketNode(ANode).Sons[cnt],TPBucketNode(AInsNode).Sons[cnt],AInsParent);
      end else begin
        help:=TBucketNode(ANode).sons[Cnt].ClipRect;
        TPBucketNode(AInsNode).Sons[Cnt]:=TPBucketLeaf.Create(help,TPBucketNode(AInsParent),NIL);
        CopyTreeData(TBucketLeaf(TBucketNode(ANode).sons[Cnt]),TPBucketLeaf(TPBucketNode(AInsNode).Sons[Cnt]));
      end;
    end;
  end;

PRocedure TDocument.CopyTreeData
   (
   ALeaf    : TBucketLeaf;
   AInsLeaf : TPBucketLeaf
   );
  var cnt       : longint;
      VertexITem : TPVertexItem;
      VItem     : TVertexITem;
      AView     : CView;
      a         : boolean;
      pn        : longint;
      InsItem   : TInsNode;
  begin
    if test mod 10 = 0 then begin
      a:=false;
    end;

    {
    AInsLeaf.VertexList:=TSLBase.Create;
    InsItem:=TInsNode.Create(ALeaf.VertexList,AInsLeaf.VertexList);
    MBucketList.Add(InsITem);
     }
  end;

PRocedure TDocument.ConvertFTreeToFPTree;
  begin
    if AProcess<>NIL then begin
      GetHeapMan.SetProcess(AProcess);

    end;
  end;


Procedure TDocument.Draw;
  var help : TPBucketBase;
      lstr : string;
      hstr : string;
      abort: boolean;
      SCount: longint;
  begin

   abort:=false;
   SCount:=GetHeapMan.Swapincount;
   GetHeapMan.Swapincount:=0;
   SCount:=GetHeapMan.Swapincount;

   if not abort then begin
    if CLayers(Layers.Get)<>NIL then begin
      {CLayers(Layers.Get).TraverseAllLayers;}
      CLayers(Layers.Get).Draw(GetHeapMan.PInfo);
      TKernel(GetHeapMan.FKernel).MClass.UnlockAll;
    end;
   end;
  end;

PRocedure TDocument.DrawByQuery;
  begin
    if (ALayer<>NIL)and(ALayer.Tree<>NIL) then ALayer.DrawByQuery(PInfo,ALayer.Tree);
  end;

Function TDocument.getLayer:CLayer;
  begin
    {Result:=FLayer.Get;}
    Result:=ALayer;
  end;
  
Function TDocument.Import:TRectItem;
  var i      : longint;
      j      : longint;
     { heigth : longint;
      width  : longint;
      a      : longint;
      b      : longint;}
  begin
    {* File duplizieren *}
    {width:=PProj(Proj)^.Size.b.x-PProj(Proj)^.Size.a.x;
    heigth:=PProj(Proj)^.Size.b.y - PProj(Proj)^.Size.a.y;
    PProj(Proj)^.Size.b.x:=PProj(Proj)^.Size.a.x + (b+1)*width+100;
    PProj(Proj)^.Size.b.y:=PProj(Proj)^.Size.a.y + (a+1)*heigth+100;}
    {*}
    for i:=0 to a do begin     {zeilen, height}
      for j:=0 to b do begin    {spalten, width}
        Result:=ImportProject(j*width,i*heigth); {x,y}
      end;
    end;
   { PProj(Proj)^.Size.b.x:=PProj(Proj)^.Size.a.x + j*width;
    PProj(Proj)^.Size.a.y:=PProj(Proj)^.Size.a.y + i*heigth;}
  end;

Function TDocument.GetLayers:CLayers;
  begin
    Result:=CLayers(Layers.Get);
  end;


Procedure   TDocument.DeSelectAll(ATextOut:Boolean);
  var i      : longint;
      AItem  : TSelItem;
      ALeaf  : TPBucketLeaf;
      AvertexItem : TSLBaseITem;
      Help        : TSLBaseITem;
      AView       : CView;
      Vlist       : TLayRefLst;
      AViewItem   : TExtPtr;
      j           : longint;
      ycount      : longint;
  begin

    ycount:=TGlobal(GetheapMan.GlobalObj).SelLst.SelectCount;
    for i:=0 to TSelList(GetHeapMan.SelList).Count-1 do begin
      AItem:=TSelList(GetHeapMan.SelList).Items[i];
      ALeaf:=TPBucketLeaf(AItem.Mptr.Get);

      {traverseleaf}

      Vlist:=TPBucketLeaf(ALeaf).VertexList;
      Vlist.lockpage(Vlist);
      if Vlist.count>0 then
      for j:=Vlist.count-1 downto 0 do begin
        AViewItem:=Vlist.Items(j);
        AView:=AViewItem.Get;
        if AView<>NIL then begin
          if TGlobal(GetheapMan.GlobalObj).SelLst.issel(AView.index2) then
            TGlobal(GetheapMan.GlobalObj).SelLst.Deletesel(AView.index2);
        end;
      end;
      Vlist.unlockpage(Vlist);

    end;
    {if ATextOut then GetHeapMan.Pinfo^.RedrawScreen(true);}
    GetHeapMan.Pinfo^.RedrawRect(Selrect);

    {SelListe loeschen}
    for i:=0 to TSelList(GetHeapMan.SelList).Count-1 do begin
      AItem:=TSelList(GetHeapMan.SelList).Items[i];
      AItem.Free;
    end;
    ycount:=TGlobal(GetheapMan.GlobalObj).SelLst.SelectCount;
     TSelList(GetHeapMan.SelList).Clear;

    SelREct.Assign(0,0,0,0);
    ClearStatus;
    SelCount:=0;

  end;


Procedure TDocument.AddIndex;
  var i      : Longint;
      j      : Longint;
      AItem  : TSelItem;
      ALeaf  : TPBucketLeaf;
      AvertexItem : TSLBaseITem;
      Help        : TSLBaseITem;
      AView       : CView;

      VList        : TlayReflst;
      AViewItem    : TExtPtr;
      AviewItem2   : TExtPtr2;
  begin
    for i:=0 to TSelList(GetHeapMan.SelList).Count-1 do begin
      AItem:=TSelList(GetHeapMan.SelList).Items[i];
      ALeaf:=TPBucketLeaf(AItem.Mptr.Get);
      Vlist:=TPBucketLeaf(ALeaf).VertexList;
      TKernel(GetHeapMan.FKernel).MClass.LockPage(Vlist);
      if Vlist.count>0 then
      for j:=Vlist.count-1 downto 0 do begin
        AViewItem:=Vlist.Items(j);
        AView:=AViewItem.Get;
        if AView<>NIL then begin
          if TGlobal(GetheapMan.GlobalObj).SelLst.isSel(AView.index2) then begin
            {$IFNDEF WMLT}
               {$IFNDEF AXDLL} // <----------------- AXDLL
            DDEHandler.AppendList(AView.Index);
               {$ENDIF} // <----------------- AXDLL
            {$ENDIF}
          end;
        end;
      end;
      TKernel(GetHeapMan.FKernel).MClass.UnLockPage(Vlist);
    end;
  end;


Procedure TDocument.AddIndex1(ALength:integer);
  var i      : Longint;
      j      : Longint;
      AItem  : TSelItem;
      ALeaf  : TPBucketLeaf;
      AvertexItem : TSLBaseITem;
      Help        : TSLBaseITem;
      AView       : CView;

      VList        : TlayReflst;
      AViewItem    : TExtPtr;
      AviewItem2   : TExtPtr2;
  begin
    for i:=0 to TSelList(GetHeapMan.SelList).Count-1 do begin
      AItem:=TSelList(GetHeapMan.SelList).Items[i];
      ALeaf:=TPBucketLeaf(AItem.Mptr.Get);
      Vlist:=TPBucketLeaf(ALeaf).VertexList;
      TKernel(GetHeapMan.FKernel).MClass.LockPage(Vlist);
      if Vlist.count>0 then
      for j:=Vlist.count-1 downto 0 do begin
        AViewItem:=Vlist.Items(j);
        AView:=AViewItem.Get;
        if AView<>NIL then begin
          if TGlobal(GetheapMan.GlobalObj).SelLst.isSel(AView.index2) then begin
            {$IFNDEF WMLT}
               {$IFNDEF AXDLL} // <----------------- AXDLL
            DDEHandler.AppendList1(AView.Index,Alength);
               {$ENDIF} // <----------------- AXDLL
            {$ENDIF}
          end;
        end;
      end;
      TKernel(GetHeapMan.FKernel).MClass.UnLockPage(Vlist);
    end;
  end;

Procedure   TDocument.Monitoring;
{$IFNDEF WMLT}
  var i      : Longint;
      j      : Longint;
      AItem  : TSelItem;
      ALeaf  : TPBucketLeaf;
      AvertexItem : TSLBaseITem;
      Help        : TSLBaseITem;
      AView       : CView;

      VList        : TlayReflst;
      AViewItem    : TExtPtr;
      AviewItem2   : TExtPtr2;
      ADATa        : PProj;
      NameLength   : Integer;
      IndexStr     : String;
      Atext        : STring;
{$ENDIF}
  begin
{$IFNDEF WMLT}
     if TGlobal(GetheapMan.GlobalObj).SelLst.SelectCount>0 then begin
      AtExt:='MON';
      AData:=PProj(Proj);
      {$IFNDEF AXDLL} // <----------------- AXDLL
      if DDEHandler.FLayerInfo=0 then begin
        DDEHandler.StartList('['+Atext+'][   ]');
        AddIndex;
        DDEHandler.EndList(TRUE);
      end else if DDEHandler.FLayerInfo=1 then begin
       if not AData^.Layers^.TranspLayer then begin
          NameLength:=Length(AData^.Layers^.TopLayer^.Text^);
          DDEHandler.StartList('['+AText+']['+AData^.Layers^.TopLayer^.Text^+'][   ]');
          AddIndex1(NameLength);
          DDEHandler.EndList1(TRUE,NameLength);
        end
      end
      else if DDEHandler.FLayerInfo=2 then begin
        NameLength:=10;
        if not AData^.Layers^.TranspLayer  then begin
          Str(AData^.Layers^.TopLayer^.Index:10,IndexStr);
          DDEHandler.StartList('['+AText+']['+IndexStr+'][   ]');
          AddIndex1(NameLength);
          DDEHandler.EndList1(TRUE,NameLength);
        end;
      end;
      {$ENDIF} // <----------------- AXDLL
      {PProj(Proj)^.Pinfo^.RedrawScreen(true);}
    end;
{$ENDIF}
  end;

Function    TDocument.SelectByPoint(Point:TDPoint;ObjType:TObjectTypes):Pointer;
var p       : TSLBaseItem;

    help    : TSLBaseItem;
    cnt     : longint;
    ALayer  : CLayer;
    ARect   : TDRect;
    Redraw  : Boolean;
  begin

    Redraw:=false;
    if ActualLayer = '' then begin
      p:=CLayers(Layers.Get).LayerList.GetHead;
      if p.next<>NIL then p:=p.next;
      ALayer:=TLayerListItem(p).Layer;
      ActualLayer:=ALayer.GetText;
    end;
    p:=CLayers(Layers.Get).LayerList.GetHead;
    while p<>NIL do begin
      inc(cnt);
      TKernel(GetHeapMan.FKernel).MClass.LockPage(p);
      ALayer:=TLayerListItem(p).Layer;
      TKernel(GetHeapMan.FKernel).MClass.LockPage(ALayer);
      if (ALayer<>NIL)then begin

        if Ansiuppercase(ALayer.GetText)=Ansiuppercase(ActualLayer) then
           Redraw:=ALayer.SelectByPoint(Self,Point,ot_any);
        
      end;
      TKernel(GetHeapMan.FKernel).MClass.UnLockPage(ALayer);
      help:=p;
      p:=p.next;
      TKernel(GetHeapMan.FKernel).MClass.UnLockPage(help);
    end;
    TKernel(GetHeapMan.FKernel).MClass.UnLockAll;

    if Redraw then PInfo^.RedrawRect(GetHeapMan.RedrawRect);
    {PInfo^.RedrawScreen(true);}
  end;


Function    TDocument.SelectByRect(Rect:TDRect;Inside:Boolean):Boolean;
var p       : TSLBaseItem;
    help    : TSLBaseItem;
    cnt     : longint;
    ALayer  : CLayer;
    ycount  : longint;
    outstr  : string;
  begin

    p:=CLayers(Layers.Get).LayerList.GetHead;
    LastSelRect.Init;
    while p<>NIL do begin
      inc(cnt);
      TKernel(GetHeapMan.FKernel).MClass.LockPage(p);
      ALayer:=TLayerListItem(p).Layer;
      TKernel(GetHeapMan.FKernel).MClass.LockPage(ALayer);
      if (ALayer<>NIL)then begin
        if AnsiCompareText(ALayer.GetText,ActualLayer)=0 then
            ALayer.SelectByRect(Self,Rect,inside);
      end;
      TKernel(GetHeapMan.FKernel).MClass.UnLockPage(ALayer);
      help:=p;
      p:=p.next;
      TKernel(GetHeapMan.FKernel).MClass.UnLockPage(help);
    end;
    Pinfo^.Redrawrect(LastSelRect);
    ycount:=TGlobal(GetheapMan.GlobalObj).SelLst.SelectCount;
    outstr:=inttostr(ycount)+' '+getlangtext(4735);
    STatusbar.ProgressText:=outstr;
    //StatusLine^.SetText2(id_Line1,outstr,false);
   { PInfo^.RedrawScreen(true);}

  end;

PRocedure TDocument.ClearDrawBit;
var p       : TSLBaseItem;
    help    : TSLBaseItem;
    cnt     : longint;
    ALayer  : CLayer;
    ARect   : TDRect;
  begin
    p:=CLayers(Layers.Get).LayerList.GetHead;
    while p<>NIL do begin
      inc(cnt);
      TKernel(GetHeapMan.FKernel).MClass.LockPage(p);
      ALayer:=TLayerListItem(p).Layer;
      TKernel(GetHeapMan.FKernel).MClass.LockPage(ALayer);
      if (ALayer<>NIL)then begin
        ALayer.ClearDrawBit(ALayer.ptree);
      end;
      TKernel(GetHeapMan.FKernel).MClass.UnLockPage(ALayer);
      help:=p;
      p:=p.next;
      TKernel(GetHeapMan.FKernel).MClass.UnLockPage(help);
    end;
    TKernel(GetHeapMan.FKernel).MClass.UnLockAll;

  end;


PRocedure TDocument.PutCheckId(AId:Longint);
  begin
    AProcess.PutCheckId(AId);
  end;

Procedure TDocument.PutObjCount(ACount:Longint);
  begin
    AProcess.PutObjCount(ACount);
  end;

Function  TDocument.GetObjCount:Longint;
  begin
    Result:=Aprocess.GetObjCount;
  end;

Procedure TDocument.REsetMemLayers;
  begin
    CLayers(Layers.Get).Resetmemlayers;
  end;

Function    TDocument.GetLayerExtPtr(Atext:string):TExtPtr2;
  var APtr : TExtPtr;
  begin
    APtr:=CLayers(Layers.Get).GetlayerExtPtr(Atext);
    REsult:=TextPtr2(APtr);
  end;

Function    TDocument.GetLayerByName(Atext:string):Pointer;
  var APtr : TExtPtr;
  begin
    APtr:=CLayers(Layers.Get).GetlayerExtPtr(Atext);
    Result:=APtr.Get;
  end;

Procedure TDocument.DoDbIndizes;
var cnt2    : integer;
    APtr    : Pointer;
    p       : TSLBaseItem;
    help    : TSLBaseItem;
    cnt     : longint;
    ALayer  : CLayer;
  begin
    PProj(Proj)^.AMemDAtaLst.Clear;

    p:=CLayers(Layers.Get).LayerList.GetHead;
    while p<>NIL do begin
      inc(cnt);
      TKernel(GetHeapMan.FKernel).MClass.LockPage(p);
      ALayer:=TLayerListItem(p).Layer;
      TKernel(GetHeapMan.FKernel).MClass.LockPage(ALayer);
      if (ALayer<>NIL)then begin
        PProj(Proj)^.AMemDataLst.AddItem(Alayer.MemData,ALayer.GetText);
      end;
      TKernel(GetHeapMan.FKernel).MClass.UnLockPage(ALayer);
      help:=p;
      p:=p.next;
      TKernel(GetHeapMan.FKernel).MClass.UnLockPage(help);
    end;

    PProj(Proj)^.AMemDataLst.CreateDbIndizes(GetLayers);
  end;

Procedure TDocument.ReAssignMemdata;
var cnt2    : integer;
    APtr    : Pointer;
    p       : TSLBaseItem;
    help    : TSLBaseItem;
    cnt     : longint;
    ALayer  : CLayer;
    AItem   : TMemDataLstItem;
  begin
    cnt2:=0;
    p:=CLayers(Layers.Get).LayerList.GetHead;
    while p<>NIL do begin
      inc(cnt);
      TKernel(GetHeapMan.FKernel).MClass.LockPage(p);
      ALayer:=TLayerListItem(p).Layer;
      TKernel(GetHeapMan.FKernel).MClass.LockPage(ALayer);
      if (ALayer<>NIL)then begin
        AItem:=PProj(Proj)^.AMemDataLst[cnt2];
        ALayer.MemData:=AItem.MemLst;
        {ALayer.Modified(ALayer);}
      end;
      inc(cnt2);
      TKernel(GetHeapMan.FKernel).MClass.UnLockPage(ALayer);
      help:=p;
      p:=p.next;
      TKernel(GetHeapMan.FKernel).MClass.UnLockPage(help);
    end;
  end;


PRocedure   Tdocument.INsertLayersHspHsd(BData,BNewData:POinter;hspList,hsdlist:TSTringLIst);
var
  ALayer : PLayer;
  MLayer : PLayer;
  MIndex : PIndex;
  cnt    : integer;
  APtr   : TExtPtr2;
  a      : integer;
  AData  : PProj;
  ANewData : PProj;
  i        : longint;
  Aname    : string;
  AView    : PView;
  AIndex   : PIndex;
  j        : longint;
  CopyView : PView;
  hsymindex : longint;
  hfontindex : longint;
  oldtop     : PLayer;
  cntall     : longint;
  INsLayer   : PLayer;
  Afill      : TSymbolfill;
  Lcount     : integer;
  LCount2    : integer;
  LIndex     : integer;
  Lindex2    : integer;
  sname      : string;


  begin
    {FileList   hsd List    wgp Teil}
    {hspList    amp Teil}
    {einzulesenes Projekt}
    AData:=PProj(BData);
    {das gerade neue erzeugte Projekt}
    ANewData:=PProj(BNewData);

    {hsp}

    LCount:=ANewData^.Layers^.LData^.count;
    LIndex:=ANewData^.Layers^.LastIndex;
    sname:=PLayer(ANewDAta^.Layers^.LDAta^.at(1))^.Text^; {default layer name von neuem Projekt}

    if hspList<>NIL then
    for i:=0 to hspList.count-1 do begin

      Aname:=hspList.strings[i];
      ALayer:=AData^.Layers^.NameToLayer(Aname);
      if Alayer<>NIL then begin

        MLayer:=ANewData^.Layers^.NameToLayer(ALayer^.TExt^);
        if MLayer= NIL then begin
          ANewData^.Layers^.LastIndex:=Alayer^.Index-1;
          MLayer:=AnewData^.Layers^.InsertLayerByReference(Alayer,ilLast);
          MLayer^.SymbolFill.Symbol:=TConSym(GetConSym).GetNewSym(ALayer^.SymbolFill.Symbol);
        end;

        {
        if ALayer^.Getstate(sf_general) then Mlayer^.SetState(sf_general,true);
        if ALayer^.Getstate(sf_LayerOff) then begin
          Mlayer^.SetState(sf_LayerOff,true);
          MLayer^.SetState(sf_HSDVisible,false);
        end else
        if not ALayer^.Getstate(sf_LayerOff) then begin
          Mlayer^.SetState(sf_LayerOff,false);
          MLayer^.SetState(sf_HSDVisible,TRUE);
        end;
         }
        OldTop:=ANewDAta^.Layers^.TopLayer;
        ANewDAta^.Layers^.TopLayer:=MLayer;
        for j:=0 to ALayer^.Data^.GetCount-1 do begin
          AIndex:=ALayer^.Data^.at(j);
          AView:=ALayer^.IndexObject(AData^.PInfo,Aindex);
          CopyView:=MakeObjectCopy(AView);
          if MLayer<>NIL then begin

            {
            PLayer(AnewData^.PInfo^.Objects)^.InsertObject(AnewData^.PInfo,CopyView,true);
            MLayer^.InsertObject(ANewData^.Pinfo,CopyView,false);
            }
            if RectItem.empty then RectItem.InitbyTDREct(CopyView^.Cliprect)
            else RectItem.CorrectByTDRect(CopyView^.Cliprect);
            if CopyView^.GetObjtype=ot_symbol then begin
              hSymIndex:=TConSym(GetConSym).GetNewSym(PSymbol(CopyView)^.SymIndex);
              Psymbol(CopyView)^.SymIndex:=hSymIndex;
            end;
            if CopyView^.GetObjtype=ot_text then begin
              hFontIndex:=TConSym(GetConSym).GetNewFont(PText(CopyView)^.Font.Font);
              PTExt(CopyView)^.Font.Font:=hFontIndex;
            end;
            if CopyView<>NIL then begin
              UpdateLastIndizesByINdex(AnewData,CopyView^.Index);
              AnewData^.InsertObject(CopyView,false);
            end;
          end;
        end;
        ANewDAta^.Layers^.TopLayer:=OldTop;
      end;
    end;

    {hsd}
     for i:=0 to hsdlist.count-1 do begin

      Aname:=hsdlist.strings[i];
      ALayer:=AData^.Layers^.NameToLayer(Aname);
      if Alayer<>NIL then begin
        MLayer:=ANewData^.Layers^.NameToLayer(ALayer^.TExt^);
        if MLayer= NIL then begin
          ANewData^.Layers^.LastIndex:=Alayer^.Index-1;
          MLayer:=AnewData^.Layers^.InsertLayerByReference(Alayer,ilLast);
          MLayer^.SymbolFill.Symbol:=TConSym(GetConSym).GetNewSym(ALayer^.SymbolFill.Symbol);
        end;

        //MLayer^.Pattern:=ALayer^.Pattern;
        //if ALayer^.Pattern > 7 then MLayer^.Pattern:=TConSym(GetConSym).GetNewPat(ALayer^.Pattern);

        {
        if ALayer^.Getstate(sf_general) then Mlayer^.SetState(sf_general,true);
        if ALayer^.Getstate(sf_LayerOff) then begin
          Mlayer^.SetState(sf_LayerOff,true);
          MLayer^.SetState(sf_HSDVisible,false);
        end else
        if not ALayer^.Getstate(sf_LayerOff) then begin
          Mlayer^.SetState(sf_LayerOff,false);
          MLayer^.SetState(sf_HSDVisible,TRUE);
        end;
         }
      end;
    end;

      {Das neue Projekt soll eine Kopie des einzulesenden PRojektes sein}
      {Demzufolge sind auch die Layerindizes anzugleich}
      {im neuen Projekt kann aber nur ein Layer zusaetzlich vorkommen: 'Erster Layer'}
      {Dieser wird defaultmaessig bei insertlayer erzeugt}
      {ist dieser Layer im einzulesenden Projekt vorhanden, so muss seine ID upgedatet werden}
      {ansonsten wird der Layer im neuen Projekt mit Lastindex hinten angehaengt}
      {oder geloescht weil sonst seine ID zweimal vergeben sein koennte}
      ALayer:=AData^.Layers^.NameToLayer(sname);
      MLayer:=ANewData^.Layers^.NameToLayer(sname);
      if ALayer = NIL then begin
        {default Layer ist im Ausgangsprojekt nicht vorhanden,loeschen}
        ANewData^.Layers^.DeleteLayer(MLayer);
      end else begin
        {default Layer ist im Ausgangsprojekt vorhanden,ID updaten}
        MLayer^.INdex:=ALayer^.Index;
      end;
      ANewData^.Layers^.LastIndex:=AData^.Layers^.LastIndex;

       {
      LCount:=AData^.Layers^.LData^.Count;
      LCount2:=ANewData^.Layers^.LData^.Count;
      LIndex:=ANewDAta^.Layers^.LastIndex;
      Lindex2:=AData^.Layers^.Lastindex;
      for i:= 1 to ANewDAta^.Layers^.LDAta^.Count-1 do begin
        MLayer:=ANewData^.Layers^.LData^.at(i);
        ALayer:=AData^.Layers^.NameToLayer(MLayer^.Text^);
      end;
        }

      (*
    cntAll:=AnewData^.Layers^.Ldata^.count;
    if cntAll>1 then begin
      {sehe ob 'Erster Layer' im alten Projekt vorhanden ist}
      {falls dort nicht vorhanden, so wir er im neuen Projekt geloeschtn}
      Alayer:=AData^.Layers^.NameToLayer(getLangtext(1500));
      if ALayer=NIL then begin
        MLayer:=ANewData^.Layers^.NameToLayer(getLangtext(1500));
        if MLayer<>NIL then begin
          MIndex:=ANewData^.Layers^.Ldata^.at(MLayer^.Index);
          ANewDAta^.Layers^.LData^.Delete(MIndex);
          Dispose(MLayer,Done);
        end;

      end;
    end;
         *)
  end;

 Procedure TDocument.InsertLayers(Bdata,BNewData:Pointer);
var
  ALayer : PLayer;
  MLayer : PLayer;
  InsLayer : PLayer;
  cnt    : integer;
  APtr   : TExtPtr2;
  a      : integer;
  AData  : PProj;
  ANewData : PProj;
  MIndex : PIndex;
  AFill  : TSymbolfill;
  begin
    AData:=PProj(BData);
    ANewData:=PProj(BNewData);


       {sehe ob 'Erster Layer' im alten Projekt vorhanden ist}
      {falls dort nicht vorhanden, so wir er im neuen Projekt geloeschtn}
      {Alayer:=AData^.Layers^.NameToLayer(getLangtext(1500));
      if ALayer=NIL then begin
        MLayer:=ANewData^.Layers^.NameToLayer(getLangtext(1500));
        MIndex:=ANewData^.Layers^.Ldata^.at(MLayer^.Index);
        ANewDAta^.Layers^.LData^.Delete(MIndex);
        Dispose(MLayer,Done);
      end;
       }
    for cnt:=0 to AData^.Layers^.LData^.Count-1 do begin
      ALayer:=AData^.Layers^.LData^.at(cnt);
      if ALayer<>nil then
      if Alayer^.text <> NIL then begin
        if ALayer^.Text^='FON' then begin
         a:=0;
        end;

        MLayer:=ANewData^.Layers^.NameToLayer(ALayer^.Text^);
        if MLayer= NIL then begin

           MLayer:=AnewData^.Layers^.InsertLayerByReference(Alayer,ilLast);
           MLayer^.SymbolFill.Symbol:=TConSym(GetConSym).GetNewSym(ALayer^.SymbolFill.Symbol);
           MLayer^.ClipRect.initbyrect(ALayer^.ClipREct);
           if Mlayer^.Linestyle.Style>=lt_Userdefined then
           MLayer^.Linestyle.Style:=TConSym(GetConSym).GetnewxLine(MLayer^.LineStyle.Style);
           if Mlayer^.Fillstyle.Pattern>=pt_Userdefined then
           MLayer^.Fillstyle.Pattern:=TConSym(GetConSym).GetnewxFill(MLayer^.FillStyle.Pattern);

          {
          if ALayer^.Getstate(sf_general) then Mlayer^.SetState(sf_general,true);
          if ALayer^.Getstate(sf_LayerOff) then begin
            Mlayer^.SetState(sf_LayerOff,true);
            MLayer^.SetState(sf_HSDVisible,false);
          end else
          if not ALayer^.Getstate(sf_LayerOff) then begin
            Mlayer^.SetState(sf_LayerOff,false);
            MLayer^.SetState(sf_HSDVisible,true);
          end;
          }
        end;

        if MLayer<>NIL then
        if ansiuppercase(Mlayer^.Text^)=ansiuppercase(getlangtext(1500)) then begin
          if not FirstLayervisited then begin
            FirstLayerVisited:=true;
            {
            MLayer^.Color:=ALayer^.Color;
            MLayer^.LineStyle:=ALayer^.LineStyle;
            MLayer^.PatColor:=ALayer^.PatColor;
            MLayer^.Pattern:=ALayer^.Pattern;
            if ALayer^.Pattern>7 then MLayer^.Pattern:=TConSym(GetConSym).GetNewPat(ALayer^.Pattern);
            }
            MLayer^.State:=ALayer^.State;
            MLayer^.Index:=ALayer^.Index;
            MLayer^.ClipRect.initbyrect(ALayer^.ClipREct);
            MLayer^.GeneralMax:=ALayer^.GeneralMax;
            MLayer^.GeneralMin:=ALayer^.GeneralMin;
            {
            if ALayer^.Getstate(sf_general) then Mlayer^.SetState(sf_general,true);
            if ALayer^.Getstate(sf_LayerOff) then begin
              Mlayer^.SetState(sf_LayerOff,true);
              MLayer^.SetState(sf_HSDVisible,false);
            end else
            if not ALayer^.Getstate(sf_LayerOff) then begin
              Mlayer^.SetState(sf_LayerOff,false);
              MLayer^.SetState(sf_HSDVisible,TRUE);
            end;
            }
          end;
        end;


      end;
    end;
  end;

{$IFNDEF AXDLL} // <----------------- AXDLL
Procedure   TDocument.ConvertWithLayers(InputChild:POinter;MainWindow:Pointer;InpFileName,HspFileName,
            HsdFileName:string;HspList,hsdlist,OutFileList:TStringList);
   var
       MconSym        : TConsym;
       AName          : Array[0..80] of Char;
       FileName       : Array[0..80] of Char;
       FName          : string;
       Data           : PProj;
       NewData        : PProj;
       ProjectRect    : TRectItem;
       cnt            : integer;
       doit           : boolean;
       doit2          : boolean;
       helpx           : longint;
       aDir            : string;
       MWnd            : TWingisMainForm;
   begin

     Statusbar.progresspanel:=true;
     //roj(Proj)^.SetCursor(crHourGlass);
     PProj(Proj)^.Layers2:=New(PLayers,Init);
     getdir(0,adir);
     PProj(Proj)^.LogFile:=false;
    {Basefile ... Proj,Newdata}
    {input amp-file .. Data}
     {outfilename is open}
     if PProj(Proj)^.LogFile then begin
       assignfile(lfile,HSP_LOGFILE);
       rewrite(lfile);
     end;
     if PProj(Proj)^.LogFile then writeln(lFile,HSP_LOGHEADER);
     MWnd:=TWingisMainForm(MainWindow);
     {GetHeapMan.Atconverting:=true;}
     AtConverting:=true;
     MConSym:=TConSym.Create;
     AConsYm:=MConsym;
     MConSym.InitGlobalProject(Proj);
     PProj(Proj)^.Atconverting:=true;
     {1.File}

     strPcopy(AName,INpfileName);
     {if PProj(Proj)^.LogFile then writeln(lFile,InpFileName);}
     if PProj(Proj)^.LogFile then WritelN(lFile);
     inputfilename:=extractfileName(inpfilename);

     {doit:=MWnd^.FileNameOpenOW(AName);}
     doit:=true;
     if doit then begin
       //atusLine^.SetText2(id_Line1,AName,false);
       Data:=TMDIChild(InputChild).Data;
       MConSym.InitActualProject(Data);
       MConSym.Updatesymbols;
       MConSym.UpdateFonts;
       MConSym.UpdatePatterns;
       AConSym:=GetConsym;
       ConvertProj:=Data;
       ConvertInfo:=Data^.PInfo;
       BHeapManager:=PProj(Proj)^.AHeapManager;
       strpcopy(fileName,HsdFileName);
       OnNew(FileName);
        //atusLine^.SetText2(id_Line4,Inputfilename,false);
       INsertrefLayersHsd(Proj,hsdList);
       ProjectRect:=ImportWithLayers(HsdList);   {Filelist = Layerlist !!}
       GEtHeapMan.TCRList.AddItem(ProjectRect);
       INsertLayersHspHsd(Data,Proj,HspList,hsdList);
       InsertViews(Data,Proj);
       InsertProjStyles(Proj,Data);  //This proj; input proj;
       UpdateLastIndizesWithOldIndizes(Proj);
       MWnd.FileNameClose(Aname);
       //PProj(Proj)^.SetCursor(idc_wait);

       BHeapManager:=PProj(Proj)^.AHeapManager;
       GEtHeapMan.TCRList.CalcAllsize;
       GetHeapMan.TCRList.EnlargeAllsize;
       CreateIndexTree;
       //Statusbar.progresspanel:=true;
       //StatusBar[0]:=Format('X: %.2f  Y: %.2f',[Pos.X/100.0,Pos.Y/100.0]);
       //StatusBar[0]:=getlangText(10186);
       //StatusLine^.SetText2(id_Line4,'',false);
       //StatusLine^.SetText2(id_Line3,'',false);
       DoDbIndizes;
       //Statusbar.ProgressPanel:=false;

       NewData:=PProj(Proj);
       NewData^.Size.a.x:=round(GetHeapMan.TCRList.Allsize.left);
       NewData^.Size.b.x:=round(GetHeapMan.TCRList.Allsize.right);
       NewData^.Size.a.y:=round(GetHeapMan.TCRList.Allsize.bottom);
       NewData^.Size.b.y:=round(GetHeapMan.TCRList.Allsize.top);
       NewData^.TurboVector:=true;   {eigentlich nur wenn Speicher true ist, sonst bei Save->abort zuruecksetzen}
       if NewDAta^.Size.Empty then begin
         NewDAta^.Size.A.Init(0,0);
         NewDAta^.Size.B.Init(siStartX,siStartY);
       end;
       {ConvertProj:=Data;
       UpdateLastIndizesWithOldIndizes(Newdata);}
       InsertBitmaps;
       Dispose(PProj(Proj)^.Layers2,Done);
       NewData^.SetModified;
       FName:=StrPas(FileName);
       PProj(Proj)^.BaseFile:=FName;
       Onstore(FileName);                 {speichert gesamete Daten das Projektes}
     end;
     MConSym.Free;
     { PProj(Proj)^.Atconverting:=false;}
     if PProj(Proj)^.LogFile then CloseFile(Lfile);
      //StatusLine^.SetText2(id_Line1,'',false);
     //StatusLine^.SetText2(id_Line2,'',false);

     //PProj(Proj)^.SetCursor(idc_Arrow);
     Statusbar.progresspanel:=false;

   end;
{$ENDIF} // <----------------- AXDLL


PRocedure TDocument.UpdateLastIndizes(AData:Pointer);
 var NewDAta : PProj;
  begin
    NewData:=ADAta;
    PObjects(Newdata^.PInfo^.Objects)^.LastTExt:=LastText;
    PObjects(Newdata^.PInfo^.Objects)^.LastCPoly:=LastCPoly;
    PObjects(Newdata^.PInfo^.Objects)^.LastPoly:=LastPoly;
    PObjects(Newdata^.PInfo^.Objects)^.LastSymbol:=LastSymbol;
    PObjects(Newdata^.PInfo^.Objects)^.LastPixel:=LastPixel;
    PObjects(Newdata^.PInfo^.Objects)^.LastMesLine:=LastMesLine;
    PObjects(Newdata^.PInfo^.Objects)^.LastCircle:=LastCircle;
    PObjects(Newdata^.PInfo^.Objects)^.LastArc:=LastArc;
    PObjects(Newdata^.PInfo^.Objects)^.LastSpline:=LastSpline;
    PObjects(Newdata^.PInfo^.Objects)^.LastGroup:=LastGroup;
  end;

PRocedure TDocument.UpdateLastIndizesMax(AData:Pointer);
 var NewDAta : PProj;
  begin
    NewData:=ADAta;
    PObjects(Newdata^.PInfo^.Objects)^.LastTExt:=MaxLastText;
    PObjects(Newdata^.PInfo^.Objects)^.LastCPoly:=MaxLastCPoly;
    PObjects(Newdata^.PInfo^.Objects)^.LastPoly:=MaxLastPoly;
    PObjects(Newdata^.PInfo^.Objects)^.LastSymbol:=MaxLastSymbol;
    PObjects(Newdata^.PInfo^.Objects)^.LastPixel:=MaxLastPixel;
    PObjects(Newdata^.PInfo^.Objects)^.LastMesLine:=MaxLastMesLine;
    PObjects(Newdata^.PInfo^.Objects)^.LastCircle:=MaxLastCircle;
    PObjects(Newdata^.PInfo^.Objects)^.LastArc:=MaxLastArc;
    PObjects(Newdata^.PInfo^.Objects)^.LastSpline:=MaxLastSpline;
    PObjects(Newdata^.PInfo^.Objects)^.LastGroup:=MaxLastGroup;
    PObjects(Newdata^.PInfo^.Symbols)^.LastGroup:=MaxLastGroup;
  end;

PRocedure TDocument.UpdateLastIndizesWithOldIndizes(AData:Pointer);
 var NewDAta : PProj;
     help    : Longint;
  begin
    NewData:=ADAta;
    PObjects(Newdata^.PInfo^.Objects)^.LastTExt:=PObjects(PProj(ConvertProj)^.PInfo^.Objects)^.LastText;
    PObjects(Newdata^.PInfo^.Objects)^.LastCPoly:=PObjects(PProj(ConvertProj)^.PInfo^.Objects)^.LastCPoly;
    PObjects(Newdata^.PInfo^.Objects)^.LastPoly:=PObjects(PProj(ConvertProj)^.PInfo^.Objects)^.LastPoly;
    PObjects(Newdata^.PInfo^.Objects)^.LastSymbol:=PObjects(PProj(ConvertProj)^.PInfo^.Objects)^.LastSymbol;
    PObjects(Newdata^.PInfo^.Objects)^.LastPixel:=PObjects(PProj(ConvertProj)^.PInfo^.Objects)^.LastPixel;
    PObjects(Newdata^.PInfo^.Objects)^.LastMesLine:=PObjects(PProj(ConvertProj)^.PInfo^.Objects)^.LastMesLine;
    PObjects(Newdata^.PInfo^.Objects)^.LastCircle:=PObjects(PProj(ConvertProj)^.PInfo^.Objects)^.LastCircle;
    PObjects(Newdata^.PInfo^.Objects)^.LastArc:=PObjects(PProj(ConvertProj)^.PInfo^.Objects)^.LastArc;
    PObjects(Newdata^.PInfo^.Objects)^.LastSpline:=PObjects(PProj(ConvertProj)^.PInfo^.Objects)^.LastSpline;

    PObjects(Newdata^.PInfo^.Objects)^.LastImage:=PObjects(PProj(ConvertProj)^.PInfo^.Objects)^.LastImage;
    PObjects(Newdata^.PInfo^.Objects)^.LastGroup:=PObjects(PProj(ConvertProj)^.PInfo^.Objects)^.LastGroup;
    PObjects(Newdata^.PInfo^.Objects)^.LastOleObj:=PObjects(PProj(ConvertProj)^.PInfo^.Objects)^.LastOleObj;
    PObjects(Newdata^.PInfo^.Objects)^.LastImage:=PObjects(PProj(ConvertProj)^.PInfo^.Objects)^.LastImage;
    PObjects(Newdata^.PInfo^.Objects)^.LastRText:=PObjects(PProj(ConvertProj)^.PInfo^.Objects)^.LastRTEXT;
    PObjects(Newdata^.PInfo^.Objects)^.LastBusGraph:=PObjects(PProj(ConvertProj)^.PInfo^.Objects)^.LastBusGraph;
    PObjects(Newdata^.PInfo^.Symbols)^.LastGroup:=PObjects(PProj(ConvertProj)^.PInfo^.Symbols)^.LasTGroup;
  end;

PRocedure TDocument.UpdateLastIndizesByIndex(AData:Pointer;AIndex:longint);
 var NewDAta : PProj;
     help    : Longint;
  begin
    NewData:=ADAta;
    Dec(AIndex);
    PObjects(Newdata^.PInfo^.Objects)^.LastTExt:=Aindex;
    PObjects(Newdata^.PInfo^.Objects)^.LastCPoly:=Aindex;
    PObjects(Newdata^.PInfo^.Objects)^.LastPoly:=Aindex;
    PObjects(Newdata^.PInfo^.Objects)^.LastSymbol:=Aindex;
    PObjects(Newdata^.PInfo^.Objects)^.LastPixel:=Aindex;
    PObjects(Newdata^.PInfo^.Objects)^.LastMesLine:=Aindex;
    PObjects(Newdata^.PInfo^.Objects)^.LastCircle:=Aindex;
    PObjects(Newdata^.PInfo^.Objects)^.LastArc:=Aindex;
    PObjects(Newdata^.PInfo^.Objects)^.LastSpline:=Aindex;

    PObjects(Newdata^.PInfo^.Objects)^.LastImage:=Aindex;
    PObjects(Newdata^.PInfo^.Objects)^.LastGroup:=Aindex;
    PObjects(Newdata^.PInfo^.Objects)^.LastOleObj:=Aindex;
    PObjects(Newdata^.PInfo^.Objects)^.LastImage:=Aindex;
    PObjects(Newdata^.PInfo^.Objects)^.LastRText:=Aindex;
    PObjects(Newdata^.PInfo^.Objects)^.LastBusGraph:=Aindex;
    PObjects(Newdata^.PInfo^.Symbols)^.LastGroup:=Aindex;
  end;


{$IFNDEF AXDLL} // <----------------- AXDLL
 Procedure   TDocument.Convert(MainWindow:Pointer;OutFileName,wgpFileName:string;FileList:TStringList);
   var
       MconSym        : TConsym;
       AName          : Array[0..80] of Char;
       FileName       : Array[0..80] of Char;
       FName          : string;
       Data           : PProj;
       NewData        : PProj;
       ProjectRect    : TRectItem;
       cnt            : integer;
       doit           : boolean;
       doit2          : boolean;
       adir           : string;
       ALayer         : PLayer;
       isLayer        : Boolean;
       MIndex         : PIndex;
       MWNd           : TWingisMainForm;
   begin

     //PProj(Proj)^.SetCursor(idc_wait);
     PProj(Proj)^.Layers2:=New(PLayers,Init);
     FirstLayerVisited:=false;
     isLayer:=false;      {layer mit Layername 'Erster Layer'}
     {soll in neuem Projekt geloescht werden, wenn er sonst nirgendwo vorkommt}
     getdir(0,adir);
     adir:=extractfilePath(OutfileName);
     {outfilename is open}
     if PProj(Proj)^.LogFile then begin
       assignfile(lfile,adir+HSP_LOGFILE);
       rewrite(lfile);
     end;
     MWnd:=TWingisMainForm(MainWindow);
     GetHeapMan.Atconverting:=true;
     AtConverting:=true;
     MConSym:=TConSym.Create;
     AConsYm:=MConsym;
     MConSym.InitGlobalProject(Proj);
     PProj(Proj)^.Atconverting:=true;
     {1.File}

     strPcopy(AName,FileList.Strings[0]);
     if PProj(Proj)^.LogFile then writeln(lFile,HSP_LOGHEADER);
     {if PProj(Proj)^.LogFile then writeln(lFile,FileList.Strings[0]);}
     if PProj(Proj)^.LogFile then WritelN(lFile);

     Application.ProcessMessages;
     MWnd.ActualChild:=MWnd.IsFileOpened(AName);
     if MWnd.ActualChild<>NIL then doit:=TRUE
     else doit:=MWnd.FileNameOpen(AName,0,False);
     Application.ProcessMessages;

     doit:=true;
     if doit then begin
       {auch TV auslesen!}
       {MWnd^.OpenProjectTV(strpas(AName));}
       //StatusLine^.SetText2(id_Line1,AName,false);
       //Statusbar[0]:=AName;
       Data:=TMDIChild(MWnd.ActualChild).Data;
       ALayer:=Data^.Layers^.NameToLayer(getlangtext(1500));
       if ALayer<>NIL then isLayer:=true;
       MConSym.InitActualProject(Data);
       MConSym.Updatesymbols;
       MConSym.UpdateFonts;
       MConSym.UpdateXLines;
       MConSym.UpdateXFills;
       MConSym.UpdatePatterns;
       AConSym:=GetConsym;
       ConvertProj:=Data;
       ConvertInfo:=Data^.PInfo;
       BHeapManager:=PProj(Proj)^.AHeapManager;
       strpcopy(fileName,WGPFileName);
       OnNew(FileName);
       InputFileName:=extractfilename(FileList.strings[0]);
       InputFileName:=copy(InputFileName,1,length(Inputfilename)-4);
       //StatusLine^.SetText2(id_Line4,Inputfilename,false);
       insertRefLayers(Data,Proj);
       ProjectRect:=Import;
       GEtHeapMan.TCRList.AddItem(ProjectRect);
       InsertLayers(Data,Proj);
       InsertViews(Data,Proj);
       //MWnd.FileNameCloseOW(Aname);
       Application.ProcessMessages;
       MWnd.FileNameClose(Aname);
       Application.ProcessMessages;


       {alle weiteren Files}
       if FileList.Count>1 then begin
         for cnt:=1 to FileList.Count-1 do begin
           {if PProj(Proj)^.LogFile then writeln(lFile,FileList.Strings[cnt]);}
           InputFileName:=extractfilename(FileList.strings[cnt]);
           InputFileName:=copy(InputFileName,1,length(Inputfilename)-4);
           if PProj(Proj)^.LogFile then WritelN(lFile);

           StrPCopy(AName,FileList.Strings[cnt]);
           //StatusLine^.SetText2(id_Line1,AName,false);

           Application.ProcessMessages;
           MWnd.ActualChild:=MWnd.IsFileOpened(AName);
           Application.ProcessMessages;
           if MWnd.ActualChild<>NIL then doit2:=TRUE
           else doit2:=MWnd.FileNameOpen(AName,0,False);

           doit2:=true;
           if doit2 then begin
             Data:=TMDIChild(MWnd.ActualChild).Data;
             ALayer:=Data^.Layers^.NameToLayer(getlangtext(1500));
             if ALayer<>NIL then isLayer:=true;
             MConSym.InitActualProject(Data);
             MConSym.Updatesymbols;
             MConSym.UpdateFonts;
             MConSym.UpdateXLines;
             MConSym.UpdateXFills;
             MConSym.UpdatePatterns;
             AConSym:=GetConsym;
             ConvertProj:=Data;
             ConvertInfo:=Data^.PInfo;
             BHeapManager:=PProj(Proj)^.AHeapManager;
             strpcopy(fileName,WGPFileName);
            //  StatusLine^.SetText2(id_Line4,Inputfilename,false);
             insertRefLayers(Data,Proj);
             ProjectRect:=Import;
             GEtHeapMan.TCRList.AddItem(ProjectRect);
             InsertLayers(Data,Proj);
             InsertViews(Data,Proj);
             //MWnd.FileNamecloseOW(AName);
             Application.ProcessMessages;
             MWnd.FileNameclose(AName);
             Application.ProcessMessages;
           end;
         end;
       end;

       BHeapManager:=PProj(Proj)^.AHeapManager;
       GEtHeapMan.TCRList.CalcAllsize;
       GetHeapMan.TCRList.EnlargeAllsize;

       CreateIndexTree;

       //StatusLine^.SetText2(id_Line1,getlangText(10186),false);
       //StatusLine^.SetText2(id_Line4,'',false);
       //StatusLine^.SetText2(id_Line3,'',false);

       DoDbIndizes;
       ResetMemLayers;

       NewData:=PProj(Proj);
       NewData^.Size.a.x:=round(GetHeapMan.TCRList.Allsize.left);
       NewData^.Size.b.x:=round(GetHeapMan.TCRList.Allsize.right);
       NewData^.Size.a.y:=round(GetHeapMan.TCRList.Allsize.bottom);
       NewData^.Size.b.y:=round(GetHeapMan.TCRList.Allsize.top);
       NewData^.TurboVector:=true;   {eigentlich nur wenn Speicher true ist, sonst bei Save->abort zuruecksetzen}
       if NewDAta^.Size.Empty then begin
         NewDAta^.Size.A.Init(0,0);
         NewDAta^.Size.B.Init(siStartX,siStartY);
       end;
       MaxlastGroup:=MConsym.GroupCount;
       UpdateLastIndizesMax(NewDAta);
       NewData^.SetModified;
       if not islayer then begin

         ALayer:=NewData^.Layers^.NameToLayer(getLangtext(1500));
         if ALayer<>NIL then begin
           MIndex:=NewData^.Layers^.Ldata^.at(ALayer^.Index);
           NewDAta^.Layers^.LData^.Delete(MIndex);
           Dispose(ALayer,Done);
         end;

       end;
       FName:=StrPas(FileName);
       INsertBitmaps;
       Dispose(PProj(Proj)^.Layers2,Done);
       PProj(Proj)^.BaseFile:=FName;
       Onstore(FileName);                 {speichert gesamete Daten das Projektes}
     end;
     MConSym.Free;
     Application.ProcessMessages;
     { PProj(Proj)^.Atconverting:=false;}
     if PProj(Proj)^.LogFile then CloseFile(Lfile);
     //StatusLine^.SetText2(id_Line1,'',false);
     //StatusLine^.SetText2(id_Line2,'',false);
     //PProj(Proj)^.SetCursor(idc_Arrow);
     Application.ProcessMessages;
   end;
{$ENDIF} // <----------------- AXDLL

 Function    TDocument.GetExtmap:Boolean;
   begin

     REsult:=Project.ExtMap;
   end;

 Function    TDocument.GetBasemap:Boolean;
   begin
     Result:=Project.BaseMap;
   end;

 Procedure   TDocument.SetBaseMap(AVal:Boolean);
   begin
     Project.BaseMap:=Aval;
   end;

 Procedure   TDocument.SetExtMap(AVal:Boolean);
   begin
     Project.ExtMap:=AVal;
   end;

 PRocedure   TDocument.SetSerialNumber(ANumber:string);
   begin
     DefsTr:=ANumber;
     isSerialNum:=true;
   end;


 Procedure   Tdocument.REsetSerialNumber;
   begin
     isSerialNum:=false;
   end;

 PRocedure   TDocument.IncStatus(AView:CView);
   var //AStatus             : PSTatus;
       A                   : Array[0..8] of longint;
       i                   : integer;
       cnt                 : integer;
       found               : boolean;
   begin
   (*
     ASTatus:=@(PProj(Proj)^.Status2);
     if AView.GetObjtype=ot_Poly then begin
       inc(PProj(Proj)^.Status2.Lines);
       AStatus^.SLaenge:=AStatus^.SLaenge+CPoly(AView).Length;
     end else
     if AView.GetObjtype=ot_CPoly then begin
       inc(PProj(Proj)^.Status2.Areas);
       AStatus^.SFlaeche:=AStatus^.SFlaeche+CCPoly(AView).Area;
     end;
     if AView.GetObjType=ot_Circle then begin
       inc(Astatus^.Circles);
     end;
     if AView.GetObjType=ot_Spline then begin
       inc(AStatus^.Splines);
     end;
     if AView.GetObjType=ot_MesLine then begin
       inc(AStatus^.MesLines);
     end;
     if AView.GetObjType=ot_Pixel then begin
       inc(AStatus^.Points);
     end;
     if AView.GetObjType=ot_Arc then begin
       inc(AStatus^.Arcs);
     end;
     if AView.GetObjType=ot_Text then begin
       inc(AStatus^.Texts);
     end;
     if AView.GetObjType=ot_Symbol then begin
       inc(AStatus^.Symbols);
     end;
     inc(Selcount);
     UpDateDiffObjects;
     *)
   end;


Procedure   TDocument.DecStatus(AView:CView);
   //var AStatus : PSTatus;
   begin
   (*
     ASTatus:=@(PProj(Proj)^.Status2);
     if AView.GetObjtype=ot_Poly then begin
       dec(PProj(Proj)^.Status2.Lines);
       AStatus^.SLaenge:=AStatus^.SLaenge-CPoly(AView).Length;
     end else
     if AView.GetObjtype=ot_CPoly then begin
       dec(PProj(Proj)^.Status2.Areas);
       AStatus^.SFlaeche:=AStatus^.SFlaeche-CCPoly(AView).Area;
     end;
     if AView.GetObjType=ot_Circle then begin
       dec(Astatus^.Circles);
     end;
     if AView.GetObjType=ot_Spline then begin
       dec(AStatus^.Splines);
     end;
     if AView.GetObjType=ot_MesLine then begin
       dec(AStatus^.MesLines);
     end;
     if AView.GetObjType=ot_Pixel then begin
       dec(AStatus^.Points);
     end;
     if AView.GetObjType=ot_Arc then begin
       dec(AStatus^.Arcs);
     end;
     if AView.GetObjType=ot_Text then begin
       dec(AStatus^.Texts);
     end;
     if AView.GetObjType=ot_Symbol then begin
       dec(AStatus^.Symbols);
     end;
     Dec(Selcount);
     upDateDiffObjects;
     *)
   end;

Procedure TDocument.clearstatus;
  begin
  {
    FillChar(PProj(Proj)^.Status2,SizeOf(TStatus),#0);
    SelCount:=0;
    DiffObjects:=false;
    }
  end;

Function  TDocument.GetSelObjectType:longint;
  begin
    Result:=SelectedObjectType;
  end;

PRocedure TDocument.UpdateDiffObjects;
   var //AStatus             : PSTatus;
       A                   : Array[0..8] of longint;
       i                   : integer;
       cnt                 : integer;
       found               : boolean;
  begin
  (*
     ASTatus:=@(PProj(Proj)^.Status2);
     DiffObjects:=false;
     found:=false;
     if Selcount>0 then begin
       a[0]:=AStatus^.Meslines;
       a[1]:=AStatus^.Points;
       a[2]:=AStatus^.Circles;
       a[3]:=AStatus^.Arcs;
       a[4]:=AStatus^.Symbols;
       a[5]:=AStatus^.Texts;
       a[6]:=AStatus^.Splines;
       a[7]:=AStatus^.Lines;
       a[8]:=AStatus^.Areas;
       for i:=0 to 8 do begin
         cnt:=i;
         if a[i]<>0 then begin
           found:=true;
           break;
         end;
       end;
       if found then begin
         {mindestens ein Item > 0}
         {suche, ob noch ein 2. Item ...zb. Status.Arcs... >0}
         if cnt<8 then
         for i:=cnt+1 to 8 do begin
           if a[i]<>0 then begin
             DiffObjects:=true;
           end;
         end;
       end;
       SelectedObjectType:=0;
       if not DiffObjects then begin
         if cnt=0 then SelectedObjectType:=ot_MesLine else
         if cnt=1 then SelectedObjectType:=ot_Pixel else
         if cnt=2 then SelectedObjectType:=ot_Circle else
         if cnt=3 then SelectedObjectType:=ot_Arc else
         if cnt=4 then SelectedObjectType:=ot_Symbol else
         if cnt=5 then SelectedObjectType:=ot_Text else
         if cnt=6 then SelectedObjectType:=ot_Spline else
         if cnt=7 then SelectedObjectType:=ot_Poly else
         if cnt=8 then SelectedObjectType:=ot_Cpoly;
       end;
       {Item an Stelle [cnt] <>0}
     end;
     *)
  end;

Procedure TDocument.ChangeObjectStyle(AColl:PViewColl;LayerName:string);
  var APtr       : TExtPTr;
      ALayer     : CLayer;
      UPdateREct : TDREct;
    Procedure ChangeStyle(AView:CView);
      var AIndex      : TIndex;
          RIndex      : PIndex;
          SIndex      : Longint;
          ObjectStyle : PObjstyleV2;
      begin

        AIndex.Index:=AView.Index;
        if Acoll^.Search(@AIndex,SIndex) then RIndex:=AColl^.At(SIndex)else RIndex:=NIL;
        if RIndex<>NIl then begin
          if AView.ModStyle then begin
            ObjectStyle:=AView.GetObjectStyle;
            System.Move(RIndex^.ObjectStyle^,ObjectStyle^,sizeof(TObjSTyleV2));
            if Aview.Cliprect.empty then UpdateREct.InitByrect(AView.Cliprect) else
            UpdateRect.CorrectByrect(AView.ClipRect);
          end;
           AView.Modified(AView);
        end;

      end;
    ProceDure DoBucket(ALeaf:TPBucketBase);
      var i           : longint;
          AvertexItem : TSLBaseITem;
          AView       : CView;
          Vlist       : TLayRefLst;
          AViewItem   : TExtPtr;
          j           : longint;
      begin
        Vlist:=TPBucketLeaf(ALeaf).VertexList;
        Vlist.lockpage(Vlist);
        if Vlist.count>0 then
        for j:=Vlist.count-1 downto 0 do begin
          AViewItem:=Vlist.Items(j);
          AView:=AViewItem.Get;
          if AView<>NIL then begin
            ChangeStyle(AView);
          end;
        end;
        Vlist.unlockpage(Vlist);
      end;
    procedure DoTree(tree:TPBucketBase);
    begin
      tree.LockPage(tree);
      if tree is TPBucketNode then with tree as tPbucketnode do begin
        doTree(sons[0]);
        doTree(sons[1]);
        doTree(sons[2]);                      
        doTree(sons[3]);
      end
      else begin
        DoBucket(Tree);
      end;
    end;
  begin
   UpdateREct.Assign(0,0,0,0);
   APtr:=Getlayers.GetLayerExtPtr(LayerName);
   ALayer:=APtr.Get;
   ALayer.LockPage(ALayer);
   if (ALayer<>NIL)then begin
     DoTree(ALayer.Ptree);
   end;
   ALayer.UnlockPage(ALayer);
   PProj(Proj)^.PInfo^.RedrawRect(UpdateREct);
  end;

function TDocument.GetProj:CProj;
  begin
    REsult:=CProj(Project);
  end;


{******************************************************************************+
  TDocument.InsertViews
--------------------------------------------------------------------------------
  Author: Martin Forst
--------------------------------------------------------------------------------
  Inserts the views of a project into the hsp-project. 
+******************************************************************************}
//InsertViews(Data,Proj);
Procedure TDocument.InsertViews(Data,NewData:Pointer);
var Cnt            : Integer;
    Cnt1           : Integer;
    Sight          : PSight;
    NewSight       : PSight;
    DefaultSight   : PSight;
    LayerData      : PSightLData;
    ALayer         : PLayer;
  Function SightWithName(Project:Pointer;const Name:String):PSight;
  var Cnt            : Integer;
  begin
    for Cnt:=0 to PProj(Project)^.Sights^.Count-1 do begin
      Result:=PProj(Project)^.Sights^.At(Cnt);
      if AnsiCompareText(PToStr(Result^.Name),Name)=0 then Exit;
    end;
    Result:=NIL;
  end;
begin
  { store the hsp default-view - if it has one }
  if PProj(NewData)^.Sights^.Default>=0 then DefaultSight:=PProj(NewData)^.Sights^.At(
      PProj(NewData)^.Sights^.Default)
  else DefaultSight:=NIL;
  for Cnt:=0 to PProj(Data)^.Sights^.Count-1 do begin
    Sight:=PProj(Data)^.Sights^.At(Cnt);
    { if view does not exist in hsp insert it }
    if SightWithName(NewData,PToStr(Sight^.Name))=NIL then begin
      { create layer-info }
      NewSight:=New(PSight,Init(PToStr(Sight^.Name),Sight^.ViewRect));
      if Sight^.LayersData<>NIL then begin
        NewSight^.LayersData:=New(PCollection,Init(5,5));
        for Cnt1:=0 to Sight^.LayersData^.Count-1 do begin
          LayerData:=Sight^.LayersData^.At(Cnt1);
          ALayer:=PProj(Data)^.Layers^.IndexToLayer(LayerData^.LIndex);
          if ALayer<>NIL then begin
            if ALayer^.Index<>0 then ALayer:=PProj(NewData)^.Layers^.NameToLayer(PToStr(ALayer^.Text));
            if ALayer<>NIL then NewSight^.LayersData^.Insert(New(PSightLData,
                Init(ALayer^.Index,LayerData^.LFlags,LayerData^.GeneralMin,
                LayerData^.GeneralMax)));
          end;
        end;
      end;
      { convert settings for hsd }
      NewSight^.ConvertToHSD;
      PProj(NewData)^.Sights^.Insert(NewSight);
      { if view is the default-view and hsp currenlty has not default-view
        set the newly insertet view to default }
      if (PProj(Data)^.Sights^.Default=Cnt) and (DefaultSight=NIL) then
          DefaultSight:=NewSight;
    end;
  end;
  if DefaultSight=NIL then PProj(NewData)^.Sights^.Default:=-1
  else PProj(NewData)^.Sights^.Default:=PProj(NewData)^.Sights^.IndexOf(DefaultSight);
end;


PRocedure TDocument.PrintDBIndizes;
  begin
    GetLayers.TraverseDBIndizes2;
  end;


  {ueberprueft selection mit ProgisID ind Btree}
Procedure TDocument.TestSearch(AProj:POinter;LayerName:string;AIndex:longint);
  var APtr   : TExtPtr;
      BLayer : CLayer;
      DBLst  : BPtree;
  begin
    APtr:=CLayers(PProj(AProj)^.Document.Getlayers).GetlayerExtPtr(LayerName);
    BLayer:=APtr.Get;
    BLayer.lockpage(Blayer);
    DBLst:=BLayer.DBIndex.Get;
    DBLst.Find(AIndex);
    BLayer.unlockpage(BLayer);
  end;

Procedure   TDocument.InsertBitMapOnRefLayer(AIndex:PIndex;ALayer:PIndex);
  var MLayer    : PLayer;
      CopyView  : PView;
      lProj     : PProj;
      lView     : PIndex;
  begin

    lProj:=PProj(Proj);
    MLayer:=PProj(proj)^.Layers2^.NameToLayer(PLayer(ALayer)^.TExt^);
    CopyView:=MakeObjectCopy(AIndex);
    if MLayer <> NIL then begin

      PLayer(lProj^.PInfo^.Objects)^.InsertObject(lproj^.PInfo,CopyView,TRUE);
      LView:=New(PIndex,Init(CopyView^.Index));
      LView^.ClipRect.InitByRect(CopyView^.ClipRect);
      if lProj^.Layers2^.IndexObject(lProj^.PInfo,LView) <> NIL then begin
        MLayer^.InsertObject(lProj^.PInfo,LView,TRUE);
      end;

    end;

  end;

Procedure TDocument.InsertBitmaps;
  var lProj      : PProj;
       cnt       : longint;
       cnt2      : longint;
       LayerSrc  : PLayer;
       LayerDest : PLayer;
       AIndex    : PIndex;
       AView     : Pview;
       LView     : PIndex;
  begin

    lProj:=PProj(proj);
    for cnt:=1 to lproj^.Layers2^.ldata^.count-1 do begin
      LayerSrc:=lProj^.Layers2^.ldata^.at(cnt);
      LayerDest:=lproj^.Layers^.nametolayer(LayerSrc^.Text^);
      if LayerDest<>NIl then begin
        for cnt2:=0 to LayerSrc^.Data^.getcount-1 do begin
          AIndex:=LayerSrc^.DAta^.at(cnt2);
          LView:=MakeObjectCopy(AIndex);
          {AView:=PView(PLayer(lProj^.PInfo^.Objects)^.IndexObject(LProj^.PInfo,AIndex));}
          LayerDest^.InsertObject(LProj^.PInfo,lView,True);
        end;
      end;
    end;

  end;

PRocedure TDocument.INsertRefLayersHsd(BNewData:POinter;hsdlist:TStringList);
  var ANewdata        : PProj;
      Aname           : string;
      i               : longint;
      Inslayer          : PLayer;
  begin
    ANewData:=PProj(BNewDAta);
    for i:=0 to hsdlist.count-1 do begin
       Aname:=hsdlist.strings[i];
       Inslayer:=ANewData^.Layers2^.NameToLayer(Aname);
       if Inslayer=NIL then ANewData^.Layers2^.InsertLayer(Aname,0,0,0,0,ilLast,DefaultSymbolFill);
    end;
  end;


PRocedure TDocument.INsertRefLayers(Bdata,BNewData:Pointer);
  var ANewdata        : PProj;
      AData           : PProj;
      Aname           : string;
      i               : longint;
      Inslayer        : PLayer;
      bLayer          : PLayer;
      MLayer          : PLayer;
      cnt             : longint;
      Afill           : TSymbolFill;

  begin

    AData:=PProj(BData);
    ANewData:=PProj(BNewData);

    for cnt:=1 to AData^.Layers^.LData^.Count-1 do begin
       bLayer:=AData^.Layers^.LData^.at(cnt);
       if bLayer<>nil then
       if blayer^.text <> NIL then begin
         MLayer:=ANewData^.Layers2^.NameToLayer(bLayer^.Text^);
         if MLayer= NIL then begin
           AFill:=DefaultSymbolFill;
           ANewData^.Layers2^.InsertLayer(BLayer^.text^,0,0,0,0,ilLast,AFill);
         end;
       end;
     end;

  end;

PRocedure TDocument.InsertProjStyles(AProj,AData:Pointer);
  begin
    PProj(AProj)^.Pinfo^.ProjStyles.Assign(PProj(AData)^.Pinfo^.ProjStyles);
  end;

Procedure TDocument.DrawSelection(AInfo:PPaint);
 var i      : longint;
      AItem  : TSelItem;
      ALeaf  : TPBucketLeaf;
      AvertexItem : TSLBaseITem;
      Help        : TSLBaseITem;
      AView       : CView;
      Vlist       : TLayRefLst;
      AViewItem   : TExtPtr;
      j           : longint;
      ycount      : longint;
  begin
    ycount:=TGlobal(GetheapMan.GlobalObj).SelLst.SelectCount;
    aInfo^.ExtCanvas.Push;
    try
      aInfo^.SetLineStyle(PInfo^.SelectionSettings.LineStyle,TRUE);
      aInfo^.SetFillStyle(PInfo^.SelectionSettings.FillStyle,TRUE);
      aInfo^.SetSymbolFill(DefaultSymbolFill);
      for i:=0 to TSelList(GetHeapMan.SelList).Count-1 do begin
        AItem:=TSelList(GetHeapMan.SelList).Items[i];
        ALeaf:=TPBucketLeaf(AItem.Mptr.Get);

        {traverseleaf}

        Vlist:=TPBucketLeaf(ALeaf).VertexList;
        if Vlist <> nil then begin
           Vlist.lockpage(Vlist);
           if Vlist.count>0 then
           for j:=Vlist.count-1 downto 0 do begin
               AViewItem:=Vlist.Items(j);
               AView:=AViewItem.Get;
               if AView<>NIL then if TGlobal(GetHeapMan.GlobalObj).SelLst.issel(AView.index2) then
                  AView.Draw(aInfo);
           end;
           Vlist.unlockpage(Vlist);
        end;
      end;
    finally
      aInfo^.ExtCanvas.Pop;
    end;
  end;

end.

