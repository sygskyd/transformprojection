
{CView. drawn u sel Routinen}
unit Uview;
{TBucketVertex entmisten!!!}
interface
uses classes,udef,WinTypes,Winprocs,uclass,Am_Paint,am_def,uvertex,uqtree,am_index;

const
      ArrowWidth   = 10;
      MaxDist      = 20;

Type

     Cview = class(TMainClass)
       State       : LongInt;
       ObjStyle    : TExtPtr;
       MOdStyle    : Boolean;
       ClipRect    : TDRect;
       Index       : LongInt;      {DBIndex}
       Index2      : Longint;      {von 1... inc...}
       Function    Visible(PInfo:PPaint):Boolean; virtual;
       Function    SelectByPoint(PInfo:PPaint;Point:TDPoint;ObjType:TObjectTypes):Boolean; virtual;  abstract;
       Function    SelectByRect(PInfo:PPaint;Rect:TDRect;Inside:Boolean):Boolean; virtual;   abstract;
       Procedure Draw(PInfo:PPaint); virtual; abstract;
       Procedure Dra; virtual; abstract;
       Procedure SetIndex(AIndex:longint);

       Procedure GetClipRect(var AClipRect:TClipRect);
       {Procedure Insert(Project:TObject);}
      { Procedure Split(ABucket:TBucketLeaf;Node:TBucketNode);}
       Procedure GetPosition(var pos :TBVertex);
       Procedure SetDrawn(ADrawn:word);
       Procedure ResetDrawn;
       Procedure SetSelected;
       Procedure ResetSelected;
       Function  Getsel: boolean;
       Function  Getdrawn : word;
       Function  NeedClipping(PInfo:PPaint):Boolean;
       Function  GetState(AState:LongInt):Boolean;
       Procedure SetState(AState:LongInt;ASet:Boolean);
       Function  GetObjType:Word; virtual; abstract;
       procedure InitObjectStyle(AStyle:PObjStyleV2);
       Function  GetObjectSTyle:PObjStyleV2;
       Function  HasLineStyle:Boolean;
       Function  HasPattern:Boolean;
       Function  HasSymbolFill:Boolean;
       {Property Index : Longint read FIndex write FIndex;}
     end;


      TVertexItem = class                      {Keine Ableitung von MainClass}
       private
        Mptr  : TExtPtr;{CView;}
        Pos   : TDPoint;
       public
        Constructor Create(AView:CView);
        Function Getx: TCoord;
        Function Gety: TCoord;
        Function GetView : CView;              {Nutze MClass aus Kernel; TExtPtr macht das automatisch}
      end;

implementation
uses udraw,ulayer,uheap;

Constructor TVertexItem.Create;
  begin
    {Mptr:=AView;}
    Mptr.PutToVar(AView);
    with AView do begin
      Pos.x:=round(ClipREct.a.x+(ClipREct.B.x-Cliprect.A.x)/2);
      Pos.y:=round(ClipREct.a.y+(ClipREct.B.y-Cliprect.A.y)/2);
    end;
  end;

Function TVertexItem.Getx:TCoord;
  begin
    Result:=TCoord(Pos.x);
  end;

Function TVertexITem.Gety:TCoord;
  begin
    Result:=TCoord(Pos.y);
  end;

Function TVertexItem.GetView;
  begin
    {Result:=Mptr;}
    Result:=Mptr.Get;
  end;


PRocedure CView.SetIndex
   (
   AIndex   : Longint
   );
  begin
    Index:=AIndex;
  end;

Function CView.Visible
   (
   PInfo           : PPaint
   )
   : Boolean;
  begin
    Visible:=PInfo^.RepaintArea.IsVisible(ClipRect);
  end;

Procedure CView.GetClipRect
   (
   var AClipRect   : TClipRect
   );
  begin
    with AClipRect do begin
      Left:=ClipRect.A.X;
      Right:=ClipRect.B.X;
      Bottom:=ClipRect.A.Y;
      Top:=ClipRect.B.Y;
    end;
  end;

Procedure  CView.GetPosition(var Pos:TBVertex);
  var l,r,b,t: double;

  begin
  l:=ClipRect.a.x;
  r:=ClipRect.b.x;
  b:=cliprect.a.y;
  t:=cliprect.b.y;
  {
    Pos.x:=round(ClipREct.a.x+(ClipREct.B.x-Cliprect.A.x)/2);
    Pos.y:=round(ClipREct.a.y+(ClipREct.B.y-Cliprect.A.y)/2);
    }
    Pos.x:=round(l+(r-l)/2);
    Pos.y:=round(b+(t-b)/2);
  end;
        (*
 Procedure CView.Insert
   (
   Project         : TObject
   );
  var Index        : Longint;
      Node         : TBucketNode;
      Son          : Longint;
      Vertex       : TBucketVertex;
      ABucket      : TBucketBase;
      Pos          : TBVertex;
      AVertexITem  : TVertexItem;
  begin
    pos:=TBVertex.Create(0,0);
    ABucket:=CLayer(Project).Tree;
    while not (ABucket is TBucketLeaf) do with ABucket as TBucketNode do begin
      GetPosition(Pos);
      Son:=CalculateSon(Pos);
      ABucket:=Sons[Son];
    end;
       
    while true do begin
        if TBucketLeaf(ABucket).VertexCount>=maxVertexEntries then begin
          Node:=TBucketLeaf(ABucket).Split;
          GetPosition(Pos);
          Son:=Node.CalculateSon(Pos);
          ABucket:=Node.Sons[Son];
        end else begin
          TBucketLeaf(ABucket).Vertexcount:=TBucketLeaf(ABucket).VertexCount+1;
          GetPosition(Pos);
          Son:=TBucketLeaf(ABucket).Calculateson(Pos);
          inc(TBucketLeaf(ABucket).Count[Son]);
          break;
        end;
    end;

    Pos.Free;
  end;
  *)
         (*
{******************************************************************************}
{ Procedure CView.Split                                                        }
{------------------------------------------------------------------------------}
{ Fuehrt das Splitting fuer allgemeinen Cliprect Mittelpunkt durch             }
{******************************************************************************}
Procedure CView.Split
  (
  ABucket           : TBucketLeaf;
  Node              : TBucketNode
  );
 var Cnt            : Longint;
     Son            : Longint;
     NewBucket      : TBucketLeaf;
     NewVertex      : TBucketVertex;
  begin

    with ABucket do begin
      Son:=Node.CalculateSon(GetPosition);
      NewBucket:=TBucketLeaf(Node.Sons[Son]);
      {NewBucket.AddObject(Self);
      InsertVertex(Position,NewVertex,NewBucket,RefList,Self,Self);
      Position:=NewVertex;
      for Cnt:=0 to Capacity-1 do if FUsed^[Cnt] then begin
        LayerRefPtr:=Items[Cnt];
        LayerObjects:=LayerLists[LayerRefPtr^.Layer.Number][otPoint];
        ObjectList:=NewBucket.LayerLists[LayerRefPtr^.Layer.Number];
        LayerRefPtr^.RefIndex:=ObjectList.Add(otPoint,LayerObjects[LayerRefPtr^.RefIndex]^);
      end;
      ABucket.DeleteObject(Self);    }
    end;
  end;
           *)



 Procedure Cview.SetDrawn(ADrawn:word);
   begin
     {Drawn:=ADrawn;}
   end;
 Procedure Cview.ResetDrawn;
   begin
    { drawn:=0;}
   end;

Function CView.Getdrawn: word;
  begin
    {Result:=drawn;}
  end;

 Procedure Cview.SetSelected;
   begin
     {Sel:=true;}
   end;
 Procedure Cview.ResetSelected;
   begin
     {sel:=false;}
   end;

Function CView.GetSel: boolean;
  begin
    {Result:=Sel;}
  end;

Function CView.NeedClipping
   (
   PInfo           : PPaint
   )
   : Boolean;
  var Point        : TPoint;
  begin
    with PInfo^ do begin
      ConvertToDisp(Self.ClipRect.A,Point);
      if not ptInRect(ClipLimits,Point) then begin
        Result:=TRUE;
        Exit;
      end;
      ConvertToDisp(Self.ClipRect.B,Point);
      if not ptInRect(ClipLimits,Point) then begin
        Result:=TRUE;
        Exit;
      end;
    end;
    Result:=FALSE;
  end;

Function Cview.GetState
   (
   AState          : LongInt
   )
   : boolean;
  begin
    GetState:=State and AState<>0;
  end;

Procedure Cview.SetState
   (
   AState          : LongInt;
   ASet            : Boolean
   );
  begin
    if ASet then State:=State or AState
    else State:=State and not(AState);
  end;

procedure CView.InitObjectStyle(AStyle:PObjStyleV2);
var help : Pointer;
  begin
    if AStyle<>NIL then begin

      help:=GetHeapMan.MAllocate(GetHeapMan.ActualPage,sizeof(TObjStyleV2));
      ObjStyle.Put(help);
      Modified(help);
      System.Move(AStyle^,help^,sizeof(TObjSTyleV2));

      ModStyle:=true;
    end;
  end;

Function Cview.GetObjectStyle:PObjStyleV2;
  begin
    Result:=PObjStyleV2(ObjStyle.Get);
  end;

Function CView.HasLineStyle:Boolean;
begin
  Result:=not(GetObjType in [ot_Text,ot_RText]);
end;

Function CView.HasSymbolFill:Boolean;
begin
  Result:=GetObjType in [ot_CPoly,ot_Circle];
end;

Function CView.HasPattern:Boolean;
begin
  Result:=GetObjType in [ot_CPoly,ot_Circle,ot_Symbol];
end;


end.



