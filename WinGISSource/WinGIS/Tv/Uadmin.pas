{331  hier wird Q-tree initialisiert}
unit Uadmin;

{190  : Setmofified(Alayer)}
interface
uses sysutils,classes,ULayer,UClass,WinTypes,UlBase,UHeap,Udef,am_paint,am_def,uqtree,am_layer,Objects,process,
     uglob,udbindex, uilst;

Const { Konstanten f�r TLayers.InsertLayer AWhere                            }
      ilTop             = 0;           { als neuen Toplayer einf�gen         }
      ilSecond          = 1;           { als zweiten Layer einf�gen          }
      ilLast            = 2;           { als letzten Layer einf�gen          }

Type CLayers = class(TMainClass)
      private
       FLayerList  : TPtr;
       TopLayer    : CLayer;
       LastIndex   : LongInt;
       Document    : Pointer;
       Proj        : Pointer;
       Def         : array[0..15] of TExtPtr;
       Function  ReadLayerList:TSLBase;
       Procedure WriteLayerList(Item:TSLBase);
       Procedure AddPointers; override;
       Procedure DelPointers; override;
      public
       ClipRect         : TClipRect;
       Constructor Create(AProj,ADocument:Pointer);
       Procedure   ResetMemLayers;
       Procedure   Draw(PInfo:PPaint);
       Property    LayerList : TSLBase read ReadLayerList write WriteLayerList;
       Function    InsertLayer(ADocument:Pointer;ACLiprect:TClipREct;AName:String;
         ACol:TColorRef;ALine:Word;APatCol:TColorRef;APat:Word;AWhere:longint;AOldIndex:Longint):CLayer;
       Function    NameToLayer(AName:String):CLayer;
       Procedure   CreateIndexTree (Proj:Pointer;AStr:STring;ASerialNum:Boolean);
       PRocedure   Init(AProj,ADocument:POinter);
       Procedure   TraverseAllLayers;
       {
       Function    SetLayerPriority(AProj:Pointer):Boolean;
       Procedure   InsertLayerInfo(Info:Pcollection);
       Procedure   CopyLayer(Info:PCollection);
       }
       Procedure   SaveOutAll;
       Procedure   SetCliprect(ARect:TClipREct);   {Set cliprect to all layers!!!}
       Function    GetLayerExtPtr(Atext: string):TExtPtr;
       Procedure   InitLayers;
       Procedure   TraverseDBIndizes;
       Procedure   TraverseDBIndizes2;
       Procedure   LayerSelectbyIndex(Document:POinter;AName:string;ALst:TDBIndexLst);
       Procedure   LayersSelectbyIndex(Document:Pointer;ALst:TDBIndexLst);
       Function    GetDef:String;
       Function    IsSerialNummer:Boolean;
     end;


     TLayerListItem=class(TSLBaseItem)
     public
        FLayer   : TPtr;{TView;}
      private
       Procedure WriteLayer(Item:CLayer);
       Function ReadLayer:CLayer;
      public
       Constructor Create(ALayer:CLayer);
       Procedure AddPointers; override;
       Property Layer: CLayer read ReadLayer write WriteLayer;
     end;

implementation

uses am_proj,udraw,uview,am_stddl,ualock,uquad,uptree,ubtreep,statebar,userintf;

Constructor CLayers.Create;
  begin
    
    LayerList:=TSLBase.Create;
    Proj:=AProj;
    Document:=ADocument;
  end;

Function  CLayers.ReadLayerList:TSLBase;
  begin
    Result:=TSLBase(REadPointer(@FLayerList));
  end;

Procedure CLayers.WriteLayerList(Item:TSLBase);
  begin
    WritePointer(@FLayerList,Item);
  end;

Procedure CLayers.AddPointers;
  begin
    GetHeapMan.AddVmtPtr(Self);
  end;

Procedure CLayers.DelPointers;
  begin
    GetHeapMan.DelVmtPtr(Self);
  end;


Function  TLayerListItem.ReadLayer:CLayer;
  begin
    Result:=CLayer(REadPointer(@FLayer));
  end;

Procedure TLayerListItem.WriteLayer(Item:CLayer);
  begin
    WritePointer(@FLayer,Item);
  end;

Constructor TLayerListItem.CReate;
  begin
   
    Layer:=ALayer;
  end;

Procedure TLayerListItem.AddPointers;
  begin
    GetHeapMan.AddVmtPtr(Self);
  end;

Function CLayers.InsertLayer
   (
   ADocument       : Pointer;
   AClipRect       : TclipREct;
   AName           : String;
   ACol            : TColorRef;
   ALine           : Word;
   APatCol         : TColorRef;
   APat            : Word;
   AWhere          : Longint;
   AOldIndex       : Longint
   )
   : CLayer;
  var ALayer       : CLayer;
      LayerItem    : TLayerListItem;
  begin
    ALayer:=CLayer.Create(AClipRect,ADocument);
    Inc(LastIndex);
    ALayer.Index:=LastIndex;
    ALayer.OldIndex:=AOldIndex;

   // ALayer.SetStyle(ACol,ALine,APatCol,APat,PProj(Proj)^.Pattern);
    ALayer.SetText(AName);
    LockPage(Alayer);

    if AWhere=ilTop then begin
      LayerItem:=TLayerListItem.Create(ALayer);
      LockPage(LayerItem);
      LayerList.AddHead(LayerItem);
      UnlockPage(LayerItem);
    end;
    UnlockPage(ALayer);
    Result:=ALayer;
  end;

Procedure CLayers.Draw(PInfo:PPaint);
  var p        : TSLBaseItem;
      help     : TSLBaseItem;
      ALayer   : CLayer;
      BLayer   : PLayer;
      cnt      : longint;
      i        : longint;
      APtr     : TEXtPtr;
      pt1      : TTraversePtr;
      pt2      : TTraversePtr;
  begin

    APtr:=GetLayerExtPtr('Active layer');
    TGlobal(GetheapMan.GlobalObj).DRawLst.ResetAll;
    cnt:=0;
     for cnt:=PProj(GetHeapMan.Proj)^.Layers^.LData^.Count-1 downto 1 do begin
      BLayer:=PProj(GetHeapMan.Proj)^.Layers^.LData^.at(cnt);
      if BLayer<>nil then
      if (BLayer^.text<>nil)and(BLayer^.GetState(sf_HSDVisible)) then begin
        Pt1:=TTraversePtr.create;
        APtr:=GetLayerExtPtr(BLayer^.Text^);
        Pt1.Item:=APtr.Get;
        ALayer:=pt1.Item;
        {LockPage(ALayer);}
        if (ALayer<>NIL)then
          {ALayer.Draw2(PInfo);}
          Pt2:=TTraversePtr.create;
          Pt2.Item:=ALayer.pTree;
          if (ALayer.pTree<>NIL)and(not GetHeapMan.AbortDraw) then ALayer.DrawPByQuery(PInfo,BLayer,{ALayer.pTree}Pt2);
          pt2.free;
        {UnlockPage(ALayer);}
        pt1.free;
      end;
      if GetHeapMan.Abortdraw then break;
     end;

    inc(GetHeapMan.ActualDraw);
    GetHeapMan.Abortdraw:=false;
  end;

Function CLayers.GetLayerExtPtr(Atext: string):TExtPtr;
  var p        : TSLBaseItem;
      help     : TSLBaseItem;
      ALayer   : CLayer;
      cnt      : longint;
      ctext    : string;
      APtr     : TExtPtr;

  begin
    Result.pn:=0;
    Result.offset:=0;
    p:=LayerList.GetHead;
    while p<>NIL do begin
      LockPage(p);
      ALayer:=TLayerListItem(p).Layer;
      Lockpage(ALayer);
      if ALayer<>nil then begin
        ctext:=alayer.gettext;
        {if Ansiuppercase(ctext) = Ansiuppercase(atext) then begin}
        if (ctext) = (atext) then begin
          {TGlobal(GetheapMan.GlobalObj).Aptr.Put(ALayer);
          Result:=TGlobal(GetheapMan.GlobalObj).Aptr;}
          APtr.PutToVar(ALayer);
          Result:=Aptr;
        end;
      end;
      Unlockpage(ALayer);
      help:=p;
      p:=p.next;
      UnlockPage(help);
    end;
  end;

Procedure CLayers.SetClipRect(ARect : TClipREct);
  var p        : TSLBaseItem;
      help     : TSLBaseItem;
      ALayer   : CLayer;
      cnt      : longint;
  begin
    cnt:=0;
    p:=LayerList.GetHead;
    while p<>NIL do begin
      inc(cnt);
      LockPage(p);
      ALayer:=TLayerListItem(p).Layer;
      LockPage(ALayer);
      if (ALayer<>NIL)then begin
        ALayer.Tree.ClipRect:=ARect;
        {Tree = Uqtree (also in Memory)}
      end;
      UnLockPage(ALayer);
      help:=p;
      p:=p.next;
      UnLockPage(help);
    end;
  end;

Procedure CLayers.SaveOutAll;
  var 
      PageNum : longint;
      SwapProcess  : TProcess;
  begin
   {
    for i:=1 to  TKernel(GetHeapMan.FKernel).Framemanager.Capacity-1 do begin
        PageNum:=TKernel(GetHeapMan.FKernel).FrameManager.FrameTable^[i].PageIndex;
        TProcess(GetHeapMan.ActualProcess).Saveout(PageNum);
    end;
    }
    TProcess(GetHeapMan.ActualProcess).SaveoutAll;
  end;

Procedure CLayers.TraverseAllLayers;
  var p        : TSLBaseItem;
      help     : TSLBaseItem;
      ALayer   : CLayer;
      cnt      : longint;
      q       : TSLBaseItem;
      help2    : TSLBaseItem;
      AView   : CView;
  begin

    cnt:=0;
    p:=LayerList.GetHead;
    while p<>NIL do begin
      inc(cnt);
      LockPage(p);
      ALayer:=TLayerListItem(p).Layer;
      LockPage(ALayer);
      if (ALayer<>NIL)then begin
        {}
        q:=ALayer.Data.Head;
        while q<>NIL do begin
          LockPage(q);
          AView:=TLayerItem(q).Mptr;
          help2:=q;
          q:=q.next;
          Unlockpage(help2);
        end;
        {}
      end;
      UnLockPage(ALayer);
      help:=p;
      p:=p.next;
      UnLockPage(help);
    end;

  end;


PRocedure Checktree(ATree:TPBucketBase);
  var lcount : longint;
      vcount : longint;
      anum   : integer;
      n0,n1,n2,n3: integer;
      acount : integer;
  Procedure DoTree(ATree:TPBucketBase);
    begin
      anum:=Atree.BucketNumber;
      if Atree is TPBucketNode then with Atree as TPBucketNode do begin
        n0:=TPBucketNOde(Atree).sons[0].BucketNumber;
        n1:=TPBucketNOde(Atree).sons[1].BucketNumber;
        n2:=TPBucketNOde(Atree).sons[2].BucketNumber;
        n3:=TPBucketNOde(Atree).sons[3].BucketNumber;
        DoTree(sons[0]);
        DoTree(sons[1]);
        DoTree(sons[2]);
        DoTree(sons[3]);
      end else begin
        acount:=TPBucketLeaf(ATree).VertexCnt;
        inc(lcount);
      end;
    end;
  begin
    lcount:=0;
    vcount:=0;
    if Atree <> nil then DoTree(Atree);
  end;

Procedure CLayers.CreateIndexTree(Proj:Pointer;AStr:String;AserialNum:Boolean);
  var p        : TSLBaseItem;
      help     : TSLBaseItem;
      ALayer   : CLayer;
      pp       : TExtPtr;
      atree     : TBucketLeaf;
      cnt       : integer;
      cnt2      : integer;
      cntdef    : longint;
      hItem    : Pointer;
      dum      : integer;
      delcnt   : longint;
      nodecnt  : longint;
      //StatusDlg: PAbort;
      pt       : TTraversePtr;
      AQTree   : TQuadtree;
      outstr   : string;
  begin
    //StatusDlg:=CreateAbortDlg(PProj(Proj)^.Parent);
    {StatusDlg^.SetPercent(100*(docount/Allcount));}
    //Statusbar.Progress:=100*(docount/Allcount);
    if length(Astr)<10 then raise ElistError.create('string zu kurz');
    cntdef:=0;

    hItem:=GetHeapMan.MAllocate(GetHeapMan.ActualPage,sizeof(longint));
    if Aserialnum then PLongint(hitem)^:=1 else PLongint(hitem)^:=0;
    Def[12].Put(HItem);
    Modified(HItem);

    p:=LayerList.GetHead;
    while p<>NIL do begin
      LockPage(p);
      ALayer:=TLayerListItem(p).Layer;
      LockPage(ALayer);
      if ALayer<>NIL then begin

        ALayer.Tree:=TBucketLeaf.Create(GetHeapMan.TCRList.Allsize,NIL,ALayer);

        ALayer.InitTree;    {Q-Tree-Wurzel bekommt Ptr auf Alayer (ALayer is locked also OK!)}

        {ALayer.CreateIndexTree(Proj);}
        AQtree:=TQuadtree.create(ALayer.tree.cliprect);
        ALayer.CreateIndexTreeNew(Proj,AQtree);
        ALayer.InitpTreenew(AQtree);

        Modified(ALayer);

        {Alayer.TreeSkeleton(ALayer.Tree,ALayer.pTree,NIL);}
        ALayer.TreeSkeletonnew(AQtree.MRoot,ALayer.ptree,NIL);
        {AQtree.checktree;
        CheckTree(ALayer.Ptree); }
        AQtree.free;

        if cntdef<10 then begin
          hItem:=GetHeapMan.MAllocate(GetHeapMan.ActualPage,sizeof(longint));
          PLongint(hitem)^:=ord(astr[cntdef+1]);
          Def[cntdef].Put(HItem);
          Modified(HItem);
        end;
        inc(cntdef);

        {}
        {
        StatusLine^.SetText2(id_Line1,getlangtext(10184)+'  '+ALayer.Text+'  '+getlangText(10185),false);
        StatusLine^.SetText2(id_Line4,Alayer.Text,false);
        StatusLine^.SetText2(id_Line3,'',false);
         }
        if length(ALayer.Text)>20 then begin
          outstr:=copy(ALayer.Text,1,10)+'..';
          StatusBar.ProgressText:=outstr;
        end else
        StatusBar.ProgressText:=ALayer.Text+'  ';


        ALayer.IntersectLayerPQuadTree(nil);

        pt:=TTraversePtr.create;
        pt.Item:=ALayer.Ptree;
        ALayer.InitRefLst(pt);
        pt.free;

        ALayer.IntersectLayerPQuadTree2(nil);

        ALayer.DeleteQTree;

        {
        StatusLine^.SetText2(id_Line1,'',false);
        StatusLine^.SetText2(id_Line4,'',false);
        StatusLine^.SetText2(id_Line3,'',false);
         }
      end;
      UnLockALL;
      LockPage(p);
      {UnLockPage(ALayer);}
      help:=p;
      p:=p.next;
      UnLockPage(help);
    end;
    randomize;

    if  cntdef < 10 then begin
      for cnt:=cntdef to 9 do begin
        dum:=random(10);
        for cnt2:=0 to dum do begin
          hItem:=GetHeapMan.MAllocate(GetHeapMan.ActualPage,sizeof(longint));
          PLongint(hitem)^:=random(64000);
        end;
        PLongint(hitem)^:=ord(astr[cnt+1]);
        Def[cnt].Put(HItem);
        Modified(HItem);
      end;
    end;
    //Dispose(StatusDlg,Done);
  end;



Function CLayers.NameToLayer
   (
   AName           : String
   )
   : CLayer;
   var p        : TSLBaseItem;
      help     : TSLBaseItem;
      ALayer   : CLayer;
      h        : string;
  begin
    Result:=NIL;

    if AName<>'' then begin
      p:=LayerList.GetHead;
      while p<>NIL do begin
        LockPage(p);
        ALayer:=TLayerListItem(p).Layer;
        LockPage(ALayer);
        if (ALayer<>NIL)then
          if ALayer.GetText=AName then begin
            Result:=ALayer;
            break;
          end;
        UnLockPage(ALayer);
        help:=p;
        p:=p.next;
        UnLockPage(help);
      end;
    end;

  end;

  


PRocedure CLayers.Init(AProj,ADocument:Pointer);

   var p        : TSLBaseItem;
      help     : TSLBaseItem;
      ALayer   : CLayer;
  begin
    Proj:=AProj;
    Document:=ADocument;

    p:=LayerList.GetHead;
    while p<>NIL do begin
      LockPage(p);
      ALayer:=TLayerListItem(p).Layer;
      LockPage(ALayer);
      ALayer.Tree:=TBucketLeaf.Create(ClipRect,NIL,Self);
      UnLockPage(ALayer);
      help:=p;
      p:=p.next;
      UnLockPage(help);
    end;
  end;
   (*
Procedure CLayers.InsertLayerInfo(Info:PCollection);
   var p        : TSLBaseItem;
      help     : TSLBaseItem;
      ALayer   : CLayer;
      cnt      : longint;
  begin

    p:=LayerList.GetHead;
    cnt:=0;
    while p<>NIL do begin
      LockPage(p);
      ALayer:=TLayerListItem(p).Layer;
      LockPage(ALayer);
      ALayer.SetSortIndex(cnt);
      inc(cnt);
      if ALayer.GetSortIndex>0 then with ALayer do
      Info^.Insert(New(PLayerInfo,Init(ALayer,GetSortIndex,GetText,not(GetState(sf_LayerOff)),
            GetState(sf_Fixed),GetColor,GetLineStyle,GetPattern,GetPatColor,GetState(sf_Transparent),
            GetState(sf_General),GetGeneralMin,GetGeneralMax)));
      UnLockPage(ALayer);
      help:=p;
      p:=p.next;
      UnLockPage(help);
    end;
  end;


Procedure CLayers.CopyLayer(Info:PCollection);
  var ALayer    : CLayer;
      i         : longint;
      AInfo     : PLayerInfo;
  begin
    for i:=0 to Info^.count-1 do begin
      AInfo:=Info^.At(i);
      ALayer:=AInfo^.Mptr.Get;
      LockPage(ALayer);

      with AInfo^ do begin
        {if ALayer.GetText<>PToStr(Name) then PProj(Proj)^.SetModified;}
        ALayer.SetText(Name^);
        ALayer.SetStyle(DrawColor,LineType,PatternColor,Pattern, PProj(Proj)^.Pattern);
        ALayer.SetState(sf_LayerOff,not Visible);
        ALayer.SetState(sf_Fixed,Fix);
        ALayer.SetState(sf_Transparent,Transparent);
        ALayer.SetState(sf_General,General);
        ALayer.SetGeneralMin(MinGeneral);
        ALayer.SetGeneralMax(MaxGeneral);
      end;
      SavePage(ALayer);
      UnLockPage(ALayer);
    end;
  end;

  *)



Procedure CLayers.REsetMemLayers;
  var p        : TSLBaseItem;
      help     : TSLBaseItem;
      ALayer   : CLayer;
      cnt      : longint;
      q       : TSLBaseItem;
      help2    : TSLBaseItem;
      AView   : CView;
      helpmem  : Pointer;
  begin

    cnt:=0;
    p:=LayerList.GetHead;
    while p<>NIL do begin
      inc(cnt);
      LockPage(p);
      ALayer:=TLayerListItem(p).Layer;
      LockPage(ALayer);
      if (ALayer<>NIL)then begin
        {}
          ALayer.MemData.Free;
          ALayer.MemData:=NIL;
        {}
      end;
      UnLockPage(ALayer);
      help:=p;
      p:=p.next;
      UnLockPage(help);
    end;

  end;

{Layer^.ExtPointer wird upgedatet in TLayer am_Layer}
PRocedure CLayers.InitLayers;
  var p        : TSLBaseItem;
      help     : TSLBaseItem;
      ALayer   : CLayer;
      BLayer   : PLayer;
      cnt      : longint;
      APtr     : TEXtPtr;
  begin
  end;


PRocedure CLayers.TraverseDBIndizes;
  var p        : TSLBaseItem;
      help     : TSLBaseItem;
      ALayer   : CLayer;
      cnt      : longint;
      ctext    : string;
      APtr     : TExtPtr;
      DBLst    : TDBLst;
      ARec     : TMRec;
  begin
    p:=LayerList.GetHead;
    while p<>NIL do begin
      LockPage(p);
      ALayer:=TLayerListItem(p).Layer;
      Lockpage(ALayer);
      if ALayer<>nil then begin
        {try }
        DBLst:=ALayer.DBIndex.Get;
        DBLst.STart;
        inc(TGlobal(GetheapMan.GlobalObj).counter);
        while not DBLst.Dest do begin
          ARec:=DBLst.Item;
          DBLst.next;
          inc(TGlobal(GetheapMan.GlobalObj).counter);
        end;
       { except end;}
      end;
      Unlockpage(ALayer);
      help:=p;
      p:=p.next;
      UnlockPage(help);
    end;
  end;

 PRocedure CLayers.TraverseDBIndizes2;
  var p        : TSLBaseItem;
      help     : TSLBaseItem;
      ALayer   : CLayer;
      cnt      : longint;
      ctext    : string;
      APtr     : TExtPtr;
      DBLst    : BPtree;
      ARec     : TMRec;
  begin
    p:=LayerList.GetHead;
    while p<>NIL do begin
      LockPage(p);
      ALayer:=TLayerListItem(p).Layer;
      Lockpage(ALayer);
      if ALayer<>nil then begin
        DBLst:=ALayer.DBIndex.Get;
        DBLst.Print;
      end;
      Unlockpage(ALayer);
      help:=p;
      p:=p.next;
      UnlockPage(help);
    end;
  end;


Procedure CLayers.LayerSelectbyIndex(Document:Pointer;AName:string;ALst:TDBIndexLst);
  var p        : TSLBaseItem;
      help     : TSLBaseItem;
      ALayer   : CLayer;
      text     : string;
      AProj    : PProj;
  begin

    AProj:=TDocument(Document).Proj;
    p:=LayerList.GetHead;
    while p<>NIL do begin
      LockPage(p);
      ALayer:=TLayerListItem(p).Layer;
      LockPage(ALayer);
      if (ALayer<>NIL)then begin
        {text:=Ansiuppercase(Alayer.GetText);}
        text:=(Alayer.GetText);
        {if Ansiuppercase(Alayer.GetText)=Ansiuppercase(AName) then begin}
        if (Alayer.GetText)=(AName) then begin
          Alayer.SelectByIndex(Document,ALst);
          break;
        end;
      end;
      UnLockPage(ALayer);
      help:=p;
      p:=p.next;
      UnLockPage(help);
    end;
    unlockall;
  end;

Procedure CLayers.LayersSelectbyIndex(Document:Pointer;ALst:TDBIndexLst);
  var p        : TSLBaseItem;
      help     : TSLBaseItem;
      ALayer   : CLayer;
      AProj    : PProj;
  begin

    AProj:=TDocument(Document).Proj;
    p:=LayerList.GetHead;
    while p<>NIL do begin
      LockPage(p);
      ALayer:=TLayerListItem(p).Layer;
      LockPage(ALayer);
      try
         if (ALayer<>NIL)then begin
            Alayer.SelectByIndex(Document,ALst);
         end;
      finally
         UnLockPage(ALayer);
         help:=p;
         p:=p.next;
         UnLockPage(help);
      end;
    end;
    unlockall;
  end;

Function CLayers.GetDef:string;
  var s    : string;
      i    : integer;
      p    : PLongint;
      t    : longint;
  begin
    s:='';
    for i:=0 to 9 do begin
      p:=Def[i].get;
      t:=p^;
      s:=s+chr(t);
    end;
    Result:=s;
  end;

Function CLayers.IsSerialNummer:Boolean;
  begin
   if PLongint(Def[12].get)^=1 then result:=true else result:=false;
  end;




end.
