
unit Uheap;

interface
uses Sysutils,classes,WinTypes,udef,usout,utdrlst,am_def,am_Paint,utils;

type  HeapCoord     = word;

const
      MaxAllocSize  = 128000000;                {65520 = groesster Block, der alloziert werden kann}
      BlockSize     = 64;
      hbnext        = 0;
      hbSize        = Sizeof(HeapCoord);
      AdrFreeList   = 12;

      {InitPageCap   = 500;}

      {isBucketSize = 500;}
      {Bis hierher wird erweitert, danach wird ausgelagert!}

      isFrameTableCapacity = 50;

      VmtSize       = 16000;
      SLBaseItem            = 0;
      SLBase                = 1;
      RecList               = 2;
      MainList              = 3;
      Test                  = 4;
      Test2                 = 5;
      RouteObject           = 6;
      CreateRouteObject     = 7;
      Corner                = 8;
      CornerListItem        = 9;
      Link                  = 10;
      GraphItem             = 11;
      PolyListItem          = 12;
      Line                  = 14;
      Vehicle               = 15;

      sLayer                = 16;
      sPoly                 = 17;
      sCPoly                = 18;
      sPoint                = 19;
      sCircle               = 20;
      sArc                  = 21;
      sText                 = 22;
      sSymbol               = 23;

      sBucketBase           = 30;
      SBucketNode           = 31;
      sBucketLeaf           = 32;
      sVertexITem           = 33;
      sLayerLst             = 34;
      sDBLst                = 35;
      sDBLstItem            = 36;
      sPointLstItem         = 37;
      sPaint                = 38;
      sProj                 = 39;
      sLayerLstItem         = 40;
      sPointLst             = 41;
      sMeasLine             = 42;
      sSpline               = 43;

      sBPNode               = 44;
      sBPKnot               = 45;
      sBPLeaf               = 46;

      sLastItem             = 47;
type
     Enotify     = Function(var AIndex:Longint):Pointer of object;
     HprvMem     = THandle;
     HHeap       = THandle;

     PVmt        = ^TVmt;
     TVmt        = array[0..VmtSize]of longint;

     PHeapAr     =^THeapAr;
     THeapAr     = array[0..254] of Pointer;

     TMyList     = class(TList)
       PageNum   : Word;
     end;

     PPage       =^TPage;
     TPage       = record
       Signature         : HeapCoord;  {2 - Bytes}        {Data Struc Version,,,usw.}
       Partition         : word;       {4}
       PageNumber        : longint;    {8}
       Capacity          : HeapCoord;  {10}
       FreeSpace         : HeapCoord;  {12}
       FreeList          : HeapCoord;  {14}
       Help              : word;       {16 ab hier koennen in 4Byte-Bloecken!!weitere Vars deklariert werden}
       IntPointerList    : Pointer;    {20}
       ExtPointerList    : Pointer;    {24}
       VmtPointerList    : Pointer;    {28}
       IncPointerList    : Pointer;    {32}
       OwnerProcess      : Pointer;    {36}    {neu}
       FrameId           : Longint;            {neu}       {ersetzt SeekFrameNum}
       Start             : Pointer;    {40}
       heapBlocknext     : HeapCoord;
       heapBlockSize     : HeapCoord;
     end;

     PAddressTable    = ^TAddressTable;
     TAddressTable    = array[0..32000] of word;       {max 2 GByte}

     TPtrNode = class
       Pn      : Word;
       Offset  : Word;
       OwnerPn : Word;
       OwnerOffset : Word;
     end;

  THeap       = class
      {FFrameCount       : longint;}
       FKernel           : TObject;
       FFramePointer     : longint;  {}
       FPageNumber       : longint;
       FrameCapacity     : longint;
       FPageCapacity     : longint;
       BlockCount        : Word;   {}
       InitSize          : longint;
       SPrivate          : PPage;
       VmtData           : PPage;
       SFrameTable       : PPage;     {}
       SPageTable        : PPage;
       SAddressTable     : PPage;
       SShedule          : PPage;
       SDocument         : PPage;     {}
       FNextPage         : Enotify;
       FFreeList         : Pointer;
       PrivateHeap       : Pointer;   {gesamter Speiherbereich z.B 16MB}
       PrivateBase       : Pointer;
       Function    GetAllocate:Boolean;
       Function    ReadFFrameTable:Pointer;
       Function    ReadFPageTable:Pointer;
       function    GetFreeList:Pointer;
      public
       FAllocate         : Boolean;
       ActualPage        : PPage;
       ActualProcess     : Pointer;
       FFrameTable        : Pointer;
       FPageTable         : Pointer;
       NullPointer       : Pointer;
       VmtTable          : PVmt;
       {SelPrivate        : Word;}
        BucketList   : TObject;
       {for test:}
       fout              : textfile;
       frompage          : Pointer;
       topage            : Pointer;
       LockCount         : longint;
       Proj              : Pointer;
       SelList           : Pointer;             {Listenelemente zeigen auf Buckets, die selektierte Objekte enhalten}
       CountLockedPages  : Word;
       SwapOutPages      : TSwapOutList;        {Testliste}
       RedrawRect        : TDRect;
       PInfo             : PPaint;
       TCRList           : TClipRectList;       {Listenelement sind die Project cliprects}
       ConSym            : Pointer;             {in TMain.Newproject : vereint Symbolen von mehreren amp-files}
       AtConverting      : Boolean;             {auf true wenn amp-Projekte konvertiert werden}
       doit              : Boolean;             {only for testing}
       TestFile          : TextFile;
       ActualDraw        : word;
       AbortDraw         : boolean;
       Layerlstcnt       : longint;
       fullObjcnt        : longint;
       LastIndex         : longint;             {Index um alle erzeugten Objekte von 1... n zu initialisieren}
       RefLayer          : Pointer;              {Actueller Layer, der gerade gezeichnet wird}
       RefLayerFix       : Pointer;
       isAttribFix       : Boolean;
       GlobalObj         : POInter;
       SwapInCount       : Longint;
       PRojModified      : Boolean;
       ReadOnly          : Boolean;
       globalheapsize    : integer;

       Constructor  Create;
       {Function     CreatePage:Pointer;}
       {Function     FreePage(Page:Pointer):word;}
       Function     Allocate(Page:Pointer;size:longint):Pointer;
       Function     MAllocate(Page:Pointer;size:longint):Pointer;
       Function     DeAllocate(p:Pointer;Size:longint):Longint;
       Function     NewMem(SelPage:word;Size:word):Pointer;
       Function     DisposeMem(p:Pointer;Size:word):longint;
       Procedure    InitGlobalPage(Page:Pointer);
       Procedure    SetActualPage(Page:PPage);
       Procedure    SetProcess(Process:Pointer);
       Procedure    WritePage(Page:PPage);
       Procedure    WritePage2(Page:PPage);
       Procedure    ReadPage(Page:PPage);
       Procedure    ReadPage2(Page:PPage);
       Procedure    AddIntPtr(var Item:Pointer);
       Procedure    DelIntPtr(var Item:Pointer);
       Procedure    AddExtPtr(var Item:Pointer);
       Procedure    DelExtPtr(var Item:Pointer);
       Procedure    AddVmtPtr(Item:Pointer);
       Procedure    DelVmtPtr(Item:Pointer);
       Procedure    AddExtPtr2(AddrItem:Pointer);
       Procedure    UpdatePointers(Page:PPage);
       Procedure    RemovePointers(Page:PPage);
       Procedure    WriteFreeList(Page:Pointer);
       Function     SeekFrameNum(PageHandle:word):longint;
       Destructor   Destroy; override;
       Property     AllocateOK:Boolean read GetAllocate write FAllocate;
       Property     NextPage:Enotify read FNextPage write FNextPage;
       Function     GetPageLinear(var AFrameIndex:Longint):Pointer;
       Function     GetSwapPage(var AFrameIndex:Longint):Pointer;
       Procedure    DelPageLinear(PageIndex:Longint);
       Function     SetNullPage:Pointer;
       Property     FramePointer:longint read FFramePointer write FFramePointer;
       Procedure    InitVmtTable;
       Procedure    SetKernel;
       Property     FrameTable:Pointer read ReadFFrameTable;
       Property     PageTable:Pointer read ReadFPageTable;
       Property     FreeList:Pointer read GetFreeList;
       Procedure    WriteBlock(Page:PPage);
       function     MakePointer(PageNum,Offset:Word):Pointer;
       Function     SwapOutPage:Longint;    {return FrameIndex}
       Procedure    WriteAllExtPtrs;
       PRocedure    WritePageExtPtrs(Pageindex:longint);
       Function     SearchForPageIndex:Longint;
      
       Procedure    ResetCachePtr;
       Procedure    CreateF;
       Procedure    WriteF(astr:string);
       Procedure    CloseF;
       Function     GetFType(VmtLink:longint):Word;
       Function    CreatePrivateHeap:Pointer;
       Procedure   FreePrivateHeap;
       function     PRojectModified:boolean;

      { Procedure    SetPageIndizes;}
     end;

Function     MemAllocate(ASize:longint):Pointer;
Procedure    AddVmtPointer(Item:Pointer);
Function     DeAllocate(p:Pointer;Size:longint):Longint;
Function     GetVmtTable:PVmt;
Function     GetHeapMan:THeap;

var BHeapManager : THeap;
    AtConverting : boolean;

implementation

uses WList, UClass, Ulbase, Process,upage,FrameTab,
     UPoly,UCPoly,ULayer,Utext,USymbol,UPixel,Ucirc,UpTree,ubucketl,usellst,ulaylst,
     uptlst, uglob, udbindex,UPaint,UProj,umeas,uspline,am_Layer,ubtreep;

Function MemAllocate(ASize:longint):POinter;
  begin
    Result:=BHeapManager.MAllocate(BHeapManager.ActualPage,ASize);
  end;

Procedure AddVmtPointer(Item:Pointer);
  begin
    if BHeapManager.ActualPage^.VmtPointerList<>NIL then
    TWordList(BHeapManager.ActualPage^.VmtPointerList).Add(GetOffset(Item));
  end;


Function  DeAllocate(p:Pointer;Size:longint):Longint;
  begin
    Result:=BHeapManager.Deallocate(p,size);
  end;

Function GetVmtTable:PVmt;
  begin
    Result:=BHeapManager.VmtTable;
  end;

Function GetHeapMan:THeap;
  begin
    Result:=BHeapManager;
  end;






Constructor THeap.Create;
  var BaseSel : Word;
   begin
     inherited Create;

     ProjModified:=false;

     BHeapManager:=Self;

     InitSize:=Blocksize*1024-Sizeof(TPage)+ 2*Sizeof(HeapCoord); {65520}

     PrivateHeap:=CreatePrivateHeap;

     BaseSel:=GetSelector(PrivateHeap);

     SPrivate:=PrivateHeap;

     initglobalPage(SPrivate);


     VmtData:=MakeLongPointer(0,(BaseSel+1*MSelectorInc));


     InitGlobalPage(VmtData);

     SetActualPage(SPrivate);
     BHeapManager.FPageNumber:=0;

     FKernel:=TKernel.Create;



     AllocateOK:=TRUE;
     NextPage:=GetPageLinear;

     VmtTable:=Allocate(VmtData,sizeof(Pointer)*VmtSize);
     InitVmtTable;
     SelList:=TSelList.Create;
     TCRList:=TClipRectList.Create;
     SwapOutPages :=TSwapOutList.Create;
     ActualDraw:=1;
     AbortDraw:=false;
     GlobalObj:=TGlobal.Create;
     LastIndex:=0;

     RefLayerFix:=New(PLayer,Init(NIL));


   end;

 Destructor THeap.Destroy;
   begin

     FKernel.Free;
     TSelList(SelList).Free;
     TCRlist.Free;
     SwapOutPages.Free;
     TGlobal(GlobalObj).free;

     freePrivateHeap;

     Dispose(PLayer(RefLayerFix),Done);

     inherited Destroy;
   end;


//   start
Function THeap.CreatePrivateHeap:Pointer;
   var hgm      : THandle;
       size     : longint;
       hpar     : PHeapAr;
       BaseSel  : word;
       fValid   : Boolean;
       cnt      : longint;
       p        : Pointer;
       Rest     : Longint;
   begin

     size:=BlockSize*1024*(isFrameTableCapacity+initblocks+1);
     if size > MaxAllocSize then raise ElistError.Create('Block too large');
     globalheapsize:=size;
     getmem(p,size);                    
     fillchar(p^,size,#0);
     PrivateBase:=p;
     Rest:=uint(p) mod 65536;
     Result:=Pointer(uint(p)+65536 - Rest);
     GlobalBase:=Result;

   end;

                                   
 Procedure THeap.FreePrivateHeap;
   var help : word;
   begin

       freemem(PrivateBase,globalheapsize);

   end;
//  end










   {
 Function  THeap.CreatePage : Pointer;
   var hgm      : THandle;
       pb       : Pointer;
       temp     : word;
   begin
     hgm:=MGlobalAlloc(GMEM_MOVEABLE,BlockSize*1024);
     if hgm<>0 then begin
       pb:=GlobalLock(hgm);
       InitGlobalPage(pb);
       inc(FPageNumber);
       Result:=pb;
     end else Result:=NIL;
   end;
   }

   {
 Function THeap.FreePage
    (
    Page       : Pointer
    )
    : word;
   begin
     GlobalUnLock(GetSelector(Page));
     Result:=GlobalFree(GetSelector(Page));
     if Result<>0 then raise ElistError.Create('Speicher erfolglos freigegeben');
   end;
  }

  Function THeap.Allocate
     (
     Page      : Pointer;
     size      : longint
     )
     : Pointer;
    begin
      if Size > InitSize then raise ElistError.Create('Block too large');
      Result:=NewMem(GetSelector(Page),Size);
      if Result=NIL then raise ElistError.Create('Allocate: Memory could not be allocated');
    end;

  Function THeap.MAllocate
     (
     Page      : Pointer;
     size      : longint
     )
     : Pointer;
    var hPage:Pointer;
        dummy : Longint;
    begin
      if Size > InitSize then begin
      raise ElistError.Create('Block too large');
      end;
      Result:=NewMem(GetSelector(Page),Size);
      if Result=NIL then begin
        hPage:=GetHeapMan.NextPage(dummy);
        if hPage<>NIL then begin
          Result:=NewMem(GetSelector(hPage),Size)
        end else begin
          Result:=GetHeapMan.NullPointer;
        end;
        FAllocate:=FALSE;
      end;
    end;

 Function THeap.DeAllocate
    (
    p          : Pointer;
    Size       : longint
    )
    :Longint;
   begin
     if Size > InitSize then raise ElistError.Create('Block too large');
     if p<> NIL then Result:=DisposeMem(p,Size) else raise ElistError.Create('Null Pointer Assignment');
     if Result=0 then raise ElistError.Create('Memory could not be disposed');
   end;

 Function THeap.NewMem
    (
    SelPage    : word;
    Size       : word
    )
    : Pointer;
  var hsize    : word;{ assembler;}
      APage    : PPage;
      Freesize : longint;
      Blocksize : word;
      p        : word;
      q        : word;
      Last     : word;
  begin
    Result:=NIL;
    APage:=MakeLongPointer(0,SelPage);
    hsize:=Size+3;
    hsize:=hsize and $FFFC;
    FreeSize:=APage^.Freespace;
    p:=APage^.Freelist;
    Last:=12; {@Freelist}
    while p<> 0 do begin
      Blocksize:=PWord(longint(Apage)+p+2)^;
      if Blocksize>=hsize then break;
      Last:=p;
      p:=PWord(longint(APage)+p)^;
    end;
    if p<>0 then begin
      if BlockSize>hsize then begin
        Result:=Pointer(longint(APage)+p);
        q:=PWord(longint(APage)+p)^;
        p:=p+hsize;
        {APage^.Freelist:=p;}
        PWord(longint(Apage)+Last)^:=p;
        Apage^.Freespace:=Apage^.Freespace-hsize;
        PWord(longint(APage)+p)^:=q;
        PWord(longint(APage)+p+2)^:=Blocksize-hsize;
      end else
      {Seite dann komplett voll}
      if Blocksize=hsize then begin
        Result:=Pointer(longint(APage)+p);
        q:=PWord(longint(APage)+p)^;
        PWord(longint(Apage)+Last)^:=q;
        Apage^.Freespace:=Apage^.Freespace-hsize;
        if APage^.FreeSpace= 0 then APage^.Freelist:=0;
      end;
    end;
   end;

 Function THeap.DisposeMem
    (
    p           : Pointer;
    Size        : word
    )
    :Longint;
  var hp       : pointer;
      Page     : Word;
      APage    : PPage;
      Freesize : Longint;
      prev     : Word;
      Del      : Word;
      next     : Word;
      hsize    : Word;
      Delsize  : Word;
      {hbnext   : Word;
      hbsize   : Word;}
      pdown    : Word;
      pup      : Word;
  begin

    Result:=0;
    Page:=GetSelector(p);
    APage:=MakeLongPointer(0,Page);
    hsize:=Size+3;
    hsize:=hsize and $FFFC;
    DelSize:=hsize;
    FreeSize:=APage^.Freespace;
    prev:=AdrFreeList;
    next:=Apage^.Freelist;
    Del:=GetOffset(p);

    if next<>Del then begin
      while next<Del do begin
        prev:=next;
        next:=PWord(longint(APage)+next)^
      end;
      PWord(longint(APage)+Del+hbnext)^:=next;
      PWord(longint(APage)+Del+hbsize)^:=Delsize;
      Apage^.FreeSpace:=Apage^.FreeSpace+Delsize;
      {APage^.FreeList:=Del;}
      pdown:=PWord(longint(APage)+Del+hbsize)^+Del;
      if pdown = next then begin
        PWord(longint(APage)+Del+hbnext)^:=
        PWord(longint(APage)+next+hbnext)^;
        PWord(longint(APage)+Del+hbsize)^:=
        PWord(longint(APage)+Del+hbsize)^+
        PWord(longint(APage)+next+hbsize)^;
      end;
      PWord(longint(APage)+Prev+hbnext)^:=Del;
      pup:=PWord(longint(APage)+Prev+hbsize)^+Prev;
      if pup=Del then begin
        PWord(longint(APage)+prev+hbnext)^:=
        PWord(longint(APage)+Del+hbnext)^;
        PWord(longint(APage)+Prev+hbsize)^:=
        PWord(longint(APage)+Prev+hbsize)^+
        PWord(longint(APage)+Del+hbsize)^;
        {APage^.FreeList:=Prev;}
      end;
      Result:=1;
    end;
  end;

 Procedure THeap.InitGlobalPage
    (
    Page   : Pointer
    );
   begin
     FillChar(Page^,65535,#0);
     PPage(Page)^.Partition:=0;
     PPage(Page)^.Signature:=2801;
     PPage(Page)^.Capacity:=BlockSize;
     PPage(Page)^.FreeSpace:=BlockSize*1024 - sizeof(TPage) + 2*sizeof(HeapCoord);
     PPage(Page)^.PageNumber:=-1;
     PPage(Page)^.IntPointerList:=NIL;
     PPage(Page)^.ExtPointerList:=NIL;
     PPage(Page)^.VmtPointerList:=NIL;
     PPage(Page)^.FreeList:=SizeOf(TPage) - 2*sizeof(HeapCoord);
     PPage(Page)^.heapBlockNext:=0;
     PPage(Page)^.HeapBlockSize:=PPage(Page)^.FreeSpace;
     PPage(Page)^.FrameId:=-1;        {in FrameTAb das gleiche}
   end;


 Procedure THeap.WriteFreeList
    (
    Page     : Pointer
    );
   var p    : word;
       pa   : PPage;
       sega : word;
       pt   : Pointer;
       size : word;
   begin
     pa:= PPage(Page);
     writeln('Full size :',pa^.FreeSpace);
     sega:=GetSelector(Page);
     p:=pa^.FreeList;
     while p<>0 do begin
       pt:=MakeLongPointer(p,sega);
       size:=PWord(MakeLongPointer(p+2,sega))^;
       writeln('Adress: ',p:4,'Size: ',size:4);
       p:=Pword(pt)^;
     end;
   end;





 Procedure THeap.UpdatePointers
    (
    Page  : PPage
    );
   begin
     Page^.Start:=MakeLongPointer(GetOffset(Page^.Start),GetSelector(Page));
     {
     if Page^.IntPointerList<>NIL then begin
       Page^.IntPointerList:=MakeLongPointer(GetOffset(Page^.IntPointerList),GetSelector(Page));
       TWordList(Page^.IntPointerList).UpdateIntPointers(Page);
     end;
     }
                       {
     if Page^.ExtPointerList<>NIL then begin
       Page^.ExtPointerList:=MakeLongPointer(GetOffset(Page^.ExtPointerList),GetSelector(Page));
       TWordList(Page^.ExtPointerList).UpdateExtPointers(Page);
     end;
                        }

     if Page^.VmtPointerList<>NIL then begin
       Page^.VmtPointerList:=MakeLongPointer(GetOffset(Page^.VmtPOinterList),GetSelector(Page));
       TWordList(Page^.VmtPointerList).UpdateVmtPointers(Page);
     end;
     {
     if Page^.IncPointerList<>NIL then begin
       Page^.IncPointerList:=MakeLongPointer(GetOffset(Page^.IncPOinterList),GetSelector(Page));
       TIncList(Page^.IncPointerList).UpdateIncPointers(Page);
     end;
     }
   end;

Procedure THeap.RemovePointers
   (
   Page :PPage
   );
  begin
    {TIncList(Page^.IncPointerList).RemoveIncPointers(Page);}    {INCPOINTERS}
  end;

 Procedure THeap.WriteBlock
    (
    Page  : PPage
    );
   var  h:PByteArray;
        i:longint;
   begin
     h:=PByteArray(Page);
     writeln('Start: ');
     for i:=0 to 50 do write(inttohex((h^[i]),2),' ');
   end;

 Procedure THeap.WritePage
    (
    Page  : PPage
    );
   var s      : TFileStream;
   begin
     s:=TFileStream.Create('d:\test',fmCreate);
     s.write(Page^,1024*BlockSize);
     s.free;
   end;

 Procedure THeap.WritePage2
    (
    Page  : PPage
    );
   var s      : TFileStream;
   begin
     s:=TFileStream.Create('d:\test2',fmCreate);
     s.write(Page^,1024*BlockSize);
     s.free;
   end;

 Procedure THeap.ReadPage
    (
    Page  : PPage
    );
   var s     : TFileStream;
   begin
     s:=TFileSTream.Create('d:\test',fmOpenRead);
     s.read(Page^,1024*BlockSize);
     s.free;
   end;

 Procedure THeap.ReadPage2
    (
    Page  : PPage
    );
   var s     : TFileStream;
   begin
     s:=TFileSTream.Create('d:\test2',fmOpenRead);
     s.read(Page^,1024*BlockSize);
     s.free;
   end;

 Procedure THeap.AddIntPtr
    (
    var Item : Pointer
    );
   begin
     TWordList(ActualPage^.IntPointerList).Add(GetOffset(@Item));
   end;

 Procedure THeap.AddExtPtr
    (
    var Item : Pointer
    );
  { var Incptr       : TIncPtr;
       PointToPage  : PPage;}
   begin
     {TWordList(ActualPage^.ExtPointerList).Add(GetOffset(@Item));}
     {
     IncPtr.Pn:=GetSelector(@Item);
     IncPtr.Offset:=GetOffset(@Item);
     PointToPage:=PPage(GetSelector(Item));
     TInclist(PointtoPage^.incPointerList).Add(IncPtr);}
   end;

  Procedure THeap.AddExtPtr2
    (
    AddrItem : Pointer
    );
   begin
     TWordList(ActualPage^.ExtPointerList).Add(GetOffset(AddrItem));
   end;

 Procedure THeap.AddVmtPtr
    (
    Item : Pointer
    );
   begin
     TWordList(ActualPage^.VmtPointerList).Add(GetOffset(Item));
   end;


 Procedure THeap.DelIntPtr
    (
    var Item : Pointer
    );
   begin
     TWordList(ActualPage^.IntPointerList).Del(GetOffset(@Item));
     {Wenn Fehler dann ActualPage nicht gesetzt!}
   end;


 Procedure THeap.DelExtPtr
    (
    var Item : Pointer
    );
   var p:PPage;
   begin
     {if Item=NIL then raise ElistError.Create('Item in Uheap NIL');}
     p:=PFrameArray(FrameTable)^[SeekFrameNum(GetSelector(@Item))].Address;
     TWordList(p^.ExtPointerList).Del(GetOffset(@Item));
   end;


 Procedure THeap.DelvmtPtr
    (
    Item : Pointer
    );
   var p:PPage;
   begin
     if Item=NIL then raise ElistError.Create('Item in Uheap NIL');
     p:=PFrameArray(FrameTable)^[SeekFrameNum(GetSelector(Item))].Address;
     TWordList(p^.VmtPointerList).Del(GetOffset(Item));
   end;


 Function THeap.SeekFrameNum
    (
    Pagehandle  : Word
    )
    :longint;
   var cnt   : longint;
       found : Boolean;
       Handle: Word;
       APage : PPage;
   begin
   (*
     found:=false;
     for cnt:= 0 to TKernel(FKernel).FrameManager.Capacity-1 do begin
       Handle:=TKernel(FKernel).FrameManager.FrameTable^[cnt].Handle;
       if PageHandle=Handle then begin
         found:=True;
         Result:=cnt;
         break;
       end;
     end;
     if not found then {Result:=-1;}  raise ElistError.Create('NOT FOUND');
     *)
     APage:=MakeLongPointer(0,PageHandle);
     Result:=APage^.FrameId;
     if Result = -1 then  raise ElistError.Create('NOT FOUND');
   end;

 Function THeap.GetAllocate
    : Boolean;
   begin
     Result:=FAllocate;
     FAllocate:=TRUE;
   end;

 Procedure THeap.SetActualPage
    (
    Page : PPage
    );
   begin
     ActualPage:=Page;
     if Page^.IntPointerList=NIL then begin
     end;
     if Page^.VmtPointerList=NIL then begin
       Page^.VmtPointerList:=TWordList.Create(2000);
     end;
   end;


  Procedure THeap.SetKernel;
   var  PageIndex   : Longint;
        FrameIndex  : Longint;
   begin
     ActualProcess:=FKernel;
     (*
     PageIndex:=TKernel(FKernel).PageTable.Count;
    { PageIndex:=TProcess(FKernel).PageTable.Count;}
     if PageIndex = 0  then begin
       SetActualPage(SPrivate);
     end else begin
       FrameIndex:=TProcess(ActualProcess).PageTable[PageIndex-1];       {OK}
       SetActualPage(TKernel(FKernel).Framemanager.FrameTable^[FrameIndex].Address);
     end;
     *)
     SetActualPage(SPrivate);
   end;

 {Setzt Seitenzeiger auf letzte Seite}
 {wenn Pagetable 0, dann wird Seite 0 erzeugt}
 {sonst wird keine neue Seite erzeugt}
 Procedure THeap.SetProcess
    (
    Process : Pointer
    );
   var  PageIndex   : Longint;
        FrameIndex  : Longint;
        TmpProcess  : Pointer;
   begin
     if Process=FKernel then raise ElistError.Create('Kernel!');
     ActualProcess:=Process;
     PageIndex:=TProcess(Process).PageTable.Count;
     if PageIndex = 0  then begin
       if TKernel(FKernel).FreeList.count = 0 then begin
         SetKernel;
         TKernel(FKernel).Framemanager.Expand;
         ActualProcess:=Process;
         {raise ElistError.Create('FreeList empty!');}
       end;
       FrameIndex:=TFreeListItem(TKernel(FKernel).FreeList.Head).Index;
       TKernel(FKernel).FreeList.DeleteHead;
       TProcess(ActualProcess).PageTable.Add(FrameIndex);
       PageIndex:=TProcess(ActualProcess).PageTable.Count-1;

       TKernel(FKernel).Framemanager.FrameTable^[FrameIndex].PageIndex:=PageIndex;
       TKernel(FKernel).Framemanager.FrameTable^[FrameIndex].Locked:=0;
       TKernel(FKernel).Framemanager.FrameTable^[FrameIndex].Modified:=0;
       TKernel(FKernel).Framemanager.FrameTable^[FrameIndex].LockCount:=0;
       TKernel(FKernel).Framemanager.FrameTable^[FrameIndex].Free:=0;

       TProcess(ActualProcess).Page0:= TKernel(FKernel).Framemanager.FrameTable^[FrameIndex].Address;
       SetActualPage(TProcess(ActualProcess).Page0);
     end else begin      {setze ActualPage auf letzte Seite des Prozesses}
       FrameIndex:=TProcess(ActualProcess).PageTable[PageIndex-1];          {OK}
       if FrameIndex<>-1 then SetActualPage(TKernel(FKernel).Framemanager.FrameTable^[FrameIndex].Address)
       else begin
         {swapped out = true!!!}
         SetActualPage(TProcess(ActualProcess).SwapIn(PageIndex-1,0));
       end;
     end;
   end;

 Procedure THeap.DelPageLinear
    (
    PageIndex : Longint
    );
   begin
     TProcess(ActualProcess).PageTable.Delete(PageIndex);     {funktion notwendig???}
   end;

 Function THeap.SearchForPageIndex:Longint;
 var i : longint;
   begin
     {PageIndex:=TProcess(ActualProcess).PageTable.Count-1;}
     {kann im Moment noch nicht expandiert werden. Sollte daher von Haus aus gross genug sein!}
     Result:=-1;
     for i:= 0 to TProcess(ActualProcess).PageTable.capacity-1 do begin
       {if TProcess(ActualProcess).PageTable.Fitems^[i].FrameIndex=-1 then begin}
       if TProcess(ActualProcess).PageTable.Items[i]=-1 then begin
         Result:=i;
         break;
       end;
     end;
   end;

  {WICHTIG : NEUE PAGE WIRD ERZEUGT                                        }
  {Pagetable Entry am Ende von PageTable kommt hinzu (Unterschied zu Getswpapage)}
  {entsprechender FrameIndex wird gesucht: wenn keiner mehr frei dann Seite auslagern}

  {Bei Getswappage kommt kein Pagetableeintrag mehr hinzu, es wird nur umgelagert}
   Function THeap.GetPageLinear(var AFrameIndex : Longint)
    : Pointer;
    var PageIndex   : Longint;
        FrameIndex  : Longint;
        TempProcess : Pointer;
        LockActualPageIndex : Longint;
   begin

     if TKernel(FKernel).FreeList.count > 0 then begin
       FrameIndex:=TFreeListItem(TKernel(FKernel).FreeList.Head).Index;
       TKernel(FKernel).FreeList.DeleteHead;
       {writeln(frameIndex);}
       {PageIndex:=TProcess(ActualProcess).PageTable.Count-1; }

     end else

     {FrameIndex = Expand or GetSwapFrameIndex}
     if TKernel(FKernel).FreeList.count = 0 then begin
       if TKernel(FKernel).Framemanager.Capacity < isFrameTableCapacity then begin
         TempProcess:=ActualProcess;
         SetKernel;
         TKernel(FKernel).Framemanager.Expand;
         ActualProcess:=TempProcess;

         FrameIndex:=TFreeListItem(TKernel(FKernel).FreeList.Head).Index;
         TKernel(FKernel).FreeList.DeleteHead;
         {raise ElistError.Create('FreeList empty!');}
       end else begin    {Swap Out}
         {FrameTable auf jeden fall voll; count=capacity!!!}
         {Lock ActualPage}
         LockActualPageIndex:=ActualPage^.PageNumber;
         FrameIndex:=SwapOutPage;                      {swap out at least one Page!}
         {SwapOutPage in GetHeapMan weil alle Prozesse koennen betroffen sein}
         {holt sich aus FrameTable beliebige Seite eines beliebigen Prozesses und lagert diese aus}
       end;
     end;

     {PageIndex = PageTable.Fcount}
     {TProcess(ActualProcess).PageTable.Add(FrameIndex);}
     TProcess(ActualProcess).PageTable.AddTail(FrameIndex);
     PageIndex:=TProcess(ActualProcess).PageTable.Count-1;   {incrementelle Seitenvergabe bei new or load (nicht bei swap!!!)}

     {Update PageTable, FrameTable}
     TKernel(FKernel).Framemanager.FrameTable^[FrameIndex].PageIndex:=PageIndex;
     TKernel(FKernel).Framemanager.FrameTable^[FrameIndex].Locked:=0;
     TKernel(FKernel).Framemanager.FrameTable^[FrameIndex].Modified:=0;
     TKernel(FKernel).Framemanager.FrameTable^[FrameIndex].LockCount:=0;
     TKernel(FKernel).Framemanager.FrameTable^[FrameIndex].Free:=0;
     Result:=TKernel(FKernel).Framemanager.FrameTable^[FrameIndex].Address;

     InitGlobalPage(Result);

     SetActualPage(Result);
     PPage(Result)^.PageNumber:=PageIndex;       {neu}
     PPage(Result)^.FrameId:=FrameIndex;  {neu}
     PPage(Result)^.OwnerProcess:=Actualprocess;                                         {new}
     AFrameIndex:=FrameIndex;
   end;


{KEINE neue PAGE wird erzeugt, Seite bleibt fix}
 {holt sich aus FrameTable beliebige Seite eines beliebigen Prozesses und lagert diese aus}
 Function THeap.GetSwapPage(var AFrameIndex:Longint): Pointer;
  var PageIndex   : Longint;
        FrameIndex  : Longint;
        TempProcess : Pointer;
   begin
     Result:=NIL;
     if TKernel(FKernel).FreeList.count > 0 then begin
       FrameIndex:=TFreeListItem(TKernel(FKernel).FreeList.Head).Index;
       TKernel(FKernel).FreeList.DeleteHead;
     end else

     {FrameIndex = Expand or GetSwapFrameIndex}
     if TKernel(FKernel).FreeList.count = 0 then begin
       if TKernel(FKernel).Framemanager.Capacity < isFrameTableCapacity then begin
         TempProcess:=ActualProcess;
         SetKernel;
         TKernel(FKernel).Framemanager.Expand;
         ActualProcess:=TempProcess;

         FrameIndex:=TFreeListItem(TKernel(FKernel).FreeList.Head).Index;
         TKernel(FKernel).FreeList.DeleteHead;
       end else begin    {Swap Out}
         {Lock ActualPage}
         FrameIndex:=SwapOutPage;                      {swap out at least one Page!}
         {SwapOutPage in GetHeapMan weil alle Prozesse koennen betroffen sein}
         {holt sich aus FrameTable beliebige Seite eines beliebigen Prozesses und lagert diese aus}
       end;
     end;
     {Update PageTable, FrameTable}
     TKernel(FKernel).Framemanager.FrameTable^[FrameIndex].PageIndex:={PageIndex}-1;
     TKernel(FKernel).Framemanager.FrameTable^[FrameIndex].Locked:=0;
     TKernel(FKernel).Framemanager.FrameTable^[FrameIndex].Modified:=0;
     TKernel(FKernel).Framemanager.FrameTable^[FrameIndex].LockCount:=0;
     TKernel(FKernel).Framemanager.FrameTable^[FrameIndex].Free:=0;
     Result:=TKernel(FKernel).Framemanager.FrameTable^[FrameIndex].Address;
    { SetActualPage(Result);}   {!!!}
     PPage(Result)^.PageNumber:=PageIndex;       {neu}
     PPage(Result)^.FrameId:=FrameIndex;  {neu}
     PPage(Result)^.OwnerProcess:=Actualprocess;                                         {new}
     AFrameIndex:=FrameIndex;

   end;
     (*
 Function THeap.GetPageLinear(var AFrameIndex : Longint)
    : Pointer;
    var PageIndex   : integer;
        FrameIndex  : Integer;
        TempProcess : Pointer;
   begin
     {if TKernel(GetHeapMan.FKernel).FreeList.count = 0 then raise ElistError.Create('FreeList empty!');}
     if TKernel(FKernel).FreeList.count = 0 then begin
       if TKernel(FKernel).Framemanager.Capacity < isFrameTableCapacity then begin
         TempProcess:=ActualProcess;
         SetKernel;
         TKernel(FKernel).Framemanager.Expand;
         ActualProcess:=TempProcess;
         {raise ElistError.Create('FreeList empty!');}
       end else begin    {Swap Out}
         {FrameTable auf jeden fall voll; count=capacity!!!}
         {Lock ActualPage}
         Result:=SwapOutPage;                      {swap out at least one Page!}
       end;
     end;
     FrameIndex:=TFreeListItem(TKernel(FKernel).FreeList.Head).Index;
     TKernel(FKernel).FreeList.DeleteHead;
     {writeln(frameIndex);}
     {PageIndex:=TProcess(ActualProcess).PageTable.Count-1; }
     PageIndex:=SearchforPageIndex;
     { TProcess(ActualProcess).PageTable.Add(FrameIndex);               {ADD bei swap in}
     TProcess(ActualProcess).PageTable.AddAtPageIndex(FrameIndex,PageIndex);
     TKernel(FKernel).Framemanager.FrameTable^[FrameIndex].PageIndex:=PageIndex;
     TKernel(FKernel).Framemanager.FrameTable^[FrameIndex].Locked:=0;
     TKernel(FKernel).Framemanager.FrameTable^[FrameIndex].Modified:=0;
     TKernel(FKernel).Framemanager.FrameTable^[FrameIndex].LockCount:=0;
     TKernel(FKernel).Framemanager.FrameTable^[FrameIndex].Free:=0;

     Result:=TKernel(FKernel).Framemanager.FrameTable^[FrameIndex].Address;

     SetActualPage(Result);
     AFrameIndex:=FrameIndex;
   end;
     *)

{Seitenersetzungs Algorythmus zB. 2nd Chance}
Function THeap.SwapOutPage:longint;
  var i: longint;
      acount : longint;
      FrameArray : PFrameArray;
      AframeTableEntry : TFrameTableEntry;
      bframeTableEntry : TFrameTableEntry;
      AFrameClass: TFrameTAble;
      APage        : PPage;
      SwapProcess  : TProcess;
      LastPage     : longint;
      doit  : boolean;
      cntx  : longint;
  begin
    Result:=-1;
    doit:=false;
    {raise ElistError.Create('Swap Out!');}
    {LOCK ACTUALPAGE in FrameTable}
    {lagert Seite irgendeines Prozesses aus; dieser muss nicht der aktuelle sein}
    {traverse FrameTable}
    {Seite mit kleinster Priority}
    {Pointer auf PPage}
    {ueber Seite->Prozess >>   Process.SwapOut(PageNum)}
    {Wichtig : Seite Kann nur vom zugehoerigen Prozess ausgespeichert werden!!!}
    FrameArray:=TKernel(FKernel).Framemanager.FrameTable;    {not used; zugriff nur auf tabelle}
    AFrameClass:=TKernel(FKernel).Framemanager;              {Klasse FrameTable}
    acount:=0;
    while true do {for i:=0 to AFrameClass.Capacity-1 do }begin
      AFrameTableEntry:=AFrameClass.FrameTable^[AFrameClass.TraversePtr];
      {if AframeTAbleEntry.Locked=0 then }begin
        APage:=AFrameTableEntry.Address;
        SwapProcess:=APage^.OwnerProcess;
        {If LockPage= AframeTableEntry.PageIndex then raise ElistError.Create('Equal');}
        {try
        if (LockPage=100)and(aframetableEntry.PageIndex=86)then raise elisterror.create('0');
        except end;}
        if AFrameTAbleEntry.PageIndex = 13 then begin
        end;
        LastPage:=Swapprocess.Pagetable.count-1;
        If (AframeTAbleEntry.Locked=0)and
           (AFrameTAbleEntry.PageIndex<LastPage-1)and(AFrameTableEntry.PageIndex<>0)  then begin
          doit:=true;
          if AframeTableEntry.PageIndex=0 then begin
            raise ElistError.Create('Zero');
          end;
          for cntx:=0 to AFrameClass.Capacity-1 do begin
            BFrameTableEntry:=AFrameClass.FrameTable^[cntx];
            if BFrametableEntry.modified = 1 then begin
            end;
          end;
          SwapProcess.SwapOut(AframeTableEntry.PageIndex);              {NUR WENN DIRTY!!!!!}   {AAAACCCCHHHTTTUUUNNNNGG!!!}
         {writeln(fout,'swap ou: ',AframeTableEntry.PageIndex);}
          {Result:=AFrameClass.TraversePtr; }    {return FrameIndex}
          {}
          Result:=TFreeListItem(TKernel(FKernel).FreeList.Head).Index;
          TKernel(FKernel).FreeList.DeleteHead;
          {}
        end;

        if AFrameClass.TraversePtr<AFrameClass.Capacity then inc(AFrameClass.TraversePtr);
        if AFrameClass.TraversePtr=AFrameClass.Capacity then AFrameClass.TraversePtr:=1;
        inc(acount);
        if acount> AFrameClass.Capacity then raise ElistError.create('zu wenige Pages!') ;
        {Sicherheitsexit falls alle pages gelockt!!! eigentlich Exeption}
        If doit then break;   {neu}
      end;

    end;
  end;

{
Procedure THeap.SwapInPage(Prozess;Pn:Integer);
Prozess,Pn ->> lagere seite mit Pn in zugehoerigem Prozess ein.
Suche zuerst freie Seite in FrameTable
wenn keine gefunden dann zuerst SwapOutgPage irgendeiner Seite
}
Function THeap.ReadFFrameTable:Pointer;
  begin
    Result:=TKernel(FKernel).FrameManager.FrameTable;
  end;

Function THeap.ReadFPageTable:Pointer;
  begin
    Result:=TProcess(ActualProcess).PageTable.GetItems;
  end;

Function THeap.GetFreeList
  : Pointer;
  begin
    Result:= TKernel(FKernel).FreeList;
  end;

Function THeap.SetNullPage
   : Pointer;
  begin
    Result:=NIL;
  end;

Function THeap.MakePointer
   (
   PageNum   : Word;
   Offset    : Word
   )
   :Pointer;
  begin
    {Result:=MakeLongPointer
    (Offset,TKernel(FKernel).FrameManager.FrameTable^[PPageArray(PageTable)^[PageNum].FrameIndex].Handle);}
    Result:=MakeLongPointer(Offset,GetSelector(TKernel(FKernel).FrameManager.
    FrameTable^[TPageTable(PageTable)[PageNum]].Address));
  end;

{
AllExtPtrs        : TList;
       APtrList          : TList;
       h:=GetSelector(FItems);
        p1:=MakeLongPointer(FItems^[cnt],GetSelector(FItems));
       }

procedure THeap.WritePageExtPtrs(pageindex:longint);
 var AFrameTable:TFrameTable;
  AFrameTableEntry : TFrameTableEntry;
  AList            : TWordList;
  cnt              : Longint;
  i                : longint;
  j                : longint;
  fr               : TextFile;
  fa               : TextFile;
  fb               : TextFile;
  AFrameIndex      : Longint;
  ExtPointer       : PPtr;
  ExtPtr           : Word;
  Segpage          : Word;
  PointerToUpdate  : Pointer;
  AnodePn          : Word;
  AnodeOffset      : word;
  FrameIndex       : Longint;
  apage            : PPage;
  AProcess         : TProcess;
  begin
    Assignfile(fr,'d:\page');
    rewrite(fr);
  {Externe Pointer / Page}


    AFrameTable:=TKernel(FKernel).FrameManager;

    AProcess:=GetHeapMan.ActualProcess;
    AFrameIndex:=AProcess.PageTable[j];
    AFrameTableEntry:=AFrameTable.FrameTable^[AFrameIndex];

      Frameindex:=TProcess(GetHeapMan.Actualprocess).Pagetable[PageIndex];
      Apage:=Tkernel(Fkernel).FrameManager.Frametable^[FrameIndex].Address;
      AList:=TWordList(APage^.ExtPointerList);
      if AList<>NIL then begin
        if AList.Count>0 then begin

          SegPage:=GetSelector(Alist.MItems);
          if AList.count>0 then
          for i:=0 to Alist.Count-1 do begin
            ExtPtr:=AList.Mitems^[i];
            ExtPointer:=MakeLongPointer(ExtPtr,SegPage);
            if ExtPointer^.PageNum<>AFrameTableEntry.PageIndex then begin
              ANodePn:=ExtPointer^.PageNum;                 {Pointer selbst}
              ANodeOffset:=ExtPointer^.Offset;

              if (Anodepn<>0)and(AnodeOffset<>0)then begin
                writeln(fr,' ','   ',ANodepn,' ',AnodeOffset);
               { PointerToUpdate:=MakePointer(ANodePn,AnodeOffset);
                writeln(fr,'------Inhalt: ',PPtr(PointerToUpdate)^.Pagenum,' ',PPtr(PointerToUpdate)^.Offset);}
              end;
            end;
          end;

        end;
        writeln(fr,'NextPage');
        writeln(fr);
      end;

    closefile(fr);
  end;

Procedure THeap.WriteAllExtPtrs;
  var AFrameTable:TFrameTable;
  AFrameTableEntry : TFrameTableEntry;
  APage            : PPage;
  cnt              : Longint;
  i                : longint;
  j                : longint;
  ANode            : TPtrNode;
  p1               : Pointer;
  h                : Word;
  AList            : TWordList;
  fr               : TextFile;
  fa               : TextFile;
  fb               : TextFile;
  AllExtPtrs       : TList;
  AProcess         : TProcess;
  AFrameIndex      : Longint;
  APtrList         : TMyList;
  Lists            : TList;
  PageIndex        : Longint;
  ExtPointer       : PPtr;
  ExtPtr           : Word;
  Segpage          : Word;
  PointerToUpdate  : Pointer;
  begin
  (*
    Assignfile(fr,'d:\ext');
    rewrite(fr);
    Assignfile(fa,'d:\inc');
    rewrite(fa);
    Assignfile(fb,'d:\trueInc');
    rewrite(fb);
    try
    ALLExtPtrs:=TList.Create;
    Lists:=Tlist.Create;       {Liste fuer alle IncPtrListen}
    AFrameTable:=TKernel(FKernel).FrameManager;

    AProcess:=GetHeapMan.ActualProcess;
    for j:=0 to AProcess.PageTable.count-1 do begin
      AFrameIndex:=AProcess.PageTable[j];
    {for cnt:=0 to AFrameTable.Capacity-1 do begin}
      AFrameTableEntry:=AFrameTable.FrameTable^[AFrameIndex];
      if AFrameTableEntry.PageIndex=-1 then raise Elisterror.create('swapped out');
      APage:=AFrameTableEntry.Address;

      {Inc POinter / Page}
      writeln(fb,'PageNum: ',AFrameTableEntry.PageIndex, ' count: ',TincList(APage^.IncPointerList).count );
      TincList(APage^.IncPointerList).Print(fb);

      {Externe Pointer / Page}
      AList:=TWordList(APage^.ExtPointerList);
      if AList<>NIL then begin
        if AList.Count>0 then begin

          APtrList:=TMylist.Create;
          APtrList.PageNum:=j;   {PageNum}
          Lists.Add(APTrList);


          SegPage:=GetSelector(Alist.FItems);
          if AList.count>0 then
          for i:=0 to Alist.Count-1 do begin
            {help:=items[cnt];}
            ExtPtr:=AList.Fitems^[i];
            ExtPointer:=MakeLongPointer(ExtPtr,SegPage);
            {
            if ExtPointer^.Offset<>0 then
            ExtPointer^.Ptr:=MakePointer(ExtPointer^.PageNum,ExtPointer^.Offset);}
            if ExtPointer^.PageNum<>AFrameTableEntry.PageIndex then begin
              ANode:=TPtrNode.Create;
              ANode.Pn:=ExtPointer^.PageNum;                 {Pointer selbst}
              ANode.Offset:=ExtPointer^.Offset;

              ANode.OwnerOffset:=AList.FItems^[i];          {Addresse des Pointers}
              ANode.OwnerPn:=AFrameTableEntry.PageIndex;

              if AllExtPTrs.count<16000 then AllExtPtrs.Add(ANode);
              if (Anode.pn<>0)and(Anode.Offset<>0)then begin
                writeln(fr,'Page: ',AFrameTableEntry.PageIndex,'   ',ANode.pn,' ',Anode.Offset);
                PointerToUpdate:=MakePointer(ANode.Pn,Anode.Offset);
                writeln(fr,'------Inhalt: ',PPtr(PointerToUpdate)^.Pagenum,' ',PPtr(PointerToUpdate)^.Offset);
              end;
            end;
          end;

        end;
        writeln(fr,'NextPage');
        writeln(fr);
      end;
    end;

    for i:=0 to AllExtPtrs.Count-1 do begin
      ANode:=AllExtPtrs[i];
      APtrList:=Lists[Anode.PN];
      APtrList.Add(ANode);
    end;


    for i:=0 to Lists.count-1 do begin
      AptrList:=Lists[i];
      writeln(fa,'Page: ',APtrList.PageNum,' Count: ',APtrList.count);
      for j:= 0 to APtrList.count-1 do begin
        ANode:=AptrList[j];
        writeln(fa,'Page: ',APtrList.PageNum,' Pointer To: ',ANode.pn,' ',Anode.Offset,'      IncPtr from: ',
                 ANode.Ownerpn,' ',Anode.OwnerOffset);
      end;
      writeln(fa,'NextPage');
      writeln(fa);
    end;

    finally
    closefile(fr);
    closefile(fa);
    closefile(fb);
    end;    *)
  end;

Procedure Theap.InitVmtTable;
  begin
    VmtTable^[SLBaseItem]        := longint(TSLBaseItem);
    VmtTable^[SLBase]            := longint(TSLBase);
  //  VmtTable^[RecList]           := longint(TRecList);
   { VmtTable^[MainList]          := longint(MainList);}
    {VmtTable^[RouteObject]       := longint(TRouteObject);
    VmtTable^[CreateRouteObject] := longint(TCreateRouteObject);
    VmtTable^[Link]              := longint(TLink);
    VmtTable^[Corner]            := longint(TCorner);
    VmtTable^[CornerListItem]    := longint(TCornerListITem);
    VmtTable^[GraphItem]         := longint(TGraphItem);
    VmtTable^[PolyListItem]      := longint(TPolyListItem);
    VmtTable^[Line]              := longint(TLine);
    VmtTable^[Vehicle]           := longint(TVehicle);    }
    {neue Datenstruktur}
    VmtTable^[sLayer]            := longint(CLayer);
    VmtTable^[sPoly]             := longint(CPoly);
    VmtTable^[sCPoly]            := longint(CCPoly);
    VmtTable^[sText]             := longint(CText);
    VmtTable^[sSymbol]           := longint(CSymbol);
    VmtTable^[SPoint]            := longint(CPixel);
    VmtTable^[sCircle]           := longint(CEllipse);
    VmtTable^[sArc]              := longint(CArc);
    VmtTable^[sBucketBase]       := longint(TPBucketBase);
    VmtTable^[sBucketNode]       := longint(TPBucketNode);
    VmtTable^[sBucketLeaf]       := longint(TPBucketLeaf);
    VmtTable^[sVertexITem]       := longint(TPVertexITem);
    VmtTable^[sLayerLst]         := longint(TLayerLst);
    VmtTable^[sLayerLstItem]     := longint(TLayerLstItem);
    VmtTable^[sPointLst]         := longint(TPOintLst);
    VmtTable^[sPointLstItem]     := longint(TPOintLstItem);
    VmtTable^[sProj]             := longint(CProj);
    VmtTable^[sPaint]            := longint(CPaint);
    VMtTable^[sDBLst]            := longint(TDBLst);
    VmtTable^[sDBlstItem]        := longint(TDBLstItem);
    VmtTable^[sMeasLine]         := longint(CMeasureLine);
    VmtTable^[sSpline]           := longint(CSpline);
    VmtTable^[SBPNode]           := longint(BPnode);
    VmtTable^[SBPKnot]           := longint(BPKnot);
    VmtTable^[SBPLeaf]           := longint(BPLeaf);
    {
      sCircle               = 20;
      sArc                  = 21;
      }
  end;

 Procedure THeap.CreateF;
 var f: textfile;
   begin
     assignfile(TestFile,'e:\wgis\t.dat');
     rewrite(TestFile);
   end;

 Procedure THeap.WriteF(astr:string);
   begin
     writeln(TestFile,astr);
   end;

 Procedure THeap.CloseF;
   begin
     closefile(TestFile);
   end;

Procedure THeap.ResetCachePtr;
  begin
    TKernel(FKernel).Framemanager.TraversePtr:=1;
  end;


function THeap.PRojectModified:boolean;
  begin
    Result:=false;
    If ProjModified then result:=true;
  end;

Function THeap.GetFType(VmtLink:Longint) : word;
  var i : Longint;
  begin
    Result:=$FFFF;
    for i:=0 to sLastItem do begin
      if VmtTable<>NIL then begin
        if VmtTable^[i]=VmtLink then begin
          Result:=i;
          break;
        end;
      end;
    end;
  end;


begin
end.
