
unit Utext;

interface
uses uheap,uview,uclass,Ulbase,wintypes,WinProcs,udef,am_paint,am_def,am_font,classes,sysutils,am_cpoly,clipping,grtools,
     ansistr, uglob;
{
OWindows,WinTypes,WinProcs,AM_Index,AM_View,AM_Paint,AM_Def,
     Strings,AM_CPoly,AM_Font,Objects;}

Const MaxTxtLen         = 255;
      minDrawHeight     = 10;          { Minimale Gr��e in 1/10 mm, die noch als Text dargestellt wird }


Type CText = class(CView)
     { private}
       Position    : TDPoint;
       Angle       : Longint;
       Width       : LongInt;
       Font        : TFontData;
       Text        : TExtPtr;             {PString}
       Procedure   AddPointers; override;
       Procedure   DelPointers; override;
       Function    GetBorder:PCPoly;
      {public}
       Procedure   CalculateClipRect(PInfo:PPaint);
       Constructor Create(aPos:TDPoint;aAngle:Longint;aWidth:Longint;aFont:TFontData;aText:String);
       Function    SelectByPoint(PInfo:PPaint;Point:TDPoint;ObjType:TObjectTypes):boolean; override;
       Function    SelectByRect(PInfo:PPaint;Rect:TDRect;Inside:Boolean):Boolean; override;
       Procedure Draw(PInfo:PPaint); override;
       Procedure   Dra;override;
       Function    GetText : STring;
       Function    GetObjType:Word; override;
       Function    GivePosX:longint;
       Function    GivePosY:Longint;
     end;

implementation

uses extcanvas;

Constructor CText.Create
   (
   aPos         : TDPoint;
   aAngle       : Longint;
   aWidth       : LongInt;
   aFont       : TFontData;
   aText       : String
   );
 var  help : Pointer;
  begin


    {if length(AText)>=MaxTxtLen then raise ElistError.create('Text zu lange');}
    {Pos}
    Position.x:=aPos.x;
    Position.y:=aPos.y;
    Angle:=aAngle;
    Width:=aWidth;
   { LockPage(Self);}
    help:=GetHeapMan.MAllocate(GetHeapMan.ActualPage,length(Atext)+1);
    Text.Put(help);
    Modified(help);
    {Modified(self);}
    System.Move(AText,help^,length(Atext)+1);
    {Text:=atext;}
    Font.Font:=aFont.Font;
    Font.Style:=aFont.Style;
    Font.Height:=aFont.Height;
   { UnlockPage(self);}
  end;

Function CText.GetText:string;
  var help   : pointer;
      AText  : array[0..maxTxtlen] of char;
  begin
    help:=Text.Get;
    StrLCopy(AText,PChar(Pointer(longint(help)+1)),PByte(help)^);
    Result:=StrPas(AText);
  end;

  {
Procedure CText.GetText(AText:PChar);
  var help : pointer;
  begin
    help:=Text.Get;
    StrLCopy(AText,PChar(Pointer(longint(help)+1)),PByte(help)^);
  end;

 }


Procedure CText.AddPointers;
  begin
    GetHeapMan.AddVmtPtr(Self);
  end;

Procedure CText.DelPointers;
  begin
    GetHeapMan.DelVmtPtr(Self);
  end;

Procedure CText.CalculateClipRect
   (
   PInfo           : PPaint
   );
  var Border       : PCPoly;
  begin
    Border:=GetBorder;
    ClipRect:=Border^.ClipRect;
    Dispose(Border,Done);
  end;

Function CText.GetBorder
   : PCPoly;
  var APoly        : PCPoly;
      Beta         : Real;
      CosL         : Real;
      SinL         : Real;
      APoint       : TDPoint;
  begin
    APoly:=New(PCPoly,Init);
	  Beta:=Angle*Pi/180;
	  CosL:=Cos(Beta);
	  SinL:=Sin(Beta);
	  APoint.Init(Position.X,Position.Y);
    APoly^.InsertPoint(APoint);
	  APoint.Move(Round(CosL*Width),Round(-SinL*Width));
    APoly^.InsertPoint(APoint);
	  APoint.Move(Round(-SinL*Font.Height),Round(-CosL*Font.Height));
    APoly^.InsertPoint(APoint);
	  APoint.Move(Round(-CosL*Width),Round(SinL*Width));
    APoly^.InsertPoint(APoint);
    APoly^.ClosePoly;
    GetBorder:=APoly;
  end;

Procedure CText.Draw
   (
   PInfo : PPaint
   );
  var AWidth       : Double;
      AHeight      : LongInt;
      FontDes      : PFontDes;
      APoly        : PCPoly;
      OldMode      : Word;
      StartPos     : TGrPoint;
      Clipped      : Boolean;
      AText        : Array[0..MaxTxtLen] of Char;
      MText        : String;
(*
     AFont        : HFont;
      OldFont      : HFont;
      StartPos     : TPoint;
      AWidth       : Double;
      BWidth       : Double;
      XMove        : Double;
      AHeight      : LongInt;
      AText        : Array[0..MaxTxtLen] of Char;
      CharWidths   : Array[0..MaxTxtLen] of Double;
      BText        : PChar;
      Beta         : Real;
      APoly        : PCPoly;
      OldMode      : Word;
      OldBKMode    : Word;
      LogFont      : TLogFont;
      Cnt          : Longint;
      ABCWidths    : TABC;
      FontDes      : PFontDes;
      CharPtr      : PChar;
      NextPtr      : PChar;
      CharCode     : Word;
      XPosition    : Double;
      YPosition    : Double;
      ClipPoints   : TClipPointList;
      CosAlpha     : Double;
      Cos2Alpha    : Double;
      SinAlpha     : Double;
      MinPosition  : Double;
      MaxPosition  : Double;
      Value        : Double;
      RealCharCount: Longint;
      StartChar    : Longint;
      LastChar     : Longint;
      IsDBCS       : Boolean;
      UseWidth     : Boolean;
      TextMetrics  : TTextMetric;
      TextSize     : LongInt;
      CWidth       : Double;
      CHeight      : Double;
      Point        : TGrPoint;

      Clipped      : Boolean;
  *)

  begin
  if Visible(PInfo) then begin
  // calculate current width and height
    AWidth:=PInfo^.CalculateDispDouble(Width);
    AHeight:=PInfo^.CalculateDisp(Font.Height);
    if (AHeight>0) and (AWidth>0)then begin
      // get information about the font
      FontDes:=PInfo^.Fonts^.GetFont(Font.Font);
      if (AHeight>=MaxInt) or ((AHeight<minDrawHeight)
          and not PInfo^.Printing) or (FontDes=NIL) then begin
        APoly:=GetBorder;
        OldMode:=PInfo^.DrawMode;
        PInfo^.DrawMode:=PInfo^.DrawMode or dm_NoPattern;
        APoly^.Draw(PInfo,Clipped);
        Dispose(APoly,Done);
        PInfo^.DrawMode:=OldMode;
      end
      else with PInfo^.ExtCanvas do begin
        // store current settings
        Push;
        try
          // setup font
          Font.Angle:=-Angle*Pi/180.0+PInfo^.ViewRotation;
          Font.Name:=FontDes^.Name^;
          Font.Size:=AHeight;
          Font.Color:=Pen.Color;
          Font.Style:=(Self.Font.Style Shr 2) and $7;
          // setup brush
          if Self.Font.Style and ts_Transparent<>0 then Brush.BackColor:=clNone
          else Brush.BackColor:=$FFFFFF;
          PInfo^.ConvertToDispDouble(Position,StartPos.X,StartPos.Y);
          // calculate position from text-alignment and draw text
          if Self.Font.Style and ts_Right<>0 then begin
            StartPos.X:=StartPos.X+AWidth*Font.CosAngle;
            StartPos.Y:=StartPos.Y+AWidth*Font.SinAngle;
            Font.Alignment:=taRight;
          end
          else if Self.Font.Style and ts_Center<>0 then begin
            StartPos.X:=StartPos.X+AWidth*Font.CosAngle/2.0;
            StartPos.Y:=StartPos.Y+AWidth*Font.SinAngle/2.0;
            Font.Alignment:=taCentered;
          end
          else Font.Alignment:=taLeft;
          MText:=GetTExt;
          StrPCopy(Atext,MText);
          if Clipped then ClippedTextOut(StartPos,AText,Length(MText))
          else TextOut(StartPos,AText,Length(MText))
          {
          if Clipped then ClippedTextOut(StartPos,@Text^[1],Length(Text^))
          else TextOut(StartPos,@Text^[1],Length(Text^))}
        finally
          // restore previous settings
          Pop;
        end;
      end;
    end;
  end;
          {

     if Visible(PInfo) then begin
      AWidth:=PInfo^.CalculateDisp(Width);
      AHeight:=PInfo^.CalculateDisp(Font.Height);
      if (AHeight>0) and (AWidth>0) then begin
        if AHeight>MaxInt then AFont:=0
        else AFont:=PInfo^.GetFont(Font,AHeight,0);
        if AFont<>0 then begin
          OldFont:=SelectObject(PInfo^.DC,AFont);
          StrPCopy(AText,Text);
          BWidth:=LoWord(GetTextExtent(PInfo^.DC,AText,StrLen(AText)));
          if Font.Style and ts_Right<>0 then XMove:=AWidth-BWidth
          else if Font.Style and ts_Center<>0 then XMove:=(AWidth-BWidth) Div 2
          else XMove:=0;
          PInfo^.ConvertToDisp(Position,StartPos);
          if XMove<>0 then begin
            Beta:=Angle*Pi/180;
            Inc(StartPos.X,Round(Cos(Beta)*XMove));
            Inc(StartPos.Y,-Round(Sin(Beta)*XMove));
          end;
          SelectObject(PInfo^.DC,OldFont);
          DeleteObject(AFont);
          AFont:=PInfo^.GetFont(Font,AHeight,Angle);
          OldFont:=SelectObject(PInfo^.DC,AFont);
          TextOut(PInfo^.DC,StartPos.X,StartPos.Y,AText,StrLen(AText));
          SelectObject(PInfo^.DC,OldFont);
          DeleteObject(AFont);
        end
        else begin

        end;
      end;
     end;
           }

  (*
 Clipped:=Needclipping(PInfo);
 if Visible(PInfo) then begin
 AWidth:=PInfo^.CalculateDisp(Width);
    AHeight:=PInfo^.CalculateDisp(Font.Height);
    if (AHeight>0) and (AWidth>0) then begin
      if (AHeight>MaxInt)
          or ((AHeight<minDrawHeight)
              and not PInfo^.Printing) then AFont:=0
      else AFont:=PInfo^.GetFont(Font,AHeight,Angle);
      if AFont<>0 then begin
        if Font.Style and ts_Transparent<>0 then OldBKMode:=SetBkMode(PInfo^.DC,Transparent)
        else OldBKMode:=SetBkMode(PInfo^.DC,Opaque);
        OldFont:=SelectObject(PInfo^.DC,AFont);
        { get information about the font }
        FontDes:=PInfo^.Fonts^.GetFont(Font.Font);
        GetTextMetrics(PInfo^.DC,TextMetrics);
        StrPCopy(Atext,GetText);
        if Clipped then begin
          { calculate rotation of the text }
          Beta:=-Angle*Pi/180;
          IsDBCS:=IsDBCSFont(PInfo^.DC);
          BWidth:=0;
          RealCharCount:=0;
          CharPtr:=AText;
          CharWidths[0]:=0.0;
          { calculate widths of the characters, must differentiate between true-type
          | and vector-fonts because with rotated fonts windows gives character-width
          | for true-type and frame-width for vector-fonts, raster-fonts are not used
          | so they need not be handled separately }
          if (FontDes<>NIL) and (FontDes^.IsTrueType) then while CharPtr^<>#0 do begin
            { calculate next character-code, if DBCS use AnsiNext }
            if IsDBCS then begin
              NextPtr:=AnsiNext(CharPtr);
              if NextPtr-CharPtr>1 then CharCode:=Byte(CharPtr^) Shl 8+Byte((CharPtr+1)^)
              else CharCode:=Byte(CharPtr^);
            end
            else begin
              CharCode:=Byte(CharPtr^);
              NextPtr:=CharPtr+1;
            end;
            { truetype-font -> use GetABCWidths }
            GetCharABCWidths(PInfo^.DC,CharCode,CharCode,ABCWidths);
            BWidth:=BWidth+ABCWidths.abcA+ABCWidths.abcB+ABCWidths.abcC-TextMetrics.tmOverhang;
            Inc(RealCharCount);
            CharWidths[RealCharCount]:=BWidth;
            CharPtr:=NextPtr;
          end
          else begin
            CosAlpha:=Abs(Cos(Beta));
            Cos2Alpha:=Abs(Cos(2*Beta));
            SinAlpha:=Abs(Sin(Beta));
            while CharPtr^<>#0 do begin
              { calculate next character-code, if DBCS, use AnsiNext }
              if IsDBCS then NextPtr:=AnsiNext(CharPtr)
              else NextPtr:=CharPtr+1;
              { other-font -> use GetCharWidth and calculate char-width out of cell-width }
              TextSize:=GetTextExtent(PInfo^.DC,CharPtr,NextPtr-CharPtr);
              CWidth:=LoWord(TextSize);
              CHeight:=HiWord(TextSize);
              { handle 45� rotation-case, uses expected font-height instead of real font-height }
              if Cos2Alpha<0.05 then BWidth:=BWidth+Abs(CWidth/CosAlpha-AHeight)-TextMetrics.tmOverhang
              else BWidth:=BWidth+Abs((CWidth*CosAlpha-CHeight*SinAlpha)/Cos2Alpha)-TextMetrics.tmOverhang;
              Inc(RealCharCount);
              CharWidths[RealCharCount]:=BWidth;
              CharPtr:=NextPtr;
            end;
          end;
          { calculate rotation of the text }
          CosAlpha:=Cos(Beta);
          SinAlpha:=Sin(Beta);
          { calculate text-position depending on text-alignment }
          PInfo^.ConvertToDispDouble(Position,XPosition,YPosition);
          if Font.Style and ts_Right<>0 then XMove:=AWidth-BWidth
          else if Font.Style and ts_Center<>0 then XMove:=(AWidth-BWidth)/2
          else XMove:=0.0;
          { calculate lower-left point of text }
          XPosition:=LimitToLong(XPosition+CosAlpha*XMove);
          YPosition:=LimitToLong(YPosition+SinAlpha*XMove);
          { calculate border of text }
          ClipPoints:=TClipPointList.Create;
          Point:=GrPoint(XPosition,YPosition);
          ClipPoints.Add(Point);
          Point:=GrPoint(Point.X+CosAlpha*BWidth,Point.Y+SinAlpha*BWidth);
          ClipPoints.Add(Point);
          Point:=GrPoint(Point.X+SinAlpha*AHeight,Point.Y-CosAlpha*AHeight);
          ClipPoints.Add(Point);
          Point:=GrPoint(Point.X-CosAlpha*BWidth,Point.Y-SinAlpha*BWidth);
          ClipPoints.Add(Point);
          { clip text-border against clipping-rectangle }
          ClipPoly(PInfo^.ClipRect,ClipPoints,is_PolyGon);
          { calculate visible characters }
          if ClipPoints.Count>0 then begin
            { start with full baseline }
            MinPosition:=BWidth;
            MaxPosition:=0;
            { project all border-points to the baseline and calculate max and min }
            for Cnt:=0 to ClipPoints.Count-1 do begin
              Value:=((ClipPoints[Cnt].X-XPosition)*CosAlpha+(ClipPoints[Cnt].Y-YPosition)*SinAlpha);
              if Value<MinPosition then MinPosition:=Value;
              if Value>MaxPosition then MaxPosition:=Value;
            end;
            { max and min now specify the visible part of the text - must now be
            | aligned to character-positions }
            StartChar:=0;
            LastChar:=RealCharCount;
            Value:=0;
            for Cnt:=0 to RealCharCount-1 do begin
              if CharWidths[Cnt]<MinPosition then begin
                StartChar:=Cnt;
                Value:=CharWidths[Cnt];
              end;
              if CharWidths[Cnt]>MaxPosition then begin
                LastChar:=Cnt;
                break;
              end;
            end;
            { determine character to start with (take dbcs into account) }
            if IsDBCS then begin
              CharPtr:=AText;
              for Cnt:=1 to StartChar do CharPtr:=AnsiNext(CharPtr);
              NextPtr:=CharPtr;
              for Cnt:=1 to LastChar-StartChar do NextPtr:=AnsiNext(NextPtr);
            end
            else begin
              CharPtr:=@AText[StartChar];
              NextPtr:=@AText[LastChar];
            end;
            { calculate position of first character to display }
            XPosition:=XPosition+CosAlpha*Value;
            YPosition:=YPosition+SinAlpha*Value;
            TextOut(PInfo^.DC,Round(XPosition),Round(YPosition),CharPtr,NextPtr-CharPtr);
          end;
          { free memory of clipping-info }
          ClipPoints.Free;
        end
        else begin
          PInfo^.ConvertToDisp(Position,StartPos);
          if Font.Style and (ts_Right or ts_Center)<>0 then begin
            { calculate size of the text }
            TextSize:=GetTextExtent(PInfo^.DC,AText,StrLen(AText));
            BWidth:=LoWord(TextSize);
            Beta:=-Angle*Pi/180;
            if (FontDes<>NIL) and not (FontDes^.IsTrueType) then begin
              { it's not a true-type font so bounds-rectangle is given instead of width}
              CHeight:=HiWord(TextSize);
              CosAlpha:=Abs(Cos(Beta));
              SinAlpha:=Abs(Sin(Beta));
              Cos2Alpha:=Abs(Cos(2*Beta));
              { handle special case with 45� rotation }
              if Cos2Alpha<0.05 then BWidth:=Abs(BWidth/CosAlpha-AHeight)
              else BWidth:=Abs((BWidth*CosAlpha-CHeight*SinAlpha)/Cos2Alpha);
            end;
            { calculate amount of move from upper-left point to text-startpoint }  
            if Font.Style and ts_Right<>0 then XMove:=AWidth-BWidth
            else XMove:=(AWidth-BWidth)/2.0;
            CosAlpha:=Cos(Beta);
            SinAlpha:=Sin(Beta);
            { calculate real position }
            StartPos.X:=LimitToLong(StartPos.X+CosAlpha*XMove);
            StartPos.Y:=LimitToLong(StartPos.Y+SinAlpha*XMove);
          end;
          { draw the text }
          TextOut(PInfo^.DC,StartPos.X,StartPos.Y,AText,StrLen(AText));
        end;
        SelectObject(PInfo^.DC,OldFont);
        DeleteObject(AFont);
        SetBkMode(PInfo^.DC,OldBKMode);
      end
      else begin
        APoly:=GetBorder;
        OldMode:=PInfo^.DrawMode;
        PInfo^.DrawMode:=PInfo^.DrawMode or dm_NoPattern;
        APoly^.Draw(PInfo,Clipped);
        Dispose(APoly,Done);
        PInfo^.DrawMode:=OldMode;
      end;
    end;

  end; {visible}

    *)
  end;



 Function  CText.SelectByPoint(PInfo:PPaint;Point:TDPoint;ObjType:TObjectTypes):boolean;
   var CPoly        : PCPoly;
   begin

    Result:=false;
    if ot_Text in ObjType then begin
      CPoly:=GetBorder;
      if CPoly^.SelectByPoint(PInfo,Point,[ot_CPoly])<>NIL then
          Result:=true;
      Dispose(CPoly,Done);
    end;

  end;

 Function  CText.SelectByRect(PInfo:PPaint;Rect:TDRect;Inside:Boolean):Boolean;
   begin
   end;

Procedure CText.Dra;
var a : longint;
  begin
    a:=Index;
  end;

Function CText.GetObjType:Word;
  begin
    Result:=ot_Text;
  end;

Function CText.GivePosX:longint;
  begin
    Result:=Position.x;
  end;

Function CText.GivePosY:Longint;
  begin
    Result:=Position.y;
  end;

end.
