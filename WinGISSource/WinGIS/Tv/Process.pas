unit Process;
{TODO :}
{WICHTIG!!! Dirty in Swapout behandeln; kein zusaetzliches Auslagern wenn Seite nicht beschrieben}
{pagetab,addrtab,freetable speichern und laden}
{Table groesse dynamisch, villeicht nicht in Seiten-Server STruktur}
{Laden : lediglich Swapin von Seiten, keine Load funktion mehr}

{Filenamen: am_child; udraw}

{calculate cliprect in usymbol und ucirc}
interface
uses SysUtils,WinTypes,ULbase,UClass,upage,FrameTab,Uheap,NLBase,Classes,uaddr,FileMan,Ubucketl,
     udef,utils;

{Page Table kann im Moment noch nicht expandiert werden. Sollte daher von Haus aus gross genug sein!}
const isPageTable = 16380;       {WICHTIG!!!}    {isFreeTable in UfileMan auf den selben Wert setzen}
      isAddrTable = 16380;       {WICHTIG!!!}
      isPageTableKernel = 10;
      {isBucketSize = 100;}
               {<=}
 isFrameTable =  isFrameTableCapacity;     {Initialwert}      {125=8MB   500=32MB}     {WICHTIG!!!}
Type TProcess = class
       AddrTable    : TAddrTab;
       Page0        : PPage;
      public
       FileMan      : TFileMan;
       PageTable    : TPageTable;
       FreeList     : TNLBase;
       Constructor Create(AFreeList:TNLBase);
       Procedure   OnLoad(AFileName:string);
       Procedure   OnStore(AfileName:string;Item:word);
       Function    GetPage(var AIndex:Longint):PPage;
       Function    IndexToPage(AIndex:integer):PPage;
       Procedure   SetActualPage(AIndex:Integer);
       Procedure   SetSTart(Item:Pointer);
       Function    GetStart:Pointer;
       Procedure   DeAllocateFreelist;
       Destructor  Destroy; override;

       Procedure   OnNew(AFileName:string);
       Procedure   OnOpen(AFileName:string);
       Procedure   OnClose;
       Procedure   OnUpdate;
       Procedure   OnImport;
       Procedure   LoadAll;

       Procedure SwapOut(APageNum:Longint);             {Param fix!}
       Function  SwapIn(APageNum,LockPage:Longint):PPage;    {Param fix!}
       Procedure SaveOut(APageNum:Longint);
       Procedure CreateSwapFile(AFileName:string);
       Procedure CloseSwapFile;
       Procedure SaveOutAll;
       PRocedure PutCheckId(AId:Longint);
       Procedure PutObjCount(ACount:Longint);
       Function  GetObjCount:Longint;
     end;

     TKernel  = class(TMainClass)
       PageTable    : TPageTable;
      public
       Page0        : PPage;
       FreeList     : TNLBase;
       Schedule     : TNLBase;
       ProcessList  : TNLBase;
       FrameManager : TFrameTable;
       MClass       : TMainClass;
       {BucketList   : TBucketList;}
       constructor Create;
       constructor Create2;
       Destructor Destroy; override;
       Procedure Reset;
     end;
implementation

uses uptree;

Constructor TProcess.Create;
  begin
    inherited Create;
    PLongInt(Self)^:=Longint(TProcess);
    PageTable:=TPageTable.Create(isPageTable); {Page TAble in Kernel, braucht nicht in Prosess(=stream)gespeichert werden}
    {ab hier wird alles im Self Process allokiert}
    GetHeapMan.ActualProcess:=Self;
    AddrTable:=TAddrTab.Create(isAddrTable);
    FileMan:=TFileMan.Create;
    FreeList:=AFreeList;
  end;

Destructor TProcess.Destroy;
  begin
    {DeAllocateFreeList;}   {automatisch durch swap out!}
    PageTable.Free;
    AddrTable.Free;
    FileMan.Free;
    inherited Destroy;
  end;

Constructor TKernel.Create;
  begin
    inherited Create;
    Plongint(Self)^:=Longint(TKernel);
    ProcessList:=TNLBase.Create;
    Schedule:=TNLBase.Create;
    FreeList:=TNLBase.Create;
    PageTable:=TPageTable.Create(isPageTableKernel);
    FrameManager:=TFrameTable.Create(isFrameTable,FreeList);
    MClass:=TMainClass.Create;
  end;

  Constructor TKernel.Create2;
  begin
    inherited Create;
    Plongint(Self)^:=Longint(TKernel);
    ProcessList:=TNLBase.Create;
    Schedule:=TNLBase.Create;
    FreeList:=TNLBase.Create;
    PageTable:=TPageTable.Create(isPageTableKernel);
    {FrameManager:=TFrameTable.Create(isFrameTable,FreeList); }
    MClass:=TMainClass.Create;
  end;


Destructor TKernel.Destroy;
  begin
    { MClass.Free;}
    ProcessList.Free;
    Schedule.Free;
    FreeList.Free;
    FrameManager.Free;
    PageTable.Free;
    inherited Destroy;
  end;

Procedure TKernel.Reset;
  begin

    ProcessList.Free;
    Schedule.Free;
    {FreeList.Clear;}
    PageTable.Free;

    ProcessList:=TNLBase.Create;
    Schedule:=TNLBase.Create;
    PageTable:=TPageTable.Create(isPageTableKernel);

    FrameManager.clear(FreeList);  {<----}
  end;

Function TProcess.IndexToPage
   (
   AIndex   : integer
   )
   : PPage;
  begin
    Result:=TKernel(GetHeapMan.FKernel).Framemanager.FrameTable^[PageTable[AIndex]].Address;
  end;

Procedure TProcess.SetActualPage
   (
   AIndex  : integer
   );
  begin
    GetHeapMan.SetActualPage(TKernel(GetHeapMan.FKernel).Framemanager.FrameTable^[PageTable[AIndex]].Address);
  end;
(*
 Function TProcess.GetPage
   : PPage;
   var  PageIndex   : integer;
        FrameIndex  : integer;
        TempProcess : Pointer;
   begin
     {if TKernel(GetHeapMan.FKernel).FreeList.count = 0 then raise ElistError.Create('FreeList empty!');}
     if TKernel(GetHeapMan.FKernel).FreeList.count = 0 then begin
       TempProcess:=GetHeapMan.ActualProcess;
       GetHeapMan.SetKernel;
       TKernel(GetHeapMan.FKernel).Framemanager.Expand;
       GetHeapMan.ActualProcess:=TempProcess;
       {raise ElistError.Create('FreeList empty!');}
     end;
     FrameIndex:=TFreeListItem(TKernel(GetHeapMan.FKernel).FreeList.Head).Index;
     TKernel(GetHeapMan.FKernel).FreeList.DeleteHead;
     PageTable.Add(FrameIndex);
     PageIndex:=PageTable.Count-1;
     TKernel(GetHeapMan.FKernel).Framemanager.FrameTable^[FrameIndex].PageIndex:=PageIndex;
     {
     if PageIndex=0 then begin
       Page0:= TKernel(GetHeapMan.FKernel).Framemanager.FrameTable^[FrameIndex].Address;
       GetHeapMan.SetActualPage(Page0);
     end;
     }
     Result:=TKernel(GetHeapMan.FKernel).Framemanager.FrameTable^[FrameIndex].Address;
     GetHeapMan.SetActualPage(Result);
   end;
*)
Function TProcess.GetPage(var AIndex : LongInt)
   : PPage;
{   var  PageIndex   : integer;
        FrameIndex  : Longint;
        TempProcess : Pointer;
 }
   begin
     Result:=GetHeapMan.GetPageLinear(AIndex);
   end;

Procedure TProcess.SetStart
   (
   Item : Pointer
   );
  var test : Pointer;
  begin
    PPage(IndextoPage(0))^.Start:=Item;
    test:=  PPage(IndextoPage(0))^.Start;
  end;

Function TProcess.GetStart
  : Pointer;
 var page : PPage;
 begin
   page:=IndexToPage(0);
   Result:=Page^.Start;
 end;

Procedure TProcess.OnLoad
   (
   AFileName : string
   );
  var cnt      : word;
      AItem    : TFreeListItem;
      Page     : Pointer;
      AStream  : TFileStream;
      ACount   : integer;
      dummy    : Longint;
      help     : Longint;
      aptr     : PPage;
      h,m,s,ms : word;
      t1,t2    : double;
      sh,sm,ss,sms : string;
      stime        : string;
      a:integer;
  begin

    {writeln(GetHeapMan.Fout,'Beginn Load');}
    t1:=now;
    if PageTable.Count=0 then begin
      GetHeapMan.ActualProcess:=Self;
      AStream:=TFileStream.Create(AFileName,fmOpenRead);
      AStream.Read(ACount,sizeof(integer));
      for cnt:= 0 to ACount-1 do begin
        Page:=GetPage(dummy);
        help:=PPage(Page)^.FrameId;
        AStream.Read(Page^,1024*Blocksize);
        PPage(Page)^.OwnerProcess:=Self;
        PPage(Page)^.FrameId:=help;
        {aptr:=TKernel(GetHeapMan.FKernel).FrameManager.FrameTable^[1].Address;}
        GetHeapMan.UpdatePointers(Page);
      end;
      AStream.Free;
    end else raise ElistError.Create('just open!');
    t2:=now;
    t2:=now;
    DecodeTime(t2-t1,h,m,s,ms);
    str(h,sh); str(m,sm); str(s,ss); str(ms,sms);
    stime:='';
    stime:=stime+sh+' '+sm+' '+ss+' '+sms;
     {
    SwapIn(0);
    SwapIn(1);
    }
    {writeln(GetHeapMan.Fout,'Load Ende '+stime);}
  end;

Procedure TProcess.OnStore
   (
   AFileName : string;
   Item      : word
   );
  var cnt      : word;
      AItem    : TFreeListItem;
      Page     : PPage;
      s        : TFileStream;
      temp     : integer;
      ACount   : integer;
      ptable   : pointer;
      index    : Longint;
  begin
    s:=TFileStream.Create(AFileName,fmCreate);
    temp:=PageTable.Count;
    s.write(temp,Sizeof(integer));
    Acount:=PageTable.count;
    for cnt:= 0 to PageTable.Count-1 do begin
      {
      AItem:=TFreeListItem.Create(PageTable[cnt]);
      FreeList.Add(AItem);
      }
      index:=PageTable[cnt];
      Page:=TKernel(GetHeapMan.FKernel).FrameManager.FrameTable^[index].Address;
      if cnt=0 then begin
        PWord(page)^:=Item;
      end;
      s.Write(Page^,1024*Blocksize);
    end;
    s.Free;
    {
    PageTable.Free;
    PageTable:=NIL;
    }
    {}
   {  ptable:=TKernel(GetHeapMan.FKernel).FrameManager.frametable^[0].address;}
   { for cnt:=0 to ACount-1 do GetHeapMan.Update Pointers(IndexToPage(cnt));}
    {}
  end;

 Procedure TProcess.DeAllocateFreeList;
 var cnt      : word;
     AItem    : TFreeListItem;
     Page     : PPage;
   begin
     {GetHeapMan.SetKernel;}
     for cnt:= 0 to PageTable.Count-1 do begin
       AItem:=TFreeListItem.Create(PageTable[cnt]);      {FrameIndex nach AItem}
       FreeList.Add(AItem);
       Page:=TKernel(GetHeapMan.FKernel).FrameManager.FrameTable^[PageTable[cnt]].Address;
       GetHeapMan.InitGlobalPage(Page);
     end;

   end;

Procedure TProcess.SwapOut
   (
   APageNum  : Longint
   );
  var AIndex:Longint;
      APage : Pointer;
      FrameIndex : Longint;
      AFrameTableEntry : PFrameTableEntry;
      AItem : TFreeLIstItem;
      TempProcess : TProcess;
      HelpPage : PPage;
      ASize    : word;
      a        : word;
      help     : longint;
      AAdress  : TPVertexItem;
  begin
    if apagenum>=425 then begin
      a:=0;
    end;

    if (APageNum<0)or(APageNum>=Pagetable.count) then raise ElistError.create('page write fault');
    APage:=IndextoPage(APageNum);
    {AddrIndex:=AddrTable.AddAt(APageNum);}

    AIndex:=AddrTable[APageNum];


    FrameIndex:=PageTable[APageNum];
    AFrameTableEntry:=@TKernel(GetHeapMan.FKernel).FrameManager.FrameTable^[FrameIndex];


    if AFrameTableEntry^.Modified=1 then begin                      {DIRTY!!!!!!!}
      if AIndex=-1 then begin
        AIndex:=FileMan.GetFreeBlockIndex;
        AddrTable[APageNum]:=AIndex;         {SeitenAdresse in file = Aindex*ItemSize}
        {GetHeapMan.SwapOutPages.AddPageNumber(APageNum);}
      end;
      FileMan.WritePosition(AIndex,APage);
      if GetHeapMan.Doit then begin
        {GetHeapMan.WriteF(inttostr(APageNum)+'   '+inttostr(FrameIndex));}
      end;
      {AAdress:=TPVertexItem(MakeLongPointer($7BA4,GetSelector(APage)));}
    end;


    {GetHeapMan.RemovePointers(APage);  {andere Seiten die ueber externe Pointer auf diese Page zugreifen}
    PPage(APage)^.FrameId:=-1;
   { FrameIndex:=PageTable[APageNum];}

    {AFrameTableEntry.Address:=NIL;
    AFrameTableEntry.Handle:=0;   }

    {GetHeapMan.DelLinearPage(AFrameTableEntry.PageIndex);}
    {PageTable.Delete(AFrameTableEntry^.PageIndex);}

    AFrameTableEntry^.PageIndex:=-1;
    AFrameTableEntry^.Locked:=0;
    AFrameTableEntry^.Modified:=0;
    AFrameTableEntry^.LockCount:=0;
    AFrameTableEntry^.Free:=0;             {neu}

    PageTable[APageNum]:=-1;      {FramIndex = -1, dh. Seite ist ausgelagert}
    help:=Pagetable[apagenum];

    TempProcess:=Self;
    GetHeapMan.SetKernel;



    AItem:=TFreeLIstItem.Create(FrameIndex);                   {?}      {in welchem Prozess?}

  

    TKernel(GetHeapMan.FKernel).FreeList.Add(AItem);          {?}


    GetHeapMan.SetProcess(TempProcess);
    {Checke Speicher bei Freelist.DeleteHead in uheap}
  end;

Function TProcess.SwapIn
   (
   APageNum  : Longint;
   LockPage  : Longint
   ):
   PPage;
  var AIndex : Longint;
      APage  : PPage;
      FileIndex : Longint;
      AFrameTableEntry : PFrameTableEntry;
      FrameIndex  : Longint;

      AFrameClass : TFrameTable;
      i           : integer;
      AAdress : TPVertexItem;
      SCount  : longint;
  begin
    if APageNum = 1207 then begin
      i:=0;
    end;
    if (APageNum<0)or(APageNum>=Pagetable.Capacity) then begin
      raise ElistError.create('page read fault');
    end;

    AIndex:=AddrTable[APageNum];

    if AIndex=-1 then raise ElistError.Create(inttostr(apagenum)+
    ' Fehler bei swap in; diese Seite wurde noch nie ausgelagert!');
    {except
      Addrtable.print('d:\ade');
      PageTable.print('d:\page');
    end; }
    {APage:=GetPage(FrameIndex);  {Unlocked and if Freelist=0 then swap out}

    APage:=GetHeapMan.GetSwapPage(FrameIndex);



    FileMan.REadPosition(AIndex,Pointer(APage));
    AAdress:=TPVertexItem(MakeLongPointer($7BA4,GetSelector(APage)));

    APage^.OwnerProcess:=Self;                                         {new}
    APage^.FrameId:=FrameIndex;

    AFrameTableEntry:=@TKernel(GetHeapMan.FKernel).FrameManager.FrameTable^[FrameIndex];
    AFrameTableEntry^.PageIndex:=APageNum;
    AFrameTableEntry^.Locked:=0;
    AFrameTableEntry^.Modified:=0;
    AFrameTableEntry^.LockCount:=0;
    AFrameTableEntry^.Free:=1;

    PageTable[APageNum]:=FrameIndex;

    GetHeapMan.UpdatePointers(APage);
    {GetHeapMan.SetActualPage(APage);}   {!!!}

    {
    AframeClass:=TKernel(GetHeapMan.FKernel).FrameManager;
    for i:=0 to Aframeclass.capacity-1 do begin
      APage:=AframeClass.FrameTable^[i].Address;
      GetHeapMan.UpdatePointers(APage);
    end;}
    Result:=APage;
    SCount:=GetHeapMan.Swapincount;
    inc(GetHeapMan.Swapincount);
  end;

Procedure TProcess.CreateSwapFile
   (
   AFileName : string
   );
  begin
    FileMan.CreateFile(AfileName);
  end;

Procedure TProcess.CloseSwapFile;
   begin
     FileMan.Closefile;
   end;


Procedure   TProcess.OnNew(AFileName:string);
  begin
    fileMan.CreateFile(AFileName);
  end;

Procedure   TProcess.OnOpen(AFileName:string);
  begin
    FileMan.OpenFile(AfileName);
  end;

Procedure   TProcess.OnImport;
  begin
  end;

Procedure TProcess.LoadAll;
  var i  : longint;
      astr  : string;
  begin
    if fileman=nil then raise ElistError.Create('fileman nil');
    fileMan.Load;
    astr:=FileMan.GetFileName;
    fileMan.SetAddrTAb;
    AddrTable:=TAddrTAb.Load(FileMan.GetSTream);
    PageTable.count:=AddrTAble.Count;
    {for i:= 0 to AddrtAble.Count-1 do begin}
    for i:=0 to 2 do begin
      if AddrTable[i]<>-1 then SwapIn(i,0);
    end;
    SwapIn(Pagetable.count-1,0);
  end;

{lagert beim Schliessen alle restlichen noch nicht ausgelagerten Seiten in Frametab aus}
Procedure TProcess.OnClose;
  var i          : longint;
      cnt        : longint;
      PageNum    : longint;
  begin
    SaveOutAll;
    if fileman=nil then raise ElistError.Create('fileman nil');
    cnt:=0;
    for i:=0 to  TKernel(GetHeapMan.FKernel).Framemanager.Capacity-1 do begin
      {if TKernel(GetHeapMan.FKernel).FrameManager.FrameTable^[i].Free=1 then inc(cnt);}
      {free=0 -> auslagern}
      if TKernel(GetHeapMan.FKernel).FrameManager.FrameTable^[i].Modified=1 then begin
        PageNum:=TKernel(GetHeapMan.FKernel).FrameManager.FrameTable^[i].PageIndex;
        if PageNum>-1 then begin
          SwapOut(PageNum);
        end;
      end;

    end;

    FileMan.PutFileName('Franz Thalhammer');
    fileMan.PutAddrTAb;
    AddrTable.Store(FileMan.GetSTream);
    FileMan.Store;
    SwapIn(0,0);

  end;

  Procedure TProcess.OnUpdate;
  var i          : longint;
      cnt        : longint;
      PageNum    : longint;
  begin
    if fileman=nil then raise ElistError.Create('fileman nil');
    cnt:=0;
    for i:=0 to  TKernel(GetHeapMan.FKernel).Framemanager.Capacity-1 do begin
      {if TKernel(GetHeapMan.FKernel).FrameManager.FrameTable^[i].Free=1 then inc(cnt);}
      {free=0 -> auslagern}
      if TKernel(GetHeapMan.FKernel).FrameManager.FrameTable^[i].Modified=1 then begin
        PageNum:=TKernel(GetHeapMan.FKernel).FrameManager.FrameTable^[i].PageIndex;
        if PageNum>-1 then begin
          SwapOut(PageNum);
        end;
      end;

    end;

  end;

Procedure TProcess.SaveOutAll;
  var i: integer;
      PageNum : longint;
      SwapProcess  : TProcess;
      f            : textfile;
  begin
  {
   assignfile(f,'e:\wgis\test.dat');
   rewrite(f);
   }
   for i:=0 to  TKernel(GetHeapMan.FKernel).Framemanager.Capacity-1 do begin
      {if TKernel(GetHeapMan.FKernel).FrameManager.FrameTable^[i].Free=1 then inc(cnt);}
      {free=0 -> auslagern}
      if TKernel(GetHeapMan.FKernel).FrameManager.FrameTable^[i].Modified=1 then begin
        PageNum:=TKernel(GetHeapMan.FKernel).FrameManager.FrameTable^[i].PageIndex;
        if (PageNum>-1)and(PageNum<>Pagetable.count-1) then begin
          SwapOut(PageNum);
         { writeln(f,Pagenum,'    ',i);}
        end;
      end;
    end;
    for i:=0 to 2 do begin
      if AddrTable[i]<>-1 then SwapIn(i,0);
    end;
    {SwapIn(Pagetable.count-1,0);}
    {CloseFile(f);}
  end;


Procedure TProcess.SaveOut
   (
   APageNum  : Longint
   );
  var AIndex:Longint;
      APage : Pointer;
      FrameIndex : Longint;
      AFrameTableEntry : PFrameTableEntry;
      AItem : TFreeLIstItem;
      TempProcess : TProcess;
  begin


    {if (APageNum<0)or(APageNum>=Pagetable.count) then raise ElistError.create('page write fault');}
    APage:=IndextoPage(APageNum);
    AIndex:=AddrTable[APageNum];
    FrameIndex:=PageTable[APageNum];
    AFrameTableEntry:=@TKernel(GetHeapMan.FKernel).FrameManager.FrameTable^[FrameIndex];

    if AIndex <>-1 then FileMan.WritePosition(AIndex,APage) else
    ElistError.Create('Index -1');
  end;



PRocedure TProcess.PutCheckId(AId:Longint);
  begin
    FileMan.putcheckId(AId);
  end;

Procedure TProcess.PutObjCount(ACount:Longint);
  begin
    FileMan.PutObjCount(ACount);
  end;

Function  TProcess.GetObjCount:Longint;
  begin
    Result:=FileMan.GetObjCount;
  end;
end.


