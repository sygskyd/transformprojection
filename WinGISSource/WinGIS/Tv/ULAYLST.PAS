 unit Ulaylst;

{Durch Umemlst ersetzt ( Layer)}
{Fehler in Add-> Umemlist.add}

interface
uses Classes,UClass,WinTypes,UHeap;

const isItems  = 256;

Type
     TMarray = array[0..isitems-1] of TExtPtr;

     TLayerLstItem = class;
     TLayerLst     = class(TMainClass)
      private
       Function ReadFHead:TLayerLstItem;
       Procedure WriteFHead(Item:TLayerLstItem);
       Function ReadFTail:TLayerLstItem;
       Procedure WriteFTail(Item:TLayerLstItem);
      public
       Fcount          : LongInt;
       FHead           : TExtPtr;
       FTail           : TExtPtr;
       Constructor Create;
       {Function Add(Item:TLayerLstItem):Boolean; {256-er Bloecke}
       {Add Item : Pointer(Textptr) autm. expand}
       {}
       Procedure Add(AItem : TExtPtr);
       Procedure Expand;
       {Procedure Delete(var Item:TLayerLstItem);}
       Function AddHead(Item:TLayerLstItem):Boolean;
       Function GetHead:TLayerLstItem;
       Procedure DeleteHead;
       Function AddTail(Item:TLayerLstItem):Boolean;
       Procedure SetTail(Item:TLayerLstItem);
       Function GetTail:TLayerLstItem;
       Procedure DeleteTail;
       Function GetNext(p:TLayerLstItem):TLayerLstItem;
       Property Count:longint read FCount write FCount;
       Property Head:TLayerLstItem read ReadFHead write WriteFHead;
       Property Tail:TLayerLstItem read ReadFTail write WriteFTail;
       Destructor Destroy; override;
       Procedure AddPointers; override;
       Procedure DelPointers; override;
     end;

     TLayerLstItem = class(TMainClass)
       Function ReadFNext:TLayerLstItem;
       Procedure WriteFNext(Item:TLayerLstItem);
       Function ReadFPrev:TLayerLstItem;
       Procedure WriteFPrev(Item:TLayerLstItem);
      public
       FNext         : TExtPtr;
       FPrev         : TExtPtr;
       Items         : TMArray;
       Count         : longint;
       Constructor Create;
       Procedure Add(AItem :TExtPtr);
       Property Next:TLayerLstItem read ReadFNext write WriteFNext;
       Property Prev:TLayerLstItem read ReadFPrev write WriteFPrev;
       Destructor Destroy; override;
       Procedure AddPointers; override;
       Procedure DelPointers; override;
     end;

implementation



Constructor TLayerLst.Create;
  begin
   
    Expand;
  end;

Destructor TLayerLst.Destroy;
  var p,help:TLayerLstItem;
  begin
    if Self<>GetHeapMan.NullPointer then begin
    p:=Head;
    while p<>NIL do begin
       help:=p;
       p:=p.Next;
       help.free;
       end;
      DelPointers;
    end;
    inherited Destroy;
  end;

Procedure TLayerLst.AddPointers;
  begin
    GetHeapMan.AddVmtPtr(Self);
  end;

Procedure TLayerLst.DelPointers;
  begin
    GetHeapMan.DelVmtPtr(Self);
  end;

Constructor TLayerLstItem.Create;
  begin
    
  end;

Destructor TLayerLstItem.Destroy;
  begin
    DelPointers;
    inherited Destroy;
  end;

Procedure TLayerLstItem.AddPointers;
  begin
    GetHeapMan.AddVmtPtr(Self);
  end;

Procedure TLayerLstItem.DelPointers;
  begin
    GetHeapMan.DelVmtPtr(Self);
  end;

function TLayerLst.ReadFHead
  : TLayerLstItem;
  begin
    {Result:=TLayerLstItem(ReadPointer(@FHead));}
    Result:=FHead.Get;
  end;

Procedure TLayerLst.WriteFHead
   (
   Item   : TLayerLstItem
   );
  begin
    {WritePointer(@FHead,Item);}
    FHead.put(Item);
  end;

function TLayerLst.ReadFTail
  : TLayerLstItem;
  begin
    {Result:=TLayerLstItem(ReadPointer(@FTail));}
    Result:=FTail.Get;
  end;

Procedure TLayerLst.WriteFTail
   (
   Item   : TLayerLstItem
   );
  begin
    {WritePointer(@FTail,Item);}
    FTail.Put(Item);
  end;


function TLayerLstItem.ReadFNext
  : TLayerLstItem;
  begin
    {Result:=TLayerLstItem(ReadPointer(@FNext));}
    Result:=FNext.Get;
  end;

Procedure TLayerLstItem.WriteFNext
   (
   Item   : TLayerLstItem
   );
  begin
    {WritePointer(@FNext,Item);}
    Fnext.Put(Item);
  end;

function TLayerLstItem.ReadFPrev
  : TLayerLstItem;
  begin
    {Result:=TLayerLstItem(ReadPointer(@FPrev));}
    Result:=FPrev.Get;
  end;

Procedure TLayerLstItem.WriteFPrev
   (
   Item   : TLayerLstItem
   );
  begin
    {WritePointer(@FPrev,Item);}
    FPrev.Put(Item);
  end;

Procedure TLayerLstItem.Add(AItem: TExtPtr);
  begin
    if Count < IsItems then begin
      Items[Count]:=AItem;
      inc(Count);
      Modified(Self);
    end else raise ElistError.create('TLayerLstItem voll');
  end;

Procedure TLayerLst.Expand;
 var Item:TLayerLstItem;
 begin
   Item:=TLayerLstItem.Create;
   if Head<>NIL then begin
      Tail.next:=Item;
      Item.prev:=Tail;
      Tail:=Item;
      Modified(Tail.next);
      Modified(Item.prev);
      end
    else begin
      Head:=Item;
      Tail:=Item;
      Modified(Head);
      end;
   inc(FCount);
   Modified(Tail);
   inc(GetHeapMan.Layerlstcnt);
 end;




Function TLayerLst.AddHead
  (
  Item:TLayerLstItem
  )
  :Boolean;
 begin
   if Head<>NIL then begin
      Item.next:=Head;
      Head.prev:=Item;
      Head:=Item;
      end
    else begin
      Head:=Item;
      Tail:=Item;
      end;
   inc(FCount);
   Result:=TRUE;
 end;

Function TLayerLst.AddTail
  (
  Item:TLayerLstItem
  )
  :Boolean;
 begin
   if Head<>NIL then begin
      Tail.next:=Item;
      Item.next:=Item; {Schwanz}
      end
    else begin
      Head:=Item;
      Tail:=Item;
      Item.next:=Head;
      end;
   inc(FCount);
   Result:=TRUE;
 end;

{Kopf loeschen allgemein}
Procedure TLayerLst.DeleteHead;
  var h:TLayerLstItem;
  begin
    h:=Head;
    Head:=h.next;
    if Head<>NIL then head.prev:=NIL;                                                              {!!!}
    h.Free;
    dec(Fcount);
  end;

{nur Schwanz loeschen, der hinter Tail zum Sortieren eingefuegt wurde}
Procedure TLayerLst.DeleteTail;
  var h:TLayerLstItem;
  begin
    h:=Tail.next;
    h.Free;
    Tail.next:=NIL;
    dec(FCount);
  end;

Procedure TLayerLst.SetTail
   (
   ITem:TLayerLstItem
   );
  begin
    Tail:=Item;
  end;


Function TLayerLst.GetNext
   (
   p:TLayerLstItem
   )
   :TLayerLstItem;
  begin
  Result:=p.next;
  end;

  {
Procedure TLayerLst.Delete
   (
    var Item:TLayerLstItem
   );
  var hItem : TLayerLstItem;
  begin

   If Item <> Head then Item.prev.next:=Item.next;
   If Item <> Tail then Item.next.prev:=Item.prev;
   If Item = Head then Head:=Item.next;
   If Item = Tail then Tail:=Item.prev;
   hItem:=Item;
   Item:=Item.next;
   hItem.free;
   dec(FCount);
  end;
}

Function TLayerLst.GetHead
   :TLayerLstItem;
  begin
  Result:=Head;
  end;

Function TLayerLst.GetTail
   :TLayerLstItem;
  begin
  Result:=Tail;
  end;


Procedure TLayerLst.Add(AItem:TExtPtr);
  var Item     : TLayerLstItem;
  begin
    Item:=Tail;
    If Item.Count < IsItems then Item.Add(AItem) else
    if Item.count = IsItems then Expand;
  end;



end.
