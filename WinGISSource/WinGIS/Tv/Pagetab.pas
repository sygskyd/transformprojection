unit Pagetab;
{Page Table kann im Moment noch nicht expandiert werden. Sollte daher von Haus aus gross genug sein!}
Interface

Uses Classes, UClass, WinTypes,UHeap, udef;

Type

     PPageTableEntry  = ^TPageTableEntry;
     TPageTableEntry  = record
       FrameIndex : integer;
       {swappedin  : Word;}
     end;

     PPageArray       = ^TPageArray;
     TPageArray       = array[0..16000]of TPageTableEntry;       {max 1 GByte}

     TPageTable           = Class(TMainClass)
       FFItems     : PPageArray;
       FCount      : integer;
       FCapacity   : integer;
       Function    GetItem(AIndex:integer):integer;
       Function    GetItems:Pointer;
       Procedure   SetItem(AIndex:integer;FrameIndex:integer);
       Procedure   SetCapacity(ACapacity:integer);
      Public
       Constructor Create(ACapacity:integer);
       Destructor  Destroy; override;
       Function    Add(FrameIndex:LongInt):integer;
       Function    AddatPageIndex(FrameIndex,PageIndex:LongInt):integer;
       Function    Delete(PageIndex:LongInt):integer;
       Property    Capacity:integer read FCapacity write SetCapacity;
       Procedure   Clear;
       Property    Count:integer read FCount;
       Function    Expand:TPageTable;
       Function    First:Pointer;
       Property    Items[AIndex:integer]:integer read GetItem write SetItem; default;
       Function    Last:Pointer;
       Property    List:Pointer read GetItems;
       Property    FItems:PPageArray read FFItems write FFItems;
     end;

Implementation

Constructor TPageTable.Create
   (
   ACapacity       : integer
   );
  var p     : Pointer;
      i     : integer;
  begin
    PLongInt(Self)^:=longint(TPageTable);
    Capacity:=ACapacity;
    for i:=0 to FCapacity-1 do begin
      FFItems^[i].FrameIndex:=-1;
    end;
  end;

Destructor TPageTable.Destroy;
  begin
    if FCapacity > 0 then GetHeapMan.DeAllocate(FItems,FCapacity * sizeof(TPageTableEntry));
  end;

Function TPageTable.Add
   (
   FrameIndex   : Longint
   )
   : integer;
  begin
    if Count=Capacity then Expand;
    FItems^[Fcount].FrameIndex:=FrameIndex;
    {FItems^[Fcount].SwappedIn:=1;}
    inc(FCount);
  end;

Function TPageTable.AddAtPageIndex
   (
   FrameIndex   : Longint;
   PageIndex    : Longint
   )
   : integer;
  begin
    if Count=Capacity then Expand;
    FItems^[PageIndex].FrameIndex:=FrameIndex;
    {FItems^[PageIndex].SwappedIn:=1;}
    inc(FCount);
  end;

Function TPageTable.Delete
   (
   PageIndex   : Longint
   )
   : integer;
  begin
    FItems^[PageIndex].FrameIndex:=-1;
  {  FItems^[PageIndex].SwappedIn:=0;}
    dec(FCount);
  end;

Procedure TPageTable.Clear;
  begin
    FillChar(FItems^,FCapacity*sizeof(TPageTableEntry),#0);
    FCount:=0;
  end;

Procedure TPageTable.SetCapacity
   (
   ACapacity       : integer
   );
  var HelpPtr      : PPageArray;
  begin
    if ACapacity>FCapacity then begin
      if FCapacity>0 then begin
        HelpPtr:=GetHeapMan.MAllocate(GetHeapMan.ActualPage,ACapacity*sizeof(TPageTableEntry));
        if HelpPtr<>NIL then begin
          System.Move(FItems^[0],HelpPtr^[0],FCapacity*sizeof(TPageTableEntry));
          GetHeapMan.DeAllocate(FItems,FCapacity*sizeof(TPageTableEntry));
          FItems:=HelpPtr;
        end;
      end
      else begin
        FItems:=GetHeapMan.MAllocate(GetHeapMan.ActualPage,ACapacity*sizeof(TPageTableEntry));
      end;
      FCapacity:=ACapacity;
    end;
  end;

Function TPageTable.Expand
   : TPageTable;
  begin
    raise ElistError.Create('kann nicht erweitert werden');
    if Capacity<4 then Capacity:=Capacity+4
    else if Capacity<9 then Capacity:=Capacity+8
    else Capacity:=Capacity+16;
  end;

Function TPageTable.First
   : Pointer;
  begin
    if Count>0 then Result:=FItems
    else Result:=NIL;
  end;

Function TPageTable.Last
   : Pointer;
  begin
    if Count>0 then Result:=@FItems^[(Count-1)*sizeof(TPageTableEntry)]
    else Result:=NIL;
  end;

Function TPageTable.GetItem
   (
   AIndex          : integer
   )
   : integer;
  begin
    Result:=FItems^[AIndex].FrameIndex;
    {Abfrage ob ausgelagert! sonst return 0}
  end;

Procedure TPageTable.SetItem
   (
   AIndex          : integer;
   FrameIndex      : integer
   );
  begin
    FItems^[AIndex].FrameIndex:=FrameIndex;
  end;

Function TPageTable.GetItems
   : Pointer;
  begin
    Result:=FItems;
  end;

end.
