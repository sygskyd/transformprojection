unit Urbase;

Interface

Uses Classes,Sysutils,udef;

Type PBooleanArray      = ^TBooleanArray;
     TBooleanArray      = Array[0..MaxInt128] of Boolean;

     PByteArray         = ^TByteArray;
     TByteArray         = Array[0..MaxInt128] of Byte;

     {*************************************************************************}
     { Class TRecList                                                          }
     {-------------------------------------------------------------------------}
     { TRecList ist eine dynamische Liste f�r Records. Im Gegensatz zu TList   }
     { werden nicht Pointer sondern die Records selbs in einem Array           }
     { gespeichert. Die Gr��e der Records mu� im Constructor �bergeben         }
     { werden. Ansonsten sind die gleichen Funktionen wie f�r TList definiert. }
     { Lediglich die Parametertypen unterscheiden sich etwas. Das Array f�r    }
     { die Records kann maximal 64 kBytes gro� werden.                         }
     {-------------------------------------------------------------------------}
     { FItems           = Zeiger auf das Array mit den Records                 }
     { FCount           = aktuelle Anzahl von Eintr�gen                        }
     { FCapacity        = aktuelle Gr��e des Arrays in Anzahl von Eintr�gen    }
     { FItemSize        = Gr��e eines Records in Bytes                         }
     {*************************************************************************}
     TRecList           = Class(TObject)
      Protected
       FItems           : PByteArray;
       FCount           : Longint;
       FCapacity        : Longint;
       FItemSize        : Longint;
       FUsed            : PBooleanArray;
       FUsedPtr         : Longint;
       {Function    GetItem(AIndex:Longint):Pointer;
       Procedure   SetItem(AIndex:Longint;Item:Pointer);}
       Procedure   SetCapacity(ACapacity:Longint);

       Procedure   OnLoadItems(S:TStream); virtual;
       Procedure   OnStoreItems(S:TStream); virtual;
      Public
       Function    GetItems:Pointer;
       Constructor Create(ACapacity:Longint;AItemSize:Longint);
       Constructor Load(S:TStream);
       Destructor  Destroy; override;
       Function    Add(var Item):Longint;
       Function    AddAt(var Item;AIndex:Longint):Boolean;
       Function    At(Item:Pointer):Longint;
       Property    Capacity:Longint read FCapacity write SetCapacity;
       Procedure   Clear;
       Property    Count:Longint read FCount write FCount;
       Procedure   Delete(Index:Longint);
       Function    Expand:TRecList;
       Function    First:Pointer;
       {Property    Items[AIndex:Longint]:Pointer read GetItem write SetItem; default;}
       Function    Last:Pointer;
       Property    List:Pointer read GetItems;
       {Function    Available(var Index:Longint):Boolean;
       Function    FirstItem:Pointer;              }
       Property    ItemSize:Longint read FItemSize;
       Procedure   Store(S:TStream); virtual;
       procedure   Print(AName:string);
     end;


Implementation

Constructor TRecList.Create
   (
   ACapacity       : Longint;
   AItemSize       : Longint
   );
  begin
    inherited Create;
    FItemSize:=AItemSize;
    Capacity:=ACapacity;
  end;

Destructor TRecList.Destroy;
  begin
    if Capacity>0 then begin
      FreeMem(FItems,Capacity*FItemSize);
      FreeMem(FUsed,Capacity);
    end;
    inherited Destroy;
  end;

Function TRecList.Add
   (
   var Item
   )
   : Longint;
  begin
    if Count=Capacity then Expand;
    while FUsed^[FUsedPtr] do FUsedPtr:=(FUsedPtr+1) Mod Capacity;
    System.Move(Item,FItems^[FUsedPtr*FItemSize],FItemSize);
    FUsed^[FUsedPtr]:=TRUE;
    Inc(FCount);
    Result:=FUsedPtr;
    FUsedPtr:=(FUsedPtr+1) Mod Capacity;
  end;

Function TRecList.AddAt(var Item;AIndex:Longint):Boolean;
  begin
    Result:=false;
    if Count=Capacity then Expand;
    if FUsed^[AIndex] then begin
      Result:=True;
      System.Move(Item,FItems^[AIndex*FItemSize],FItemSize);
      Inc(Fcount);
    end;
  end;

Function TRecList.At
   (
    Item:Pointer
   )
    : Longint;
   var help:longint;
  begin
    help:=((longint(Item)-longint(FItems)) div FItemSize);
    Result:=help;
  end;


Procedure TRecList.Clear;
  begin
    FCount:=0;     {????}
  end;

Procedure TRecList.SetCapacity
   (
   ACapacity       : Longint
   );
  var HelpPtr      : PByteArray;
      HelpFree     : PBooleanArray;
  begin
    if ACapacity>FCapacity then begin
      if FCapacity>0 then begin
        GetMem(HelpPtr,ACapacity*FItemSize);
        System.Move(FItems^[0],HelpPtr^[0],FCapacity*FItemSize);
        FreeMem(FItems,FCapacity*FItemSize);
        FItems:=HelpPtr;
        FillChar(FItems^[FCapacity],(ACapacity-FCapacity)*FItemSize,$FF);     {***************************??}
        GetMem(HelpFree,ACapacity);
        System.Move(FUsed^[0],HelpFree^[0],FCapacity);
        FillChar(HelpFree^[FCapacity],ACapacity-FCapacity,#0);
        FreeMem(FUsed,FCapacity);
        FUsed:=HelpFree;
      end
      else begin
        GetMem(FItems,ACapacity*FItemSize);
        FillChar(FItems^[FCapacity],(ACapacity-FCapacity)*FItemSize,$FF);     {***************************??}
        GetMem(FUsed,ACapacity);
        FillChar(FUsed^[0],ACapacity,#0);
      end;
      FCapacity:=ACapacity;
    end;
  end;

Procedure TRecList.Delete
   (
   Index           : Longint
   );
  begin
    {$IFDEF DEBUG}
    if Index>Capacity then Raise EListError.Create('Listindex out of range');
    if not FUsed^[Index] then Raise EListError.Create('Access to a free listitem');
    {$ENDIF}
    FUsed^[Index]:=FALSE;
    Dec(FCount);
  end;

Function TRecList.Expand
   : TRecList;
  begin
    if Capacity<4 then Capacity:=Capacity+4
    else if Capacity<9 then Capacity:=Capacity+8
    else Capacity:=Capacity+16;
  end;

Function TRecList.First
   : Pointer;
  begin
    if Count>0 then Result:=FItems
    else Result:=NIL;
  end;

Function TRecList.Last
   : Pointer;
  begin
    if Count>0 then Result:=@FItems^[(Count-1)*FItemSize]
    else Result:=NIL;
  end;

(*
Function TRecList.GetItem
   (
   AIndex          : Longint
   )
   : Pointer;
  begin
    {$IFDEF DEBUG}
    if AIndex>Capacity then Raise EListError.Create('Listindex out of range');
    {$ENDIF}
    if not FUsed^[AIndex] then Result:=NIL
    else Result:=@FItems^[AIndex*FItemSize];
  end;

Procedure TRecList.SetItem
   (
   AIndex          : Longint;
   Item            : Pointer
   );
  begin
    {$IFDEF DEBUG}
    if AIndex>Capacity then Raise EListError.Create('Listindex out of range');
    if not FUsed^[AIndex] then Raise EListError.Create('Access to a free listitem');
    {$ENDIF}
    System.Move(Item^,FItems^[AIndex*FItemSize],FItemSize);
  end;
  *)
Function TRecList.GetItems
   : Pointer;
  begin
    Result:=FItems;
  end;
         {
Function TRecList.Available
   (
   var Index:Longint
   )
   :Boolean;
  begin
    Result:=False;
    while Index < Capacity do begin
      if Items[Index]<>NIL then begin
        Result:=TRUE;
        break;
      end;
      inc(Index);
    end;
  end;
          }
          {
Function TRecList.FirstItem:Pointer;
  var index : Longint;
  begin
    Result:=NIL;
    index:=0;
    while  index < Capacity do begin
      if Items[Index]<>NIL then begin
        Result:=@FItems^[Index*FItemSize];
        break;
      end;
      inc(index);
    end;
  end;

           }

Constructor TRecList.Load
   (
   S               : TStream
   );
  var ACapacity    : Longint;
  begin
    S.Read(ACapacity,SizeOf(ACapacity));
    S.Read(FCount,SizeOf(FCount));
    S.Read(FItemSize,SizeOf(FItemSize));
    S.Read(FUsedPtr,SizeOf(FUsedPtr));
    Capacity:=ACapacity;
    S.Read(FUsed^,Capacity);
    OnLoadItems(S);
  end;

Procedure TRecList.Store
   (
   S               : TStream
   );
  begin
    S.Write(FCapacity,SizeOf(FCapacity));
    S.Write(FCount,SizeOf(FCount));
    S.Write(FItemSize,SizeOf(FItemSize));
    S.Write(FUsedPtr,SizeOf(FUsedPtr));
    S.Write(FUsed^,Capacity);
    OnStoreItems(S);
  end;

Procedure TRecList.OnLoadItems
   (
   S               : TStream
   );
  begin
    S.Read(FItems^,Capacity*FItemSize);
  end;

Procedure TRecList.OnStoreItems
   (
   S               : TStream
   );
  begin
    S.Write(FItems^,Capacity*FItemSize);
  end;

PRocedure TReclist.Print(AName:string);
  var i              : Longint;
      fn             : textfile;
      help           : longint;
  begin
    assignfile(fn,Aname);
    rewrite(fn);
    for i:=0 to count-1 do begin
        System.Move(FItems^[I*FItemSize],help,FItemSize);
        writeln(fn,help);
    end;
    closefile(fn);
  end;

end.
