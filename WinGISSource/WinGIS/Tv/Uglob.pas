unit Uglob;

interface
uses uclass,udrwsel,classes;

type TGlobal = class
       Ptr          : TExtPtr;    {reserviert}
       cnt          : word;       {reserviert}   {Zaehler fuer Pointliste}
       APtr         : TExtPtr;    {jederzeit frei}
       LeafNumber   : longint;    {res. fuer Aufbau Mem-Quadtree}
       DrawLst      : TbitLst; {REFERENZ( erz.in udraw;) res. fuer isSel- isDraw Abfrage eines Objektes}
       SelLst       : TBitLst;
       hptr         : Pointer;
       bPtr         : pointer;
       BucketList   : TExtPtrLst;
       counter      : longint;
       Root         : Pointer;
       Procedure ResetLeafNumber;
       Procedure IncLeafNumber;
       constructor Create;
       destructor destroy; override;
     end;

     PExtPtr2 = ^TExtPtr2;
     TExtPtr2 = Object             {Ab jetzt Schnittstelle fuer externe Pointer!!!}
       {PTr     : Pointer;}
       Pn      : Word;
       Offset  : Word;

     end;

{var GlobalObj   : TGlobal;}
implementation



constructor TGlobal.create;
  begin
    inherited Create;
    Bucketlist:=TExtPtrLst.create;
  end;

Destructor TGlobal.Destroy;
  begin
    BucketList.free;
    inherited destroy;
  end;

Procedure TGlobal.ResetLeafNumber;
  begin
    Leafnumber:=0;
  end;

PRocedure TGlobal.IncLeafNumber;
  begin
    inc(LeafNumber);
  end;

begin
end.
