
{mit Items[cnt] Zugriff auf Cpoint von PolyLine}
{PRocedure CPoly.CalculateClipRect;}
{Function CPoly.SearchForLine}
unit Upoly;
{getpoints Lock, Unlock?}
interface
uses Classes,uheap,uview,uclass,Ulbase,wintypes,WinProcs,udef,am_paint,am_def,upoint,clipping,uptlst,grtools;

Type PPoints       = ^TPoints;
     TPoints       = Array [1..16380] of TPoint;

     CPoly = class(CView)
      protected
       {FData     : TExtPtr;}
       FData2    : TExtPtr;
       {Function   ReadData:TSLBase;
       Procedure  WriteData(Item:TSLBase);}
       Destructor Destroy; override;
       Procedure  AddPointers; override;
       Procedure  DelPointers; override;
       function   GetItem(AIndex:longint):CPoint;
       function   GetCount:longint;
       Function   GetDAta:TPointLst;
      public
       Length     : Double;
       {Procedure  CalculateClipRect;}
       Procedure  GetPoints(PInfo:PPaint;var Points:PPoints);
       Procedure  FreePoints(Points:PPoints);
       Constructor Create(ALimit:longint);
       {Property   Data:TSLBase read ReadData write WRiteData;}
       Property   Data:TPointLst read GetData;
       Procedure  Draw(PInfo:PPaint); override;
       Function   InsertPoint(Point:CPoint):Boolean;
       Function    SelectByPoint(PInfo:PPaint;Point:TDPoint;ObjType:TObjectTypes):boolean; override;
       Function    SelectByRect(PInfo:PPaint;Rect:TDRect;Inside:Boolean):Boolean; override;
       Function    SearchForLine(PInfo:PPaint;var Point:TDPoint;ObjType:TObjectTypes;ADist:LongInt;
                     var PointInd:Longint):boolean; virtual;
       Procedure   GetClippedPoints(PInfo:PPaint;var Points:TClipPointList); virtual;
       Property   Items[AIndex:Longint]:CPoint read GetItem;
       Property   Count : longint read GetCount;
       Function    GetObjType:Word; override;
       Function    ClipByPoint(PInfo:PPaint;Point:TDPoint;ObjType:Word;var Cnt:Integer;
         var Rpoint:TDPoint;Radius:Longint):CView;
       Function    ClipForLine(PInfo:PPaint;var Point:TDPoint;ObjType:Word;ADist:LongInt;
         var PointInd:Integer;var MCnt:Integer;Radius:Longint):CView;
     end;


implementation


Constructor CPoly.Create(ALimit:Longint);
 var help : pointer;
  begin

    {Data:=TSLBase.Create;}


    Lockpage(self);
    help:=TPointLst.Create(ALimit);
    FData2.put(help);
    unlockpage(self);

  end;


Destructor CPoly.Destroy;
  begin
    DelPointers;
    inherited Destroy;
  end;

Procedure CPoly.AddPointers;
  begin
    GetHeapMan.AddVmtPtr(Self);
  end;

Procedure CPoly.DelPointers;
  begin
    GetHeapMan.DelVmtPtr(Self);
  end;





Procedure CPoly.GetPoints
   (
   PInfo           : PPaint;
   var Points      : PPoints
   );
  var
      X : Clong;
      Y : Clong;
      Cnt : Longint;
      APoint : TDPoint;
      BPoint : CPoint;
      p      : TPointLst;
      pt     : TPointLstItem;
      ht     : TPointLstItem;
      ABlock : TPtArray;
      ACount : integer;
      i      : integer;
  begin

     cnt:=1;
     p:=fdata2.Get;
     GetMem(Points,Word(SizeOf(TPoint))*p.Count);
     pt:=p.head;
     while pt<>NIL do begin
       ht:=Pt;
       ht.lockpage(ht);
       ht.getblock(@ABlock,acount);

       for i:=0 to ACount-1 do begin
         APoint.x:=ABlock[i].x;
         APoint.y:=ABlock[i].y;
         PInfo^.ConvertToDisp(APoint,Points^[cnt]);

         inc(cnt);
       end;

       pt:=ht.next;
       ht.unlockpage(ht);
     end;

  end;



function CPoly.GetItem(AIndex:Longint):CPoint;
  var p      : TPointLst;
      i      : integer;
      BPoint : CPoint;
  begin

    lockpage(self);
     {
    p:=FData2.Get;
    p.start;
    for i:=1 to AIndex do p.next;
    BPOint:=p.item;
    Result.x:=Bpoint.x;
    Result.y:=BPoint.y;
     }
    p:=FData2.Get;
    Result:=p.items[AIndex];
    unlockpage(self);

  end;

Procedure CPoly.FreePoints
   (
   Points          : PPoints
   );
  begin
    FreeMem(Points,Word(SizeOf(TPoint))*TPointLst(FData2.Get).Count);
  end;

Function CPoly.InsertPoint
   (
   Point  : CPoint
   )
   :Boolean;
  var APointItem : TPointItem;
      alist       : TPointLst;
  begin
    Alist:=FData2.Get;
    AList.Add(Point);
  end;

Procedure CPoly.Draw
   (
   PInfo : PPaint
   );
  var Points       : PPoints;
      Cnt          : Integer;
      OldPen       : THandle;
      OldBrush     : THandle;
      OldR2        : THandle;
      DrawPoints   : Integer;
      ClipPoints   : TClipPointList;
      GrPoint      : TGrPoint;
      Clipped      : Boolean;
  begin

                                         (*
    if Visible(PInfo) then begin
     Clipped:=Needclipping(PInfo);
     if Clipped then begin

       GetClippedPoints(PInfo,ClipPoints);
       ClipPoly(PInfo^.ClipRect,ClipPoints,is_Polyline);
       DrawPoints:=ClipPoints.Count;
       if DrawPoints>0 then begin
         GetMem(Points,Word(SizeOf(TPoint))*DrawPoints);
         for Cnt:=0 to DrawPoints-1 do begin
           GrPoint:=ClipPoints[Cnt];
           Points^[Cnt+1]:=Point(Round(GrPoint.X),Round(GrPoint.Y));
         end;
       end;
       ClipPoints.Free;

     end else begin

       GetPoints(PInfo,Points);
       DrawPoints:=TPointLst(FData2.Get).Count;

     end;

     if Drawpoints>0 then begin
      try
        PolyLine(PInfo^.DC,Points^,DrawPoints);
      except
        MsgBox(PInfo^.HWindow,12000,mb_Ok+mb_IconExClamation,'');
      end;
       FreeMem(Points,Word(DrawPoints)*SizeOf(TPoint));
     end;

    end;
    *)
   if Visible(PInfo) then begin
     Clipped:=Needclipping(PInfo);
     if Clipped then begin
        GetClippedPoints(PInfo,ClipPoints);
        PInfo^.ExtCanvas.ClippedPolyline(ClipPoints,TRUE);
        ClipPoints.Free;
      end
      else begin
        GetPoints(PInfo,Points);
        PInfo^.ExtCanvas.Polyline(Points^,TPointLst(FData2.Get).Count,TRUE);
        //FreeMem(Points,Data^.Count*SizeOf(TPoint));
        FreeMem(Points,(TPointLst(FData2.Get).Count)*SizeOf(TPoint));
      end;
      {
      if PInfo^.GetDrawMode(dm_EditPoly) then begin
        if Clipped then begin
          GetClippedPoints(PInfo,ClipPoints);
          ClipPolyline(ClipPoints,PInfo^.ClipRect);
          DrawPoints:=ClipPoints.Count;
          if DrawPoints>0 then begin
            GetMem(Points,Word(SizeOf(TPoint))*DrawPoints);
            for Cnt:=0 to DrawPoints-1 do begin
              GrPoint:=ClipPoints[Cnt];
              Points^[Cnt+1]:=Point(Round(GrPoint.X),Round(GrPoint.Y));
            end;
          end;
          ClipPoints.Free;
        end
        else begin
          GetPoints(PInfo,Points);
          DrawPoints:=Data^.Count;
        end;
        if DrawPoints>0 then begin
          LPToDP(PInfo^.DC,Points^,DrawPoints);
          SetMapMode(PInfo^.DC,mm_Text);
          OldPen:=SelectObject(PInfo^.DC,GetStockObject(Black_Pen));
          OldBrush:=SelectObject(PInfo^.DC,GetStockObject(Hollow_Brush));
          OldR2:=SetRop2(PInfo^.DC,r2_NOTXORPen);
          for Cnt:=1 to DrawPoints do with Points^[Cnt] do
              Rectangle(PInfo^.DC,X-HandleSize,Y-HandleSize,X+HandleSize1,Y+HandleSize1);
          SelectObject(PInfo^.DC,OldPen);
          SelectObject(PInfo^.DC,OldBrush);
          SetROP2(PInfo^.DC,OldR2);
          SetMapMode(PInfo^.ExtCanvas.Handle,mm_LoMetric);
          FreeMem(Points,DrawPoints*SizeOf(TPoint));
        end;
      end;
        }
   end;
end;

Function    CPoly.SelectByPoint(PInfo:PPaint;Point:TDPoint;ObjType:TObjectTypes):Boolean;
var MDist        : LongInt;
    AIndex       : Longint;
  begin
    MDist:=PInfo^.CalculateDraw(poMaxDist);
    Result:=SearchForLine(PInfo,Point,ObjType,MDist,AIndex);
  end;

Function CPoly.SearchForLine
   (
   PInfo           : PPaint;
   var Point       : TDPoint;          { Mausposition                                  }
   ObjType         : TObjectTypes;             { erlaubte Objekttypen                          }
   ADist           : LongInt;          { maximaler (normal) Abstand von der Linie      }
   var PointInd    : Longint           { Anfangspunkt der Linie zu der der Abstand     }
                                       { unterschritten wurde                          }
   )
   : Boolean;
  var Cnt          : Longint;
      CntTo        : longint;
      ExpRect      : TDRect;
      BDist        : Double;
      BPoint       : TDPoint;
      p            : TPointLst;
      prev         : TPointLst;
      X            : Clong;
      Y            : Clong;
      Start        : TDPoint;
      Dest         : TDPoint;
      abort        : boolean;
      normal       : Boolean;
  begin

    abort:=false;
    Result:=false;
    {if ObjType and GetObjType<>0 then }begin
      ExpRect.InitbyREct(ClipREct);

      ExpRect.Grow(ADist);

      if ExpRect.PointInside(Point) then begin
        Cnt:=0;

        P:=FData2.Get;
        p.Start;

        while not p.Dest do begin
          Start.x:=p.Item.x;
          Start.y:=p.Item.y;
          Dest.x:=p.ItemNext.x;
          Dest.y:=p.ItemNext.y;
          normal:=PInfo^.NormalDistance(@Start,@Dest,@Point,BDist,BPoint);
          if(BDist<ADist)and(normal) then begin
            abort:=true;
            result:=true;
            Point.Init(BPoint.X,BPoint.Y);
            break;
          end;
         p.next;
         inc(cnt);
        end;



      end;
    
    end;

  end;




Function    CPoly.SelectByRect(PInfo:PPaint;Rect:TDRect;Inside:Boolean):Boolean;
var MDist        : LongInt;
  begin
  mdist:=0;
  end;



Procedure   Cpoly.GetClippedPoints(PInfo:PPaint;var Points:TClipPointList);
  var Count        : Integer;
      Point        : TGrPoint;
      cnt          : integer;
      APoint       : TDPoint;
      BPoint       : CPoint;
      p            : TPointLst;
      pt     : TPointLstItem;
      ht     : TPointLstItem;
      ABlock : TPtArray;
      ACount : integer;
      i      : integer;
  begin
  {
    cnt:=0;
    p:=FData2.Get;
    p.Start;
    Points:=TClipPointList.Create;
    Points.Capacity:=p.Count;

     while not p.Dest do begin
       BPoint:=p.Item;
       inc(cnt);
       APoint.x:=BPoint.x;
       APoint.y:=BPoint.y;
       PInfo^.ConvertToDispDouble(APoint,Point.X,Point.Y);
       Points.Add(Point);
       p.next;
     end;
     }
       cnt:=1;
     p:=fdata2.Get;
     Points:=TClipPointList.Create;
     Points.Capacity:=p.Count;

     pt:=p.head;
     while pt<>NIL do begin
       ht:=Pt;
       ht.lockpage(ht);
       ht.getblock(@ABlock,acount);

       for i:=0 to ACount-1 do begin
         APoint.x:=ABlock[i].x;
         APoint.y:=ABlock[i].y;
         PInfo^.ConvertToDispDouble(APoint,Point.X,Point.Y);
         Points.Add(Point);

         inc(cnt);
       end;

       pt:=ht.next;
       ht.unlockpage(ht);
     end;
  end;

function  CPoly.GetCount:Longint;
   begin
     Result:=TPointLst(FData2.Get).Count
   end;

Function CPoly.GetObjType:Word;
  begin
    Result:=ot_Poly;
  end;

Function CPoly.ClipForLine
   (
   PInfo           : PPaint;
   var Point       : TDPoint;          { Mausposition                                  }
   ObjType         : Word;             { erlaubte Objekttypen                          }
   ADist           : LongInt;          { maximaler (normal) Abstand von der Linie      }
   var PointInd    : Integer;          { Anfangspunkt der Linie zu der der Abstand     }
   var MCnt        : Integer;          { unterschritten wurde                          }
   Radius          : Longint
   )
   : CView;
  var ExpRect      : TDRect;
      BDist        : Double;
      BPoint       : TDPoint;
      Cnt          : Integer;
      CntTo        : Integer;
      point1       : TDPOInt;
      point2       : TDPoint;
  begin
    Result:=NIL;
    MCnt:=-1;
    if ObjType and GetObjType<>0 then begin
      ExpRect.Init;
      ExpRect:=ClipRect;
      ExpRect.Grow(ADist);
      ADist:=Radius;
      if ExpRect.PointInside(Point) then begin
        Cnt:=0;
        CntTo:=Count-1;
        point1.x:=Items[cnt].x;
        point1.y:=Items[cnt].y;
        point2.x:=Items[cnt+1].x;
        point2.y:=Items[cnt+1].y;
        while (Cnt<CntTo) and (not(PInfo^.NormalDistance(@point1,@point2,@Point,BDist,BPoint))or (BDist>ADist)) do begin
           point1.x:=Items[cnt].x;
           point1.y:=Items[cnt].y;
           point2.x:=Items[cnt+1].x;
           point2.y:=Items[cnt+1].y;
           Inc(Cnt);
        end;
        if Cnt<CntTo then begin
          REsult:=Self;
          Point.Init(BPoint.X,BPoint.Y);
          PointInd:=Cnt;
          MCnt:=Cnt;
        end;
      end;
      ExpRect.Done;
    end;
  end;

Function CPoly.ClipByPoint
   (
   PInfo           : PPaint;
   Point           : TDPoint;
   ObjType         : Word;
   var Cnt         : Integer;
   var RPoint      : TDPoint;
   Radius          : Longint
   )
   : CView;
  var MDist        : LongInt;
      AIndex       : Integer;
  begin
    MDist:=PInfo^.CalculateDraw(poMaxDist);
    Result:=ClipForLine(PInfo,Point,ObjType,MDist,AIndex,Cnt,Radius);
    if cnt<>-1 then RPoint.Init(Point.x,Point.y) else RPoint.Init(0,0);
  end;

Function CPoly.GetData: TPointLst;
  begin
    Lockpage(Self);
    Result:=Fdata2.Get;
    Unlockpage(Self);
  end;


end.


