unit Utbase;

interface
uses sysutils,classes;

type PBase = ^TBase;
     TBase = record
       InpFileName     : string;     {1st Input file ... choose layers}
       HspFileName     : string;     {out}
       HsdFileName     : string;     {out}
       OutFileList     : TStringList;   {out}    {Result : in Basemap umwandeln ampfile-list, save as HS-f : hsd-list}
       hsplist         : TStringlist;   {out}      {save as hs : hsp list}
       hsdList         : TStringList;   {out}
       InpFileLayers   : TStringList;   {in}
       OpenFileList    : TStringList;   {in}


       SerialNumber    : string;
       BaseMap         : Boolean;
       ExtMap          : Boolean;
       AttributesFix   : Boolean;
       isSerialNumber  : Boolean;
       SNmaybemodified : Boolean;
       isPassWd        : Boolean;
       DBUpdate        : Boolean;
       LogFile         : Boolean;
       OK              : Boolean;

     end;

implementation

end.

