unit Am_ar;

Interface
Uses SysUtils,Messages,AM_Coll,Objects,AM_Admin,AM_Def,AM_Index,
AM_Layer,AM_Poly,AM_Proj,AM_CPoly,AM_StdDl,AM_Dlg1,WinProcs,WinTypes,Windos,
SLBase,ConTree,DLBase,Area,Sort,Classes,ResDlg,Controls,ProjStyle;

Const { Konstanten f�r TAreaData.Traverse}
      tr_trace          = 0;
      tr_BackTrace      = 1;
      ar_Envelope       = 0;
      ar_Lines          = 1;
      ar_Area           = 2;
      ar_Merge          = 3;
      ar_Points         = 4;

      { IDs der Dialogelemente                                               }
      id_Radius         = 100;
      id_SourceList     = 101;
      id_DestLayer      = 102;
      id_ErrorLayer     = 130;
      id_IntersectLayer = 131;
      id_Frame          = 132;
      id_Intersect      = 133;
      id_Umfahrende     = 140;
      id_Lines          = 141;
      id_Area           = 142;
      id_Merge          = 143;

Type {**********************************************************************}
     { Object TAreaDlg                                                       }
     {-----------------------------------------------------------------------}
     {***********************************************************************}
     {$IFNDEF AXDLL} // <----------------- AXDLL
     PAreaDlg           = ^TAreaDlg;
     TAreaDlg           = Class(TLayerDlg)
      Public
       LData             : PAreaData;
       Constructor Init(AParent:TComponent;AData:PAreaData;ALayers:PLayers;AProjStyles:TProjectStyles);
       Function    CanClose:Boolean; override;
       Procedure   SetupWindow; override;
     end;
     {$ENDIF} // <----------------- AXDLL

     TAreaObject =  class(TArea)
                     private
                     public
                      CurPos: TDPoint;
                      Constructor Create(AParent:TComponent;Project:PProj);
                      Procedure DoArea;
                      Procedure DoAreaWithoutDlg(SourceLayer: String;DestinationLayer:String; Typ:Integer);
                      Procedure DoAreaWithoutDlg2(SLCnt: Integer;SourceLayer: Array of String;DestinationLayer:String;Typ:Integer);
                      Procedure DoAreaWithoutDlg3(SourceLayer: PLayer;DestinationLayer:PLayer; Mode: Integer);
                      Destructor Destroy; override;
                     end;


      var MyObject:TAreaObject;

//++ Glukhov Bug#199 Build#157 16.05.01
var
// To save the last parameters
  DALastRadius      : LongInt = 5;
//-- Glukhov Bug#199 Build#157 16.05.01

implementation

Uses AM_Main,AM_ProjP, AM_ProjO, UserIntf, StateBar, LayerHndl
  , AM_Circl, AM_Splin    // Glukhov Bug#199 Build#157 27.04.01
  ;

Constructor TAreaObject.Create
   (
   AParent: TComponent;
   Project: PProj
   );
  begin
    inherited Create(Project);
    PProj(Proj)^.NodeList:=NodeList;
  end;

Destructor TAreaObject.Destroy;
  begin
    inherited Destroy;
  end;

Procedure TAreaObject.DoAreaWithoutDlg2
   (
   SLCnt            : Integer;
   SourceLayer      : Array of String;
   DestinationLayer : String;
   Typ              : Integer
   );
  var Cnt,Cnt5     : LongInt;
      AIndex       : PIndex;
      AObject      : PCPoly;
      MyObject     : PPoly;
      Cnt2         : Integer;
      Cnt3         : Integer;
      APoint       : PDPoint;
      ResultLayer  : PLayer;
      ErgebnisLayer: PLayer;
      check        : boolean;
      sh,sm,ss,sms : string;
      stime        : string;
      mem          : longint;
      edit_feld    : array[0..10]of char;
      ALayer       : PLayer;
      atext        : array[0..40]of char;
      AItem        : TSListItem;
      MSort        : TMergeSort;
      MLayer       : PLayer;
      Sum          : longint;
      cntx         : longint;
      actual       : double;
      actualn      : double;
      h,m,s,ms     : word;
      t1,t2        : double;
      SourLayer    : PLayer;
      Island       : Integer;
      Start        : Integer;
      i            : Integer;
   Procedure DoCopyPoly
      (
      Start        : Integer;
      Stop         : Integer
      );
     var Cnt       : Integer;
     begin
       Alist:=TSLBase.Create;
       for Cnt:=Start to Stop do begin
         APoint:=AObject^.Data^.at(Cnt);
         AItem:=TSListItem.Create;
         AItem.x:=APoint^.x;
         AItem.y:=APoint^.y;
         AList.Add(AItem);
       end;
       ConvertPolyToEdge;
       AList.Free;
     end;
//++ Glukhov Bug#199 Build#157 27.04.01
  procedure DoCopyArc;
    var
      Cnt, Num  : Integer;
      Pnt       : TDPoint;
    begin
      Alist:=TSLBase.Create;
      with PProj(Proj).PInfo.VariousSettings do begin
        if bConvRelMistake
        then  Num:= PEllipse(AObject).ApproximatePointsNumber( dbConvRelMistake, True )
        else  Num:= PEllipse(AObject).ApproximatePointsNumber( dbConvAbsMistake, False );
      end;
      Pnt.Init( 0, 0 );
      for Cnt:=0 to Num-1 do begin
        Pnt:= PEllipse(AObject).ApproximatePoint( Cnt, Num );
        AItem:=TSListItem.Create;
        AItem.x:= Pnt.X;
        AItem.y:= Pnt.Y;
        AList.Add(AItem);
      end;
      ConvertPolyToEdge;
      AList.Free;
    end;
//-- Glukhov Bug#199 Build#157 27.04.01
//++ Glukhov Bug#199 Build#157 21.05.01
  procedure DoCopySpline;
    var
      nSeg, iSeg  : Integer;
      nPnt, iPnt  : Integer;
      Pnt         : TDPoint;
    begin
      Alist:=TSLBase.Create;
      Pnt.Init( 0, 0 );
      nSeg:= PSpline(AObject).SegmentsNum;
      // for each spline segment
      for iSeg:=0 to nSeg-1 do begin
        with PProj(Proj).PInfo.VariousSettings do begin
          if bConvRelMistake
          then  nPnt:= PSpline(AObject).ApprSegmPointsNumber( iSeg, dbConvRelMistake, True )
          else  nPnt:= PSpline(AObject).ApprSegmPointsNumber( iSeg, dbConvAbsMistake, False );
        end;
        // for each approximate point
        for iPnt:=0 to nPnt-1 do begin
          Pnt:= PSpline(AObject).ApprSegmPoint( iSeg, iPnt, nPnt );
          AItem:=TSListItem.Create;
          AItem.x:= Pnt.X;
          AItem.y:= Pnt.Y;
          AList.Add(AItem);
        end;
      end;
      ConvertPolyToEdge;
      AList.Free;
    end;
//-- Glukhov Bug#199 Build#157 21.05.01

  begin
    DebugMessage:='';
    FillChar(Data,SizeOf(Data),#0);
//++ Glukhov Bug#199 Build#157 16.05.01
//    Data.Radius:= 5;
    Data.Radius:= DALastRadius;
//-- Glukhov Bug#199 Build#157 16.05.01
    cnt5:=0;
    Data.Frame:=FALSE;
    Data.Trace:=tr_BackTrace;
    Data.ERLayer:=FALSE;
    Data.ISLayer:=FALSE;
    Data.Intersections:=TRUE;
    Data.Area:=Typ;            {Umh�llende}     {ar_Envelope ar_Area}

    if Data.Layers<>NIL then Dispose(Data.Layers,Done);
    Data.Layers:=New(PLongColl,Init(5,5));

    Data.DestLayer:=PProj(Proj)^.Layers^.NameToLayer(DestinationLayer)^.Index;
    for i:=0 to SLCnt-1 do begin
        SourLayer:=PProj(Proj)^.Layers^.NameToLayer(SourceLayer[i]);
        if SourLayer<>NIL then begin
           Data.Layers^.Insert(Pointer(SourLayer^.Index));
        end;
    end;

      if Data.ERLayer then begin
        MLayer:=PProj(Proj)^.Layers^.NameToLayer(GetLangText(3914));
        if MLayer=NIL then MLayer:=PProj(Proj)^.Layers^.InsertLayer(GetLangText(3914),
            $000000FF,lt_Solid,$000000FF,pt_Solid,ilTop,DefaultSymbolFill);
        Data.ErrorLayer:=MLayer^.Index;
      end;

      if Data.ISLayer then begin
        MLayer:=PProj(Proj)^.Layers^.NameToLayer(GetLangText(3915));
        if MLayer=NIL then MLayer:=PProj(Proj)^.Layers^.InsertLayer(GetLangText(3915),
            $00FF0000,lt_Solid,$00FF0000,pt_Solid,ilTop,DefaultSymbolFill);
        Data.IntersectLayer:=MLayer^.Index;
      end;

//!?!?!      if PProj(Proj)^.Layers^.TranspLayer=0 then PProj(Proj)^.Layers^.DetermineTopLayer;

      t1:=now;
      if Data.Radius=0 then Data.Radius:=1;
      STree.Radius:=Data.Radius;
      Radius := Data.Radius;
      DeltaX := Data.Radius;
      DeltaY := Data.Radius;
      PProj(Proj)^.SetCursor(crHourGlass);

      StatusBar.ProgressPanel:=TRUE;
      StatusBar.ProgressText:=GetLangText(4200);
      StatusBar.AbortText:=GetLangText(3909);

      try
        Sum:=0;
        cntx:=0;
        actual:=0;
        for Cnt3:=0 to Data.Layers^.Count-1 do begin
          ALayer:=PProj(Proj)^.Layers^.IndexToLayer(LongInt(Data.Layers^.At(Cnt3)));
          Sum:=Sum+ALayer^.Data^.GetCount;
        end;

        for Cnt3:=0 to Data.Layers^.Count-1 do
        begin
          ALayer:=PProj(Proj)^.Layers^.IndexToLayer(LongInt(Data.Layers^.At(Cnt3)));
          if ALayer^.Data^.GetCount = 0 then AllAbort:=True;
          for Cnt:=0 to ALayer^.Data^.GetCount-1 do
             begin
             if StatusBar.QueryAbort then begin
               AllAbort:=TRUE;
               Break;
             end;
             AIndex:=ALayer^.Data^.At(Cnt);
             AObject:=Pointer(PLayer(PProj(Proj)^.PInfo^.Objects)^.IndexObject(PProj(Proj)^.PInfo,AIndex));
             if (AObject<>NIL)then begin
                actualn:=((100/Sum)*cntx);
                if (actualn-actual)> 1 then begin
                  if StatusBar.QueryAbort then begin
                    AllAbort:=TRUE;
                    Break;
                  end;
                  StatusBar.Progress:=round(actualn);
                  actual:=actualn;
                end;
                inc(cntx);

//++ Glukhov Bug#199 Build#157 27.04.01
                if (AObject^.GetObjType = ot_Spline)  then DoCopySpline;
                if (AObject^.GetObjType = ot_Circle)  then DoCopyArc;
                if (AObject^.GetObjType = ot_Arc)  then DoCopyArc;
//-- Glukhov Bug#199 Build#157 27.04.01
                if ((AObject^.GetObjType=ot_Poly)
                or (AObject^.GetObjType=ot_CPoly)) then
                     begin
                       if AObject^.GetState(sf_IslandArea) then with AObject^ do begin
                         DoCopyPoly(0,IslandInfo^[0]-1);
                         Start:=IslandInfo^[0];
                         for Island:=1 to IslandCount-1 do begin
                           DoCopyPoly(Start,Start+IslandInfo^[Island]-1);
                           Start:=Start+IslandInfo^[Island]+1;
                         end;
                       end
                       else DoCopyPoly(0,AObject^.Data^.Count-1);
                     end;   {Line,Poly}
                end; {AObject<>NIL}
             end;   {1 Layer durchgehen}
        end;  {alle Layer}

        {*******************************************************************}
        {***************** Berechnungs Routine *****************************}
        {*******************************************************************}
         StatusBar.ProgressText:=GetLangText(4201);
         if Data.Frame then begin
           ProjectSize(ProjSize);
           AddFrame(ProjSize);
         end;
         if not AllAbort then begin
           Msort:=TMergeSort.Create(EdgeList);
           MSort.MergeSort;
           MSort.Free;
         end;
         StatusBar.ProgressText:=GetLangText(4202);
         if not AllAbort then EdgeList.completeList;
         if not AllAbort then EdgeList.Count:=SetListCount(EdgeList);
         if not AllAbort then Snap2;
         if not AllAbort then NodeList.Count:=SetListCount(NodeList);

         StatusBar.ProgressText:=GetLangText(4212);
         if not AllAbort then EleminateEqual;
         StatusBar.ProgressText:=GetLangText(4203);
         if not AllAbort then EleminateDouble;

         StatusBar.ProgressText:=GetLangText(4214);
         if not AllAbort then SortLinkList;
         StatusBar.ProgressText:=GetLangText(4204);
         if not AllAbort then SortList(NodeList);

         StatusBar.ProgressText:=GetLangText(4209);
         if not AllAbort then if Data.Intersections then Intersection;
         if not AllAbort then NodeList.Count:=SetListCount(NodeList);

         StatusBar.ProgressText:=GetLangText(4202);
         if not AllAbort then Snap3;
         FinalList.Count:=SetListCount(FinalList);

         StatusBar.ProgressText:=GetLangText(4212);
         if not AllAbort then EleminateEqual2;
         StatusBar.ProgressText:=GetLangText(4203);
         if not AllAbort then EleminateDouble2;              

         StatusBar.ProgressText:=GetLangText(4204);
         if not AllAbort then SortList(FinalList);

         StatusBar.ProgressText:=GetLangText(4211);
         if not AllAbort then MergeLink;
         StatusBar.ProgressText:=GetLangText(4215);
         if ((Data.Area = ar_Area)or(Data.Area = ar_Envelope))and(not AllAbort) then begin
           EleminateLines;
           EleminateLinesBack;
         end;
         {StatusBar.ProgressText:=GetLangText(4206,FALSE);}

         t2:=now;
         DecodeTime(t2-t1,h,m,s,ms);
         str(h,sh); str(m,sm); str(s,ss); str(ms,sms);
         stime:='';
         stime:=stime+sh+' '+sm+' '+ss+' '+sms;
         if not AllAbort then begin
           if (Data.Area = ar_Area)or(Data.Area = ar_Envelope) then BackTrace else
           if Data.Area = ar_Lines then DrawLines;
         end;

        {*******************************************************************}
      finally
        StatusBar.ProgressPanel:=FALSE;
        PProj(Proj)^.SetCursor(crDefault);
        PProj(Proj)^.PInfo^.RedrawScreen(False);
      end;
  end;

Procedure TAreaObject.DoAreaWithoutDlg
   (
   SourceLayer      : String;
   DestinationLayer : String;
   Typ              : Integer
   );
  var Cnt,Cnt5     : LongInt;
      AIndex       : PIndex;
      AObject      : PCPoly;
      MyObject     : PPoly;
      Cnt2         : Integer;
      Cnt3         : Integer;
      APoint       : PDPoint;
      ResultLayer  : PLayer;
      ErgebnisLayer: PLayer;
      check        : boolean;
      sh,sm,ss,sms : string;
      stime        : string;
      mem          : longint;
      edit_feld    : array[0..10]of char;
      ALayer       : PLayer;
      atext        : array[0..40]of char;
      AItem        : TSListItem;
      MSort        : TMergeSort;
      MLayer       : PLayer;
      Sum          : longint;
      cntx         : longint;
      actual       : double;
      actualn      : double;
      h,m,s,ms     : word;
      t1,t2        : double;
      SourLayer    : PLayer;
      Island       : Integer;
      Start        : Integer;
      i            : Integer;
   Procedure DoCopyPoly
      (
      Start        : Integer;
      Stop         : Integer
      );
     var Cnt       : Integer;
     begin
       Alist:=TSLBase.Create;
       for Cnt:=Start to Stop do begin
         APoint:=AObject^.Data^.at(Cnt);
         AItem:=TSListItem.Create;
         AItem.x:=APoint^.x;
         AItem.y:=APoint^.y;
         AList.Add(AItem);
       end;
       ConvertPolyToEdge;
       AList.Free;
     end;
//++ Glukhov Bug#199 Build#157 27.04.01
  procedure DoCopyArc;
    var
      Cnt, Num  : Integer;
      Pnt       : TDPoint;
    begin
      Alist:=TSLBase.Create;
      with PProj(Proj).PInfo.VariousSettings do begin
        if bConvRelMistake
        then  Num:= PEllipse(AObject).ApproximatePointsNumber( dbConvRelMistake, True )
        else  Num:= PEllipse(AObject).ApproximatePointsNumber( dbConvAbsMistake, False );
      end;
      Pnt.Init( 0, 0 );
      for Cnt:=0 to Num-1 do begin
        Pnt:= PEllipse(AObject).ApproximatePoint( Cnt, Num );
        AItem:=TSListItem.Create;
        AItem.x:= Pnt.X;
        AItem.y:= Pnt.Y;
        AList.Add(AItem);
      end;
      ConvertPolyToEdge;
      AList.Free;
    end;
//-- Glukhov Bug#199 Build#157 27.04.01
//++ Glukhov Bug#199 Build#157 21.05.01
  procedure DoCopySpline;
    var
      nSeg, iSeg  : Integer;
      nPnt, iPnt  : Integer;
      Pnt         : TDPoint;
    begin
      Alist:=TSLBase.Create;
      Pnt.Init( 0, 0 );
      nSeg:= PSpline(AObject).SegmentsNum;
      // for each spline segment
      for iSeg:=0 to nSeg-1 do begin
        with PProj(Proj).PInfo.VariousSettings do begin
          if bConvRelMistake
          then  nPnt:= PSpline(AObject).ApprSegmPointsNumber( iSeg, dbConvRelMistake, True )
          else  nPnt:= PSpline(AObject).ApprSegmPointsNumber( iSeg, dbConvAbsMistake, False );
        end;
        // for each approximate point
        for iPnt:=0 to nPnt-1 do begin
          Pnt:= PSpline(AObject).ApprSegmPoint( iSeg, iPnt, nPnt );
          AItem:=TSListItem.Create;
          AItem.x:= Pnt.X;
          AItem.y:= Pnt.Y;
          AList.Add(AItem);
        end;
      end;
      ConvertPolyToEdge;
      AList.Free;
    end;
//-- Glukhov Bug#199 Build#157 21.05.01

  begin
    DebugMessage:='';
    FillChar(Data,SizeOf(Data),#0);
//++ Glukhov Bug#199 Build#157 16.05.01
//    Data.Radius:= 5;
    Data.Radius:= DALastRadius;
//-- Glukhov Bug#199 Build#157 16.05.01
    cnt5:=0;
    Data.Frame:=FALSE;
    Data.Trace:=tr_BackTrace;
    Data.ERLayer:=FALSE;
    Data.ISLayer:=FALSE;
    Data.Intersections:=TRUE;
    Data.Area:=Typ;            {Umh�llende}     {ar_Envelope ar_Area}

    Data.DestLayer:=PProj(Proj)^.Layers^.NameToLayer(DestinationLayer)^.Index;
        SourLayer:=PProj(Proj)^.Layers^.NameToLayer(SourceLayer);
        if Data.Layers<>NIL then Dispose(Data.Layers,Done);
        Data.Layers:=New(PLongColl,Init(5,5));
        if SourLayer<>NIL then begin
           Data.Layers^.Insert(Pointer(SourLayer^.Index));
        end;

      if Data.ERLayer then begin
        MLayer:=PProj(Proj)^.Layers^.NameToLayer(GetLangText(3914));
        if MLayer=NIL then MLayer:=PProj(Proj)^.Layers^.InsertLayer(GetLangText(3914),
            $000000FF,lt_Solid,$000000FF,pt_Solid,ilTop,DefaultSymbolFill);
        Data.ErrorLayer:=MLayer^.Index;
      end;

      if Data.ISLayer then begin
        MLayer:=PProj(Proj)^.Layers^.NameToLayer(GetLangText(3915));
        if MLayer=NIL then MLayer:=PProj(Proj)^.Layers^.InsertLayer(GetLangText(3915),
            $00FF0000,lt_Solid,$00FF0000,pt_Solid,ilTop,DefaultSymbolFill);
        Data.IntersectLayer:=MLayer^.Index;
      end;

//!?!?!      if PProj(Proj)^.Layers^.TranspLayer=0 then PProj(Proj)^.Layers^.DetermineTopLayer;

      t1:=now;
      if Data.Radius=0 then Data.Radius:=1;
      STree.Radius:=Data.Radius;
      Radius := Data.Radius;
      DeltaX := Data.Radius;
      DeltaY := Data.Radius;
      PProj(Proj)^.SetCursor(crHourGlass);

      StatusBar.ProgressPanel:=TRUE;
      StatusBar.ProgressText:=GetLangText(4200);
      StatusBar.AbortText:=GetLangText(3909);

      try
        Sum:=0;
        cntx:=0;
        actual:=0;
        for Cnt3:=0 to Data.Layers^.Count-1 do begin
          ALayer:=PProj(Proj)^.Layers^.IndexToLayer(LongInt(Data.Layers^.At(Cnt3)));
          Sum:=Sum+ALayer^.Data^.GetCount;
        end;

        for Cnt3:=0 to Data.Layers^.Count-1 do
        begin
          ALayer:=PProj(Proj)^.Layers^.IndexToLayer(LongInt(Data.Layers^.At(Cnt3)));
          for Cnt:=0 to ALayer^.Data^.GetCount-1 do
             begin
             if StatusBar.QueryAbort then begin
               AllAbort:=TRUE;
               Break;
             end;
             AIndex:=ALayer^.Data^.At(Cnt);
             AObject:=Pointer(PLayer(PProj(Proj)^.PInfo^.Objects)^.IndexObject(PProj(Proj)^.PInfo,AIndex));
             if (AObject<>NIL)then begin
                actualn:=((100/Sum)*cntx);
                if (actualn-actual)> 1 then begin
                  if StatusBar.QueryAbort then begin
                    AllAbort:=TRUE;
                    Break;
                  end;
                  StatusBar.Progress:=round(actualn);
                  actual:=actualn;
                end;
                inc(cntx);

//++ Glukhov Bug#199 Build#157 27.04.01
                if (AObject^.GetObjType = ot_Spline)  then DoCopySpline;
                if (AObject^.GetObjType = ot_Circle)  then DoCopyArc;
                if (AObject^.GetObjType = ot_Arc)  then DoCopyArc;
//-- Glukhov Bug#199 Build#157 27.04.01
                if ((AObject^.GetObjType=ot_Poly)
                or (AObject^.GetObjType=ot_CPoly)) then
                     begin
                       if AObject^.GetState(sf_IslandArea) then with AObject^ do begin
                         DoCopyPoly(0,IslandInfo^[0]-1);
                         Start:=IslandInfo^[0];
                         for Island:=1 to IslandCount-1 do begin
                           DoCopyPoly(Start,Start+IslandInfo^[Island]-1);
                           Start:=Start+IslandInfo^[Island]+1;
                         end;
                       end
                       else DoCopyPoly(0,AObject^.Data^.Count-1);
                      end;   {Line,Poly}
                end; {AObject<>NIL}
             end;   {1 Layer durchgehen}
        end;  {alle Layer}

        {*******************************************************************}
        {***************** Berechnungs Routine *****************************}
        {*******************************************************************}
         StatusBar.ProgressText:=GetLangText(4201);
         if Data.Frame then begin
           ProjectSize(ProjSize);
           AddFrame(ProjSize);
         end;
         if not AllAbort then begin
           Msort:=TMergeSort.Create(EdgeList);
           MSort.MergeSort;
           MSort.Free;
         end;
         StatusBar.ProgressText:=GetLangText(4202);
         if not AllAbort then EdgeList.completeList;
         if not AllAbort then EdgeList.Count:=SetListCount(EdgeList);
         if not AllAbort then Snap2;
         if not AllAbort then NodeList.Count:=SetListCount(NodeList);

         StatusBar.ProgressText:=GetLangText(4212);
         if not AllAbort then EleminateEqual;
         StatusBar.ProgressText:=GetLangText(4203);
         if not AllAbort then EleminateDouble;

         StatusBar.ProgressText:=GetLangText(4214);
         if not AllAbort then SortLinkList;
         StatusBar.ProgressText:=GetLangText(4204);
         if not AllAbort then SortList(NodeList);

         StatusBar.ProgressText:=GetLangText(4209);
         if not AllAbort then if Data.Intersections then Intersection;
         if not AllAbort then NodeList.Count:=SetListCount(NodeList);

         StatusBar.ProgressText:=GetLangText(4202);
         if not AllAbort then Snap3;
         FinalList.Count:=SetListCount(FinalList);

         StatusBar.ProgressText:=GetLangText(4212);
         if not AllAbort then EleminateEqual2;
         StatusBar.ProgressText:=GetLangText(4203);
         if not AllAbort then EleminateDouble2;

         StatusBar.ProgressText:=GetLangText(4204);
         if not AllAbort then SortList(FinalList);

         StatusBar.ProgressText:=GetLangText(4211);
         if not AllAbort then MergeLink;
         StatusBar.ProgressText:=GetLangText(4215);
         if ((Data.Area = ar_Area)or(Data.Area = ar_Envelope))and(not AllAbort) then begin
           EleminateLines;
           EleminateLinesBack;
         end;
         {StatusBar.ProgressText:=GetLangText(4206,FALSE);}

         t2:=now;
         DecodeTime(t2-t1,h,m,s,ms);
         str(h,sh); str(m,sm); str(s,ss); str(ms,sms);
         stime:='';
         stime:=stime+sh+' '+sm+' '+ss+' '+sms;
         if not AllAbort then begin
           if (Data.Area = ar_Area)or(Data.Area = ar_Envelope) then BackTrace else
           if Data.Area = ar_Lines then DrawLines;
         end;

        {*******************************************************************}
      finally
        StatusBar.ProgressPanel:=FALSE;
      PProj(Proj)^.SetCursor(crDefault);
      end;
  end;

Procedure TAreaObject.DoArea;
  var Cnt,Cnt5     : LongInt;
      AIndex       : PIndex;
      AObject      : PCPoly;
      Cnt3         : Integer;
      APoint       : PDPoint;
      sh,sm,ss,sms : string;
      stime        : string;
      mem          : longint;
      edit_feld    : array[0..10]of char;
      ALayer       : PLayer;
      atext        : array[0..40]of char;
      AItem        : TSListItem;
      MSort        : TMergeSort;
      MLayer       : PLayer;
      Sum          : longint;
      cntx         : longint;
      actual       : double;
      actualn      : double;
      {$IFDEF DEBUG}
      BIndex       : TIndex;
      {$ENDIF}
      h,m,s,ms     : word;
      t1,t2        : double;
      Island       : Integer;
      Start        : Integer;
   Procedure DoCopyPoly
      (
      Start        : Integer;
      Stop         : Integer
      );
     var Cnt       : Integer;
     begin
       Alist:=TSLBase.Create;
       for Cnt:=Start to Stop do begin
         APoint:=AObject^.Data^.at(Cnt);
         AItem:=TSListItem.Create;
         AItem.x:=APoint^.x;
         AItem.y:=APoint^.y;
         AList.Add(AItem);
       end;
       ConvertPolyToEdge;
       AList.Free;
     end;                                         {mf<}
//++ Glukhov Bug#199 Build#157 27.04.01
  procedure DoCopyArc;
    var
      Cnt, Num  : Integer;
      Pnt       : TDPoint;
    begin
      Alist:=TSLBase.Create;
      with PProj(Proj).PInfo.VariousSettings do begin
        if bConvRelMistake
        then  Num:= PEllipse(AObject).ApproximatePointsNumber( dbConvRelMistake, True )
        else  Num:= PEllipse(AObject).ApproximatePointsNumber( dbConvAbsMistake, False );
      end;
      Pnt.Init( 0, 0 );
      for Cnt:=0 to Num-1 do begin
        Pnt:= PEllipse(AObject).ApproximatePoint( Cnt, Num );
        AItem:=TSListItem.Create;
        AItem.x:= Pnt.X;
        AItem.y:= Pnt.Y;
        AList.Add(AItem);
      end;
      ConvertPolyToEdge;
      AList.Free;
    end;
//-- Glukhov Bug#199 Build#157 27.04.01
//++ Glukhov Bug#199 Build#157 21.05.01
  procedure DoCopySpline;
    var
      nSeg, iSeg  : Integer;
      nPnt, iPnt  : Integer;
      Pnt         : TDPoint;
    begin
      Alist:=TSLBase.Create;
      Pnt.Init( 0, 0 );
      nSeg:= PSpline(AObject).SegmentsNum;
      // for each spline segment
      for iSeg:=0 to nSeg-1 do begin
        with PProj(Proj).PInfo.VariousSettings do begin
          if bConvRelMistake
          then  nPnt:= PSpline(AObject).ApprSegmPointsNumber( iSeg, dbConvRelMistake, True )
          else  nPnt:= PSpline(AObject).ApprSegmPointsNumber( iSeg, dbConvAbsMistake, False );
        end;
        // for each approximate point
        for iPnt:=0 to nPnt-1 do begin
          Pnt:= PSpline(AObject).ApprSegmPoint( iSeg, iPnt, nPnt );
          AItem:=TSListItem.Create;
          AItem.x:= Pnt.X;
          AItem.y:= Pnt.Y;
          AList.Add(AItem);
        end;
      end;
      ConvertPolyToEdge;
      AList.Free;
    end;
//-- Glukhov Bug#199 Build#157 21.05.01
  begin
    DebugMessage:='';
    FillChar(Data,SizeOf(Data),#0);
//++ Glukhov Bug#199 Build#157 16.05.01
//    Data.Radius:= 5;
    Data.Radius:= DALastRadius;
//-- Glukhov Bug#199 Build#157 16.05.01
    cnt5:=0;
    Data.Frame:=True;
    Data.Trace:=tr_BackTrace;
    Data.ERLayer:=true;
    Data.ISLayer:=true;
    Data.Intersections:=TRUE;
    Data.Area:=ar_Area;
    {$IFNDEF AXDLL} // <----------------- AXDLL
    if ExecDialog(TAreaDlg.Init(PProj(Proj)^.Parent,@Data,
        PProj(Proj)^.Layers,PProj(Proj)^.PInfo^.ProjStyles))=id_OK then
    {$ENDIF} // <----------------- AXDLL
    begin
//++ Glukhov Bug#199 Build#157 16.05.01
      DALastRadius:= Data.Radius;
//-- Glukhov Bug#199 Build#157 16.05.01
      if Data.ERLayer then begin
        MLayer:=PProj(Proj)^.Layers^.NameToLayer(GetLangText(3914));
        if MLayer=NIL then MLayer:=PProj(Proj)^.Layers^.InsertLayer(GetLangText(3914),
            $000000FF,lt_Solid,$000000FF,pt_Solid,ilTop,DefaultSymbolFill);
        Data.ErrorLayer:=MLayer^.Index;
      end;

      if Data.ISLayer then begin
        MLayer:=PProj(Proj)^.Layers^.NameToLayer(GetLangText(3915));
        if MLayer=NIL then MLayer:=PProj(Proj)^.Layers^.InsertLayer(GetLangText(3915),
            $00FF0000,lt_Solid,$00FF0000,pt_Solid,ilTop,DefaultSymbolFill);
        Data.IntersectLayer:=MLayer^.Index;
      end;
      {
      if Data.Area= ar_Envelope then begin
        MLayer:=PProj(Proj)^.Layers^.NameToLayer(GetLangText(3920));
        if MLayer=NIL then MLayer:=PProj(Proj)^.Layers^.InsertLayer(GetLangText(3920),
            $000000FF,lt_Solid,$000000FF,pt_Solid,ilTop);
        Data.EnvelopeLayer:=MLayer^.Index;
      end;    }

      t1:=now;
      if Data.Radius=0 then Data.Radius:=1;
      STree.Radius:=Data.Radius;
      Radius := Data.Radius;
      DeltaX := Data.Radius;
      DeltaY := Data.Radius;
      PProj(Proj)^.SetCursor(crHourGlass);
      
      StatusBar.ProgressPanel:=TRUE;
      StatusBar.ProgressText:=GetLangText(4200);
      StatusBar.AbortText:=GetLangText(3909);

      try
        Sum:=0;
        cntx:=0;
        actual:=0;
        for Cnt3:=0 to Data.Layers^.Count-1 do begin
          ALayer:=PProj(Proj)^.Layers^.IndexToLayer(LongInt(Data.Layers^.At(Cnt3)));
          Sum:=Sum+ALayer^.Data^.GetCount;
        end;

        for Cnt3:=0 to Data.Layers^.Count-1 do
        begin
          ALayer:=PProj(Proj)^.Layers^.IndexToLayer(LongInt(Data.Layers^.At(Cnt3)));
          for Cnt:=0 to ALayer^.Data^.GetCount-1 do
             begin
             if StatusBar.QueryAbort then begin
               AllAbort:=TRUE;
               Break;
             end;
             AIndex:=ALayer^.Data^.At(Cnt);
             AObject:=Pointer(PLayer(PProj(Proj)^.PInfo^.Objects)^.IndexObject(PProj(Proj)^.PInfo,AIndex));
             if (AObject<>NIL)then begin
                {}
                actualn:=((100/Sum)*cntx);
                if (actualn-actual)> 1 then begin
                  if StatusBar.QueryAbort then begin
                    AllAbort:=TRUE;
                    Break;
                  end;
                  StatusBar.Progress:=round(actualn);
                  actual:=actualn;
                end;
                inc(cntx);

//++ Glukhov Bug#199 Build#157 27.04.01
                if (AObject^.GetObjType = ot_Spline)  then DoCopySpline;
                if (AObject^.GetObjType = ot_Circle)  then DoCopyArc;
                if (AObject^.GetObjType = ot_Arc)  then DoCopyArc;
//-- Glukhov Bug#199 Build#157 27.04.01
                if ((AObject^.GetObjType=ot_Poly)
                or (AObject^.GetObjType=ot_CPoly)) then
                    begin
                      if AObject^.GetState(sf_IslandArea) then with AObject^ do begin
                        DoCopyPoly(0,IslandInfo^[0]-1);
                        Start:=IslandInfo^[0];
                        for Island:=1 to IslandCount-1 do begin
                          DoCopyPoly(Start,Start+IslandInfo^[Island]-1);
                          Start:=Start+IslandInfo^[Island]+1;
                        end;
                      end
                      else DoCopyPoly(0,AObject^.Data^.Count-1);                      
                   end;   {Line,Poly}
                end; {AObject<>NIL}
             end;   {1 Layer durchgehen}
        end;  {alle Layer}

        {$IFDEF DEBUG}
        for Cnt3:=0 to Data.Layers^.Count-1 do begin
          ALayer:=PProj(Proj)^.Layers^.IndexToLayer(LongInt(Data.Layers^.At(Cnt3)));
          while ALayer^.Data^.GetCount>0 do begin
             AIndex:=ALayer^.Data^.At(0);
             BIndex.Index:=AIndex^.Index;
             PLayer(PProj(Proj)^.PInfo^.Objects)^.DeleteIndex(PProj(Proj)^.PInfo,@BIndex);
             PProj(Proj)^.Layers^.DeleteIndex(PProj(Proj)^.PInfo,@BIndex);
          end;
        end;
        {$ENDIF}

        {***************<****************************************************}
        {***************** Berechnungs Routine *****************************}
        {*******************************************************************}
         {StatusBar.Progress:=0);}
         StatusBar.ProgressText:=GetLangText(4201);
         if EdgeList.Count>1 then begin
           if Data.Frame then begin
             ProjectSize(ProjSize);
             AddFrame(ProjSize);
           end;
           if not AllAbort then begin
             Msort:=TMergeSort.Create(EdgeList);
             MSort.MergeSort;
             MSort.Free;
           end;
           StatusBar.ProgressText:=GetLangText(4202);
           if not AllAbort then EdgeList.completeList;
           if not AllAbort then EdgeList.Count:=SetListCount(EdgeList);
           if not AllAbort then Snap2;
           if not AllAbort then NodeList.Count:=SetListCount(NodeList);
           StatusBar.ProgressText:=GetLangText(4212);
           if not AllAbort then EleminateEqual;
           {CheckEqual;}
           StatusBar.ProgressText:=GetLangText(4203);
           if not AllAbort then EleminateDouble;
           StatusBar.ProgressText:=GetLangText(4214);
           if not AllAbort then SortLinkList;
           StatusBar.ProgressText:=GetLangText(4204);
           if not AllAbort then SortList(NodeList);
           StatusBar.ProgressText:=GetLangText(4209);
           if not AllAbort then if Data.Intersections then Intersection;
           if not AllAbort then NodeList.Count:=SetListCount(NodeList);
           {StatusBar.ProgressText:=GetLangText(4205,FALSE);}
           StatusBar.ProgressText:=GetLangText(4202);
           if not AllAbort then Snap3;
           FinalList.Count:=SetListCount(FinalList);
           StatusBar.ProgressText:=GetLangText(4212);
           if not AllAbort then EleminateEqual2;
           StatusBar.ProgressText:=GetLangText(4203);
           if not AllAbort then EleminateDouble2;
           StatusBar.ProgressText:=GetLangText(4204);
           if not AllAbort then SortList(FinalList);
           StatusBar.ProgressText:=GetLangText(4211);
           StatusBar.ProgressText:=GetLangText(4215);
           if ((Data.Area = ar_Area)or(Data.Area = ar_Envelope))and(not AllAbort) then begin
             MergeLink;
             EleminateLines;
             EleminateLinesBack;
           end;
           if (Data.Area = ar_Merge)and(not AllAbort) then begin
             MergeLink;
             MergeToPolyLines;
             MergeToPolylInesBack;
           end;
           if Data.Area = ar_Area then StatusBar.ProgressText:=GetLangText(4206);
           if Data.Area = ar_Envelope then StatusBar.ProgressText:=GetLangText(4217);
           if Data.Area = ar_Lines then StatusBar.ProgressText:=GetLangText(4216);
           t2:=now;
           DecodeTime(t2-t1,h,m,s,ms);
           str(h,sh); str(m,sm); str(s,ss); str(ms,sms);
           stime:='';
           stime:=stime+sh+' '+sm+' '+ss+' '+sms;
           if not AllAbort then begin
             if (Data.Area = ar_Area)or(Data.Area = ar_Envelope) then BackTrace else
             if Data.Area = ar_Lines then DrawLines;;
           end;
         end;{Edgelist.count > 0 }
        {*******************************************************************}
      finally
        StatusBar.ProgressPanel:=FALSE;
        PProj(Proj)^.SetCursor(crDefault);
        if (AllAbort) and (DebugMessage <> '') then begin
           if Data.ERLayer then begin
              AIndex:=PProj(Proj)^.SetPixel(LastTreePos.X,LastTreePos.Y,Data.ErrorLayer);
              if AIndex <> nil then begin
                 PProj(Proj)^.Layers^.Select(PProj(Proj)^.PInfo,AIndex);
                 {$IFNDEF WMLT}
                 ProcShowAllSel(Proj);
                 {$ENDIF}
              end;
           end;
           //MessageBox(WingisMainForm.Handle,PChar(AnsiString(DebugMessage)),PChar(AnsiString(ClassName)),MB_APPLMODAL or MB_ICONERROR);
        end;
      end;

{!?!?!      PProj(Proj)^.Layers^.SetTopLayer(PProj(Proj)^.Layers^.GetTopLayer);
      if PProj(Proj)^.Layers^.TranspLayer=0 then PProj(Proj)^.Layers^.DetermineTopLayer;
      ProcSetWidthText(Proj);                                                        }

    end;
  end;

{============================================================================}
{ TAreaDlg                                                                   }
{============================================================================}
{$IFNDEF AXDLL} // <----------------- AXDLL
Constructor TAreaDlg.Init
   (
   AParent         : TComponent;
   AData           : PAreaData;
   ALayers         : PLayers;
   AProjStyles     : TProjectStyles
   );
  begin
    inherited Init(AParent,'D_AREA',ALayers,AProjStyles);
    LData:=AData;
    HelpContext:=5030;
  end;

Procedure TAreaDlg.SetupWindow;
  begin
    inherited SetupWindow;
    FillLayerList(id_SourceList,TRUE,FALSE);
    FillLayerList(id_DestLayer,FALSE,TRUE);
    SetMultiLayerSelection(id_SourceList,LData^.Layers,TRUE);
    SetFieldLong(id_Radius,LData^.Radius);
    CheckRadioButton(Handle,id_Umfahrende,id_Area,id_Umfahrende+LData^.Area);
    if LData^.ERLayer then CheckDlgButton(Handle,id_ErrorLayer,0);
    if LData^.ISLayer then CheckDlgButton(Handle,id_IntersectLayer,0);
    if LData^.Frame then CheckDlgButton(Handle,id_Frame,0);
    if LData^.Intersections then CheckDlgButton(Handle,id_Intersect,1);
  end;

Function TAreaDlg.CanClose
   : Boolean;
  var ARadius      : LongInt;
      ADestLayer   : LongInt;
  begin
    CanClose:=FALSE;
    if not GetFieldLong(id_Radius,ARadius) then MsgBox(Handle,210,mb_IconExclamation or mb_Ok,'')
    else if SendDlgItemMsg(id_SourceList,lb_GetSelCount,0,0)=0 then begin
      MsgBox(Handle,841,mb_IconExclamation or mb_OK,'');
      SetFocus(GetItemHandle(id_SourceList));
    end
    else if GetLayer(id_DestLayer,ADestLayer,959,10125,10128,ilTop) then with LData^ do begin
      if Layers<>NIL then Dispose(Layers,Done);        {ilSecond}
      Layers:=New(PLongColl,Init(5,5));
      GetMultiLayerSelection(id_SourceList,TRUE,Layers);
      Radius:=ARadius;
      DestLayer:=ADestLayer;
      if IsDlgButtonChecked(Handle,id_Umfahrende)=1 then LData^.Area:=ar_Envelope else
      if IsDlgButtonChecked(Handle,id_Lines)     =1 then LData^.Area:=ar_Lines else
      if IsDlgButtonChecked(Handle,id_Area)      =1 then LData^.Area:=ar_Area;
      if IsDlgButtonChecked(Handle,id_Merge)     =1 then LData^.Area:=ar_Merge;
      LData^.ERLayer:=IsDlgButtonChecked(Handle,id_ErrorLayer)=1;
      LData^.ISLayer:=IsDlgButtonChecked(Handle,id_IntersectLayer)=1;
      LData^.Frame:=IsDlgButtonChecked(Handle,id_Frame)=1;
      LData^.Intersections:=IsDlgButtonChecked(Handle,id_Intersect)=1;
      CanClose:=TRUE;
    end;
  end;
{$ENDIF} // <----------------- AXDLL

procedure TAreaObject.DoAreaWithoutDlg3(SourceLayer,DestinationLayer: PLayer; Mode: Integer);
  var Cnt,Cnt5     : LongInt;
      AIndex       : PIndex;
      AObject      : PCPoly;
      MyObject     : PPoly;
      Cnt2         : Integer;
      Cnt3         : Integer;
      APoint       : PDPoint;
      ResultLayer  : PLayer;
      ErgebnisLayer: PLayer;
      check        : boolean;
      sh,sm,ss,sms : string;
      stime        : string;
      mem          : longint;
      edit_feld    : array[0..10]of char;
      ALayer       : PLayer;
      atext        : array[0..40]of char;
      AItem        : TSListItem;
      MSort        : TMergeSort;
      MLayer       : PLayer;
      Sum          : longint;
      cntx         : longint;
      actual       : double;
      actualn      : double;
      h,m,s,ms     : word;
      t1,t2        : double;
      SourLayer    : PLayer;
      Island       : Integer;
      Start        : Integer;
      i            : Integer;
   Procedure DoCopyPoly
      (
      Start        : Integer;
      Stop         : Integer
      );
     var Cnt       : Integer;
     begin
       Alist:=TSLBase.Create;
       for Cnt:=Start to Stop do begin
         APoint:=AObject^.Data^.at(Cnt);
         AItem:=TSListItem.Create;
         AItem.x:=APoint^.x;
         AItem.y:=APoint^.y;
         AList.Add(AItem);
       end;
       ConvertPolyToEdge;
       AList.Free;
     end;
//++ Glukhov Bug#199 Build#157 27.04.01
  procedure DoCopyArc;
    var
      Cnt, Num  : Integer;
      Pnt       : TDPoint;
    begin
      Alist:=TSLBase.Create;
      with PProj(Proj).PInfo.VariousSettings do begin
        if bConvRelMistake
        then  Num:= PEllipse(AObject).ApproximatePointsNumber( dbConvRelMistake, True )
        else  Num:= PEllipse(AObject).ApproximatePointsNumber( dbConvAbsMistake, False );
      end;
      Pnt.Init( 0, 0 );
      for Cnt:=0 to Num-1 do begin
        Pnt:= PEllipse(AObject).ApproximatePoint( Cnt, Num );
        AItem:=TSListItem.Create;
        AItem.x:= Pnt.X;
        AItem.y:= Pnt.Y;
        AList.Add(AItem);
      end;
      ConvertPolyToEdge;
      AList.Free;
    end;
//-- Glukhov Bug#199 Build#157 27.04.01
//++ Glukhov Bug#199 Build#157 21.05.01
  procedure DoCopySpline;
    var
      nSeg, iSeg  : Integer;
      nPnt, iPnt  : Integer;
      Pnt         : TDPoint;
    begin
      Alist:=TSLBase.Create;
      Pnt.Init( 0, 0 );
      nSeg:= PSpline(AObject).SegmentsNum;
      // for each spline segment
      for iSeg:=0 to nSeg-1 do begin
        with PProj(Proj).PInfo.VariousSettings do begin
          if bConvRelMistake
          then  nPnt:= PSpline(AObject).ApprSegmPointsNumber( iSeg, dbConvRelMistake, True )
          else  nPnt:= PSpline(AObject).ApprSegmPointsNumber( iSeg, dbConvAbsMistake, False );
        end;
        // for each approximate point
        for iPnt:=0 to nPnt-1 do begin
          Pnt:= PSpline(AObject).ApprSegmPoint( iSeg, iPnt, nPnt );
          AItem:=TSListItem.Create;
          AItem.x:= Pnt.X;
          AItem.y:= Pnt.Y;
          AList.Add(AItem);
        end;
      end;
      ConvertPolyToEdge;
      AList.Free;
    end;
//-- Glukhov Bug#199 Build#157 21.05.01

  begin
    DebugMessage:='';
    FillChar(Data,SizeOf(Data),#0);
//++ Glukhov Bug#199 Build#157 16.05.01
//    Data.Radius:= 5;
    Data.Radius:= DALastRadius;
//-- Glukhov Bug#199 Build#157 16.05.01
    cnt5:=0;
    Data.Frame:=FALSE;
    Data.Trace:=tr_BackTrace;
    Data.ERLayer:=FALSE;
    Data.ISLayer:=FALSE;
    Data.Intersections:=TRUE;
    Data.Area:=ar_Lines;//ar_Points;

    Data.DestLayer:=DestinationLayer^.Index;
    SourLayer:=SourceLayer;

        if Data.Layers<>NIL then Dispose(Data.Layers,Done);
        Data.Layers:=New(PLongColl,Init(5,5));
        if SourLayer<>NIL then begin
           Data.Layers^.Insert(Pointer(SourLayer^.Index));
        end;

      if Data.ERLayer then begin
        MLayer:=PProj(Proj)^.Layers^.NameToLayer(GetLangText(3914));
        if MLayer=NIL then MLayer:=PProj(Proj)^.Layers^.InsertLayer(GetLangText(3914),
            $000000FF,lt_Solid,$000000FF,pt_Solid,ilTop,DefaultSymbolFill);
        Data.ErrorLayer:=MLayer^.Index;
      end;

      if Data.ISLayer then begin
        MLayer:=PProj(Proj)^.Layers^.NameToLayer(GetLangText(3915));
        if MLayer=NIL then MLayer:=PProj(Proj)^.Layers^.InsertLayer(GetLangText(3915),
            $00FF0000,lt_Solid,$00FF0000,pt_Solid,ilTop,DefaultSymbolFill);
        Data.IntersectLayer:=MLayer^.Index;
      end;

//!?!?!      if PProj(Proj)^.Layers^.TranspLayer=0 then PProj(Proj)^.Layers^.DetermineTopLayer;

      t1:=now;
      if Data.Radius=0 then Data.Radius:=1;
      STree.Radius:=Data.Radius;
      Radius := Data.Radius;
      DeltaX := Data.Radius;
      DeltaY := Data.Radius;
      PProj(Proj)^.SetCursor(crHourGlass);

      StatusBar.ProgressPanel:=TRUE;
      StatusBar.ProgressText:=GetLangText(4200);
      StatusBar.AbortText:=GetLangText(3909);

      try
        Sum:=0;
        cntx:=0;
        actual:=0;
        for Cnt3:=0 to Data.Layers^.Count-1 do begin
          ALayer:=PProj(Proj)^.Layers^.IndexToLayer(LongInt(Data.Layers^.At(Cnt3)));
          Sum:=Sum+ALayer^.Data^.GetCount;
        end;

        for Cnt3:=0 to Data.Layers^.Count-1 do
        begin
          ALayer:=PProj(Proj)^.Layers^.IndexToLayer(LongInt(Data.Layers^.At(Cnt3)));
          for Cnt:=0 to ALayer^.Data^.GetCount-1 do
             begin
             if StatusBar.QueryAbort then begin
               AllAbort:=TRUE;
               Break;
             end;
             AIndex:=ALayer^.Data^.At(Cnt);
             AObject:=Pointer(PLayer(PProj(Proj)^.PInfo^.Objects)^.IndexObject(PProj(Proj)^.PInfo,AIndex));
             if (AObject<>NIL)then begin
                actualn:=((100/Sum)*cntx);
                if (actualn-actual)> 1 then begin
                  if StatusBar.QueryAbort then begin
                    AllAbort:=TRUE;
                    Break;
                  end;
                  StatusBar.Progress:=round(actualn);
                  actual:=actualn;
                end;
                inc(cntx);

//++ Glukhov Bug#199 Build#157 27.04.01
                if (AObject^.GetObjType = ot_Spline)  then DoCopySpline;
                if (AObject^.GetObjType = ot_Circle)  then DoCopyArc;
                if (AObject^.GetObjType = ot_Arc)  then DoCopyArc;
//-- Glukhov Bug#199 Build#157 27.04.01
                if ((AObject^.GetObjType=ot_Poly)
                or (AObject^.GetObjType=ot_CPoly)) then
                     begin
                       if AObject^.GetState(sf_IslandArea) then with AObject^ do begin
                         DoCopyPoly(0,IslandInfo^[0]-1);
                         Start:=IslandInfo^[0];
                         for Island:=1 to IslandCount-1 do begin
                           DoCopyPoly(Start,Start+IslandInfo^[Island]-1);
                           Start:=Start+IslandInfo^[Island]+1;
                         end;
                       end
                       else DoCopyPoly(0,AObject^.Data^.Count-1);
                     end;   {Line,Poly}
                end; {AObject<>NIL}
             end;   {1 Layer durchgehen}
        end;  {alle Layer}

        {*******************************************************************}
        {***************** Berechnungs Routine *****************************}
        {*******************************************************************}
         StatusBar.ProgressText:=GetLangText(4201);
         if Data.Frame then begin
           ProjectSize(ProjSize);
           AddFrame(ProjSize);
         end;
         if not AllAbort then begin
           Msort:=TMergeSort.Create(EdgeList);
           MSort.MergeSort;
           MSort.Free;
         end;
         StatusBar.ProgressText:=GetLangText(4202);
         if not AllAbort then EdgeList.completeList;
         if not AllAbort then EdgeList.Count:=SetListCount(EdgeList);
         if not AllAbort then Snap2;
         if not AllAbort then NodeList.Count:=SetListCount(NodeList);

         StatusBar.ProgressText:=GetLangText(4212);
         if not AllAbort then EleminateEqual;
         StatusBar.ProgressText:=GetLangText(4203);
         if not AllAbort then EleminateDouble;

         StatusBar.ProgressText:=GetLangText(4214);
         if not AllAbort then SortLinkList;
         StatusBar.ProgressText:=GetLangText(4204);
         if not AllAbort then SortList(NodeList);

         StatusBar.ProgressText:=GetLangText(4209);
         if not AllAbort then if Data.Intersections then Intersection2;
         if not AllAbort then NodeList.Count:=SetListCount(NodeList);

         StatusBar.ProgressText:=GetLangText(4202);
         if not AllAbort then Snap3;
         FinalList.Count:=SetListCount(FinalList);

         StatusBar.ProgressText:=GetLangText(4212);
         if not AllAbort then EleminateEqual2;
         StatusBar.ProgressText:=GetLangText(4203);
         if not AllAbort then EleminateDouble2;

         StatusBar.ProgressText:=GetLangText(4204);
         if not AllAbort then SortList(FinalList);

         StatusBar.ProgressText:=GetLangText(4211);
         if not AllAbort then MergeLink;
         StatusBar.ProgressText:=GetLangText(4215);
         if ((Data.Area = ar_Area)or(Data.Area = ar_Envelope))and(not AllAbort) then begin
           EleminateLines;
           EleminateLinesBack;
         end;
         {StatusBar.ProgressText:=GetLangText(4206,FALSE);}

         t2:=now;
         DecodeTime(t2-t1,h,m,s,ms);
         str(h,sh); str(m,sm); str(s,ss); str(ms,sms);
         stime:='';
         stime:=stime+sh+' '+sm+' '+ss+' '+sms;

        {*******************************************************************}
      finally
        StatusBar.ProgressPanel:=FALSE;
      PProj(Proj)^.SetCursor(crDefault);
      end;
  end;

end.

