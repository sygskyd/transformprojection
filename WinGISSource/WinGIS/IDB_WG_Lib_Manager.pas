unit IDB_WG_Lib_Manager;
{
Internal database. Ver. II.
This module provides a management on IDB dll.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 31-01-2001
}
interface

uses Windows, Forms, Messages, Classes, Objects,
  IDB_CallbacksDef, Lists;

type
  TCheck = function: Boolean;
   { Types to connect to the library's functions and procedures. }
   //********     Initialization and finalization routnes of the DLL.  ************
  TInitLibrary = function(AnApplication: Integer; AScreen: Integer; AMainForm: Integer; ALanguageFileExtension: PChar): Boolean; stdcall;
  TFinLibrary = procedure; stdcall;

  TGetVersion = function: PChar; stdcall;
  TSetReadOnlyState = procedure(bReadOnlyState: Boolean); stdcall;
   //****************             GeoText routines.           *********************
  TGetGeoText = function(AProject: Pointer; iLayerIndex, iItemID: Integer {; pcGeoTextIDFieldName, pcGeoTextValueFieldName: PChar}): PChar; stdcall;
  TSetGeoText = procedure(AProject: Pointer; iLayerIndex, iItemID: Integer; { pcGeoTextIDFieldName, pcGeoTextValueFieldName,} pcGeoTextValue: PChar); stdcall;
  TGetTableFieldsNamesList = function(AProject: Pointer; ALayer: Pointer): PChar; stdcall;
  TGetTableVisibleFieldsNamesList = function(AProject: Pointer; ALayer: Pointer): PChar; stdcall;
   //****************            IDB core routines.           *********************
  TAddProject = function(AProjectFormHandle: Integer; AProject: Pointer; bProjectFileHasReadOnlyState: Boolean): Boolean; stdcall;
  TDeleteProject = procedure(AProject: Pointer); stdcall;
   {brovak}
  TShowAttributeWindow = procedure(AProject, ALayer, Aalayer: Pointer); stdcall;
   {brovak}
  TRefreshAllDataWindows = procedure(AProject: Pointer); stdcall;
  TCloseAttributeWindow = procedure(AProject, ALayer: Pointer); stdcall;
   {brovak}
  TIsOpenedWindow = function(AProject: Pointer): integer; stdcall;
   {brovak}
  TCloseAllWindowsOfProject = procedure(AProject: Pointer); stdcall;
  TCloseAllWindowsOfAllProjects = procedure; stdcall;
  TShowMonitoringWindow = procedure(AProject: Pointer; bAlwaysShowMonitoring: Boolean); stdcall;
  TShowInfoWindow = procedure(AProject: Pointer); stdcall;
  TClearInfoWindow = procedure(AProject: Pointer); stdcall;
  TCloseInfoWindow = procedure; stdcall;
  THideInfoWindow = procedure; stdcall;
  TProjectIsBeingSaved = procedure(AProject: Pointer); stdcall;
  TSaveDatabaseAs = procedure(AProject: Pointer; pcNewProjectName: PChar); stdcall;
  TDeleteProjectAndDatabaseFiles = procedure(pcProjectFileName: PChar); stdcall;
  TInsertItem = procedure(AProject, ALayer: Pointer; iNewItemIndex: Integer); stdcall;
// ++ Cadmensky
  TOverlayInsertItem = procedure(AProject, ALayer: Pointer; iNewItemIndex, iAItemIndex, iBItemIndex: Integer); stdcall;
// -- Cadmensky

// ++ Cadmensky Version 2.2.8
  TMoveAttributeInfo = function(AProject, ADestLayer: Pointer): Integer; stdcall;
// -- Cadmensky Version 2.2.8
  TCopyAttributeInfo = function(AProject, ASourceLayer, ADestLayer: Pointer; iSourceItemID, iDestItemID: Integer): Integer; stdcall;
  TGetReadyToInsertRecords = function(AProject, ALayer: Pointer; ARecordTemplate: TList): Integer; stdcall;
   //   TInsertRecord = Function(iPosition, iLayerItemID: Integer): Boolean; stdcall;
  TInsertRecord = function(AProject, ALayer: Pointer; iLayerItemID: Integer): Boolean; stdcall;
  TEndInserting = procedure(AProject, ALayer: Pointer); stdcall;
   //   TEndInserting = Procedure(iPosition: Integer); stdcall;
  TGetReadyToPolygonOverlaying = function(AProject, ASourceLayer1, ASourceLayer2, ADestinationLayer: Pointer): Boolean; stdcall;
  TEndOfPolygonOverlaying = procedure(AProject: Pointer); stdcall;

  TSet_DoNotAddAttributeRecords_Sign = procedure(AProject: Pointer; bValue: Boolean); stdcall;
  TDeleteLayerTables = procedure(AProject, ALayer: Pointer); stdcall;
  TLoadGeoTextData = function(AProject, ALayer: Pointer; pcSourceDBFileName, pcSourceFieldName, pcDestinationFieldName, pcProgisIDFieldName: PChar): Boolean; stdcall;
  TGetActiveWindowProject = function: Pointer; stdcall;

  TX_GotoObjectRecord = function(AProject, ALayer: Pointer; iItemIndex: Integer): Boolean; stdcall;
  TX_AddRecord = function(AProject, ALayer: Pointer; iItemIndex: Integer): Boolean; stdcall;
  TX_GetFieldCount = function(AProject, ALayer: Pointer): Integer; stdcall;
  TX_GetFieldName = function(AProject, ALayer: Pointer; iFieldIndex: Integer): PChar; stdcall;
  TX_GetFieldValueByFieldName = function(AProject, ALayer: Pointer; pcFieldName: PChar): Variant; stdcall;
  TX_GetFieldValueByFieldIndex = function(AProject, ALayer: Pointer; iFieldIndex: Integer): Variant; stdcall;
  TX_SetFieldValueByFieldName = procedure(AProject, ALayer: Pointer; pcFieldName: PChar; FieldValue: Variant); stdcall;
  TX_SetFieldValueByFieldIndex = procedure(AProject, ALayer: Pointer; iFieldIndex: Integer; FieldValue: Variant); stdcall;

   //****************   WinGIS <-> IDB interaction routines    ********************
   { Callback functions }
  TSetAnnotationSettingsDialogExecuteCallback = procedure(AFunction: TAnnotationSettingsDialogExecute); stdcall;
  TSetMakeAnnotationInProjectCallback = procedure(AFunction: TMakeAnnotationInProject); stdcall;
  TUpdateAnnotationData = procedure(AProject: Pointer; iLayerIndex, iItemID: Integer; AnAnnotData: PCollection); stdcall;
  TSetMakeAnnotationUpdateCallback = procedure(AProcedure: TMakeAnnotationUpdate); stdcall;
  TMakeAnnotations = procedure(AProject: Pointer); stdcall;
  TSetLayerForInsertCallback = function(AFunction: TLayerForInsert): Pointer; stdcall;
   {brovak}

  TMakeSpecialChart = procedure(AProject, Alayer: Pointer); stdcall;
  TSendSpecialforChart = procedure(AProject: Pointer; PseudoDDEString: Ansistring); stdcall;
  TSetMakeSpecialChartCallBack = procedure(AFunction: TMakeSpecialChart); stdcall;
  TSetSendSpecialforChartCallback = procedure(AFunction: TSendSpecialforChart); stdcall;

// ++ Commented by Cadmensky IDB Version 2.2.9
//   TShowNewThematicMapDialog = procedure (AProject: Pointer; sConnection, sSQLText, sProgisIDFieldName: AnsiString); stdcall;
// -- Commented by Cadmensky IDB Version 2.2.9
  TSetShowNewThematicMapDialogCallBack = procedure(AFunction: TShowNewThematicMapDialog); stdcall;

  TShowPercentAtWinGIS = procedure(Flag, IDMessage, IDCurrent: integer); stdcall;
  TSetShowPercentAtWinGISCallback = procedure(AFunction: TShowPercentAtWinGIS); stdcall;

  TSendToTMSelectedID = procedure(AProject: Pointer; AProgisIDList: TIntList); stdcall;
  TSetSendToTMSelectedIDCallback = procedure(Afunction: TSendToTMSelectedID); stdcall;

  TSetChartSettingsDialogExecuteCallback = procedure(AFunction: TChartSettingsDialogExecute); stdcall;
  TSetMakeChartInProjectCallback = procedure(AFunction: TMakeChartInProject); stdcall;
  TUpdateChartData = procedure(AProject: Pointer; iLayerIndex, iItemID: Integer; AnChartData: PCollection); stdcall;
  TSetMakeChartUpdateCallback = procedure(AProcedure: TMakeChartUpdate); stdcall;
  TMakeChart = procedure(AProject: Pointer); stdcall;

   {brovak}

  TSetShowProjectCallback = procedure(AProcedure: TShowProject); stdcall;
  TSetShowSelectedItemsInProjectCallback = procedure(AProcedure: TShowSelectedItemsInProject); stdcall;
  TSetShowItemInProjectWithZoomCallback = procedure(AProcedure: TShowItemInProjectWithZoom); stdcall;
  TSetDeselectAllObjectsCallback = procedure(AProcedure: TDeselectAllObjects); stdcall;
// ++ Commented by Cadmensky IDB Version 2.2.9
{   TGetProjectUndoStepsCountCallback = procedure (AProcedure: TGetProjectUndoStepsCount); stdcall;
   TGetProjectFileSizeCallback = procedure (AProcedure: TGetProjectFileSize); stdcall;}
// -- Commented by Cadmensky IDB Version 2.2.9
  TSetCheckItemExistingCallback = procedure(AFunction: TItemExistsInProject); stdcall;
  TSetItemToGuidCallback = procedure(AFunction: TItemToGuid); stdcall;
  TSetProjectModifiedCallback = procedure(AProcedure: TSetProjectModified); stdcall;
  TSetStartProjectGUIDCallback = procedure(AProcedure: TSetStartProjectGUID); stdcall;
  TSetCurrentProjectGUIDCallback = procedure(AProcedure: TSetCurrentProjectGUID); stdcall;
  TGetStartProjectGUIDCallback = procedure(AFunction: TGetStartProjectGUID); stdcall;
  TGetCurrentProjectGUIDCallback = procedure(AFunction: TGetCurrentProjectGUID); stdcall;
  TGetProjectIsNewAndNotSavedCallback = procedure(AFunction: TGetProjectIsNewAndNotSaved); stdcall;
  TGetProjectFullFileNameCallback = procedure(AFunction: TGetProjectFullFileName); stdcall;
  TGetLayerNameCallback = procedure(AFunction: TGetLayerName); stdcall;
  TGetLayerIndexCallback = procedure(AFunction: TGetLayerIndex); stdcaLL;
  TGetAllProjectWindowsNumberCallback = procedure(AFunction: TGetAllProjectWindowsNumber); stdcall;
  TGetProjectLayerNumberCallback = procedure(AFunction: TGetProjectLayerNumber); stdcall;
  TGetLayerPointerByPositionCallback = procedure(AFunction: TGetLayerPointerByPosition); stdcall;
  TGetLayerPointerByIndexCallback = procedure(AFunction: TGetLayerPointerByIndex); stdcall;
// ++ Cadmensky
  TGetActiveLayerCallback = procedure(AFunction: TGetActiveLayer); stdcall;
// -- Cadmensky
  TLayerIsFixedCallback = function(AFunction: TLayerIsFixed): Boolean; stdcall;
  TSetGetWinGISINIFileNameCallback = procedure(AFunction: TGetWinGISINIFileName); stdcall;
  TSetDefaultProjectFileNameCallback = procedure(AFunction: TGetDefaultProjectFileName); stdcall;
{   TGeneratePointCallback = procedure (AFunction: TGeneratePoint); stdcall;
   TGenerateSymbolCallback = procedure (AFunction: TGenerateSymbol); stdcall;}
  TShowDonePercentCallback = procedure(AFunction: TShowDonePercent); stdcall;
// ++ Commented by Cadmensky Version 2.2.8
//   TSetGatherGarbageInProjectCallback = procedure (AFunction: TGatherGarbageInProject); stdcall;
// -- Commented by Cadmensky Version 2.2.8
  TSetRedrawProjectWindowCallback = procedure(AProcedure: TRedrawProjectWindow); stdcall;
  TSetChangeGeoTextParametersCallback = procedure(AProcedure: TChangeGeoTextParameters); stdcall;
// ++ Cadmensky Version 2.2.8
  TSetGetLayerItemsCountCallback = procedure(AFunction: TGetLayerItemsCount); stdcall;
  TSetGetLayerItemIndexByNumberCallback = procedure(AFunction: TGetLayerItemIndexByNumber); stdcall;
  TSetIsObjectSelectedCallback = procedure(AFunction: TIsObjectSelected); stdcall;
// -- Cadmensky Version 2.2.8
  TSetRunThematicMapBuilderCallback = procedure(AProcedure: TRunThematicMapBuilder); stdcall;
  TSetRunMonitoringCallback = procedure(AProcedure: TRunMonitoring); stdcall;
  TSetGetObjectPropsCallback = procedure(AFunction: TGetObjectProps); stdcall;
  TSetReverseUsingIDBInfoCallback = procedure(AProcedure: TReverseUsingOfIDBInfo); stdcall;
  TSetGetMDIChildWindowStateCallback = procedure(AFunction: TGetMDIChildWindowState); stdcall;
// ++ Cadmensky
//   TSetFillLayerNamesListCallback = procedure (AFunction: TFillLayerNamesList); stdcall;
  TSetNessesitySaveUndoDataSignCallback = procedure(AProcedure: TSetNessesitySaveUndoDataSign); stdcall;
  TSetSaveUndoDataCallback = procedure(AProcedure: TSaveUndoData); stdcall;
// -- Cadmensky
   {  }
  TGetItemIDToBeDeleted_FromIDB = function(AProject: Pointer; ALayer: Pointer): Integer; stdcall;
  TGetReadyToReceiveIDsOfObjectToBeDeleted_FromWG = function(AProject: Pointer): Boolean; stdcall;
  TReceiveIDOfObjectToBeDeleted_FromWG = procedure(AProject, ALayer: Pointer; iObjectID: Integer); stdcall;
  TDeleteObjects_FromWG = procedure(AProject: Pointer); stdcall;

  TGetReadyToReceiveIDsOfObjectForMonitoring = function(AProject: Pointer): Boolean; stdcall;
  TReceiveIDOfObjectForMonitoring = procedure(AProject, ALayer: Pointer; iObjectID: Integer); stdcall;

  TGetReadyToReceiveIDsOfObjectsToBeMade = function(AProject: Pointer): Boolean; stdcall;
  TReceiveIDOfObjectToBeMade = procedure(AProject, ALayer: Pointer; iItemID: Integer); stdcall;

  TSetGeoTextFieldNames = procedure(AProject, ALayer: Pointer; pcGeoTextIDFieldName, pcGeoTextFieldName: PChar); stdcall;
  TGetGeoTextIDFieldName = function(AProject, ALayer: Pointer): PChar; stdcall;
  TGetGeoTextFieldName = function(AProject, ALayer: Pointer): PChar; stdcall;

  TSet_AnExternalDBIsWorking_Sign = procedure(AProject: Pointer; bValue: Boolean); stdcall;

  TGetReadyToReceiveIDsOfObjectsToBeRestored = function(AProject: Pointer): Boolean; stdcall;
  TReceiveIDOfObjectToBeRestored = procedure(AProject: Pointer; ALayer: Pointer; iObjectID: Integer); stdcall;
  TExecuteRestoring = procedure(AProject: Pointer); stdcall;

   // ++ Cadmensky
  TUpdateCalcFields = procedure(AProject: Pointer; ALayer: Pointer; iObjectID: Integer); stdcall;
  TShowRecordsForSelectedObjects = procedure(AProject: Pointer); stdcall;
   // -- Cadmensky

  TGetReadyToReceiveIDsOfObjectsToBeUsedForDragAndDrop = function(AProject: Pointer): Boolean; stdcall;
  TReceiveIDOfObjectToBeUsedForDragAndDrop = procedure(AProject, ALayer: Pointer; iObjectID: Integer); stdcall;
  TExecuteDragAndDrop = procedure(AProject: Pointer; ASourceGridOfDragging: Pointer); stdcall;

  TTheUserWantsToCancel = function(AWindow: TForm): Boolean; stdcall;

  TF_GetLinesPerLayer = function(AProject: Pointer; Layername: PChar): PChar; stdcall;
  TF_GetSymbolsPerLayer = function(AProject: Pointer; Layername: PChar): PChar; stdcall;
  TF_SymbolAllowed = function(AProject: Pointer; SymbolName, LineStyleName: PChar): PChar; stdcall;

   { Class that implements the access to IDB. }
  TIDBLib = class
  private
      { IDB library handle }
    hIDBLibrary: THandle;
    bTheLibraryWasInitialized: Boolean;
      { Initialization and finalization routnes of the DLL }
    InitLibraryFunc: TInitLibrary;
    FinLibraryProc: TFinLibrary;
    GetVersionFunc: TGetVersion;
    SetReadOnlyStateProc: TSetReadOnlyState;
      { GeoText routines }
    GetGeoTextFunc: TGetGeoText;
    SetGeoTextProc: TSetGeoText;
    GetTableFieldsNamesListFunc: TGetTableFieldsNamesList;
    GetTableVisibleFieldsNamesListFunc: TGetTableVisibleFieldsNamesList;
      { IDB core routines }
    AddProjectFunc: TAddProject;
    DeleteProjectProc: TDeleteProject;
    ShowAttributeWindowProc: TShowAttributeWindow;
    RefreshAllDataWindowsProc: TRefreshAllDataWindows;
    CloseAttributeWindowProc: TCloseAttributeWindow;
    CloseAllWindowsOfProjectProc: TCloseAllWindowsOfProject;
      {brovak}
    IsOpenedWindowProc: TIsOpenedWindow;
      {brovak}
    CloseAllWindowsOfAllProjectsProc: TCloseAllWindowsOfAllProjects;
    ShowMonitoringWindowProc: TShowMonitoringWindow;
    ShowInfoWindowProc: TShowInfoWindow;
    ClearInfoWindowProc: TClearInfoWindow;
    CloseInfoWindowProc: TCloseInfoWindow;
    HideInfoWindowProc: THideInfoWindow;
    ProjectIsBeingSavedProc: TProjectIsBeingSaved;
    SaveDatabaseAsProc: TSaveDatabaseAs;
    DeleteProjectAndDatabaseFilesProc: TDeleteProjectAndDatabaseFiles;
    InsertItemProc: TInsertItem;
// ++ Cadmensky
    OverlayInsertItemProc: TOverlayInsertItem;
// -- Cadmensky

// ++ Cadmensky Version 2.2.8
    MoveAttributeInfoProc: TMoveAttributeInfo;
// -- Cadmensky Version 2.2.8
    CopyAttributeInfoProc: TCopyAttributeInfo;
// ++ Commented by Cadmensky IDB Version 2.2.8
{      GetReadyToInsertRecordsFunc: TGetReadyToInsertRecords;
      InsertRecordFunc: TInsertRecord;
      EndInsertingProc: TEndInserting;}
// ++ Commented by Cadmensky IDB Version 2.2.8
    GetReadyToPolygonOverlayingFunc: TGetReadyToPolygonOverlaying;
    EndOfPolygonOverlayingProc: TEndOfPolygonOverlaying;

    Set_DoNotAddAttributeRecords_SignProc: TSet_DoNotAddAttributeRecords_Sign;
    DeleteLayerTablesProc: TDeleteLayerTables;

    LoadGeoTextDataFunc: TLoadGeoTextData;

    GetActiveWindowProjectFunc: TGetActiveWindowProject;

    X_GotoObjectRecordFunc: TX_GotoObjectRecord;
    X_AddRecordFunc: TX_AddRecord;
    X_GetFieldCountFunc: TX_GetFieldCount;
    X_GetFieldNameFunc: TX_GetFieldName;
    X_GetFieldValueByFieldNameFunc: TX_GetFieldValueByFieldName;
    X_GetFieldValueByFieldIndexFunc: TX_GetFieldValueByFieldIndex;
    X_SetFieldValueByFieldNameProc: TX_SetFieldValueByFieldName;
    X_SetFieldValueByFieldIndexProc: TX_SetFieldValueByFieldIndex;

    SetMakeAnnotationInProjectCallbackProc: TSetMakeAnnotationInProjectCallback;
    SetAnnotationSettingsDialogExecuteCallbackProc: TSetAnnotationSettingsDialogExecuteCallback;
    UpdateAnnotationDataProc: TUpdateAnnotationData;
    SetMakeAnnotationUpdateCallbackProc: TSetMakeAnnotationUpdateCallback;
    MakeAnnotationsProc: TMakeAnnotations;
    SetLayerForInsertCallbackProc: TSetLayerForInsertCallback;

      {brovak}
    SetMakeSpecialChartCallBackProc: TSetMakeSpecialChartCallback;
    SetSendSpecialforChartCallbackProc: TSetSendSpecialforChartCallback;
    SetShowNewThematicMapDialogCallBackProc: TSetShowNewThematicMapDialogCallBack;
    SetSendToTMSelectedIDCallbackProc: TSetSendToTMSelectedIDCallback;

    SetShowPercentAtWinGISCallBackProc: TSetShowPercentAtWinGISCallBack;

    SetMakeChartInProjectCallbackProc: TSetMakeChartInProjectCallback;
    SetChartSettingsDialogExecuteCallbackProc: TSetChartSettingsDialogExecuteCallback;
    UpdateChartDataProc: TUpdateChartData;
    SetMakeChartUpdateCallbackProc: TSetMakeChartUpdateCallback;
    MakeChartProc: TMakeChart;
      {brovak}

        { Callback functions }
    SetShowProjectCallbackProc: TSetShowProjectCallback;
    SetShowSelectedItemsInProjectCallbackProc: TSetShowSelectedItemsInProjectCallback;
    SetShowItemInProjectWithZoomCallbackProc: TSetShowItemInProjectWithZoomCallback;
    SetDeselectAllObjectsCallbackProc: TSetDeselectAllObjectsCallback;
// ++ Commented by Cadmensky IDB Version 2.2.9
{      GetProjectUndoStepsCountCallbackProc: TGetProjectUndoStepsCountCallback;
      GetProjectFileSizeCallbackProc: TGetProjectFileSizeCallback;}
// -- Commented by Cadmensky IDB Version 2.2.9
    SetCheckItemExistingCallbackProc: TSetCheckItemExistingCallback;
    SetItemToGuidCallbackProc: TSetItemToGuidCallback;
    SetSetProjectModifiedCallbackProc: TSetProjectModifiedCallback;
    SetSetStartProjectGUIDCallbackProc: TSetStartProjectGUIDCallback;
    SetSetCurrentProjectGUIDCallbackProc: TSetCurrentProjectGUIDCallback;
    SetGetStartProjectGUIDCallbackProc: TGetStartProjectGUIDCallback;
    SetGetCurrentProjectGUIDCallbackProc: TGetCurrentProjectGUIDCallback;
    SetGetProjectIsNewAndNotSavedCallbackProc: TGetProjectIsNewAndNotSavedCallback;
    SetGetProjectFullFileNameCallbackProc: TGetProjectFullFileNameCallback;
    SetGetLayerNameCallbackProc: TGetLayerNameCallback;
    SetGetLayerIndexCallbackProc: TGetLayerIndexCallback;
    SetGetAllProjectWindowsNumberCallbackProc: TGetAllProjectWindowsNumberCallback;
    SetGetProjectLayerNumberCallbackProc: TGetProjectLayerNumberCallback;
    SetGetLayerPointerByPositionCallbackProc: TGetLayerPointerByPositionCallback;
    SetGetLayerPointerByIndexCallbackProc: TGetLayerPointerByIndexCallback;
// ++ Cadmensky
    SetGetActiveLayerCallbackProc: TGetActiveLayerCallback;
// -- Cadmensky
    SetLayerIsFixedCallbackProc: TLayerIsFixedCallback;
    SetGetWinGISINIFileNameCallbackProc: TSetGetWinGISINIFileNameCallback;
    SetGetDefaultProjectFileNameCallbackProc: TSetDefaultProjectFileNameCallback;
{      SetGeneratePointCallbackProc: TGeneratePointCallback;
      SetGenerateSymbolCallbackProc: TGenerateSymbolCallback;}
    SetShowDonePercentCallbackProc: TShowDonePercentCallback;
// ++ Commented by Cadmensky Version 2.2.8
//      SetGatherGarbageInProjectCallbackProc: TSetGatherGarbageInProjectCallback;
//      SetGatherSelectedGarbageInProjectCallbackProc: TSetGatherGarbageInProjectCallback;
// -- Commented by Cadmensky Version 2.2.8
    SetRedrawProjectWindowCallbackProc: TSetRedrawProjectWindowCallback;
    SetChangeGeoTextParametersCallbackProc: TSetChangeGeoTextParametersCallback;
// ++ Cadmensky Version 2.2.8
    SetGetLayerItemsCountCallbackProc: TSetGetLayerItemsCountCallback;
    SetGetLayerItemIndexByNumberCallbackProc: TSetGetLayerItemIndexByNumberCallback;
    SetIsObjectSelectedCallbackProc: TSetIsObjectSelectedCallback;
// -- Cadmensky Version 2.2.8
    SetRunThematicMapBuilderCallbackProc: TSetRunThematicMapBuilderCallback;
    SetRunMonitoringCallbackProc: TSetRunMonitoringCallback;
    SetGetObjectPropsCallbackProc: TSetGetObjectPropsCallback;
    SetReverseUsingIDBInfoCallbackProc: TSetReverseUsingIDBInfoCallback;
    SetGetMDIChildWindowStateCallbackProc: TSetGetMDIChildWindowStateCallback;
// ++ Cadmensky
//      SetFillLayerNamesListCallbackProc: TSetFillLayerNamesListCallback;
    SetSetNessesitySaveUndoDataSignCallbackProc: TSetNessesitySaveUndoDataSignCallback;
    SetSaveUndoDataCallbackProc: TSetSaveUndoDataCallback;
// -- Cadmensky
      {   }
    GetItemIDToBeDeleted_FromIDBFunc: TGetItemIDToBeDeleted_FromIDB;
    GetReadyToReceiveIDsOfObjectToBeDeleted_FromWGFunc: TGetReadyToReceiveIDsOfObjectToBeDeleted_FromWG;
    ReceiveIDOfObjectToBeDeleted_FromWGProc: TReceiveIDOfObjectToBeDeleted_FromWG;
    DeleteObjects_FromWGProc: TDeleteObjects_FromWG;

    GetReadyToReceiveIDsOfObjectsForMonitoringFunc: TGetReadyToReceiveIDsOfObjectForMonitoring;
    ReceiveIDOfObjectForMonitoringProc: TReceiveIDOfObjectForMonitoring;

    GetReadyToReceiveIDsOfObjectsToBeMadeFunc: TGetReadyToReceiveIDsOfObjectsToBeMade;
    ReceiveIDOfObjectToBeMadeProc: TReceiveIDOfObjectToBeMade;

    SetGeoTextFieldNamesProc: TSetGeoTextFieldNames;
    GetGeoTextIDFieldNameFunc: TGetGeoTextIDFieldName;
    GetGeoTextFieldNameFunc: TGetGeoTextFieldName;

    Set_AnExternalDBIsWorking_SignProc: TSet_AnExternalDBIsWorking_Sign;

    GetReadyToReceiveIDsOfObjectsToBeRestoredFunc: TGetReadyToReceiveIDsOfObjectsToBeRestored;
    ReceiveIDOfObjectToBeRestoredProc: TReceiveIDOfObjectToBeRestored;
    ExecuteRestoringProc: TExecuteRestoring;

// ++ Cadmensky
    UpdateCalcFieldsProc: TUpdateCalcFields;
    ShowRecordsForSelectedObjectsProc: TShowRecordsForSelectedObjects;
// -- Cadmensky

// ++ Commented by Cadmensky IDB Version 2.3.0
{      GetReadyToReceiveIDsOfObjectsToBeUsedForDragAndDropFunc: TGetReadyToReceiveIDsOfObjectsToBeUsedForDragAndDrop;
      ReceiveIDOfObjectToBeUsedForDragAndDropProc: TReceiveIDOfObjectToBeUsedForDragAndDrop;
      ExecuteDragAndDropProc: TExecuteDragAndDrop;}
// -- Commented by Cadmensky IDB Version 2.3.0

    TheUserWantsToCancelFunc: TTheUserWantsToCancel;

  public
    FMAllowed: Boolean;

    F_GetLinesPerLayer: TF_GetLinesPerLayer;
    F_GetSymbolsPerLayer: TF_GetSymbolsPerLayer;
    F_SymbolAllowed: TF_SymbolAllowed;

    constructor Create(AMainForm: TForm; sIDBLibFileName, sLanguageFileExtension: AnsiString);
    destructor Free;

    function LibraryWasLoaded: Boolean;
    function GetLibraryVersion: AnsiString;
    procedure SetReadOnlyState(bReadOnlyState: Boolean);
      //****************             GeoText routines.           *********************
    function GetGeoText(AProject: Pointer; iLayerIndex, iItemID: Integer {; sGeoTextIDFieldName, sGeoTextValueFieldName: AnsiString}): AnsiString;
    procedure SetGeoText(AProject: Pointer; iLayerIndex, iItemID: Integer; { sGeoTextIDFieldName, sGeoTextValueFieldName,} sGeoTextValue: AnsiString);
    function GetTableFieldsNamesList(AProject: Pointer; ALayer: Pointer): AnsiString;
    function GetTableVisibleFieldsNamesList(AProject: Pointer; ALayer: Pointer): AnsiString;
      //****************            IDB core routines.           *********************
    function AddProject(AProjectFormHandle: Integer; AProject: Pointer; bProjectFileHasReadOnlyState: Boolean): Boolean;
    procedure DeleteProject(AProject: Pointer);
    procedure ShowDataTable(AProject, ALayer, AALayer: Pointer);
      {brovak}
    function IsOpenedWindow(AProject: Pointer): integer;
      {brovak}
    procedure RefreshAllDataWindows(AProject: Pointer);
    procedure CloseAttributeWindow(AProject, ALayer: Pointer);
    procedure CloseAllWindowsOfAProject(AProject: Pointer);
    procedure CloseAllWindowsOfAllProjects;
    procedure ShowMonitoringWindow(AProject: Pointer; bAlwaysShowMonitoring: Boolean);
    procedure ShowInfoWindow(AProject: Pointer);
    procedure ClearInfoWindow(AProject: Pointer);
    procedure CloseInfoWindow;
    procedure HideInfoWindow;

    procedure ProjectIsBeingSaved(AProject: Pointer);
    procedure SaveDatabaseAs(AProject: Pointer; sNewProjectName: AnsiString);
    procedure DeleteProjectAndDatabaseFiles(sProjectFileName: AnsiString);

    procedure InsertItem(AProject, ALayer: Pointer; iNewItemIndex: Integer);
    procedure OverlayInsertItem(AProject, ALayer: Pointer; iNewItemIndex, iAItemIndex, iBItemIndex: Integer);
// ++ Cadmensky Version 2.2.8
    function MoveAttributeInfo(AProject, ADestLayer: Pointer): integer;
// -- Cadmensky Version 2.2.8
    function CopyAttributeInfo(AProject, ASourceLayer, ADestLayer: Pointer; iSourceItemID, iDestItemID: Integer): integer;
// ++ Commented by Cadmensky IDB Version 2.2.8
{      function GetReadyToInsertRecords (AProject, ALayer: Pointer; ARecordTemplate: TList): Integer;
      //      Function InsertRecord(iPosition, iLayerItemID: Integer): Boolean;
      function InsertRecord (AProject, ALayer: Pointer; iLayerItemID: Integer): Boolean;
      //      Procedure EndInserting(iPosition: Integer);
      procedure EndInserting (AProject, ALayer: Pointer);}
// -- Commented by Cadmensky IDB Version 2.2.8

    function GetReadyToPolygonOverlaying(AProject, ASourceLayer1, ASourceLayer2, ADestinationLayer: Pointer): Boolean;
    procedure EndOfPolygonOverlaying(AProject: Pointer);

    procedure Set_DoNotAddAttributeRecords_Sign(AProject: Pointer; bValue: Boolean);

    procedure DeleteLayerTables(AProject, ALayer: Pointer);

    function LoadGeoTextData(AProject, ALayer: Pointer; sSourceDBFileName, sSourceFieldName, sDestinationFieldName, sProgisIDFieldName: AnsiString): Boolean;
    function GetActiveWindowProject: Pointer;
      // ++ Cadmensky
      //      procedure CorrectCalcFields(AProject: Pointer; ALayer: Pointer; iItemID: integer);
      // -- Cadmensky
            //****************   WinGIS <-> IDB interaction routines    ********************
            { Callback functions }

    procedure SetAnnotationSettingsDialogExecuteCallback(AFunction: TAnnotationSettingsDialogExecute);
    procedure SetMakeAnnotationInProjectCallback(AFunction: TMakeAnnotationInProject);
    procedure SetMakeAnnotationUpdateCallback(AProcedure: TMakeAnnotationUpdate);
    procedure MakeAnnotations(AProject: Pointer);
    procedure UpdateAnnotationData(AProject: Pointer; iLayerIndex, iItemID: Integer; AnAnnotData: PCollection);
    procedure SetLayerForInsertCallback(AFunction: TLayerForInsert);
      {Brovak}
    procedure SetMakeSpecialChartCallBack(AFunction: TMakeSpecialChart);
    procedure SetSendSpecialforChartCallback(AFunction: TSendSpecialforChart);
    procedure SetShowNewThematicMapDialogCallBack(AFunction: TShowNewThematicMapDialog);
    procedure SetSendToTMSelectedIDCallback(AFunction: TSendToTMSelectedID);
    procedure SetShowPercentAtWinGISCallback(AFunction: TShowPercentAtWinGIS);

    procedure SetChartSettingsDialogExecuteCallback(AFunction: TChartSettingsDialogExecute);
    procedure SetMakeChartInProjectCallback(AFunction: TMakeChartInProject);
    procedure SetMakeChartUpdateCallback(AProcedure: TMakeChartUpdate);
    procedure MakeChart(AProject: Pointer);
    procedure UpdateChartData(AProject: Pointer; iLayerIndex, iItemID: Integer; AnChartData: PCollection);
      {brovak}

    procedure SetShowProjectCallback(AProcedure: TShowProject);
    procedure SetShowSelectedItemsInProjectCallback(AProcedure: TShowSelectedItemsInProject);
    procedure SetShowItemInProjectWithZoomCallback(AProcedure: TShowItemInProjectWithZoom);
    procedure SetDeselectAllObjectsCallback(AProcedure: TDeselectAllObjects);
// ++ Commented by Cadmensky IDB Version 2.2.9
{      procedure GetProjectUndoStepsCountCallback (AFunction: TGetProjectUndoStepsCount);
      procedure GetProjectFileSizeCallback (AFunction: TGetProjectFileSize);}
// -- Commented by Cadmensky IDB Version 2.2.9
    procedure SetCheckItemExistingCallback(AFunction: TItemExistsInProject);
    procedure SetItemToGuidCallback(AFunction: TItemToGuid);
    procedure SetProjectModifiedCallback(AProcedure: TSetProjectModified);
    procedure SetSetStartProjectGUIDCallback(AProcedure: TSetStartProjectGUID);
    procedure SetSetCurrentProjectGUIDCallback(AProcedure: TSetCurrentProjectGUID);
    procedure SetGetStartProjectGUIDCallback(AFunction: TGetStartProjectGUID);
    procedure SetGetCurrentProjectGUIDCallback(AFunction: TGetCurrentprojectGUID);
    procedure SetGetProjectIsNewAndNotSavedCallback(AFunction: TGetProjectIsNewAndNotSaved);
    procedure SetGetProjectFullFileNameCallback(AFunction: TGetProjectFullFileName);
    procedure SetGetLayerNameCallback(AFunction: TGetLayerName);
    procedure SetGetLayerIndexCallback(AFunction: TGetLayerIndex);
    procedure SetGetAllProjectWindowsNumberCallback(AFunction: TGetAllProjectWindowsNumber);
    procedure SetGetProjectLayerNumberCallback(AFunction: TGetProjectLayerNumber);
    procedure SetGetLayerPointerByPositionCallback(AFunction: TGetLayerPointerByPosition);
    procedure SetGetLayerPointerByIndexCallback(AFunction: TGetLayerPointerByIndex);
    procedure SetGetActiveLayerCallback(AFunction: TGetActiveLayer);
    procedure SetLayerIsFixedCallback(AFunction: TLayerIsFixed);
    procedure SetGetWinGISINIFileNameCallback(AFunction: TGetWinGISINIFileName);
    procedure SetGetDefaultProjectFileNameCallback(AFunction: TGetDefaultProjectFileName);
{      procedure SetGeneratePointCallback (AFunction: TGeneratePoint);
      procedure SetGenerateSymbolCallback (AFunction: TGenerateSymbol);}
    procedure SetShowDonePercentCallback(AFunction: TShowDonePercent);
// ++ Commented by Cadmensky Version 2.2.8
//      procedure SetGatherGarbageInProjectCallback (AProcedure: TGatherGarbageInProject);
//      procedure SetGatherSelectedGarbageInProjectCallback (AProcedure: TGatherGarbageInProject);
// ++ Commented by Cadmensky Version 2.2.8
    procedure SetRedrawProjectWindowCallback(AProcedure: TRedrawProjectWindow);
    procedure SetChangeGeoTextParametersCallback(AProcedure: TChangeGeoTextParameters);
// ++ Cadmensky Version 2.2.8
    procedure SetGetLayerItemsCountCallback(AFunction: TGetLayerItemsCount);
    procedure SetGetLayerItemIndexByNumberCallback(AFunction: TGetLayerItemIndexByNumber);
    procedure SetIsObjectSelectedCallback(AFunction: TIsObjectSelected);
// -- Cadmensky Version 2.2.8
    procedure SetRunThematicMapBuilderCallback(AFunction: TRunThematicMapBuilder);
    procedure SetRunMonitoringCallback(AProcedure: TRunMonitoring);
    procedure SetGetObjectPropsCallback(AFunction: TGetObjectProps);
    procedure SetReverseUsingIDBInfoCallback(AProcedure: TReverseUsingOfIDBInfo);
    procedure SetGetMDIChildWindowStateCallback(AFunction: TGetMDIChildWindowState);
//      procedure SetFillLayerNamesListCallback (AProcedure: TFillLayerNamesList);
    procedure SetSetNessesitySaveUndoDataSignCallback(AProcedure: TSetNessesitySaveUndoDataSign);
    procedure SetSaveUndoDataCallback(AProcedure: TSaveUndoData);
      {   }
    function GetItemIDToBeDeleted_FromIDB(AProject: Pointer; ALayer: Pointer): Integer;
    function GetReadyToReceiveIDsOfObjectToBeDeleted_FromWG(AProject: Pointer): Boolean;
    procedure ReceiveIDOfObjectToBeDeleted_FromWG(AProject, ALayer: Pointer; iObjectID: Integer);
    procedure DeleteObjects_FromWG(AProject: Pointer);

    function GetReadyToReceiveIDsOfObjectsForMonitoring(AProject: Pointer): Boolean;
    procedure ReceiveIDOfObjectForMonitoring(AProject, ALayer: Pointer; iObjectID: Integer);

    procedure SetGeoTextFieldNames(AProject, ALayer: Pointer; sGeoTextIDFieldName, sGeoTextFieldName: AnsiString);
    function GetGeoTextIDFieldName(AProject, ALayer: Pointer): AnsiString;
    function GetGeoTextFieldName(AProject, ALayer: Pointer): AnsiString;
      { Undo routines }
    procedure Set_AnExternalDBIsWorking_Sign(AProject: Pointer; bValue: Boolean);

    function GetReadyToReceiveIDsOfObjectsToBeRestored(AProject: Pointer): Boolean;
    procedure ReceiveIDOfObjectToBeRestored(AProject: Pointer; ALayer: Pointer; iObjectID: Integer);
    procedure ExecuteRestoring(AProject: Pointer);

// ++ Cadmensky
    procedure UpdateCalcFields(AProject: Pointer; ALayer: Pointer; iObjectID: Integer);
    procedure ShowRecordsForSelectedObjects(AProject: Pointer);
// -- Cadmensky

// ++ Commented by Cadmensky IDB Version 2.3.0
{      function GetReadyToReceiveIDsOfObjectsToBeUsedForDragAndDrop (AProject: Pointer): Boolean;
      procedure ReceiveIDOfObjectToBeUsedForDragAndDrop (AProject, ALayer: Pointer; iObjectID: Integer);
      procedure ExecuteDragAndDrop (AProject: Pointer; ASourceGridOfDragging: TObject);}
// -- Commented by Cadmensky IDB Version 2.3.0

    function GetReadyToReceiveIDsOfObjectsToBeMade(AProject: Pointer): Boolean;
    procedure ReceiveIDOfObjectToBeMade(AProject, ALayer: Pointer; iItemID: Integer);

    function X_GotoObjectRecord(AProject, ALayer: Pointer; iItemIndex: Integer): Boolean;
    function X_AddRecord(AProject, ALayer: Pointer; iItemIndex: Integer): Boolean;
    function X_GetFieldCount(AProject, ALayer: Pointer): Integer;
    function X_GetFieldName(AProject, ALayer: Pointer; iFieldIndex: Integer): AnsiString;
    function X_GetFieldValueByFieldName(AProject, ALayer: Pointer; sFieldName: AnsiString): Variant;
    function X_GetFieldValueByFieldIndex(AProject, ALayer: Pointer; iFieldIndex: Integer): Variant;
    procedure X_SetFieldValueByFieldName(AProject, ALayer: Pointer; sFieldName: AnsiString; FieldValue: Variant);
    procedure X_SetFieldValueByFieldIndex(AProject, ALayer: Pointer; iFieldIndex: Integer; FieldValue: Variant);

    function TheUserWantsToCancel(AWindow: TForm): Boolean;
  end;

implementation

uses SysUtils, AM_Main,
  MultiLng;

constructor TIDBLib.Create(AMainForm: TForm; sIDBLibFileName, sLanguageFileExtension: AnsiString);
var
  pcLangFileExt: PChar;
  iLength: Integer;
begin
  bTheLibraryWasInitialized := FALSE;
{$IFNDEF AXDLL} // <----------------- AXDLL

  FMAllowed := True;

   // Load IDB dll.
  hIDBLibrary := LoadLibrary(PChar(sIDBLibFileName));
  WingisMainForm.CheckLibStatus(hIDBLibrary, ExtractFileName(sIDBLibFileName), 'IDB');
  if hIDBLibrary = 0 then
    EXIT;
   // Detect all function from IDB dll.
   //********     Initialization and finalization routnes of the DLL.  **********//
  InitLibraryFunc := GetProcAddress(hIDBLibrary, 'InitLibrary');
  if @InitLibraryFunc = nil then
    EXIT;

  FinLibraryProc := GetProcAddress(hIDBLibrary, 'FinLibrary');
  if @FinLibraryProc = nil then
    EXIT;

  GetVersionFunc := GetProcAddress(hIDBLibrary, 'GetVersion');

  SetReadOnlyStateProc := GetProcAddress(hIDBLibrary, 'SetReadOnlyState');
  if @SetReadOnlyStateProc = nil then
    EXIT;

   //****************             GeoText routines.           *******************//
  GetGeoTextFunc := GetProcAddress(hIDBLibrary, 'GetGeoText');
  if @GetGeoTextFunc = nil then
    EXIT;

  SetGeoTextProc := GetProcAddress(hIDBLibrary, 'SetGeoText');
  if @SetGeoTextProc = nil then
    EXIT;

  GetTableFieldsNamesListFunc := GetProcAddress(hIDBLibrary, 'GetTableFieldsNamesList');
  if @GetTableFieldsNamesListFunc = nil then
    EXIT;

  GetTableVisibleFieldsNamesListFunc := GetProcAddress(hIDBLibrary, 'GetTableVisibleFieldsNamesList');
  if @GetTableVisibleFieldsNamesListFunc = nil then
    EXIT;
   //****************            IDB core routines            *******************//
  AddProjectFunc := GetProcAddress(hIDBLibrary, 'AddProject');
  if @AddProjectFunc = nil then
    EXIT;

  DeleteProjectProc := GetProcAddress(hIDBLibrary, 'DeleteProject');
  if @DeleteProjectProc = nil then
    EXIT;

  ShowAttributeWindowProc := GetProcAddress(hIDBLibrary, 'ShowAttributeWindow');
  if @ShowAttributeWindowProc = nil then
    EXIT;

  SetMakeSpecialChartCallBackProc := GetProcAddress(hIDBLibrary, 'SetMakeSpecialChartCallBack');
  if @SetMakeSpecialChartCallBackProc = nil then
    exit;

  SetLayerForInsertCallbackProc := GetProcAddress(hIDBLibrary, 'SetLayerForInsertCallback');
  if @SetLayerForInsertCallbackProc = nil then
    Exit;

  SetSendSpecialforChartCallbackProc := GetProcAddress(hIDBLibrary, 'SetSendSpecialforChartCallBack');
  if @SetSendSpecialforChartCallbackProc = nil then
    exit;

  SetShowNewThematicMapDialogCallBackProc := GetProcAddress(hIDBLibrary, 'SetShowNewThematicMapDialogCallBack');
  if @SetShowNewThematicMapDialogCallBackProc = nil then
    exit;

  SetShowPercentAtWinGISCallbackProc := GetProcAddress(hIDBLIbrary, 'SetShowPercentAtWinGISCallBack');
  if @SetShowPercentAtWinGISCallbackProc = nil then
    exit;

  SetSendToTMSelectedIDCallBackProc := GetProcAddress(hIDBLibrary, 'SetSendToTMSelectedIDCallBack');
  if @SetSendToTMSelectedIDCallBackProc = nil then
    exit;

  RefreshAllDataWindowsProc := GetProcAddress(hIDBLibrary, 'RefreshAllDataWindows');
  if @RefreshAllDataWindowsProc = nil then
    EXIT;

  CloseAttributeWindowProc := GetProcAddress(hIDBLibrary, 'CloseAttributeWindow');
  if @CloseAttributeWindowProc = nil then
    EXIT;

  CloseAllWindowsOfProjectProc := GetProcAddress(hIDBLibrary, 'CloseAllWindowsOfProject');
  if @CloseAllWindowsOfProjectProc = nil then
    EXIT;

   {brovak}
  IsOpenedWindowProc := GetProcAddress(hIDBLibrary, 'IsOpenedWindow');
  if @IsOpenedWindowProc = nil then
    EXIT;
   {brovak}
  CloseAllWindowsOfAllProjectsProc := GetProcAddress(hIDBLibrary, 'CloseAllWindowsOfAllProjects');
  if @CloseAllWindowsOfAllProjectsProc = nil then
    EXIT;

  ShowMonitoringWindowProc := GetProcAddress(hIDBLibrary, 'ShowMonitoringWindow');
  if @ShowMonitoringWindowProc = nil then
    EXIT;

  ShowInfoWindowProc := GetProcAddress(hIDBLibrary, 'ShowInfoWindow');
  if @ShowInfoWindowProc = nil then
    EXIT;

  ClearInfoWindowProc := GetProcAddress(hIDBLibrary, 'ClearInfoWindow');
  if @ClearInfoWindowProc = nil then
    EXIT;

  CloseInfoWindowProc := GetProcAddress(hIDBLibrary, 'CloseInfoWindow');
  if @CloseInfoWindowProc = nil then
    EXIT;

  HideInfoWindowProc := GetProcAddress(hIDBLibrary, 'HideInfoWindow');
  if @HideInfoWindowProc = nil then
    EXIT;

  ProjectIsBeingSavedProc := GetProcAddress(hIDBLibrary, 'ProjectIsBeingSaved');
  if @ProjectIsBeingSavedProc = nil then
    EXIT;

  SaveDatabaseAsProc := GetProcAddress(hIDBLibrary, 'SaveDatabaseAs');
  if @SaveDatabaseAsProc = nil then
    EXIT;

  DeleteProjectAndDatabaseFilesProc := GetProcAddress(hIDBLibrary, 'DeleteProjectAndDatabaseFiles');
  if @DeleteProjectAndDatabaseFilesProc = nil then
    EXIT;

  InsertItemProc := GetProcAddress(hIDBLibrary, 'InsertItem');
  if @InsertItemProc = nil then
    EXIT;

   // ++ Cadmensky
  OverlayInsertItemProc := GetProcAddress(hIDBLibrary, 'OverlayInsertItem');
  if @OverlayInsertItemProc = nil then
    EXIT;
   // -- Cadmensky

// ++ Cadmensky Version 2.2.8
  MoveAttributeInfoProc := GetProcAddress(hIDBLibrary, 'MoveAttributeInfo');
  if @MoveAttributeInfoProc = nil then
    EXIT;

// -- Cadmensky Version 2.2.8

  CopyAttributeInfoProc := GetProcAddress(hIDBLibrary, 'CopyAttributeInfo');
  if @CopyAttributeInfoProc = nil then
    EXIT;

// ++ Commented by Cadmensky IDB Version 2.2.8
{   GetReadyToInsertRecordsFunc := GetProcAddress (hIDBLibrary, 'GetReadyToInsertRecords');
   if @GetReadyToInsertRecordsFunc = nil then EXIT;

   InsertRecordFunc := GetProcAddress (hIDBLibrary, 'InsertRecord');
   if @InsertRecordFunc = nil then EXIT;

   EndInsertingProc := GetProcAddress (hIDBLibrary, 'EndInserting');
   if @EndInsertingProc = nil then EXIT;}
// ++ Commented by Cadmensky IDB Version 2.2.8

  GetReadyToPolygonOverlayingFunc := GetProcAddress(hIDBLibrary, 'GetReadyToPolygonOverlaying');
  if @GetReadyToPolygonOverlayingFunc = nil then
    EXIT;

  EndOfPolygonOverlayingProc := GetProcAddress(hIDBLibrary, 'EndOfPolygonOverlaying');
  if @EndOfPolygonOverlayingProc = nil then
    EXIT;

  Set_DoNotAddAttributeRecords_SignProc := GetProcAddress(hIDBLibrary, 'Set_DoNotAddAttributeRecords_Sign');
  if @Set_DoNotAddAttributeRecords_SignProc = nil then
    EXIT;

  DeleteLayerTablesProc := GetProcAddress(hIDBLibrary, 'DeleteLayerTables');
  if @DeleteLayerTablesProc = nil then
    EXIT;

  GetActiveWindowProjectFunc := GetProcAddress(hIDBLibrary, 'GetActiveWindowProject');
  if @GetActiveWindowProjectFunc = nil then
    EXIT;

  X_GotoObjectRecordFunc := GetProcAddress(hIDBLibrary, 'X_GotoObjectRecord');
  if @X_GotoObjectRecordFunc = nil then
    EXIT;

  X_AddRecordFunc := GetProcAddress(hIDBLibrary, 'X_AddRecord');
  if @X_AddRecordFunc = nil then
    EXIT;

  X_GetFieldCountFunc := GetProcAddress(hIDBLibrary, 'X_GetFieldCount');
  if @X_GetFieldCountFunc = nil then
    EXIT;

  X_GetFieldNameFunc := GetProcAddress(hIDBLibrary, 'X_GetFieldName');
  if @X_GetFieldNameFunc = nil then
    EXIT;

  X_GetFieldValueByFieldNameFunc := GetProcAddress(hIDBLibrary, 'X_GetFieldValueByFieldName');
  if @X_GetFieldValueByFieldNameFunc = nil then
    EXIT;

  X_GetFieldValueByFieldIndexFunc := GetProcAddress(hIDBLibrary, 'X_GetFieldValueByFieldIndex');
  if @X_GetFieldValueByFieldIndexFunc = nil then
    EXIT;

  X_SetFieldValueByFieldNameProc := GetProcAddress(hIDBLibrary, 'X_SetFieldValueByFieldName');
  if @X_SetFieldValueByFieldNameProc = nil then
    EXIT;

  X_SetFieldValueByFieldIndexProc := GetProcAddress(hIDBLibrary, 'X_SetFieldValueByFieldIndex');
  if @X_SetFieldValueByFieldIndexProc = nil then
    EXIT;

  UpdateAnnotationDataProc := GetProcAddress(hIDBLibrary, 'UpdateAnnotationData');
  if @UpdateAnnotationDataProc = nil then
    EXIT;

  UpdateChartDataProc := GetProcAddress(hIDBLibrary, 'UpdateChartData');
  if @UpdateChartDataProc = nil then
    EXIT;

   { Callback functions }
  SetShowProjectCallbackproc := GetProcAddress(hIDBLibrary, 'SetShowProjectCallback');
  if @SetShowProjectCallbackproc = nil then
    EXIT;

  SetShowSelectedItemsInProjectCallbackProc := GetProcAddress(hIDBLibrary, 'SetShowSelectedItemsInProjectCallback');
  if @SetShowSelectedItemsInProjectCallbackProc = nil then
    EXIT;

  SetShowItemInProjectWithZoomCallbackProc := GetProcAddress(hIDBLibrary, 'SetShowItemInProjectWithZoomCallback');
  if @SetShowItemInProjectWithZoomCallbackProc = nil then
    EXIT;

  SetDeselectAllObjectsCallbackProc := GetProcAddress(hIDBLibrary, 'SetDeselectAllObjectsCallback');
  if @SetDeselectAllObjectsCallbackProc = nil then
    EXIT;

  SetCheckItemExistingCallbackProc := GetProcAddress(hIDBLibrary, 'SetCheckItemExistingCallback');
  if @SetCheckItemExistingCallbackProc = nil then
    EXIT;

  SetItemToGuidCallbackProc := GetProcAddress(hIDBLibrary, 'SetItemToGuidCallback');
  if @SetItemToGuidCallbackProc = nil then
    EXIT;

  SetSetProjectModifiedCallbackProc := GetProcAddress(hIDBLibrary, 'SetSetProjectModifiedCallback');
  if @SetSetProjectModifiedCallbackProc = nil then
    EXIT;

  SetSetStartProjectGUIDCallbackProc := GetProcAddress(hIDBLibrary, 'SetSetStartProjectGUIDCallback');
  if @SetSetStartProjectGUIDCallbackProc = nil then
    EXIT;

  SetSetCurrentProjectGUIDCallbackProc := GetprocAddress(hIDBLibrary, 'SetSetCurrentProjectGUIDCallback');
  if @SetSetCurrentProjectGUIDCallbackProc = nil then
    EXIT;

  SetGetStartProjectGUIDCallbackProc := GetprocAddress(hIDBLibrary, 'SetGetStartProjectGUIDCallback');
  if @SetGetStartProjectGUIDCallbackProc = nil then
    EXIT;

  SetGetCurrentProjectGUIDCallbackProc := GetProcAddress(hIDBLibrary, 'SetGetCurrentProjectGUIDCallback');
  if @SetGetCurrentProjectGUIDCallbackProc = nil then
    EXIT;

  SetGetProjectIsNewAndNotSavedCallbackProc := GetProcAddress(hIDBLibrary, 'SetGetProjectIsNewAndNotSavedCallback');
  if @SetGetProjectIsNewAndNotSavedCallbackProc = nil then
    EXIT;

  SetGetAllProjectWindowsNumberCallbackProc := GetProcAddress(hIDBLibrary, 'SetGetAllProjectWindowsNumberCallback');
  if @SetGetAllProjectWindowsNumberCallbackProc = nil then
    EXIT;

  SetGetProjectLayerNumberCallbackProc := GetProcAddress(hIDBLibrary, 'SetGetProjectLayerNumberCallback');
  if @SetGetProjectLayerNumberCallbackProc = nil then
    EXIT;

  SetGetLayerPointerByPositionCallbackProc := GetProcAddress(hIDBLibrary, 'SetGetLayerPointerByPositionCallback');
  if @SetGetLayerPointerByPositionCallbackProc = nil then
    EXIT;

  SetGetLayerPointerByIndexCallbackProc := GetProcAddress(hIDBLibrary, 'SetGetLayerPointerByIndexCallback');
  if @SetGetLayerPointerByIndexCallbackProc = nil then
    EXIT;

// ++ Cadmensky
  SetGetActiveLayerCallbackProc := GetProcAddress(hIDBLibrary, 'SetGetActiveLayerCallback');
  if @SetGetActiveLayerCallbackProc = nil then
    EXIT;
// -- Cadmensky

  SetLayerIsFixedCallbackProc := GetProcAddress(hIDBLibrary, 'SetLayerIsFixedCallback');
  if @SetLayerIsFixedCallbackProc = nil then
    EXIT;

{   SetGeneratePointCallbackProc := GetProcAddress (hIDBLibrary, 'SetGeneratePointCallback');
   if @SetGeneratePointCallbackProc = nil then EXIT;

   SetGenerateSymbolCallbackProc := GetProcAddress (hIDBLibrary, 'SetGenerateSymbolCallback');
   if @SetGenerateSymbolCallbackProc = nil then EXIT;}

  SetAnnotationSettingsDialogExecuteCallbackProc := GetProcAddress(hIDBLibrary, 'SetAnnotationSettingsDialogExecuteCallback');
  if @SetAnnotationSettingsDialogExecuteCallbackProc = nil then
    EXIT;

  SetMakeAnnotationInProjectCallbackProc := GetProcAddress(hIDBLibrary, 'SetMakeAnnotationInProjectCallback');
  if @SetMakeAnnotationInProjectCallbackProc = nil then
    EXIT;
   {brovak}
  SetChartSettingsDialogExecuteCallbackProc := GetProcAddress(hIDBLibrary, 'SetChartSettingsDialogExecuteCallback');
  if @SetChartSettingsDialogExecuteCallbackProc = nil then
    EXIT;

  SetMakeChartInProjectCallbackProc := GetProcAddress(hIDBLibrary, 'SetMakeChartInProjectCallback');
  if @SetMakeChartInProjectCallbackProc = nil then
    EXIT;
   {brovak}
  SetShowDonePercentCallbackProc := GetProcAddress(hIDBLibrary, 'SetShowDonePercentCallback');
  if @SetShowDonePercentCallbackProc = nil then
    EXIT;

  SetGetProjectFullFileNameCallbackProc := GetProcAddress(hIDBLibrary, 'SetGetProjectFullFileNameCallback');
  if @SetGetProjectFullFileNameCallbackProc = nil then
    EXIT;

  SetGetLayerNameCallbackProc := GetProcAddress(hIDBLibrary, 'SetGetLayerNameCallback');
  if @SetGetLayerNameCallbackProc = nil then
    EXIT;

  SetGetLayerIndexCallbackProc := GetProcAddress(hIDBLibrary, 'SetGetLayerIndexCallback');
  if @SetGetLayerIndexCallbackProc = nil then
    EXIT;

// ++ Commented by Cadmensky Version 2.2.8
{   SetGatherGarbageInProjectCallbackProc := GetProcAddress (hIDBLibrary, 'SetGatherGarbageInProjectCallback');
   if @SetGatherGarbageInProjectCallbackProc = nil then EXIT;

   SetGatherSelectedGarbageInProjectCallbackProc := GetProcAddress (hIDBLibrary, 'SetGatherSelectedGarbageInProjectCallback');
   if @SetGatherSelectedGarbageInProjectCallbackProc = nil then EXIT;}
// -- Commented by Cadmensky Version 2.2.8

  SetRedrawProjectWindowCallbackProc := GetProcAddress(hIDBLibrary, 'SetRedrawProjectWindowCallback');
  if @SetRedrawProjectWindowCallbackProc = nil then
    EXIT;

// ++ Cadmensky Version 2.2.8
  SetGetLayerItemsCountCallbackProc := GetProcAddress(hIDBLibrary, 'SetGetLayerItemsCountCallback');
  if @SetGetLayerItemsCountCallbackProc = nil then
    EXIT;

  SetGetLayerItemIndexByNumberCallbackProc := GetProcAddress(hIDBLibrary, 'SetGetLayerItemIndexByNumberCallback');
  if @SetGetLayerItemIndexByNumberCallbackProc = nil then
    EXIT;

  SetIsObjectSelectedCallbackProc := GetProcAddress(hIDBLibrary, 'SetIsObjectSelectedCallback');
  if @SetIsObjectSelectedCallbackProc = nil then
    EXIT;
// -- Cadmensky Version 2.2.8

  SetRunThematicMapBuilderCallbackProc := GetProcAddress(hIDBLibrary, 'SetRunThematicMapBuilderCallback');
  if @SetRunThematicMapBuilderCallbackProc = nil then
    EXIT;

  SetRunMonitoringCallbackProc := GetProcAddress(hIDBLibrary, 'SetRunMonitoringCallback');
  if @SetRunMonitoringCallbackProc = nil then
    EXIT;

  SetGetMDIChildWindowStateCallbackProc := GetProcAddress(hIDBLibrary, 'SetGetMDIChildWindowStateCallback');
  if @SetGetMDIChildWindowStateCallbackProc = nil then
    EXIT;

  SetMakeAnnotationUpdateCallbackProc := GetProcAddress(hIDBLibrary, 'SetMakeAnnotationUpdateCallback');
  if @SetMakeAnnotationUpdateCallbackProc = nil then
    EXIT;

   {brovak}
  SetMakeChartUpdateCallbackProc := GetProcAddress(hIDBLibrary, 'SetMakeChartUpdateCallback');
  if @SetMakeChartUpdateCallbackProc = nil then
    EXIT;
   {brovak}

   {   }
  GetItemIDToBeDeleted_FromIDBFunc := GetProcAddress(hIDBLibrary, 'GetItemIDToBeDeleted_FromIDB');
  if @GetItemIDToBeDeleted_FromIDBFunc = nil then
    EXIT;

  GetReadyToReceiveIDsOfObjectToBeDeleted_FromWGFunc := GetProcAddress(hIDBLibrary, 'GetReadyToReceiveIDsOfObjectToBeDeleted_FromWG');
  if @GetReadyToReceiveIDsOfObjectToBeDeleted_FromWGFunc = nil then
    EXIT;

  ReceiveIDOfObjectToBeDeleted_FromWGProc := GetProcAddress(hIDBLibrary, 'ReceiveIDOfObjectToBeDeleted_FromWG');
  if @ReceiveIDOfObjectToBeDeleted_FromWGProc = nil then
    EXIT;

  DeleteObjects_FromWGProc := GetProcAddress(hIDBLibrary, 'DeleteObjects_FromWG');
  if @DeleteObjects_FromWGProc = nil then
    EXIT;

  GetReadyToReceiveIDsOfObjectsForMonitoringFunc := GetProcAddress(hIDBLibrary, 'GetReadyToReceiveIDsOfObjectsForMonitoring');
  if @GetReadyToReceiveIDsOfObjectsForMonitoringFunc = nil then
    EXIT;

  ReceiveIDOfObjectForMonitoringProc := GetProcAddress(hIDBLibrary, 'ReceiveIDOfObjectForMonitoring');
  if @ReceiveIDOfObjectForMonitoringProc = nil then
    EXIT;

  GetReadyToReceiveIDsOfObjectsToBeMadeFunc := GetProcAddress(hIDBLibrary, 'GetReadyToReceiveIDsOfObjectsToBeMade');
  if @GetReadyToReceiveIDsOfObjectsToBeMadeFunc = nil then
    EXIT;

  ReceiveIDOfObjectToBeMadeProc := GetProcAddress(hIDBLibrary, 'ReceiveIDOfObjectToBeMade');
  if @ReceiveIDOfObjectToBeMadeProc = nil then
    EXIT;

  MakeAnnotationsProc := GetProcAddress(hIDBLibrary, 'MakeAnnotations');
  if @MakeAnnotationsProc = nil then
    EXIT;

   {brovak}
  MakeChartProc := GetProcAddress(hIDBLibrary, 'MakeChart');
  if @MakeChartProc = nil then
    EXIT;
   {brovak}

  SetGeoTextFieldNamesProc := GetProcAddress(hIDBLibrary, 'SetGeoTextFieldNames');
  if @SetGeoTextFieldNamesProc = nil then
    EXIT;

  GetGeoTextIDFieldNameFunc := GetProcAddress(hIDBLibrary, 'GetGeoTextIDFieldName');
  if @GetGeoTextIDFieldNameFunc = nil then
    EXIT;

  GetGeoTextFieldNameFunc := GetProcAddress(hIDBLibrary, 'GetGeoTextFieldName');
  if @GetGeoTextFieldNameFunc = nil then
    EXIT;

  LoadGeoTextDataFunc := GetProcAddress(hIDBLibrary, 'LoadGeoTextData');
  if @LoadGeoTextDataFunc = nil then
    EXIT;

  SetGetWinGISINIFileNameCallbackProc := GetProcAddress(hIDBLibrary, 'SetGetWinGISINIFileNameCallback');
  if @SetGetWinGISINIFileNameCallbackProc = nil then
    EXIT;

  SetGetDefaultProjectFileNameCallbackProc := GetProcAddress(hIDBLibrary, 'SetGetDefaultProjectFileNameCallback');
  if @SetGetDefaultProjectFileNameCallbackProc = nil then
    EXIT;

  SetGetObjectPropsCallbackProc := GetProcAddress(hIDBLibrary, 'SetGetObjectPropsCallback');
  if @SetGetObjectPropsCallbackProc = nil then
    EXIT;

  SetReverseUsingIDBInfoCallbackProc := GetProcAddress(hIDBLibrary, 'SetReverseUsingIDBInfoCallback');
  if @SetReverseUsingIDBInfoCallbackProc = nil then
    EXIT;

  GetReadyToReceiveIDsOfObjectsToBeRestoredFunc := GetProcAddress(hIDBLibrary, 'GetReadyToReceiveIDsOfObjectsToBeRestored');
  if @GetReadyToReceiveIDsOfObjectsToBeRestoredFunc = nil then
    EXIT;

  ReceiveIDOfObjectToBeRestoredProc := GetProcAddress(hIDBLibrary, 'ReceiveIDOfObjectToBeRestored');
  if @ReceiveIDOfObjectToBeRestoredProc = nil then
    EXIT;

// ++ Cadmensky
  UpdateCalcFieldsProc := GetProcAddress(hIDBLibrary, 'UpdateCalcFields');
  if @UpdateCalcFieldsProc = nil then
    EXIT;

  ShowRecordsForSelectedObjectsProc := GetProcAddress(hIDBLibrary, 'ShowRecordsForSelectedObjects');
  if @ShowRecordsForSelectedObjectsProc = nil then
    EXIT;
// -- Cadmensky

  ExecuteRestoringProc := GetProcAddress(hIDBLibrary, 'ExecuteRestoring');
  if @ExecuteRestoringProc = nil then
    EXIT;

// ++ Commented by Cadmensky IDB Version 2.3.0
{   GetReadyToReceiveIDsOfObjectsToBeUsedForDragAndDropFunc := GetProcAddress (hIDBLibrary, 'GetReadyToReceiveIDsOfObjectsToBeUsedForDragAndDrop');
   if @GetReadyToReceiveIDsOfObjectsToBeUsedForDragAndDropFunc = nil then EXIT;

   ReceiveIDOfObjectToBeUsedForDragAndDropProc := GetProcAddress (hIDBLibrary, 'ReceiveIDOfObjectToBeUsedForDragAndDrop');
   if @ReceiveIDOfObjectToBeUsedForDragAndDropProc = nil then EXIT;

   ExecuteDragAndDropProc := GetProcAddress (hIDBLibrary, 'ExecuteDragAndDrop');
   if @ExecuteDragAndDropProc = nil then EXIT;}
// -- Commented by Cadmensky IDB Version 2.3.0

  SetChangeGeoTextParametersCallbackProc := GetProcAddress(hIDBLibrary, 'SetChangeGeoTextParametersCallback');
  if @SetChangeGeoTextParametersCallbackProc = nil then
    EXIT;

   { This function is not strongly required. If is missed, the canceling won't work. No more...}
  TheUserWantsToCancelFunc := GetProcAddress(hIDBLibrary, 'TheUserWantsToCancel');

// ++ Cadmensky
// ++ UndoRedo
  SetSetNessesitySaveUndoDataSignCallbackProc := GetProcAddress(hIDBLibrary, 'SetSetNessesitySaveUndoDataSignCallback');
  if @SetSetNessesitySaveUndoDataSignCallbackProc = nil then
    EXIT;

  SetSaveUndoDataCallbackProc := GetProcAddress(hIDBLibrary, 'SetSaveUndoDataCallback');
  if @SetSaveUndoDataCallbackProc = nil then
    EXIT;
// -- UndoRedo
// -- Cadmensky

  @F_GetLinesPerLayer := GetProcAddress(hIDBLibrary, 'F_GetLinesPerLayer');
  if @F_GetLinesPerLayer = nil then
    FMAllowed := False;

  @F_GetSymbolsPerLayer := GetProcAddress(hIDBLibrary, 'F_GetSymbolsPerLayer');
  if @F_GetSymbolsPerLayer = nil then
    FMAllowed := False;

  @F_SymbolAllowed := GetProcAddress(hIDBLibrary, 'F_SymbolAllowed');
  if @F_SymbolAllowed = nil then
    FMAllowed := False;

      // Init IDB library.
  iLength := Length(sLanguageFileExtension) + 1;
  GetMem(pcLangFileExt, iLength);
  try
    StrPCopy(pcLangFileExt, SLanguageFileExtension);
    if @InitLibraryFunc <> nil then
      bTheLibraryWasInitialized := InitLibraryFunc(Integer(Application), Integer(Screen), Integer(AMainForm), pcLangFileExt);
  finally
    FreeMem(pcLangFileExt, iLength);
  end;
{$ENDIF}
end;

destructor TIDBLib.Free;
begin
   // Get ready to unload IDB dll.
  if @FinLibraryProc <> nil then
    FinLibraryProc;
  if hIDBLibrary <> 0 then
    FreeLibrary(hIDBLibrary);
end;

function TIDBLib.LibraryWasLoaded: Boolean;
begin
  RESULT := SELF.bTheLibraryWasInitialized;
end;

function TIDBLib.GetLibraryVersion: AnsiString;
var
  pcData: PChar;
begin
  if @GetVersionFunc <> nil then
  begin
    pcData := GetVersionFunc;
    RESULT := pcData;
    FreeMem(pcData);
  end
  else
    RESULT := MlgStringList['IDB', 12]; // Unknown
end;

procedure TIDBLib.SetReadOnlyState(bReadOnlyState: Boolean);
begin
  if @SetReadOnlyStateProc <> nil then
    SetReadOnlyStateProc(bReadOnlyState);
end;

//****************             GeoText routines.           *******************//

function TIDBLib.GetGeoText(AProject: Pointer; iLayerIndex, iItemID: Integer {; sGeoTextIDFieldName, sGeoTextValueFieldName: AnsiString}): AnsiString;
var
  pcData: PChar;
begin
  if @GetGeoTextFunc <> nil then
  begin
    pcData := GetGeoTextFunc(AProject, iLayerIndex, iItemID {, PChar (sGeoTextIDFieldName), PChar (sGeoTextValueFieldName)});
    RESULT := pcData;
    FreeMem(pcData);
  end
  else
    RESULT := '';
end;

procedure TIDBLib.SetGeoText(AProject: Pointer; iLayerIndex, iItemID: Integer; {sGeoTextIDFieldName, sGeoTextValueFieldName,} sGeoTextValue: AnsiString);
begin
  if @SetGeoTextProc <> nil then
    SetGeoTextProc(AProject, iLayerIndex, iItemID, {PChar (sGeoTextIDFieldName), PChar (sGeoTextValueFieldName),} PChar(sGeoTextValue));
end;

function TIDBLib.GetTableFieldsNamesList(AProject: Pointer; ALayer: Pointer): AnsiString;
var
  pcData: PChar;
begin
  if @GetTableFieldsNamesListFunc <> nil then
  begin
    pcData := GetTableFieldsNamesListFunc(AProject, ALayer);
    RESULT := pcData;
    FreeMem(pcData);
  end
  else
    RESULT := '';
end;

function TIDBLib.GetTableVisibleFieldsNamesList(AProject: Pointer; ALayer: Pointer): AnsiString;
var
  pcData: PChar;
begin
  if @GetTableVisibleFieldsNamesListFunc <> nil then
  begin
    pcData := GetTableVisibleFieldsNamesListFunc(AProject, ALayer);
    RESULT := pcData;
    FreeMem(pcData);
  end
  else
    RESULT := '';
end;

//****************            IDB core routines.           *******************//

function TIDBLib.AddProject(AProjectFormHandle: Integer; AProject: Pointer; bProjectFileHasReadOnlyState: Boolean): Boolean;
begin
  if @AddProjectFunc <> nil then
    RESULT := AddProjectFunc(AProjectFormHandle, AProject, bProjectFileHasReadOnlyState)
  else
    RESULT := FALSE;
end;

procedure TIDBLib.DeleteProject(AProject: Pointer);
begin
  if @DeleteProjectProc <> nil then
    DeleteProjectProc(AProject);
end;

procedure TIDBLib.ShowDataTable(AProject, ALayer, AALayer: Pointer);
begin
   // ++ Cadmensky
  if (SELF.IsOpenedWindow(AProject) div 2 > 0) then
    SELF.CloseAllWindowsOfAProject(AProject);
   // -- Cadmensky

  if @ShowAttributeWindowProc <> nil then
    ShowAttributeWindowProc(AProject, ALayer, AAlayer);
end;

procedure TIDBLib.SetMakeSpecialChartCallback(AFunction: TMakeSpecialChart);
begin;
  if @SetMakeSpecialChartCallbackProc <> nil then
    SetMakeSpecialChartCallbackProc(AFunction);
end;

procedure TIDBLib.SetSendSpecialforChartCallback(AFunction: TSendSpecialforChart);
begin;
  if @SetSendSpecialforChartCallbackProc <> nil then
    SetSendSpecialforChartCallbackProc(AFunction);
end;

procedure TIDBLib.SetShowNewThematicMapDialogCallBack(AFunction: TShowNewThematicMapDialog);
begin
  if @SetShowNewThematicMapDialogCallBackProc <> nil then
    SetShowNewThematicMapDialogCallBackProc(AFunction);
end;

procedure TIDBLib.SetShowPercentAtWinGISCallBack(AFunction: TShowPercentAtWinGIS);
begin;
  if @SetShowPercentAtWinGISCallBackProc <> nil then
    SetShowPercentAtWinGISCallBackProc(AFunction);
end;

procedure TIDBLib.SetSendToTMSelectedIDCallBack(AFunction: TSendToTMSelectedID);
begin;
  if @SetSendToTMSelectedIDCallBackProc <> nil then
    SetSendToTMSelectedIDCallBackProc(AFunction);
end;

procedure TIDBLib.RefreshAllDataWindows(AProject: Pointer);
begin
  if @RefreshAllDataWindowsProc <> nil then
    RefreshAllDataWindowsProc(AProject);
end;

procedure TIDBLib.CloseAttributeWindow(AProject, ALayer: Pointer);
begin
  if @CloseAttributeWindowProc <> nil then
    CloseAttributeWindowProc(AProject, ALayer);
end;

{brovak}

function TIDBLib.IsOpenedWindow(AProject: Pointer): integer;
begin
  Result := 0;
  if @IsOpenedWindowProc <> nil then
    Result := IsOpenedWindowProc(AProject);
end;
{brovak}

procedure TIDBLib.CloseAllWindowsOfAProject(AProject: Pointer);
begin
  if @CloseAllWindowsOfProjectProc <> nil then
    CloseAllWindowsOfProjectProc(AProject);
end;

procedure TIDBLib.CloseAllWindowsOfAllProjects;
begin
  if @CloseAllWindowsOfAllProjectsProc <> nil then
    CloseAllWindowsOfAllProjectsProc;
end;

procedure TIDBLib.ShowMonitoringWindow(AProject: Pointer; bAlwaysShowMonitoring: Boolean);
begin
  if @ShowMonitoringWindowProc <> nil then
    ShowMonitoringWindowProc(AProject, bAlwaysShowMonitoring);
end;

procedure TIDBLib.ShowInfoWindow(AProject: Pointer);
begin
  if @ShowInfoWindowProc <> nil then
    ShowInfoWindowProc(AProject);
end;

procedure TIDBLib.ClearInfoWindow(AProject: Pointer);
begin
  if @ClearInfoWindowProc <> nil then
    ClearInfoWindowProc(AProject);
end;

procedure TIDBLib.CloseInfoWindow;
begin
  if @CloseInfoWindowProc <> nil then
    CloseInfoWindowProc;
end;

procedure TIDBLib.HideInfoWindow;
begin
  if @HideInfoWindowProc <> nil then
    HideInfoWindowProc;
end;

//****************   WinGIS <-> IDB interaction routines.   ******************//

function TIDBLib.GetItemIDToBeDeleted_FromIDB(AProject: Pointer; ALayer: Pointer): Integer;
begin
  if @GetItemIDToBeDeleted_FromIDBFunc <> nil then
    RESULT := GetItemIDToBeDeleted_FromIDBFunc(AProject, ALayer)
  else
    RESULT := -1;
end;

function TIDBLib.GetReadyToReceiveIDsOfObjectToBeDeleted_FromWG(AProject: Pointer): Boolean;
begin
  if @GetReadyToReceiveIDsOfObjectToBeDeleted_FromWGFunc <> nil then
    RESULT := GetReadyToReceiveIDsOfObjectToBeDeleted_FromWGFunc(AProject)
  else
    RESULT := FALSE;
end;

procedure TIDBLib.ReceiveIDOfObjectToBeDeleted_FromWG(AProject, ALayer: Pointer; iObjectID: Integer);
begin
  if @ReceiveIDOfObjectToBeDeleted_FromWGProc <> nil then
    ReceiveIDOfObjectToBeDeleted_FromWGProc(AProject, ALayer, iObjectID);
end;

procedure TIDBLib.DeleteObjects_FromWG(AProject: Pointer);
begin
  if @DeleteObjects_FromWGProc <> nil then
    DeleteObjects_FromWGProc(AProject);
end;

procedure TIDBLib.SetShowProjectCallback(AProcedure: TShowProject);
begin
  if @SetShowProjectCallbackProc <> nil then
    SetShowProjectCallbackProc(AProcedure);
end;

procedure TIDBLib.SetShowSelectedItemsInProjectCallback(AProcedure: TShowSelectedItemsInProject);
begin
  if @SetShowSelectedItemsInProjectCallbackProc <> nil then
    SetShowSelectedItemsInProjectCallbackProc(AProcedure);
end;

procedure TIDBLib.SetShowItemInProjectWithZoomCallback(AProcedure: TShowItemInProjectWithZoom);
begin
  if @SetShowItemInProjectWithZoomCallbackProc <> nil then
    SetShowItemInProjectWithZoomCallbackProc(AProcedure);
end;

procedure TIDBLib.SetDeselectAllObjectsCallback(AProcedure: TDeselectAllObjects);
begin
  if @SetDeselectAllObjectsCallbackProc <> nil then
    SetDeselectAllObjectsCallbackProc(AProcedure);
end;

// ++ Commented by Cadmensky IDB Version 2.2.9
{procedure TIDBLib.GetProjectUndoStepsCountCallback (AFunction: TGetProjectUndoStepsCount);
begin
   if @GetProjectUndoStepsCountCallbackProc <> nil then
      GetProjectUndoStepsCountCallbackProc (AFunction);
end;

procedure TIDBLib.GetProjectFileSizeCallback (AFunction: TGetProjectFileSize);
begin
   if @GetProjectFileSizeCallbackProc <> nil then
      GetProjectFileSizeCallbackProc (AFunction);
end;}
// -- Commented by Cadmensky IDB Version 2.2.9

procedure TIDBLib.SetCheckItemExistingCallback(AFunction: TItemExistsInProject);
begin
  if @SetCheckItemExistingCallbackProc <> nil then
    SetCheckItemExistingCallbackProc(AFunction);
end;

procedure TIDBLib.SetItemToGuidCallback(AFunction: TItemToGuid);
begin
  if @SetItemToGuidCallbackProc <> nil then
    SetItemToGuidCallbackProc(AFunction);
end;

procedure TIDBLib.SetProjectModifiedCallback(AProcedure: TSetProjectModified);
begin
  if @SetSetProjectModifiedCallbackProc <> nil then
    SetSetProjectModifiedCallbackProc(AProcedure);
end;

procedure TIDBLib.SetSetStartProjectGUIDCallback(AProcedure: TSetStartProjectGUID);
begin
  if @SetSetStartProjectGUIDCallbackProc <> nil then
    SetSetStartProjectGUIDCallbackProc(AProcedure);
end;

procedure TIDBLib.SetSetCurrentProjectGUIDCallback(AProcedure: TSetCurrentProjectGUID);
begin
  if @SetSetCurrentProjectGUIDCallbackProc <> nil then
    SetSetCurrentProjectGUIDCallbackProc(AProcedure);
end;

procedure TIDBLib.SetGetStartProjectGUIDCallback(AFunction: TGetStartProjectGUID);
begin
  if @SetGetStartProjectGUIDCallbackProc <> nil then
    SetGetStartProjectGUIDCallbackProc(AFunction);
end;

procedure TIDBLib.SetGetCurrentProjectGUIDCallback(AFunction: TGetCurrentprojectGUID);
begin
  if @SetGetCurrentProjectGUIDCallbackProc <> nil then
    SetGetCurrentProjectGUIDCallbackProc(AFunction);
end;

procedure TIDBLib.SetGetProjectIsNewAndNotSavedCallback(AFunction: TGetProjectIsNewAndNotSaved);
begin
  if @SetGetProjectIsNewAndNotSavedCallbackProc <> nil then
    SetGetProjectIsNewAndNotSavedCallbackProc(AFunction);
end;

procedure TIDBLib.SetGetProjectFullFileNameCallback(AFunction: TGetProjectFullFileName);
begin
  if @SetGetProjectFullFileNameCallbackProc <> nil then
    SetGetProjectFullFileNameCallbackProc(AFunction);
end;

procedure TIDBLib.SetGetLayerNameCallback(AFunction: TGetLayerName);
begin
  if @SetGetLayerNameCallbackProc <> nil then
    SetGetLayerNameCallbackProc(AFunction);
end;

procedure TIDBLib.SetGetLayerIndexCallback(AFunction: TGetLayerIndex);
begin
  if @SetGetLayerIndexCallbackProc <> nil then
    SetGetLayerIndexCallbackProc(AFunction);
end;

procedure TIDBLib.SetGetAllProjectWindowsNumberCallback(AFunction: TGetAllProjectWindowsNumber);
begin
  if @SetGetAllProjectWindowsNumberCallbackProc <> nil then
    SetGetAllProjectWindowsNumberCallbackProc(AFunction);
end;

procedure TIDBLib.SetGetProjectLayerNumberCallback(AFunction: TGetProjectLayerNumber);
begin
  if @SetGetProjectLayerNumberCallbackProc <> nil then
    SetGetProjectLayerNumberCallbackProc(AFunction);
end;

procedure TIDBLib.SetGetLayerPointerByPositionCallback(AFunction: TGetLayerPointerByPosition);
begin
  if @SetGetLayerPointerByPositionCallbackProc <> nil then
    SetGetLayerPointerByPositionCallbackProc(AFunction);
end;

procedure TIDBLib.SetGetLayerPointerByIndexCallback(AFunction: TGetLayerPointerByIndex);
begin
  if @SetGetLayerPointerByIndexCallbackProc <> nil then
    SetGetLayerPointerByIndexCallbackProc(AFunction);
end;

procedure TIDBLib.SetGetActiveLayerCallback(AFunction: TGetActiveLayer);
begin
  if @SetGetActiveLayerCallbackProc <> nil then
    SetGetActiveLayerCallbackProc(AFunction);
end;

procedure TIDBLib.SetLayerIsFixedCallback(AFunction: TLayerIsFixed);
begin
  if @SetLayerIsFixedCallbackProc <> nil then
    SetLayerIsFixedCallbackProc(AFunction);
end;

procedure TIDBLib.SetGetWinGISINIFileNameCallback(AFunction: TGetWinGISINIFileName);
begin
  if @SetGetWinGISINIFileNameCallbackProc <> nil then
    SetGetWinGISINIFileNameCallbackProc(AFunction);
end;

procedure TIDBLib.SetGetDefaultProjectFileNameCallback(AFunction: TGetDefaultProjectFileName);
begin
  if @SetGetDefaultProjectFileNameCallbackProc <> nil then
    SetGetDefaultProjectFileNameCallbackProc(AFunction);
end;

procedure TIDBLib.ProjectIsBeingSaved(AProject: Pointer);
begin
  if @ProjectIsBeingSavedProc <> nil then
    ProjectIsBeingSavedProc(AProject);
end;

procedure TIDBLib.SaveDatabaseAs(AProject: Pointer; sNewProjectName: AnsiString);
begin
  if @SaveDatabaseAsProc <> nil then
    SaveDatabaseAsProc(AProject, PChar(sNewProjectName));
end;

procedure TIDBLib.DeleteProjectAndDatabaseFiles(sProjectFileName: AnsiString);
begin
  if @DeleteProjectAndDatabaseFilesProc <> nil then
    DeleteProjectAndDatabaseFilesProc(PChar(sProjectFileName));
end;

function TIDBLib.GetReadyToReceiveIDsOfObjectsForMonitoring(AProject: Pointer): Boolean;
begin
  if @GetReadyToReceiveIDsOfObjectsForMonitoringFunc <> nil then
    RESULT := GetReadyToReceiveIDsOfObjectsForMonitoringFunc(AProject)
  else
    RESULT := FALSE;
end;

procedure TIDBLib.ReceiveIDOfObjectForMonitoring(AProject, ALayer: Pointer; iObjectID: Integer);
begin
  if @ReceiveIDOfObjectForMonitoringProc <> nil then
    ReceiveIDOfObjectForMonitoringProc(AProject, ALayer, iObjectID);
end;

procedure TIDBLib.SetGeoTextFieldNames(AProject, ALayer: Pointer; sGeoTextIDFieldName, sGeoTextFieldName: AnsiString);
begin
  if @SetGeotextFieldNamesProc <> nil then
    SetGeotextFieldNamesProc(AProject, ALayer, PChar(sGeoTextIDFieldName), PChar(sGeoTextFieldName));
end;

function TIDBLib.GetGeoTextIDFieldName(AProject, ALayer: Pointer): AnsiString;
var
  pcData: PChar;
begin
  if @GetGeoTextIDFieldNameFunc <> nil then
  begin
    pcData := GetGeoTextIDFieldNameFunc(AProject, ALayer);
    RESULT := pcData;
    FreeMem(pcData);
  end
  else
    RESULT := '';
end;

function TIDBLib.GetGeoTextFieldName(AProject, ALayer: Pointer): AnsiString;
var
  pcData: PChar;
begin
  if @GetGeoTextFieldNameFunc <> nil then
  begin
    pcData := GetGeoTextFieldNameFunc(AProject, ALayer);
    RESULT := pcData;
    FreeMem(pcData);
  end
  else
    RESULT := '';
end;

{procedure TIDBLib.SetGeneratePointCallback (AFunction: TGeneratePoint);
begin
   if @SetGeneratePointCallbackProc <> nil then
      SetGeneratePointCallbackProc (AFunction);
end;

procedure TIDBLib.SetGenerateSymbolCallback (AFunction: TGenerateSymbol);
begin
   if @SetGenerateSymbolCallbackProc <> nil then
      SetGenerateSymbolCallbackProc (AFunction);
end;}

procedure TIDBLib.SetAnnotationSettingsDialogExecuteCallback(AFunction: TAnnotationSettingsDialogExecute);
begin
  if @SetAnnotationSettingsDialogExecuteCallbackProc <> nil then
    SetAnnotationSettingsDialogExecuteCallbackProc(AFunction);
end;

procedure TIDBLib.SetMakeAnnotationInProjectCallback(AFunction: TMakeAnnotationInProject);
begin
  if @SetMakeAnnotationInProjectCallbackProc <> nil then
    SetMakeAnnotationInProjectCallbackProc(AFunction);
end;

{brovak}

procedure TIDBLib.SetLayerForInsertCallback(AFunction: TLayerForInsert);
begin
  if @SetLayerForInsertCallbackProc <> nil then
    SetLayerForInsertCallbackProc(AFunction);
end;

procedure TIDBLib.SetChartSettingsDialogExecuteCallback(AFunction: TChartSettingsDialogExecute);
begin
  if @SetChartSettingsDialogExecuteCallbackProc <> nil then
    SetChartSettingsDialogExecuteCallbackProc(AFunction);
end;

procedure TIDBLib.SetMakeChartInProjectCallback(AFunction: TMakeChartInProject);
begin
  if @SetMakeChartInProjectCallbackProc <> nil then
    SetMakeChartInProjectCallbackProc(AFunction);
end;
{brovak}

procedure TIDBLib.SetShowDonePercentCallback(AFunction: TShowDonePercent);
begin
  if @SetShowDonePercentCallbackProc <> nil then
    SetShowDonePercentCallbackProc(AFunction);
end;

// ++ Commented by Cadmensky Version 2.2.8
{procedure TIDBLib.SetGatherGarbageInProjectCallback (AProcedure: TGatherGarbageInProject);
begin
   if @SetGatherGarbageInProjectCallbackProc <> nil then
      SetGatherGarbageInProjectCallbackProc (AProcedure);
end;

procedure TIDBLib.SetGatherSelectedGarbageInProjectCallback (AProcedure: TGatherGarbageInProject);
begin
   if @SetGatherSelectedGarbageInProjectCallbackProc <> nil then
      SetGatherSelectedGarbageInProjectCallbackProc (AProcedure);
end;}
// -- Commented by Cadmensky Version 2.2.8

procedure TIDBLib.SetRedrawProjectWindowCallback(AProcedure: TRedrawProjectWindow);
begin
  if @SetRedrawProjectWindowCallbackProc <> nil then
    SetRedrawProjectWindowCallbackProc(AProcedure);
end;

procedure TIDBLib.SetChangeGeoTextParametersCallback(AProcedure: TChangeGeoTextParameters);
begin
  if @SetChangeGeoTextParametersCallbackProc <> nil then
    SetChangeGeoTextParametersCallbackProc(AProcedure);
end;

// ++ Cadmensky IDB Version 2.2.8

procedure TIDBLib.SetGetLayerItemsCountCallback(AFunction: TGetLayerItemsCount);
begin
  if @SetGetLayerItemsCountCallbackProc <> nil then
    SetGetLayerItemsCountCallbackProc(AFunction);
end;

procedure TIDBLib.SetGetLayerItemIndexByNumberCallback(AFunction: TGetLayerItemIndexByNumber);
begin
  if @SetGetLayerItemIndexByNumberCallbackProc <> nil then
    SetGetLayerItemIndexByNumberCallbackProc(AFunction);
end;

procedure TIDBLib.SetIsObjectSelectedCallback(AFunction: TIsObjectSelected);
begin
  if @SetIsObjectSelectedCallbackProc <> nil then
    SetIsObjectSelectedCallbackProc(AFunction);
end;
// -- Cadmensky IDB Version 2.2.8

procedure TIDBLib.SetRunThematicMapBuilderCallback(AFunction: TRunThematicMapBuilder);
begin
  if @SetRunThematicMapBuilderCallbackProc <> nil then
    SetRunThematicMapBuilderCallbackProc(AFunction);
end;

procedure TIDBLib.SetRunMonitoringCallback(AProcedure: TRunMonitoring);
begin
  if @SetRunMonitoringCallbackProc <> nil then
    SetRunMonitoringCallbackProc(AProcedure);
end;

procedure TIDBLib.SetGetObjectPropsCallback(AFunction: TGetObjectProps);
begin
  if @SetGetObjectPropsCallbackProc <> nil then
    SetGetObjectPropsCallbackProc(AFunction);
end;

{procedure TIDBLib.ClearUndoData (AProject: Pointer);
begin
   if @ClearUndoDataProc <> nil then
      ClearUndoDataProc (AProject);
end;

procedure TIDBLib.GetReadyToReceiveUndoData (AProject: Pointer);
begin
   if @GetReadyToReceiveUndoDataProc <> nil then
      GetReadyToReceiveUndoDataProc (AProject);
end;

function TIDBLib.PutDataInUndo (AProject: Pointer; AData: Pointer; iSize: Integer): Boolean;
begin
   if @PutDataInUndoFunc <> nil then
      RESULT := PutDataInUndoFunc (AProject, AData, iSize)
   else
      RESULT := FALSE;
end;

procedure TIDBLib.SaveUndoData (AProject: Pointer; AnUndoType: TUndoType = utUnknownUNDO; bClearRedoData: Boolean = TRUE);
begin
   if @SaveUndoDataProc <> nil then
      SaveUndoDataProc (AProject, AnUndoType, bClearRedoData);
end;

function TIDBLib.ThereAreDataForRestoring (AProject: Pointer): Boolean;
begin
   if @ThereAreDataForRestoringFunc <> nil then
      RESULT := ThereAreDataForRestoringFunc (AProject)
   else
      RESULT := FALSE;
end;

function TIDBLib.GetNextUndoDescription (AProject: Pointer): AnsiString;
var pcData: PChar;
begin
   if @GetNextUndoDescriptionFunc <> nil then
   begin
      pcData := GetNextUndoDescriptionFunc (AProject);
      RESULT := pcData;
      FreeMem (pcData);
   end
   else
      RESULT := '';
end;

function TIDBLib.GetUndoData (AProject: Pointer; var pBuff: Pointer; var iSize: Integer; var AnUndoType: TUndoType): Boolean; //AnsiString;
begin
   if @GetUndoDataFunc <> nil then
      RESULT := GetUndoDataFunc (AProject, pBuff, iSize, AnUndoType)
   else
      RESULT := FALSE;
end;

procedure TIDBLib.OnChangeUndoStepsCount (AProject: Pointer; iNewUndoStepsCount: integer);
begin
   if @OnChangeUndoStepsCountProc <> nil then
      OnChangeUndoStepsCountProc (AProject, iNewUndoStepsCount);
end;

procedure TIDBLib.AddSymbolMappingEntry (AProject: Pointer; sSymLibName, sSymName, sNewSymName: AnsiString);
begin
   if @AddSymbolMappingEntryProc <> nil then
      AddSymbolMappingEntryProc (AProject, PChar (sSymLibName), PChar (sSymName), PChar (sNewSymName));
end;

function TIDBLib.SymbolNameExists (AProject: Pointer; sSymbolLibraryName, sSymbolName: AnsiString): Boolean;
begin
   if @SymbolNameExistsFunc <> nil then
      RESULT := SymbolNameExistsFunc (AProject, PChar (sSymbolLibraryName), PChar (sSymbolName))
   else
      RESULT := FALSE;
end;

function TIDBLib.GetSymbolActualName (AProject: Pointer; sSymbolLibraryName, sSymbolName: AnsiString): AnsiString;
begin
   if @GetSymbolActualNameFunc <> nil then
      RESULT := GetSymbolActualNameFunc (AProject, PChar (sSymbolLibraryName), PChar (sSymbolName))
   else
      RESULT := '';
end;

procedure TIDBLib.Set_ThereIsNoNecessityToPutDataInUndo_Sign (AProject: Pointer; bValue: Boolean);
begin
   if @Set_ThereIsNoNecessityToPutDataInUndo_SignProc <> nil then
      Set_ThereIsNoNecessityToPutDataInUndo_SignProc (AProject, bValue);
end;}

procedure TIDBLib.Set_AnExternalDBIsWorking_Sign(AProject: Pointer; bValue: Boolean);
begin
  if @Set_AnExternalDBIsWorking_SignProc <> nil then
    Set_AnExternalDBIsWorking_SignProc(AProject, bValue);
end;

function TIDBLib.GetReadyToReceiveIDsOfObjectsToBeRestored(AProject: Pointer): Boolean;
begin
  if @GetReadyToReceiveIDsOfObjectsToBeRestoredFunc <> nil then
    RESULT := GetReadyToReceiveIDsOfObjectsToBeRestoredFunc(AProject)
  else
    RESULT := FALSE;
end;

procedure TIDBLib.ReceiveIDOfObjectToBeRestored(AProject: Pointer; ALayer: Pointer; iObjectID: Integer);
begin
  if @ReceiveIDOfObjectToBeRestoredProc <> nil then
    ReceiveIDOfObjectToBeRestoredProc(AProject, ALayer, iObjectID);
end;

// ++ Cadmensky

procedure TIDBLib.UpdateCalcFields(AProject: Pointer; ALayer: Pointer; iObjectID: Integer);
begin
  if @UpdateCalcFieldsProc <> nil then
    UpdateCalcFieldsProc(AProject, ALayer, iObjectID);
end;

procedure TIDBLib.ShowRecordsForSelectedObjects(AProject: Pointer);
begin
  try
    if @ShowRecordsForSelectedObjectsProc <> nil then
      ShowRecordsForSelectedObjectsProc(AProject);
  except
  end;
end;
// -- Cadmensky

procedure TIDBLib.ExecuteRestoring(AProject: Pointer);
begin
  if @ExecuteRestoringProc <> nil then
    ExecuteRestoringProc(AProject);
end;

procedure TIDBLib.InsertItem(AProject, ALayer: Pointer; iNewItemIndex: Integer);
begin
  if @InsertItemProc <> nil then
    InsertItemProc(AProject, ALayer, iNewItemIndex);
end;

procedure TIDBLib.OverlayInsertItem(AProject, ALayer: Pointer; iNewItemIndex, iAItemIndex, iBItemIndex: Integer);
begin
  if @OverlayInsertItemProc <> nil then
    OverlayInsertItemProc(AProject, ALayer, iNewItemIndex, iAItemIndex, iBItemIndex);
end;

// ++ Cadmensky Version 2.2.8

function TIDBLib.MoveAttributeInfo(AProject, ADestLayer: Pointer): integer;
begin
  if @MoveAttributeInfoProc <> nil then
    Result := MoveAttributeInfoProc(AProject, ADestLayer);
end;
// -- Cadmensky Version 2.2.8

function TIDBLib.CopyAttributeInfo(AProject, ASourceLayer, ADestLayer: Pointer; iSourceItemID, iDestItemID: Integer): integer;
begin
  if @CopyAttributeInfoProc <> nil then
    Result := CopyAttributeInfoProc(AProject, ASourceLayer, ADestLayer, iSourceItemID, iDestItemID);
end;

procedure TIDBLib.Set_DoNotAddAttributeRecords_Sign(AProject: Pointer; bValue: Boolean);
begin
  if @Set_DoNotAddAttributeRecords_SignProc <> nil then
    Set_DoNotAddAttributeRecords_SignProc(AProject, bValue);
end;

// ++ Commented by Cadmensky IDB Version 2.3.0
{function TIDBLib.GetReadyToReceiveIDsOfObjectsToBeUsedForDragAndDrop (AProject: Pointer): Boolean;
begin
   if @GetReadyToReceiveIDsOfObjectsToBeUsedForDragAndDropFunc <> nil then
      RESULT := GetReadyToReceiveIDsOfObjectsToBeUsedForDragAndDropFunc (AProject)
   else
      RESULT := FALSE;
end;

procedure TIDBLib.ReceiveIDOfObjectToBeUsedForDragAndDrop (AProject, ALayer: Pointer; iObjectID: Integer);
begin
   if @ReceiveIDOfObjectToBeUsedForDragAndDropProc <> nil then
      ReceiveIDOfObjectToBeUsedForDragAndDropProc (AProject, ALayer, iObjectID);
end;

procedure TIDBLib.ExecuteDragAndDrop (AProject: Pointer; ASourceGridOfDragging: TObject);
begin
   if @ExecuteDragAndDropProc <> nil then
      ExecuteDragAndDropProc (AProject, ASourceGridOfDragging);
end;}
// -- Commented by Cadmensky IDB Version 2.3.0

function TIDBLib.X_GotoObjectRecord(AProject, ALayer: Pointer; iItemIndex: Integer): Boolean;
begin
  if @X_GotoObjectRecordFunc <> nil then
    RESULT := X_GotoObjectRecordFunc(AProject, ALayer, iItemIndex)
  else
    RESULT := FALSE;
end;

function TIDBLib.X_AddRecord(AProject, ALayer: Pointer; iItemIndex: Integer): Boolean;
begin
  if @X_AddRecordFunc <> nil then
    RESULT := X_AddRecordFunc(AProject, ALayer, iItemIndex)
  else
    RESULT := FALSE;
end;

function TIDBLib.X_GetFieldCount(AProject, ALayer: Pointer): Integer;
begin
  if @X_GetFieldCountFunc <> nil then
    RESULT := X_GetFieldCountFunc(AProject, ALayer)
  else
    RESULT := 0;
end;

function TIDBLib.X_GetFieldName(AProject, ALayer: Pointer; iFieldIndex: Integer): AnsiString;
var
  pcData: PChar;
begin
  if @X_GetFieldNameFunc <> nil then
  begin
    pcData := X_GetFieldNameFunc(AProject, ALayer, iFieldIndex);
    RESULT := pcData;
    FreeMem(pcData);
  end
  else
    RESULT := 'ProgisID';
end;

function TIDBLib.X_GetFieldValueByFieldName(AProject, ALayer: Pointer; sFieldName: AnsiString): Variant;
begin
  if @X_GetFieldValueByFieldNameFunc <> nil then
    RESULT := X_GetFieldValueByFieldNameFunc(AProject, ALayer, PChar(sFieldName))
  else
    RESULT := 0;
end;

function TIDBLib.X_GetFieldValueByFieldIndex(AProject, ALayer: Pointer; iFieldIndex: Integer): Variant;
begin
  if @X_GetFieldValueByFieldIndexFunc <> nil then
    RESULT := X_GetFieldValueByFieldIndexFunc(AProject, ALayer, iFieldIndex)
  else
    RESULT := 0;
end;

procedure TIDBLib.X_SetFieldValueByFieldName(AProject, ALayer: Pointer; sFieldName: AnsiString; FieldValue: Variant);
begin
  if @X_SetFieldValueByFieldNameProc <> nil then
    X_SetFieldValueByFieldNameProc(AProject, ALayer, PChar(sFieldName), FieldValue);
end;

procedure TIDBLib.X_SetFieldValueByFieldIndex(AProject, ALayer: Pointer; iFieldIndex: Integer; FieldValue: Variant);
begin
  if @X_SetFieldValueByFieldIndexProc <> nil then
    X_SetFieldValueByFieldIndexProc(AProject, ALayer, iFieldIndex, FieldValue);
end;

procedure TIDBLib.DeleteLayerTables(AProject, ALayer: Pointer);
begin
  if @DeleteLayerTablesProc <> nil then
    DeleteLayerTablesProc(AProject, ALayer);
end;

function TIDBLib.LoadGeoTextData(AProject, ALayer: Pointer; sSourceDBFileName, sSourceFieldName, sDestinationFieldName, sProgisIDFieldName: AnsiString): Boolean;
begin
  if @LoadGeoTextDataFunc <> nil then
    RESULT := LoadGeoTextDataFunc(AProject, ALayer, PChar(sSourceDBFileName), PChar(sSourceFieldName), PChar(sDestinationFieldName), PChar(sProgisIDFieldName))
  else
    RESULT := FALSE;
end;

function TIDBLib.GetActiveWindowProject: Pointer;
begin
  if @GetActiveWindowProjectFunc <> nil then
    RESULT := GetActiveWindowProjectFunc
  else
    RESULT := nil;
end;

function TIDBLib.GetReadyToReceiveIDsOfObjectsToBeMade(AProject: Pointer): Boolean;
begin
  if @GetReadyToReceiveIDsOfObjectsToBeMadeFunc <> nil then
    RESULT := GetReadyToReceiveIDsOfObjectsToBeMadeFunc(AProject)
  else
    RESULT := FALSE;
end;

procedure TIDBLib.ReceiveIDOfObjectToBeMade(AProject, ALayer: Pointer; iItemID: Integer);
begin
  if @ReceiveIDOfObjectToBeMadeProc <> nil then
    ReceiveIDOfObjectToBeMadeProc(AProject, ALayer, iItemID);
end;

procedure TIDBLib.MakeAnnotations(AProject: Pointer);
begin
  if @MakeAnnotationsProc <> nil then
    MakeAnnotationsProc(AProject);
end;

procedure TIDBLib.UpdateAnnotationData(AProject: Pointer; iLayerIndex, iItemID: Integer; AnAnnotData: PCollection);
begin
  if @UpdateAnnotationDataProc <> nil then
    UpdateAnnotationDataProc(AProject, iLayerIndex, iItemID, AnAnnotData);
end;

{brovak}

procedure TIDBLib.MakeChart(AProject: Pointer);
begin
  if @MakeChartProc <> nil then
    MakeChartProc(AProject);
end;

procedure TIDBLib.UpdateChartData(AProject: Pointer; iLayerIndex, iItemID: Integer; AnChartData: PCollection);
begin
  if @UpdateChartDataProc <> nil then
    UpdateChartDataProc(AProject, iLayerIndex, iItemID, AnChartData);
end;

{brovak}

// ++ Commented by Cadmensky IDB Version 2.2.8
{function TIDBLib.GetReadyToInsertRecords (AProject, ALayer: Pointer; ARecordTemplate: TList): Integer;
begin
   if @GetReadyToInsertRecordsFunc <> nil then
      RESULT := GetReadyToInsertRecordsFunc (AProject, ALayer, ARecordTemplate)
   else
      RESULT := -1;
end;

function TIDBLib.InsertRecord (AProject, ALayer: Pointer; iLayerItemID: Integer): Boolean;
begin
   if @InsertRecordFunc <> nil then
      RESULT := InsertRecordFunc (AProject, ALayer, iLayerItemID)
   else
      RESULT := FALSE;
end;

procedure TIDBLib.EndInserting (AProject, ALayer: Pointer);
begin
   if @EndInsertingProc <> nil then
      EndInsertingProc (AProject, ALayer);
end;}
// ++ Commented by Cadmensky IDB Version 2.2.8

function TIDBLib.TheUserWantsToCancel(AWindow: TForm): Boolean;
begin
  if @TheUserWantsToCancelFunc <> nil then
    RESULT := TheUserWantsToCancelFunc(AWindow)
  else
    RESULT := FALSE;
end;

function TIDBLib.GetReadyToPolygonOverlaying(AProject, ASourceLayer1, ASourceLayer2, ADestinationLayer: Pointer): Boolean;
begin
  if @GetReadyToPolygonOverlayingFunc <> nil then
    RESULT := GetReadyToPolygonOverlayingFunc(AProject, ASourceLayer1, ASourceLayer2, ADestinationLayer)
  else
    RESULT := FALSE;
end;

procedure TIDBLib.EndOfPolygonOverlaying(AProject: Pointer);
begin
  if @EndOfPolygonOverlayingProc <> nil then
    EndOfPolygonOverlayingProc(AProject);
end;

procedure TIDBLib.SetReverseUsingIDBInfoCallback(AProcedure: TReverseUsingOfIDBInfo);
begin
  if @SetReverseUsingIDBInfoCallbackProc <> nil then
    SetReverseUsingIDBInfoCallbackProc(AProcedure);
end;

procedure TIDBLib.SetGetMDIChildWindowStateCallback(AFunction: TGetMDIChildWindowState);
begin
  if @SetGetMDIChildWindowStateCallbackProc <> nil then
    SetGetMDIChildWindowStateCallbackProc(AFunction);
end;

procedure TIDBLib.SetMakeAnnotationUpdateCallback(AProcedure: TMakeAnnotationUpdate);
begin
  if @SetMakeAnnotationUpdateCallbackProc <> nil then
    SetMakeAnnotationUpdateCallbackProc(AProcedure);
end;

{brovak}

procedure TIDBLib.SetMakeChartUpdateCallback(AProcedure: TMakeChartUpdate);
begin
  if @SetMakeChartUpdateCallbackProc <> nil then
    SetMakeChartUpdateCallbackProc(AProcedure);
end;

{brovak}

// ++ Cadmensky

{procedure TIDBLib.SetFillLayerNamesListCallback (AProcedure: TFillLayerNamesList);
begin
   if @SetFilllayerNamesListCallbackProc <> nil then
      SetFillLayerNamesListCallbackProc (AProcedure);
end;}

procedure TIDBLib.SetSetNessesitySaveUndoDataSignCallback(AProcedure: TSetNessesitySaveUndoDataSign);
begin
  if @SetSetNessesitySaveUndoDataSignCallbackProc <> nil then
    SetSetNessesitySaveUndoDataSignCallbackProc(AProcedure);
end;

procedure TIDBLib.SetSaveUndoDataCallback(AProcedure: TSaveUndoData);
begin
  if @SetSaveUndoDataCallbackProc <> nil then
    SetSaveUndoDataCallbackProc(AProcedure);
end;
// -- Cadmensky

end.

