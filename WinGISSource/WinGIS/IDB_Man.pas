unit IDB_Man;
{
Internal database. Ver. II.
Internal database main manager.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 07-06-2000, 12.03.2001

for some operations with attribute data, I have to note that these operation were called by
    External database (WinMON for example). to take into account that the external database is
    working I have a special flag - bExternalDBIsWorking.

About my way to name some variables...
    Sometimes I named some variables so that they are consist of big letter of according object.
    for example, everywhere in this module I named a variable that gives me an access to project
    define data as pPD: ^TrecProjectsDefs and to layer's define as pLD: ^TrecLayerDefs.
    The same of names of form variables - DDF: TIDB_DescriptionDefinitionForm;
}
interface

uses Forms, Dialogs, Classes, Windows, Messages, Controls, SysUtils, Gauges, Graphics, ToolsLib,
  Objects,
  Am_Proj, Am_Layer, Am_View, Am_Index, Am_Group,
  IDB_CallBacksDef, IDB_Consts, MenuFn, lists, DK_DestLayer
{$IFDEF IDBCOM}
  , IDB_TLB
{$ELSE}
  , IDB_WG_Lib_Manager
{$ENDIF}
  ;

type
  TIDB_OldMemoryStream = class(TOldMemoryStream)
//  TIDB_OldMemoryStream = object(TOldMemoryStream)
  public
    procedure SetPointer(APointer: Pointer; Size: LongInt);
    function GetDataStreamPointer: Pointer;
  end;

   // This is the Inaternal Database Manager itself.
  TIDB = class //TInternalDataBase
  private
    AnOldScreenCursor: TCursor;
    procedure SetScreenCursor;
    procedure RestoreScreenCursor;

    procedure ShowMonitoringData(AProject: PProj; bNeedToShowMonitoringWindow, bAlwaysShowMonitoring: Boolean);

    function GetObjectTypeByItemIndex(iIndex: Integer): Integer;
  public
{$IFDEF IDBCOM}
    IDBLib: IIDBLib;
{$ELSE}
    IDBLib: TIDBLib;
{$ENDIF}
    constructor Create;
    destructor Free;
    function LibraryWasLoaded: Boolean;
      /////////////// FROM WINGIS TO IDB
    procedure SetReadOnlyState(bReadOnlyState: Boolean);

    procedure AddProject(AProject: PProj);
    procedure DeleteProject(AProject: PProj);
    procedure ProjectIsSaving(AProject: PProj);

    procedure DeleteProjectAndDatabaseFiles(sProjectFileName: AnsiString);
    procedure SaveDatabaseAs(AProject: PProj; sNewProjectName: AnsiString);

    procedure ShowDataTable;
    procedure ShowDataTableWithSelectBefore(AProject: PProj);
    procedure ShowMonitoringTable(bAlwaysShowMonitoring: Boolean);
    procedure ShowInfoTable;
    procedure HideInfoTable;
    procedure ClearInfoTable(AProject: Pointer);
    procedure CloseInfoTable;
    procedure RefreshDataWindows(AProject: PProj);
    procedure CloseAllWindowsOfAProject(AProject: Pointer);
    procedure CloseDataWindowsOfAllProjects;

//      procedure DeleteLayerTables (AProject: PProj; ALayer: PLayer);

      {brovak}
    function IsOpenedWindow(AProject: Pointer): integer;
      {brovak}

    procedure GetReadyToReceiveDeletedObjectID(AProject: PProj);
    procedure ReceiveDeletedObjectID(AProject: PProj; ALayer: PLayer; AView: PView; AItem: PIndex);
    procedure ExecuteDeleting(AProject: PProj; bShowProgressBarInMainWindow: Boolean = TRUE);

    procedure SetSignOfMultiOperations(AProject: PProj; bValue: Boolean);

    procedure InsertItem(AProject: PProj; ALayer: PLayer; ANewItem: PIndex);
// ++ Cadmensky
    procedure OverlayInsertItem(AProject: PProj; ALayer: PLayer; iNewItemIndex, iAItemIndex, iBItemIndex: integer);
// -- Cadmensky

// ++ Cadmensky Version 2.2.8
    function MoveAttributeInfo(AProject: PProj; ADestLayer: PLayer): Integer;
    function CopyAttributeInfo(AProject: PProj; ASourceLayer, ADestLayer: PLayer; iSourceItemID, iDestItemID: Integer): Integer;
// -- Cadmensky Version 2.2.8

{$IFNDEF IDBCOM}
// ++ Commented by Cadmensky IDB Version 2.2.8
{      function GetReadyToInsertRecords (AProject, ALayer: Pointer; ARecordTemplate: TList): Integer;
      //        function InsertRecord(iPosition, iLayerItemID: Integer): Boolean;
      function InsertRecord (AProject, ALayer: Pointer; iLayerItemID: Integer): Boolean;
      procedure EndInserting (AProject: PProj; ALayer: PLayer);}
// -- Commented by Cadmensky IDB Version 2.2.8
{$ENDIF}

    function GetReadyToPolygonOverlaying(AProject, ASourceLayer1, ASourceLayer2, ADestinationLayer: Pointer): Boolean;
    procedure EndOfPolygonOverlaying(AProject: Pointer);

    procedure SetDBIsWorkingSign(AProject: PProj; bValue: Boolean);
    procedure SetIAmGenerateObjectsMyselfSign(AProject: PProj; bValue: Boolean);

    function GetTableFieldsNamesList(AProject: PProj; ALayer: PLayer): AnsiString;
    function GetTableVisibleFieldsNamesList(AProject: PProj; ALayer: PLayer): AnsiString;

    function GetActiveWindowProject: Pointer;
      // The next operations are purposed for the GeoText creating dialog.
      // They call out (and receive) to (and from) this dialog wich field
      // is a field in an attribute table that contained GeoText information.
    function GetGeoTextIDFieldName(AProject: PProj; ALayer: PLayer): AnsiString;
    function GetGeoTextFieldName(AProject: PProj; ALayer: PLayer): AnsiString;
    procedure SetGeoTextFieldNames(AProject: PProj; ALayer: PLayer; sGeoTextIDFieldName, sGeoTextFieldName: AnsiString);
    function GetGeoText(AProject: PProj; iLayerIndex, iItemID: Integer {; sGeoTextIDFieldName, sGeoTextValueFieldName: AnsiString}): AnsiString;
    procedure SetGeoText(AProject: PProj; iLayerIndex, iItemID: Integer; {sGeoTextIDFieldName, sGeoTextValueFieldName,} sGeoTextValue: AnsiString);

    function LoadGeoTextData(AProject, ALayer: Pointer; sSourceDBFileName, sSourceFieldName, sDestinationFieldName, sProgisIDFieldName: AnsiString): Boolean;

    procedure MakeAnnotations(AProject: PProj);
    procedure UpdateAnnotationData(AProject: Pointer; iLayerIndex, iItemID: Integer; AnAnnotData: PCollection);

      {brovak}

    procedure MakeChart(AProject: PProj);
    procedure UpdateChartData(AProject: Pointer; iLayerIndex, iItemID: Integer; AnChartData: PCollection);
      {brovak}

// ++ Commented by Cadmensky IDB Version 2.3.0
{      procedure GetReadyToReceiveDroppedIDs (AProject: PProj);
      procedure DropDataFromTable (AProject: PProj; ALayer: PLayer; iIDOfItemWhichWasBeenDropped: Integer; ASourceFormOfDragging: TWinControl);
      procedure ExecuteDragAndDrop (AProject: PProj; ASourceGridOfDragging: TObject);}
// -- Commented by Cadmensky IDB Version 2.3.0

      // This is the interface that allows to an external object to get an access to attribute data.
    function X_GotoObjectRecord(AProject: PProj; iLayerIndex: Integer; iItemIndex: Integer): Boolean;
    function X_AddRecord(AProject: PProj; iLayerIndex: Integer; iItemIndex: Integer): Boolean;
    function X_GetFieldCount(AProject: PProj; iLayerIndex: Integer): Integer;
    function X_GetFieldName(AProject: PProj; iLayerIndex: Integer; iFieldIndex: Integer): AnsiString;
    function X_GetFieldValue(AProject: PProj; iLayerIndex: Integer; sFieldName: AnsiString): variant; overload;
    function X_GetFieldValue(AProject: PProj; iLayerIndex: Integer; iFieldIndex: Integer): variant; overload;
    procedure X_SetFieldValue(AProject: PProj; iLayerIndex: Integer; sFieldName: AnsiString; FieldValue: variant); overload;
    procedure X_SetFieldValue(AProject: PProj; iLayerIndex: Integer; iFieldIndex: Integer; FieldValue: variant); overload;
      /////////////// TO WINGIS FROM IDB
    procedure DeleteObjectsInProject(AProject: PProj; ALayer: PLayer);

    function TheUserWantsToCancel(AWindow: TForm): Boolean;
// ++ Cadmensky
    procedure UpdateCalcFields(AProject: PProj; ALayer: PLayer; iItemID: integer);
    procedure ShowRecordsForSelectedObjects(AProject: PProj);

    function GetReadyToReceiveIDsOfObjectToBeDeleted_FromWG(AProject: Pointer): Boolean;
    function GetReadyToReceiveIDsOfObjectsToBeRestored(AProject: Pointer): Boolean;

    procedure ReceiveIDOfObjectToBeRestored(AProject: Pointer; ALayer: Pointer; iObjectID: Integer);
    procedure ExecuteRestoring(AProject: Pointer);
    procedure DeleteObjects_FromWG(AProject: Pointer);
// -- Cadmensky
  end; // TIDB end

function GetObjectProps(AProject, ALayer: Pointer; iItemID: Integer; ADataPropsType: TDataProps): PChar; stdcall;
function MakeLayerForInsert(AProject, ALayer: Pointer): Pointer; stdcall;

implementation

uses IniFiles,
  Am_Main, Am_Child, UserIntf, Am_Def, Am_ProjO, Am_Font, Am_DBGra, Am_Text, Am_Obj,
  ResDlg, Am_Dlg6, AM_Point, Am_Sym, StyleDef, XLines, XFills, Am_Ini,
  NumTools, PropCollect, GRTools, SymLib, SymRol, RollUp, MultiLngFile,
  VerMgr, ToolTips, UserUtils, AM_Poly, AM_CPoly, AM_Circl, Am_Admin, WinOSInfo,
  IDB_SelectInListBox, IDB_Utils, TM_WG_Lib_Manager, DK_AnnotSettings, attachhndl,
// ++ Cadmensky
  BMPImage;
// -- Cadmensky

/////////////////////   Old stream type   //////////////////////////////////

procedure TIDB_OldMemoryStream.SetPointer(APointer: Pointer; Size: LongInt);
begin
  inherited SetPointer(APointer, Size);
end;

function TIDB_OldMemoryStream.GetDataStreamPointer: Pointer;
begin
  RESULT := SELF.Memory;
end;

/////////////////       Interaction (callback) routines       //////////////////

procedure SetProjectModified(AProject: Pointer); stdcall;
begin
  PProj(AProject).Modified := TRUE;
end;

function ItemExistsInProject(AProject: Pointer; ALayer: Pointer; iItemID: Integer): Boolean; stdcall;
begin
  RESULT := FALSE;
  try
    if PProj(AProject).SymbolMode = sym_Project then
      if PProj(AProject).Layers.IndexToLayer(PLayer(ALayer).Index) <> nil then
        if PLayer(ALayer).HasIndexObject(iItemID) <> nil then
          RESULT := TRUE;
  except
  end;
end;

function ItemToGuid(AProject: Pointer; ALayer: Pointer; iItemID: Integer): TGUID; stdcall;
var
  AItem: PIndex;
  AView: PView;
begin
  try
    if (PProj(AProject).SymbolMode = sym_Project) then
    begin
      AItem := New(PIndex, Init(iItemID));
      try
        AView := PLayer(PProj(AProject)^.PInfo^.Objects)^.IndexObject(PProj(AProject)^.PInfo, AItem);
        if AView <> nil then
        begin
          Result := AView^.GUID;
        end
        else
        begin
          ResetGuid(Result);
        end;
      finally
        Dispose(AItem, Done);
      end;
    end;
  except
    ResetGuid(Result);
  end;
end;

{ These 4 procedures set and return current and start values of GUIDs.}

function GetStartProjectGUID(AProject: Pointer): PChar; stdcall;
var
  sGUID: AnsiString;
begin
  RESULT := '';
  PProj(AProject).Registry.OpenKey(cs_ProjectRegistry_IDB_KeyName, TRUE);
  sGUID := PProj(AProject).Registry.ReadString(cs_ProjectRegistry_IDB_StartGUID_ParameterName);
  RESULT := PChar(sGUID);
end;

function GetCurrentProjectGUID(AProject: Pointer): PChar; stdcall;
begin
  RESULT := '';
  PProj(AProject).Registry.OpenKey(cs_ProjectRegistry_IDB_KeyName, TRUE);
  RESULT := PChar(PProj(AProject).Registry.ReadString(cs_ProjectRegistry_IDB_GUID_ParameterName));
end;

procedure SetStartProjectGUID(AProject: Pointer; pcValue: PChar); stdcall;
var
  sGUID: AnsiString;
begin
  PProj(AProject).Registry.OpenKey(cs_ProjectRegistry_IDB_KeyName, TRUE);
  sGUID := pcValue;
  PProj(AProject).Registry.WriteString(cs_ProjectRegistry_IDB_StartGUID_ParameterName, sGUID);
end;

procedure SetCurrentProjectGUID(AProject: Pointer; pcValue: PChar); stdcall;
var
  sGUID: AnsiString;
begin
  PProj(AProject).Registry.OpenKey(cs_ProjectRegistry_IDB_KeyName, TRUE);
  sGUID := pcValue;
  PProj(AProject).Registry.WriteString(cs_ProjectRegistry_IDB_GUID_ParameterName, sGUID);
end;

function GetProjectNewAndNotSavedState(AProject: Pointer): Boolean; stdcall;
begin
  RESULT := FALSE;
   // At first I recognize whether a project is a symbol editor or not.
  if PProj(AProject).SymbolMode <> sym_Project then
    exit;
  RESULT := PProj(AProject).bProjectIsNewAndNoSaved;
end;

function GetProjectFullFileName(AProject: Pointer): PChar; stdcall;
begin
  RESULT := PChar(PProj(AProject).FName);
end;

function GetLayerName(ALayer: Pointer): PChar; stdcall;
var
  sValue: AnsiString;
begin
  sValue := PLayer(ALayer).Text^;
  RESULT := PChar(sValue);
end;

function LayerIsVisible(ALayer: Pointer): Boolean; stdcall;
begin
  RESULT := FALSE;
  try
    RESULT := not PLayer(ALayer).GetState(sf_LayerOff);
  except
    exit;
  end;
end;

function GetActiveLayer(AProject: Pointer): Pointer; stdcall;
begin
// ++ Cadmensky IDB Version 2.3.3
  RESULT := nil;
  if AProject <> nil then
  try
// -- Cadmensky IDB Version 2.3.3
    RESULT := PProj(AProject).Layers.TopLayer;
// ++ Cadmensky IDB Version 2.3.3
  except
  end;
// -- Cadmensky IDB Version 2.3.3
end;

function LayerIsFixed(ALayer: Pointer): Boolean; stdcall;
begin
  RESULT := FALSE;
  try
    RESULT := PLayer(ALayer).GetState(sf_Fixed);
  except
    exit;
  end;
end;

function GetLayerIndex(ALayer: Pointer): Integer; stdcall;
begin
// ++ Cadmensky IDB Version 2.3.3
  RESULT := 0;
  if ALayer <> nil then
  try
// -- Cadmensky IDB Version 2.3.3
    RESULT := PLayer(ALayer).Index;
// ++ Cadmensky IDB Version 2.3.3
  except
  end;
// -- Cadmensky IDB Version 2.3.3
end;

function GetAllProjectWindowsNumber: Integer; stdcall;
var
  iI: Integer;
begin
  RESULT := 0;
  for iI := 0 to WinGISMainForm.MDIChildCount - 1 do
    if WinGISMainForm.MDIChildren[iI] is TMDIChild then
      Inc(RESULT);
end;

function GetProjectLayerNumber(AProject: Pointer): Integer; stdcall;
begin
  RESULT := PProj(AProject).Layers.LData.Count;
end;

function GetLayerPointerByPosition(AProject: Pointer; iLayerPos: Integer): Pointer; stdcall;
begin
  RESULT := PProj(AProject).Layers.LData.At(iLayerPos);
end;

function GetLayerPointerByIndex(AProject: Pointer; iLayerIndex: Integer): Pointer; stdcall;
begin
  RESULT := PProj(AProject).Layers.IndexToLayer(iLayerIndex);
end;

procedure ShowDonePercent(iDonePercent: Integer); stdcall;
begin
  StatusBar.Progress := iDonePercent;
  UserInterface.Update([uiStatusBar]);
  Application.ProcessMessages;
  if iDonePercent > 99 then
    StatusBar.ProgressPanel := FALSE;
end;

{ This procedure inserts a new point in a project using attribute data from a database table. }

function GeneratePoint(AProject: Pointer; ALayer: Pointer; ilXCoord, ilYCoord: LongInt; iExistedProgisID: Integer; var iNewProgisID: Integer): Boolean; stdcall;
var
  Pos: TDPoint;
  NewPoint, AnExistedPoint: PPixel;
  ALayerItem: PIndex;
begin
  RESULT := FALSE;
   // At first I recognize whether a project is a symbol editor or not.
  if PProj(AProject).SymbolMode <> sym_Project then
    exit;
  if ALayer = nil then
    exit;

  iNewProgisID := -1;

  AnExistedPoint := nil;
   // if the Progis ID of existed object is OK, I try to get this object.
  ALayerItem := PLayer(ALayer).HasIndexObject(iExistedProgisID);
  if ALayerItem <> nil then
    AnExistedPoint := ALayerItem.MPtr;
   // It the point exists, I move it according new coordination.
  if AnExistedPoint <> nil then
  begin
    AnExistedPoint.MoveRel(ilXCoord - AnExistedPoint.Position.X, ilYCoord - AnExistedPoint.Position.Y);
    RESULT := TRUE;
  end
      // if the object doesn't exist, I create new point.
  else
  begin
    Pos.Init(ilXCoord, ilYCoord);
    NewPoint := New(PPixel, Init(Pos));
    if PProj(AProject).InsertObjectOnLayer(NewPoint, PLayer(ALayer).Index) then
    begin
      iNewProgisID := NewPoint.Index;
      RESULT := TRUE;
    end
    else
      Dispose(NewPoint);
  end;
end;

{ This procedure inserts a new symbol in a project using attribute data from a database table. }

function GenerateSymbol(AProject: Pointer; ALayer: Pointer; ilXCoord, ilYCoord: LongInt; iSymbolNumber: Integer; dblSymbolSize: Double; iExistedProgisID: Integer; var iNewProgisID: Integer): Boolean; stdcall;
var
  ANewSymbol, AnExistedSymbol: PSymbol;
  AItem: PIndex;
  APos: TDPoint;
  bObjectChanged, bF: Boolean;
  iI: Integer;
begin
  RESULT := FALSE;
   // At first I recognize whether a project is a symbol editor or not.
  if PProj(AProject).SymbolMode <> sym_Project then
    exit;

  iSymbolNumber := iSymbolNumber + 600000000;
   // At first try to find whether the such symbol is used in the project or not.
  bF := FALSE;
  for iI := 0 to PSymbols(PProj(AProject).PInfo.Symbols).Data.GetCount - 1 do
  begin
    AItem := PSymbols(PProj(AProject).PInfo.Symbols).Data.At(iI);
    if AItem.Index = iSymbolNumber then
    begin
      bF := TRUE;
    end;
  end;
   // if the symbol with the such number isn't used in the project, I go out from the procedure and go
   // to the next record.
  if not bF then
    exit;

  iNewProgisID := -1;

   // Check whether the existed ProgisID is correct or not.
   // It the existed ProgisID is correct, try to find it among the layer's objects.
  AnExistedSymbol := nil;

   // try to find the existed item in the project's layer.
  AItem := PLayer(ALayer).HasIndexObject(iExistedProgisID);
   // if this item exists, I give a pointer to the symbol itself.
  if AItem <> nil then
    AnExistedSymbol := AItem.MPtr;
   // if the symbol already exists, I move it.
  if AnExistedSymbol <> nil then
  begin
    AnExistedSymbol.MoveRel(ilXCoord - AnExistedSymbol.Position.X, ilYCoord - AnExistedSymbol.Position.Y);
    RESULT := TRUE;
  end
      // if the symbol doesn't exist, I start to try to create it.
  else
  begin
    APos.Init(ilXCoord, ilYCoord);
    ANewSymbol := New(PSymbol, Init(PProj(AProject).PInfo, APos, iSymbolNumber, 1, 0));
    if (dblSymbolSize > -1) and (DblCompare(dblSymbolSize, dblNoChange) <> 0) then
      NumTools.Update(ANewSymbol.Size, dblSymbolSize, bObjectChanged);
    if PProj(AProject).InsertObject(ANewSymbol, FALSE) then
    begin
      iNewProgisID := ANewSymbol.Index;
      RESULT := TRUE;
    end
    else
      if ANewSymbol <> nil then
        Dispose(ANewSymbol, Done);
  end;
end;

function ItemHasSuitableType(AItem: PIndex): Boolean;
var
  iwObjectType: Word;
begin
   // Now we work with Pixel, Line, Polygon, Symbol, Spline, Image, Circle and Arc.
   // Data about other objects don't stored in the IDB.
  RESULT := FALSE;
  iwObjectType := AItem.GetObjType;
  if ((iwObjectType = ot_Pixel) or (iwObjectType = ot_Poly)
    or (iwObjectType = ot_CPoly) or (iwObjectType = ot_Symbol)
    or (iwObjectType = ot_Spline) or (iwObjectType = ot_Image)
    or (iwObjectType = ot_Circle) or (iwObjectType = ot_Arc))
    or (iwObjectType = ot_Text) then
    RESULT := TRUE;
end;

{$IFNDEF IDBCOM}
{ I check whether a record of an object exists or not. if the record doesn't exist, I'll add it.
  if such record exists but state of the object is marked as 'deleted', I'll marked the state as 'just added'.}

// ++ Commented by Cadmensky Version 2.2.8
{function GatherGarbageInProject (AProject, ALayer: Pointer; AGauge: Integer; AWindow: TForm; ARecordTemplate: TList): Integer; stdcall;
var
   iI, iItemsCount: Integer;
   ALayerItem: PIndex;
   bUserWantsToCancel: Boolean;
begin
   RESULT := 0;
   iItemsCount := PLayer (ALayer).Data.GetCount;
   TGauge (AGauge).Progress := 0;
   TGauge (AGauge).MaxValue := iItemsCount;

   WinGISMainForm.IDB_Man.GetReadyToInsertRecords (AProject, ALayer, ARecordTemplate);
   try
      for iI := 0 to iItemsCount - 1 do
      begin
         ALayerItem := PLayer (ALayer).Data.At (iI);
         if ItemHasSuitableType (ALayerItem) then
            if WinGISMainForm.IDB_Man.InsertRecord (AProject, ALayer, ALayerItem.Index) then
               Inc (RESULT);
         TGauge (AGauge).Progress := TGauge (AGauge).Progress + 1;
         Application.ProcessMessages;
         if WinGISMainForm.IDB_Man.TheUserWantsToCancel (AWindow) then
            BREAK;
      end; // for iI end
   finally
      WinGISMainForm.IDB_Man.EndInserting (AProject, ALayer);
   end;
end;

function GatherSelectedGarbageInProject (AProject, ALayer: Pointer; AGauge: Integer; AWindow: TForm; ARecordTemplate: TList): Integer; stdcall;
var
   iPosition,
      iI, iItemsCount: Integer;
   ASelectedItem,
      ALayerItem: PIndex;
begin
   RESULT := 0;

   iItemsCount := PProj (AProject).Layers.SelLayer.SelInfo.Count;
   TGauge (AGauge).Progress := 0;
   TGauge (AGauge).MaxValue := iItemsCount;

   iPosition := WinGISMainForm.IDB_Man.GetReadyToInsertRecords (AProject, ALayer, ARecordTemplate);
   try
      for iI := 0 to iItemsCount - 1 do
      begin
         ASelectedItem := PProj (AProject).Layers.SelLayer.Data.At (iI);
         ALayerItem := PLayer (ALayer).HasIndexObject (ASelectedItem.Index);

         if (ALayerItem <> nil)
            and
            (ALayerItem.GetState (sf_Selected))
            and
            (ItemHasSuitableType (ALayerItem)) then
            //                  if WinGISMainForm.IDB_Man.InsertRecord(iPosition, ALayerItem.Index) then
            if WinGISMainForm.IDB_Man.InsertRecord (AProject, ALayer, ALayerItem.Index) then
               Inc (RESULT);
         TGauge (AGauge).Progress := TGauge (AGauge).Progress + 1;
         Application.ProcessMessages;
         if WinGISMainForm.IDB_Man.TheUserWantsToCancel (AWindow) then
            BREAK;
      end; // for iI end
   finally
      WinGISMainForm.IDB_Man.EndInserting (AProject, ALayer);
   end;
end;}
// -- Commented by Cadmensky Version 2.2.8

{$ENDIF}

{ Show selected in table items and zoom project if it is required. }

procedure ShowSelectedItemsInProject(AProject, ALayer: Pointer; iItemID: Integer; bSelect, bNeedZoom: Boolean); stdcall;
var
  ALayerItem: PIndex;
  TmpLayer: PLayer;
  i, iI: integer;
begin
   // At first I recognize whether a project is a symbol editor or not.
  if PProj(AProject).SymbolMode <> sym_Project then
    exit;
  if ALayer <> nil then
  begin
    ALayerItem := PLayer(ALayer).HasIndexObject(iItemID);
  end
  else
  begin
    for i := 1 to PProj(AProject)^.Layers^.LData^.Count - 1 do
    begin
      TmpLayer := PProj(AProject)^.Layers^.LData^.At(i);
      if TmpLayer <> nil then
      begin
        ALayerItem := TmpLayer^.HasIndexObject(iItemID);
        if ALayerItem <> nil then
        begin
          PLayer(TmpLayer)^.Select(PProj(AProject)^.PInfo, ALayerItem);
          PProj(AProject)^.Layers^.SetTopLayer(TmpLayer);
          break;
        end;
      end;
    end;
    ALayerItem := nil;
  end;

  if (ALayerItem <> nil) and (ALayer <> nil) then
  begin
// ++ Cadmensky IDB Version 2.2.8
    if bSelect then
// -- Cadmensky IDB Version 2.2.8
      PLayer(ALayer).Select(PProj(AProject).PInfo, ALayerItem)
// ++ Cadmensky IDB Version 2.2.8
    else
      PLayer(ALayer).Deselect(PProj(AProject).PInfo, ALayerItem);
// -- Cadmensky IDB Version 2.2.8
  end;

  if bNeedZoom then
  begin
    ProcShowAllSel(PProj(AProject));
    for iI := 0 to WinGISMainForm.MDIChildCount - 1 do
      if TMDIChild(WinGISMainForm.MDIChildren[iI]).Data = AProject then
        WinGISMainForm.MDIChildren[iI].Show;
  end;
end;

procedure DeselectAllObjects(AProject: Pointer); stdcall;
begin
  PProj(AProject).DeselectAll(true, false);
end;

function GetProjectUndoStepsCount(AProject: Pointer): integer; stdcall;
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  Result := WinGISMainForm.IDBUndoStepsCount[PProj(AProject)]
{$ENDIF}
end;

function GetProjectFileSize(AProject: Pointer): integer; stdcall;
begin
  Result := PProj(AProject).FSize;
end;

procedure ShowItemInProjectWithZoom(AProject, ALayer: Pointer; iItemID: Integer); stdcall;
{ I took this procedure from Am_ProjO.pas - procedure ProcShowAllSel. }
var
  AView: PView;
  AItem: PIndex;
  dZoomFact: Double;
  iFrameSize: Integer;
  ilPosX1, ilPosY1,
    ilPosX2, ilPosY2: LongInt;
  dRealPosX1, dRealPosY1,
    dRealPosX2, dRealPosY2,
    dXDist, dYDist, dNewDist,
    dRealZoom: Double;
  ARect: TDRect;
begin
   // At first I recognize whether a project is a symbol editor or not.
  if PProj(AProject).SymbolMode <> sym_Project then
    exit;

  if PProj(AProject).Pinfo.variousSettings.ZoomSizeType = 0 then
  begin
    dZoomFact := PProj(AProject).PInfo.variousSettings.ZoomSize;
    iFrameSize := 0;
  end
  else
  begin
    dZoomFact := 0;
    iFrameSize := Round(PProj(AProject).PInfo.variousSettings.ZoomSize * 20);
  end;

  AItem := PLayer(ALayer).HasIndexObject(iItemID);
  AView := PLayer(ALayer).IndexObject(PProj(AProject).PInfo, AItem);

  ilPosX1 := AView.ClipRect.A.X;
  ilPosY1 := AView.ClipRect.A.Y;
  ilPosX2 := AView.ClipRect.B.X;
  ilPosY2 := AView.ClipRect.B.Y;

  dRealPosX1 := ilPosX1 - (ilPosX2 - ilPosX1) * dZoomFact;
  dRealPosY1 := ilPosY1 - (ilPosY2 - ilPosY1) / 1.17 * dZoomFact;

  dRealPosX2 := ilPosX2 + (ilPosX2 - ilPosX1) * dZoomFact;
  dRealPosY2 := ilPosY2 + (ilPosY2 - ilPosY1) / 1.17 * dZoomFact;

  dXDist := dRealPosX2 - dRealPosX1;
  dYDist := dRealPosY2 - dRealPosY1;

  if dXDist < dYDist * 1.33 then
  begin
    dNewDist := dYDist * 1.33;
    dRealPosX1 := dRealPosX1 - (dNewDist - dXDist) / 2;
    dRealPosX2 := dRealPosX2 + (dNewDist - dXDist) / 2;
  end
  else
    if dXDist > dYDist * 1.33 then
    begin
      dNewDist := dXDist / 1.33;
      dRealPosY1 := dRealPosY1 - (dNewDist - dYDist) / 2;
      dRealPosY2 := dRealPosY2 + (dNewDist - dYDist) / 2;
    end;

  if dRealPosX1 >= -MaxLongInt then
    ARect.A.X := Round(dRealPosX1)
  else
    ARect.A.X := -MaxLongInt;

  if dRealPosY1 >= -MaxLongInt then
    ARect.A.Y := Round(dRealPosY1)
  else
    ARect.A.Y := -MaxLongInt;

  if dRealPosX2 <= MaxLongInt then
    ARect.B.X := Round(dRealPosX2)
  else
    ARect.B.X := MaxLongInt;

  if dRealPosY2 <= MaxLongInt then
    ARect.B.Y := Round(dRealPosY2)
  else
    ARect.B.Y := MaxLongInt;

  dRealZoom := PProj(AProject).CalculateZoom(RotRect(ARect.A.X, ARect.A.Y, ARect.XSize, ARect.YSize, 0), iFrameSize, TRUE);
  if dRealZoom > IniFile.MaxZoom then
  begin
    dXDist := Arect.B.X - ARect.A.X;
    dYDist := ARect.B.Y - ARect.A.Y;
    dNewDist := Round(dXDist * dRealZoom / IniFile.MaxZoom);
    ARect.A.X := ARect.A.X - Round((dNewDist - dXDist) / 2);
    ARect.B.X := ARect.B.X + Round((dNewDist - dXDist) / 2);
    dNewDist := Round(dYDist * dRealZoom / IniFile.MaxZoom);
    ARect.A.Y := ARect.A.Y - Round((dNewDist - dYDist) / 2);
    ARect.B.Y := ARect.B.Y + Round((dNewDist - dYDist) / 2);
  end;

  PProj(AProject).ZoomByRect(RotRect(ARect.A.X, ARect.A.Y, ARect.XSize, ARect.YSize, 0), iFrameSize, TRUE);
end;

{ Show project window for which data table are showed. }

procedure ShowProject(AProject: Pointer); stdcall;
var
  iI: Integer;
  AForm: TForm;
begin
   // At first I recognize whether a project is a symbol editor or not.
  if PProj(AProject).SymbolMode <> sym_Project then
    exit;
  for iI := 0 to WinGisMainForm.MDIChildCount - 1 do
  begin
    AForm := WinGisMainForm.MDIChildren[iI];
    if (AForm is TMDIChild) and (TMDIChild(AForm).Data = AProject) then
    begin
      AForm.Show;
      exit;
    end;
  end; // for iI end
end;

procedure RedrawProjectWindow(AProject: Pointer); stdcall;
begin
  PProj(AProject).PInfo.RedrawScreen(TRUE);
end;

procedure MakeSpecialChart(AProject, ALayer: Pointer); stdcall;
{$IFDEF IDBCOM}
var
  DestLayer: Pointer;
{$ENDIF}
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFDEF IDBCOM}
  DestLayer := MakeLayerForInsert(AProject, ALayer);
  TMDIChild(PProj(AProject).Parent).DBGraphic.DBGraphic(AProject, true, DestLayer);
{$ELSE}
  TMDIChild(PProj(AProject).Parent).DBGraphic.DBGraphic(AProject, true, ALayer);
{$ENDIF}
// ++ UndoRedo
  if WinGISMainForm.WeAreUsingUndoFunction[AProject] then
    WinGISMainForm.UndoRedo_Man.SaveUndoData(AProject, utUnInsert);
// -- UndoRedo
{$ENDIF}
end;

procedure SendSpecialforChart(AProject: Pointer; PseudoDDEString: Ansistring); stdcall;
var
  TmpStroka: AnsiString;
begin;
{$IFNDEF AXDLL} // <----------------- AXDLL
  TmpStroka := '[DDD][]' + PseudoDDEString;
  TMDIChild(PProj(AProject).Parent).DBGraphic.InsertString(PChar(TmpStroka));
{$ENDIF}
end;

procedure ShowNewThematicMapDialog(AProject: Pointer; sConnection, sSQLText, sProgisIDFieldName, sThematicFieldName: AnsiString); stdcall;
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
// ++ Cadmensky IDB Version 2.2.9
  if WinGisMainForm.WeAreUsingTheIDB[AProject] then
    WinGisMainForm.IDB_Man.SetIAmGenerateObjectsMyselfSign(AProject, true);

  if WinGISMainForm.WeAreUsingUndoFunction[AProject] then
    WinGISMainForm.UndoRedo_Man.SetNecessitySaveUndoDataSign(AProject, false);
// -- Cadmensky IDB Version 2.2.9

  TMDIChild(PProj(AProject).Parent).ProcCreateThematicMap(sConnection,
    sSQLText,
    sProgisIDFieldName,
    sThematicFieldName,
    -1,
    -1,
    '',
    '',
    '',
    True);

// ++ Cadmensky IDB Version 2.2.9
  if WinGisMainForm.WeAreUsingTheIDB[AProject] then
    WinGisMainForm.IDB_Man.SetIAmGenerateObjectsMyselfSign(AProject, false);

  if WinGISMainForm.WeAreUsingUndoFunction[AProject] then
  begin
    WinGISMainForm.UndoRedo_Man.SetNecessitySaveUndoDataSign(AProject, true);
    WinGISMainForm.UndoRedo_Man.SaveUndoData(AProject, utUnChange);
  end;
// -- Cadmensky IDB Version 2.2.9
  PProj(AProject).PInfo.RedrawScreen(TRUE);
{$ENDIF}
end;
//brovak

procedure ShowPercentAtWinGIS(Flag, IDMessage, IDCurrent: integer); stdcall;
begin;

  case Flag of
    0:
      begin;
        StatusBar.ProgressPanel := TRUE;
        StatusBar.ProgressText := GetLangText(IDMessage);
        StatusBar.Progress := 0;
        UserInterface.Update([uiStatusBar]);
      end;
      //prepare progressbar
    1:
      begin;
        StatusBar.ProgressText := GetLangText(IDMessage);
        StatusBar.Progress := IDCurrent;
        UserInterface.Update([uiStatusBar]);
      end;

    2:
      begin;
        StatusBar.ProgressPanel := FALSE;
      end;
      //close

  end;
end;

procedure SendToTMSelectedID(AProject: Pointer; AProgisIDList: TIntList); stdcall;
var
  i: integer;
begin;
  TMDIChild(PProj(AProject).Parent).ProgisIdTMList.Clear;
  for i := 0 to AProgisIDList.Count - 1 do
    TMDIChild(PProj(AProject).Parent).ProgisIdTMList.Add(AProgisIDList.Items[i]);
end;

{brovak}

function SetObjectStyle(AProject, ALayer: Pointer; iItemID, iLineColor, iLineStyle, iLineWidth, iPatternColor, iPatternStyle, iTransparency: Integer; iObjectSize: Integer): Boolean; stdcall;
const
  ci_WindowsLStyles = 5;
  ci_WindowsHStyles = 8;
var
  AItem: PIndex;
  bObjectWasChanged, bObjectStyleWasCreated: Boolean;
  ALineStyle: TLineProperties;
  AFillStyle: TFillProperties;
  bStyleWasFound: Boolean;
  iI: Integer;
  LineStyle: TXLineStyle;
  FillStyle: TCustomXFill;
  listLineStyles: TXLineStyleList;
  listFillStyles: TXFillStyleList;
begin
  RESULT := FALSE;
  AItem := PLayer(ALayer).HasIndexObject(iItemID);
  if AItem = nil then
    exit;
  bObjectWasChanged := FALSE;
   // if we are using undo function, I have to put data about object to be changed in Undo data.
   // Call of this function was made from TIDB_ViewForm.Thematicmap1Click. After all required changes
   // will be done, in that procedure all undo data will be saved.
{$IFNDEF AXDLL} // <----------------- AXDLL
  if WinGisMainForm.WeAreUsingUndoFunction[AProject] then
    WinGISMainForm.UndoRedo_Man.AddObjectInUndoData(AProject, PLayer(ALayer), AItem);
{$ENDIF}

  bObjectStyleWasCreated := FALSE;
   // Get object style. Create it if it is necessary.
  if AItem.ObjectStyle = nil then
  begin
    AItem.SetState(sf_OtherStyle, TRUE);
    AItem.ObjectStyle := New(PObjStyleV2);
    AItem.ObjectStyle.LineStyle := DefaultLineStyle;
    AItem.ObjectStyle.FillStyle := DefaultFillStyle;
    AItem.ObjectStyle.SymbolFill := DefaultSymbolFill;
    bObjectStyleWasCreated := TRUE;
  end
  else
  begin
    UpdateUseCounts(PProj(AProject).PInfo, AItem.ObjectStyle.LineStyle, AItem.ObjectStyle.FillStyle, AItem.ObjectStyle.SymbolFill, FALSE);
  end;
  if AItem.HasLineStyle then
  begin
    ALineStyle := TLineProperties.Create;
      // LINE STYLE settings.
    if bObjectStyleWasCreated then
    begin
      ALineStyle.FillColor := PLayer(ALayer).LineStyle.FillColor;
      ALineStyle.Scale := PLayer(ALayer).LineStyle.Scale;
      ALineStyle.WidthType := PLayer(ALayer).LineStyle.WidthType;
    end
    else
    begin
      ALineStyle.FillColor := AItem.ObjectStyle.LineStyle.FillColor;
      ALineStyle.Scale := AItem.ObjectStyle.LineStyle.Scale;
      ALineStyle.WidthType := AItem.ObjectStyle.LineStyle.WidthType;
    end;
      // LINE COLOR.
    if iLineColor = ci_DoNotChange then
    begin
         // if the object style was created then get line color from the layer settings
         // otherwise get line color from the item itself.
      if bObjectStyleWasCreated then
        ALineStyle.Color := PLayer(ALayer).LineStyle.Color
      else
        ALineStyle.Color := AItem.ObjectStyle.LineStyle.Color;
    end
    else
         // Get line color from the layer settings.
      if iLineColor = ci_UseLayerSettings then
        ALineStyle.Color := PLayer(ALayer).LineStyle.Color
               // Set line color to the received value.
      else
      begin
        if iLineColor < 0 then
          iLineColor := 0;
        ALineStyle.Color := iLineColor;
      end;
      // LINE TYPE.
    if iLineStyle = ci_DoNotChange then
    begin
         // if object style was created then get line style from the layer settings
         // otherwise I get line style from the item itself.
      if bObjectStyleWasCreated then
        ALineStyle.Style := PLayer(ALayer).LineStyle.Style
      else
        ALineStyle.Style := AItem.ObjectStyle.LineStyle.Style;
    end
    else
         // Get line style from the layer settings.
      if iLineStyle = ci_UseLayerSettings then
        ALineStyle.Style := PLayer(ALayer).LineStyle.Style
               // Set line style to the received value.
      else
      begin
        if iLineStyle < 0 then
          iLineStyle := 0;
        if iLineStyle < ci_WindowsLStyles then
          ALineStyle.Style := iLineStyle
        else
        begin
          if bObjectStyleWasCreated then
            ALineStyle.Style := PLayer(ALayer).LineStyle.Style
          else
            ALineStyle.Style := AItem.ObjectStyle.LineStyle.Style;
          bStyleWasFound := FALSE;
          iI := 0;
          listLineStyles := PProj(AProject).PInfo.ProjStyles.XLineStyles;
          repeat
            LineStyle := listLineStyles.Items[iI];
            if LineStyle <> nil then
              if iLineStyle = LineStyle.Number then
              begin
                ALineStyle.Style := iLineStyle;
                bStyleWasFound := TRUE;
              end;
            Inc(iI);
          until (bStyleWasFound) or (iI >= listLineStyles.Capacity);
        end;
      end;
      // LINE WIDTH.
    if iLineWidth = ci_DoNotChange then
    begin
         // if object style was created then get line width from the layer settings
         // otherwise I get line width from the item itself.
      if bObjectStyleWasCreated then
        ALineStyle.Width := PLayer(ALayer).LineStyle.Width
      else
        ALineStyle.Width := AItem.ObjectStyle.LineStyle.Width;
    end
    else
         // Get line width from the layer settings.
      if iLineWidth = ci_UseLayerSettings then
        ALineStyle.Width := PLayer(ALayer).LineStyle.Width
      else
      begin
            // Set line width to the received value.
        if iLineWidth < 0 then
          iLineWidth := 0;
        ALineStyle.Width := iLineWidth;
      end;
  end; // if AItem.HasLineStyle then
  if AItem.HasPattern then
  begin
    AFillStyle := TFillProperties.Create;
    if bObjectStyleWasCreated then
    begin
      AFillStyle.Scale := PLayer(ALayer).FillStyle.Scale;
      AFillStyle.ScaleType := PLayer(ALayer).FillStyle.ScaleType;
    end
    else
    begin
      AFillStyle.Scale := AItem.ObjectStyle.FillStyle.Scale;
      AFillStyle.ScaleType := AItem.ObjectStyle.FillStyle.ScaleType;
    end;
      // OBJECT FORECOLOR.
    if iPatternColor = ci_DoNotChange then
    begin
         // if object style was created, use the layer settings, otherwise use the object settings.
      if bObjectStyleWasCreated then
        AFillStyle.ForeColor := PLayer(ALayer).FillStyle.ForeColor
      else
        AFillStyle.ForeColor := AItem.ObjectStyle.FillStyle.ForeColor;
    end
    else
    begin
         // Get color value form the layer settings.
      if iPatternColor = ci_UseLayerSettings then
        AFillStyle.ForeColor := PLayer(ALayer).FillStyle.ForeColor
      else
      begin
            // Set color value to the received value.
        if iPatternColor < 0 then
          iPatternColor := 0;
        AFillStyle.ForeColor := iPatternColor;
      end;
    end;
      // OBJECT PATTERN STYLE.
    if iPatternStyle = ci_DoNotChange then
    begin
         // if object style was created, use the layer settings, otherwise get settings from the object itself.
      if bObjectStyleWasCreated then
        AFillStyle.Pattern := PLayer(ALayer).FillStyle.Pattern
      else
        AFillStyle.Pattern := AItem.ObjectStyle.FillStyle.Pattern;
    end
    else
    begin
         // Get layer settings.
      if iPatternStyle = ci_UseLayerSettings then
        AFillStyle.Pattern := PLayer(ALayer).FillStyle.Pattern
      else
      begin
            // Set pattern style to the received value.
        if iPatternStyle < 0 then
          iPatternStyle := 0;
        if iPatternStyle < ci_WindowsHStyles then
          AFillStyle.Pattern := iPatternStyle
        else
        begin
          if bObjectStyleWasCreated then
            AFillStyle.Pattern := PLayer(ALayer).FillStyle.Pattern
          else
            AFillStyle.Pattern := AItem.ObjectStyle.FillStyle.Pattern;
          bStyleWasFound := FALSE;
          iI := 0;
          listFillStyles := PProj(AProject).PInfo.ProjStyles.XFillStyles;
          repeat
            FillStyle := listFillStyles.Items[iI];
            if FillStyle <> nil then
              if iPatternStyle = FillStyle.Number then
              begin
                AFillStyle.Pattern := iPatternStyle;
                bStyleWasFound := TRUE;
              end;
            Inc(iI);
          until (bStyleWasFound) or (iI >= listFillStyles.Capacity);
        end;
      end;
    end;
      // OBJECT TRANSPARENCY = OBJECT BACKCOLOR
    if iTransparency = ci_DoNotChange then
    begin
         // if the object style was created, use the layer settings, otherwise use the settings from the object itself.
      if bObjectStyleWasCreated then
        AFillStyle.BackColor := PLayer(ALayer).FillStyle.BackColor
      else
        AFillStyle.BackColor := AItem.ObjectStyle.FillStyle.BackColor;
    end
    else
    begin
         // Use the layer settings.
      if iTransparency = ci_UseLayerSettings then
        AFillStyle.BackColor := PLayer(ALayer).FillStyle.BackColor
      else
      begin
            // Set the settings to the received value.
            // 0 - not transparent.
            // 1 - transparent.
        if iTransparency < 0 then
          iTransparency := 0
        else
          if iTransparency > 1 then
            iTransparency := 1;
        if iTransparency = 0 then
        begin
          if PLayer(ALayer).FillStyle.BackColor >= 0 then
            AFillStyle.BackColor := PLayer(ALayer).FillStyle.BackColor
          else
            AFillStyle.BackColor := clWhite;
        end
        else
        begin
          AFillStyle.BackColor := intNone;
        end;
      end;
    end;
  end; // if AItem.HasPattern then
   // And now we do it!!!!!
  if (AItem.HasLineStyle) and (UpdateLineStyle(@(AItem.ObjectStyle.LineStyle), ALineStyle)) then
    bObjectWasChanged := TRUE;
  if (AItem.HasPattern) and (UpdateFillStyle(@(AItem.ObjectStyle.FillStyle), AFillStyle)) then
    bObjectWasChanged := TRUE;
  if (iObjectSize <> ci_DoNotChange) and (AItem.MPtr <> nil) and (PIndex(AItem.MPtr).GetObjType = ot_Symbol) then
  begin
    PSymbol(AItem.MPtr).Size := iObjectSize;
    PSymbol(AItem.MPtr).CalculateClipRect(PProj(AProject).PInfo);
  end;
  if bObjectWasChanged then
    RESULT := TRUE;
end;

function AnnotationSettingsDialogExecute(AProject, ACurrentLayer: Pointer; AnAnnotData: PCollection): Boolean; stdcall;
{$IFNDEF AXDLL} // <----------------- AXDLL
var
   (*
      bNP: Boolean;
     *)
  ADestLayer: PLayer;
  AForm: TDK_AnnotSettingsForm;
  AnOldFont: TFontData;
{$ENDIF}
begin
  RESULT := FALSE;
{$IFNDEF AXDLL} // <----------------- AXDLL
  ADestLayer := PProj(AProject).Layers.IndexToLayer(PProj(AProject).PInfo.AnnotSettings.AnnotLayer);
  if (ADestLayer = nil) or (ADestLayer.SortIndex = 0) then
  begin
    PProj(AProject).PInfo.AnnotSettings.AnnotLayer := PLayer(ACurrentLayer).Index;
    ADestLayer := ACurrentLayer;
  end;
   (*
        bNP := FALSE; // The necessity of recalculate new position.
        if ExecDialog(TDSelFontLayer.Init(PProj(AProject).Parent, PProj(AProject).Layers, @ADestLayer, AnAnnotData, PProj(AProject).PInfo, @PProj(AProject).PInfo.AnnotSettings.AnnotFont, @bNP, 'SelFontLayer', TRUE)) <> id_OK then exit;
        PProj(AProject).PInfo.AnnotSettings.AnnotLayer := ADestLayer.Index;
        RESULT := TRUE;
   *)
  AnOldFont.Init;
  AnOldFont := PProj(AProject).ActualFont;
  try
    PProj(AProject).ActualFont := PProj(AProject).PInfo.AnnotSettings.AnnotFont;

    AForm := TDK_AnnotSettingsForm.Create(Application, AProject, ADestLayer, AnAnnotData, ACurrentLayer);
    AForm.MakeWindowAsCreateDialog;
    RESULT := AForm.ShowModal = mrOK;
    if RESULT then
    begin
      PProj(AProject).PInfo.AnnotSettings.AnnotLayer := AForm.GetDestinationLayer.Index;
      PProj(AProject).PInfo.AnnotSettings.AnnotFont := PProj(AProject).ActualFont;
      PProj(AProject).PInfo.AnnotSettings.KeepLinkToDBAlive := AForm.KeepLinkToDBAlive;
    end;
  finally
    PProj(AProject).ActualFont := AnOldFont;
  end;
   {brovak}
  IniFile.ReadAdvAnnotation;
   {brovak}
{$ENDIF}
end;

(*function ChartSettingsDialogExecute(AProject, ACurrentLayer: Pointer; AnChartData: PCollection): Boolean; stdcall;
var
{
   bNP: Boolean;
}
   ADestLayer: PLayer;
   AForm: TDK_ChartSettingsForm;
   AnOldFont: TFontData;
begin
     RESULT := FALSE;
     ADestLayer := PProj(AProject).Layers.IndexToLayer(PProj(AProject).PInfo.  AnnotSettings.AnnotLayer);
     if (ADestLayer = nil) or (ADestLayer.SortIndex = 0) then begin
        PProj(AProject).PInfo.AnnotSettings.AnnotLayer := PLayer(ACurrentLayer).Index;
        ADestLayer := ACurrentLayer;
        end;
{
     bNP := FALSE; // The necessity of recalculate new position.
     if ExecDialog(TDSelFontLayer.Init(PProj(AProject).Parent, PProj(AProject).Layers, @ADestLayer, AnAnnotData, PProj(AProject).PInfo, @PProj(AProject).PInfo.AnnotSettings.AnnotFont, @bNP, 'SelFontLayer', TRUE)) <> id_OK then exit;
     PProj(AProject).PInfo.AnnotSettings.AnnotLayer := ADestLayer.Index;
     RESULT := TRUE;
}
     AnOldFont.Init;
     AnOldFont := PProj(AProject).ActualFont;
     try
        PProj(AProject).ActualFont := PProj(AProject).PInfo.AnnotSettings.AnnotFont;

        AForm := TDK_AnnotSettingsForm.Create(Application, AProject, ADestLayer, AnAnnotData);
        AForm.MakeWindowAsCreateDialog;
        RESULT := AForm.ShowModal = mrOK;
        if RESULT then begin
           PProj(AProject).PInfo.AnnotSettings.AnnotLayer := AForm.GetDestinationLayer.Index;
           PProj(AProject).PInfo.AnnotSettings.AnnotFont := PProj(AProject).ActualFont;
           PProj(AProject).PInfo.AnnotSettings.KeepLinkToDBAlive := AForm.KeepLinkToDBAlive;
           end;
     finally
        PProj(AProject).ActualFont := AnOldFont;
     end;
end;
  *)

function MakeLayerForInsert(AProject, ALayer: Pointer): Pointer; stdcall;
var
  AForm: TSetDestForm;
  i: integer;
begin;
  Result := ALayer;
  AForm := TSetDestForm.Create(Application);
  AForm.CurrentLayer := ALayer;
  AForm.CurrentProject := AProject;
  try
    if AForm.ShowModal = mrOk then
      Result := AForm.CurrentLayer;
  finally
    AForm.Free;
  end;
end;

//functon hardly modified by Brovak. Now in test mode. Your comments please write to brovak@progis.ru

// ++ Cadmensky IDB Version 2.3.6

function MakeAnnotationInProject(AProject, AnObjectLayer: Pointer; iItemID: Integer; sAnnotData: PChar): Boolean; stdcall;
//function MakeAnnotationInProject (AProject, AnObjectLayer: Pointer; iItemID: Integer; AnAnnotData: PCollection): Boolean; stdcall;
// -- Cadmensky IDB Version 2.3.6
var
  ilTextHeight, ilWidth, ilHeight, ilPosX, ilPosY: LongInt;
  ATextRect: TRect;
  AATextPos: TDPoint;
  ATextPos: TDRect;
  ATextFont, AOldFont: HFont;
  AFontDes: PFontDes;
  AItem, Bitem, CItem: PIndex;
  AObj: PView;
  AText: PText;
  iI: Integer;
  Annotation: PAnnot;
// ++ Commented by Cadmensky IDB Version 2.3.6
//   sAnnotationText: AnsiString;
// -- Commented by Cadmensky IDB Version 2.3.6
  ADestLayer: PLayer; // Cadmensky
  TmpAngle: integer;
  k: integer;
  Tmplayer: PLayer;
  AView, BView: PView;
  AdvOffset: integer;
  TmpPOint: TGrpoint;
  Existed: boolean;
  Attached: PCollection;
  Rect: TDRECT;

  function Convert(i: integer): integer;
  begin;
    case I of
      1: Result := 0;
      2: Result := 3;
      3: Result := 6;
      4: Result := 1;
      5: Result := 4;
      6: Result := 7;
      7: Result := 2;
      8: Result := 5;
      9: Result := 8;
    end;
  end;
begin
  RESULT := FALSE;
  Existed := false;
// ++ Commented by Cadmensky IDB Version 2.3.6
//   sAnnotationText := '';
// -- Commented by Cadmensky IDB Version 2.3.6
   // ++ Cadmensky
  ADestLayer := PProj(AProject).Layers.IndexToLayer(PProj(AProject).PInfo.AnnotSettings.AnnotLayer);
   // -- Cadmensky
  TmpLayer := PProj(AProject).Layers.TopLayer;
  PProj(AProject).Layers.TopLayer := ADestLayer;
        // Preparing operations.
// ++ Commented by Cadmensky IDB Version 2.3.5
//   PProj (AProject).DeSelectAll (TRUE);
// -- Commented by Cadmensky IDB Version 2.3.5
  ilTextHeight := PProj(AProject).PInfo.AnnotSettings.AnnotFont.Height;

  PProj(AProject).RedrawAfterInsert := FALSE;

// ++ Commented by Cadmensky IDB Version 2.3.6
{   for iI := 0 to AnAnnotData.Count - 1 do
   begin
      Annotation := AnAnnotData.At (iI);
      if Annotation.HasValue then sAnnotationText := sAnnotationText + Annotation.LineValue + #13;
   end;
   sAnnotationText := Copy (sAnnotationText, 1, Length (sAnnotationText) - 1);}
// -- Commented by Cadmensky IDB Version 2.3.6

   {brovak}
   // We need to check - it is new annotation or old changed.
  BItem := TmpLayer.HasIndexObject(iItemID);
  if not Assigned(Bitem) then
  begin;
    for k := 1 to PProj(AProject).Layers.LData.Count - 1 do
    begin
      TmpLayer := PProj(AProject).Layers.LData.At(k);
      BItem := TmpLayer.HasIndexObject(iItemID);
      if Assigned(Bitem) then
        Break
    end;
  end;
   //check master object - have it annotations already or not
    //  ObjectColletion:=New(PCollection, Init(5, 5));
  if not Assigned(BItem) then
    Exit;

  AView := PView(PLayer(PProj(AProject).PInfo^.Objects)^.IndexObject(PProj(AProject).PInfo, BItem));

  if AView = nil then
    Exit;
  if AView.GetAttachedObjs <> nil then
  begin;
    Attached := AView.GetAttachedObjs;
    if Attached.Count > 0 then
    begin;
      for ii := 0 to Attached.Count - 1 do
      begin;
        CItem := Attached.At(ii);
        if CItem.GetObjType = Ot_text then
          if PText(CItem).IsItAnnot = true then
          begin;
             //At this version only one annotation we can have for object
            Rect.InitByRect(PText(CItem).ClipRect);
            Existed := true;
            break;
          end;
      end; //for

    end; //PView
  end;

   // The main work.
  AFontDes := PProj(AProject).PInfo.Fonts.GetFont(PProj(AProject).PInfo.AnnotSettings.AnnotFont.Font);
  ATextFont := PProj(AProject).PInfo.GetFont(PProj(AProject).PInfo.AnnotSettings.AnnotFont, PProj(AProject).PInfo.CalculateDisp(ilTextHeight), 0);

  if ATextFont > 0 then
  begin
    ATextRect.Left := 0;
    ATextRect.Top := 0;

    AItem := PLayer(AnObjectLayer).HasIndexObject(iItemID);
    AObj := PLayer(PProj(AProject).PInfo.Objects).IndexObject(PProj(AProject).PInfo, AItem);
    if AObj <> nil then
    begin
      AOldFont := SelectObject(PProj(AProject).PInfo.ExtCanvas.Handle, ATextFont);

         // The following command does not draws text but measures only it's length.
// ++ Cadmensky IDB Version 2.3.6
      DrawText(PProj(AProject).PInfo.ExtCanvas.Handle, PChar(sAnnotData), Length(sAnnotData), ATextRect, DT_CalcRect);
//         DrawText (PProj (AProject).PInfo.ExtCanvas.Handle, PChar (sAnnotationText), Length (sAnnotationText), ATextRect, DT_CalcRect);
// -- Cadmensky IDB Version 2.3.6
      if (AFontDes <> nil) and (AFontDes.IsVertical) then
      begin
        ilHeight := PProj(AProject).PInfo.CalculateDraw(ATextRect.Right);
        ilWidth := PProj(AProject).PInfo.CalculateDraw(Abs(ATextRect.Bottom));
      end
      else
      begin
        ilWidth := PProj(AProject).PInfo.CalculateDraw(ATextRect.Right);
        ilHeight := PProj(AProject).PInfo.CalculateDraw(Abs(ATextRect.Bottom));
      end;
         {brovak}
      TmpAngle := CalculateTextPos1(AItem, AObj, ilPosX, ilPosY, ilWidth, ilHeight, ilTextHeight);
         {brovak}
     //    AATextPos.Init(0,0);
      //   ATextPos.Assign (ilPosX, ilPosY, ilPosX - ilWidth, ilPosY - ilHeight);
      TmpPoint.X := ilPosX;
      TmpPOint.Y := ilPosY;
     //    MovePointAngle(TmpPoint,-abs(ilHeight),(TmpAngle*pi)/180);
      AAtextPos.X := Round(TmpPoint.X); //ilPosX;
      AAtextPos.Y := Round(TmpPoint.Y); //ilPosY;
       //  AText.Angle:= 90;
      if Existed = false then
      begin;
// ++ Cadmensky IDB Version 2.3.6
        AText := New(PText, InitCalculate(PProj(AProject).PInfo, PProj(AProject).PInfo.AnnotSettings.AnnotFont, sAnnotData, AATextPos, TmpAngle, 0));
//            AText := New (PText, InitCalculate (PProj (AProject).PInfo, PProj (AProject).PInfo.AnnotSettings.AnnotFont, sAnnotationText, AATextPos, TmpAngle));
// -- Cadmensky IDB Version 2.3.6
        AText.IsItAnnot := true;
      end
      else
      begin;
// ++ Cadmensky IDB Version 2.3.6
        PText(CItem).Text := NewStr(sAnnotData);
//            PText (CItem).Text := NewStr (sAnnotationText);
// -- Cadmensky IDB Version 2.3.6
        PText(CItem).Pos.X := AAtextPos.X;
        PText(CItem).Pos.Y := AAtextPos.Y;
        PText(CItem).Angle := TmpAngle;
        AText := Ptext(CItem);
      end;
      if PView(Bitem).GetObjType = ot_Symbol then
        AText.Font.Style := AText.Font.Style or ts_Transparent;
      if Existed = false then
      begin;
        if PProj(AProject).Layers.IndexObject(PProj(AProject).PInfo, AText) = nil then
// ++ Cadmensky
            //              if not PProj(AProject).InsertObjectOnLayer(AText, PLayer(AnObjectLayer).Index) then

          if not PProj(AProject).InsertObjectOnLayer(AText, PLayer(ADestLayer).Index) then
// -- Cadmensky
            PProj(AProject).CorrectSize(AText.ClipRect, FALSE)

      end
      else
      begin;
        PText(CItem).UpdateTextData(PProj(AProject).Pinfo);
        PText(CItem).CalculateClipRect(PProj(AProject).Pinfo);
        RedrawWithAllAttached(PProj(AProject), PView(CItem), Rect);
      end;
      SelectObject(PProj(AProject).PInfo.ExtCanvas.Handle, AOldFont);
      DeleteObject(ATextFont);

      if Assigned(Bitem) and (Existed = false) then
      begin;
        AView := PView(PLayer(PProj(AProject).PInfo^.Objects)^.IndexObject(PProj(AProject).PInfo, BItem));
        AText.AttachToObject(PProj(AProject).PInfo, AView);
        AText.IsUseOffset := IniFile.AdvAnnotationSettings.OffSetAdv;
        if IniFile.AdvAnnotationSettings.OffSetAdv = true then
        begin;
          AText.AttData.AliPosOfObject := Convert(IniFile.AdvAnnotationSettings.OffSetAdvDirection);
          AText.AttData.AliPosOfMaster := cAlignMM;
        end;
      end;

      if Atext.GetObjMaster(PProj(AProject).PInfo).GetObjType = ot_Poly then
      begin;
        Atext.ObjectOffSetType := IniFile.AdvAnnotationSettings.PolyLineAdvDirection;
      end
      else
      begin;
        if Atext.GetObjMaster(PProj(AProject).PInfo).GetObjType = ot_Arc then
          if IniFile.AdvAnnotationSettings.ArcAdv = true then
            Atext.ObjectOffSetType := IniFile.AdvAnnotationSettings.ArcAdvDirection;
      end;
    end;

  end;
  PProj(AProject).RedrawAfterInsert := TRUE;
  RESULT := TRUE;
end;

function GetWinGISINIFileName: PChar; stdcall;
var
  sResult: AnsiString;
begin
  sResult := OSInfo.WingisIniFileName;
  GetMem(RESULT, Length(sResult) + 1);
  StrPCopy(RESULT, sResult);
end;

function GetDefaultProjectFileName: PChar; stdcall;
var
  IniFile: IniFiles.TIniFile;
  sDefaultFileName: AnsiString;
begin
  IniFile := IniFiles.TIniFile.Create(OSInfo.WingisIniFileName);
  RESULT := PChar(IniFile.ReadString('Settings', 'TemplateFile', ''));
  IniFile.Free;
end;

function ProjectFileHasReadOnlyState(AProject: Pointer): Boolean;
begin
  RESULT := TMDIChild(PProj(AProject).Parent).WasReadOnly;
end;

procedure ChangeGeoTextParameters(AProject, ALayer: Pointer; pcGeoTextIDFieldName, pcGeoTextDataFieldName: PChar); stdcall;
begin
  if PProj(AProject).SymbolMode <> sym_Project then
    exit;
   // if the layer hasn't the IDB as a source of Geotext information, go out.
  if PLayer(ALayer)^.TooltipInfo.BDEOrLocal <> int_IDB then
    exit;
  PLayer(Alayer).TooltipInfo.SetIDB(cs_AttributeTableNamePrefix + IntToStr(PLayer(ALayer).Index), pcGeoTextIDFieldName, pcGeoTextDataFieldName);
end;

function GetLayerItemsCount(AProject, ALayer: Integer; bSelected: Boolean = false): Integer; stdcall;
var
  iI: Integer;
  ALayerItem, ASelectedItem: PIndex;
begin
  RESULT := -1;
  try
    if ALayer = 0 then
      RESULT := PProj(AProject).Layers.SelLayer.SelInfo.Count
    else
    begin
      if bSelected then
      begin
        RESULT := 0;
        for iI := 0 to PProj(AProject).Layers.SelLayer.SelInfo.Count - 1 do
        begin
          ASelectedItem := PProj(AProject).Layers.SelLayer.Data.At(iI);
          ALayerItem := PLayer(ALayer).HasIndexObject(ASelectedItem.Index);
          if (not PLayer(Alayer)^.GetState(sf_LayerOff)) and (ALayerItem <> nil) then
            Inc(RESULT);
        end;
      end
      else
        RESULT := PLayer(ALayer).Data.GetCount;
    end;
  except
  end;
end;

function GetLayerItemIndexByNumber(AProject, ALayer, iItemNumber: Integer; bSelected: Boolean): Integer; stdcall;
var
  ALayerItem, ASelectedItem: PIndex;
  AView: PView;
begin
  RESULT := -1;
  try

    if ALayer <> 0 then
    begin
      if bSelected then
      begin
        ASelectedItem := PProj(AProject).Layers.SelLayer.Data.At(iItemNumber);
        ALayerItem := PLayer(ALayer).HasIndexObject(ASelectedItem.Index)
      end
      else
        ALayerItem := PLayer(ALayer).Data.At(iItemNumber);
    end
    else
      ALayerItem := PProj(AProject).Layers.SelLayer.Data.At(iItemNumber);

    if (ALayerItem <> nil) and (ItemHasSuitableType(ALayerItem)) then
      RESULT := ALayerItem.Index;
  except
  end;
end;

function IsObjectSelected(AProject: Pointer; iItemID: Integer): boolean; stdcall;
var
  ALayerItem, ASelectedItem: PIndex;
begin
  RESULT := PProj(AProject).Layers.SelLayer.HasIndexObject(iItemID) <> nil;
end;

function RunThematicMapBuilder(AProject, ALayer: Pointer; pcAttTableName, pcDefaultDataSourceField, pcListOfFieldsThatAreNotToBeVisible: PChar; AGauge: TGauge): Boolean; stdcall;
var
  IniFile: IniFiles.TIniFile;
  ATM_Lib: TTMLib;
  sTMVersion,
    sMessage,
    sTMLibFileName,
    sLanguageFileExtension,
    sDBFileName: AnsiString;
begin
  IniFile := IniFiles.TIniFile.Create(OSInfo.WingisIniFileName);
  sLanguageFileExtension := IniFile.ReadString('Settings', 'Language', ''); // Default language is english.
  if Trim(sLanguageFileExtension) = '' then
    sLanguageFileExtension := '044';
  IniFile.Free;
   // Create and init the IDB library.
  sTMLibFileName := ExtractFilePath(Application.ExeName) + 'TM.DLL';
   //     sTMLibFileName := 'D:\TM_Dll\TM.DLL';

  sDBFileName := MakeDBFileNameFromProjectFileName(PProj(AProject).FName);

   // if the TM.dll file is mmissing, 'Thematic map' menu item of IDB will be disabled.
   // if this routine executes, the dll at least exists.
  ATM_Lib := TTMLib.Create(Application, sTMLibFileName, sLanguageFileExtension, 'WinGIS');
  sTMVersion := ATM_Lib.GetLibraryVersion;
  if (ATM_Lib.LibraryWasLoaded) and (sTMVersion = cs_TM_NormalVersion) then
  begin
    ATM_Lib.SetSetObjectStyleCallback(SetObjectStyle);
    try
      RESULT := ATM_Lib.StartTM(AProject,
        ALayer,
        0,
        sDBFileName,
        '',
        '',
        pcAttTableName,
        'PROGISID',
        pcDefaultDataSourceField,
        pcListOfFieldsThatAreNotToBeVisible, // '_WG_STATE' + #13#10 + '_WG_INTERNAL_ID'
        0,
        sDBFileName,
        '',
        '',
        'WGIDB_SPTB',
        '_WG_SPF',
        AGauge);
    except
    end;
  end
  else
  begin
      //16='TM.DLL was not loaded because the file has wrong version (you have %s but you need %s). You can replace TM.DLL file and try to use the thematic map support again.'
    sMessage := Format(MlgStringList['IDB', 16], [sTMVersion, cs_TM_NormalVersion]);
    Application.MessageBox(PChar(sMessage), 'WinGIS', MB_OK + MB_DEFBUTTON1 + MB_ICONWARNING);
  end;
  ATM_Lib.Free;
end;

procedure ShowMonitoring(AProject: Pointer; bNeedToShowMonitoringWindow: Boolean); stdcall;
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  WinGISMainForm.IDB_Man.ShowMonitoringData(AProject, bNeedToShowMonitoringWindow, false);
{$ENDIF}
end;

function GetObjectProps(AProject, ALayer: Pointer; iItemID: Integer; ADataPropsType: TDataProps): PChar; stdcall;
var
  dblData: Double;
  sData: AnsiString;
  AItem: PIndex;
  i: integer;
// ++ Cadmensky
  ADPoint: TDPoint;
  APoint: TDPoint;
  AView: PView;
  iLinkID: integer;
// -- Cadmensky
  X, Y: Integer;
begin
  sData := '';
  try
    AItem := PLayer(ALayer).HasIndexObject(iItemID);
    if AItem <> nil then
    begin
// ++ Cadmensky
      if AItem.MPtr = nil then
        AView := PLayer(ALayer).IndexObject(PProj(AProject).PInfo, AItem);
    {+++brovak}
      if ADataPropsType = dtParent then
      begin
        iLinkID := PView(AItem.MPtr).GetIdxMaster;
        if iLinkID <> 0 then
          sData := IntToStr(iLinkID);
      end
      else
     {---brovak}

// -- Cadmensky
         // Data types = dtLength, dtArea, dtPerimeter, dtX, dtY
        case AItem.GetObjType of
          {+++brovak}
          ot_Text:
            begin
              try
                case ADataPropsType of
                  dtX:
                    begin
                      dblData := PText(AItem.MPtr).GivePosX / 100;
                      sData := Format('%.4f', [dblData]);
                    end;
                  dtY:
                    begin
                      dblData := PText(AItem.MPtr).GivePosY / 100;
                      sData := Format('%.4f', [dblData]);
                    end;
                  dtSize:
                    begin;
                      sData := FloatToStr(PTExt(AItem.MPtr).Font.Height);
                    end;
                  dtText:
                    begin;
                      sData := PtOStr(PText(AItem.MPtr).Text);
                    end;
                  dtRotation:
                    begin
                      dblData := PText(AItem.MPtr).Angle;

                      while dblData < 0 do
                        dblData := dblData + 360;
                      while dblData >= 360 do
                        dblData := dblData - 360;

                      sData := FloatToStr(dblData);
                    end;

                end;
              except
              end;
            end;

         {---brovak}

          ot_Pixel:
            begin
              try
                case ADataPropsType of
                  dtX:
                    begin
                        //sData := FloatToStr (PPixel (AItem.MPtr).GivePosX / 100);
                      dblData := PPixel(AItem.MPtr).GivePosY / 100;
                      sData := Format('%.4f', [dblData]);
                    end;
                  dtY:
                    begin
                        //sData := FloatToStr (PPixel (AItem.MPtr).GivePosY / 100);
                      dblData := PPixel(AItem.MPtr).GivePosX / 100;
                      sData := Format('%.4f', [dblData]);
                    end;
                end;
              except
              end;
            end;
          ot_Poly, ot_Spline:
            begin
              try
                case ADataPropsType of
                  dtSize:
                    begin
                      dblData := PPoly(AItem.MPtr).Laenge;
                      sData := Format('%.4f', [dblData]);
                    end;
                  dtVerticesCount:
                    sData := IntToStr(PPoly(AItem.MPtr).Data.Count);
// ++ Cadmensky
                  dtX:
                    try
                      dblData := PDPoint(PPoly(AItem.MPtr)^.Data^.At(0))^.Y / 100;
                      sData := Format('%.4f', [dblData]);
//                              sData := FloatToStr (PPoly (AItem.MPtr).Data.At(0).X);
                    except
                      sData := '';
                    end;
                  dtY:
                    try
                      dblData := PDPoint(PPoly(AItem.MPtr)^.Data^.At(0))^.X / 100;
                      sData := Format('%.4f', [dblData]);
//                              sData := FloatToStr (PPoly (AItem.MPtr).Data.At(0).Y);
                    except
                      sData := '';
                    end;
                  dtX1: // WAI: also last point of poly
                    try
                      dblData := PDPoint(PPoly(AItem.MPtr)^.Data^.At(PPoly(AItem.Mptr)^.Data^.Count - 1))^.Y / 100;
                      sData := Format('%.4f', [dblData]);
                    except
                      sData := '';
                    end;
                  dtY1:
                    try
                      dblData := PDPoint(PPoly(AItem.MPtr)^.Data^.At(PPoly(AItem.Mptr)^.Data^.Count - 1))^.X / 100;
                      sData := Format('%.4f', [dblData]);
                    except
                      sData := '';
                    end;
                end;
              except
              end;
            end;
          ot_CPoly:
            begin
              try
                case ADataPropsType of
                  dtArea:
                    begin
                      dblData := PCPoly(AItem.MPtr).Flaeche;
                      sData := Format('%.4f', [dblData]);
                    end;
                  dtSize:
                    begin
                      dblData := PCPoly(AItem.MPtr).Laenge;
                      sData := Format('%.4f', [dblData]);
                    end;
                  dtVerticesCount:
                    sData := IntToStr(PPoly(AItem.MPtr).Data.Count - 1);

                        {brovak}
                  dtX:
                    begin
// ++ Cadmensky
                      APoint.Init(0, 0);
                      APoint := Pcpoly(AItem.MPtr)^.GetCenter;
                      Dbldata := APoint.Y / 100;
//                              SData := FloatToStr (dblData / 100);
                      sData := Format('%.4f', [dblData]);
{                              ADPoint := PCPoly (AItem.MPtr).GetComplCenterOfGravity;
                              sData := FloatToStr (ADPoint.X);
                              ADPoint.Free;}
// ++ Cadmensky
                    end;
                  dtY:
                    begin
// ++ Cadmensky
                      APoint.Init(0, 0);
                      APoint := Pcpoly(AItem.MPtr)^.GetCenter;
                      Dbldata := APoint.X / 100;
//                              SData := FloatToStr (dblData / 100);
                      sData := Format('%.4f', [dblData]);
{                              ADPoint := PCPoly (AItem.MPtr).GetComplCenterOfGravity;
                              sData := FloatToStr (ADPoint.Y);
                              ADPoint.Free;}
// ++ Cadmensky
                    end;
                        {brovak}
                end;
              except
              end;
            end;
          ot_Symbol:
            begin
              try
                if PSymbol(AItem.MPtr).SymPtr = nil then
                  PSymbol(AItem.MPtr).SetSymPtr(PProj(AProject).PInfo^.Symbols);

                case ADataPropsType of
                  dtX:
                    begin
//                              sData := FloatToStr (PSymbol (AItem.MPtr).GivePosX / 100);
                      Dbldata := PSymbol(AItem.MPtr).GivePosY / 100;
                      sData := Format('%.4f', [dblData]);
                    end;
                  dtY:
                    begin
//                              sData := FloatToStr (PSymbol (AItem.MPtr).GivePosY / 100);
                      Dbldata := PSymbol(AItem.MPtr).GivePosX / 100;
                      sData := Format('%.4f', [dblData]);
                    end;
                           {+++brovak}
                  dtSymbolName:
                    begin
                      sData := PSymbol(AItem.MPtr).SymPtr.GetName;
                    end;
                  dtText:
                    begin;
                      sData := PSymbol(AItem.MPtr).SymPtr.GetLibName;
                    end;
                  dtSize:
                    begin;
                      sData := FloatToStr(PSymbol(AItem.MPtr).Size);
                    end;
                  dtRotation:
                    begin
                      if PSymbol(AItem.MPtr)^.UsedDefaultAngle then
                      begin
                        dblData := 0;
                      end
                      else
                      begin
                        dblData := PSymbol(AItem.MPtr).Angle;
                      end;

                      sData := FloatToStr((dblData * 180) / PI);
                    end;
                          {---brovak}
                end;
              except
              end;
            end;
          ot_Circle:
            begin
              try
                case ADataPropsType of
                  dtX: sData := FloatToStr(PEllipse(AItem.MPtr).GivePosY / 100);
                  dtY: sData := FloatToStr(PEllipse(AItem.MPtr).GivePosX / 100);
                        {brovak}
                  dtSize:
                    begin;
                      sData := IntToStr(PEllipse(AItem.MPtr).GiveRadius);
                    end;
                         {brovak}
                end;
              except
              end;
            end;
          ot_Arc:
            begin
              try
                case ADataPropsType of
                  dtX: sData := FloatToStr(PEllipseArc(AItem.MPtr).GivePosY / 100);
                  dtY: sData := FloatToStr(PEllipseArc(AItem.MPtr).GivePosX / 100);
                        {+++brovak}
                  dtText: sData := FloatToStr(PEllipseArc(AItem.MPtr).Angle);
                        {--brovak}
                end;
              except
              end;
            end;
// ++ Cadmensky
          ot_Image:
            try
              case ADataPropsType of
                     {brovak}
                dtText: sData := PImage(AItem.MPtr).FileName^;
                dtArea: sData := FloatToStr(PImage(AItem.MPtr).GiveArea / 100); //wai
                dtX: sData := FloatToStr(PImage(AItem.MPtr).GivePosY / 100); //wai
                dtY: sData := FloatToStr(PImage(AItem.MPtr).GivePosX / 100); //wai
                dtSize: sData := FloatToStr(PImage(AItem.MPtr).GivePerimeter / 100); //wai
                dtX1: sData := FloatToStr(PImage(AItem.MPtr).GivePosY1 / 100); //wai
                dtY1: sData := FloatToStr(PImage(AItem.MPtr).GivePosX1 / 100);
                      //WAI
                      {brovak}
              end;
// -- Cadmensky
            except
            end;
        end; // case end
    end;
  finally
    GetMem(RESULT, Length(sData) + 1);
    StrPCopy(RESULT, sData);
  end;
end;

procedure ReverseUsingOfIDBInfo; stdcall;
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  if AnIDBInfoButton <> nil then
    AnIDBInfoButton.Down := FALSE;
  WinGISMainForm.WeAreUsingTheIDBInfoWindow := FALSE;
{$ENDIF}
end;

function GetMDIChildWindowState: TWindowState; stdcall;
begin
  RESULT := wsMaximized;
  try
    RESULT := WinGISMainForm.ActualChild.WindowState;
// ++ Commented by Cadmensky IDB Version 2.3.8
//      if RESULT = wsMaximized then
//         WinGISMainForm.ActualChild.WindowState := wsNormal; // It will be restored with IDB attribute window together.
// -- Commented by Cadmensky IDB Version 2.3.8
  except
  end;
end;

procedure MakeAnnotationUpdate(ALayer: Pointer; iItemID: LongInt); stdcall;
var
  iI, iStartIndex, iFinishIndex: Integer;
  AProject: PProj;
  AItem: TIndex;
  AItemPtr: PIndex;
begin
   (*  AProject := PLayers(PLayer(ALayer).Owner).Parent;
     AItem.Index := mi_RText;
     PLayer(ALayer).Data.Search(@AItem, iStartIndex);
     AItem.Index := mi_BusGraph;
     PLayer(ALAyer).Data.Search(@AItem, iFinishIndex);
     for iI := iStartIndex to iFinishIndex - 1 do
     begin
       AItemPtr := PLayer(ALayer).IndexObject(AProject.PInfo, PLayer(ALayer).Data.At(iI));
       if (AItemPtr <> nil) and (PRTExt(AItemPtr).ID = iItemID) then
       begin
         PRText(AItemPtr).GetDataFromDB(PLayer(ALayer).Index);

         AItemPtr^.Invalidate(AProject^.PInfo);
         PRText(AItemPtr)^.CalculateClipRect(AProject^.PInfo);
         AProject^.UpdateClipRect(PView(AItemPtr));
         AItemPtr^.Invalidate(AProject^.PInfo);
         if not AProject^.CorrectSize(PView(AItemPtr)^.ClipRect, TRUE) then AProject^.PInfo^.RedrawInvalidate;
         AProject^.SetModified;
       end;
     end; // for iI end
     *)
end;

procedure SetNessesitySaveUndoDataSign(AProject: Pointer; bValue: Boolean);
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  if WinGISMainForm.WeAreUsingUndoFunction[AProject] then
    WinGISMainForm.UndoRedo_Man.SetNecessitySaveUndoDataSign(AProject, bValue);
{$ENDIF}
end;

procedure SaveUndoData(AProject: Pointer; AnUndoType: TUndoType);
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  if WinGISMainForm.WeAreUsingUndoFunction[AProject] then
    WinGISMainForm.UndoRedo_Man.SaveUndoData(AProject, AnUndoType);
{$ENDIF}
end;

function GetWinGISMainFormProperty(iProperty: integer): Integer;
begin
  case iProperty of
    1: RESULT := WinGISMainForm.Left;
    2: RESULT := WinGISMainForm.Top;
    3: RESULT := WinGISMainForm.Width;
    4: RESULT := WinGISMainForm.Height;
  end;
end;

procedure SetWinGISMainFormProperty(iProperty, iValue: integer);
begin
  case iProperty of
    1: WinGISMainForm.Left := iValue;
    2: WinGISMainForm.Top := iValue;
    3: WinGISMainForm.Width := iValue;
    4: WinGISMainForm.Height := iValue;
    5:
      case iValue of
        0: WinGISMainForm.Align := alNone;
        1: WinGISMainForm.Align := alLeft;
      end;
  end;
end;

///////////////// I N T E R N A L   D A T A B A S E   M A N A G E R ////////////

constructor TIDB.Create;
var
  IniFile: IniFiles.TIniFile;
  sIDBlibFileName,
    sLanguageFileExtension,
    sIDBVersion,
    sMessage: AnsiString;
begin
  IniFile := IniFiles.TIniFile.Create(OSInfo.WingisIniFileName);
  sLanguageFileExtension := IniFile.ReadString('Settings', 'Language', ''); // Default language is english.
  if Trim(sLanguageFileExtension) = '' then
    sLanguageFileExtension := '044';
  IniFile.Free;
   // Create and init the IDB library.
  sIDBLibFileName := ExtractFilePath(Application.ExeName) + 'IDB.DLL';
   //     sIDBLibFileName := 'd:\IDB_DLL\IDB.DLL';

{$IFNDEF IDBCOM}
  SELF.IDBLib := TIDBLib.Create(WinGisMainForm, sIDBLibFileName, sLanguageFileExtension);
{$ELSE}
  SELF.IDBLib := CoIDBLib.Create;
  SELF.IDBLib.InitLibrary(Integer(Application.Handle), Integer(Screen), Integer(WinGisMainForm.Handle), PChar(sLanguageFileExtension));
{$ENDIF}

{$IFNDEF IDBCOM}
   // Connect all callbacks.
  SELF.IDBLib.SetShowProjectCallback(ShowProject);
  SELF.IDBLib.SetShowSelectedItemsInProjectCallback(ShowSelectedItemsInProject);
  SELF.IDBLib.SetShowItemInProjectWithZoomCallback(ShowItemInProjectWithZoom);
  SELF.IDBLib.SetDeselectAllObjectsCallback(DeselectAllObjects);
// ++ Commented by Cadmensky IDB Version 2.2.9
{   SELF.IDBLib.GetProjectUndoStepsCountCallback (GetProjectUndoStepsCount);
   SELF.IDBLib.GetProjectFileSizeCallback (GetProjectFileSize);}
// -- Commented by Cadmensky IDB Version 2.2.9

  SELF.IDBLib.SetCheckItemExistingCallback(ItemExistsInProject);
  SELF.IDBLib.SetItemToGuidCallback(ItemToGuid);
  SELF.IDBLib.SetProjectModifiedCallback(SetProjectModified);

  SELF.IDBLib.SetSetStartProjectGUIDCallback(SetStartProjectGUID);
  SELF.IDBLib.SetSetCurrentProjectGUIDCallback(SetCurrentProjectGUID);
  SELF.IDBLib.SetGetStartProjectGUIDCallback(GetStartProjectGUID);
  SELF.IDBLib.SetGetCurrentProjectGUIDCallback(GetCurrentProjectGUID);

  SELF.IDBLib.SetGetProjectIsNewAndNotSavedCallback(GetProjectNewAndNotSavedState);
  SELF.IDBLib.SetGetProjectFullFileNameCallback(GetProjectFullFileName);
  SELF.IDBLib.SetGetLayerNameCallback(GetLayerName);
  SELF.IDBLib.SetGetLayerIndexCallback(GetLayerIndex);
// ++ Cadmensky
  SELF.IDBLib.SetGetActiveLayerCallback(GetActiveLayer);
// -- Cadmensky
  SELF.IDBLib.SetLayerIsFixedCallBack(LayerIsFixed);

  SELF.IDBLib.SetGetAllProjectWindowsNumberCallback(GetAllProjectWindowsNumber);
  SELF.IDBLib.SetGetProjectLayerNumberCallback(GetProjectLayerNumber);
  SELF.IDBLib.SetGetLayerPointerByPositionCallback(GetLayerPointerByPosition);
  SELF.IDBLib.SetGetLayerPointerByIndexCallback(GetLayerPointerByIndex);

  SELF.IDBLib.SetGetWinGISINIFileNameCallback(GetWinGISINIFileName);
  SELF.IDBLib.SetGetDefaultProjectFileNameCallback(GetDefaultProjectFileName);

{   SELF.IDBLib.SetGeneratePointCallback (GeneratePoint);
   SELF.IDBLib.SetGenerateSymbolCallback (GenerateSymbol);}
  SELF.IDBLib.SetAnnotationSettingsDialogExecuteCallback(AnnotationSettingsDialogExecute);
  SELF.IDBLib.SetMakeAnnotationInProjectCallback(MakeAnnotationInProject);
  SELF.IDBLib.SetMakeAnnotationUpdateCallback(MakeAnnotationUpdate);

  Self.IDBLib.SetLayerForInsertCallback(MakeLayerForInsert);

  SELF.IDBLib.SetShowDonepercentCallback(ShowDonePercent);
// ++ Commented by Cadmensky Version 2.2.8
//   SELF.IDBLib.SetGatherGarbageInProjectCallback (GatherGarbageInProject);
//   SELF.IDBLib.SetGatherSelectedGarbageInProjectCallback (GatherSelectedGarbageInProject);
// -- Commented by Cadmensky Version 2.2.8

  SELF.IDBLib.SetRedrawProjectWindowCallback(RedrawProjectWindow);
  SELF.IDBLib.SetChangeGeoTextParametersCallback(ChangeGeoTextParameters);
// ++ Cadmensky Version 2.2.8
  SELF.IDBLib.SetGetLayerItemsCountCallback(GetLayerItemsCount);
  SELF.IDBLib.SetGetLayerItemIndexByNumberCallback(GetLayerItemIndexByNumber);
  SELF.IDBLib.SetIsObjectSelectedCallback(IsObjectSelected);
// -- Cadmensky Version 2.2.8
  SELF.IDBLib.SetRunThematicMapBuilderCallback(RunThematicMapBuilder);
  SELF.IDBLib.SetRunMonitoringCallback(ShowMonitoring);
  SELF.IDBLib.SetGetObjectPropsCallback(GetObjectProps);
  SELF.IDBLib.SetReverseUsingIDBInfoCallback(ReverseUsingOfIDBInfo);
  SELF.IDBLib.SetGetMDIChildWindowStateCallback(GetMDIChildWindowState);

  SELF.IDBLib.SetMakeSpecialChartCallBack(MakeSpecialChart);
  SELF.IDBLib.SetSendSpecialforChartCallback(SendSpecialforChart);
  SELF.IDBLib.SetShowNewThematicMapDialogCallBack(ShowNewThematicMapDialog);
  SELF.IDBLib.SetSendToTMSelectedIDCallback(SendToTMSelectedID);
  SELF.IDBLib.SetShowPercentAtWinGISCallback(ShowPercentAtWinGIS);

   // ++ Cadmensky
  SELF.IDBLib.SetSetNessesitySaveUndoDataSignCallback(SetNessesitySaveUndoDataSign);
  SELF.IDBLib.SetSaveUndoDataCallback(SaveUndoData);
   // -- Cadmensky
{$ELSE}
  SELF.IDBLib.SetShowProjectCallback(Integer(@ShowProject));
  SELF.IDBLib.SetShowSelectedItemsInProjectCallback(Integer(@ShowSelectedItemsInProject));
  SELF.IDBLib.SetShowItemInProjectWithZoomCallback(Integer(@ShowItemInProjectWithZoom));
  SELF.IDBLib.SetDeselectAllObjectsCallback(Integer(@DeselectAllObjects));

  SELF.IDBLib.SetCheckItemExistingCallback(Integer(@ItemExistsInProject));
  SELF.IDBLib.SetItemToGuidCallback(Integer(@ItemToGuid));
  SELF.IDBLib.SetSetProjectModifiedCallback(Integer(@SetProjectModified));

  SELF.IDBLib.SetSetStartProjectGUIDCallback(Integer(@SetStartProjectGUID));
  SELF.IDBLib.SetSetCurrentProjectGUIDCallback(Integer(@SetCurrentProjectGUID));
  SELF.IDBLib.SetGetStartProjectGUIDCallback(Integer(@GetStartProjectGUID));
  SELF.IDBLib.SetGetCurrentProjectGUIDCallback(Integer(@GetCurrentProjectGUID));

  SELF.IDBLib.SetGetProjectIsNewAndNotSavedCallback(Integer(@GetProjectNewAndNotSavedState));
  SELF.IDBLib.SetGetProjectFullFileNameCallback(Integer(@GetProjectFullFileName));
  SELF.IDBLib.SetGetLayerNameCallback(Integer(@GetLayerName));
  SELF.IDBLib.SetGetLayerIndexCallback(Integer(@GetLayerIndex));
// ++ Cadmensky
  SELF.IDBLib.SetGetActiveLayerCallback(Integer(@GetActiveLayer));
  SELF.IDBLib.SetGetLayerItemsCountCallback(Integer(@GetLayerItemsCount));
  SELF.IDBLib.SetGetLayerItemIndexByNumberCallback(Integer(@GetLayerItemIndexByNumber));
  SELF.IDBLib.SetIsObjectSelectedCallback(Integer(@IsObjectSelected));
// -- Cadmensky
  SELF.IDBLib.SetLayerIsFixedCallBack(Integer(@LayerIsFixed));

  SELF.IDBLib.SetGetAllProjectWindowsNumberCallback(Integer(@GetAllProjectWindowsNumber));
  SELF.IDBLib.SetGetProjectLayerNumberCallback(Integer(@GetProjectLayerNumber));
  SELF.IDBLib.SetGetLayerPointerByPositionCallback(Integer(@GetLayerPointerByPosition));
  SELF.IDBLib.SetGetLayerPointerByIndexCallback(Integer(@GetLayerPointerByIndex));

  SELF.IDBLib.SetGetWinGISINIFileNameCallback(Integer(@GetWinGISINIFileName));
  SELF.IDBLib.SetGetDefaultProjectFileNameCallback(Integer(@GetDefaultProjectFileName));

  SELF.IDBLib.SetAnnotationSettingsDialogExecuteCallback(Integer(@AnnotationSettingsDialogExecute));
  SELF.IDBLib.SetMakeAnnotationInProjectCallback(Integer(@MakeAnnotationInProject));
  SELF.IDBLib.SetMakeAnnotationUpdateCallback(Integer(@MakeAnnotationUpdate));

//   Self.IDBLib.SetLayerForInsertCallback (Integer (@MakeLayerForInsert));

  SELF.IDBLib.SetShowDonePercentCallback(Integer(@ShowDonePercent));

//   SELF.IDBLib.SetRedrawProjectWindowCallback (Integer (@RedrawProjectWindow));
  SELF.IDBLib.SetChangeGeoTextParametersCallback(Integer(@ChangeGeoTextParameters));
//   SELF.IDBLib.SetGetSelectedItemsCountCallback (Integer (@GetSelectedItemsCount));
  SELF.IDBLib.SetRunThematicMapBuilderCallback(Integer(@RunThematicMapBuilder));
  SELF.IDBLib.SetRunMonitoringCallback(Integer(@ShowMonitoring));
  SELF.IDBLib.SetGetObjectPropsCallback(Integer(@GetObjectProps));
  SELF.IDBLib.SetReverseUsingIDBInfoCallback(Integer(@ReverseUsingOfIDBInfo));
  SELF.IDBLib.SetGetMDIChildWindowStateCallback(Integer(@GetMDIChildWindowState));

  SELF.IDBLib.SetMakeSpecialChartCallBack(Integer(@MakeSpecialChart));
  SELF.IDBLib.SetSendSpecialforChartCallback(Integer(@SendSpecialforChart));
  SELF.IDBLib.SetShowNewThematicMapDialogCallBack(Integer(@ShowNewThematicMapDialog));
  SELF.IDBLib.SetSendToTMSelectedIDCallback(Integer(@SendToTMSelectedID));
  SELF.IDBLib.SetShowPercentAtWinGISCallback(Integer(@ShowPercentAtWinGIS));

// ++ Cadmensky
  SELF.IDBLib.SetSetNessesitySaveUndoDataSignCallback(Integer(@SetNessesitySaveUndoDataSign));
  SELF.IDBLib.SetSaveUndoDataCallback(Integer(@SaveUndoData));
  SELF.IDBLib.SetGetWinGISMainFormPropertyCallBack(Integer(@GetWinGISMainFormProperty));
  SELF.IDBLib.SetSetWinGISMainFormPropertyCallBack(Integer(@SetWinGISMainFormProperty));
// -- Cadmensky
{$ENDIF}

{$IFNDEF IDBCOM}
  if not FileExists(sIDBlibFileName) then
  begin
      // 'IDB.DLL was not loaded because the file is missing. You can put IDB.DLL (version ' + cs_IDB_NormalVersion + ') to WinGIS directory\n"' + ExtractFilePath(Application.ExeName) + '"\nand try to switch the Internal database on again.'
    sMessage := SmartString(MlgStringList['IDB', 10], [cs_IDB_NormalVersion, ExtractFilePath(Application.ExeName)]);
    Application.MessageBox(PChar(sMessage), 'WinGIS', MB_OK + MB_DEFBUTTON1 + MB_ICONWARNING);
  end
  else
  begin
    if not SELF.IDBLib.LibraryWasLoaded then
    begin
         // 'IDB.DLL was not loaded'
      sMessage := MlgStringList['IDB', 13];
      Application.MessageBox(PChar(sMessage), 'WinGIS', MB_OK + MB_DEFBUTTON1 + MB_ICONWARNING);
    end
{      else
      begin
         sIDBVersion := SELF.IDBLib.GetLibraryVersion;
         if sIDBVersion <> cs_IDB_NormalVersion then
         begin
         // 'IDB.DLL was not loaded because the file has wrong version (You have ' + sIDBVersion + ' but You need ' + cs_IDB_NormalVersion + '). You can replace IDB.DLL file and try to switch the Internal database on again.'
            sMessage := Format (MlgStringList['IDB', 11], [sIDBVersion, cs_IDB_NormalVersion]);
            Application.MessageBox (PChar (sMessage), 'WinGIS', MB_OK + MB_DEFBUTTON1 + MB_ICONWARNING);
         end;
      end;}
  end;
{$ENDIF}
end;

destructor TIDB.Free;
begin
{$IFNDEF IDBCOM}
  SELF.IDBLib.Free;
{$ELSE}
  SELF.IDBLib.FinLibrary;
{$ENDIF}
end;

procedure TIDB.SetScreenCursor;
begin
  AnOldScreenCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
end;

procedure TIDB.RestoreScreenCursor;
begin
  Screen.Cursor := AnOldScreenCursor;
end;

function TIDB.LibraryWasLoaded: Boolean;
begin
{$IFNDEF IDBCOM}
  RESULT := FALSE;
  if Self <> nil then
    RESULT := (SELF.IDBLib.LibraryWasLoaded) and (SELF.IDBLib.GetLibraryVersion = cs_IDB_NormalVersion);
{$ELSE}
  RESULT := true;
{$ENDIF}
end;

procedure TIDB.SetReadOnlyState(bReadOnlyState: Boolean);
begin
{$IFNDEF IDBCOM}
  SELF.IDBLib.SetReadOnlyState(bReadOnlyState);
{$ELSE}
  if bReadOnlyState then
    SELF.IDBLib.SetReadOnlyState(1)
  else
    SELF.IDBLib.SetReadOnlyState(0);
{$ENDIF}
end;

procedure TIDB.CloseAllWindowsOfAProject(AProject: Pointer);
begin
{$IFNDEF IDBCOM}
  SELF.IDBLib.CloseAllWindowsOfAProject(AProject);
{$ELSE}
  SELF.IDBLib.CloseAllWindowsOfProject(Integer(AProject));
{$ENDIF}
end;

{This procedure closes ALL data windows of ALL project that are using the IDB.}

procedure TIDB.CloseDataWindowsOfAllProjects;
begin
  if Self <> nil then
    SELF.IDBLib.CloseAllWindowsOfAllProjects;
end;

{Refresh data in all data window for concrete project.}

procedure TIDB.RefreshDataWindows(AProject: PProj);
begin
  if AProject.SymbolMode <> sym_Project then
    exit;
{$IFDEF IDBCOM}
  SELF.IDBLib.RefreshAllDataWindows(Integer(AProject));
{$ELSE}
  SELF.IDBLib.RefreshAllDataWindows(AProject);
{$ENDIF}
end;
{brovak}

function TIDB.IsOpenedWindow(AProject: Pointer): integer;
begin;
  RESULT := 0;
  if Self <> nil then
{$IFDEF IDBCOM}
    RESULT := SELF.IDBLib.IsOpenedWindow(Integer(AProject));
{$ELSE}
    RESULT := SELF.IDBLib.IsOpenedWindow(AProject);
{$ENDIF}
end;
{brovak}
{Add project in projects definitions list, it this is reruired.}

procedure TIDB.AddProject(AProject: PProj);
begin
  try
   // At first I recognize whether a project is a symbol editor or not.
    if AProject.SymbolMode <> sym_Project then
      exit;
{$IFDEF IDBCOM}
    if ProjectFileHasReadOnlyState(AProject) then
      SELF.IDBLib.AddProject(TForm(AProject.Parent).Handle, Integer(AProject), 1)
    else
      SELF.IDBLib.AddProject(TForm(AProject.Parent).Handle, Integer(AProject), 0);
{$ELSE}
    SELF.IDBLib.AddProject(TForm(AProject.Parent).Handle, AProject, ProjectFileHasReadOnlyState(AProject));
{$ENDIF}
  except
  end;
end;

{Delete project from project definitions list}

procedure TIDB.DeleteProject(AProject: PProj);
begin
  try
   // At first I recognize whether a project is a symbol editor or not.
    if AProject.SymbolMode <> sym_Project then
      exit;
{$IFDEF IDBCOM}
    SELF.IDBLib.DeleteProject(Integer(AProject));
{$ELSE}
    SELF.IDBLib.DeleteProject(AProject);
{$ENDIF}
  except
  end;
end;

procedure TIDB.SetIAmGenerateObjectsMyselfSign(AProject: PProj; bValue: Boolean);
begin
{$IFDEF IDBCOM}
  if bValue then
    SELF.IDBLib.Set_DoNotAddAttributeRecords_Sign(Integer(AProject), 1)
  else
    SELF.IDBLib.Set_DoNotAddAttributeRecords_Sign(Integer(AProject), 0);
{$ELSE}
  SELF.IDBLib.Set_DoNotAddAttributeRecords_Sign(AProject, bValue);
{$ENDIF}
end;

{It's called by WinGis for layer attribute table data showing.}

procedure TIDB.ShowDataTable;
var
  ALayer: PLayer;
  AALayer: PLayer;
begin
   // At first I recognize whether a project is a symbol editor or not.
  if PProj(WinGisMainForm.ActualProj).SymbolMode <> sym_Project then
    exit;
  ALayer := PProj(WinGisMainForm.ActualProj).Layers.TopLayer;
{brovak}
  AALayer := PProj(WinGisMainForm.ActualProj).Layers^.IndexToLayer(MenuFunctions['ListLayers'].ItemIndex);
{$IFDEF IDBCOM}
  SELF.IDBLib.ShowAttributeWindow(Integer(WinGisMainForm.ActualProj), Integer(ALayer), Integer(AALayer));
{$ELSE}
  SELF.IDBLib.ShowDataTable(WinGisMainForm.ActualProj, ALayer, AALayer);
{$ENDIF}
{brovak}
end;

procedure TIDB.ShowDataTableWithSelectBefore(AProject: PProj);
var
  SILBF: TIDB_SelectInListBoxForm;
  iI: Integer;
  ALayer: PLayer;
{brovak}
  AALayer: Player;
{brovak}
begin
   // At first I recognize whether a project is a symbol editor or not.
  if PProj(WinGisMainForm.ActualProj).SymbolMode <> sym_Project then
    exit;
  SILBF := TIDB_SelectInListBoxForm.Create(Application);
  try
    SILBF.ListBox1.MultiSelect := FALSE;
    SILBF.ListBox1.PopupMenu := nil;
    SILBF.ListBox1.Items.Clear;
      // if there are more than one layer.
    for iI := 1 to AProject.Layers.LData.Count - 1 do
    begin
      ALayer := AProject.Layers.LData.At(iI);
      if LayerIsVisible(ALayer) then
        SILBF.ListBox1.Items.Add(ALayer.Text^);
    end;
    SILBF.ListBox1.ItemIndex := 0;
{brovak}
     // SELF.IDBLib.ShowDataTable(WinGisMainForm.ActualProj, ALayer, AALayer);
{brovak}
    if SILBF.ShowModal = mrOK then
    begin
      ALayer := AProject.Layers.NameToLayer(SILBF.ListBox1.Items[SILBF.ListBox1.ItemIndex]);
{brovak}
      AALayer := Alayer;
{brovak}
{$IFDEF IDBCOM}
      SELF.IDBLib.ShowAttributeWindow(Integer(WinGisMainForm.ActualProj), Integer(ALayer), Integer(AALayer));
{$ELSE}
      SELF.IDBLib.ShowDataTable(WinGisMainForm.ActualProj, ALayer, AALayer);
{$ENDIF}
    end;
  finally
    SILBF.Free;
  end;
end;

{It's called by WinGis for monitoring data showing.}

procedure TIDB.ShowMonitoringTable(bAlwaysShowMonitoring: Boolean);
begin
   // At first I recognize whether a project is a symbol editor or not.
  if PProj(WinGisMainForm.ActualProj).SymbolMode <> sym_Project then
    exit;
   // ++ Cadmensky
{   if (SELF.IsOpenedWindow (PProj (WinGisMainForm.ActualProj)) mod 2 > 0) then
      SELF.CloseAllWindowsOfAProject (PProj (WinGisMainForm.ActualProj));}
   // -- Cadmensky
  ShowMonitoringData(WinGisMainForm.ActualProj, true, bAlwaysShowMonitoring);
end;

procedure TIDB.ShowInfoTable;
var
  AProject: PProj;
  ALayer: PLayer;
  iI, iJ: Integer;
  ASelectedItem, ALayerItem: PIndex;
begin
  AProject := WinGisMainForm.ActualProj;
   // At first I recognize whether a project is a symbol editor or not.
  if AProject.SymbolMode <> sym_Project then
    exit;
   // Show nfo window with data.
  SetScreenCursor;
  try
{$IFDEF IDBCOM}
    SELF.IDBLib.GetReadyToReceiveIDsOfObjectsForMonitoring(Integer(AProject));
{$ELSE}
    SELF.IDBLib.GetReadyToReceiveIDsOfObjectsForMonitoring(AProject);
{$ENDIF}
    for iI := 0 to AProject.Layers.SelLayer.Data.GetCount - 1 do
    begin
      ASelectedItem := AProject.Layers.SelLayer.Data.At(iI);
      for iJ := 1 to AProject.Layers.LData.Count - 1 do
      begin
        ALayer := AProject.Layers.LData.At(iJ);
        ALayerItem := ALayer.HasIndexObject(ASelectedItem.Index);
        if (ALayerItem <> nil) and ALayerItem.GetState(sf_Selected) then
        begin
{$IFDEF IDBCOM}
          SELF.IDBLib.ReceiveIDOfObjectForMonitoring(Integer(AProject), Integer(ALayer), ALayerItem.Index);
{$ELSE}
          SELF.IDBLib.ReceiveIDOfObjectForMonitoring(AProject, ALayer, ALayerItem.Index);
{$ENDIF}
          BREAK;
        end;
      end; // for iJ end
    end; // for iI end
{$IFDEF IDBCOM}
    SELF.IDBLib.ShowInfoWindow(Integer(AProject));
{$ELSE}
    SELF.IDBLib.ShowInfoWindow(AProject);
{$ENDIF}
  finally
    RestoreScreenCursor;
  end;
end;

procedure TIDB.ClearInfoTable(AProject: Pointer);
begin
{$IFDEF IDBCOM}
  SELF.IDBLib.ClearInfoWindow(Integer(AProject));
{$ELSE}
  SELF.IDBLib.ClearInfoWindow(AProject);
{$ENDIF}
end;

procedure TIDB.HideInfoTable;
begin
  SELF.IDBLib.HideInfoWindow;
end;

procedure TIDB.CloseInfoTable;
begin
  SELF.IDBLib.CloseInfoWindow;
end;

{ Show data for monitoring }

procedure TIDB.ShowMonitoringData(AProject: PProj; bNeedToShowMonitoringWindow, bAlwaysShowMonitoring: Boolean);
var
  iI, iJ: Integer;
  iwK: Word;
  ALayer: PLayer;
  bFound: Boolean;
  ASelectedItem, ALayerItem: PIndex;
begin
   // At first I recognize whether a project is a symbol editor or not.
  if AProject.SymbolMode <> sym_Project then
    exit;

  if AProject.Layers.SelLayer.Data.GetCount < 1 then
    exit;

  SetScreenCursor;
  try
{$IFDEF IDBCOM}
    SELF.IDBLib.GetReadyToReceiveIDsOfObjectsForMonitoring(Integer(AProject));
{$ELSE}
    SELF.IDBLib.GetReadyToReceiveIDsOfObjectsForMonitoring(AProject);
{$ENDIF}
    for iI := 0 to AProject.Layers.SelLayer.Data.GetCount - 1 do
    begin
      ASelectedItem := AProject.Layers.SelLayer.Data.At(iI);
      for iJ := 1 to AProject.Layers.LData.Count - 1 do
      begin
        ALayer := AProject.Layers.LData.At(iJ);
        ALayerItem := ALayer.HasIndexObject(ASelectedItem.Index);
        if (ALayerItem <> nil)
          and
          ALayerItem.GetState(sf_Selected) then
        begin
{$IFDEF IDBCOM}
          SELF.IDBLib.ReceiveIDOfObjectForMonitoring(Integer(AProject), Integer(ALayer), ALayerItem.Index);
{$ELSE}
          SELF.IDBLib.ReceiveIDOfObjectForMonitoring(AProject, ALayer, ALayerItem.Index);
{$ENDIF}
          BREAK;
        end;
      end; // for iJ end
    end; // for iI end
    RestoreScreenCursor;
    if bNeedToShowMonitoringWindow then
{$IFDEF IDBCOM}
      SELF.IDBLib.ShowMonitoringWindow(Integer(AProject));
{$ELSE}
      SELF.IDBLib.ShowMonitoringWindow(AProject, bAlwaysShowMonitoring);
{$ENDIF}
  except
  end;
end;

{ Set IDB manager in state for receiveng IDs of objects to be deleted. }

procedure TIDB.GetReadyToReceiveDeletedObjectID(AProject: PProj);
begin
   // At first I recognize whether a project is a symbol editor or not.
  if AProject.SymbolMode <> sym_Project then
    exit;
{$IFDEF IDBCOM}
  SELF.IDBLib.GetReadyToReceiveIDsOfObjectToBeDeleted_FromWG(Integer(AProject));
{$ELSE}
  SELF.IDBLib.GetReadyToReceiveIDsOfObjectToBeDeleted_FromWG(AProject);
{$ENDIF}
end;

{ Receiving IDs of objects to be deleted. }

procedure TIDB.ReceiveDeletedObjectID(AProject: PProj; ALayer: PLayer; AView: PView; AItem: PIndex);
begin
   // At first I recognize whether a project is a symbol editor or not.
  if AProject.SymbolMode <> sym_Project then
    exit;
{$IFDEF IDBCOM}
  SELF.IDBLib.ReceiveIDOfObjectToBeDeleted_FromWG(Integer(AProject), Integer(ALayer), AItem.Index);
{$ELSE}
  SELF.IDBLib.ReceiveIDOfObjectToBeDeleted_FromWG(AProject, ALayer, AItem.Index);
{$ENDIF}
end;

{ Execute deleting of objects. }

procedure TIDB.ExecuteDeleting(AProject: PProj; bShowProgressBarInMainWindow: Boolean = TRUE);
begin
   // At first I recognize whether a project is a symbol editor or not.
  if AProject.SymbolMode <> sym_Project then
    exit;

  StatusBar.ProgressPanel := TRUE;
   // 'Deleting from DB'
  StatusBar.ProgressText := MlgStringList['IDB', 3];

{$IFDEF IDBCOM}
  SELF.IDBLib.DeleteObjects_FromWG(Integer(AProject));
{$ELSE}
  SELF.IDBLib.DeleteObjects_FromWG(AProject);
{$ENDIF}
end;

{ This function detects the type of the object by object index.
  This function was gotten from AM_Index.pas of WinGIS. }

function TIDB.GetObjectTypeByItemIndex(iIndex: Integer): Integer;
begin
  if iIndex < mi_RText then
    RESULT := ot_Text
  else
    if iIndex < mi_BusGraph then
      RESULT := ot_RText
    else
      if iIndex < mi_MesLine then
        RESULT := ot_BusGraph
      else
        if iIndex < mi_Symbol then
          RESULT := ot_MesLine
        else
          if iIndex < mi_Circle then
            RESULT := ot_Symbol
          else
            if iIndex < mi_Arc then
              RESULT := ot_Circle
            else
              if iIndex < mi_Pixel then
                RESULT := ot_Arc
              else
                if iIndex < mi_Group then
                  RESULT := ot_Pixel
                else
                  if iIndex < mi_Spline then
                    RESULT := ot_Group
                  else
                    if iIndex < mi_Poly then
                      RESULT := ot_Spline
                    else
                      if iIndex < mi_CPoly then
                        RESULT := ot_Poly
                      else
                        if iIndex < mi_Image then
                          RESULT := ot_CPoly
                        else
                          if iIndex < mi_OleObj then
                            RESULT := ot_Image
                          else
                            RESULT := ot_OleObj;
end;

{function TIDB.AddObjectInUndoData (AProject: PProj; AView: PView): Boolean;
var
   SO: TIDB_OldMemoryStream;
   ibRealTypeOfObject: Byte;
begin
   RESULT := FALSE;
   // At first I recognize whether a project is a symbol editor or not.
   if AProject.SymbolMode <> sym_Project then exit;
   SO.Init (0, 0);
   ibRealTypeOfObject := AView.GetObjType;
   SO.Write (ibRealTypeOfObject, SizeOf (ibRealTypeOfObject));
   try
      SELF.AddInUndoData (TOldMemoryStream (SO), AView);
      SELF.IDBLib.PutDataInUndo (AProject, SO.GetDataStreamPointer, SO.GetSize);
   finally
      SO.Done;
   end;
   RESULT := TRUE;
end;}

{function TIDB.AddObjectInUndoData (AProject: PProj; ALayer: PLayer; AItem: PIndex): Boolean;
var
   SO: TIDB_OldMemoryStream;
   ibRealTypeOfObject: Byte;
begin
   RESULT := FALSE;
   // At first I recognize whether a project is a symbol editor or not.
   if AProject.SymbolMode <> sym_Project then exit;
   SO.Init (0, 0);
   ibRealTypeOfObject := AItem.GetObjType;
   SO.Write (ibRealTypeOfObject, SizeOf (ibRealTypeOfObject));
   try
      if AItem.MPtr = nil then ALayer.IndexObject (AProject.PInfo, AItem);
      SELF.AddInUndoData (TOldMemoryStream (SO), ALayer, AItem.MPtr, AItem);
      SELF.IDBLib.PutDataInUndo (AProject, SO.GetDataStreamPointer, SO.GetSize);
   finally
      SO.Done;
   end;
   RESULT := TRUE;
end;}

{ This function puts an index of new added object in the undo data of the IDB. This
  information can be used later when the user will want to do the undo operation.}

{function TIDB.AddObjectInUndoData (AProject: PProj; ALayer: PLayer; iItemIndex: Integer): Boolean;
var
   SO: TIDB_OLDMemoryStream;
   ibRealTypeOfObject: Byte;
begin
   RESULT := FALSE;
   if not WinGisMAinForm.WeAreUsingUndoFunction[AProject] then exit;
   // At first I recognize whether a project is a symbol editor or not.
   if AProject.SymbolMode <> sym_Project then exit;
   SO.Init (0, 0);
   ibRealTypeOfObject := GetObjectType (iItemIndex);
   SO.Write (ibRealTypeOfObject, SizeOf (ibRealTypeOfObject));
   try
      SELF.AddInUndoData (TOldMemoryStream (SO), ALayer, iItemIndex);
      SELF.IDBLib.PutDataInUndo (AProject, SO.GetDataStreamPointer, SO.GetSize);
   finally
      SO.Done;
   end;
   RESULT := TRUE;
end;

procedure TIDB.SaveUndoData (AProject: PProj; AnUndoType: TUndoType = utUnknownUNDO);
begin
   // This check was added by me because call from TIDB_ViewForm can't has an access to
   // WinGisMainForm... There I call this function but here I check a necessity to call it
   // indeed.
   if not WinGisMainForm.WeAreUsingUndoFunction[AProject] then exit;
   // At first I recognize whether a project is a symbol editor or not.
   if AProject.SymbolMode <> sym_Project then exit;
   SELF.IDBLib.SaveUndoData (AProject, AnUndoType);

   if AnUndoButton <> nil then
      AnUndoButton.Hint := SELF.IDBLib.GetNextUndoDescription (AProject);
end;}

procedure TIDB.SetSignOfMultiOperations(AProject: PProj; bValue: Boolean);
begin
   // At first I recognize whether a project is a symbol editor or not.
  if AProject.SymbolMode <> sym_Project then
    exit;
   {     GetProjectDefs(AProject, pPD);
        if pPD.IDB_UndoMan <> nil then
           pPD.IDB_UndoMan.SetMultiSign(bValue);}
end;

{procedure TIDB.DeleteSymbol (AProject: PProj; ASymbol: PSGroup);

   function GetSymLibInfo: TSymLibInfo;
   var
      iI: Integer;
      RollUp: TRollupForm;
   begin
      RESULT := nil;
      // Find RollupForm with symbols. All data about attached symbols libraries are contained in it.
      for iI := 0 to UserInterface.RollupCount - 1 do
      begin
         Rollup := UserInterface.Rollups[iI];
         if Rollup is TSymbolRollupForm then
         begin
            // Get information about attached symbol libraries.
            RESULT := TSymbolRollupForm (RollUp).GetSymLibInfo;
            exit;
         end;
      end; // for iI end
   end;

   function GenerateNewSymbolName (sSymbolLibraryName, sSymbolName: AnsiString): AnsiString;
   var
      iI: Integer;
      AProjectLibrarySymbols: PSymbols;
   begin
      RESULT := sSymbolName + ' - ' + sSymbolLibraryName;
      AProjectLibrarySymbols := GetSymLibInfo.GetLib (0);
      if AProjectLibrarySymbols = nil then exit;
      iI := 0;
      while AProjectLibrarySymbols.GetSymbol (RESULT, sSymbolLibraryName) <> nil do
      begin
         Inc (iI);
         RESULT := sSymbolName + '(' + IntToStr (iI) + ') - ' + sSymbolLibraryName;
      end;
   end;

var
   SO: TIDB_OldMemoryStream;
begin
   // At first I recognize whether a project is a symbol editor or not.
   if AProject.SymbolMode <> sym_Project then exit;
   if not WinGisMainForm.WeAreUsingUndoFunction[AProject] then exit;
   So.Init (0, 0);
   try
      SELF.IDBLib.GetReadyToReceiveUndoData (AProject);
      SELF.AddInUndoData (TOldMemoryStream (SO), ASymbol);
      SELF.IDBLib.PutDataInUndo (AProject, SO.GetDataStreamPointer, SO.GetSize);
      SELF.IDBLib.SaveUndoData (AProject, utUnDelete);
   finally
      SO.Done;
   end;
   SELF.IDBLib.AddSymbolMappingEntry (AProject, ASymbol.GetLibName, ASymbol.GetName, GenerateNewSymbolName (ASymbol.GetLibName, ASymbol.GetName));
end;}

{procedure TIDB.OnChangeUndoStepsCount (AProject: PProj; iNewUndoStepsCount: integer);
begin
   if AProject.SymbolMode <> sym_Project then exit;
   if not WinGisMainForm.WeAreUsingUndoFunction[AProject] then exit;
   SELF.IDBLib.OnChangeUndoStepsCount (AProject, iNewUndoStepsCount);
end;}

{procedure TIDB.AddSymbolMappingEntry (AProject: PProj; sSymbolLibraryName, sSymbolName, sNewSymbolName: AnsiString);
begin
   if AProject.SymbolMode <> sym_Project then exit;
   if not WinGisMainForm.WeAreUsingUndoFunction[AProject] then exit;
   SELF.IDBLib.AddSymbolMappingEntry (AProject, sSymbolLibraryName, sSymbolName, sNewSymbolName);
end;

function TIDB.SymbolNameInMappingExists (AProject: PProj; sSymbolLibraryName, sSymbolName: AnsiString): Boolean;
begin
   RESULT := FALSE;
   if AProject.SymbolMode <> sym_Project then exit;
   SELF.IDBLib.SymbolNameExists (AProject, sSymbolLibraryName, sSymbolName);
end;}

{Save databse file 'as' when project is saved 'as'}

procedure TIDB.SaveDatabaseAs(AProject: PProj; sNewProjectName: AnsiString);
begin
   // At first I recognize whether a project is a symbol editor or not.
  if AProject.SymbolMode <> sym_Project then
    exit;
{$IFDEF IDBCOM}
  SELF.IDBLib.SaveDatabaseAs(Integer(AProject), PChar(sNewProjectName));
{$ELSE}
  SELF.IDBLib.SaveDatabaseAs(AProject, sNewProjectName);
{$ENDIF}
end;

{ Insert new record in layer attribute table.
  if record without ProgisID value was found then ask user for select which from them must be used. }

procedure TIDB.InsertItem(AProject: PProj; ALayer: PLayer; ANewItem: PIndex);
begin
   // At first I recognize whether a project is a symbol editor or not.
  if AProject.SymbolMode <> sym_Project then
    exit;
   // Check type of new item.
  if not ItemHasSuitableType(ANewItem) then
    exit;
{$IFDEF IDBCOM}
  SELF.IDBLib.InsertItem(Integer(AProject), Integer(ALayer), ANewItem.Index);
{$ELSE}
  if Self <> nil then
    SELF.IDBLib.InsertItem(AProject, ALayer, ANewItem.Index);
{$ENDIF}
end;

// ++ Cadmensky

procedure TIDB.OverlayInsertItem(AProject: PProj; ALayer: PLayer; iNewItemIndex, iAItemIndex, iBItemIndex: integer);
begin
{$IFDEF IDBCOM}
  SELF.IDBLib.OverlayInsertItem(Integer(AProject), Integer(ALayer), iNewItemIndex, iAItemIndex, iBItemIndex);
{$ELSE}
  SELF.IDBLib.OverlayInsertItem(AProject, ALayer, iNewItemIndex, iAItemIndex, iBItemIndex);
{$ENDIF}
end;
// -- Cadmensky

// ++ Cadmensky Version 2.2.8

function TIDB.MoveAttributeInfo(AProject: PProj; ADestLayer: PLayer): Integer;
begin
   // At first I recognize whether a project is a symbol editor or not.
  if AProject.SymbolMode <> sym_Project then
    exit;
{$IFDEF IDBCOM}
  RESULT := SELF.IDBLib.MoveAttributeInfo(Integer(AProject), Integer(ADestLayer));
{$ELSE}
  RESULT := SELF.IDBLib.MoveAttributeInfo(AProject, ADestLayer);
{$ENDIF}
end;
// -- Cadmensky Version 2.2.8

function TIDB.CopyAttributeInfo(AProject: PProj; ASourceLayer, ADestLayer: PLayer; iSourceItemID, iDestItemID: Integer): integer;
begin
   // At first I recognize whether a project is a symbol editor or not.
  if AProject.SymbolMode <> sym_Project then
    exit;
{$IFDEF IDBCOM}
  RESULT := SELF.IDBLib.CopyAttributeInfo(Integer(AProject), Integer(ASourceLayer), Integer(ADestLayer), iSourceItemID, iDestItemID);
{$ELSE}
  RESULT := SELF.IDBLib.CopyAttributeInfo(AProject, ASourceLayer, ADestLayer, iSourceItemID, iDestItemID);
{$ENDIF}
end;

{ This procedure is called from IDB. The cause of its calling is receiving by TMDIChild window
  the message from IDB. }

procedure TIDB.DeleteObjectsInProject(AProject: PProj; ALayer: PLayer);
var
  iItemIndex: Integer;
  ALayerItem: PIndex;
begin
   // At first I recognize whether a project is a symbol editor or not.
  if AProject.SymbolMode <> sym_Project then
    exit;
   {   iItemIndex := SELF.IDBLib.GetItemIDToBeDeleted_FromIDB (AProject, ALayer);
      if WinGISMainForm.WeAreUsingUndoFunction[AProject] then
         SELF.IDBLib.GetReadyToReceiveUndoData (AProject);
      while iItemIndex > -1 do
      begin
         ALayerItem := ALayer.HasIndexObject (iItemIndex);
   //       During deleteing all objects will be put into UNDO data of IDB.
         if ALayerItem <> nil then
            try // Cadmensky
               ALayer^.DeleteIndex (AProject^.PInfo, ALayerItem);
            except // Cadmensky
            end; // Cadmensky
         iItemIndex := SELF.IDBLib.GetItemIDToBeDeleted_FromIDB (AProject, ALayer);
      end; // while end
      if WinGISMainForm.WeAreUsingUndoFunction[AProject] then SELF.SaveUndoData (AProject, utUnDelete);}
{$IFNDEF AXDLL} // <----------------- AXDLL
  AProject^.DeleteSelected(true);
  RedrawProjectWindow(AProject);
{$ENDIF}
end;

{Deleting all layer's tables.}

{procedure TIDB.DeleteLayerTables (AProject: PProj; ALayer: PLayer);
var
   SO: TIDB_OldMemoryStream;
begin
   // At first I recognize whether a project is a symbol editor or not.
   if AProject.SymbolMode <> sym_Project then exit;

   if (WinGisMainForm.WeAreUsingUndoFunction[AProject]) then
   begin
      SO.Init (0, 0);
      try
         SELF.AddInUndoData (TOldMemoryStream (SO), AProject, ALayer);
         SELF.IDBLib.PutDataInUndo (AProject, SO.GetDataStreamPointer, SO.GetSize);
      finally
         SO.Done;
      end;
      SELF.SaveUndoData (AProject, utUnDelete);
   end;
end;}

{ When a project was just created, was never saved and is closing, it is necessary
  to delete files of both project and database. }

procedure TIDB.DeleteProjectAndDatabaseFiles(sProjectFileName: AnsiString);
begin
{$IFDEF IDBCOM}
  SELF.IDBLib.DeleteProjectAndDatabaseFiles(PChar(sProjectFileName));
{$ELSE}
  SELF.IDBLib.DeleteProjectAndDatabaseFiles(sProjectFileName);
{$ENDIF}
end;

{ This procedure sets the sing that means EXTERNAL database is working (or any similar
  through DDE-Command tract). In this case I haven't necessity in the using of data in
  the pattern table. }

procedure TIDB.SetDBIsWorkingSign(AProject: PProj; bValue: Boolean);
begin
{$IFNDEF IDBCOM}
   // At first I recognize whether a project is a symbol editor or not.
  if AProject.SymbolMode <> sym_Project then
    exit;
  SELF.IDBLib.Set_AnExternalDBIsWorking_Sign(AProject, bValue);
{$ENDIF}
end;

{ This functions returns a list of names of field's names. It is used in Create Tooltips routines. }

function TIDB.GetTableFieldsNamesList(AProject: PProj; ALayer: PLayer): AnsiString;
begin
{$IFDEF IDBCOM}
  RESULT := SELF.IDBLib.GetTableFieldsNamesList(Integer(AProject), Integer(ALayer));
{$ELSE}
  RESULT := SELF.IDBLib.GetTableFieldsNamesList(AProject, ALayer);
{$ENDIF}
end;

function TIDB.GetTableVisibleFieldsNamesList(AProject: PProj; ALayer: PLayer): AnsiString;
begin
{$IFDEF IDBCOM}
  RESULT := SELF.IDBLib.GetTableVisibleFieldsNamesList(Integer(AProject), Integer(ALayer));
{$ELSE}
  RESULT := SELF.IDBLib.GetTableVisibleFieldsNamesList(AProject, ALayer);
{$ENDIF}
end;

function TIDB.GetGeoTextIDFieldName(AProject: PProj; ALayer: PLayer): AnsiString;
begin
{$IFDEF IDBCOM}
  RESULT := SELF.IDBLib.GetGeoTextIDFieldName(Integer(AProject), Integer(ALayer));
{$ELSE}
  RESULT := SELF.IDBLib.GetGeoTextIDFieldName(AProject, ALayer);
{$ENDIF}
end;

function TIDB.GetGeoTextFieldName(AProject: PProj; ALayer: PLayer): AnsiString;
begin
{$IFDEF IDBCOM}
  RESULT := SELF.IDBLib.GetGeoTextFieldName(Integer(AProject), Integer(ALayer));
{$ELSE}
  RESULT := SELF.IDBLib.GetGeoTextFieldName(AProject, ALayer);
{$ENDIF}
end;

procedure TIDB.SetGeoTextFieldNames(AProject: PProj; ALayer: PLayer; sGeoTextIDFieldName, sGeoTextFieldName: AnsiString);
begin
{$IFDEF IDBCOM}
  SELF.IDBLib.SetGeoTextFieldNames(Integer(AProject), Integer(ALayer), PChar(sGeoTextIDFieldName), PChar(sGeotextFieldName));
{$ELSE}
  SELF.IDBLib.SetGeoTextFieldNames(AProject, ALayer, sGeoTextIDFieldName, sGeotextFieldName);
{$ENDIF}
end;

function TIDB.GetGeoText(AProject: PProj; iLayerIndex, iItemID: Integer {; sGeoTextIDFieldName, sGeoTextValueFieldName: AnsiString}): AnsiString;
begin
  try
{$IFDEF IDBCOM}
    RESULT := SELF.IDBLib.GetGeoText(Integer(AProject), iLayerIndex, iItemID);
{$ELSE}
    if Self <> nil then
      RESULT := SELF.IDBLib.GetGeoText(AProject, iLayerIndex, iItemID {, sGeoTextIDFieldName, sGeoTextValueFieldName});
{$ENDIF}
  except
  end;
end;

procedure TIDB.SetGeoText(AProject: PProj; iLayerIndex, iItemID: Integer; {sGeoTextIDFieldName, sGeoTextValueFieldName,} sGeoTextValue: AnsiString);
begin
{$IFDEF IDBCOM}
  SELF.IDBLib.SetGeoText(Integer(AProject), iLayerIndex, iItemID, PChar(sGeoTextValue));
{$ELSE}
  if Self <> nil then
    SELF.IDBLib.SetGeoText(AProject, iLayerIndex, iItemID, { sGeoTextIDFieldName, sGeoTextValueFieldName,} sGeoTextValue);
{$ENDIF}
end;

{ When a project is saving, I mark all added records as surely exists
  and delete all records that were marked as deleted once. }

procedure TIDB.ProjectIsSaving(AProject: PProj);
begin
{$IFDEF IDBCOM}
  SELF.IDBLib.ProjectIsBeingSaved(Integer(AProject));
{$ELSE}
  if Self <> nil then
    SELF.IDBLib.ProjectIsBeingSaved(AProject);
{$ENDIF}
  if AnUndoButton <> nil then
    AnUndoButton.Hint := '';
  if ARedoButton <> nil then
    ARedoButton.Hint := '';
end;

{ This function produces annotation objects in WinGIS project but it does them not by requirement of
  the IDB user interface but by requirement of WinGIS interface. A ' Database\Label objects' menu items
  of WinGIS has been clicked by the user.}

procedure TIDB.MakeAnnotations(AProject: PProj);
var
  iI, iJ, iItemID: Integer;
  ALayer: PLayer;
  ALayerItem: PIndex;
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
   // Check - may be a project isn't a normal project.
  if AProject.SymbolMode <> sym_Project then
    exit;
   // Check whether in the project there are selected items or not.
  if AProject.Layers.SelLayer.Data.GetCount < 1 then
    exit;
{$IFDEF IDBCOM}
  SELF.IDBLib.GetReadyToReceiveIDsOfObjectsToBeMade(Integer(AProject));
{$ELSE}
  SELF.IDBLib.GetReadyToReceiveIDsOfObjectsToBeMade(AProject);
{$ENDIF}
  if WinGISMainForm.WeAreUsingUndoFunction[AProject] then
    WinGISMainForm.UndoRedo_Man.GetReadyToReceiveUndoData(AProject);
   // Fill an array with selected items IDs and pointers to layers that contains these selected items.
  for iI := 0 to AProject.Layers.SelLayer.Data.GetCount - 1 do
  begin
    iItemID := PIndex(AProject.Layers.SelLayer.Data.At(iI)).Index;
    for iJ := 1 to AProject.Layers.LData.Count - 1 do
    begin
      ALayer := AProject.Layers.LData.At(iJ);
      ALayerItem := ALayer.HasIndexObject(iItemID);
      if (ALayerItem <> nil) and
        (Boolean(ALayerItem.State and sf_Selected)) then
      begin
{$IFDEF IDBCOM}
        SELF.IDBLib.ReceiveIDOfObjectToBeMade(Integer(AProject), Integer(ALayer), iItemID);
{$ELSE}
        SELF.IDBLib.ReceiveIDOfObjectToBeMade(AProject, ALayer, iItemID);
{$ENDIF}
        BREAK;
      end;
    end; // for iJ end
  end; // for iI end
  if WinGISMainForm.WeAreUsingUndoFunction[AProject] then
    WinGISMainForm.UndoRedo_Man.SetNecessitySaveUndoDataSign(AProject, false);
{$IFDEF IDBCOM}
  SELF.IDBLib.MakeAnnotations(Integer(AProject));
{$ELSE}
  SELF.IDBLib.MakeAnnotations(AProject);
{$ENDIF}
  if WinGISMainForm.WeAreUsingUndoFunction[AProject] then
  begin
    WinGISMainForm.UndoRedo_Man.SetNecessitySaveUndoDataSign(AProject, true);
    WinGISMainForm.UndoRedo_Man.SaveUndoData(AProject, utUnInsert);
  end;
{$ENDIF}
end;

procedure TIDB.UpdateAnnotationData(AProject: Pointer; iLayerIndex, iItemID: Integer; AnAnnotData: PCollection);
begin
{$IFDEF IDBCOM}
  SELF.IDBLib.UpdateAnnotationData(Integer(AProject), iLayerIndex, iItemID, Integer(AnAnnotData));
{$ELSE}
  SELF.IDBLib.UpdateAnnotationData(AProject, iLayerIndex, iItemID, AnAnnotData);
{$ENDIF}
end;
{**brovak}

procedure TIDB.MakeChart(AProject: PProj);
var
  iI, iJ, iItemID: Integer;
  ALayer: PLayer;
  ALayerItem: PIndex;
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
   // Check - may be a project isn't a normal project.
  if AProject.SymbolMode <> sym_Project then
    exit;
   // Check whether in the project there are selected items or not.
  if AProject.Layers.SelLayer.Data.GetCount < 1 then
    exit;
{$IFDEF IDBCOM}
  SELF.IDBLib.GetReadyToReceiveIDsOfObjectsToBeMade(Integer(AProject));
{$ELSE}
  SELF.IDBLib.GetReadyToReceiveIDsOfObjectsToBeMade(AProject);
{$ENDIF}
  if WinGISMainForm.WeAreUsingUndoFunction[AProject] then
    WinGISMainForm.UndoRedo_Man.GetReadyToReceiveUndoData(AProject);
   // Fill an array with selected items IDs and pointers to layers that contains these selected items.
  for iI := 0 to AProject.Layers.SelLayer.Data.GetCount - 1 do
  begin
    iItemID := PIndex(AProject.Layers.SelLayer.Data.At(iI)).Index;
    for iJ := 1 to AProject.Layers.LData.Count - 1 do
    begin
      ALayer := AProject.Layers.LData.At(iJ);
      ALayerItem := ALayer.HasIndexObject(iItemID);
      if (ALayerItem <> nil)
        and
        (Boolean(ALayerItem.State and sf_Selected)) then
      begin
{$IFDEF IDBCOM}
        SELF.IDBLib.ReceiveIDOfObjectToBeMade(Integer(AProject), Integer(ALayer), iItemID);
{$ELSE}
        SELF.IDBLib.ReceiveIDOfObjectToBeMade(AProject, ALayer, iItemID);
{$ENDIF}
        BREAK;
      end;
    end; // for iJ end
  end; // for iI end
{$IFDEF IDBCOM}
  SELF.IDBLib.MakeChart(Integer(AProject));
{$ELSE}
  SELF.IDBLib.MakeChart(AProject);
{$ENDIF}
  if WinGISMainForm.WeAreUsingUndoFunction[AProject] then
    WinGISMainForm.UndoRedo_Man.SaveUndoData(AProject, utUnInsert);
{$ENDIF}
end;

procedure TIDB.UpdateChartData(AProject: Pointer; iLayerIndex, iItemID: Integer; AnChartData: PCollection);
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFDEF IDBCOM}
  SELF.IDBLib.UpdateChartData(Integer(AProject), iLayerIndex, iItemID, Integer(AnChartData));
{$ELSE}
  SELF.IDBLib.UpdateChartData(AProject, iLayerIndex, iItemID, AnChartData);
{$ENDIF}
{$ENDIF}
end;

{brovak}

// ++ Commented by Cadmensky IDB Version 2.3.0
{procedure TIDB.GetReadyToReceiveDroppedIDs (AProject: PProj);
begin
   // At first I recognize whether a project is a symbol editor or not.
   if AProject.SymbolMode <> sym_Project then exit;
   SELF.IDBLib.GetReadyToReceiveIDsOfObjectsToBeUsedForDragAndDrop (AProject);
end;

procedure TIDB.DropDataFromTable (AProject: PProj; ALayer: PLayer; iIDOfItemWhichWasBeenDropped: Integer; ASourceFormOfDragging: TWinControl);
begin
   // At first I recognize whether a project is a symbol editor or not.
   if AProject.SymbolMode <> sym_Project then exit;
   SELF.IDBLib.ReceiveIDOfObjectToBeUsedForDragAndDrop (AProject, ALayer, iIDOfItemWhichWasBeenDropped);
end;

procedure TIDB.ExecuteDragAndDrop (AProject: PProj; ASourceGridOfDragging: TObject);
begin
   // At first I recognize whether a project is a symbol editor or not.
   if AProject.SymbolMode <> sym_Project then exit;
   SELF.IDBLib.ExecuteDragAndDrop (AProject, ASourceGridOfDragging);
   SELF.IDBLib.RefreshAllDataWindows (AProject);
end;}
// -- Commented by Cadmensky IDB Version 2.3.0

{ This is the interface that allows to an external object to get an access to attribute data. }

function TIDB.X_GotoObjectRecord(AProject: PProj; iLayerIndex: Integer; iItemIndex: Integer): Boolean;
var
  ALayer: PLayer;
begin
  RESULT := FALSE;
   // At first I recognize whether a project is a symbol editor or not.
  if AProject.SymbolMode <> sym_Project then
    exit;
  ALayer := AProject.Layers.IndexToLayer(iLayerIndex);
  if ALayer = nil then
    exit;
{$IFDEF IDBCOM}
  RESULT := SELF.IDBLib.X_GotoObjectRecord(Integer(AProject), Integer(ALayer), iItemIndex) > 0;
{$ELSE}
  RESULT := SELF.IDBLib.X_GotoObjectRecord(AProject, ALayer, iItemIndex);
{$ENDIF}
end;

function TIDB.X_AddRecord(AProject: PProj; iLayerIndex: Integer; iItemIndex: Integer): Boolean;
var
  ALayer: PLayer;
begin
  RESULT := FALSE;
   // At first I recognize whether a project is a symbol editor or not.
  if AProject.SymbolMode <> sym_Project then
    exit;
  ALayer := AProject.Layers.IndexToLayer(iLayerIndex);
  if ALayer = nil then
    exit;
{$IFDEF IDBCOM}
  RESULT := SELF.IDBLib.X_AddRecord(Integer(AProject), Integer(ALayer), iItemIndex) > 0;
{$ELSE}
  RESULT := SELF.IDBLib.X_AddRecord(AProject, ALayer, iItemIndex);
{$ENDIF}
end;

function TIDB.X_GetFieldCount(AProject: PProj; iLayerIndex: Integer): Integer;
var
  ALayer: PLayer;
begin
  RESULT := 0;
   // At first I recognize whether a project is a symbol editor or not.
  if AProject.SymbolMode <> sym_Project then
    exit;
  ALayer := AProject.Layers.IndexToLayer(iLayerIndex);
  if ALayer = nil then
    exit;
{$IFDEF IDBCOM}
  RESULT := SELF.IDBLib.X_GetFieldCount(Integer(AProject), Integer(ALayer));
{$ELSE}
  RESULT := SELF.IDBLib.X_GetFieldCount(AProject, ALayer);
{$ENDIF}
end;

function TIDB.X_GetFieldName(AProject: PProj; iLayerIndex: Integer; iFieldIndex: Integer): AnsiString;
var
  ALayer: PLayer;
begin
  RESULT := 'ProgisID';
   // At first I recognize whether a project is a symbol editor or not.
  if AProject.SymbolMode <> sym_Project then
    exit;
  ALayer := AProject.Layers.IndexToLayer(iLayerIndex);
  if ALayer = nil then
    exit;
{$IFDEF IDBCOM}
  RESULT := SELF.IDBLib.X_GetFieldName(Integer(AProject), Integer(ALayer), iFieldIndex);
{$ELSE}
  RESULT := SELF.IDBLib.X_GetFieldName(AProject, ALayer, iFieldIndex);
{$ENDIF}
end;

function TIDB.X_GetFieldValue(AProject: PProj; iLayerIndex: Integer; sFieldName: AnsiString): variant;
var
  ALayer: PLayer;
begin
  RESULT := 0;
   // At first I recognize whether a project is a symbol editor or not.
  if AProject.SymbolMode <> sym_Project then
    exit;
  ALayer := AProject.Layers.IndexToLayer(iLayerIndex);
  if ALayer = nil then
    exit;
{$IFDEF IDBCOM}
  RESULT := SELF.IDBLib.X_GetFieldValueByFieldName(Integer(AProject), Integer(ALayer), PChar(sFieldName));
{$ELSE}
  RESULT := SELF.IDBLib.X_GetFieldValueByFieldName(AProject, ALayer, sFieldName);
{$ENDIF}
end;

function TIDB.X_GetFieldValue(AProject: PProj; iLayerIndex: Integer; iFieldIndex: Integer): variant;
var
  ALayer: PLayer;
begin
  RESULT := 0;
   // At first I recognize whether a project is a symbol editor or not.
  if AProject.SymbolMode <> sym_Project then
    exit;
  ALayer := AProject.Layers.IndexToLayer(iLayerIndex);
  if ALayer = nil then
    exit;
{$IFDEF IDBCOM}
  RESULT := SELF.IDBLib.X_GetFieldValueByFieldIndex(Integer(AProject), Integer(ALayer), iFieldIndex);
{$ELSE}
  RESULT := SELF.IDBLib.X_GetFieldValueByFieldIndex(AProject, ALayer, iFieldIndex);
{$ENDIF}
end;

procedure TIDB.X_SetFieldValue(AProject: PProj; iLayerIndex: Integer; sFieldName: AnsiString; FieldValue: Variant);
var
  ALayer: PLayer;
begin
   // At first I recognize whether a project is a symbol editor or not.
  if AProject.SymbolMode <> sym_Project then
    exit;
  ALayer := AProject.Layers.IndexToLayer(iLayerIndex);
  if ALayer = nil then
    exit;
{$IFDEF IDBCOM}
  SELF.IDBLib.X_SetFieldValueByFieldName(Integer(AProject), Integer(ALayer), PChar(sFieldName), FieldValue);
{$ELSE}
  SELF.IDBLib.X_SetFieldValueByFieldName(AProject, ALayer, sFieldName, FieldValue);
{$ENDIF}
end;

procedure TIDB.X_SetFieldValue(AProject: PProj; iLayerIndex: Integer; iFieldIndex: Integer; FieldValue: variant);
var
  ALayer: PLayer;
begin
   // At first I recognize whether a project is a symbol editor or not.
  if AProject.SymbolMode <> sym_Project then
    exit;
  ALayer := AProject.Layers.IndexToLayer(iLayerIndex);
  if ALayer = nil then
    exit;
{$IFDEF IDBCOM}
  SELF.IDBLib.X_SetFieldValueByFieldIndex(Integer(AProject), Integer(ALayer), iFieldIndex, FieldValue);
{$ELSE}
  SELF.IDBLib.X_SetFieldValueByFieldIndex(AProject, ALayer, iFieldIndex, FieldValue);
{$ENDIF}
end;

function TIDB.LoadGeoTextData(AProject, ALayer: Pointer; sSourceDBFileName, sSourceFieldName, sDestinationFieldName, sProgisIDFieldName: AnsiString): Boolean;
begin
{$IFDEF IDBCOM}
  RESULT := SELF.IDBLib.LoadGeoTextData(Integer(AProject),
    Integer(ALayer),
    PChar(sSourceDBFileName),
    PChar(sSourceFieldName),
    PChar(sDestinationFieldName),
    PChar(sProgisIDFieldName)) > 0;
{$ELSE}
  RESULT := SELF.IDBLib.LoadGeoTextData(AProject, ALayer, sSourceDBFileName, sSourceFieldName, sDestinationFieldName, sProgisIDFieldName);
{$ENDIF}
end;

function TIDB.GetActiveWindowProject: Pointer;
begin
{$IFDEF IDBCOM}
  RESULT := Pointer(SELF.IDBLib.GetActiveWindowProject);
{$ELSE}
  RESULT := SELF.IDBLib.GetActiveWindowProject;
{$ENDIF}
end;

{$IFNDEF IDBCOM}

// ++ Commented by Cadmensky IDB Version 2.2.8
{function TIDB.GetReadyToInsertRecords (AProject, ALayer: Pointer; ARecordTemplate: TList): Integer;
begin
   RESULT := SELF.IDBLib.GetReadyToInsertRecords (AProject, ALayer, ARecordTemplate);
end;

function TIDB.InsertRecord (AProject, ALayer: Pointer; iLayerItemID: Integer): Boolean;
begin
   RESULT := SELF.IDBLib.InsertRecord (AProject, ALayer, iLayerItemID);
end;

procedure TIDB.EndInserting (AProject: PProj; ALayer: PLayer);
begin
   SELF.IDBLib.EndInserting (AProject, ALayer);
end;}
// -- Commented by Cadmensky IDB Version 2.2.8
{$ENDIF}

function TIDB.TheUserWantsToCancel(AWindow: TForm): Boolean;
begin
{$IFDEF IDBCOM}
  RESULT := SELF.IDBLib.TheUserWantsToCancel(Integer(AWindow)) > 0;
{$ELSE}
  RESULT := SELF.IDBLib.TheUserWantsToCancel(AWindow);
{$ENDIF}
end;

function TIDB.GetReadyToPolygonOverlaying(AProject, ASourceLayer1, ASourceLayer2, ADestinationLayer: Pointer): Boolean;
begin
{$IFDEF IDBCOM}
  RESULT := SELF.IDBLib.GetReadyToPolygonOverlaying(Integer(AProject),
    Integer(ASourceLayer1),
    Integer(ASourceLayer2),
    Integer(ADestinationLayer)) > 0;
{$ELSE}
  RESULT := SELF.IDBLib.GetReadyToPolygonOverlaying(AProject, ASourceLayer1, ASourceLayer2, ADestinationLayer);
{$ENDIF}
end;

procedure TIDB.EndOfPolygonOverlaying(AProject: Pointer);
begin
{$IFDEF IDBCOM}
  SELF.IDBLib.EndOfPolygonOverlaying(Integer(AProject));
{$ELSE}
  SELF.IDBLib.EndOfPolygonOverlaying(AProject);
{$ENDIF}
end;

// ++ Cadmensky

procedure TIDB.UpdateCalcFields(AProject: PProj; ALayer: PLayer; iItemID: integer);
begin
{$IFDEF IDBCOM}
  SELF.IDBLib.UpdateCalcFields(Integer(AProject), Integer(ALayer), iItemID);
{$ELSE}
  SELF.IDBLib.UpdateCalcFields(AProject, ALayer, iItemID);
{$ENDIF}
end;

procedure TIDB.ShowRecordsForSelectedObjects(AProject: PProj);
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  if Self <> nil then
  begin
    if not WinGISMainForm.WeAreUsingTheIDBInfoWindow then
{$IFDEF IDBCOM}
      SELF.IDBLib.ShowRecordsForSelectedObjects(Integer(AProject));
{$ELSE}
      SELF.IDBLib.ShowRecordsForSelectedObjects(AProject);
{$ENDIF}
  end;
{$ENDIF}
end;

function TIDB.GetReadyToReceiveIDsOfObjectToBeDeleted_FromWG(AProject: Pointer): Boolean;
begin
{$IFDEF IDBCOM}
  RESULT := SELF.IDBLib.GetReadyToReceiveIDsOfObjectToBeDeleted_FromWG(Integer(AProject)) > 0;
{$ELSE}
  RESULT := SELF.IDBLib.GetReadyToReceiveIDsOfObjectToBeDeleted_FromWG(AProject);
{$ENDIF}
end;

function TIDB.GetReadyToReceiveIDsOfObjectsToBeRestored(AProject: Pointer): Boolean;
begin
{$IFDEF IDBCOM}
  RESULT := SELF.IDBLib.GetReadyToReceiveIDsOfObjectsToBeRestored(Integer(AProject)) > 0;
{$ELSE}
  RESULT := SELF.IDBLib.GetReadyToReceiveIDsOfObjectsToBeRestored(AProject);
{$ENDIF}
end;

procedure TIDB.ReceiveIDOfObjectToBeRestored(AProject: Pointer; ALayer: Pointer; iObjectID: Integer);
begin
{$IFDEF IDBCOM}
  SELF.IDBLib.ReceiveIDOfObjectToBeRestored(Integer(AProject), Integer(ALayer), iObjectID);
{$ELSE}
  SELF.IDBLib.ReceiveIDOfObjectToBeRestored(AProject, ALayer, iObjectID);
{$ENDIF}
end;

procedure TIDB.ExecuteRestoring(AProject: Pointer);
begin
{$IFDEF IDBCOM}
  SELF.IDBLib.ExecuteRestoring(Integer(AProject));
{$ELSE}
  SELF.IDBLib.ExecuteRestoring(AProject);
{$ENDIF}
end;

procedure TIDB.DeleteObjects_FromWG(AProject: Pointer);
begin
{$IFDEF IDBCOM}
  SELF.IDBLib.DeleteObjects_FromWG(Integer(AProject));
{$ELSE}
  SELF.IDBLib.DeleteObjects_FromWG(AProject);
{$ENDIF}
end;

// -- Cadmensky

end.

