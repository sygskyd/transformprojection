unit PPP_decl;
interface
Uses WinTypes,
     WinProcs;

Type PBool     = ^WordBool;
     PBoolean  = ^Boolean;
     PByte     = ^Byte;
     PWord     = ^Word;
     PShortInt = ^ShortInt;
     PInteger  = ^Integer;
     PLongInt  = ^LongInt;
     PSingle   = ^Single;
     PDouble   = ^Double;

     HGlobal                 =  THandle;
     PRGBTriple              = ^TRGBTriple;
     PRGBQuad                = ^TRGBQuad;
     PMenuItemTemplateHeader = ^TMenuItemTemplateHeader;
     PMenuItemTemplate       = ^TMenuItemTemplate;
     PMultiKeyHelp           = ^TMultiKeyHelp;

{+// global flag error values */ }
Const PP_NO_ERROR = $00;
Const WRONG_CF_SERIAL_NUMBER = $01;
Const CANNOT_OPEN_FILE = $02;
Const CANNOT_WRITE_FILE = $03;
Const CANNOT_READ_FILE = $04;
Const CANNOT_CLOSE_FILE = $05;
Const CORRUPT_CONTROL_FILE = $06;
Const WRONG_CF_VERSION = $07;
Const INCORRECT_PARAMETERS = $08;
Const UNSUPPORTED_FEATURE = $09;
Const CANNOT_HARD_CODE_SERIAL = $0A;
Const WRONG_CPF_VERSION = $0B;
Const COPY_PROT_TEST_FAILED = $0C;
Const CANNOT_CHANGE_ATTRIBS = $0D;

{+// global defines */ }
Const EXTERNAL_CONTROL_FILE = 1;
Const INTERNAL_CONTROL_FILE = 2;
Const SERIAL_DEFAULT = 0;
Const SERIAL_CONTROLFILE = 1;
Const SERIAL_HARDCODED = 2;
Const PP_SUCCESS = 1;
Const PP_FAILURE = 0;

{+// file attributes defines - used in copy protection functions */ }
Const PP_NORMAL = $00;
Const PP_RDONLY = $01;
Const PP_HIDDEN = $02;
Const PP_SYSTEM = $04;

{+// copy protection and computer number defines */ }
Const COPY_EXT_SINGLE = 1;
Const COPY_EXT_MULTIPLE = 2;
Const COPY_INTERNAL = 3;
Const COMPNO_BIOS = $0001;
Const COMPNO_HDSERIAL = $0002;
Const COMPNO_HDLOCK = $0004;
Const COMPNO_HDTYPE = $0008;
Const COMPNO_NETNAME = $0010;

{+// global defines for expiration types */ }
Const NO_EXPIRATION = 'N';
Const EXE_LIMIT = 'E';
Const SHAREWARE_VER = 'S';
Const PAYMENT_LIMIT = 'P';
Const DEMO_VERSION = 'D';

{+// the following are the set/get variable numbers for pp_set* and pp_get* */ }

Const VAR_COMPANY = 1;
Const VAR_NAME = 2;
Const VAR_ADDRESS1 = 3;
Const VAR_ADDRESS2 = 4;
Const VAR_CITY = 5;
Const VAR_STATE = 6;
Const VAR_ZIP = 7;
Const VAR_PHONE = 8;
Const VAR_PRODUCT = 9;
Const VAR_SERIAL = 10;
Const VAR_DISTRIBUTOR = 11;
Const VAR_PP_COMPANY = 12;
Const VAR_PP_ADDRESS1 = 13;
Const VAR_PP_ADDRESS2 = 14;
Const VAR_PP_ADDRESS3 = 15;
Const VAR_PP_PHONE = 16;
Const VAR_PP_FAX = 17;
Const VAR_LAST_DATE = 18;
Const VAR_LAST_TIME = 19;
Const VAR_EXPIRE_DATE = 20;
Const VAR_EXPIRE_TYPE = 21;
Const VAR_EXE_LIMIT = 22;
Const VAR_EXE_COUNT = 23;
Const VAR_LAN_LIMIT = 24;
Const VAR_LAN_COUNT = 25;
Const VAR_USER_DEF_1 = 26;
Const VAR_USER_DEF_2 = 27;
Const VAR_USER_DEF_3 = 28;
Const VAR_USER_DEF_4 = 29;

procedure pp_adddays(_1: PInteger; _2: PInteger; _3: PInteger; _4: Integer); stdcall;
function pp_advect: Bool; stdcall;
procedure pp_cedate(_1: LongInt; _2: PInteger; _3: PInteger; _4: PInteger); stdcall;
function pp_cenum: LongInt; stdcall;
function pp_ctcodes(_1: Integer; _2: LongInt; _3: LongInt): LongInt; stdcall;
procedure pp_clearbit(_1: PLongInt; _2: Integer); stdcall;
function pp_compno(_1: LongInt; _2: PChar; _3: Integer): LongInt; stdcall;
function pp_copycheck(_1: PChar; _2: Integer; _3: LongInt): Bool; stdcall;
function pp_copywrite(_1: PChar; _2: Integer; _3: LongInt; _4: Integer): Bool; stdcall;
procedure pp_decrypt(_1: PChar; _2: PChar; _3: PChar); stdcall;
procedure pp_encrypt(_1: PChar; _2: PChar; _3: PChar); stdcall;
procedure pp_errorstr(_1: PChar); stdcall;
function pp_errornum: Integer; stdcall;
function pp_exect: Integer; stdcall;
function pp_exelimit: Integer; stdcall;
function pp_expired: Bool; stdcall;
procedure pp_expdate(_1: PInteger; _2: PInteger; _3: PInteger); stdcall;
function pp_exptype: Integer; stdcall;
procedure pp_getdate(_1: PInteger; _2: PInteger; _3: PInteger); stdcall;
function pp_getregchar(_1: PChar; _2: PChar): Bool; stdcall;
procedure pp_gettime(_1: PInteger; _2: PInteger; _3: PInteger; _4: PInteger); stdcall;
procedure pp_getvarchar(_1: Integer; _2: PChar); stdcall;
procedure pp_getvardate(_1: Integer; _2: PInteger; _3: PInteger; _4: PInteger); stdcall;
function pp_getvarnum(_1: Integer): LongInt; stdcall;
function pp_hdlockwr(_1: PChar; _2: Integer): Bool; stdcall;
function pp_hdserial(_1: Integer): LongInt; stdcall;
function pp_initcf(_1: PChar): Bool; stdcall;
function pp_killprot(_1: PChar): Bool; stdcall;
function pp_lanactive: Bool; stdcall;
function pp_lancheck: Bool; stdcall;
function pp_landecr(_1: Integer): Bool; stdcall;
function pp_lanincr(_1: Integer): Bool; stdcall;
function pp_lanlimit: Integer; stdcall;
function pp_lanminus(_1: Integer): Bool; stdcall;
function pp_lanplus(_1: Integer): Bool; stdcall;
function pp_lanusers: Integer; stdcall;
procedure pp_lastday(_1: Integer; _2: PInteger; _3: Integer); stdcall;
procedure pp_netvolume(_1: Integer); stdcall;
function pp_nexect(_1: Integer): Bool; stdcall;
function pp_nexelim(_1: Integer): Bool; stdcall;
function pp_nexptype(_1: Integer; _2: Integer; _3: Integer; _4: Integer): Bool; stdcall;
function pp_nlanct(_1: Integer): Bool; stdcall;
function pp_nlanlim(_1: Integer): Bool; stdcall;
procedure pp_npdate(_1: Integer; _2: PInteger; _3: Integer; _4: Integer); stdcall;
function pp_password(_1: PChar): LongInt; stdcall;
function pp_redir(_1: Integer): Bool; stdcall;
procedure pp_serial(_1: PChar); stdcall;
procedure pp_setbit(_1: PLongInt; _2: Integer); stdcall;
function pp_setregchar(_1: PChar; _2: PChar): Bool; stdcall;
procedure pp_setvarchar(_1: Integer; _2: PChar); stdcall;
procedure pp_setvardate(_1: Integer; _2: Integer; _3: Integer; _4: Integer); stdcall;
procedure pp_setvarnum(_1: Integer; _2: LongInt); stdcall;
function pp_testbit(_1: LongInt; _2: Integer): Bool; stdcall;
function pp_ucode(_1: LongInt; _2: LongInt; _3: LongInt): Integer; stdcall;
function pp_unlock: Bool; stdcall;
function pp_upddate: Bool; stdcall;
function pp_valdate: Bool; stdcall;
function pp_writecf(_1: PChar): Bool; stdcall;

implementation

Const DLLName = 'WinLoc32.DLL';

procedure pp_adddays; external DLLName;
function pp_advect: Bool; external DLLName;
procedure pp_cedate; external DLLName;
function pp_cenum; external DLLName;
function pp_ctcodes; external DLLName;
procedure pp_clearbit; external DLLName;
function pp_compno; external DLLName;
function pp_copycheck; external DLLName;
function pp_copywrite; external DLLName;
procedure pp_decrypt; external DLLName;
procedure pp_encrypt; external DLLName;
procedure pp_errorstr; external DLLName;
function pp_errornum; external DLLName;
function pp_exect; external DLLName;
function pp_exelimit; external DLLName;
function pp_expired; external DLLName;
procedure pp_expdate; external DLLName;
function pp_exptype; external DLLName;
procedure pp_getdate; external DLLName;
function pp_getregchar; external DLLName;
procedure pp_gettime; external DLLName;
procedure pp_getvarchar; external DLLName;
procedure pp_getvardate; external DLLName;
function pp_getvarnum; external DLLName;
function pp_hdlockwr; external DLLName;
function pp_hdserial; external DLLName;
function pp_initcf; external DLLName;
function pp_killprot; external DLLName;
function pp_lanactive; external DLLName;
function pp_lancheck; external DLLName;
function pp_landecr; external DLLName;
function pp_lanincr; external DLLName;
function pp_lanlimit; external DLLName;
function pp_lanminus; external DLLName;
function pp_lanplus; external DLLName;
function pp_lanusers; external DLLName;
procedure pp_lastday; external DLLName;
procedure pp_netvolume; external DLLName;
function pp_nexect; external DLLName;
function pp_nexelim; external DLLName;
function pp_nexptype; external DLLName;
function pp_nlanct; external DLLName;
function pp_nlanlim; external DLLName;
procedure pp_npdate; external DLLName;
function pp_password; external DLLName;
function pp_redir; external DLLName;
procedure pp_serial; external DLLName;
procedure pp_setbit; external DLLName;
function pp_setregchar; external DLLName;
procedure pp_setvarchar; external DLLName;
procedure pp_setvardate; external DLLName;
procedure pp_setvarnum; external DLLName;
function pp_testbit; external DLLName;
function pp_ucode; external DLLName;
function pp_unlock; external DLLName;
function pp_upddate; external DLLName;
function pp_valdate; external DLLName;
function pp_writecf; external DLLName;

end.
