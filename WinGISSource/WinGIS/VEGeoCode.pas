unit VEGeoCode;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, MultiLng;

type
  TVEGeoCodeForm = class(TForm)
    GroupBoxInput: TGroupBox;
    GroupBoxButtons: TGroupBox;
    ButtonCreate: TButton;
    ButtonClose: TButton;
    LabelAdress: TLabel;
    ButtonSearch: TButton;
    RadioButtonSymbol: TRadioButton;
    RadioButtonPoint: TRadioButton;
    GroupBoxResults: TGroupBox;
    ListBoxResults: TListBox;
    ButtonShow: TButton;
    EditAddress: TEdit;
    MlgSection: TMlgSection;
    procedure EditAddressChange(Sender: TObject);
    procedure ButtonCloseClick(Sender: TObject);
    procedure ButtonSearchClick(Sender: TObject);
    procedure ButtonCreateClick(Sender: TObject);
    procedure EditAddressKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ListBoxResultsDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ButtonShowClick(Sender: TObject);
  private
  public
  end;

var
  VEGeoCodeForm: TVEGeoCodeForm;

implementation

uses VEModule;

{$R *.DFM}

procedure TVEGeoCodeForm.EditAddressChange(Sender: TObject);
begin
     ButtonSearch.Enabled:=(Trim(EditAddress.Text) <> '');
end;

procedure TVEGeoCodeForm.ButtonCloseClick(Sender: TObject);
begin
     Close;
end;

procedure TVEGeoCodeForm.ButtonSearchClick(Sender: TObject);
var
AFound: Boolean;
begin
     Screen.Cursor:=crHourGlass;
     try
        ListBoxResults.Clear;
        ButtonCreate.Enabled:=VEForm.SearchAddress(EditAddress.Text);
        ButtonShow.Enabled:=ButtonCreate.Enabled;
        if (ButtonCreate.Enabled) and (ListBoxResults.Items.Count > 0) then begin
           ListBoxResults.ItemIndex:=0;
           Application.ProcessMessages;
           ButtonShowClick(nil);
        end;
     finally
        Screen.Cursor:=crDefault;
     end;
end;

procedure TVEGeoCodeForm.ButtonCreateClick(Sender: TObject);
begin
     VEForm.CreateAddressObject(VEForm.GeoCodeResults[ListBoxResults.ItemIndex]);
end;

procedure TVEGeoCodeForm.EditAddressKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     if (Key = VK_RETURN) and (ButtonSearch.Enabled) then begin
        ButtonSearchClick(nil);
     end;
end;

procedure TVEGeoCodeForm.ListBoxResultsDblClick(Sender: TObject);
begin
     if ButtonShow.Enabled then begin
        ButtonShowClick(nil);
     end;
end;

procedure TVEGeoCodeForm.FormShow(Sender: TObject);
begin
     EditAddress.SetFocus;
end;

procedure TVEGeoCodeForm.ButtonShowClick(Sender: TObject);
begin
     VEForm.ShowAddressRegion(VEForm.GeoCodeResults[ListBoxResults.ItemIndex]);
end;

end.
