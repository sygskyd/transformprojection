unit IDB_Consts;
{
Internal database. Ver. II.
Various constants that I use in other parts of Internal Database.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 22-01-2001
}
interface

Const
     cs_IDB_NormalVersion = '2.3.9';
     cs_UndoRedo_NormalVersion = '1.0.3';
     cs_TM_NormalVersion = '1.0.6';

     cs_IDB_DatabaseFileExt = 'wgi';
{ This is name of database's attribute table.}
     cs_AttributeTableNamePrefix = 'ATT';   // ATT + LayerIndex
{ Before I'll do an Undo operations I have to put deleting data into an undo table and redo one
  in a database. As I can do now not only with items og project but with a layer, I have to put
  in the undo table also sign what I had put in this table - a layer or a project item.
  These constants are determine that.}
     ci_LayerItem = 1;    // TIndex for Undo
     ci_SymbolsGroup = 2; // TSGroup for Undo
     ci_ProjectItem = 3;  // TView for Undo
     ci_Layer = 4;        // TLayer for Undo
     ci_NewObject = 5;    // An object was added and now the user wants to delete it as undo.
     ci_DeletedObject = 6;// An object was restored during Undo and needs to be deleted during Redo.
     ci_DeletedLayer = 7; // TLayer for Redo
     ci_ProjectItemTiedToLayerItem  = 8;
{ These parameters determines key and parameter name in the WinGIS project registry. }
     cs_ProjectRegistry_IDB_KeyName = '\Project\IDB\GUID';
     // Here I storage the start value of GUID. This value is created during creating a new project
     // and is continually all life time of the project.
     cs_ProjectRegistry_IDB_StartGUID_ParameterName = 'StartGUIDValue';
     // Here I storage the current value of GUID. This value changes during each saving of the project.
     cs_ProjectRegistry_IDB_GUID_ParameterName = 'GUIDValue';
     // Here I storage the local settings of IDB.
     cs_Projectregistry_IDB_LocalSettings_KeyName = '\Project\IDB\LocalSettings';
{ These constatns I use in SetObjectStyle procedure in TIDB object. }
     ci_DoNotChange = -1;
     ci_UseLayerSettings = -2;

     cs_CalcFieldAreaName = '_Area';
// ++ Cadmensky IDB Version 2.3.8
     cs_CalcFieldSizeName = '_Size';
     cs_CalcFieldTextName = '_Text';
     cs_CalcFieldlinkIDName = '_LinkID';
//     cs_CalcFieldLengthName = '_Length';
//     cs_CalcFieldPerimeterName = '_Perimeter';
// -- Cadmensky IDB Version 2.3.8
     cs_CalcFieldXName = '_X';
     cs_CalcFieldYName = '_Y';
     cs_CalcFieldVerticesCount = '_Vertices';
     cs_CalcFieldSymbolName = '_Symbol Name';

implementation

end.
