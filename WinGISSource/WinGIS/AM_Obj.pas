{****************************************************************************}
{ Unit AM_Obj                                                                }
{----------------------------------------------------------------------------}
{                                                                            }
{----------------------------------------------------------------------------}
{ Autor: Martin Forst                                                        }
{----------------------------------------------------------------------------}
{ Modifikationen                                                             }
{  16.02.1994 Martin Forst                                                   }
{  05.04.1994 Martin Forst   Kreise und Kreisb�gen                           }
{****************************************************************************}
unit AM_Obj;

interface

uses WinTypes, WinProcs, Dialogs, Forms, Classes, SysUtils, Controls, Objects,
  AM_Def, AM_Paint, AM_Layer, AM_View, AM_Index, AM_Text, AM_Sym, AM_Group,
  AM_Font, AM_Ole, AM_Ini,
  Hash, SymVer, UserIntf;

const
  SymTableExpandLevel = 0.8; { Schwelle f�r Verg��erung }
  SymTableExpandTo = 1.2; { Vergr��erungsfaktor }
  ObjFileVersion = 3; { Version f�r TObjects }
  SymbolsFileVersion = 1; { Version f�r TSymbols }
   {++ Moskaliov Business Graphics BUILD#150 17.01.01}
  ChartsLibFileVersion = 1; { Version of TChartsLib }
  ChartsTableInitCapacity = 10; { Threshold for Enlargement }
  ChartsTableExpandLevel = 0.8; { Threshold for Enlargement }
  ChartsTableExpandTo = 1.2; { Enlargement }

   {-- Moskaliov Business Graphics BUILD#150 17.01.01}
type
  PObjects = ^TObjects;
  TObjects = object(TLayer)
    LastText: LongInt;
    LastRText: LongInt;
    LastBusGraph: LongInt;
    LastSymbol: LongInt;
    LastPixel: LongInt;
    LastGroup: LongInt;
    LastPoly: LongInt;
    LastCPoly: LongInt;
    LastSpline: LongInt;
    LastImage: LongInt;
    LastMesLine: LongInt;
    LastCircle: LongInt; {0504F}
    LastArc: LongInt; {0504F}
    LastOLEObj: LongInt; {0707Ray}
   {++ Moskaliov Business Graphics BUILD#150 17.01.01}
    LastChartKind: LongInt;
   {-- Moskaliov Business Graphics BUILD#150 17.01.01}
    FileVersion: Word;
    constructor Init(AOwner: PLayer);
    constructor Load(S: TOldStream);
    constructor LoadOld(S: TOldStream);
    constructor LoadOld2(S: TOldStream);
    constructor LoadOld3(S: TOldStream);
    procedure DrawObject(PInfo: PPaint; AItem: PIndex; Clipped: Boolean); virtual;
    function DeleteIndex(PInfo: PPaint; AItem: PIndex): Boolean; virtual;
    function IndexObject(PInfo: PPaint; AItem: PIndex): PView; virtual;
    procedure InitLastInd;
    function InsertObject(PInfo: PPaint; Item: PIndex; Paint: Boolean): Boolean; virtual;
    procedure SetIndex(AItem: PIndex); virtual;
    procedure Store(S: TOldStream); virtual;
  end;

  TSymHashTable = class(THashTable)
  protected
    function OnCompareItems(Item1, Item2: Pointer): Boolean; override;
    procedure OnFreeItem(Item: Pointer); override;
    function OnGetHashValue(Item: Pointer): LongInt; override;
  end;

  PSymbols = ^TSymbols;
  TSymbols = object(TObjects)
    SymTable: TSymHashTable;
    ExternLibs: TList;
    constructor Init(AOwner: PLayer);
    constructor LoadV0(S: TOldStream);
    constructor Load(S: TOldStream);
    procedure Assign(ASymbols: PSymbols);
    function ChangeName(ASym: PSGroup; NewName: string): Boolean;
    procedure CheckForDiffSyms(PInfo: Pointer; var ProjModified: Boolean);
    function CheckName(AName, ALibName: string; BSym: PSGroup): Boolean;
    procedure CheckSymTexts(ProjFonts: pFonts; ExtFonts: pFonts; ASym: pSGroup);
    function CopySymIntoProject(PInfo: PPaint; ASym: PSGroup): Boolean;
    procedure DrawSymbol(PInfo: PPaint; Symbol: PSGroup); virtual;
    function DeleteSymbol(ASym: PSGroup): Boolean;
    function GetNewSymName(Start, ALib: string): string;
    function GetSymbol(const SymName, LibName: string): PSGroup;
    function GetProjectSymbol(const SymName, LibName: string): PSGroup;
    function GetExtSymbol(const SymName, LibName: string): PSGroup;
    function GetExtLib(const LibName: string): Pointer;
    function InsertLibSymbol(PInfo: PPaint; const ALib: string; ASym: PSGroup; Paint: Boolean): Boolean;
    function InsertObject(PInfo: PPaint; Item: PIndex; Paint: Boolean): Boolean; virtual;
{++ IDB}
{ I needed to create these functions 'cause WinGIS insert new object ONLY setting to it new ProgisID.
  But when when I make undelete operation, I want to insert object using old data about ProgisID of
  this object.}
    procedure InsertLibSymbolAfterUndo(const ALib: string; ASym: PSGroup);
    procedure InsertObjectAfterUndo(AItem: PIndex);
{-- IDB}
    function InsertSymbol(PInfo: PPaint; ASym: PSGroup; Paint: Boolean): Boolean;
    procedure Store(S: TOldStream); virtual;
    function StoreExtLibs(AskUser: Boolean): Word;
    procedure UpdateSym(ProjSym: PSGroup; ExtSym: PSGroup);
    procedure UseExt(ProjSym: PSGroup; ExtSym: PSGroup);
    function UseNew(ProjSym: PSGroup; ExtSym: PSGroup): Boolean;
    function UseOld(ProjSym: PSGroup; ExtSym: PSGroup): Boolean;
    procedure UseProj(ProjSym: PSGroup; ExtSym: PSGroup);
    destructor Done; virtual;
  end;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}
  TChartsHashTable = class(THashTable)
  protected
{$IFNDEF AXDLL} // <----------------- AXDLL
    function KeyOfValue(Item: Pointer): LongInt; override;
{$ENDIF} // <----------------- AXDLL
    function OnCompareItems(Item1, Item2: Pointer): Boolean; override;
    function OnGetHashValue(Item: Pointer): LongInt; override;
    procedure OnFreeItem(Item: Pointer); override;
  end;

  PChartsLib = ^TChartsLib;
  TChartsLib = object(TObjects)
    ChartsTable: TChartsHashTable;
    constructor Init(AOwner: PLayer);
    constructor Load(S: TOldStream);
    destructor Done; virtual;
    procedure Store(S: TOldStream); virtual;
    function AddChartKind(AKind: PChartKind; IsNewChart: Boolean): PChartKind;
    function DeleteChartKind(AKind: PChartKind): Boolean;
    function FindChartKind(KindIndex: LongInt; HashIndex: LongInt): PChartKind;
    procedure InitChartKinds(PInfo: PPaint);
  end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

implementation

uses AM_RText, AM_BuGra, ExtLib, MultiLng, SymRol
{++ IDB}
  , Am_Main, ActiveX
{-- IDB}
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
  , Graphics, TeEngine, Series
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
  ;

procedure TObjects.InitLastInd;
begin
  LastText := mi_Text;
  LastRText := mi_RText;
  LastBusGraph := mi_BusGraph;
  LastSymbol := mi_Symbol;
  LastPixel := mi_Pixel;
  LastGroup := mi_Group;
  LastPoly := mi_Poly;
  LastCPoly := mi_CPoly;
  LastSpline := mi_Spline;
  LastImage := mi_Image;
  LastMesLine := mi_MesLine;
  LastCircle := mi_Circle; {0504F}
  LastArc := mi_Arc; {0504F}
  LastOLEObj := mi_OLEObj;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
  LastChartKind := mi_ChartKind;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
  FileVersion := 0;
end;

constructor TObjects.Init(AOwner: PLayer);
begin
  TLayer.Init(AOwner);
  InitLastInd;
end;

constructor TObjects.LoadOld(S: TOldStream);

  procedure DoAll(Item: PIndex); far;
  var
    AType: Word;
  begin
    AType := Item^.GetObjType;
    case AType of
      ot_Text: LastText := Item^.Index;
      ot_Symbol: LastSymbol := Item^.Index;
      ot_Pixel: LastPixel := Item^.Index;
      ot_Group: LastGroup := Item^.Index;
      ot_Poly: LastPoly := Item^.Index;
      ot_CPoly: LastCPoly := Item^.Index;
    end;
  end;

begin
  TLayer.Load(S);
  InitLastInd;
  Data^.ForEach(@DoAll);
end;

constructor TObjects.LoadOld2(S: TOldStream);
begin
  InitLastInd;
  S.Read(LastText, SizeOf(LastText));
  S.Read(LastSymbol, SizeOf(LastSymbol));
  S.Read(LastPixel, SizeOf(LastPixel));
  S.Read(LastGroup, SizeOf(LastGroup));
  S.Read(LastPoly, SizeOf(LastPoly));
  S.Read(LastCPoly, SizeOf(LastCPoly));
  TLayer.Load(S);
end;

constructor TObjects.LoadOld3(S: TOldStream);
begin
  InitLastInd;
  S.Read(LastText, SizeOf(LastText));
  S.Read(LastSymbol, SizeOf(LastSymbol));
  S.Read(LastPixel, SizeOf(LastPixel));
  S.Read(LastGroup, SizeOf(LastGroup));
  S.Read(LastPoly, SizeOf(LastPoly));
  S.Read(LastCPoly, SizeOf(LastCPoly));
  S.Read(LastSpline, SizeOf(LastSpline));
  S.Read(LastImage, SizeOf(LastImage));
  S.Read(LastMesLine, SizeOf(LastMesLine));
  S.Read(LastMesLine, SizeOf(LastMesLine));
  S.Read(LastMesLine, SizeOf(LastMesLine));
  LastMesLine := mi_MesLine;
  TLayer.Load(S);
end;

constructor TObjects.Load(S: TOldStream);
var
  Version: Word;
begin
  InitLastInd;
  S.Read(Version, SizeOf(Version));
  FileVersion := Version;
  TLayer.Load(S);
  S.Read(LastText, SizeOf(LastText));
  S.Read(LastSymbol, SizeOf(LastSymbol));
  S.Read(LastPixel, SizeOf(LastPixel));
  S.Read(LastGroup, SizeOf(LastGroup));
  S.Read(LastPoly, SizeOf(LastPoly));
  S.Read(LastCPoly, SizeOf(LastCPoly));
  S.Read(LastSpline, SizeOf(LastSpline));
  S.Read(LastImage, SizeOf(LastImage));
  S.Read(LastMesLine, SizeOf(LastMesLine));
  if Version > 0 then
  begin
    S.Read(LastCircle, SizeOf(LastCircle));
    S.Read(LastArc, SizeOf(LastArc));
  end;
  if Version > 1 then
  begin
    S.Read(LastRText, SizeOf(LastRText));
    S.Read(LastBusGraph, SizeOf(LastBusGraph));
  end;
  if Version > 2 then
    S.Read(LastOLEObj, sizeof(LastOLEObj)); {0707Ray}
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
  if Version > 5 then
    S.Read(LastChartKind, sizeof(LastChartKind));
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
end;

procedure TObjects.SetIndex(AItem: PIndex);
begin
  if AItem <> nil then
    case AItem^.GetObjType of
      ot_Pixel:
        begin
          Inc(LastPixel);
          AItem^.Index := LastPixel;
        end;
      ot_Poly:
        begin
          Inc(LastPoly);
          AItem^.Index := LastPoly;
        end;
      ot_CPoly:
        begin
          Inc(LastCPoly);
          AItem^.Index := LastCPoly;
        end;
      ot_Group:
        begin
          Inc(LastGroup);
          AItem^.Index := LastGroup;
        end;
      ot_Text:
        begin
          Inc(LastText);
          AItem^.Index := LastText;
        end;
      ot_RText:
        begin
          Inc(LastRText);
          AItem^.Index := LastRText;
        end;
      ot_BusGraph:
        begin
          Inc(LastBusGraph);
          AItem^.Index := LastBusGraph;
        end;
      ot_Symbol:
        begin
          Inc(LastSymbol);
          AItem^.Index := LastSymbol;
        end;
      ot_Spline:
        begin
          Inc(LastSpline);
          AItem^.Index := LastSpline;
        end;
      ot_Image:
        begin
          Inc(LastImage);
          AItem^.Index := LastImage;
        end;
      ot_MesLine:
        begin
          Inc(LastMesLine);
          AItem^.Index := LastMesLine;
        end;
      ot_Circle:
        begin {0504F}
          Inc(LastCircle);
          AItem^.Index := LastCircle;
        end;
      ot_Arc:
        begin
          Inc(LastArc);
          AItem^.Index := LastArc;
        end; {0504F}
      ot_OLEObj:
        begin
          Inc(LastOLEObj);
          AItem^.Index := LastOLEObj; {0707Ray}
        end;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
      ot_ChartKind:
        begin
          Inc(LastChartKind);
          AItem^.Index := LastChartKind;
        end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
    end;
end;

procedure TObjects.DrawObject(PInfo: PPaint; AItem: PIndex; Clipped: Boolean);
var
  ObjIndex: LongInt;
  DrawObj: PView;
begin
  if Data^.Search(AItem, ObjIndex) then
  begin
    DrawObj := PView(Data^.At(ObjIndex));
    DrawObj^.Draw(PInfo, Clipped);
  end
  else
    MsgBox(0, 1002, mb_Ok or mb_IconExclamation, '');
end;

function TObjects.InsertObject(PInfo: PPaint; Item: PIndex; Paint: Boolean): Boolean;
var
  ASymbol: PSGroup;

  procedure IncUseCount(AFont: PFontDes);
  begin
    if AFont <> nil then
      Inc(AFont^.UseCount);
  end;

begin
  InsertObject := FALSE;

  if Item = nil then
    exit;

  if Data^.GetCount <= MaxObjects then
  begin {sl}
    SetIndex(Item);
    Data^.Insert(Item);
    if Item^.GetObjType = ot_Text then
      IncUseCount(PInfo^.Fonts^.GetFont(PText(Item)^.Font.Font));
    if Item^.GetObjType = ot_RText then
      IncUseCount(PInfo^.Fonts^.GetFont(PRText(Item)^.Font.Font));
    if Item^.GetObjType = ot_BusGraph then
      IncUseCount(PInfo^.Fonts^.GetFont(PBusGraph(Item)^.SVFont.Font));
    if Item^.GetObjType = ot_Symbol then
    begin
      ASymbol := PSymbol(Item)^.GetSymbol(PInfo);
      if ASymbol <> nil then
        Inc(ASymbol^.UseCount);
    end;
    InsertObject := TRUE;
  end;
end;

function TObjects.IndexObject(PInfo: PPaint; AItem: PIndex): PView;
var
  LIndex: LongInt;
begin
  if AItem^.MPtr = nil then
  begin
    if Data^.Search(AItem, LIndex) then
      Result := PView(Data^.At(LIndex))
    else
      Result := nil;
    AItem^.MPtr := Result;
  end
  else
    if AItem^.Index = PIndex(AItem^.MPtr)^.Index then
      Result := AItem^.MPtr
    else
    begin
      if Data^.Search(AItem, LIndex) then
        Result := PView(Data^.At(LIndex))
      else
        Result := nil;
      AItem^.MPtr := Result;
    end;
end;

function TObjects.DeleteIndex(PInfo: PPaint; AItem: PIndex): Boolean;

  procedure DecUseCount(AFont: PFontDes);
  begin
    if AFont <> nil then
      Dec(AFont^.UseCount);
  end;

var
  DelInd: LongInt;
  BItem: PIndex;
  AFont: PFontDes;
  ASymbol: PSGroup;
begin
  if Data^.Search(AItem, DelInd) then
  begin
    BItem := Data^.At(DelInd);
//++ Glukhov ObjAttachment 19.10.00
// IDB_Warning!!!
    if BItem.GetState(sf_Attached) then
      PView(BItem).DetachMe;
    if Assigned(PView(BItem).AttData) then
      PView(BItem).DetachFromMe(PInfo);
//-- Glukhov ObjAttachment 19.10.00
    if BItem^.GetObjType = ot_Text then
      DecUseCount(PInfo^.Fonts^.GetFont(PText(BItem)^.Font.Font));
    if BItem^.GetObjType = ot_RText then
      DecUseCount(PInfo^.Fonts^.GetFont(PRText(BItem)^.Font.Font));
    if BItem^.GetObjType = ot_BusGraph then
      DecUseCount(PInfo^.Fonts^.GetFont(PBusGraph(BItem)^.SVFont.Font));
    if BItem^.GetObjType = ot_Symbol then
    begin
      ASymbol := PSymbol(BItem)^.GetSymbol(PInfo);
      if ASymbol <> nil then
        Dec(ASymbol^.UseCount);
    end;
    Data^.AtFree(DelInd);
    DeleteIndex := TRUE;
  end
  else
    DeleteIndex := FALSE;
end;

procedure TObjects.Store(S: TOldStream);
var
  Version: Word;
begin
// Version:=5;   //-- Moskaliov Business Graphics BUILD#150 17.01.01
  Version := 6; //++ Moskaliov Business Graphics BUILD#150 17.01.01
  S.Write(Version, SizeOf(Version));
  TLayer.Store(S);
  S.Write(LastText, SizeOf(LastText));
  S.Write(LastSymbol, SizeOf(LastSymbol));
  S.Write(LastPixel, SizeOf(LastPixel));
  S.Write(LastGroup, SizeOf(LastGroup));
  S.Write(LastPoly, SizeOf(LastPoly));
  S.Write(LastCPoly, SizeOf(LastCPoly));
  S.Write(LastSpline, SizeOf(LastSpline));
  S.Write(LastImage, SizeOf(LastImage));
  S.Write(LastMesLine, SizeOf(LastMesLine));
  S.Write(LastCircle, SizeOf(LastCircle));
  S.Write(LastArc, SizeOf(LastArc));
  S.Write(LastRText, SizeOf(LastRText));
  S.Write(LastBusGraph, SizeOf(LastBusGraph));
  S.Write(LastOLEObj, sizeof(LastOLEObj));
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
  S.Write(LastChartKind, sizeof(LastChartKind));
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
end;

{******************************************************************************}
{ Methoden des Objekts TSymbols                                                }
{******************************************************************************}

constructor TSymbols.Init(AOwner: PLayer);
begin
  inherited Init(AOwner);
  SymTable := TSymHashTable.Create(10);
  ExternLibs := TList.Create;
end;

constructor TSymbols.LoadV0(S: TOldStream);

  procedure DoAll(AItem: PIndex); far;
  begin
    SymTable.Add(AItem);
  end;

begin
  inherited Load(S);
  SymTable := TSymHashTable.Create(LimitToLong(1.2 * Data^.GetCount + 10));
                   { Hash-Tabelle soll nie ganz voll sein !!! }
  Data^.ForEach(@DoAll);
  ExternLibs := TList.Create;
end;

constructor TSymbols.Load(S: TOldStream);

  procedure DoAll(AItem: PIndex); far;
  begin
    SymTable.Add(AItem);
  end;

var
  Version: Byte;
  ExtLibCnt: LongInt;
  i: LongInt;
  NewExtLib,
    aExtLib: PExtLib;
begin
  inherited Load(S);
  S.Read(Version, sizeof(Version));
  ExternLibs := TList.Create;
  S.Read(ExtLibCnt, sizeof(ExtLibCnt));
  for i := 0 to ExtLibCnt - 1 do
  begin
    NewExtLib := Pointer(S.Get);
    aExtLib := NewExtLib^.AlreadyLoaded;
    if (aExtLib <> nil) then
    begin
      Dispose(NewExtLib, Done);
      NewExtLib := aExtLib;
      Inc(NewExtLib^.UseCount);
    end
    else
    begin
      NewExtLib^.ReLoad;
    end;
    ExternLibs.Add(NewExtLib);
  end;

  SymTable := TSymHashTable.Create(LimitToLong(1.2 * Data^.GetCount + 10));
                   { Hash-Tabelle soll nie ganz voll sein !!! }
  Data^.ForEach(@DoAll);

end;

{******************************************************************************}
{ Methode GetNewSymName                                                        }
{ Generiert einen Namen f�r ein neues Symbol anch dem Schema Startname+' (xx)' }
{ Als Start kann ein beliebiger String angegeben werden (z.B. 'Neues Symbol')  }

function TSymbols.GetNewSymname(Start, ALib: string): string;
var
  NewSymName,
    Si: string;
  i: Integer;
begin
  NewSymName := Start;
  i := 0;
{++ IDB}
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
  if not WinGisMainForm.WeAreUsingUndoFunction[WinGisMainForm.ActualProj] then
  begin
       // This is an old way to create new symbol name. It doesn't use the mapper information
       // in UNDO manager of the IDB.
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
{-- IDB}
    Si := '';
    while (i < NewSymLimit) and (not CheckName(NewSymName + Si, ALib, nil)) do
    begin
      i := i + 1;
      Str(i, Si);
      Si := ' (' + Si + ')';
    end;
    GetNewSymName := NewSymName + Si;
{++ IDB}
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
  end
  else
  begin
      // This is new way to create new symbol name. This way uses information in the symbol's mapper
      // in the IDB. I need to complete eval boolean statements therefore I set B+. If earlier B was
      // switched off, I set B- after my procedure.
{$IFOPT B-}
{$DEFINE B_Options_was_switched_off}
{$ENDIF}
{$B+}
    RESULT := Start;
    while (i < NewSymLimit)
      and
               // Symbol with such name exists (or not) in the required library.
    (not (CheckName(RESULT, ALib, nil))
      or
               // Symbol with such name exists (or not) in the symbol mapper in the IDB.
      (WinGisMainForm.UndoRedo_Man.SymbolNameInMappingExists(WinGisMainForm.ActualProj, ALib, RESULT))) do
    begin
      Inc(i);
      RESULT := Start + ' (' + IntToStr(i) + ')';
    end;
{$IFDEF B_Options_was_switched_off}
{$B-}
{$ENDIF}
  end;
{-- IDB}
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
end;

procedure TSymbols.CheckForDiffSyms
  (
  PInfo: Pointer;
  var ProjModified: Boolean
  );
var
  ProjSym,
    ExtSym: PSGroup;
  i, j: LongInt;
  ExtLib: PExtLib;
  ExtSymList,
    ProjSymList,
    ActList: TList;
  hStr,
    Cnt: string;
  SymVerDlg: TSymVerCheck;
  AValue: pInteger;
begin
  ProjModified := False;
  ExtSymList := TList.Create;
  ProjSymList := TList.Create;
  ActList := TList.Create;
  for i := 0 to ExternLibs.Count - 1 do
  begin { alle ext. Bib. untersuchen }
    ExtLib := pExtLib(ExternLibs.Items[i]);
    if (ExtLib^.Loaded) then
      for j := 0 to ExtLib^.Symbols^.Data^.GetCount - 1 do
      begin { alle Symbole untersuchen }
        ExtSym := PSGroup(ExtLib^.Symbols^.Data^.At(j));
        ProjSym := SymTable.Find(ExtSym);
        if (ProjSym <> nil) then
        begin { Symbole mit gleichem Namen gefunden }
          if (ExtSym^.LastChange <> ProjSym^.LastChange) then
          begin
            ExtSymList.Add(ExtSym);
            ProjSymList.Add(ProjSym);
            AValue := new(PInteger);
            AValue^ := el_UseNew;
            ActList.Add(AValue);
          end;
        end;
      end;
  end;
  if (ExtSymList.Count > 0) then
  begin
    hStr := MlgStringList['SymVerDlg', 17];
    Str(ExtSymList.Count, Cnt);
    i := Pos('%s', hStr);
    Delete(hStr, i, 2);
    Insert(Cnt, hStr, i);
    if (MessageDialog(hStr, mtInformation, [mbOK], 0) = mrOk) then
    begin
      SymVerDlg := TSymVerCheck.Create(Application);
      SymVerDlg.PInfo := PInfo;
      SymVerDlg.ProjSyms := ProjSymList;
      SymVerDlg.ExtSyms := ExtSymList;
      SymVerDlg.ActList := ActList;
      SymVerDlg.Fonts := pPaint(PInfo)^.Fonts;
      if SymVerDlg.ShowModal = mrOK then
      begin
        i := ExtSymList.Count - 1;
        while (i >= 0) do
        begin
          ExtSym := ExtSymList.Items[i];
          ProjSym := ProjSymList.Items[i];
          AValue := PInteger(ActList.Items[i]);
          case AValue^ of
            el_UseNew:
              if UseNew(ProjSym, ExtSym) then
                ProjModified := True;
            el_UseOld:
              if UseOld(ProjSym, ExtSym) then
                ProjModified := True;
            el_UseProj:
              begin
                UseProj(ProjSym, ExtSym);
                ProjModified := True;
              end;
            el_UseExt: UseExt(ProjSym, ExtSym);
            el_RenProj:
              begin
                ProjSym^.SetName(ProjSym^.GetName +
                  MlgStringList['SymVerDlg', 20]); { Hinweis anh�ngen }
                ProjSym^.SetLibName('');
                ProjModified := True;
              end;
          end;
          i := i - 1;
        end;
      end
      else
        MessageDialog(MlgStringList['SymVerDlg', 19],
          mtWarning, [mbOK], 0);
      i := ExtSymList.Count - 1;
      while (i >= 0) do
      begin
        ExtSymList.Delete(i);
        ProjSymList.Delete(i);
        Dispose(PInteger(ActList.Items[i]));
        ActList.Delete(i);
        i := i - 1;
      end;
    end;
  end;
  ExtSymList.Free;
  ProjSymList.Free;
  ActList.Free;

end;

{******************************************************************************}
{ Methode UseNew                                                               }
{ verwendet im Projekt das neuere der beiden Symbole                           }
{ ist ProjSym neuer wird der Bibliotheksverweis (LibName) aufgel�st und ev.    }
{       ein neuer Name ( xxx(1) ) generiert                                    }
{ ist ExtSym neuer werden die �nderungen in ProjSym kopiert                    }

function TSymbols.UseNew
  (
  ProjSym: PSGroup;
  ExtSym: PSGroup
  ): Boolean;
begin
  UseNew := False;
  if (ProjSym^.LastChange > ExtSym^.LastChange) then
  begin { Projektsymbol ist neuer, und soll weiterverwendet werden }
    ProjSym^.SetName(GetNewSymName(ProjSym^.GetName, '')); { ev. neuen Namen generieren }
      { wenn kein Symbol mit gleichem Namen existiert, wird der Namen "nicht" ge�ndert }
    ProjSym^.SetLibName(''); { ist jetzt Symbol des Projekts }
    SymTable.Add(ProjSym); { f�r "flotten" Zugriff auf Symbole  (hatte vorher keinen Hash Eintrag)}
    UseNew := True; { Projekt ge�ndert }
  end
  else { ext. Symbol ist neuer -> �nderungen ins Projektsymbol kopieren }
    UpdateSym(ProjSym, ExtSym);
end;

{******************************************************************************}
{ Methode UseOld                                                               }
{ verwendet im Projekt das �ltere der beiden Symbole                           }
{ ist ProjSym �lter wird der Bibliotheksverweis (LibName) aufgel�st und ev.    }
{       ein neuer Name ( xxx(1) ) generiert                                    }
{ ist ExtSym �lter werden die �nderungen in ProjSym kopiert                    }

function TSymbols.UseOld
  (
  ProjSym: PSGroup;
  ExtSym: PSGroup
  ): Boolean;
begin
  UseOld := False;
  if (ProjSym^.LastChange < ExtSym^.LastChange) then
  begin { Projektsymbol ist �lter, und soll weiterverwendet werden }
    ProjSym^.SetName(GetNewSymName(ProjSym^.GetName, '')); { ev. neuen Namen generieren }
      { wenn kein Symbol mit gleichem Namen existiert, wird der Namen "nicht" ge�ndert }
    ProjSym^.SetLibName(''); { ist jetzt Symbol des Projekts }
    SymTable.Add(ProjSym); { f�r "flotten" Zugriff auf Symbole  (hatte vorher keinen Hash Eintrag)}
    UseOld := True; { Projekt ge�ndert }
  end
  else { ext. Symbol ist neuer -> �nderungen ins Projektsymbol kopieren }
    UpdateSym(ProjSym, ExtSym);
end;

{******************************************************************************}
{ Methode UseProj                                                              }
{ verwendet das im Projekt gespeicherte Symbol weiter                          }

procedure TSymbols.UseProj
  (
  ProjSym: PSGroup;
  ExtSym: PSGroup
  );
begin { Projektsymbol soll weiterverwendet werden }
  ProjSym^.SetName(GetNewSymName(ProjSym^.GetName, '')); { ev. neuen Namen generieren }
      { wenn kein Symbol mit gleichem Namen existiert, wird der Namen "nicht" ge�ndert }
  ProjSym^.SetLibName(''); { ist jetzt Symbol des Projekts }
  SymTable.Add(ProjSym); { f�r "flotten" Zugriff auf Symbole  (hatte vorher keinen Hash Eintrag)}
end;

{******************************************************************************}
{ Methode UseExt                                                               }
{ verwendet im Projekt das externe Symbol                                      }

procedure TSymbols.UseExt
  (
  ProjSym: PSGroup;
  ExtSym: PSGroup
  );
begin { ext. Symbol ist neuer -> �nderungen ins Projektsymbol kopieren }
  UpdateSym(ProjSym, ExtSym);
end;

{******************************************************************************}
{ Methode UpdateSym                                                            }
{ updatet ProjSym mit ExtSym                                                   }

procedure TSymbols.UpdateSym
  (
  ProjSym: PSGroup;
  ExtSym: PSGroup
  );
begin
  Dispose(ProjSym^.Data, Done);
  ProjSym^.Data := MakeObjectCopy(ExtSym^.Data);
  ProjSym^.LastChange := ExtSym^.LastChange;
  ProjSym^.RefPoint.Init(ExtSym^.RefPoint.X, ExtSym^.RefPoint.Y);
  ProjSym^.RefLinePoint1.X := ExtSym^.RefLinePoint1.X;
  ProjSym^.RefLinePoint1.Y := ExtSym^.RefLinePoint1.Y;
  ProjSym^.RefLinePoint2.X := ExtSym^.RefLinePoint2.X;
  ProjSym^.RefLinePoint2.Y := ExtSym^.RefLinePoint2.Y;
  ProjSym^.DefaultAngle := ExtSym^.DefaultAngle;
  ProjSym^.DefaultSize := ExtSym^.DefaultSize;
  ProjSym^.DefaultSizeType := ExtSym^.DefaultSizeType;
  DisposeStr(ProjSym^.Name);
  ProjSym^.Name := NewStr(ExtSym^.GetName);
  DisposeStr(ProjSym^.LibName);
  ProjSym^.LibName := NewStr('');
  ProjSym^.CalculateClipRect;
  WingisMainForm.ActualChild.Data^.RefreshSymbolMan(True);
end;

procedure TSymbols.Store(S: TOldStream);
var
  Version: Byte;
  ExtLibCnt,
    i: LongInt;
begin
  inherited Store(S);
  Version := SymbolsFileVersion;
  S.Write(Version, sizeof(Version));
  ExtLibCnt := ExternLibs.Count;
  S.Write(ExtLibCnt, sizeof(ExtLibCnt));
  for i := 0 to ExtLibCnt - 1 do
    S.Put(PExtLib(ExternLibs.Items[i]));
end;

{******************************************************************************+
| Function TSymbols.StoreExLibs                                                |
|------------------------------------------------------------------------------|
| versucht alle ge�nderten ext. Symbolbibliotheken zu speichern                |
| bei AskUser = False werden alle ge�nderten Bibliotheken gespeichert, andernf.|
| wird der Benutzer gefragt.                                                   |
| R�ckgabewert entspricht denen von MessageDialog (mbYes,mbNo,mbCancel)        |
+******************************************************************************}

function TSymbols.StoreExtLibs(AskUser: Boolean): Word;
var
  ExtLib: pExtLib;
  i,
    retMess: Integer;
begin
  Result := mrYes;
  for i := 0 to ExternLibs.Count - 1 do
  begin
    ExtLib := pExtLib(ExternLibs.Items[i]);
    if (ExtLib^.Modified) then
    begin
      if (AskUser) then
      begin
        retMess := MessageDialog(Format(MlgStringList['SymbolMsg', 4], [ExtLib^.LibName^]),
          mtConfirmation, [mbYes, mbNo, mbCancel], 0);
        case retMess of
          mrYes:
            if (not ExtLib^.SaveExtLib) then
              Result := mrCancel;
          mrCancel: Result := mrCancel;
        end;
      end
      else
        if (not ExtLib^.SaveExtLib) then
          Result := mrCancel;
    end;
  end;
end;

function TSymbols.InsertObject(PInfo: PPaint; Item: PIndex; Paint: Boolean): Boolean;
begin
  SetIndex(Item);
  Data^.Insert(Item);
  SymTable.Add(Item); { f�r "flotten" Zugriff auf Symbole }
  if ((SymTable.Count / SymTable.Capacity) > SymTableExpandLevel) then
    SymTable.Capacity := Trunc(SymTable.Count * SymTableExpandTo); { wenn "zu voll" Hashtable vergr��ern }
  InsertObject := TRUE;
end;

function TSymbols.InsertSymbol(PInfo: PPaint; ASym: PSGroup; Paint: Boolean): Boolean;
begin
  if (CheckName(ASym^.GetName, ASym^.GetLibName, ASym)) then
  begin
    Result := InsertObject(PInfo, ASym, Paint);
    if Result then
    begin
      WingisMainForm.ActualChild.Data^.RefreshSymbolMan(True);
    end;
  end
  else
  begin
    Result := False;
  end;
end;

function TSymbols.InsertLibSymbol(PInfo: PPaint; const ALib: string;
  ASym: PSGroup; Paint: Boolean): Boolean;
var
  i: Integer;
  ASymLib: PExtLib;
begin
  InsertLibSymbol := False;
  if (ALib = '') then
  begin
    if (CheckName(ASym^.GetName, ASym^.GetLibName, ASym)) then
      InsertLibSymbol := InsertObject(PInfo, ASym, Paint);
  end
  else
  begin
    ASymLib := GetExtLib(ALib);
    if (ASymLib <> nil) then
      if (ASymLib^.CheckName(ASym^.GetName, ASym)) then
        InsertLibSymbol := ASymLib^.InsertSymbol(PInfo, ASym, Paint);
  end;
end;

{++ IDB}
{ I have to use my own function InsertAfterUndo 'cause ordinary functions add object with
  new index. But I need to restore object with OLD index. Ivanoff. 11.09.2000.}

procedure TSymbols.InsertObjectAfterUndo(AItem: PIndex);
begin
  Data.Insert(AItem);
  SymTable.Add(AItem);
  if ((SymTable.Count / SymTable.Capacity) > SymTableExpandLevel) then
    SymTable.Capacity := Trunc(SymTable.Count * SymTableExpandTo);
end;

procedure TSymbols.InsertLibSymbolAfterUndo(const ALib: string; ASym: PSGroup);
var
  iI: Integer;
  AExtSymLib: PExtLib;
begin
  if ALib = '' then
    InsertObjectAfterUndo(ASym)
  else
  begin
    AExtSymLib := GetExtLib(ALib);
    if AExtSymLib <> nil then
    begin
      AExtSymLib.InsertSymbolAfterUndo(ASym);
      InsertObjectAfterUndo(ASym);
    end;
  end;
end;
{-- IDB}

function TSymbols.DeleteSymbol
  (
  ASym: PSGroup
  )
  : Boolean;
var
  retSymTable,
    retLayer: Boolean;
  DelInd: LongInt;
begin
  retLayer := False;
  retSymTable := SymTable.Delete(ASym); { Text- und Symbolchecks sind �berfl�ssig }
  if Data^.Search(ASym, DelInd) then
  begin
    Data^.AtFree(DelInd);
    retLayer := true;
  end;
  DeleteSymbol := retSymTable and retLayer;
end;

function TSymbols.CheckName(AName: string; ALibName: string; BSym: PSGroup): Boolean;
var
  i: LongInt;
  ASym,
    Found: PSGroup;
  AText: string;
begin
  ASym := new(PSGroup, InitName(AName, ALibName));
  Found := PSGroup(SymTable.Find(ASym));
  Dispose(ASym, Done);
  if (Found = nil) or (Found = BSym) then
    CheckName := True { nicht gefunden oder }
                                                         { dasselbe Symbol ist ok }
  else
    CheckName := False; { Namen bereits vorhanden ! }
end;

function TSymbols.ChangeName { �ndert den Namen eines Symbols }
  ({ und korrigiert die Symbol-Hashtabelle }
  ASym: PSGroup;
  NewName: string
  ): Boolean;
begin
  ChangeName := False; { im Zweifelsfall hauts nit hin }
  if (CheckName(NewName, ASym^.GetLibName, ASym)) then
  begin { kein anderes Symbol mit diesem Namen ?}
    SymTable.Delete(ASym); { Symbol mit altem Namen aus HashTable l�schen }
    ASym^.SetName(NewName); { Symbol umbenennen }
    SymTable.Add(ASym); { Symbol mit neuem Namen wieder einf�gen }
    ChangeName := True; { R�ckgabewert = ok }
  end;
end;

function TSymbols.GetSymbol
  (
  const SymName: string;
  const LibName: string
  ): PSGroup;
var
  ASym,
    Found: PSGroup;
  i: Integer;
begin
  ASym := new(PSGroup, InitName(SymName, LibName));
  Found := PSGroup(SymTable.Find(ASym)); { Projektsym }
  if (Found = nil) then
  begin { extern suchen }
    i := ExternLibs.Count - 1;
    while (i >= 0) do
    begin
      if (PExtLib(ExternLibs.Items[i])^.LibName^ = LibName) and
        (PExtLib(ExternLibs.Items[i])^.Loaded) then
      begin
        Found := PSGroup(
          PSymbols(
          PExtLib(ExternLibs.Items[i])^.Symbols)^.SymTable.Find(ASym)); { ext. Bib-Sym }
        i := -1; { kann nur einmal dieselbe eBib finden }
      end;
      i := i - 1;
    end;
  end;
  Dispose(ASym, Done);
  GetSymbol := Found;
end;

function TSymbols.GetProjectSymbol(const SymName: string; const LibName: string): PSGroup;
var
  ASym,
    Found: PSGroup;
  i: Integer;
begin
  ASym := new(PSGroup, InitName(SymName, LibName));
  Found := PSGroup(SymTable.Find(ASym)); { Projektsym }
  Dispose(ASym, Done);
  GetProjectSymbol := Found;
end;

function TSymbols.GetExtSymbol
  (
  const SymName: string;
  const LibName: string
  ): PSGroup;
var
  ASym,
    Found: PSGroup;
  i: Integer;
begin
  Found := nil; { es lebe der Bug }
  ASym := new(PSGroup, InitName(SymName, LibName));
  i := ExternLibs.Count - 1;
  while (i >= 0) do
  begin
    if (PExtLib(ExternLibs.Items[i])^.LibName^ = LibName) and
      (PExtLib(ExternLibs.Items[i])^.Loaded) then
    begin
      Found := PSGroup(
        PSymbols(
        PExtLib(ExternLibs.Items[i])^.Symbols)^.SymTable.Find(ASym)); { ext. Bib-Sym }
      i := -1; { kann nur einmal dieselbe eBib finden }
    end;
    i := i - 1;
  end;
  Dispose(ASym, Done);
  GetExtSymbol := Found;
end;

function TSymbols.GetExtLib
  (
  const LibName: string
  ): Pointer;
var
  i: Integer;
begin
  GetExtLib := nil;
  i := ExternLibs.Count - 1;
  while (i >= 0) do
  begin
    if StrComp(PChar(PExtLib(ExternLibs.Items[i])^.LibName^), PChar(AnsiString(LibName))) = 0 then
      GetExtLib := ExternLibs.Items[i];
    i := i - 1;
  end;
end;

procedure TSymbols.DrawSymbol
  (
  PInfo: PPaint;
  Symbol: PSGroup
  );
begin
  if Assigned(Symbol) then
  begin
    Symbol^.Draw(PInfo, False);
  end
  else
  begin
    PInfo^.SymbolDrawError := 1;
  end;
end;

{******************************************************************************}
{ CopySymIntoProject                                                           }
{ R�ckgabewert : False    Projekt nicht ver�ndert                              }
{                True     Projekt ver�ndert -> SetModified sollte aufgerufen werden }
{******************************************************************************}

function TSymbols.CopySymIntoProject(PInfo: PPaint; ASym: PSGroup): Boolean;
var
  BSym: PSGroup;
  ExtLib: pExtLib;
  ExtSymList,
    ProjSymList,
    ActList: TList;
  ExtSym,
    ProjSym: pSGroup;
  AValue: PInteger;
  i: Integer;
  ProjModified: Boolean;
  SymVerDlg: TSymVerCheck;
begin
  Result := False;
  BSym := GetProjectSymbol(ASym^.GetName, '');
  if (BSym <> nil) then
  begin
    UpdateSym(BSym, ASym);
    exit;
  //       DeleteSymbol(BSym);
//     if (BSym^.LastChange<>ASym^.LastChange) then begin
//        BSym^.SetName(GetNewSymName(BSym^.GetName,''));
//     end;
  end;
{        MessageDialog(MlgStringList['SymbolMsg',5],mtInformation,[mbOK],0);
        ExtSymList:=TList.Create;
        ProjSymList:=TList.Create;
        ActList:=TList.Create;
        ExtSymList.Add(ASym);
        ProjSymList.Add(BSym);
        AValue:=new(PInteger);
        AValue^:=el_UseNew;
        ActList.Add(AValue);
        SymVerDlg:=TSymVerCheck.Create(Application);
        SymVerDlg.PInfo:=PInfo;
        SymVerDlg.ProjSyms:=ProjSymList;
        SymVerDlg.ExtSyms:=ExtSymList;
        SymVerDlg.ActList:=ActList;
        SymVerDlg.Fonts:=pPaint(PInfo)^.Fonts;
        if SymVerDlg.ShowModal=mrOK then begin
           i:=ExtSymList.Count-1;
           while (i>=0) do begin
                 ExtSym:=ExtSymList.Items[i];
                 ProjSym:=ProjSymList.Items[i];
                 AValue:=PInteger(ActList.Items[i]);
                 case AValue^ of
                     el_UseNew : if UseNew(ProjSym,ExtSym) then
                                    Result:=True;
                     el_UseOld : if UseOld(ProjSym,ExtSym) then
                                    Result:=True;
                     el_UseProj: begin
                                   UseProj(ProjSym,ExtSym);
                                   Result:=True;
                                 end;
                     el_UseExt : UseExt(ProjSym,ExtSym);
                     el_RenProj: begin
                                   ProjSym^.SetName(ProjSym^.GetName+MlgStringList['SymVerDlg',20]);
                                   ProjSym^.SetLibName('');
                                   Result:=True;
                                 end;
                 end;
                 i:=i-1;
           end;
        end
        else begin
             MessageDialog(MlgStringList['SymVerDlg',19],mtWarning,[mbOK],0);
        end;
     end;
  end
  else begin}
  BSym := MakeObjectCopy(ASym);
  ExtLib := GetExtLib(ASym^.GetLibName);
  if (ExtLib <> nil) then
  begin
    CheckSymTexts(PInfo^.Fonts, ExtLib^.Fonts, BSym);
  end;
  InsertSymbol(PInfo, BSym, False);
//    end;
end;

{******************************************************************************}
{ CheckSymTexts passt Texte im Symbol an die Projektfonts an                   }
{   nicht vorhandene Fonts werden aus ExtFonts kopiert  (<>TExtLib.CheckTexts) }
{******************************************************************************}

procedure TSymbols.CheckSymTexts
  (
  ProjFonts: pFonts;
  ExtFonts: pFonts;
  aSym: pSGroup
  );
var
  ProjFontDes,
    ExtFontDes: pFontDes;
  i: Integer;
  aObj: pIndex;
  aText: pText;
  FontID: Integer16;
begin
  if (ProjFonts = nil) or (ExtFonts = nil) then
    exit;
  i := 0;
  while (i < ASym^.Data^.Count) do
  begin
    aObj := PIndex(ASym^.Data^.At(i));
    if (aObj^.GetObjType = ot_Text) then
    begin { nur Texte anpassen }
      aText := pText(aObj);
      ExtFontDes := ExtFonts^.GetFont(aText^.Font.Font);
      if (ExtFontDes <> nil) then
      begin { nur wenn Font existiert }
        FontID := ProjFonts^.IDFromName(ExtFontDes^.Name^); { FontNr im Projekt ermitteln }
        if (FontID > 0) then
          aText^.Font.Font := FontID { Font exist., ID anpassen }
        else
        begin { Font exist nicht,  }
          ProjFontDes := MakeObjectCopy(ExtFontDes); { Kopie erstellen }
          Inc(ProjFonts^.MaxNum);
          FontID := ProjFonts^.MaxNum;
          ProjFontDes^.FNum := FontID;
          ProjFonts^.Insert(ProjFontDes); { zu ext. Font hinzuf�gen }
          aText^.Font.Font := FontID;
        end;
      end; // else aText^.Font.Font:=1;   { im Zweifelsfall Text mit Nr. 1 verwenden }
      ProjFontDes := ProjFonts^.GetFont(aText^.Font.Font);
      if (ProjFontDes <> nil) then
        Inc(ProjFontDes^.UseCount);
    end;
    Inc(i);
  end;
end;

destructor TSymbols.Done;
var
  i: LongInt;
  aExtLib: pExtLib;
begin
  SymTable.Free;
   { ExtLibs einzeln freigeben !!!}
  i := ExternLibs.Count - 1;
  while (i >= 0) do { PExtLibs ist Object -> selber per Hand }
  begin { freigeben !!!}
    aExtLib := PExtLib(ExternLibs.Items[i]);
    if (aExtLib^.UseCount = 0) then
    begin
//++ Glukhov Bug#73 BUILD#128 06.09.00
//             aExtLib^.RemoveLastInstance;
//-- Glukhov Bug#73 BUILD#128 06.09.00
               //Dispose(aExtLib,Done);   { "letze" Instanz }
    end
    else
      Dec(aExtLib^.UseCount); { wird einmal "weniger" gebraucht }
    Dec(i);
  end;
  ExternLibs.Free;
  inherited Done;
end;

{******************************************************************************}
{ Methoden der Klasse TSymHashTable                                            }
{******************************************************************************}

function TSymHashTable.OnGetHashValue { ermittelt einen Longint Wert f�r }
  ({ die Hash-Funktion (noch Index sp�ter }
  Item: Pointer { Symbolname }
  ): LongInt;
begin
  if Assigned(Item) then
    OnGetHashValue := PSGroup(Item)^.GetHashIndex
  else
    OnGetHashValue := 0;
end;

function TSymHashTable.OnCompareItems { liefert True, wenn die beiden Elemente }
  ({ gleich sind }
  Item1,
  Item2: Pointer
  ): Boolean;
var
  name1, name2: AnsiString;
  lib1, lib2: AnsiString;
begin
  if Assigned(Item1) and Assigned(Item2) then
  begin
    name1 := Lowercase(PSGroup(Item1)^.GetName);
    name2 := Lowercase(PSGroup(Item2)^.GetName);
    lib1 := Lowercase(PSGroup(Item1)^.GetLibName);
    lib2 := Lowercase(PSGroup(Item2)^.GetLibName);
    Result := (StrComp(PChar(name1), PChar(name2)) + StrComp(PChar(lib1), PChar(lib2))) = 0;
  end
  else
  begin
    Result := False;
  end;
end;

procedure TSymHashTable.OnFreeItem(Item: Pointer);
begin
{    Dispose(PIndex(Item),Done);    noch nit }
end;

procedure TSymbols.Assign(ASymbols: PSymbols);
var
  Cnt: Integer;
begin
  LastText := ASymbols^.LastText;
  LastRText := ASymbols^.LastRText;
  LastBusGraph := ASymbols^.LastBusGraph;
  LastSymbol := ASymbols^.LastSymbol;
  LastPixel := ASymbols^.LastPixel;
  LastGroup := ASymbols^.LastGroup;
  LastPoly := ASymbols^.LastPoly;
  LastCPoly := ASymbols^.LastCPoly;
  LastSpline := ASymbols^.LastSpline;
  LastImage := ASymbols^.LastImage;
  LastMesLine := ASymbols^.LastMesLine;
  LastCircle := ASymbols^.LastCircle;
  LastArc := ASymbols^.LastArc;
  LastOLEObj := ASymbols^.LastOLEObj;
  FileVersion := ASymbols^.FileVersion;
  SymTable.Clear;
//   Dispose(Data,Done);

  Data^.DeleteAll;

   {for Cnt:=0 to ASymbols^.Data^.GetCount-1 do begin
       Data^.Insert(ASymbols^.Data^.At(Cnt));
   end;
   }
  Data := MakeObjectCopy(ASymbols^.Data);

  SymTable.Capacity := ASymbols^.SymTable.Capacity;
  for Cnt := 0 to Data.GetCount - 1 do
    SymTable.Add(Data.At(Cnt));
//   ExternLibs.Clear;
//   for Cnt:=0 to ASymbols^.ExternLibs.Count-1 do
//       ExternLibs.Add(ASymbols^.ExternLibs[Cnt]);
end;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}
{******************************************************************************}
{ Methods of the class TChartsHashTable                                        }
{******************************************************************************}

function TChartsHashTable.OnGetHashValue(Item: Pointer): LongInt;
begin
  if Assigned(Item) then
    OnGetHashValue := PChartKind(Item)^.GetHashIndex
  else
    OnGetHashValue := 0;
end;

function TChartsHashTable.OnCompareItems(Item1, Item2: Pointer): Boolean;
begin
  OnCompareItems := False;
end;

procedure TChartsHashTable.OnFreeItem(Item: Pointer);
begin
  if Item <> nil then
    Dispose(Item);
end;

{$IFNDEF AXDLL} // <----------------- AXDLL

function TChartsHashTable.KeyOfValue(Item: Pointer): LongInt;
begin
  Result := PChartKind(Item)^.Index;
end;
{$ENDIF} // <----------------- AXDLL

{******************************************************************************}
{ Methods of the Object TChartsLib                                             }
{******************************************************************************}

constructor TChartsLib.Init(AOwner: PLayer);
begin
  inherited Init(AOwner);
  ChartsTable := TChartsHashTable.Create(ChartsTableInitCapacity);
end;

constructor TChartsLib.Load(S: TOldStream);

  procedure DoAll(AItem: PIndex); far;
  begin
    ChartsTable.Add(AItem);
  end;

var
  Version: Byte;
begin
  inherited Load(S);
  S.Read(Version, sizeof(Version));

  ChartsTable := TChartsHashTable.Create(LimitToLong(ChartsTableExpandTo * Data^.GetCount + ChartsTableInitCapacity));
  Data^.ForEach(@DoAll);
end;

procedure TChartsLib.Store(S: TOldStream);
var
  Version: Byte;
begin
  inherited Store(S);
  Version := ChartsLibFileVersion;
  S.Write(Version, sizeof(Version));
end;

destructor TChartsLib.Done;
var
  Cnt: Integer;
  KindsCount: Integer;
  ChartKindPtr: PChartKind;
begin
  if (ChartsTable.Count > 0) then
  begin
    KindsCount := ChartsTable.Count;
    for Cnt := 0 to ChartsTable.Capacity - 1 do
    begin
      if (ChartsTable.Items[Cnt] <> nil) and (ChartsTable.Items[Cnt] <> MarkedAsFree) then
      begin
        ChartKindPtr := PChartKind(ChartsTable.Items[Cnt]);
        ChartsTable.Delete(ChartsTable.Items[Cnt]);
        ChartKindPtr^.Done;
        Dec(KindsCount);
        if KindsCount = 0 then
          Break;
      end;
    end;
  end;
  ChartsTable.Free;
end;

function TChartsLib.AddChartKind(AKind: PChartKind; IsNewChart: Boolean): PChartKind;
begin
  Result := AKind;
  if not IsNewChart then
  begin
    Result := PChartKind(ChartsTable.Find(AKind));
    if Result = nil then
    begin
      SetIndex(AKind);
      Data^.Insert(AKind);
      with ChartsTable do
      begin
        Add(AKind);
        if ((Count / Capacity) > ChartsTableExpandLevel) then
        begin
          Capacity := Trunc(Count * ChartsTableExpandTo);
        end;
      end;
      Result := AKind;
      Inc(Result^.UseCount);
    end;
  end
  else
  begin
    SetIndex(AKind);
    Data^.Insert(AKind);
    with ChartsTable do
    begin
      Add(AKind);
      if ((Count / Capacity) > ChartsTableExpandLevel) then
      begin
        Capacity := Trunc(Count * ChartsTableExpandTo);
      end;
    end;
    Result := AKind;
    Inc(Result^.UseCount);
  end;
end;

function TChartsLib.DeleteChartKind(AKind: PChartKind): Boolean;
var
  BKind: PChartKind;
begin
  Result := FALSE;
  if AKind <> nil then
  begin
    BKind := PChartKind(ChartsTable.Find(AKind));
    if BKind <> nil then
    begin
      Result := TRUE;
      if AKind <> BKind then
        AKind^.Free;
      Dec(BKind^.UseCount);
      if BKind^.UseCount < 1 then
      begin
        Result := ChartsTable.Delete(BKind);
        Data^.Delete(BKind);
        BKind^.Free;
      end;
    end
    else
      AKind^.Free;
  end;
end;

function TChartsLib.FindChartKind(KindIndex: LongInt; HashIndex: LongInt): PChartKind;
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  Result := ChartsTable.FindByIndex(HashIndex, KindIndex);
{$ENDIF} // <----------------- AXDLL
end;

procedure TChartsLib.InitChartKinds(PInfo: PPaint);
var
  ObjList: TList;
  AChartsLibPtr: PChartsLib;
  Ind: Integer;
  Cnt: Integer;
  OldBusGraphPtr: POldBusGraph;
  View3D: Boolean;
  MarksStyle: TSeriesMarksStyle;
  MarksTrasparent: Boolean;
  MarksColor: TColor;
  MarksShadowColor: TColor;
  MarksFont: PFontDes;
  MarksFontName: string;
  MarksFontCharset: TFontCharset;
  AChartKindPtr: PChartKind;
  NewObjectPtr: PBusGraph;
  ShowValues: Boolean; // Show Values Labels (TRUE-yes, FALSE-no)
  MarksVisible: Boolean; // Show Marks Labels  (TRUE-yes, FALSE-no)
  Width: Integer;
  Height: Integer;
  THeight: Integer;
  BackRatio: Double;
  tmpMinValue: Double;
  TmpItem: PDGraph;
  tmpString: string;
  TWidth: LongInt;
  tmpValue: LongInt;
  MarginLeft: Integer;
  MarginTop: Integer;
  MarginRight: Integer;
  MarginBottom: Integer;
  AxisVisible: Boolean;
  AxisFontName: string;
  AxisFontCharset: TFontCharset;
  LeftAxisVisible: Boolean;
  LeftAxisLabels: Boolean;
  LeftALabStyle: TAxisLabelStyle;
  LeftALabAngle: Integer;
  LeftALabSize: Integer;
  BottomAxisVisible: Boolean;
  BottomAxisGrdVsbl: Boolean;
  BottomAxisLabels: Boolean;
  BottomALabStyle: TAxisLabelStyle;
  BottomALabAngle: Integer;
  BottomALabSize: Integer;
begin

  if (PInfo^.OldBusGraphList <> nil) and (PInfo^.Fonts <> nil) then
  begin
    AChartsLibPtr := PChartsLib(PInfo^.ChartViewKinds);
    ObjList := POldBuGraList(PInfo^.OldBusGraphList)^.ObjList;
    for Ind := ObjList.Count - 1 downto 0 do
    begin
      OldBusGraphPtr := ObjList.Items[Ind];

      View3D := FALSE;

      if OldBusGraphPtr^.DiagParams and dp_ShowVals > 0 then
        ShowValues := TRUE
      else
        ShowValues := FALSE;

      if OldBusGraphPtr^.DiagParams and dp_ValAbsRel > 0 then
        MarksStyle := smsValue
      else
        MarksStyle := smsPercent;
      MarksTrasparent := OldBusGraphPtr^.SVFont.Style and ts_Transparent <> 0;
      MarksColor := clWhite;
      MarksShadowColor := clWhite;

      MarksFont := PInfo^.Fonts^.GetFont(OldBusGraphPtr^.SVFont.Font);
      MarksFontName := MarksFont.Name^;
      MarksFontCharset := MarksFont.CharSet;

      AxisFontName := MarksFontName;
      AxisFontCharset := MarksFontCharset;

      NewObjectPtr := OldBusGraphPtr^.NewObjectPtr;

      NewObjectPtr^.RelAbsSize := FALSE;
      NewObjectPtr^.OffsetX := 0;
      NewObjectPtr^.OffsetY := 0;

      tmpMinValue := PDGraph(NewObjectPtr^.ValItems^.Items[0]).Value;
      if ShowValues then
      begin
        if MarksStyle = smsValue then
        begin
          TWidth := 0;
          for Cnt := 0 to NewObjectPtr^.ValItems^.Count - 1 do
          begin
            TmpItem := PDGraph(NewObjectPtr^.ValItems^.Items[Cnt]);
            Str(TmpItem.Value: 0: 0, tmpString);
            tmpValue := Round(OldBusGraphPtr^.SVFont.Height * PInfo^.GetTextRatio(OldBusGraphPtr^.SVFont, tmpString));
            if tmpValue > TWidth then
              TWidth := tmpValue;
            if TmpItem.Value < tmpMinValue then
              tmpMinValue := TmpItem.Value;
          end;
        end
        else
          TWidth := Round(OldBusGraphPtr^.SVFont.Height * PInfo^.GetTextRatio(OldBusGraphPtr^.SVFont, '999,99 %'));
        tmpValue := TWidth mod co_FontWidthPrec;
        TWidth := TWidth div co_FontWidthPrec;
        if tmpValue > 0 then
          Inc(TWidth);
        TWidth := TWidth * co_FontWidthPrec;
      end
      else
        for Cnt := 0 to NewObjectPtr^.ValItems^.Count - 1 do
        begin
          TmpItem := PDGraph(NewObjectPtr^.ValItems^.Items[Cnt]);
          if TmpItem.Value < tmpMinValue then
            tmpMinValue := TmpItem.Value;
        end;

                // temporarily
      BackRatio := co_BaseChartSize / OldBusGraphPtr^.NCDiameter;
      THeight := Round(OldBusGraphPtr^.SVFont.Height * BackRatio);

      MarginLeft := 0;
      MarginTop := 0;
      MarginRight := 0;
      MarginBottom := 0;

      AxisVisible := FALSE;
      LeftAxisVisible := FALSE;
      LeftAxisLabels := FALSE;
      LeftALabStyle := talNone;
      LeftALabAngle := 0;
      LeftALabSize := 0;
      BottomAxisVisible := FALSE;
      BottomAxisGrdVsbl := FALSE;
      BottomAxisLabels := FALSE;
      BottomALabStyle := talNone;
      BottomALabAngle := 0;
      BottomALabSize := 0;
      MarksVisible := FALSE;

      case NewObjectPtr^.DiagType of
        dtCircleDiag:
          begin
            BackRatio := co_BaseChartSize / OldBusGraphPtr^.NCDiameter;
            if OldBusGraphPtr^.DiagParams and dp_CDiamAbsRel > 0 then
            begin
              NewObjectPtr^.RelAbsSize := TRUE;
              BackRatio := BackRatio / co_RelChartPercent;
            end;
            MarksVisible := ShowValues;
            if MarksVisible then
            begin
              NewObjectPtr^.OffsetX := -TWidth;
              NewObjectPtr^.OffsetY := TWidth;
              if NewObjectPtr^.RelAbsSize then
              begin
                TWidth := TWidth * 2 + OldBusGraphPtr^.NCDiameter * co_RelChartPercent;
                Width := Round(TWidth * BackRatio);
                TWidth := TWidth + Round(OldBusGraphPtr^.NCDiameter * (NewObjectPtr^.Sum / 100 - co_RelChartPercent));
              end
              else
              begin
                TWidth := TWidth * 2 + OldBusGraphPtr^.NCDiameter;
                Width := Round(TWidth * BackRatio);
              end;
              Height := Width;
              NewObjectPtr^.ChartWidth := TWidth;
              NewObjectPtr^.ChartHeight := TWidth;
            end
            else
            begin
              Width := co_BaseChartSize;
              Height := co_BaseChartSize;
              if NewObjectPtr^.RelAbsSize then
                TWidth := Round(OldBusGraphPtr^.NCDiameter * NewObjectPtr^.Sum * (co_RelChartPercent / 100))
              else
                TWidth := OldBusGraphPtr^.NCDiameter;
              NewObjectPtr^.ChartWidth := TWidth;
              NewObjectPtr^.ChartHeight := TWidth;
            end;
          end;
        dtBeamDiag:
          begin
            NewObjectPtr^.ChartWidth := OldBusGraphPtr^.BWidth * NewObjectPtr^.ValItems^.Count;
            NewObjectPtr^.ChartHeight := OldBusGraphPtr^.GesHeight;
            if ShowValues then
            begin
              BottomAxisLabels := TRUE;
              BottomALabStyle := talMark;
              BottomALabAngle := 90;
              NewObjectPtr^.ChartHeight := NewObjectPtr^.ChartHeight + TWidth;
            end;
            BackRatio := co_BaseChartSize / NewObjectPtr^.ChartHeight;
            if ShowValues then
              BottomALabSize := Round(TWidth * BackRatio);
            AxisVisible := TRUE;
            BottomAxisVisible := TRUE;
            Height := co_BaseChartSize;
            Width := Round(BackRatio * NewObjectPtr^.ChartWidth);
          end;
        dtLineDiag,
          dtAreaDiag,
          dtPointDiag:
          begin
            NewObjectPtr^.ChartWidth := OldBusGraphPtr^.GesWidth + OldBusGraphPtr^.BWidth;
            NewObjectPtr^.ChartHeight := OldBusGraphPtr^.GesHeight + Round(OldBusGraphPtr^.BWidth * 0.2);
            NewObjectPtr^.OffsetX := -Round(OldBusGraphPtr^.BWidth * 0.5);
            MarginLeft := Round(OldBusGraphPtr^.BWidth * 100 / NewObjectPtr^.ChartWidth);
            MarginRight := MarginLeft;
            if ShowValues then
            begin
              BottomAxisLabels := TRUE;
              BottomALabStyle := talMark;
              BottomALabAngle := 90;
              NewObjectPtr^.ChartHeight := NewObjectPtr^.ChartHeight + TWidth;
            end;
            BackRatio := co_BaseChartSize / NewObjectPtr^.ChartHeight;
            if ShowValues then
              BottomALabSize := Round(TWidth * BackRatio);
            AxisVisible := TRUE;
            BottomAxisVisible := TRUE;
            BottomAxisGrdVsbl := TRUE;
            Height := co_BaseChartSize;
            Width := Round(BackRatio * NewObjectPtr^.ChartWidth);
          end;
      end; // End of CASE-statement

      THeight := Round(OldBusGraphPtr^.SVFont.Height * BackRatio);
      AChartKindPtr := New(PChartKind,
        InitKind(NewObjectPtr^.DiagType, Width, Height, View3D,
        TRUE, bvNone, bvNone, 1, bsNone, 0,
        MarginLeft, MarginTop, MarginRight, MarginBottom, TRUE, FALSE, AxisVisible,
        AxisFontName, AxisFontCharset, THeight,
        LeftAxisVisible, FALSE, FALSE, FALSE, LeftAxisLabels, 0, LeftALabStyle, LeftALabAngle, LeftALabSize,
        BottomAxisVisible, BottomAxisGrdVsbl, FALSE, FALSE, BottomAxisLabels, 0, BottomALabStyle, BottomALabAngle, BottomALabSize,
        MarksVisible, MarksStyle, MarksFontName, MarksFontCharset, THeight,
        MarksTrasparent, MarksColor, FALSE, 0, MarksShadowColor));

      case NewObjectPtr^.DiagType of
        dtCircleDiag:
          begin
{$IFNDEF AXDLL} // <----------------- AXDLL
            AChartKindPtr^.TemplateChart.View3DOptions.ZoomText := FALSE;
            TPieSeries(AChartKindPtr^.TemplateChartSeries).Circled := TRUE;
                                     // equivalent NCDiameter
            TPieSeries(AChartKindPtr^.TemplateChartSeries).CustomXRadius := co_HalfChartSize;
            TPieSeries(AChartKindPtr^.TemplateChartSeries).CustomYRadius := co_HalfChartSize;
{$ENDIF} // <----------------- AXDLL
          end;
        dtBeamDiag:
          begin
{$IFNDEF AXDLL} // <----------------- AXDLL
            TBarSeries(AChartKindPtr^.TemplateChartSeries).BarStyle := bsRectangle;
            TBarSeries(AChartKindPtr^.TemplateChartSeries).BarWidthPercent := 100;
            TBarSeries(AChartKindPtr^.TemplateChartSeries).SideMargins := FALSE;
            AChartKindPtr^.TemplateChart.BackWall.Frame.Visible := FALSE;
{$ENDIF} // <----------------- AXDLL
          end;
        dtLineDiag,
          dtAreaDiag:
          begin
{$IFNDEF AXDLL} // <----------------- AXDLL
            AChartKindPtr^.TemplateChart.LeftAxis.AutomaticMaximum := TRUE;
            if tmpMinValue < 0 then
              AChartKindPtr^.TemplateChart.LeftAxis.AutomaticMinimum := TRUE
            else
            begin
              AChartKindPtr^.TemplateChart.LeftAxis.AutomaticMinimum := FALSE;
              AChartKindPtr^.TemplateChart.LeftAxis.Minimum := 0;
            end;
            AChartKindPtr^.TemplateChart.BackWall.Frame.Visible := TRUE;
{$ENDIF} // <----------------- AXDLL
          end;
        dtPointDiag:
          begin
{$IFNDEF AXDLL} // <----------------- AXDLL
            TPointSeries(AChartKindPtr^.TemplateChartSeries).Pointer.Style := psRectangle;
            TPointSeries(AChartKindPtr^.TemplateChartSeries).Pointer.HorizSize := Round(OldBusGraphPtr^.GesWidth / 50 * BackRatio);
            TPointSeries(AChartKindPtr^.TemplateChartSeries).Pointer.VertSize := TPointSeries(AChartKindPtr^.TemplateChartSeries).Pointer.HorizSize;
            AChartKindPtr^.TemplateChart.LeftAxis.AutomaticMaximum := TRUE;
            if tmpMinValue < 0 then
              AChartKindPtr^.TemplateChart.LeftAxis.AutomaticMinimum := TRUE
            else
            begin
              AChartKindPtr^.TemplateChart.LeftAxis.AutomaticMinimum := FALSE;
              AChartKindPtr^.TemplateChart.LeftAxis.Minimum := 0;
            end;
            AChartKindPtr^.TemplateChart.BackWall.Frame.Visible := TRUE;
{$ENDIF} // <----------------- AXDLL
          end;
      end; // End of CASE-statement

      with NewObjectPtr^ do
      begin
        ChartKindPtr := AChartsLibPtr^.AddChartKind(AChartKindPtr, False);
        ChartHashIndex := AChartKindPtr^.GetHashIndex;
        ChartKindIndex := ChartKindPtr^.Index;
        CalculateClipRect;
      end;

                // Moor has made his business, - Moor can die
      ObjList.Delete(Ind);
      OldBusGraphPtr^.Free;
    end;

    POldBuGraList(PInfo^.OldBusGraphList)^.Done;
    PInfo^.OldBusGraphList := nil;
  end;

end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

{******************************************************************************}
{ Registrierungsteil                                                           }
{******************************************************************************}

const
  RObjectsOld: TStreamRec = (
    ObjType: rn_ObjectsOld;
    VmtLink: TypeOf(TObjects);
    Load: @TObjects.LoadOld;
    Store: nil);

  RObjects: TStreamRec = (
    ObjType: rn_Objects;
    VmtLink: TypeOf(TObjects);
    Load: @TObjects.Load;
    Store: @TObjects.Store);

  RSymsOld3: TStreamRec = (
    ObjType: rn_SymOld3;
    VmtLink: TypeOf(TSymbols);
    Load: @TSymbols.LoadOld3;
    Store: @TSymbols.Store);

  RSymsOld2: TStreamRec = (
    ObjType: rn_SymsOld2;
    VmtLink: TypeOf(TSymbols);
    Load: @TSymbols.LoadOld2;
    Store: @TSymbols.Store);

  RSymbols: TStreamRec = (
    ObjType: rn_Symbols;
    VmtLink: TypeOf(TSymbols);
    Load: @TSymbols.LoadV0;
    Store: nil);

  RSymbolsOld: TStreamRec = (
    ObjType: rn_SymbolsOld;
    VmtLink: TypeOf(TSymbols);
    Load: @TSymbols.LoadOld;
    Store: nil);

  RObjOld2: TStreamRec = (
    ObjType: rn_ObjOld2;
    VmtLink: TypeOf(TObjects);
    Load: @TObjects.LoadOld2;
    Store: nil);

  RObjOld3: TStreamRec = (
    ObjType: rn_ObjOld3;
    VmtLink: TypeOf(TObjects);
    Load: @TObjects.LoadOld3;
    Store: nil);

  RSymbolsV1: TStreamRec = (
    ObjType: rn_SymbolsV1;
    VmtLink: TypeOf(TSymbols);
    Load: @TSymbols.Load;
    Store: @TSymbols.Store);

{++ Moskaliov Business Graphics BUILD#150 17.01.01}
  RChartsLib: TStreamRec = (
    ObjType: rn_ChartsLib;
    VmtLink: TypeOf(TChartsLib);
    Load: @TChartsLib.Load;
    Store: @TChartsLib.Store);
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

begin
  RegisterType(RObjectsOld);
  RegisterType(RSymbolsOld);
  RegisterType(RSymsOld2);
  RegisterType(RSymsOld3);
  RegisterType(RSymbols);
  RegisterType(RObjOld2);
  RegisterType(RObjOld3);
  RegisterType(RObjects);
  RegisterType(RSymbolsV1);
  RegisterType(RChartsLib); {++ Moskaliov Business Graphics BUILD#150 17.01.01}
end.

