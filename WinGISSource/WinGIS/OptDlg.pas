Unit OptDlg;

Interface

Uses WinProcs,WinTypes, Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     StdCtrls, WCtrls,Menus,Tabnotbk,Validate,Spinbtn,AM_Def,MultiDlg,
     ComCtrls, MultiLng, Spin, ExtCtrls, DigIntf;

Type TOptionsDialog = class(TMultiFormDialog)
       AutoSaveCheck    : TCheckBox;
       AutoSaveEdit     : TEdit;
       AutoSaveSpin     : TSpinBtn;
       AutoSaveVal      : TIntValidator;
       BackupFileCheck  : TCheckBox;
       CancelBtn        : TButton;
       DigitizerCombo   : TComboBox;
       DigitizerSetupBtn: TButton;
       Group1           : TWGroupBox;
       Group2           : TWGroupBox;
       Label1           : TLabel;
       Label4           : TLabel;
       MlgSection       : TMlgSection;
       NoteBook         : TWTabbedNoteBook;
       OkBtn            : TButton;
    WGroupBox3: TWGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    UnitsCombo: TComboBox;
    ScaleEdit: TEdit;
    ScaleSpin: TSpinBtn;
    DlgTemplate: TOpenDialog;
    EditTemplate: TEdit;
    Button1: TButton;
    LabelTemplate: TLabel;
    EditHistSize: TEdit;
    SpinBtnHistSize: TSpinBtn;
    Label7: TLabel;
    HistSizeValidator: TIntValidator;
    Label8: TLabel;
    AreaCombo: TComboBox;
    LabelIni: TLabel;
       Procedure   AutoSaveCheckClick(Sender: TObject);
       Procedure   DigitizerSetupBtnClick(Sender: TObject);
       Procedure   FormCreate(Sender: TObject);
       Procedure   FormHide(Sender: TObject);
       Procedure   FormShow(Sender: TObject);
       procedure   FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure Button1Click(Sender: TObject);
    procedure OkBtnClick(Sender: TObject);
    procedure DigitizerComboChange(Sender: TObject);
    procedure AreaComboChange(Sender: TObject);
    procedure LabelIniClick(Sender: TObject);
      Private
      Public
     end;

Implementation

{$R *.DFM}

Uses AM_Child,AM_Dev,AM_Ini,AM_Main,Measures,Objects,ResDlg,StyleDef,UserIntf,IniFiles,VerMgr,FileCtrl,
     LicenseHndl,WinOSInfo,ShellApi;

Procedure TOptionsDialog.FormShow(Sender: TObject);
var AList          : PStrCollection;
    Cnt            : Integer;
begin
  with IniFile^ do begin
    { Digitizer }
    if GetList(TRUE,'INPUTDEVICES',AList) then begin
      for Cnt:=0 to AList^.Count-1 do DigitizerCombo.Items.Add(StrPas(AList^.At(Cnt)));
      DigitizerCombo.ItemIndex:=DigitizerCombo.Items.IndexOf(StrPas(DigitData.DefInput));
      Dispose(AList,Done);
    end;

    case AreaFact of
           1: AreaCombo.ItemIndex:=0;
           10000: AreaCombo.ItemIndex:=1;
           1000000: AreaCombo.ItemIndex:=2;
    else
          AreaCombo.ItemIndex:=0;
          AreaFact:=1;
    end;

    with Options do begin
      BackupFileCheck.Checked:=BackupFile;
      AutoSaveCheck.Checked:=AutoSave;
      AutoSaveEdit.Enabled:=AutoSave;
      AutoSaveVal.AsInteger:=SaveInterv;
      EditHistSize.Text:=IntToStr(HistSize);
    end;
  end;
  if WingisLicense < WG_STD then begin
     BackupFileCheck.Checked:=False;
     BackupFileCheck.Enabled:=False;
     AutoSaveCheck.Checked:=False;
     AutoSaveCheck.Enabled:=False;
     AutoSaveEdit.Enabled:=False;
  end;
  Notebook.PageIndex:=0;
end;

Procedure TOptionsDialog.FormHide(Sender: TObject);
  begin
    if ModalResult=mrOK then with IniFile^ do begin
      with DigitData do begin
        StrPCopy(DefInput,DigitizerCombo.Items[DigitizerCombo.ItemIndex]);
        SetDefaultInput(DefInput);
        SetInput(DefInput);
      end;
      with Options do begin
        BackupFile:=BackupFileCheck.Checked;
        AutoSave:=AutoSaveCheck.Checked;
        AutoSave:=AutoSaveEdit.Enabled;
        SaveInterv:=AutoSaveVal.AsInteger;
        HistSize:=StrToIntDef(EditHistSize.Text,4);
        WingisMainForm.FileHistory.MaxEntryCount:=HistSize;
      end;
    end;
  end;

Procedure TOptionsDialog.DigitizerSetupBtnClick(Sender: TObject);
  var AData        : TDigitData;
  begin
  Digitizer.EndDigit;
    StrPCopy(AData.DefInput,DigitizerCombo.Items[DigitizerCombo.ItemIndex]);
    IniFile^.ReadInput(AData.DefInput,@AData);
    if AData.WinTAB then begin
      if ExecDialog(TWinTABDlg.Init(Self,@AData))=id_OK then IniFile^.SaveInput(@AData);
    end
    else if ExecDialog(TConnDlg.Init(Self,@AData))=id_OK then IniFile^.SaveInput(@AData);
  end;

Procedure TOptionsDialog.AutoSaveCheckClick(Sender: TObject);
  begin
    AutoSaveEdit.Enabled:=AutoSaveCheck.Checked;
  end;

Procedure TOptionsDialog.FormCreate(Sender: TObject);
var
Ini: TIniFile;
begin
    RemoveFirstPage:=FALSE;
    DialogNotebook:=Notebook;
    Ini:=TIniFile.Create(OSInfo.WingisIniFileName);
    if Ini <> nil then begin
       EditTemplate.Text:=Ini.ReadString('Settings','TemplateFile','');
       Ini.Free;
    end;
end;

procedure TOptionsDialog.FormCloseQuery(Sender:TObject;var CanClose:Boolean);
begin
  CanClose:=CheckValidators(Self);
end;

procedure TOptionsDialog.Button1Click(Sender: TObject);
begin
     if DlgTemplate.Execute then begin
        EditTemplate.Text:=DlgTemplate.FileName;
     end;
end;

procedure TOptionsDialog.OkBtnClick(Sender: TObject);
var
Ini: TIniFile;
begin
     Ini:=TIniFile.Create(OSInfo.WingisIniFileName);
     if Ini <> nil then begin
        Ini.WriteString('Settings','TemplateFile',EditTemplate.Text);
        Ini.WriteInteger('Settings','AreaDivider',IniFile^.AreaFact);
        Ini.Free;
     end;
end;
procedure TOptionsDialog.DigitizerComboChange(Sender: TObject);
begin
     Digitizer.EndDigit;
end;

procedure TOptionsDialog.AreaComboChange(Sender: TObject);
begin
     case AreaCombo.ItemIndex of
          0: IniFile^.AreaFact:=1;
          1: IniFile^.AreaFact:=10000;
          2: IniFile^.AreaFact:=1000000;
     end;
end;

procedure TOptionsDialog.LabelIniClick(Sender: TObject);
begin
     ShellExecute(0,'open',PChar(OSInfo.WingisIniFileName),nil,nil,SW_SHOW);
     ModalResult:=mrCancel;
end;

end.
