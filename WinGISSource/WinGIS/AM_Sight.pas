{*******************************************************************************
| Unit
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
| Modifikationen
|  16.02.1994 Martin Forst
+******************************************************************************}
unit AM_Sight;

interface

uses Messages, WinTypes, WinProcs, AM_Def, AM_Coll, AM_Admin, AM_Layer, Objects,
  Controls, ResDlg, GrTools;

const
  colInit = 10;
  colExpand = 5;

  cosInit = 10;
  cosExpand = 5;

  slSightName = 100;

  lf_LayerOff = $80000000;
  lf_GenerOn = $40000000;
  lf_HSDVisible = $20000000;

type
  PSightLData = ^TSightLData;
  TSightLData = object(TOldObject)
    LIndex: LongInt;
    LFlags: LongInt;
    GeneralMin: Double;
    GeneralMax: Double;
    constructor Init(AIndex, AFlags: LongInt; AGenMin, AGenMax: Double);
    constructor Load(S: TOldStream);
    destructor Done; virtual;
    procedure Store(S: TOldStream); virtual;
  end;

  PPSight = ^PSight;
  PSight = ^TSight;
  TSight = object(TOldObject)
    Name: PString;
    ViewRect: TRotRect;
    VisibleRect: TDRect;
    LayersData: PCollection;
    Description: string;
    constructor Init(AName: string; const ARect: TRotRect);
    constructor Load(S: TOldStream);
    constructor LoadNew(S: TOldStream);
    constructor Load40(S: TOldStream);
    destructor Done; virtual;
    procedure ConvertToHSD;
    procedure SetLayers(ALayers: PLayers);
    procedure Store(S: TOldStream); virtual;
  end;

  PSights = ^TSights;
  TSights = object(TSortedCollection)
    Default: Integer16;
    constructor Init(AInit, AExpand: Integer);
    constructor Load(S: TOldStream);
    function Compare(Key1, Key2: Pointer): Integer; virtual;
    procedure Store(S: TOldStream);
    procedure SetGeneralData(ALayers: PLayers);
    function NamedSight(const Name: string): PSight;
  end;

  PSightData = ^TSightData;
  TSightData = object(TOldObject)
    Name: PString;
    SaveSight: Boolean;
    SaveLayer: Boolean;
    AsDefault: Boolean;
    constructor Init;
    constructor Load(S: TOldStream);
    destructor Done; virtual;
    procedure Store(S: TOldStream);
  end;

implementation

uses NumTools, SysUtils, Classes, StyleDef;

const
  id_Name = 100;
  id_SaveS = 101;
  id_SaveL = 102;

constructor TSightLData.Init
  (
  AIndex: LongInt;
  AFlags: LongInt;
  AGenMin: Double;
  AGenMax: Double
  );
begin
  inherited Init;
  LIndex := AIndex;
  LFlags := AFlags;
  GeneralMin := AGenMin;
  GeneralMax := AGenMax;
end;

destructor TSightLData.Done;
begin
  inherited Done;
end;

constructor TSightLData.Load
  (
  S: TOldStream
  );
begin
  inherited Init;
  S.Read(LIndex, SizeOf(LIndex));
  S.Read(LFlags, SizeOf(LFlags));
  S.Read(GeneralMin, SizeOf(GeneralMin));
  S.Read(GeneralMax, SizeOf(GeneralMax));
end;

procedure TSightLData.Store
  (
  S: TOldStream
  );
begin
  S.Write(LIndex, SizeOf(LIndex));
  S.Write(LFlags, SizeOf(LFlags));
  S.Write(GeneralMin, SizeOf(GeneralMin));
  S.Write(GeneralMax, SizeOf(GeneralMax));
end;

constructor TSight.Init
  (
  AName: string;
  const ARect: TRotRect
  );
begin
  inherited Init;
  Name := NewStr(AName);
  ViewRect := ARect;
  LayersData := nil;
end;

destructor TSight.Done;
begin
  DisposeStr(Name);
  if LayersData <> nil then
    Dispose(LayersData, Done);
  inherited Done;
end;

constructor TSight.Load
  (
  S: TOldStream
  );

  procedure DoAll
      (
      Item: Pointer
      ); far;
  var
    LayerIndex: LongInt;
    LayerFlags: LongInt;
    LValues: PSightLData;
  begin
    LayerIndex := LongInt(Item);
    LayerFlags := LayerIndex and lf_LayerOff;
    LayerIndex := LayerIndex and not lf_LayerOff;
    LValues := New(PSightLData, Init(LayerIndex, LayerFlags, GENERALIZE_MIN, GENERALIZE_MAX));
    LayersData^.Insert(LValues);
  end;
var
  Overview: Boolean;
  ARect: TDRect;
  Layers: PLongColl;
begin
  Name := S.ReadStr;
  ARect.Load(S);
  with ARect do
    ViewRect := RotRect(A.X, A.Y, XSize, YSize, 0);
  S.Read(OverView, SizeOf(Overview));
  Layers := Pointer(S.Get);
  if Layers <> nil then
  begin
    LayersData := New(PCollection, Init(colInit, colExpand));
    Layers^.ForEach(@DoAll);
    Dispose(Layers, Done);
  end
  else
    LayersData := nil;
end;

constructor TSight.LoadNew
  (
  S: TOldStream
  );
var
  Overview: Boolean;
  {****************************************************************************+
    Procedure ConvertOldGeneralization:
  ------------------------------------------------------------------------------
    Converts old generalization-information to a scale-value using current
    monitor-size (is not exact because the original monitor-size is not known).
  +****************************************************************************}

  procedure DoAll
      (
      Item: PSightLData
      ); far;
  var
    DC: THandle;
  begin
    with Item^ do
      if LFlags and lf_GenerOn = 0 then
      begin
        GeneralMin := GENERALIZE_MIN;
        GeneralMax := GENERALIZE_MAX;
      end
      else
      begin
        DC := GetDC(GetDesktopWindow);
        if DblCompare(GeneralMin, 0) = 0 then
          GeneralMin := GENERALIZE_MAX
        else
          GeneralMin := GetDeviceCaps(DC, HORZSIZE) / GeneralMin / 10.0;
        if DblCompare(GeneralMax, 2 * (MaxLongInt / 1)) = 0 then
          GeneralMax := GENERALIZE_MIN
        else
          if DblCompare(GeneralMax, 0) = 0 then
            GeneralMax := GENERALIZE_MIN
          else
            GeneralMax := GetDeviceCaps(DC, HORZSIZE) / GeneralMax / 10.0;
        Swap(GeneralMin, GeneralMax);
        ReleaseDC(GetDesktopWindow, DC);
      end;
  end;
var
  ARect: TDRect;
begin
  Name := S.ReadStr;
  ARect.Load(S);
  with ARect do
    ViewRect := RotRect(A.X, A.Y, XSize, YSize, 0);
  S.Read(OverView, SizeOf(Overview));
  LayersData := Pointer(S.Get);
  if LayersData <> nil then
    LayersData^.ForEach(@DoAll);
  if ProjectVersion >= 24 then
    Description := S.ReadString;
end;

constructor TSight.Load40
  (
  S: TOldStream
  );
begin
  Name := S.ReadStr;
  S.Read(ViewRect, SizeOf(ViewRect));
  LayersData := Pointer(S.Get);
  if ProjectVersion >= 24 then
    Description := S.ReadString;
end;

procedure TSight.Store
  (
  S: TOldStream
  );
begin
  S.WriteStr(Name);
  S.Write(ViewRect, SizeOf(ViewRect));
  S.Put(LayersData);
  S.WriteString(Description);
end;

procedure TSight.SetLayers
  (
  ALayers: PLayers
  );

  procedure DoAll
      (
      Item: PLayer
      ); far;
  var
    LValues: PSightLData;
    LayerFlags: LongInt;
  begin
    LayerFlags := 0;
    if Item^.GetState(sf_LayerOff) then
      LayerFlags := LayerFlags or lf_LayerOff;
    if (DblCompare(Item^.GeneralMin, GENERALIZE_MIN) > 0) or (DblCompare(Item^.GeneralMax, GENERALIZE_MAX) < 0) then
    begin
      LayerFlags := LayerFlags or lf_GenerOn;
    end;
    if Item^.GetState(sf_HSDVisible) then
      LayerFlags := LayerFlags or lf_HSDVisible;
    LValues := New(PSightLData, Init(Item^.Index, LayerFlags, Item^.GeneralMin, Item^.GeneralMax));
    LayersData^.Insert(LValues);
  end;
begin
  LayersData := New(PCollection, Init(colInit, colExpand));
  ALayers^.LData^.ForEach(@DoAll);
end;

procedure TSight.ConvertToHSD;
var
  Cnt: Integer;
  Item: PSightLData;
begin
  { set the hsd-visible flag equal to the layer visible flag }
  if LayersData <> nil then
    for Cnt := 0 to LayersData^.Count - 1 do
    begin
      Item := LayersData^.At(Cnt);
      if Item^.LFlags and lf_LayerOff <> 0 then
        Item^.LFlags := Item^.LFlags and not lf_HSDVisible
      else
        Item^.LFlags := Item^.LFlags or lf_HSDVisible
    end;
end;

constructor TSights.Init
  (
  AInit: Integer;
  AExpand: Integer
  );
begin
  TSortedCollection.Init(AInit, AExpand);
  Default := -1;
end;

constructor TSights.Load(S: TOldStream);
var
  List: TList;
  Cnt: Integer;
begin
  TSortedCollection.Load(S);
  S.Read(Default, SizeOf(Default));
  if Default >= Count then
    Default := -1;
  // WinGIS 3 didn't sort the items correclty
  List := TList.Create;
  for Cnt := 0 to Count - 1 do
    List.Add(At(Cnt));
  DeleteAll;
  for Cnt := 0 to List.Count - 1 do
    Insert(List[Cnt]);
  List.Free;
end;

procedure TSights.Store
  (
  S: TOldStream
  );
begin
  TSortedCollection.Store(S);
  S.Write(Default, SizeOf(Default));
end;

function TSights.Compare
  (
  Key1: Pointer;
  Key2: Pointer
  )
  : Integer;
var
  K1: PSight absolute Key1;
  K2: PSight absolute Key2;
begin
  Result := AnsiCompareText(K1^.Name^, K2^.Name^);
end;

procedure TSights.SetGeneralData
  (
  ALayers: PLayers
  );

  procedure DoAll
      (
      Item: PSight
      ); far;

    procedure AktLayer
        (
        Item1: PSightLData
        ); far;
    var
      ALayer: PLayer;
    begin
      ALayer := ALayers^.IndexToLayer(Item1^.LIndex);
      if ALayer <> nil then
      begin
        Item1^.GeneralMin := ALayer^.GeneralMin;
        Item1^.GeneralMax := ALayer^.GeneralMax;
      end;
    end;
  begin
    if Item^.LayersData <> nil then
      Item^.LayersData^.ForEach(@AktLayer);
  end;
begin
  ForEach(@DoAll);
end;

function TSights.NamedSight(const Name: string): PSight;
var
  ASight: PSight;
  AIndex: Integer;
begin
  ASight := New(PSight, Init(Name, RotRect(0, 0, 0, 0, 0)));
  if Search(ASight, AIndex) then
    Result := At(AIndex)
  else
    Result := nil;
  Dispose(ASight);
end;

constructor TSightData.Init;
begin
  inherited Init;
  Name := nil;
  SaveSight := TRUE;
  SaveLayer := TRUE;
end;

constructor TSightData.Load
  (
  S: TOldStream
  );
var
  Version: Word;
begin
  inherited Init;
  S.Read(Version, SizeOf(Version));
  Name := S.ReadStr;
  S.Read(SaveSight, SizeOf(SaveSight));
  S.Read(SaveLayer, SizeOf(SaveLayer));
end;

destructor TSightData.Done;
begin
  DisposeStr(Name);
  inherited Done;
end;

procedure TSightData.Store
  (
  S: TOldStream
  );
var
  Version: Word;
begin
  Version := 0;
  S.Write(Version, SizeOf(Version));
  S.WriteStr(Name);
  S.Write(SaveSight, SizeOf(SaveSight));
  S.Write(SaveLayer, SizeOf(SaveLayer));
end;

const
  RSight: TStreamRec = (
    ObjType: rn_Sight;
    VmtLink: TypeOf(TSight);
    Load: @TSight.Load;
    Store: @TSight.Store);

  RSightV33: TStreamRec = (
    ObjType: rn_SightV33;
    VmtLink: TypeOf(TSight);
    Load: @TSight.LoadNew;
    Store: @TSight.Store);

  RSightV40: TStreamRec = (
    ObjType: rn_SightV40;
    VmtLink: TypeOf(TSight);
    Load: @TSight.Load40;
    Store: @TSight.Store);

  RSights: TStreamRec = (
    ObjType: rn_Sights;
    VmtLink: TypeOf(TSights);
    Load: @TSights.Load;
    Store: @TSights.Store);

  RSightLData: TStreamRec = (
    ObjType: rn_SightLData;
    VmtLink: TypeOf(TSightLData);
    Load: @TSightLData.Load;
    Store: @TSightLData.Store);
begin
  RegisterType(RSight);
  RegisterType(RSightV33);
  RegisterType(RSightV40);
  RegisterType(RSights);
  RegisterType(RSightLData);
end.

