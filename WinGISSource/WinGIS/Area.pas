unit Area;
(*{$DEFINE TEST}*)
{$DEFINE TEST5}
interface
uses Messages,SysUtils,WinProcs,WinTypes,SlBase,Classes,Lists,Tree,SegTree,AM_Coll,Vector,Am_Stddl,
     AM_Def;

const isScolList = 10;
      isFrontList = 2;

Type PAreaData          = ^TAreaData;
     TAreaData          = Record
       Radius           : LongInt;
       Layers           : PLongColl;
       DestLayer        : LongInt;
       ErrorLayer       : LongInt;
       IntersectLayer   : LongInt;
       EnvelopeLayer    : LongInt;
       ERLayer          : Boolean;
       ISLayer          : Boolean;
       Frame            : Boolean;
       Trace            : Byte;
       Area             : Byte;
       Intersections    : Boolean;
       Envelope         : Boolean;
       DrawLayer        : longint;
     end;

     TVertex    = record
       x        : Double;
       y        : Double;
     end;

     TClipRect          = Record
       Left             : double;
       Right            : double;
       Bottom           : double;
       Top              : double;
     end;

     TEdge        = class;
     TLink        = class;
     TArea        = class
      private
       FEdgeList  : TSLBase;
       FNodeList  : TSLBase;
       FResultList: TList;
       FPointList : TList;
       FDrawList  : TList;
       FResultCol : TList;     {List or FResultList}
       Abbruch    : Boolean;
       {$IFDEF DEBUG}
       FName      : TextFile;
       {$ENDIF}
      public
       FinalList  : TSLBase;
       STree      : TSegTree;
       AList      : TSLBase;
       DeltaX     : LongInt;
       DeltaY     : LongInt;
       Radius     : LongInt;
       Proj       : Pointer{PProj};
       Data       : TAreaData;
       DrawRect   : TDRect;
       ProjSize   : TClipRect;
       AllAbort   : Boolean;
       DebugMessage: String;
       LastTreePos: TDPoint;
       ManualMode : Boolean;
       ManualPos  : TDPoint;
       Constructor Create(AProj:Pointer);
       Procedure ReadFile(FileName:TFileName);
       Procedure ConvertPolyToEdge;
       Procedure AddEdge(p,q:TSLBaseItem);
       Procedure Snap;
       Procedure Snap2;
       Procedure Snap3;
       Procedure EleminateEqual;                 {new}
       Procedure EleminateDouble;
       Procedure EleminateEqual2;                 {new}
       Procedure EleminateDouble2;
       Procedure RemoveEmptyNode;                {new}
       Procedure RemoveEmptyNode2;
       Procedure KillEdge(Edge:TEdge);
       Procedure KillEdge2(Edge:TEdge);
       Procedure KillWholeEdge(Edge:TEdge);
       Procedure SetNil(Edge:TEdge);
       Function  SetListCount(List:TSLBase):Longint;
       Procedure EleminateLines;
       Procedure EleminateLinesBack;
       Procedure MergeToPolyLines;
       Procedure MergeToPolyLinesBack;
       Procedure SortLinkList;
       Procedure SortList(List:TSLBase);
       Procedure Intersection;
       Procedure Intersection2;
       Function  TraverseList(var List:TRecList):Boolean;
       Procedure EstablishLink(Edge: TEdge);
       Procedure MergeLink;
       Procedure Trace;
       Procedure BackTrace;                      {new}
       Procedure BackTraverse(var go,Start:TEdge); {new}
       Procedure BackLink(var go,Start:TEdge); {new}
       Function  ActualiceTrace:Double;
       Procedure DrawFResultList;
       Procedure DrawFResultListEnvelope;
       Procedure DrawLines;                      {new}
       Procedure DrawPoints;
       Procedure DrawLines2(Dest:TSLBaseITem);
       Procedure Drawlines3;
       Procedure SaveFResultList(var f:TextFile);
       Procedure SelectLine(go:TEdge);
       Procedure DrawList;
       Procedure InsertEqual(AArea:TArea;p:TSLBaseItem;SItem1,SItem2:TSegItem);
       Procedure InsertAt1(AArea:TArea;p:TSLBaseItem;var SItem:TSegItem;SameVertex:Boolean);
       Procedure InsertAt2(AArea:TArea;p:TSLBaseItem;var SItem1,SItem2:TSegItem;flag:integer);
       Procedure InsertVertex(AArea:TArea;p:TSLBaseItem;Vertex:TDVertex;var SItem1,SItem2:TSegItem;flag:integer);
       Procedure InsertTail(AArea:TArea;Vertex:TDVertex;SItem1,SItem2:TSegItem);
       Procedure DeleteBackSegItems(p:TLink);
       Procedure PrintNodeList3;
       Procedure PrintNodeList2;
       Procedure PrintNodeList1;
       Procedure PrintNodeList;
       Procedure PrintFinalList;
       Procedure CheckSorted;
       Procedure ProjectSize(var ClipRect:TClipRect);
       Procedure AddFrame(ClipRect:TClipREct);
       Procedure AddFrameEdge(Start,Ende:TVertex);
       Procedure InsertLineToErrorLayer(Edge:TEdge);
       Procedure InsertLineToIntersectionLayer(Edge:TEdge);
       Procedure DeleteLayer(Index:longint);
       {$IFDEF DEBUG}
{       Procedure AppendList(MList:TSLBase);
       Procedure SaveEdgeList;
       Procedure PrintSnapList(SList:TRecList);
       Procedure WriteText(s:String);
       Procedure PrintFList;
       Procedure PrintNode(p:TSLBaseItem);}
       {$ENDIF}
       Destructor Destroy; override;                {modified}
       Property EdgeList : TSLBase read FEdgeList write FEdgeList;
       Property NodeList : TSLBase read FNodeList write FNodeList;
       Function  InsertToNodeList(Start:TSLBaseItem;Vertex:TDVertex;var SItem1,SItem2:TSegItem;flag:integer;q:TSLBaseItem):Boolean;
       function InsertToProject(Vertex: TDVertex): Boolean;
     end;

     TSListItem = class(TSLBaseItem)
      public
       x : Double;
       y : Double;
       Function Getx:Double; override;
       Function Gety:Double; override;
       Procedure Print; override;
       {$IFDEF DEBUG}
       {Procedure Print; override;}
       {$ENDIF}
     end;

     TEdge         = class
      private
      public
       Link      : TEdge;
       x         : Double;
       y         : Double;
       next      : TEdge;
       SegItem   : TSegItem;
       back      : TLink;
       Index     : word;
       v         : Integer16;
       Destructor Destroy;  override;
     end;

     TEdgeListItem = class(TSLBaseItem)
      public
       PtEdge     : TEdge;
       Constructor Create(p:TObject);
       Constructor Init;   override;
       Function KeyOfX(Item:Pointer):Pointer; override;
       Function KeyOfY(Item:Pointer):Pointer; override;
       Function OnCompareItems(Key1,Key2:Pointer):Integer; override;
       Procedure Print; override;
       Destructor Destroy; override;
     end;

     TLink       = class(TSLBaseItem)
      private
       Function GetCount:Word;
      public
       X         : Double;
       Y         : Double;
       List      : TRecList;
       FrontList : TRecList;
       BackList  : TRecList;
       Constructor Create;
       Constructor Create2;
       Procedure InsertEdge(Edge:TEdge);
       Procedure InsertEdgeSec(Edge:TEdge;ListType:Integer);
       Procedure InsertList(SList:TRecList);
       Procedure InsertList2(SCol:TSLBaseItem;SCount:Integer);
       Procedure VerifyInsertEdge(Edge:TEdge);
       Function  OnCompareEdges(Item1,Item2:TEdge):Integer;
       Procedure InsertEdgeSorted(AArea:TArea;var SItem:TSegItem);
       Procedure InsertEdgeSorted2(AArea:TArea;Vertex:TDVertex;Edge:TEdge;var Above:TSegItem);
       Function  InsertFrontEdge(AArea:TArea;AEdge,RedoEdge,RedoEdgeNext:TEdge):Boolean;
       Function  InsertBackEdge(AArea:TArea;AEdge,RedoEdge,RedoEdgeNext:TEdge):Boolean;
       Procedure CalculateMean;
       Procedure CalculateMean2;
       Property  MCount:Word read GetCount;
       Function  SearchList(Edge:TEdge; Index:Integer): TRecList;
       Procedure RestoreBackList;
       Procedure RestoreFrontList;
       Function  DoesEdgeExist(AEdge:TEdge;AMList:TRecList;Redoedge,Redoedgenext:TEdge):Boolean;
       Procedure InsertToFinalList(SCol:TSLBaseItem;Scount:Integer);
       {$IFDEF DEBUG}
{       Procedure Print; override;}
       {$ENDIF}
       Destructor Destroy; override;
     end;

     TTree     = class(TBaseTree)
      private
       List  : TRecList;
      protected
       Procedure   TraverseTree(p:TTreeBaseItem); override;
      public
       Owner : TArea;
       constructor Create(AOwner:TArea);
       Function OnCompareItems(key1,key2:Pointer):Integer; override;
       Function Key1Of(Item:Pointer):Pointer; override;
       Function Key2Of(Item:Pointer):Pointer; override;
       Function ResultZero(p,k:TTreeBaseItem):Boolean; override;
     end;

     TTreeItem = class(TTreeBaseItem)
      private
      public
       PtEdge : TEdge;
       constructor Create(key:TEdge);
     end;


implementation
uses Contree,Am_Ar,Am_Proj,AM_Poly,AM_CPoly,AM_Point,AM_Index,AM_Layer,UserIntf,AM_ProjO;

Procedure KeepWinAlive(Var Abort:Boolean);
var M:TMsg;
begin
Abort:=False;
if PeekMessage(M,0,0,0, PM_REMOVE) then
   begin
   if M.message = wm_lbuttondown then begin
     Abort:=TRUE;
   end;
     TranslateMessage(M);
     DispatchMessage(M);
   end;
end;

Procedure LoopWait;
  var a,b,c : double;
      i,j   : longint;

  begin
    a:=1.0;
    b:=2.0;
    for i:=1 to 1 do begin
      for j:=1 to 2000 do c:=a*b;
    end;
  end;

Procedure TArea.CheckSorted;
var p     : TSLBaseItem;
    check : Boolean;
  begin
    check:=False;
    p:=NodeList.Head;
    while p.next<>NIL do begin
      if (Tlink(p).x > TLink(p.next).x) then check:=TRue else
      if (TLink(p).x = TLink(p.next).x) then begin
        while (p.next<>NIl)and(TLink(p).x=TLink(p.next).x) do begin
          if (TLink(p).y>TLInk(p.next).y) then check:=True;
          p:=p.next;
        end;
      end;
      if check then raise ElistError.Create(' not Sorted ' );
      p:=p.next;
    end;
  end;

Procedure TArea.ReadFile
   (
   FileName:TFileName
   );
  var f      : TextFile;
      x      : Double;
      y      : Double;
      AItem  : TSListItem;
      p      : TObject;
  begin
    {$IFDEF DEBUG}
    {AssignFile(FName,'c:\wingis\dat\out.dat');}
    {$ENDIF}
    AssignFile(f,FileName);
    {rewrite(FName);}
    reset(f);
    while not eof(f) do begin
      readln(f,x,y);
      if (x=0)and(y=0) then begin
        if AList<>NIL then begin
          {AppendList(Alist);}
          ConvertPolyToEdge;
          AList.Free;
        end;
        Alist:=TSLBase.Create;
      end
      else begin
        AItem:=TSListItem.Create;
        AItem.x:=x;
        AItem.y:=y;
        AList.Add(AItem);
      end;
    end;
    {AppendList(AList);}
    ConvertPolyToEdge;
    AList.Free;
    CloseFile(f);
    {$IFDEF DEBUG}
    {CloseFile(FName);}
    {$ENDIF}
  end;

 {$IFDEF DEBUG}
{ Procedure TArea.SaveEdgeList;
   var f:Textfile;
       p:TSLBaseItem;
   begin
     AssignFile(f,'c:\wingis\dat\oute.dat');
     rewrite(f);
     p:=EdgeList.GetHead;
     while p<>NIL do begin
       writeln(f,TEdge(TEdgeListItem(p).PtEdge).x,' ',TEdge(TEdgeListItem(p).PtEdge).y);
       p:=EdgeList.Getnext(p);
     end;
     CloseFile(f);
   end;

 Procedure TArea.AppendList
    (
    MList :TSLBase
    );
   var p : TSLBaseItem;
   begin
     Append(FName);
     p:=MList.GetHead;
     while p<>NIL do begin
       writeln(FName,TSListItem(p).x);
       p:=MList.GetNext(p);
     end;
     writeln(FName);
   end;}

 {$ENDIF}

Constructor TArea.Create
   (
   AProj  : Pointer
   );
  begin
    inherited Create;
    FPointList:=TList.Create;
    FDrawList :=TList.Create;
    EDgeList:=TSLBase.Create;
    FNodeList:=TSLBase.Create;
    FinalList:=TSLBase.Create;
    FResultList:=TList.Create;
    FResultCol:=TList.Create;
    Proj:=AProj;
    STree:=TSegTree.Create(1,Self);
    {STree.Radius:=Data.Radius;  in AM_Area}
    AllAbort:=FALSE;
    ManualMode:=False;
    ManualPos.Init(0,0);
  end;

Procedure TArea.ConvertPolyToEdge;
   var p            : TSLBaseItem;
       EdgeListItem : TEdgeListItem;
   begin
     {$IFDEF DEBUG}
     if AList.count<=1 then Raise EListError.Create('Nur ein Punkt!');
     {$ENDIF}
     p:=AList.GetHead;
     while p.next<>NIL do begin
       AddEdge(p,p.next);
       p:=AList.GetNext(p);
     end;
  end;

Destructor TArea.Destroy;
var   p      : TSLBaseItem;
      h      : TSLBaseItem;
      count  : Integer;
      Index  : Integer;
      SList  : TRecList;
      ERun   : TEdge;
  begin
    FResultList.Free;
    FResultCol.Free;
    if not AllAbort then STree.Free;   {**********************}

    EdgeList.Free;

    p:=FinalList.GetHead;   {finallist!!!}
    while p<>NIL do begin
      SList:=TLink(p).FrontList;
      if SList.count>0 then begin
        count:=0;
        index:=0;
        while count<SList.Count do begin
          if (SList[index]<>NIL) then begin
            ERun:=TEdge(SList[Index]^);
            Erun.Free;
            inc(count);
          end;
          inc(index);
        end;
      end;
      SList:=TLink(p).BAckList;
      if SList.count>0 then begin
        count:=0;
        index:=0;
        while count<SList.Count do begin
          if (SList[index]<>NIL) then begin
            ERun:=TEdge(SList[Index]^);
            Erun.Free;
            inc(count);
          end;
          inc(index);
        end;
      end;
      p:=p.next;
    end;
    FNodeList.Free;
    FinalList.Free;
    inherited Destroy;
  end;

Procedure TSListItem.Print;
  var f:TextFile;
  begin
  end;


Function TSListItem.Getx
   :Double;
  begin
    Result:=x;
  end;

Function TSListItem.Gety
   :Double;
  begin
    Result:=y;
  end;


Destructor TEdgeListItem.Destroy;
  begin
    {p.free;}
    inherited Destroy;
  end;

Procedure TArea.AddEdge
   (
   p  : TSLBaseItem;
   q  : TSLBaseItem
   );
  var Edge          : TEdge;
      hEdge         : TEdge;
      EdgeListItem  : TEdgeListItem;
  begin
    EdgeListItem:=TEdgeListItem.Create(p);
    Edge:=TEdge(EdgeListItem.PtEdge);
    System.Move(TSListItem(p).x,Edge.x,16);
    EdgeList.Add(EdgeListItem);

    EdgeListItem:=TEdgeListItem.Create(q);
    hEdge:=TEdge(EdgeListItem.PtEdge);
    System.Move(TSListItem(q).x,hEdge.x,16);
    EdgeList.Add(EdgeListItem);

    Edge.next:=hEdge;
    hEdge.next:=Edge;
  end;

Procedure TArea.Snap2;
  var SCol     : TSLBaseItem;
      S        : TSLBaseItem;
      SRun     : TSLBaseItem;
      pprev    : TSLBaseItem;
      p        : TSLBaseItem;
      prun     : TSlBaseItem;
      XMin     : Double;
      XMax     : Double;
      YMin     : Double;
      YMax     : Double;
      Link     : TLink;
      SCount   : Integer;
      whole    : longint;
      actualn  : double;
      actual   : double;
      cnt      : longint;
      Edge     : TSLBaseItem;
      i        : longint;
  begin
    actual:=0;
    cnt:=0;
    {$IFNDEF AXDLL}  // <----------- AXDLL
    StatusBar.Progress:=0;
    {$ENDIF} // <----------- AXDLL
    while EdgeList.Head<>NIL do begin
      actualn:=((100/EdgeList.Count)*cnt);
      if (actualn-actual)> 1 then begin
        if AllAbort                                         {sl}
          {$IFNDEF AXDLL}  // <----------- AXDLL
            or StatusBar.QueryAbort
          {$ENDIF} // <----------- AXDLL
          then begin
          AllAbort:=TRUE;
          Break;
        end;
        {$IFNDEF AXDLL}  // <----------- AXDLL
        StatusBar.Progress:=round(actualn);
        {$ENDIF} // <----------- AXDLL
        actual:=actualn;
      end;

      SCol:=EdgeList.GetHead;
      EdgeList.Head:=EdgeList.Head.next;      {prev Pointer!!!}
      SCol.next:=NIL;
      S:=SCol;
      SRun:=S;
      SCount:=1;
      while(S<>NIL)and(EdgeList.Head<>NIL)do begin
        inc(cnt);
        p:=EdgeList.Head;
        pprev:=NIL;
        XMin := TEdge(TEdgeListItem(S).PtEdge).x - DeltaX;
        XMax := TEdge(TEdgeListItem(S).PtEdge).x + DeltaX;
        YMin := TEdge(TEdgeListItem(S).PtEdge).y - DeltaY;
        YMax := TEdge(TEdgeListItem(S).PtEdge).y + DeltaY;
        while (p <> NIL) and ( TEdge(TEdgeListItem(p).PtEdge).x <= xmax) do begin
          if (TEdge(TEdgeListItem(p).PtEdge).x < XMax)and
             (TEdge(TEdgeListItem(p).PtEdge).x > XMin)and
             (TEdge(TEdgeListItem(p).PtEdge).y < YMax)and
             (TEdge(TEdgeListItem(p).PtEdge).y > YMin) then begin
            {*************************************************}
            {p in Scol an der Stelle srun.next einfuegen und}
            {p aus EdgeList entfernen}
            Srun.next:=p;
            SRun:=SRun.next;
            inc(SCount);
            if pprev = NIL then begin
              EdgeList.Head:=p.next;
              SRun.next:=NIL;
              p:=EdgeList.Head;
              pprev:=NIL;
            end
            else begin
              pprev.next:=p.next;
              p:=p.next;
              SRun.next:=NIL;
            end;
          end
          else begin
            pprev:=p;
            p:=p.next;
          end;
        end;
        S:=S.next;
      end;
      {SCol zusammenfuegen zu Link}

      Link:=TLink.Create;
      Link.InsertList2(SCol,SCount);
      Link.CalculateMean;

      {if Nodelist.Head = NIL then NodeList.Add(Link) else begin
        if (Link.x-TLink(Nodelist.Tail).x)>eps then NodeList.Add(Link) else begin
          p:=NodeList.Tail;
          while (p<>NIL)and((Link.x-TLink(p).x)< -eps) do p:=p.prev;
          if p<>NIL then begin
            if ((Link.x-TLink(p).x)> eps)then NodeList.InsertNext(p,Link) else
            EListError.Create(' Equal Snap ');
          end else NodeList.Add(Link);
        end;
      end; }
      if Nodelist.Count = 0 then NodeList.Add(Link) else begin
        if Link.x > TLink(NodeList.Tail).x then NodeList.Add(Link) else begin
          prun:=NodeList.Tail;
          while (prun<>NIL) and (Link.x < TLink(prun).x) do prun:=prun.prev;
          if prun=NIL then NodeList.AddHead(Link) else
          if Link.x > TLink(prun).x then NodeList.InsertNext(prun,Link) else
          if Link.x = TLink(prun).x then begin
            while (prun<>NIL) and (Link.x = TLink(prun).x)and (Link.y < TLink(prun).y) do prun:=prun.prev;
            if prun=NIL then NodeList.AddHead(Link) else
            if (TLink(prun).x = Link.x )and (Link.y> TLink(prun).y) then NodeList.InsertNext(prun,Link) else
            if (TLink(prun).x <> Link.x) then NodeList.InsertNext(prun,Link);{ else
            if Link.y = TLink(prun).y then raise EListError.Create(' Equal Snap ');}
          end;
          {raise EListError.Create(' Equal Snap ');}
        end;
      end;
      {}
    end;
  end;

Procedure TArea.Snap3;
  var SCol     : TSLBaseItem;
      S        : TSLBaseItem;
      SRun     : TSLBaseItem;
      pprev    : TSLBaseItem;
      p        : TSLBaseItem;
      prun     : TSlBaseItem;
      XMin     : Double;
      XMax     : Double;
      YMin     : Double;
      YMax     : Double;
      Link     : TLink;
      SCount   : Integer;
      actualn  : double;
      actual   : double;
      cnt      : longint;

  begin
    {$IFNDEF AXDLL}  // <----------- AXDLL
    StatusBar.Progress:=0;
    {$ENDIF}  // <----------- AXDLL
    p:=NodeList.GetHead;

    cnt:=0;
    while NodeList.Head<>NIL do begin
      actualn:=((100/NodeList.Count)*cnt);
      if (actualn-actual)> 1 then begin
        if AllAbort                                         {sl}
            {$IFNDEF AXDLL}  // <----------- AXDLL
            or StatusBar.QueryAbort
            {$ENDIF}  // <----------- AXDLL
          then begin
          AllAbort:=TRUE;
          Break;
        end;
        {$IFNDEF AXDLL}  // <----------- AXDLL
        StatusBar.Progress:=round(actualn);
        {$ENDIF}  // <----------- AXDLL
        actual:=actualn;
      end;

      SCol:=NodeList.GetHead;
      NodeList.Head:=NodeList.Head.next;      {prev Pointer!!!}
      SCol.next:=NIL;
      S:=SCol;
      SRun:=S;
      SCount:=1;
      while(S<>NIL)and(NodeList.Head<>NIL)do begin
        inc(cnt);
        p:=NodeList.Head;
        pprev:=NIL;
        XMin := Tlink(s).x - DeltaX;
        XMax := TLink(s).x + DeltaX;
        YMin := TLink(s).y - DeltaY;
        YMax := TLink(s).y + DeltaY;
        while (p <> NIL) and ( TLink(p).x <= xmax) do begin
          if (TLink(p).x < XMax)and
             (TLink(p).x > XMin)and
             (TLink(p).y < YMax)and
             (TLink(p).y > YMin) then begin

             {p in Scol an der Stelle srun.next einfuegen und}
             {p aus EdgeList entfernen}
            Srun.next:=p;
            SRun:=SRun.next;
            inc(SCount);
            if pprev = NIL then begin
              NodeList.Head:=p.next;
              SRun.next:=NIL;
              p:=NodeList.Head;
              pprev:=NIL;
            end
            else begin
              pprev.next:=p.next;
              p:=p.next;
              SRun.next:=NIL;
            end;
          end
          else begin
            pprev:=p;
            p:=p.next;
          end;
        end;
        S:=S.next;
      end;
      {SCol zusammenfuegen zu Link}
      {try
      if abs(xmax-7681998.5)<2 then raise ElistError.Create('Stop');
      except end;
      try        }
      Link:=TLink.Create2;
      Link.InsertToFinalList(SCol,Scount);
     { except end;}
      Link.CalculateMean2;


      if Finallist.Count = 0 then FinalList.Add(Link) else begin
        if Link.x > TLink(FinalList.Tail).x then FinalList.Add(Link) else begin
          prun:=FinalList.Tail;
          while (prun<>NIL) and (Link.x < TLink(prun).x) do prun:=prun.prev;
          if prun=NIL then FinalList.AddHead(Link) else
          if Link.x > TLink(prun).x then FinalList.InsertNext(prun,Link) else
          if Link.x = TLink(prun).x then begin
            while (prun<>NIL) and (Link.x = TLink(prun).x)and (Link.y < TLink(prun).y) do prun:=prun.prev;
            if prun=NIL then FinalList.AddHead(Link) else
            if (TLink(prun).x = Link.x )and (Link.y> TLink(prun).y) then FinalList.InsertNext(prun,Link) else
            if (TLink(prun).x <> Link.x) then FinalList.InsertNext(prun,Link);{ else
            if Link.y = TLink(prun).y then raise EListError.Create(' Equal Snap ');  }
          end;
        end;
      end;
    end;
  end;



Procedure TArea.Snap;
  var SCol   : TRecList;
      SIndex : Integer;
      SCount : Integer;
      XMin   : Double;
      XMax   : Double;
      YMin   : Double;
      YMax   : Double;
      Link   : TLink;
      Edge   : TEdge;
      P      : TSLBaseItem;
  begin
    while EdgeList.count > 0 do begin
    SCol := TRecList.Create(isScolList,sizeof(TEdge));
      SCount:=0;
      SIndex:=0;
      SCol.Add(TEdge(TEdgeListItem(EdgeList.GetHead).PtEdge));
      EdgeList.DeleteHead;
      while (SCount<=SCol.Count-1)and(EdgeList.Count>0) do begin
        if SCol[SIndex]<>NIL then begin
          Edge:=TEdge(SCol[Sindex]^);
          P:=EdgeList.GetHead;
          XMin := TEdge(Edge).x - DeltaX;
          XMax := TEdge(Edge).x + DeltaX;
          YMin := TEdge(Edge).y - DeltaY;
          YMax := TEdge(Edge).y + DeltaY;
          while (p<>NIL) and (TEdge(TEdgeListItem(p).PtEdge).x <XMax) do begin
            if (TEdge(TEdgeListItem(p).PtEdge).x < XMax)and
               (TEdge(TEdgeListItem(p).PtEdge).x > XMin)and
               (TEdge(TEdgeListItem(p).PtEdge).y < YMax)and
               (TEdge(TEdgeListItem(p).PtEdge).y > YMin) then begin
              Scol.Add(TEdge(TEdgeLIstItem(p).PtEdge));
              EdgeList.Delete(p);
            end
            else p:=p.next;
          end;
          inc(SCount);
        end;
        inc(SIndex);
      end; {while SCount}
     { PrintSnapList(SCol);}
      Link:=TLink.Create;
      Link.InsertList(SCol);
      Link.CalculateMean;
      NodeList.Add(Link);
      SCol.Free;
    end;
  end;

Function TArea.SetListCount
   (
   List   : TSLBase
   )
   : longint;
  var p         : TSLBaseItem;
      i         : longint;
  begin
    i:=0;
    p:=List.GetHead;
    while p<>NIL do begin
      p:=p.next;
      inc(i);
    end;
    Result:=i;
  end;

Constructor TEdgeListItem.Create
   (
   p : TObject
   );
  var Edge   : TEdge;
  begin
    Edge:=TEdge.Create;
    Edge.x:=3.4E37;
    Edge.y:=3.4E37;
    PtEdge:=Edge;
  end;

Constructor TEdgeListItem.Init;
  var Edge   : TEdge;
  begin
    Edge:=TEdge.Create;
    Edge.x:=3.4E37;
    Edge.y:=3.4E37;
    PtEdge:=Edge;
  end;

Function TEdgeListItem.KeyOfX
   (
   Item:     Pointer
   )
   : Pointer;
  begin
    Result:=@TEdgeListItem(Item).PtEdge.x;
  end;

Function TEdgeListItem.KeyOfY
   (
   Item:     Pointer
   )
   : Pointer;
  begin
    Result:=@TEdgeListItem(Item).PtEdge.y;
  end;

Function TEdgeListItem.OnCompareItems
   (
   Key1   : Pointer;
   Key2   : Pointer
   )
   : Integer;
  begin
     if PDouble(Key1)^ < PDouble(Key2)^ then Result:= -1 else
     if PDouble(Key1)^ > PDouble(Key2)^ then Result:=  1 else
     if (abs(PDouble(Key1)^ - PDouble(Key2)^) < Eps )then Result:=0;
  end;

Destructor TEdge.Destroy;
  begin
    inherited Destroy;
  end;

Procedure TEdgeListItem.Print;
  var f:TextFile;
  begin
  end;

{$IFDEF DEBUG}
{Procedure TEdgeListItem.Print;
  var f:TextFile;
  begin
    AssignCrt(f);
    rewrite(f);
    Writeln(PtEdge.x:5:2,' ',PtEdge.y:5:2);
    Closefile(f);
  end;
 }

{Procedure TArea.PrintSnapList
   (
   SList:TRecList
   );
  var S     : TSLBaseItem;
      index : Integer;
      count : Integer;
      f     : TextFile;
      Edge  : TEdge;
  begin
    index:=0;
    count:=0;
    AssignCrt(f);
    rewrite(f);
    writeln('SnapList: ');
    while count<SList.count do begin
      if SList[index]<>NIL then begin
        Edge:=TEdge(SList[Index]^);
        Write(Edge.x:5:2,' ',Edge.y:5:2,'  ');
        inc(count);
      end;
      inc(index);
    end;
  writeln(f);
  CloseFile(f);
  end;

Procedure TArea.WriteText
   (
   s:String
   );
  var f:TextFile;
  begin
    AssignCrt(f);
    rewrite(f);
    writeln(s);
    CloseFile(f);
  end;}
{$ENDIF}

Procedure TArea.KillEdge
   (
   Edge: TEdge
   );
  var Link    : TLink;
      hEdge   : TEdge;
  begin
    hEdge:=Edge.next;
    Link:=hEdge.back;
    Link.List.Delete(hEdge.Index);
    hEdge.Free;
    Edge.Free;
  end;

Procedure TArea.KillEdge2
   (
   Edge: TEdge
   );
  var Link    : TLink;
      hEdge   : TEdge;
      List    : TRecList;
  begin
    hEdge:=Edge.next;
    Link:=hEdge.back;
    List:=Link.SearchList(hEdge,hEdge.Index);
    List.Delete(hEdge.Index);
    hEdge.Free;
    Edge.Free;
  end;

Procedure TArea.KillWholeEdge
   (
   Edge: TEdge
   );
  var Link1   : TLink;
      Link2   : TLink;
      List1   : TRecList;
      List2   : TRecLIst;
  begin
    Link1:=Edge.back;
    SetNil(Edge);
    List1:=Link1.SearchList(Edge,Edge.Index);
    List1.Delete(Edge.Index);
    Link2:=Edge.next.back;
    List2:=Link2.SearchList(Edge.next,Edge.next.Index);
    List2.Delete(Edge.next.Index);
    Edge.next.free;
    Edge.free;
  end;

Procedure TArea.RemoveEmptyNode;
  var p      : TSLBaseItem;
      help   : TSLBaseItem;
      count  : Integer;
      Index  : Integer;
      SList  : TRecList;
      ERun   : TEdge;
  begin
    p:=NodeList.GetHead;
    while p<>NIL do begin
      SList:=TLink(p).List;
      if SList.count=0 then begin
        help:=p;
        p:=p.next;
        help.Free;
      end else
      p:=p.next;
    end;
  end;

Procedure TArea.RemoveEmptyNode2;
  var p      : TSLBaseItem;
      help   : TSLBaseItem;
      count  : Integer;
      Index  : Integer;
      SList  : TRecList;
      ERun   : TEdge;
  begin
    p:=NodeList.GetHead;
    while p<>NIL do begin
      if (TLink(p).FrontList.count=0)and(TLink(p).BackList.count=0)then begin
        help:=p;
        p:=p.next;
        help.Free;
      end else
      p:=p.next;
    end;
  end;



Procedure TArea.EleminateEqual;
  var p        : TSLBaseItem;
      count    : Integer;
      Index    : Integer;
      SList    : TRecList;
      ERun     : TEdge;
      cnt      : integer;

      cntx     : integer;
      actualn  : double;
      actual   : double;
  begin
    {$IFNDEF AXDLL}  // <----------- AXDLL
    StatusBar.Progress:=0;
    {$ENDIF}  // <----------- AXDLL
    cntx:=0;
    p:=NodeList.GetHead;
    while p<>NIL do begin

      actualn:=((100/NodeList.Count)*cntx);
      if (actualn-actual)> 1 then begin
        if AllAbort                                         {sl}
            {$IFNDEF AXDLL}  // <----------- AXDLL
            or StatusBar.QueryAbort
            {$ENDIF}  // <----------- AXDLL
          then begin
          AllAbort:=TRUE;
          Break;
        end;
        {$IFNDEF AXDLL}  // <----------- AXDLL
        StatusBar.Progress:=round(actualn);
        {$ENDIF}  // <----------- AXDLL
        actual:=actualn;
      end;
      inc(cntx);

      SList:=TLink(p).List;
      if SList.count>0 then begin
        count:=0;
        index:=0;
        cnt:=SList.Count;
        while Index<Cnt do begin
          if (SList[index]<>NIL) then begin
            ERun:=TEdge(SList[Index]^);
            if (abs(ERun.x - ERun.next.x)<Eps)and
               (abs(ERun.y - ERun.next.y)<Eps) then begin
                 KillEdge(ERun);
                 SList.Delete(Index);
               end;
            inc(count);
          end;
          inc(index);
        end;
      end;
      p:=p.next;
    end;
  end;


Procedure TArea.EleminateEqual2;
  var p      : TSLBaseItem;
      count  : Integer;
      Index  : Integer;
      SList  : TRecList;
      ERun   : TEdge;
      cnt    : integer;
      cntx     : integer;
      actualn  : double;
      actual   : double;
  begin
    {$IFNDEF AXDLL}  // <----------- AXDLL
    StatusBar.Progress:=0;
    {$ENDIF}  // <----------- AXDLL
    cntx:=0;
    p:=FinalList.GetHead;
    while p<>NIL do begin

      actualn:=((100/FinalList.Count)*cntx);
      if (actualn-actual)> 1 then begin
        if AllAbort                                         {sl}
            {$IFNDEF AXDLL}  // <----------- AXDLL
            or StatusBar.QueryAbort
            {$ENDIF}  // <----------- AXDLL
          then begin
          AllAbort:=TRUE;
          Break;
        end;
        {$IFNDEF AXDLL}  // <----------- AXDLL
        StatusBar.Progress:=round(actualn);
        {$ENDIF}  // <----------- AXDLL
        actual:=actualn;
      end;
      inc(cntx);

      SList:=TLink(p).FrontList;
      if SList.count>0 then begin
        count:=0;
        index:=0;
        cnt:=SList.Count;
        while Index<Cnt do begin
          if (SList[index]<>NIL) then begin
            ERun:=TEdge(SList[Index]^);
            if (abs(ERun.x - ERun.next.x)<Eps)and
               (abs(ERun.y - ERun.next.y)<Eps) then begin
                 KillEdge2(ERun);
                 SList.Delete(Index);
               end;
            inc(count);
          end;
          inc(index);
        end;
      end;
      p:=p.next;
    end;
  end;


Procedure TArea.EleminateDouble;
  var p      : TSLBaseItem;
      count  : Integer;
      Count2 : Integer;
      Index  : Integer;
      Index2 : Integer;
      SList  : TRecList;
      ERun   : TEdge;
      ECheck : TEdge;
      cnt1   : integer;
      cnt2   : integer;
      cntx     : integer;
      actualn  : double;
      actual   : double;
  begin
    {$IFNDEF AXDLL}  // <----------- AXDLL
    StatusBar.Progress:=0;
    {$ENDIF}  // <----------- AXDLL
    cntx:=0;
    p:=NodeList.GetHead;
    while p<>NIL do begin

      actualn:=((100/NodeList.Count)*cntx);
      if (actualn-actual)> 1 then begin
        if AllAbort                                         {sl}
            {$IFNDEF AXDLL}  // <----------- AXDLL
            or StatusBar.QueryAbort
            {$ENDIF}  // <----------- AXDLL
          then begin
          AllAbort:=TRUE;
          Break;
        end;
        {$IFNDEF AXDLL}  // <----------- AXDLL
        StatusBar.Progress:=round(actualn);
        {$ENDIF}  // <----------- AXDLL
        actual:=actualn;
      end;
      inc(cntx);

      SList:=TLink(p).List;
      if SList.count>1 then begin
        count:=0;
        index:=0;
        cnt1:=SList.Capacity-1;
        cnt2:=SList.Capacity;
        {while count<SList.Count-1 do begin}
        while Index<cnt1 do begin
          if (SList[index]<>NIL) then begin
            ERun:=TEdge(SList[Index]^);

            count2:=0;
            index2:=0;
           { while count2<SList.Count do begin}
            while Index2<cnt2 do begin
              if (Slist[index2]<>NIL) then begin
                if Index<>Index2 then begin
                  ECheck:=TEdge(SList[Index2]^);
                  if (abs(ERun.x - ECheck.x)<Eps)and
                     (abs(ERun.y - ECheck.y)<Eps)and
                     (abs(ERun.next.x - ECheck.next.x)<Eps)and
                     (abs(ERun.next.y - ECheck.next.y)<Eps)then begin
                    KillEdge(ECheck);
                    SList.Delete(Index2);
                  end;
                end;
                inc(Count2);
              end;
              inc(Index2);
            end;

            inc(count);
          end;
          inc(index);
        end;
      end;
      p:=p.next;
    end;
  end;

Procedure TArea.EleminateDouble2;
  var p      : TSLBaseItem;
      count  : Integer;
      Count2 : Integer;
      Index  : Integer;
      Index2 : Integer;
      SList  : TRecList;
      ERun   : TEdge;
      ECheck : TEdge;
      cnt1   : integer;
      cnt2   : integer;
      cntx     : integer;
      actualn  : double;
      actual   : double;
  begin
    {$IFNDEF AXDLL}  // <----------- AXDLL
    StatusBar.Progress:=0;
    {$ENDIF}  // <----------- AXDLL
    cntx:=0;
    p:=FinalList.GetHead;
    while p<>NIL do begin

      actualn:=((100/FinalList.Count)*cntx);
      if (actualn-actual)> 1 then begin
        if AllAbort                                         {sl}
            {$IFNDEF AXDLL}  // <----------- AXDLL
            or StatusBar.QueryAbort
            {$ENDIF}  // <----------- AXDLL
          then begin
          AllAbort:=TRUE;
          Break;
        end;
        {$IFNDEF AXDLL}  // <----------- AXDLL
        StatusBar.Progress:=round(actualn);
        {$ENDIF}  // <----------- AXDLL
        actual:=actualn;
      end;
      inc(cntx);

      SList:=TLink(p).FrontList;
      if SList.count>1 then begin
        count:=0;
        index:=0;
        cnt1:=SList.Capacity-1;
        cnt2:=SList.Capacity;
        {while count<SList.Count-1 do begin}
        while Index<cnt1 do begin
          if (SList[index]<>NIL) then begin
            ERun:=TEdge(SList[Index]^);

            count2:=0;
            index2:=0;
           { while count2<SList.Count do begin}
            while Index2<cnt2 do begin
              if (Slist[index2]<>NIL) then begin
                if Index<>Index2 then begin
                  ECheck:=TEdge(SList[Index2]^);
                  if (abs(ERun.x - ECheck.x)<Eps)and
                     (abs(ERun.y - ECheck.y)<Eps)and
                     (abs(ERun.next.x - ECheck.next.x)<Eps)and
                     (abs(ERun.next.y - ECheck.next.y)<Eps)then begin
                    KillEdge2(ECheck);
                    SList.Delete(Index2);
                  end;
                end;
                inc(Count2);
              end;
              inc(Index2);
            end;

            inc(count);
          end;
          inc(index);
        end;
      end;
      p:=p.next;
    end;
  end;

Procedure TArea.SortLinkList;
  var p           : TSLBaseItem;
      SList       : TRecList;
      count       : Integer;
      Index       : Integer;
      Edge        : TEdge;
      cnt         : Integer;
      actual      : double;
      actualn     : double;
      cntx        : Integer;
  begin
    {$IFNDEF AXDLL}  // <----------- AXDLL
    StatusBar.Progress:=0;
    {$ENDIF}  // <----------- AXDLL
    cnt:=0;
    cntx:=0;
    p:=NodeList.GetHead;
    while p<>NIL do begin
      actualn:=((100/NodeList.Count)*cntx);
      if (actualn-actual)> 1 then begin
        if AllAbort                                         {sl}
            {$IFNDEF AXDLL}  // <----------- AXDLL
            or StatusBar.QueryAbort
            {$ENDIF}  // <----------- AXDLL
          then begin
          AllAbort:=TRUE;
          Break;
        end;
        {$IFNDEF AXDLL}  // <----------- AXDLL
        StatusBar.Progress:=round(actualn);
        {$ENDIF}  // <----------- AXDLL
        actual:=actualn;
      end;
      inc(cntx);

      count:=0;
      index:=0;
      SList:=TLink(p).List;

      while count<SList.count do begin
        if SList[Index]<>NIL then begin
          Edge:=TEdge(SList[Index]^);
          TLink(p).VerifyInsertEdge(Edge);
          inc(Count);
        end;
        inc(Index);
      end;
      SList.Free;
      TLink(p).List:=NIL;
      p:=p.next;
    end;
  end;

Procedure TArea.SortList
   (
   List  : TSLBase
   );
  var p           : TSLBaseItem;
      SList       : TRecList;
      count       : Integer;
      Index       : Integer;
      Edge        : TEdge;
      cntx        : Integer;
      actual      : double;
      actualn     : double;
  begin
    {$IFNDEF AXDLL}  // <----------- AXDLL
    StatusBar.Progress:=0;
    {$ENDIF}  // <----------- AXDLL
    cntx:=0;
    p:=List.GetHead;
    while p<>NIL do begin

      actualn:=((100/List.Count)*cntx);
      if (actualn-actual)> 1 then begin
        if AllAbort                                         {sl}
            {$IFNDEF AXDLL}  // <----------- AXDLL
            or StatusBar.QueryAbort
            {$ENDIF}  // <----------- AXDLL
          then begin
          AllAbort:=TRUE;
          Break;
        end;
        {$IFNDEF AXDLL}  // <----------- AXDLL
        StatusBar.Progress:=round(actualn);
        {$ENDIF}  // <----------- AXDLL
        actual:=actualn;
      end;
      inc(cntx);

      if TLink(p).BackList.count > 1 then begin
        TraverseList(TLink(p).BackList);
      end;
      if TLink(p).FrontList.count >1 then begin
        TraverseList(TLink(p).FrontList);
      end;

      p:=p.next;
    end;
  end;

Function TArea.TraverseList
   (
   var List: TRecList
   )
   :Boolean;
  var count    : Integer;
      Index    : Integer;
      SList    : TRecLIst;
      Edge     : TEdge;
      ATree    : TTree;
      AItem    : TTreeItem;
      found    : Boolean;
  begin
    Result:=TRUE;
    found:=false;
    count:=0;
    index:=0;
    ATree:=TTree.Create(Self);

    while count<List.count do begin
      if List[Index]<>NIL then begin
        Edge:=TEdge(List[Index]^);
        AItem:=TTreeItem.Create(Edge);
        found:=ATree.InsertInTree(AItem);
        if not found then Result:=False;
        inc(Count);
      end;
      inc(Index);
    end;
    ATree.Traverse;
    List.Free;
    List:=ATree.List;
    ATree.Free
  end;


Procedure TArea.MergeLink;
  var p           : TSLBaseItem;
      List        : TRecList;
      count       : Integer;
      Index       : Integer;
      Edge        : TEdge;
      NextNode    : TLink;
      NextIndex   : Integer;
      LinkIndex   : Integer;
      actualn     : double;
      actual      : double;
      cntx        : longint;
  begin
    {$IFNDEF AXDLL}  // <----------- AXDLL
    StatusBar.Progress:=0;
    {$ENDIF}  // <----------- AXDLL
    cntx:=0;
    p:=FinalList.GetHead;
    while p<>NIL do begin
    {Merge FrontList}

      actualn:=((100/FinalList.Count)*cntx);
      if (actualn-actual)> 1 then begin
        if AllAbort                                         {sl}
            {$IFNDEF AXDLL}  // <----------- AXDLL
            or StatusBar.QueryAbort
            {$ENDIF}  // <----------- AXDLL
          then begin
          AllAbort:=TRUE;
          Break;
        end;
        {$IFNDEF AXDLL}  // <----------- AXDLL
        StatusBar.Progress:=round(actualn);
        {$ENDIF}  // <----------- AXDLL
        actual:=actualn;
      end;
      inc(cntx);

      count:=0;
      index:=0;
      List:=TLink(p).FrontList;
      while count<List.count do begin
        if List[Index]<>NIL then begin
          Edge:=TEdge(List[Index]^);
          EstablishLink(Edge);
          inc(Count);
        end;
        inc(Index);
      end;

      {Merge BackList}
      count:=0;
      index:=0;
      List:=TLink(p).BackList;
      while count<List.count do begin
        if List[Index]<>NIL then begin
          Edge:=TEdge(List[Index]^);
          EstablishLink(Edge);
          inc(Count);
        end;
        inc(Index);
      end;


      p:=p.next;
    end;
  end;


Procedure TArea.EstablishLink
   (
   Edge     : TEdge
   );
  var  NextNode    : TLink;
       NextIndex   : Longint;
       LinkIndex   : Longint;
  begin
    NextNode:=Edge.next.back;

    if Edge.next.v = 1 then begin
      {BackList}
      NextIndex:=Edge.next.Index;
      inc(NextIndex);
      if NextNode.BackList.Available(NextIndex) then
      Edge.next.link:=TEdge(NextNode.BackList[NextIndex]^) else begin
        if NextNode.FrontList.Count>0 then Edge.next.link:=TEdge(NextNode.FrontList.FirstItem^) else
        if NextNode.BackList.Count >1 then Edge.next.link:=TEdge(NextNode.BackList.FirstItem^);
      end;
    end
    else if Edge.next.v = 2 then begin
      {FrontList}
      NextIndex:=Edge.next.Index;
      inc(NextIndex);
      if NextNode.FrontList.Available(NextIndex) then
      Edge.next.Link:=TEdge(NextNode.FrontList[NextIndex]^)else begin
        if NextNode.BackList.Count>0 then Edge.next.Link:=TEdge(NextNode.BackList.FirstItem^) else begin
          if NextNode.FrontList.Count>1 then Edge.next.link:=TEdge(NextNode.FrontList.FirstItem^);
        end;
      end;
    end;
  end;
  (*
Procedure TArea.EstablishLink
   (
   Edge     : TEdge
   );
  var  NextNode    : TLink;
       NextIndex   : Integer;
       LinkIndex   : Integer;
  begin
    NextNode:=Edge.next.back;
    NextIndex:=Edge.next.Index;
    if Edge.next.v=1 then begin
      {BackList}
      if NextNode.BackList.Available(NextIndex+1) then
      Edge.next.link:=TEdge(NextNode.BackList[NextIndex+1]^) else begin
        if NextNode.FrontList.Count>0 then Edge.next.link:=TEdge(NextNode.FrontList[0]^) else begin
          if NextNode.BackList.Count>1 then Edge.next.link:=TEdge(NextNode.BackList[0]^);
        end;
      end;
    end
    else if Edge.next.v = 2 then begin
      {FrontList}
      if NextNode.FrontList.Available(NextIndex+1) then
      Edge.next.Link:=TEdge(NextNode.FrontList[NextIndex+1]^)else begin
        if NextNode.BackList.Count>0 then Edge.next.Link:=TEdge(NextNode.BackList[0]^) else begin
          if NextNode.FrontList.Count>1 then Edge.next.link:=TEdge(NextNode.FrontList[0]^);
        end;
      end;
    end;
  end;
   *)
Procedure TArea.SelectLine
   (
   go : TEdge
   );
  var x1       : double;
      y1       : double;
      x2       : double;
      y2       : double;
      mid      : TDPoint;
      SelPt    : PIndex;
  begin
    x1:=go.x;
    y1:=go.y;
    x2:=go.next.x;
    y2:=go.next.y;
    mid.x:=round(x1+(x2-x1)/2);
    mid.y:=round(y1+(y2-y1)/2);
    SelPt:=PProj(Proj)^.Layers^.SelectByPoint(PProj(Proj)^.Pinfo,mid,ot_Any);
  end;

Procedure TArea.Trace;
  var p           : TSLBaseItem;
      List        : TRecList;
      count       : Integer;
      Index       : Integer;
      Edge        : TEdge;
      Start       : TEdge;
      go          : TEdge;
      Abbruch     : Boolean;
      ar          : Double;
      f           : TextFile;
      tb          : Boolean;
  begin
    AssignFile(f,'d:\dat\outc.dat');
    rewrite(f);
    p:=NodeList.GetHead;
    while p<>NIL do begin
      count:=0;
      index:=0;
      List:=TLink(p).FrontList;
      while count<List.count do begin
        if List[Index]<>NIL then begin
          Edge:=TEdge(List[Index]^);

          ar:=0;
          Start:=Edge;
          Abbruch:=False;
          go:=Start;
          {$IFDEF DEBUG}
          {try
           If go.x=7676524 then raise EListError.Create('Position found!');
          except
          end; }
          {$ENDIF}
          if go.v<5 then begin
            repeat
              if (go.next.v<5)or(go.next.v=20) then begin
                ar:=ar+((go.x+go.next.x)*(go.y-go.next.y));
                go.v:=10;
                go:=go.next;
                {SelectLine(go);}
                FResultList.Add(go.next);
              end
              else if go.next.v=10 then begin
                Abbruch:=TRUE;
              end;
              if go.link<>NIL then begin
                go:=go.link;
              end
              else begin
                Abbruch:=TRUE;
              end;
            until (go=Start)or(Abbruch);
          end;{go.v < 5; entspricht go.v = 0}
          ActualiceTrace;
          {PProj(Proj)^.DeselectAll(tb);}
          if not Abbruch then begin
            if (FResultList.Count>0)and (ar < 0) then begin
              {FResultCol.Add(FResultList);}
               if Data.Area=ar_Area then DrawFResultList;                           {******************}
              {SaveFResultList(f);}
            end else
            if(FResultList.Count>0)and (ar > 0) then begin
               if Data.Area=ar_Envelope then DrawFResultListEnvelope;
            end;
          end;{not Abbruch}
          FResultList.Free;
          FResultList:=TList.Create;

          inc(Count);
        end;
        inc(Index);
      end;
      p:=p.next;
    end;
    CloseFile(f);
  end;

Function TArea.ActualiceTrace
   : Double;
  var cnt    : Integer;
      Edge   : TEdge;
      APoint : TDPoint;
      ar     : Double;
  begin
    ar:=0;
    for cnt:=0 to FResultList.count-1 do begin
      Edge:=FResultList[cnt];
      if Edge.v=10 then Edge.v:=20;
      if Edge.next.v=10 then Edge.next.v:=20;
      ar:=ar+((Edge.x+Edge.next.x)*(Edge.y-Edge.next.y));

      {$IFDEF TEST2}
     { Apoint.x:=round(Edge.x);
      APoint.y:=round(Edge.y);
      PProj(Proj)^.SelectPoint(APoint);
      PProj(Proj)^.DeletePoint;
      DrawRect.AssignByPoint(Apoint,100);
      PProj(Proj)^.PInfo^.RedrawRect(DrawRect);}
      {$ENDIF}

    end;
    {$IFDEF TEST}
     InvalidateRect(PProj(Proj)^.Parent^.Handle,NIL,TRUE);
    {$ENDIF}
    Result:=ar;
  end;

Procedure TArea.SaveFResultList
   (
   var f:TextFile
   );
  var Edge    : TEdge;
      cnt     : Integer;

  begin
    Append(f);
    writeln(f,0,' ',0);
    for cnt:=0 to FResultList.Count-1 do begin
      Edge:=FResultList[Cnt];
      writeln(f,Edge.x:5:2,' ',Edge.y:5:2);
    end;
  end;

Procedure TArea.DrawFResultList;
  var Edge    : TEdge;
      cnt     : Integer;
      AObject : PCPoly;
      APoint  : PDPoint;

  begin
    AObject:=New(PCPoly,Init);
    APoint :=New(PDPoint);
    for cnt:=0 to FResultList.Count-1 do begin
      Edge:=FResultList[Cnt];
      APoint^.Init(round(Edge.x),round(Edge.y));
      AObject^.InsertPoint(APoint^);
    end;
    AObject^.ClosePoly;
    AObject^.CalculateClipRect;

    if ManualMode then begin
       if AObject^.PointInside(PProj(Proj)^.PInfo,ManualPos) then begin
          if not PProj(Proj)^.InsertObjectOnLayer(AObject,Data.DestLayer) then begin
             AllAbort:=TRUE;
          end
          else begin
             {$IFNDEF AXDLL}
             ProcDBSendObject(Proj,AObject,'EIN');
             {$ENDIF}
          end;
       end;
    end
    else begin
       if not PProj(Proj)^.InsertObjectOnLayer(AObject,Data.DestLayer) then AllAbort:=TRUE;
    end;

    Dispose(APoint,Done);
  end;

Procedure TArea.DrawFResultListEnvelope;
  var Edge    : TEdge;
      cnt     : Integer;
      AObject : PCPoly;
      APoint  : PDPoint;

  begin
    AObject:=New(PCPoly,Init);
    APoint :=New(PDPoint);
    for cnt:=0 to FResultList.Count-1 do begin
      Edge:=FResultList[Cnt];
      APoint^.Init(round(Edge.x),round(Edge.y));
      AObject^.InsertPoint(APoint^);
    end;
    AObject^.ClosePoly;
    AObject^.CalculateClipRect;
    if not PProj(Proj)^.InsertObjectOnLayer(AObject,Data.DestLayer) then {sl}
        AllAbort:=TRUE;
    Dispose(APoint,Done);
  end;

Procedure TArea.DrawPoints;
  var APoint   : PDPoint;
      AObject  : PPixel;
      index    : integer;
      count    : integer;
      Edge     : TEdge;
      List     : TRecList;
      p        : TSLBaseItem;
      ALayer   : PLayer;
  begin
    p:=FinalList.GetHead;
    APoint:=new(PDPoint,Init(10,10));
    while p<>NIL do begin
      count:=0;
      index:=0;
      List:=TLink(p).FrontList;
      while count<List.count do begin
        if List[Index]<>NIL then begin
          Edge:=TEdge(List[Index]^);

          APoint^.Init(round(Edge.x),round(Edge.y));
          AObject:=New(PPixel,Init(APoint^));
          if not PProj(Proj)^.InsertObjectonLayer(AObject,Data.DestLayer) then {sl}
              AllAbort:=TRUE;

          APoint^.Init(round(Edge.Next.x),round(Edge.Next.y));
          AObject:=New(PPixel,Init(APoint^));
          if not PProj(Proj)^.InsertObjectonLayer(AObject,Data.DestLayer) then {sl}
              AllAbort:=TRUE;

          inc(Count);
        end;
        inc(Index);
      end;
      p:=p.next;
    end;
    Dispose(APoint,Done);
  end;

Procedure TArea.DrawLines;
  var APoint   : PDPoint;
      AObject  : PPoly;
      index    : integer;
      count    : integer;
      Edge     : TEdge;
      List     : TRecList;
      p        : TSLBaseItem;
      ALayer   : PLayer;
      ACount   : Integer;
  begin
    p:=FinalList.GetHead;
    APoint:=new(PDPoint,Init(10,10));
    ACount:=0;
    while p<>NIL do begin
      count:=0;
      index:=0;
      List:=TLink(p).FrontList;
      while count<List.count do begin
        if List[Index]<>NIL then begin
         try
          Edge:=TEdge(List[Index]^);

          AObject:=new(PPoly,Init);
          APoint^.Init(round(Edge.x),round(Edge.y));
          AObject^.InsertPoint(APOint^);
          APoint^.Init(round(Edge.next.x),round(Edge.next.y));
          AObject^.InsertPoint(APoint^);

          if not PProj(Proj)^.InsertObjectonLayer(AObject,Data.DestLayer,True) then {sl}
              AllAbort:=TRUE;
         finally
          inc(Count);
         end;
        end;
        inc(Index);
      end;
      p:=p.next;

      StatusBar.Progress:=(100/FinalList.Count)*ACount;
      Inc(ACount);
    end;
    Dispose(APoint,Done);
  end;

Procedure TArea.DrawLines2
  (
  Dest : TSLBaseItem
  );
  var APoint   : PDPoint;
      AObject  : PPoly;
      index    : integer;
      count    : integer;
      Edge     : TEdge;
      List     : TRecList;
      p        : TSLBaseItem;
      ALayer   : PLayer;
  begin
 {   ALayer:=PProj(Proj)^.Layers^.IndexToLayer(Data.DestLayer);
    if ALayer<>NIL then begin
      Dispose(ALayer,Done);
      ALayer:=Proj^.Layers^.InsertLayer('Ergebnislayer',c_LRed,lt_Solid,c_LRed,pt_Solid,ilTop);
      Data.DestLayer:=ALayer^.Index;
    end;
  }
    p:=NodeList.GetHead;
    APoint:=new(PDPoint);
    while (p<>Dest.next)and(p<>NIL) do begin
      count:=0;
      index:=0;
      List:=TLink(p).FrontList;
      while count<List.count do begin
        if List[Index]<>NIL then begin
          Edge:=TEdge(List[Index]^);

          AObject:=new(PPoly,Init);
          APoint^.Init(round(Edge.x),round(Edge.y));
          AObject^.InsertPoint(APOint^);
          APoint^.Init(round(Edge.next.x),round(Edge.next.y));
          AObject^.InsertPoint(APoint^);
          if not PProj(Proj)^.InsertObjectonLayer(AObject,Data.DestLayer) then {sl}
              AllAbort:=TRUE;
          inc(Count);
        end;
        inc(Index);
      end;
      p:=p.next;
    end;
  end;

Procedure TArea.EleminateLines;
  var p           : TSLBaseItem;
      List        : TRecList;
      count       : Integer;
      Index       : Integer;
      LBack       : TLink;
      Edge        : TEdge;
      go          : TEdge;
      help        : TEdge;
      deleted     : Boolean;
      leave       : Boolean;
  begin
  p:=FinalList.GetHead;
    while p<>NIL do begin
      count:=0;
      index:=0;
      deleted:=false;
      List:=TLink(p).FrontList;
        while count<List.count do begin
        if List[Index]<>NIL then begin
          Edge:=TEdge(List[Index]^);
          if Edge.v<5 then begin
            if (TLink(p).BackList.count=0) and (List.count=1) then begin
              go:=Edge;
              leave:=false;
              deleted:=true;
              while go<>NIL do begin
                LBack:=TLink(go.back);
                help:=go;
                go:=go.next;
                go:=go.link;
                if go<>NIL then
                if TLink(go.Back).MCount>2 then leave:=TRUE;
                if Data.ERLayer then InsertLineToErrorLayer(help);
                KillWholeEdge(help);
                if leave then break;
              end;
            end;

          end;{Edge.v<5}
          inc(Count);

        end;
        inc(Index);
      end;
      p:=p.next;
    end;
    {CloseFile(f);}
  end;

Procedure TArea.MergetoPolyLines;
 var p           : TSLBaseItem;
      List        : TRecList;
      count       : Integer;
      Index       : Integer;
      Edge        : TEdge;
      next        : TEdge;
      APoint      : PDPoint;
      AObject     : PPoly;
      ALayer      : PLayer;
      sel         : Boolean;
  begin
    p:=FinalList.GetHead;
    APoint:=new(PDPoint,Init(10,10));
    while p<>NIL do begin
      count:=0;
      index:=0;

      List:=TLink(p).FrontList;
      if (List.Count + TLink(p).BackList.Count)>2 then begin
        while count<List.count do begin
          if List[Index]<>NIL then begin
            Edge:=TEdge(List[Index]^);
            if Edge.v<>100 then begin
              next:=Edge.next;
              Edge.v:=100;
              next.v:=100;
              AObject:=new(PPoly,Init);
              APoint^.Init(round(Edge.x),round(Edge.y));
              AObject^.InsertPoint(APOint^);
              while (next.link<>NIL) do begin
                if (next.link.link=next)and(next.back<>TLink(p)) then begin
                  Edge:=next.link;
                  next:=Edge.next;
                  Edge.v:=100;
                  next.v:=100;
                  APoint^.Init(round(Edge.x),round(Edge.y));
                  AObject^.InsertPoint(APOint^);
                end else break;
              end;
              APoint^.Init(round(next.x),round(next.y));
              AObject^.InsertPoint(APOint^);
              if not PProj(Proj)^.InsertObjectonLayer(AObject,Data.DestLayer) then {sl}
                  AllAbort:=TRUE;
            end;
            inc(count);
          end;
          inc(Index);
        end;
      end;

      p:=p.next;
    end;
    Dispose(Apoint,Done);
  end;

Procedure TArea.MergetoPolyLinesBack;
 var p           : TSLBaseItem;
      List        : TRecList;
      count       : Integer;
      Index       : Integer;
      Edge        : TEdge;
      next        : TEdge;
      APoint      : PDPoint;
      AObject     : PPoly;
      ALayer      : PLayer;
      sel         : Boolean;
  begin
    p:=FinalList.GetHead;
    APoint:=new(PDPoint,Init(10,10));
    while p<>NIL do begin
      count:=0;
      index:=0;
      List:=TLink(p).BackList;
      if (List.Count + TLink(p).FrontList.Count)>2 then begin
        while count<List.count do begin
          if List[Index]<>NIL then begin
            Edge:=TEdge(List[Index]^);
            if Edge.v<>100 then begin
              next:=Edge.next;
              Edge.v:=100;
              next.v:=100;
              AObject:=new(PPoly,Init);
              APoint^.Init(round(Edge.x),round(Edge.y));
              AObject^.InsertPoint(APOint^);
              while (next.link<>NIL) do begin
                if (next.link.link=next)and(next.back<>TLink(p)) then begin
                  Edge:=next.link;
                  next:=Edge.next;
                  Edge.v:=100;
                  next.v:=100;
                  APoint^.Init(round(Edge.x),round(Edge.y));
                  AObject^.InsertPoint(APOint^);
                end else break;
              end;
              APoint^.Init(round(next.x),round(next.y));
              AObject^.InsertPoint(APOint^);
              if not PProj(Proj)^.InsertObjectonLayer(AObject,Data.DestLayer) then {sl}
                  AllAbort:=TRUE;
            end;
            inc(count);
          end;
          inc(Index);
        end;
      end;

      p:=p.next;
    end;
    Dispose(Apoint,Done);
  end;

Procedure TArea.EleminateLinesBack;
  var p           : TSLBaseItem;
      List        : TRecList;
      count       : Integer;
      Index       : Integer;
      LBack       : TLink;
      Edge        : TEdge;
      go          : TEdge;
      help        : TEdge;
      deleted     : Boolean;
      leave       : Boolean;
  begin
  p:=FinalList.GetHead;
    while p<>NIL do begin
      count:=0;
      index:=0;
      deleted:=false;
      List:=TLink(p).BackList;
        while count<List.count do begin
        if List[Index]<>NIL then begin
          Edge:=TEdge(List[Index]^);
          if Edge.v<5 then begin
            if (TLink(p).FrontList.count=0) and (List.count=1) then begin
              go:=Edge;
              leave:=false;
              deleted:=true;
              while go<>NIL do begin
                LBack:=TLink(go.back);
                help:=go;
                go:=go.next;
                go:=go.link;
                if go<>NIL then
                if TLink(go.Back).MCount>2 then leave:=TRUE;
                if Data.ERLayer then InsertLineToErrorLayer(help);
                KillWholeEdge(help);
                if leave then break;
              end;
            end;

          end;{Edge.v<5}
          inc(Count);

        end;
        inc(Index);
      end;
      p:=p.next;
    end;
    {CloseFile(f);}
  end;


Procedure TArea.BackTrace;
  var p           : TSLBaseItem;
      List        : TRecList;
      count       : Integer;
      Index       : Integer;
      Edge        : TEdge;
      Start       : TEdge;
      go          : TEdge;
      ar          : Double;
      f           : TextFile;
      tb          : Boolean;
      APoint      : TDPoint;
      APixel      : PPixel;
      SelPt       : PIndex;
      LBack       : TLink;
      Layer       : PLayer;
      whole       : longint;
      actualn     : double;
      actual      : double;
      cnt         : longint;

  begin
    {$IFNDEF AXDLL}  // <----------- AXDLL
    StatusBar.Progress:=0;
    {$ENDIF}  // <----------- AXDLL
    {AssignFile(f,'d:\dat\outc.dat');
    rewrite(f);}
    actual:=0;
    cnt:=0;

    p:=FinalList.GetHead;
    while p<>NIL do begin

      inc(cnt);
      actualn:=((100/Finallist.count)*cnt);
        if (actualn-actual)> 1 then begin
        if AllAbort                                         {sl}
            {$IFNDEF AXDLL}  // <----------- AXDLL
            or StatusBar.QueryAbort
            {$ENDIF}  // <----------- AXDLL
          then begin
          AllAbort:=TRUE;
          Break;
        end;
        {$IFNDEF AXDLL}  // <----------- AXDLL
        StatusBar.Progress:=round(actualn);
        {$ENDIF}  // <----------- AXDLL
        actual:=actualn;
      end;

      count:=0;
      index:=0;
      List:=TLink(p).FrontList;
      while count<List.count do begin
        if List[Index]<>NIL then begin
          Edge:=TEdge(List[Index]^);

          ar:=0;
          Start:=Edge;
          Abbruch:=False;
          go:=Start;

          if go.v<5 then begin
            repeat
              if go.v=20 then Abbruch:=TRUE;
              if (go.next.v<5)or(go.next.v=20) then begin
                go.v:=10;
                if FResultList.Count > 16000 then begin
                  {$IFNDEF AXDLL}  // <----------- AXDLL
                  if not Data.Intersections then
                  MsgBox(PProj(Proj)^.Parent.Handle,3921,mb_IconExclamation or mb_Ok,'') else
                  MsgBox(PProj(Proj)^.Parent.Handle,3922,mb_IconExclamation or mb_Ok,'');
                  {$ENDIF}  // <----------- AXDLL
                  AllAbort:=TRUE;
                  Exit;
                end;
                FResultList.Add(go);
                FDrawList.Add(go);
                 go:=go.next;
              end;
              if go.link<>NIL then begin
                go:=go.link;
              end
              else begin
                {raise ElistError.Create('Segment End Nil');}
                Abbruch:=True;
              end;
              if not Abbruch then
              if (go.next.v=10)and(go.link.next.v<5) then begin
                {try}
                BackTraverse(go,Start);
                {except begin DrawList;Exit;end;
                end; }
              end;
            until (go=Start)or(Abbruch);
          end;{go.v < 5; entspricht go.v = 0}
          ar:=ActualiceTrace;
          if not Abbruch then begin
            if (FResultList.Count>0)and (ar < 0) then begin
               if Data.Area=ar_Area then DrawFResultList;                           {******************}
            end else
            if(FResultList.Count>0)and (ar > 0) then begin
               if Data.Area=ar_Envelope then DrawFResultListEnvelope;
            end;
          end;{not Abbruch}
          FResultList.Free;
          FPointList.Free;
          FDrawList.Free;
          FResultList:=TList.Create;
          FPointlist:=TList.Create;
          FDrawList :=TList.Create;
          inc(Count);
        end;
        inc(Index);
      end;
      p:=p.next;
    end;
    {CloseFile(f);}
  end;

Procedure TArea.BackTraverse
   (
   var go      :       TEdge;
   var Start   :       TEdge
   );
  var help       : TEdge;
      AItem      : TEdge;
      APoint     : TDPoint;
      APixel     : PPixel;
      Layer      : PLayer;
  begin
    while(go.next.v=10)and(go.link.next.v<5)do begin
      help:=go.next;
      repeat
        AItem:=TEdge(FResultList[FResultList.count-1]);
        AItem.v:=20;
        FResultList.delete(FResultList.count-1);
        AItem:=TEdge(FResultList[FResultList.count-1]);
      until AItem = help;

      while (go.next.v=10)and(go.next.link.link=go.next)and(go.next<>Start) do begin
        help:=go;
        go:=go.next;
        go:=go.link;
        help.v:=20;
        help.next.v:=20;
        KillWholeEdge(help);
        FResultList.delete(FResultList.count-1);
      end; {(go.next.v=10)and(go.next.link.link=go.next)}
      help:=go;
      if go.next=Start then Abbruch:=TRUE;
      go:=go.next;
      go:=go.link;
      help.v:=20;
      help.next.v:=20;
      KillWholeEdge(help);
      FResultList.delete(FResultList.count-1);
    end;   {go.next.v=10}
  end;

Procedure TArea.BackLink
   (
   var go     :       TEdge;
   var Start  :       TEdge
   );
  var help       : TEdge;
      AItem      : TEdge;
      APoint     : TDPoint;
      APixel     : PPixel;
      Layer      : PLayer;
  begin
      while (go.next.v=10)and(go.next.link.link=go.next)and(go.next<>Start) do begin
        help:=go;
        go:=go.next;
        go:=go.link;
        help.v:=20;
        help.next.v:=20;
        {KillWholeEdge(help);}
        FResultList.delete(FResultList.count-1);
      end; {(go.next.v=10)and(go.next.link.link=go.next)}
      help:=go;
      go:=go.next;
      go:=go.link;
      help.v:=20;
      help.next.v:=20;
      {KillWholeEdge(help);}
      FResultList.delete(FResultList.count-1);
  end;
(*
Procedure TArea.BackTrace;
  var p           : TSLBaseItem;
      List        : TRecList;
      count       : Integer;
      Index       : Integer;
      Edge        : TEdge;
      Start       : TEdge;
      go          : TEdge;
      Abbruch     : Boolean;
      ar          : Double;
      f           : TextFile;
      tb          : Boolean;
      APoint      : TDPoint;
      APixel      : PPixel;
      SelPt       : PIndex;
      LBack       : TLink;
      Layer       : PLayer;
  begin
    {AssignFile(f,'d:\dat\outc.dat');
    rewrite(f);}
    p:=NodeList.GetHead;
    while p<>NIL do begin
      count:=0;

      index:=0;
      List:=TLink(p).FrontList;
      while count<List.count do begin
        if List[Index]<>NIL then begin
          Edge:=TEdge(List[Index]^);

          ar:=0;
          Start:=Edge;
          Abbruch:=False;
          go:=Start;

          if go.v<5 then begin
            repeat
              if go.v=20 then Abbruch:=TRUE;
              if (go.next.v<5)or(go.next.v=20) then begin
                {ar:=ar+((go.x+go.next.x)*(go.y-go.next.y));}
                go.v:=10;
              {  try
                  if (abs(go.x-12063353)<1)and(abs(go.y-18045675)<1) then raise ElistError.Create('Stop');
                  except
                end;}
                FResultList.Add(go);
                FDrawList.Add(go);
                {$IFDEF TEST5}
                if FDrawlist.Count>1000 then begin
                  DrawList;
                  Exit;
                end;
                {$ENDIF}
                {$IFDEF TEST}
                APoint.Init(round(go.x),round(go.y));
                APixel:=New(PPixel,Init(APoint));
                FPointList.Add(APixel);
                PProj(Proj)^.InsertObjectOnLayer(APixel,Data.DestLayer);
                DrawRect.AssignByPoint(Apoint,100);
                PProj(Proj)^.PInfo^.RedrawRect(DrawRect);
                LoopWait;
                if StatusBar.QueryAbort then begin
                  {Exit;}
                  try
                  raise ElistError.Create('Stop');
                  except
                  end;
                end;
                {$ENDIF}

                go:=go.next;
              end;
              if go.link<>NIL then begin
                go:=go.link;
              end
              else begin
                {raise ElistError.Create('Segment End Nil');}
                Exit;
              end;
              if go.next.v=10 then begin
               if count=0 then Abbruch:=True else
                BackTraverse(go);
              end;
            until (go=Start)or(Abbruch);
          end;{go.v < 5; entspricht go.v = 0}
          ar:=ActualiceTrace;
          if not Abbruch then begin
            if (FResultList.Count>0)and (ar < 0) then begin
              {FResultCol.Add(FResultList);}
               if Data.Area=ar_Area then DrawFResultList;                           {******************}
              {SaveFResultList(f);}
            end else
            if(FResultList.Count>0)and (ar > 0) then begin
               if Data.Area=ar_Envelope then DrawFResultListEnvelope;
            end;
          end;{not Abbruch}
          FResultList.Free;
          FPointList.Free;
          FDrawList.Free;
          FResultList:=TList.Create;
          FPointlist:=TList.Create;
          FDrawList :=TList.Create;
          inc(Count);
        end;
        inc(Index);
      end;
      p:=p.next;
    end;
    {CloseFile(f);}
  end;

Procedure TArea.BackTraverse
   (
   var go :       TEdge
   );
  var help       : TEdge;
      AItem      : TEdge;
      APoint     : TDPoint;
      APixel     : PPixel;
      Layer      : PLayer;
  begin

    while(go.next.v=10)do begin
      help:=go.next;
      {AItem:=TEdge(FResultList[FResultList.count-1]);
      FDrawList.Add(AItem.next);}
      repeat
        {$IFDEF TEST}
        APixel:=PPixel(FPointList[FPointList.count-1]);
        FPointList.Delete(FPointlist.count-1);
        Layer:=PProj(Proj)^.Layers^.IndexToLayer(Data.DestLayer);
        Layer^.Select(PProj(Proj)^.PInfo,APixel);
        {Layer^.DeleteIndex(PProj(Proj)^.PInfo,APixel);}
        DrawRect.AssignByPoint(Apoint,100);
        PProj(Proj)^.PInfo^.RedrawRect(DrawRect);
        LoopWait;
        AItem:=TEdge(FResultList[FResultList.count-1]);
        if AItem.x<>APixel^.Position.x then raise ElistError.create('Koordinaten ungleich' );
        {$ENDIF}
        AItem:=TEdge(FResultList[FResultList.count-1]);
        AItem.v:=20;
        {$IFDEF TEST5}
        if FResultList.count=0 then begin
          DrawList;
          Exit;
        end;
        {$ENDIF}
        FResultList.delete(FResultList.count-1);
        FDrawList.Add(AItem);
        AItem:=TEdge(FResultList[FResultList.count-1]);
      until AItem = help;


      if go.next.link=nil then raise ElistError.Create('Nil in BackTraverse');
      while (go.next.v=10)and(go.next.link.link=go.next) do begin
        help:=go;
        go:=go.next;
        go:=go.link;
        help.v:=20;
        help.next.v:=20;
        KillWholeEdge(help);
        if StatusBar.QueryAbort then begin
          {Exit}
          try
          raise ElistError.Create('Stop');
          except
          end;
        end;

        {$IFDEF TEST}
        APixel:=PPixel(FPointList[FPointList.count-1]);
           FPointList.Delete(FPointlist.count-1);
        Layer:=PProj(Proj)^.Layers^.IndexToLayer(Data.DestLayer);
        Layer^.Select(PProj(Proj)^.PInfo,APixel);
        {Layer^.DeleteIndex(PProj(Proj)^.PInfo,APixel);}
        DrawRect.AssignByPoint(Apoint,100);
        PProj(Proj)^.PInfo^.RedrawRect(DrawRect);
        LoopWait;
        AItem:=TEdge(FResultList[FResultList.count-1]);
            if AItem.x<>APixel^.Position.x then raise ElistError.create('Koordinaten ungleich' );
        {$ENDIF}

        try
        AItem:=TEdge(FResultList[FResultList.count-1]);
            if AItem.x<>help.next.x        then raise ElistError.create('Koordinaten ungleich' );
        except
        end;
        AItem:=TEdge(FResultList[FResultList.count-1]);
         FDrawList.Add(AItem);
        FResultList.delete(FResultList.count-1);
      end; {(go.next.v=10)and(go.next.link.link=go.next)}
      help:=go;
      go:=go.next;
      go:=go.link;
      help.v:=20;
      help.next.v:=20;
      KillWholeEdge(help);

      {$IFDEF TEST}
      APixel:=PPixel(FPointList[FPointList.count-1]);
         FPointList.Delete(FPointlist.count-1);
      Layer:=PProj(Proj)^.Layers^.IndexToLayer(Data.DestLayer);
      Layer^.Select(PProj(Proj)^.PInfo,APixel);
      DrawRect.AssignByPoint(Apoint,100);
      PProj(Proj)^.PInfo^.RedrawRect(DrawRect);
      LoopWait;
      {$ENDIF}
      {$IFDEF TEST5}
      if FResultList.count=0 then begin
          DrawList;
          Exit;
        end;
       {$ENDIF}
      FResultList.delete(FResultList.count-1);

    end;   {go.next.v=10}
  end;
*)


Constructor TLink.Create;
  begin
    inherited Create;
    FrontList:=TRecList.Create(isFrontList,sizeof(TEdge));
    BackList :=TRecList.Create(isFrontList,sizeof(TEdge));
  end;

Constructor TLink.Create2;
  begin
    inherited Create;
  end;

{$IFDEF DEBUG}
(*Procedure TArea.PrintFList;
  begin
  end;

Procedure TArea.PrintNode
   (
   p:TSLBaseItem
   );
  var f       : TextFile;
      count   : Integer;
      Index   : Integer;
      SList   : TRecList;
      Edge    : TEdge;
  begin
    AssignCrt(f);
    rewrite(f);
    Writeln(f, 'Entry','@Node: ',longint(p));
      {FrontList}
      count:=0;
      index:=0;
      SList:=TLink(p).FrontList;
      writeln(f,'FrontList');
      while count<SList.count do begin
        if SList[Index]<>NIL then begin
          Edge:=TEdge(SList[Index]^);
          write(f,Edge.x:5:2,' ',Edge.y:5:2,'next:  ',Edge.next.x:5:2,' ',Edge.next.y:5:2,
          ' Index: ',Edge.Index,' Node:',longint(Edge.back),' v: ',Edge.v,' ');
          if Edge.next.Link<>NIL then
          writeln(f,'Link: ',Edge.next.link.next.x:5:2,' ',Edge.next.link.next.y:5:2) else writeln(f,'NIL');
          inc(Count);
        end;
        inc(Index);
      end;

      count:=0;
      index:=0;
      SList:=TLink(p).BackList;
      writeln(f,'BackList');
      while count<SList.count do begin
        if SList[Index]<>NIL then begin
          Edge:=TEdge(SList[Index]^);
          write(f,Edge.x:5:2,' ',Edge.y:5:2,'next:  ',Edge.next.x:5:2,' ',Edge.next.y:5:2,
          ' Index: ',Edge.Index,' Node:',longint(Edge.back),' v: ',Edge.v,' ');
          if Edge.next.Link<>NIL then
          writeln(f,'Link: ',Edge.next.link.next.x:5:2,' ',Edge.next.link.next.y:5:2) else writeln(f,'NIL');
          inc(Count);
        end;
        inc(Index);
      end;
      CloseFile(f);
  end;   *)
{$ENDIF}

Procedure TArea.PrintNodeList;
  var p           : TSLBaseItem;
      SList       : TRecList;
      count       : Integer;
      Index       : Integer;
      Edge        : TEdge;
      f           : TextFile;
      c           : integer;
  begin
    Assignfile(f,'d:\dat\node.dat');
    rewrite(f);
    p:=NodeList.GetHead;
    c:=0;
    while p<>NIL do begin
      inc(c);

      Writeln(f, 'Entry',c,'@Node: ',longint(p));
      {FrontList}
      count:=0;
      index:=0;
      SList:=TLink(p).FrontList;
      writeln(f,'FrontList');
      while count<SList.count do begin
        if SList[Index]<>NIL then begin
          Edge:=TEdge(SList[Index]^);
          write(f,Edge.x:5:2,' ',Edge.y:5:2,'next:  ',Edge.next.x:5:2,' ',Edge.next.y:5:2,
          ' Index: ',Edge.Index,' n ' ,Edge.next.Index,' Node:',longint(Edge.back),' v: ',Edge.v,' ');
          if Edge.next.Link<>NIL then
          writeln(f,'Link: ',Edge.next.link.next.x:5:2,' ',Edge.next.link.next.y:5:2) else writeln(f,'NIL');
          inc(Count);
        end;
        inc(Index);
      end;

      {BackList}
      count:=0;
      index:=0;
      SList:=TLink(p).BackList;
      writeln(f,'BackList');
      while count<SList.count do begin
        if SList[Index]<>NIL then begin
          Edge:=TEdge(SList[Index]^);
          write(f,Edge.x:5:2,' ',Edge.y:5:2,'next:  ',Edge.next.x:5:2,' ',Edge.next.y:5:2,
          ' Index: ',Edge.Index,' n ',Edge.next.Index,' Node:',longint(Edge.back),' v: ',Edge.v,' ');
          if Edge.next.Link<>NIL then
          writeln(f,'Link: ',Edge.next.link.next.x:5:2,' ',Edge.next.link.next.y:5:2) else writeln(f,'NIL');
          inc(Count);
        end;
        inc(Index);
      end;
   {
      count:=0;
      index:=0;
      SList:=TLink(p).List;
      Writeln(f, 'Entry',c,'@Node: ',longint(p));
      while count<SList.count do begin
        if SList[Index]<>NIL then begin
          Edge:=TEdge(SList[Index]^);
          writeln(f,Edge.x:5:2,' ',Edge.y:5:2,'next:  ',Edge.next.x:5:2,' ',Edge.next.y:5:2,
          ' Index: ',Edge.Index,' Node:',longint(Edge.back),' v: ',Edge.v);
          inc(Count);
        end;
        inc(Index);
      end;

    }
      p:=p.next;
    end;
    Closefile(f);
  end;

Procedure TArea.PrintFinalList;
  var p           : TSLBaseItem;
      SList       : TRecList;
      count       : Integer;
      Index       : Integer;
      Edge        : TEdge;
      f           : TextFile;
      c           : integer;
  begin
    Assignfile(f,'d:\dat\final.dat');
    rewrite(f);
    p:=FinalList.GetHead;
    c:=0;
    while p<>NIL do begin
      inc(c);

      Writeln(f, 'Entry',c,'@Node: ',longint(p));
      {FrontList}
      count:=0;
      index:=0;
      SList:=TLink(p).FrontList;
      writeln(f,'FrontList');
      while count<SList.count do begin
        if SList[Index]<>NIL then begin
          Edge:=TEdge(SList[Index]^);
          write(f,Edge.x:5:2,' ',Edge.y:5:2,'next:  ',Edge.next.x:5:2,' ',Edge.next.y:5:2,
          ' Index: ',Edge.Index,' n ' ,Edge.next.Index,' Node:',longint(Edge.back),' v: ',Edge.v,' ');
          if Edge.next.Link<>NIL then
          writeln(f,'Link: ',Edge.next.link.next.x:5:2,' ',Edge.next.link.next.y:5:2) else writeln(f,'NIL');
          inc(Count);
        end;
        inc(Index);
      end;

      {BackList}
      count:=0;
      index:=0;
      SList:=TLink(p).BackList;
      writeln(f,'BackList');
      while count<SList.count do begin
        if SList[Index]<>NIL then begin
          Edge:=TEdge(SList[Index]^);
          write(f,Edge.x:5:2,' ',Edge.y:5:2,'next:  ',Edge.next.x:5:2,' ',Edge.next.y:5:2,
          ' Index: ',Edge.Index,' n ',Edge.next.Index,' Node:',longint(Edge.back),' v: ',Edge.v,' ');
          if Edge.next.Link<>NIL then
          writeln(f,'Link: ',Edge.next.link.next.x:5:2,' ',Edge.next.link.next.y:5:2) else writeln(f,'NIL');
          inc(Count);
        end;
        inc(Index);
      end;

      p:=p.next;
    end;
    Closefile(f);
  end;

Procedure TArea.PrintNodeList1;
  var p           : TSLBaseItem;
      SList       : TRecList;
      count       : Integer;
      Index       : Integer;
      Edge        : TEdge;
      f           : TextFile;
      c           : integer;
  begin
    Assignfile(f,'d:\dat\node1.dat');
    rewrite(f);
    p:=NodeList.GetHead;
    c:=0;
    while p<>NIL do begin
      writeln(f,'x: ',TLink(p).x:5:2,'y: ',TLink(p).y:5:2);
      p:=p.next;
    end;
    Closefile(f);
  end;

Procedure TArea.PrintNodeList2;
  var p           : TSLBaseItem;
      SList       : TRecList;
      count       : Integer;
      Index       : Integer;
      Edge        : TEdge;
      f           : TextFile;
      c           : integer;
  begin
    Assignfile(f,'d:\dat\node2.dat');
    rewrite(f);
    p:=finalList.GetHead;
    c:=0;
    while p<>NIL do begin
      writeln(f,'x: ',TLink(p).x:5:2,'y: ',TLink(p).y:5:2);
      p:=p.next;
    end;
    Closefile(f);
  end;

Procedure TArea.PrintNodeList3;
  var p           : TSLBaseItem;
      SList       : TRecList;
      count       : Integer;
      Index       : Integer;
      Edge        : TEdge;
      f           : TextFile;
      c           : integer;
      Linknext    : TLink;
      Edgenext    : TEdge;
      IndexNext   : Integer;
      EdgeMirror  : TEdge;
  begin
    Assignfile(f,'d:\dat\node.dat');
    rewrite(f);
    p:=NodeList.GetHead;
    c:=0;
    while p<>NIL do begin
      inc(c);

      Writeln(f, 'Entry',c,'@Node: ',longint(p),'  ' ,TLink(p).x:5:2,'  ' ,TLink(p).y:5:2);
      {FrontList}
      count:=0;
      index:=0;
      SList:=TLink(p).List;
      writeln(f,'FrontList');
      while count<SList.count do begin
        if SList[Index]<>NIL then begin
          {EdgeNext und EdgeMirror bezeichnen dasselbe DatenElement }
          {Pointer muessen ident sein!}
          Edge:=TEdge(SList[Index]^);
          LinkNext:=Edge.next.back;
          IndexNext:=Edge.next.Index;
          EdgeNext:=TEdge(LinkNext.List[IndexNext]^);
          EdgeMirror:=EdgeNext.Next;
          writeln(f,Edge.x:5:2,' ',longint(Edge),'  ',longint(EdgeMirror),' c: ',SList.Capacity,' ',SList.count,
          ' i: ',Index,' next: ',Edge.next.x:5:2,' ',longint(Edge.next),'  ' ,longint(EdgeNext),
          ' c: ',Slist.Capacity,' ',SList.Count,' i: ',IndexNext);
          inc(Count);
        end;
        inc(Index);
      end;
      writeln(f);
      p:=p.next;
    end;
    Closefile(f);
  end;


Procedure TLink.InsertEdge
   (
   Edge:TEdge
   );
  begin
    Edge.Index:=List.Add(Edge);
    Edge.back:=Self;
  end;

Procedure TLink.InsertEdgeSec
   (
   Edge     :TEdge;
   ListType :Integer
   );
  begin
    if ListType = 1 then begin
      Edge.Index:=BackList.Add(Edge);
      Edge.back:=Self;
      Edge.v:=1;
    end else
    if ListType = 2 then begin
      Edge.Index:=FrontList.Add(Edge);
      Edge.back:=Self;
      Edge.v:=2;
    end;
  end;

Procedure TLink.VerifyInsertEdge
   (
   Edge:TEdge
   );
  begin
    {1: BackList}
    {2: FrontList}
    if Edge.x < Edge.next.x then begin
      InsertEdgeSec(Edge,2);
    end else
    if Edge.x > Edge.next.x then begin
      InsertEdgeSec(Edge,1);
    end else
    if (abs(Edge.x-Edge.next.x)<Eps) then begin
      if Edge.y < Edge.next.y then begin
        InsertEdgeSec(Edge,2);
      end else
      if Edge.y > Edge.next.y then begin
        InsertEdgeSec(Edge,1);
      end else
      if (abs(Edge.y-Edge.next.y)<Eps) then begin
        try
        raise ElistError.Create('Equal Points found! ');
        except
        end;
      end;
    end;
  end;

Procedure TLink.InsertList2
   (
   SCol   : TSLBaseItem;
   SCount : Integer
   );
  var Edge   : TEdge;
      Shelp  : TSLBaseItem;
  begin
    List:=TRecList.Create(SCount,sizeof(TEdge));
    while SCol<>NIL do begin
      Edge:=TEdge(TEdgeListItem(SCol).PtEdge);
      InsertEdge(Edge);
      Shelp:=SCol;
      SCol:=SCol.next;
      SHelp.free;
    end;
  end;

Procedure TLink.InsertList
   (
   SList:TRecList
   );
  var count : Integer;
      Index : Integer;
      Edge  : TEdge;
  begin
    List:=TRecList.Create(SList.Count,sizeof(TEdge));
    count:=0;
    index:=0;
    while count<SList.count do begin
      if SList[index]<>NIL then begin
        Edge:=TEdge(SList[Index]^);
        InsertEdge(Edge);
        inc(count);
      end;
      inc(Index);
    end;
  end;


Procedure  TLink.CalculateMean;
  var SumX  : Double;
      SumY  : Double;
      Count : Integer;
      Index : Integer;
      Edge  : TEdge;
      MeanX : Double;
      MeanY : Double;
  begin
    count:=0;
    index:=0;
    SumX :=0;
    SumY :=0;
    while count<List.count do begin
      if List[index]<>NIL then begin
        Edge:=TEdge(List[Index]^);
        SumX:=SumX+Edge.x;
        SumY:=SumY+Edge.y;
        inc(count);
      end;
      inc(Index);
    end;
    MeanX:=SumX/count;
    MeanY:=SumY/count;

    count:=0;
    index:=0;
    while count<List.count do begin
      if List[index]<>NIL then begin
        Edge:=TEdge(List[Index]^);
        Edge.x:=MeanX;
        Edge.y:=MeanY;
        inc(count);
      end;
      inc(Index);
    end;
    X:=MeanX;
    Y:=MeanY;
  end;

 Procedure  TLink.CalculateMean2;
  var SumX  : Double;
      SumY  : Double;
      Count : Integer;
      Index : Integer;
      Edge  : TEdge;
      MeanX : Double;
      MeanY : Double;
      cntx  : Integer;
  begin
    cntx:=0;
    SumX :=0;
    SumY :=0;
    count:=0;
    index:=0;
    while count<FrontList.count do begin
      if FrontList[index]<>NIL then begin
        Edge:=TEdge(FrontList[Index]^);
        SumX:=SumX+Edge.x;
        SumY:=SumY+Edge.y;
        inc(count);
        inc(cntx);
      end;
      inc(Index);
    end;

    count:=0;
    index:=0;
    while count<BackList.count do begin
      if BackList[index]<>NIL then begin
        Edge:=TEdge(BackList[Index]^);
        SumX:=SumX+Edge.x;
        SumY:=SumY+Edge.y;
        inc(count);
        inc(cntx);
      end;
      inc(Index);
    end;

   { try
    if SumX=0 then raise ElistError.Create('Summe Null');
    except end;}
    if cntx> 0 then begin
    MeanX:=SumX/cntx;
    MeanY:=SumY/cntx;
    end else begin Meanx:=0;Meany:=0;end;

    count:=0;
    index:=0;
    try
    while count<BackList.count do begin
      if BackList[index]<>NIL then begin
        Edge:=TEdge(BackList[Index]^);
        Edge.x:=MeanX;
        Edge.y:=MeanY;
        inc(count);
      end;
      inc(Index);
    end;
    except end;
    count:=0;
    index:=0;
    while count<FrontList.count do begin
      if FrontList[index]<>NIL then begin
        Edge:=TEdge(FrontList[Index]^);
        Edge.x:=MeanX;
        Edge.y:=MeanY;
        inc(count);
      end;
      inc(Index);
    end;
    X:=MeanX;
    Y:=MeanY;
  end;


 {$IFDEF DEBUG}
{ Procedure TLink.Print;
   var Count : Integer;
       Index : Integer;
       f     : TextFile;
       Edge  : TEdge;
   begin
     count:=0;
     index:=0;
     AssignCrt(f);
     rewrite(f);
     while count<List.count do begin
       if List[index]<>NIL then begin
         Edge:=TEdge(List[index]^);
         Write(f,'L: ',Edge.x:5:2 , ' ',Edge.y:5:2);
         inc(count);
       end;
       inc(Index);
     end;
     Writeln(f);
     CloseFile(f);
   end;}
 {$ENDIF}


Procedure TLink.InsertToFinalList
   (
   SCol      : TSLBaseItem;
   Scount    : Integer
   );
  var p             : TSLBaseItem;
      FrontCount    : Integer;
      BackCount     : Integer;
      index         : Integer;
      cnt           : Integer;
      Shelp         : TSLBaseItem;
      BList         : TRecList;
      FList         : TRecList;
  begin
    p:=SCol;
    FrontCount:=0;
    BackCount:=0;
    while p<>NIL do begin
      FrontCount:=FrontCount+TLink(p).FrontList.Count;
      BackCount:=BackCount  +TLink(p).BackList.Count;
      p:=p.next;
    end;
    if FrontCount > 0 then FrontList:=TRecList.Create(FrontCount,sizeof(TEdge))
    else FrontList:=TRecList.Create(isFrontList,sizeof(TEdge));
    if BAckCount > 0  then BackList :=TRecList.Create(BackCount,sizeof(TEdge))
    else BackList :=TRecList.Create(isFrontList,sizeof(TEdge));

    p:=SCol;
    while p<>NIL do begin
      BList:=TLink(p).BackList;
      FList:=TLink(p).FrontList;

      cnt:=0; index:=0;
      while cnt<BList.Count do begin
        if BList[index]<>NIL then begin
          InsertEdgeSec(TEdge(BList[index]^),1);
          inc(cnt);
        end;
        inc(index);
      end;
      cnt:=0; index:=0;
      While cnt<FList.Count do begin
        if FList[index]<>NIL then begin
          InsertEdgeSec(TEdge(FList[index]^),2);
          inc(cnt);
        end;
        inc(index);
      end;

      Shelp:=p;
      p:=p.next;
      Shelp.free;
    end;
  end;

Destructor TLink.Destroy;
  begin
    if List<>NIL then List.Free;
    FrontList.Free;
    BackList.Free;
    inherited Destroy;
  end;


Function TTree.OnCompareItems
   (
   key1    : Pointer;     {Tree}
   key2    : Pointer      {InsertEdge ; Run}
   )
   :Integer;
  var Edge1  : TEdge;
      Edge2  : TEdge;
      vec    : Double;
  begin
    Edge1:=TEdge(key1);
    Edge2:=TEdge(key2);
    vec :=
      (Edge2.next.x - Edge2.x)*
      (Edge1.next.y - Edge1.y)-
      (Edge1.next.x - Edge1.x)*
      (Edge2.next.y - Edge2.y);

    if vec < -eps then Result:=-1
    else
    if vec > eps then  Result:=1
    else
    if abs(vec) < eps then Result:=0;
  end;

Function TTree.Key1Of {Tree}
   (
   Item:Pointer
   )
   : Pointer;
  begin
    Result:=TTreeItem(Item).PtEdge;
  end;

Function TTree.Key2Of  {Edge}
   (
   Item:Pointer
   )
   : Pointer;
  begin
    Result:=TTreeItem(Item).PtEdge;
  end;

Constructor TTreeItem.Create
   (
   key:TEdge
   );
  begin
    PtEdge:=key;
  end;

Procedure TTree.TraverseTree
   (
   p:TTreeBaseItem
   );
  var Edge  : TEdge;
  begin
    if p<>NIL then begin
      TraverseTree(p.left);
      Edge:=TTreeItem(p).PtEdge;
      Edge.Index:=List.Add(Edge);
      TraverseTree(p.Right);
    end;
  end;


Constructor TTree.Create
   (
    AOwner : TArea
   );
  begin
    inherited Create;
    Owner:=AOwner;
    List:=TRecList.Create(isFrontList,sizeof(TEdge));
  end;

Procedure TArea.Intersection;
  var p,q           : TSLBaseItem;
      count       : Integer;
      Index       : Integer;
      Edge        : TEdge;
      Vertex      : TDVertex;
      SItem       : TSegItem;
      SItem1      : TSegItem;
      SItem2      : TSegItem;
      f           : TextFile;
      cnt         : longint;
      cntx        : longint;
      actual      : double;
      actualn     : double;
      whole       : double;
      found       : boolean;
      SameVertex  : Boolean;
      Above       : TSegItem;
      flag        : Integer;
      a           : integer;

  begin
  try
    {$IFNDEF AXDLL}  // <----------- AXDLL
    StatusBar.Progress:=0;
    {$ENDIF} // <----------- AXDLL
    found:=false;
    cnt:=0;
    actual:=0;
    cntx:=0;
    whole:=Nodelist.Count;
    Vertex:=TDVertex.Create;

    p:=NodeList.GetHead;
    while p<>NIL do begin

      actualn:=((100/NodeList.count)*cntx);
      if (actualn-actual)> 1 then begin
        if AllAbort                                         {sl}
        {$IFNDEF AXDLL}  // <----------- AXDLL
            or StatusBar.QueryAbort
        {$ENDIF} // <----------- AXDLL
        then begin
          AllAbort:=TRUE;
          Break;
        end;
        {$IFNDEF AXDLL}  // <----------- AXDLL
        StatusBar.Progress:=round(actualn);
        {$ENDIF} // <----------- AXDLL
        actual:=actualn;
      end;

   {   try
        if abs(Tlink(p).y-45778) < 2 then raise ElistError.Create('position found');
        except begin
        end;
      end;
      }

      count:=0;
      index:=0;
      while count<TLink(p).BackList.count do begin
        if TLink(p).BackList[Index]<>NIL then begin
          Edge:=TEdge(TLink(p).BackList[Index]^);

          if (abs(Edge.x-Edge.next.x)>Eps)or(abs(Edge.y-Edge.next.y)>Eps)then begin
            SItem:=Edge.SegItem;
            if Edge.SegItem<>NIL then begin
              if STree.CheckAboveBelow(p,Vertex,SItem,SItem1,SItem2,flag,q,SameVertex) then begin
                if Data.ISLayer then InsertLineToIntersectionLayer(TEdge(TSegItem(SItem).StartEdge));
                if Data.ISLayer then InsertLineToIntersectionLayer(TEdge(TSegItem(SItem2).StartEdge));
                InsertToNodeList(p,Vertex,SItem1,SItem2,flag,q);
              end;
              STree.Delete(TCTreeBaseItem(SItem));
              Edge.SegItem:=NIL;
              Edge.next.SegItem:=NIL;
              {Stree.Save;
              Stree.Save2;}
            end;
          end;  {Edge>0}

          inc(Count);
        end;
        inc(Index);
      end;

      count:=0;
      index:=0;
      SameVertex:=True;
      while SameVertex do begin
       SameVertex:=False;
        while Index<TLink(p).FrontList.Capacity do begin
          if TLink(p).FrontList[Index]<>NIL then begin
            Edge:=TEdge(TLink(p).FrontList[Index]^);
            SameVertex:=False;
            if (abs(Edge.x-Edge.next.x)>Eps)or(abs(Edge.y-Edge.next.y)>Eps)then begin
              SItem:=TSegItem.Create;
              SItem.StartEdge:=Edge;
              Edge.SegItem:=SItem;
              Edge.next.SegItem:=SItem;
              STree.InsertInTree(SItem);
              {STree.DrawInsertTree(SItem);}
              if SItem<>NIL then begin
                if STree.CheckAbove(p,Vertex,SItem,SItem2,flag,q,SameVertex) then begin
                   if Data.ISLayer then InsertLineToIntersectionLayer(TEdge(TSegItem(SItem).StartEdge));
                   if Data.ISLayer then InsertLineToIntersectionLayer(TEdge(TSegItem(SItem2).StartEdge));
                   InsertToNodeList(p,Vertex,SItem,SItem2,flag,q);
                   if Vertex.x<TLink(p).x then ElistError.Create('Punkt kleiner');
                end;
              end;
              if SItem<>NIL then begin
                if STree.CheckBelow(p,Vertex,SItem,SItem2,flag,q,Samevertex) then begin
                   if Data.ISLayer then InsertLineToIntersectionLayer(TEdge(TSegItem(SItem).StartEdge));
                   if Data.ISLayer then InsertLineToIntersectionLayer(TEdge(TSegItem(SItem2).StartEdge));
                   InsertToNodeList(p,Vertex,SItem,SItem2,flag,q);
                   if Vertex.x<TLink(p).x then ElistError.Create('Punkt kleiner');
                end;
              end;
              if SameVertex then begin
                index:=0;
                if SItem<>NIL then STree.Delete(TCTreeBaseItem(SItem));
                DeleteBackSegItems(Tlink(p));
                break;
              end;
              {inc(Count);}
            end; {Edge>0}

          end;
          inc(Index);
        end;

      end;{while SameVertex}

      {Stree.Save;}
      {STree.Save2;}
      p:=p.next;
      inc(cntx);
      {if Stree.Abort then EXit;}
    end;
  except
    on E: Exception do begin
       DebugMessage:=E.Message;
       AllAbort:=True;
    end;
  end;
end;

Procedure TArea.DeleteBackSegItems
   (
   p : TLink
   );
  var index          : integer;
      count          : integer;
      SItem          : TSegItem;
      Edge           : TEdge;
  begin
    count:=0;
    index:=0;
    while count<TLink(p).BackList.count do begin
      if TLink(p).BackList[Index]<>NIL then begin
        Edge:=TEdge(TLink(p).BackList[Index]^);
        SItem:=Edge.SegItem;
        if Edge.SegItem<>NIL then begin
           STree.Delete(TCTreeBaseItem(SItem));
           Edge.SegItem:=NIL;
           Edge.next.SegItem:=NIL;
         end;
        inc(Count);
      end;
      inc(Index);
    end;
  end;

Function TArea.InsertToNodeList
   (
   Start       : TSLBaseItem;
   Vertex      : TDVertex;
   var SItem1  : TSegItem;
   var SItem2  : TSegItem;
   flag        : integer;
   q           : TSLBaseItem
   )
   : Boolean;
  var p           : TSLBaseItem;
      Startp      : TSLBaseItem;
      Actp        : TSLBaseItem;
      Destp       : TSLBaseItem;
      LimitUpper  : Double;
      LimitLower  : Double;
      r           : double;
      found       : Boolean;
      SameVertex  : boolean;
  begin
    Result:=False;
    SameVertex:=False;
    found:=false;
    r:=Eps;

    if q <> NIL then begin
      InsertAt2(Self,q,SItem1,SItem2,flag);
    end else
    begin
      p:=Start;
      while (p<>NIL)and(TLink(p).x < Vertex.x) do p:=p.next;
      if p<>NIL then begin
        if TLink(p).x > Vertex.x then InsertVertex(Self,p,Vertex,SItem1,SItem2,flag) else
        if TLink(p).x = Vertex.x then begin
          while (TLink(p).x = Vertex.x) and (p<>NIL) and (TLink(p).y <=Vertex.y) do p:=p.next;
          if p<>NIL then begin
            InsertVertex(Self,p,Vertex,SItem1,SItem2,flag);
          end else  raise ElistError.Create('Area; last coordinate');
        end else  raise ElistError.Create('Area; last coordinate');
        {darf aber sowieso nicht vorkommen}
      end;
    end;


(*
    while (p.prev<>NIL)and(TLink(p).x > (Vertex.x - r)) do p:=p.prev;
    while (p<>NIL)and(TLink(p).x < (Vertex.x - r)) do p:=p.next;
    Startp:=p;
    while (p<>NIL)and(TLink(p).x < Vertex.x) do p:=p.next;
    while (p<>NIL)and(TLink(p).x = Vertex.x)and(TLink(p).y < Vertex.y) do p:=p.next;
    if    (p<>NIL)and(TLink(p).x = Vertex.x)and(TLink(p).y = Vertex.y) then found:=TRUE;
    Actp:=p;
    while (p<>NIL)and(TLink(p).x < (Vertex.x + r)) do p:=p.next;
    Destp:=p;

    p:=Startp;
    if (not found) then begin
      while p<>Destp do begin
        if (abs(TLink(p).x - Vertex.x) < r )and(abs(TLink(p).y - Vertex.y) < r) then begin
          found:=TRUE;
          Actp:=p;
          break;
        end;
        p:=p.next;
      end;
    end;

    if found then  begin
      if Actp=Start then begin
        Result:=True;
        SameVertex:=True;
      end;
    {1}
      if (TEdge(SItem1.StartEdge).back=Tlink(Actp))or(TEdge(SItem1.StartEdge).next.back=Tlink(Actp))then
        InsertAt1(Self,Actp,SItem2,SameVertex) else
      if (TEdge(SItem2.STartEdge).back=TLink(Actp))or(TEdge(SItem2.StartEdge).next.back=Tlink(Actp)) then
        InsertAt1(Self,Actp,SItem1,SameVertex) else
     {2}
      if (TEdge(SItem1.StartEdge).back<>TLink(Actp))and(TEDge(SItem1.StartEdge).next.back<>TLink(Actp))and
         (TEdge(SItem2.StartEdge).back<>TLink(Actp))and(TEdge(SItem2.StartEdge).next.back<>Tlink(Actp))then
      InsertAt2(Self,Actp,SItem1,SItem2);
    end else
     {3}
    if not found then InsertVertex(Self,Actp,Vertex,SItem1,SItem2);

  *)



     {**************************************************************************}
    (*
    p:=Start;
    while (p<>NIL)and(TLink(p).x < Vertex.x) do p:=p.next;
    if p<>NIL then begin
      if TLink(p).x > Vertex.x then InsertVertex(Self,p,Vertex,SItem1,SItem2) else
      if TLink(p).x = Vertex.x then begin
        while (TLink(p).x = Vertex.x) and (p<>NIL) and (TLink(p).y < Vertex.y) do p:=p.next;
        if p<>NIL then begin
          if (TLink(p).x = Vertex.x)and(TLink(p).y > Vertex.y) then InsertVertex(Self,p,Vertex,SItem1,SItem2)else
          if (TLink(p).x = Vertex.x)and(TLink(p).y = Vertex.y) then  Insertequal(Self,p,SItem1,SItem2);
          if (TLink(p).x <> Vertex.x)then InsertVertex(Self,p,Vertex,SItem1,SItem2);
        end else  raise ElistError.Create('Area; last coordinate');
      end else  raise ElistError.Create('Area; last coordinate');
      {darf aber sowieso nicht vorkommen}
    end;
    *)
    {**************************************************************************}

   { if Nodelist.Count = 0 then NodeList.Add(Link) else begin
        if Link.x > TLink(NodeList.Tail).x then NodeList.Add(Link) else begin
          prun:=NodeList.Tail;
          while (prun<>NIL) and (Link.x < TLink(prun).x) do prun:=prun.prev;
          if prun=NIL then NodeList.AddHead(Link) else
          if Link.x > TLink(prun).x then NodeList.InsertNext(prun,Link) else
          if Link.x = TLink(prun).x then begin
            while (prun<>NIL) and (Link.x = TLink(prun).x)and (Link.y < TLink(prun).y) do prun:=prun.prev;
            if prun=NIL then NodeList.AddHead(Link) else
            if Link.y> TLink(prun).y then NodeList.InsertNext(prun,Link) else
            if Link.y= TLink(prun).y then raise EListError.Create(' Equal Snap ');
          end;
        end;
      end;  }

(*    while (p<>NIL)do begin
      if TLink(p).x < Vertex.x  then p:=p.next else break;
    end;
    if p<>NIL then begin
      LimitUpper:=Vertex.x+eps;
      LimitLower:=Vertex.x-eps;
      if TLink(p).x < LimitLower then begin
        raise ElistError.Create('Fehler, Area InsertToNodeList');
      end else
      if TLink(p).x > LimitUpper then begin
        {x of Vertex is smaller}
        InsertVertex(Self,p,Vertex,SItem1,SItem2);
      end else begin
        {equal x}
        while (TLink(p).x < LimitUpper) and (TLink(p).x > LimitLower) and (p<>NIL) do begin
          if TLink(p).y  < Vertex.y then p:=p.next else break;
        end;
        if (TLink(p).y -Vertex.y) < -eps then  raise ElistError.Create('Fehler, Area InsertToNodeList')
        else
        if (TLink(p).y -Vertex.y) >  eps then begin
          {eqal x, y of Vertex is smaller}
          InsertVertex(Self,p,Vertex,SItem1,SItem2);
        end
        else begin
          {equal x and y}
          Insertequal(Self,p,SItem1,SItem2);
        end;
      end;
    end
    else  raise ElistError.Create('last coordinate'); {p=NIL}


    if p<>NIL then begin
      {Verify equal or taller}
      if(abs(TLink(p).x-Vertex.x)<eps) then begin
        raise ElistError.Create('equal coordinate');
        Insertequal(Self,p,SItem1,SItem2);
      end else
      if (TLink(p).x > Vertex.x) then begin
        InsertTaller(Self,p,Vertex,SItem1,SItem2);
      end
      else raise ElistError.Create('Error in TArea.InsertTONodeList');
    end else
    begin
       raise ElistError.Create('last coordinate');
      InsertTail(Self,Vertex,SItem1,SItem2);
    end;*)
  end;



Procedure TArea.InsertAt1
   (
   AArea      : TArea;
   p          : TSLBaseItem;
   var SItem  : TSegItem;
   SameVertex : Boolean
   );
  begin
    TLink(p).InsertEdgeSorted(AArea,SItem);
  end;


Procedure TArea.InsertEqual     {equal x,y}
   (
   AArea  : TArea;
   p      : TSLBaseItem;
   SItem1 : TSegItem;
   SItem2 : TSegItem
   );
  begin
    TLink(p).InsertEdgeSorted(AArea,SItem1);
    TLink(p).InsertEdgeSorted(AArea,SItem2);
  end;

Procedure TArea.InsertAt2
   (
   AArea      : TArea;
   p          : TSLBaseItem;
   var SItem1 : TSegItem;
   var SItem2 : TSegItem;
   flag       : Integer
   );
  begin
    if flag=0 then begin
      TLink(p).InsertEdgeSorted(AArea,SItem1);
      TLink(p).InsertEdgeSorted(AArea,SItem2);
    end else
    if flag = 1 then  TLink(p).InsertEdgeSorted(AArea,SItem1) else
    if flag = 2 then  TLink(p).InsertEdgeSorted(AArea,SItem2);
    inc(AArea.NodeList.FCount);
  end;

Procedure TArea.InsertVertex
   (
   AArea       : TArea;
   p           : TSLBaseItem;
   Vertex      : TDVertex;
   var SItem1      : TSegItem;
   var SItem2      : TSegItem;
   flag            : integer
   );
  var Node : TLink;
  begin
    Node:=TLink.Create;
    Node.x:=Vertex.x;
    Node.y:=Vertex.y;
    Node.prev:=p.prev;
    Node.next:=p;
    if p.prev<>NIL then p.prev.next:=Node;
    p.prev:=Node;
    if Node.prev=NIL then NodeList.Head:=Node;
    if flag=0 then begin
      Node.InsertEdgeSorted(AArea,SItem1);
      Node.InsertEdgeSorted(AArea,SItem2);
    end else
    if flag = 1 then  Node.InsertEdgeSorted(AArea,SItem1) else
    if flag = 2 then  Node.InsertEdgeSorted(AArea,SItem2);

    inc(AArea.NodeList.FCount);
    {printnode(node);}
  end;

Procedure TArea.InsertTail
   (
   AArea     : TArea;
   Vertex    : TDVertex;
   SItem1    : TSegItem;
   SItem2    : TSegItem
   );
  var Node : TLink;
  begin
    Node:=TLink.Create;
    Node.x:=Vertex.x;
    Node.y:=Vertex.y;
    Node.prev:=NodeList.GetTail;
    NodeList.GetTail.next:=Node;
    NodeLIst.Tail:=Node;
    Node.InsertEdgeSorted(AArea,SItem1);
    Node.InsertEdgeSorted(AArea,SItem2);
  end;


Procedure TLink.InsertEdgeSorted
   (
   AArea:TArea;
   var SItem:TSegItem
   );
   var EdgeBack  : TEdge;
       EdgeFront : TEdge;
       h         : Boolean;
       Link      : TLink;
  begin
    EdgeBack:=TEdge.Create;
    EdgeBack.x:=X;
    EdgeBack.y:=Y;
    EdgeFront:=TEdge.Create;
    EdgeFront.x:=X;
    EdgeFront.y:=Y;

    EdgeBack.next:=TEdge(SItem.StartEdge);
    EdgeFront.next:=TEdge(SItem.StartEdge).next;
    EdgeBack.next.next:=EdgeBack;
    EdgeFront.next.next:=EdgeFront;
    EdgeBack.SegItem:=SItem;
    h:=InsertBackEdge(AArea,EdgeBack,NIL,NIL);
    if not h then begin
      Link:=EdgeBack.next.back;
      Link.FrontList.Delete(EdgeBack.next.index);
      if EdgeBack.SegItem<>NIL then AArea.STree.Delete(TCTreeBaseItem(EdgeBack.SegItem));
      SItem:=NIL;
    end;
    h:=InsertFrontEdge(AArea,EdgeFront,NIL,NIL);
    if not h then begin
      Link:=EdgeFront.next.back;
      Link.BackList.Delete(EdgeFront.next.index);
      if EdgeFront.SegItem<>NIL then AArea.STree.Delete(TCTreeBaseItem(EdgeFront.SegItem));
      SItem:=NIL;
    end;
  end;


Procedure TLink.InsertEdgeSorted2
   (
   AArea     : TArea;
   Vertex    : TDVertex;
   Edge      : TEdge;
   var Above : TSegItem
   );
   var EdgeBack  : TEdge;
       EdgeFront : TEdge;
       h         : Boolean;
       Link      : TLink;
  begin
    EdgeBack:=TEdge.Create;
    EdgeBack.x:=X;
    EdgeBack.y:=Y;
    EdgeFront:=TEdge.Create;
    EdgeFront.x:=X;
    EdgeFront.y:=Y;
    EdgeBack.next:=TEdge(Above.StartEdge);
    EdgeFront.next:=TEdge(Above.StartEdge).next;
    EdgeBack.next.next:=EdgeBack;
    EdgeFront.next.next:=EdgeFront;
    EdgeBack.SegItem:=Above;

    h:=InsertBackEdge(AArea,EdgeBack,NIL,NIL);
    if not h then begin
      Link:=EdgeBack.next.back;
      Link.FrontList.Delete(EdgeBack.next.index);
      if EdgeBack.SegItem<>NIL then AArea.STree.Delete(TCTreeBaseItem(EdgeBack.SegItem));
      Above:=NIL; {Troubles possible}
    end;
    h:=InsertFrontEdge(AArea,EdgeFront,NIL,NIL);
    if not h then begin
      Link:=EdgeFront.next.back;
      Link.BackList.Delete(EdgeFront.next.index);
      {
      if EdgeFront.SegItem<>NIL then AArea.STree.Delete(TCTreeBaseItem(EdgeFront.SegItem));
      Above:=NIL;}
    end;
  end;

Function TLink.DoesEdgeExist(AEdge:TEdge;AMList:TReclist;Redoedge,Redoedgenext:TEdge):Boolean;
  var Edge1 : TEdge;
      EdgeNext : TEdge;
      cnt   : integer;
      Index : Integer;
  begin
    cnt:=0;
    Index:=0;
    Result:=false;
    while cnt < AMList.count do begin
      if AMList[Index]<>NIL then begin
        Edge1:=TEdge(AMList[Index]^);
        if (AEdge.x=Edge1.x)and(AEdge.y=Edge1.y)and(AEdge.next.x=Edge1.next.x)and(AEdge.next.y=Edge1.next.y) then begin
          if (RedoEdge<>NIL)and(RedoEdgeNext<>Nil) then begin
            RedoEdge.next:=RedoEdgeNext;
          end;
          Result:=true;
        end;
        inc(cnt);
      end;
      inc(Index);
    end;

  end;

Function TLink.InsertFrontEdge
   (
    AArea     : TArea;
    AEdge     : TEdge;
    RedoEdge,Redoedgenext : TEdge
   )
   :Boolean;
  var acheck : Boolean;
  begin
    Result:=True;
    {
    if (AEdge.x > AEdge.next.x) then raise ElistError.Create('TLink.insertFrontEdge') else
    if (AEdge.x = AEdge.next.y)and(AEdge.y > AEdge.next.y)then raise ElistError.Create('TLink.insertFrontEdge');
    }
    acheck:=DoesEdgeExist(AEdge,FrontList,Redoedge,Redoedgenext);
    if not acheck then AEdge.Index:=FrontList.Add(AEdge);
    AEdge.Back:=Self;
    AEdge.v:=2;
    if FrontList.Count > 0 then Result:=AArea.TraverseList(FrontList);
  end;

Function TLink.InsertBackEdge
   (
    AArea     : TArea;
    AEdge     : TEdge;
    RedoEdge,RedoEdgeNext: TEdge
   )
   :Boolean;
  var acheck : Boolean;
  begin
    {
    if (AEdge.x < AEdge.next.x) then raise ElistError.Create('TLink.insertBAckEdge') else
    if (AEdge.x = AEdge.next.y)and(AEdge.y < AEdge.next.y)then raise ElistError.Create('TLink.insertBAckEdge');
    }
    acheck:=DoesEdgeExist(AEdge,FrontList,RedoEdge,RedoEdgeNext);
    if not acheck then AEdge.Index:=BackList.Add(AEdge);
    AEdge.Back:=Self;
    AEdge.v:=1;
    if BackList.Count > 0 then Result:=AArea.TraverseList(BackList);
  end;

Procedure TLink.RestoreBackList;
  var hList : TRecList;
      cnt   : Integer;
      Index : Integer;
      AEdge : TEdge;
  begin
    hList:=TRecList.Create(BackList.Count,BackList.ItemSize);
    cnt:=0;
    Index:=0;
    while cnt < BackList.count do begin
      if BackList[Index]<>NIL then begin
        AEdge:=TEdge(BAckList[Index]^);
        AEdge.Index:=hList.Add(AEdge);
        AEdge.Back:=Self;
        AEdge.v:=1;
        inc(cnt);
      end;
      inc(Index);
    end;
    BackList.Free;
    BAckList:=hList;
  end;

Procedure TLink.RestoreFrontList;
  var hList : TRecList;
      cnt   : Integer;
      Index : Integer;
      AEdge : TEdge;
  begin
    hList:=TRecList.Create(FrontList.Count,FrontList.ItemSize);
    cnt:=0;
    Index:=0;
    while cnt < FrontList.count do begin
      if FrontList[Index]<>NIL then begin
        AEdge:=TEdge(FrontList[Index]^);
        AEdge.Index:=hList.Add(AEdge);
        AEdge.Back:=Self;
        AEdge.v:=2;
        inc(cnt);
      end;
      inc(Index);
    end;
    FrontList.Free;
    FrontList:=hList;
  end;

Function TLink.OnCompareEdges
   (
   Item1   : TEdge;
   Item2   : TEdge               {Key}
   )
   : Integer;
  var vec: Double;
  begin
    vec:=
      (Item1.next.x  - Item1.x)*
      (Item2.next.y  - Item2.y)-
      (Item2.next.x  - Item2.x)*
      (Item1.next.y  - Item1.y);

    if vec < -eps then Result:=1
    else
    if vec > eps then  Result:=-1
    else
    if abs(vec) < eps then raise ElistError.Create('Double Segment in TLink.OnCompare');
  end;


  Function TLink.GetCount : Word;
    begin
      Result:=FrontList.Count + BackList.Count;
    end;

  Function TLink.SearchList
     (
     Edge  : TEdge;
     Index : Integer
     )
     :TRecList;
    var found  : boolean;
        AIndex : longint;
    begin
      found:=false;
      AIndex:=Index;
      if BackList.Available(AIndex) then begin
        if TEdge(Backlist[AIndex]^)=Edge then begin
          found:= True;
          Result:=BackList;
        end;
      end;
      if not found then begin
        AIndex:=Index;
        if FrontList.Available(AIndex) then begin
          if TEdge(Frontlist[AIndex]^)=Edge then begin
            found:=TRue;
            Result:=FrontList;
          end;
        end;
      end;
      if not found then raise ElistError.Create('Error in Tlink.SearchList');
    end;

  Procedure TArea.SetNil
     (
     Edge:TEdge
     );
    var Link  :TLink;
        hEdge :TEdge;
        List  :TRecList;
        index :integer;
        cnt   :integer;
    begin
    {*********************** Edge *********************************************}
      Link:=Edge.back;

      List:=Link.FrontList;
      index:=0;
      cnt:=0;
      while cnt<List.Count do begin
      if List[index]<>NIL then begin
        hEdge:=TEDge(list[index]^);
        if hEdge.link=EDge then begin
          if Link.MCount=2 then hEDge.link:=NIL else
          if Link.MCount>2 then hEdge.link:=Edge.Link;
        end;
        inc(cnt);
      end;
      inc(index);
      end;

      List:=Link.BackList;
      index:=0;
      cnt:=0;
      while cnt<List.Count do begin
      if List[index]<>NIL then begin
        hEdge:=TEDge(list[index]^);
        if hEdge.link=EDge then begin
          if Link.MCount=2 then hEDge.link:=NIL else
          if Link.MCount>2 then hEdge.link:=Edge.Link;
        end;
        inc(cnt);
      end;
      inc(index);
      end;
      {*********************** Edge.next **************************************}
      Link:=Edge.next.back;

      List:=Link.FrontList;
      index:=0;
      cnt:=0;
      while cnt<List.Count do begin
      if List[index]<>NIL then begin
        hEdge:=TEDge(list[index]^);
        {if hEdge.link=EDge.next then hEDge.link:=NIL;}
        if hEdge.link=Edge.next then begin
          if Link.MCount=2 then hEdge.link:=Nil else
          if Link.MCount>2 then hEDge.link:=EDge.next.link;
        end;
        inc(cnt);
      end;
      inc(index);
      end;

      List:=Link.BackList;
      index:=0;
      cnt:=0;
      while cnt<List.Count do begin
      if List[index]<>NIL then begin
        hEdge:=TEDge(list[index]^);
        {if hEdge.link=EDge.next then hEDge.link:=NIL;}
        if hEdge.link=Edge.next then begin
          if Link.MCount=2 then hEdge.link:=Nil else
          if Link.MCount>2 then hEDge.link:=EDge.next.link;
        end;
        inc(cnt);
      end;
      inc(index);
      end;
    end;

Procedure TArea.ProjectSize
   (
   var ClipRect : TClipRect
   );
  var p          : TSLBaseItem;
      x          : double;
      y          : double;
      xmin       : double;
      xmax       : double;
      ymin       : double;
      ymax       : double;
  begin
    p:=FEdgeList.GetHead;
    xmin:=TEdge(TEdgelistItem(p).PtEdge).x;
    ymin:=TEdge(TEdgelistItem(p).PtEdge).y;
    xmax:=TEdge(TEdgelistItem(p).PtEdge).x;
    ymax:=TEdge(TEdgelistItem(p).PtEdge).y;
    while p<>NIL do begin
      x:=TEdge(TEdgelistItem(p).PtEdge).x;
      y:=TEdge(TEdgelistItem(p).PtEdge).y;
      if xmin<x then xmin:=x;
      if xmax>x then xmax:=x;
      if ymin<y then ymin:=y;
      if ymax>y then ymax:=y;
      p:=p.next;
    end;
    ClipRect.left:=xmin;
    ClipRect.right:=xmax;
    ClipRect.Top:=ymax;
    ClipRect.Bottom:=ymin;
  end;

Procedure TArea.AddFrame
   (
   ClipRect : TClipRect
   );
  var Vertex1  : TVertex;
      Vertex2  : TVertex;
      Offs     : Double;
  begin
    {Offs:=-1;}
    Offs:=0;
    Vertex1.x := ClipRect.left+Offs;      {top Line}
    Vertex1.y := ClipRect.Top-Offs;
    Vertex2.x := ClipRect.Right-Offs;
    Vertex2.y := ClipRect.Top-Offs;
    AddFrameEdge(Vertex1,Vertex2);

    Vertex1.x := ClipRect.left+Offs;       {Bottom Line}
    Vertex1.y := ClipRect.Bottom+Offs;
    Vertex2.x := ClipRect.Right-Offs;
    Vertex2.y := ClipRect.Bottom+Offs;
    AddFrameEdge(Vertex1,Vertex2);

    Vertex1.x := ClipRect.left+Offs;       {left Line}
    Vertex1.y := ClipRect.top-Offs;
    Vertex2.x := ClipRect.left+Offs;
    Vertex2.y := ClipRect.Bottom+Offs;
    AddFrameEdge(Vertex1,Vertex2);

    Vertex1.x := ClipRect.right-Offs;       {right Line}
    Vertex1.y := ClipRect.top-Offs;
    Vertex2.x := ClipRect.right-Offs;
    Vertex2.y := ClipRect.Bottom+Offs;
    AddFrameEdge(Vertex1,Vertex2);
  end;

Procedure TArea.AddFrameEdge
   (
    Start  : TVertex;
    Ende   : TVertex
   );
  var Edge          : TEdge;
      hEdge         : TEdge;
      EdgeListItem  : TEdgeListItem;
  begin
    EdgeListItem:=TEdgeListItem.Create(NIL);
    Edge:=TEdge(EdgeListItem.PtEdge);
    System.Move(Start.x,Edge.x,16);
    EdgeList.Add(EdgeListItem);

    EdgeListItem:=TEdgeListItem.Create(NIL);
    hEdge:=TEdge(EdgeListItem.PtEdge);
    System.Move(Ende.x,hEdge.x,16);
    EdgeList.Add(EdgeListItem);

    Edge.next:=hEdge;
    hEdge.next:=Edge;
  end;


Procedure TArea.InsertLineToErrorLayer
   (
   Edge   : TEdge
   );
  var APoint   : PDPoint;
      AObject  : PPoly;
      ALayer   : PLayer;
  begin
    APoint:=new(PDPoint);
    AObject:=new(PPoly,Init);
    APoint^.Init(round(Edge.x),round(Edge.y));
    AObject^.InsertPoint(APOint^);
    APoint^.Init(round(Edge.next.x),round(Edge.next.y));
    AObject^.InsertPoint(APoint^);
    if not PProj(Proj)^.InsertObjectonLayer(AObject,Data.ErrorLayer) then  {sl}
        AllAbort:=TRUE;
    Dispose(APoint,Done);
  end;

Procedure TArea.InsertLineToIntersectionLayer
   (
   Edge   : TEdge
   );
  var APoint   : PDPoint;
      AObject  : PPoly;
      ALayer   : PLayer;
  begin
    APoint:=new(PDPoint);
    AObject:=new(PPoly,Init);
    APoint^.Init(round(Edge.x),round(Edge.y));
    AObject^.InsertPoint(APOint^);
    APoint^.Init(round(Edge.next.x),round(Edge.next.y));
    AObject^.InsertPoint(APoint^);
    if not PProj(Proj)^.InsertObjectonLayer(AObject,Data.IntersectLayer) then  {sl}
        AllAbort:=TRUE;
    Dispose(APoint,Done);
  end;

Procedure TArea.DrawList;
  var MLayer   : PLayer;
      cnt      : longint;
      go       : TEdge;
      APoint   : PDPoint;
      AObject  : PPoly;
  begin
    MLayer:=PProj(Proj)^.Layers^.NameToLayer('DrawLine');
    if MLayer=NIL then MLayer:=PProj(Proj)^.Layers^.InsertLayer('DrawLine',
      $00FF0000,lt_Solid,$00FF0000,pt_Solid,0,DefaultSymbolFill); {0=ilTop}
    Data.DrawLayer:=MLayer^.Index;
    cnt:=0;
    APoint:=new(PDPoint);
    AObject:=new(PPoly,Init);
    while cnt<FDrawList.count do begin
      go:=TEDge(FDrawList[cnt]);
      APoint^.Init(round(go.x),round(go.y));
      AObject^.InsertPoint(APOint^);
      if cnt<1000 then inc(cnt) else break;
    end;
    if not PProj(Proj)^.InsertObjectonLayer(AObject,Data.DrawLayer) then {sl}
        AllAbort:=TRUE;
    Dispose(APoint,Done);
  end;


Procedure TArea.DeleteLayer
   (
   Index : Longint
   );
  var ALayer       : PLayer;
      BIndex       : TIndex;
      AIndex       : PIndex;
      AObject      : PPoly;
      Rect         : TDRect;
  begin
    ALayer:=PProj(Proj)^.Layers^.IndexToLayer(Index);
    while ALayer^.Data^.GetCount>0 do begin
      AIndex:=ALayer^.Data^.At(0);
      AObject:=Pointer(PLayer(PProj(Proj)^.PInfo^.Objects)^.IndexObject(PProj(Proj)^.PInfo,AIndex));

      BIndex.Index:=AIndex^.Index;
      PLayer(PProj(Proj)^.PInfo^.Objects)^.DeleteIndex(PProj(Proj)^.PInfo,@BIndex);
      PProj(Proj)^.Layers^.DeleteIndex(PProj(Proj)^.PInfo,@BIndex);
      {PProj(Proj)^.PInfo^.RedrawRect(Rect);}
    end;
    PProj(Proj)^.PInfo^.RedrawScreen(TRUE);
  end;


Procedure TArea.DrawLines3;
  var APoint      : PDPoint;
      AObject     : PPoly;
      index       : integer;
      count       : integer;
      cnt3        : integer;
      Edge        : TEdge;
      List        : TRecList;
      p           : TSLBaseItem;
      ALayer      : PLayer;
      FirstLayer  : longint;

  begin
    FirstLayer:= longint(Data.Layers^.At(0));
    for Cnt3:=0 to Data.Layers^.Count-1 do begin
      DeleteLayer(LongInt(Data.Layers^.At(Cnt3)));
    end;
    p:=NodeList.GetHead;
    APoint:=new(PDPoint);
    while p<>NIL do begin
      count:=0;
      index:=0;
      List:=TLink(p).FrontList;
      while count<List.count do begin
        if List[Index]<>NIL then begin
          Edge:=TEdge(List[Index]^);

          AObject:=new(PPoly,Init);
          APoint^.Init(round(Edge.x),round(Edge.y));
          AObject^.InsertPoint(APOint^);
          APoint^.Init(round(Edge.next.x),round(Edge.next.y));
          AObject^.InsertPoint(APoint^);
          if not PProj(Proj)^.InsertObjectonLayer(AObject,FirstLayer) then {sl}
              AllAbort:=TRUE;

          inc(Count);
        end;
        inc(Index);
      end;
      p:=p.next;
    end;
    Dispose(APoint,Done);
  end;

 Function TTree.ResultZero
   (
   p           : TTreeBaseITem;
   k           : TTreeBaseItem
   )
   :Boolean;
  var key      : TEdge;
      Etree    : TEdge;
      New1     : TEdge;
      Edge1    : TEdge;
      Link     : TLink;
      dtreex   : double;
      dtreey   : double;
      dkeyx    : double;
      dkeyy    : double;
      found    : Boolean;
      RedoEdge, Redoedgenext : TEdge;
      EqualPos : Boolean;
  begin
    found:=FALSE;
    key :=TEdge(TTreeItem(k).PtEdge);
    Etree:=TEdge(TTreeItem(p).PtEdge);

    dkeyx:=abs(key.next.x-key.x);
    dkeyy:=abs(key.next.y-key.y);
    dtreex:=abs(etree.next.x-etree.x);
    dtreey:=abs(etree.next.y-etree.y);

    EqualPos:=False;

    if (Owner.LastTreePos.X = Trunc(ETree.x)) and (Owner.LastTreePos.Y = Trunc(ETree.y)) then begin
       EqualPos:=True;
    end;

    Owner.LastTreePos.Init(Trunc(ETree.x),Trunc(Etree.y));

    if not EqualPos then begin
 //    if Etree.x <> Etree.next.x then begin
      {A}
      if (dtreex > dkeyx)or(dtreey > dkeyy) then begin
        found:=TRUE;
        New1:=TEdge.Create;
        New1.x:=key.next.x;
        New1.y:=key.next.y;
        Edge1:=Etree.next;
        New1.next:=Edge1;
        RedoEdge:=Edge1;
        RedoEdgenext:=Edge1.next;
        Edge1.next:=New1;

        Link:=key.next.back;
        if (key.next.x > key.x) then Link.InsertFrontEdge(Owner,New1,RedoEdge,RedoEdgeNext)else
        if (key.next.x < key.x) then Link.InsertBackEdge(Owner,New1,RedoEdge,RedoEdgeNext) else
        if (key.next.x = key.x) then begin
          if (key.next.y>key.y )then Link.InsertFrontEdge(Owner,New1,RedoEdge,RedoEdgeNext)else
          Link.InsertBackEdge(Owner,New1,NIL,NIL);
        end;

        TEdge(TTreeItem(p).PtEdge):=key;
      end else
      if (dtreex < dkeyx)or(dtreey < dkeyy) then begin
        found:=TRUE;
        New1:=TEdge.Create;
        New1.x:=Etree.next.x;
        New1.y:=Etree.next.y;
        Edge1:=key.next;
        New1.next:=Edge1;
        RedoEdge:=Edge1;
        RedoEdgenext:=Edge1.next;
        Edge1.next:=New1;

        Link:=Etree.next.back;
        if (key.next.x > key.x) then Link.InsertFrontEdge(Owner,New1,RedoEdge,RedoEdgeNext)else
        if (key.next.x < key.x) then Link.InsertBackEdge(Owner,New1,RedoEdge,RedoEdgeNext) else
        if (key.next.x = key.x) then begin
          if (key.next.y>key.y) then Link.InsertFrontEdge(Owner,New1,RedoEdge,RedoEdgeNext)else
          Link.InsertBackEdge(Owner,New1,NIL,NIL);
        end;
      end;
    end;
    Result:=found;
  end;

procedure TArea.Intersection2;
  var p,q           : TSLBaseItem;
      count       : Integer;
      Index       : Integer;
      Edge        : TEdge;
      Vertex      : TDVertex;
      SItem       : TSegItem;
      SItem1      : TSegItem;
      SItem2      : TSegItem;
      f           : TextFile;
      cnt         : longint;
      cntx        : longint;
      actual      : double;
      actualn     : double;
      whole       : double;
      found       : boolean;
      SameVertex  : Boolean;
      Above       : TSegItem;
      flag        : Integer;
      a           : integer;

begin
  try
    {$IFNDEF AXDLL}  // <----------- AXDLL
    StatusBar.Progress:=0;
    {$ENDIF} // <----------- AXDLL
    found:=false;
    cnt:=0;
    actual:=0;
    cntx:=0;
    whole:=Nodelist.Count;
    Vertex:=TDVertex.Create;

    p:=NodeList.GetHead;
    while p<>NIL do begin

      actualn:=((100/NodeList.count)*cntx);
      if (actualn-actual)> 1 then begin
        if AllAbort                                         {sl}
        {$IFNDEF AXDLL}  // <----------- AXDLL
            or StatusBar.QueryAbort
        {$ENDIF} // <----------- AXDLL
        then begin
          AllAbort:=TRUE;
          Break;
        end;
        {$IFNDEF AXDLL}  // <----------- AXDLL
        StatusBar.Progress:=round(actualn);
        {$ENDIF} // <----------- AXDLL
        actual:=actualn;
      end;

   {   try
        if abs(Tlink(p).y-45778) < 2 then raise ElistError.Create('position found');
        except begin
        end;
      end;
      }

      count:=0;
      index:=0;
      while count<TLink(p).BackList.count do begin
        if TLink(p).BackList[Index]<>NIL then begin
          Edge:=TEdge(TLink(p).BackList[Index]^);

          if (abs(Edge.x-Edge.next.x)>Eps)or(abs(Edge.y-Edge.next.y)>Eps)then begin
            SItem:=Edge.SegItem;
            if Edge.SegItem<>NIL then begin
              if STree.CheckAboveBelow(p,Vertex,SItem,SItem1,SItem2,flag,q,SameVertex) then begin
                if Data.ISLayer then InsertLineToIntersectionLayer(TEdge(TSegItem(SItem).StartEdge));
                if Data.ISLayer then InsertLineToIntersectionLayer(TEdge(TSegItem(SItem2).StartEdge));
                InsertToNodeList(p,Vertex,SItem1,SItem2,flag,q);
              end;
              STree.Delete(TCTreeBaseItem(SItem));
              Edge.SegItem:=NIL;
              Edge.next.SegItem:=NIL;
              {Stree.Save;
              Stree.Save2;}
            end;
          end;  {Edge>0}

          inc(Count);
        end;
        inc(Index);
      end;

      count:=0;
      index:=0;
      SameVertex:=True;
      while SameVertex do begin
       SameVertex:=False;
        while Index<TLink(p).FrontList.Capacity do begin
          if TLink(p).FrontList[Index]<>NIL then begin
            Edge:=TEdge(TLink(p).FrontList[Index]^);
            SameVertex:=False;
            if (abs(Edge.x-Edge.next.x)>Eps)or(abs(Edge.y-Edge.next.y)>Eps)then begin
              SItem:=TSegItem.Create;
              SItem.StartEdge:=Edge;
              Edge.SegItem:=SItem;
              Edge.next.SegItem:=SItem;
              STree.InsertInTree(SItem);
              {STree.DrawInsertTree(SItem);}
              if SItem<>NIL then begin
                if STree.CheckAbove(p,Vertex,SItem,SItem2,flag,q,SameVertex) then begin
                   if Data.ISLayer then InsertLineToIntersectionLayer(TEdge(TSegItem(SItem).StartEdge));
                   if Data.ISLayer then InsertLineToIntersectionLayer(TEdge(TSegItem(SItem2).StartEdge));

                   InsertToNodeList(p,Vertex,SItem,SItem2,flag,q);
                   InsertToProject(Vertex);
                end;
              end;
              if SItem<>NIL then begin
                if STree.CheckBelow(p,Vertex,SItem,SItem2,flag,q,Samevertex) then begin
                   if Data.ISLayer then InsertLineToIntersectionLayer(TEdge(TSegItem(SItem).StartEdge));
                   if Data.ISLayer then InsertLineToIntersectionLayer(TEdge(TSegItem(SItem2).StartEdge));

                   InsertToNodeList(p,Vertex,SItem,SItem2,flag,q);
                   InsertToProject(Vertex);
                end;
              end;
              if SameVertex then begin
                index:=0;
                if SItem<>NIL then STree.Delete(TCTreeBaseItem(SItem));
                DeleteBackSegItems(Tlink(p));
                break;
              end;
              {inc(Count);}
            end; {Edge>0}

          end;
          inc(Index);
        end;

      end;{while SameVertex}

      {Stree.Save;}
      {STree.Save2;}
      p:=p.next;
      inc(cntx);
      {if Stree.Abort then EXit;}
    end;
  except

  end;
end;

function TArea.InsertToProject(Vertex: TDVertex): Boolean;
  var APoint   : PDPoint;
      AObject  : PPixel;
  begin
    APoint:=new(PDPoint,Init(10,10));
    try
       APoint^.Init(round(Vertex.x),round(Vertex.y));
       AObject:=New(PPixel,Init(APoint^));
       if not PProj(Proj)^.InsertObjectonLayer(AObject,Data.DestLayer) then begin
          AllAbort:=TRUE;
       end;
    finally
       Dispose(APoint,Done);
    end;
  end;

end.

