Unit FMSelDlg;

Interface

Uses Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,StdCtrls,
     Validate,Spinbtn,WCtrls,Combobtn,DipGrid,Menus,SymLib,AM_Obj,AM_Font,ExtLib,
     AM_Group,AM_Paint,AM_Proj,AM_Def,MultiLng,ExtCtrls, Buttons, ActnList,
     ImgList, ProjStyle, PropCollect, XLines, NumTools, CommonResources, XStyles,
     StyleDef;

Type TFMSelDialog = class(TWForm)
       LibraryCombo:TComboBox;
       SymbolList:TDipList;
       Label1:TWLabel;
       MlgSection:TMlgSection;
       OKBtn:TButton;
       CancelBtn:TButton;
       Bevel1:TBevel;
    ImageList: TImageList;
    DipList: TDipList;
    MlgSection1: TMlgSection;
       Procedure SymbolListDrawCell(Sender:TObject; Canvas:TCanvas;
         Index:Integer; Rect:TRect);
       Procedure SymbolListClick(Sender:TObject);
       Procedure LibraryComboClick(Sender:TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SymbolListMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure DipListMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure DipListDrawCell(Sender: TObject; Canvas: TCanvas;
      Index: Integer; Rect: TRect);
    procedure FormDestroy(Sender: TObject);
      Private
       FLastIndex       : Integer;
       FProject         : pProj;
       FSymInfo         : TSymLibInfo;
       FSymbols         : pSymbols;
       FFonts           : pFonts;
       FExtLib          : pExtLib;
       FExternLibs      : TList;
       PInfo            : PPaint;

       FOnSelectStyle   : TNotifyEvent;
       FProjStyles      : TProjectStyles;
       FStyle           : Integer;
       FStyleOptions    : TSelectOptionList;
       FXLineList       : TStringList;

       Procedure   UpdateLibChange;
       Procedure   UpdateSymChange;

       Procedure   DoSelectStyle;
       Procedure   UpdateStylesList;

      Public
       Function    GetSelSym:pSGroup;
       Property    Project:PProj read FProject write FProject;
     end;

Function GetSymbol(aParent:TComponent;aProj:pProj):pSGroup;

var FMSelDialog: TFMSelDialog = nil;

implementation

{$R *.DFM}

uses AM_Child;

Const SystemLineStyles  = 5;

Function TFMSelDialog.GetSelSym:pSGroup;
begin
  {if ModalResult=mrOk then }GetSelSym:=FSymInfo.Sym;
//                      else GetSelSym:=nil;
end;

Function GetSymbol(aParent:TComponent;aProj:pProj):pSGroup;
var SymSelDlg   :TFMSelDialog;
    SymToReturn :pSGroup;
begin
  SymToReturn:=NIL;
  SymSelDlg:=TFMSelDialog.Create(aParent);
  SymSelDlg.Project:=aProj;
  try
    SymSelDlg.ShowModal;
    SymToReturn:=SymSelDlg.GetSelSym;
  finally
    SymSelDlg.Free;
  end;
  GetSymbol:=SymToReturn;
end;

Procedure TFMSelDialog.SymbolListDrawCell(Sender:TObject; Canvas:TCanvas; Index:Integer; Rect:TRect);
var
aSym: PSGroup;
begin
  if FSymInfo.SymCount<Index then Exit;
  aSym:=FSymInfo.GetSym(Index);
  Canvas.FillRect(Rect);
  Canvas.Pen.Color:=clBlack;
  if aSym<>nil then begin
    PInfo^.ExtCanvas.Handle:=Canvas.Handle;
    aSym^.DrawSymbolInRect(PInfo,Rect);
  end;
end;

Procedure TFMSelDialog.SymbolListClick(Sender:TObject);
begin
  FSymInfo.SymIndex:=SymbolList.ItemIndex;
  Project^.ActualSym:=FSymInfo.Sym;
end;

Procedure TFMSelDialog.LibraryComboClick(Sender:TObject);
begin
  FSymInfo.LibIndex:=LibraryCombo.ItemIndex;
  UpdateLibChange;
  Project^.ActSymLib:=FSymInfo.LibName;
end;

procedure TFMSelDialog.FormCreate(Sender: TObject);
begin
  FExternLibs:=TList.Create;
  // setup pinfo
  PInfo:=New(PPaint,Init);
  PInfo^.ExtCanvas.Cached:=FALSE;
  Dispose(PInfo^.Fonts,Done);


  FProjStyles:=TProjectStyles.Create;

  FStyleOptions:=TSelectOptionList.Create();
  SetupSelectOptions(FStyleOptions,sotLineStyle,[]);
  FXLineList:=TStringList.Create;
  FXLineList.Sorted:=TRUE;
  FXLineList.Duplicates:=dupAccept;

end;

procedure TFMSelDialog.FormShow(Sender: TObject);
var RealProj       : PProj;
    SymParent      : TMDIChild;
    AHeight        : Integer;
    XLineStyle     : TXLineStyle;
begin
  RealProj:=Project;
  if (Project^.SymbolMode=sym_SymbolMode) then begin
    SymParent:=TMDIChild(Project^.SymEditInfo.SymParent);
    if (SymParent<>nil) then RealProj:=SymParent.Data;
  end;
  FSymbols:=PSymbols(RealProj^.PInfo^.Symbols);
  FFonts:=RealProj^.PInfo^.Fonts;
  PInfo^.Fonts:=FFonts;
  FExtLib:=nil;
  FSymInfo:=TSymLibInfo.Create(RealProj,FSymbols,MlgSection[17]);
  FSymInfo.CopyToListBox(LibraryCombo);
  if (Project^.ActSymLib='') then FSymInfo.LibIndex:=0
  else FSymInfo.LibIndex:=LibraryCombo.Items.IndexOf(Project^.ActSymLib);
  FSymInfo.SymIndex:=FSymInfo.IndexOf(Project^.ActualSym);
  SymbolList.ItemIndex:=FSymInfo.IndexOf(Project^.ActualSym);
  UpdateLibChange;


  UpdateStylesList;
  with DipList do begin
    Count:=FXLineList.Count{+SystemLineStyles};
    HeaderRows:=FStyleOptions.Count;
    AHeight:=Count Div ColCount+HeaderRows;
    if Count Mod ColCount<>0 then Inc(AHeight);
    if AHeight>8 then AHeight:=8;
//    Height:=AHeight*CellHeight;
    if FStyle<0 then ItemIndex:=-FStyleOptions.IndexOfValue(FStyle)-1
    else if FStyle<lt_UserDefined then ItemIndex:=FStyle
    else begin
      XLineStyle:=FProjStyles.XLineStyles.NumberedStyles[FStyle];
      if XLineStyle=NIL then ItemIndex:=0
      else ItemIndex:=FXLineList.IndexOfObject(XLineStyle)+SystemLineStyles;
    end;
  end;
//  ClientHeight:=DipList.Height+2;

end;

Procedure TFMSelDialog.UpdateLibChange;
begin
  if Project=NIL then Exit;
  SymbolList.Count:=FSymInfo.SymCount;
  SymbolList.ItemIndex:=FSymInfo.SymIndex;
  LibraryCombo.ItemIndex:=FSymInfo.LibIndex;
  SymbolList.RePaint;
  UpdateSymChange;
end;

Procedure TFMSelDialog.UpdateSymChange;
var aSym  : pSGroup;
begin
  if Project<>NIL then Exit;
  aSym:=FSymInfo.Sym;
  if (Project^.SymbolMode<>sym_SymbolMode) then begin
    Project^.ActualSym:=aSym;
    Project^.ActSymLib:=FSymInfo.LibName
  end
  else begin
    Project^.ActualSym:=nil;
    Project^.ActSymLib:='';
  end;
end;

procedure TFMSelDialog.SymbolListMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var Index          : Integer;
begin
  Index:=SymbolList.ItemAtPos(Point(X,Y),True);
  if Index<>FLastIndex then begin
    Application.CancelHint;
    FLastIndex:=Index;
    if Index<0 then SymbolList.Hint:=''
    else SymbolList.Hint:=PToStr(FSymInfo.GetSym(Index)^.Name);
  end;
end;

procedure TFMSelDialog.DipListMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var Index          : Integer;
begin
  Index:=DipList.ItemAtPos(Point(X,Y),TRUE);
  if Index>=-DipList.HeaderRows then DoSelectStyle;
end;

Procedure TFMSelDialog.DoSelectStyle;
begin
  with DipList do if ItemIndex>=-FStyleOptions.Count then begin
    if ItemIndex>=SystemLineStyles then FStyle:=
        TXLineStyle(FXLineList.Objects[ItemIndex-SystemLineStyles]).Number
    else if ItemIndex>=0 then FStyle:=ItemIndex
    else FStyle:=FStyleOptions.Values[Abs(ItemIndex)-1];
  end;
end;

procedure TFMSelDialog.DipListDrawCell(Sender: TObject; Canvas: TCanvas;
  Index: Integer; Rect: TRect);
var AText          : String;
    ARect          : TRect;
    Y              : Integer;
    Item           : TXLineStyle;
    Points       : Array[0..1] of TPoint;
begin
  with Canvas do begin
    if Index<0 then begin
      AText:=FStyleOptions.ShortNames[Abs(Index)-1];
      TextRect(Rect,CenterWidthInRect(TextWidth(AText),Rect),
          CenterHeightInRect(TextHeight(AText),Rect),AText);
    end
    else begin
      FillRect(Rect);
      ARect:=Classes.Rect(Rect.Left+DipList.RowHeight+3,
          Rect.Top,Rect.Left+80,Rect.Bottom);
      Y:=CenterHeightInRect(0,Rect);
      {
      if Index<SystemLineStyles then begin
        CommonListBitmaps.Draw(Canvas,Rect.Left+2,CenterHeightInRect(14,Rect),clbSystem);
        Pen.Style:=TPenStyle(Index);
        Pen.Width:=0;
        with ARect do Polyline([Point(Left+1,Y-1),Point(Right-1,Y-1)]);
        with ARect do Polyline([Point(Left+1,Y),Point(Right-1,Y)]);
        AText:=MlgSection1[20+Index];
      end
      else begin
      }
        CommonListBitmaps.Draw(Canvas,Rect.Left+2,CenterHeightInRect(14,Rect),clbUser);
        Item:=TXLineStyle(FXLineList.Objects[Index{-SystemLineStyles}]);
        with ARect do begin
          Points[0]:=Point(Left+1,Y);
          Points[1]:=Point(Right-1,Y);
          XPolyline(Canvas.Handle,Points,2,Item.StyleDef^.XLineDefs,
              Item.StyleDef^.Count,0,0,True,(DipList.RowHeight-2)
              /RectHeight(Item.ClipRect)/5);
        end;
        AText:=Item.Name;
      {
      end;
      }
      ARect:=InflatedWidth(Rect,-85,0);
      TextRect(ARect,ARect.Left,CenterHeightInRect(TextHeight(AText),ARect),AText);
    end;
  end;
end;

procedure TFMSelDialog.FormDestroy(Sender: TObject);
begin
  FXLineList.Free;
  FProjStyles.Free;
end;

Procedure TFMSelDialog.UpdateStylesList;
var XLineStyle   : TXLineStyle;
    Cnt          : Integer;
begin
  FProjStyles.Assign(FProject^.PInfo^.ProjStyles);

  FXLineList.Clear;
  if FProjStyles<>NIL then for Cnt:=0 to FProjStyles.XLineStyles.Capacity-1 do begin
    XLineStyle:=FProjStyles.XLineStyles.Items[Cnt];
    if XLineStyle<>NIL then FXLineList.AddObject(XLineStyle.Name,XLineStyle);
  end;
end;

end.
