Unit SymRol;

Interface

{$IFNDEF AXDLL} // <----------------- AXDLL

Uses AM_Font,AM_Obj,AM_Paint,AM_Proj,Classes,Combobtn,Controls,DipGrid,ExtCtrls,
     ExtLib,Graphics,Menus,Messages,MultiLng,ProjHndl,PropCollect,RollUp,Spinbtn,
     StdCtrls,StyleDef,SymEdit,SymLib,SymbolPropertyDlg,Validate,WCtrls,Windows,
     Dialogs,ImgList,ActnList,AM_Group,AM_Layer,AM_Index,AM_Sym, Buttons;

Type TSymbolRollupForm = class(TProjRollupForm)
       AngleVal         : TMeasureLookupValidator;
       DeleteSymMenu    : TMenuItem;
       EditSymMenu      : TMenuItem;
       Label1           : TWLabel;
       LibraryCombo     : TComboBox;
       LibraryPopupMenu : TPopupMenu;
       NewSymMenu       : TMenuItem;
       PropertySymMenu  : TMenuItem;
       Rollup           : TRollup;
       SizeVal          : TMeasureLookupValidator;
       SymbolList       : TDipList;
       SymbolPopupMenu  : TPopupMenu;
       LibNewMenu       : TMenuItem;
       LibOpenMenu      : TMenuItem;
       LibCloseMenu     : TMenuItem;
       LibSaveMenu      : TMenuItem;
       LibAllSaveMenu   : TMenuItem;
       MlgSection1      : TMlgSection;
       MenuBtn          : TPopupMenuBtn;
       Panel            : TPanel;
       Label2           : TWLabel;
       Label3           : TWLabel;
       AngleEdit        : TEdit;
       SizeEdit         : TEdit;
       SizeSpin         : TSpinBtn;
       AngleSpin        : TSpinBtn;
       N2               : TMenuItem;
       ScaleIndependendCheck: TCheckBox;
       N4               : TMenuItem;
       OpenDialog       : TOpenDialog;
       OrganizeMenu     : TMenuItem;
       SaveDialog       : TWSaveDialog;
       ActionList: TActionList;
       ImageList: TImageList;
       NewLibraryAction: TAction;
       OpenLibraryAction: TAction;
       CloseLibraryAction: TAction;
       SaveLibraryAction: TAction;
       SaveAllLibrariesAction: TAction;
       OrganizeAction: TAction;
       NewSymbolAction: TAction;
       EditSymbolAction: TAction;
       DeleteSymbolAction: TAction;
       SymbolPropertyAction: TAction;
       SymbolListAction: TAction;
       SymbolMosaicAction: TAction;
       N3: TMenuItem;
       ViewMenu: TMenuItem;
       MosaicMenu: TMenuItem;
       ListMenu: TMenuItem;
{++ Glukhov SearchSymbol BUILD#115 26.06.00 }
    SearchSymMenu: TMenuItem;
    SearchSymAction: TAction;
    SelectSymMenu: TMenuItem;
    DeleteUnused: TMenuItem;
    TempSizeVal: TMeasureLookupValidator;
{-- Glukhov SearchSymbol BUILD#115 26.06.00 }
       Procedure   DeleteSymMenuClick(Sender: TObject);
       Procedure   DipListDrawEventProc(Sender:TObject;Canvas:TCanvas;Index:Integer;Rect:TRect);
       Procedure   EditSymMenuClick(Sender: TObject);
       Procedure   FormCanResize(Sender: TObject; var NewWidth,
                       NewHeight: Integer; var Resize: Boolean);
       Procedure   FormCreate(Sender: TObject);
       Procedure   FormDestroy(Sender: TObject);
       Procedure   FormPaint(Sender: TObject);
       Procedure   LibAllSaveMenuClick(Sender: TObject);
       Procedure   LibCloseMenuClick(Sender: TObject);
       Procedure   LibNewMenuClick(Sender: TObject);
       Procedure   LibOpenMenuClick(Sender: TObject);
       Procedure   LibraryComboClick(Sender: TObject);
       Procedure   LibSaveMenuClick(Sender: TObject);
       Procedure   NewSymMenuClick(Sender: TObject);
       Procedure   OrganizeMenuClick(Sender: TObject);
       Procedure   PropertySymMenuClick(Sender: TObject);
{++ Glukhov SearchSymbol BUILD#115 26.06.00 }
       Procedure   SearchSymMenuClick(Sender: TObject);
{-- Glukhov SearchSymbol BUILD#115 26.06.00 }
       Procedure   SymbolListClick(Sender: TObject);
       Procedure   SymbolListMouseMove(Sender:TObject;Shift:TShiftState;X,Y:Integer);
       procedure   SizeEditChange(Sender: TObject);
       Procedure   AngleEditChange(Sender: TObject);
       Procedure   ScaleIndependendCheckClick(Sender: TObject);
       procedure SymbolListActionExecute(Sender: TObject);
       procedure SymbolMosaicActionExecute(Sender: TObject);
    procedure SymbolListDblClick(Sender: TObject);
    procedure SelectSymMenuClick(Sender: TObject);
    procedure DeleteUnusedClick(Sender: TObject);
      Private
       FStdSyms         : String;
       FExtLib          : pExtLib;
       FFonts           : pFonts;
       FLastIndex       : Integer;
       FExternLibs      : TList;
       PInfo            : PPaint;
       FEditOptions     : TSelectOptionList;
       Procedure   SetListMosaic(AMosaic:Boolean);
       Procedure   UpdateEnabled;
       Procedure   CheckSize;
// IDB. Rem by Ivanoff. the procedure was removed below.       Procedure   UpdateLibChange;
       Procedure   UpdateSymChange;
      Protected
       Procedure   SetProject(AProject:PProj); override;
      Public
       FSymInfo         : TSymLibInfo;
       FSymbols         : pSymbols;
       FSendSymInfo     : Boolean;
       FWasVisible      : Boolean;
       FSendSymIndex    : Integer;
       Procedure   CloseSymLib(AName: String);
       Function    OpenSymLib(LibNamePath:String;NewLib:Boolean):Boolean;
       Procedure   SetActSymbol(ASymbol:PSGroup);
       Function    SetActLib(AName: String):Boolean;
       Procedure   UpdateSymbols;
{++ IDB}
       Function GetSymLibInfo: TSymLibInfo; // This function was added 'cause it is necessary for undelete of symbols.
       Procedure UpdateLibChange; // This procedure was moved here from above.
{-- IDB}
     end;
{$ENDIF} // <----------------- AXDLL
Implementation
{$IFNDEF AXDLL} // <----------------- AXDLL

{$R *.DFM}

Uses AM_Child,AM_Def,AM_Main,ExtCanvas,Forms,Measures,NumTools,
     SysUtils, OrganizeDlg, MenuFn, AM_ProjO, SymSelDlg
{$IFNDEF WMLT}
     ,AM_DDE
{$ENDIF}
     ;

Procedure TSymbolRollupForm.SetProject
   (
   AProject        : PProj
   );
var SymParent : TMDIChild;
    RealProj  : pProj;
begin
  if Project<>NIL then FSymInfo.Free;
  inherited SetProject(AProject);
  SizeVal.LookupList.Clear;
  AngleVal.LookupList.Clear;
  if Project=NIL then begin
    SizeEdit.Text:=''; 
    AngleEdit.Text:='';
    LibraryCombo.Items.Clear;
    SymbolList.Count:=0;
  end
  else begin
    PInfo^.Fonts:=AProject^.PInfo^.Fonts;
    LibraryCombo.Items.Clear;
    FStdSyms:=MlgStringList['SymbolRollup',17];
    RealProj:=Project;
    if (Project^.SymbolMode=sym_SymbolMode) then begin
      SymParent:=TMDIChild(Project^.SymEditInfo.SymParent);
      if (SymParent<>nil) then RealProj:=SymParent.Data;
    end;
    if RealProj = nil then exit;
    FSymbols:=PSymbols(RealProj^.PInfo^.Symbols);
    FFonts:=RealProj^.PInfo^.Fonts;
    FExtLib:=nil;
    FSymInfo:=TSymLibInfo.Create(RealProj,FSymbols,FStdSyms);
    FSymInfo.CopyToListBox(LibraryCombo);
    if (Project^.ActSymLib='') then FSymInfo.LibIndex:=0
    else FSymInfo.LibIndex:=LibraryCombo.Items.IndexOf(Project^.ActSymLib);
    FSymInfo.SymIndex:=FSymInfo.IndexOf(Project^.ActualSym);

    if (Project^.SymbolMode<>sym_SymbolMode) then begin
      SymbolList.ItemIndex:=FSymInfo.IndexOf(Project^.ActualSym);
    end;

    SetSizeField(Project^.ActSymSize,Project^.ActSymSizeType,SizeVal,FEditOptions,
      ScaleIndependendCheck);
    SetDoubleField(Project^.ActSymAngle,1,AngleVal,muRad,FEditOptions);
    UpdateLibChange;
  end;
  UpdateEnabled;
end;

Procedure TSymbolRollupForm.DipListDrawEventProc(Sender:TObject;Canvas:TCanvas;
    Index:Integer;Rect:TRect);
var aSym    : pSGroup;
    X,Y     : Integer;
    s       : String;
    OrigRect: TRect;
    i       : Integer;
begin
  if FSymInfo=NIL then Exit;
  if FSymInfo.SymCount<Index then Exit;

  aSym:=FSymInfo.GetSym(Index);

  if aSym = nil then exit;

  Canvas.FillRect(Rect);

  OrigRect:=Rect;
  if (aSym<>nil) and (ASym^.Index >= mi_Group) and (ASym^.GetObjType = ot_Group) then begin
    if SymbolList.ColCount=1 then begin
       Rect.Right:=Rect.Left+Max((RectWidth(Rect) div 3),RectHeight(Rect));
    end
    else begin
       Rect.Right:=Rect.Left+RectWidth(Rect);
    end;
    PInfo^.ExtCanvas.Handle:=Canvas.Handle;
    try
       aSym^.DrawSymbolInRect(PInfo,Rect);
    except
    end;
    if SymbolList.ColCount=1 then begin
      // force VCL to recreate the drawing-objects
      Canvas.Refresh;
      // draw the symbol-name
      Canvas.Font.Name:='Small Fonts';
      Canvas.Font.Size:=7;


      s:=PToStr(aSym^.Name);
      X:=Rect.Right + 5;
      Y:=(Rect.Top + (RectHeight(Rect) div 3)) - (Canvas.TextHeight(s) div 2);
      Canvas.TextOut(X,Y,s);

      SetSymSizeField(ASym^.DefaultSize,ASym^.DefaultSizeType,TempSizeVal);

      s:=MlgSection1.Strings[42] + TempSizeVal.AsText;
      Y:=(Rect.Bottom - (RectHeight(Rect) div 3)) - (Canvas.TextHeight(s) div 2);
      Canvas.TextOut(X,Y,s);

      if aSym^.UseCount > 0 then begin
         s:=MlgSection1.Strings[43] + IntToStr(aSym^.UseCount);
         X:=OrigRect.Right-(RectWidth(OrigRect) div 3);
         Canvas.TextOut(X,Y,s);
      end;
    end;
  end;
end;

Procedure TSymbolRollupForm.UpdateEnabled;
var AEnable        : Boolean;
begin
  AEnable:=Project<>NIL;
{++ Moskaliov BUG#148 BUILD#100 10.01.00}
//SetControlEnabled(LibraryCombo,AEnable);
  SymbolList.Enabled:=AEnable;
  EditSymMenu.Enabled:=AEnable;
  if AEnable then
     begin
        AEnable:= (Project.SymbolMode <> sym_SymbolMode);                  // TRUE if the project is not symbol-edit progect
        AEnable:= AEnable and (Project^.SymEditInfo.SymEditChild = nil);   // TRUE if the symbol-editor is not activ for this project
        AEnable:= (not Project^.PInfo^.IsMU);
     end;
  NewSymMenu.Enabled:=AEnable;
{++ Glukhov SearchSymbol BUILD#115 26.06.00 }
  SearchSymMenu.Enabled:= AEnable;
  SelectSymMenu.Enabled:= AEnable;
{-- Glukhov SearchSymbol BUILD#115 26.06.00 }
  DeleteSymMenu.Enabled:=AEnable;
  SetControlEnabled(LibraryCombo,AEnable);
{-- Moskaliov BUG#148 BUILD#100 10.01.00}
  EditSymMenu.Enabled:=AEnable;
  PropertySymMenu.Enabled:=AEnable;
  SetControlEnabled(SizeEdit,AEnable);
  if not AEnable then ScaleIndependendCheck.Enabled:=FALSE;
  SetControlEnabled(AngleEdit,AEnable);
  MenuBtn.Enabled:=AEnable;
  AEnable:=AEnable and (LibraryCombo.ItemIndex<>0);
  SaveLibraryAction.Enabled:=AEnable;
  CloseLibraryAction.Enabled:=AEnable;
end;

Procedure TSymbolRollupForm.LibraryComboClick(Sender: TObject);
begin
  if Project=NIL then Exit;
  if FSymInfo=NIL then Exit;
  FSymInfo.LibIndex:=LibraryCombo.ItemIndex;
  UpdateLibChange;
  Project^.ActSymLib:=FSymInfo.LibName;
  UpdateEnabled;
end;

Function TSymbolRollupForm.SetActLib(AName: String):Boolean;
var AExtLib        : PExtLib;
    ALibIndex      : Integer;
begin
  Result:=FALSE;
  if Project=NIL then Exit;
  if FSymInfo=NIL then Exit;
  AExtLib:=FSymbols^.GetExtLib(ExtractFileName(AName));
  if (AExtLib <> NIL) or (AName = '') then begin
    if AExtLib <> NIL then LibraryCombo.ItemIndex:=FSymbols^.ExternLibs.IndexOf(AExtLib)+1
    else LibraryCombo.ItemIndex:=0;
    FSymInfo.LibIndex:=LibraryCombo.ItemIndex;
    UpdateLibChange;
    Project^.ActSymLib:=FSymInfo.LibName;
    UpdateEnabled;
    Result:=TRUE;
  end;
end;

Procedure TSymbolRollupForm.UpdateLibChange;
begin
{++ Glukhov SearchSymbol BUILD#115 26.06.00 }
  SearchSymMenu.Enabled:= False;
{-- Glukhov SearchSymbol BUILD#115 26.06.00 }
  if Project=NIL then Exit;
  if FSymInfo=NIL then Exit;

  SymbolList.Count:=FSymInfo.SymCount;

{++ Glukhov SearchSymbol BUILD#115 26.06.00 }
  if FSymInfo.SymCount > 0  then SearchSymMenu.Enabled:= True;
{-- Glukhov SearchSymbol BUILD#115 26.06.00 }
  if (Project^.SymbolMode<>sym_SymbolMode) then begin
     SymbolList.ItemIndex:=FSymInfo.SymIndex;
  end;
  LibraryCombo.ItemIndex:=FSymInfo.LibIndex;
  UpdateSymChange;
end;

Procedure TSymbolRollupForm.UpdateSymChange;
var aSym  : pSGroup;
    i     : Integer;
begin
  if Project=NIL then Exit;
  aSym:=FSymInfo.Sym;
  if (Project^.SymbolMode<>sym_SymbolMode) then begin
    Project^.ActualSym:=aSym;
    Project^.ActSymLib:=FSymInfo.LibName;
    SymbolList.ItemIndex:=FSymInfo.SymIndex;
  end else begin
    Project^.ActualSym:=nil;
    Project^.ActSymLib:='';
{    aSym:=Project^.SymEditInfo.SymbolToEdit;
    if aSym<>nil then begin
      if aSym^.Libname<>nil then
        if FSymInfo.LibName<>aSym^.LibName^ then
          for i:=0 to FSymInfo.LibCount-1 do
            if FSymInfo.GetLibName(i)=aSym^.LibName^ then
              FSymInfo.LibIndex:=i;
      FSymInfo.SymIndex:=FSymInfo.IndexOf(aSym);
      SymbolList.ItemIndex:=FSymInfo.SymIndex;   }
  end;
  SymbolList.RePaint;
end;

Procedure TSymbolRollupForm.LibNewMenuClick(Sender: TObject);
var AFilter   : String;
begin
  SaveDialog.DefaultExt:=MlgStringList['SymbolMsg',16];
  SaveDialog.Title:=MlgStringList['SymbolMsg',17];
  AFilter:=MlgStringList['SymbolMsg',14]+'|'+MlgStringList['SymbolMsg',15];
  SaveDialog.Filter:=AFilter;        { Filter f�r wsl bzw. alle Dateien zusammenstricken }
  SaveDialog.Options:=[ofCreatePrompt,ofHideReadOnly];
  if (SaveDialog.Execute) then begin  { Dialog wurde mit OK beendet }
    OpenSymLib(SaveDialog.FileName,True);
    end;
end;

Procedure TSymbolRollupForm.LibOpenMenuClick(Sender: TObject);
begin
  OpenDialog.DefaultExt:=MlgStringList['SymbolMsg',16];
  OpenDialog.Filter:=MlgStringList['SymbolMsg',14]+'|'+MlgStringList['SymbolMsg',15];
  OpenDialog.Options:=[ofFileMustExist,ofHideReadOnly];
  if OpenDialog.Execute then OpenSymLib(OpenDialog.FileName,False);
end;

Function TSymbolRollupForm.OpenSymLib
   (
   LibNamePath  : String;
   NewLib       : Boolean
   )
   : Boolean;
var NewExtLib,
    aExtLib   : PExtLib;
    LibName   : String;
    LibIndex,
    retMess   : Integer;
begin
    Result:=FALSE;
    LibName:=ExtractFileName(LibNamePath);
    if (LibraryCombo.Items.IndexOf(LibName)>-1) then begin  { Bib. bereits ge�ffnet }
      retMess:=MessageDialog(MlgStringList['SymbolRollup',5],
                        mtConfirmation,[mbYes,mbNo,mbCancel],0);
      if (retMess=mrCancel) then Exit;
      LibraryCombo.ItemIndex:=LibraryCombo.Items.IndexOf(LibName);   { selektieren }
      FSymInfo.LibIndex:=LibraryCombo.ItemIndex;
      if (retMess=mrYes) then begin
        FSymInfo.ExtLib^.ReLoad;
        FSymInfo.UpdateExtLib;
        end;
      UpDateLibChange;
      Result:=TRUE;
      Exit;
      end;
    if (FileExists(LibNamePath)) then begin
      if (NewLib) then begin
        retMess:=MessageDialog(Format(MlgStringList['SymbolRollup',6],[LibNamePath]),
                        mtConfirmation,[mbYes,mbNo,mbCancel],0);
        if (retMess=mrCancel) then Exit;
        end;
      NewExtLib:=new(PExtLib,Init(FSymbols^.Owner,LibNamePath));
      aExtLib:=NewExtLib^.AlreadyLoaded;
      if (aExtLib<>nil) then begin
        Dispose(NewExtLib,Done);
        NewExtLib:=aExtLib;
        Inc(NewExtLib^.UseCount);
        end;
      if (NewLib) and (retMess=mrYes) then NewExtLib^.NewExtLib
        else if (not NewExtLib^.Loaded) then NewExtLib^.ReLoad; { externe Bibliothek laden }
      Result:=TRUE;
      end else begin    { neue externe Bibliothek... }
      if (not NewLib) then begin
        retMess:=MessageDialog(Format(MlgStringList['SymbolRollup',7],[LibNamePath]),
          mtConfirmation,[mbYes,mbNo,mbCancel],0);
        if (retMess=mrCancel) or (retMess=mrNo) then Exit;
        end;
      NewExtLib:=new(PExtLib,Init(FSymbols^.Owner,LibNamePath));
      NewExtLib^.NewExtLib;           { leere Symbolbibliothek erzeugen }
      Result:=TRUE;
      end;

  if NewExtLib^.LibState <> el_UnAvailable then begin
    LibIndex:=LibraryCombo.Items.Add(LibName);
    FSymbols^.ExternLibs.Add(NewExtLib); { zur Liste der externen Bibliotheken }
                                         { anh�ngen... }
    LibraryCombo.ItemIndex:=LibIndex;
    FSymInfo.LibIndex:=LibIndex;
    FSymInfo.Add(NewExtLib);
    FSymInfo.UpdateProjLib;              { Projektsymbolinfo neumachen }
    UpdateLibChange;
    FSymInfo.SetProjModified;
  end;
end;

Procedure TSymbolRollupForm.LibCloseMenuClick(Sender: TObject);
var SelLibName  : String;
    i           : Integer;
    LibToDelete : PExtLib;
    LibClosePoss: Boolean;
begin
  LibToDelete:=nil;
  SelLibName:=FSymInfo.LibName;
  if (SelLibName<>FStdSyms) then begin
    LibClosePoss:=True;
    if (Project^.SymbolMode=sym_SymbolMode) and   { Symboleditor mit Symbol aus zu schlie�ender Bibliothek }
       (Project^.SymEditInfo.SymbolToEdit^.GetLibName=FSymInfo.LibName) then
         LibClosePoss:=False;
    if (Project^.SymEditInfo.SymEditChild<>nil) then
      if (TMDIChild(Project^.SymEditInfo.SymEditChild).Data^.SymEditInfo
        .SymbolToEdit^.GetLibName=FSymInfo.LibName) then
          LibClosePoss:=False;
    if (not LibClosePoss) then begin
      MessageDialog(MlgStringList['SymbolRollup',15],mtInformation,[mbOk],0);
      exit;
      end;
    LibToDelete:=FSymInfo.ExtLib;
    if (LibToDelete<>nil) then begin
      if MessageDialog(Format(MlgStringList['SymbolRollup',8],[LibToDelete^.LibName^]),
                mtInformation,[mbYes,mbNo],0)=mrYes then begin
        LibraryCombo.Items.Delete(LibraryCombo.ItemIndex);
        FSymInfo.DeleteLib(FSymInfo.LibIndex);
        LibraryCombo.ItemIndex:=0;              { ersten Eintrag selektieren }
        i:=FSymbols^.ExternLibs.IndexOf(LibToDelete);
        if (i>-1) then FSymbols^.ExternLibs.Delete(i);
        if (LibToDelete^.UseCount=0) then begin
          LibToDelete^.RemoveLastInstance;                {100298-1Ray}
          Dispose(LibToDelete,Done);  { "letze" Instanz }
          end else Dec(LibToDelete^.UseCount);        { wird einmal "weniger" gebraucht }
        FSymInfo.UpdateProjLib;                 { Projektsymbolinfo neumachen }
        FSymInfo.LibIndex:=0;
        UpdateLibChange;
        FSymInfo.SetProjModified;
        end;
      end;
    end;
end;

Procedure TSymbolRollupForm.CloseSymLib(AName: String);
var SelLibName  : String;
    i           : Integer;
    LibToDelete : PExtLib;
    LibClosePoss: Boolean;
    DelIndex    : Integer;
begin
  LibToDelete:=nil;
  SelLibName:=AName;
  if (SelLibName<>FStdSyms) then begin
    LibClosePoss:=True;
    if (Project^.SymbolMode=sym_SymbolMode) and   { Symboleditor mit Symbol aus zu schlie�ender Bibliothek }
       (Project^.SymEditInfo.SymbolToEdit^.GetLibName=FSymInfo.LibName) then
         LibClosePoss:=False;
    if (Project^.SymEditInfo.SymEditChild<>nil) then
      if (TMDIChild(Project^.SymEditInfo.SymEditChild).Data^.SymEditInfo
        .SymbolToEdit^.GetLibName=FSymInfo.LibName) then
          LibClosePoss:=False;
    if (not LibClosePoss) then begin
      MessageDialog(MlgStringList['SymbolRollup',15],mtInformation,[mbOk],0);
      exit;
      end;
    LibToDelete:=FSymbols^.GetExtLib(SelLibName);
    if (LibToDelete<>nil) then begin
      DelIndex:=LibraryCombo.Items.IndexOf(SelLibName);
      if LibraryCombo.ItemIndex = DelIndex then LibraryCombo.ItemIndex:=0;
      LibraryCombo.Items.Delete(DelIndex);
      FSymInfo.DeleteLib(DelIndex);
      i:=FSymbols^.ExternLibs.IndexOf(LibToDelete);
      if (i>-1) then FSymbols^.ExternLibs.Delete(i);
      if (LibToDelete^.UseCount=0) then begin
        LibToDelete^.RemoveLastInstance;                {100298-1Ray}
        Dispose(LibToDelete,Done);  { "letze" Instanz }
        end else Dec(LibToDelete^.UseCount);        { wird einmal "weniger" gebraucht }
      FSymInfo.UpdateProjLib;                 { Projektsymbolinfo neumachen }
      FSymInfo.LibIndex:=0;
      UpdateLibChange;
      FSymInfo.SetProjModified;
      end;
    end;
end;

Procedure TSymbolRollupForm.NewSymMenuClick(Sender: TObject);
var SymName  : AnsiString;
    ExtLib   : PExtLib;
    ASym     : PSGroup;
begin
  ExtLib:=FSymInfo.ExtLib;
  if (ExtLib=nil) then begin
    SymName:=FSymInfo.Lib^.GetNewSymName(MlgStringList['SymbolMsg',3],'');
    if (not InputQuery(MlgStringList['SymbolMsg',3],MlgStringList['SymbolMsg',18],SymName)) or (Trim(SymName) = '') then begin
       exit;
    end;
    SymName:=FSymInfo.Lib^.GetNewSymName(SymName,'');
    ASym:=New(PSGroup,InitName(SymName,''));
    FSymInfo.Lib^.InsertLibSymbol(nil,'',ASym,False);   { Leeres Symbol einf�gen}
    if (Project<>nil) then Project^.SetModified;
    end else begin
    SymName:=FSymInfo.Lib^.GetNewSymName(MlgStringList['SymbolMsg',3],
                      ExtLib^.LibName^);               {Name f�r neues Symbol }
    ASym:=New(PSGroup,InitName(SymName,PToStr(ExtLib^.LibName)));
    FSymInfo.Lib^.InsertSymbol(nil,ASym,False);        { Leeres Symbol einf�gen }
    ExtLib.LibState:=ExtLib.LibState or el_Modified;  { ext. Bib wurde ge�ndert }
  end;
  FSymInfo.GetSymInfo.AddSym(ASym);
  UpdateLibChange;
{++ Moskaliov BUG#475 BUILD#95 06.12.99}
  EditSymMenuClick(Sender);
{-- Moskaliov BUG#475 BUILD#95 06.12.99}
end;

Procedure TSymbolRollupForm.DeleteSymMenuClick(Sender: TObject);
var ASymbol      : PSGroup;
begin
  if (FSymInfo.SymSelected) then begin
    ASymbol:=FSymInfo.Sym;
    if (FSymInfo.Lib=FSymbols) and (ASymbol^.UseCount>0) then
      MessageDialog(MlgStringList['SymbolRollup',16],mtInformation,[mbOk],0)
      else begin
      if (MessageDialog(Format(MlgStringList['SymbolRollup',9],[ASymbol^.GetName]),
              mtWarning,[mbYes,mbNo],0)=mrYes) then begin
{++ IDB}
        {$IFNDEF WMLT}
        If WinGisMainForm.WeAreUsingUndoFunction[SELF.Project] Then
           // Put the symbol in UNDO data.
           WinGisMainForm.UndoRedo_Man.DeleteSymbol(SELF.Project, ASymbol);
        {$ENDIF}   
{-- IDB}
        if (FSymInfo.Lib=FSymbols) then FSymbols^.DeleteSymbol(ASymbol)
          else FSymInfo.ExtLib^.DeleteSymbol(ASymbol);
        FSymInfo.Delete(ASymbol);
        FSymInfo.UpdateProjLib;     { Symbol k�nnte bereits im Projekt sein }
        UpdateLibChange;
        FSymInfo.SetModified;
        end;
      end;
    end;
end;

{++ Glukhov SearchSymbol BUILD#115 26.06.00 }
Procedure TSymbolRollupForm.SearchSymMenuClick(Sender: TObject);
var ExtLib      : pExtLib;
    iOldSymIdx  : Integer;
    SymPropDlg  : TSymbolPropertyDialog;
    DlgRes      : TModalResult;
begin
  if Not FSymInfo.SymSelected then Exit;

  SymPropDlg:=TSymbolPropertyDialog.Create(self);
  SymPropDlg.Fonts:=FFonts;
  SymPropDlg.bSearchMode:= True;

  SymPropDlg.Symbols:= FSymInfo.Lib;
  SymPropDlg.ProjSymbols:= FSymbols;
  SymPropDlg.Symbol:= FSymInfo.Sym;

  DlgRes:= SymPropDlg.ShowModal;
//  if (DlgRes = mrOK) and (SymPropDlg.HasChanged) then begin
  if (DlgRes = mrOK) then begin
    SetActSymbol( SymPropDlg.Symbol );
  end;
  SymPropDlg.Free;
//  if AEnable then SymbolList.Repaint;
//  SymbolList.Repaint;
end;
{-- Glukhov SearchSymbol BUILD#115 26.06.00 }

Procedure TSymbolRollupForm.PropertySymMenuClick(Sender: TObject);
var ExtLib      : pExtLib;
    ProjSym     : PSGroup;
    SymPropDlg  : TSymbolPropertyDialog;
begin
   if (FSymInfo.SymSelected) then
      begin
         SymPropDlg:=TSymbolPropertyDialog.Create(self);
//         SymPropDlg.Project:=Project;
         SymPropDlg.Fonts:=FFonts;
         SymPropDlg.Symbols:=FSymInfo.Lib;
         SymPropDlg.ProjSymbols:=FSymbols;
         SymPropDlg.Symbol:=FSymInfo.Sym;
         if SymPropDlg.ShowModal=mrOK then
            begin
               if (SymPropDlg.HasChanged) then
                  begin { The data were realy changed}
                     ExtLib:=FSymInfo.ExtLib;
                     if (ExtLib<>nil) then
                        begin  { Projektsymbol aktuell halten }
                           ProjSym:=FSymbols^.GetProjectSymbol(SymPropDlg.Symbol^.GetName,
                                                               SymPropDlg.Symbol^.GetLibName);
                           if (ProjSym<>nil) then
                              FSymbols^.UpdateSym(ProjSym,SymPropDlg.Symbol);
                        end;
                     FSymInfo.SetModified;
                     FSymInfo.GetSymInfo.Sort;
                      //brovak Bug 526
                     SetActSymbol(SymPropDlg.Symbol );

{++ Moskaliov RedrawAfterSymPropWasChanged BUILD#125 01.08.00}
                     if FSymInfo.Sym^.UseCount > 0 then
                        PProj(WinGISMainForm.ActualProj)^.PInfo^.RedrawScreen(True);
{-- Moskaliov RedrawAfterSymPropWasChanged BUILD#125 01.08.00}
                  end;
            end;
         SymPropDlg.Free;
         UpdateSymChange;
      end
   else
      MessageDialog(MlgStringList['SymbolRollup',11],mtInformation,[mbOK],0);
end;

Procedure TSymbolRollupForm.EditSymMenuClick(Sender: TObject);
var SymProject : PProj;
    SymChild   : TMDIChild;
    SymCapt    : PChar;
    ASym       : PSGroup;
{++ IDB}
    iI: Integer;
    AProject: Pointer;
{-- IDB}
//++ Glukhov Bug#396 Build#161 24.05.01
// The project actual font data
    iFnt, iSty, iH  : Integer;
//-- Glukhov Bug#396 Build#161 24.05.01
 //Brovak Bug 526
    TmpProj:PProj;
    AIdx: Integer;
begin
  TmpProj:=Project;

  if (FSymInfo.SymSelected) then begin

     ASym:=FSymInfo.Sym;

    if (Project^.SymEditInfo.SymEditChild<>nil) then begin
      AIdx:=SymbolList.ItemIndex;
      if not Project^.SymEditInfo.CanCloseSymEditor then begin
         exit;
      end;
      SymbolList.ItemIndex:=AIdx;
    end;

//    if (Project^.SymbolMode=sym_SymbolMode) then exit;   { vorerst }

    SymProject:=New(PProj,Init);
    if (SymProject=nil) then begin
      MessageDialog(MlgStringList['SymbolRollup',13],
                        mtInformation,[mbOK],0);
      exit;
      end;
{++ Ivanoff BUG#555 BUILD#100}
{Fonts of the symbol project and the project sometimes may be different.
As these projects use number of font, but not font name, in the project and
in the symbol project the same fonts may have different numbers.
Following statement synchronises these fonts.}
    SymProject.PInfo.Fonts := Project.PInfo.Fonts;
{-- Ivanoff}
//++ Glukhov Bug#396 Build#161 24.05.01
// Save the project actual font data
    iFnt:= Project.ActualFont.Font;
    iSty:= Project.ActualFont.Style;
    iH  := Project.ActualFont.Height;
//-- Glukhov Bug#396 Build#161 24.05.01
    SymProject.SymbolMode:=sym_SymbolMode;
    with SymProject^.SymEditInfo do begin
      SymbolToEdit:=ASym;
      if (SymbolToEdit^.InEditor) then begin
        MessageDialog(MlgStringList['SymbolRollup',19],
                        mtInformation,[mbOK],0);
        Dispose(SymProject,Done);
        exit;
        end;

      SymExtLib:=FSymInfo.ExtLib;
      if (FExtLib<>nil) then SymParent:=self  { Bib ohne Projekt bearbeiten }
        else
{++ IDB}
            Begin
{-- IDB}
            SymParent:=WinGISMainForm.GetCurrentChild;  { Vaterprojekt merken }
{++ IDB}
            // If we have as a top window a window of the IDB, I have to find the project
            // window to correct work of symbol editor. Ivanoff.
            {$IFNDEF WMLT}
            If SymParent = NIL Then Begin
               TRY
               AProject := WinGISMainForm.IDB_Man.GetActiveWindowProject;
               For iI:=0 To WinGisMainForm.MDIChildCount-1 Do
                   If (WinGisMainForm.MDIChildren[iI] IS TMDIChild)
                      AND
                      (TMDIChild(WinGisMainForm.MDIChildren[iI]).Data = AProject) Then Begin
                         SymParent := WinGISMainForm.MDIChildren[iI];
                         BREAK;
                         End;
               EXCEPT
               END;
               End;
            {$ENDIF}
            End
{-- IDB}
      end;

    SymCapt:=StrNew('SymbolEditor');

    SymChild:=TMDIChild.Create(WinGISMainForm,SymCapt,SymProject,False);
    if (SymChild=nil) then begin
      MessageDialog(MlgStringList['SymbolRollup',13],
                        mtInformation,[mbOK],0);
      Dispose(SymProject,Done);          { wenn was schiefgeht, aufr�umen }
      StrDispose(SymCapt);
      exit;
      end;

    SymProject^.SymEditInfo.SymEditChild:=SymChild;
    if (FExtLib=nil) then
      TMDIChild(SymProject^.SymEditInfo.SymParent)
         .Data^.SymEditInfo.SymEditChild:=SymChild;   { Ursprungsprojekt Symboleditorfenster auch mitteilen }

    SymProject^.SymEditInfo.SymEditor:=
              TSymEditor.Create(SymProject,SymProject^.SymEditInfo.SymExtLib);

    if (SymProject^.SymEditInfo.SymEditor=nil) then begin
      MessageDialog(MlgStringList['SymbolRollup',13],
                        mtInformation,[mbOK],0);
      Project^.SymEditInfo.SymEditChild:=nil;
      SymChild.Destroy;
      StrDispose(SymCapt);
      Exit; 
      end;
   //brovak
    TSymEditor(SymProject^.SymEditInfo.SymEditor).BossProject:=TmpProj;
//++ Glukhov Bug#396 Build#161 24.05.01
// The symbol actual font has to correspond to the project actual font
    SymProject.ActualFont.Font:= iFnt;
    SymProject.ActualFont.Style:= iSty;
    SymProject.ActualFont.Height:= iH;
//-- Glukhov Bug#396 Build#161 24.05.01
    SymProject^.SymEditInfo.SymbolToEdit^.InEditor:=True;{ Symbol wird bearbeitet }

    StrDispose(SymCapt);

    SymProject^.SetOverView;

    end else MessageDialog(MlgStringList['SymbolRollup',11],
                        mtInformation,[mbOK],0);
end;

Procedure TSymbolRollupForm.LibAllSaveMenuClick(Sender: TObject);
begin
  if (FExtLib<>nil) then FExtLib^.SaveExtLib
    else FSymbols^.StoreExtLibs(False);       { Keine Benutzerabfrage }
end;

Procedure TSymbolRollupForm.LibSaveMenuClick(Sender: TObject);
var ExtLib        : PExtLib;
begin
  if FExtLib<>NIL then FExtLib^.SaveExtLib
  else begin
    ExtLib:=FSymInfo.ExtLib;
    if ExtLib<>NIL then ExtLib^.SaveExtLib;
  end;
end;

Procedure TSymbolRollupForm.FormPaint(Sender: TObject);
begin
  if Project<>NIL then begin
    FSymInfo.UpdateProjLib;
    UpdateLibChange;
  end           
  else SymbolList.ItemIndex:=-1;
end;

Procedure TSymbolRollupForm.SymbolListMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var Index        : Integer;
begin
  Index:=SymbolList.ItemAtPos(Point(X,Y),True);
  if Index<>FLastIndex then begin
    Application.CancelHint;
    FLastIndex:=Index;
    if Index<0 then SymbolList.Hint:=''
    else SymbolList.Hint:=PToStr(FSymInfo.GetSym(Index)^.Name);
  end;

  if SymbolSelDlg <> nil then begin
     SymbolSelDlg.LabelTT.Caption:=SymbolList.Hint;
  end;

end;

Procedure TSymbolRollupForm.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  if SymbolList.Parent<>NIL then with SymbolList.Parent do begin
    // calculate client-area-size for new size
    NewWidth:=NewWidth-Width+ClientWidth;
    NewHeight:=NewHeight-Height+ClientHeight;
    // resize the symbol-grid
    DisableAlign;
    SymbolList.Width:=NewWidth-2*SymbolList.Left;
    SymbolList.Height:=NewHeight-SymbolList.Top-Panel.Height;
    SymbolList.ColCount:=SymbolList.ClientWidth Div SymbolList.CellWidth;
    // resize the library-combobox and the menubutton
    LibraryCombo.Width:=SymbolList.Width-24;
    MenuBtn.Left:=LibraryCombo.Width+12;
    // resize the dialog
    NewWidth:=SymbolList.Width+2*SymbolList.Left+Width-ClientWidth;
    NewHeight:=SymbolList.Height+SymbolList.Top+Height-ClientHeight+Panel.Height;
    EnableAlign;
  end;                                            
end;

Procedure TSymbolRollupForm.OrganizeMenuClick(Sender: TObject);
{$IFNDEF WMLT}
var Dialog         : TOrganizeDialog;
{$ENDIF}
begin
{$IFNDEF WMLT}
  Dialog:=TOrganizeDialog.Create(Self);
  with Dialog do try
    XLineStyles:=Project^.PInfo^.ProjStyles.XLineStyles;
    XFillStyles:=Project^.PInfo^.ProjStyles.XFillStyles;
    Fonts:=Project^.PInfo^.Fonts;
    Symbols:=PSymbols(Project^.PInfo^.Symbols);
    StartupPage:=opSymbols;
    if ShowModal=mrOK then begin
      if ProjectModified then Project^.SetModified;
    end;
  finally
    Dialog.Free;
  end;
{$ENDIF}
end;

Procedure TSymbolRollupForm.FormCreate(Sender: TObject);
begin
  FExternLibs:=TList.Create;
  // setup pinfo
  PInfo:=New(PPaint,Init);
  PInfo^.ExtCanvas.Cached:=FALSE;
  PInfo^.Printing:=TRUE;
  Dispose(PInfo^.Fonts,Done);
  FEditOptions:=TSelectOptionList.Create;
  SetupSelectOptions(FEditOptions,sotSymbol,[soObject]);
  UpdateEnabled;
  FSendSymInfo:=FALSE;
  FWasVisible:=FALSE;
  FSendSymIndex:=-1;
end;

Procedure TSymbolRollupForm.SymbolListClick(Sender: TObject);
begin
  if Project<>NIL then begin
    if FSymInfo=NIL then Exit;
    if not FSendSymInfo then begin
      FSymInfo.SymIndex:=SymbolList.ItemIndex;
      UpdateSymChange;
      Project^.ActualSym:=FSymInfo.Sym;
    end
    else FSendSymIndex:=SymbolList.ItemIndex;
  end;
end;

Procedure TSymbolRollupForm.SetActSymbol(ASymbol: PSGroup);
begin
  if Project<>NIL then begin
    if FSymInfo=NIL then Exit;
    FSymInfo.SymIndex:=FSymInfo.IndexOf(ASymbol);
    UpdateSymChange;
    Project^.ActualSym:=FSymInfo.Sym;
  end;
end;

Procedure TSymbolRollupForm.FormDestroy(Sender: TObject);
begin
  FExternLibs.Free;
  PInfo^.Fonts:=NIL;
  PInfo^.ExtCanvas.Handle:=0;
  Dispose(PInfo,Done);
  if Project<>NIL then FSymInfo.Free;
  FEditOptions.Free;
end;

Procedure TSymbolRollupForm.SizeEditChange(Sender: TObject);
var AEnable        : Boolean;
begin
  AEnable:=SizeEdit.Enabled and (SizeVal.LookupIndex=-1)
      and not (SizeVal.Units in [muDrawingUnits,muNone]);
{  if not AEnable then ScaleIndependendCheck.Checked:=FALSE;;
  ScaleIndependendCheck.Enabled:=AEnable;}
  if SizeVal.Value = 0 then begin
     ScaleIndependendCheck.Checked:=False;
     ScaleIndependendCheck.Enabled:=False;
  end
  else begin
     ScaleIndependendCheck.Enabled:=True;
  end;

  if SizeVal.ValidInput then begin
    Project^.ActSymSize:=GetSizeField(SizeVal,FEditOptions,ScaleIndependendCheck);
    Project^.ActSymSizeType:=GetSizeTypeField(SizeVal,FEditOptions,ScaleIndependendCheck);
  end;
end;

Procedure TSymbolRollupForm.AngleEditChange(Sender: TObject);
begin
  if AngleVal.ValidInput then Project^.ActSymAngle:=GetDoubleField(1,AngleVal,
      muRad,FEditOptions);
end;

Procedure TSymbolRollupForm.ScaleIndependendCheckClick(Sender: TObject);
begin
  CheckSize;
  try
     SizeEdit.SetFocus;
     if Sender <> nil then begin
        if TWinControl(Sender).Enabled then begin
           TWinControl(Sender).SetFocus;
        end;
     end;
  except
  end;
  if SizeVal.ValidInput then begin
    Project^.ActSymSize:=GetSizeField(SizeVal,FEditOptions,ScaleIndependendCheck);
    Project^.ActSymSizeType:=GetSizeTypeField(SizeVal,FEditOptions,ScaleIndependendCheck);
  end;
end;

procedure TSymbolRollupForm.CheckSize;
begin
     if ScaleIndependendCheck.Checked then begin
        if SizeVal.Value <> 0 then SizeVal.Units:=muMillimeters;
        SizeVal.UseableUnits:=32;
        SizeVal.MinValue.Value:=1;
        SizeVal.MinValue.Units:=muMillimeters;
        SizeVal.MaxValue.Units:=muMillimeters;
     end
     else begin
        if SizeVal.Value <> 0 then SizeVal.Units:=muDrawingUnits;
        SizeVal.UseableUnits:=2;
        SizeVal.MinValue.Value:=0.1;
        SizeVal.MinValue.Units:=muDrawingUnits;
        SizeVal.MaxValue.Units:=muDrawingUnits;
     end;
     if SizeVal.Value = 0 then begin
        ScaleIndependendCheck.Checked:=False;
        ScaleIndependendCheck.Enabled:=False;
     end
     else begin
        ScaleIndependendCheck.Enabled:=True;
     end;
end;

procedure TSymbolRollupForm.UpdateSymbols;
begin
  if Project=NIL then Exit;
  FSymInfo.UpdateProjLib;              { Projektsymbolinfo neumachen }
  UpdateLibChange;
  Project^.ActSymLib:=FSymInfo.LibName;
  UpdateEnabled;
end;

procedure TSymbolRollupForm.SymbolListActionExecute(Sender: TObject);
begin
  SetListMosaic(FALSE);
end;

procedure TSymbolRollupForm.SymbolMosaicActionExecute(Sender: TObject);
begin
  SetListMosaic(TRUE);
end;

procedure TSymbolRollupForm.SetListMosaic(AMosaic: Boolean);
var NewColCount  : Integer;
begin
  with SymbolList do begin
    if AMosaic then NewColCount:=4
    else NewColCount:=1;
    if AMosaic then MosaicMenu.Checked:=TRUE
    else ListMenu.Checked:=TRUE;
    CellWidth:=(ClientWidth-1) Div NewColCount;
    ColCount:=NewColCount;
    ShowHint:=AMosaic;
  end;
end;

procedure TSymbolRollupForm.SymbolListDblClick(Sender: TObject);
var SendStr      : String;
    OldSymIndex  : Integer;
    SendSName    : String;
    SendSIndex   : LongInt;
begin
  if Project<>NIL then begin
    if FSendSymInfo then begin
      if not FWasVisible then RollupToolWindow.Visible:=FALSE;
      FWasVisible:=FALSE;
      FSendSymInfo:=FALSE;
      if FSymInfo<>NIL then begin
        FSendSymIndex:=SymbolList.ItemIndex;
        if FSendSymIndex<>-1 then begin
          OldSymIndex:=FSymInfo.SymIndex;
          FSymInfo.SymIndex:=FSendSymIndex;
          UpdateSymChange;
          SendSName:=FSymInfo.Sym.GetName;
          SendSIndex:=FSymInfo.Sym.Index-600000000;
          FSymInfo.SymIndex:=OldSymIndex;
          UpdateSymChange;
        end
        else begin
          SendSName:='';
          SendSIndex:=-1;
        end;
        {$IFNDEF WMLT}
        SendStr:='[SYM]['+IntToStr(SendSIndex)+']['+SendSName+']';
        DDEHandler.SendString(SendStr);
        DDEHandler.SendString('[END][]');
        DDEHandler.SetAnswerMode(FALSE);
        {$ENDIF}
      end;
      FSendSymIndex:=-1;
    end
    else MenuFunctions['DrawSymbol'].Execute; //Project^.SetActualMenu(mn_Symbol);
  end;
end;

{++ IDB} // It is necessary for undelete of symbols.
Function TSymbolRollupForm.GetSymLibInfo: TSymLibInfo;
Begin
     RESULT := SELF.FSymInfo;
End;
{-- IDB}
procedure TSymbolRollupForm.SelectSymMenuClick(Sender: TObject);
var
i: Integer;
ALayer: PLayer;

  procedure DoAll(AItem: PIndex);
  var
  SItem: PIndex;
  AGroup: PSGroup;
  begin
       if AItem^.GetObjType = ot_Symbol then begin
          SItem:=ALayer^.IndexObject(Project^.PInfo,AItem);
          if SItem <> nil then begin
             AGroup:=PSymbol(SItem)^.GetSymbol(Project^.PInfo);
             if AGroup <> nil then begin
               try
                  if AGroup^.Index = Project^.ActualSym^.Index then begin
                     ALayer^.Select(Project^.PInfo,AItem);
                  end;
               except
                  PSymbol(SItem)^.SetSymPtr(Project^.PInfo^.Symbols);
                  if PSymbol(SItem)^.SymPtr <> nil then begin
                     PSymbol(SItem)^.SetState(sf_SymName,False);
                     if PSymbol(SItem)^.SymPtr^.Index = Project^.ActualSym^.Index then begin
                        ALayer^.Select(Project^.PInfo,AItem);
                     end;
                  end;
               end;
             end;
          end;
       end;
  end;

begin
  if Project <> nil then begin
      if Project^.ActualSym <> nil then begin
         for i:=1 to Project^.Layers^.LData^.Count-1 do begin
             ALayer:=Project^.Layers^.LData^.At(i);
             if not ALayer^.GetState(sf_LayerOff) then begin
                ALayer^.Data^.ForEach(@DoAll);
             end;
         end;
         if (Project^.Layers^.SelLayer^.SelInfo.Count=1) and (Project^.Layers^.SelLayer^.SelInfo.SelectedType=ot_Symbol) then begin
            {$IFNDEF WMLT}
            ProcShowAllSel(Project);
            {$ENDIF}
         end;
      end;
  end;
end;

procedure TSymbolRollupForm.DeleteUnusedClick(Sender: TObject);
begin
     if MsgBox(WingisMainForm.Handle,79004,mb_IconQuestion or mb_YesNo,'') = id_Yes then begin
        Project^.DeleteUnusedSymbols;
     end;
end;

{$ENDIF} // <----------------- AXDLL

end.
