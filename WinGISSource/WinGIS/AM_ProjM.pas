Unit Am_ProjM;

Interface

Uses WinProcs,WinTypes,AM_Def,Classes,GrTools;

Const idlMeasureFrom2nd = 100;
      idlCreateLine     = 101;
      idoMeasureFrom2nd = 102;
      idoCreateLines    = 104;

Type PConstructOpt      = ^TConstructOpt;
     TConstructOpt      = Record
       lMeasureFrom2nd  : Boolean;
       lCreateLine      : Boolean;
       oMeasureFrom2nd  : Boolean;
       oCreateLines     : Boolean;
     end;

     PConstructOptDlg   = ^TConstructOptDlg;
     TConstructOptDlg   = Class(TGrayDialog)
      Public
       Data             : PConstructOpt;
       Constructor Init(AParent:TComponent;AData:PConstructOpt);
       Function    CanClose:Boolean; override;
       Procedure   SetupWindow; override;
     end;

Function CutCircles(Const Middle1:TDPoint;Const Radius1:Double;Const Middle2:TDPoint;
   Const Radius2:Double;var Cut1:TDPoint;var Cut2:TDPoint):Integer;

Procedure ProcMarkCircleCut(AProj:Pointer;var AClick,AMouse:TDPoint);

Procedure ProcMarkCircle(AProj:Pointer;var AClick,AMouse:TDPoint);

Procedure ProcMarkLinearOrtho(AProj:Pointer;var AClick,AMouse:TDPoint);

Procedure ProcMarkLinePar(AProj:Pointer;var AClick,AMouse:TDPoint);

Procedure ProcMarkAngle(AProj:Pointer;var AClick,AMouse:TDPoint);

Function RectWithPixelSize(DC:THandle;X:LongInt;Y:LongInt;PixSize:Integer):TRect;

Procedure SetConstructOptions(AProj:Pointer);

Procedure ReadConstructOpt;

Procedure WriteConstructOpt;

Procedure ProcCalcPointOnCircle(AProj:Pointer;AMouse:TDPoint;var APos:TDPoint);


{++ Brovak BUG 105 BUILD 130}
Procedure ChangeDrawRubberBoxMode(AProj: Pointer);
Procedure ChangeDraw3PointMode(AProj: Pointer);
Procedure RubberBoxCalc(AProj: Pointer;AMousePoint:TDPOint;Flag:boolean;Count:integer);
Procedure ThreePointsCalc(AProj: Pointer;AMousePos:TDPOint;Flag:boolean;Count:integer);
Procedure RubberBoxDraw(AProj: Pointer;AMousePoint:TDPOint);
Procedure ThreePointsDraw(AProj: Pointer;AMousePoint:TDPOint);
{--Brovak}



var ConstructOpt   : TConstructOpt;
    CMarkSnapped   : Boolean;
    XMove1         : Double;
    YMove1         : Double;
    XMove2         : Double;
    YMove2         : Double;

Implementation

Uses AM_Proj,AM_Point,AM_Poly,AM_Ini,ResDlg,SysUtils,UserIntf,multilng,dialogs,menufn,AM_Index,AM_ProjO,AM_Meas;

Function CutCircles
   (
   Const Middle1        : TDPoint;
   Const Radius1        : Double;
   Const Middle2        : TDPoint;
   Const Radius2        : Double;
   var Cut1             : TDPoint;
   var Cut2             : TDPoint
   )
   : Integer;
  var Sab               : Double;
      Gamma             : Double;
      P                 : Double;
      H                 : Double;
      Beta              : Double;
  begin
    Sab:=Middle1.Dist(Middle2);
    if (Sab>Abs(Radius2-Radius1))
        and (Sab<Radius1+Radius2) then begin
      P:=(Sqr(Sab)+Sqr(Radius1)-Sqr(Radius2))/2/Sab;
      Beta:=P/Sab;
      Gamma:=Sqrt(Sqr(Radius1/Sab)-Sqr(Beta));
      Cut1.Init(LimitToLong(Middle1.X+Beta*(Middle2.X-Middle1.X)-Gamma*(Middle2.Y-Middle1.Y)),
          LimitToLong(Middle1.Y+Beta*(Middle2.Y-Middle1.Y)+Gamma*(Middle2.X-Middle1.X)));
      Cut2.Init(LimitToLong(Middle1.X+Beta*(Middle2.X-Middle1.X)+Gamma*(Middle2.Y-Middle1.Y)),
          LimitToLong(Middle1.Y+Beta*(Middle2.Y-Middle1.Y)-Gamma*(Middle2.X-Middle1.X)));
      if Cut1.IsDiff(Cut2) then Result:=2
      else Result:=1;
    end
    else CutCircles:=0;
  end;

Procedure ProcMarkCircleCut
   (
   AProj           : Pointer;
   var AClick      : TDPoint;
   var AMouse      : TDPoint
   );
  var Point1       : TDPoint;
      Point2       : TDPoint;
      ADist        : Double;
      BDist        : Double;
      APixel       : PPixel;
  begin
    with PProj(AProj)^ do case MarkState of
      0: begin
          Line^.StartIt(PInfo,AClick,FALSE);
          SetStatusText(GetLangText(11215),FALSE);
          MarkState:=1;
        end;
      1: begin
          MsgMouseMove(AClick);
          SetStatusText(Format(GetLangText(11220),[Line^.StartPos.Dist(AMouse)/100]),FALSE);
          SelCircle^.StartIt(PInfo,Line^.StartPos,FALSE);
          MarkState:=2;
          MsgMouseMove(AMouse);
        end;
      2: begin
          MsgMouseMove(AClick);
          SetStatusText(Format(GetLangText(11221),[Line^.StartPos.Dist(AMouse)/100]),FALSE);
          InpCircle^.StartIt(PInfo,Line^.LastPos,FALSE);
          MarkState:=3;
          MsgMouseMove(AMouse);
        end;
      3: begin
          MsgMouseMove(AClick);
          BreakInput(FALSE,FALSE);
          case CutCircles(SelCircle^.StartPos,SelCircle^.GetRadius,
              InpCircle^.StartPos,InpCircle^.GetRadius,Point1,Point2) of
            0: MsgBox(Parent.Handle,11219,mb_Ok or mb_IconExclamation,'');
            1: begin
                   if bConstrDim then begin
                      MesPoint1.Init(SelCircle^.StartPos.X,SelCircle^.StartPos.Y);
                      MesPoint2.Init(Point1.X,Point1.Y);
                      ProcInsertMeasure(AProj,Point1,True);

                      MesPoint1.Init(InpCircle^.StartPos.X,InpCircle^.StartPos.Y);
                      MesPoint2.Init(Point1.X,Point1.Y);
                      ProcInsertMeasure(AProj,Point1,True);
                   end;
                   APixel:=New(PPixel,Init(Point1));
                   if InsertObject(APixel,FALSE) then begin
                      {$IFNDEF WMLT}
                      {$IFNDEF AXDLL} // <----------------- AXDLL
                      ProcDBSendObject(PProj(AProj),APixel,'EIN');
                      {$ENDIF} // <----------------- AXDLL
                      {$ENDIF}
                   end;
               end;
            2: begin
                   ADist:=Point1.Dist(AMouse);
                   BDist:=Point2.Dist(AMouse);
                   if bConstrDim then begin
                      if (ADist < BDist) then begin
                         MesPoint1.Init(InpCircle^.StartPos.X,InpCircle^.StartPos.Y);
                         MesPoint2.Init(Point1.X,Point1.Y);
                         ProcInsertMeasure(AProj,Point1,True);
                      end
                      else begin
                         MesPoint1.Init(InpCircle^.StartPos.X,InpCircle^.StartPos.Y);
                         MesPoint2.Init(Point2.X,Point2.Y);
                         ProcInsertMeasure(AProj,Point1,True);
                      end;
                      if (ADist < BDist) then begin
                         MesPoint1.Init(SelCircle^.StartPos.X,SelCircle^.StartPos.Y);
                         MesPoint2.Init(Point1.X,Point1.Y);
                         ProcInsertMeasure(AProj,Point1,True);
                      end
                      else begin
                         MesPoint1.Init(SelCircle^.StartPos.X,SelCircle^.StartPos.Y);
                         MesPoint2.Init(Point2.X,Point2.Y);
                         ProcInsertMeasure(AProj,Point1,True);
                      end;
                   end;
                   if (ADist < BDist) then begin
                      APixel:=New(PPixel,Init(Point1));
                      if InsertObject(APixel,FALSE) then begin
                         {$IFNDEF WMLT}
                         {$IFNDEF AXDLL} // <----------------- AXDLL
                         ProcDBSendObject(PProj(AProj),APixel,'EIN');
                         {$ENDIF} // <----------------- AXDLL
                         {$ENDIF}
                      end;
                   end
                   else begin
                        APixel:=New(PPixel,Init(Point2));
                        if InsertObject(APixel,FALSE) then begin
                           {$IFNDEF WMLT}
                           {$IFNDEF AXDLL} // <----------------- AXDLL
                           ProcDBSendObject(PProj(AProj),APixel,'EIN');
                           {$ENDIF} // <----------------- AXDLL
                           {$ENDIF}
                        end;
                   end;
               end;
          end;
        end;
    end;
  end;

Procedure ProcMarkCircle
   (
   AProj           : Pointer;
   var AClick      : TDPoint;
   var AMouse      : TDPoint
   );
  var Point1       : TDPoint;
      Point2       : TDPoint;
      BPos         : TDPoint;
      ALine        : PPoly;
      p,pr         : TGrPoint;
      APixel       : PPixel;
  begin
    CMarkSnapped:=False;
    with PProj(AProj)^ do case MarkState of
      0: begin
          SelCircle^.StartIt(PInfo,AClick,FALSE);
          Line^.StartIt(PInfo,AClick,FALSE);
          MarkState:=1;
        end;
      1: begin
          MsgMouseMove(AMouse);
          MarkState:=2;
        end;
      2: begin
          BreakInput(FALSE,FALSE);
          SelCircle^.EndIt(PInfo);
          Line^.EndIt(PInfo);
          p.x:=Line^.StartPos.x;
          p.y:=Line^.StartPos.y;
          pr:=MovedPointAngle(p,SelCircle^.GetRadius,Line^.StartPos.CalculateAngle(Line^.LastPos));
          BPos.Init(Trunc(pr.x),Trunc(pr.y));
          APixel:=New(PPixel,Init(BPos));
          if InsertObject(APixel,FALSE) then begin
             {$IFNDEF WMLT}
             {$IFNDEF AXDLL} // <----------------- AXDLL
             ProcDBSendObject(PProj(AProj),APixel,'EIN');
             {$ENDIF} // <----------------- AXDLL
             {$ENDIF}
          end;
          if ConstructOpt.lCreateLine then begin
              ALine:=New(PPoly,Init);
              ALine^.InsertPoint(Line^.StartPos);
              MesPoint1.Init(Line^.StartPos.X,Line^.StartPos.Y);
              ALine^.InsertPoint(BPos);
              MesPoint2.Init(BPos.X,BPos.Y);
              if bConstrDim then begin
                 ProcInsertMeasure(AProj,AClick,True);
              end
              else begin
                 InsertObject(ALine,FALSE)
              end;
          end;
          MarkState:=0;
        end;
    end;
  end;

Procedure ProcCalcPointOnCircle(AProj:Pointer;AMouse:TDPoint;var APos:TDPoint);
var
p,pr         : TGrPoint;
AIndex       : PIndex;
i            : Integer;
p1,p2        : TDPoint;
gp1,gp2      : TGrPoint;
gpm          : TGrPoint;
gpr1,gpr2    : TGrPoint;
m            : TGrPoint;
res          : Integer;
d,dt         : Double;
PointList    : TList;
begin
    with PProj(AProj)^ do begin
         p.x:=Line^.StartPos.x;
         p.y:=Line^.StartPos.y;
         pr:=MovedPointAngle(p,SelCircle^.GetRadius,Line^.StartPos.CalculateAngle(APos));
         APos.Init(Trunc(pr.x),Trunc(pr.y));
         if SnapState then begin
            d:=0;
            PointList:=TList.Create;
            try
               AIndex:=Layers^.TopLayer^.SearchByPoint(PInfo,APos,[ot_Poly,ot_CPoly]);
               if (AIndex <> nil) and (PointList <> nil) then begin
                  m.x:=Line^.StartPos.x;
                  m.y:=Line^.StartPos.y;
                  for i:=0 to PPoly(AIndex)^.Data^.Count-2 do begin
                      p1.Init(TDPoint(PPoly(AIndex)^.Data^.At(i)^).x,TDPoint(PPoly(AIndex)^.Data^.At(i)^).y);
                      p2.Init(TDPoint(PPoly(AIndex)^.Data^.At(i+1)^).x,TDPoint(PPoly(AIndex)^.Data^.At(i+1)^).y);
                      gp1.x:=p1.x;
                      gp1.y:=p1.y;
                      gp2.x:=p2.x;
                      gp2.y:=p2.y;
                      gpr1.x:=gp1.x;
                      gpr1.y:=gp1.y;
                      gpr2.x:=gp2.x;
                      gpr2.y:=gp2.y;
                      res:=CircleLineIntersection(m,SelCircle^.GetRadius,gp1,gp2,gpr1,gpr2);
                      if res > 0 then begin
                         if gpr1.x <> gp1.x then PointList.Add(Pointer(Trunc(gpr1.x)));
                         if gpr1.y <> gp1.y then PointList.Add(Pointer(Trunc(gpr1.y)));
                         if gpr2.x <> gp2.x then PointList.Add(Pointer(Trunc(gpr2.x)));
                         if gpr2.y <> gp2.y then PointList.Add(Pointer(Trunc(gpr2.y)));
                      end;
                  end;
                  gpm.x:=APos.x;
                  gpm.y:=APos.y;
                  for i:=0 to (PointList.Count div 2) - 1 do begin
                      gp1.x:=Integer(PointList[i*2]);
                      gp1.y:=Integer(PointList[i*2+1]);
                      dt:=GrPointDistance(gp1,gpm);
                      if (d = 0) or (dt < d) then begin
                         d:=dt;
                         APos.Init(Trunc(gp1.x),Trunc(gp1.y));
                         CMarkSnapped:=True;
                      end;
                  end;
               end;
            finally
               PointList.Free;
            end;
            if (d = 0) and (CMarkSnapped) then begin
               APos.Init(Trunc(Line^.LastPos.x),Trunc(Line^.LastPos.y));
            end;
         end
         else begin
            CMarkSnapped:=False;
         end;
    end;
end;

////////////////////////////////////////

Procedure ProcMarkLinePar(AProj:Pointer;var AClick,AMouse:TDPoint);
  var NDist        : Double;
      ALine        : PPoly;
      BPos         : TDPoint;
      APoint       : TDPoint;
      BPoint       : TDPoint;
      APixel       : PPixel;
  begin
    with PProj(AProj)^ do case MarkState of
      0: begin
          Line^.StartIt(PInfo,AClick,FALSE);
          MarkState:=1;
        end;
      1: begin
          MsgMouseMove(AClick);
          SetStatusText(Format(GetLangText(11205),[0.0]),FALSE);
          Line1^.StartIt(PInfo,AClick,FALSE);
          MarkState:=2;
          MsgMouseMove(AMouse);
        end;
      2: begin
          MsgMouseMove(AClick);
          XMove1:=Line1.LastPos.X-Line1.StartPos.X;
          YMove1:=Line1.LastPos.Y-Line1.StartPos.Y;
          Line1.EndIt(PInfo);

          APoint.Init(Line^.StartPos.X,Line^.StartPos.Y);
          BPoint.Init(Line^.LastPos.X,Line^.LastPos.Y);
          APoint.Move(XMove1,YMove1);
          BPoint.Move(XMove1,YMove1);
          Line^.MoveIt(PInfo,APoint,BPoint);

          MarkState:=3;
          SetStatusText (GetLangText (11203), TRUE);
          MsgMouseMove(AClick);
        end;
      3: begin
          Line2^.StartIt(PInfo,AClick,FALSE);
          MarkState:=4;
        end;
      4: begin
          MsgMouseMove(AClick);
          SetStatusText(Format(GetLangText(11205),[0.0]),FALSE);
          Line3^.StartIt(PInfo,AClick,FALSE);
          MarkState:=5;
          MsgMouseMove(AMouse);
        end;
      5: begin
          MsgMouseMove(AClick);

          XMove2:=Line3.LastPos.X-Line3.StartPos.X;
          YMove2:=Line3.LastPos.Y-Line3.StartPos.Y;

          APoint.Init(Line2^.StartPos.X,Line2^.StartPos.Y);
          BPoint.Init(Line2^.LastPos.X,Line2^.LastPos.Y);
          APoint.Move(XMove2,YMove2);
          BPoint.Move(XMove2,YMove2);
          Line2^.MoveIt(PInfo,APoint,BPoint);

          BreakInput(FALSE,FALSE);

          if PInfo^.LineCut2(Line^.StartPos,Line^.LastPos,Line2^.StartPos,Line2^.LastPos,APoint) then begin
              if bConstrDim then begin
                 MesPoint1.Init(APoint.X,APoint.Y);
                 BPoint.Init(APoint.X,APoint.Y);
                 BPoint.Move(-XMove1,-YMove1);
                 MesPoint2.Init(BPoint.X,BPoint.Y);
                 ProcInsertMeasure(AProj,AClick,True);

                 MesPoint1.Init(APoint.X,APoint.Y);
                 BPoint.Init(APoint.X,APoint.Y);
                 BPoint.Move(-XMove2,-YMove2);
                 MesPoint2.Init(BPoint.X,BPoint.Y);
                 ProcInsertMeasure(AProj,AClick,True);
              end;
              APixel:=New(PPixel,Init(APoint));
              if InsertObject(APixel,FALSE) then begin
                 {$IFNDEF WMLT}
                 {$IFNDEF AXDLL} // <----------------- AXDLL
                 ProcDBSendObject(PProj(AProj),APixel,'EIN');
                 {$ENDIF} // <----------------- AXDLL
                 {$ENDIF}
              end;
          end;
        end;
    end;
  end;

////////////////////////////////////////////////

Procedure ProcMarkLinearOrtho
   (
   AProj           : Pointer;
   var AClick      : TDPoint;
   var AMouse      : TDPoint
   );
  var NDist        : Double;
      ALine        : PPoly;
      BPos         : TDPoint;
      APixel       : PPixel;
  begin
    with PProj(AProj)^ do case MarkState of
      0: begin
          Line^.StartIt(PInfo,AClick,FALSE);
          SetStatusText(Format(GetLangText(11204),[0.0]),FALSE);
          MarkState:=1;
        end;
      1: begin
          MsgMouseMove(AClick);
          if ActualMen=mn_OrthoMark then SetStatusText(Format(GetLangText(11206),[0.0,0.0]),FALSE)
          else SetStatusText(Format(GetLangText(11205),[0.0]),FALSE);
          Line1^.StartIt(PInfo,AClick,FALSE);
          MarkState:=2;
          MsgMouseMove(AMouse);
        end;
      2: begin
          MsgMouseMove(AClick);
          BreakInput(FALSE,FALSE);
          if ActualMen=mn_DistMark then begin
            PInfo^.NormalDistance(@Line^.StartPos,@Line^.LastPos,@AClick,NDist,AClick);
            APixel:=New(PPixel,Init(AClick));
            if InsertObject(APixel,FALSE) then begin
                 {$IFNDEF WMLT}
                 {$IFNDEF AXDLL} // <----------------- AXDLL
                 ProcDBSendObject(PProj(AProj),APixel,'EIN');
                 {$ENDIF} // <----------------- AXDLL
                 {$ENDIF}
            end;
            if ConstructOpt.lCreateLine then begin
              ALine:=New(PPoly,Init);

              if ConstructOpt.lMeasureFrom2nd then begin
                 ALine^.InsertPoint(Line^.LastPos);
                 MesPoint1.Init(Line^.LastPos.X,Line^.LastPos.Y);
              end
              else begin
                 ALine^.InsertPoint(Line^.StartPos);
                 MesPoint1.Init(Line^.StartPos.X,Line^.StartPos.Y);
              end;
              ALine^.InsertPoint(AClick);
              MesPoint2.Init(AClick.X,AClick.Y);

              if bConstrDim then begin
                 ProcInsertMeasure(AProj,AClick,True);
              end
              else begin
                 InsertObject(ALine,FALSE)
              end;
            end;
          end
          else begin
            APixel:=New(PPixel,Init(AClick));
            if InsertObject(APixel,FALSE) then begin
                 {$IFNDEF WMLT}
                 {$IFNDEF AXDLL} // <----------------- AXDLL
                 ProcDBSendObject(PProj(AProj),APixel,'EIN');
                 {$ENDIF} // <----------------- AXDLL
                 {$ENDIF}
            end;
            PInfo^.NormalDistance(@Line^.StartPos,@Line^.LastPos,@AClick,NDist,BPos);
            if ConstructOpt.oCreateLines then begin
              ALine:=New(PPoly,Init);

              if ConstructOpt.oMeasureFrom2nd then begin
                 ALine^.InsertPoint(Line^.LastPos);
                 MesPoint1.Init(Line^.LastPos.X,Line^.LastPos.Y);
              end
              else begin
                 ALine^.InsertPoint(Line^.StartPos);
                 MesPoint1.Init(Line^.StartPos.X,Line^.StartPos.Y);
              end;
              ALine^.InsertPoint(BPos);
              MesPoint2.Init(BPos.X,BPos.Y);

              if bConstrDim then begin
                 ProcInsertMeasure(AProj,AClick,True);
              end
              else begin
                 InsertObject(ALine,FALSE)
              end;

              ALine:=New(PPoly,Init);
              ALine^.InsertPoint(BPos);
              MesPoint1.Init(BPos.X,BPos.Y);
              ALine^.InsertPoint(AClick);
              MesPoint2.Init(AClick.X,AClick.Y);

              if bConstrDim then begin
                 ProcInsertMeasure(AProj,AClick,True);
              end
              else begin
                 InsertObject(ALine,FALSE)
              end;
            end;
          end;
        end;
    end;
  end;

Procedure ProcMarkAngle
   (
   AProj           : Pointer;
   var AClick      : TDPoint;
   var AMouse      : TDPoint
   );
  var
  APixel           : PPixel;
  begin
    with PProj(AProj)^ do case MarkState of
       0: begin
           Line^.StartIt(PInfo,AClick,FALSE);
           SetStatusText(Format(GetLangText(11218),[0.0,0.0]),FALSE);
           MarkState:=1;
         end;
       1: begin
           MsgMouseMove(AClick);
           Line1^.StartIt(PInfo,Line^.StartPos,FALSE);
           SetStatusText(Format(GetLangText(11237),[0.0,0.0]),FALSE);
           MarkState:=2;
           MsgMouseMove(AMouse);
         end;
       2: begin
           MsgMouseMove(AClick);
           Line2^.StartIt(PInfo,Line^.LastPos,FALSE);
           SetStatusText(Format(GetLangText(11238),[0.0,0.0]),FALSE);
           MarkState:=3;
           MsgMouseMove(AMouse);
         end;
       3: begin
           MsgMouseMove(AClick);
           BreakInput(FALSE,FALSE);
           if PInfo^.LineCut2(Line1^.StartPos,Line1^.LastPos,Line2^.StartPos,Line2^.LastPos,AClick) then begin
              if bConstrDim then begin
                 MesPoint1.Init(Line1^.StartPos.X,Line1^.StartPos.Y);
                 MesPoint2.Init(Line2^.StartPos.X,Line2^.StartPos.Y);
                 ProcInsertMeasure(AProj,AClick,True);

                 MesPoint1.Init(Line1^.StartPos.X,Line1^.StartPos.Y);
                 MesPoint2.Init(AClick.X,AClick.Y);
                 ProcInsertMeasure(AProj,AClick,True);

                 MesPoint1.Init(Line2^.StartPos.X,Line2^.StartPos.Y);
                 MesPoint2.Init(AClick.X,AClick.Y);
                 ProcInsertMeasure(AProj,AClick,True);
              end;
              APixel:=New(PPixel,Init(AClick));
              if InsertObject(APixel,FALSE) then begin
                 {$IFNDEF WMLT}
                 {$IFNDEF AXDLL} // <----------------- AXDLL
                 ProcDBSendObject(PProj(AProj),APixel,'EIN');
                 {$ENDIF} // <----------------- AXDLL
                 {$ENDIF}
              end;
           end;
         end;
     end;
   end;

Function RectWithPixelSize
   (
   DC              : THandle;
   X               : LongInt;
   Y               : LongInt;
   PixSize         : Integer
   )
   : TRect;
  begin
    with Result do begin
      Left:=X;
      Top:=Y;
      LpToDp(DC,Result,1);
      Right:=Left+PixSize+1;
      Bottom:=Top+PixSize+1;
      Left:=left-PixSize;
      Top:=Top-PixSize;
      DpToLp(DC,Result,2);
    end;
  end;

{=============================================================================}
{ TConstructOptDlg                                                            }
{=============================================================================}

Constructor TConstructOptDlg.Init
   (
   AParent         : TComponent;
   AData           : PConstructOpt
   );
  begin
    inherited Init(AParent,'ConstructOptDlg');
    Data:=AData;
  end;

Procedure TConstructOptDlg.SetupWindow;
  begin
    inherited SetupWindow;
    with Data^ do begin
      if lMeasureFrom2nd then CheckDlgButton(Handle,idlMeasureFrom2nd,1);
      if lCreateLine then CheckDlgButton(Handle,idlCreateLine,1);
      if oMeasureFrom2nd then CheckDlgButton(Handle,idoMeasureFrom2nd,1);
      if oCreateLines then CheckDlgButton(Handle,idoCreateLines,1);
    end;
  end;

Function TConstructOptDlg.CanClose
   : Boolean;
  begin
    Result:=TRUE;
    with Data^ do begin
      lMeasureFrom2nd:=IsDlgButtonChecked(Handle,idlMeasureFrom2nd)=1;
      lCreateLine:=IsDlgButtonChecked(Handle,idlCreateLine)=1;
      oMeasureFrom2nd:=IsDlgButtonChecked(Handle,idoMeasureFrom2nd)=1;
      oCreateLines:=IsDlgButtonChecked(Handle,idoCreateLines)=1;
    end;
  end;

Procedure SetConstructOptions
   (
   AProj           : Pointer
   );
  var Dialog       : TGrayDialog;
  begin
    if ExecDialog(TConstructOptDlg.Init(PProj(AProj)^.Parent,@ConstructOpt))=id_OK then
        WriteConstructOpt;
  end;

Procedure ReadConstructOpt;
  begin
    with ConstructOpt do begin
      lMeasureFrom2nd:=IniFile^.ReadBoolean('Construct','linMeasureFrom2nd',False);
      lCreateLine:=IniFile^.ReadBoolean('Construct','linCreateLine',False);
      oMeasureFrom2nd:=IniFile^.ReadBoolean('Construct','ortMeasureFrom2nd',False);
      oCreateLines:=IniFile^.ReadBoolean('Construct','ortCreateLines',False);
    end;
  end;

Procedure WriteConstructOpt;
  begin
    with ConstructOpt do begin
      IniFile^.WriteBoolean('Construct','linMeasureFrom2nd',lMeasureFrom2nd);
      IniFile^.WriteBoolean('Construct','linCreateLine',lCreateLine);
      IniFile^.WriteBoolean('Construct','ortMeasureFrom2nd',oMeasureFrom2nd);
      IniFile^.WriteBoolean('Construct','ortCreateLines',oCreateLines);
    end;
  end;

{+++++++++ Brovak BUG 105 BUILD#130 ---------}
// Set on/off mode for easy drawing rectangle with 3 points
Procedure ChangeDraw3PointMode(AProj: Pointer);
Begin
            PPRoj(AProj).Line.EndIt(PPRoj(AProj).Pinfo);
            If  PPRoj(AProj).SelRect^.IsOn then
          begin;
           Pproj(AProj).NewPoly^.EndIt(PPRoj(AProj).Pinfo);
           PPRoj(AProj).NewPoly^.PointInsert(PPRoj(AProj).PInfo,PPRoj(AProj).SelRect^.StartPos,TRUE);
           PPRoj(AProj).NewPoly.StartPos:=  PPRoj(AProj).SelRect^.StartPos;
           PPRoj(AProj).SelRect^.Endit(PPRoj(AProj).Pinfo);
           end;

     PPRoj(AProj).bDraw3Points := NOT PPRoj(AProj).bDraw3Points;
      If PPRoj(AProj).bDraw3Points=true
        then
         begin;
            If PPRoj(AProj).NewPoly.Data.Count=3 then //if all point already set
              if MessageDialog(GetLangText(11293),mtWarning,[mbYes,mbNo],0) = idNo
                 then
                  begin;
                   PPRoj(AProj).bDraw3Points:=false;
                   MenuFunctions['DrawRectBy3Points'].Checked:=true;
                   Exit;
                 end;
               if PPRoj(AProj).NewPoly.Data.Count=3 then
             ThreePointsCalc(AProj,PDPOINT(PProj(AProj).NewPoly.Data.At(2))^,false,PPRoj(AProj).NewPoly.Data.Count);
              if PPRoj(AProj).NewPoly.Data.Count=2 then
              ThreePointsCalc(AProj,PProj(AProj).AOldMousePos,false,PPRoj(AProj).NewPoly.Data.Count);

         end
         else
         Pproj(AProj).NewPolyP^.EndIt(PProj(AProj).PInfo);
End;

// draw poly or rubberbox-image of future rectangle
Procedure ThreePointsCalc(AProj: Pointer;AMousePos:TDPOint;Flag:boolean;Count:integer);
var
   NDist: Double;
   NPoint: TDPoint;
   ABaseStartPoint, ABaseEndPoint: PDPoint;

 //Draw future rectangle as pseudo rubberbox
 Procedure DrawRubRect(x1,x2,x3,x4,y1,y2,y3,y4:longint);
  var TmpPoint:TDPOINT;
  begin;

   Pproj(AProj).NewPolyP^.EndIt(PProj(AProj).PInfo);
   PProj(Aproj).NewPolyP^.LastPos:=PProj(Aproj).NewPolyP^.StartPos;
   TmpPoint.init(x2,y2);
    PProj(Aproj).NewPolyP^.PointInsert(PProj(AProj).PInfo,TmpPoint,true);
   TmpPoint.X:=x3; TmpPOint.Y:=y3;
    PProj(Aproj).NewPolyP^.PointInsert(PProj(AProj).PInfo,TmpPoint,true);
   TmpPoint.X:=x4; TmpPOint.Y:=y4;
    PProj(Aproj).NewPolyP^.PointInsert(PProj(AProj).PInfo,TmpPoint,true);
   TmpPoint.X:=x1; TmpPOint.Y:=y1;
    PProj(Aproj).NewPolyP^.PointInsert(PProj(AProj).PInfo,TmpPoint,true);

   PProj(AProj)^.NewPolyP^.Closed:=True;
   PProj(AProj)^.NewPolyP^.ShowAreaStatus(PProj(AProj).PInfo);
   PProj(AProj)^.NewPolyP^.Closed:=False;
  end;

 //Defines there  Mouse are situated and Call drawing procedure with parameters
 Procedure Check;
 var dx,dy:longint;
  begin;

     ABaseStartPoint := PProj(AProj).NewPoly.Data.At(0);
     ABaseEndPoint := PProj(AProj).NewPoly.Data.At(1);
     PProj(AProj).PInfo^.NormalDistance(ABaseStartPoint, ABaseEndPoint, @AMousePos, NDist, NPoint);

     Dx:=NPoint.X-AMousePos.X;
     Dy:=NPoint.Y-AMousePos.Y;

 //     Npoint inside in pseudo rectangle
   if ((Npoint.X>=ABaseStartPoint.X) and (Npoint.X<=ABaseEndPoint.X))
                                    or
      ((Npoint.X>=ABaseEndPoint.X) and (Npoint.X<=ABaseStartPoint.X))

     then
      begin;
        DrawRubRect(ABaseStartPoint.X,ABaseEndPoint.X,ABaseEndPoint.X-dx,ABaseStartPoint.X-dx,
                    ABaseStartPoint.Y,ABaseEndPoint.Y,ABaseEndPoint.Y-dy,ABaseStartPoint.Y-dy );
        Exit;
      end //if
   else  //NPoint outside
             begin;
                if ((ABaseEndPoint.X>ABaseStartPoint.X) and (NPoint.X>ABaseEndPoint.X))
                                                        or
                   ((ABaseEndPoint.X<ABaseStartPoint.X) and (NPoint.X<ABaseEndPOint.X))
                     then
                      begin;
                        DrawRubRect(ABaseStartPoint.X,NPoint.X,NPoint.X-dx,ABaseStartPoint.X-dx,
                        ABaseStartPoint.Y,NPoint.Y,NPoint.Y-dy,ABaseStartPoint.Y-dy );
                       Exit
                      end;

                if ((ABaseEndPoint.X>ABaseStartPoint.X) and (NPoint.X<ABaseStartPoint.X))
                     or
                   ((ABaseEndPoint.X<ABaseStartPoint.X) and (NPoint.X>ABaseEndPOint.X))
                     then
                      begin;
                        DrawRubRect(NPoint.X,ABaseEndPoint.X,ABaseEndPoint.X-dx,NPoint.X-dx,
                        NPoint.Y,ABaseEndPoint.Y,ABaseEndPoint.Y-dy,NPoint.Y-dy );
                       Exit
                      end;

             end; //else

  end; //procedure  Check


   Procedure InsertRect; //insert rectangle to project
    var i:integer;
    begin;
     Pproj(AProj).NewPoly.EndIt(PProj(AProj).Pinfo);
      for i:=0 to 3 do
        PProj(AProj).NewPoly^.PointInsert(PProj(AProj).Pinfo,TDPoint(PProj(AProj).NewPolyP^.Data.At(i)^),true);
       PProj(AProj).NewPolyP^.Endit(PProj(AProj).Pinfo);
       PProj(Aproj).MsgLeftDblClick(AMousePos);
    end;


 begin;

     case Count of
 0: if Flag= true //true - click
     then
        begin;
       PPRoj(AProj).NewPoly^.PointInsert(PPRoj(AProj).PInfo,AMousePos,TRUE);
       PPRoj(AProj).NewPoly^.LineOn := True;
        end;
 1:  if Flag= true //true - click
       then
        begin;
       PPRoj(AProj).NewPoly^.PointInsert(PPRoj(AProj).PInfo,AMousePos,TRUE);
       PPRoj(AProj).NewPoly^.LineOn := True;
        end;

 2: begin;
     Check;
     if Flag=true then InsertRect;
    end;

 3: begin;
     AmousePos:=TDPOINT(PProj(AProj).NewPoly^.Data.At(2)^);
     Check;
     InsertRect;
     end;
 end;
end;

//draw
 Procedure ThreePointsDraw(AProj: Pointer;AMousePoint:TDPOint);
 begin;
  if PPRoj(AProj).NewPoly^.Data.Count>0 then
     PPRoj(AProj).NewPoly^.MouseMove(PPRoj(AProj).PInfo,AMousePoint);
  ThreePointsCalc(AProj,AMousePoint,false,PProj(AProj).NewPoly.Data.Count);
 end;

//change on/off RubberBox mode
Procedure ChangeDrawRubberBoxMode(AProj: Pointer);
Begin
     PPRoj(AProj).bDrawRubberBox := NOT PPRoj(AProj).bDrawRubberBox;

        If PPRoj(AProj).bDrawRubberBox=true
        then
         begin;
           If PPRoj(AProj).NewPoly.Data.Count>0 then
            begin;
             If PPRoj(AProj).NewPoly.Data.Count =2
              then
                if MessageDialog(GetLangText(11293),mtWarning,[mbYes,mbNo],0) = idNo
                 then
                 begin;
                 PPRoj(AProj).bDrawRubberBox:=false;
                 MenuFunctions['DrawRectByRubberBox'].Checked:=true;
                 Exit;
                 end;
                  RubberBoxCalc(AProj,PPRoj(AProj).AOldMousePos,false,PPRoj(AProj).NewPoly.Data.Count);
             end;
         end
           Else
          Begin;
           if PPRoj(AProj).SelRect.IsOn then  begin;
              PPRoj(AProj).NewPoly^.PointInsert(PPRoj(AProj).PInfo,PPRoj(AProj).SelRect^.StartPos,TRUE);
              PPRoj(AProj).NewPoly^.StartPos:=PPRoj(AProj).SelRect^.StartPos;
              PPRoj(AProj).SelRect^.EndIt(PPRoj(AProj).PINfo);
              PPRoj(AProj).NewPoly^.MouseMove(PPRoj(AProj).PInfo,PPRoj(AProj).AOldMousePos);
                                              end;
          End;

End;

//draw RubberBox or insert rectangle
Procedure RubberBoxCalc(AProj: Pointer;AMousePoint:TDPOint;Flag:boolean;Count:integer);

 procedure DrawRectangle;
  var StPt,EnPt,TmpPt:TDPoint;
  begin;
     if Flag=false then
      begin;
      StPT.Init(PDPOINT(PProj(AProj).NewPoly.Data.At(PProj(AProj).NewPoly.Data.Count-2))^.X,PDPOINT(PProj(AProj).NewPoly.Data.At(PProj(AProj).NewPoly.Data.Count-2))^.Y);
      EnPt.Init(PDPOINT(PProj(AProj).NewPoly.Data.At(PProj(AProj).NewPoly.Data.Count-1))^.X,PDPOINT(PProj(AProj).NewPoly.Data.At(PProj(AProj).NewPoly.Data.Count-1))^.Y);
      end
      else
      begin;
      StPt.Init(PProj(AProj).SelRect^.StartPos.X,PProj(AProj).SelRect^.StartPos.Y);
      EnPt.Init(AMousePoint.X,AMousePoint.Y);
      end;

      if PProj(AProj).SelREct^.IsOn then PProj(AProj).SelREct^.EndIt(PProj(AProj).Pinfo);
      PProj(AProj).NewPoly^.EndIt(PProj(AProj).Pinfo);

      //create the rectange
      PProj(AProj).NewPoly^.PointInsert(PProj(AProj).PInfo,StPt,true);
       TmpPt.x:=StPt.X; TmpPt.y:=EnPt.Y;
      PProj(AProj).NewPoly^.PointInsert(PProj(AProj).PInfo,TmpPt,true);
      PProj(AProj).NewPoly^.PointInsert(PProj(AProj).PInfo,EnPt,true);
      TmpPt.x:=EnPt.X; TmpPt.y:=StPt.Y;
      PProj(AProj).NewPoly^.PointInsert(PProj(AProj).PInfo,TmpPt,true);
  end;


 begin;
   case Count of
    0: begin;
         PProj(AProj).SelRect^.StartPos:=AMousePoint;
         PPRoj(AProj).SelRect^.StartIt(PPRoj(AProj).PINfo,AMousePoint,false);
       end;
    1: begin;
       if Flag=false then //change FirstPoint Polyline to FirstPoint RubberBox
        begin;
          PProj(AProj).SelRect^.StartPos:=PPRoj(AProj).NewPoly^.StartPos;
          PPRoj(AProj).SelRect^.StartIt(PPRoj(AProj).PINfo,PPRoj(AProj).NewPoly^.StartPos,false);
          PProj(AProj).NewPoly^.EndIt(PPRoj(AProj).PINfo);
          PPRoj(AProj).SelRect^.MouseMove(PPRoj(AProj).PINfo,AMousePoint);
        end
        else
        begin; //Finish RubberBox and Draw Rectange
         DrawRectangle;
         PProj(Aproj).MsgLeftDblClick(AMousePoint); //insert Rectangle
        end;
       end;
    2: begin;
       DrawRectangle;
       PProj(Aproj).MsgLeftDblClick(AMousePoint);
       end;
   end;

 end;


Procedure RubberBoxDraw(AProj: Pointer;AMousePoint:TDPOint);
 begin;
if  PPRoj(AProj).SelRect^.IsOn then
     PPRoj(AProj).SelRect^.MouseMove(PProj(AProj).PInfo, AMousePoint);
 end;

{------ Brovak}



end.