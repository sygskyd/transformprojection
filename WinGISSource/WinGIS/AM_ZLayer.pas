{*******************************************************************************
I Implements a layer object with a z order list                                I
I Used by the symbol editor                                                    I
I------------------------------------------------------------------------------I
I Autor : Raimund Leitner                                                      I
*******************************************************************************}
unit am_zlayer;

interface

Uses Classes,AM_Layer,AM_Paint,AM_Index;

Type PZLayer = ^TZLayer;
     TZLayer = object(TLayer)
       ZOrder : TList;
       Constructor Init(AOwner:PLayer);
       Destructor  Done; virtual;
       Function    InsertObject(PInfo:PPaint;Item:PIndex;Paint:Boolean):Boolean; virtual;
       Function    DeleteIndex(PInfo:PPaint;AItem:PIndex):Boolean; virtual;
       Function    Draw(PInfo:PPaint):Boolean; virtual;
       Procedure   InvertZOrder;
       end;

implementation

Uses AM_Def;

Constructor TZLayer.Init
   (
   AOwner          : PLayer
   );
  begin
    inherited Init(AOwner);
    ZOrder:=TList.Create;
  end;

Destructor TZLayer.Done;
  begin
    ZOrder.Free;
    inherited Done;
  end;

Function TZLayer.InsertObject
   (
   PInfo           : PPaint;
   Item            : PIndex;
   Paint           : Boolean
   )
   : Boolean;
begin
  Result:=inherited InsertObject(PInfo,Item,Paint);
  ZOrder.Add(Item);
end;

Procedure TZLayer.InvertZOrder;
var i,j   : Integer;
    aItem : PIndex;
begin
  if ZOrder.Count=0 then exit;
  for i:=0 to (ZOrder.Count-1) div 2 do begin
    aItem:=PIndex(ZOrder[i]);
    j:=ZOrder.Count-1-i;
    ZOrder[i]:=ZOrder[j];
    ZOrder[j]:=aItem;
    end;
end;

Function TZLayer.DeleteIndex(PInfo:PPaint;AItem:PIndex):Boolean;
var i : Integer;
begin
  i:=0;
  while (i<ZOrder.Count) and
        (PIndex(ZOrder[i])^.Index<>AItem^.Index) do i:=i+1;
  if (i<ZOrder.Count) then
    Zorder.Delete(i);
  Result:=inherited DeleteIndex(PInfo,AItem);
end;

Function TZLayer.Draw
   (
   PInfo           : PPaint
   )
   : Boolean;
  Function DoAll
     (
     Item          : PIndex
     )
     : Boolean; Far;
    var AItem       : PIndex;
    begin
      if Item^.ClipRect.A.X=0 then if Item^.ClipRect.B.X=0 then begin
        AItem:=IndexObject(PInfo,Item);
        Item^.ClipRect.InitByRect(AItem^.ClipRect);
      end;
      if Item^.Visible(PInfo) then DrawObject(PInfo,Item,Item^.NeedClipping(PInfo));
      Result:=PInfo^.KeepWindowsAlive;
    end;
  var i     : Integer;
      Item  : PIndex;
  begin
{++ Ivanoff BUG#NEW13 BUILD#100}
    RESULT := FALSE;
{-- Ivanoff}
  // without z-order act as normal TLayer
    if ZOrder.Count=0 then begin
      inherited Draw(PInfo);
      Exit;
      end;

    if (PInfo^.ZoomToScale(PInfo^.Zoom)>=GeneralMin)
        and (PInfo^.ZoomToScale(PInfo^.Zoom)<=GeneralMax)
        and not GetState(sf_LayerOff)
        and (not(PInfo^.Printing or PInfo^.PreViewing) or Printable) then begin
      PInfo^.ExtCanvas.Push;
      try
        PInfo^.SetLineStyle(LineStyle,TRUE);
        PInfo^.SetFillStyle(FillStyle,TRUE);
        PInfo^.SetSymbolFill(SymbolFill);
        PInfo^.BeginPaint(0,0,dm_Paint,-1);
        for i:=0 to ZOrder.Count-1 do begin
          Item:=PIndex(ZOrder[i]);
{++ Ivanoff BUG#635 BUILD#100}
{The function did not return result of drawing.}
          RESULT := DoAll(Item);
{-- Ivanoff}
          end;
//        Draw:=Data^.LastThat(@DoAll)<>NIL;
      finally
        PInfo^.ExtCanvas.Pop;
      end;
    end
    else Result:=FALSE;
  end;


end.
