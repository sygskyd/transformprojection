{****************************************************************************}
{ Unit AM_Ini                                                                }
{----------------------------------------------------------------------------}
{ - Routinen zur Verwaltung des INI-Files.                                   }
{ - Dialog zum setzten der Einstellungen                                     }
{ - DDE-Initialisierung zur Datenbankanwendung                               }
{----------------------------------------------------------------------------}
{ Modifikationen                                                             }
{  16.02.1994 Martin Forst                                                   }
{  11.03.1994 Martin Forst   Verwaltung f�r User-Datenbanken hinzugef�gt.    }
{  30.03.1994 Martin Forst   Verwaltung f�r User-Datenbanken                 }
{  04.04.1994 Martin Forst   Verwaltung f�r User-Datenbanken                 }
{  05.04.1994 Martin Forst   Koordinatensystem                               }
{  05.10.1995 Michael Bramberger   Bitmap- Transparenz                       }
{  20.10.1997 Heinz Ofner    DDE-Verwaltung                                  }
{****************************************************************************}
unit AM_Ini;

interface
 
uses WinDOS, Messages, AM_Def, Objects, AM_Modul, Classes, ResDlg, Controls, Windows, DDEDef;

const
  bsReadList = 1000;
  bsReadEntry = 100;

  asInterval = 5;

  SETTINGS: PChar = 'SETTINGS';

type
  PDigitData = ^TDigitData;
  TDigitData = record
    DefInput: array[0..slDevName] of Char;
    LibName: array[0..slLibName] of Char;
    PortDef: array[0..slPortDef] of Char;
    PortName: array[0..slPortName] of Char;
    WinTAB: Boolean; {~Ray WinTAB Treiber ? }
    Mouse: Boolean; { Digitizer emuliert Mouse}
    DigitMode: Boolean;
    DevChange: Boolean;
  end;

  POptionsData = ^TOptionsData;
  TOptionsData = record
    BackupFile: Boolean;
    AutoSave: Boolean;
    SaveInterv: Integer;
    HistSize: Integer;
  end;

  PPrinterData = ^TPrinterData;
  TPrinterData = record
    Printer: array[0..slPrnName] of Char;
    Default: array[0..slPrnName] of Char;
  end;

  PGPSData = ^TGPSData;
  TGPSData = record
    ShowAs: array[0..20] of Char;
    LongitudeScale: Real;
    LatitudeScale: Real;
    DBImp: Boolean;
  end;

//++Sygsky: 29-JAN-2001 for Freddie. Add painting for 1bit *.PRI ONLY!
  T1BITIMAGECOLORS = record
    Black: DWORD;
    White: DWORD;
    end;
//--Sygsky

//++ Default for PointsCoord
 TLoadPointsCoordDataFromFile= record
     ReadFromRow:integer;
     BaseDelimeter:string;
     DataIntoDelimeters:boolean;
     Delleft:string ;
     Delright:string;
     XColumn:integer;
     YColumn:integer;
     KoeffX:boolean;
     KoeffDataX:double;
     KoeffY:boolean;
     KoeffDataY:double;
     LoadMode:integer;
     //for projection
     InputMode:integer;
     DegreeMode:integer;

   end;
//--Brovak

//++ Default for AdvAnnotation
 TAdvAnnotationSettings = record
 UseAdvancedOptions:boolean;
 PolygonAdv:boolean;
 PolyLineAdv:boolean;
 PolyLineAdvDirection:byte; //3 variants
 ArcAdv:boolean;
 ArcAdvDirection:byte; //3 variants
 OffSetAdv:boolean;
 OffSetAdvDirection:byte; //9 variants
   end;
//--Brovak


  PIniFile = ^TIniFile;
  TIniFile = object(TOldObject)
    Parent: TWinControl;
    IniFile: array[0..fsPathName] of Char;
    IniFound: Boolean;
    Options: TOptionsData;
    DigitData: TDigitData;
    PrinterData: TPrinterData;
    //++brovak
    LoadPointsCoordDataFromFile:TLoadPointsCoordDataFromFile;
    AdvAnnotationSettings:TAdvAnnotationSettings;
    //--brovak
    AreaFact: LongInt;
    ModuleData: PModData;
    ProjLayerName: string;
    MaxZoom: Real;
    AskQuit: Boolean;
//++ Glukhov Bug#236 Build#144 07.12.00
//       AutoBmpReload    : Word;
// Image Reload mode
    AutoBmpReload: LongInt;
// The possible values:
//    -1  do not load the images;
//    0   do not unload the images;
//    1   use the buffer of images to unload the oldest images in buffer;
//    2   use the buffer of images and load the images as background process if possible
// The Reload variables (used in modes 1 or 2)
    AutoBmpBfSize: LongInt; // Size of buffer, in Kb
    AutoBmpBfUsed: LongInt; // Used size of buffer, in Kb
    AutoBmpLoaded: PCollection; // List of loaded images
    AutoBmpToLoad: PCollection; // List of images to be loaded
//-- Glukhov Bug#236 Build#144 07.12.00
//++Sygsky: 29-JAN-2001 for Freddie. Add painting for 1bit *.PRI ONLY!
    VMSIBWSetting: T1BITIMAGECOLORS;
    BgProjBrush:THandle;
//--Sygsky
    AutoReplaceFonts: Boolean;
    SaveSightName: string;
    SaveActSight: Boolean;
    SaveLMName: string;
    FrameSelect: Boolean;
    LDTRFMaxRelation: Real;

    DDEMLConnect: Boolean; {allgemeine Einstellungen f�r die DDEML-Kommunikation}
    DDEMLSend: Boolean;
    DBStringFormat: Word;
    DBLayerInfos: Word;
    DDESeparateSign: Char;
    DDEMLCallBack: Word;
    MWReady: Boolean;
    GReady: Boolean;
    GReadyAfterView: Boolean;
    DBSelMode: Boolean;
    SendLMSettings: Boolean;
    ShowAfterDSA: Boolean;
    OneSelect: Boolean;
    DBAutoInsert: Boolean;
    SendInfosToDB: LongInt;
    DoOneSelect: Boolean;

    SimplyMode       : boolean; //brovak - for new F4

    BmpPixelsExact: Boolean;
    BmpAreaExact: Boolean;

    MMAutoPlay: Boolean;
    ShowStatus: Word;

    GVLChangeMode: Boolean;
    GeneralViewLayer: string;
    GVLCModeStruct: PCollection;

    PictureDir: string;
    PictureDrive: string;
    MMDir: string;
    MMDrive: string;
    HSDDir: string;
    HSDDrive: string;

    MouseZoom: Boolean;
    SendMouseInfo: Boolean;
    ShowPolyCenter: Boolean;

    GSLayer: string;
    FSLayer: string;
    SLayer: string;

    MaxTMClasses: Integer;

    VariousSettings: TVariousSettings;
    SelectionSettings: TSelectionSettings;
    BitmapSettings: TBitmapSettings;
    ThreeDSettings: TThreeDSettings;
    FontSettings: TFontSettings;
    DatabaseSettings: PDBSettings;

    constructor Init(AParent: TWinControl);
    destructor Done; virtual;
    function GetList(Private: Boolean; AName: PChar; var BResult: PStrCollection): Boolean;
    function GetRealVidMode(Res: PChar): Integer;
    procedure ReadAreaFact;
    function ReadConnection: string;
    procedure ReadGeneralDBSettings;
    //procedure ReadDDEApps(DDEApps: PCollection);
    //procedure ReadDDEMLApps(DDEMLApps: PCollection);
    function ReadDDEMLWingisServerSettings(Service, Topic, Item: PChar): Boolean;
    function ReadBoolean(App, Key: PChar; Default: Boolean): Boolean;
    procedure ReadDefaultInput;
    function ReadInput(ADev: PChar; AData: PDigitData): Boolean;
    procedure ReadOptions;
    function ReadPassword(var APassWord: string): Boolean;
    function ReadLTPassword(var APassWord: string): Boolean;
    procedure ReadProjectLayerName;
    procedure WriteProjectLayerNames;
    procedure ReadPrinter;
    procedure ReadGPSOptions(var AGPSData: TGPSData);
    procedure SaveOptions;
    procedure SavePrinter;
    procedure SaveInput(AData: PDigitData);
    procedure SetDefaultInput(AName: PChar);
    function SetInput(ADev: PChar): Boolean;
    function SetOptions: Boolean;
    procedure WriteBoolean(App, Key: PChar; AData: Boolean);
    procedure WriteInteger(App, Key: PChar; AData: LongInt);
    procedure WritePassword(APassWord: string);
    function ReadInteger(AppName, KeyName: PChar; ADefault: LongInt): LongInt;
    function ReadFloat(AppName, KeyName: PChar; const ADefault: Double): Double;
    procedure WriteFloat(AppName, KeyName: PChar; const AValue: Double);
    procedure WriteLTPassword(APassWord: string);
    procedure WriteGPSOptions(AGPSData: TGPSData);
    procedure ReadModuleData;
    procedure ReadMouseZoomSettings;
    procedure WriteModuleData;
    Procedure   ReadF4SimplyMode; //brovak
    Procedure   WriteF4SimplyMode; //For new F4
   {brovak}
    Procedure  ReadAdvAnnotation;
    Procedure  WriteAdvAnnotation;
   {brovak}
    procedure WriteDBSelOpt(DBSelMod: Boolean);
    function ReadTranspSelOpt: Byte;
    procedure WriteTranspSelOpt(TranspSelMod: Byte);
    //function WriteDDEDDEMLSettings(ANewDDEList, ANewDDEMLList: PCollection): Boolean;
    function ReadDiagOpts(var DType: Byte; var DWidth: LongInt; var DHeight: LongInt; var DDiam: LongInt;
      var DAbsRel: Boolean; var DAnnot: Boolean; var DAAbsRel: Boolean): Boolean;
    procedure WriteDiagOpts(DType: Byte; DWidth: LongInt; DHeight: LongInt; DDiam: LongInt;
      DAbsRel: Boolean; DAnnot: Boolean; DAAbsRel: Boolean);
    procedure ReadDirSettings;
       {Function    ReadIntro(var AAnimation:PChar;var ASound:PChar;var ATime:Word):Boolean;}//old intro
    procedure ReadLDTRFRelation;
    procedure ReadSelOptions;
    procedure ReadFloatingTextSettings;
    function  ReadRGBQuad(AppName, KeyName: PChar; ADefault: DWORD): DWORD;
    function  ReadBgProjBrush(AppName, KeyName: PChar; ADefault: DWORD): THandle; //++Sygsky bug #333
    {++Brovak - for LoadPointsCoord }
    procedure ReadDataForLoadPoint;
    procedure WriteDataForLoadPoint;
    //for projections
    procedure WriteDataForLoadPoint1;


    procedure ReadSpecFrameRect(AProject:pointer);
    procedure WriteSpecFrameRect(AProject:pointer);

    {--brovak}
    function  GetAreaUnit: String;
  end;

function Code(AStr: string): string;

function UnCode(AStr: string): string;

var
  IniFile: PIniFile;

implementation

uses AM_Child, BMPImage, AM_Main, AM_Dlg1, AM_Layer, SysUtils, Win32Def,
  UserIntf, NumTools, Tooltips, AM_Font, WinOSInfo
  {$IFNDEF WMLT}
  ,AM_DDE
  {$ENDIF}
  , VerMgr, AM_proj,
  TextUtils,Forms;

{===============================================================================================}
{ TIniFile                                                                                      }
{===============================================================================================}

constructor TIniFile.Init
  (
  AParent: TWinControl
  );
var
  ProgName: array[0..255] of Char;
  Path: array[0..255] of Char;
  AName: array[0..255] of Char;
  Ext: array[0..255] of Char;
  Search: TSearchRec;
  DosError: Integer;
begin
  inherited Init;
  Parent := AParent;
  StrPCopy(IniFile, OSInfo.WingisIniFileName);
  DosError := FindFirst(IniFile, faAnyFile, Search);

  if DosError <> 0 then
  begin
    //MsgBox(Parent.Handle, 1012, mb_OK or mb_IconExclamation, '');
    IniFound := FALSE;
  end
  else
    IniFound := TRUE;

  with FontSettings do
  begin
    UseGlobals := TRUE;
    ChosenFont := New(PFontData, Init);
    AllFonts := New(PFonts, Init);
    AllFonts^.CheckInsertFonts(0);
  end;
  DatabaseSettings := New(PDBSettings, Init(TRUE));
  ReadOptions;
  ReadDefaultInput;
  ReadInput(DigitData.DefInput, @DigitData);
  DigitData.DigitMode := FALSE;
  ReadPrinter;
  ReadAreaFact;
  ModuleData := New(PModData, Init);
  ReadModuleData;
//++ Glukhov Bug#236 Build#144 07.12.00
  if AutoBmpReload < 0 then
  begin
    BitmapSettings.GlobInvisiblity := False;
    AutoBmpReload := 0;
  end;
  AutoBmpBfUsed := 0;
  AutoBmpBfSize := AutoBmpBfSize * 1024; // in bytes
  if AutoBmpBfSize < 2048 * 1024 then AutoBmpBfSize := 2048 * 1024;
  AutoBmpLoaded := New(PCollection, Init(8, 8));
  AutoBmpToLoad := New(PCollection, Init(8, 8));
//-- Glukhov Bug#236 Build#144 07.12.00
  if AutoBmpReload <> 0 then GVLCModeStruct
    := New(PCollection, Init(3, 3)) // Glukhov: I do not see any sense here
  else
    GVLCModeStruct := nil;
  ReadProjectLayerName;
  DoOneSelect := FALSE;
  ReadGeneralDBSettings;
  ReadSelOptions;
  ReadMouseZoomSettings;
  ReadLDTRFRelation;
  ReadDirSettings;
  ReadFloatingTextSettings;
  {$IFNDEF WMLT}
  ReadDataForLoadPoint;
  {$ENDIF}
end;

destructor TIniFile.Done;
begin
  if GVLCModeStruct <> nil then Dispose(GVLCModeStruct, Done);
//++ Glukhov Bug#236 Build#144 14.12.00
  AutoBmpLoaded.DeleteAll;
  Dispose(AutoBmpLoaded, Done);
  AutoBmpLoaded := nil;
  AutoBmpToLoad.DeleteAll;
  Dispose(AutoBmpToLoad, Done);
  AutoBmpToLoad := nil;
//-- Glukhov Bug#236 Build#144 14.12.00
  ModuleData^.UnRegisterTRLib;
  Dispose(ModuleData, Done);
  with FontSettings do
  begin
    Dispose(ChosenFont, Done);
    Dispose(AllFonts, Done);
  end;
  Dispose(DatabaseSettings, Done);
  inherited Done;
end;


{++ brovak for new F4 Dialog}
Procedure TIniFile.ReadF4SimplyMode;
 var Buffer : Array[0..255] of char;
  begin
    GetPrivateProfileString('SETTINGS','F4SimplyMode','TRUE',Buffer,SizeOf(Buffer),IniFile);
    if (StrComp(Buffer,'0') = 0) or (StrComp(Buffer,'TRUE') = 0) then SimplyMode:=true
    else SimplyMode:=false;
 end;


Procedure TIniFile.WriteF4SimplyMode;
  begin
    WriteBoolean('SETTINGS','F4SimplyMode',SimplyMode);
 end;
  {--brovak}

procedure TIniFile.ReadProjectLayerName;
{$IFNDEF WMLT}
var
  sLimit: array[0..255] of Char;
  TempPointer: PChar;
  FirstLayer: Boolean;
  VName: array[0..255] of Char;
  LName: array[0..255] of Char;
  DDEStrEnd: Boolean;
  GVLChange: POverViewData;
{$ENDIF}
begin
{$IFNDEF WMLT}
  GetPrivateProfileString(SETTINGS, 'LoadLayer', 'Projectlayer', sLimit, sizeof(sLimit), IniFile);
  ProjLayerName := StrPas(sLimit);
  if ProjLayerName = '' then ProjLayerName := 'Projectlayer';
  if AutoBmpReload <> 0 then
  begin // Glukhov: I do not see any sense here
    GetPrivateProfileString(SETTINGS, 'GVLChangeMode', 'FALSE', sLimit, sizeof(sLimit), IniFile);
    if StrComp(sLimit, 'TRUE') = 0 then
      GVLChangeMode := TRUE
    else
      GVLChangeMode := FALSE;
    if GVLChangeMode then
    begin
      GetPrivateProfileString(SETTINGS, 'GeneralViewLayer', 'GeneralView', sLimit, sizeof(sLimit), IniFile);
      if StrLen(sLimit) > 0 then
      begin
        TempPointer := sLimit;
        FirstLayer := TRUE;
        DDEStrEnd := FALSE;
        while not DDEStrEnd do
        begin
          {$IFNDEF AXDLL} // <----------------- AXDLL
          DDEStrEnd := ReadIniText(TempPointer, DDEHandler.FDDESepSign, ';', VName);
          {$ENDIF} // <----------------- AXDLL
          if not DDEStrEnd then
            DDEStrEnd := ReadIniText(TempPointer, ';', ';', LName)
          else
            StrCopy(LName, '');
          GVLChange := New(POverViewData, Init(VName, LName));
          GVLCModeStruct^.Insert(GVLChange);
          if FirstLayer then GeneralViewLayer := StrPas(PChar(@LName));
          FirstLayer := FALSE;
        end;
      end
      else
        GeneralViewLayer := ''; ;
    end
    else
    begin
      GetPrivateProfileString(SETTINGS, 'GeneralViewLayer', 'GeneralView', sLimit, sizeof(sLimit), IniFile);
      GeneralViewLayer := StrPas(sLimit);
    end;
  end
  else
  begin
    GVLChangeMode := FALSE;
    GeneralViewLayer := 'GeneralView';
  end;
  GetPrivateProfileString(SETTINGS, 'GSLayer', '', sLimit, sizeof(sLimit), IniFile);
  GSLayer := StrPas(sLimit);
  GetPrivateProfileString(SETTINGS, 'FSLayer', '', sLimit, sizeof(sLimit), IniFile);
  FSLayer := StrPas(sLimit);
  GetPrivateProfileString(SETTINGS, 'SLayer', '', sLimit, sizeof(sLimit), IniFile);
  SLayer := StrPas(sLimit);
{$ENDIF}
end;

procedure TIniFile.WriteProjectLayerNames;
var
  WriteName: array[0..255] of Char;
begin
  StrPCopy(WriteName, GSLayer);
  WritePrivateProfileString(SETTINGS, 'GSLayer', WriteName, IniFile);
  StrPCopy(WriteName, FSLayer);
  WritePrivateProfileString(SETTINGS, 'FSLayer', WriteName, IniFile);
  StrPCopy(WriteName, SLayer);
  WritePrivateProfileString(SETTINGS, 'SLayer', WriteName, IniFile);
end;

procedure TIniFile.ReadDirSettings;
var
  sDir: array[0..255] of Char;
  function CorrectEntry
      : string;
  var
    TmpStr: PChar;
    TmpString: string;
  begin
    TmpString := StrPas(sDir);
    if ByteToCharLen(TmpString, 255) > 0 then
    begin
      if ByteToCharLen(TmpString, 255) = 1 then StrCat(sDir, ':');
      TmpStr := AnsiStrRScan(sDir, '\');
      if TmpStr = nil then
        StrCat(sDir, '\')
      else
      begin
        TmpString := StrPas(TmpStr);
        if ByteToCharLen(TmpString, 255) > 1 then StrCat(sDir, '\');
      end;
      Result := StrPas(sDir);
    end
    else
      Result := '';
  end;
  function SetCurDriveAsParam
      : string;
  var
    ProgName: array[0..255] of Char;
    Path: array[0..255] of Char;
  begin
    GetModuleFileName(HInstance, ProgName, SizeOf(ProgName));
    Result := StrPas(StrLCopy(Path, ProgName, 3));
  end;
begin
  GetPrivateProfileString(SETTINGS, 'PictureDirectory', '', sDir, SizeOf(sDir), IniFile);
  PictureDir := CorrectEntry;
  GetPrivateProfileString(SETTINGS, 'MMDirectory', '', sDir, SizeOf(sDir), IniFile);
  MMDir := CorrectEntry;
  GetPrivateProfileString(SETTINGS, 'HSDDirectory', '', sDir, SizeOf(sDir), IniFile);
  HSDDir := CorrectEntry;
  GetPrivateProfileString(SETTINGS, 'PictureDrive', '', sDir, SizeOf(sDir), IniFile);
  if StrComp(sDir, '.') = 0 then
    PictureDrive := SetCurDriveAsParam
  else
    PictureDrive := CorrectEntry;
  GetPrivateProfileString(SETTINGS, 'MMDrive', '', sDir, SizeOf(sDir), IniFile);
  if StrComp(sDir, '.') = 0 then
    MMDrive := SetCurDriveAsParam
  else
    MMDrive := CorrectEntry;
  GetPrivateProfileString(SETTINGS, 'HSDDrive', '', sDir, SizeOf(sDir), IniFile);
  if StrComp(sDir, '.') = 0 then
    HSDDrive := SetCurDriveAsParam
  else
    HSDDrive := CorrectEntry;
end;

{ //--------oldintro
 Function TIniFile.ReadIntro
   (
   var AAnimation  : PChar;
   var ASound      : PChar;
   var ATime       : Word
   )
   : Boolean;
  var sText        : Array[0..1024] of Char;
      TmpString    : PChar;
      TmpVal       : LongInt;
      StrOK        : Boolean;
      FoundStr     : Boolean;
      MoreStrs     : Boolean;
  begin
    Result:=FALSE;
    AAnimation:=NIL;
    ASound:=NIL;
    GetPrivateProfileString(SETTINGS,'Intro','',sText,SizeOf(sText),IniFile);
    if StrLen(sText) > 0 then begin
      StrOK:=ReadDDECommandPChar(sText,0,0,WgBlockStart,DDEHandler.FDDESepSign,
                                             WgBlockEnd,TmpString,MoreStrs);
      if TmpString <> NIL then begin
        AAnimation:=StrNew(TmpString);
        StrDispose(TmpString);
      end
      else AAnimation:=NIL;
      StrOK:=ReadDDECommandPChar(sText,0,1,WgBlockStart,DDEHandler.FDDESepSign,
                                             WgBlockEnd,TmpString,MoreStrs);
      if TmpString <> NIL then begin
        ASound:=StrNew(TmpString);
        StrDispose(TmpString);
      end
      else ASound:=NIL;
      StrOK:=ReadDDECommandLongInt(sText,0,2,WgBlockStart,DDEHandler.FDDESepSign,
                                               WgBlockEnd,TmpVal,FoundStr,MoreStrs);
      ATime:=TmpVal;
      if (AAnimation <> NIL) or (ASound <> NIL) then Result:=TRUE;
    end;
  end;
//-------------------------}

function TIniFile.ReadConnection
  : string;
var
  sConnect: array[0..fsPathName] of Char;
begin
  GetPrivateProfileString(SETTINGS, 'Connection', '', sConnect, SizeOf(sConnect), IniFile);
  Result := StrPas(sConnect);
end;

procedure TIniFile.ReadMouseZoomSettings;
var
  sMouseZoom: array[0..255] of char;
begin
  GetPrivateProfileString(SETTINGS, 'MouseZoom', '1', sMouseZoom, SizeOf(sMouseZoom), IniFile);
  if StrComp(sMouseZoom, '0') = 0 then
    MouseZoom := FALSE
  else
    MouseZoom := TRUE;
end;

procedure TIniFile.ReadDefaultInput;
begin
  with DigitData do
    GetPrivateProfileString(SETTINGS, 'Input', '', DefInput, SizeOf(DefInput), IniFile);
end;

procedure TIniFile.ReadLDTRFRelation;
var
  sInfo: array[0..255] of Char;
  Error: Integer;
begin
  GetPrivateProfileString(SETTINGS, 'LDTRFMaxRelation', '0.1', sInfo, SizeOf(sInfo), IniFile);
  Val(sInfo, LDTRFMaxRelation, Error);
  if (LDTRFMaxRelation < 0) or (LDTRFMaxRelation > 1) or (Error <> 0) then LDTRFMaxRelation := 0.1;
end;

procedure TIniFile.SetDefaultInput
  (
  AName: PChar
  );
begin
  if StrComp(AName, '') <> 0 then
    with DigitData do
    begin
      StrCopy(DefInput, AName);
      WritePrivateProfileString(SETTINGS, 'Input', DefInput, IniFile);
    end;
end;

function TIniFile.GetList
  (
  Private: Boolean;
  AName: PChar;
  var BResult: PStrCollection
  )
  : Boolean;
var
  APuffer: array[0..bsReadList] of Char;
  Copied: Integer;
  Split: PChar;
begin
  if Private then
    Copied := GetPrivateProfileString(AName, nil, '', APuffer, SizeOf(APuffer), IniFile)
  else
    Copied := GetProfileString(AName, nil, '', APuffer, SizeOf(APuffer));
  if Copied = 0 then
  begin
    GetList := FALSE;
    BResult := nil;
  end
  else
  begin
    BResult := New(PStrCollection, Init(10, 5));
    Split := APuffer;
    while Split^ <> #0 do
    begin
      if StrComp(Split, 'WinGISDDEServer') <> 0 then BResult^.Insert(StrNew(Split));
        {BResult^.Insert(StrNew(Split));}
      Inc(Split, StrLen(Split) + 1);
    end;
    GetList := TRUE;
  end;
end;

procedure TIniFile.ReadOptions;
var
  Buffer: array[0..255] of Char;
  Error: Integer;
  BitmapOptions: Integer;
begin
  with Options do
  begin
    BackupFile := ReadBoolean('SAVEOPTIONS', 'Backup', TRUE);
    AutoSave := ReadBoolean('SAVEOPTIONS', 'AutoSave', TRUE);
    SaveInterv := GetPrivateProfileInt('SAVEOPTIONS', 'AutoSaveInterval', asInterval, IniFile);
    HistSize := GetPrivateProfileInt('SETTINGS', 'FileHistorySize', 10, IniFile);
    HistSize:=Min(HistSize,20);
    with BitmapSettings do
    begin
      Transparency := GetPrivateProfileInt(SETTINGS, 'BitmapTransparency', 0, IniFile);
      if (Transparency > 100) or (Transparency < 0) then Transparency := 0;
      TransparencyType := GetPrivateProfileInt(SETTINGS, 'BitmapTransparencyType', tt_White, IniFile);
      if (TransparencyType <> tt_White) and (TransparencyType <> tt_Black) then
        TransparencyType := tt_White;
      ShowMinSize := GetPrivateProfileInt(SETTINGS, 'BitmapShowSize', 0, IniFile) * 10;
      BitmapOptions := GetPrivateProfileInt(SETTINGS, 'BitmapOptions', 0, IniFile);
      ShowFrame := BitmapOptions and 1 <> 0;
      ShowBitmap := BitmapOptions and 2 <> 0;
//++ Glukhov Bug#236 Build#144 04.12.00
      GlobInvisiblity := ReadBoolean(SETTINGS, 'BitmapGlobInvisibility', False);
//-- Glukhov Bug#236 Build#144 04.12.00
//++Sygsky: 29-JAN-2001 for Freddie. Add painting for 1bit *.PRI ONLY!
      VMSIBWSetting.Black := ReadRGBQuad(SETTINGS, '1BitVMSIBlack', 0); // Black default
      VMSIBWSetting.White := ReadRGBQuad(SETTINGS, '1BitVMSIWhite', $00FFFFFF); // White default
//++Sygsky: 09-JUN-2001 for Slava2. Bug #333
      BgProjBrush := ReadBgProjBrush(SETTINGS, 'BgProjBrush', WHITE_BRUSH);
//--Sygsky
    end;

    with VariousSettings do
    begin
      SnapRadius := Round(ReadFloat('SNAP', 'SnapRadius', 2));
      SnapRadiusType := ReadInteger('SNAP', 'SnapRadiusType', 2);
      SnapRadiusType := Max(0, Min(VariousSettings.SnapRadiusType, 2));
      SnapToNearest := ReadBoolean('SNAP', 'SearchNearest', FALSE);
      ZoomSize := ReadFloat(SETTINGS, 'ZoomFact', 1.0);
      ZoomSizeType := ReadInteger(SETTINGS, 'ZoomFactType', 0);
      ZoomSizeType := Max(0, Min(VariousSettings.ZoomSizeType, 1));
      ShowEdgePoints := ReadBoolean(SETTINGS, 'ShowPolyPoints', FALSE);
      CombineRadius := ReadFloat('Settings', 'CombineRadius', 0.2);
          {++ Brovak BUG#574 BUILD#108 - Additional Param Set for draw/no draw rect for hidden text }
      ShowHiddenText := true;
          {-- Brovak }
//++ Glukhov ObjAttachment 23.10.00
      bDelAttached := ReadBoolean(SETTINGS, 'DeleteAttached', TRUE);
      bDelAttachedAsk := ReadBoolean(SETTINGS, 'AskToDeleteAttached', TRUE);
//-- Glukhov ObjAttachment 23.10.00
//++ Glukhov Bug#199 Build#157 14.05.01
// Arc (Circle) conversion settings
      bConvRelMistake := ReadBoolean(SETTINGS, 'ConvertRelPrecision', TRUE);
      dbConvRelMistake := ReadFloat(SETTINGS, 'ConvertRelPrecValue', 0.4);
      dbConvRelMistake:= dbConvRelMistake / 100.0;
      dbConvAbsMistake := ReadFloat(SETTINGS, 'ConvertAbsPrecValue', 50.0);
//-- Glukhov Bug#199 Build#157 14.05.01
    end;

    with SelectionSettings do
    begin
      with LineStyle do
      begin
        GetPrivateProfileString(SETTINGS, 'SelectColor', '$00FF0000', Buffer, SizeOf(Buffer), IniFile);
        Color := HexToLong(Copy(StrPas(Buffer), 2, 255));
        Style := GetPrivateProfileInt(SETTINGS, 'SelectLineType', lt_Dot, IniFile);
        Width := GetPrivateProfileInt(SETTINGS, 'SelectLineWidth', 0, IniFile);
        WidthType := GetPrivateProfileInt(SETTINGS, 'SelectScaleType', stScaleIndependend, IniFile);
      end;
      with FillStyle do
      begin
        Pattern := GetPrivateProfileInt(SETTINGS, 'SelectPattern', pt_NoPattern, IniFile);
        GetPrivateProfileString(SETTINGS, 'SelectPatternColor', '$00000000', Buffer, SizeOf(Buffer), IniFile);
        ForeColor := HexToLong(Copy(StrPas(Buffer), 2, 255));
        GetPrivateProfileString(SETTINGS, 'SelectBackgroundColor', 'none', Buffer, SizeOf(Buffer), IniFile);
        if StrIComp(Buffer, 'none') = 0 then
          BackColor := clNone
        else
          BackColor := HexToLong(Copy(StrPas(Buffer), 2, 255));
      end;
      TransparentMode := ReadTranspSelOpt;
    end;

    with FontSettings do
    begin
      ChosenFont^.Font := GetPrivateProfileInt('GlobalFont', 'Font', 0, IniFile);
      ChosenFont^.Style := GetPrivateProfileInt('GlobalFont', 'Style', 0, IniFile);
      ChosenFont^.Height := GetPrivateProfileInt('GlobalFont', 'Height', 1, IniFile);
//++ Glukhov Bug#63 BUILD#128 14.09.00
// Check and correct the chosen font data
      FontSettings.ChosenFont^.Font := CheckChosenFont(FontSettings, nil);
//-- Glukhov Bug#63 BUILD#128 14.09.00
//++ Glukhov NonzeroText Build#166 27.11.01
      if ChosenFont^.Height <= 0 then ChosenFont^.Height:= cMinFontHeight;
//-- Glukhov NonzeroText Build#166 27.11.01
    end;

    BmpPixelsExact := True; //GetPrivateProfileInt(SETTINGS, 'BitmapLoadAllPixels', 0, IniFile) = 0;
    BmpAreaExact := True; //GetPrivateProfileInt(SETTINGS, 'BitmapLoadWholeArea', 1, IniFile) = 0;
    GetPrivateProfileString(SETTINGS, 'MaxZoom', '1.00', Buffer, SizeOf(Buffer), IniFile);
    Val(Buffer, MaxZoom, Error);
    if (Error <> 0) or (MaxZoom < 1) then MaxZoom := 1;
    GetPrivateProfileString(SETTINGS, 'AskQuit', '1', Buffer, SizeOf(Buffer), IniFile);
    if StrComp(Buffer, '0') = 0 then
      AskQuit := FALSE
    else
      AskQuit := TRUE;
    GetPrivateProfileString(SETTINGS, 'AutoBmpReload', '0', Buffer, SizeOf(Buffer), IniFile);
    if StrComp(Buffer, '1') = 0 then
      AutoBmpReload := 1
    else
      if StrComp(Buffer, '2') = 0 then
        AutoBmpReload := 2
//++ Glukhov Bug#236 Build#144 07.12.00
//      else if StrComp(Buffer,'3') = 0 then AutoBmpReload:= 3
      else
        if StrComp(Buffer, '-1') = 0 then
          AutoBmpReload := -1
//-- Glukhov Bug#236 Build#144 07.12.00
        else
          AutoBmpReload := 0;
//++ Glukhov Bug#236 Build#144 14.12.00
    GetPrivateProfileString(SETTINGS, 'AutoBmpBfSize', '2048', Buffer, SizeOf(Buffer), IniFile);
    Val(Buffer, AutoBmpBfSize, Error);
    if (Error <> 0) or (AutoBmpBfSize < 2048) then AutoBmpBfSize := 2048;
//-- Glukhov Bug#236 Build#144 14.12.00
    GetPrivateProfileString(SETTINGS, 'MMAutoPlay', 'FALSE', Buffer, SizeOf(Buffer), IniFile);
    if StrComp(Buffer, 'TRUE') = 0 then
      MMAutoPlay := TRUE
    else
      MMAutoPlay := FALSE;
    AutoReplaceFonts := ReadBoolean(SETTINGS, 'AutoReplaceFonts', FALSE);
    ShowStatus := $FFFF;
    FrameSelect := ReadBoolean(SETTINGS, 'FrameSelect', TRUE);
  end;
  ShowPolyCenter:=ReadBoolean('SETTINGS','ShowPolyCenter',FALSE);
  MaxTMClasses:=ReadInteger('SETTINGS','MaxTMClasses',50);
  SendMouseInfo:=ReadBoolean('SETTINGS','SendMouseInfo',False);
end;

procedure TIniFile.ReadGPSOptions(var AGPSData: TGPSData);
var
  AText: array[0..20] of Char;
  Code: Integer;
begin
  with AGPSData do
  begin
    DBImp := ReadBoolean('GPSOPTIONS', 'DBImport', TRUE);
    GetPrivateProfileString('GPSOPTIONS', 'ShowAs', 'Point', ShowAs, 20, IniFile);
    GetPrivateProfileString('GPSOPTIONS', 'LatitudeScale', '100000.0', @AText[0], 20, IniFile);
    Val(StrPas(AText), LatitudeScale, Code);
    if Code <> 0 then LatitudeScale := 100000.0;
    GetPrivateProfileString('GPSOPTIONS', 'LongitudeScale', '100000.0', @AText[0], 20, IniFile);
    Val(StrPas(AText), LongitudeScale, Code);
    if Code <> 0 then LongitudeScale := 100000.0;
  end;
end;

procedure TIniFile.WriteGPSOptions(AGPSData: TGPSData);
var
  AText: array[0..20] of Char;
  AStr: string;
begin
  with AGPSData do
  begin
    WriteBoolean('GPSOPTIONS', 'DBImport', DBImp);
    WritePrivateProfileString('GPSOPTIONS', 'ShowAs', ShowAs, IniFile);
    Str(LatitudeScale: 0: 2, AStr);
    StrPCopy(AText, AStr);
    WritePrivateProfileString('GPSOPTIONS', 'LatitudeScale', AText, IniFile);
    Str(LongitudeScale: 0: 2, AStr);
    StrPCopy(AText, AStr);
    WritePrivateProfileString('GPSOPTIONS', 'LongitudeScale', AText, IniFile);
  end;
end;

procedure TIniFile.SaveOptions;
var
  AText: array[0..255] of Char;
  BitmapOptions: Integer;
begin
  with Options do
  begin
    //WritePrivateProfileString('SAVEOPTIONS', nil, nil, IniFile);
    WriteBoolean('SAVEOPTIONS', 'Backup', BackupFile);
    WriteBoolean('SAVEOPTIONS', 'AutoSave', AutoSave);
    WriteInteger('SAVEOPTIONS', 'AutoSaveInterval', SaveInterv);
    WriteInteger('SETTINGS', 'FileHistorySize', Min(HistSize,20));
  end;
  with BitmapSettings do
  begin
    WriteInteger(SETTINGS, 'BitmapTransparency', Transparency);
    WriteInteger(SETTINGS, 'BitmapTransparencyType', TransparencyType);
    WriteInteger(SETTINGS, 'BitmapShowSize', Round(ShowMinSize / 10));
    if BitmapSettings.ShowFrame then
      BitmapOptions := 1
    else
      BitmapOptions := 0;
    if BitmapSettings.ShowBitmap then BitmapOptions := BitmapOptions or 2;
    WriteInteger(SETTINGS, 'BitmapOptions', BitmapOptions);
//++ Glukhov Bug#236 Build#144 04.12.00
    WriteBoolean(SETTINGS, 'BitmapGlobInvisibility', GlobInvisiblity);
//-- Glukhov Bug#236 Build#144 04.12.00
  end;
  with SelectionSettings do
  begin
    with LineStyle do
    begin
      StrPCopy(AText, '$' + LongToHex(Color, 8));
      WritePrivateProfileString(SETTINGS, 'SelectColor', AText, IniFile);
      WriteInteger(SETTINGS, 'SelectLineType', Style);
      WriteInteger(SETTINGS, 'SelectLineWidth', Round(Width));
      WriteInteger(SETTINGS, 'SelectScaleType', WidthType);
    end;
    with FillStyle do
    begin
      WriteInteger(SETTINGS, 'SelectPattern', Pattern);
      StrPCopy(AText, '$' + LongToHex(ForeColor, 8));
      WritePrivateProfileString(SETTINGS, 'SelectPatternColor', AText, IniFile);
      if BackColor = clNone then
        WritePrivateProfileString(SETTINGS, 'SelectBackgroundColor', 'none', IniFile)
      else
      begin
        StrPCopy(AText, '$' + LongToHex(BackColor, 8));
        WritePrivateProfileString(SETTINGS, 'SelectBackgroundColor', AText, IniFile);
      end;
    end;
    WriteTranspSelOpt(TransparentMode);
  end;
  with VariousSettings do
  begin
    WriteFloat('SNAP', 'SnapRadius', SnapRadius);
    WriteInteger('SNAP', 'SnapRadiusType', SnapRadiusType);
    WriteBoolean('SNAP', 'SearchNearest', SnapToNearest);
    WriteBoolean(SETTINGS, 'ShowPolyPoints', ShowEdgePoints);
    WriteFloat(SETTINGS, 'ZoomFact', ZoomSize);
    WriteInteger(SETTINGS, 'ZoomFactType', ZoomSizeType);
    WriteFloat('Settings', 'CombineRadius', CombineRadius);
//++ Glukhov ObjAttachment 23.10.00
    WriteBoolean(SETTINGS, 'DeleteAttached', bDelAttached);
    WriteBoolean(SETTINGS, 'AskToDeleteAttached', bDelAttachedAsk);
//-- Glukhov ObjAttachment 23.10.00
//++ Glukhov Bug#199 Build#157 14.05.01
// Arc (Circle) conversion settings
    WriteBoolean(SETTINGS, 'ConvertRelPrecision', bConvRelMistake);
//    WriteFloat(SETTINGS, 'ConvertRelPrecValue', dbConvRelMistake);
    WriteFloat(SETTINGS, 'ConvertRelPrecValue', dbConvRelMistake * 100.0);
    WriteFloat(SETTINGS, 'ConvertAbsPrecValue', dbConvAbsMistake);
//-- Glukhov Bug#199 Build#157 14.05.01
  end;

  with FontSettings.ChosenFont^ do
  begin
    WriteInteger('GlobalFont', 'Font', Font);
    WriteInteger('GlobalFont', 'Style', Style);
    WriteInteger('GlobalFont', 'Height', Height);
  end;
end;

function TIniFile.ReadBoolean
  (
  App: PChar;
  Key: PChar;
  Default: Boolean
  )
  : Boolean;
var
  APuffer: array[0..bsReadEntry] of Char;
begin
  GetPrivateProfileString(App, Key, 'DEF$', APuffer, SizeOf(APuffer), IniFile);
  if (StrIComp(APuffer, 'FALSE') = 0) or (StrIComp(APuffer, '0') = 0) then
    ReadBoolean := FALSE
  else
    if (StrIComp(APuffer, 'TRUE') = 0) or (StrIComp(APuffer, '1') = 0) then
      ReadBoolean := TRUE
    else
      ReadBoolean := Default;
end;

procedure TIniFile.WriteBoolean
  (
  App: PChar;
  Key: PChar;
  AData: Boolean
  );
begin
  if AData then
    WritePrivateProfileString(App, Key, 'TRUE', IniFile)
  else
    WritePrivateProfileString(App, Key, 'FALSE', IniFile);
end;

function TIniFile.ReadInput
  (
  ADev: PChar;
  AData: PDigitData
  )
  : Boolean;
var
  APuffer: array[0..bsReadEntry] of Char;
  Copied: Integer;
  Split: PChar;
  SplitPos: PChar;
  WinTAB2: PChar;
  WinTAB1: array[0..8] of Char;
begin
  ReadInput := FALSE;
  Copied := GetPrivateProfileString('InputDevices', ADev, '', APuffer, SizeOf(APuffer), IniFile);
  if Copied <> 0 then
    with AData^ do
    begin
      DeleteBlanks(APuffer);
      StrCopy(DefInput, ADev);
      Split := APuffer;
      SplitPos := StrScan(Split, ',');
      StrLCopy(LibName, Split, SplitPos - Split);
      Split := SplitPos + 1;
      WinTAB2 := StrScan(Split, ',');
      StrLCopy(WinTAB1, Split, WinTAB2 - Split);
      WinTAB2 := StrScan(WinTAB2, ',');
      StrCopy(PortDef, Split);
      WinTAB := FALSE;
      Mouse := FALSE;
      Split := PortDef;
      SplitPos := StrScan(Split, ':');
      if SplitPos = nil then
        StrCopy(PortName, '')
      else
        StrLCopy(PortName, Split, SplitPos - Split + 1);
      if StrComp(StrUpper(WinTAB1), 'WINTAB') = 0 then
      begin
        WinTAB := TRUE;
        Mouse := FALSE;
        if StrComp(StrUpper(WinTAB2 + 1), 'MOUSE') = 0 then Mouse := TRUE;
      end;
      ReadInput := TRUE;
    end;
end;

procedure TIniFile.WriteInteger
  (
  App: PChar;
  Key: PChar;
  AData: LongInt
  );
var
  AStr: string;
begin
  Str(AData, AStr);
  AStr := AStr + #0;
  WritePrivateProfileString(App, Key, @AStr[1], IniFile);
end;

procedure TIniFile.SaveInput
  (
  AData: PDigitData
  );
var
  APuffer: array[0..bsReadEntry] of Char;
begin
  with AData^ do
  begin
    StrCopy(APuffer, LibName);
    StrCat(APuffer, ',');
    if AData^.WinTAB then
    begin
      StrCat(APuffer, 'WINTAB,');
      if AData^.Mouse then
        StrCat(APuffer, 'MOUSE')
      else
        StrCat(APuffer, 'NOMOUSE');
    end
    else
      StrCat(APuffer, PortDef);
    if not WritePrivateProfileString('INPUTDEVICES', DefInput, APuffer, IniFile) then
      MsgBox(Parent.Handle, 7050, mb_OK or mb_IconExclamation, IniFile);
  end;
end;

function TIniFile.SetInput
  (
  ADev: PChar
  )
  : Boolean;
begin
  SetInput := ReadInput(ADev, @DigitData);
end;

function TIniFile.SetOptions
  : Boolean;
var
  ADataBase: array[0..255] of Char;
  CurDataBase: array[0..255] of Char;
  OldShowPoints: Boolean;
  AChild: TMDIChild;
  OldSelectColor: TColorRef;
  OldSelectType: Integer;
  OldSelectWidth: Integer;
  DoRedraw: Boolean;
  OldPattern: Integer;
  OldPatColor: TColorRef;
  OldTransp: Boolean;
  ADDEApp: array[0..255] of Char;
  CurDDEApp: array[0..255] of Char;
{  Procedure CopyDDE
     (
     Item            : PDDEApp
     ); Far;
    begin
      NewDDEList^.Insert(New(PDDEApp,InitByListEntry(Item)));
    end;
  Procedure CopyDDEML
     (
     Item            : PDDEMLApp
     ); Far;
    begin
      NewDDEMLList^.Insert(New(PDDEMLApp,InitByListEntry(Item)));
    end;}
begin
  GetPrivateProfileString(SETTINGS, 'DBApplication', '', CurDataBase, SizeOf(CurDataBase), IniFile);
  StrCopy(ADataBase, CurDataBase);
  GetPrivateProfileString(SETTINGS, 'DDEApplication', '', CurDDEApp, SizeOf(CurDDEApp), IniFile);
  StrCopy(ADDEApp, CurDDEApp);
{    with Options do begin
      OldShowPoints:=ShowPolyPoints;
      OldSelectColor:=SelectColor;
      OldSelectType:=SelectLineType;
      OldSelectWidth:=SelectLineWidth;
      OldPattern:=SelectPattern;
      OldPatColor:=SelectPatColor;
      OldTransp:=SelectTransp;
    end;}
{    if ExecDialog(TOptionsDlg.Init(Parent,@Options,@ADataBase,@ADDEApp))=id_OK then begin}
  SaveOptions;
  if StrComp(ADataBase, CurDataBase) <> 0 then
  begin
    {$IFNDEF WMLT}
    if not WritePrivateProfileString(SETTINGS, 'DBApplication', ADataBase, IniFile) then
      MsgBox(Parent.Handle, 7050, mb_OK or mb_IconExclamation, IniFile)
    else
      SetDBApplication;
    {$ENDIF}
  end;
  if StrComp(ADDEApp, CurDDEApp) <> 0 then
  begin
    {$IFNDEF WMLT}
    if not WritePrivateProfileString(SETTINGS, 'DDEApplication', ADDEApp, IniFile) then
      MsgBox(Parent.Handle, 7050, mb_OK or mb_IconExclamation, IniFile)
    else
      SetDBApplication;
    {$ENDIF}
  end;
{      with Options do if (ShowPolyPoints<>OldShowPoints)
          or (SelectColor<>OldSelectColor)
          or (SelectLineType<>OldSelectType)
          or (SelectLineWidth<>OldSelectWidth)
          or (SelectPattern<>OldPattern)
          or (SelectPatColor<>OldPatColor)
          or (SelectTransp<>OldTransp) then begin
        AChild:=Pointer(WinGISMainForm.GetCurrentChild);
        if AChild<>NIL then begin
          if (ShowPolyPoints<>OldShowPoints)
              or (PLayer(AChild.Data^.Layers^.SelLayer)^.Data^.GetCount<>0) then
            AChild.Data^.PInfo^.RedrawScreen(TRUE)
        end;
      end;}
  SetOptions := TRUE;
{    else SetOptions:=FALSE;}
end;

procedure TIniFile.ReadModuleData;
var
  AText: CodeArray;
  Code: string;
  Inst: Boolean;
  AModule: PModule;
  i: Integer;
  procedure ParseModule
      (
      BText: CodeArray;
      var ACode: string;
      var AInst: Boolean
      );
  var
    AStr1: string;
    i: Integer;
  begin
    AStr1 := StrPas(BText);
    i := Pos(',', AStr1);
    if i = 0 then
    begin
      ACode := '';
      AInst := FALSE;
    end
    else
    begin
      ACode := Copy(AStr1, i + 1, Length(AStr1) - i) + #0;
      AStr1 := Copy(AStr1, 1, i - 1);
      if AStr1 = 'Installed' then
        AInst := TRUE
      else
        AInst := FALSE;
    end;
  end;
begin
  ModuleData^.ModuleList^.FreeAll;

  GetPrivateProfileString('MODULES', in_Turboraster, '', AText, sizeof(AText), IniFile);
  ParseModule(Atext, Code, Inst);
  AModule := New(PModule, Init(in_TurboRaster + #0, Code, im_TurboRaster, Inst));
  ModuleData^.ModuleList^.Insert(AModule);

  GetPrivateProfileString('MODULES', in_GPSModule, '', AText, sizeof(AText), IniFile);
  ParseModule(AText, Code, Inst);
  AModule := New(PModule, Init(in_GPSModule + #0, Code, im_GPSModule, Inst));
  ModuleData^.ModuleList^.Insert(AModule);

  GetPrivateProfileString('MODULES', in_RoutePlaner, '', AText, sizeof(AText), IniFile);
  ParseModule(AText, Code, Inst);
  AModule := New(PModule, Init(in_RoutePlaner + #0, Code, im_RoutePlaner, Inst));
  ModuleData^.ModuleList^.Insert(AModule);

  ModuleData^.RegisterTRLib;
end;

procedure TIniFile.WriteModuleData;
var
  i: Integer;
  AModule: PModule;
  AStr: string;
begin
  with ModuleData^.ModuleList^ do
    for i := 0 to Count - 1 do
    begin
      AModule := PModule(At(i));
      if AModule^.Installed then
        AStr := 'Installed,'
      else
        AStr := 'NotInstalled,';
      AStr := AStr + AModule^.ModuleCode + #0;
      WritePrivateProfileString('MODULES', @AModule^.ModuleName[1],
        @AStr[1], IniFile);
    end;
end;

procedure TIniFile.ReadPrinter;
var
  Port: array[0..50] of Char;
  SetWinDef: Boolean;
begin
  with PrinterData do
  begin
    SetWinDef := FALSE;
    if GetPrivateProfileString('PRINT', 'DEFAULT', '', Default, SizeOf(Default), IniFile) = 0 then SetWinDef := TRUE;
    if GetProfileString('DEVICES', Default, '', Port, SizeOf(Port)) = 0 then SetWinDef := TRUE;
    if SetWinDef then GetDeviceName(Default, Printer, Port);
    StrCopy(Printer, Default);
  end;
end;

procedure TIniFile.SavePrinter;
begin
  with PrinterData do
    WritePrivateProfileString('PRINT', 'Default', Default, IniFile);
end;

procedure TIniFile.ReadAreaFact;
var
  sADivi: array[0..20] of Char;
  Error: Integer;
begin
  GetPrivateProfileString(SETTINGS, 'AreaDivider', '1', sADivi, SizeOf(sADivi), IniFile);
  Val(sADivi, AreaFact, Error);
  if (AreaFact <= 0) or (Error <> 0) then AreaFact := 1;
end;

procedure TIniFile.WriteDBSelOpt
  (
  DBSelMod: Boolean
  );
var
  sDBSOpt: array[0..15] of Char;
begin
  if DBSelMod = TRUE then
    StrCopy(sDBSOpt, 'All Layers')
  else
    StrCopy(sDBSOpt, 'TopLayer');
  WritePrivateProfileString(SETTINGS, 'DBSelOpt', sDBSOpt, IniFile);
end;

function TIniFile.ReadTranspSelOpt
  : Byte;
var
  sTranspSOpt: array[0..25] of Char;
begin
  GetPrivateProfileString(SETTINGS, 'TranspSelOpt', 'All Objects, All Layers', sTranspSOpt, SizeOf(sTranspSOpt), IniFile);
  if StrComp(sTranspSOpt, 'All Objects') = 0 then
    Result := $04 or $80 {alter Eintrag m�glich}
  else
    if StrComp(sTranspSOpt, 'TopObject') = 0 then
      Result := $04 or $40 {alter Eintrag m�glich}
    else
      if StrComp(sTranspSOpt, 'All Layers, All Objects') = 0 then
        Result := $04 or $80
      else
        if StrComp(sTranspSOpt, 'All Layers, Top Object') = 0 then
          Result := $04 or $40
        else
          if StrComp(sTranspSOpt, 'Top Layer, All Objects') = 0 then
            Result := $02 or $80
          else
            if StrComp(sTranspSOpt, 'Top Layer, Top Object') = 0 then
              Result := $02 or $40
            else
              if StrComp(sTranspSOpt, 'Top Object, All Objects') = 0 then
                Result := $01 or $80
              else
                if StrComp(sTranspSOpt, 'Top Object, Top Object') = 0 then
                  Result := $01 or $40
                else
                  Result := $04 or $80; {falscher Eintrag m�glich}
end;

procedure TIniFile.WriteTranspSelOpt
  (
  TranspSelMod: Byte
  );
var
  sTranspSOpt: array[0..25] of Char;
begin
  case TranspSelMod of
    65: StrCopy(sTranspSOpt, 'Top Object, Top Object');
    66: StrCopy(sTranspSOpt, 'Top Layer, Top Object');
    68: StrCopy(sTranspSOpt, 'All Layers, Top Object');
    129: StrCopy(sTranspSOpt, 'Top Object, All Objects');
    130: StrCopy(sTranspSOpt, 'Top Layer, All Objects');
    132: StrCopy(sTranspSOpt, 'All Layers, All Objects');
  else
    StrCopy(sTranspSOpt, 'All Layers, All Objects');
  end;
  WritePrivateProfileString(SETTINGS, 'TranspSelOpt', sTranspSOpt, IniFile);
end;

function TIniFile.ReadDiagOpts
  (
  var DType: Byte;
  var DWidth: LongInt;
  var DHeight: LongInt;
  var DDiam: LongInt;
  var DAbsRel: Boolean;
  var DAnnot: Boolean;
  var DAAbsRel: Boolean
  ): Boolean;
var
  sDiagOpts: array[0..30] of Char;
  DiagOpt: Real;
  Error: Integer;
begin
  GetPrivateProfileString('DIAGOPTIONS', 'DiagType', '1', sDiagOpts, SizeOf(sDiagOpts), IniFile);
  Val(sDiagOpts, DiagOpt, Error);
  if (Error = 0) and (DiagOpt >= 0) and (DiagOpt <= 5) then
    DType := Round(DiagOpt)
  else
    DType := 0;
  GetPrivateProfileString('DIAGOPTIONS', 'Width', '1.00', sDiagOpts, SizeOf(sDiagOpts), IniFile);
  Val(sDiagOpts, DiagOpt, Error);
  if Error = 0 then
    DWidth := Round(DiagOpt * 100)
  else
    DWidth := 100;
  GetPrivateProfileString('DIAGOPTIONS', 'Height', '1.00', sDiagOpts, SizeOf(sDiagOpts), IniFile);
  Val(sDiagOpts, DiagOpt, Error);
  if Error = 0 then
    DHeight := Round(DiagOpt * 100)
  else
    DHeight := 100;
  GetPrivateProfileString('DIAGOPTIONS', 'Diameter', '1.00', sDiagOpts, SizeOf(sDiagOpts), IniFile);
  Val(sDiagOpts, DiagOpt, Error);
  if Error = 0 then
    DDiam := Round(DiagOpt * 100)
  else
    DDiam := 100;
  GetPrivateProfileString('DIAGOPTIONS', 'CDAbsRel', '1', sDiagOpts, SizeOf(sDiagOpts), IniFile);
  if StrComp(sDiagOpts, '0') = 0 then
    DAbsRel := FALSE
  else
    DAbsRel := TRUE;
  GetPrivateProfileString('DIAGOPTIONS', 'Annotation', '0', sDiagOpts, SizeOf(sDiagOpts), IniFile);
  if StrComp(sDiagOpts, '1') = 0 then
    DAnnot := TRUE
  else
    DAnnot := FALSE;
  GetPrivateProfileString('DIAGOPTIONS', 'AnnotAbsRel', '1', sDiagOpts, SizeOf(sDiagOpts), IniFile);
  if StrComp(sDiagOpts, '0') = 0 then
    DAAbsRel := FALSE
  else
    DAAbsRel := TRUE;
end;

procedure TIniFile.WriteDiagOpts
  (
  DType: Byte;
  DWidth: LongInt;
  DHeight: LongInt;
  DDiam: LongInt;
  DAbsRel: Boolean;
  DAnnot: Boolean;
  DAAbsRel: Boolean
  );
var
  sDiagOpts: array[0..30] of Char;
  DiagOpt: Real;
  Error: Integer;
begin
  Str(DType: 0, sDiagOpts);
  WritePrivateProfileString('DIAGOPTIONS', 'DiagType', sDiagOpts, IniFile);
  DiagOpt := DWidth / 100;
  Str(DiagOpt: 0: 2, sDiagOpts);
  WritePrivateProfileString('DIAGOPTIONS', 'Width', sDiagOpts, IniFile);
  DiagOpt := DHeight / 100;
  Str(DiagOpt: 0: 2, sDiagOpts);
  WritePrivateProfileString('DIAGOPTIONS', 'Height', sDiagOpts, IniFile);
  DiagOpt := DDiam / 100;
  Str(DiagOpt: 0: 2, sDiagOpts);
  WritePrivateProfileString('DIAGOPTIONS', 'Diameter', sDiagOpts, IniFile);
  if DAbsRel then
    StrCopy(sDiagOpts, '1')
  else
    StrCopy(sDiagOpts, '0');
  WritePrivateProfileString('DIAGOPTIONS', 'CDAbsRel', sDiagOpts, IniFile);
  if DAnnot then
    StrCopy(sDiagOpts, '1')
  else
    StrCopy(sDiagOpts, '0');
  WritePrivateProfileString('DIAGOPTIONS', 'Annotation', sDiagOpts, IniFile);
  if DAAbsRel then
    StrCopy(sDiagOpts, '1')
  else
    StrCopy(sDiagOpts, '0');
  WritePrivateProfileString('DIAGOPTIONS', 'AnnotAbsRel', sDiagOpts, IniFile);
end;

{***********************************************************************************************}
{ Function TIniFile.ReadPassword                                                                }
{-----------------------------------------------------------------------------------------------}
{ Liest das kodierte Passwort aus dem Ini-File ([SETTINGS], FixLayers=). Ist kein Eintrag       }
{ vorhanden gibt ReadPassword eine Fehlermeldung aus und FALSE zur�ck, andernfalls steht        }
{ in APassWord das dekodierte Passwort.
{ MODIFYED BY BROVAK! NOW IT WORKING WITH REGISTRY }
{                                                     }
{-----------------------------------------------------------------------------------------------}
{ Parameter:                                                                                    }
{  APassWord       o = dekodiertes Pa�wort                                                      }
{-----------------------------------------------------------------------------------------------}
{ Ergebnis: FALSE, wenn die Definition nicht gelesen werden konnte, TRUE sonst                  }
{***********************************************************************************************}

function TIniFile.ReadPassword
  (
  var APassWord: string
  )
  : Boolean;
var
  Count: Integer;
  APuffer: string;
  InStr: string;
begin
  APuffer := PProj(Wingismainform.ActualProj).FixLayerPassword;
  InStr := '';
  for Count := 0 to Length(APuffer) div 2 - 1 do
    InStr := InStr + Char(HexToLong(Copy(APuffer, Count * 2 + 1, 2)));
  APassWord := UnCode(InStr);
  ReadPassWord := TRUE;

end;

function TIniFile.ReadLTPassword
  (
  var APassWord: string
  )
  : Boolean;
var
  Count: Integer;
  APuffer: string;
  InStr: string;
begin
  Count := GetPrivateProfileString(SETTINGS, 'LTProject', '', @APuffer[1], SizeOf(APuffer), IniFile);
  APuffer[0] := Char(Count);
  if Count = 0 then
  begin
    MsgBox(Parent.Handle, 10133, mb_IconExclamation or mb_OK, '');
    ReadLTPassword := FALSE;
  end
  else
  begin
    InStr := '';
    for Count := 0 to Length(APuffer) div 2 - 1 do
      InStr := InStr + Char(HexToLong(Copy(APuffer, Count * 2 + 1, 2)));
    APassWord := UnCode(InStr);
    ReadLTPassword := TRUE;
  end;
end;

{***********************************************************************************************}
{ Procedure TIniFile.WritePassword                                                              }
{-----------------------------------------------------------------------------------------------}
{ Kodiert das Pa�wort und schreibt es in das INI-File.                                          }
{ mod. by Brovak !!!!!! MODIFIED ! NOW IT WRITE TO REGISTRY !!!!!!
{-----------------------------------------------------------------------------------------------}
{ Parameter:                                                                                    }
{  APassWord       i = uncodiertes Pa�wort                                                      }
{***********************************************************************************************}

procedure TIniFile.WritePassword
  (
  APassWord: string
  );
var
  Cnt: Integer;
  OutStr, OutStr1: string;
begin
  APassWord := Code(APassWord);
  OutStr := '';
  for Cnt := 1 to Length(APassWord) do
    OutStr := OutStr + LongToHex(Byte(APassWord[Cnt]), 2);
  OutStr := OutStr + #0;
     //write password to project's registry
  PProj(Wingismainform.ActualProj).FixLayerPassword := Outstr;
  PProj(Wingismainform.ActualProj).Modified := true;

  {falsed password for hacker writed to ini  Thank SygSky for good idea :=)}
  Randomize; OutStr1 := '';
  for Cnt := 1 to Length(APassWord) do
    OutStr1 := OutStr1 + LongToHex(Random(Byte(APassWord[Cnt])), 2);
  OutStr1 := OutStr1 + #0;
  WritePrivateProfileString(SETTINGS, 'FixLayers', @OutStr1[1], IniFile);

end;

procedure TIniFile.WriteLTPassword
  (
  APassWord: string
  );
var
  Cnt: Integer;
  OutStr: string;
begin
  APassWord := Code(APassWord);
  OutStr := '';
  for Cnt := 1 to Length(APassWord) do
    OutStr := OutStr + LongToHex(Byte(APassWord[Cnt]), 2);
  OutStr := OutStr + #0;
  WritePrivateProfileString(SETTINGS, 'LTProject', @OutStr[1], IniFile);
end;

{***********************************************************************************************}
{ Procedure TIniFile.ReadGeneralDBSettings                                                      }
{-----------------------------------------------------------------------------------------------}
{ Liest die grunds�tzlichen Einstellungen f�r alle DDEML-Verbindungen aus der ini aus. Diese    }
{ Einstellungen dienen als Defaultwerte beim Auslesen der Verbindungsparameter der              }
{ einzelnen Datenbankverbindungen. Au�erdem sind sie f�r Verbindungen mit der alten             }
{ DDE-Schnittstelle entscheidend.                                                               }
{***********************************************************************************************}

procedure TIniFile.ReadGeneralDBSettings;
var
  Buffer: array[0..255] of Char;
  BufferValue: string;
  Value: Integer;
  Error: Integer;
  TmpStr: PChar;
begin
    {DDEMLConnect}
  GetPrivateProfileString(SETTINGS, 'DDEMLConnect', 'FALSE', Buffer, SizeOf(Buffer), IniFile);
  if (StrComp(Buffer, 'TRUE') = 0) or (StrComp(Buffer, '1') = 0) then
    DDEMLConnect := TRUE
  else
    DDEMLConnect := FALSE;
    {DDEMLSend}
  GetPrivateProfileString(SETTINGS, 'DDEMLSend', 'FALSE', Buffer, SizeOf(Buffer), IniFile);
  if (StrComp(Buffer, 'TRUE') = 0) or (StrComp(Buffer, '1') = 0) then
    DDEMLSend := TRUE
  else
    DDEMLSend := FALSE;
    {DBStringFormat}
  GetPrivateProfileString(SETTINGS, 'DBStringFormat', 'Wingis', Buffer, SizeOf(Buffer), IniFile);
  if StrComp(Buffer, 'Desma') = 0 then
    DBStringFormat := str_Desma
  else
    DBStringFormat := str_Wingis;
    {DBLayerInfos}
  GetPrivateProfileString(SETTINGS, 'DBLayerInfo', 'None', Buffer, SizeOf(Buffer), IniFile);
  if StrComp(Buffer, 'Index') = 0 then
    DBLayerInfos := li_Index
  else
    if StrComp(Buffer, 'Text') = 0 then
      DBLayerInfos := li_Text
    else
      DBLayerInfos := li_None;
    {DDESeparateSign}

  GetPrivateProfileString(SETTINGS, 'DDESeparateSign', ',', Buffer, SizeOf(Buffer), IniFile);
  if StrLComp(Buffer, '#', 1) = 0 then
  begin
    BufferValue := StrPas(Buffer);
    Delete(BufferValue, 1, 1);
    Val(BufferValue, Value, Error);
    if (Error = 0) and (Value >= 0) and (Value <= 255) then
      DDESeparateSign := Chr(Value)
    else
      DDESeparateSign := ',';
  end
  else
    DDESeparateSign := Buffer[0];

  GetPrivateProfileString(SETTINGS, 'DDESeperateSign', @DDESeparateSign, Buffer, SizeOf(Buffer), IniFile);
  if StrLComp(Buffer, '#', 1) = 0 then
  begin
    BufferValue := StrPas(Buffer);
    Delete(BufferValue, 1, 1);
    Val(BufferValue, Value, Error);
    if (Error = 0) and (Value >= 0) and (Value <= 255) then
      DDESeparateSign := Chr(Value)
    else
      DDESeparateSign := ',';
  end
  else
    DDESeparateSign := Buffer[0];

//  DDESeparateSign:=#255;

    {DDEHandler.FDDESepSign:=DDESeparateSign;}
    {DDEMLCallBack}
  GetPrivateProfileString(SETTINGS, 'DDEMLCallBack', '0', Buffer, SizeOf(Buffer), IniFile);
  if StrComp(Buffer, '1') = 0 then
    DDEMLCallBack := DBCBackDDE_
  else
    DDEMLCallBack := DBCBackBool;
    {MWReady}
  GetPrivateProfileString(SETTINGS, 'MainWindowReadyMessage', '0', Buffer, SizeOf(Buffer), IniFile);
  if (StrComp(Buffer, 'TRUE') = 0) or (StrComp(Buffer, '1') = 0) then
    MWReady := TRUE
  else
    MWReady := FALSE;
    {GReady}
  GetPrivateProfileString(SETTINGS, 'GraphicReadyMessage', '0', Buffer, SizeOf(Buffer), IniFile);
  if (StrComp(Buffer, 'TRUE') = 0) or (StrComp(Buffer, '1') = 0) then
    GReady := TRUE
  else
    GReady := FALSE;
    {GReadyAfterView}
  GetPrivateProfileString(SETTINGS, 'GraphicReadyMessageAfterView', '0', Buffer, SizeOf(Buffer), IniFile);
  if (StrComp(Buffer, 'TRUE') = 0) or (StrComp(Buffer, '1') = 0) then
    GReadyAfterView := TRUE
  else
    GReadyAfterView := FALSE;
    {DBSelMode}
  GetPrivateProfileString(SETTINGS, 'DBSelOpt', 'All Layers', Buffer, SizeOf(Buffer), IniFile);
  if StrComp(Buffer, 'TopLayer') = 0 then
    DBSelMode := FALSE
  else
    DBSelMode := TRUE;
    {SendLMSettings}
  GetPrivateProfileString(SETTINGS, 'LayerManagerInfo', 'FALSE', Buffer, SizeOf(Buffer), IniFile);
  if (StrComp(Buffer, 'TRUE') = 0) or (StrComp(Buffer, '1') = 0) then
    SendLMSettings := TRUE
  else
    SendLMSettings := FALSE;
    {ShowAfterDSA}
  GetPrivateProfileString(SETTINGS, 'ShowADSA', '0', Buffer, SizeOf(Buffer), IniFile);
  if (StrComp(Buffer, 'TRUE') = 0) or (StrComp(Buffer, '1') = 0) then
    ShowAfterDSA := TRUE
  else
    ShowAfterDSA := FALSE;
    {OneSelect}
  GetPrivateProfileString(SETTINGS, 'OneSelect', 'FALSE', Buffer, SizeOf(Buffer), IniFile);
  if (StrComp(Buffer, 'TRUE') = 0) or (StrComp(Buffer, '1') = 0) then
    OneSelect := TRUE
  else
    OneSelect := FALSE;
    {DBAutoInsert}
  GetPrivateProfileString(SETTINGS, 'DBAutoInsert', 'FALSE', Buffer, SizeOf(Buffer), IniFile);
  if (StrComp(Buffer, 'TRUE') = 0) or (StrComp(Buffer, '1') = 0) then
    DBAutoInsert := TRUE
  else
    DBAutoInsert := FALSE;
    {SendInfosToDB}
  GetPrivateProfileString(SETTINGS, 'SendInfosToDB', '', Buffer, SizeOf(Buffer), IniFile);
  SendInfosToDB := 0;
  TmpStr := Buffer;
  if (TmpStr <> nil) and (StrLen(TmpStr) > 0) then
  begin
    if StrLComp(TmpStr, '1', 1) = 0 then SendInfosToDB := SendInfosToDB or di_ProjOpen;
    TmpStr := AnsiNext(TmpStr);
    if (TmpStr <> nil) and (StrLen(TmpStr) > 0) then
    begin
      if StrLComp(TmpStr, '1', 1) = 0 then SendInfosToDB := SendInfosToDB or di_ProjSave;
      TmpStr := AnsiNext(TmpStr);
      if (TmpStr <> nil) and (StrLen(TmpStr) > 0) then
      begin
        if StrLComp(TmpStr, '1', 1) = 0 then SendInfosToDB := SendInfosToDB or di_ProjSaveAs;
        TmpStr := AnsiNext(TmpStr);
        if (TmpStr <> nil) and (StrLen(TmpStr) > 0) then
        begin
          if StrLComp(TmpStr, '1', 1) = 0 then SendInfosToDB := SendInfosToDB or di_ProjNew;
          TmpStr := AnsiNext(TmpStr);
          if (TmpStr <> nil) and (StrLen(TmpStr) > 0) then
          begin
            if StrLComp(TmpStr, '1', 1) = 0 then SendInfosToDB := SendInfosToDB or di_ProjChange;
            TmpStr := AnsiNext(TmpStr);
            if (TmpStr <> nil) and (StrLen(TmpStr) > 0) then
            begin
              if StrLComp(TmpStr, '1', 1) = 0 then SendInfosToDB := SendInfosToDB or di_ProjClose;
            end;
          end;
        end;
      end;
    end;
  end;
end;

{***********************************************************************************************}
{ Function TIniFile.ReadDDEMLWingisServerSettings                                               }
{-----------------------------------------------------------------------------------------------}
{ Liest die Verbindungsparameter des WinGIS-DDEML-Servers aus der ini aus.                      }
{-----------------------------------------------------------------------------------------------}
{ Parameter:                                                                                    }
{ Service          = Speicherbereich f�r den DDE-Service-String                                 }
{ Topic            = Speicherbereich f�r den DDE-Topic-String                                   }
{ Item             = Speicherbereich f�r den DDE-Item-String                                    }
{-----------------------------------------------------------------------------------------------}
{ Ergebnis: TRUE, wenn alle Parameter vorhanden sind, sonst FALSE                               }
{***********************************************************************************************}

function TIniFile.ReadDDEMLWingisServerSettings
  (
  Service: PChar;
  Topic: PChar;
  Item: PChar
  )
  : Boolean;
var
  CPuffer: string;
  APuffer: array[0..255] of Char;
  IniStr: PChar;
  Tmp: PChar;
begin
  Result := TRUE;
  GetPrivateProfileString('DDEMLSettings', 'WinGISDDEServer', 'WinGIS,System,Topics', APuffer, SizeOf(APuffer), IniFile);
  IniStr := APuffer;
  if StrComp(APuffer, '') <> 0 then
  begin
    Tmp := IniStr;
    IniStr := StrScan(IniStr, ',');
    if (StrComp(Tmp, '') <> 0) and (IniStr <> nil) then
    begin
      IniStr^ := #0;
      Inc(IniStr);
      StrCopy(Service, Tmp);
      Tmp := IniStr;
      IniStr := StrScan(IniStr, ',');
      if (StrComp(Tmp, '') <> 0) and (IniStr <> nil) then
      begin
        IniStr^ := #0;
        Inc(IniStr);
        StrCopy(Topic, Tmp);
        Tmp := IniStr;
        IniStr := StrScan(IniStr, ',');
        if StrComp(Tmp, '') <> 0 then
        begin
          StrCopy(Item, Tmp);
        end
        else
          Result := FALSE;
      end
      else
        Result := FALSE;
    end
    else
      Result := FALSE;
  end
  else
    Result := FALSE;
end;

{***********************************************************************************************}
{ Procedure TIniFile.ReadDDEMLApps                                                              }
{-----------------------------------------------------------------------------------------------}
{ Liest die in der ini vorhandenen Eintr�ge f�r Appliktionen mit der neuen DDEML-Schnittstelle  }
{ aus und speichert deren Parameter in der Liste DDEMLApps des DDEHandlers.                     }
{-----------------------------------------------------------------------------------------------}
{ Parameter:                                                                                    }
{ DDEMLApps        = Zeiger auf die Collection DDEMLApps des DDEHandlers                        }
{***********************************************************************************************}

{
procedure TIniFile.ReadDDEMLApps
  (
  DDEMLApps: PCollection
  );
var
  AppList: PStrCollection;
  procedure ReadSettingsNew
      (
      Item: PChar
      );
  var
    SectionName: array[0..255] of Char;
    ABuffer: array[0..255] of Char;
    AExeFile: array[0..255] of Char;
    AStartParams: array[0..255] of Char;
    AService: array[0..255] of Char;
    ATopic: array[0..255] of Char;
    AItem: array[0..255] of Char;
    AConnect: Boolean;
    ASendStrings: Boolean;
    AStrFormat: Word;
    ADBLayerInfo: Word;
    ASepSign: Char;
    ACallBack: Word;
    AMWReady: Boolean;
    AGReady: Boolean;
    AGReadyAV: Boolean;
    ADBSelMode: Boolean;
    ASLMSettings: Boolean;
    AShowADSA: Boolean;
    AOneSelect: Boolean;
    ADBAutoEIN: Boolean;
    ASIPOpen: Word;
    ASIPSave: Word;
    ASIPSaveAs: Word;
    ASIPNew: Word;
    ASIPChange: Word;
    ASIPClose: Word;
    ValidEntry: Boolean;
  begin
    StrCopy(SectionName, 'DDEML');
    StrCat(SectionName, Item);
    GetPrivateProfileString(SectionName, 'Application', '', AExeFile, SizeOf(AExeFile), IniFile);
    GetPrivateProfileString(SectionName, 'StartParams', '', AStartParams, SizeOf(AStartParams), IniFile);
    GetPrivateProfileString(SectionName, 'Service', '', AService, SizeOf(AService), IniFile);
    GetPrivateProfileString(SectionName, 'Topic', '', ATopic, SizeOf(ATopic), IniFile);
    GetPrivateProfileString(SectionName, 'Item', '', AItem, SizeOf(AItem), IniFile);
    if (StrComp(AExeFile, '') = 0) or (StrComp(AService, '') = 0)
      or (StrComp(ATopic, '') = 0) or (StrComp(AItem, '') = 0) then
      ValidEntry := FALSE
    else
    begin
      ValidEntry := TRUE;
      GetPrivateProfileString(SectionName, 'DDEMLConnect', '', ABuffer, SizeOf(ABuffer), IniFile);
      if StrComp(ABuffer, '') = 0 then
        AConnect := DDEMLConnect
      else
        if (StrComp(ABuffer, 'TRUE') = 0) or (StrComp(ABuffer, '1') = 0) then
          AConnect := TRUE
        else
          AConnect := FALSE;
      GetPrivateProfileString(SectionName, 'DDEMLSend', '', ABuffer, SizeOf(ABuffer), IniFile);
      if StrComp(ABuffer, '') = 0 then
        ASendStrings := DDEMLSend
      else
        if (StrComp(ABuffer, 'TRUE') = 0) or (StrComp(ABuffer, '1') = 0) then
          ASendStrings := TRUE
        else
          ASendStrings := FALSE;
      GetPrivateProfileString(SectionName, 'DBStringFormat', '', ABuffer, SizeOf(ABuffer), IniFile);
      if StrComp(ABuffer, '') = 0 then
        AStrFormat := DBStringFormat
      else
        if StrComp(ABuffer, 'Desma') = 0 then
          AStrFormat := str_Desma
        else
          AStrFormat := str_Wingis;
      GetPrivateProfileString(SectionName, 'DBLayerInfo', '', ABuffer, SizeOf(ABuffer), IniFile);
      if StrComp(ABuffer, '') = 0 then
        ADBLayerInfo := DBLayerInfos
      else
        if StrComp(ABuffer, 'Index') = 0 then
          ADBLayerInfo := li_Index
        else
          if StrComp(ABuffer, 'Text') = 0 then
            ADBLayerInfo := li_Text
          else
            ADBLayerInfo := li_None;
      GetPrivateProfileString(SectionName, 'DDESeparateSign', '', ABuffer, SizeOf(ABuffer), IniFile);
      if StrComp(ABuffer, '') = 0 then
        ASepSign := DDESeparateSign
      else
        ASepSign := ABuffer[0];
      GetPrivateProfileString(SectionName, 'DDEMLCallBack', '', ABuffer, SizeOf(ABuffer), IniFile);
      if StrComp(ABuffer, '') = 0 then
        ACallBack := DDEMLCallBack
      else
        if StrComp(ABuffer, '1') = 0 then
          ACallBack := DBCBackDDE_
        else
          ACallBack := DBCBackBool;
      GetPrivateProfileString(SectionName, 'MainWindowReadyMessage', '', ABuffer, SizeOf(ABuffer), IniFile);
      if StrComp(ABuffer, '') = 0 then
        AMWReady := MWReady
      else
        if (StrComp(ABuffer, 'TRUE') = 0) or (StrComp(ABuffer, '1') = 0) then
          AMWReady := TRUE
        else
          AMWReady := FALSE;
      GetPrivateProfileString(SectionName, 'GraphicReadyMessage', '', ABuffer, SizeOf(ABuffer), IniFile);
      if StrComp(ABuffer, '') = 0 then
        AGReady := GReady
      else
        if (StrComp(ABuffer, 'TRUE') = 0) or (StrComp(ABuffer, '1') = 0) then
          AGReady := TRUE
        else
          AGReady := FALSE;
      GetPrivateProfileString(SectionName, 'GraphicReadyMessageAfterView', '', ABuffer, SizeOf(ABuffer), IniFile);
      if StrComp(ABuffer, '') = 0 then
        AGReadyAV := GReadyAfterView
      else
        if (StrComp(ABuffer, 'TRUE') = 0) or (StrComp(ABuffer, '1') = 0) then
          AGReadyAV := TRUE
        else
          AGReadyAV := FALSE;
      GetPrivateProfileString(SectionName, 'DBSelOpt', '', ABuffer, SizeOf(ABuffer), IniFile);
      if StrComp(ABuffer, '') = 0 then
        ADBSelMode := DBSelMode
      else
        if StrComp(ABuffer, 'TopLayer') = 0 then
          ADBSelMode := FALSE
        else
          ADBSelMode := TRUE;
      GetPrivateProfileString(SectionName, 'LayerManagerInfo', '', ABuffer, SizeOf(ABuffer), IniFile);
      if StrComp(ABuffer, '') = 0 then
        ASLMSettings := DDEMLSend
      else
        if (StrComp(ABuffer, 'TRUE') = 0) or (StrComp(ABuffer, '1') = 0) then
          ASLMSettings := TRUE
        else
          ASLMSettings := FALSE;
      GetPrivateProfileString(SectionName, 'ShowADSA', '', ABuffer, SizeOf(ABuffer), IniFile);
      if StrComp(ABuffer, '') = 0 then
        AShowADSA := ShowAfterDSA
      else
        if (StrComp(ABuffer, 'TRUE') = 0) or (StrComp(ABuffer, '1') = 0) then
          AShowADSA := TRUE
        else
          AShowADSA := FALSE;
      GetPrivateProfileString(SectionName, 'OneSelect', '', ABuffer, SizeOf(ABuffer), IniFile);
      if StrComp(ABuffer, '') = 0 then
        AOneSelect := OneSelect
      else
        if (StrComp(ABuffer, 'TRUE') = 0) or (StrComp(ABuffer, '1') = 0) then
          AOneSelect := TRUE
        else
          AOneSelect := FALSE;
      GetPrivateProfileString(SectionName, 'DBAutoInsert', '', ABuffer, SizeOf(ABuffer), IniFile);
      if StrComp(ABuffer, '') = 0 then
        ADBAutoEIN := DBAutoInsert
      else
        if (StrComp(ABuffer, 'TRUE') = 0) or (StrComp(ABuffer, '1') = 0) then
          ADBAutoEIN := TRUE
        else
          ADBAutoEIN := FALSE;
      GetPrivateProfileString(SectionName, 'SendInfoProjectOpen', '', ABuffer, SizeOf(ABuffer), IniFile);
      if StrComp(ABuffer, '') = 0 then
      begin
        if (SendInfosToDB or di_ProjOpen) <> 0 then
          ASIPOpen := 1
        else
          ASIPOpen := 0;
      end
      else
        if StrComp(ABuffer, '1') = 0 then
          ASIPOpen := 1
        else
          ASIPOpen := 0;
      GetPrivateProfileString(SectionName, 'SendInfoProjectSave', '', ABuffer, SizeOf(ABuffer), IniFile);
      if StrComp(ABuffer, '') = 0 then
      begin
        if (SendInfosToDB or di_ProjSave) <> 0 then
          ASIPSave := 1
        else
          ASIPSave := 0;
      end
      else
        if StrComp(ABuffer, '1') = 0 then
          ASIPSave := 1
        else
          ASIPSave := 0;
      GetPrivateProfileString(SectionName, 'SendInfoProjectSaveAs', '', ABuffer, SizeOf(ABuffer), IniFile);
      if StrComp(ABuffer, '') = 0 then
      begin
        if (SendInfosToDB or di_ProjSaveAs) <> 0 then
          ASIPSaveAs := 1
        else
          ASIPSaveAs := 0;
      end
      else
        if StrComp(ABuffer, '1') = 0 then
          ASIPSaveAs := 1
        else
          ASIPSaveAs := 0;
      GetPrivateProfileString(SectionName, 'SendInfoProjectNew', '', ABuffer, SizeOf(ABuffer), IniFile);
      if StrComp(ABuffer, '') = 0 then
      begin
        if (SendInfosToDB or di_ProjNew) <> 0 then
          ASIPNew := 1
        else
          ASIPNew := 0;
      end
      else
        if StrComp(ABuffer, '1') = 0 then
          ASIPNew := 1
        else
          ASIPNew := 0;
      GetPrivateProfileString(SectionName, 'SendInfoProjectChange', '', ABuffer, SizeOf(ABuffer), IniFile);
      if StrComp(ABuffer, '') = 0 then
      begin
        if (SendInfosToDB or di_ProjChange) <> 0 then
          ASIPChange := 1
        else
          ASIPChange := 0;
      end
      else
        if StrComp(ABuffer, '1') = 0 then
          ASIPChange := 1
        else
          ASIPChange := 0;
      GetPrivateProfileString(SectionName, 'SendInfoProjectClose', '', ABuffer, SizeOf(ABuffer), IniFile);
      if StrComp(ABuffer, '') = 0 then
      begin
        if (SendInfosToDB or di_ProjClose) <> 0 then
          ASIPClose := 1
        else
          ASIPClose := 0;
      end
      else
        if StrComp(ABuffer, '1') = 0 then
          ASIPClose := 1
        else
          ASIPClose := 0;
      if ValidEntry then
      begin
        DDEMLApps^.Insert(New(PDDEMLApp, Init(Item, AExeFile, AStartParams, AService, ATopic, AItem,
          AConnect, FALSE, ASendStrings, FALSE, AStrFormat, ADBLayerInfo,
          ASepSign, ACallBack, AMWReady, AGReady, AGReadyAV, ADBSelMode,
          ASLMSettings, AShowADSA, AOneSelect, ADBAutoEIN, ASIPOpen,
          ASIPSave, ASIPSaveAs, ASIPNew, ASIPChange, ASIPClose)));
        DatabaseSettings^.Databases^.Insert(New(PDBSetting, Init(Item, AConnect, ASendStrings)));
      end;
    end;
  end;
  procedure DoAll
      (
      Item: PChar
      ); far;
  var
    APuffer: array[0..1024] of Char;
    AExeFile: array[0..256] of Char;
    AService: array[0..128] of Char;
    ATopic: array[0..128] of Char;
    AItem: array[0..128] of Char;
    AConnect: Boolean;
    ASendStrings: Boolean;
    AStrFormat: Word;
    ADBLayerInfo: Word;
    ValidEntry: Boolean;
    Count: Integer;
    Scan: PChar;
    Start: PChar;
    Tmp: array[0..255] of Char;
    function DoCopy
        (
        Dest: PChar
        )
        : Boolean;
    begin
      Start := Scan + 1;
      Scan := StrScan(Scan + 1, ',');
      if Scan <> nil then
      begin
        Scan^ := #0;
        if Dest <> nil then StrCopy(Dest, Start);
        DoCopy := TRUE;
      end
      else
        DoCopy := FALSE;
    end;
  begin
    ValidEntry := FALSE;
    Count := GetPrivateProfileString('DDEMLSettings', Item, '', APuffer, SizeOf(APuffer), IniFile);
    if StrComp(APuffer, 'Section') = 0 then
      ReadSettingsNew(Item)
    else
    begin
      if Count <> 0 then
      begin
        if APuffer[StrLen(APuffer) - 1] <> ',' then StrCat(APuffer, ',');
        Scan := @APuffer;
        Dec(Scan);
        if DoCopy(AExeFile) and DoCopy(AService) and DoCopy(ATopic) and DoCopy(AItem) then
        begin
          ValidEntry := TRUE;
          AConnect := FALSE;
          ASendStrings := FALSE;
          AStrFormat := 0;
          ADBLayerInfo := 0;

          if DoCopy(Tmp) then
          begin
            if StrComp(Tmp, 'TRUE') = 0 then
              AConnect := TRUE
            else
              AConnect := FALSE;
            if DoCopy(Tmp) then
            begin
              if StrComp(Tmp, 'TRUE') = 0 then
                ASendStrings := TRUE
              else
                ASendStrings := FALSE;
              if DoCopy(Tmp) then
              begin
                if StrComp(Tmp, 'Desma') = 0 then
                  AStrFormat := str_Desma
                else
                  AStrFormat := str_Wingis;
                if DoCopy(Tmp) then
                begin
                  if StrComp(Tmp, 'Index') = 0 then
                    ADBLayerInfo := li_Index
                  else
                    if StrComp(Tmp, 'Text') = 0 then
                      ADBLayerInfo := li_Text
                    else
                      ADBLayerInfo := li_None;
                end;
              end;
            end;
          end;
        end
        else
          Count := 0;
      end;
      if ValidEntry then
      begin
        DDEMLApps^.Insert(New(PDDEMLApp, Init(Item, AExeFile, nil, AService, ATopic, AItem, AConnect, FALSE,
          ASendStrings, FALSE, AStrFormat, ADBLayerInfo, DDESeparateSign,
          DDEMLCallBack, MWReady, GReady, GReadyAfterView, DBSelMode,
          SendLMSettings, ShowAfterDSA, OneSelect, DBAutoInsert,
          SendInfosToDB and di_ProjOpen,
          (SendInfosToDB and di_ProjSave) shr 1,
          (SendInfosToDB and di_ProjSaveAs) shr 2,
          (SendInfosToDB and di_ProjNew) shr 3,
          (SendInfosToDB and di_ProjChange) shr 4,
          (SendInfosToDB and di_ProjClose) shr 5)));
        DatabaseSettings^.Databases^.Insert(New(PDBSetting, Init(Item, AConnect, ASendStrings)));
      end;
    end;
  end;
begin
  if GetList(TRUE, 'DDEMLSettings', AppList) then
  begin
    AppList^.ForEach(@DoAll);
    Dispose(AppList, Done);
  end;
end;
}

{***********************************************************************************************}
{ Procedure TIniFile.ReadDDEApps                                                                }
{-----------------------------------------------------------------------------------------------}
{ Liest die in der ini vorhandenen Eintr�ge f�r Applikationen mit der alten DDE-Schnittstelle   }
{ aus und speichert deren Parameter in der Liste DDEApps des DDEHandlers.                       }
{-----------------------------------------------------------------------------------------------}
{ Parameter:                                                                                    }
{ DDEApps          = Zeiger auf die Collection DDEApps des DDEHandlers                          }
{***********************************************************************************************}

{
procedure TIniFile.ReadDDEApps
  (
  DDEApps: PCollection
  );
var
  AppList: PStrCollection;
  ActiveApp: PDDEApp;
  AName: array[0..255] of Char;
  Count: Integer;
  procedure DoAll
      (
      Item: PChar
      ); far;
  var
    APuffer: array[0..1024] of Char;
    AExeFile: array[0..255] of Char;
    AService: array[0..255] of Char;
    ATopic: array[0..255] of Char;
    ANumber: array[0..255] of Char;
    AMenu: Integer;
    ASendStrings: Boolean;
    AStrFormat: Word;
    ADBLayerInfo: Word;
    ValidEntry: Boolean;
    Scan: PChar;
    Start: PChar;
    Tmp: array[0..255] of Char;
    Error: Integer;
    function DoCopy
        (
        Dest: PChar
        )
        : Boolean;
    begin
      Start := Scan + 1;
      Scan := StrScan(Scan + 1, ',');
      if Scan <> nil then
      begin
        Scan^ := #0;
        if Dest <> nil then StrCopy(Dest, Start);
        DoCopy := TRUE;
      end
      else
        DoCopy := FALSE;
    end;
  begin
    ValidEntry := FALSE;
    Count := GetPrivateProfileString('DBApplications', Item, '', APuffer, SizeOf(APuffer), IniFile);
    if Count <> 0 then
    begin
      StrCat(APuffer, ',');
      Scan := @APuffer;
      Dec(Scan);
      if DoCopy(AExeFile) and DoCopy(ANumber) and DoCopy(AService) and DoCopy(ATopic) then
      begin
        ValidEntry := TRUE;
        ASendStrings := TRUE;
        AStrFormat := 0;
        ADBLayerInfo := 0;
        if DoCopy(Tmp) then
        begin
          if StrComp(Tmp, 'FALSE') = 0 then
            ASendStrings := FALSE
          else
            ASendStrings := TRUE;
          if DoCopy(Tmp) then
          begin
            if StrComp(Tmp, 'Desma') = 0 then
              AStrFormat := 1
            else
              AStrFormat := 0;
            if DoCopy(Tmp) then
            begin
              if StrComp(Tmp, 'Index') = 0 then
                ADBLayerInfo := 2
              else
                if StrComp(Tmp, 'Text') = 0 then
                  ADBLayerInfo := 1
                else
                  ADBLayerInfo := 0;
            end;
          end;
        end;
      end
      else
        Count := 0;
    end;
    Val(ANumber, AMenu, Error);
    if Error <> 0 then ValidEntry := FALSE;
    if ValidEntry then
      DDEApps^.Insert(New(PDDEApp, Init(Item, AExeFile, AService, ATopic,
        AMenu, ASendStrings, AStrFormat, ADBLayerInfo)));
  end;
  function SearchActiveApp
      (
      Item: PDDEApp
      )
      : Boolean; far;
  begin
    Result := StrComp(Item^.Name, PChar(@AName)) = 0;
  end;
begin
  if GetList(TRUE, 'DBApplications', AppList) then
  begin
    AppList^.ForEach(@DoAll);
    Count := GetPrivateProfileString(SETTINGS, 'DBApplication', '', AName, SizeOf(AName), IniFile);
    if Count = 0 then
      MsgBox(Parent.Handle, 10148, mb_OK or mb_IconExclamation, '')
    else
    begin;
      ActiveApp := DDEApps^.FirstThat(@SearchActiveApp);
      if ActiveApp <> nil then
        ActiveApp^.Connect := TRUE
      else
        MsgBox(Parent.Handle, 10148, mb_OK or mb_IconExclamation, '');
    end;
  end;
  if AppList <> nil then Dispose(AppList, Done);
end;
}

{***********************************************************************************************}
{ Function TIniFile.WriteDDEDDEMLSettings                                                       }
{-----------------------------------------------------------------------------------------------}
{ Schreibt die im DDE-Settings-Dialog des Optionen-Dialoges eingestellten Werte f�r die         }
{ einzelnen DDE- und DDEML-Verbindungen ins IniFile.                                            }
{-----------------------------------------------------------------------------------------------}
{ ANewDDEList    = Collection mit den Einstellungen f�r den alten DDE-Standard                  }
{ ANewDDEMLList  = Collection mit den Einstellungen f�r den neuen DDEML-Standard                }

{-----------------------------------------------------------------------------------------------}
{ Ergebnis: TRUE                                                                                }
{***********************************************************************************************}

{
function TIniFile.WriteDDEDDEMLSettings
  (
  ANewDDEList: PCollection;
  ANewDDEMLList: PCollection
  )
  : Boolean;
  procedure WriteDDESettings
      (
      Item: PDDEApp
      ); far;
  var
    WriteStr: array[0..1024] of Char;
    TmpStr: array[0..255] of Char;
  begin
    with Item^ do
    begin
      if ExeFile <> nil then
        StrCopy(WriteStr, ExeFile)
      else
        StrCopy(WriteStr, '');
      StrCat(WriteStr, ',');
      Str(Menu: 0, TmpStr);
      StrCat(WriteStr, TmpStr);
      StrCat(WriteStr, ',');
      if Service <> nil then StrCat(WriteStr, Service);
      StrCat(WriteStr, ',');
      if Topic <> nil then StrCat(WriteStr, Topic);
      StrCat(WriteStr, ',');
      if SendStrings then
        StrCat(WriteStr, 'TRUE')
      else
        StrCat(WriteStr, 'FALSE');
      StrCat(WriteStr, ',');
      if StrFormat = 1 then
        StrCat(WriteStr, 'Desma')
      else
        StrCat(WriteStr, 'Wingis');
      StrCat(WriteStr, ',');
      if DBCLayerInf = 2 then
        StrCat(WriteStr, 'Index')
      else
        if DBCLayerInf = 1 then
          StrCat(WriteStr, 'Text')
        else
          StrCat(WriteStr, 'None');
      WritePrivateProfileString('DBApplications', Name, WriteStr, IniFile);
      if Connect then WritePrivateProfileString(SETTINGS, 'DBApplication', Name, IniFile);
    end;
  end;
  procedure WriteDDEMLSettings
      (
      Item: PDDEMLApp
      ); far;
  var
    SecName: array[0..255] of Char;
    TmpStr: array[0..255] of Char;
    TmpChar: PChar;
  begin
    with Item^ do
    begin
      WritePrivateProfileString('DDEMLSettings', Name, 'Section', IniFile);
      StrCopy(SecName, 'DDEML');
      StrCat(SecName, Name);
      WritePrivateProfileString(SecName, 'Application', ExeFile, IniFile);
      WritePrivateProfileString(SecName, 'StartParams', StartParams, IniFile);
      WritePrivateProfileString(SecName, 'Service', Service, IniFile);
      WritePrivateProfileString(SecName, 'Topic', Topic, IniFile);
      WritePrivateProfileString(SecName, 'Item', Item, IniFile);
      if Connect then
        StrCopy(TmpStr, 'TRUE')
      else
        StrCopy(TmpStr, 'FALSE');
      WritePrivateProfileString(SecName, 'DDEMLConnect', TmpStr, IniFile);
      if SendStrings then
        StrCopy(TmpStr, 'TRUE')
      else
        StrCopy(TmpStr, 'FALSE');
      WritePrivateProfileString(SecName, 'DDEMLSend', TmpStr, IniFile);
      if StrFormat = DesmaStrFmt then
        StrCopy(TmpStr, 'Desma')
      else
        StrCopy(TmpStr, 'Wingis');
      WritePrivateProfileString(SecName, 'DBStringFormat', TmpStr, IniFile);
      if DBCLayerInf = DBLInfoIndex then
        StrCopy(TmpStr, 'Index')
      else
        if DBCLayerInf = DBLInfoText then
          StrCopy(TmpStr, 'Text')
        else
          StrCopy(TmpStr, 'None');
      WritePrivateProfileString(SecName, 'DBLayerInfo', TmpStr, IniFile);
      TmpStr[0] := CDDESepSign;
      TmpChar := AnsiNext(TmpStr);
      TmpChar[0] := #0;
      WritePrivateProfileString(SecName, 'DDESeparateSign', TmpStr, IniFile);
      if CCallBack = 1 then
        StrCopy(TmpStr, '1')
      else
        StrCopy(TmpStr, '0');
      WritePrivateProfileString(SecName, 'DDEMLCallBack', TmpStr, IniFile);
      if CMWReady then
        StrCopy(TmpStr, 'TRUE')
      else
        StrCopy(TmpStr, 'FALSE');
      WritePrivateProfileString(SecName, 'MainWindowReadyMessage', TmpStr, IniFile);
      if CGReady then
        StrCopy(TmpStr, 'TRUE')
      else
        StrCopy(TmpStr, 'FALSE');
      WritePrivateProfileString(SecName, 'GraphicReadyMessage', TmpStr, IniFile);
      if CGReadyAV then
        StrCopy(TmpStr, 'TRUE')
      else
        StrCopy(TmpStr, 'FALSE');
      WritePrivateProfileString(SecName, 'GraphicReadyMessageAfterView', TmpStr, IniFile);
      if CDBSelMode then
        StrCopy(TmpStr, 'All Layers')
      else
        StrCopy(TmpStr, 'TopLayer');
      WritePrivateProfileString(SecName, 'DBSelOpt', TmpStr, IniFile);
      if CSendLMInfo then
        StrCopy(TmpStr, 'TRUE')
      else
        StrCopy(TmpStr, 'FALSE');
      WritePrivateProfileString(SecName, 'LayerManagerInfo', TmpStr, IniFile);
      if CShowADSA then
        StrCopy(TmpStr, 'TRUE')
      else
        StrCopy(TmpStr, 'FALSE');
      WritePrivateProfileString(SecName, 'ShowADSA', TmpStr, IniFile);
      if COneSelect then
        StrCopy(TmpStr, 'TRUE')
      else
        StrCopy(TmpStr, 'FALSE');
      WritePrivateProfileString(SecName, 'OneSelect', TmpStr, IniFile);
      if CDBAutoIns then
        StrCopy(TmpStr, 'TRUE')
      else
        StrCopy(TmpStr, 'FALSE');
      WritePrivateProfileString(SecName, 'DBAutoInsert', TmpStr, IniFile);
      if COpenInfo = 1 then
        StrCopy(TmpStr, '1')
      else
        StrCopy(TmpStr, '0');
      WritePrivateProfileString(SecName, 'SendInfoProjectOpen', TmpStr, IniFile);
      if CSaveInfo = 1 then
        StrCopy(TmpStr, '1')
      else
        StrCopy(TmpStr, '0');
      WritePrivateProfileString(SecName, 'SendInfoProjectSave', TmpStr, IniFile);
      if CSaveAsInfo = 1 then
        StrCopy(TmpStr, '1')
      else
        StrCopy(TmpStr, '0');
      WritePrivateProfileString(SecName, 'SendInfoProjectSaveAs', TmpStr, IniFile);
      if CNewInfo = 1 then
        StrCopy(TmpStr, '1')
      else
        StrCopy(TmpStr, '0');
      WritePrivateProfileString(SecName, 'SendInfoProjectNew', TmpStr, IniFile);
      if CNewInfo = 1 then
        StrCopy(TmpStr, '1')
      else
        StrCopy(TmpStr, '0');
      WritePrivateProfileString(SecName, 'SendInfoProjectChange', TmpStr, IniFile);
      if CNewInfo = 1 then
        StrCopy(TmpStr, '1')
      else
        StrCopy(TmpStr, '0');
      WritePrivateProfileString(SecName, 'SendInfoProjectClose', TmpStr, IniFile);
    end;
  end;
begin
  ANewDDEList^.ForEach(@WriteDDESettings);
  ANewDDEMLList^.ForEach(@WriteDDEMLSettings);
  Result := TRUE;
end;
}

{*****************************************************************************************************************************}

procedure TIniFile.ReadSelOptions;
var
  Buffer: array[0..20] of Char;
begin
  GetPrivateProfileString(SETTINGS, 'FrameSelect', 'TRUE', Buffer, SizeOf(Buffer), IniFile);
  if StrComp(StrUpper(Buffer), 'FALSE') = 0 then
    FrameSelect := FALSE
  else
    FrameSelect := TRUE;
end;

function TIniFile.GetRealVidMode(Res: PChar): Integer;
begin
  GetRealVidMode := GetPrivateProfileInt('TRMODES', Res, 46, IniFile);
end;

{===============================================================================================}
{ Diverses                                                                                      }
{===============================================================================================}

{***********************************************************************************************}
{ Function Code                                                                                 }
{-----------------------------------------------------------------------------------------------}
{ Codiert AStr. Das erste Zeichen wird mit $3F XOr-verkn�pft. Jedes weitere Zeichen             }
{ mit dem uncodierten Zeichen davor.                                                            }
{-----------------------------------------------------------------------------------------------}
{ Parameter:                                                                                    }
{  AStr            i = zu codierender String                                                    }
{-----------------------------------------------------------------------------------------------}
{ Ergebnis: codierter String                                                                    }
{***********************************************************************************************}

function Code
  (
  AStr: string
  )
  : string;
var
  Cnt: Integer;
begin
  AStr[1] := Char(Byte(AStr[1]) xor $3F);
  for Cnt := 2 to Length(AStr) do
    AStr[Cnt] := Char(Byte(AStr[Cnt]) xor Byte(AStr[Cnt - 1]));
  Code := AStr;
end;

{***********************************************************************************************}
{ Function UnCode                                                                               }
{-----------------------------------------------------------------------------------------------}
{ Dekodiert einen String, der mit Code kodiert wurde.                                           }
{-----------------------------------------------------------------------------------------------}
{ Parameter:                                                                                    }
{  AStr            i = zu decodierender String                                                  }
{-----------------------------------------------------------------------------------------------}
{ Ergebnis: decodierter String                                                                  }
{***********************************************************************************************}

function UnCode
  (
  AStr: string
  )
  : string;
var
  Code: Byte;
  NewChar: Char;
  Cnt: Integer;
begin
  Code := $3F;
  for Cnt := 1 to Length(AStr) do
  begin
    NewChar := Char(Byte(AStr[Cnt]) xor Code);
    Code := Byte(AStr[Cnt]);
    AStr[Cnt] := NewChar;
  end;
  UnCode := AStr;
end;

function TIniFile.ReadFloat
  (
  AppName: PChar;
  KeyName: PChar;
  const ADefault: Double
  )
  : Double;
var
  Str: array[0..50] of Char;
  Error: Integer;
begin
  GetPrivateProfileString(AppName, KeyName, '', Str, SizeOf(Str), IniFile);
  if StrComp(Str, '') = 0 then
    Result := ADefault
  else
  begin
    Val(Str, Result, Error);
    if Error <> 0 then Result := ADefault;
  end;
end;

function TIniFile.ReadInteger
  (
  AppName: PChar;
  KeyName: PChar;
  ADefault: LongInt
  )
  : LongInt;
var
  Str: array[0..50] of Char;
  Error: Integer;
begin
  GetPrivateProfileString(AppName, KeyName, '', Str, SizeOf(Str), IniFile);
  if StrComp(Str, '') = 0 then
    Result := ADefault
  else
    if StrIComp(Str, 'TRUE') = 0 then
      Result := 1
    else
      if StrIComp(Str, 'FALSE') = 0 then
        Result := 0
      else
      begin
        Val(Str, Result, Error);
        if Error <> 0 then Result := ADefault;
      end;
end;

procedure TIniFile.WriteFloat
  (
  AppName: PChar;
  KeyName: PChar;
  const AValue: Double
  );
var
  ValStr: array[0..50] of Char;
begin
  Str(AValue: 0: 2, ValStr);
  WritePrivateProfileString(AppName, KeyName, ValStr, IniFile);
end;

procedure TIniFile.ReadFloatingTextSettings;
var
  aStr: array[0..255] of Char;
begin
  with TooltipThread do
  begin
    GetPrivateProfileString('FloatingText', 'Fontname', str_FontToolTip, aStr, SizeOf(aStr), IniFile);
    FTStyle := StrPas(aStr);
    FTColor := ReadInteger('FloatingText', 'TextColor', RGB_TextToolTip);
    FTBorderColor := ReadInteger('FloatingText', 'BorderColor', RGB_BorderToolTip);
    FTBackColor := ReadInteger('FloatingText', 'BackColor', RGB_BackToolTip);
  end;
end;

//++Sygsky: to read RGB QUADs for 1 bit PRI painting
var
  JHC: PChar = 'JHC!@#$%^&*()_+0987654321`~|\';
  RGBQuadDelimiters: TCharSet = [#32, ';', ',', '!', ':', '-', '+'];

function TIniFile.ReadRGBQuad(AppName, KeyName: PChar; ADefault: DWORD): DWORD;
var
  Buf: array[0..100] of Char;
  AStr: AnsiString;
  Word: string[16];
  i: Integer;
  Val: DWORD;
  Mask: DWORD;
begin
  Result := aDefault;
  i := GetPrivateProfileString(SETTINGS, KeyName, JHC, Buf, 100, IniFile);
  SetString(AStr, Buf, i);
  if AStr = JHC then // Returns default value as nothing was detected
    Exit;
  Q_SpaceCompressInPlace(AStr); // Trim+Delete multiple spaces
  // Now the text should be in form '### ### ###' where numbers stands for
  // corresponding RED,GREEN,BLUE values in decimal form, i.e. '255 0 0' will stand for RED
  // '0 255 0' for GREEN etc.
  // If you will set only 2 numbers,only R and B will be set, if
  // you set only 1, R will be set, all other will be filled from defult values!
  for i := 1 to 3 do // Read R, G and B words
  begin
    Word := Q_GetWordN(i, AStr, RGBQuadDelimiters);
    if Word = EmptyStr then
      Break;
    if Word = '#' then // Skip this value, as special skip sign '#' was detected
      Continue;
    try
      Val := StrToInt(Word) shl ((3 - i) * 8);
    except
      Exit;
    end;
    Mask := $FF shl ((3 - i) * 8);
    Result := (Result and (not Mask)) or Val;
  end;
end;
//--Sygsky

//++Sygsky: to read Background project brush, bug #333
type
  TBrushDescr = record
    Val: THandle;
    Nam: AnsiString;
end;

const
  StdBrushes: array [WHITE_BRUSH..(NULL_BRUSH+1)] of TBrushDescr =
  (
    (Val:WHITE_BRUSH; Nam:'WHITE_BRUSH'),
    (Val:LTGRAY_BRUSH;Nam:'LTGRAY_BRUSH'),
    (Val:GRAY_BRUSH;  Nam:'GRAY_BRUSH'),
    (Val:DKGRAY_BRUSH;Nam:'DKGRAY_BRUSH'),
    (Val:BLACK_BRUSH; Nam:'BLACK_BRUSH'),
    (Val:NULL_BRUSH;  Nam:'NULL_BRUSH'),
    (Val:HOLLOW_BRUSH;Nam:'HOLLOW_BRUSH')
  );

function TIniFile.ReadBgProjBrush(AppName, KeyName: PChar; ADefault: DWORD): THandle;
var
  Buf: array[0..100] of Char;
  AStr: AnsiString;
  Word: string[16];
  i: Integer;
  Val: DWORD;
  Mask: DWORD;
  Color : Cardinal;
begin
  Result := GetStockObject(aDefault);
  i := GetPrivateProfileString(SETTINGS, KeyName, JHC, Buf, 100, IniFile);
  SetString(AStr, Buf, i);
  if AStr = JHC then // Returns default value as nothing was detected
    Exit;
  Q_SpaceCompressInPlace(AStr); // Trim+Delete multiple spaces
  // Now the text should be in form '### ### ###' where numbers stands for
  // corresponding RED,GREEN,BLUE values in decimal form, i.e. '255 0 0' will stand for RED
  // '0 255 0' for GREEN etc.
  // ...
  // Or it can be one of the follows:
  //WHITE_BRUSH	White brush.
  //LTGRAY_BRUSH	Light gray brush.
  //GRAY_BRUSH         Gray brush.
  //DKGRAY_BRUSH	Dark gray brush.
  //BLACK_BRUSH	Black brush.
  //HOLLOW_BRUSH	Hollow brush (equivalent to NULL_BRUSH).
  //NULL_BRUSH	        Null brush (equivalent to HOLLOW_BRUSH).
  //
  // If you will set only 2 numbers,only R and B will be set, if
  // you set only 1, R will be set, all other will be filled from defult values!
  for i := WHITE_BRUSH to (NULL_BRUSH+1) do
    if CompareText(StdBrushes[i].Nam,AStr) = 0 then // We found standard Brush
    begin
      Result := GetStockObject(StdBrushes[i].Val);
      Exit;
    end;

  Color := 0; // White as a default color for operation
  for i := 1 to 3 do // Read R, G and B words
  begin
    Word := Q_GetWordN(i, AStr, RGBQuadDelimiters);
    if Word = EmptyStr then
      Break;
    if Word = '#' then // Skip this value, as special skip sign '#' was detected
      Continue;
    try
      Val := StrToInt(Word) shl ((i - 1) * 8);
    except
      Exit; // Return defult value
    end;
    Mask := $FF shl ((i - 1) * 8);
    Color := (Color and (not Mask)) or Val;// Result := (Result and (not Mask)) or Val;
  end;
  Result := CreateSolidBrush(Color)
end;
//--Sygsky

{+++Brovak}
Procedure  TIniFile.ReadDataForLoadPoint;

var Buffer : Array[0..255] of char;

 begin;
  with LoadPointsCoordDataFromFile do
   begin;
     ReadFromRow:=ReadInteger('LoadPointsCoord', 'ReadFromRow', 1);
      GetPrivateProfileString('LoadPointsCoord','BaseDelimeter',';',Buffer,SizeOf(Buffer),IniFile);
       BaseDelimeter:=StrPas(Buffer);
      DataIntoDelimeters:=ReadBoolean('LoadPointsCoord', 'DataIntoDelimeters', FALSE);
      GetPrivateProfileString('LoadPointsCoord','Delleft','',Buffer,SizeOf(Buffer),IniFile);
       Delleft:=StrPas(Buffer);
      GetPrivateProfileString('LoadPointsCoord','Delright','',Buffer,SizeOf(Buffer),IniFile);
       Delright:=StrPas(Buffer);
      XColumn:=ReadInteger('LoadPointsCoord', 'XColumn', 1);
      YColumn:=ReadInteger('LoadPointsCoord', 'YColumn', 2);
       KoeffX:=ReadBoolean('LoadPointsCoord', 'KoeffX', FALSE);
       KoeffDataX:=ReadFloat('LoadPointsCoord', 'KoeffDataX', 1);
       KoeffY:=ReadBoolean('LoadPointsCoord', 'KoeffY', FALSE);
       KoeffDataY:=ReadFloat('LoadPointsCoord', 'KoeffDataY', 1);
      LoadMode:=ReadInteger('LoadPointsCoord', 'Mode', 1);
  end;
 end;

 Procedure  TIniFile.WriteDataForLoadPoint;

var Buffer : Array[0..255] of char;

 begin;
  with LoadPointsCoordDataFromFile do
   begin;
     WriteInteger('LoadPointsCoord', 'ReadFromRow',ReadFromRow);

     StrPCopy(Buffer, BaseDelimeter);
     WritePrivateProfileString('LoadPointsCoord','BaseDelimeter',Buffer,IniFile);
     WriteBoolean('LoadPointsCoord', 'DataIntoDelimeters',DataIntoDelimeters);
     StrPCopy(Buffer, Delleft);
     WritePrivateProfileString('LoadPointsCoord','Delleft',Buffer,IniFile);
     StrPCopy(Buffer, Delright);
     WritePrivateProfileString('LoadPointsCoord','Delright',Buffer,IniFile);

     WriteInteger('LoadPointsCoord', 'XColumn',XColumn);
     WriteInteger('LoadPointsCoord', 'YColumn',YColumn);
     WriteBoolean('LoadPointsCoord', 'KoeffX', KoeffX);
     WriteBoolean('LoadPointsCoord', 'KoeffY', KoeffY);
     WriteFloat('LoadPointsCoord', 'KoeffDataX', KoeffDataX);
     WriteFloat('LoadPointsCoord', 'KoeffDataY', KoeffDataY);
     WriteInteger('LoadPointsCoord', 'Mode',LoadMode);
  end;
 end;

  Procedure  TIniFile.WriteDataForLoadPoint1;
  begin;
   with LoadPointsCoordDataFromFile do
   begin;
    WriteInteger('LoadPointsCoord', 'InputMode',InputMode);
    WriteInteger('LoadPointsCoord', 'DegreeMode',DegreeMode);
   end;
  end;



  //for projections

 {--Brovak}

Procedure  TIniFile.ReadAdvAnnotation;
 begin;
  with AdvAnnotationSettings do
   begin;
   // UseAdvancedOptions:=ReadBoolean('AdvAnnotation','UseAdvancedOptions',FALSE);
   // PolygonAdv:=ReadBoolean('AdvAnnotation','PolygonAdv',FALSE);
   // PolyLineAdv:=ReadBoolean('AdvAnnotation','PolyLineAdv',FALSE);
    PolyLineAdvDirection:=ReadInteger('AdvAnnotation','PolyLineAdvDirection',0);
    ArcAdv:=ReadBoolean('AdvAnnotation','ArcAdv',FALSE);
    ArcAdvDirection:=ReadInteger('AdvAnnotation','ArcAdvDirection',0);
    OffSetAdv:=ReadBoolean('AdvAnnotation','OffSetAdv',FALSE);
    OffSetAdvDirection:=ReadInteger('AdvAnnotation','OffSetAdvDirection',0);
   end;

 end;

Procedure  TIniFile.WriteAdvAnnotation;
 begin;
   with AdvAnnotationSettings do
    begin;
   //   WriteBoolean('AdvAnnotation', 'UseAdvancedOptions', UseAdvancedOptions);
   //   WriteBoolean('AdvAnnotation', 'PolygonAdv', PolygonAdv);
   //   WriteBoolean('AdvAnnotation', 'PolyLineAdv', PolyLineAdv);
      WriteInteger('AdvAnnotation', 'PolyLineAdvDirection',PolyLineAdvDirection);
      WriteBoolean('AdvAnnotation', 'ArcAdv', ArcAdv);
      WriteInteger('AdvAnnotation', 'ArcAdvDirection',ArcAdvDirection);
      WriteBoolean('AdvAnnotation', 'OffSetAdv', OffSetAdv);
      WriteInteger('AdvAnnotation', 'OffSetAdvDirection',OffSetAdvDirection);
    end;

 end;
{brovak}
Procedure TIniFile.ReadSpecFrameRect(AProject:pointer);
   var  Buffer: array[0..255] of Char;
   AText: array[0..20] of Char;

   begin;
     with PProj(Aproject).FramePrintSpec do
       begin;

       UseSpecFrame := ReadBoolean('PrintFrameOptions', 'UseSpecFrame',FALSE);
       UseTextMarks:= ReadBoolean('PrintFrameOptions', 'UseTextMarks',FALSE);
       LabelOutside:=ReadBoolean('PrintFrameOptions', 'LabelOutside',FALSE);

         with PProj(Aproject).FramePrintSpec.Font do
          begin
            Font := GetPrivateProfileInt('PrintFrameOptions', 'Font', 0, IniFile);
            Style := GetPrivateProfileInt('PrintFrameOptions', 'Style', 0, IniFile);
            Height := GetPrivateProfileInt('PrintFrameOptions', 'Height', 1, IniFile);
          end;

        GetPrivateProfileString('PrintFrameOptions', 'Color', '$00FF0000', Buffer, SizeOf(Buffer), IniFile);
        Color := HexToLong(Copy(StrPas(Buffer), 2, 255));
        GetPrivateProfileString('PrintFrameOptions', 'BackColor', '$00FF0000', Buffer, SizeOf(Buffer), IniFile);
        BackColor := HexToLong(Copy(StrPas(Buffer), 2, 255));

       UseLeft:=ReadBoolean('PrintFrameOptions', 'UseLeft',FALSE);
       UseRight:=ReadBoolean('PrintFrameOptions', 'UseRight',FALSE);
       UseTop:=ReadBoolean('PrintFrameOptions', 'UseTop',FALSE);
       UseBottom:=ReadBoolean('PrintFrameOptions', 'UseBottom',FALSE);

       StepLength:=ReadFloat('PrintFrameOptions', 'StepLength',100);
       MarksLength:=ReadFloat('PrintFrameOptions', 'MarksLength',100);

       UseCross:=ReadBoolean('PrintFrameOptions', 'UseCross',FALSE);
       CrossLength:=ReadFloat('PrintFrameOptions', 'CrossLength',100);

       GetPrivateProfileString('PrintFrameOptions', 'CrossColor', '$00FF0000', Buffer, SizeOf(Buffer), IniFile);
       CrossColor := HexToLong(Copy(StrPas(Buffer), 2, 255));

       GetPrivateProfileString('PrintFrameOptions', 'GridColor', '$00FF0000', Buffer, SizeOf(Buffer), IniFile);
       GridColor := HexToLong(Copy(StrPas(Buffer), 2, 255));

   end;

  end;

 Procedure TInifile.WriteSpecFrameRect(AProject:pointer);
   var  Buffer: array[0..255] of Char;
   AText: array[0..20] of Char;

  begin;
   with PProj(Aproject).FramePrintSpec do
    begin;

        WriteBoolean('PrintFrameOptions', 'UseSpecFrame',UseSpecFrame);
        WriteBoolean('PrintFrameOptions', 'UseTextMarks',UseTextMarks);
        WriteBoolean('PrintFrameOptions', 'LabelOutside',LabelOutside);

      with PProj(Aproject).FramePrintSpec.Font do
          begin
           WriteInteger('PrintFrameOptions', 'Font', Font);
           WriteInteger('PrintFrameOptions', 'Style', Style);
           WriteInteger('PrintFrameOptions', 'Height', Height);
          end;

        StrPCopy(AText, '$' + LongToHex(Color, 8));
        WritePrivateProfileString('PrintFrameOptions', 'Color', AText, IniFile);
        StrPCopy(AText, '$' + LongToHex(BackColor, 8));
        WritePrivateProfileString('PrintFrameOptions', 'BackColor', AText, IniFile);

        WriteBoolean('PrintFrameOptions', 'UseLeft',UseLeft );
        WriteBoolean('PrintFrameOptions', 'UseRight', UseRight);
        WriteBoolean('PrintFrameOptions', 'UseTop', UseTop);
        WriteBoolean('PrintFrameOptions', 'UseBottom',UseBottom );

        WriteFloat('PrintFrameOptions','StepLength',StepLength);
        WriteFloat('PrintFrameOptions','MarksLength',MarksLength);

        WriteBoolean('PrintFrameOptions', 'UseCross',UseCross );

        WriteFloat('PrintFrameOptions','CrossLength',CrossLength);

        StrPCopy(AText, '$' + LongToHex(CrossColor, 8));
        WritePrivateProfileString('PrintFrameOptions', 'CrossColor', AText, IniFile);

        StrPCopy(AText, '$' + LongToHex(GridColor, 8));
        WritePrivateProfileString('PrintFrameOptions', 'GridColor', AText, IniFile);


  end;

 end;

{brovak}

function TIniFile.GetAreaUnit: String;
begin
  case AreaFact of
      1:        Result:=' m�';
      100:      Result:=' a';
      10000:    Result:=' ha';
      1000000:  Result:=' km�';
  else
      Result:='';
  end;
end;

end.

