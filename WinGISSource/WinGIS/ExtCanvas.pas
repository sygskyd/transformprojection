{******************************************************************************+
  Unit ExtCanvas
--------------------------------------------------------------------------------
  Author: Martin Forst
--------------------------------------------------------------------------------
  Encapsualtes the windows-device-context like the delphi TCanvas-class.
  ExtCanvas provides some extended features like user-defined line- and fill-
  styles. It can also handle a bitmap-cache of the current screen-display
  which allows fast screen-updates if the graphic-content has not changed.
*******************************************************************************}
Unit ExtCanvas;

Interface

Uses VermGr,Windows,Classes,Graphics,GrTools,Hash,Lists,NumTools,SyncObjs,XStyles, AM_Ini, AM_Def;

Const // line-type-constants
      ltSolid           = 0;
      ltDash            = 1;
      ltDot             = 2;
      ltDashDot         = 3;
      ltDashDDot        = 4;

      // user-defined line-type
      ltUserDefined     = 1000;

      // pattern-type-constants
      ptNoPattern       = 0;
      ptFDiagonal       = 1;
      ptCross           = 2;
      ptDiagCross       = 3;
      ptBDiagonal       = 4;
      ptHorizontal      = 5;
      ptVertical        = 6;
      ptSolid           = 7;

      // pattern-types for vector- or bitmap-fill
      ptVectorXFill     = 8;
      ptBitmapXFill     = 9;

      // textstyle-constants
      tsItalic          = 1;
      tsBold            = 2;
      tsUnderlined      = 4;
      tsStrikeOut       = 8;

      // refresh-intervall
      RefreshInterval      = 1000;

Type PLongArray    = ^TLongArray;
     TLongArray    = Array[0..1000] of LongInt;

     TUserFillProc = Procedure(Param1,Param2,Param3:LongInt);

     TExtCanvas    = Class;

     PExtBrushStyle  = ^TExtBrushStyle;
     TExtBrushStyle  = Record
       Style       : Integer;
       ForeColor   : TColor;
       BackColor   : TColor;
       Scale       : Double;
     end;

     {**************************************************************************
       Class TExtBrush
     ---------------------------------------------------------------------------
       Brush used by TExtCanvas.
     +*************************************************************************}
     TExtBrush     = Class
      Private
       // style of the brush, one of the ptXXXX-constants
       FStyle      : Integer;
       // foregound-color of the brush
       FForeColor  : TColor;
       // background-color of the brush
       FBackColor  : TColor;
       // scale for user-defined fill-styles
       FScale      : Double;
       // true if the brush was selected for the device-cntext
       FHandleValid: Boolean;
       // true if all additional device-context-settings (bkcolor, bkmode...) are set
       FValid      : Boolean;
       Function    GetExtStyle:TExtBrushStyle;
       Procedure   SetBackColor(const AColor:TColor);
       Procedure   SetExtStyle(const AStyle:TExtBrushStyle);
       Procedure   SetForeColor(const AColor:TColor);
       Procedure   SetScale(const AScale:Double);
       Procedure   SetStyle(const AStyle:Integer);
      Public
       Property    BackColor:TColor read FBackColor write SetBackColor;
       Property    ExtStyle:TExtBrushStyle read GetExtStyle write SetExtStyle;
       Property    ForeColor:TColor read FForeColor write SetForeColor;
       Property    Scale:Double read FScale write SetScale;
       Property    Style:Integer read FStyle write SetStyle;
     end;

     PExtPenStyle  = ^TExtPenStyle;
     TExtPenStyle  = Record
       Style       : Integer;
       Color       : TColor;
       FillColor   : TColor;
       Width       : Integer;
       Scale       : Double;
     end;

     {**************************************************************************
       Class TExtPen
     ---------------------------------------------------------------------------
     +*************************************************************************}
     TExtPen       = Class
      Private
       // style of the pen, one of the ltXXXX constants
       FStyle      : Integer;
       // line-color for windows- and user-defined line-styles
       FColor      : TColor;
       // fill-color for user-defined line-styles
       FFillColor  : TColor;
       // width of the line, only useapble for solid lines
       FWidth      : Integer;
       // scale for user-defined line-styles
       FScale      : Double;
       // true if the pen was selected for the device-context
       FHandleValid: Boolean;
       // true if all additional device-context-settings (bkcolor, bkmode...) are set
       FValid      : Boolean;
       Function    GetExtStyle:TExtPenStyle;
       Procedure   SetColor(const AColor:TColor);
       Procedure   SetExtStyle(const AStyle:TExtPenStyle);
       Procedure   SetFillColor(const AColor:TColor);
       Procedure   SetScale(const AScale:Double);
       Procedure   SetStyle(const AStyle:Integer);
       Procedure   SetWidth(const AWidth:Integer);
      Public
       Property    Color:TColor read FColor write SetColor;
       Property    ExtStyle:TExtPenStyle read GetExtStyle write SetExtStyle;
       Property    FillColor:TColor read FFillColor write SetFillColor;
       Property    Scale:Double read FScale write SetScale;
       Property    Style:Integer read FStyle write SetStyle;
       Property    Width:Integer read FWidth write SetWidth;
     end;

     TTextAlignment= (taLeft,taCentered,taRight);

     PExtFontStyle = ^TExtFontStyle;
     TExtFontStyle = Record
       Angle       : Double;
       Color       : TColor;
       Name        : AnsiString;
       Size        : Integer;
       Style       : Integer;
       Alignment   : TTextAlignment;
     end;

     TExtFont      = Class
      Private
       FAngle      : Double;
       FColor      : TColor;
       FName       : AnsiString;
       FSize       : Integer;
       FStyle      : Integer;
       FAlignment  : TTextAlignment;
       FHandleValid: Boolean;
       FValid      : Boolean;
       FCosAngle   : Double;
       FSinAngle   : Double;
       Function    GetExtFontStyle:TExtFontStyle;
       Procedure   SetAngle(const AAngle:Double);
       Procedure   SetColor(AColor:TColor);
       Procedure   SetExtFontStyle(const AStyle:TExtFontStyle);
       Procedure   SetName(const AName:AnsiString);
       Procedure   SetSize(ASize:Integer);
       Procedure   SetStyle(AStyle:Integer);
      Public
{++ Moskaliov BUG#268 BUILD#108 29.03.00}
       Constructor Create;
{-- Moskaliov BUG#268 BUILD#108 29.03.00}
       Property    Alignment:TTextAlignment read FAlignment write FAlignment;
       Property    Angle:Double read FAngle write SetAngle;
       Property    Color:TColor read FColor write SetColor;
       Property    ExtStyle:TExtFontStyle read GetExtFontStyle write SetExtFontStyle;
       Property    Name:AnsiString read FName write SetName;
       Property    Size:Integer read FSize write SetSize;
       Property    Style:Integer read FStyle write SetStyle;
       Property    CosAngle:Double read FCosAngle;
       Property    SinAngle:Double read FSinAngle;
     end;

     TExtCanvasStates = (ecsPen, ecsBrush, ecsFont);
     TExtCanvasState  = set of TExtCanvasStates;

     // map-mode, don't change order, Windows-map-mode = Ord(MapMode)+1
     TMapMode      = (mmText,mmLoMetric,mmHiMetric,mmLoEnglish,mmHiEnglish,mmTwips,mmIsotropic,mmAnIsotropic);

     TRefreshThread     = Class(TThread)
      Private
       FExtCanvas  : TExtCanvas;
       FWaitEvents : Array[0..3] of DWORD;
      Protected
       Procedure   Execute; override;
      Public
       Constructor Create(ExtCanvas:TExtCanvas);
       Destructor  Destroy; override;
       Procedure   Start;
       Procedure   Stop;
     end;

     {**************************************************************************
       Class TExtCanvas
     ---------------------------------------------------------------------------
     +*************************************************************************}
     TExtCanvas      = Class(TComponent)
      Private
       FBitmap          : HBitmap;
       FBitmapDC        : HDC;
       FBitmapSize      : TRect;
       FBitmapXFill     : PBitmapXFillDef;
       FHandleSection   : TCriticalSection;
       FOldBitmap       : HBitmap;
       FBrush           : TExtBrush;
       FCached          : Boolean;
       FArcResolution   : Double;
       FClipRect        : TRect;
       FClipStack       : TIntList;
       FPhysicalSize    : TRect;
       FDC              : HDC;
       FDrawCached      : Boolean;
       FDrawVertices    : Boolean;
       FVerticesColor   : Integer;
       FFont            : TExtFont;
       FHandle          : HDC;
       FMapMode         : TMapMode;
       FPen             : TExtPen;
       FPointsConverted : Boolean;
       FRefreshThread   : TRefreshThread;
       FStack           : Pointer;
       FXLineStyle      : PXLineStyleDef;
       FXFillStyle      : PXFillStyleDef;
       FViewRotation    : Double;
       FInvalidRegions  : THandle;
       FBrushHashTable  : TAutoHashTable;
       FFontHashTable   : TAutoHashTable;
       FPenHashTable    : TAutoHashTable;
       Procedure   DeselectAllTools;
       Function    GetLogicalSize:TRect;
       Function    GetFontType(const FontName:AnsiString):Integer;
       Procedure   InvalidateAllTools;
       Procedure   SetCached(ACached:Boolean);
       Procedure   SetMapMode(AMapMode:TMapMode);
       Procedure   RequiredState(State:TExtCanvasState);
      Public
       Constructor Create(AOwner:TComponent); override;
       Destructor  Destroy; override;
       // dc handles
       Procedure   FreeBitmapCache;
       Procedure   RecreateBitmapCache;
       Procedure   RefreshBitmapCache;
       Procedure   SetDC(NewDC:HDC);
       Property    BitmapSize:TRect read FBitmapSize write FBitmapsize;
       Property    Handle:HDC read FDC write SetDC;
       Property    WindowDC:HDC read FHandle write SetDC;
       Property    BitmapDC:HDC read FBitmapDC;
       Property    BitmapHandle: HBitmap read FBitmap;
       // drawing tools and drwing-styles
       Property    Brush:TExtBrush read FBrush;
       Property    Font:TExtFont read FFont;
       Property    Pen:TExtPen read FPen;
       Property    XLineStyle:PXLineStyleDef read FXLineStyle write FXLineStyle;
       Property    XFillStyle:PXFillStyleDef read FXFillStyle write FXFillStyle;
       Property    ViewRotation:Double read FViewRotation write FViewRotation;
       Property    BitmapXFill:PBitmapXFillDef read FBitmapXFill write FBitmapXFill;
       // clipping functions
       Property    ClipRect:TRect read FClipRect write FClipRect;
       Procedure   ClipPolyline(var Points:TGrPointList);
       Procedure   ClipPolygon(var Points:TGrPointList);
       // cache functions
       Procedure   SetDrawCached(ACached:Boolean);
       Property    CachedDraw:Boolean read FDrawCached write SetDrawCached default False;
       Property    CacheBitmap:HBitmap read FBitmap;
       Property    OldBitmap: HBitmap read FOldBitmap;
       Property    VerticesColor: Integer read FVerticesColor write FVerticesColor default 0;
       Procedure   Resize(const PhysicalSize:TRect);
       Procedure   DrawFromCache(const ARect:TRect);
//++ Glukhov DragView Build#150 30.01.01
       Procedure   DrawFromCacheWithOffset(const ARect:TRect; Xoff, Yoff: Integer);
//-- Glukhov DragView Build#150 30.01.01
       // size functions
       Property    LogicalSize:TRect read GetLogicalSize;
       Property    PhysicalSize:TRect read FPhysicalSize;
       // invalidate/validate-functions
       Procedure   Invalidate(Rect:TRect);
       Procedure   Validate(Rect:TRect);
       Function    IsInvalidated(Rect:TRect):Boolean;
       // drawing functions
       Procedure   Arc(const Center:TGrPoint;const Radius:Double;BeginAngle,EndAngle:Double);
       Procedure   BezierCurve(var Points;PointCount:Integer);
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
       Procedure   Ellipse(const Center: TPoint; XRadius: Integer; YRadius: Integer); overload;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
       Procedure   Circle(const Center:TPoint;Radius:Integer); overload;
       Procedure   Circle(const Center:TPoint;Radius:Integer;
                       UserFillProc:TUserFillProc;Param1:LongInt=0;
                       Param2:LongInt=0;Param3:LongInt=0); overload;
       Procedure   ClippedArc(const Center:TGrPoint;const Radius:Double;
                       const BeginAngle,EndAngle:Double);
       Procedure   ClippedBezierCurve(var Points:TGrPointList);
       Procedure   ClippedCircle(const Center:TGrPoint;const Radius:Double); overload;
       Procedure   ClippedCircle(const Center:TGrPoint;const Radius:Double;
                       UserFillProc:TUserFillProc;Param1:LongInt=0;
                       Param2:LongInt=0;Param3:LongInt=0); overload;
       Procedure   ClippedIslandArea(var Points:TGrPointList;PointCount:Integer;
                       const IslandInfo:TLongArray;IslandCount:Integer); overload;
       Procedure   ClippedIslandArea(var Points:TGrPointList;PointCount:Integer;
                       const IslandInfo:TLongArray;IslandCount:Integer;
                       UserFillProc:TUserFillProc;Param1:LongInt=0;
                       Param2:LongInt=0;Param3:LongInt=0); overload;
       Procedure   ClippedPolyline(var Points:TGrPointList;DrawVertices:Boolean);
       Procedure   ClippedPolygon(var Points:TGrPointList;DrawVertices:Boolean); overload;
       Procedure   ClippedPolygon(var Points:TGrPointList;DrawVertices:Boolean;
                       UserFillProc:TUserFillProc;Param1:LongInt=0;
                       Param2:LongInt=0;Param3:LongInt=0); overload;
       Procedure   ClippedTextOut(Position:TGrPoint;Text:PChar;TextLength:Integer);
       Procedure   FillRect(const Rect:TRect;Brush:THandle);
       Procedure   IslandArea(var Points;PointCount:Integer;const IslandInfo:TLongArray;
                       IslandCount:Integer); overload;
       Procedure   IslandArea(var Points;PointCount:Integer;const IslandInfo:TLongArray;
                       IslandCount:Integer;UserFillProc:TUserFillProc;Param1:LongInt=0;
                       Param2:LongInt=0;Param3:LongInt=0); overload;
       Procedure   MoveTo(XPos,YPos:Integer);
       Procedure   Polyline(var Points;PointCount:Integer;DrawVertices:Boolean);
       Procedure   Polygon(var Points;PointCount:Integer;DrawVertices:Boolean); overload;
       Procedure   Polygon(var Points;PointCount:Integer;DrawVertices:Boolean;
                       UserFillProc:TUserFillProc;Param1:LongInt=0;
                       Param2:LongInt=0;Param3:LongInt=0); overload;
       Procedure   LineTo(XPos,YPos:Integer);
       Procedure   FrameRect(X1,Y1,X2,Y2:Integer); overload;
       Procedure   FrameRect(const Rect:TRect); overload;
       Procedure   FrameRect(const Rect:TGrRect); overload;
       Procedure   FrameRect(const Rect:TRotRect); overload;
       Procedure   Rectangle(X1,Y1,X2,Y2:Integer); overload;
       Procedure   Rectangle(const Rect:TRect); overload;
       Procedure   Rectangle(const Rect:TGrRect); overload;
       Procedure   Rectangle(const Rect:TRotRect); overload;
       Procedure   TextOut(const Position:TGrPoint;Text:PChar;TextLength:Integer);
       // set the polygon as the clipping-area of the device-context
       Procedure   SetClippingPolygon(var Points;PointCount:Integer);
       Procedure   SetClippingCircle(Const Center:TPoint;Radius:Integer);
       Procedure   SetClippingRect(ARect:TRect);
       // text information-functions
       Function    TextWidth(Text:PChar;TextLength:Integer):Integer;
       Function    TextHeight(Text:PChar;TextLength:Integer):Integer;
       Function    TextExtent(Text:PChar;TextLength:Integer):TSize;
       // save-restore functions
       Procedure   BeginPaint;
       Procedure   EndPaint;
       Procedure   Pop;
       Procedure   PopClipping;
       Procedure   Push;
       Procedure   PushClipping;
       // other functions
       Procedure   ConvertToXStylePoints(var Points;PointCount:Integer);
       Property    PointsConverted:Boolean read FPointsCOnverted;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
       Procedure   GetBitmapPartCopy(ABitmap: TBitmap; ARect: TRect);
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
      Published
       Property    ArcResolution:Double read FArcResolution write FArcResolution;
       Property    Cached:Boolean read FCached write SetCached default False;
       Property    DrawVertices:Boolean read FDrawVertices write FDrawVertices;
       Property    MapMode:TMapMode read FMapMode write SetMapMode;
       Procedure   ScreenShot(AName:String);
     end;

Implementation

Uses Cache,Clipping,SysUtils;

const HandleSize   = 2;

Type PExtCanvasStyle = ^TExtCanvasStyle;
     TExtCanvasStyle = Record
       BrushStyle    : TExtBrushStyle;
       PenStyle      : TExtPenStyle;
       FontStyle     : TExtFontStyle;
       XLineStyle    : PXLineStyleDef;
       XFillStyle    : PXFillStyleDef;
       DrawCached    : Boolean;
       Next          : PExtCanvasStyle;
     end;

     PPenHashItem       = ^TPenHashItem;
     TPenHashItem       = Record
       Style            : TExtPenStyle;
       Handle           : THandle;
       CacheIndex       : Integer;
     end;

     TPenHashTable      = Class(TAutoHashTable)
      Private
       FCache           : TCache;
       Function    GetPen(const Style:TExtPenStyle):THandle;
       Function    KeyOf(Item:Pointer):Pointer; override;
       Function    OnCompareItems(Item1,Item2:Pointer):Boolean; override;
       Procedure   OnFreeItem(Item:Pointer); override;
       Procedure   OnFreeCacheItem(Item:Pointer);
       Function    OnGetHashValue(Item:Pointer):LongInt; override;
      Public
       Constructor Create;
       Destructor  Destroy; override;
       Property    Pens[const Style:TExtPenStyle]:THandle read GetPen; default;
     end;

     PBrushHashItem     = ^TBrushHashItem;
     TBrushHashItem     = Record
       Style            : TExtBrushStyle;
       Handle           : THandle;
       CacheIndex       : Integer;
     end;

     TBrushHashTable    = Class(TAutoHashTable)
      Private
       FCache           : TCache;
       Function    GetBrush(const Style:TExtBrushStyle):THandle;
       Function    KeyOf(Item:Pointer):Pointer; override;
       Function    OnCompareItems(Item1,Item2:Pointer):Boolean; override;
       Procedure   OnFreeItem(Item:Pointer); override;
       Procedure   OnFreeCacheItem(Item:Pointer);
       Function    OnGetHashValue(Item:Pointer):LongInt; override;
      Public
       Constructor Create;
       Destructor  Destroy; override;
       Property    Brushes[const Style:TExtBrushStyle]:THandle read GetBrush; default;
     end;

     PFontHashItem      = ^TFontHashItem;
     TFontHashItem      = Record
       Style            : TExtFontStyle;
       HashIndex        : LongInt;
       Handle           : THandle;
       CacheIndex       : Integer;
     end;

     TFontHashTable    = Class(TAutoHashTable)
      Private
       FCache           : TCache;
       Function    GetFont(const Style:TExtFontStyle):THandle;
       Function    OnCompareItems(Item1,Item2:Pointer):Boolean; override;
       Procedure   OnFreeItem(Item:Pointer); override;
       Procedure   OnFreeCacheItem(Item:Pointer);
       Function    OnGetHashValue(Item:Pointer):LongInt; override;
      Public
       Constructor Create;
       Destructor  Destroy; override;
       Property    Fontes[const Style:TExtFontStyle]:THandle read GetFont; default;
     end;

Const isPenCache        = 128;
      isBrushCache      = 128;
      isFontCache       = 128;

{==============================================================================+
  TPenHashTable
+==============================================================================}

Constructor TPenHashTable.Create;
begin
  inherited Create(isPenCache);
  FCache:=TCache.Create(isPenCache,isPenCache);
  FCache.OnFreeItem:=OnFreeCacheItem;
end;

Destructor TPenHashTable.Destroy;
begin
  FCache.Free;
  inherited Destroy;
end;

Function TPenHashTable.KeyOf(Item:Pointer):Pointer;
begin
  Result:=@PPenHashItem(Item)^.Style;
end;

Function TPenHashTable.OnCompareItems(Item1,Item2:Pointer):Boolean;
begin
  Result:=FALSE;
  if PExtPenStyle(Item1)^.Color=PExtPenStyle(Item2)^.Color then
      if PExtPenStyle(Item1)^.Style=PExtPenStyle(Item2)^.Style then
      if PExtPenStyle(Item1)^.Width=PExtPenStyle(Item2)^.Width then
    Result:=TRUE
end;

Procedure TPenHashTable.OnFreeItem(Item:Pointer);
begin
  with PPenHashItem(Item)^ do begin
    if Handle<>0 then DeleteObject(Handle);
    Dispose(Item);
  end;
end;

Procedure TPenHashTable.OnFreeCacheItem(Item:Pointer);
begin
  Delete(Item);
  OnFreeItem(Item);
end;

Function TPenHashTable.OnGetHashValue(Item:Pointer):LongInt;
begin                                
  with PExtPenStyle(Item)^ do Result:=LongInt(Color) XOr LongInt(Style);
end;

Function TPenHashTable.GetPen(const Style:TExtPenStyle):THandle;
var Pen          : PPenHashItem;
begin
  Pen:=Find(@Style);
  if Pen=NIL then begin
    Pen:=New(PPenHashItem);
    Pen^.Style:=Style;
    Pen^.Handle:=CreatePen(Style.Style,Style.Width,Style.Color);
    Add(Pen);
    Pen^.CacheIndex:=FCache.Add(Pen,1);
  end
  else FCache.Referenced(Pen^.CacheIndex);
  Result:=Pen^.Handle;
end;

{==============================================================================+
TBrushHashTable
+==============================================================================}

Constructor TBrushHashTable.Create;
begin
  inherited Create(isBrushCache);
  FCache:=TCache.Create(isBrushCache,isBrushCache);
  FCache.OnFreeItem:=OnFreeCacheItem;
end;

Destructor TBrushHashTable.Destroy;
begin
  FCache.Free;
  inherited Destroy;
end;

Function TBrushHashTable.KeyOf(Item:Pointer):Pointer;
begin
  Result:=@PBrushHashItem(Item)^.Style;
end;     

Function TBrushHashTable.OnCompareItems(Item1,Item2:Pointer):Boolean;
begin
  Result:=FALSE;
  if PExtBrushStyle(Item1)^.ForeColor=PExtBrushStyle(Item2)^.ForeColor then
      if PExtBrushStyle(Item1)^.Style=PExtBrushStyle(Item2)^.Style then
    Result:=TRUE
end;

Procedure TBrushHashTable.OnFreeItem(Item:Pointer);
begin
  with PBrushHashItem(Item)^ do begin
    if Handle<>0 then DeleteObject(Handle);
    Dispose(Item);
  end;
end;

Procedure TBrushHashTable.OnFreeCacheItem(Item:Pointer);
begin
  Delete(Item);
  OnFreeItem(Item);
end;

Function TBrushHashTable.OnGetHashValue(Item:Pointer):LongInt;
begin
  with PExtBrushStyle(Item)^ do Result:=Abs(LongInt(ForeColor) xor LongInt(Style));
end;

Function TBrushHashTable.GetBrush(const Style:TExtBrushStyle):THandle;
var Brush          : PBrushHashItem;
    Pattern        : Integer;
begin
  Brush:=Find(@Style);
  if Brush=NIL then begin
    Brush:=New(PBrushHashItem);
    Brush^.Style:=Style;
    if Style.Style=ptSolid then Brush^.Handle:=CreateSolidBrush(Style.ForeColor)
    else begin
      case Style.Style of
        ptFDiagonal : Pattern:=hs_FDiagonal;
        ptCross     : Pattern:=hs_Cross;
        ptDiagCross : Pattern:=hs_DiagCross;
        ptBDiagonal : Pattern:=hs_BDiagonal;
        ptHorizontal: Pattern:=hs_Horizontal;
        ptVertical  : Pattern:=hs_Vertical;
        else begin
          Result:=GetStockObject(HOLLOW_BRUSH);
          Dispose(Brush);
          Exit;
        end;
      end;
      Brush^.Handle:=CreateHatchBrush(Pattern,Style.ForeColor);
    end;
    Add(Brush);
    Brush^.CacheIndex:=FCache.Add(Brush,1);
  end
  else FCache.Referenced(Brush^.CacheIndex);
  Result:=Brush^.Handle;
end;

{==============================================================================+
TFontHashTable
+==============================================================================}

Constructor TFontHashTable.Create;
begin
  inherited Create(isFontCache);
  FCache:=TCache.Create(isFontCache,isFontCache);
  FCache.OnFreeItem:=OnFreeCacheItem;
end;

Destructor TFontHashTable.Destroy;
begin
  FCache.Free;
  inherited Destroy;
end;

Function TFontHashTable.OnCompareItems(Item1,Item2:Pointer):Boolean;
begin
  Result:=FALSE;
  with PFontHashItem(Item1)^ do if Style.Angle=PFontHashItem(Item2)^.Style.Angle then
      if Style.Size=PFontHashItem(Item2)^.Style.Size then
      if Style.Style=PFontHashItem(Item2)^.Style.Style then
      if CompareText(Style.Name,PFontHashItem(Item2)^.Style.Name)=0 then
    Result:=TRUE
end;

Procedure TFontHashTable.OnFreeItem(Item:Pointer);
begin
  with PFontHashItem(Item)^ do begin
    if Handle<>0 then DeleteObject(Handle);
    Dispose(Item);
  end;
end;

Procedure TFontHashTable.OnFreeCacheItem(Item:Pointer);
begin
  Delete(Item);
  OnFreeItem(Item);
end;

Function TFontHashTable.OnGetHashValue(Item:Pointer):LongInt;
begin
  Result:=PFontHashItem(Item)^.HashIndex;
end;

Function TFontHashTable.GetFont(const Style:TExtFontStyle):THandle;
var Font         : PFontHashItem;
    FindData     : TFontHashItem;
    LogFont      : TLogFont;
begin
  FindData.Style:=Style;
  FindData.HashIndex:=String2HashIndex(Style.Name) xor Style.Size;
  Font:=Find(@FindData);
  if Font=NIL then begin
    Font:=New(PFontHashItem);
    Font^:=FindData;
    with LogFont do begin
      lfHeight:=Style.Size;
      lfWidth:=0;
      lfEscapement:=-Round(Style.Angle*1800.0/Pi);
      lfOrientation:=lfEscapement;
      if Style.Style and tsBold<>0 then lfWeight:=FW_BOLD
      else lfWeight:=FW_NORMAL;
      lfItalic:=Byte(Style.Style and tsItalic<>0);
      lfUnderline:=Byte(Style.Style and tsUnderlined<>0);
      lfStrikeOut:=Byte(Style.Style and tsStrikeOut<>0);
      lfCharSet:=DEFAULT_CHARSET;
      StrPCopy(lfFaceName,Style.Name);
      lfQuality:=DEFAULT_QUALITY;
      { Everything else as default }
      lfOutPrecision:=OUT_DEFAULT_PRECIS;
      lfClipPrecision:=CLIP_DEFAULT_PRECIS or CLIP_LH_ANGLES;
      lfPitchAndFamily := DEFAULT_PITCH;
      Font^.Handle:=CreateFontIndirect(LogFont);
    end;
    Add(Font);
    Font^.CacheIndex:=FCache.Add(Font,1);
  end
  else FCache.Referenced(Font^.CacheIndex);
  Result:=Font^.Handle;
end;

{==============================================================================+
TExtBrush
+==============================================================================}

Function TExtBrush.GetExtStyle
 : TExtBrushStyle;
begin
  Result:=PExtBrushStyle(@FStyle)^;
end;

Procedure TExtBrush.SetBackColor(const AColor:TColor);
begin
  FBackColor:=AColor;
  FValid:=False;
end;

Procedure TExtBrush.SetForeColor(const AColor:TColor);
begin
  FForeColor:=AColor;
  FHandleValid:=False;
end;

Procedure TExtBrush.SetScale(const AScale:Double);
begin
  FScale:=AScale;
end;

Procedure TExtBrush.SetStyle(const AStyle:Integer);
begin
  FStyle:=AStyle;
  FHandleValid:=False;
end;

Procedure TExtBrush.SetExtStyle(const AStyle:TExtBrushStyle);
begin
  PExtBrushStyle(@FStyle)^:=AStyle;
  FHandleValid:=FALSE;
  FValid:=False;
end;

{==============================================================================+
TExtPen
+==============================================================================}

Function TExtPen.GetExtStyle:TExtPenStyle;
begin
  Result:=PExtPenStyle(@FStyle)^;
end;

Procedure TExtPen.SetColor(const AColor:TColor);
begin
  FColor:=AColor;
  FHandleValid:=False;
end;

Procedure TExtPen.SetFillColor(const AColor:TColor);
begin
  FFillColor:=AColor;
end;

Procedure TExtPen.SetScale(const AScale:Double);
begin
  FScale:=AScale;
end;

Procedure TExtPen.SetStyle(const AStyle:Integer);
begin
  FStyle:=AStyle;
  FHandleValid:=False;
end;

Procedure TExtPen.SetWidth(const AWidth:Integer);
begin
  FWidth:=AWidth;
  FHandleValid:=False;
end;

Procedure TExtPen.SetExtStyle
 (
 const AStyle    : TExtPenStyle
 );
begin
  PExtPenStyle(@FStyle)^:=AStyle;
  FHandleValid:=FALSE;
end;

{==============================================================================+
  TExtCanvas
+==============================================================================}

Constructor TExtCanvas.Create(AOwner:TComponent);
begin
  inherited Create(AOwner);
  // create hash-tables
  FPenHashTable:=TPenHashTable.Create;
  FBrushHashTable:=TBrushHashTable.Create;
  FFontHashTable:=TFontHashTable.Create;
  // create critical section for exclusive access to the bitmap-handle
  FHandleSection:=TCriticalSection.Create;
  // create pen and brush
  FPen:=TExtPen.Create;
  FBrush:=TExtBrush.Create;
  FFont:=TExtFont.Create;
  // set default arc-resolution to 1 degree
  FArcResolution:=Pi/180;
  // set refresh-interval to 1 second and create refresh-thread
  FRefreshThread:=TRefreshThread.Create(Self);
  // create clip-stack
  FClipStack:=TIntList.Create;
  // create region that holds validated areas
  FInvalidRegions:=CreateRectRgn(0,0,0,0);
end;

Destructor TExtCanvas.Destroy;
begin
  // free the refreshing-thread
  FRefreshThread.Free;
  // select system-pens and brushes for the canvas
  DeselectAllTools;
  // remove bitmap-cache
  FreeBitmapCache;
  // remove brush, pen and font
  FBrush.Free;
  FPen.Free;
  FFont.Free;
  // free clip-stack
  FClipStack.Free;
  // destroy critical section
  FHandleSection.Free;
  // free hash-tables
  FPenHashTable.Free;
  FBrushHashTable.Free;
  FFontHashTable.Free;
  if FInvalidRegions<>0 then DeleteObject(FInvalidRegions);
  inherited Destroy;
end;

{******************************************************************************+
  Procedure TExtCanvas.ClippedPolyline
--------------------------------------------------------------------------------
  Draws a polyline. The polyline is clipped to the current clipping-rectangle.
  The point-list is set to the clipped-version of the polyline.
+******************************************************************************}
Procedure TExtCanvas.ClippedPolyline(var Points:TGrPointList;DrawVertices:Boolean);
var Cnt          : Integer;
    WinPoints    : PPointArray;
begin
  Assert(not RectEmpty(FClipRect));
  Clipping.ClipPolyline(Points,FClipRect);
  if Points.Count>0 then begin
    GetMem(WinPoints,Points.Count*SizeOf(TPoint));
    try
      for Cnt:=0 to Points.Count-1 do begin
        WinPoints^[Cnt].X:=Round(Points[Cnt].X);
        WinPoints^[Cnt].Y:=Round(Points[Cnt].Y);
      end;
      Polyline(WinPoints^,Points.Count,DrawVertices);
    finally
      FreeMem(WinPoints,Points.Count*SizeOf(TPoint));
    end;
  end;
end;

{******************************************************************************+
  Procedure TExtCanvas.ClippedPolygon
--------------------------------------------------------------------------------
  Draws a polygon. The polygon is clipped to the current clipping-rectangle.
  The point-list is set to the clipped-version of the polygon.
+******************************************************************************}
Procedure TExtCanvas.ClippedPolygon(var  Points:TGrPointList;DrawVertices:Boolean);
var Cnt          : Integer;
    WinPoints    : PPointArray;
begin
  Assert(not RectEmpty(FClipRect));
  Clipping.ClipPolygon(Points,FClipRect);
  if Points.Count>0 then begin
    GetMem(WinPoints,Points.Count*SizeOf(TPoint));
    try
      for Cnt:=0 to Points.Count-1 do begin
        WinPoints^[Cnt].X:=Round(Points[Cnt].X);
        WinPoints^[Cnt].Y:=Round(Points[Cnt].Y);
      end;
      Polygon(WinPoints^,Points.Count,DrawVertices);
    finally
      FreeMem(WinPoints,Points.Count*SizeOf(TPoint));
    end;
  end;
end;

{******************************************************************************+
  Procedure TExtCanvas.ClippedPolygon
--------------------------------------------------------------------------------
  Draws a polygon. The polygon is clipped to the current clipping-rectangle.
  The point-list is set to the clipped-version of the polygon.
+******************************************************************************}
Procedure TExtCanvas.ClippedPolygon(var  Points:TGrPointList;DrawVertices:Boolean;
    UserFillProc:TUserFillProc;Param1,Param2,Param3:LongInt);
var Cnt            : Integer;
    WinPoints      : PPointArray;
begin
  Assert(not RectEmpty(FClipRect));
  Clipping.ClipPolygon(Points,FClipRect);
  if Points.Count>0 then begin
    GetMem(WinPoints,Points.Count*SizeOf(TPoint));
    try
      for Cnt:=0 to Points.Count-1 do begin
        WinPoints^[Cnt].X:=Round(Points[Cnt].X);
        WinPoints^[Cnt].Y:=Round(Points[Cnt].Y);
      end;
      Polygon(WinPoints^,Points.Count,DrawVertices,UserFillProc,
          Param1,Param2,Param3);
    finally
      FreeMem(WinPoints,Points.Count*SizeOf(TPoint));
    end;
  end;
end;

{******************************************************************************+
  Procedure TExtCanvas.Polyline
--------------------------------------------------------------------------------
  Draws a polyline with the current pen-style. The points may be converted
  to coordinates needed by the xstyle-module. In this case the PointsConverted
  property is set to true. Draws a small rectangle at every vertex if
  DrawVertices is true.
+******************************************************************************}
Procedure TExtCanvas.Polyline(var Points;PointCount:Integer;DrawVertices:Boolean);
var Brush          : HBRUSH;
    Cnt            : Integer;
begin
  try
    if FPen.Style>=ltUserDefined then begin
      // convert points to coordinates needed by xstyles
      ConvertToXStylePoints(Points,PointCount);
      if FXLineStyle<>NIL then XPolyline(FDC,TPointArray(Points),PointCount,
          FXLineStyle^.XLineDefs,FXLineStyle^.Count,FPen.Color,FPen.FillColor,
          true,FPen.Scale);
      FPointsConverted:=TRUE;
    end
    else begin
      RequiredState([ecsPen]);
      Windows.Polyline(FDC,Points,PointCount);
      FPointsConverted:=FALSE;
    end;
    // draw rectangles at vertices
    if DrawVertices then if FDrawVertices then begin
      if not FPointsConverted then ConvertToXStylePoints(Points,PointCount);
      //Brush:=GetStockObject(Black_Brush);
      Brush:=CreateSolidBrush(FVerticesColor);
      for Cnt:=0 to PointCount-1 do with TPointArray(Points)[Cnt] do
        Windows.FillRect(FDC,Rect(X-HandleSize,Y-HandleSize,
            X+HandleSize+1,Y+HandleSize+1),Brush);
      DeleteObject(Brush);
      FPointsConverted:=TRUE;
    end;
    // reset the map-mode if it was changed
    if FPointsConverted then Windows.SetMapMode(FDC,Ord(FMapMode)+1);
  except
  end;
end;

{******************************************************************************+
  Procedure TExtCanvas.Polygon
--------------------------------------------------------------------------------
  Draws a polygon with the current pen-style. The points may be converted
  to coordinates needed by the xstyle-module. In this case the PointsConverted
  property is set to true. Draws a small rectangle at every vertex if
  DrawVertices is true.
+******************************************************************************}
Procedure TExtCanvas.Polygon(var Points;PointCount:Integer;DrawVertices:Boolean);
var Brush        : HBRUSH;
    Cnt          : Integer;
    APoint       : TPoint;
begin
  try

    if (FPen.FStyle>=ltUserDefined) or (FBrush.FStyle>=ptVectorXFill) then begin
      if FPen.Style<ltUserDefined then begin
        // convert pen-width to device-units
        APoint.X:=FPen.FWidth; APoint.Y:=FPen.FWidth;
        LPToDP(FDC,APoint,1);
        APoint.Y:=FPen.FWidth;
        FPen.Width:=APoint.X;
      end;
      // convert points to coordinates needed by xstyles
      ConvertToXStylePoints(Points,PointCount);
      if FBrush.Style<ptVectorXFill then begin
        // need the currently selected brush
        RequiredState([ecsBrush]);
        // select null-pen to avoid drawing of outline
        SelectObject(FDC,GetStockObject(NULL_PEN));
        // draw interior
        Windows.Polygon(FDC,Points,PointCount);
        // invalidate pen because currently null-pen selected
        FPen.FHandleValid:=FALSE;
      end
      else if (FBrush.FStyle=ptVectorXFill) and (FXFillStyle<>NIL) then
          XLFillPolygon(FDC,TPointArray(Points),PointCount,
          FXFillStyle^.XFillDefs,FXFillStyle^.Count,FBrush.FForeColor,
          FBrush.FBackColor,FBrush.FScale,FViewRotation)
      else if (FBrush.FStyle=ptBitmapXFill) and (FBitmapXFill<>NIL) then
          BMFillPolygon(FDC,TPointArray(Points),PointCount,
          FBitmapXFill.BitmapHandle,srcCopy,1);  //////
      if FPen.Style<ltUserDefined then begin
        // select pen for device-context
        RequiredState([ecsPen]);
        // draw outline
        Windows.Polyline(FDC,Points,PointCount);
        // restore original pen width
        FPen.Width:=APoint.Y;
      end
      else if FXLineStyle<>NIL then XPolyline(FDC,TPointArray(Points),PointCount,
          FXLineStyle^.XLineDefs,FXLineStyle^.Count,FPen.Color,FPen.FillColor,
          true,FPen.Scale);
      FPointsConverted:=TRUE;
    end
    else begin
      RequiredState([ecsPen,ecsBrush]);
      Windows.Polygon(FDC,Points,PointCount);
      FPointsConverted:=FALSE;
    end;
    // draw rectangles at vertices
    if DrawVertices then if FDrawVertices then begin
      if not FPointsConverted then ConvertToXStylePoints(Points,PointCount);
      //Brush:=GetStockObject(Black_Brush);
      Brush:=CreateSolidBrush(FVerticesColor);
      for Cnt:=0 to PointCount-1 do with TPointArray(Points)[Cnt] do
        Windows.FillRect(FDC,Rect(X-HandleSize,Y-HandleSize,
            X+HandleSize+1,Y+HandleSize+1),Brush);
      DeleteObject(Brush);
      FPointsConverted:=TRUE;
    end;
    // reset the map-mode if it was changed
    if FPointsConverted then Windows.SetMapMode(FDC,Ord(FMapMode)+1);
  except
  end;
end;

{******************************************************************************+
  Procedure TExtCanvas.Polygon
--------------------------------------------------------------------------------
  Draws a polygon with the current pen-style. The points may be converted
  to coordinates needed by the xstyle-module. In this case the PointsConverted
  property is set to true. Draws a small rectangle at every vertex if
  DrawVertices is true. Draws the polygon in the following order:
  1. Windows or user-defined fill-style, 2. Call of user-fill-function,
  3. Windows or user-defined outline
+******************************************************************************}
Procedure TExtCanvas.Polygon(var Points;PointCount:Integer;DrawVertices:Boolean;
    UserFillProc:TUserFillProc;Param1,Param2,Param3:LongInt);
var Brush          : HBRUSH;
    Cnt            : Integer;
    APoint         : TPoint;
begin
  try
    if (FPen.FStyle>=ltUserDefined) or (FBrush.FStyle>=ptVectorXFill) then begin
      if FPen.Style<ltUserDefined then begin
        // convert pen-width to device-units
        APoint.X:=FPen.FWidth; APoint.Y:=FPen.FWidth;
        LPToDP(FDC,APoint,1);
        APoint.Y:=FPen.FWidth;
        FPen.Width:=APoint.X;
      end;
      // convert points to coordinates needed by xstyles
      ConvertToXStylePoints(Points,PointCount);
      FPointsConverted:=TRUE;
    end
    else begin
      APoint.Y:=FPen.Width;
      FPointsConverted:=FALSE;
    end;
    if FBrush.Style<ptVectorXFill then begin
      // need the currently selected brush
      RequiredState([ecsBrush]);
      // select null-pen to avoid drawing of outline
      SelectObject(FDC,GetStockObject(NULL_PEN));
      // draw interior
      Windows.Polygon(FDC,Points,PointCount);
      // invalidate pen because currently null-pen selected
      FPen.FHandleValid:=FALSE;
    end
    else if (FBrush.FStyle=ptVectorXFill) and (FXFillStyle<>NIL) then
        XLFillPolygon(FDC,TPointArray(Points),PointCount,
        FXFillStyle^.XFillDefs,FXFillStyle^.Count,FBrush.FForeColor,
        FBrush.FBackColor,FBrush.FScale,FViewRotation)
    else if (FBrush.FStyle=ptBitmapXFill) and (FBitmapXFill<>NIL) then
        BMFillPolygon(FDC,TPointArray(Points),PointCount,
        FBitmapXFill.BitmapHandle,srcCopy,1);
    // convert points back to logical coordinates
    if FPointsConverted then begin
      Windows.SetMapMode(FDC,Ord(FMapMode)+1);
      DPToLP(FDC,Points,PointCount);
      FPointsConverted:=FALSE;
    end;
    // call user-fill-procedure
    // store current clipping
    PushClipping;
    try
      // set polygon clipping
      SetClippingPolygon(Points,PointCount);
      UserFillProc(Param1,Param2,Param3);
    finally
      // restore previous clipping
      PopClipping;
    end;
    // draw outline
    if FPen.Style<ltUserDefined then begin
      // select pen for device-context
      RequiredState([ecsPen]);
      // draw outline
      Windows.Polyline(FDC,Points,PointCount);
      // restore original pen width
      FPen.Width:=APoint.Y;
      FPointsConverted:=FALSE;
    end
    else if FXLineStyle<>NIL then begin
      ConvertToXStylePoints(Points,PointCount);
      XPolyline(FDC,TPointArray(Points),PointCount,
        FXLineStyle^.XLineDefs,FXLineStyle^.Count,FPen.Color,FPen.FillColor,
        true,FPen.Scale);
      FPointsConverted:=TRUE;
    end;
    // draw rectangles at vertices
    if DrawVertices then if FDrawVertices then begin
      if not FPointsConverted then ConvertToXStylePoints(Points,PointCount);
      //Brush:=GetStockObject(Black_Brush);
      Brush:=CreateSolidBrush(FVerticesColor);
      for Cnt:=0 to PointCount-1 do with TPointArray(Points)[Cnt] do
        Windows.FillRect(FDC,Rect(X-HandleSize,Y-HandleSize,
            X+HandleSize+1,Y+HandleSize+1),Brush);
      DeleteObject(Brush);
      FPointsConverted:=TRUE;
    end;
    // reset the map-mode if it was changed
    if FPointsConverted then Windows.SetMapMode(FDC,Ord(FMapMode)+1);
  except
    // Windows 95 sometimes causes an exception in the GDI
  end;
end;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}
Procedure TExtCanvas.Ellipse(const Center: TPoint; XRadius: Integer; YRadius: Integer);
var Points       : Array[0..1] of TPoint;
    APoint       : TPoint;
begin
   if (FPen.Style >= ltUserDefined) or (FBrush.Style >= ptVectorXFill) then
      begin
         if FPen.Style < ltUserDefined then
            begin
               // convert pen-width to device-units
               APoint.X:=FPen.FWidth; APoint.Y:=FPen.FWidth;
               LPToDP(FDC,APoint,1);
               APoint.Y:=FPen.FWidth;
               FPen.Width:=APoint.X;
            end;
         // convert points to coordinates needed by xstyles
         Points[0]:=Center;
         Points[1].X:=XRadius;
         Points[1].Y:=YRadius;
         ConvertToXStylePoints(Points,2);
         XRadius:=Points[1].X;
         YRadius:=Points[1].Y;
         if FBrush.Style < ptVectorXFill then
            begin
               // need the currently selected brush
               RequiredState([ecsBrush]);
               // select null-pen to avoid drawing of outline
               SelectObject(FDC,GetStockObject(NULL_PEN));
               // draw interior
               Windows.Ellipse(FDC,Points[0].X-XRadius,Points[0].Y-YRadius,
                           Points[0].X+XRadius,Points[0].Y+YRadius);
               // invalidate pen because currently null-pen selected
               FPen.FHandleValid:=FALSE;
            end
         else
            if (FBrush.FStyle = ptVectorXFill) and (FXFillStyle <> NIL) then
               XLFillEllipse(FDC,Points[0].X-Points[1].X,Points[0].Y-Points[1].Y,
                                 Points[0].X+Points[1].X,Points[0].Y+Points[1].Y,
                                 FXFillStyle^.XFillDefs,FXFillStyle^.Count,FBrush.ForeColor,
                                 FBrush.BackColor,FBrush.Scale,FViewRotation)
            else
               if (FBrush.FStyle = ptBitmapXFill) and (FBitmapXFill <> NIL) then
                  BMFillEllipse(FDC,Points[0].X-Points[1].X,Points[0].Y-Points[1].Y,
                                    Points[0].X+Points[1].X,Points[0].Y+Points[1].Y,
                                    FBitmapXFill.BitmapHandle,srcCopy,1);
            if FPen.Style < ltUserDefined then
               begin
                  // need the pen
                  RequiredState([ecsPen]);
                  // select null-pen to avoid drawing of outline
                  SelectObject(FDC,GetStockObject(HOLLOW_BRUSH));
                  Windows.Ellipse(FDC,Points[0].X-XRadius,Points[0].Y-YRadius,
                              Points[0].X+XRadius,Points[0].Y+YRadius);
                  // invalidate brush because currently null-brush selected
                  FBrush.FHandleValid:=FALSE;
                  // restore original pen width
                  FPen.Width:=APoint.Y;
               end
            else
               if FXLineStyle <> NIL then
                  XEllipse(FDC,Points[0].X,Points[0].Y,XRadius,YRadius,0.0,
                           FXLineStyle^.XLineDefs,FXLineStyle^.Count,
                           FPen.Color,FPen.FillColor,False,FPen.Scale);
            Windows.SetMapMode(FDC,Ord(FMapMode)+1);
      end
   else
      begin
         RequiredState([ecsPen,ecsBrush]);
         Windows.Ellipse(FDC,Center.X-XRadius,Center.Y-YRadius,Center.X+XRadius,Center.Y+YRadius);
      end;
end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

{******************************************************************************+
  Procedure TExtCanvas.Circle
--------------------------------------------------------------------------------
+******************************************************************************}
Procedure TExtCanvas.Circle(const Center:TPoint;Radius:Integer);
var Points       : Array[0..1] of TPoint;
    APoint       : TPoint;
begin
  if (FPen.Style>=ltUserDefined) or (FBrush.Style>=ptVectorXFill) then begin
    if FPen.Style<ltUserDefined then begin
      // convert pen-width to device-units
      APoint.X:=FPen.FWidth; APoint.Y:=FPen.FWidth;
      LPToDP(FDC,APoint,1);
      APoint.Y:=FPen.FWidth;
      FPen.Width:=APoint.X;
    end;
    // convert points to coordinates needed by xstyles
    Points[0]:=Center;
    Points[1].X:=Radius;
    Points[1].Y:=Radius;
    ConvertToXStylePoints(Points,2);
    Radius:=Points[1].X;
    if FBrush.Style<ptVectorXFill then begin
      // need the currently selected brush
      RequiredState([ecsBrush]);
      // select null-pen to avoid drawing of outline
      SelectObject(FDC,GetStockObject(NULL_PEN));
      // draw interior                            
      Windows.Ellipse(FDC,Points[0].X-Radius,Points[0].Y-Radius,Points[0].X+Radius,Points[0].Y+Radius);
      // invalidate pen because currently null-pen selected
      FPen.FHandleValid:=FALSE;
    end
    else if (FBrush.FStyle=ptVectorXFill) and (FXFillStyle<>NIL) then
        XLFillEllipse(FDC,Points[0].X-Points[1].X,
            Points[0].Y-Points[1].Y,Points[0].X+Points[1].X,Points[0].Y+Points[1].Y,
            FXFillStyle^.XFillDefs,FXFillStyle^.Count,FBrush.ForeColor,
            FBrush.BackColor,FBrush.Scale,FViewRotation)
    else if (FBrush.FStyle=ptBitmapXFill) and (FBitmapXFill<>NIL) then
        BMFillEllipse(FDC,Points[0].X-Points[1].X,
            Points[0].Y-Points[1].Y,Points[0].X+Points[1].X,Points[0].Y+Points[1].Y,
            FBitmapXFill.BitmapHandle,srcCopy,1);
    if FPen.Style<ltUserDefined then begin
      // need the pen
      RequiredState([ecsPen]);
      // select null-pen to avoid drawing of outline
      SelectObject(FDC,GetStockObject(HOLLOW_BRUSH));
      Windows.Ellipse(FDC,Points[0].X-Radius,Points[0].Y-Radius,Points[0].X+Radius,
          Points[0].Y+Radius);
      // invalidate brush because currently null-brush selected
      FBrush.FHandleValid:=FALSE;
      // restore original pen width
      FPen.Width:=APoint.Y;
    end
    else if FXLineStyle<>NIL then XEllipse(FDC,Points[0].X,Points[0].Y,
        Radius,Radius,0.0,FXLineStyle^.XLineDefs,FXLineStyle^.Count,FPen.Color,
            FPen.FillColor,False,FPen.Scale);
    Windows.SetMapMode(FDC,Ord(FMapMode)+1);
  end
  else begin
    RequiredState([ecsPen,ecsBrush]);
    Windows.Ellipse(FDC,Center.X-Radius,Center.Y-Radius,Center.X+Radius,Center.Y+Radius);
  end;  
end;

{******************************************************************************+
  Procedure TExtCanvas.Circle
--------------------------------------------------------------------------------
+******************************************************************************}
Procedure TExtCanvas.Circle(const Center:TPoint;Radius:Integer;
    UserFillProc:TUserFillProc;Param1,Param2,Param3:LongInt);
var Points       : Array[0..1] of TPoint;
    APoint       : TPoint;
begin
  try
    Points[0]:=Center;
    Points[1].X:=Radius;
    Points[1].Y:=Radius;
    if (FPen.FStyle>=ltUserDefined) or (FBrush.FStyle>=ptVectorXFill) then begin
      if FPen.Style<ltUserDefined then begin
        // convert pen-width to device-units
        APoint.X:=FPen.FWidth; APoint.Y:=FPen.FWidth;
        LPToDP(FDC,APoint,1);
        APoint.Y:=FPen.FWidth;
        FPen.Width:=APoint.X;
      end;
      // convert points to coordinates needed by xstyles
      ConvertToXStylePoints(Points,2);
      FPointsConverted:=TRUE;
    end
    else begin
      APoint.Y:=FPen.Width;
      FPointsConverted:=FALSE;
    end;
    if FBrush.Style<ptVectorXFill then begin
      // need the currently selected brush
      RequiredState([ecsBrush]);
      // select null-pen to avoid drawing of outline
      SelectObject(FDC,GetStockObject(NULL_PEN));
      // draw interior
      Windows.Ellipse(FDC,Points[0].X-Points[1].X,Points[0].Y-Points[1].X,Points[0].X+Points[1].X,Points[0].Y+Points[1].X);
      // invalidate pen because currently null-pen selected
      FPen.FHandleValid:=FALSE;
    end
    else if (FBrush.FStyle=ptVectorXFill) and (FXFillStyle<>NIL) then
        XLFillEllipse(FDC,Points[0].X-Points[1].X,Points[0].Y-Points[1].Y,
            Points[0].X+Points[1].X,Points[0].Y+Points[1].Y,
            FXFillStyle^.XFillDefs,FXFillStyle^.Count,FBrush.ForeColor,
            FBrush.BackColor,FBrush.Scale,FViewRotation)
    else if (FBrush.FStyle=ptBitmapXFill) and (FBitmapXFill<>NIL) then
        BMFillEllipse(FDC,Points[0].X-Points[1].X,
            Points[0].Y-Points[1].Y,Points[0].X+Points[1].X,Points[0].Y+Points[1].Y,
            FBitmapXFill.BitmapHandle,srcCopy,1);
    // convert points back to logical coordinates
    if FPointsConverted then begin
      Windows.SetMapMode(FDC,Ord(FMapMode)+1);
      DPToLP(FDC,Points,2);
      FPointsConverted:=FALSE;
    end;
    // call user-fill-procedure
    // store current clipping
    PushClipping;
    try
      // set clipping-circle
      SetClippingCircle(Center,Points[1].X);
      UserFillProc(Param1,Param2,Param3);
    finally
      // restore previous clipping
      PopClipping;
    end;
    if FPen.Style<ltUserDefined then begin
      // need the pen
      RequiredState([ecsPen]);
      // select null-pen to avoid drawing of outline
      SelectObject(FDC,GetStockObject(HOLLOW_BRUSH));
      Windows.Ellipse(FDC,Points[0].X-Points[1].X,Points[0].Y-Points[1].X,Points[0].X+Points[1].X,
          Points[0].Y+Points[1].X);
      // invalidate brush because currently null-brush selected
      FBrush.FHandleValid:=FALSE;
      // restore original pen width
      FPen.Width:=APoint.Y;
      FPointsConverted:=FALSE;
    end
    else if FXLineStyle<>NIL then begin
      ConvertToXStylePoints(Points,2);
      XEllipse(FDC,Points[0].X,Points[0].Y,
        Points[1].X,Points[1].X,0.0,FXLineStyle^.XLineDefs,FXLineStyle^.Count,
            FPen.Color,FPen.FillColor,False,FPen.Scale);
      FPointsConverted:=TRUE;
    end;
    if FPointsConverted then Windows.SetMapMode(FDC,Ord(FMapMode)+1);
  except
  end;
end;

{******************************************************************************+
  Function TExtCanvas.ClippedCircle
--------------------------------------------------------------------------------
  Draws a circle with the specified radius around the specified center-point.
  The circle is drawn with the current pen and the actual brush. The circle
  is clipped to the current clipping-rectangle. FALSE is returned if the
  circle is not drawn because no intersection with the clipping-rectangle
  was found (because the circle lies fully outside the clipping-area or
  fully inside).
+******************************************************************************}
Procedure TExtCanvas.ClippedCircle(const Center:TGrPoint;const Radius:Double);
var ClipCount    : Integer;
    ClipInfo     : TClippingInfo;
    CurVisible   : Boolean;
    PointCnt     : Integer;
    Points       : Array[0..2000] of TPoint;
    Cnt          : Integer;
    Cnt1         : Integer;
    Angle1       : Double;
    Angle2       : Double;
    Steps        : Integer;
    SinAngle     : Double;
    CosAngle     : Double;
begin
  Assert(not RectEmpty(FClipRect));
  ClipCount:=ClipCircle(Center,Radius,FClipRect,ClipInfo);
  if ClipCount>0 then begin
    CurVisible:=not ClipInfo.FirstVisible;
    PointCnt:=0;
    for Cnt:=0 to ClipCount-1 do begin
      if not CurVisible or ClipInfo.EdgeInfo[Cnt] then begin
        Points[PointCnt].X:=Round(ClipInfo.Points[Cnt].X);
        Points[PointCnt].Y:=Round(ClipInfo.Points[Cnt].Y);
        Inc(PointCnt);
      end;
      if not ClipInfo.EdgeInfo[Cnt] then begin
        CurVisible:=not CurVisible;
        if CurVisible then begin
          Angle1:=LineAngle(Center,ClipInfo.Points[Cnt]);
          Angle2:=LineAngle(Center,ClipInfo.Points[(Cnt+1) Mod ClipCount]);
          if Angle2<Angle1 then Angle2:=Angle2+2*Pi-Angle1
          else Angle2:=Angle2-Angle1;
          Steps:=Max(1,Round(Angle2/FArcResolution));
          Angle2:=Angle2/Steps;
          for Cnt1:=PointCnt to PointCnt+Steps do begin
            SinCos(Angle1,SinAngle,CosAngle);
            Points[Cnt1].X:=Round(Center.X+Radius*CosAngle);
            Points[Cnt1].Y:=Round(Center.Y+Radius*SinAngle);
            Angle1:=Angle1+Angle2;
          end;
          PointCnt:=PointCnt+Steps+1;
        end;
      end;
    end;
    Polygon(Points,PointCnt,FALSE);
  end
  // no intersections with clipping-rectangle found, check if circle
  // lies inside the clipping-area
  else if (Center.X-Radius>=FClipRect.Left) then if (Center.Y-Radius<=FClipRect.Top) then
      if (Center.X+Radius<=FClipRect.Right) then if (Center.Y+Radius>=FClipRect.Bottom) then
    Circle(Point(Round(Center.X),Round(Center.Y)),Round(Radius));
end;

{******************************************************************************+
  Function TExtCanvas.ClippedCircle
--------------------------------------------------------------------------------
  Draws a circle with the specified radius around the specified center-point.
  The circle is drawn with the current pen and the actual brush. The circle
  is clipped to the current clipping-rectangle. FALSE is returned if the
  circle is not drawn because no intersection with the clipping-rectangle
  was found (because the circle lies fully outside the clipping-area or
  fully inside).
+******************************************************************************}
Procedure TExtCanvas.ClippedCircle(const Center:TGrPoint;const Radius:Double;
    UserFillProc:TUserFillProc;Param1,Param2,Param3:LongInt);
var ClipCount    : Integer;
    ClipInfo     : TClippingInfo;
    CurVisible   : Boolean;
    PointCnt     : Integer;
    Points       : Array[0..2000] of TPoint;
    Cnt          : Integer;
    Cnt1         : Integer;
    Angle1       : Double;
    Angle2       : Double;
    Steps        : Integer;
    SinAngle     : Double;
    CosAngle     : Double;
begin
  Assert(not RectEmpty(FClipRect));
  ClipCount:=ClipCircle(Center,Radius,FClipRect,ClipInfo);
  if ClipCount>0 then begin
    CurVisible:=not ClipInfo.FirstVisible;
    PointCnt:=0;
    for Cnt:=0 to ClipCount-1 do begin
      if not CurVisible or ClipInfo.EdgeInfo[Cnt] then begin
        Points[PointCnt].X:=Round(ClipInfo.Points[Cnt].X);
        Points[PointCnt].Y:=Round(ClipInfo.Points[Cnt].Y);
        Inc(PointCnt);
      end;
      if not ClipInfo.EdgeInfo[Cnt] then begin
        CurVisible:=not CurVisible;
        if CurVisible then begin
          Angle1:=LineAngle(Center,ClipInfo.Points[Cnt]);
          Angle2:=LineAngle(Center,ClipInfo.Points[(Cnt+1) Mod ClipCount]);
          if Angle2<Angle1 then Angle2:=Angle2+2*Pi-Angle1
          else Angle2:=Angle2-Angle1;
          Steps:=Max(1,Round(Angle2/FArcResolution));
          Angle2:=Angle2/Steps;
          for Cnt1:=PointCnt to PointCnt+Steps do begin
            SinCos(Angle1,SinAngle,CosAngle);
            Points[Cnt1].X:=Round(Center.X+Radius*CosAngle);
            Points[Cnt1].Y:=Round(Center.Y+Radius*SinAngle);
            Angle1:=Angle1+Angle2;
          end;
          PointCnt:=PointCnt+Steps+1;
        end;
      end;
    end;
    Polygon(Points,PointCnt,FALSE,UserFillProc,Param1,Param2,Param3);
  end
  // no intersections with clipping-rectangle found, check if circle
  // lies inside the clipping-area
  else if (Center.X>=FClipRect.Left) then if (Center.Y<=FClipRect.Top) then
      if (Center.X<=FClipRect.Right) then if (Center.Y>=FClipRect.Bottom) then
    Circle(Point(Round(Center.X),Round(Center.Y)),Round(Radius),UserFillProc,
        Param1,Param2,Param3);
end;

{******************************************************************************+
  Procedure TExtCanvas.SetDC
--------------------------------------------------------------------------------

+******************************************************************************}
Procedure TExtCanvas.SetDC(NewDC:HDC);
begin
  if FHandle<>NewDC then begin
    FHandleSection.Enter;
    try
      DeselectAllTools;
      FHandle:=NewDC;
      // set the map-mode for the device-context
      if FHandle<>0 then Windows.SetMapMode(FHandle,Ord(FMapMode)+1);
      if FDC = 0 then FDC:=FHandle; { Error handling for XPolyline }
      // if not chached drawing set output-handle to canvas
      if FDrawCached then RecreateBitmapCache
      else FDC:=FHandle;
    finally
      FHandleSection.Leave;
    end;
  end;
end;

Procedure TExtCanvas.SetCached(ACached:Boolean);
begin
  if FCached<>ACached then begin
    FCached:=ACached;
    if FCached then RecreateBitmapCache
    else begin
      SetDrawCached(False);
      FreeBitmapCache;
    end;
  end;
end;

{******************************************************************************+
  Procedure TExtCanvas.SetDrawCached
--------------------------------------------------------------------------------
  Sets the output-destination to the bitmap (true) or directly to the
  device-context (false). Can only be used if Cached is true.
+******************************************************************************}
Procedure TExtCanvas.SetDrawCached(ACached:Boolean);
begin
  if FDrawCached<>ACached then begin
    if not FCached then FDrawCached:=FALSE
    else FDrawCached:=ACached;
    // select system-pens and brushes for the canvas
    if FDC<>0 then begin
      SelectObject(FDC,GetStockObject(BLACK_PEN));
      SelectObject(FDC,GetStockObject(HOLLOW_BRUSH));
      SelectObject(FDC,GetStockObject(SYSTEM_FONT));
    end;
    // invalidate all drawing-objects
    InvalidateAllTools;
    // set the current drawing device-context
    if FDrawCached then FDC:=FBitmapDC
    else FDC:=FHandle;
  end;
end;

{******************************************************************************+
  Procedure TExtCanvas.ClipPolyline
--------------------------------------------------------------------------------
  Clips the specified polyline to the current clipping-rectangle.
+******************************************************************************}
Procedure TExtCanvas.ClipPolyline(var Points:TGrPointList);
begin
  Clipping.ClipPolyline(Points,FClipRect);
end;

{******************************************************************************+
  Procedure TExtCanvas.ClipPolygon
--------------------------------------------------------------------------------
  Clips the specified polygon to the current clipping-rectangle.
+******************************************************************************}
Procedure TExtCanvas.ClipPolygon(var Points:TGrPointList);
begin
  Clipping.ClipPolygon(Points,FClipRect);
end;

{******************************************************************************+
  Procedure TExtCanvas.Resize
--------------------------------------------------------------------------------
  Call this method if the size of the device-context changes. Resizes the
  cache-bitmap (if caching is activated).
+******************************************************************************}
Procedure TExtCanvas.Resize(const PhysicalSize:TRect);
begin
  FPhysicalSize:=PhysicalSize;
  if FCached then RecreateBitmapCache;
  DeleteObject(FInvalidRegions);
  FInvalidRegions:=CreateRectRgnIndirect(PhysicalSize);
end;

{******************************************************************************+
  Procedure TExtCanvas.Pop
--------------------------------------------------------------------------------
  Restores the previously by the push-operation stored pen-, brush- and
  font-style.
+******************************************************************************}
Procedure TExtCanvas.Pop;
var Item: PExtCanvasStyle;
begin
  Item:=FStack;
  if Item<>NIL then with Item^ do begin
    FStack:=Next;
    FBrush.ExtStyle:=BrushStyle;
    FPen.ExtStyle:=PenStyle;
    FFont.ExtStyle:=FontStyle;
    FXLineStyle:=XLineStyle;
    FXFillStyle:=XFillStyle;
    SetDrawCached(DrawCached);
  end;
  Dispose(Item); 
end;

{******************************************************************************+
  Procedure TExtCanvas.Push
--------------------------------------------------------------------------------
  Stores the current settings of the canvas. The settings may be reactivated
  using the pop-operation. The pen-, brush- and font-style are stored.
+******************************************************************************}
Procedure TExtCanvas.Push;
var Item         : PExtCanvasStyle;
begin
  New(Item);
  with Item^ do begin
    BrushStyle:=FBrush.ExtStyle;
    PenStyle:=FPen.ExtStyle;
    FontStyle:=FFont.ExtStyle;
    XLineStyle:=FXLineStyle;
    XFillStyle:=FXFillStyle;
    DrawCached:=FDrawCached;
    Next:=FStack;
  end;
  FStack:=Item;
end;

Procedure TExtCanvas.RequiredState(State:TExtCanvasState);
begin
  if ecsPen in State then begin
    if not FPen.FHandleValid then begin
      SelectObject(FDC,TPenHashTable(FPenHashTable)[FPen.ExtStyle]);
      FPen.FHandleValid:=True;
    end;
    if not FPen.FValid then begin
      FPen.FValid:=TRUE;
    end;
  end;
  if ecsBrush in State then begin
    if not FBrush.FHandleValid then begin
      // create and select the brush with the specified color
      SelectObject(FDC,TBrushHashTable(FBrushHashTable)[FBrush.ExtStyle]);
      FBrush.FHandleValid:=True;
    end;
    if not FBrush.FValid then begin
      // set the background-color and background-mode
      if FBrush.BackColor<0 then SetBKMode(FDC,TRANSPARENT)
      else begin
        SetBKColor(FDC,FBrush.BackColor);
        SetBKMode(FDC,OPAQUE);
      end;
      FBrush.FValid:=True;
    end;
  end;
  if ecsFont in State then begin
    if not FFont.FHandleValid then begin
      SelectObject(FDC,TFontHashTable(FFontHashTable)[FFont.ExtStyle]);
      FFont.FHandleValid:=True;
    end;
    if not FFont.FValid then begin
      SetTextColor(FDC,FFont.Color);
      FFont.FValid:=TRUE;                                         
    end;
  end;
end;

Procedure TExtCanvas.ConvertToXStylePoints(var Points;PointCount:Integer);
begin
  // convert points to device-coordinates
  LPToDP(FDC,Points,PointCount);
  // switch to pixelwise -map-mode
  Windows.SetMapMode(FDC,mm_Text);
end;

Procedure TExtCanvas.SetMapMode(AMapMode:TMapMode);
begin
  FMapMode:=AMapMode;
  if (FHandle<>0) then Windows.SetMapMode(FHandle,Ord(FMapMode)+1);
  if (FBitmapDC<>0) then Windows.SetMapMode(FBitmapDC,Ord(FMapMode)+1);
end;

{******************************************************************************+
  Procedure TExtCanvas.DrawFromCache
--------------------------------------------------------------------------------
  Redraws the spezified rectangle from the bitmap-cache. The rectangle-
  coordinates must be given in logical coordinates.
+******************************************************************************}
Procedure TExtCanvas.DrawFromCache(const ARect:TRect);
begin
     FHandleSection.Enter;
     try
       if (FBItmapDC<>0) and (FHandle<>0) then with ARect do BitBlt(FHandle,
           Left,Top,Right-Left,Bottom-Top,FBitmapDC,Left,Top,srcCopy);
     finally
       FHandleSection.Leave;
     end;
end;

//++ Glukhov DragView Build#150 30.01.01
Procedure TExtCanvas.DrawFromCacheWithOffset(const ARect:TRect; Xoff, Yoff: Integer);
var
  Rect  : TRect;
begin
    FHandleSection.Enter;
    try
      with ARect do begin
        if ((Left < Right) and (Xoff > 0))
        or ((Left > Right) and (Xoff < 0)) then begin
          Rect:= ARect;
          Rect.Right:= ARect.Left + Xoff + 1;
          windows.FillRect( FHandle, Rect, GetStockObject(White_Brush) );
        end;
        if ((Top < Bottom) and (Yoff > 0))
        or ((Top > Bottom) and (Yoff < 0)) then begin
          Rect:= ARect;
          Rect.Bottom:= ARect.Top + Yoff + 1;
          windows.FillRect( FHandle, Rect, GetStockObject(White_Brush) );
        end;

        if (FBItmapDC<>0) and (FHandle<>0)
        then BitBlt(
            FHandle, Left+Xoff, Top+Yoff, Right-Left, Bottom-Top,
            FBitmapDC, Left, Top, srcCopy );

        if ((Left < Right) and (Xoff < 0))
        or ((Left > Right) and (Xoff > 0)) then begin
          Rect:= ARect;
          Rect.Left:= ARect.Right + Xoff - 1;
          windows.FillRect( FHandle, Rect, GetStockObject(White_Brush) );
        end;
        if ((Top < Bottom) and (Yoff < 0))
        or ((Top > Bottom) and (Yoff > 0)) then begin
          Rect:= ARect;
          Rect.Top:= ARect.Bottom + Yoff - 1;
          windows.FillRect( FHandle, Rect, GetStockObject(White_Brush) );
        end;
      end;
    finally
      FHandleSection.Leave;
    end;
end;
//-- Glukhov DragView Build#150 30.01.01

Procedure TExtCanvas.BeginPaint;
begin
  // force reselection of pen, brush and font
  InvalidateAllTools;
  // if cached drawing->enable thread to periodically update the screen
  if FDrawCached then FRefreshThread.Start;
end;

Procedure TExtCanvas.EndPaint;
begin
  FRefreshThread.Stop;
  if FDrawCached then begin
     DrawFromCache(LogicalSize);
     FRefreshThread.Stop;
  end;
end;

{******************************************************************************+
  Procedure TExtCanvas.FillRect
--------------------------------------------------------------------------------
  Fills the specified rectangle with the specified brush.
+******************************************************************************}
Procedure TExtCanvas.FillRect(const Rect:TRect;Brush:THandle);
begin
  Windows.FillRect(FDC,Rect,Brush);
end;

{******************************************************************************+
  Function TExtCanvas.GetLogicalSize
--------------------------------------------------------------------------------
  Calculates the size of the device-context in logical units.
+******************************************************************************}
Function TExtCanvas.GetLogicalSize:TRect;
begin
  Result:=FPhysicalSize;
  DPToLP(FHandle,Result,2);
end;

{******************************************************************************+
  Procedure TExtCanvas.RecreateBitmapCache
--------------------------------------------------------------------------------
  Creates a new bitmap-cache. Any already existing cache is deleted. The size
  of the bitmap is taken from FPhysicalSize. The function first checks if
  the size has changed, otherwise no new cache-bitmap is created.
+******************************************************************************}
Procedure TExtCanvas.RecreateBitmapCache;
begin
  // check if size has changed
  if (RectWidth(FBitmapSize)<>RectWidth(FPhysicalSize)) or
      (RectHeight(FBitmapSize)<>RectHeight(FPhysicalSize)) then begin
    FHandleSection.Enter;
    try
      // reselect old bitmap
      if FOldBitmap<>0 then begin
        SelectObject(FBitmapDC,FOldBitmap);
        FOldBitmap:=0;
      end;
      // delete current bitmap
      if FBitmap<>0 then begin
        DeleteObject(FBitmap);
        FBitmap:=0;
      end;
      // create dc compatible to screen
      if FBitmapDC=0 then begin
        FBitmapDC:=CreateCompatibleDC(0);
        if FDrawCached then FDC:=FBitmapDC;
      end;
      // create new bitmap and select it into the device-context
      if FBitmapDC<>0 then begin
        FBitmap:=CreateCompatibleBitmap(FHandle,RectWidth(FPhysicalSize),
            RectHeight(FPhysicalSize));
        if FBitmap<>0 then begin
          FOldBitmap:=SelectObject(FBitmapDC,FBitmap);
          FBitmapSize:=FPhysicalSize;
          Windows.SetMapMode(FBitmapDC,Ord(FMapMode)+1);
        end;
      end;
    finally
      FHandleSection.Leave;
    end;
  end;
end;

{******************************************************************************+
  Procedure TExtCanvas.FreeBitmapCache
--------------------------------------------------------------------------------
  Frees a currently existing bitmap-cache.
+******************************************************************************}
Procedure TExtCanvas.FreeBitmapCache;
begin
  if FBitmapDC<>0 then begin
    FHandleSection.Enter;
    try
      if FOldBitmap<>0 then begin
        SelectObject(FBitmapDC,FOldBitmap);
        FOldBitmap:=0;
      end;
      if FBitmap<>0 then begin
        DeleteObject(FBitmap);
        FBitmap:=0;
      end;
      DeleteDC(FBitmapDC);
      FBitmapDC:=0;
      FillChar(FBitmapSize,SizeOf(FBitmapSize),0);
    finally
      FHandleSection.Leave;
    end;
  end;
end;

Procedure TExtCanvas.RefreshBitmapCache;
begin
    FHandleSection.Enter;
    try
      if FBitmapDC<>0 then begin
         if FOldBitmap<>0 then begin
            SelectObject(FBitmapDC,FOldBitmap);
            FOldBitmap:=0;
         end;
         if FBitmap<>0 then begin
            DeleteObject(FBitmap);
            FBitmap:=0;
         end;
         DeleteDC(FBitmapDC);
         FBitmapDC:=0;
         FillChar(FBitmapSize,SizeOf(FBitmapSize),0);
      end;
      if FBitmapDC=0 then begin
        FBitmapDC:=CreateCompatibleDC(0);
        if FDrawCached then FDC:=FBitmapDC;
      end;
      if FBitmapDC<>0 then begin
        FBitmap:=CreateCompatibleBitmap(GetDC(0),RectWidth(FPhysicalSize),RectHeight(FPhysicalSize));
        if FBitmap<>0 then begin
          FOldBitmap:=SelectObject(FBitmapDC,FBitmap);
          FBitmapSize:=FPhysicalSize;
          Windows.SetMapMode(FBitmapDC,Ord(FMapMode)+1);
        end;
      end;
    finally
      FHandleSection.Leave;
    end;
end;

{******************************************************************************+
  Procedure TExtCanvas.Arc
--------------------------------------------------------------------------------
  Draws an arc around the specified center with the specified radius and
  begin- and end-angles. The arc is interpolated by a polyline, the
  resolution is taken from FArcResolution.
+******************************************************************************}
Procedure TExtCanvas.Arc(const Center:TGrPoint;const Radius:Double;BeginAngle,
    EndAngle:Double);
var Points       : Array[0..2000] of TPoint;
    Cnt1         : Integer;
    PointCnt     : Integer;
    SinAngle     : Double;
    CosAngle     : Double;
begin
  // calculate arcangle
  if EndAngle<BeginAngle then EndAngle:=EndAngle+2*Pi-BeginAngle
  else EndAngle:=EndAngle-BeginAngle;
  // calculate number of points to draw and the angle-step
  PointCnt:=Max(1,Round(EndAngle/FArcResolution));
  PointCnt:=Min(PointCnt,Length(Points)-1); 
  EndAngle:=EndAngle/PointCnt;
  // calculate polyline-points
  for Cnt1:=0 to PointCnt do begin
    SinCos(BeginAngle,SinAngle,CosAngle);
    Points[Cnt1].X:=Round(Center.X+Radius*CosAngle);
    Points[Cnt1].Y:=Round(Center.Y+Radius*SinAngle);
    BeginAngle:=BeginAngle+EndAngle;
  end;
  // draw polyline, switch off (and restore) drawing of vertices
  Polyline(Points,PointCnt+1,FALSE);
end;

{******************************************************************************+
  Procedure TExtCanvas.ClippedArc
--------------------------------------------------------------------------------
  Draws an arc around the specified center with the specified radius and
  begin- and end-angles. The arc is interpolated by a polyline, the
  resolution is taken from FArcResolution. The arc is clipped to the current
  clipping-rectangle.
+******************************************************************************}
Procedure TExtCanvas.ClippedArc(const Center:TGrPoint;const Radius,BeginAngle,
    EndAngle:Double);
var Points       : Array[0..2000] of TPoint;
    ClipCount    : Integer;
    ClipInfo     : TClippingInfo;
    Cnt          : Integer;
    Cnt1         : Integer;
    Angle1       : Double;
    Angle2       : Double;
    SinAngle     : Double;
    CosAngle     : Double;
    PointCnt     : Integer;
begin
  // calculate clipping-information
  Assert(not RectEmpty(FClipRect));
  ClipCount:=ClipArc(Center,Radius,BeginAngle,EndAngle,FClipRect,ClipInfo);
  if ClipCount>0 then begin
    // draw clipped arc-segments
    for Cnt:=0 to ClipCount Div 2-1 do begin
      // calculate start- and end-angle of the segment
      Angle1:=LineAngle(Center,ClipInfo.Points[Cnt*2]);
      Angle2:=LineAngle(Center,ClipInfo.Points[Cnt*2+1]);
      // calculate arcangle
      if Angle2<Angle1 then Angle2:=Angle2+2*Pi-Angle1
      else Angle2:=Angle2-Angle1;
      // calculate number of points to draw and the angle-step
      PointCnt:=Max(1,Round(Angle2/FArcResolution));
      PointCnt:=Min(PointCnt,Length(Points)-1);
      Angle2:=Angle2/PointCnt;
      // calculate points for the segment
      for Cnt1:=0 to PointCnt do begin
        SinCos(Angle1,SinAngle,CosAngle);
        Points[Cnt1].X:=Round(Center.X+Radius*CosAngle);
        Points[Cnt1].Y:=Round(Center.Y+Radius*SinAngle);
        Angle1:=Angle1+Angle2;
      end;
      // draw segment-polyline
      Polyline(Points,PointCnt+1,FALSE);
    end;
  end
  // no intersections with clipping-rectangle found, check if circle
  // lies inside the clipping-area
  else if (Radius<FClipRect.Right-FClipRect.Left) then
      if (Center.X>=FClipRect.Left) then if (Center.Y<=FClipRect.Top) then
      if (Center.X<=FClipRect.Right) then if (Center.Y>=FClipRect.Bottom) then
    Arc(Center,Radius,BeginAngle,EndAngle);
end;

{******************************************************************************+
  Procedure TExtCanvas.IslandArea
--------------------------------------------------------------------------------
+******************************************************************************}
Procedure TExtCanvas.IslandArea(var Points;PointCount:Integer;const IslandInfo:TLongArray;
     IslandCount:Integer);
var Start          : Integer;
    Cnt            : Integer;
    Brush          : HBrush;
    APoint         : TPoint;
begin
  try
    if (FPen.Style>=ltUserDefined) or (FBrush.Style>=ptVectorXFill) then begin
      if FPen.Style<ltUserDefined then begin
        // convert pen-width to device-units
        APoint.X:=FPen.FWidth; APoint.Y:=FPen.FWidth;
        LPToDP(FDC,APoint,1);
        APoint.Y:=FPen.FWidth;
        FPen.Width:=APoint.X;
      end;
      // convert points to coordinates needed by xstyles
      ConvertToXStylePoints(Points,PointCount);
      if FBrush.Style<ptVectorXFill then begin
        // need the currently selected brush
        RequiredState([ecsBrush]);
        // select null-pen to avoid drawing of outline
        SelectObject(FDC,GetStockObject(NULL_PEN));
        // draw interior
        Windows.Polygon(FDC,Points,PointCount);
        // invalidate pen because currently null-pen selected
        FPen.FHandleValid:=FALSE;
      end
      else if (FBrush.FStyle=ptVectorXFill) and (FXFillStyle<>NIL) then
          XLFillPolygon(FDC,TPointArray(Points),PointCount,
              FXFillStyle^.XFillDefs,FXFillStyle^.Count,FBrush.ForeColor,
              FBrush.BackColor,FBrush.Scale,FViewRotation)
      else if (FBrush.FStyle=ptBitmapXFill) and (FBitmapXFill<>NIL) then
          BMFillPolygon(FDC,TPointArray(Points),PointCount,
              FBitmapXFill.BitmapHandle,srcCopy,1);
      if FPen.Style<ltUserDefined then begin
        // need the pen
        RequiredState([ecsPen]);
        // draw outline
        Start:=IslandInfo[0];
        Windows.Polyline(FDC,Points,Start);
        for Cnt:=1 to IslandCount-1 do begin
          Windows.Polyline(FDC,TPointArray(Points)[Start],IslandInfo[Cnt]);
          Start:=Start+IslandInfo[Cnt]+1;
        end;
        // restore original pen width
        FPen.Width:=APoint.Y;
      end
      else if FXLineStyle<>NIL then begin
        Start:=IslandInfo[0];
        XPolyline(FDC,TPointArray(Points),Start,FXLineStyle^.XLineDefs,FXLineStyle^.Count,FPen.Color,
            FPen.FillColor,False,FPen.Scale);
        for Cnt:=1 to IslandCount-1 do begin
          XPolyline(FDC,TPointArray(Points)[Start],IslandInfo[Cnt],FXLineStyle^.XLineDefs,FXLineStyle^.Count,
              FPen.Color,FPen.FillColor,False,FPen.Scale);
          Start:=Start+IslandInfo[Cnt]+1;
        end;
      end;
      FPointsConverted:=TRUE;
    end
    else begin
      // select the brush, select a NULL pen and draw interior
      RequiredState([ecsBrush]);
      SelectObject(FDC,GetStockObject(NULL_PEN));
      Windows.Polygon(FDC,Points,PointCount);
      FPen.FHandleValid:=FALSE;
      // draw outline
      RequiredState([ecsPen]);
      Start:=IslandInfo[0];
      Windows.Polyline(FDC,Points,Start);
      for Cnt:=1 to IslandCount-1 do begin
        Windows.Polyline(FDC,TPointArray(Points)[Start],IslandInfo[Cnt]);
        Start:=Start+IslandInfo[Cnt]+1;
      end;
      FPointsConverted:=FALSE;
    end;
    // draw points on vertices
    if FDrawVertices then begin
      if not FPointsConverted then ConvertToXStylePoints(Points,PointCount);
      //Brush:=GetStockObject(Black_Brush);
      Brush:=CreateSolidBrush(FVerticesColor);
      for Cnt:=0 to PointCount-1 do with TPointArray(Points)[Cnt] do
        Windows.FillRect(FDC,Rect(X-HandleSize,Y-HandleSize,
            X+HandleSize+1,Y+HandleSize+1),Brush);
      DeleteObject(Brush);
      FPointsConverted:=TRUE;
    end;
    if FPointsConverted then Windows.SetMapMode(FDC,Ord(FMapMode)+1);
  except
    // Windows 95 sometimes causes an exception in the GDI
  end;
end;

{******************************************************************************+
  Procedure TExtCanvas.IslandArea
--------------------------------------------------------------------------------
+******************************************************************************}
Procedure TExtCanvas.IslandArea(var Points;PointCount:Integer;const IslandInfo:TLongArray;
    IslandCount:Integer;UserFillProc:TUserFillProc;Param1,Param2,Param3:LongInt);
var Start          : Integer;
    Cnt            : Integer;
    Brush          : HBrush;
    APoint         : TPoint;      
begin
  try
    if (FPen.FStyle>=ltUserDefined) or (FBrush.FStyle>=ptVectorXFill) then begin
      if FPen.Style<ltUserDefined then begin
        // convert pen-width to device-units
        APoint.X:=FPen.FWidth; APoint.Y:=FPen.FWidth;
        LPToDP(FDC,APoint,1);
        APoint.Y:=FPen.FWidth;
        FPen.Width:=APoint.X;
      end;
      // convert points to coordinates needed by xstyles
      ConvertToXStylePoints(Points,PointCount);
      FPointsConverted:=TRUE;
    end
    else begin
      APoint.Y:=FPen.Width;
      FPointsConverted:=FALSE;
    end;
    if FBrush.Style<ptVectorXFill then begin
      // need the currently selected brush
      RequiredState([ecsBrush]);
      // select null-pen to avoid drawing of outline
      SelectObject(FDC,GetStockObject(NULL_PEN));
      // draw interior
      Windows.Polygon(FDC,Points,PointCount);
      // invalidate pen because currently null-pen selected
      FPen.FHandleValid:=FALSE;
    end                                 
    else if (FBrush.FStyle=ptVectorXFill) and (FXFillStyle<>NIL) then
        XLFillPolygon(FDC,TPointArray(Points),PointCount,
            FXFillStyle^.XFillDefs,FXFillStyle^.Count,FBrush.ForeColor,
            FBrush.BackColor,FBrush.Scale,FViewRotation)
    else if (FBrush.FStyle=ptBitmapXFill) and (FBitmapXFill<>NIL) then {!?!?!};
    // convert points back to logical coordinates
    if FPointsConverted then begin
      Windows.SetMapMode(FDC,Ord(FMapMode)+1);
      DPToLP(FDC,Points,PointCount);
      FPointsConverted:=FALSE;
    end;
    // store current clipping
    PushClipping;
    try
      // set polygon clipping
      SetClippingPolygon(Points,PointCount);
      UserFillProc(Param1,Param2,Param3);
    finally
      // restore previous clipping
      PopClipping;
    end;
    if FPen.Style<ltUserDefined then begin
      // need the pen
      RequiredState([ecsPen]);
      // draw outline
      Start:=IslandInfo[0];
      Windows.Polyline(FDC,Points,Start);
      for Cnt:=1 to IslandCount-1 do begin
        Windows.Polyline(FDC,TPointArray(Points)[Start],IslandInfo[Cnt]);
        Start:=Start+IslandInfo[Cnt]+1;
      end;
      // restore original pen width
      FPen.Width:=APoint.Y;
      FPointsConverted:=FALSE;
    end
    else if FXLineStyle<>NIL then begin
      ConvertToXStylePoints(Points,PointCount);
      Start:=IslandInfo[0];
      XPolyline(FDC,TPointArray(Points),Start,FXLineStyle^.XLineDefs,
          FXLineStyle^.Count,FPen.Color,FPen.FillColor,False,FPen.Scale);
      for Cnt:=1 to IslandCount-1 do begin
        XPolyline(FDC,TPointArray(Points)[Start],IslandInfo[Cnt],
            FXLineStyle^.XLineDefs,FXLineStyle^.Count,
            FPen.Color,FPen.FillColor,False,FPen.Scale);
        Start:=Start+IslandInfo[Cnt]+1;
      end;
      FPointsConverted:=TRUE;
    end;
    // draw points on vertices
    if FDrawVertices then begin
      if not FPointsConverted then ConvertToXStylePoints(Points,PointCount);
      //Brush:=GetStockObject(Black_Brush);
      Brush:=CreateSolidBrush(FVerticesColor);
      for Cnt:=0 to PointCount-1 do with TPointArray(Points)[Cnt] do
        Windows.FillRect(FDC,Rect(X-HandleSize,Y-HandleSize,
            X+HandleSize+1,Y+HandleSize+1),Brush);
      DeleteObject(Brush);      
      FPointsConverted:=TRUE;
    end;
    // reset the map-mode if it was changed
    if FPointsConverted then Windows.SetMapMode(FDC,Ord(FMapMode)+1);
  except
    // Windows 95 sometimes causes an exception in the GDI
  end;  
end;

Procedure TExtCanvas.ClippedIslandArea(var Points:TGrPointList;PointCount:Integer;
   const IslandInfo:TLongArray;IslandCount:Integer);
begin
end;

Procedure TExtCanvas.ClippedIslandArea(var Points:TGrPointList;PointCount:Integer;
   const IslandInfo:TLongArray;IslandCount:Integer;UserFillProc:TUserFillProc;
   Param1,Param2,Param3:LongInt);
begin
end;

Procedure TExtCanvas.TextOut(const Position:TGrPoint;Text:PChar;TextLength:Integer);
var Size         : TSize;
    TextWidth    : Double;
    TextHeight   : Double;
    CosAlpha     : Double;
    Cos2Alpha    : Double;
    SinAlpha     : Double;
    Offset       : Double;
begin
  RequiredState([ecsBrush,ecsFont]);
  if FFont.FAlignment<>taLeft then begin
    // calculate width of the text
    GetTextExtentPoint32(FDC,Text,TextLength,Size);
    TextWidth:=Size.cx;
    if GetFontType(FFont.Name) and TRUETYPE_FONTTYPE = 0 then begin
      // it's not a true-type font so bounds-rectangle is given instead of width
      TextHeight:=Size.cy;
      CosAlpha:=Abs(FFont.FCosAngle);
      SinAlpha:=Abs(FFont.FSinAngle);
      Cos2Alpha:=Abs(Cos(2*FFont.Angle));
      // handle special case with 45� rotation
      if Cos2Alpha<0.05 then TextWidth:=Abs(TextWidth/CosAlpha-FFont.Size)
      else TextWidth:=Abs((TextWidth*CosAlpha-TextHeight*SinAlpha)/Cos2Alpha);
    end;
    // calculate amount of move from upper-left point to text-startpoint
    if FFont.FAlignment=taRight then Offset:=-TextWidth
    else Offset:=-TextWidth/2.0;
    // calculate real position
    Windows.TextOut(FDC,Round(Position.X+FFont.FCosAngle*Offset),
        Round(Position.Y+FFont.FSinAngle*Offset),Text,TextLength);
  end
  else Windows.TextOut(FDC,Round(Position.X),Round(Position.Y),Text,TextLength);
end;

{******************************************************************************+
  Procedure TExtCanvas.ClippedTextOut
--------------------------------------------------------------------------------
  Draws the specified text with the current attributes of the font-property.
  The text is clipped to the clip-rectangle. The function currently does not
  support kerning because kerning is not used by any standard-windows text-
  output-function. 
--------------------------------------------------------------------------------
  This function may not work with systems where the character-width depends
  on the preceiding or the following character. Such a systems could be the
  arabian windows-version - must be checked.
--------------------------------------------------------------------------------
  Position = position where the text shall be drawn. Depending on the
    alignment-property of the font position specified the upper-left corner
    (taLeft), the upper-right corner (taRight) or the upper-center (taCentered)
    of the bounding-rectangle.
  Text = pointer to the string to draw
  TextLength = number of characters to draw
+******************************************************************************}
Procedure TExtCanvas.ClippedTextOut(Position:TGrPoint;Text:PChar;TextLength:Integer);
var RealWidth       : Double;
    CharWidths   : Array[0..255] of Double;
    Cnt          : Integer;
    ABCWidths    : TABC;
    CharPtr      : PChar;
    NextPtr      : PChar;
    CharCode     : Word;
    ClipPoints   : TGrPointList;
    SinAlpha     : Double;
    CosAlpha     : Double;
    Cos2Alpha    : Double;
    MinPosition  : Double;
    MaxPosition  : Double;
    Value        : Double;
    RealCharCount: Integer;
    StartChar    : Integer;
    LastChar     : Integer;
    IsDBCS       : Boolean;
    TextMetrics  : TTextMetric;
    Size         : TSize;
    Point        : TGrPoint;
    Offset       : Double;
begin
  // ensure Brush and Font are selected for the device-context
  RequiredState([ecsBrush,ecsFont]);
//!?!?how to implement in japanese windows nt?     IsDBCS:=IsDBCSFont(FDC);
  IsDBCS:=FALSE;
  RealWidth:=0;
  RealCharCount:=0;
  CharPtr:=Text;
  CharWidths[0]:=0.0;
  GetTextMetrics(FDC,TextMetrics);
  // calculate widths of the characters, must differentiate between true-type
  // and vector-fonts because with rotated fonts windows gives character-width
  // for true-type and frame-width for vector-fonts and raster-fonts
  if GetFontType(FFont.Name) and TRUETYPE_FONTTYPE<>0 then begin
    Cnt:=0;
    while Cnt<TextLength do begin
      // calculate next character-code, DBCS use AnsiNext
      if IsDBCS then begin
        NextPtr:=AnsiNext(CharPtr);
        if NextPtr-CharPtr>1 then CharCode:=Byte(CharPtr^) Shl 8+Byte((CharPtr+1)^)
        else CharCode:=Byte(CharPtr^);
      end
      else begin
        CharCode:=Byte(CharPtr^);
        NextPtr:=CharPtr+1;
      end;
      // use GetABCWidths to calculate real character widths
      GetCharABCWidths(FDC,CharCode,CharCode,ABCWidths);
      RealWidth:=RealWidth+ABCWidths.abcA+ABCWidths.abcB+ABCWidths.abcC;
      Inc(RealCharCount);
      CharWidths[RealCharCount]:=RealWidth;
      Inc(Cnt,NextPtr-CharPtr);
      CharPtr:=NextPtr;
    end;
    // the character-overhang was added for each character, ist must be
    // subtracted except for the last character
    RealWidth:=RealWidth-(RealCharCount-1)*TextMetrics.tmOverhang;
  end
  else begin
    CosAlpha:=Abs(FFont.FCosAngle);
    SinAlpha:=Abs(FFont.FSinAngle);
    Cos2Alpha:=Abs(Cos(2*FFont.Angle));
    Cnt:=0;
    while Cnt<TextLength do begin
      // calculate next character-code, if DBCS, use AnsiNext
      if IsDBCS then NextPtr:=AnsiNext(CharPtr)
      else NextPtr:=CharPtr+1;
      // use GetCharWidth and calculate char-width out of cell-width
      GetTextExtentPoint32(FDC,CharPtr,NextPtr-CharPtr,Size);
      // handle 45� rotation-case, uses expected font-height instead of real font-height
      if Cos2Alpha<0.05 then RealWidth:=RealWidth+Abs(Size.cx/CosAlpha-FFont.FSize)
      else RealWidth:=RealWidth+Abs((Size.cx*CosAlpha-Size.cy*SinAlpha)/Cos2Alpha);
      Inc(RealCharCount);
      CharWidths[RealCharCount]:=RealWidth;
      Inc(Cnt,NextPtr-CharPtr);
      CharPtr:=NextPtr;
    end;
  end;
  // calculate text-position-offset depending on text-alignment
  if FFont.FAlignment=taRight then Offset:=-RealWidth
  else if FFont.FAlignment=taCentered then Offset:=-RealWidth/2.0
  else Offset:=0.0;
  // calculate lower-left point of text 
  Position.X:=Position.X+FFont.FCosAngle*Offset;
  Position.Y:=Position.Y+FFont.FSinAngle*Offset;
  // calculate border of text
  ClipPoints:=TGrPointList.Create;
  try
    Point:=Position;
    ClipPoints.Add(Point);
    Point:=GrPoint(Point.X+FFont.FCosAngle*RealWidth,Point.Y+FFont.FSinAngle*RealWidth);
    ClipPoints.Add(Point);
    Point:=GrPoint(Point.X+FFont.FSinAngle*FFont.FSize,Point.Y-FFont.FCosAngle*FFont.FSize);
    ClipPoints.Add(Point);
    Point:=GrPoint(Point.X-FFont.FCosAngle*RealWidth,Point.Y-FFont.FSinAngle*RealWidth);
    ClipPoints.Add(Point);
    // clip text-border against clipping-rectangle
    Clipping.ClipPolygon(ClipPoints,FClipRect);
    // calculate visible characters
    if ClipPoints.Count>0 then begin
      // start with full baseline
      MinPosition:=RealWidth;
      MaxPosition:=0;
      // project all border-points to the baseline and calculate max and min
      for Cnt:=0 to ClipPoints.Count-1 do begin
        Value:=((ClipPoints[Cnt].X-Position.X)*FFont.FCosAngle
            +(ClipPoints[Cnt].Y-Position.Y)*FFont.FSinAngle);
        if Value<MinPosition then MinPosition:=Value;
        if Value>MaxPosition then MaxPosition:=Value;
      end;
      // max and min now specify the visible part of the text - must be
      // aligned to character-positions
      StartChar:=0;
      LastChar:=RealCharCount;
      Value:=0;
      for Cnt:=0 to RealCharCount-1 do begin
        if CharWidths[Cnt]<MinPosition then begin
          StartChar:=Cnt;
          Value:=CharWidths[Cnt];
        end;
        if CharWidths[Cnt]>MaxPosition then begin
          LastChar:=Cnt;
          break;
        end;
      end;
      // determine character to start with (take dbcs into account)
      if IsDBCS then begin
        CharPtr:=Text;
        for Cnt:=1 to StartChar do CharPtr:=AnsiNext(CharPtr);
        NextPtr:=CharPtr;
        for Cnt:=1 to LastChar-StartChar do NextPtr:=AnsiNext(NextPtr);
      end
      else begin
        CharPtr:=@Text[StartChar];
        NextPtr:=@Text[LastChar];
      end;
      // finally draw the text
      Windows.TextOut(FDC,Round(Position.X+FFont.FCosAngle*Value),
          Round(Position.Y+FFont.FSinAngle*Value),CharPtr,NextPtr-CharPtr);
    end;
  finally
    // free memory of clipping-info
    ClipPoints.Free;
  end;
end;

{******************************************************************************+
  Function TExtCanvas.TextWidth
--------------------------------------------------------------------------------
  Calculates the width of the specified text. This function always returns
  the length of the base-line and not the width of the bounding-box like
  the windows-function does for rotated vector- or bitmap-fonts.
+******************************************************************************}
Function TExtCanvas.TextWidth(Text:PChar;TextLength:Integer):Integer;
var Size         : TSize;
    Cos2Alpha    : Double;
begin
  // ensure the font is selected for the device-context
  RequiredState([ecsFont]);
  // get windows-font-size-information
  GetTextExtentPoint32(FDC,Text,TextLength,Size);
  Result:=Size.cx;
  // if it's not a true-type font the bounds-rectangle is given instead of the width
  if GetFontType(FFont.Name) and TRUETYPE_FONTTYPE = 0 then begin
    Cos2Alpha:=Abs(Cos(2*FFont.Angle));
    // handle special case with 45� rotation, use height specified when
    // creating the font to calculate the width
    if Cos2Alpha<0.05 then Result:=Round(Abs(Abs(Result/FFont.FCosAngle)-FFont.Size))
    else Result:=Round((Abs(Result*FFont.FCosAngle)-Abs(Size.cy*FFont.FSinAngle))/Cos2Alpha);
  end;
end;

{******************************************************************************+
  Function TExtCanvas.TextHeight
--------------------------------------------------------------------------------
  Calculates the height of the specified text. This function always returns
  the height of the text and not the width of the bounding-box like
  the windows-function does for rotated vector- or bitmap-fonts.
+******************************************************************************}
Function TExtCanvas.TextHeight(Text:PChar;TextLength:Integer):Integer;
var Size         : TSize;
    Cos2Alpha    : Double;
begin
  // ensure the font is selected for the device-context
  RequiredState([ecsFont]);
  // get windows-font-size-information
  GetTextExtentPoint32(FDC,Text,TextLength,Size);
  Result:=Size.cy;
  // if it's not a true-type font the bounds-rectangle is given instead of height
  if GetFontType(FFont.Name) and TRUETYPE_FONTTYPE = 0 then begin
    Cos2Alpha:=Abs(Cos(2*FFont.Angle));
    // handle special case with 45� rotation, use height the font was created
    // with in this case
    if Cos2Alpha<0.05 then Result:=FFont.Size
    else Result:=Round((Abs(Result*FFont.FCosAngle)-Abs(Size.cx*FFont.FSinAngle))/Cos2Alpha);
  end;
end;

Function TExtCanvas.TextExtent
 (
 Text            : PChar;
 TextLength      : Integer
 )
 : TSize;
var Cos2Alpha    : Double;
begin
  // ensure the font is selected for the device-context
  RequiredState([ecsFont]);
  GetTextExtentPoint32(FDC,Text,TextLength,Result);
  // if it's not a true-type font the bounds-rectangle is given instead of height
  if GetFontType(FFont.Name) and TRUETYPE_FONTTYPE = 0 then begin
    Cos2Alpha:=Abs(Cos(2*FFont.Angle));
    // handle special case with 45� rotation, use height specified when
    // creating the font to calculate the width
    if Cos2Alpha<0.05 then Result.cx:=Round(Abs(Abs(Result.cx/FFont.FCosAngle)-FFont.Size))
    else Result.cx:=Round((Abs(Result.cx*FFont.FCosAngle)-Abs(Result.cy*FFont.FSinAngle))/Cos2Alpha);
    // handle special case with 45� rotation, use height the font was created
    // with in this case
    if Cos2Alpha<0.05 then Result.cy:=FFont.Size
    else Result.cy:=Round((Abs(Result.cy*FFont.FCosAngle)-Abs(Result.cx*FFont.FSinAngle))/Cos2Alpha);
  end;
end;

Function EnumFontFamProc(EnumLogFont:PEnumLogFont;TextMetrics:PNewTextMetric;
    FontType:Integer;Param:PInteger):Integer; stdcall;
begin
  Param^:=FontType;
  Result:=0;
end;

Function TExtCanvas.GetFontType(const FontName:AnsiString):Integer;
begin
  EnumFontFamilies(FDC,PChar(FontName),@EnumFontFamProc,Integer(@Result));
end;

Procedure TExtCanvas.MoveTo(XPos:Integer;YPos:Integer);
begin
  Windows.MoveToEx(FDC,XPos,YPos,NIL);
end;

Procedure TExtCanvas.LineTo(XPos:Integer;YPos:Integer);
var Points       : Array[0..1] of TPoint;
begin
  if FPen.Style>=ltUserDefined then begin
    GetCurrentPositionEx(FDC,@Points[0]);
    Points[1].X:=XPos;
    Points[1].Y:=YPos;
    Polyline(Points,2,FALSE);
  end
  else begin
    RequiredState([ecsPen]);
    Windows.LineTo(FDC,XPos,YPos);
    FPointsConverted:=FALSE;
  end;
end;
    
Procedure TExtCanvas.Rectangle(X1,Y1,X2,Y2:Integer);
var Points         : Array[0..3] of TPoint;
begin
  try
    if (FPen.FStyle>=ltUserDefined) or (FBrush.FStyle>=ptVectorXFill) then begin
      Points[0]:=Point(X1,Y1);
      Points[1]:=Point(X2,Y1);
      Points[2]:=Point(X2,Y2);
      Points[3]:=Point(X1,Y2);
      Polygon(Points,4,FALSE);
    end
    else begin
      RequiredState([ecsPen,ecsBrush]);
      Windows.Rectangle(FDC,X1,Y1,X2,Y2);
      FPointsConverted:=FALSE;
    end;
  except
  end;  
end;

Procedure TExtCanvas.Rectangle(const Rect:TRect);
var Points         : Array[0..3] of TPoint;
begin
  try
    with Rect do if (FPen.FStyle>=ltUserDefined) or (FBrush.FStyle>=ptVectorXFill) then begin
      Points[0]:=Point(Left,Bottom);
      Points[1]:=Point(Right,Bottom);
      Points[2]:=Point(Right,Top);
      Points[3]:=Point(Left,Top);
      Polygon(Points,4,FALSE);
    end
    else begin
      RequiredState([ecsPen,ecsBrush]);
      Windows.Rectangle(FDC,Left,Bottom,Right,Top);
      FPointsConverted:=FALSE;
    end;
  except
  end;
end;

Procedure TExtCanvas.Rectangle(const Rect:TGrRect);
var Points         : Array[0..3] of TPoint;
begin
  try
    with Rect do if (FPen.FStyle>=ltUserDefined) or (FBrush.FStyle>=ptVectorXFill) then begin
      Points[0]:=Point(Round(Left),Round(Bottom));
      Points[1]:=Point(Round(Right),Round(Bottom));
      Points[2]:=Point(Round(Right),Round(Top));
      Points[3]:=Point(Round(Left),Round(Top));
      Polygon(Points,4,FALSE);
    end
    else begin
      RequiredState([ecsPen,ecsBrush]);
      Windows.Rectangle(FDC,Round(Left),Round(Bottom),Round(Right),Round(Top));
      FPointsConverted:=FALSE;
    end;
  except
  end;
end;

Procedure TExtCanvas.Rectangle(const Rect:TRotRect);
var Points         : Array[0..3] of TPoint;
    CosAngle       : Double;
    SinAngle       : Double;
    CurX           : Double;
    CurY           : Double;
begin
  try
    SinCos(Rect.Rotation,SinAngle,CosAngle);
    CurX:=Rect.X;
    CurY:=Rect.Y;
    Points[0]:=Point(Round(CurX),Round(CurY));
    CurX:=CurX+Rect.Width*CosAngle;
    CurY:=CurY+Rect.Width*SinAngle;
    Points[1]:=Point(Round(CurX),Round(CurY));
    CurX:=CurX-Rect.Height*SinAngle;
    CurY:=CurY+Rect.Height*CosAngle;
    Points[2]:=Point(Round(CurX),Round(CurY));
    CurX:=CurX-Rect.Width*CosAngle;
    CurY:=CurY-Rect.Width*SinAngle;
    Points[3]:=Point(Round(CurX),Round(CurY));
    Polygon(Points,4,FALSE);
  except
  end;
end;

Procedure TExtCanvas.FrameRect(X1,Y1,X2,Y2:Integer);
var Points         : Array[0..4] of TPoint;
begin
  try
    if FPen.FStyle>=ltUserDefined then begin
      Points[0]:=Point(X1,Y1);
      Points[1]:=Point(X2,Y1);
      Points[2]:=Point(X2,Y2);
      Points[3]:=Point(X1,Y2);
      Points[4]:=Point(X1,Y1);
      Polyline(Points,5,FALSE);
    end
    else begin
      RequiredState([ecsPen]);
      Windows.MoveToEx(FDC,X1,Y1,NIL);
      Windows.LineTo(FDC,X1,Y2);
      Windows.LineTo(FDC,X2,Y2);
      Windows.LineTo(FDC,X2,Y1);
      Windows.LineTo(FDC,X1,Y1);
      FPointsConverted:=FALSE;
    end;
  except
  end;  
end;

Procedure TExtCanvas.FrameRect(const Rect:TRect);
var Points         : Array[0..4] of TPoint;
begin
  try
    with Rect do if FPen.FStyle>=ltUserDefined then begin
      Points[0]:=Point(Left,Bottom);
      Points[1]:=Point(Right,Bottom);
      Points[2]:=Point(Right,Top);
      Points[3]:=Point(Left,Top);
      Points[4]:=Point(Left,Bottom);
      Polyline(Points,5,FALSE);
    end
    else begin
      RequiredState([ecsPen]);
      Windows.MoveToEx(FDC,Left,Bottom,NIL);
      Windows.LineTo(FDC,Left,Top);
      Windows.LineTo(FDC,Right,Top);
      Windows.LineTo(FDC,Right,Bottom);
      Windows.LineTo(FDC,Left,Bottom);
      FPointsConverted:=FALSE;
    end;
  except
  end;
end;

Procedure TExtCanvas.FrameRect(const Rect:TGrRect);
var Points         : Array[0..4] of TPoint;
begin
  try
    with Rect do if FPen.FStyle>=ltUserDefined then begin
      Points[0]:=Point(Round(Left),Round(Bottom));
      Points[1]:=Point(Round(Right),Round(Bottom));
      Points[2]:=Point(Round(Right),Round(Top));
      Points[3]:=Point(Round(Left),Round(Top));
      Points[4]:=Point(Round(Left),Round(Bottom));
      Polyline(Points,5,FALSE);
    end
    else begin
      RequiredState([ecsPen,ecsBrush]);
      Windows.MoveToEx(FDC,Round(Left),Round(Bottom),NIL);
      Windows.LineTo(FDC,Round(Left),Round(Top));
      Windows.LineTo(FDC,Round(Right),Round(Top));
      Windows.LineTo(FDC,Round(Right),Round(Bottom));
      Windows.LineTo(FDC,Round(Left),Round(Bottom));
      FPointsConverted:=FALSE;
    end;
  except
  end;
end;

Procedure TExtCanvas.FrameRect(const Rect:TRotRect);
var Points         : Array[0..4] of TPoint;
    CosAngle       : Double;
    SinAngle       : Double;
    CurX           : Double;
    CurY           : Double;
begin
  try
    SinCos(Rect.Rotation,SinAngle,CosAngle);
    CurX:=Rect.X;
    CurY:=Rect.Y;
    Points[0]:=Point(Round(CurX),Round(CurY));
    CurX:=CurX+Rect.Width*CosAngle;
    CurY:=CurY+Rect.Width*SinAngle;
    Points[1]:=Point(Round(CurX),Round(CurY));
    CurX:=CurX-Rect.Height*SinAngle;
    CurY:=CurY+Rect.Height*CosAngle;
    Points[2]:=Point(Round(CurX),Round(CurY));
    CurX:=CurX-Rect.Width*CosAngle;
    CurY:=CurY-Rect.Width*SinAngle;
    Points[3]:=Point(Round(CurX),Round(CurY));
    Points[4]:=Points[1];
    Polyline(Points,4,FALSE);
  except
  end;
end;

{******************************************************************************+
  Procedure TExtCanvas.BezierCurve
--------------------------------------------------------------------------------
  Draws a curve consisting of cubic bezier-segments. The first curve is
  specified by the first and fourth-point using the second and third-point
  as control points. Each consecutive segment needs three more points, the
  first point of the segment is the fourth of the previous segment.

  !?!? This function currently does not support extended line-styles
+******************************************************************************}
Procedure TExtCanvas.BezierCurve(var Points;PointCount:Integer);
begin
  RequiredState([ecsPen,ecsBrush]);
  PolyBezier(FDC,Points,PointCount);
end;

{******************************************************************************+
  Procedure TExtCanvas.ClippedBezierCurve
--------------------------------------------------------------------------------
  !?!? This function currently does not support clipping and extended line-styles
+******************************************************************************}
Procedure TExtCanvas.ClippedBezierCurve(var Points:TGrPointList);
var Cnt            : Integer;
    WinPoints      : PPointArray;
begin
  GetMem(WinPoints,Points.Count*SizeOf(TPoint));
  try
    for Cnt:=0 to Points.Count-1 do begin
      WinPoints^[Cnt].X:=Round(Points[Cnt].X);
      WinPoints^[Cnt].Y:=Round(Points[Cnt].Y);
    end;
    BezierCurve(WinPoints^,Points.Count);
  finally
    FreeMem(WinPoints,Points.Count*SizeOf(TPoint));
  end;
end;

{******************************************************************************+
  Procedure TExtCanvas.DeselectAllTools
--------------------------------------------------------------------------------
  Deselects the pen, brush and font from the current drawing-context by
  selection stock-objects.
+******************************************************************************}
Procedure TExtCanvas.DeselectAllTools;
begin
  if FDC<>0 then begin
    SelectObject(FDC,GetStockObject(BLACK_PEN));
    SelectObject(FDC,GetStockObject(HOLLOW_BRUSH));
    SelectObject(FDC,GetStockObject(SYSTEM_FONT));
  end;
end;

{******************************************************************************+
  Procedure TExtCanvas.InvalidateAllTools
--------------------------------------------------------------------------------
  Invalidates pen, brush and font by setting the valid-flags to false.
  Call this function to force reselection of tools the next time they are used.
+******************************************************************************}
Procedure TExtCanvas.InvalidateAllTools;
begin
  FBrush.FHandleValid:=FALSE;
  FBrush.FValid:=FALSE;
  FFont.FHandleValid:=FALSE;
  FFont.FValid:=FALSE;
  FPen.FHandleValid:=FALSE;
  FPen.FValid:=FALSE;
end;

{******************************************************************************+
  Procedure TExtCanvas.PushClipping
--------------------------------------------------------------------------------
  Stores the current clipping-area and pushes it on the stack. 
+******************************************************************************}
Procedure TExtCanvas.PushClipping;
var Rgn            : HRgn;
    Result         : Integer;
begin
  // create a temporary region
  Rgn:=CreateRectRgn(0,0,1,1);
  // get the current clipping-region
  Result:=GetClipRgn(FDC,Rgn);
  // if dc has no clipping-region or an error occurred delete the temporary region
  if Result<>1 then begin
    if Rgn<>0 then DeleteObject(Rgn);
    Rgn:=Result;
  end;
  // push the region on the stack
  FClipStack.Add(Rgn);
end;

{******************************************************************************+
  Procedure TExtCanvas.PopClipping
--------------------------------------------------------------------------------
  Restores the previous clipping-area and removes it from the stack.
+******************************************************************************}
Procedure TExtCanvas.PopClipping;
var Region         : Integer;
begin
  if FClipStack.Count>0 then begin
    // get and remove the last item on the stack
    Region:=FClipStack[FClipStack.Count-1];
    FClipStack.Delete(FClipStack.Count-1);
    // if valid region or null-region->select it for the device-context
    if Region>0 then begin
      SelectClipRgn(FDC,Region);
      DeleteObject(Region);
    end
    else SelectClipRgn(FDC,0);
  end;
end;

{******************************************************************************+
  Procedure TExtCanvas.SetClippingPolygon
--------------------------------------------------------------------------------
  Selects the specified polygon as the clipping-region of the current device-
  context. The clipping-region is AND-combined with the current clipping-area.
  The coordinates mut be given in logical units.
+******************************************************************************}
Procedure TExtCanvas.SetClippingPolygon(var Points;PointCount:Integer);
var Rgn            : HRgn;
begin
  // convert points to device-coordinates
  LPToDP(FDC,Points,PointCount);
  try
    // create polygon-region
    Rgn:=CreatePolygonRgn(Points,PointCount,ALTERNATE);
    if Rgn<>0 then begin
      // select the region as the clipping-region
      ExtSelectClipRgn(FDC,Rgn,RGN_AND);
      // delete the temporary region
      DeleteObject(Rgn);
    end;
  finally
    // convert coordinates back to logical-coordinates
    DPToLP(FDC,Points,PointCount);
  end;
end;

{******************************************************************************+
  Procedure TExtCanvas.SetClippingCircle
--------------------------------------------------------------------------------
  Selects the specified circle as the clipping-region of the current device-
  context. The clipping-region is AND-combined with the current clipping-area.
  The coordinates mut be given in logical units.
+******************************************************************************}
Procedure TExtCanvas.SetClippingCircle(Const Center:TPoint;Radius:Integer);
var Points         : Array[0..1] of TPoint;
    Rgn            : HRGN;
begin
  Points[0]:=Center;
  Points[1].X:=Radius;
  Points[1].Y:=Radius;
  // convert points to device-coordinates
  LPToDP(FDC,Points,2);
  Rgn:=CreateEllipticRgn(Points[0].X-Points[1].X,Points[0].Y-Points[1].Y,
      Points[0].X+Points[1].X,Points[0].Y+Points[1].Y);
  if Rgn<>0 then begin
    // select the region as the clipping-region
    ExtSelectClipRgn(FDC,Rgn,RGN_AND);
    // delete the temporary region
    DeleteObject(Rgn);
  end;
end;

{******************************************************************************+
  Procedure TExtCanvas.SetClippingRect
--------------------------------------------------------------------------------
  Selects the specified rectangle as the clipping-region of the current device-
  context. The clipping-region is AND-combined with the current clipping-area.
  The coordinates mut be given in logical units.
+******************************************************************************}
Procedure TExtCanvas.SetClippingRect(ARect:TRect);
begin
  LPToDP(FDC,ARect,2);
  Inc(ARect.Right); Inc(ARect.Bottom);
  DPToLP(FDC,ARect,2);
  IntersectClipRect(FDC,ARect.Left,ARect.Top,ARect.Right,ARect.Bottom);
end;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}
Procedure TExtCanvas.GetBitmapPartCopy(ABitmap: TBitmap; ARect: TRect);
begin
   StretchBlt(ABitmap.Canvas.Handle,0,0,
              ABitmap.Width,ABitmap.Height,
              FDC,ARect.Left,ARect.Bottom,
              ARect.Right-ARect.Left,ARect.Top-ARect.Bottom,
              SRCCOPY);
end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

{==============================================================================+
  TRefreshThread
+==============================================================================}

Constructor TRefreshThread.Create(ExtCanvas:TExtCanvas);
begin
  inherited Create(Suspended);
  //Priority:=tpHigher;
  FExtCanvas:=ExtCanvas;
  FWaitEvents[0]:=CreateEvent(NIL,FALSE,FALSE,NIL);
  FWaitEvents[1]:=CreateEvent(NIL,FALSE,FALSE,NIL);
  FWaitEvents[2]:=Handle;
end;

Destructor TRefreshThread.Destroy;
begin
  // ensure terminated is set
  Terminate;
  // issue a stop, because terminated is set execute will terminate
  Stop;
  // free the events
  CloseHandle(FWaitEvents[0]);
  CloseHandle(FWaitEvents[1]);
  inherited Destroy;
end;

Procedure TRefreshThread.Execute;
var Event          : Integer;
    Msg            : TMsg;
    Timer          : Integer;
begin
  while not Terminated do begin
    Event:=WaitForMultipleObjects(3,@FWaitEvents[0],FALSE,INFINITE);
    if RunRemote then Exit;
    if Terminated then Exit;
    if Event-WAIT_OBJECT_0>=1 then Continue;
    Timer:=SetTimer(0,0,RefreshInterval,NIL);
    while not Terminated do begin
      Event:=MsgWaitForMultipleObjects(2,FWaitEvents[1],FALSE,INFINITE,QS_TIMER);
      KillTimer(0,Timer);
      if Terminated then Exit;
      if Event-WAIT_OBJECT_0=2 then begin
        while PeekMessage(Msg,0,0,0,pm_Remove) do;
        FExtCanvas.DrawFromCache(FExtCanvas.LogicalSize);
        Timer:=SetTimer(0,0,RefreshInterval,NIL);
      end
      else break;
    end;
  end;
end;

Procedure TRefreshThread.Start;
begin
  SetEvent(FWaitEvents[0]);
end;

Procedure TRefreshThread.Stop;
begin
  SetEvent(FWaitEvents[1]);
end;

{==============================================================================+
  TExtFont
+==============================================================================}

{++ Moskaliov BUG#268 BUILD#108 29.03.00}
Constructor TExtFont.Create;
begin
  inherited Create;
  FAngle:=0;
  SinCos(FAngle,FSinAngle,FCosAngle);
end;
{-- Moskaliov BUG#268 BUILD#108 29.03.00}

Procedure TExtFont.SetAngle(const AAngle:Double);
begin
  if AAngle<>FAngle then begin
    FAngle:=AAngle;
    SinCos(FAngle,FSinAngle,FCosAngle);
    FHandleValid:=FALSE;
  end;
end;

Procedure TExtFont.SetColor(AColor:TColor);
begin
  if AColor<>FColor then begin
    FColor:=AColor;
    FValid:=FALSE;
  end;
end;

Procedure TExtFont.SetSize(ASize:Integer);
begin
  if ASize<>FSize then begin
    FSize:=ASize;
    FHandleValid:=FALSE;
  end;
end;

Procedure TExtFont.SetName(const AName:AnsiString);
begin
  if CompareText(AName,FName)<>0 then begin
     FName:=AName;
     FHandleValid:=FALSE;
   end;
end;

Function TExtFont.GetExtFontStyle:TExtFontStyle;
begin
  Result:=PExtFontStyle(@FAngle)^;
end;

Procedure TExtFont.SetExtFontStyle(const AStyle:TExtFontStyle);
begin
  PExtFontStyle(@FAngle)^:=AStyle;
  SinCos(FAngle,FSinAngle,FCosAngle);
  FHandleValid:=FALSE;
  FValid:=FALSE;
end;

Procedure TExtFont.SetStyle(AStyle:Integer);
begin
  if AStyle<>FStyle then begin
    FStyle:=AStyle;
    FHandleValid:=FALSE;
  end;
end;

procedure TExtCanvas.ScreenShot(AName: String);
var ABitmap : TBitmap;
begin
  ABitmap:=TBItmap.Create;
  ABitmap.Handle:=FBitmap;
  ABitmap.SaveToFile(AName);
  ABitmap.Free;
end;

procedure TExtCanvas.Invalidate(Rect: TRect);
var Rgn            : THandle;
begin
  Rgn:=CreateRectRgnIndirect(Rect);
  CombineRgn(FInvalidRegions,FInvalidRegions,Rgn,RGN_OR);
  DeleteObject(Rgn);
end;

function TExtCanvas.IsInvalidated(Rect: TRect): Boolean;
begin
  LPToDP(FDC,Rect,2);
  Result:=RectInRegion(FInvalidRegions,Rect);
end;

procedure TExtCanvas.Validate(Rect: TRect);
var Rgn            : THandle;
begin
  LPToDP(FDC,Rect,2);
  Rgn:=CreateRectRgnIndirect(Rect);
  CombineRgn(FInvalidRegions,FInvalidRegions,Rgn,RGN_DIFF);
  DeleteObject(Rgn);
end;

end.
