{****************************************************************************}
{ Unit OmegaPhoto                                                            }
{----------------------------------------------------------------------------}
{ Contains user interface routines and entries to handle with TOmegaPhotoDLL }
{ unit.                                                                      }
{----------------------------------------------------------------------------}
{ Date of creation:05-MAR-1999.                                              }
{ Full adaptation:                                                           }
{----------------------------------------------------------------------------}
{ Author: Sigolaeff Victor, PROGIS-Moscow, Russia, mailto: sygsky@progis.ru}
{----------------------------------------------------------------------------}
{ Changes:
{----------------------------------------------------------------------------}
{****************************************************************************}
unit OmegaHndl;

interface

uses
  Windows, SysUtils, Forms, Dialogs, Messages,
  ProjHndl, UserIntf, MenuFn, MenuHndl, AM_Def, AM_Proj,
  Classes, Graphics, Controls, StdCtrls, MultiLng,
  TOmegaPhoto, TrLib32, AM_Index, Buttons, ExtCtrls, AM_Layer, TabFiles;

type
  TInfoForm = class(TForm)
    MlgSection1: TMlgSection;
    TransformTypeComboBox: TComboBox;
    OkBitBtn: TBitBtn;
    Panel1: TPanel;
    PointInfoGroupBox: TGroupBox;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    ProjXLab: TLabel;
    ProjYLab: TLabel;
    ProjStatLab: TLabel;
    ImgXLab: TLabel;
    ImgYLab: TLabel;
    ImgStatLab: TLabel;
    PointsInfoComboBox: TComboBox;
    NumberLabel: TLabel;
    PointNumLabel: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure PointsInfoComboBoxChange(Sender: TObject);
{    procedure FormActivate(Sender: TObject);}
  private
    { Private declarations }
  public
    { Public declarations }
  end;

{$ifdef FUTURE}

type
  To_Omega = record
    Msg: TOmegaPhotoNotifyMsg;
    case TOmegaPhotoNotifyMsg of :
      omInit: ();
      omShow: ();
      omHide: ();
      omSrcRasterName: (FileName: string);
      omDstRasterName: (FileName: string);
      omNewControlPoint: ();
      omDeleteControlPoint: ();
      omChangeControlPoint: ();
      omSelectControlPoint: ();
      omTransPars: ();
      omTransDone: ();
  end;
{$endif}

const { For hint in info combobox }
  InfoHintStr = 'P. #:(Project x , Project Y, status ),( Image   X, Image   Y, status )';
  OmegaTabExtension = '.omg'; // extension for TABs created for Omega purposes

type
  TOmegaShow = class(TProjMenuHandler)
    procedure OnStart; override;
  end;
type
  TOmegaAdd = class(TProjMenuHandler)
    procedure OnStart; override;
  private
{    procedure OnStart;}
  end;

{$ifdef FULL_OMEGA}
type
  TOmegaDel = class(TProjMenuHandler)
    procedure OnStart; override;
  end;
type
  TOmegaSel = class(TProjMenuHandler)
    procedure OnStart; override;
  end;
type
  TOmegaNext = class(TProjMenuHandler)
    procedure OnStart; override;
  end;
type
  TOmegaPrev = class(TProjMenuHandler)
    procedure OnStart; override;
  end;
{$endif}

type
  TOmegaMove = class(TProjMenuHandler)
    procedure OnStart; override;
  end;
type
  TOmegaInfo = class(TProjMenuHandler)
    procedure OnStart; override;
  end;
type
  TOmegaCancel = class(TProjMenuHandler)
    procedure OnStart; override;
  end;
type
  TOmegaExec = class(TProjMenuHandler)
    procedure OnStart; override;
  end;

function StartOmega(AWindow: Pointer; SrcFile: AnsiString): Boolean;
function OmegaCount: Integer;
function OmegaInUse: Boolean;
procedure ShowOmega;
procedure StopOmega(Cancel: Boolean);
function AddPoint2Omega(AIndex: PIndex): Integer;
function DeleteFromOmega(PointIndex: Integer): Boolean;
function CheckUpdateDelayed: Integer;
function OmegaParam: Integer;
function ChangeToOmega(ProgisId: Integer; XMove, YMove: Integer): Boolean;
function InAddFromOmega: Boolean;
procedure DeSelectAllSelectOne(PointIndex: Integer);

function GetOmegaRefLayername: AnsiString; // To know the name externally
function GetOmegaRefLayerId: PLayer; // The same for ID

//procedure ChangePoint2WinGIS(Ind:Integer; PPoint:PRelPoint);

implementation

uses ImgDlg, AM_Child, AM_Admin, {AM_Layer,} AM_Point, OmegaLayer, AM_View,
  AM_ProjP, ListHndl, UserUtils, BMPImage,
  OmegaLog, TextUtils, WinDOS;

{$R *.DFM}
{$R *.MFN}

const
  OmegaTabExt = '.omg';
const
  Section: AnsiString = 'OmegaHndl';
var
  InfoForm: TInfoForm;
  OmegaLogFile : TOmegaLog = nil;
  OrigImgInfo: PBitMapInfo;



{-------------------------------------------------}
  Omega: TOmegaPhotoDLL = nil;
  OmegaProj: PProj = nil; // Our Omega object
  OmegaChild: TMDIChild = nil; // Our Window we are called from
  OmegaProjSym: Integer; // Old state of Symbol (just in case)
  OmegaOrigFile: AnsiString; // Original file name
  OmegaResFile: AnsiString; // Result file name to insert into project
  OmegaSrcFile: AnsiString = ''; // Omega src BMP file name
  OmegaDstFile: AnsiString = ''; // Omega dst BMP file name
  OldCaption: AnsiString = ''; // Original caption for parent
  RefLayer: POmegaLayer = nil; // New layer
  RefLayerID: PLayer;
  RefLayerName: AnsiString = ''; // New layer name
  InsertLayerID: PLayer;
  AddingFromOmega: Boolean = False;
  OldModified: Boolean;
  OmegaCorners: TPoint2Array { = ((0,0),(0,0))};
  OmegaTab: TTabFile = nil;
  OmegaOrigHeight: Integer;

procedure EndSession(Str:AnsiString = '');
begin
  if Str <> '' then
     OmegaLogFile.WriteLog(Str);
  OmegaLogFile.WriteLog('*** Omega session ended');
end;

procedure InfoMess(Txt: AnsiString);
begin
  MessageDlg(CStyleFormat(Txt), mtInformation, [mbOK], 0);
end;

procedure OmegaMsg(MsgId: Integer);
const
  Caption: Integer = 45;
begin
  SmartMsgBox(OmegaChild.Handle, Section, MsgId, Caption, MB_OK + MB_ICONEXCLAMATION, []);
end;

{------------------------------------------------------------------------------}

procedure TInfoForm.FormCreate(Sender: TObject);
var
  I: Integer;
begin
  TransformTypeComboBox.ItemIndex := OmegaParam; { values from [0..4] }
  { Fill combobox with Omega points indexes }
  with PointsInfoComboBox do
  begin { Fill list with point info }
    if Omega.Count > 0 then
    begin
      for I := 1 to Omega.Count do { for all the items }
        Items.Add(IntToStr(I));
      ItemIndex := Omega.SelectedIndex;
    end
    else
      Enabled := False; { Disable it as no point to select at all }
    with PointNumLabel do
      Caption := Format(Caption, [Omega.Count]);
  end;
  PointsInfoComboBoxChange(Sender);
end;
{------------------------------------------------------------------------------}

function OmegaParam: Integer;
begin
  Result := 0;
  if Omega <> nil then
    Result := Omega.TransformType;
end;
{------------------------------------------------------------------------------}
{
  This function is called only when Omega is upper window in system!
  Returns new point index!
}

function AddPoint2WinGIS(RelPoint: PRelPoint; OmegaIndex: Integer): Integer;
var
  NewPoint: POmegaPixel;
  NewPos: TDPoint;
begin
  AddingFromOmega := True;
  Result := 1;
{
  Translate coordinates from Omega representation to WinGIS pne
  and syncronize some data in structure TRelPoint!
}
  with RelPoint^ do
  begin
    case ProjectStatus of
      rsPreSet, rsSet:
        begin
        end
    else
      with Project do
      begin
        X := 0; Y := 0;
      end;
    end;
{ Generate new point object }
    NewPos.Init(Round(Project.X), Round(Project.Y));
  end;
  NewPoint := New(POmegaPixel, Init(NewPos));
  OmegaProj.Layers.SelLayer.DeSelectAll(OmegaProj.PInfo); // Deselect all
  OmegaProj.InsertObject(NewPoint, True); // Insert and select it!
  AddingFromOmega := False;
end;
{----------------------------------------------------}
{ Call it only from TOmegaPhoto.pas pleassssssssssse }

procedure ChangePoint2WinGIS(Ind: Integer; PPoint: PRelPoint);
var
  Point: TDPoint;
begin
  Point.Init(Round(PPoint.Project.X), Round(PPoint.Project.Y));
  try
    RefLayer.UpdateItem(Ind, Point, OmegaProj.PInfo);
  finally
    Point.Done;
  end;
end;

{----------------------------------------------------}

function DeleteFromOmega(PointIndex: Integer): Boolean;
begin
  Result := False;
  if Omega = nil then Exit;
  if not Omega.InCallBack then
    Result := Omega.DeleteControlPoint(PointIndex) <> -1
end;
{----------------------------------------------------}

function ChangeToOmega(ProgisId: Integer; XMove, YMove: Integer): Boolean;
var
  I: Integer;
begin
  if not Omega.InCallBack then
  begin
  { Read index in order of order of  points creation }
    I := RefLayer.ObjIndexByProgisId[ProgisId];
    if I <> -1 then
    begin
      Result := Omega.ChangeControlPoint(I, XMove, YMove) <> 0;
      Exit;
    end;
  end;
  Result := False;
end;
{------------------------------------}

procedure DeSelectAllSelectOne(PointIndex: Integer);
var
  AView: PView;
begin
  if Omega = nil then Exit;
  if InRange(0, PointIndex, Omega.Count - 1, True) then
  begin
    OmegaProj.DeSelectAll(True); { Deselect all objs from selection  in any case }
    AView := RefLayer.GetObjFromList(OmegaProj.PInfo, PointIndex);
    if AView <> nil then
      RefLayer.Select(OmegaProj.PInfo, AView);
  end;
end;
{------------------------------------}

function CheckUpdateDelayed: Integer;
var
  PAddr: PRelPoint;
  I: Integer;
begin
  Result := 0;
  if Omega = nil then
    Exit;
  for I := 0 to Omega.Count - 1 do
  begin
    PAddr := Omega.PointAddress[I];
    if PAddr.UpdateDelayed then
    begin
      ChangePoint2WinGIS(I, PAddr);
      PAddr.UpdateDelayed := False;
      Inc(Result);
    end;
  end;
end;
{------------------------------------------------------------------------------}

function MyOmegaCallBack(Msg, Param1, Param2: Integer): Integer; pascal;
  procedure InfoOnOmegaMsg(MsgId: Integer);
  begin
{    ShowMessage('Message received:'#10#13 +
      Omega.MsgName(TOmegaPhotoNotifyMsg(MsgId)));}
  end;
begin
  Result := 0; // False as a default return
  if Omega = nil then
    Exit;
  with Omega do
    case TOmegaPhotoNotifyMsg(Msg) of
{------------------------------------- User close Omega by Alt-F4 }
      omHide:
        begin
          StopOmega(True); // Cancel me
          Result := 1;
        end;
{------------------------------------- Set WinGIS foreground ------------------}
      omShow:
        begin
          with Application do
            if MainForm <> nil then
              MainForm.BringToFront;
          Result := 1;
        end;
{---------------------------------- Add more reference point (already in list!)}
      omNewControlPoint:
        begin
          Result := AddPoint2WinGIS(PRelPoint(Param2), Param1);
        end;
{------------------------------------- Delete point ---------------------------}
      omDeleteControlPoint:
        begin
          if RefLayer.DeleteObj(OmegaProj.PInfo, Param1 - 1) then
            Result := 1;
        end;
{------------------------------------- Change point coordinate/set condition --}
      omChangeControlPoint:
        begin
//          ChangePoint2WinGIS(Param1 - 1, PRelPoint(Param2));
          ShowOmega;
          Result := 1;
        end;
{------------------------------------- Select control point -------------------}
      omSelectControlPoint:
        begin
          DeselectAllSelectOne(Param1 - 1);
          Result := 1;
          ShowOmega; { Set Omega window to the foreground }
          Exit;
        end;
      omTransPars: Result := 1;
      omTransDone:
        begin
          if Param1 = 0 then
            FillChar(OmegaCorners, 0, SizeOf(OmegaCorners))
          else
          begin
            MoveMemory(@OmegaCorners, Pointer(Param1), SizeOf(OmegaCorners));
            if MessageDlg('Transformation was completed SUCCESFULLY!'#10#13 +
              'Do you want to insert the picture to WinGIS?',
              mtConfirmation, [mbYes, mbNo], 0) = mrYes then
              StopOmega(False);
          end;
          Result := 1;
        end;
    else
      InfoOnOmegaMsg(Msg);
    end;
end;

procedure NotImplemented;
begin
  InfoMess('This option still not implemented'#10#13 +
    'Mailto:wingis@progis.com');
end;

function OmegaCount: Integer;
begin
  Result := 0;
  if Omega = nil then
    Exit
  else
    if RefLayer <> nil then
      Result := RefLayer.ObjCounter;
end;

function OmegaInUse: Boolean;
begin
{  Result := Omega <> nil;} result := TOmegaPhoto.AlreadyInUse;
end;

procedure StartSaveProgress(SrcFile: AnsiString);
begin
  Windows.SetCursor(DWORD(crHourGlass));
end;

procedure StopProgress;
begin
  SetCursor(DWORD(crDefault));
end;

function GetOmegaRefLayerName: AnsiString;
begin
  Result := RefLayerName;
end;

function GetOmegaRefLayerId: PLayer;
begin
  Result := RefLayerId;
end;

procedure ExitUnit; far;
begin
  OmegaLogFile.Logging := FALSE;
  OmegaLogFile.Free;
end;

procedure InitUnit;
var
  ProgName          : array[0..255] of Char;
  Path              : array[0..255] of Char;
  AName             : array[0..255] of Char;
  Ext               : array[0..255] of Char;
begin
  GetModuleFileName(HInstance, ProgName, SizeOf(ProgName));
  FileSplit(ProgName, Path, AName, Ext);
  StrCat(Path, 'OmegaPhoto.log');
  OmegaLogFile.FileName := StrPas(Path);
end;

procedure InitLog;
begin
  if OmegaLogFile = nil then
  begin
    OmegaLogFile := TOmegaLog.Create;
    InitUnit;
    OmegaLogFile.Logging := True; // Now by default, later read from ini
    AddExitProc(ExitUnit);
  end;
end;

function StartOmega(AWindow: Pointer; SrcFile: AnsiString): Boolean;
const
  Prefix = 'OMG'; // You know what it is stand for
  Ext = BMPExt; // The only extension for OmegaPhoto
var
  DelExt: AnsiString;
  BitsCount: WORD;
  Compression: DWORD;
  i, Idx, ImgWidth, ImgHeight: Integer;
  NewPoint1: POmegaPixel;
  Point1: TDPoint;
  OmegaPoint: PRelPoint;
  Src: THandle;
  BMPInfo: PBitMapInfo;
  Res: Boolean;

begin
  {$IFNDEF WMLT}

  if IsVMSIImage(SrcFile) then begin
     Result:=False;
     exit;
  end;

  InitLog;
  OmegaLogFile.WriteLog('*** Omega session started');
  OmegaLogFile.WriteLogFmt('File name:"%s"',[SrcFile]);
  OmegaChild := TMDIChild(AWindow); { Save window where image will be loaded }
  Result := False; // Default return
  OmegaSrcFile := ''; // Mark as not ready
  if UpperCase(ExtractFileExt(SrcFile)) = Ext then // Omega can handle file directly
    if FileIsBMP(SrcFile, @BitsCount, @Compression) then
      if BitsCount <= 24 then
        if Compression = BI_RGB then
          OmegaSrcFile := SrcFile;
  if OmegaSrcFile = '' then { It was not a good old BMP, let's recreate it }
  begin
    OmegaSrcFile := GenerateUniqueName('', Prefix, Ext); // Use temp BMP image
    StartSaveProgress(SrcFile);
    try
      DelExt := TrLib.ImageGetExpFormats;
      if DelExt = EmptyStr then
      begin
        { No export DELs}
        SmartMsgBox(TMDIChild(AWindow).Handle, 'Main', 4652, 4649, MB_OK + MB_ICONEXCLAMATION, []);
        EndSession('ERR:No corresponding *.DEL found');
        Exit;
      end;
      if not TrLib32.IsExtInFilter(DelExt, Ext) then
      begin
        { BMP handler is not available }
        SmartMsgBox(TMDIChild(AWindow).Handle, 'OmegaHndl', 57, 45, MB_OK + MB_ICONEXCLAMATION, [Ext]);
        EndSession('ERR:BMP driver not found');
        Exit;
      end;
      OmegaLogFile.WriteLogFmt('Converting to "%s"...',[OmegaSrcFile]);
      if not TrLib32.SaveImageAs(SrcFile, OmegaSrcFile) then
      begin
        SmartMsgBox(TMDIChild(AWindow).Handle, 'OmegaHndl', 56, 45, MB_OK + MB_ICONEXCLAMATION, [SrcFile, OmegaSrcFile]);
//      'ImgMan32.dll can''t save file\n''%s''\nas ''%s''\nSorry...'
        EndSession('ERR:Can''t convert to BMP!');
        Exit;
      end;
      OmegaLogFile.WriteLog('Converted succesfully');
    finally
      StopProgress;
    end;
  end;
  if Omega = nil then { Omega stil not loaded, load it NOW! }
  begin
  { Create Omega object - it should be unical one ONLY! }
    Omega := TOmegaPhotoDLL.Create;
    if Omega = nil then
    begin
      EndSession('INTERNAL ERR:Can''t create TOmegaPhoto!');
      Exit;
    end;
  end
  else
    if TOmegaPhoto.AlreadyInUse then { Already in use! Bye-bye baby[ies]... }
    begin
      OmegaMsg(60);
      EndSession('INTERNAL ERR:TOmegaPhoto already in use!');
      Exit;
    end;
  OmegaDstFile := GenerateUniqueName('', Prefix, Ext); { Destination temp file name }
  with Omega do
  try
    OmegaProj := OmegaChild.Data;
    InsertLayerID := OmegaProj.Layers.TopLayer;
    OldModified := OmegaProj.Modified;
    OmegaProjSym := OmegaProj.SymbolMode; { Store old mode }
    OmegaProj.SymbolMode := sym_Omega; { Set new one }
    if Init = 0 then
    begin { Init Omega }
      OmegaMsg(58);
      StopOmega(True);
      EndSession('INTERNAL ERR:Can''t init Omega!');
      Exit;
    end;
    Show;
    SetCallBack(MyOmegaCallBack); { Set my Callback function }
    OmegaLogFile.WriteLog('Set source image to Omega');
    if SetSrcImage(OmegaSrcFile) = 0 then
    begin // Set src BMP file
      OmegaMsg(59);
      StopOmega(True);
      EndSession('ERR:Can''t set source image!');
      Exit;
    end;
    OmegaLogFile.WriteLog('Set succesfully');
    { Now it is a time to handle WinGIS menues/toolbars etc }
    SetDstImage(OmegaDstFile); // Set destination file name for Omega
    OmegaLogFile.WriteLogFmt('Set destination image as "%s"',[OmegaDstFile]);
    if TransformType <= 0 then
      TransformType := 1 { Set Affine as a default one }
    else
      TransformType := TransformType; { Who can answer why I did it is a true programmer :-}
{------------------------- Create all we need to handle reference points-------}
    with OmegaProj^ do
    begin
      OldCaption := Parent.Caption;
      Parent.Caption := Parent.Caption + ' ... ' + MlgStringList['OmegaHndl', 12];
      RefLayerName := MlgStringList['OmegaHndl', 11];
      if RefLayerName = '' then
        RefLayerName := 'Omega'' s reference layer(by Sygsky)'; // To be safety :))
    {----------- Create new layer and set it to top ----------------------}
      with Layers^ do
      begin
        DeselectAllLayers(PInfo); // Deselect all the selected objects
        {$IFNDEF AXDLL}
        RefLayer := Layers.InsertOmegaLayer(RefLayerName, RGBColors[c_Green], // GREEEEEEEEEN
        {$ENDIF}
          RGBColors[c_Green], RGBColors[c_Green],
          RGBColors[c_Green], ilTop,
          DefaultSymbolFill); { Add work layer }
        RefLayerId := Layers.NameToLayer(RefLayerName);
        Layers.SetTopLayer(RefLayer);
        FillChar(OmegaCorners, SizeOf(OmegaCorners), 0);
      end;
    end;

    OmegaOrigFile := SrcFile;
    OmegaResFile := '';
    UserInterface.Update([uiContextToolBars]); { Pleassse USE it in ANY cassse}

//++++++++++++++++++++++++++++++++++++++++++++++++++ Check TAB file to exist
    if OmegaTab = nil then
      OmegaTab := TTabFile.Create(ChangeFileExt(SrcFile, OmegaTabExtension))
    else
      OmegaTab.LoadTAB(ChangeFileExt(SrcFile, OmegaTabExtension));
    with OmegaProj^ do
    begin
      OmegaOrigHeight := 0;
      BMPInfo := TrLib.GetHeader(PChar(OmegaOrigFile), i, Res, 1);
      if BMPInfo <> nil then // Image is real one
      try
        OmegaOrigHeight := BMPInfo.bmiHeader.biHeight;
        if OmegaTab.IsValid  and OmegaTab.NameCoincides(SrcFile)then // TAB exists!
        begin
          OmegaLogFile.WriteLogFmt('Reference tab file %s found. Loading...',[OmegaTab.FileName]);
          ImgWidth := BMPInfo.bmiHeader.biWidth;
          OmegaOrigHeight := BMPInfo.bmiHeader.biHeight;
          DeSelectAll(False);
          OmegaLogFile.WriteLogFmt('%u ref. point[s] loaded', [OmegaTab.Cnt]);
          for i := 0 to OmegaTab.Cnt - 1 do
          begin
            Point1.X := Round(OmegaTab.ProjPoint[i].X);
            Point1.Y := Round(OmegaTab.ProjPoint[i].Y);
            OmegaLogFile.WriteLogFmt('Point #%3u: X=%10d, Y=%10d', [i+1, Point1.X, Point1.Y]);
            NewPoint1 := New(POmegaPixel, Init(Point1));
            if InsertObject(NewPoint1, FALSE) then // Insert into layer only!
            begin
              Idx := Omega.Count - 1;
              OmegaPoint := PointAddress[Idx]; // Last point used
              with OmegaPoint.Image do
              begin
                X := OmegaTab.ImagePoint[Idx].X;
                  // me-map coordinate system as for Omega Y goes from bottom to top
                Y := OmegaOrigHeight - 1 - OmegaTab.ImagePoint[Idx].Y;
              end;
              Omega.UpdatePoint(Idx);
            end;
          end;
        end;
      finally
        VirtualFree(BMPInfo, 0, MEM_RELEASE);
      end;
    end;
//---------------------------------------------- End of TAB file checking
  except
    Result := False;
    Exit;
  end;
  Result := True;
  {$ENDIF}
end;

var
  StopDelayed: Boolean = False;

{-----------------------------------}

procedure StopOmega(Cancel: Boolean);
var
  Order: Integer;
  iSection: Integer;
  OmegaCoords: array of Double;
  InsertPicture, bTemp: Boolean;
  Temp, CompName, UserName: AnsiString;

  Info: PImage;
  i, H, OrigH, OrigW, ResH, ResW: Integer;
  n: Cardinal;
  A: array of TRefPoint;
  P: PRelPoint;
  TabRect: TRect;
begin
  {$IFNDEF WMLT}
  { Convert boolean to integer }
  if Cancel then
    Order := 1
  else
    Order := 0;

  if StopDelayed then
  begin { Retry later - now recursion detected }
    PostMessage(OmegaChild.Handle, wm_Command, cm_StopOmega, Order); { Cancel = True }
//    OmegaLogFile.WriteLog('WARN:Can''t stop as a recursive call detected');
    Exit;
  end;

  try
    StopDelayed := True;
    if Omega = nil then
      Exit;
    if Omega.InCallBack then
    begin
      PostMessage(OmegaChild.Handle, wm_Command, cm_StopOmega, Order); { Cancel = True }
//      OmegaLogFile.WriteLog('WARN:Can''t stop as callback isn''t finished');
      Exit;
    end;
    { Default - we have to insert picture to WinGIS! }
    InsertPicture := True;
    Omega.SetSrcImage(EmptyStr); { Close files in Omega }
    Omega.SetDstImage(EmptyStr);
    OmegaLogFile.WriteLog('Close Omega internally handled images...');
    if not Cancel then { We have to load picture into WinGIS }
    begin { Try to store image in other file }
      OmegaLogFile.WriteLog('Begin image store...');
      OmegaResFile := OmegaOrigFile; { To inform save dialog on extension to a default extension of file }
      if ImgDlg.SaveImageFile(OmegaResFile) then { User agreed to insert image :-) }
      begin
        OmegaLogFile.WriteLog('Stored succesfully');
        OmegaLogFile.WriteLog('Deleting Omega input temp image...');
        DeleteFile(OmegaResFile); { Clear space just in case }
        OmegaLogFile.WriteLog('Deleted');
        OmegaLogFile.WriteLogFmt('Save result Omega image "%s" as "%s"...',[OmegaDstFile,OmegaResFile]);
        if TrLib32.SaveImageAs(OmegaDstFile, OmegaResFile) then { Try to restore file }
        begin
          OmegaLogFile.WriteLog('Saved succesfully, now delete output Omega temp image...');
          DeleteFile(OmegaDstFile); { and delete temp one if OK }
          OmegaLogFile.WriteLog('Deleted');
          InsertPicture := True;
        end
        else
        begin { Image man was unable to store file with user name  and extension}
          { We have to try to rename result work file as an result one }
          OmegaLogFile.WriteLog('ERR: Not saved, try even if to rename output Omega temp image...');
          Temp := ChangeFileExt(OmegaResFile, '.BMP');
          if not RenameFile(OmegaDstFile, Temp) then
          begin
            SmartMsgBox(OmegaChild.Handle, 'OmegaHndl', 53, 45, MB_OK + MB_ICONWARNING,
              [OmegaResFile, OmegaDstFile, Temp]);
            OmegaLogFile.WriteLog('ERR:Can''t rename image');
            InsertPicture := False;
          end
          else { Renamed, ask user to use it except wanted}
          begin
            OmegaLogFile.WriteLog('Renamed');
            if SmartMsgBox(OmegaChild.Handle, 'OmegaHndl', 54, 45, MB_YESNO + MB_ICONWARNING,
              [OmegaResFile, Temp]) = IDYES then
              OmegaResFile := Temp
            else
            begin
              OmegaLogFile.WriteLog('Cancelled by user');
              InsertPicture := False; { He not wants to save it as BMP :-(}
            end;
          end;
        end;
      end
      else { User don't want to use results, he is playing only :-!}
      begin
        OmegaLogFile.WriteLog('User cancelled result image store!');
        InsertPicture := False;
      end;
    end
    else { Func is called with Cancel=True - he doesn't want to save a new file }
    begin
      OmegaLogFile.WriteLog('User cancelled image save!');
      if FileExists(OmegaDstFile) then
        if FileIsImageOne(OmegaDstFile) then
          SmartMsgBox(OmegaChild.Handle, 'OmegaHndl', 55, 45, MB_OK + MB_ICONWARNING, [OmegaDstFile])
        else
          DeleteFile(OmegaDstFile); { Some strange file - delete it just in case }
      InsertPicture := False;
    end;

    { Clear  all the Omega properties and refresh status }
    with OmegaProj^ do
    begin
      if OldCaption <> '' then
      begin
        Parent.Caption := OldCaption;
        OldCaption := '';
      end;
      FinishMenuHandler;
      MenuFunctions['MovRelPoint'].Checked := False;
      MenuFunctions['AddRelPoint'].Checked := False;
      SetActualMenu(mn_Select);
      if RefLayer <> nil then
      begin { Delete our reference layer }
        // First save points information
        SetLength(A, Omega.Count);
        Temp := EmptyStr;
        for i := 0 to Omega.Count - 1 do
        begin
          P := Omega.PointAddress[i];
          A[i].Proj.X := P.Project.X;
          A[i].Proj.Y := P.Project.Y;
          A[i].Img.X := Round(P.Image.X);
          A[i].Img.Y := OmegaOrigHeight - Round(P.Image.Y) - 1;
        end;
        RefLayer.DeleteAllObjects(PInfo);
        Layers.DeleteLayer(RefLayer);
        RefLayer := nil;
      end;
      if SymbolMode = sym_Omega then
        SymbolMode := OmegaProjSym;
      OmegaProj.Layers.SetTopLayer(InsertLayerID); // Restore top layer
      if not Cancel then // Load picture with coordinates from Omega
        if InsertPicture then // Re-store result  according to user choice
        begin
          OmegaLogFile.WriteLog('Prepare to insert picture to WinGIS');
          SetLength(OmegaCoords, 4);
          OmegaCoords[0] := OmegaCorners[0].X;
          OmegaCoords[1] := OmegaCorners[0].Y;
          OmegaCoords[2] := OmegaCorners[1].X;
          OmegaCoords[3] := OmegaCorners[1].Y;
          OmegaLogFile.WriteLogFmt('Into a rectangle (%f,%f,%f,%f)',
                                   [OmegaCoords[0],OmegaCoords[1],OmegaCoords[2],OmegaCoords[3]]);
          if ProcInsertCBitMap(OmegaChild, OmegaResFile, OmegaCoords, @Info) then { Picture is inserted! }
          begin
            // Make OMG/TAB file for original image
            OmegaCoords[0] := Info.ClipRect.A.X;
            OmegaCoords[1] := Info.ClipRect.A.Y;
            OmegaCoords[2] := Info.ClipRect.B.X;
            OmegaCoords[3] := Info.ClipRect.B.Y;
            OmegaLogFile.WriteLog('Inserted succesfully! Generating TAB file...');
            CreateTabFile(OmegaOrigFile, Info.Info.Width, Info.Info.Height, nil {@TabRect}, A,
                          OmegaTabExtension,'***       Omega service of WinGIS      ***'#13 +
                          'Don''t use this file as a TAB one itself.');
//++ TODO:  add more information from OmegaPhoto by Almir request
            iSection := MlgStringList.Sections[Section];
            n := MAX_COMPUTERNAME_LENGTH+1;
            SetLength(CompName, n);
            if GetComputerName(PChar(CompName), n) then
            begin
               SetLength(CompName, n);
               Q_SpaceCompressInPlace(CompName);
            end
            else
               CompName := 'Not detected';
            n := 256;
            SetLength(UserName, n);
            if GetUserName(PChar(UserName), n) then
            begin
               SetLength(UserName, n);
               Q_SpaceCompressInPlace(UserName);
            end
            else
               UserName := 'Not detected';
            OrigImgInfo := TrLib.GetHeader(PChar(OmegaOrigFile), i, bTemp, 1);
            if (OrigImgInfo <> nil) then
            begin
              OrigW := OrigImgInfo.bmiHeader.biWidth;
              OrigH := OrigImgInfo.bmiHeader.biHeight;
              VirtualFree(OrigImgInfo, 0, MEM_RELEASE); // free memory
            end
            else
            begin
              OrigW := 0; OrigH := 0;
            end;
            Temp := Format(
                           '++++++++++++++++++++++++++++++++++++++++++++'#13 +
                           'project coordinates in meters, image ones in pixels.'#13#13 +
                           'computer name "%s".'#13 +
                           'user name "%s".'#13 +
                           'original filename "%s".'#13 +
                           '  its W=%d, H=%d'#13 +
                           'resulted filename "%s".'#13 +
                           '  its W=%d, H=%d'#13 +
                           'model of tranform "%s".'#13 +
                           'project rectangle { {%11.2f, %11.2f}, {%11.2f, %11.2f} }.'#13,
                           [
                            CompName,
                            UserName,
                            OmegaOrigFile,
                            OrigW, OrigH,
                            OmegaResFile,
                            Info.Info.Width, Info.Info.Height,
                            MlgStringList.SectionStrings[iSection, 16 + OmegaParam],
                            OmegaCoords[0]/100., OmegaCoords[1]/100.,OmegaCoords[2]/100., OmegaCoords[3]/100.
                           ]);
            Temp := Temp + 'points of transformation:'#13 +
                           ' project X | project Y | image X   | image Y'#13;
            for i := 0 to Length(A) - 1 do
            begin
                 Temp := Temp + Format('%11.2f|%11.2f|%11.2f|%11.2f'#13,
                                       [ A[i].Proj.X/100., A[i].Proj.Y/100., A[i].Img.X,A[i].Img.Y]);
            end;
            Temp := Temp + '--------------------------------------------';
            //  and for resulting one to mark it be inserted into project (we were working!)
            Info.UpdateTAB('***       Omega service of WinGIS      ***'#13 + Temp);
            OldModified := True;
          end
          else
           OmegaLogFile.WriteLog('ERR:Not inserted!');
        end;
      OmegaProj.Modified := OldModified; { Restore old modified state. All is safety if Omega was sucessful }
    end;
    if FileExists(OmegaSrcFile) then { Delete our temp source BMP image }
      if CompareText(OmegaSrcFile, OmegaOrigFile) <> 0 then { if it is not original one }
        DeleteFile(OmegaSrcFile);
    { It is time to kill Omega ITSELF :o( }
    Omega.Stop;
    UpdateLayersViewsLegendsLists(OmegaProj);
    UserInterface.Update([uiContextToolBars]);
  finally
    StopDelayed := False;
    if Assigned(OmegaTab) then
    begin
      OmegaTab.Free;
      OmegaTab := nil;
    end;
  end;
  EndSession;
  {$ENDIF}
end;

{ Returns new point index! }

function AddPoint2Omega(AIndex: PIndex): Integer;
var
  NewPoint: TRelPoint;
  SItem, AItem: PIndex;
  AView: PView;
begin
  Result := -1;
  if Omega <> nil then
  begin
    if not AddingFromOmega then
    begin // We are called from WinGIS by user's insertion
      with NewPoint do
      begin // that is the point not already in list.
        with Project do
        begin
          SItem := New(PIndex, Init(0)); // Create temp item with real ID
          try
            SItem.Index := AIndex.Index;
            AItem := RefLayer.HasObject(SItem); // Object in Layer ?
            AView := OmegaProj.Layers.IndexObject(OmegaProj.PInfo, AItem); // Get view of our point
            if AView <> nil then // Found !
              with PPixel(AView)^ do
              begin
                X := PPixel(AView).Position.X;
                Y := PPixel(AView).Position.Y;
              end;
          finally
            SItem.Done;
          end;
        end;
        with Image do
        begin
          X := 0;
          Y := 0;
        end;
        with Shift do
        begin
          X := 0; Y := 0;
        end;
        ProjectStatus := rsSet; // Point is set in WinGIS
        ImageStatus := rsPreSet; // and not set in OmegaPhoto
        NameSize := 60; // ??? just in case ( to mimic C++Builder source )

      end;
      Result := Omega.AddControlPoint(NewPoint);
      Omega.SelectControlPoint(Result - 1);
      Omega.Show;
    end
    else
      Result := Omega.Count; // We avoid recusion call of our caller
  end;
end;

{ TOmegaShow }
{
  Show OmegaPhoto window. First test it to be created at all.
}

procedure TOmegaShow.OnStart;
begin
  Omega.Show;
end;

{ TOmegaAdd }

procedure TOmegaAdd.OnStart;
begin
  { Insert point[s] mode enter }
  if (OmegaProj <> nil) then
    with OmegaProj^ do
    begin
      FinishMenuHandler;
      MenuFunctions['MovRelPoint'].Checked := False;
      case MenuFunctions['AddRelPoint'].Checked of
        TRUE: { Moved DOWN  - change to Add points cursor etc }
          SetActualMenu(mn_SetOmegaPoint);
        FALSE:
          begin
            SetActualMenu(mn_Select);
          end;
      end;
    end;
end;

{ TOmegaMove }

procedure TOmegaMove.OnStart;
begin
  { Prepare to move point }
  if (Omega <> nil) then
    with OmegaProj^ do
    begin
      FinishMenuHandler;
      MenuFunctions['AddRelPoint'].Checked := False;
      case MenuFunctions['MovRelPoint'].Checked of
        TRUE:
          SetActualMenu(mn_Snap);
        FALSE:
          begin
            ActualMen := OldMenu;
            PInfo.SetTheCursor(crArrow);
          end;
      end;
    end;
end;

{$ifdef FULL_OMEGA}
{ TOmegaDel }

procedure TOmegaDel.OnStart;
begin
  { Delete active point }
  NotImplemented;
end;

{ TOmegaSel }

procedure TOmegaSel.OnStart;
begin
  { Select wated point }
  NotImplemented;
end;

{ TOmegaNext }

procedure TOmegaNext.OnStart;
begin
  {  Change to next point (rotating) }
  NotImplemented;
end;

{ TOmegaPrev }

procedure TOmegaPrev.OnStart;
begin
  {  Change to previous point (rotating) }
  NotImplemented;
end;
{$endif}

{ TOmegaInfo }

procedure TOmegaInfo.OnStart;
var
  OldType: Integer;
  Selected: Integer;
begin
  { Here to show some info and get/set some parameter[s] }
  { NotImplemented; }
  OldType := Omega.TransformType;
  Selected := Omega.SelectedIndex;
  InfoForm := TInfoForm.Create(OmegaChild);
  try
    with InfoForm do
      if InfoForm.ShowModal = mrOK then { User clicked OK button }
      begin
        if TransformTypeComboBox.ItemIndex <> OldType then { User change  transform type }
          Omega.TransformType := TransformTypeComboBox.ItemIndex;
        with PointsInfoComboBox do
          if ItemIndex <> Selected then
            if ItemIndex <> -1 then { User change point selected }
              DeSelectAllSelectOne(ItemIndex);
      end;
  finally
    InfoForm.Free;
  end;
end;

{ TOmegaCancel }

procedure TOmegaCancel.OnStart;
begin
 { Cancel all the changes and free all the resources }
  StopOmega(True);
end;

{ TOmegaExec }

procedure TOmegaExec.OnStart;
var
  PBuf: PPoint2Array;
begin
  { Here to send command for resulting transformation }
  if Omega.TransformType = 0 then
  begin
    OmegaMsg(52);
    Exit;
  end;
  Omega.Show;
  PBuf := Omega.MakeTransformation;
  if PBuf <> nil then
  begin
    OmegaMsg(50);
    CopyMemory(@OmegaCorners, PBuf, SizeOf(TPoint2Array));
    StopOmega(False);
  end
  else
    OmegaMsg(51);
end;

procedure TInfoForm.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #27 then
    ModalResult := mrCancel;
end;

function InAddFromOmega: Boolean;
begin
  Result := AddingFromOmega;
end;

procedure TInfoForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//  Action := caFree;
end;

procedure TInfoForm.PointsInfoComboBoxChange(Sender: TObject);
var
  ItemIndx: Integer;
  PPoint: PRelPoint;

  procedure HintToCaption(RootControl: TControl);
  var
    Control: TControl;
    I: Integer;
  begin
    if RootControl is TWinControl then
      for I := 0 to TWinControl(RootControl).ControlCount - 1 do
      begin
        Control := TWinControl(RootControl).Controls[I];
        with Control do
          if Control.ClassName = 'TLabel' then
            TLabel(Control).Caption := TLabel(Control).Hint
          else
            if Control is TWinControl then
              if ControlCount > 0 then
                HintToCaption(Control);
      end;
  end;

begin
  with PointsInfoComboBox do
  begin
    ItemIndx := ItemIndex;
    if ItemIndx = -1 then
    begin { Just in case }
      if Items.Count = 0 then
      begin
        { Set default information on labels }
        HintToCaption(PointInfoGroupBox);
        Text := '---';
      end;
      Exit;
    end;
  end;
  Omega.SelectControlPoint(ItemIndx); { Select point in Omega }
  PPoint := Omega.PointAddress[ItemIndx]; { Get address of a selected point in Omega}
  with PPoint^ do
  begin
    ProjXLab.Caption := Format('X: %10.2f', [Project.X / 100.]);
    ProjYLab.Caption := Format('Y: %10.2f', [Project.Y / 100.]);
    ProjStatLab.Caption := RelStatNames[ProjectStatus];
    ImgXLab.Caption := Format('X: %10.2f', [Image.X]);
    ImgYLab.Caption := Format('Y: %10.2f', [Image.Y]);
    ImgStatLab.Caption := RelStatNames[ImageStatus];
  end;
end;

procedure ShowOmega;
begin
{  if Omega <> nil then
    if Omega.InCallBack then
      PostMessage(OmegaChild.Handle, wm_Command, cm_ShowOmega, 0)
    else}
  Omega.Show;
end;
initialization
  MenuFunctions.RegisterFromResource(HInstance, 'OmegaHndl', 'OmegaHndl');
  TOmegaAdd.Registrate('AddRelPoint');
  TOmegaCancel.Registrate('CancelOmega');
  TOmegaInfo.Registrate('InfRelPoint');
  TOmegaMove.Registrate('MovRelPoint');
{$ifdef FULL_OMEGA}
  TOmegaDel.Registrate('DelRelPoint');
  TOmegaNext.Registrate('NxtRelPoint');
  TOmegaPrev.Registrate('PrvRelPoint');
  TOmegaSel.Registrate('SelRelPoint');
{$endif}
  TOmegaExec.Registrate('ExecExitOmega');
  TOmegaShow.Registrate('ShowOmega');
end.

