{*******************************************************************************
| Unit AM_Exp
|-------------------------------------------------------------------------------
| Autor: Martin Forst
|-------------------------------------------------------------------------------
| Dialog und Funktionen f�r das Explodieren von Objekten
+******************************************************************************}
unit AM_Exp;

interface

uses Messages, WinTypes, WinProcs, AM_Def, AM_Admin, AM_Layer, SysUtils, AM_Coll,
  AM_Dlg1, AM_StdDl, Classes, Objects, Controls, ProjStyle;

type
  PExplodeDat = ^TExplodeDat;
  TExplodeDat = object(TOldObject)
    ExpPolys: Boolean;
    ExpCPolys: Boolean;
    Layer: LongInt;
    constructor Init;
    constructor Load(S: TOldStream);
    procedure Store(S: TOldStream);
  end;

  PExplodeDlg = ^TExplodeDlg;
  TExplodeDlg = class(TLayerDlg)
  public
    Data: PExplodeDat;
    BResult: PPIndexColl;
    constructor Init(AParent: TComponent; AData: PExplodeDat; ALayer: PLayers; ABResult: PPIndexColl;
      AProjStyle: TProjectStyles);
    function CanClose: Boolean; override;
    function GetLayers(ALayer: LongInt): Boolean;
    procedure SetLayer(ALayer: LongInt);
    procedure SetupWindow; override;
  end;

procedure Explode(AProj: Pointer);

procedure ExplodeWithoutDlg(AProj: Pointer; SourceLayer, DestLayer: PLayer);

implementation

uses AM_Proj, AM_Index, AM_Poly, AM_CPoly, AM_Paint, ResDlg, StateBar, UserIntf;

const
  id_Polys = 100;
  id_CPolys = 101;
  id_List = 102;
  id_Layer = 103;

constructor TExplodeDlg.Init
  (
  AParent: TComponent;
  AData: PExplodeDat;
  ALayer: PLayers;
  ABResult: PPIndexColl;
  AProjStyle: TProjectStyles
  );
begin
  inherited Init(AParent, 'EXP', ALayer, AProjStyle);
  Data := AData;
  BResult := ABResult;
  HelpContext := 5010;
end;

procedure TExplodeDlg.SetLayer
  (
  ALayer: LongInt
  );
var
  BLayer: PLayer;
  AText: array[0..255] of Char;
begin
  if ALayer > 0 then
  begin
    BLayer := Layers^.IndexToLayer(ALayer);
    if BLayer <> nil then
      SetDlgItemText(Handle, id_Layer, StrPCopy(AText, PToStr(BLayer^.Text)));
  end
  else
    SetDlgItemText(Handle, id_Layer, '');
end;

procedure TExplodeDlg.SetupWindow;
begin
  inherited SetupWindow;
  with Data^ do
  begin
    if ExpPolys then
      CheckDlgButton(Handle, id_Polys, 1);
    if ExpCPolys then
      CheckDlgButton(Handle, id_CPolys, 1);
  end;
  FillLayerList(id_List, TRUE, FALSE);
  FillLayerList(id_Layer, FALSE, TRUE);
end;

function TExplodeDlg.CanClose
  : Boolean;
var
  ALayer: LongInt;
begin
  CanClose := FALSE;
  if (IsDlgButtonChecked(Handle, id_Polys) = 0)
    and (IsDlgButtonChecked(Handle, id_CPolys) = 0) then
  begin
    MsgBox(Handle, 957, mb_IconExclamation or mb_OK, '');
    SetFocus(GetItemHandle(id_Polys));
  end
  else
  begin
    if SendDlgItemMsg(id_List, lb_GetSelCount, 0, 0) = 0 then
    begin
      MsgBox(Handle, 958, mb_IconExclamation or mb_OK, '');
      SetFocus(GetItemHandle(id_List));
    end
    else
      if GetLayer(id_Layer, ALayer, 959, 10125, 10128, ilTop)
      and GetLayers(ALayer) then
        with Data^ do
        begin
          ExpPolys := IsDlgButtonChecked(Handle, id_Polys) = 1;
          ExpCPolys := IsDlgButtonChecked(Handle, id_CPolys) = 1;
          Layer := ALayer;
          CanClose := TRUE;
        end;
  end;
end;

function TExplodeDlg.GetLayers
  (
  ALayer: LongInt
  )
  : Boolean;
var
  Puffer: array[0..bsSelLayer] of Integer;
  AName: array[0..255] of Char;
  Items: Integer;
  BLayer: PLayer;
  Cnt: Integer;
begin
  Items := SendDlgItemMsg(id_List, lb_GetSelItems, bsSelLayer, LongInt(@Puffer));
  BResult^ := New(PIndexColl, Init(5, 5));
  Cnt := 0;
  while Cnt < Items do
  begin
    SendDlgItemMsg(id_List, lb_GetText, Puffer[Cnt], LongInt(@AName));
    BLayer := Layers^.NameToLayer(StrPas(AName));
    if BLayer <> nil then
    begin
      if BLayer^.Index = ALayer then
      begin
        MsgBox(Handle, 961, mb_IconExclamation or mb_OK, PToStr(BLayer^.Text));
        BResult^^.DeleteAll;
        Dispose(BResult^, Done);
        Cnt := Items;
      end
      else
        BResult^^.Insert(BLayer);
    end;
    Inc(Cnt);
  end;
  GetLayers := Cnt = Items;
end;

constructor TExplodeDat.Init;
begin
  TOldObject.Init;
  ExpPolys := TRUE;
  ExpCPolys := TRUE;
  Layer := 0;
end;

constructor TExplodeDat.Load
  (
  S: TOldStream
  );
var
  Version: Word;
begin
  TOldObject.Init;
  S.Read(Version, SizeOf(Version));
  S.Read(ExpPolys, SizeOf(ExpPolys));
  S.Read(ExpCPolys, SizeOf(ExpCPolys));
  S.Read(Layer, SizeOf(Layer));
end;

procedure TExplodeDat.Store
  (
  S: TOldStream
  );
var
  Version: Word;
begin
  Version := 0;
  S.Write(Version, SizeOf(Version));
  S.Write(ExpPolys, SizeOf(ExpPolys));
  S.Write(ExpCPolys, SizeOf(ExpCPolys));
  S.Write(Layer, SizeOf(Layer));
end;

procedure Explode
  (
  AProj: Pointer
  );
var
  ToExplode: PIndexColl;
  BLayer: PLayer;
  CurItem: LongInt;
  Objects: PViewColl;
  AllItems: LongInt;

  procedure DoCreateList
      (
      Item: PLayer
      ); far;

    procedure DoItems
        (
        Item: PIndex
        ); far;
    var
      AIndex: LongInt;
    begin
      if not Objects^.Search(Item, AIndex) then
        Objects^.AtInsert(AIndex, Item);
    end;
  begin
    Item^.Data^.ForEach(@DoItems);
  end;

  function DoItems
      (
      AItem: PIndex
      )
      : Boolean; far;
  var
    BItem: PPoly;
  begin
    BItem := Pointer(BLayer^.IndexObject(PProj(AProj)^.PInfo, AItem));
    if ((BItem^.GetObjType = ot_Poly) and (PProj(AProj)^.ExplodeOpt.ExpPolys)) or
      ((BItem^.GetObjType = ot_CPoly) and (PProj(AProj)^.ExplodeOpt.ExpCPolys)) then
      BItem^.Explode(PProj(AProj)^.PInfo, BLayer);
    Inc(CurItem);
    StatusBar.Progress := 100 * CurItem / AllItems;
    DoItems := StatusBar.QueryAbort;
  end;
begin
  with PProj(AProj)^ do
  begin
    DeselectAll(TRUE);
    if ExecDialog(TExplodeDlg.Init(Parent, @ExplodeOpt, Layers,
      @ToExplode, PInfo^.ProjStyles)) = id_OK then
    begin
      Layers^.DetermineTopLayer;
      StatusBar.ProgressPanel := TRUE;
      try
        StatusBar.ProgressText := GetLangText(962);
        StatusBar.AbortText := GetLangText(964);
        SetCursor(crHourGlass);
        Objects := New(PViewColl, Init);
        ToExplode^.ForEach(@DoCreateList);
        BLayer := Layers^.IndexToLayer(ExplodeOpt.Layer);
        AllItems := Objects^.GetCount;
        CurItem := 0;
        Objects^.FirstThat(@DoItems);
        ToExplode^.DeleteAll;
        Dispose(ToExplode, Done);
        Objects^.DeleteAll;
        Dispose(Objects, Done);
        SetModified;
      finally
        SetCursor(crDefault);
        StatusBar.ProgressPanel := FALSE;
        PInfo^.RedrawScreen(TRUE);
      end;
    end;
  end;
end;

procedure ExplodeWithoutDlg
  (
  AProj: Pointer;
  SourceLayer: PLayer;
  DestLayer: PLayer
  );
var
  BLayer: PLayer;
  CurItem: LongInt;
  Objects: PViewColl;
  AllItems: LongInt;

  procedure DoCreateList
      (
      Item: PLayer
      );

    procedure DoItems
        (
        Item: PIndex
        ); far;
    var
      AIndex: LongInt;
    begin
      if not Objects^.Search(Item, AIndex) then
        Objects^.AtInsert(AIndex, Item);
    end;
  begin
    Item^.Data^.ForEach(@DoItems);
  end;

  function DoItems
      (
      AItem: PIndex
      )
      : Boolean; far;
  var
    BItem: PPoly;
  begin
    BItem := Pointer(BLayer^.IndexObject(PProj(AProj)^.PInfo, AItem));
    if ((BItem^.GetObjType = ot_Poly) and (PProj(AProj)^.ExplodeOpt.ExpPolys)) or
      ((BItem^.GetObjType = ot_CPoly) and (PProj(AProj)^.ExplodeOpt.ExpCPolys)) then
      BItem^.Explode(PProj(AProj)^.PInfo, BLayer);
    Inc(CurItem);
    StatusBar.Progress := 100 * CurItem / AllItems;
    DoItems := StatusBar.QueryAbort;
  end;
begin
  with PProj(AProj)^ do
  begin
    StatusBar.ProgressPanel := TRUE;
    try
      StatusBar.ProgressText := GetLangText(962);
      StatusBar.AbortText := GetLangText(964);
      SetCursor(crHourGlass);
      Objects := New(PViewColl, Init);
      DoCreateList(SourceLayer);
      BLayer := DestLayer;
      AllItems := Objects^.GetCount;
      CurItem := 0;
      Objects^.FirstThat(@DoItems);
      Objects^.DeleteAll;
      Dispose(Objects, Done);
      SetModified;
    finally
      SetCursor(crDefault);
      StatusBar.ProgressPanel := FALSE;
      PInfo^.RedrawScreen(TRUE);
    end;
  end;
end;

end.

