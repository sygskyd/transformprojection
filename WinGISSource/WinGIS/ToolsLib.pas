unit ToolsLib;

interface

uses SysUtils, Classes, Windows;

function SToF(s: AnsiString): Double;
function Utf8ToUnicode(Dest: PWideChar; MaxDestChars: Cardinal; Source: PChar; SourceBytes: Cardinal): Cardinal;
function Utf8Decode(const S: AnsiString): WideString;
function GuidIsNull(AGUID: TGUID): Boolean;
procedure ResetGuid(var AGUID: TGUID);
function StringToList(s: AnsiString; c: Char): TStringList;
procedure DrawTransparentBitmap(DC: HDC; hBmp: HBITMAP; xStart: integer; yStart: integer; cTransparentColor: COLORREF);

implementation

procedure DrawTransparentBitmap(DC: HDC; hBmp: HBITMAP; xStart: integer; yStart: integer; cTransparentColor: COLORREF);
var
bm: BITMAP;
cColor: COLORREF;
bmAndBack, bmAndObject, bmAndMem, bmSave:HBITMAP;
bmBackOld, bmObjectOld, bmMemOld, bmSaveOld: HBITMAP;
hdcMem, hdcBack, hdcObject, hdcTemp, hdcSave:HDC;
ptSize: TPOINT;
begin
     hdcTemp := CreateCompatibleDC(dc);
     SelectObject(hdcTemp, hBmp);
     GetObject(hBmp, sizeof(BITMAP), @bm);
     ptSize.x := bm.bmWidth;
     ptSize.y := bm.bmHeight;
     DPtoLP(hdcTemp, ptSize, 1);
     hdcBack:= CreateCompatibleDC(dc);
     hdcObject := CreateCompatibleDC(dc);
     hdcMem:= CreateCompatibleDC(dc);
     hdcSave:= CreateCompatibleDC(dc);
     bmAndBack:= CreateBitmap(ptSize.x, ptSize.y, 1, 1, nil);
     bmAndObject := CreateBitmap(ptSize.x, ptSize.y, 1, 1, nil);
     bmAndMem:= CreateCompatibleBitmap(dc, ptSize.x, ptSize.y);
     bmSave := CreateCompatibleBitmap(dc, ptSize.x, ptSize.y);
     bmBackOld:= SelectObject(hdcBack, bmAndBack);
     bmObjectOld := SelectObject(hdcObject, bmAndObject);
     bmMemOld:= SelectObject(hdcMem, bmAndMem);
     bmSaveOld:= SelectObject(hdcSave, bmSave);
     SetMapMode(hdcTemp, GetMapMode(dc));
     BitBlt(hdcSave, 0, 0, ptSize.x, ptSize.y, hdcTemp, 0, 0, SRCCOPY);
     cColor := SetBkColor(hdcTemp, cTransparentColor);
     BitBlt(hdcObject, 0, 0, ptSize.x, ptSize.y, hdcTemp, 0, 0, SRCCOPY);
     SetBkColor(hdcTemp, cColor);
     BitBlt(hdcBack, 0, 0, ptSize.x, ptSize.y, hdcObject, 0, 0, NOTSRCCOPY);
     BitBlt(hdcMem, 0, 0, ptSize.x, ptSize.y, dc, xStart, yStart, SRCCOPY);
     BitBlt(hdcMem, 0, 0, ptSize.x, ptSize.y, hdcObject, 0, 0, SRCAND);
     BitBlt(hdcTemp, 0, 0, ptSize.x, ptSize.y, hdcBack, 0, 0, SRCAND);
     BitBlt(hdcMem, 0, 0, ptSize.x, ptSize.y, hdcTemp, 0, 0, SRCPAINT);
     BitBlt(dc, xStart, yStart, ptSize.x, ptSize.y, hdcMem, 0, 0, SRCCOPY);
     BitBlt(hdcTemp, 0, 0, ptSize.x, ptSize.y, hdcSave, 0, 0, SRCCOPY);
     DeleteObject(SelectObject(hdcBack, bmBackOld));
     DeleteObject(SelectObject(hdcObject, bmObjectOld));
     DeleteObject(SelectObject(hdcMem, bmMemOld));
     DeleteObject(SelectObject(hdcSave, bmSaveOld));
     DeleteDC(hdcMem);
     DeleteDC(hdcBack);
     DeleteDC(hdcObject);
     DeleteDC(hdcSave);
     DeleteDC(hdcTemp);
end;

function StringToList(s: AnsiString; c: Char): TStringList;
var
i,n: Integer;
APart: AnsiString;
begin
     Result:=TStringList.Create;
     n:=StrLen(PChar(s))+1;
     APart:='';
     for i:=1 to n do begin
         if (s[i] = c) or (i = n) then begin
            Result.Add(APart);
            APart:='';
         end
         else begin
            APart:=APart + s[i];
         end;
     end;
end;

function SToF(s: AnsiString): Double;
begin
     try
        s:=StringReplace(s,'.',DecimalSeparator,[rfReplaceAll]);
        s:=StringReplace(s,',',DecimalSeparator,[rfReplaceAll]);
        Result:=StrToFloat(s);
     except
        Result:=0;
     end;
end;

function Utf8ToUnicode(Dest: PWideChar; MaxDestChars: Cardinal; Source: PChar; SourceBytes: Cardinal): Cardinal;
var
  i, count: Cardinal;
  c: Byte;
  wc: Cardinal;
begin
  if Source = nil then
  begin
    Result := 0;
    Exit;
  end;
  Result := Cardinal(-1);
  count := 0;
  i := 0;
  if Dest <> nil then
  begin
    while (i < SourceBytes) and (count < MaxDestChars) do
    begin
      wc := Cardinal(Source[i]);
      Inc(i);
      if (wc and $80) <> 0 then
      begin
        wc := wc and $3F;
        if i > SourceBytes then Exit;           // incomplete multibyte char
        if (wc and $20) <> 0 then
        begin
          c := Byte(Source[i]);
          Inc(i);
          if (c and $C0) <> $80 then  Exit;     // malformed trail byte or out of range char
          if i > SourceBytes then Exit;         // incomplete multibyte char
          wc := (wc shl 6) or (c and $3F);
        end;
        c := Byte(Source[i]);
        Inc(i);
        if (c and $C0) <> $80 then Exit;       // malformed trail byte

        Dest[count] := WideChar((wc shl 6) or (c and $3F));
      end
      else
        Dest[count] := WideChar(wc);
      Inc(count);
    end;
	if count >= MaxDestChars then count := MaxDestChars-1;
	Dest[count] := #0;
  end
  else
  begin
	while (i <= SourceBytes) do
	begin
	  c := Byte(Source[i]);
	  Inc(i);
	  if (c and $80) <> 0 then
	  begin
		if (c and $F0) = $F0 then Exit;  // too many bytes for UCS2
		if (c and $40) = 0 then Exit;    // malformed lead byte
		if i > SourceBytes then Exit;         // incomplete multibyte char

		if (Byte(Source[i]) and $C0) <> $80 then Exit;  // malformed trail byte
		Inc(i);
		if i > SourceBytes then Exit;         // incomplete multibyte char
		if ((c and $20) <> 0) and ((Byte(Source[i]) and $C0) <> $80) then Exit; // malformed trail byte
		Inc(i);
	  end;
	  Inc(count);                                
	end;
  end;
  Result := count+1;
end;

function Utf8Decode(const S: AnsiString): WideString;
var
  L: Integer;
  Temp: WideString;
begin
  Result := '';
  if S = '' then Exit;
  SetLength(Temp, Length(S));

  L := Utf8ToUnicode(PWideChar(Temp), Length(Temp)+1, PChar(S), Length(S));
  if L > 0 then
    SetLength(Temp, L-1)
  else
    Temp := '';
  Result := Temp;
end;

function GuidIsNull(AGUID: TGUID): Boolean;
var
i: Integer;
begin
     Result:=(AGUID.D1 = 0) and (AGUID.D2 = 0) and (AGUID.D3 = 0);
     if Result then begin
        for i:=0 to 7 do begin
            if AGUID.D4[i] <> 0 then begin
               Result:=False;
               break;
            end;
        end;
     end;
end;

procedure ResetGuid(var AGUID: TGUID);
var
i: Integer;
begin
     AGUID.D1:=0;
     AGUID.D2:=0;
     AGUID.D3:=0;
     for i:=0 to 7 do begin
         AGUID.D4[i]:=0;
     end;
end;

end.
