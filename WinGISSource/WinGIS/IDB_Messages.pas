unit IDB_Messages;      
{
Internal database. Ver. II.
Various constants that I use in other parts of Internal Database.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 09-02-2001
}
interface

Uses Messages;

Const
     WM_IDB_Message = wm_User + $2742;

     IDB_DeleteObjectsAction  = 1; { Sent as:
                                     SendMessage(Handle of a project window,
                                                 WM_IDB_Message,
                                                 IDB_DeleteObjectsAction,
                                                 A pointer to the layer for which the deleting operation wil be done
                                                 );
                                   }
     IDB_MustBeDisconnected = 2;   { Sent as:
                                     PostMessage(Handle of main window of WinGIS,
                                                 WM_IDB_Message,
                                                 IDB_MustBeDisconnected,
                                                 A pointer to a project that should be disconnected from the IDB);
                                   }
     IDB_RefreshDataWindow = 3;    { Sent as:
                                     SendMesage(Handle of a data window,
                                                WM_IDB_Message,
                                                IDB_RefreshDataWindow,
                                                0);
                                   }

implementation

end.
