{******************************************************************************+
  Unit Tooltips
--------------------------------------------------------------------------------
  Author : Raimund Leitner
--------------------------------------------------------------------------------
  Implements the feature to display a short info about an object by the
  cursor. Uses a thread to search in background without user disturbance

27.03.2000
  Dennis Ivanoff, Progis Moscow, Russia
  xBase files as a source of tooltips information were added. (xBase founded on dbf format).
  External source for tooltips after loading still Paradox type.
*******************************************************************************}
unit ToolTips;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls,
  AM_Paint, AM_Index, AM_Def, ExtCanvas, GrTools, TooltipShow, MultiLng, ActiveX
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
  , AM_DDE
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
{++ Tooltips DLL}
  , DTI_LibMan
{-- Tooltips DLL}
  ;

const
  MlgSectionName = 'Tooltips';
  // count of found items remembered in TTooltipsThread
  int_RingElements = 20;

  // back color of tool tip text
  RGB_BackToolTip = $E7FFFF; // light yellow (=clInfoBk)
  // border color of tool tip text
  RGB_BorderToolTip = clDkGray; // black
  // text color of tool tip text
  RGB_TextToolTip = $0; // black
  // text font for tool tip text
  str_FontToolTip = 'MS Sans Serif';
  // text size for tool tip text
  int_SizeToolTip = 50;
  // max length for floating text
  int_FloatingTextMaxLen = 100;
  // default delay time
  int_DefaultDelay = 500;

  // constants for TToolTipInfo,BDEOrLocal
  int_None = 0; // no tooltips
  int_Local = 1; // tooltips stored local in project
  int_BDE = 2; // tooltips stored in BDE database
  int_DDE = 3; // tooltips stored in DDE-connected DB
  int_IDB = 4; // The sign of using the Internal Database for working with GeoText (TToolTip).
  int_ADO = 5;

  // constants for TToolTipInfo,AutoText
  int_Manu = 0; // manual input of tooltiptext
  int_ID = 1; // tooltip shows ID
  int_Area = 2; // tooltip shows area
  int_Peri = 3; // tooltip shows perimeter
  int_Coord = 4; // tooltip shows coordinates
  int_SymNr = 5; // tooltip shows symbolnumber
  int_SymNa = 6; // tooltip shows symbolname
  int_ANone = 0; // no auto tooltext
  int_Auto = 1; // tooltiptexts for all objects on layer will be filled autmatically
  int_Sel = 2; // tooltiptexts for all selected objects on layer will be filled autmatically
  int_Deflt = 3; // default tooltiptext
  // border to floating text (to match TooltipShow form)
  int_UR_x = 7;
  int_UR_y = 6;
  int_LR_x = 4;
  int_LR_y = 4;

  // multi line floating texts
  LineBreakChars = [#10, #13, '|'];

type
{******************************************************************************+
  Class TTooltipInfo
--------------------------------------------------------------------------------
  Info about tooltips of layer (TLayer)
*******************************************************************************}
  TToolTipInfo = class(TPersistent)
  protected
    procedure AssignTo(Dest: TPersistent); override;
  public
    BDEOrLocal: Integer;
    BDETable: string; // filename (with complete path)
    IDColName: string; // name of ID column
    FTColName: string; // name of floating text column
    AutoText: Integer;
    {ShowDefault: Boolean;}
    FillAuto: Integer;
    constructor Create;
    procedure SetBDE(aTable: string; aIDCol: string; aFTCol: string);
{++ IDB}
    procedure SetIDB(aTable: string; aIDCol: string; aFTCol: string);
{-- IDB}
    procedure SetADO(ATable: AnsiString; AIDCol: AnsiString; AFTCol: AnsiString);

    procedure SetLocal;
    procedure SetNone;
    procedure SetDDE;
    procedure SetFillNone;
    procedure SetFillAuto;
    procedure SetFillAutoSelected;
    procedure SetShowDefault;
    procedure CopyFrom(Source: TTooltipInfo);
    function IsDifferent(Other: TTooltipInfo): Boolean;
  end;

{******************************************************************************+
  Class TRingBuffer
--------------------------------------------------------------------------------
  Encapsulates a ring buffer with various number of elements
*******************************************************************************}
  TRingBuffer = class(TList)
  private
    FElements: Integer;
    FCurrent: Integer;
  public
    constructor Create(Elements: Integer);
    procedure SaveNext(Element: Pointer);
  end;

{******************************************************************************+
  Class TFloatingTextInfo
--------------------------------------------------------------------------------
  Object with text and position of floating text
*******************************************************************************}
  TFloatingTextInfo = class
  public
    FText: PString;
    FTextPos: TDPoint;
    FIndex: LongInt;
    FSize: TSize;
    constructor Create;
    destructor Destroy; override;
  end;

{******************************************************************************+
  Class TToolTipThread
--------------------------------------------------------------------------------
  Encapsulates the thread used for displaying the tooltips
*******************************************************************************}
  TTooltipThread = class(TThread)
  private
    FTooltipLayer: Pointer;
    FLastProject: Pointer;
    FMouseX,
      FMouseY: Integer;
    FPInfo: pPaint;
    FChanged: Boolean;
    FDisplayed: Boolean;
    FRingBuffer: TRingBuffer;
    FShowForm: TTooltipShowForm;
    FFloatTextList: TList; // List of fixed floating texts
    FDispX,
      FDispY: Integer; // position of actual displayed floating text
    FItem: PIndex; // current floating text object
    FInfoBkColor: TColor; // background color of info window (-1 for none)
    FInfoTextColor: TColor; // text color of info text
    FInfoFrmColor: TColor; // frame color of info window (-1 for none)
    FInfoTextStyle: string; // text style name (default 'MS Sans Serif')
    FDelayTime: Integer; // idle time for display
    FToSuspend: Boolean; // true waiting for "suresuspend"
    FDDEIndex: LongInt; // current pending dde index request
    FDDEResult: string; // last result from dde request
    procedure SetMouseX(MouseX: Integer);
    procedure SetMouseY(MouseY: Integer);
    procedure SetProject(aProject: Pointer);
    function GetBDEInfo(Index: LongInt): string;
    function GetDDEInfo(Index: LongInt): string;
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
{++ IDB}
    function GetIDBInfo(Index: LongInt): string;
{-- IDB}
    function GetADOInfo(Index: LongInt): string;
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
    procedure GetFloatingTextRect(aPInfo: pPaint; FTextPos: TDPoint;
      TextSize: TSize; var Rect: array of TPoint);
    procedure ChangeFloatingText(aItem: pIndex; NewFT: string);
    procedure CheckCurrentProject;
    procedure DrawFloatingText(aPInfo: pPaint; aFTInfo: TFloatingTextInfo);
    procedure ChangeFloatingTextForObject(Index: LongInt; NewFT: string);
    procedure ShowAllFTLocal(aLayer: pointer; Flag: boolean); //Changed by Brovak
    procedure ShowAllFTBDE(aLayer: pointer; Flag: boolean); //Changed by Brovak
    function CreateFTInfo(aItem: pIndex; aText: string): TFloatingTextInfo;
    function IsMouseOverFT: Integer;
    function IsFTDisplayed: Boolean;
    function IsNewEditPossible: Boolean;
    function IsDeletePossible: Boolean;
    function IsFixPossible: Boolean;
  protected
    procedure Execute; override;
  public
        {++Brovak Bug 106 BUILD 135}
    ShowAllFlag: boolean;
    ShowSelectedFlag: boolean;
        {--Brovak}

    constructor Create;
      // for internal use only
    property Layer: Pointer read FTooltipLayer write FTooltipLayer;
    property Project: Pointer read FLastProject write SetProject;
    property PInfo: PPaint read FPInfo write FPInfo;
    property MouseX: Integer read FMouseX write SetMouseX;
    property MouseY: Integer read FMouseY write SetMouseY;
    procedure Mouse(Proj: Pointer; X: Integer; Y: Integer);
    destructor Destroy; override;

    procedure DBGeoText(AItem: PIndex; aText: string);
      // dde callback function

    procedure DDEGeotextReady;
      // start/end methods
    procedure StartFloatingText(aProject: Pointer);
    procedure EndFloatingText;
    procedure SureSuspend;

{      // start Floating text thread
      Procedure   TTResume;
      // suspend Floating text thread
      Procedure   TTSuspend;}

      {++brovak BUG 106 build 135}
    function IsExistSelected: boolean;
      {brovak}

      // fix current displayed floating text
    procedure FixFloatingText;
      // draw all fixed floating texts
    procedure DrawFixedFloatingTexts(aPInfo: pPaint);
      // draw fixed floating texts for the object with the given index
    procedure DrawFloatingTextsForObject(aPInfo: pPAint; Index: LongInt);
      // new/edit current floating text
    procedure ChangeCurrentFloatingText;
      // display all possible floating texts of layer
      {++Brovak BUG 106 build 136}
    procedure ChangeCurrentFloatingTextForSelected;
      {--brovak}
    procedure ShowAllFloatingTexts(Flag: boolean); //changed by Brovak
      // display all ft for selected objects
    procedure ShowSelectedFloatingTexts(Flag: boolean); //changed by Brovak
      // delete the current floating text
    procedure DeleteCurrentFloatingText;
      // clear (delete) fixed floating texts
    procedure ClearFixedFloatingTexts;
      // a floating text is currently displayed
    property IsFloatingTextDisplayed: Boolean read IsFTDisplayed;
      // mouse cursor is over an fixed floating text
    property IsMouseOverFloatingText: Integer read IsMouseOverFT;
      // true, when new/edit floating text possible
    property NewEditPossible: Boolean read IsNewEditPossible;
      // true, when delete floating text possible
    property DeletePossible: Boolean read IsDeletePossible;
    property FixPossible: Boolean read IsFixPossible;

      // floating text style properties
    property FTStyle: string read FInfoTextStyle write FInfoTextStyle;
    property FTColor: TColor read FInfoTextColor write FInfoTextColor;
      // background of info window (clNone for none)
    property FTBackColor: TColor read FInfoBkColor write FInfoBkColor;
      // color of border of info window (clNone for none)
    property FTBorderColor: TColor read FInfoFrmColor write FInfoFrmColor;

      // delay time setting
    property DelayTime: Integer read FDelayTime write FDelayTime;
    property ToSuspend: Boolean read FToSuspend write FToSuspend;
  end;

var
  TooltipThread: TTooltipThread;

function CleanFTString(aText: string): string;
function CleanFTStringForInput(aText: string): string;
procedure CheckADOString(var s: string; pn: string);

implementation

uses AM_Proj, AM_Layer, AM_Admin, AM_Child, AM_Main, AM_Obj, DDEDef;

{******************************************************************************+
  Class TTooltipThread
{******************************************************************************}

constructor TTooltipThread.Create;
begin
  inherited Create(True);
  Priority := tpLowest;
  FreeOnTerminate := True;
  FPInfo := nil;
  FTooltipLayer := nil;
  FLastProject := nil;
  FChanged := False;
  FDisplayed := False;
  FRingBuffer := TRingBuffer.Create(int_RingElements);
  FFloatTextList := TList.Create;
  FItem := nil;
  FInfoBkColor := RGB_BackToolTip;
  FInfoTextColor := RGB_TextToolTip;
  FInfoFrmColor := RGB_BorderToolTip;
  FInfoTextStyle := str_FontTooltip;
  FDelayTime := int_DefaultDelay;
  FToSuspend := False;
 {+++Brovak BUG 106 Build 135}
  ShowAllFlag := false;
  ShowSelectedFlag := false;
 {--Brovak}
end;

{******************************************************************************+
  Procedure TTooltipThread.Execute
--------------------------------------------------------------------------------
  Searches for an object beyound the cursor and displays the text information
*******************************************************************************}

procedure TTooltipThread.Execute;
var
  aChild: TMDIChild;
  aStr: string;

  function SearchInRingBuffer(Pos: TDPoint): PIndex;
  var
    i: Integer;
    Item: PIndex;
  begin
    Result := nil;
    for i := 0 to FRingBuffer.Count - 1 do
    begin
      Item := PIndex(FRingBuffer.Items[i]);
      if Item^.SelectByPoint(FPInfo, Pos, ot_Any) <> nil then
      begin
        Result := Item;
        Exit;
      end;
    end;
  end;

  function GetTooltip(X, Y: Integer): string;
  var
    aPos: TDPoint;
    Item: PIndex;
    aText: string;
  begin
    FPInfo^.MsgToDrawPos(X, Y, aPos);
    // look in cache (disabled because of problem when deleting objects)
//    Item:=SearchInRingBuffer(aPos);
    Item := nil;
    // otherwise search in whole tooltip-layer
    if Item = nil then
    begin
      FT_StopSearch := False;
      Item := PLayer(FTooltipLayer)^.BkSearchByPoint(FPinfo, aPos, ot_Any);
      // Save for later (user can stay on (near) this item)
//      if Item<>nil then FRingBuffer.SaveNext(Item);
      // remember some last objects found and check !!
    end;
    aText := '';
    FItem := Item; // remember current object (for edit)
    if (Item <> nil) then
    begin
      // get tooltip-text
//      DDEHandler.SendString('[OGT][' + IntToStr(Item^.Index) + ']');
      case PLayer(FTooltipLayer)^.TooltipInfo.BDEOrLocal of
        int_Local:
          if Item^.Tooltip <> nil then
            aText := Item^.Tooltip^;
        int_BDE: aText := GetBDEInfo(Item^.Index);
        int_DDE: aText := GetDDEInfo(Item^.Index);
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
{++ IDB}
        // Get ToolTip information from the Internal Database.
        int_IDB:
          if WinGISMainForm.WeAreUsingTheIDB[FLastProject] then
            AText := GetIDBInfo(Item.Index);
{-- IDB}
        int_ADO: AText := GetADOInfo(Item.Index);
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
      end;
    end;
//    aText:=aText+'|und'+#10+'nu'+#13+#10+'end';
    // parse control chars
    Result := CleanFTString(aText);
  end;

  function StillOverSameObject: Boolean;
  var
    aPos: TDPoint;
  begin
    Result := False;
    FPInfo^.MsgToDrawPos(FDispX, FDispY, aPos);
    if FItem <> nil then
      Result := (FItem^.SelectByPoint(FPinfo, aPos, ot_Any) = FItem)
  end;

begin
  if FShowForm <> nil then
  begin
    FShowForm.SetPopupMenuStrings;
  end;
  while not Terminated do
  begin
    if FToSuspend then
    begin
      FToSuspend := False;
      if FShowForm <> nil then
      begin
        FShowForm.Hide;
      end;
      Suspend;
    end;
    Sleep(FDelayTime);
    // check current project
    CheckCurrentProject;
    if (FPInfo <> nil) and (FTooltipLayer <> nil) then
      if not PLayer(FToolTiplayer).GetState(sf_LayerOff or sf_LayerOffAtGen) then
      begin
        if (FChanged) then
        begin
          FChanged := False;
        // search for object (first in tooltip-cache, then in project)
        // and return string for tooltip ('' means no object/tooltip found)
          aStr := GetTooltip(FMouseX, FMouseY);
        // only display if found something
          aChild := TWinGISMainForm(Application.MainForm).GetCurrentChild;
          if (aStr <> '') and (aChild <> nil) then
          begin
            FDispX := FMouseX; // remember position
            FDispY := FMouseY;
            if StillOverSameObject then
            begin
              with FShowForm do
              begin
                if TooltipText <> aStr then
                  Hide;
                MoveTo({aChild.ClientOrigin.X+}FDispX, {aChild.ClientOrigin.Y+} FDispY);
                TooltipText := aStr;
                Show; // show floating text
                TooltipText := aStr;
              end;
              FDisplayed := True; // floating text is displayed
            end;
          end
          else
          begin
            if (FDisplayed) then
            begin
            // hide tooltip window
              FShowForm.Hide;
              FDisplayed := False;
            end;
          end; { if }
        end; { if }
      end
      else
        FShowForm.Hide;
  end; { while }

end;

procedure TTooltipThread.CheckCurrentProject;
var
  aProject: PProj;
  aChild: TMDIChild;
begin
  // if application is closed while tooltip-thread running...
  if Application.MainForm = nil then
  begin
    EndFloatingText;
    Exit;
  end;
  aChild := TWinGISMainForm(Application.MainForm).GetCurrentChild;
  // any child present?
  if aChild <> nil then
  begin
    aProject := aChild.Data;
    // still in same project ?
    if aProject = FLastProject then
      Exit;
    if aProject <> nil then
      // tooltips enabled for "new" current project ?
      if aProject^.TooltipLayer <> nil then
      begin
        StartFloatingText(aProject);
        Exit;
      end;
  end;
  // not any child then end thread
  EndFloatingText;
end;

function TTooltipThread.GetBDEInfo(Index: LongInt): string;
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  RESULT := WinGISMainForm.DTI_Man.GetGeoText(PLayer(FTooltipLayer).TooltipInfo.BDETable,
    Index,
    PLayer(FTooltipLayer)^.TooltipInfo.IDColName,
    PLayer(FTooltipLayer)^.TooltipInfo.FTColName);
{$ENDIF} // <----------------- AXDLL
end;

{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
{++ IDB}

function TTooltipThread.GetIDBInfo(Index: LongInt): string;
begin
  RESULT := '';
  if not WinGISMainForm.WeAreUsingTheIDB[FLastProject] then
    EXIT;
  CoInitialize(0);
  try
    RESULT := WinGISMainForm.IDB_Man.GetGeoText(SELF.FLastProject,
      PLayer(FTooltipLayer).Index,
      Index {,
                                                 PLayer(FTooltipLayer).TooltipInfo.IDColName,
                                                 PLayer(FTooltipLayer).TooltipInfo.FTColName});
  finally
    CoUninitialize;
  end;
end;
{-- IDB}

function TTooltipThread.GetADOInfo(Index: LongInt): string;
begin
  RESULT := WinGISMainForm.DTI_Man.ADO_GetGeoText(PLayer(FTooltipLayer).TooltipInfo.BDETable,
    Index,
    PLayer(FTooltipLayer).TooltipInfo.IDColName,
    PLayer(FTooltipLayer).TooltipInfo.FTColName);
end;
{$ENDIF} // <----------------- AXDLL
{$ENDIF}

function TTooltipThread.GetDDEInfo
  (
  Index: LongInt
  ): string;
var
  DDEString: string;
  TmpStr: string;
begin
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
  Result := '';
  // Result:='wait for dde...';
  // if already sent/answered dde request use saved answer
  if FDDEIndex = Index then
    Result := FDDEResult
  // otherwise store request and set to void answer
  // and send dde request
  else
  begin
    try
      FDDEIndex := Index;
      FDDEResult := '';
      if DDEHandler.FLayerInfo = 2 then
      begin
        Str(pProj(Project)^.Layers^.TopLayer^.Index: 10, TmpStr);
        DDEString := '[LFT][' + TmpStr + '][';
      end
      else
        if DDEHandler.FLayerInfo = 1 then
          DDEString := '[LFT][' + pProj(Project)^.Layers^.TopLayer^.Text^ + ']['
        else
          DDEString := '[LFT]['; {if DDEHandler.FLayerInfo = 0}
      Str(Index: 10, TmpStr);
      DDEString := DDEString + TmpStr + ']';
      FChanged := False;
      DDEHandler.SendString(DDEString);
       // just for test
      DDEGeoTextReady;
    finally
    end;
  end;
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
end;

procedure TTooltipThread.DDEGeoTextReady;
var
  DDEString,
    Cmd,
    ID,
    Text: string;
  i: Integer;
  Index: LongInt;
  Err: Integer;
// parse DDE command [SFT][ID][Text]
begin
  // no data available
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
  for i := 0 to 100 do
  begin
    if DDEHandler.DDEData^.GetCount > 0 then
    begin
      break;
    end;
    Sleep(10);
  end;
  if DDEHandler.DDEData^.GetCount = 0 then
  begin
    Exit;
  end;
  DDEString := StrPas(DDEHandler.DDEData^.At(0));
  if DDEHandler.DDEData^.GetCount > 0 then
    PChar(DDEHandler.DDEData^.At(0))[0] := DDEStrUsed;
  DDEHandler.DeleteDDEData;
//  DDEString:='[SFT][1][DDE-GeoText]';
  Cmd := Copy(DDEString, 1, 5);
  // check for right DDE command
  if Cmd <> '[SFT]' then
    Exit;
  DDEString := Copy(DDEString, 6, Length(DDEString));
  i := Pos('][', DDEString);
  if i = 0 then
    Exit;
  ID := Copy(DDEString, 2, i - 2);
  Text := Copy(DDEString, i + 2, Length(DDEString) - i - 2);
  Val(ID, Index, err);
  if err = 0 then
  begin
    FDDEIndex := Index;
    FDDEResult := Text;
    FChanged := True;
  end;
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
end;

procedure TTooltipThread.StartFloatingText
  (
  aProject: Pointer
  );
var
  bF: Boolean;
begin
  // setup internal variables for correct running
  FLastProject := aProject;
  FTooltipLayer := pProj(aProject)^.TooltipLayer;
  FPInfo := pProj(aProject)^.PInfo;
  // clear internal caches
  FRingBuffer.Clear;
  FFloatTextList.Clear;

  FShowForm := TTooltipShowForm.CreateParented(PProj(aProject).Parent.Handle);

//  SetParent(FShowForm.Handle,PProj(aProject).Parent.Handle);
  if FTooltipLayer <> nil then
    // resume(start) tooltipthread
    Resume;
end;

procedure TTooltipThread.EndFloatingText;
begin
  if Assigned(FShowForm) then
  begin
    FShowForm.Free;
    FShowForm := nil;
  end;
  FLastProject := nil;
  FTooltipLayer := nil;
  FPInfo := nil;
  // clear internal caches
  FRingBuffer.Clear;
  FFloatTextList.Clear;
  // be sure that tooltiothread is in Sleep(...)
  SureSuspend;
end;

procedure TTooltipThread.SureSuspend;
begin
  if Assigned(FShowForm) then
  begin
    FShowForm.Hide;
  end;
  if not Suspended then
  begin
    ToSuspend := True;
    FT_StopSearch := True; // stop searching immediately
  end;
end;

procedure TTooltipThread.Mouse
  (
  Proj: Pointer;
  X: Integer;
  Y: Integer
  );
begin
  // only if mouse coords from RIGHT (active) project
  if Proj = FLastProject then
  begin
    MouseX := X;
    MouseY := Y;
  end;
end;

procedure TTooltipThread.SetMouseX
  (
  MouseX: Integer
  );
begin
  if FMouseX <> MouseX then
  begin
    FChanged := True;
    FT_StopSearch := True;
    FMouseX := MouseX;
  end;
end;

procedure TTooltipThread.SetMouseY
  (
  MouseY: Integer
  );
begin
  if FMouseY <> MouseY then
  begin
    FChanged := True;
    FT_StopSearch := True;
    FMouseY := MouseY;
  end;
end;

procedure TTooltipThread.SetProject
  (
  aProject: Pointer
  );
begin
  FLastProject := aProject;
  FTooltipLayer := pProj(aProject)^.TooltipLayer;
  FPInfo := pProj(aProject)^.PInfo;
end;

procedure TTooltipThread.FixFloatingText;
var
  aFTInfo: TFloatingTextInfo;
begin
  aFTInfo := TFloatingTextInfo.Create;
  with aFTInfo do
  begin
  // calculate text position
    FPInfo^.MsgToDrawPos(FDispX + 4, FDispY - FShowForm.Height, FTextPos);
    FText := NewStr(FShowForm.TooltipText);
    FIndex := FItem^.Index;
  end;

  FFloatTextList.Add(aFTInfo);
  FPInfo^.Extcanvas.CachedDraw := False;
  DrawFloatingText(FPInfo, aFTInfo);
  FPInfo^.Extcanvas.CachedDraw := True;
end;

procedure TTooltipThread.GetFloatingTextRect
  (
  aPInfo: pPaint;
  FTextPos: TDPoint;
  TextSize: TSize;
  var Rect: array of TPoint
  );
var
  aGrPos: TGrPoint;
begin
  with aPInfo^ do
  begin
    ConvertToDispDouble(FTextPos, aGrPos.X, aGrPos.Y);
    Rect[0].X := Round(aGRPos.X - int_UR_x);
    Rect[0].Y := Round(aGRPos.Y + int_UR_y);
    Rect[1].X := Round(aGRPos.X + TextSize.cx + int_LR_x);
    Rect[1].Y := Rect[0].Y;
    Rect[2].X := Rect[1].X;
    Rect[2].Y := Round(aGRPos.Y - TextSize.cy - int_LR_y);
    Rect[3].X := Rect[0].X;
    Rect[3].Y := Rect[2].Y;
    Rect[4].X := Rect[0].X;
    Rect[4].Y := Rect[0].Y;
  end;
end;

procedure TTooltipThread.DBGeoText(AItem: PIndex; aText: string);
begin
  ChangeFloatingText(AItem, aText);
end;

procedure TTooltipThread.DrawFloatingText
  (
  aPInfo: pPaint;
  aFTInfo: TFloatingTextInfo
  );
var
  aGrPos: TGrPoint;
  aPoint: TPoint;
  Rect: array[0..4] of TPoint;
  LineStrs: TStringList;
  i: Integer;

  function MultiLineTextExtent
      (
      aText: string;
      var Lines: TStringList
      ): TSize;
  var
    aSize,
      FullSize: TSize;
    aStr: string;
    Stop: Integer;
  begin

    // if only one line, be simple...
    if Pos(#13, aText) = 0 then
    begin
      Result := aPInfo^.ExtCanvas.TextExtent(@aText[1], Length(aText));
      Lines.Add(aText);
      Exit;
    end;

    FullSize.cx := 0;
    FullSize.cy := 0;

    while (Length(aText) > 0) do
    begin
      // search for next line wrap
      Stop := Pos(#13, aText);
      if Stop = 0 then
        Stop := Length(aText) + 1;
      // get this line
      aStr := Copy(aText, 1, Stop - 1);
      Lines.Add(aStr);
      // empty lines have same size
      if Length(aStr) = 0 then
        aStr := ' ';
      // calculate size of this line
      aSize := aPInfo^.ExtCanvas.TextExtent(@aStr[1], Length(aStr));
      // calculate resulting size
      FullSize.cx := Max(FullSize.cx, aSize.cx);
      FullSize.cy := FullSize.cy + aSize.cy;
      // go for next line
      aText := Copy(aText, Stop + 1, Length(aText));
    end;
    Result := FullSize;
  end;

begin
  LineStrs := TStringList.Create;
  with aPInfo^, ExtCanvas, aFTInfo do
  begin
    // calculate textpos
    ConvertToDispDouble(FTextPos, aGrPos.X, aGrPos.Y);

    Push;
    CachedDraw := False;
    try

      // for same text size under Win95/98/NT
      APoint := Point(0, FShowForm.Font.Size);
      DPToLP(Handle, APoint, 1);

      // setup font
      Font.Angle := 0;
      Font.Name := FInfoTextStyle;
      //Font.Size:=45;
      Font.Size := Abs(APoint.Y);
      Font.Color := FInfoTextColor;
      Font.Style := 0;
      // setup brush
      Brush.Style := ptSolid;
      Brush.BackColor := clNone; // Text transparent !!
      Font.Alignment := taLeft;
      if FInfoFrmColor = -1 then
        Pen.Style := ord(psClear)
      else
        Pen.Color := FInfoFrmColor; // Border of tooltip
      if FInfoBKColor = -1 then
        Brush.Style := ptNoPattern // no info background
      else
        Brush.ForeColor := FInfoBkColor; // fillcolor of tooltip
      // get text size (the real one and care of multilines)
      FSize := MultiLineTextExtent(FText^, LineStrs);
      // incarnate bounding rect to match displayed form
      GetFloatingTextRect(aPInfo, FTextPos, FSize, Rect);
      // draw bounding rect
      Polygon(Rect, 4, false);
      // draw  floating text (each line separately!!!)
      for i := 0 to LineStrs.Count - 1 do
      begin
        TextOut(aGrPos, @(LineStrs.Strings[i])[1], Length(LineStrs.Strings[i]));
        aGrPos.y := aGrPos.y - 41;
      end;
    finally
      // restore previous settings
      Pop;
    end;
  end;
  LineStrs.Free;
end;

procedure TTooltipThread.DrawFloatingTextsForObject
  (
  aPInfo: pPaint;
  Index: LongInt
  );
var
  i: Integer;
  aFTInfo: TFloatingTextInfo;
begin
  with FFloatTextList do
    for i := 0 to Count - 1 do
    begin
      aFTInfo := TFloatingTextInfo(Items[i]);
      if aFTInfo.FIndex = Index then
        DrawFloatingText(aPInfo, aFTInfo);
    end;
end;

procedure TTooltipThread.DrawFixedFloatingTexts
  (
  aPInfo: pPaint
  );
var
  i: Integer;
  aFTInfo: TFloatingTextInfo;
begin
  with FFloatTextList do
    for i := 0 to Count - 1 do
    begin
      aFTInfo := TFloatingTextInfo(Items[i]);
      DrawFloatingText(aPInfo, aFTInfo);
    end;
end;

procedure TTooltipThread.ClearFixedFloatingTexts;
begin
  FFloatTextList.Clear;
end;

procedure TTooltipThread.ChangeFloatingText
  (
  aItem: pIndex;
  NewFT: string
  );
begin
  if aItem <> nil then
  begin
    with pLayer(FTooltipLayer)^.TooltipInfo do
      case BDEOrLocal of
        int_Local:
          begin
            DisposeStr(FItem^.Tooltip);
            aItem^.Tooltip := nil;
            if NewFT <> '' then
              aItem^.Tooltip := NewStr(NewFT);
            PProj(Project)^.SetModified; // project has been modified
          end;
{$IFNDEF AXDLL} // <----------------- AXDLL
        int_BDE: WinGISMainForm.DTI_Man.SetGeoText(PLayer(FToolTipLayer).TooltipInfo.BDETable,
            AItem.Index,
            PLayer(FToolTipLayer).TooltipInfo.IDColName,
            PLayer(FToolTipLayer).TooltipInfo.FTColName,
            NewFT);
{$ENDIF} // <----------------- AXDLL
                    // only db has changed !!
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
{++ IDB}
        int_IDB:
          begin
            if WinGISMainForm.WeAreUsingTheIDB[FLastProject] then
              WinGISMainForm.IDB_Man.SetGeoText(FLastProject,
                PLayer(FTooltipLayer).Index,
                AItem.Index,
                                                         {PLayer(FTooltipLayer).TooltipInfo.IDColName,
                                                         PLayer(FTooltipLayer).TooltipInfo.FTColName,}
                NewFT);
          end;
{-- IDB}
        int_ADO:
          begin
            WinGISMainForm.DTI_Man.ADO_SetGeoText(PLayer(FToolTipLayer).TooltipInfo.BDETable,
              AItem.Index,
              PLayer(FToolTipLayer).TooltipInfo.IDColName,
              PLayer(FToolTipLayer).TooltipInfo.FTColName,
              NewFT);
          end;
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
      end;
    ChangeFloatingTextForObject(aItem^.Index, NewFT);
    with pProj(Project)^.Layers^, FPinfo^.ExtCanvas do
    begin
      DrawFromCache(ClipRect);
      SelLayer^.Draw(FPInfo);
      DrawFixedFloatingTexts(FPInfo);
    end;
  end;
end;

procedure TTooltipThread.ChangeFloatingTextForObject
  (
  Index: LongInt;
  NewFT: string
  );
var
  i: Integer;
  aFTInfo: TFloatingTextInfo;
begin
  NewFT := CleanFTString(NewFT);
  with FFloatTextList do
    for i := 0 to Count - 1 do
    begin
      aFTInfo := TFloatingTextInfo(Items[i]);
      if aFTInfo.FIndex = Index then
      begin
        DisposeStr(aFTInfo.FText);
        aFTInfo.FText := NewStr(NewFT);
      end;
    end;
end;

procedure TTooltipThread.ShowAllFloatingTexts(Flag: boolean);
begin
  with pLayer(FTooltipLayer)^ do
    case TooltipInfo.BDEOrLocal of
      int_Local: ShowAllFTLocal(FTooltipLayer, Flag);
      int_BDE: ShowAllFTBDE(FTooltipLayer, Flag);
    end;
  with pProj(Project)^.Layers^, FPinfo^.ExtCanvas do
  begin
    DrawFromCache(ClipRect);
    SelLayer^.Draw(FPInfo);
    DrawFixedFloatingTexts(FPInfo);
  end;
end;

procedure TTooltipThread.ShowSelectedFloatingTexts(Flag: boolean);
begin
  with pProj(Project)^.Layers^ do
  begin
    if TopLayer <> FTooltipLayer then
    begin
      MessageDialog(MlgStringList[MlgSectionName, 1], mtError, [mbOk], 0);
      Exit;
    end;
    with pLayer(FTooltipLayer)^ do
      case TooltipInfo.BDEOrLocal of
        int_Local: ShowAllFTLocal(SelLayer, Flag);
        int_BDE: ShowAllFTBDE(SelLayer, Flag);
      end;
    with FPinfo^.ExtCanvas do
    begin
      DrawFromCache(ClipRect);
      SelLayer^.Draw(FPInfo);
      DrawFixedFloatingTexts(FPInfo);
    end;
  end;
end;

function TTooltipThread.CreateFTInfo
  (
  aItem: pIndex;
  aText: string
  ): TFloatingTextInfo;
var
  aFTInfo: TFloatingTextInfo;
  aPos: TDPoint;
  aPoint: TPoint;
  aWidth,
    aHeight: Integer;
begin
  aFTInfo := TFloatingTextInfo.Create;
  // build FT info for drawing
  with FPInfo^, aFTInfo, aItem^.ClipRect do
  begin
    FText := NewStr(aText);
    FIndex := aItem^.Index;
     // get text size
    FShowForm.SetTooltipText(FText^);
    aWidth := FShowForm.Width;
    aHeight := FShowForm.Height;
     // convert center point to screen (physical) coordinates
    aPos.Init(Round(CenterX), Round(CenterY));
    ProjPointToPhysPoint(aPos, aPoint);
     // align text
    FPInfo^.MsgToDrawPos(aPoint.X - aWidth div 2,
      aPoint.Y - aHeight div 2, aPos);
     // set text position
    FTextPos.Init(aPos.X, aPos.Y);
  end;
  Result := aFTInfo;
end;

procedure TTooltipThread.ShowAllFTLocal
  (
  aLayer: pointer; Flag: boolean
  );
var
  i: LongInt;
  aFTInfo: TFloatingTextInfo;
  aItem: pIndex;
begin
  FFloatTextList.Clear;
  with pLayer(aLayer)^ do
    for i := 0 to Data^.GetCount - 1 do
    begin
      aItem := pIndex(Data^.At(i));
      // get "real" complete object
      aItem := IndexObject(FPInfo, aItem);
      if (aItem <> nil) then
        if aItem^.Tooltip <> nil then
   {++ changed-and-added by Brovak BUG 106 Build 136}
          if Flag = true then
          begin;
            aFTInfo := CreateFTInfo(aItem, aItem^.Tooltip^);
               // add to list for display in one drawing circle
            FFloatTextList.Add(aFTInfo);
          end
          else
          begin;
            ChangeFloatingTextForObject(aitem.Index, '');
          end;
    {--Brovak}
    end;
end;

procedure TTooltipThread.ShowAllFTBDE
  (
  aLayer: pointer; Flag: boolean
  );
var
  i: LongInt;
  aFTInfo: TFloatingTextInfo;
  aItem: pIndex;
  aText: string;
begin
  FFloatTextList.Clear;
  with pLayer(aLayer)^ do
    for i := 0 to Data^.GetCount - 1 do
    begin
      aItem := pIndex(Data^.At(i));
      aText := GetBDEInfo(aItem^.Index);
      if aText <> '' then
      begin
     {++ changed-and-added by Brovak BUG 106 Build 136}
        if Flag = true then
        begin;
          aFTInfo := CreateFTInfo(aItem, aText);
        // add to list for display in one drawing circle
          FFloatTextList.Add(aFTInfo);
        end
        else
        begin;
          ChangeFloatingTextForObject(aitem.Index, '');
        end;
     {--Brovak}
      end;

    end;
end;

procedure TTooltipThread.ChangeCurrentFloatingText;
var
  aStr,
    bStr: string;
  FTId: Integer;
  aFTInfo: TFloatingTextInfo;
  DefVal: Boolean;
begin
  with FShowForm do
  begin
    // not a hint why InputQuery doesn't work ?!?!
    bStr := '';
    DefVal := FALSE;
    FTId := IsMouseOverFT;
    if (FTId = -1) and FDisplayed then
      bStr := TooltipText;
    if (FTId <> -1) then
    begin
      aFTInfo := FFloatTextList.Items[FTId];
      bStr := aFTInfo.FText^;

      {Item:=PLayer(FTooltipLayer)^.BkSearchByPoint(FPinfo,aPos,ot_Any);}
      { FItem  }

    end;
    // convert #13 into | pipes
    bStr := CleanFTStringForInput(bStr);

    // if show default value
    with PLayer(FTooltipLayer)^.TooltipInfo do
    begin
      if (BDEOrLocal = int_Local) and (FillAuto = int_Deflt) and (AutoText > int_Manu) then
      begin
        bStr := pProj(Project)^.DoGetTooltipText(FItem, FItem^.GetObjType, FillAuto);
        DefVal := TRUE;
      end;
    end;
    aStr := InputBox(MlgStringList[MlgSectionName, 2], MlgStringList[MlgSectionName, 3],
      bStr);
    if (bStr <> aStr) or DefVal then
    begin
      ChangeFloatingText(FItem, aStr);
      aStr := CleanFTString(aStr);
      if (FTId = -1) then
        TooltipText := aStr;
    end;
  end;
end;

//Brovak Bug 106 Set/Change Geotext's text for selected objects

procedure TTooltipThread.ChangeCurrentFloatingTextForSelected;
var
  aStr, astr1,
    bStr: AnsiString;
  FTId: Integer;
  aFTInfo: TFloatingTextInfo;
  DefVal: Boolean;
  I: longInt;
  AITem: Pindex;
begin

//  Alayer:=  pProj(Project)^.Layers^.SelLayer; //Selected
  with pProj(Project)^.Layers^ do
  begin
    if TopLayer <> FTooltipLayer then
    begin
      MessageDialog(MlgStringList[MlgSectionName, 1], mtError, [mbOk], 0);
      Exit;
    end;
  end;
  with FShowForm do
  begin
    bStr := '';
    DefVal := FALSE;
    FTId := IsMouseOverFT;
    if (FTId = -1) and FDisplayed then
      bStr := TooltipText;
    if (FTId <> -1) then
    begin
      aFTInfo := FFloatTextList.Items[FTId];
      bStr := aFTInfo.FText^;
    end;
    // convert #13 into | pipes
    bStr := CleanFTStringForInput(bStr);

    // if show default value
    with PLayer(FTooltipLayer)^.TooltipInfo do
    begin
      if (BDEOrLocal = int_Local) and (FillAuto = int_Deflt) and (AutoText > int_Manu) then
      begin
        bStr := pProj(Project)^.DoGetTooltipText(FItem, FItem^.GetObjType, FillAuto);
        DefVal := TRUE;
      end;
    end;

    if InputQuery(MlgStringList[MlgSectionName, 7], MlgStringList[MlgSectionName, 3], bstr) = true then
    begin;
      astr := bstr;
      astr1 := CleanFtString(astr);
      with Player(pProj(Project)^.Layers^.SelLayer)^ do
        for i := 0 to Data^.Getcount - 1 do
        begin;
          AItem := PIndex(Data^.At(i));
          AItem := IndexObject(FPInfo, Aitem);
          if (Aitem <> nil) then
          begin;
            FItem := Aitem;
            ChangeFloatingText(FItem, aStr);
          end;
        end;
      if (FTId = -1) then
        TooltipText := aStr1;
    end;
  end;
end;
{--Brovak}

procedure TTooltipThread.DeleteCurrentFloatingText;
var
  FTId: Integer;
//    aFTInfo : TFloatingTextInfo;
begin
  FTId := IsMouseOverFT;
  if FTId <> -1 then
  begin
    if MessageDialog(MlgStringList[MlgSectionName, 6], mtConfirmation,
      [mbYes, mbNo], 0) = mrYes then
    begin
//      aFTInfo:=FFloatTextList.Items[FTId];  perhaps latter for "better" redrawing
      FFloatTextList.Delete(FTId);
      with pProj(Project)^.layers^, FPinfo^.ExtCanvas do
      begin
        DrawFromCache(ClipRect);
        SelLayer^.Draw(FPInfo);
        DrawFixedFloatingTexts(FPInfo);
      end;
    end;
  end;
end;

function TTooltipThread.IsFTDisplayed: Boolean;
begin
  Result := FDisplayed or (IsMouseOverFT <> -1) or (FItem <> nil);
end;

function TTooltipThread.IsNewEditPossible: Boolean;
begin
  Result := FDisplayed or (IsMouseOverFT <> -1) or (FItem <> nil);
end;

function TTooltipThread.IsDeletePossible: Boolean;
begin
  Result := (IsMouseOverFT <> -1);
end;

function TTooltipThread.IsFixPossible: Boolean;
begin
  Result := FDisplayed;
end;

function TTooltipThread.IsMouseOverFT: Integer;
var
  i: LongInt;
  aFTInfo: TFloatingTextInfo;
  aRect: TDRect;
  MousePos: TDPoint;
  TextPos,
    MPos: TGrPoint;
begin
  Result := -1;
  if FPInfo = nil then
    Exit;
  aRect.Init;
  FPInfo^.MsgToDrawPos(FMouseX, FMouseY, MousePos);
  FPInfo^.ConvertToDispDouble(MousePos, MPos.X, MPos.Y);

  MousePos.X := Round(MPos.X);
  MousePos.Y := Round(MPos.Y);

  with FFloatTextList do
    for i := 0 to Count - 1 do
    begin
      aFTInfo := TFloatingTextInfo(Items[i]);
      with aFTInfo do
      begin
        FPInfo^.ConvertToDispDouble(FTextPos, TextPos.X, TextPos.Y);
        aRect.A.Init(Round(TextPos.X - int_UR_x),
          Round(TextPos.Y - int_LR_y - FSize.cy));
        aRect.B.Init(Round(TextPos.X + int_LR_x + FSize.cx),
          Round(TextPos.Y + int_UR_y));
      end;
      if aRect.PointInside(MousePos) then
      begin
        Result := i;
        Exit;
      end;
    end;
end;

destructor TToolTipThread.Destroy;
begin
//  Suspend;
//  EndSession;
//  FShowForm.Free;
  FRingBuffer.Clear;
  FRingBuffer.Free;
  FFloatTextList.Clear;
  FFloatTextList.Free;
  inherited Destroy;
end;

{******************************************************************************+
  Class TRingBuffer
*******************************************************************************}

constructor TRingBuffer.Create
  (
  Elements: Integer
  );
begin
  inherited Create;
  FElements := Elements;
  FCurrent := 0;
end;

procedure TRingBuffer.SaveNext
  (
  Element: Pointer
  );
begin
  if Count < FElements then
  begin
    // Neue Elemente anfordern
    Add(Element);
  end
  else
  begin
    // replace oldest entry
    Items[FCurrent] := Element;
    Inc(FCurrent);
    if FCurrent >= FElements then
      FCurrent := 0;
  end;
end;

{******************************************************************************+
  Class TTooltipInfo
*******************************************************************************}

constructor TTooltipInfo.Create;
begin
  inherited Create;
  BDEOrLocal := int_None;
  BDETable := '';
  IDColName := '';
  FTColName := '';
end;

procedure TTooltipInfo.SetBDE
  (
  aTable: string;
  aIDCol: string;
  aFTCol: string
  );
begin
  BDEOrLocal := int_BDE;
  BDETable := aTable;
  IDColName := aIDCol;
  FTColName := aFtCol;
end;

procedure TTooltipInfo.SetLocal;
begin
  BDEOrLocal := int_Local;
  BDETable := '';
  IDColName := '';
  FTColName := '';
end;

{++ IDB}

procedure TTooltipInfo.SetIDB(aTable: string; aIDCol: string; aFTCol: string);
begin
  BDEOrLocal := int_IDB;
  BDETable := ATable;
  IDColName := AIDCol;
  FTColName := AFTCol;
end;
{-- IDB}

procedure TTooltipInfo.SetADO(ATable: AnsiString; AIDCol: AnsiString; AFTCol: AnsiString);
begin
  BDEOrLocal := int_ADO;
  BDETable := ATable;
  IDColName := AIDCol;
  FTColName := AFTCol;
end;

procedure TTooltipInfo.SetDDE;
begin
  BDEOrLocal := int_DDE;
  BDETable := '';
  IDColName := '';
  FTColName := '';
end;

procedure TTooltipInfo.SetNone;
begin
  BDEOrLocal := int_None;
  BDETable := '';
  IDColName := '';
  FTColName := '';
end;

procedure TTooltipInfo.SetFillAuto;
begin
  FillAuto := int_Auto;
end;

procedure TTooltipInfo.SetFillAutoSelected;
begin
  FillAuto := int_Sel;
end;

procedure TTooltipInfo.SetShowDefault;
begin
  FillAuto := int_Deflt;
end;

procedure TTooltipInfo.SetFillNone;
begin
  FillAuto := int_ANone;
end;

procedure TTooltipInfo.CopyFrom
  (
  Source: TTooltipInfo
  );
begin
  BDEOrLocal := Source.BDEOrLocal;
  BDETable := Source.BDETable;
  IDColName := Source.IDColName;
  FTColName := Source.FTColName;
end;

function TTooltipInfo.IsDifferent
  (
  Other: TTooltipInfo
  ): Boolean;
begin
  Result := (BDEOrLocal <> Other.BDEOrLocal) or
    (BDETable <> Other.BDETable) or
    (IDColName <> Other.IDColName) or
    (FTColName <> Other.FTColName);
end;

{******************************************************************************+
  Class TFloatingTextInfo
*******************************************************************************}

constructor TFloatingTextInfo.Create;
begin
  inherited Create;
  FText := nil;
end;

destructor TFloatingTextInfo.Destroy;
begin
  DisposeStr(FText);
  inherited Destroy;
end;

{*******************************************************************************
Some String routines
*******************************************************************************}

{*******************************************************************************
I CleanFTString                                                                I
+------------------------------------------------------------------------------+
I Converts all sequences #13#10 #10#13 and all Chars in LinebreakChars (look   I
I above) into #13 and returns the string                                       I
*******************************************************************************}

function CleanFTString(aText: string): string;
var
  Start: Integer;
begin
  // clean any #13#10 or #10#13 sequences to #13
  repeat
    Start := Pos(#13 + #10, aText);
    if Start > 0 then
      Delete(aText, Start + 1, 1);
  until (Start = 0);
  repeat
    Start := Pos(#10 + #13, aText);
    if Start > 0 then
      Delete(aText, Start + 1, 1);
  until (Start = 0);
    // clean any control characters to #13
  for Start := 1 to Length(aText) do
    if aText[Start] in LineBreakChars then
      aText[Start] := #13;
  Result := aText;
end;

{*******************************************************************************
I CleanFTStringForInput                                                        I
+------------------------------------------------------------------------------+
I Converts all #13 into | pipes for proper input                               I
*******************************************************************************}

function CleanFTStringForInput(aText: string): string;
var
  Start: Integer;
begin
  // clean any #13#10 or #10#13 sequences to #13
  repeat
    Start := Pos(#13 + #10, aText);
    if Start > 0 then
      Delete(aText, Start + 1, 1);
  until (Start = 0);
  repeat
    Start := Pos(#10 + #13, aText);
    if Start > 0 then
      Delete(aText, Start + 1, 1);
  until (Start = 0);
    // clean any control characters to '|'
  for Start := 1 to Length(aText) do
    if aText[Start] in LineBreakChars then
      aText[Start] := '|';
  Result := aText;
end;

procedure CheckADOString(var s: string; pn: string);
var
  sa: array[1..20] of string;
  i, n: Integer;
  Filename: AnsiString;
  NewPath: AnsiString;
begin
  n := ExtractStringItems(sa, s, '|');
  if n > 0 then
  begin
    for i := 0 to n do
    begin
      Filename := sa[i];
      if Pos('\', Filename) > 0 then
      begin
        if not FileExists(Filename) then
        begin
          Filename := ExtractFileName(Filename);
          NewPath := ExtractFilePath(pn);
          Filename := NewPath + FileName;
          s := StringReplace(s, sa[i], Filename, []);
        end;
        break;
      end;
    end;
  end;
end;

{+++Brovak Bug 106 BUILD  136}

function TTooltipThread.IsExistSelected: boolean;
begin;
  Result := false;
  if pProj(Project)^.Layers^.SelLayer.Data.GetCount > 0 then
    Result := true;
end;
{--Brovak}

{******************************************************************************+
  Initialization
*******************************************************************************}

procedure TToolTipInfo.AssignTo(Dest: TPersistent);
begin
  with Dest as TTooltipInfo do
  begin
    BDEOrLocal := Self.BDEOrLocal;
    BDETable := Self.BDETable;
    IDColName := Self.IDColName;
    FTColName := Self.FTColName;
  end;
end;

initialization
  begin
  // Tooltip-Thread erzeugen, aber noch nicht starten!
    TooltipThread := TTooltipThread.Create;
  end;

finalization
  begin
  end;

end.

