unit Startscr;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, ExtCtrls, StdCtrls;

type TStartScreen = class(TForm)
       Panel1: TPanel;
       Timer: TTimer;
       Image: TImage;                  
       Label1: TLabel;
       Label3: TLabel;
       Button1: TButton;
       Label2: TLabel;
       Label4: TLabel;
       Label5: TLabel;
       Procedure TimerTimer(Sender: TObject);
       Procedure FormClose(Sender: TObject; var Action: TCloseAction);
       Procedure Button1Click(Sender: TObject);
      Private
      Public
     end;

var StartScreen: TStartScreen;

Implementation

{$R *.DFM}

Procedure TStartScreen.TimerTimer(Sender: TObject);
begin
  Close;  
end;

Procedure TStartScreen.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action:=caFree;
  StartScreen:=NIL;
end;


Procedure TStartScreen.Button1Click(Sender: TObject);
begin
  Close;
end;

end.
