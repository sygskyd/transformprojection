{******************************************************************************+
| Unit SymLib                                                                  |
| Implementiert Symbolinfo f�r sortierte Anzeige im SymbolRollup               |
| 11.09.97 Raimund Leitner                                                     |
+******************************************************************************}
unit Symlib;

interface

uses SysUtils,WinTypes,WinProcs,Messages,Classes,Graphics,Controls,Forms,Dialogs,
     StdCtrls,ExtCtrls,WinDOS,AM_Proj,AM_Group,AM_Obj,AM_Paint,AM_Def,SymEdit,
     SymbolPropertyDlg,UserIntf,ExtLib;

const mrEdit = 102;

type
  TSymInfo = class
  private
    FSymbols    : PSymbols;
    FExtLib     : PExtLib;
    FSymList    : TList;
    FIndexSym   : Integer;
    FLibName    : PString;
    function    GetSymCount:Integer;
    procedure   SetIndex(Index:Integer);
    function    GetLibName:String;
    function    IGetSym:PSGroup;
  public
    constructor Create(Symbols:PSymbols;ExtLib:PExtLib;LibName:PString;OnlyUsed:Boolean=False);
    constructor AltCreate(Symbols:PSymbols;ExtLib:PExtLib;LibName:PString);

    function    GetSym(Index:Integer):PSGroup;
    function    Delete(Symbol:PSGroup):Boolean;
    procedure   AddSym(Symbol:PSGroup);
    procedure   Sort;
    function    Valid(Index:Integer):Boolean;

    function    IndexOf(aSym:pSGroup):Integer; {++ IDB} overload;
                // I added this function to find a symbol not only by name but also
                // by name. Ivanoff.
    function    IndexOf(sSymbolName: AnsiString):Integer; overload;
    {-- IDB}

    destructor  Destroy; override;

    property    SymIndex:Integer read FIndexSym write SetIndex;
    property    SymCount:Integer read GetSymCount;
    property    Sym:PSGroup      read IGetSym;
    property    LibName:String   read GetLibName;
    property    Lib:PSymbols     read FSymbols;
    property    ExtLib:PExtLib   read FExtLib;
  end;

  TSymLibInfo = class
  private
    FProject    : PProj;
    FProjSymbols: PSymbols;
    FSymLibList : TList;
    FIndexLib   : Integer;
    FProjStr    : PString;
    function    GetSymIndex:Integer;
    procedure   SetSymIndex(Index:Integer);
    function    GetSymCount:Integer;
    function    IGetSym:PSGroup;
    procedure   SetLibIndex(Index:Integer);
    function    GetLibCount:Integer;
    function    IGetLib:PSymbols;
    function    IGetExtLib:PExtLib;
    function    IGetLibName:String;
  public
    constructor Create(Project:pProj;ProjSymbols:PSymbols;ProjStr:String);

    function    GetLib(Index:Integer):PSymbols;
    function    GetExtLib(Index:Integer):PExtLib;
    function    GetLibName(Index:Integer):String;
    function    GetSymInfo:TSymInfo; {++ IDB} overload;
                // I added the function to return TSymInfo not only of currect library
                // but also of any one. Ivanoff.
    Function    GetSymInfo(iLibraryNumber: Integer): TSymInfo; overload;
    {-- IDB}

    function    IndexOf(aSym:pSGroup):Integer;

    procedure   UpdateExtLib;
    procedure   UpdateProjLib;

    function    SymSelected:Boolean;
    function    GetSym(Index:Integer):PSGroup;

    procedure   CopyToListBox(ListBox : TComboBox);
    procedure   Add(ExtLib:PExtLib);
    function    Delete(Symbol:PSGroup):Boolean;

    Procedure   DeleteLib(Index:Integer);                    
    Procedure   InsertLib(Index:Integer;aSymInfo:TSymInfo);

    Procedure   SetModified;      { setzt Modified bei selektierter Bib bzw. Projekt }
    Procedure   SetProjModified;  { setzt Modified bei Projekt }

    destructor  Destroy; override;

    property    LibIndex:Integer read FIndexLib write SetLibIndex;
    property    LibCount:Integer read GetLibCount;
    property    Lib:PSymbols     read IGetLib;
    property    ExtLib:PExtLib   read IGetExtLib;
    property    LibName:String   read IGetLibName;

    property    SymCount:Integer read GetSymCount;
    property    SymIndex:Integer read GetSymIndex write SetSymIndex;
    property    Sym:PSGroup      read IGetSym;
  end;

implementation

uses AM_Child, AM_Main, AM_Font;

{******************************************************************************}
{ Methoden f�r TSymLibInfo                                                     }
{******************************************************************************}
constructor TSymLibInfo.Create
   (
   Project     : pProj;
   ProjSymbols : pSymbols;
   ProjStr     : String
   );
var i       : Integer;
    ExtLib  : pExtLib;
begin
  inherited Create;
  if (ProjSymbols=nil) then Fail;
  FProject:=Project;
  FProjSymbols:=ProjSymbols;
  FIndexLib:=0;
  FProjStr:=NewStr(ProjStr);
  FSymLibList:=TList.Create;
  if (FSymLibList=nil) then Fail;

  with ProjSymbols^ do begin
    for i:=0 to ExternLibs.Count-1 do begin
      ExtLib:=pExtLib(ExternLibs.Items[i]);
      if (ExtLib^.Loaded) then  { nur verf�gbare anzeigen }
        FSymLibList.Add(TSymInfo.Create(ExtLib^.Symbols,ExtLib,ExtLib^.LibName)) { externe Bib. hinzuf�gen }
        else
        FSymLibList.Add(TSymInfo.AltCreate(ProjSymbols,ExtLib,ExtLib^.LibName));
      end;
    end;
  InsertLib(0,TSymInfo.Create(ProjSymbols,nil,FProjStr));   { Projektsymbole einf�gen }
end;

procedure TSymLibInfo.UpdateExtLib;
var ExtLib : PExtLib;
begin
  ExtLib:=GetSymInfo.ExtLib;
  if (ExtLib^.Loaded) then begin
    FSymLibList.Delete(FIndexLib);
    FSymLibList.Insert(FIndexLib,TSymInfo.Create(ExtLib^.Symbols,ExtLib,ExtLib^.LibName));
    end;
end;

procedure TSymLibInfo.UpdateProjLib;
begin
  FSymLibList.Delete(0);
  FSymLibList.Insert(0,TSymInfo.Create(FProjSymbols,nil,FProjStr));   { Projektsymbole einf�gen }
end;
                                 
function TSymLibInfo.GetSymInfo:TSymInfo;
begin
  if FSymLibList.Count > FIndexLib then
     GetSymInfo:=TSymInfo(FSymLibList.Items[FIndexLib]);
end;

{++ IDB}
Function TSymLibInfo.GetSymInfo(iLibraryNumber: Integer): TSymInfo;
Begin
     If iLibraryNumber < FSymLibList.Count Then
        RESULT := TSymInfo(FSymLibList.Items[iLibraryNumber]);
End;
{-- IDB}

function TSymLibInfo.IGetLibName:String;
var help : String;
begin
  Help:=GetSymInfo.FLibName^;
  if (Help=FProjStr^) then Help:='';
  IGetLibName:=Help;
end;

function TSymLibInfo.GetLibName(Index:Integer):String;
var help : String;
begin
  Help:=TSymInfo(FSymLibList.Items[Index]).FLibName^;
{  if (Help=FProjStr^) then Help:='';}
  GetLibName:=Help;
end;

function TSymLibInfo.IGetLib:PSymbols;
begin
  IGetLib:=GetSymInfo.Lib;
end;

function TSymLibInfo.IGetExtLib:PExtLib;
begin
  IGetExtLib:=GetSymInfo.ExtLib;
end;

function TSymLibInfo.GetExtLib(Index:Integer):PExtLib;
begin
  GetExtLib:=TSymInfo(FSymLibList.Items[Index]).ExtLib;
end;

function TSymLibInfo.IGetSym:PSGroup;
begin
  IGetSym:=GetSymInfo.Sym;
end;

function TSymLibInfo.IndexOf(aSym:pSGroup):Integer;
begin
  IndexOf:=GetSymInfo.IndexOf(aSym);
end;

function TSymLibInfo.SymSelected:Boolean;
begin
  SymSelected:=(SymIndex>=0) and (SymIndex<SymCount);
end;

function TSymLibInfo.GetLib(Index:Integer):PSymbols;
begin
  GetLib:=TSymInfo(FSymLibList.Items[Index]).Lib;
end;

procedure TSymLibInfo.Add(ExtLib:PExtLib);
begin                          { externe Bib. hinzuf�gen und gleich selektieren }
  LibIndex:=FSymLibList.Add(TSymInfo.Create(ExtLib^.Symbols,ExtLib,ExtLib^.LibName));
end;

function TSymLibInfo.Delete(Symbol:PSGroup):Boolean;
begin
  Delete:=GetSymInfo.Delete(Symbol);
end;

Procedure TSymLibInfo.DeleteLib(Index:Integer);
begin
  FSymLibList.Delete(Index);
end;

Procedure TSymLibInfo.InsertLib(Index:Integer;aSymInfo:TSymInfo);
begin
  FSymLibList.Insert(Index,aSymInfo);
end;

function TSymLibInfo.GetSymCount:Integer;
begin
  GetSymCount:=GetSymInfo.SymCount;
end;

function TSymLibInfo.GetLibCount:Integer;
begin
  GetLibCount:=FSymLibList.Count;
end;

function TSymLibInfo.GetSym(Index:Integer):PSGroup;
begin
  if (Index<0) then Index:=0;
  GetSym:=GetSymInfo.GetSym(Index);
end;

function TSymLibInfo.GetSymIndex:Integer;
begin
  GetSymIndex:=GetSymInfo.SymIndex;
end;

procedure TSymLibInfo.SetSymIndex(Index:Integer);
begin
  if (Index<0) then Index:=0;
  GetSymInfo.SymIndex:=Index;
end;

procedure TSymLibInfo.SetLibIndex(Index:Integer);
begin
  if (Index<0) then Index:=0;
  FIndexLib:=Index;
end;

procedure TSymLibInfo.CopyToListBox(ListBox:TComboBox);
var i : Integer;
begin
  for i:=0 to LibCount-1 do
    ListBox.Items.Add(GetLibName(i));
end;

procedure TSymLibInfo.SetModified;
begin
  if (GetSymInfo.ExtLib<>nil) then GetSymInfo.ExtLib^.SetModified(true)
    else SetProjModified;
end;

procedure TSymLibInfo.SetProjModified;
begin
 if (FProject<>nil) then FProject^.SetModified;
end;

destructor TSymLibInfo.Destroy;
var Cnt            : Integer;
begin
  DisposeStr(FProjStr);
  for Cnt:=0 to FSymLibList.Count-1 do TObject(FSymLibList[Cnt]).Free;
  FSymLibList.Free;
  inherited Destroy;
end;

{******************************************************************************}
{ Methoden f�r TSymInfo                                                        }
{******************************************************************************}
constructor TSymInfo.Create
   (
   Symbols     : PSymbols;
   ExtLib      : PExtLib;
   LibName     : PString;
   OnlyUsed    : Boolean = False
   );
var i       : Integer;
    ASym,
    BSym,
    ExtSym  : PSGroup;
{++ IDB}
    sSymLibName, sSymName: AnsiString;
    iI: Integer;
{-- IDB}
begin
  inherited Create;
  if (Symbols=nil) or (LibName=nil) then Fail;
  FSymbols:=Symbols;
  FIndexSym:=0;
  FLibName:=LibName;
  FExtLib:=ExtLib;
  FSymList:=TList.Create;
  if (FSymList=nil) then Fail;
  for i:=0 to FSymbols^.Data^.GetCount-1 do begin
   ASym:=PSGroup(Symbols^.Data^.At(i));
   if (not OnlyUsed) or (ASym^.UseCount > 0) then begin
    if (ExtLib<>nil) then FSymList.Add(ASym)    { Symbol hinzuf�gen (ext. Bib}
      else begin                                { Projekt-registerblatt }
      if (ASym^.GetLibName='') then { Projekt-Symbole }
        FSymList.Add(ASym)    { Symbol hinzuf�gen }
      else begin
        ExtSym:=Symbols^.GetExtSymbol(ASym^.GetName,ASym^.GetLibName);
        BSym:=Symbols^.GetProjectSymbol(ASym^.GetName,ASym^.GetLibName);
        if (BSym<>nil) then BSym:=BSym;
        if (ExtSym=nil) then {++ IDB - UNDO} Begin
{ These lines were added by Ivanoff to avoid some problems where an user switches off
  an external symbol library.
  19.09.2000.
  I do the following: I get symbol name, symbol library name and create new symbol
  name in the following way: SymbolName_-_SymbolLibraryName. Then I set symbol library
  name in the zero value. It means the symbol belongs to project symbol's library.}
           {$IFNDEF WMLT}
           {$IFNDEF AXDLL} // <----------------- AXDLL
           If (ASym.GetLibName <> '') AND (WinGisMainForm.WeAreUsingUndoFunction[WinGISMainForm.ActualProj]) Then Begin
              Symbols.SymTable.Delete(ASym);
              sSymLibName := ASym.GetLibName;
              sSymName := ASym.GetName;
              ASym.SetLibName('');
//              ASym.SetName(sSymName + ' - ' + sSymLibName);
              iI := 0;
              While Symbols.GetProjectSymbol(ASym.GetName, ASym.GetLibName) <> NIL Do Begin
                    Inc(iI);
//                    ASym.SetName(sSymName + '(' + IntToStr(iI) +') - ' + sSymLibName);
                    ASym.SetName(sSymName + '(' + IntToStr(iI) +')');
                    End;
              // Add information in the symbol's mapper of the IDB.
              {$IFNDEF WMLT}
              WinGisMainForm.UndoRedo_Man.AddSymbolMappingEntry(WinGisMainForm.ActualProj, sSymLibName, sSymName, ASym.GetName);
              {$ENDIF}
              // Add the symbol in hash table using new library name and new symbol name.
              Symbols.SymTable.Add(ASym);
              End;
           {$ENDIF} // <----------------- AXDLL
           {$ENDIF}
           {-- IDB - UNDO}
              FSymList.Add(ASym)    { wird noch nirgend angezeigt }
           {++ IDB}
           End
           {-- IDB}
          else if (ASym^.LastChange<>ExtSym^.LastChange) then
            FSymList.Add(ASym);       { Projekt und ext. Symbol vergleichen... }
        end;
      end;
     end;
    end;
  Sort;
end;

constructor TSymInfo.AltCreate
   (
   Symbols     : PSymbols;
   ExtLib      : PExtLib;
   LibName     : PString
   );
var i       : Integer;
    ASym    : PSGroup;
begin
  inherited Create;
  if (Symbols=nil) or (LibName=nil) then Fail;
  FSymbols:=Symbols;
  FIndexSym:=0;
  FLibName:=LibName;
  FExtLib:=ExtLib;
  FSymList:=TList.Create;
  if (FSymList=nil) then Fail;
  for i:=0 to FSymbols^.Data^.GetCount-1 do begin
    ASym:=PSGroup(Symbols^.Data^.At(i));
    if (ASym^.GetLibName=LibName^) then { nur "externen" Symbole der richtigen Bib}
      FSymList.Add(ASym);       { Symbol hinzuf�gen }
    end;
  Sort;
end;

procedure TSymInfo.Sort;
function CompareSym(Item1, Item2: Pointer): Integer;
var s1,s2 : String;
  begin
   s1:=PSGroup(Item1)^.GetName;
   s2:=PSGroup(Item2)^.GetName;
   if (s1>s2) then CompareSym:=1
     else if (s1=s2) then CompareSym:=0
       else CompareSym:=-1;
  end;
var ASym : PSGroup;
begin
  ASym:=Sym;
  FSymList.Sort(TListSortCompare(@CompareSym));
  SymIndex:=FSymList.IndexOf(ASym);            { wieder richtiges selektieren }
end;

Procedure TSymInfo.AddSym(Symbol:PSGroup);
begin
  FIndexSym:=FSymList.Add(Symbol);
  Sort;
end;

Function TSymInfo.Delete(Symbol:PSGroup):Boolean;
var i : Integer;
begin
  i:=FSymList.IndexOf(Symbol);
  if (i<0) then Delete:=False
    else begin
    FSymList.Delete(i);
    if (i>=SymCount) then SymIndex:=i-1;   { wenn letzes Symbol, vorletztes selektieren }
    Delete:=True;
    end;
end;

function TSymInfo.Valid(Index:Integer):Boolean;
begin
  Valid:=(Index>=0) and (Index<SymCount);
end;

function TSymInfo.GetSymCount:Integer;
begin
  GetSymCount:=FSymList.Count;
end;

function TSymInfo.IGetSym:PSGroup;
begin
  if (Valid(FIndexSym)) then IGetSym:=PSGroup(FSymList.Items[FIndexSym])
                        else IGetSym:=nil;
end;

function TSymInfo.IndexOf(aSym:pSGroup):Integer;
begin
  IndexOf:=FSymList.IndexOf(aSym);
end;

{++ IDB}
function TSymInfo.IndexOf(sSymbolName: AnsiString):Integer;
Var
   iI: Integer;
Begin
     RESULT := -1;
     For iI:=0 To SELF.FSymList.Count-1 Do
         If PSGroup(FSymList.Items[iI]).Name^ = sSymbolName Then Begin
            RESULT := iI;
            EXIT;
            End;
End;
{-- IDB}

function TSymInfo.GetSym(Index:Integer):PSGroup;
begin
  if (Valid(Index)) then GetSym:=PSGroup(FSymList.Items[Index])
                    else GetSym:=nil;
end;

function TSymInfo.GetLibName:String;
begin
  if (FLibName<>nil) then GetLibName:=FLibName^
                     else GetLibName:='';
end;

procedure TSymInfo.SetIndex(Index:Integer);
begin
  if (Index<0) then Index:=0;
  FIndexSym:=Index;
end;

destructor TSymInfo.Destroy;
begin
  FSymList.Free;
  inherited Destroy;
end;

end.
