unit GPSConn;

interface

uses Windows, Messages, SysUtils, Classes, AM_Sym, AM_Proj, AM_Paint, AxGisPro_TLB,
     WinOSInfo, ToolsLib, AM_Main, ErrHndl, AM_Def, Forms;

type TGPSPos = record
     Latitude: Double;
     Longitude: Double;
end;

type TGPSConnection = Class
private
   GPSSymbol: PSymbol;
   Proj: PProj;
   AXGisPro: TAXGisProjection;
   Lib: THandle;
   function NMEAToGPSPos(NMEAString: AnsiString; var AGPSPos: TGPSPos): Boolean;
   function NMEAToDeg(s: AnsiString; var d: Double): Boolean;
   function GPSPosToPoint(AGPSPos: TGPSPos): TPoint;
   procedure UpdateGPSSymbol(APoint: TPoint);
   function OpenLib: Boolean;
   procedure CloseLib;
   procedure ShowGPSStatus(s: AnsiString);
   procedure Recenter;
public
   ComPortAvailable: Boolean;
   Active: Boolean;
   constructor Create;
   destructor Destroy;
   function StartGPS(AProj: PProj; ASym: PSymbol): Boolean;
   procedure EndGPS;
   procedure CheckComPorts;
end;

const GPSAPI_DLL = 'gpsapi.dll';

var GPSConnection: TGPSConnection = nil;

procedure NMEACallback(NMEA: PChar); stdcall;

implementation

procedure NMEACallback(NMEA: PChar); stdcall;
var
s: AnsiString;
AGPS: TGPSPos;
APoint: TPoint;
begin
     if WinGISMainForm.ActualChild.InWMPaint then begin
        exit;
     end;
     s:=StrPas(NMEA);
     if GPSConnection.NMEAToGPSPos(s,AGPS) then begin
        APoint:=GPSConnection.GPSPosToPoint(AGPS);
        GPSConnection.UpdateGPSSymbol(APoint);
     end;
end;

{ TGPSConnection }

constructor TGPSConnection.Create;
begin
     Active:=False;
     GPSSymbol:=nil;
     Proj:=nil;
     Lib:=0;
     ComPortAvailable:=False;
     AXGisPro:=TAXGisProjection.Create(nil);
     AxGisPro.WorkingPath:=OSInfo.WingisDir;
     AxGisPro.LngCode:='044';
     AXGisPro.ShowOnlySource:=True;
end;

destructor TGPSConnection.Destroy;
begin
     AXGisPro.Free;
end;

function TGPSConnection.StartGPS(AProj: PProj; ASym: PSymbol): Boolean;
begin
     Result:=False;
     Proj:=AProj;
     GPSSymbol:=ASym;
     if ASym <> nil then begin
        Result:=OpenLib;
     end;
     Active:=Result;
     if Active then begin
        Proj^.DeSelectAll(False);
     end;
end;

procedure TGPSConnection.EndGPS;
begin
     if Lib > 0 then begin
        CloseLib;
     end;
     Active:=False;
     Proj:=nil;
     GPSSymbol:=nil;
     ShowGPSStatus('');
end;

procedure TGPSConnection.CheckComPorts;
var
i: Integer;
AHandle: THandle;
begin
     if ComPortAvailable then begin
        exit;
     end;
     ComPortAvailable:=False;
     for i:=1 to 16 do begin
         AHandle:=CreateFile(PChar('\\.\COM' + IntToStr(i)),GENERIC_READ,0,nil,OPEN_EXISTING,FILE_FLAG_OVERLAPPED,0);
         if AHandle <> INVALID_HANDLE_VALUE then begin
            FileClose(AHandle);
            ComPortAvailable:=True;
            break;
         end;
     end;
end;

function TGPSConnection.OpenLib: Boolean;
var
StartLiveGPS: function (ACallback: Pointer): Integer; stdcall;
begin
     Result:=False;
     Lib:=LoadLibrary(PChar(OSInfo.WingisDir + GPSAPI_DLL));
     if Lib > 0 then begin
        @StartLiveGPS:=GetProcAddress(Lib,'StartLiveGPS');
        if @StartLiveGPS <> nil then begin
           Result:=(StartLiveGPS(@NMEACallback) = 0);
        end;
     end;
end;

procedure TGPSConnection.CloseLib;
var
StopLiveGPS: function: Integer; stdcall;
begin
     if Lib > 0 then begin
        @StopLiveGPS:=GetProcAddress(Lib,'StopLiveGPS');
        if @StopLiveGPS <> nil then begin
           StopLiveGPS;
        end;
        FreeLibrary(Lib);
        Lib:=0;
     end;
end;

function TGPSConnection.NMEAToDeg(s: AnsiString; var d: Double): Boolean;
var
dd,mm: Double;
l: Integer;
begin
     Result:=False;
     d:=0;
     s:=Trim(s);
     if s <> '' then begin
        l:=StrLen(PChar(s));
        //Latitude
        if Pos('.',s) = 5 then begin
           dd:=SToF(Copy(s,1,2));
           mm:=SToF(Copy(s,3,StrLen(Pchar(s))-3));
        end
        // Longitude
        else if Pos('.',s) = 6 then begin
           dd:=SToF(Copy(s,1,3));
           mm:=SToF(Copy(s,4,StrLen(PChar(s))-4));
        end;
        d:=dd + (mm/60);
        Result:=(d >= -180) and (d <= 180);
     end;
end;

function TGPSConnection.NMEAToGPSPos(NMEAString: AnsiString; var AGPSPos: TGPSPos): Boolean;
var
ParamsList: TStringList;
GPSQuality,SatNum: Integer;
HDOP: Double;
AStatus,Q,T: AnsiString;
ConvOK: Boolean;
begin
     Result:=False;
     GPSQuality:=0;
     SatNum:=0;
     HDOP:=0;
     NMEAString:=Trim(NMEAString);
     if Pos('$GPGGA',NMEAString) = 1 then begin
        ParamsList:=StringToList(NMEAString,',');
        try
           if ParamsList.Count > 8 then begin
              T:=ParamsList[1];
              T:=Copy(T,1,6);
              GPSQuality:=StrToInt(ParamsList[6]);
              SatNum:=StrToInt(ParamsList[7]);
              HDOP:=SToF(ParamsList[8]);
              ConvOK:=NMEAToDeg(ParamsList[2],AGPSPos.Latitude);
              if ParamsList[3] = 'S' then begin
                 AGPSPos.Latitude:=-AGPSPos.Latitude;
              end;
              ConvOK:=ConvOK and NMEAToDeg(ParamsList[4],AGPSPos.Longitude);
              if ParamsList[5] = 'W' then begin
                 AGPSPos.Longitude:=-AGPSPos.Longitude;
              end;
              if GPSQuality = 0 then begin
                 Q:='NO GPS FIX';
              end
              else if GPSQuality = 1 then begin
                 Q:='GPS FIX';
              end
              else if GPSQuality >=2 then begin
                 Q:='DGPS FIX';  
              end;
              if GPSQuality > 0 then begin
                 AStatus:=' ' + Q + ' | SAT=' + IntToStr(SatNum) + ' | HDOP=' + FloatToStr(HDOP) + ' ';
              end
              else begin
                 AStatus:=' ' + Q + ' ';
              end;
              ShowGPSStatus(AStatus);
              Result:=(ConvOK) and (GPSQuality > 0);
           end;
        finally
           ParamsList.Free;
        end;
     end;
end;

procedure TGPSConnection.ShowGPSStatus(s: AnsiString);
begin
     try
        if s <> '' then begin
           WingisMainForm.ActualChild.Data^.ClearStatusText;
           WingisMainForm.ActualChild.Data^.SetStatusText(s,True);
        end
        else begin
           WingisMainForm.ActualChild.Data^.ClearStatusText;
        end;
        Application.ProcessMessages;
     except
        on E: Exception do begin
           DebugMsg('TGPSConnection.ShowGPSStatus',E.Message);
        end;
     end;
end;

function TGPSConnection.GPSPosToPoint(AGPSPos: TGPSPos): TPoint;
var
X,Y: Double;
begin
     AxGisPro.SourceProjSettings:='Geodetic Phi/Lamda';
     AxGisPro.SourceProjection:='Phi/Lamda values';
     AxGisPro.SourceDate:='WGS84';
     AxGisPro.SourceXOffset:=0;
     AxGisPro.SourceYOffset:=0;
     AxGisPro.SourceScale:=1;

     AxGisPro.TargetProjSettings:=Proj^.PInfo^.ProjectionSettings.ProjectProjSettings;
     AxGisPro.TargetProjection:=Proj^.PInfo^.ProjectionSettings.ProjectProjection;
     AxGisPro.TargetDate:=Proj^.PInfo^.ProjectionSettings.ProjectDate;
     AxGisPro.TargetXOffset:=Proj^.PInfo^.ProjectionSettings.ProjectionXOffset;
     AxGisPro.TargetYOffset:=Proj^.PInfo^.ProjectionSettings.ProjectionYOffset;
     AxGisPro.TargetScale:=Proj^.PInfo^.ProjectionSettings.ProjectionScale;

     X:=AGPSPos.Longitude;
     Y:=AGPSPos.Latitude;
     AxGisPro.Calculate(X,Y);
     Result.X:=Trunc(X*100);
     Result.Y:=Trunc(Y*100);
end;

procedure TGPSConnection.UpdateGPSSymbol(APoint: TPoint);
var
OldDrawPoint,NewDrawPoint: TPoint;
ARect: TDRect;
AMsg: TWMPaint;
begin
     if GPSSymbol <> nil then begin
        if (APoint.X <> GPSSymbol^.Position.X) or (APoint.Y <> GPSSymbol^.Position.Y) then begin
           ARect.InitByRect(GPSSymbol^.ClipRect);
           Proj^.PInfo^.ProjPointToVirtPoint(GPSSymbol^.Position,OldDrawPoint);
           LPToDP(Proj^.PInfo^.ExtCanvas.Handle,OldDrawPoint,1);
           GPSSymbol^.Position.X:=APoint.X;
           GPSSymbol^.Position.Y:=APoint.Y;
           Proj^.PInfo^.ProjPointToVirtPoint(GPSSymbol^.Position,NewDrawPoint);
           LPToDP(Proj^.PInfo^.ExtCanvas.Handle,NewDrawPoint,1);
           GPSSymbol^.CalculateClipRect(Proj^.PInfo);
           Proj^.CorrectSize(GPSSymbol^.ClipRect,False);
           Proj^.UpdateClipRect(GPSSymbol);
           Proj^.SelAllRect.CorrectByRect(GPSSymbol^.ClipRect);
           ARect.CorrectByRect(GPSSymbol^.ClipRect);
           if (OldDrawPoint.X <> NewDrawPoint.X) or (OldDrawPoint.Y <> NewDrawPoint.Y) then begin
              Proj^.PInfo^.FromGraphic:=True;
              Proj^.PInfo^.RedrawRect(ARect);
           end;
           ARect.Done;
           Proj^.SetModified;
           Recenter;
        end;
     end;
end;

procedure TGPSConnection.Recenter;
var
X,Y,L,R,T,B: Integer;
begin
     if GPSSymbol <> nil then begin
        X:=GPSSymbol^.Position.X;
        Y:=GPSSymbol^.Position.Y;
        L:=Trunc(Proj^.CurrentViewRect.X);
        B:=Trunc(Proj^.CurrentViewRect.Y);
        R:=Trunc(Proj^.CurrentViewRect.X+Proj^.CurrentViewRect.Width);
        T:=Trunc(Proj^.CurrentViewRect.Y+Proj^.CurrentViewRect.Height);
        if (X < L) or (X > R) or (Y < B) or (Y > T) then begin
           Proj^.ZoomByFactorWithCenter(GPSSymbol.Position.X,GPSSymbol.Position.Y,1.0);
        end;
     end;
end;

end.
