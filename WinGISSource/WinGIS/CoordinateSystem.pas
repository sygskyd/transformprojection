unit CoordinateSystem;

interface
uses GrTools
{$IFNDEF AXDLL} // <----------------- AXDLL
     {+++Brovak}
  , OleCtrls, AxGisPro_TLB
     {--Brovak}
{$ENDIF} // <----------------- AXDLL
  ;

type
  TCoordinateType = (ctCarthesian, ctGeodatic);

  TCoordinateSystem = class
  private
    FXName: string;
    FYName: string;
    FType: TCoordinateType;

       {+++brovak for projections}
{$IFNDEF AXDLL} // <----------------- AXDLL
    AxGISProjection1: TAxGisProjection;
{$ENDIF} // <----------------- AXDLL
       {--brovak}

    procedure SetType(const Value: TCoordinateType);
  public
    constructor Create;
    destructor Destroy;
    property CoordinateType: TCoordinateType read FType write SetType;

    function ShowInProjectionFormats(X, Y: Double; AProj: Pointer): string;
    function FormatCoordinates(const Text: string; const X, Y: Double): string; overload;
       //changed by Brovak
    function FormatCoordinates(const X, Y: Double; AProj: Pointer): string; overload;
    function FormatCoordinates(const Point: TGrPoint): string; overload;
{++ Moskaliov BUG#90 BUILD#150 05.01.01}
    function FormatCoordinates(const X1, Y1, X2, Y2: Double): string; overload;
{-- Moskaliov BUG#90 BUILD#150 05.01.01}
    property XName: string read FXName;
    property YName: string read FYName;
  end;
implementation
uses MultiLng, SysUtils, Am_proj, am_main, AM_Def, WinOSInfo;

var
  Proj: pointer;
{ TCoordinateSystem }

constructor TCoordinateSystem.Create;
begin
  inherited Create;
  CoordinateType := ctCarthesian;
{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}
  {++Brovak}
  AxGisProjection1 := TAxGisProjection.Create(nil);
  AxGisProjection1.WorkingPath := OSInfo.WingisDir;
  AxGisProjection1.LngCode := '044'; // set english language code here due to warnings and errors are not handeled
  {--Brovak}
{$ENDIF}
{$ENDIF} // <----------------- AXDLL
end;

destructor TCoordinateSystem.Destroy;
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}
  {++Brovak}
  AxGisProjection1.Destroy;
  {--Brovak}
{$ENDIF}
{$ENDIF} // <----------------- AXDLL
end;

{++brovak for projections}

function TCoordinateSystem.ShowInProjectionFormats(X, Y: Double; AProj: Pointer): string;
var
  X1, Y1, XTmp, YTmp, Scale: double;
  XStr, YStr: shortstring;
  AMin, ASec: double;
begin;
  //for default
  Result := XName + ': ' + FormatFloat('#,##0.00', X / 100) + ' ' + YName + ': ' + FormatFloat('#,##0.00', Y / 100);
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
        // setup the source projection
  if PProj(Aproj).PInfo.CoordinateSystem.CoordinateType = ctGeodatic then
    AxGISProjection1.SourceCoordinateSystem := cs_Geodetic
  else
    AxGISProjection1.SourceCoordinateSystem := cs_Carthesic;
  AxGISProjection1.SourceProjSettings := PProj(Aproj).PInfo.ProjectionSettings.ProjectProjSettings;
  AxGISProjection1.SourceProjection := PProj(Aproj).PInfo.ProjectionSettings.ProjectProjection;
  AxGISProjection1.SourceDate := PProj(Aproj).PInfo.ProjectionSettings.ProjectDate;
  AxGISProjection1.SourceXOffset := PProj(Aproj).PInfo.ProjectionSettings.ProjectionXOffset;
  AxGISProjection1.SourceYOffset := PProj(Aproj).PInfo.ProjectionSettings.ProjectionYOffset;
  AxGisProjection1.SourceScale := PProj(Aproj).PInfo.ProjectionSettings.ProjectionScale;
  AxGISProjection1.UsedInputCoordSystem := cs_Carthesic; // due to WinGIS coordinates are always stored carthesic

        // setup the target projection
  AxGisProjection1.TargetCoordinateSystem := cs_Geodetic; // Phi / Lamda values are geodetic
  AxGISProjection1.TargetProjSettings := 'Geodetic Phi/Lamda';
  AxGISProjection1.TargetProjection := 'Phi/Lamda values';
  AxGISProjection1.TargetDate := 'WGS84';
  AxGISProjection1.TargetXOffset := 0; // now offset will be used
  AxGISProjection1.TargetYOffset := 0;
  AxGISProjection1.TargetScale := 1.0; // no scaling of result
  AxGISProjection1.UsedOutputCoordSystem := cs_Carthesic; // due to WinGIS coordinates are always stored carthesic

  X1 := X / 100;
  Y1 := Y / 100; // due to WinGIS used centimeters but projection calculation needs meters
  AxGISProjection1.Calculate(X1, Y1);

  AMin := Abs(Frac(X1) * 60);
  ASec := Trunc(Frac(Amin) * 60);
  Amin := Trunc(Amin);
  Xstr := FormatFloat('00', trunc(X1)) + chr(176) + FormatFloat('00', AMin) + '''' + FormatFloat('00', ASec) + '"';

  AMin := Abs(Frac(Y1) * 60);
  ASec := Trunc(Frac(Amin) * 60);
  Amin := Trunc(Amin);
  Ystr := FormatFloat('00', trunc(Y1)) + chr(176) + FormatFloat('00', AMin) + '''' + FormatFloat('00', ASec) + '"';

  if FType = ctGeodatic then
  begin
    Result := MlgStringList['CoordinateSystem', 3] + ': ' + XStr + '  ' + MlgStringList['CoordinateSystem', 4] + ': ' + YStr;
  end
  else
  begin
    Result := MlgStringList['CoordinateSystem', 4] + ': ' + XStr + '  ' + MlgStringList['CoordinateSystem', 3] + ': ' + YStr;
  end;

{$ENDIF} // <----------------- AXDLL
{$ENDIF}

end;
{-- Brovak}

//changed by Brovak

function TCoordinateSystem.FormatCoordinates(const X, Y: Double; AProj: Pointer): string;
begin
  if PProj(Aproj).PInfo.ProjectionSettings.ShowCoordInDegrees = true then
    Result := ShowInProjectionFormats(X, Y, Aproj)
  else
    Result := XName + ': ' + FormatFloat('#,##0.00', X / 100) + ' ' + YName + ': ' + FormatFloat('#,##0.00', Y / 100)
end;

function TCoordinateSystem.FormatCoordinates(const Point: TGrPoint): string;
begin
  Result := FormatCoordinates(Point.X, Point.Y, Proj);
end;

procedure TCoordinateSystem.SetType(const Value: TCoordinateType);
begin
  FType := Value;
  case FType of
    ctCarthesian:
      begin
        FXName := MlgStringList['CoordinateSystem', 1];
        FYName := MlgStringList['CoordinateSystem', 2];
      end;
    ctGeodatic:
      begin
        FXName := MlgStringList['CoordinateSystem', 2];
        FYName := MlgStringList['CoordinateSystem', 1];
      end;
  end;
end;

function TCoordinateSystem.FormatCoordinates(const Text: string; const X, Y: Double): string;
begin
  Result := Format(Text, [XName, X / 100, YName, Y / 100]);
end;

{++ Moskaliov BUG#90 BUILD#150 05.01.01}

function TCoordinateSystem.FormatCoordinates(const X1, Y1, X2, Y2: Double): string;
begin
  Result := XName + '1: ' + FormatFloat('#,##0.00', X1 / 100) + ' '
    + YName + '1: ' + FormatFloat('#,##0.00', Y1 / 100) + ' '
    + XName + '2: ' + FormatFloat('#,##0.00', X2 / 100) + ' '
    + YName + '2: ' + FormatFloat('#,##0.00', Y2 / 100);

end;
{-- Moskaliov BUG#90 BUILD#150 05.01.01}

end.

