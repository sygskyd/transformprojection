Unit LineDropDown;
                                       
Interface

Uses Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,DipGrid,
     ComboBtn,ProjStyle,PropCollect,StyleDef,MultiLng;

Type TLineDropDownForm = Class(TDropDownForm)
       DipList     : TDipList;
       MlgSection  : TMlgSection;
       Procedure   DipListDrawCell(Sender:TObject;Canvas:TCanvas;Index:Integer;Rect:TRect);
       Procedure   DipListMouseUp(Sender: TObject; Button: TMouseButton;
                       Shift: TShiftState; X, Y: Integer);
       Procedure   FormCreate(Sender: TObject);
       Procedure   FormDestroy(Sender: TObject);
       Procedure   FormShow(Sender: TObject);
      Private
       FOnSelectStyle   : TNotifyEvent;
       FProjStyles      : TProjectStyles;
       FStyle           : Integer;
       FStyleOptions    : TSelectOptionList;
       FXLineList       : TStringList;
       Procedure   DoSelectStyle;
       Procedure   SetStyleOptions(const Value: TSelectOptionList);
       Procedure   UpdateStylesList;
      Public
       Property    Style:Integer read FStyle write FStyle;
       Property    StyleOptions:TSelectOptionList read FStyleOptions write SetStyleOptions;
       Property    OnSelectStyle:TNotifyEvent read FOnSelectStyle write FOnSelectStyle;
       Property    ProjectStyles:TProjectStyles read FProjStyles write FProjStyles;
     end;

Implementation

{$R *.DFM}

Uses CommonResources,NumTools,XLines,XStyles;

Const SystemLineStyles  = 5;

Procedure TLineDropDownForm.DipListDrawCell(Sender: TObject;
  Canvas: TCanvas; Index: Integer; Rect: TRect);
var AText          : String;
    ARect          : TRect;
    Y              : Integer;
    Item           : TXLineStyle;
    Points       : Array[0..1] of TPoint;
begin
  with Canvas do begin
    if Index<0 then begin
      AText:=FStyleOptions.ShortNames[Abs(Index)-1];
      TextRect(Rect,CenterWidthInRect(TextWidth(AText),Rect),
          CenterHeightInRect(TextHeight(AText),Rect),AText);
    end
    else begin
      FillRect(Rect);
      ARect:=Classes.Rect(Rect.Left+DipList.RowHeight+3,
          Rect.Top,Rect.Left+80,Rect.Bottom);
      Y:=CenterHeightInRect(0,Rect);
      if Index<SystemLineStyles then begin
        CommonListBitmaps.Draw(Canvas,Rect.Left+2,CenterHeightInRect(14,Rect),clbSystem);
        Pen.Style:=TPenStyle(Index);
        Pen.Width:=0;
        with ARect do Polyline([Point(Left+1,Y-1),Point(Right-1,Y-1)]);
        with ARect do Polyline([Point(Left+1,Y),Point(Right-1,Y)]);
        AText:=MlgSection[20+Index];
      end
      else begin
        CommonListBitmaps.Draw(Canvas,Rect.Left+2,CenterHeightInRect(14,Rect),clbUser);
        Item:=TXLineStyle(FXLineList.Objects[Index-SystemLineStyles]);
        with ARect do begin
          Points[0]:=Point(Left+1,Y);
          Points[1]:=Point(Right-1,Y);
          XPolyline(Canvas.Handle,Points,2,Item.StyleDef^.XLineDefs,
              Item.StyleDef^.Count,0,0,True,(DipList.RowHeight-2)
              /RectHeight(Item.ClipRect)/5);
        end;
        AText:=Item.Name;
      end;
      ARect:=InflatedWidth(Rect,-85,0);
      TextRect(ARect,ARect.Left,CenterHeightInRect(TextHeight(AText),ARect),AText);
    end;
  end;
end;

Procedure TLineDropDownForm.SetStyleOptions(const Value:TSelectOptionList);
begin
  FStyleOptions.Assign(Value);
end;

Procedure TLineDropDownForm.FormCreate(Sender: TObject);
begin
  FStyleOptions:=TSelectOptionList.Create();
  SetupSelectOptions(FStyleOptions,sotLineStyle,[]);
  FXLineList:=TStringList.Create;
  FXLineList.Sorted:=TRUE;
  FXLineList.Duplicates:=dupAccept;
end;

Procedure TLineDropDownForm.FormDestroy(Sender: TObject);
begin
  FXLineList.Free;
end;

Procedure TLineDropDownForm.UpdateStylesList;
var XLineStyle   : TXLineStyle;
    Cnt          : Integer;
begin
  FXLineList.Clear;
  if FProjStyles<>NIL then for Cnt:=0 to FProjStyles.XLineStyles.Capacity-1 do begin
    XLineStyle:=FProjStyles.XLineStyles.Items[Cnt];
    if XLineStyle<>NIL then FXLineList.AddObject(XLineStyle.Name,XLineStyle);
  end;
end;

Procedure TLineDropDownForm.FormShow(Sender: TObject);
var AHeight        : Integer;
    XLineStyle     : TXLineStyle;
begin
  UpdateStylesList;
  with DipList do begin
    Count:=FXLineList.Count+SystemLineStyles;
    HeaderRows:=FStyleOptions.Count;
    AHeight:=Count Div ColCount+HeaderRows;
    if Count Mod ColCount<>0 then Inc(AHeight);
    if AHeight>8 then AHeight:=8;
    Height:=AHeight*CellHeight;
    if FStyle<0 then ItemIndex:=-FStyleOptions.IndexOfValue(FStyle)-1
    else if FStyle<lt_UserDefined then ItemIndex:=FStyle
    else begin
      XLineStyle:=FProjStyles.XLineStyles.NumberedStyles[FStyle];
      if XLineStyle=NIL then ItemIndex:=0
      else ItemIndex:=FXLineList.IndexOfObject(XLineStyle)+SystemLineStyles;
    end;
  end;
  ClientHeight:=DipList.Height+2;
end;

Procedure TLineDropDownForm.DoSelectStyle;
begin
  with DipList do if ItemIndex>=-FStyleOptions.Count then begin
    if ItemIndex>=SystemLineStyles then FStyle:=
        TXLineStyle(FXLineList.Objects[ItemIndex-SystemLineStyles]).Number
    else if ItemIndex>=0 then FStyle:=ItemIndex
    else FStyle:=FStyleOptions.Values[Abs(ItemIndex)-1];
  end;
  if Assigned(FOnSelectStyle) then OnSelectStyle(Self);
end;

Procedure TLineDropDownForm.DipListMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var Index          : Integer;
begin
  Index:=DipList.ItemAtPos(Point(X,Y),TRUE);
  if Index>=-DipList.HeaderRows then DoSelectStyle;
  Hide;
end;

end.
