{******************************************************************************+
  Module NewViewDlg
--------------------------------------------------------------------------------
  Author: Martin Forst
--------------------------------------------------------------------------------
  Dialog to enter the name and to set the options for a new view.
+******************************************************************************}
Unit NewViewDlg;

Interface

{$IFNDEF AXDLL} // <----------------- AXDLL

Uses Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     StdCtrls,WCtrls,ExtCtrls,AM_Proj,MultiLng, Validate;

Type TNewViewDialog = Class(TWForm)
       Bevel1           : TBevel;
       Bevel2           : TBevel;
       CancelBtn        : TButton;
       MlgSection       : TMlgSection;
       NameEdit         : TEdit;
       NameVal          : TValidator;
       OkBtn            : TButton;
       SaveLayerCheck   : TCheckBox;
       SaveViewCheck    : TCheckBox;
       StartupCheck     : TCheckBox;
       WLabel1          : TWLabel;
       Procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
       Procedure FormHide(Sender: TObject);
      Private
       FProject    : PProj;
       FIndex      : Integer;
      Public
       Property    Project:PProj read FProject write FProject;
       Property    InsertPosition:Integer read FIndex;
     end;
{$ENDIF} // <----------------- AXDLL
Implementation
{$IFNDEF AXDLL} // <----------------- AXDLL
{$R *.DFM}

Uses AM_Def,AM_Projo,AM_Sight,GrTools;

Procedure TNewViewDialog.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
var Sight          : PSight;
    Index          : Integer;
begin
  if ModalResult<>mrOK then CanClose:=TRUE
  else begin
    CanClose:=FALSE;
    if CheckValidators(Self) then begin
      if not SaveViewCheck.Checked and not SaveLayerCheck.Checked then
          MessageDialog(MlgSection[9],mtInformation,[mbOK],0)
      else begin
        Sight:=New(PSight,Init(NameVal.AsText,RotRect(0,0,0,0,0)));
        if FProject^.Sights^.Search(Sight,Index) then begin
          if MessageDialog(Format(MlgSection[8],[PToStr(Sight^.Name)]),
              mtConfirmation,[mbYes,mbNo],0)=mrYes then begin
            FProject^.Sights^.AtFree(Index);
            CanClose:=TRUE;
          end
          else ActiveControl:=NameEdit;
        end
        else CanClose:=TRUE;
        Dispose(Sight,Done);
      end;  
    end;
  end;  
end;

Procedure TNewViewDialog.FormHide(Sender: TObject);
var Sight          : PSight;
    Index          : Integer;
begin
  if ModalResult=mrOK then begin
    // set or clear the view-rectangle
    if SaveViewCheck.Checked then Sight:=New(PSight,Init(NameVal.AsText,
        FProject^.PInfo^.GetCurrentScreen))
    else Sight:=New(PSight,Init(NameVal.AsText,RotRect(0,0,0,0,0)));
    // set or clear the layer-data
    if SaveLayerCheck.Checked then Sight^.SetLayers(FProject^.Layers);
    // add the new view to the list
    FProject^.Sights^.Insert(Sight);
    // update default-view-index
    Index:=FProject^.Sights^.IndexOf(Sight);
    if StartupCheck.Checked then FProject^.Sights^.Default:=Index
    else if Index<=FProject^.Sights.Default then Inc(FProject^.Sights^.Default);
    // send changes to database
    {$IFNDEF WMLT}
    ProcDBAddView(NameVal.AsText);
    ProcDBSendViews(Project);
    {$ENDIF}
    FIndex:=Index;
  end
  else FIndex:=-1;
end;
{$ENDIF} // <----------------- AXDLL
end.
