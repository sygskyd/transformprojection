�
 TBUSGRAPHDLG 0   TPF0TBusGraphDlgBusGraphDlgLeft;Top� BorderStylebsDialogCaption!0ClientHeightcClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnCloseQueryFormCloseQueryOnShowFormShowPixelsPerInch`
TextHeight TLabelLabelFactorLeftTop� WidthHeightAnchorsakLeftakBottom Caption!176Visible  
TWGroupBoxPositionGrBoxLeftTopWidth� HeightQAnchorsakLeftakBottom Caption!14TabOrder TWLabelXPosLabLeftTopWidth1HeightAutoSizeCaption!15FocusControlXPosEditAlignToControl	alcCenter  TWLabelYPosLabLeftTop0Width1HeightAutoSizeCaption!16FocusControlYPosEditAlignToControl	alcCenter  TEditXPosEditLeftMTopWidthPHeightAnchorsakTopakRight ImeNameMSIME95TabOrderTextXPosEditOnClickPositionEditClick  TSpinBtnXPosSpinLeft� TopHeightAnchorsakTopakRight 	ArrowkeysCtrlIncrement       �@	Increment أp=
ף�?Max ��(<
ף@Min أp=
ף�OnClickPositionSpinClickShiftIncrement       ��?	ValidatorXPosVal  TEditYPosEditLeftMTop,WidthPHeightAnchorsakTopakRight ImeNameMSIME95TabOrderTextYPosEditOnClickPositionEditClick  TSpinBtnYPosSpinLeft� Top,HeightAnchorsakTopakRight 	ArrowkeysCtrlIncrement       �@	Increment أp=
ף�?Max ��(<
ף@Min أp=
ף�OnClickPositionSpinClickShiftIncrement       ��?	ValidatorYPosVal   
TWGroupBox	SizeGrBoxLeft� TopWidth� HeightQAnchorsakLeftakBottom Caption!17TabOrder TWLabelWidthLabLeftTopWidth1HeightAutoSizeCaption!18FocusControl	WidthEditAlignToControl	alcCenter  TEdit	WidthEditLeftMTopWidthPHeightAnchorsakTopakRight ImeNameMSIME95TabOrderText	WidthEditOnClickSizeEditClick  TSpinBtn	WidthSpinLeft� TopHeightAnchorsakTopakRight 	ArrowkeysCtrlIncrement       �@	Increment ��������?Max ��(<
ף@Min       ��?OnClickSizeSpinClickShiftIncrement       ��?	ValidatorChartWidthVal   TPanel
ChartPanelLeftTopdWidth� Height� AnchorsakLeftakBottom 
BevelInner	bvLowered
BevelOuterbvNoneCaption
ChartPanelColorclWhiteParentShowHintShowHint	TabOrder TChart	ViewChartLeftTopWidth� Height� CursorcrHandPointAllowPanningpmNoneBackWall.Brush.Color��� BackWall.ColorclNoneBackWall.Pen.Color� � BackWall.Pen.VisibleBackWall.SizeBackWall.Gradient.EndColorclWhiteBackWall.TransparencydBottomWall.Dark3DBottomWall.Pen.VisibleBottomWall.SizeFoot.Text.StringsFoot Foot.VisibleGradient.DirectiongdFromTopLeftGradient.EndColorclWhiteLeftWall.Dark3DLeftWall.Pen.VisibleLeftWall.SizeLeftWall.Transparent	LeftWall.VisibleLegend.VisibleMarginBottom
MarginLeftMarginRight	MarginTopTitle.Text.StringsTChart Title.VisibleOnClickBackgroundViewChartClickBackgroundOnClickSeriesViewChartClickSeriesBottomAxis.Grid.VisibleBottomAxis.LabelsAngleZBottomAxis.LabelsFont.CharsetANSI_CHARSETBottomAxis.LabelsFont.StylefsBold BottomAxis.LabelsSeparation BottomAxis.LabelsSize/BottomAxis.LabelStyletalMarkBottomAxis.MinorTicks.VisibleBottomAxis.TickLength
BottomAxis.Ticks.VisibleBottomAxis.TicksInner.VisibleBottomAxis.VisibleChart3DPercentF
ClipPointsFrame.Color� � Frame.VisibleLeftAxis.AutomaticLeftAxis.AutomaticMinimumLeftAxis.ExactDateTimeLeftAxis.LabelsLeftAxis.LabelsFont.CharsetANSI_CHARSETLeftAxis.LabelsFont.Height�LeftAxis.LabelsFont.NameBangleLeftAxis.TickOnLabelsOnlyLeftAxis.VisibleRightAxis.VisibleTopAxis.Inverted	TopAxis.VisibleView3DView3DOptions.Elevation;View3DOptions.OrthogonalView3DOptions.Perspective View3DOptions.RotationhView3DOptions.ZoomText
Zoom.AllowOnBeforeDrawChartViewChartBeforeDrawChartAlignalClient
BevelOuterbvNone
BevelWidth ColorclWhiteParentShowHintShowHint	TabOrder PrintMargins  
TPieSeries
ViewSeriesMarks.ArrowLengthMarks.Visible	SeriesColorclRedCircledPieValues.NamePiePieValues.OrderloNoneRotationAngle}Shadow.Color���     TButtonOkBtnLeftTopdWidthoHeightAnchorsakLeftakBottom Caption!8Default	ModalResultTabOrder   TButton	CancelBtnLeftTop� WidthoHeightAnchorsakLeftakBottom Cancel	Caption!9ModalResultTabOrder  TButton	ChangeBtnLeftTop� WidthoHeightAnchorsakLeftakBottom Cancel	Caption!13TabOrderOnClickChangeBtnClick  TButton
SetTypeBtnLeftTop� WidthoHeightAnchorsakLeftakBottom Caption!11TabOrderOnClickSetTypeBtnClick  	TCheckBoxcbPropLeftTop� WidthoHeightAnchorsakLeftakBottom Caption!175TabOrderOnClickcbPropClick  TEdit
EditFactorLeftTopWidthiHeightAnchorsakLeftakBottom TabOrderText
EditFactorVisible  TSpinBtn
FactorSpinLeftzTopHeightAnchorsakRightakBottom 	ArrowkeysCtrlIncrement       �@	Increment       ��?Max ��(<
ף@Min       ��?ShiftIncrement       �@	Validator	FactorValVisible  	TComboBoxComboBoxModeLeftTop(WidthyHeightStylecsDropDownListAnchorsakLeftakBottom 
ItemHeightItems.Strings!177!178!179 TabOrder
Visible  	TCheckBoxCBTranspLeftTopHWidthyHeightAnchorsakLeftakBottom Caption!157TabOrder  TFloatValidatorYPosValEditYPosEditCommasMinValue أp=
ף�	MaxLengthMaxValue ��(<
ף@ShowTrailingZeros	LeftTop�   TFloatValidatorXPosValEditXPosEditCommasMinValue أp=
ף�	MaxLengthMaxValue ��(<
ף@ShowTrailingZeros	Left8Top�   TFloatValidatorChartWidthValEdit	WidthEditDefaultValue       �@CommasMinValue أp=
ף�?	MaxLengthMaxValue ��(<
ף@ShowTrailingZeros	LeftXTop�   TMlgSection
MlgSectionSectionBusinessGraphicsLeftXTop�   TColorDialogColorDialog1Ctl3D	Left8Top�   TChartEditorViewChartEditorChart	ViewChart
DefaultTabcet3DHideTabs
cetGeneralcetAxis	cetTitles	cetLegendcetPanel	cetPagingcetWallscetSeriesData	cetExportcetToolscetPrintPreview OptionsceChange Title!4LeftTop�   TFloatValidator	FactorValEdit
EditFactorDefaultValue       ��?CommasMinValue أp=
ף�?	MaxLengthMaxValue ��(<
ף@ShowTrailingZeros	LeftTop�    