�
 TLEGENDDIALOG 0�!  TPF0TLegendDialogLegendDialogLeft<TophBorderStylebsDialogCaption!1ClientHeight9ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenter
OnActivateFormActivateOnCloseQueryFormCloseQueryOnShowFormShowPixelsPerInch`
TextHeight TBevelBevel1LeftTopWidth�Height  TPanelPanel1Left Top Width�Height
BevelOuterbvNoneTabOrder  TWLabelWLabel2LeftTopWidthAHeightAutoSizeCaption!2FocusControlLegendsComboAlignToControl	alcCenter  TWLabelWLabel1LeftTop*WidthAHeightAutoSizeCaption!3AlignToControl	alcCenter  TSpeedButtonRemoveLegendBtnLeft�TopWidthHeightHint!40EnabledFlat	
Glyph.Data
�   �   BM�       v   (               �                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� wwwwwwwwwwwwwwwwwp     wp�����wp�"���wp�"���wp�D�OOp�D���p��DO������q����x�   xwwwww��wwww��wwwwwwqwwwwParentShowHintShowHint	OnClickRemoveLegendBtnClick  TSpeedButtonCopyLegendBtnLeft�TopWidthHeightHint!39EnabledFlat	
Glyph.Data
�   �   BM�       v   (               �                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� wwwwwwwwwwwwwwwwwp     wp�����wp�"���wp�"���w   �OOw�����w���DOw� ��� �����      ��wwwww� wwwww��wwwww  wwwwwParentShowHintShowHint	OnClickCopyLegendBtnClick  TSpeedButtonAddLegendBtnLeftpTopWidthHeightHint!37EnabledFlat	
Glyph.Data
�   �   BM�       v   (               �                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� wwwwwwwwwwwwwwwwwp     wp�����wp�"���wp�"���wp�D�OOwp�D�������DO������x���������    ����wwwwx���wwww�x��wwwwwwwwwwwwParentShowHintShowHint	OnClickAddLegendBtnClick  TSpeedButtonRenameLegendBtnLeft�TopWidthHeightHint!38EnabledFlat	
Glyph.Data
�   �   BM�       v   (               �                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 33333333       3������3"/ODO�3"/����3������3DODD��3DO�    �������@��� ���������O��    ����3330�D� 3330����3330    ParentShowHintShowHint	OnClickRenameLegendBtnClick  TImageLegendeSymbolsLeftdTopWidth� HeightAutoSize	Picture.Data
�  TBitmapv  BMv      v   (   �                                   �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� �wx�               ��wx�qqf �w���ff`��p�qqqp ��fff�  q�� ��fff�p  �qqqp �fffwp p        ��ff`�w�   �qq ��f ��p    p��� ��� pp�qqqp���w�������               ���Visible  	TSpeedBtnHierarchicBtnLeftTopgWidthHeightHint!5Down	
Glyph.Data
F  B  BMB      B   (                                  |  �     |||||||||||||||||�=�=�=�=�=�=�=�=|||  ||||       �=||�=  �=|||||||||||||      |||�=�=�=�=�=�=�=�=|�=      �=||       �=|          |||||||||||||  ||||�=�=�=�=�=�=�=�=|||  ||||       �=|||  |||||||||||||||  ||||�=�=�=�=�=�=�=�=|||  ||||       �=|||  |||||||||||||||  ||||�=�=�=�=�=�=�=�=|||  ||||       �=|||  |||||||||||||||  |||
GroupIndexOnClickHierarchicBtnClickParentShowHintShowHint	  	TSpeedBtnAlphabeticBtnLeft#TopgWidthHeightHint!6Down
Glyph.Data
F  B  BMB      B   (                                  |  �     ||||||||||||||||| < < < < < <|||||  |||| < <||| <||||�=  �=|||| < <|||||||      ||||| < <|||||�=      �=||||| < <||||          || <||| < <|||||  |||| < < < < < <|||||  |||||||||||||||  ||||   |   ||||  ||||�= ||| �=||||  |||||     |||||  |||||�= | �=|||||  ||||||   ||||||  ||||||�= �=||||||  ||||||| |||||||  |||
GroupIndexOnClickAlphabeticBtnClickParentShowHintShowHint	  TLabelLabel2Left8TopkWidth1HeightAutoSizeCaption!7FocusControl
SearchEdit  	TSpeedBtn	SearchBtnLeft�TopgWidthHeightHint!32Down
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 333333333333333333333333333333�333333433333338�3333334C33????���3CCCDDD33������3333334C3333338�333333433?�3?�833 3 333��3��333 3 333���333 0�333�����333 00333�����333    333�����3330� �333388��33333 033333���33333 033333�8�33333333333333333333
GroupIndex OnClickSearchBtnClickParentShowHintShowHint	  	TSpeedBtnEntryStyleBtnLeftGTopgWidthHeightHint!42Down
Glyph.Data
�   �   BM�       v   (               �                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 33333333333333334DC3DDDC34333DC334334DC333DDDD3333C3DD3333C3DC333344DC333344D333333DD333333DC333333DC333333433333333333333333333
GroupIndex OnClickEntryStyleBtnClickParentShowHintShowHint	  	TSpeedBtnTitleStyleBtnLefthTop&WidthHeightHint!41Down
Glyph.Data
�   �   BM�       v   (               �                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 33333333333333334DC3DDDC34333DC334334DC333DDDD3333C3DD3333C3DC333344DC333344D333333DD333333DC333333DC333333433333333333333333333
GroupIndex OnClickTitleStyleBtnClickParentShowHintShowHint	  TColumnListboxLegendListboxLeftTop� Width�Height� Columns	Separator		LineBreakMinWidthOnPaintLegendListboxColumns0PaintWidth AutoSize		Separator		LineBreakMinWidth� OnPaintLegendListboxColumns1PaintWidth�  	LineBreakMinWidth� OnPaintLegendListboxColumns2PaintWidth�   
ItemHeightMultiSelect		PopupMenu
PopupMenu1
Scrollbars
ssVerticalTabOrderOnClickLegendListboxClick
OnDblClickLegendListboxDblClick
OnDragDropLegendListboxDragDrop
OnDragOverLegendListboxDragOver
OnDrawItemLegendListboxDrawItemOnMouseDownLegendListboxMouseDownOnMouseMoveLegendListboxMouseMove	OnMouseUpLegendListboxMouseUp  TEdit	TitleEditLeftHTop&WidthHeightTabOrderOnChangeTitleEditChange  	TComboBoxLegendsComboLeftHTopWidthHeightStylecsDropDownList
ItemHeightTabOrder OnChangeLegendsComboChange  TEdit
SearchEditLefthTopgWidthqHeightTabOrderOnChangeSearchEditChange  	TCheckBoxchkScBarLeftTopAWidth� HeightCaption!44TabOrderOnClickchkScBarClick  	TCheckBox	chkTranspLeft� TopAWidth� HeightCaption!45TabOrderOnClickchkTranspClick   TButtonOkBtnLeft'TopWidthaHeightCaption!35Default	ModalResultTabOrder  TButton	CancelBtnLeft�TopWidthaHeightCancel	Caption!36ModalResultTabOrder  
TPopupMenu
PopupMenu1Left� Top 	TMenuItem	AddEntry1Caption!9OnClickAddEntry1Click  	TMenuItemDeleteEntry1Caption!10OnClickDeleteEntry1Click  	TMenuItemN2Caption-  	TMenuItemChangeText1Caption!11OnClickChangeText1Click  	TMenuItemChooseSymbol1Caption!12OnClickChooseSymbol1Click  	TMenuItemN1Caption-  	TMenuItem
MoveToTop1Caption!13OnClickMoveToTop1Click  	TMenuItemMoveToBottom1Caption!14OnClickMoveToBottom1Click  	TMenuItemUp1Caption!15OnClickUp1Click  	TMenuItemDown1Caption!16OnClick
Down1Click   TMlgSectionMlgSection1SectionLegendDialogsLeft� Top  TInplaceEditorInplaceEditorAutoEdit	IgnoreCursorKeysListboxLegendListbox	OnEditingInplaceEditorEditingOnEditedInplaceEditorEditedLeft Top   