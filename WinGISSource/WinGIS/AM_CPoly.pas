{****************************************************************************}
{ Unit AM_CPoly                                                              }
{----------------------------------------------------------------------------}
{ Grafikobjekt für Flächen                                                   }
{----------------------------------------------------------------------------}
{ Modifikationen                                                             }
{  16.02.1994 Martin Forst                                                   }
{  12.04.1994 Martin Forst   Selektieren mit Kreis und Poly                  }
{  12.06.1994 Martin Forst   ClosePoly-Abfrage ob nicht bereits geschlossen  }
{  29.09.2005 Mariacher      Add new method PointInsideWithoutBorder         }
{****************************************************************************}
unit AM_CPoly;

interface

uses WinTypes, WinProcs, AM_Def, AM_View, AM_Poly, AM_Paint, AM_Index, AM_Ini, Objects,
  AM_Layer, Clipping, ExtCanvas;

const
  MaxDist = 20;
  NeighDist = 0;

type
  PCPoly = ^TCPoly;
  TCPoly = object(TPoly)
    IslandInfo: PLongArray;
    IslandCount: Integer16;
    constructor Init;
    constructor Load(S: TOldStream);
    constructor LoadOld(S: TOldStream);
    procedure ClosePoly; virtual;
    procedure ReversePoly; virtual; // Glukhov Bug#84 BUILD#125 01.11.00
    function DeletePointByIndex(PointInd: Integer): Boolean; virtual;
    function Direction: Boolean;
    procedure Draw(PInfo: PPaint; Clipped: Boolean); virtual;
    procedure EliminatePoints(Radius: LongInt); virtual;
    procedure Explode(PInfo: PPaint; ALayer: PLayer); virtual;
    procedure FreeIslandInfo;
    function GetObjType: Word; virtual;
    procedure GetClippedPoints(PInfo: PPaint; var Points: TClipPointList); virtual;
    function GetNextPoint(PointInd: Integer; var Point: TDPoint): Boolean; virtual;
    function GetPreviousPoint(PointInd: Integer; var Point: TDPoint): Boolean; virtual;
    procedure InsertPointByIndex(var PointInd: Integer; Point: TDPoint); virtual;
    procedure MovePoint(PointInd: Integer; Point: TDPoint; ReCalcClipRect: boolean = true); virtual;
    function PointInside(PInfo: PPaint; Point: TDPoint): Boolean;
    function PointInsideWithoutBorder(PInfo: PPaint; Point: TDPoint): Boolean; // Mariacher 29-09-2005. New method the check if point is INSIDE poly (ignore points on border)
    function SelectByCircle(PInfo: PPaint; Middle: TDPoint;
      Radius: LongInt; const ObjType: TObjectTypes; Inside: Boolean): Boolean; virtual;
    function SelectByNeighbour(PInfo: PPaint; Item: PIndex): Boolean; virtual;
    function SelectByPoint(PInfo: PPaint; Point: TDPoint; const ObjType: TObjectTypes): PIndex; virtual;
    function Flaeche(ADiv: Integer = 1): Double;
    function GetCenter: TDPoint;
    procedure CheckPoly;
    function ReadWKT(WKTString: AnsiString): Boolean;
    function ToWKT: AnsiString;
    procedure Store(S: TOldStream); virtual;
    destructor Done; virtual;
  end;

  PSCPoly = ^TSCPoly;
  TSCPoly = object(TCPoly)
    constructor Load(S: TOldStream);
    constructor LoadOld(S: TOldStream);
    procedure Store(S: TOldStream); virtual;
    procedure GetClippedPoints(PInfo: PPaint; var Points: TClipPointList); virtual;
    procedure GetPoints(PInfo: PPaint; var Points: PPoints); virtual;
  end;

procedure ProcCreateIslandArea(PProject: Pointer; DestLayer: PLayer = nil);

procedure ProcCreateIslandAreaWithoutDlg(PProject: Pointer; SourceLayer, DestinatLayer: PLayer);

implementation

uses AM_Main, AM_ProjM, AM_Proj, Math, AM_ProjO, SysUtils
{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}
  , AM_DDE
{$ENDIF}
{$ENDIF} // <----------------- AXDLL
  , AM_Sym, Islands, ResDlg, GrTools, Forms, Messages,
  Classes, XFills, XLines, XStyles, Controls, LayerHndl, SymbolFill, ToolsLib;

procedure ProcCreateIslandArea
  (
  PProject: Pointer;
  DestLayer: PLayer = nil
  );
var
  Proj: PProj;
  A1, A2, A3: PCPoly;
  P1: PIndex;
  SelLayer: PLayer;
  Tmp: string;
  NewArea: Double;
  Island1: PIslandStruct;
  Island2: PIslandStruct;
  Cnt: LongInt;
  OldTop: PLayer;
  PointCnt: Integer;
begin
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
  Proj := PProj(PProject);
  if DestLayer = nil then
    DestLayer := SelectLayerDialog(Proj, [sloNewLayer]);
  if DestLayer <> nil then
    with Proj^ do
    begin
      SetCursor(crHourGlass);
      SelLayer := Proj^.Layers^.SelLayer;
      if (SelLayer^.Data^.GetCount >= 2) then
      begin
        P1 := PIndex(SelLayer^.Data^.At(0));
        A1 := PCPoly(SelLayer^.IndexObject(Proj^.PInfo, P1));
        PointCnt := A1^.Data^.Count;
        Island1 := PolyToIslandStruct(Proj^.PInfo, A1);
        for Cnt := 1 to SelLayer^.Data^.GetCount - 1 do
        begin
          P1 := PIndex(SelLayer^.Data^.At(Cnt));
          A2 := PCPoly(SelLayer^.IndexObject(Proj^.PInfo, P1));
          PointCnt := PointCnt + A2^.Data^.Count + 1;
          if PointCnt >= MaxCollectionSize then
            Break;
          Island2 := PolyToIslandStruct(Proj^.PInfo, A2);
          Island1 := InsertIsland(Proj^.PInfo, Island1, Island2);
        end;
        if PointCnt >= MaxCollectionSize then
          MsgBox(PInfo^.HWindow, 15020, mb_Ok + mb_IconExclamation, '')
        else
        begin
          A3 := IslandStructToPoly(Island1, TRUE);
          if A3 <> nil then
          begin
            Proj^.InsertObjectOnLayer(A3, DestLayer^.Index);
            if DDEHandler.FLayerInfo = 1 then
              DDEHandler.StartList('[NIA][' + PToStr(DestLayer^.Text) + '][   ]')
            else
              if DDEHandler.FLayerInfo = 2 then
              begin
                Str(DestLayer^.Index: 10, Tmp);
                DDEHandler.StartList('[NIA][' + Tmp + '][   ]');
              end
              else
                DDEHandler.StartList('[NIA][   ]');
            for Cnt := 0 to SelLayer^.Data^.GetCount - 1 do
            begin
              P1 := PIndex(SelLayer^.Data^.At(Cnt));
              DDEHandler.AppendList(P1^.Index);
            end;
            DDEHandler.AppendList(A3^.Index);
            NewArea := A3^.Flaeche;
            Str(NewArea: 0: 2, Tmp);
            DDEHandler.AppendString(Tmp);
            DDEHandler.EndList(TRUE);
            if (DDEHandler.DoAutoIns) {and (DDEHandler.FDBAutoIn)} then
            begin
              OldTop := Proj^.Layers^.TopLayer;
              Proj^.Layers^.TopLayer := DestLayer;
              Proj^.DBEditAuto(PIndex(A3));
              Proj^.Layers^.TopLayer := OldTop;
            end;
          end;
        end;
        DestroyIslandStruct(Island1, TRUE);
      end;
      SetCursor(crDefault);
    end;
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
end;

procedure ProcCreateIslandAreaWithoutDlg
  (
  PProject: Pointer;
  SourceLayer: PLayer;
  DestinatLayer: PLayer
  );
var
  Proj: PProj;
  A1, A2, A3: PCPoly;
  P1: PIndex;
  SelLayer: PLayer;
  Island1: PIslandStruct;
  Island2: PIslandStruct;
  Cnt: LongInt;
  DestLayer: PLayer;
  IItem: PIndex;
begin
  Proj := PProj(PProject);
  SelLayer := SourceLayer;
  DestLayer := DestinatLayer;
  if (SelLayer^.Data^.GetCount >= 2) then
  begin
    P1 := PIndex(SelLayer^.Data^.At(0));
    A1 := PCPoly(SelLayer^.IndexObject(Proj^.PInfo, P1));
    Island1 := PolyToIslandStruct(Proj^.PInfo, A1);
    for Cnt := 1 to SelLayer^.Data^.GetCount - 1 do
    begin
      P1 := PIndex(SelLayer^.Data^.At(Cnt));
      A2 := PCPoly(SelLayer^.IndexObject(Proj^.PInfo, P1));
      Island2 := PolyToIslandStruct(Proj^.PInfo, A2);
      Island1 := InsertIsland(Proj^.PInfo, Island1, Island2);
    end;
    A3 := IslandStructToPoly(Island1, TRUE);
    if A3 <> nil then
      Proj^.InsertObjectOnLayer(A3, DestLayer^.Index);
    DestroyIslandStruct(Island1, TRUE);
  end
  else
  begin
    if (SelLayer^.Data^.GetCount = 1) then
    begin
      if SelLayer^.Data^.At(0)^.GetObjType = ot_CPoly then
      begin
        IItem := New(PIndex, Init(SelLayer^.Data^.At(0)^.Index));
        DestLayer^.InsertObject(Proj^.PInfo, IItem, FALSE);
        Proj^.Layers^.IndexObject(Proj^.PInfo, SelLayer^.Data^.At(0))^.Invalidate(Proj^.PInfo);
      end;
    end;
  end;
end;

{**************************************** TCPoly ****************************************}

constructor TCPoly.Init;
begin
  inherited Init;
  IslandInfo := nil;
  IslandCount := 0;
end;

constructor TCPoly.LoadOld(S: TOldStream);
begin
  inherited LoadOld(S);
  IslandInfo := nil;
  IslandCount := 0;
end;

constructor TCPoly.Load
  (
  S: TOldStream
  );
begin
  inherited Load(S);
  if (GetState(sf_IslandArea)) then
  begin {Inselfläche!}
    S.Read(IslandCount, sizeof(IslandCount)); { Anzahl Inselflächen }
    GetMem(IslandInfo, IslandCount * sizeof(IslandInfo^[0]));
    S.Read(IslandInfo^, IslandCount * sizeof(IslandInfo^[0]));
  end
  else
  begin
    IslandInfo := nil;
    IslandCount := 0;
  end;
end;

procedure TCPoly.Store
  (
  S: TOldStream
  );
begin
  inherited Store(S);
  if GetState(sf_IslandArea) then
  begin {Inselflächeninfo speichern}
    S.Write(IslandCount, sizeof(IslandCount)); { Anzahl Inselflächen }
    S.Write(IslandInfo^, sizeof(IslandInfo^[0]) * IslandCount); {Inselinfo }
  end;
end;

destructor TCPoly.Done;
begin
  FreeIslandInfo;
  inherited Done;
end;

type
  PPPoints = ^TPPoints;
  TPPoints = array[0..0] of TPoint;

procedure TCPoly.CheckPoly;
var
  IslandInfoSum, AllPointsCount: Integer;
  i: Integer;
begin
  if GetState(sf_IslandArea) and (IslandCount > 0) then
  begin
    AllPointsCount := Data^.Count;
    IslandInfoSum := 0;
    for i := 0 to IslandCount - 1 do
    begin
      Inc(IslandInfoSum, IslandInfo^[i]);
    end;
    if (AllPointsCount < (IslandInfoSum + (IslandCount - 1))) then
    begin
      IslandCount := 0;
      SetState(sf_IslandArea, False);
    end;
  end;
end;

procedure TCPoly.Draw
  (
  PInfo: PPaint;
  Clipped: Boolean
  );
var
  Points: PPoints;
  ListCapacity: Integer;
  Cnt: Integer;
  DrawPoints: Integer;
  OldPen: THandle;
  OldBrush: THandle;
  OldR2: THandle;
  Start: Integer;
  NewIslandInfo: PLongArray;
  NewIslandCount: Integer;
  RPoint: TPoint;
  ClipPoints: TClipPointList;
  GrPoint: TGrPoint;

  procedure SwitchToDCMode;
  var
    Size: TSize;
  begin
    if not PInfo^.ExtCanvas.PointsConverted then
      PInfo^.ExtCanvas.ConvertToXStylePoints(Points^, DrawPoints);
    SetMapMode(PInfo^.ExtCanvas.Handle, mm_Text);
  end;

  procedure DoDrawHandles
      (
      Start: Integer;
      Stop: Integer
      );
  var
    Cnt: Integer;
    AAngle: Double;
    AGrPoint, BGrPoint, CGrPoint: TGrPoint;
  begin
    for Cnt := Start + 1 to Stop do
    begin
      with Points^[Cnt] do
      begin
        if (X > 0) and (Y > 0)
          and (X < PInfo^.ExtCanvas.PhysicalSize.Right) and (Y < PInfo^.ExtCanvas.PhysicalSize.Bottom) then
        begin

          if Cnt <= (Stop - 1) then
          begin
            Rectangle(PInfo^.ExtCanvas.Handle, X - HandleSize, Y - HandleSize, X + HandleSize1, Y + HandleSize1);
//                  TextOut(PInfo^.ExtCanvas.Handle,X,Y,PChar(IntToStr(Cnt-1)),StrLen(PChar(IntToStr(Cnt-1))));
          end;

               { Direction - Mark }

          if Cnt > Start + 1 then
          begin
            AAngle := LineAngle(GrTools.GrPoint(X, Y), GrTools.GrPoint(Points^[Cnt - 1].X, Points^[Cnt - 1].Y));
            AGrPoint := GrTools.GrPoint(X, Y);
            GrTools.MovePointAngle(AGrPoint, 30, AAngle);
            MoveToEx(PInfo^.ExtCanvas.Handle, Round(AGrPoint.X), Round(AGrPoint.Y), nil);
            BGrPoint := GrTools.GrPoint(AGrPoint.X, AGrPoint.Y);
            GrTools.MovePointAngle(BGrPoint, 18, AAngle + Pi / 8 + Pi);
            LineTo(PInfo^.ExtCanvas.Handle, Round(BGrPoint.X), Round(BGrPoint.Y));
            MoveToEx(PInfo^.ExtCanvas.Handle, Round(AGrPoint.X), Round(AGrPoint.Y), nil);
            CGrPoint := GrTools.GrPoint(AGrPoint.X, AGrPoint.Y);
            GrTools.MovePointAngle(CGrPoint, 18, AAngle - PI / 8 + Pi);
            LineTo(PInfo^.ExtCanvas.Handle, Round(CGrPoint.X), Round(CGrPoint.Y));
            MoveToEx(PInfo^.ExtCanvas.Handle, Round(BGrPoint.X), Round(BGrPoint.Y), nil);
            LineTo(PInfo^.ExtCanvas.Handle, Round(CGrPoint.X), Round(CGrPoint.Y));
          end;

               { End }

        end;
      end;
    end;
  end;

  procedure DrawCenter;
  const
    LineLen = 7;
  var
    Pnt: TDPoint;
    APoint: TPoint;
  begin
    PInfo^.ConvertToDisp(GetCenter, APoint);
    with PInfo^.ExtCanvas do
    begin
      Pen.Style := lt_Solid;
      MoveTo(APoint.X - LineLen, APoint.Y - LineLen);
      LineTo(APoint.X + LineLen + 1, APoint.Y + LineLen + 1);
      MoveTo(APoint.X + LineLen, APoint.Y - LineLen);
      LineTo(APoint.X - LineLen + 1, APoint.Y + LineLen + 1);
    end;
  end;

  procedure SetPointsCapacity
      (
      ACapacity: Integer
      );
  var
    HelpPtr: PPoints;
  begin
    if ACapacity > ListCapacity then
    begin
      if ListCapacity > 0 then
      begin
        GetMem(HelpPtr, ACapacity * SizeOf(TPoint));
        Move(Points^, HelpPtr^, ListCapacity * SizeOf(TPoint));
        FreeMem(Points, ListCapacity * SizeOf(TPoint));
        Points := HelpPtr;
      end
      else
        GetMem(Points, ACapacity * SizeOf(TPoint));
      ListCapacity := ACapacity;
    end;
  end;

  procedure DoClipIslands
      (
      Start: Integer;
      Stop: Integer
      );
  var
    Cnt: Integer;
    GrPoint: TGrPoint;
  begin
    ClipPoints.Clear;
      {++Sygsky: Cut too long call }
    if (Stop - 1) >= Data.Count then
      Stop := Data.Count;
      {--Sygsky}
    for Cnt := Start + 1 to Stop - 1 do
    begin
      PInfo^.ConvertToDispDouble(PDPoint(Data^.At(Cnt))^, GrPoint.X, GrPoint.Y);
      ClipPoints.Add(GrPoint);
    end;
    ClipPolygon(ClipPoints, PInfo^.ClipRect);
    if ClipPoints.Count > 0 then
    begin
      SetPointsCapacity(DrawPoints + ClipPoints.Count + 1);
      for Cnt := 0 to ClipPoints.Count - 1 do
      begin
        GrPoint := ClipPoints[Cnt];
        Points^[DrawPoints + Cnt + 1] := Point(Round(GrPoint.X), Round(GrPoint.Y));
      end;
      NewIslandInfo^[NewIslandCount] := ClipPoints.Count;
      if NewIslandCount = 0 then
      begin
        RPoint := Points^[1];
        DrawPoints := DrawPoints + ClipPoints.Count;
      end
      else
      begin
        Points^[DrawPoints + ClipPoints.Count + 1] := RPoint;
        DrawPoints := DrawPoints + ClipPoints.Count + 1;
      end;
      Inc(NewIslandCount);
    end;
  end;
begin
// ++ Cadmensky
  Points := nil;
// -- Cadmensky

  if PInfo^.PreViewing then
  begin
    Clipped := False;
  end;

  CheckPoly;

  if Clipped then
  begin

    if GetState(sf_IslandArea) then
    begin
         { clip island-areas }
      ListCapacity := 0;
      SetPointsCapacity(Data^.Count);
      DrawPoints := 0;
      NewIslandCount := 0;
      GetMem(NewIslandInfo, IslandCount * SizeOf(LongInt));
      ClipPoints := TClipPointList.Create;
      Start := IslandInfo^[0];
      DoClipIslands(0, Start);
      for Cnt := 1 to IslandCount - 1 do
      begin
        DoClipIslands(Start, Start + IslandInfo^[Cnt]);
        Start := Start + IslandInfo^[Cnt] + 1;
      end;
    end
    else
    begin
         { clip normal areas }
      GetClippedPoints(PInfo, ClipPoints);
      ClipPolygon(ClipPoints, PInfo^.ClipRect);
      DrawPoints := ClipPoints.Count;
      if DrawPoints > 0 then
      begin
        GetMem(Points, Word(SizeOf(TPoint)) * DrawPoints);
        for Cnt := 0 to DrawPoints - 1 do
        begin
          GrPoint := ClipPoints[Cnt];
          Points^[Cnt + 1] := Point(Round(GrPoint.X), Round(GrPoint.Y));
        end;
      end;
    end;
    ListCapacity := DrawPoints;
    ClipPoints.Free;
  end
  else
  begin
    GetPoints(PInfo, Points);
    DrawPoints := Data^.Count;
    ListCapacity := DrawPoints;
    NewIslandInfo := IslandInfo;
    NewIslandCount := IslandCount;
  end;
  if DrawPoints > 0 then
  begin
    if not GetState(sf_IslandArea) then
    begin
      if PInfo^.ActSymbolFill.FillType = symfNone then
        PInfo^.ExtCanvas.Polygon(
          Points^, DrawPoints, TRUE)
      else
        PInfo^.ExtCanvas.Polygon(Points^, DrawPoints, TRUE,
          @SymbolFillRectangle, LongInt(PInfo), Integer(@ClipRect), 0)
    end
    else
    begin
      if PInfo^.ActSymbolFill.FillType = symfNone then
        PInfo^.ExtCanvas.IslandArea(
          Points^, DrawPoints, NewIslandInfo^, NewIslandCount)
      else
        PInfo^.ExtCanvas.IslandArea(Points^, DrawPoints, NewIslandInfo^, NewIslandCount,
          @SymbolFillRectangle, LongInt(PInfo), Integer(@ClipRect), 0);
    end;
    if PInfo^.GetDrawMode(dm_EditPoly) then
    begin
      SwitchToDCMode;
      OldPen := SelectObject(PInfo^.ExtCanvas.Handle, GetStockObject(Black_Pen));
      OldBrush := SelectObject(PInfo^.ExtCanvas.Handle, GetStockObject(Hollow_Brush));
      OldR2 := SetRop2(PInfo^.ExtCanvas.Handle, r2_NOTXORPen);
      if GetState(sf_IslandArea) then
      begin
        Start := NewIslandInfo^[0];
        DoDrawHandles(0, Start);
        for Cnt := 1 to NewIslandCount - 1 do
        begin
          DoDrawHandles(Start, Start + NewIslandInfo^[Cnt]);
          Start := Start + NewIslandInfo^[Cnt] + 1;
        end;
      end
      else
        DoDrawHandles(0, DrawPoints);

      SelectObject(PInfo^.ExtCanvas.Handle, OldPen);
      SelectObject(PInfo^.ExtCanvas.Handle, OldBrush);
      SetRop2(PInfo^.ExtCanvas.Handle, OldR2);
         { reset the map-mode }
      SetMapMode(PInfo^.ExtCanvas.Handle, mm_LoMetric);
    end;

      //DrawCenter;

  end;
  if Clipped and GetState(sf_IslandArea) then
    FreeMem(NewIslandInfo, IslandCount * SizeOf(LongInt));
  FreeMem(Points);
{   if ListCapacity > 0 then
      FreeMem (Points, Word (ListCapacity) * SizeOf (TPoint))
// ++ Cadmensky
   else
      FreeMem (Points)}
// -- Cadmensky
end;

procedure TCPoly.FreeIslandInfo;
begin
  if (GetState(sf_IslandArea) and (IslandInfo <> nil)) then
  try FreeMem(IslandInfo, IslandCount * sizeof(LongInt));
  except
  end;
  IslandCount := 0;
end;

function TCPoly.Direction: Boolean;
var
  F: Double;
  Count: Integer;
begin
  F := 0.0;
  for Count := 1 to Data^.Count - 1 do
    F := F + ((0.0 + PDPoint(Data^.At(Count))^.X + PDPoint(Data^.At(Count - 1))^.X) *
      (0.0 + PDPoint(Data^.At(Count))^.Y - PDPoint(Data^.At(Count - 1))^.Y));
  if (F > 0) then
    Direction := True
  else
    Direction := False;
end;

function TCPoly.SelectByPoint
  (
  PInfo: PPaint;
  Point: TDPoint;
  const ObjType: TObjectTypes
  )
  : PIndex;
begin
  SelectByPoint := nil;
  if (ot_CPoly in ObjType) and
    PointInside(PInfo, Point) then
    SelectByPoint := @Self;
end;

function TCPoly.PointInside
  (
  PInfo: PPaint;
  Point: TDPoint
  )
  : Boolean;
var
  CutCnt: Integer;
  Cnt: Integer;
begin
  Result := FALSE;
  if (ClipRect.PointInside(Point)) then
  begin
    CutCnt := 0;
    for Cnt := 0 to Data^.Count - 2 do
      if PInfo^.GetCut(Data^.At(Cnt), Data^.At(Cnt + 1), @Point) then
        Inc(CutCnt);
    if (CutCnt mod 2) = 1 then
      Result := TRUE
    else
      Result := CheckOnBorder(Point);
  end;
end;

// Mariacher 28-09-2005
// Support method to check if point is inside polygon, but not on border line

function TCPoly.PointInsideWithoutBorder
  (
  PInfo: PPaint;
  Point: TDPoint
  )
  : Boolean;
var
  CutCnt: Integer;
  Cnt: Integer;
begin
  Result := FALSE;
  if (ClipRect.PointInside(Point)) then
  begin
    CutCnt := 0;
    for Cnt := 0 to Data^.Count - 2 do
      if PInfo^.GetCut(Data^.At(Cnt), Data^.At(Cnt + 1), @Point) then
        Inc(CutCnt);
    if (CutCnt mod 2) = 1 then
    begin
      if not CheckOnBorder(Point) then
        Result := TRUE;
    end;
  end;
end;

function TCPoly.SelectByNeighbour
  (
  PInfo: PPaint;
  Item: PIndex
  )
  : Boolean;

  function IsNeighbour
      (
      Item1: PCPoly;
      Item2: PCPoly
      )
      : Boolean;

    function CheckInside
        (
        Item: PDPoint
        )
        : Boolean; far;
    begin
      CheckInside := Item2^.PointInside(PInfo, Item^);
    end;

    function CheckLines
        (
        Item: PDPoint
        )
        : Boolean; far;
    begin
      CheckLines := Item2^.CheckNormalDist(PInfo, Item, NeighDist);
    end;

  begin
    IsNeighbour := (Item1^.Data^.FirstThat(@CheckLines) <> nil) or
      (Item1^.Data^.FirstThat(@CheckInside) <> nil);
  end;

  function CheckPoints
      (
      AItem: PDPoint
      )
      : Boolean; far;

  begin
    CheckPoints := PCPoly(Item)^.CheckPointDist(AItem, MaxDist);
  end;

var
  Clip: TDRect;
  Found: Boolean;
begin
  if not GetState(sf_UnVisible) then
  begin
    Clip.Init;
    Clip := ClipRect;
    Clip.A.Move(-NeighDist, -NeighDist);
    Clip.B.Move(NeighDist, NeighDist);
    Found := FALSE;
    if Clip.IsVisible(PView(Item)^.ClipRect) then
    begin
      Found := IsNeighbour(@Self, PCPoly(Item)) or IsNeighbour(PCPoly(Item), @Self);
      if not (Found) then
        Found := Data^.FirstThat(@CheckPoints) <> nil;
    end;
    Clip.Done;
    SelectByNeighbour := Found;
  end
  else
    Result := FALSE;
end;

function TCPoly.Flaeche(ADiv: Integer = 1): Double;
var
  F: Double;
  i: Integer;
begin
  F := 0.0;
  for i := 1 to Data^.Count - 1 do
  begin
    F := F + ((0.0 + PDPoint(Data^.At(i))^.X + PDPoint(Data^.At(i - 1))^.X) * (0.0 + PDPoint(Data^.At(i))^.Y - PDPoint(Data^.At(i - 1))^.Y));
  end;
{$IFNDEF AXDLL}
  if ADiv = 1 then
  begin
    Result := Abs(F / 20000 / IniFile^.AreaFact);
  end;
{$ELSE}
  Result := Abs(F / 20000);
{$ENDIF}
end;

function TCPoly.GetCenter: TDPoint;
var
  i, k, si, ni, nk: Integer;
  P1, P2: TGrPoint;
  Center: TDPoint;
  LastLen, Len: Integer;
  Step: Integer;
begin
  if Data^.Count <= 4 then
  begin
    P1.X := 0;
    P1.Y := 0;
    for i := 1 to Data^.Count - 1 do
    begin
      P1.X := P1.X + PDPoint(Data^.At(i))^.X;
      P1.Y := P1.Y + PDPoint(Data^.At(i))^.Y;
    end;
    if Data^.Count <= 1 then
    begin
      Result.Init(PDPoint(Data^.At(0))^.X, PDPoint(Data^.At(0))^.Y);
    end
    else
    begin
      Result.Init(Trunc(P1.X / (Data^.Count - 1)), Trunc(P1.Y / (Data^.Count - 1)));
    end;
  end
  else
  begin
    Center.X := (ClipRect.A.X + ClipRect.B.X) div 2;
    Center.Y := (ClipRect.A.Y + ClipRect.B.Y) div 2;
    if PointInside(WingisMainForm.ActualChild.Data^.PInfo, Center) then
    begin
      Result.Init(Center.X, Center.Y);
    end
    else
    begin
      LastLen := 0;
      ni := Data^.Count - 3;
      if GetState(sf_IslandArea) then
      begin
        ni := IslandInfo^[0] - 3;
      end;
      si := 1;
      if GetState(sf_IslandArea) then
      begin
        ni := IslandInfo^[0];
        for i := 0 to IslandCount - 2 do
        begin
          if IslandInfo^[i + 1] - IslandInfo^[i] > ni then
          begin
            ni := IslandInfo^[i + 1] - IslandInfo^[i];
            si := IslandInfo^[i];
          end;
        end;
      end;
      if ni > 50 then
      begin
        Step := ni div Trunc(Log2(ni));
        Step := Max(Step, 1);
      end
      else
      begin
        Step := 1;
      end;
      i := si;
      while i <= ni do
      begin
        nk := ni + 1;
        if i > 1 then
        begin
          Inc(nk);
        end;
        k := i + 2;
        while k <= nk do
        begin
          P1.X := PDPoint(Data^.At(i))^.X;
          P1.Y := PDPoint(Data^.At(i))^.Y;
          P2.X := PDPoint(Data^.At(k))^.X;
          P2.Y := PDPoint(Data^.At(k))^.Y;
          Len := Trunc(Sqrt(Sqr(P1.X - P2.X) + Sqr(P1.Y - P2.Y)));
          if Len > LastLen then
          begin
            Center.Init(Trunc((P1.X + P2.X) / 2), Trunc((P1.Y + P2.Y) / 2));
            if PointInside(WingisMainForm.ActualChild.Data^.PInfo, Center) then
            begin
              LastLen := Len;
              Result.Init(Center.X, Center.Y);
            end;
          end;
          k := k + Step;
        end;
        i := i + Step;
      end;
      if LastLen = 0 then
      begin
        Result.Init(PDPoint(Data^.At(1))^.X, PDPoint(Data^.At(1))^.Y);
      end;
    end
  end;
end;

function TCPoly.GetObjType
  : Word;
begin
  GetObjType := ot_CPoly;
end;

function TCPoly.SelectByCircle
  (
  PInfo: PPaint;
  Middle: TDPoint;
  Radius: LongInt;
  const ObjType: TObjectTypes;
  Inside: Boolean
  )
  : Boolean;
var
  Clip: TDRect;
  Cnt: Integer;
begin
  if not GetState(sf_UnVisible) then
  begin
    SelectByCircle := FALSE;
    if ot_CPoly in ObjType then
    begin
      Clip.Init;
      Clip.Assign(Middle.X - Radius, Middle.Y - Radius,
        Middle.X + Radius, Middle.Y + Radius);
      if Inside then
      begin
        if ClipRect.IsInside(Clip) then
        begin
          for Cnt := 1 to Data^.Count - 1 do
            if PDPoint(Data^.At(Cnt))^.Dist(Middle) > Radius then
              Exit;
          SelectByCircle := TRUE;
        end;
      end
      else
        SelectByCircle := ClipRect.IsVisible(Clip)
          and (CheckPointDist(@Middle, Radius)
          or CheckNormalDist(PInfo, @Middle, Radius)
          or PointInside(PInfo, Middle));
    end;
  end
  else
    Result := FALSE;
end;

procedure TCPoly.ClosePoly;
var
  First: PDPoint;
  Last: PDPoint;
begin
  First := Pointer(Data^.At(0));
  Last := Pointer(Data^.At(Data^.Count - 1)); {1206F}
  if (First^.X <> Last^.X)
    or (First^.Y <> Last^.Y) then
    Data^.Insert(First); {1206F}
end;

//++ Glukhov Bug#84 BUILD#125 01.11.00

procedure TCPoly.ReversePoly;
var
  Island: Integer;
  Start: Integer;
  Stop: Integer;
  Point: PDPoint;
  i, Cnt: Integer;
  x, y: longint;
begin
  if not GetState(sf_IslandArea) then
  begin
      // A closed polyline
    inherited ReversePoly;
    Exit;
  end;
   // Init 1st island data
  Island := 0;
  Start := 0;
  Stop := IslandInfo^[0] - 1;
   // For every island do
  while Island < IslandCount do
  begin
      // Reverse the island points but the 1st and last ones
    Cnt := (Stop - Start + 1) div 2 - 1;
    for i := 1 to Cnt do
    begin
      x := Data.Items[Start + i].X;
      y := Data.Items[Start + i].Y;
      Data.Items[Start + i].Init(Data.Items[Stop - i].X, Data.Items[Stop - i].Y);
      Data.Items[Stop - i].Init(x, y);
    end;
      // Prepare data for the next island
    Inc(Island);
    if Island = 1 then
      Start := Stop + 1
    else
      Start := Stop + 2;
    Stop := Start + IslandInfo^[Island] - 1;
  end;
end;
//-- Glukhov Bug#84 BUILD#125 01.11.00

function TCPoly.GetNextPoint
  (
  PointInd: Integer;
  var Point: TDPoint
  )
  : Boolean;
begin
  GetNextPoint := TRUE;
  Point.Init(0, 0);
  if PointInd = Data^.Count - 1 then
    Point := PDPoint(Data^.At(1))^
  else
    Point := PDPoint(Data^.At(PointInd + 1))^;
end;

function TCPoly.GetPreviousPoint
  (
  PointInd: Integer;
  var Point: TDPoint
  )
  : Boolean;
var
  Start: Integer;
  Stop: Integer;
  Island: Integer;
begin
  GetPreviousPoint := TRUE;
  Point.Init(0, 0);
  if GetState(sf_IslandArea) then
  begin
    Start := 0;
    Stop := IslandInfo^[0] - 1;
    Island := 0;
    while (Stop < PointInd)
      and (Island < IslandCount) do
    begin
      Inc(Island);
      if Island = 1 then
        Start := Stop + 1
      else
        Start := Stop + 2;
      Stop := Start + IslandInfo^[Island] - 1;
    end;
    if PointInd = Start then
      Point := PDPoint(Data^.At(Stop - 1))^
    else
      Point := PDPoint(Data^.At(PointInd - 1))^;
  end
  else
  begin
    if PointInd = 0 then
      Point := PDPoint(Data^.At(Data^.Count - 2))^
    else
      Point := PDPoint(Data^.At(PointInd - 1))^;
  end;
end;

procedure TCPoly.MovePoint
  (
  PointInd: Integer;
  Point: TDPoint;
  ReCalcClipRect: boolean = true
  );
var
  Start: Integer;
  Stop: Integer;
  Island: Integer;
begin
  if GetState(sf_IslandArea) then
  begin
    if PointInd = 0 then
    begin
      Stop := IslandInfo^[0] - 1;
      PDPoint(Data^.At(0))^ := Point;
      PDPoint(Data^.At(Stop))^ := Point;
      for Island := 1 to IslandCount - 1 do
      begin
        if Island = 1 then
          Stop := Stop + IslandInfo^[Island]
        else
          Stop := Stop + IslandInfo^[Island] + 1;
// ++ Cadmensky
        PDPoint(Data^.At(Stop + 1))^.Init(Point.X, Point.Y);
//            PDPoint (Data^.At (Stop + 1))^ := Point;
//  Cadmensky
      end;
    end
    else
    begin
      Island := 0;
      Start := 0;
      Stop := IslandInfo^[0] - 1;
      while Stop < PointInd do
      begin
        Inc(Island);
        if Island = 1 then
          Start := Stop + 1
        else
          Start := Stop + 2;
        Stop := Start + IslandInfo^[Island] - 1;
      end;
      if PointInd = Start then
      begin
// ++ Cadmensky
{            PDPoint (Data^.At (Start))^ := Point;
            PDPoint (Data^.At (Stop))^ := Point;}
        PDPoint(Data^.At(Start))^.Init(Point.X, Point.Y);
        PDPoint(Data^.At(Stop))^.Init(Point.X, Point.Y);
// -- Cadmensky
      end
      else
// ++ Cadmensky
        PDPoint(Data^.At(PointInd))^.Init(Point.X, Point.Y);
//            PDPoint (Data^.At (PointInd))^ := Point;
// -- Cadmensky
    end;
  end
  else
  begin
    if (PointInd = 0) or (PointInd = Data^.Count - 1) then
    begin
// ++ Cadmensky
      PDPoint(Data^.At(0))^.Init(Point.X, Point.Y);
      PDPoint(Data^.At(Data^.Count - 1))^.Init(Point.X, Point.Y);
{         PDPoint (Data^.At (0))^ := Point;
         PDPoint (Data^.At (Data^.Count - 1))^ := Point;}
// -- Cadmensky
    end
    else
// ++ Cadmensky
      PDPoint(Data^.At(PointInd))^.Init(Point.X, Point.Y);
//            PDPoint (Data^.At (PointInd))^ := Point;
// -- Cadmensky
  end;
  if ReCalcClipRect = true then
    CalculateClipRect;
end;

procedure TCPoly.InsertPointByIndex
  (
  var PointInd: Integer;
  Point: TDPoint
  );
var
  NewPoint: PDPoint;
  Island: Integer;
  Start: Integer;
  Stop: Integer;
begin
  if Data^.Count >= MaxCollectionSize then
    Exit;
  NewPoint := New(PDPoint, Init(Point.X, Point.Y));
  if GetState(sf_IslandArea) then
  begin
    Island := 0;
    Start := 0;
    Stop := IslandInfo^[0] - 1;
    while Stop < PointInd do
    begin
      Inc(Island);
      if Island = 1 then
        Start := Stop + 1
      else
        Start := Stop + 2;
      Stop := Start + IslandInfo^[Island] - 1;
    end;
    if PointInd = Start then
      PointInd := Stop;
    Data^.AtInsert(PointInd, NewPoint);
    Inc(IslandInfo^[Island]);
    if SnapInfo <> nil then
      SnapInfo^.InsertAndRenumber(PointInd);
    CalculateClipRect;
    UpdateData;
  end
  else
  begin
    if PointInd = 0 then
      PointInd := Data^.Count - 1;
    Data^.AtInsert(PointInd, NewPoint);
    if SnapInfo <> nil then
      SnapInfo^.InsertAndRenumber(PointInd);
    CalculateClipRect;
    UpdateData;
  end;
end;

function TCPoly.DeletePointByIndex
  (
  PointInd: Integer
  )
  : Boolean;
var
  Start: Integer;
  Stop: Integer;
  Island: Integer;
  Point: PDPoint;
begin
  Result := FALSE;
  if Data^.Count > 4 then
  begin
    if GetState(sf_IslandArea) then
    begin
      if PointInd = 0 then
      begin
        if IslandInfo^[0] > 4 then
        begin
          Stop := IslandInfo^[0] - 1;
          Point := Data^.At(1);
          PDPoint(Data^.At(Stop))^ := Point^;
          for Island := 1 to IslandCount - 1 do
          begin
            if Island = 1 then
              Stop := Stop + IslandInfo^[Island]
            else
              Stop := Stop + IslandInfo^[Island] + 1;
            ;
            PDPoint(Data^.At(Stop + 1))^ := Point^;
          end;
          Data^.AtFree(0);
          Dec(IslandInfo^[0]);
          Result := TRUE;
        end;
      end
      else
      begin
        Island := 0;
        Start := 0;
        Stop := IslandInfo^[0] - 1;
        while Stop < PointInd do
        begin
          Inc(Island);
          if Island = 1 then
            Start := Stop + 1
          else
            Start := Stop + 2;
          Stop := Start + IslandInfo^[Island] - 1;
        end;
        if Stop - Start > 3 then
        begin
          if PointInd = Start then
            PDPoint(Data^.At(Stop))^ := PDPoint(Data^.At(Start + 1))^;
          Data^.AtFree(PointInd);
          Dec(IslandInfo^[Island]);
          Result := TRUE;
        end;
      end;
    end
    else
    begin
      if (PointInd = 0) or (PointInd = Data^.Count - 1) then
      begin
        Data^.AtFree(0);
        Data^.AtFree(Data^.Count - 1);
        ClosePoly;
      end
      else
        Data^.AtFree(PointInd);
      Result := TRUE;
    end;
    if Result then
    begin
      if SnapInfo <> nil then
      begin
        SnapInfo^.DeleteAndRenumber(PointInd);
        if SnapInfo^.Count = 0 then
          Dispose(SnapInfo, Done);
      end;
      CalculateClipRect;
      UpdateData;
    end;
  end;
end;

function TCPoly.ReadWKT(WKTString: AnsiString): Boolean;
var
  i, k: Integer;
  Start, Stop: Integer;
  s: AnsiString;
  PointList, IslandList: TList;
  APoint: PDPoint;
  x, y: Integer;
begin
  Result := False;
  Start := Pos('(', WKTString);
  Stop := StrLen(PChar(WKTString));
  s := '';
  k := 2;
  PointList := TList.Create;
  IslandList := TList.Create;
  try
    for i := Start to Stop do
    begin
      if ((WKTString[i] >= Char('0')) and (WKTString[i] <= Char('9'))) or (WKTString[i] = '.') or (WKTString[i] = Char('-')) then
      begin
        s := s + WKTString[i];
      end
      else
        if (WKTString[i] = ' ') or (WKTString[i] = ',') or (WKTString[i] = ')') then
        begin
          if s <> '' then
          begin
            PointList.Add(Pointer(Round(SToF(s) * 100)));
            Inc(k);
          end;
          s := '';
          if (WKTString[i] = ')') and (WKTString[i - 1] <> ')') then
          begin
            PointList.Add(PointList[0]);
            PointList.Add(PointList[1]);
            IslandList.Add(Pointer(k div 2));
            k := 0;
          end;
        end
        else
          if (WKTString[i] <> '(') then
          begin
            break;
          end;
    end;
    for i := 0 to PointList.Count - 1 do
    begin
      if (i mod 2) = 0 then
      begin
        x := Integer(PointList[i]);
      end
      else
      begin
        y := Integer(PointList[i]);
        APoint := New(PDPoint, Init(x, y));
        Data^.Insert(APoint);
      end;
    end;
    ClosePoly;
    if IslandList.Count >= 2 then
    begin
      IslandCount := IslandList.Count;
      GetMem(IslandInfo, IslandCount * Sizeof(IslandInfo^[0]));
      for i := 0 to IslandCount - 1 do
      begin
        IslandInfo^[i] := Integer(IslandList[i]);
      end;
    end
    else
    begin
      IslandCount := 0;
    end;
    SetState(sf_IslandArea, (IslandCount > 0));
    CalculateClipRect;
    Result := (not ClipRect.IsEmpty) and (Data^.Count > 2);
  finally
    PointList.Free;
    IslandList.Free;
  end;
end;

function TCPoly.ToWKT: AnsiString;
var
  s: AnsiString;
  i, n: Integer;
  p: PDPoint;
  xy: AnsiString;
  ic, ii: Integer;
begin
  Result := '';
  n := Data^.Count;
  if IslandCount > 0 then
  begin
    s := 'MULTIPOLYGON ((';
    ii := 0;
    ic := IslandInfo^[ii] - 1;
    s := s + '(';
    for i := 0 to n - 1 do
    begin
      if i < ic then
      begin
        p := Data^.At(i);
        xy := FloatToStr(p^.x / 100) + ' ' + FloatToStr(p^.y / 100);
        s := s + xy;
        if (i < ic - 1) and (i < n - 1) then
        begin
          s := s + ', ';
        end;
      end;
      if (i = ic) and (i < n - 1) then
      begin
        s := s + '),(';
        Inc(ii);
        ic := ic + IslandInfo^[ii] + 1;
      end;
    end;
    s := s + ')';
  end
  else
  begin
    s := 'POLYGON ((';
    for i := 0 to n - 1 do
    begin
      p := Data^.At(i);
      xy := FloatToStr(p^.x / 100) + ' ' + FloatToStr(p^.y / 100);
      s := s + xy;
      if i < n - 1 then
      begin
        s := s + ', ';
      end;
    end;
  end;
  s := s + '))';
  Result := s;
end;

////////////////////////////////////////////////////////////////////////////////

constructor TSCPoly.Load
  (
  S: TOldStream
  );
var
  Dummy: Boolean;
begin
  TCPoly.Load(S);
  S.Read(Dummy, SizeOf(Dummy));
end;

constructor TSCPoly.LoadOld
  (
  S: TOldStream
  );
var
  Dummy: Boolean;
begin
  TCPoly.LoadOld(S);
  S.Read(Dummy, SizeOf(Dummy));
end;

procedure TSCPoly.Store
  (
  S: TOldStream
  );
var
  Dummy: Boolean;
begin
  TCPoly.Store(S);
  S.Write(Dummy, SizeOf(Dummy));
end;

procedure TSCPoly.GetPoints
  (
  PInfo: PPaint;
  var Points: PPoints
  );
var
  Cnt: Integer;
  PPoints: PPPoints absolute Points;
begin
  GetMem(Points, Word(SizeOf(TPoint)) * Data^.Count);
  for Cnt := 0 to Data^.Count - 1 do
    PInfo^.ScaleToDisp(PDPoint(Data^.At(Cnt))^, PPoints^[Cnt]);
end;

{*****************************************************************************************}
{ Procedure TCPoly.EliminatePoints                                                        }
{  Löscht Punkte, deren Abstand voneinander kleiner Radius ist.                           }
{*****************************************************************************************}

procedure TCPoly.EliminatePoints
  (
  Radius: LongInt
  );
var
  SelRect: TDRect;
  IsInside: Boolean;
  NewX: Double;
  NewY: Double;
  Cnt: Integer;
  APoint: PDPoint;
  BPoint: PDPoint;
  PointCnt: Integer;
  HasEnd: Boolean;
begin
  Cnt := 0;
  SelRect.Init;
  while (Cnt < Data^.Count - 1) and (Data^.Count > 4) do
  begin
    APoint := Data^.At(Cnt);
    NewX := APoint^.X;
    NewY := APoint^.Y;
    PointCnt := 1;
    SelRect.AssignByPoint(APoint^, Radius);
    HasEnd := Cnt = 0;
    repeat
      Inc(Cnt);
      BPoint := Data^.At(Cnt);
      IsInside := SelRect.PointInside(BPoint^);
      if IsInside then
      begin
        NewX := NewX + BPoint^.X;
        NewY := NewY + BPoint^.Y;
        Inc(PointCnt);
        HasEnd := HasEnd or (Cnt = Data^.Count - 1);
        Data^.Free(TDPoint(BPoint^));
        if SnapInfo <> nil then
          SnapInfo^.DeleteAndRenumber(Cnt);
      end;
    until (Cnt >= Data^.Count - 1) or (Data^.Count <= 4) or not IsInside;
    APoint^.Init(Trunc(NewX / PointCnt), Trunc(NewY / PointCnt));
    if HasEnd then
    begin
      PDPoint(Data^.At(0))^ := APoint^;
      PDPoint(Data^.At(Data^.Count - 1))^ := APoint^;
    end;
  end;
  CalculateClipRect;
end;

procedure TCPoly.Explode
  (
  PInfo: PPaint;
  ALayer: PLayer
  );
var
  APoly: PPoly;

  procedure DoExplode
      (
      StartIndex: Integer;
      StopIndex: Integer
      );
  var
    Cnt: Integer;
  begin
    for Cnt := StartIndex to StopIndex - 2 do
    begin
      APoly := New(PPoly, Init);
      APoly^.InsertPoint(PDPoint(Data^.At(Cnt))^);
      APoly^.InsertPoint(PDPoint(Data^.At(Cnt + 1))^);
      if APoly^.Data^.Count < 2 then
        Dispose(APoly, Done)
      else
      begin
        if PLayer(PInfo^.Objects)^.InsertObject(PInfo, APoly, FALSE) then
        begin
          ALayer^.InsertObject(PInfo, New(PIndex, Init(APoly^.Index)), FALSE);
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
          if (DDEHandler.DoAutoIns) then
          begin
            ProcDBSendObject(PInfo^.AProj, PIndex(APoly), 'EIN');
          end;
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
        end;
      end;
    end;
  end;
var
  Start: Integer;
  Island: Integer;
begin
  if GetState(sf_IslandArea) then
  begin
    DoExplode(0, IslandInfo^[0]);
    Start := IslandInfo^[0];
    for Island := 1 to IslandCount - 1 do
    begin
      DoExplode(Start, Start + IslandInfo^[Island]);
      Start := Start + IslandInfo^[Island] + 1;
    end;
  end
  else
    DoExplode(0, Data^.Count);
end;

type
  PDouble = ^Double;
  TDoubleColl = object(TSortedCollection)
    function Compare(Item1, Item2: Pointer): Integer; virtual;
    procedure FreeItem(Item: Pointer); virtual;
  end;

function TDoubleColl.Compare
  (
  Item1: Pointer;
  Item2: Pointer
  )
  : Integer;
begin
  if Abs(PDouble(Item1)^ - PDouble(Item2)^) < 1E-10 then
    Result := 0
  else
    if PDouble(Item1)^ > PDouble(Item2)^ then
      Result := 1
    else
      Result := -1;
end;

procedure TDoubleColl.FreeItem
  (
  Item: Pointer
  );
begin
  Dispose(PDouble(Item));
end;

procedure TCPoly.GetClippedPoints
  (
  PInfo: PPaint;
  var Points: TClipPointList
  );
var
  Count: Integer;
  Point: TGrPoint;
begin
  Points := TClipPointList.Create;
  Points.Capacity := Data^.Count - 1;
  for Count := 0 to Data^.Count - 2 do
  begin
    PInfo^.ConvertToDispDouble(PDPoint(Data^.At(Count))^, Point.X, Point.Y);
    Points.Add(Point);
  end;
end;

procedure TSCPoly.GetClippedPoints
  (
  PInfo: PPaint;
  var Points: TClipPointList
  );
var
  Count: Integer;
  Point: TGrPoint;
begin
  Points := TClipPointList.Create;
  Points.Capacity := Data^.Count - 1;
  for Count := 0 to Data^.Count - 2 do
  begin
    PInfo^.ScaleToDispDouble(PDPoint(Data^.At(Count))^, Point.X, Point.Y);
    Points.Add(Point);
  end;
end;

const
  RCPolyOld: TStreamRec = (
    ObjType: rn_CPolyOld;
    VmtLink: TypeOf(TCPoly);
    Load: @TCPoly.LoadOld;
    Store: @TCPoly.Store);

  RCPoly: TStreamRec = (
    ObjType: rn_CPoly;
    VmtLink: TypeOf(TCPoly);
    Load: @TCPoly.Load;
    Store: @TCPoly.Store);

  RSCPoly: TStreamRec = (
    ObjType: rn_SCPoly;
    VmtLink: TypeOf(TSCPoly);
    Load: @TSCPoly.Load;
    Store: @TSCPoly.Store);

  RSCPolyOld: TStreamRec = (
    ObjType: rn_SCPolyOld;
    VmtLink: TypeOf(TSCPoly);
    Load: @TSCPoly.LoadOld;
    Store: @TSCPoly.Store);

begin
  RegisterType(RCPolyOld);
  RegisterType(RCPoly);
  RegisterType(RSCPolyOld);
  RegisterType(RSCPoly);
end.

