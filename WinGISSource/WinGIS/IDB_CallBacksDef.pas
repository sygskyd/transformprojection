unit IDB_CallBacksDef;
{
Internal database. Ver. II.
This unit contains declarations of callbacks function of IDB that I use for
interaction between WinGIS and IDB and also some constants that have to be knowon
in WinGIS.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 09-02-2001
}
interface

uses Gauges, Forms, Classes,
   Objects, Lists;

type
   TUndoType = (utUnInsert,
      utUnDelete,
      utUnChange,
      utUnMove,
      utUnResize,
      utUnTransform,
      utUnknownUNDO);
   TRedoType = (rtInsert,
      rtDelete,
      rtChange,
      rtMove,
      rtResize,
      rtTransform,
      rtUnknownREDO);

// ++ Commented by Cadmensky IDB Version 2.2.9
{   TLayerNameItem = record
      iLayerIndex: integer;
      sLayerTableName: AnsiString;
   end;
   PLayerNameItem = ^TLayerNameItem;}
// -- Commented by Cadmensky IDB Version 2.2.9

   TShowProject = procedure (AProject: Pointer); stdcall;
   TShowSelectedItemsInProject = procedure (AProject, ALayer: Pointer; iItemID: Integer; bSelect, bNeedZoom: Boolean); stdcall;
   TShowItemInProjectWithZoom = procedure (AProject, ALayer: Pointer; iItemID: Integer); stdcall;
   TDeselectAllObjects = procedure (AProject: Pointer); stdcall;

   TGetProjectUndoStepsCount = function (AProject: Pointer): integer; stdcall;
   TGetProjectFileSize = function (AProject: Pointer): integer; stdcall;

   TItemExistsInProject = function (AProject: Pointer; ALayer: Pointer; iItemID: Integer): Boolean; stdcall;
   TItemToGuid = function (AProject: Pointer; ALayer: Pointer; iItemID: Integer): TGUID; stdcall;

   TSetProjectModified = procedure (AProject: Pointer); stdcall;

   TSetStartProjectGUID = procedure (AProject: Pointer; pcGUID: PChar); stdcall;
   TSetCurrentProjectGUID = procedure (AProject: Pointer; pcGUID: PChar); stdcall;
   TGetStartProjectGUID = function (AProject: Pointer): PChar; stdcall;
   TGetCurrentProjectGUID = function (AProject: Pointer): PChar; stdcall;

   TGetProjectIsNewAndNotSaved = function (AProject: Pointer): Boolean; stdcall;
   TGetProjectFullFileName = function (AProject: Pointer): PChar; stdcall;
   TGetLayerName = function (ALayer: Pointer): PChar; stdcall;
   TGetLayerIndex = function (ALayer: Pointer): Integer; stdcall;
   TLayerIsFixed = function (ALayer: Pointer): Boolean; stdcall;

   TChangeGeoTextParameters = procedure (AProject, ALayer: Pointer; pcGeoTextIDFieldName, pcGeoTextDataFieldName: PChar); stdcall;

   TGetAllProjectWindowsNumber = function: Integer; stdcall;
   TGetProjectLayerNumber = function (AProject: Pointer): Integer; stdcall;
   TGetLayerPointerByPosition = function (AProject: Pointer; iLayerPos: Integer): Pointer; stdcall;
   TGetLayerPointerByIndex = function (AProject: Pointer; iLayerIndex: Integer): Pointer; stdcall;
// ++ Cadmensky
   TGetActiveLayer = function (AProject: Pointer): Pointer; stdcall;
// -- Cadmensky

   TGetWinGISINIFileName = function: PChar; stdcall;
   TGetDefaultProjectFileName = function: PChar; stdcall;
// ++ Cadmensky
   TGetLayerItemsCount = function (AProject, ALayer: Integer; bSelected: Boolean = false): Integer; stdcall;
   TGetLayerItemIndexByNumber = function (AProject, ALayer: Integer; iItemNumber: Integer; bSelected: Boolean): Integer; stdcall;
   TIsObjectSelected = function (AProject: Pointer; iItemID: Integer): Boolean; stdcall;
// -- Cadmensky

   TGetNecessityInMakeDatabaseFileBackup = function (AProject: Pointer): Boolean; stdcall;

{   TGeneratePoint = function (AProject: Pointer; ALayer: Pointer; ilXCoord, ilYCoord: LongInt; iExistedProgisID: Integer; var iNewProgisID: Integer): Boolean; stdcall;
   TGenerateSymbol = function (AProject: Pointer; ALayer: Pointer; ilXCoord, ilYCoord: LongInt; iSymbolNumber: Integer; dblSymbolSize: Double; iExistedProgisID: Integer; var iNewProgisID: Integer): Boolean; stdcall;}

   TAnnotationSettingsDialogExecute = function (AProject, ACurrentLayer: Pointer; AnAnnotData: PCollection): Boolean; stdcall;
// ++ Cadmensky IDB Version 2.3.6
   TMakeAnnotationInProject = function (AProject, AnObjectLayer: Pointer; iItemID: Integer; sAnnotData: PChar): Boolean; stdcall;
//   TMakeAnnotationInProject = function (AProject, AnObjectLayer: Pointer; iItemID: Integer; AnAnnotData: PCollection): Boolean; stdcall;
// -- Cadmensky IDB Version 2.3.6
   TMakeAnnotationUpdate = procedure (ALayer: Pointer; iItemID: LongInt); stdcall;
    {brovak}
   TChartSettingsDialogExecute = function (AProject, ACurrentLayer: Pointer; AnChartData: PCollection): Boolean; stdcall;
   TMakeChartInProject = function (AProject, AnObjectLayer: Pointer; iItemID: Integer; AnChartData: PCollection): Boolean; stdcall;
   TMakeChartUpdate = procedure (ALayer: Pointer; iItemID: LongInt); stdcall;

   TLayerForInsert = function (Aproject, Alayer: pointer): Pointer; stdcall;

   TMakeSpecialChart = procedure (AProject, Alayer: Pointer); stdcall;
   TSendSpecialforChart = procedure (AProject: Pointer; PseudoDDEString: ansistring); stdcall;

   TShowNewThematicMapDialog = procedure (AProject: Pointer; sConnection, sSQLText, sProgisIDFieldName, sThematicFieldName: AnsiString); stdcall;
   TSendToTMSelectedID = procedure (AProject: Pointer; AProgisIDList: TIntList); stdcall;
   TShowPercentAtWinGIS = procedure (Flag, IDMessage, IDCurrent: integer); stdcall;
    {brovak}
   TShowDonePercent = procedure (iDonePercent: Integer); stdcall;

// ++ Commented by Cadmensky Version 2.2.8
//   TGatherGarbageInProject = function (AProject, ALayer: Pointer; AGauge: Integer; ADatabaseWindow: TForm; ARecordTemplate: TList): Integer; stdcall;
// -- Commented by Cadmensky Version 2.2.8

   TSetObjectStyle = function (AProject, ALayer: Pointer; iItemID, iLineColor, iLineStyle, iLineWidth, iPatternColor, iPatternStyle, iTransparency: Integer; iObjectSize: Integer): Boolean; stdcall;
   TRedrawProjectWindow = procedure (AProject: Pointer); stdcall;

   TRunThematicMapBuilder = function (AProject, ALayer: Pointer; pcAttTableName, pcDefaultDataSourceField, pcListOfFieldsThatAreNotToBeVisible: PChar; AGauge: TGauge): Boolean; stdcall;

   TRunMonitoring = procedure (AProject: Pointer; bNeedToShowMonitoringWindow: boolean); stdcall;

   TDataProps = ({dtLength, }dtArea, {dtPerimeter, }dtX, dtY, dtX1, dtY1, dtVerticesCount, dtSymbolName, dtSize, dtText, dtParent, dtRotation);
   TGetObjectProps = function (AProject, ALayer: Pointer; iItemID: Integer; ADataPropsType: TDataProps): PChar; stdcall;
   TReverseUsingOfIDBInfo = procedure; stdcall;

   TGetMDIChildWindowState = function: TWindowState; stdcall;
// ++ Cadmensky
   TSetNessesitySaveUndoDataSign = procedure (AProject: Pointer; bValue: boolean);
   TSaveUndoData = procedure (AProject: Pointer; AnUndoType: TUndoType);
// -- Cadmensky

implementation

end.

