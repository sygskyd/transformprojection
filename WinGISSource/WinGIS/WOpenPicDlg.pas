unit WOpenPicDlg;

interface

uses Messages, Windows, SysUtils, Classes, Controls, StdCtrls, Graphics,
  ExtCtrls, Buttons, Dialogs, Img_Info, TrLib32, TabFiles
{$ifndef VMSI}
  , MultiLng
{$endif}
  ;
{$define TAB_SUPPORT}

const
  PIC_LOW_SIZE: Integer = 1024 * 1024; // One megabyte picture is rather small
  CoordChkBoxName = 'LoadCoord';
  TransformChkBoxName = 'LoadTransform';
  Mess1 = '';
  Mess2 = '';
  Mess3 = '';
  Mess4 = '';
type
  POpenStatus = ^OpenStatus;
{ Result/Order statuses for image selection
  osNone - simply load it
  osCoordinates - load with user coordinates input (still not realized)
  osTransformation - load with Omega transformation
  osNotImage -  user not selected valid image
}
  OpenStatus = (osNone, osCoordinates, osTransformation, osNotImage);

  PDlgFlags = ^DlgCtrlFlags;
  DlgFlags = (dfTransformationOn, dfCoordinatesOn, dfEnableVectorFormat);
  DlgCtrlFlags = set of DlgFlags;

type
{ TWPOpenPictureDialog }
  TWOpenPictureDialog = class(TOpenDialog)
  private
{    FPicture: TPicture;}
    StatusAddr: POpenStatus;
    FPicturePanel: TPanel;
    FPictureLabel: TLabel;
{    FPreviewButton: TSpeedButton;}
    FPaintPanel: TPanel;
    FPaintBox: TPaintBox;
    FTimer: TTimer;
    LoadCoord: TCheckBox;
    LoadTransform: TCheckBox;
    Flags: DlgCtrlFlags; // Flags on which elements to generate on create

    FImgInfo: TrLibInfo;
    FullName: AnsiString; { Last true name of a file clicked }
    DirName: AnsiString; { Directory name with terminating '\'}
    fTabFilterIndex: Integer; { *.TAB file filter index }
    FOmegaOn: Boolean;
    PrevOmega, // Prev station of Omega loading
      PrevCoord: Boolean; // Prev. station of coodinates loading
    WasTabSelected: Boolean;
    procedure PaintBoxPaint(Sender: TObject);
    procedure PreviewClick(Sender: TObject);
  protected
    procedure DoClose; override;
    procedure DoSelectionChange; override;
    procedure DoShow; override;
    procedure OnTimerEvent(Sender: TObject);
    procedure LoadCBoxClick(Sender: TObject);
    procedure DoTypeChange; override;
  public
    RawName: AnsiString; { Last copy of a raw file name clicked incl. directory }
    constructor Create(AOwner: TComponent; PFlags: PDlgFlags; PStatus: POpenStatus); reintroduce;
    destructor Destroy; override;
    function Execute: Boolean; override;
    property GetOmegaOn: Boolean read FOmegaOn;
    property TabFilterIndex: Integer read fTabFilterIndex;
    procedure SetOmegaOn;
  end;

function ExtractFirstQuote(Str: AnsiString): AnsiString;
//function Str2FileNames(Str: AnsiString): AnsiString;

implementation

uses Consts, Forms, CommDlg, Dlgs, Math, FileCtrl, LicenseHndl, AM_Def;

{ TOpenPictureDialog }

{$R *.RES}

function MyStat(hImg: THandle; nPercent: Integer; dwUserInfo: DWORD): Integer; stdcall;
begin
  Application.ProcessMessages;
  Result := 1;
end;

constructor TWOpenPictureDialog.Create
  (
  AOwner: TComponent;
  PFlags: PDlgFlags;
  PStatus: POpenStatus
  );
begin
  inherited Create(AOwner);
  fTabFilterIndex := -2;
  WasTabSelected := False; { Not TAB files were selected }
  StatusAddr := PStatus;
  Flags := PFlags^;
  FPicturePanel := TPanel.Create(Self);

  with FPicturePanel do
  begin
    Name := 'PicturePanel';
    Caption := '';
    SetBounds(204, 5, 169, 200);
    BevelOuter := bvNone;
    BorderWidth := 6;
    TabOrder := 1;
    FPictureLabel := TLabel.Create(Self);
    ShowHint := True;
    with FPictureLabel do
    begin
      Name := 'PictureLabel';
{$ifdef VMSI}
      Hint := '(XY size)color[s] page#|(image Width x Height)color bits depth, page num';
{$else}
      Hint := MlgStringList['Main', 50];
{$endif}
      Caption := '';
      SetBounds(6, 6, 157, 23);
      Align := alTop;
      AutoSize := False;
      Parent := FPicturePanel;
    end;
    if dfTransformationOn in Flags then // Create the element
    begin
      LoadTransform := TCheckBox.Create(Self);
      with LoadTransform do
      begin
        Name := TransformChkBoxName;
{$ifdef VMSI}
        Caption := 'Load and &Transform';
{$else}
        Caption := MlgStringList['Main', 51];
{$endif}
        SetBounds(0, 0, 157, 17);
        Align := alBottom;
        Parent := FPicturePanel;
        if dfCoordinatesOn in Flags then { Set only if opposite control exists }
          OnClick := LoadCBoxClick;
{$ifdef VMSI}
        Hint := 'Transform picture before loading';
{$else}
        Hint := MlgStringList['Main', 52];
{$endif}
        LoadTransform.Enabled:=(WingisLicense >= WG_PROF);
      end;
    end;
  end;
  if dfCoordinatesOn in Flags then
  begin
    LoadCoord := TCheckBox.Create(Self);
    with LoadCoord do
    begin
      Name := CoordChkBoxName;
{$ifdef VMSI}
      Caption := 'Load with &Coordinates';
{$else}
      Caption := MlgStringList['Main', 53];
{$endif}
      SetBounds(0, 0, 157, 17);
      Align := alBottom;
      Parent := FPicturePanel;
{$ifdef VMSI}
      Hint := 'Asks corner points before loading';
{$else}
      Hint := MlgStringList['Main', 54];
{$endif}
      if dfTransformationOn in Flags then // Use user proc. only if needed
        OnClick := LoadCBoxClick;
{$ifdef VMSI}
      Hint := 'This option still is under development';
{$else}
      Hint := MlgStringList['Main', 55];
{$endif}
//++ Glukhov LoadImageInRect BUILD#123 20.07.00
//      Enabled := False;
//{$ifndef DEBUG}
//      Visible := False;
//{$endif}
      Visible := True;
      Enabled := True;
//-- Glukhov LoadImageInRect BUILD#123 20.07.00
    end;
  end;
  FPaintPanel := TPanel.Create(Self);
  with FPaintPanel do
  begin
    Name := 'PaintPanel';
    Caption := '';
    SetBounds(6, 29, 157, 145);
    Align := alClient;
    BevelInner := bvRaised;
    BevelOuter := bvLowered;
    TabOrder := 0;
    FPaintBox := TPaintBox.Create(Self);
    Parent := FPicturePanel;
    with FPaintBox do
    begin
      Name := 'PaintBox';
      SetBounds(0, 0, 153, 141);
      Align := alClient;
{$ifdef VMSI}
      Hint := 'Preview area';
{$else}
      Hint := MlgStringList['Main', 56];
{$endif}
      OnDblClick := PreviewClick;
      OnClick := PreviewClick;
      OnPaint := PaintBoxPaint;
      Parent := FPaintPanel;
      Tag := 0; // Mark analyzing before preview = DirectDraw
    end;
  end;
  FTimer := TTimer.Create(Self);
  with FTimer do
  begin
    Name := 'PictureTimer';
    Enabled := False;
    Interval := 500;
    OnTimer := OnTimerEvent;
  end;
end;

destructor TWOpenPictureDialog.Destroy;
begin
  FTimer.Free;
  FPaintBox.Free;
  FPaintPanel.Free;
  LoadTransform.Free;
  LoadCoord.Free;
  FPictureLabel.Free;
  FPicturePanel.Free;
  if FImgInfo <> nil then
    FImgInfo.Free;
  inherited Destroy;
end;

procedure TWOpenPictureDialog.DoSelectionChange;
var
  ValidPicture{, WildCards}: Boolean;
  Status: WORD;
  PicSize: Integer;
  SingleName: AnsiString;
  Info: AnsiString;
  TabFile: TTABFile;
  IsMultiSel: Boolean;

  function ValidFile(const FileName: AnsiString): Boolean;
  var
    Res: DWORD;
  begin
    Result := False;
    Res := GetFileAttributes(PChar(FIleName));
    if Res = $FFFFFFFF then Exit; // Some error
    if (Res and FILE_ATTRIBUTE_DIRECTORY) <> 0 then Exit; // Directory not file
    Result := True;
  end;

begin
  FTimer.Enabled := False;
  IsMultiSel:=False;
  if FullName <> FileName then
  begin
    if FImgInfo <> nil then
    begin { Clear previous one }
      FImgInfo.Free;
      FImgInfo := nil;
    end;
    RawName := FileName;
    FullName := FileName;
    SingleName := RawName;
    if DirectoryExists(FileName) then
      DirName := FileName { Directory clicked }
    else
      DirName := ExtractFilePath(FileName); {Directory part of file name }
    if ofAllowMultiSelect in Options then
      if Pos('"', RawName) <> 0 then { Multiple files enabled ?}
      begin
        SingleName := ExtractFirstQuote(RawName);
        FullName := DirName + SingleName;
      end;
    ValidPicture := FileExists(FullName) and ValidFile(FullName);
    if ValidPicture then { Test it to be picture }
    try

      IsMultiSel:=(Pos('"',Filename) > 0);

      if IsVMSIImage(FullName) or (IsMultiSel) then begin
         LoadTransform.Checked:=False;
         LoadTransform.Enabled:=False;
      end
      else begin
         LoadTransform.Enabled:=(WingisLicense >= WG_PROF);
      end;

      if FilterIndex = TabFilterIndex then { Special case: we look only TAB files }
      begin
        TabFile := TTABFile.Create(FullName);
        try
          if TabFile.IsValid then { Extract image file name from TAB file and try to review it }
          begin
            if TabFile.ImageCount = 1 then // only one name was found
            begin
              FImgInfo := TRLibInfo.Create(ExtractFilePath(TabFile.FileName) + TabFile.ImageName, Status);
              if FImgInfo <> nil then { Image from TAB exists }
                if dfCoordinatesOn in Flags then { Mark it for coordinated positioning  }
                  LoadCoord.Checked := True;
            end
          end;
        finally
          TabFile.Free;
        end;
      end
      else
        FImgInfo := TRLibInfo.Create(FullName, Status);
      if Status <> 0 then
        ValidPicture := False
      else
      begin
        if FImgInfo.IsVector and (not (dfEnableVectorFormat in Flags)) then // Vector files are not allowed to load // Vector is enabled
          ValidPicture := False
        else
          if
            FImgInfo.BitCount > 24 then
            ValidPicture := False
          else
          begin
{$ifdef VMSI}
            Info := Format(' (%dx%d)%d bit[s]', [FImgInfo.Width, FImgInfo.Height, FImgInfo.BitCount]);
{$else}
            Info := Format(MlgStringList['Main', 57], [FImgInfo.Width, FImgInfo.Height, FImgInfo.BitCount]);
{$endif}
            if FImgInfo.PageCount > 1 then { Display page number }
            { but only if it isn't VMSI - any info about is a TOP SECRET! }
              if not FImgInfo.IsVMSI then
{$ifdef VMSI}
                Info := Info + ' ' + IntToStr(FImgInfo.PageCount) + ' pages';
{$else}
                Info := Info + ' ' + IntToStr(FImgInfo.PageCount) + MlgStringList['Main', 58];
{$endif}
            FPictureLabel.Caption := Info;
            PicSize := FImgInfo.LineLength * FImgInfo.Height; // Define picture size
            if (PicSize <= PIC_LOW_SIZE) then
              FTimer.Interval := 1
            else
              FTimer.Interval := 500;
          end;
      end;
    except
      ValidPicture := False;
    end;
    if not ValidPicture then
    begin
      FPictureLabel.Caption := '';
      FPaintBox.Invalidate;
    end
    else
      FTimer.Enabled := True;
  end;
  inherited DoSelectionChange;
end;

procedure TWOpenPictureDialog.DoClose;
begin
     inherited DoClose;
     { Hide any hint windows left behind }

     if LoadTransform <> nil then begin
        FOmegaOn := LoadTransform.Checked;
     end;

     Application.HideHint;
     StatusAddr^ := osNone;

     {if ofAllowMultiSelect in Options then begin
        if (Files.Count <= 0) and (Filename = '') then begin
           StatusAddr^ := osNotImage;
           Exit;
        end;
     end
     else if FImgInfo = nil then begin
        StatusAddr^ := osNotImage;
        Exit;
     end;
     }
     
     if not FImgInfo.FormatIsSupported then begin
        StatusAddr^ := osNotImage; { Bad image }
     end
     else begin
        if FImgInfo.IsVector and (not (dfEnableVectorFormat in Flags)) then begin
           StatusAddr^ := osNotImage;
        end
        else begin
           if (LoadTransform <> nil) and (LoadTransform.Checked) then begin
              StatusAddr^ := osTransformation; { Use Omega}
              Exit;
           end
           else if LoadCoord <> nil then begin
              if LoadCoord.Checked then
                 StatusAddr^ := osCoordinates; { Ask user coordinates }
              end;
           end;
        end;
end;

procedure TWOpenPictureDialog.DoShow;
var
  PreviewRect: TRect;
begin
  { Set preview area to entire dialog }
  GetClientRect(Handle, PreviewRect);
  { Move preview area to right of static area }
  PreviewRect.Left := GetStaticRect.Right;
  Inc(PreviewRect.Top, 4);
  FPicturePanel.BoundsRect := PreviewRect;
  FPicturePanel.ParentWindow := Handle;
  inherited DoShow;
end;

function TWOpenPictureDialog.Execute;
begin
  fTabFilterIndex := GetFilterIndex(Filter, '*.TAB');
  if fTabFilterIndex = -1 then { Ensure to never find it }
    fTabFilterIndex := -2;
  if NewStyleControls and not (ofOldStyleDialog in Options) then
    Template := 'JHCTEMPLATE'
  else
    Template := nil;
  Result := inherited Execute;
end;

procedure TWOpenPictureDialog.PaintBoxPaint(Sender: TObject);
var
  DrawRect: TRect;
  SNone, SNone1: string;
  TxtHeight, Middle: Integer;
  Mem, Step: DWORD;
begin
  FTimer.Enabled := False;
  SNone1 := '';
{$ifdef VMSI}
  SNone := '(Format not Suported)';
{$else}
  SNone := MlgStringList['Main', 59];
{$endif}
  with TPaintBox(Sender) do
  begin
    Canvas.Brush.Color := Color;
    DrawRect := ClientRect;
    if FImgInfo <> nil then
    begin
      if FImgInfo.FormatIsSupported then
        {
          Not use 32 bits images ONLY as
          ImageMan not support currently 25 bits and higher images
        }
        if FImgInfo.BitCount <= 24 then
          if ((FImgInfo.IsVector) and (dfEnableVectorFormat in Flags))
            or
            (not FImgInfo.IsVector) then
          begin
            Mem := GetAvailPhysMem;
            if (Tag = 0) and // if Tag = 0 then analyze resources before show
              (not FImgInfo.IsVMSI) and // VMSI can be loaded very very fast!
              (Mem < (FImgInfo.ImageSize * 2)) and // memory requirement is rather small
              (not FImgInfo.IsVector) and // is raster, not vector
              (not (FImgInfo.IsScaleSelf)) // can be previewd very fast
              then
            begin { Too large }
{$ifdef VMSI}
              SNone := 'Too large image';
              SNone1 := '[Double] click HERE to show';
{$else}
              SNone := MlgStringList['Main', 60];
              SNone1 := MlgStringList['Main', 61];
{$endif}
            end
            else
              if FImgInfo.Width > 0 then
              begin
                with DrawRect do
                  if (FImgInfo.Width > Right - Left) or (FImgInfo.Height > Bottom - Top) then
                  begin
                    if FImgInfo.Width > FImgInfo.Height then
                      Bottom := Top + MulDiv(FImgInfo.Height, Right - Left, FImgInfo.Width)
                    else
                      Right := Left + MulDiv(FImgInfo.Width, Bottom - Top, FImgInfo.Height);
                  end
                  else
                  begin
                    with DrawRect do
                    begin
                      Bottom := FImgInfo.Height - 1;
                      Right := FImgInfo.Width - 1;
                    end;
                  end;
                Screen.Cursor := crHourGlass;
                try
                  Step := FImgInfo.ImageVolume div 100;
                  if Step = 0 then
                    Step := FImgInfo.ImageSize - 1;
                  if Step = 0 then
                    Inc(Step);
                  TrLib.ImageSetDefaultStatusProc(MyStat, Step, 0);
                  TrLib.DrawFullImage(PChar(FImgInfo.FileName), Canvas.Handle, @DrawRect);
                  TrLib.ImageSetDefaultStatusProc(nil, 1024 * 1024, 0);
                finally
                  Screen.Cursor := crDefault;
                end;
                Tag := 0;
                Exit;
              end
              else
{$ifdef VMSI}
                SNone := '(Empty?!)'; ;
{$else}
                SNone := MlgStringList['Main', 62]; ;
{$endif}
          end;
    end
    else
      SNone := srNone;
    with DrawRect, Canvas do
    begin
      TxtHeight := TextHeight(SNone);
      Middle := Top + (Bottom - Top) div 2;
      if SNone1 <> '' then
      begin
        TextOut(Left + (Right - Left - TextWidth(SNone)) div 2, Middle - TxtHeight - 2, SNone);
        TextOut(Left + (Right - Left - TextWidth(SNone1)) div 2, Middle + 2, SNone1);
      end
      else
        TextOut(Left + (Right - Left - TextWidth(SNone)) div 2, Middle - TxtHeight div 2, SNone);
    end;
  end;
  Tag := 0; { Clear flag to prevent drawing of a large image without checking resources}
end;

procedure TWOpenPictureDialog.PreviewClick(Sender: TObject);
begin
  with FPaintBox do
  begin
    FTimer.Enabled := False;
    Tag := 1; // DirectDraw - not analyze resources before loading
    Invalidate;
  end;
end;

procedure TWOpenPictureDialog.OnTimerEvent(Sender: TObject);
begin
  (Sender as TTimer).Enabled := False;
  FPaintBox.Invalidate;
end;

procedure TWOpenPictureDialog.DoTypeChange;
begin
  inherited DoTypeChange;
  DefaultExt := ExtractFilterExt(Filter, FilterIndex);
  if FilterIndex = TabFilterIndex then { User selects *.TAB}
  begin
    WasTabSelected := True; { Mark tab selection }
//    Options := Options + [ofAllowMultiSelect]; { Useless! }
    if dfTransformationOn in Flags then
      with LoadTransform do
      begin
        PrevOmega := Checked;
        Enabled := False;
        Checked := False;
      end;
    if dfCoordinatesOn in Flags then
      with LoadCoord do
      begin
        PrevCoord := Checked;
//        Enabled := False;
        Checked := True;
      end;
  end
  else { User selects non TABbed file! }
  begin
    if WasTabSelected then
    begin
//      Options := Options - [ofAllowMultiSelect]; { Useless! }
      if dfCoordinatesOn in Flags then
        with LoadCoord do
        begin
          Checked := PrevCoord;
//          Enabled := True;
        end;
      if dfTransformationOn in Flags then
        with LoadTransform do
        begin
          Checked := PrevOmega;
          Enabled := (WingisLicense >= WG_PROF);
        end;
    end;
    WasTabSelected := False;
  end;
end;

procedure TWOpenPictureDialog.LoadCBoxClick(Sender: TObject);
var
  Opposite: TCheckBox;
begin
  with Sender as TCheckBox do
  begin
    if Enabled and Visible then
    begin
      if Name = CoordChkBoxName then
        Opposite := LoadTransform
      else
        Opposite := LoadCoord;
      if Checked = True then
        if Opposite <> nil then
          if Opposite.State = cbChecked then
            Opposite.State := cbUnChecked;
    end;
  end;
end;

function ExtractFirstQuote(Str: AnsiString): AnsiString;
var
  Pos1, Pos2: Integer;
begin
  Result := '';
  Pos1 := Pos('"', Str);
  if Pos1 <> 0 then
  begin
    Result := Result + Copy(Str, Pos1 + 1, MaxInt);
    Pos2 := Pos('"', Result);
    if Pos2 = 0 then
    begin
      Result := '';
      Exit;
    end;
    Result := Copy(Result, 1, Pos2 - 1);
  end;
end;

procedure TWOpenPictureDialog.SetOmegaOn;
begin
  if LoadTransform <> nil then
  begin
    LoadTransform.Checked := True;
    FOmegaOn := True;
  end;
end;

end.

