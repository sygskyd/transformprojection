
{****************************************************************************}
{**                                                                        **}
{**                           Hardlock E-Y-E                               **}
{**                             API-Calls                                  **}
{**                                                                        **}
{**          Interface Unit for High Level Hardlock API DLL                **}
{**                                                                        **}
{**                    (c) 1993 by ///FAST Electronic                      **}
{**                                                                        **}
{**                    Computer: IBM-PC or compatible                      **}
{**                    OS      : Windows 3.1                               **)
{**                    Language: Borland Pascal 7.0                        **}
{**                    Authors : Axel Koehler                              **)
{**                    Version : 3.18                                      **}
{**                    Date    : 15-Dec-1993                               **}
{**                                                                        **}
{****************************************************************************}
{$A+,B-,D+,F+,G-,I-,L+,N-,R-,S-,V-,W+,X+}

UNIT HiAPI_TP;

INTERFACE

{****************************************************************************}
{$I ApiDefs }
{****************************************************************************}

const DLLName = 'WINHLOC3.DLL';

FUNCTION DongleAccess ( VAR ApiStruc ) : Word; stdcall;
FUNCTION HL_LOGIN     ( ModAd, AccessMode : Word; var RefKey, VerKey ) : Word; stdcall;
FUNCTION HL_LOGOUT    : Word; stdcall;
FUNCTION HL_PORTINF   : SMALLINT; stdcall;
FUNCTION HL_USERINF   : SMALLINT; stdcall;
FUNCTION HL_MAXUSER   : SMALLINT; stdcall;
FUNCTION HL_ACCINF    : SMALLINT; stdcall;
FUNCTION HL_VERSION   : SMALLINT; stdcall;
FUNCTION HL_HLSVERS   : SMALLINT; stdcall;
FUNCTION HL_AVAIL     : Word; stdcall;
FUNCTION HL_CODE      ( VAR BUFFER; Cnt : Word ) : Word; stdcall;
FUNCTION HL_READ      ( Register : Word; VAR Value : Word) : Word; stdcall;
FUNCTION HL_READBL    ( VAR Eeprom ) : Word; stdcall;
FUNCTION HL_WRITEBL   ( VAR Eeprom ) : Word; stdcall;
FUNCTION HL_ABORT     : Word; stdcall;
FUNCTION HL_WRITE     ( Register, Value : Word) : SMALLINT; stdcall;
FUNCTION HL_MEMINF    : SMALLINT; stdcall;
FUNCTION HL_CALC      (i1,i2,i3,i4:SMALLINT):SMALLINT; stdcall;

IMPLEMENTATION

Function  DongleAccess; external DLLName;
FUNCTION  HL_LOGIN;     external DLLName;
FUNCTION  HL_LOGOUT;    external DLLName;
FUNCTION  HL_PORTINF;   external DLLName;
FUNCTION  HL_USERINF;   external DLLName;
FUNCTION  HL_MAXUSER;   external DLLName;
FUNCTION  HL_ACCINF;    external DLLName;
FUNCTION  HL_VERSION;   external DLLName;
FUNCTION  HL_HLSVERS;   external DLLName;
FUNCTION  HL_AVAIL;     external DLLName;
FUNCTION  HL_CODE;      external DLLName;
FUNCTION  HL_READ;      external DLLName;
FUNCTION  HL_READBL;    external DLLName;
FUNCTION  HL_WRITEBL;   external DLLName;
FUNCTION  HL_WRITE;     external DLLName;
FUNCTION  HL_ABORT;     external DLLName;
FUNCTION  HL_MEMINF;    external DLLName;
FUNCTION  HL_CALC;      external DLLName;

End.
