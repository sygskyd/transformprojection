{******************************************************************************+
  Unit PropHndl
--------------------------------------------------------------------------------
  Dialog to show and edit the properties of the selected objects.
--------------------------------------------------------------------------------
  Autor: Martin Forst
--------------------------------------------------------------------------------
+******************************************************************************}
unit PropHndl;

interface
{$IFNDEF AXDLL} // <----------------- AXDLL
uses ProjHndl,
{$IFNDEF WMLT}
   SymbolFillDlg,
{$ENDIF}
   AttachHndl, // Glukhov ObjAttachment 26.10.00
   Dialogs,
   FillDlg, LineDlg, Tabnotbk, PropDlg, {++brovak} measures, offsetdlg, textchangedlg {--brovak}, FontStyleDlg
{$IFNDEF WMLT}
{$ENDIF}
   ;

type
   TEditObjectStyleHandler = class (TProjMenuHandler)
   private
      {+++ brovak - for Freddie's  request}
      FillDialog: TPatternDialog;
      LineDialog: TLineStyleDialog;
{$IFNDEF WMLT}
      SymbolFillDialog: TSymbolFillDialog;
{$ENDIF}
      PropertyDialog: TPropertyDialog;
      FontstyleDialog: TFontstyleDialog;
      ItemType: word;
       {--- brovak}
{$IFNDEF WMLT}
//      LinkStyleDialog: TAttPropertyDialog; // Glukhov ObjAttachment 26.10.00
      OffSetDialog: TOffsetDialog; //brovak
      TextchangeDialog: TTextChangeDialog;
{$ENDIF}
   public
      procedure OnStart; override;

       {+++ Brovak  - for Freddie's  request
       Changes in all appropriate elements of the dialogue property to a Layer (layerColor)}
      procedure SetAsLayerSettingsForSelectedItemsProp (Sender: TObject);
       {-- brovak }

   end;

procedure ProcSetObjectStyleFromDB (AProj: Pointer);
{$ENDIF} // <----------------- AXDLL
implementation
{$IFNDEF AXDLL} // <----------------- AXDLL
uses AM_Def, AM_Font, AM_Index, AM_Layer, AM_Sym, AM_Text, Windows, LicenseHndl
{$IFNDEF WMLT}
   , BitmapPropertiesDlg, SymbolStyleDlg
{$ENDIF}
   , BMPImage,
   Controls, { brovak FillDlg,FontStyleDlg,brovak LineDlg,} MultiDlg, PropCollect, { PropDlg,} DDEDef,
     { brovak SymbolFillDlg,} UserIntf, NumTools, AM_Child, AM_Proj
{$IFNDEF WMLT}
   , AM_DDE
{$ENDIF}
   , uheap, uclass, uview, uptree, ulbase, ulayref, usellst, uglob, utext, usymbol, XLines, XFills, StyleDef
   , AM_Poly, AM_CPoly // Glukhov Bug#84 BUILD#125 01.11.00
{++ IDB_UNDO}
   , Am_Main, IDB_CallBacksDef
{-- IDB_UNDO}
   , AM_View, AM_ProjO // Glukhov ObjAttachment 26.10.00
   , TAbFiles, SysUtils // Sygsky: for TAB file service
{$IFNDEF WMLT}
   , PointsCoordDlg // F4 Dialog
{$ENDIF}
   , BusGraphics, AM_BuGra, MUModule
   ;

{brovak for Freddie's  request
 This procedure set all propertyes of Fill,Line,SymbollFill of Selected Objects as LayerProperty}

procedure TEditObjectStyleHandler.SetAsLayerSettingsForSelectedItemsProp (Sender: TObject);
{$IFNDEF WMLT}
var
   i: integer;
   OldCurrentPage: string;
{$ENDIF}
begin;
{$IFNDEF WMLT}
   OldCurrentPage := PropertyDialog.TabbedNoteBook.ActivePage;

  //Change in FillDialog
   if Self.FillDialog <> nil then
   begin;
      PropertyDialog.TabbedNoteBook.ActivePage := FillDialog.Caption;
        // FillProperties.
      with FillDialog do
      begin;
          {-1 because before item soLayer item is soNone}
         ForeColorBtn.SelectedColor := - (1 + soLayer);
         BackColorBtn.SelectedColor := - (1 + soLayer);
         PatternList.ItemIndex := - (1 + soLayer);
         ScaleEdit.Text := MlgStringList['SelectionNames', 91];
        //  FillDialog.Update;
      end;
   end;
   //Change in LineDialog
   if Self.LineDialog <> nil then
   begin;
      PropertyDialog.TabbedNoteBook.ActivePage := LineDialog.Caption;
      with LineDialog do
      begin;
         ColorComboBtn.SelectedColor := - (1 + soLayer);
         FillColorComboBtn.SelectedColor := - (1 + soLayer);
         LineStylesList.ItemIndex := -soLayer;
         WidthEdit.Text := MlgStringList['SelectionNames', 91];
         ScaleEdit.Text := WidthEdit.Text;
        //     LineDialog.Update;
      end;
   end;
    //Change in SymbolFillDialog
   if Self.SymbolFillDialog <> nil then
   begin;
      PropertyDialog.TabbedNoteBook.ActivePage := SymbolFillDialog.Caption;
      with SymbolFillDialog do
      begin;
         DistanceEdit.Text := MlgStringList['SelectionNames', 91];
         LineDistEdit.Text := DistanceEdit.Text;
         OffsetEdit.Text := DistanceEdit.Text;
         AngleEdit.Text := DistanceEdit.Text;
         WidthEdit.Text := MlgStringList['SelectionNames', 131];
         SymAngleEdit.Text := WidthEdit.Text;

         TypeCombo.ItemIndex := TypeCombo.Items.IndexOf (MlgStringList['SelectionNames', 111]);
       //   SymbolFillDialog.Update;
      end;
   end;

       //Brovak Change in FontStyleDialog
   if Self.FontStyleDialog <> nil then
   begin;
      PropertyDialog.TabbedNoteBook.ActivePage := FontStyleDialog.Caption;
        // FillProperties.
      with FontstyleDialog do
      begin;
         FontStyleDialog.FForeColor := $000000;
         FontStyleDialog.FBackColor := $FFFFFF;
         FontStyleDialog.PreviewWindow.Invalidate;
         ForeColorBtn.SelectedColor := - (1 + soLayer);
         BackColorBtn.SelectedColor := - (1 + soLayer);

      end;
   end;

   PropertyDialog.TabbedNoteBook.ActivePage := OldCurrentPage;
   Project^.PInfo^.RedrawScreen (False);
{$ENDIF}
end;

procedure TEditObjectStyleHandler.OnStart;
{$IFNDEF WMLT}
var
   Cnt: LongInt;
   Item: PIndex;
   LayerItem: PIndex;
//    PropertyDialog         : TPropertyDialog;  { brovak. declaration moved to private section}
//    FillDialog             : TPatternDialog;   { brovak. declaration moved to private section}
//    FontStyleDialog        : TFOntStyleDialog; { brovak}
//    LineDialog             : TLineStyleDialog; { brovak. declaration moved to private section}
//    SymbolFillDialog       : TSymbolFillDialog;{ brovak. declaration moved to private section}
{$IFNDEF WMLT}
   PointsCoordDlg: TPointsCoordDialog; // Brovak
   BitmapPropertiesDialog: TBitmapPropertiesDialog;
   SymbolStyleDialog: TSymbolStyleDialog;
{$ENDIF}
   Layer: PLayer;
   AnyBitmap: Boolean;
   AnyLineStyle: Boolean;
   AnyFillStyle: Boolean;
   AnySymbolFill: Boolean;
   AnyText: Boolean;
   AnySymbol: Boolean;
   AnyCoord: Boolean; //brovak
   ALinkStyle: Boolean; // Glukhov ObjAttachment 26.10.00
   VItem: PView; // Glukhov ObjAttachment 26.10.00
   Bitmap: PImage;
   AFontDes: PFontDes;
   AText: PText;
   NewFont: TFontData;
   RedrawRect: TDRect;
   NeedRedraw: Boolean;
   ObjectChanged: Boolean;
   ObjectStyleCreated: Boolean;
   ASymbol: PSymbol;
   AttributesEditable: Boolean;
   OldClipRect: TDRect;
    //HSD
   i: longint;
   j: integer;
   m: integer; //brovak
   mm: integer;
   AItem: TSelItem;
   ALeaf: TPBucketLeaf;
   AvertexItem: TSLBaseITem;
   Help: TSLBaseITem;
   AView: CView;
   Vlist: TLayRefLst;
   AViewItem: TExtPtr;
   ObjStyle: PObjStyleV2;
   BText: CText;
   BSymbol: CSymbol;
   AFont: TFontData;
{++ IDB_UNDO UnChange}
   bUndoDataMustBeStored: Boolean;
{-- IDB_UNDO UnChange}
//++Sygsky: for TAB service
   TABFile: TTabFile;
   TABName: AnsiString;
   TmpPosPoint: TDPoint;
   bTmp: Boolean;
//--Sygsky
{$ENDIF}
begin
{$IFNDEF WMLT}
   if Project^.Layers^.SelLayer^.Data^.GetCount = 1 then
   begin
      Item := Project^.Layers^.SelLayer^.Data^.At (0);
      Item := Project^.Layers^.IndexObject (Project^.PInfo, Item);
   end
   else
   begin
      Item := nil;
   end;
   if (Item <> nil) and (Item^.GetObjType = ot_BusGraph) then
   begin
      BusGraphics.ProcChangeBusGraph (Project, Item, PBusGraph (Item)^.ChartKindPtr, True);
   end
   else
   begin
      UserInterface.BeginWaitCursor;
      try
    // determine if top-layer is editable
         AttributesEditable := not TMDIChild (Project^.Parent).IsReadOnly
            and (Project^.Layers^.TopLayer <> nil)
            and not Project^.Layers^.TopLayer.GetState (sf_Fixed);
    { create dialogs and initialize the properties }
     //bro
    {+++ points coord dialog }
         PointsCoordDlg := TPointsCoordDialog.Create (Self);

         with PointsCoordDlg do
         begin
            SimplyMode := false;
            SwitchBtn.Visible := false;
            Panel1.Visible := false;
         end;
    {--bro}

    { fill-dialog }
         FillDialog := TPatternDialog.Create (Self);
         with FillDialog do
         begin
            InitializeFillStyle (FillStyle);
            FillStyle.UpdatesEnabled := AttributesEditable;
            ProjectStyles := Self.Project^.PInfo^.ProjStyles;
         end;
    { symbolfill-dialog }
         SymbolFillDialog := TSymbolFillDialog.Create (Self);
         with SymbolFillDialog do
         begin
            InitializeSymbolFillStyle (SymbolFill);
            SymbolFill.UpdatesEnabled := AttributesEditable;
            Project := Self.Project;
            ProjectStyles := Self.Project^.PInfo^.ProjStyles;
         end;
    { linestyle-dialog }
         LineDialog := TLineStyleDialog.Create (Self);
         with LineDialog do
         begin
            InitializeLineStyle (LineStyle);
            LineStyle.UpdatesEnabled := AttributesEditable;
            Project := Self.Project;
            ProjectStyles := Self.Project^.PInfo^.ProjStyles;
//++ Glukhov Bug#84 BUILD#125 23.11.00
            if not Project^.PInfo^.IsMU then begin
               ChkChangeDirection.Visible := True;
            end;
//-- Glukhov Bug#84 BUILD#125 23.11.00
         end;
    { bitmap dialog }
         BitmapPropertiesDialog := TBitmapPropertiesDialog.Create (Self);
         with BitmapPropertiesDialog do
         begin
            with BitmapStyle do
            begin
               PropertyMode := pmCollect;
               UpdatesEnabled := AttributesEditable;
            end;
         end;
    // symbol-properties dialog
         SymbolStyleDialog := TSymbolStyleDialog.Create (Self);
         with SymbolStyleDialog do
         begin
            Project := Self.Project;
            with SymbolStyle do
            begin
               PropertyMode := pmCollect;
               SizeOptions.Options := [soObject];
               AngleOptions.Options := [soObject];
               UpdatesEnabled := AttributesEditable;
            end;
         end;
    // font-style dialog
         FontStyleDialog := TFontStyleDialog.Create (Self);
         with FontStyleDialog do
         begin
            Fonts := Project^.PInfo^.Fonts;
            with TextStyle do
            begin
               PropertyMode := pmCollect;
               UpdatesEnabled := AttributesEditable;
            end;
      {+++brovak BUG83 BUILD127}
            FontStyleDialog.AngleEdit.Visible := true;
            FontStyleDialog.AngleSpin.Visible := true;
            FontStyleDialog.AngleLabel.Visible := true;
            FontStyleDialog.AngleVal.Edit := FontStyleDialog.AngleEdit;
            FontStyleDialog.SetColorSupport1;
      {---brovak}
         end;

//++ Glukhov ObjAttachment 26.10.00
//         LinkStyleDialog := TAttPropertyDialog.Create (Self);
//         LinkStyleDialog.Project := Self.Project;
//-- Glukhov ObjAttachment 26.10.00

    { collect information about selected objects }
         AnyLineStyle := FALSE;
         AnyFillStyle := FALSE;
         AnySymbolFill := FALSE;
         AnyBitmap := FALSE;
         AnyText := FALSE;
         AnySymbol := FALSE;
         ALinkStyle := False; // Glukhov ObjAttachment 26.10.00
         AnyCoord := false;
         TABFile := nil; // Sygsky

         with Project^, Layers^ do
         begin
//++ Glukhov ObjAttachment 26.10.00
            if SelLayer^.Data^.GetCount = 1 then
            begin
               Item := SelLayer^.Data^.At (0);
               Item := IndexObject (PInfo, Item);
               {
               if Assigned (PView (Item).AttData) then
               begin
                  if PView(Item)^.GetObjMaster(Project^.PInfo) <> nil then begin
                     LinkStyleDialog.CurObject := PView (Item);
                     ALinkStyle := True;
                  end;
               end;
               }
      {++brovak}
               ItemType := Item.GetObjType;
               if (ItemType in [Ot_poly, Ot_cpoly, ot_circle, ot_Pixel, ot_Arc, ot_Symbol, ot_Text]) then
                  AnyCoord := true;
               if ItemType in [Ot_poly, Ot_cpoly] then
                  if PPoly (Item).Data.Count > 5000 then
                     if MsgBox (Self.Parent.Handle, 2500, MB_YESNO + MB_ICONQUESTION + MB_DEFBUTTON1, '') = IDNO then
                        AnyCoord := false;
      {--brovak}

            end;

//-- Glukhov ObjAttachment 26.10.00
            for Cnt := 0 to SelLayer^.Data^.GetCount - 1 do
            begin
               Item := SelLayer^.Data^.At (Cnt);
               Layer := TopLayer;
               LayerItem := Layer^.HasObject (Item);
               if LayerItem <> nil then
                  with LayerItem^ do
                  begin
                     if HasLineStyle then
                     begin
                        AnyLineStyle := TRUE;
                        if ObjectStyle = nil then
                           CollectLineStyle (nil, LineDialog.LineStyle)
                        else
                           CollectLineStyle (@ObjectStyle^.LineStyle, LineDialog.LineStyle);
                     end;
                     if HasPattern then
                     begin
                        AnyFillStyle := TRUE;
                        if ObjectStyle = nil then
                           CollectFillStyle (nil, FillDialog.FillStyle)
                        else
                           CollectFillStyle (@ObjectStyle^.FillStyle, FillDialog.FillStyle);
                     end;
                     if HasSymbolFill then
                     begin
                        AnySymbolFill := TRUE;
                        if ObjectStyle = nil then
                           CollectSymbolFill (nil, SymbolFillDialog.SymbolFill)
                        else
                           CollectSymbolFill (@ObjectStyle.SymbolFill, SymbolFillDialog.SymbolFill);
                     end;
                     if LayerItem^.GetObjType = ot_Image then
                     begin
                        Bitmap := PImage (Layer^.IndexObject (Project^.PInfo, LayerItem));
                        if Bitmap <> nil then
                           with BitmapPropertiesDialog, BitmapPropertiesDialog.BitmapStyle do
                           begin
                              AnyBitmap := TRUE;
//++Sygsky: for TAB service
                              Inc (Amount);
                              TABName := ChangeFileExt (Bitmap.FileName^, TABExtension);
                              if TABFile = nil then
                                 TABFIle := TTabFile.Create (TABName)
                              else
                                 TABFile.LoadTAB (TabName);
                              if TABFile.IsValid and TabFile.NameCoincides (Bitmap.FileName^) then
                              begin
                                 Inc (WithTAB);
                                 BitMap.Options := BitMap.Options or opt_HasTAbFile;
                              end
                              else
                                 BitMap.Options := BitMap.Options and (not opt_HasTAbFile);
                              ABitmap := Bitmap; // Just in case
//--Sygsky
                              ShowBitmap := BooleanStyle (Bitmap^.ShowOpt <> 0);
                              ShowFrame := BooleanStyle (Bitmap^.Options and 1 = 0);
                              Transparency := Bitmap^.Transparency;
                              TransparencyType := Bitmap^.TransparencyType;
                           end;
                     end
                     else
                        if LayerItem^.GetObjType = ot_Text then
                        begin
                           AText := PText (Layer^.IndexObject (Project^.PInfo, LayerItem));
                           AFontDes := Project^.PInfo^.Fonts^.GetFont (AText^.Font.Font);
             {+++Brovak BUG#32 BUILD 127}
                           if ObjectStyle <> nil then
                              CollectFontStyle (@AText^.Font, AFontDes, FontStyleDialog.TextStyle,
                                 -AText^.Angle * Pi / 180.0, ObjectStyle.LineStyle.Color, ObjectStyle.FillStyle.BackColor)
                           else
                           begin;
                              CollectFontStyle (@AText^.Font, AFontDes, FontStyleDialog.TextStyle,
                                 -AText^.Angle * Pi / 180.0, clFromLayer, clFromLayer);
                           end;

                           FontStyleDialog.SetAlign(AText^.FAlign);

                           AnyText := TRUE;
                        end
                        else
                           if LayerItem^.GetObjType = ot_Symbol then
                              with SymbolStyleDialog do
                              begin
                                 ASymbol := PSymbol (Layer^.IndexObject (Project^.PInfo, LayerItem));
                                 SymbolStyle.Symbol := ASymbol^.SymIndex;
                                 SymbolStyle.Size := ASymbol^.Size;
                                 SymbolStyle.SizeType := ASymbol^.SizeType;
                                 SymbolStyle.Angle := ASymbol^.Angle;
                                 AnySymbol := TRUE;
                              end;
                  end;
            end;
         end;
    //SelectHsd;

         if Project^.Document.Loaded then
            for i := 0 to TSelList (GetHeapMan.SelList).Count - 1 do
            begin
               AItem := TSelList (GetHeapMan.SelList).Items[i];
               ALeaf := TPBucketLeaf (AItem.Mptr.Get);

    {traverseleaf}

               Vlist := TPBucketLeaf (ALeaf).VertexList;
               Vlist.lockpage (Vlist);
               if Vlist.count > 0 then
                  for j := Vlist.count - 1 downto 0 do
                  begin
                     AViewItem := Vlist.Items (j);
                     AView := AViewItem.Get;
                     AView.lockpage (AView);
                     if AView <> nil then
                     begin
                        if TGlobal (GetheapMan.GlobalObj).SelLst.issel (AView.index2) then
                        begin

                           if Aview.HasLineStyle then
                           begin
                              AnyLineStyle := TRUE;
                              ObjSTyle := AView.GetObjectStyle;
                              if not AView.ModStyle then
                                 CollectLineStyle (nil, LineDialog.LineStyle)
                              else
                                 CollectLineStyle (@ObjStyle^.LineStyle, LineDialog.LineStyle);
                           end;

                           if AView.HasPattern then
                           begin
                              AnyFillStyle := TRUE;
                              ObjSTyle := AView.GetObjectStyle;
                              if not AView.ModStyle then
                                 CollectFillStyle (nil, FillDialog.FillStyle)
                              else
                                 CollectFillStyle (@ObjStyle^.FillStyle, FillDialog.FillStyle);
                           end;
                           if Aview.HasSymbolFill then
                           begin
                              AnySymbolFill := TRUE;
                              ObjSTyle := AView.GetObjectStyle;
                              if not Aview.ModStyle then
                                 CollectSymbolFill (nil, SymbolFillDialog.SymbolFill)
                              else
                                 CollectSymbolFill (@ObjStyle.SymbolFill, SymbolFillDialog.SymbolFill);
                           end;

                           if AView.GetObjType = ot_Text then
                           begin
                  {
            BText:=CText(Aview);
            Afont.Style:=BText.Font.Style;
            Afont.Font:=BText.Font.Font;
            Afont.Height:=BText.Font.Height;
            AFontDes:=Project^.PInfo^.Fonts^.GetFont(BText.Font.Font);
            CollectFontStyle(@Afont,AFontDes,FontStyleDialog.TextStyle,
                -AText^.Angle*Pi/180.0);
            AnyText:=TRUE;
              }
                           end
                           else
                              if AView.GetObjType = ot_Symbol then
                                 with SymbolStyleDialog do
                                 begin

                                    BSymbol := CSymbol (AView);
                                    BSymbol.SetSymptr (Project^.Pinfo^.Symbols);
                                    SymbolStyle.Symbol := BSymbol.SymPtr^.Index;
                                    SymbolStyle.Size := BSymbol.Size;
                                    SymbolStyle.SizeType := BSymbol.SizeType;
                                    SymbolStyle.Angle := BSymbol.Angle;
                                    AnySymbol := TRUE;

                                 end;
                        end;
                     end;
                     AView.unlockpage (Aview);
                  end;
               Vlist.unlockpage (Vlist);
            end;

      finally
         UserInterface.EndWaitCursor;
      end;
  // create property-dialog
      PropertyDialog := TPropertyDialog.Create (Parent);
      try
         with PropertyDialog do
         begin
      // setup dialog
            Project := Self.Project;
      // initialize redraw-rectangle, will be set empty
            RedrawRect.Init;

      // add needed dialogs to the properties-dialog
         try

            if Project^.PInfo^.IsMU then begin
               FillDialog.PopupMenu.AutoPopup:=False;
            end;

            if Project^.PInfo^.IsMU then begin
               LineDialog.PopupMenu.AutoPopup:=False;
            end;

            if AnyLineStyle then AddDialog (LineDialog, LineDialog.ControlsPanel, LineDialog.Caption);
            if AnyFillStyle then AddDialog (FillDialog, FillDialog.ControlsPanel, FillDialog.Caption);

            if not Project^.PInfo^.IsMU then begin
               if AnySymbolFill then AddDialog (SymbolFillDialog, SymbolFillDialog.ControlsPanel, SymbolFillDialog.Caption);
            end;

            if not Project^.PInfo^.IsMU then begin
               if AnyBitmap then AddDialog (BitmapPropertiesDialog, BitmapPropertiesDialog.ControlPanel, BitmapPropertiesDialog.Caption);
            end;

            if AnyText then begin
               FontStyleDialog.cbAlign.Visible:=True;
               AddDialog (FontStyleDialog, FontStyleDialog.ControlPanel, FontStyleDialog.Caption);
            end;

            if AnySymbol then begin
               if Project^.PInfo^.IsMU then begin
                  SymbolStyleDialog.SymbolSelectBtn.Enabled:=False;
               end;
               AddDialog (SymbolStyleDialog, SymbolStyleDialog.ControlPanel, SymbolStyleDialog.Caption);
            end;

         finally

            if AnyCoord and (ItemType = ot_Text) then
            begin;
               OffSetDialog := TOffSetDialog.Create (Self);
               OffSetDialog.CurrentProject := Project;
               OffSetDialog.CurrentText := Atext;
               OffSetDialog.MasterItem := Atext.GetObjMaster (Project^.Pinfo);
               if Atext.IsItAnnot = true then
               begin;
                  AddDialog (OffSetDialog, OffSetDialog.ControlPanel, MlgStringList['OffSetDialog', 1]);
                  OffSetDialog.IndexMinimum := OffSetDialog.ConvertFromTextOffsetOptions (Atext.AttData.AliPosOfObject);
                  OffSetDialog.PropertiesInit;
               end;
               TextChangeDialog := TTextChangeDialog.Create (Self);
               AddDialog (TextChangeDialog, TextChangeDialog.ControlPanel, MlgStringList['TextChangeDialog', 1]);
               TextChangeDialog.CurrentText := PtoStr (AText.text);
               TextChangeDialog.LoadText;
            end;
         end;
      //++ Glukhov ObjAttachment 26.10.00
            {
            if ALinkStyle then
            begin;
               if AnyCoord and (ItemType = ot_Text) and (Atext.IsItAnnot = true) then
               begin;
                  LinkStyleDialog.HideAllFlag := true;
                  TmpPosPoint.Init (Atext.Pos.X, Atext.Pos.Y);
               end;
               AddDialog (
                  LinkStyleDialog, LinkStyleDialog.ControlPanel, LinkStyleDialog.Caption);

            end;
            }
//-- Glukhov ObjAttachment 26.10.00
      // switch to common-page

            if (AnyCoord) and (not Project^.PInfo^.IsMU) then
            begin
               PointsCoordDlg.FileButton.Visible := False;
               PointsCoordDlg.Project := @Project^;
               PointsCoordDlg.IsFormShow := false;

               AddDialog (PointsCoordDlg, PointsCoordDlg.ControlsPanel, MlgStringList['PointsCoordDialog', 45]);

               with Project^ do
               begin;
                  OldSnapItem := GiveMeSourceObjectFromSelect (Layers^.SelLayer.Data.At (0));
                  if ItemType in [ot_Cpoly, ot_poly] then
                     PointsCoordDlg.PrepareGrid (@Project^, Mn_Snap, -1);
                  if ItemType = ot_Circle then
                     PointsCoordDlg.PrepareGrid (@Project^, mn_Edit_Circle, -1);
                  if ItemType = ot_Pixel then
                     PointsCoordDlg.PrepareGrid (@Project^, mn_Edit_Point, -1);
                  if ItemType = ot_Arc then
                     PointsCoordDlg.PrepareGrid (@Project^, mn_Edit_Arc, -1);
                  if ItemType = ot_Symbol then
                     PointsCoordDlg.PrepareGrid (@Project^, mn_Edit_Symbol, -1);
                  if ItemType = ot_Text then
                  begin;
                     PointsCoordDlg.PrepareGrid (@Project^, mn_Edit_Text, -1);
       {brovak}
                  end;
               end;
            end;

            TabbedNotebook.PageIndex := 0;

      {+++ brovak}
            PropertyDialog.SetDefaultBtn.OnClick := SetAsLayerSettingsForSelectedItemsProp;
            PropertyDialog.SetDefaultBtn.Enabled := false;
            if (AnyLineStyle = true) or (AnyFillStyle = true) or (AnySymbolFill = true) or (AnyText = true) then PropertyDialog.SetDefaultBtn.Enabled := true;
       {--}

            if (ShowModal = mrOK) and AttributesEditable then
               with Project^, Layers^ do
               begin
                  UserInterface.BeginWaitCursor;
{++ IDB_UNDO UnChange}
                  bUndoDataMustBeStored := FALSE;
{-- IDB_UNDO UnChange}
                  try
          // change object-styles for selected objects
                     for Cnt := 0 to SelLayer^.Data^.GetCount - 1 do
                     begin
                        Item := SelLayer^.Data^.At (Cnt);
{+++ Brovak   (Thanks Ivanoff for idea and main code of correction this.)
Now work with set of selected objects were on diffirient layers ("Active Layer") will go correctly}

                        LayerItem := Project^.Layers^.TopLayer^.HasObject (Item);
                        if LayerItem = nil then
                        begin
                           for m := 1 to SELF.Project.Layers.LData.Count - 1 do
                               begin
                              Layer := Project.Layers.LData.At (m);
                              LayerItem := Layer^.HasObject (Item);
                              if LayerItem <> nil then BREAK;
                           end;
                        end;
                        if LayerItem = nil then CONTINUE;
{ Brovak ---}
{++ IDB_UNDO UnChange}
            // Put the item in Undo data in the IDB. Ivanoff.
{$IFNDEF WMLT}
                        if (WinGisMainForm.WeAreUsingUndoFunction[Project])
                           and
                           (WinGisMainForm.UndoRedo_Man.AddObjectInUndoData (SELF.Project, Layer, LayerItem)) then
                           bUndoDataMustBeStored := TRUE;
{$ENDIF}
{-- IDB_UNDO UnChange}
                        OldClipRect.InitByRect (LayerItem^.ClipRect);
                        ObjectChanged := FALSE;
                        if LayerItem <> nil then
                           with LayerItem^ do
                           begin
                              if ObjectStyle = nil then
                              begin
                                 SetState (sf_OtherStyle, TRUE);
                                 ObjectStyle := New (PObjStyleV2);
                                 ObjectStyle.LineStyle := DefaultLineStyle;
                                 ObjectStyle.FillStyle := DefaultFillStyle;
                                 ObjectStyle.SymbolFill := DefaultSymbolFill;
                                 ObjectStyleCreated := TRUE;
                              end
                              else
                              begin
                                 ObjectStyleCreated := FALSE;
                                 with ObjectStyle^ do
                                    UpdateUseCounts (Project^.PInfo,
                                       LineStyle, FillStyle, SymbolFill, FALSE);
                              end;
                              if AnyCoord then
                              begin;
                                 PointsCoordDlg.SetNewCoordForEdit;
                                 ObjectChanged := true;
                              end;
               {+++Brovak BUG#32 BUILD 127}
                              if (FontStyleDialog <> nil) and (FontStyleDialog.AngleEdit.Visible = true) then
                              begin;
                                 if DblCompare (FontStyleDialog.ForeColorBtn.SelectedColor, clNoChange) <> 0 then ObjectStyle^.LineStyle.Color := FontStyleDialog.ForeColorBtn.SelectedColor;
                                 if DblCompare (FontStyleDialog.BackColorBtn.SelectedColor, clNoChange) <> 0 then ObjectStyle^.FillStyle.BackColor := FontStyleDialog.BackColorBtn.SelectedColor;
                                 ObjectChanged := true;
                              end;
               {brovak}
//++ Glukhov Bug#84 BUILD#125 01.11.00
                              if AnyLineStyle then
                              begin
                                 if LineDialog.ChkChangeDirection.Checked then
                                 begin
                                    case LayerItem.GetObjType of
                                       ot_Poly:
                                          begin
                                             PPoly (Layer.IndexObject (Project.PInfo, LayerItem)).ReversePoly;
                                             ObjectChanged := True;
                                          end;
                                       ot_CPoly:
                                          begin
                                             PCPoly (Layer.IndexObject (Project.PInfo, LayerItem)).ReversePoly;
                                             ObjectChanged := True;
                                          end;
                                    end;
                                 end;
                              end;
//-- Glukhov Bug#84 BUILD#125 01.11.00
                              try
                                 if HasLineStyle and UpdateLineStyle (@ObjectStyle^.LineStyle,
                                    LineDialog.LineStyle) then ObjectChanged := TRUE;
                                 if HasPattern and UpdateFillStyle (@ObjectStyle^.FillStyle,
                                    FillDialog.FillStyle) then ObjectChanged := TRUE;
                                 if HasSymbolFill and UpdateSymbolFillStyle (@ObjectStyle^.SymbolFill,
                                    SymbolFillDialog.SymbolFill) then ObjectChanged := TRUE;
                              except
                                 continue;
                              end;
              // remove object-style if everything is set to the layer-style

                              if HasAllLayerStyle and {+++Brovak BUG#32 BUILD 127} (Item^.GetObjType <> ot_Text) then
                              begin
                // remove object-style from the object
                                 SetState (sf_OtherStyle, FALSE);
                                 Dispose (ObjectStyle);
                                 ObjectStyle := nil;
                // if object-style was created then the object-style was not changed
                                 if ObjectStyleCreated then ObjectChanged := FALSE;
                              end
                              else
                                 with ObjectStyle^ do
                                    UpdateUseCounts (Project^.PInfo,
                                       LineStyle, FillStyle, SymbolFill, TRUE);
                              if Item^.GetObjType = ot_Image then
                              begin
                // update bitmap-settings  !?!?!? set ObjectChanged if any change
                                 Bitmap := PImage (Layer^.IndexObject (Project^.PInfo, LayerItem));
                                 if Bitmap <> nil then
                                    with BitmapPropertiesDialog, BitmapPropertiesDialog.BitmapStyle do
                                    begin
                                       if ShowBitmap = bsYes then
                                          NumTools.Update (Bitmap^.ShowOpt, so_ShowBmp, ObjectChanged)
                                       else
                                          if ShowBitmap = bsNo then NumTools.Update (Bitmap^.ShowOpt, 0, ObjectChanged);
                                       if ShowFrame = bsYes then
                                          NumTools.Update (Bitmap^.Options, Bitmap^.Options and not (1), ObjectChanged)
                                       else
                                          if ShowFrame = bsNo then NumTools.Update (Bitmap^.Options, Bitmap^.Options or (1), ObjectChanged);
                                       if Transparency >= 0 then NumTools.Update (Bitmap^.Transparency, Transparency, ObjectChanged);
                                       if TransparencyType >= 0 then NumTools.Update (Bitmap^.TransparencyType, TransparencyType, ObjectChanged);
//++Sygsky: TAB service
                                       with BitMap^ do
                                       begin
                                          case TabAction of
                                             taNone: ;
                                             taFresh:
                                                if ((Options and opt_HasTABFile) <> 0) then // Add it to disk
                                                   UpdateTAB;
                                             taUpdateAdd:
                                                UpdateTAB;
                                             taAdd:
                                                if ((Options and opt_HasTABFile) = 0) then // Add it to disk
                                                   UpdateTAB;
                                          end;
//                        Options := Options and (not opt_HasTABFile); // Wipe the temp flag
                                       end;
//--Sygsky
                                    end;
                              end
                              else
                                 if LayerItem^.GetObjType = ot_Text then
                                    with FontStyleDialog.TextStyle do
                                    begin
                // update text-settings
                                       AText := PText (Layer^.IndexObject (Project^.PInfo, LayerItem));
                                       NewFont.Init;
                                       NewFont := AText^.Font;
                                       if FontName[1] > sNone[1] then NewFont.Font := PInfo^.Fonts^.IDFromName (FontName);
                                       if DblCompare (Height, dblNone) > 0 then NewFont.Height := LimitToLong (Height * 100);
                                       if Bold = bsYes then
                                          NewFont.Style := NewFont.Style or ts_Bold
                                       else
                                          if Bold = bsNo then NewFont.Style := NewFont.Style and not ts_Bold;
                                       if Italic = bsYes then
                                          NewFont.Style := NewFont.Style or ts_Italic
                                       else
                                          if Italic = bsNo then NewFont.Style := NewFont.Style and not ts_Italic;
                                       if Underlined = bsYes then
                                          NewFont.Style := NewFont.Style or ts_Underl
                                       else
                                          if Underlined = bsNo then NewFont.Style := NewFont.Style and not ts_Underl;
                                       if Transparent = bsYes then
                                          NewFont.Style := NewFont.Style or ts_Transparent
                                       else
                                          if Transparent = bsNo then NewFont.Style := NewFont.Style and not ts_Transparent;
                                       if FixedHeight = bsYes then
                                          NewFont.Style := NewFont.Style or ts_FixHeight
                                       else
                                          if FixedHeight = bsNo then NewFont.Style := NewFont.Style and not ts_FixHeight;
                                       case Alignment of
                                          taRight: NewFont.Style := (NewFont.Style or ts_Right) and not (ts_Center);
                                          taCentered: NewFont.Style := (NewFont.Style or ts_Center) and not (ts_Right);
                                          taLeft: NewFont.Style := NewFont.Style and not (ts_Right or ts_Center);
                                       end;
              {+++brovak BUG 83 BUILD 127}
                                       if FontStyleDialog.AngleEdit.Visible = true then
                                       begin;
                                          Rotation := GetDoubleField (1, FontStyleDialog.AngleVal, muRad, AngleOptions); //*180.0/Pi;
                                          if DblCompare (Rotation, dblNoChange) <> 0 then
                                          begin;
                                             ObjectChanged := true;
                                             if DblCompare (abs (Rotation), (2 * PI)) = 0 then Rotation := 0;
                                             Atext^.Angle := -Round (Rotation * 180.0 / Pi);
                                          end;
                                       end;
                {---brovak}
                                       if FontStyleDialog.cbAlign.Checked then begin
                                          AText.SetAlign(Byte(FontStyleDialog.TextPosPanel.TextAlign));
                                       end;

                                       if AnyCoord and (ItemType = ot_Text) then
                                          Atext.SetText (TextChangeDialog.CurrentText);

               (* tempopary closed
                 if (AnyCoord=true) and (ItemType = ot_Text) and (Atext.IsItAnnot=true)
                 then
                  if not (TmpPosPoint.IsDiff(AText.Pos))
                   then
                    begin;
                    OffSetDialog.CurrentText.Text:=AText.Text;
                    OffSetDialog.CurrentText.Font:=AText.Font;
                    OffSetDialog.ChangePositionObjprop;
                    Atext.Pos:=OffSetDialog.CurrentText.Pos;
                    Atext.IsUseOffset:= OffSetDialog.CurrentText.IsUseOffset;
                    Atext.AttData.AliPosOfObject:=OffSetDialog.CurrentText.AttData.AliPosOfObject;
                    Atext.ObjectOffSetType:=OffSetDialog.CurrentText.ObjectOffSetType;
                    Atext.Angle:=OffSetDialog.CurrentText.Angle;
                    ObjectChanged:=true;
                    end;
                  *)

                // determine if any property has changed
                                       if ObjectChanged or not NewFont.IsEqual (AText^.Font) then
                                       begin
                                          RedrawRect.CorrectByRect (AText^.ClipRect);
//++ Glukhov Bug#468 Build#162 01.07.01
//                  AText^.SetNewText( PInfo, PToStr(AText^.Text), NewFont, True, False );
                                          AText^.SetNewText (PInfo, PToStr (AText^.Text), NewFont);
//-- Glukhov Bug#468 Build#162 01.07.01
                                          UpdateClipRect (AText);
                                          ObjectChanged := TRUE;
                                       end;
                                    end
                                 else
                                    if LayerItem^.GetObjType = ot_Symbol then
                                       with SymbolStyleDialog.SymbolStyle do
                                       begin
                                          ASymbol := PSymbol (Layer^.IndexObject (Project^.PInfo, LayerItem));
                // change the symbol-size and -rotation
                                          if DblCompare (Size, dblNoChange) <> 0 then
                                          begin
                                             NumTools.Update (ASymbol^.Size, Size, ObjectChanged);
                                             NumTools.Update (ASymbol^.SizeType, SizeType, ObjectChanged);
                                          end;
{++ Ivanoff BUG#51 BUILD95
If an angle of rotation equals 360 degrees, the angle should be equal zero.
Because sin360 not equals zero, it equals 2e-16.}
                                          if DblCompare (abs (Angle), (2 * PI)) = 0 then Angle := 0;
{-- Ivanoff}
                                          if DblCompare (Angle, dblNoChange) <> 0 then NumTools.Update (ASymbol^.Angle, Angle, ObjectChanged);
{++ Ivanoff BUG#331 BUILD#95
After change the symbol size and an angle of rotation of the symbol in the dialog, the symbol ClipRect was not calculated.}
                                          if ObjectChanged then
                                          begin
                                             ASymbol.CalculateClipRect (Project.PInfo);
                                             UpdateClipRect (ASymbol);
                                          end;
{-- Ivanoff}
                // change the symbol
                                          if (Symbol > intFromLayer) and (Symbol <> ASymbol^.SymIndex) then
                                          begin
                                             ASymbol^.SetSymPtr (PInfo^.Symbols);
                                             if ASymbol^.SymPtr <> nil then Dec (ASymbol^.SymPtr^.UseCount);
                                             ASymbol^.SymIndex := Symbol;
                                             ASymbol^.SymPtr := nil;
                                             ASymbol^.SetSymPtr (PInfo^.Symbols);
                                             if ASymbol^.SymPtr <> nil then Inc (ASymbol^.SymPtr^.UseCount);
                                             ASymbol^.CalculateClipRect (PInfo);
{++ Ivanoff BUG#331 BUILD#95}
                                             UpdateClipRect (ASymbol);
{-- Ivanoff}
                                             ObjectChanged := TRUE;
                                          end;
                                       end;
//++ Glukhov ObjAttachment 27.10.00
                              {
                              if ALinkStyle then
                              begin
                                    LinkStyleDialog.CurObject.MoveRel (
                                       LinkStyleDialog.HorMove, LinkStyleDialog.VerMove);
                                    UpdateClipRect (LinkStyleDialog.CurObject);
                                    ObjectChanged := True;
                              end;
                              }
              // add current object to the redraw-area if it's style has changed
                              if ObjectChanged then
                              begin
                                 MU.LogUpdObjectStyle(Project.PInfo,Layer,LayerItem);
// We have to correct all the attached objects for the changed object
// IDB_Warning!!! See AttachHndl
                                 VItem := Layer.IndexObject (Project.PInfo, LayerItem);
                                 if Assigned (VItem) then begin
                                    UpdateWithAllAttached (Project, VItem, OldClipRect, RedrawRect);
                                    if VItem^.GetObjType in [ot_Text,ot_Symbol] then begin
                                       MU.LogUpdObjectData(Project^.PInfo,Layer,VItem);
                                    end;
                                 end;
//-- Glukhov ObjAttachment 27.10.00
                              end;
                           end;
                     end;
{++ IDB_UNDO UnChange}
          // And now we have to store all Undo data. If it is necessary. Ivanoff.
{$IFNDEF WMLT}
                     if (WinGisMainForm.WeAreUsingUndoFunction[Project]) and (bUndoDataMustBeStored) then
                        WinGisMainForm.UndoRedo_Man.SaveUndoData (SELF.Project, utUnChange);
{$ENDIF}
{-- IDB_UNDO UnChange}
          // redraw graphic if any object-style has changed
                     {
                     if not RedrawRect.IsEmpty then
                     begin
                        PInfo^.FromGraphic := TRUE;
                        PInfo^.RedrawRect (RedrawRect);
                     end;
                     }
                     PInfo^.RedrawScreen(False);
                  finally
                     UserInterface.EndWaitCursor;
                  end;
               end;
         end;
      finally
         PropertyDialog.Free;
      end;
   end;
{$ENDIF}
end;

{**************************************** ProcSetObjectStyleFromDB ****************************************}

procedure ProcSetObjectStyleFromDB
   (
   AProj: Pointer
   );
{$IFNDEF WMLT}
var
   SItem: PIndex;
   Cnt: LongInt;
   ObjectLayer: string;
   RedrawRect: TDRect;
      {IndexColl    : PViewColl;}
{++ IDB_UNDO UnChange}
   bUndoDataMustBeStored: Boolean;
{-- IDB_UNDO UnChange}

   procedure DoAll
         (
         Item: PChar
         ); far;
   const
      WindowsLStyles = 5;
      WindowsHStyles = 8;
   var
      ExistObject: PIndex;
      i: Integer;
      OldTop: PLayer;
      SearchOnAllL: Boolean;
      ResStr: string;
      ResLInt: LongInt;
      ObjLayer: PLayer;
      SFound: Boolean;
      MStrs: Boolean;
      StrToLong: Boolean;
      MoreStrs: Boolean;
      StrOK: Boolean;
      OldClipRect: TDRect;
      ObjectChanged: Boolean;
      ObjectStyleCreated: Boolean;
      ALineStyle: TLineProperties;
      AFillStyle: TFillProperties;
      ALineStyles: TXLineStyleList;
      AFillStyles: TXFillStyleList;
      LItem: TXLineStyle;
      HItem: TCustomXFill;
      StyleFound: Boolean;
      DDEStrSect: Integer;
      ASymbol: PSymbol;
      ResReal: Real;
        {ListObject   : PIndex;}
        {ID           : LongInt;}
        {Data         : TStyleData;}
   begin
      //LayerInfo
      DDEStrSect := 1;
      OldTop := PProj (AProj)^.Layers^.TopLayer;
      if DDEHandler.FGotLInfo = 0 then
      begin
         SearchOnAllL := TRUE;
      end
      else
         if DDEHandler.FGotLInfo = 1 then
         begin
            StrOK := ReadDDECommandString (Item, DDEStrSect, 0, WgBlockStart, WgBlockEnd, WgBlockEnd, ResStr, StrToLong, MoreStrs);
            if StrOK and (ResStr <> '') then
            begin
               SearchOnAllL := FALSE;
               ObjLayer := PProj (AProj)^.Layers^.NameToLayer (ResStr);
               if ObjLayer <> nil then
                  ObjectLayer := ObjLayer^.Text^
               else
                  SearchOnAllL := TRUE;
            end
            else
               SearchOnAllL := TRUE;
            Inc (DDEStrSect);
         end
         else
            if DDEHandler.FGotLInfo = 2 then
            begin
               if ReadDDECommandLongInt (Item, DDEStrSect, 0, WgBlockStart, WgBlockEnd, WgBlockEnd, ResLInt, SFound, MStrs) then
               begin
                  SearchOnAllL := FALSE;
                  ObjLayer := PProj (AProj)^.Layers^.IndexToLayer (ResLInt);
                  if ObjLayer <> nil then
                     ObjectLayer := ObjLayer^.Text^
                  else
                     SearchOnAllL := TRUE;
               end
               else
                  SearchOnAllL := TRUE;
               Inc (DDEStrSect);
            end;
      // ID
      if ReadDDECommandLongInt (Item, DDEStrSect, 0, WgBlockStart, WgBlockEnd, WgBlockEnd, ResLInt, SFound, MStrs) then
      begin
         SItem^.Index := ResLInt;
         if not SearchOnAllL then PProj (AProj)^.Layers^.TopLayer := ObjLayer;
         if (PProj (AProj)^.Layers^.TopLayer <> nil)
            and (not PProj (AProj)^.Layers^.TopLayer^.GetState (sf_LayerOff)) then
            ExistObject := PProj (AProj)^.Layers^.TopLayer^.HasObject (SItem)
         else
            ExistObject := nil;
         if (ExistObject = nil) and SearchOnAllL then
         begin
            i := 0;
            while (ExistObject = nil) and (i < PProj (AProj)^.Layers^.LData^.Count) do
            begin
               PProj (AProj)^.Layers^.TopLayer := PProj (AProj)^.Layers^.LData^.At (i);
               if (not PProj (AProj)^.Layers^.TopLayer^.GetState (sf_LayerOff))
                  and (not PProj (AProj)^.Layers^.TopLayer^.GetState (sf_Fixed)) then
                  ExistObject := PProj (AProj)^.Layers^.TopLayer^.HasObject (SItem);
               i := i + 1;
            end;
         end;
        (*
        {ist die ID im amp-file nicht vorhanden so kann sie hoechstwahrscheinlich im hsd-file vorkommen}
        {dazu wird zur ID in einer Pindex-Struktur der Objektstil gemerkt und in einer PViewcoll gespeichert}
        {chg Franz 98-11-09}
        if ExistObject=NIL then begin
          ExistObject:=New(PIndex,Init(ID));
          ExistObject^.ObjectStyle:=New(PObjStyle);
          INdexColl^.Insert(ExistObject);
          FillChar(ExistObject^.ObjectStyle^,SizeOf(TObjStyle),#0);  { Layerdaten und alte Objektdaten auslesen }
          with ExistObject^.ObjectStyle^ do begin
            Color:=coFromLayer;
            LineType:=ltFromLayer;
            LineWidth:=lwFromLayer;
            Pattern:=ptFromLayer;
            PatternColor:=coFromLayer;
            Transparent:=trFromLayer;
          end;
        end;
        {end Franz}
        *)
      end
      else
         ExistObject := nil;
      if ExistObject <> nil then
         with ExistObject^ do
         begin
            Inc (DDEStrSect);
            OldClipRect.InitByRect (ExistObject^.ClipRect);
            ObjectChanged := FALSE;
            if ObjectStyle = nil then
            begin
               SetState (sf_OtherStyle, TRUE);
               ObjectStyle := New (PObjStyleV2);
               ObjectStyle.LineStyle := DefaultLineStyle;
               ObjectStyle.FillStyle := DefaultFillStyle;
               ObjectStyle.SymbolFill := DefaultSymbolFill;
               ObjectStyleCreated := TRUE;
            end
            else
            begin
               ObjectStyleCreated := FALSE;
               with ObjectStyle^ do
                  UpdateUseCounts (PProj (AProj)^.PInfo, LineStyle, FillStyle, SymbolFill, FALSE);
            end;
        //LineProperties
            if HasLineStyle then
            begin
               ALineStyle := TLineProperties.Create;
          // properties not supported by SOS
               if ObjectStyleCreated then
               begin
                  ALineStyle.FillColor := PProj (AProj)^.Layers^.TopLayer.LineStyle.FillColor;
                  ALineStyle.Scale := PProj (AProj)^.Layers^.TopLayer.LineStyle.Scale;
                  ALineStyle.WidthType := PProj (AProj)^.Layers^.TopLayer.LineStyle.WidthType;
               end
               else
               begin
                  ALineStyle.FillColor := ObjectStyle.LineStyle.FillColor;
                  ALineStyle.Scale := ObjectStyle.LineStyle.Scale;
                  ALineStyle.WidthType := ObjectStyle.LineStyle.WidthType;
               end;
          // LinieColor
               if ReadDDECommandLongInt (Item, DDEStrSect, 0, WgBlockStart,
                  DDEHandler.FDDESepSign, WgBlockEnd, ResLInt, SFound, MStrs) then
               begin
                  if (ResLInt < -2) or (ResLInt > $FFFFFF) then ResLInt := -1;
               end
               else
                  ResLInt := -1;
               if ResLInt = -2 then
                  ALineStyle.Color := PProj (AProj)^.Layers^.TopLayer.LineStyle.Color
               else
                  if ResLInt = -1 then
                  begin
                     if ObjectStyleCreated then
                        ALineStyle.Color := PProj (AProj)^.Layers^.TopLayer.LineStyle.Color
                     else
                        ALineStyle.Color := ObjectStyle.LineStyle.Color;
                  end
                  else
                     if ResLInt >= 0 then ALineStyle.Color := ResLInt;
          // LineType
               if ReadDDECommandLongInt (Item, DDEStrSect, 1, WgBlockStart,
                  DDEHandler.FDDESepSign, WgBlockEnd, ResLInt, SFound, MStrs) then
               begin
                  if (ResLInt < -2) then ResLInt := -1;
               end
               else
                  ResLInt := -1;
               if ResLInt = -2 then
                  ALineStyle.Style := PProj (AProj)^.Layers^.TopLayer.LineStyle.Style
               else
                  if ResLInt = -1 then
                  begin
                     if ObjectStyleCreated then
                        ALineStyle.Style := PProj (AProj)^.Layers^.TopLayer.LineStyle.Style
                     else
                        ALineStyle.Style := ObjectStyle.LineStyle.Style;
                  end
                  else
                     if ResLInt >= 0 then
                     begin
                        if ResLInt < WindowsLStyles then
                           ALineStyle.Style := ResLInt
                        else
                        begin
                           if ObjectStyleCreated then
                              ALineStyle.Style := PProj (AProj)^.Layers^.TopLayer.LineStyle.Style
                           else
                              ALineStyle.Style := ObjectStyle.LineStyle.Style;
                           StyleFound := FALSE;
                           Cnt := 0;
                           i := 0;
                           ALineStyles := PProj (AProj)^.PInfo^.ProjStyles.XLineStyles;
                           ResLInt := ResLInt - WindowsLStyles + 1000;
                           repeat
                              LItem := ALineStyles.Items[i];
                              if LItem <> nil then
                              begin
                                 if ResLInt = LItem.Number then
                                 begin
                                    ALineStyle.Style := LItem.Number;
                                    StyleFound := TRUE;
                                 end
                                 else
                                    Inc (Cnt);
                              end;
                              Inc (i);
                           until StyleFound or (i >= ALineStyles.Capacity);
                        end;
                     end;
          // LineWidth
//          if (ResLInt = 0) or (ResLInt >= WindowsLStyles-1) then begin
               if ReadDDECommandLongInt (Item, DDEStrSect, 2, WgBlockStart,
                  DDEHandler.FDDESepSign, WgBlockEnd, ResLInt, SFound, MStrs) then
               begin
                  if (ResLInt < -2) then ResLInt := -1;
               end
               else
                  ResLInt := -1;
               if ResLInt = -2 then
                  ALineStyle.Width := PProj (AProj)^.Layers^.TopLayer.LineStyle.Width
               else
                  if ResLInt = -1 then
                  begin
                     if ObjectStyleCreated then
                        ALineStyle.Width := PProj (AProj)^.Layers^.TopLayer.LineStyle.Width
                     else
                        ALineStyle.Width := ObjectStyle.LineStyle.Width;
                  end
                  else
                     if ResLInt >= 0 then ALineStyle.Width := ResLInt;
//          end
//          else ALineStyle.Width:=0;
            end;
        // HatchProperties
            if HasPattern then
            begin
               AFillStyle := TFillProperties.Create;
          // properties not supported by SOS
               if ObjectStyleCreated then
               begin
                  AFillStyle.Scale := PProj (AProj)^.Layers^.TopLayer.FillStyle.Scale;
                  AFillStyle.ScaleType := PProj (AProj)^.Layers^.TopLayer.FillStyle.ScaleType;
               end
               else
               begin
                  AFillStyle.Scale := ObjectStyle.FillStyle.Scale;
                  AFillStyle.ScaleType := ObjectStyle.FillStyle.ScaleType;
               end;
          // ForeColor
               if ReadDDECommandLongInt (Item, DDEStrSect, 3, WgBlockStart,
                  DDEHandler.FDDESepSign, WgBlockEnd, ResLInt, SFound, MStrs) then
               begin
                  if (ResLInt < -2) or (ResLInt > $FFFFFF) then ResLInt := -1;
               end
               else
                  ResLInt := -1;
               if ResLInt = -2 then
                  AFillStyle.ForeColor := PProj (AProj)^.Layers^.TopLayer.FillStyle.ForeColor
               else
                  if ResLInt = -1 then
                  begin
                     if ObjectStyleCreated then
                        AFillStyle.ForeColor := PProj (AProj)^.Layers^.TopLayer.FillStyle.ForeColor
                     else
                        AFillStyle.ForeColor := ObjectStyle.FillStyle.ForeColor;
                  end
                  else
                     if ResLInt >= 0 then AFillStyle.ForeColor := ResLInt;
          // HatchType
               if ReadDDECommandLongInt (Item, DDEStrSect, 4, WgBlockStart,
                  DDEHandler.FDDESepSign, WgBlockEnd, ResLInt, SFound, MStrs) then
               begin
                  if (ResLInt < -2) then ResLInt := -1;
               end
               else
                  ResLInt := -1;
               if ResLInt = -2 then
                  AFillStyle.Pattern := PProj (AProj)^.Layers^.TopLayer.FillStyle.Pattern
               else
                  if ResLInt = -1 then
                  begin
                     if ObjectStyleCreated then
                        AFillStyle.Pattern := PProj (AProj)^.Layers^.TopLayer.FillStyle.Pattern
                     else
                        AFillStyle.Pattern := ObjectStyle.FillStyle.Pattern;
                  end
                  else
                     if ResLInt >= 0 then
                     begin
                        if ResLInt < WindowsHStyles then
                           AFillStyle.Pattern := ResLInt
                        else
                        begin
                           if ObjectStyleCreated then
                              AFillStyle.Pattern := PProj (AProj)^.Layers^.TopLayer.FillStyle.Pattern
                           else
                              AFillStyle.Pattern := ObjectStyle.FillStyle.Pattern;
                           StyleFound := FALSE;
                           Cnt := 0;
                           i := 0;
                           AFillStyles := PProj (AProj)^.PInfo^.ProjStyles.XFillStyles;
                           repeat
                              HItem := AFillStyles.Items[i];
                              if HItem <> nil then
                              begin
                                 if ResLInt = HItem.Number then
                                 begin
                                    AFillStyle.Pattern := HItem.Number;
                                    StyleFound := TRUE;
                                 end
                                 else
                                    Inc (Cnt);
                              end;
                              Inc (i);
                           until StyleFound or (i >= AFillStyles.Capacity);
                        end;
                     end;
          // Transparency = BackColor
               if ReadDDECommandLongInt (Item, DDEStrSect, 5, WgBlockStart,
                  DDEHandler.FDDESepSign, WgBlockEnd, ResLInt, SFound, MStrs) then
               begin
                  if (ResLInt < -2) or (ResLInt > 1) then ResLInt := -1;
               end
               else
                  ResLInt := -1;
               if ResLInt = -2 then
                  AFillStyle.BackColor := PProj (AProj)^.Layers^.TopLayer.FillStyle.BackColor
               else
                  if ResLInt = -1 then
                  begin
                     if ObjectStyleCreated then
                        AFillStyle.BackColor := PProj (AProj)^.Layers^.TopLayer.FillStyle.BackColor
                     else
                        AFillStyle.BackColor := ObjectStyle.FillStyle.BackColor;
                  end
                  else
                     if ResLInt = 0 then
                     begin
                        if PProj (AProj)^.Layers^.TopLayer.FillStyle.BackColor >= 0 then
                           AFillStyle.BackColor := PProj (AProj)^.Layers^.TopLayer.FillStyle.BackColor
                        else
                           AFillStyle.BackColor := $FFFFFF;
                     end
                     else
                        if ResLInt = 1 then AFillStyle.BackColor := intNone;
            end;
        // Symbolsize
            if ExistObject^.GetObjType = ot_Symbol then
            begin
               if ReadDDECommandReal (Item, DDEStrSect, 6, WgBlockStart,
                  DDEHandler.FDDESepSign, WgBlockEnd, ResReal, SFound, MStrs) then
               begin
                  if (ResReal <= 0.01) and (DblCompare (ResReal, -2) <> 0) then ResReal := -1;
               end
               else
                  ResReal := -1;
               if DblCompare (ResReal, -1) <> 0 then
               begin
                  if DblCompare (ResReal, -2) = 0 then
                     ResReal := 100
                        ; //else ResReal:=ResReal*100;
                  ASymbol := PSymbol (PProj (AProj)^.Layers^.TopLayer^.IndexObject (PProj (AProj)^.PInfo, ExistObject));
                  if DblCompare (ResReal, dblNoChange) <> 0 then
                  begin
                     if ASymbol^.SizeType = 1 then
                     begin
                        NumTools.Update (ASymbol^.Size, ResReal * 10, ObjectChanged);
                     end
                     else
                        if ASymbol^.SizeType = 2 then
                        begin
                           NumTools.Update (ASymbol^.SizeType, ASymbol^.SymPtr^.DefaultSizeType, ObjectChanged);
                           if ASymbol^.SymPtr^.DefaultSizeType <> 1 then
                           begin
                              NumTools.Update (ASymbol^.Size, ResReal * 100, ObjectChanged);
                           end
                           else
                           begin
                              NumTools.Update (ASymbol^.Size, ResReal * 10, ObjectChanged);
                           end;
                        end
                        else
                           if ASymbol^.SizeType = 0 then
                           begin
                              NumTools.Update (ASymbol^.Size, ResReal * 100, ObjectChanged);
                           end;
//              NumTools.Update(ASymbol^.SizeType,ASymbol^.SizeType,ObjectChanged);
                  end;
                  ASymbol.CalculateClipRect (PProj (AProj)^.PInfo);
                  PProj (AProj)^.UpdateClipRect (ASymbol);
               end;
            end;
        // Visibility
            if ReadDDECommandLongInt (Item, DDEStrSect, 7, WgBlockStart,
               DDEHandler.FDDESepSign, WgBlockEnd, ResLInt, SFound, MStrs) then
            begin
          {ListObject:=PProj(AProj)^.Layers^.IndexObject(PProj(AProj)^.PInfo,ExistObject);}
          {if (ResLInt = 1) and ListObject^.GetState(sf_UnVisible) then begin}
               if (ResLInt = 1) and ExistObject^.GetState (sf_UnVisible) then
               begin
            {ListObject^.SetState(sf_UnVisible,FALSE);}
                  ExistObject^.SetState (sf_UnVisible, FALSE); {Index on layer}
                  ObjectChanged := TRUE;
               end
          {else if (ResLInt = 0) and not ListObject^.GetState(sf_UnVisible) then begin}
               else
                  if (ResLInt = 0) and not ExistObject^.GetState (sf_UnVisible) then
                  begin
            {ListObject^.SetState(sf_UnVisible,TRUE);}
                     ExistObject^.SetState (sf_UnVisible, TRUE);
                     ObjectChanged := TRUE;
                  end;
            end;
{++ IDB_UNDO UnChange}
        // The object's properties will now be changed according to DDE command.
        // We need to save current data about this object to use it later during
        // Undo operation. Ivanoff.
{$IFNDEF WMLT}
            if (WinGisMainForm.WeAreUsingUndoFunction[AProj])
               and
               (WinGisMainForm.UndoRedo_Man.AddObjectInUndoData (AProj, PProj (AProj).Layers.TopLayer, ExistObject)) then
               bUndoDataMustBeStored := TRUE;
{$ENDIF}
{-- IDB_UNDO UnChange}
            if HasLineStyle and UpdateLineStyle (@ObjectStyle^.LineStyle, ALineStyle) then ObjectChanged := TRUE;
            if HasPattern and UpdateFillStyle (@ObjectStyle^.FillStyle, AFillStyle) then ObjectChanged := TRUE;
            if ObjectChanged then
            begin
               RedrawRect.CorrectByRect (OldClipRect);
               RedrawRect.CorrectByRect (ClipRect);
            end;
         end;
      PProj (AProj)^.Layers^.TopLayer := OldTop;
      Item[0] := DDEStrUsed;
   end;
{$ENDIF}
begin
{$IFNDEF WMLT}
    {IndexColl:=New(PViewColl,Init);}
   RedrawRect.Init;
   SItem := New (PIndex, Init (0));
{++ IDB_UNDO UnChange}
   bUndoDataMustBeStored := FALSE;
{-- IDB_UNDO UnChange}
   DDEHandler.DDEData^.ForEach (@DoAll);
{++ IDB_UNDO UnChange}
{$IFNDEF WMLT}
   if (WinGisMainForm.WeAreUsingUndoFunction[AProj]) and (bUndoDataMustBeStored) then
      WinGisMainForm.UndoRedo_Man.SaveUndoData (AProj, utUnChange);
{$ENDIF}
{-- IDB_UNDO UnChange}
   Dispose (SItem, Done);
   DDEHandler.DeleteDDEData;
   PProj (AProj)^.SetModified;
    {cnt:=IndexColl^.GetCount;
    if PProj(AProj)^.Document.loaded then begin
      PProj(AProj)^.Document.ChangeObjectStyle(IndexColl,ObjectLayer);
    end;
    IndexColl^.FreeAll;
    Dispose(INdexColl,Done);}
   if not RedrawRect.IsEmpty then
   begin
      PProj (AProj)^.PInfo^.FromGraphic := TRUE;
      PProj (AProj)^.PInfo^.RedrawRect (RedrawRect);
   end;
{$ENDIF}
end;

{==============================================================================+
  Initalisierungs- und Terminierungscode
+==============================================================================}

initialization
   begin
      TEditObjectStyleHandler.Registrate ('EditObjectProperties');
   end;
{$ENDIF} // <----------------- AXDLL
end.
d.
