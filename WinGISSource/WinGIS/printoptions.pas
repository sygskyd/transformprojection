Unit PrintOptions;

Interface

Uses Classes,GrTools,PageSetup,RegDB,PrintTemplates;

Type  TPrintOptions         = Class(TPersistent)
      Private
       FKeepScale          : Boolean;
       FPrintToFile        : Boolean;
       FPageSetup          : TPageSetup;
       FPrintAngle         : Double;
       FPrintRect          : TRotRect;
       FGraphics           : TGraphics;
       FRegistry           : TRegistryDatabase;
      Public
       Constructor Create;
       Destructor  Destroy; override;
       Property    Graphics:TGraphics read FGraphics write FGraphics;
       Property    KeepScale:Boolean read FKeepScale write FKeepScale;
       Property    PageSetup:TPageSetup read FPageSetup;
       Property    PrintRotation:Double read FPrintAngle write FPrintAngle;
       Property    PrintRect:TRotRect read FPrintRect write FPrintRect;
       Property    PrintToFile:Boolean read FPrintToFile write FPrintToFile;
       Procedure   ReadFromRegistry(Registry:TRegistryDatabase);
       Procedure   WriteToRegistry(Registry:TRegistryDatabase);
     end;

Implementation

Constructor TPrintOptions.Create;
begin
  inherited Create;
  FGraphics:=TGraphics.Create;
  FPageSetup:=FGraphics.PageSetup;
  FRegistry:=TRegistryDatabase.Create;
end;

Destructor TPrintOptions.Destroy;
begin
  FGraphics.Free;
  FRegistry.Free;
  inherited Destroy;
end;

procedure TPrintOptions.ReadFromRegistry(Registry: TRegistryDatabase);
begin
   with Registry do
      begin
         FKeepScale:=ReadBool('KeepScale');
         FPrintRect.X:=ReadFloat('PrintRange\X');
         FPrintRect.Y:=ReadFloat('PrintRange\Y');
         FPrintRect.Width:=ReadFloat('PrintRange\Width');
         FPrintRect.Height:=ReadFloat('PrintRange\Height');
         FPrintRect.Rotation:=ReadFloat('PrintRange\Rotation');
         FPrintToFile:=ReadBool('PrintToFile');
         FGraphics.LoadFromRegistry(Registry,'Graphics\');
      end;
end;

procedure TPrintOptions.WriteToRegistry(Registry: TRegistryDatabase);
begin
   with Registry do
      begin
         WriteBool('KeepScale',FKeepScale);
         WriteFloat('PrintRange\X',FPrintRect.X);
         WriteFloat('PrintRange\Y',FPrintRect.Y);
         WriteFloat('PrintRange\Width',FPrintRect.Width);
         WriteFloat('PrintRange\Height',FPrintRect.Height);
         WriteFloat('PrintRange\Rotation',FPrintRect.Rotation);
         WriteBool('PrintToFile',FPrintToFile);
         FGraphics.SaveToRegistry(Registry,'Graphics\');
      end;
end;

end.
 