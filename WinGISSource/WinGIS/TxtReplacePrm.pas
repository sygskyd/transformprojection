//****************************************************************************
// Unit TxtReplacePrm
//----------------------------------------------------------------------------
// Provides a dialog that allows to search the text objects
// to replace their content.
//----------------------------------------------------------------------------
// Modifications
//  17.04.2001, Y.Glukhov, creation
//****************************************************************************
Unit TxtReplacePrm;

Interface

Uses
    Windows, Messages, SysUtils, ProjHndl,
    AM_View, AM_Def, AM_Proj, MultiLng, Objects, ExtCtrls, StdCtrls,
    Spinbtn, WCtrls, Controls, Classes;

Type TTxtReplaceHandler = class(TProjMenuHandler)
    Procedure   OnStart; override;
end;

Type
  TTxtRplPrmDialog   = Class(TWForm)
    Bevel1           : TBevel;
    CancelBtn        : TButton;
    MlgSection       : TMlgSection;
    OKBtn            : TButton;
    ControlPanel1: TPanel;
    ControlPanel: TPanel;
    WLabel3: TWLabel;
    Group1: TWGroupBox;
    ChkSubStr: TCheckBox;
    RadioGrpSelSrcSet: TRadioGroup;
    ChkCaseSens: TCheckBox;
    edtSearchFor: TEdit;
    edtReplaceBy: TEdit;
    ChkReplaceAll: TCheckBox;
    chkWildcards: TCheckBox;
    ChkZoomFound: TCheckBox;
    ChkRestView: TCheckBox;

    Procedure   FormShow(Sender: TObject);
    Procedure   FormHide(Sender: TObject);
    Procedure   FormCreate(Sender: TObject);
    Procedure   FormDestroy(Sender: TObject);
    procedure ChkReplaceAllOnClick(Sender: TObject);
    procedure ChkZoomFoundOnClick(Sender: TObject);

  Private
    bInitialized    : Boolean;

    Procedure   InitFormData;

  Public
    Project         : PProj;

  end;

var
// To save the last searching parameters
  RTLastSearchFor   : String = '';
  RTLastReplaceBy   : String = '';
  RTLastChkSubStr   : Boolean = True;
  RTLastChkCaseSens : Boolean = True;
  RTLastGrpSelSrcSet: Integer = 1;
  RTLastChkZoomFnd  : Boolean = False;
  RTLastChkOriViewC : Boolean = False;
  RTLastChkOriViewE : Boolean = False;
  RTLastChkReplAll  : Boolean = False;

Implementation

{$R *.DFM}

Uses
  MenuFn, GrTools, Am_Main, IDB_CallBacksDef,
  AM_Index, AM_Text, AM_Layer, AM_ProjO, AM_Admin,
  TxtSearchDlg, TxtReplaceDlg, AttachHndl;

Procedure TTxtReplaceHandler.OnStart;
var
  PrmDialog   : TTxtRplPrmDialog;
  AskDialog   : TTxtReplaceDialog;
  Selected    : PCollection;
  irpl, ipos  : Integer;
  i, len, ir  : Integer;
  Item        : PIndex;
  TxtItem     : PText;
  hcur        : integer;
  ls1, ls2    : AnsiString;
  RedrawRect  : TDRect;
  OldClipRect : TDRect;
  OldViewRect : TRotRect;
  NewViewRect : TRotRect;
  bZoom       : Boolean;

//++ Glukhov Bug#387 Build#152 17.04.01
  function CollectObjects( AItem: PIndex ): Boolean; Far;
  var
    bSel      : Boolean;
    Idx       : PIndex;
    Layer     : PLayer;
    i         : Integer;
  begin
    CollectObjects:= False;
    bSel:= False;
    TxtItem:= Pointer( Project.Layers.IndexObject( Project.PInfo, AItem ) );
    if TxtItem.TemplateMatched( RTLastSearchFor, RTLastChkSubStr, RTLastChkCaseSens, False )
    then  bSel:= True;
    if bSel {and (RTLastGrpSelSrcSet = 2)} then
    begin // whole project
      bSel:= True;
      for i:=1 to Project.Layers.LData.Count-1 do begin
        Layer:= Project.Layers.LData.At(i);
        if (Layer^.GetState( sf_LayerOff )) or
           (Layer^.GetState(sf_Fixed)) then begin
          Idx:= Layer.HasObject( TxtItem );
          if Assigned( Idx ) then begin
            bSel:= False;
            break;
          end;
        end;
      end;
    end;
    if bSel  then Selected.Insert( TxtItem );
  end;
//-- Glukhov Bug#387 Build#152 17.04.01

begin
  {$IFNDEF WMLT}
  Project.SetActualMenu( mn_Select );
  PrmDialog:= TTxtRplPrmDialog.Create( Project.Parent );
  PrmDialog.Project:= Project;
  ir:= PrmDialog.ShowModal;
  PrmDialog.Free;
  if ir = mrOK then begin
    Selected:= New( PCollection, Init( 16, 16 ) );
    // Seach the text
    hcur := Project.PInfo.SetTheCursor(crHourGlass);
    case RTLastGrpSelSrcSet of
      0: begin  // selected objects
        Project.Layers.SelLayer.FirstObjectType( ot_Text, @CollectObjects );
      end;
      1: begin  // current layer
        Project.Layers.TopLayer.FirstObjectType( ot_Text, @CollectObjects );
      end;
      2: begin  // whole project
        PLayer( Project.PInfo.Objects).FirstObjectType( ot_Text, @CollectObjects );
      end;
    end;
    Project.PInfo.SetTheCursor( hcur );

    if Selected.Count = 0 then begin
      // No such objects - Exit
      MsgBox( Project.PInfo.HWindow, 11298, Mb_IconExclamation or Mb_OK, '' );
      Dispose( Selected );
      Exit;
    end;

    // Init the rectangle to redraw (set to empty)
    RedrawRect.Init;
    // Save the current view
    OldViewRect:= Project.CurrentViewRect;
    // Init data to Update the found objects
    len:= Length( RTLastSearchFor );
    // Init a dialog to make the replacement individually
    if not RTLastChkReplAll then begin
      AskDialog:= TTxtReplaceDialog.Create( Project.Parent );
      AskDialog.Project:= Project;
      AskDialog.sSearchFor:= RTLastSearchFor;
    end;

    // For each found objects
    for i:=0 to Selected.Count-1 do begin
      TxtItem:= PText( Selected.At( i ) );
      OldClipRect.InitByRect( TxtItem.ClipRect );
      ls1:= TxtItem^.Text^;
      irpl:= 1;
      ipos:= Pos( Uppercase(RTLastSearchFor), Uppercase(ls1) );
      while ipos > 0 do begin
        irpl:= ipos + irpl - 1;
        if RTLastChkZoomFnd then begin
          // Zoom the found object
          with OldClipRect do NewViewRect:= RotRect(A.X,A.Y,XSize,YSize,0);
//            InflateRotRect( NewViewRect, 10, 10 );
          if (Project.CurrentViewRect.Width <> NewViewRect.Width)
          or (Project.CurrentViewRect.Height <> NewViewRect.Height)
          or (Project.CurrentViewRect.X <> NewViewRect.X)
          or (Project.CurrentViewRect.Y <> NewViewRect.Y)
          then
//          Project.ZoomByRect( NewViewRect, 0, True );
          Project.ZoomByRect( NewViewRect, 1, True );
        end;
        if not RTLastChkReplAll then begin
          AskDialog.TxtClipRect:= OldClipRect;

          // Ask for replacement
          AskDialog.sText:= ls1;
          AskDialog.iSelStart:= irpl-1;
          AskDialog.iSelLength:= len;
          AskDialog.edtReplaceBy.Text:= RTLastReplaceBy;
          ir:= AskDialog.ShowModal;

          if RTLastChkOriViewC then begin
            // Reset the old view
            Project.ZoomByRect( OldViewRect, 0, True );
          end;

        end else ir:= mrOk;
        if ir = mrCancel then break;
        if ir = mrOk then begin
          // Delete the sought substring
          Delete( ls1, irpl, len );
          // Insert the replacing substring
          Insert( RTLastReplaceBy, ls1, irpl );
          // Prepare the search in the same string
          irpl:= irpl + Length( RTLastReplaceBy );
        end
        else begin  // ir = mrIgnore
          // Skip replacement, Prepare the search in the same string
          irpl:= irpl + len;
        end;
        // search in the same string
        ls2:= Copy( ls1, irpl, Length(ls1)-irpl+1 );
        ipos:= Pos( RTLastSearchFor, ls2 );
      end;
      if ls1 <> TxtItem.Text^ then begin
{$IFNDEF AXDLL}
{++ IDB_UNDO Update}
      if (WinGisMainForm.WeAreUsingUndoFunction[Project]) and
         (WinGisMainForm.UndoRedo_Man.AddObjectInUndoData(Project, PView(TxtItem)))
//        (WinGisMainForm.IDB_Man.AddObjectInUndoData(Project, Layers.TopLayer, LView.Index))
      then  WinGisMainForm.UndoRedo_Man.SaveUndoData(Project, utUnChange);
//      then  WinGisMainForm.IDB_Man.SaveUndoData(Project, utUnInsert);
{-- IDB_UNDO Update}
{$ENDIF}

        // Update the changed object
//++ Glukhov Bug#468 Build#162 01.07.01
//        TxtItem.SetNewText( Project.PInfo, ls1, TxtItem.Font, True, False );
        TxtItem.SetNewText( Project.PInfo, ls1, TxtItem.Font );
//-- Glukhov Bug#468 Build#162 01.07.01
        UpdateWithAllAttached( Project, TxtItem, OldClipRect, RedrawRect );
      end;
      if ir = mrCancel then break;
    end;

    // Free the used objects
    if not RTLastChkReplAll  then AskDialog.Free;
    Selected.DeleteAll;
    Dispose( Selected );

    bZoom:= False;
    if RTLastChkOriViewE then begin
      // Reset the old view
      if (Project.CurrentViewRect.Width <> OldViewRect.Width)
      or (Project.CurrentViewRect.Height <> OldViewRect.Height)
      or (Project.CurrentViewRect.X <> OldViewRect.X)
      or (Project.CurrentViewRect.Y <> OldViewRect.Y)
      then  bZoom:= True;
    end;

    if bZoom  then Project.ZoomByRect( OldViewRect, 0, True )
    else
    if not RedrawRect.IsEmpty then begin
    // Redraw the results
      Project.PInfo.FromGraphic:= True;
      Project.PInfo.RedrawRect( RedrawRect );
    end;

  end;
  {$ENDIF}
end;

Procedure TTxtRplPrmDialog.InitFormData;
  begin
    edtSearchFor.Text:= RTLastSearchFor;
    if RTLastSearchFor = ''  then edtSearchFor.Text:= TSLastTxtTemplate;
    edtReplaceBy.Text:= RTLastReplaceBy;
    ChkSubStr.Checked:= RTLastChkSubStr;
    ChkWildcards.Checked:= False;
    ChkWildcards.Enabled:= False;
    ChkCaseSens.Checked:= RTLastChkCaseSens;
    RadioGrpSelSrcSet.ItemIndex:= RTLastGrpSelSrcSet;
    ChkZoomFound.Checked:= RTLastChkZoomFnd;
    ChkReplaceAll.Checked:= RTLastChkReplAll;
    chkRestView.Checked:= RTLastChkOriViewE;
  end;

Procedure TTxtRplPrmDialog.FormShow(Sender: TObject);
  begin
    if bInitialized then Exit;
    edtSearchFor.SetFocus;
    bInitialized:= True;
  end;

Procedure TTxtRplPrmDialog.FormHide(Sender: TObject);
  begin
    if ModalResult=mrOK then begin
      if edtSearchFor.Text = '' then begin
        MsgBox( Project.PInfo.HWindow, 11297, Mb_IconExclamation or Mb_OK, '' );
        edtSearchFor.SetFocus;
        Exit;
      end;
      if ChkReplaceAll.Checked  then ChkZoomFound.Checked:= False;
      RTLastSearchFor:= edtSearchFor.Text;
      RTLastReplaceBy:= edtReplaceBy.Text;
      RTLastChkSubStr:= ChkSubStr.Checked;
      RTLastChkCaseSens:= ChkCaseSens.Checked;
      RTLastGrpSelSrcSet:= RadioGrpSelSrcSet.ItemIndex;
      RTLastChkZoomFnd:= ChkZoomFound.Checked;
      RTLastChkReplAll:= ChkReplaceAll.Checked;
      RTLastChkOriViewE:= chkRestView.Checked;
    end;
    bInitialized:= False;
  end;

Procedure TTxtRplPrmDialog.FormCreate(Sender: TObject);
begin
  InitFormData;
  bInitialized:= False;
end;

Procedure TTxtRplPrmDialog.FormDestroy(Sender: TObject);
begin
  bInitialized:= False;
end;

procedure TTxtRplPrmDialog.ChkZoomFoundOnClick(Sender: TObject);
begin
  if chkZoomFound.Checked
  then  if ChkReplaceAll.Checked  then ChkReplaceAll.Checked:= False;
end;

procedure TTxtRplPrmDialog.ChkReplaceAllOnClick(Sender: TObject);
begin
  if ChkReplaceAll.Checked
  then  if chkZoomFound.Checked  then chkZoomFound.Checked:= False;
end;


//=============================================================================
//  Initialization
//=============================================================================

initialization
  begin
    TTxtReplaceHandler.Registrate('ExtrasTextReplace');
  end;

end.
