{******************************************************************************+
  Unit AM_Proj
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
  Modifikationen
+******************************************************************************}
unit AM_Proj;

interface

{$O-}

uses Messages, WinTypes, WinProcs, SysUtils, AM_Def, AM_View, AM_Poly, AM_CPoly, AM_Point,
  AM_Paint, AM_Admin, AM_Group, AM_Input, AM_Index, AM_Obj, AM_Sym, AM_Layer, AM_Text,
  AM_Sel, AM_Font, AM_Selec, WinDos, AM_Digit,
  AM_Gener, AM_Coll, AM_Splin, AM_Coord, AM_Punkt, AM_Dev, AM_Ini, BmpImage, GifImage,
  AM_Sight, AM_Snap, AM_Meas, AM_Circl, AM_Cut, AM_TextE, AM_Pass,
  Controls, Classes, Forms, AM_StdDl, AM_RText, AM_Offse,
  ResDlg, AM_LDTRF, AM_TRD, AM_ProjM, SLBase, Area, Transfor, XFills, Objects,
  MenuFn, Palette, Statebar, OLE, MenuHndl, RegDB, Measures, XLines, ExtLib, Lists, GrTools,
  AM_Exp, Tooltips, UDraw, UHeap, ToolsLib,
{$IFNDEF AXDLL} // <----------------- AXDLL
  AM_SelDB,
{$ENDIF} // <----------------- AXDLL
  umdlst, AM_Combi, AM_MMedi, Legend, AM_Const, Propcollect, AM_OLE, DDEDef, InpHndl
{$IFNDEF AXDLL} // <----------------- AXDLL
  , PolyEdit1
{$IFNDEF WMLT}
  , PointsCoordDlg, AM_Dlg1, AM_Dlg3, AM_Dlg5, AM_BuGra, AM_Dlg6, AM_DDE
{$ENDIF}
{$ENDIF} // <----------------- AXDLL
  ;

const
  InputColor = C_BLUE;
  InputLine = LT_DOT;

  MinZoom = 30;
  MinSel = 30;
  MinMove = 3;

  spColor = c_Black;
  spLine = lt_Solid;

// current project-version, 12 is 3.4.3, 20 and up is for 4.0

//  vnProject = 26; // Glukhov, ObjAttachment, 16.11.2000
//  vnProject = 27; // Glukhov Bug#468(Text drawn along) Build#162, 02.08.01

  vnProject = 28;

  ObjPoint = 0;
  ImgPoint = 1;
  SelectBmp = 2;

  vp_Colors = 1;
  vp_Gray = 2;
  vp_Bmp = 3;

type
{++ IDB}
  PIDBSettings = ^TIDBSettings;
  TIDBSettings = record
    bUseGlobalIDBSettings: Boolean;
    bLocalWeAreUsingTheIDB: Boolean; // When this flag is set in TRUE state, I mean this project uses the Internal Database kernel. Ivanoff.
    bLocalWeAreUsingTheIDBUserInterface: Boolean; // When this flag is set in TRUE state, I mean this project uses the Internal Database user interface.
                                                    // This flag is ignored if bLocalWeAreUsingTheIDB flag is set in FALSE state. Ivanoff.
    bLocalWeAreUsingUndoFunction: Boolean; // When this flag is set to TRUE state I mean this project uses the Internal Database
                                             // as base for an undo/redo function. This flag is ignored if bLocalWeAreUsingTheIDB flag is set in FALSE state. Ivanoff.
    iLocalUndoStepsCount: integer;
  end;
{-- IDB}

   //TProjectState = set of (psUpdateScrollers);

  PProj = ^TProj;
  TProj = object(TOldObject)
    FName: PChar; { Projektname  }
    FSize: integer; {Project file size  }
    Digitize: PDigitize; { Digitalisieren                      }
    ActualMen: Integer; { aktuelles Men�                      }
    NewMenu: Word;
    ActualSym: PSGroup; { aktuelles Symbol           260397Ray}
    ActSymAngle: Double;
    ActSymLib: string; { aktuelle Symbolbibliothek }
    ActSymSize: Double;
    ActSymSizeType: Integer8;
    ActualFont: TFontData;
    ActualText: string;
    ActualData: Pointer; //++ Glukhov: to reference any data, f.i., image
    Layers: PLayers;
    Layers2: PLayers;
    Line: PInputLine;
    Line1: PInputLine;
    Line2: PInputLine;
    Line3: PInputLine;
    Lines: PInputLines;
    NewPoly: PNewPoly;
    NewPoly1: PNewPoly;
       {brovak}
    NewPolyP: PNewPoly;
       {brovak}

    OffsetItem: PPoly;
    SnapState: Boolean;
    Parent: TForm;
    PointInd: Integer;
    PInfo: PPaint;
    ShowNum: LongInt;
//++ Glukhov Bug#66 BUILD#128 19.09.00
// The data are set when clicked item "Show next selected"
// They are used to update status bar
    bShowNum: Boolean;
    nSelObjs: Integer;
    ZoomRotRect: TRotRect;
//-- Glukhov Bug#66 BUILD#128 19.09.00
    SelList: PUnsortedColl;
    SelRect: PInputRect;
    SelPoint: TDPoint;
    SelAllRect: TDRect;
    ZoomRect: PInputRect;
    TextRect: PInputText;
    SelCircle: PInputCircle;
    InpCircle: PInputCircle;
    InpOffset: PInpOffset;
    MovePoly: PMovePoly;
    MoveCircle: PMoveCircle;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
//  MoveMulti: PMoveMulti;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
    InfoPoly: PInfoPoly;
       {TestPoly         : PInfoPoly;}
    EditSpline: PInputSpline;
    SymbolInp: PInputSymbol;
    InputBox: PInputBox;
    EditTxtBox: PEditTxtRect; // Glukhov Bug#264 Build#161 15.06.01
    SnapItem: PIndex;

       {brovak for 193}
    OLdSnapItem: PIndex;
    IsEditVarious: Boolean;
    EscPressedTwo: Boolean;
    EditVertexMode: byte;
       {--brovak}
    //Rtext=>Text
    WasConverted: boolean;
      {brovak}
    FramePrintSpec: TPrintSpecFrameSettings;

      {brovak}

    Size: TDRect;
    DeleteAll: Boolean;
    OldMenu: Integer;
      {++brovak - for zooming for painting instrument}
    OldCursor: Integer;
      {brovak}
    DigitMode: Boolean;
    RefPoint: TRealPoint;
    InpRef: Boolean;
    RefCnt: Integer;
    GenerData: TGenerDat;
    DigitOpt: TDigitOpt;
    ExplodeOpt: TExplodeDat;
    SnapData: TSnapData;
    SaveSight: TSightData;
    MeasureData: TMeasureData;
    ClickCnt: Integer;
    ClickCnt1: Integer;
    Sights: PSights;
    DigitSyms: PGroup;
    DigitLine: Boolean;
    LastDigit: Word;
    Modified: Boolean;
    NotUsedFonts: Boolean;
    MesPoint1: TDPoint;
    MesPoint2: TDPoint;
    MesCutState: Integer;
    SelectCursor: Integer;
    BitmapCount: Integer;
    BitmapsasPictures: PCollection;
    BitmapsasFrames: PCollection;
    InsertedObjects: PBigUStrColl;
    PolyPartProps: TPolyPartProps;
    MultiMedia: PMultiMedia;
    SRQDatas: PChar;
    DDELineData: PChar;
    DGRLayerDefColl: PCollection;
    NodeList: TSLBase;
    RedrawAfterInsert: Boolean;
    BorderCorrected: Boolean;
    ZoomCursor: Boolean;

    RoutePoint: TDPoint;
    RealPointS: TDPoint;
    RealPointD: TDPOint;
    ToolDlg: Pointer;
    RealPoint: TDPoint;
    FRouteName: string;
    SaveFile: string[79];
    RouteCode: Integer16;
    Flag: Byte;
    NextTranspLayer: PLayer;
         {+++brovak - for zoom on right mouse on painting instruments }
    ZoomOnSelect: boolean;
    IsZoomingNow: boolean;
       {--brovak}
       {++brovak - Transparent for Move/Insert/Delete Point}
    TranspActive: boolean;
       {--brovak}

       {+++Brovak - Variable for FixLayersPassword. Now it write to registry !}
    FixLayerPassword: string;
       {---Brovak}

       {+++brovak}
    InputTextFlag: boolean;
       {--brovak}

       { Variablen f�r LineCut                                                                 }
    CutObj1: PPoly; { LineCut 1. Objekt                                      }
    CutPos1: TDPoint; { LineCut MausPosition                                   }

       { Variablen f�r Combine                                                                 }
    CombineData: TCombineData;
    StartPoints: PPoly; { Start- und Endpunkte der zusammen-                     }
                                      { gef�gten Polys                                         }
    SearchRect: TDRect;
    InputArc: PInputArc;
    TrimmOptions: Integer16;
    AreaCutOptions: TAreaCutDat;

    OldR2: Integer;

    RTrafoState: Byte;
    RTrafoCnt: Word;
    RTrafoInfo: PTLPoints;
    RTrafoType: Byte;
       {++brovak #MOD for Copying part of selected Image}
    CSelectForCut: Byte;
    CutRectOfImage: TRect;
       {--brovak}
    SuperImageOption: Byte; // Sygsky for SuperImages

       { Variablen f�r Diagramm- und Text�bernahme und f�r den Objectzoom                      }
    DiagramType: Byte; { Darstellungsart des Diagrammes                         }
    CircDiagAbsRel: Boolean; { Kreisdiagramm mit summenabh�ngigem Durchmesser         }
    DiagAnnot: Boolean; { Diagramme mit oder ohne Beschriftung                   }
    DAnnotAbsRel: Boolean; { Absolut- oder Relativwerte angeben                     }
    BeamWidth: LongInt; { Balkendiagrammbreite                                   }
    BeamHeight: LongInt; { Balkendiagrammh�he                                     }
    CircleDiameter: LongInt; { Kreisdiagrammdurchmesser                               }
    DiagFont: TFontData; { Textart bei Diagrammbeschriftung                       }
       {AnnotFont        : TFontData;}{ Textart bei automatischer Beschriftung                 }

    Flags: Byte; { Kombination von pfXXXX                                 }

    SelData: TSelectData;
    SelOffsetData: TOffsetData;
    NeighData: TSelectData;
    NeighOffsetData: TOffsetData;
    CombPointAreaData: TPointData;

    SelectData: TSelectData;
    SelectMask: TObjectTypes;
    NeighOrSelect: Boolean;
    SelectOffsetData: TOffsetData;
    OffsetData: TOffsetData;
{$IFNDEF AXDLL} // <----------------- AXDLL
    DBSelection: PDBSelection;
{$ENDIF} // <----------------- AXDLL
    GeoReference: PLPointArray;
    DatabaseSettings: PDBSettings;
       { OLE - Variable}
    OleHandle: LHClientDoc;
    OleStatus: TOleStatus;
    OleRect: PInputRect;

       { TurboRaster }
    RefPtCnt: Integer;

    UserPaletteFile: PChar;
    WinGISPalette: HPalette;

    MarkState: Integer;
    NoSnap: Boolean;

    Radius1: LongInt;
    LastDistance: Double;
    LastLineDist: Double;

    SelNewObjectsDB: Boolean;
    SelNewObjectsGIS: Boolean;

    BMPLoadAction: Boolean;

       { Variablen f�r Symboleditor }
    SymbolMode: Integer; { Symbol oder Projektmode }
    SymEditInfo: TSymEditInfo; { Div. Infos von Symboleditor ben�tigt }

    Construct: PConstruct;

    ScrollXOffset: Double;
    ScrollYOffset: Double;

    HadModifyWarning: Boolean;

    Document: TDocument;
    FileList: TStringlist;
    AHeapmanager: THeap;
    AMemDataLst: TMemList;
    AtConverting: boolean;
    BaseMap: Boolean;
    ExtMap: Boolean;
    AttribFix: Boolean;
    LogFile: Boolean;
    BaseFile: string;
    DBUpdate: Boolean;
    Main: Pointer;
    TurboVector: Boolean;
    CheckID: Longint;
    TVConverting: Boolean;
    CurrentViewRect: TRotRect;
    TSOldActiveLayer: PLayer;
      //ProjectState: TProjectState;
    StatusText: TStringList;
    MenuHandler: TMenuHandler;
    MenuHandlerStack: TList;
    Registry: TRegistryDataBase;

    BTXFileName: LongInt;

    FileVersion: Integer;
    ToolTipLayer: PLayer; { 03-140199-001 }
    Legends: TLegendList;
    LoadResult: Boolean;

{++ Ivanoff NA2 BUILD#106}
    AOldMousePos: TDPoint;
{-- Ivanoff}

{++ Brovak Build 132 BUG 105 ++}
    bDrawRubberBox: Boolean;
    bDraw3Points: Boolean;
{-- Brovak}
{++ IDB}
    bProjectIsNewAndNoSaved: Boolean; // Project was just created and never was saved.
{-- IDB}

    LastClickPos: TPoint;
    CurSnapMarkerPoint: TPoint;
    LastSnapMarkerTickCount: DWord;
    IsCanvas: Boolean;
    NoClickInfo: Boolean;
    ProjSizeOfScreen: TRotRect;
    MUUpdateLayer: PLayer;
    CurPos: TDPoint;
    SplitL1, SplitL2: Integer;
    MouseHintPos: TPoint;
{++ IDB}
    IDBSettings: PIDBSettings;
{-- IDB}
    IgnoreClick: Boolean;
    IntList1: TIntList;
    IntList2: TIntList;
    EditNeighboursMode: Boolean;
    AllowOnlySnappedPoints: Boolean;
    ProjGuid: string;
    ObjTypeFilter: TObjectTypes;
    CurSnapPoly: PPoly;
    CmdList: TStringList;
    Deg400Mode: Boolean;
    bConstrDim: Boolean;
    CoordMsgTarget: Integer;
    FirstPaint: Boolean;

    ScalePolyObj: PPoly;
    ScalePolyPos1: TDPoint;
    ScalePolyPos2: TDPoint;

    constructor Init;
    constructor Load(S: TOLEClientStream);
    procedure InitObjects;
    destructor Done; virtual;
    procedure BreakInput(SetMenu, MenuChange: Boolean);
    function CalculateZoom(Rect: TRotRect; AdditionalFrame: Integer; ShowBorder: Boolean): Double;
    procedure CalculateSize;
    procedure CalculateSymbolClip(Edited: PSortedLong);
    procedure ChangeRedraw(WantRedraw: Byte);
    function CorrectSize(ARect: TDRect; Redraw: Boolean): Boolean;
    procedure CreateSRQDatas(DDELine: PChar);
    function Cursor: TCursor;
{++ IDB}
    procedure DoFillTooltipText(ALayer: PLayer; bPutDataIntoIDB: Boolean; AItem: PIndex; FillWith: Integer; DeleteOld: Boolean);
{ The old variant. It was replaced by me. Ivanoff.
       Procedure   DoFillTooltipText(AItem:PIndex;FillWith:Integer;DeleteOld:Boolean);}
{-- IDB}
    function DoGetTooltipText(aIndex: PIndex; AObjType: Integer; AToolTipType: Integer): string;
    procedure DrawGraphic; overload;
    procedure DrawGraphicTransform; overload;
    procedure DrawGraphic(APInfo: PPaint); overload;
    procedure DrawSelection;
    procedure EnableMenus;
    procedure GenerateDlg;
    function InputText(var Text: string): Boolean;
    procedure MsgMouseWheel(Msg: TMessage);
    procedure MsgActivate;
    procedure MsgDeActivate;
    procedure MsgDefault(var Msg: TMessage);
    procedure MsgDigitize(Info: PDigitInfo);
    procedure MsgKeyDown(var Key: Word; Shift: TShiftState); virtual;
    procedure MsgKeyUp(var Key: Word; Shift: TShiftState); virtual;
    procedure MsgLeftClick(DrawPos: TDPoint);
    function MsgLeftClick1(DrawPos: TDPoint): Boolean;
    procedure MsgLeftDblClick(DrawPos: TDPoint);
    function MsgLeftClick2(DrawPos: TDPoint): Boolean;
    procedure MsgLeftUp(DrawPos: TDPoint);
    procedure MsgMouseMove(DrawPos: TDPoint);
    procedure MsgRightClick(DrawPos: TDPoint);
    procedure MsgRightDblClick(DrawPos: TDPoint);
    procedure MsgRightUp(DrawPos: TDPoint);
    procedure MsgSize;
    procedure MsgTimer(var Msg: TWMTimer);
    function SearchByPoint(Point: TDPoint; const ObjType: TObjectTypes): PIndex; virtual;
    function SearchByPointSnapLayers(Point: TDPoint; const ObjType: TObjectTypes): PIndex; virtual;
    procedure DeSelectAll(ATextOut: Boolean; bIDBCall: boolean = true);
    procedure DeSelectAllWithoutDlg(ATextOut: Boolean);
    procedure SetActualMenu(AMenu: Word; FromBreakInput: Boolean = FALSE);
    procedure SetCursor(ACursor: Integer);
    procedure SetLayerPriority(DDEString: AnsiString = '');
    procedure CreateLayerManagerView;
    procedure SetModified;
    procedure SetOverView;
    procedure SetupProject(AParent: TForm; AName: PChar);
    procedure SetupWindow(AParent: TForm);
       {+++Brovak - for selection by polygon}
    function SnapPoint(DrawPos: TDPoint; Exclude: LongInt;
      const ObjType: TObjectTypes; var SnapPos: TDPoint; var SnaPObj: PIndex; Shift: TShiftState = []; MButton: TMouseButton = mbMiddle): Boolean;
       {--Brovak}
    procedure Store(S: TOldStream); virtual;
    procedure UpdatePaintOffset;
    procedure UpdateCliprect(AView: PView);
    function InsertObject(Item: PView; ASel: Boolean): Boolean;
    function InsertObjectImp(Item: PView; PTextLayer: PLayer): Boolean;
    function InsertObjectOnLayer(Item: PView; ALayer: LongInt; SuppressUndo: Boolean = False): Boolean;
    function InsertObjectWithStyle(Item: PView; ALayer: PLayer;
      const AStyle: TObjStyleV2): Boolean;

    procedure DefaultMenuHandler(MenuFunction: TMenuFunction);
    procedure OnMouseMove(const X, Y: Integer; Shift: TShiftState);
    procedure OnMouseDown(const X, Y: Integer; Button: TMouseButton; Shift: TShiftState);
    procedure OnMouseUp(const X, Y: Integer; Button: TMouseButton; Shift: TShiftState);

    procedure ClearMenuHandlers;
    function FinishMenuHandler: Boolean;
    procedure UpdateStatusBar;
//++ Glukhov PreciseScale Build#161 08.06.01
// Returns the maximum view rectangle
    function GetViewLimit: TDRect;
// Returns the minimum zoom value
    function GetZoomLimit: Double;
// To limit a zoom rect by Size of project (5 times of it)
      //procedure  LimitZoomRect(Rect1: TRotRect; var Rect2: TRotRect);
// Returns True, if the Rect1 was limited, Rect2 is a result
    function LimitZoomRect(Rect1: TRotRect; var Rect2: TRotRect): Boolean;
    procedure ZoomByFactor(const Factor: Double);
    procedure ZoomByFactorWithCenter(const XCenter, YCenter: Double; const Factor: Double);
    procedure ZoomByScaleWithCenter(const XCenter, YCenter: Double; const AScale: Double);
    procedure ZoomByRect(const Rect: TRotRect; AdditionalFrame: Integer; WithBorder: Boolean);
    procedure ClearStatusText;
    procedure SetStatusText(const AText: string; ASave: Boolean);
    function GetMenuFunctionName: string;

    procedure OnOptionsChanged;
    procedure OnGlobalOptionsChanged;
    procedure ReadSettingsFromRegistry;
    procedure WriteSettingsToRegistry;

    procedure InitClipRect;

       // deletes the objects in the list from the project
    procedure DeleteObjectList(ObjectList: TIntList);
    procedure CorrectSelAllRect(const Rect: TDRect);
       {++brovak BUG104 BUILD 130}
    procedure GetNewDrawPos(var DrawPos: TDPOint);
       {--brovak}
       {brovak - 193}
    {++brovak BUG 258 }
    procedure GetNewDrawPos1(var DrawPos: TDPOint);
    procedure GetNewDrawPos2(var DrawPos: TDPOint);
       {--brovak}
    function CheckFixedOrNot: boolean;

    function GiveMeSourceObjectFromSelect(MirrorItem: Pindex): PIndex;
    procedure FindOLdSnapItem;

{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
    procedure DBSetGeoText(Idx: Integer; s: AnsiString);
    procedure DBGetGeoText(Idx: Integer);
    procedure SetStartPoint;
{$ENDIF} // <----------------- AXDLL
    procedure MsgCoordInput(Info: PCoordData);
    procedure AddDBEntry(ACommunicName: PChar; AConProj, ASendProj: Boolean);
    procedure LayerCopy(DoMove: Boolean; DestLayer: PLayer);
{$IFNDEF AXDLL} // <----------------- AXDLL
    procedure DeleteSelected(SilentMode: boolean);
    function DeleteDialog(var DelAll: Boolean): Boolean;
    procedure DBEdit;
    procedure DBEditAuto(AIndex: PIndex);
    procedure SetTransparentSelect(AState: Boolean; ASendLInfo: Boolean);
{$ENDIF} // <----------------- AXDLL
    procedure Select(AItem: PIndex);
    function SelectBetweenPolys(Poly1: Pointer; Poly2: Pointer; const ObjType: TObjectTypes; Inside: Boolean): Boolean;
    function SelectByCircle(Middle: TDPoint; Radius: LongInt; const ObjType: TObjectTypes; Inside: Boolean): Boolean; virtual;
    function SelectByNeighbour(AItem: PIndex): Boolean; virtual;
    function SelectByPoint(Point: TDPoint; const ObjType: TObjectTypes; EditObjectModeFlag: boolean = false): PIndex; virtual;
    function SelectByPoly(Poly: Pointer; const ObjType: TObjectTypes; Inside: Boolean): Boolean;
    function SelectByRect(Rect: TDRect; Inside: Boolean; const ObjType: TObjectTypes = ot_Any): Boolean;
    function SelectByIndex(Id: Integer): Boolean;
{$IFNDEF AXDLL} // <----------------- AXDLL
    function SelectTextStyle(var AFont: TFontData): Boolean;
{$ENDIF} // <----------------- AXDLL
    procedure EditOLEObject;
    procedure ShowPopup;
{$IFNDEF AXDLL} // <----------------- AXDLL
    function InputCoord: Boolean;
    procedure CutLineByLength(Id: Integer; Length: Double; Mode: Integer; DestLayer: PLayer);
    procedure CalcLinePoint;
    function CalcLinePoint2(Id: Integer; Length: Integer): TDPoint;
    procedure SplitLine(Id: Integer; DestLayer: PLayer);
    procedure SendClickInfo(x, y, Button, Mode, DblClick: Integer);
{$ENDIF} // <----------------- AXDLL
    procedure CommitCurrentDigitalization;
    procedure MarkSnapPoint(DrawPos: TDPoint; bNoSnap: Boolean = False);
{$IFNDEF AXDLL} // <----------------- AXDLL
    procedure DBInsertSymbol(Name, Lib: AnsiString; X, Y, Angle, SymSize: Double; ScaleInd: Integer);
    procedure DBCreateBuffer(Id: Integer; DestLayer: string; CornerMode, BufferMode, Dist: Integer);
    procedure MouseActionFromDB(X, Y, Action: Integer);
    procedure ChangeImagePropertiesFromDB(Id, Display, TranspMode, Transp: Integer);
    procedure DeleteLayerListFromDb(s: AnsiString);
    procedure DBCreateView(Name: string; SaveRegion, SaveLayers, IsDefault: Integer);
    procedure DBDeleteView(Name: string);
{$ENDIF} // <----------------- AXDLL
    function PolyCombine(DLayerName: string; Ids: array of Integer; IdCnt: Integer): Integer;
    procedure SetLayerSnap(Layername: string; OnOff: Integer);
{$ENDIF}
    procedure CalculateSplitLength;
    function FindUsedPoint(ARealPoint: TDPOint): integer; //Brovak
    procedure RefreshSymbolMan(Full: Boolean);
    procedure DeleteUnusedSymbols;
    function HasLinkedObj(AIndex: PIndex): Boolean;
    {brovak}
{$IFNDEF AXDLL} // <----------------- AXDLL
    procedure ConvertAnnotationToTexts;
{$ENDIF} // <----------------- AXDLL
    {brovak}
    function SetPixel(X, Y: Integer; ALayerIndex: Integer): PIndex;
    procedure OrientPolys(ADir: Boolean);
    procedure ImportSymbolLib(Filename: AnsiString);
    procedure DoGenerateArea(APoint: TDPoint);
    procedure StoreGuids(S: TOldStream);
    procedure LoadGuids(S: TOldStream); //TOLEClientStream);
    procedure FillGuids;
    function IndexToGuid(AIndex: Integer): TGUID;
    function GUIDToIndex(AGUID: TGUID): Integer;
    function InsertWKTObject(ALayer: PLayer; s: AnsiString): PView;
    procedure ProcScalePolyline;
  end;

implementation

uses Graphics, AM_Child, AM_Main, AM_ProjO, Dialogs, DigIntf, Win32Def, SymEdit,
  ProjHndl, UserIntf, AM_ProjP, NumTools, FontStyleDlg, StyleDef, MultiLng,
  LayerHndl, FlatSB, CommCtrl, UintfCtrls, ProgressDlg, {AM_DDECS,} UGLob, uprojstyle,
  CoordinateSystem, ListHndl, jpeg, ActiveX, ComObj, SymRol, ProjRep, Am_ar,
  trlib32, NewImageHndl, LicenseHndl, ErrHndl,
{$IFNDEF AXDLL} // <----------------- AXDLL
  imgdlg, {++brovak}
{$ENDIF} // <----------------- AXDLL
  AttachHndl, // Glukhov ObjAttachment 15.11.00
  MTextInput, // Glukhov Bug#114 Build#125 20.11.00
  TxtAdvDlg // Glukhov Bug#468 Build#162 01.07.01
{$IFNDEF AXDLL} // <----------------- AXDLL
  , Omegalayer // ++Sygsky
  , PrnSelRange //++ Moskaliov BUG#90 BUILD#150 05.01.01
{$ENDIF} // <----------------- AXDLL
{$IFNDEF WMLT}
  , AM_DBGra
{$IFNDEF AXDLL} // <----------------- AXDLL
  , BusGraphics //++ Moskaliov Business Graphics BUILD#150 17.01.01
{$ENDIF} // <----------------- AXDLL
  , PolyPartition
{$IFNDEF AXDLL} // <----------------- AXDLL
{++ IDB_UNDO}
  , IDB_CallbacksDef, IDB_Consts
{++ IDB_UNDO}
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
  , UserUtils, PolyPartDlg, VEModule, WinOSInfo, MUModule, WMSModule;

function GetShiftState: TShiftState;
begin
  Result := [];
  if GetKeyState(VK_SHIFT) < 0 then
    Include(Result, ssShift);
  if GetKeyState(VK_CONTROL) < 0 then
    Include(Result, ssCtrl);
  if GetKeyState(VK_MENU) < 0 then
    Include(Result, ssAlt);
end;

constructor TProj.Init;
begin
  inherited Init;
  SelAllRect.Assign(0, 0, 0, 0);
  Layers := New(PLayers, Init);
  PInfo := New(PPaint, Init);
  PInfo^.Symbols := New(PSymbols, Init(nil));
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
  PInfo^.ChartViewKinds := New(PChartsLib, Init(nil));
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
  PInfo^.Objects := New(PObjects, Init(nil));
  Size.Init;
  Size.Assign(0, 0, siStartX, siStartY);
  Sights := New(PSights, Init(cosInit, cosExpand));
  MeasureData.Init;
  GenerData.Init;
  DigitOpt.Init;
  ExplodeOpt.Init;
  SaveSight.Init;
  SnapData.Init;
  ActualFont.Init;
  PInfo^.FontSettings.ChosenFont := @ActualFont;
  Registry := TRegistryDataBase.Create;
  InitObjects;
  FillChar(AreaCutOptions, SizeOf(AreaCutOptions), #0);
{$IFNDEF WMLT}
  CombineData.Distance := 20;
  CombineData.MakeAreas := FALSE;
  CombineData.DestLayer := 0;
  TrimmOptions := to_BothLines;
  MultiMedia := New(PMultiMedia, Init);
{$IFNDEF AXDLL} // <----------------- AXDLL
  DatabaseSettings := New(PDBSettings, InitByList(TRUE, IniFile^.DatabaseSettings));
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
  Flags := 0;
  FileVersion := 0;
  Legends := TLegendList.Create;
//++ Glukhov Bug#236 Build#144 14.12.00
//    PInfo^.bMustLoad := TRUE;
//-- Glukhov Bug#236 Build#144 14.12.00
{++ Ivanoff NA2 BUILD#106}
  AOldMousePos.Init(0, 0);
{-- Ivanoff}
{++ IDB}
  bProjectIsNewAndNoSaved := FALSE;
{-- IDB}
{++ Bug#195 Ivanoff}
  FileVersion := vnProject;
  ProjectVersion := vnProject;
{-- Bug#195}
  IsCanvas := False;
end;

destructor TProj.Done;
begin
  try
    if Layers <> nil then
      Dispose(Layers, Done);
    if PInfo <> nil then
      Dispose(PInfo, Done);
    //brovak
    Dispose(NewPolyP, Done);
    //brovak
    Dispose(NewPoly, Done);
    Dispose(SelRect, Done);
    Dispose(ZoomRect, Done);
    Dispose(TextRect, Done);
    Dispose(SelCircle, Done);
    Dispose(InpOffset, Done);
    Dispose(Line, Done);
    Dispose(Line1, Done);
    Dispose(Line2, Done);
    Dispose(Line3, Done);
    Dispose(InpCircle, Done);
    Dispose(Lines, Done);
    Dispose(MovePoly, Done);
    Dispose(MoveCircle, Done);
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
// Dispose(MoveMulti, Done);
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
    Dispose(InfoPoly, Done);
    {Dispose(TestPoly,Done);}
    Dispose(SymbolInp, Done);
    Dispose(Digitize, Done);
    Dispose(EditSpline, Done);
    Dispose(InputBox, Done);
    Dispose(EditTxtBox, Done); // Glukhov Bug#264 Build#161 15.06.01
    SelList^.DeleteAll;
    Dispose(SelList, Done);
    Dispose(BitmapsasPictures, Done);
    Dispose(BitmapsasFrames, Done);
{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}
    if DatabaseSettings <> nil then
      Dispose(DatabaseSettings, Done);
    if MultiMedia <> nil then
      Dispose(MultiMedia, Done);
{$ENDIF}
{$ENDIF} // <----------------- AXDLL
    if InsertedObjects <> nil then
      Dispose(InsertedObjects, Done);
    Dispose(OleRect, Done);
    MeasureData.Done;
    GenerData.Done;
    DigitOpt.Done;
    ExplodeOpt.Done;
    SaveSight.Done;
    SnapData.Done;
    ActualFont.Done;
    Size.Done;
    if Sights <> nil then
      Dispose(Sights, Done);
    if RTrafoInfo <> nil then
      FreeMem(RTrafoInfo, sizeof(TLPoint) * 8190);
    {AnnotFont.Done;}
    DiagFont.Done;
    if CombPointAreaData.LayersAreas <> nil then
      Dispose(CombPointAreaData.LayersAreas, Done);
    if CombPointAreaData.LayersPoints <> nil then
      Dispose(CombPointAreaData.LayersPoints, Done);
    if DGRLayerDefColl <> nil then
      Dispose(DGRLayerDefColl, Done);
    ClearMenuHandlers;
    MenuHandlerStack.Free;
    Registry.Free;
    StatusText.Free;
    SymEditInfo.Free;
{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}
    Dispose(DBSelection, Done);
{$ENDIF}
{$ENDIF} // <----------------- AXDLL
    Legends.Free;

    BHeapManager := AHeapManager;
    Document.Free;
    FileList.Free;
    AHeapManager.Free;
    AMemDataLst.Free;
    AHeapManager := nil;
    BHeapManager := AHeapManager;

{++ IDB}
    Dispose(SELF.IDBSettings);
{-- IDB}
    IntList1.Free;
    IntList2.Free;
    CmdList.Free;

    inherited Done;
  except
    on E: Exception do
    begin
      DebugMsg('TProj.Done', E.Message);
    end;
  end;
end;

procedure TProj.InitObjects;
var
  Syms: PLayer;
  ProjMod: Boolean;
begin
  Layers^.Parent := @Self;
  ObjTypeFilter := ot_Any;
  CurSnapPoly := nil;
  NewPoly := New(PNewPoly, Init(InputColor, InputLine, FALSE));
    //brovak
  NewPolyP := New(PNewPoly, Init(InputColor, InputLine, FALSE));
    //brovak
  SelRect := New(PInputRect, Init(InputColor, InputLine));
  ZoomRect := New(PInputRect, Init(InputColor, InputLine));
  SelCircle := New(PInputCircle, Init(InputColor, InputLine));
  InpOffset := New(PInpOffset, Init(InputColor, InputLine));
  TextRect := New(PInputText, Init(InputColor, InputLine));
  Line := New(PInputLine, Init(InputColor, InputLine));
  Line1 := New(PInputLine, Init(InputColor, InputLine));
  Line2 := New(PInputLine, Init(InputColor, InputLine));
  Line3 := New(PInputLine, Init(InputColor, InputLine));
  InpCircle := New(PInputCircle, Init(InputColor, InputLine));
  Lines := New(PInputLines, Init(InputColor, InputLine));
  MovePoly := New(PMovePoly, Init(InputColor, InputLine));
  MoveCircle := New(PMoveCircle, Init(InputColor, InputLine));
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
// MoveMulti := New(PMoveMulti, Init(InputColor, InputLine));
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
  InfoPoly := New(PInfoPoly, Init(InputColor, InputLine));
    {TestPoly:=New(PInfoPoly,Init(c_LRed,InputLine));}
  SymbolInp := New(PInputSymbol, Init(InputColor, InputLine));
  EditSpline := New(PInputSpline, Init(InputColor, InputLine, spColor, spLine));
  InputBox := New(PInputBox, Init(InputColor, InputLine, spColor, spLine, m_Default));
//++ Glukhov Bug#264 Build#161 15.06.01
  EditTxtBox := New(PEditTxtRect, Init(InputColor, InputLine, spColor, spLine, m_Default));
//-- Glukhov Bug#264 Build#161 15.06.01
  Syms := PLayer(PInfo^.Symbols);
  if Syms^.Data^.GetCount > 0 then
    ActualSym := PSGroup(Syms^.Data^.At(0))
  else
    ActualSym := nil;
  ActSymLib := '';
  ActualMen := -2;
  SnapState := FALSE;
  DeleteAll := FALSE;
  AllowOnlySnappedPoints := FALSE;
  Digitize := New(PDigitize, Init);
  DigitMode := FALSE;
  DigitSyms := nil;
  RefPoint.Init(0, 0);
  ClickCnt := 0;
  Modified := FALSE;
  ZoomCursor := FALSE;
  MesCutState := 0;
  DigitLine := FALSE;
  RTrafoCnt := 0;
  RTrafoState := SelectBmp;
  GetMem(RTrafoInfo, sizeof(TLPoint) * 8190);
  SelList := New(PUnsortedColl, Init);
  EditNeighboursMode := False;
  ShowNum := 0;
//++ Glukhov Bug#66 BUILD#128 19.09.00
  bShowNum := False;
//-- Glukhov Bug#66 BUILD#128 19.09.00
  PInfo^.AProj := @self;
  SelectCursor := crArrow;
  BitmapCount := 0;
  BitmapsasPictures := New(PCollection, Init(5, 5));
  BitmapsasFrames := New(PCollection, Init(5, 5));
  SelData.SelectWith := swPoly;
  SelData.SelectObject := FALSE;
  SelData.SelectInside := FALSE;
  SelData.SelectTypes := ot_Any;
  SelData.SelectLayer := 0;
  SelOffsetData.RoundEdges := FALSE;
  SelOffsetData.OffsetType := otStreight;
  NeighData.SelectWith := swPoly;
  NeighData.SelectObject := FALSE;
  NeighData.SelectInside := FALSE;
  NeighData.SelectTypes := [ot_CPoly];
  NeighData.SelectLayer := 0;
  NeighOffsetData.RoundEdges := FALSE;
  NeighOffsetData.OffsetType := otStreight;
  OffsetData.RoundEdges := FALSE;
  OffsetData.OffsetType := otSingle;
  CombPointAreaData.LayersAreas := nil;
  CombPointAreaData.LayersPoints := nil;
  CombPointAreaData.ObjectTypes := [ot_Pixel];
  InsertedObjects := New(PBigUStrColl, Init);
  OleRect := New(PInputRect, Init(InputColor, InputLine));
  DGRLayerDefColl := New(PCollection, Init(5, 5));
{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}
  DBSelection := New(PDBSelection, Init(@Self));
{$ENDIF}
{$ENDIF} // <----------------- AXDLL
  Digitize^.DigitOptions := @DigitOpt;
  InpRef := FALSE;
  RedrawAfterInsert := TRUE;
  BorderCorrected := FALSE;

  SymbolMode := sym_Project; { "normaler" Projektmode }
  SymEditInfo := TSymEditInfo.Create;
  with SymEditInfo do
  begin
    SymbolToEdit := nil; { f�r Symboleditor 1196Ray}
    SymEditChild := nil; { kein Symboleditor-Child }
    SymParent := nil; { kein Vaterprojekt eines Symboleditors }
    SymEditor := nil; { kein aktiver Symboleditor }
    SymExtLib := nil; { ext. Bib., die editiert wird }
  end;
  if (PSymbols(PInfo^.Symbols)^.ExternLibs.Count > 0) then
  begin
    PSymbols(PInfo^.Symbols)^.CheckForDiffSyms(PInfo, ProjMod);
                                   { auf unterschiedliche Symbole untersuchen }
    if (ProjMod) then
      SetModified; { Projekt wurde durch Symbolversionanpassungen ge�ndert ! }
  end;

  if (PSymbols(PInfo^.Symbols)^.ExternLibs.Count > 0) then
    PSymbols(PInfo^.Symbols)^.CheckForDiffSyms(PInfo, Modified); { auf unterschiedliche Symbole untersuchen }

  ScrollXOffset := 0;
  ScrollYOffset := 0;

   //ProjectState := [];

  FirstPaint := True;

  MenuHandler := nil;
  MenuHandlerStack := TList.Create;
  StatusText := TStringList.Create;
  BTXFileName := 90000000;
{++ IDB}
  New(SELF.IDBSettings);
{-- IDB}
  ReadSettingsFromRegistry;
{$IFNDEF AXDLL} // <----------------- AXDLL
  OnGlobalOptionsChanged;
{$ENDIF} // <----------------- AXDLL
    {AnnotFont.Init;
    AnnotFont:=ActualFont;}
  DiagFont.Init;
  DiagFont := ActualFont;
  DiagFont.Height := 1;
{$IFNDEF AXDLL} // <----------------- AXDLL
  IniFile^.ReadDiagOpts(DiagramType, BeamWidth, BeamHeight, CircleDiameter, CircDiagAbsRel, DiagAnnot, DAnnotAbsRel);
{$ENDIF} // <----------------- AXDLL
  BMPLoadAction := False;
  HadModifyWarning := FALSE;
{$IFNDEF WMLT}
  GeoReference := nil;
{$ENDIF}
  SelNewObjectsDB := FALSE;
  SelNewObjectsGIS := FALSE;
  Document := TDocument.Create(@Self, PInfo);
  FileList := TStringList.Create;
  AHeapManager := THeap.Create;
  AHeapManager.Pinfo := PInfo;
  AHeapManager.Proj := @Self;
  Document.Proj := @Self;
  AMemDataLst := TMemList.Create;
  Atconverting := FALSE;
  LogFile := FALSE;
  ActSymSize := dblFromObject;
  ActSymSizeType := stProjectScale;
  ActSymAngle := dblFromObject;
  bConstrDim := False;
  CurPos.Init(0, 0);
  CoordMsgTarget := 0;
  IntList1 := TIntList.Create;
  IntList2 := TIntList.Create;
  CmdList := TStringList.Create;

  ScalePolyObj := nil;
  ScalePolyPos1.Init(0, 0);
  ScalePolyPos2.Init(0, 0);
end;

procedure TProj.SetupWindow
  (
  AParent: TForm
  );
begin
  Parent := AParent;
  PInfo^.SetupWindow(AParent);
  WinGISMainForm.ActualProj := @Self;
end;

procedure TProj.SetupProject
  (
  AParent: TForm;
  AName: PChar
  );

  procedure DoRText(Item: PRText); far;
  begin
    Item^.CalculateClipRect(PInfo);
    UpdateClipRect(Item);
  end;
begin
  Parent := AParent;
  WinGISMainForm.ActualProj := @Self;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
{$IFNDEF AXDLL} // <----------------- AXDLL
// ++ Cadmensky
//   WinGISMainForm.LoadingProj := nil;
  WinGISMainForm.LoadingProj := @Self;
// -- Cadmensky
{$ENDIF} // <----------------- AXDLL
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
  PInfo^.SetupPaint(MeasureData.ArrowLen);
//++ Glukhov Bug#63 BUILD#128 14.09.00
{$IFNDEF AXDLL} // <----------------- AXDLL
  OnGlobalOptionsChanged;
{$ENDIF} // <----------------- AXDLL
//-- Glukhov Bug#63 BUILD#128 14.09.00
  Digitize^.Parent := Parent;

  FName := AName;

  try
    if PInfo^.IsMU then
    begin
      MU.ApplyChanges(True);
    end;
    MU.CheckReorganize;
  except
    on E: Exception do
    begin
      DebugMsg('TProj.SetupProject', E.Message);
    end;
  end;

  if (Sights <> nil) and ((Sights^.Default <> -1) and (Sights^.Default >= 0) and (Sights^.Default < Sights^.Count)) then
  begin
    ProcSetSight(@Self, Sights^.At(Sights^.Default));
    if RectEmpty(CurrentViewRect) then
    begin
      SetOverview;
    end;
  end
  else
  begin
    SetOverview;
  end;

  if FileVersion < 2000 then
  begin
// ++ Cadmensky
    try
// -- Cadmensky
      PLayer(PInfo^.Objects)^.FirstObjectType(ot_RText, @DoRText);
// ++ Cadmensky
    except
    end;
// -- Cadmensky
  end;
end;

{******************************************************************************+
  Procedure TProj.DrawGraphic
--------------------------------------------------------------------------------
  Draws all layers except the selection-layer.
+******************************************************************************}

procedure TProj.DrawGraphic();
begin
  try
    if (TurboVector) and (not TVConverting) then
      BHeapManager := AHeapManager;

    if (VEForm <> nil) then
    begin
      VEForm.APInfo := PInfo;
      VEForm.DrawBingMapsView;
    end;

    if (WMSForm <> nil) and (WMSForm.CheckBoxOnOff.Checked) and (PInfo^.ProjectionSettings.ProjectProjection <> 'NONE') then
    begin
      WMSForm.APInfo := PInfo;
      WMSForm.DrawMap;
    end;

    if Document.Loaded then
    begin
      Layers^.Draw2(@Self, PInfo);
    end
    else
    begin
      Layers^.Draw(PInfo);
    end;

    FirstPaint := False;

    UpdateLayersViewsLegendsLists(@Self);
{$IFNDEF AXDLL}
    if IniFile^.ReadBoolean('SETTINGS', 'GraphicReadyMessage', True) then
    begin
      WingisMainForm.ActualChild.RedrawCanvas;
    end;
{$ENDIF}
  except
    on E: Exception do
    begin
      DebugMsg('TProj.DrawGraphic', E.Message);
    end;
  end;
end;
{*******************************************************************************

Drawing proj and transforming coords with new Projection

*******************************************************************************}
procedure TProj.DrawGraphicTransform();
begin
  try
    if (TurboVector) and (not TVConverting) then
      BHeapManager := AHeapManager;

    if (VEForm <> nil) then
    begin
      VEForm.APInfo := PInfo;
      VEForm.DrawBingMapsView;
    end;

    if (WMSForm <> nil) and (WMSForm.CheckBoxOnOff.Checked) and (PInfo^.ProjectionSettings.ProjectProjection <> 'NONE') then
    begin
      WMSForm.APInfo := PInfo;
      WMSForm.DrawMap;
    end;

    if Document.Loaded then
    begin
      Layers^.Draw2(@Self, PInfo);
    end
    else
    begin
      Layers^.DrawTransform(PInfo);
    end;

    FirstPaint := False;

    UpdateLayersViewsLegendsLists(@Self);
{$IFNDEF AXDLL}
    if IniFile^.ReadBoolean('SETTINGS', 'GraphicReadyMessage', True) then
    begin
      WingisMainForm.ActualChild.RedrawCanvas;
    end;
{$ENDIF}
  except
    on E: Exception do
    begin
      DebugMsg('TProj.DrawGraphic', E.Message);
    end;
  end;
end;

{******************************************************************************+
  Procedure TProj.DrawGraphic
--------------------------------------------------------------------------------
  Draws all layers except the selection-layer.
+******************************************************************************}

procedure TProj.DrawGraphic(APInfo: PPaint);
begin
  try
    if (TurboVector) and (not TVConverting) then
      BHeapManager := AHeapManager;

    if (VEForm <> nil) then
    begin
      VEForm.APInfo := APInfo;
      VEForm.DrawBingMapsView;
    end;

    if (WMSForm <> nil) and (WMSForm.CheckBoxOnOff.Checked) and (PInfo^.ProjectionSettings.ProjectProjection <> 'NONE') then
    begin
      WMSForm.APInfo := APInfo;
      WMSForm.DrawMap;
    end;

    if Document.Loaded then
    begin
      Layers^.Draw2(@Self, APInfo);
    end
    else
    begin
      Layers^.Draw(APInfo);
    end;
  except
    on E: Exception do
    begin
      DebugMsg('TProj.DrawGraphic', E.Message);
    end;
  end;
end;

{******************************************************************************+
  Procedure TProj.DrawSelection
--------------------------------------------------------------------------------
  Draws the selection-layer and the input-handlers currenlty active.
+******************************************************************************}

procedure TProj.DrawSelection;
var
  Cnt: Integer;
begin
  try
    if (TurboVector) and (not TVConverting) then
      BHeapManager := AHeapManager;
    if Document.Loaded then
      Document.DrawSelection(PInfo);
    Layers^.SelLayer^.Draw(PInfo);
    TooltipThread.DrawFixedFloatingTexts(PInfo);
    if Assigned(MenuHandler) then
    begin
      MenuHandler.Paint;
    end;
    for Cnt := 0 to MenuHandlerStack.Count - 1 do
      TMenuHandler(MenuHandlerStack[Cnt]).Paint;
    case ActualMen of
      mn_PartFree,
        mn_PartParallel,
        mn_CParallel,
        mn_CBufferDraw,
        mn_CIslands,
        mn_DrawSymLine,
        mn_Poly,
        mn_EntToDrawAlong, // Glukhov Bug#468 Build#162 16.08.01
        mn_Distance,
        mn_CPoly,
        mn_Combine,
        mn_CombineSel,
        mn_CombineSelOne:
        begin
          NewPoly^.Draw(PInfo, TRUE);
          NewPoly^.DrawMarks(PInfo);
        end;
      mn_Select: SelRect^.Draw(PInfo);
      mn_OffsetForSel,
        mn_OffsetOK: InpOffset^.Draw(PInfo);
      mn_MesPoint,
        mn_Move,
        mn_Snap: Line^.Draw(PInfo);
      mn_SnapPoly,
        mn_InsPOK: Lines^.Draw(PInfo);
      mn_Text,
        mn_DBText,
        mn_SizeRot: TextRect^.Draw(PInfo);
      mn_MovePartPuff,
        mn_MovePartPara,
        mn_MovePoly: MovePoly^.Draw(PInfo);
      mn_MoveCircle: MoveCircle^.Draw(PInfo);
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
//  mn_MoveMulti: MoveMulti^.Draw(PInfo);
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
      mn_InfoPoly:
        begin
          InfoPoly^.Draw(PInfo);
                           {TestPoly^.Draw(PInfo);}
        end;
      mn_SymbolInp: SymbolInp^.Draw(PInfo);
      mn_SplineEdit: EditSPline^.Draw(PInfo);
      mn_EditImage,
//++ Glukhov Bug#264 Build#161 15.06.01
//    mn_EditTxtRect, // Glukhov Bug#170 BUILD#125 03.11.00
//-- Glukhov Bug#264 Build#161 15.06.01
      mn_EditOLE: InputBox^.Draw(PInfo);
      mn_EditTxtRect: EditTxtBox^.Draw(PInfo); // Glukhov Bug#264 Build#161 15.06.01
      mn_DrawForSelect:
        case SelectData.SelectWith of
          swCircle: SelCircle^.Draw(PInfo);
          swLine,
            swOffset,
            swPoint,
            swPoly: NewPoly^.Draw(PInfo, TRUE);
          swRect: SelRect^.Draw(PInfo);
        end;
      mn_OrthoMark,
        mn_DistMark:
        begin
          if MarkState > 0 then
            Line^.Draw(PInfo);
          if MarkState > 1 then
            Line1^.Draw(PInfo);
        end;
      mn_LineMark:
        begin
          if MarkState > 0 then
            Line^.DrawIt(PInfo);
          if MarkState > 1 then
            Line1^.DrawIt(PInfo);
          if MarkState > 2 then
            Line2^.DrawIt(PInfo);
          if MarkState > 4 then
            Line3^.DrawIt(PInfo);
        end;
      mn_CircleCutMark:
        begin
          if MarkState > 0 then
            Line^.Draw(PInfo);
          if MarkState > 1 then
          begin
            Lines^.Draw(PInfo);
            SelCircle^.Draw(PInfo);
          end;
          if MarkState > 2 then
            InpCircle^.Draw(PInfo);
        end;
      mn_CircleMark:
        begin
          Line^.Draw(PInfo);
          SelCircle^.Draw(PInfo);
        end;
      mn_AngleMark:
        begin
          if MarkState > 0 then
            Line^.Draw(PInfo);
          if MarkState > 2 then
            Line1^.Draw(PInfo);
          if MarkState > 3 then
            Line2^.Draw(PInfo);
        end;
      mn_CutImage, mn_SuperImage:
        begin
          if SelRect^.IsOn then
            SelRect^.Draw(PInfo);
        end;
    end;
    PInfo^.StopPaint;
    PInfo^.SymbolDrawError := 0;
  except
    on E: Exception do
    begin
      DebugMsg('TProj.DrawSelection', E.Message);
    end;
  end;
end;

{*************************************************************************************}
{ Function TProj.SelectByPoint                                                        }
{*************************************************************************************}

{$IFNDEF WMLT}

function TProj.SelectByPoint
  (
  Point: TDPoint; { Mausclick-Position                           }
  const ObjType: TOBjectTypes; { erlaubte Objekttypen (ot_XXX)                }
  EditObjectModeFlag: boolean = false
  )
  : PIndex; { Element das selektiert wurde, oder NIL, wenn }
                                       { kein Element unter den Cursor                }
var
  i: Integer;
  TopestLayer: PLayer;
  Rect: TDRect;
  ARes: Boolean;

  function SelectAllLayers(Item: PLayer): Boolean;
  begin
    Result := False;
    if (not Item^.GetState(sf_LayerOff)) and (Item^.Text <> nil)
      and (Item^.GetState(sf_DoSelect)) and (not Item^.GetState(sf_LayerOffAtGen)) then
      if Item^.SelectAllByPoint(PInfo, Point, ObjType) then
      begin
        Result := True;
        TopestLayer := Item;
      end;
  end;

  function SelectOneLayer
      (
      Item: PLayer
      )
      : Boolean;
  begin
    Result := FALSE;
    if (not Item^.GetState(sf_LayerOff)) and (Item^.Text <> nil)
      and (not Item^.GetState(sf_LayerOffAtGen)) then
      if Item^.SelectAllByPoint(PInfo, Point, ObjType) then
      begin
        TopestLayer := Item;
        Result := TRUE;
      end;
  end;

  function SelectOneObject
      (
      Item: PLayer
      )
      : Boolean;
  begin
    Result := FALSE;
    if (not Item^.GetState(sf_LayerOff)) and (Item^.Text <> nil)
      and (not Item^.GetState(sf_LayerOffAtGen)) then
      if Item^.SelectByPoint(PInfo, Point, ObjType) <> nil then
      begin
        TopestLayer := Item;
        Result := TRUE;
      end;
  end;
begin
  ARes := False;
  SetStatusText(GetLangText(4003), TRUE);
  PInfo^.GetCurrentScreen(Rect);
//  Width := (Rect.XSize);
  with Layers^ do
    if ((ActualMen = mn_Select) or (ActualMen = mn_DrawForSelect) or
      (ActualMen = mn_SelForSelect)) and TranspLayer then
    begin
      Result := nil;
      TopestLayer := TopLayer;
{$IFNDEF WMLT}
//      if not DBSelection^.Selecting then Self.DeSelectAll(TRUE);
{$ENDIF}
       {for i:=0 to LData^.Count-1 do PLayer(LData^.At(i))^.DeSelectAll(PInfo);}
      if (PInfo^.SelectionSettings.TransparentMode and $04 <> 0) then
{++ Ivanoff BUG#635 BUILD#100}
{If the project is not symbol-editor project selection go on old way.
If the project is symbol-editor project selection on the reference layer is forbidden.}
      begin
        if SymbolMode <> sym_SymbolMode then
        begin
{-- Ivanoff}
          for i := LData^.Count - 1 downto 0 do
          begin
            ARes := ARes or SelectAllLayers(PLayer(LData^.At(i)));
          end;
        end
{++ Ivanoff BUG#635 BUILD#100}
        else
        begin
          for i := LData^.Count - 1 downto 1 do
          begin
            if Player(LData.Items[i]).Text^ <> 'ReferenceLayer' then
            begin
              SelectAllLayers(PLayer(LData^.At(i)))
            end;
          end;
        end;
      end
{-- Ivanoff}
      else
        if (PInfo^.SelectionSettings.TransparentMode and $02 <> 0) then
        begin
          i := 0;
          while ((i < LData^.Count) and not SelectOneLayer(PLayer(LData^.At(i)))) do
            Inc(i);
        end
        else
        begin
          i := 0;
          while ((i < LData^.Count) and not SelectOneObject(PLayer(LData^.At(i)))) do
            Inc(i);
        end;
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
      if not DBSelection^.Selecting or
        (DBSelection^.Selecting and DBSelection^.OldTransp) then
        if EditObjectModeFlag = false then
          SetTopLayer(TopestLayer)
        else
          SetTopLayer(TopestLayer, true); //move to fixed but not set to select mode
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
    end
    else
    begin
      if not TopLayer^.GetState(sf_LayerOffAtGen) then
        Result := TopLayer^.SelectByPoint(PInfo, Point, ObjType)
      else
        Result := nil;
    end;
  if Document.loaded then
    Document.SelectByPoint(Point, ot_Any);
  if (Result = nil) and (ARes) then
  begin
    Result := Layers^.SelLayer^.Data^.At(Layers^.SelLayer^.Data^.GetCount - 1);
  end;
  ClearStatusText;
  UserInterface.Update([uiMenus, uiStatusBar]);
end;
{$ENDIF}

function TProj.SearchByPoint
  (
  Point: TDPoint;
  const ObjType: TObjectTypes
  )
  : PIndex;
begin
  SetStatusText(GetLangText(4019), TRUE);
  SearchByPoint := Layers^.SearchByPoint(PInfo, Point, ObjType);
  ClearStatusText;
end;

function TProj.SearchByPointSnapLayers
  (
  Point: TDPoint;
  const ObjType: TObjectTypes
  )
  : PIndex;
var
  BResult: PIndex;

  function DoAll
      (
      Item: PLayer
      )
      : Boolean; far;
  begin
    DoAll := FALSE;
    if Item^.UseForSnap then
    begin
      BResult := Item^.SearchByPoint(PInfo, Point, ObjType);
      DoAll := BResult <> nil;
    end;
  end;
begin
  SetCursor(crHourGlass);
  SetStatusText(GetLangText(4019), TRUE);
  BResult := nil;
  Layers^.LData^.FirstThat(@DoAll);
  ClearStatusText;
  SetCursor(crDefault);
  SearchbyPointSnapLayers := BResult;
end;

{$IFNDEF WMLT}

function TProj.SelectByRect(Rect: TDRect; Inside: Boolean; const ObjType: TObjectTypes = ot_Any): Boolean;
var
  Border: PCPoly;
  i: Integer;
  ARes: Boolean;

  function SelectAllLayersByPoly(Item: PLayer): Boolean;
  begin
    Result := False;
    if (not Item^.GetState(sf_LayerOff))
      and (Item^.Text <> nil)
      and (Item^.GetState(sf_DoSelect))
      and (not Item^.GetState(sf_LayerOffAtGen)) then
    begin
      Result := Item^.SelectByPoly(PInfo, Border, ObjType, Inside);
    end;
  end;

begin
  Result := False;
  ARes := False;
  if IniFile^.FrameSelect and ((not Layers^.TopLayer^.GetState(sf_LayerOffAtGen)) or (Layers^.TranspLayer)) then
  begin
    if Inside and (Abs(PInfo^.ViewRotation) < 1E-4) then
    begin
      PInfo^.SelectionRect.Init;
      Border := SelRect^.GetBorder(PInfo);
      if Layers^.TranspLayer then
      begin
        for i := Layers^.LData^.Count - 1 downto 1 do
        begin
          ARes := SelectAllLayersByPoly(PLayer(Layers^.LData^.At(i)));
          Result := Result or ARes;
        end;
      end
      else
      begin
        if ObjType = ot_Any then
        begin
          Result := Layers^.SelectByRectFast(PInfo, Rect);
        end
        else
        begin
          Result := Layers^.TopLayer^.SelectByPoly(PInfo, Border, ObjType, Inside);
        end;
      end;
      if Document.Loaded then
      begin
        Document.SelectByRect(Rect, FALSE);
      end;
      if Result then
      begin
        CorrectSelAllRect(PInfo^.SelectionRect);
        PInfo^.GetCurrentScreen(PInfo^.SelectionRect);
        PInfo^.RedrawRect(PInfo^.SelectionRect);
      end;
    end
    else
    begin
      Border := SelRect^.GetBorder(PInfo);
      try
        if Layers^.TranspLayer then
        begin
          for i := Layers^.LData^.Count - 1 downto 1 do
          begin
            ARes := SelectAllLayersByPoly(PLayer(Layers^.LData^.At(i)));
            Result := Result or ARes;
          end;
          PInfo^.RedrawRect(PInfo^.SelectionRect);
        end
        else
        begin
          Result := SelectByPoly(Border, ObjType, Inside);
        end;
      finally
        Dispose(Border, Done);
      end;
    end;
    if Layers^.SelLayer^.Data^.GetCount = 1 then
    begin
      for i := Layers^.LData^.Count - 1 downto 1 do
      begin
        if PLayer(Layers^.LData^.At(i))^.HasObject(PIndex(Layers^.SelLayer^.Data^.At(0))) <> nil then
        begin
          Layers^.TopLayer := PLayer(Layers^.LData^.At(i));
          break;
        end;
      end;
    end;
    UserInterface.Update([uiMenus, uiStatusBar, uiLayers]);
  end;
end;

function TProj.SelectByIndex(Id: Integer): Boolean;
var
  AItem: PIndex;

  function DoAll(Item: PLayer): Boolean; far;
  begin
    Result := False;
    AItem := Item^.HasIndexObject(Id);
    if AItem <> nil then
    begin
      Item^.Select(PInfo, AItem);
      Result := True;
      SelectByIndex := True;
    end;
  end;

begin
  SelectByIndex := False;
  Layers^.LData^.FirstThat(@DoAll);
end;
{$ENDIF}

function TProj.InsertObject
  (
  Item: PView;
  ASel: Boolean
  )
  : Boolean;
var
  LView: PIndex;
  Corrected: Boolean;
begin
  Result := FALSE;
  try
    if Layers^.IndexObject(PInfo, Item) = nil then {Objekt in Objektliste vorhanden?}
      PLayer(PInfo^.Objects)^.InsertObject(PInfo, Item, TRUE); {in Objektliste aufnehmen}
    LView := New(PIndex, Init(Item^.Index));
    LView^.ClipRect.InitByRect(Item^.ClipRect);
    if Layers^.IndexObject(PInfo, LView) <> nil then
    begin {Objekt in Objektliste vorhanden?}
      Layers^.TopLayer^.InsertObject(PInfo, LView, TRUE); {in TopLayer einf�gen}
{$IFNDEF AXDLL} // <----------------- AXDLL
{++ IDB}
{$IFNDEF WMLT}
        // I insert this new project item in the Internal Database.
      if WinGisMainForm.WeAreUsingTheIDB[@SELF] then
        WinGisMainForm.IDB_Man.InsertItem(@SELF, Layers.TopLayer, LView);
{++ IDB_UNDO UnInsert}
      if (WinGisMainForm.WeAreUsingUndoFunction[@SELF])
        and
        (WinGisMainForm.UndoRedo_Man.AddObjectInUndoData(@SELF, Layers.TopLayer, LView.Index)) then
        WinGisMainForm.UndoRedo_Man.SaveUndoData(@SELF, utUnInsert);
{-- IDB_UNDO UnInsert}
      with Layers^.TopLayer^.TooltipInfo do
      begin
        if (BDEOrLocal = int_Local) and (FillAuto = int_Auto) and (AutoText > int_Manu) then
          DoFillTooltipText(nil, FALSE, Item, AutoText, FALSE);
        if (BDEOrLocal = int_IDB) and (FillAuto = int_Auto) and (AutoText > int_Manu) then
          DoFillTooltipText(Layers.TopLayer, TRUE, Item, AutoText, FALSE);
      end;
{ The old variant. It was replaced by me. Ivanoff.
        with Layers^.TopLayer^.TooltipInfo do begin
          if (BDEOrLocal = int_Local) and (FillAuto = int_Auto) and (AutoText > int_Manu) then
            DoFillTooltipText(Item,AutoText,FALSE);
        end;}
{$ENDIF}
{-- IDB}
{$ENDIF} // <----------------- AXDLL
      if ASel then
        Layers^.TopLayer^.Select(PInfo, LView);
      if RedrawAfterInsert then
        CorrectSize(Item^.ClipRect, TRUE) {sollte bei vielen Inserts FALSE sein}
      else
      begin
        Corrected := CorrectSize(Item^.ClipRect, FALSE); {TRUE wenn neue Projektgrenzen}
        if not BorderCorrected then
          BorderCorrected := Corrected;
      end;
      if SelNewObjectsGIS and not LView^.GetState(sf_Selected) then
        Layers^.TopLayer^.Select(PInfo, LView);
      SetModified;
      UserInterface.Update([uiMenus]);
      Result := TRUE;
    end
    else
      Dispose(LView, Done);
  except
  end;
end;

function TProj.InsertObjectWithStyle
  (
  Item: PView;
  ALayer: PLayer;
  const AStyle: TObjStyleV2
  )
  : Boolean;
var
  LView: PIndex;
  Corrected: Boolean;
begin
  Result := FALSE;
  try
    if Layers^.IndexObject(PInfo, Item) = nil then {Objekt in Objektliste vorhanden?}
      PLayer(PInfo^.Objects)^.InsertObject(PInfo, Item, TRUE);
    LView := New(PIndex, Init(Item^.Index));
    LView^.ClipRect.InitByRect(Item^.ClipRect);
    if Layers^.IndexObject(PInfo, LView) <> nil then
    begin
      LView^.ChangeStyle(PInfo, AStyle, FALSE);
      ALayer^.InsertObject(PInfo, LView, TRUE);
      if RedrawAfterInsert then
        CorrectSize(Item^.ClipRect, TRUE) {sollte bei vielen Inserts FALSE sein}
      else
      begin
        Corrected := CorrectSize(Item^.ClipRect, FALSE); {TRUE wenn neue Projektgrenzen}
        if not BorderCorrected then
          BorderCorrected := Corrected;
      end;
{++ IDB}
{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}
        // I insert this new project item in the Internal Database.
      if WinGisMainForm.WeAreUsingTheIDB[@SELF] then
        WinGisMainForm.IDB_Man.InsertItem(@SELF, ALayer, LView);
{++ IDB_UNDO UnInsert}
      if (WinGisMainForm.WeAreUsingUndoFunction[@SELF])
        and
        (WinGisMainForm.UndoRedo_Man.AddObjectInUndoData(@SELF, ALayer, LView.Index)) then
        WinGisMainForm.UndoRedo_Man.SaveUndoData(@SELF, utUnInsert);
{-- IDB_UNDO UnInsert}
      with ALayer^.TooltipInfo do
      begin
        if (BDEOrLocal = int_Local) and (FillAuto = int_Auto) and (AutoText > int_Manu) then
          DoFillTooltipText(nil, FALSE, Item, AutoText, FALSE);
          // Insert TToolInfo into the Internal Database.
        if (BDEOrLocal = int_IDB) and (FillAuto = int_Auto) and (AutoText > int_Manu) then
          DoFillTooltipText(ALayer, TRUE, Item, AutoText, FALSE);
      end;
{ The old variant. It was replaced by me. Ivanoff.
        with ALayer^.TooltipInfo do begin
          if (BDEOrLocal = int_Local) and (FillAuto = int_Auto) and (AutoText > int_Manu) then
            DoFillTooltipText(Item,AutoText,FALSE);
        end; }
{$ENDIF}
{$ENDIF} // <----------------- AXDLL
{-- IDB}
      SetModified;
      Result := TRUE;
    end
    else
      Dispose(LView, Done);
  except
  end;
end;

function TProj.InsertObjectImp
  (
  Item: PView;
  PTextLayer: PLayer
  )
  : Boolean;
var
  LView: PIndex;
  Corrected: Boolean;
begin
  Result := FALSE;
  try
    if Layers^.IndexObject(PInfo, Item) = nil then {Objekt in Objektliste vorhanden?}
      PLayer(PInfo^.Objects)^.InsertObject(PInfo, Item, TRUE);
    LView := New(PIndex, Init(Item^.Index));
    LView^.ClipRect.InitByRect(Item^.ClipRect);
    if Layers^.IndexObject(PInfo, LView) <> nil then
    begin
      if PTextLayer <> nil then
        PTextLayer^.InsertObject(PInfo, LView, TRUE)
      else
        Layers^.TopLayer^.InsertObject(PInfo, LView, TRUE);
{++ IDB}
{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}
        // I insert this new project item in the Internal Database.
      if WinGisMainForm.WeAreUsingTheIDB[@SELF] then
        WinGisMainForm.IDB_Man.InsertItem(@SELF, Layers.TopLayer, LView);
{++ IDB_UNDO UnInsert}
      if (WinGisMainForm.WeAreUsingUndoFunction[@SELF])
        and
        (WinGisMainForm.UndoRedo_Man.AddObjectInUndoData(@SELF, Layers.TopLayer, LView.Index)) then
        WinGisMainForm.UndoRedo_Man.SaveUndoData(@SELF, utUnInsert);
{-- IDB_UNDO UnInsert}
      with Layers^.TopLayer^.TooltipInfo do
      begin
        if (BDEOrLocal = int_Local) and (FillAuto = int_Auto) and (AutoText > int_Manu) then
          DoFillTooltipText(nil, TRUE, Item, AutoText, FALSE);
          // Insert TToolInfo into the Internal Database.
        if (BDEOrLocal = int_IDB) and (FillAuto = int_Auto) and (AutoText > int_Manu) then
          DoFillTooltipText(Layers.TopLayer, TRUE, Item, AutoText, FALSE);
      end;
{ The old variant. It was replaced by me. Ivanoff.
        with Layers^.TopLayer^.TooltipInfo do begin
          if (BDEOrLocal = int_Local) and (FillAuto = int_Auto) and (AutoText > int_Manu) then
            DoFillTooltipText(Item,AutoText,FALSE);
        end;}
{$ENDIF}
{$ENDIF} // <----------------- AXDLL
{-- IDB}
      if RedrawAfterInsert then
        CorrectSize(Item^.ClipRect, TRUE) {sollte bei vielen Inserts FALSE sein}
      else
      begin
        Corrected := CorrectSize(Item^.ClipRect, FALSE); {TRUE wenn neue Projektgrenzen}
        if not BorderCorrected then
          BorderCorrected := Corrected;
      end;
      SetModified;
      Result := TRUE;
    end;
  except
  end;
end;

function TProj.InsertObjectOnLayer
  (
  Item: PView;
  ALayer: LongInt;
  SuppressUndo: Boolean = False
  )
  : Boolean;
var
  LView: PIndex;
  Layer: PLayer;
  Corrected: Boolean;
begin
  Result := FALSE;
  try
    Layer := Layers^.IndexToLayer(ALayer); {Ziellayer}
    if Layer <> nil then
    begin
      PLayer(PInfo^.Objects)^.InsertObject(PInfo, Item, TRUE); {in Objektliste aufnehmen}
      LView := New(PIndex, Init(Item^.Index));
      LView^.ClipRect.InitByRect(Item^.ClipRect);
      if Layers^.IndexObject(PInfo, LView) <> nil then
      begin {Objekt in Objektliste vorhanden?}
        Layer^.InsertObject(PInfo, LView, TRUE); {in TopLayer einf�gen}
{++ IDB}
{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}
        // I insert this new project item in the Internal Database.
        if WinGisMainForm.WeAreUsingTheIDB[@SELF] then
          WinGisMainForm.IDB_Man.InsertItem(@SELF, Layer, LView);

        if not SuppressUndo then
        begin
{++ IDB_UNDO UnInsert}
          if (WinGisMainForm.WeAreUsingUndoFunction[@SELF])
            and
            (WinGisMainForm.UndoRedo_Man.AddObjectInUndoData(@SELF, Layer, LView.Index)) then
            WinGisMainForm.UndoRedo_Man.SaveUndoData(@SELF, UtUninsert);
{-- IDB_UNDO UnInsert}
        end;

        with Layer^.TooltipInfo do
        begin
          if (BDEOrLocal = int_Local) and (FillAuto = int_Auto) and (AutoText > int_Manu) then
            DoFillTooltipText(nil, FALSE, Item, AutoText, FALSE);
          // Insert TToolInfo into the Internal Database.
          if (BDEOrLocal = int_IDB) and (FillAuto = int_Auto) and (AutoText > int_Manu) then
            DoFillTooltipText(Layer, TRUE, Item, AutoText, FALSE);
        end;
{ The old variant. It was replaced by me. Ivanoff.
          with Layer^.TooltipInfo do begin
            if (BDEOrLocal = int_Local) and (FillAuto = int_Auto) and (AutoText > int_Manu) then
              DoFillTooltipText(Item,AutoText,FALSE);
          end;}
{$ENDIF}
{$ENDIF} // <----------------- AXDLL
{-- IDB}
        if RedrawAfterInsert then
          CorrectSize(Item^.ClipRect, TRUE) {sollte bei vielen Inserts FALSE sein}
        else
        begin
          Corrected := CorrectSize(Item^.ClipRect, FALSE); {TRUE wenn neue Projektgrenzen}
          if not BorderCorrected then
            BorderCorrected := Corrected;
        end;
        if SelNewObjectsGIS and not LView^.GetState(sf_Selected) then
          Layer^.Select(PInfo, LView);
        SetModified;
        Result := TRUE;
      end
      else
        Dispose(LView, Done);
    end;
  except
  end;
end;

constructor TProj.Load(S: TOLEClientStream);
var
  AllCount: Integer;
  Count: Integer;
  ProgressDialog: TProgressDialog;
  CurLayer: PLayer;

  function DoSymbols1(Item: PSGroup): Boolean; far;
  begin
    Item^.UseCount := 0;
    DoSymbols1 := FALSE;
  end;

  function DoSymbols2(Item: PSymbol): Boolean; far;
  var
    ASymbol: PSGroup;
  begin
    ASymbol := Item^.GetSymbol(PInfo);
    if ASymbol <> nil then
    begin
      Inc(ASymbol^.UseCount);
    end;
    DoSymbols2 := FALSE;
  end;

  procedure DoAll(Item: PText); far;
  begin
    if Item^.GetObjType = ot_Text then
    begin
      Inc(PInfo^.Fonts^.GetFont(Item^.Font.Font)^.UseCount);
    end
    else
    begin
      if Item^.GetObjType = ot_Symbol then
      begin
        Inc(PSymbol(Item)^.GetSymbol(PInfo)^.UseCount);
      end;
    end;
  end;

  procedure LoadOldBitmapFills;
  var
    Count: SmallInt;
    Limit: SmallInt;
    Delta: SmallInt;
    Duplicates: Boolean;
    ObjNumber: Word;
    Cnt: Integer;
    PatNumber: LongInt;
    LastNumber: LongInt;
    Style: TBitmapXFill;
    BmpInfo: ^TBitmapInfoHeader;
    BMPData: Pointer;
    DIBData: array[0..SizeOf(TBitmapInfoHeader) + 192] of Char;
  begin
    S.Read(ObjNumber, SizeOf(ObjNumber));
    S.Read(Count, SizeOf(SmallInt));
    S.Read(Limit, SizeOf(SmallInt));
    S.Read(Delta, SizeOf(SmallInt));
    for Cnt := 0 to Count - 1 do
    begin
      Style := TBitmapXFill.Create;
      Style.Name := IntToStr(Cnt);
      S.Read(ObjNumber, SizeOf(ObjNumber));
      BmpInfo := Pointer(@DIBData);
      with BmpInfo^ do
      begin
        biSize := sizeof(TBitmapInfoHeader);
        biWidth := 8;
        biHeight := 8;
        biPlanes := 1;
        biBitCount := 24;
        biCompression := bi_RGB;
        biSizeImage := 0;
        biXPelsPerMeter := 0;
        biYPelsPerMeter := 0;
        biClrUsed := 0;
        biClrImportant := 0;
      end;
      Style.DIBBitmap.Handle := CreateDIBSection(0, PBitmapInfo(@DIBData)^, DIB_RGB_COLORS, BmpData, 0, 0);
      S.Read(BmpData^, 192);
      S.Read(PatNumber, SizeOf(PatNumber));
      Style.Number := PatNumber;
      PInfo^.ProjStyles.XFillStyles.Add(Style);
    end;
    S.Read(Duplicates, SizeOf(Duplicates));
    S.Read(LastNumber, SizeOf(LastNumber));
    PInfo^.ProjStyles.XFillStyles.LastNumber := LastNumber + 1;
  end;

  function DoAllObjects(AObject: PIndex): Boolean; far;
  begin
    if Layers^.ExistsObject(AObject) = 0 then
    begin
      PLayer(PInfo^.Objects)^.DeleteIndex(PInfo, AObject);
    end;
    Inc(Count);
    ProgressDialog.Progress := 100 * Count / AllCount;
    Result := FALSE;
  end;

  procedure ReadSymParamsForSize;
  var
    SymSInfo: PCollection;

    procedure DoAll(Item: PSSizeInfo); far;
    var
      FoundSym: PSGroup;
    begin
      if Item^.ShowRSize then
      begin
        FoundSym := PSGroup(PSymbols(PInfo^.Symbols)^.HasIndexObject(Item^.Index));
        if FoundSym <> nil then
        begin
          FoundSym^.DefaultSize := Item^.ShowSize;
          FoundSym^.DefaultSizeType := stScaleIndependend;
        end;
      end;
    end;

  begin
    SymSInfo := Pointer(S.Get);
    if SymSInfo <> nil then
    begin
      SymSInfo^.ForEach(@DoAll);
      Dispose(SymSInfo, Done);
    end;
  end;

  procedure GetObjMasters(AView: PView);
  begin
    if AView <> nil then
    begin
      AView^.GetObjMaster(PInfo);
    end;
  end;

var
  NextObj: Word;
  Version: Byte;
  Buffer: array[0..1200] of Char;
  Cnt, Cnt2, n: Integer;
  ALayer: PLayer;
  AIndex: PIndex;
  SnapOpt: TSnapOpt;
  AObject: PView;
begin
  inherited Init;
  LoadResult := False;
  try
    TurboVector := FALSE;
    Registry := TRegistryDataBase.Create;
//    S.Proj := SELF;
    S.Project := @self;
    StrCopy(Buffer, 'WINGIS');
    OLEStatus := OLE_ERROR_NAME;
    WasConverted := false;
    bNoImageLoading := (ssShift in GetShiftState);
    if S.ParentWindow <> nil then
    begin
      OleStatus := OleRegisterClientDoc(Buffer, TMDIChild(TOLEClientStream(S).ParentWindow).FName, 0, OleHandle);
      if S.ParentWindow <> nil then
      begin
        if (OleStatus <> OLE_OK) and (OleStatus <> OLE_ERROR_ALREADY_REGISTERED) then
        begin
          MsgBox(WinGISMainForm.Handle, 11102, Mb_IconExclamation or Mb_OK, '');
        end
        else
        begin
          OleStatus := OLE_OK;
        end;
      end;
    end;
    S.Read(NextObj, SizeOf(NextObj));
    S.Seek(S.GetPos - SizeOf(NextObj), soFromBeginning);
    if (NextObj = rn_Layers) or (NextObj = rn_LayersOld) then
    begin
      Convert := FALSE;
      Layers := Pointer(S.Get);
      PInfo := Pointer(S.Get);
      if Convert then
      begin
        PLayer(PInfo^.Objects)^.Data^.ForEach(@DoAll);
      end;
      S.Read(NextObj, SizeOf(NextObj));
      S.Seek(S.GetPos - SizeOf(NextObj), soFromBeginning);
      if NextObj <> rn_Sights then
      begin
        Sights := New(PSights, Init(cosInit, cosExpand));
      end
      else
      begin
        Sights := Pointer(S.Get);
      end;
      Size.Load(S);
      MeasureData.Init;
      GenerData.Init;
      DigitOpt.Init;
      ExplodeOpt.Init;
      SaveSight.Init;
      SnapData.Init;
      ActualFont.Init;
    end
    else
    begin
      S.Read(Version, SizeOf(Version));
      FileVersion := Version;
      ProjectVersion := Version;
      if Version >= 20 then
      begin
        Registry.LoadFromStream(S);
      end;
      S.Read(Flags, SizeOf(Flags));
      Layers := Pointer(S.Get);
{$IFNDEF AXDLL}
      WinGISMainForm.LoadingProj := @Self;
{$ENDIF}
      PInfo := Pointer(S.Get);
      Sights := Pointer(S.Get);
      if Sights = nil then
      begin
        Sights := New(PSights, Init(cosInit, cosExpand));
      end;
      Size.Load(S);
      MeasureData.Load(S);
      GenerData.Load(S);
      DigitOpt.Load(S);
      if Version < 20 then
      begin
        S.Read(Buffer, 12);
      end;
      ExplodeOpt.Load(S);
      SaveSight.Load(S);
      SnapData.Load(S);
      ActualFont.Load(S);
      PInfo^.FontSettings.ChosenFont := @ActualFont;
      if (Version > 1) and (Version < 20) then
      begin
        SnapOpt.Load(S);
      end;
      if Version < 2 then
      begin
        CombineData.Distance := 20;
        CombineData.MakeAreas := FALSE;
        CombineData.DestLayer := 0;
        TrimmOptions := to_BothLines;
        FillChar(AreaCutOptions, SizeOf(AreaCutOptions), #0);
      end
      else
      begin
        S.Read(CombineData, SizeOf(CombineData));
        S.Read(TrimmOptions, SizeOf(TrimmOptions));
        S.Read(AreaCutOptions, SizeOf(AreaCutOptions));
      end;
      if Version > 2 then
      begin
        if Version < 20 then
        begin
          LoadOldBitmapFills;
          if Version < 12 then
          begin
            S.Read(Buffer, 271 * sizeof(TColorRef));
          end
          else
          begin
            S.Read(Buffer, 272 * sizeof(TColorRef))
          end;
        end;
      end;
      if Version > 3 then
      begin
        MultiMedia := Pointer(S.Get);
      end
      else
      begin
        MultiMedia := New(PMultiMedia, Init);
      end;
      if MultiMedia = nil then
      begin
        MultiMedia := New(PMultiMedia, Init);
      end;
      if Version > 8 then
      begin
        S.Read(RouteCode, Sizeof(RouteCode));
        S.Read(SaveFile, Sizeof(SaveFile));
      end;
      if Version < 10 then
      begin
        for Cnt := 0 to Layers^.LData^.count - 1 do
        begin
          ALayer := Layers^.LData^.at(cnt);
          for cnt2 := 0 to ALayer^.Data^.GetCount - 1 do
          begin
            AIndex := ALayer^.DATa^.At(cnt2);
            AObject := Pointer(PLayer(PInfo^.Objects)^.IndexObject(PInfo, AIndex));
            AIndex^.ClipRect.InitByRect(AObject^.ClipREct);
          end;
        end;
      end;
      if Version < 12 then
      begin
        Sights^.SetGeneralData(Layers);
      end;
      if PObjects(PInfo^.Symbols)^.FileVersion <= 3 then
      begin
        PLayer(PInfo^.Symbols)^.Data^.ForEach(@DoSymbols1);
        PLayer(PInfo^.Objects)^.FirstObjectType(ot_Symbol, @DoSymbols2);
      end;
      if Version > 11 then
      begin
        S.Read(TurboVector, SizeOf(Boolean));
        S.Read(CheckId, SizeOf(LongInt));
        if Version < 20 then
        begin
          S.Read(BaseFile, SizeOf(SaveFile));
        end
        else
        begin
          S.Read(BaseFile, SizeOf(string));
        end;
        S.Read(BaseMap, SizeOf(Boolean));
        S.Read(ExtMap, SizeOf(Boolean));
        S.Read(AttribFix, SizeOf(Boolean));
        S.Read(LogFile, SizeOf(Boolean));
        TVConverting := TRUE;
        if BaseFile = '' then
        begin
          TurboVector := FALSE;
        end;
        if Turbovector then
        begin
          TVConverting := FALSE;
          if Version < 20 then
          begin
            MessageDialog(MlgStringList['Main', 16001], mtError, [mbOK], 0);
          end;
        end;
      end;
      if (Version < 20) and (S.Status = stOK) then
      begin
        ReadSymParamsForSize;
        S.Status := stOK;
        if (Version < 10) or (PObjects(PInfo^.Symbols)^.FileVersion <= 4) then
        begin
          InitClipRect;
        end;
      end;
      if Version >= 25 then
      begin
        DatabaseSettings := Pointer(S.Get);
      end
      else
      begin
{$IFNDEF AXDLL}
        DatabaseSettings := New(PDBSettings, InitByList(TRUE, IniFile^.DatabaseSettings));
{$ENDIF}
      end;
      if Version < 20 then
      begin
        Registry.WriteDateTime('\Project\Info\CreationDate', Now);
        ProgressDialog := TProgressDialog.Create(Parent);
        ProgressDialog.Caption := 'Converting project, please wait ...';
        ProgressDialog.Dialog.Show;
        for Cnt := 0 to Layers^.LData^.Count - 1 do
        begin
          CurLayer := Layers^.LData^.At(Cnt);
          CurLayer^.Data^.Sort;
          CurLayer^.Data^.RemoveDuplicates;
        end;
        Count := 0;
        AllCount := PLayer(PInfo^.Objects)^.Data^.GetCount;
        PLayer(PInfo^.Objects)^.Data^.LastThat(@DoAllObjects);
        ProgressDialog.Free;
      end;
      PLayer(PInfo^.Objects)^.Data^.ForEach(@GetObjMasters);
      InitObjects;
      ProjectVersion := vnProject;
      LoadGuids(S);
      FillGuids;
    end;
    LoadResult := True;
  except
    on E: Exception do
    begin
      DebugMsg('TProj.Load', E.Message);
    end;
  end;
end;

procedure TProj.Store(S: TOldStream);
var
  Version: Byte;
begin
{$IFNDEF AXDLL}
  SetCursor(crHourGlass);
{$ENDIF}
  Version := vnProject;
  S.Write(Version, SizeOf(Version));
  WriteSettingsToRegistry;
  Registry.StoreToStream(S);
  S.Write(Flags, SizeOf(Flags));
  S.Put(Layers);
  S.Put(PInfo);
  S.Put(Sights);
  Size.Store(S);
  MeasureData.Store(S);
  GenerData.Store(S);
  DigitOpt.Store(S);
  ExplodeOpt.Store(S);
  SaveSight.Store(S);
  SnapData.Store(S);
  ActualFont.Store(S);
  S.Write(CombineData, SizeOf(CombineData));
  S.Write(TrimmOptions, SizeOf(TrimmOptions));
  S.Write(AreaCutOptions, SizeOf(AreaCutOptions));
  S.Put(MultiMedia);
  S.Write(RouteCode, Sizeof(RouteCode));
  S.Write(SaveFile, Sizeof(SaveFile));
  S.Write(TurboVector, SizeOf(Boolean));
  S.Write(CheckId, SizeOf(LongInt));
  S.Write(BaseFile, SizeOf(string));
  S.Write(BaseMap, SizeOf(Boolean));
  S.Write(ExtMap, SizeOf(Boolean));
  S.Write(AttribFix, SizeOf(Boolean));
  S.Write(LogFile, SizeOf(Boolean));
  S.Put(DatabaseSettings);
  StoreGuids(S);
{$IFNDEF AXDLL}
  SetCursor(crDefault);
{$ENDIF}
end;

procedure TProj.SetActualMenu(AMenu: Word; FromBreakInput: Boolean);
var
  DoBreak: Boolean;
  EndIt: Boolean;
  I: Integer;
begin
  if (MenuHandler = nil) and (AMenu = ActualMen) and
    not (ActualMen in [mn_SelForSelect, mn_DrawForSelect]) then
  begin
    UserInterface.Update([uiMenus, uiCursor, uiStatusBar]);
    Exit;
  end;
  if Assigned(MenuHandler) then
    ClearMenuHandlers
  else
  begin
    MenuFunctions[GetMenuFunctionName].Checked := FALSE;
    if ActualMen = mn_Select then
      MenuFunctions['EditSelect'].Checked := FALSE;
  end;
//++ Glukhov Bug#66 BUILD#128 19.09.00
  bShowNum := False;
//-- Glukhov Bug#66 BUILD#128 19.09.00

  if FromBreakInput then
    ActualMen := 0;

  NewMenu := ActualMen;
  EndIt := TRUE;
   { Men�punkte bei denen ActualMen nicht gesetzt werden darf                          }
  case AMenu of
    mn_TranspLayerDB:
      begin
      end;
  else
    EndIt := FALSE;
  end;
  if not EndIt then
  begin
    case AMenu of
      mn_Symbol:
        if (ActualSym = nil) then
        begin
          MessageDialog(MlgStringList['SymbolMsg', 11], mtInformation, [mbOk], 0);
          MenuFunctions['DrawSymbol'].Checked := FALSE;
        end
        else
          NewMenu := AMenu;
      mn_Text:
        if PInfo^.FontsInstalled then
        begin
          ActualText := '';
          if InputText(ActualText) then
          begin
            NewMenu := AMenu;
            DeSelectAll(TRUE);
            SetStatusText(GetLangText(11737), TRUE);
            InputTextFlag := true;
          end
          else
          begin
//++ Glukhov Bug#291 Build#150 24.01.01
            ActualMen := mn_text;
//-- Glukhov Bug#291 Build#150 24.01.01
            SetActualMenu(mn_Select);
            Exit;
          end;
        end;
      mn_DBText:
        if PInfo^.FontsInstalled then
        begin
{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}
          ProcGetDBText(@Self, ActualText);
{$ENDIF}
{$ENDIF} // <----------------- AXDLL
          NewMenu := AMenu;
          DeSelectAll(TRUE);
        end;
      mn_RefPoints:
        begin
          Digitize^.NewReference;
          InpRef := FALSE;
          RefCnt := 1;
          SetStatusText(Format(GetLangText(7100), [RefCnt]), TRUE);
          NewMenu := AMenu;
        end;
    else
      NewMenu := AMenu;
    end; // END of CASE statement

    DoBreak := TRUE;

    case ActualMen of
      mn_MesCut,
        mn_MesOrtho:
        begin
          ClearStatusText;
          MesCutState := 0;
          DoBreak := FALSE;
        end;
    end; // END of CASE statement

    case NewMenu of
      mn_RefPoints,
        mn_OffsetForSel,
        mn_OffsetOK,
        mn_GenerRect: DoBreak := FALSE;
      mn_CParallelS: DoBreak := FALSE;
      mn_MovePartPara:
        begin
          DoBreak := FALSE;
                                 {if ActualMen=mn_PartParallel then}
          SetStatusText(GetLangText(11753), FALSE);
                                 {else if ActualMen=mn_PartFree then begin
                                 end;}
        end;
      mn_Offset:
        if ActualMen = mn_OffsetOK then
          DoBreak := FALSE;
      mn_SelForSelect,
        mn_DrawForSelect:
        begin
          NeighOrSelect := SelectData.NeighOrSelect;
//               if ActualMen = mn_OffsetForSel then
          DoBreak := FALSE;
        end;
      mn_RTrafo:
        begin
          RTrafoCnt := 0;
          RTrafoState := SelectBmp;
        end;
//++ Glukhov LoadImageInRect BUILD#123 20.07.00
      mn_SetImageRect:
        begin
          RTrafoCnt := 0;
          SetStatusText(GetLangText(11765), True);
        end;
//-- Glukhov LoadImageInRect BUILD#123 20.07.00
//++ Glukhov ObjAttachment 11.10.00
      mn_AttachObject: SetStatusText(GetLangText(11770), True);
//-- Glukhov ObjAttachment 11.10.00
//++ Glukhov Bug#203 Build#151 08.02.01
      mn_GetCoordinates: SetStatusText(GetLangText(11296), True);
//-- Glukhov Bug#203 Build#151 08.02.01
//++ Glukhov Bug#468 Build#162 16.08.01
      mn_SelToDrawAlong: SetStatusText(GetLangText(11780), True);
      mn_EntToDrawAlong: SetStatusText(GetLangText(11782), True);
//-- Glukhov Bug#468 Build#162 16.08.01
      mn_RefBitmap: RefPtCnt := 0;
    end; // END of CASE statement

    if DoBreak then
      BreakInput(FALSE, TRUE);

    if AMenu in mnDeSelAll then
      DeSelectAll(TRUE);

         { Setzen der Statuszeile                                                 }
    case AMenu of
      mn_OffsetOK,
        mn_OffsetForSel: SetStatusText(Format(GetLangText(208), [InpOffset^.AktDist / 100]), TRUE);
      mn_GenerRef: SetStatusText(GetLangText(4017), TRUE);
      mn_GenerRect: SetStatusText(GetLangText(4018), FALSE);
      mn_MesOrtho,
        mn_MesCut: SetStatusText(GetLangText(928), TRUE);
      mn_LineCut:
        if TrimmOptions = to_ToArea then
          SetStatusText(GetLangText(11246), TRUE)
        else
          SetStatusText(GetLangText(10101), TRUE);
      mn_Combine: SetStatusText(GetLangText(10108), TRUE);
      mn_RTrafo: SetStatusText(GetLangText(10222), TRUE);
      mn_InsertOLEObject,
        mn_InsLinkObj,
        mn_PasteOLEObj: SetStatusText(GetLangText(11121), TRUE);
      mn_RefBitmap: SetStatusText(GetLangText(11123), TRUE);
      mn_OrthoMark,
        mn_CircleMark,
        mn_DistMark:
        begin
          SetStatusText(GetLangText(11203), TRUE);
          MarkState := 0;
        end;
      mn_LineMark:
        begin
          SetStatusText(GetLangText(11203), TRUE);
          MarkState := 0;
        end;
      mn_CircleCutMark:
        begin
          SetStatusText(GetLangText(11214), TRUE);
          MarkState := 0;
          NoSnap := FALSE;
        end;
      mn_AngleMark:
        begin
          SetStatusText(GetLangText(11217), TRUE);
          MarkState := 0;
        end;
      mn_RefPoints:
        begin
        end;
//++Fedor
      mn_CutImage:
        begin;
          CSelectForCut := 1;
          SetStatusText(GetLangText(11733), TRUE);
        end;
//--Fedor
      mn_SuperImage:
        begin
          case
            SuperImageOption of
            0: I := 10;
            1: I := 11;
            2: I := 12;
          else
            I := 9;
          end;
          CSelectForCut := 1;
          SetStatusText(GetLangText(I), TRUE);
        end;
      mn_GenArea:
        begin;
          SetStatusText(GetLangText(10200), TRUE);
        end;

    else
      ClearStatusText;
    end; // END of CASE statement

         { Setzen der Cursorform                                                          }
    case NewMenu of
      mn_Symbol: SetCursor(idc_Sym);
      mn_Text,
        mn_DBText: SetCursor(idc_Text);
      mn_SetRefPoint,
        mn_SetOmegaPoint, {++Sygsky: Omega }
        mn_Pixel: SetCursor(idc_Pixel);
      mn_PartFree,
        mn_PartParallel,
        mn_CParallel,
        mn_CBufferDraw,
        mn_Poly: SetCursor(idc_Poly);
      mn_EntToDrawAlong: SetCursor(idc_Poly); // Glukhov Bug#468 Build#162 16.08.01
      mn_DrawSymLine: SetCursor(idc_Poly);
      mn_CIslands,
        mn_CPoly: SetCursor(idc_CPoly);
      mn_CParallelS: SetCursor(crArrow);
      mn_MovePartPuff,
        mn_MovePartPara,
        mn_CMovePoint,
        mn_Snap: SetCursor(idc_Move);
      mn_CInsPoint,
        mn_InsPoint: SetCursor(idc_Ins);
      mn_CDelPoint,
        mn_DelPoint: SetCursor(idc_Del);
      mn_Offset,
        mn_OffsetOK: SetCursor(idc_Offset);
      mn_Distance: SetCursor(idc_Distance);
      mn_Move: SetCursor(idc_Move);
      mn_PseudoSelect: SetCursor(crArrow);
      mn_GenerRect: SetCursor(idc_Rect);
      mn_MesPoint: SetCursor(idc_MesPoint);
      mn_MesCut: SetCursor(idc_MesCut);
      mn_MesOrtho: SetCursor(idc_MesOrtho);
      mn_LineCut:
        if TrimmOptions = to_BothLines then
          SetCursor(idc_TrimmBoth)
        else
          SetCursor(idc_TrimmFirst); {0804F}
      mn_Combine:
        if CombineData.MakeAreas then
          SetCursor(idc_CombineAreas)
        else
          SetCursor(idc_CombineLines);
      mn_Select:
        begin
{++ Ivanoff BUG#635 BUILD#100}
{Set kind of cursor for symbol_editor-project.}
          if PProj(Layers.Parent).SymbolMode = sym_SymbolMode then
            SelectCursor := crArrow
          else
// {-- Ivanoff}
            if Layers^.TranspLayer then
              SelectCursor := idc_TranspSelect
            else
              SelectCursor := crArrow;
          SetCursor(SelectCursor);
        end;
      mn_DrawForSelect,
        mn_SelForSelect:
        case SelectData.SelectWith of
          swCircle: SetCursor(idc_Circle);
          swPoly: SetCursor(idc_SelPoly);
          swRect: SetCursor(idc_Neigh);
          swLine: SetCursor(idc_Poly);
          swOffset: SetCursor(idc_Offset);
          swPoint: SetCursor(idc_Neighbour);
          swPixel: SetCursor(idc_SelPoint);
        end;
      mn_DBSelCircle: SetCursor(idc_Circle);
      mn_DBSelRect: SetCursor(idc_Neigh);
      mn_SuperImage: //++Sygsky: 21-JAN-2001
        SetCursor(idc_Rect);

      mn_GenArea: SetCursor(idc_RefCircle);
    else
      SetCursor(crArrow);
    end; // END of CASE statement

    ActualMen := NewMenu;
    MenuFunctions[GetMenuFunctionName].Checked := TRUE;
{$IFNDEF WMLT}
    if ActualMen = mn_Select then
      MenuFunctions['EditSelect'].Checked := TRUE;
{$ENDIF}
  end;
end;

procedure TProj.MsgLeftClick
  (
  DrawPos: TDPoint
  );
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  LastClickPos.X := DrawPos.X;
  LastClickPos.Y := DrawPos.Y;
  if ClickCnt = 0 then
  begin
    if MsgLeftClick1(DrawPos) then
      Inc(ClickCnt);
  end
  else
    if MsgLeftClick2(DrawPos) then
      ClickCnt := 0;
{$IFNDEF WMLT}
  SendClickInfo(DrawPos.x, DrawPos.y, Integer(mbLeft), ActualMen, 0);
{$ENDIF}
{$ENDIF} // <----------------- AXDLL
end;

{**************************************************************************************}
{ Function TProj.SnapPoint                                                             }
{  Ist SnapState FALSE, so gibt SnapPoint TRUE zur�ck. Andernfalls sucht die Routine   }
{  einen Punkt in der N�he von DrawPos (durchsucht werden alle in ObjTypes angegebenen }
{  Objekttypen). Wird ein Punkt gefunden, so gibt SnapPoint TRUE zur�ck, sonst FALSE.  }
{  SnapPoint gibt entweder DrawPos zur�ck (SnapState=FALSE oder kein Punkt gefunden),  }
{  oder die Position des gefundenen Punktes.                                           }
{  In Exclude kann der Geo-Code eines Objektes �bergeben werden, das nicht behandelt   }
{  werden soll.                                                                        }
{                                                                                      }
{  Changed By Brovak - for selection with polygon. Added mouse's variables processing  }
{                          (now it only for left double-click)                         }
{**************************************************************************************}

function TProj.SnapPoint
  (
  DrawPos: TDPoint;
  Exclude: LongInt;
  const ObjType: TObjectTypes;
  var SnapPos: TDPoint;
  var SnaPObj: PIndex;
   {++Brovak}
  Shift: TShiftState = [];
  MButton: TMouseButton = mbMiddle
  )
   {-- Brovak}
: Boolean;

var
  SelectCircle: PEllipse;

  function DoAll
      (
      Item: PLayer
      )
      : Boolean; far;
  begin
    DoAll := FALSE;
    if Item <> nil then
      DoAll := Item^.SearchForPoint(PInfo, SelectCircle, Exclude, ObjType, SnapPos, SnaPObj);
  end;

  procedure DoAllNearest(Item: PLayer); far;

    function DoAllLines(AItem: PIndex): PIndex;
    var
      APos: TDPoint;
      AView: PView;
      PtIdx: Integer;
    begin
      Result := nil;
      if (AItem^.Visible(PInfo)) and (AItem^.GetObjType in [ot_Poly, ot_CPoly]) then
      begin
        AView := Item^.IndexObject(PInfo, AItem);
        if AView <> nil then
        begin
          APos.Init(DrawPos.x, DrawPos.y);
          Result := PPoly(AView)^.SearchForLine(PInfo, APos, [ot_Poly, ot_CPoly], SelectCircle^.GiveRadius, PtIdx);
          if Result <> nil then
          begin
            SnapPos.Init(APos.x, APos.y);
            SnapObj := AView;
          end;
        end;
      end;
    end;

  begin
    if Item^.UseForSnap then
    begin
      if Item^.SearchForNearestPoint(PInfo, DrawPos, SelectCircle, Exclude, ObjType, SnapPos, SnaPObj) then
      begin
        Result := TRUE;
      end;
      if GetKeyState(VK_SHIFT) < 0 then
      begin
        if Item^.Data^.FirstThat(@DoAllLines) <> nil then
        begin
          Result := True;
        end;
      end;
    end;
  end;
var
  ADistance: Double;
  APoint: TPoint;
//  AX, AY: Double;
begin
  {+++Brovak }
  if (MButton = mbLeft) and (ssDouble in Shift) then
  begin;
    Result := true;
    Exit;
  end;
  {--Brovak}

  Result := FALSE;
  SnapPos.Init(DrawPos.X, DrawPos.Y);
  if not (SnapState) then
    SnapPoint := TRUE
  else
  begin
    case PInfo^.VariousSettings.SnapRadiusType of
      0: ADistance := PInfo^.CalculateDraw(LimitToLong(PInfo^.VariousSettings.SnapRadius * 10));
      1:
        begin
          APoint := Point(LimitToLong(PInfo^.VariousSettings.SnapRadius)
            , LimitToLong(PInfo^.VariousSettings.SnapRadius));
          DPToLP(PInfo^.ExtCanvas.Handle, APoint, 1);
          ADistance := PInfo^.CalculateDraw(APoint.X);
        end;
    else
      ADistance := PInfo^.VariousSettings.SnapRadius * 100.0;
    end;
    SelectCircle := New(PEllipse, Init(DrawPos, Trunc(ADistance), Trunc(ADistance), 0));
    SnapPos.Init(maxLongInt, maxLongInt);
    Layers^.LData^.ForEach(@DoAllNearest);
    Dispose(SelectCircle, Done);

    if not Result then
    begin
      SnapPos.Init(DrawPos.X, DrawPos.Y);
      SnapObj := nil;
      Result := not AllowOnlySnappedPoints;
    end;
  end;
end;

{************************************************************************************}
{ Function MsgLeftClick1                                                             }
{  Wird aufgerufen, wenn die linke Maustaste gedr�ckt wird. Gibt MsgLeftClick1 TRUE  }
{  zur�ck, so wird beim n�chsten Mausclick die Routine MsgLeftClick2 aufgerufen,     }
{  ansonsten wieder MsgLeftClick1.                                                   }
{************************************************************************************}

function TProj.MsgLeftClick1
  (
  DrawPos: TDPoint
  )
  : Boolean;
{$IFNDEF AXDLL} // <----------------- AXDLL
var
  Item: PIndex;
  Border: PCPoly;
  Point1: TDPoint;
  Point2: TDPoint;
  Point3: TDPoint;
  Found1: Boolean;
  Found2: Boolean;
  CoData: TCoordData;
  AOffset: LongInt;
  CBorder: PEllipse;
  CPos: TDPoint;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
//  CRad         : LongInt;
  CRadX: LongInt;
  CRadY: LongInt;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
  ALayer: PLayer;
  SnapObject: PIndex;
  APoint: PPixel;
  SelectCircle: PEllipse;
  ASymbol: PSymbol;
  NewPoint: PPixel;
  NewPoint1: POmegaPixel; {++Sygsky: Omega}
  DispPos: TPoint;
  NDist: Double;
  Dummy: AnsiString;
  InsPoly: PPoly;
  AView: PView;
{++brovak #MOD for copying part of selected Image}
  NewPicFile: AnsiString;
  CutDRect: TDRect;
  ICount: integer;
{--brovak}
  SIPtr: PSIStruct;
{++ Ivanoff OrthoDraw}
  BPos: TDPoint;
  ABaseStartPoint, ABaseEndPoint: PDPoint;
{-- Ivanoff}
  I: Integer;
  OldSelMode: TSelectionMode;
  OldTransp: Boolean;
  AObjType: TObjectTypes;
{$ENDIF} // <----------------- AXDLL
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  MsgLeftClick1 := FALSE;
  SnapObject := nil;
  if ActualMen <= 0 then
    SetActualMenu(mn_Select);
  case ActualMen of
{$IFNDEF WMLT}
    mn_ScalePolyline:
      begin
        if SnapPoint(DrawPos, 0, ot_Any, Point1, SnapObject) then
        begin
          ScalePolyPos1.X := Point1.X;
          ScalePolyPos1.Y := Point1.Y;
        end;
        Result := True;
        ClearStatusText;
        SetStatusText(GetLangText(11751), True);
      end;
    mn_Select:
      begin
        if not (Layers^.TopLayer^.GetState(sf_Fixed)) then
        begin
          SnapItem := Layers^.SelLayer^.SearchByPoint(PInfo, DrawPos, [ot_Text, ot_Symbol, ot_RText, ot_BusGraph]);
          if (SnapItem <> nil) and (not IsCanvas) then
          begin
            if SnapItem^.GetObjType = ot_Text then
            begin
              ActualMen := mn_MovePoly;
              Border := PText(SnapItem)^.GetBorder;
              MovePoly^.SetPoly(Border);
              Dispose(Border, Done);
              MovePoly^.StartIt(PInfo, DrawPos, FALSE);
            end
            else
              if SnapItem^.GetObjType = ot_RText then
              begin
                ActualMen := mn_MovePoly;
                Border := PRText(SnapItem)^.GetBorder(PInfo);
                MovePoly^.SetPoly(Border);
                Dispose(Border, Done);
                MovePoly^.StartIt(PInfo, DrawPos, FALSE);
              end
              else
                if SnapItem^.GetObjType = ot_Symbol then
                begin
                  ActualMen := mn_MoveSym;
                  Border := New(PCPoly, Init);
                  PSymbol(SnapItem)^.GetBorder(PInfo, Border);
                  MovePoly^.SetPoly(Border);
                  Dispose(Border, Done);
                  MovePoly^.StartIt(PInfo, DrawPos, FALSE);
                end
                else
                  if SnapItem^.GetObjType = ot_BusGraph then
                  begin
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
                    with PBusGraph(SnapItem)^ do
                    begin
                      Point1.x := ClipRect.A.x;
                      Point1.y := ClipRect.A.y;
                      Point2.x := ClipRect.B.x;
                      Point2.y := ClipRect.B.y;
                    end;
                    ActualMen := mn_MoveCircle;

                    CPos.Init(Round((Point1.X + Point2.X) * 0.5), Round((Point1.Y + Point2.Y) * 0.5));
                    CRadX := Round(Abs(Point2.X - Point1.X) * 0.5);
                    CRadY := Round(Abs(Point2.Y - Point1.Y) * 0.5);

                    CBorder := New(PEllipse, Init(CPos, CRadX, CRadY, 0));
                    MoveCircle^.SetCircle(CBorder);
                    Dispose(CBorder, Done);
                    CPos.Done;
                    ActualMen := mn_MovePoly;
                    Border := New(PCPoly, Init);
                    Border^.InsertPoint(Point1);
                    CPos.Init(Point1.X, Point2.Y);
                    Border^.InsertPoint(CPos);
                    Border^.InsertPoint(Point2);
                    CPos.Init(Point2.X, Point1.Y);
                    Border^.InsertPoint(CPos);
                    Border^.ClosePoly;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
                    MovePoly^.SetPoly(Border);
                    Dispose(Border, Done);
                    MovePoly^.StartIt(PInfo, DrawPos, FALSE);
                  end;
          end
          else
            SelRect^.StartIt(PInfo, DrawPos, FALSE);
        end
        else
          SelRect^.StartIt(PInfo, DrawPos, FALSE);
      end;

    mn_SelPoly:
      begin
        SnapObject := SelectByPoint(DrawPos, [ot_Poly, ot_CPoly]);
        if SnapObject <> nil then
        begin
          DeselectAll(False);
          ProcReplacePolyPoints(@Self, SnapObject);
        end;
      end;
    mn_GenArea:
      begin
        if Layers^.TopLayer^.BkSearchByPoint(PInfo, DrawPos, ot_Any) = nil then
        begin
          DoGenerateArea(DrawPos);
        end;
      end;
    mn_Text:
      begin;
        TextRect^.SetNewText(PInfo, @ActualFont, ActualText);
        TextRect^.StartIt(PInfo, DrawPos, FALSE);
               {+++brovak BUG#80 BUILD 127}
        if InputTextFlag = true then
        begin;
          if Statustext.Count > 0 then
            ClearStatusText;
          InputTextFlag := false;
        end;
                {--brovak}
        SetStatusText(Format(GetLangText(207), [Abs(TextRect^.Height / 100), TextRect^.GetDisplayAngle]), TRUE);
        MsgLeftClick1 := TRUE;
      end;

    mn_DBText:
      begin
        TextRect^.SetNewText(PInfo, @ActualFont, ActualText);
        TextRect^.StartIt(PInfo, DrawPos, FALSE);
        SetStatusText(Format(GetLangText(207), [Abs(TextRect^.Height / 100), TextRect^.GetDisplayAngle]), TRUE);
        MsgLeftClick1 := TRUE;
      end;
    mn_SetRefPoint:
      if SnapPoint(DrawPos, 0, ot_Any, Point1, SnapObject) then
      begin
        APoint := New(PPixel, Init(Point1));
        InsertObjectWithStyle(APoint, TSymEditor(SymEditInfo.SymEditor).FRefLayer,
          TSymEditor(SymEditInfo.SymEditor).RefStyleData);
        TSymEditor(SymEditInfo.SymEditor).SetRefPoint(APoint);
      end;
    mn_Pixel:
      if SnapPoint(DrawPos, 0, ot_Any, Point1, SnapObject) then
      begin
        NewPoint := New(PPixel, Init(Point1));
        InsertObject(NewPoint, FALSE);
        if (DDEHandler.DoAutoIns) {and (DDEHandler.FDBAutoIn)} then
          DBEditAuto(PIndex(NewPoint));
      end;
    mn_SetOmegaPoint: {++Sygsky: Omega add-in}
      if SnapPoint(DrawPos, 0, ot_Any, Point1, SnapObject) then
      begin
        NewPoint1 := New(POmegaPixel, Init(Point1));
        DeSelectAll(False);
        InsertObject(NewPoint1, TRUE);
      end;
    mn_Symbol:
      if SnapPoint(DrawPos, 0, ot_Any, Point2, SnapObject) then
      begin
        ASymbol := New(PSymbol, InitName(PInfo, Point2, ActualSym^.GetName, ActualSym^.GetLibName, ActSymSize, ActSymSizeType, ActSymAngle));
        if ASymbol <> nil then
        begin
          InsertObject(ASymbol, FALSE);
          RefreshSymbolMan(False);
          if GetKeyState(VK_CONTROL) < 0 then
          begin
            ProcRotateSymbol(@Self, ASymbol);
            PInfo^.RedrawInvalidate;
          end;
          if (DDEHandler.DoAutoIns) then
          begin
            DBEditAuto(PIndex(ASymbol));
          end;
        end;
      end;
    mn_CParallel:
      begin
        if NewPoly^.Data^.Count < 2 then
        begin
          if SnapPoint(DrawPos, 0, ot_Any, Point1, SnapObject) then
          begin
            NewPoly^.PointInsert(PInfo, Point1, TRUE);
            NewPoly^.LineOn := True;
            NewPoly^.LastIns := True;
            if NewPoly^.Data^.Count = 1 then
              SetStatusText(GetLangText(11379), FALSE);
            if NewPoly^.Data^.Count = 2 then
              SetStatusText(GetLangText(11380), FALSE);
          end
          else
            NewPoly^.LastIns := False;
        end
        else
        begin
          NewPoly^.PointInsert(PInfo, Point1, TRUE);
          InsPoly := New(PPoly, Init);
          if not NewPoly^.CheckCopyData(PInfo, InsPoly, TRUE) then
            Dispose(InsPoly, Done)
          else
            InsertObject(InsPoly, FALSE);
          ClearStatusText;
          PostMessage(Parent.Handle, wm_Command, cm_DoParallel, 0);
        end;
      end;
    mn_PartParallel:
      begin
        if NewPoly^.Data^.Count < 1 then
        begin
          if SnapPoint(DrawPos, 0, ot_Any, Point1, SnapObject) then
          begin
            if PolyPart.CheckPoint(NewPoly, Point1, TRUE) then
            begin
              NewPoly^.PointInsert(PInfo, Point1, FALSE);
              NewPoly^.LineOn := TRUE;
              NewPoly^.LastIns := TRUE;
              if NewPoly^.Data^.Count = 1 then
                SetStatusText(GetLangText(11751), FALSE);
            end
            else
              NewPoly^.LastIns := FALSE;
          end
          else
            NewPoly^.LastIns := FALSE;
        end
        else
        begin
          if SnapPoint(DrawPos, 0, ot_Any, Point1, SnapObject) then
          begin
            if PolyPart.CheckPoint(NewPoly, Point1, FALSE) then
            begin
              PolyPart.CreatePartLine(NewPoly, Point1);
              SetStatusText(GetLangText(11752), FALSE);
            end
            else
              NewPoly^.LastIns := FALSE;
          end
          else
            NewPoly^.LastIns := FALSE;
        end;
      end;
    mn_MovePartPara:
      begin
{        MovePoly^.SetPoly(NewPoly);
        PolyPart.SetPartLine(NewPoly);
        MovePoly^.StartIt(PInfo, DrawPos, FALSE);
        PolyPart.CalcStartMovePoly;}
      end;
    mn_MovePartPuff:
      begin

      end;
    mn_PartFree:
      begin
        if NewPoly^.Data^.Count < 1 then
        begin
          if SnapPoint(DrawPos, 0, ot_Any, Point1, SnapObject) then
          begin
            NewPoly^.PointInsert(PInfo, Point1, FALSE);
            NewPoly^.LineOn := TRUE;
            NewPoly^.LastIns := TRUE;
            if NewPoly^.Data^.Count = 1 then
              SetStatusText(GetLangText(11760), FALSE);
          end
          else
            NewPoly^.LastIns := FALSE;
        end
        else
          if NewPoly^.Data^.Count > 0 then
          begin
            if SnapPoint(DrawPos, 0, ot_Any, Point1, SnapObject) then
            begin
              NewPoly^.PointInsert(PInfo, Point1, FALSE);
              NewPoly^.LineOn := TRUE;
              NewPoly^.LastIns := TRUE;
              if NewPoly^.Data^.Count > 1 then
                SetStatusText(GetLangText(11761), FALSE);
            end
            else
              NewPoly^.LastIns := FALSE;
                       {end
                       else begin
                         if SnapPoint(DrawPos,0,ot_Any,Point1,SnapObject) then begin
                           if PolyPart.CheckPoint(NewPoly,Point1,FALSE) then begin
                             PolyPart.CreatePartLine(NewPoly,Point1);
                             SetStatusText(GetLangText(11752),FALSE);
                           end
                           else NewPoly^.LastIns:=FALSE;
                         end
                         else NewPoly^.LastIns:=FALSE;}
          end;
      end;
    mn_InfoPoly:
      begin
                       {InfoPoly^.EndIt(PInfo);}
        SetActualMenu(mn_Select);
        SendMessage(Parent.Handle, wm_Command, cm_ShowPartDlg, 0);
      end;
    mn_CParallelS:
      begin
        ClearStatusText;
        Construct^.SidePoint.Init(DrawPos.X, DrawPos.Y);
        PostMessage(Parent.Handle, wm_Command, cm_DoInParallel, 0);
      end;
{++ Ivanoff NA2 BUILD#106}
(*      mn_CBufferDraw,
      mn_CIslands,
      mn_DrawSymLine,
      mn_CPoly,
      mn_Poly : Begin
                  if SnapPoint(DrawPos,0,ot_Any,Point1,SnapObject) then begin
                    NewPoly^.PointInsert(PInfo,Point1,TRUE);
                    NewPoly^.LineOn := True;
                    NewPoly^.LastIns := True;  {letzter Punkt wurde eingef�gt}
                  end else NewPoly^.LastIns:=False;
                End;*)
    mn_CBufferDraw,
      mn_CIslands,
      mn_DrawSymLine:
      begin
        if SnapPoint(DrawPos, 0, ot_Any, Point1, SnapObject) then
        begin
          NewPoly^.PointInsert(PInfo, Point1, TRUE);
          NewPoly^.LineOn := True;
          NewPoly^.LastIns := True; {letzter Punkt wurde eingef�gt}
        end
        else
          NewPoly^.LastIns := False;
      end;
    mn_CPoly,
      mn_EntToDrawAlong, // Glukhov Bug#468 Build#162 16.08.01
      mn_Poly:
      begin
        if (GetKeyState(vk_control) < 0) then
        begin
          AObjType := [ot_Poly, ot_CPoly];
          SnapPoint(DrawPos, 0, AObjType, Point1, SnapObject);
          if SnapObject = nil then
          begin
            AObjType := ot_Any;
          end;
        end
        else
        begin
          AObjType := ot_Any;
        end;
        SnapObject := nil;
        if SnapPoint(DrawPos, 0, AObjType, Point1, SnapObject) then
                   {+++Brovak. changed hardly for bug 104 build 132}
        begin

          if (GetKeyState($43) < 0) then
          begin
            ZoomByFactorWithCenter(Point1.X, Point1.Y, 1.0);
          end;

          if (ActualMen = mn_CPoly)
            and ((Self.bDrawRubberBox = true) or (Self.bDraw3Points = true)) then
          begin;
            if not SnapState then
            begin

                           //correct pos, if user drawing with Shift
              if (GetKeyState(vk_shift) < 0) and (NewPoly.Data.Count > 0) then
                GetNewDrawPos(Point1);
                           //correct pos, if user drawing with Control
              if (GetKeyState(vk_control) < 0) and (NewPoly.Data.Count > 1) then
                GetNewDrawPos1(Point1);
            end;

            if Self.bDrawRubberBox = true then //Draw with RubberBox (2 points)
            begin;
              if SelRect^.IsOn then
                RubberBoxCalc(@SELF, Point1, true, 1)
              else
                RubberBoxCalc(@SELF, Point1, true, 0)
            end
            else //Draw with 3 points
              ThreePointsCalc(@SELF, Point1, true, NewPoly^.Data.Count);
          end
          else
          begin; //traditional drawing or orthogonal
            if (not SELF.NewPoly.bDrawOrtho) or (NewPoly.Data.Count < 2) then
            begin
              if not SnapState then
              begin
                if (GetKeyState(vk_shift) < 0) and (NewPoly.Data.Count > 0) then
                  GetNewDrawPos(Point1);
                if (GetKeyState(vk_control) < 0) and (NewPoly.Data.Count > 1) then
                  GetNewDrawPos1(Point1);
              end;

              NewPoly^.MouseMove(PInfo, Point1);
              NewPoly^.PointInsert(PInfo, Point1, TRUE);
              NewPoly^.Save(SnapObject);
              NewPoly^.Closed := (ActualMen = mn_CPoly);
              NewPoly^.MouseMove(PInfo, DrawPos);
              NewPoly^.LineOn := True;

              if (SnapObject <> nil) and (SnapObject^.GetObjType in [ot_Poly, ot_CPoly]) then
              begin
                CurSnapPoly := PPoly(PLayer(PInfo^.Objects)^.IndexObject(PInfo, SnapObject));
                if CurSnapPoly <> nil then
                begin
                  CurSnapPoly^.SnapStart := CurSnapPoly^.FindIndexOfPoint(Point1);
                  NewPoly^.LastSnapPos.Init(Point1.X, Point1.Y);
                  CurSnapPoly^.SnapEnd := -1;
                end;
              end;

            end
            else
            begin
                         {++ Ivanoff OrthoDraw}
              ABaseStartPoint := SELF.NewPoly.Data.At(SELF.NewPoly.Data.Count - 2);
              ABaseEndPoint := SELF.NewPoly.Data.At(SELF.NewPoly.Data.Count - 1);
              SELF.PInfo.NormalDistance(ABaseStartPoint, ABaseEndPoint, @DrawPos, NDist, BPos);
              SELF.NewPoly.DeleteLastPoint(SELF.PInfo);
              SELF.NewPoly.PointInsert(SELF.PInfo, BPos, TRUE);
              SELF.NewPoly.PointInsert(SELF.PInfo, DrawPos, TRUE);
              NewPoly.ClipRect.CorrectByPoint(DrawPos);
              NewPoly.Invalidate(PInfo);
                         {-- Ivanoff}
            end;
            NewPoly^.LastIns := True; {letzter Punkt wurde eingef�gt}
          end;
        end //snappoint
                     {---brovak}
      end;
{-- Ivanoff}
{$ENDIF}
    mn_Distance:
      begin
        if SnapPoint(DrawPos, 0, ot_Any, Point2, SnapObject) then
        begin
          AOffset := NewPoly^.Data^.Count;
          NewPoly^.PointInsert(PInfo, Point2, TRUE);
          //if AOffset = NewPoly^.Data^.Count then Exit;
          if PInfo^.CoordinateSystem.CoordinateType = ctGeodatic then
{++ Ivanoff BUG#378}
{            SetStatusText(Format(GetLangText(5102), [NewPoly^.GetYDist / 100,
              NewPoly^.GetXDist / 100, NewPoly^.GetDistance / 100]), NewPoly^.Data^.Count = 1)}
            SetStatusText(Format(GetLangText(5102), [NewPoly^.GetYDist / 100, NewPoly^.GetXDist / 100, NewPoly^.GetDistance / 100, PInfo^.ConvAngle(NewPoly.GetAngle, True)]), NewPoly^.Data^.Count = 1)
{-- Ivanoff BUG#378}
          else
{++ Ivanoff BUG#378}
{            SetStatusText(Format(GetLangText(5102), [NewPoly^.GetXDist / 100,
              NewPoly^.GetYDist / 100, NewPoly^.GetDistance / 100]), NewPoly^.Data^.Count = 1);}
            SetStatusText(Format(GetLangText(5102), [NewPoly^.GetXDist / 100, NewPoly^.GetYDist / 100, NewPoly^.GetDistance / 100, NewPoly.GetAngle]), NewPoly^.Data^.Count = 1);
{-- Ivanoff BUG#378}
          LastDistance := NewPoly^.GetDistance / 100;
          LastLineDist := NewPoly^.GetLastDist(PInfo) / 100;
        end;
      end; {aaaa}
{$IFNDEF WMLT}
    mn_Offset:
      begin
        OffsetItem := PPoly(SelectByPoint(DrawPos, [ot_Poly, ot_CPoly]));
        if OffsetItem <> nil then
        begin
          InpOffset^.SetPoly(PPoly(OffsetItem));
          InpOffset^.StartIt(PInfo, DrawPos, FALSE);
          InpOffset^.MouseMove(PInfo, DrawPos); {1004F}
          SetActualMenu(mn_OffsetOK);
        end;
      end;
    mn_OffsetOK:
      begin
        BreakInput(TRUE, FALSE);
        InpOffset^.GetOffset(AOffset);
        ProcCreateOffset(@Self, AOffset);
      end;
    mn_SplitLine:
      begin
        InpOffset^.EndIt(PInfo);
        BreakInput(TRUE, False);
        if (SplitL1 > 0) and (SplitL2 > 0) then
        begin
          if PInfo^.AnnotSettings.ShowDialog then
          begin
            ALayer := SelectLayerDialog(@Self, [sloNewLayer], 2020);
          end
          else
          begin
            ALayer := Layers^.TopLayer;
          end;
          if ALayer <> nil then
            SplitLine(InpOffset^.Poly^.Index, ALayer);
        end;
        SetActualMenu(mn_Select);
      end;
    mn_Move:
      begin
        if SnapPoint(DrawPos, 0, ot_Any, Point1, SnapObject) then
        begin
          Line^.StartIt(PInfo, Point1, TRUE);
          MsgLeftClick1 := TRUE;
        end;
      end;
    mn_Snap:
      begin
       {brovak 193}
        EscPressedTwo := false;
        if OldSnapItem = nil then
          FindOldSnapItem;
        {brovak}
        Pinfo.EditPoly := false;
        SnapItem := SelectByPoint(DrawPos, [ot_Pixel]);
        if SnapItem <> nil then
        begin
        //bro
         // Self.Layers.TopLayer.Select(Pinfo,SnapItem);
       //  Pinfo.EditPoly:=false;
          Line^.StartIt(PInfo, PPixel(SnapItem)^.Position, TRUE);
          SetCursor(idc_SnapWin);
          MsgLeftClick1 := TRUE;
        end
        else
        begin
          PInfo^.EditPoly := TRUE;
          if OldsnapItem <> nil then
            SnapItem := OldsnapItem.MPtr
          else
          begin
            if Layers^.TranspLayer then
            begin
              for i := 1 to Layers^.LData^.Count - 1 do
              begin
                SnapItem := PLayer(Layers^.LData^.At(i))^.SelectByPoint(PInfo, DrawPos, [ot_Poly, ot_CPoly]);
                if SnapItem <> nil then
                  break;
              end;
            end
            else
              SnapItem := SelectByPoint(DrawPos, [ot_Poly, ot_CPoly]);
          end;
          if SnapItem <> nil then
            OldSnapItem := GiveMeSourceObjectFromSelect(Layers^.SelLayer.Data.At(0));

          if SnapItem <> nil then
          begin
            if Layers^.IsObjectFixed(SnapItem) then
            begin {3003F}
              MsgBox(Parent.Handle, 10147, mb_OK, '');
              DeselectAll(FALSE);
              OldSnapItem := nil;
            end
            else
            begin
              OldSnapItem := nil;
              ActualMen := mn_SnapPoly;
              PointInd := -1;
            end; {3003F}
          end;
        end;
      end;
      {Brovak}
    mn_PseudoSelect:
      begin;
        Pinfo.EditPoly := false;
        if (Layers^.SelLayer^.Data^.GetCount <> 0) then
        begin;
          Item := Layers^.TopLayer^.HasObject(SnapItem);
          if Item <> nil then
            Layers^.TopLayer^.DeSelect(PInfo, Item);
        end;
        SnapItem := SelectByPoint(DrawPos, [ot_Poly, ot_Circle, ot_arc, ot_text, ot_symbol, ot_Pixel, ot_CPoly]);
        if SnapItem <> nil then
        begin;
          OldSnapItem := GiveMeSourceObjectFromSelect(Layers^.SelLayer.Data.At(0));

        end
        else
        begin;
          if OldsnapItem <> nil then
            SnapItem := OldsnapItem.MPtr
          else
          begin
            if Layers^.TranspLayer then
            begin
              for i := 1 to Layers^.LData^.Count - 1 do
              begin
                SnapItem := PLayer(Layers^.LData^.At(i))^.SelectByPoint(PInfo, DrawPos, [ot_Poly, ot_Circle, ot_arc, ot_text, ot_symbol, ot_Pixel, ot_CPoly]);
                if SnapItem <> nil then
                  break;
              end;
            end
            else
              SnapItem := SelectByPoint(DrawPos, [ot_Poly, ot_Circle, ot_arc, ot_text, ot_symbol, ot_Pixel, ot_CPoly]);
          end;
          if SnapItem <> nil then
            OldSnapItem := GiveMeSourceObjectFromSelect(Layers^.SelLayer.Data.At(0));
        end;

      end;
   {--brovak}
    mn_InsPoint:
      begin

        EscPressedTwo := false;
        if OldSnapItem = nil then
          FindOLdSnapItem;
        PInfo^.EditPoly := TRUE;
        if OldsnapItem <> nil then
          SnapItem := OLdSnapItem.Mptr
        else
          if Layers^.TranspLayer then
          begin
            for i := 1 to Layers^.LData^.Count - 1 do
            begin
              SnapItem := PLayer(Layers^.LData^.At(i))^.SelectByPoint(PInfo, DrawPos, [ot_Poly, ot_CPoly]);
              if SnapItem <> nil then
                break;
            end;
          end
          else
            SnapItem := SelectByPoint(DrawPos, [ot_Poly, ot_CPoly]);
        if SnapItem <> nil then
          OldSnapItem := GiveMeSourceObjectFromSelect(Layers^.SelLayer.Data.At(0));
        if SnapItem <> nil then
        begin
          if Layers^.IsObjectFixed(SnapItem) then
          begin
            MsgBox(Parent.Handle, 10147, mb_IconExclamation + mb_OK, '');
            DeselectAll(FALSE);
            OldSnapItem := nil;
          end
          else
          begin
            ActualMen := mn_InsPOK;
            PointInd := -1;
            OldSnapItem := nil;
          end;
        end; {3003F}
      end;
    mn_DelPoint:
      begin

        EscPressedTwo := false;
        if OldSnapItem = nil then
          FindOldSnapItem;
        PInfo^.EditPoly := TRUE;
        if OldsnapItem <> nil then
          SnapItem := OldSnapItem.Mptr
        else
          if Layers^.TranspLayer then
          begin
            for i := 1 to Layers^.LData^.Count - 1 do
            begin
              SnapItem := PLayer(Layers^.LData^.At(i))^.SelectByPoint(PInfo, DrawPos, [ot_Poly, ot_CPoly]);
              if SnapItem <> nil then
                break;
            end;
          end
          else
            SnapItem := SelectByPoint(DrawPos, [ot_Poly, ot_CPoly]);
        if SnapItem <> nil then
          OldSnapItem := GiveMeSourceObjectFromSelect(Layers^.SelLayer.Data.At(0));
        if SnapItem <> nil then
        begin
          if Layers^.IsObjectFixed(SnapItem) then
          begin {3003F}
            MsgBox(Parent.Handle, 10147, mb_IconExclamation or mb_OK, '');
            DeselectAll(FALSE);
          end
          else
          begin
            OldSnapItem := nil;
            ActualMen := mn_DelPOk;
            PointInd := -1;
          end; {3003F}
        end;
      end;
    mn_DelPOK:
      begin

        case PInfo^.VariousSettings.SnapRadiusType of
          0: NDist := PInfo^.CalculateDraw(LimitToLong(PInfo^.VariousSettings.SnapRadius * 10));
          1:
            begin
              DispPos := Point(LimitToLong(PInfo^.VariousSettings.SnapRadius),
                LimitToLong(PInfo^.VariousSettings.SnapRadius));
              DPToLP(PInfo^.ExtCanvas.Handle, DispPos, 1);
              NDist := PInfo^.CalculateDraw(DispPos.X);
            end;
        else
          NDist := PInfo^.VariousSettings.SnapRadius * 100.0;
        end;
        SelectCircle := New(PEllipse, Init(DrawPos, Round(NDist), Round(NDist), 0));
        Point1.Init(maxLongInt, maxLongInt);
        EscPressedTwo := false;

        if (Layers^.SelLayer^.Data^.GetCount <> 0)
          and PPoly(SnapItem)^.SearchNearestPoint(SelectCircle, DrawPos, Point1, PointInd) then
        begin
          Layers^.SelLayer^.SelInfo.Delete(PView(SnapItem), nil, nil);
{++ IDB_UNDO}// A point is deleted from any object (polyline or polygon).
          if (WinGisMainForm.WeAreUsingUndoFunction[@SELF])
            and
            (WinGisMainForm.UndoRedo_Man.AddObjectInUndoData(@SELF, PView(SnapItem))) then
            WinGisMainForm.UndoRedo_Man.SaveUndoData(@SELF, utUnChange);
{-- IDB_UNDO}
      //    PPoly(SnapItem)^.InvalidateTriple(PInfo, PointInd);
          CutDRect.InitByRect(SnapItem.ClipRect);
          if (GetKeyState(vk_shift) < 0) or (EditNeighboursMode) then //if Shift pressed
                  // for All layers
          begin;
            for ICount := 1 to Layers.Ldata.Count - 1 do
            begin
              if (not PLayer(Layers.LData.At(ICount)).GetState(sf_Fixed)) and
                (not PLayer(Layers.LData.At(ICount)).GetState(sf_LayerOff)) then
              begin

                PLayer(Layers.LData.At(ICount)).DeletePointForAllNeigboursPoly(Pinfo, Point1);
              end;
            end;
            Layers^.SelLayer^.SelInfo.Add(PView(SnapItem));
          end
          else
          begin;
            if PPoly(SnapItem)^.DeletePointByIndex(PointInd) then
            begin
              Layers^.SelLayer^.SelInfo.Add(PView(SnapItem));
              PInfo^.RedrawInvalidate;
            end
            else
            begin
              Layers^.SelLayer^.SelInfo.Add(PView(SnapItem));
              PPoly(SnapItem)^.ValidateTriple(PInfo, PointInd);
            end;
          end;
        //  UpdateClipRect(PPoly(SnapItem));
          RedrawWithAllAttached(@Self, PView(SnapItem), CutDRect);
          ProcDBSendObject(@Self, SnapItem, 'UPD');
          SetModified;
        end

        else
        begin
          DeSelectAll(TRUE);
          SnapItem := SelectByPoint(DrawPos, [ot_Poly, ot_CPoly]);
          if SnapItem = nil then
          begin;
            ActualMen := mn_DelPoint;
            OldSnapItem := nil;
             {+++Brovak - For tranparent}

            if Layers^.TranspLayer then
            begin;
              Dispose(SelectCircle, Done);
              MsgLeftClick1(DrawPos);
              Exit;
            end;
            {---Brovak}
          end
          else
          begin;
            if CheckFixedOrNot = true then
              Exit;
            OldSnapItem := GiveMeSourceObjectFromSelect(Layers^.SelLayer.Data.At(0));
          end;
        end;
        PInfo^.RedrawScreen(False);
        Dispose(SelectCircle, Done);
      end;
    mn_SnapPoly,
      mn_InsPOK:
      begin
//Pinfo.EditPoly:=true;
        case PInfo^.VariousSettings.SnapRadiusType of
          0: NDist := PInfo^.CalculateDraw(LimitToLong(PInfo^.VariousSettings.SnapRadius * 10));
          1:
            begin
              DispPos := Point(LimitToLong(PInfo^.VariousSettings.SnapRadius),
                LimitToLong(PInfo^.VariousSettings.SnapRadius));
              DPToLP(PInfo^.ExtCanvas.Handle, DispPos, 1);
              NDist := PInfo^.CalculateDraw(DispPos.X);
            end;
        else
          NDist := PInfo^.VariousSettings.SnapRadius * 100.0;
        end;
        SelectCircle := New(PEllipse, Init(DrawPos, Round(NDist), Round(NDist), 0));
        Point3.Init(maxLongInt, maxLongInt);
        if (Layers^.SelLayer^.Data^.GetCount <> 0)
          and PPoly(SnapItem)^.SearchNearestPoint(SelectCircle, DrawPos, Point3, PointInd) then
        begin
          if not PPoly(SnapItem)^.GetPreviousPoint(PointInd, Point1) then
          begin
            Found1 := PPoly(SnapItem)^.GetNextPoint(PointInd, Point1);
            Found2 := FALSE;
          end
          else
            Found2 := PPoly(SnapItem)^.GetNextPoint(PointInd, Point2);
          Lines^.StartIt(PInfo, Point1, TRUE);
          EscPressedTwo := false;
          if ActualMen = mn_SnapPoly then
          begin;
            Lines^.SetEnd(PInfo, DrawPos, Found2, Point2);
            RealPoint.Init(Point3.X, Point3.Y); //bro
          end
          else
          begin;
            Lines^.SetEnd(PInfo, DrawPos, TRUE, Point3);
            RealPoint.Init(Point2.X, Point2.Y); //bro
            MesPoint1.Init(Point1.X, Point1.Y);
            MesPoint2.Init(Point3.X, Point3.Y);
          end;
          SetCursor(idc_SnapWin);
          MsgLeftClick1 := TRUE;
        end
        else
        begin
          DeSelectAll(TRUE);
          SnapItem := SelectByPoint(DrawPos, [ot_Poly, ot_CPoly]);
          if SnapItem <> nil then
          begin;
            if CheckFixedOrNot = true then
              Exit;
            PointInd := -1;
            OldSnapItem := GiveMeSourceObjectFromSelect(Layers^.SelLayer.Data.At(0));
          end

          else
          begin
            OldSnapItem := nil;
            if ActualMen = mn_SnapPoly then
              ActualMen := mn_Snap
            else
              ActualMen := mn_InsPoint;
           {+++Brovak - For tranparent}

            if Layers^.TranspLayer then
            begin;
              Dispose(SelectCircle, Done);
              MsgLeftClick1(DrawPos);
              Exit;
            end;
            {---Brovak}
          end;
        end;
        Dispose(SelectCircle, Done);
      end;
    mn_SizeRot:
      begin
        TextRect^.EndIt(PInfo);
        SnapItem^.Invalidate(PInfo);
        TextRect^.GetText(PInfo, PText(SnapItem));
        SnapItem^.Invalidate(PInfo);
        CorrectSize(PView(SnapItem)^.ClipRect, TRUE);
        UpdateCliprect(PView(SnapItem));
        SelAllRect.CorrectByRect(PView(SnapItem)^.ClipRect);
        SetModified;
        ClearStatusText;
        SetCursor(SelectCursor);
        ActualMen := mn_Select;
      end;
    mn_SymbolInp:
      begin
        SymbolInp^.EndIt(PInfo);
        SnapItem^.Invalidate(PInfo);
        SymbolInp^.GetSymbol(PInfo, PSymbol(SnapItem));
        SnapItem^.Invalidate(PInfo);
        UpdateClipRect(PSymbol(SnapItem));
        SelAllRect.CorrectByRect(PView(SnapItem)^.ClipRect);
        CorrectSize(PView(SnapItem)^.ClipRect, TRUE);
        PInfo^.RedrawScreen(False);
        SetModified;
        ClearStatusText;
        SetCursor(SelectCursor);
        ActualMen := mn_Select;
      end;
    mn_RefPoints:
      if InpRef then
      begin
        if SnapPoint(DrawPos, 0, ot_Any, Point1, SnapObject) then
        begin
          with Point1 do
          begin
            CoData.XPos := X;
            CoData.YPos := Y;
          end;
          MsgCoordInput(@CoData);
        end;
      end;
    mn_GenerRef:
      begin
        if SnapPoint(DrawPos, 0, ot_Any, Point1, SnapObject) then
        begin
          with Point1 do
          begin
            CoData.XPos := X;
            CoData.YPos := Y;
          end;
          MsgCoordInput(@CoData);
        end;
      end;
    mn_GenerRect:
      begin
        if SnapPoint(DrawPos, 0, ot_Any, Point1, SnapObject) then
        begin
          SelRect^.StartIt(PInfo, Point1, FALSE);
          MsgLeftClick1 := TRUE;
        end;
      end;
    mn_SplineEdit: EditSpline^.MouseClick(PInfo, DrawPos);
    mn_EditImage,
//++ Glukhov Bug#264 Build#161 15.06.01
//    mn_EditTxtRect, // Glukhov Bug#170 BUILD#125 03.11.00
//-- Glukhov Bug#264 Build#161 15.06.01
    mn_EditOLE: InputBox^.MouseClick(PInfo, DrawPos);
//++ Glukhov Bug#264 Build#161 15.06.01
    mn_EditTxtRect: EditTxtBox^.MouseClick(PInfo, DrawPos);
//-- Glukhov Bug#264 Build#161 15.06.01
    mn_MesPoint:
      if SnapPoint(DrawPos, 0, [ot_Poly, ot_Pixel, ot_CPoly, ot_Symbol], MesPoint1, SnapObject) then
      begin
        Line^.StartIt(PInfo, MesPoint1, FALSE);
        MsgLeftClick1 := TRUE;
      end;
    mn_MesCut:
      if MesCutState = 0 then
      begin
        if SnapPoint(DrawPos, 0, [ot_Poly, ot_CPoly], MesPoint1, SnapObject) then
        begin
          MesCutState := 1;
          SetStatusText(GetLangText(929), FALSE);
        end;
      end
      else
        if MesCutState = 1 then
        begin
          if SnapPoint(DrawPos, 0, [ot_Poly, ot_CPoly], MesPoint2, SnapObject) then
          begin
            MesCutState := 2;
            SetStatusText(GetLangText(930), FALSE);
          end;
        end
        else
          if MesCutState = 2 then
          begin
            SnapItem := SearchByPointSnapLayers(DrawPos, [ot_Poly, ot_CPoly]);
            if SnapItem <> nil then
            begin
              if PPoly(SnapItem)^.GetCutPoint(PInfo, MesPoint1, MesPoint2, Point1) then
              begin
                if Point1.Dist(MesPoint2) < Point1.Dist(MesPoint1) then
                  MesPoint1 := MesPoint2;
                MesPoint2 := Point1;
                ProcInsertMeasure(@Self, DrawPos);
                BreakInput(TRUE, FALSE);
              end;
            end;
          end;
    mn_MesOrtho:
      if MesCutState = 0 then
      begin
        if SnapPoint(DrawPos, 0, [ot_Poly, ot_CPoly], MesPoint1, SnapObject) then
        begin
          MesCutState := 1;
          SetStatusText(GetLangText(929), FALSE);
        end;
      end
      else
        if MesCutState = 1 then
        begin
          if SnapPoint(DrawPos, 0, [ot_Poly, ot_CPoly], MesPoint2, SnapObject) then
          begin
            MesCutState := 2;
            SetStatusText(GetLangText(931), FALSE);
          end;
        end
        else
          if MesCutState = 2 then
          begin
            if SnapPoint(DrawPos, 0, [ot_Poly, ot_CPoly], Point1, SnapObject) then
            begin
              ProcInsertMeasure(@Self, Point1);
              BreakInput(TRUE, FALSE);
            end;
          end;
    mn_LineCut:
      begin
        CutObj1 := Pointer(SearchByPoint(DrawPos, [ot_Poly]));
        if CutObj1 <> nil then
        begin
          CutObj1^.SearchForLine(PInfo, DrawPos, [ot_Poly],
            PInfo^.CalculateDraw(poMaxDist), PointInd);
          if (PointInd = 0) or (PointInd = CutObj1^.Data^.Count - 2) then
          begin
            if Layers^.IsObjectFixed(CutObj1) then {3003F}
              MsgBox(Parent.Handle, 10147, mb_IconExclamation or mb_OK, '')
            else
            begin
              Select(CutObj1);
              CutPos1.Init(DrawPos.X, DrawPos.Y);
              if TrimmOptions = to_ToArea then
                SetStatusText(GetLangText(11245), FALSE)
              else
                SetStatusText(GetLangText(10102), FALSE);
              MsgLeftClick1 := TRUE;
            end;
          end; {3003F}
        end;
      end;
    mn_Combine:
      begin
        Item := SearchByPoint(DrawPos, [ot_Poly]);
        if Item <> nil then
          with PPoly(Item)^ do
          begin
            SearchForLine(PInfo, DrawPos, [ot_Poly],
              PInfo^.CalculateDraw(poMaxDist), PointInd);
            if (PointInd = 0) or (PointInd = Data^.Count - 2) then
              with PPoly(Item)^ do
              begin
                StartPoints := New(PPoly, Init);
                if Data^.Count = 2 then
                begin
                  if PDPoint(Data^.At(0))^.Dist(DrawPos) < PDPoint(Data^.At(1))^.Dist(DrawPos) then
                    PointInd := 0
                  else
                    PointInd := 1;
                end;
                with Data^ do
                  ProcCombineSearch(@Self, PPoly(Item), PointInd = 0)
              end;
          end;
      end;
    mn_CombineSel:
      begin
        Item := SearchByPoint(DrawPos, [ot_Poly]);
        if Item <> nil then
        begin
          with PPoly(Item)^.Data^ do
          begin
            PPoly(Item)^.SearchForLine(PInfo, DrawPos, [ot_Poly],
              PInfo^.CalculateDraw(poMaxDist), PointInd);
            if Count = 2 then
              Found1 := PDPoint(At(0))^.Dist(DrawPos) > PDPoint(At(1))^.Dist(DrawPos)
            else
              if PointInd = 0 then
                Found1 := FALSE
              else
                if PointInd = Count - 2 then
                  Found1 := TRUE
                else
                begin
                  with NewPoly^.Data^, PDPoint(At(Count - 1))^ do
                    Point3.Init(X, Y);
                  Found1 := PDPoint(At(0))^.Dist(Point3) > PDPoint(At(Count - 1))^.Dist(Point3);
                end;
          end;
          ProcCombineSearch(@Self, PPoly(Item), Found1);
        end;
      end;
    mn_CombineSelOne:
      begin
        Item := SearchByPoint(DrawPos, [ot_Poly]);
        if Item <> nil then
        begin
          with PPoly(Item)^.Data^ do
          begin
            DeselectAll(FALSE);
            ProcCombineSearch(@Self, PPoly(Item), not SearchRect.PointInside(PDPoint(At(0))^));
          end;
        end;
      end;
          {+++ brovak #MOD for copying part of selected file}
    mn_CutImage:
      begin;
        case CSelectForCut of
          1:
            begin {select Image}
              SnapItem := SelectByPoint(DrawPos, [ot_Image]);
              if SnapItem <> nil then
                if PImage(SnapItem).IsVMSITech then
                begin
                  DeSelectAll(True); // VMSI can't be CUT by any means
                end
                else
                begin
                  SetStatusText(GetLangText(11734), False);
                  CSelectForCut := 2;
                  SetCursor(crCross);
                end;
            end;
          2:
            begin {Select First Point}
              if (SnapPoint(DrawPos, 0, ot_Any, Point1, SnapObject))
                and (PImage(SnapItem)^.ClipRect.PointInside(Point1)) then
              begin
                CutRectOfImage.TopLeft.x := Point1.x;
                CutRectOfImage.TopLeft.y := Point1.y;
                SetStatusText(GetLangText(11735), False);
                CSelectForCut := 3;
                SelRect^.StartIt(PInfo, DrawPos, FALSE);
              end;
            end;
          3:
            begin {Select Second Point}
              if SnapPoint(DrawPos, 0, ot_Any, Point2, SnapObject)
                and (PImage(SnapItem)^.ClipRect.PointInside(Point2)) then
              begin
                CutRectOfImage.BottomRight.x := Point2.x;
                CutRectOfImage.BottomRight.y := Point2.y;
                SetValidRect(CutRectOfImage);
                CSelectForCut := 0;
                SelRect^.IsOn := false;
                SelRect^.EndIt(PInfo);
                // Calculating
                if FileExists(PImage(SnapItem)^.FileName^) then
                begin;
                  NewPicFile := PImage(SnapItem)^.FileName^;
//                  ClearStatusText;
                  if ImgDlg.SaveImageFile(NewPicFile) = true then
                  begin;
                    DeleteFile(NewPicFile);
                    with CutRectofImage do
                      CutDRect.Assign(Left, Top, Right, Bottom);
{                    if SavePartialImageAs(PImage(SnapItem)^.FileName^, NewPicFile, CutRectOfImage) = true then}
                    if PImage(SnapItem).Cutting(NewPicFile, CutDRect) then
                      ShowMessage(NewPicFile + GetLangText(11736))
                    else
                      MsgBox(PInfo.HWindow, 5501, MB_OK + MB_ICONERROR, NewPicFile);
                  end;
                end
                else
                  MsgBox(PInfo.HWindow, 5502, MB_OK + MB_ICONERROR, PImage(SnapItem)^.FileName^);
                ClearStatusText;
                SetCursor(SelectCursor);
                SetActualMenu(mn_Select, TRUE);
                DeselectAll(false);
              end;
            end;
        end;
      end;
       {-- brovak}
    mn_SuperImage: //++Sygsky: 22-JAN-2002 to support super image processing
      begin
        case CSelectForCut of
          1:
            begin {First Point was selected}
              SetStatusText(GetLangText(13), False);
              CSelectForCut := 2;
              SelRect^.StartIt(PInfo, DrawPos, FALSE);
            end;
          2:
            begin { Second Point of rectangular Frame was selected }
              SetStatusText(GetLangText(13), False);
              SelRect^.IsOn := false;
              SelRect^.EndIt(PInfo);
              OldSelMode := PInfo.SelectionMode;
//              XChgByte(Byte(Layers.TranspLayer), Byte(OldTransp));
              OldTransp := Layers.TranspLayer;
              Layers.TranspLayer := True; // Force transparency
              try
                PInfo.SelectionMode := smAdditive;
                SelRect.GetRect(CutDRect);
                Self.SelectByRect(CutDRect, FALSE, [ot_Image]);
                ICount := Layers^.SelLayer^.Data.GetCount;
                if ICount = 0 then
                begin
//                  ShowMessage('No images put into your frame :o(');
                  MsgBox(PInfo.HWindow, 14, MB_OK + MB_ICONWARNING, '');
                end
                else
                begin
                  SIPtr := AllocMem(SizeOf(TSIStruct));
                  SIPtr.Size := SizeOf(TSIStruct);
                  SIPtr.Count := ICount;
                  GetMem(SIPtr.PWGArray, SizeOf(PIndex) * ICount);
                  try
                    SiPtr.Mode := SuperImageOption;
//                SiPtr.FreeOnExit := False - set as default after AllocMem
                    for I := 0 to Pred(ICount) do // Fill all the items
                      SIPtr.PWGArray[I] := Layers^.SelLayer^.Data.At(I);
                    SelRect.GetRect(SIPtr.FrameRect);
                    SendMessage(Self.Parent.Handle, WM_COMMAND, cm_SuperImage, Integer(SIPtr));
                  finally
                    FreeMem(SIPtr.PWGArray);
                    FreeMem(SIPtr); // As we don't set FreeOnExit flag in struct
                  end;
                end;
              finally
                if Layers^.SelLayer^.Data.GetCount > 0 then
                  Self.DeSelectAll(FALSE);
                PInfo.SelectionMode := OldSelMode;
//                XChgByte(Byte(Layers.TranspLayer), Byte(OldTransp));
                Layers.TranspLayer := OldTransp;
                SetCursor(SelectCursor);
                SetActualMenu(mn_Select, TRUE);
                ClearStatusText;
              end;
            end; // end of case (CSelectPoint = 2)
        end; // of case on CSelectPoint
      end; // of mn_SuperImage left mouse click
//++ Glukhov LoadImageInRect BUILD#123 20.07.00
{(*}
    mn_SetImageRect:
      begin
        if RTrafoCnt = 0 then
        begin
          if SnapPoint(DrawPos, 0, ot_Any, Point1, SnapObject) then
          begin
            // Save the 1st corner
            RTrafoInfo^[0].X := Point1.X;
            RTrafoInfo^[0].Y := Point1.Y;
            Inc(RTrafoCnt);
            SetStatusText(GetLangText(11766), False);
            SelRect^.StartIt(PInfo, DrawPos, False);
          end;
        end;
        if RTrafoCnt = 1 then
        begin
          if SnapPoint(DrawPos, 0, ot_Any, Point1, SnapObject) then
          begin
            if (RTrafoInfo^[0].X <> Point1.X) and
              (RTrafoInfo^[0].Y <> Point1.Y) then
            begin
              // Save the 2nd corner
              RTrafoInfo^[1].X := Point1.X;
              RTrafoInfo^[1].Y := Point1.Y;
              Inc(RTrafoCnt);
              SelRect^.EndIt(PInfo);
              // Set the image ClipRect
              if RTrafoInfo^[0].X < RTrafoInfo^[1].X then
              begin
                PImage(ActualData).ClipRect.A.X := RTrafoInfo^[0].X;
                PImage(ActualData).ClipRect.B.X := RTrafoInfo^[1].X;
              end
              else begin
                PImage(ActualData).ClipRect.A.X := RTrafoInfo^[1].X;
                PImage(ActualData).ClipRect.B.X := RTrafoInfo^[0].X;
              end;
              if RTrafoInfo^[0].Y < RTrafoInfo^[1].Y then
              begin
                PImage(ActualData).ClipRect.A.Y := RTrafoInfo^[0].Y;
                PImage(ActualData).ClipRect.B.Y := RTrafoInfo^[1].Y;
              end
              else begin
                PImage(ActualData).ClipRect.A.Y := RTrafoInfo^[1].Y;
                PImage(ActualData).ClipRect.B.Y := RTrafoInfo^[0].Y;
              end;
              // Insert image in project
              if InsertObject(PImage(ActualData), False) then
              begin
                // The secret code that I do not understand
                if (DDEHandler.DoAutoIns) then DBEditAuto(PIndex(PImage(ActualData)));
                Inc(BitmapCount);
                PImage(ActualData).Invalidate(PInfo);
                ActualData := nil;
//                  UserInterface.Update([uiMenus]);
                SetActualMenu(mn_Select);
              end;
            end;
          end;
        end;
      end;
//-- Glukhov LoadImageInRect BUILD#123 20.07.00

//++ Glukhov ObjAttachment 11.10.00
    mn_AttachObject:
      begin
        Item:= PView(ActualData);
        SnapItem:= SelectByPoint(DrawPos, ot_Any);
        if Assigned(SnapItem) and Assigned(Item) then
        begin
          // Find an original object for the selected one
          SnapItem:= Layers^.IndexObject(PInfo, SnapItem);
          if SnapItem.Index = Item.Index then
          begin
            // Restore the selection (the side effect was a deselection)
            ProcSelDeselIndex(@Self, SnapItem);
            // Show message 'attached to itself'
            MsgBox(PInfo.HWindow, 11771, Mb_IconExclamation or Mb_OK, '');
          end
          else begin
            // Ask an acknowledgement
            if MsgBox(PInfo.HWindow, 11772, mb_IconQuestion or mb_YesNo or mb_DefButton1, '') = id_Yes then
            begin
              case PView(Item).AttachToObject(PInfo, PView(SnapItem)) of
              -1, 0: // processed already
                begin
                end;
              -2: // loop attachment
                MsgBox(PInfo.HWindow, 11773, Mb_IconExclamation or Mb_OK, '');
              else
                begin
                  ActualData := nil;
                  SetModified;
                  SetActualMenu(mn_Select);
                end;
              end;
            end;
            // Deselect the object
            ProcSelDeselIndex(@Self, SnapItem);
          end;
        end;
      end;
//-- Glukhov ObjAttachment 11.10.00

//++ Glukhov Bug#468 Build#162 16.07.01
    mn_SelToDrawAlong:
      begin
        SnapItem:= SelectByPoint(DrawPos, [ot_Poly, ot_Arc, ot_Spline]);
        if Assigned(SnapItem) and Assigned(ActualData) then
        begin
          // Find an original object for the selected one
          SnapItem:= Layers^.IndexObject(PInfo, SnapItem);
          // Ask an acknowledgement
          if MsgBox(PInfo.HWindow, 11781, mb_IconQuestion or mb_YesNo or mb_DefButton1, '') = id_Yes then
          begin
            // Deselect the object
            ProcSelDeselIndex(@Self, SnapItem);
            // Return to dialog
            TTxtAdvDialog(ActualData).oSelLine:= PView( SnapItem );
            ProcForTxtAdvancedHandler( @Self, True );
          end
          else begin
            // Deselect the object
            ProcSelDeselIndex(@Self, SnapItem);
          end;
        end;
      end;
//-- Bug#468 Build#162 16.07.01

//++ Glukhov Bug#203 Build#151 08.02.01
    mn_GetCoordinates:
      begin
        if SnapPoint(DrawPos, 0, ot_Any, Point1, SnapObject) then
        begin
          ProcSendCoordinates( @Self, Point1.X, Point1.Y );
          RTrafoCnt:= 1;
          SetActualMenu(mn_Select);
        end;
      end;
{*)}
//-- Glukhov Bug#203 Build#151 08.02.01

    mn_RTrafo:
      begin
        case RTrafoState of
          SelectBmp:
            begin
              SnapItem := SelectByPoint(DrawPos, [ot_Image]);
              if SnapItem <> nil then
              begin
                SetStatusText(GetLangText(10220), False);
                RTrafoState := ObjPoint;
              end;
            end;
          ObjPoint:
            begin
              if SnapPoint(DrawPos, 0, ot_Any, Point1, SnapObject) then
              begin
                RTrafoInfo^[RTrafoCnt * 2].X := Point1.X;
                RTrafoInfo^[RTrafoCnt * 2].Y := Point1.Y;
                RTrafoState := ImgPoint;
                SetStatusText(GetLangText(10221), False);
              end;
            end;
          ImgPoint:
            begin
              if SnapPoint(DrawPos, 0, ot_Any, Point1, SnapObject) then
                if PImage(SnapItem)^.ClipRect.PointInside(Point1) then
                begin
                  RTrafoInfo^[(RTrafoCnt * 2) + 1].X := Point1.X;
                  RTrafoInfo^[(RTrafoCnt * 2) + 1].Y := Point1.Y;
                  SetStatusText(GetLangText(10220), False);
                  RTrafoState := ObjPoint;
                  Inc(RTrafoCnt);
                  if (RTrafoType = tfLinear) and (RTrafoCnt = 2) then
                  begin
                    ProcRasterTransform(@Self);
                    RTrafoCnt := 0;
                    RTrafoState := SelectBmp;
                  end;
                end;
            end;
        end;
      end;

    mn_SelForSelect:
      begin
        ALayer := Layers^.IndexToLayer(SelectData.SelectLayer);
        if ALayer <> nil then
        begin
          SetCursor(crHourGlass);
          SetStatusText(GetLangText(4019), TRUE);
          Item := ALayer^.SearchByPoint(PInfo, DrawPos, SelectMask);
          ClearStatusText;
          SetCursor(crDefault);
        end
        else
          Item := nil;
        if (SelectData.SelectWith = swOffset)
          and (SelectOffsetData.OffsetType = otDouble)
          and (Item <> nil) then
          if not PPoly(Item)^.IsClosed then
            Item := nil;
        if Item <> nil then
          case SelectData.SelectWith of
            swCircle:
              begin
                if DBSelection^.Selecting then
                begin
                  DBSelection^.SelItem := Item;
                  DBSelection^.ContinueDBSelection
                end
                else
                  SelectByCircle(PEllipse(Item)^.Position, PEllipse(Item)^.PrimaryAxis,
                    SelectData.SelectTypes, SelectData.SelectInside);
              end;
            swPoly:
              begin
                if DBSelection^.Selecting then
                begin
                  DBSelection^.SelItem := Item;
                  DBSelection^.ContinueDBSelection
                end
                else
                  SelectByPoly(Item, SelectData.SelectTypes, SelectData.SelectInside);
              end;
            swLine:
              begin
                if DBSelection^.Selecting then
                begin
                  DBSelection^.SelItem := Item;
                  DBSelection^.ContinueDBSelection
                end
                else
                  Layers^.SelectByPolyLine(PInfo, Item, SelectData.SelectTypes);
              end;
            swPoint:
              begin
                if DBSelection^.Selecting then
                begin
                  DBSelection^.SelItem := Item;
                  DBSelection^.ContinueDBSelection
                end
                else
                  SelectByNeighbour(Item);
              end;
            swOffset:
              begin
                OffsetItem := Pointer(Item);
                if DBSelection^.Selecting and (DBSelection^.SOffset <> 0) then
                begin
                  DBSelection^.SelItem := Item;
                  DBSelection^.CorrectDir := TRUE;
                  DBSelection^.ContinueDBSelection;
                end
                else
                begin
                  if DBSelection^.Selecting then
                  begin
                    DBSelection^.SelItem := Item;
                    DBSelection^.CorrectDir := FALSE;
                  end;
                  InpOffset^.SetPoly(PPoly(OffsetItem));
                  InpOffset^.StartIt(PInfo, DrawPos, FALSE);
                  InpOffset^.MouseMove(PInfo, DrawPos);
                  SetActualMenu(mn_OffsetForSel);
                end;
              end;
            swPixel:
              begin
                AView := Layers^.IndexObject(PInfo, Item);
                SelPoint.Init(PPixel(AView)^.Position.X, PPixel(AView)^.Position.Y);
                if DBSelection^.Selecting then
                begin
                  DBSelection^.SelItem := Item;
                  DBSelection^.ContinueDBSelection;
                end;
              end;
          end;
      end;
    mn_DrawForSelect:
      if SnapPoint(DrawPos, 0, ot_Any, Point1, SnapObject) then
      begin
        case SelectData.SelectWith of
          swCircle:
            begin
              SelCircle^.StartIt(PInfo, Point1, FALSE);
              SetStatusText(Format(GetLangText(205), [0.0]), TRUE);
              MsgLeftClick1 := TRUE;
            end;
          swLine,
            swOffset,
            swPoint,
            swPoly:
            begin
              NewPoly^.PointInsert(PInfo, Point1, FALSE);
              NewPoly^.LastIns := True;
              NewPoly^.LineOn := True;
            end;
          swRect:
            begin
              SelRect^.StartIt(PInfo, Point1, FALSE);
              MsgLeftClick1 := TRUE;
            end;
          swPixel:
            begin
              SelPoint.Init(DrawPos.X, DrawPos.Y);
              if DBSelection^.Selecting then
                DBSelection^.ContinueDBSelection;
            end;
        end;
      end
      else
        NewPoly^.LastIns := False;
    mn_DBSelCircle:
      if SnapPoint(DrawPos, 0, ot_Any, Point1, SnapObject) then
      begin
        SelCircle^.StartIt(PInfo, Point1, FALSE);
        SetStatusText(Format(GetLangText(205), [0.0]), TRUE);
        MsgLeftClick1 := TRUE;
      end;
    mn_DBSelRect:
      if SnapPoint(DrawPos, 0, ot_Any, Point1, SnapObject) then
      begin
        SelRect^.StartIt(PInfo, Point1, FALSE);
        MsgLeftClick1 := TRUE;
      end;
    mn_OffsetForSel:
      begin
        InpOffset^.GetOffset(AOffset);
        if DBSelection^.Selecting then
        begin
          DBSelection^.SetOffset(AOffset);
          DBSelection^.CorrectDir := FALSE;
          DBSelection^.ContinueDBSelection;
        end
        else
          DoCreateOffsetAndSelect(@Self, AOffset);
      end;
    mn_InsertOLEObject,
      mn_InsLinkObj,
      mn_PasteOLEObj:
      begin {*************** OLE **********}
        if SnapPoint(DrawPos, 0, ot_Any, Point1, SnapObject) then
        begin
          OLERect^.StartIt(PInfo, Point1, FALSE);
          MsgLeftClick1 := True;
          SetStatusText(GetLangText(11122), False);
        end;
      end;
    mn_RefBitmap:
      begin { TR-DDE }
        if (GeoReference = nil) then
        begin
          GetMem(GeoReference, sizeof(LPOINT) * 128);
          if (GeoReference = nil) then
          begin
            MsgBox(WinGISMainForm.Handle, 11166, Mb_IconExclamation or Mb_OK, '');
            SetActualMenu(mn_Select);
            RefPtCnt := 0;
            exit;
          end;
        end;
        if SnapPoint(DrawPos, 0, ot_Any, Point1, SnapObject) then
        begin
{$R-}
          GeoReference^[RefPtCnt + 1].x := Point1.x;
          GeoReference^[RefPtCnt + 1].y := Point1.y;
{$R+}
          Inc(RefPtCnt);
          Str(RefPtCnt + 1, Dummy);
          Dummy := Dummy + GetLangText(11124);
          SetStatusText(Dummy, False);
          if (RefPtCnt > 1) and
            ((WinGISMainForm.TurboRasterDDE^.CommVersion = 1) and
            WinGISMainForm.TurboRasterDDE^.IsConnected) then
          begin
            ClearStatusText;
            GeoReference^[0].x := RefPtCnt;
            GeoReference^[0].y := 0;
            WinGISMainForm.TurboRasterDDE^.SendRefPoints(GeoReference);
            SetActualMenu(mn_Select);
            RefPtCnt := 0;
            FreeMem(GeoReference, sizeof(LPOINT) * 128);
            GeoReference := nil;
          end;
        end;
      end;
    mn_OrthoMark,
      mn_DistMark:
      if SnapPoint(DrawPos, 0, ot_Any, Point1, SnapObject) then
        ProcMarkLinearOrtho(@Self, Point1, DrawPos);
    mn_CircleCutMark:
      if SnapPoint(DrawPos, 0, ot_Any, Point1, SnapObject) then
        ProcMarkCircleCut(@Self, Point1, DrawPos);
    mn_CircleMark:
      if SnapPoint(DrawPos, 0, ot_Any, Point1, SnapObject) then
        ProcMarkCircle(@Self, Point1, DrawPos);
    mn_AngleMark:
      if SnapPoint(DrawPos, 0, ot_Any, Point1, SnapObject) then
        ProcMarkAngle(@Self, Point1, DrawPos);
    mn_LineMark:
      if SnapPoint(DrawPos, 0, ot_Any, Point1, SnapObject) then
        ProcMarkLinePar(@Self, Point1, DrawPos);
{$ENDIF}
  end;
{$ENDIF} // <----------------- AXDLL
end;

{***************************************************************************************}
{ Procedure TProj.MsgLeftDblClick                                                       }
{***************************************************************************************}

procedure TProj.MsgLeftDblClick
  (
  DrawPos: TDPoint
  );
{$IFNDEF AXDLL} // <----------------- AXDLL
var
  InsPoly: PPoly;
  AItem: PIndex;
  Number: LongInt;
  Rect: TDRect;
  Ratio: Double; // Glukhov Bug#170 BUILD#125 03.11.00
  {++ Ortho End}
  Ndist: Double;
  OrtoEnd: TDPoint;
  {-- Ortho End}
  DelLast: Boolean;
{$ENDIF} // <----------------- AXDLL
begin
  ClearStatusText;
{$IFNDEF AXDLL} // <----------------- AXDLL
  case ActualMen of
{$IFNDEF WMLT}
    mn_PartFree, mn_PartParallel:
      begin
        if not PolyPart.bDeleteSourcePoly then
        begin
          MsgRightClick(CurPos);
        end
        else
        begin
          if NewPoly^.Data^.Count > 1 then
          begin
            SetActualMenu(mn_MovePartPara);
            if NewPoly^.Data^.Count > 2 then
            begin
              if not PolyPart.bDeleteSourcePoly then
              begin
                NewPoly^.DeleteLastPoint(PInfo);
              end;
            end;
            MovePoly^.SetPoly(NewPoly);
            PolyPart.SetPartLine(NewPoly);
            MovePoly^.StartIt(PInfo, CurPos, FALSE);
            PolyPart.CalcStartMovePoly;
            OldCursor := idc_Move;
            SetCursor(OldCursor);
          end;
          OldMenu := ActualMen;
        end;
      end;
    mn_Select:
      if (not Layers^.TopLayer^.GetState(sf_Fixed)) then
      begin
        SnapItem := Layers^.SelLayer^.SearchByPoint(PInfo, DrawPos, [ot_Text, ot_Symbol,
          ot_Spline, ot_Image, ot_RText, ot_BusGraph, ot_OLEObj, ot_Poly, ot_CPoly, ot_Circle, ot_Arc, ot_pixel]);
        if SnapItem <> nil then
        begin
(*brovak-new edit style!          if SnapItem^.GetObjType = ot_Text then
          begin    *)
//++ Glukhov Bug#170 BUILD#125 03.11.00
{ Commented the old code
            TextRect^.SetText(PInfo,PText(SnapItem));
            SetStatusText(Format(GetLangText(207),[Abs(TextRect^.Height/100),TextRect^.GetDisplayAngle]),TRUE);
            SetCursor(idc_Size);
            ActualMen:=mn_SizeRot;
}
(*            if PInfo.ViewRotation <> 0 then
            begin
            // Ignore text moving for the rotated project view
              MsgBox(WinGISMainForm.Handle, 11294, Mb_IconExclamation or Mb_OK, '')
            end
            else begin
              AItem := Layers^.TopLayer^.HasObject(SnapItem);
              if Assigned(AItem) then Layers.TopLayer.DeSelect(PInfo, AItem);
              ActualMen := mn_EditTxtRect;
//++ Glukhov Bug#264 Build#161 15.06.01
{
              InputBox^.SetRect(SnapItem^.ClipRect);
              if (PText(SnapItem).Font.Style and ts_FixHeight) <> 0 then
                InputBox^.Mode := m_FixSize
              else
              InputBox^.Mode := m_CornersOnly;
              InputBox^.StartIt(PInfo, DrawPos, True);
}
*)
//++ Glukhov Bug#468 Build#162 16.07.01
(*              if (PText(SnapItem).Font.Style and ts_FixHeight) <> 0
              then  EditTxtBox^.Mode:= m_FixSize
              else  EditTxtBox^.Mode:= m_CornersOnly;

              if (PText(SnapItem).fAdvMode = cwTxtAdvRegular)
              or (PText(SnapItem).fAdvMode = cwTxtAdvSpaced) then begin
              // can be rotated
                EditTxtBox^.Mode:= EditTxtBox^.Mode + m_Rotation;
                EditTxtBox^.SetTxtRect(
                  SnapItem^.ClipRect, PText(SnapItem).Pos,
                  PText(SnapItem).Angle, PText(SnapItem).Height, PText(SnapItem).Width );
              end
              else begin
              // cwTxtAdvSelPoly - text drawn along, use ClipRect only
                EditTxtBox^.TxtPos.Init(
                  SnapItem.ClipRect.A.X, SnapItem.ClipRect.B.Y );
                EditTxtBox^.SetTxtRect(
                  SnapItem^.ClipRect, EditTxtBox^.TxtPos, 0,
                  SnapItem^.ClipRect.B.Y - SnapItem^.ClipRect.A.Y,
                  SnapItem^.ClipRect.B.X - SnapItem^.ClipRect.A.X );
              end;
//-- Glukhov Bug#468 Build#162 16.07.01
              EditTxtBox^.StartIt(PInfo, DrawPos, True);
//-- Glukhov Bug#264 Build#161 15.06.01
            end;
//-- Glukhov Bug#170 BUILD#125 03.11.00
          end
          else *)
(*            if SnapItem^.GetObjType = ot_Symbol then
            begin
{++ IDB_UNDO}// The symbol properties have been changed (Size or angle will be changed).
              if (WinGisMainForm.bWeAreUsingUndoFunction)
                and
                (WinGisMainForm.IDB_Man.AddObjectInUndoData(@SELF, PView(SnapItem))) then
                WinGisMainForm.IDB_Man.SaveUndoData(@SELF, utUnChange);
{-- IDB_UNDO}
              SymbolInp^.SetSymbol(PInfo, PSymbol(SnapItem));
              SetStatusText(Format(GetLangText(209), [SymbolInp^.Scaling,
                (SymbolInp^.AktAngle + SymbolInp^.RotAngle) * Rad2Deg + 360 mod 360]), TRUE);
              SetCursor(idc_Size);
              ActualMen := mn_SymbolInp;
            end
            else  *)

          if SnapItem^.GetObjType = ot_Spline then
          begin
            AItem := Layers^.TopLayer^.HasObject(SnapItem);
            if AItem <> nil then
              Layers^.TopLayer^.DeSelect(PInfo, AItem);
            PInfo^.RedrawInvalidate;
            ActualMen := mn_SplineEdit;
            EditSpline^.SetSpline(PSpline(SnapItem));
            EditSpline^.StartIt(PInfo, DrawPos, TRUE);
          end
          else

            if SnapItem^.GetObjType = ot_Image then
            begin
{++ Glukhov HELMERT BUILD#115 08.06.00 }
              if PInfo^.ViewRotation <> 0 then
              begin
            // Ignore bitmap editing for the rotated project view
                MsgBox(WinGISMainForm.Handle, 11292, Mb_IconExclamation or Mb_OK, '')
              end
              else
              begin
{-- Glukhov HELMERT BUILD#115 08.06.00 }
                AItem := Layers^.TopLayer^.HasObject(SnapItem);
                if AItem <> nil then
                  Layers^.TopLayer^.DeSelect(PInfo, AItem);
                ActualMen := mn_EditImage;
                InputBox^.SetRect(SnapItem^.ClipRect);
                InputBox^.StartIt(PInfo, DrawPos, TRUE);
{++ Glukhov HELMERT BUILD#115 08.06.00 }
              end;
{-- Glukhov HELMERT BUILD#115 08.06.00 }
            end
            else
              if SnapItem^.getObjType = ot_OLEObj then
              begin
                AItem := Layers^.TopLayer^.HasObject(SnapItem);
                if AItem <> nil then
                  Layers^.TopLayer^.DeSelect(PInfo, AItem);
                ActualMen := mn_EditOLE;
                InputBox^.SetRect(PView(SnapItem)^.ClipRect);
                InputBox^.StartIt(PInfo, DrawPos, TRUE);
              end
              else
                if SnapItem^.GetObjType = ot_RText then
                  ProcChangeGraphText(@Self, SnapItem)
                else
                  if SnapItem^.GetObjType = ot_BusGraph then
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
                  begin
                           //AM_DBGra.ProcChangeBusGraph(@Self, SnapItem); //old procedure
                    BusGraphics.ProcChangeBusGraph(@Self, SnapItem, PBusGraph(SnapItem)^.ChartKindPtr, True);
                  end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

           {brovak 193}

          if ((SnapItem^.GetObjType in [ot_poly, ot_cpoly, ot_circle, ot_arc, ot_pixel, ot_symbol, ot_text]) and (not IsCanvas)) then
          begin;
            IsEditVarious := true;
            OldSnapItem := nil;
            FindOLdSnapItem;

            if WingisLicense >= WG_STD then
            begin
              TMDIChild(Self.Parent).SwitchEditPointInVariousObjects;
              MenuFunctions['EditPointInVariousObjects'].Checked := true;
            end;
          end;

          {brovak}

        end;
      end;
    mn_Snap, mn_SnapPoly:
      begin;
        if SymbolMode <> sym_Omega then
        begin;
          BreakInput(false, true);
          EditVertexMode := 2;
          TMDIChild(Self.Parent).SwitchEditPointInVariousObjects;
        end;
      end;
    mn_InsPoint, mn_InsPOK:
      begin
        if SymbolMode <> sym_Omega then
        begin
          BreakInput(false, true);
          EditVertexMode := 3;
          TMDIChild(Self.Parent).SwitchEditPointInVariousObjects;
        end;
      end;
    mn_DelPoint, mn_DelPOk:
      begin
        if SymbolMode <> sym_Omega then
        begin
          BreakInput(false, true);
          EditVertexMode := 5;
          TMDIChild(Self.Parent).PseudoSelectMode;
        end;
      end;

    mn_PseudoSelect:
      begin;
        if SymbolMode <> sym_Omega then
        begin
          BreakInput(false, true);
          EditVertexMode := 1;
          TMDIChild(Self.Parent).SwitchEditPointInVariousObjects;
        end;
      end;

    mn_CIslands:
      begin
        InsPoly := New(PCPoly, Init);
        if not NewPoly^.CheckCopyData(PInfo, InsPoly, TRUE) then
          Dispose(InsPoly, Done)
        else
        begin
          InsertObject(InsPoly, FALSE);
          PostMessage(Parent.Handle, wm_Command, cm_DoInsIsland, 0);
        end;
      end;
    mn_CBufferDraw:
      begin
        InsPoly := New(PPoly, Init);
        if not NewPoly^.CheckCopyData(PInfo, InsPoly, TRUE) then
        begin
          Dispose(InsPoly, Done);
          Construct^.BufferLine := nil;
        end
        else
        begin
          InsertObject(InsPoly, FALSE);
          Construct^.BufferLine := InsPoly;
        end;
        OffsetItem := Construct^.BufferLine;
        PostMessage(Parent.Handle, wm_Command, cm_DoBuffer, 0);
      end;
    mn_DrawSymLine:
      begin { Refernzlinie f�r Symboleditor zeichnen}
        InsPoly := New(PPoly, Init);
        if not NewPoly^.CheckCopyData(PInfo, InsPoly, TRUE) then
          Dispose(InsPoly, Done)
        else
        begin
          TSymEditor(SymEditInfo.SymEditor).SetUnitLine(InsPoly);
          InsertObjectWithStyle(InsPoly, TSymEditor(SymEditInfo.SymEditor).FRefLayer,
            TSymEditor(SymEditInfo.SymEditor).RefStyleData);
        end;
      end;
    mn_CPoly:
      begin
        InsPoly := New(PCPoly, Init);
        //! - mod by Brovak  Bug N 171 build 135

        {++ Ortho End}
        if newpoly.bDrawOrtho then
        begin
          if SELF.NewPoly.Data.Count > 4 then
          begin
            OrtoEnd.Init(0, 0);
            PInfo^.NormalDistance(
              SELF.NewPoly.Data.At(SELF.NewPoly.Data.Count - 3),
              SELF.NewPoly.Data.At(SELF.NewPoly.Data.Count - 2),
              SELF.NewPoly.Data.At(0), Ndist, OrtoEnd);
            SELF.NewPoly.DeleteLastPoint(SELF.PInfo);
            SELF.NewPoly.DeleteLastPoint(SELF.PInfo);
            SELF.NewPoly.PointInsert(pinfo, OrtoEnd, true);
//              SELF.NewPoly.PointInsert(pinfo,DrawPos,true);
            NewPoly.ClipRect.CorrectByPoint(OrtoEnd);
            NewPoly.Invalidate(PInfo);
          end;
        end;
        {-- Ortho End}

        if not NewPoly^.CheckCopyData(PInfo, InsPoly, {not (Self.SnapState)} FALSE) then
          Dispose(InsPoly, Done)
        else
        begin
          InsertObject(InsPoly, FALSE);
          if DDEHandler.DoAutoIns then
            DBEditAuto(PIndex(InsPoly));
        end;
      end;
//++ Glukhov Bug#468 Build#162 16.08.01
    mn_EntToDrawAlong:
      begin
        InsPoly := New(PPoly, Init);
        if not NewPoly^.CheckCopyData(PInfo, InsPoly, True) then
          Dispose(InsPoly, Done)
        else
        begin
          // Return to dialog
          TTxtAdvDialog(ActualData).oSelLine := PView(InsPoly);
          ProcForTxtAdvancedHandler(@Self, True);
        end;
      end;
//-- Glukhov Bug#468 Build#162 16.08.01
    mn_Poly:
      begin
        InsPoly := New(PPoly, Init);
        DelLast := NewPoly^.DeleteLast;
        if not NewPoly^.CheckCopyData(PInfo, InsPoly, DelLast) then
          Dispose(InsPoly, Done)
        else
        begin
          InsertObject(InsPoly, FALSE);
          if (DDEHandler.DoAutoIns) {and (DDEHandler.FDBAutoIn)} then
            DBEditAuto(PIndex(InsPoly));
        end;
      end;
{$ENDIF}
    mn_Distance:
      begin
{$IFNDEF WMLT}
        ProcSendDistanceToDB(LastDistance);
{$ENDIF}
        InsPoly := New(PPoly, Init);
        NewPoly^.CheckCopyData(PInfo, InsPoly, TRUE);
        Dispose(InsPoly, Done);
        ClearStatusText;
        LastDistance := 0;
        PInfo^.RedrawScreen(False);
      end;
{$IFNDEF WMLT}
    mn_SplineEdit:
      begin
        BreakInput(TRUE, FALSE);
        SnapItem^.Invalidate(PInfo);

        EditSpline^.GetSpline(PSpline(SnapItem));

        SnapItem^.Invalidate(PInfo);
        PInfo^.RedrawInvalidate;
        ProcDBSendObject(@Self, SnapItem, 'UPD');
        SetModified;
      end;
    mn_EditImage:
      begin
        BreakInput(TRUE, FALSE);
//++ Glukhov ObjAttachment 15.11.00
        // Save the old ClipRect
        Rect.InitByRect(SnapItem.ClipRect);
        if not Rect.IsEqual(InputBox.Rect) then
        begin
//-- Glukhov ObjAttachment 15.11.00
{++ IDB_UNDO}// Moving or resizing of an image.
          if (WinGisMainForm.WeAreUsingUndoFunction[@SELF])
            and
            (WinGisMainForm.UndoRedo_Man.AddObjectInUndoData(@SELF, PView(SnapItem))) then
            WinGisMainForm.UndoRedo_Man.SaveUndoData(@SELF, utUnChange);
{-- IDB_UNDO}
//++ Glukhov ObjAttachment 15.11.00
{ Old code
          Control.Init(SnapItem^.ClipRect.A.X,SnapItem^.ClipRect.A.Y);
          SnapItem^.Invalidate(PInfo);
          InputBox^.GetRect(SnapItem^.ClipRect);
          SnapItem^.Invalidate(PInfo);
          if not (IniFile^.BmpPixelsExact or IniFile^.BmpAreaExact) then begin
            PImage(SnapItem)^.UnLoad;
          end;
          if not CorrectSize(SnapItem^.ClipRect,TRUE) then PInfo^.RedrawInvalidate;
          if (Control.X <> SnapItem^.ClipRect.A.X)
              or (Control.Y <> SnapItem^.ClipRect.A.Y) then
            ProcDBSendObject(@Self,SnapItem,'UPD');
          UpdateClipRect(PImage(SnapItem));
          Control.Done;
          SetModified;
}
//--
          InputBox^.GetRect(SnapItem^.ClipRect);
//++ Glukhov Bug#454 Build#161 14.06.01
          PImage(SnapItem).MakeRotFromClip;
//-- Glukhov Bug#454 Build#161 14.06.01
          RedrawWithAllAttached(@Self, PView(SnapItem), Rect);
          if WinGisMainForm.WeAreUsingTheIDB[@self] then
            WinGISMainForm.IDB_Man.UpdateCalcFields(@self, Layers^.TopLayer, SnapItem^.Index);
        end;
//-- Glukhov ObjAttachment 15.11.00
      end;

//++ Glukhov Bug#264 Build#161 15.06.01
    mn_EditTxtRect:
      begin
        BreakInput(True, False);
        EditTxtBox^.Mode := m_Default;
        // Save the old ClipRect
        Rect.InitByRect(SnapItem.ClipRect);
        if not Rect.IsEqual(EditTxtBox.Rect) then
        begin
{++ IDB_UNDO}
          // Movement, resizing, or rotation of a text
          if (WinGisMainForm.WeAreUsingUndoFunction[@SELF])
            and
            (WinGisMainForm.UndoRedo_Man.AddObjectInUndoData(@SELF, PView(SnapItem))) then
            WinGisMainForm.UndoRedo_Man.SaveUndoData(@SELF, utUnChange);
{-- IDB_UNDO}
//++ Glukhov Bug#468 Build#162 16.07.01
          if (PText(SnapItem).fAdvMode = cwTxtAdvRegular)
            or (PText(SnapItem).fAdvMode = cwTxtAdvSpaced) then
          begin
            // Set the new text data
            PText(SnapItem).Pos := EditTxtBox.TxtPos;
            PText(SnapItem).Angle := EditTxtBox.Angle;
          end
          else
          begin
            // cwTxtAdvSelPoly - text drawn along, set the add.shift
            PText(SnapItem).MoveRel(
              EditTxtBox.Rect.A.X - Rect.A.X, EditTxtBox.Rect.B.Y - Rect.B.Y);
          end;
//-- Glukhov Bug#468 Build#162 16.07.01

          if (PText(SnapItem).Font.Style and ts_FixHeight) = 0 then
          begin
            // Scale the font height
//++ Glukhov Bug#468 Build#162 16.07.01
            if (PText(SnapItem).fAdvMode = cwTxtAdvRegular)
              or (PText(SnapItem).fAdvMode = cwTxtAdvSpaced) then
            begin
              Ratio := EditTxtBox.Height / PText(SnapItem).Height;
            end
            else
            begin
              // cwTxtAdvSelPoly - text drawn along, use ClipRect only
              Ratio := EditTxtBox.Height / Rect.YSize;
            end;
            Number := Round(Ratio * PText(SnapItem).Font.Height);
            if Number < cMinFontHeight then
              Number := cMinFontHeight;
            PText(SnapItem).Font.Height := Number;
          end;
          // Recalculate the other text data
          PText(SnapItem).UpdateTextData(PInfo);
//-- Glukhov Bug#468 Build#162 01.07.01
          RedrawWithAllAttached(@Self, PView(SnapItem), Rect);
          ActualMen := mn_Select;
          PInfo^.RedrawScreen(False);
        end;
      end;
//-- Glukhov Bug#264 Build#161 15.06.01

    mn_EditOLE:
      begin
        POleClientObject(SnapItem)^.UpdateFromServer;
        SnapItem^.Invalidate(PInfo);
        InputBox^.GetRect(PView(SnapItem)^.ClipRect);
        SnapItem^.Invalidate(PInfo);
        if not CorrectSize(PView(SnapItem)^.ClipRect, TRUE) then
          PInfo^.RedrawInvalidate;
        UpdateClipRect(PView(SnapItem));
        BreakInput(TRUE, FALSE);
        SetModified;
      end;
    mn_Combine,
      mn_CombineSel,
      mn_CombineSelOne:
      begin
        if CombineData.MakeAreas then
          InsPoly := New(PCPoly, Init)
        else
          InsPoly := New(PPoly, Init);
        if not NewPoly^.CheckCopyData(PInfo, InsPoly, FALSE) then
          Dispose(InsPoly, Done)
        else
          if InsertObjectOnLayer(InsPoly, CombineData.DestLayer) then
          begin
            if CombineData.DestLayer <> Layers^.TopLayer^.Index then
            begin
              InsPoly^.Invalidate(PInfo);
              PInfo^.RedrawInvalidate;
            end;
          end;
        BreakInput(TRUE, FALSE);
      end;
    mn_RTrafo:
      if RTrafoType = tfHelmert then
      begin
        if (RTrafoState <> SelectBmp) and (RTrafoCnt > 0) then
        begin
          ProcRasterTransform(@Self);
          RTrafoCnt := 0;
          RTrafoState := SelectBmp;
        end;
      end;
    mn_DrawForSelect:
      case SelectData.SelectWith of
        swLine:
          begin
            InsPoly := New(PPoly, Init);
            if NewPoly^.GetPoly(PInfo, InsPoly, TRUE) then
            begin
              if DBSelection^.Selecting then
                DBSelection^.ContinueDBSelection
              else
                Layers^.SelectByPolyLine(PInfo, InsPoly, SelectData.SelectTypes);
            end;
            NewPoly^.EndIt(PInfo);
            Dispose(InsPoly, Done);
          end;
        swPoly:
          begin
            InsPoly := New(PCPoly, Init);
            if NewPoly^.GetPoly(PInfo, InsPoly, TRUE) then
            begin
              if DBSelection^.Selecting then
                DBSelection^.ContinueDBSelection
              else
                SelectByPoly(InsPoly, SelectData.SelectTypes, SelectData.SelectInside);
            end;
            NewPoly^.EndIt(PInfo);
            Dispose(InsPoly, Done);
          end;
        swPoint:
          begin
            InsPoly := New(PCPoly, Init);
            if NewPoly^.CheckCopyData(PInfo, InsPoly, TRUE) then
              SelectByNeighbour(InsPoly);
            Dispose(InsPoly, Done);
          end;
        swOffset:
          begin
            if (SelectOffsetData.OffsetType = otSingle) or
              (SelectOffsetData.OffsetType = otDouble) then
              InsPoly := New(PCPoly, Init)
            else
              InsPoly := New(PPoly, Init);
            if NewPoly^.GetPoly(PInfo, InsPoly, TRUE) then
            begin
              OffsetItem := InsPoly;
              if DBSelection^.Selecting and (DBSelection^.SOffset <> 0) then
              begin
                DBSelection^.CorrectDir := TRUE;
                DBSelection^.ContinueDBSelection
              end
              else
              begin
                InpOffset^.SetPoly(PPoly(OffsetItem));
                InpOffset^.StartIt(PInfo, DrawPos, FALSE);
                InpOffset^.MouseMove(PInfo, DrawPos);
                SetActualMenu(mn_OffsetForSel);
              end;
            end
            else
            begin
              NewPoly^.EndIt(PInfo);
              Dispose(InsPoly, Done);
            end
          end;
      end;
    mn_RefBitmap:
      begin
        if (RefPtCnt > 1) and
          ((WinGISMainForm.TurboRasterDDE^.CommVersion > 1) or
          not WinGISMainForm.TurboRasterDDE^.IsConnected) then
        begin
          ClearStatusText;
          RefPtCnt := RefPtCnt - 1;
          GeoReference^[0].x := RefPtCnt;
          GeoReference^[0].y := 0;
          if (WinGISMainForm.TurboRasterDDE^.IsConnected) then
          begin
            WinGISMainForm.TurboRasterDDE^.SendRefPoints(GeoReference);
            FreeMem(GeoReference, sizeof(LPOINT) * 128);
            GeoReference := nil;
          end
                         {else DDEMLServer^.SendRefPoints(GeoReference);}
          else
            DDEHandler.SendRefPoints(GeoReference);
          SetActualMenu(mn_Select);
          RefPtCnt := 0;
        end;
      end;
{$ENDIF}
  end;
{$IFNDEF WMLT}
  SendClickInfo(DrawPos.x, DrawPos.y, Integer(mbLeft), ActualMen, 1);
{$ENDIF}
{$ENDIF} // <----------------- AXDLL
end;

procedure TProj.MsgMouseMove
  (
  DrawPos: TDPoint
  );
var
  NDist: Double;
  NPoint: TDPoint;
  ADist: Double;
  BDist: Double;
  XM: LongInt;
  YM: LongInt;
  Temp: Integer;
  Point1: TDPoint;
  SnapObject: PIndex;
  AObjType: TObjectTypes;
  AStartPoint: TDPoint;
  AStartPointIndex: Integer;
begin
{++ Ivanoff NA2 BUILD#106}
  SELF.AOldMousePos.X := DrawPos.X;
  SELF.AOldMousePos.Y := DrawPos.Y;
{-- Ivanoff}
{++Brovak}
  NewPolyP^.EndIt(Pinfo);
{--Brovak}
  case ActualMen of
    mn_Zoom:
      begin
        ZoomRect^.MouseMove(PInfo, DrawPos);
        if ZoomRect^.IsBigEnough(PInfo, MinZoom, MinZoom, TRUE) then
        begin
          KillTimer(Parent.Handle, idt_Mouse);
          if not ZoomCursor then
            PInfo^.SetTheCursor(idc_Zoom);
          ZoomCursor := TRUE;
        end
        else
        begin
          if ZoomCursor then
            PInfo^.SetTheCursor(crArrow);
          ZoomCursor := FALSE;
        end;
      end;
    mn_Select:
      begin
        SelRect^.MouseMove(PInfo, DrawPos);
        if SelRect^.IsItOn then
        begin
          if SelRect^.IsBigEnough(PInfo, MinSel, MinSel, FALSE) then
            SetCursor(idc_Rect)
          else
            SetCursor(SelectCursor);
        end;
      end;
      {+++brovak #MOD for copying part of Image}
    mn_CutImage, mn_SuperImage:
      begin;
        if ActualMen = mn_CutImage then
          Temp := 3
        else
          Temp := 2;
        if CSelectForCut = Temp then
        begin;
          SelRect^.MouseMove(PInfo, DrawPos);
          if SelRect^.IsItOn then
          begin
            if SelRect^.IsBigEnough(PInfo, MinSel, MinSel, FALSE) then
              SetCursor(idc_Rect)
            else
              SetCursor(SelectCursor);
          end;
        end;
      end;
       {--brovak}

//++ Glukhov LoadImageInRect BUILD#123 20.07.00
    mn_SetImageRect:
      begin;
        if RTrafoCnt = 1 then
        begin;
          SelRect^.MouseMove(PInfo, DrawPos);
          if SelRect^.IsItOn then
          begin
            if SelRect^.IsBigEnough(PInfo, MinSel, MinSel, FALSE) then
              SetCursor(idc_Rect)
            else
              SetCursor(SelectCursor);
          end;
        end;
      end;
//-- Glukhov LoadImageInRect BUILD#123 20.07.00
    mn_DrawSymLine, mn_Distance:
      begin
        if SnapPoint(DrawPos, 0, ot_Any, Point1, SnapObject) then
        begin
          NewPoly^.MouseMove(PInfo, Point1);
        end;
      end;
{$IFNDEF WMLT}
    mn_MovePartPuff,
      mn_MovePartPara:
      begin
        MovePoly^.MouseMove(PInfo, DrawPos);
        if MovePoly^.IsItOn then
        begin
          if MovePoly^.IsEnoughToMove(PInfo, MinMove, MinMove) then
          begin
            SetCursor(idc_Move);
            MovePoly^.GetMove(XM, YM);
            PolyPart.CalcStateMoveDist(XM, YM);
          end
          else
            SetCursor(crDefault);
        end;
      end;
    mn_MovePoly,
      mn_MoveSym:
      begin
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
        MovePoly^.MouseMove(PInfo, DrawPos);
        if MovePoly^.IsItOn then
          if MovePoly^.IsEnoughToMove(PInfo, MinMove, MinMove) then
            SetCursor(idc_Move)
          else
            SetCursor(crDefault);
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
      end;
    mn_MoveCircle:
      begin
        MoveCircle^.MouseMove(PInfo, DrawPos);
        if MoveCircle^.IsItOn then
        begin
          if MoveCircle^.IsEnoughToMove(PInfo, MinMove, MinMove) then
            SetCursor(idc_Move)
          else
            SetCursor(crDefault);
        end;
      end;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
{    mn_MoveMulti:
      begin
        MoveMulti^.MouseMove(PInfo, DrawPos);
        if MoveMulti^.IsItOn then
        begin
          if MoveMulti^.IsEnoughToMove(PInfo, MinMove, MinMove) then
            SetCursor(idc_Move)
          else
            SetCursor(crDefault);
        end;
      end;   }
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
    mn_Text,
      mn_DBText,
      mn_SizeRot:
      begin
        TextRect^.MouseMove(PInfo, DrawPos);
          {+++brovak BUG80 BUILD#127}
        if InputTextFlag = false then
          {--brovak}
          SetStatusText(Format(GetLangText(207), [Abs(TextRect^.Height / 100), TextRect^.GetDisplayAngle]), FALSE);
      end;
    mn_GenerRect: SelRect^.MouseMove(PInfo, DrawPos);
    mn_PartFree,
      mn_PartParallel,
      mn_CParallel,
      mn_CBufferDraw,
      mn_CIslands,
//      mn_Poly,
{++ Ivanoff NA2 BUILD#106}
    mn_Poly,
      mn_EntToDrawAlong, // Glukhov Bug#468 Build#162 16.08.01
      mn_CPoly:
      begin
                  {+++Brovak. changed hardly for bug 104 build 132}
        if ((Self.bDrawRubberBox = true) or (Self.bDraw3Points = true))
          and (ActualMen = mn_Cpoly) then //Construct rectangle mode
        begin; //Correct for with Shift Mode

          if not SnapState then
          begin
            if (GetKeyState(vk_shift) < 0) and (NewPoly.Data.Count > 0) then
              GetNewDrawPos(DrawPos);
            if (GetKeyState(vk_control) < 0) and (NewPoly.Data.Count > 1) then
              GetNewDrawPos1(DrawPos);
          end;

          if Self.bDrawRubberBox = true then
          begin
            RubberBoxDraw(@SELF, DrawPos); //RubberBox Move
            if ActualMen = mn_CPoly then
            begin
              ClearStatusText;
              if SelRect^.IsOn then
              begin
                SetStatusText(GetLangText(11370) + ' ' + FormatStr(((0.0 + Abs(SelRect^.StartPos.x - SelRect.LastPos.x) * (0.0 + Abs(SelRect^.StartPos.y - SelRect.LastPos.y)))) / 100 / IniFile^.AreaFact, 2) + IniFile^.GetAreaUnit, True);
              end;
            end
          end
          else
            ThreePointsDraw(@SELF, DrawPos) //Move Pseudo RubberBox - 3Points mode
        end
        else
        begin; //traditional or ortho drawing
                  {++brovak  BUG#104 BUILD 130}

          if not SnapState then
          begin
            if (GetKeyState(vk_shift) < 0) then
              GetNewDrawPos(DrawPos);
            if (GetKeyState(vk_control) < 0) and (NewPoly.Data.Count > 1) then
              GetNewDrawPos1(DrawPos);
          end;
                  {--brovak}

                                  {+++Brovak. changed hardly for bug 104 build 132}
          SnapObject := nil;
          if (GetKeyState(vk_control) < 0) then
          begin
            AObjType := [ot_Poly, ot_CPoly];
          end
          else
          begin
            AObjType := ot_Any;
          end;
          if SnapPoint(DrawPos, 0, AObjType, Point1, SnapObject) then
          begin
            if (not NewPoly^.bDrawOrtho) then
            begin
              if (GetKeyState(vk_control) < 0) and (SnapObject <> nil) and (SnapObject^.GetObjType in [ot_Poly, ot_CPoly]) then
              begin

                AStartPoint.Init(NewPoly^.LastSnapPos.X, NewPoly^.LastSnapPos.Y);
                AStartPointIndex := PPoly(SnapObject)^.FindIndexOfPoint(AStartPoint);
                if AStartPointIndex > -1 then
                begin
                  CurSnapPoly^.SnapStart := AStartPointIndex;
                  NewPoly^.SnapObject := SnapObject;
                  CurSnapPoly := PPoly(SnapObject);
                end;

                if NewPoly^.SnapObject = SnapObject then
                begin
                  if CurSnapPoly^.SnapEnd <> CurSnapPoly^.FindIndexOfPoint(Point1) then
                  begin
                    CurSnapPoly^.SnapEnd := CurSnapPoly^.FindIndexOfPoint(Point1);
                    if (CurSnapPoly^.SnapEnd > -1) then
                    begin
                      NewPoly^.DrawAlong(PInfo, CurSnapPoly);
                    end;
                  end;
                end
                else
                begin
                  if CurSnapPoly <> nil then
                  begin
                    CurSnapPoly^.SnapEnd := -1;
                  end;
                  NewPoly^.Reset(PInfo);
                end
              end
              else
              begin
                if CurSnapPoly <> nil then
                begin
                  CurSnapPoly^.SnapEnd := -1;
                end;
                NewPoly^.Reset(PInfo);
              end;
            end;
          end;
          NewPoly^.MouseMove(PInfo, Point1);

               //ClearStatusText;
          if (NewPoly^.bDrawOrtho) and (NewPoly^.Data^.Count > 1) then
          begin
            SetStatusText(Format(GetLangText(208), [NewPoly^.GetLastOrthoDist(PInfo) / 100]), False);
          end;

{               if (GetKeyState (vk_shift) < 0) then begin
                 if SnapPoint(DrawPos, 0, [ot_Poly,ot_CPoly], Point1, SnapObject) then begin
                  if (SnapObject <> nil) and (CurSnapPoly <> nil) then begin
                     if SnapObject = NewPoly^.SnapObject then begin
                        if CurSnapPoly^.FindIndexOfPoint(Point1) <> CurSnapPoly^.SnapEnd then begin
                           NewPoly^.Reset(PInfo);
                           CurSnapPoly^.SnapEnd:=CurSnapPoly^.FindIndexOfPoint(Point1);
                           //NewPoly^.DrawAlong(PInfo,CurSnapPoly);
                        end;
                     end;
                  end;
                 end;
               end
               else begin
                  NewPoly^.Reset(PInfo);
                  NewPoly^.MouseMove (PInfo, DrawPos);
               end;
}
        end;
      end;
                {--Brovak}
{-- Ivanoff}
{$ENDIF}
    mn_MesPoint: Line^.MouseMove(PInfo, DrawPos);
    mn_Move,
      mn_Snap: Line^.MouseMove(PInfo, DrawPos);
    mn_SnapPoly,
      mn_InsPOK:
      begin
        if GetKeyState(VK_CONTROL) < 0 then
        begin
          GetNewDrawPos2(DrawPos);
          Lines^.MouseMove(PInfo, DrawPos);
          if Lines^.IsOn then
          begin
            MarkSnapPoint(DrawPos, True);
          end;
        end
        else
        begin
          Lines^.MouseMove(PInfo, DrawPos);
          if Lines.IsOn then
          begin
            MarkSnapPoint(DrawPos);
          end;
        end;
      end;
    mn_OffsetForSel:
      begin
        NewPoly^.DrawReally := TRUE;
        NewPoly^.Draw(PInfo, TRUE);
        NewPoly^.DrawReally := FALSE;
        InpOffset^.MouseMove(PInfo, DrawPos);
        SetStatusText(Format(GetLangText(208), [InpOffset^.AktDist / 100]), FALSE);
      end;
    mn_OffsetOK:
      begin
        if SnapPoint(DrawPos, 0, ot_Any, Point1, SnapObject) then
        begin
          InpOffset^.MouseMove(PInfo, Point1);
        end;
        SetStatusText(Format(GetLangText(208), [InpOffset^.AktDist / 100]), FALSE);
      end;
    mn_SplitLine:
      begin
        InpOffset^.MouseMove(PInfo, DrawPos);
        CalculateSplitLength;
      end;
    mn_SymbolInp:
      begin
        SymbolInp^.MouseMove(PInfo, DrawPos);
        SetStatusText(Format(GetLangText(209), [SymbolInp^.Scaling,
          (SymbolInp^.AktAngle + SymbolInp^.RotAngle) * Rad2Deg + 360 mod 360]), FALSE);
      end;
    mn_SplineEdit: EditSpline^.MouseMove(PInfo, DrawPos);
    mn_EditImage,
//++ Glukhov Bug#264 Build#161 15.06.01
//    mn_EditTxtRect, // Glukhov Bug#170 BUILD#125 03.11.00
//-- Glukhov Bug#264 Build#161 15.06.01
    mn_EditOLE: InputBox^.MouseMove(PInfo, DrawPos);
//++ Glukhov Bug#264 Build#161 15.06.01
    mn_EditTxtRect: EditTxtBox^.MouseMove(PInfo, DrawPos);
//-- Glukhov Bug#264 Build#161 15.06.01
    mn_Combine,
      mn_CombineSel,
      mn_CombineSelOne: NewPoly^.MouseMove(PInfo, DrawPos);
    mn_DrawForSelect:
      case SelectData.SelectWith of
        swCircle:
          begin
            SelCircle^.MouseMove(PInfo, DrawPos);
            if SelCircle^.IsOn then
              SetStatusText(Format(GetLangText(205), [SelCircle^.GetRadius / 100]), FALSE);
          end;
        swLine,
          swOffset,
          swPoint,
          swPoly: NewPoly^.MouseMove(PInfo, DrawPos);
        swRect: SelRect^.MouseMove(PInfo, DrawPos);
      end;
    mn_DBSelCircle:
      begin
        SelCircle^.MouseMove(PInfo, DrawPos);
        if SelCircle^.IsOn then
          SetStatusText(Format(GetLangText(205), [SelCircle^.GetRadius / 100]), FALSE);
      end;
    mn_DBSelRect: SelRect^.MouseMove(PInfo, DrawPos);
    mn_InsertOLEObject,
      mn_InsLinkObj,
      mn_PasteOLEObj: OLERect^.MouseMove(PInfo, DrawPos);

    mn_LineMark:
      case MarkState of
        1:
          begin
            Line^.MouseMove(PInfo, DrawPos);
            SetStatusText(Format(GetLangText(11204), [PInfo^.ConvAngle(360 - AngleDifference(Line^.StartPos.CalculateAngle(Line^.LastPos), PI / 2) * 180 / Pi, True)]), FALSE);
          end;
        2:
          begin
            PInfo^.NormalDistance(@Line^.StartPos, @Line^.LastPos, @DrawPos, NDist, NPoint);
            ADist := 0.0;
            if Abs(Line^.StartPos.CalculateAngle(NPoint) - Line^.StartPos.CalculateAngle(Line^.LastPos)) > 1 then
              BDist := (-Line^.StartPos.Dist(NPoint) - ADist) / 100.0
            else
              BDist := (Line^.StartPos.Dist(NPoint) - ADist) / 100.0;
            if CCW(Line^.StartPos, Line^.LastPos, DrawPos) < 0 then
              NDist := -NDist;
            SetStatusText(Format(GetLangText(11205), [NDist / 100.0]), FALSE);
            Line1^.MoveIt(PInfo, NPoint, DrawPos);
          end;
        4:
          begin
            Line2^.MouseMove(PInfo, DrawPos);
            SetStatusText(Format(GetLangText(11204), [PInfo^.ConvAngle(360 - AngleDifference(Line^.StartPos.CalculateAngle(Line2^.LastPos), PI / 2) * 180 / Pi, True)]), FALSE);
          end;
        5:
          begin
            PInfo^.NormalDistance(@Line2^.StartPos, @Line2^.LastPos, @DrawPos, NDist, NPoint);
            ADist := 0.0;
            if Abs(Line2^.StartPos.CalculateAngle(NPoint) - Line2^.StartPos.CalculateAngle(Line2^.LastPos)) > 1 then
              BDist := (-Line2^.StartPos.Dist(NPoint) - ADist) / 100.0
            else
              BDist := (Line2^.StartPos.Dist(NPoint) - ADist) / 100.0;
            if CCW(Line2^.StartPos, Line2^.LastPos, DrawPos) < 0 then
              NDist := -NDist;
            SetStatusText(Format(GetLangText(11205), [NDist / 100.0]), FALSE);
            Line3^.MoveIt(PInfo, NPoint, DrawPos);
          end;
      end;

    mn_OrthoMark,
      mn_DistMark:
      case MarkState of
        1:
          begin
            Line^.MouseMove(PInfo, DrawPos);
            SetStatusText(Format(GetLangText(11204), [PInfo^.ConvAngle(360 - AngleDifference(Line^.StartPos.CalculateAngle(Line^.LastPos), PI / 2) * 180 / Pi, True)]), FALSE);
          end;
        2:
          begin
            PInfo^.NormalDistance(@Line^.StartPos, @Line^.LastPos, @DrawPos, NDist, NPoint);
            if ((ActualMen = mn_DistMark)
              and (ConstructOpt.lMeasureFrom2nd))
              or ((ActualMen = mn_OrthoMark)
              and (ConstructOpt.oMeasureFrom2nd)) then
              ADist := Line^.StartPos.Dist(Line^.LastPos)
            else
              ADist := 0.0;
            if Abs(Line^.StartPos.CalculateAngle(NPoint) - Line^.StartPos.CalculateAngle(Line^.LastPos)) > 1 then
              BDist := (-Line^.StartPos.Dist(NPoint) - ADist) / 100.0
            else
              BDist := (Line^.StartPos.Dist(NPoint) - ADist) / 100.0;
            if ActualMen = mn_OrthoMark then
            begin
              if CCW(Line^.StartPos, Line^.LastPos, DrawPos) < 0 then
                NDist := -NDist;
              SetStatusText(Format(GetLangText(11206), [BDist, NDist / 100.0]), FALSE);
            end
            else
              SetStatusText(Format(GetLangText(11205), [BDist]), FALSE);
            Line1^.MoveIt(PInfo, NPoint, DrawPos);
          end;
      end;
    mn_CircleCutMark:
      case MarkState of
        1: Line^.MouseMove(PInfo, DrawPos);
        2:
          begin
            SelCircle^.MouseMove(PInfo, DrawPos);
            SetStatusText(Format(GetLangText(11220), [SelCircle^.GetRadius / 100]), FALSE);
          end;
        3:
          begin
            InpCircle^.MouseMove(PInfo, DrawPos);
            SetStatusText(Format(GetLangText(11221), [InpCircle^.GetRadius / 100]), FALSE);
          end;
      end;

    mn_CircleMark:
      case MarkState of
        1:
          begin
            SelCircle^.MouseMove(PInfo, DrawPos);
            Line^.MouseMove(PInfo, DrawPos);
            SetStatusText(Format(GetLangText(11222), [SelCircle^.GetRadius / 100]), FALSE);
          end;
        2:
          begin
            ProcCalcPointOnCircle(@Self, DrawPos, DrawPos);
            Line^.MouseMove(PInfo, DrawPos);
            SetStatusText(Format(GetLangText(11223), [SelCircle^.GetRadius / 100, PInfo^.ConvAngle(360 - AngleDifference(Line^.StartPos.CalculateAngle(Line^.LastPos), PI / 2) * 180 / Pi, True)]), FALSE);
          end;
        3:
          begin
            Line^.MouseMove(PInfo, DrawPos);
            SelCircle^.MouseMove(PInfo, DrawPos);
          end;
      end;

    mn_AngleMark:
      case MarkState of
        1:
          begin
            Line^.MouseMove(PInfo, DrawPos);
            SetStatusText(Format(GetLangText(11218), [Line^.StartPos.Dist(Line^.LastPos) / 100,
              PInfo^.ConvAngle(360 - AngleDifference(Line^.StartPos.CalculateAngle(Line^.LastPos), PI / 2) * 180 / Pi, True)]), FALSE);
          end;
        2:
          begin
            Line1^.MouseMove(PInfo, DrawPos);
            SetStatusText(Format(GetLangText(11237),
              [PInfo^.ConvAngle(
                360 - AngleDifference(Line1^.StartPos.CalculateAngle(Line1^.LastPos),
                Line^.StartPos.CalculateAngle(Line^.LastPos)
                ) * 180 / Pi, True
                ),
              PInfo^.ConvAngle(
                360 - AngleDifference(Line1^.StartPos.CalculateAngle(Line1^.LastPos),
                Pi / 2
                ) * 180 / Pi, True
                )]), FALSE);
          end;
        3:
          begin
            Line2^.MouseMove(PInfo, DrawPos);
            SetStatusText(Format(GetLangText(11238),
              [PInfo^.ConvAngle(
                360 - AngleDifference(Line2^.StartPos.CalculateAngle(Line2^.LastPos),
                Line^.LastPos.CalculateAngle(Line^.StartPos)
                ) * 180 / Pi, True
                ),
              PInfo^.ConvAngle(
                360 - AngleDifference(Line2^.StartPos.CalculateAngle(Line2^.LastPos),
                Pi / 2
                ) * 180 / Pi, True
                )]), FALSE);
          end;
      end;
  end;
end;

procedure TProj.SetLayerPriority(DDEString: AnsiString = '');
begin
  if ActualMen = mn_OffsetOk then
    Exit;
  if Layers^.SetLayerPriority(@Self, DDEString) then
  begin
    PInfo^.RedrawScreen(TRUE);
    SetModified;
    RefreshSymbolMan(False);
    TooltipThread.StartFloatingText(@Self);
  end;
  CreateLayerManagerView;
end;

procedure TProj.CreateLayerManagerView;
var
  AIndex: Integer;
  ASaveSight: PSight;
  ASight: PSight;
  AllLayersViewName: AnsiString;

  function HasSaveSight(Item: PSight): Boolean; far;
  begin
    Inc(AIndex);
    Result := Item^.Name^ = AllLayersViewName;
  end;

begin
  AllLayersViewName := 'Layer Manager';
  AIndex := -1;
  ASaveSight := Sights^.FirstThat(@HasSaveSight);
  if ASaveSight <> nil then
    Sights^.AtDelete(AIndex);
  ASight := New(PSight, Init(AllLayersViewName, RotRect(0, 0, 0, 0, 0)));
  ASight^.SetLayers(Layers);
  Sights^.Insert(ASight);
end;

procedure TProj.MsgLeftUp
  (
  DrawPos: TDPoint
  );
{$IFNDEF AXDLL} // <----------------- AXDLL
var
  SelectRect: TDRect;
  XM: LongInt;
  YM: LongInt;
  Point: TDPoint;
  Rect: TDRect;
  SnapObject: PIndex;
//  Border: PCPoly;
  YCount: Integer;
//  APtr: Pointer;
{$ENDIF} // <----------------- AXDLL
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}
  SnapObject := nil;
  case ActualMen of
    mn_Select:
      if SelRect^.IsOn then
      begin
        SelRect^.EndIt(PInfo);
        SetCursor(SelectCursor);
        if SelRect^.IsBigEnough(PInfo, MinSel, MinSel, FALSE) then
        begin
          SelRect^.GetRect(SelectRect);
{++ Ivanoff}
          if WinGisMainForm.WeAreUsingTheIDBUserInterface[@SELF]
            and
            WinGISMainForm.WeAreUsingTheIDBInfoWindow then
            DeSelectAll(FALSE);
{-- Ivanoff}
          if SelectByRect(SelectRect, PInfo^.SelectInside, ObjTypeFilter) then
          begin
            ProcDBSendUserSelection;
          end;
{++ Ivanoff}
          if (WinGisMainForm.WeAreUsingTheIDBUserInterface[@SELF]) and (WinGISMainForm.WeAreUsingTheIDBInfoWindow) then
            if SELF.Layers.SelLayer.Data.GetCount >= 1 then
              WinGISMainForm.IDB_Man.ShowInfoTable
        (*brovak     Else
                WinGISMainForm.IDB_Man.HideInfoTable;
{-- Ivanoff}    brovak*)
        end
        else
        begin
//          if not(SnapState) then begin
          if IniFile^.DoOneSelect then
            DeselectAll(TRUE);
{++ Ivanoff}
          if WinGisMainForm.WeAreUsingTheIDBUserInterface[@SELF]
            and
            WinGISMainForm.WeAreUsingTheIDBInfoWindow then
            DeSelectAll(FALSE);
{-- Ivanoff}
          SnapObject := SelectByPoint(DrawPos, ObjTypeFilter);
          if SnapObject <> nil then
          begin
            ProcDBSendUserSelection;
          end;
{++ Ivanoff}
          if (WinGisMainForm.WeAreUsingTheIDBUserInterface[@SELF]) and (WinGISMainForm.WeAreUsingTheIDBInfoWindow) then
            if SELF.Layers.SelLayer.Data.GetCount >= 1 then
              WinGISMainForm.IDB_Man.ShowInfoTable;
(*brovak             Else
                WinGISMainForm.IDB_Man.HideInfoTable

                   ; brovak*)
{-- Ivanoff}
          YCount := 0;
          if Aheapmanager <> nil then
          begin
            if Document.Loaded then
              YCount := TGlobal(Aheapmanager.GlobalObj).SelLst.SelectCount;
          end;
          if (IniFile^.DoOneSelect) or (MenuFunctions['DatabaseAutoMonitoring'].Checked) then
          begin
            if (Layers^.SelLayer^.Data^.GetCount > 0) or (YCount > 0) then
            begin
//                if DDEHandler.FOneSelect then DDEHandler.SetDBSendStrings(DDECommOSel,NIL);
              ProcDBMonitoring(@Self);
//                if DDEHandler.FOneSelect then DDEHandler.SetDBSendStrings(DDECommAll,NIL);
            end;
          end;
//          end
//          else begin
//            Point.Init(0,0);
//            if ProcSnapObjects(@Self,DrawPos,0,ot_Any,Point,SnapObject) then
//              if SnapObject <> NIL then ProcSelDeselIndex(@Self,SnapObject);
//          end;
        end;
      end;
    mn_MovePartPara:
      begin
        MovePoly^.EndIt(PInfo);
        if MovePoly^.IsEnoughToMove(PInfo, MinMove, MinMove) then
        begin
          MovePoly^.GetMove(XM, YM);
          PolyPart.SetMoveDist(XM, YM);
        end
        else
        begin
          PolyPartitionDlg.CalcButtonClick(nil);
        end;
        ClearStatusText;
        SendMessage(Parent.Handle, wm_Command, cm_ShowPartDlg, 0);
      end;
    mn_MovePoly:
      begin
        MovePoly^.EndIt(PInfo);
        if MovePoly^.IsEnoughToMove(PInfo, MinMove, MinMove) then
        begin
          MovePoly^.GetMove(XM, YM);
          if SnapItem^.GetObjType = ot_Text then
          begin
            Point.Init(0, 0);
            Point := PText(SnapItem)^.Pos;
            Point.Move(XM, YM);
            if SnapPoint(Point, 0, ot_Any, Point, SnapObject) then
            begin
              MovePoly^.GetMove(XM, YM);
              SnapItem^.Invalidate(PInfo);
              PText(SnapItem)^.Pos := Point;
              PText(SnapItem)^.CalculateClipRect(PInfo);
              UpdateClipRect(PView(SnapItem));
              SnapItem^.Invalidate(PInfo);
              if PInfo^.IsMU then
              begin
                MU.LogUpdObjectData(PInfo, Layers^.TopLayer, PView(SnapItem));
              end;
              if not CorrectSize(PView(SnapItem)^.ClipRect, TRUE) then
                PInfo^.RedrawInvalidate;
              ProcDBSendObject(@Self, SnapItem, 'UPD');
              if WinGisMainForm.WeAreUsingTheIDB[@self] then
                WinGISMainForm.IDB_Man.UpdateCalcFields(@self, Layers^.TopLayer, SnapItem^.Index);
              SetModified;
            end;
          end
          else
            if SnapItem^.GetObjType = ot_RText then
            begin
              Rect.Init;
              Rect := PRText(SnapItem)^.TextRect;
              Rect.MoveRel(XM, YM);
              Point.Init(Rect.A.X, Rect.A.Y);
              if SnapPoint(Point, 0, ot_Any, Point, SnapObject) then
              begin
                MovePoly^.GetMove(XM, YM);
                SnapItem^.Invalidate(PInfo);
                PRText(SnapItem)^.TextRect := Rect;
                PRText(SnapItem)^.CalculateClipRect(PInfo);
                UpdateClipRect(PView(SnapItem));
                SnapItem^.Invalidate(PInfo);
                if not CorrectSize(PView(SnapItem)^.ClipRect, TRUE) then
                  PInfo^.RedrawInvalidate;
                SetModified;
              end;
            end
            else
              if SnapItem^.GetObjType = ot_BusGraph then
              begin
                Point.Init(0, 0);
                Point := PBusGraph(SnapItem)^.Position;
                Point.Move(XM, YM);
                if SnapPoint(Point, 0, ot_Any, Point, SnapObject) then
                begin

                  ProcMoveRelSelected(@Self, XM, YM);

                  MovePoly^.GetMove(XM, YM);
                  SnapItem^.Invalidate(PInfo);
                  PBusGraph(SnapItem)^.Position := Point;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
//--          PBusGraph(SnapItem)^.CalculateClipRect(PInfo);
                  PBusGraph(SnapItem)^.CalculateClipRect;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
                  UpdateClipRect(PView(SnapItem));
                  SnapItem^.Invalidate(PInfo);
                  if not CorrectSize(PView(SnapItem)^.ClipRect, TRUE) then
                    PInfo^.RedrawInvalidate;
                  SetModified;
                end;
                PInfo^.RedrawRect(SnapItem^.ClipRect);
              end;
          SelAllRect.CorrectByRect(PView(SnapItem)^.ClipRect);
          if WinGisMainForm.WeAreUsingTheIDB[@self] then
            WinGISMainForm.IDB_Man.UpdateCalcFields(@self, Layers^.TopLayer, SnapItem^.Index);
        end
        else
          SelectByPoint(DrawPos, ot_Any);
        SetActualMenu(mn_Select);
      end;
    mn_MoveCircle:
      begin
        MoveCircle^.EndIt(PInfo);
        if MoveCircle^.IsEnoughToMove(PInfo, MinMove, MinMove) then
        begin
          MoveCircle^.GetMove(XM, YM);
          if SnapItem^.GetObjType = ot_BusGraph then
          begin
            Point.Init(0, 0);
            Point := PBusGraph(SnapItem)^.Position;
            Point.Move(XM, YM);
            if SnapPoint(Point, 0, ot_Any, Point, SnapObject) then
            begin
              MoveCircle^.GetMove(XM, YM);
              SnapItem^.Invalidate(PInfo);
              PBusGraph(SnapItem)^.Position := Point;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
//--              PBusGraph(SnapItem)^.CalculateClipRect(PInfo);
              PBusGraph(SnapItem)^.CalculateClipRect;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
              UpdateClipRect(PView(SnapItem));
              SelAllRect.CorrectByRect(PView(SnapItem)^.ClipRect);
              SnapItem^.Invalidate(PInfo);
              if not CorrectSize(PView(SnapItem)^.ClipRect, TRUE) then
                PInfo^.RedrawInvalidate;
              SetModified;
            end;
          end;
          if WinGisMainForm.WeAreUsingTheIDB[@self] then
            WinGISMainForm.IDB_Man.UpdateCalcFields(@self, Layers^.TopLayer, SnapItem^.Index);
        end
        else
        begin
          SelectByPoint(DrawPos, ot_Any);
          SnapItem^.Invalidate(PInfo);
          if not CorrectSize(PView(SnapItem)^.ClipRect, TRUE) then
            PInfo^.RedrawInvalidate
          else
            PInfo^.RedrawRect(SnapItem^.ClipRect);
        end;
        SetActualMenu(mn_Select);
      end;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
{    mn_MoveMulti:
      begin
        MoveMulti^.EndIt(PInfo);
        if MoveMulti^.IsEnoughToMove(PInfo, MinMove, MinMove) then
        begin
          MoveMulti^.GetMove(XM, YM);
          if SnapItem^.GetObjType = ot_BusGraph then
          begin
            Point.Init(0, 0);
            Point := PBusGraph(SnapItem)^.Position;
            Point.Move(XM, YM);
            if SnapPoint(Point, 0, ot_Any, Point, SnapObject) then
            begin
              MoveMulti^.GetMove(XM, YM);
              SnapItem^.Invalidate(PInfo);
              PBusGraph(SnapItem)^.Position := Point;
//--          PBusGraph(SnapItem)^.CalculateClipRect(PInfo);
              PBusGraph(SnapItem)^.CalculateClipRect;
              UpdateClipRect(PView(SnapItem));
              SelAllRect.CorrectByRect(PView(SnapItem)^.ClipRect);
              SnapItem^.Invalidate(PInfo);
              if not CorrectSize(PView(SnapItem)^.ClipRect, TRUE) then PInfo^.RedrawInvalidate;
              SetModified;
            end;
          end;
        end
        else
          SelectByPoint(DrawPos, ot_Any);
        SetActualMenu(mn_Select);
      end;   }
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
    mn_MoveSym:
      begin
        MovePoly^.EndIt(PInfo);
        if MovePoly^.IsEnoughToMove(PInfo, MinMove, MinMove) then
        begin
// ++ Cadmensky
{++ IDB_UNDO}
{$IFNDEF WMLT}
          // A point is inserted in any object (polyline or polygon)
          // or any point of this object has been moved by an user.
          if WinGisMainForm.WeAreUsingUndoFunction[@SELF] and
            WinGisMainForm.UndoRedo_Man.AddObjectInUndoData(@SELF, PView(SnapItem)) then
            WinGisMainForm.UndoRedo_Man.SaveUndoData(@SELF, utUnMove);
{$ENDIF}
{-- IDB_UNDO}
// -- Cadmensky
          MovePoly^.GetMove(XM, YM);
          Point.Init(0, 0);
          Point := PSymbol(SnapItem)^.Position;
          Point.Move(XM, YM);
          if SnapPoint(Point, 0, ot_Any, Point, SnapObject) then
          begin
            PInfo^.LineWidth := Max(Round(Layers^.TopLayer^.LineStyle.Width),
              Round(IniFile^.SelectionSettings.LineStyle.Width)); {!?!?!?}
            SnapItem^.Invalidate(PInfo);
            PSymbol(SnapItem)^.Position := Point;
            PSymbol(SnapItem)^.CalculateClipRect(PInfo);
            SnapItem^.Invalidate(PInfo);
            if not CorrectSize(PView(SnapItem)^.ClipRect, TRUE) then
              PInfo^.RedrawInvalidate;
            UpdateClipRect(PSymbol(SnapItem));
            SelAllRect.CorrectByRect(PView(SnapItem)^.ClipRect);
            if PInfo^.IsMU then
            begin
              MU.LogUpdObjectData(PInfo, Layers^.TopLayer, PView(SnapItem));
            end;
            ProcDBSendObject(@Self, SnapItem, 'UPD');
            SetModified;
          end;
        end
        else
          SelectByPoint(DrawPos, ot_Any);
        SetActualMenu(mn_Select);
      end;
    mn_SizeRot:
      if not TextRect^.IsItOn then
        ActualMen := mn_Select;
    mn_Text,
      mn_DBText:
      if not TextRect^.IsItOn then
        SetActualMenu(mn_Select);
    mn_SymbolInp:
      if not SymbolInp^.IsItOn then
        ActualMen := mn_Select;
    mn_SplineEdit: EditSpline^.MouseUp;
    mn_EditImage,
//++ Glukhov Bug#264 Build#161 15.06.01
//    mn_EditTxtRect, // Glukhov Bug#170 BUILD#125 03.11.00
//-- Glukhov Bug#264 Build#161 15.06.01
    mn_EditOLE: InputBox^.MouseUp(PInfo);
//++ Glukhov Bug#264 Build#161 15.06.01
    mn_EditTxtRect: EditTxtBox^.MouseUp(PInfo);
//-- Glukhov Bug#264 Build#161 15.06.01
  end;
{$ENDIF}
{$ENDIF} // <----------------- AXDLL
end;

{**************************************************************************************}
{ Function TProj.MsgLeftClick2                                                         }
{  Wenn TRUE zur�ckgegeben, dann wird beim n�chsten Mausclick wieder MsgLeftClick1     }
{  aufgerufen, ansonsten wieder MsgLeftClick2.                                         }
{**************************************************************************************}

function TProj.MsgLeftClick2
  (
  DrawPos: TDPoint
  )
  : Boolean;
{$IFNDEF AXDLL} // <----------------- AXDLL
var
  Middle: TDPoint;
  Radius: LongInt;
  XMove: LongInt;
  YMove: LongInt;
  Point1: TDPoint;
  Point2: TDPoint;
  Point3: TDPoint;
  NText: PText;
  Rect: TDRect;
  CutObj2: PPoly;
  SendStr: string;
  TempString: string;
  SnapObject: PIndex;
  AView: PView;
  NewOleRect: TDRect;

  ICount: integer; //brovak
//  CurLayer: PLayer;
{$ENDIF} // <----------------- AXDLL
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  MsgLeftClick2 := TRUE;
{$IFNDEF WMLT}
  SnapObject := nil;
  case ActualMen of
    mn_ScalePolyline:
      begin
        ClearStatusText;
        SetActualMenu(mn_Select);
        if ScalePolyObj <> nil then
        begin
          if SnapPoint(DrawPos, 0, ot_Any, Point1, SnapObject) then
          begin
            ScalePolyPos2.X := Point1.X;
            ScalePolyPos2.Y := Point1.Y;
            ProcScalePolyline;
          end;
        end;
        ScalePolyObj := nil;
      end;
    mn_Move:
      begin
        if SnapPoint(DrawPos, 0, ot_Any, Point3, SnapObject) then
        begin
          Line^.EndIt(PInfo);
          Line^.GetLine(Point1, Point2);
          XMove := LimitToLong(Point3.XDist(Point1));
          YMove := LimitToLong(Point3.YDist(Point1));
          ProcMoveRelSelected(@Self, XMove, YMove);
          SelAllRect.MoveRel(XMove, YMove);
          PInfo^.RedrawScreen(False);
          SetModified;
        end
        else
          MsgLeftClick2 := FALSE;
      end;
    mn_Snap:
      if SnapItem <> nil then
      begin
        Line^.GetLine(Point1, Point2);
        if SnapPoint(DrawPos, SnapItem^.Index, ot_Any, Point2, SnapObject) then
        begin
          NewOleRect.InitByRect(Self.SnapItem.ClipRect);
          Line^.EndIt(PInfo);
       //   SnapItem^.Invalidate(PInfo);
          XMove := LimitToLong(Point2.XDist(Point1));
          YMove := LimitToLong(Point2.YDist(Point1));
          AView := Layers^.IndexObject(PInfo, SnapItem);
          AView^.MoveRel(XMove, YMove);
          AView^.UpdateData;
     //     UpdateClipRect(AView);
   //       SnapItem^.ClipRect.InitByRect(AView^.ClipRect);
//          SnapItem^.Invalidate(PInfo);
          DeSelectAll(TRUE);
          RedrawWithAllAttached(@Self, PView(SnapItem), NewOleRect);
 //         PInfo^.RedrawInvalidate;
          SetModified;
        end;
        SnapItem := nil;
        DeSelectAll(TRUE);
        SetCursor(idc_Move);
      end;
    mn_SnapPoly,
      mn_InsPOK:
      if PointInd >= 0 then
      begin
        Lines^.GetLines(Point1);
        if SnapPoint(DrawPos, SnapItem^.Index, ot_Any, Point1, SnapObject) then
        begin
          Point2.Init(Lines^.StartPos.X, Lines^.StartPos.Y);
          Point3.Init(Lines^.EndPos.X, Lines^.EndPos.Y);

          if GetKeyState(VK_CONTROL) < 0 then
          begin
            GetNewDrawPos2(Point1);
          end;

          Lines^.EndIt(PInfo);

{++ IDB_UNDO}
{$IFNDEF WMLT}
          // A point is inserted in any object (polyline or polygon)
          // or any point of this object has been moved by an user.
          if (WinGisMainForm.WeAreUsingUndoFunction[@SELF]) and
            (WinGisMainForm.UndoRedo_Man.AddObjectInUndoData(@SELF, PView(SnapItem))) then
            WinGisMainForm.UndoRedo_Man.SaveUndoData(@SELF, utUnChange);
{$ENDIF}
{-- IDB_UNDO}
          Layers^.SelLayer^.SelInfo.Delete(PView(SnapItem), nil, nil);
          if ActualMen = mn_SnapPoly then
          begin

          {++ MOD by Brovak BUG 194 BUILD 135}

            if (GetKeyState(vk_shift) < 0) or (EditNeighboursMode) then //if Shift pressed
            begin;
              Point2.Init(PDPoint(PPoly(SnapItem)^.Data.At(PointInd)).X, PDPoint(PPoly(SnapItem)^.Data.At(PointInd)).Y);
                  // for All layers
              for ICount := 1 to Layers.Ldata.Count - 1 do
                if (PLayer(Layers.LData.At(ICount))^.UseForSnap) and
                  (not PLayer(Layers.LData.At(ICount))^.GetState(sf_Fixed)) and
                  (not PLayer(Layers.LData.At(ICount))^.GetState(sf_LayerOff)) then
                  PLayer(Layers.LData.At(ICount)).MovePointForAllNeigboursPoly(Pinfo, Point2, Point1);
            end
            else
            begin;
              NewOleRect.InitByRect(Self.SnapItem.ClipRect);
          //    PPoly(SnapItem)^.InvalidateTriple(PInfo, PointInd);
              PPoly(SnapItem)^.MovePoint(PointInd, Point1);
              PPoly(SnapItem)^.UpdateData;
           //   PPoly(SnapItem)^.InvalidateTriple(PInfo, PointInd);
           //   PInfo^.RedrawInvalidate;
              RedrawWithAllAttached(@Self, PView(SnapItem), NewOleRect);
               {--Brovak}
            end;
          end
          else
          begin
            if (GetKeyState(vk_shift) < 0) or (EditNeighboursMode) then
            begin //if Shift pressed
              for ICount := 1 to Layers.Ldata.Count - 1 do
              begin
                if (not PLayer(Layers.LData.At(ICount)).GetState(sf_Fixed)) and
                  (not PLayer(Layers.LData.At(ICount)).GetState(sf_LayerOff)) then
                begin
                  PLayer(Layers.LData.At(ICount)).InsertPointForAllNeigboursPoly(Pinfo, POint2, Point1, Point3);
                end;
              end;
            end
            else
            begin
             {+++ Brovak #463 Insert point after last or before first}
              if (SnapItem.GetObjType = ot_Poly) and (GetKeyState(vk_control) < 0) then
                if PointInd = 0 then
                  PointInd := -1
                else
                  if PointInd = PPoly(SnapItem)^.Data.Count - 1 then
                    PointInd := PPoly(SnapItem)^.Data.Count;
             {---brovak }
              NewOleRect.InitByRect(Self.SnapItem.ClipRect);
              PPoly(SnapItem)^.InsertPointByIndex(PointInd, Point1);
              RedrawWithAllAttached(@Self, PView(SnapItem), NewOleRect);
             // PPoly(SnapItem)^.InvalidateTriple(PInfo, PointInd);
            //  PInfo^.RedrawInvalidate;
            end;
          end;
          UpdateClipRect(PPoly(SnapItem));
          ProcDBSendObject(@Self, SnapItem, 'UPD');
          Layers^.SelLayer^.SelInfo.Add(PView(SnapItem));
          UserInterface.Update([uiStatusBar]);
          SelAllRect.CorrectByRect(PView(SnapItem)^.ClipRect);
          CorrectSize(PView(SnapItem)^.ClipRect, TRUE);

// ++ Cadmensky IDB Version 2.3.1
          if WinGisMainForm.WeAreUsingTheIDB[@SELF] then
// -- Cadmensky IDB Version 2.3.1
            WinGISMainForm.IDB_Man.UpdateCalcFields(@SELF, Layers^.TopLayer, SnapItem^.Index);

          PInfo^.RedrawScreen(False);
          SetModified;
          PointInd := -1;
          if ActualMen = mn_SnapPoly then
            SetCursor(idc_Move)
          else
            SetCursor(idc_Ins);
        end
        else
          MsgLeftClick2 := FALSE;
      end;
    mn_Text,
      mn_DBText:
      begin
        TextRect^.EndIt(PInfo);
        NText := New(PText, Init(ActualFont, ActualText, 0));
        TextRect^.GetText(PInfo, NText);
        if InsertObject(NText, FALSE) then
        begin
          if DDEHandler.DoAutoIns then
          begin
            ProcDBSendObject(@Self, NText, 'EIN');
          end;
        end;
        ClearStatusText;
      end;
    mn_GenerRect:
      begin
        SelRect^.EndIt(PInfo);
        SelRect^.GetRect(Rect);
{++ IDB}
{$IFNDEF WMLT}
        if WinGisMainForm.WeAreUsingTheIDB[@SELF] then
          WinGisMainForm.IDB_Man.SetIAmGenerateObjectsMyselfSign(@SELF, TRUE);
        if WinGisMainForm.WeAreUsingUndoFunction[@SELF] then
          WinGisMainForm.UndoRedo_Man.SetNecessitySaveUndoDataSign(@SELF, false);
        try
{$ENDIF}
{-- IDB}
          DoGenerate(@Self, Rect);
{++ IDB}
{$IFNDEF WMLT}
        finally
          if WinGisMainForm.WeAreUsingTheIDB[@SELF] then
          begin
            WinGisMainForm.IDB_Man.SetIAmGenerateObjectsMyselfSign(@SELF, FALSE);
          end;
{++ IDB_UNDO UnInsert}
          if WinGisMainForm.WeAreUsingUndoFunction[@SELF] then
          begin
            WinGisMainForm.UndoRedo_Man.SetNecessitySaveUndoDataSign(@SELF, true);
            WinGisMainForm.UndoRedo_Man.SaveUndoData(@SELF, utUnInsert);
          end;
{-- IDB_UNDO UnInsert}
        end;
{$ENDIF}
{-- IDB}
        SetActualMenu(mn_Select);
        SetModified;
      end;
    mn_MesPoint:
      if SnapPoint(DrawPos, 0, [ot_Pixel, ot_Poly, ot_CPoly, ot_Symbol], MesPoint2, SnapObject) and
        MesPoint1.IsDiff(MesPoint2) then
      begin
        BreakInput(TRUE, FALSE);
        ProcInsertMeasure(@Self, DrawPos);

        if (ssCtrl in GetShiftState) then
        begin
          MsgLeftClick2 := FALSE;
          ClickCnt := 1;
          Line^.StartIt(PInfo, MesPoint1, False);
        end;
      end
      else
        MsgLeftClick2 := FALSE;
    mn_LineCut:
      begin
        MsgLeftClick2 := FALSE;
        if TrimmOptions = to_FirstLine then
          CutObj2 := Pointer(SearchByPoint(DrawPos, [ot_Poly]))
        else
          if TrimmOptions = to_ToArea then
            CutObj2 := Pointer(SearchByPoint(DrawPos, [ot_CPoly]))
          else
            CutObj2 := Pointer(SearchByPoint(DrawPos, [ot_Poly]));
        if CutObj2 <> nil then
          MsgLeftClick2 := ProcTrimm(@Self, CutObj2, DrawPos);
      end;
    mn_DrawForSelect:
      if SnapPoint(DrawPos, 0, ot_Any, Point1, SnapObject) then
        case SelectData.SelectWith of
          swCircle:
            begin
              SelCircle^.MouseMove(PInfo, Point1);
              SelCircle^.GetCircle(Middle, Radius);
              if DBSelection^.Selecting then
                DBSelection^.ContinueDBSelection
              else
                SelectByCircle(Middle, Radius, SelectData.SelectTypes, SelectData.SelectInside);
              BreakInput(TRUE, FALSE);
            end;
          swRect:
            begin
              SelRect^.MouseMove(PInfo, Point1);
              SelRect^.GetRect(Rect);
              CutObj2 := New(PCPoly, Init);
              Point1.X := Rect.A.X;
              Point1.Y := Rect.A.Y;
              CutObj2^.InsertPoint(Point1);
              Point1.Y := Rect.B.Y;
              CutObj2^.InsertPoint(Point1);
              Point1.X := Rect.B.X;
              CutObj2^.InsertPoint(Point1);
              Point1.Y := Rect.A.Y;
              CutObj2^.InsertPoint(Point1);
              Point1.X := Rect.A.X;
              CutObj2^.InsertPoint(Point1);
              if DBSelection^.Selecting then
                DBSelection^.ContinueDBSelection
              else
                SelectByPoly(CutObj2, SelectData.SelectTypes, SelectData.SelectInside);
              Dispose(CutObj2, Done);
              SelRect^.EndIt(PInfo);
            end;
        end
      else
        MsgLeftClick2 := FALSE;
    mn_DBSelCircle:
      if SnapPoint(DrawPos, 0, ot_Any, Point1, SnapObject) then
      begin
        SelCircle^.MouseMove(PInfo, Point1);
        SelCircle^.GetCircle(Middle, Radius);
        SendStr := '[SEL][1' + DDEHandler.FOutSepSign;
        if DDEHandler.FLayerInfo = 2 then
          Str(Layers^.TopLayer^.Index: 10, TempString)
        else
          TempString := Layers^.TopLayer^.Text^;
        SendStr := SendStr + TempString + DDEHandler.FOutSepSign;
        Str(Middle.X / 100: 0: 2, TempString);
        SendStr := SendStr + TempString + DDEHandler.FOutSepSign;
        Str(Middle.Y / 100: 0: 2, TempString);
        SendStr := SendStr + TempString + DDEHandler.FOutSepSign;
        Str(Radius / 100: 0: 2, TempString);
        SendStr := SendStr + TempString + ']';
        DDEHandler.SendString(SendStr);
        BreakInput(TRUE, FALSE);
      end
      else
        MsgLeftClick2 := FALSE;
    mn_DBSelRect:
      if SnapPoint(DrawPos, 0, ot_Any, Point1, SnapObject) then
      begin
        SelRect^.MouseMove(PInfo, Point1);
        SelRect^.GetRect(Rect);
        SendStr := '[SEL][0' + DDEHandler.FOutSepSign;
        if DDEHandler.FLayerInfo = 2 then
          Str(Layers^.TopLayer^.Index: 10, TempString)
        else
          TempString := Layers^.TopLayer^.Text^;
        SendStr := SendStr + TempString + DDEHandler.FOutSepSign;

        Str(Rect.A.X / 100: 0: 2, TempString);
        SendStr := SendStr + TempString + DDEHandler.FOutSepSign;
        Str(Rect.A.Y / 100: 0: 2, TempString);
        SendStr := SendStr + TempString + DDEHandler.FOutSepSign;
        Str(Rect.B.X / 100: 0: 2, TempString);
        SendStr := SendStr + TempString + DDEHandler.FOutSepSign;
        Str(Rect.B.Y / 100: 0: 2, TempString);
        SendStr := SendStr + TempString + ']';
        DDEHandler.SendString(SendStr);
        SelRect^.EndIt(PInfo);
      end
      else
        MsgLeftClick2 := FALSE;
    mn_InsertOLEObject,
      mn_InsLinkObj,
      mn_PasteOLEObj:
      if OLERect^.IsOn then
      begin
        if SnapPoint(DrawPos, 0, ot_Any, Point1, SnapObject) then
        begin
          OLERect^.MouseMove(PInfo, Point1);
          OLERect^.EndIt(PInfo);
          OLERect^.GetRect(NewOLERect);
          ClearStatusText;
          ProcInsertOLEObject(@Self, NewOLERect);
          SetActualMenu(mn_Select);
        end
        else
          MsgLeftClick2 := FALSE;
      end;
  end;
{$ENDIF}
{$ENDIF} // <----------------- AXDLL
end;

{$IFNDEF WMLT}

function TProj.SelectByNeighbour(AItem: PIndex): Boolean;
begin
  if not Layers^.TopLayer^.GetState(sf_LayerOffAtGen) then
  begin
    Result := Layers^.SelectByNeighbour(PInfo, AItem);
    UserInterface.Update([uiMenus, uiStatusBar]);
  end
  else
    Result := FALSE;
end;

function TProj.SelectByPoly(Poly: Pointer; const ObjType: TObjectTypes;
  Inside: Boolean): Boolean;
var
  i: Integer;

  procedure SelectAllLayersByPoly
      (
      Item: PLayer
      );
  begin
    if (not Item^.GetState(sf_LayerOff)) and (Item^.Text <> nil)
      and (Item^.GetState(sf_DoSelect)) and (not Item^.GetState(sf_LayerOffAtGen)) then
      Item^.SelectByPoly(PInfo, Poly, ObjType, Inside);
  end;

begin
  if ((not Layers^.TopLayer^.GetState(sf_LayerOffAtGen)) or (Layers^.TranspLayer)) then
  begin
    PInfo^.SelectionRect.Init;
    if Layers^.TranspLayer then
    begin
      for i := Layers^.LData^.Count - 1 downto 1 do
        SelectAllLayersByPoly(PLayer(Layers^.LData^.At(i)));
      Result := True;
    end
    else
      Result := Layers^.SelectByPoly(PInfo, Poly, ObjType, Inside);

    if Result then
    begin
      CorrectSelAllRect(PInfo^.SelectionRect);
      PInfo^.RedrawRect(PInfo^.SelectionRect);
      UserInterface.Update([uiMenus, uiStatusBar]);
    end;
  end
  else
    Result := FALSE;
end;

function TProj.SelectByCircle(Middle: TDPoint; Radius: LongInt;
  const ObjType: TObjectTypes; Inside: Boolean): Boolean;
var
  i: Integer;

  procedure SelectAllLayersByCirlce
      (
      Item: PLayer
      );
  begin
    if (not Item^.GetState(sf_LayerOff)) and (Item^.Text <> nil)
      and (Item^.GetState(sf_DoSelect)) and (not Item^.GetState(sf_LayerOffAtGen)) then
      Item^.SelectByCircle(PInfo, Middle, Radius, ObjType, Inside);
  end;

begin
  if ((not Layers^.TopLayer^.GetState(sf_LayerOffAtGen)) or (Layers^.TranspLayer)) then
  begin
    PInfo^.SelectionRect.Init;
    if Layers^.TranspLayer then
    begin
      for i := Layers^.LData^.Count - 1 downto 1 do
        SelectAllLayersByCirlce(PLayer(Layers^.LData^.At(i)));
      Result := True;
    end
    else
      Result := Layers^.SelectByCircle(PInfo, Middle, Radius, ObjType, InSide);
    if Result then
    begin
      CorrectSelAllRect(PInfo^.SelectionRect);
      PInfo^.RedrawRect(PInfo^.SelectionRect);
      UserInterface.Update([uiMenus, uiStatusBar]);
    end;
  end
  else
    Result := FALSE;
end;
{$ENDIF}

procedure TProj.MsgRightUp(DrawPos: TDPoint);
var
//  Rect: TDRect;
//  Disp: TPoint;
  RRect: TRotRect;
begin
  case ActualMen of
    mn_Zoom:
      with ZoomRect^ do
        if IsOn then
        begin
          SetTimer(Parent.Handle, idt_Popup, GetDoubleClickTime + 1, nil);
          ActualMen := OldMenu;
          EndIt(PInfo);
          RRect := RotRect(GrPoint(StartPos.X, StartPos.Y), GrPoint(LastPos.X, LastPos.Y),
            -PInfo^.ViewRotation);
          if (PInfo^.CalculateDisp(RRect.Width) > MinZoom)
            and (PInfo^.CalculateDisp(RRect.Height) > MinZoom) then
          begin
            KillTimer(Parent.Handle, idt_Popup);
            ZoomByRect(RRect, 0, False);
          end;
          PInfo^.SetTheCursor(crArrow);
        end;
    mn_SymbolInp:
      begin
        SymbolInp^.InvertRotation(DrawPos);
        if SymbolInp^.Rotate then
          SetCursor(idc_Rotate)
        else
          SetCursor(idc_Size);
      end;
  end;
end;

procedure TProj.MsgRightClick
  (
  DrawPos: TDPoint
  );
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}
  case ActualMen of
    mn_PartFree, mn_PartParallel:
      begin
        if not PolyPart.bDeleteSourcePoly then
        begin
          if NewPoly^.Data^.Count > 1 then
          begin
            SetActualMenu(mn_MovePartPara);
            if NewPoly^.Data^.Count > 2 then
            begin
              if not PolyPart.bDeleteSourcePoly then
              begin
                NewPoly^.DeleteLastPoint(PInfo);
              end;
            end;
            MovePoly^.SetPoly(NewPoly);
            PolyPart.SetPartLine(NewPoly);
            MovePoly^.StartIt(PInfo, CurPos, FALSE);
            PolyPart.CalcStartMovePoly;
            OldCursor := idc_Move;
            SetCursor(OldCursor);
          end;
          OldMenu := ActualMen;
        end;
      end;
    mn_MovePartPara:
      begin
      end;
    mn_Text,
      mn_SizeRot:
      begin
        TextRect^.InvertRotate;
        if TextRect^.Rotate then
          SetCursor(idc_Rotate)
        else
          SetCursor(idc_Size);
      end;
    mn_SymbolInp:
      begin
        SymbolInp^.InvertRotation(DrawPos);
        if SymbolInp^.Rotate then
          SetCursor(idc_Rotate)
        else
          SetCursor(idc_Size);
      end;
         {brovak for zooming disable this condition}
  else
{$ENDIF}
    if (ActualMen <> mn_Zoom) then
    begin
      if (IniFile^.MouseZoom) and (bMouseZoom) then
      begin
        ActualMen := mn_Zoom;
        ZoomRect^.StartIt(PInfo, DrawPos, TRUE);
        ZoomCursor := FALSE;
      end;
    end;
{$IFNDEF WMLT}
  end;
  SendClickInfo(DrawPos.x, DrawPos.y, Integer(mbRight), ActualMen, 0);
{$ENDIF}
{$ENDIF} // <----------------- AXDLL
end;

procedure TProj.MsgRightDblClick
  (
  DrawPos: TDPoint
  );
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  KillTimer(Parent.Handle, idt_Popup);
//  case ActualMen of
//    mn_Text: ;
//  else
  if (IniFile^.MouseZoom) and (bMouseZoom) then
    ZoomByFactorWithCenter(DrawPos.X,
      DrawPos.Y, 0.5);
//  end;
{$IFNDEF WMLT}
  SendClickInfo(DrawPos.x, DrawPos.y, Integer(mbRight), ActualMen, 1);
{$ENDIF}
{$ENDIF} // <----------------- AXDLL
end;

//++ Glukhov PreciseScale Build#161 08.06.01
// Returns the maximum view rectangle

function TProj.GetViewLimit: TDRect;
begin
  Result.Init;
  Result.Assign(-MaxInt, -MaxInt, MaxInt, MaxInt)
end;

function TProj.GetZoomLimit: Double;
var
  LimRect: TDRect;
  LimRotRect: TRotRect;
begin
  LimRect.InitByRect(GetViewLimit);
  LimRotRect := RotRect(LimRect.A.X, LimRect.A.Y, LimRect.XSize, LimRect.YSize, 0);
  Result := CalculateZoom(LimRotRect, 0, False);
end;

// The previous Bug#61 required to limit a zoom up to K*project Size
//------------------------------------
// The 1st rectangle is given to zoom a view.
// Procedure copies it to the 2nd rectangle and cuts it
// to limit its maximum size by the double project size
// (or somehow else)
// Returns True, if the 1st rectangle was cut (added to check the precise zooming)
//------------------------------------

function TProj.LimitZoomRect(Rect1: TRotRect; var Rect2: TRotRect): Boolean;
var
  LimRect: TDRect;
begin
  Result := False;
  LimRect.InitByRect(GetViewLimit);
//  LimRect.Inflate(LimFactor * Size.XSize, LimFactor * Size.YSize);
  Rect2 := Rect1;
  if (Rect2.Width > LimRect.XSize) then
  begin
    Rect2.Width := LimRect.XSize;
    Result := True;
  end;
  if (Rect2.Height > LimRect.YSize) then
  begin
    Rect2.Height := LimRect.YSize;
    Result := True;
  end;
  if (Rect2.X < LimRect.A.X) then
  begin
    Rect2.X := LimRect.A.X;
    Result := True;
  end;
  if (Rect2.Y < LimRect.A.Y) then
  begin
    Rect2.Y := LimRect.A.Y;
    Result := True;
  end;
end;

//----------------------------------------------------------------------------//

procedure TProj.ZoomByFactor(const Factor: Double);
var
  Center: TGrPoint;
begin
  Center := RectCenter(CurrentViewRect);
  ZoomByFactorWithCenter(Center.X, Center.Y, Factor);
end;

procedure TProj.ZoomByFactorWithCenter(const XCenter, YCenter, Factor: Double);
var
  Center: TGrPoint;
  Rect: TRotRect;
begin
  Rect := CurrentViewRect;
  Center := RectCenter(CurrentViewRect);
  MoveRect(Rect, XCenter - Center.X, YCenter - Center.Y);

  InflateRotRect(Rect,
    (1.0 / Factor - 1.0) * CurrentViewRect.Width / 2.0,
    (1.0 / Factor - 1.0) * CurrentViewRect.Height / 2.0);

  ZoomByRect(Rect, 0, FALSE);
end;

procedure TProj.ZoomByScaleWithCenter(const XCenter, YCenter, AScale: Double);
var
  Center: TGrPoint;
  Rect: TRotRect;
  OldZoom: Double;
begin
  OldZoom := PInfo^.ZoomToScale(PInfo^.Zoom);
  Rect := CurrentViewRect;
  Center := RectCenter(CurrentViewRect);
  MoveRect(Rect, XCenter - Center.X, YCenter - Center.Y);
  LimitZoomRect(Rect, CurrentViewRect);
  PInfo^.SetZoom(CalculateZoom(CurrentViewRect, 0, FALSE));
  InflateRotRect(CurrentViewRect, 0, 0);
  if AScale >= 1 then
  begin
    ZoomByFactor((1 / AScale) / PInfo^.ZoomToScale(PInfo^.Zoom));
  end
  else
  begin
    ZoomByFactor(OldZoom / PInfo^.ZoomToScale(PInfo^.Zoom));
  end;
  UpdatePaintOffset;
end;

procedure TProj.ZoomByRect(const Rect: TRotRect; AdditionalFrame: Integer; WithBorder: Boolean);
begin
  LimitZoomRect(Rect, CurrentViewRect);

  PInfo^.SetZoom(CalculateZoom(CurrentViewRect, AdditionalFrame, WithBorder));
  InflateRotRect(CurrentViewRect, AdditionalFrame / PInfo^.Zoom, AdditionalFrame / PInfo^.Zoom);

  UpdatePaintOffset;

{$IFNDEF AXDLL} // <----------------- AXDLL
  if Assigned(MenuHandler) then
    if MenuHandler.SubHandler <> nil then
      MenuHandler.SubHandler.NeedZoom := True;
{$ENDIF} // <----------------- AXDLL
end;

//----------------------------------------------------------------------------//

procedure TProj.UpdatePaintOffset;
var
  XSteps: Double;
  YSteps: Double;
  XPage: Double;
  YPage: Double;
  WorldSize: TGrRect;
  WorldView: TGrRect;
  WindowRect: TRect;
  WindowWidth: Integer;
  WindowHeight: Integer;
  ScrollXStep: Double;
  ScrollYStep: Double;
  XPos: Double;
  YPos: Double;
begin
  WorldSize := ISOFrame(RotatedRect(RotRect(Size.A.X, Size.A.Y, Size.XSize, Size.YSize, 0), PInfo^.ViewRotation));
  WorldView := ISOFrame(RotatedRect(CurrentViewRect, PInfo^.ViewRotation));
  ScaleRect(WorldSize, PInfo^.Zoom, PInfo^.Zoom);
  ScaleRect(WorldView, PInfo^.Zoom, PInfo^.Zoom);

  PInfo^.CurrentVirtRect(WindowRect);
  WindowWidth := RectWidth(WindowRect);
  WindowHeight := RectHeight(WindowRect);

  if (WindowWidth <= 0) or (WindowHeight <= 0) then
  begin
    exit;
  end;

  XSteps := RectWidth(WorldSize) / WindowWidth;
  XPage := MaxInt / XSteps;
  XSteps := XSteps * XPage;

  YSteps := RectHeight(WorldSize) / WindowHeight;
  YPage := MaxInt / YSteps;
  YSteps := YSteps * YPage;

  ScrollXStep := RectWidth(WorldSize) / XSteps;
  ScrollYStep := RectHeight(WorldSize) / YSteps;

  ScrollXOffset := WorldSize.Left;
  ScrollYOffset := WorldSize.Top;

  XPos := (XSteps / RectWidth(WorldSize)) * ((WorldView.Right + WorldView.Left - WindowWidth) / 2.0 - WorldSize.Left);
  YPos := (YSteps / RectHeight(WorldSize)) * (WorldSize.Top - (WorldView.Top + WorldView.Bottom + WindowHeight) / 2.0);

  ScrollXOffset := ScrollXOffset + XPos * ScrollXStep;
  ScrollYOffset := ScrollYOffset - YPos * ScrollYStep;

  PInfo^.FromGraphic := TRUE;
  Parent.Invalidate;

  if not FirstPaint then
  begin
    UpdateWindow(PInfo^.HWindow);
  end;
end;

function TProj.CalculateZoom(Rect: TRotRect; AdditionalFrame: Integer; ShowBorder: Boolean): Double;
var
  WorldSize: TGrRect;
  WorldRect: TGRRect;
  WindowRect: TRect;
  Zoom1: Double;
  Zoom2: Double;
  Border: Integer;
begin
  { Begrenzungsrechtecke f�r Projektgr��e und Zoom-Rechteck im World-
  + Koordinatensystem berechen }
  WorldSize := ISOFrame(RotRect(Size.A.X, Size.A.Y, Size.XSize, Size.YSize,
    PInfo^.ViewRotation));
  Rect.Rotation := Rect.Rotation + PInfo^.ViewRotation;
  WorldRect := ISOFrame(Rect);
  { Fenstergr��e in virtuellen Einheiten berechnen und Breite der Scroller
  + abziehen }
  PInfo^.CurrentVirtRect(WindowRect);
  { Zoom-Faktoren berechnen }
  if ShowBorder then
    Border := 100
  else
    Border := 0;

  if RectWidth(WorldSize) >= RectWidth(WorldRect) then
    Zoom1 := Abs((RectWidth(WindowRect) - Border - AdditionalFrame) / RectWidth(WorldRect))
  else
    Zoom1 := Abs((RectWidth(WindowRect) - AdditionalFrame) / RectWidth(WorldRect));
  if RectHeight(WorldSize) >= RectHeight(WorldRect) then
    Zoom2 := Abs((RectHeight(WindowRect) - Border - AdditionalFrame) / RectHeight(WorldRect))
  else
    Zoom2 := Abs((RectHeight(WindowRect) - AdditionalFrame) / RectHeight(WorldRect));
  { kleineren der beiden Zoom-Faktoren berechen }
  Result := Min(Zoom1, Zoom2);
end;

procedure TProj.SetOverView;
var
  ARect: TDRect;
  AWidth, AHeight: Double;
begin
  ARect.Init;
  PLayer(PInfo^.Objects)^.GetSize(ARect);
  if Document.Loaded then
  begin
    ARect.CorrectbyRect(Document.GetProj.Size);
  end;
  if ARect.IsEmpty then
  begin
    ARect.A.Init(0, 0);
    ARect.B.Init(siStartX, siStartY);
  end;
  Size := ARect;
  AWidth := ARect.XSize;
  AHeight := ARect.YSize;
  if (PInfo^.GetObjCount = 1) and (AWidth <= 100) and (AHeight <= 100) then
  begin
    ARect.Inflate(20000, 20000);
  end;
  ZoomByRect(RotRect(ARect.A.X, ARect.A.Y, ARect.XSize, ARect.YSize, 0), 0, TRUE);
end;

procedure TProj.MsgSize;
begin
  if (PInfo^.HWindow <> 0) then
  begin
    GetClientRect(PInfo^.HWindow, PInfo^.ClipRect);
    PInfo^.ExtCanvas.Resize(PInfo^.ClipRect);
    DPToLP(PInfo^.ExtCanvas.Handle, PInfo^.ClipRect, 2);
    PInfo^.ClipLimits := PInfo^.ClipRect;
    PInfo^.ClipLimits.Top := PInfo^.ClipRect.Bottom;
    PInfo^.ClipLimits.Bottom := PInfo^.ClipRect.Top;
    PInfo^.ExtCanvas.ClipRect := PInfo^.ClipRect;
    ZoomByRect(CurrentViewRect, 0, TRUE);
  end;
end;

{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL

procedure TProj.DeleteSelected(SilentMode: boolean);
var
  DelAll: Boolean;
begin
  DelAll := DeleteAll;
  if Layers^.TranspLayer and (PInfo^.SelectionSettings.TransparentMode and $04 <> 0)
    and (PInfo^.SelectionSettings.TransparentMode and $80 <> 0) then
    DeleteAll := TRUE;
      {if not(DeleteAll)
          or (MsgBox(Parent.Handle,1009,mb_YesNo or mb_IconQuestion or mb_DefButton2,'')=id_Yes) then begin}
        // Suspend Tooltip-Thread
  TooltipThread.SureSuspend;
  SetCursor(crHourGlass);
  Layers^.DeleteSelected(@Self, PInfo, DDEHandler, False, BitmapCount, MultiMedia, SilentMode);
  SetCursor(crDefault);
        // resume Tooltip-Thread
  TooltipThread.Resume;
  BreakInput(TRUE, FALSE);
   {++brovak - edit/poly thru doubleclick}
  if MenuHandler is TProjMenuHandler then
    if MenuHandler.SubHandler.ClassName = 'TPolyEditor1' then
    begin;
      SnapItem := nil;
      OldSnapItem := nil;
      MenuHandler.SubHandler := nil;
    end;
          {--brovak}

  SetModified;

   {if not SilentMode then
      PInfo^.RedrawScreen (False);}

    {end;}
  DeleteAll := DelAll;
  RefreshSymbolMan(False);
end;

procedure TProj.SetStartPoint;

  procedure DoAll
      (
      Item: PChar
      ); far;
  begin
//!?!?Routing      ProcSetStartPoint(@self,Item);
    Item[0] := DDEStrUsed;
  end;
begin
  DDEHandler.DDEData^.ForEach(@DoAll);
  DDEHandler.DeleteDDEData;
end;
{$ENDIF} // <----------------- AXDLL
{$ENDIF}

procedure TProj.SetCursor(ACursor: Integer);
begin
  if IniFile^.ReadBoolean('SETTINGS', 'UpdateCursor', TRUE) then
  begin
    PInfo^.SetTheCursor(ACursor);
    if (MenuHandler <> nil) then
    begin
      MenuHandler.Cursor := ACursor;
    end;
  end;
end;

function TProj.InputText(var Text: string): Boolean;
var
  TextDialog: TMTextInput;
begin
  Text := '';
  TextDialog := TMTextInput.Create(Parent);
  if TextDialog.ShowModal = mrOK then
  begin
//++ Glukhov Bug#291 Build#150 23.01.01
//    Text:= TextDialog.memText.Text;
//    Result:= True;
    Text := TrimTextManually(TextDialog.memText.Text, cwTrimNorm);
    if Trim(Text) = '' then
      Result := False
    else
      Result := True;
//-- Glukhov Bug#291 Build#150 23.01.01
  end
  else
    Result := False;
  TextDialog.Free;
end;

{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL

function TProj.SelectTextStyle
  (
  var AFont: AM_Font.TFontData
  )
  : Boolean;
var
  Dialog: TFontStyleDialog;
begin
  Result := FALSE;
  if PInfo^.FontsInstalled then
  begin
    Dialog := TFontStyleDialog.Create(Parent);
    try
      Dialog.Fonts := PInfo^.Fonts;
      Dialog.FontStyle := @ActualFont;
      Dialog.ShowModal;
    finally
      Dialog.Free;
    end;
  end;
end;
{$ENDIF} // <----------------- AXDLL
{$ENDIF}

procedure TProj.CreateSRQDatas
  (
  DDELine: PChar
  );
begin
  SRQDatas := StrNew(DDELine);
end;

procedure TProj.ChangeRedraw
  (
  WantRedraw: Byte
  );
var
  Cnt: Integer;
begin
  for Cnt := 0 to WinGISMainForm.ComponentCount - 1 do
    if WinGISMainForm.Components[Cnt].ClassName = MDIChildType then
      case WantRedraw of
        CNoRedraw: TMDIChild(WinGISMainForm.Components[Cnt]).ChangeRedraw(FALSE);
        CDoRedraw: TMDIChild(WinGISMainForm.Components[Cnt]).ChangeRedraw(TRUE);
        COldRedraw:
          TMDIChild(WinGISMainForm.Components[Cnt]).ChangeRedraw(
            TMDIChild(WinGISMainForm.Components[Cnt]).OldRedraw);
      end;
end;

procedure TProj.MsgDeActivate;
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  if DigitMode then
    Digitizer.DeActivateDigitizer;
{$ENDIF} // <----------------- AXDLL
end;

procedure TProj.MsgActivate;
begin
  WinGISMainForm.ActualProj := @Self;
  WinGISMainForm.ActualChild := TMDIChild(Parent);
  if DigitMode then
  begin
      { deactivate the digitizer-mode if active digitizer changed or digitizer
      + cannot be activated }
{$IFNDEF AXDLL} // <----------------- AXDLL
    if (Digitize^.CurrentDigitizer <> Digitizer.ActiveDigitizer)
      or not Digitizer.ActivateDigitizer(@Self) then
    begin
      MenuFunctions['DigitDigitizerOnOff'].Checked := FALSE;
      DigitMode := FALSE;
    end
    else
      MenuFunctions['DigitDigitizerOnOff'].Checked := TRUE;
{$ENDIF} // <----------------- AXDLL
  end;
{!?!?!      StatusLine.SetTopLayer(Layers^.TopLayer^.Text,Layers^.TopLayer^.GetState(sf_Fixed));}
// if current project is changed, setup tooltipthread correctly
  if TooltipLayer <> nil then
  begin
    TooltipThread.StartFloatingText(@self);
  end
  else
  begin
    TooltipThread.EndFloatingText;
  end;
end;

procedure TProj.CalculateSymbolClip
  (
  Edited: PSortedLong
  );

  procedure DoAll
      (
      Item: PSymbol
      ); far;
  var
    AIndex: Integer;
  begin
    if (Item^.GetObjType = ot_Symbol) and Edited^.Search(Pointer(Item^.SymIndex), AIndex) then
    begin
      Item^.CalculateClipRect(PInfo);
      UpdateClipRect(Item);
    end;
  end;
begin
  if Edited^.Count > 0 then
  begin
    PObjects(PInfo^.Objects)^.Data^.ForEach(@DoAll);
    PInfo^.RedrawScreen(TRUE);
  end;
end;

{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL

function TProj.DeleteDialog
  (
  var DelAll: Boolean
  )
  : Boolean;
begin
  DeleteDialog := ExecDialog(TDDelete.Init(Parent, Layers^.SelLayer^.Data^.GetCount,
    @DelAll, Layers^.TranspLayer, ((PInfo^.SelectionSettings.TransparentMode and $04 <> 0)
    and (PInfo^.SelectionSettings.TransparentMode and $80 <> 0)))) = idOK;
end;

function TProj.InputCoord: Boolean;
var
  More: Boolean;
  DoIt: Boolean;
  Data: TCoordData;
  Z: TPointsCoordDialog; //Brovak new F4 Dialog
  PointPos: TPOint;
  {+++Brovak fot orhto}
  AbaseEndPoint, AbaseStartPoint: Pdpoint;
  D1, D2, D3: TGrpoint;
  Angle: double;
  {---Brovak}

{++Brovak for F4-Circle}

  procedure CreateNewSircle(ACircleType: integer);
  begin;
{$IFNDEF WMLT}
    Z := PointsCoordDlg.TPointsCoordDialog.Create(Parent);
    with Z do
    begin;
      CircleType := ACircleType;
      PrepareGrid(@self, mn_New_Circle, -1);
      ShowModal;
      Free;
    end;
{$ENDIF}
  end;

  procedure CreateNewArc(AArcType: integer);
  begin;
{$IFNDEF WMLT}
    Z := PointsCoordDlg.TPointsCoordDialog.Create(Parent);
    with Z do
    begin;
      ArcType := AArcType;
      PrepareGrid(@self, mn_New_Arc, -1);
      ShowModal;
      Free;
    end;
{$ENDIF}
  end;
 {--brovak}

begin
  Data.PredefVal := False;
  {++brovak}
  GetCursorPos(PointPos);
  IniFile.ReadF4SimplyMode;
  {--brovak}
  InputCoord := FALSE;
  DoIt := FALSE;
  More := FALSE;
  with Data do
  begin
    Lines := 2;
    Text3 := 0;
    Text1 := 0;
    Text2 := 0;
    NeedPos := TRUE;
    NeedLine3 := TRUE;
    AlternativeCaption := '';

  end;
  if MenuHandler is TProjMenuHandler then
  begin
    DoIt := TProjMenuHandler(MenuHandler).CoordinateInput;
{++ Moskaliov BUG#90 BUILD#150 05.01.01}
    if DoIt then
      if MenuHandler.ClassName = 'TPrintHandler' then
      begin
        TSelectPrintRangeHandler(MenuHandler.SubHandler).InputCoordDlgSetup(Data);
        if ExecDialog(TCoordInp.Init(Parent, More, @Data)) = id_Ok then
          TSelectPrintRangeHandler(MenuHandler.SubHandler).InputCoordDlgSetValues(Data);
        DoIt := FALSE;
      end;
{-- Moskaliov BUG#90 BUILD#150 05.01.01}
    //******* Creating of Circle
    if MenuHandler.ClassName = 'TDrawCircle2PointsHandler' then
    begin;
      CreateNewSircle(2);
      Exit;
    end;
    if MenuHandler.ClassName = 'TDrawCircleCenterRadiusHandler' then
    begin;
      CreateNewSircle(1);
      Exit;
    end;
    if MenuHandler.ClassName = 'TDrawCircle3PointsHandler' then
    begin;
      CreateNewSircle(3);
      Exit;
    end;
    if MenuHandler.ClassName = 'TDrawCircle2PointsRadiusHandler' then
    begin;
      CreateNewSircle(4);
      Exit;
    end;
    if MenuHandler.ClassName = 'TDrawCircleInRectangleHandler' then
    begin;
      CreateNewSircle(5);
      Exit;
    end;

      //*** End of creating circle

      //*** Creating of Arcs
    if MenuHandler.ClassName = 'TDrawArcCenterHandler' then
    begin;
      CreateNewArc(1);
      Exit;
    end;

    if MenuHandler.ClassName = 'TDrawArc2PointsRadiusHandler' then
    begin;
      CreateNewArc(2);
      Exit;
    end;

    if MenuHandler.ClassName = 'TDrawArc3PointsHandler' then
    begin;
      CreateNewArc(3);
      Exit;
    end;

       //*** End of creating arcs

  end
  else
    if ActualMen in mnCoord then
      with Data do
      begin
        DoIt := TRUE;
        case ActualMen of
{$IFNDEF WMLT}
         {BROVAK - New F4 for all Poly}
          mn_EntToDrawAlong, // Glukhov Bug#468 Build#162 16.08.01
            mn_Poly, mn_CPoly:
            begin;
              Z := PointsCoordDlg.TPointsCoordDialog.Create(Parent);
              Z.Zbase.Init(0, 0);
              Z.Zbase := AOldMousePos;

             //define angle for ortho
              Z.RadioGroup1.ItemIndex := 1;

              if NewPoly^.Data^.Count >= 2 then
              begin;
                ABaseStartPoint := SELF.NewPoly.Data.At(SELF.NewPoly.Data.Count - 2);
                ABaseEndPoint := SELF.NewPoly.Data.At(SELF.NewPoly.Data.Count - 1);
                D1.X := ABaseStartPoint^.X;
                D1.Y := ABaseStartPoint^.Y;
                D2.X := ABaseEndPoint^.X;
                D2.Y := ABaseEndPoint^.Y;
                D3.X := AoldMousePos.X;
                D3.Y := AoldMousePos.Y;
                Angle := LineAngle(D1, D2);
                RotatePoint(D1, -Angle);
                RotatePoint(D2, -Angle);
                RotatePoint(D3, -Angle);
                if (LineAngle(D1, D3) >= 0) and (LineAngle(D1, D3) <= pi) then
                  Z.RadioGroup1.ItemIndex := 0;
              end;

              Z.SimplyMode := IniFile.SimplyMode;

              if (NewPoly^.bDrawOrtho) and (NewPoly^.Data^.Count > 1) then
              begin
                Lines := 1;
                Text3 := 203;
                PredefVal := True;
                ValuesPredefined[2] := NewPoly^.GetLastOrthoDist(PInfo);
              end
              else
              begin
                Z.PrepareGrid(@self, Ot_Cpoly, -1);
                if Z.ShowModal <> mrOK then
                  SetCursorPos(PointPos.X, PointPos.Y);
                Z.Free;
                Exit;
              end;
            end;

          mn_Pixel:
            begin;
              Z := PointsCoordDlg.TPointsCoordDialog.Create(Parent);
              Z.SimplyMode := IniFile.SimplyMode;
              Z.PrepareGrid(@self, Ot_Pixel, -1);
              if Z.ShowModal <> mrOK then
                SetCursorPos(PointPos.X, PointPos.Y);
              Z.Free;
              Exit;
            end;

          mn_Symbol:
            begin;
              Z := PointsCoordDlg.TPointsCoordDialog.Create(Parent);
              Z.SimplyMode := IniFile.SimplyMode;
              Z.PrepareGrid(@self, Ot_Symbol, -1);
              if Z.ShowModal <> mrOK then
                SetCursorPos(PointPos.X, PointPos.Y);
              Z.Free;
              Exit;
            end;

          mn_Text:
            begin;
              Z := PointsCoordDlg.TPointsCoordDialog.Create(Parent);
              Z.SimplyMode := IniFile.SimplyMode;
              Z.PrepareGrid(@self, Ot_text, -1);
              if Z.ShowModal <> mrOK then
                SetCursorPos(PointPos.X, PointPos.Y);
              Z.Free;
              Exit;
            end;

          mn_SnapPoly:
            if not Layers^.TopLayer^.GetState(sf_Fixed) then
              if PointInd <> -1 then
              begin;
                OldSnapItem := GiveMeSourceObjectFromSelect(Layers^.SelLayer.Data.At(0));
                Z := PointsCoordDlg.TPointsCoordDialog.Create(Parent);
                Z.PrepareGrid(@self, Mn_SnapPoly, FindUsedPoint(RealPoint));
                z.SimplyMode := IniFile.SimplyMode;
                if Z.ShowModal = mrOk then
                begin;
                  AOldMousePos.X := Trunc(StrToFloat(Z.PointsStringGrid.Cells[1, FindUsedPoint(RealPoint)]) * 100);
                  AOldMousePos.Y := Trunc(StrToFloat(Z.PointsStringGrid.Cells[2, FindUsedPoint(RealPoint)]) * 100);
                  MsgMouseMove(AOldMousePos);
                  MsgLeftClick(AOldMousePos);
                end
                else
                  SetCursorPos(PointPos.X, POintPos.Y);
                Z.Free;
                Exit;
              end
              else
              begin;
                MessageDialog(MlgStringList['PointsCoordDialog', 46], mtError, [mbOk], 0);
                Exit;
              end
            else
              Exit;

          mn_InsPOk:
            if not Layers^.TopLayer^.GetState(sf_Fixed) then
              if PointInd <> -1 then
              begin;
                OldSnapItem := GiveMeSourceObjectFromSelect(Layers^.SelLayer.Data.At(0));
                Z := PointsCoordDlg.TPointsCoordDialog.Create(Parent);
                z.PrevPoint := PPoly(SnapItem)^.FindIndexOfPoint(MesPoint1);
                z.NextPoint := PPoly(SnapItem)^.FindIndexOfPoint(MesPoint2);
                Z.PrepareGrid(@self, Mn_InsPOk, FindUsedPoint(RealPoint));
                Z.SimplyMode := IniFile.SimplyMode;
                if Z.ShowModal = mrOk then
                begin;
                  AOldMousePos.X := Trunc(StrToFloat(Z.PointsStringGrid.Cells[1, Z.PointsStringGrid.Row]) * 100);
                  AOldMousePos.Y := Trunc(StrToFloat(Z.PointsStringGrid.Cells[2, Z.PointsStringGrid.Row]) * 100);
                  MsgMouseMove(AOldMousePos);
                  MsgLeftClick(AOldMousePos);
                end
                else
                  SetCursorPos(PointPos.X, PointPos.Y);
                Z.Free;
                Exit;
              end
              else
              begin;
                MessageDialog(MlgStringList['PointsCoordDialog', 46], mtError, [mbOk], 0);
                Exit;
              end
            else
              Exit;
            {--Brovak poly}

{$ENDIF}
{            mn_Pixel,}
          mn_DrawSymLine,
        //    mn_Symbol,
          mn_Distance:
            begin
              Lines := 2;
              More := TRUE;
            end;
                  { Only single points should be insert not to remain input
                    coordinates dialog on the screen}
          mn_CutImage,
            mn_SuperImage,
            mn_RTrafo, //++ Glukhov Bug#136 BUILD#128 25.08.00
            mn_GetCoordinates, // Glukhov Bug#203 Build#151 08.02.01
            mn_SetImageRect, // Glukhov LoadImageInRect BUILD#123 20.07.00
                  { Only 1 point should be inserted as we have to immediately
                    switch to Omega window after it}
        //  mn_text,
          mn_SetOmegaPoint: ;
           // mn_InsPOk,
           // mn_SnapPoly,
          mn_MesPoint,
            mn_Move,
            mn_RefPoints,
            mn_GenerRef,
            mn_GenerRect: Lines := 2;
          mn_OffsetForSel,
            mn_OffsetOK:
            begin
              Lines := 1;
              Text3 := 203;
              PredefVal := True;
              ValuesPredefined[2] := InpOffset^.AktDist;
              AlternativeCaption := Getlangtext(199);
            end;
          mn_DrawForSelect:
            case SelectData.SelectWith of
              swCircle:
                begin
                  if SelCircle^.IsOn then
                    Lines := 1
                  else
                    Lines := 2;
                  Text3 := 15100;
                end;
              swLine,
                swOffset,
                swPoint,
                swPoly:
                begin
                  Lines := 2;
                  More := TRUE;
                end;
            else
              DoIt := FALSE;
            end;
          mn_OrthoMark,
            mn_DistMark:
            case MarkState of
              0: Lines := 2;
              1:
                begin
                  Lines := 3;
                  Text3 := 15101;
                  NeedPos := FALSE;
                end;
              2:
                if ActualMen = mn_DistMark then
                begin
                  Lines := 1;
                  Text3 := 203;
                end
                else
                begin
                  Lines := 2;
                  Text1 := 203;
                  Text2 := 15102;
                end;
            end;

          mn_LineMark:
            case MarkState of
              0: Lines := 2;
              1:
                begin
                  Lines := 3;
                  Text3 := 15101;
                  NeedPos := FALSE;
                end;
              2:
                begin
                  Lines := 1;
                  Text3 := 15102;
                end;
              3: Lines := 2;
              4:
                begin
                  Lines := 3;
                  Text3 := 15101;
                  NeedPos := FALSE;
                end;
              5:
                begin
                  Lines := 1;
                  Text3 := 15102;
                end;

            end;

          mn_CircleMark:
            case MarkState of
              0: Lines := 2;
              1:
                begin
                  Lines := 1;
                  Text3 := 15100;
                end;
              2:
                begin
                  Lines := 1;
                  Text3 := 15101;
                end;
            end;
          mn_CircleCutMark:
            case MarkState of
              0, 1: Lines := 2;
              2, 3:
                begin
                  Lines := 1;
                  Text3 := 15100;
                end;
            end;
          mn_AngleMark:
            case MarkState of
              0, 1: Lines := 2;
              2, 3:
                begin
                  Lines := 4;
                  Text3 := 11239;
                  Text4 := 11240;
                  NeedPos := FALSE;
                end;
            end;
          mn_SplitLine:
            begin
              Lines := 1;
              Text3 := 203;
            end;
        else
          DoIt := FALSE;
        end; // end of CASE statement
      end; // end of statement "with Data do"
  if DoIt then
  begin
    Data.CoordSystem := Integer(PInfo^.CoordinateSystem.CoordinateType);
    if ExecDialog(TCoordInp.Init(Parent, More, @Data)) = id_Ok then
    begin
      if not More then
        SendMessage(Parent.Handle, wm_ICoord, 0, LongInt(@Data));
      InputCoord := TRUE;
    end;
  end;
end;
{$ENDIF} // <----------------- AXDLL

procedure TProj.LayerCopy(DoMove: Boolean; DestLayer: PLayer);
var
  CopyTo: PLayer;
  iSomeProblemInIDB: integer; // Cadmensky
  sMessage: AnsiString; // Cadmensky
  iObjCount, iPos: integer;

  procedure DoAll(Item: PIndex); far;
  var
    IItem: PIndex;
  begin
    {++ Ivanoff 25.09.2000}
    { When an user does moving any object on the same layer to that the object belongs,
      after deselect this object is not visible and doesn't belong to any layer.}
    {-- Ivanoff 25.09.2000}

    IItem := CopyTo^.HasObject(Item);
    if IItem = nil then
    begin
      IItem := New(PIndex, Init(Item^.Index));
      IItem.ClipRect.InitByRect(Item.ClipRect);
      if CopyTo^.InsertObject(PInfo, IItem, FALSE) then
      begin
// ++ Cadmensky
        if Item.ObjectStyle <> nil then
        begin
          Item.CopyObjectStyleTo(IItem);
          MU.LogUpdObjectStyle(PInfo, CopyTo, IItem);
        end;
// -- Cadmensky

        Layers^.IndexObject(PInfo, Item)^.Invalidate(PInfo);
{++ IDB}
{$IFNDEF AXDLL} // <----------------- AXDLL
{            if WinGisMainForm.WeAreUsingTheIDB[@SELF] then
            begin
//             I need to know which layer is a source of selected object.
               bF := FALSE;
               for iI := 0 to SELF.Layers.LData.Count - 1 do
               begin
                  ALayer := SELF.Layers.LData.At (iI);
                  ALayerIndex := ALayer.HasIndexObject (Item.Index);
                  if (ALayerIndex <> nil) and ALayerIndex.GetState (sf_Selected) then
                  begin
                     bF := TRUE;
                     BREAK;
                  end;
               end;
//             Insert item attribute record.
               WinGisMainForm.IDB_Man.InsertItem (@SELF, CopyTo, IItem);
//             And copy all data from the source record to the destination record.
//             The operation for the same layer and the same item will not be executed.
               if bF then
                  if iSomeProblemInIDB = 0 then // Cadmensky
                     iSomeProblemInIDB := WinGISMainForm.IDB_Man.CopyAttributeInfo (@SELF, ALayer, CopyTo, Item.Index, Item.Index);
            end;}
{$ENDIF} // <----------------- AXDLL
{-- IDB}
        PInfo^.FromGraphic := TRUE;
      end
      else
      begin
        Dispose(IItem, Done);
        IItem := nil;
      end;
      if DoMove and (IItem <> nil) and (not Layers^.TopLayer^.GetState(sf_Fixed)) then
      begin
        IItem.SetState(sf_Selected, TRUE);
// ++ Cadmensky
{$IFNDEF AXDLL} // <----------------- AXDLL
        WinGisMainForm.UndoRedo_Man.AddObjectInUndoData(@SELF, CopyTo, IItem^.Index);
{$ENDIF} // <----------------- AXDLL
// -- Cadmensky
        Layers^.TopLayer^.DeleteIndex(PInfo, Item);
        Layers^.IndexObject(PInfo, Item)^.Invalidate(PInfo);
        PInfo^.FromGraphic := TRUE;
      end;
    end;
    Inc(iPos);
    StatusBar.Progress := 100 * iPos / iObjCount;
  end;

begin
  if DestLayer = nil then
  begin
    if DoMove then
      CopyTo := SelectLayerDialog(@Self, [sloNewLayer, sloInvisibleLayers], 2020)
    else
      CopyTo := SelectLayerDialog(@Self, [sloNewLayer, sloInvisibleLayers], 2010);
  end
  else
    CopyTo := DestLayer;

  if (CopyTo <> nil) and (not CopyTo^.GetState(sf_Fixed)) then
  begin
    SetCursor(crHourGlass);
    SetStatusText(GetLangText(4004), TRUE);
{++ IDB}
{$IFNDEF AXDLL} // <----------------- AXDLL
    if WinGisMainForm.WeAreUsingTheIDB[@SELF] then
      WinGisMainForm.IDB_Man.GetReadyToReceiveDeletedObjectID(@SELF);
    if WinGisMainForm.WeAreUsingUndoFunction[@SELF] then
      WinGisMainForm.UndoRedo_Man.GetReadyToReceiveDeletedObjectID(@SELF);
{$ENDIF} // <----------------- AXDLL
{-- IDB}
// ++ Cadmensky
    iSomeProblemInIDB := 0;

// -- Cadmensky

// ++ Cadmensky IDB Version 2.2.8
{$IFNDEF AXDLL} // <----------------- AXDLL
    if WinGisMainForm.WeAreUsingTheIDB[@SELF] then
      WinGisMainForm.IDB_Man.MoveAttributeInfo(@SELF, CopyTo);

    StatusBar.ProgressPanel := TRUE;
    StatusBar.Progress := 0;
    StatusBar.ProgressText := GetLangText(4012);

    iObjCount := Layers^.SelLayer^.Data^.GetCount;
    iPos := 1;
{$ENDIF} // <----------------- AXDLL
// -- Cadmensky IDB Version 2.2.8
    Layers^.SelLayer^.Data^.ForEach(@DoAll);

// ++ Cadmensky
{$IFNDEF AXDLL} // <----------------- AXDLL
    StatusBar.ProgressPanel := FALSE;
    if iSomeProblemInIDB <> 0 then
    begin
      sMessage := MlgStringList['IDB', 19] + #13#10 + #13#10;
      if iSomeProblemInIDB = 1 then
        sMessage := sMessage + MlgStringList['IDB', 20]
      else
        if iSomeProblemInIDB = 2 then
          sMessage := sMessage + MlgStringList['IDB', 21]
        else
          sMessage := sMessage + MlgStringList['IDB', 20] + #13#10 + MlgStringList['IDB', 21];

      Application.MessageBox(PChar(sMessage), 'WinGIS', MB_OK + MB_DEFBUTTON1 + MB_ICONWARNING);
    end;
{$ENDIF} // <----------------- AXDLL
// -- Cadmensky

{++ IDB}
{$IFNDEF AXDLL} // <----------------- AXDLL
    if WinGisMainForm.WeAreUsingTheIDB[@SELF] then
    begin
      WinGisMainForm.IDB_Man.ExecuteDeleting(@SELF, FALSE);
      if WinGISMainForm.WeAreUsingTheIDBUserInterface[@SELF] then
        WinGISMainForm.IDB_Man.RefreshDataWindows(@SELF);
    end;
    if WinGisMainForm.WeAreUsingUndoFunction[@SELF] then
      WinGisMainForm.UndoRedo_Man.ExecuteDeleting(@SELF, FALSE);
{$ENDIF} // <----------------- AXDLL
{-- IDB}
{$IFNDEF AXDLL} // <----------------- AXDLL
    SetCursor(crDefault);
    ClearStatusText;
{$ENDIF} // <----------------- AXDLL
    if DoMove then
      Layers^.SetTopLayer(CopyTo);
    PInfo^.RedrawInvalidate;
{$IFNDEF AXDLL} // <----------------- AXDLL
    UserInterface.Update([uiLayers, uiWorkingLayer]);
{$ENDIF} // <----------------- AXDLL
    SetModified;
  end;
end;

procedure TProj.AddDBEntry
  (
  ACommunicName: PChar;
  AConProj: Boolean;
  ASendProj: Boolean
  );
begin
  if DatabaseSettings <> nil then
    DatabaseSettings^.Databases^.Insert(New(PDBSetting, Init(ACommunicName, AConProj, ASendProj)));
end;

{$IFNDEF AXDLL} // <----------------- AXDLL

procedure TProj.DBEdit;
begin
  if Layers^.SelLayer^.Data^.GetCount = 1 then
    ProcDBSendObject(@Self, Layers^.SelLayer^.Data^.At(0), 'EIN');
end;

procedure TProj.DBEditAuto
  (
  AIndex: PIndex
  );
begin
  {DDEHandler.SetDBSendStrings(DDECommAIns,NIL);}
  ProcDBSendObject(@Self, AIndex, 'EIN');
  {DDEHandler.SetDBSendStrings(DDECommAll,NIL);}
end;
{$ENDIF} // <----------------- AXDLL
{$ENDIF}

{**************************************************************************************}
{ Function TProj.CorrectSize                                                           }
{  Korrigiert die Gr��e des Projektes, damit ARect und ein Rand von siBorder           }
{  enthalten ist. �ndert sich die Projektgr��e und ist Redraw TRUE, so wird die        }
{  Bildschirmanzeige erneuert. Die Funtion liefert TRUE, wenn die Projektgr��e         }
{  ge�ndert wurde.                                                                     }
{**************************************************************************************}

function TProj.CorrectSize
  (
  ARect: TDRect;
  Redraw: Boolean
  )
  : Boolean;
var
  ASize: TDRect;
begin
  CorrectSize := FALSE;
  if not ARect.IsEmpty then
  begin
    ASize.Init;
    ASize := Size;
    Size.CorrectByRect(ARect);
    if not Size.IsEqual(ASize) then
    begin
      if Redraw then
        UpdatePaintOffset;
      CorrectSize := TRUE;
    end;
  end;
end;

procedure TProj.BreakInput
  (
  SetMenu: Boolean;
  MenuChange: Boolean
  );
begin
{$IFNDEF AXDLL} // <----------------- AXDLL

{++Brovak Bug 500}
//You can use then var for detect press "Esc" when you procedure
//is working . Don't forget set it to false before start your procedure ! :)
  StopExecutingFunction := true;
{--Brovak}
  if ActualMen in mnBreakESC then
  begin

    case ActualMen of
      mn_Snap:
        begin
          Line^.EndIt(PInfo);
          PInfo^.EditPoly := FALSE;
          if (EscPressedTwo = true) and (SetMenu = true) then
            SetActualMenu(mn_Select, TRUE); //br

          if SnapItem <> nil then
            if Snapitem.GetObjType = ot_Pixel then
            begin;
              DeselectAll(true);
              EscPressedTwo := true;
            end;
          OldSnapitem := nil;
          PointInd := -1;
        end;

      mn_SnapPoly:
        begin
          Lines^.EndIt(PInfo);
          SetCursor(idc_Move);
          PointInd := -1;
          if MenuChange then
          begin
            Layers^.SelLayer^.Draw(PInfo);
            PInfo^.EditPoly := FALSE;
          end;
          if (EscPressedTwo = true) and (SetMenu = true) then
          begin;
            PInfo^.EditPoly := FALSE;
            SetActualMenu(mn_Select, true);
            Exit;
          end;
          EscPressedTwo := true;
        end;

      mn_DelPOK:
        begin;
          if MenuChange then
          begin
            Layers^.SelLayer^.Draw(PInfo);
            PInfo^.EditPoly := FALSE;
          end;
          PInfo^.EditPoly := FALSE;
          if SetMenu = true then
            SetActualMenu(mn_Select, TRUE);
        end;

      mn_DelPoint:
        begin;
          PInfo^.EditPoly := FALSE;
          if SetMenu = true then
            SetActualMenu(mn_Select, TRUE);
        end;
      mn_InsPOK:
        begin
          PointInd := -1;
          Lines^.EndIt(PInfo);
          SetCursor(idc_Ins);
          if MenuChange then
          begin
            Layers^.SelLayer^.Draw(PInfo);
            PInfo^.EditPoly := FALSE;
          end;
          if (EscPressedTwo = true) and (SetMenu = true) then
          begin;
            PInfo^.EditPoly := FALSE;
            SetActualMenu(mn_Select, true);
            Exit;
          end;
          EscPressedTwo := true;
         //     else  SetActualMenu(mn_Select,TRUE);//br;
        end;

      mn_InsPoint:
        begin;
          PInfo^.EditPoly := FALSE;
          if SetMenu = true then
            SetActualMenu(mn_Select, TRUE);
        end;
      mn_Select:
        if SelRect^.IsOn then
        begin
          SelRect^.EndIt(PInfo);
          SetCursor(SelectCursor);
        end;
{$IFNDEF WMLT}
      mn_PartFree,
        mn_PartParallel:
        begin
          NewPoly^.EndIt(PInfo);
          ClearStatusText;
          if not PolyPart.bDeleteSourcePoly then
          begin
            SetActualMenu(mn_Select, TRUE);
            ShowPartDlg(@Self);
          end;
        end;
      mn_CParallel:
        begin
          NewPoly^.EndIt(PInfo);
          SetStatusText(GetLangText(11374), FALSE);
        end;
      mn_CBufferDraw:
        begin
          NewPoly^.EndIt(PInfo);
          SetStatusText(GetLangText(11401), FALSE);
        end;
      mn_CIslands,
        mn_DrawSymLine,
        mn_Poly,
        mn_CPoly:
        begin;
          NewPoly^.EndIt(PInfo);
           {+++brovak bug 105 build 132}
          NewPolyP^.EndIt(Pinfo);
          SelRect^.EndIt(Pinfo);
          ClearStatusText;
           {---brovak}
        end;
//++ Bug#468 Build#162 16.07.01
{(*}
      mn_EntToDrawAlong:
        begin
          NewPoly^.EndIt(PInfo);
          NewPolyP^.EndIt(Pinfo);
          SelRect^.EndIt(Pinfo);
          ClearStatusText;
          SetActualMenu( mn_Select, True );
          if Assigned( ActualData ) then ProcForTxtAdvancedHandler( @Self, True );
        end;
{*)}
//-- Bug#468 Build#162 16.07.01
      mn_RefPoints:
        begin
          if MenuChange then
          begin
            ClearStatusText;
            if Digitize^.RefsInput then
              Digitize^.CalculateScale;
          end;
        end;
      mn_OffsetOK:
        begin
          InpOffset^.EndIt(PInfo);
          ClearStatusText;
          //DeSelectAll(FALSE);
          SetActualMenu(mn_Offset);
        end;
      mn_OffsetForSel:
        begin
          InpOffset^.EndIt(PInfo);
          ClearStatusText;
          if DBSelection^.Selecting then
            DBSelection^.EndDBSelection(TRUE, FALSE)
          else
          begin
            if SelectData.SelectObject then
            begin
              if not DBSelection^.EndingSelection then
                SetActualMenu(mn_SelForSelect);
            end
            else
            begin
              NewPoly^.EndIt(PInfo);
              if OffsetItem <> nil then
                Dispose(OffsetItem, Done);
              OffsetItem := nil;
              if not DBSelection^.EndingSelection then
                SetActualMenu(mn_DrawForSelect);
            end;
          end;
        end;
{$ENDIF}
      mn_Distance:
        if NewPoly^.Data^.Count >= 1 then
        begin
          NewPoly^.EndIt(PInfo);
          LastDistance := 0;
          ClearStatusText;
        end;
      mn_MesPoint: Line^.EndIt(PInfo);
      mn_MesCut:
        begin
          SetStatusText(GetLangText(928), FALSE);
          MesCutState := 0;
        end;
      mn_MesOrtho:
        begin
          SetStatusText(GetLangText(928), FALSE);
          MesCutState := 0;
        end;
      mn_Combine,
        mn_CombineSel,
        mn_CombineSelOne:
        begin
          NewPoly^.EndIt(PInfo);
          if MenuChange then
            //ClearStatusText
          else
            SetStatusText(GetLangText(10108), FALSE);
          if ActualMen = mn_CombineSelOne then
            DeselectAll(FALSE);
          ActualMen := mn_Combine;
        end;
      mn_LineCut:
        begin
          if MenuChange then
            ClearStatusText
          else
            if TrimmOptions = to_ToArea then
              SetStatusText(GetLangText(11246), FALSE)
            else
              SetStatusText(GetLangText(10101), FALSE);
          DeselectAll(FALSE);
        end;
      mn_DrawForSelect:
        case SelectData.SelectWith of
          swCircle:
            begin
              if SelCircle^.IsOn then
                ClearStatusText;
              SelCircle^.EndIt(PInfo);
              if DBSelection^.Selecting then
                DBSelection^.EndDBSelection(TRUE, FALSE);
            end;
          swRect:
            begin
              SelRect^.EndIt(PInfo);
              if DBSelection^.Selecting then
                DBSelection^.EndDBSelection(TRUE, FALSE);
            end;
          swLine,
            swPoint,
            swOffset,
            swPoly:
            begin
              NewPoly^.EndIt(PInfo);
              if DBSelection^.Selecting then
                DBSelection^.EndDBSelection(TRUE, FALSE);
            end;
          swPixel:
            if DBSelection^.Selecting then
              DBSelection^.EndDBSelection(TRUE, FALSE);
        end;
      mn_SelForSelect:
        case SelectData.SelectWith of
          swPoly,
            swCircle,
            swLine,
            swPoint,
            swOffset,
            swPixel:
            if DBSelection^.Selecting then
              DBSelection^.EndDBSelection(TRUE, FALSE);
        end;
      mn_DBSelCircle:
        begin
          if SelCircle^.IsOn then
            ClearStatusText;
          SelCircle^.EndIt(PInfo);
        end;
      mn_DBSelRect: SelRect^.EndIt(PInfo);
      mn_OrthoMark,
        mn_DistMark:
        begin
          if MenuChange then
            ClearStatusText
          else
            SetStatusText(GetLangText(11203), FALSE);
          if MarkState > 0 then
            Line^.EndIt(PInfo);
          if MarkState > 1 then
            Line1^.EndIt(PInfo);
          MarkState := 0;
        end;
      mn_LineMark:
        begin
          if MenuChange then
            ClearStatusText
          else
            SetStatusText(GetLangText(11203), FALSE);
          if MarkState > 0 then
            Line^.EndIt(PInfo);
          if MarkState > 1 then
            Line1^.EndIt(PInfo);
          if MarkState > 2 then
            Line2^.EndIt(PInfo);
          if MarkState > 4 then
            Line3^.EndIt(PInfo);
          MarkState := 0;
        end;
      mn_CircleCutMark:
        begin
          if MenuChange then
            ClearStatusText
          else
            SetStatusText(GetLangText(11214), FALSE);
          if MarkState > 0 then
            Line^.EndIt(PInfo);
          if MarkState > 1 then
            SelCircle^.EndIt(PInfo);
          if MarkState > 2 then
            InpCircle^.EndIt(PInfo);
          MarkState := 0;
        end;
      mn_CircleMark:
        begin
          if MenuChange then
            ClearStatusText
          else
            SetStatusText(GetLangText(11203), FALSE);
          Line^.EndIt(PInfo);
          SelCircle^.EndIt(PInfo);
          MarkState := 0;
        end;
      mn_AngleMark:
        begin
          if MenuChange then
            ClearStatusText
          else
            SetStatusText(GetLangText(11217), FALSE);
          if MarkState > 0 then
            Line^.EndIt(PInfo);
          if MarkState > 1 then
            Line1^.EndIt(PInfo);
          if MarkState > 2 then
            Line2^.EndIt(PInfo);
          MarkState := 0;
        end;
    else
      begin
        case ActualMen of
          mn_Move: Line^.EndIt(PInfo);
          mn_SizeRot:
            begin
              if TextRect^.IsOn then
                ClearStatusText;
              TextRect^.EndIt(PInfo);
            end;
          mn_SymbolInp:
            begin
              SymbolInp^.EndIt(PInfo);
              ClearStatusText;
            end;
          mn_GenerRef,
            mn_GenerRect:
            begin
              SelRect^.EndIt(PInfo);
              ClearStatusText;
            end;
          mn_Text,
            mn_DBText:
            begin
              if TextRect^.IsOn then
                ClearStatusText;
              TextRect^.EndIt(PInfo);
                {brovak BBUG#80 BULD127}
              if InputTextFlag = true then
              begin;
                ClearStatusText;
                InputTextFlag := false;
              end;
                {--brovak}
            end;
          mn_SplineEdit: EditSpline^.EndIt(PInfo);
          mn_EditImage,
//++ Glukhov Bug#264 Build#161 15.06.01
//          mn_EditTxtRect, // Glukhov Bug#170 BUILD#125 03.11.00
//-- Glukhov Bug#264 Build#161 15.06.01
          mn_EditOLE: InputBox^.EndIt(PInfo);
//++ Glukhov Bug#264 Build#161 15.06.01
          mn_EditTxtRect: EditTxtBox^.EndIt(PInfo);
//-- Glukhov Bug#264 Build#161 15.06.01
          mn_RTrafo: ClearStatusText;
{$IFNDEF WMLT}
//++ Glukhov Bug#203 Build#151 08.02.01
          mn_GetCoordinates:
            begin
              if RTrafoCnt = 0 then
                ProcSendCoordinates(@Self, Low(LongInt), Low(LongInt));
              RTrafoCnt := 1;
              ClearStatusText;
            end;
//-- Glukhov Bug#203 Build#151 08.02.01
{$ENDIF}
              {++brovak #MOD for copyinf of selected part of image}
          mn_CutImage, mn_SuperImage:
            begin;
              SelRect^.IsOn := false;
              SelRect^.EndIt(PInfo);
              DeselectAll(false);
              ClearStatusText;
              SetCursor(SelectCursor);
              SetActualMenu(mn_Select, TRUE);
            end;
            {--brovak}

//++ Glukhov LoadImageInRect BUILD#123 20.07.00
{(*}
          mn_SetImageRect:
            begin;
              if Assigned(ActualData) then
              begin
                Dispose(PImage(ActualData), Done);
                ActualData:= nil;
              end;
              if SelRect^.IsOn then SelRect^.EndIt(PInfo);
              ClearStatusText;
              SetActualMenu(mn_Select, TRUE);
            end;
//-- Glukhov LoadImageInRect BUILD#123 20.07.00

//++ Glukhov ObjAttachment 11.10.00
          mn_AttachObject:
            begin
              ActualData:= nil;
              ClearStatusText;
              SetActualMenu(mn_Select, True);
            end;
//-- Glukhov ObjAttachment 11.10.00

//++ Bug#468 Build#162 16.07.01
          mn_SelToDrawAlong:
            begin
              ClearStatusText;
              SetActualMenu( mn_Select, True );
              if Assigned( ActualData ) then ProcForTxtAdvancedHandler( @Self, True );
              SetMenu:= False;
            end;
{*)}
//-- Bug#468 Build#162 16.07.01

        end;

        if SetMenu then
          SetActualMenu(mn_Select, TRUE);
      end;
    end;
    ClickCnt := 0;
        {+++Brovak BUG 102 BUILD 132}
  end;
{$IFNDEF WMLT}
  if (MenuFunctions['SelectTransparent'].Checked = true) then
  begin;
    Self.SetTransparentSelect(false, false);
    MenuFunctions['SelectTransparent'].Checked := false;
  end;
{$ENDIF}
     {--Brovak}
{$ENDIF} // <----------------- AXDLL
end;

procedure TProj.DeSelectAll(ATextOut: Boolean; bIDBCall: boolean = true);
var
  OldTop: PLayer;
begin
  if SelCircle^.IsOn or NewPoly^.LineOn or Line^.IsOn or Lines^.IsOn
//++ Glukhov Bug#170 BUILD#125 03.11.00
//    or (ActualMen in [mn_SizeRot,mn_SymbolInp,mn_SplineEdit,mn_EditImage])
  or (ActualMen in
    [mn_SizeRot, mn_SymbolInp, mn_SplineEdit, mn_EditImage, mn_EditTxtRect])
//-- Glukhov Bug#170 BUILD#125 03.11.00
  {or InpOffset^.IsOn}then
    Exit;
  if ActualMen in [mn_Move] then
    SetActualMenu(mn_Select);
  if Layers^.SelLayer^.SelInfo.Count > 0 then
  begin
{$IFNDEF AXDLL} // <----------------- AXDLL
    SetCursor(crHourGlass);
{++ IDB_INFO}
//++Cadmensky
//    If WinGISMainForm.WeAreUsingTheIDBInfoWindow Then
//       WinGISMainForm.IDB_Man.ClearInfoTable(@SELF);
//--Cadmensky
{$ENDIF} // <----------------- AXDLL
{-- IDB_INFO}
    PInfo^.GetCurrentScreen(SelAllRect);
    OldTop := Layers^.TopLayer;
    Layers^.DeselectAllLayers(PInfo);
    SelList^.DeleteAll;
    Layers^.TopLayer := OldTop;
{$IFNDEF AXDLL} // <----------------- AXDLL
    SetCursor(crDefault);
{$ENDIF} // <----------------- AXDLL
    if Document.Loaded then
      Document.DeselectAll(FALSE);
{$IFNDEF AXDLL} // <----------------- AXDLL
    PInfo^.RedrawRect(SelAllRect);
{$ENDIF} // <----------------- AXDLL
    SelAllRect.Init;
// ++ Cadmensky
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
    if bIDBCall then
      if WinGisMainForm.WeAreUsingTheIDB[@SELF] then
        WinGISMainForm.IDB_Man.ShowRecordsForSelectedObjects(@SELF);
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
// -- Cadmensky
  end;
end;

procedure TProj.DeSelectAllWithoutDlg
  (
  ATextOut: Boolean
  );
var
  OldTop: PLayer;
begin
  if SelCircle^.IsOn or NewPoly^.LineOn or Line^.IsOn or Lines^.IsOn
//++ Glukhov Bug#170 BUILD#125 03.11.00
//    or (ActualMen in [mn_SizeRot,mn_SymbolInp,mn_SplineEdit,mn_EditImage])
  or (ActualMen in
    [mn_SizeRot, mn_SymbolInp, mn_SplineEdit, mn_EditImage, mn_EditTxtRect])
//-- Glukhov Bug#170 BUILD#125 03.11.00
  or InpOffset^.IsOn then
    Exit;
  if ActualMen in [mn_Move] then
    SetActualMenu(mn_Select);
  if Layers^.SelLayer^.SelInfo.Count > 0 then
  begin
    // SetCursor(crHourGlass);
    PInfo^.GetCurrentScreen(SelAllRect);
    OldTop := Layers^.TopLayer;
    Layers^.DeselectAllLayersWithoutDlg(PInfo);
    SelList^.DeleteAll;
    Layers^.TopLayer := OldTop;
    // SetCursor(crDefault);
    if Document.Loaded then
      Document.DeselectAll(FALSE);
    // PInfo^.RedrawRect(SelAllRect);
    SelAllRect.Init;
  end;
end;

procedure TProj.MsgDefault
  (
  var Msg: TMessage
  );
begin
end;

procedure TProj.MsgDigitize
  (
  Info: PDigitInfo
  );
var
  APoint: TDPoint;
  AKey: Word;
begin
  case Info^.Action of
    diPosition:
      if ActualMen = mn_RefPoints then
      begin
        IgnoreClick := True;
        InpRef := TRUE;
        RefPoint.Init(Info^.XPos, Info^.YPos);
        SetStatusText(Format(GetLangText(7101), [RefCnt]), FALSE);
      end
      else
      begin
        Digitize^.CalculateDraw(Info^.XPos, Info^.YPos, APoint);
        CurPos.Init(APoint.X, APoint.Y);
        if Assigned(MenuHandler) then
        begin
          MenuHandler.OnMouseMove(APoint.X, APoint.Y, GetShiftState);
          MenuHandler.OnMouseDown(APoint.X, APoint.Y, mbLeft, GetShiftState);
          FinishMenuHandler;
          if Assigned(MenuHandler) then
            MenuHandler.OnMouseUp(APoint.X, APoint.Y, mbLeft, GetShiftState);
        end
        else
        begin
          StatusBar[0] := PInfo^.CoordinateSystem.FormatCoordinates(APoint.X, APoint.Y, @Self);
          MsgLeftClick(APoint);
          MsgLeftUp(APoint);
        end;
      end;
    diEndPoly:
      if ActualMen <> mn_RefPoints then
      begin
        Digitize^.CalculateDraw(Info^.XPos, Info^.YPos, APoint);
        if Assigned(MenuHandler) then
        begin
          MenuHandler.OnMouseMove(APoint.X, APoint.Y, GetShiftState);
          MenuHandler.OnMouseDown(APoint.X, APoint.Y, mbLeft, [ssDouble]);
          FinishMenuHandler;
          if Assigned(MenuHandler) then
            MenuHandler.OnMouseUp(APoint.X, APoint.Y, mbLeft, [ssDouble]);
        end
        else
          MsgLeftClick(APoint);
        MsgLeftDblClick(APoint)
      end;
    diCancel:
      if Assigned(MenuHandler) then
      begin
        AKey := vk_Escape;
        MenuHandler.OnKeyDown(AKey, GetShiftState);
        FinishMenuHandler;
      end
      else
        BreakInput(TRUE, FALSE);
    diPoint: SetActualMenu(mn_Pixel);
    diPoly: SetActualMenu(mn_Poly);
    diCPoly: SetActualMenu(mn_CPoly);
  end;
  Dispose(Info);
end;

{$IFNDEF WMLT}

procedure TProj.MsgCoordInput
  (
  Info: PCoordData
  );
{$IFNDEF AXDLL} // <----------------- AXDLL
var
  DrawPos: TDPoint;
  APos: LongInt;
  Rect: TDRect;
  Angle: Real;
  MousePos: TDPoint;
  Point: TPoint;
  Area: Double;
  Cnt: Integer;
  ALayer: PLayer;
  ABaseStartPoint, ABaseEndPoint: PDPoint;
  BPos, CPos: TDPoint;
  NDist: Double;
{$ENDIF} // <----------------- AXDLL
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  DrawPos.Init(Info^.XPos, Info^.YPos);
  GetCursorPos(Point);
  DPToLP(PInfo^.ExtCanvas.Handle, Point, 1);
  PInfo^.ConvertToDraw(Point, MousePos);
  case ActualMen of
    mn_SplitLine:
      begin
        InpOffset^.StartPos := CalcLinePoint2(InpOffset^.Poly^.Index, Info^.Line3);
        CalculateSplitLength;
        InpOffset^.EndIt(PInfo);
        BreakInput(TRUE, False);
        if (SplitL1 > 0) and (SplitL2 > 0) then
        begin
          ALayer := SelectLayerDialog(@Self, [sloNewLayer], 2020);
          if ALayer <> nil then
            SplitLine(InpOffset^.Poly^.Index, ALayer);
        end;
        SetActualMenu(mn_Select);
      end;
    mn_RefPoints:
      begin
        Digitize^.InsertReference(RefPoint, DrawPos);
        Inc(RefCnt);
        SetStatusText(Format(GetLangText(7100), [RefCnt]), FALSE);
        InpRef := FALSE;
        if (DigitOpt.Transform = tfLinear) and (RefCnt > 2) then
          SetActualMenu(mn_Select);
        UserInterface.Update([uiMenus]);
      end;
    mn_GenerRef:
      begin
        GenerData.StartPoint := DrawPos;
        SetActualMenu(mn_GenerRect);
      end;
    mn_OffsetOK:
      begin
        BreakInput(TRUE, FALSE);
        if OffsetItem^.GetObjType = ot_CPoly then
        begin
          Area := 0.0;
          with OffsetItem^ do
            for Cnt := 1 to Data^.Count - 1 do
              Area := Area + ((0.0 + PDPoint(Data^.At(Cnt))^.X + PDPoint(Data^.At(Cnt - 1))^.X) *
                (0.0 + PDPoint(Data^.At(Cnt))^.Y - PDPoint(Data^.At(Cnt - 1))^.Y));
          if Area < 0 then
            Info^.Line3 := -Info^.Line3;
        end;
        ProcCreateOffset(@Self, Info^.Line3);
      end;
    mn_OffsetForSel: DoCreateOffsetAndSelect(@Self, Info^.Line3);
    mn_DrawForSelect:
      case SelectData.SelectWith of
        swCircle:
          begin
            if SelCircle^.IsOn then
              SelCircle^.GetCircle(DrawPos, APos);
            DrawPos := SelCircle^.StartPos;
            DrawPos.Move(Info^.Line3, 0);
            SelCircle^.MouseMove(PInfo, DrawPos);
            SelCircle^.GetCircle(DrawPos, APos);
            SelectByCircle(DrawPos, Info^.Line3, SelectData.SelectTypes, SelectData.SelectInside);
            BreakInput(TRUE, FALSE);
            ClickCnt := 0;
          end;
      else
        begin
          MsgMouseMove(DrawPos);
          MsgLeftClick(DrawPos);
        end;
      end;

    mn_LineMark:
      case MarkState of
        0: MsgLeftClick(DrawPos);
        1:
          begin
            if DrawPos.X <> MaxLongInt then
            begin
              ProcMarkLinePar(@Self, DrawPos, MousePos);
            end
            else
            begin
              DrawPos := Line^.StartPos;
              PInfo^.GetCurrentScreen(Rect);
              DrawPos.MoveAngle(PI / 2 - PInfo^.ConvAngle(Info^.Line3, False) * Pi / 18000, Round(Rect.YSize * 3 / 4));
              ProcMarkLinePar(@Self, DrawPos, MousePos);
            end;
          end;
        2:
          begin
            DrawPos := Line1^.StartPos;
            Angle := Line^.StartPos.CalculateAngle(Line^.LastPos);
            Angle := Angle + Pi / 2;
            DrawPos.MoveAngle(Angle, Info^.Line3);
            ProcMarkLinePar(@Self, DrawPos, MousePos);
          end;
        3: MsgLeftClick(DrawPos);
        4:
          begin
            if DrawPos.X <> MaxLongInt then
            begin
              ProcMarkLinePar(@Self, DrawPos, MousePos);
            end
            else
            begin
              DrawPos := Line2^.StartPos;
              PInfo^.GetCurrentScreen(Rect);
              DrawPos.MoveAngle(PI / 2 - PInfo^.ConvAngle(Info^.Line3, False) * Pi / 18000, Round(Rect.YSize * 3 / 4));
              ProcMarkLinePar(@Self, DrawPos, MousePos);
            end;
          end;
        5:
          begin
            DrawPos := Line3^.StartPos;
            Angle := Line2^.StartPos.CalculateAngle(Line2^.LastPos);
            Angle := Angle + Pi / 2;
            DrawPos.MoveAngle(Angle, Info^.Line3);
            ProcMarkLinePar(@Self, DrawPos, MousePos);
          end;
      end;

    mn_OrthoMark,
      mn_DistMark:
      case MarkState of
        0: MsgLeftClick(DrawPos);
        1:
          begin
            if DrawPos.X <> MaxLongInt then
              ProcMarkLinearOrtho(@Self, DrawPos, MousePos)
            else
            begin
              DrawPos := Line^.StartPos;
              PInfo^.GetCurrentScreen(Rect);
              DrawPos.MoveAngle(PI / 2 - PInfo^.ConvAngle(Info^.Line3, False) * Pi / 18000, Round(Rect.YSize * 3 / 4));
              ProcMarkLinearOrtho(@Self, DrawPos, MousePos)
            end;
          end;
        2:
          if ActualMen = mn_DistMark then
          begin
            if ConstructOpt.lMeasureFrom2nd then
              DrawPos := Line^.LastPos
            else
              DrawPos := Line^.StartPos;
            DrawPos.MoveAngle(Line^.StartPos.CalculateAngle(Line^.LastPos), Info^.Line3);
            ProcMarkLinearOrtho(@Self, DrawPos, MousePos);
          end
          else
          begin
            if ConstructOpt.oMeasureFrom2nd then
              DrawPos := Line^.LastPos
            else
              DrawPos := Line^.StartPos;
            Angle := Line^.StartPos.CalculateAngle(Line^.LastPos);
            DrawPos.MoveAngle(Angle, Info^.XPos);
            Angle := Angle + Pi / 2;
            DrawPos.MoveAngle(Angle, Info^.YPos);
            ProcMarkLinearOrtho(@Self, DrawPos, MousePos);
          end;
      end;
    mn_CircleMark:
      case MarkState of
        0: ProcMarkCircle(@Self, DrawPos, MousePos);
        1:
          begin
            DrawPos.X := SelCircle^.StartPos.X + Info^.Line3;
            DrawPos.Y := SelCircle^.StartPos.Y;
            ProcMarkCircle(@Self, DrawPos, DrawPos);
          end;
        2:
          begin
            DrawPos := Line^.StartPos;
            DrawPos.MoveAngle(PI / 2 - PInfo^.ConvAngle(Info^.Line3, False) * Pi / 18000, SelCircle^.GetRadius);
            MsgMouseMove(DrawPos);
            ProcMarkCircle(@Self, DrawPos, DrawPos);
          end;
      end;
    mn_CircleCutMark:
      case MarkState of
        0, 1: ProcMarkCircleCut(@Self, DrawPos, MousePos);
        2:
          begin
            DrawPos.X := SelCircle^.StartPos.X + Info^.Line3;
            DrawPos.Y := SelCircle^.StartPos.Y;
            ProcMarkCircleCut(@Self, DrawPos, MousePos);
          end;
        3:
          begin
            DrawPos.X := InpCircle^.StartPos.X + Info^.Line3;
            DrawPos.Y := InpCircle^.StartPos.Y;
            ProcMarkCircleCut(@Self, DrawPos, CurPos);
          end;
      end;
    mn_AngleMark:
      case MarkState of
        0, 1: ProcMarkAngle(@Self, DrawPos, MousePos);
        2:
          begin
            if Info^.XPos <> MaxLongInt then
              ProcMarkAngle(@Self, DrawPos, MousePos)
            else
            begin
              DrawPos := Line^.StartPos;
              PInfo^.GetCurrentScreen(Rect);
              if Info^.Line3 <> MaxLongInt then
                DrawPos.MoveAngle(PInfo^.ConvAngle(360 - Info^.Line3, False) * Pi / 18000
                  + Line^.StartPos.CalculateAngle(Line^.LastPos), Round(Rect.YSize * 3 / 4))
              else
                DrawPos.MoveAngle(PI / 2 - PInfo^.ConvAngle(Info^.Line4, False) * Pi / 18000, Round(Rect.YSize * 3 / 4));
              ProcMarkAngle(@Self, DrawPos, MousePos);
            end;
          end;
        3:
          begin
            if Info^.XPos <> MaxLongInt then
              ProcMarkAngle(@Self, DrawPos, MousePos)
            else
            begin
              DrawPos := Line^.LastPos;
              PInfo^.GetCurrentScreen(Rect);
              if Info^.Line3 <> MaxLongInt then
                DrawPos.MoveAngle(PInfo^.ConvAngle(360 - Info^.Line3, False) * Pi / 18000
                  + Line^.LastPos.CalculateAngle(Line^.StartPos), Round(Rect.YSize * 3 / 4))
              else
                DrawPos.MoveAngle(Pi / 2 - PInfo^.ConvAngle(Info^.Line4, False) * Pi / 18000, Round(Rect.YSize * 3 / 4));
              ProcMarkAngle(@Self, DrawPos, MousePos);
            end;
          end;
      end;
  else
    if Assigned(MenuHandler) then
    begin
      MenuHandler.OnMouseMove(DrawPos.X, DrawPos.Y, GetShiftState);
      MenuHandler.OnMouseDown(DrawPos.X, DrawPos.Y, mbLeft, GetShiftState);
      FinishMenuHandler;
    end
    else
    begin
      if (ActualMen in [mn_Poly, mn_CPoly]) and (NewPoly^.bDrawOrtho) and (NewPoly^.Data^.Count > 1) then
      begin
        ABaseStartPoint := NewPoly^.Data^.At(NewPoly^.Data^.Count - 2);
        ABaseEndPoint := NewPoly^.Data^.At(NewPoly^.Data^.Count - 1);

        PInfo.NormalDistance(ABaseStartPoint, ABaseEndPoint, @NewPoly^.LastPos, NDist, CPos);

        Angle := ABaseStartPoint^.CalculateAngle(ABaseEndPoint^);
        BPos.Init(ABaseStartPoint^.X, ABaseStartPoint^.Y);
        BPos.MoveAngle(Angle, Info^.Line3);

        NewPoly^.DeleteLastPoint(PInfo);
        NewPoly^.PointInsert(PInfo, BPos, True);

        BPos.MoveAngle(Angle - PI / 2, (MaxInt div 2));
        NewPoly^.PointInsert(PInfo, BPos, True);

        NewPoly^.ClipRect.CorrectByPoint(BPos);
        NewPoly^.Invalidate(PInfo);
      end
      else
      begin
        MsgMouseMove(DrawPos);
        MsgLeftClick(DrawPos);
      end;
    end;
  end;
{$ENDIF} // <----------------- AXDLL
end;
{$ENDIF}

procedure TProj.CalculateSize;
begin
  SetCursor(crHourGlass);
  SetStatusText(GetLangText(4015), TRUE);
  Size.Init;
  PLayer(PInfo^.Objects)^.GetSize(Size);
  if Document.Loaded then
    Size.CorrectbyRect(Document.GetProj.Size);
  if Size.IsEmpty then
  begin
    Size.A.Init(0, 0);
    Size.B.Init(siStartX, siStartY);
  end;
  SetCursor(crDefault);
  ClearStatusText;
  UpdatePaintOffset;
  SetModified;
end;

procedure TProj.GenerateDlg;
begin
  if ExecDialog(TGenerDlg.Init(Parent, @GenerData, Layers, PInfo^.ProjStyles,
    Integer(PInfo^.CoordinateSystem.CoordinateType))) = id_OK then
    SetActualMenu(mn_GenerRef);
end;

{**************************************************************************************}
{ Procedure TProj.SetModified                                                          }
{  Wird aufgerufen, wenn die Projektdaten modifiziert wurden.
{**************************************************************************************}

procedure TProj.SetModified;
begin
  Modified := TRUE;
end;

{**************************************************************************************}
{ Procedure TProj.Select                                                               }
{--------------------------------------------------------------------------------------}
{ Selektiert ein Objekt, sofern dieses am TopLayer liegt.                              }
{**************************************************************************************}

{$IFNDEF WMLT}

procedure TProj.Select
  (
  AItem: PIndex
  );
var
  AIndex: PIndex;
begin
  if not Layers^.TopLayer^.GetState(sf_LayerOffAtGen) then
  begin
    AIndex := Layers^.TopLayer^.HasObject(AItem);
    if AIndex <> nil then
      Layers^.TopLayer^.Select(PInfo, AIndex);
  end;
end;

procedure TProj.EditOLEObject;
begin
  POLEClientObject(Layers^.SelLayer^.IndexObject(PInfo, SelList^.At(0)))^.OpenObject(1);
end;
{$ENDIF}

{initialisiert das CliREct des dem Object Aview zugehoerigen PIndex}
{muss auf allen Layern realisiert werden!!! nicht nur am TopLayer}

procedure TProj.UpdateClipRect
  (
  AView: PView
  );
var
  MIndex: TIndex;
  AItem: PIndex;
  Cnt: Integer;
  ALayer: PLayer;
begin
  MIndex.Index := AView^.Index;
  for Cnt := 0 to Layers^.LData^.Count - 1 do
  begin
    ALayer := Layers^.LData^.At(Cnt);
    AItem := ALayer^.HasObject(@MIndex);
    if AItem <> nil then
      AItem^.ClipRect.Assign(AView^.ClipRect.A.X, AView^.ClipRect.A.Y, AView^.ClipRect.B.X, AView^.ClipRect.B.Y);
  end;
end;

{$IFNDEF WMLT}

function TProj.SelectBetweenPolys
  (
  Poly1: Pointer;
  Poly2: Pointer;
  const ObjType: TObjectTypes;
  Inside: Boolean
  ): Boolean;
begin
  if not Layers^.TopLayer^.GetState(sf_LayerOffAtGen) then
    Result := Layers^.SelectBetweenPolys(PInfo, Poly1, Poly2, ObjType, Inside)
  else
    Result := FALSE;
end;
{$ENDIF}

procedure TProj.MsgKeyDown
  (
  var Key: Word;
  Shift: TShiftState
  );
begin
  if Assigned(MenuHandler) then
  begin
    MenuHandler.OnKeyDown(Key, Shift);
    FinishMenuHandler;
  end
  else
  begin
    case Key of
      vk_Return:
        begin
          MsgLeftClick(CurPos);
          MsgLeftUp(CurPos);
          MsgLeftDblClick(CurPos);
        end;
      vk_Escape: BreakInput(TRUE, FALSE);
      107: ZoomByFactor(2.0);
      109: ZoomByFactor(0.5);
    end;
  end;
end;

procedure TProj.MsgKeyUp
  (
  var Key: Word;
  Shift: TShiftState
  );
begin
{++ Ivanoff NA1 BUILD#106}
{If Ctrl+Z has been pressed, we have to delete last input point for lines and polygon.}
{For Internal database it also can means an user wants to do undeleting operation.}

  if (ssCTRL in Shift) and (Key = VK_TAB) and (CurSnapPoly <> nil) and (NewPoly <> nil) then
  begin
    NewPoly^.DirFlag := not NewPoly^.DirFlag;
    NewPoly^.Reset(PInfo);
    NewPoly^.DrawAlong(PInfo, CurSnapPoly);
  end;

  if (ssCTRL in Shift) and ((Chr(Key) = 'z') or (Chr(Key) = 'Z')) then
    case SELF.ActualMen of
      mn_EntToDrawAlong, // Glukhov Bug#468 Build#162 16.08.01
        mn_Poly, mn_CPoly:
        begin;
          NewPoly.DeleteLastPoint(SELF.PInfo);
        {+++brovak bug 105 build 132}
          NewPolyP.EndIt(SELF.PInfo);
        {brovak}
        end;

//{$IFDEF IDB_UNDO}
{++ IDB}
{        mn_Select: If WinGisMainForm.bWeAreUsingUndoFunction Then
                      WinGisMainForm.IDB_Man.RestoreDeletingData(@SELF);}
{-- IDB}
//{$ENDIF}
    end; // Case end

{-- Ivanoff}

  if (ssCTRL in Shift) and (KEY = VK_F12) then
  begin
    ProcExportLayerManager(@Self);
  end;

  if Assigned(MenuHandler) then
  begin
    MenuHandler.OnKeyUp(Key, Shift);
    FinishMenuHandler;
  end;
end;

{******************************************************************************+
  Procedure TProj.OnMouseMove
--------------------------------------------------------------------------------
+******************************************************************************}

procedure TProj.OnMouseMove(const X, Y: Integer; Shift: TShiftState);
var
{++ Ivanoff BUG#91}
  ALeftLowerPoint,
{-- Ivanoff BUG#91}
  Pos: TDPoint;
  SnapObject: PIndex;
begin
  IgnoreClick := False;
  if (DigitMode) and (ActualMen <> mn_RefPoints) and (ActualMen <> mn_Zoom) and (ActualMen <> mn_Select) then
    exit;
  MouseHintPos.x := X;
  MouseHintPos.y := Y + 25;
  PInfo^.MsgToDrawPos(X, Y, Pos);
  // tell tooltipthread mouse position {03-140199-001}
  TooltipThread.Mouse(@Self, X, Y);
  // end tooltip modification  // end tooltip modification

{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
  if GetTickCount < LastSnapMarkerTickCount then
    LastSnapMarkerTickCount := 0;
  if (WingisMainForm.UseMarkSnapPoint)
    and (SnapState)
    and (GetTickCount - LastSnapMarkerTickCount > 250)
    and (ActualMen <> mn_Select)
    and (ActualMen <> mn_Zoom)
    and ((ActualMen <> mn_CircleMark) or (MarkState <= 1))
    and (ActualMen <> mn_MovePartPara)
    and (ActualMen <> mn_MovePartPuff) then
  begin
    LastSnapMarkerTickCount := GetTickCount;
{++ Ivanoff BUG#91}
    try
      if (MenuHandler <> nil) and (MenuHandler.SubHandler is TSelectPrintRangeHandler) then
      begin
        ALeftLowerPoint.Init(Round(TSelectPrintRangeHandler(MenuHandler.SubHandler).SnapSencePointX), Round(TSelectPrintRangeHandler(MenuHandler.SubHandler).SnapSebcePointY));
        MarkSnapPoint(ALeftLowerPoint);
      end
      else
{-- Ivanoff BUG#91}
        MarkSnapPoint(Pos);
{++ Ivanoff BUG#91}
    except
      MarkSnapPoint(Pos);
    end;
{-- Ivanoff BUG#91}
  end;

{$ENDIF} // <----------------- AXDLL
{$ENDIF}
  if MaxSize.PointInside(Pos) then
  begin
    if Assigned(MenuHandler) then
    begin
      MenuHandler.OnMouseMove(Pos.X, Pos.Y, Shift);
      FinishMenuHandler;
    end
    else
    begin
      MsgMouseMove(Pos);
    end;
  end;

  if SnapState then
  begin
    SnapPoint(Pos, 0, ot_Any, Pos, SnapObject);
  end;

  StatusBar[0] := PInfo^.CoordinateSystem.FormatCoordinates(Pos.X, Pos.Y, @Self);

  if CoordMsgTarget <> 0 then
  begin
    if (CurPos.x <> Pos.x) or (CurPos.Y <> Pos.y) then
    begin
      PostMessage(CoordMsgTarget, WM_WGMousePos, Pos.x, Pos.y);
    end;
  end;

  CurPos.X := Pos.x;
  CurPos.Y := Pos.Y;
end;

{******************************************************************************+
  Procedure TProj.OnMouseDown
--------------------------------------------------------------------------------
+******************************************************************************}

procedure TProj.OnMouseDown(const X, Y: Integer; Button: TMouseButton;
  Shift: TShiftState);
{$IFNDEF AXDLL} // <----------------- AXDLL
var
  Pos: TDPoint;
  FlagAllow: boolean;
{$ENDIF} // <----------------- AXDLL
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  if (IgnoreClick) or ((DigitMode) and (ActualMen <> mn_RefPoints) and (Button = mbLeft) and (ActualMen <> mn_Select)) then
    exit;
  KillTimer(Parent.Handle, idt_Popup);
  CurSnapPoly := nil;
  IsZoomingNow := false;
  PInfo^.MsgToDrawPos(X, Y, Pos);
  if MaxSize.PointInside(Pos) then
  begin
    if ZoomRect^.IsOn then
    begin
      ZoomRect^.EndIt(PInfo);
      SetActualMenu(mn_Select);
      BreakInput(TRUE, FALSE)
    end;
    if Assigned(MenuHandler) then
    begin
     {+++ Brovak BUG 524,539 BUILD 167}
      FlagAllow := false;
      if Menuhandler.AllowDblclick = true then
        FlagAllow := true
      else
        if MenuHandler.SubHandler <> nil then
          if MenuHandler.SubHandler.AllowDblclick = true then
            FlagAllow := true;
      if (FlagAllow = true) and (ssRight in Shift) then
        case ClickCnt1 of
          1:
            begin; //it is really right doubleclick
              KillTimer(Parent.Handle, idt_Mouse);
              ClickCnt1 := 0;
              if (IniFile^.MouseZoom) and (bMouseZoom) then
                ZoomByFactorWithCenter(Pos.X, Pos.Y, 0.5);
              if MenuHandler.SubHandler <> nil then
                MenuHandler.SubHandler.NeedZoom := true;
            end;
          0: // we will wait, may be it will doubleclick ?
            begin;
              SetTimer(Parent.Handle, idt_Mouse, GetDoubleClickTime + 1, nil);
              ClickCnt1 := 1;
            end;
        end; //case
       {-- Brovak }

      MenuHandler.OnMouseDown(Pos.X, Pos.Y, Button, Shift);
      FinishMenuHandler;
    end
    else
      if ssDouble in Shift then
      begin
        if Button = mbLeft then
          MsgLeftDblClick(Pos)
        else
          if Button = mbRight then
          begin
            KillTimer(Parent.Handle, idt_Mouse);
            MsgRightDblClick(Pos);
        {++brovak for zoom}
            IsZoomingNow := true;
        {--brovak}
          end;
      end
      else
      begin
        if Button = mbLeft then
        begin
          KillTimer(Parent.Handle, idt_Mouse);
          MsgLeftClick(Pos)
        end
        else
          if Button = mbRight then
          begin
  {++brovak. Changes for zooming when an user is using painting instruments. }
            IsZoomingNow := true;
            OldMenu := ActualMen;
            OldCursor := Pinfo^.Cursor;
            if ActualMen = mn_Select then
              ZoomOnSelect := true
            else
              ZoomOnSelect := false;
            SetTimer(Parent.Handle, idt_Mouse, GetDoubleClickTime + 1, nil);
            MsgRightClick(Pos);
          end;
  {--brovak}
      end;
  end;
{$ENDIF} // <----------------- AXDLL
end;

{******************************************************************************}
{ Procedure TProj.OnMouseUp                                                    }
{------------------------------------------------------------------------------}
{******************************************************************************}

procedure TProj.OnMouseUp
  (
  const X: Integer;
  const Y: Integer;
  Button: TMouseButton;
  Shift: TShiftState
  );
{$IFNDEF AXDLL} // <----------------- AXDLL
var
  Pos: TDPoint;
  FlagAllow: boolean;
{$ENDIF} // <----------------- AXDLL
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  PInfo^.MsgToDrawPos(X, Y, Pos);
  if MaxSize.PointInside(Pos) then
  begin
    if Assigned(MenuHandler) then
    begin
      FlagAllow := false;
      if Menuhandler.AllowDblclick = true then
        FlagAllow := true
      else
        if MenuHandler.SubHandler <> nil then
          if MenuHandler.SubHandler.AllowDblclick = true then
            FlagAllow := true;

      if not MenuHandler.OnMouseUp(Pos.X, Pos.Y, Button, Shift)
        and (Button = mbRight)
        and
         {+++ Brovak BUG 524,539 BUILD 167}
      (FlagAllow = false)
         {-- Brovak }then
{$IFNDEF WMLT}ShowPopup{$ENDIF};
      FinishMenuHandler;
    end
    else
      if Button = mbLeft then
        MsgLeftUp(Pos)
  {+++brovak Changes for zooming when an user is using painting instruments.}

      else
        if Button = mbRight then
        begin
          MsgRightUp(Pos);
          ActualMen := OldMenu;
          SetCursor(OldCursor);
{$IFNDEF WMLT}
          if not (ActualMen in [mn_PartFree, mn_PartParallel, mn_MovePartPara, mn_Select, mn_Text, mn_SizeRot, mn_SymbolInp])
            and (IsZoomingNow = false) then
            ShowPopup
          else
{$ENDIF}
            IsZoomingNow := false;
        end;
  end;
     {--brovak}
{$ENDIF} // <----------------- AXDLL
end;

{*******************************************************************************
| Procedure TProj.EnableMenus
|-------------------------------------------------------------------------------
+******************************************************************************}

procedure TProj.EnableMenus;
begin
  if Assigned(MenuHandler) then
    MenuHandler.OnEnableMenus;
end;

{******************************************************************************+
  Procedure TProj.DefaultMenuHandler
--------------------------------------------------------------------------------
  Wird vom Men�-System aufgerufen, wenn ein Men�punkt gew�hlt wird.
+******************************************************************************}

procedure TProj.DefaultMenuHandler(MenuFunction: TMenuFunction);
var
  AHandlerClass: TMenuHandlerClass;
  AMenuHandler: TMenuHandler;
  AIndex: Integer;
  AName: string;
  Cnt: Integer;
begin
  AName := MenuFunction.Name;
  // look if current menu-handler supports the menu-function
  if (MenuHandler <> nil) and (CallMethodByName(MenuHandler, AName)) then
    Exit;
  // if menu-point currently active then call reinvoke-method
  if (MenuHandler <> nil) and (CompareText(MenuHandler.MenuName, AName) = 0) then
  begin
    MenuHandler.OnReinvoke;
        // check if menu-handler has finished it's work
    FinishMenuHandler;
    Exit;
  end;
  { registrierten Men�handler suchen }
  AHandlerClass := MenuHandlers[AName];
  if Assigned(AHandlerClass) then
  begin
    if (AHandlerClass.HandlerType and $FF) in [mhtReturnToPushed, mhtReturnAndReset, mhtReturnOrClear] then
    begin
              { type is mhtReturnToPushed or mhtReturnAndReset or mhtReturnOrClear ->
              + active menu-handler must be removed }
      if MenuHandler <> nil then
      begin
        MenuHandler.OnDeActivate;
        MenuHandler.OnEnd;
        MenuFunctions[MenuHandler.MenuName].Checked := False;
        MenuHandler.Free;
        MenuHandler := nil;
      end;
              { Men�handler am Stack suchen }
      AIndex := MenuHandlerStack.Count - 1;
      while AIndex >= 0 do
      begin
        if (CompareText(TMenuHandler(MenuHandlerStack[AIndex]).MenuName, AName) = 0) then
          break;
        Dec(AIndex);
      end;
      if (AIndex >= 0) or ((AHandlerClass.HandlerType and $FF) = mhtReturnOrClear) then
        for Cnt := MenuHandlerStack.Count - 1 downto AIndex + 1 do
        begin
          AMenuHandler := MenuHandlerStack[Cnt];
          AMenuHandler.OnDeActivate;
          AMenuHandler.OnEnd;
          AMenuHandler.Free;
          MenuHandlerStack.Delete(Cnt);
        end;
              { Men�handler am Stack gefunden->aktivieren }
      if AIndex >= 0 then
      begin
        MenuHandler := MenuHandlerStack[AIndex];
        MenuHandlerStack.Delete(AIndex);
        if AHandlerClass.HandlerType = mhtReturnAndReset then
        begin
                          { Reset der Men�funktion wird derzeit durch Neuerzeugung simuliert }
          MenuHandler.OnDeActivate;
          MenuHandler.OnEnd;
          MenuHandler.Free;
          MenuFunctions[MenuHandler.MenuName].Checked := False;
          MenuHandler := nil;
        end
        else
        begin
                          { zugeh�rige Men�button setzen, UserInterface updaten }
          MenuHandler.OnActivate;
          MenuFunctions[MenuHandler.MenuName].Checked := TRUE;
          UserInterface.Update([uiMenus, uiCursor, uiStatusBar]);
          Exit;
        end;
      end;
    end
    else
      if (MenuHandler <> nil) and (AHandlerClass.HandlerType and $FF = mhtPushActive)
        and (MenuHandler.HandlerType and mhtDontPush <> 0) then
      begin
        MenuFunctions[MenuHandler.MenuName].Checked := FALSE;
                 { Typ ist mhtPushActive und aktiver Men�hander darf auf den Stack gelegt werden }
        with MenuHandlerStack do
          if (Count > 0) and (TMenuHandler(Items[Count - 1]).MenuName = MenuFunction.Name) then
          begin
            MenuHandler.OnDeActivate;
            MenuHandler.OnEnd;
            MenuHandler.Free;
            MenuHandler := Items[Count - 1];
            Delete(Count - 1);
            MenuFunctions[MenuHandler.MenuName].Checked := TRUE;
            UserInterface.Update([uiMenus, uiCursor, uiStatusBar]);
            Exit;
          end
          else
          begin
            MenuHandler.OnDeActivate;
            MenuHandler.OnEnd;
            MenuHandler.Free;
            MenuHandler := nil;
          end;
      end;
    AMenuHandler := AHandlerClass.Create(nil);
    if AMenuHandler is TProjMenuHandler then
      TProjMenuHandler(AMenuHandler).Project := @Self;
    AMenuHandler.Parent := Parent;
    AMenuHandler.Canvas := Parent.Canvas;
    AMenuHandler.OnStart;
    if AMenuHandler.Status <> mhsNotFinished then
    begin
      AMenuHandler.Free;
      Exit;
    end;
    MenuFunctions[GetMenuFunctionName].Checked := FALSE;
    case AMenuHandler.HandlerType and $FF of
      mhtPushActive:
        if MenuHandler <> nil then
        begin
          MenuHandler.OnDeActivate;
          MenuHandlerStack.Add(MenuHandler);
        end;
      mhtReturnOrClear:
        begin
          ClearMenuHandlers;
          //ActualMen := 0;
        end;
    else
      ActualMen := 0;
    end; // End of CASE-statement
    MenuHandler := AMenuHandler;
    MenuHandler.OnActivate;
    UserInterface.Update([uiMenus, uiCursor, uiStatusBar]);
  end;
end;

procedure TProj.UpdateStatusBar;
begin
//++ Glukhov Bug#66 BUILD#128 19.09.00
// The data are set when clicked item "Show next selected"
// Process the data
  if bShowNum then
  begin
    if (nSelObjs <> SelList^.GetCount)
      or (ZoomRotRect.Width <> CurrentViewRect.Width)
      or (ZoomRotRect.Height <> CurrentViewRect.Height)
      or (ZoomRotRect.X <> CurrentViewRect.X)
      or (ZoomRotRect.Y <> CurrentViewRect.Y) then
    begin
      bShowNum := False;
      ClearStatusText;
    end;
  end;
//-- Glukhov Bug#66 BUILD#128 19.09.00
  if Assigned(MenuHandler) then
    StatusBar[1] := MenuHandler.StatusText
  else
    if StatusText.Count > 0 then
      StatusBar[1] := StatusText[StatusText.Count - 1]
    else
      StatusBar[1] := Layers^.SelLayer^.SelInfo.StatusText;
  StatusBar[2] := 'M 1:' + FormatStr(1 / PInfo^.ZoomToScale(PInfo^.Zoom), 0);
end;

{******************************************************************************+
  Procedure TProj.SetStatusText
--------------------------------------------------------------------------------
  Setzt den Text in der Statuszeile.
--------------------------------------------------------------------------------
  AText = Neuer Text
  ASave = Gibt an, ob der Text gesichert werden soll.
+******************************************************************************}

procedure TProj.SetStatusText
  (
  const AText: string;
  ASave: Boolean
  );
begin
  if ASave then
    StatusText.Add(AText)
  else
    if StatusText.Count > 0 then
      StatusText[StatusText.Count - 1] := AText;
  UserInterface.Update([uiStatusBar]);
end;

{******************************************************************************+
  Procedure TProj.ClearStatusText
--------------------------------------------------------------------------------
  L�scht den Text in der Statuszeile.
+******************************************************************************}

procedure TProj.ClearStatusText;
begin
  if StatusText.Count > 0 then
  begin
    StatusText.Delete(StatusText.Count - 1);
    UserInterface.Update([uiStatusBar]);
  end
  else
    {MessageDialog('Fehler in der Statuszeile', mtError, [mbOK], 0)};
end;

{******************************************************************************+
  Function TProj.Cursor
--------------------------------------------------------------------------------
  Liefert den aktuell gesetzten Cursor zur�ck.
+******************************************************************************}

function TProj.Cursor: TCursor;
begin
  if MenuHandler <> nil then
    Result := MenuHandler.Cursor
  else
    Result := PInfo^.Cursor;
end;

{******************************************************************************+
  Procedure TProj.FinishMenuHandler
--------------------------------------------------------------------------------
  Entfernt den aktuellen Men�handler. Ist noch ein Men�handler am Stack
  vorhanden, so wird dieser aktiviert und das User-Interface entsprechend
  upgedated.
+******************************************************************************}

function TProj.FinishMenuHandler: Boolean;
var
  AStatus: TMenuHandlerStatus;
begin
  Result := TRUE;
  if (MenuHandler <> nil) and (MenuHandler.Status <> mhsNotFinished) then
  begin
    MenuHandler.OnDeActivate;
    MenuHandler.OnEnd;
    MenuFunctions[GetMenuFunctionName].Checked := FALSE;
    AStatus := MenuHandler.Status;
    MenuHandler.Free;
    MenuHandler := nil;
    if AStatus = mhsReturnToDefault then
      SetActualMenu(mn_Select)
    else
      with MenuHandlerStack do
      begin
        if Count > 0 then
        begin
          MenuHandler := MenuHandlerStack[Count - 1];
          MenuHandler.OnActivate;
          Delete(Count - 1);
        end;
        MenuFunctions[GetMenuFunctionName].Checked := TRUE;
        if ActualMen = mn_Select then
          MenuFunctions['EditSelect'].Checked := TRUE;
        UserInterface.Update([uiMenus, uiCursor, uiStatusBar]);
      end;
  end;
end;

{******************************************************************************+
  Procedure TProj.ClearMenuHandlers
--------------------------------------------------------------------------------
  Removes and frees all MenuHandlers from the stack (and also the active).
+******************************************************************************}

procedure TProj.ClearMenuHandlers;

  procedure FreeMenuHandler
      (
      AHandler: TMenuHandler
      );
  begin
    if AHandler <> nil then
    begin
      AHandler.OnEnd;
      AHandler.Free;
    end;
  end;
var
  Cnt: Integer;
begin
  if MenuHandler <> nil then
  begin
    MenuHandler.OnDeActivate;
    MenuFunctions[MenuHandler.MenuName].Checked := FALSE;
  end;
  FreeMenuHandler(MenuHandler);
  MenuHandler := nil;
  if MenuHandlerStack.Count > 1 then
    for Cnt := 0 to MenuHandlerStack.Count - 1 do
      FreeMenuHandler(MenuHandlerStack[Cnt]);
end;

{******************************************************************************+
  Procedure TProj.ReadSettingsFromRegistry
--------------------------------------------------------------------------------
  Reads the settings stored in the registry-database into the internal
  variables of the project.
+******************************************************************************}

procedure TProj.ReadSettingsFromRegistry;
var
  ActiveLayerName: string;
  ALayer: PLayer;
  AGuid: TGUID;
begin
  with Registry do
  begin
    OpenKey('\Project\Settings', FALSE);

    ActiveLayerName := ReadString('ActiveLayerName');
    if ActiveLayerName <> '' then
    begin
      ALayer := Layers^.NameToLayer(ActiveLayerName);
      if ALayer <> nil then
      begin
        Layers^.TopLayer := ALayer;
      end;
    end;

    ProjGuid := '';
    ProjGuid := ReadString('ProjGUID');
    if ProjGuid = '' then
    begin
      if CoCreateGuid(AGuid) = S_OK then
      begin
        ProjGuid := GuidToString(AGuid);
        SetModified;
      end;
    end;

    Deg400Mode := ReadBool('Deg400Mode');

        {++ Brovak for set layer's fix password - moved from ini to registry}
        {section called by FillSetProp for dummy hacker ;)) }
    FixLayerPassword := ReadString('FillSetProp');

         {--Brovak}

      // scale factor
    PInfo^.ProjectScale := ReadFloat('Scale');
    if LastError <> rdbeNoError then
      PInfo^.ProjectScale := 1 / 10000;
      // project units
    PInfo^.SetProjectUnits(TMeasureUnits(ReadInteger('Units')));
    if LastError <> rdbeNoError then
      PInfo^.SetProjectUnits(muMeters);
      // coordinate system
    PInfo^.CoordinateSystem.CoordinateType := TCoordinateType(ReadInteger('CoordinateSystem'));
    if (LastError <> rdbeNoError) then
      PInfo^.CoordinateSystem.CoordinateType := ctCarthesian;
      // projection settings
    with PInfo^.ProjectionSettings do
    begin
      ProjectProjection := ReadString('ProjProjection');
      ProjectDate := ReadString('ProjDate');
      ProjectProjSettings := Readstring('ProjProjectionSettings');
      ProjectionXOffset := ReadFloat('ProjectionXOffset');
      ProjectionYOffset := ReadFloat('ProjectionYOffset');
      //added by Brovak
      ProjectionScale := ReadFloat('ProjectionScale');

      if ProjectProjection = '' then
        ProjectProjection := 'NONE';
      if ProjectDate = '' then
        ProjectDate := 'NONE';
      if ProjectProjSettings = '' then
        ProjectProjSettings := 'NONE';
      if ProjectionScale = 0 then
        ProjectionScale := 1;
      ShowCoordInDegrees := ReadBool('ShowCoordInDegrees');
    end;
      // color palette
    PInfo^.ProjStyles.Palette.ReadFromRegistry(Registry, '\Project\Palette');
      // extended line-styles
    PInfo^.ProjStyles.XLineStyles.ReadFromRegistry(Registry, '\Project\XLineStyles');
      // extended fill-styles
    if FileVersion >= 20 then
      PInfo^.ProjStyles.XFillStyles.
        ReadFromRegistry(Registry, '\Project\XFillStyles');
      // selection options
    with PInfo^.SelectionSettings do
    begin
      OpenKey('\Project\Settings\Selection', FALSE);
      UseGlobals := ReadBool('UseGlobals');
      if LastError <> rdbeNoError then
        UseGlobals := TRUE;
      if not UseGlobals then
      begin
        DBSelectionMode := ReadBool('DBSelectionMode');
        TransparentMode := ReadInteger('TransparentMode');
        ReadLineStyleFromRegistry(Registry, '\Project\Settings\Selection\LineStyle', LineStyle);
        ReadFillStyleFromRegistry(Registry, '\Project\Settings\Selection\FillStyle', FillStyle);
      end;
    end;
      // various options
    with PInfo^.VariousSettings do
    begin
      OpenKey('\Project\Settings\Various', FALSE);
      UseGlobals := ReadBool('UseGlobals');
      if LastError <> rdbeNoError then
        UseGlobals := TRUE;
      if not UseGlobals then
      begin
        ShowEdgePoints := ReadBool('ShowEdgePoints');
        ZoomSize := ReadFloat('ZoomSize');
        ZoomSizeType := ReadInteger('ZoomSizeType');
        CombineRadius := ReadFloat('CombineRadius');
        CombineRadiusType := ReadInteger('CombineRadiusType');
        SnapToNearest := ReadBool('SnapToNearest');
        SnapRadius := ReadFloat('SnapRadius');
        SnapRadiusType := ReadInteger('SnapRadiusType');
          {++ Brovak BUG#574 BUILD#108 - Additional Param Set for draw/no draw rect for hidden text }
        ShowHiddenText := ReadBool('ShowHiddenText');
          {-- Brovak }
//++ Glukhov Bug#199 Build#157 14.05.01
        // Arc (Circle) conversion settings
        bConvRelMistake := ReadBool('ConvertRelPrecision');
{$IFNDEF AXDLL} // <----------------- AXDLL
        if LastError <> rdbeNoError then
          bConvRelMistake := IniFile^.VariousSettings.bConvRelMistake;
{$ENDIF} // <----------------- AXDLL
        dbConvRelMistake := ReadFloat('ConvertRelPrecValue');
{$IFNDEF AXDLL} // <----------------- AXDLL
        if LastError <> rdbeNoError then
          dbConvRelMistake := IniFile^.VariousSettings.dbConvRelMistake;
{$ENDIF} // <----------------- AXDLL
        dbConvAbsMistake := ReadFloat('ConvertAbsPrecValue');
{$IFNDEF AXDLL} // <----------------- AXDLL
        if LastError <> rdbeNoError then
          dbConvAbsMistake := IniFile^.VariousSettings.dbConvAbsMistake;
{$ENDIF} // <----------------- AXDLL
//-- Glukhov Bug#199 Build#157 14.05.01
      end;
//++ Glukhov ObjAttachment 23.10.00
{$IFNDEF AXDLL} // <----------------- AXDLL
      bDelAttached := IniFile^.VariousSettings.bDelAttached;
      bDelAttachedAsk := IniFile^.VariousSettings.bDelAttachedAsk;
{$ENDIF} // <----------------- AXDLL
//-- Glukhov ObjAttachment 23.10.00
    end;
      // bitmap options
    with PInfo^.BitmapSettings do
    begin
      OpenKey('\Project\Settings\Bitmaps', FALSE);
      UseGlobals := ReadBool('UseGlobals');
      if LastError <> rdbeNoError then
        UseGlobals := TRUE;
      if not UseGlobals then
      begin
        ShowBitmap := ReadBool('ShowBitmap');
        ShowFrame := ReadBool('ShowFrame');
        ShowMinSize := ReadInteger('ShowMinSize');
        Transparency := ReadInteger('Transparency');
        TransparencyType := ReadInteger('TransparencyType');
//++ Glukhov Bug#236 Build#144 04.12.00
        GlobInvisiblity := ReadBool('GlobInvisibility');
        if LastError <> rdbeNoError then
          GlobInvisiblity := False;
//-- Glukhov Bug#236 Build#144 04.12.00
      end;
    end;
      // font options
    with PInfo^.FontSettings do
    begin
      OpenKey('\Project\Settings\Fonts', FALSE);
      UseGlobals := ReadBool('UseGlobals');
      if LastError <> rdbeNoError then
        UseGlobals := TRUE;
    end;

      // annotation options
    with PInfo^.AnnotSettings do
    begin
      OpenKey('\Project\Settings\Annotations', FALSE);
      if LastError = rdbeNoError then
      begin
        AnnotLayer := ReadInteger('AnnotationLayer');
        AnnotFont.Init;
          {AnnotFont:=ActualFont;}
        AnnotFont.Font := ReadInteger('AnnotationFont');
        AnnotFont.Style := ReadInteger('AnnotationStyle');
        AnnotFont.Height := ReadInteger('AnnotationHeight');
        Angle := ReadFloat('AnnotationAngle');
        Color := ReadInteger('AnnotationColor');
        Position := ReadInteger('AnnotationPosition');
        ShowDialog := ReadBool('AnnotationShowDialog');
{++ Ivanoff}
        KeepLinkToDBAlive := FALSE; // ReadBool('KeepLinkToDBAlive');
{-- Ivanoff}
      end
      else
      begin
        AnnotLayer := 0;
        AnnotFont.Init;
        AnnotFont := ActualFont;
        Angle := 0;
        Color := 0;
        Position := 0;
        ShowDialog := TRUE;
{++ Ivanoff}
        KeepLinkToDBAlive := FALSE;
{-- Ivanoff}
      end;
    end;
       {brovak}
          // printframe options
    with FramePrintSpec do
    begin
      OpenKey('\Project\Settings\SpecFrame', FALSE);
      if LastError = rdbeNoError then
      begin
        UseSpecFrame := ReadBool('PrintMarkFrame');
        Font.Init;
        UseTextMarks := ReadBool('UseText');
        Font.Font := ReadInteger('Font');
        Font.Style := ReadInteger('Style');
        Font.Height := ReadInteger('Height');
        Color := ReadInteger('Color');
        BackColor := ReadInteger('BackColor');

        UseLeft := ReadBool('Left');
        UseRight := ReadBool('Right');
        UseTop := ReadBool('Top');
        UseBottom := ReadBool('Bottom');

        StepLength := ReadFloat('Step');
        MarksLength := ReadFloat('MarkLength');
        UseCross := ReadBool('UseCrosses');
        CrossLength := ReadFloat('CrossLength');
        CrossColor := ReadInteger('CrossColor');
        GridColor := ReadInteger('GridColor');
      end
      else
      begin
        UseSpecFrame := false;
        Font.Init;
        Font.Font := 0; //haven't font
        UseTextMarks := false;
        Color := ClBlack;
        BackColor := ClWhite;

        UseLeft := false;
        UseRight := false;
        UseTop := false;
        UseBottom := false;

        StepLength := 0;
        MarksLength := 500;
        UseCross := false;
        CrossLength := 1000;
        CrossColor := clBlack;
        GridColor := clBlack;
      end;
    end;

       {brovak}

      // read floating text settings
    OpenKey('\Project\Settings\FloatingText', False);
    if ReadBool('Enabled') then
      TooltipLayer := Layers^.NameToLayer(
        ReadString('CurrentLayer'));
      // read legends
    Legends := TLegendList.ReadFromRegistry(Registry, '\Project\Legends');
{++ IDB}
{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}
    if SELF.IDBSettings <> nil then
    begin
      if Registry.KeyExists(cs_Projectregistry_IDB_LocalSettings_KeyName) then
      begin
        OpenKey(cs_Projectregistry_IDB_LocalSettings_KeyName, TRUE);
        SELF.IDBSettings.bUseGlobalIDBSettings := ReadBool('UseGlobal');
        SELF.IDBSettings.bLocalWeAreUsingTheIDB := ReadBool('UseIDB');
        SELF.IDBSettings.bLocalWeAreUsingTheIDBUserInterface := ReadBool('UseIDBInterface');
        SELF.IDBSettings.bLocalWeAreUsingUndoFunction := ReadBool('UseIDBUndo');
        SELF.IDBSettings.iLocalUndoStepsCount := ReadInteger('IDBUndoStepsCount');
      end
      else
      begin
        SELF.IDBSettings.bUseGlobalIDBSettings := TRUE;
        SELF.IDBSettings.bLocalWeAreUsingTheIDB := FALSE;
        SELF.IDBSettings.bLocalWeAreUsingTheIDBUserInterface := FALSE;
        SELF.IDBSettings.bLocalWeAreUsingUndoFunction := FALSE;
        SELF.IDBSettings.iLocalUndoStepsCount := 500;
      end;
    end;
{$ENDIF}
{$ENDIF} // <----------------- AXDLL
{-- IDB}
  end;
end;

{******************************************************************************+
  Procedure TProj.WriteSettingsToRegistry
--------------------------------------------------------------------------------
  Writes all settings normally stored in the registry but copied to internal
  variables back to the registry
+******************************************************************************}

procedure TProj.WriteSettingsToRegistry;
begin
  with Registry do
  begin
    OpenKey('\Project\Settings', TRUE);

    WriteString('ActiveLayerName', Layers^.TopLayer^.Text^);
    WriteString('ProjGUID', ProjGuid);
    WriteBool('Deg400Mode', Deg400Mode);

          {++ Brovak for set layer's fix password - moved from ini to registry}
        {name section for dummy hacker}
    WriteString('FillSetProp', FixLayerPassword);
       {--Brovak}

      // project scale
    WriteFloat('Scale', PInfo^.ProjectScale);
      // project units
    WriteInteger('Units', Integer(PInfo^.ProjectUnits));
      // coordinate system
    WriteInteger('CoordinateSystem', Integer(PInfo^.CoordinateSystem.CoordinateType));
      // projection settings
    with PInfo^.ProjectionSettings do
    begin
      WriteString('ProjProjection', ProjectProjection);
      WriteString('ProjDate', ProjectDate);
      WriteString('ProjProjectionSettings', ProjectProjSettings);
      WriteFloat('ProjectionXOffset', ProjectionXOffset);
      WriteFloat('ProjectionYOffset', ProjectionYOffset);
      WriteFloat('ProjectionScale', ProjectionScale);
      //added by Brovak
      WriteBool('ShowCoordInDegrees', ShowCoordInDegrees);
    end;
      // color palette
    PInfo^.ProjStyles.Palette.WriteToRegistry(Registry, '\Project\Palette');
      // extended line-styles
    PInfo^.ProjStyles.XLineStyles.WriteToRegistry(Registry, '\Project\XLineStyles');
      // extended fill-styles
    PInfo^.ProjStyles.XFillStyles.WriteToRegistry(Registry, '\Project\XFillStyles');
      // selection options
    with PInfo^.SelectionSettings do
    begin
      OpenKey('\Project\Settings\Selection', TRUE);
      WriteBool('UseGlobals', UseGlobals);
      WriteBool('DBSelectionMode', DBSelectionMode);
      WriteInteger('TransparentMode', TransparentMode);
      WriteLineStyleToRegistry(Registry, '\Project\Settings\Selection\LineStyle', LineStyle);
      WriteFillStyleToRegistry(Registry, '\Project\Settings\Selection\FillStyle', FillStyle);
    end;
      // various options
    with PInfo^.VariousSettings do
    begin
      OpenKey('\Project\Settings\Various', TRUE);
      WriteBool('UseGlobals', UseGlobals);
      WriteBool('ShowEdgePoints', ShowEdgePoints);
      WriteFloat('ZoomSize', ZoomSize);
      WriteInteger('ZoomSizeType', ZoomSizeType);
      WriteFloat('CombineRadius', CombineRadius);
      WriteInteger('CombineRadiusType', CombineRadiusType);
      WriteBool('SnapToNearest', SnapToNearest);
      WriteFloat('SnapRadius', SnapRadius);
      WriteInteger('SnapRadiusType', SnapRadiusType);
       {++ Brovak BUG#574 BUILD#108 - Additional Param Set for draw/no draw rect for hidden text }
      WriteBool('ShowHiddenText', ShowHiddenText);
       {-- Brovak}
//++ Glukhov Bug#199 Build#157 14.05.01
      // Arc (Circle) conversion settings
      WriteBool('ConvertRelPrecision', bConvRelMistake);
      WriteFloat('ConvertRelPrecValue', dbConvRelMistake);
      WriteFloat('ConvertAbsPrecValue', dbConvAbsMistake);
//-- Glukhov Bug#199 Build#157 14.05.01
    end;
      // bitmap options
    with PInfo^.BitmapSettings do
    begin
      OpenKey('\Project\Settings\Bitmaps', TRUE);
      WriteBool('UseGlobals', UseGlobals);
      WriteBool('ShowBitmap', ShowBitmap);
      WriteBool('ShowFrame', ShowFrame);
      WriteInteger('ShowMinSize', ShowMinSize);
      WriteInteger('Transparency', Transparency);
      WriteInteger('TransparencyType', TransparencyType);
//++ Glukhov Bug#236 Build#144 04.12.00
      WriteBool('GlobInvisibility', GlobInvisiblity);
//-- Glukhov Bug#236 Build#144 04.12.00
    end;
      // font options
    with PInfo^.FontSettings do
    begin
      OpenKey('\Project\Settings\Fonts', TRUE);
      WriteBool('UseGlobals', UseGlobals);
    end;

      // annotation options
    with PInfo^.AnnotSettings do
    begin
      OpenKey('\Project\Settings\Annotations', TRUE);
      WriteInteger('AnnotationLayer', AnnotLayer);
      WriteInteger('AnnotationFont', AnnotFont.Font);
      WriteInteger('AnnotationStyle', AnnotFont.Style);
      WriteInteger('AnnotationHeight', AnnotFont.Height);
      WriteFloat('AnnotationAngle', Angle);
      WriteInteger('AnnotationColor', Color);
      WriteInteger('AnnotationPosition', Position);
      WriteBool('AnnotationShowDialog', ShowDialog);
{++ Ivanoff}
//      WriteBool('KeepLinkToDBAlive', KeepLinkToDBAlive);
{-- Ivanoff}
    end;
{brovak}

    with FramePrintSpec do
    begin
      OpenKey('\Project\Settings\SpecFrame', TRUE);
      WriteBool('PrintMarkFrame', UseSpecFrame);
      WriteBool('UseText', UseTextMarks);
      WriteInteger('Font', Font.Font);
      WriteInteger('Style', Font.Style);
      WriteInteger('Height', Font.Height);
      WriteInteger('Color', Color);
      WriteInteger('BackColor', BackColor);

      WriteBool('Left', UseLeft);
      WriteBool('Right', UseRight);
      WriteBool('Top', UseTop);
      WriteBool('Bottom', UseBottom);

      WriteFloat('Step', StepLength);
      WriteFloat('MarkLength', MarksLength);
      WriteBool('UseCrosses', UseCross);
      WriteFloat('CrossLength', CrossLength);
      WriteInteger('CrossColor', CrossColor);
      WriteInteger('GridColor', GridColor);

    end;

{brovak}

    OpenKey('\Project\Settings\FloatingText', True);
    if (TooltipLayer <> nil) then
    begin
      WriteBool('Enabled', True);
      WriteString('CurrentLayer', PToStr(TooltipLayer^.Text));
    end
    else
    begin
      WriteBool('Enabled', False);
      WriteString('CurrentLayer', '');
    end;
      // write legends
    Legends.WriteToRegistry(Registry, '\Project\Legends');
{++ IDB}
{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}
    if SELF.IDBSettings <> nil then
    begin
      OpenKey(cs_Projectregistry_IDB_LocalSettings_KeyName, TRUE);
      WriteBool('UseGlobal', SELF.IDBSettings.bUseGlobalIDBSettings);
      WriteBool('UseIDB', SELF.IDBSettings.bLocalWeAreUsingTheIDB);
      WriteBool('UseIDBInterface', SELF.IDBSettings.bLocalWeAreUsingTheIDBUserInterface);
      WriteBool('UseIDBUndo', SELF.IDBSettings.bLocalWeAreUsingUndoFunction);
      WriteInteger('IDBUndoStepsCount', SELF.IDBSettings.iLocalUndoStepsCount);
    end;
{$ENDIF}
{$ENDIF} // <----------------- AXDLL
{-- IDB}
  end;
end;

{******************************************************************************+
  Function TProj.GetMenuFunctionName
--------------------------------------------------------------------------------
  Determines the menu-function-name of the current menu. This function is
  for backward-compatibility only. Add only menu-functions that use the
  old mouse- and keyboard-handling with the MsgMouse-messages of the project.
+******************************************************************************}

function TProj.GetMenuFunctionName
  : string;
begin
  if MenuHandler <> nil then
    Result := MenuHandler.MenuName
  else
    case ActualMen of
      mn_Zoom,
        mn_Select: Result := Registry.ReadString('\User\WinGISUI\DrawingTools\Selection\InputType');
      mn_Pixel: Result := 'DrawPoint';
      mn_CPoly: Result := 'DrawPolygon';
      mn_Poly: Result := 'DrawPolyline';
      mn_SnapPoly,
        mn_Snap: Result := 'EditPointMove';
      mn_InsPOK,
        mn_InsPoint: Result := 'EditPointInsert';
      mn_DelPoint,
        mn_DelPOK: Result := 'EditPointDelete';
      mn_OffsetOK,
        mn_Offset: Result := 'ExtrasOffset';
      mn_Move: Result := 'EditMove';
      mn_Symbol: Result := 'DrawSymbol';
      mn_Text: Result := 'DrawText';
      mn_RTrafo: Result := 'RasterTransformationLinear';
      {++brovak #MOD for copy part of selected file}
      mn_CutImage: Result := 'CutSelectedPartOfImage';
      {--brovak}
      mn_SuperImage: Result := 'MultiCopyCombineAndSave';

      mn_InsertOLEObject: Result := 'InsertOLEObject';
      mn_GenerRef,
        mn_GenerRect: Result := 'ExtrasGenerate';
      mn_MesPoint: Result := 'DrawMeasure';
      mn_MesCut: Result := 'DrawMeasureCut';
      mn_MesOrtho: Result := 'DrawMeasureOrtho';
      mn_LineCut: Result := 'ExtrasTrimm';
      mn_Distance: Result := 'ExtrasMeasureDistance';
      mn_Combine,
        mn_CombineSel,
        mn_CombineSelOne: Result := 'ExtrasCombine';
      mn_DistMark: Result := 'ConstructDistance';
      mn_LineMark: Result := 'ConstructLines';
      mn_OrthoMark: Result := 'ConstructOrthogonal';
      mn_CircleCutMark: Result := 'ConstructTwoDistances';
      mn_AngleMark: Result := 'ConstructTwoAngles';
      mn_CircleMark: Result := 'ConstructAngle';
      mn_RefPoints: Result := 'DigitCalibrateDigitizer';
      mn_SelForSelect,
        mn_DrawForSelect:
        if NeighOrSelect then
          Result := 'ExtrasNeighbourSearch'
        else
          Result := 'ExtrasSelect'
{      mn_InsLinkObj      : Result:=
      mn_PasteOLEObj     : Result:=
      mn_EditOLE
      mn_DrawArc2
      mn_OffsetForSel
      mn_MovePoly
      mn_SizeRot
      mn_MoveSym
      mn_SymbolInp

      mn_DBText
      mn_SplineEdit
      mn_EditImage
      mn_LineSnap
      mn_MoveCircle
      mn_ShowDBSel
      mn_MoveMulti
      mn_RefBitmap
      mn_DBSelCircle
      mn_DBSelRect
      mn_Legend
      mn_ProjBorder
      mn_ProjScript
      mn_SelStart
      mn_SelDest
      mn_SelGo
      mn_SetSel
      mn_Ponti
      mn_Inspect
      mn_DrawSymLine
      mn_SetRefPoint
      mn_SetOmegaPoint}
      else
        Result := 'Default';
    end;
end;

{******************************************************************************+
  Procedure TProj.OnGlobalOptionsChanged
--------------------------------------------------------------------------------
  Called if the global-settings are changed. Updates all project-options
  that are set to "use global settings". Calls OnOptionsChanged that to update
  project-internal properties.
+******************************************************************************}

procedure TProj.OnGlobalOptionsChanged;
begin
  with PInfo^ do
  begin
    if SelectionSettings.UseGlobals then
    begin
      SelectionSettings := IniFile^.SelectionSettings;
      SelectionSettings.UseGlobals := TRUE;
    end;
    if VariousSettings.UseGlobals then
    begin
      VariousSettings := IniFile^.VariousSettings;
      VariousSettings.UseGlobals := TRUE;
    end;
    if BitmapSettings.UseGlobals then
    begin
      BitmapSettings := IniFile^.BitmapSettings;
      BitmapSettings.UseGLobals := TRUE;
    end;
    if FontSettings.UseGlobals then
    begin
//++ Glukhov Bug#63 BUILD#128 14.09.00
{*  The old code was commented
      ActualFont.Font:=IniFile^.FontSettings.ChosenFont^.Font;
      ActualFont.Style:=IniFile^.FontSettings.ChosenFont^.Style;
      ActualFont.Height:=IniFile^.FontSettings.ChosenFont^.Height;
      FontSettings.ChosenFont^.Font:=IniFile^.FontSettings.ChosenFont^.Font;
      FontSettings.ChosenFont^.Style:=IniFile^.FontSettings.ChosenFont^.Style;
      FontSettings.ChosenFont^.Height:=IniFile^.FontSettings.ChosenFont^.Height;
*}
// Copy the chosen font data
      FontSettings.ChosenFont^ := IniFile^.FontSettings.ChosenFont^;
// Check and correct the chosen font data
      FontSettings.ChosenFont^.Font := CheckChosenFont(
        FontSettings, IniFile^.FontSettings.AllFonts);
// Copy the font data to the actual font (it seems that it is superfluous action)
      ActualFont := FontSettings.ChosenFont^;
//-- Glukhov Bug#63 BUILD#128 14.09.00
{++ Ivanoff BUG#1001FG BUILD#106}
      PInfo.AnnotSettings.AnnotFont := ActualFont;
{-- Ivanoff}
      FontSettings.UseGlobals := TRUE;
    end;
    if ThreeDSettings.UseGlobals then
    begin
      ThreeDSettings := IniFile^.ThreeDSettings;
      ThreeDSettings.UseGLobals := TRUE;
    end;

{++ IDB}
{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}
    if SELF.IDBSettings <> nil then
      if SELF.IDBSettings.bUseGlobalIDBSettings then
      begin
        SELF.IDBSettings.bLocalWeAreUsingTheIDB := WinGISMainForm.WeAreUsingTheIDB[nil];
        SELF.IDBSettings.bLocalWeAreUsingTheIDBUserInterface := WinGISmainForm.WeAreUsingTheIDBUserInterface[nil];
        SELF.IDBSettings.bLocalWeAreUsingUndoFunction := WinGISmainForm.WeAreUsingUndoFunction[nil];
        SELF.IDBSettings.iLocalUndoStepsCount := WinGISmainForm.IDBUndoStepsCount[nil];
      end;
{$ENDIF}
{$ENDIF} // <----------------- AXDLL
{-- IDB}

    OnOptionsChanged;
  end;
end;

{******************************************************************************+
  Procedure TProj.OnOptionsChanged
--------------------------------------------------------------------------------
  Called if the project-settings were changed by the user or by a dde-command.
  Updates all project-internal properties.
+******************************************************************************}

procedure TProj.OnOptionsChanged;
{++ Brovak BUG#574 BUILD#108 -
 This procedure was changed for additional condition Set for
 draw/no draw rect for hidden text }
begin
  with PInfo^ do
  begin
  // switch showing of edge-points on or off, cause redraw if change in stat
    if (ExtCanvas.DrawVertices <> VariousSettings.ShowEdgePoints)
      or (VariousSettings.OldHiddenText <> VariousSettings.ShowHiddenText) then
    begin;
      if ExtCanvas.DrawVertices <> VariousSettings.ShowEdgePoints then
        ExtCanvas.DrawVertices := VariousSettings.ShowEdgePoints;
//++ Glukhov Bug#236 Build#144 06.12.00
//       PInfo^.RedrawScreen(TRUE);
//-- Glukhov Bug#236 Build#144 06.12.00
    end;
  end;
{-- Brovak}
//++ Glukhov Bug#236 Build#144 06.12.00
  PInfo.RedrawScreen(True);
//-- Glukhov Bug#236 Build#144 06.12.00
end;

{******************************************************************************+
  Procedure TProj.InitClipRect
--------------------------------------------------------------------------------
  Initializes all clip-rectangles of the objects-references on the layers.
+******************************************************************************}

procedure TProj.InitClipRect;
var
  Cnt: Longint;
  Cnt2: Longint;
  ALayer: PLayer;
  AIndex: PIndex;
  AObject: PView;
begin
  for Cnt := 0 to Layers^.LData^.count - 1 do
  begin
    ALayer := Layers^.LData^.at(Cnt);
    for Cnt2 := 0 to ALayer^.Data^.GetCount - 1 do
    begin
      AIndex := ALayer^.Data^.At(Cnt2);
      AObject := Pointer(PLayer(PInfo^.Objects)^.IndexObject(PInfo, AIndex));
      AIndex^.ClipRect.InitByRect(AObject^.ClipRect);
    end;
  end;
end;

procedure TProj.DeleteObjectList
  (
  ObjectList: TIntList
  );
begin

end;

procedure TProj.CorrectSelAllRect(const Rect: TDRect);
begin
  if SelAllRect.IsEmpty then
    SelAllRect.InitByRect(Rect)
  else
    SelAllRect.CorrectByRect(Rect);
end;

{*****************************************************************************************************************************}

function TProj.DoGetTooltipText
  (
  aIndex: PIndex;
  AObjType: Integer;
  AToolTipType: Integer
  )
  : string;
{$IFNDEF AXDLL} // <----------------- AXDLL
var
  AView: PView;

  function SetID
      : string;
  var
    ToolStr: string;
  begin
    Str(aView^.Index: 0, ToolStr);
    Result := ToolStr;
  end;

  function SetCoords
      (
      AObjType: Integer
      )
      : string;
  var
    ToolStr: string;
    TmpStr: string;
  begin
    case AObjType of
      ot_Pixel:
        begin
          Str(PPixel(aView)^.Position.X / 100: 0: 2, ToolStr);
          ToolStr := ToolStr + ' / ';
          Str(PPixel(aView)^.Position.Y / 100: 0: 2, TmpStr);
          ToolStr := ToolStr + TmpStr;
        end;
      ot_CPoly:
        begin
          Str(PCPoly(aView)^.ClipRect.CenterX / 100: 0: 2, ToolStr);
          ToolStr := ToolStr + ' / ';
          Str(PCPoly(aView)^.ClipRect.CenterY / 100: 0: 2, TmpStr);
          ToolStr := ToolStr + TmpStr;
        end;
      ot_Circle:
        begin
          Str(PEllipse(aView)^.Position.X / 100: 0: 2, ToolStr);
          ToolStr := ToolStr + ' / ';
          Str(PEllipse(aView)^.Position.Y / 100: 0: 2, TmpStr);
          ToolStr := ToolStr + TmpStr;
        end;
      ot_Arc:
        begin
          Str(PEllipseArc(aView)^.Position.X / 100: 0: 2, ToolStr);
          ToolStr := ToolStr + ' / ';
          Str(PEllipseArc(aView)^.Position.Y / 100: 0: 2, TmpStr);
          ToolStr := ToolStr + TmpStr;
        end;
      ot_Text:
        begin
          Str(PText(aView)^.Pos.X / 100: 0: 2, ToolStr);
          ToolStr := ToolStr + ' / ';
          Str(PText(aView)^.Pos.Y / 100: 0: 2, TmpStr);
          ToolStr := ToolStr + TmpStr;
        end;
      ot_RText:
        begin
          Str(PRText(aView)^.TextRect.CenterX / 100: 0: 2, ToolStr);
          ToolStr := ToolStr + ' / ';
          Str(PRText(aView)^.TextRect.CenterY / 100: 0: 2, TmpStr);
          ToolStr := ToolStr + TmpStr;
        end;
      ot_BusGraph:
        begin
{$IFNDEF WMLT}
          Str(PBusGraph(aView)^.Position.X / 100: 0: 2, ToolStr);
          ToolStr := ToolStr + ' / ';
          Str(PBusGraph(aView)^.Position.Y / 100: 0: 2, TmpStr);
          ToolStr := ToolStr + TmpStr;
{$ENDIF}
        end;
      ot_Image:
        begin
          Str(PImage(aView)^.ClipRect.CenterX / 100: 0: 2, ToolStr);
          ToolStr := ToolStr + ' / ';
          Str(PImage(aView)^.ClipRect.CenterY / 100: 0: 2, TmpStr);
          ToolStr := ToolStr + TmpStr;
        end;
      ot_Symbol:
        begin
          Str(PSymbol(aView)^.Position.X / 100: 0: 2, ToolStr);
          ToolStr := ToolStr + ' / ';
          Str(PSymbol(aView)^.Position.Y / 100: 0: 2, TmpStr);
          ToolStr := ToolStr + TmpStr;
        end;
    end;
    Result := ToolStr;
  end;

  function SetPerimeter
      (
      AObjType: Integer
      )
      : string;
  var
    ToolStr: string;
  begin
    case AObjType of
      ot_Poly: Str(PPoly(aView)^.Laenge: 0: 2, ToolStr);
      ot_CPoly: Str(PCPoly(aView)^.Laenge: 0: 2, ToolStr);
      ot_Circle: Str(PEllipse(aView)^.Laenge: 0: 2, ToolStr);
      ot_Arc: Str(PEllipseArc(aView)^.Laenge: 0: 2, ToolStr);
      ot_Spline: Str(PSpline(aView)^.Laenge: 0: 2, ToolStr);
      ot_Image: Str((PImage(aView)^.ClipRect.XSize * 2 + PImage(aView)^.ClipRect.YSize * 2) / 100: 0: 2, ToolStr);
    end;
    Result := ToolStr;
  end;

  function SetArea
      (
      AObjType: Integer
      )
      : string;
  var
    ToolStr: string;
  begin
    case AObjType of
      ot_CPoly: Str(PCPoly(aView)^.Flaeche: 0: 2, ToolStr);
      ot_Circle: Str(PEllipse(aView)^.Flaeche: 0: 2, ToolStr);
      ot_Image: Str((PImage(aView)^.ClipRect.XSize * PImage(aView)^.ClipRect.YSize) / 10000: 0: 2, ToolStr);
    end;
    Result := ToolStr;
  end;

  function SetSymNumber
      : string;
  var
    ToolStr: string;
  begin
    Str(PSymbol(aView)^.SymIndex - 600000000: 0, ToolStr);
    Result := ToolStr;
  end;

  function SetSymName
      : string;
  begin
    Result := PSymbol(aView)^.SymPtr^.Name^;
  end;
{$ENDIF} // <----------------- AXDLL
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  aView := PObjects(PInfo^.Objects)^.IndexObject(PInfo, AIndex);
  if AView <> nil then
  begin
    case AToolTipType of
      int_ID: Result := SetID;
      int_Area: Result := SetArea(AObjType);
      int_Peri: Result := SetPerimeter(AObjType);
      int_Coord: Result := SetCoords(AObjType);
      int_SymNr: Result := SetSymNumber;
      int_SymNa: Result := SetSymName;
    else
      Result := '';
    end;
  end;
{$ENDIF} // <----------------- AXDLL
end;

{++ IDB}

procedure TProj.DoFillTooltipText
  (
  ALayer: PLayer;
  bPutDataIntoIDB: Boolean;
  AItem: PIndex;
  FillWith: Integer;
  DeleteOld: Boolean
  );
{ The old variant. It was replaced by me. Ivanoff.
Procedure TProj.DoFillTooltipText
   (
   AItem           : PIndex;
   FillWith        : Integer;
   DeleteOld       : Boolean
   );
{-- IDB}
var
  aView: PView;

  procedure SetToolTip
      (
      AObjType: Integer;
      AToolTipType: Integer
      );
  var
    sLayerIndex: AnsiString;
    iI: Integer;
    ATempLayer: PLayer;
    ATempIndex: PIndex;
  begin
    DisposeStr(aView^.Tooltip);
{++ IDB}
{$IFNDEF WMLT}
      // This is the old way to do it when we don't need to do it via the IDB.
    if not bPutDataIntoIDB then
{$ENDIF}
{-- IDB}
      aView^.Tooltip := NewStr(DoGetTooltipText(AItem, AObjType, AToolTipType))
{++ IDB}
      // This is way to do it when we want to use the IDB for this task.
{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}
    else
    begin
      if ALayer <> nil then
      begin
           // A project item is located on layer.
        if WinGisMainForm.WeAreUsingTheIDB[@SELF] then
          WinGISMainForm.IDB_Man.SetGeoText(@SELF, ALayer.Index, AView.Index,
                  {ALayer.TooltipInfo.IDColName,
                  ALayer.TooltipInfo.FTColName,}
            DoGetTooltipText(AItem, AObjType, AToolTipType));
      end
      else
      begin
           // A item is located on layer of selected items.
        if WinGisMainForm.WeAreUsingTheIDB[@SELF] then
          for iI := 1 to SELF.Layers.LData.Count - 1 do
          begin // << From 1 'cause Layer with index = 0 is the layer of selected project items.
            ATempLayer := SELF.Layers.LData.At(iI);
            if ATempLayer = nil then
              CONTINUE;
               // Does this layer has an item or not?
            ATempIndex := ATempLayer.HasObject(AView);
            if ATempIndex = nil then
              CONTINUE;
            sLayerIndex := IntToStr(ATempLayer.Index);
            WinGISMainForm.IDB_Man.SetGeoText(@SELF, ALayer.Index, AView.Index,
                     {ALayer.TooltipInfo.IDColName,
                     ALayer.TooltipInfo.FTColName,}
              DoGetTooltipText(AItem, AObjType, AToolTipType));
          end; // for iI end
      end;
    end;
{$ENDIF}
{$ENDIF} // <----------------- AXDLL
{-- IDB}
  end;

  procedure ClearToolTip;
  begin
    DisposeStr(aView^.Tooltip);
    aView^.Tooltip := nil;
  end;
begin
  aView := PObjects(PInfo^.Objects)^.IndexObject(PInfo, AItem);
  if aView^.GetObjType = ot_Pixel then
  begin
    case FillWith of
      int_ID: SetToolTip(ot_Pixel, int_ID);
      int_Coord: SetToolTip(ot_Pixel, int_Coord);
      int_Area,
        int_Peri,
        int_SymNr,
        int_SymNa:
        if DeleteOld then
          ClearToolTip;
    end;
  end
  else
    if aView^.GetObjType = ot_Poly then
    begin
      case FillWith of
        int_ID: SetToolTip(ot_Poly, int_ID);
        int_Peri: SetToolTip(ot_Poly, int_Peri);
        int_Area,
          int_Coord,
          int_SymNr,
          int_SymNa:
          if DeleteOld then
            ClearToolTip;
      end;
    end
    else
      if aView^.GetObjType = ot_CPoly then
      begin
        case FillWith of
          int_ID: SetToolTip(ot_CPoly, int_ID);
          int_Coord: SetToolTip(ot_CPoly, int_Coord);
          int_Peri: SetToolTip(ot_CPoly, int_Peri);
          int_Area: SetToolTip(ot_CPoly, int_Area);
          int_SymNr,
            int_SymNa:
            if DeleteOld then
              ClearToolTip;
        end;
      end
      else
        if aView^.GetObjType = ot_Circle then
        begin
          case FillWith of
            int_ID: SetToolTip(ot_Circle, int_ID);
            int_Peri: SetToolTip(ot_Circle, int_Peri);
            int_Area: SetToolTip(ot_Circle, int_Area);
            int_Coord: SetToolTip(ot_Circle, int_Coord);
            int_SymNr,
              int_SymNa:
              if DeleteOld then
                ClearToolTip;
          end;
        end
        else
          if aView^.GetObjType = ot_Arc then
          begin
            case FillWith of
              int_ID: SetToolTip(ot_Arc, int_ID);
              int_Peri: SetToolTip(ot_Arc, int_Peri);
              int_Coord: SetToolTip(ot_Arc, int_Coord);
              int_Area,
                int_SymNr,
                int_SymNa:
                if DeleteOld then
                  ClearToolTip;
            end;
          end
          else
            if aView^.GetObjType = ot_Text then
            begin
              case FillWith of
                int_ID: SetToolTip(ot_Text, int_ID);
                int_Coord: SetToolTip(ot_Text, int_Coord);
                int_Peri,
                  int_Area,
                  int_SymNr,
                  int_SymNa:
                  if DeleteOld then
                    ClearToolTip;
              end;
            end
            else
              if aView^.GetObjType = ot_RText then
              begin
                case FillWith of
                  int_ID: SetToolTip(ot_RText, int_ID);
                  int_Coord: SetToolTip(ot_RText, int_Coord);
                  int_Peri,
                    int_Area,
                    int_SymNr,
                    int_SymNa:
                    if DeleteOld then
                      ClearToolTip;
                end;
              end
              else
                if aView^.GetObjType = ot_BusGraph then
                begin
                  case FillWith of
                    int_ID: SetToolTip(ot_BusGraph, int_ID);
                    int_Coord: SetToolTip(ot_BusGraph, int_Coord);
                    int_Peri,
                      int_Area,
                      int_SymNr,
                      int_SymNa:
                      if DeleteOld then
                        ClearToolTip;
                  end;
                end
                else
                  if aView^.GetObjType = ot_Spline then
                  begin
                    case FillWith of
                      int_ID: SetToolTip(ot_Spline, int_ID);
                      int_Peri: SetToolTip(ot_Spline, int_Peri);
                      int_Coord,
                        int_Area,
                        int_SymNr,
                        int_SymNa:
                        if DeleteOld then
                          ClearToolTip;
                    end;
                  end
                  else
                    if aView^.GetObjType = ot_Image then
                    begin
                      case FillWith of
                        int_ID: SetToolTip(ot_Image, int_ID);
                        int_Coord: SetToolTip(ot_Image, int_Coord);
                        int_Peri: SetToolTip(ot_Image, int_Peri);
                        int_Area: SetToolTip(ot_Image, int_Area);
                        int_SymNr,
                          int_SymNa:
                          if DeleteOld then
                            ClearToolTip;
                      end;
                    end
                    else
                      if aView^.GetObjType = ot_Symbol then
                      begin
                        case FillWith of
                          int_ID: SetToolTip(ot_Symbol, int_ID);
                          int_Coord: SetToolTip(ot_Symbol, int_Coord);
                          int_SymNr: SetToolTip(ot_Symbol, int_SymNr);
                          int_SymNa: SetToolTip(ot_Symbol, int_SymNa);
                          int_Peri,
                            int_Area:
                            if DeleteOld then
                              ClearToolTip;
                        end;
                      end
                      else
                        if aView^.GetObjType = ot_MesLine then
                        begin
                          case FillWith of
                            int_ID,
                              int_Coord,
                              int_Peri,
                              int_Area,
                              int_SymNr,
                              int_SymNa:
                              if DeleteOld then
                                ClearToolTip;
                          end;
                        end
                        else
                          if aView^.GetObjType = ot_OLEObj then
                          begin
                            case FillWith of
                              int_ID,
                                int_Coord,
                                int_Peri,
                                int_Area,
                                int_SymNr,
                                int_SymNa:
                                if DeleteOld then
                                  ClearToolTip;
                            end;
                          end;
end;

const
  RProj: TStreamRec = (
    ObjType: rn_Proj;
    VmtLink: TypeOf(TProj);
    Load: @TProj.Load;
    Store: @TProj.Store);

{$IFNDEF WMLT}

procedure TProj.ShowPopup;
var
  PopupMenu: TUIPopupMenu;
  APoint: TPoint;
  i: Integer;
  AnyEnabled: Boolean;
begin
  if IsCanvas then
  begin
    PopupMenu := UserInterface.PopupMenus['CanvasWindowPopup'];
    AnyEnabled := False;
    for i := 0 to PopupMenu.Items.Count - 1 do
    begin
      if (PopupMenu.Items[i].Enabled)
        and (PopupMenu.Items[i].Visible)
        and (Pos('Separator', PopupMenu.Items[i].Name) = 0)
        and (ActualMen <> mn_SymbolInp) then
      begin
        AnyEnabled := True;
        break;
      end;
    end;
    if (PopupMenu <> nil) and (AnyEnabled) then
    begin
      GetCursorPos(APoint);
      PopupMenu.Popup(APoint.X, APoint.Y);
    end;
  end
  else
  begin
    PopupMenu := UserInterface.PopupMenus['ProjectWindowPopup'];
    AnyEnabled := False;
    for i := 0 to PopupMenu.Items.Count - 1 do
    begin
      if (PopupMenu.Items[i].Enabled)
        and (PopupMenu.Items[i].Visible)
        and (Pos('Separator', PopupMenu.Items[i].Name) = 0)
        and (ActualMen <> mn_SymbolInp) then
      begin
        AnyEnabled := True;
        break;
      end;
    end;
    if (PopupMenu <> nil) and (AnyEnabled) then
    begin
      GetCursorPos(APoint);
      PopupMenu.Popup(APoint.X, APoint.Y);
    end;
  end;
end;
{$ENDIF}

{brovak Changes this procedure for zooming when an user is using painting instruments. }

procedure TProj.MsgTimer(var Msg: TWMTimer);
begin
  if Msg.TimerID = idt_Mouse then
  begin
    KillTimer(Parent.Handle, idt_Mouse);
    {+++ Brovak BUG 524,539 BUILD 167}
    if Assigned(MenuHandler) then
    begin;
      ClickCnt1 := 0;
{$IFNDEF WMLT}
      ShowPopup; //or ahother rightclick MenuHandler
{$ENDIF}
      Exit;
    end;
     {--- Brovak }
{$IFNDEF WMLT}
//++ Glukhov Bug#393 Build#161 23.05.01
// Remove an extra PopUp
    if (ActualMen in [mn_Text]) then
//-- Glukhov Bug#393 Build#161 23.05.01
      KillTimer(Parent.Handle, idt_Popup);
{$ENDIF}
    if ActualMen = mn_Zoom then
      SelRect^.EndIt(PInfo);
  end
  else
    if Msg.TimerID = idt_Popup then
    begin
      KillTimer(Parent.Handle, idt_Popup);
{$IFNDEF WMLT}
      ShowPopup;
{$ENDIF}
    end;
end;

{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL

procedure TProj.CutLineByLength(Id: Integer; Length: Double; Mode: Integer; DestLayer: PLayer);
var
  PolyItem, SItem: PIndex;
  NObj, SObj: PView;
  Poly: PPoly;
  a, b, c, xbegin, ybegin, xend, yend, xnew, ynew: Extended;
  Cmd: string;
  cnt, i: Integer;
// ++ Cadmensky
  APoint: TDPoint;
// -- Cadmensky
begin
  SItem := New(PIndex, Init(Id));
//  PolyItem := nil;
  PolyItem := Layers^.TopLayer^.HasObject(SItem);
  if PolyItem <> nil then
  begin
    if PolyItem^.GetObjType in [ot_Poly] then
    begin
      if Mode = 0 then
        Cmd := 'UPD'
      else
        if Mode = 1 then
        begin
          SObj := PView(Layers^.TopLayer^.IndexObject(PInfo, SItem));
          if SObj <> nil then
          begin
            NObj := MakeObjectCopy(SObj);
            InsertObjectOnLayer(NObj, DestLayer^.Index);
            SItem := New(PIndex, Init(NObj^.Index));
//            PolyItem := nil;
            PolyItem := DestLayer^.HasObject(SItem);
            Cmd := 'EIN';
          end;
        end;
      Poly := PPoly(Layers^.TopLayer^.IndexObject(PInfo, PolyItem));
      Poly^.Invalidate(PInfo);
      if (Poly^.Laenge * 100) > Length then
      begin
        cnt := Poly^.Data^.Count;
        for i := cnt - 1 downto 0 do
        begin
          if (Poly^.Laenge * 100) > Length then
          begin
            xend := PDPoint(Poly^.Data^.At(i))^.x;
            yend := PDPoint(Poly^.Data^.At(i))^.y;
            Poly^.Data^.AtDelete(i);
          end
          else
            break;
        end;
        if (Poly^.Laenge * 100) < Length then
        begin
          xbegin := PDPoint(Poly^.Data^.At(i))^.x;
          ybegin := PDPoint(Poly^.Data^.At(i))^.y;
          Length := Length - Round(Poly^.Laenge * 100);
          a := xend - xbegin;
          b := yend - ybegin;
          c := Sqrt(a * a + b * b);
          if xbegin < xend then
            xnew := xbegin + Sqrt(c * c - b * b) * (length / c)
          else
            xnew := xbegin - Sqrt(c * c - b * b) * (length / c);
          if ybegin < yend then
            ynew := ybegin + Sqrt(c * c - a * a) * (Length / c)
          else
            ynew := ybegin - Sqrt(c * c - a * a) * (Length / c);
          APoint.Init(Round(xnew), Round(ynew));
          Poly^.Data^.Insert(@APoint);
          APoint.Done;
        end;
        Poly^.CalculateClipRect;
        Poly^.ClosePoly;
        PInfo^.RedrawInvalidate;
        SetModified;
        ProcDBSendObject(@Self, PolyItem, Cmd);
      end;
      DeselectAll(False);
    end;
  end;
end;

procedure TProj.CalcLinePoint;
var
  PolyItem, SItem: PIndex;
  Poly: PPoly;
  a, b, c, xbegin, ybegin, xend, yend, xnew, ynew: Extended;
  Id, Length: Integer;
  cnt, i, k: Integer;
  Angle: Double;
begin
  for k := 0 to IntList1.Count - 1 do
  begin
    Id := IntList1[k];
    Length := IntList2[k];
    if (Id > -1) and (Length > -1) then
    begin
      SItem := New(PIndex, Init(Id));
      PolyItem := Layers^.TopLayer^.HasObject(SItem);
      if (PolyItem <> nil) and (PolyItem^.GetObjType in [ot_Poly]) then
      begin
        Poly := PPoly(Layers^.TopLayer^.IndexObject(PInfo, PolyItem));
        if (Poly^.Laenge * 100) > Length then
        begin
          cnt := Poly^.Data^.Count;
          for i := cnt - 1 downto 0 do
          begin
            if (Poly^.Laenge(i) * 100) > Length then
            begin
              xend := PDPoint(Poly^.Data^.At(i))^.x;
              yend := PDPoint(Poly^.Data^.At(i))^.y;
            end
            else
            begin
              break;
            end;
          end;
          if (Poly^.Laenge(i) * 100) <= Length then
          begin
            xbegin := PDPoint(Poly^.Data^.At(i))^.x;
            ybegin := PDPoint(Poly^.Data^.At(i))^.y;
            Length := Length - Round(Poly^.Laenge(i) * 100);
            a := xend - xbegin;
            b := yend - ybegin;
            c := Sqrt(a * a + b * b);
            if xbegin < xend then
              xnew := xbegin + Sqrt(c * c - b * b) * (length / c)
            else
              xnew := xbegin - Sqrt(c * c - b * b) * (length / c);
            if ybegin < yend then
              ynew := ybegin + Sqrt(c * c - a * a) * (Length / c)
            else
              ynew := ybegin - Sqrt(c * c - a * a) * (Length / c);

            Angle := LineAngle(GrPoint(xbegin, ybegin), GrPoint(xend, yend)) / PI * 180;
            if Angle < 0 then
            begin
              Angle := 360 + Angle;
            end;
            DDEHandler.SendString('[POL][' + IntToStr(Id) + '][' + IntToStr(Trunc(xnew)) + '][' + IntToStr(Trunc(ynew)) + '][' + IntToStr(Trunc(Angle)) + ']');
          end;
        end;
      end;
    end;
  end;
  DDEHandler.SendString('[END][]');
  IntList1.Clear;
  IntList2.Clear;
end;

function TProj.CalcLinePoint2(Id: Integer; Length: Integer): TDPoint;
var
  PolyItem, SItem: PIndex;
  Poly: PPoly;
  a, b, c, xbegin, ybegin, xend, yend, xnew, ynew: Extended;
  cnt, i: Integer;
//   Angle: Double;
begin
  if (Id > -1) and (Length > -1) then
  begin
    SItem := New(PIndex, Init(Id));
    PolyItem := Layers^.TopLayer^.HasObject(SItem);
    if (PolyItem <> nil) and (PolyItem^.GetObjType in [ot_Poly]) then
    begin
      Poly := PPoly(Layers^.TopLayer^.IndexObject(PInfo, PolyItem));
      if (Poly^.Laenge * 100) > Length then
      begin
        cnt := Poly^.Data^.Count;
        for i := cnt - 1 downto 0 do
        begin
          if (Poly^.Laenge(i) * 100) > Length then
          begin
            xend := PDPoint(Poly^.Data^.At(i))^.x;
            yend := PDPoint(Poly^.Data^.At(i))^.y;
          end
          else
          begin
            break;
          end;
        end;
        if (Poly^.Laenge(i) * 100) <= Length then
        begin
          xbegin := PDPoint(Poly^.Data^.At(i))^.x;
          ybegin := PDPoint(Poly^.Data^.At(i))^.y;
          Length := Length - Round(Poly^.Laenge(i) * 100);
          a := xend - xbegin;
          b := yend - ybegin;
          c := Sqrt(a * a + b * b);
          if xbegin < xend then
            xnew := xbegin + Sqrt(c * c - b * b) * (length / c)
          else
            xnew := xbegin - Sqrt(c * c - b * b) * (length / c);
          if ybegin < yend then
            ynew := ybegin + Sqrt(c * c - a * a) * (Length / c)
          else
            ynew := ybegin - Sqrt(c * c - a * a) * (Length / c);

                  //Angle := LineAngle (GrPoint (xbegin, ybegin), GrPoint (xend, yend)) / PI * 180;
                  {
                  if Angle < 0 then
                  begin
                     Angle := 360 + Angle;
                  end;
                  }
          Result.Init(Round(xnew), Round(ynew));
        end;
      end;
    end;
  end;
end;

procedure TProj.SendClickInfo(x, y, Button, Mode, DblClick: Integer);
var
  s: string;
  Pt, Pts: TDPoint;
  SnapObject, Idx: PIndex;
  Id: Integer;
  Snapped: Boolean;
begin
  if (IniFile^.SendMouseInfo)
    and (not NoClickInfo) then
  begin
    Snapped := False;
    Pt.x := x;
    Pt.y := y;
    if SnapPoint(Pt, 0, ot_Any, Pts, SnapObject) then
    begin
      Snapped := (Pt.x <> Pts.x) or (Pt.y <> Pts.y);
      Pt.x := Pts.x;
      Pt.y := Pts.y;
    end;
    Idx := Layers^.SearchByPoint(PInfo, Pt, ot_Any);
    Id := 0;
    if Idx <> nil then
      Id := Idx^.Index;
    s := '[SCI][' + FloatToStr(Pt.x / 100) + '][' + FloatToStr(Pt.y / 100) + '][' + IntToStr(Id) + '][' +
      IntToStr(Button) + '][' + IntToStr(Mode) + '][' + IntToStr(DblClick) + '][' + IntToStr(Integer(Snapped)) + ']';
    DDEHandler.SendString(s);
  end;
end;

procedure TProj.SetTransparentSelect(AState: Boolean; ASendLInfo: Boolean);
var
  TmpStr: string;
begin
  if AState then
  begin
    TSOldActiveLayer := Layers^.TopLayer;
    ActualMen := mn_Select;
    MenuFunctions['EditSelect'].Checked := TRUE;
  end
  else
  begin
    Layers^.TopLayer := TSOldActiveLayer;
    UpdateLayersViewsLegendsLists(@Self);
  end;

  Layers^.TranspLayer := AState;
  if ActualMen = mn_Select then
  begin
    if Layers^.TranspLayer then
      SelectCursor := idc_TranspSelect
    else
      SelectCursor := crArrow;
    SetCursor(SelectCursor);
  end;
  if ASendLInfo and (DDEHandler.FSendLMSets or CoreConnected) then
  begin
    DDEHandler.SetDBSendStrings(DDECommLMI, nil);
    if Layers^.TranspLayer then
      DDEHandler.SendString('[ALA][]')
    else
    begin
      if DDEHandler.FLayerInfo <> 2 then
        DDEHandler.SendString('[ALA][' + Layers^.TopLayer^.Text^ + ']')
      else
      begin
        Str(Layers^.TopLayer^.Index: 10, TmpStr);
        DDEHandler.SendString('[ALA][' + TmpStr + ']');
      end;
    end;
    DDEHandler.SetDBSendStrings(DDECommAll, nil);
  end;
end;
{$ENDIF} // <----------------- AXDLL
{$ENDIF}

{+++Brovak Bug 104 BUILD 130}
// Procedure set coord for new point as 0/45/90 grad}

procedure TProj.GetNewDrawPos(var DrawPos: TDPOint);
var
  OldPoint: PDPoint; //previous point
  S_XDist, S_YDist: longint;
  XKoeff, YKoeff: integer;
begin; //func
  OldPoint := NewPoly^.GetLastPoint(PInfo);
  if OldPoint = nil then
    Exit;

  if (OldPoint.X = DrawPos.X) or (OldPoint.Y = DrawPos.Y) or (DrawPos.Y = DrawPos.X) then
    Exit; //correction not needed

  S_XDist := DrawPos.X - OldPoint.X;
  S_YDist := DrawPos.Y - OldPOint.Y;
    //Detect Dekart Quarter
  if (S_XDist > 0) and (S_YDist > 0) then
  begin;
    XKoeff := 1;
    YKoeff := 1;
  end; //1st Quarter
  if (S_XDist < 0) and (S_YDist > 0) then
  begin;
    XKoeff := -1;
    YKoeff := 1;
  end; //2nd Quarter
  if (S_XDist < 0) and (S_YDist < 0) then
  begin;
    XKoeff := -1;
    YKoeff := -1;
  end; //3rd Quarter
  if (S_XDist > 0) and (S_YDist < 0) then
  begin;
    XKoeff := 1;
    YKoeff := -1;
  end; //4th Quarter

  S_XDist := abs(S_XDist);
  S_YDist := abs(S_YDist);

  if ((S_Xdist / S_Ydist > 0.6) and (S_Xdist / S_Ydist < 1)) then
  begin; //  dy>dx, near 45 grd
    DrawPos.Y := OLdPOint.Y + YKoeff * S_XDist;
    Exit;
  end;
  if ((S_Ydist / S_Xdist > 0.6) and (S_Ydist / S_Xdist < 1)) then
  begin; //  dx>dy, near 45 grd
    DrawPos.X := OLdPOint.X + XKoeff * S_YDist;
    Exit;
  end;
  if S_Xdist / S_Ydist > 1 then
    DrawPos.Y := OldPoint.Y
  else
    DrawPos.X := OldPoint.X
end; //func
 {--Brovak}

 {++brovak BUG 258 }

procedure TProj.GetNewDrawPos1(var DrawPos: TDPOint);
var
  BasePoint1, BasePoint2, GrDrawPos: TGrPOint;
  i: integer;
  NDist: Double;
  NPoint: TDPoint;
  z1: TGrpoint;

  function GetDelta(CheckPoint: TGrPOint): double;
  var
    GIP: double;
  begin;
    GIP := GrPointDistance(GrDrawPos, CheckPoint);
    Result := SQRT(abs(GIP * GIP - NDIST * NDIST));
  end;

  procedure CalculateNewBaseLine;
  begin;
    if BasePoint2.X < BasePoint1.X then
    begin;
      if NPoint.X < BasePoint2.X then
        z1 := MovedPointAngle(BasePoint2, GetDelta(BasePoint2), LineAngle(BasePoint2, GrPoint(Npoint.X, NPoint.Y)))
      else
        z1 := MovedPointAngle(BasePoint1, GetDelta(BasePoint1), LineAngle(BasePoint1, GrPoint(Npoint.X, NPoint.Y)));
    end
    else
    begin;
      if NPoint.X > BasePoint2.X then
        z1 := MovedPointAngle(BasePoint2, GetDelta(BasePoint2), LineAngle(BasePoint2, GrPoint(Npoint.X, NPoint.Y)))
      else
        z1 := MovedPointAngle(BasePoint1, GetDelta(BasePoint1), LineAngle(BasePoint1, GrPoint(Npoint.X, NPoint.Y)));
    end;
  end;

begin;
  if NewPoly^.Data.Count < 2 then
    Exit;
  GrDrawPos := GrPoint(DrawPos.X, DrawPos.Y);
  i := NewPoly^.Data.Count - 2;
  BasePoint1 := Grpoint(TdPoint(NewPoly^.Data.At(i)^).X, TDpoint(NewPoly^.Data.At(i)^).Y);
  BasePoint2 := Grpoint(TdPoint(NewPoly^.Data.At(i + 1)^).X, TDpoint(NewPoly^.Data.At(i + 1)^).Y);

  if Pinfo.NormalDistance(Pdpoint(NewPoly^.Data.At(i)), PDPoint(NewPoly^.Data.At(i + 1)), @DrawPos, NDist, NPoint) = false then
  begin;
    CalculateNewBaseLine;
    DrawPos.X := Trunc(LimitToLong(Z1.X));
    DrawPos.Y := Trunc(LimitToLong(Z1.Y));
  end
  else
  begin;
    DrawPos.X := NPoint.X;
    DrawPos.Y := Npoint.Y;
  end;
  Exit;
end;
{--Brovak}

procedure TProj.GetNewDrawPos2(var DrawPos: TDPOint);
var
  BasePoint1, BasePoint2, GrDrawPos: TGrPOint;
  NDist: Double;
  NPoint: TDPoint;
  z1: TGrpoint;

  function GetDelta(CheckPoint: TGrPOint): double;
  var
    GIP: double;
  begin;
    GIP := GrPointDistance(GrDrawPos, CheckPoint);
    Result := SQRT(abs(GIP * GIP - NDIST * NDIST));
  end;

  procedure CalculateNewBaseLine;
  begin;
    if BasePoint2.X < BasePoint1.X then
    begin;
      if NPoint.X < BasePoint2.X then
        z1 := MovedPointAngle(BasePoint2, GetDelta(BasePoint2), LineAngle(BasePoint2, GrPoint(Npoint.X, NPoint.Y)))
      else
        z1 := MovedPointAngle(BasePoint1, GetDelta(BasePoint1), LineAngle(BasePoint1, GrPoint(Npoint.X, NPoint.Y)));
    end
    else
    begin;
      if NPoint.X > BasePoint2.X then
        z1 := MovedPointAngle(BasePoint2, GetDelta(BasePoint2), LineAngle(BasePoint2, GrPoint(Npoint.X, NPoint.Y)))
      else
        z1 := MovedPointAngle(BasePoint1, GetDelta(BasePoint1), LineAngle(BasePoint1, GrPoint(Npoint.X, NPoint.Y)));
    end;
  end;

begin;
  GrDrawPos := GrPoint(DrawPos.X, DrawPos.Y);
  BasePoint1 := Grpoint(Lines^.StartPos.X, Lines^.StartPos.Y);
  BasePoint2 := Grpoint(Lines^.EndPos.X, Lines^.EndPos.Y);

  if Pinfo.NormalDistance(@Lines.StartPos, @Lines.EndPos, @DrawPos, NDist, NPoint) = false then
  begin;
    CalculateNewBaseLine;
    DrawPos.X := Trunc(LimitToLong(Z1.X));
    DrawPos.Y := Trunc(LimitToLong(Z1.Y));
  end
  else
  begin;
    DrawPos.X := NPoint.X;
    DrawPos.Y := Npoint.Y;
  end;
end;

function TProj.GiveMeSourceObjectFromSelect(MirrorItem: Pindex): PIndex;
var
  i: integer;
  ALayer: PLayer;
begin;

  for i := 1 to Layers.LData.Count - 1 do
  begin // First layer - layer of selected items.
    ALayer := Layers.LData.At(i);
    Result := ALayer.HasIndexObject(MirrorItem.Index);
    if (Result <> nil) and (Boolean(Result.State and sf_Selected)) then
    begin
      if (Layers^.TranspLayer) and (IsEditVarious) then
      begin
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
        SetTransparentSelect(false, false);
        MenuFunctions['SelectTransparent'].Checked := false;
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
        Layers^.SetTopLayer(ALayer);
      end;
      Exit;
    end;
  end; // For iJ end
  Result := nil;
end;

procedure TProj.FindOldSnapItem;
begin;
  if (Layers^.SelLayer.Selinfo.HspCount = 1) and (SymbolMode <> sym_Omega) then
  begin;
    OldSnapItem := GiveMeSourceObjectFromSelect(Layers^.SelLayer.Data.At(0));
// ++ Cadmensky
    if OldSnapItem = nil then
      Exit;
// -- Cadmensky
    if not (OldSnapItem.GetObjType in [ot_POly, ot_CPoly (*,ot_Circle]*)]) then
      OldSnapItem := nil;
  end;
end;

{$IFNDEF WMLT}

procedure TProj.CommitCurrentDigitalization;
var
  APoint: TDPoint;
begin
  APoint.Init(LastClickPos.x + 1, LastClickPos.y + 1);
  NoClickInfo := True;
  MsgLeftClick(APoint);
  MsgLeftDblClick(APoint);
  NoClickInfo := False;
  APoint.Done;
end;

procedure TProj.MarkSnapPoint(DrawPos: TDPoint; bNoSnap: Boolean = False);
const
  d: Integer = 4;
var
  SnapPos {, IPoint}: TDPoint;
  SnapObject: PIndex;
  Point: TPoint;
  OldMapMode: Integer;

  procedure EraseMarker;
  var
    RRect: TRect;
  begin
    RRect.Left := CurSnapMarkerPoint.x - (d + 2);
    RRect.Right := CurSnapMarkerPoint.x + (d + 2);
    RRect.Top := CurSnapMarkerPoint.y - (d + 2);
    RRect.Bottom := CurSnapMarkerPoint.y + (d + 2);
    InvalidateRect(PInfo^.HWindow, @RRect, True);
  end;
begin
  if bNoSnap then
  begin
    SnapPos.Init(DrawPos.X, DrawPos.Y);

  end;

  SnapObject := nil;

  if (bNoSnap) or (SnapPoint(DrawPos, 0, ot_Any, SnapPos, SnapObject) and (SnapObject <> nil)) then
  begin
    PInfo^.ProjPointToVirtPoint(SnapPos, Point);
    with WingisMainForm.ActualChild.Canvas, Point do
    begin
      LPToDP(Handle, Point, 1);
      OldMapMode := SetMapMode(Handle, mm_Text);
      Pen.Color := clRed;
      Pen.Width := 1;
      MoveTo(X - d, Y - d);
      LineTo(X + d, Y - d);
      MoveTo(X + d, Y - d);
      LineTo(X + d, Y + d);
      MoveTo(X + d, Y + d);
      LineTo(X - d, Y + d);
      MoveTo(X - d, Y + d);
      LineTo(X - d, Y - d);
      if (x <> CurSnapMarkerPoint.x) or (y <> CurSnapMarkerPoint.y) then
        EraseMarker;
      CurSnapMarkerPoint.x := X;
      CurSnapMarkerPoint.y := Y;
      SetMapMode(Handle, OldMapMode);
    end;
  end
  else
  begin
    EraseMarker;
    CurSnapMarkerPoint.x := 0;
    CurSnapMarkerPoint.y := 0;
  end;
  PostMessage(PInfo^.HWindow, WM_PAINT, 0, 1);
end;

{$IFNDEF AXDLL} // <----------------- AXDLL

procedure TProj.DBInsertSymbol(Name, Lib: AnsiString; X, Y, Angle, SymSize: Double; ScaleInd: Integer);
var
  ASymbol: PSymbol;
  Pos: TDPoint;
  HasSize: Boolean;
  ObjectChanged: Boolean;
  CreateOnLayer: PLayer;
  TempStr: array[0..255] of Char;
  ProjID: LongInt;
  DBID: array[0..255] of Char;
begin
  DBID := '0';
  HasSize := True;
  if SymSize < 0.01 then
  begin
    SymSize := ActSymSize;
  end;
  Pos.X := Trunc(X * 100);
  Pos.Y := Trunc(Y * 100);
  CreateOnLayer := Layers^.TopLayer;
  if not CreateOnLayer^.GetState(sf_Fixed) then
  begin
    ASymbol := New(PSymbol, InitName(PInfo, Pos, Name, Lib, ActSymSize, ActSymSizeType, ActSymAngle));
    if ASymbol <> nil then
    begin
      if HasSize then
      begin
        if DblCompare(SymSize, dblNoChange) <> 0 then
        begin
          NumTools.Update(ASymbol^.Size, SymSize, ObjectChanged);
          NumTools.Update(ASymbol^.SizeType, ScaleInd, ObjectChanged);
        end;
      end;
      ASymbol^.CalculateClipRect(PInfo);
      if InsertObject(ASymbol, False) then
      begin
        if DDEHandler.FLayerInfo <> 0 then
        begin
          Str(CreateOnLayer^.Index: 10, TempStr);
          InsertedObjects^.Insert(StrNew(TempStr));
        end;
        ProjID := ASymbol^.Index;
        InsertedObjects^.Insert(StrNew(DBID));
        Str(ProjID: 10, TempStr);
        InsertedObjects^.Insert(StrNew(TempStr));
      end
      else
      begin
        if ASymbol <> nil then
        begin
          Dispose(ASymbol, Done);
          ASymbol := nil;
        end;
      end;
      if (SelNewObjectsDB) and (ASymbol <> nil) then
      begin
        Select(ASymbol);
      end;
      if BorderCorrected then
      begin
        BorderCorrected := False;
      end;
      ProcSendNewIDsToDB(@Self, 'GRD');
    end;
  end;
end;

procedure TProj.DBCreateBuffer(Id: Integer; DestLayer: string; CornerMode, BufferMode, Dist: Integer);
var
  DLayer: PLayer;
  OldTop: PLayer;
  SItem, AItem: PIndex;
  Poly: PPoly;

  function DoAll(Item: PLayer): Boolean;
  begin
    Result := False;
    if (not Item^.GetState(sf_LayerOff)) and (not Item^.GetState(sf_LayerOffAtGen)) then
    begin
      SItem := New(PIndex, Init(Id));
      AItem := nil;
      AItem := Item^.HasObject(SItem);
      if AItem <> nil then
      begin
        Poly := PPoly(Item^.IndexObject(PInfo, AItem));
        Item^.Select(PInfo, AItem);
        Result := True;
      end;
    end;
  end;

begin
  OldTop := Layers^.TopLayer;
  DeselectAll(False);
  Layers^.LData^.FirstThat(@DoAll);
  if AItem <> nil then
  begin
    DLayer := Layers^.NameToLayer(DestLayer);
    if DLayer = nil then
    begin
      DLayer := Layers^.TopLayer;
    end;
    if (DLayer <> nil) then
    begin
      OffsetItem := Poly;
      OffsetData.RoundEdges := CornerMode <> 0;
      OffsetData.OffsetType := TOffsetType(BufferMode);
      if Dist > 0 then
      begin
        Layers^.TopLayer := DLayer;
        ProcCreateOffset(@Self, Dist);
        Layers^.SetTopLayer(OldTop);
        DeselectAll(False);
      end
      else
      begin
        InpOffset^.SetPoly(PPoly(OffsetItem));
        InpOffset^.StartIt(PInfo, CurPos, FALSE);
        InpOffset^.MouseMove(PInfo, CurPos);
        SetActualMenu(mn_OffsetOK);
      end;
    end;
  end;
end;

procedure TProj.MouseActionFromDB(X, Y, Action: Integer);
var
  Pt: TDPoint;
begin
  Pt.x := x;
  Pt.y := y;
  NoClickInfo := True;
  case Action of
    0:
      begin
        MsgLeftClick(Pt);
        MsgLeftUp(Pt);
      end;
    1:
      begin
        MsgLeftClick(Pt);
        MsgLeftUp(Pt);
        MsgLeftDblClick(Pt);
      end;
  end;
  NoClickInfo := False;
end;

procedure TProj.ChangeImagePropertiesFromDB(Id, Display, TranspMode, Transp: Integer);
var
  SItem, IItem: PIndex;
  ObjectChanged: Boolean;
  Bitmap: PImage;

  function DoAll(Item: PLayer): Boolean;
  begin
    Result := False;
    if (not Item^.GetState(sf_LayerOff)) and (not Item^.GetState(sf_LayerOffAtGen)) then
    begin
      IItem := nil;
      IItem := Item^.HasObject(SItem);
      if IItem <> nil then
      begin
        if IItem^.GetObjType = ot_Image then
        begin
          Bitmap := PImage(Item^.IndexObject(PInfo, IItem));
          Result := True;
        end;
      end;
    end;
  end;

begin
  ObjectChanged := False;
  DeselectAll(False);
  SItem := New(PIndex, Init(Id));
  Bitmap := nil;
  Layers^.LData^.FirstThat(@DoAll);
  if Bitmap <> nil then
  begin
    with Bitmap^ do
    begin
      if Display >= 0 then
      begin
        if Display > 1 then
          NumTools.Update(ShowOpt, 1, ObjectChanged)
        else
          NumTools.Update(ShowOpt, 0, ObjectChanged);
        if (Display = 1) or (Display >= 3) then
          NumTools.Update(Options, 0, ObjectChanged)
        else
          NumTools.Update(Options, 1, ObjectChanged);
      end;
      if (Transp >= 0) and (Transp <= 100) then
        NumTools.Update(Transparency, Transp, ObjectChanged);
      if (TranspMode = 0) then
        NumTools.Update(TransparencyType, 2, ObjectChanged);
      if (TranspMode >= 1) then
        NumTools.Update(TransparencyType, 1, ObjectChanged);
    end;
    if ObjectChanged then
    begin
      SetModified;
      Bitmap.Invalidate(PInfo);
      PInfo^.RedrawInvalidate;
    end;
  end;
end;

procedure TProj.DeleteLayerListFromDb(s: AnsiString);
var
  Layername: string;
  lcnt, i: Integer;
  l, ex: PLayer;
begin
  ex := Layers^.TopLayer;
  lcnt := StrToInt(GetDDEItem(s, 2));
  for i := 0 to lcnt - 1 do
  begin
    Layername := Trim(GetDDEItem(s, i + 3));
    l := Layers^.NameToLayer(Layername);
    if (l <> nil) and (l <> ex) then
    begin
      ProcDeleteLayer(@Self, l);
      SetModified;
    end;
  end;
  PInfo^.RedrawScreen(False);
  UserInterface.Update([uiLayers, uiWorkingLayer, uiViews]);
end;

procedure TProj.DBCreateView(Name: string; SaveRegion, SaveLayers, IsDefault: Integer);
var
  Sight: PSight;
  Index: Integer;
begin
  if (SaveRegion <> 0) or (SaveLayers <> 0) then
  begin
    if SaveRegion <> 0 then
    begin
      Sight := New(PSight, Init(Name, PInfo^.GetCurrentScreen));
      if VEModule.VEForm.VEActive then
      begin
        Sight^.ViewRect.Rotation := 0;
      end;
    end
    else
    begin
      Sight := New(PSight, Init(Name, RotRect(0, 0, 0, 0, 0)));
    end;
  end;
  if Sight <> nil then
  begin
    if SaveLayers <> 0 then
    begin
      Sight^.SetLayers(Layers);
    end;
    Sights^.Insert(Sight);
    Index := Sights^.IndexOf(Sight);
    if IsDefault <> 0 then
    begin
      Sights^.Default := Index;
    end
    else
      if Index <= Sights.Default then
      begin
        Inc(Sights^.Default);
      end;
    Userinterface.Update([uiViews]);
    SetModified;
    ProcDBAddView(Name);
    ProcDBSendViews(@Self);
  end;
end;

procedure TProj.DBDeleteView(Name: string);
var
  Sight: PSight;
  Index: Integer;
begin

  Sight := Sights^.NamedSight(Name);
  if (Sight <> nil) then
  begin
    Index := Sights^.IndexOf(Sight);
    Sights^.AtFree(Index);
    if Index = Sights.Default then
      Sights.Default := -1
    else
      if Index < Sights.Default then
        Dec(Sights^.Default);
    SetModified;
    Userinterface.Update([uiViews]);
    ProcDBSendViews(@Self);
  end;
end;
{$ENDIF} // <----------------- AXDLL

function TProj.PolyCombine(DLayerName: string; Ids: array of Integer; IdCnt: Integer): Integer;
var
  Poly1, Poly2: PPoly;
  Item1, Item2: PIndex;
  n, i, j: Integer;
  Pnt, LPnt, RPnt: TDPoint;
  SearchRad: Integer;
// ++ Cadmensky
  APoint: TDPoint;
// -- Cadmensky
begin
  SearchRad := 500;
  Result := 0;
  for n := 0 to IdCnt - 1 do
  begin
    Item1 := New(PIndex, Init(Ids[0]));
    Poly1 := PPoly(Layers^.TopLayer^.IndexObject(PInfo, Item1));
    if Poly1 <> nil then
    begin
      for i := 1 to IdCnt - 1 do
      begin
        Item2 := New(PIndex, Init(Ids[i]));
        if Item2 <> nil then
        begin
          Poly2 := PPoly(Layers^.TopLayer^.IndexObject(PInfo, Item2));
          if Poly2 <> nil then
          begin
            LPnt.x := PDPoint(Poly1^.Data^.At(0))^.x;
            LPnt.y := PDPoint(Poly1^.Data^.At(0))^.y;

            RPnt.x := PDPoint(Poly1^.Data^.At(Poly1^.Data^.Count - 1))^.x;
            RPnt.y := PDPoint(Poly1^.Data^.At(Poly1^.Data^.Count - 1))^.y;

            Pnt.x := PDPoint(Poly2^.Data^.At(0))^.x;
            Pnt.y := PDPoint(Poly2^.Data^.At(0))^.y;

            if (Abs(Pnt.x - LPnt.x) < SearchRad) and (Abs(Pnt.y - LPnt.y) < SearchRad) then
            begin
              for j := Poly2^.Data.Count - 1 downto 1 do
              begin
                APoint.Init(TDPoint(Poly2.Data^.At(j)^).x, TDPoint(Poly2.Data^.At(j)^).y);
                Poly1^.Data^.AtInsert(0, @APoint);
                APoint.Done;
              end;
            end;

            if (Abs(Pnt.x - RPnt.x) < SearchRad) and (Abs(Pnt.y - RPnt.y) < SearchRad) then
            begin
              for j := 0 to Poly2^.Data.Count - 1 do
              begin
                APoint.Init(TDPoint(Poly2.Data^.At(j)^).x, TDPoint(Poly2.Data^.At(j)^).y);
                Poly1^.Data^.Insert(@APoint);
                APoint.Done;
              end;
            end;
          end;
        end;
      end;
    end;
  end;
  if Poly1 <> nil then
  begin
    Poly1^.CalculateClipRect;
{$IFNDEF AXDLL} // <----------------- AXDLL
    WingisMainForm.ActualChild.DBMoveToLayer('[MOL][' + DLayerName + '][1][' + IntToStr(Poly1^.Index) + ']');
{$ENDIF} // <----------------- AXDLL
    SetModified;
    if (Poly1^.Index > 1000000) then
    begin
      Result := Poly1^.Index;
    end;
    PInfo^.RedrawRect(Poly1^.ClipRect);
  end;
end;

procedure TProj.SetLayerSnap(Layername: string; OnOff: Integer);
var
  Layer: PLayer;
begin
  Layer := Layers^.NameToLayer(Layername);
  if Layer <> nil then
  begin
    if Layer^.UseForSnap <> (OnOff <> 0) then
    begin
      Layer^.UseForSnap := (OnOff <> 0);
      SetModified;
    end;
  end;
end;
{$ENDIF}

function TProj.FindUsedPoint(ARealPoint: TDPOint): integer;
var
  i: integer;
begin;
  Result := -1;
  if OLdSnapItem = nil then
    Exit;

  for i := 0 to Ppoly(OLdSnapItem.mptr)^.Data.Count - 1 do
  begin;
    if (ARealPoint.X = PDPOINT(PPoly(OLdSnapItem.mptr)^.Data^.At(i))^.X)
      and
      (ARealPoint.Y = PDPOINT(PPoly(OLdSnapItem.mptr)^.Data.At(i))^.Y) then
    begin;
      Result := i;
      Exit;
    end;
  end;
end;
{--brovak}

procedure TProj.MsgMouseWheel(Msg: TMessage);
var
  Factor: Double;
  BMFact: Double;
begin
  if bMouseZoom then
  begin
    if Msg.WParam > 0 then
      Factor := 2.0
    else
      Factor := 0.5;

    BMFact := VEForm.GetResFactor;
    ZoomByFactor(Factor * BMFact);
  end;
end;

function TProj.CheckFixedOrNot: boolean;
var
  m: integer;
  LayerItem: PIndex;
  LLayer: PLayer;
begin;
  Result := false;
  for m := 1 to Layers.LData.Count - 1 do
  begin
    LLayer := Layers.LData.At(m);
    if LLayer.GetState(sf_Fixed) = true then
    begin;
      LayerItem := LLayer^.HasObject(SnapItem);
      if LayerItem <> nil then
      begin;
        MsgBox(0, 1300, mb_Ok or MB_ICONINFORMATION, '');
        Result := true;
        DeselectAll(true);
        Pinfo.EditPoly := false;
        if EditVertexMode = 4 then
        begin;
          SnapItem := nil;
          OldSnapItem := nil;
          MenuHandler.SubHandler := nil;
        end;
        Exit;
      end;
    end;
  end;
end;

procedure TProj.CalculateSplitLength;
var
  i: Integer;
  gp1, gp2: TGrPoint;
  p1, p2: TDPoint;
  d: Double;
begin
  SplitL1 := 0;
  for i := 0 to InpOffset^.PoiNum - 1 do
  begin
    p1.Init(TDPoint(PPoly(InpOffset^.Poly)^.Data^.At(i)^).x, TDPoint(PPoly(InpOffset.Poly)^.Data^.At(i)^).y);
    p2.Init(TDPoint(PPoly(InpOffset^.Poly)^.Data^.At(i + 1)^).x, TDPoint(PPoly(InpOffset.Poly)^.Data^.At(i + 1)^).y);
    gp1.x := p1.x;
    gp1.y := p1.y;
    gp2.x := p2.x;
    gp2.y := p2.y;
    d := GrPointDistance(gp1, gp2);
    SplitL1 := SplitL1 + Trunc(d);
  end;
  p1.Init(TDPoint(PPoly(InpOffset^.Poly)^.Data^.At(InpOffset^.PoiNum)^).x, TDPoint(PPoly(InpOffset.Poly)^.Data^.At(InpOffset^.PoiNum)^).y);
  p2.Init(InpOffset^.StartPos.x, InpOffset^.StartPos.y);
  gp1.x := p1.x;
  gp1.y := p1.y;
  gp2.x := p2.x;
  gp2.y := p2.y;
  d := GrPointDistance(gp1, gp2);
  SplitL1 := SplitL1 + Trunc(d);
  SplitL2 := Trunc(InpOffset^.Poly^.Laenge * 100) - SplitL1;
  ClearStatusText;
  SetStatusText(Format('%.2f | %.2f', [SplitL1 / 100, SplitL2 / 100]), True);
end;

{$IFNDEF AXDLL} // <----------------- AXDLL
{$IFNDEF WMLT}

procedure TProj.SplitLine(Id: Integer; DestLayer: PLayer);
var
  Poly: PPoly;
  i: Integer;
  pt: TDPoint;
  OldTop: PLayer;
begin
  OldTop := Layers^.TopLayer;
  if DestLayer <> nil then
    Layers^.TopLayer := DestLayer;
  try
    for i := 0 to InpOffset^.PoiNum do
    begin
      pt.Init(TDPoint(PPoly(InpOffset^.Poly)^.Data^.At(i)^).x, TDPoint(PPoly(InpOffset.Poly)^.Data^.At(i)^).y);
      NewPoly^.PointInsert(PInfo, pt, TRUE);
    end;
    pt.Init(InpOffset^.StartPos.x, InpOffset^.StartPos.y);
    NewPoly^.PointInsert(PInfo, pt, TRUE);
    Poly := New(PPoly, Init);
    if not NewPoly^.CheckCopyData(PInfo, Poly, TRUE) then
    begin
      Dispose(Poly, Done);
      Poly := nil;
    end
    else
    begin
      if not InsertObject(Poly, FALSE) then
      begin
        Dispose(Poly, Done);
        Poly := nil;
      end;
    end;
    SetModified;
    ProcDBSendObject(@Self, PIndex(Poly), 'EIN');
    pt.Init(InpOffset^.StartPos.x, InpOffset^.StartPos.y);
    NewPoly^.PointInsert(PInfo, pt, TRUE);
    for i := InpOffset^.PoiNum + 1 to InpOffset^.Poly^.Data^.Count - 1 do
    begin
      pt.Init(TDPoint(PPoly(InpOffset^.Poly)^.Data^.At(i)^).x, TDPoint(PPoly(InpOffset.Poly)^.Data^.At(i)^).y);
      NewPoly^.PointInsert(PInfo, pt, TRUE);
    end;
    Poly := New(PPoly, Init);
    if not NewPoly^.CheckCopyData(PInfo, Poly, TRUE) then
    begin
      Dispose(Poly, Done);
      Poly := nil;
    end
    else
    begin
      if not InsertObject(Poly, FALSE) then
      begin
        Dispose(Poly, Done);
        Poly := nil;
      end;
    end;
    SetModified;
    ProcDBSendObject(@Self, PIndex(Poly), 'EIN');
  finally
    Layers^.TopLayer := OldTop;
  end;
end;
{$ENDIF}
{$ENDIF} // <----------------- AXDLL

procedure TProj.RefreshSymbolMan(Full: Boolean);
{$IFNDEF AXDLL} // <----------------- AXDLL
var
  Rollup: TSymbolRollupForm;
{$ENDIF} // <----------------- AXDLL
begin
{$IFNDEF AXDLL} // <----------------- AXDLL
  Rollup := TSymbolRollupForm(UserInterface.RollupNamed['SymbolRollupForm']);
  if Rollup <> nil then
  begin
    if Full then
    begin
      Userinterface.Update([uiSymbols]);
    end
    else
    begin
      Rollup.SymbolList.Repaint;
    end;
  end;
{$ENDIF} // <----------------- AXDLL
end;

 //by Brovak
 //Now part of code remed. May be it will useful in future.
 //Please don't remove it

{$IFNDEF AXDLL} // <----------------- AXDLL

procedure TProj.ConvertAnnotationToTexts;
var
  ii, j, Count: integer;
  TextsID, LayersID, AttacheID: PCOllection;
  TmpLayer, TmpLayer1, OldLayer: PLayer;
  Ntext: Ptext;
  ALayer: PLayer;
  BBitem: Pindex;
  DDITem: PRText;
  AAItem: PIndex;
  AView: PView;
  ID: Longint;
  TmpString, TmpString1: ansistring;

    //convert label object data to text data object

  function Makeit(KItem: PRText): boolean; far;
  var
    k: integer;
          //copying annotcollection data from label to text
  (*  procedure DoAll(AItem: PAnnotColl); far;
    var Annotation: PAnnotColl;
      Temp: array[0..255] of Char;
      HVal: Boolean;
    begin
      StrCopy(Temp, AItem^.Name);
      HVal := AItem^.HasVal;
      Ntext.LineNames^.Insert(New(PAnnotColl, Init(StrNew(Temp), HVal)));
    end; *)
  begin
    AAItem := Pointer(Layers.IndexObject(PInfo, KItem));
    DDITem := PRText(AAItem);
    if AAitem <> nil then
      for k := 1 to Layers.LData.Count - 1 do
      begin
        ALayer := Layers.LData.At(k);
        BBItem := ALayer.HasObject(AAItem);
        if Assigned(BBitem) then
          break;
      end;
    if BBitem <> nil then
    begin;
      if Layers.TopLayer <> ALayer then
        Layers.TopLayer := Alayer;
      if ALayer^.SelectIndexWithoutDlg(Self.PInfo, PRText(BBItem)) = true then
      begin;
        try
          NText := New(PText, Init(ActualFont, ActualText, 0));
          NText^.Pos.X := DDItem^.TextRect.A.X;
          NText^.Pos.Y := DDItem^.TextRect.A.Y;
          Ntext^.Font := DDItem^.Font;
            //convert Annotation text. Drop symbol #10 from string as not needed for text-object
          TmpString := StrPas(DDItem^.RText);
          TmpString1 := '';
          for k := 1 to Length(TmpString) do
            if TmpString[k] <> #10 then
              TmpString1 := TmpString1 + TmpString[k];
          NText^.Text := Newstr(TmpString1);
            //set annotation flag for text.
          NText^.IsItAnnot := true;
          NText^.IsUseOffset := false;
          NText^.ObjectOffSetType := -1;

      //    NText.ilSourceLayerID:=DDItem^.ilSourceLayerID;
      //    NText^.bKeepLinkToDBActive := DDItem^.bKeepLinkToDBActive;
          ID := DDItem^.ID;
      //    Ntext^.ID := DDItem^.ID;
            //Copying annotation's data collection
      //    DDItem^.LineNames.ForEach(@DoAll);
            //Insert pointer's of objects for future work
          TextsID^.Insert(Pointer(Ntext));
          LayersID^.Insert(Pointer(ALayer));
          AttacheID^.Insert(Pointer(ID));
          Inc(Count);
        except;
          ShowMessage('Error converting !');
        end;
      end; //select
    end;
    Result := false;
  end;

begin;
//exit;
//Statusbar.ProgressText:='Converting...';
//UserInterface.Update([uiStatusBar]);

//store old toplayer. It will return at end of procedure
  Oldlayer := Layers.TopLayer;

  TextsID := New(PCollection, Init(5, 5));
  LayersID := New(PCollection, Init(5, 5));
  AttacheID := New(PCollection, Init(5, 5));
//count of converted objects
  Count := 0;
  WasConverted := true; //global flag - we need make it procedure only once
//converting...
  PLayer(PInfo.Objects).FirstObjectType(ot_RText, @Makeit);
  if Count > 0 then
  begin;
    try
  //delete old annotations
{$IFNDEF WMLT}
      DeleteSelected(true);
{$ENDIF}
      DeselectAll(false);
      TmpLayer1 := OldLayer;
      for ii := 0 to Count - 1 do
      begin;
        TmpLayer := PLayer(LayersID^.At(ii));
    //if current top layer not layer for this object insert
        if Layers^.TopLayer <> TmpLayer then
          Layers^.TopLayer := TmpLayer;
        InsertObject(PText(TextsID^.At(ii)), false);
    //    PText(TextsID^.At(ii)).UpdateTextData(Pinfo);
    //search object for attach annotation to him
    //    ID := PText(TextsID^.At(ii)).ID;
        ID := Longint(AttacheID^.At(ii));
        BBItem := TmpLayer1.HasIndexObject(ID);
        if not Assigned(BBItem) then
          for j := 1 to Layers.LData.Count - 1 do
          begin
            TmpLayer := Layers.LData.At(j);
            BBItem := TmpLayer.HasIndexObject(ID);
            if Assigned(BBitem) then
            begin
              TmpLayer1 := TmpLayer;
              break;
            end;
          end;
        //if object was founded then..
        if Assigned(BBitem) then
        begin;
          if Layers^.TopLayer <> TmpLayer1 then
            Layers^.TopLayer := TmpLayer1;
          AView := PView(PLayer(PInfo^.Objects)^.IndexObject(PInfo, BBItem));
          PText(TextsID^.At(ii)).AttachToObject(PInfo, AView);
        end;
      end; //for
    except
      showMessage('Error inserting/deleting when converting');
    end;
  end; //count>0
//return old toplyer
  Layers.TopLayer := Oldlayer;

//free temporary collections
  TextsID.DeleteAll;
  TextsID.Done;
  LayersID.DeleteAll;
  LayersID.Done;
  AttacheID.DeleteAll;
  AttacheID.Done;
//We NEED save result of converting, because without it this new Wingis version can't work with
//annotation of old type
{$IFNDEF WMLT}
  if Count > 0 then
    TMDIChild(Self.Parent).FileSave;
{$ENDIF}
end;

{$ENDIF} // <----------------- AXDLL

{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL

procedure TProj.DBSetGeoText(Idx: Integer; s: AnsiString);
var
  SItem: PIndex;
  AItem: PIndex;
begin
  SItem := New(PIndex, Init(Idx));
  try
    AItem := Layers^.IndexObject(PInfo, SItem);
    if AItem <> nil then
    begin
      if s <> '' then
      begin
        AItem^.Tooltip := NewStr(s);
      end
      else
      begin
        Dispose(AItem^.Tooltip);
        AItem^.Tooltip := nil;
      end;
      SetModified;
    end;
  finally
    if SItem <> nil then
      Dispose(SItem);
  end;
end;

procedure TProj.DBGetGeoText(Idx: Integer);
var
  SItem: PIndex;
  AItem: PIndex;
  s: AnsiString;
begin
  s := '';
  SItem := New(PIndex, Init(Idx));
  try
    AItem := Layers^.IndexObject(PInfo, SItem);
    if AItem <> nil then
    begin
      if AItem^.Tooltip <> nil then
      begin
        s := AItem^.Tooltip^;
      end;
    end;
  finally
    DDEHandler.SendString('[RGTEXT][' + s + ']');
    if SItem <> nil then
      Dispose(SItem);
  end;
end;
{$ENDIF} // <----------------- AXDLL
{$ENDIF}

procedure TProj.DeleteUnusedSymbols;
var
  AnyDeleted: Boolean;
  DelList: TList;
  i: Integer;

  procedure DoAll(ASym: PSGroup);
  begin
    if ASym^.UseCount = 0 then
    begin
      DelList.Add(ASym);
    end;
  end;

begin
  AnyDeleted := False;
  DelList := TList.Create;
  try
    PSymbols(PInfo^.Symbols)^.Data^.ForEach(@DoAll);
    for i := 0 to DelList.Count - 1 do
    begin
{$IFNDEF WMLT}
{$IFNDEF AXDLL} // <----------------- AXDLL
      if WinGisMainForm.WeAreUsingUndoFunction[@Self] then
      begin
        WinGisMainForm.UndoRedo_Man.DeleteSymbol(@Self, DelList[i]);
      end;
{$ENDIF} // <----------------- AXDLL
{$ENDIF}
      if PSymbols(PInfo^.Symbols)^.DeleteSymbol(DelList[i]) then
      begin
        AnyDeleted := True;
        SetModified;
      end;
    end;
    if AnyDeleted then
    begin
      RefreshSymbolMan(True);
    end;
  finally
    DelList.Free;
  end;
end;

function TProj.HasLinkedObj(AIndex: PIndex): Boolean;
var
  AView: PView;
begin
  Result := False;
  AView := PLayer(PInfo^.Objects)^.IndexObject(PInfo, AIndex);
  if AView <> nil then
  begin
    Result := Assigned(AView^.AttData);
  end;
end;

function TProj.SetPixel(X, Y: Integer; ALayerIndex: Integer): PIndex;
var
  APixel: PPixel;
  APoint: TDPoint;
begin
  Result := nil;
  APoint.Init(X, Y);
  APixel := New(PPixel, Init(APoint));
  if not InsertObjectOnLayer(APixel, ALayerIndex) then
  begin
    Dispose(APixel, Done);
  end
  else
  begin
    Result := APixel;
  end;
end;

procedure TProj.OrientPolys(ADir: Boolean);

  procedure DoAll(AItem: PIndex);
  var
    APoly: PPoly;
    BDir: Boolean;
  begin
    if AItem^.GetObjType in [ot_CPoly, ot_Poly] then
    begin
      APoly := PCPoly(PLayer(PInfo^.Objects)^.IndexObject(PInfo, AItem));
      if APoly <> nil then
      begin
        BDir := APoly^.GetDirection;
        if ADir <> BDir then
        begin
          APoly^.ReversePoly;
        end;
      end;
    end;
  end;

begin
  Layers^.SelLayer^.Data^.ForEach(@DoAll);
end;

procedure TProj.ImportSymbolLib(Filename: AnsiString);
var
  AExtLib: PExtLib;
  ASym, BSym: PSGroup;
  i, n: Integer;
  SymName: PString;
begin
  if FileExists(Filename) then
  begin
    AExtLib := New(PExtLib, Init(nil, Filename));
    try
      AExtLib^.LoadExtLib;
      if AExtLib^.Symbols <> nil then
      begin
        n := AExtLib^.Symbols^.Data^.GetCount;
        for i := 0 to n - 1 do
        begin
          ASym := PSGroup(AExtLib^.Symbols^.Data^.At(i));
          if (ASym <> nil) and (ASym^.Name <> nil) then
          begin
            SymName := NewStr(ASym^.Name^);
            BSym := AExtLib^.GetSymbol(SymName);
            Dispose(SymName);
            if BSym <> nil then
            begin
              PSymbols(PInfo^.Symbols)^.CopySymIntoProject(PInfo, BSym);
            end;
          end;
        end;
        RefreshSymbolMan(True);
      end;
    finally
      Dispose(AExtLib, Done);
    end;
  end;
end;

procedure TProj.DoGenerateArea(APoint: TDPoint);
begin
  MyObject := TAreaObject.Create(Parent, @Self);
  try
    MyObject.ManualMode := True;
    MyObject.ManualPos.X := APoint.X;
    MyObject.ManualPos.Y := APoint.Y;
    MyObject.DoAreaWithoutDlg(Layers^.TopLayer^.Text^, Layers^.TopLayer^.Text^, ar_Area);
  finally
    MyObject.Free;
  end;
end;

procedure TProj.StoreGuids(S: TOldStream);
var
  i, n: Integer;
  AGUID: TGUID;
  AView: PView;
begin
  n := PLayer(PInfo^.Objects)^.Data.GetCount;
  S.Write(n, Sizeof(Integer));
  for i := 0 to n - 1 do
  begin
    AView := PView(PLayer(PInfo^.Objects)^.Data^.At(i));
    if AView <> nil then
    begin
      AGUID := AView^.GUID;
      S.Write(AGUID, Sizeof(TGUID));
    end;
  end;
end;

procedure TProj.LoadGuids(S: TOldStream); //TOLEClientStream);
var
  i, n: Integer;
  AGUID: TGUID;
  AView: PView;
begin
  n := 0;
  S.Read(n, Sizeof(Integer));
  for i := 0 to n - 1 do
  begin
    AView := PView(PLayer(PInfo^.Objects)^.Data^.At(i));
    if AView <> nil then
    begin
      S.Read(AGUID, Sizeof(TGUID));
      if s.Status <> stOk then
      begin
        break;
      end;
      AView^.GUID := AGUID;
    end;
  end;
end;

procedure TProj.FillGuids;
var
  i, n: Integer;
  AGUID: TGUID;
  AView: PView;
begin
  n := PLayer(PInfo^.Objects)^.Data^.GetCount;
  for i := 0 to n - 1 do
  begin
    AView := PView(PLayer(PInfo^.Objects)^.Data^.At(i));
    if AView <> nil then
    begin
      if GuidIsNull(AView^.GUID) then
      begin
        CoCreateGuid(AGUID);
        AView^.GUID := AGUID;
        SetModified;
      end;
    end;
  end;
end;

function TProj.IndexToGuid(AIndex: Integer): TGUID;
var
  AView: PView;
  AItem: PIndex;
begin
  if AIndex > 0 then
  begin
    AItem := New(PIndex, Init(AIndex));
    try
      AView := PLayer(PInfo^.Objects)^.IndexObject(PInfo, AItem);
      if AView <> nil then
      begin
        Result := AView^.GUID;
      end
      else
      begin
        ResetGuid(Result);
      end;
    finally
      Dispose(AItem, Done);
    end;
  end
  else
  begin
    ResetGuid(Result);
  end;
end;

function TProj.GUIDToIndex(AGUID: TGUID): Integer;
var
  AView: PView;

  function DoAll(AItem: PView): Boolean;
  begin
    Result := False;
    if (GuidToString(AItem^.GUID) = GuidToString(AGUID)) then
    begin
      Result := True;
    end;
  end;

begin
  AView := PView(PLayer(PInfo^.Objects)^.Data^.FirstThat(@DoAll));
  if AView <> nil then
  begin
    Result := AView^.Index;
  end
  else
  begin
    Result := 0;
  end;
end;

function TProj.InsertWKTObject(ALayer: PLayer; s: AnsiString): PView;
var
  ACPoly: PCPoly;
  APoly: PPoly;
  APixel: PPixel;
  AView: PView;
  APos: TDPoint;
begin
  Result := nil;
  AView := nil;
  if ALayer <> nil then
  begin
    s := Uppercase(s);
    s := StringReplace(s, #10, '', [rfReplaceAll]);
    s := StringReplace(s, #13, '', [rfReplaceAll]);

    if (Pos('GEOMETRYCOLLECTION', s) > 0) then
    begin
      s := StringReplace(s, 'GEOMETRYCOLLECTION', 'TEMP', []);
      s := StringReplace(s, 'POLYGON', '', [rfReplaceAll]);
      s := StringReplace(s, 'LINESTRING', '', [rfReplaceAll]);
      s := StringReplace(s, 'POINT', '', [rfReplaceAll]);
      s := StringReplace(s, 'TEMP', 'POLYGON', [rfReplaceAll]);
    end;

    if (Pos('POLYGON', s) > 0) then
    begin
      ACPoly := New(PCPoly, Init);
      if ACPoly^.ReadWKT(s) then
      begin
        AView := ACPoly;
      end
      else
      begin
        Dispose(ACPoly, Done);
      end;
    end
    else
      if (Pos('LINESTRING', s) > 0) then
      begin
        APoly := New(PPoly, Init);
        if APoly^.ReadWKT(s) then
        begin
          AView := APoly;
        end
        else
        begin
          Dispose(APoly, Done);
        end;
      end
      else
        if (Pos('POINT', s) > 0) then
        begin
          APos.Init(0, 0);
          APixel := New(PPixel, Init(APos));
          if APixel^.ReadWKT(s) then
          begin
            AView := APixel;
          end
          else
          begin
            Dispose(APixel, Done);
          end;
        end;
    if AView <> nil then
    begin
      if not InsertObjectOnLayer(AView, ALayer^.Index, True) then
      begin
        Dispose(AView, Done);
      end
      else
      begin
        Result := AView;
      end;
    end;
  end;
end;

procedure TProj.ProcScalePolyline;
var
  Angle1: Double;
  Angle2: Double;
  RAngle: Double;
  XScale: Double;
  YScale: Double;
  Point1: TDPoint;
  Point2: TDPoint;
  Count: Integer;
  DX: Integer;
  DY: Integer;
  i: Integer;
  XDist1: Double;
  YDist1: Double;
  XDist2: Double;
  YDist2: Double;
  XNew: Double;
  YNew: Double;
  MinX: Integer;
  MinY: Integer;
begin
  Point1.Init(0, 0);
  Point2.Init(0, 0);

  Count := ScalePolyObj^.Data^.Count;

  Point1.X := PDPoint(ScalePolyObj^.Data^.At(0))^.X;
  Point1.Y := PDPoint(ScalePolyObj^.Data^.At(0))^.Y;
  Point2.X := PDPoint(ScalePolyObj^.Data^.At(Count - 1))^.X;
  Point2.Y := PDPoint(ScalePolyObj^.Data^.At(Count - 1))^.X;

  if Point1.X > Point2.X then
  begin
    ScalePolyObj^.ReversePoly;
  end;

  if ScalePolyPos1.X > ScalePolyPos2.X then
  begin
    DX := ScalePolyPos1.X;
    ScalePolyPos1.X := ScalePolyPos2.X;
    ScalePolyPos2.X := DX;
    DY := ScalePolyPos1.Y;
    ScalePolyPos1.Y := ScalePolyPos2.Y;
    ScalePolyPos2.Y := DY;
  end;

  Point1.X := PDPoint(ScalePolyObj^.Data^.At(0))^.X;
  Point1.Y := PDPoint(ScalePolyObj^.Data^.At(0))^.Y;
  Point2.X := PDPoint(ScalePolyObj^.Data^.At(Count - 1))^.X;
  Point2.Y := PDPoint(ScalePolyObj^.Data^.At(Count - 1))^.X;

  DX := ScalePolyPos1.X - Point1.X;
  DY := ScalePolyPos1.Y - Point1.Y;

  for i := 0 to Count - 1 do
  begin
    Inc(PDPoint(ScalePolyObj^.Data^.At(i))^.X, DX);
    Inc(PDPoint(ScalePolyObj^.Data^.At(i))^.Y, DY);
  end;

  Angle1 := Point1.CalculateAngle(Point2);
  Angle2 := ScalePolyPos1.CalculateAngle(ScalePolyPos2);
  RAngle := Angle2 - Angle1;

  for i := 0 to Count - 1 do
  begin
    PDPoint(ScalePolyObj^.Data^.At(i))^.Rotate(RAngle);
  end;

  Point1.X := PDPoint(ScalePolyObj^.Data^.At(0))^.X;
  Point1.Y := PDPoint(ScalePolyObj^.Data^.At(0))^.Y;
  Point2.X := PDPoint(ScalePolyObj^.Data^.At(Count - 1))^.X;
  Point2.Y := PDPoint(ScalePolyObj^.Data^.At(Count - 1))^.X;

  DX := ScalePolyPos1.X - Point1.X;
  DY := ScalePolyPos1.Y - Point1.Y;

  for i := 0 to Count - 1 do
  begin
    PDPoint(ScalePolyObj^.Data^.At(i))^.Move(DX, DY);
  end;

  XDist1 := PDPoint(ScalePolyObj^.Data^.At(Count - 1))^.X - PDPoint(ScalePolyObj^.Data^.At(0))^.X;
  YDist1 := PDPoint(ScalePolyObj^.Data^.At(Count - 1))^.Y - PDPoint(ScalePolyObj^.Data^.At(0))^.Y;

  XDist2 := ScalePolyPos2.X - PDPoint(ScalePolyObj^.Data^.At(0))^.X;
  YDist2 := ScalePolyPos2.Y - PDPoint(ScalePolyObj^.Data^.At(0))^.Y;

  XScale := XDist2 / XDist1;
  YScale := YDist2 / YDist1;

  MinX := ScalePolyObj^.GivePosXMin;
  MinY := ScalePolyObj^.GivePosYMin;

  for i := 1 to Count - 1 do
  begin
    XNew := PDPoint(ScalePolyObj^.Data^.At(i))^.X - PDPoint(ScalePolyObj^.Data^.At(0))^.X;
    XNew := XNew * XScale;
    XNew := XNew + PDPoint(ScalePolyObj^.Data^.At(0))^.X;
    YNew := PDPoint(ScalePolyObj^.Data^.At(i))^.Y - PDPoint(ScalePolyObj^.Data^.At(0))^.Y;
    YNew := YNew * YScale;
    YNew := YNew + PDPoint(ScalePolyObj^.Data^.At(0))^.Y;
    PDPoint(ScalePolyObj^.Data^.At(i))^.X := Trunc(XNew);
    PDPoint(ScalePolyObj^.Data^.At(i))^.Y := Trunc(YNew);
  end;

  ScalePolyObj^.CalculateClipRect;
  PInfo^.RedrawScreen(False);
end;

begin
  RegisterType(RProj);
end.

