/////////////////////////////////////////////////////////////////////////////
//
// pcx.c
//
// PCX File DIL
//
// ImageMan Image Processing Library
//
// Copyright (c) 1991-5 Data Techniques, Inc.
// All Rights Reserved
//
// ///////////////////////////////////////////////////////////////////////////
// $History: PCX.C $
// 
// *****************  Version 7  *****************
// User: Erict        Date: 6/12/00    Time: 12:35p
// Updated in $/ImageMan 16-32/DILS
// The CalcImgLen() function did not subtract the overall offset
// fs->offset causing the ImgOpenEmbedded to work improperly.
// 
// *****************  Version 6  *****************
// User: Ericj        Date: 10/18/99   Time: 2:59p
// Updated in $/ImageMan 16-32/DILS
// VC6 include difference
// 
// *****************  Version 5  *****************
// User: Ericj        Date: 3/24/98    Time: 4:25p
// Updated in $/ImageMan 16-32/DILS
// Didn't freee scratch data.  Fixed 1,992(b) leak.
// 
// *****************  Version 4  *****************
// User: Timk         Date: 9/13/95    Time: 3:28p
// Updated in $/ImageMan 16-32/DILS
// Re-adjusted the 24-bit line-copying code to work in pixel increments
// instead of byte increments. This makes much more sense, and seems to
// work more accurately (i.e., no artifacts).
// 
// *****************  Version 3  *****************
// User: Timk         Date: 9/05/95    Time: 5:33p
// Updated in $/ImageMan 16-32/DILS
// Fixed 24-bit line overwrite bug.
// 
// *****************  Version 2  *****************
// User: Timk         Date: 6/30/95    Time: 12:04p
// Updated in $/ImageMan 16-32/DILS
// Part 1 of massive changes for read/write using function pointer blocks.
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 1/17/95    Time: 4:39p
// Created in $/ImageMan 16-32/DILS
// ImageMan 4.0 Beta 1

#include "internal.h"
#pragma hdrstop
#include <assert.h>
#include <stdarg.h>

#ifndef WIN32
#include <string.h>
#endif

#include "pcx.h"
#include "pcxrc.h"     // error/stringtable defines

#define DCXSIG (987654321L)
#define VERSION "PCX/DCX File Reader V2.0"
#define WBUFSIZE  512

#ifdef NDEBUG
#undef OutputDebugString
#define OutputDebugString(S) ((void)0)
#endif

typedef struct tagFileStruct {
	LPVOID		lpOpenInfo;
	LPVOID		lpHandle;
	LPIMG_IO		lpIO;
	long        offset;			  // offset of image in file
	long        imbedlen;		  // len of embedded image file
	long        lRowOffset;		  // offset for next row for this image
	DWORD FAR  *lpOffsets;		  // If DCX file -> offsets to each page
	long        lImgLen;			  // Length of current image
	UINT        row;				  // next row to read for this image
	UINT        scratchSize;	  // Size of scratch buffer for a scan line
	int         nPageNo;			  // Current Page #
	int         nPages;			  // # of pgs in file
	PCXHEADER   pcx;				  // PCX header info
	HGLOBAL     hscratch;		  // scratch buffer
}           FileStruct;

static int  wencget(FileStruct FAR *, UCHAR FAR *);
static int  fill_palette(LPBITMAPINFO, FileStruct FAR *);
static int  read_row(FileStruct FAR *, LPSTR, LPSTR, int, int);
static long CalcImgLen(FileStruct FAR *, int);
#ifndef WIN32
static DWORD timeGetTime(void);	// Changed in VC6 - EJW
#endif

#ifdef WIN32

#ifdef WINGIS // so the file TLSByJHC.h was included

#ifdef NDEBUG
#define SETERROR(E) bJHCTLSSetValue((DWORD)(E))
#else

static void IM2PCXerr(int);
#define SETERROR(E) { \
	SETERROR(E) bJHCTLSSetValue((DWORD)(E)); \
	if ((E) != IMG_OK) IM2PCXerr(E); \
	}

#endif

#define GETERROR    nJHCTLSGetValue()

#else

static DWORD TlsIndex;			  // Thread local storage index
#ifdef NDEBUG
#define SETERROR(E) TlsSetValue(TlsIndex,(LPVOID)(DWORD)(E))
#else
static void IM2PCXerr(int);
#define SETERROR(E) { \
	TlsSetValue(TlsIndex,(LPVOID)(DWORD)(E)); \
	if ((E) != IMG_OK) IM2PCXerr(E); \
	}
#endif
#define GETERROR    ((DWORD)TlsGetValue(TlsIndex))
#endif

#else
static int  nErrID;				  // id of latest error
#define SETERROR(E) nErrID = (E)
#define GETERROR    (nErrID)
#endif


static HINSTANCE pcxInstance;

#if DEBUG
static DEBUGPROC DebugStringOut;
#endif

static const unsigned short translate[16] = {
	0x0000, 0x0100, 0x1000, 0x1100, 0x0001, 0x0101, 0x1001, 0x1101,
	0x0010, 0x0110, 0x1010, 0x1110, 0x0011, 0x0111, 0x1011, 0x1111
};


#ifdef WIN32
// /////////////////////////////////////////////////////////////////////////
//
// DllMain
//
// This is the Win32 'DllEntryPoint'. It is similar in function to LibMain
// and the WEP in 16-bit land, but is much more thorough. LibMain is
// called whenever a process or thread attaches or detaches to the
// BMP DIL
//
// Entry:
// hinstDLL  - handle to the ImageMan DLL
// fdwReason - notification code (attach/detach)
// DLL_PROCESS_ATTACH
// DLL_PROCESS_DETACH
// DLL_THREAD_ATTACH
// DLL_THREAD_DETACH
// lpvReserved - NULL during dynamic process attach/detach, non-NULL
// during static attach/detach.
// Exit:
// DLL_PROCESS_ATTACH - TRUE for success, FALSE for failure
// all others         - return code ignored
// /////////////////////////////////////////////////////////////////////////

BOOL APIENTRY DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
	switch (fdwReason) {
		// The DLL is attaching to a process, due to LoadLibrary.
		case DLL_PROCESS_ATTACH:
			assert(!lpvReserved);  // must always be loaded via LoadLibrary!

			pcxInstance = hinstDLL;// save our identity

#ifndef WINGIS
			// Allocate a "Thread Local Storage" (TLS) index right away.

			if ((TlsIndex = TlsAlloc()) == 0xFFFFFFFF)
				return (FALSE);
#else
			bJHCTLSClear();
#endif
			// Fall thru: Initialize the index for first thread.

		// The attached process created a new thread.
		case DLL_THREAD_ATTACH:
#ifndef WINGIS
			// Initialize the TLS index for this thread... it's actually 'nErrID'
			if (!TlsSetValue(TlsIndex, (LPVOID) 0))
				return (FALSE);
#else
			nJHCTLSAlloc(); // Stamp this thread to be existed
#endif
			break;

		// The thread of the attached process terminates.
		case DLL_THREAD_DETACH:
#ifdef WINGIS
			bJHCTLSFree(); // Free this thread entry
#endif
			break;

		// The DLL is detaching from a process, due to a call to FreeLibrary.
		case DLL_PROCESS_DETACH:
#ifndef WINGIS
			// Release the TLS index.
			TlsFree(TlsIndex);
#else
			bJHCTLSClear();
#endif
			break;
	}

	return (TRUE);

	UNREFERENCED_PARAMETER(lpvReserved);
}

#else // WIN32

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int WINAPI  LibMain(HINSTANCE hInstance, WORD wDataSeg, WORD wHeapSize, LPSTR lpCmdLine)
{
#if DEBUG
	{
		HANDLE      hMod = GetModuleHandle("IMGMAN2.DLL");
		if (hMod)
			DebugStringOut = (DEBUGPROC) GetProcAddress(hMod, "DebugStringOut");
	}
#endif

	if (!wHeapSize)
		return (0);					  // no heap indicates fatal error

	pcxInstance = hInstance;
	return (1);
}

#endif // WIN32

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
void WINAPI WEP(int bSystemExit)
{
	return;
	UNREFERENCED_PARAMETER(bSystemExit);
}

//------------------------------------------------------------------

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
BOOL IMAPI  IMDLverify_img(LPSTR lpVerfBuf, LPVOID lpHandle, LPIMG_IO lpIO)
{
	return ((((LPPCXHEADER) lpVerfBuf)->man == 10 && ((LPPCXHEADER) lpVerfBuf)->enc == 1)
			  || ((LPDCXHEADER) lpVerfBuf)->dwSig == DCXSIG);
	UNREFERENCED_PARAMETER(lpHandle);
	UNREFERENCED_PARAMETER(lpIO);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLset_page(HGLOBAL hImage, int nPage)
{
	PCXHEADER   pcx;
	FileStruct FAR *fs;
	UINT        scratchsize;
	int         retval = IMG_OK;

	if (!(fs = (FileStruct FAR *) GlobalLock(hImage))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}
	DEBUGOUT(DILAPI, "\t%s-->set_page(%d,%d)", (LPSTR) __FILE__, hImage, nPage);

	fs->lpIO->seek(fs->lpHandle, fs->lpOffsets[nPage] + fs->offset, 0);

	if ((fs->lpIO->read(fs->lpHandle, (LPSTR) & pcx, sizeof(PCXHEADER)) != sizeof(PCXHEADER))) {
		SETERROR(IDS_READERR);
		return (IMG_FILE_ERR);
	}
	// check the header info
	if (pcx.man != 10) {
		SETERROR(IDS_BADMAN);
		retval = IMG_FILE_ERR;
	} else if (pcx.ver != 0 && pcx.ver != 2 && pcx.ver != 3 && pcx.ver != 4 && pcx.ver != 5) {
		SETERROR(IDS_BADVER);
		retval = IMG_FILE_ERR;
	} else
		retval = IMG_OK;

	scratchsize = pcx.nplanes * pcx.byteline;
	if (fs->scratchSize < scratchsize) {
		// This page requires a larger scratch buffer.
		if (!(fs->hscratch = GlobalReAlloc(fs->hscratch, scratchsize + 2, DLLHND))) {
			fs->scratchSize = 0;
			SETERROR(IDS_MEMERR);
			retval = IMG_NOMEM;
		}
		fs->scratchSize = scratchsize;
	}
	fs->pcx = pcx;
	fs->lImgLen = CalcImgLen(fs, nPage);
	fs->row = 0;
	fs->nPageNo = nPage;
	fs->lRowOffset = sizeof(PCXHEADER) + fs->lpOffsets[nPage] + fs->offset;

	return (retval);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLget_page(HGLOBAL hImage, LPINT nPage)
{
	FileStruct FAR *fs;

	if (!(fs = (FileStruct FAR *) GlobalLock(hImage))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}
	DEBUGOUT(DILAPI, "\t%s-->get_page(%d,%x:%x)", (LPSTR) __FILE__, hImage, HIWORD(nPage), LOWORD(nPage));

	*nPage = fs->nPageNo;

	GlobalUnlock(hImage);

	return (IMG_OK);
}


// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
static long CalcImgLen(FileStruct FAR * fs, int nPage)
{
	long        len, lEOF, lPos;

	if (nPage == fs->nPages - 1) {
		if (fs->offset) lEOF = fs->offset + fs->imbedlen;
		else {
			lPos = fs->lpIO->seek(fs->lpHandle, 0, 1);
			lEOF = fs->lpIO->seek(fs->lpHandle, 0, 2);
			fs->lpIO->seek(fs->lpHandle, lPos, 0);
		}
		len = lEOF - fs->lpOffsets[nPage] - fs->offset;
	} else
		len = fs->lpOffsets[nPage + 1] - fs->lpOffsets[nPage] - fs->offset;

	return (len);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLescape(HGLOBAL hImage, int nInCnt, LPVOID lpIn, LPVOID lpOut)
{
	return (IMG_NSUPPORT);
	UNREFERENCED_PARAMETER(hImage);
	UNREFERENCED_PARAMETER(nInCnt);
	UNREFERENCED_PARAMETER(lpIn);
	UNREFERENCED_PARAMETER(lpOut);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
LPSTR IMAPI IMDLget_ver(void)
{
	return ((LPSTR) VERSION);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLinit_image(LPCSTR lpOpenInfo, LPHANDLE hFile, LPVOID lpHandle, long offset,
									            long lLen, LPIMG_IO lpIO)
{
	FileStruct FAR *fs;
	PCXHEADER   pcx;
	int         retval;
	long        lSig;
	DWORD FAR  *lpOffsets;
	HANDLE      hMem;
	int         nSize;
	int         nPages = 0;

	lpIO->seek(lpHandle, offset, 0);

	// Read a long to determine if it's a DCX or PCX file
	lpIO->read(lpHandle, (HPSTR)&lSig, sizeof(lSig));
	lpIO->seek(lpHandle, -4l, 1);		  // Move back 4 bytes

	if (lSig == DCXSIG) {
		DCXHEADER   dcxHdr;

		// Load DCX Info
		lpIO->read(lpHandle, (HPSTR)&dcxHdr, sizeof(dcxHdr));

		// Count the # of pages in file
		while (nPages < 1024 && dcxHdr.dwOffsets[nPages])
			nPages++;

		// Position file at 1st PCX header
		lpIO->seek(lpHandle, dcxHdr.dwOffsets[0] + offset, 0);
		nSize = sizeof(DWORD) * nPages;

		if (hMem = GlobalAlloc(DLLHND, nSize)) {
			if (lpOffsets = (DWORD FAR *) GlobalLock(hMem)) {
				_fmemcpy(lpOffsets, dcxHdr.dwOffsets, nSize);
			}
		}
	} else {							  // PCX Format

		nPages = 1;

		nSize = sizeof(DWORD) * nPages;
		if (hMem = GlobalAlloc(DLLHND, nSize)) {
			if (lpOffsets = (DWORD FAR *) GlobalLock(hMem)) {
				lpOffsets[0] = 0;
			}
		}
	}

	if ((lpIO->read(lpHandle, (LPSTR) & pcx, sizeof(PCXHEADER)) != sizeof(PCXHEADER))) {
		lpIO->close(lpHandle);
		SETERROR(IDS_READERR);
		return (IMG_FILE_ERR);
	}
	// check the header info
	if (pcx.man != 10) {
		SETERROR(IDS_BADMAN);
		retval = IMG_FILE_ERR;
	} else if (pcx.ver != 0 && pcx.ver != 2 && pcx.ver != 3 && pcx.ver != 4 && pcx.ver != 5) {
		SETERROR(IDS_BADVER);
		retval = IMG_FILE_ERR;
	} else
		retval = IMG_OK;

	if (retval != IMG_OK) {
		lpIO->close(lpHandle);
		return (retval);
	}
	// create new internal structure
	if (!(*hFile = GlobalAlloc(DLLHND, sizeof(FileStruct)))) {
		lpIO->close(lpHandle);
		SETERROR(IDS_MEMERR);
		return (IMG_NOMEM);
	}
	// fill in the structure
	fs = (FileStruct FAR *) GlobalLock(*hFile);
	fs->imbedlen = lLen;
	fs->nPages = nPages;
	fs->pcx = pcx;
	fs->lpOpenInfo = (LPVOID)lpOpenInfo;
	fs->lpHandle = lpHandle;
	fs->lpIO = lpIO;
	fs->scratchSize = pcx.nplanes * pcx.byteline;
	fs->offset = offset;
	if (!(fs->hscratch = GlobalAlloc(DLLHND, fs->scratchSize + 2))) {
		GlobalUnlock(*hFile);
		lpIO->close(lpHandle);
		GlobalFree(*hFile);
		SETERROR(IDS_MEMERR);
		return (IMG_NOMEM);
	}

	fs->row = 0;
	fs->lRowOffset = sizeof(PCXHEADER) + fs->offset;
	fs->lpOffsets = lpOffsets;
	fs->lImgLen = CalcImgLen(fs, 0);

	if (!fs->imbedlen)
		fs->imbedlen = fs->lImgLen;

	GlobalUnlock(*hFile);

	DEBUGOUT(IMGDETAIL, "\t\tPCX Ver = %d", pcx.ver);
	DEBUGOUT(IMGDETAIL, "\t\tPCX Bits/Pixel = %d", pcx.bits);
	DEBUGOUT(IMGDETAIL, "\t\tPCX Planes = %d", pcx.nplanes);

	return (IMG_OK);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLset_ext(HGLOBAL hFile, LPSTR ext)
{
	DEBUGOUT(DILAPI, "\t%s-->set_ext(%d,%s)", (LPSTR) __FILE__, hFile, (LPSTR) ext);
	// no extensions to differentiate between - just return OK
	return (IMG_OK);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLclose_image(HGLOBAL hFile)
{
	FileStruct FAR *fs;

	DEBUGOUT(DILAPI, "\t%s-->close_image(%d)", (LPSTR) __FILE__, hFile);
	// check to see if file is still open - if it is, close it
	if ((fs = (FileStruct FAR *) GlobalLock(hFile)) == NULL) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}
	if (fs->lpHandle) fs->lpIO->close(fs->lpHandle);

	if (fs->lpOffsets)
		GlobalFreePtr(fs->lpOffsets);

	if (fs->hscratch)
		GlobalFree(fs->hscratch);

	GlobalUnlock(hFile);
	GlobalFree(hFile);
	return (IMG_OK);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLimg_name(HGLOBAL hFile, LPHANDLE lpHand)
{
	FileStruct FAR *fs;

	if (!(fs = (FileStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}
//	*lpHand = fs->fName;

	GlobalUnlock(hFile);
	return (IMG_OK);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLopen(HGLOBAL hFile, LPVOID lpHandle)
{
	FileStruct FAR *fs;

	if (!(fs = (FileStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}
	if (!fs->lpHandle) {
		if (fs->lpIO->open(fs->lpOpenInfo, OF_READ, &(fs->lpHandle)) != IMG_OK) {
			GlobalUnlock(hFile);
			SETERROR(IDS_NOOPEN);
			return (IMG_NOFILE);
		}
	}
	fs->lpIO->seek(fs->lpHandle, fs->lRowOffset, 0);
	GlobalUnlock(hFile);

	return (IMG_OK);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLclose_keep(HGLOBAL hFile)
{
	FileStruct FAR *fs;

	DEBUGOUT(DILAPI, "\t%s-->close_keep(%d)", (LPSTR) __FILE__, hFile);
	if (!(fs = (FileStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}
	if (fs->lpHandle) {
		fs->lpIO->close(fs->lpHandle);
		fs->lpHandle = NULL;
	}
	GlobalUnlock(hFile);
	return (IMG_OK);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLread_rows(HGLOBAL hFile, LPSTR buf, UINT rows, UINT rowSize,
								              UINT start, LPSTATUS lpStat)
{
	FileStruct FAR *fs;
	LPSTR       scratch;
	HPSTR       tmpBuf;
	int         tmp;
	int         n;					  // # of bits/pixel for returned DIB

	DEBUGOUT(DILAPI, "\t%s-->%s(%d, %x:%x, %u, %u, %u, %x:%x)", (LPSTR) __FILE__, (LPSTR) "read_rows", hFile, HIWORD(buf), LOWORD(buf), rows, rowSize, start, HIWORD(lpStat), LOWORD(lpStat));

	if (!(fs = (FileStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}
	assert(fs->scratchSize);	  // make sure we have a scratch buffer

	// figure where to start when reading a row given passed start loc
	if (fs->pcx.nplanes == 4)
		n = 4;
	else if (fs->pcx.nplanes == 3) {
		// pcx.bits == 8 indicates 24-bit color image
		n = (fs->pcx.bits == 8) ? 3 : 4;
	} else
		n = 1;
	start /= n;

	scratch = GlobalLock(fs->hscratch);

	tmpBuf = (HPSTR) buf;
	tmpBuf += (rows - 1) * (long) rowSize;

	fs->row += rows;

	while (rows--) {
		if ((tmp = read_row(fs, tmpBuf, scratch, start, rowSize)) != IMG_OK) {
			return tmp;
		}
		tmpBuf -= rowSize;
		lpStat->lAcc += rowSize;
		if (lpStat->lpfnStat && lpStat->lAcc > lpStat->lInterval) {
			lpStat->lDone += lpStat->lAcc;
			lpStat->lDone = min(lpStat->lDone, lpStat->lTotal);
			(*lpStat->lpfnStat) (lpStat->hImage, (int) ((lpStat->lDone * 100) / lpStat->lTotal), lpStat->dwUserInfo);
			lpStat->lAcc %= lpStat->lInterval;
		}
	}

	GlobalUnlock(fs->hscratch);
	GlobalUnlock(hFile);
	return (IMG_OK);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
static int  read_row(FileStruct FAR * fs, LPSTR buf, LPSTR scratch, int start, int len)
{
	LPSTR       cout, coutMax;
	LPSTR       cout2[4];
	UCHAR       data;
	int         count;
	int         i, wid;
	USHORT      wdata, wdata2, HUGE * ibuf;
	UCHAR       bdata;
	HPSTR       tmpbuf;

	cout = scratch;
	coutMax = scratch + (fs->pcx.byteline * fs->pcx.nplanes);

	// read in the PCX format scan line
	do {

		if ((count = wencget(fs, &data)) < 0) {
			SETERROR(IDS_EOF);
			return (IMG_FILE_ERR);
		}
		if ((cout + count) > coutMax)
			count = coutMax - cout;

		while (count--)
			*cout++ = data;

	} while (cout < coutMax);

	// convert the scratch scan line into a DIB scan line
	// we've got nplanes scan lines to combine - each line is byteline bytes
	// long.

	wid = fs->pcx.win[2] - fs->pcx.win[0] + 1;

	scratch += start;
	cout = scratch;
	if (fs->pcx.nplanes == 1) {  // just copy mono-plane bits to buf

		if (len > fs->pcx.byteline)
			len = fs->pcx.byteline;

		hmemcpy(buf, scratch, len);
	} else if (fs->pcx.nplanes * fs->pcx.bits < 24) {
		ibuf = (USHORT HUGE *) buf;
		cout2[0] = cout;
		cout2[1] = cout + fs->pcx.byteline;
		cout2[2] = cout2[1] + fs->pcx.byteline;
		cout2[3] = cout2[2] + fs->pcx.byteline;
		while ((cout2[0] < scratch + (wid + 7) / 8) && (len > 0)) {
			wdata = 0;
			wdata2 = 0;
			for (i = fs->pcx.nplanes - 1; i >= 0; i--) {
				wdata <<= 1;
				wdata2 <<= 1;
				bdata = *(cout2[i])++;
				wdata |= translate[bdata >> 4];
				wdata2 |= translate[bdata & 0x0f];
			}
			*ibuf++ = wdata;
			*ibuf++ = wdata2;
			len -= 4;
		}
	} else if (fs->pcx.nplanes * fs->pcx.bits == 24) {	// 24-bit image
		//
		// In a 24-bit image, we have 3 rows of fs->pcx.byteline bytes
		// Order is bgr.
		//
		// We need to combine 1st byte of 1st row with 1st byte of 2nd row
		// with 1st byte of 3rd row -- this makes 1 DIB pixel, 24 bits long
		//

		i = fs->pcx.byteline;	  // makes for easy access to byteline from asm

		for (tmpbuf = (HPSTR) buf, len=0; len<wid ; len++, cout++) {
			*tmpbuf++ = *(cout + 2 * i);	// get B
			*tmpbuf++ = *(cout + i);	// get G
			*tmpbuf++ = *cout;	  // get R

		}
	}
	return (IMG_OK);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLset_row(HGLOBAL hFile, UINT row)
{
	FileStruct FAR *fs;
	register int count;
	int         bytes, rowbytes;
	UCHAR       data;
	UINT        nRowCnt = row;

	DEBUGOUT(DILAPI, "\t%s-->set_row(%d,%d)", (LPSTR) __FILE__, hFile, row);
	if (!(fs = (FileStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}
	if (!row) {
		fs->lpIO->seek(fs->lpHandle, sizeof(PCXHEADER) + fs->lpOffsets[fs->nPageNo] + fs->offset, 0);
	} else {
		//
		// adjust for the fact that we've got a current row
		// If row < fs->row, we've got to start from beginning of file,
		// otherwise, we can calculate an offset from current row.
		if (nRowCnt >= fs->row) {
			nRowCnt -= fs->row;
			fs->lpIO->seek(fs->lpHandle, fs->lRowOffset, 0);
		} else {
			fs->lpIO->seek(fs->lpHandle, sizeof(PCXHEADER) + fs->lpOffsets[fs->nPageNo] + fs->offset, 0);
		}

		// read in the PCX format scan line
		rowbytes = fs->pcx.byteline * fs->pcx.nplanes;
		while (nRowCnt--) {
			bytes = 0;
			do {
				if ((count = wencget(fs, &data)) < 0) {
					GlobalUnlock(hFile);
					SETERROR(IDS_EOF);
					return (IMG_FILE_ERR);
				}
				bytes += count;
			} while (bytes < rowbytes);
		}
	}

	fs->row = row;

	GlobalUnlock(hFile);
	return (IMG_OK);
}

// /////////////////////////////////////////////////////////////////////////
//
// wencget
//
// Reads a count/data pair from the PCX/DCX file.
//
// Entry:
// fs   - pointer to our file's state
// pbyt - location to place data
// Exit:
// Returns -1 on error, or the data count if successful
//
// /////////////////////////////////////////////////////////////////////////

static int  wencget(FileStruct FAR * fs, UCHAR FAR * pbyt)
{
	int         i;
	register UCHAR c;

	i = 1;							  // safety play

	c = (UCHAR) (fs->lpIO->getchar(fs->lpHandle));
	if ((c & 0xc0) == 0xc0) {
		// encoded... get pixel value
		i = c & 0x3f;
		c = (UCHAR) (fs->lpIO->getchar(fs->lpHandle));
	}
	*pbyt = c;

	return (i);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLbuildDIBhead(HGLOBAL hFile, LPINTERNALINFO lpii)
{
	LPBITMAPINFO lpbi;
	BITMAPINFOHEADER bi;
	int         nBits, nDIBits;
	FileStruct FAR *fs;
	long        wid, hi;
	HGLOBAL     hMem;

	DEBUGOUT(DILAPI, "\t%s-->buildDIBhead(%d)", (LPSTR) __FILE__, hFile);
	if (!(fs = (FileStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}
	nBits = fs->pcx.nplanes * fs->pcx.bits;

	if (nBits == 1) {
		nDIBits = 1;
		bi.biClrUsed = 2;
	} else if (nBits < 8) {
		nDIBits = 4;
		bi.biClrUsed = 16;
	} else if (nBits < 24) {
		nDIBits = 8;
		bi.biClrUsed = 256;
	} else {
		nDIBits = 24;
		bi.biClrUsed = 0;
	}

	wid = fs->pcx.win[2] - fs->pcx.win[0] + 1;
	hi = fs->pcx.win[3] - fs->pcx.win[1] + 1;

	bi.biSize = sizeof(BITMAPINFOHEADER);
	bi.biWidth = wid;
	bi.biHeight = hi;
	bi.biPlanes = 1;				  // Always

	bi.biBitCount = nDIBits;
	bi.biCompression = BI_RGB;
	bi.biSizeImage = ROWSIZE(wid, nDIBits) * hi;
	bi.biXPelsPerMeter = (long) (fs->pcx.hrez * 39.37);	// inches -> meters

	bi.biYPelsPerMeter = (long) (fs->pcx.vrez * 39.37);	// inches -> meters

	bi.biClrImportant = 0;

	lpii->lOrgWid = (long) wid;
	lpii->lOrgHi = (long) hi;
	lpii->nPages = fs->nPages;
	lpii->bFlags = 0;

	if (!(hMem = GlobalAlloc(GHND, sizeof(BITMAPINFOHEADER) + PALETTESIZE(&bi)))) {
		SETERROR(IDS_MEMERR);
		return (IMG_NOMEM);
	}
	lpbi = (LPBITMAPINFO) GlobalLock(hMem);
	*(LPBITMAPINFOHEADER) lpbi = bi;

	// now fill in the palette information
	fill_palette(lpbi, fs);

	lpii->lpbi = lpbi;

	GlobalUnlock(hFile);
	return (IMG_OK);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLprint(HGLOBAL hFile, HDC hPrnDC, LPRECT lpDest, LPRECT lpSrc)
{
	SETERROR(IDS_NOPRINT);
	return (IMG_NOTAVAIL);
	UNREFERENCED_PARAMETER(hFile);
	UNREFERENCED_PARAMETER(hPrnDC);
	UNREFERENCED_PARAMETER(lpDest);
	UNREFERENCED_PARAMETER(lpSrc);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLrender(HGLOBAL hFile, HDC hDC, LPRECT lpDest, LPRECT lpSrc)
{
	return (IMG_ERR);
	UNREFERENCED_PARAMETER(hFile);
	UNREFERENCED_PARAMETER(hDC);
	UNREFERENCED_PARAMETER(lpDest);
	UNREFERENCED_PARAMETER(lpSrc);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLerror_string(LPSTR lpszBuf, int nMaxLen)
{
	LoadString(pcxInstance, GETERROR, lpszBuf, nMaxLen);
	return (IMG_OK);
}

// ======================================================================
// Helper Routines
// ======================================================================

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
static int  fill_palette(LPBITMAPINFO lpbi, FileStruct FAR * fs)
{
#define SETRGB(I,R,G,B)  *(LPDWORD)((lpbi->bmiColors)+(I)) = ((DWORD)(R)<<16)|((WORD)(G)<<8)|(UCHAR)(B)
	UCHAR       c;
	PCXPAL      pal[256];
	int         i;

	i = fs->pcx.nplanes * fs->pcx.bits;	// find bits/pixel

	if (i > 8)
		return (0);					  // Ignore palette if more than 8 bits/pixel

	else if (i == 1) {			  // B&W image

		SETRGB(0, 0, 0, 0);
		SETRGB(1, 255, 255, 255);
		return (0);
	} else if (fs->pcx.ver == 3 || fs->pcx.ver == 4) {	// no palette info

		SETRGB(0, 0, 0, 0);
		SETRGB(1, 84, 84, 255);
		SETRGB(2, 84, 255, 84);
		SETRGB(3, 84, 255, 255);
		SETRGB(4, 255, 84, 84);
		SETRGB(5, 255, 84, 255);
		SETRGB(6, 255, 255, 84);
		SETRGB(7, 255, 255, 255);
		SETRGB(8, 84, 84, 84);
		SETRGB(9, 0, 0, 168);
		SETRGB(10, 0, 168, 0);
		SETRGB(11, 0, 168, 168);
		SETRGB(12, 168, 0, 0);
		SETRGB(13, 168, 0, 168);
		SETRGB(14, 168, 168, 0);
		SETRGB(15, 168, 168, 168);
		return (0);
	} else if (fs->pcx.ver == 5 && i >= 8 && i < 24) {
		// Version 3.0 PCX
		//
		// We need to find the 256-color palette, which is located
		// 769 bytes from the end of the file. NOTE that if the pcx
		// image is imbedded in another file, we can't just search
		// from the end of the file -- we've GOT to decompress
		// the entire thing and find the palette at the end of
		// the image instead.
		//
		c = 0;
		if (fs->lpIO->seek(fs->lpHandle, fs->offset + fs->lpOffsets[fs->nPageNo] + fs->lImgLen - 769L, 0) != HFILE_ERROR) {
			fs->lpIO->read(fs->lpHandle, &c, 1);
		}
		if (c == 12) {
			fs->lpIO->read(fs->lpHandle, (LPSTR) pal, sizeof(pal));
			for (i = 256; i--;) {
				lpbi->bmiColors[i].rgbRed = pal[i].red;
				lpbi->bmiColors[i].rgbGreen = pal[i].green;
				lpbi->bmiColors[i].rgbBlue = pal[i].blue;
			}
			return (0);
		}
	}
	// Default: extract colors from embedded colormap.
	if ((i = 1 << i) > 16)
		i = 16;						  // clamp to 16... just in case

	while (i--) {
		lpbi->bmiColors[i].rgbRed = fs->pcx.colmap[i][0];
		lpbi->bmiColors[i].rgbGreen = fs->pcx.colmap[i][1];
		lpbi->bmiColors[i].rgbBlue = fs->pcx.colmap[i][2];
	}

	return (0);

#undef SETRGB
}

#ifndef NDEBUG
static void IM2PCXerr(int e)
{
	char        s1[80], s2[120];

	LoadString(pcxInstance, e, s1, sizeof(s1));
	wsprintf(s2, "IM2PCX: Error: (%d) \"%s\".\n", e, s1);
	OutputDebugString(s2);
}
#endif

#if DEBUG
void __cdecl FAR Debug(UINT wCategory, LPSTR lpFormat,...)
{
	va_list     vaArgs;

	if (DebugStringOut) {
		va_start(vaArgs, lpFormat);
		(*DebugStringOut) (wCategory, lpFormat, vaArgs);
		va_end(vaArgs);
	}
}
#else
void __cdecl FAR Debug(UINT wCategory, LPSTR lpFormat,...)
{
	UNREFERENCED_PARAMETER(wCategory);
	UNREFERENCED_PARAMETER(lpFormat);
}
#endif
