/////////////////////////////////////////////////////////////////////////////
//
// fxwipe.c
//
// ImageMan Effects Module: Wipe
// Exports:
//   doWipe
//
// Version 1.0
// Copyright (c) 1996 Data Techniques, Inc.
// Copyright (c) 1996 Chris Roueche
// All Rights Reserved
//
/////////////////////////////////////////////////////////////////////////////
// $History: fxwipe.cpp $
// 
// *****************  Version 2  *****************
// User: Johnd        Date: 4/10/98    Time: 5:06p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Chgd Error Returns to be consistent with other ImageMan Defines
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 7/10/97    Time: 2:03p
// Created in $/imageman 16-32/dll-api level 2
// Initial Lvl 2 Checkin
//

#define STRICT
#define WIN32_LEAN_AND_MEAN
#define NOSERVICE
#include <windows.h>
#include <windowsx.h>
#include "imgman.h"
#include "fxparam.h"

// Wipe-specific storage can be found in fxparam.h:tag_Wipe

/////////////////////////////////////////////////////////////////////////////
//
/////////////////////////////////////////////////////////////////////////////

static int initwipe(PEFFECTBLK peb)
{
	RECT r;

	// initialize a wipe

	peb->dwExtra &= 0xff;

	if (peb->dwExtra == 0) peb->dwExtra = 1;
	peb->wipe.bkbox = peb->wipe.bounds = peb->rDst;
	// If the image is sliding in, I can exclude the background from
	// the scroll area if OVERBKGND is specified.
	peb->wipe.prScroll = (peb->dwFlags&IMDF_WIPE_OVERBKGND) ? &peb->rDst : &peb->wipe.bounds;
	r.left = r.top = 0;
	// 'step' gets axis increment
	if (peb->nEffect <= IMFX_WIPERIGHT) { // x-axis
		r.left = peb->nStepsTotal = peb->rDst.right-peb->rDst.left;
		peb->wipe.step.cy = 0;
		peb->wipe.step.cx = (int)peb->dwExtra;
		if (peb->nEffect == IMFX_WIPELEFT) {
			peb->wipe.step.cx = -peb->wipe.step.cx;
		}
		else {
			r.left = -r.left;
		}
	}
	else { // y-axis wipe
		r.top = peb->nStepsTotal = peb->rDst.bottom-peb->rDst.top;
		peb->wipe.step.cx = 0;
		peb->wipe.step.cy = (int)peb->dwExtra;
		if (peb->nEffect == IMFX_WIPEUP) {
			peb->wipe.step.cy = -peb->wipe.step.cy;
		}
		else {
			r.top = -r.top;
		}
	}
	// offset destination area only when sliding the image in
	if (!(peb->dwFlags&IMDF_WIPE_IMGEXPOSE)) OffsetRect(&peb->rDst,r.left,r.top);

	return (IMG_OK);
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
//
// doWipe
//
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

int doWipe(HDC hDC,PEFFECTBLK peb)
{
	int x;
	RECT r;

	if (INITIALIZE(peb)) {
		if ((x = initwipe(peb)) != IMG_OK) return (x);
	}

	if (CLEANUP(peb)) return (IMG_OK);

	peb->nStepsDone += (UINT)peb->dwExtra;

	if (peb->dwFlags&IMDF_WIPE_IMGEXPOSE) {
		if (!(peb->dwFlags&IMDF_WIPE_OVERBKGND)) {
			ScrollDC(hDC,peb->wipe.step.cx,peb->wipe.step.cy,&peb->wipe.bkbox,
				&peb->wipe.bkbox,NULL,NULL);
		}
		// move background and generate update rectangle
		r = peb->wipe.bkbox;
		OffsetRect(&peb->wipe.bkbox,peb->wipe.step.cx,peb->wipe.step.cy);
		IntersectRect(&peb->wipe.bkbox,&peb->wipe.bkbox,&peb->wipe.bounds);
		SubtractRect(&r,&r,&peb->wipe.bkbox);
	} else {
		// on last step, make sure step amount locks image into final position
		if (peb->nStepsDone >= peb->nStepsTotal) {
			peb->wipe.step.cx += peb->wipe.bounds.left-(peb->rDst.left+peb->wipe.step.cx);
			peb->wipe.step.cy += peb->wipe.bounds.top-(peb->rDst.top+peb->wipe.step.cy);
		}
		OffsetRect(&peb->rDst,peb->wipe.step.cx,peb->wipe.step.cy);
		ScrollDC(hDC,peb->wipe.step.cx,peb->wipe.step.cy,peb->wipe.prScroll,
			peb->wipe.prScroll,NULL,&r);
	}
	clipblt(hDC,peb,&r);
	return (IMG_OK);
}

