// ///////////////////////////////////////////////////////////////////////////
//
// xpri.h
//
// PRI DEL Defines & Prototypes
//
// ImageMan Image Processing Library
//
// Copyright (c) 1991-5 Data Techniques, Inc.
// All Rights Reserved
//
// ///////////////////////////////////////////////////////////////////////////
// $History: XPRI.H $
// 
// *****************  Version 1  *****************
// User: Sygsky  Date: 04-OCT-2000 Time: 16:24
// Created in $/ImageMan 16-32/DELS
// ImageMan 4.0 Beta 1

#define EXP_NOOPEN		1		  /* unable to open file */
#define EXP_WRITE_HEAD	2		  /* unable to write header */
#define EXP_WRITE			3		  /* error during writing of data */

#define DLL_FILEDESCRIPTION        "ImageMan PRI Writer\0"
#define DLL_ORIGINALFILENAME       "XPRI.DEL\0"

