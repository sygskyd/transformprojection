/////////////////////////////////////////////////////////////////////////////
//
// xtif.c
//
//
// ImageMan TIFF DEL Code
//
// Copyright (c) 1991-5 Data Techniques, Inc.
// All Rights Reserved
//
// ///////////////////////////////////////////////////////////////////////////
// $History: XTIF.C $
// 
// *****************  Version 22  *****************
// User: Erict        Date: 7/26/00    Time: 3:23p
// Updated in $/ImageMan 16-32/DELS
// In get_ext removed the check of hImgManInst for returning the filter
// string. Not even sure why this compiled because there was no return if
// hImgManInst was NULL.
// 
// *****************  Version 21  *****************
// User: Erict        Date: 7/06/00    Time: 12:59p
// Updated in $/ImageMan 16-32/DELS
// fixed the begin_write to match the xfax.del so that an empty file will
// be dealt with properly by having the header info written to it first.
// 
// *****************  Version 20  *****************
// User: Ericj        Date: 7/06/00    Time: 8:06a
// Updated in $/ImageMan 16-32/DELS
// CMYK images did not correctly calculate nRowSize for images with a
// width divisible by 4.  The last fix corrected the problem for images
// that were not divisible by four.
// 
// *****************  Version 19  *****************
// User: Ericj        Date: 1/21/00    Time: 4:05p
// Updated in $/ImageMan 16-32/DELS
// Changed the tif del so it no longer needs to import the GetOptBlk
// settings from the DLL.
// 
// *****************  Version 18  *****************
// User: Ericj        Date: 12/09/99   Time: 2:28p
// Updated in $/ImageMan 16-32/DELS
// Fixed 24 Packbit images having Red and Blue swapped.
// 
// *****************  Version 17  *****************
// User: Ericw        Date: 5/17/99    Time: 1:15p
// Updated in $/ImageMan 16-32/DELS
// Fixed bug 180.  Writing certain widths of CMYK files would cause the
// image to be skewed when viewing.  nRowSize calculated incorrectly in
// the write output function.
// 
// *****************  Version 16  *****************
// User: Ericj        Date: 4/29/98    Time: 11:06a
// Updated in $/ImageMan 16-32/DELS
// Changed fname in LEXPORTINFO to MAX_PATH to export files
// longer than 100 characters.
// 
// *****************  Version 15  *****************
// User: Johnd        Date: 4/10/98    Time: 5:19p
// Updated in $/ImageMan 16-32/DELS
// Added Support for TIFF_PAGES option, Chgd resolution tags to use a
// denominator of 1 instead of 100, Fixed bug which caused a infinite loop
// when trying to append to a corrupted image.
// 
// *****************  Version 14  *****************
// User: Johnd        Date: 3/03/98    Time: 4:12p
// Updated in $/ImageMan 16-32/DELS
// Added support for TIFF_NEWSUBFILETYPE, TIFF_PAGES, TIFF_PAGENUMBER
// options and also allowed LZW compression to be used for 1 bit images.
// 
// *****************  Version 13  *****************
// User: Ericw        Date: 1/05/98    Time: 5:43p
// Updated in $/ImageMan 16-32/DELS
// Problem setting CMYK (esp. BitsPerSample) if the file already existed.
// 
// *****************  Version 12  *****************
// User: Ericw        Date: 12/15/97   Time: 5:46p
// Updated in $/ImageMan 16-32/DELS
// Supports CMYK color model writing.
// 
// *****************  Version 11  *****************
// User: Johnd        Date: 12/02/97   Time: 1:09p
// Updated in $/ImageMan 16-32/DELS
// Added code to make sure IFD is always WORD aligned per TIFF 6 spec.
// 
// *****************  Version 10  *****************
// User: Johnd        Date: 1/09/97    Time: 2:56p
// Updated in $/ImageMan 16-32/DELS
// Added support for TIFF_BYTEFORMAT = MOTOROLA for writing MM encoded
// tiff files.
// 
// *****************  Version 9  *****************
// User: Johnd        Date: 9/16/96    Time: 5:24p
// Updated in $/ImageMan 16-32/DELS
// Changed nCompress from SHORT to USHORT otherwise it wouldnt write
// PACKBITS compressed files in 32 bits.
// 
// *****************  Version 8  *****************
// User: Johnd        Date: 8/26/96    Time: 1:32p
// Updated in $/ImageMan 16-32/DELS
// Changed G4ROWS parameter to 7K from 2K to handle images with large #'s
// of runs.
// 
// *****************  Version 7  *****************
// User: Johnd        Date: 6/27/96    Time: 5:49p
// Updated in $/ImageMan 16-32/DELS
// Fixed Bug #376 (Hang when appending TIFF Images)
// 
// *****************  Version 6  *****************
// User: Johnd        Date: 4/25/96    Time: 10:03a
// Updated in $/ImageMan 16-32/DELS
// Changed default compression for color images from NONE to Packbits
// 
// *****************  Version 5  *****************
// User: Timk         Date: 2/01/96    Time: 1:26p
// Updated in $/ImageMan 16-32/DELS
// Put in #pragma to turn off optimizations under 32-bit compiler.
// Otherwise it won't compile (internal compiler error).
// 
// *****************  Version 4  *****************
// User: Timk         Date: 7/25/95    Time: 10:56p
// Updated in $/ImageMan 16-32/DELS
// Added support for writing using IO pointer blocks.
// 
// *****************  Version 3  *****************
// User: Timk         Date: 5/25/95    Time: 3:31p
// Updated in $/ImageMan 16-32/DELS
// Added USELZE define to selectively include LZW code.
// 
// *****************  Version 2  *****************
// User: Johnd        Date: 2/03/95    Time: 11:18a
// Updated in $/ImageMan 16-32/DELS
// Added support for writing Artist & Copyright tags.
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 1/18/95    Time: 6:27p
// Created in $/ImageMan 16-32/DELS
// ImageMan 4.0 Beta 1
//

#define WIN32_LEAN_AND_MEAN
#define _WINDLL
#include <windows.h>
#include <windowsx.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <assert.h>

#include "internal.h"
#include "optblk.h"
#include "tiff.h"

#ifndef WIN32
#include "windos.h"
#endif

#include "xtif.h"
#include "lzwx.h"
#include "optblk.h"

#ifdef WIN32

#ifdef WINGIS // so the file TLSByJHC.h was included to "internal.h"

#define SETERROR(E) bJHCTLSSetValue((DWORD)(E))
#define GETERROR    nJHCTLSGetValue()

#else
// Since we only need to store the error value on a per-thread basis, I'm
// just gonna place it in our DWORD TLS slot.
static DWORD TlsIndex;
#define SETERROR(E) TlsSetValue(TlsIndex,(LPVOID)(DWORD)(E))
#define GETERROR    ((DWORD)TlsGetValue(TlsIndex))
#endif
int FAR     _fatoi(char FAR * lpInt);

#else

static int  nErrID;				  // id of latest error
#define SETERROR(E) nErrID = (E)
#define GETERROR    (nErrID)

#endif

#define TAGBLKSIZE                      20
#define COMPRESSBUFSIZE 					32000
#define CLEARCODE                       256
#define EOICODE                         257

// Group4 encoding definitions
#define G4ROWSIZE                       7000
#define G4_PASS                         1
#define G4_HORZ                         2
#define G4_V0                           3
#define G4_VR1                          4
#define G4_VR2                          5
#define G4_VR3                          6
#define G4_VL1                          7
#define G4_VL2                          8
#define G4_VL3                          9

#define RTCLEN                          9               // Len in bytes of RTC @ End of FAXCCITT3 files

typedef int NEAR *NPINT;

typedef struct _EXPORTINFO {
	char        fName[MAX_PATH];		  // name of file to export
	LPVOID		lpHandle;		  // IO job handle
	LPIMG_IO		lpIO;				  // IO block
	HANDLE      hTags;			  // handle to tag mem block
	HANDLE      hCompBuf;		  // handle to 4K compression buffer
	short       nCompUsed;		  // # of bytes used in compression buf
	short       nTags;			  // # of tags there's room for
	short       nIndex;			  // next open tag slot
	short       nOutRowSize;	  // size, in bytes, of each row
	short       nStripRows;		  // # of rows in each strip
	short       nStripCur;		  // # of rows written for current strip
	long        lStripStart;	  // offset of start of current strip
	USHORT       nCompress;		  // compression type
	LPSTRINGTABLE lpTable;		  // LZW string table (if needed, of course)
	short       nCurCode;
	short       nCurPre;			  // current prefix - for LZW compression
	short       nLZWbits;		  // current # of bits in output codes
	short       nBitChange;		  // max code to allocate w/current code size
	short       nAvail;			  // # of bits available for output in output byte
	short       nRowCnt;		  // Current row in image
	NPINT       pPrev2d;		  // previous 2d row (array of integer run lengths)
	NPINT       pCur2d;			  // current 2d row (array of integer run lengths)
	short       nUsed2d;		  // # of bits used in current output byte for 2d coding
	BITMAPINFOHEADER bmi;
	DirStruct FAR *lpTags;
	long        lDirOffset;		  // offset of current directory structure.
	long        lOldOffset;
	short       bAppend;		  // 1 if appending, 0 if creating or overwriting
	short       bII;			  // 1 if intel order, 0 if motorola order
	short		iColorModel ;		// 5 = cmyk, otherwise rgb
	short		iSamplesPerPixel ;  // rgb = 3, cmyk = 4
	short       nFillOrder;
	long		nPages;				// # of Pages in existing file
}           EXPORTINFO;
typedef EXPORTINFO FAR *LPEXPORTINFO;

typedef struct _RUNINFO {
	unsigned char outp[8];
}           RUNINFO;
typedef RUNINFO NEAR *NPRUNINFO;

int         AddTag(LPEXPORTINFO lpex, int nTag, int nType, long lLen, long lValue);
int         AddTagArray(LPEXPORTINFO lpex, int nTag, int nType, long lLen, LPSTR lpData);
int         AddToTag(LPEXPORTINFO lpex, int nTag, LPSTR lpData);
int         CreateTags(LPEXPORTINFO lpex, LPBITMAPINFO lpDIB, HANDLE hOptBlk);
int         WriteTags(LPEXPORTINFO lpex);

int         PackBits(LPSTR lpOut, HPSTR lpIn, int nRowSize);
int         LZWpack(LPSTR lpOut, HPSTR lpIn, int nRowSize, LPEXPORTINFO lpex);
void PASCAL swapColors(LPSTR lpBuf, int nLen, int nBits);
void        set_error(int nIndex, LPCSTR lpFilename);
int         ClearLZW(LPEXPORTINFO lpex, LPSTR lpOut);
int         WriteCode(LPSTR lpOut, int nCode, LPEXPORTINFO lpex);
int NEAR PASCAL round(double);
int         CCITT1Dpack(LPSTR lpOut, HPSTR lpIn, LPEXPORTINFO lpex, int bEOL);
int         CCITT2Dpack(LPSTR lpOut, HPSTR lpIn, LPEXPORTINFO lpex);
void        MakeCCITTTable(void);
LPSTR       WriteCCITTCode(LPSTR lpOut, LPINT lpUsed, int nRunLen, int bBlk);
LPSTR       WriteCCITT2DCode(LPSTR lpOut, LPINT lpUsed, short nCode);
void        swap(LPSTR);
void        swapl(LPSTR);
void        swapa(LPSTR, short);
void        swapla(LPSTR, short);
void        flipBits(unsigned char far *, unsigned int);

void RGBToCMYK ( int nRows, int nRowSize, LPSTR lpBits, LPSTR lpCMYK ) ;


// Bit flip table

unsigned char fliptable[] = {
	0, 128, 64, 192, 32, 160, 96, 224, 16,
	144, 80, 208, 48, 176, 112, 240, 8,
	136, 72, 200, 40, 168, 104, 232, 24,
	152, 88, 216, 56, 184, 120, 248, 4,
	132, 68, 196, 36, 164, 100, 228, 20,
	148, 84, 212, 52, 180, 116, 244, 12,
	140, 76, 204, 44, 172, 108, 236, 28,
	156, 92, 220, 60, 188, 124, 252, 2,
	130, 66, 194, 34, 162, 98, 226, 18,
	146, 82, 210, 50, 178, 114, 242, 10,
	138, 74, 202, 42, 170, 106, 234, 26,
	154, 90, 218, 58, 186, 122, 250, 6,
	134, 70, 198, 38, 166, 102, 230, 22,
	150, 86, 214, 54, 182, 118, 246, 14,
	142, 78, 206, 46, 174, 110, 238, 30,
	158, 94, 222, 62, 190, 126, 254, 1,
	129, 65, 193, 33, 161, 97, 225, 17,
	145, 81, 209, 49, 177, 113, 241, 9,
	137, 73, 201, 41, 169, 105, 233, 25,
	153, 89, 217, 57, 185, 121, 249, 5,
	133, 69, 197, 37, 165, 101, 229, 21,
	149, 85, 213, 53, 181, 117, 245, 13,
	141, 77, 205, 45, 173, 109, 237, 29,
	157, 93, 221, 61, 189, 125, 253, 3,
	131, 67, 195, 35, 163, 99, 227, 19,
	147, 83, 211, 51, 179, 115, 243, 11,
	139, 75, 203, 43, 171, 107, 235, 27,
	155, 91, 219, 59, 187, 123, 251, 7,
	135, 71, 199, 39, 167, 103, 231, 23,
	151, 87, 215, 55, 183, 119, 247, 15,
	143, 79, 207, 47, 175, 111, 239, 31,
	159, 95, 223, 63, 191, 127, 255,
};

#define TBLENTRIES  105 // # of entries in white & black tables

unsigned short wcodes[TBLENTRIES] = {0x3500, 0x1c00, 0x7000, 0x8000,
	0xb000, 0xc000, 0xe000, 0xf000, 0x9800, 0xa000, 0x3800, 0x4000,
	0x2000, 0x0c00, 0xd000, 0xd400, 0xa800, 0xac00, 0x4e00, 0x1800,
	0x1000, 0x2e00, 0x0600, 0x0800, 0x5000, 0x5600, 0x2600, 0x4800,
	0x3000, 0x0200, 0x0300, 0x1a00, 0x1b00, 0x1200, 0x1300, 0x1400,
	0x1500, 0x1600, 0x1700, 0x2800, 0x2900, 0x2a00, 0x2b00, 0x2c00,
	0x2d00, 0x0400, 0x0500, 0x0a00, 0x0b00, 0x5200, 0x5300, 0x5400,
	0x5500, 0x2400, 0x2500, 0x5800, 0x5900, 0x5a00, 0x5b00, 0x4a00,
	0x4b00, 0x3200, 0x3300, 0x3400, 0xd800, 0x9000, 0x5c00, 0x6e00,
	0x3600, 0x3700, 0x6400, 0x6500, 0x6800, 0x6700, 0x6600, 0x6680,
	0x6900, 0x6980, 0x6a00, 0x6a80, 0x6b00, 0x6b80, 0x6c00, 0x6c80,
	0x6d00, 0x6d80, 0x4c00, 0x4c80, 0x4d00, 0x6000, 0x4d80, 0x0100,
	0x0180, 0x01a0, 0x0120, 0x0130, 0x0140, 0x0150, 0x0160, 0x0170,
0x01c0, 0x01d0, 0x01e0, 0x01f0, 0x0001};

short       wlen[TBLENTRIES] = {
	8, 6, 4, 4, 4, 4, 4, 4,
	5, 5, 5, 5, 6, 6, 6, 6,
	6, 6, 7, 7, 7, 7, 7, 7,
	7, 7, 7, 7, 7, 8, 8, 8,
	8, 8, 8, 8, 8, 8, 8, 8,
	8, 8, 8, 8, 8, 8, 8, 8,
	8, 8, 8, 8, 8, 8, 8, 8,
	8, 8, 8, 8, 8, 8, 8, 8,
	5, 5, 6, 7, 8, 8, 8, 8,
	8, 8, 9, 9, 9, 9, 9, 9,
	9, 9, 9, 9, 9, 9, 9, 9,
	9, 6, 9, 11, 11, 11, 12,
	12, 12, 12, 12, 12, 12,
12, 12, 12, 12};

unsigned short bcodes[TBLENTRIES] = {0x0dc0, 0x4000, 0xc000, 0x8000, 0x6000, 0x3000, 0x2000, 0x1800,
	0x1400, 0x1000, 0x0800, 0x0a00, 0x0e00, 0x0400, 0x0700, 0x0c00,
	0x05c0, 0x0600, 0x0200, 0x0ce0, 0x0d00, 0x0d80, 0x06e0, 0x0500,
	0x02e0, 0x0300, 0x0ca0, 0x0cb0, 0x0cc0, 0x0cd0, 0x0680, 0x0690,
	0x06a0, 0x06b0, 0x0d20, 0x0d30, 0x0d40, 0x0d50, 0x0d60, 0x0d70,
	0x06c0, 0x06d0, 0x0da0, 0x0db0, 0x0540, 0x0550, 0x0560, 0x0570,
	0x0640, 0x0650, 0x0520, 0x0530, 0x0240, 0x0370, 0x0380, 0x0270,
	0x0280, 0x0580, 0x0590, 0x02b0, 0x02c0, 0x05a0, 0x0660, 0x0670,
	0x03c0, 0x0c80, 0x0c90, 0x05b0, 0x0330, 0x0340, 0x0350, 0x0360,
	0x0368, 0x0250, 0x0258, 0x0260, 0x0268, 0x0390, 0x0398, 0x03a0,
	0x03a8, 0x03b0, 0x03b8, 0x0290, 0x0298, 0x02a0, 0x02a8, 0x02d0,
	0x02d8, 0x0320, 0x0328,
	0x0100, 0x0180, 0x01a0, 0x0120, 0x0130, 0x0140, 0x0150, 0x0160,
0x0170, 0x01c0, 0x01d0, 0x01e0, 0x01f0, 0x0001};

short       blen[TBLENTRIES] = {
	10, 3, 2, 2, 3, 4, 4, 5,
	6, 6, 7, 7, 7, 8, 8, 9,
	10, 10, 10, 11, 11, 11, 11, 11,
	11, 11, 12, 12, 12, 12, 12, 12,
	12, 12, 12, 12, 12, 12, 12, 12,
	12, 12, 12, 12, 12, 12, 12, 12,
	12, 12, 12, 12, 12, 12, 12, 12,
	12, 12, 12, 12, 12, 12, 12, 12,
	10, 12, 12, 12, 12, 12, 12, 13,
	13, 13, 13, 13, 13, 13, 13, 13,
	13, 13, 13, 13, 13, 13, 13, 13,
	13, 13, 13, 11, 11, 11, 12, 12,
12, 12, 12, 12, 12, 12, 12, 12, 12};

// Order in 2d arrays is: Pass, Horz, V0, VR1, VR2, VR3, VL1, VL2, VL3
unsigned char c2d[10] = {0x00, 0x10, 0x20, 0x80, 0x60, 0x0c, 0x06, 0x40, 0x08, 0x04};
short       len2d[10] = {0, 4, 3, 1, 3, 6, 7, 3, 6, 7};

HANDLE      hInst;
int         nOutCnt;
RUNINFO     runinfo[256];
DEBUGPROC   DebugStringOut;
XERRORPROC  XSetError;


#ifdef WIN32
// /////////////////////////////////////////////////////////////////////////
//
// DllMain
//
// This is the Win32 'DllEntryPoint'. It is similar in function to LibMain
// and the WEP in 16-bit land, but is much more thorough. LibMain is
// called whenever a process or thread attaches or detaches to the
// BMP DIL
//
// Entry:
// hinstDLL  - handle to the ImageMan DLL
// fdwReason - notification code (attach/detach)
// DLL_PROCESS_ATTACH
// DLL_PROCESS_DETACH
// DLL_THREAD_ATTACH
// DLL_THREAD_DETACH
// lpvReserved - NULL during dynamic process attach/detach, non-NULL
// during static attach/detach.
// Exit:
// DLL_PROCESS_ATTACH - TRUE for success, FALSE for failure
// all others         - return code ignored
// /////////////////////////////////////////////////////////////////////////

BOOL APIENTRY DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
	switch (fdwReason) {
			// The DLL is attaching to a process, due to LoadLibrary.
			case DLL_PROCESS_ATTACH:
			assert(!lpvReserved);  // must always be loaded via LoadLibrary!

			MakeCCITTTable();

			OutputDebugString("IM2XTIF: Process++\n");
			hInst = hinstDLL;		  // save our identity
#ifndef WINGIS
			// Allocate a "Thread Local Storage" (TLS) index right away.

			if ((TlsIndex = TlsAlloc()) == 0xFFFFFFFF)
				return (FALSE);
#else
			bJHCTLSClear();
#endif
			// Fall thru: Initialize the index for first thread.

			// The attached process created a new thread.
		case DLL_THREAD_ATTACH:
#ifndef WINGIS
			OutputDebugString("IM2XTIF: Thread++\n");
			// Initialize the TLS index for this thread... it's actually 'nErrID'
			if (!TlsSetValue(TlsIndex, (LPVOID) 0))
				return (FALSE);
#else
			nJHCTLSAlloc(); // Stamp this thread to be existed
#endif
			break;

			// The thread of the attached process terminates.
		case DLL_THREAD_DETACH:
#ifdef WINGIS
			bJHCTLSFree(); // Free this thread entry
#else
			OutputDebugString("IM2XTIF: Thread--\n");
#endif
			break;

			// The DLL is detaching from a process, due to a call to FreeLibrary.
		case DLL_PROCESS_DETACH:
#ifndef WINGIS
			OutputDebugString("IM2XTIF: Process--\n");
			// Release the TLS index.
			TlsFree(TlsIndex);
#else
			bJHCTLSClear();  // Release all the threads error descriptors
#endif
			break;
	}
	OutputDebugString("IM2XTIF: OK\n");

	return (TRUE);

	UNREFERENCED_PARAMETER(lpvReserved);
}

#else // WIN32

int FAR PASCAL LibMain(hInstance, wDataSeg, wHeapSize, lpCmdLine)
	HANDLE      hInstance;
	WORD        wDataSeg, wHeapSize;
	LPSTR       lpCmdLine;
{
	if (!wHeapSize)				  /* no heap indicates fatal error */
		return 0;

	hInst = hInstance;			  // save instance handle

	MakeCCITTTable();

#if DEBUG
	{
		HANDLE      hMod = GetModuleHandle("IMGMAN2.DLL");
		if (hMod)
			DebugStringOut = (DEBUGPROC) GetProcAddress(hMod, "DebugStringOut");
	}
#endif

	return 1;
}
#endif

int WINAPI  WEP(int bSystemExit)
{
	if (bSystemExit) {			  // if exiting Windows entirely

	}
	return 0;
}

LPSTR IMAPI IMXDLget_ext(LPXINITSTRUCT lpInit)
{
	lpInit->lFlags = XF_ALLCOLORS | XF_MULTIPAGE;
	XSetError = lpInit->SetError;


	return (LPSTR) "TIF;Tag Image File Format (*.tif)";
}

LPSTR IMAPI IMXDLget_ver(void)
{
	return (LPSTR) "1.00";
}

#ifdef WIN32
#pragma optimize ("",off)
#endif

HANDLE IMAPI IMXDLbegin_write(LPVOID lpOpenInfo, LPIMG_IO lpIO, LPCSTR lpszAlias, LPBITMAPINFO lpDIB, HANDLE hOptBlk, LPXINFO lpInfo)
{
	TiffHead    tHead;
	LPVOID		lpHandle;
	HANDLE      hImage = NULL;
	LPEXPORTINFO lpex;
	long        lDirOffset, lOldOffset;
	int         bAppend, bII = 1, nErrVal, iColorModel = 0, iSamples ;
	short		nDirCnt;
	char		optbuf[80];
	long		nPages = 0;
	BOOL		bFileOK = 1;

	bAppend = 0;

	if ((nErrVal = lpIO->open(lpOpenInfo, OF_EXIST, &lpHandle)) == IMG_OK) 
	{
		bAppend = 1;
		nErrVal = lpIO->open(lpOpenInfo, OF_READWRITE, &lpHandle);
		//
		// find the last IFD entry and store it's location away for future use...
		//

		if( lpIO->read(lpHandle, &tHead, sizeof(TiffHead)) != sizeof(TiffHead) )
			bFileOK = 0;

		if (tHead.order == MM)
				bII = 0;					  // YUCK! this is gnarly, but we gotta do it.

			lDirOffset = tHead.ifd0;
		while (bFileOK && lDirOffset) {
			if (!bII)
				swapl((LPSTR) & lDirOffset);

			if( lpIO->seek(lpHandle, lDirOffset, 0) != lDirOffset )		// go to the IFD
				bFileOK = 0;	

			if( lpIO->read(lpHandle, &nDirCnt, sizeof(short)) != sizeof(short) )
				bFileOK = 0;

			if (!bII)
				swap((LPSTR) & nDirCnt);

			lpIO->seek(lpHandle, nDirCnt * sizeof(DirStruct), 1);	// skip over directory entries

			lOldOffset = lpIO->seek(lpHandle, 0, 1);

			if( lOldOffset != lDirOffset + (nDirCnt * sizeof(DirStruct)) + sizeof(short) )
				bFileOK = 0;

			if (lpIO->read(lpHandle, &lDirOffset, sizeof(LONG)) != sizeof(LONG))
				bFileOK = 0;
			
			nPages++;
		}

		lpIO->seek(lpHandle, 0, 2);	  // seek to end of file

		if( !bFileOK )
			lpIO->close( lpHandle );
	}

	if( !bAppend || !bFileOK ) {
		// If we want Moto Format then set bII to 0
		if (!(OptBlkGet (hOptBlk, "TIFF_BYTEFORMAT", (LPSTR) optbuf)) || !_fstrcmp((LPSTR) optbuf, "MOTOROLA"))
			bII = 0;

		bAppend = 0;
		nErrVal = lpIO->open(lpOpenInfo, OF_CREATE, &lpHandle);
		lOldOffset = 0L;
	}

	//
	// at this point, file should be opened and positioned to the proper
	// spot to begin writing our file...
	//
	if (nErrVal == IMG_OK) {

		lDirOffset = lpIO->seek(lpHandle, 0, 1);	// get offset to put our dirstruct at

		// now create and initialize our lovely data structure
		hImage = GlobalAlloc(DLLHND, sizeof(EXPORTINFO));
		if (hImage) {
			lpex = (LPEXPORTINFO) GlobalLock(hImage);

			if (!bAppend) {
				tHead.ver = 42;
				
				if( bII )
					tHead.order = 'I' * 256 + 'I';
				else {
					tHead.order = 'M' * 256 + 'M';
					swapa( (LPSTR)&tHead.ver, 1 );
				}
				tHead.ifd0 = 0L;	  // we'll set this later...

				if (lpIO->write(lpHandle, (LPSTR) & tHead, sizeof(TiffHead)) != sizeof(TiffHead)) {
				}
			}
			// check for cmyk color model
			if (!(OptBlkGet(hOptBlk, "TIFF_COLORMODEL", (LPSTR) optbuf)) || !_fstrcmp((LPSTR) optbuf, "CMYK"))
				{
					iColorModel = PHOTOINT_CMYK ;
					iSamples = 4 ;
				}
			else
				iSamples = 3;

			lpex->lpIO = lpIO;
			lpex->lpHandle = lpHandle;
			_fstrcpy(lpex->fName, lpszAlias);
			lpex->nOutRowSize = (int) ((lpDIB->bmiHeader.biWidth * lpDIB->bmiHeader.biBitCount + 7) / 8);
			lpex->nIndex = 0;
			lpex->nTags = TAGBLKSIZE;
			lpex->hTags = GlobalAlloc(DLLHND, sizeof(DirStruct) * TAGBLKSIZE);
			lpex->lpTags = (DirStruct FAR *) GlobalLock(lpex->hTags);
			lpex->hCompBuf = GlobalAlloc(DLLHND, COMPRESSBUFSIZE);
			lpex->nCompUsed = 0;
			lpex->nRowCnt = 0;
			lpex->bmi = lpDIB->bmiHeader;
			lpex->lStripStart = 0L;
			lpex->lDirOffset = lDirOffset;
			lpex->lOldOffset = lOldOffset;
			lpex->bAppend = bAppend;
			lpex->bII = bII;
			lpex->iColorModel = iColorModel ;
			lpex->iSamplesPerPixel = iSamples ;
			lpex->nPages = nPages;
			if ( iColorModel == PHOTOINT_CMYK )		// adjust the row size up
				lpex->nOutRowSize += ( lpex->nOutRowSize / 3 ) ;

			// now fill in the tags
			CreateTags(lpex, lpDIB, hOptBlk);

			//
			// fill in LZW stuff if doing LZW decompression
			//
			if (lpex->nCompress == LZW) {
				lpex->nCurPre = -1;
				lpex->nLZWbits = 9;
				lpex->nBitChange = 512;
				lpex->nAvail = 8;
				lpex->lpTable = NULL;
			} else if (lpex->nCompress == (int) FAXCCITT4) {
				lpex->pPrev2d = (NPINT) LocalLock(LocalAlloc(LHND, sizeof(int) * G4ROWSIZE));
				lpex->pCur2d = (NPINT) LocalLock(LocalAlloc(LHND, sizeof(int) * G4ROWSIZE));
				// init the 1st imaginary run of white bits
				*(lpex->pPrev2d) = (int) (lpDIB->bmiHeader.biWidth);
				*(lpex->pPrev2d + 1) = (int) (lpDIB->bmiHeader.biWidth);
				lpex->nUsed2d = 0;
			}
			GlobalUnlock(lpex->hTags);
			GlobalUnlock(hImage);
		} else {
				MessageBox(NULL, (LPSTR)lpOpenInfo, "no hImage mem", MB_OK);
			lpIO->close(lpHandle);
		}
	} else {
		MessageBox(NULL, (LPSTR)lpOpenInfo, "Cant Open", MB_OK);
		hImage = NULL;
		set_error(EXP_NOOPEN, lpszAlias);
	}

	lpInfo->lFlags = XF_ALLCOLORS | XF_MULTIPAGE;	// yes, we have multipage!

	return hImage;
}
#ifdef WIN32
#pragma optimize ("",on)
#endif

int IMAPI   IMXDLwrite_block(HANDLE hImage, int nRows, int nRowSize, LPSTR lpBits, LPXINFO lpInfo)
{
	LPEXPORTINFO lpex;
	long        lOffset, lTmp;
	HPSTR       hp;
	LPSTR       lpOut;
	LPSTR				lpCMYK = NULL ;
	LPSTR       lp24Tmp;

	lpex = (LPEXPORTINFO) GlobalLock(hImage);

	if ( lpex->iColorModel == PHOTOINT_CMYK )
		{
			lpCMYK = (LPSTR) GlobalAllocPtr ( GHND, (lpex->bmi.biWidth+1) * lpex->iSamplesPerPixel * nRows ) ;
			RGBToCMYK ( nRows, nRowSize, lpBits, lpCMYK ) ;

			if (lpex->bmi.biWidth % 4)
				nRowSize = (lpex->bmi.biWidth + 1) * lpex->iSamplesPerPixel ;
			else
				nRowSize = lpex->bmi.biWidth * lpex->iSamplesPerPixel ;

			lpBits = lpCMYK ;
		}
	//
	// Since we're receiving a DIB block, we need to start at the last
	// row in the block and work our way backwards...

	hp = (HPSTR) lpBits + (long) (nRows - 1) * nRowSize;
	lpOut = (LPSTR) GlobalLock(lpex->hCompBuf);

	if (lpex->bmi.biBitCount == 24) {
		lp24Tmp = GlobalAllocPtr(GHND, (lpex->bmi.biWidth + 1) * lpex->iSamplesPerPixel);
	} else
		lp24Tmp = NULL;

	while (nRows--) {

		//
		// Flush our compression buffer to disk if needed.
		// We do this if we're starting a new strip or if the buffer won't
		// hold 4 lines of umcompressed image information - 4 lines is used
		// because a worst-case image might cause us to compress to slightly
		// larger than we started, so we need a little extra buffer.
		//
		if (!lpex->nStripCur || (lpex->nOutRowSize * 6 > (COMPRESSBUFSIZE - lpex->nCompUsed))) {
			if (lpex->nCompUsed) {
				if (!lpex->nStripCur && lpex->nCompress == LZW) {
					lpex->nCompUsed += WriteCode(lpOut + lpex->nCompUsed, lpex->nCurPre, lpex);
					lpex->nCompUsed += WriteCode(lpOut + lpex->nCompUsed, EOICODE, lpex);
					lpex->nAvail = 8;
					lpex->nCompUsed++;	// make sure last byte is written...

				} else if (!lpex->nStripCur && lpex->nCompress == FAXCCITT4) {
					// init the 1st imaginary run of white bits
					*(lpex->pPrev2d) = (int) (lpex->bmi.biWidth);
					*(lpex->pPrev2d + 1) = (int) (lpex->bmi.biWidth);
					lpex->nUsed2d = 0;
					lpex->nCompUsed++;	// make sure last byte is written...

				}
				if (lpex->nFillOrder == 2)
					flipBits(lpOut, lpex->nCompUsed);

				lpex->lpIO->write(lpex->lpHandle, lpOut, lpex->nCompUsed);

				// if the strip size is larger than the output buffer size,
				// we may need to preserve the last byte
				if (lpex->nCompress != NOCOMPRESSBYTE && lpex->nStripCur) {
					lpOut[0] = lpOut[lpex->nCompUsed];
					_fmemset(lpOut + 1, 0x00, lpex->nCompUsed);
				} else {
					_fmemset(lpOut, 0x00, lpex->nCompUsed);
				}
				lpex->nCompUsed = 0;
			}
		}
		//
		// Write out strip offset if at start of new strip.
		// Also, update StripByteCount array with size of last strip.
		//
		if (!lpex->nStripCur) {	  // end of a strip of stuff...

			lOffset = lpex->lpIO->seek(lpex->lpHandle, 0, 1);	// get current file offset

			AddToTag(lpex, STRIPSOFFSETS, (LPSTR) & lOffset);
			lpex->nStripCur = lpex->nStripRows;
			if (lpex->lStripStart) {
				lTmp = lOffset - lpex->lStripStart;
				AddToTag(lpex, STRIPBYTECOUNTS, (LPSTR) & lTmp);
			}
			lpex->lStripStart = lOffset;
			if (lpex->nCompress == LZW) {
				lpex->nLZWbits = 9;
				lpex->nCompUsed += ClearLZW(lpex, lpOut + lpex->nCompUsed);
			}
		}
		//
		// deal with our progress indicator...
		//
		lpInfo->lIncCnt += nRowSize;
		if (lpInfo->lIncCnt > lpInfo->lIncrement) {
			lpInfo->lProcessed += lpInfo->lIncCnt;
			if (lpInfo->lpfnStat)
				if (!(*lpInfo->lpfnStat) (0, (int) ((lpInfo->lProcessed * 100) / lpInfo->lTotalBytes), lpInfo->dwUserInfo)) {
					if (lp24Tmp)
						GlobalFreePtr(lp24Tmp);

					GlobalUnlock(lpex->hCompBuf);
					GlobalUnlock(hImage);
					if ( lpCMYK )
						GlobalFreePtr ( lpCMYK ) ; 
					return IMGX_CANCELLED;
				}
			lpInfo->lIncCnt = 0;
		}
		//
		// Now output the row in the appropriate compression format
		//
		switch (lpex->nCompress) {
			case NOCOMPRESSBYTE:
				if (lpex->bmi.biBitCount == 24) {
					_fmemmove(lpOut + lpex->nCompUsed, (LPSTR) hp, lpex->nOutRowSize);
					if ( lpex->iColorModel != PHOTOINT_CMYK )
						swapColors(lpOut + lpex->nCompUsed, lpex->nOutRowSize, 24);
				} else {
					_fmemmove(lpOut + lpex->nCompUsed, (LPSTR) hp, lpex->nOutRowSize);
				}

				lpex->nCompUsed += lpex->nOutRowSize;
				break;

			case PACKBITS:
				if (lpex->bmi.biBitCount == 24) {
					_fmemmove(lp24Tmp, (LPSTR) hp, lpex->nOutRowSize);
					if ( lpex->iColorModel != PHOTOINT_CMYK )
						swapColors(lp24Tmp, lpex->nOutRowSize, 24);
					lpex->nCompUsed += PackBits(lpOut + lpex->nCompUsed, lp24Tmp, lpex->nOutRowSize);
				} 
				else
					lpex->nCompUsed += PackBits(lpOut + lpex->nCompUsed, hp, lpex->nOutRowSize);
				break;

			case LZW:
				if (lpex->bmi.biBitCount == 24) {
					_fmemmove(lp24Tmp, (LPSTR) hp, lpex->nOutRowSize);
					if ( lpex->iColorModel != PHOTOINT_CMYK )
						swapColors(lp24Tmp, lpex->nOutRowSize, 24);
					lpex->nCompUsed += LZWpack(lpOut + lpex->nCompUsed, lp24Tmp, lpex->nOutRowSize, lpex);
				} else {
					lpex->nCompUsed += LZWpack(lpOut + lpex->nCompUsed, hp, lpex->nOutRowSize, lpex);
				}

				break;

			case CCITT3:
				lpex->nCompUsed += CCITT1Dpack(lpOut + lpex->nCompUsed, hp, lpex, FALSE);
				break;

			case FAXCCITT3:
				lpex->nCompUsed += CCITT1Dpack(lpOut + lpex->nCompUsed, hp, lpex, TRUE);
				break;

			case FAXCCITT4:
				lpex->nCompUsed += CCITT2Dpack(lpOut + lpex->nCompUsed, hp, lpex);
				break;
		}

		hp -= nRowSize;
		lpex->nStripCur--;
		lpex->nRowCnt++;
	}

	if (lp24Tmp)
		GlobalFreePtr(lp24Tmp);

	if ( lpCMYK )
		GlobalFreePtr ( lpCMYK ) ;

	GlobalUnlock(lpex->hCompBuf);
	GlobalUnlock(hImage);
	return IMGX_OK;
}

int IMAPI   IMXDLend_write(HANDLE hImage, LPXINFO lpInfo)
{
	LPEXPORTINFO lpex;
	long        lOffset;
	LPSTR       lpOut;

	lpex = (LPEXPORTINFO) GlobalLock(hImage);

	//
	// Output any straggling data in the compression buffer...
	//
	if (lpex->nCompUsed || lpex->nCompress == LZW || lpex->nCompress == FAXCCITT4
		 || lpex->nCompress == FAXCCITT3) {
		lpOut = (LPSTR) GlobalLock(lpex->hCompBuf);
		if (lpex->nCompress == LZW) {
			lpex->nCompUsed += WriteCode(lpOut + lpex->nCompUsed, lpex->nCurPre, lpex);
			lpex->nCompUsed += WriteCode(lpOut + lpex->nCompUsed, EOICODE, lpex);
			lpex->nCompUsed++;	  // make sure the last byte is written...

		} else if (lpex->nCompress == FAXCCITT3) {
			LPSTR       lpBytes;

			if (lpex->nCompUsed + RTCLEN > COMPRESSBUFSIZE) {
				if (lpex->nFillOrder == 2)
					flipBits(lpOut, lpex->nCompUsed);

				lpex->lpIO->write(lpex->lpHandle, lpOut, lpex->nCompUsed);
				lpex->nCompUsed = 0;
			}
			lpBytes = lpOut + lpex->nCompUsed;

			*lpBytes++ = 0x00;
			*lpBytes++ = 0x10;
			*lpBytes++ = 0x01;

			*lpBytes++ = 0x00;
			*lpBytes++ = 0x10;
			*lpBytes++ = 0x01;

			*lpBytes++ = 0x00;
			*lpBytes++ = 0x10;
			*lpBytes++ = 0x01;

			lpex->nCompUsed += RTCLEN;
		} else if (lpex->nCompress == FAXCCITT4) {
			lpex->nCompUsed++;	  // make sure the last byte is written...

		}
		if (lpex->nFillOrder == 2)
			flipBits(lpOut, lpex->nCompUsed);

		lpex->lpIO->write(lpex->lpHandle, lpOut, lpex->nCompUsed);
		lpex->nCompUsed = 0;
		GlobalUnlock(lpex->hCompBuf);
	}
	//
	// Now update the strip byte counts for our final strip...
	//
	lOffset = lpex->lpIO->seek(lpex->lpHandle, 0, 1);	// get current file offset

	lOffset -= lpex->lStripStart;
	AddToTag(lpex, STRIPBYTECOUNTS, (LPSTR) & lOffset);

	//
	// Now write out all our neato tags...
	//
	lpex->lpTags = (DirStruct FAR *) GlobalLock(lpex->hTags);
	WriteTags(lpex);
	GlobalUnlock(lpex->hTags);

	//
	// Finally, clean up our mess...
	//
	lpInfo->lWritten = lpex->lpIO->seek(lpex->lpHandle, 0, SEEK_END);
	lpex->lpIO->close(lpex->lpHandle);

	// clean up LZW decompression buffers
	if (lpex->nCompress == LZW) {
		if (lpex->lpTable)
			GlobalFreePtr(lpex->lpTable);
	} else if (lpex->nCompress == FAXCCITT4) {

		LocalFree( LocalHandle( lpex->pPrev2d ) );

		LocalFree( LocalHandle( lpex->pCur2d ) );
	}
	GlobalFree(lpex->hCompBuf);
	GlobalFree(lpex->hTags);

	GlobalUnlock(hImage);
	GlobalFree(hImage);
	return IMGX_OK;
}

// //////////////////////////////////////////////////////////////////////////
//
// Helper Functions
//
// //////////////////////////////////////////////////////////////////////////
void        set_error(int nIndex, LPCSTR lpFilename)
{
	XSETERROR(hInst, nIndex, "IMGXTIF.DEL", lpFilename);
}

int         AddTag(LPEXPORTINFO lpex, int nTag, int nType, long lLen, long lValue)
{
	DirStruct FAR *lpTag;

	lpTag = &(lpex->lpTags[lpex->nIndex++]);
	lpTag->tag = nTag;
	lpTag->type = nType;
	lpTag->len = lLen;

	switch (nType) {
		case IFDBYTE:
			lpTag->value.ival.i1 = (short) lValue;
			break;
		case IFDSHORT:
			lpTag->value.ival.i1 = (short) lValue;
			break;
		case IFDLONG:
			lpTag->value.lval = lValue;
			break;
		case IFDRATIONAL:
			break;
		default:
			break;
	}

	return 0;
}

int         AddTagArray(LPEXPORTINFO lpex, int nTag, int nType, long lLen, LPSTR lpData)
{
	DirStruct FAR *lpTag;
	HANDLE      hData;
	int         nSize;
	LPSTR       lpBuf;

	lpTag = &(lpex->lpTags[lpex->nIndex++]);
	lpTag->tag = nTag;			  // make it negative so we can spot it later...

	lpTag->type = -nType;

	switch (nType) {
		case IFDBYTE:
		case IFDASCII:
			nSize = IFDBYTESIZE;
			break;
		case IFDSHORT:
			nSize = IFDSHORTSIZE;
			break;
		case IFDLONG:
			nSize = IFDLONGSIZE;
			break;
		case IFDRATIONAL:
			nSize = IFDRATIONALSIZE;
			break;
		default:
			break;
	}

	hData = GlobalAlloc(DLLHND, lLen * nSize);
	lpTag->value.lval = (long) hData;

	if (lpData) {
		lpTag->len = lLen;
		lpBuf = (LPSTR) GlobalLock(hData);
		_fmemmove(lpBuf, lpData, (int) lLen * nSize);
		GlobalUnlock(hData);
	} else {
		lpTag->len = 0;			  // we'll be adding the data later...

	}

	return 0;
}

int         AddToTag(LPEXPORTINFO lpex, int nTag, LPSTR lpData)
{
	int         i, nSize;
	LPSTR       lpTagData;

	for (i = 0; i < lpex->nIndex && nTag != abs(lpex->lpTags[i].tag); i++)
		;

	if (lpex->lpTags[i].tag == nTag && lpex->lpTags[i].type < 0 ) {	// the tag exists, and is an array

		lpTagData = (LPSTR) GlobalLock((HANDLE) (lpex->lpTags[i].value.lval));

		switch (-lpex->lpTags[i].type) {
			case IFDBYTE:
			case IFDASCII:
				nSize = IFDBYTESIZE;
				break;
			case IFDSHORT:
				nSize = IFDSHORTSIZE;
				break;
			case IFDLONG:
				nSize = IFDLONGSIZE;
				break;
			case IFDRATIONAL:
				nSize = IFDRATIONALSIZE;
				break;
			default:
				break;
		}

		lpTagData += (int) (nSize * lpex->lpTags[i].len);
		_fmemmove(lpTagData, lpData, nSize);
		GlobalUnlock((HANDLE) (lpex->lpTags[i].value.lval));
		lpex->lpTags[i].len++;
	} else if (lpex->lpTags[i].tag == nTag) {	// it exists, and is a single val

		switch (lpex->lpTags[i].type) {
			case IFDBYTE:
			case IFDASCII:
				lpex->lpTags[i].value.ival.i1 = (int) (*lpData);
				break;
			case IFDSHORT:
				lpex->lpTags[i].value.ival.i1 = *((LPINT) lpData);
				break;
			case IFDLONG:
				lpex->lpTags[i].value.lval = *((LPLONG) lpData);
				break;
			default:
				break;
		}
	}
	return 0;
}

//
// This function writes out the tags and their associated information
// at the current location in the file (lpHandle). The pointer to the
// directory information is then updated in the file header.
//
int         WriteTags(LPEXPORTINFO lpex)
{
	long        lCurOffset, lJunk;
	short       i, nSize;
	LPSTR       lpData;
	HANDLE      hData;

	//
	// The first thing to do is write out any arrays of data that we'll
	// be including in the file; for instance, the StripOffsets array or
	// the name of the program that created the image.
	//
	for (i = 0; i < lpex->nIndex; i++) {
		if (lpex->lpTags[i].type < 0) {
			// lpex->lpTags[i].tag *= -1;	// reverse the sign to normal

			lpex->lpTags[i].type *= -1;

			switch (lpex->lpTags[i].type) {
				case IFDBYTE:
				case IFDASCII:
					nSize = IFDBYTESIZE;
					break;
				case IFDSHORT:
					nSize = IFDSHORTSIZE;
					break;
				case IFDLONG:
					nSize = IFDLONGSIZE;
					break;
				case IFDRATIONAL:
					nSize = IFDRATIONALSIZE;
					break;
				default:
					break;
			}

			//
			// if the tag[i].len * nSize <= sizeof(long), it will all fit
			// in the IFD entry itself, and does not require an extra
			// array...
			//
			hData = (HANDLE) lpex->lpTags[i].value.lval;
			lpData = (LPSTR) GlobalLock(hData);
			if (lpex->lpTags[i].len * nSize > sizeof(long)) {
				// swap the items if need be
				if (!lpex->bII) {
					switch (nSize) {
						case IFDSHORT:
							swapa(lpData, (short) lpex->lpTags[i].len);
							break;
						case IFDLONG:
							swapla(lpData, (short) lpex->lpTags[i].len);
							break;
						case IFDRATIONAL:
							swapla(lpData, (short) lpex->lpTags[i].len * 2);
							break;
					}
				}
				lCurOffset = lpex->lpIO->seek(lpex->lpHandle, 0L, 1);	// get current location in file

				lpex->lpIO->write(lpex->lpHandle, lpData, (int) (lpex->lpTags[i].len) * nSize);
				lpex->lpTags[i].value.lval = lCurOffset;	// store the offset

			} else {					  // it all fits in the IFD itself

				lpex->lpTags[i].value.lval = *((LPLONG) lpData);
			}
			GlobalUnlock(hData);
			GlobalFree(hData);
		}
	}


	lCurOffset = lpex->lpIO->seek(lpex->lpHandle, 0L, 1);	// get current location in file

	// Make sure we are word aligned per tiff 6 spec
	if( lCurOffset & 1 ) {
		// if we arent then we write 1 trash byte & increment the offset
		lpex->lpIO->write(lpex->lpHandle, (LPSTR) &i, 1);
		lCurOffset++;
	}

	//
	// write the # of tags at the front of tag array
	//
	i = lpex->nIndex;
	if (!lpex->bII)
		swap((LPSTR) (&i));
	lpex->lpIO->write(lpex->lpHandle, (LPSTR) & i, sizeof(short));

	for (i = 0; i < lpex->nIndex; i++) {
		if (!lpex->bII) {
			if (lpex->lpTags[i].type == IFDSHORT && lpex->lpTags[i].len == 1) {
				swap((LPSTR) & lpex->lpTags[i].value.ival.i1);
			} else {
				swapl((LPSTR) & lpex->lpTags[i].value.lval);
			}
			swap((LPSTR) & lpex->lpTags[i].tag);
			swap((LPSTR) & lpex->lpTags[i].type);
			swapl((LPSTR) & lpex->lpTags[i].len);
		}
		lpex->lpIO->write(lpex->lpHandle, (LPSTR) (&(lpex->lpTags[i])), sizeof(DirStruct));
	}

	//
	// Now write the trailing 0
	lJunk = 0L;
	lpex->lpIO->write(lpex->lpHandle, (LPSTR) & lJunk, sizeof(long));

	//
	// Now update the IFD pointer chain
	//
	if (!lpex->bII)
		swapl((LPSTR) & lCurOffset);
	if (lpex->bAppend) {
		lpex->lpIO->seek(lpex->lpHandle, lpex->lOldOffset, 0);
		lpex->lpIO->write(lpex->lpHandle, (LPSTR) & lCurOffset, sizeof(long));
	} else {
		lpex->lpIO->seek(lpex->lpHandle, lpex->lDirOffset + 4, 0);
		lpex->lpIO->write(lpex->lpHandle, (LPSTR) & lCurOffset, sizeof(long));
	}

	return 0;
}

int         CreateTags(LPEXPORTINFO lpex, LPBITMAPINFO lpDIB, HANDLE hOptBlk)
{
	int         nStrips, nPhoto, nColors, i;
	char        optbuf[100];
	struct Rational resolution;
	short       palbuf[256 * 3];  // space for the largest possible palette
	int			nVal;

	//
	// REMEMBER -- The tags must be added in sequential order by tag
	// ordinal.
	//


	nVal = 0;	// NEWSUBFILETYPES defaults to zero

	if (!OptBlkGet(hOptBlk, "TIFF_NEWSUBFILETYPE", (LPSTR) optbuf))
		nVal = _fatoi(optbuf);
		

	AddTag(lpex, NEWSUBFILETYPE, IFDLONG, 1, nVal);

	AddTag(lpex, IMAGEWIDTH, IFDSHORT, 1, (int) lpDIB->bmiHeader.biWidth);
	AddTag(lpex, IMAGELENGTH, IFDSHORT, 1, (int) lpDIB->bmiHeader.biHeight);

	//
	// handle bits per sample -- if it's a 24-bit image, we have to
	// create an array of 3 entries, each w/8 bits/sample
	//
	if (lpDIB->bmiHeader.biBitCount < 24)
		AddTag(lpex, BITSPERSAMPLE, IFDSHORT, 1, lpDIB->bmiHeader.biBitCount);
	else {
		short junk[4] = { 8,8,8,8 };
		// note that samples per pixel will only write 3 if that's all it needs
		AddTagArray(lpex, BITSPERSAMPLE, IFDSHORT, lpex->iSamplesPerPixel, (LPSTR) junk);
	}

	//
	// Figger which compression type to use
	// We have 2 OptBlk entries which affect our compression choice:
	// COMPRESSION, which can be on or off
	// TIFF_COMPRESS, which can be PACKBITS, LZW, MODCCITT, GROUP3, or GROUP4
	//
	// Note that the only compression type which can be used for color images
	// is LZW. Therefore, if COMPRESS is ON, all color image will be
	// compressed using LZW encoding. The TIFF_COMPRESS option specifies the
	// compression technique to use when writing monochrome tiff images.
	//
	if (OptBlkGet(hOptBlk, "COMPRESS", (LPSTR) optbuf) || _fstrcmp((LPSTR) optbuf, "ON")) {
		lpex->nCompress = NOCOMPRESSBYTE;
	} else {							  // compression is ON

		if (lpDIB->bmiHeader.biBitCount == 1) {
			if (!OptBlkGet(hOptBlk, "TIFF_COMPRESS", (LPSTR) optbuf)) {
				if (!_fstrcmp(optbuf, "MODCCITT"))
					lpex->nCompress = CCITT3;
				else if (!_fstrcmp(optbuf, "PACKBITS"))
					lpex->nCompress = PACKBITS;
				else if (!_fstrcmp(optbuf, "GROUP3"))
					lpex->nCompress = FAXCCITT3;
				else if (!_fstrcmp(optbuf, "GROUP4"))
					lpex->nCompress = FAXCCITT4;
#ifdef USELZW
				else if (!_fstrcmp(optbuf, "LZW"))
					lpex->nCompress = LZW;
#endif
				else
					lpex->nCompress = PACKBITS;
			} else					  // default to PACKBITS as a good old-fashioned RLE method.

				lpex->nCompress = PACKBITS;

		} else
#ifdef USELZW
			lpex->nCompress = LZW;
#else
			lpex->nCompress = PACKBITS;
#endif
	}
	AddTag(lpex, COMPRESSION, IFDSHORT, 1, (long) (lpex->nCompress));

	//
	// Determine the correct Photometric Interpretation based on the
	// info in lpDIB.
	//
	switch (lpDIB->bmiHeader.biBitCount) {
		case 1:
			nPhoto = nColors = 1;
			break;
		case 4:
			nPhoto = 3;				  // Palette Color

			nColors = 16;
			break;
		case 8:
			nPhoto = 3;				  // Palette Color

			nColors = 256;
			break;
		case 24:
			nPhoto = ( lpex->iColorModel == PHOTOINT_CMYK ) ? 5 : 2 ;
			nColors = -1;
			break;
		default:
			break;
	}

	if (lpex->nCompress == FAXCCITT3 || lpex->nCompress == FAXCCITT4)
		nPhoto = 0;

	AddTag(lpex, PHOTOMETRICINTERPRETATION, IFDSHORT, 1, (long) nPhoto);

	// If TIFF__FillOrder = 2 Then set FillOrder flag
	if (!OptBlkGet(hOptBlk, "TIFF_FILLORDER", (LPSTR) optbuf) && nColors == 1) {
		AddTag(lpex, FILLORDER, IFDSHORT, 1, (long) 2);
		lpex->nFillOrder = 2;
	} else
		lpex->nFillOrder = 1;

	//
	// calculate RowsPerStrip before determining # of strip offsets
	//

	if (OptBlkGet(hOptBlk, "TIFF_ROWSPERSTRIP", (LPSTR) optbuf)) {
		//
		// try to create strips at about 8k as a default, but don't allow
		// more than 200 strips (the 200 is arbitrary, BTW)...
		//
		nStrips = min(200, (int) ((lpex->nOutRowSize * lpDIB->bmiHeader.biHeight + 8095) / 8096));

		lpex->nStripRows = (int) (lpDIB->bmiHeader.biHeight + nStrips - 1) / nStrips;

		if (nStrips == 1)
			AddTag(lpex, STRIPSOFFSETS, IFDLONG, 1, 0L);	// this'll get filled later

		else
			AddTagArray(lpex, STRIPSOFFSETS, IFDLONG, nStrips, NULL);
	} else {
		lpex->nStripRows = _fatoi(optbuf);

		nStrips = (int) (lpDIB->bmiHeader.biHeight + lpex->nStripRows - 1) / lpex->nStripRows;
		if (!nStrips)
			nStrips = 1;			  // at least 1 strip!

		if (nStrips == 1) {
			AddTag(lpex, STRIPSOFFSETS, IFDLONG, 1, 0L);	// this'll get filled later

		} else {
			AddTagArray(lpex, STRIPSOFFSETS, IFDLONG, nStrips, NULL);
		}
	}
	lpex->nStripCur = 0;

	if (lpDIB->bmiHeader.biBitCount == 24) {
		AddTag(lpex, SAMPLESPERPIXEL, IFDSHORT, 1, (long)lpex->iSamplesPerPixel );
	} else {
		AddTag(lpex, SAMPLESPERPIXEL, IFDSHORT, 1, 1L);
	}

	AddTag(lpex, ROWSPERSTRIP, IFDSHORT, 1, lpex->nStripRows);

	if (nStrips == 1)
		AddTag(lpex, STRIPBYTECOUNTS, IFDLONG, 1, 0L);
	else
		AddTagArray(lpex, STRIPBYTECOUNTS, IFDLONG, nStrips, NULL);

	resolution.denominator = 1;

	if (!OptBlkGet(hOptBlk, "TIFF_XRES", (LPSTR) optbuf))
		resolution.numerator = (long) _fatoi((LPSTR) optbuf);
	else
		resolution.numerator = (long) round(lpDIB->bmiHeader.biXPelsPerMeter / 39.37);

	if (!resolution.numerator)
		resolution.numerator = 300;

	AddTagArray(lpex, XRESOLUTION, IFDRATIONAL, 1, (LPSTR) & resolution);

	resolution.denominator = 1;
	if (!OptBlkGet(hOptBlk, "TIFF_YRES", (LPSTR) optbuf))
		resolution.numerator = (long) _fatoi((LPSTR) optbuf);
	else
		resolution.numerator = (long) round(lpDIB->bmiHeader.biYPelsPerMeter / 39.37);

	if (!resolution.numerator)
		resolution.numerator = 300;

	AddTagArray(lpex, YRESOLUTION, IFDRATIONAL, 1, (LPSTR) & resolution);

	if (lpex->nCompress == FAXCCITT3)
		AddTag(lpex, GROUP3OPTIONS, IFDLONG, 1, 4L);

	AddTag(lpex, RESOLUTIONUNIT, IFDSHORT, 1, INCHES);

	// Add PAGENUMBER Tag if we have specified a TIFF_PAGES option
	if (!OptBlkGet(hOptBlk, "TIFF_PAGES", (LPSTR) optbuf)) {
		short PgInfo[2];

		PgInfo[0] = lpex->nPages+1;
		PgInfo[1] = _fatoi( optbuf );

		AddTagArray(lpex, PAGENUMBER, IFDSHORT, 2, (LPSTR) &PgInfo);
	}

	AddTagArray(lpex, SOFTWARE, IFDASCII, 36, "ImageMan by Data Techniques, Inc.");

	if (!OptBlkGet(hOptBlk, "TIFF_ARTIST", (LPSTR) optbuf))
		AddTagArray(lpex, ARTIST, IFDASCII, _fstrlen(optbuf)+1, optbuf );
	
	//
	// Now do the Color Map, if needed.
	//
	if (nColors == 16 || nColors == 256) {
		for (i = 0; i < nColors; i++) {
			palbuf[i] = (int) ((lpDIB->bmiColors[i].rgbRed * 0xffffL) / 255);
			palbuf[i + nColors] = (int) ((lpDIB->bmiColors[i].rgbGreen * 0xffffL) / 255);
			palbuf[i + nColors * 2] = (int) ((lpDIB->bmiColors[i].rgbBlue * 0xffffL) / 255);
		}

		AddTagArray(lpex, COLORMAP, IFDSHORT, nColors * 3, (LPSTR) palbuf);
	}

	if (!OptBlkGet(hOptBlk, "TIFF_COPYRIGHT", (LPSTR) optbuf))
		AddTagArray(lpex, COPYRIGHT, IFDASCII, _fstrlen(optbuf)+1, optbuf );

	return 0;
}

//
// This function packs the bits pointed to by lpIn into the buffer pointed
// to by lpOut and returns the size of the packed row.
// nRowSize is the size of the input row, in bytes.
//
int         PackBits(LPSTR lpOut, HPSTR lpIn, int nRowSize)
{
	LPSTR       lpEnd = lpIn + nRowSize;
	LPSTR       p, p0, pOut;
	int         nRun;

	p = lpIn;
	pOut = lpOut;

	while (p < lpEnd) {

		// literal runs
		p0 = p++;
		while ((*p != *(p - 1)) && (p < lpEnd))
			p++;
		// p now points at 2nd repeated char or end of input
		nRun = (p == lpEnd) ? (p - p0) : (p - p0 - 1);
		if (nRun) {
			while (nRun > 128) {
				*pOut++ = 127;
				_fmemmove(pOut, p0, 128);
				pOut += 128;
				p0 += 128;
				nRun -= 128;
			}
			*pOut++ = nRun - 1;
			while (nRun--)
				*pOut++ = *p0++;
		}
		// repeating runs
		p0 = p - 1;
		while ((*p == *(p - 1)) && (p < lpEnd))
			p++;
		// p now points at 1st non-repeating char
		nRun = p - p0;
		if (nRun > 1) {
			while (nRun > 128) {
				*pOut++ = (char) -127;
				*pOut++ = *p0;
				nRun -= 128;
			}
			*pOut++ = -(nRun - 1);
			*pOut++ = *p0;
		}
	}

	return (pOut - lpOut);
}

void PASCAL swapColors(LPSTR lpBuf, int nLen, int nBits)
{
	char        tmp;

	nLen /= 3;

	while (nLen--) {
		tmp = *lpBuf;
		*lpBuf = *(lpBuf + 2);
		*(lpBuf + 2) = tmp;
		lpBuf += 3;
	}
}


//
// This function packs the bits pointed to by lpIn into the buffer pointed
// to by lpOut and returns the size of the packed row.
// nRowSize is the size of the input row, in bytes.
// The LZW compression scheme is implemented with this function.
//
int         LZWpack(LPSTR lpOut, HPSTR lpIn, int nRowSize, LPEXPORTINFO lpex)
{
	short       nCurPre, tmpk, p;
	unsigned char k;
	HPSTR       lpEnd;
	LPSTR       lpStart;
	LPSTRINGTABLE lpTbl;

	lpStart = lpOut;
	lpEnd = lpIn + nRowSize;
	nCurPre = lpex->nCurPre;
	lpTbl = lpex->lpTable;


	if (nCurPre == -1)
		nCurPre = (unsigned char) *lpIn++;

	while (lpIn < lpEnd) {
		k = (unsigned char) *lpIn++;

		if (nCurPre < 258)
			p = 258;
		else
			p = nCurPre;

		tmpk = FindString(lpTbl, nCurPre, k);	// is the string in the table?

		if (tmpk) {
			nCurPre = tmpk;
		} else {
			lpOut += WriteCode(lpOut, nCurPre, lpex);
			AddString(lpTbl, nCurPre, k, lpex->nCurCode++);
			nCurPre = k;

			// now update code size if needed
			if (lpex->nCurCode == lpex->nBitChange) {
				if (lpex->nLZWbits == 12) {
					lpOut += ClearLZW(lpex, lpOut);
				} else {
					lpex->nLZWbits++;
					lpex->nBitChange <<= 1;
					if (lpex->nLZWbits == 12)
						lpex->nBitChange -= 1;
				}
			}
		}
	}

	lpex->nCurPre = nCurPre;

	return (lpOut - lpStart);
}



//
// This function outputs an LZW compression code containing nBits bits to
// the compression buffer...
//
// Returns the # of bytes written to output buffer.
//
int         WriteCode(LPSTR lpOut, int nCode, LPEXPORTINFO lpex)
{
	int         nWritten = 1;
	int         nAvail = lpex->nAvail;
	int         nBitCnt = lpex->nLZWbits;
	unsigned short tmp;

	tmp = nCode;

	tmp >>= nBitCnt - nAvail;

	if (nAvail == 8)
		*lpOut = 0;

	*lpOut |= (tmp & 0xff);

	tmp = nCode;
	tmp <<= (16 - nBitCnt) + nAvail;

	*(lpOut + 1) = (unsigned char) (tmp >> 8);

	if ((nBitCnt - nAvail) >= 8) {
		tmp = nCode;
		nWritten++;
		*(lpOut + 2) = (unsigned char) ((tmp << (24 - nBitCnt) + nAvail) >> 8);

	}
	lpex->nAvail = 8 - ((lpex->nLZWbits - lpex->nAvail) % 8);

	return nWritten;
}


//
// This function outputs a clear code and initializes the string table.
//
int         ClearLZW(LPEXPORTINFO lpex, LPSTR lpOut)
{
	int         nRet;

	nRet = WriteCode(lpOut, CLEARCODE, lpex);
	lpex->nLZWbits = 9;
	lpex->nBitChange = 512;
	lpex->nCurCode = EOICODE + 1;
	lpex->nCurPre = -1;
	InitializeStringTable(&(lpex->lpTable));

	return nRet;
}

int NEAR PASCAL round(double val)
{

	if (val - (int) val < 0.5)
		return (int) val;
	else
		return (int) val + 1;
}

//
// This function packs the bits pointed to by lpIn into the buffer pointed
// to by lpOut and returns the size of the packed row.
// nRowSize is the size of the input row, in bytes.
// The CCITT1D compression scheme is implemented with this function.
//
// If bEOL is 1, this routine will tack an EOL code onto the end of the
// row (otherwise, it won't).
//
int         CCITT1Dpack(LPSTR lpOut, HPSTR lpIn, LPEXPORTINFO lpex, int bEOL)
{
	LPSTR       lpStart = lpOut;
	unsigned char *npInfo;
	int         i, nCurRunLen;
	unsigned char bBlk;
	int         nUsed = 0;		  // # of bits used in current output byte

	int         nPixWid = (int) lpex->bmi.biWidth;

	if (lpex->nRowCnt == 0 && bEOL)	// if GROUP3 FAX then add initial EOL

		lpOut = WriteCCITTCode(lpOut, &nUsed, -1, 0);

	if ((*lpIn & 0x80) == 0) {	  // need a 0 length white run first

		lpOut = WriteCCITTCode(lpOut, &nUsed, 0, TRUE);
		bBlk = 0;
	} else
		bBlk = 0xff;

	nCurRunLen = 0;
	while (nPixWid) {

		// try to optimize byte runs...
		while (*(HPUSTR) lpIn == bBlk && nPixWid > 8) {
			lpIn++;
			nCurRunLen += 8;
			nPixWid -= 8;
		}

		npInfo = runinfo[*(HPUSTR) lpIn++].outp;
		for (i = 0; i < 8 && nPixWid; i++) {
			if (*npInfo == bBlk) {
				nCurRunLen++;
			} else {
				if (nCurRunLen >= 64) {
					int         nLeft = nCurRunLen % 64;
					lpOut = WriteCCITTCode(lpOut, &nUsed, nCurRunLen - nLeft, bBlk);
					lpOut = WriteCCITTCode(lpOut, &nUsed, nLeft, bBlk);
				} else {
					lpOut = WriteCCITTCode(lpOut, &nUsed, nCurRunLen, bBlk);
				}
				nCurRunLen = 1;
				bBlk = ~bBlk;
			}
			npInfo++;
			nPixWid--;
		}
	}

	if (nCurRunLen) {
		if (nCurRunLen >= 64) {
			int         nLeft = nCurRunLen % 64;
			lpOut = WriteCCITTCode(lpOut, &nUsed, nCurRunLen - nLeft, bBlk);
			lpOut = WriteCCITTCode(lpOut, &nUsed, nLeft, bBlk);
		} else {
			lpOut = WriteCCITTCode(lpOut, &nUsed, nCurRunLen, bBlk);
		}
	}
	if (bEOL) {						  // we need an EOL

		lpOut = WriteCCITTCode(lpOut, &nUsed, -1, 0);
	}
	if (nUsed)
		lpOut++;						  // can't leave any partial bytes for next row.

	return lpOut - lpStart;
}

//
// CCITT2Dpack
//
// Produces Group4 images
//
int         CCITT2Dpack(LPSTR lpOut, HPSTR lpIn, LPEXPORTINFO lpex)
{
	LPSTR       lpStart = lpOut;
	unsigned char *npInfo;
	short       i, j, nCurRunLen;
	unsigned char bBlk, bPrevBlk;
	int         nUsed = lpex->nUsed2d;	// # of bits used in current output byte

	short       nPixWid = (int) lpex->bmi.biWidth;
	NPINT       pPrev, pCur, pTmp;
	short       a0, a1, a2, b1, b2;

	//
	// OK, here's how it's gonna work.
	// - First, generate the current line's runs into lpex->pCur2d
	// - second, compare lpex->pCur2d to lpex->pPrev2d and output the line
	// - third, swap lpPrev2d & lpCur2d
	// - fourth, repeat this until done...
	//

	pPrev = lpex->pPrev2d;
	pCur = lpex->pCur2d;

	// part 1 - generate current row's runs
	if ((*lpIn & 0x80) == 0) {	  // need a 0 length white run first

		*pCur++ = 0;
		bBlk = 0x00;
	} else
		bBlk = 0xff;

	nCurRunLen = 0;
	a0 = 0;							  // use a0 just as a counter for this loop

	j = nPixWid;
	while (nPixWid) {

		// try to optimize byte runs...
		while (*(HPUSTR) lpIn == bBlk && nPixWid > 8) {
			lpIn++;
			nCurRunLen += 8;
			nPixWid -= 8;
		}

		npInfo = runinfo[*(HPUSTR) lpIn++].outp;
		for (i = 0; i < 8 && nPixWid; i++) {
			if (*npInfo == bBlk) {
				nCurRunLen++;
			} else {
				a0 += nCurRunLen;
				*pCur++ = a0;
				nCurRunLen = 1;
				bBlk = ~bBlk;
			}
			npInfo++;
			nPixWid--;
		}
	}
	// store the last value
	if (nCurRunLen)
		*pCur++ = a0 + nCurRunLen;
	// store a couple extra nPixWid values as safeties
	*pCur++ = j;
	*pCur++ = j;

	// OK, now we got our run lengths --- let's output the row!
	nPixWid = j;
	pCur = lpex->pCur2d;
	a0 = 0;
	bBlk = 0xff;					  // start with a white run

	bPrevBlk = 0x00;				  // pPrev[0] is location of 1st black pixel

	while (a0 < nPixWid) {
		a1 = *pCur;
		while (*pPrev > a0 && pPrev > lpex->pPrev2d) {
			pPrev--;
			bPrevBlk = ~bPrevBlk;
		}
		while (*pPrev <= a0 && a0 > 0) {
			pPrev++;
			bPrevBlk = ~bPrevBlk;
		}
		if (bPrevBlk == bBlk) {
			if (*pPrev < nPixWid)
				pPrev++;
			bPrevBlk = ~bPrevBlk;
		}
		b1 = *pPrev;
		b2 = *(pPrev + 1);

		// now output our 2d code, depending on the "mode" (pass, vertical, horz)
		if (b2 < a1) {				  // pass mode
			// output pass mode code '0001b'

			DEBUGOUT(IMGDETAIL, "\tPASS, b2 = %d", b2);
			lpOut = WriteCCITT2DCode(lpOut, &nUsed, G4_PASS);
			a0 = b2;
			pPrev += 2;
		} else if (abs(a1 - b1) <= 3) {	// vertical mode
			// output vertical code based on (a1-a0)

			switch (a1 - b1) {
				case -3:			  // VL(3)

					DEBUGOUT(IMGDETAIL, "\tVL3, a1: %d, b1: %d", a1, b1);
					lpOut = WriteCCITT2DCode(lpOut, &nUsed, G4_VL3);
					break;
				case -2:			  // VL(2)

					DEBUGOUT(IMGDETAIL, "\tVL2, a1: %d, b1: %d", a1, b1);
					lpOut = WriteCCITT2DCode(lpOut, &nUsed, G4_VL2);
					break;
				case -1:			  // VL(1)

					DEBUGOUT(IMGDETAIL, "\tVL1, a1: %d, b1: %d", a1, b1);
					lpOut = WriteCCITT2DCode(lpOut, &nUsed, G4_VL1);
					break;
				case 0:				  // V(0)

					DEBUGOUT(IMGDETAIL, "\tV0, %d", a1);
					lpOut = WriteCCITT2DCode(lpOut, &nUsed, G4_V0);
					break;
				case 1:				  // VR(1)

					DEBUGOUT(IMGDETAIL, "\tVR1, a1: %d, b1: %d", a1, b1);
					lpOut = WriteCCITT2DCode(lpOut, &nUsed, G4_VR1);
					break;
				case 2:				  // VR(2)

					DEBUGOUT(IMGDETAIL, "\tVR2, a1: %d, b1: %d", a1, b1);
					lpOut = WriteCCITT2DCode(lpOut, &nUsed, G4_VR2);
					break;
				case 3:				  // VR(3)

					DEBUGOUT(IMGDETAIL, "\tVR3, a1: %d, b1: %d", a1, b1);
					lpOut = WriteCCITT2DCode(lpOut, &nUsed, G4_VR3);
					break;
			}
			a0 = a1;
			pCur++;
			bBlk = ~bBlk;
		} else {						  // horizontal mode

			int         runlen;

			a2 = *(pCur + 1);
			DEBUGOUT(IMGDETAIL, "\tHORZ, a0: %d, a1: %d, a2: %d", a0, a1, a2);
			// Output HORZ code '001b'
			lpOut = WriteCCITT2DCode(lpOut, &nUsed, G4_HORZ);
			runlen = a1 - a0;
			if (runlen >= 64) {
				int         nLeft = runlen % 64;
				lpOut = WriteCCITTCode(lpOut, &nUsed, runlen - nLeft, bBlk);
				lpOut = WriteCCITTCode(lpOut, &nUsed, nLeft, bBlk);
			} else
				lpOut = WriteCCITTCode(lpOut, &nUsed, (a1 - a0), bBlk);

			bBlk = ~bBlk;
			runlen = a2 - a1;
			if (runlen >= 64) {
				int         nLeft = runlen % 64;
				lpOut = WriteCCITTCode(lpOut, &nUsed, runlen - nLeft, bBlk);
				lpOut = WriteCCITTCode(lpOut, &nUsed, nLeft, bBlk);
			} else
				lpOut = WriteCCITTCode(lpOut, &nUsed, (a2 - a1), bBlk);
			bBlk = ~bBlk;

			a0 = a2;
			pCur += 2;
		}
	}

	lpex->nUsed2d = nUsed;
	pTmp = lpex->pCur2d;
	lpex->pCur2d = lpex->pPrev2d;
	lpex->pPrev2d = pTmp;
	return lpOut - lpStart;
}

LPSTR       WriteCCITT2DCode(LPSTR lpOut, LPINT lpUsed, short nCode)
{
	*lpOut |= (c2d[nCode] >> *lpUsed);

	if (len2d[nCode] >= (8 - *lpUsed)) {	// we'll fill up *lpOut with this code

		lpOut++;
		*lpOut = (c2d[nCode] << (8 - *lpUsed));
		*lpUsed = len2d[nCode] - (8 - *lpUsed);
	} else {							  // code will fit in *lpOut

		*lpUsed += len2d[nCode];
	}

	return lpOut;
}

LPSTR       WriteCCITTCode(unsigned char far * lpOut, LPINT lpUsed, int nRunLen, int bBlk)
{
	unsigned short nLen, nCode;
	unsigned short tmp;

	while (nRunLen > 1728) {
		lpOut = WriteCCITTCode(lpOut, lpUsed, 1728, bBlk);
		nRunLen -= 1728;
	}

	//
	// if the run length is > 64, we need to adjust so that our
	// array references will be workable. For every multiple of
	// 64 we are above 64, we add 1 to the nRunLen (past 64, that
	// is...
	//

	if (nRunLen == -1) {			  // Write padded EOL

		if (*lpUsed)
			lpOut++;

		*lpOut++ = 0x00;
		*lpOut++ = 0x01;

		*lpUsed = 0;

		return lpOut;

	}
	if (nRunLen > 64)
		nRunLen = 64 + nRunLen / 64 - 1;

	if (bBlk) {
		nLen = wlen[nRunLen];
		nCode = wcodes[nRunLen];
	} else {
		nLen = blen[nRunLen];
		nCode = bcodes[nRunLen];
	}

	if (nLen <= (8 - *lpUsed)) {
		*lpOut |= ((nCode >> 8) >> *lpUsed);
	} else if (nLen <= (16 - *lpUsed)) {
		tmp = nCode;
		tmp >>= *lpUsed;
		*lpOut |= (tmp >> 8);
		*(lpOut + 1) = (unsigned char) tmp & 0xff;

	} else {
		tmp = nCode;
		tmp >>= *lpUsed;
		*lpOut |= (tmp >> 8);

		*(lpOut + 1) = (unsigned char) (tmp & 0xff);

		tmp = (nCode & 0x00ff) << 8;
		*(lpOut + 2) = (unsigned char) (tmp >> *lpUsed);
	}

	lpOut += (*lpUsed + nLen) / 8;
	*lpUsed = (*lpUsed + nLen) % 8;
	return lpOut;
}

void        MakeCCITTTable(void)
{
	short       i, j;
	unsigned char mask;

	for (i = 0; i < 256; i++) {
		mask = 0x80;
		for (j = 0; j < 8; j++) {
			runinfo[i].outp[j] = (((unsigned char) i & mask) ? 0xff : 0x00);
			mask >>= 1;
		}
	}
}

void        swap(a)
	LPSTR       a;
{
	register char tmp;

	tmp = a[1];
	a[1] = a[0];
	a[0] = tmp;
}

void        swapl(a)
	LPSTR       a;
{
	register char tmp;

	tmp = a[0];
	a[0] = a[3];
	a[3] = tmp;

	tmp = a[1];
	a[1] = a[2];
	a[2] = tmp;
}

void        swapa(LPSTR buf, short count)
{
	while (count--) {
		swap(buf);
		buf += 2;
	}
}

void        swapla(LPSTR buf, short count)
{
	while (count--) {
		swapl(buf);
		buf += 4;
	}
}

#if DEBUG
void _cdecl FAR Debug(UINT wCategory, LPSTR lpFormat,...)
{
	LPSTR       vaArgs;
	BOOL        bResult;

	va_start(vaArgs, lpFormat);
	(*DebugStringOut) (wCategory, lpFormat, vaArgs);
	va_end(vaArgs);
}
#else
void _cdecl FAR Debug(UINT wCategory, LPSTR lpFormat,...)
{
}
#endif

void        flipBits(unsigned char far * lpBits, unsigned int nBytes)
{

	while (nBytes--) {
		*lpBits = fliptable[*lpBits];
		lpBits++;
	}
}

#ifdef WIN32
//
// Given a far pointer to a string which represents an int, convert from
// string to int...
//
int FAR     _fatoi(char FAR * lpInt)
{
	int         i = 0;

	while (*lpInt) {
		i *= 10;
		i += (*lpInt++ - '0');
	}

	return i;
}
#endif

void RGBToCMYK ( int nRowsIn, int nRowSize, LPSTR lpBits, LPSTR lpCMYK )
{
	int nRows, nPos ;
	UCHAR uC, uM, uY, uK, uR, uG, uB, uDiv ;
	LPSTR lpRow = lpBits ;

	// just remember data in is bgr and needs to go out cmyk
	for ( nRows = 0 ; nRows < nRowsIn ; nRows ++ )
		{
			for ( nPos = 0 ; nPos < nRowSize ; nPos += 3 )
				{
					uK = uB = 255 - *lpRow++ ;
					uG = 255 - *lpRow++ ;
					uR = 255 - *lpRow++ ;
					if ( uG < uK )
						uK = uG ;
					if ( uR < uK )
						uK = uR ;
					uDiv = 255 - uK ;
					if ( uDiv == 0 ) uDiv = 1 ;
					uC = ( uR - uK ) ; // / uDiv ;
					uM = ( uG - uK ) ; // / uDiv ;
					uY = ( uB - uK ) ; // / uDiv ;
					* lpCMYK++ = uC ;
					* lpCMYK++ = uM ;
					* lpCMYK++ = uY ;
					* lpCMYK++ = uK ;
				}
			lpRow = lpBits + ( nRowSize * nRows ) ;
		}
}


