/////////////////////////////////////////////////////////////////////////////
//
// prif.h
//
//
// PRI Support Structures & Defines & Prototypes
//
// ImageMan Image Processing Library
//
// Copyright (c) 1991-5 Data Techniques, Inc.
// All Rights Reserved
//
// ///////////////////////////////////////////////////////////////////////////
// $History: PRIF.H $
// 
// *****************  Version 1  *****************
// User: Sygsky  Date: 04-OCT-2000 Time: 16:42
// Created in $/ImageMan 16-32/DELS
// ImageMan 4.0 Beta 1
// 

typedef struct tagTiffStruct {
	USHORT      ByteOrder;
	UINT        FileType;
	UINT        ImageWidth;           /* Required Field */
	UINT        ImageLength;          /* Required Field */
	UINT        GrayBits;             /* Required Field */
	UINT        Compression;
	ULONG       StripsPerImage;
	ULONG       StripFileLoc;         /* where the strip is if there's only 1 */
	HGLOBAL     StripOffsets;         /* Handle to an array to hold the offsets to the strips of the
										   * image. Must be of size StripsPerImage */
	ULONG       RowsPerStrip;
	ULONG       StripBytesFileLoc;  /* strip byte count if there's only 1 */
	HGLOBAL     StripByteCounts;  /* Handle to an array to hold the byte counts of the strips of the
										   * image. Must be of size StripsPerImage */

	// Tile support stuff. When an image is tiled, these values supercede StripsPerImage, RowsPerStrip
	// StripOffsets, and StripByteCounts
									
	ULONG           TilesAcross;             // TilesAcross = (ImageWidth + (TileWidth -1)) / TileWidth
	ULONG           TilesDown;               // TilesDown = (ImageLength + (TileLength - 1)) / TileLength
	ULONG           TilesPerImage;           //  = TilesAcross * TilesDown * SamplesPerPixel
	ULONG           TileWidth;               // width of tile in pixels, multiple of 16      
	ULONG           TileLength;              // length of tile in pixels, multiple of 16, tiles may be rectangular
	ULONG           TileFileLoc;             // where tile is if there's only 1
	HGLOBAL         TileOffsets;             // Handle to array to hold offsets to the tiles
	HGLOBAL         TileByteCounts;          // Handle to array to hold byte counts of tiles 
	UINT			TileBytesLine;			 // bytes per row within a tile
	//

	UINT        WhiteValue;           /* Photometric Interpretation */
	UINT        MinimumValue;
	UINT        MaximumValue;
	short       ScanType;
	UINT        CellWidth;
	UINT        CellLength;
	UINT        FillOrder;
	ULONG       XRezNumerator;        /* resolution numerators */
	ULONG       XRezDenominator;  /* and denominators.  */
	ULONG       YRezNumerator;
	ULONG       YRezDenominator;
	UINT        XPosition;
	UINT        YPosition;
	HGLOBAL     ColorMap;             /* Handle to array to hold color map */
	HGLOBAL     ColorResponseCurves;        /* Handle to array to hold the color  response curves */
	UINT        ColorResponseUnit;
	UINT        Orientation;
	UINT        SamplesPerPixel;
	UINT        PlanarConfiguration;
	HGLOBAL     GrayScaleResponse;  /* Handle to array to hold the gray scale response curve */
	UINT        GrayResponseUnits;
	UINT        BytesLine;            /* Required Field */
	UINT        ResolutionUnit;  /* Unit of measure, inches, */
	UINT        PageNumber;           /* Page number of # of pages */
	UINT        Pages;                        /* Number of pages */
	ULONG       Group3Options;
	ULONG       Group4Options;
	int         Predictor;            /* LZW predictor */
}           TiffStruct;


struct Rational {
	long        numerator;
	long        denominator;
};

struct FourBytes {
	char        byte0, byte1, byte2, byte3;
};

union LongWord {
	long        l;
	struct FourBytes b;
};

struct TwoBytes {
	char        byte0, byte1;
};

union ShortWord {
	short       i;
	struct TwoBytes b;
};

#define MAXIMAGEHEADERSIZE 1024
#define MAXIMAGEHEADERTAGS 40
#define MAXHEADERDATASIZE  2048
#define DEFIMAGEDATAOFFSET 512l

/* TIFF equates for Tag names and Field types */

#define FF 0x4646
#define VS 0x5356                                         /* "VS" as a file type stamp */
#define MM 0x4D4D

#define PHOTOINT_CMYK 5

#define NEWSUBFILETYPE  254       /* Public Tags */
#define FILETYPE 255
#define IMAGEWIDTH 256
#define IMAGELENGTH 257
#define BITSPERSAMPLE 258
#define COMPRESSION 259
#define PHOTOMETRICINTERPRETATION 262
#define THRESHHOLDING 263
#define CELLWIDTH 264
#define CELLLENGTH 265
#define FILLORDER 266
#define DOCUMENTNAME 269
#define IMAGEDESCRIPTION 270
#define MAKE 271
#define MODEL 272
#define STRIPSOFFSETS 273
#define ORIENTATION 274
#define SAMPLESPERPIXEL 277
#define ROWSPERSTRIP 278
#define STRIPBYTECOUNTS 279
#define MINIMUMVALUE 280
#define MAXIMUMVALUE 281
#define XRESOLUTION 282
#define YRESOLUTION 283
#define PLANARCONFIG 284
#define PAGENAME 285
#define XPOSITION 286
#define YPOSITION 287
#define FREEOFFSETS 288
#define FREEBYTECOUNTS 289
#define GRAYRESPONSEUNITS  290
#define GRAYSCALERESPONSE  291
#define GROUP3OPTIONS  292
#define GROUP4OPTIONS  293
#define RESOLUTIONUNIT 296
#define PAGENUMBER 297
#define COLORRESPONSEUNIT 300
#define COLORRESPONSECURVES 301
#define SOFTWARE        305
#define ARTIST 315
#define PREDICTOR 317
#define COLORMAP        320
// tiled image defines
#define TILEWIDTH       322
#define TILELENGTH      323
#define TILEOFFSETS     324
#define TILEBYTECOUNTS  325

#define COPYRIGHT       33432

#define IFDBYTE 1                                         /* Data Types */
#define IFDASCII 2
#define IFDSHORT 3
#define IFDLONG 4
#define IFDRATIONAL 5

#define IFDBYTESIZE 1                     /* Data Type Sizes (in bytes) */
#define IFDASCIISIZE 1
#define IFDSHORTSIZE 2
#define IFDLONGSIZE 4
#define IFDRATIONALSIZE 8

#define NONE   1                                          /* Resolution Units */
#define INCHES  2
#define CENTIMETERS 3

/* Error return Codes */

#define FILEIOERROR -1
#define NOTTIFF -2
#define UNKNOWNCOMPRESSION -3
#define COMPRESSIONERROR -4
#define BADTAG -5
#define TOMANYIMAGES -10
#define BADFILEMODE -11
#define FILEOVERWRITE -12
#define CANTOPENFILE -13
#define CANTCLOSEFILE -20

#define MAXIMAGES 16
#define MAX_BUFFERS 256
#define MAXLINESIZE 2048                  /* 1 line of 8.5", 300 Dpi., 4 bit Gray Scale, 160% scale */

/* compression constants */
#define NOCOMPRESSBYTE  1                 /* no compression, byte aligned */
#define CCITT3                          2                 /* CCITT Group 3 1-Dimensional  */
#define FAXCCITT3                       3                 /* Fax Compatible CCITT Group 3 */
#define FAXCCITT4                       4                 /* Fax Compatible CCITT Group 4 */
#define LZW                                     5                 /* LZW compression */
#define NOCOMPRESSWORD  32771     /* no compression, word aligned */
#define PACKBITS                        0x8005U  /* Packbits compression         */

typedef struct tagTiffHead {
	USHORT      order;
	USHORT      ver;
	ULONG       ifd0;
}           TiffHead;

typedef struct doubleInt {
	USHORT      i1;
	USHORT      i2;
};

union tiffValue {
	ULONG       lval;
	struct doubleInt ival;
};

typedef struct tagDirStruct {     // structure of a TIFF directory entry

	SHORT       tag;
	SHORT           type;
	ULONG       len;
	union tiffValue value;
}           DirStruct;

int SeekToFirstPage(LPVOID lpHandle, LPIMG_IO lpIO, long offset, USHORT FAR *lpByteOrder, ULONG FAR *lpCurOffset);
int NextPage(LPVOID lpHandle, LPIMG_IO lpIO, long offset, ULONG FAR *lpCurOffset, BOOL bSwap);
int CountPages(LPVOID, LPIMG_IO, long, UINT FAR *, BOOL);
int FAR PASCAL ReadTiffHead(LPVOID, LPIMG_IO, TiffStruct FAR *, long);

