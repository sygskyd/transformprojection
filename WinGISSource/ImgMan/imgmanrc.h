//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Imgman.rc
//
#define __IMF_TOP                       0
#define __IM_TOP                        0
#define __STAT_VALUELEFT                60
#define IDI_ICON1                       102
#define IDC_DBGIMGMAN                   1002
#define IDC_DBGINTERNAL                 1003
#define IDC_DBGDIL                      1004
#define IDC_DBGDILINT                   1005
#define IDC_DBGIMAGE                    1006
#define IDC_LIST1                       1007
#define IDC_CHECK1                      1008
#define IDC_CLEAROUTPUT                 1008
#define IDC_LOG                         1009
#define IDC_STARTLINE                   1011
#define IDC_ENDLINE                     1013
#define IDC_STATICCOLOR                 1320
#define IDC_STATICCOMPRESS              1321
#define IDC_COMPRESS                    1330
#define IDC_NOCOMPRESS                  1331
#define IDC_LZWCOMP                     1332
#define IDC_PACKBITS                    1333
#define IDC_MODCCITT                    1334
#define IDC_GROUP3                      1335
#define IDC_GROUP4                      1336
#define IDC_RGBCOLOR                    1337
#define IDC_CMYKCOLOR                   1338
#define IDC_GIFINTERLACED               1339
#define IDC_APPENDIMAGE                 1340
#define IDC_STATICPNGFILTER             1341
#define IDC_STATICPNGCOMPRESS           1342
#define IDC_PNGFILTERNONE               1343
#define IDC_PNGFILTERSUB                1344
#define IDC_PNGFILTERUP                 1345
#define IDC_PNGFILTERAVERAGE            1346
#define IDC_PNGFILTERPAETH              1347
#define IDC_PNGNOCOMPRESS               1348
#define IDC_PNGAVERAGECOMPRESS          1349
#define IDC_PNGMAXCOMPRESS              1350
#define IDC_XRES                        1351
#define IDC_YRES                        1352
#define IDC_TIFFROWSPERSTRIP            1353
#define IDC_TIFFFILLORDERLSB            1354
#define IDC_TIFFFILLORDERMSB            1355
#define IDC_TIFFBYTEFORMAT              1356
#define IDC_TIFFINTEL                   1357
#define IDC_TIFFMOTOROLA                1358
#define IDC_TIFFARTIST                  1359
#define IDC_TIFFCOPYRIGHT               1360
#define IDC_STATICTIFFXRES              1361
#define IDC_STATICTIFFYRES              1362
#define IDC_STATICTIFFROWSPERSTRIP      1363
#define IDC_STATICTIFFFILLORDER         1364
#define IDC_STATICTIFFBYTEFORMAT        1365
#define IDC_STATICTIFFARTIST            1366
#define IDC_STATICTIFFCOPYRIGHT         1367
#define IDC_LOSSYQUALITY                1368
#define IDC_STATICJPEGLOSSY             1369
#define IDC_SHOWSTATS                   1370
#define IDC_SHOWPREVIEW                 1371
#define IDC_PREVIEWIMAGE                1372
#define IDC_STATNAME                    1373
#define IDC_STATSIZE                    1374
#define IDC_STATTYPE                    1375
#define IDC_STATCOLORS                  1376
#define IDC_STATDIMENSIONS              1377
#define IDC_STATCOLORMODEL              1378
#define IDC_STATDATE                    1379
#define IDC_STATNAMEVALUE               1380
#define IDC_STATSIZEVALUE               1381
#define IDC_STATTYPEVALUE               1382
#define IDC_STATCOLORSVALUE             1383
#define IDC_STATDIMENSIONSVALUE         1384
#define IDC_STATCOLORMODELVALUE         1385
#define IDC_BUTTON1                     1385
#define IDC_STATDATEVALUE               1386
#define IDC_IMGERRSTRING                1387
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        103
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1386
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
