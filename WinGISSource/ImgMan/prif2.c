/////////////////////////////////////////////////////////////////////////////
//
// prif2.c
//
// Routines for dealing with PRI file format.
//
// ImageMan Image Processing Library
//
// Copyright (c) 1991-5 Data Techniques, Inc.
// All Rights Reserved
//
// ///////////////////////////////////////////////////////////////////////////
// $History: PRIF2.C $
// 
// *****************  Version 1  *****************
// User: Sygsky Date: 04-OCT-2000 Time: 16:46
// Created in $/ImageMan 16-32/DILS
// ImageMan 4.0 Beta 1

#include "internal.h"
#pragma hdrstop
#include <stdio.h>
#include <stdlib.h>
#include "prif.h"
#include "lzw.h"
#include "pri.h"

//#ifdef WIN32
// inline swaps for 32-bit compiles
#define BSWAP(S)  (S) = (WORD)(((UINT)(S)<<8)|(((UINT)(S))>>8))
#define BSWAPL(L) (L) = ((L)<<24)|((ULONG)(L)>>24)\
                          |(((L)>>8)&0x0000ff00)|(((L)<<8)&0x00ff0000)
//#else
// swaps for 16-bit compiles, very fast if "inline intrinsics" optimization is on
//#define BSWAP(S) (S) = _rotl((S),8)
//#define BSWAPL(L) (L) = (_rotl(LOWORD(L),8)<<16)|_rotl(HIWORD(L),8)
//#endif

#ifdef WIN32

#ifdef WINGIS // so the file TLSByJHC.h was included

#define SETERROR(E) bJHCTLSSetValue((DWORD)(E))
#define GETERROR    nJHCTLSGetValue()

#else

extern DWORD TlsIndex;			  // Thread local storage index
#define SETERROR(E) TlsSetValue(TlsIndex,(LPVOID)(DWORD)(E))
#define GETERROR    ((DWORD)TlsGetValue(TlsIndex))

#endif

#else
extern int  nErrID;				  // id of latest error
#define SETERROR(E) nErrID = (E)
#define GETERROR    (nErrID)
#endif

static int  ProcessIfdEntry(LPVOID lpHandle, LPIMG_IO lpIO, TiffStruct FAR * ts, LPLONG curOffset, long offset);
static int  CreateDefaultTiffHead(TiffStruct FAR * ImageDescriptor);
static int  ReadDirectory(LPVOID lpHandle, LPIMG_IO lpIO, LPLONG offset, DirStruct FAR * ds, int order);
static HGLOBAL ReadArray(LPVOID, LPIMG_IO, long, UINT, int, int);
static void Short2LongArray(ULONG FAR *, UINT);
static void swap(LPVOID);
static void swapl(LPVOID);
static void swapa(LPVOID, UINT);
static void swapla(LPVOID, UINT);


// Read in the TIFF header and all tags - fill in TiffStruct
int FAR PASCAL ReadTiffHead(LPVOID lpHandle, LPIMG_IO lpIO, TiffStruct FAR * ts, long offset)
{
	long        curOffset;
	USHORT      dirCount, entry;
	int			nErr;

	// now fill in ts with intelligent (hopefully) defaults
	CreateDefaultTiffHead(ts);

	curOffset = lpIO->seek(lpHandle, 0, SEEK_CUR);

	if (lpIO->read(lpHandle, (HPSTR)&dirCount, sizeof(dirCount)) != sizeof(dirCount)) {
		SETERROR(IDS_READERR);
		return (IMG_FILE_ERR);
	}
	curOffset += 2;				  // adjust for reading dirCount

	if (ts->ByteOrder == MM)
		BSWAP(dirCount);

	// read in directory entries and process them
	for (entry = dirCount; entry--;) {
		if ((nErr = ProcessIfdEntry(lpHandle, lpIO, ts, (LPLONG) & curOffset, offset)) != IMG_OK) {
			return (nErr);
		}
	}

		// now calculate bytes/line for both stripped and tiled images
	ts->BytesLine = (UINT)(((long)ts->ImageWidth * ts->GrayBits * ts->SamplesPerPixel + 7) / 8);
	
	// Strips vs Tiles
	if (ts->TileWidth) {											// is it a tiled image
		// calculate TilesAcross, TilesDown, TilesPerImage
		ts->TilesAcross = (ts->ImageWidth + (ts->TileWidth - 1)) / ts->TileWidth;
		ts->TilesDown = (ts->ImageLength + (ts->TileLength - 1)) / ts->TileLength;
		ts->TilesPerImage = ts->TilesAcross * ts->TilesDown * ts->SamplesPerPixel;
		ts->TileBytesLine = (UINT)(((long)ts->TileWidth * ts->GrayBits * ts->SamplesPerPixel + 7) / 8);
		// apparently sometimes the TileOffsets #324, and TileByteCounts #325 are not used in a tiled image but
		// are taken from analagous StripOffsets #273 and StipByteCounts #279. I don't read the tiff
		// spec this way but some of the 'test' images from Aldus do this so, 'Oh Well....'
		if (!ts->TileOffsets) {
			ts->TileOffsets = ts->StripOffsets;
			ts->StripOffsets = NULL;
		}
		if (!ts->TileByteCounts) {		
			ts->TileByteCounts = ts->StripByteCounts;
			ts->StripByteCounts = 0;
		}

		if (! ts->TilesPerImage) ts->TilesPerImage = 1;
	}
	else {															// or a stripped image
		// calculate strips/image - these aren't in file
		ts->StripsPerImage = (ts->ImageLength + ts->RowsPerStrip - 1) / ts->RowsPerStrip;
		if (!ts->StripsPerImage) ts->StripsPerImage = 1;
	}

	return (IMG_OK);
}

int SeekToFirstPage(LPVOID lpHandle, LPIMG_IO lpIO, long offset, USHORT FAR *lpByteOrder, ULONG FAR *lpCurOffset)
{
	ULONG       curOffset;
	USHORT      dirCount;
	TiffHead    tHead;
	TiffStruct  ts;

	lpIO->seek(lpHandle, offset, SEEK_SET);

	if (lpIO->read(lpHandle, (HPSTR)&tHead, sizeof(TiffHead)) != sizeof(TiffHead)) {
		SETERROR(IDS_READERR);
		return (IMG_FILE_ERR);
	}
	if (lpByteOrder)
		*lpByteOrder = tHead.order;

	switch (ts.ByteOrder = tHead.order) {
		case VS:
			break;
		default:
			SETERROR(IDS_BADSWAP);
			return (IMG_INV_FILE);
	}

	curOffset = tHead.ifd0 + offset;
	lpIO->seek(lpHandle, curOffset, SEEK_SET);
	if (lpCurOffset) *lpCurOffset = curOffset;

	return (IMG_OK);
}

int NextPage(LPVOID lpHandle, LPIMG_IO lpIO, long offset, ULONG FAR *lpCurOffset, BOOL bSwap)
{
	DWORD       curOffset;
	WORD        dirCount;
	UINT        u;

	lpIO->seek(lpHandle, *lpCurOffset, SEEK_SET);	//seek to current page offset

	// now read # of directory entries and skip over them
	if ((u = (UINT)lpIO->read(lpHandle, &dirCount, sizeof(dirCount))) == HFILE_ERROR) {
		SETERROR(IDS_READERR);
		return (IMG_FILE_ERR);
	} 
	else if (u != sizeof(dirCount)) {
		SETERROR(IDS_READERR);
		return (IMG_FILE_ERR);
	}

	if (bSwap) BSWAP(dirCount);
	// Skip over directory entries... this may take us past the EOF.
	lpIO->seek(lpHandle, dirCount * sizeof(DirStruct), SEEK_CUR);

	// read offset of next IFD
	if ((u = (UINT)lpIO->read(lpHandle, &curOffset, sizeof(curOffset))) == HFILE_ERROR) {
		SETERROR(IDS_READERR);
		return (IMG_FILE_ERR);
	} 
	else if (u == sizeof(curOffset)) {	// valid read

		if (bSwap) BSWAPL(curOffset);
		lpIO->seek(lpHandle, curOffset+offset, SEEK_SET);
	} 
	else curOffset = 0;			  // went past end-of-file, so must be last page.

	*lpCurOffset = curOffset+offset;

	return IMG_OK;
}

int         CountPages(LPVOID lpHandle, LPIMG_IO lpIO, long offset, UINT FAR * lpCnt, BOOL bSwap)
{
	DWORD       curOffset;
	WORD        dirCount;
	UINT        u;

	*lpCnt = 0;

	if (SeekToFirstPage(lpHandle, lpIO, offset, NULL, &curOffset) != IMG_OK) {
	}
//	curOffset = lpIO->seek(lpHandle, 0, SEEK_CUR);

	// CAR: NT is pretty picky when it comes to reading past the EOF; unlike
	// Win16 (which says it succeeds), NT will correctly report 0 bytes read.
	// This isn't, however, an error, and should be treated as if 'curOffset'
	// was successfully read and was zero.

	while (curOffset) {
		if ((u = (UINT)lpIO->read(lpHandle, &dirCount, sizeof(dirCount))) == HFILE_ERROR) {
			SETERROR(IDS_READERR);
			return (IMG_FILE_ERR);
		} 
		else if (u != sizeof(dirCount)) {
			break;					  // no valid page here, so leave
		}

		if (bSwap) BSWAP(dirCount);
		// Skip over directory entries... this may take us past the EOF.
		lpIO->seek(lpHandle, dirCount * sizeof(DirStruct), SEEK_CUR);

		if ((u = (UINT)lpIO->read(lpHandle, &curOffset, sizeof(curOffset))) == HFILE_ERROR) {
			SETERROR(IDS_READERR);
			return (IMG_FILE_ERR);
		} 
		else if (u == sizeof(curOffset)) {	// valid read

			if (bSwap) BSWAPL(curOffset);
			lpIO->seek(lpHandle, curOffset+offset, SEEK_SET);
		} 
		else curOffset = 0;			  // went past end-of-file, so must be last page.

		(*lpCnt)++;
	}

	return (IMG_OK);
}

static int  ProcessIfdEntry(LPVOID lpHandle, LPIMG_IO lpIO, TiffStruct FAR * ts, LPLONG curOffset, long offset)
{
	int         err;
	DirStruct   ds;

	if ((err = ReadDirectory(lpHandle, lpIO, curOffset, (DirStruct FAR *) & ds, ts->ByteOrder)) != IMG_OK) {
		return (err);				  // SETERROR done inside readdirectory().

	}
	switch (ds.tag) {
		case FILETYPE:
			ts->FileType = ds.value.ival.i1;
			break;
		case IMAGEWIDTH:
			if( ds.type == IFDLONG )
				ts->ImageWidth = ds.value.lval;
			else
				ts->ImageWidth = ds.value.ival.i1;
				
			if (ts->ImageWidth <= 0) {
				SETERROR(IDS_BADMAN);
				return (IMG_NSUPPORT);
			}
			break;
		case IMAGELENGTH:
			if( ds.type == IFDLONG )
				ts->ImageLength = ds.value.lval;
			else
				ts->ImageLength = ds.value.ival.i1;
			if (ts->ImageLength <= 0) {
				SETERROR(IDS_BADMAN);
				return (IMG_NSUPPORT);
			}
			break;
		case BITSPERSAMPLE:
			if (ds.len <= 1)
				ts->GrayBits = ds.value.ival.i1;
			else {					  // we've got ds.len bitspersample entries

				if ( ( ds.len != 3 ) &&
					   ( ds.len != 4 ) ) {  // only work with RGB and CMYK

					SETERROR(IDS_BADBITS);
					return (IMG_NSUPPORT);
				} else {
					HANDLE      hbps;
					LPLONG      lpbps;

					// CAR: Since GrayBits should be the "bits per sample" value,
					// we no longer add all the array values up. Instead, we force
					// all samples to be the same depth, then set GrayBits to that
					// depth.
					hbps = ReadArray(lpHandle, lpIO, ds.value.lval + offset, (int)ds.len, ts->ByteOrder, (int)ds.type);
					lpbps = (LPLONG) GlobalLock(hbps);
					if (lpbps[0] != lpbps[1] || lpbps[0] != lpbps[2]) {
						GlobalUnlock(hbps);	// all samples must be the same size

						GlobalFree(hbps);
						SETERROR(IDS_BADBITS);
						return (IMG_NSUPPORT);
					}
					ts->GrayBits = (UINT)lpbps[0];
					GlobalUnlock(hbps);
					GlobalFree(hbps);
				}
			}
			break;
		case PHOTOMETRICINTERPRETATION:
			ts->WhiteValue = ds.value.ival.i1;
			if ( ( ts->WhiteValue > 5  ) || 
				   ( ts->WhiteValue == 4 ) ) { // must be 0, 1, 2, 3 or 5.

				SETERROR(IDS_BADPLANES);
				return (IMG_NSUPPORT);
			}
			break;
		case COMPRESSION:
			ts->Compression = ds.value.ival.i1;
			if (ts->Compression != FAXCCITT3 && ts->Compression != FAXCCITT4
				 && ts->Compression != NOCOMPRESSBYTE && ts->Compression != CCITT3
#ifdef USELZW
				 && ts->Compression != LZW 
#endif
				 && ts->Compression != PACKBITS) {
				SETERROR(IDS_BADCOMPRESS);
				return (IMG_NSUPPORT);
			}
			break;
		case THRESHHOLDING:
			ts->ScanType = ds.value.ival.i1;
			break;
		case ROWSPERSTRIP:
			ts->RowsPerStrip = (ds.type == IFDLONG) ? ds.value.lval : (ULONG) ds.value.ival.i1;
			if ( !ts->RowsPerStrip || ts->RowsPerStrip > ts->ImageLength)
				ts->RowsPerStrip = ts->ImageLength;
//			if (!(ts->StripOffsets) && (ts->ImageLength > ts->RowsPerStrip) ) 
//				ts->ImageLength = (UINT)(ts->RowsPerStrip);
			break;
			
		case SAMPLESPERPIXEL:
			ts->SamplesPerPixel = ds.value.ival.i1;
			break;

		case STRIPSOFFSETS:
			// Here we try to read in the strip offset info. If there is only 1
			// strip, it's stored in the directory's offset value. In this case,
			// we store the offset to the strip in ts->StripFileLoc and set the
			// ts->StripOffsets HANDLE to NULL so we'll know that there's only
			// 1 strip (StripsPerImage will tell us this also). If there's more
			// than 1 strip, ts->StripOffsets is a GlobalHandle to the array
			// containing the strip info. Note that the strip offsets can be
			// long or short values - they are converted to longs when the
			// array is read in.
			if( ts->StripOffsets ) {
				GlobalFree( ts->StripOffsets );
				ts->StripOffsets = NULL;
			}

			if (ds.len == 1) {	  // 1 strip, it's in offset

				ts->StripFileLoc = ds.type == IFDLONG ? ds.value.lval : (long) ds.value.ival.i1;
				ts->StripOffsets = NULL;
			} else {
				if (!(ts->StripOffsets = ReadArray(lpHandle, lpIO, ds.value.lval + offset, (int)ds.len, ts->ByteOrder, (int)ds.type))) {
					SETERROR(IDS_MEMERR);
					return (IMG_NOMEM);
				}
			}
			break;

		case TILEOFFSETS:
			// analagous process to STRIPSOFFSETS except with the tiles
			if( ts->TileOffsets ) {
				GlobalFree( ts->TileOffsets );
				ts->TileOffsets = NULL;
			}

			if (ds.len == 1) {	  // 1 tile, it's in offset

				ts->TileFileLoc = ds.type == IFDLONG ? ds.value.lval : (long) ds.value.ival.i1;
				ts->TileOffsets = NULL;
			} else {
				if (!(ts->TileOffsets = ReadArray(lpHandle, lpIO, ds.value.lval + offset, (int)ds.len, ts->ByteOrder, (int)ds.type))) {
					SETERROR(IDS_MEMERR);
					return (IMG_NOMEM);
				}
			}
			break;
		case TILEWIDTH:
			ts->TileWidth = ds.value.ival.i1;
			if (ts->TileWidth <= 0) {
				SETERROR(IDS_BADMAN);
				return (IMG_NSUPPORT);
			}
			break;

		case TILELENGTH:
			ts->TileLength = ds.value.ival.i1;
			if (ts->TileLength <= 0) {
				SETERROR(IDS_BADMAN);
				return (IMG_NSUPPORT);
			}
			break;

			break;
		case TILEBYTECOUNTS:
			break;

		case STRIPBYTECOUNTS:
			break;

		case MINIMUMVALUE:
			ts->MinimumValue = ds.value.ival.i1;
			break;

		case MAXIMUMVALUE:
			ts->MaximumValue = ds.value.ival.i1;
			break;

		case PLANARCONFIG:
			if (ts->GrayBits > 1) {
				ts->PlanarConfiguration = ds.value.ival.i1;
				if (ts->PlanarConfiguration != 1 && ts->PlanarConfiguration != 2) {
					SETERROR(IDS_BADPLANES);
					return (IMG_NSUPPORT);
				}
			}
			break;

		case RESOLUTIONUNIT:
			ts->ResolutionUnit = ds.value.ival.i1;
			break;

		case XRESOLUTION:
			{
				HGLOBAL     hVal;
				LPLONG      lpl;

				hVal = ReadArray(lpHandle, lpIO, ds.value.lval + offset, 2, ts->ByteOrder, IFDLONG);
				if (hVal == NULL) {
					SETERROR(IDS_MEMERR);
					return (IMG_NOMEM);
				}
				lpl = (LPLONG) GlobalLock(hVal);
				ts->XRezNumerator = *lpl;
				ts->XRezDenominator = *(lpl + 1);
				if (!ts->XRezDenominator)
					ts->XRezDenominator = 1;
				GlobalUnlock(hVal);
				GlobalFree(hVal);
			}
			break;
		case YRESOLUTION:
			{
				HGLOBAL     hVal;
				LPLONG      lpl;

				hVal = ReadArray(lpHandle, lpIO, ds.value.lval + offset, 2, ts->ByteOrder, IFDLONG);
				if (hVal == NULL) {
					SETERROR(IDS_MEMERR);
					return (IMG_NOMEM);
				}
				lpl = (LPLONG) GlobalLock(hVal);
				ts->YRezNumerator = *lpl;
				ts->YRezDenominator = *(lpl + 1);
				if (!ts->YRezDenominator)
					ts->YRezDenominator = 1;
				GlobalUnlock(hVal);
				GlobalFree(hVal);
			}
			break;
		case COLORMAP:
			{
				int         nEntries;

				if( ts->ColorMap ) {
					GlobalFree( ts->ColorMap );
					ts->ColorMap = NULL;
				}

				nEntries = 2 << (ts->GrayBits - 1);
				ts->ColorMap = ReadArray(lpHandle, lpIO, ds.value.lval + offset, nEntries * 3, ts->ByteOrder, IFDSHORT);
			}
			break;
		case FILLORDER:
			ts->FillOrder = ds.value.ival.i1;
			break;
		case GROUP3OPTIONS:
			ts->Group3Options = ds.value.lval;
			// we don't currently support 2D encoded G3 data. Sorry.
			if ( (ts->Group3Options & 0x01) && 
				 ((ts->Compression == CCITT3) || (ts->Compression == FAXCCITT3)) ) {
				SETERROR(IDS_BADCOMPRESS);
				return (IMG_NSUPPORT);
			}
			break;
		case GROUP4OPTIONS:
			ts->Group4Options = ds.value.lval;
			break;
		case PREDICTOR:
			ts->Predictor = ds.value.ival.i1;
			if (ts->Predictor != 1 && ts->Predictor != 2) {
				SETERROR(IDS_BADPREDICTOR);
				return (IMG_NSUPPORT);
			}
			break;
		default:
			break;
	}

	return (IMG_OK);
}

static int  CreateDefaultTiffHead(TiffStruct FAR * ImageData)
{
	ImageData->FileType = 1;
	ImageData->ImageWidth = 100;
	ImageData->ImageLength = 100;
	ImageData->GrayBits = 1;
	ImageData->Compression = 1;
	ImageData->StripsPerImage = 1l;			// tiled image depends on this initialization
	ImageData->RowsPerStrip = 0xffffffffl;
	ImageData->WhiteValue = 0;
	ImageData->MinimumValue = 0;
	ImageData->MaximumValue = 1;
	ImageData->ScanType = 1;
	ImageData->FillOrder = 1;
	ImageData->XRezNumerator = 300;
	ImageData->XRezDenominator = 1;
	ImageData->YRezNumerator = 300;
	ImageData->YRezDenominator = 1;
	ImageData->Orientation = 1;
	ImageData->SamplesPerPixel = 1;
	ImageData->PlanarConfiguration = 1;
	ImageData->XPosition = 0;
	ImageData->YPosition = 0;
	ImageData->GrayResponseUnits = 2;
	ImageData->ColorResponseUnit = 2;
	ImageData->BytesLine = 1;
	ImageData->ResolutionUnit = 2;
	ImageData->PageNumber = 0;
	// ImageData->Pages = 0;	// can't set pages here - it's done in init_image
	ImageData->Group3Options = 0l;
	ImageData->Group4Options = 0l;
	ImageData->ColorMap = 0;
	ImageData->StripByteCounts = 0;
	ImageData->StripOffsets = NULL;
	ImageData->ColorMap = NULL;
		// tile stuff
	ImageData->TileWidth = 0;		// use TileWidth as flag to indicate this is tiled image or not
	ImageData->TileLength = 0;		//   because it must be filled in ProcessIfdEntry
	ImageData->TilesPerImage = 0;	
	ImageData->TileOffsets = NULL;
	ImageData->TileByteCounts = 0;

	return IMG_OK;
}

static int  ReadDirectory(LPVOID lpHandle, LPIMG_IO lpIO, LPLONG offset, DirStruct FAR * ds, int order)
{
	lpIO->seek(lpHandle, *offset, SEEK_SET);

	if (lpIO->read(lpHandle, ds, sizeof(DirStruct)) != sizeof(DirStruct)) {
		SETERROR(IDS_READERR);
		return (IMG_FILE_ERR);
	}
	if (order == MM) {
		BSWAP(ds->tag);
		BSWAP(ds->type);
		BSWAPL(ds->len);
		if (ds->type == IFDSHORT && ds->len == 1)
			BSWAP(ds->value.ival.i1);
		else
			BSWAPL(ds->value.lval);
	}
	*offset += sizeof(DirStruct);

	return (IMG_OK);
}

// Reads in an array of 'num' items of 'size' bytes from file specified by
// 'fhand' starting at offset 'offset'
// Note that the array created will contain only long offsets - short
// offsets are perverted to longs.
static HGLOBAL ReadArray(LPVOID lpHandle, LPIMG_IO lpIO, long offset, UINT num, int order, int size)
{
	ULONG       bufSize;
	UINT        readSize;
	HGLOBAL     hMem;
	LPSTR       buf;
	long        filePos;

	bufSize = num * sizeof(ULONG);	// allocate space for longs - ints are converted
	// if short offsets, we only want to read 1/2 buf

	readSize = size == IFDLONG ? (UINT) bufSize : (UINT) (bufSize / 2);

	if (!(hMem = GlobalAlloc(DLLHND, bufSize)))
		return (NULL);
	buf = GlobalLock(hMem);

	// before we can seek to offset location, we've got to save our current
	// file position so we can reset it later
	filePos = lpIO->seek(lpHandle, 0, SEEK_CUR);
	lpIO->seek(lpHandle, offset, SEEK_SET);

	if (lpIO->read(lpHandle, buf, readSize) != readSize) {
		GlobalUnlock(hMem);
		GlobalFree(hMem);
		return (NULL);
	}
	lpIO->seek(lpHandle, filePos, SEEK_SET);

	if (order == MM) {
		if (size == IFDLONG)
			swapla(buf, num);
		else
			swapa(buf, num);
	}
	// now convert the short array to a long array - if needed
	if (size == IFDSHORT)
		Short2LongArray((LPLONG) buf, num);

	GlobalUnlock(hMem);
	return (hMem);
}

// given an array of shorts that need to be longs and that's
// big enough to hold 'em, make the short array a long array
static void Short2LongArray(ULONG FAR * lptr, UINT elements)
{
	USHORT FAR *sptr;

	sptr = (USHORT FAR *) lptr + elements;	// point to last short element

	for (lptr += elements; elements--;) {
		*--lptr = *--sptr;		  // move each short to its new position as a long

	}
}

#if 0
void        swap(LPVOID a)
{
	*(USHORT FAR *) a = ((UCHAR FAR *) a)[0] << 8) | ((UCHAR FAR *) a)[1];
}

void        swapl(LPVOID a)
{
#define A(N) (((UCHAR FAR *)a)[N])
	register char tmp;

	tmp = A(0);
	A(0) = A(3);
	A(3) = tmp;
	tmp = A(1);
	A(1) = A(2);
	A(2) = tmp;
#undef A
}
#endif

// swap array of shorts
void        swapa(LPVOID buf, UINT count)
{
	register USHORT FAR *uptr;

	for (uptr = buf; count--; uptr++) {
		BSWAP(*uptr);
	}
}

// swap array of longs
void        swapla(LPVOID buf, UINT count)
{
	register ULONG FAR *lptr;

	for (lptr = buf; count--; lptr++) {
		BSWAPL(*lptr);
	}
}
