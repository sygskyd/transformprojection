////////////////////////////////////////////////////////////////////////////
//
// pcd.c
//
// Photo CD DIL Module
//
// ImageMan Image Processing Library
//
// Copyright (c) 1991-6 Data Techniques, Inc.
// All Rights Reserved
//
/////////////////////////////////////////////////////////////////////////////
// $History: PCD.C $
// 
// *****************  Version 4  *****************
// User: Johnd        Date: 10/02/97   Time: 3:14p
// Updated in $/ImageMan 16-32/DILS
// Fixed bug in ReadRows which caused a GPF when attempting to load a
// portion of a PCD image. Also fixed  a bug which caused black images
// because we werent closing our file handle in Close.
// 
// *****************  Version 3  *****************
// User: Johnd        Date: 9/16/96    Time: 6:12p
// Updated in $/ImageMan 16-32/DILS
// Fixed Bug 424 (Crash on ImgCopy)
// 
// *****************  Version 2  *****************
// User: Johnd        Date: 6/27/96    Time: 3:48p
// Updated in $/ImageMan 16-32/DILS
// Fixed Bug #361 (Crash w/OCX - GPF if no statusproc)
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 2/28/96    Time: 12:14p
// Created in $/ImageMan 16-32/DILS
// Initial Photo CD DIL Release

#include "internal.h"
#pragma hdrstop
#include <assert.h>
#include <string.h>
#include "pcd.h"     // stringtable defines

#ifdef WIN32
#define KPWIN32
#include "pcdlib.h"
#else

typedef struct {
        short left;
        short top;
        short right;
        short bottom;
} PCDrect;

#include "pcdlib16.h"
#endif

#define VERSION "PCD File Reader V1.0"
#define WBUFSIZE  1024
#define _LREADSIZE 32000L

#ifdef NDEBUG
#undef OutputDebugString
#define OutputDebugString(S) ((void)0)
#endif

typedef struct tagFileStruct {
	LPVOID		lpOpenInfo;		// info needed to reopen the image
	LPVOID		lpHandle;		// handle to file particulars for IMG_IO block functions
	LPIMG_IO		lpIO;		// IO block 
	long        offset;			// offset of image in file
	DWORD       rowSize;		// size of 1 row of the image
	UINT        row;			// # of row to start reading at
	WORD		nWidth;
	WORD		nHeight;

	PCDphotoHdl pcd;
}           FileStruct;


#ifdef WIN32
// Since we only need to store the error value on a per-thread basis, I'm
// just gonna place it in our DWORD TLS slot.

#ifdef WINGIS // so the file TLSByJHC.h was included into internal.h

#define SETERROR(E) bJHCTLSSetValue((DWORD)(E))
#define GETERROR    nJHCTLSGetValue()

#else

static DWORD TlsIndex;
#define 	(E) TlsSetValue(TlsIndex,(LPVOID)(DWORD)(E))
#define GETERROR    ((DWORD)TlsGetValue(TlsIndex))

#endif

#else
static int  nErrID;				  // id of latest error
#define SETERROR(E) nErrID = (E)
#define GETERROR    (nErrID)
#endif

static HINSTANCE pcdInstance;
#if DEBUG
static DEBUGPROC DebugStringOut;
#endif

#ifdef WIN32
// /////////////////////////////////////////////////////////////////////////
//
// DllMain
//
// This is the Win32 'DllEntryPoint'. It is similar in function to LibMain
// and the WEP in 16-bit land, but is much more thorough. LibMain is
// called whenever a process or thread attaches or detaches to the
// BMP DIL
//
// Entry:
// hinstDLL  - handle to the ImageMan DLL
// fdwReason - notification code (attach/detach)
// DLL_PROCESS_ATTACH
// DLL_PROCESS_DETACH
// DLL_THREAD_ATTACH
// DLL_THREAD_DETACH
// lpvReserved - NULL during dynamic process attach/detach, non-NULL
// during static attach/detach.
// Exit:
// DLL_PROCESS_ATTACH - TRUE for success, FALSE for failure
// all others         - return code ignored
// /////////////////////////////////////////////////////////////////////////

BOOL APIENTRY DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
	switch (fdwReason) {
 		// The DLL is attaching to a process, due to LoadLibrary.
 		case DLL_PROCESS_ATTACH:
			assert(!lpvReserved);  // must always be loaded via LoadLibrary!

			// OutputDebugString("IM2PCD: Process++\n");
			pcdInstance = hinstDLL;// save our identity
			// Allocate a "Thread Local Storage" (TLS) index right away.

#ifndef WINGIS // ++ Sygsky
			if ((TlsIndex = TlsAlloc()) == 0xFFFFFFFF)
				return (FALSE);
#else
			bJHCTLSClear();
#endif
		// Fall thru: Initialize the index for first thread.
		
 		// The attached process created a new thread.
		case DLL_THREAD_ATTACH:
#ifndef WINGIS
			OutputDebugString("IM2PCD: Thread++\n");
			// Initialize the TLS index for this thread... it's actually 'nErrID'
			if (!TlsSetValue(TlsIndex, (LPVOID) 0))
				return (FALSE);
#else
			nJHCTLSAlloc(); // Stamp this thread to be existed
#endif
			break;

 		// The thread of the attached process terminates.
		case DLL_THREAD_DETACH:
//			OutputDebugString("IM2PCD: Thread--\n");
#ifdef WINGIS
			bJHCTLSFree(); // Free this thread entry
#endif
			break;

 		// The DLL is detaching from a process, due to a call to FreeLibrary.
		case DLL_PROCESS_DETACH:
#ifndef WINGIS
			OutputDebugString("IM2PCD: Process--\n");
			// Release the TLS index.
			TlsFree(TlsIndex);
#else
			bJHCTLSClear();
#endif
			break;
	}

	//OutputDebugString("IM2PCD: OK\n");
	return (TRUE);

	UNREFERENCED_PARAMETER(lpvReserved);
}

#else // WIN32

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int WINAPI  LibMain(HANDLE hInstance, WORD wDataSeg, WORD wHeapSize, LPSTR lpCmdLine)
{
	if (!wHeapSize)
		return (0);					  // no heap indicates fatal error

#if DEBUG
	{
		HANDLE      hMod = GetModuleHandle("IMGMAN2.DLL");
		if (hMod)
			DebugStringOut = (DEBUGPROC) GetProcAddress(hMod, "DebugStringOut");
	}
#endif

	pcdInstance = hInstance;
	return (1);

	UNREFERENCED_PARAMETER(wDataSeg);
	UNREFERENCED_PARAMETER(lpCmdLine);
}
#endif // WIN32

void ProgressProc( short numerator, short denominator, long data )
{
	LPSTATUS lpStat;

	lpStat = (LPSTATUS)data;

	(*lpStat->lpfnStat)( lpStat->hImage, (int) numerator * 100 / denominator, lpStat->dwUserInfo );

}


// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
void WINAPI WEP(int bSystemExit)
{
	return;
	UNREFERENCED_PARAMETER(bSystemExit);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
LPSTR IMAPI IMDLget_ver(void)
{
	return ((LPSTR) (VERSION));
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLescape(HANDLE hImage, int nInCnt, LPVOID lpIn, LPVOID lpOut)
{
	return (IMG_NSUPPORT);
	UNREFERENCED_PARAMETER(hImage);
	UNREFERENCED_PARAMETER(nInCnt);
	UNREFERENCED_PARAMETER(lpIn);
	UNREFERENCED_PARAMETER(lpOut);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
BOOL IMAPI  IMDLverify_img(LPSTR lpVerfBuf, LPVOID lpHandle, LPIMG_IO lpIO)
{
	return 2;		// Maybe
	UNREFERENCED_PARAMETER(lpHandle);
	UNREFERENCED_PARAMETER(lpIO);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLinit_image(LPCSTR lpOpenInfo, LPHANDLE hFile, LPVOID lpHandle, long offset, long lLen, LPIMG_IO lpIO)
{
	FileStruct FAR *fs;
	PCDphotoHdl pcd;
	PCDstatus	pcdStatus;

//	DEBUGOUT(DILAPI, "\t%s-->init_image(%s,%x:%x,%d,%ld,%ld)", (LPSTR) __FILE__, (LPSTR) file, HIWORD(hFile), LOWORD(hFile), offset, lLen);

	pcdStatus = PCDopen( (char FAR *)lpOpenInfo, &pcd );

	if( pcdStatus != pcdSuccess ) {
		SETERROR(IDS_NOOPEN);
		return IMG_ERR;
	}

	// create new internal structure
	if (!(*hFile = GlobalAlloc(DLLHND, sizeof(FileStruct)))) {
		SETERROR(IDS_MEMERR);
		return (IMG_NOMEM);
	}
	// fill in the structure
	fs = (FileStruct FAR *) GlobalLock(*hFile);
	fs->lpOpenInfo = (LPVOID)lpOpenInfo;
	fs->lpHandle = lpHandle;
	fs->lpIO = lpIO;
	fs->offset = offset;
	// fs->imbedlen = lLen;
	fs->rowSize = 0;				  // these are not known 'till buildDIBhead

	fs->pcd = pcd;

	fs->row = 0;

	GlobalUnlock(*hFile);

	return (IMG_OK);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLset_ext(HGLOBAL hFile, LPSTR ext)
{
	DEBUGOUT(DILAPI, "\t%s-->set_ext(%d,%s)", (LPSTR) __FILE__, hFile, (LPSTR) ext);
	// no extensions to differentiate between - just return OK
	return (IMG_OK);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLclose_image(HGLOBAL hFile)
{
	FileStruct FAR *fs;

	DEBUGOUT(DILAPI, "\t%s-->close_image(%d)", (LPSTR) __FILE__, hFile);
	// check to see if file is still open - if it is, close it
	if ((fs = (FileStruct FAR *) GlobalLock(hFile)) == NULL) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}

	if (fs->lpHandle) {
		fs->lpIO->close(fs->lpHandle);
		fs->lpHandle = NULL;
	}

	GlobalUnlock(hFile);
	GlobalFree(hFile);
	return (IMG_OK);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLimg_name(HGLOBAL hFile, HANDLE FAR * lpHand)
{
	FileStruct FAR *fs;

	if (!(fs = (FileStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}
//	*lpHand = fs->fName;

	GlobalUnlock(hFile);
	return (IMG_OK);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLopen(HGLOBAL hFile, LPVOID lpHandle)
{
	FileStruct FAR *fs;
	PCDstatus	pcdStatus;
	int			retval = IMG_OK;

	if (!(fs = (FileStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}

	pcdStatus = PCDopen( (char FAR *)fs->lpOpenInfo, &fs->pcd );

	if( pcdStatus != pcdSuccess ) {
		SETERROR(IDS_NOOPEN);
		retval = IMG_ERR;
	}

	GlobalUnlock(hFile);
	return retval;
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLclose_keep(HANDLE hFile)
{
	FileStruct FAR *fs;

	DEBUGOUT(DILAPI, "\t%s-->close_keep(%d)", (LPSTR) __FILE__, hFile);
	if (!(fs = (FileStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}

	if (fs->lpHandle) {
		fs->lpIO->close(fs->lpHandle);
		fs->lpHandle = NULL;
	}

	if( fs->pcd ) {
		PCDclose( fs->pcd );
		fs->pcd = NULL;
	}

	GlobalUnlock(hFile);
	return (IMG_OK);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLread_rows(HGLOBAL hFile, LPSTR buf, UINT rows, UINT rowSize, UINT start, LPSTATUS lpStat)
{
	FileStruct FAR *fs;
	HPSTR       tmpBuf = (HPSTR) buf;
	PCDstatus	pcdStatus;
	int			retval = IMG_OK;
	PCDrect		pcdRect;

	DEBUGOUT(DILAPI, "\t%s-->%s(%d, %x:%x, %u, %u, %u, %x:%x)", (LPSTR) __FILE__, (LPSTR) "read_rows", hFile, HIWORD(buf), LOWORD(buf), rows, rowSize, start, HIWORD(lpStat), LOWORD(lpStat));
	if (!(fs = (FileStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);											
		return (IMG_INV_HAND);
	}

	if( lpStat->lpfnStat ) {
		PCDsetProgress( fs->pcd, ProgressProc, (long)lpStat );
	}
	
	pcdRect.left = start / 3;
	pcdRect.top = fs->row;
	pcdRect.right = pcdRect.left + rowSize / 3;
	pcdRect.bottom = fs->row+rows;

	pcdStatus = PCDgetBlock( fs->pcd, &pcdRect, tmpBuf, rowSize );
	
	GlobalUnlock(hFile);

	if( pcdStatus != pcdSuccess ) {
		SETERROR(IDS_READERR);
		retval = IMG_ERR;
	}

	return retval;
}


// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLset_row(HGLOBAL hFile, UINT row)
{
	FileStruct FAR *fs;

	DEBUGOUT(DILAPI, "\t%s-->set_row(%d,%d)", (LPSTR) __FILE__, hFile, row);
	if (!(fs = (FileStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}

	fs->row = row;

	GlobalUnlock(hFile);
	return (IMG_OK);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLbuildDIBhead(HGLOBAL hFile, LPINTERNALINFO lpii)
{
	LPBITMAPINFOHEADER lpbi;
	HANDLE hMem;
	FileStruct FAR *fs;
	PCDstatus	pcdStatus;
	PCDrect		rect;

	DEBUGOUT(DILAPI, "\t%s-->buildDIBhead(%d)", (LPSTR) __FILE__, hFile);
	if (!(fs = (FileStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}

	if ((hMem = GlobalAlloc(GHND,	sizeof(BITMAPINFOHEADER) )) == NULL) {
		SETERROR(IDS_MEMERR);
		return (IMG_NOMEM);
	}
	lpbi = (LPBITMAPINFOHEADER) GlobalLock(hMem);

	pcdStatus = PCDgetSize( fs->pcd, &rect );

	lpbi->biWidth = rect.right;
	lpbi->biHeight = rect.bottom;	

	fs->nWidth = rect.right;
	fs->nHeight = rect.bottom;

	// read in the header
	lpbi->biSize = sizeof(BITMAPINFOHEADER);
	lpbi->biPlanes = 1;
	lpbi->biBitCount = 24;
	lpbi->biCompression = BI_RGB;
	lpbi->biSizeImage = ROWSIZE(lpbi->biWidth, lpbi->biBitCount) * lpbi->biHeight;
	lpbi->biXPelsPerMeter = 0;
	lpbi->biYPelsPerMeter = 0;
	lpbi->biClrUsed = 0;
	lpbi->biClrImportant = 0;

	lpii->lOrgWid = lpbi->biWidth;	// set height&wid for Imgmgr

	lpii->lOrgHi = lpbi->biHeight;
	lpii->bFlags = 0;

	lpbi->biSizeImage = 0;
	lpbi->biCompression = BI_RGB;

	fs->rowSize = ROWSIZE(lpbi->biWidth, lpbi->biBitCount);

	lpii->lpbi = (LPBITMAPINFO) lpbi;

	GlobalUnlock(hFile);
	return (IMG_OK);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLprint(HGLOBAL hFile, HDC hPrnDC, LPRECT lpDest, LPRECT lpSrc)
{
	return (IMG_NOTAVAIL);
	UNREFERENCED_PARAMETER(hFile);
	UNREFERENCED_PARAMETER(hPrnDC);
	UNREFERENCED_PARAMETER(lpDest);
	UNREFERENCED_PARAMETER(lpSrc);
}


// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLrender(HGLOBAL hFile, HDC hDC, LPRECT lpDest, LPRECT lpSrc)
{
	return (IMG_ERR);
	UNREFERENCED_PARAMETER(hFile);
	UNREFERENCED_PARAMETER(hDC);
	UNREFERENCED_PARAMETER(lpDest);
	UNREFERENCED_PARAMETER(lpSrc);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLload_wmf(HANDLE hImage, HMETAFILE FAR * lpmeta, LPRECT lpbbox)
{
	return (IMG_ERR);
	UNREFERENCED_PARAMETER(hImage);
	UNREFERENCED_PARAMETER(lpmeta);
	UNREFERENCED_PARAMETER(lpbbox);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLget_page(HANDLE hImage, LPINT nPage)
{
	return (IMG_ERR);
	UNREFERENCED_PARAMETER(hImage);
	UNREFERENCED_PARAMETER(nPage);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLset_page(HANDLE hImage, int nPage)
{
	return (IMG_ERR);
	UNREFERENCED_PARAMETER(hImage);
	UNREFERENCED_PARAMETER(nPage);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLerror_string(LPSTR lpszBuf, int nMaxLen)
{
	LoadString(pcdInstance, GETERROR, lpszBuf, nMaxLen);
	return (IMG_OK);
}

#if DEBUG
void __cdecl FAR Debug(UINT wCategory, LPSTR lpFormat,...)
{
	va_list     vaArgs;

	if (DebugStringOut) {
		va_start(vaArgs, lpFormat);
		(*DebugStringOut) (wCategory, lpFormat, vaArgs);
		va_end(vaArgs);
	}
}
#else
void __cdecl FAR Debug(UINT wCategory, LPSTR lpFormat,...)
{
	UNREFERENCED_PARAMETER(wCategory);
	UNREFERENCED_PARAMETER(lpFormat);
}
#endif
