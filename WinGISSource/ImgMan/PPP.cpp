// PPP.cpp   Class wrapper for Protection Plus
// Implements Class CPPP
//

#define STRICT
#include "PPP.h"


//////////////////////////////////////////////////////////////
// Constructors / Destructors
//

CPPP::CPPP()
{
	m_hInst = NULL;
	m_Results = NULL;
	m_lfHandle = NULL;
	m_bCheckDate = FALSE;
	memset((void *)m_LicenseFileName, '\0', sizeof(m_LicenseFileName));
	memset((void *)m_LicenseFilePath, '\0', sizeof(m_LicenseFilePath));
}


CPPP::~CPPP()
{
	// Clean up, Clean up

	if (m_lfHandle)
	{
		// Update the last date/time used fields
		pp_upddate(m_lfHandle, 0);

		// Always close the handle so the memory resources are freed
		pp_lfclose(m_lfHandle);
	}
}


//////////////////////////////////////////////////////////////
// Public Functions
//
void CPPP::SetLicenseFileName(LPSTR lpName)
{
	if (lpName)
	{
#ifdef WIN32
		strncpy(m_LicenseFileName, lpName, sizeof(m_LicenseFileName));
#else
		_fstrncpy(m_LicenseFileName, lpName, sizeof(m_LicenseFileName));
#endif
	}

	return;
}

BOOL CPPP::SetHINSTANCE(long  hInst) 
{

	char	*pRet;
	long	lRet;

	m_hInst = (HINSTANCE)hInst;
	lRet = GetModuleFileName( (HMODULE)hInst, m_LicenseFilePath, sizeof(m_LicenseFilePath)); 
	m_LicenseFilePath[lRet] = '\0';
	pRet = strrchr(m_LicenseFilePath, '\\');
	pRet[1] = '\0';

	strncat(m_LicenseFilePath, m_LicenseFileName, sizeof(m_LicenseFilePath));
		
	return(DoesFileExist());
}

BOOL CPPP::SetLicenseFilePath(LPSTR lpPath)
{
	int		nLen;

#ifdef WIN32
		strncpy(m_LicenseFilePath, lpPath, sizeof(m_LicenseFilePath));
#else
		_fstrncpy(m_LicenseFilePath, lpPath, sizeof(m_LicenseFilePath));
#endif
	
	nLen = strlen(m_LicenseFilePath);
	if (m_LicenseFilePath[nLen - 1] != '\\') 
		strncat(m_LicenseFilePath, "\\\0", sizeof(m_LicenseFilePath));

	strncat(m_LicenseFilePath, m_LicenseFileName, sizeof(m_LicenseFilePath));

	return(DoesFileExist());
}

BOOL CPPP::IsLicenseFileInWindowsDir()
{
	UINT	unRet;

	unRet = GetWindowsDirectory(m_LicenseFilePath, sizeof(m_LicenseFilePath));
	m_LicenseFilePath[unRet] = '\0';
	if (m_LicenseFilePath[unRet - 1] != '\\') 
		strncat(m_LicenseFilePath, "\\\0", sizeof(m_LicenseFilePath));

	strncat(m_LicenseFilePath, m_LicenseFileName, sizeof(m_LicenseFilePath));

	return(DoesFileExist());
}

void CPPP::SetPassword(LPSTR lpPassword)
{
	// Password. Limit of 124 characters

	if (lpPassword)
	{
#ifdef WIN32
		strncpy(m_Password, lpPassword, sizeof(m_Password));
#else
		_fstrncpy(m_Password, lpPassword, sizeof(m_Password));
#endif
	}

	return;
}
	
void CPPP::SetCompNo(LONG lCompNo)
{
	m_compID = lCompNo;
	return;
}

LONG CPPP::GetCompNo(void)
{
	// Regarding the Third Parameter:  For Windows libraries, if a ZERO string ("0") is passed, 
	// the drive Windows is installed on will be used, otherwise the current drive will be used.
	return pp_compno(COMPNO_NETNAME, NULL, "0");
}

LONG CPPP::GetHDNo(void)
{
	// Regarding the Third Parameter:  For Windows libraries, if a ZERO string ("0") is passed, 
	// the drive Windows is installed on will be used, otherwise the current drive will be used.
	return pp_compno(COMPNO_HDSERIAL, NULL, "0");
}

BOOL CPPP::SetAll(long hInst, LPSTR lpLicFileName, LPSTR lpPassword, LONG lpCompNo)
{
	BOOL bRet = FALSE;


	SetCompNo(lpCompNo);
	SetPassword(lpPassword);
	SetLicenseFileName(lpLicFileName);

	if (hInst)
	{
		bRet = SetHINSTANCE(hInst);
	}
	else
	{
		bRet = IsLicenseFileInWindowsDir();
	}

	return bRet;
}

BOOL CPPP::IsLicensed()
{
	char	tmpStr[MAX_PATH];
	LONG	result;

	m_authorized = FALSE;

	if (!(m_lfHandle)) {
		if (!(LoadLicenseFile()))
		{
			return FALSE;
		}
	}

	// Let's see if this computer is authorized
	result = pp_copycheck( m_lfHandle, 0, m_compID );

	if( result == PP_SUCCESS )
	{

		if (result == PP_TRUE)
		{
			// So far, so good.  Let's make sure they didn't turn the clock back
			if ((pp_valdate(m_lfHandle) == PP_FALSE) && m_bCheckDate)
			{
				strncpy(m_ErrString, "Your clock has been turned back.  Please correct and re-run application", sizeof(m_ErrString));
			}
			else
			{
				// Licensed version set to go.
				m_authorized = TRUE;
			}
		}
		else
		{
			// Something went wrong
			pp_errorstr(result, tmpStr);
			wsprintf(m_ErrString, "License Status not enabled. Error: %d  %s", result, tmpStr);
		}

	} 
	else
	{
		// error with pp_copycheck() - let's see why
		pp_errorstr(result, tmpStr);
		wsprintf(m_ErrString, "Checking License file failed. Error: %d  %s", result, tmpStr);
	}

	return(m_authorized);
}

void CPPP::GetErrString(LPSTR lpErr, int nSize)
{
#ifdef WIN32
		strncpy(lpErr, m_ErrString, nSize);
#else
		_fstrncpy(lpErr, m_ErrString, nSize);
#endif
}

BOOL CPPP::GetVarChar(enumVarChar var_no, LPSTR buffer)
{
	BOOL bRet = TRUE;
	LONG lRet = NULL;

	if (!(m_lfHandle)) {
		if (!(LoadLicenseFile()))
		{
			return FALSE;
		}
	}

	lRet = pp_getvarchar(m_lfHandle, var_no, buffer);
	if (lRet != PP_SUCCESS)
	{
		char	tmpStr[100];
		pp_errorstr(lRet, tmpStr);
		wsprintf(m_ErrString, "Reading Character String in License file failed. Error: %d  %s", lRet, tmpStr);
		bRet = FALSE;
	}
	return bRet;
}

BOOL CPPP::GetVarDate(enumVarDate var_no, LPLONG month_hours, LPLONG day_minutes, LPLONG year_secs)
{
	BOOL bRet = TRUE;
	LONG lRet = NULL;

	if (!(m_lfHandle)) {
		if (!(LoadLicenseFile()))
		{
			return FALSE;
		}
	}

	lRet = pp_getvardate(m_lfHandle, var_no, month_hours, day_minutes, year_secs);
	if (lRet != PP_SUCCESS)
	{
		char	tmpStr[100];
		pp_errorstr(lRet, tmpStr);
		wsprintf(m_ErrString, "Reading Character String in License file failed. Error: %d  %s", lRet, tmpStr);
		bRet = FALSE;
	}
	return bRet;
}

BOOL CPPP::GetVarNum(enumVarNum var_no, LPLONG value)
{
	BOOL bRet = TRUE;
	LONG lRet = NULL;

	if (!(m_lfHandle)) {
		if (!(LoadLicenseFile()))
		{
			return FALSE;
		}
	}

	lRet = pp_getvarnum(m_lfHandle, var_no, value);
	if (lRet != PP_SUCCESS)
	{
		char	tmpStr[100];
		pp_errorstr(lRet, tmpStr);
		wsprintf(m_ErrString, "Reading Character String in License file failed. Error: %d  %s", lRet, tmpStr);
		bRet = FALSE;
	}
	return bRet;
}

void CPPP::CheckDate(BOOL bCheck)
{
	m_bCheckDate = bCheck;
}

//////////////////////////////////////////////////////////////
// Private Functions
//

BOOL	CPPP::DoesFileExist()
{
	BOOL				bRet = FALSE;

#ifdef WIN32
	WIN32_FIND_DATA		fData;
	HANDLE				hFind;

	hFind = FindFirstFile((LPCTSTR)m_LicenseFilePath, &fData);
	if (hFind != INVALID_HANDLE_VALUE)
	{
		bRet = TRUE;
		FindClose(hFind);
	}
#else
	POFSTRUCT		pof = new OFSTRUCT;
	HFILE		hf;

	hf = OpenFile((LPCSTR)m_LicenseFilePath, pof, OF_EXIST);
	if (hf != HFILE_ERROR)
		bRet = TRUE;

	delete pof;
#endif

	return(bRet);
}

BOOL CPPP::LoadLicenseFile()
{
	LONG	result;

	if (m_LicenseFilePath[0] == '\0')
	{
		strncpy(m_ErrString, "You have not specified a license file.", sizeof(m_ErrString));
		return FALSE;
	}

	// Initialize the PPP static library
	//pp_initlib( m_hInstance );

	// See if library responds as expected
	if( pp_libtest(1518238629) != 4178757 )
	{
		strncpy(m_ErrString, "Invalid License File Library.", sizeof(m_ErrString));
		return FALSE;
	}


	// Save the handle in m_lfHandle to be used throughout the application
	// when access license file related functions such as pp_copycheck()
	result = pp_lfopen( m_LicenseFilePath, NULL, LF_FILE, m_Password, &m_lfHandle );
	if( result != PP_SUCCESS )
	{
		char	tmpStr[MAX_PATH];

		m_lfHandle = NULL;

		if (result== 15)		// Error 15 happens if No license file exists
		{
			pp_errorstr(result, tmpStr);
			wsprintf(m_ErrString, "License File Doesn't exist. Error: %d  %s", result, tmpStr);
		}
		else 
		{
			pp_errorstr(result, tmpStr);
			wsprintf(m_ErrString, "Opening License File Failed. Error: %d  %s", result, tmpStr);
		}
		return FALSE;
	}
	return TRUE;
}