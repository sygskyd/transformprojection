/////////////////////////////////////////////////////////////////////////////
//
//    IMTWAIN.C
//
//    ImageMan TWAIN Scanner Support
//
//    ImageMan Image Processing Library
//
//    Copyright (c) 1996 Data Techniques, Inc.
//    All Rights Reserved
//
/////////////////////////////////////////////////////////////////////////////
// $History: twain.cpp $
// 
// *****************  Version 2  *****************
// User: Johnd        Date: 3/21/00    Time: 11:59a
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Changed type of AUTOFEED and FEEDERENABLED caps to BOOL instead of
// UINT16 since the Fujitsu TWAIN drivers complained. Also added
// ICAP_UNITS inches to the caps we set althought it should already be the
// default.
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 7/10/97    Time: 2:03p
// Created in $/imageman 16-32/dll-api level 2
// Initial Lvl 2 Checkin
// 
// *****************  Version 4  *****************
// User: Johnd        Date: 9/24/96    Time: 9:21a
// Updated in $/ImageMan 16-32/DLL
// Fixed Misc items
// 
// *****************  Version 2  *****************
// User: Johnd        Date: 9/10/96    Time: 11:46a
// Updated in $/ImageMan 16-32/SCANOCX
// Fixed Bug 415 (Kodak DC40/50 not working)
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 8/21/96    Time: 3:22p
// Created in $/imageman 16-32/scanocx
// Initial Checkin
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 2/19/96    Time: 10:26p
// Created in $/ImageMan 16-32/DLL
// Initial Release of New TWAIN Support


#include "internal.h"
#pragma hdrstop

#include <assert.h>
#include <string.h>

#include "windows.h"
#include "windowsx.h"
#include "imgman.h"
#include "imtwain.h"

#ifndef WIN32
#define EXPORT __export
#define HUGE	__huge
#else
#define EXPORT
#define HUGE
#endif

//------------ Constants and Macros

#define STATIC static
#define VALID_HANDLE 32
#define IsValidHandle(h) ((h!=0) && ((h) >= 32))

//------------ Global variables

STATIC int			iAvailable;			// TWAIN available: 0:unknown, -1:No, 1:Yes

											
extern HINSTANCE hInst; 			// who we is in this life... available to all!


//------------ Forward declarations

void TWAIN_NativeXferReady(HTWAINJOB lpJob, LPMSG pmsg);
HWND CreateProxyWindow(void);
unsigned short Intersect16(unsigned int wMask, unsigned int nItems, TW_UINT16 far *pItem);
unsigned BitCount(unsigned W);
int GetCompleteImage (HTWAINJOB lpJob);
void NEAR PASCAL CorrectImage( HANDLE hDIB );


int IMAPI ScanSelectSource(HWND hwnd)
{
	int fProxyWindow = FALSE;
	TW_IDENTITY		NewSourceId;
	LPTWAINJOB lpJob;
	int retval;


 	if (!hwnd || !IsWindow(hwnd)) {
 		hwnd = CreateProxyWindow();
 		if( hwnd ) 
 			fProxyWindow = TRUE;
 	}

	lpJob = ScanLowInitTwainJob( hwnd ); 

	if( !lpJob ) {
		if (fProxyWindow)
			DestroyWindow(hwnd);
		return 0;
	}

 	if (IsWindow(hwnd) && TWAIN_LoadSourceManager( lpJob ) && TWAIN_OpenSourceManager( lpJob ) ) {
		TWAIN_Mgr(lpJob, DG_CONTROL, DAT_IDENTITY, MSG_GETDEFAULT, &NewSourceId);
		TWAIN_Mgr(lpJob, DG_CONTROL, DAT_IDENTITY, MSG_USERSELECT, &NewSourceId);
	}

  TWAIN_CloseSourceManager( lpJob );
	TWAIN_UnloadSourceManager( lpJob );

	if (fProxyWindow)
		DestroyWindow(hwnd);

	retval = (lpJob->rc == TWRC_SUCCESS);

	GlobalFreePtr( lpJob );

	return retval;
} 


HANDLE IMAPI ScanAcquirePage(HWND hwnd, WORD wPixTypes)
{
	int fProxyWindow = FALSE;
	LPTWAINJOB lpJob;
	HANDLE hDib = NULL;

 	if (!IsWindow(hwnd)) {
 		// hwnd isn't a valid window handle - most likely NULL
 		hwnd = CreateProxyWindow();
 		if (hwnd) fProxyWindow = TRUE;
 	}

 	if (!IsWindow(hwnd)) {
		//TWAIN_ErrorBox("Unable to create proxy window");

	} else {

		lpJob = ScanLowInitTwainJob( hwnd );

		ScanLowAcquirePages( lpJob, 1, wPixTypes, TWAIN_SHOWUI, &hDib);

	  	if (fProxyWindow)
  	  	DestroyWindow(hwnd);

		ScanLowCloseJob( lpJob );
	}

	return hDib;
}

HTWAINJOB IMAPI ScanLowInitTwainJob( HWND hwnd )
{
	HTWAINJOB lpJob;

	lpJob = (LPTWAINJOB) GlobalAllocPtr( GHND, sizeof(TWAINJOB) );

	if( lpJob ) {
		lpJob->AppId.Id = 0;						// init to 0, but Source Manager will assign real value
	  	lpJob->AppId.Version.MajorNum = 5;
	  	lpJob->AppId.Version.MinorNum = 0;
	  	lpJob->AppId.Version.Language = TWLG_USA;
	  	lpJob->AppId.Version.Country  = TWCY_USA;
	  	lstrcpy (lpJob->AppId.Version.Info,  "V 5.0");

	  	lpJob->AppId.ProtocolMajor =    TWON_PROTOCOLMAJOR;
	  	lpJob->AppId.ProtocolMinor =    TWON_PROTOCOLMINOR;
	  	lpJob->AppId.SupportedGroups =  DG_IMAGE | DG_CONTROL;
	  	lstrcpy (lpJob->AppId.Manufacturer,  "Data Techniques, Inc.");
	  	lstrcpy (lpJob->AppId.ProductFamily, "ImageMan");
	  	lstrcpy (lpJob->AppId.ProductName,   "ImageMan");
		
		lpJob->nState = 1;

		lpJob->dcLayout.Frame.Left.Whole = 0;
		lpJob->dcLayout.Frame.Top.Whole = 0;
		lpJob->dcLayout.Frame.Bottom.Whole = 0;
		lpJob->dcLayout.Frame.Right.Whole = 0;

		lpJob->nBrightness = 0;
		lpJob->nContrast = 0;

		lpJob->nPages = 1;
		lpJob->nPgCount = 0;
		lpJob->nResolution = 0;
		lpJob->SourceDevice[0] = '\0';

		lpJob->hwnd = hwnd;
	}

	return lpJob;
}

void IMAPI ScanLowRegisterApp(	
	LPTWAINJOB	lpJob,
	int		nMajorNum, int nMinorNum,	// major and incremental revision of application. E.g.
													// for version 2.1, nMajorNum == 2 and nMinorNum == 1
	int		nLanguage,						// language of this version (use TWLG_xxx from TWAIN.H)
	int		nCountry,						// country of this version (use TWCY_xxx from TWAIN.H)
	LPSTR	lpszVersion,						// version info string e.g. "1.0b3 Beta release"
	LPSTR	lpszMfg,								// name of manufacturer/developer e.g. "Crazbat Software"
	LPSTR	lpszFamily,							// product family e.g. "BitStomper"
	LPSTR	lpszProduct)						// specific product e.g. "BitStomper Deluxe Pro"
{

	lpJob->AppId.Id = 0;						
  lpJob->AppId.Version.MajorNum = nMajorNum;
  lpJob->AppId.Version.MinorNum = nMinorNum;
  lpJob->AppId.Version.Language = nLanguage;
  lpJob->AppId.Version.Country  = nCountry;
  lstrcpy (lpJob->AppId.Version.Info,  lpszVersion);

  lpJob->AppId.ProtocolMajor =    TWON_PROTOCOLMAJOR;
  lpJob->AppId.ProtocolMinor =    TWON_PROTOCOLMINOR;
  lpJob->AppId.SupportedGroups =  DG_IMAGE | DG_CONTROL;
  lstrcpy (lpJob->AppId.Manufacturer,  lpszMfg);
  lstrcpy (lpJob->AppId.ProductFamily, lpszFamily);
  lstrcpy (lpJob->AppId.ProductName,   lpszProduct);
} 


int IMAPI ScanLowAcquirePages( HTWAINJOB lpJob, int nPages, WORD wPixTypes, int nFlags, HANDLE FAR *lphDIB )
{

	lpJob->bShowUI = nFlags & TWAIN_SHOWUI;
	lpJob->bUseADF = nFlags & TWAIN_USEADF;
	lpJob->nPages = nPages;

	if( TWAIN_LoadSourceManager(lpJob) && TWAIN_OpenSourceManager(lpJob) && TWAIN_OpenDefaultSource(lpJob) && \
		TWAIN_NegotiateXferCount(lpJob) && TWAIN_SetCaps(lpJob ) && TWAIN_NegotiatePixelTypes(lpJob, wPixTypes) && \
		TWAIN_EnableSource(lpJob) ) {
			EnableWindow(lpJob->hwnd, FALSE);

			TWAIN_ModalEventLoop(lpJob);

			EnableWindow(lpJob->hwnd, TRUE);
	}

	if( lphDIB )
		*lphDIB = lpJob->hDib;

	return 1;
}

int IMAPI ScanLowCloseJob( HTWAINJOB lpJob )
{
	TWAIN_DisableSource( lpJob );
	TWAIN_CloseSource( lpJob );	
  TWAIN_CloseSourceManager( lpJob );
  TWAIN_UnloadSourceManager( lpJob );

	GlobalFreePtr( lpJob );

	return 1;
}

int IMAPI ScanIsTWAINAvailable(void)
// return 1 if TWAIN services are available, 0 if 'TWAIN-less' system
{
	LPTWAINJOB 	lpJob;

	if (iAvailable == 0) {
		lpJob = (HTWAINJOB) GlobalAllocPtr( GHND, sizeof(TWAINJOB) );

		if (TWAIN_LoadSourceManager(lpJob)) {
			iAvailable = 1;
			TWAIN_UnloadSourceManager(lpJob);
		} else {
			iAvailable = -1;
		}

		GlobalFreePtr( lpJob );
	}

	return (iAvailable > 0);
}

void IMAPI ScanLowSetScanArea( HTWAINJOB lpJob, float Left, float Top, float Right, float Bottom )
{
	lpJob->dcLayout.Frame.Left.Whole = (int) Left;
	lpJob->dcLayout.Frame.Left.Frac =  (int)((Left - (int)Left) * 65536);

	lpJob->dcLayout.Frame.Top.Whole = (int) Top;
	lpJob->dcLayout.Frame.Top.Frac = (int)((Top - (int)Top)* 65536);

	lpJob->dcLayout.Frame.Right.Whole = (int)Right;
	lpJob->dcLayout.Frame.Right.Frac =  (int)((Right - (int)Right)* 65536);

	lpJob->dcLayout.Frame.Bottom.Whole = (int)Bottom;
	lpJob->dcLayout.Frame.Bottom.Frac =  (int)((Bottom - (int)Bottom) * 65536);
}

void IMAPI ScanLowSetBrightnessContrast( HTWAINJOB lpJob, WORD nBrightness, WORD nContrast )
{
	lpJob->nBrightness = nBrightness;
	lpJob->nContrast = nContrast;
}

void IMAPI ScanLowSetResolution( HTWAINJOB lpJob, WORD nResolution )
{
	lpJob->nResolution = nResolution;
}
 
int FAR PASCAL TWAIN_State( HTWAINJOB lpJob)
{
	return lpJob->nState;
} 

void IMAPI ScanLowSetScanDevice( HTWAINJOB lpJob, LPCSTR lpDeviceName )
{
	if( lpJob )
		_fstrcpy( lpJob->SourceDevice, lpDeviceName );
}

int FAR PASCAL  TWAIN_LoadSourceManager(HTWAINJOB lpJob)
{
	char		szSMDir[128];
	int			cc;
	OFSTRUCT	of;

	if (lpJob->nState >= 2) return TRUE;			// DSM already loaded

	GetWindowsDirectory(szSMDir, sizeof(szSMDir));
	cc = lstrlen(szSMDir);
	if (cc && szSMDir[cc-1] != ':') {
		lstrcat(szSMDir, "\\");
	}
#ifdef WIN32
	lstrcat(szSMDir, "TWAIN_32.DLL");			
#else
	lstrcat(szSMDir, "TWAIN.DLL");
#endif
	if (OpenFile(szSMDir, &of, OF_EXIST) != -1) {
		lpJob->hDSMLib = LoadLibrary(szSMDir);
	} else {
		lpJob->hDSMLib = NULL;
	}
	if (IsValidHandle((int)lpJob->hDSMLib)) {
		lpJob->pDSM_Entry = (DSMENTRYPROC) GetProcAddress(lpJob->hDSMLib, MAKEINTRESOURCE(1));
		if (lpJob->pDSM_Entry) {
			iAvailable = 1;
			lpJob->nState = 2;
			SendMessage( lpJob->hwnd, WM_IMTWAIN, lpJob->nState, (LPARAM) lpJob );
		} else {
			FreeLibrary(lpJob->hDSMLib);
			lpJob->hDSMLib = NULL;
		}
	} else {
		lpJob->pDSM_Entry = NULL;
	}

	return (lpJob->nState >= 2);
}


int FAR PASCAL  TWAIN_OpenSourceManager(HTWAINJOB lpJob)
{
	TW_INT32 hwnd32 = (TW_INT32)lpJob->hwnd;

	if (TWAIN_LoadSourceManager(lpJob) &&
		TWAIN_Mgr(lpJob, DG_CONTROL, DAT_PARENT, MSG_OPENDSM, &hwnd32)) {
		lpJob->nState = 3;
		SendMessage( lpJob->hwnd, WM_IMTWAIN, lpJob->nState, (LPARAM) lpJob );
	}

	return (lpJob->nState >= 3);
}


int FAR PASCAL  TWAIN_OpenDefaultSource( HTWAINJOB lpJob)
{
	if (lpJob->nState != 3) return FALSE;

	// open the system default source
	if( lpJob->SourceDevice[0] )
		_fstrcpy( lpJob->SourceId.ProductName, lpJob->SourceDevice );
	else
		lpJob->SourceId.ProductName[0] = '\0';

	lpJob->SourceId.Id = 0;

	if (TWAIN_Mgr(lpJob, DG_CONTROL, DAT_IDENTITY, MSG_OPENDS, &lpJob->SourceId)) {
		lpJob->nState = 4;
		SendMessage( lpJob->hwnd, WM_IMTWAIN, lpJob->nState, (LPARAM) lpJob );
	}

	return (lpJob->nState == 4);
}


int FAR PASCAL  TWAIN_EnableSource(HTWAINJOB lpJob)
{
	if (lpJob->nState != 4) 
		return FALSE;

	lpJob->twUI.ShowUI = lpJob->bShowUI;
	lpJob->twUI.hParent = (TW_HANDLE)lpJob->hwnd;
	if (TWAIN_DS(lpJob, DG_CONTROL, DAT_USERINTERFACE, MSG_ENABLEDS, &lpJob->twUI)) {
		lpJob->nState = 5;
		SendMessage( lpJob->hwnd, WM_IMTWAIN, lpJob->nState, (LPARAM) lpJob );
		// note, source will set twUI.ModalUI.
	}
	return (lpJob->nState == 5);
}


int FAR PASCAL  TWAIN_DisableSource(HTWAINJOB lpJob)
{
	if (lpJob->nState == 5 ) {
		TWAIN_DS(lpJob, DG_CONTROL, DAT_USERINTERFACE, MSG_DISABLEDS, &lpJob->twUI);
		lpJob->nState = 4;
		SendMessage( lpJob->hwnd, WM_IMTWAIN, lpJob->nState, (LPARAM) lpJob );
	}
	return (lpJob->nState <= 4);
}


int FAR PASCAL  TWAIN_CloseSource(HTWAINJOB lpJob)
{
	lpJob->rc = TWRC_SUCCESS;

	if (lpJob->nState == 5) 
		TWAIN_DisableSource( lpJob );

	if (lpJob->nState == 4 &&
		TWAIN_Mgr(lpJob, DG_CONTROL, DAT_IDENTITY, MSG_CLOSEDS, &lpJob->SourceId)) {
		lpJob->nState = 3;
		SendMessage( lpJob->hwnd, WM_IMTWAIN, lpJob->nState, (LPARAM) lpJob );
	}
	return (lpJob->nState <= 3);
} 


int FAR PASCAL  TWAIN_CloseSourceManager(HTWAINJOB lpJob)
{
	TW_INT32 hwnd32 = (TW_INT32)lpJob->hwnd;

	lpJob->rc = TWRC_SUCCESS;

	if (TWAIN_CloseSource( lpJob ) &&
		TWAIN_Mgr(lpJob, DG_CONTROL, DAT_PARENT, MSG_CLOSEDSM, &hwnd32)) {
		lpJob->nState = 2;
		SendMessage( lpJob->hwnd, WM_IMTWAIN, lpJob->nState, (LPARAM) lpJob );
	}
	return (lpJob->nState <= 2);
}


int FAR PASCAL  TWAIN_UnloadSourceManager(HTWAINJOB lpJob)
{
	if (lpJob->nState == 2) {
	
		if (lpJob->hDSMLib) {
			FreeLibrary(lpJob->hDSMLib);
			lpJob->hDSMLib = NULL;
		}
		lpJob->pDSM_Entry = NULL;
		lpJob->nState = 1;
		SendMessage( lpJob->hwnd, WM_IMTWAIN, lpJob->nState, (LPARAM) lpJob );
	}
	return (lpJob->nState == 1);
} 



void FAR PASCAL  TWAIN_ModalEventLoop(HTWAINJOB lpJob)
{
	MSG msg;

	while ((lpJob->nState >= 5) && !lpJob->hDib && GetMessage((LPMSG)&msg, NULL, 0, 0)) {

		if (!TWAIN_MessageHook (lpJob, (LPMSG)&msg)) {

			TranslateMessage ((LPMSG)&msg);
			DispatchMessage ((LPMSG)&msg);
		}
	} // while
}


int FAR PASCAL  TWAIN_MessageHook(HTWAINJOB lpJob, LPMSG lpmsg)
// returns TRUE if msg processed by TWAIN (source)
{
	int   bProcessed = FALSE;

	if (lpJob->nState >= 5) {
		// source enabled
		TW_EVENT	twEvent;

		twEvent.pEvent = (TW_MEMREF)lpmsg;
		twEvent.TWMessage = MSG_NULL;

		// see if source wants to process (eat) the message
		TWAIN_DS(lpJob, DG_CONTROL, DAT_EVENT, MSG_PROCESSEVENT, &twEvent);

		bProcessed = (lpJob->rc == TWRC_DSEVENT);

		switch (twEvent.TWMessage) {
			case MSG_XFERREADY:
				lpJob->nState = 6;
				TWAIN_NativeXferReady(lpJob, lpmsg);
				break;

			case MSG_CLOSEDSREQ:
				TWAIN_DisableSource(lpJob);
				break;

			case MSG_NULL:
				// no message returned from DS
				break;
		}
  }

	return bProcessed;
} 


void TWAIN_NativeXferReady(HTWAINJOB lpJob, LPMSG pmsg)
{
	TW_PENDINGXFERS   dcPendingXfer;
	
	dcPendingXfer.Count = lpJob->nPages;

	while( lpJob->nPages > 0) {
	
		if( !GetCompleteImage( lpJob ) ) {
			lpJob->nPages = 0;
			break;
		}
			
		// Required for proper 6<->7 state transitions

		TWAIN_DS(lpJob, DG_CONTROL, DAT_PENDINGXFERS, MSG_ENDXFER, (TW_MEMREF)&dcPendingXfer);
		lpJob->nState = dcPendingXfer.Count ? 6 : 5;

		// if ...Xfer.Count > 0 then loop until it's zero or nMaxPages == 0
		// otherwise just don't call the DCTerminate() function so the user 
		// can do more scans

		lpJob->nPages--;
		lpJob->nPgCount++;

		if( dcPendingXfer.Count == 0 )		// Don't call GetCompleteImage unless
			break;									// an image is waiting

	}

	if( lpJob->nPages > 0 ) {
	//	TransInfo.Status = 0;
	//	TransInfo.Event = SOURCECLOSED;
	//	Debug(1, "Sending MSG_CLOSEDREQ since out of paper in Transfer loop and MaxPages > 0" );
			
	//	SendMessage (hWnd, PM_XFERDONE, NULL, (LONG)(TRANSFERINFO far *)&TransInfo );
	}

	if( dcPendingXfer.Count == 0 && !lpJob->bShowUI )
		lpJob->nPages = 0;

	if( lpJob->nPages <= 0) {
		TWAIN_AbortAllPendingXfers(lpJob);
		//Debug(1, "Shutting down the Source since nMaxPages <= 0" );
		//DCTerminate();		// Shut down Source

	}
} 

int GetCompleteImage (HTWAINJOB lpJob)
{
	TW_UINT32   hBitMap = 0;

	TWAIN_DS(lpJob, DG_IMAGE, DAT_IMAGENATIVEXFER, MSG_GET, (TW_MEMREF)&hBitMap);
	
	switch (lpJob->rc) {
		case TWRC_XFERDONE:
			lpJob->nState = 7;
					
			lpJob->hDib = (HBITMAP)hBitMap;
			CorrectImage( lpJob->hDib );
			SendMessage( lpJob->hwnd, WM_IMSCAN, (unsigned int)lpJob->hDib, (LPARAM) lpJob );
			return 1;			
		
		case TWRC_CANCEL:		// the user canceled or wants to rescan the image
//				lpJob->nState = 7;
//				return 0;
										
		case TWRC_FAILURE:	// An error ocurred
		default:
				lpJob->nState = 5;
				TWAIN_DisableSource(lpJob);
				return 0;
	}
}


int FAR PASCAL  TWAIN_AbortAllPendingXfers(HTWAINJOB lpJob)
{
	if (lpJob->nState == 7 &&
		TWAIN_DS(lpJob, DG_CONTROL, DAT_PENDINGXFERS, MSG_ENDXFER, &lpJob->pendingXfers)) {
		lpJob->nState = lpJob->pendingXfers.Count ? 6 : 5;
	}
	if (lpJob->nState == 6 &&
		TWAIN_DS(lpJob, DG_CONTROL, DAT_PENDINGXFERS, MSG_RESET, &lpJob->pendingXfers)) {
		lpJob->nState = 5;
	}
	return (lpJob->nState <= 5);
}

int FAR PASCAL  TWAIN_NegotiateXferCount(HTWAINJOB lpJob)
{
	TW_CAPABILITY	cap;
 	pTW_ONEVALUE  	pv;
	int				fSuccess = FALSE;

	cap.Cap = CAP_XFERCOUNT;
 	cap.ConType = TWON_ONEVALUE;

	if (cap.hContainer = GlobalAlloc(GHND, sizeof(TW_ONEVALUE)))
   	{
    	pv = (pTW_ONEVALUE)GlobalLock(cap.hContainer);
		pv->ItemType = TWTY_INT16;
		pv->Item = lpJob->nPages;
    	GlobalUnlock(cap.hContainer);

		fSuccess = TWAIN_DS(lpJob, DG_CONTROL, DAT_CAPABILITY, MSG_SET, &cap);
		GlobalFree(cap.hContainer);
	}
	
	return 1;
}

int FAR PASCAL  TWAIN_SetCaps(HTWAINJOB lpJob)
{
	TW_CAPABILITY	dcCap;

	if (lpJob->nState != 4) 
		return FALSE;

	// Set Default Units to Inches - This s/b the default already
	dcCap.Cap = ICAP_UNITS;
	BuildUpOneValue( &dcCap, TWTY_UINT16, (TW_UINT16) TWUN_INCHES );
	TWAIN_DS(lpJob, DG_CONTROL, DAT_CAPABILITY, MSG_SET, (TW_MEMREF)&dcCap );

	GlobalFree(dcCap.hContainer);

	lpJob->dcLayout.FrameNumber = TWON_DONTCARE32;
	lpJob->dcLayout.PageNumber = TWON_DONTCARE32;
	lpJob->dcLayout.DocumentNumber = TWON_DONTCARE32;

	if( lpJob->dcLayout.Frame.Left.Whole ||
				 lpJob->dcLayout.Frame.Top.Whole ||
				 lpJob->dcLayout.Frame.Right.Whole ||
				 lpJob->dcLayout.Frame.Bottom.Whole  ) {			    

				 TWAIN_DS(lpJob, DG_IMAGE, DAT_IMAGELAYOUT, MSG_SET, (TW_MEMREF)&lpJob->dcLayout );
	}

	dcCap.Cap = ICAP_BRIGHTNESS;
	BuildUpOneValue( &dcCap, TWTY_FIX32, (TW_UINT32)lpJob->nBrightness );
	TWAIN_DS(lpJob, DG_CONTROL, DAT_CAPABILITY, MSG_SET, (TW_MEMREF)&dcCap );
	GlobalFree(dcCap.hContainer);

	
	dcCap.Cap = ICAP_CONTRAST;
	BuildUpOneValue( &dcCap, TWTY_FIX32, (TW_UINT32)lpJob->nContrast );
	TWAIN_DS(lpJob, DG_CONTROL, DAT_CAPABILITY, MSG_SET, (TW_MEMREF)&dcCap );
	GlobalFree(dcCap.hContainer);


	if( lpJob->bUseADF ) {
		dcCap.Cap = CAP_FEEDERENABLED;
		BuildUpOneValue( &dcCap, TWTY_BOOL, TRUE );
		TWAIN_DS(lpJob, DG_CONTROL, DAT_CAPABILITY, MSG_SET, (TW_MEMREF)&dcCap );
		GlobalFree(dcCap.hContainer);

		dcCap.Cap = CAP_AUTOFEED;
		BuildUpOneValue( &dcCap, TWTY_BOOL, TRUE );
		TWAIN_DS(lpJob, DG_CONTROL, DAT_CAPABILITY, MSG_SET, (TW_MEMREF)&dcCap );
		GlobalFree(dcCap.hContainer);
	}

	if( lpJob->nResolution != 0 ) {

		dcCap.Cap = ICAP_YRESOLUTION;
		BuildUpOneValue( &dcCap, TWTY_FIX32, (TW_UINT32)lpJob->nResolution);
		TWAIN_DS(lpJob, DG_CONTROL, DAT_CAPABILITY, MSG_SET, (TW_MEMREF)&dcCap );

		GlobalFree(dcCap.hContainer);

		dcCap.Cap = ICAP_XRESOLUTION;
		BuildUpOneValue( &dcCap, TWTY_FIX32, (TW_UINT32)lpJob->nResolution);
		TWAIN_DS(lpJob, DG_CONTROL, DAT_CAPABILITY, MSG_SET, (TW_MEMREF)&dcCap );

		GlobalFree(dcCap.hContainer);
		
	}


	return TRUE;
}

int FAR PASCAL TWAIN_NegotiatePixelTypes(HTWAINJOB lpJob, unsigned wPixType)
{
	TW_CAPABILITY 		cap;
	int					fSuccess = FALSE;
	int					nPixVal;

	if (TWAIN_ANYTYPE == wPixType) {
		return TRUE;			// that was easy!
	}

	if( wPixType == TWAIN_BW )
		nPixVal = 0;
	else
	if( wPixType == TWAIN_GRAY )
		nPixVal = 1;
	else
	if( wPixType == TWAIN_RGB )
		nPixVal = 2;
	else
		nPixVal = 3;		// Palette Color

	cap.Cap = ICAP_PIXELTYPE;
	BuildUpOneValue( &cap, TWTY_UINT16, nPixVal );

	fSuccess = TWAIN_DS(lpJob, DG_CONTROL, DAT_CAPABILITY, MSG_SET, (TW_MEMREF)&cap);

	return 1;
}



//-------------------------- The primitive functions


int FAR PASCAL  TWAIN_DS(HTWAINJOB lpJob, unsigned long dg, unsigned dat, unsigned msg, void FAR *pd)
// Call the current source with a triplet
{
	assert(lpJob->nState >= 4);
	lpJob->rc = TWRC_FAILURE;
	if (lpJob->pDSM_Entry) {
		lpJob->rc = (*lpJob->pDSM_Entry)(&lpJob->AppId, &lpJob->SourceId, dg,dat,msg, (TW_MEMREF)pd);
	}
	return (lpJob->rc == TWRC_SUCCESS);
}



int FAR PASCAL  TWAIN_Mgr(HTWAINJOB lpJob, unsigned long dg, unsigned dat, unsigned msg, void FAR *pd)
// Call the Source Manager with a triplet
{
	lpJob->rc = TWRC_FAILURE;
	if (lpJob->pDSM_Entry) {
		lpJob->rc = (*lpJob->pDSM_Entry)(&lpJob->AppId, NULL, dg, dat, msg, (TW_MEMREF)pd);
	}
	return (lpJob->rc == TWRC_SUCCESS);
}



unsigned FAR PASCAL TWAIN_GetResultCode(HTWAINJOB lpJob)
{
	return lpJob->rc;
} 



unsigned FAR PASCAL  TWAIN_GetConditionCode(HTWAINJOB lpJob)
{
	TW_STATUS	twStatus;

	if (lpJob->nState >= 4) {
		// get source status if open
		TWAIN_DS(lpJob, DG_CONTROL, DAT_STATUS, MSG_GET, (TW_MEMREF)&twStatus);
	} else if (lpJob->nState == 3) {
		// otherwise get source manager status
		TWAIN_Mgr(lpJob, DG_CONTROL, DAT_STATUS, MSG_GET, (TW_MEMREF)&twStatus);
	} else {
		// nothing open, not a good time to get condition code!
		return TWCC_SEQERROR;
	}
	if (lpJob->rc == TWRC_SUCCESS) {
		return twStatus.ConditionCode;
	} else {
		return TWCC_BUMMER;			// what can I say. 
	}
} 


//------------ Private functions


HWND CreateProxyWindow(void)
{
	HWND hwnd;
	hwnd = CreateWindow("STATIC",						// class
						"Acquire Proxy",				// title
						WS_POPUPWINDOW | WS_VISIBLE,	// style	WS_VISIBLE
						CW_USEDEFAULT, CW_USEDEFAULT,	// x, y
						CW_USEDEFAULT, CW_USEDEFAULT,	// width, height
						HWND_DESKTOP,					// parent window
						NULL,							// hmenu
						hInst,
						NULL);							// lpvparam
	return hwnd;
} 

unsigned BitCount(unsigned W)
{
	unsigned n = 0;

	while (W) {
		n += (W & 1);
		W >>= 1;
	} // while
	return n;
} // BitCount


unsigned short Intersect16(unsigned int wMask, unsigned int nItems, TW_UINT16 far *pItem)
{
	unsigned	short wSet;
	unsigned	short i;

	// In wSet, construct set of available items.
	// Note that items that cannot be represented in wMask are also
	// unrepresentable in wSet so are implicitly discarded
	for (i = wSet = 0 ; i < nItems; i++) {
		wSet |= 1 << pItem[i];
	} // for

	// Discard anything in wMask that isn't in wSet
	wMask &= wSet;

	// Re-fill the item table with intersection set
	for (i = nItems = 0; wMask ; wMask>>=1,i++) {
		if (wMask & 1) {
			pItem[nItems++] = i;
		}
	} // for

	return nItems;
}

VOID BuildUpOneValue (pTW_CAPABILITY pData, TW_UINT16 ItemType, TW_UINT32 Item)
{
    pTW_ONEVALUE pOneValue;

    if ((pData->hContainer = (TW_HANDLE)GlobalAlloc(GMEM_MOVEABLE, sizeof(TW_ONEVALUE))) != NULL){
        // tell APP the ConType you are returning
        pData->ConType = TWON_ONEVALUE;
        if ((pOneValue = (pTW_ONEVALUE)GlobalLock(pData->hContainer)) != NULL){
            pOneValue->ItemType = ItemType;    // TWTY_XXXX
            pOneValue->Item     = Item;        // TWPT_XXXX...
            GlobalUnlock(pData->hContainer);
        }
    }

    return;
}

void NEAR PASCAL CorrectImage( HANDLE hDIB )
{
	unsigned char HUGE * lpDIB;
	LPBITMAPINFO lpbi;
	long lLen;

	lpDIB = (unsigned char HUGE *)GlobalLock( hDIB );

	if( !lpDIB )
		return;

	lpbi = (LPBITMAPINFO) lpDIB;
	
	if( lpbi->bmiHeader.biBitCount != 1 ) {
		GlobalUnlock( hDIB );
		return;
	}

	// Its B/W, now to see if its inverted

	if( lpbi->bmiColors[1].rgbRed == 0 && lpbi->bmiColors[1].rgbGreen == 0 && lpbi->bmiColors[1].rgbBlue == 0 ) {
		// Invert the Bits

		lLen = (lpbi->bmiHeader.biWidth + 31 ) / 32 * 4;		// Calc row size
		lLen *= lpbi->bmiHeader.biHeight;						

		lpDIB += sizeof(BITMAPINFOHEADER) + sizeof(RGBQUAD) * 2;

		while( lLen-- ) {
			*lpDIB = ~(*lpDIB);
			lpDIB++;
		}

		// Swap the entries in the Palette
		lpbi->bmiColors[0].rgbRed = 0;
		lpbi->bmiColors[0].rgbGreen = 0;
		lpbi->bmiColors[0].rgbBlue = 0;

		lpbi->bmiColors[1].rgbRed = 0xff;
		lpbi->bmiColors[1].rgbGreen = 0xff;
		lpbi->bmiColors[1].rgbBlue = 0xff;
	}

	GlobalUnlock( hDIB );
}
