/////////////////////////////////////////////////////////////////////////////
//
// optblk.h
//
//
// Option Block includes
//
// ImageMan Image Processing Library
//
// Copyright (c) 1991-1995 Data Techniques, Inc.
// All Rights Reserved
//
/////////////////////////////////////////////////////////////////////////////
// $History: optblk.h $
// 
// *****************  Version 2  *****************
// User: Sygsky       Date: 21-MAY-20001  Time: 16:16p of Moscow
// Updated in $/ImageMan 16-32/DLL-API Level 2
// SentinelIsOK added
// 
// *****************  Version 2  *****************
// User: Johnd        Date: 10/10/97   Time: 5:54p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Made these functions "C" functions since we also link this module with
// the DIL/DELS that arent CPP
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 7/10/97    Time: 2:04p
// Created in $/imageman 16-32/dll-api level 2
// Initial Lvl 2 Checkin
// 
// *****************  Version 2  *****************
// User: Johnd        Date: 9/19/96    Time: 12:04p
// Updated in $/ImageMan 16-32/DLL
// Fixed Bug 403 (Long FileName issues)
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 1/18/95    Time: 3:59p
// Created in $/ImageMan 16-32/DLL
// ImageMan 4.0 Beta 1

#ifdef __cplusplus
extern "C" {
#endif

typedef struct tagOPTBLK {
	char  sentinel[4];   // always == 'THK' (extra char to keep long aligned! -CAR)
	int   nBlkSize;      // size of block
	int   nUsed;         // # of bytes used in block
	char  text[2];       // text of option block (goes beyond structure!)
} OPTBLK,FAR *LPOPTBLK;

bool FAR NEAR SentinelIsOK(LPOPTBLK pOptBlk); //++Sygsky: code bum on sentinel check :o()
bool FAR NEAR cSentinelIsOK(char *sentinel); //++Sygsky: code bum on sentinel check :o()
HANDLE FAR OptBlkCreate(LPSTR);
HANDLE FAR OptBlkAdd(HANDLE, LPCSTR, LPCSTR);
int FAR OptBlkDel(HANDLE);
int FAR OptBlkGet(HANDLE, LPCSTR, LPSTR);
int FAR OptBlkGetLength(HANDLE, LPCSTR, LPINT);

typedef int (FAR PASCAL *LPFNOPTBLKGET)(HANDLE, LPSTR, LPSTR);

#ifdef __cplusplus
}
#endif
