/////////////////////////////////////////////////////////////////////////////
//
//    imgio.h
//
//    ImageMan structured IO module
//
//    ImageMan Image Processing Library
//
//    Copyright (c) 1991-5 Data Techniques, Inc.
//    All Rights Reserved
//
/////////////////////////////////////////////////////////////////////////////
//$Header: /ImageMan 16-32/DILS/IMGIO.H 4     3/20/98 9:02a Johnd $
//$History: IMGIO.H $
// 
// *****************  Version 4  *****************
// User: Johnd        Date: 3/20/98    Time: 9:02a
// Updated in $/ImageMan 16-32/DILS
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 7/10/97    Time: 2:04p
// Created in $/imageman 16-32/dll-api level 2
// Initial Lvl 2 Checkin
// 
// *****************  Version 3  *****************
// User: Timk         Date: 7/27/95    Time: 12:27p
// Updated in $/ImageMan 16-32/DILS
// Memory writing now correctly expands memory blocks when needed.
// 
// *****************  Version 2  *****************
// User: Timk         Date: 7/25/95    Time: 11:01p
// Updated in $/ImageMan 16-32/DILS
// Added writing using IO function blocks.
// 
// *****************  Version 1  *****************
// User: Timk         Date: 6/30/95    Time: 12:06p
// Created in $/ImageMan 16-32/DILS
// Header file for imgio.c

#ifdef __cplusplus
extern "C" {
#endif

// standard file block I/O functions 
int fileio_open(LPVOID lpIn, unsigned uFlags, LPVOID FAR *lplpHandle);
int fileio_close(LPVOID lpHandle);
DWORD fileio_read(LPVOID lpHandle, LPVOID hpBuf, DWORD dwBytes);
DWORD fileio_seek(LPVOID lpHandle, LONG lOffset, WORD nOrigin);
DWORD fileio_write(LPVOID lpHandle, LPVOID hpBuf, DWORD dwBytes);
int fileio_error(LPVOID lpHandle, LPSTR lpErrBuf);
int fileio_getchar(LPVOID lpHandle);

// memory block I/O functions 
typedef struct tagMEM_IO {
	HPSTR		 	hpMem;		//the input memory block
	DWORD			dwSize;		//size of the mem block
	DWORD			dwUsed;		//char which defines the end of file
	HPSTR			hpCur;		//pointer to current byte in memory block
	HANDLE		hMem;			//global handle to the memory block we're writing into
	WORD			wFlags;		//TRUE if we're writing to this "file"
} MEM_IO, FAR *LPMEM_IO;

#define MEMIO_WRITING	0x01	//set if we're writing to the "file"
#define MEMIO_EXISTS	0x02	//set if the mem block contains a "file" already
#define MEMIO_NOALLOC	0x04	//set if we want the memio funcs to use the 
										//passed-in MEM_IO pointer. This allows us to 
										//retrieve the writing information at the end
										//of a write operation. (ie, by setting this bit
										//we can retrieve the GlobalReAlloced memory
										//handle without resorting to even weirder tricks)

int memio_open(LPVOID lpIn, unsigned uFlags, LPVOID FAR *lplpHandle);
int memio_close(LPVOID lpHandle);
DWORD memio_read(LPVOID lpHandle, LPVOID hpBuf, DWORD dwBytes);
DWORD memio_seek(LPVOID lpHandle, LONG lOffset, WORD nOrigin);
DWORD memio_write(LPVOID lpHandle, LPVOID hpBuf, DWORD dwBytes);
int memio_error(LPVOID lpHandle, LPSTR lpErrBuf);
int memio_getchar(LPVOID lpHandle);


#ifdef __cplusplus
}
#endif



