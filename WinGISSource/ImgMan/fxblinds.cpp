/////////////////////////////////////////////////////////////////////////////
//
// fxblinds.c
//
// ImageMan Effects Module: Venetian Blinds
// Exports:
//   doBlinds
//
// Version 1.0
// Copyright (c) 1996 Data Techniques, Inc.
// Copyright (c) 1996 Chris Roueche
// All Rights Reserved
//
/////////////////////////////////////////////////////////////////////////////
// $History: fxblinds.cpp $
// 
// *****************  Version 3  *****************
// User: Johnd        Date: 4/10/98    Time: 5:06p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Chgd Error Returns to be consistent with other ImageMan Defines
// 
// *****************  Version 2  *****************
// User: Ericj        Date: 4/09/98    Time: 9:52a
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Cleaned up the Clipping Region when done with the effect.
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 7/10/97    Time: 2:02p
// Created in $/imageman 16-32/dll-api level 2
// Initial Lvl 2 Checkin
//

#define STRICT
#define WIN32_LEAN_AND_MEAN
#define NOSERVICE
#include <windows.h>
#include <windowsx.h>
#include "imgman.h"
#include "fxparam.h"

#define DEFAULT_SLATSIZE 10
#define DEFAULT_STEP     1

// Curtain-specific storage can be found in fxparam.h:tag_Blinds

/////////////////////////////////////////////////////////////////////////////
//
/////////////////////////////////////////////////////////////////////////////

static void NEAR initblinds(PEFFECTBLK peb)
{
	int step,count,j;
	RECT r;
	SIZE size;
	HRGN hrgn1,hrgn2;

	if ((j = HIWORD(peb->dwExtra)) <= 0) j = DEFAULT_SLATSIZE;
	if ((step = LOWORD(peb->dwExtra)) <= 0) step = DEFAULT_STEP;

	size.cx = size.cy = 0;
	peb->blind.step.cx = peb->blind.step.cy = 0;

	r = peb->blind.bbox = peb->rDst;
	switch (peb->nEffect) {
	case IMFX_VBLIND_LEFT:
		count = r.right-r.left;
		r.left = r.right-step;
		size.cx = -j;
		peb->blind.step.cx = -step;
		break;
	case IMFX_VBLIND_RIGHT:
		count = r.right-r.left;
		r.right = r.left+step;
		size.cx = j;
		peb->blind.step.cx = step;
		break;
	case IMFX_VBLIND_UP:
		count = r.bottom-r.top;
		r.top = r.bottom-step;
		size.cy = -j;
		peb->blind.step.cy = -step;
		break;
	case IMFX_VBLIND_DOWN:
		count = r.bottom-r.top;
		r.bottom = r.top+step;
		size.cy = j;
		peb->blind.step.cy = step;
		break;
	}

	// Create initial image pass-thru region by combining all slats into
	// one region. This should prove more efficient than using multiple
	// bitblts since regions are fairly quick while a bitblt may take a
	// while to setup (esp. with DIBs).
	hrgn1 = CreateRectRgn(0,0,0,0);
	hrgn2 = CreateRectRgnIndirect(&r);
	for (count = (count+j-1)/j; --count >= 0; ) {
		CombineRgn(hrgn1,hrgn1,hrgn2,RGN_OR);
		OffsetRgn(hrgn2,size.cx,size.cy);
	}
	peb->blind.hThruRgn = hrgn1;
	DeleteRgn(hrgn2);

	if (!(peb->dwFlags&IMDF_VBLIND_IMGEXPOSE)) {
		OffsetRect(&peb->rDst,-size.cx,-size.cy);
	}

	peb->nStepsTotal = j+step-1;
	peb->dwExtra = step;
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
//
// doBlinds
//
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

int doBlinds(HDC hDC,PEFFECTBLK peb)
{
	HRGN hrgn;

	if (INITIALIZE(peb)) initblinds(peb);
	if (CLEANUP(peb)) {
		if (peb->blind.hThruRgn) DeleteRgn(peb->blind.hThruRgn);
		SelectClipRgn(hDC, NULL);
		return (IMG_OK);
	}

	peb->nStepsDone += (UINT)peb->dwExtra;

	// slide background image if required
	if (!(peb->dwFlags&IMDF_CURTAIN_OVERBKGND)) {
		// invert the thru region to create a background region
		hrgn = CreateRectRgnIndirect(&peb->blind.bbox);
		CombineRgn(hrgn,hrgn,peb->blind.hThruRgn,RGN_DIFF);
		if (peb->hClipRgn) CombineRgn(hrgn,hrgn,peb->hClipRgn,RGN_AND);
		SelectClipRgn(hDC,hrgn);
		ScrollDC(hDC,peb->blind.step.cx,peb->blind.step.cy,
			&peb->blind.bbox,&peb->blind.bbox,NULL,NULL);
		SelectClipRgn(hDC,NULL);
		DeleteRgn(hrgn);
	}

	// if image is sliding into place, move into this iteration's position
	if (!(peb->dwFlags&IMDF_VBLIND_IMGEXPOSE)) {
		if (peb->nStepsDone < peb->nStepsTotal) {
			OffsetRect(&peb->rDst,peb->blind.step.cx,peb->blind.step.cy);
		}
		else peb->rDst = peb->blind.bbox; // don't go past final spot
	}

	// setup the pass region and transfer image thru it
	if (peb->hClipRgn) {
		hrgn = CreateRectRgn(0,0,0,0);
		CombineRgn(hrgn,peb->hClipRgn,peb->blind.hThruRgn,RGN_AND);
		SelectClipRgn(hDC,hrgn);
		DeleteRgn(hrgn);
	}
	else {
		SelectClipRgn(hDC,peb->blind.hThruRgn);
	}
	clipblt(hDC,peb,&peb->rDst);

	if (peb->nStepsDone < peb->nStepsTotal) {
		// Create next iteration's pass filter
		hrgn = CreateRectRgn(0,0,0,0);
		CombineRgn(hrgn,peb->blind.hThruRgn,NULL,RGN_COPY);
		OffsetRgn(hrgn,peb->blind.step.cx,peb->blind.step.cy);
		CombineRgn(peb->blind.hThruRgn,peb->blind.hThruRgn,hrgn,RGN_OR);
		DeleteRgn(hrgn);
	}

	return (IMG_OK);
}

