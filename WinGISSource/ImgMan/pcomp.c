#include <stdio.h>
#include <string.h>
#include <stdlib.h>


#define TRUE 1
#define FALSE 0

char	buf1[100], buf2[100];
long	nRow, nCol, nWidth;

int fgetnum(FILE *f, char *buf)
{
	int	c;

	while ((c = fgetc(f)) != EOF && !isdigit(c));
	while (isdigit(c) && (c != EOF)) {
		*buf++ = c;
		c = fgetc(f);
	}

	*buf = 0;
	if (c == EOF) return EOF;
	else return 0;
}

int main(int argc, char **argv)
{
	FILE *f1, *f2;
	int	i, bQuit=FALSE;

	if (argc != 4) {
		printf("\r\n\r\nUsage: pcomp file1 file2 width\r\n\r\n");
		exit(1);
	}

	nWidth = (long)atoi(argv[3]);
	if (!nWidth) {
		printf("\r\nInvalid width!\r\n");
		exit(1);
	}

	f1 = fopen(argv[1], "r");
	f2 = fopen(argv[2], "r");

	if ((f1 != NULL) && (f2 != NULL)) {	
		while (!bQuit) {
			for (i=0; i<nWidth && bQuit==FALSE; i++) {
//				if (!fgets(buf1, 100, f1)) bQuit = TRUE;
//				if (!fgets(buf2, 100, f2)) bQuit = TRUE;
				if (fgetnum(f1, buf1)) bQuit = TRUE;
				if (fgetnum(f2, buf2)) bQuit = TRUE;
				if (strcmp(buf1, buf2)) {
					printf("\nRow %ld, Col %d (%s, %s)\r\n", nRow, i, buf1, buf2);
					bQuit = TRUE;
				}           
				else printf("%4s ", buf1);
			}
			nRow++;                   
			printf("\n");
		}
	}
	else {
		printf("\r\nError opening files!\r\n");
	}


	printf("First %ld rows identical\r\n", nRow);
	fcloseall();
	return 0;
}
