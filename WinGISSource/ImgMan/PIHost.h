/////////////////////////////////////////////////////////////////////////////
//
//    PIHost.h
//
//    ImageMan Plug-In Host Main Module Header File
//
//    ImageMan Image Processing Library
//
//    Copyright (c) 1998 Data Techniques, Inc.
//    All Rights Reserved
//	 
/////////////////////////////////////////////////////////////////////////////
//$Header: /ImageMan 16-32/DLL-API Level 2/PIHost.h 3     4/24/98 11:02a Ericw $
//
//$History: PIHost.h $
// 
// *****************  Version 3  *****************
// User: Ericw        Date: 4/24/98    Time: 11:02a
// Updated in $/ImageMan 16-32/DLL-API Level 2
// New function ImgPlugInInvokeEx.
// 
// *****************  Version 2  *****************
// User: Ericw        Date: 4/08/98    Time: 3:57p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Fixed the comment header - no code.
// 

#ifdef __cplusplus
extern "C"
{
#endif

typedef long (IMAPI * PIIMINITPROC     )(LPSTR) ;
typedef long (IMAPI * PIIMLOADPROC     )(LPSTR) ;
typedef long (IMAPI * PIIMABOUTPROC    )(HWND,LPSTR) ;
typedef long (IMAPI * PIIMINVOKEPROC   )(HANDLE,PDIB,HWND,RECT,LPBYTE*,BOOL) ;
typedef long (IMAPI * PIIMHOSTENUMPROC )(PIIMENUMCALLBACKPROC, LONG, LONG) ;
typedef long (IMAPI * PIIMSETSTATUSPROC)(STATUSPROC,LONG,DWORD) ;


long IMAPI PIIMInit          ( LPSTR lpPlugInDir  ) ;
long IMAPI PIIMLoadSpecific  ( LPSTR lpPlugInName ) ;
long IMAPI PIIMPlugInAbout   ( HWND hOwner, LPSTR lpPlugInName ) ;
long IMAPI PIIMInvokePlugIn  ( HANDLE hImage, PDIB pDIB, HWND hOwner, RECT rect, BOOL bUI ) ;
long IMAPI PIIMEnumerate     ( PIIMENUMCALLBACKPROC enumProc, LONG lPIType, LONG lParam ) ;
long IMAPI PIIMSetStatusProc ( STATUSPROC lpStat, LONG lInterval, DWORD dwUser ) ;

#ifdef __cplusplus
}
#endif
