/////////////////////////////////////////////////////////////////////////////
//
// jpeg.c
//
// JPEG file reader
//
// ImageMan Image Processing Library
//
// Copyright (c) 1991-5 Data Techniques, Inc.
// All Rights Reserved
//
// ///////////////////////////////////////////////////////////////////////////
// $History: JPEG.C $
// 
// *****************  Version 12  *****************
// User: Johnd        Date: 6/29/00    Time: 4:53p
// Updated in $/ImageMan 16-32/DILS
// Fixed Issue 256 (Incorrect Resolution read from file)
// 
// *****************  Version 11  *****************
// User: Ericj        Date: 11/03/98   Time: 10:30a
// Updated in $/ImageMan 16-32/DILS
// Opening corrupt images from a memory block got stuck
// in endless loop.
// 
// *****************  Version 11  *****************
// User: Ericj        Date: 10/23/98   Time: 2:04p
// Updated in $/ImageMan 16-32/DILS
// Error Reading Memory Based Images if Corrupt.
// 
// *****************  Version 10  *****************
// User: Johnd        Date: 4/10/98    Time: 5:56p
// Updated in $/ImageMan 16-32/DILS
// Fixed JPG xDens/yDens issue
// 
// *****************  Version 9  *****************
// User: Johnd        Date: 3/20/98    Time: 12:01p
// Updated in $/ImageMan 16-32/DILS
// Added support for getting the xDens/yDens fields
// 
// *****************  Version 8  *****************
// User: Timk         Date: 8/06/97    Time: 1:53p
// Updated in $/ImageMan 16-32/DILS
// Fixed 16-bit bugs with wide JPEG images.
// 
// *****************  Version 7  *****************
// User: Timk         Date: 7/31/97    Time: 3:18p
// Updated in $/ImageMan 16-32/DILS
// Fixed overflow bug when reading wide images in 16-bit JPEG reader.
// 
// *****************  Version 6  *****************
// User: Timk         Date: 3/05/96    Time: 11:36a
// Updated in $/ImageMan 16-32/DILS
// One of those picture-scanners (from Storm Technologies) produces JPEGs
// that have two APP0 markers. The second one doesn't say JFIF in it, so
// we were aborting. Fixed it so if the first APP0 marker is OK, we ignore
// all others.
// 
// *****************  Version 5  *****************
// User: Timk         Date: 1/29/96    Time: 1:13a
// Updated in $/ImageMan 16-32/DILS
// Fixed bugs w/reading portions of an image (set_row problems). Also,
// changed global dct array to local for reentrancy purposes.
// 
// *****************  Version 4  *****************
// User: Timk         Date: 6/30/95    Time: 12:04p
// Updated in $/ImageMan 16-32/DILS
// Part 1 of massive changes for read/write using function pointer blocks.
// 
// *****************  Version 3  *****************
// User: Timk         Date: 6/09/95    Time: 11:19a
// Updated in $/ImageMan 16-32/DILS
// Updated to use faster look-ahead table for Huffman code extraction. As
// a by-product of this change I had to alter the input stream to be wider
// (16/32 bits) instead of 8.
// 
// *****************  Version 2  *****************
// User: Timk         Date: 6/05/95    Time: 9:20a
// Updated in $/ImageMan 16-32/DILS
// Changed to use cool new fast reverse-DCT code.
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 1/17/95    Time: 6:36p
// Created in $/ImageMan 16-32/DILS
// ImageMan 4.0 Beta 1

#include "internal.h"
#pragma hdrstop
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "jpg.h"     // stringtable defines

#define BITHOLDERSIZE    8  // # of bits we're reading at a time
#define MAXCODESIZE      15 // maximum # of bits in a code
#define SAMP_22_11_11    1
#define SAMP_21_11_11    2
#define SAMP_NONSTANDARD 0

#ifdef WIN32
// inline swaps for 32-bit compiles
#define BSWAP(S)  (S) = (WORD)(((UINT)((WORD)(S))<<8)|(((UINT)((WORD)(S)))>>8))
#else
// swaps for 16-bit compiles, very fast if "inline intrinsics" optimization is on
#define BSWAP(S) (S) = _rotl((S),8)
#endif


// These error defines taken from im2jpg.c
#ifdef WIN32

#ifdef WINGIS // so the file TLSByJHC.h was included
#define SETERROR(E) bJHCTLSSetValue((DWORD)(E))
#define GETERROR    nJHCTLSGetValue()

#else

extern DWORD TlsIndex;			  // Thread local storage index: for error code
#define SETERROR(E) TlsSetValue(TlsIndex,(LPVOID)(DWORD)(E))
#define GETERROR    ((DWORD)TlsGetValue(TlsIndex))
#endif


#else
extern int  nErrID;				  // id of latest error
#define SETERROR(E) nErrID = (E)
#define GETERROR    (nErrID)
#endif

#define BLOCKOFFSET(lp,h,v,rowsize) (lp+h*DCTSIZE+v*DCTSIZE*rowsize)

static void processRestart(FileStruct FAR *);
static void SubSamp2to1(HPSTR, HPSTR, LONG, LONG);
static void SubSamp_V11_H21(HPSTR, HPSTR, LONG, LONG);
static int __inline receive(FileStruct FAR *, int);
static int  GetHuffCode(FileStruct FAR *, LPHUFFTABLE);


/* ZAG[i] is the natural-order position of the i'th element of zigzag order. If the incoming data
 * is corrupted, huff_decode_mcu could attempt to reference values beyond the end of the array.  To
 * avoid a wild store, we put some extra zeroes after the real entries. */

static UINT const ZAG[64 + 16] = {
	0, 1, 8, 16, 9, 2, 3, 10,
	17, 24, 32, 25, 18, 11, 4, 5,
	12, 19, 26, 33, 40, 48, 41, 34,
	27, 20, 13, 6, 7, 14, 21, 28,
	35, 42, 49, 56, 57, 50, 43, 36,
	29, 22, 15, 23, 30, 37, 44, 51,
	58, 59, 52, 45, 38, 31, 39, 46,
	53, 60, 61, 54, 47, 55, 62, 63,
	0, 0, 0, 0, 0, 0, 0, 0,		  /* extra entries in case k>63 below */
	0, 0, 0, 0, 0, 0, 0, 0
};

static JSAMPLE dct_clamp[MAXJSAMPLE*3];	//used for clamping DCT calculations

/* Figure F.12: extend sign bit; used in "receive()". */

#define huff_EXTEND(V,N)  ((V) < extend_test[N] ? (V)+extend_offset[N] : (V))

static int const extend_test[MAXCODESIZE + 1] = {	/* entry n is 2**(n-1) */
	0, 0x0001, 0x0002, 0x0004, 0x0008, 0x0010, 0x0020, 0x0040, 0x0080,
	0x0100, 0x0200, 0x0400, 0x0800, 0x1000, 0x2000, 0x4000
};

static int const extend_offset[MAXCODESIZE + 1] = {	/* entry n is (-1 << n) + 1 */
	0, ((-1) << 1) + 1, ((-1) << 2) + 1, ((-1) << 3) + 1, ((-1) << 4) + 1,
	((-1) << 5) + 1, ((-1) << 6) + 1, ((-1) << 7) + 1, ((-1) << 8) + 1,
	((-1) << 9) + 1, ((-1) << 10) + 1, ((-1) << 11) + 1, ((-1) << 12) + 1,
	((-1) << 13) + 1, ((-1) << 14) + 1, ((-1) << 15) + 1
};

static int const maskused[MAXCODESIZE + 1] = {	/* entry n is (-1 >> (16-n)) */
	0x0000, 0x0001, 0x0003, 0x0007,
	0x000f, 0x001f, 0x003f, 0x007f,
	0x00ff, 0x01ff, 0x03ff, 0x07ff,
	0x0fff, 0x1fff, 0x3fff, 0x7fff
};

// returns actual number of rows processed
int         OutputActualPixels(HPSTR lpOutBuf, LPJPEGINFO lpjpeg, UINT nRowSize,
								             UINT nRowsLeft, UINT nStartRow, UINT nStartPix, UINT nPixLen)
{
	UINT j;      
	LONG nMCURowSize;

	nMCURowSize = lpjpeg->nMCUsPerRow * lpjpeg->nMaxHorz * DCTSIZE;
	j = lpjpeg->nMaxVert * DCTSIZE;

	// if we have to skip rows, we need to fiddle with nRowsLeft so we don't
	// over-read from our current buffer...
	if (nStartRow)
		nRowsLeft = min(nRowsLeft, j - nStartRow);

	// Process only rows we have (j) or need; both j and nRowsLeft get result.
	if (nRowsLeft > j)
		nRowsLeft = j;
	else
		j = nRowsLeft;

	if (lpjpeg->nComponents == 1) {
		HPSTR       lp1 = lpjpeg->npComponents[0].lpUpSampsBase + nMCURowSize * nStartRow + nStartPix;

		// Find minimum scanline size outside the loops
		nPixLen = min(nPixLen, lpjpeg->nWidth);

		// if there's only one component, just copy each scanline directly
		while (j--) {
			hmemcpy(lpOutBuf, lp1, nPixLen);
			lpOutBuf -= nRowSize;  // move to next DIB scanline

			lp1 += nMCURowSize;	  // move to next source line

		}
	} 
	else {
		LONG         i;
		UINT        Y, Cb, Cr;
		HPSTR       lp1, lp2, lp3;// pointers to the separate component samples

		// process all three source components
		i = nMCURowSize * nStartRow + nStartPix / 3;
		lp1 = lpjpeg->npComponents[0].lpUpSampsBase + i;	// Y

		lp2 = lpjpeg->npComponents[1].lpUpSampsBase + i;	// Cb

		lp3 = lpjpeg->npComponents[2].lpUpSampsBase + i;	// Cr

		// Find minimum scanline size outside the loops
		if ((nPixLen /= 3) > lpjpeg->nWidth)
			nPixLen = lpjpeg->nWidth;

		nMCURowSize -= nPixLen;	  // delta which'll get us to next chunk o' data

		nRowSize += nPixLen * 3;  // compensate for pixels we fill

		while (j--) {
			{
				for (i = nPixLen; --i >= 0;) {
					Y = *lp1++;
					Cb = *lp2++;
					Cr = *lp3++;

					// Cb = Cr = 128;	// uncomment this for grayscale-only
					// indexing is faster (use when huge-ism isn't relevent)
					((HPSTR) lpOutBuf)[0] = rgb_clamp[Y + RGBCLAMP_OFFSET + rgb_b[Cb]];	// Blue

					((HPSTR) lpOutBuf)[1] = rgb_clamp[Y + RGBCLAMP_OFFSET + rgb_gb[Cb] + rgb_gr[Cr]];	// Green

					((HPSTR) lpOutBuf)[2] = rgb_clamp[Y + RGBCLAMP_OFFSET + rgb_r[Cr]];	// Red

					((HPSTR) lpOutBuf) += 3;
				}
			}
			lp1 += (LONG)nMCURowSize;
			lp2 += (LONG)nMCURowSize;
			lp3 += (LONG)nMCURowSize;
			lpOutBuf -= (LONG)nRowSize;  // move to next DIB scanline

		}
	}
	return (nRowsLeft);
}

//
// this function up-samples each component in the given JPEGINFO struct.
//
int         UpSample(LPJPEGINFO lpjpeg)
{
	NPCOMPONENTINFO npComp;
	int         i;
	int         nVertSamp, nHorzSamp;
	long         nRowSize = (LONG)lpjpeg->nMCUsPerRow * lpjpeg->nMaxHorz * DCTSIZE;
	long         cpysize = DCTSIZE2 * (LONG)lpjpeg->nMCUsPerRow * lpjpeg->nMaxHorz * lpjpeg->nMaxVert;

	//
	// for each component in the image, we'll move through it's MCU-buffer
	// and place each block in its corresponding output location.
	//
	for (i = 0; i < lpjpeg->nComponents; i++) {

		npComp = &lpjpeg->npComponents[i];
		nHorzSamp = npComp->sampfactor >> 4;
		nVertSamp = npComp->sampfactor & 0x0f;

		if (nHorzSamp == lpjpeg->nMaxHorz && nVertSamp == lpjpeg->nMaxVert) {
			//
			// No need to upsample, just copy it to lpUpSampsBase
			//
			hmemcpy(npComp->lpUpSampsBase, npComp->lpSampsBase, cpysize);
		} else if (nHorzSamp == 1 && nVertSamp == 1 && lpjpeg->nMaxHorz == 2) {
			if (lpjpeg->nMaxVert == 1) {
				SubSamp_V11_H21(npComp->lpSampsBase, npComp->lpUpSampsBase, DCTSIZE * lpjpeg->nMCUsPerRow, nRowSize);
			} else if (lpjpeg->nMaxVert == 2) {
				SubSamp2to1(npComp->lpSampsBase, npComp->lpUpSampsBase, DCTSIZE * lpjpeg->nMCUsPerRow, nRowSize);
			}
		}
	}
	return (IMG_OK);
}

//
// This function automatically handles 2:1 vertical and 2:1 horizontal
// subsampling.
//
void        SubSamp2to1(HPSTR lpSampsBase, HPSTR lpUpSampsBase, LONG nSampsRowSize, LONG nRowSize)
{
	long         j, k;
	HPSTR       lpCur, lpCur2;

	// start at bottom and move upwards
	lpCur = (HPSTR)lpUpSampsBase;

	// keep from doing nasty multiplies within loops
	for (j = DCTSIZE; j--;) {
		lpCur2 = lpCur + nRowSize;
		for (k = nSampsRowSize; --k >= 0;) {	// for each sample

			lpCur[0] = lpCur[1] = lpCur2[0] = lpCur2[1] = *lpSampsBase++;
			lpCur += 2L;
			lpCur2 += 2L;
		}
		lpCur = lpCur2;			  // skip what lpCur2 filled in

	}
}

//
// This function automatically handles 1:1 vertical and 2:1 horizontal
// subsampling.
//
void        SubSamp_V11_H21(HPSTR lpSampsBase, HPSTR lpUpSampsBase, LONG nSampsRowSize, LONG nRowSize)
{
	int         j, k;

	nRowSize -= 2 * nSampsRowSize;	// delta that'll get us to next row

	for (j = DCTSIZE; j--;) {	  // each row

		for (k = nSampsRowSize; k--;) {	// each sample

			lpUpSampsBase[0] = lpUpSampsBase[1] = *lpSampsBase++;
			lpUpSampsBase += 2;
		}
		lpUpSampsBase += nRowSize;// move to next row

	}
}

//
// Retrieves a complete MCU and stores the IDCT'd data in the sub-sample
// buffer for each component.
//

int         GetAnMCU(FileStruct FAR * fs)
{
	int         i, j, k, m, n, t;
	NPCOMPONENTINFO npComp;
	HPSTR       lpBlkBase;
	LONG         nMCURowSize;	  // size of a subsampled row
	int         mylastdc;
	LPHUFFTABLE lphuff_ac, lphuff_dc;
	IFAST_MULT_TYPE FAR  *lpqtable;
	int  decode[DCTSIZE2];	  // size for a block

	// for each component, we need to read an 8x8 block of coefficients
	npComp = fs->jpeg.npComponents;
	for (i = fs->jpeg.nComponents; i--; npComp++) {

		nMCURowSize = fs->jpeg.nMCUsPerRow * (npComp->sampfactor >> 4) * DCTSIZE;
		mylastdc = npComp->lastdc;
		lphuff_ac = &fs->jpeg.huff[npComp->achufftbl + 2];
		lphuff_dc = &fs->jpeg.huff[npComp->dchufftbl];
		lpqtable = fs->jpeg.qtbl[npComp->quanttable].ifast_table;

		for (j = 0; j < (npComp->sampfactor & 0x0f); j++) {	// vertical

			lpBlkBase = npComp->lpSamps + nMCURowSize * DCTSIZE * j;

			for (k = npComp->sampfactor >> 4; k--;) {	// horizontal
				// zero out the MCU info first

				_fmemset(decode, 0, sizeof(decode));

				// read the DC coefficient first
				if (t = GetHuffCode(fs, lphuff_dc)) {
					mylastdc += receive(fs, t);
				}
				decode[0] = mylastdc;

				// now do the other 63 ac coefficients - remember to break the loop when
				// you see the end of the list (the rest are zeros)
				for (m = 1; m < DCTSIZE2; m++) {
					n = GetHuffCode(fs, lphuff_ac);
					t = n & 0x0F;
					n >>= 4;
					if (t) {
						m += n;
						decode[ZAG[m]] = receive(fs, t);
					} else {
						if (n != 15)
							break;	  // this is our cue to break the loop

						m += 15;
					}
				}
				// reverse-DCT the thing now that we got it...
				jpeg_idct_ifast((UCHAR FAR *)&dct_clamp[CENTERJSAMPLE], lpqtable, decode, lpBlkBase, nMCURowSize);

				lpBlkBase += DCTSIZE;	// move to next DCT-block base

			}
		}
		npComp->lastdc = mylastdc;
		npComp->lpSamps += DCTSIZE * (npComp->sampfactor >> 4);
	}

	fs->jpeg.nRstCnt--;
	if (fs->jpeg.nRstInterval && !fs->jpeg.nRstCnt) {
		processRestart(fs);
		if (fs->nErr) {
			return (IMG_ERR);
		}
	}
	return (IMG_OK);
}

//
// returns the next nBits bits from the input bit stream as an integer
//

// BITFUFSIZE is the size, in bits, of the buffer we use
// to store the front end of our input bit stream.
// This value must be a multiple of the character size
// (generally 8 bits), and must be more than the char size.
#define BITBUFSIZE	sizeof(int)*8

// BIT_THRESHOLD is the minimun # of bits we need to 
// have in the bit buffer for quick-decoding of our
// huffman input codes. 
#define BIT_THRESHOLD	BITBUFSIZE-8

#define FILL_BIT_BUFFER \
	while (mybits <= BIT_THRESHOLD) { \
		mycodes |= (wgetc(fs,TRUE) << (BITBUFSIZE - mybits - 8)); \
		mybits += 8; \
	}

static int receive(FileStruct FAR * fs, int nBits)
{
	unsigned	mycodes, mybits;
	int	retval,tmpbits;

	mycodes = fs->jpeg.nCodes;
	mybits = fs->jpeg.nBit;

	// make sure our bit buffer is as full as we can
	// get it...
	FILL_BIT_BUFFER;

	// If there are already enough bits in mycodes to
	// extract nBits, just do that and return
	if ((WORD)nBits <= mybits) {
		retval = mycodes >>(BITBUFSIZE - nBits);
		mycodes <<= nBits;
		mybits -= nBits;
	}
	else {
		// if we get here, the number of bits required is
		// greater than we can stuff into mybits, so we'll
		// work a little.
		//
		// This code should really only get executed in cases
		// where sizeof(int) is 16, i.e., 16-bit code
		//
		// Since the largest code can't exceed 16 bits we'll 
		// copy everything into retval then get some more 
		// into mycodes.
		// 

		retval = mycodes >> (BITBUFSIZE - mybits);
		tmpbits = nBits;
		nBits -= mybits;
		mybits = 0;
		mycodes = 0;
		retval <<= nBits;	//we'll fill these in shortly...

		// now we only have to fill in the lower bits of retval
		// with the hi bits of the next input byte
		FILL_BIT_BUFFER;

		retval |= (mycodes >> (BITBUFSIZE - nBits));
		mycodes <<= nBits;
		mybits -= nBits;
		nBits = tmpbits;
	}

	fs->jpeg.nCodes = mycodes;
	fs->jpeg.nBit = mybits;

	return (huff_EXTEND(retval, nBits));
}

// returns the next huffman code from the input stream...
static int  GetHuffCode(FileStruct FAR * fs, LPHUFFTABLE huff)
{
	//
	// Since most of the codes are 8 bits in length or less,
	// (more than 95% of them for most images is a statistic I've
	// seen somewhere), we're optimizing this routine for this
	// case. We've created a 256-entry table of bit values for
	// codes 8 bits or less. We use this as a direct lookup. If
	// we don't find a entry in this table (denoted by a table
	// value of 0) then we get a huffman code the old-fashioned 
	// way, through sleazy bit manipulation (yuck!). 

	int	retval,l;
	unsigned mycodes, mybits;
	
	mycodes = fs->jpeg.nCodes;
	mybits = fs->jpeg.nBit;

	// first make sure we've got at least 8 bits in our buffer
	FILL_BIT_BUFFER;

	retval = mycodes >> (BITBUFSIZE - 8);
	if (huff->nQuickBits[retval]) {
		l = huff->nQuickBits[retval];	// we need this length later...
		mybits -= l;
		mycodes <<= l;
		retval = huff->nQuickVal[retval];
	}
	else {
		// OK, we'll have to do this the hard way...
		
		// retval already contains the first 8 bits, so since
		// we know the code is longer than this we'll just 
		// adjust mycodes & mybits to account for this (we couldn't
		// adjust mybits above because if we had a hit
		// in the QuickVal array we might not use all 8 bits)
		mycodes <<= 8;
		mybits -= 8;

		for (l = 7; retval > huff->max[l] && l < 16; l++) {

			FILL_BIT_BUFFER;

			// put the top bit of mycodes into the bottom bit of
			// retval...
			retval <<= 1;
			retval |= (mycodes >> (BITBUFSIZE-1));
			mycodes <<= 1;
			mybits--;
			
		}

		if (l >= 16) {
			DEBUGOUT(DILERR, "l > 16 in GetHuffCode!", NULL);
			OutputDebugString("yikes!\r\n");
			return 0;
		}

		retval = (huff->huffval[huff->valptr[l] - huff->min[l] + retval]);
	}

	fs->jpeg.nCodes = mycodes;
	fs->jpeg.nBit = mybits;

	return retval; 
}

static void processRestart(FileStruct FAR * fs)
{
	int         i, nTmpCnt;
	
	// now find that pesky RSTn marker
	// This marker takes the form 0xFF 0xDn, where n ranges
	// from 0-7. 
	//
	// It probably is at least partially contained in
	// nCodes already, so we'll recreate the bytes that
	// are in nCodes and look at them.
	//

	if (fs->jpeg.nBit >= 8) {
		nTmpCnt = fs->jpeg.nBit/8;
		fs->jpeg.nCodes <<= (fs->jpeg.nBit % 8);
		fs->jpeg.nBit -= (fs->jpeg.nBit % 8);

		// now we've got only the input bytes that we want to look
		// at in nCodes.
	}	
	else {
		nTmpCnt = 0;
		fs->jpeg.nCodes = 0;
		fs->jpeg.nBit = 0;
	}


	// now we read from nCodes 'till it's empty, then from
	// the input stream (wgetc), looking for the 0xFF
	while (1) {
		if (nTmpCnt) {
			i = (fs->jpeg.nCodes >> (BITBUFSIZE-8));
			nTmpCnt--;
			fs->jpeg.nCodes <<= 8;
			fs->jpeg.nBit -= 8;
		}
		else { 
			i=wgetc(fs,FALSE);
			if (fs->nErr) {
				return;	
			}			
		}

		if (i == 0xff) {	// now skip over the 0xDn byte
			if (nTmpCnt) {
				i = (fs->jpeg.nCodes >> (BITBUFSIZE-8));
				nTmpCnt--;
				fs->jpeg.nCodes <<= 8;
				fs->jpeg.nBit -= 8;
			}
			else {
				i=wgetc(fs,FALSE);
				if (fs->nErr) {
					return;	
				}
			}
			break;	//exit the loop at this point.
		}
	}


	// reset the last_dc values
	for (i = 0; i < fs->jpeg.nComponents; i++) {
		fs->jpeg.npComponents[i].lastdc = 0;
	}

	fs->jpeg.nRstCnt = fs->jpeg.nRstInterval;
}

//
// Given a file handle (which should be positioned to the start of the image)
// this function will traverse the JPEG file from SOI to SOS, extracting
// various important data and stashing it into the JPEGINFO struct pointed
// to by lpjpg
//
// returns IMG_OK if read was successful, some kinda error code otherwise
//
int         DecodeJPEGHeader(LPVOID lpHandle, LPIMG_IO lpIO, LPJPEGINFO lpjpeg)
{
	USHORT      nMarker, nMarkerLen, nTemp;
	int         nStat, i, j, k, l, p, ctr, count, nDHTCnt, lookbits;
	long        lCurOffset;
	FRAMEHEAD   frame;
	UCHAR       cTemp;
	UINT        code;
	JFIFHEAD    jfif;
	LPHUFFTABLE	lpHuff;

	nStat = IMG_OK;

	jfif.id[0] = 0;	//make sure this is 0 so we can tell if it's been filled in later

	lpjpeg->nRstInterval = 0;

	// initialize dct_clamp, a clamping array used when calculating the
	// inverse DCT. The clamping array combines the clamping value with
	// the adjustment of the sample by CENTERJSAMPLE, thus saving a little
	// time.
	for (i=0; i<CENTERJSAMPLE; i++) dct_clamp[i] = 0;
	for (i=0; i<MAXJSAMPLE; i++) dct_clamp[i+CENTERJSAMPLE] = i;
	for (i=0; i<CENTERJSAMPLE; i++) dct_clamp[i+CENTERJSAMPLE+MAXJSAMPLE] = MAXJSAMPLE;

	if ((lpIO->read(lpHandle, &nMarker, sizeof(nMarker)) != sizeof(nMarker)) || (nMarker != M_SOI)) {
		SETERROR(IDS_READERR);
		return (IMG_FILE_ERR);
	}
	// now loop through the file, reading the markers and jumping to
	// the next marker
	do {
		if (lpIO->read(lpHandle, &nMarker, sizeof(nMarker)) != sizeof(nMarker)) {
			SETERROR(IDS_READERR);
			return (IMG_FILE_ERR);
		}
		lCurOffset = lpIO->seek(lpHandle, 0, SEEK_CUR);	// save current offset

		lpIO->read(lpHandle, &nMarkerLen, sizeof(nMarkerLen));
		BSWAP(nMarkerLen);

		switch (nMarker) {
			case M_SOF0:
			case M_SOF1:
			case M_SOF2:
			case M_SOF3:
			case M_SOF5:
			case M_SOF6:
			case M_SOF7:
			case M_JPG:
			case M_SOF9:
			case M_SOF10:
			case M_SOF11:
			case M_SOF13:
			case M_SOF14:
			case M_SOF15:
				// must be SOF0 for us to be able to deal with it!
				if (nMarker != M_SOF0) {
					SETERROR(IDS_BADVER);
					return IMG_NSUPPORT;
				}
				// DEBUGOUT(IMGDETAIL,"SOF: %x, Len = %d", nMarker, nMarkerLen);
				lpIO->read(lpHandle, &frame, sizeof(frame));
				BSWAP(frame.x);
				BSWAP(frame.y);
				// DEBUGOUT(IMGDETAIL,"\t\tWidth: %d, Height: %d, Precision: %u, Components: %u", frame.x, frame.y, (unsigned int)frame.precision, (unsigned int)frame.components);
				lpjpeg->nWidth = frame.x;
				lpjpeg->nHeight = frame.y;
				lpjpeg->nPrecision = (int) frame.precision;
				lpjpeg->nComponents = (int) frame.components;

				// allocate space for the component info
				lpjpeg->nMaxVert = lpjpeg->nMaxHorz = 1;
				lpjpeg->npComponents = (NPCOMPONENTINFO) LocalAlloc(LMEM_FIXED, sizeof(COMPONENTINFO) * frame.components);
				for (i = 0, nTemp = 0; i < frame.components; i++) {
					if (lpIO->read(lpHandle, &(lpjpeg->npComponents[i]), 3) != 3) {
						SETERROR(IDS_READERR);
						return (IMG_FILE_ERR);
					}
					nTemp += (lpjpeg->npComponents[i].sampfactor & 0xf0 + lpjpeg->npComponents[i].sampfactor & 0x0f) * 64;	// each 8x8 block is 64 bytes

					if ((lpjpeg->npComponents[i].sampfactor >> 4) > lpjpeg->nMaxHorz) {
						lpjpeg->nMaxHorz = lpjpeg->npComponents[i].sampfactor >> 4;
					}
					if ((lpjpeg->npComponents[i].sampfactor & 0x0f) > lpjpeg->nMaxVert) {
						lpjpeg->nMaxVert = lpjpeg->npComponents[i].sampfactor & 0x0f;
					}
				}

				// look for standard sub-sampling ratios
				if ((lpjpeg->npComponents[0].sampfactor == 0x22)
					 && (lpjpeg->npComponents[1].sampfactor == 0x11)
					 && (lpjpeg->npComponents[2].sampfactor == 0x11)) {
					lpjpeg->nStandardSamp = SAMP_22_11_11;
				} else if ((lpjpeg->npComponents[0].sampfactor == 0x21)
							  && (lpjpeg->npComponents[1].sampfactor == 0x11)
							  && (lpjpeg->npComponents[2].sampfactor == 0x11)) {
					lpjpeg->nStandardSamp = SAMP_21_11_11;
				} else {
					lpjpeg->nStandardSamp = SAMP_NONSTANDARD;
				}
				// if (lpjpeg->nMaxHorz != 2 || lpjpeg->nMaxVert != 2) {
				// LocalFree((HANDLE)(lpjpeg->npComponents));
				// nErrID = IDS_BADVER;
				// return IMG_NSUPPORT;
				// }

				// OK, now allocate space for the subsampled data to be placed into
				lpjpeg->nMCUsPerRow = (lpjpeg->nWidth + DCTSIZE * lpjpeg->nMaxHorz - 1) / (DCTSIZE * lpjpeg->nMaxHorz);
				lpjpeg->nMCURowsPerImage = (lpjpeg->nHeight + DCTSIZE * lpjpeg->nMaxVert - 1) / (DCTSIZE * lpjpeg->nMaxVert);
				for (i = 0; i < frame.components; i++) {
					// calculate un-upsampled buffer size
					long         lSize = (lpjpeg->npComponents[i].sampfactor >> 4) * (long)lpjpeg->nMCUsPerRow * DCTSIZE2 * (lpjpeg->npComponents[i].sampfactor & 0x0f);
					lpjpeg->npComponents[i].lpSampsBase = GlobalAllocPtr(GHND, lSize);
					lpjpeg->npComponents[i].lpSamps = lpjpeg->npComponents[i].lpSampsBase;
					assert(lpjpeg->npComponents[i].lpSampsBase);

					// calculate up-sampled buffer size...
					lSize = (long)lpjpeg->nMaxHorz * lpjpeg->nMaxVert * DCTSIZE2 * lpjpeg->nMCUsPerRow;
					lpjpeg->npComponents[i].lpUpSamps = GlobalAllocPtr(GHND, lSize);
					lpjpeg->npComponents[i].lpUpSampsBase = lpjpeg->npComponents[i].lpUpSamps;
					assert(lpjpeg->npComponents[i].lpUpSamps);
				}

				// DEBUGOUT(IMGDETAIL, "Max Horz: %d, Max Vert: %d, MCUsPerRow = %d, MCURows = %d", lpjpeg->nMaxHorz, lpjpeg->nMaxVert, lpjpeg->nMCUsPerRow, lpjpeg->nMCURowsPerImage);

#if DEBUG
				for (i = 0; i < frame.components; i++) {
					// DEBUGOUT(IMGDETAIL, "\t\t\t\tComponent %d: %d, %x, %d", i, lpjpeg->npComponents[i].identifier,lpjpeg->npComponents[i].sampfactor,lpjpeg->npComponents[i].quanttable);
				}
#endif
				break;

			case M_SOS:
				lpIO->read(lpHandle, &cTemp, 1);
				// DEBUGOUT(IMGDETAIL,"Marker: %s, %d Components", (LPSTR)"M_SOS ", (int)cTemp);
				for (i = 0; i < cTemp; i++) {	// for each component in the scan

					lpIO->read(lpHandle, &nTemp, 2);	// read component ID & huff tables

					BSWAP(nTemp);
					// DEBUGOUT(IMGDETAIL,"\t\t\tComponent: %d, DC: %d, AC: %d", nTemp >> 8, (nTemp & 0x00f0) >> 4, nTemp & 0x000f);
					// now set the correct AC & DC tables for the current component
					for (j = 0; j < lpjpeg->nComponents; j++) {
						if (lpjpeg->npComponents[j].identifier == (nTemp >> 8)) {
							lpjpeg->npComponents[j].dchufftbl = (UCHAR) (nTemp & 0x00f0) >> 4;
							lpjpeg->npComponents[j].achufftbl = (UCHAR) (nTemp & 0x000f);
							lpjpeg->npComponents[j].lastdc = 0;
							break;
						}
					}
				}
				lpjpeg->lOffset = lCurOffset + nMarkerLen;	// save offset to data

				break;

			case M_DQT:
				nDHTCnt = nMarkerLen - 2;
				while (nDHTCnt > 0) {
					lpIO->read(lpHandle, &cTemp, 1);
					lpjpeg->qtbl[cTemp & 0x000f].nPrecision = ((cTemp & 0x00f0) >> 4);
					lpIO->read(lpHandle, &(lpjpeg->qtbl[cTemp & 0x000f]), 64);

					/* For AA&N IDCT method, multipliers are equal to quantization
					 * coefficients scaled by scalefactor[row]*scalefactor[col], where
					 *   scalefactor[0] = 1
					 *   scalefactor[k] = cos(k*PI/16) * sqrt(2)    for k=1..7
					 * For integer operation, the multiplier table is to be scaled by
					 * IFAST_SCALE_BITS.  The multipliers are stored in natural order.
					 */
					{
						IFAST_MULT_TYPE FAR *ifmtbl;
						UCHAR FAR *qtbl;
#define IFAST_SCALE_BITS  2	/* fractional bits in scale factors */
#define CONST_BITS 14
						static const SHORT aanscales[DCTSIZE2] = {
						  /* precomputed values scaled up by 14 bits */
						  16384, 22725, 21407, 19266, 16384, 12873,  8867,  4520,
						  22725, 31521, 29692, 26722, 22725, 17855, 12299,  6270,
						  21407, 29692, 27969, 25172, 21407, 16819, 11585,  5906,
						  19266, 26722, 25172, 22654, 19266, 15137, 10426,  5315,
						  16384, 22725, 21407, 19266, 16384, 12873,  8867,  4520,
						  12873, 17855, 16819, 15137, 12873, 10114,  6967,  3552,
						   8867, 12299, 11585, 10426,  8867,  6967,  4799,  2446,
						   4520,  6270,  5906,  5315,  4520,  3552,  2446,  1247
						};
						/* ZIG[i] is the zigzag-order position of the i'th element of a DCT block */
						/* read in natural order (left to right, top to bottom). */
						static const int ZIG[DCTSIZE2] = {
						     0,  1,  5,  6, 14, 15, 27, 28,
						     2,  4,  7, 13, 16, 26, 29, 42,
						     3,  8, 12, 17, 25, 30, 41, 43,
						     9, 11, 18, 24, 31, 40, 44, 53,
						    10, 19, 23, 32, 39, 45, 52, 54,
						    20, 22, 33, 38, 46, 51, 55, 60,
						    21, 34, 37, 47, 50, 56, 59, 61,
						    35, 36, 48, 49, 57, 58, 62, 63
						};
						SHIFT_TEMPS

						ifmtbl = lpjpeg->qtbl[cTemp & 0x00f].ifast_table;
						qtbl = lpjpeg->qtbl[cTemp & 0x00f].table;

						for (i = 0; i < DCTSIZE2; i++) {
						  ifmtbl[i] = (IFAST_MULT_TYPE)
						    DESCALE(MULTIPLY16V16((INT32) qtbl[ZIG[i]],
									  (INT32) aanscales[i]),
							    CONST_BITS-IFAST_SCALE_BITS);
						}
					}
					nDHTCnt -= DCTSIZE2 + 1;
				}
				// DEBUGOUT(IMGDETAIL,"Marker: %s, id = %d, precision = %d", (LPSTR)"M_DQT ", cTemp & 0x000f, (cTemp & 0x00f0) >> 4);
				break;

			case M_DHT:
				nDHTCnt = nMarkerLen - 2;
				while (nDHTCnt > 0) {	// there may be multiple tables

					lpIO->read(lpHandle, &cTemp, 1);		
					k = (int) (cTemp & 0x0f);
					if (cTemp & 0x0010) {
						k += 2;
					}

					lpHuff = &(lpjpeg->huff[k]);

					// DEBUGOUT(IMGDETAIL,"Marker: %s, Table(0-4): %d, Type: %s", (LPSTR)"M_DHT ", (int)(cTemp & 0x0f), cTemp & 0x10 ? (LPSTR)"AC" : (LPSTR)"DC");
					// read bits array
					lpIO->read(lpHandle, lpjpeg->huff[k].bits, 16);
					// read in the HUFFVAL array
					for (i = 0, count = 0; i < 16; i++)
						count += lpjpeg->huff[k].bits[i];

					lpHuff->huffval = (PSTR) LocalAlloc(LMEM_FIXED, count);
					lpHuff->huffcode = (PINT) LocalAlloc(LMEM_FIXED, count * sizeof(int));
					lpIO->read(lpHandle, lpHuff->huffval, count);

					nDHTCnt -= 1 + 16 + count;	// keep track of which table we're on...

					// now generate the codes for this table
					// we don't generate a HUFFSIZE table -- it appears to be
					// somewhat redundant...
					for (i = 0, code = 0, count = 0; i < 16; i++, code <<= 1) {	// for each entry in  BITS array
						// DEBUGOUT(IMGDETAIL, "Huffman codes of length %d, %d", i+1, lpHuff->bits[i]);

						lpHuff->min[i] = code;
						lpHuff->valptr[i] = count;
						for (j = 0; j < lpHuff->bits[i]; j++) {
							lpHuff->huffcode[count++] = code++;
							// DEBUGOUT(IMGDETAIL, "\t\t\thuffcode[%d] = %u", count-1, code-1);
						}
						lpHuff->max[i] = code - 1;
						// DEBUGOUT(IMGDETAIL, "\t\tmax[%d] = %d", i, lpHuff->max[i]);
					}

					// setup our quick-look table for faster Huffman code retrieval
					_fmemset(lpHuff->nQuickBits, 0, sizeof(lpHuff->nQuickBits));

					p = 0;
					for (l = 1; l <= HUFF_QUICK_BITS; l++) {
					  for (i = 1; i <= (int) lpHuff->bits[l-1]; i++, p++) {
					    /* l = current code's length, p = its index in huffcode[] & huffval[]. */
					    /* Generate left-justified code followed by all possible bit sequences */
					    lookbits = lpHuff->huffcode[p] << (HUFF_QUICK_BITS-l);
					    for (ctr = 1 << (HUFF_QUICK_BITS-l); ctr > 0; ctr--) {
							lpHuff->nQuickBits[lookbits] = l;
							lpHuff->nQuickVal[lookbits] = lpHuff->huffval[p];
							lookbits++;
					    }
					  }
					}
					// DEBUGOUT(IMGDETAIL, "bits = [%d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d]",
					// lpHuff->bits[0], lpHuff->bits[1],
					// lpHuff->bits[2], lpHuff->bits[3],
					// lpHuff->bits[4], lpHuff->bits[5],
					// lpHuff->bits[6], lpHuff->bits[7],
					// lpHuff->bits[8], lpHuff->bits[9],
					// lpHuff->bits[10], lpHuff->bits[11],
					// lpHuff->bits[12], lpHuff->bits[13],
					// lpHuff->bits[14], lpHuff->bits[15]);
				}
				break;
			case M_DAC:
				DEBUGOUT(IMGDETAIL, "Marker: %s, Len = %d", "(LPSTR)M_DAC ", nMarkerLen);
				break;

			case M_EOI:
				DEBUGOUT(IMGDETAIL, "Marker: %s, Len = %d", (LPSTR) "M_EOI ", nMarkerLen);
				break;

			case M_DNL:
				DEBUGOUT(IMGDETAIL, "Marker: %s, Len = %d", (LPSTR) "M_DNL ", nMarkerLen);
				break;
			case M_DRI:
				lpIO->read(lpHandle, &nTemp, 2); // CAR: must read into a 'WORD'

				BSWAP(nTemp);
				lpjpeg->nRstCnt = lpjpeg->nRstInterval = (int) nTemp;
				DEBUGOUT(IMGDETAIL, "Marker: %s, Len = %d", (LPSTR) "M_DRI ", nMarkerLen);
				break;
			case M_DHP:
				DEBUGOUT(IMGDETAIL, "Marker: %s, Len = %d", (LPSTR) "M_DHP ", nMarkerLen);
				break;
			case M_EXP:
				DEBUGOUT(IMGDETAIL, "Marker: %s, Len = %d", (LPSTR) "M_EXP ", nMarkerLen);
				break;
			case M_APP0:
				DEBUGOUT(IMGDETAIL, "Marker: %s, Len = %d", (LPSTR) "M_APP0", nMarkerLen);
				// read the  header, dude
				if (!jfif.id[0]) {
					lpIO->read(lpHandle, &jfif, sizeof(JFIFHEAD));
					if (_fstrcmp("JFIF", jfif.id)) {
						SETERROR(IDS_BADVER);
						return IMG_NSUPPORT;
					}

					lpjpeg->DensUnit = jfif.units;
	
					BSWAP(jfif.xdens);
					BSWAP(jfif.ydens);
					
					if( jfif.units ) {
						lpjpeg->xDens = jfif.xdens;
						lpjpeg->yDens = jfif.ydens;
					} else {
						lpjpeg->xDens = 0;
						lpjpeg->yDens = 0;
					}						

					DEBUGOUT(IMGDETAIL, "JFIF Header Info: Ver=%x, units=%d, xd=%d, yd=%d, xThumb=%d",
								jfif.version, jfif.units, jfif.xdens, jfif.ydens, jfif.xthumb);
				}
				break;
			case M_APP15:
				DEBUGOUT(IMGDETAIL, "Marker: %s, Len = %d", (LPSTR) "M_APP1", nMarkerLen);
				break;
			case M_JPG0:
				DEBUGOUT(IMGDETAIL, "Marker: %s, Len = %d", (LPSTR) "M_JPG0", nMarkerLen);
				break;
			case M_JPG13:
				DEBUGOUT(IMGDETAIL, "Marker: %s, Len = %d", (LPSTR) "M_JPG1", nMarkerLen);
				break;
			case M_COM:
				DEBUGOUT(IMGDETAIL, "Marker: %s, Len = %d", (LPSTR) "M_COM ", nMarkerLen);
				break;
			default:
				break;
		}
		lpIO->seek(lpHandle, lCurOffset + nMarkerLen, SEEK_SET);	// seek to next marker

	} while (nMarker != M_EOI && nMarker != M_SOS);

	return (IMG_OK);
}
