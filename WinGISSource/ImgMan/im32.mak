# 32 Bit
# ImageMan 6.0 Makefile
#

!if defined(DEBUG)
DEBUGFLAG=/DDEBUG=1
!else
DEBUGFLAG=
!endif

!if defined(DEMO)
DEMOFLAG=/DDEMO=1
!else
DEMOFLAG=
!endif

.SUFFIXES: .dil .del .cpp .o

!ifdef RELEASE

CFLAGS=/c /G5d /W3 /MT /Ox /J /Zi /DWIN32 /D_X86_ /DNDEBUG $(DEBUGFLAG) $(DEMOFLAG)
LINKOPTS=/DLL /DEBUG:NONE /SUBSYSTEM:windows /PDB:NONE

!else

CFLAGS=/c /G5d /W3 /MT /Od /J /Zi /DWIN32 /D_X86_ $(DEBUGFLAG) $(DEMOFLAG)
LINKOPTS=/DLL /DEBUG /DEBUGTYPE:cv /SUBSYSTEM:windows /PDB:NONE /PROFILE

!endif

!ifdef USELZW
CFLAGS = $(CFLAGS) /DUSELZW
!endif

DILBASE=IM31
DELBASE=IM31
DLLBASE=32
WLIBS=user32.lib gdi32.lib kernel32.lib comdlg32.lib advapi32.lib shell32.lib

.rc.res:
	rc -r $(@B).rc


.c.obj:
	cl $(CFLAGS) $<

.cpp.obj:
  cl $(CFLAGS) $<

DILS=$(DILBASE)pri.dil $(DILBASE)bmp.dil $(DILBASE)gif.dil \
$(DILBASE)pcx.dil $(DILBASE)png.dil  \
$(DILBASE)jpg.dil $(DILBASE)tif.dil $(DILBASE)wmf.dil $(DILBASE)dxf.dil \
$(DILBASE)png.dil

DELS=$(DELBASE)xpri.del $(DELBASE)xbmp.del $(DELBASE)xpcx.del $(DELBASE)xjpg.del \
$(DELBASE)xgif.del $(DELBASE)xtif.del  \
$(DELBASE)xpng.del

DLL=imgman$(DLLBASE).dll

#all: $(DILS) $(DLL) $(DELS) imhost32.dll
all: $(DILS) $(DLL) $(DELS)

##########################################################################################
# ImageMan DLL

#imgman$(DLLBASE).lib: imgman$(DLLBASE).dll
       #implib imgman$(DLLBASE).lib imgman$(DLLBASE).dll

imgman$(DLLBASE).dll: imgman.obj contrast.obj filter.obj optblk.obj imgx.obj \
dibfuncs.obj color.obj color2.obj effects.obj imgio.obj twain.obj dib.obj \
fxshape.obj fxblinds.obj fxcurtn.obj fxmosaic.obj fxwipe.obj imgfx.obj fxblur.obj \
hostint.obj cdialogs.obj nag.obj imgman.res
	link $(LINKOPTS) /base:"0x00707100" $** $(WLIBS) SKEYLI32.LIB MPR.LIB NETAPI32.LIB RPCRT4.LIB /nodefaultlib:"Libc.lib" -OUT:imgman$(DLLBASE).dll -DEF:imgman32.def -MAP:imgman32.map

optblk.obj: optblk.cpp optblk.h

imgman.obj: imgman.cpp imgman.h ppp.cpp ppp.h nag.cpp nag.h

imgio.obj:  imgio.c imgio.h

Hostint.obj: hostint.cpp pihost.h internal.h imgman.h

contrast.obj:   contrast.cpp

cdialogs.obj:   cdialogs.cpp

filter.obj:     filter.cpp

imgx.obj:       imgx.cpp imgman.h

color.obj:      color.cpp

color2.obj:     color2.cpp

dibfuncs.obj:   dibfuncs.cpp

effects.obj:    effects.cpp

twain.obj:      twain.cpp

dib.obj:      dib.cpp

fxshape.obj:    fxshape.cpp

fxblinds.obj:   fxblinds.cpp

fxcurtn.obj:    fxcurtn.cpp

fxmosaic.obj:   fxmosaic.cpp

fxwipe.obj:             fxwipe.cpp

fxblur.obj:             fxblur.cpp

imgfx.obj:              imgfx.cpp

nag.obj: 		nag.cpp nag.h

##############################################################################################
#
# Common file to handle local storage for different threads without calling to TLSAlloc() etc.
#
##############################################################################################

TLSByJHC.obj:	TLSByJHC.c TLSByJHC.h

##########################################################################################
# ImageMan Plug-In Host DLL
#
# imhost32.dll: hostmain.obj cpipl.obj dib.obj impihost.res
#  link $(LINKOPTS) /base:"0x00707200"$** $(WLIBS) imgman32.lib -OUT:imhost32.dll -DEF:impihost.def -MAP:imhost32.map
#
# hostmain.obj: hostmain.cpp imgman.h internal.h pihost.h dib.h pigeneral.h cpipl.h
#
# cpipl.obj: cpipl.cpp cpipl.h
#
#dib.obj: dib.cpp dib.h
#
##########################################################################################
# Windows Bitmap DIL

$(DILBASE)bmp.dil: bmp.obj TLSByJHC.obj bmp.res
	link $(LINKOPTS) /base:"0x00707300" $** $(WLIBS) -OUT:$(DILBASE)bmp.dil -DEF:bmp.def -MAP:$(DILBASE)bmp.map

bmp.obj: bmp.c bmp.h


##########################################################################################
# Photo CD DIL
#
#$(DILBASE)pcd.dil: pcd.obj TLSByJHC.obj pcd.res
#	link $(LINKOPTS) /base:"0x00707400" $** $(WLIBS) pcdlib32.lib -OUT:$(DILBASE)pcd.dil -DEF:pcd.def -MAP:$(DILBASE)pcd.map
#
#pcd.obj: pcd.c pcd.h
#
#
##########################################################################################
# EPSF DIL
#
#$(DILBASE)eps.dil: eps.obj eps.res
#	link $(LINKOPTS) /base:"0x00707500" $** $(WLIBS) -OUT:$(DILBASE)eps.dil -DEF:eps.def -MAP:$(DILBASE)eps.map
#
#eps.obj: eps.c eps.h
#
#
##########################################################################################
# GIF DIL

$(DILBASE)gif.dil: gif.obj lzw.obj TLSByJHC.obj gif.res
	link $(LINKOPTS) /base:"0x00707600" $** $(WLIBS) -OUT:$(DILBASE)gif.dil -DEF:gif.def -MAP:$(DILBASE)gif.map

gif.obj: gif.c gif.h

lzw.obj: lzw.c lzw.h

##########################################################################################
# IMG DIL
#
#$(DILBASE)img.dil: img.obj TLSByJHC.obj img.res
#	link $(LINKOPTS) /base:"0x00707700" $** $(WLIBS) -OUT:$(DILBASE)img.dil -DEF:img.def -MAP:$(DILBASE)img.map
#
#img.obj: img.c img.h
#
#
##########################################################################################
# WPG DIL
#
# $(DILBASE)wpg.dil: wpg.obj wpg.res
#	link $(LINKOPTS) /base:"0x00707800" $** $(WLIBS) -OUT:$(DILBASE)wpg.dil -DEF:wpg.def -MAP:$(DILBASE)wpg.map
#
# wpg.obj: wpg.c wpg.h
#
#
##########################################################################################
# PCX DIL

$(DILBASE)pcx.dil: pcx.obj TLSByJHC.obj pcx.res
	link $(LINKOPTS) /base:"0x00707900" $** $(WLIBS) -OUT:$(DILBASE)pcx.dil -DEF:pcx.def -MAP:$(DILBASE)pcx.map

pcx.obj: pcx.c pcx.h pcxrc.h


##########################################################################################
# TGA DIL
#
#$(DILBASE)tga.dil: tga.obj tga.res
#	link $(LINKOPTS) /base:"0x00708000" $** $(LIBS) -OUT:$(DILBASE)tga.dil -DEF:tga.def -MAP:$(DILBASE)tga.map
#
#tga.obj: tga.c tga.h #rc.h
#
##########################################################################################
# JPG DIL

$(DILBASE)jpg.dil: jpg.obj jpeg.obj jrevdct.obj TLSByJHC.obj jpg.res
	link $(LINKOPTS) /base:"0x00708100" $** $(WLIBS) -OUT:$(DILBASE)jpg.dil -DEF:jpg.def -MAP:$(DILBASE)jpg.map

jpg.obj: jpg.c jpg.h

jpeg.obj: jpeg.c jpg.h

jrevdct.obj: jrevdct.c jpg.h

##########################################################################################
# PNG DIL

$(DILBASE)png.dil: png.obj TLSByJHC.obj png.res
	link $(LINKOPTS) /base:"0x00708200" $** $(WLIBS) zlib32.lib -OUT:$(DILBASE)png.dil -DEF:png.def -MAP:$(DILBASE)png.map

png.obj: png.c png.h zlib.h

##########################################################################################
# TIF DIL

$(DILBASE)tif.dil: tif.obj tiff2.obj lzw.obj un2d.obj TLSByJHC.obj tif.res
	link $(LINKOPTS) /base:"0x00708300" $** $(WLIBS) -OUT:$(DILBASE)tif.dil -DEF:tif.def -MAP:$(DILBASE)tif.map

tif.obj: tif.c tif.h ccitt.h

lzw.obj: lzw.c lzw.h

tiff2.obj: tiff2.c

un2d.obj: un2d.c

##########################################################################################
# PRI DIL

$(DILBASE)pri.dil: pri.obj prif2.obj lzw.obj un2d.obj TLSByJHC.obj pri.res
	link $(LINKOPTS) /base:"0x00708300" $** $(WLIBS) -OUT:$(DILBASE)pri.dil -DEF:pri.def -MAP:$(DILBASE)pri.map

pri.obj: pri.c pri.h ccitt.h

lzw.obj: lzw.c lzw.h

prif2.obj: prif2.c

un2d.obj: un2d.c

##########################################################################################
# WMF DIL

$(DILBASE)wmf.dil: wmf.obj TLSByJHC.obj wmf.res
	link $(LINKOPTS) /base:"0x00708400" $** $(WLIBS) -OUT:$(DILBASE)wmf.dil -DEF:wmf.def -MAP:$(DILBASE)wmf.map

wmf.obj: wmf.c wmf.h wmferr.h

##########################################################################################
# DXF DIL

$(DILBASE)dxf.dil: dxf.obj dxf2.obj TLSByJHC.obj dxf.res
	link $(LINKOPTS) /base:"0x00708500" $** $(WLIBS) -OUT:$(DILBASE)dxf.dil -DEF:dxf.def -MAP:$(DILBASE)dxf.map

dxf.obj: dxf.c dxf.h

dxf2.obj: dxf2.c dxf2.h

##########################################################################################
# FAX DIL
#
#$(DILBASE)fax.dil: fax.obj fax2.obj fax.res
#	link $(LINKOPTS) /base:"0x00708600" $** $(WLIBS) -OUT:$(DILBASE)fax.dil -DEF:fax.def -MAP:$(DILBASE)fax.map
#
#fax.obj: fax.c fax.h fax2.h
#fax2.obj: fax2.c fax2.h
#
##########################################################################################
# BMP DEL

$(DELBASE)xbmp.del: xbmp.obj TLSByJHC.obj xbmp.res
	link $(LINKOPTS) /base:"0x00708700" $** $(WLIBS) -OUT:$(DELBASE)xbmp.del -DEF:xbmp.def -MAP:$(DELBASE)xbmp.map

xbmp.obj: xbmp.c xbmp.h

##########################################################################################
# DCX DEL
#
#$(DELBASE)xdcx.del: xdcx.obj TLSByJHC.obj xdcx.res
#	link $(LINKOPTS) /base:"0x00708700" $** $(WLIBS) -OUT:$(DELBASE)xdcx.del -DEF:xdcx.def -MAP:$(DELBASE)xdcx.map
#
#xdcx.obj: xdcx.c xdcx.h
#
#
##########################################################################################
# EPS DEL
#
#$(DELBASE)xeps.del: xeps.obj xeps.res
#	link $(LINKOPTS) /base:"0x00708700" $** $(WLIBS) -OUT:$(DELBASE)xeps.del -DEF:xeps.def -MAP:$(DELBASE)xeps.map
#
#xeps.obj: xeps.c xeps.h
#
#
##########################################################################################
# PCX DEL

$(DELBASE)xpcx.del: xpcx.obj TLSByJHC.obj xpcx.res
	link $(LINKOPTS) /base:"0x00708700" $** $(WLIBS) -OUT:$(DELBASE)xpcx.del -DEF:xpcx.def -MAP:$(DELBASE)xpcx.map

xpcx.obj: xpcx.c xpcx.h


##########################################################################################
# JPG DEL

$(DELBASE)xjpg.del: xjpg.obj jfwddct.obj optblk.obj TLSByJHC.obj xjpg.res 
	link $(LINKOPTS) /base:"0x00708700" $** $(WLIBS) -OUT:$(DELBASE)xjpg.del -DEF:xjpg.def -MAP:$(DELBASE)xjpg.map

xjpg.obj: xjpg.c xjpg.h
optblk.obj:     optblk.cpp optblk.h
jfwddct.obj: jfwddct.c

##########################################################################################
# GIF DEL

$(DELBASE)xgif.del: xgif.obj lzwx.obj  optblk.obj TLSByJHC.obj xgif.res 
	link $(LINKOPTS) /base:"0x00708700" $** $(WLIBS) -OUT:$(DELBASE)xgif.del -DEF:xgif.def -MAP:$(DELBASE)xgif.map

xgif.obj: xgif.c xgif.h
optblk.obj:     optblk.cpp optblk.h
lzwx.obj:       lzwx.c lzwx.h


##########################################################################################
# TIF DEL

$(DELBASE)xtif.del: xtif.obj lzwx.obj optblk.obj TLSByJHC.obj xtif.res 
	link $(LINKOPTS) /base:"0x00708700" $** $(WLIBS) -OUT:$(DELBASE)xtif.del -DEF:xtif.def -MAP:$(DELBASE)xtif.map

xtif.obj: xtif.c xtif.h
optblk.obj:     optblk.cpp optblk.h
lzwx.obj:       lzwx.c lzwx.h

##########################################################################################
# PRI DEL

$(DELBASE)xpri.del: xpri.obj lzwx.obj optblk.obj TLSByJHC.obj xpri.res 
	link $(LINKOPTS) /base:"0x00708700" $** $(WLIBS) -OUT:$(DELBASE)xpri.del -DEF:xpri.def -MAP:$(DELBASE)xpri.map

xpri.obj: xpri.c xpri.h
optblk.obj:     optblk.cpp optblk.h
lzwx.obj:       lzwx.c lzwx.h

##########################################################################################
# FAX DEL
#
#$(DELBASE)xfax.del: xfax.obj xfax.res optblk.obj
#	link $(LINKOPTS) /base:"0x00708700" $** $(WLIBS) -OUT:$(DELBASE)xfax.del -DEF:xfax.def -MAP:$(DELBASE)xfax.map
#
#xfax.obj: xfax.c xfax.h fax2.h
#
##########################################################################################
# PNG DEL

$(DELBASE)xpng.del: xpng.obj optblk.obj pngmain.obj pngtrans.obj pngwrite.obj pngwtran.obj pngwutil.obj pngio.obj pngmem.obj pngerror.obj TLSByJHC.obj xpng.res
	link $(LINKOPTS) /base:"0x00708700" $** $(WLIBS) zlib32.lib -OUT:$(DELBASE)xpng.del -DEF:xpng32.def -MAP:$(DELBASE)xpng.map


xpng.obj:       xpng.c xpng.h optblk.h png.h
optblk.obj:     optblk.cpp optblk.h
pngmain.obj:    pngmain.c
pngtrans.obj:   pngtrans.c
pngwrite.obj:   pngwrite.c
pngwtran.obj:   pngwtran.c
pngwutil.obj:   pngwutil.c
pngio.obj:      pngio.c
pngmem.obj:     pngmem.c
pngerror.obj:   pngerror.c

##########################################################################################
# clean target

clean:
	@-erase *.obj
	@-erase *.map
	@-erase *.pch
	@-erase *.pdb
	@-erase *.res
