//////////////////////////////////////////////////////////////////////////////
//
// 	DIB.cpp: General purpose DIB handling class
//
//	Copyright(c) 1997 Data Techniques, Inc.
//
//////////////////////////////////////////////////////////////////////////////
//$Header: /ImageMan 16-32/DLL - V 6.xx API/dib.h 6     5/19/99 2:40p Ericw $
//$History: dib.h $
// 
// *****************  Version 6  *****************
// User: Ericw        Date: 5/19/99    Time: 2:40p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// As part of bug #161 and a new found issue with copying an image from
// the clipboard.  This was regarding bmps that are 24 bit _and_ also had
// a palette with the biClrUsed value set.
// 
// *****************  Version 5  *****************
// User: Ericj        Date: 11/13/98   Time: 12:04p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Fixed 16 bit Dib Constructor in 16 bits used in ImgFromClipboard.
// Fixed 16 bit Data Offset in ImgToClipboard.
// 
// *****************  Version 4  *****************
// User: Johnd        Date: 11/04/98   Time: 5:10p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Updates to properly update DIBSECTION color table when calling
// GethBitmap()
// 
// *****************  Version 3  *****************
// User: Johnd        Date: 11/02/98   Time: 4:42p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Misc 6.02 Updates
// 
// *****************  Version 2  *****************
// User: Ericw        Date: 1/19/98    Time: 11:09a
// Updated in $/ImageMan 16-32/DLL-API Level 2
// New function to get the bit count - GetBitCount.
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 7/10/97    Time: 2:04p
// Created in $/imageman 16-32/dll-api level 2
// Initial Lvl 2 Checkin
// 

class DIB {

private:

	HANDLE hDIB;
	VOID FAR * lpDIB;
	LPBITMAPINFO lpBMI;

	HANDLE	hDIBSection;

public:

	BOOL IsValid( void ) { return ((lpDIB != NULL && hDIB != NULL) || hDIBSection); };

	DIB( LONG nWidth, LONG nHeight, WORD nBitsPerPixel = 1, BOOL bDIBSection = 1, LPBITMAPINFO lpInitBMI = NULL );
	DIB( HANDLE hDIB, BOOL bCopy = 0);
	DIB( HBITMAP hNewDIBSection );

	BOOL IsDIBSection( void ) { return hDIBSection != NULL; };

	~DIB(void);

	LONG GetColors( LPBITMAPINFOHEADER lpbmi ) ;

	LONG GetWidth(void) { return lpBMI->bmiHeader.biWidth; };
	LONG GetHeight(void) { return lpBMI->bmiHeader.biHeight; };

	void SetXRes( LONG nXRes ) { lpBMI->bmiHeader.biXPelsPerMeter = (LONG)((float)nXRes * 39.37); };
	void SetYRes( LONG nYRes ) { lpBMI->bmiHeader.biYPelsPerMeter = (LONG)((float)nYRes * 39.37); };

	LPSTR GetDIBPtr( void ) { return (LPSTR)lpBMI; };
	LPSTR GetDataPtr(void);

	DWORD GetPalEntries( void );

	DWORD GetBitsOffset( void );

	WORD GetBitCount ( void ) { return ((LPBITMAPINFOHEADER)lpBMI)->biBitCount ;} ;

	LPBITMAPINFO GetLPBI( void ) { return lpBMI; };

	int GetRowPadding( void );

	DWORD GetMemSize( void );
	DWORD GetImageMemSize( void );

	DWORD GetRowSize( void );

	HANDLE GethDIB( BOOL bNewDIB );

	HANDLE GethDIBSection( void ) { return hDIBSection; };
};

typedef DIB * PDIB;
