/////////////////////////////////////////////////////////////////////////////
//
//    IMGMAN.H
//
//    ImageMan Prototypes & User Structures
//
//    ImageMan Image Processing Library
//
//    Copyright (c) 1991-5 Data Techniques, Inc.
//    All Rights Reserved
//
/////////////////////////////////////////////////////////////////////////////
// $History: imgman.h $
// 
// *****************  Version 25  *****************
// User: Erict        Date: 3/21/00    Time: 4:34p
// Updated in $/FaxMan Net/FaxDILs
// added error codes for faxmannet. they come after the existing ones and
// shouldn't affect existing code.
// 
// *****************  Version 24  *****************
// User: Johnd        Date: 11/11/98   Time: 1:10p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Chgd angle param in ImgRotate to UINT to allow 16 bit code to specify
// angles > 326
// 
// *****************  Version 23  *****************
// User: Johnd        Date: 11/02/98   Time: 4:43p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Misc 6.02 Updates
// 
// *****************  Version 22  *****************
// User: Johnd        Date: 8/19/98    Time: 1:38p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Chgd ImgFilterLow to take a FAR * to the kernel for 16 bit Builds
// 
// *****************  Version 21  *****************
// User: Johnd        Date: 8/07/98    Time: 3:08p
// Branched in $/ImageMan 16-32/DLL - V 6.xx API
// 
// *****************  Version 20  *****************
// User: Ericw        Date: 4/24/98    Time: 11:02a
// Updated in $/ImageMan 16-32/DLL-API Level 2
// New function ImgPlugInInvokeEx.
// 
// *****************  Version 19  *****************
// User: Johnd        Date: 4/16/98    Time: 4:25p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Removed nColors param from ImgReduceColorsPal
// 
// *****************  Version 18  *****************
// User: Johnd        Date: 4/16/98    Time: 11:47a
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Misc Updates
// 
// *****************  Version 16  *****************
// User: Johnd        Date: 4/15/98    Time: 2:09p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Chg ImgReduceColors(Pal) to make Octree Method the default
// 
// *****************  Version 15  *****************
// User: Johnd        Date: 4/10/98    Time: 5:06p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Chgd Error Returns to be consistent with other ImageMan Defines
// 
// *****************  Version 14  *****************
// User: Johnd        Date: 4/10/98    Time: 9:59a
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Chgd IMGFX error defines to be more consistent with IMG_* defines
// 
// *****************  Version 13  *****************
// User: Ericw        Date: 4/07/98    Time: 4:26p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Now has an IMGPI_CANCELLED return code and ImgPlugInInvoke now takes an
// LPRECT instead of a RECT.
// 
// *****************  Version 12  *****************
// User: Ericw        Date: 4/07/98    Time: 11:51a
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Changed the PlugInLoadSpecific to just PlugInLoad - too wordy.
// 
// *****************  Version 11  *****************
// User: Ericw        Date: 4/02/98    Time: 4:41p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// SUPPORT FOR PLUG-IN HOSTING !!!!!
// 
// *****************  Version 8  *****************
// User: Johnd        Date: 3/20/98    Time: 9:05a
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Updated  internal error handling. Added error string info to TLS data
// instead of the global buffers where it had been stored. Also added
// SetError() func to set the internal error state.
// 
// *****************  Version 7  *****************
// User: Ericw        Date: 2/03/98    Time: 12:15p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Changed ImgOpenEx to ImgOpenDialog with OPENFILENAME parameter.
// 
// *****************  Version 6  *****************
// User: Ericw        Date: 1/19/98    Time: 11:18a
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Common dialog flags and new ImgOpenEx prototype.
// 
// *****************  Version 5  *****************
// User: Ericw        Date: 9/23/97    Time: 5:09p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// New ImgReduceColorsPal & IMG_OCTREEPALETTE.
// 
// *****************  Version 4  *****************
// User: Ericw        Date: 7/28/97    Time: 2:58p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Added COPY_INTERPOLATE and changed filter kernel filter data type so VC
// 5.0 would stop complaining about it.
// 
// *****************  Version 3  *****************
// User: Ericw        Date: 7/16/97    Time: 3:12p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// new ImgContrast function declaration.
// 
// *****************  Version 2  *****************
// User: Ericw        Date: 7/10/97    Time: 2:43p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Implemented convolution kernels!!
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 7/10/97    Time: 2:04p
// Created in $/imageman 16-32/dll-api level 2
// Initial Lvl 2 Checkin
// 
// *****************  Version 8  *****************
// User: Timk         Date: 12/20/96   Time: 2:52p
// Updated in $/ImageMan 16-32/DLL
// Added SCAN_OK and SCAN_ERROR defines.
// 
// *****************  Version 7  *****************
// User: Johnd        Date: 2/19/96    Time: 10:27p
// Updated in $/ImageMan 16-32/DLL
// Added TWAIN Defines
// 
// *****************  Version 6  *****************
// User: Johnd        Date: 10/11/95   Time: 12:33p
// Updated in $/ImageMan 16-32/DLL
// Chgd __cdecl to __stdcall when WIN32 defined
// 
// *****************  Version 5  *****************
// User: Timk         Date: 7/27/95    Time: 12:25p
// Updated in $/ImageMan 16-32/DILS
// memory writing now correctly expands mem block when needed.
// 
// *****************  Version 4  *****************
// User: Timk         Date: 7/25/95    Time: 11:02p
// Updated in $/ImageMan 16-32/DILS
// Added writing using IO function blocks.
// 
// *****************  Version 3  *****************
// User: Timk         Date: 6/30/95    Time: 12:08p
// Updated in $/ImageMan 16-32/DLL
// Part 1 of massive changes for read/write using function blocks.
// 
// *****************  Version 2  *****************
// User: Johnd        Date: 2/01/95    Time: 5:54p
// Updated in $/ImageMan 16-32/DLL
// Added LOADDS define to WIN32 code
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 1/18/95    Time: 3:59p
// Created in $/ImageMan 16-32/DLL
// ImageMan 4.0 Beta 1

// 
// This include file is broken into several sections:
//      1. Defines
//      2. Structures
//      3. Macros
//      4. 

#ifndef __IMGMAN2_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "commdlg.h"

/////////////////////////////////////////////////////////////////////////////
// Defines
/////////////////////////////////////////////////////////////////////////////
#ifdef WIN32
#define IMAPI __stdcall
#define LOADDS
#else
#define IMAPI WINAPI
#endif

#ifndef LOADDS
#define LOADDS _loadds
#endif

// Error defines
#define IMG_ERR         0    // this means an error occurred
#define IMG_OK          1              
#define IMG_INV_FILE    2    // unsupported file type
#define IMG_FILE_ERR    3    // error reading from file
#define IMG_NOFILE      4    // file not found
#define IMG_NOTAVAIL    5    // info not available
#define IMG_NOMEM       6    // insufficient memory
#define IMG_CLOSED      7    // image file is closed
#define IMG_INV_HAND    8    // invalid image handle
#define IMG_NSUPPORT    9    // image option not supported
#define IMG_PROC_ERR    10   // error processing image
#define IMG_PRINT_ERR   11   // error printing image 
#define IMG_BAD_PRN     12   // printer don't support bitmaps
#define IMG_BAD_SRC     13   // specified invalid source rect
#define IMG_BAD_TYPE    14   // tried to use ImgGetDIB on vector img
#define IMG_NO_DILS     15   // found no DILs during initialization
#define IMG_INV_PAGE    16   // tried to seek to a non-existent page
#define IMG_NO_PROC     17   // Couldn't get * to DIL function
#define IMG_TOOBIG      18   // DIB would be > 16MB 
#define IMG_PASTECLIP   19	// Error pasting DIB or WMF onto clipboard
#define IMG_BLITERR     20   // Error StretchBlit'ing to dc
#define IMG_DIBERR      21   // Error Blit'ing DIB to dc
#define IMG_METAFILEERR 22   // Error Playing metafile to dc
#define IMG_CANCELLED   23   // Load was cancelled
#define IMG_BADBITMAP	   24   // Must pass a DibSection that was created with a hSection != NULL
#define IMG_INVFILTER    25   // Invalid filter specified
#define IMG_DLGCANCEL    26   // Common dialog open cancelled
#define IMG_BUFFTOOSMALL 27   // Open filename buffer too small
#define IMG_DLGERROR     28   // Common dialog error
#define	IMG_BAD_PARAM	 29	  // Bad Parameter passed to function	

// these added to support FaxManNet additional error reporting
// in the im32fax.dil
#define	IMG_INVWIDTH			30
#define IMG_INVCOMPRESSION		31
#define IMG_INVFILLORDER		32
#define IMG_INVGROUP3OPTIONS	33
#define IMG_INVNUMSTRIPS		34
#define IMG_INVSUBFILETYPE		35
//////////////////////////////////////////////////////////////

#define IMGPI_UNAVAIL	             40
#define IMGPI_ALREADYLOADED        41
#define IMGPI_NOTLOADED            42
#define IMGPI_CANTUNLOAD           43
#define IMGPI_ERRORLOADINGPLUGIN   44
#define IMGPI_NOPLUGINSELECTED     45
#define IMGPI_PLUGINERROR          46
#define IMGPI_ERRORENUMERATING     47
#define IMGPI_NOMEM                48
#define IMGPI_CANCELLED            49

#define IMGFX_INVALIDPARAM   100
#define IMGFX_INVALIDIMAGE   102
#define IMGFX_OUTOFMEMORY    103
#define IMGFX_OUTOFRESOURCES 104


// Image flags
#define IMG_RENDER_SELF 1
#define IMG_PRINT_SELF  2
#define IMG_PRNT_VECTOR 4
#define IMG_DISP_VECTOR 8       // 1 if image will display in vector format
#define IMG_MEM_BASED   16      // 1 if image not read from Disk
#define IMG_SCALE_SELF  32      //++Sygsky: 1 if image can scale itself 

// Raster-Op defines
#define IMG_TRANSPARENT (0xffffffff)

// Color Reduction Defines
#define IMG_BAYER          1
#define IMG_BURKES         2
#define IMG_FLOYD          4
#define IMG_GRAYSCALE      8
#define IMG_FIXEDPALETTE   16
#define IMG_OCTREEPALETTE  32
#define IMG_MEDIANCUT  	   64

//
// ImgCopy flags (for B&W images)
#define COPY_DEL           0    // pick out individual pixels (no AND or OR)
#define COPY_AND           0x01 // preserve black pixels
#define COPY_OR            0x02 // preserve white pixels
#define COPY_INTERPOLATE   0x04 // interpoloate between pixels
#define COPY_ANTIALIAS     0x10 // supersample to produce 16 level grayscale
#define COPY_ANTIALIAS256  0x20 // produce 256 level grayscale

// File I/O function block structure
typedef struct tagImgIO {
	// open - 
	//		performs the equivalent of the file open operation. The
	//		lpIn parameter allows an application to send any desired
	//		information to the open function. Open returns an LPVOID
	//		which enables the functions to maintain as much context
	//		information as needed. This allows each I/O group to do
	//		it's own buffering, for instance.
	//
	// close - 
	//		closes a given I/O context.
	//
	// error 
	//		Handles weird or non-standard error conditions. The function
	//		is expected to fill in the lpErrBuf parm with a text string
	//		defining the error, and return the I/O block-defined
	//		error value (which will be passed back to the application).
	//
	int	(*open)(LPVOID lpIn, unsigned uFlags, LPVOID FAR *lpHandle);	//opens the file
	int	(*close)(LPVOID lpHandle);						//closes the file
	DWORD	(*read)(LPVOID lpHandle, LPVOID hpBuf, DWORD dwBytes);
	DWORD (*write)(LPVOID lpHandle, LPVOID hpBuf, DWORD dwBytes);
	DWORD	(*seek)(LPVOID lpHandle, LONG lOffset, WORD nOrigin);
	int	(*getchar)(LPVOID lpHandle);
	int	(*error)(LPVOID lpHandle, LPSTR lpErrBuf);
} IMG_IO, FAR *LPIMG_IO;

/////////////////////////////////////////////////////////////////////////////
// Typedefs
/////////////////////////////////////////////////////////////////////////////

// convolution filter stuff
typedef struct FILTERKERNEL
	{
		int   iScale   ;
		int   iOffset  ;
		int   iWidth   ;
		int   iHeight  ;
		int FAR * lpFilter ;
	} FILTERKERNEL ;

typedef FILTERKERNEL FAR * LPKERNEL ;

// pre-defined filter convolutions
typedef enum
	{
	  IMG_SHARPEN = 0  ,
		IMG_BLUR         ,
		IMG_EMBOSS       ,
		IMG_EDGEDETECT1  ,
		IMG_EDGEDETECT2  ,
		IMG_VERTEDGE     ,
		IMG_HORIZEDGE    ,
		IMG_DESPECKLE    ,
		IMG_LAPLACESHARP
	} enumFilter ;

// contrast stuff
#define RGB_BLUE    1
#define RGB_GREEN   2
#define RGB_RED     4
#define RGB_FULL  RGB_BLUE|RGB_GREEN|RGB_RED

// status function typedef
typedef int (WINAPI *STATUSPROC)(HANDLE,UINT,DWORD);

// prototypes for ImageMan functions
int    IMAPI ImgInit(void);
int    IMAPI ImgClose(HANDLE);
int    IMAPI ImgGetStatus(void);
int    IMAPI ImgErrBox(HWND);
int    IMAPI ImgErrString(LPSTR, int);
int    IMAPI ImgVerString(LPSTR, int);
void   IMAPI ImgShutdown(void);
HANDLE IMAPI ImgGetDIB(HANDLE, BOOL, LPRECT);
HANDLE IMAPI ImgGetWMF(HANDLE, LPRECT);
HANDLE IMAPI ImgOpenSolo(LPCSTR, LPCSTR);
HANDLE IMAPI ImgOpenDialog(LPOPENFILENAME, long);
HANDLE IMAPI ImgOpenMem(LPCSTR, DWORD, LPCSTR);
HANDLE IMAPI ImgGetPalette(HANDLE);
int    IMAPI ImgSetROP(HANDLE,DWORD);
DWORD  IMAPI ImgGetROP(HANDLE);
int    IMAPI ImgDrawImage(HANDLE, HDC, LPRECT, LPRECT);
LPBITMAPINFO  IMAPI ImgGetInfo(HANDLE, LPINT);
HANDLE IMAPI ImgGetDDB(HANDLE,HDC);
HANDLE IMAPI ImgFromDIB(HANDLE);
HANDLE IMAPI ImgFromWMF(HANDLE,LPRECT);
HANDLE IMAPI ImgFromClipboard(void);
int    IMAPI ImgToClipboard(HANDLE);
LPSTR  IMAPI ImgGetExt(void);
HANDLE IMAPI ImgLoadLibraryExt(LPSTR);
int    IMAPI ImgLoad(HANDLE, LPRECT);
int    IMAPI ImgUnload(HANDLE, BOOL, BOOL, BOOL);
int    IMAPI ImgCreateDDB(HANDLE,HDC,int,int);
int    IMAPI ImgPageCount(HANDLE, LPINT);
int    IMAPI ImgSetPage(HANDLE, int);
int    IMAPI ImgGetPage(HANDLE, LPINT);
HANDLE IMAPI ImgRotate(HANDLE, UINT, COLORREF);
int    IMAPI ImgBrightness(HANDLE, int);
int    IMAPI ImgGamma(HANDLE, int);
int    IMAPI ImgInvert(HANDLE hImage);
int    IMAPI ImgContrast( HANDLE, int iPerc, int iMedian, int iChannel) ;
int    IMAPI ImgMirror( HANDLE, BOOL, BOOL );
int    IMAPI ImgFilter( HANDLE, enumFilter ) ;
int    IMAPI ImgFilterLow( HANDLE, LPKERNEL, int iChannel ) ;

HANDLE IMAPI ImgReduceColors(HANDLE,UINT,DWORD);
HANDLE IMAPI ImgReduceColorsPal(HANDLE,DWORD,HPALETTE );
HANDLE IMAPI ImgIncreaseColors(HANDLE, WORD);

HANDLE IMAPI ImgCopy(HANDLE,int,int,LPRECT,DWORD);

int    IMAPI ImgPrintImage(HANDLE, HDC, LPRECT, LPRECT);
HANDLE IMAPI ImgExt(void);
HANDLE IMAPI ImgOpenEmbedded(LPCSTR,long,long,LPCSTR);
void   IMAPI ImgSetDefaultStatusProc(STATUSPROC,long,DWORD);
int    IMAPI ImgSetStatusProc(HANDLE,STATUSPROC,long,DWORD);

HANDLE    IMAPI ImgGetHBitmap( HANDLE );
LPVOID	IMAPI ImgGetDIBPtr( HANDLE );
LPVOID	IMAPI ImgGetDataPtr( HANDLE );
HANDLE	IMAPI ImgFromDIBSection( HBITMAP );

int IMAPI ImgGetInfoStruct(HANDLE hImage, LPBITMAPINFOHEADER lpbi, LPINT lpbFlags);

HANDLE IMAPI ImgOpenCustom(LPVOID lpOpenInfo, WORD wSize, LPIMG_IO lpIO, LPSTR lpszImgIDStr, LPCSTR lpExt);

// ///////////////////////////////////////////////////////////////
// prototype for the Plug-IN features
// ///////////////////////////////////////////////////////////////

#define IM_PI_ANYTYPE     0X00000001
#define IM_PI_COLORPICKER 0X00000002
#define IM_PI_IMPORT      0X00000004
#define IM_PI_EXPORT      0X00000008
#define IM_PI_EXTENSION   0X00000010
#define IM_PI_FILTER      0X00000020
#define IM_PI_FILEFORMAT  0X00000040
#define IM_PI_PARSER      0X00000080
#define IM_PI_SELECTION   0X00000100
#define IM_PI_TYPECOUNT   9

typedef long PLUGINTYPE ;

typedef BOOL (IMAPI * PIIMENUMCALLBACKPROC)(LPSTR,LPSTR,LPSTR,LONG) ;

BOOL   IMAPI ImgPlugInHostIsAvailable () ;
BOOL   IMAPI ImgPlugInHostIsEnabled   () ;
int    IMAPI ImgPlugInHostLoad    ( LPSTR lpPlugInDir ) ;
int    IMAPI ImgPlugInHostUnload  () ;
int		 IMAPI ImgPlugInAbout ( HWND hOwner, LPSTR lpPlugIn ) ;
int    IMAPI ImgPlugInLoad ( LPSTR lpPlugInName ) ;
int    IMAPI ImgPlugInInvoke    ( HANDLE hImage, HWND hOwner, LPRECT rect ) ;
int    IMAPI ImgPlugInInvokeEx    ( HANDLE hImage, HWND hOwner, LPRECT rect, LPBYTE * lpData, BOOL bUI ) ;
int    IMAPI ImgPlugInEnumerate ( PIIMENUMCALLBACKPROC enumProc, LONG lPIType, LONG lParam ) ;
int    IMAPI ImgPlugInSetStatusProc ( STATUSPROC lp, LONG lInt, DWORD dwUser ) ;

// macros
#define PALETTESIZE(lpbi) (((lpbi)->biClrUsed ? (lpbi)->biClrUsed : ((1<<(lpbi)->biBitCount)&0x01ff))*sizeof(RGBQUAD))
#define PALCOLORS(lpbi)   (((lpbi)->biClrUsed ? (lpbi)->biClrUsed : ((1<<(lpbi)->biBitCount)&0x01ff)))
#define BITSOFFSET(lpbi)  (sizeof(BITMAPINFOHEADER) + PALETTESIZE(lpbi))
#define ROWSIZE(w,bits)   (((w)*(bits)+31)/32*4)
#define RECTWID(r)        ((r)->right-(r)->left+1)
#define RECTHI(r)         ((r)->bottom-(r)->top+1)

#define TWAIN_BW				0x0001		// 1-bit per pixel, B&W 	(equiv. to TWPT_BW)
#define TWAIN_GRAY				0x0002		// 1,4, or 8-bit grayscale  (equiv. to TWPT_GRAY)
#define TWAIN_RGB				0x0004		// 24-bit RGB color         (equiv. to TWPT_RGB)
#define TWAIN_PALETTE			0x0008		// 1,4, or 8-bit palette    (equiv. to TWPT_PALETTE)
#define TWAIN_ANYTYPE			0x0000		// any of the above
#define TWAIN_USEADF			0x0001
#define TWAIN_SHOWUI			0x0002
#define SCAN_OK					1
#define SCAN_ERROR				0

int IMAPI ScanSelectSource(HWND hwnd);
HANDLE IMAPI ScanAcquirePage(HWND hwndApp, WORD wPixTypes);
int IMAPI ScanIsTWAINAvailable(void);

// ImageMan FileOpen Dialog flags
#define IMG_OFN_DIALOG         0x00000001
#define IMG_OFN_ALLOWTHUMBOPT	 0x00000002
#define IMG_OFN_SHOWTHUMB      0x00000004
#define IMG_OFN_ALLOWSTATSOPT  0x00000008
#define IMG_OFN_SHOWSTATS      0x00000010

#define IMG_OFN_FULL  (IMG_OFN_DIALOG|IMG_OFN_ALLOWTHUMBOPT|IMG_OFN_SHOWTHUMB|IMG_OFN_ALLOWSTATSOPT|IMG_OFN_SHOWSTATS)
#define IMG_OFN_OPTIONS (IMG_OFN_FULL-IMG_OFN_DIALOG)

// ImageMan Export stuff

#define IMGX_ERR          0
#define IMGX_OK           1
#define IMGX_NO_DELS    202
#define IMGX_INV_FILE   203  // unsupported file type
#define IMGX_FILE_ERR   204  // error writing file
#define IMGX_NOMEM      205  // insufficient memory
#define IMGX_INV_HAND   206  // invalid image handle
#define IMGX_NSUPPORT   207  // image option not supported
#define IMGX_BAD_DEL    208  // couldn't load the DEL
#define IMGX_DISK_FULL  209  // disk full when writing
#define IMGX_EXISTS     210  // file exists
#define IMGX_NO_OPEN    211  // error opening file
#define IMGX_DELETE_ERR 212  // error deleting file
#define IMGX_ABORTED    213  // user aborted export
#define IMGX_BADDIB     214  // Can't lock hDIB in ImgXWriteDIB
#define IMGX_CANCELLED  215	// Load was cancelled

// Define xinfo struct

typedef struct _XINFO {
	long    lFlags;         // flags pertaining to an image
	long    lProcessed;  	// # of source bytes processed
	long    lTotalBytes;		// # of total source bytes to process
	long    lWritten;       // # of bytes written to export file
	long    lIncrement;     // call status func after this many bytes processed
	long    lIncCnt;        // increment counter
	DWORD   dwUserInfo;     // user info passed to status proc
	int     (WINAPI *lpfnStat)(HANDLE, unsigned int, DWORD);
} XINFO;

typedef XINFO FAR *LPXINFO;

// define flags for use in XINFO struct's lFlags member
#define XF_BACKWARD     0x01    // if on, last row written first (like DIB)
#define XF_EXPORTOPTS   0x02    // if on, the format has export options
#define XF_MULTIPAGE    0x04    // if on, format supports multiple images
#define XF_MONOCHROME   0x08    // 1 if can export mono images
#define XF_4BIT         0x10    // 1 if can export 4-bit (16-color) images
#define XF_8BIT         0x20    // 1 if can export 8-bit (256 color) images
#define XF_24BIT        0x40    // 1 if can export 24-bit images
#define XF_PALETTE      0x80    // 1 if can include a palette (otherwise it's grayscale)
#define XF_ALLCOLORS    XF_MONOCHROME | XF_4BIT | XF_8BIT | XF_24BIT | XF_PALETTE

// ImgXWriteDIB defines
#define IMGXOPT_FILE_PROMPT	0x0001
#define IMGXOPT_OVERWRITE    0x0002
#define IMGXOPT_COMPRESS     0x0004
#define IMGXOPT_OPT_PROMPT   0x0008
#define IMGXOPT_OVERWRITE_PROMPT 0x00010
#define IMGXOPT_SAVE_PROMPT      0x00020

// ImgMan/X callback functions for use by DELs (and ImageMan/X)
void IMAPI LOADDS ImgXSetError(HANDLE,int,LPCSTR,LPCSTR);
typedef void (IMAPI *XERRORPROC)(HANDLE, int, LPCSTR, LPCSTR);

typedef struct tagXInitStruct {
	long    lFlags;
	XERRORPROC SetError;
	HANDLE  hImgManInst;    // instance of ImageMan DLL
} XINITSTRUCT, FAR *LPXINITSTRUCT;

// Define bitmapinfo struct which contains entries for all 256 pal entries
typedef struct _BITMAPINFO256 {
	BITMAPINFOHEADER        bmiHeader;
	RGBQUAD              	pal[256];
} BITMAPINFO256;


/////////////////////////////////////////////////////////////////////
// Prototypes
/////////////////////////////////////////////////////////////////////
int    IMAPI ImgXInit(void);
int    IMAPI ImgXGetExt(LPSTR);
HANDLE IMAPI ImgXBeginWrite(LPCSTR,LPBITMAPINFO,HANDLE,LPXINFO);
int    IMAPI ImgXWriteBlock(HANDLE,int,LPSTR,LPXINFO);
int    IMAPI ImgXEndWrite(HANDLE,LPXINFO);
HANDLE IMAPI LOADDS ImgXOptBlkCreate(LPSTR);
HANDLE IMAPI LOADDS ImgXOptBlkAdd(HANDLE,LPSTR,LPSTR);
int    IMAPI LOADDS ImgXOptBlkGet(HANDLE,LPSTR,LPSTR);
int    IMAPI LOADDS ImgXOptBlkDel(HANDLE);
int    IMAPI ImgXAbort(HANDLE);
HANDLE IMAPI ImgXFileDialog(HANDLE,HWND);
int    IMAPI ImgXWriteBMPBlock(HANDLE,HDC,HBITMAP,int,LPXINFO);

int    IMAPI ImgXWriteDIB(LPCSTR,LPCSTR,HANDLE,HANDLE,HWND,DWORD);
int    IMAPI ImgXWriteImage(LPCSTR,LPCSTR,HANDLE,HANDLE,HWND,DWORD);

HANDLE IMAPI ImgXBeginWriteMem(LPCSTR, LPBITMAPINFO, HANDLE, LPXINFO, HANDLE, DWORD);
int IMAPI ImgXEndWriteMem(HANDLE, LPXINFO, HANDLE FAR *, DWORD FAR *);

LPSTR  IMAPI IMXDLget_ext(LPXINITSTRUCT);
HANDLE IMAPI IMXDLbegin_write(LPVOID, LPIMG_IO, LPCSTR, LPBITMAPINFO, HANDLE, LPXINFO );
int    IMAPI IMXDLwrite_block( HANDLE, int, int, LPSTR, LPXINFO );
int    IMAPI IMXDLend_write( HANDLE, LPXINFO );
LPSTR  IMAPI IMXDLget_ver( void );

// Archaic ImgX functions - These are here for compatibility purposes only
// use the ImgErrBox, ImgErrString and ImgGetStatus calls instead
int    IMAPI ImgXErrBox(HWND);
void   IMAPI ImgXErrString(LPSTR);
int    IMAPI ImgXGetStatus(void);


#ifndef IMFXDECL
#ifdef WIN32
#define IMFXDECL __declspec(dllimport)
#else
#define IMFXDECL
#endif
#endif

// Here's the list of supported image effects. Immediately after each
// effect family is a list of effect modifiers (flags) appropriate for
// that family. Enjoy!
enum tag_ImageManDrawEffects {
	// NONE: Draw the image with no effects (like ImgDrawImage)
	IMFX_NONE,

#define IMDF_NONE           0 // generic flag for all effects

	// WIPE: Image wipes across DC
	IMFX_WIPELEFT,            // ...from right edge
	IMFX_WIPERIGHT,           // ...from left edge
	IMFX_WIPEUP,              // ...from bottom
	IMFX_WIPEDOWN,            // ...from top
#define IMDF_WIPE_IMGSLIDE  0     // image slides into view <default>
#define IMDF_WIPE_IMGEXPOSE 1     // image is exposed in-place
#define IMDF_WIPE_PUSHBKGND 0     // new image pushes background out <default>
#define IMDF_WIPE_OVERBKGND 2     // new image floats above background

	// CURTAIN: Image displayed about a central axis (like stage curtain)
	IMFX_VCURTAIN_OUT,        // vertical curtain, from middle
	IMFX_VCURTAIN_IN,         // vertical curtain, to middle
	IMFX_HCURTAIN_OUT,        // horizontal curtain, from middle
	IMFX_HCURTAIN_IN,         // horizontal curtain, to middle
#define IMDF_CURTAIN_IMGSLIDE  0  // image slides 
#define IMDF_CURTAIN_IMGEXPOSE 1  // image exposed in-place <default>
#define IMDF_CURTAIN_PUSHBKGND 0  // background pushed out
#define IMDF_CURTAIN_OVERBKGND 2  // image appears over background <default>

	// MOSAIC: Image displayed via random rectangles 
	IMFX_MOSAIC,              // ...spread over entire area
	IMFX_MOSAICLEFT,          // ...creeping from the right edge
	IMFX_MOSAICRIGHT,         // ...creeping from the left edge
	IMFX_MOSAICUP,            // ...creeping from the bottom
	IMFX_MOSAICDOWN,          // ...creeping from the top

	// BLUR: Zooms in and out of focus
	IMFX_BLUR_IN,             // blur image in (from bad to good)
	IMFX_BLUR_OUT,            // blur image out (from good to bad)

	// SHAPES: Image displayed using various shapes
	IMFX_SHAPE_EXPLODE,
	IMFX_SHAPE_IMPLODE,
#define IMDF_SHAPE_RECTANGLE      0
#define IMDF_SHAPE_ROUNDRECTANGLE 1
#define IMDF_SHAPE_ELLIPSE        2

	// BLIND: Venetian blind effect
	IMFX_VBLIND_LEFT,
	IMFX_VBLIND_RIGHT,
	IMFX_VBLIND_UP,
	IMFX_VBLIND_DOWN,
#define IMDF_VBLIND_IMGSLIDE  0  // image slides 
#define IMDF_VBLIND_IMGEXPOSE 1  // image exposed in-place <default>
#define IMDF_VBLIND_PUSHBKGND 0  // background pushed out
#define IMDF_VBLIND_OVERBKGND 2  // image appears over background <default>

	IMFX_ENUM                 // number of effects supported
};



// IMFXCALLBACK
//
// This user provided routine is called between every FX operation. It
// can be used for FX timing as well as tracking display progress.
// It currently shares the same format as ImageMan's STATUSPROC for user
// convenience.
//
// Parameters:
//   hImg       - handle of image being drawn
//   nPercent   - percent of process completed
//   dwUserInfo - user data passed into ImgDrawImageFX()
// Returns:
//   The user should return 0 (zero) to continue processing, or an error
//   code to stop. Any error returned from the callback will also be
//   returned by ImgDrawImageFX().
//
typedef int (WINAPI *IMFXCALLBACK)(HANDLE hImg,UINT nPercent,DWORD dwUserInfo);


// ImgDrawImageFX
//
// Draws an image to the given DC using the specified drawing effect.
// The image will be loaded if necessary. A vector image will draw
// with IMFX_NONE *unless* a rendered bitmap (DDB) is available (use
// ImgCreateDDB).
//
// Clip Regions:
//   Any active clip region in the given DC will be maintained. Note, however,
// that an internal *copy* is used in some effects. If you modify the DC's
// clip region in the callback, the region may or may not be restored to that
// copy at some point. We advise the clip region not be modified during
// an effect.
//
// Parameters:
//   hImg        - ImageMan image handle. If NULL, the specified effect
//                 is applied using the DC's background brush.
//   hDC         - destination DC
//   pDstBounds  - specifies destination location and size in logical 
//                 coordinates.
//   pSrcBounds  - specifies source pixels to display. If NULL, use entire
//                 source.
//   nEffect     - one of IMFX_* listed above
//   dwFlags     - one of IMDF_* listed above, a per-effect modifier
//   dwExtra     - per-effect extra data (low/high word) [range] <default>
//      Slide           : step increment <1>  / unused
//      Wipe            : step increment <1>  / unused
//      Mosaic          : cell width <1>      / cell height <cell width>
//      Creeping Mosaic : square cell size <1>/ fill percent [0..100]
//      Curtain         : step increment <1>  / unused
//      Venetian Blind  : step increment <1>  / slat size <10>
//   pfnCallback - address of a callback (status) function, to be called
//                 between every effect operation (may be NULL)
//   nInterval   - number of percentage points between each callback
//                 invocation (e.g. '5' will callback at 5%,10%,15%,...)
//                 Valid values are in the range [0..100], where zero will
//                 callback after every single atomic operation (lots!).
//   dwUserInfo  - data to be passed inn to your callback function
// Returns:
//   0 (zero) if successful, otherwise the error code. The error code may
//   have been supplied by your callback.
//

int IMAPI ImgDrawImageFX(HANDLE hImg,
	HDC hDC,LPRECT pDstBounds,LPRECT pSrcBounds,
	UINT nEffect,DWORD dwFlags,DWORD dwExtra,
	IMFXCALLBACK pfnCallback,UINT nInterval,DWORD dwUserInfo, HPALETTE hPal);

#ifdef __cplusplus
}
#endif

#define __IMGMAN2_H__
#endif

