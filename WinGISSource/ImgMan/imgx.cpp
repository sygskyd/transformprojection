/////////////////////////////////////////////////////////////////////////////
//
// imgx.c
//
// ImageMan Image Export Code
//
// Copyright (c) 1991-5 Data Techniques, Inc.
// All Rights Reserved
//
// ///////////////////////////////////////////////////////////////////////////
// $History: imgx.cpp $
// 
// *****************  Version 8  *****************
// User: Ericj        Date: 7/14/99    Time: 5:22p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Added PPP demo stuff.
// 
// *****************  Version 7  *****************
// User: Johnd        Date: 3/20/98    Time: 2:41p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Chgd Aborted error code to IMG_DLGCANCEL in imgXwriteDIB
// 
// *****************  Version 6  *****************
// User: Johnd        Date: 3/20/98    Time: 9:05a
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Updated  internal error handling. Added error string info to TLS data
// instead of the global buffers where it had been stored. Also added
// SetError() func to set the internal error state.
// 
// *****************  Version 5  *****************
// User: Johnd        Date: 3/18/98    Time: 12:34p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Added return code for handling vector images in ImgXwriteImage().
// 
// *****************  Version 4  *****************
// User: Johnd        Date: 2/06/98    Time: 10:49a
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Fixed DEMO build code
// 
// *****************  Version 3  *****************
// User: Ericw        Date: 2/03/98    Time: 12:28p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Moved all the common dialog stuff out of this module an into the
// cdialog module.
// 
// *****************  Version 2  *****************
// User: Ericw        Date: 1/19/98    Time: 11:33a
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Common dialog support for saving.
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 7/10/97    Time: 2:03p
// Created in $/imageman 16-32/dll-api level 2
// Initial Lvl 2 Checkin
// 
// *****************  Version 12  *****************
// User: Johnd        Date: 9/25/96    Time: 1:26p
// Updated in $/ImageMan 16-32/DLL
// Added Support for Demo Build
// 
// *****************  Version 11  *****************
// User: Johnd        Date: 9/19/96    Time: 12:04p
// Updated in $/ImageMan 16-32/DLL
// Fixed Bug 403 (Long FileName issues)
// 
// *****************  Version 10  *****************
// User: Johnd        Date: 9/10/96    Time: 5:10p
// Updated in $/ImageMan 16-32/DLL
// Fixed Bug #417
// 
// *****************  Version 9  *****************
// User: Johnd        Date: 6/07/96    Time: 4:07p
// Updated in $/ImageMan 16-32/DLL
// Fixed Bug in BeginWriteInternal if bad format extension was specified
// 
// *****************  Version 8  *****************
// User: Johnd        Date: 4/04/96    Time: 3:43p
// Updated in $/ImageMan 16-32/DLL
// Fixed typo in last update
// 
// *****************  Version 7  *****************
// User: Johnd        Date: 4/04/96    Time: 3:40p
// Updated in $/ImageMan 16-32/DLL
// Removed SaveAsHook proc.
// 
// *****************  Version 6  *****************
// User: Timk         Date: 1/29/96    Time: 12:59a
// Updated in $/ImageMan 16-32/DLL
// Removed some #debug lines that no longer make sense.
// 
// *****************  Version 5  *****************
// User: Timk         Date: 7/27/95    Time: 12:25p
// Updated in $/ImageMan 16-32/DLL
// memory writing now correctly expands mem block when needed.
// 
// *****************  Version 4  *****************
// User: Timk         Date: 7/25/95    Time: 11:07p
// Updated in $/ImageMan 16-32/DLL
// Added writing using IO function blocks.
// 
// *****************  Version 3  *****************
// User: Timk         Date: 5/22/95    Time: 5:16p
// Updated in $/ImageMan 16-32/DLL
// Fixed bug w/period in directories for file saves.
// 
// *****************  Version 2  *****************
// User: Johnd        Date: 5/17/95    Time: 9:10a
// Updated in $/ImageMan 16-32/DLL
// Added code to call SETXSTATUS in ImgXSetError
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 1/18/95    Time: 4:18p
// Created in $/ImageMan 16-32/DLL
// ImageMan 4.0 Beta 1
//

#include "internal.h"
#pragma hdrstop
#include <assert.h>
#include <string.h>
#include <commdlg.h>
#include <dlgs.h>               // more commdlg includes
#include <stdlib.h>
#include "imgio.h"
#include "imgmanrc.h"

#ifndef WIN32
#include <direct.h>
#include <dos.h>
#include "windos.h"
#endif

#include "optblk.h"

#define MAXEXTS 20

#ifdef WIN32
#define DELFSPEC "im31x*.del"
#else
#define DELFSPEC "im11x*.del"
#endif

extern BOOL DisplayEvalDlg(void);
extern LPDWORD dLastIOError; // Sygsky

typedef struct _XJOB {
	char        sentinel[3];
	LPSTR        lpszFullName;		// full pathname to image
	HANDLE      hImage;			  	// handle to the image we're exporting
	HINSTANCE   hDLL;				// handle to DLL to write this image
	HANDLE      hOpt;				// handle to options for this image
	UINT        nRowSize;		  	// size of a row of bytes for this image
	BOOL        bDel;				// 1 if want to remove file
	LPMEM_IO		pMEMIO;			// if used, points to MEM_IO struct used for in-memory writing
	BITMAPINFO256 bmi256;
}           XJOB, NEAR * NPXJOB, FAR * LPXJOB;

typedef struct tagEXTinfo {
	char        ext[4];			  	// ext. supported (w/ terminating null)
	char        DLL[100];		  	// path & name of DLL for this ext
	char        ver[32];			// Version string for this ext
	char        desc[100];		  	// descriptive text for this DLL
	char        mask[8];			// search mask for Save dialog
	DWORD       lFlags;			  	// flags for this extension
}           EXTinfo;

static int NEAR XaddExtension(LPSTR, LPSTR, LPSTR, LPSTR, DWORD);
static void NEAR Xget_ext(LPSTR, LPSTR);
static LPSTR NEAR Xfind_ext(LPSTR);
static long NEAR Xfind_flags(LPSTR ext);
static LPSTR NEAR gen_fullname(LPCSTR, HANDLE);
static int PASCAL XDibNumColors(LPBITMAPINFOHEADER);
static void NEAR XSetOptBlkOpts ( HANDLE, DWORD, LPBITMAPINFOHEADER ) ;
extern BOOL IsLicensed();

// ImageMan status
#ifndef WIN32
extern int  img_status;
#endif

extern HINSTANCE hInst;			  // who we is in this life... available to all!

// standard IO function blocks
extern IMG_IO file_ioblk;
extern IMG_IO mem_ioblk;

/*static*/ LPSTR lpXExtString;		  // list of extensions

// TODO
// static char ErrModule[30], ErrString[150], ErrFile[MAX_PATH];
//#ifndef WIN32
//extern int         img_status;
//#endif

// process specific globals
static EXTinfo XEXTList[MAXEXTS];	// room for 20 - go to Dynamic allocation later

extern LPINTERNALINFO NEAR PASCAL GetImgPtr(HANDLE);

#ifdef __cplusplus
extern "C" {
#endif
UINT CALLBACK LOADDS ImgSaveProc ( HWND hDlg, UINT message,
												 WPARAM wParam, LPARAM lParam ) ;
#ifdef __cplusplus
	}
#endif

int IMAPI   ImgXWriteDIB(LPCSTR lpszFileName, LPCSTR lpszExt, HANDLE hDIB,
								             HANDLE hOptions, HWND hWnd, DWORD lOpts)
{
	XINFO       xInfo;
	LPSTR       lpBits;
	LPBITMAPINFOHEADER lpbi;
	int         nColors;
	int         nPaletteBytes;
	HANDLE      hJob;
	HANDLE      hOptBlk;
	int         retval = IMGX_OK;
	int         bSaveOptBlk = 0;
	LPTHREADINFO lpInfo = GetThreadInfo();

	if (!(IsLicensed()))
	{
		if( !DisplayEvalDlg() )
			return IMGX_ERR;
	}

	if (lpInfo) {
		xInfo.lpfnStat = lpInfo->lpfnDefaultStat;
		xInfo.lIncrement = lpInfo->lDefaultInterval;
		xInfo.dwUserInfo = lpInfo->dwDefaultUserInfo;
	} else {
		xInfo.lpfnStat = NULL;	  // if this is NULL, none of the rest matters

	}

	if (hOptions) {
		hOptBlk = hOptions;
		bSaveOptBlk = 1;
	} else
		hOptBlk = ImgXOptBlkCreate(NULL);

	if (lpszExt)
		OptBlkAdd(hOptBlk, "EXTENSION", lpszExt);

	if (!(lpbi = (LPBITMAPINFOHEADER) GlobalLock(hDIB))) {
		SetError(lpszFileName, IMGX_BADDIB, 0, NULL, hInst);
		return (IMGX_ERR);
	}
	nColors = XDibNumColors(lpbi);

	if (lOpts & (IMGXOPT_FILE_PROMPT|IMGXOPT_SAVE_PROMPT) ) {
		XSetOptBlkOpts ( hOptBlk, lOpts, lpbi ) ;
		if (!ImgXFileDialog(hOptBlk, hWnd)) {
			OptBlkDel(hOptBlk);
			SetError(lpszFileName, IMGX_ABORTED, 0, NULL, hInst);

			return (IMGX_ERR);
		}
	}
	if (lOpts & IMGXOPT_OVERWRITE)
		OptBlkAdd(hOptBlk, "OVERWRITE", "ON");
	else if (lOpts & IMGXOPT_OVERWRITE_PROMPT)
		OptBlkAdd(hOptBlk, "OVERWRITE", "PROMPT");

	nPaletteBytes = nColors * (lpbi->biSize == sizeof(BITMAPCOREHEADER) ? sizeof(RGBTRIPLE) : sizeof(RGBQUAD));
	lpBits = (LPSTR) lpbi + lpbi->biSize + nPaletteBytes;

	hJob = ImgXBeginWrite(lpszFileName, (LPBITMAPINFO) lpbi, hOptBlk, &xInfo);

	if (!bSaveOptBlk)
		ImgXOptBlkDel(hOptBlk);

	if (!hJob) {
		GlobalUnlock(hDIB);
		return (IMGX_ERR);
	}
	if (ImgXWriteBlock(hJob, (int) lpbi->biHeight, lpBits, &xInfo)) {
		if (!ImgXEndWrite(hJob, &xInfo))
			retval = IMGX_ERR;
	} else
		retval = IMGX_ERR;

	GlobalUnlock(hDIB);

	return (retval);
}

int IMAPI   ImgXWriteImage(LPCSTR lpszFileName, LPCSTR lpszExt, HANDLE hImage, HANDLE hOptions, HWND hWnd, DWORD lOpts)
{
	XINFO       xInfo;
	LPSTR       lpBits;
	LPBITMAPINFO lpbi;
	HANDLE      hJob;
	HANDLE      hOptBlk;
	int         retval = IMGX_OK;
	int         bSaveOptBlk = 0;
	LPTHREADINFO lpInfo = GetThreadInfo();
	LPINTERNALINFO img;

	if (!(IsLicensed()))
	{
		if( !DisplayEvalDlg() )
			return IMGX_ERR;
	}

	if (!(img = GetImgPtr(hImage))) {
		SetError(lpszFileName, IMGX_BADDIB, 0, NULL, hInst);
		return (IMGX_ERR);
	}

	if (img->bFlags & IMG_DISP_VECTOR) {
		SetError(lpszFileName, IMGX_NSUPPORT, 0, NULL, hInst);
		return (IMGX_ERR);
	}

	if (lpInfo) {
		xInfo.lpfnStat = lpInfo->lpfnDefaultStat;
		xInfo.lIncrement = lpInfo->lDefaultInterval;
		xInfo.dwUserInfo = lpInfo->dwDefaultUserInfo;
	} else {
		xInfo.lpfnStat = NULL;	  // if this is NULL, none of the rest matters

	}

	if (hOptions) {
		hOptBlk = hOptions;
		bSaveOptBlk = 1;
	} else
		hOptBlk = ImgXOptBlkCreate(NULL);

	if (lpszExt)
		OptBlkAdd(hOptBlk, "EXTENSION", lpszExt);

	if (lOpts & IMGXOPT_OVERWRITE)
		OptBlkAdd(hOptBlk, "OVERWRITE", "ON");
	else if (lOpts & IMGXOPT_OVERWRITE_PROMPT)
		OptBlkAdd(hOptBlk, "OVERWRITE", "PROMPT");

	if( !img->pDIB || !img->pDIB->IsValid() ) {
		// Load it
		InternalLoad(img, NULL, &img->pDIB, &img->hWMF );
	}

	if( !img->pDIB->IsValid() )
		return IMGX_ERR;

	lpbi = img->pDIB->GetLPBI();

	if (lOpts & (IMGXOPT_FILE_PROMPT|IMGXOPT_SAVE_PROMPT)) {
		XSetOptBlkOpts ( hOptBlk, lOpts, (LPBITMAPINFOHEADER)lpbi ) ;
		if (!ImgXFileDialog(hOptBlk, hWnd)) {
			OptBlkDel(hOptBlk);
			SetError(lpszFileName, IMG_DLGCANCEL, 0, NULL, hInst);

			return (IMGX_ERR);
		}
	}
	lpBits = img->pDIB->GetDataPtr();

	hJob = ImgXBeginWrite(lpszFileName, lpbi, hOptBlk, &xInfo);

	if (!bSaveOptBlk)
		ImgXOptBlkDel(hOptBlk);

	if (!hJob) {
		return (IMGX_ERR);
	}
	if (ImgXWriteBlock(hJob, (int) lpbi->bmiHeader.biHeight, lpBits, &xInfo)) {
		if (!ImgXEndWrite(hJob, &xInfo))
			retval = IMGX_ERR;
	} else
		retval = IMGX_ERR;

	return retval;
}


static int PASCAL XDibNumColors(LPBITMAPINFOHEADER lpbi)
{
	int         c;

	if (lpbi->biClrUsed && lpbi->biBitCount < 24)
		return ((int) lpbi->biClrUsed);

	// 1->2, 4->16, 8->256, 24->0
	c = (1 << lpbi->biBitCount);
	return (c & 0x0FFF);
}

HANDLE BeginWriteInternal(LPVOID lpOpenInfo, LPIMG_IO lpIO, LPCSTR lpszAlias, LPSTR lpszExt, LPBITMAPINFO lpDIB, HANDLE hOptBlk, LPXINFO lpInfo)
{
	DLLxbegin_write lpFunc;
	DLLxget_ext lpfunc_ext;
	HANDLE      hJob, hDLL;
	NPXJOB      npJob;
	LPSTR 		lpDLLName;

	hJob = NULL;
	npJob = NULL;

	//
	// Load the DEL...
	//
	
	lpDLLName = Xfind_ext(lpszExt);


	// lpDLLName = NULL if the format type wasnt found so return
	if( !lpDLLName ) {
		SetError(lpszAlias, IMGX_INV_FILE, 0, NULL, hInst);
		return NULL;
	}


	hDLL = LoadLibraryPath( lpDLLName );

	// Ugly, but effective...
	if (lpfunc_ext = (DLLxget_ext) GetProcAddress((HINSTANCE) hDLL, XGET_EXT)) {
		XINITSTRUCT xis;

		xis.SetError = ImgXSetError;
		xis.hImgManInst = hInst;
		(*lpfunc_ext) (&xis);
	}

	if (hDLL) {
if (hJob = LocalAlloc(LMEM_FIXED, sizeof(XJOB))) {
			npJob = (NPXJOB) LocalLock(hJob);
			if (lpFunc = (DLLxbegin_write) GetProcAddress((HINSTANCE) hDLL, XBEGIN_WRITE)) {
				lpInfo->lProcessed = lpInfo->lWritten = lpInfo->lIncCnt = 0L;
				npJob->hImage = (*lpFunc) (lpOpenInfo, lpIO, lpszAlias, lpDIB, hOptBlk, lpInfo);
			}
			if (npJob->hImage) {
				npJob->hDLL = (HINSTANCE) hDLL;
				npJob->hOpt = hOptBlk;
				// CAR: couldn't resist... could ya?
				// THK: resist what?
				npJob->sentinel[0] = 'T';
				npJob->sentinel[1] = 'H';
				npJob->sentinel[2] = 'K';
				
				npJob->lpszFullName = (LPSTR) GlobalAllocPtr( GHND, _fstrlen( lpszAlias ) + 1 );

				_fstrcpy(npJob->lpszFullName, lpszAlias);

				npJob->nRowSize = (int) ROWSIZE(lpDIB->bmiHeader.biWidth, lpDIB->bmiHeader.biBitCount);
				npJob->bDel = 0;
				lpInfo->lTotalBytes = npJob->nRowSize * lpDIB->bmiHeader.biHeight;
				npJob->bmi256.bmiHeader = lpDIB->bmiHeader;
			} else {
				//ImgXSetError( hInst, IMGX_NO_OPEN, NULL, NULL );
				LocalUnlock(hJob);
				LocalFree(hJob);
				npJob = NULL;
			}
		}
	} else {
		ImgXSetError(hInst, IMGX_BAD_DEL, NULL, NULL);
	}

	return (HANDLE)npJob;
}

HANDLE IMAPI ImgXBeginWriteMem(LPCSTR lpszExt, LPBITMAPINFO lpDIB, HANDLE hOptBlk, LPXINFO lpInfo, HANDLE hMemBlk, DWORD dwUsed)
{
	LPMEM_IO	pMem;
	NPXJOB   npJob;
	HANDLE	hJob;

	pMem = (LPMEM_IO)GlobalAllocPtr(GHND, sizeof(MEM_IO));
	pMem->wFlags = MEMIO_WRITING | MEMIO_NOALLOC;

	if (hMemBlk) {
		pMem->wFlags |= MEMIO_EXISTS;
	}
	else {
		hMemBlk = GlobalAlloc(GHND, 512*1024L);	//start w/512k file
	}

	pMem->hMem = hMemBlk;

	pMem->hpMem = (HPSTR)GlobalLock(pMem->hMem);
	pMem->dwSize = GlobalSize(pMem->hMem);
	pMem->dwUsed = dwUsed;
	
	
	hJob = BeginWriteInternal(pMem, &mem_ioblk, "MemImg", (LPSTR)lpszExt, lpDIB, hOptBlk, lpInfo);
	npJob = (NPXJOB) hJob;
	
	if( hJob )
		npJob->pMEMIO = pMem;

	return hJob;
}

HANDLE IMAPI ImgXBeginWrite(LPCSTR lpszFileName, LPBITMAPINFO lpDIB, HANDLE hOptBlk, LPXINFO lpInfo)
{
#ifndef WIN32
	struct find_t buf;
#endif
	char        extBuf[4], jbuf[144];
	LPSTR		lpszOutputFileName;
	BOOL        bMultiPage, bAppend;
	HANDLE		retval = NULL;

	// We need to:
	// Generate the file's name based on OptBlk defaults and lpszFileName
	// Check for existence of the file -- if it exists, proceed according
	// to option in optblk.
	// Find the correct DEL to load based on the filename and/or extension
	// Create a local memory object to contain the job info
	// Load the DEL and begin writing...
	// Return the handle to the local memory struct as the job handle


	// Generate the full pathname for file
	lpszOutputFileName = gen_fullname(lpszFileName, hOptBlk);

	//
	// Find the extension and place it in extBuf
	// The extension can be either in the filename or in the OptBlk.
	// An extension listing in the OptBlk overrides the filename!
	//
	// If the option block contains an EXT= statement then get the extension

	if (OptBlkGet(hOptBlk, "EXTENSION", (LPSTR) & extBuf)) {
		// OptBlkGet returns 1 if not found
		Xget_ext(extBuf, lpszOutputFileName);
	}

#if defined(DEBUG) && defined(DBF_TRACE)
	DebugOutput(DBF_APPLICATION | DBF_TRACE, "BeginWrite using Ext of: %s, File: %s", (LPSTR) extBuf, (LPSTR) lpszOutputFileName);
#endif

	if (Xfind_ext(extBuf)) {

		bMultiPage = (Xfind_flags(extBuf) & XF_MULTIPAGE) ? 1 : 0;

		// get APPEND = setting from Opt block
		// bAppend == 1 means that if there's a conflict, we'll append the new image
		// Don't set bApppend unless bMultiPage is also true
		bAppend = bMultiPage && !OptBlkGet(hOptBlk, "APPEND", jbuf)
			&& (!_fstrcmp(jbuf, "ON") || !_fstrcmp(jbuf, "YES"));

		//
		// now check for a name clash -- if the file exists, we have to
		// follow OptBlk settings to see what to do...
		//
		if (!bAppend
#ifdef WIN32
		// cheesy way of doing "FileExists()" in Win32
			 && GetFileAttributes(lpszOutputFileName) != MAXDWORD) {
#else
			 && !_fdos_findfirst(lpszOutputFileName, _A_NORMAL, &buf)) {
#endif
			// it exists
			if (!OptBlkGet(hOptBlk, "OVERWRITE", jbuf) && !_fstrcmp(jbuf, "ON")) {
				// go ahead & overwrite - just fall through here & do nothing...
			} else if (!_fstrcmp(jbuf, "PROMPT")) {

//TODO: Make jBUF dymanically allocated
				wsprintf(jbuf, "%s\r\nFile exists. Do you want to overwrite it?", (LPSTR) lpszOutputFileName);
				if (MessageBox(NULL, jbuf, "File Conflict", MB_YESNO | MB_ICONQUESTION) == IDNO) {
					ImgXSetError(hInst, IMGX_ABORTED, NULL, lpszOutputFileName);
					return (NULL);
				}
			} else {					  // don't write it

				ImgXSetError(hInst, IMGX_EXISTS, NULL, lpszOutputFileName);
				return (NULL);
			}

			DeleteFile(lpszOutputFileName);
		}

		retval = BeginWriteInternal(lpszOutputFileName, &file_ioblk, lpszOutputFileName, extBuf, lpDIB, hOptBlk, lpInfo);

		GlobalFreePtr( lpszOutputFileName );

		return retval;
	} 
	else {							  // the extension wasn't found,
		ImgXSetError(hInst, IMGX_INV_FILE, NULL, NULL);
		return NULL;
	}

}

int IMAPI   ImgXWriteBlock(HANDLE hJob, int nLines, LPSTR lpBits, LPXINFO lpInfo)
{
	NPXJOB      npJob;
	DLLxwrite_block lpFunc;

	npJob = (NPXJOB) hJob;

	//
	// check sentinels to make sure structure is valid
	//
//	if (npJob->sentinel[0] != 'T' || npJob->sentinel[1] != 'H' || npJob->sentinel[2] != 'K') {
	if (!cSentinelIsOK(npJob->sentinel))	{
		ImgXSetError(hInst, IMGX_INV_HAND, NULL, NULL);
	} else {
		lpFunc = (DLLxwrite_block) GetProcAddress(npJob->hDLL, XWRITE_BLOCK);
		SETSTATUS((*lpFunc) (npJob->hImage, nLines, npJob->nRowSize, lpBits, lpInfo));
	}

	return (GETSTATUS);
}

int EndWriteInternal(HANDLE hJob, LPXINFO lpInfo, DWORD FAR *lpLen, BOOL bDeleteFile)
{
	NPXJOB      npJob;
	DLLxend_write lpFunc;
	int         mystatus = IMGX_OK;

	npJob = (NPXJOB) hJob;

	//
	// check sentinels to make sure structure is valid
	//
//	if (npJob->sentinel[0] != 'T' || npJob->sentinel[1] != 'H' || npJob->sentinel[2] != 'K') {
	if (!cSentinelIsOK(npJob->sentinel)) {
		mystatus = IMGX_INV_HAND;
		ImgXSetError(hInst, IMGX_INV_HAND, NULL, NULL);
	} else {
		lpFunc = (DLLxend_write) GetProcAddress(npJob->hDLL, XEND_WRITE);
		assert(lpFunc);
		mystatus = (*lpFunc) (npJob->hImage, lpInfo);
		FreeLibrary(npJob->hDLL);

		if (lpInfo->lpfnStat) {
			(*lpInfo->lpfnStat) (0, 100, lpInfo->dwUserInfo);
		}

		// store the total # of bytes written
		*lpLen = lpInfo->lWritten;

		if (npJob->bDel && bDeleteFile) {		  // need to delete the file...
			if (!DeleteFile(npJob->lpszFullName)) {
				mystatus = IMGX_DELETE_ERR;
				ImgXSetError(hInst, IMGX_DELETE_ERR, NULL, npJob->lpszFullName);
			}
		}

		if( npJob->lpszFullName )
			GlobalFreePtr( npJob->lpszFullName );

		hJob = LocalHandle(npJob);
		LocalUnlock(hJob);
		LocalFree(hJob);
	}
	SETSTATUS(mystatus);

	return (mystatus);
}

int IMAPI ImgXEndWriteMem(HANDLE hJob, LPXINFO lpInfo, HANDLE FAR *phMem, DWORD FAR *pdwLen)
{
	NPXJOB      npJob;
	LPMEM_IO	pMem;
	int	nRetval;

	npJob = (NPXJOB) hJob;
	pMem = npJob->pMEMIO;

	nRetval = EndWriteInternal(hJob, lpInfo, pdwLen, FALSE);

	// store the memory handle, in case it was GlobalReAlloc'd
	if (pMem) {
		*phMem = pMem->hMem;
		GlobalFreePtr(pMem);
	}

	return nRetval;
}

int IMAPI ImgXEndWrite(HANDLE hJob, LPXINFO lpInfo)
{
	DWORD	dwLen;
	int	 	nStatus;
	NPXJOB  npJob;

	npJob = (NPXJOB) hJob;

	//
	// check sentinels to make sure structure is valid
	//
//	if (npJob->sentinel[0] != 'T' || npJob->sentinel[1] != 'H' || npJob->sentinel[2] != 'K') {
	if (!cSentinelIsOK(npJob->sentinel)) {
		nStatus = IMGX_INV_HAND;
		ImgXSetError(hInst, IMGX_INV_HAND, NULL, NULL);
		return nStatus;
	} 

	nStatus = EndWriteInternal(hJob, lpInfo, &dwLen, TRUE);

	SETSTATUS(nStatus);
	return nStatus;
}

int IMAPI   ImgXWriteBMPBlock(HANDLE hJob, HDC hDC, HBITMAP hBitmap, int nNumScans, LPXINFO lpInfo)
{
	NPXJOB      npJob;
	DLLxwrite_block lpFunc;
	BITMAPINFO256 bmi256;		  // used for temp use

	HBITMAP     hTmpBits;		  // buffer to hold DIB

	LPSTR       lpTmpBits;
	int         nMaxLines, nStartScan;
	HDC         hMemDC, hDC2;
	HBITMAP     hTmpBM /* ,hOldBM */ ;
	HPALETTE    hOldPal;
	int         mystatus = IMGX_OK;

	npJob = (NPXJOB) hJob;

	//
	// check sentinels to make sure structure is valid
	//
//	if (npJob->sentinel[0] != 'T' || npJob->sentinel[1] != 'H' || npJob->sentinel[2] != 'K') {
	if (!cSentinelIsOK(npJob->sentinel)) {
		mystatus = IMGX_INV_HAND;
		ImgXSetError(hInst, IMGX_INV_HAND, NULL, NULL);
	} else {
		hTmpBits = (HBITMAP) GlobalAlloc(GHND, 32768UL);	// work w/32K output buffer

		lpTmpBits = (LPSTR) GlobalLock(hTmpBits);
		assert(lpTmpBits);
		lpFunc = (DLLxwrite_block) GetProcAddress(npJob->hDLL, XWRITE_BLOCK);

		nMaxLines = (WORD) (32768UL / npJob->nRowSize);

		bmi256 = npJob->bmi256;
		//
		// now we have to figger out if we need a temporary bitmap
		// to contain just the portion we want, or if the image is
		// gonna fit entirely in the 4K block we've allocated...
		//

		if (nMaxLines < nNumScans) {
			hTmpBM = CreateCompatibleBitmap(hDC, (int) npJob->bmi256.bmiHeader.biWidth, nMaxLines);
			hMemDC = CreateCompatibleDC(hDC);
			hDC2 = CreateCompatibleDC(hDC);
			SelectBitmap(hDC2, hBitmap);
			// hOldBM = CreateCompatibleBitmap(hDC, 1, 1);  // just has color info
			hOldPal = SelectPalette(hDC, (HPALETTE) GetStockObject(DEFAULT_PALETTE), 0);
			SelectPalette(hDC, hOldPal, 0);
			SelectPalette(hMemDC, hOldPal, 0);

			//
			// We have to determine if we should write backwards or forwards...
			//
			if (lpInfo->lFlags & XF_BACKWARD) {	// it's backwards

				nStartScan = nNumScans;

				while (nNumScans && (mystatus == IMGX_OK)) {
					int         nWriteLines = min(nNumScans, nMaxLines);

					nStartScan -= nWriteLines;

					SelectBitmap(hMemDC, hTmpBM);
					BitBlt(hMemDC, 0, 0, (int) bmi256.bmiHeader.biWidth, nWriteLines,
							 hDC2, 0, nStartScan, SRCCOPY);
					// SelectObject(hMemDC, hOldBM);

					bmi256.bmiHeader.biHeight = nWriteLines;
					GetDIBits(hMemDC, hTmpBM, 0, nWriteLines, lpTmpBits, (LPBITMAPINFO) & bmi256, DIB_RGB_COLORS);
					mystatus = (*lpFunc) (npJob->hImage, nWriteLines, npJob->nRowSize, lpTmpBits, lpInfo);
					nNumScans -= nWriteLines;
				}
			} else {					  // forwards

				nStartScan = 0;

				while (nNumScans) {
					int         nWriteLines = min(nNumScans, nMaxLines);

					SelectBitmap(hMemDC, hTmpBM);
					BitBlt(hMemDC, 0, 0, (int) (bmi256.bmiHeader.biWidth), nWriteLines,
							 hDC2, 0, nStartScan, SRCCOPY);
					// SelectObject(hMemDC, hOldBM);

					bmi256.bmiHeader.biHeight = nWriteLines;
					GetDIBits(hMemDC, hTmpBM, 0, nWriteLines, lpTmpBits, (LPBITMAPINFO) & bmi256, DIB_RGB_COLORS);
					mystatus = (*lpFunc) (npJob->hImage, nWriteLines, npJob->nRowSize, lpTmpBits, lpInfo);
					nStartScan += nWriteLines;
					nNumScans -= nWriteLines;
				}
			}

			DeleteDC(hMemDC);
			DeleteDC(hDC2);
			DeleteObject(hTmpBM);
			// DeleteObject(hOldBM);
		} else {						  // it fits entirely in 4k buffer

			GetDIBits(hDC, hBitmap, 0, nNumScans, lpTmpBits, (LPBITMAPINFO) & bmi256, DIB_RGB_COLORS);
			mystatus = (*lpFunc) (npJob->hImage, nNumScans, npJob->nRowSize, lpTmpBits, lpInfo);
		}

		GlobalUnlock(hTmpBits);
		GlobalFree(hTmpBits);
	}
	SETSTATUS(mystatus);

	return mystatus;
}

int IMAPI   ImgXAbort(HANDLE hJob)
{
	NPXJOB      npJob;

	npJob = (NPXJOB) hJob;

	//
	// check sentinels to make sure structure is valid
	//
//	if (npJob->sentinel[0] != 'T' || npJob->sentinel[1] != 'H' || npJob->sentinel[2] != 'K') {
	if (!cSentinelIsOK(npJob->sentinel)) {
		ImgXSetError(hInst, IMGX_INV_HAND, NULL, NULL);
		return (IMGX_ERR);
	}
	npJob->bDel = 1;

	return (IMGX_OK);
}

int IMAPI   ImgXGetExt(LPSTR lpszBuf)
{
	if (!lpXExtString)
		return (IMGX_ERR);
	else if (!lpszBuf)
		return (_fstrlen(lpXExtString) + 1);

	_fstrcpy(lpszBuf, lpXExtString);
	return (IMGX_OK);
}

HANDLE IMAPI LOADDS ImgXOptBlkCreate(LPSTR lpszInit)
{
	return OptBlkCreate(lpszInit);
}

HANDLE IMAPI LOADDS ImgXOptBlkAdd(HANDLE hOptBlk, LPSTR lpszKey, LPSTR lpszValue)
{
	return OptBlkAdd(hOptBlk, lpszKey, lpszValue);
}

int IMAPI LOADDS ImgXOptBlkGet(HANDLE hOptBlk, LPSTR lpszKey, LPSTR lpszBuf)
{
	return OptBlkGet(hOptBlk, lpszKey, lpszBuf);
}

int IMAPI LOADDS ImgXOptBlkDel(HANDLE hOptBlk)
{
	return OptBlkDel(hOptBlk);
}

// //////////////////////////////////////////////////////////////////////////
//
// int IMAPI ImgXErrBox(HWND parent);
//
// Displays a message box containing a message detailing the last error
// encountered within ImageMan/X.
//
// Returns: IMGX_OK
//
// //////////////////////////////////////////////////////////////////////////

int IMAPI   ImgXErrBox(HWND hParent)
{
	return ImgErrBox( hParent );
}

// //////////////////////////////////////////////////////////////////////////
//
// void IMAPI ImgXErrString(LPSTR lpBuf);
//
// Returns a string containing text of error, module which generated it,
// and the name of the file. lpBuf should be at least 512 chars wide.
//
// Returns: void
//
// //////////////////////////////////////////////////////////////////////////

void IMAPI  ImgXErrString(LPSTR lpBuf)
{
	ImgErrString( lpBuf, 255 );
}

int IMAPI   ImgXGetStatus(void)
{
	return ImgGetStatus();
}

#ifdef _WIN32
void IMAPI  ImgXSetStatus(int status)
{
	ImgSetStatus( status );
}
#endif


// /////////////////////////////////////////////////////////////////////////
//
// ImgXSetError
//
// Sets the error string buffers for the name of the offending (or offended)
// module, the name of the file which generated the error, and the text
// of the error message, retrieved from the proper resource file...
//
// This function is intended to be called from imgx.c or the various
// export libraries to report errors. It will retrieve the error message
// from the resource file based on nIndex. It is NOT intended to be used by
// the applications using ImageMan/X, although I guess if someone finds
// it and has a purpose for it, it's not such a horrible thing...
//
// /////////////////////////////////////////////////////////////////////////

void IMAPI LOADDS ImgXSetError(HANDLE hInstance, int nIndex, LPCSTR lpModule, LPCSTR lpFileName)
{
	SetError( lpFileName, nIndex, 0, 0, (HINSTANCE) hInstance );
}

HANDLE IMAPI ImgXFileDialog(HANDLE hOptBlk, HWND hWndOwner)
{
	char szValue[ 100 ] ;
	OPENFILENAME ofn;
	char        szDirName[MAX_PATH];
	char        szFile[MAX_PATH];
	char        szFileTitle[MAX_PATH];
	LPSTR       pJbuf;
	char        jbuf[300], defExtBuf[5];
	int         n, bNewOptFlag;

	SAVEDIALOG  SaveDialog ;

	// fill in the filter array
	_fmemset(jbuf, '\0', sizeof(jbuf));

	// clear out save struct
	_fmemset( (LPSAVEDIALOG)&SaveDialog, 0, sizeof(SaveDialog) ) ;

	pJbuf = jbuf;
	for (n = 0; n < MAXEXTS; n++) {
		if (XEXTList[n].DLL[0]) {
			_fstrcpy(pJbuf, XEXTList[n].desc);
			pJbuf += _fstrlen(pJbuf) + 1;
			_fstrcpy(pJbuf, XEXTList[n].mask);
			pJbuf += _fstrlen(pJbuf) + 1;
		}
	}

	// Get Default Extension
	defExtBuf[0] = '\0';
	OptBlkGet(hOptBlk, "DEFEXT", defExtBuf);

	//
	// Create an OptBlk if one don't already exist.
	//
	if (hOptBlk)
		bNewOptFlag = 0;
	else {
		hOptBlk = OptBlkCreate(NULL);
		bNewOptFlag = 1;
	}

	szFile[0] = '\0';
	OptBlkGet(hOptBlk, "FILENAME", szFile);

	szDirName[0] = '\0';
	OptBlkGet(hOptBlk, "PATH", szDirName);

	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = hWndOwner;
	ofn.hInstance = hInst ;
	ofn.lpstrFilter = (LPSTR) jbuf;
	ofn.lpstrCustomFilter = (LPSTR) NULL;
	ofn.nMaxCustFilter = 0L;
	ofn.nFilterIndex = 0L;
	ofn.lpstrFile = (LPSTR)szFile;
	ofn.nMaxFile = sizeof(szFile);
	ofn.lpstrFileTitle = (LPSTR)szFileTitle;
	ofn.nMaxFileTitle = sizeof(szFileTitle);
	ofn.lpstrInitialDir = szDirName;
	ofn.lpstrTitle = NULL ;
	ofn.Flags = OFN_HIDEREADONLY ;
	szValue[0] = '\0';
	OptBlkGet( hOptBlk, "SAVE_DIALOG", szValue );
	if ( ! ( _fstricmp(szValue,"ON") ) )
		{
			ofn.Flags |= OFN_ENABLEHOOK | OFN_ENABLETEMPLATE ;
#ifdef _WIN32
			ofn.Flags |= OFN_EXPLORER ;
			ofn.lpfnHook = (LPOFNHOOKPROC) ImgSaveProc ;
#else
#define OFN_NONETWORKBUTTON 0x00020000 
			ofn.Flags |= OFN_NONETWORKBUTTON ;
			ofn.lpfnHook = ImgSaveProc ;
#endif
			ofn.lpTemplateName = "IMGSAVEDLG" ;
		}
	else
		{
			ofn.lpTemplateName = NULL ;
			ofn.lpfnHook = NULL ;
		}
	ofn.nFileOffset = 0;
	ofn.nFileExtension = 0;
	if ( ! defExtBuf[0] )
		_fstrcpy ( defExtBuf, "BMP" ) ;
	ofn.lpstrDefExt = (LPSTR) defExtBuf;
	SaveDialog.hOptBlk = hOptBlk ;
	ofn.lCustData = (DWORD) (LPSAVEDIALOG) &SaveDialog ;

	_fstrcpy ( (LPSTR)SaveDialog.szFileType, ofn.lpstrDefExt ) ;
	szValue[0] = 0 ;
	OptBlkGet( hOptBlk, "COLORS", (LPSTR)&szValue ) ;
#ifdef _WIN32
  SaveDialog.iColors = atoi ( (LPSTR)szValue ) ;
#else
  SaveDialog.iColors = _fatoi ( (LPSTR)szValue ) ;
#endif

	if (n=GetSaveFileName(&ofn)) {
		OptBlkAdd(hOptBlk, "FILENAME", szFile);
	} else {
		long cderr ;
		long err ;
		cderr = CommDlgExtendedError() ;
		switch ( cderr )
			{
				case 0 :
					err = IMG_DLGCANCEL ;
					break ;
				default :		// some other dlg error occurred
					err = IMG_DLGERROR ;
					break ;
			}
		SetError( NULL, err, 0, NULL, hInst ) ;
		SETSTATUS(err) ;
		if (bNewOptFlag) {		  // get rid of the block we created

			OptBlkDel(hOptBlk);
		}
		return (NULL);
	}

	return (hOptBlk);
}

// ========================================================================
//
// Helper functions
//
// ========================================================================

#ifdef WIN32
static int  processDEL(LPCSTR path, LPSTR lpExtList)
{
	HINSTANCE   hdll;
	HANDLE      fhandle;
	WIN32_FIND_DATA finfo;
	char        buf[MAX_PATH];
	LPSTR       lpext, lpdesc;
	DLLxget_ext lpfnExt;
	DLLxget_ver lpfnVer;
	XINITSTRUCT xis;
	int         count, delcount = 0;

	if (!*path)
		return (0);

	// create full path
	strcpy(buf, path);
	if (buf[strlen(buf) - 1] != '\\')
		strcat(buf, "\\");
	strcat(buf, DELFSPEC);

	if ((fhandle = FindFirstFile(buf, &finfo)) == INVALID_HANDLE_VALUE)
		return (0);
	do {
		// create the full path name of located DIL
		strcpy(buf, path);
		if (buf[strlen(buf) - 1] != '\\')
			strcat(buf, "\\");
		strcat(buf, finfo.cFileName);

		OutputDebugString("IM32: DEL (");
		OutputDebugString(buf);
		OutputDebugString(")\n");

		if (!GetModuleHandle(buf) && (hdll = LoadLibrary(buf))) {
			count = 0;
			if (lpfnVer = (DLLxget_ver) GetProcAddress(hdll, XGET_VER)) {
				if (lpfnExt = (DLLxget_ext) GetProcAddress(hdll, XGET_EXT)) {
					// get the extension list & flags; set the error processing callback
					xis.SetError = ImgXSetError;
					xis.hImgManInst = hInst;
					lpext = (*lpfnExt) (&xis);
					// find extension list and description(s)
					if (lpdesc = strchr(lpext, ';')) {
						*lpdesc++ = '\0';
					}
					// Add extensions stored in buffer to our internal lists
					// JGD: Looks like a BUG here if the Extension doesn't contain
					// a space then it wont be added to the X Stuff

					// for (p1 = lpext; p2 = strchr(p1,' '); p1 = p2+1) {
					//*p2 = '\0';

					if (XaddExtension(lpext, finfo.cFileName, (*lpfnVer) (), lpdesc, xis.lFlags)) {
						// only add extension when successful
						strcat(lpExtList, *lpExtList ? ",*." : "*.");
						strcat(lpExtList, lpext);
						count++; // Count addition of new extension
					}
					// }

					if (lpdesc)
						*(lpdesc - 1) = ';';
				}
			}
			if (count)
				delcount++;			  // one more DEL available

			FreeLibrary(hdll);	  // always free up the DEL

		}
	} while (FindNextFile(fhandle, &finfo));
	FindClose(fhandle);
	return (delcount);
}
#endif // WIN32


// //////////////////////////////////////////////////////////////////////////
//
// int IMAPI ImgXInit(void)
//
// Initializes the ImageMan/X library. Checks for usable DEL's to support
// images and initializes them. Retrieves list of supported file extensions
// for images from the DEL's.
//
// Returns: IMGX_OK on successful initialization, 0 on failure.
//
// Globals: extString, img_status
//
// //////////////////////////////////////////////////////////////////////////

int IMAPI   ImgXInit(void)
{
#ifdef WIN32

	if (!findDLL(processDEL, &lpXExtString)) {
		// We can't set this here, 'cause the THREADINFO isn't in place yet
		// SETXSTATUS(IMGX_NO_DELS);
		ImgXSetError(hInst, IMGX_NO_DELS, NULL, NULL);
		return (IMGX_ERR);
	}
	return (IMGX_OK);

#else // WIN32

	struct find_t buf;
	int         stat, oldDrive, curDrive, i;
	LPSTR       ext;
	DLLxget_ext lpfunc;
	DLLxget_ver lpfunc2;
	HINSTANCE   hDLL;
	HGLOBAL     hPath;
	char        buf1[20], buf2[10], srchSpec[MAX_PATH], fullName[MAX_PATH];
	LPSTR       p, path, curDir, foundDLLS, pDesc;
	char        szModPath[MAX_PATH];
	int         bDELFlag = 0;	  // 1 if at least 1 DIL was found, 0 otherwise

	GetModuleFileName(hInst, szModPath, sizeof(szModPath));

	p = szModPath + _fstrlen(szModPath) - 1;	// Strip off the the filename of the DLL

	while (p > szModPath && *p != '\\')
		p--;
	*++p = '\0';

	// Get current path for searching...
	p = GetDOSEnvironment();
	while (*p) {
		if (!_fstrncmp(p, "PATH=", 5)) {
			hPath = GlobalAlloc(DLLHND, _fstrlen(p) + _fstrlen(szModPath));	// +1 for '/0', -5 for PATH=

			path = (LPSTR) GlobalLock(hPath);
			_fstrcpy(path, szModPath);
			_fstrcat(path, ";.;");
			_fstrcat(path, p + 5);
			break;
		}
		p += _fstrlen(p) + 1;
	}

	// now we search each directory in path, keeping a list of DLLs that
	// we've found along the way...any DLL that we find is compared to those
	// in the list -- if the dll's already in the list, it's discarded,
	// otherwise it's added to the list & initialized.

	_fstrupr(path);
	curDrive = oldDrive = _getdrive();
	curDir = _fstrtok(path, ";");

	foundDLLS = (LPSTR) GlobalLock(GlobalAlloc(DLLHND, 1000));

	while (curDir) {
		// strip out drive letter & change drives if needed
		if (_fstrchr(curDir, ':')) {
			if ((int) (*curDir - 'A' + 1) != curDrive) {
				_chdrive(*curDir - 'A' + 1);
				curDrive = *curDir - 'A' + 1;
			}
			curDir += 2;
		} else if (curDrive != oldDrive) {
			_chdrive(oldDrive);
		}
		_fstrcpy(srchSpec, curDir);
		if (srchSpec[_fstrlen(srchSpec) - 1] != '\\')
			_fstrcat(srchSpec, "\\");
		_fstrcat(srchSpec, DELFSPEC);

		stat = _fdos_findfirst(srchSpec, _A_NORMAL, &buf);

		while (!stat && !_fstrstr(foundDLLS, buf.name)) {
			// first copy this DLL name to foundDLLS string to avoid duplicates
			// put space in-between names to avoid accidental matches
			_fstrcat(foundDLLS, buf.name);
			_fstrcat(foundDLLS, " ");

			if ((hDLL = LoadLibrary(buf.name)) != NULL) {
				if (lpfunc = (DLLxget_ext) GetProcAddress(hDLL, XGET_EXT)) {
					XINITSTRUCT xis;

					// get the extension list & flags, & set the error processing callback
					xis.SetError = ImgXSetError;
					xis.hImgManInst = hInst;
					ext = (*lpfunc) (&xis);

					//
					// extract just the extension -- forget the explanatory text for now
					//
					for (i = 0; ext[i] != ';'; i++) {
						buf1[i] = ext[i];
					}
					pDesc = ext + i + 1;
					buf1[i] = '\0';

					p = _fstrtok(buf1, " ");
					while (p) {
						wsprintf(buf2, "*.%s", p);
						if (lpXExtString) {
							long        lSize;
							lSize = GlobalSize(GlobalPtrHandle(lpXExtString)) + _fstrlen(buf2) + 1;
							lpXExtString = (LPSTR) GlobalReAllocPtr(lpXExtString, lSize, GMEM_MOVEABLE);
							_fstrcat(lpXExtString, ",");
							_fstrcat(lpXExtString, buf2);
						} else {
							lpXExtString = (LPSTR) GlobalAllocPtr(GHND, _fstrlen(buf2) + 1);
							_fstrcpy(lpXExtString, buf2);
						}

						lpfunc2 = (DLLxget_ver) GetProcAddress(hDLL, XGET_VER);
						fullName[0] = curDrive + 'A' - 1;
						fullName[1] = ':';
						fullName[2] = '\\';
						fullName[3] = '\0';
						_fgetcwd((char _far *) (&fullName[3]), 64);
						//
						// if current dir is root, we'll get a null
						// string back from _fgetcwd. we have to check
						// for this, else we'll end up with two '\'
						// characters in a row, a definite no-no in the
						// DOS world...
						//
						if (_fstrlen((LPSTR) & fullName[3]) > 0) {
							_fstrcat(fullName, "\\");
						}
						_fstrcat(fullName, buf.name);

						XaddExtension((LPSTR) p, (LPSTR) fullName, (*lpfunc2) (), pDesc, xis.lFlags);
						p = _fstrtok(NULL, " ");
					}
				}
				FreeLibrary(hDLL);
				bDELFlag = TRUE;	  // remember that we've seen a DIL

			}
			stat = _fdos_findnext(&buf);
		}

		// NOTE that _fstrtok(NULL, ";") WON'T WORK! We used _fstrtok in the
		// inner loop, which reset our original stuff. We can, fortunately,
		// get around this problem by beginning our search from beginning of
		// next string.
		//
		// I know that, upon first examination, this code looks like it could
		// go beyond end of path's global memory block. It does, indeed,
		// set curDir to a value greater than the end of the path string;
		// however, the global block was allocated with a few extra bytes,
		// which are set to 0 upon allocation (DLLHND), so IT WORKS FINE
		// EVERY TIME!
		//
		
		curDir = _fstrtok(curDir + _fstrlen(curDir) + 1, ";");
	}

	_chdrive(oldDrive);			  // make sure we end up where we started!

	GlobalUnlock(hPath);
	GlobalFree(hPath);

	GlobalFreePtr(foundDLLS);

	if (bDELFlag) {
		SETSTATUS(IMGX_OK);
		return (IMGX_OK);
	} else {							  // we never saw a DEL, and that's an ERROR!

		ImgXSetError(hInst, IMGX_NO_DELS, NULL, NULL);
		return (IMGX_ERR);
	}
#endif // WIN32
}

// ////////////////////////////////////////////////////////////////////////
//
// int XaddExtension(LPSTR ext, LPSTR DLL);
//
// Adds the passed extension and DLL filename to the list of associated
// extensions and DLLs kept internally to the ImageMan.
//
// Returns: 0
//
// Globals: XEXTList[]
//
// //////////////////////////////////////////////////////////////////////////

static int NEAR XaddExtension(LPSTR ext, LPSTR DLL, LPSTR ver, LPSTR desc, DWORD lFlags) {
	int         i;

	for         (i = 0; XEXTList[i].DLL[0] && i < MAXEXTS; i++) {
		// just return if it's already in the table
		if (!_fstrcmp(XEXTList[i].ext, ext))
			return (0);
	}
	            _fstrcpy(XEXTList[i].DLL, DLL);
	_fstrcpy(XEXTList[i].ext, ext);
	_fstrcpy(XEXTList[i].ver, ver);
	_fstrcpy(XEXTList[i].desc, desc);
	_fstrcpy(XEXTList[i].mask, "*.");	// 'mask' gets "*.ext"

	_fstrcat(XEXTList[i].mask, ext);
	XEXTList[i].lFlags = lFlags;

	return (IMGX_OK);
}

// //////////////////////////////////////////////////////////////////////////
//
// void Xget_ext(LPSTR extBuf, LPSTR file);
//
// Retrieves the file extension from a given filename and places it in extBuf.
//
// Returns: nothing
//
// Globals: none
//
// //////////////////////////////////////////////////////////////////////////

static void NEAR Xget_ext(LPSTR extBuf, LPSTR file) {
	LPSTR       p;

	if (p = _fstrrchr(file, '.')) _fstrncpy(extBuf, p + 1, 4);
	else *extBuf = '\0';
}

// //////////////////////////////////////////////////////////////////////////
//
// LPSTR Xfind_ext(LPSTR ext);
//
// Searches XEXTList[] for extension - if found, returns pointer to name of
// DLL that supports the extension.
//
// Returns: LPSTR to DLL filename on success, NULL on failure
//
// Globals: none
//
// //////////////////////////////////////////////////////////////////////////
// returns a LPSTR that points to name of DLL to use for this extension

static LPSTR NEAR Xfind_ext(LPSTR ext) {
	int         i;

	for (i = 0; i < MAXEXTS && XEXTList[i].ext[0] ; i++) {
		if (!lstrcmpi(ext, XEXTList[i].ext))
			return (XEXTList[i].DLL);
	}

	            return (NULL);
}

// //////////////////////////////////////////////////////////////////////////
//
// long Xfind_flags(LPSTR ext);
//
// Searches XEXTList[] for extension - if found, returns flags for the extension
//
// Returns: long representing flags for the given extension
//
// Globals: none
//
// //////////////////////////////////////////////////////////////////////////
static long NEAR Xfind_flags(LPSTR ext) {
	int         i;

	for         (i = 0; i < MAXEXTS; i++) {
		if (!lstrcmpi(ext, XEXTList[i].ext))
			return (XEXTList[i].lFlags);
	}

	            return (0L);
}

static LPSTR NEAR gen_fullname(LPCSTR lpFname, HANDLE hOptBlk ) {
	LPSTR		lpOutputFileName;
	LPSTR		lpTmpFileName;
	int			nLength = 0;
	int			nPathLength = 0;

	//
	// If lpFname contains a path already, it takes precedence, so we
	// just return lpFname.
	//
	// If it doesn't, we check hOptBlk to see if it has a path defined
	// in it. If it doesn't, we just return lpFname with no path
	// applied to it.
	//

	// make sure lpFname exists at all...

	if( !lpFname ) {
		OptBlkGetLength( hOptBlk, "FILENAME", &nLength );

		if( nLength ) {
			lpTmpFileName = (LPSTR) GlobalAllocPtr( GHND,  nLength + 1 );

			OptBlkGet(hOptBlk, "FILENAME", lpTmpFileName);
		} else
			return NULL;
	} else
		lpTmpFileName = (LPSTR) lpFname;


	if (!_fstrchr(lpTmpFileName, '\\') && !_fstrchr(lpTmpFileName, ':') && !OptBlkGetLength(hOptBlk, "PATH", &nPathLength)) {

		lpOutputFileName = (LPSTR) GlobalAllocPtr(GHND,  _fstrlen( lpTmpFileName ) + nPathLength + 1 );

		OptBlkGet( hOptBlk, "PATH", lpOutputFileName );		

		_fstrcat(lpOutputFileName, lpTmpFileName);

	} else {						  // lpFname contains path or there's no path in opt blk
		lpOutputFileName = (LPSTR) GlobalAllocPtr( GHND, _fstrlen( lpTmpFileName ) + 1 );
		_fstrcpy(lpOutputFileName, lpTmpFileName);
	}

	if( nLength )
		GlobalFreePtr ( lpTmpFileName );

	return lpOutputFileName;
}

void NEAR XSetOptBlkOpts ( HANDLE hOpts, DWORD lOpts, LPBITMAPINFOHEADER lpbi )
{
	long lRes ;
  char sztemp[ 100 ] ;
	if ( lOpts & IMGXOPT_SAVE_PROMPT )
		OptBlkAdd ( hOpts, "SAVE_DIALOG", "ON" ) ;

	if ( lOpts & IMGXOPT_COMPRESS )
		OptBlkAdd ( hOpts, "COMPRESS", "ON" ) ;

	wsprintf ( sztemp, "%d", lpbi->biBitCount ) ;
	OptBlkAdd ( hOpts, "COLORS", sztemp ) ;

	sztemp[0] = 0 ;
	OptBlkGet( hOpts, "TIFF_XRES", sztemp ) ;
	if ( ! sztemp[0] )
		OptBlkGet( hOpts, "XRES", sztemp ) ;
	else
		OptBlkAdd ( hOpts, "XRES", sztemp ) ;
	if ( ! sztemp[0] )
		{
			lRes = (long)( lpbi->biXPelsPerMeter / 39.37 ) ;
			if ( ! lRes )
				lRes = 300 ;
			wsprintf ( sztemp, "%ld", lRes ) ;
			OptBlkAdd ( hOpts, "XRES", sztemp ) ;
		}

	sztemp[0] = 0 ;
	OptBlkGet( hOpts, "TIFF_YRES", sztemp ) ;
	if ( ! sztemp[0] )
		OptBlkGet( hOpts, "YRES", sztemp ) ;
	else
		OptBlkAdd ( hOpts, "YRES", sztemp ) ;
	if ( ! sztemp[0] )
		{
			lRes = (long)( lpbi->biYPelsPerMeter / 39.37 ) ;
			if ( ! lRes )
				lRes = 300 ;
			wsprintf ( sztemp, "%ld", lRes ) ;
			OptBlkAdd ( hOpts, "YRES", sztemp ) ;
		}
}
