/////////////////////////////////////////////////////////////////////////////
//
//    color2.h
//
//    Color Reduction/Dithering Module Includes/Prototypes
//
//    ImageMan Image Processing Library
//
//    Copyright (c) 1993 Data Techniques, Inc.
//    All Rights Reserved
//
/////////////////////////////////////////////////////////////////////////////
// $History: color2.h $
// 
// *****************  Version 3  *****************
// User: Ericw        Date: 12/03/97   Time: 5:07p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Took out NEAR on InitTransPal - not good under 16 release mode.
// 
// *****************  Version 2  *****************
// User: Ericw        Date: 9/23/97    Time: 5:14p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Implemented the octree color reduction stuff for v6 
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 7/10/97    Time: 2:04p
// Created in $/imageman 16-32/dll-api level 2
// Initial Lvl 2 Checkin
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 1/18/95    Time: 3:59p
// Created in $/ImageMan 16-32/DLL
// ImageMan 4.0 Beta 1

// Size of color translation lookup table
#define LOOKUPSIZE (32768UL*sizeof(WORD))

// Maximum size of "optimal" image palette
#define NEWPALSIZE (256*sizeof(RGBQUAD))

#ifdef __cplusplus
extern "C" {
#endif


extern LPWORD PASCAL GenOptimizedPalette ( LPBITMAPINFO lpbi, HPSTR lpInBits, UINT nColors ) ;
extern LPWORD PASCAL GenOctreePalette    ( LPBITMAPINFO lpbi, HPSTR lpInBits, UINT nColors ) ;

extern void PASCAL RemapPixels(LPBITMAPINFO lpInBMI,HPSTR lpInBits,LPBITMAPINFO lpOutBMI, HPSTR lpOutBits,WORD FAR *lpLookup);

typedef struct imgPal {
	UINT        num;
	UCHAR       Red, Green, Blue;
	UCHAR       pad;				  // keep long aligned

}           PALETTE;

typedef struct tagColorDistanceStuff {
	UINT        nColors;
	UINT        redindex[256];
	PALETTE     Pals[256];		  // temp sorted palette

}           CDSTUFF, NEAR * PCDSTUFF, FAR * LPCDSTUFF;

extern int PASCAL Closest(int red, int green, int blue, PCDSTUFF pStuff) ;
extern PCDSTUFF PASCAL InitTransPal(UINT nColors, LPRGBQUAD lpPal) ;

#ifdef __cplusplus
}
#endif
