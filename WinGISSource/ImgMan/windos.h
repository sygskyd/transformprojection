/////////////////////////////////////////////////////////////////////////////
//
//    windos.h
//
//    ImageMan DOS common support code Prototypes
//
//    Copyright (c) 1992 Data Techniques, Inc.
//    All Rights Reserved
//
/////////////////////////////////////////////////////////////////////////////
// $History: windos.h $
// 
// *****************  Version 2  *****************
// User: Ericw        Date: 1/20/98    Time: 2:57p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Extern C for function prototypes.
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 7/10/97    Time: 2:04p
// Created in $/imageman 16-32/dll-api level 2
// Initial Lvl 2 Checkin
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 1/18/95    Time: 3:59p
// Created in $/ImageMan 16-32/DLL
// ImageMan 4.0 Beta 1

#ifdef __cplusplus
extern "C" {
#endif

int _fdos_findfirst(char _far * lpszMask, unsigned nAttribs, struct find_t _far *buf);
int _fdos_findnext(struct find_t _far *buf);
void _fgetcwd(char _far *buf, int nMax);
int _fchdir(char _far *lpDir);
int _funlink(char _far *lpBye);
int _fatoi(char _far *);
HANDLE LoadLibraryPath(LPSTR lpszPath);
FARPROC FAR GetAnyProcAddress(HANDLE hInst, LPSTR lpProc);
unsigned long FAR PASCAL HugeWrite(int fHand, HPSTR hpBuf, unsigned long lBytes);

#ifdef __cplusplus
}
#endif
