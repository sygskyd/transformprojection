/////////////////////////////////////////////////////////////////////////////
//
// fxparam.h
//
// Common effects defines and structures
//
// Copyright (c) 1996 Data Techniques, Inc.
// Copyright (c) 1996 Chris Roueche
// All Rights Reserved
//
/////////////////////////////////////////////////////////////////////////////
// $History: fxparam.h $
// 
// *****************  Version 3  *****************
// User: Johnd        Date: 8/19/98    Time: 10:50a
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Updates for Win16 Compilation
// 
// *****************  Version 2  *****************
// User: Johnd        Date: 7/15/98    Time: 9:32a
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// 16 Bit code change
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 7/10/97    Time: 2:04p
// Created in $/imageman 16-32/dll-api level 2
// Initial Lvl 2 Checkin
//

#define MKDWORD(_A,_B,_C,_D) (((DWORD)(_A))|((DWORD)(_B)<<8)|((DWORD)(_C)<<16)|((DWORD)(_D)<<24))

typedef struct tag_DTIFraction {
	int num,den;
} DTIFRACTION;

// Wipe:
struct tag_Wipe {
	RECT   bounds;   // original wipe/slide boundary
	RECT   bkbox;    // background position (may be moving)
	LPRECT prScroll; // ScrollDC boundary (either bounds or rDst)
	SIZE   step;     // x&y scroll increments
};

// Mosaic:
struct tag_Mosaic {
	LPINT  pused;    // counters & bit map
	UINT   bwidth;   // bit map's row width
	SIZE   cell;
	SIZE   grid;
	int    edge;     // position of creeping edge
	int    extent;   // size of creeping area (width/height)
	int    nfilled;  // cells filled in leading edge
};

// Blur:
struct tag_Blur {
	SIZE    max;     // maximum width/height
	SIZE    lense;   // intermediate bitmap scale e.g. 1:N
	SIZE    step;
	SIZE    accum;
	int     dir;     // blur direction (-1 is in, +1 is out)
	HDC     hdc;
	HBITMAP hbm;
	HBITMAP holdbm;
};

// Shapes:
struct tag_Shape {
	RECT        bbox;
	DTIFRACTION xstep;
	DTIFRACTION ystep;
	SIZE        accum;
	int         dir;
};
#ifdef WIN32
// Curtain:
struct tag_Curtain {
	RECT  bbox;
	RECT  bkbox1,bkbox2;
	LPLONG pedge1,pedge2;
	SIZE  step;
	LPLONG pinc;
};
#else
// Curtain:
struct tag_Curtain {
	RECT  bbox;
	RECT  bkbox1,bkbox2;
	LPINT pedge1,pedge2;
	SIZE  step;
	LPINT pinc;
};

#endif

// Blind:
struct tag_Blind {
	RECT bbox;       // initial destination (rDst unless image is sliding)
	SIZE step;       // growth increment
	HRGN hThruRgn;
};

// Common stuff:
typedef struct tag_EffectParamBlock {
	LPBITMAPINFOHEADER pdib;
	void FAR *pbits;
	HDC   hMemDC;
	UINT  nEffect;
	DWORD dwFlags;
	DWORD dwExtra;
	RECT  rSrc;
	RECT  rDst;
	HRGN  hClipRgn;  // DC's original clippage
	BOOL  b121;
	int   nStepsDone;
	int   nStepsTotal;
	HPALETTE hPal;
	union {
//	union tag_PerEffect {
		struct tag_Wipe    wipe;
		struct tag_Mosaic  mosaic;
		struct tag_Blur    blur;
		struct tag_Shape   shape;
		struct tag_Curtain curtain;
		struct tag_Blind   blind;
	};
} EFFECTBLK,*PEFFECTBLK;

#define INITIALIZE(P) ((P)->nStepsTotal == 0)
#define CLEANUP(P)    ((P)->nStepsDone >= (P)->nStepsTotal)

typedef int (*PFNEFFECT)(HDC,PEFFECTBLK);

extern void clipblt(HDC,PEFFECTBLK,LPRECT);
