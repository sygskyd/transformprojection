/////////////////////////////////////////////////////////////////////////////
//
//    optblk.c
//
//    ImageMan Export Option Block handling code
//
//    Copyright (c) 1991-5 Data Techniques, Inc.
//    All Rights Reserved
//
/////////////////////////////////////////////////////////////////////////////
// $History: optblk.cpp $
// 
// *****************  Version 6  *****************
// User: Sygsky		Date: 05-MAY-2001	Time: 11:13a
// Updated in $/ImageMan 16-32/DLL - V 7.xx API
// Some optimization of sentinel check
// characters.
// 
// *****************  Version 5  *****************
// User: Erict        Date: 7/13/00    Time: 9:24a
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// OptBlkCreate did not convert the \r characters to carriage return
// characters.
// 
// *****************  Version 4  *****************
// User: Johnd        Date: 3/06/00    Time: 1:28p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Fixed OptBVlk create so it called AnsiUpper on our copy of the passed
// in string instead of calling it on the lpStr passed to the function.
// Under VC 6 with a literal string parameter this would cause a GPF.
// 
// *****************  Version 3  *****************
// User: Ericj        Date: 4/29/98    Time: 12:08p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Changed AddKey so it would use a pointer to a string instead of a
// buffer.
// 
// *****************  Version 2  *****************
// User: Johnd        Date: 10/10/97   Time: 5:54p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Made these functions "C" functions since we also link this module with
// the DIL/DELS that arent CPP
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 7/10/97    Time: 2:03p
// Created in $/imageman 16-32/dll-api level 2
// Initial Lvl 2 Checkin
// 
// *****************  Version 3  *****************
// User: Johnd        Date: 9/26/96    Time: 10:07a
// Updated in $/ImageMan 16-32/DLL
// Fixed OptBlk size Bug #433
// 
// *****************  Version 2  *****************
// User: Johnd        Date: 9/19/96    Time: 12:04p
// Updated in $/ImageMan 16-32/DLL
// Fixed Bug 403 (Long FileName issues)
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 1/18/95    Time: 3:59p
// Created in $/ImageMan 16-32/DLL
// ImageMan 4.0 Beta 1

#include "internal.h"
#pragma hdrstop
#include <string.h>
#include "optblk.h"

#define GROWBLK 256


static char SentinelStamp[4] = "THK";


static int NEAR RemoveKey(LPOPTBLK, LPCSTR);
static int NEAR AddKey(LPOPTBLK, LPCSTR, LPCSTR);

bool FAR SentinelIsOK(LPOPTBLK pOptBlk)
{
	return (memcmp(SentinelStamp, pOptBlk->sentinel, 3) == 0);
}

bool FAR cSentinelIsOK(char *sentinel)
{
	return (memcmp(SentinelStamp, sentinel, 3) == 0);
}

HANDLE FAR OptBlkCreate(LPSTR lpszInit)
{
	HANDLE    hBlk;
	LPOPTBLK  lpOpt;
	long      lSize; 
	int       nUsed;
  
	if (lpszInit) {
		nUsed = _fstrlen(lpszInit);
		lSize = sizeof(OPTBLK) + nUsed - 1;
	}
	else {
		lSize = sizeof(OPTBLK) + GROWBLK - 1;
		nUsed = 0;
	}
  
	if (hBlk = GlobalAlloc(GHND, lSize)) {
		lpOpt = (LPOPTBLK)GlobalLock(hBlk);
		lpOpt->nBlkSize = (int)lSize - sizeof(OPTBLK);
		lpOpt->nUsed = nUsed;
		memcpy(lpOpt->sentinel, SentinelStamp, 3); //++Sygsky
//		lpOpt->sentinel[0] = 'T';
//		lpOpt->sentinel[1] = 'H';
//		lpOpt->sentinel[2] = 'K';
        
		// Now initialize the OptBlk with lpszInit
		if (lpszInit) {
			// translate "\r" 2 char string to dec 13
			LPSTR	p;
			while (p = _fstrchr(lpszInit, '\\'))
			{
				p[0] = '\r';
				p[1] = '\r';
			}

			_fstrcpy(lpOpt->text, lpszInit);
			AnsiUpper(lpOpt->text);
		}
    
		GlobalUnlock(hBlk);
	}
  
	return (hBlk);
}

int FAR OptBlkDel(HANDLE hBlk)
{
  return ((int)GlobalFree(hBlk));
}

HANDLE FAR OptBlkAdd(HANDLE hBlk, LPCSTR lpszKey, LPCSTR lpszAdd)
{
	LPOPTBLK  lpOpt;
	int       nLen;

	if (!hBlk) return (NULL);
  
	lpOpt = (LPOPTBLK)GlobalLock(hBlk);

	// check for sentinel value...
	if (!SentinelIsOK(lpOpt)) {
//	if (!lpOpt || lpOpt->sentinel[0] != 'T' || lpOpt->sentinel[1] != 'H'
//		|| lpOpt->sentinel[2] != 'K') {
		GlobalUnlock(hBlk);
		return (NULL); // it's not a valid OptBlk
	}

	// First remove the old key.
	// Then, if lpszAdd != NULL, add the new value as if the 
	// old one were never there!
	if (lpszKey) {
		RemoveKey(lpOpt, lpszKey);
      
		if (lpszAdd) {
			// make sure we've got enough room to store this doggie...

			// The 4 extra chars are for the '\r' on the front and the " = " in
			// between the key and the value.
			nLen = _fstrlen(lpszKey) + _fstrlen(lpszAdd) + 5; 
			if (lpOpt->nUsed + nLen >= lpOpt->nBlkSize) {
				GlobalUnlock(hBlk); 
				// NOTE -- even though we unlock hBlk, lpOpt is OK 'till we
				// reallocate hBlk. It is only after reallocation that hBlk can
				// move in memory...
				hBlk = GlobalReAlloc(hBlk, lpOpt->nBlkSize + nLen + GROWBLK, 0);
				lpOpt = (LPOPTBLK)GlobalLock(hBlk);
				lpOpt->nBlkSize += GROWBLK + nLen;
			}
			AddKey(lpOpt, lpszKey, lpszAdd);
		}
	}
	GlobalUnlock(hBlk);  
	return (hBlk);
}

//
// Returns 0 if the key was found, 1 if it was not
//
int FAR OptBlkGet(HANDLE hBlk, LPCSTR lpszKey, LPSTR lpBuf)
{
	LPOPTBLK lpOpt;
	LPSTR    lpPtr, lpEnd;
	char     jbuf[200];
	int      retval = 0;

	if (!hBlk) return (1);

	_fstrcpy(jbuf,lpszKey);
	_fstrcat(jbuf," =");
	AnsiUpper(jbuf);
	lpOpt = (LPOPTBLK)GlobalLock(hBlk);

	// check for sentinel value...
	if (!SentinelIsOK(lpOpt)) {
//	if (!lpOpt || lpOpt->sentinel[0] != 'T' || lpOpt->sentinel[1] != 'H' 
//		|| lpOpt->sentinel[2] != 'K') {
		GlobalUnlock(hBlk);
		return (1);	// it's not a valid OptBlk
	}

	if (lpPtr = _fstrstr(lpOpt->text, jbuf)) {
		lpPtr = _fstrchr(lpPtr, '=');
		lpPtr += 2;   // go past the equals and the space
		if (lpEnd = _fstrchr(lpPtr, '\r')) {
			_fstrncpy(lpBuf, lpPtr, (lpEnd - lpPtr));
			lpBuf[lpEnd - lpPtr] = '\0';
		}
		else _fstrcpy(lpBuf, lpPtr);
	}
	else {
		retval = 1;	// not found
	}
	GlobalUnlock(hBlk);
	return (retval);
}

static int NEAR RemoveKey(LPOPTBLK lpOpt, LPCSTR lpszKey)
{
	char  jbuf[200];
	int   nLen;
	LPSTR lpStart,lpEnd;

	_fstrcpy(jbuf,lpszKey);
	_fstrcat(jbuf," =");
	AnsiUpper(jbuf);
  
	if (lpStart = _fstrstr(lpOpt->text, jbuf)) {
		if (lpEnd = _fstrchr(lpStart, '\r')) {
			lpEnd++;    // increment past end of string
			nLen = lpEnd - lpStart;
			_fmemmove(lpStart,lpEnd,((LPSTR)(lpOpt->text + lpOpt->nUsed) - lpEnd)+1);
		}
		else {    // it's the last entry...
			nLen = _fstrlen(lpStart);
			*(lpStart-1) = '\0';    // backup 1 to write over carriage return
			if (lpOpt->text[0] == '\0') nLen++; // if deleted all keys...
		}
		lpOpt->nUsed -= nLen;
	}
  
	return 0;
}

static int NEAR AddKey(LPOPTBLK lpOpt, LPCSTR lpszKey, LPCSTR lpszAdd)
{
	_fstrcat(lpOpt->text, "\r");
	_fstrcat(lpOpt->text,lpszKey);
	_fstrcat(lpOpt->text," = ");
	_fstrcat(lpOpt->text,lpszAdd);
	AnsiUpper(lpOpt->text);

	lpOpt->nUsed = _fstrlen(lpOpt->text) + 1;
  
	return (0);
}

int FAR OptBlkGetLength(HANDLE hBlk, LPCSTR lpszKey, LPINT lpSize)
{
	LPOPTBLK lpOpt;
	LPSTR    lpPtr, lpEnd;
	char     jbuf[200];
	int      retval = 0;

	if (!hBlk) return (1);

	_fstrcpy(jbuf,lpszKey);
	_fstrcat(jbuf," =");
	AnsiUpper(jbuf);
	lpOpt = (LPOPTBLK)GlobalLock(hBlk);

	// check for sentinel value...
	if (!SentinelIsOK(lpOpt)) {
//	if (!lpOpt || lpOpt->sentinel[0] != 'T' || lpOpt->sentinel[1] != 'H' 
//		|| lpOpt->sentinel[2] != 'K') {
		GlobalUnlock(hBlk);
		return (1);	// it's not a valid OptBlk
	}

	if (lpPtr = _fstrstr(lpOpt->text, jbuf)) {
		lpPtr = _fstrchr(lpPtr, '=');
		lpPtr += 2;   // go past the equals and the space
		if (lpEnd = _fstrchr(lpPtr, '\r')) {
			*lpSize = lpEnd - lpPtr;
		} else 
			*lpSize = _fstrlen( lpPtr );
	}
	else {
		retval = 1;	// not found
	}
	GlobalUnlock(hBlk);
	return (retval);
}
