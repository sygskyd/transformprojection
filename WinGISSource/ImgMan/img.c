// ///////////////////////////////////////////////////////////////////////////
//
// img.c
//
//
// GEM IMG file decoder
//
//
// ImageMan Image Processing Library
//
// Copyright (c) 1993-5 Data Techniques, Inc.
// All Rights Reserved
//
// ///////////////////////////////////////////////////////////////////////////
// $History: IMG.C $
// 
// *****************  Version 2  *****************
// User: Timk         Date: 6/30/95    Time: 12:04p
// Updated in $/ImageMan 16-32/DILS
// Part 1 of massive changes for read/write using function pointer blocks.
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 1/17/95    Time: 4:03p
// Created in $/ImageMan 16-32/DILS
// ImageMan 4.0 Beta 1
// 

#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <assert.h>

#include "internal.h"
#include "img.h"     // stringtable defines

#define WBUFSIZE  512
#define VERSION "IMG File Reader V1.0"

typedef struct tagFileStruct {
	LPVOID		lpOpenInfo;
	LPVOID		lpHandle;
	LPIMG_IO		lpIO;
	long        offset;			  // offset of image in file
	long        imbedlen;
	int         nPatLen;			  // pattern length
	int         nPixWid;			  // pixel width (determines resolution)
	int         nPixDepth;		  // pixel depth (determines resolution)
	int         nWidth;			  // width of image
	int         nRows;			  // # of rows in image
	long        rowSize;			  // size of 1 row of the image
	int         nCurRow;			  // current row
	char        wbuf[WBUFSIZE];
	int         wbufcnt;
}           FileStruct;

typedef struct tagIMGHEADER {
	WORD        wSig[3];			  // signature words
	WORD        nPatLen;
	WORD        nPixWid;			  // # of (get this!) *microns* per pixel!!
	WORD        nPixDepth;		  // same as above stupidity...
	WORD        nLineWidth;		  // width of image
	WORD        nImageDepth;	  // height of image

}           IMGHEADER;


int PASCAL  wgetc(FileStruct FAR *);
int FAR PASCAL read_row(FileStruct FAR *, LPSTR, LPSTR, unsigned, unsigned);
void        swap(LPSTR a);

HANDLE      hInst;
DEBUGPROC   DebugStringOut;

#ifdef WIN32

#ifdef WINGIS // so the file TLSByJHC.h was included

#ifdef NDEBUG
#define SETERROR(E) bJHCTLSSetValue((DWORD)(E))
#else  // NDEBUG
static void IM2IMGerr(int);
#define SETERROR(E) { \
	bJHCTLSSetValue((DWORD)(E))); \
	if ((E) != IMG_OK) IM2IMGerr(E); \
	}
#endif // NDEBUG

#define GETERROR    nJHCTLSGetValue()

#else	// WINGIS

static DWORD TlsIndex;			  // Thread local storage index
#ifdef NDEBUG
#define SETERROR(E) TlsSetValue(TlsIndex,(LPVOID)(DWORD)(E))
#else // NDEBUG
static void IM2IMGerr(int);
#define SETERROR(E) { \
	TlsSetValue(TlsIndex,(LPVOID)(DWORD)(E)); \
	if ((E) != IMG_OK) IM2IMGerr(E); \
	}
#endif //NDEBUG

#define GETERROR    ((DWORD)TlsGetValue(TlsIndex))
#endif // WINGIS

#else // WIN32
static int  nErrID;				  // id of latest error
#define SETERROR(E) nErrID = (E)
#define GETERROR    (nErrID)
#endif // WIN32


#ifdef WIN32
// /////////////////////////////////////////////////////////////////////////
//
// DllMain
//
// This is the Win32 'DllEntryPoint'. It is similar in function to LibMain
// and the WEP in 16-bit land, but is much more thorough. LibMain is
// called whenever a process or thread attaches or detaches to the
// BMP DIL
//
// Entry:
// hinstDLL  - handle to the ImageMan DLL
// fdwReason - notification code (attach/detach)
// DLL_PROCESS_ATTACH
// DLL_PROCESS_DETACH
// DLL_THREAD_ATTACH
// DLL_THREAD_DETACH
// lpvReserved - NULL during dynamic process attach/detach, non-NULL
// during static attach/detach.
// Exit:
// DLL_PROCESS_ATTACH - TRUE for success, FALSE for failure
// all others         - return code ignored
// /////////////////////////////////////////////////////////////////////////
#pragma optimize("",off)
BOOL APIENTRY DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
	switch (fdwReason) {
		// The DLL is attaching to a process, due to LoadLibrary.
		case DLL_PROCESS_ATTACH:
			assert(!lpvReserved);  // must always be loaded via LoadLibrary!

			hInst = hinstDLL;		  // save our identity
#ifndef WINGIS
			// Allocate a "Thread Local Storage" (TLS) index right away.

			if ((TlsIndex = TlsAlloc()) == 0xFFFFFFFF)
				return (FALSE);
			// Fall thru: Initialize the index for first thread.
#else
			bJHCTLSClear();
#endif

		// The attached process created a new thread.
		case DLL_THREAD_ATTACH:
#ifndef WINGIS
			// Initialize the TLS index for this thread... it's actually 'nErrID'
			if (!TlsSetValue(TlsIndex, (LPVOID) 0))
				return (FALSE);
#else
			nJHCTLSAlloc(); // Stamp this thread to be existed
#endif
			break;

		// The thread of the attached process terminates.
		case DLL_THREAD_DETACH:
#ifdef WINGIS
			bJHCTLSFree(); // Free this thread entry
#endif
			break;

		// The DLL is detaching from a process, due to a call to FreeLibrary.
		case DLL_PROCESS_DETACH:
#ifndef WINGIS
			// Release the TLS index.
			TlsFree(TlsIndex);
#else
			bJHCTLSClear();
#endif
			break;
	}
	return (TRUE);

	UNREFERENCED_PARAMETER(lpvReserved);
}
#pragma optimize("",on)

#else // WIN32

int FAR PASCAL LibMain(hInstance, wDataSeg, wHeapSize, lpCmdLine)
	HANDLE      hInstance;
	WORD        wDataSeg, wHeapSize;
	LPSTR       lpCmdLine;
{
	if (wHeapSize == 0)			  /* no heap indicates fatal error */
		return 0;

#if DEBUG
	{
		HANDLE      hMod = GetModuleHandle("IMGMAN2.DLL");
		if (hMod)
			DebugStringOut = (DEBUGPROC) GetProcAddress(hMod, "DebugStringOut");
	}
#endif

	hInst = hInstance;
	return 1;
}
#endif

VOID WINAPI WEP(int bSystemExit)
{
	if (bSystemExit) {			  // if exiting Windows entirely

	}
}

//------------------------------------------------------------------

int IMAPI   IMDLverify_img(LPSTR lpVerfBuf, LPVOID lpHandle, LPIMG_IO lpIO)
{
	IMGHEADER FAR *lpHead = (IMGHEADER FAR *) lpVerfBuf;

	if (lpHead->wSig[0] != 256 || lpHead->wSig[1] != 2048 || lpHead->wSig[2] != 256)
		return 0;
	else
		return 1;
}

LPSTR IMAPI IMDLget_ver()
{
	return (LPSTR) (VERSION);
}

int IMAPI   IMDLescape(HANDLE hImage, int nInCnt, LPVOID lpIn, LPVOID lpOut)
{
	return IMG_NSUPPORT;
}

int IMAPI   IMDLinit_image(LPCSTR lpOpenInfo, LPHANDLE hFile, LPVOID lpHandle, long offset, long lLen, LPIMG_IO lpIO)
{
	FileStruct FAR *fs;
	LPSTR       tStr;
	IMGHEADER   head;

	lpIO->seek(lpHandle, offset, 0);
	if (lpIO->read(lpHandle, &head, sizeof(head)) != sizeof(head)) {
		lpIO->close(lpHandle);
		SETERROR(IDS_READERR);
		return IMG_FILE_ERR;
	}
	if (head.wSig[0] != 256 || head.wSig[1] != 2048 || head.wSig[2] != 256) {
		lpIO->close(lpHandle);
		SETERROR(IDS_READERR);
		return IMG_FILE_ERR;
	}
	// create new internal structure
	if ((*hFile = GlobalAlloc(DLLHND, (long) sizeof(FileStruct))) == NULL) {
		lpIO->close(lpHandle);
		SETERROR(IDS_MEMERR);
		return IMG_NOMEM;
	}
	// fill in the structure
	fs = (FileStruct FAR *) GlobalLock(*hFile);
	fs->nCurRow = 0;
	fs->lpOpenInfo = lpOpenInfo;
	fs->lpHandle = lpHandle;
	fs->lpIO = lpIO;
	fs->offset = offset;
	fs->imbedlen = lLen;
	fs->nPatLen = head.nPatLen;
	swap((LPSTR) & fs->nPatLen);
	fs->nPixWid = head.nPixWid;
	swap((LPSTR) & fs->nPixWid);
	fs->nPixDepth = head.nPixDepth;
	swap((LPSTR) & fs->nPixDepth);
	fs->nWidth = head.nLineWidth;
	swap((LPSTR) & fs->nWidth);
	fs->nRows = head.nImageDepth;
	swap((LPSTR) & fs->nRows);
	fs->rowSize = (fs->nWidth + 7) / 8;

	fs->wbufcnt = (int) WBUFSIZE;

	GlobalUnlock(*hFile);

	return IMG_OK;
}

int IMAPI   IMDLset_ext(HANDLE hFile, LPSTR ext)
{
	DEBUGOUT(DILAPI, "\t%s-->set_ext(%d,%s)", (LPSTR) __FILE__, hFile, (LPSTR) ext);
	// no extensions to differentiate between - just return OK
	return IMG_OK;
}

int IMAPI   IMDLclose_image(HANDLE hFile)
{
	FileStruct FAR *fs;

	DEBUGOUT(DILAPI, "\t%s-->close_image(%d)", (LPSTR) __FILE__, hFile);
	// check to see if file is still open - if it is, close it
	if ((fs = (FileStruct FAR *) GlobalLock(hFile)) == NULL) {
		SETERROR(IDS_INVHAND);
		return IMG_INV_HAND;
	}
	if (fs->lpHandle) {
		fs->lpIO->close(fs->lpHandle);
	}

	GlobalUnlock(hFile);
	GlobalFree(hFile);
	return IMG_OK;
}

int IMAPI   IMDLimg_name(HANDLE hFile, HANDLE FAR * lpHand)
{
	FileStruct FAR *fs;

	if ((fs = (FileStruct FAR *) GlobalLock(hFile)) == NULL) {
		SETERROR(IDS_INVHAND);
		return IMG_INV_HAND;
	}
//	*lpHand = fs->fName;

	GlobalUnlock(hFile);
	return IMG_OK;
}

int IMAPI   IMDLopen(HANDLE hFile, LPVOID lpHandle)
{
	FileStruct FAR *fs;
	OFSTRUCT    of;

	if ((fs = (FileStruct FAR *) GlobalLock(hFile)) == NULL) {
		SETERROR(IDS_INVHAND);
		return IMG_INV_HAND;
	}
	if (!fs->lpHandle) {
		if (fs->lpIO->open(fs->lpOpenInfo, OF_READ, &(fs->lpHandle)) != IMG_OK) {
			GlobalUnlock(hFile);
			SETERROR(IDS_NOOPEN);
			return IMG_NOFILE;
		}
	}
	fs->lpIO->seek(fs->lpHandle, fs->offset + sizeof(IMGHEADER), 0);

	fs->wbufcnt = (int) WBUFSIZE;

	GlobalUnlock(hFile);
	return IMG_OK;
}

int IMAPI   IMDLclose_keep(HANDLE hFile)
{
	FileStruct FAR *fs;

	DEBUGOUT(DILAPI, "\t%s-->close_keep(%d)", (LPSTR) __FILE__, hFile);
	if ((fs = (FileStruct FAR *) GlobalLock(hFile)) == NULL) {
		SETERROR(IDS_INVHAND);
		return IMG_INV_HAND;
	}
	if (!fs->lpHandle) {
		fs->lpIO->close(fs->lpHandle);
		fs->lpHandle = NULL;
	}

	GlobalUnlock(hFile);
	return IMG_OK;
}

int IMAPI   IMDLread_rows(HANDLE hFile, LPSTR buf, UINT rows, UINT rowSize, UINT start, LPSTATUS lpStat)
{
	FileStruct FAR *fs;
	HPSTR       tmpBuf, lpOut, lpPat;
	LPSTR       lpDecodeTmp;
	unsigned char c;
	int         nWid, nRepeat, i, j, k;

	DEBUGOUT(DILAPI, "\t%s-->%s(%d, %x:%x, %u, %u, %u, %x:%x)", (LPSTR) __FILE__, (LPSTR) "read_rows", hFile, HIWORD(buf), LOWORD(buf), rows, rowSize, start, HIWORD(lpStat), LOWORD(lpStat));
	if ((fs = (FileStruct FAR *) GlobalLock(hFile)) == NULL) {
		SETERROR(IDS_INVHAND);
		return IMG_INV_HAND;
	}
	lpDecodeTmp = GlobalAllocPtr(GHND, fs->rowSize);

	lpOut = (HPSTR) buf;
	lpOut += (LONG) rowSize *(rows - 1);

	while (rows--) {

		DEBUGOUT(DILSETLINE, (LPSTR) (&rows), NULL);
		DEBUGOUT(DILINT, "\t%sRows = %d", (LPSTR) __FILE__, rows);

		lpStat->lAcc += rowSize;
		if (lpStat->lpfnStat && lpStat->lAcc > lpStat->lInterval) {
			lpStat->lDone += lpStat->lAcc;
			lpStat->lDone = min(lpStat->lDone, lpStat->lTotal);
			(*lpStat->lpfnStat) (lpStat->hImage, (int) ((lpStat->lDone * 100) / lpStat->lTotal), lpStat->dwUserInfo);
			lpStat->lAcc %= lpStat->lInterval;
		}
		nRepeat = 0;
		nWid = (fs->nWidth + 7) / 8;

		tmpBuf = lpDecodeTmp;

		while (nWid > 0) {
			c = wgetc(fs);
			switch (c) {
				case 0x80:			  // literal string - output directly to buf

					for (i = wgetc(fs); i; i--, nWid--)
						*tmpBuf++ = ~wgetc(fs);
					break;
				case 0x00:			  // repeat or pattern

					c = wgetc(fs);
					if (c == 0x00) {  // it's a repeat count

						c = wgetc(fs);
						nRepeat = wgetc(fs);
						DEBUGOUT(DILINT, "\t\t\tRepeat = %d", nRepeat);
					} else {			  // pattern run: c = run length of pattern

						DEBUGOUT(DILINT, "\t\t\tPattern Run = %d", c);
						j = fs->nPatLen;
						lpPat = tmpBuf;
						while (j--) {
							*tmpBuf++ = ~wgetc(fs);
							nWid--;
						}
						for (k = c - 1; k--;) {
							_fmemcpy(tmpBuf, lpPat, fs->nPatLen);
							tmpBuf += fs->nPatLen;
							nWid -= fs->nPatLen;
						}
					}
					break;
				default:
					if (c > 0x80) {  // black run

						_fmemset(tmpBuf, 0x00, c & 0x7f);
					} else {			  // white run

						_fmemset(tmpBuf, 0xff, c & 0x7f);
					}
					tmpBuf += c & 0x7f;
					nWid -= c & 0x7f;
					break;
			}
		}
#if DEBUG
		if (nWid)
			DEBUGOUT(DILINT, "\t\t\tnWid != 0: %d", nWid);
#endif

		if (nRepeat) {
			for (i = 0; i < nRepeat; i++, lpOut -= rowSize) {
				_fmemcpy(lpOut, lpDecodeTmp + start, rowSize);
				fs->nCurRow++;
			}
		} else {
			_fmemcpy(lpOut, lpDecodeTmp + start, rowSize);
			lpOut -= (long) rowSize;
			fs->nCurRow++;
		}
	}

	GlobalFreePtr(lpDecodeTmp);

	GlobalUnlock(hFile);
	return IMG_OK;
}


int IMAPI   IMDLset_row(HANDLE hFile, unsigned row)
{
	FileStruct FAR *fs;
	LPSTR       lpBuf;
	STATUS      stat;

	DEBUGOUT(DILAPI, "\t%s-->set_row(%d,%d)", (LPSTR) __FILE__, hFile, row);
	if ((fs = (FileStruct FAR *) GlobalLock(hFile)) == NULL) {
		SETERROR(IDS_INVHAND);
		return IMG_INV_HAND;
	}
	lpBuf = GlobalAllocPtr(GHND, fs->rowSize);

	if (fs->nCurRow != row - 1) {
		fs->lpIO->seek(fs->lpHandle, fs->offset + sizeof(IMGHEADER), 0);
	}
	stat.lpfnStat = NULL;
	while (row--)
		IMDLread_rows(hFile, lpBuf, 1, (UINT)fs->rowSize, 0, (LPSTATUS) & stat);

	GlobalFreePtr(lpBuf);
	GlobalUnlock(hFile);
	return IMG_OK;
}

int IMAPI   IMDLbuildDIBhead(HANDLE hFile, INTERNALINFO FAR * lpii)
{
	LPBITMAPINFOHEADER lpbi;
	FileStruct FAR *fs;
	HANDLE      hMem;

	DEBUGOUT(DILAPI, "\t%s-->buildDIBhead(%d)", (LPSTR) __FILE__, hFile);
	if ((fs = (FileStruct FAR *) GlobalLock(hFile)) == NULL) {
		SETERROR(IDS_INVHAND);
		return IMG_INV_HAND;
	}
	if ((hMem = GlobalAlloc(GHND, sizeof(BITMAPINFO) + sizeof(RGBQUAD))) == NULL) {
		SETERROR(IDS_MEMERR);
		return IMG_NOMEM;
	}
	lpbi = (LPBITMAPINFOHEADER) GlobalLock(hMem);
	lpbi->biSize = sizeof(BITMAPINFOHEADER);
	lpbi->biWidth = fs->nWidth;
	lpbi->biHeight = fs->nRows;
	lpbi->biBitCount = 1;
	lpbi->biPlanes = 1;
	lpbi->biCompression = BI_RGB;
	lpbi->biSizeImage = ((lpbi->biWidth * lpbi->biBitCount + 31) / 32 * 4) * lpbi->biHeight;
	lpbi->biXPelsPerMeter = 0;
	lpbi->biYPelsPerMeter = 0;
	lpbi->biClrUsed = 0;
	lpbi->biClrImportant = 0;

	// set palette entries
	((LPBITMAPINFO) (lpbi))->bmiColors[0].rgbBlue = 0;
	((LPBITMAPINFO) (lpbi))->bmiColors[0].rgbRed = 0;
	((LPBITMAPINFO) (lpbi))->bmiColors[0].rgbGreen = 0;
	((LPBITMAPINFO) (lpbi))->bmiColors[0].rgbReserved = 0;
	((LPBITMAPINFO) (lpbi))->bmiColors[1].rgbBlue = 0xff;
	((LPBITMAPINFO) (lpbi))->bmiColors[1].rgbRed = 0xff;
	((LPBITMAPINFO) (lpbi))->bmiColors[1].rgbGreen = 0xff;
	((LPBITMAPINFO) (lpbi))->bmiColors[1].rgbReserved = 0;

	lpii->lOrgWid = lpbi->biWidth;	// set height&wid for Imgmgr

	lpii->lOrgHi = lpbi->biHeight;
	lpii->bFlags = 0;
	lpii->lpbi = (LPBITMAPINFO) lpbi;

	GlobalUnlock(hFile);
	return IMG_OK;
}

int IMAPI   IMDLprint(hFile, hPrnDC, lpDest, lpSrc)
	HANDLE      hFile;
	HDC         hPrnDC;
	LPRECT      lpDest, lpSrc;
{
	return 0;
}


int IMAPI   IMDLrender(HANDLE hFile, HDC hDC, LPRECT lpDest, LPRECT lpSrc)
{
	return 0;
}

int IMAPI   IMDLerror_string(LPSTR lpszBuf, int nMaxLen)
{
	LoadString(hInst, GETERROR, lpszBuf, nMaxLen);
	return IMG_OK;
}

void        swap(a)
	LPSTR       a;
{
	register char tmp;

	tmp = a[1];
	a[1] = a[0];
	a[0] = tmp;
}

// wgetc
// acts like getc(), just uses your own buffer and counter
int PASCAL  wgetc(FileStruct FAR * fs)
{
	if (fs->wbufcnt >= WBUFSIZE) {
		if ((fs->lpIO->read(fs->lpHandle, (LPSTR) fs->wbuf, WBUFSIZE)) == -1)
			return EOF;

		fs->wbufcnt = 0;
	}
	return (int) ((unsigned char) fs->wbuf[fs->wbufcnt++]);
}

#ifndef NDEBUG
static void IM2IMGerr(int e)
{
	char        s1[80], s2[120];

	LoadString(hInst, e, s1, sizeof(s1));
	wsprintf(s2, "IM2IMG: Error: (%d) \"%s\".\n", e, s1);
	OutputDebugString(s2);
}
#endif

#if DEBUG
void CDECL FAR Debug(UINT wCategory, LPSTR lpFormat,...)
{
	LPSTR       vaArgs;

	if (DebugStringOut) {
		va_start(vaArgs, lpFormat);
		(*DebugStringOut) (wCategory, lpFormat, vaArgs);
		va_end(vaArgs);
	}
}
#else
void CDECL FAR Debug(UINT wCategory, LPSTR lpFormat,...)
{
}
#endif
