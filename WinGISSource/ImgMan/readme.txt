ImageMan DLL Suite
Version 6.50

August 8, 2000

Thank you for purchasing the ImageMan DLL Suite.  Please be sure to register
your copy of ImageMan by sending in the Registration card. By doing so we'll keep
you informed of new releases and you'll be able to download updates from our Web 
based Online Update Center.

When visiting our website also be sure to join the ImageMan E-Mail Mailing list. By 
doing so we'll keep you informed of new updates, sample and tips & tricks for using
ImageMan.

ImageMan includes 60 days of free phone, email, fax and web support. The sixty day 
support period starts with your first support call, after that several plans are 
available to provide various levels of support. Visit our website at 
www.data-tech.com for complete plan details and to order

If you have any questions, problems or suggestions please contact us at:

Mail:   Data Techniques, Inc.
        300 Pensacola Road
        Burnsville, NC 28714

Tech:	828-682-0161	9-5 EST
Sales:  828-682-4111
Fax:    828-682-0025

Email:  support@data-tech.com

Web:    www.data-tech.com


Important Information about GIF and LZW Compression
---------------------------------------------------
If you are going to distribute the GIF and TIFF (w/LZW support) Readers
and writers you'll need to obtain a license from Unisys as mentioned below.

By default these files are locked up in an encrypted zipfile (IMLZW60.ZIP).
To unlock this file you must fax us the first page of your Unisys LZW license
and we'll call you with the password to unlock the GIF/TIFF w/LZW enabled
filters.


	WARNING
	-------

Use of this software for providing LZW capability for any purpose
is not authorized unless user first enters into a license agreement
with Unisys under US Patent No. 4,558,302 and foreign counterparts.
For information concerning licensing, please contact:

	Unisys
	Welch Licensing Department - C1SW19
	Township Line & Union Meeting Roads
	PO Box 500
	Blue Bell, PA 19424


Changes in version 6.50
--------------
- Fixed a bug which caused the ImageMan DLL to leave invalid image files open after calling ImgOpenSolo, etc..
- Fixed a bug in the TIFF Group4 Decoder which could cause a GPF when the first line of an image was all black.
- Fixed a bug which caused an error when displaying multi-page tiff files that were embedded in other files.
- Fixed palette leak with a palettized image when setting PageNumber property.
- When saving an image from within the VB design time environment with ADO or ODBC loaded you get the 32508 error, "Could not load export filter."  This problem was due to a Thread Local Storage limit of 64 TLS indexes per proccess.  The fix is to dynamically link in the C runtime library.  While there is still a remote possibility of running out of TLS indexes we now only use 1 internal TLS index for every DIL and DEL loaded instead of 2.
- Fixed a bug which caused a GPF when loading certain G3 tiffs which had black data on the extreme right edge of the first line of the image.
- Fixed an issue in the JPG DIL/DEL which causes the x/y resolutions to be written/read incorrectly.
- Fixed problem opening embedded PNG files.
- Tiff CMYK support was corrected to support writing out tiff files for all different size images without skewing.

Changes in version 6.04
--------------
- Fixed problem attempting to open DXF files with no height and width specified.  This was causing us to crash.
- Attempting to open 32 bit bitmap now returns the not-supported error.  This was causing us to crash if you got the image open, then loaded it and tried to do something with it.
- Fixed a bug in the TIFF file which prevented us from opening files with LONG sized Width/Height tags
- Fixed problem with common dialog preview window incorrectly reducing the color depth on the image when in 32 bit display mode.
- Fixed a problem writing CMYK images at certain widths would cause the image to be skewed when viewed.
- Fixed problem when copying image from clipboard that did not have the number of palette entries compared to the bmi bit count.  Images would not get copied in correctly.
- Fixed bug setting nMaxColors in the CImgMan2 class when running in 32 bit display mode.

Changes in Version 6.02
--------------
- Fixed a bug in octree palette reduction - 16 bit only.  The color selection quality was being lost.
- Added support for reading malformed Kofax tiff files (RowsPerStrip tag is zero).
- Fixed a bug which caused the Red & Blue entries of the palette from an image created by ImgReduceToPal to be swapped.
- Fixed a bug in ImgInvert which left part of the top right of an image uninverted.
- Fixed a problem which caused a GPF when loading portions of some TIFF G4 images.
- Fixed a bug in our internal Image loading code which caused the initial image data to be corrupted when images with less than the number of colors supported by the bitness ie 63 colors in an 8 bit image were loaded.

Changes in Version 6.01
--------------
- (16 Bit Only) - Fixed a potential GPF when calling ImgInit on systems with PATH statements in the environment.
- Fixed a bug in the DLL which caused a GPF when attempting to call ImgOpenMem on a GIF image.
- Fixed a bug which could cause a GPF when using ImgCopy to scale an image vertically and using the interpolated scaling method. This only occured when scaling vertically and leaving the horizontal size the same.
- Fixed ImgContrast function so that filtering sub-channels works for all channels instead of just the blue channel no matter which sub-channel was selected.
- Fixed bug causing crash in the ImgPlugInHostLoad function.
- Fixed a bug in the Plugin Support which caused a GPF when ImgPlugInHostLoad was called and the specified directory didnt contain any plugin files or didnt exist.
- Fixed a bug in ImgFilterLow() which could cause a GPF if the filter values exceed a certain threshold.
- Fixed a small memory leak which occured when closing images.
