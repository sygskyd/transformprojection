//////////////////////////////////////////////////////////////
// Nag.h- Header file for Bother screens
//

#include <windows.h>
#include <shellapi.h>
#include <tchar.h>
#include <time.h>

#ifdef WIN32
#include <winreg.h>  
#define LOADDS 
#else    
#define LOADDS _loadds 
#include <string.h>

// Win 16 DialogIndirect stuff
typedef struct {
    WORD x;
    WORD y;
    WORD cx;
    WORD cy;
    WORD id;
	DWORD style;  
} DLGITEMTEMPLATE;
typedef DLGITEMTEMPLATE FAR *LPDLGITEMTEMPLATE;

#define ULONG unsigned long

#define DS_CENTER 0
#define DS_3DLOOK 0
#define DS_SETFOREGROUND 0


#endif

LRESULT DisplayBotherScreen(long hinst);
LRESULT LOADDS CALLBACK DialogProc (HWND hdlg, UINT iMsg, WPARAM wParam, LPARAM lParam);