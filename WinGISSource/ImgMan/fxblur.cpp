/////////////////////////////////////////////////////////////////////////////
//
// fxblur.c
//
// ImageMan Effects Module: Blur In & Blur Out
// Exports:
//   doBlur
//
// Version 1.0
// Copyright (c) 1996 Data Techniques, Inc.
// Copyright (c) 1996 Chris Roueche
// All Rights Reserved
//
/////////////////////////////////////////////////////////////////////////////
// $History: fxblur.cpp $
// 
// *****************  Version 2  *****************
// User: Johnd        Date: 4/10/98    Time: 5:06p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Chgd Error Returns to be consistent with other ImageMan Defines
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 7/10/97    Time: 2:02p
// Created in $/imageman 16-32/dll-api level 2
// Initial Lvl 2 Checkin
//

#define STRICT
#define WIN32_LEAN_AND_MEAN
#define NOSERVICE
#include <windows.h>
#include <windowsx.h>
#include "imgman.h"
#include "fxparam.h"

// Blur-specific storage can be found in fxparam.h:tag_Blur

/////////////////////////////////////////////////////////////////////////////
//
/////////////////////////////////////////////////////////////////////////////

static int nextblurlevel(int scale,int max,int step)
{
	int level = scale+step;

	if (level >= 1 && level <= max) {
		level = max/scale;
		do {
			scale += step;
		} while (scale > 1 && scale < max && max/scale == level);
	}
	return (scale);
}

/////////////////////////////////////////////////////////////////////////////
//
/////////////////////////////////////////////////////////////////////////////

static int enumblurlevels(int scale,int max,int step)
{
	int i = 0;

	do {
		i++;
		scale = nextblurlevel(scale,max,step);
	} while (scale > 1 && scale < max);

	return (i);
}

/////////////////////////////////////////////////////////////////////////////
//
/////////////////////////////////////////////////////////////////////////////

static int initblur(HDC hDC,PEFFECTBLK peb)
{
	peb->blur.max.cx = peb->rSrc.right-peb->rSrc.left;
	peb->blur.max.cy = peb->rSrc.bottom-peb->rSrc.top;

	// Create an offscreen "lense" half the size of the source area
	peb->blur.hdc = CreateCompatibleDC(hDC);
	if (!peb->blur.hdc) return (IMGFX_OUTOFRESOURCES);
	peb->blur.hbm = CreateCompatibleBitmap(hDC,peb->blur.max.cx,peb->blur.max.cy);
	if (!peb->blur.hbm) {
		DeleteDC(peb->blur.hdc);
		return (IMGFX_OUTOFRESOURCES);
	}

	if( peb->hPal ) {
		SelectPalette( peb->blur.hdc, peb->hPal, 0);
		RealizePalette( peb->blur.hdc );
	}

	peb->blur.holdbm = SelectBitmap(peb->blur.hdc,peb->blur.hbm);

  // determine initial lense scale and direction
	if (peb->nEffect == IMFX_BLUR_IN) { // start w/ one giant pixel
		peb->blur.lense = peb->blur.max;
		peb->blur.dir = -1;
	}
	else { // start w/ full image
		peb->blur.lense.cx = peb->blur.lense.cy = 1;
		peb->blur.dir = 1;
	}

	// find number of output levels and setup accumulators
	peb->blur.accum.cx = peb->blur.accum.cy = 0;
	peb->blur.step.cx = enumblurlevels(1,peb->blur.max.cx,1);
	peb->blur.step.cy = enumblurlevels(1,peb->blur.max.cy,1);
	peb->nStepsTotal = max(peb->blur.step.cx,peb->blur.step.cy);

	return (IMG_OK);
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
//
// doBlur
//
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

int doBlur(HDC hDC,PEFFECTBLK peb)
{
	int x,y;

	if (INITIALIZE(peb)) {
		// initialize the first time thru
		if ((x = initblur(hDC,peb)) != IMG_OK) return (x);

		if( peb->hMemDC && peb->hPal ) {
			SelectPalette( peb->hMemDC, peb->hPal, 0);
			RealizePalette( peb->hMemDC );
		}
	}
	if (CLEANUP(peb)) { // cleanup
		SelectBitmap(peb->blur.hdc,peb->blur.holdbm);
		DeleteBitmap(peb->blur.hbm);
		DeleteDC(peb->blur.hdc);
		return (IMG_OK);
	}

 	// copy image onto lense bitmap
	x = peb->blur.max.cx/peb->blur.lense.cx;
	y = peb->blur.max.cy/peb->blur.lense.cy;
	if (peb->pdib || peb->hMemDC) {
		if (peb->hMemDC) {
			StretchBlt(peb->blur.hdc,0,0,x,y,
				peb->hMemDC,peb->rSrc.left,peb->rSrc.top,
				peb->rSrc.right-peb->rSrc.left,
				peb->rSrc.bottom-peb->rSrc.top,SRCCOPY);
		}
		else {
			StretchDIBits(peb->blur.hdc,0,0,x,y,
				peb->rSrc.left,(int)peb->pdib->biHeight-peb->rSrc.bottom,
				peb->rSrc.right-peb->rSrc.left,peb->rSrc.bottom-peb->rSrc.top,
				peb->pbits,(LPBITMAPINFO)peb->pdib,DIB_RGB_COLORS,SRCCOPY);
		}
		// then copy lense image onto destination
		StretchBlt(hDC,peb->rDst.left,peb->rDst.top,
			peb->rDst.right-peb->rDst.left,peb->rDst.bottom-peb->rDst.top,
			peb->blur.hdc,0,0,x,y,SRCCOPY);
	}
	else {
		// no image, so erase clipped area (can we do something better here?)
		Rectangle(hDC,peb->rDst.left,peb->rDst.top,peb->rDst.right+1,peb->rDst.bottom+1);
	}

	// update lense scale
	if ((peb->blur.accum.cx += peb->blur.step.cx) >= peb->nStepsTotal) {
		peb->blur.accum.cx -= peb->nStepsTotal;
		peb->blur.lense.cx = nextblurlevel(peb->blur.lense.cx,peb->blur.max.cx,peb->blur.dir);
	}
	if ((peb->blur.accum.cy += peb->blur.step.cy) >= peb->nStepsTotal) {
		peb->blur.accum.cy -= peb->nStepsTotal;
		peb->blur.lense.cy = nextblurlevel(peb->blur.lense.cy,peb->blur.max.cy,peb->blur.dir);
	}

	peb->nStepsDone++;

	return (IMG_OK);
}
