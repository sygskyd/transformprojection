/////////////////////////////////////////////////////////////////////////////
//
//    constrast.cpp
//
//    Image Constrast module
//
//    ImageMan Image Processing Library
//
//    Copyright (c) 1997 Data Techniques, Inc.
//    All Rights Reserved
//
/////////////////////////////////////////////////////////////////////////////
// $History: contrast.cpp $
// 
// *****************  Version 9  *****************
// User: Ericw        Date: 6/01/99    Time: 4:09p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Fixed bug #197.  Contrasting was not checking for valid color depth
// images to protect itself from.
// 
// *****************  Version 8  *****************
// User: Ericw        Date: 7/27/98    Time: 12:28p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Fixed bug #121 contrasting only blue on sub-channel contrasting.
// 
// *****************  Version 7  *****************
// User: Ericj        Date: 5/06/98    Time: 1:21p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Error checking was to zealous and I prevented contrast from passing
// in negative values.
// 
// *****************  Version 6  *****************
// User: Ericj        Date: 4/01/98    Time: 4:25p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Added bad parameter error checking for some functions.
// 
// *****************  Version 5  *****************
// User: Johnd        Date: 3/20/98    Time: 9:05a
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Updated  internal error handling. Added error string info to TLS data
// instead of the global buffers where it had been stored. Also added
// SetError() func to set the internal error state.
// 
// *****************  Version 4  *****************
// User: Ericw        Date: 1/14/98    Time: 2:35p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Color channel now defaults to RGB_FULL if no specific channel is
// selected.
// 
// *****************  Version 3  *****************
// User: Ericw        Date: 10/01/97   Time: 3:05p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Speed improvement using pin instead of clamping.
// 
// *****************  Version 2  *****************
// User: Ericw        Date: 7/28/97    Time: 2:43p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Declared img_status for 16bit build.
// 
// *****************  Version 1  *****************
// User: Ericw        Date: 7/16/97    Time: 3:13p
// Created in $/ImageMan 16-32/DLL-API Level 2
// Contrasting first version checkin!.
// 

#include "internal.h"
#include <windowsx.h>
#pragma hdrstop
#include <string.h>

#define PIN_OFFSET (256)  // offset to identity lookup
extern int pin[] ;

extern HINSTANCE hInst;			  // who we is in this life... available to all!

void PASCAL GenHistogram
              ( LPBITMAPINFO lpInBMI,HPSTR lpBits,
                USHORT FAR *lpHist,UINT FAR *count) ;

int IMAPI ImgContrast ( HANDLE hImage, int iPercentage, int iMedian, int iChannel )
{
	LPINTERNALINFO lpII ;
	LPBITMAPINFO   lpbi ;
	HPUSTR lpBits ;
	HPUSTR lpBGR  ;
	USHORT FAR *lpHist ;
//	ULONG  FAR *lpSum  ;
	UINT   uCount ;
	UINT   uxx   ;
	UINT   uKVal ;
	UINT   uNVal ;
	UINT   uColors ;
	long   lValue ;
	long   lColor ;
	ULONG  lRow, lCol ;
	ULONG  lHeight, lWidth ;
	ULONG  lRowSize ;
	UINT   uRed, uGreen, uBlue ;
	long   lLowColor  = 0 ;
	long   lHighColor = 0 ;

	if ( iPercentage == 0 )
		return ( IMG_OK ) ;

	if ( iChannel == 0 )
		iChannel = RGB_FULL ;

    if ( (iPercentage < -100) || (iPercentage > 100) || 
			(iMedian < -1) || (iMedian > 255) ||
			(iChannel < 0) || (iChannel > 7) ) {
		SetError( NULL, IMG_BAD_PARAM, 0, NULL, hInst );
		return (IMG_ERR);
	}

	if (!(lpII = (LPINTERNALINFO)GlobalLock( hImage ))) {
		SetError( NULL, IMG_INV_HAND, 0, NULL, hInst );
		return (IMG_ERR);
	}
	if (lpII->bFlags & IMG_DISP_VECTOR) {
		SetError( NULL, IMG_BAD_TYPE, 0, NULL, hInst );
		GlobalUnlock(hImage);
		return (IMG_ERR);
	}
  

	if (!lpII->pDIB) {
		if (InternalLoad( lpII, NULL, &lpII->pDIB, &lpII->hWMF ) != IMG_OK) {
			GlobalUnlock(hImage);
			return (IMG_ERR);
		}
	}
	if ( lpII->pDIB->GetBitCount() < 8 ) { // only do 8 or 24 bit images
	  	// for real, we'll eventually call ImgIncreaseColors
		SetError( NULL, IMG_PROC_ERR, 0, NULL, hInst );
		return ( IMG_ERR ) ;
	}

	lpBits = (HPUSTR) lpII->pDIB->GetDataPtr() ;

	// Create the color histogram
	// Allocate histogram array, 5 bits of precision for each color component,
	// 15 bits is 0-0x7FFF, or 32768 entries
	if(!(lpHist = (USHORT FAR *) GlobalAllocPtr(DLLHND,32768UL*sizeof(USHORT)) ))
		return (NULL);
  GenHistogram ( lpII->pDIB->GetLPBI(), (HPSTR)lpBits, lpHist, & uCount ) ;

	lHeight  = lpII->pDIB->GetHeight () ;
	lWidth   = lpII->pDIB->GetWidth  () ;
	lRowSize = lpII->pDIB->GetRowSize() ;
	uxx = 0 ;
  while ( ( uxx <= 32767   ) &&
		      ( lLowColor == 0 ) )
		{
			if ( lpHist[ uxx ] )
				lLowColor = uxx ;
			uxx++ ;
		}
	uxx = 32767 ;
	while ( ( uxx ) && ( lHighColor == 0 ) )
		{
			if ( lpHist[uxx] )
				lHighColor = uxx ;
			uxx++ ;
		}
	if ( iMedian == 0 )
		iMedian = 128 ;
	else
		if ( iMedian == -1 )     // use median from histogram info
			{
				uxx = ( lLowColor + lHighColor ) / 2 ;
				uBlue   = ( uxx & 0x001F ) << 3 ;
				uGreen  = ( ( uxx & 0x0260 ) >> 5 ) << 3 ;
				uRed    = ( ( uxx & 0x7C00 ) >> 10 ) << 3 ;
				iMedian = ( uBlue + uGreen + uRed ) / 3 ;
			}
	if ( lpII->lpbi->bmiHeader.biBitCount == 24 )
		{
			for ( lRow = 0 ; lRow < lHeight ; lRow++ )
				{
					for ( lCol = 0 ; lCol < lRowSize ; lCol+=3 )
						{
							lpBGR = lpBits + lCol ;
							uBlue  = *lpBGR >> 3 ;
							uGreen = *(lpBGR+1) >> 3 ;
							uRed   = *(lpBGR+2) >> 3 ;
							uKVal = uRed<<10 | uGreen<<5 | uBlue ;
							if ( ( uKVal > lLowColor  ) && 
									 ( uKVal < lHighColor ) )
								{
									for ( uxx = 0 ; uxx < 3 ; uxx ++ )
										{
											int iDoit = ( uxx == 0 ) ? iChannel & RGB_BLUE  :
																	( uxx == 1 ) ? iChannel & RGB_GREEN :
																	( uxx == 2 ) ? iChannel & RGB_RED   : 0 ;
											if ( iDoit ) {
													lValue = ( ( (*lpBGR - iMedian ) * iPercentage ) / 100 ) ;    // this gives color delta
													lValue = *lpBGR + lValue ;        // this gives the new color
													*lpBGR = pin[PIN_OFFSET+lValue] ;
											}
											lpBGR++ ;
										}
								}
						}
					lpBits += lRowSize ;
				}
		}
	else
		{
			lpbi = lpII->pDIB->GetLPBI() ;
			if ( lpbi->bmiHeader.biClrUsed )
				uColors = (UINT) lpbi->bmiHeader.biClrUsed ;
			else
				uColors = 1 << lpbi->bmiHeader.biBitCount ;
			for ( uxx = 0 ; uxx < uColors ; uxx++ )
				{
				  uBlue  = lpbi->bmiColors[uxx].rgbBlue  >> 3 ;
					uGreen = lpbi->bmiColors[uxx].rgbGreen >> 3 ;
					uRed   = lpbi->bmiColors[uxx].rgbRed   >> 3 ;
					uKVal  = uRed<<10 | uGreen<<5 | uBlue  ;
					if ( ( uKVal > lLowColor  ) &&
						   ( uKVal < lHighColor ) )
						{
							if ( iChannel & RGB_RED )
								{
								  lColor = lpbi->bmiColors[uxx].rgbRed ;
									lValue = ( ( (lColor - iMedian ) * iPercentage ) / 100 ) ;
									lValue = lColor + lValue ;
									uNVal = (UINT)CLAMPED( lValue ) ;
									lpbi->bmiColors[uxx].rgbRed = uNVal ;
								}
							if ( iChannel & RGB_GREEN )
								{
									lColor = lpbi->bmiColors[uxx].rgbGreen ;
									lValue = ( ( (lColor - iMedian ) * iPercentage ) / 100 ) ;
									lValue = lColor + lValue ;
									uNVal = (UINT)CLAMPED( lValue ) ;
									lpbi->bmiColors[uxx].rgbGreen = uNVal ;
								}
							if ( iChannel & RGB_BLUE )
								{
									lColor = lpbi->bmiColors[uxx].rgbBlue ;
									lValue = ( ( (lColor - iMedian ) * iPercentage ) / 100 ) ;
									lValue = lColor + lValue ;
									uNVal = (UINT)CLAMPED( lValue ) ;
									lpbi->bmiColors[uxx].rgbBlue = uNVal ;
								}
						}
				}
			// update the palette in the structure
			_fmemcpy ( lpII->lpbi, lpbi, (int)BITSOFFSET((LPBITMAPINFOHEADER)lpbi) ) ;
		}
//	GlobalFreePtr ( lpSum  ) ;
	GlobalUnlock(hImage) ;
	GlobalFreePtr ( lpHist ) ;
	return ( IMG_OK ) ;
}
