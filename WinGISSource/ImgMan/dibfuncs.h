/////////////////////////////////////////////////////////////////////////////
//
// dibfuncs.h
//
// ImageMan SCaling Modules Defines/Prototypes
//
// Copyright (c) 1991-5 Data Techniques, Inc.
// All Rights Reserved
//
// ///////////////////////////////////////////////////////////////////////////
// $History: dibfuncs.h $
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 7/10/97    Time: 2:04p
// Created in $/imageman 16-32/dll-api level 2
// Initial Lvl 2 Checkin
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 1/18/95    Time: 3:59p
// Created in $/ImageMan 16-32/DLL
// ImageMan 4.0 Beta 1

extern HGLOBAL SetDIBScale(LPBITMAPINFOHEADER,LPRECT,LPBITMAPINFOHEADER,LPRECT,DWORD);
extern int ScaleDIB(HPSTR,HPSTR,int,int,UINT,HANDLE);
extern int EndDIBScale(HANDLE);
extern int ExtractDIB(LPBITMAPINFOHEADER,HPSTR,HPSTR,LPRECT);
