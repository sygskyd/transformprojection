/////////////////////////////////////////////////////////////////////////////
//
//   color2.c
//
//   24 Bit Color Reduction Module
//      Implements Heckberts Median Cut & Popularity algorithms
//
//   Copyright (c) 1991-7 Data Techniques, Inc.
//   All Rights Reserved
//
/////////////////////////////////////////////////////////////////////////////
// $History: color2.cpp $
// 
// *****************  Version 6  *****************
// User: Ericw        Date: 8/20/98    Time: 3:33p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Fixed bug 128:  Octree palette problems in 16 bit.  It was a UINT vs
// long issue.
// 
// *****************  Version 5  *****************
// User: Ericw        Date: 10/03/97   Time: 2:50p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Implemented octree reduction when reducing an image that is already a
// palettized image (specifically 256 to 16).
// 
// *****************  Version 4  *****************
// User: Ericw        Date: 9/23/97    Time: 5:14p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Implemented octree color reduction stuff for v6.
// 
// *****************  Version 3  *****************
// User: Ericw        Date: 7/28/97    Time: 2:42p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Remove NEAR spec on GenHistogram.  Would let the contrast stuff build
// correctly under 16bit.
// 
// *****************  Version 2  *****************
// User: Ericw        Date: 7/16/97    Time: 3:11p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Exposed GenHistogram for the contrast feature.
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 7/10/97    Time: 2:02p
// Created in $/imageman 16-32/dll-api level 2
// Initial Lvl 2 Checkin
// 
// *****************  Version 2  *****************
// User: Johnd        Date: 6/01/96    Time: 12:33p
// Updated in $/ImageMan 16-32/DLL
// Fixed Bug #372 - Overflow in ROWSIZE() in RemapPixels with large Images
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 1/18/95    Time: 3:59p
// Created in $/ImageMan 16-32/DLL
// ImageMan 4.0 Beta 1

#include "internal.h"
#pragma hdrstop
#include <assert.h>
#include <string.h>
#include "color2.h"

#include <stdio.h>

#ifndef WIN32
#define BI_BITFIELDS  3L
#endif

#define RED(x)   ((UCHAR)(((x)>>10)&0x001f))
#define GREEN(x) ((UCHAR)(((x)>>5)&0x001f))
#define BLUE(x)  ((UCHAR)((x)&0x001f))

#define HIST_SIZE ( 1 << 15 );

typedef struct tagColorRect {
	struct tagColorRect FAR *next;
	USHORT first,last;        // Indexes into colorList
	long histSum;             // total color count for region

	UCHAR redMin, redMax;
	UCHAR greenMin, greenMax;
	UCHAR blueMin, blueMax;

	UCHAR widestColor;
	UCHAR red,green,blue;     // Eventual palette color
	UCHAR pad[2];             // keep structure long aligned
} COLORRECT, FAR *LPCOLORRECT;

typedef struct _NODE {
    BOOL bIsLeaf;               // TRUE if node has no children
    long nPixelCount;           // Number of pixels represented by this leaf
    long nRedSum;               // Sum of red components
    long nGreenSum;             // Sum of green components
    long nBlueSum;              // Sum of blue components
    struct _NODE FAR * pChild[8];    // Pointers to child nodes
    struct _NODE FAR * pNext;        // Pointer to next reducible node
} NODE;


void   AddColor   ( NODE FAR * FAR *, BYTE, BYTE, BYTE, UINT, UINT, long FAR *, NODE FAR * FAR * ) ;
void   ReduceTree ( UINT, long FAR *, NODE FAR * FAR * ) ;
void   DeleteTree         ( NODE FAR * FAR * ) ;
void   GetPaletteColors   (NODE FAR *, PALETTEENTRY FAR *, UINT FAR *);
int    GetRightShiftCount ( DWORD ) ;
int    GetLeftShiftCount  ( DWORD ) ;
NODE FAR * CreateNode ( UINT, UINT, long FAR *, NODE FAR * FAR * ) ;

static LPCOLORRECT NEAR AllocCR(void)
{
	return (LPCOLORRECT) GlobalAllocPtr(GHND, sizeof(COLORRECT));
}

// Insert COLORECT into list sorted by HistSum
static void NEAR PASCAL InsertCR(LPCOLORRECT FAR *head, LPCOLORRECT lpCR)
{
	if (!*head) {                           // If empty list
		*head = lpCR;
		lpCR->next = NULL;
	} 
	else {
		LPCOLORRECT lpCur,lpPrev;

		lpCur = *head;
		lpPrev = NULL;
		while (lpCur) {
			if (lpCR->histSum > lpCur->histSum) {
				lpCR->next = lpCur;
				if (lpPrev) lpPrev->next = lpCR;
				else *head = lpCR;
				return;
			}
			lpPrev = lpCur;
			lpCur = lpCur->next;
		}
		// Tack onto end of list
		lpPrev->next = lpCR;
		lpCR->next = NULL;
	}
}

static void NEAR PASCAL FreeCRs(LPCOLORRECT head )
{
	LPCOLORRECT lpCur,lpNxt;

	if( !head )
		return;

	lpCur = head;

	while (lpCur) {
		lpNxt = lpCur->next;
		GlobalFreePtr( lpCur );
		lpCur = lpNxt;
	}
}

// Set the mins, maxs & calc the histSum for a ColorRect
// Used by GenOptimizedPalette
static void NEAR PASCAL ProcColorRect(USHORT FAR *lpHist,USHORT FAR *lpColorList,
	LPCOLORRECT lpCR)
{
	UINT i;
	UCHAR color;
	int redDelta,greenDelta,blueDelta;

	lpCR->redMin = lpCR->greenMin = lpCR->blueMin = 31;
	lpCR->redMax = lpCR->greenMax = lpCR->blueMax = 0;
	lpCR->histSum = 0;

	for (i = lpCR->first; i <= lpCR->last; i++) {
		color = RED(lpColorList[i]);
		if (color < lpCR->redMin) lpCR->redMin = color;
		if (color > lpCR->redMax) lpCR->redMax = color;

		color = BLUE(lpColorList[i]);
		if (color < lpCR->blueMin) lpCR->blueMin = color;
		if (color > lpCR->blueMax) lpCR->blueMax = color;

		color = GREEN(lpColorList[i]);
		if (color < lpCR->greenMin) lpCR->greenMin = color;
		if (color > lpCR->greenMax) lpCR->greenMax = color;

		// Add color count to HistSum
		lpCR->histSum += lpHist[lpColorList[i]];
	}                       

	// Determine the widest color
	redDelta = lpCR->redMax - lpCR->redMin;
	greenDelta = lpCR->greenMax - lpCR->greenMin;
	blueDelta = lpCR->blueMax - lpCR->blueMin;

	if (redDelta >= greenDelta && redDelta >= blueDelta) lpCR->widestColor = 'R';
	else if (greenDelta >= redDelta && greenDelta >= blueDelta) lpCR->widestColor = 'G';
	else lpCR->widestColor = 'B';
}

// Used by GenOptimizedPalette
//
// lpInBMI - pointer to bitmap header
// lpBits  - pointer to bits of the image
// lpHist  - pre-allocated array of shorts; destination of color counts
// count   - will receive number of non-zero entries in histogram

void PASCAL GenHistogram(LPBITMAPINFO lpInBMI,HPSTR lpBits,
	USHORT FAR *lpHist,UINT FAR *count)
{
	UCHAR red,green,blue;
	UINT i,idx;
	int nBits,nPad;
	DWORD nRows,nWidth,lRowSize;

	assert(lpHist);
	assert(count);

	nBits = lpInBMI->bmiHeader.biBitCount;

	// Calc Padding
	lRowSize = ROWSIZE(lpInBMI->bmiHeader.biWidth,nBits);
	nPad = (int)(lRowSize-(((lpInBMI->bmiHeader.biWidth*nBits)+7)/8));

	// Traverse DIB; see what colors are used
	for (nRows = lpInBMI->bmiHeader.biHeight; nRows--; lpBits += nPad){
		for (nWidth = lpInBMI->bmiHeader.biWidth; nWidth--; ) {
			if (nBits == 8) {
				// CAR: it'd be faster to make a pre-converted 256 entry lookup table
				// so we could just say "lpHist[*lpBits++]++;"
				idx = *lpBits++;
				blue = lpInBMI->bmiColors[idx].rgbBlue >> 3;
				green = lpInBMI->bmiColors[idx].rgbGreen >> 3;
				red  = lpInBMI->bmiColors[idx].rgbRed >> 3;
			} 
			else { // Must be 24 bit
				blue = *lpBits++ >> 3;
				green = *lpBits++ >> 3;
				red  = *lpBits++ >> 3;
			}
			lpHist[red<<10|green<<5|blue]++;
		}
	}

	// Set 'count' to number of non-zero entries
	for (idx = 0, i = 32768; i--; ) {
		if (lpHist[i]) 
			idx++;
	}
	*count = idx;
}

// Used by GenOptimizedPalette
static BOOL NEAR PASCAL InOrder(UCHAR Color,UINT Color1,UINT Color2)
{
	if (Color == 'R') return (RED(Color1) < RED(Color2));
	else if (Color == 'G') return (GREEN(Color1) < GREEN(Color2));
	else return (BLUE(Color1) < BLUE(Color2));
}

// Sort the Color List by the widestColor value
// Used by GenOptimizedPalette
static void NEAR PASCAL SortColors(USHORT FAR *lpColorList,UCHAR Color,
	UINT first,UINT last)
{
	UINT indx,jndx,temp;

	indx = first;

	if (indx < last) {
		// Check for already sorted list
		while (InOrder(Color,lpColorList[indx],lpColorList[indx+1]) && indx < last) {
			indx++;
		}
	}

	if (indx == last) return;

	jndx = last;
	temp = lpColorList[indx];

	while (indx != jndx) {

		while (indx != jndx && InOrder(Color,temp,lpColorList[jndx])) jndx--;

		if (indx != jndx) {
			lpColorList[indx] = lpColorList[jndx];
			indx++;

			while (indx != jndx && InOrder(Color,lpColorList[indx],temp)) indx++;

			if (indx != jndx) {
				lpColorList[jndx] = lpColorList[indx];
				jndx--;
			}
		}
	}
	lpColorList[indx] = temp;
	if (first < jndx) SortColors(lpColorList,Color,first,jndx-1);
	if (indx < last) SortColors(lpColorList,Color,indx+1,last);
}

// Used by GenOptimizedPalette
static void NEAR PASCAL CalcColor(USHORT FAR *lpHist,USHORT FAR *lpColorList,
	LPCOLORRECT lpCR)
{
	UINT ColorIdx,i;
	long red, green, blue;

	red = green = blue = 0;

	for (i = lpCR->first; i <= lpCR->last; i++) {
		ColorIdx = lpColorList[i];

		red += RED(ColorIdx) * (long)lpHist[ColorIdx];
		green += GREEN(ColorIdx) * (long)lpHist[ColorIdx];
		blue += BLUE(ColorIdx) * (long)lpHist[ColorIdx];
	}

	lpCR->red = (UCHAR)(red/lpCR->histSum);
	lpCR->green = (UCHAR)(green/lpCR->histSum);
	lpCR->blue = (UCHAR)(blue/lpCR->histSum);

	lpCR->red <<= 3;
	lpCR->blue <<= 3;
	lpCR->green <<= 3;
}

LPWORD PASCAL GenOptimizedPalette(LPBITMAPINFO lpbi,HPSTR lpInBits,UINT nColors)
{
	UINT i,count,nColorRects;
	UINT idx;
	LPCOLORRECT lpCR,lpCR2,lpPrev;
	long sum,median;
	USHORT FAR *lpHist,FAR *lpColorList;
	LPCOLORRECT lpColorRects;
	LPWORD lpLookup;
	LPRGBQUAD lpPal;

	// Create the color histogram
	// Allocate histogram array, 5 bits of precision for each color component,
	// 15 bits is 0-0x7FFF, or 32768 entries
	if(!(lpHist = (USHORT FAR *) GlobalAllocPtr(GHND,32768UL*sizeof(USHORT)) ))
		return (NULL);

	GenHistogram(lpbi,lpInBits,lpHist,&count);

	// Create the ColorList
	if (!(lpColorList = (USHORT FAR *) GlobalAllocPtr(GHND,(count+1)*sizeof(USHORT)))) {
		GlobalFreePtr(lpHist);
		return (NULL);
	}

	for (idx = i = 0; i <= 32767; i++) {
		if (lpHist[i]) lpColorList[idx++] = i;
	}

	// Create the first ColorRect
	lpCR = AllocCR();

	// Init this colorRect to the whole ColorList
	lpCR->first = 0;
	lpCR->last = count - 1;
	lpCR->next = NULL;

	nColorRects = 1;

	ProcColorRect(lpHist,lpColorList,lpCR);

	lpColorRects = lpCR;

	if (nColors > count) nColors = count;

	while (nColorRects < nColors) {
		// Get first ColorRect & remove it from the list
		lpPrev = lpCR = lpColorRects;

		// Don't try to split cubes with only one color
		while (lpCR && lpCR->first == lpCR->last) {
			lpPrev = lpCR;
			lpCR = lpCR->next;
		}

		// Must have a cube to split!
		assert(lpCR);

		if (lpCR == lpColorRects) 
			lpColorRects = lpCR->next;
		else 
			lpPrev->next = lpCR->next;

		// sort from colorRect.first to colorRect.last by colorRect.widestColor
		SortColors(lpColorList,lpCR->widestColor,lpCR->first,lpCR->last);

		//      find the median point
		sum = 0;        
		median = lpCR->histSum/2;
		for (i = lpCR->first; i < lpCR->last; i++) {
			sum += lpHist[lpColorList[i]];
			if (sum > median) break;   //Found the median
		}               

		// split the colorRect at the median into 2 colorRects

		// Create a 2nd colorRect and set it to the 2nd half of the colorList
		lpCR2 = AllocCR();

		if (i == lpCR->last) {
			lpCR2->first = i;
			lpCR2->last = lpCR->last;
			lpCR2->next = NULL;

			lpCR->last = i-1;
			lpCR->next = NULL;
		} 
		else {
			lpCR2->first = i+1;
			lpCR2->last = lpCR->last;
			lpCR2->next = NULL;

			lpCR->last = i;
			lpCR->next = NULL;
		}

		//      scan the colorList to define the mins, maxs and Histsun of the 1st ColorRect
		ProcColorRect(lpHist,lpColorList,lpCR);
	
		//      insert the colorRect based on its histsum
		InsertCR(&lpColorRects,lpCR);

		//      scan the colorList to define the mins, maxs and Histsun of the 2nd ColorRect
		ProcColorRect(lpHist,lpColorList,lpCR2); 

		//      insert the colorRect based on its histsum
		InsertCR(&lpColorRects,lpCR2);

		nColorRects++;
	 }

	// Calc the Optimized Palette
	for (lpCR = lpColorRects,i = nColorRects; i--; lpCR = lpCR->next) {
		CalcColor(lpHist,lpColorList,lpCR);
	}

	// Free the Histogram and colorList
	GlobalFreePtr( lpHist );
	GlobalFreePtr( lpColorList );

	// Allocate the color translation table and corresponding palette
	if (!(lpLookup = (LPWORD)GlobalAllocPtr(GHND,LOOKUPSIZE+NEWPALSIZE))) {
		return (NULL);
	}
	// Fill in just-allocated color translation table.
#ifdef WIN32
	_fmemset(lpLookup,0xff,LOOKUPSIZE);
#else
	// fill whole segment, assuming lpLookup's offset is 0.
	for (i = 32768; i--; ) lpLookup[i] = 0xffff;
#endif

	for (lpCR = lpColorRects, i = 0; i < nColorRects; i++, lpCR = lpCR->next) {
		UINT red,green,blue;

		for (red = lpCR->redMin; red <= lpCR->redMax; red++) {
			for (green = lpCR->greenMin; green <= lpCR->greenMax; green++) {
				for (blue = lpCR->blueMin; blue <= lpCR->blueMax; blue++) {
					lpLookup[red<<10|green<<5|blue] = (WORD)i;
				}
			}
		}
	}

	// Fill in the new palette at the end of the lookup table.
	lpCR = lpColorRects;
	lpPal = (LPRGBQUAD)(((HPSTR)lpLookup)+LOOKUPSIZE);

	for (i = nColorRects; i--; lpCR = lpCR->next, lpPal++) {
		lpPal->rgbRed = lpCR->red;
		lpPal->rgbGreen = lpCR->green;
		lpPal->rgbBlue = lpCR->blue;
}
	// Free the ColorsRects
	FreeCRs( lpColorRects );

	return (lpLookup);
}

void PASCAL RemapPixels(LPBITMAPINFO lpInBMI,HPSTR lpInBits,LPBITMAPINFO lpOutBMI,
	HPSTR lpOutBits, WORD FAR *lpLookup)
{
	int x,nWidth,nHeight;
	UCHAR red,green,blue;
	UINT idx, nIdx ;
	int nInBits,nOutBits;
	long lRowSize;
	int nInPad = 0;
	int nOutPad = 0;
	PCDSTUFF pStuff ;

	nWidth = (int) lpInBMI->bmiHeader.biWidth;
	nHeight = (int) lpInBMI->bmiHeader.biHeight;
	nInBits = lpInBMI->bmiHeader.biBitCount;
	nOutBits = lpOutBMI->bmiHeader.biBitCount;

	lRowSize = ROWSIZE((long)nWidth,nInBits);
	nInPad = (int)(lRowSize-((long)nWidth*nInBits+7)/8);

	lRowSize = ROWSIZE(lpOutBMI->bmiHeader.biWidth,nOutBits);
	nOutPad = (int)(lRowSize-(lpOutBMI->bmiHeader.biWidth*nOutBits+7)/8);

	if ( !(pStuff = InitTransPal((nOutBits == 4) ? 16 : 256, lpOutBMI->bmiColors )) )
	  return ;

	// Remap the pixels
	while (nHeight--) {
		for (x = 0; x < nWidth; x++) {
			if (nInBits == 4) {
				if (x&1) idx = *lpInBits++&0x0f;
				else idx = *lpInBits>>4;

				blue = lpInBMI->bmiColors[idx].rgbBlue;
				green = lpInBMI->bmiColors[idx].rgbGreen;
				red  = lpInBMI->bmiColors[idx].rgbRed;
			} 
			else if (nInBits == 8) {
				idx = *lpInBits++;
				blue = lpInBMI->bmiColors[idx].rgbBlue;
				green = lpInBMI->bmiColors[idx].rgbGreen;
				red  = lpInBMI->bmiColors[idx].rgbRed;
			} 
			else {
				blue = *lpInBits++;
				green = *lpInBits++;
				red  = *lpInBits++;
			}

			idx = ((red&0xf8)<<7)|((green&0xf8)<<2)|(blue>>3);

			if ((nIdx = lpLookup[idx]) == 0xffffU) {
				nIdx = Closest(red, green, blue, pStuff) ;
				lpLookup[idx] = nIdx ;
			}

			if (nOutBits == 4) {
				if (x&1) 
					*lpOutBits++ |= lpLookup[idx]&0x0f;
				else 
					*lpOutBits = (UCHAR)lpLookup[idx]<<4;
			} else 
				*lpOutBits++ = (UCHAR)lpLookup[idx]; // Must be 8 bit data
		}

		if (nOutBits == 4 && (nWidth&1)) 
			lpOutBits++;

		lpInBits += nInPad;
		lpOutBits += nOutPad;
	}
}

LPWORD PASCAL GenOctreePalette( LPBITMAPINFO lpbi, HPSTR lpInBits, UINT nColors )
{
	LONG i, j, lRowSize  ;
	HPSTR pbBits, pwBits ;
	DWORD rmask, gmask, bmask ;
	int rright, gright, bright ;
	int rleft,  gleft,  bleft  ;
	UINT uIdx ;
	BYTE r, g, b;
	WORD wColor;
	DWORD dwSize;
	PALETTEENTRY  FAR* palEntry = NULL ;
	NODE FAR * pReducibleNodes[9];
	NODE FAR * pTree = NULL ;
	long      nLeafCount = 0 ;
	LPWORD    lpLookup ;
	LPRGBQUAD lpRGB ;

	LONG lWidth     = lpbi->bmiHeader.biWidth    ;
	LONG lHeight    = lpbi->bmiHeader.biHeight   ;
	UINT nColorBits = 8 ;
	lRowSize = ROWSIZE( lWidth, lpbi->bmiHeader.biBitCount ) ;

	for ( i = 0 ; i <= (int) nColorBits ; i++ )
		pReducibleNodes[i] = NULL;
	dwSize = NEWPALSIZE ;
  
  switch ( lpbi->bmiHeader.biBitCount )
		{
		  case 16 :
			  if ( lpbi->bmiHeader.biCompression == BI_BITFIELDS)
					{
						rmask = lpInBits [0];
	          gmask = lpInBits [1];
		        bmask = lpInBits [2];
				  }
				else
					{
						rmask = 0x7C00;
	          gmask = 0x03E0;
		        bmask = 0x001F;
				  }
				rright = GetRightShiftCount (rmask);
				gright = GetRightShiftCount (gmask);
				bright = GetRightShiftCount (bmask);

				rleft = GetLeftShiftCount (rmask);
				gleft = GetLeftShiftCount (gmask);
				bleft = GetLeftShiftCount (bmask);

				for ( i = 0 ; i < lHeight; i++) 
					{
						pwBits = lpInBits ;
						for ( j = 0 ; j < lWidth; j++) {
								wColor = *pwBits++;
								b = (BYTE) (((wColor & (WORD) bmask) >> bright) << bleft);
								g = (BYTE) (((wColor & (WORD) gmask) >> gright) << gleft);
								r = (BYTE) (((wColor & (WORD) rmask) >> rright) << rleft);
								AddColor (&pTree, r, g, b, nColorBits, 0, &nLeafCount,
										pReducibleNodes);
								while (nLeafCount > nColors )
									ReduceTree (nColorBits, &nLeafCount, pReducibleNodes);
							}
						lpInBits += lRowSize ;
				}
				break;

		  case 24 :
				for (i = 0 ; i < lHeight; i++ )
					{
						pbBits = lpInBits ;
						for ( j = 0 ; j < lWidth; j++ ) 
							{
								b = *pbBits++;
								g = *pbBits++;
								r = *pbBits++;
								AddColor ( &pTree, r, g, b, nColorBits, 
								           0, &nLeafCount, pReducibleNodes ) ;
								while (nLeafCount > nColors )
									ReduceTree (nColorBits, &nLeafCount, pReducibleNodes);
							}
						lpInBits += lRowSize ;
					}
				break;

		  case 8 :		// from a 256 color palette image
				for (i = 0 ; i < lHeight; i++ )
					{
						pbBits = lpInBits ;
						for ( j = 0 ; j < lWidth; j++ ) 
							{
								uIdx = *pbBits++ ;
								b = lpbi->bmiColors[uIdx].rgbBlue  ;
								g = lpbi->bmiColors[uIdx].rgbGreen ;
								r = lpbi->bmiColors[uIdx].rgbRed   ;
								AddColor ( &pTree, r, g, b, nColorBits, 
								           0, &nLeafCount, pReducibleNodes ) ;
								while (nLeafCount > nColors )
									ReduceTree (nColorBits, &nLeafCount, pReducibleNodes);
							}
						lpInBits += lRowSize ;
					}
				break;

		  default: // DIB must be 16, 24, or 32-bit!
			  return NULL;
  }

  if ( nLeafCount > nColors )
		{ // Sanity check
      DeleteTree (&pTree);
      return NULL;
		}
  // Create a logical palette from the colors in the octree
  dwSize = ( nLeafCount ) * sizeof (PALETTEENTRY) ;
  palEntry = (PALETTEENTRY FAR*) GlobalAllocPtr( GHND, dwSize ) ;
	if ( palEntry == NULL )
		{
      DeleteTree (&pTree);
      return NULL;
		}
	UINT nIndex = 0 ;
  GetPaletteColors ( pTree, palEntry, &nIndex ) ;

	if ( !(lpLookup = (LPWORD)GlobalAllocPtr(GHND, LOOKUPSIZE + NEWPALSIZE )) )
		return NULL ;

#ifdef WIN32
	_fmemset(lpLookup,0xff,LOOKUPSIZE);
#else
	// fill whole segment, assuming lpLookup's offset is 0.
	for (i = 32768; i--; ) lpLookup[i] = 0xffff;
#endif

	LPPALETTEENTRY lpPal = palEntry ;
	lpRGB = (LPRGBQUAD)( ((HPSTR)lpLookup) + LOOKUPSIZE ) ;
	for ( UINT uxx = 0 ; uxx < nLeafCount ; uxx++, lpRGB++, lpPal++ )
		{
		  lpRGB->rgbRed   = lpPal->peRed   ;
			lpRGB->rgbGreen = lpPal->peGreen ;
			lpRGB->rgbBlue  = lpPal->peBlue  ;
			lpRGB->rgbReserved = 0 ;
		}
	
  GlobalFreePtr ( palEntry ) ;
  DeleteTree ( &pTree ) ;
  return lpLookup ;
}

void GetPaletteColors ( NODE FAR* pTree, PALETTEENTRY FAR* pPalEntries, UINT FAR* pIndex )
{
  int i;
  if (pTree->bIsLeaf) 
		{
      pPalEntries[*pIndex].peRed   = (BYTE) ((pTree->nRedSum) / (pTree->nPixelCount));
      pPalEntries[*pIndex].peGreen = (BYTE) ((pTree->nGreenSum) / (pTree->nPixelCount));
      pPalEntries[*pIndex].peBlue  = (BYTE) ((pTree->nBlueSum) / (pTree->nPixelCount));
      (*pIndex)++;
		}
  else
		{
      for ( i = 0 ; i < 8 ; i++ )
				{
          if ( pTree->pChild[i] != NULL )
            GetPaletteColors (pTree->pChild[i], pPalEntries, pIndex);
				}
		}
}

void AddColor (NODE FAR* FAR* ppNode, BYTE r, BYTE g, BYTE b, UINT nColorBits,
  UINT nLevel, long FAR* pLeafCount, NODE FAR* FAR* pReducibleNodes)
{
  int nIndex, shift;
  static BYTE mask[8] = { 0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02, 0x01 };

  // If the node doesn't exist, create it
  if ( *ppNode == NULL )
		*ppNode = CreateNode( nLevel, nColorBits, pLeafCount, pReducibleNodes ) ;

  // Update color information if it's a leaf node
  if ( (*ppNode)->bIsLeaf )
		{
      (*ppNode)->nPixelCount++ ;
      (*ppNode)->nRedSum   += r ;
      (*ppNode)->nGreenSum += g ;
      (*ppNode)->nBlueSum  += b ;
		}
  else		  // Recurse a level deeper if the node is not a leaf
		{
      shift = 7 - nLevel;
      nIndex = (((r & mask[nLevel]) >> shift) << 2) |
               (((g & mask[nLevel]) >> shift) << 1) |
                ((b & mask[nLevel]) >> shift) ;
      AddColor ( &((*ppNode)->pChild[nIndex]), r, g, b, nColorBits,
                 nLevel + 1, pLeafCount, pReducibleNodes ) ;
		}
}

NODE FAR * CreateNode ( UINT nLevel, UINT nColorBits, long FAR * pLeafCount,
                   NODE FAR * FAR * pReducibleNodes )
{
  NODE FAR * pNode ;
  if ( ! (pNode = (NODE FAR*) GlobalAllocPtr( GHND, sizeof (NODE))) )
      return NULL;

  pNode->bIsLeaf = ( nLevel == nColorBits ) ? TRUE : FALSE;
  if ( pNode->bIsLeaf )
		(*pLeafCount)++ ;
  else
		{ // Add the node to the reducible list for this level
      pNode->pNext = pReducibleNodes[nLevel];
      pReducibleNodes[nLevel] = pNode;
		}
  return pNode ;
}

void ReduceTree (UINT nColorBits, long FAR * pLeafCount, NODE FAR * FAR * pReducibleNodes)
{
  int i;
  NODE FAR * pNode;
  long nRedSum, nGreenSum, nBlueSum, nChildren;

  // Find the deepest level containing at least one reducible node
  for (i = nColorBits - 1 ; (i>0) && (pReducibleNodes[i] == NULL); i--);

  // Reduce the node most recently added to the list at level i
  pNode = pReducibleNodes[i];
  pReducibleNodes[i] = pNode->pNext;

  nRedSum = nGreenSum = nBlueSum = nChildren = 0;
  for ( i = 0 ; i < 8 ; i++ )
		{
      if ( pNode->pChild[i] != NULL )
				{
          nRedSum   += pNode->pChild[i]->nRedSum   ;
          nGreenSum += pNode->pChild[i]->nGreenSum ;
          nBlueSum  += pNode->pChild[i]->nBlueSum  ;
          pNode->nPixelCount += pNode->pChild[i]->nPixelCount;

          GlobalFreePtr ( pNode->pChild[i] ) ;

          pNode->pChild[i] = NULL;
          nChildren++;
				}
		}
  pNode->bIsLeaf   = TRUE ;
  pNode->nRedSum   = nRedSum   ;
  pNode->nGreenSum = nGreenSum ;
  pNode->nBlueSum  = nBlueSum  ;
  *pLeafCount -= (nChildren - 1) ;
}

void DeleteTree ( NODE FAR * FAR * ppNode )
{
  int i;
  for ( i = 0 ; i < 8 ; i++ )
		{
			if ( (*ppNode)->pChild[i] != NULL )
				DeleteTree ( &((*ppNode)->pChild[i]) ) ;
		}
  GlobalFreePtr ( *ppNode ) ;
  *ppNode = NULL;
}

int GetRightShiftCount (DWORD dwVal)
{
  int i;
  for ( i = 0 ; i < sizeof (DWORD) * 8; i++ )
		{
      if (dwVal & 1)
          return i;
      dwVal >>= 1;
		}
  return -1;
}

int GetLeftShiftCount (DWORD dwVal)
{
  int i, nCount = 0 ;
  for ( i = 0 ; i < sizeof (DWORD) * 8; i++ )
		{
      if (dwVal & 1)
          nCount++;
      dwVal >>= 1;
		}
  return (8 - nCount);
}

#if DEBUG
static void NEAR PASCAL DumpHistogram(USHORT FAR * lpHist,UINT count)
{
	UINT i;
	char buf[200];

	for (i = 0; i < count; i++) {
		wsprintf(buf, "\n\r#%05u = %u", i, lpHist[i]);
		OutputDebugString(buf);
	}
}

static void NEAR PASCAL DumpCR(LPCOLORRECT lpCR)
{

//	DebugOutput( DBF_TRACE | DBF_APPLICATION, "first = %u, last = %u", lpCR->first, lpCR->last  );
//
//	DebugOutput( DBF_TRACE | DBF_APPLICATION, "Hist = %ld", lpCR->histSum  );
//
//	DebugOutput(DBF_TRACE | DBF_APPLICATION, "red Min = %d, Max = %d", lpCR->redMin, lpCR->redMax  );
//
//	DebugOutput(DBF_TRACE | DBF_APPLICATION, "green Min = %d, Max = %d", lpCR->greenMin, lpCR->greenMax  );
//
//	DebugOutput(DBF_TRACE | DBF_APPLICATION, "blue Min = %d, Max = %d\r\n-----", lpCR->blueMin, lpCR->blueMax  );
	UNREFERENCED_PARAMETER(lpCR);
}

#if 0
static void NEAR WalkCRList(void)
{
	LPCOLORRECT lpCR;

	lpCR = lpColorRects;

	while (lpCR) {
		DumpCR(lpCR);
		lpCR = lpCR->next;
	}
}
#endif
#endif /* #if DEBUG */

#if 0
// Used only by module color.c
// CAR: Seems rather silly to keep this global... I'm gonna inline it...
void PASCAL InitDibHeader(LPBITMAPINFOHEADER lpOutBMI,UINT nColors,LPRGBQUAD lpPal)
{
	if (nColors <= 16) {
		MEMCPY((LPBITMAPINFO)lpOutBMI->bmiColors,lpPal,sizeof(RGBQUAD)*16);
	} 
	else {
		MEMCPY((LPBITMAPINFO)lpOutBMI->bmiColors,lpPal,sizeof(RGBQUAD)*256);
	}
}
#endif


