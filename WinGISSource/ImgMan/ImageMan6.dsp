# Microsoft Developer Studio Project File - Name="ImageMan6" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** NICHT BEARBEITEN **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=ImageMan6 - Win32 Release
!MESSAGE Dies ist kein g�ltiges Makefile. Zum Erstellen dieses Projekts mit NMAKE
!MESSAGE verwenden Sie den Befehl "Makefile exportieren" und f�hren Sie den Befehl
!MESSAGE 
!MESSAGE NMAKE /f "ImageMan6.mak".
!MESSAGE 
!MESSAGE Sie k�nnen beim Ausf�hren von NMAKE eine Konfiguration angeben
!MESSAGE durch Definieren des Makros CFG in der Befehlszeile. Zum Beispiel:
!MESSAGE 
!MESSAGE NMAKE /f "ImageMan6.mak" CFG="ImageMan6 - Win32 Release"
!MESSAGE 
!MESSAGE F�r die Konfiguration stehen zur Auswahl:
!MESSAGE 
!MESSAGE "ImageMan6 - Win32 Release" (basierend auf  "Win32 (x86) Dynamic-Link Library")
!MESSAGE "ImageMan6 - Win32 Debug" (basierend auf  "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""$/Tools/zlib", MFBAAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "ImageMan6 - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir ".\Release"
# PROP BASE Intermediate_Dir ".\Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir ".\Release"
# PROP Intermediate_Dir ".\Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /c
# ADD CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "WINGIS" /YX /J /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /fo".\imgman.res" /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /dll /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib SKEYLI32.LIB MPR.LIB NETAPI32.LIB RPCRT4.LIB /nologo /subsystem:windows /dll /machine:I386 /nodefaultlib:"Libc.lib" /out:"c:\wingis\imgman32.dll"

!ELSEIF  "$(CFG)" == "ImageMan6 - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir ".\Debug"
# PROP BASE Intermediate_Dir ".\Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir ".\Debug"
# PROP Intermediate_Dir ".\Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /YX /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "WINGIS" /YX /J /FD /c
# ADD BASE MTL /nologo /D "_DEBUG" /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /fo".\imgman.res" /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /dll /debug /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib SKEYLI32.LIB MPR.LIB NETAPI32.LIB RPCRT4.LIB /nologo /subsystem:windows /dll /debug /machine:I386 /nodefaultlib:"Libc.lib" /out:"c:\wingis\Imgman32.dll"
# SUBTRACT LINK32 /pdb:none

!ENDIF 

# Begin Target

# Name "ImageMan6 - Win32 Release"
# Name "ImageMan6 - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;hpj;bat;for;f90"
# Begin Source File

SOURCE=.\CDialogs.cpp
# End Source File
# Begin Source File

SOURCE=.\COLOR.cpp
# End Source File
# Begin Source File

SOURCE=.\COLOR2.cpp
# End Source File
# Begin Source File

SOURCE=.\contrast.cpp
# End Source File
# Begin Source File

SOURCE=.\Dib.cpp
# End Source File
# Begin Source File

SOURCE=.\DIBFUNCS.cpp
# End Source File
# Begin Source File

SOURCE=.\EFFECTS.cpp
# End Source File
# Begin Source File

SOURCE=.\filter.cpp
# End Source File
# Begin Source File

SOURCE=.\fxblinds.cpp
# End Source File
# Begin Source File

SOURCE=.\fxblur.cpp
# End Source File
# Begin Source File

SOURCE=.\fxcurtn.cpp
# End Source File
# Begin Source File

SOURCE=.\fxmosaic.cpp
# End Source File
# Begin Source File

SOURCE=.\fxshape.cpp
# End Source File
# Begin Source File

SOURCE=.\fxwipe.cpp
# End Source File
# Begin Source File

SOURCE=.\HostInt.cpp
# End Source File
# Begin Source File

SOURCE=.\Imgfx.cpp
# End Source File
# Begin Source File

SOURCE=.\Imgio.c
# End Source File
# Begin Source File

SOURCE=.\imgman.cpp
# End Source File
# Begin Source File

SOURCE=.\Imgman.rc
# End Source File
# Begin Source File

SOURCE=.\Imgman32.def
# End Source File
# Begin Source File

SOURCE=.\Imgx.cpp
# End Source File
# Begin Source File

SOURCE=.\nag.cpp
# End Source File
# Begin Source File

SOURCE=.\OPTBLK.cpp
# End Source File
# Begin Source File

SOURCE=.\PPP.cpp
# End Source File
# Begin Source File

SOURCE=.\Twain.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl;fi;fd"
# Begin Source File

SOURCE=.\Color.h
# End Source File
# Begin Source File

SOURCE=.\Color2.h
# End Source File
# Begin Source File

SOURCE=.\dib.h
# End Source File
# Begin Source File

SOURCE=.\Dibfuncs.h
# End Source File
# Begin Source File

SOURCE=.\Imgio.h
# End Source File
# Begin Source File

SOURCE=.\Imgman.h
# End Source File
# Begin Source File

SOURCE=.\imgx.h
# End Source File
# Begin Source File

SOURCE=.\Imtwain.h
# End Source File
# Begin Source File

SOURCE=.\Internal.h
# End Source File
# Begin Source File

SOURCE=.\Nag.h
# End Source File
# Begin Source File

SOURCE=.\Optblk.h
# End Source File
# Begin Source File

SOURCE=.\PPP.h
# End Source File
# Begin Source File

SOURCE=.\Twain.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;cnt;rtf;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\ICON1.ICO
# End Source File
# End Group
# Begin Source File

SOURCE=.\16Lkup.dat
# End Source File
# Begin Source File

SOURCE=.\256lkup.dat
# End Source File
# Begin Source File

SOURCE=.\Img2dat.rc
# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1
# End Source File
# End Target
# End Project
