/////////////////////////////////////////////////////////////////////////////
//
// xdcx.h
//
// ImageMan/X DCX export library include file
//
// Copyright (c) 1991, 1992 Data Techniques, Inc.
// All Rights Reserved
//
// ///////////////////////////////////////////////////////////////////////////
// $History: XDCX.H $
// 
// *****************  Version 5  *****************
// User: Johnd        Date: 3/20/98    Time: 8:45a
// Updated in $/ImageMan 16-32/DELS
// Updated Version Info to use new scheme
// 
// *****************  Version 4  *****************
// User: Johnd        Date: 2/28/96    Time: 2:05p
// Updated in $/ImageMan 16-32/DELS
// Updated Version Info to 5.00
// 
// *****************  Version 3  *****************
// User: Timk         Date: 7/25/95    Time: 10:53p
// Updated in $/ImageMan 16-32/DELS
// Added support for writing using IO pointer blocks.
// 
// *****************  Version 2  *****************
// User: Timk         Date: 5/19/95    Time: 1:04a
// Updated in $/ImageMan 16-32/DELS
// Added version resource information
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 1/18/95    Time: 4:45p
// Created in $/ImageMan 16-32/DELS
// ImageMan 4.0 Beta 1

#define MAXPATH	256
#define OUTBUFSIZE 2048

typedef PCXHEADER pcxhead;
typedef PCXHEADER FAR *LPPCXHEAD;

typedef struct _EXPORTINFO {
	char        szfName[MAXPATH];// name of file to export
	LPVOID		lpHandle;
	LPIMG_IO		lpIO;
	pcxhead     pcx;				  // pcx header info
	PCXPAL      pal[256];		  // Pallette info
	int         nColors;			  // color info
	HANDLE      hScratch;
	DCXHEADER   dcxHeader;		  // Fixed size DCX headerinfo.
	char        gacbOutBuff[OUTBUFSIZE];
	UINT        gwOutCnt;
}           EXPORTINFO, FAR * LPEXPORTINFO, *PEXPORTINFO;

#define EXP_NOOPEN		1		  /* unable to open file */
#define EXP_WRITE_HEAD	2		  /* unable to write header */
#define EXP_WRITE		3			  /* error during writing of data */

int NEAR PASCAL round(double);

int         WriteRow(LPEXPORTINFO lpex, int nByteLine, HPSTR lpBits);
int         EncodePCXRun(LPEXPORTINFO lpex, char nData, char nRepeat);
int         FillPCXHead(LPPCXHEAD pPCX, LPBITMAPINFO lpDIB, HANDLE hOptBlk);
void        set_error(int nIndex, LPCSTR lpFilename);
void        FillDCXHeader(LPDCXHEADER dcxHeader, int nPageNo, DWORD dwOffset);

#define DLL_FILEDESCRIPTION        "ImageMan DCX Writer\0"
#define DLL_ORIGINALFILENAME       "XDCX.DEL\0"
