/////////////////////////////////////////////////////////////////////////////
//
// xpng.h
//
// ImageMan/X PNG export library
//
// Copyright (c) 1991-5 Data Techniques, Inc.
// All Rights Reserved
//
// ///////////////////////////////////////////////////////////////////////////
// $History: xpng.h $
// 
// *****************  Version 2  *****************
// User: Johnd        Date: 3/20/98    Time: 8:45a
// Updated in $/ImageMan 16-32/DELS
// Updated Version Info to use new scheme
// 
// *****************  Version 1  *****************
// User: Timk         Date: 11/26/96   Time: 7:42a
// Created in $/imageman 16-32/dels
// Initial Checking of PNG Export Code
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 1/18/95    Time: 4:34p
// Created in $/ImageMan 16-32/DELS
// ImageMan 4.0 Beta 1

#define EXP_NOOPEN		1		  // unable to open file
#define EXP_WRITE_HEAD	2		  // unable to write header
#define EXP_WRITE		3		  // error during writing of data

#define DLL_FILEDESCRIPTION        "ImageMan PNG Writer\0"
#define DLL_ORIGINALFILENAME       "PNG.DEL\0"
#define TEXT_KEY                   "Software\0"
#define TEXT_COMMENT               "Written by ImageMan from Data Techniques, Inc.\0"
