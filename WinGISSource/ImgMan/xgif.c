/////////////////////////////////////////////////////////////////////////////
//
// xgif.c
//
// ImageMan GIF DEL Code
//
// Copyright (c) 1991, 1997 Data Techniques, Inc.
// All Rights Reserved
//
// ///////////////////////////////////////////////////////////////////////////
// $History: XGIF.C $
// 
// *****************  Version 10  *****************
// User: Erict        Date: 6/29/00    Time: 1:28p
// Updated in $/ImageMan 16-32/DELS
// removed ptr to OptBlk. Makefile now links with optblk.obj so call
// doesn't have to be made thru ptr.
// 
// *****************  Version 9  *****************
// User: Ericj        Date: 4/29/98    Time: 11:03a
// Updated in $/ImageMan 16-32/DELS
// Changed fname in LEXPORTINFO to MAX_PATH to export files
// longer than 100 characters.
// 
// *****************  Version 8  *****************
// User: Ericw        Date: 3/19/98    Time: 12:02p
// Updated in $/ImageMan 16-32/DELS
// Now write an Application Extension Block giving LOOP information to
// those who need it - like IE, Netscape and GIFCon.
// 
// *****************  Version 7  *****************
// User: Ericw        Date: 3/19/98    Time: 9:52a
// Updated in $/ImageMan 16-32/DELS
// We now write the Local Color Table.  This was causing palette problems
// for multiple frames with images that used different palettes.
// 
// *****************  Version 6  *****************
// User: Ericw        Date: 3/17/98    Time: 2:58p
// Updated in $/ImageMan 16-32/DELS
// Wasn't settting both color depth values in the Global Descriptor in the
// header.
// 
// *****************  Version 5  *****************
// User: Ericw        Date: 11/26/97   Time: 9:39a
// Updated in $/ImageMan 16-32/DELS
// Implemented animated GIF support as multi-page files just the
// multi-page tifs and set the version numbers to 6.0.
// 
// *****************  Version 4  *****************
// User: Timk         Date: 10/30/96   Time: 11:39p
// Updated in $/ImageMan 16-32/DELS
// Added support for writing interlaced GIFs.
// 
// *****************  Version 3  *****************
// User: Timk         Date: 1/29/96    Time: 12:49a
// Updated in $/ImageMan 16-32/DELS
// Fixed bug which wasn't writing last LZW code to file.
// 
// *****************  Version 2  *****************
// User: Timk         Date: 7/25/95    Time: 10:54p
// Updated in $/ImageMan 16-32/DELS
// Added support for writing using IO pointer blocks.
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 1/18/95    Time: 6:17p
// Created in $/ImageMan 16-32/DELS
// ImageMan 4.0 Beta 1
//

#define WIN32_LEAN_AND_MEAN

#define _WINDLL
#include <windows.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <assert.h>
#include "internal.h"
#include "xgif.h"
#include "lzwx.h"
#include "optblk.h"

#pragma pack(4)

#ifndef WIN32
#include "windos.h"
#endif

#define COMPRESSBUFSIZE 8192

#ifdef WIN32
#define SELECTOROF(x) (x)
#endif

#ifdef WIN32


#ifdef WINGIS // so the file TLSByJHC.h was included to "internal.h"

#ifdef NDEBUG
#define SETERROR(E) bJHCTLSSetValue((DWORD)(E))
#else
static void IM2PCXerr(int);
#define SETERROR(E) { \
	 SETERROR(E) bJHCTLSSetValue((DWORD)(E)); \
	if ((E) != IMG_OK) IM2PCXerr(E); \
	}
#endif
#define GETERROR    nJHCTLSGetValue()
#else

static DWORD TlsIndex;			  // Thread local storage index
#ifdef NDEBUG
#define SETERROR(E) TlsSetValue(TlsIndex,(LPVOID)(DWORD)(E))
#else
static void IM2PCXerr(int);
#define SETERROR(E) { \
	TlsSetValue(TlsIndex,(LPVOID)(DWORD)(E)); \
	if ((E) != IMG_OK) IM2PCXerr(E); \
	}
#endif
#define GETERROR    ((DWORD)TlsGetValue(TlsIndex))

#endif



#else
static int  nErrID;				  // id of latest error
#define SETERROR(E) nErrID = (E)
#define GETERROR    (nErrID)
#endif

typedef struct _EXPORTINFO {
	char        fName[MAX_PATH];		  // name of file to export
	LPVOID		lpHandle;
	LPIMG_IO		lpIO;
	int         nImgWidth;
	int         nImgHeight;
	BOOL				bInterlaced;		//TRUE if writing interlaced image
	LPSTRINGTABLE lpTable;		  // LZW string table
	int         nResetSize;		  // starting code size
	int         nCurCode;
	int         nCurPre;			  // current prefix - for LZW compression
	int         nLZWbits;		  // current # of bits in output codes
	int         nBitChange;		  // max code to allocate w/current code size
	int         nAvail;			  // # of bits available for output in output byte
	LPSTR       lpDecodeBuf;
	int         nCompUsed;
	int         nOutRowSize;
	LPSTR       lpInBuf;			  // used to expand input for <8 bits/pixel
}           EXPORTINFO, FAR * LPEXPORTINFO;

static WORD wStartRowOffset[4] = {0, 4, 2, 1};	//initial row offset for current pass on interlaced images
static WORD wRowSkip[4] = {8, 8, 4, 2};	//# of rows to skip for current pass on interlaced images

int         LZWpack(LPSTR, HPSTR, int, LPEXPORTINFO);
int         WriteCode(LPSTR lpOut, int nCode, LPEXPORTINFO lpex);
int         ClearLZW(LPEXPORTINFO lpex, LPSTR lpOut);
void        set_error(int, LPCSTR);
XERRORPROC  XSetError;

HANDLE      hInst;

DEBUGPROC   DebugStringOut;
unsigned    CLEARCODE;
unsigned    EOICODE;
//LPFNOPTBLKGET lpfnOptBlkGet;

#ifdef WIN32
// /////////////////////////////////////////////////////////////////////////
//
// DllMain
//
// This is the Win32 'DllEntryPoint'. It is similar in function to LibMain
// and the WEP in 16-bit land, but is much more thorough. LibMain is
// called whenever a process or thread attaches or detaches to the
// BMP DIL
//
// Entry:
// hinstDLL  - handle to the ImageMan DLL
// fdwReason - notification code (attach/detach)
// DLL_PROCESS_ATTACH
// DLL_PROCESS_DETACH
// DLL_THREAD_ATTACH
// DLL_THREAD_DETACH
// lpvReserved - NULL during dynamic process attach/detach, non-NULL
// during static attach/detach.
// Exit:
// DLL_PROCESS_ATTACH - TRUE for success, FALSE for failure
// all others         - return code ignored
// /////////////////////////////////////////////////////////////////////////

BOOL APIENTRY DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
	switch (fdwReason) {
		// The DLL is attaching to a process, due to LoadLibrary.
		case DLL_PROCESS_ATTACH:
			assert(!lpvReserved);  // must always be loaded via LoadLibrary!

			hInst = hinstDLL;		  // save our identity
#ifndef WINGIS
			// Allocate a "Thread Local Storage" (TLS) index right away.

			if ((TlsIndex = TlsAlloc()) == 0xFFFFFFFF)
				return (FALSE);
#else
			bJHCTLSClear();
#endif
			// Fall thru: Initialize the index for first thread.

		// The attached process created a new thread.
		case DLL_THREAD_ATTACH:
#ifndef WINGIS
			// Initialize the TLS index for this thread... it's actually 'nErrID'
			if (!TlsSetValue(TlsIndex, (LPVOID) 0))
				return (FALSE);
#else
			nJHCTLSAlloc(); // Stamp this thread to be existed
#endif
			break;

		// The thread of the attached process terminates.
		case DLL_THREAD_DETACH:
#ifdef WINGIS
			bJHCTLSFree(); // Free this thread entry
#endif
			break;

		// The DLL is detaching from a process, due to a call to FreeLibrary.
		case DLL_PROCESS_DETACH:
#ifndef WINGIS
			// Release the TLS index.
			TlsFree(TlsIndex);
#else
			bJHCTLSClear();  // Release all the threads error descriptors
#endif
			break;
	}

	return (TRUE);

	UNREFERENCED_PARAMETER(lpvReserved);
}

#else // WIN32
int WINAPI LibMain(hInstance,wDataSeg,wHeapSize,lpCmdLine)
HANDLE      hInstance;
WORD        wDataSeg, wHeapSize;
LPSTR       lpCmdLine;
{

	if (wHeapSize == 0)			  /* no heap indicates fatal error */
		return 0;

	hInst = hInstance;			  // save instance handle

#if DEBUG
	{
		HANDLE      hMod = GetModuleHandle("IMGMAN2.DLL");
		if (hMod)
			DebugStringOut = (DEBUGPROC) GetProcAddress(hMod, "DebugStringOut");
	}
#endif

	return 1;
}
#endif

VOID WINAPI WEP(int bSystemExit)
{
	if (bSystemExit) {			  // if exiting Windows entirely

	}
}

LPSTR IMAPI IMXDLget_ext(LPXINITSTRUCT lpInit)
{
	lpInit->lFlags = XF_MULTIPAGE | XF_MONOCHROME | XF_4BIT | XF_8BIT | XF_PALETTE;
	XSetError = lpInit->SetError;

//	if (lpInit->hImgManInst)
//#ifdef WIN32
//		lpfnOptBlkGet = (LPFNOPTBLKGET) GetProcAddress(lpInit->hImgManInst, "ImgXOptBlkGet");
//#else
//		lpfnOptBlkGet = (LPFNOPTBLKGET) GetAnyProcAddress(lpInit->hImgManInst, "ImgXOptBlkGet");
//#endif

	return (LPSTR) "GIF;CompuServe GIF (*.GIF)";
}

LPSTR IMAPI IMXDLget_ver(void)
{
	return (LPSTR) "1.00";
}

HANDLE IMAPI IMXDLbegin_write(LPVOID lpOpenInfo, LPIMG_IO lpIO, LPCSTR lpszAlias, LPBITMAPINFO lpDIB, HANDLE hOptBlk, LPXINFO lpInfo)
{
	long        lfaEnd ;
	LPVOID      lpHandle;
	HANDLE      hImage = NULL;
	LPEXPORTINFO lpex;
	GIFHEAD     gifHead;
	LOCALHEAD   localHead;
	RGBTRIPLE   pal[256];		  // up to 256 palette entries
	char				optbuf[100];
	int         nColors = (1 << lpDIB->bmiHeader.biBitCount);
	int         i;
	BOOL				bAppend = FALSE ;
	BYTE        bCodeSize;
	GCEXTENSION gcExtension ;
	AEBLOCK     aeBlock ;

	// verify that we're not tryin' to write a 24-bit image
	if (lpDIB->bmiHeader.biBitCount > 8) {
		set_error(EXP_BAD_COLORS, lpszAlias);
		return NULL;
	}

	if ( lpIO->open(lpOpenInfo, OF_EXIST, &lpHandle) == IMG_OK ) {
		bAppend = TRUE ;
		lpIO->open(lpOpenInfo, OF_READWRITE, &lpHandle);
	}

	if ( ! bAppend ) {
		if ( lpIO->open(lpOpenInfo, OF_CREATE, &lpHandle) != IMG_OK) {
			set_error(EXP_NOOPEN, lpszAlias);
			return NULL ;
		}
	}

	// now create and initialize our data structure
	hImage = GlobalAlloc(GHND, sizeof(EXPORTINFO));
	if ( ! hImage)
		{
			lpIO->close(lpHandle);
			return NULL ;
		}
	lpex = (LPEXPORTINFO) GlobalLock(hImage);
	lpex->lpHandle = lpHandle;
	lpex->lpIO = lpIO;
	_fstrcpy(lpex->fName, lpszAlias);

	// we'll set the flags regardless of append because ImgDescptor
	// uses this value since it was already calculated
	gifHead.fGlobal = 0x80 + (BYTE) (lpDIB->bmiHeader.biBitCount - 1) ;
	gifHead.fGlobal |= (BYTE) ((lpDIB->bmiHeader.biBitCount - 1)<<4) ;
	if ( ! bAppend )
		{
			_fstrcpy(gifHead.signature, "GIF89a");
			gifHead.screen_width  = (WORD)lpDIB->bmiHeader.biWidth ;
			gifHead.screen_height = (WORD)lpDIB->bmiHeader.biHeight ;
			gifHead.nBkrnd = 0;
			gifHead.zero = 0;

			// write out file header
			if (lpIO->write(lpHandle, (LPSTR) & gifHead, sizeof(GIFHEAD)) != sizeof(GIFHEAD))
				set_error(EXP_WRITE_HEAD, lpszAlias);

			//
			// now write out the palette - NOTE that the GIF palette is written
			// in RGB order, whereas the RGBTRIPLE is in BGR order, SO...
			// we swap the blue and red entries when we setup the palette.
			//
			// rgbtBlue is really the RED component
			// rgbtRed is really the BLUE component
			//
			// So what if it's weird? It works just fine!
			//
			for (i = 0; i < nColors; i++) {
				pal[i].rgbtBlue = lpDIB->bmiColors[i].rgbRed;
				pal[i].rgbtGreen = lpDIB->bmiColors[i].rgbGreen;
				pal[i].rgbtRed = lpDIB->bmiColors[i].rgbBlue;
			}
			if (lpIO->write(lpHandle, (LPSTR) pal, sizeof(RGBTRIPLE) * nColors) != sizeof(RGBTRIPLE) * nColors)
				set_error(EXP_WRITE_HEAD, lpszAlias);

			// now write Application Extension Block with animating LOOP info.
			aeBlock.Introducer  = 0x21 ;
			aeBlock.AELabel     = 0xFF ;
			aeBlock.nBlockLen   = 0x0B ;
			_fstrcpy ( aeBlock.szNScape, "NETSCAPE2.0" ) ;
			aeBlock.nDataBLen   = 0x03 ;
			aeBlock.nLoopCount  = 0x01 ;
			aeBlock.nIterations = 1000 ;
			aeBlock.term        = 0    ;

			if (lpIO->write(lpHandle, (LPSTR) & aeBlock, sizeof(AEBLOCK)) != sizeof(AEBLOCK))
				set_error(EXP_WRITE_HEAD, lpszAlias);
		}
	else
		{
			//  jump to the end of the file for appending
			lfaEnd = lpIO->seek ( lpHandle, 0, SEEK_END ) ;
			//  ... then back up 1 so we overwrite the preivous End Of Stream marker.
			//  Without doing this IE and GIFCon would only see the first image.
			lpIO->seek ( lpHandle, lfaEnd-1, SEEK_SET ) ;
		}

	// now fill in and write a graphic control extension
	gcExtension.Introducer = 0x21 ;
	gcExtension.GCLabel    = 0xF9 ;
	gcExtension.Size       = 0x04 ;
	gcExtension.Flags      = 0    ;
  gcExtension.Delay      = 100  ;  // hardcoded 10 miliseconds for now
	gcExtension.TranspIdx  = 0    ;
	gcExtension.Term       = 0    ;

	// write out file graphic control extension block
	if (lpIO->write(lpHandle, (LPSTR) & gcExtension, sizeof(GCEXTENSION)) != sizeof(GCEXTENSION))
		set_error(EXP_WRITE_HEAD, lpszAlias);

	// now fill-in and write Local Header
	localHead.punctuation = ',';
	localHead.image_left = localHead.image_top = 0;
	localHead.image_wide = (WORD) (lpDIB->bmiHeader.biWidth);
	localHead.image_deep = (WORD) (lpDIB->bmiHeader.biHeight);
	localHead.fLocal = ( gifHead.fGlobal & 0x87 ) ;

	// check to see if they want interlaced write or normal write
	if (!(OptBlkGet(hOptBlk, "GIF_INTERLACE", (LPSTR) optbuf)) && !_fstrcmp((LPSTR) optbuf, "ON"))
		lpex->bInterlaced = TRUE;
	else
		lpex->bInterlaced = FALSE;
	if ( lpex->bInterlaced )
		localHead.fLocal |= 0x40 ;

	if (lpIO->write(lpHandle, (LPSTR) & localHead, sizeof(localHead)) != sizeof(localHead)) {
		set_error(EXP_WRITE_HEAD, lpszAlias);
	}

	// now write the local color table
	for (i = 0; i < nColors; i++) {
		pal[i].rgbtBlue = lpDIB->bmiColors[i].rgbRed;
		pal[i].rgbtGreen = lpDIB->bmiColors[i].rgbGreen;
		pal[i].rgbtRed = lpDIB->bmiColors[i].rgbBlue;
	}
	if (lpIO->write(lpHandle, (LPSTR) pal, sizeof(RGBTRIPLE) * nColors) != sizeof(RGBTRIPLE) * nColors)
		set_error(EXP_WRITE_HEAD, lpszAlias);

	// write the initial LZW code size
	bCodeSize = (BYTE) (lpDIB->bmiHeader.biBitCount);
	if (bCodeSize == 1)
		bCodeSize = 2;
	lpIO->write(lpHandle, (LPSTR) (&bCodeSize), 1);

	// setup our LZW encoding variables
	lpex->nResetSize = bCodeSize + 1;
	lpex->nCurPre = -1;
	lpex->nLZWbits = bCodeSize + 1;
	lpex->nBitChange = 1 << lpex->nLZWbits;
	lpex->nAvail = 8;
	lpex->lpTable = NULL;
	lpex->lpDecodeBuf = GlobalAllocPtr(GHND, COMPRESSBUFSIZE);
	CLEARCODE = 1 << bCodeSize;
	EOICODE = CLEARCODE + 1;

	lpex->nCompUsed = ClearLZW(lpex, lpex->lpDecodeBuf);
	lpex->nOutRowSize = (int) (lpDIB->bmiHeader.biWidth);
	if (bCodeSize == 2)
		lpex->nOutRowSize;

	if (bCodeSize == 8)
		lpex->lpInBuf = NULL;
	else
		lpex->lpInBuf = (LPSTR) GlobalAllocPtr(GHND, lpex->nOutRowSize);
	lpex->nImgWidth = lpex->nOutRowSize;
	lpex->nImgHeight = (int) (lpDIB->bmiHeader.biHeight);

	GlobalUnlock ( hImage ) ;

	lpInfo->lFlags = XF_MONOCHROME | XF_4BIT | XF_8BIT | XF_PALETTE;

	return hImage;
}

//
// This function compresses and writes one row to output
//
int write_row_data(LPEXPORTINFO lpex, HPSTR hpRow, int nRowSize, LPXINFO lpInfo)
{
	BYTE        outbuf[256];
	int         retval = IMGX_OK;

	//
	// Flush our compression buffer to disk if needed.
	// We do this if we're starting a new strip or if the buffer won't
	// hold 2 lines of umcompressed image information - 2 lines is used
	// because a worst-case image might cause us to compress to slightly
	// larger than we started, so we need a little extra buffer.
	//

	if ((lpex->nOutRowSize * 2) > (COMPRESSBUFSIZE - lpex->nCompUsed) && lpex->nCompUsed) {
		int         nCopy;
		LPSTR       lpTmp2 = lpex->lpDecodeBuf;
		BYTE        bTemp = lpex->lpDecodeBuf[lpex->nCompUsed];	// save the last byte - we 're still workin' on it

		//
		// we have to write in 256-byte blocks
		//

		lpTmp2 = lpex->lpDecodeBuf;

		while (lpex->nCompUsed) {
			nCopy = min(255, lpex->nCompUsed);
			outbuf[0] = nCopy;
			_fmemcpy(&outbuf[1], lpTmp2, nCopy);
			lpTmp2 += nCopy;
			lpex->nCompUsed -= nCopy;
			lpex->lpIO->write(lpex->lpHandle, outbuf, nCopy + 1);
		}
		*lpex->lpDecodeBuf = bTemp;
	}
	//
	// deal with our progress indicator...
	//

	lpInfo->lIncCnt += nRowSize;
	if (lpInfo->lIncCnt > lpInfo->lIncrement) {
		lpInfo->lProcessed += lpInfo->lIncCnt;
		if (lpInfo->lpfnStat)
			if( !((*lpInfo->lpfnStat) (0, (int) ((lpInfo->lProcessed * 100) / lpInfo->lTotalBytes), lpInfo->dwUserInfo) )) {
				return IMGX_CANCELLED;
			}

		lpInfo->lIncCnt = 0;
	}
	// encode a row into our temp buf
	if (!lpex->lpInBuf) {	  // if it's 8 bits/pixel

		lpex->nCompUsed += LZWpack(lpex->lpDecodeBuf + lpex->nCompUsed, hpRow, lpex->nOutRowSize, lpex);
	} 
	else {
		HPSTR       lpTmp = lpex->lpInBuf;
		HPSTR       hpTmp = (HPSTR) hpRow;

		int         k;

		// expand our input into lpex->lpInBuf, 1 byte/pixel
		if (lpex->nResetSize == 5) {	// 4 bits/pixel

			for (k = 0; (k < (lpex->nImgWidth + 1) / 2); k++) {
				*lpTmp++ = (*hpTmp & 0xf0) >> 4;
				*lpTmp++ = *hpTmp++ & 0x0f;
			}
		} else {					  // monochrome

			for (k = 0; k < (lpex->nImgWidth + 7) / 8; k++) {
				*lpTmp++ = (*hpTmp & 0x80) >> 7;
				*lpTmp++ = (*hpTmp & 0x40) >> 6;
				*lpTmp++ = (*hpTmp & 0x20) >> 5;
				*lpTmp++ = (*hpTmp & 0x10) >> 4;
				*lpTmp++ = (*hpTmp & 0x08) >> 3;
				*lpTmp++ = (*hpTmp & 0x04) >> 2;
				*lpTmp++ = (*hpTmp & 0x02) >> 1;
				*lpTmp++ = *hpTmp++ & 0x01;
			}
		}
		lpex->nCompUsed += LZWpack(lpex->lpDecodeBuf + lpex->nCompUsed, lpex->lpInBuf, lpex->nOutRowSize, lpex);
	}

	return retval;
}

int IMAPI   IMXDLwrite_block(HANDLE hImage, int nRows, int nRowSize, LPSTR lpBits, LPXINFO lpInfo)
{
	LPEXPORTINFO lpex;
	int         retval = IMGX_OK;
	HPSTR       hpBits;
	WORD				wPass, wCurRow;

	lpex = (LPEXPORTINFO) GlobalLock(hImage);

	//
	// Since we're receiving a DIB block, we need to start at the last
	// row in the block and work our way backwards...
	hpBits = (HPSTR) lpBits + (long) (nRows - 1) * nRowSize;

	// if we're trying to write an interlaced image, we have to write
	// the entire image in one block...there's no other possible way to
	// do it using the ImageMan/X API...
	//
	// So if we're not writing all of the rows in the image, return an error...
	if (lpex->bInterlaced) {
		if (nRows != lpex->nImgHeight) {
			GlobalUnlock(lpex);
			set_error(EXP_BAD_INTERLACE_ROWS, lpex->fName);
			return IMGX_NSUPPORT;
		}

		for (wPass = 0; wPass < 4; wPass++) {
			wCurRow = wStartRowOffset[wPass];
			hpBits = (HPSTR) lpBits + (long) (nRows - 1) * nRowSize;
			hpBits -= (nRowSize * (LONG)wCurRow);

			while ((wCurRow < lpex->nImgHeight) && (retval == IMGX_OK)) {
				retval = write_row_data(lpex, hpBits, nRowSize, lpInfo);		
				wCurRow += wRowSkip[wPass];
				hpBits -= (nRowSize * (LONG)wRowSkip[wPass]);
			}
		}
	}
	else while (nRows-- && (retval == IMGX_OK)) {
		retval = write_row_data(lpex, hpBits, nRowSize, lpInfo);		
		hpBits -= (long) nRowSize;
	}

	GlobalUnlock(hImage);
	return retval;
}

int IMAPI   IMXDLend_write(HANDLE hImage, LPXINFO lpInfo)
{
	LPEXPORTINFO lpex;
	int         retval = IMGX_OK;
	LPSTR       lpTmp2;
	int         nCopy;
	BYTE        outbuf[256];
	HPSTR       hpTmp;

	lpex = (LPEXPORTINFO) GlobalLock(hImage);

	// write the last code
	lpex->nCompUsed += WriteCode(lpex->lpDecodeBuf+lpex->nCompUsed, lpex->nCurPre, lpex);

	// write the EOICODE
	lpex->nCompUsed += WriteCode(lpex->lpDecodeBuf + lpex->nCompUsed, EOICODE, lpex);

	if (lpex->nAvail < 8) {
		hpTmp = (HPSTR) (lpex->lpDecodeBuf) + lpex->nCompUsed;
		*hpTmp >>= lpex->nAvail;
		lpex->nCompUsed++;		  // get the last remmants from the last byte

	}
	// flush our remaining data
	lpTmp2 = lpex->lpDecodeBuf;

	while (lpex->nCompUsed) {
		nCopy = min(255, lpex->nCompUsed);
		outbuf[0] = nCopy;

		_fmemcpy(&outbuf[1], lpTmp2, nCopy);
		lpTmp2 += nCopy;
		lpex->nCompUsed -= nCopy;

		lpex->lpIO->write(lpex->lpHandle, outbuf, nCopy + 1);
	}

	// now write a zero-length block followed by the image terminator
	outbuf[0] = 0;
	outbuf[1] = ';';
	lpex->lpIO->write(lpex->lpHandle, (LPSTR) outbuf, 2);

	lpInfo->lWritten = lpex->lpIO->seek(lpex->lpHandle, 0, SEEK_END);
	lpex->lpIO->close(lpex->lpHandle);

	GlobalFreePtr(lpex->lpTable);
	GlobalFreePtr(lpex->lpDecodeBuf);

	if (lpex->lpInBuf)
		GlobalFreePtr(lpex->lpInBuf);

	GlobalUnlock(hImage);
	GlobalFree(hImage);

	return retval;
}

// //////////////////////////////////////////////////////////////////////////
//
// Helper functions
//
// //////////////////////////////////////////////////////////////////////////
int         LZWpack(LPSTR lpOut, HPSTR lpIn, int nRowSize, LPEXPORTINFO lpex)
{
	int         nCurPre, tmpk, p;
	unsigned char k;
	HPSTR       lpEnd;
	LPSTR       lpStart;
	LPSTRINGTABLE lpTbl;

	lpStart = lpOut;
	lpEnd = lpIn + nRowSize;
	nCurPre = lpex->nCurPre;
	lpTbl = lpex->lpTable;


	if (nCurPre == -1)
		nCurPre = (unsigned char) *lpIn++;

	while (lpIn < lpEnd) {
		k = (unsigned char) *lpIn++;

		if (nCurPre < (CLEARCODE + 2))
			p = (CLEARCODE + 2);
		else
			p = nCurPre;

		tmpk = FindString(lpTbl, nCurPre, k);	// is the string in the table?

		if (tmpk) {
			nCurPre = tmpk;
		} 
		else {
			lpOut += WriteCode(lpOut, nCurPre, lpex);
			AddString(lpTbl, nCurPre, k, lpex->nCurCode++);
			nCurPre = k;

			// now update code size if needed
			if (lpex->nCurCode == (lpex->nBitChange + 1)) {
				if (lpex->nLZWbits == 12) {
					lpOut += ClearLZW(lpex, lpOut);
				} 
				else {
					lpex->nLZWbits++;
					lpex->nBitChange <<= 1;
					if (lpex->nLZWbits == 12)
						lpex->nBitChange -= 1;
				}
			}
		}
	}

	lpex->nCurPre = nCurPre;

	return (lpOut - lpStart);
}

// This function outputs an LZW compression code containing nBits bits to
// the compression buffer...
//
// Returns the # of bytes written to output buffer.
//
// This differs from the WriteCode function used in the TIFF LZW writer
// in that it fills bytes from lsb to msb.

int         WriteCode(LPSTR lpOut, int nCode, LPEXPORTINFO lpex)
{
	int         nWritten = 0;
	int         nAvail = lpex->nAvail;
	int         nBitCnt = lpex->nLZWbits;
	BYTE FAR   *lpByte = (BYTE FAR *) lpOut;

	while (nBitCnt--) {
		if (!nAvail) {
			lpByte++;
			*lpByte = 0;
			nWritten++;
			nAvail = 8;
		}
		(*lpByte) >>= 1;
		if (nCode & 0x01)
			*lpByte |= 0x80;		  // set the high bit

		nAvail--;
		nCode >>= 1;
	}

	lpex->nAvail = nAvail;
	return nWritten;
}

//
// This function outputs a clear code and initializes the string table.
//
int         ClearLZW(LPEXPORTINFO lpex, LPSTR lpOut)
{
	int         nRet;

	nRet = WriteCode(lpOut, CLEARCODE, lpex);
	lpex->nLZWbits = lpex->nResetSize;
	lpex->nBitChange = 1 << lpex->nResetSize;
	EOICODE = CLEARCODE + 1;
	lpex->nCurCode = EOICODE + 1;
	lpex->nCurPre = -1;
	InitializeStringTable(&(lpex->lpTable));
	return nRet;
}

void        set_error(int nIndex, LPCSTR lpFilename)
{
	XSETERROR(hInst, nIndex, "IM10XGIF.DEL", lpFilename);
}

#if DEBUG
void _cdecl FAR Debug(UINT wCategory, LPSTR lpFormat,...)
{
	LPSTR       vaArgs;
	BOOL        bResult;

	va_start(vaArgs, lpFormat);
	(*DebugStringOut) (wCategory, lpFormat, vaArgs);
	va_end(vaArgs);
}
#else
void _cdecl FAR Debug(UINT wCategory, LPSTR lpFormat,...)
{
}
#endif
