/////////////////////////////////////////////////////////////////////////////
//
// dibfuncs.c
//
// ImageMan Scaling Module 
//
// ImageMan Image Processing Library
//
// Copyright (c) 1991-5 Data Techniques, Inc.
// All Rights Reserved
//
// ///////////////////////////////////////////////////////////////////////////
// $History: dibfuncs.cpp $
// 
// *****************  Version 14  *****************
// User: Johnd        Date: 7/20/00    Time: 4:13p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Fixed Issue 264 - Img wasnt scaled when vertical scale was 1 and
// interpolated
// 
// *****************  Version 13  *****************
// User: Ericw        Date: 1/22/99    Time: 1:06p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// GPF interpolated scaling up in the x axis when the rows are dword
// aligned.
// 
// *****************  Version 12  *****************
// User: Ericw        Date: 7/27/98    Time: 11:54a
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Fixed bug #120 - scaling GPF when scaling only vertically.
// 
// *****************  Version 11  *****************
// User: Johnd        Date: 3/05/98    Time: 12:08p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Cleaned up some warnings
// 
// *****************  Version 10  *****************
// User: Ericw        Date: 2/27/98    Time: 3:50p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Interpolated scaling up had signed/unsigned problem causing color
// adjustments to go WAY out of the valid range.
// 
// *****************  Version 9  *****************
// User: Timk         Date: 2/26/98    Time: 1:48a
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Altered antialiasing code to better handle large scaling-down factors
// (no more white to gray background, in theory).
// 
// *****************  Version 8  *****************
// User: Timk         Date: 2/24/98    Time: 3:38p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Fixed a little buggie in 24-bit downsizing -- a one-off decrement
// during scaling.
// 
// *****************  Version 7  *****************
// User: Timk         Date: 2/19/98    Time: 3:35p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Improved color antialiasing code (when scaling down) to include all
// pixels being reduced instead of just the pixels on a single row.
// 
// *****************  Version 6  *****************
// User: Timk         Date: 11/04/97   Time: 9:52a
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Fixed anti-aliasing support so it'll work on something other than even
// divisions of an image.
// 
// *****************  Version 5  *****************
// User: Ericw        Date: 10/17/97   Time: 6:38p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Scaling rows up by non-proportional values could caused black stripes
// on the right side of the image and the main image body totally
// unscaled.
// 
// *****************  Version 4  *****************
// User: Ericw        Date: 10/08/97   Time: 1:48p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Scaling up bug with interpolation - last column of pixels on a white
// backgrounded image would show black. p2 was accounting for the fact
// that it might just beyond the end of the row.
// 
// *****************  Version 3  *****************
// User: Ericw        Date: 10/01/97   Time: 3:05p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Speed improvement using pin instead of clamping.
// 
// *****************  Version 2  *****************
// User: Ericw        Date: 7/28/97    Time: 3:23p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Implemented pixel interpolation/extrapolation for scaling at 24bit.  In
// the process, pretty much re-wrote ScaleDIB using functions for the
// different bitnesses.  It's easier to read and in some cases is even
// faster.
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 7/10/97    Time: 2:02p
// Created in $/imageman 16-32/dll-api level 2
// Initial Lvl 2 Checkin
// 
// *****************  Version 7  *****************
// User: Timk         Date: 8/02/96    Time: 12:35a
// Updated in $/ImageMan 16-32/DLL
// Fixed crash bug in anti-aliasing code when scaling down by a large
// amount.
// 
// *****************  Version 6  *****************
// User: Timk         Date: 7/29/96    Time: 1:45p
// Updated in $/ImageMan 16-32/DLL
// Fixed bug in anti-alias scaling which was causing scaled images to cut
// off right side of image. Bug was in SumUpEachBitCell function, case 4.
// 
// *****************  Version 5  *****************
// User: Johnd        Date: 7/17/96    Time: 5:11p
// Updated in $/ImageManVB/Image Control
// Fixed bug which caused Black border when doing COPY_DEL on certain
// images (1 bpp)
// 
// *****************  Version 4  *****************
// User: Johnd        Date: 9/22/95    Time: 9:27a
// Updated in $/ImageMan 16-32/DLL
// Chged ExtractDIB B&W copy to for (j = 0; j < nCopySize; j++).
// 
// *****************  Version 2  *****************
// User: Timk         Date: 3/30/95    Time: 12:33p
// Updated in $/ImageMan 16-32/DLL
// Fixed scaling bugs, including not scaling if new image was same height
// as original and scaling down width and up height.
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 1/18/95    Time: 3:59p
// Created in $/ImageMan 16-32/DLL
// ImageMan 4.0 Beta 1

#include "internal.h"
#pragma hdrstop
#include <assert.h>
#include <string.h>
#include "dibfuncs.h"

#define MAXVALUE 512

typedef struct tagScaleInfo {
	UINT        nBPP;				  // bits/pixel
	UINT        nSrcByte;		  // byte to start scaling from
	UINT        nSrcWid;
	UINT        nDstWid;
	DWORD       lOrgSrcRowSize;
	UINT        nSrcRowSize;
	UINT        nDstRowSize;
	LPSTR       lpWorkBuf;		  // temporary scanline workspace (AND, OR, and ANTIALIAS)
	UINT				nCellWidth;			// width, in pixels, of default cell width
	DWORD       lFlags;
}           SCALEINFO, FAR * LPSCALEINFO;

static UCHAR const mask_1[8] = {128, 64, 32, 16, 8, 4, 2, 1};
static UCHAR const mask_2[9] = {0, 0x1, 0x3, 0x7, 0xf, 0x1f, 0x3f, 0x7f, 0xff};
static UCHAR const mask_3[9] = {0, 0x80, 0xc0, 0xe0, 0xf0, 0xf8, 0xfc, 0xfe, 0xff};

#define PIN_OFFSET (256)  // offset to identity lookup
extern int pin[] ;

// Converts a value into the number of "on" bits in that value (ANTIALIAS)
static UCHAR const bitCount[256] = {
	0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4,	// 0-15
	 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,	// 16-31
	 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,	// 32-47
	 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,	// 48-63

	1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,	// 64-79
	 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,	// 80-95
	 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,	// 96-111
	 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,	// 112-127

	1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,	// 128-143
	 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,	// 144-159
	 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,	// 160-175
	 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,	// 176-191

	2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,	// 192-
	 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,	// 208-
	 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,	// 224-
	 4, 5, 5, 6, 5, 6, 6, 7, 5, 6, 6, 7, 6, 7, 7, 8	// 240-255
};


static int  ScaleMonoDIB_And(HPSTR, HPSTR, LPSCALEINFO);
static int  ScaleMonoDIB_Or(HPSTR, HPSTR, LPSCALEINFO);
static void SumUpEachBitCell(UINT FAR *, HPSTR, LPSCALEINFO, UINT, UINT);

#define SCALEPARMS HPSTR FAR * hpIn, HPSTR FAR * hpOut, \
			   UINT nDstWid, UINT nSrcWid, \
         DWORD lFlags,  LPWORD FAR * lpScalar, int nSrcRows, int nSrcRowSize

void ScaleDnRow1bpp ( SCALEPARMS ) ;
void ScaleUpRow1bpp ( SCALEPARMS ) ;
void ScaleRow4bpp   ( SCALEPARMS ) ;
void ScaleRow8bpp   ( SCALEPARMS ) ;
void ScaleRow24bpp  ( SCALEPARMS ) ;

// ///////////////////////////////////////////////////////////////////////////
//
// Creates the initial "scaling state" to scale an arbitrary sub-rectangle
// of DIB 'sinfo' into an arbitrary area of the destination DIB 'dinfo'.
// The source and destination must share the same bits/pixel, unless we're
// supersampling a monochrome source... in this case the destination is
// a grayscale DIB.
//
// Returns: Handle to the "scaling state" for the given parameters. This
// handle is not de-allocated until EndDIBScale is called.
// The first "int" in this handle indicates the # of bits/pixel the
// scale handle was setup for.
//
// ///////////////////////////////////////////////////////////////////////////

HGLOBAL     SetDIBScale(LPBITMAPINFOHEADER lpsi, LPRECT lpsr, LPBITMAPINFOHEADER lpdi,
								            LPRECT lpdr, DWORD flags)
{
	UINT        nSrcPix, nSrcErr, nBlkSize, nBPP;
	DWORD       nSrcRowSize;
	int         i, nErrSum;
	LPWORD      lpOffset;
	UINT        nSrcMask, nDstMask;
	UINT        maxvalue, maxentry, normshift;
	int         nStart, nByteRun, nPixRun, nCurPix;
	int         nSrcWid, nSrcHt, nDstWid, nDstHt;
	UCHAR FAR  *lpAndOffset;
	HGLOBAL     hScale = NULL;

	assert(!lpdr);					  // Eeek! not supported, yet...

	nDstWid = (int) lpdi->biWidth;
	nDstHt = (int) lpdi->biHeight;

	assert(lpsr);					  // must have a source area

	nSrcWid = lpsr->right - lpsr->left + 1;
	nSrcHt = lpsr->bottom - lpsr->top + 1;

	nBPP = (UINT) lpsi->biBitCount;
	nSrcRowSize = ROWSIZE(lpsi->biWidth, nBPP);

	if (nSrcWid < nDstWid) {	  // growing

		nSrcPix = nDstWid / nSrcWid;
		nSrcErr = nDstWid % nSrcWid;

		nBlkSize = sizeof(WORD) * nSrcWid;
	} 
	else {							  // shrinking

		nSrcPix = nSrcWid / nDstWid;
		nSrcErr = nSrcWid % nDstWid;

		nBlkSize = sizeof(WORD) * nDstWid;
		if (nBPP < 8)
			nBlkSize *= 3;			  // srcMask(lobyte of int)|srcOffset|dstOffset

		if (flags & (COPY_ANTIALIAS | COPY_ANTIALIAS256)) {
			// I need to calculate the maximum value that can be reached when I
			// sum the '1' bits in the compression cell. This is simply the
			// width of the cell multiplied by its height. Unfortunately, the
			// cell size may vary (due to a non-integral reduction scale).
			// Since I don't want a cell of all 1's coming up almost-white, I
			// calculate the max value off the smallest cell size, then add a few
			// extra whites on the end for larger-than-normal cells.
			// This top-weights the larger cells (it's easier for them to produce
			// a white pixel), and may not be the best solution for faxes;
			// ideally, we'd have a seperate normalization array for each cell size.
			maxvalue = nSrcPix * (nSrcHt / nDstHt);
			maxentry = maxvalue + (nSrcErr ? (nSrcHt / nDstHt) : 0) + 1;
			for (normshift = 0; maxentry > MAXVALUE; normshift++) {
				maxvalue >>= 1;
				maxentry >>= 1;
			}
		}
	}

	hScale = GlobalAlloc(GHND, nBlkSize + sizeof(SCALEINFO));
	if (hScale) {
		i = lpsr->left;			  // find starting byte within source scanline

		switch (nBPP) {
			case 1:
				i /= 8;
				break;
			case 4:
				i /= 2;
				break;
			case 24:
				i *= 3;
				break;
		}

		lpOffset = (LPWORD) GlobalLock(hScale);
		((LPSCALEINFO) lpOffset)->nBPP = nBPP;
		((LPSCALEINFO) lpOffset)->nSrcByte = i;
		((LPSCALEINFO) lpOffset)->nSrcWid = nSrcWid;
		((LPSCALEINFO) lpOffset)->nDstWid = nDstWid;
		((LPSCALEINFO) lpOffset)->lOrgSrcRowSize = nSrcRowSize;
		((LPSCALEINFO) lpOffset)->nCellWidth = nSrcPix;

		((LPSCALEINFO) lpOffset)->nSrcRowSize = ROWSIZE(nSrcWid, nBPP);
		((LPSCALEINFO) lpOffset)->nDstRowSize = ROWSIZE(nDstWid, lpdi->biBitCount);

		if (flags & (COPY_AND | COPY_OR)) {
			((LPSCALEINFO) lpOffset)->lpWorkBuf = (LPSTR) GlobalAllocPtr(GHND, nSrcRowSize);
		} 
		else if (flags & (COPY_ANTIALIAS | COPY_ANTIALIAS256)) {
			((LPSCALEINFO) lpOffset)->lpWorkBuf = (LPSTR) GlobalAllocPtr(GHND, nDstWid*2 * sizeof(UINT)
																				  + (maxentry+10) * sizeof(BYTE));
		
		} 
		else
			((LPSCALEINFO) lpOffset)->lpWorkBuf = NULL;

		((LPSCALEINFO) lpOffset)->lFlags = flags;
		lpOffset += sizeof(SCALEINFO) / sizeof(WORD);

		if (nSrcWid >= nDstWid) {
			switch (nBPP) {
				case 1:
					if (flags & (COPY_AND | COPY_OR | COPY_ANTIALIAS | COPY_ANTIALIAS256)) {
						// must process all pixels, even ones we'd normally skip!
						lpAndOffset = (UCHAR FAR *) lpOffset;

						for (i = 0, nErrSum = 0, nCurPix = 0; i < nDstWid; i++) {
							nErrSum += nSrcErr;
							if (nErrSum >= nDstWid) {
								nPixRun = nSrcPix + 1;
								nErrSum -= nDstWid;
							} 
							else nPixRun = nSrcPix;

							// nPixRun is now the # of source pixels represented
							// by a single pixel in the destination.

							// now determine how many bytes we're gonna have to
							// deal with. Usually it'll just be one (or possibly 2),
							// but sometimes we could be scaling down tremendously.
							//
							// nStart = # of pixels in first byte of run
							// nByteRun = # of full source bytes covered by output pixel
							// nEnd = # of pixels in last byte of run
							nStart = 8 - nPixRun % 8;
							if (nCurPix / 8 == (nCurPix + nPixRun - 1) / 8) {	// all the pixels we want are from current byte

								if ((nCurPix + nPixRun) % 8) {
									*lpAndOffset++ = 1;
									*lpAndOffset++ = (mask_2[nPixRun] << ((8 - nCurPix % 8) - nPixRun));
								} 
								else {
									*lpAndOffset++ = 2;
									*lpAndOffset++ = mask_2[nPixRun];
								}
							} 
							else if ((nCurPix / 8 + 1) == ((nCurPix + nPixRun - 1) / 8)) {	// spans 2 bytes

								if ((nCurPix + nPixRun) % 8) {
									*lpAndOffset++ = 3;
									*lpAndOffset++ = mask_2[8 - nCurPix % 8];
									*lpAndOffset++ = mask_3[nPixRun - (8 - nCurPix % 8)];
								} 
								else {
									*lpAndOffset++ = 4;
									*lpAndOffset++ = mask_2[8 - nCurPix % 8];	// just need 1st mask

								}
							} 
							else {
								if ((nPixRun + nCurPix) % 8) {
									*lpAndOffset++ = 5;
									*lpAndOffset++ = mask_2[8 - nCurPix % 8];	// first mask

									nByteRun = (nPixRun - (8 - nCurPix % 8)) / 8;
									*lpAndOffset++ = (UCHAR) nByteRun;
									*lpAndOffset++ = mask_3[(nPixRun + nCurPix) % 8];	// last mask

								} 
								else {
									*lpAndOffset++ = 6;
									*lpAndOffset++ = mask_2[8 - nCurPix % 8];	// first mask

									nByteRun = (nPixRun - (8 - nCurPix % 8)) / 8;
									*lpAndOffset++ = (UCHAR) nByteRun;
								}
							}
							// we need to store the cell width, which is stored in nPixRun
							// this number will be ignored for all other scaling forms
							*lpAndOffset++ = nPixRun;

							nCurPix += nPixRun;
						}
					} 
					else {
						// just sample the source
						nSrcMask = lpsr->left % 8;
						nDstMask = 0;

						for (i = 0, nErrSum = 0; i < nDstWid; i++) {
							nErrSum += nSrcErr;
							*lpOffset++ = mask_1[nSrcMask];
							if (nErrSum > nDstWid) {
								nErrSum -= nDstWid;
								*lpOffset++ = (nSrcMask + nSrcPix + 1) / 8;
								*lpOffset++ = (nDstMask + 1) / 8;
								nSrcMask = (nSrcMask + nSrcPix + 1) % 8;
							} 
							else {
								*lpOffset++ = (nSrcMask + nSrcPix) / 8;
								*lpOffset++ = (nDstMask + 1) / 8;
								nSrcMask = (nSrcMask + nSrcPix) % 8;
							}
							nDstMask = (nDstMask + 1) % 8;
						}
					}
					break;

				case 4:
					{
						int         nSrcMask, nDstMask;

						nSrcMask = nDstMask = 0xf0;

						for (i = 0, nErrSum = 0; i < nDstWid; i++) {
							nErrSum += nSrcErr;
							*lpOffset++ = nSrcMask & 0x00ff;	// make sure the hi byte ain't set
							//
							// When altering the source mask, we utilize the fact that
							// the mask is either the hi-nybble or lo-nybble of the
							// given source byte. If the pixel-increment amount is
							// odd, we should flip the source mask -- otherwise, it
							// remains the same.
							//

							if (nErrSum > nDstWid) {
								nErrSum -= nDstWid;
								*lpOffset++ = (nSrcPix + 1 + (nSrcMask & 1)) / 2;
								if (!(nSrcPix & 1))
									nSrcMask ^= 0xffff;
							} 
							else {
								*lpOffset++ = (nSrcPix + (nSrcMask & 1)) / 2;
								if (nSrcPix & 1)
									nSrcMask ^= 0xffff;
							}
							*lpOffset++ = (nDstMask & 0x0f) ? 1 : 0;
							nDstMask ^= 0xffff;
						}
					}
					break;

				case 8:
					for (i = 0, nErrSum = 0; i < nDstWid; i++, lpOffset++) {
						nErrSum += nSrcErr;
						if (nErrSum > nDstWid) {
							nErrSum -= nDstWid;
							*lpOffset = nSrcPix + 1;
						} 
						else
							*lpOffset = nSrcPix;
					}
					break;

				case 24:
					nSrcPix *= 3;	  // pre-scale for RGB bytes (avoid multiplying inside loop)

					for (i = 0, nErrSum = 0; i < nDstWid; i++, lpOffset++) {
						nErrSum += nSrcErr;
						if (nErrSum > nDstWid) {
							nErrSum -= nDstWid;
							*lpOffset = nSrcPix + 3;
						} 
						else *lpOffset = nSrcPix;
					}
					break;
			}
		} 
		else {
			int         nTotal = 0;

			for (i = 0, nErrSum = 0; i < nSrcWid; i++, lpOffset++) {
				nErrSum += nSrcErr;
				if (nErrSum > nSrcWid) {
					nErrSum -= nSrcWid;
					*lpOffset = nSrcPix + 1;
					nTotal += nSrcPix + 1;
				} 
				else {
					*lpOffset = nSrcPix;
					nTotal += nSrcPix;
				}
			}
			if (nTotal != nDstWid)
				(*(lpOffset - 1)) += (nDstWid - nTotal);
		}

		GlobalUnlock(hScale);
	}
	return (hScale);
}

// ///////////////////////////////////////////////////////////////////////////
//
// Deallocates the DIB scaling handle hScale.
//
// Returns: 0 on success, non-zero on failure.
//
// ///////////////////////////////////////////////////////////////////////////
int         EndDIBScale(HANDLE hScale)
{
	LPSCALEINFO lpScale = (LPSCALEINFO) GlobalLock(hScale);

	if (lpScale->lpWorkBuf)
		GlobalFreePtr(lpScale->lpWorkBuf);
	GlobalUnlock(hScale);
	GlobalFree(hScale);
	return (0);
}

// ///////////////////////////////////////////////////////////////////////////
//
// Scales the given input buffer into the given output buffer using the
// scaling values (pre-set using SetDIBScale) contained in hScale.
//
// Returns: zero on error, non-zero otherwise
//
// ///////////////////////////////////////////////////////////////////////////
int ScaleDIB(HPSTR hpInBuf, HPSTR hpOutBuf, int nSrcRows, int nDstRows, UINT nSkipRow, HANDLE hScale)
{
	DWORD lFlags ;
	UINT  nBits  ;
	UCHAR p1, p2 ;        // each point to interpolate between
	long  lVAdjust ;
	long  lNew ;
	HPSTR hpOut ;
	int         i, j, k, copyCnt;
	LPWORD      lpOffset, lpTemp;
	HPSTR       hpNextOut, hpNextIn;
	DWORD       nInRowSize, nOutRowSize;
	int         nDstWid, nSrcWid;
	int         nRowErr, nRowSkip, nRowAcc = 0, nErrSkip = 0, nRowCnt;
	HPSTR       hpTmp;
	int         retval = 0;
	LPSCALEINFO lpScale = (LPSCALEINFO) GlobalLock(hScale);
	void (*ScaleFunc)( SCALEPARMS ) ;

	hpInBuf += lpScale->nSrcByte ;
	lFlags  =  lpScale->lFlags   ;
	nBits   =  lpScale->nBPP     ;

	nInRowSize = (int) (lpScale->lOrgSrcRowSize);
	hpInBuf += nInRowSize * nSkipRow;	// skip to proper row in image

	nOutRowSize = lpScale->nDstRowSize;

	hpNextOut = hpOutBuf + nOutRowSize;
	hpNextIn = hpInBuf + nInRowSize;

	nDstWid = lpScale->nDstWid;
	nSrcWid = lpScale->nSrcWid;

	lpOffset = (LPWORD) (lpScale + 1);

		switch ( nBits )
			{
				case  4: ScaleFunc = ScaleRow4bpp  ; break ;
				case  8: ScaleFunc = ScaleRow8bpp  ; break ;
				case 24: ScaleFunc = ScaleRow24bpp ; break ;
			}
	// do this differently depending on whether source rows are greater than
	// dest rows or vice-versa...
	if (nDstRows < nSrcRows) {
		nRowSkip = nSrcRows / nDstRows;
		nRowErr = nSrcRows % nDstRows;
		nErrSkip = nRowSkip;	//nErrSkip is the # of rows to skip including accumulated error (i.e., nRowSkip or nRowSkip+1)

		hpNextIn += (nRowSkip - 1) * nInRowSize;

		for (i = 0; i < nDstRows; i++) {
			lpTemp = lpOffset;

			if ( nBits == 1 ) 
				{
					if (nDstWid <= nSrcWid) {
						if ( lFlags & COPY_AND) {
							LONG HUGE  *lpIn;
							LPLONG      lpLongLineBuf;

							lpLongLineBuf = (LPLONG) lpScale->lpWorkBuf;

							hmemcpy(lpLongLineBuf, hpInBuf, nInRowSize);
							for (j = 1, hpTmp = hpInBuf; j < nRowSkip; j++) {
								lpIn = (LONG HUGE *) hpTmp;
								for (k = (int) (nInRowSize / 4); k--;) {
									lpLongLineBuf[k] &= lpIn[k];
								}
								hpTmp += nInRowSize;
							}
							ScaleMonoDIB_And((HPSTR) lpLongLineBuf, hpOutBuf, lpScale);
						} 
						else if ( lFlags & COPY_OR) {
							LONG HUGE  *lpIn;
							LPLONG      lpLongLineBuf;

							lpLongLineBuf = (LPLONG) lpScale->lpWorkBuf;

							hmemcpy(lpLongLineBuf, hpInBuf, nInRowSize);
							for (j = 1, hpTmp = hpInBuf; j < nRowSkip; j++) {
								lpIn = (LONG HUGE *) hpTmp;
								for (k = (int) (nInRowSize / 4); k--;) {
									lpLongLineBuf[k] |= lpIn[k];
								}
								hpTmp += nInRowSize;
							}
							ScaleMonoDIB_Or((HPSTR) lpLongLineBuf, hpOutBuf, lpScale);
						} 
						else if ( lFlags & (COPY_ANTIALIAS | COPY_ANTIALIAS256)) {
							UINT FAR   *lpacc = (UINT FAR *) lpScale->lpWorkBuf;
#ifdef WIN32
							BYTE       *lpnorm = (BYTE *) (lpacc + nDstWid);
#else
							BYTE        __based((__segment) lpacc) * lpnorm = (BYTE __based((__segment) lpacc) *)(nDstWid * sizeof(UINT));
#endif

							// clear our accumulators
							_fmemset(lpacc, 0, nDstWid*2 * sizeof(UINT));

							// for each accumulated value, we need to scale it down to
							// the 0-15 range or 0-255 range. 
							if (lpScale->lFlags & COPY_ANTIALIAS) {	// 16-level gray
								// now accumulate each scanline
								for (hpTmp = hpInBuf, j = nErrSkip; --j >= 0; hpTmp += nInRowSize) {
									SumUpEachBitCell(lpacc, hpTmp, lpScale, 
										1, 1);
								}
								// now create one output byte containing two four-bit pixels
								for (j = nDstWid / 2; --j >= 0; lpacc += 4) {
									*hpOutBuf++ = (((lpacc[0]*15)/lpacc[1]) << 4) | ((lpacc[2]*15)/lpacc[3]);
								}
								if (nDstWid & 1)
									*hpOutBuf = (((lpacc[0]*15)/lpacc[1]) << 4);
							} 
							else {	  // 256-level gray
								// now accumulate each scanline
								for (hpTmp = hpInBuf, j = nErrSkip; --j >= 0; hpTmp += nInRowSize) {
									SumUpEachBitCell(lpacc, hpTmp, lpScale, 
										1, 1);
								}

								for (j = nDstWid; --j >= 0; lpacc+=2)
									*hpOutBuf++ = (lpacc[0]*255)/lpacc[1];
							}
						} 
						else {
							ScaleDnRow1bpp ( &hpInBuf, &hpOutBuf, nDstWid, nSrcWid, lFlags,  &lpTemp , 0, nInRowSize);
						}
					} 
					else 
						ScaleUpRow1bpp ( &hpInBuf, &hpOutBuf, nDstWid, nSrcWid, lFlags,  &lpTemp, 0 , nInRowSize);
				}
			else
				ScaleFunc ( &hpInBuf, &hpOutBuf, nDstWid, nSrcWid, lFlags,  &lpTemp,  nErrSkip, nInRowSize);

			hpOutBuf = hpNextOut;
			hpNextOut += nOutRowSize;

			hpInBuf = hpNextIn;

			nRowAcc += nRowErr;
			if (nRowAcc >= nDstRows) {
				nRowAcc -= nDstRows;
				hpNextIn += nInRowSize * (nRowSkip + 1);
				nErrSkip = nRowSkip+1;
			} 
			else {
				hpNextIn += nInRowSize * nRowSkip;
				nErrSkip = nRowSkip;
			}
		}
	} 
	else if (nDstRows >= nSrcRows) {    // scale it up
		int    nDstRowCnt = nDstRows;
		HPSTR  hpLastOut  ;
		HPSTR  hpPrevLast ;

		nRowCnt   =  nDstRows / nSrcRows;
		nRowErr   =  nDstRows % nSrcRows;
		hpNextOut += ((nRowCnt - 1) * nOutRowSize);
												
		for (i = 0; i < nSrcRows; i++ )
			{
				hpLastOut = hpOutBuf;
				lpTemp = lpOffset;
				if ( nBits == 1)
					{
						if (nDstWid <= nSrcWid) 
							{
								if ( lFlags & COPY_AND)
									ScaleMonoDIB_And ( (HPSTR) hpInBuf, hpOutBuf, lpScale ) ;
								else 
									if ( lFlags & COPY_OR)
										ScaleMonoDIB_Or ( (HPSTR) hpInBuf, hpOutBuf, lpScale ) ;
									else
										ScaleDnRow1bpp ( &hpInBuf, &hpOutBuf, nDstWid, nSrcWid, lFlags,  &lpTemp , 0, nInRowSize);
							}
						else
							ScaleUpRow1bpp ( &hpInBuf, &hpOutBuf, nDstWid, nSrcWid, lFlags,  &lpTemp, 0, nInRowSize );
					}
				else
					ScaleFunc ( &hpInBuf, &hpOutBuf, nDstWid, nSrcWid, lFlags,  &lpTemp , nErrSkip, nInRowSize);

				nRowAcc += nRowErr;

				if (nRowAcc >= nSrcRows) {
					copyCnt = nRowCnt + 1;
					nRowAcc -= nSrcRows;
					hpOutBuf = hpNextOut + nOutRowSize;
					hpNextOut += (nRowCnt + 1) * nOutRowSize;
					nDstRowCnt--;
				} 
				else {
					copyCnt = nRowCnt;
					hpOutBuf = hpNextOut;
					hpNextOut += nRowCnt * nOutRowSize;
				}
				if ( lFlags & COPY_INTERPOLATE )
					{
						for ( k = 0 ; k < nOutRowSize ; k++ )
							{
								hpOut = hpLastOut + k ;
								p1 = * hpOut ;
								p2 = *( hpLastOut + k ) ;
								lVAdjust = ( ( p2 - p1 ) * 100 / copyCnt ) + 50 ;
								lNew = (UINT) p1 * 100 ;
								for ( j = 0 ; j < copyCnt ; j++ )
									{
										*( hpOut+(nOutRowSize*j) ) = (UINT)pin[PIN_OFFSET+(lNew / 100)] ;
										lNew += lVAdjust ;
									}
							}
						hpPrevLast = hpLastOut ;
					}
				else
					{
						for (j = 0; j < copyCnt; j++)
							hmemcpy(hpLastOut + nOutRowSize * j, hpLastOut, nOutRowSize);
					}
				hpInBuf = hpNextIn;
				hpNextIn += nInRowSize;
			}  //  for i < nSrcRows
		if ( lFlags & COPY_INTERPOLATE )
			{
				for (j = 0; j < copyCnt; j++)
					hmemcpy(hpLastOut + nOutRowSize * j, hpLastOut, nOutRowSize);
			}
	}
	GlobalUnlock ( hScale ) ;

	return retval;
}

static int  ScaleMonoDIB_And(HPSTR hpInBuf, HPSTR hpOutBuf, LPSCALEINFO lpScale)
{
	int         j, k, nByteRun;
	UINT        i;
	UCHAR       inMask, outByte;
	UCHAR FAR  *lpTemplate;		  // pointer to scaling template info

	int         retval = 0;

	lpTemplate = (UCHAR FAR *) (lpScale + 1);

	for (i = 0; i < (lpScale->nDstWid + 7) / 8; i++) {	// for each dest byte

		outByte = 0;

		for (j = 0; j < 8; j++) {
			outByte <<= 1;

			switch (*lpTemplate++) {
				case 1:				  // current byte, no increment
					// opcode | srcmask

					inMask = *lpTemplate++;
					if ((*hpInBuf & inMask) == inMask)
						outByte++;
					break;

				case 2:				  // rest of current byte, +1
					// opcode | srcmask

					inMask = *lpTemplate++;
					if ((*hpInBuf & inMask) == inMask)
						outByte++;
					hpInBuf++;
					break;

				case 3:				  // rest of current byte, part of next, +1
					// opcode | srcmask1 | srcmask2

					inMask = *lpTemplate++;
					if ((*hpInBuf & inMask) == inMask) {
						inMask = *lpTemplate++;	// get second mask

						if ((hpInBuf[1] & inMask) == inMask)
							outByte++;
					} 
					else
						lpTemplate++;  // adjust for second mask

					hpInBuf++;
					break;

				case 4:				  // rest of current byte, all of next, +2

					inMask = *lpTemplate++;
					if ((*hpInBuf & inMask) == inMask) {
						if (hpInBuf[1] == 0xff)
							outByte++;
					}
					hpInBuf += 2;
					break;

				case 5:				  // rest of current, run of full bytes, partial last byte, +?

					inMask = *lpTemplate++;
					nByteRun = *lpTemplate++;
					if ((*hpInBuf & inMask) == inMask) {
						for (k = 1; k <= nByteRun; k++) {
							if (hpInBuf[k] != 0xff) {
								break;
							}
						}
						inMask = *lpTemplate++;
						if (!(outByte & 0x01) && ((hpInBuf[nByteRun + 1] & inMask) == inMask)) {
							outByte++;
						}
					} 
					else {
						lpTemplate++;
					}
					hpInBuf += nByteRun + 1;
					break;

				case 6:
					inMask = *lpTemplate++;
					nByteRun = *lpTemplate++;
					if ((*hpInBuf & inMask) == inMask) {
						for (k = 1; k <= nByteRun; k++) {
							if (hpInBuf[k] != 0xff) {
								break;
							}
						}
						if (k > nByteRun)
							outByte++;
					}
					hpInBuf += nByteRun;
					break;
			}
			lpTemplate++;	//skip anti-aliasing pixel value 
		}
		*hpOutBuf++ = outByte;
	}

	return retval;
}


static int  ScaleMonoDIB_Or(HPSTR hpInBuf, HPSTR hpOutBuf, LPSCALEINFO lpScale)
{
	int         j, k, nByteRun;
	UINT        i;
	UCHAR       inMask, outByte;
	UCHAR FAR  *lpTemplate;		  // pointer to scaling template info

	int         retval = 0;

	lpTemplate = (UCHAR FAR *) (lpScale + 1);

	for (i = 0; i < (lpScale->nDstWid + 7) / 8; i++) {	// for each dest byte

		outByte = 0;

		for (j = 0; j < 8; j++) {
			outByte <<= 1;

			switch (*lpTemplate++) {
				case 1:				  // current byte, no increment
					// opcode | srcmask

					inMask = *lpTemplate++;
					if (*hpInBuf & inMask)
						outByte++;
					break;

				case 2:				  // rest of current byte, +1
					// opcode | srcmask

					inMask = *lpTemplate++;
					if (*hpInBuf & inMask)
						outByte++;
					hpInBuf++;
					break;

				case 3:				  // rest of current byte, part of next, +1
					// opcode | srcmask1 | srcmask2

					inMask = *lpTemplate++;
					if (!(*hpInBuf & inMask)) {
						inMask = *lpTemplate++;	// get second mask

						if (hpInBuf[1] & inMask)
							outByte++;
					} 
					else {
						lpTemplate++;  // adjust for second mask

						outByte++;
					}
					hpInBuf++;
					break;

				case 4:				  // rest of current byte, all of next, +2

					inMask = *lpTemplate++;
					if (!(*hpInBuf & inMask)) {
						if (hpInBuf[1])
							outByte++;
					} 
					else outByte++;
					hpInBuf += 2;
					break;

				case 5:				  // rest of current, run of full bytes, partial last byte, +?

					inMask = *lpTemplate++;
					nByteRun = *lpTemplate++;
					if (!(*hpInBuf & inMask)) {
						for (k = 1; k <= nByteRun; k++) {
							if (hpInBuf[k]) {
								outByte++;
								break;
							}
						}
						inMask = *lpTemplate++;
						if (!(outByte & 0x01) && ((hpInBuf[nByteRun + 1] & inMask))) {
							outByte++;
						}
					} 
					else {
						lpTemplate++;
						outByte++;
					}
					hpInBuf += nByteRun + 1;
					break;

				case 6:
					inMask = *lpTemplate++;
					nByteRun = *lpTemplate++;
					if (!(*hpInBuf & inMask)) {
						for (k = 1; k <= nByteRun; k++) {
							if (hpInBuf[k]) {
								outByte++;
								break;
							}
						}
					} 
					else outByte++;

					hpInBuf += nByteRun;
					break;
			}
			lpTemplate++;	//skip anti-aliasing pixel value 
		}
		*hpOutBuf++ = outByte;
	}

	return retval;
}

// Anti-alias via supersampling
static void SumUpEachBitCell(UINT FAR * lpacc, HPSTR hpInBuf, LPSCALEINFO lpScale, UINT pixval1, UINT pixval2)
{
	UINT        i, k;
	register UINT accum;
	UCHAR FAR  *lpTemplate;		  // pointer to scaling template info

	lpTemplate = (UCHAR FAR *) (lpScale + 1);

	// For each destination accumulator, grab at least one source pixel
	for (i = lpScale->nDstWid; i--; lpTemplate += 3, lpacc+=2) {
		// I can always take some bits from the current source byte
		// (this would be 'case 1')
		accum = bitCount[lpTemplate[1] & *hpInBuf];
		switch (lpTemplate[0]) {
			case 1:					  // current byte, no increment
				// opcode | srcmask | Cell width flag
				lpacc[1] += lpTemplate[2];
				break;

			case 2:					  // rest of current byte, +1
				// opcode | srcmask | Call width flag
				lpacc[1] += lpTemplate[2];
				hpInBuf++;			  // just move to next source byte

				break;

			case 3:					  // rest of current byte, part of next, +1
				// opcode | srcmask1 | srcmask2 | Call width flag
				lpacc[1] += lpTemplate[3];
				accum += bitCount[lpTemplate[2] & *++hpInBuf];
				lpTemplate++;		  // adjust for second mask

				break;

			case 4:					  // rest of current byte, all of next, +2
				// opcode | srcmask | Call width flag
				lpacc[1] += lpTemplate[2];
				accum += bitCount[*++hpInBuf];
				hpInBuf++;
				break;

			case 5:					  // rest of current, run of full bytes, partial last byte, +?
				// opcode | srcmask1 | N | srcmask2 | Call width flag
				lpacc[1] += lpTemplate[4];
				for (k = lpTemplate[2]; k--;) {
					accum += bitCount[*++hpInBuf];
				}
				accum += bitCount[lpTemplate[3] & *++hpInBuf];
				lpTemplate += 2;	  // adjust for count and last mask

				break;

			case 6:					  // rest of current, run of full bytes
				// opcode | srcmask1 | N | Call width flag
				lpacc[1] += lpTemplate[3];
				for (k = lpTemplate[2]; k--;) {
					accum += bitCount[*++hpInBuf];
				}
				hpInBuf++;
				lpTemplate++;		  // adjust for count

				break;
		}
		lpacc[0] += (accum);
	}
}

// ///////////////////////////////////////////////////////////////////////////
//
// Extracts a given rectangle from the source buffer into the dest buffer.
//
// Returns: 0 on success, non-zero on error.
//
// ///////////////////////////////////////////////////////////////////////////
int         ExtractDIB(LPBITMAPINFOHEADER lpbiSrc, HPSTR hpSrcBuf, HPSTR hpDstBuf, LPRECT lpRect)
{
	int         nSrcOffset;		  // offset into each source row to begin copying from

	int         nSrcRowSize, nDstRowSize, i, j, nCopySize, nShiftCnt;

	nSrcRowSize = (int) ROWSIZE(lpbiSrc->biWidth, lpbiSrc->biBitCount);
	nDstRowSize = (int) ROWSIZE(RECTWID(lpRect), lpbiSrc->biBitCount);

	hpSrcBuf -= ((long) nSrcRowSize * lpRect->top);	// position to start of first src row

	switch (lpbiSrc->biBitCount) {
		case 1:
			nSrcOffset = lpRect->left / 8;
			hpSrcBuf += nSrcOffset;
			nShiftCnt = lpRect->left % 8;
			if (nShiftCnt) {
				int         nShiftRightCnt = 8 - nShiftCnt;
				HPSTR       hpTmpDst, hpTmpSrc;

				nCopySize = RECTWID(lpRect) / 8 + ((nShiftCnt + lpRect->right % 8) ? 1 : 0);
				for (i = RECTHI(lpRect); i; i--) {
					hpTmpDst = hpDstBuf;
					hpTmpSrc = hpSrcBuf;
					for (j = 0; j < nCopySize; j++) {
						*hpTmpDst++ = ((*hpTmpSrc) << nShiftCnt) | ((*(hpTmpSrc + 1)) >> nShiftRightCnt);
						hpTmpSrc++;
					}
					hpDstBuf -= nDstRowSize;
					hpSrcBuf -= nSrcRowSize;
				}
			} 
			else {					  // it's an even byte boundary -- can just copy bytes!

				nCopySize = RECTWID(lpRect) / 8 + (RECTWID(lpRect) % 8 ? 1 : 0);
				for (i = RECTHI(lpRect); i; i--) {
					hmemcpy(hpDstBuf, hpSrcBuf, nCopySize);
					hpDstBuf -= nDstRowSize;
					hpSrcBuf -= nSrcRowSize;
				}
			}
			break;

		case 4:
			nSrcOffset = lpRect->left / 2;
			hpSrcBuf += nSrcOffset;
			nShiftCnt = lpRect->left & 0x0001;
			if (nShiftCnt) {
				HPSTR       hpTmpDst, hpTmpSrc;

				nCopySize = (RECTWID(lpRect) / 2) + (lpRect->left & 0x0001) + (lpRect->right & 0x0001);
				for (i = RECTHI(lpRect); i; i--) {
					hpTmpDst = hpDstBuf;
					hpTmpSrc = hpSrcBuf;
					for (j = 0; j < nCopySize; j++) {
						*hpTmpDst++ = ((*hpTmpSrc) << 4) | ((*(hpTmpSrc + 1)) >> 4);
						hpTmpSrc++;
					}
					hpDstBuf -= nDstRowSize;
					hpSrcBuf -= nSrcRowSize;
				}
			} 
			else {					  // it's an even byte boundary -- can just copy bytes!

				nCopySize = RECTWID(lpRect) / 2 + (lpRect->right & 0x0001);
				for (i = RECTHI(lpRect); i; i--) {
					hmemcpy(hpDstBuf, hpSrcBuf, nCopySize);
					hpDstBuf -= nDstRowSize;
					hpSrcBuf -= nSrcRowSize;
				}
			}
			break;

		case 8:
		case 24:
			nSrcOffset = lpRect->left;
			nCopySize = RECTWID(lpRect);
			if (lpbiSrc->biBitCount == 24) {
				nSrcOffset *= 3;
				nCopySize *= 3;
			}
			hpSrcBuf += nSrcOffset;
			for (i = RECTHI(lpRect); i; i--) {
				hmemcpy(hpDstBuf, hpSrcBuf, (LONG) nCopySize);
				hpDstBuf -= nDstRowSize;
				hpSrcBuf -= nSrcRowSize;
			}
			break;
	}

	return 0;
}

void ScaleDnRow1bpp ( SCALEPARMS )
{
	int    j ;
	WORD   nDstMask ;
  HPSTR  hpInBuf  = * hpIn     ;
	HPSTR  hpOutBuf = * hpOut    ;
	LPWORD lpTemp   = * lpScalar ;
	UCHAR       outbyte = 0;

	for ( j = 0; j < nDstWid / 8; j++ )
		{
			if ( *hpInBuf & (char) (*lpTemp++) )
				outbyte |= 0x80;
			hpInBuf += *lpTemp++;
			lpTemp++;

			if (*hpInBuf & (char) (*lpTemp++))
				outbyte |= 0x40;
			hpInBuf += *lpTemp++;
			lpTemp++;

			if (*hpInBuf & (char) (*lpTemp++))
				outbyte |= 0x20;
			hpInBuf += *lpTemp++;
			lpTemp++;

			if (*hpInBuf & (char) (*lpTemp++))
				outbyte |= 0x10;
			hpInBuf += *lpTemp++;
			lpTemp++;

			if (*hpInBuf & (char) (*lpTemp++))
				outbyte |= 0x08;
			hpInBuf += *lpTemp++;
			lpTemp++;

			if (*hpInBuf & (char) (*lpTemp++))
				outbyte |= 0x04;
			hpInBuf += *lpTemp++;
			lpTemp++;

			if (*hpInBuf & (char) (*lpTemp++))
				outbyte |= 0x02;
			hpInBuf += *lpTemp++;
			lpTemp++;

			if (*hpInBuf & (char) (*lpTemp++))
				outbyte |= 0x01;
			hpInBuf += *lpTemp++;
			lpTemp++;

			*hpOutBuf++ = outbyte;
			outbyte = 0;
		}
		// do the last byte
		for (j = 0, nDstMask = 0x80, outbyte = 0; j < nDstWid % 8; j++) 
			{
				if (*hpInBuf & (char) (*lpTemp++))
					outbyte |= nDstMask;
				nDstMask >>= 1;
				hpInBuf += *lpTemp++;
				lpTemp++;
			}
		if( nDstWid % 8 )
			*hpOutBuf = outbyte;
}

void ScaleUpRow1bpp ( SCALEPARMS )
{
	int    j, k ;
	WORD   nDstMask, nSrcMask ;
  HPSTR  hpInBuf  = * hpIn     ;
	HPSTR  hpOutBuf = * hpOut    ;
	LPWORD lpTemp   = * lpScalar ;

	for (j = 0, nDstMask = 0x80, nSrcMask = 0x80; j < nSrcWid; j++)
		{
			if (*hpInBuf & nSrcMask) 
				{
					for (k = *lpTemp; k--;) 
						{
							*hpOutBuf |= (char) (nDstMask);
							nDstMask >>= 1;
							if (!nDstMask) 
								{
									nDstMask = 0x80;
									hpOutBuf++;
								}
						}
				} 
			else
				{
					for (k = *lpTemp; k--;) 
						{
							nDstMask >>= 1;
							if (!nDstMask) 
								{
									nDstMask = 0x80;
									hpOutBuf++;
								}
						}
				}
			lpTemp++;
			nSrcMask >>= 1;
			if (!nSrcMask) 
				{
					hpInBuf++;
					nSrcMask = 0x80;
				}
		}
}

void ScaleRow4bpp ( SCALEPARMS )
{
	int    j, k ;
	WORD   nDstMask ;
  HPSTR  hpInBuf  = * hpIn     ;
	HPSTR  hpOutBuf = * hpOut    ;
	LPWORD lpTemp   = * lpScalar ;

	if ( nDstWid <= nSrcWid )
		{
			for ( j = 0, nDstMask = 0x00f0; j < nDstWid; j++ )
				{
					if ( *lpTemp & 0x000f )
						*hpOutBuf |= (*hpInBuf & (char) (*lpTemp++)) << (nDstMask & 0x00f0 ? 4 : 0);
					else
						*hpOutBuf |= (*hpInBuf & (char) (*lpTemp++)) >> (nDstMask & 0x000f ? 4 : 0);
					nDstMask ^= 0xffff ;
					hpInBuf  += *lpTemp++ ;
					hpOutBuf += *lpTemp++ ;
				}
		}
	else
		{
			UCHAR       nSrcMask = 0xf0;

			for ( j = nSrcWid, nDstMask = 0xf0; j--; )
				{
					for (k = *lpTemp; k--;) 
						{
							if (*hpInBuf & nSrcMask)
								{
									*hpOutBuf |= (nDstMask == nSrcMask ? *hpInBuf & nSrcMask :
															(nSrcMask & 0x0f ? (*hpInBuf & nSrcMask << 4) :
															(*hpInBuf & nSrcMask >> 4)));
								}
							nDstMask ^= 0xff ;
							if ( nDstMask & 0xf0 )
								hpOutBuf++;
						}
					nSrcMask ^= 0xff ;
					if ( nSrcMask & 0xf0 )
						hpInBuf++ ;
					lpTemp++ ;
				}
		}
}

void ScaleRow8bpp ( SCALEPARMS )
{
	int    j, k ;
  HPSTR  hpInBuf  = * hpIn  ;
	HPSTR  hpOutBuf = * hpOut ;
	LPWORD lpTemp   = * lpScalar ;

	if ( nDstWid <= nSrcWid )
		{
			for (j = nDstWid; j--;)
				{
					*hpOutBuf++ = *hpInBuf;
					hpInBuf += *lpTemp++;
				}
		} 
	else
		{
			for (j = nSrcWid; j--;) 
				{
					for (k = *lpTemp; k--;)
						*hpOutBuf++ = *hpInBuf;
					hpInBuf++;
					lpTemp++;
				}
		}
}

void ScaleRow24bpp ( SCALEPARMS )
{
  LPWORD lpTemp   = *lpScalar ;
	HPSTR  hpInBuf  = *hpIn;
	HPSTR		hpTmp, hpTmp2;
	HPSTR  hpOutBuf = *hpOut ;
	UINT   uAccum[3]    ;  // accumulator for extrapolator for each RGB
  long   alHAdjust[3] ;  // horizontal adjustment for interpolation for each RGB
	long   alNewRGB[3]  ;  // running interpolated values for new pixesl
	UINT   ixx, k ;
	long   j ;
	UCHAR  p1, p2 ;        // each point to interpolate between

	if ( nDstWid <= nSrcWid )
		{
			if ( lFlags & COPY_INTERPOLATE ) {
				for ( j = nDstWid; j-- ; ) {
					// *lpTemp was multiplied by 3, so divide 
					// to get pixel count we're replacing...
					UINT uPixWid, uPixCnt;
					uPixWid = (*lpTemp/3);
					if( !nSrcRows ) nSrcRows = 1;
					uPixCnt = uPixWid * nSrcRows;
					uAccum[0] = uAccum[1] = uAccum[2] = 0;	//zero accumulators

					for (ixx = 0, hpTmp = hpInBuf; ixx < nSrcRows; ixx++) {
						for (k=0, hpTmp2 = hpTmp; k<uPixWid; k++) {
							uAccum[0] += *(hpTmp2);
							uAccum[1] += *(hpTmp2+1);
							uAccum[2] += *(hpTmp2+2);
							hpTmp2 += 3;
						}
						hpTmp += nSrcRowSize;
					}
//					hpInBuf += (3*uPixWid);

					if ( uPixCnt ) {
						*hpOutBuf++ = (UCHAR)(uAccum[0] / uPixCnt ) ;
						*hpOutBuf++ = (UCHAR)(uAccum[1] / uPixCnt ) ;
						*hpOutBuf++ = (UCHAR)(uAccum[2] / uPixCnt ) ;
						hpInBuf += (3*uPixWid) ;
					} else {
						*hpOutBuf++ = *hpInBuf++ ;
						*hpOutBuf++ = *hpInBuf++ ;
						*hpOutBuf++ = *hpInBuf++ ;
					}
					lpTemp++ ;
				}
			}
			else {
				for (j = nDstWid; j ; j-- ) {
					*hpOutBuf++ = *hpInBuf;
					*hpOutBuf++ = *(hpInBuf + 1);
					*hpOutBuf++ = *(hpInBuf + 2);
					hpInBuf += *lpTemp++;
				}
			}
		} 
	else		// else we are scaling up
		{
			if ( lFlags & COPY_INTERPOLATE )
				{
					for (j = nSrcWid ; --j ; ) {
						for ( ixx = 0 ; ixx < 3 ; ixx++ )
							{
								p1 = *(hpInBuf + ixx) ;
								if ( j == (nSrcWid - 1) )		// if it's the last pixel p2 would point to p1
									p2 = p1 ;
								else
									p2 = *(hpInBuf + ixx + 3 ) ;
								alHAdjust[ixx] = ( ( p2 - p1 ) * 100 / (int)*lpTemp ) + 50 ;
								alNewRGB[ixx] = (UINT)p1 * 100  ;
							}
						for (k = *lpTemp; k-- ; ) {
							for ( ixx = 0 ; ixx < 3 ; ixx++ )  // for each rgb
								{
									*hpOutBuf++ = (UINT)pin[PIN_OFFSET+(alNewRGB[ixx] / 100)] ;
									alNewRGB[ixx] += alHAdjust[ixx] ;
								}
						}
						hpInBuf += 3;
						lpTemp ++ ;
					}
				}
			else
				{
					for (j = nSrcWid; j--;) {
						for (k = *lpTemp; k--;) {
							*hpOutBuf++ = *hpInBuf;
							*hpOutBuf++ = *(hpInBuf + 1);
							*hpOutBuf++ = *(hpInBuf + 2);
						}
						hpInBuf += 3;
						lpTemp++;
					}
				}
		}
	* hpIn     = hpInBuf  ;
	* hpOut    = hpOutBuf ;
	* lpScalar = lpTemp   ;

}

