//////////////////////////////////////////////////////////////
// Nag.cpp - Implementation of Bother Screens
//

#include "nag.h"
                 
#ifdef WIN32                 
	static TCHAR	tcURL[500];
#else
	static char 	cURL[500];
#endif	

LPWORD lpwAlign ( LPWORD lpIn)
{    
	ULONG ul;  

	ul = (ULONG) lpIn; 
	ul +=3;
    ul >>=2;    
	ul <<=2;    
	return (LPWORD) ul;
}
     
#pragma optimize ( "y", off )  
LRESULT LOADDS CALLBACK DialogProc (HWND hdlg, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	static int	nCountDown;
	static BOOL		bPosChange;

	switch (iMsg)
	{
	case WM_INITDIALOG:
#ifndef _DEBUG
		nCountDown = 5;
#else
		nCountDown = 2;
#endif


		bPosChange = TRUE;
		SetWindowPos(hdlg, HWND_TOPMOST, 0,0, 0, 0, SWP_NOSIZE | SWP_NOMOVE | SWP_SHOWWINDOW);


		while (!SetTimer (hdlg, 1, 1000, NULL))
			if (IDCANCEL == MessageBox (hdlg, 
					"Too many clocks or timers!", "Timer conflict",
					MB_ICONEXCLAMATION | MB_RETRYCANCEL))
				return TRUE;
		return TRUE;
		break;		
#ifndef WIN32
	// These following two messages are needed to paint the dialog grey in 16 bits.
	case WM_PAINT:    
	{                                     
		PAINTSTRUCT ps;
		
		// Paint Background for Orphan.
		HANDLE hdc = BeginPaint (hdlg, &ps);
		FillRect(hdc, &ps.rcPaint,(HBRUSH) GetStockObject(LTGRAY_BRUSH)); //COLOR_BTNFACE));
		EndPaint(hdlg, &ps);
		break;		
	}    
	case WM_CTLCOLOR:
	{
		if ((HIWORD(lParam)) == CTLCOLOR_STATIC)  // Static control
		{    
			// Make static text controls grey.
			SetBkMode((HDC)wParam, TRANSPARENT);		
			return (int)GetStockObject(LTGRAY_BRUSH);
		}
		break;
	}
#endif
	case WM_TIMER:
		{
			TCHAR		tcTemp[10];

			wsprintf(tcTemp, _T("Wait  %d"), --nCountDown);
			SetDlgItemText (hdlg, IDOK, tcTemp);

			if (nCountDown < 1)
			{
				HWND	hwndChild;

				SetDlgItemText (hdlg, IDOK, _T("O&kay"));
				hwndChild = GetDlgItem(hdlg, IDOK);
				EnableWindow( hwndChild, TRUE);
				hwndChild = GetDlgItem(hdlg, 101);
				EnableWindow(hwndChild, TRUE);
				KillTimer(hdlg, 1);
			}
			return TRUE;
		}
	case WM_COMMAND: 
		if (nCountDown < 1)
		{
			switch (LOWORD(wParam))
			{
			case IDOK:
				EndDialog(hdlg, TRUE);
				break;
			case 101:
#ifdef WIN32			
				ShellExecute(hdlg, "open", tcURL, NULL, NULL, SW_SHOWNORMAL); 
#else 
       			ShellExecute(hdlg, "open", cURL, NULL, NULL, SW_SHOWNORMAL); 
#endif				
				EndDialog(hdlg, TRUE);
				break;
			}	
		}
	case WM_WINDOWPOSCHANGED:
		// Need Code to prevent user from moving window.
		// And a toggle flag so this is not recursive!!
		if (bPosChange)
			bPosChange = FALSE;
		else
		{
			bPosChange = TRUE;
			SetWindowPos(hdlg, HWND_TOPMOST, 0,0, 0, 0, SWP_NOSIZE | SWP_NOMOVE | SWP_SHOWWINDOW);
		}
		return 0;
	}
	// Eat any message prior to nCount < 1 that normally needs to be 
	// dealt with by the Default WindowProc.
	if (nCountDown > 0)
	{
		switch (iMsg)
		{
		case WM_DESTROY:
		case WM_CLOSE:
		case WM_QUIT:
		case WM_MOVE:
		case WM_SIZE:
		case WM_DRAWITEM:
			return TRUE;
		}
	}
	return FALSE;
}
#pragma optimize ( "y", on )

//#pragma pack(1)
#ifdef WIN32
	#define LPSMALL  LPWORD
#else
	#define LPSMALL	LPBYTE
#endif		


LRESULT DisplayBotherScreen(long hinst)
{

    HGLOBAL hgbl;    
    LPDLGITEMTEMPLATE lpdit;     
	LPWORD lpw;   
    LPSMALL	lps;
#ifdef WIN32	
	LPDLGTEMPLATE lpdt;
    LPWSTR lpwsz;   
    TCHAR	tcPrompt[500], *tcp;
	int		nchar;
#else         
	LPSMALL	lpdt;
	//LPSTR	lpwsz;   
	char cPrompt[500];
	int 	nRet;
#endif
    LRESULT ret;          
    
	

#ifdef WIN32
	{
		HKEY	hKey;


		if (ERROR_SUCCESS == RegCreateKey(HKEY_LOCAL_MACHINE, 
			_T("SOFTWARE\\DTI\\General"), &hKey))
		{
			DWORD	dwType = REG_SZ;

//
// Hard coded text not in Resources needs to be edited.
//

			DWORD	dwLen = sizeof(tcURL);
			if (!(ERROR_SUCCESS ==RegQueryValueEx(hKey, _T("URL"), NULL, &dwType, (unsigned char*)tcURL, &dwLen)))
				_tcsncpy(tcURL, 
					_T("http://www.data-tech.com/Orders/catalog.asp"),
					sizeof(tcURL));

			dwLen = sizeof(tcPrompt);
			if (!(ERROR_SUCCESS ==RegQueryValueEx(hKey, _T("Prompt"), NULL, &dwType, (unsigned char*)tcPrompt, &dwLen)))
				_tcsncpy(tcPrompt, 
					_T("To order ImageMan from Data Techniques Inc. Please Visit our Web site www.data-tech.com online ordering page."),
					sizeof(tcPrompt));

// End hard coded text.
			tcp = tcPrompt;
			RegCloseKey(hKey);
		}
	}
#else         
	// Get strings out of Win.ini   
	GetProfileString( "DTI,GENERAL", "Prompt", " ", cPrompt, sizeof(cPrompt));  
	if ('\0' == cPrompt[0])
		_fstrncpy(cPrompt, "To order ImageMan from Data Techniques Inc. Please Visit our Web site www.data-tech.com online ordering page.", sizeof(cPrompt));
		
	GetProfileString( "DTI,GENERAL", "URL", " ", cURL, sizeof(cURL));
	if ('\0' == cURL[0])
	         _fstrncpy(cURL, "http://www.data-tech.com/Orders/catalog.asp", sizeof(cURL));
	
#endif

    hgbl = GlobalAlloc(GMEM_ZEROINIT, 2048);
    if (!hgbl)
        return -1;
    // Define a dialog box.
#ifdef WIN32   
    lpdt = (LPDLGTEMPLATE)GlobalLock(hgbl);
    memset((void *)lpdt, NULL, 2048);
    
	// No Caption
	lpdt->style = DS_CENTER | DS_MODALFRAME | DS_3DLOOK | WS_POPUP 
				| WS_VISIBLE | DS_SETFOREGROUND | DS_SETFONT; 
    lpdt->cdit = 4;  // number of controls
	lpdt->x  = 0;		lpdt->y  = 0;
	lpdt->cx = 134;		lpdt->cy = 94;   
	lpdt->dwExtendedStyle = 0;
	lps = (LPSMALL) (lpdt + 1);
#else        
    lpdt = (LPSMALL)GlobalLock(hgbl);
    _fmemset((void __far *)lpdt, 0xFF, 2048);

	LPDWORD	lpdw;
	 // Stucture did not work because it was WORD aligned!!!  
	 // Even with Pragma Pack  
	lps = (LPSMALL)lpdt; 
	lpdw = (LPDWORD)lpdt;
	*lpdw++ = WS_POPUP | DS_SETFONT;     // style	
	lps = (LPBYTE)lpdw;
	*lps++ = 4;     //# controls
	lpw = (LPWORD)lps;
	*lpw++ = 0;    	//X
	*lpw++ = 0;		//Y
    *lpw++ = 134;    	//CX
	*lpw++ = 94;		//CY   
	lps = (LPBYTE)lpw;	         
#endif            
    
    *lps++ = 0;   // no menu
    *lps++ = 0;   // predefined dialog box class (by default)
	*lps++ = 0;   // No Title

	// FONT 8, "MS Sans Serif"   
	lpw = (LPWORD)lps;
	*lpw++ = 8;
#ifdef WIN32
	lpwsz = (LPWSTR) lpw;
	nchar = 1+ MultiByteToWideChar (CP_ACP, 0, "MS Sans Serif", -1, lpwsz, 50);
    lpw   += nchar;  
    lpw = lpwAlign (lpw);
#else  
	lps = (LPSMALL)lpw; 
	_fstrcpy((LPSTR)lps, "MS Sans Serif");    
	nRet = _fstrlen((LPSTR)lps);    
	lps += nRet + 1;   
	lpw = (LPWORD) lps;
#endif
   
    //-----------------------
    // Define an OK button.
    //-----------------------
    lpdit = (LPDLGITEMTEMPLATE) lpw;
	lpdit->x  = 7; lpdit->y  = 73;
	lpdit->cx = 50; lpdit->cy = 14;
    lpdit->id = IDOK;  // OK button identifier
    lpdit->style = WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | WS_DISABLED;
       
#ifdef WIN32 
	lpdit->dwExtendedStyle = 0;
	lpw = (LPWORD) (lpdit + 1);  
    *lpw++ = 0xFFFF;
    *lpw++ = 0x0080;    // button class
    lpwsz = (LPWSTR) lpw;
    nchar = 1 + MultiByteToWideChar (CP_ACP, 0, "Wait  5", -1, lpwsz, 50);
    lpw   += nchar;
	// Need to skip this line... LpwAlign got messed up??? Don't ask me why...
    //*lpw++ = 0;              // no creation data !!!    
    lpw = lpwAlign (lpw);
#else    
	lps = (LPSMALL) (lpdit + 1);
	*lps++ = 0x80;   	
	_fstrcpy((LPSTR)lps, "Wait 5");    
	nRet = _fstrlen((LPSTR)lps);    
	lps += nRet + 1; 
	*lps++ = 0;  // no creation data
	lpw = (LPWORD) lps;
#endif 

    //-----------------------
    // Define an Order button.
    //-----------------------
    lpdit = (LPDLGITEMTEMPLATE) lpw;
	lpdit->x  = 77; lpdit->y  = 73;
	lpdit->cx = 50; lpdit->cy = 14;
    lpdit->id = 101; //ID_HELP;    // Help button identifier
    lpdit->style = WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | WS_DISABLED;

#ifdef WIN32   
	lpdit->dwExtendedStyle = 0;
    lpw = (LPWORD) (lpdit + 1);
    *lpw++ = 0xFFFF;
    *lpw++ = 0x0080;                 // button class atom
	lpwsz = (LPWSTR) lpw;
    nchar = 1+MultiByteToWideChar (CP_ACP, 0, "&Order", -1, lpwsz, 50);
    lpw   += nchar;              
    *lpw++ = 0;                      // no creation data      
    lpw = lpwAlign (lpw);
#else       
	lps = (LPSMALL) (lpdit + 1);
	*lps++ = 0x80;   	
	_fstrcpy((LPSTR)lps, "&Order");    
	nRet = _fstrlen((LPSTR)lps);    
	lps += nRet + 1; 
	*lps++ = 0;  // no creation data
	lpw = (LPWORD) lps;
#endif

    //-----------------------
    // Define a static text control/ Title 
    //-----------------------
    lpdit = (LPDLGITEMTEMPLATE) lpw;
			lpdit->x  = 7; lpdit->y  = 7;
			lpdit->cx = 110; lpdit->cy = 17;
    lpdit->id = 200; //ID_TEXT;  // text identifier
    lpdit->style = WS_CHILD | WS_VISIBLE | SS_CENTER;

//
// Hard coded text not in Resources needs to be edited.
//
#ifdef WIN32    
	lpdit->dwExtendedStyle = 0;
    lpw = (LPWORD) (lpdit + 1);
    *lpw++ = 0xFFFF;
    *lpw++ = 0x0082;                         // static class
	lpwsz = (LPWSTR) lpw;
    nchar = 1+ MultiByteToWideChar (CP_ACP, 0, "ImageMan Developers Toolkit\nEvaluation Version.", -1, lpwsz, 50);
    lpw   += nchar;  
    // Need to skip this line... LpwAlign got messed up??? Don't ask me why...
    //*lpw++ = 0;              // no creation data !!!        
    lpw = lpwAlign (lpw);
#else     
	lps = (LPSMALL) (lpdit + 1);
	*lps++ = 0x82;   	
	_fstrcpy((LPSTR)lps, "ImageMan Developers Toolkit\nEvaluation Version.");    
	nRet = _fstrlen((LPSTR)lps);    
	lps += nRet + 1; 
	*lps++ = 0;  							// no creation data
	lpw = (LPWORD) lps;
#endif
// End hard coded text.

    //-----------------------
    // Define Second static text control/ Message 
    //-----------------------
    lpdit = (LPDLGITEMTEMPLATE) lpw;
			lpdit->x  = 7; lpdit->y  = 27;
			lpdit->cx = 118; lpdit->cy = 43;
    lpdit->id = 200; //ID_TEXT;  // text identifier
    lpdit->style = WS_CHILD | WS_VISIBLE | SS_LEFT;

#ifdef WIN32
	lpdit->dwExtendedStyle = 0;
    lpw = (LPWORD) (lpdit + 1);
    *lpw++ = 0xFFFF;
    *lpw++ = 0x0082;                         // static class
	for (lpwsz = (LPWSTR)lpw;
        *lpwsz++ = (WCHAR) *tcp++;)      
    lpw = (LPWORD)lpwsz;
    *lpw++ = 0;                              // no creation data         
    lpw = lpwAlign (lpw);
#else     
	lps = (LPSMALL) (lpdit + 1);
	*lps++ = 0x82;   
    _fstrcpy((LPSTR)lps, cPrompt);    
	nRet = _fstrlen((LPSTR)lps);    
	lps += nRet + 1; 
	*lps++ = 0;  							// no creation data
	lpw = (LPWORD) lps;   
#endif


    GlobalUnlock(hgbl);
#ifdef WIN32
    ret = DialogBoxIndirect((HMODULE)hinst,
                            (LPDLGTEMPLATE) hgbl,
                            NULL,	//hwndOwner,
                            (DLGPROC) DialogProc);
#else
	    DLGPROC dlgprc = NULL;
	    
	    dlgprc = (DLGPROC) MakeProcInstance((DLGPROC)DialogProc, (HMODULE)hinst);	
	    ret = DialogBoxIndirect((HMODULE)hinst,
                            hgbl,
                            NULL,	//hwndOwner,
                            (DLGPROC) dlgprc);
#endif
    GlobalFree(hgbl);

    return ret;
}

