/////////////////////////////////////////////////////////////////////////////
//
//    imgio.c
//
//    ImageMan structured IO module
//
//    ImageMan Image Processing Library
//
//    Copyright (c) 1991-5 Data Techniques, Inc.
//    All Rights Reserved
//
/////////////////////////////////////////////////////////////////////////////
//$Header: /ImageMan 16-32/DLL - V 6.xx API/IMGIO.C 9     10/05/99 9:30a Ericj $
//$History: IMGIO.C $
// 
// *****************  Version 9  *****************
// User: Ericj        Date: 10/05/99   Time: 9:30a
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Added many Ouput Debug strings for tracking down file handling errors.
// 
// *****************  Version 8  *****************
// User: Ericw        Date: 8/11/98    Time: 11:02a
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// My fileio_read fixes caused looping.  Ooopsy!
// 
// *****************  Version 7  *****************
// User: Ericw        Date: 8/10/98    Time: 5:19p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Fixed bug #126.  Caused GIFs to no longer load because the fileio_read
// was returning the count of bytes requested not the actual bytes read.
// 32bit only.  Attempting to open a GIF would hang the system.
// 
// *****************  Version 6  *****************
// User: Johnd        Date: 7/07/98    Time: 3:32p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Fixed Bug #113 (OF_EXIST creates a file instead of checking for
// existence) Also fixed #116
// 
// *****************  Version 5  *****************
// User: Ericj        Date: 4/29/98    Time: 12:18p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Added 32 bit code for supporting long path and file names.
// 
// *****************  Version 2  *****************
// User: Ericj        Date: 4/29/98    Time: 10:52a
// Updated in $/ImageMan 16-32/DLL - V 5.xx API
// Added 32 bit code for supporting long path and file names.
// 
// *****************  Version 3  *****************
// User: Timk         Date: 7/27/95    Time: 12:27p
// Updated in $/ImageMan 16-32/DILS
// Memory writing now correctly expands memory blocks when needed.
// 
// *****************  Version 2  *****************
// User: Timk         Date: 7/25/95    Time: 11:01p
// Updated in $/ImageMan 16-32/DILS
// Added writing using IO function blocks.
// 
// *****************  Version 1  *****************
// User: Timk         Date: 6/30/95    Time: 12:06p
// Created in $/ImageMan 16-32/DILS
// Functions for for use with I/O blocks.

#include "internal.h"
#pragma hdrstop
#include <string.h>
#include <stdio.h>
#include "imgio.h"


////////////////////////////////////////////////////////////////////////////////
//
// standard file I/O routines for use with the IMG_IO struct.
//
////////////////////////////////////////////////////////////////////////////////
#define FILE_BUFFER_SIZE	512
#ifdef WIN32
	typedef struct tagFILE_IO {
		HANDLE	fHand;
		int	nErrVal;
		WORD	nBufCnt;
		char	buf[FILE_BUFFER_SIZE];
	} FILE_IO, FAR *LPFILE_IO;
#else
	typedef struct tagFILE_IO {
		int	fHand;
		int	nErrVal;
		WORD	nBufCnt;
		char	buf[FILE_BUFFER_SIZE];
	} FILE_IO, FAR *LPFILE_IO;
#endif

int fileio_open(LPVOID lpIn, unsigned uFlags, LPVOID FAR *lplpHandle)
{
#ifdef WIN32
	DWORD	dwAccess = 0, dwShare = 0, dwCreate = 0;
#else
	OFSTRUCT	of;
#endif
	LPFILE_IO	lpIO;

#ifdef DEBUG
	char	cDBG[200];
	wsprintf(cDBG, "Attempting to Open File %s\n\rIn fileio_open with the following flags: %ud\n\r", lpIn, uFlags);
	OutputDebugString((LPCTSTR) cDBG);
#endif

	lpIO = (LPFILE_IO)GlobalAllocPtr(GHND, sizeof(FILE_IO));
	if (lpIO) {
#ifdef WIN32
		// Section added using CreateFile to support file names longer
		// than 128 bytes.

		if (OF_EXIST & (WORD)uFlags) {
#ifdef DEBUG
		OutputDebugString((LPCTSTR) "OF_EXIST\n\r");
#endif
			dwAccess = GENERIC_READ;
			dwShare = FILE_SHARE_READ;
			dwCreate = OPEN_EXISTING; 
			// will fail if file exists, need to Get last error
		}
		else {
			if ((OF_READ & (WORD)uFlags) || (uFlags == 0)) {
#ifdef DEBUG
				OutputDebugString((LPCTSTR) "OF_READ\n\r");
#endif
				dwAccess |= GENERIC_READ;
				dwShare = FILE_SHARE_READ;
				dwCreate = OPEN_EXISTING;
			}
			if (OF_WRITE & (WORD)uFlags) {
#ifdef DEBUG
				OutputDebugString((LPCTSTR) "OF_WRITE\n\r");
#endif
				dwAccess |= GENERIC_WRITE;
				dwShare = 0;
				dwCreate = OPEN_ALWAYS;
			}
			if (OF_READWRITE  & (WORD)uFlags) {
#ifdef DEBUG
				OutputDebugString((LPCTSTR) "OF_READWRITE\n\r");
#endif
				dwAccess = GENERIC_READ | GENERIC_WRITE;
				dwShare = 0;
				dwCreate = OPEN_ALWAYS;
			}
			if (OF_CREATE & (WORD)uFlags) {
#ifdef DEBUG
				OutputDebugString((LPCTSTR) "OF_CREATE\n\r");
#endif
				dwAccess = GENERIC_READ | GENERIC_WRITE;
				dwCreate = CREATE_ALWAYS;
			}
			if (OF_SHARE_COMPAT & (WORD)uFlags)
#ifdef DEBUG
				OutputDebugString((LPCTSTR) "OF_SHARE_COMPAT\n\r");
#endif
				dwShare = FILE_SHARE_READ | FILE_SHARE_WRITE;
		}
		lpIO->fHand = CreateFile(lpIn, dwAccess, dwShare, NULL, dwCreate, FILE_ATTRIBUTE_NORMAL, NULL);
		if (lpIO->fHand == INVALID_HANDLE_VALUE) {
#ifdef DEBUG
			char	cDBG[200];
			wsprintf(cDBG, "Error Loading File %s in fileio_open: %ld\n\r", lpIn, GetLastError());
			OutputDebugString((LPCTSTR) cDBG);
#endif
		   GlobalFreePtr(lpIO);
		   return IMG_NOFILE;

		}
#ifdef DEBUG
		{
		char	cDBG[200];
		wsprintf(cDBG, "File %s opened in fileio_open with following handle: %ld\n\r", lpIn, lpIO->fHand);
		OutputDebugString((LPCTSTR) cDBG);
		}
#endif
		if ((uFlags & OF_EXIST) ) {
#ifdef DEBUG
			char	cDBG[200];
			wsprintf(cDBG, "File %s closed in fileio_open with following handle: %ld\n\r", lpIn, lpIO->fHand);
			OutputDebugString((LPCTSTR) cDBG);
#endif
			CloseHandle( lpIO->fHand );
			GlobalFreePtr(lpIO);
			*lplpHandle = NULL;
		} 
#else
		if ((lpIO->fHand = OpenFile((LPSTR)lpIn, &of, uFlags)) == HFILE_ERROR) {
			GlobalFreePtr(lpIO);
			return IMG_NOFILE;
		}

		// if uFlags == OF_EXIST, we're just checking to see whether
		// the file exists or not. The file will NOT be opened, so
		// we need to set lplpHandle to NULL and free any memory
		// we've allocated...

		if (uFlags == OF_EXIST) {
			GlobalFreePtr(lpIO);
			*lplpHandle = NULL;
		}
#endif
		else {
			lpIO->nBufCnt = FILE_BUFFER_SIZE;
			lpIO->nErrVal = IMG_OK;
			*lplpHandle = lpIO;
		}

		return IMG_OK;
	}
	else {
		return IMG_NOMEM;
	}
}
 
int fileio_close(LPVOID lpHandle)
{
	LPFILE_IO lpIO = (LPFILE_IO)lpHandle;
#ifdef WIN32
	BOOL	bRet = FALSE;
#endif

#ifdef DEBUG
	char	cDBG[200];
	wsprintf(cDBG, "File closed in fileio_close with following handle %ld\n\r", (unsigned long) *(unsigned long *)lpHandle);
	OutputDebugString((LPCTSTR) cDBG);
#endif
	
#ifdef WIN32
	bRet = CloseHandle(lpIO->fHand);

#ifdef DEBUG
	wsprintf(cDBG, "CloseHandle returned %s\n\r", bRet ? "TRUE" : "FALSE");
	OutputDebugString((LPCTSTR) cDBG);
	if (!(bRet))
		wsprintf(cDBG, "Get Last error after CloseHandle returned %ld\n\r", GetLastError());
#endif
	if (!(bRet))
	{
		bRet = CloseHandle(lpIO->fHand);

#ifdef DEBUG
	wsprintf(cDBG, "CloseHandle Second Time returned %s\n\r", bRet ? "TRUE" : "FALSE");
	OutputDebugString((LPCTSTR) cDBG);
	if (!(bRet))
		wsprintf(cDBG, "Get Last error after CloseHandle Second Time returned %ld\n\r", GetLastError());
#endif
	}
#else
	_lclose(lpIO->fHand);
#endif

	GlobalFreePtr(lpIO);
	return IMG_OK;
}

DWORD fileio_read(LPVOID lpHandle, LPVOID hpBuf, DWORD dwBytes)
{
	LPFILE_IO lpIO = (LPFILE_IO)lpHandle;
	DWORD	dwBytesRead = 0;
#ifdef WIN32
	BOOL  bRead = TRUE ;
	DWORD	wCnt, wTmp;
#else
	WORD	wCnt, wTmp;
#endif


#ifdef WIN32
	while ( ( dwBytes ) && bRead ) {
		wCnt = (WORD)min(dwBytes, 32768);	 //max 32K chunks
		bRead = ReadFile(lpIO->fHand, hpBuf, wCnt, &wTmp, NULL) ;
		if ( !( bRead ) || ( wCnt != wTmp ) ) {
#else
	while (dwBytes) {
		wCnt = (WORD)min(dwBytes, 32768);	 //max 32K chunks
		if ( (wTmp = _lread(lpIO->fHand, hpBuf, wCnt)) != wCnt) {
#endif
			dwBytesRead += wTmp;
			lpIO->nErrVal = IMG_FILE_ERR;	//error reading from file
			return dwBytesRead;
		}
		else {
			dwBytes -= wTmp;
			dwBytesRead += wTmp ;
			(HPSTR)hpBuf += (LONG)wTmp ;
		}
	}

	return dwBytesRead;
}

DWORD fileio_seek(LPVOID lpHandle, LONG lOffset, WORD nOrigin)
{
	LPFILE_IO lpIO = (LPFILE_IO)lpHandle;
#ifdef WIN32
	DWORD dwRet;
#endif

	// seeking will always flush any buffered data we've got
	lpIO->nBufCnt = FILE_BUFFER_SIZE;
#ifdef WIN32
	// Watch out! since the third parameter is NULL can only access
	// files 2^32 - 2 in bytes. ;-)
	dwRet = SetFilePointer(lpIO->fHand, lOffset, NULL, (DWORD) nOrigin);
	if (dwRet == 0xFFFFFFFF)
		return (HFILE_ERROR);
	else
		return (dwRet);
#else
	return _llseek(lpIO->fHand, lOffset, nOrigin);
#endif
}

DWORD fileio_write(LPVOID lpHandle, LPVOID hpBuf, DWORD dwBytes)
{
	LPFILE_IO lpIO = (LPFILE_IO)lpHandle;
	DWORD	dwBytesWritten = 0;
#ifdef WIN32
	DWORD	wCnt, wTmp, dRet;
#else
	WORD	wCnt, wTmp;
#endif

	while (dwBytes) {
		wCnt = (WORD)min(dwBytes, 32768);
#ifdef WIN32
		if (!(WriteFile(lpIO->fHand, hpBuf, wCnt, &wTmp, NULL))) {
			dRet = GetLastError();
#else
		if ( (wTmp = _lwrite(lpIO->fHand, hpBuf, wCnt)) != wCnt) {
#endif
			dwBytesWritten += wTmp;
			lpIO->nErrVal = IMG_FILE_ERR;
			return dwBytesWritten;
		}
		else {
			dwBytes -= wTmp ;
			(HPSTR)hpBuf += (LONG)wTmp ;
			dwBytesWritten += wTmp ;
		}
	}
	return dwBytesWritten;
}

int fileio_error(LPVOID lpHandle, LPSTR lpErrBuf)
{
	LPFILE_IO lpIO = (LPFILE_IO)lpHandle;

#ifdef DEBUG
	char	cDBG[200];
	wsprintf(cDBG, "File Error function fileio_error  handle: %ld  error number: %ld\n\r", (unsigned long *)lpHandle, lpIO->nErrVal);
	OutputDebugString((LPCTSTR) cDBG);
#endif

	_fstrcpy(lpErrBuf, "Error reading/writing to file.");
	return lpIO->nErrVal;
}

int fileio_getchar(LPVOID lpHandle)
{
#ifdef WIN32
	DWORD	wTemp;
#endif

	LPFILE_IO lpIO = (LPFILE_IO)lpHandle;

	if (lpIO->nBufCnt >= FILE_BUFFER_SIZE) {
#ifdef WIN32
		ReadFile(lpIO->fHand, lpIO->buf, FILE_BUFFER_SIZE, &wTemp , NULL);
#else
		_lread(lpIO->fHand, lpIO->buf, FILE_BUFFER_SIZE);
#endif
		lpIO->nBufCnt = 0;
	}

	return lpIO->buf[lpIO->nBufCnt++];
}


///////////////////////////////////////////////////////////////////////////
//
//	Memory I/O functions
//
///////////////////////////////////////////////////////////////////////////
int memio_open(LPVOID lpIn, unsigned uFlags, LPVOID FAR *lplpHandle)
{
	// We need to properly handle all the uFlags variants, including
	// OF_CREATE, OF_READ, OF_WRITE, & OF_EXIST
	LPMEM_IO pIn, pNew;
	
	// lpIn is a PMEM_IO
	pIn = (LPMEM_IO)lpIn;

	// if we're doing an OF_EXIST, then we'll just return IMG_OK
	// if the mem block already contains data, otherwise we'll
	// return IMG_ERR. 
	//
	// NOTE THAT THIS LIMITS OUR WRITING TO BLOCKS WHICH CONTAIN
	// ONLY DATA OF THE TYPE WE'RE TRYING TO WRITE. i.e., if 
	// we're writing a TIFF image, the block can contain only
	// other TIFF images.
	if (uFlags & OF_EXIST) {
		*lplpHandle = NULL;
		return (pIn->wFlags & MEMIO_EXISTS) ? (IMG_OK) : (IMG_ERR);
	}
	else {
		// if wFlags has the MEMIO_NOALLOC flag set, we'll use the
		// global struct passed in instead of allocating one.
		// It's up to the caller to be sure this is what they want.
		if (pIn->wFlags & MEMIO_NOALLOC) {
			pNew = pIn;
		}
		else {
			pNew = (LPMEM_IO)GlobalAllocPtr(GHND, sizeof(MEM_IO));
			*pNew = *pIn;
		}
		pNew->hpCur = pNew->hpMem;
		*lplpHandle = (LPVOID)pNew;
	}

	return IMG_OK;
}
 
int memio_close(LPVOID lpHandle)
{
	LPMEM_IO pIn = (LPMEM_IO)lpHandle;
	
	if (!(pIn->wFlags & MEMIO_NOALLOC)) GlobalFreePtr(pIn);

	return IMG_OK;
}

DWORD memio_read(LPVOID lpHandle, LPVOID hpBuf, DWORD dwBytes)
{
	LPMEM_IO pIn = (LPMEM_IO)lpHandle;
	DWORD	dwCopy, dwMin;

	// We only want to copy bytes up to the end of the memory buffer.
	// Since lots of reasons can cause us to try to read past the
	// end of a file (and hence, our file-like memory buffer), we'll have
	// to be extra-careful about this.
	dwCopy = dwBytes;
	if ( (pIn->hpCur+dwCopy) > (pIn->hpMem+pIn->dwSize)) {
		dwCopy = (pIn->hpMem+pIn->dwSize) - pIn->hpCur;
		dwBytes = dwCopy;
	}

	dwBytes = dwCopy;
	while (dwCopy) {
		dwMin = min(dwCopy, 65535L);
		_fmemcpy((LPSTR)hpBuf, pIn->hpCur, (WORD)dwMin);
		dwCopy -= dwMin;
		pIn->hpCur += dwMin;
		((HPSTR)hpBuf) += dwMin;
	}

	return dwBytes;
}

DWORD memio_seek(LPVOID lpHandle, LONG lOffset, WORD nOrigin)
{
	LPMEM_IO pIn = (LPMEM_IO)lpHandle;

	//
	// Remember...seeking past the end of the file and then writing
	// at that point will extend the file to that point!
	//
	switch (nOrigin) {
		case SEEK_SET:
			break;
		case SEEK_CUR:
			lOffset = (pIn->hpCur - pIn->hpMem) + lOffset;
			break;
		case SEEK_END:
			lOffset = pIn->dwUsed - lOffset;
			break;
	}

	// lOffset is now the offset into the memory block we want. We can
	// now clamp it to make sure it's within our memory block...
	if (lOffset < 0) lOffset = 0;
	if ((!pIn->wFlags & MEMIO_WRITING) && ((DWORD)lOffset > pIn->dwSize-1)) lOffset = pIn->dwSize-1;

	pIn->hpCur = pIn->hpMem+lOffset;

	return lOffset;
}

#define MEMBLK_GROW_SIZE	(256*1024L)
#define MEMWRITE_MAX	32000
DWORD memio_write(LPVOID lpHandle, LPVOID hpBuf, DWORD dwBytes)
{
	LPMEM_IO pIn = (LPMEM_IO)lpHandle;
	LONG		lNewOffset;	//amount we'll be adding to memory block
	LONG		lCurOffset = pIn->hpCur - pIn->hpMem;
	DWORD		dwCopy;
	DWORD	dwNewSize;

	// if hpMem is NULL, we know there's a problem with
	// the MEM_IO struct, and we'll exit quickly
	if (!pIn->hpMem) return 0;

	//
	// If hpCur points past the end of the memory block, we'll
	// have to extend the memory block.
	//
	// We also have to extend the memory block if we don't have 
	// enough space to put dwBytes into it.
	//

	lNewOffset = lCurOffset + dwBytes;

	if ((DWORD)(lNewOffset+1) > pIn->dwSize) {	// just output the bytes
		dwNewSize = pIn->dwSize;
		while ((DWORD)(lNewOffset+1) > dwNewSize) dwNewSize += MEMBLK_GROW_SIZE;
		pIn->hMem = GlobalReAlloc(pIn->hMem, dwNewSize, GHND);
		if (pIn->hMem) {
			LONG lDiff = pIn->hpCur - pIn->hpMem;

			pIn->hpMem = (HPSTR)GlobalLock(pIn->hMem);
			pIn->hpCur = pIn->hpMem + lDiff;
			pIn->dwSize = dwNewSize;
		}
		else {
			pIn->hpMem = NULL;
			pIn->dwSize = 0;
			pIn->dwUsed = 0;
			pIn->hpCur = 0;
			pIn->wFlags = 0;
			dwBytes = 0;
			return 0;	//this is an error!
		}
	}

	dwCopy = dwBytes;
	while (dwCopy) {
		_fmemcpy(pIn->hpCur, hpBuf, (int)min(MEMWRITE_MAX, dwCopy));
		pIn->hpCur += min(MEMWRITE_MAX, dwCopy);
		(HPSTR)hpBuf += min(MEMWRITE_MAX, dwCopy);
		dwCopy -= min(MEMWRITE_MAX, dwCopy);
	}

	if ( (DWORD)(pIn->hpCur - pIn->hpMem) > pIn->dwUsed) 
		pIn->dwUsed = (pIn->hpCur - pIn->hpMem);

	return dwBytes;
}

int memio_error(LPVOID lpHandle, LPSTR lpErrBuf)
{
	return 0;

}

int memio_getchar(LPVOID lpHandle)
{
	LPMEM_IO pIn = (LPMEM_IO)lpHandle;
	int	retval;

	if (pIn->hpCur < (pIn->hpMem+pIn->dwSize)) {
		retval = *pIn->hpCur;
		pIn->hpCur++;
		return retval;
	}
	else return -1;
}
