/////////////////////////////////////////////////////////////////////////////
//
// pri.c
//
// PRI DIL Main Module
//
// ImageMan Image Processing Library
//
// Copyright (c) 1991-95 Data Techniques, Inc.
// All Rights Reserved
//
// ///////////////////////////////////////////////////////////////////////////
// $History: PRI.C $
// 
// *****************  Version 1  *****************
// User: Sygsky from JHC Date: 04-SEP-2000    Time: 16:31
// Created in $/ImageMan 16-32/DILS
// ImageMan 4.0 Beta 1

#include "internal.h"
#pragma hdrstop
#include <assert.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <stdio.h>
#include <memory.h>
#include "lzw.h"
#include "prif.h"
#include "pri.h"

#define CCITT_MKTABLES 0
#define TBLENTRIES     105
#define VERSION        "PRI File Reader V1.2"


int         unCCITT(FileStruct FAR *, HPSTR, int);
int         unPackBits(FileStruct FAR *, HPSTR, int);
int         unCCITT2D(FileStruct FAR *, LPSTR, int);	// un2d.c

void        initCCITT(void);
void        initBBuf(FileStruct FAR *, int);	// un2d.c

static int  fill_palette(LPBITMAPINFO, FileStruct FAR *);
static int  read_row(FileStruct FAR *, HPSTR, HPSTR, UINT, UINT, BOOL);
static int	read_tile_rows(FileStruct FAR *, HPSTR, LPSTR, unsigned, unsigned, unsigned, LPSTATUS, HGLOBAL);
static int  getDecodedRow(FileStruct FAR *, HPSTR, UINT, UINT, BOOL);
static int  fill_palette(LPBITMAPINFO lpbi, FileStruct FAR * fs);
static int  ReadPlanarRow(FileStruct FAR *, HPSTR);
static long stripLoc(UINT strip, FileStruct FAR * fs);
static long tileLoc(UINT tile, FileStruct FAR * fs);
static int  set_row(FileStruct FAR * fs, unsigned row);
static void swapColors(HPSTR lpBuf, int nLen, int nBits);
static void flipBufOrder(FileStruct FAR *);
static int  GetStripNo(FileStruct FAR * fs, UINT row);
static int  GetTileNo(FileStruct FAR * fs, UINT row, UINT col);
static int	GetLPStatus (LPSTATUS lpStat, unsigned, LPSTR, HGLOBAL);

void CMYKToRGB ( FileStruct FAR * fs, HPSTR buf, HPSTR scratch, UINT len) ;

// These tables are shared with un2d.c
UCHAR const wlen[TBLENTRIES] = {// white code bit-length
	8, 6, 4, 4, 4, 4, 4, 4,
	5, 5, 5, 5, 6, 6, 6, 6,
	6, 6, 7, 7, 7, 7, 7, 7,
	7, 7, 7, 7, 7, 8, 8, 8,
	8, 8, 8, 8, 8, 8, 8, 8,
	8, 8, 8, 8, 8, 8, 8, 8,
	8, 8, 8, 8, 8, 8, 8, 8,
	8, 8, 8, 8, 8, 8, 8, 8,
	5, 5, 6, 7, 8, 8, 8, 8,
	8, 8, 9, 9, 9, 9, 9, 9,
	9, 9, 9, 9, 9, 9, 9, 9,
	9, 6, 9,
	11, 11, 11, 12, 12, 12, 12, 12,
	12, 12, 12, 12, 12, 12
};

UCHAR const blen[TBLENTRIES] = {// black code bit-length
	10, 3, 2, 2, 3, 4, 4, 5,
	6, 6, 7, 7, 7, 8, 8, 9,
	10, 10, 10, 11, 11, 11, 11, 11,
	11, 11, 12, 12, 12, 12, 12, 12,
	12, 12, 12, 12, 12, 12, 12, 12,
	12, 12, 12, 12, 12, 12, 12, 12,
	12, 12, 12, 12, 12, 12, 12, 12,
	12, 12, 12, 12, 12, 12, 12, 12,
	10, 12, 12, 12, 12, 12, 12, 13,
	13, 13, 13, 13, 13, 13, 13, 13,
	13, 13, 13, 13, 13, 13, 13, 13,
	13, 13, 13,
	11, 11, 11, 12, 12, 12, 12, 12,
	12, 12, 12, 12, 12, 12
};

// These tables are only needed when generating the white/black tables.
#if CCITT_MKTABLES
#define CCITT_INIT  initCCITT()

UCHAR       whiteTab[4196];	  // white codes are <= 12 bits

UCHAR       blackTab[8192];	  // black codes are <= 13 bits

// Values used to setup "whiteTab" and "blackTab" arrays.
static UINT const wcodes[TBLENTRIES] = {
	0x3500, 0x1c00, 0x7000, 0x8000, 0xb000, 0xc000, 0xe000, 0xf000,
	0x9800, 0xa000, 0x3800, 0x4000, 0x2000, 0x0c00, 0xd000, 0xd400,
	0xa800, 0xac00, 0x4e00, 0x1800, 0x1000, 0x2e00, 0x0600, 0x0800,
	0x5000, 0x5600, 0x2600, 0x4800, 0x3000, 0x0200, 0x0300, 0x1a00,
	0x1b00, 0x1200, 0x1300, 0x1400, 0x1500, 0x1600, 0x1700, 0x2800,
	0x2900, 0x2a00, 0x2b00, 0x2c00, 0x2d00, 0x0400, 0x0500, 0x0a00,
	0x0b00, 0x5200, 0x5300, 0x5400, 0x5500, 0x2400, 0x2500, 0x5800,
	0x5900, 0x5a00, 0x5b00, 0x4a00, 0x4b00, 0x3200, 0x3300, 0x3400,
	0xd800, 0x9000, 0x5c00, 0x6e00, 0x3600, 0x3700, 0x6400, 0x6500,
	0x6800, 0x6700, 0x6600, 0x6680, 0x6900, 0x6980, 0x6a00, 0x6a80,
	0x6b00, 0x6b80, 0x6c00, 0x6c80, 0x6d00, 0x6d80, 0x4c00, 0x4c80,
	0x4d00, 0x6000, 0x4d80,
	0x0100, 0x0180, 0x01a0, 0x0120, 0x0130, 0x0140, 0x0150, 0x0160,
	0x0170, 0x01c0, 0x01d0, 0x01e0, 0x01f0, 0x0010
};

static UINT const bcodes[TBLENTRIES] = {
	0x0dc0, 0x4000, 0xc000, 0x8000, 0x6000, 0x3000, 0x2000, 0x1800,
	0x1400, 0x1000, 0x0800, 0x0a00, 0x0e00, 0x0400, 0x0700, 0x0c00,
	0x05c0, 0x0600, 0x0200, 0x0ce0, 0x0d00, 0x0d80, 0x06e0, 0x0500,
	0x02e0, 0x0300, 0x0ca0, 0x0cb0, 0x0cc0, 0x0cd0, 0x0680, 0x0690,
	0x06a0, 0x06b0, 0x0d20, 0x0d30, 0x0d40, 0x0d50, 0x0d60, 0x0d70,
	0x06c0, 0x06d0, 0x0da0, 0x0db0, 0x0540, 0x0550, 0x0560, 0x0570,
	0x0640, 0x0650, 0x0520, 0x0530, 0x0240, 0x0370, 0x0380, 0x0270,
	0x0280, 0x0580, 0x0590, 0x02b0, 0x02c0, 0x05a0, 0x0660, 0x0670,
	0x03c0, 0x0c80, 0x0c90, 0x05b0, 0x0330, 0x0340, 0x0350, 0x0360,
	0x0368, 0x0250, 0x0258, 0x0260, 0x0268, 0x0390, 0x0398, 0x03a0,
	0x03a8, 0x03b0, 0x03b8, 0x0290, 0x0298, 0x02a0, 0x02a8, 0x02d0,
	0x02d8, 0x0320, 0x0328,
	0x0100, 0x0180, 0x01a0, 0x0120, 0x0130, 0x0140, 0x0150, 0x0160,
	0x0170, 0x01c0, 0x01d0, 0x01e0, 0x01f0, 0x0010
};
#else
#define CCITT_INIT  ((void)0) // no init necessary
#include "ccitt.h"            // include pre-generated CCITT black/white tables
#endif

#define UNCCITT unCCITT      // Always use 'PUREC' version

// byte mask for writing CCITT black pixels
UCHAR const runmask[] = {0x00, 0x80, 0xC0, 0xE0, 0xF0, 0xF8, 0xFC, 0xFE};

// These must be exported to "tiff.c"
#ifdef WIN32

#ifdef WINGIS // so the file TLSByJHC.h was included to "internal.h"

#define SETERROR(E) bJHCTLSSetValue((DWORD)(E))
#define GETERROR    nJHCTLSGetValue()

#else

DWORD       TlsIndex;			  // Thread local storage index
#define SETERROR(E) TlsSetValue(TlsIndex,(LPVOID)(DWORD)(E))
#define GETERROR    ((DWORD)TlsGetValue(TlsIndex))

#endif

#else
int         nErrID;				  // id of latest error
#define SETERROR(E) nErrID = (E)
#define GETERROR    (nErrID)
#endif

static HINSTANCE tiffInstance;
#if DEBUG
static DEBUGPROC DebugStringOut;
#endif
#ifndef WIN32
static int  (FAR PASCAL * lpfnUnCCITT) (int, LPSTR, int, int, int, int);
#endif

#ifdef WIN32
// /////////////////////////////////////////////////////////////////////////
//
// DllMain
//
// This is the Win32 'DllEntryPoint'. It is similar in function to LibMain
// and the WEP in 16-bit land, but is much more thorough. LibMain is
// called whenever a process or thread attaches or detaches to the
// BMP DIL
//
// Entry:
// hinstDLL  - handle to the ImageMan DLL
// fdwReason - notification code (attach/detach)
// DLL_PROCESS_ATTACH
// DLL_PROCESS_DETACH
// DLL_THREAD_ATTACH
// DLL_THREAD_DETACH
// lpvReserved - NULL during dynamic process attach/detach, non-NULL
// during static attach/detach.
// Exit:
// DLL_PROCESS_ATTACH - TRUE for success, FALSE for failure
// all others         - return code ignored
// /////////////////////////////////////////////////////////////////////////

BOOL APIENTRY DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
	switch (fdwReason) {
			// The DLL is attaching to a process, due to LoadLibrary.
		case DLL_PROCESS_ATTACH:
			assert(!lpvReserved);  // must always be loaded via LoadLibrary!

			tiffInstance = hinstDLL;

			CCITT_INIT;

#ifndef WINGIS
			// Allocate a "Thread Local Storage" (TLS) index right away.
			if ((TlsIndex = TlsAlloc()) == 0xFFFFFFFF)
				return (FALSE);
#else
			bJHCTLSClear();
#endif
			// Fall thru: Initialize the index for first thread.
			
			// The attached process created a new thread.
		case DLL_THREAD_ATTACH:
#ifndef WINGIS
			// Initialize the TLS index for this thread... it's actually 'nErrID'
			if (!TlsSetValue(TlsIndex, (LPVOID) 0))
				return (FALSE);
#else
			nJHCTLSAlloc(); // Stamp this thread to be existed
#endif
			break;
		
			// The thread of the attached process terminates.
		case DLL_THREAD_DETACH:
#ifdef WINGIS
			bJHCTLSFree(); // Free this thread entry
#endif
			break;

			// The DLL is detaching from a process, due to a call to FreeLibrary.
		case DLL_PROCESS_DETACH:
#ifndef WINGIS
			// Release the TLS index.
			TlsFree(TlsIndex);
#else
			bJHCTLSClear();  // Release all the threads error descriptors
#endif
			break;
	}

	return (TRUE);

	UNREFERENCED_PARAMETER(lpvReserved);
}

#else // WIN32

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int WINAPI  LibMain(HINSTANCE hInstance, WORD wDataSeg, WORD wHeapSize, LPSTR lpCmdLine)
{
	if (!wHeapSize)
		return (0);					  // no heap indicates fatal error

#if DEBUG
	{
		HANDLE      hMod = GetModuleHandle("IMGMAN2.DLL");
		if (hMod)
			DebugStringOut = (DEBUGPROC) GetProcAddress(hMod, "DebugStringOut");
	}
#endif

	tiffInstance = hInstance;

	CCITT_INIT;

	return (1);
}
#endif // WIN32

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
void WINAPI WEP(int bSystemExit)
{
	return;
	UNREFERENCED_PARAMETER(bSystemExit);
}

//------------------------------------------------------------------

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
BOOL IMAPI  IMDLverify_img(LPSTR lpVerfBuf, LPVOID lpHandle, LPIMG_IO lpIO)
{
	return (*((USHORT FAR *) lpVerfBuf) == VS);
	UNREFERENCED_PARAMETER(lpHandle);
	UNREFERENCED_PARAMETER(lpIO);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
#pragma optimize ("",off)

int IMAPI   IMDLset_page(HANDLE hImage, int nPage)
{
	FileStruct FAR *fs;
	int         retval = IMG_OK;

	if (!(fs = (FileStruct FAR *) GlobalLock(hImage))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}
	if (nPage < 0 || (unsigned int) nPage >= fs->tiffHead.Pages) {
		SETERROR(IDS_INVPAGE);
		retval = IMG_INV_PAGE;
	} 
	else if (fs->nCurPage == nPage) {
		GlobalUnlock(hImage);
		return retval;
	}
	else if (fs->nCurPage > nPage) {
		// seek to first page
		retval = SeekToFirstPage(fs->lpHandle, fs->lpIO, fs->offset, &fs->tiffHead.ByteOrder, &fs->lCurIFDOffset);
		fs->nCurPage = 0;
	}
	
	while (fs->nCurPage < nPage) {
		NextPage(fs->lpHandle, fs->lpIO, fs->offset, &fs->lCurIFDOffset, (fs->tiffHead.ByteOrder == MM));
		fs->nCurPage++;
	}

	if (retval == IMG_OK) {

		// Free any arrays allocated for previous pages data
		if (fs->tiffHead.StripOffsets)
			GlobalFree(fs->tiffHead.StripOffsets);

		if (fs->tiffHead.TileOffsets)
			GlobalFree(fs->tiffHead.TileOffsets);

		if (fs->tiffHead.ColorMap) 
			GlobalFree(fs->tiffHead.ColorMap);

		retval = ReadTiffHead(fs->lpHandle, fs->lpIO, &(fs->tiffHead), fs->offset);
	}
	fs->row = 0;				  // next row to read is 1st row in image

	LZWCloseContext(fs->lpLZW);
	fs->lpLZW = LZWOpenContext(9, 1, fs->tiffHead.ImageWidth, fs->tiffHead.BytesLine, 1);

	// reset buffer
	fs->wbufpos = fs->wbufsize;

	GlobalUnlock(hImage);

	return retval;
}
#pragma optimize ("",on)

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLget_page(HANDLE hImage, LPINT nPage)
{
	FileStruct FAR *fs;

	if (!(fs = (FileStruct FAR *) GlobalLock(hImage))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}
	*nPage = fs->nCurPage;

	GlobalUnlock(hImage);
	return IMG_OK;
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLescape(HANDLE hImage, int nInCnt, LPVOID lpIn, LPVOID lpOut)
{
	return IMG_NSUPPORT;
	UNREFERENCED_PARAMETER(hImage);
	UNREFERENCED_PARAMETER(nInCnt);
	UNREFERENCED_PARAMETER(lpIn);
	UNREFERENCED_PARAMETER(lpOut);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
LPSTR IMAPI IMDLget_ver(void)
{
	return (LPSTR) (VERSION);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLset_ext(HGLOBAL hFile, LPSTR ext)
{
	// no extensions to differentiate between - just return OK
	DEBUGOUT(DILAPI, "\t%s-->set_ext(%d,%s)", (LPSTR) __FILE__, hFile, (LPSTR) ext);
	return IMG_OK;
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLinit_image(LPCSTR lpOpenInfo, LPHANDLE hFile, LPVOID lpHandle, long offset, long lLen, LPIMG_IO lpIO)
{
	FileStruct FAR *fs;
	TiffStruct  ts;
	int         retval;
	DWORD       fsmem, bufmem;
	ULONG		lCurIFDOffset;
	long		bufDimValue;

//	DEBUGOUT(DILAPI, "\t%s-->init_image(%s,%x:%x,%d,%ld,%ld)", (LPSTR) __FILE__, (LPSTR) file, HIWORD(hFile), LOWORD(hFile), offset, lLen);

	// go to the first page
	if ((retval = SeekToFirstPage(lpHandle, lpIO, offset, &ts.ByteOrder, &lCurIFDOffset)) != IMG_OK) {
		lpIO->close(lpHandle);
		return (retval);
	}
	if ((retval = ReadTiffHead(lpHandle, lpIO, &ts, offset)) != IMG_OK) {
		lpIO->close(lpHandle);
		return (retval);
	}
	// calculate buffer dimension value component depending on tiles or strips
	bufDimValue = (ts.TileWidth) ? ts.TileWidth : ts.ImageWidth;

	// We may need to allocate extra buffers for different compressions
	fsmem = sizeof(FileStruct);
	bufmem = WBUFSIZE;
//	if (ts.Compression == PACKBITS) {
		// Ensure the read buffer is large enough for a worst case scanline.
		if (2 * bufDimValue > WBUFSIZE) {
			bufmem = (2 * bufDimValue + 3) & ~3;	// DWORD aligned!

		}
//	} else if (ts.Compression == FAXCCITT4) {
		// Group 4 decompression requires 2 buffers, each long enough for a worst
		// case scanline (alternating black & white pixels).
		fsmem += (bufDimValue * 2L + 2) * 2 * sizeof(int);
//	}
	// create new internal structure
	if (!(*hFile = GlobalAlloc(DLLHND, fsmem + bufmem))) {
		lpIO->close(lpHandle);
		SETERROR(IDS_MEMERR);
		return IMG_NOMEM;
	}
	// fill in structure
	fs = (FileStruct FAR *) GlobalLock(*hFile);
	fs->tiffHead = ts;
	fs->lpHandle = lpHandle;
	fs->lpIO = lpIO;
	fs->offset = offset;
	fs->row = 0;							// next row to read is 1st row in image
	fs->lpOpenInfo = (LPVOID)lpOpenInfo;	//store info needed to open the image later
	fs->lCurIFDOffset = lCurIFDOffset;
	fs->wbufpos = fs->wbufsize = (WORD)bufmem;
	fs->bBuf2Offset = bufDimValue * sizeof(int) + 2;	// Group 4 2nd buffer offset

	if ((retval = CountPages(lpHandle, lpIO, offset, &(fs->tiffHead.Pages), ts.ByteOrder == MM)) != IMG_OK) {
		lpIO->close(lpHandle);
		GlobalFree(*hFile);
		return (retval);
	}

	if (fs->tiffHead.TileWidth)	
		lpIO->seek(fs->lpHandle, tileLoc(0, fs), SEEK_SET);
	else
		lpIO->seek(fs->lpHandle, stripLoc(0, fs), SEEK_SET);
	

	// if it's an LZW compressed image, we have to setup LZW context struct
	// if (fs->tiffHead.Compression == LZW) {
	fs->lpLZW = LZWOpenContext(9, 1, fs->tiffHead.ImageWidth, fs->tiffHead.BytesLine, 1);
	// }
	// Sygsky: initialize bufers to allow set_row as first call to image with not zero row index
	// Reinit our code buffers since we are starting from the image top
	if (fs->tiffHead.Compression == FAXCCITT4) {
		fs->nBits = 0;
		fs->lCodes = 0;
		initBBuf(fs, fs->tiffHead.ImageWidth);
	}
	
#if DEBUG
	DEBUGOUT(IMGDETAIL, "\t\tIntel Byte Order");
	switch (fs->tiffHead.Compression) {
		case NOCOMPRESSBYTE:
			DEBUGOUT(IMGDETAIL, "\t\t%s", (LPSTR) "No Compression");
			break;
		case CCITT3:
			DEBUGOUT(IMGDETAIL, "\t\t%s", (LPSTR) "Modified Huffman");
			break;
		case FAXCCITT3:
			DEBUGOUT(IMGDETAIL, "\t\t%s", (LPSTR) "Group 3");
			break;
		case FAXCCITT4:
			DEBUGOUT(IMGDETAIL, "\t\t%s", (LPSTR) "Group 4");
			break;
		case LZW:
			DEBUGOUT(IMGDETAIL, "\t\tLZW, Predictor = %d", fs->tiffHead.Predictor);
			break;
		case PACKBITS:
			DEBUGOUT(IMGDETAIL, "\t\t%s", (LPSTR) "PACKBITS");
			break;
	}
	DEBUGOUT(IMGDETAIL, "\t\tBits/Sample: %d", fs->tiffHead.GrayBits);
	DEBUGOUT(IMGDETAIL, "\t\tSamples/Pixel: %d", fs->tiffHead.SamplesPerPixel);
	DEBUGOUT(IMGDETAIL, "\t\tStrips: %d", fs->tiffHead.StripsPerImage);
	DEBUGOUT(IMGDETAIL, "\t\tRows/Strip: %d", fs->tiffHead.RowsPerStrip);
	DEBUGOUT(IMGDETAIL, "\t\tFillOrder: %d", fs->tiffHead.FillOrder);
	DEBUGOUT(IMGDETAIL, "\t\tPhotoMetric Interp: %d", fs->tiffHead.WhiteValue);
#endif

	GlobalUnlock(*hFile);

	return IMG_OK;
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLclose_image(HGLOBAL hFile)
{
	FileStruct FAR *fs;

	DEBUGOUT(DILAPI, "\t%s-->close_image(%d)", (LPSTR) __FILE__, hFile);
	// check to see if file is still open - if it is, close it
	if (!(fs = (FileStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}

	if (fs->lpHandle) {
		fs->lpIO->close(fs->lpHandle);
		fs->lpHandle = NULL;
	}

	if (fs->tiffHead.StripOffsets)
		GlobalFree(fs->tiffHead.StripOffsets);

	if (fs->tiffHead.TileOffsets)
		GlobalFree(fs->tiffHead.TileOffsets);

	if (fs->tiffHead.ColorMap) GlobalFree(fs->tiffHead.ColorMap);

	// if it's an LZW compressed image, we have to close LZW context struct
	// if (fs->tiffHead.Compression == LZW) {
	LZWCloseContext(fs->lpLZW);
	// }

	GlobalUnlock(hFile);
	GlobalFree(hFile);
	return IMG_OK;
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLimg_name(HGLOBAL hFile, LPHANDLE lpHand)
{
	FileStruct FAR *fs;

	if (!(fs = (FileStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}
//	*lpHand = fs->fName;

	GlobalUnlock(hFile);
	return IMG_OK;
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLopen(HGLOBAL hFile, LPVOID lpHandle)
{
	FileStruct FAR *fs;

//	DEBUGOUT(DILAPI, "\t%s-->open(%d,%d)", (LPSTR) __FILE__, hFile, fHand);
	if (!(fs = (FileStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}
	if (!fs->lpHandle) {
		if (fs->lpIO->open(fs->lpOpenInfo, OF_READ, &fs->lpHandle) != IMG_OK) {
			GlobalUnlock(hFile);
			SETERROR(IDS_NOOPEN);
			return IMG_NOFILE;
		}
	}
	// seek to first row in image
	if (fs->tiffHead.TileWidth) 
		fs->lpIO->seek(fs->lpHandle, tileLoc(0, fs), SEEK_SET);	
	else
		fs->lpIO->seek(fs->lpHandle, stripLoc(0, fs), SEEK_SET);	

	// if it's an LZW compressed image, we have to setup LZW context struct
	// if (fs->tiffHead.Compression == LZW) {
	LZWResetContext(fs->lpLZW);
	// }

	// reset buffer
	fs->wbufpos = fs->wbufsize;

	GlobalUnlock(hFile);

	return IMG_OK;
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLclose_keep(HGLOBAL hFile)
{
	FileStruct FAR *fs;

	DEBUGOUT(DILAPI, "\t%s-->close_keep(%d)", (LPSTR) __FILE__, hFile);
	if (!(fs = (FileStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}
	if (fs->lpHandle) {
		fs->lpIO->close(fs->lpHandle);
		fs->lpHandle = NULL;
	}
	GlobalUnlock(hFile);
	return IMG_OK;
}

// #pragma optimize ("",off)  // CAR: Why???
///////////////////////////////////////////////////////////////////////////
//
///////////////////////////////////////////////////////////////////////////
int IMAPI IMDLread_rows(HGLOBAL hFile,LPSTR buf,UINT rows,UINT rowSize, UINT start, LPSTATUS lpStat) {
	FileStruct FAR *fs;
	LPSTR       scratch;
	HPSTR       tmpBuf;
	int         tmp, strip;
	BOOL		needscratch = FALSE;
	ULONG		bufDimValue;

	DEBUGOUT(DILAPI, "\t%s-->%s(%d, %x:%x, %u, %u, %u, %x:%x)", (LPSTR) __FILE__, (LPSTR) "read_rows", hFile, HIWORD(buf), LOWORD(buf), rows, rowSize, start, HIWORD(lpStat), LOWORD(lpStat));
	if          (!(fs = (FileStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}

	// calculate buffer dimension value component depending on tiles or strips
	bufDimValue = (fs->tiffHead.TileWidth) ? fs->tiffHead.TileBytesLine : fs->tiffHead.BytesLine;

	// create the scratch buffer 
	// IF loading partial scanlines 
	// OR if the input pixel size differs from the output pixel size 
	// OR if this is a compressed tiled image. The tiled image scratch buffer will be one tile byte row
	// OR requested rowsize goes in to last tile column AND last tile column is padded (whew!)
	
	if (start < rowSize)		// prevent UINT wrap
	{
		if ((rowSize - start)  < (UINT)fs->lRowSize)	needscratch = TRUE;
	}
	else 
	{
		if (rowSize  < (UINT)fs->lRowSize) 	needscratch = TRUE;
	}

	if ((UINT)fs->nPixBits != (fs->tiffHead.GrayBits * fs->tiffHead.SamplesPerPixel)) needscratch = TRUE;
	if ((fs->tiffHead.TileWidth) && (fs->tiffHead.Compression != NOCOMPRESSBYTE)) needscratch = TRUE;
	if	  ((fs->tiffHead.TileWidth) 
		&& ((start + rowSize > (fs->tiffHead.TilesAcross -1) * fs->tiffHead.TileBytesLine))
		&& (fs->tiffHead.TilesAcross * fs->tiffHead.TileWidth > fs->tiffHead.ImageWidth)) needscratch = TRUE;
		
	if (needscratch) {
		if (!(scratch = GlobalAllocPtr(DLLHND,  bufDimValue  + 3))) {
			SETERROR(IDS_MEMERR);
			return (IMG_NOMEM);
		}
	} 
	else {
		scratch = NULL;				
	}

	tmpBuf = (HPSTR) buf + (rows - 1) * (long) rowSize;	// start at beginning of last row in buffer

	if (fs->tiffHead.TileWidth)	{						// if this is a tiled image
		if ((tmp = read_tile_rows(fs, tmpBuf, scratch, start, rowSize, rows, lpStat, hFile)) != IMG_OK) {
			return tmp;
		}
	}
	else {												// if this is a stripped image
		while (rows--) {
#if DEBUG
			DEBUGOUT(DILSETLINE, (LPSTR) (&rows), NULL);
			DEBUGOUT(DILAPI, "\t%sRows = %d, lCodes = %lx", (LPSTR) __FILE__, rows, fs->lCodes);
#endif
			if((tmp = read_row(fs, tmpBuf, scratch, start, rowSize, FALSE)) != IMG_OK) {

#if DEBUG
					DEBUGOUT(DILERR, "\t\t%s: read_row error, rows=%d: %d", (LPSTR) __FILE__, rows, tmp);
#endif
					return tmp;
			}

#if DEBUG
			if (*((LPSTR) ((LONG) buf & 0xFFFF0000)) != 40)
				DEBUGOUT(DILERR, "Wrap-Error, rows = %d, tmpBuf = %x:%x", rows, HIWORD(tmpBuf), LOWORD(tmpBuf));
#endif

			tmpBuf -= rowSize;
			fs->row++;
			
			if ((fs->tiffHead.StripsPerImage > 1) && (fs->row % fs->tiffHead.RowsPerStrip == 0)) {
				strip = (int) ((fs->row) / fs->tiffHead.RowsPerStrip);
				fs->lpIO->seek(fs->lpHandle, stripLoc(strip, fs), SEEK_SET);
				fs->wbufpos = fs->wbufsize;	// reset read buffer

				fs->lCodes = 0;
				fs->nBits = 0;
				LZWResetContext(fs->lpLZW);
				initBBuf(fs, fs->tiffHead.ImageWidth);
			}
			if (GetLPStatus(lpStat, rowSize, scratch, hFile) == IMG_CANCELLED) return IMG_CANCELLED;
		}
	}													// end stripped image 
	if (scratch)
		GlobalFreePtr(scratch);
	GlobalUnlock(hFile);
	return (IMG_OK);
}


static int GetLPStatus (LPSTATUS lpStat, unsigned rowSize, LPSTR scratch, HGLOBAL hFile)
{
	if (lpStat) {
		lpStat->lAcc += rowSize;
		if (lpStat->lpfnStat && lpStat->lAcc > lpStat->lInterval) {
			lpStat->lDone += lpStat->lAcc;
			lpStat->lDone = min(lpStat->lDone, lpStat->lTotal);
			
			if( !((*lpStat->lpfnStat) (lpStat->hImage, (int) ((lpStat->lDone * 100) / lpStat->lTotal), lpStat->dwUserInfo) )) {
				if (scratch)
					GlobalFreePtr(scratch);

				GlobalUnlock(hFile);
				return IMG_CANCELLED;
			}
								
			lpStat->lAcc %= lpStat->lInterval;
		}
	}
	return IMG_OK;
}

//#pragma optimize ("",on)
//#pragma optimize ("ge",off)

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLset_row(HGLOBAL hFile, UINT row)
{
	FileStruct FAR *fs;
	int         retval;

	DEBUGOUT(DILAPI, "\t%s-->set_row(%d,%d)", (LPSTR) __FILE__, hFile, row);
	if (!(fs = (FileStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}

	if (fs->tiffHead.TileWidth) {			// if this is a tiled image
		fs->nBits = 0;
		fs->lCodes = 0;
		initBBuf(fs, fs->tiffHead.TileWidth);
		LZWResetContext(fs->lpLZW);
		fs->wbufpos = fs->wbufsize;			// Reset our buffer counter
		fs->row = row;						//	for a tiled image, read_tile_rows takes care of file pointer
		retval = IMG_OK;
	}
	else 									// if this is a stripped image
		retval = set_row(fs, row);			//  use set_row to position file pointer

	GlobalUnlock(hFile);

	return retval;
}

static int  GetStripNo(FileStruct FAR * fs, UINT row)
{
	int strip;

	if (fs->tiffHead.StripsPerImage > 1 && row >= fs->tiffHead.RowsPerStrip) {
		strip = (int) row / fs->tiffHead.RowsPerStrip;
	} else
		strip = 0;
	
	return strip;
}

static int  GetTileNo(FileStruct FAR * fs, UINT row, UINT col)
{	// return tile number given the pixel row and column byte
	int tile, tilesWide;

	if (fs->tiffHead.TilesPerImage > 1 ) {			// figure how many tiles wide
		tilesWide = (int)(fs->tiffHead.ImageWidth / fs->tiffHead.TileWidth) +	
									(fs->tiffHead.ImageWidth % fs->tiffHead.TileWidth > 0);
		tile = ((int)(row / fs->tiffHead.TileLength) * tilesWide) + 
									(int)(col / fs->tiffHead.TileBytesLine);
	} else
		tile = 0;
	
	return tile;
}

static int  set_row(FileStruct FAR * fs, UINT row)
{
	// use StripOffsets to position to start of strip containing desired row
	int         strip, rowCnt, errVal;
	int			CurStrip;
	long        offset;

	
	// If the row we want is greater than/equal to the current row and its in the same
	// strip then we can just skip ahead to the desired row otherwise we need to go to the appropriate
	// strip and then seek to the desired row from there.

	// get offset of strip containing desired row
	strip = GetStripNo( fs, row );

	CurStrip = GetStripNo( fs, fs->row );

	if( row && row >= fs->row  && CurStrip == strip ) {
		rowCnt = row - fs->row;

		// Skip delta # of rows - No Need to seek back to start of strip

		if (fs->tiffHead.Compression == NOCOMPRESSBYTE) {
			// Just seek if source isn't compressed.
			fs->lpIO->seek(fs->lpHandle, (long)fs->tiffHead.BytesLine * rowCnt, SEEK_CUR);
		} else {
			LPSTR       scratch;

			// create a scratch buffer
			if (!(scratch = GlobalAllocPtr(DLLHND, fs->tiffHead.BytesLine + 3))) {
				SETERROR(IDS_MEMERR);
				return (IMG_NOMEM);
			}
			while (rowCnt--) {
				if ((errVal = getDecodedRow(fs, scratch, 0, 0, FALSE)) != IMG_OK)
					return (errVal);
			}
			GlobalFreePtr(scratch);
		}

		fs->row = row;

		return (IMG_OK);
	}

	// Reinit our code buffers since we are switching strips
	fs->nBits = 0;
	fs->lCodes = 0;

	initBBuf(fs, fs->tiffHead.ImageWidth);
	LZWResetContext(fs->lpLZW);

	offset = stripLoc(strip, fs);
	
	fs->wbufpos = fs->wbufsize;		// Reset our buffer counter

	// Move to appropriate strip...
	fs->lpIO->seek(fs->lpHandle, offset, SEEK_SET);

	// ...then move inside strip until row we want is next.
	rowCnt = (int) (row % fs->tiffHead.RowsPerStrip);
	if (fs->tiffHead.Compression == NOCOMPRESSBYTE) {
		// Just seek if source isn't compressed.
		fs->lpIO->seek(fs->lpHandle, (long)fs->tiffHead.BytesLine * rowCnt, SEEK_CUR);
	} else {
		LPSTR       scratch;

		// create a scratch buffer
		if (!(scratch = GlobalAllocPtr(DLLHND, fs->tiffHead.BytesLine + 3))) {
			SETERROR(IDS_MEMERR);
			return (IMG_NOMEM);
		}
		while (rowCnt--) {
			if ((errVal = getDecodedRow(fs, scratch, 0,  0, FALSE)) != IMG_OK)
				return (errVal);
		}
		GlobalFreePtr(scratch);
	}
	fs->row = row;

	return (IMG_OK);
}



//#pragma optimize ("",on)


// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLbuildDIBhead(HGLOBAL hFile, INTERNALINFO FAR * lpii)
{
	LPBITMAPINFO lpbi;
	BITMAPINFOHEADER bi;
	FileStruct FAR *fs;
	UINT        nDIBits;
	HGLOBAL     hMem;

	DEBUGOUT(DILAPI, "\t%s-->buildDIBhead(%d)", (LPSTR) __FILE__, hFile);
	if (!(fs = (FileStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}
	nDIBits = fs->tiffHead.GrayBits * fs->tiffHead.SamplesPerPixel;

	// force bit-depth to valid DIB value
	if (nDIBits > 8)
		nDIBits = 24;
	else if (nDIBits > 4)
		nDIBits = 8;
	else if (nDIBits > 1)
		nDIBits = 4;

	fs->nPixBits = nDIBits;

	bi.biClrUsed = (1 << nDIBits) & 0x01ff;

	bi.biSize = sizeof(BITMAPINFOHEADER);
	bi.biWidth = fs->tiffHead.ImageWidth;
	bi.biHeight = fs->tiffHead.ImageLength;
	bi.biPlanes = 1;				  // Always

	bi.biBitCount = nDIBits;
	bi.biCompression = BI_RGB;
	bi.biSizeImage = ((bi.biWidth * nDIBits + 31) / 32 * 4) * bi.biHeight;
	bi.biXPelsPerMeter = fs->tiffHead.ResolutionUnit == INCHES ?
		(LONG) (fs->tiffHead.XRezNumerator * 39.37) / fs->tiffHead.XRezDenominator :
		(LONG) (fs->tiffHead.XRezNumerator * 100) / fs->tiffHead.XRezDenominator;
	bi.biYPelsPerMeter = fs->tiffHead.ResolutionUnit == INCHES ?
		(LONG) (fs->tiffHead.YRezNumerator * 39.37) / fs->tiffHead.YRezDenominator :
		(LONG) (fs->tiffHead.YRezNumerator * 100) / fs->tiffHead.YRezDenominator;
	bi.biClrImportant = 0;

	lpii->lOrgWid = bi.biWidth;
	lpii->lOrgHi = bi.biHeight;
	lpii->bFlags = 0;
	lpii->nPages = fs->tiffHead.Pages;

	fs->lRowSize = ROWSIZE(bi.biWidth , bi.biBitCount);

	if (!(hMem = GlobalAlloc(GHND, sizeof(BITMAPINFOHEADER) + PALETTESIZE(&bi)))) {
		SETERROR(IDS_MEMERR);
		return IMG_NOMEM;
	}
	lpbi = (LPBITMAPINFO) GlobalLock(hMem);
	*(LPBITMAPINFOHEADER) lpbi = bi;

	// now fill in the palette information
	fill_palette(lpbi, fs);

	lpii->lpbi = lpbi;

	GlobalUnlock(hFile);
	return IMG_OK;
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLprint(HGLOBAL hFile, HDC hPrnDC, LPRECT lpDest, LPRECT lpSrc)
{
	SETERROR(IDS_NOPRINT);
	return IMG_NOTAVAIL;
	UNREFERENCED_PARAMETER(hFile);
	UNREFERENCED_PARAMETER(hPrnDC);
	UNREFERENCED_PARAMETER(lpDest);
	UNREFERENCED_PARAMETER(lpSrc);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLrender(HGLOBAL hFile, HDC hDC, LPRECT lpDest, LPRECT lpSrc)
{
	return 0;
	UNREFERENCED_PARAMETER(hFile);
	UNREFERENCED_PARAMETER(hDC);
	UNREFERENCED_PARAMETER(lpDest);
	UNREFERENCED_PARAMETER(lpSrc);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLerror_string(LPSTR lpszBuf, int nMaxLen)
{
	LoadString(tiffInstance, GETERROR, lpszBuf, nMaxLen);
	return IMG_OK;
}

// ----------------------------------------------------------------------
// Helper Functions
// ----------------------------------------------------------------------

static int  read_row(FileStruct FAR * fs, HPSTR buf, HPSTR scratch, UINT start, UINT len, BOOL LastCol) 
{
	// This routine assumes that file pointer is pointing to next line to
	// read.
	int         errVal,i;
	UINT		maxLen;	//
	// If we're reading in a partial row, we'll need to decode into
	// the scratch buffer and copy the portion we want. Otherwise,
	// we can decode straight into output buffer...
	//
	// We also need to use the scratch buffer if the input pixel size
	// differs from the output pixel size (i.e., 2 bits/pixel in the input
	// goes into a 16-color DIB, which has 4 bits/pixel).
	// EAW: Added check for CMYK as the extra check for special cases.
	//
	// set compare length depending on tiled or stripped image
	maxLen = (fs->tiffHead.TileWidth) ? fs->tiffHead.TileBytesLine : fs->lRowSize;

	if ( ( len < (ULONG)maxLen ) ||
		   ( (UINT)fs->nPixBits != (fs->tiffHead.GrayBits * fs->tiffHead.SamplesPerPixel)) ) {
		if ((errVal = getDecodedRow(fs, scratch, start, len, LastCol)) != IMG_OK) return (errVal);
		// now adjust for differing input and output pixel widths
		if ((UINT)fs->nPixBits != fs->tiffHead.GrayBits * fs->tiffHead.SamplesPerPixel) {
			switch (fs->nPixBits) {
				case 4:
					switch (fs->tiffHead.GrayBits * fs->tiffHead.SamplesPerPixel) {
						case 2:
							for (i = 0; (UINT)i<len/2; i++) {
								*buf++ = (UCHAR)((UCHAR)*scratch & (UCHAR)0xC0)>>2 | (UCHAR)((UCHAR)*scratch & (UCHAR)0x30)>>4;
								*buf++ = (UCHAR)((UCHAR)*scratch & (UCHAR)0x0C)<<2 | (UCHAR)((UCHAR)*scratch & (UCHAR)0x03);
								scratch++;
							}
							break;
						case 3:
							break;
					}
					break;
				case 8:
					break;
				case 24:
					if ( fs->tiffHead.WhiteValue == PHOTOINT_CMYK )
						{
							CMYKToRGB ( fs, buf, scratch, len ) ;
					} else {		// 32 bit Image Data
						for ( i = 0; i < len; i+=3 ) {
							buf[i+2] = *scratch++;
							buf[i+1] = *scratch++;
							buf[i] = *scratch++;
							scratch++;
						}
					}
					break ;
			}
		}
		else {
			if ( fs->tiffHead.WhiteValue != PHOTOINT_CMYK )
				hmemcpy(buf, scratch + start, len);
			else
				{
					CMYKToRGB ( fs, buf, scratch, len ) ;
				}
		}
	} else {
		if ((errVal = getDecodedRow(fs, buf, start, len, LastCol)) != IMG_OK) return (errVal);
	}

	return IMG_OK;
}

static int  read_tile_rows(FileStruct FAR * fs, HPSTR buf, LPSTR scratch, 
						   unsigned start, unsigned len, unsigned rows, LPSTATUS lpStat, HGLOBAL hFile)
{	
	// reads all of each tile for number of rows requested, does the jumping around in the output buffer
	// reads forward through each tile, writes backward through output buffer
	int			firstTile, tile;
	int			rowCount, retval, tileRow, tileCol, tileRows, tileCols, firstTileCol, firstTileRow;
	ULONG		rectLeft, rectRight, rectTop, rectBottom;	// absolute coordinates of read area within a tile
	int			rowInTile, colInTile, tileRowCount, tileColCount;						
	HPSTR		bufOffset, tileStart, rowStart;			// output buffer placeholders
	BOOL		LastCol;

	firstTile = GetTileNo(fs, fs->row, start);
	firstTileCol = firstTile % fs->tiffHead.TilesAcross;
	firstTileRow = (int) (firstTile / fs->tiffHead.TilesAcross);
	// calculate num tile rows and cols in requested area
	tileCols = GetTileNo(fs, fs->row, start + len) - firstTile + 1; 
	tileRows = ((int)(GetTileNo(fs, fs->row + rows, start)/fs->tiffHead.TilesAcross)) -	// last row #
				((int)(firstTile/fs->tiffHead.TilesAcross))	+ 1	;						// minus first row # + 1

	rowStart = buf;			// point to start of output buffer	
	tileRow = firstTileRow;
	for (tileRowCount = 0; tileRowCount < tileRows; tileRowCount++) {			// down tile rows
		tileCol = firstTileCol;
		tileStart = rowStart;
		for (tileColCount = 0; tileColCount < tileCols; tileColCount++) {	// across tile columns
			tile = (tileRow * fs->tiffHead.TilesAcross) + tileCol;			// figure tile number

			// calculate the area to read within this tile
			rectLeft = max((tileCol * fs->tiffHead.TileBytesLine), start);
			rectRight = min((tileCol + 1) * fs->tiffHead.TileBytesLine, start + len);
			rectTop = max((tileRow * fs->tiffHead.TileLength), fs->row);
			rectBottom = min((tileRow + 1) * fs->tiffHead.TileLength, fs->row + rows);
				
			fs->lpIO->seek(fs->lpHandle, tileLoc(tile, fs), SEEK_SET);	// seek to this tile

			///////////// this does equivalent of set_row for stripped image//////////
			LastCol = (tileCol == fs->tiffHead.TilesAcross);				// if last col in image, might be padded
			rowInTile = rectTop - (tileRow * fs->tiffHead.TileLength);		// calculate row offset within tile
			colInTile = rectLeft - (tileCol * fs->tiffHead.TileBytesLine);	// calculate col offset within tile
			if (rowCount = rowInTile) {									// if not starting on row 0
				while (rowCount--) {									// decode down to row we want
					if (scratch) 
						retval = getDecodedRow(fs, scratch, colInTile, rectRight - rectLeft, LastCol);
					else 
						retval = getDecodedRow(fs, buf, colInTile, rectRight - rectLeft, LastCol);
					if (retval != IMG_OK) return retval;
				}
			}
			////////////////////////////////////////////////////////////////////

			rowCount = rectBottom - rectTop;		// number of rows to read within this tile
			bufOffset = tileStart;					// where to start writing this tile's stuff
			while (rowCount--) {					// do rows within each tile
				retval = read_row(fs, bufOffset, scratch, colInTile, rectRight - rectLeft, LastCol);
				if (retval != IMG_OK) 
					return retval;

				bufOffset -= len;				// wrap output buffer to next write point
				// check user-defined input
				if (GetLPStatus(lpStat, len, scratch, hFile) == IMG_CANCELLED) return IMG_CANCELLED;

			}	// end rows within tile
			
			tileStart += (rectRight - rectLeft);	// jump to next col in output buffer
			
			fs->wbufpos = fs->wbufsize;	// reset read buffer
			fs->lCodes = 0;
			fs->nBits = 0;
			LZWResetContext(fs->lpLZW);
			initBBuf(fs, fs->tiffHead.TileWidth);
			tileCol++;
		}	// end columns of tiles

		rowStart -= ((rectBottom - rectTop) * len);	// on last col. jump back up to next row in output buffer
		tileRow++;
	}		// end rows of tiles


	fs->row += rows;
	return IMG_OK;
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
static int  getDecodedRow(FileStruct FAR * fs, HPSTR scratch, UINT start, UINT len, BOOL LastCol)
{
	int		nCount, retval, iPixOffset;
	UINT		maxLen;

	// set output row size depending on compression and strips vs tiles
	if (   (fs->tiffHead.Compression == NOCOMPRESSBYTE)
		|| (fs->tiffHead.Compression == LZW)
		|| (fs->tiffHead.Compression == PACKBITS)) {

			if (fs->tiffHead.TileWidth && LastCol)  // last column might be padded
				maxLen = start + len;
		//	if ((fs->tiffHead.TileWidth) && (start + len < fs->tiffHead.TileBytesLine)) // might be last padded tile
		//		maxLen = start + len;					
			else if (fs->tiffHead.TileWidth)		// tiled image, normally treat as regular row, read width of tile
				maxLen = fs->tiffHead.TileBytesLine;
			else									// stripped image, read width of strip
				maxLen = fs->tiffHead.BytesLine;
	}
	else
		maxLen = fs->tiffHead.TileWidth ? fs->tiffHead.TileWidth : fs->tiffHead.ImageWidth;
	

	switch (fs->tiffHead.Compression) {
		case NOCOMPRESSBYTE:	  // no compression, just read the row

			if (fs->tiffHead.PlanarConfiguration == 2) {
				ReadPlanarRow(fs, scratch);
				assert(fs->tiffHead.WhiteValue == 2);
				break;				  // don't need to swap... already in DIB's BGR order

			} 
			else if (fs->lpIO->read(fs->lpHandle, scratch, maxLen) != maxLen) {
					SETERROR(IDS_READERR);
					return IMG_FILE_ERR;
			}									// stripped image, read row length of image
			if (fs->tiffHead.WhiteValue == 0) {
				UINT HUGE  *lpFlip = (UINT HUGE *) scratch;

				for (nCount = (maxLen + sizeof(UINT) - 1) / sizeof(UINT); nCount--;) {
					*lpFlip++ ^= ~0;  // 'not' each byte

				}
			} else if (fs->tiffHead.WhiteValue == 2) {
				swapColors(scratch, maxLen, fs->tiffHead.GrayBits * fs->tiffHead.SamplesPerPixel);
			}
			break;
		case CCITT3:
			//
			// For modified Huffman encoding, we'll use standard UNCCITT routine.
			// Before calling the routine, however, we'll align lCodes
			// on the next byte boundary, since all rows must begin on a
			// byte boundary (note that this is not a requirement for FAXCCITT3).
			//
			if (nCount = fs->nBits % 8) {
				fs->lCodes <<= nCount;
				fs->nBits -= nCount;
			}
			if (fs->tiffHead.WhiteValue == 1)
				fs->tiffHead.WhiteValue = 0;
			
			return UNCCITT(fs, scratch, maxLen);
			break;

		case FAXCCITT3:
			if (fs->tiffHead.WhiteValue == 1)
				fs->tiffHead.WhiteValue = 0;

			return UNCCITT(fs, scratch, maxLen);
			break;

		case FAXCCITT4:

			if (fs->tiffHead.WhiteValue == 1)
				fs->tiffHead.WhiteValue = 0;

			return unCCITT2D(fs, scratch, maxLen);
			break;

		case LZW:
			retval = LZWDecodeRow(fs, scratch, fs->lpLZW);
			if (fs->tiffHead.WhiteValue == 0) {
				UINT HUGE  *lpFlip = (UINT HUGE *) scratch;

				for (nCount = (maxLen + sizeof(UINT) - 1) / sizeof(UINT); nCount--;) {
					*lpFlip++ ^= ~0;  // 'not' each byte

				}
			} else if (fs->tiffHead.WhiteValue == 2) {
				swapColors(scratch, maxLen, fs->tiffHead.GrayBits * fs->tiffHead.SamplesPerPixel);
			}
			if (fs->tiffHead.Predictor == 2) {
				HPSTR       p = scratch + 1;

				nCount = maxLen - 1;
				if ((fs->tiffHead.GrayBits * fs->tiffHead.SamplesPerPixel) >= 24) {
//					iPixOffset = ( fs->tiffHead.WhiteValue == PHOTOINT_CMYK ) ? 4 : 3 ;
					iPixOffset = fs->tiffHead.SamplesPerPixel ;
					for (p += (iPixOffset-1), nCount -= (iPixOffset-1); nCount--; p++)
						*p += *(p - iPixOffset);
				} else {
					for (; nCount--; p++)
						*p += *(p - 1);
				}
			}
			return retval;
			break;
		case NOCOMPRESSWORD:
			break;
		case PACKBITS:
			if (fs->nPixBits == 24)
			{
				int	nRet;

				nRet = unPackBits(fs, scratch, maxLen);
				swapColors(scratch, maxLen, 24);

				return (nRet);
			}
			else
				return unPackBits(fs, scratch, maxLen);
			break;
		default:
			break;
	}

	return IMG_OK;
}


static int  fill_palette(LPBITMAPINFO lpbi, FileStruct FAR * fs)
{
#define SETRGB(I,R,G,B)  *(LPDWORD)((lpbi->bmiColors)+(I)) = ((DWORD)(R)<<16)|((WORD)(G)<<8)|(UCHAR)(B)
#define bHead lpbi->bmiHeader
	WORD         i;
	LPLONG      lpPal;
	WORD			wMaxCol = 2 << (fs->tiffHead.GrayBits-1);

	switch (bHead.biClrUsed) {
		case 2:
			SETRGB(0, 0, 0, 0);
			SETRGB(1, 255, 255, 255);
			break;

		case 16:
			// look for the color map here -- we have to map numbers
			// in range 0-65535 to range 0-255
			if (fs->tiffHead.ColorMap && fs->tiffHead.WhiteValue == 3) {
				lpPal = (LPLONG) GlobalLock(fs->tiffHead.ColorMap);
				for (i = 0; i < wMaxCol; i++) {
					lpbi->bmiColors[i].rgbRed = (UCHAR) (lpPal[i] / 256);
					lpbi->bmiColors[i].rgbGreen = (UCHAR) (lpPal[i + wMaxCol] / 256);
					lpbi->bmiColors[i].rgbBlue = (UCHAR) (lpPal[i + wMaxCol*2] / 256);
				}
				GlobalUnlock(fs->tiffHead.ColorMap);
			} else {					  // setup palette for grayscale

				for (i = 16; i--;) {
					lpbi->bmiColors[i].rgbRed = lpbi->bmiColors[i].rgbGreen
						= lpbi->bmiColors[i].rgbBlue = (BYTE)(i << 4);
				}
			}
			break;
		case 256:
			// look for the color map here -- we have to map numbers
			// in range 0-65535 to range 0-255
			if (fs->tiffHead.ColorMap) {
				lpPal = (LPLONG) GlobalLock(fs->tiffHead.ColorMap);
				for (i = 0; i < 256; i++) {
					lpbi->bmiColors[i].rgbRed = (UCHAR) (lpPal[i] / 256);
					lpbi->bmiColors[i].rgbGreen = (UCHAR) (lpPal[i + 256] / 256);
					lpbi->bmiColors[i].rgbBlue = (UCHAR) (lpPal[i + 512] / 256);
				}
				GlobalUnlock(fs->tiffHead.ColorMap);
			} else {					  // setup palette for grayscale

				for (i = 256; i--;) {
					lpbi->bmiColors[i].rgbRed = lpbi->bmiColors[i].rgbGreen
						= lpbi->bmiColors[i].rgbBlue = (BYTE)i;
				}
			}
			break;
		default:
			break;
	}

	return 0;

#undef bHead
#undef SETRGB
}

static long stripLoc(UINT strip, FileStruct FAR * fs)
{	
	LPLONG      stripsOff;
	long        sloc;
	UINT        nStrips;

	nStrips = (UINT) max(fs->tiffHead.StripsPerImage, 1);

	if (fs->tiffHead.PlanarConfiguration == 2)
		nStrips *= 3;

	if (nStrips > 1) {
		assert(fs->tiffHead.StripOffsets);
		stripsOff = (LPLONG) GlobalLock(fs->tiffHead.StripOffsets);
		sloc = (strip < nStrips) ? stripsOff[strip] : stripsOff[0];
		GlobalUnlock(fs->tiffHead.StripOffsets);
	} else
		sloc = fs->tiffHead.StripFileLoc;

	return (sloc + fs->offset);
}


static long tileLoc(UINT tile, FileStruct FAR * fs)
{	//tile number returns file location of start of tile data
	LPLONG      TileOff;
	long        tloc;
	UINT        nTiles;

	nTiles = (UINT) max(fs->tiffHead.TilesPerImage, 1);

	if (fs->tiffHead.PlanarConfiguration == 2)
		nTiles *= 3;

	if (nTiles > 1) {
		assert(fs->tiffHead.TileOffsets);
		TileOff = (LPLONG) GlobalLock(fs->tiffHead.TileOffsets);
		tloc = (tile < nTiles) ? TileOff[tile] : TileOff[0];
		GlobalUnlock(fs->tiffHead.TileOffsets);
	} else		// if there is only 1 tile in image (not likely)
		tloc = fs->tiffHead.TileFileLoc;

	return (tloc + fs->offset);
}


UINT        wgetword(FileStruct FAR * fs)
{
	if (fs->wbufpos >= fs->wbufsize) {
		fs->lpIO->read(fs->lpHandle, fs->wbuf, fs->wbufsize);
		fs->wbufpos = 0;
		if (BYTEFLIP(fs))
			flipBufOrder(fs);
	}
	fs->wbufpos += sizeof(WORD);
	// we must swap bytes of word to maintain bit stream order
#ifdef WIN32
	return ((fs->wbuf[fs->wbufpos - 2] << 8) | fs->wbuf[fs->wbufpos - 1]);
#else
	return (_rotl(*(LPWORD) (fs->wbuf + fs->wbufpos - 2), 8));
#endif
}

// Used by lzw module
UINT        wgetu(LPVOID cData)
{
#define lpFS ((FileStruct FAR *)cData)

	if (lpFS->wbufpos >= lpFS->wbufsize) {
		lpFS->lpIO->read(lpFS->lpHandle, lpFS->wbuf, lpFS->wbufsize);
		lpFS->wbufpos = 0;
		if (BYTEFLIP(lpFS))
			flipBufOrder(lpFS);
	}
	// Pre-adjust the buffer position so we don't need a temporary for the
	// return value. The "-sizeof(UINT)" adds no additional time to the access.
	lpFS->wbufpos += sizeof(UINT);
	return (*(UINT FAR *) (lpFS->wbuf + lpFS->wbufpos - sizeof(UINT)));
}

static void flipBufOrder(FileStruct FAR * fs)
{
	// bit-flip table (e.g. 11000101 -> 10100011 [0xc5 -> 0xa3])
	static UCHAR const flipOrder[256] = {
		0x00, 0x80, 0x40, 0xc0, 0x20, 0xa0, 0x60, 0xe0, 0x10, 0x90, 0x50, 0xd0, 0x30, 0xb0, 0x70, 0xf0,
		0x08, 0x88, 0x48, 0xc8, 0x28, 0xa8, 0x68, 0xe8, 0x18, 0x98, 0x58, 0xd8, 0x38, 0xb8, 0x78, 0xf8,
		0x04, 0x84, 0x44, 0xc4, 0x24, 0xa4, 0x64, 0xe4, 0x14, 0x94, 0x54, 0xd4, 0x34, 0xb4, 0x74, 0xf4,
		0x0c, 0x8c, 0x4c, 0xcc, 0x2c, 0xac, 0x6c, 0xec, 0x1c, 0x9c, 0x5c, 0xdc, 0x3c, 0xbc, 0x7c, 0xfc,
		0x02, 0x82, 0x42, 0xc2, 0x22, 0xa2, 0x62, 0xe2, 0x12, 0x92, 0x52, 0xd2, 0x32, 0xb2, 0x72, 0xf2,
		0x0a, 0x8a, 0x4a, 0xca, 0x2a, 0xaa, 0x6a, 0xea, 0x1a, 0x9a, 0x5a, 0xda, 0x3a, 0xba, 0x7a, 0xfa,
		0x06, 0x86, 0x46, 0xc6, 0x26, 0xa6, 0x66, 0xe6, 0x16, 0x96, 0x56, 0xd6, 0x36, 0xb6, 0x76, 0xf6,
		0x0e, 0x8e, 0x4e, 0xce, 0x2e, 0xae, 0x6e, 0xee, 0x1e, 0x9e, 0x5e, 0xde, 0x3e, 0xbe, 0x7e, 0xfe,
		0x01, 0x81, 0x41, 0xc1, 0x21, 0xa1, 0x61, 0xe1, 0x11, 0x91, 0x51, 0xd1, 0x31, 0xb1, 0x71, 0xf1,
		0x09, 0x89, 0x49, 0xc9, 0x29, 0xa9, 0x69, 0xe9, 0x19, 0x99, 0x59, 0xd9, 0x39, 0xb9, 0x79, 0xf9,
		0x05, 0x85, 0x45, 0xc5, 0x25, 0xa5, 0x65, 0xe5, 0x15, 0x95, 0x55, 0xd5, 0x35, 0xb5, 0x75, 0xf5,
		0x0d, 0x8d, 0x4d, 0xcd, 0x2d, 0xad, 0x6d, 0xed, 0x1d, 0x9d, 0x5d, 0xdd, 0x3d, 0xbd, 0x7d, 0xfd,
		0x03, 0x83, 0x43, 0xc3, 0x23, 0xa3, 0x63, 0xe3, 0x13, 0x93, 0x53, 0xd3, 0x33, 0xb3, 0x73, 0xf3,
		0x0b, 0x8b, 0x4b, 0xcb, 0x2b, 0xab, 0x6b, 0xeb, 0x1b, 0x9b, 0x5b, 0xdb, 0x3b, 0xbb, 0x7b, 0xfb,
		0x07, 0x87, 0x47, 0xc7, 0x27, 0xa7, 0x67, 0xe7, 0x17, 0x97, 0x57, 0xd7, 0x37, 0xb7, 0x77, 0xf7,
		0x0f, 0x8f, 0x4f, 0xcf, 0x2f, 0xaf, 0x6f, 0xef, 0x1f, 0x9f, 0x5f, 0xdf, 0x3f, 0xbf, 0x7f, 0xff
	};
	LPSTR       p;
	int         count;

	for (p = fs->wbuf, count = fs->wbufsize; count--; p++) {
		*p = flipOrder[*p];
	}
}

#if CCITT_MKTABLES
void        initCCITT(void)
{
	UINT        i, j, k, index, mask;

	for (j = 0; j < TBLENTRIES; j++) {
		mask = 0xffff U >> wlen[j];
		k = 1 << (16 - wlen[j]);
		for (i = 0; i < k; i++) {
			index = (i & mask) | wcodes[j];
			whiteTab[index >> 4] = j;
		}

		mask = 0xffff U >> blen[j];
		k = 1 << (16 - blen[j]);
		for (i = 0; i < k; i++) {
			index = (i & mask) | bcodes[j];
			blackTab[index >> 3] = j;
		}
	}
}
#endif

static void swapColors(HPSTR lpBuf, int nLen, int nBits)
{
	if (nBits == 24) {
		register UCHAR tmp;

		// turn RGB into BGR for Windows DIB.
		for (; nLen > 0; lpBuf += 3, nLen -= 3) {
			tmp = *lpBuf;
			*lpBuf = *(lpBuf + 2);
			*(lpBuf + 2) = tmp;
		}
	}
}

static int  ReadPlanarRow(FileStruct FAR * fs, HPSTR lpBuf)
{
	LPSTR       rgbbuf, rgbptr;
	HPSTR       dstptr;
	long        StartOff, StripOff, StripRow;
	int         i, j;
	int         BytesPlane;
	int         strip = (int) ((fs->row) / fs->tiffHead.RowsPerStrip);

	if (!(rgbbuf = GlobalAllocPtr(DLLHND, fs->tiffHead.BytesLine))) {
		SETERROR(IDS_MEMERR);
		return (IMG_NOMEM);
	}

	BytesPlane = fs->tiffHead.BytesLine / 3;
	StripRow = (fs->row % fs->tiffHead.RowsPerStrip);

	StartOff = fs->lpIO->seek(fs->lpHandle, 0, SEEK_CUR);

	// Read the red row
	StripOff = stripLoc(strip, fs);
	StripOff += StripRow * fs->tiffHead.ImageWidth;
	fs->lpIO->seek(fs->lpHandle, StripOff, SEEK_SET);
	fs->lpIO->read(fs->lpHandle, rgbbuf, BytesPlane);

	// Seek to the green plane
	StripOff = stripLoc((int) (strip + fs->tiffHead.StripsPerImage), fs);
	StripOff += StripRow * fs->tiffHead.ImageWidth;
	fs->lpIO->seek(fs->lpHandle, StripOff, SEEK_SET);
	fs->lpIO->read(fs->lpHandle, rgbbuf + BytesPlane, BytesPlane);

	// read the BLUE plane
	StripOff = stripLoc((int) (strip + 2 * fs->tiffHead.StripsPerImage), fs);
	StripOff += StripRow * fs->tiffHead.ImageWidth;
	fs->lpIO->seek(fs->lpHandle, StripOff, SEEK_SET);
	fs->lpIO->read(fs->lpHandle, rgbbuf + 2 * BytesPlane, BytesPlane);

	fs->lpIO->seek(fs->lpHandle, StartOff, SEEK_SET);

	// place pixels in DIB's "BGR" order so we needn't call swapColors!
	for (rgbptr = rgbbuf, i = 3; i--;) {
		for (dstptr = lpBuf + i, j = BytesPlane; j--; dstptr += 3) {
			*dstptr = *rgbptr++;
		}
	}

	GlobalFreePtr(rgbbuf);

	return (IMG_OK);
}

// /////////////////////////////////////////////////////////////////////////
//
// ccitt_blackrun
//
// CCITT helper routine to write 'runlen' black bits starting at 'bitpos'.
// Also used by 2D CCITT code in un2d.c
//
// Parametrons:
// buf    - base of current scanline
// bitpos - bit index within scanline to begin writing
// runlen - black bit count
//
// /////////////////////////////////////////////////////////////////////////
void        ccitt_blackrun(HPSTR buf, int bitpos, int runlen)
{
	if (runlen <= 0)
		return;

	buf += bitpos / 8;
	// write any leading bits (partial byte fill)
	if (bitpos &= 7) {
		if (runlen + bitpos < 8) {
			*buf ^= runmask[runlen] >> bitpos;	// clear only middle part of byte

			return;
		}
		*buf++ &= runmask[bitpos];// clear to end of byte

		runlen -= 8 - bitpos;
	}
	// write as many full bytes as we can
	for (bitpos = runlen / 8; --bitpos >= 0;)
		*buf++ = 0;
	// write any trailing bits
	if( runlen & 7 )
		*buf ^= runmask[runlen & 7];
}

// /////////////////////////////////////////////////////////////////////////
//
// unCCITT
//
// Read 1 CCITT 1D compressed scanline.
//
// /////////////////////////////////////////////////////////////////////////
static int  unCCITT(FileStruct FAR * fs, HPSTR buf, int nWidth)
{
	int         runlen, bitpos, i, nbits;
	UCHAR       cbits;
	ULONG       lcode;
	BOOL        writeblack;

	// To save time, we only write black runs. This requires us to pre-set
	// the entire scanline to white.
	MemFill(buf, (nWidth + 7) / 8, ~0);

	// Get local copies of bit accumulator stuff, and prime accumulator
	lcode = fs->lCodes;
	nbits = fs->nBits;

	// CCITT spec says to assume we start with white pixels. We set everything
	// to black; the toggle code is at the top of the loop so I don't need
	// to setup the tables up here.
	writeblack = PHOTOFLIP(fs);
	bitpos = 0;

	// Search for and strip any leading EOL. An EOL is the only code which has
	// 8 leading zero bits; we'll use this for quick detection.
	if (nbits < 16) {
		lcode |= (ULONG) wgetword(fs) << (16 - nbits);
		nbits += 16;
	}

	if (!(lcode & 0xFF000000)) {  // delay shifting these 8 bits out

		while (!(lcode & 0xFF000000)) {
			lcode <<= 8;
			nbits -= 8;
			if (nbits < 12) {
				lcode |= (ULONG) wgetword(fs) << (16 - nbits);
				nbits += 16;
			}
		}
		// now shift out the first bit...
		while (!(lcode & 0x80000000)) {
			lcode <<= 1;
			nbits --;
		}
		lcode <<= 1;
		nbits --;
	}

	// now process alternating white & black runs 'till we're done...
	// If we encounter an EOL while processing, we'll exit and
	// leave the EOL for the start of the next line...
	while (TRUE) {
		// Process a white run.
		runlen = 0;
		goto skipadd_1;
		do {
			runlen += i - 63 << 6;  // accumulate Make-Up Codes

	skipadd_1:
			if (nbits < CCITT_WHITEMAX) {
				lcode |= (ULONG) wgetword(fs) << (16 - nbits);
				nbits += 16;
			}
			if (!(lcode & 0xFF000000)) {
				fs->lCodes = lcode;
				fs->nBits = nbits;
				return IMG_OK;
			}
			i = whiteTab[GETTOPBITS(lcode, CCITT_WHITEMAX)];
			cbits = wlen[i];
			nbits -= cbits;
			lcode <<= cbits;
		} while (i > 63);
		if (runlen += i) {
			if (!writeblack) ccitt_blackrun(buf, bitpos, min(runlen, nWidth-bitpos));
			if ((bitpos += runlen) >= nWidth)
				break;
		}
		// Process a black run.
		runlen = 0;
		goto skipadd_2;
		do {
			runlen += i - 63 << 6;
	skipadd_2:
			if (nbits < CCITT_BLACKMAX) {
				lcode |= (ULONG) wgetword(fs) << (16 - nbits);
				nbits += 16;
			}
			if (!(lcode & 0xFF000000)) {
				fs->lCodes = lcode;
				fs->nBits = nbits;
				return IMG_OK;
			}
			i = blackTab[GETTOPBITS(lcode, CCITT_BLACKMAX)];
			cbits = blen[i];
			nbits -= cbits;
			lcode <<= cbits;
		} while (i > 63);
		if (runlen += i) {
			if (writeblack) {
				if (runlen+bitpos <= nWidth)	//only write runlen bits if it's not too many!
					ccitt_blackrun(buf, bitpos, runlen);
				else 
					ccitt_blackrun(buf, bitpos, (nWidth - bitpos));
			}
			if ((bitpos += runlen) >= nWidth)
				break;
		}
	}

	// update filestruct's state
	fs->lCodes = lcode;
	fs->nBits = nbits;

	// sanity check
//	if (bitpos != nWidth) {
//		SETERROR(IDS_PROCESS);
//		return (IMG_PROC_ERR);
//	}
	return (IMG_OK);
}

// /////////////////////////////////////////////////////////////////////////
//
// unPackBits
//
// /////////////////////////////////////////////////////////////////////////
int        unPackBits(FileStruct FAR * fs, HPSTR buf, int count)
{

	// Rather than calling wgetc() for every fetch, I'm gonna guarantee
	// there's enough data in the buffer for the current line.
	// The absolute worst case is 1 byte becomes 2 (absolute runs of 1);
	// while this would be a stupid encoding, we gotta be safe.
	if (fs->wbufsize - fs->wbufpos <= (UINT)(2 * count)) {
		if ((fs->wbufpos = fs->wbufsize - fs->wbufpos) > 0) {
			// copy remaining data down; memory could overlap!
			_fmemmove(fs->wbuf, fs->wbuf + fs->wbufsize - fs->wbufpos, fs->wbufpos);
		}
		fs->lpIO->read(fs->lpHandle, fs->wbuf + fs->wbufpos, fs->wbufsize - fs->wbufpos);
		fs->wbufpos = 0;
	}
	// Now we definitely have enough bytes in our buffer, so we can read
	// directly (and quickly) from the buffer.
	{
		int         i;
		LPSTR       p = fs->wbuf + fs->wbufpos;
		UCHAR       c, photo = PHOTOFLIP(fs) ? ~0 : 0;

		do {
			// be sure to loop one more than 'i': if i = 5, loop 6 times
			if ((i = (signed char) *p++) < 0) {
				count += i;			  // encoded run

				c = *p++ ^ photo;
				do {
					*buf++ = c;
				} while (++i <= 0);
			} else {
				count -= i;			  // absolute run

				do {
					*buf++ = *p++ ^ photo;
				} while (--i >= 0);
			}
		} while (--count > 0);
		fs->wbufpos = p - fs->wbuf;	// adjust buffer position

		assert(fs->wbufpos <= fs->wbufsize);
		return (count >= 0 ? IMG_OK : IMG_PROC_ERR);
	}
}

void CMYKToRGB ( FileStruct FAR * fs, HPSTR buf, HPSTR scratch, UINT len)
{
	UCHAR  uBlack ;
	int    i, iPixel ;
	UCHAR uR, uG, uB ;

	for ( i = 0 ; (UINT)i < len / 3 ; i++ )
		{
			uBlack = (UCHAR) * ( scratch + 3 ) ;
/*
			for ( ixx = 2 ; ixx >= 0 ; ixx-- )
				{
					iPixel = 255 - (*scratch++ + uBlack) ;
					if ( iPixel < 0 ) 
						iPixel = 0 ;
					
					*(buf+ixx) = (UCHAR) iPixel ;
				}
			buf += 3 ;
*/
			iPixel = 255 - (*scratch++ + uBlack) ;
			if ( iPixel < 0 ) iPixel = 0 ;
			uR = (UCHAR) iPixel ;

			iPixel = 255 - (*scratch++ + uBlack) ;
			if ( iPixel < 0 ) iPixel = 0 ;
			uG = (UCHAR) iPixel ;

			iPixel = 255 - (*scratch++ + uBlack) ;
			if ( iPixel < 0 ) iPixel = 0 ;
			uB = (UCHAR) iPixel ;

			* buf++ = uB ;
			* buf++ = uG ;
			* buf++ = uR ;

			scratch ++ ;		// skip the black
			if ( fs->tiffHead.SamplesPerPixel > 4 )  // LEAD Fools puts 5
				scratch ++ ;
		}
}

#if DEBUG
void __cdecl FAR Debug(UINT wCategory, LPSTR lpFormat,...)
{
	LPSTR       vaArgs;

	if (DebugStringOut) {
		va_start(vaArgs, lpFormat);
		(*DebugStringOut) (wCategory, lpFormat, vaArgs);
		va_end(vaArgs);
	}
}
#else
void __cdecl FAR Debug(UINT wCategory, LPSTR lpFormat,...)
{
}
#endif
