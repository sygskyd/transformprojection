/////////////////////////////////////////////////////////////////////////////
//
// fxshape.c
//
// ImageMan Effects Module: Shape explosion and implosion.
// Exports:
//   doShape
//
// Version 1.0
// Copyright (c) 1996 Data Techniques, Inc.
// Copyright (c) 1996 Chris Roueche
// All Rights Reserved
//
/////////////////////////////////////////////////////////////////////////////
// $History: fxshape.cpp $
// 
// *****************  Version 4  *****************
// User: Johnd        Date: 4/10/98    Time: 5:06p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Chgd Error Returns to be consistent with other ImageMan Defines
// 
// *****************  Version 3  *****************
// User: Ericj        Date: 4/09/98    Time: 2:00p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Fixed problem a portion of an image remaining while imploding and
// problem if ystep and xstep aren't both set.
// 
// *****************  Version 2  *****************
// User: Ericw        Date: 2/03/98    Time: 12:11p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Needed to include internal.h
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 7/10/97    Time: 2:03p
// Created in $/imageman 16-32/dll-api level 2
// Initial Lvl 2 Checkin
//
// 04/16/96 CAR  Made non-rectangular shapes expand until entire image
//               displayed (math is hard! :) Fixed problems with implosions,
//               DC clippage, and rectangles.

#define STRICT
#define WIN32_LEAN_AND_MEAN
#define NOSERVICE
#include <windows.h>
#include <windowsx.h>
#include "internal.h"
#include "imgman.h"
#include "fxparam.h"
#pragma hdrstop
#include <math.h>

// Blur-specific storage can be found in fxparam.h:tag_Shape

// keep elliptical corners round on a rounded rectangle
#define RRECTCORNER_X(_R) (((_R).right-(_R).left)/4)
#define RRECTCORNER_Y(_R) (((_R).right-(_R).left)/4)


/////////////////////////////////////////////////////////////////////////////
//
// ellipse_maxbox
//
// Find the bounding rectangle of an ellipse which fully exposes the image
// within the destination area. This ellipse will have the same eccentricity,
// and completely enclose the destination rectangle.
//   c = sqrt(a^2-b^2), e = c/a, and x^2/a^2+y^2/b^2=1 were used to find
// a' and b', where 2a' is the width and 2b' is the height of the final
// bounding box.
// 
/////////////////////////////////////////////////////////////////////////////

static void NEAR ellipse_maxbox(LPRECT prdst)
{
	double a,b,a2,b2;

	a = (prdst->right-prdst->left)/2.0;
	b = (prdst->bottom-prdst->top)/2.0;

	b2 = 2.0*(b*b); // leave unsqrt'd to avoid a b2*b2 term in next line
	a2 = sqrt((a*a)/(b*b)*b2)-a;
	b2 = sqrt(b2)-b;

	prdst->left -= (int)a2;
	prdst->right += (int)a2;
	prdst->top -= (int)b2;
	prdst->bottom += (int)b2;
}

/////////////////////////////////////////////////////////////////////////////
//
// rrect_maxbox
//
// Find the bounding rectangle of a rounded rectangle which fully exposes
// the image within the destination area.
// I need to ensure the corners are exposed, and each rounded corner is
// really an ellipse. Thus, I use similar code to the ellipse maxbox
// function to determine the bounding box here.
//
/////////////////////////////////////////////////////////////////////////////

static void NEAR rrect_maxbox(LPRECT prdst)
{
	double a,b,a2,b2;

	// use corner ellipse
	a = RRECTCORNER_X(*prdst)/2.0;
	b = RRECTCORNER_Y(*prdst)/2.0;

	b2 = 2.0*(b*b); // leave unsqrt'd to avoid a b2*b2 term in next line
	a2 = sqrt((a*a)/(b*b)*b2)-a;
	b2 = sqrt(b2)-b;

	prdst->left -= (int)a2;
	prdst->right += (int)a2;
	prdst->top -= (int)b2;
	prdst->bottom += (int)b2;
}

/////////////////////////////////////////////////////////////////////////////
//
/////////////////////////////////////////////////////////////////////////////

static void NEAR initshape(PEFFECTBLK peb)
{
	int x,y,i;
	RECT bbox;

	// initialize rectangle explode/implode
	// LOWORD(dwExtra) == x step amount,HIWORD(dwExtra) == y step amount
	peb->shape.xstep.num = LOWORD(peb->dwExtra);
	peb->shape.ystep.num = HIWORD(peb->dwExtra);
	peb->shape.xstep.den = peb->shape.ystep.den = 1;
	// Determine the largest area I must fill to fully expose the image. For
	// example, a circle filled out to the destination rectangle would leave
	// the corners unrevealed... I need to go beyond the destination box to 
	// fill in those corners.
	bbox = peb->rDst;
	if (peb->dwFlags&IMDF_SHAPE_ROUNDRECTANGLE) {
		rrect_maxbox(&bbox);
	}
	else if (peb->dwFlags&IMDF_SHAPE_ELLIPSE) {
		ellipse_maxbox(&bbox);
	}
	// generate proportional (rectangular) step if none was specified
	x = (bbox.right-bbox.left+1)/2;
	y = (bbox.bottom-bbox.top+1)/2;
	peb->nStepsTotal = min(x,y);
	if (peb->shape.xstep.num == 0) {
		peb->shape.xstep.num = x;
		//peb->shape.xstep.den = peb->nStepsTotal;
	}
	if (peb->shape.ystep.num == 0) {
		// If no ystep is specified make it a square effect.
		peb->shape.ystep.num = peb->shape.xstep.num; 
		//peb->shape.ystep.num = y;
		//peb->shape.ystep.den = peb->nStepsTotal;
	}
	peb->shape.accum.cx = peb->shape.accum.cy = 0;
	// setup initial inclusion/exclusion area
	if (peb->nEffect == IMFX_SHAPE_EXPLODE) {
		peb->shape.bbox.left = peb->shape.bbox.right = (peb->rDst.left+peb->rDst.right)/2;
		peb->shape.bbox.top = peb->shape.bbox.bottom = (peb->rDst.top+peb->rDst.bottom)/2;
		peb->shape.dir = 1;
	}
	else { // imploding, bbox becomes exclusion area
		peb->shape.bbox = bbox;
		peb->shape.dir = -1;
	}
	// total steps to process
	i = peb->shape.xstep.num/peb->shape.xstep.den;
	x = (x+i-1)/i;
	i = peb->shape.ystep.num/peb->shape.ystep.den;
	y = (y+i-1)/i;
	if (peb->shape.xstep.den == 1 && peb->shape.ystep.den == 1 && peb->shape.dir > 0) {
		peb->nStepsTotal = max(x,y); // greater if non-proportional
	}
	else {
		peb->nStepsTotal = min(x,y); // lesser if proportional
	}
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
//
// doShape (a.k.a. Fun With Regions :)
//
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

int doShape(HDC hDC,PEFFECTBLK peb)
{
	int x,y;
	HRGN hrgn,hrgn2;
	RECT r;

	if (INITIALIZE(peb)) initshape(peb);
	if (CLEANUP(peb)) {
		if ((peb->shape.dir > 0) && (peb->dwFlags&IMDF_SHAPE_ROUNDRECTANGLE))
			if (peb->hClipRgn) 
				SelectClipRgn(hDC,peb->hClipRgn);
			else
				SelectClipRgn(hDC,NULL);
			clipblt(hDC,peb,&peb->rDst);
		return (IMG_OK);
	}
	// adjust box size
	x = y = 0;
	if ((peb->shape.accum.cx += peb->shape.xstep.num) >= peb->shape.xstep.den) {
		x = peb->shape.dir*peb->shape.accum.cx/peb->shape.xstep.den;
		peb->shape.accum.cx %= peb->shape.xstep.den;
	}
	if ((peb->shape.accum.cy += peb->shape.ystep.num) >= peb->shape.ystep.den) {
		y = peb->shape.dir*peb->shape.accum.cy/peb->shape.ystep.den;
		peb->shape.accum.cy %= peb->shape.ystep.den;
	}
	InflateRect(&peb->shape.bbox,x,y);

	// setup inclusion shape
	if (peb->dwFlags&IMDF_SHAPE_ROUNDRECTANGLE) {
		hrgn = CreateRoundRectRgn(peb->shape.bbox.left,peb->shape.bbox.top,
			peb->shape.bbox.right+1,peb->shape.bbox.bottom+1,
			RRECTCORNER_X(peb->shape.bbox),RRECTCORNER_Y(peb->shape.bbox));
	}
	else if (peb->dwFlags&IMDF_SHAPE_ELLIPSE) {
		hrgn = CreateEllipticRgn(peb->shape.bbox.left,peb->shape.bbox.top,
			peb->shape.bbox.right+1,peb->shape.bbox.bottom+1);
	}
	else hrgn = NULL;

	// update the display
	if (peb->shape.dir > 0) { // explode
		if (hrgn) {
			if (peb->hClipRgn) CombineRgn(hrgn,hrgn,peb->hClipRgn,RGN_AND);
			SelectClipRgn(hDC,hrgn);
		}
		IntersectRect(&r,&peb->shape.bbox,&peb->rDst);
		clipblt(hDC,peb,&r);
	}
	else { // implode
		if (peb->hClipRgn) {
			// Must combine DC's clippage with my exclusion region. If 'hrgn'
			// is NULL, assume shape is a rectangle. If that fails, assume
			// the area is empty and create an empty region.
			if (!hrgn) hrgn = CreateRectRgnIndirect(&peb->shape.bbox);
			if (!hrgn) hrgn = CreateRectRgn(0,0,0,0);
			CombineRgn(hrgn,peb->hClipRgn,hrgn,RGN_DIFF);
			SelectClipRgn(hDC,hrgn);
		}
		else if (hrgn) {
			// Turn shape's region into an exclusion region.
			hrgn2 = CreateRectRgnIndirect(&peb->rDst);
			CombineRgn(hrgn,hrgn2,hrgn,RGN_DIFF);
			DeleteRgn(hrgn2);
			SelectClipRgn(hDC,hrgn);
		}
		else if (peb->shape.bbox.right > peb->shape.bbox.left
			&& peb->shape.bbox.bottom > peb->shape.bbox.top) {
			// Simply exclude a rectangle.
			ExcludeClipRect(hDC,peb->shape.bbox.left,peb->shape.bbox.top,
				peb->shape.bbox.right,peb->shape.bbox.bottom);
		}
		clipblt(hDC,peb,&peb->rDst);
		SelectClipRgn(hDC,NULL);
	}
	if (hrgn) DeleteRgn(hrgn);

	peb->nStepsDone++;

	return (IMG_OK);
}

