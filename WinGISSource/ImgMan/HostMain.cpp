/////////////////////////////////////////////////////////////////////////////
//
//    HostMain.cpp
//
//    ImageMan Plug-In Host Main Module
//
//    ImageMan Image Processing Library
//
//    Copyright (c) 1998 Data Techniques, Inc.
//    All Rights Reserved
//	 
/////////////////////////////////////////////////////////////////////////////
//$Header: /ImageMan 16-32/DLL - V 6.xx API/HostMain.cpp 10    11/09/98 12:46p Johnd $
//
//$History: HostMain.cpp $
// 
// *****************  Version 10  *****************
// User: Johnd        Date: 11/09/98   Time: 12:46p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Misc Optimizations and Fix in DIB.cpp for unitialized hDIB when using
// width/height ctor.
// 
// *****************  Version 9  *****************
// User: Ericw        Date: 11/06/98   Time: 2:48p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Some thread info was not being re-initialized causing the preview of an
// image that is exactly the same size as a previous preview to be the
// original preview.  This also fixed some other flaky crash when sliding
// the sliders around and also after running multiple plugins with no UI.
// This fixes issue #151
// 
// *****************  Version 8  *****************
// User: Ericw        Date: 7/21/98    Time: 11:59a
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Bug #119 fixed (crashing ImgPlugInHostLoad)
// 
// *****************  Version 7  *****************
// User: Ericw        Date: 4/24/98    Time: 11:01a
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Filter cancel test in TestAbort function.  Fairly substantial rework of
// AdvanceState and main processing loop to support inbound and outbound
// tiling processing.
// 
// *****************  Version 4  *****************
// User: Ericw        Date: 4/08/98    Time: 3:55p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Cleaned up and tightened up the About function.
// 
// *****************  Version 3  *****************
// User: Ericw        Date: 4/07/98    Time: 4:33p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Some prelimary work on setting up and filtering sub-rect.
// 

#include "internal.h"
#pragma hdrstop

#include "PIHost.h"
#include "PIFilter.h"
#include "PIAbout.h"

#include "stdarg.h"
#include "stdio.h"

typedef void (*ENTRYPROC)(short, LPVOID, LPLONG, short*);

#include "cpipl.h"

typedef struct tagPITHREADINFO {
	int status ;
	char szFilterName[ MAX_PATH ] ;
	long lWidth  ;
	long lHeight ;
	FilterRecord * pFRecord ;
	ENTRYPROC   ENTRYPOINT ;
	HANDLE hImage     ;
	HANDLE hTile      ;
	Rect   rectLast   ;
	Rect   rectInLast ;
	BOOL   bFirstTile ;
	PDIB   pDIBIn     ;
	HINSTANCE hCurrPlugIn ;   /* the currently selected plug-in */
	CPiPL * pPiPL ;
	LPSTR lpPlugInDir ;
	STATUSPROC lpStat ;
	BOOL       bCancelFilter ;
	LONG       lInterval ;
	DWORD      dwStatUserInfo ;
} PITHREADINFO, * LPPITHREADINFO ;

static DWORD TlsIndex;          		// Thread local storage index

typedef struct tagPIDATA {
	char buff[1024];
} PIDATA, * LPPIDATA ;

#pragma hdrstop

HINSTANCE hHostInst ;			/* the one and only "me" */

static char * szPlugInExt[] = { "*.8bp", "*.8bc", "*.8ba", "*.8be", "*.8bx", "*.8bf",
                                "*.8bi", "*.8by", "*.8bs", "" } ;

// ///////////////////////////////////////////////////////////////
// Following are the callback procs
// ///////////////////////////////////////////////////////////////
AdvanceStateProc   PIIMAdvanceState  () ;
TestAbortProc      PIIMTestAbortProc () ;
//ColorServicesProc  PIIMColorServicesProc ( ColorServicesInfo * Info ) ;
ProgressProc       PIIMProgressProc ( int32 done, int32 total ) ;
ProcessEventProc   PIIMProcessEvent ( LPSTR er ) ;
DisplayPixelsProc  PIIMDisplayPixelsProc (const PSPixelMap * source, const VRect *srcrect,
																					int32 dstRow, int32 dstCol, unsigned32 platcxt ) ;
HostProc           PIIMHostProc ( int16 selector, int32 * data ) ;

#ifdef DEBUG
void PrintfDebugString ( LPSTR fmt, ... )
{
  char Temp [1000] ;
  va_list argptr ;
  va_start ( argptr, fmt ) ;
  vsprintf ( Temp, fmt, argptr ) ;
  OutputDebugString ( Temp ) ;
}
#endif

LPPITHREADINFO GetPIThreadInfo(void)
{
	LPPITHREADINFO lpInfo = NULL ;

	// Retrieve pointer stored for us by system.
	if (!(lpInfo = (LPPITHREADINFO) TlsGetValue(TlsIndex))) {
		// Whoops, this thread doesn't have any memory yet. Try to get some.
		if (lpInfo = (LPPITHREADINFO)LocalAlloc(LPTR,sizeof(PITHREADINFO))) {
			TlsSetValue(TlsIndex,lpInfo);
			memset ( lpInfo, 0x00, sizeof(PITHREADINFO) ) ;
		}
	}
	return (lpInfo);
}

long IMAPI PIIMInit ( LPSTR lpPlugInDir  )
{
	int ixx ;
	LPPITHREADINFO lpInfo = GetPIThreadInfo() ;
	lpInfo->lpPlugInDir = lpPlugInDir ;

	if ( ! lpPlugInDir ) return IMG_OK ;

	if ( ixx = strlen( lpPlugInDir ) ) {
    if ( !(lpInfo->lpPlugInDir = (LPSTR)GlobalAllocPtr ( GHND, ixx+2 )) ) return IMG_ERR ;
		strcpy ( lpInfo->lpPlugInDir, lpPlugInDir ) ;
		if ( lpInfo->lpPlugInDir[ixx-1] != '\\' )
			{
				lpInfo->lpPlugInDir[ixx] = '\\' ;
				lpInfo->lpPlugInDir[ixx+1] = '\0' ;
			}
	}
	return IMG_OK ;
}

BOOL RectsNotEmpty ( FilterRecordPtr fRecord )
{
	BOOL bRes = ( ( fRecord->inRect.top     == 0 ) &&
		            ( fRecord->inRect.left    == 0 ) &&
								( fRecord->inRect.right   == 0 ) &&
								( fRecord->inRect.bottom  == 0 ) &&
								( fRecord->outRect.top    == 0 ) &&
		            ( fRecord->outRect.left   == 0 ) &&
								( fRecord->outRect.right  == 0 ) &&
								( fRecord->outRect.bottom == 0 ) ) ;
	return ( ! bRes ) ;
}

void InvertDIB( LPSTR lpData, ULONG ulHeight, ULONG ulWidth )
{
  char * tRow; 
  LPSTR  lpTop;
  LPSTR  lpBottom;
  ULONG  nRows  = ulHeight >> 1 ;

  tRow = new char [ ulWidth+16 ] ;

  lpTop = lpData;
  lpBottom = lpTop + (( ulHeight * ulWidth ) - ulWidth);
    
  for ( ULONG ulxx = 0 ; ulxx < nRows ; ulxx++ )
    {
      memcpy ( tRow, lpTop, ulWidth ) ;
      memcpy ( lpTop, lpBottom, ulWidth ) ;
      memcpy ( lpBottom, tRow, ulWidth ) ;

      lpTop += ulWidth ;
      lpBottom -= ulWidth ;
    }

	delete [] tRow ;
}

void SwapRBs ( LPSTR lpData, ULONG ulHeight, ULONG ulImgWidth, ULONG ulRowWidth )
{
	long lxx, lyy ;
	UCHAR uc ;
	long lPad = ulRowWidth - ulImgWidth ;
	for ( lxx = 0 ; lxx < ulHeight ; lxx ++ )
		{
			for ( lyy = 0 ; lyy < ulImgWidth / 3 ; lyy ++ )
				{
					uc = *lpData ;		// save b
					*lpData = lpData[2] ;
					*(lpData+2) = uc ;
					lpData += 3 ;
				}
			lpData += lPad ;
		}
}

BOOL APIENTRY DllMain( HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved )
{
	LPPITHREADINFO lpInfo ;
	switch ( fdwReason )
		{
			case DLL_PROCESS_ATTACH :
				hHostInst = hinstDLL ;
				// Allocate a "Thread Local Storage" (TLS) index right away.
				if ((TlsIndex = TlsAlloc()) == 0xFFFFFFFF) return (FALSE);

				// note: no break so we fall-through to thread attach - on purpose.

			case DLL_THREAD_ATTACH :
				if (!(lpInfo = (LPPITHREADINFO)LocalAlloc(LPTR,sizeof(PITHREADINFO))))
					return (FALSE);
				memset ( lpInfo, 0x00, sizeof(PITHREADINFO) ) ;

				lpInfo->status = IMG_OK;
				if ( ! TlsSetValue(TlsIndex,lpInfo) ) {
					LocalFree(lpInfo);
					lpInfo = NULL ;
					return (FALSE);
				}
				break ;
			case DLL_THREAD_DETACH:
				// Release the allocated memory for this thread.
				if (lpInfo = (LPPITHREADINFO) TlsGetValue(TlsIndex)) {
					if ( lpInfo->hCurrPlugIn ) {
						FreeLibrary( lpInfo->hCurrPlugIn ) ;
						lpInfo->hCurrPlugIn = 0 ;
						if ( lpInfo->pPiPL )
							delete lpInfo->pPiPL ;
						if ( lpInfo->pFRecord )
							GlobalFreePtr ( lpInfo->pFRecord ) ;
						if ( lpInfo->lpPlugInDir )
							GlobalFreePtr ( lpInfo->lpPlugInDir ) ;
					}
					LocalFree((HLOCAL)lpInfo);
				}
				break;

			// The DLL is detaching from a process, due to
			// process termination or call to FreeLibrary.
			case DLL_PROCESS_DETACH:
				// Release the allocated memory for this thread.
				if (lpInfo = (LPPITHREADINFO) TlsGetValue(TlsIndex)) {
					if ( lpInfo->hCurrPlugIn ) {
						FreeLibrary( lpInfo->hCurrPlugIn ) ;
						lpInfo->hCurrPlugIn = 0 ;
						if ( lpInfo->pPiPL ) {
							delete lpInfo->pPiPL ;
							lpInfo->pPiPL = NULL ;
						}
						if ( lpInfo->pFRecord ) {
							GlobalFreePtr ( lpInfo->pFRecord ) ;
							lpInfo->pFRecord = NULL ;
						}
						if ( lpInfo->lpPlugInDir )
							GlobalFreePtr ( lpInfo->lpPlugInDir ) ;
					}
					LocalFree((HLOCAL)lpInfo);
				}
				// Release the TLS index.
				TlsFree(TlsIndex);

				break;
		}
	return ( true ) ;
}

int CALLBACK WEP(int bSystemExit)
{
	return (0);
	UNREFERENCED_PARAMETER(bSystemExit);
}

LPSTR MakePIName ( LPSTR piName, LPSTR piOut )
{
	LPPITHREADINFO lpInfo = GetPIThreadInfo() ;
	strcpy ( piOut, lpInfo->lpPlugInDir  ) ;
	strcat ( piOut, piName ) ;
	return ( piOut ) ;
}

long IMAPI PIIMGetPlugInList ( long lPlugInTypes, LPINT lpPlugInCount )
{
	return ( IMG_OK ) ;
}

long IMAPI PIIMLoadSpecific ( LPSTR lpPlugInName )
{
	DWORD dErr = 0 ;
	LPPITHREADINFO lpInfo = GetPIThreadInfo() ;
	char PIName[ MAX_PATH ] ;

	if ( lpInfo->hCurrPlugIn ) {		// if there is one already loaded, unload the old
		if ( lpInfo->pPiPL ) {
			delete lpInfo->pPiPL ;
			lpInfo->pPiPL = NULL ;
		}
		if ( lpInfo->pFRecord ) {
			GlobalFreePtr ( lpInfo->pFRecord ) ;
			lpInfo->pFRecord = NULL ;
		}
		FreeLibrary( lpInfo->hCurrPlugIn ) ;
		lpInfo->hCurrPlugIn = NULL ;
	}
	if ( lpPlugInName == NULL || lpPlugInName[0] == '\0' )
		return IMGPI_ERRORLOADINGPLUGIN ;

	MakePIName ( lpPlugInName, PIName ) ;
	if ( ! ( lpInfo->hCurrPlugIn = LoadLibrary( PIName )) ) {
		long ixx = GetLastError() ;
		return IMGPI_ERRORLOADINGPLUGIN ;
	}
	if ( lpInfo->pPiPL ) {
		delete lpInfo->pPiPL ;
		lpInfo->pPiPL = NULL ;
	}
	if ( lpInfo->pFRecord ) {
		GlobalFreePtr ( lpInfo->pFRecord ) ;
		lpInfo->pFRecord = NULL ;
	}
	if ( (! ( lpInfo->pPiPL = new CPiPL( lpInfo->hCurrPlugIn ) )) ||
		   ( lpInfo->pPiPL->PiPLGetLastError() != IMG_OK ) ){
		FreeLibrary( lpInfo->hCurrPlugIn ) ;
		lpInfo->hCurrPlugIn = NULL ;
		return lpInfo->pPiPL->PiPLGetLastError() ;
//		return IMGPI_ERRORLOADINGPLUGIN ;
	}
	lpInfo->ENTRYPOINT = (ENTRYPROC)GetProcAddress( lpInfo->hCurrPlugIn, lpInfo->pPiPL->EntryProc() ) ;
	if ( ! lpInfo->ENTRYPOINT ) {
		if ( lpInfo->pPiPL ) {
			delete lpInfo->pPiPL ;
			lpInfo->pPiPL = NULL ;
		}
		if ( lpInfo->pFRecord ) {
			GlobalFreePtr ( lpInfo->pFRecord ) ;
			lpInfo->pFRecord = NULL ;
		}
		FreeLibrary( lpInfo->hCurrPlugIn ) ;
		lpInfo->hCurrPlugIn = NULL ;
		return IMGPI_ERRORLOADINGPLUGIN ;
	}
	if ( (dErr = lpInfo->pPiPL->PiPLGetLastError()) == 0 )
		dErr = IMG_OK ;
	return ( dErr ) ;
}

long IMAPI PIIMPlugInAbout ( HWND hOwner, LPSTR lpPlugInName )
{
	LPPITHREADINFO lpInfo = GetPIThreadInfo () ;
	short sRes ;
	HINSTANCE hPI ;
	CPiPL * pPiPL ;
	AboutRecord ARecord ;
	PlatformData pData ;
	char PIName[ MAX_PATH ] ;
	long lpiData = 0 ;
	LPSTR lpEntryName ;
	ENTRYPROC ENTRYPOINT ;
	if ( ! lpPlugInName ) {
		if ( ! lpInfo->hCurrPlugIn )
			return ( IMGPI_NOPLUGINSELECTED ) ;
		else
			hPI = lpInfo->hCurrPlugIn ;
	} else {
		if ( ! (hPI = LoadLibrary(MakePIName(lpPlugInName, PIName))) )
			return ( IMGPI_ERRORLOADINGPLUGIN ) ;
	}
	// at this point we have either already returned from a previous error
	// or we are now ready to fill in the filterRecord and invoke about !!!
	_fmemset( &ARecord, 0x00, sizeof(AboutRecord) ) ;

	if ( !(pPiPL = new CPiPL ( hPI )) )
		return ( IMGPI_ERRORLOADINGPLUGIN ) ;
	pData.hwnd = (long)hOwner ;
	ARecord.platformData = & pData ;
	if ( !(lpEntryName = pPiPL->EntryProc()) ) {
		delete pPiPL ;
		return ( IMGPI_ERRORLOADINGPLUGIN ) ;
	}
	if ( !(ENTRYPOINT = (ENTRYPROC)GetProcAddress( hPI, lpEntryName )) ) {
		delete pPiPL ;
		return ( IMGPI_ERRORLOADINGPLUGIN ) ;
	}
	ENTRYPOINT( filterSelectorAbout, &ARecord, &lpiData, &sRes) ;

	if ( pPiPL )
		delete pPiPL ;
	if ( ( hPI != lpInfo->hCurrPlugIn ) && ( hPI ) )
		FreeLibrary( hPI ) ;

	return ( IMG_OK ) ;
}

TestAbortProc PIIMTestAbortProc ()
{
	LPPITHREADINFO lpInfo = GetPIThreadInfo () ;
	return ( (TestAbortProc)lpInfo->bCancelFilter ) ;
}
/*
ColorServicesProc PIIMColorServicesProc ( ColorServicesInfo * Info )
{
	return noErr ;
}
*/
ProcessEventProc   PIIMProcessEvent ( LPSTR er )
{
	return noErr ;
}

HostProc PIIMHostProc ( int16 selector, int32 * data )
{
	int16 ixx = selector ;
	return 0;
}

DisplayPixelsProc  PIIMDisplayPixelsProc 
		( const PSPixelMap * source, const VRect *srcrect,
			int32 dstRow, int32 dstCol, unsigned32 platcxt )
{
	LPPITHREADINFO lpInfo = GetPIThreadInfo () ;
	PDIB pDIB ;
	HANDLE hImage, hDrawImage = NULL;
	RECT   rectDraw ;
	LPSTR lpBits, lpInBits ;
	long lWidth, iInRow ;
	DWORD dRowSize ;
	long   lColors = 1L << GetDeviceCaps((HDC)platcxt, BITSPIXEL) * GetDeviceCaps((HDC)platcxt,PLANES) ;

	if (source->version > 1) 
		return 0 ;

	if ( !(pDIB = new DIB ( srcrect->right, srcrect->bottom, 24, TRUE )) )
		return 0 ;

	lWidth = pDIB->GetWidth() * 3 ;
	SwapRBs ( (LPSTR)source->baseAddr, pDIB->GetHeight(), source->rowBytes, source->rowBytes) ;

	lpBits = pDIB->GetDataPtr();
	dRowSize = pDIB->GetRowSize () ;
	lpInBits = (LPSTR)source->baseAddr + (source->rowBytes * (pDIB->GetHeight()-1));

	// copy and invert the image from source to pDIB
	for ( iInRow = 0 ; iInRow < pDIB->GetHeight(); iInRow++ ) {
		memcpy ( lpBits, lpInBits, lWidth ) ; 
		lpInBits  -= source->rowBytes;
		lpBits += dRowSize ;
	}

	hImage = ImgFromPDIB( pDIB );

	rectDraw.top  = dstRow ;
	rectDraw.left = dstCol ;
	rectDraw.right  = dstCol + srcrect->right - 1 ;
	rectDraw.bottom = dstRow + srcrect->bottom - 1 ;
	if ( lColors <= 256 ) {
		HPALETTE hPal ;
		if ( !(hDrawImage = ImgReduceColors( hImage, lColors, IMG_FIXEDPALETTE | IMG_FLOYD )) ) {
			ImgClose( hImage ) ;
			return 0 ;
		}
		if ( hPal = (HPALETTE)ImgGetPalette ( hDrawImage ) ) {
		   	HPALETTE hOldPal;
		   					
			hOldPal = SelectPalette( (HDC)platcxt, hPal, TRUE ) ;
			RealizePalette ( (HDC)platcxt ) ;
			ImgDrawImage ( hDrawImage, (HDC)platcxt, & rectDraw, NULL ) ;
			DeleteObject ( SelectPalette((HDC)platcxt, hOldPal, FALSE )  ) ;
		} else {
			ImgClose( hImage ) ;
			ImgClose ( hDrawImage ) ;
			return 0 ;
		}

		ImgClose ( hDrawImage ) ;
	} else
		if ( ImgDrawImage ( hImage, (HDC)platcxt, &rectDraw, NULL ) != IMG_OK ) {
			ImgClose( hImage ) ;
			return 0 ;
		}

	if( hDrawImage )
		ImgClose( hDrawImage );

	ImgClose ( hImage ) ;

	return noErr ;
}

void PackInvertAndSwap ( LPSTR pTarget, LPSTR pSource, PDIB pDIB )
{
	long  lInPos, lOutPos,  lInRow, lRowWidth ;
	LPSTR lpBits, lpInBits ;
	LPSTR lpOutBits = pTarget ;

	// to "un-pad" and pack the bitmap bytes.
	lpBits   = pDIB->GetDataPtr () ;
	lInPos = lOutPos = 0 ;
	lRowWidth = pDIB->GetWidth() * 3 ;
	for ( lInRow = 0 ; lInRow < pDIB->GetHeight() ; lInRow ++ ) 
		{
			lpInBits  = (LPSTR)lpBits + lInPos ;
			memcpy ( pTarget, lpInBits, lRowWidth ) ;
			SwapRBs ( pTarget, 1, lRowWidth, lRowWidth) ;
			pTarget += lRowWidth ;
			lInPos  += pDIB->GetRowSize() ;		// the padded width
		}
	InvertDIB ( (LPSTR)lpOutBits, pDIB->GetHeight(), lRowWidth )	;
}

void ProcessOutRect ( Rect rectOut, PDIB pDIBIn, LPSTR lpOutData )
{
	LPPITHREADINFO lpInfo = GetPIThreadInfo() ;
	FilterRecord * pFR = lpInfo->pFRecord ;
	LPSTR lpBits, lpOutBits = lpOutData ;
	long lOutWidth = ( rectOut.right - rectOut.left ) ;
	long outHeight = ( rectOut.bottom - rectOut.top ) ;
	long outRowBytes ;
	long lWidth = pDIBIn->GetWidth() * ( pDIBIn->GetBitCount() >> 3 ) ;

	if ( lOutWidth > pDIBIn->GetWidth() )
		lOutWidth = pDIBIn->GetWidth () ;
	if ( outHeight > pDIBIn->GetHeight() )
		outHeight = pDIBIn->GetHeight() ;
	outRowBytes = ( lOutWidth * ( pDIBIn->GetBitCount() >> 3 ) ) ;
/*
PrintfDebugString ( "\nProcessOutRect.....\n" ) ;
PrintfDebugString ( "\tinRect.top %d, .left %d, .bottom %d, .right %d\n",
									 pFR->inRect.top, pFR->inRect.left,
									 pFR->inRect.bottom, pFR->inRect.right ) ;
PrintfDebugString ( "\toutRect.top %d, .left %d, .bottom %d, .right %d\n",
									 pFR->outRect.top, pFR->outRect.left,
									 pFR->outRect.bottom, pFR->outRect.right ) ;
PrintfDebugString ( "\trectOut.top %d, .left %d, .bottom %d, .right %d\n",
									 rectOut.top, rectOut.left,
									 rectOut.bottom, rectOut.right ) ;
*/
	lpBits = pDIBIn->GetDataPtr() ;
	// first, invert the whole stinker
	InvertDIB ( lpBits, pDIBIn->GetHeight(), pDIBIn->GetRowSize() )	;
	// the swap BGR to RGB
	SwapRBs ( lpBits, pDIBIn->GetHeight(), lWidth, pDIBIn->GetRowSize() ) ;
	// now point to where you are really going to move data into
	lpBits = pDIBIn->GetDataPtr() + ( rectOut.top * pDIBIn->GetRowSize() ) +
							( rectOut.left * (pDIBIn->GetBitCount() >> 3) ) ;
	for ( long lRow = 0 ; lRow < outHeight ; lRow ++ ) {
		memcpy ( lpBits, lpOutBits, outRowBytes ) ;
		lpOutBits += outRowBytes ;
		lpBits += pDIBIn->GetRowSize() ;
	}

	lpBits = pDIBIn->GetDataPtr() ;
	SwapRBs ( lpBits, pDIBIn->GetHeight(), lWidth, pDIBIn->GetRowSize() ) ;
	// now we need to re-invert the dib
	InvertDIB ( lpBits, pDIBIn->GetHeight(), pDIBIn->GetRowSize() )	;
}

							long IMAPI PIIMInvokePlugIn  ( HANDLE hImage, PDIB pDIB, HWND hOwner, RECT rect, LPBYTE * lpParams, BOOL bUI ) {
	LPPITHREADINFO lpInfo = GetPIThreadInfo() ;
	long  lData = 0 ;
	LPLONG lpData = & lData ;
	long  lRes = IMG_OK ;
	long  lImgSize, lRowWidth ;
	short sRes ;
	FilterRecord * pFRecord   ;
	PlatformData   pfData     ;
	Rect           rectImg    ;

	if ( ( rect.top    >= rect.bottom ) ||
		   ( rect.left   >= rect.right  ) ||
			 ( rect.right  > pDIB->GetWidth() ) ||
			 ( rect.bottom >= pDIB->GetHeight() ) )
		return IMG_BAD_PARAM ;

	if ( ! lpInfo->hCurrPlugIn )
		return ( IMGPI_NOPLUGINSELECTED ) ;

	if ( !(pFRecord = (FilterRecord*)GlobalAllocPtr ( GHND, sizeof(FilterRecord) )) )
		return ( IMGPI_NOMEM ) ;
	pfData.hwnd = (long)hOwner ;
	pFRecord->platformData = & pfData ;
	pFRecord->planes = 3 ;   // rgb channels.

	lImgSize  = pDIB->GetImageMemSize () ;
	lRowWidth = pDIB->GetWidth() * ( pDIB->GetBitCount() / 8 ) ;

	if ( !(pFRecord->inData = GlobalAllocPtr(GHND, lImgSize)) ) {
		GlobalFreePtr( pFRecord ) ;
		lpInfo->pFRecord = NULL ;
		return IMGPI_NOMEM ;
	}
	if ( !(pFRecord->outData = GlobalAllocPtr ( GHND, lImgSize ) ) ) {
		GlobalFreePtr( pFRecord->inData ) ;
		GlobalFreePtr( pFRecord ) ;
		lpInfo->pFRecord = NULL ;
		return IMGPI_NOMEM ;
	}

	pFRecord->maxSpace = lImgSize * 2 ;
	pFRecord->hostSig    = kPhotoshopSignature ;
	pFRecord->filterCase = filterCaseFlatImageNoSelection ;
	pFRecord->planes     = 3 ;   // rgb channels.
	pFRecord->inHiPlane  = pFRecord->outHiPlane = 2 ;
	pFRecord->imageMode  = plugInModeRGBColor ;
	rectImg.right = pFRecord->wholeSize.v = pFRecord->imageSize.v = (short)pDIB->GetWidth  () ;
	rectImg.bottom = pFRecord->wholeSize.h = pFRecord->imageSize.h = (short)pDIB->GetHeight () ;
	rectImg.top  = rectImg.left = 0 ;
	lpInfo->rectLast.top   = lpInfo->rectLast.bottom   = 0 ;
	lpInfo->rectInLast.top = lpInfo->rectInLast.bottom = 0 ;
	lpInfo->pDIBIn     = NULL ;
	lpInfo->hTile	     = NULL ;
	lpInfo->lWidth	   = rectImg.right ;
	lpInfo->lHeight    = rectImg.bottom ;
	lpInfo->pFRecord   = pFRecord ;
	lpInfo->hImage     = hImage   ;		// save the input image
	lpInfo->pDIBIn     = pDIB     ;
	lpInfo->bFirstTile = TRUE     ;

	pFRecord->filterRect = rectImg ;
	pFRecord->inputRate  = 1 ;

	pFRecord->imageSize.v = rectImg.right ;
	pFRecord->imageSize.h = rectImg.bottom ;

	// first set the addresses for the callbacks
	pFRecord->advanceState  =  (AdvanceStateProc) PIIMAdvanceState      ;
	pFRecord->abortProc     =     (TestAbortProc) PIIMTestAbortProc     ;
//	pFRecord->colorServices = (ColorServicesProc) PIIMColorServicesProc ;
	pFRecord->progressProc  =      (ProgressProc) PIIMProgressProc      ;
	pFRecord->displayPixels = (DisplayPixelsProc) PIIMDisplayPixelsProc ;
	pFRecord->hostProc      =          (HostProc) PIIMHostProc          ;
	pFRecord->processEvent  =  (ProcessEventProc) PIIMProcessEvent      ;

	lpInfo->ENTRYPOINT( filterSelectorParameters, pFRecord, lpData, &sRes) ;
	if ( sRes != 0 ) {	// an error occurred
		if ( pFRecord->outData )
			GlobalFreePtr( pFRecord->outData ) ;
		if ( pFRecord )
			GlobalFreePtr ( pFRecord ) ;
		lpInfo->pFRecord = NULL ;
		return ( IMGPI_PLUGINERROR ) ;
	}

	lpInfo->ENTRYPOINT( filterSelectorPrepare, pFRecord, lpData, &sRes ) ;
	if ( sRes != 0 ) {
		if ( pFRecord )
			GlobalFreePtr ( pFRecord ) ;
		lpInfo->pFRecord = NULL ;
		return ( IMGPI_PLUGINERROR ) ;
	}

	// NOW WE CAN GO FOR IT!!!!
	if ( ! ( bUI ) )
		*lpData = 0 ;
	lpInfo->ENTRYPOINT( filterSelectorStart, pFRecord, lpData, &sRes ) ;

	if ( ( sRes != 0 ) ||
			 ( sRes == 1 ) ) {
		if ( pFRecord->outData )
			GlobalFreePtr ( pFRecord->outData ) ;
		if ( pFRecord->inData )
			GlobalFreePtr ( pFRecord->inData  ) ;
		GlobalFreePtr ( pFRecord ) ;
		lpInfo->pFRecord = NULL ;
		return ( sRes == 1 ? IMGPI_CANCELLED : IMGPI_PLUGINERROR ) ;
	}
	lpInfo->bFirstTile = FALSE ;

	MSG msg ;
	while ( ( sRes == noErr ) &&
		      ( RectsNotEmpty( pFRecord ) ) ) {
		PIIMAdvanceState() ;

		lpInfo->ENTRYPOINT ( filterSelectorContinue, pFRecord, lpData, &sRes ) ;

		if ( lpInfo->rectLast.bottom && lpInfo->rectLast.right ) {
			ProcessOutRect ( lpInfo->rectLast, pDIB, (LPSTR)pFRecord->outData ) ;
			lpInfo->rectLast = pFRecord->outRect ;
		}
		if ( PeekMessage(&msg, NULL, 0,0, PM_NOREMOVE)) {
			TranslateMessage( &msg ) ;
			DispatchMessage( &msg ) ;
		}
	}

	lpInfo->ENTRYPOINT( filterSelectorFinish, pFRecord, lpData, &sRes ) ;

	// we have to do this explicitly as the FF PIs don't send 100%
	if ( lpInfo->lpStat )
		(*lpInfo->lpStat)( lpInfo->hImage, 100, lpInfo->dwStatUserInfo ) ;

	// do this one last time incase anything is still left
	if ( lpInfo->rectLast.bottom && lpInfo->rectLast.right )
		ProcessOutRect ( lpInfo->rectLast, lpInfo->pDIBIn, (LPSTR)pFRecord->outData ) ;

	if ( lpInfo->hTile ) {
		ImgClose ( lpInfo->hTile ) ;
		lpInfo->hTile = NULL ;
	}

	if ( pFRecord->outData )
		GlobalFreePtr ( pFRecord->outData ) ;
	if ( pFRecord->inData )
		GlobalFreePtr ( pFRecord->inData  ) ;
	GlobalFreePtr ( pFRecord ) ;
	lpInfo->pFRecord = NULL ;

	// check to see if we should send back the parameters
	if ( lpParams )
		*lpParams = (LPBYTE) (lpData) ;

	return ( IMG_OK ) ;				//  yeah !!!!
}

BOOL NewInRect ( Rect * in )
{
	BOOL bNew ;
	LPPITHREADINFO lpInfo = GetPIThreadInfo() ;
	Rect last = lpInfo->rectInLast ;
	bNew = ( ( in->top    != last.top    ) ||
		       ( in->left   != last.left   ) ||
					 ( in->bottom != last.bottom ) ||
					 ( in->right  != last.right  ) ) ;
	if ( bNew ) 
		lpInfo->rectInLast = * in ;
	return bNew ;
}

int GetImageTile ()
{
	long lImgSize, lWidth, lHeight ;
	RECT rect ;
	LPPITHREADINFO lpInfo = GetPIThreadInfo() ;
	PDIB pDIB, pDIBIn = lpInfo->pDIBIn ;
	FilterRecord * pFR = lpInfo->pFRecord ;
	HANDLE hImage = lpInfo->hImage ;
	HANDLE hTile ;

	if ( lpInfo->bCancelFilter )
		return userCanceledErr ;

	lWidth  = pFR->inRect.right - pFR->inRect.left ;
	lHeight = pFR->inRect.bottom - pFR->inRect.top ;
	rect.top    = pFR->inRect.top    ;
	rect.left   = pFR->inRect.left   ;
	rect.bottom = pFR->inRect.bottom - 1 ;
	rect.right  = pFR->inRect.right  - 1 ;

	if ( ( NewInRect( & pFR->inRect ) ) &&
	     (( pFR->inRect.right  ) &&
		    ( pFR->inRect.bottom )) ) {
		if ( lpInfo->hTile )
			ImgClose ( lpInfo->hTile ) ;
		hTile = ImgCopy ( hImage, lWidth, lHeight, 
			                lpInfo->bFirstTile ? NULL : &rect, COPY_INTERPOLATE ) ;
		if ( ! ( lpInfo->hTile = hTile ) )  // assignment = not == on purpose
			return 0 ;  // if we can't get a scaled image!
	} else
		hTile = lpInfo->hTile ;

	if ( ! (pDIB = new DIB( ImgGetDIB(hTile,TRUE,NULL), TRUE )) )
		return 0 ;  // if we can't get the dib!

	lImgSize = ( pDIB->GetBitCount() >> 3 ) * lWidth * lHeight ;

	PackInvertAndSwap ( (LPSTR)pFR->inData, pDIB->GetDataPtr(), pDIB ) ;

	pFR->inRowBytes = pFR->outRowBytes = lWidth * ( pDIB->GetBitCount() >> 3 ) ;
	if ( ( ! lpInfo->bFirstTile ) ) {
		if ( lpInfo->rectLast.bottom && lpInfo->rectLast.right )
			ProcessOutRect ( lpInfo->rectLast, pDIBIn, (LPSTR)pFR->outData ) ;
		lpInfo->rectLast = pFR->outRect ;
	} else
		lpInfo->bFirstTile = FALSE ;

	delete pDIB ;
	return IMG_OK ;
}

AdvanceStateProc PIIMAdvanceState ()
{
	LPPITHREADINFO lpInfo = GetPIThreadInfo() ;
	if ( ! GetImageTile() ) {
		lpInfo->rectInLast.top = -1 ;
		return 0 ;			// Get a tile fails
	}
	return noErr ;
}

#pragma optimize ( "y", off )
long IMAPI PIIMEnumerate ( PIIMENUMCALLBACKPROC enumProc, LONG lType, LONG lParam )
{
	LPPITHREADINFO lpInfo = GetPIThreadInfo() ;
	CPiPL * pPIPL ;
	LPSTR lpExt ;
	char FileName [ MAX_PATH ] ;
	HANDLE hFind ;
	WIN32_FIND_DATA FindData ;
	long lErr = 0 ;
	BOOL bContinue = TRUE ;

	switch ( lType )
		{
			case IM_PI_FILTER :
				lpExt = szPlugInExt[ 5 ] ;
				break ;
			case IM_PI_COLORPICKER :
				lpExt = szPlugInExt[ 1 ] ;
				break ;
			case IM_PI_IMPORT :
				lpExt = szPlugInExt[ 2 ] ;
				break ;
			case IM_PI_EXPORT :
				lpExt = szPlugInExt[ 3 ] ;
				break ;
			case IM_PI_EXTENSION :
				lpExt = szPlugInExt[ 4 ] ;
				break ;
			case IM_PI_FILEFORMAT :
				lpExt = szPlugInExt[ 6 ] ;
				break ;
			case IM_PI_PARSER :
				lpExt = szPlugInExt[ 7 ] ;
				break ;
			case IM_PI_SELECTION :
				lpExt = szPlugInExt[ 8 ] ;
				break ;
			default :
			case IM_PI_ANYTYPE :
				lpExt = "*.8B?" ;
				break ;
		}
	MakePIName ( lpExt, FileName ) ;
	if ( (hFind = FindFirstFile( FileName, &FindData )) == (HANDLE)INVALID_HANDLE_VALUE )
		return IMGPI_ERRORENUMERATING ;

	if ( PIIMLoadSpecific ( FindData.cFileName ) == IMG_OK ) {
		pPIPL = lpInfo->pPiPL ;
		if ( ! ( (*enumProc)(pPIPL->LongName(), FindData.cFileName, pPIPL->Category(), lParam)) ) {
			FindClose ( hFind ) ;
			return IMG_OK ;
		}
	}

	do
		{
			if ( FindNextFile ( hFind, & FindData ) ) {
				if ( PIIMLoadSpecific ( FindData.cFileName ) == IMG_OK ) {
					pPIPL = lpInfo->pPiPL ;
					bContinue = (*enumProc)(pPIPL->LongName(), FindData.cFileName, pPIPL->Category(), lParam)  ;
				}
			} else
				lErr = GetLastError() ;
		}
	while
		( ( lErr != ERROR_NO_MORE_FILES  ) &&
		  ( lErr != ERROR_INVALID_HANDLE ) && 
			( bContinue ) ) ;
	FindClose ( hFind ) ;

	return IMG_OK ;
}
#pragma optimize ( "y", on )

ProgressProc PIIMProgressProc ( int32 done, int32 total )
{
	LPPITHREADINFO lpInfo = GetPIThreadInfo () ;
	FilterRecord * pFR = lpInfo->pFRecord ;
	int iPerc ;

	lpInfo->bFirstTile = FALSE ;	// turn this on so AdvanceState won't call the preview setup

	if ( ! lpInfo->lpStat )		// if they ain't one set get out!
		return FALSE ;

	iPerc = total ? (done*100) / total : 0 ;
	lpInfo->bCancelFilter = ((*lpInfo->lpStat)( lpInfo->hImage, iPerc, lpInfo->dwStatUserInfo ) == 0 ) ;
	return FALSE ;	// false says continue, true says abort
}

long IMAPI PIIMSetStatusProc ( STATUSPROC lpStat, LONG lInterval, DWORD dwUser )
{
	LPPITHREADINFO lpInfo = GetPIThreadInfo () ;
	
	lpInfo->lpStat         = lpStat    ;
	lpInfo->lInterval      = lInterval ;
	lpInfo->dwStatUserInfo = dwUser    ;
	return IMG_OK ;
}
