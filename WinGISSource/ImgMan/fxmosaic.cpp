/////////////////////////////////////////////////////////////////////////////
//
// fxmosaic.c
//
// ImageMan Effects Module: Mosaic and Creeping Mosaic
// Exports:
//   doMosaic
//   doCreepX
//   doCreepY
//
// Version 1.0
// Copyright (c) 1996 Data Techniques, Inc.
// Copyright (c) 1996 Chris Roueche
// All Rights Reserved
//
/////////////////////////////////////////////////////////////////////////////
// $History: fxmosaic.cpp $
// 
// *****************  Version 2  *****************
// User: Johnd        Date: 4/10/98    Time: 5:06p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Chgd Error Returns to be consistent with other ImageMan Defines
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 7/10/97    Time: 2:03p
// Created in $/imageman 16-32/dll-api level 2
// Initial Lvl 2 Checkin
//

#define STRICT
#define WIN32_LEAN_AND_MEAN
#define NOSERVICE
#include <windows.h>
#include <windowsx.h>
#include "imgman.h"
#include "fxparam.h"
#pragma hdrstop
#include <stdlib.h>

// Mosaic-specific storage can be found in fxparam.h:tag_Mosaic

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
//
// doMosaic
//
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

int doMosaic(HDC hDC,PEFFECTBLK peb)
{
	int x,y;
	RECT rclip;
	register LPBYTE prow;

	if (INITIALIZE(peb)) {
		// initialize mosaic (loword is cell width, hiword is cell height)
		if ((peb->mosaic.cell.cx = LOWORD(peb->dwExtra)) == 0) peb->mosaic.cell.cx = 5;
		if ((peb->mosaic.cell.cy = HIWORD(peb->dwExtra)) == 0) peb->mosaic.cell.cy = peb->mosaic.cell.cx;

		peb->mosaic.grid.cx = ((peb->rDst.right-peb->rDst.left)+peb->mosaic.cell.cx-1)/peb->mosaic.cell.cx;
		peb->mosaic.grid.cy = ((peb->rDst.bottom-peb->rDst.top)+peb->mosaic.cell.cy-1)/peb->mosaic.cell.cy;

		// Find DWORD aligned byte width of one map row.
		peb->mosaic.bwidth = ((peb->mosaic.grid.cx+31)/32)*4;
		// Allocate an array of counters I'll use to sense completed rows,
		// and enough space for a row-major bit map so I can easily sense 
		// completed cells.
		peb->mosaic.pused = (LPINT)GlobalAllocPtr(GHND,
			(DWORD)(peb->mosaic.bwidth+sizeof(int))*peb->mosaic.grid.cy);
		if (!peb->mosaic.pused) return (IMGFX_OUTOFMEMORY); // fail if no memory

		peb->nStepsTotal = peb->mosaic.grid.cx*peb->mosaic.grid.cy;
	}
	if (CLEANUP(peb)) {
		GlobalFreePtr(peb->mosaic.pused);
		return (IMG_OK);
	}

	// find row with an unset cell
	do {
		y = rand()%peb->mosaic.grid.cy;
	} while (peb->mosaic.pused[y] == peb->mosaic.grid.cx);
	peb->mosaic.pused[y]++;

	// find an unset cell in this row
	prow = (LPBYTE)(peb->mosaic.pused+peb->mosaic.grid.cy)+y*peb->mosaic.bwidth;
	do {
		x = rand()%peb->mosaic.grid.cx;
	} while (*(prow+(UINT)x/8)&(0x0080U>>(x&7)));
	// mark this cell done
	*(prow+(UINT)x/8) |= (0x0080U>>(x&7));

	rclip.right = (rclip.left = peb->rDst.left+x*peb->mosaic.cell.cx)+peb->mosaic.cell.cx;
	rclip.bottom = (rclip.top = peb->rDst.top+y*peb->mosaic.cell.cy)+peb->mosaic.cell.cy;
	clipblt(hDC,peb,&rclip);

	peb->nStepsDone++;

	return (IMG_OK);
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
//
// doCreepX
//
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

int doCreepX(HDC hDC,PEFFECTBLK peb)
{
	int x,y;
	RECT rclip;
	register LPBYTE pcol;

	if (INITIALIZE(peb)) {
		// initialize mosaic (loword is cell size, hiword is pixel fill %)
		if ((peb->mosaic.cell.cx = LOWORD(peb->dwExtra)) == 0) peb->mosaic.cell.cx = 1;
		peb->mosaic.cell.cy = peb->mosaic.cell.cx;

		peb->mosaic.grid.cx = ((peb->rDst.right-peb->rDst.left)+peb->mosaic.cell.cx-1)/peb->mosaic.cell.cx;
		peb->mosaic.grid.cy = ((peb->rDst.bottom-peb->rDst.top)+peb->mosaic.cell.cy-1)/peb->mosaic.cell.cy;

		// turn fill percent into a pixel count
		if ((peb->dwExtra = HIWORD(peb->dwExtra)) > 100) peb->dwExtra = 100;
		peb->dwExtra = (peb->dwExtra*peb->mosaic.grid.cy)/100;

		// Find DWORD aligned byte width of one map column.
		peb->mosaic.bwidth = ((peb->mosaic.grid.cy+31)/32)*4;
		// Allocate an array of counters I'll use to sense completed columns,
		// and enough space for a column-major bit map so I can easily sense 
		// completed cells.
		peb->mosaic.pused = (LPINT)GlobalAllocPtr(GHND,
			(DWORD)(peb->mosaic.bwidth+sizeof(int))*peb->mosaic.grid.cx);
		if (!peb->mosaic.pused) return (IMGFX_OUTOFMEMORY); // fail if no memory

		// Setup creep direction and initial grid-relative bounds
		peb->mosaic.extent = 1;
		peb->mosaic.edge = (peb->nEffect == IMFX_MOSAICLEFT) 
			? peb->mosaic.grid.cx-1 : 0;
		peb->mosaic.nfilled = 0;

		peb->nStepsTotal = peb->mosaic.grid.cx*peb->mosaic.grid.cy;
	}
	if (CLEANUP(peb)) {
		GlobalFreePtr(peb->mosaic.pused);
		return (IMG_OK);
	}

	// find an in-bounds column with an unset cell
	do {
		x = rand()%peb->mosaic.extent+peb->mosaic.edge;
	} while (peb->mosaic.pused[x] == peb->mosaic.grid.cy);
	peb->mosaic.pused[x]++;

	// find an unset cell in this column
	pcol = (LPBYTE)(peb->mosaic.pused+peb->mosaic.grid.cx)+x*peb->mosaic.bwidth;
	do {
		y = rand()%peb->mosaic.grid.cy;
	} while (*(pcol+(UINT)y/8)&(0x0080U>>(y&7)));
	// mark this cell done
	*(pcol+(UINT)y/8) |= (0x0080U>>(y&7));

	// draw the cell
	rclip.right = (rclip.left = peb->rDst.left+x*peb->mosaic.cell.cx)+peb->mosaic.cell.cx;
	rclip.bottom = (rclip.top = peb->rDst.top+y*peb->mosaic.cell.cy)+peb->mosaic.cell.cy;
	clipblt(hDC,peb,&rclip);

	// update active edge
	if (peb->mosaic.extent < peb->mosaic.grid.cx) {
		if (peb->nEffect == IMFX_MOSAICLEFT) {
			if (x == peb->mosaic.edge) {
				if (++peb->mosaic.nfilled >= (int)peb->dwExtra) {
					peb->mosaic.edge--;
					peb->mosaic.extent++;
					peb->mosaic.nfilled = 0;
				}
			}
		}
		else if (x == peb->mosaic.edge+peb->mosaic.extent-1) {
			if (++peb->mosaic.nfilled >= (int)peb->dwExtra) {
				peb->mosaic.extent++;
				peb->mosaic.nfilled = 0;
			}
		} 
	}

	peb->nStepsDone++;

	return (IMG_OK);
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
//
// doCreepY
//
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

int doCreepY(HDC hDC,PEFFECTBLK peb)
{
	int x,y;
	RECT rclip;
	register LPBYTE prow;

	if (INITIALIZE(peb)) {
		// initialize mosaic (loword is cell size, hiword is pixel fill %)
		if ((peb->mosaic.cell.cx = LOWORD(peb->dwExtra)) == 0) peb->mosaic.cell.cx = 1;
		peb->mosaic.cell.cy = peb->mosaic.cell.cx;

		peb->mosaic.grid.cx = ((peb->rDst.right-peb->rDst.left)+peb->mosaic.cell.cx-1)/peb->mosaic.cell.cx;
		peb->mosaic.grid.cy = ((peb->rDst.bottom-peb->rDst.top)+peb->mosaic.cell.cy-1)/peb->mosaic.cell.cy;

		// turn fill percent into a pixel count
		if ((peb->dwExtra = HIWORD(peb->dwExtra)) > 100) peb->dwExtra = 100;
		peb->dwExtra = (peb->dwExtra*peb->mosaic.grid.cy)/100;

		// Find DWORD aligned byte width of one map row.
		peb->mosaic.bwidth = ((peb->mosaic.grid.cx+31)/32)*4;
		// Allocate an array of counters I'll use to sense completed rows,
		// and enough space for a row-major bit map so I can easily sense 
		// completed cells.
		peb->mosaic.pused = (LPINT)GlobalAllocPtr(GHND,
			(DWORD)(peb->mosaic.bwidth+sizeof(int))*peb->mosaic.grid.cy);
		if (!peb->mosaic.pused) return (IMGFX_OUTOFMEMORY); // fail if no memory

		// Setup creep direction and initial grid-relative bounds
		peb->mosaic.extent = 1;
		peb->mosaic.edge = (peb->nEffect == IMFX_MOSAICUP) 
			? peb->mosaic.grid.cy-1 : 0;
		peb->mosaic.nfilled = 0;

		peb->nStepsTotal = peb->mosaic.grid.cx*peb->mosaic.grid.cy;
	}
	if (CLEANUP(peb)) {
		GlobalFreePtr(peb->mosaic.pused);
		return (IMG_OK);
	}

	// find an in-bounds row with an unset cell
	do {
		y = rand()%peb->mosaic.extent+peb->mosaic.edge;
	} while (peb->mosaic.pused[y] == peb->mosaic.grid.cx);
	peb->mosaic.pused[y]++;

	// find an unset cell in this row
	prow = (LPBYTE)(peb->mosaic.pused+peb->mosaic.grid.cy)+y*peb->mosaic.bwidth;
	do {
		x = rand()%peb->mosaic.grid.cx;
	} while (*(prow+(UINT)x/8)&(0x0080U>>(x&7)));
	// mark this cell done
	*(prow+(UINT)x/8) |= (0x0080U>>(x&7));

	// draw the cell
	rclip.right = (rclip.left = peb->rDst.left+x*peb->mosaic.cell.cx)+peb->mosaic.cell.cx;
	rclip.bottom = (rclip.top = peb->rDst.top+y*peb->mosaic.cell.cy)+peb->mosaic.cell.cy;
	clipblt(hDC,peb,&rclip);

	// update active edge
	if (peb->mosaic.extent < peb->mosaic.grid.cy) {
		if (peb->nEffect == IMFX_MOSAICUP) {
			if (y == peb->mosaic.edge) {
				if (++peb->mosaic.nfilled >= (int)peb->dwExtra) {
					peb->mosaic.edge--;
					peb->mosaic.extent++;
					peb->mosaic.nfilled = 0;
				}
			}
		}
		else if (y == peb->mosaic.edge+peb->mosaic.extent-1) {
			if (++peb->mosaic.nfilled >= (int)peb->dwExtra) {
				peb->mosaic.extent++;
				peb->mosaic.nfilled = 0;
			}
		} 
	}

	peb->nStepsDone++;

	return (IMG_OK);
}
