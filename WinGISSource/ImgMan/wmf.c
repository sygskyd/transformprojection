/////////////////////////////////////////////////////////////////////////////
//
// wmf.c
//
// Windows Metafile DIL Module
//
// ImageMan Image Processing Library
//
// Copyright (c) 1991-5 Data Techniques, Inc.
// All Rights Reserved
//
// ///////////////////////////////////////////////////////////////////////////
// $History: WMF.C $
// 
// *****************  Version 2  *****************
// User: Timk         Date: 6/30/95    Time: 12:05p
// Updated in $/ImageMan 16-32/DILS
// Part 1 of massive changes for read/write using function pointer blocks.
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 1/18/95    Time: 10:21a
// Created in $/ImageMan 16-32/DILS
// ImageMan 4.0 Beta 1

#include <string.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>

#include "internal.h"
#include "wmf.h"                // Metafile related Structures
#include "wmferr.h"					// Minor Error Class Defines


typedef struct tagFileStruct {
	LPVOID		lpOpenInfo;
	APMFILEHEADER hdr;			  // Placeable Metafile  header
	BOOL        bAPM;				  // 1 if Placeable Metafile
	long        lOffset;			  // Offset in file
	long        lLength;			  // len of metafile
}           IMAGESTRUCT;

typedef IMAGESTRUCT FAR *LPIMAGESTRUCT;
typedef IMAGESTRUCT NEAR *NPIMAGESTRUCT;

#ifdef WIN32
#define SELECTOROF(x) (x)
#endif

#define VERSION "Windows Metafile Reader V1.1"
#ifdef WIN32

#ifdef WINGIS // so the file TLSByJHC.h was included

#ifdef NDEBUG
#define SETERROR(E) bJHCTLSSetValue((DWORD)(E))
#else
static void IM2WMFerr(int);
#define SETERROR(E) { \
	SETERROR(E) bJHCTLSSetValue((DWORD)(E)); \
	if ((E) != IMG_OK) IM2WMFerr(E); \
	}
#endif // NDEBUG

#define GETERROR    nJHCTLSGetValue()

#else // WINGIS

static DWORD TlsIndex;			  // Thread local storage index
#ifdef NDEBUG
#define SETERROR(E) TlsSetValue(TlsIndex,(LPVOID)(DWORD)(E))
#else
static void IM2WMFerr(int);
#define SETERROR(E) { \
	TlsSetValue(TlsIndex,(LPVOID)(DWORD)(E)); \
	if ((E) != IMG_OK) IM2WMFerr(E); \
	}
#endif
#define GETERROR    ((DWORD)TlsGetValue(TlsIndex))

#endif // WINGIS

#else
static int  nErrID;				  // id of latest error
#define SETERROR(E) nErrID = (E)
#define GETERROR    (nErrID)
#endif

HANDLE      hInst;


#ifdef WIN32
// /////////////////////////////////////////////////////////////////////////
//
// DllMain
//
// This is the Win32 'DllEntryPoint'. It is similar in function to LibMain
// and the WEP in 16-bit land, but is much more thorough. LibMain is
// called whenever a process or thread attaches or detaches to the
// BMP DIL
//
// Entry:
// hinstDLL  - handle to the ImageMan DLL
// fdwReason - notification code (attach/detach)
// DLL_PROCESS_ATTACH
// DLL_PROCESS_DETACH
// DLL_THREAD_ATTACH
// DLL_THREAD_DETACH
// lpvReserved - NULL during dynamic process attach/detach, non-NULL
// during static attach/detach.
// Exit:
// DLL_PROCESS_ATTACH - TRUE for success, FALSE for failure
// all others         - return code ignored
// /////////////////////////////////////////////////////////////////////////

BOOL APIENTRY DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
	switch (fdwReason) {

		// The DLL is attaching to a process, due to LoadLibrary.
		case DLL_PROCESS_ATTACH:
			assert(!lpvReserved);  // must always be loaded via LoadLibrary!

			hInst = hinstDLL;		  // save our identity
#ifndef WINGIS
			// Allocate a "Thread Local Storage" (TLS) index right away.

			if ((TlsIndex = TlsAlloc()) == 0xFFFFFFFF)
				return (FALSE);
#else
			bJHCTLSClear();
#endif
			// Fall thru: Initialize the index for first thread.

		// The attached process created a new thread.
		case DLL_THREAD_ATTACH:
#ifndef WINGIS
			// Initialize the TLS index for this thread... it's actually 'nErrID'
			if (!TlsSetValue(TlsIndex, (LPVOID) 0))
				return (FALSE);
#else
			nJHCTLSAlloc(); // Stamp this thread to be existed
#endif
			break;

		// The thread of the attached process terminates.
		case DLL_THREAD_DETACH:
#ifdef WINGIS
			bJHCTLSFree(); // Free this thread entry
#endif
			break;

		// The DLL is detaching from a process, due to a call to FreeLibrary.
		case DLL_PROCESS_DETACH:
#ifndef WINGIS
			// Release the TLS index.
			TlsFree(TlsIndex);
#else
			bJHCTLSClear();
#endif
			break;
	}

	return (TRUE);

	UNREFERENCED_PARAMETER(lpvReserved);
}

#else // WIN32
int FAR PASCAL LibMain( HANDLE Instance, WORD wDataSeg, WORD wHeapSize,\
								LPSTR lpCmdLine)
{
	if (wHeapSize == 0)			  // no heap indicates fatal error
		return 0;

	return 1;
}
#endif

VOID WINAPI WEP(int bSystemExit)
{
	if (bSystemExit) {			  // if exiting Windows entirely

	}
}




int IMAPI   IMDLescape(HANDLE hImage, int nInCnt, LPVOID lpIn, LPVOID lpOut)
{
	return IMG_NSUPPORT;
}

int IMAPI   IMDLverify_img(LPSTR lpVerfBuf, LPVOID lpHandle, LPIMG_IO lpIO)
{
	APMFILEHEADER FAR *lpapmHdr = (APMFILEHEADER FAR *) lpVerfBuf;
	if (lpapmHdr->key != 0x9AC6CDD7)
		return 0;
	else
		return 1;
}

// //////////////////////////////////////////////////////////////////////////
//
// int FAR PASCAL IMDLinit_image( LPSTR, LPINT, int, long )
//
// Create a temporary file containing a real windows metafile which can
// be played with PlayMetafile(). Save the tmp filename.
//
// //////////////////////////////////////////////////////////////////////////

int IMAPI   IMDLinit_image(LPCSTR lpOpenInfo, LPHANDLE hImage, LPVOID lpHandle, long offset, long lLen, LPIMG_IO lpIO)
{
	LPIMAGESTRUCT lpImage;
	METAFILEHEADER mfHdr;
	APMFILEHEADER apmHdr;
	BOOL        bAPM;

	// Read the Placeable Header Struct.

	lpIO->seek(lpHandle, offset, 0);
	if (lpIO->read(lpHandle, (LPSTR) & apmHdr, sizeof(APMFILEHEADER)) != sizeof(APMFILEHEADER)) {
		SETERROR(IDS_BADREAD);
		lpIO->close(lpHandle);
		return IMG_FILE_ERR;
	}
	// Check the signature for Placeable WMF
	// If Not Placeable then seek back to beg of file and read WMF Header

	if (apmHdr.key != 0x9AC6CDD7) {
		lpIO->close(lpHandle);

		SETERROR(IDS_NOTPLACEABLE);

		return IMG_INV_FILE;
	} else
		bAPM = 1;


	// Read the metafile header

	if (lpIO->read(lpHandle, (LPSTR) & mfHdr, sizeof(mfHdr)) != sizeof(mfHdr)) {

		lpIO->close(lpHandle);

		SETERROR(IDS_BADREAD);

		return IMG_FILE_ERR;
	}
	// Verify that its a Metafile 1st Word s/b 2 or 1
	// 1 == Memory Based,  2 == Disk Based Metafile

	if (mfHdr.mtType != 2 && mfHdr.mtType != 1) {
		SETERROR(IDS_BADMETAFILE);
		lpIO->close(lpHandle);
		return IMG_INV_FILE;
	}
	// If not embedded then close the file

	lpIO->close(lpHandle);

	// Allocate the Image Structure

	*hImage = GlobalAlloc(DLLHND, sizeof(IMAGESTRUCT));

	if (!*hImage) {
		SETERROR(IDS_NOMEM);

		return IMG_NOMEM;
	}
	lpImage = (LPIMAGESTRUCT) GlobalLock( *hImage);

	// Save the tmp filename & header info

	lpImage->lOffset = offset;
	lpImage->lLength = lLen;

	lpImage->bAPM = bAPM;
	_fmemcpy(&lpImage->hdr, &apmHdr, sizeof(apmHdr));
	lpImage->lpOpenInfo = lpOpenInfo;

	GlobalUnlock(*hImage);

	return IMG_OK;
}

int IMAPI   IMDLimg_name(HANDLE hImage, HANDLE FAR * lpHand)
{
	LPIMAGESTRUCT lpImage;

	if ((lpImage = (LPIMAGESTRUCT) GlobalLock(hImage)) == NULL) {
		SETERROR(IDS_INVHAND);
		return IMG_INV_HAND;
	}
//	*lpHand = (HANDLE) GlobalHandle(SELECTOROF(lpImage->lpName));

	return IMG_OK;
}

// //////////////////////////////////////////////////////////////////////////
//
// int FAR PASCAL IMDLopen( HANDLE hFile, int fHand )
//
// Does nothing.
//
// //////////////////////////////////////////////////////////////////////////


int IMAPI   IMDLopen(HANDLE hFile, LPVOID lpHandle)
{
	return IMG_OK;
}


// //////////////////////////////////////////////////////////////////////////
//
// int FAR PASCAL IMDLclose_image( HANDLE hImage )
//
// Delete the tmp metafile from disk.
//
// //////////////////////////////////////////////////////////////////////////

int IMAPI   IMDLclose_image(HANDLE hImage)
{

	GlobalUnlock(hImage);
	GlobalFree(hImage);

	return IMG_OK;
}


// /////////////////////////////////////////////////////////////////////////
//
// int FAR PASCAL IMDLclose_keep( HANDLE hFile )
//
// does nothing
//
// /////////////////////////////////////////////////////////////////////////

int IMAPI   IMDLclose_keep(HANDLE hFile)
{
	return IMG_OK;
}



int IMAPI   IMDLbuildDIBhead(HANDLE hImage, LPINTERNALINFO lpInfo)
{
	LPIMAGESTRUCT lpImage;
	LPBITMAPINFOHEADER lpbi;
	HANDLE      hMem;

	if ((lpImage = (LPIMAGESTRUCT) GlobalLock(hImage)) == NULL) {
		SETERROR(IDS_INVHAND);
		return IMG_INV_HAND;
	}
	if ((hMem = GlobalAlloc(DLLHND, sizeof(BITMAPINFOHEADER))) == NULL) {
		SETERROR(IDS_NOMEM);
		return IMG_NOMEM;
	}
	lpbi = (LPBITMAPINFOHEADER) GlobalLock(hMem);

	if (!lpbi) {
		SETERROR(IDS_NOMEM);
		return IMG_NOMEM;
	}
	lpbi->biWidth = lpImage->hdr.bbox.right - lpImage->hdr.bbox.left;
	lpbi->biHeight = lpImage->hdr.bbox.bottom - lpImage->hdr.bbox.top;

	// Set the resolution
	lpbi->biBitCount = 24;

	if (lpImage->hdr.inch) {
		lpbi->biXPelsPerMeter = (DWORD) (lpImage->hdr.inch * 39.37);	// bbox.right - lpImage->hdr.bbox.left;

		lpbi->biYPelsPerMeter = (DWORD) (lpImage->hdr.inch * 39.37);	// bbox.bottom - lpImage->hdr.bbox.top;

	} else {
		lpbi->biXPelsPerMeter = (DWORD) (1440 * 39.37);	// bbox.right - lpImage->hdr.bbox.left;

		lpbi->biYPelsPerMeter = (DWORD) (1440 * 39.37);	// bbox.bottom - lpImage->hdr.bbox.top;

	}

	lpInfo->lpbi = (LPBITMAPINFO) lpbi;

	lpInfo->bFlags = IMG_PRNT_VECTOR | IMG_DISP_VECTOR;

	if (lpImage->bAPM) {
		lpInfo->lOrgWid = lpImage->hdr.bbox.right - lpImage->hdr.bbox.left + 1;
		lpInfo->lOrgHi = lpImage->hdr.bbox.bottom - lpImage->hdr.bbox.top + 1;
	} else {
		lpInfo->lOrgWid = 0;
		lpInfo->lOrgHi = 0;
	}

	GlobalUnlock(hImage);

	return IMG_OK;
}


int IMAPI   IMDLrender(HANDLE hImage, HDC hDC, LPRECT lpDest, LPRECT lpSrc)
{
	return IMG_OK;
}


int IMAPI   IMDLprint(HANDLE hImage, HDC hPrnDC, LPRECT lpDest, LPRECT lpSrc)
{
	return IMG_OK;
}


int IMAPI   IMDLerror_string(LPSTR lpszBuf, int nMaxLen)
{
	LoadString(hInst, GETERROR, lpszBuf, nMaxLen);
	return IMG_OK;
}

LPSTR IMAPI IMDLget_ver()
{
	return (LPSTR) (VERSION);
}


int IMAPI   IMDLset_ext(HANDLE hFile, LPSTR ext)
{
	// no extensions to differentiate between - just return OK
	return IMG_OK;
}


int IMAPI   IMDLload_wmf(HANDLE hFile, HMETAFILE FAR * lpHWMF, LPRECT lpBBOX, LPIMG_IO lpIO)
{
	LPIMAGESTRUCT lpImage;
	HPSTR       lpWmf;
	HPSTR       lpBuf;
	OFSTRUCT    of;
	LONG        lSize, lWmfSize;
	HMETAFILE   hWmf;
	LPVOID		lpHandle;

	if ((lpImage = (LPIMAGESTRUCT) GlobalLock(hFile)) == NULL) {
		SETERROR(IDS_INVHAND);
		return IMG_INV_HAND;
	}
	if (lpIO->open(lpImage->lpOpenInfo, OF_READ, &lpHandle) == IMG_OK) {
		if (lpImage->lLength)
			lSize = lpImage->lLength;
		else
			lSize = lpIO->seek(lpHandle, 0, 2);

		lWmfSize = lSize;
		lpWmf = lpBuf = GlobalAllocPtr(GHND, lSize);

		if (!lpWmf) {
			SETERROR(IDS_NOMEM);
			return IMG_NOMEM;
		}
		if (lpImage->bAPM)
			lpIO->seek(lpHandle, sizeof(APMFILEHEADER) + lpImage->lOffset, 0);

		if (lSize < 0x7fff) {
			lpIO->read(lpHandle, lpBuf, (UINT) lSize);
		} else {
			lSize -= sizeof(APMFILEHEADER);
			while (lSize) {
				lpIO->read(lpHandle, lpBuf, (UINT) min(lSize, 0x7fff));
				if (lSize > 0x7fff)
					lSize -= 0x7fff;
				else
					lSize = 0;
				lpBuf += 0x7fff;
			}
		}

#ifdef WIN32
		hWmf = SetMetaFileBitsEx(lWmfSize, lpWmf);
		GlobalFreePtr(lpWmf);
#else
		hWmf = (HANDLE) SELECTOROF(lpWmf);
#endif
		lpIO->close(lpHandle);
	}
	GlobalUnlock(hFile);
	*lpHWMF = hWmf;
	lpBBOX->left = lpImage->hdr.bbox.left;
	lpBBOX->top = lpImage->hdr.bbox.top;
	lpBBOX->bottom = lpImage->hdr.bbox.bottom;
	lpBBOX->right = lpImage->hdr.bbox.right;
	return IMG_OK;
}

int IMAPI   IMDLread_rows(hFile, buf, rows, rowSize, start, lpStat)
	HANDLE      hFile;
	LPSTR       buf;
	unsigned    rows, rowSize, start;
	LPSTATUS    lpStat;
{

	return IMG_OK;
}


int IMAPI   IMDLset_row(HANDLE hFile, unsigned row)
{

	return IMG_OK;
}



#ifndef NDEBUG
static void IM2WMFerr(int e)
{
	char        s1[80], s2[120];

	LoadString(hInst, e, s1, sizeof(s1));
	wsprintf(s2, "IM2WMF: Error: (%d) \"%s\".\n", e, s1);
	OutputDebugString(s2);
}
#endif
