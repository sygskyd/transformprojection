/////////////////////////////////////////////////////////////////////////////
//
//    filter.c
//
//    Convolution Filter Module
//
//    ImageMan Image Processing Library
//
//    Copyright (c) 1997 Data Techniques, Inc.
//    All Rights Reserved
//
/////////////////////////////////////////////////////////////////////////////
// $Header: /ImageMan 16-32/DLL - V 6.xx API/filter.cpp 14    11/06/98 10:25a Ericj $
// $History: filter.cpp $
// 
// *****************  Version 14  *****************
// User: Ericj        Date: 11/06/98   Time: 10:25a
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Revamped Sharpen Kernel to be more useful.
// 
// *****************  Version 13  *****************
// User: Johnd        Date: 8/19/98    Time: 1:38p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Chgd ImgFilterLow to take a FAR * to the kernel for 16 bit Builds
// 
// *****************  Version 12  *****************
// User: Ericw        Date: 8/10/98    Time: 5:16p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Fixed bug #125.  Filters with negative kernel values would cause the
// introduction of noise in the image due to a signed/unsigned problem.
// 
// *****************  Version 11  *****************
// User: Ericw        Date: 7/10/98    Time: 4:02p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Border processing problems.  This time I simply replicate the input
// pixels so that the kernel has 'real' data to process.
// 
// *****************  Version 10  *****************
// User: Johnd        Date: 7/06/98    Time: 6:45p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Fixed Bug #107 (GPF with Large filter kernel values)
// 
// *****************  Version 9  *****************
// User: Johnd        Date: 3/20/98    Time: 9:05a
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Updated  internal error handling. Added error string info to TLS data
// instead of the global buffers where it had been stored. Also added
// SetError() func to set the internal error state.
// 
// *****************  Version 8  *****************
// User: Ericw        Date: 3/16/98    Time: 5:46p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Part of the band data was getting filled in with already filtered image
// data.
// 
// *****************  Version 7  *****************
// User: Ericw        Date: 3/12/98    Time: 4:24p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Still some problem with setting up the input bands not accounting for
// proper orientation base on the target pixel causing the downward shift
// by half the height of the kernel.
// 
// *****************  Version 6  *****************
// User: Ericw        Date: 2/24/98    Time: 11:23a
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Last fix screwed up the input row data, which oddly only showed when
// using the emboss filter.
// 
// *****************  Version 5  *****************
// User: Ericw        Date: 2/20/98    Time: 7:05p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Images were shifting down and to the left 1 row of pixels on each
// filter.
// 
// *****************  Version 4  *****************
// User: Ericw        Date: 10/01/97   Time: 3:05p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Speed improvement using pin instead of clamping.
// 
// *****************  Version 3  *****************
// User: Ericw        Date: 7/28/97    Time: 2:43p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Declared img_status for 16bit build.
// 
// *****************  Version 2  *****************
// User: Ericw        Date: 7/16/97    Time: 3:08p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// 2 new filters EdgeDetect2 and LaplaceSharpen also fixed bugs with
// overrunning the image boundaries.
// 
// *****************  Version 1  *****************
// User: Ericw        Date: 7/10/97    Time: 2:44p
// Created in $/ImageMan 16-32/DLL-API Level 2
// Convolution kernel module!  First version
// 

#include "internal.h"
#include <windowsx.h>
#pragma hdrstop
#include <string.h>

#define PIN_OFFSET (256)  // offset to identity lookup
extern int pin[] ;

#ifndef WIN32
extern int  img_status;
#endif

extern HINSTANCE hInst;			  // who we is in this life... available to all!

//static int aisharp[] = { 0,  0, 0,  0, 1,  0, 0,  0, 0 } ;
static int aisharp[] = { -1, -2, -1, -2, 42, -2, -1, -2, -1 } ;
FILTERKERNEL SharpenKernel =
	{
	  30, 0,
		3, 3,
		aisharp
	} ;

static int ailaplace[] = { 1, 1, 1, 1, -8, 1, 1, 1, 1, } ;
FILTERKERNEL LaplaceKernel =  // sharpens
	{
	  0, 0,
		3, 3,
		ailaplace
	} ;

/*
static int aiblur[] = { 0, 0, 0, 0, 0, 0, 0,
												0, 0, 0, 0, 0, 0, 0,
												0, 0, 0, 0, 0, 0, 0,
												0, 0, 0, 1, 0, 0, 0,
												0, 0, 0, 0, 0, 0, 0,
												0, 0, 0, 0, 0, 0, 0,
												0, 0, 0, 0, 0, 0, 0  } ;
*/
static int aiblur[] = { 7, 12, 7, 12, 24, 12, 7, 12, 7 } ;
FILTERKERNEL BlurKernel =
	{
//0, 0,
		100, 0,
//7, 7,
		3, 3,
		aiblur
	} ;

static int aiemboss[] = { -1, 0, 0, 0, 0, 0, 0, 0, 1 } ;
FILTERKERNEL EmbossKernel =
	{
		0, 128,
		3, 3,
		aiemboss
	} ;

static int aiedge1[] = { 0, -1, 0, -1, 4, -1, 0, -1, 0 } ;
FILTERKERNEL EdgeKernel1 =
	{
	  0, 0,
		3, 3,
		aiedge1
	} ;

static int aiedge2[] ={ -1, -1, -1, -1, 8, -1, -1, -1, -1 } ;
FILTERKERNEL EdgeKernel2 =
	{
	  0, 0,
		3, 3,
aiedge2
	} ;

static int aivedge[] ={ 0, 0, 0, -1, 2, -1, 0, 0, 0 } ;
FILTERKERNEL VEdgeKernel =
	{
	  0, 0,
		3, 3,
		aivedge
	} ;

static int aihedge[] = { 0, -1, 0, 0, 2, 0, 0, -1, 0 } ;
FILTERKERNEL HEdgeKernel =
	{
	  0, 0,
		3, 3,
		aihedge
	} ;

int IMAPI ImgFilter( HANDLE hImage, enumFilter iFilter )
	{
		LPKERNEL lpkernel = ( iFilter == IMG_SHARPEN      ) ? & SharpenKernel :
		                    ( iFilter == IMG_BLUR         ) ? & BlurKernel    :
												( iFilter == IMG_EMBOSS       ) ? & EmbossKernel  : 
												( iFilter == IMG_EDGEDETECT1  ) ? & EdgeKernel1   :
												( iFilter == IMG_EDGEDETECT2  ) ? & EdgeKernel2   :
												( iFilter == IMG_VERTEDGE     ) ? & VEdgeKernel   :
												( iFilter == IMG_HORIZEDGE    ) ? & HEdgeKernel   :
												( iFilter == IMG_LAPLACESHARP ) ? & LaplaceKernel : 0 ;
		if ( lpkernel )
			{
				int iRes = ImgFilterLow ( hImage, lpkernel, RGB_FULL ) ;
				return ( iRes ) ;
			}
		else
			return ( IMG_INVFILTER ) ;
	}

int IMAPI ImgFilterLow ( HANDLE hImage, LPKERNEL lpKernel, int iChannel )
{
	LPINTERNALINFO lpII ;
	PDIB   pDIB ;
	HPUSTR lpSrc, lpDst, pBand ;
	HPUSTR lpBits       ;
	int		 nWidth,  nHeight  ;
	int    iKWidth, iKHeight ;
	int    iKScale, iKOffset ;
	int    iVSpan, iHSpan, iHSpanX3 ;
	int    ixx, iyy, idelta, iRowPad ;
	long   iVPos,  iHPos   ;
	long	 lRowSize, lByteCount, lBandWidth ;
	HPUSTR FAR * alpBand ;
	int   FAR  * pFilterIn = lpKernel->lpFilter ;
  int    FAR * pFilter ;
	long     lRes ;
	unsigned uSrc ;

	if ( ( !(lpKernel->iWidth  & 0x01) ) ||  // no even numbered dimensions
		   ( !(lpKernel->iHeight & 0x01) ) ) {
		SetError( NULL, IMG_PROC_ERR, 0, NULL, hInst );
		return ( IMG_ERR ) ;
	}
	if ( ( lpKernel->iWidth  < 3 ) ||        // minimum size is 3 x 3
		   ( lpKernel->iHeight < 3 ) ) {
		SetError( NULL, IMG_PROC_ERR, 0, NULL, hInst );
		return ( IMG_ERR ) ;
	}
	if (!(lpII = (LPINTERNALINFO)GlobalLock( hImage ))) {
		SetError( NULL, IMG_INV_HAND, 0, NULL, hInst );
		return (IMG_ERR);
	}
	if (lpII->bFlags & IMG_DISP_VECTOR) {
		SetError( NULL, IMG_BAD_TYPE, 0, NULL, hInst );
		return (IMG_ERR);
	}
	if ( lpII->lpbi->bmiHeader.biBitCount != 24 ) { // only do 24 bit images
	  	// for real, we'll eventually call ImgIncreaseColors
		SetError( NULL, IMG_PROC_ERR, 0, NULL, hInst );
	    return ( IMG_ERR ) ;
	}

	if (!lpII->pDIB) {
		if (InternalLoad( lpII, NULL, &lpII->pDIB, &lpII->hWMF ) != IMG_OK) {
			return (IMG_ERR);
		}
	}
	pDIB = lpII->pDIB ;
	iRowPad  = pDIB->GetRowPadding() ;
	nWidth   = pDIB->GetWidth  () ;
	nHeight  = pDIB->GetHeight () ;
	iKWidth  = lpKernel->iWidth  ;
	iKHeight = lpKernel->iHeight ;
	iKScale  = lpKernel->iScale  ;
	iKOffset = lpKernel->iOffset ;
	iVSpan   = (lpKernel->iHeight & 0xFE) >> 1 ;
	iHSpan   = (lpKernel->iWidth & 0xFE) >> 1 ;
	iHSpanX3 = iHSpan * 3 ;

	lpBits = (HPUSTR)pDIB->GetDataPtr() ;
	if ( ! lpBits ) {
		SETSTATUS(IMG_NOMEM);
		return (IMG_ERR);
	}
	lRowSize = pDIB->GetRowSize () ;

	alpBand = (HPUSTR FAR *)GlobalAllocPtr ( GHND, sizeof(HPUSTR) * iKHeight ) ;
	if ( ! alpBand ) {
		SETSTATUS(IMG_NOMEM);
		return (IMG_ERR);
	}
	lByteCount = nWidth * 3 ;

	lBandWidth = lByteCount + ( ( iHSpan * 2 ) * 3 ) ;
	for ( ixx = 0 ; ixx < iKHeight ; ixx ++ )    // allocate the input save buffers
		{
			alpBand[ixx] = (HPUSTR)GlobalAllocPtr( GHND, lBandWidth ) ;
			if ( ! ( alpBand[ixx] ) ) {
				SETSTATUS(IMG_NOMEM);
				return (IMG_ERR);
			}
		}
	lpDst = lpBits ;
	for ( iVPos = 0 ; iVPos < nHeight ; iVPos++ )
		{
			HPUSTR lpDup, lpRep ;
		  // the following sets up a ptr to the start of each src row in the band
		  for ( iyy = 0 ; iyy < iKHeight ; iyy++ ) {
				idelta = iVPos + ( iyy  - iVSpan ) ;

				if ( ( idelta <= (nHeight-1) ) &&     // don't overrun the end of the image
					   ( idelta >= 0           ) ) {    // don't underrun the top
					if ( ( iyy < iVSpan ) && ( idelta >= 0 ) ) {
						hmemcpy ( alpBand[iyy], alpBand[iyy+1], lBandWidth ) ;
					} else {
						lpSrc  = lpBits + (idelta * lRowSize) ;
						hmemcpy ( alpBand[iyy]+iHSpanX3, lpSrc, lByteCount ) ;
					}
				} else {
//					_fmemset ( alpBand[iyy], '\0', lBandWidth ) ;
//					unsigned char pFill = 0xff ;
//					_fmemset ( alpBand[iyy], pFill, lBandWidth ) ;
						hmemcpy ( alpBand[iyy]+iHSpanX3, lpBits, lByteCount ) ;
				}
				// Now we replicate the edges for the input pixels.
				// First do the left edge.
				lpDup = alpBand[iyy] ;
				lpRep = lpDup + iHSpanX3 ;
				for ( ixx = 0 ; ixx < iHSpan ; ixx ++ ) {
					hmemcpy ( lpDup, lpRep, 3 ) ;  // 3 for an RGB combo
					lpDup += 3 ;
				}
				lpDup = (alpBand[iyy] + iHSpanX3) + ( nWidth * 3 ) ;
				lpRep = lpDup - 3 ;
				for ( ixx = 0 ; ixx < iHSpan ; ixx ++ ) {
					hmemcpy ( lpDup, lpRep, 3 ) ;  // 3 for an RGB combo
					lpDup += 3 ;
				}
			}
		  for ( iHPos = 0 ; iHPos < lByteCount ; iHPos++ )
				{
				  lRes = 0 ;
					pFilter = pFilterIn ;
					// now we are ready to go through the filter itself
					for ( iyy = 0 ; iyy < iKHeight ; iyy ++ )
						{
						  pBand = alpBand[iyy] + iHPos ;
							for ( ixx = 0 ; ixx < iKWidth ; ixx++ )
								{
								  uSrc = * pBand ;
									pBand += 3 ;

									lRes += (long)uSrc * (long)*pFilter ++;
								}
						}
					if ( iKScale )
						lRes /= (long)iKScale ;

					lRes += iKOffset ;
					if( lRes > 255 )
						lRes = 255;
					else
					if (lRes < 0 )
						lRes = 0;

					*lpDst++ = (char)lRes ;
				}
			lpDst += iRowPad ;
		}
	for ( ixx = 0 ; ixx < iKHeight ; ixx++ )  // can the save buffers
		GlobalFreePtr ( alpBand[ixx] ) ;
	GlobalFreePtr ( alpBand ) ;

	return ( IMG_OK ) ;
}
