/////////////////////////////////////////////////////////////////////////////
//
//    imgman.c
//
//    ImageMan Main Module
//
//    ImageMan Image Processing Library
//
//    Copyright (c) 1991-5 Data Techniques, Inc.
//    All Rights Reserved
//	 
/////////////////////////////////////////////////////////////////////////////
//$Header: /ImageMan 16-32/DLL - V 6.xx API/imgman.cpp 39    7/11/00 2:19p Ericj $
//$History: imgman.cpp $
// 
// *****************  Version 39  *****************
// User: Ericj        Date: 7/11/00    Time: 2:19p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Removed PPP licensing stuff in the interest of simplicity.
// 
// *****************  Version 38  *****************
// User: Ericj        Date: 3/01/00    Time: 8:55a
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Needed to initialize the memory for 4 bit images as well as monochrome
// images for Windows 98.
// 
// *****************  Version 37  *****************
// User: Ericj        Date: 9/28/99    Time: 10:07a
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Implemented 16 bit DialogBoxIndirect.
// 
// *****************  Version 36  *****************
// User: Ericj        Date: 9/22/99    Time: 4:06p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Needed to initialize a monochrome images memory to 0.  We were only
// setting the 1 bits and on a 98 machine this led to trash in the data.
// 
// *****************  Version 35  *****************
// User: Ericj        Date: 8/21/99    Time: 4:39a
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// C:\imageman\imgman.cpp
// 
// *****************  Version 34  *****************
// User: Ericj        Date: 8/12/99    Time: 3:25a
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Changed PPP interface to compile and Link in both 16 and 32 bits.
// With PPP Phase 1 (external DLL).
// 
// *****************  Version 33  *****************
// User: Ericj        Date: 8/12/99    Time: 2:03a
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Made compile with current mak file.
// 
// *****************  Version 32  *****************
// User: Ericj        Date: 7/14/99    Time: 5:21p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Added PPP demo stuff.
// 
// *****************  Version 31  *****************
// User: Johnd        Date: 7/13/99    Time: 12:24p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Fixed Bug 206 (Invalid Image Files are left open after calling
// ImgOpen*)
// 
// *****************  Version 30  *****************
// User: Johnd        Date: 6/02/99    Time: 11:00a
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Fixed Bug 199 (ImgUnLoad writing data to invalid img->lpbi)
// 
// *****************  Version 29  *****************
// User: Johnd        Date: 5/28/99    Time: 9:56a
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Chgd ImgUnload to reset lpbi width/height  to OrgWidth & OrgHeight.
// Previously it only did that for vector images. This change allows you
// to use ImgLoad to load tiles from an image and then call unload  & Load
// to load another.
// 
// *****************  Version 28  *****************
// User: Ericw        Date: 5/20/99    Time: 12:12p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Part of bug #164 fix to protect ourselves from zero height/width images
// and cleaned up more on error-return cleanup processing.
// 
// *****************  Version 27  *****************
// User: Ericw        Date: 5/20/99    Time: 10:18a
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Expires now on day 335 of 1999..
// 
// *****************  Version 26  *****************
// User: Ericw        Date: 1/06/99    Time: 12:31p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Changed expiration date and found bug in IsExpired checking.
// 
// *****************  Version 25  *****************
// User: Johnd        Date: 11/13/98   Time: 12:51p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Fixed Area Code in Demo Vers
// 
// *****************  Version 24  *****************
// User: Johnd        Date: 11/02/98   Time: 4:43p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Misc 6.02 Updates
// 
// *****************  Version 23  *****************
// User: Johnd        Date: 10/05/98   Time: 11:55a
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Fixed Bug #132 (BMP w/63 colors)
// 
// *****************  Version 22  *****************
// User: Johnd        Date: 8/19/98    Time: 10:53a
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// removed 1 last OutputDebugString & fixed MAXEXT off by one bug
// 
// *****************  Version 21  *****************
// User: Johnd        Date: 7/15/98    Time: 8:49a
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Chgd ImgFromPDIB to use IMAPI
// 
// *****************  Version 20  *****************
// User: Johnd        Date: 7/07/98    Time: 2:50p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Added DEMO dlg display to ImgOpenMem function
// 
// *****************  Version 19  *****************
// User: Johnd        Date: 7/06/98    Time: 5:16p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Fixed Bug #108 (GPF on ImgInit in 16 bit DLL)
// 
// *****************  Version 18  *****************
// User: Johnd        Date: 7/06/98    Time: 12:13p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Fixed bug #104 (LPBI Mem leak in ImgClose)
// 
// *****************  Version 17  *****************
// User: Johnd        Date: 4/17/98    Time: 1:28p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Upped Expire Date for Demo Build
// 
// *****************  Version 16  *****************
// User: Johnd        Date: 4/16/98    Time: 11:47a
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Misc Updates
// 
// *****************  Version 15  *****************
// User: Johnd        Date: 4/16/98    Time: 11:31a
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Fixed 16 bit GetThreadInfo() issues
// 
// *****************  Version 14  *****************
// User: Johnd        Date: 4/16/98    Time: 10:35a
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Fixed 16 bit Bug in ImgInvert
// 
// *****************  Version 13  *****************
// User: Johnd        Date: 4/13/98    Time: 9:57a
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Fixed ImgGetDIBPtr() function to return * to DIB instead of * to lpbi
// 
// *****************  Version 12  *****************
// User: Ericw        Date: 4/02/98    Time: 4:41p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// SUPPORT FOR PLUG-IN HOSTING !!!!!
// 
// *****************  Version 11  *****************
// User: Johnd        Date: 3/20/98    Time: 1:50p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Fixed a bug in ImgSetpage() which caused a GPF potentially
// 
// *****************  Version 10  *****************
// User: Johnd        Date: 3/20/98    Time: 9:05a
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Updated  internal error handling. Added error string info to TLS data
// instead of the global buffers where it had been stored. Also added
// SetError() func to set the internal error state.
// 
// *****************  Version 9  *****************
// User: Johnd        Date: 3/17/98    Time: 10:57a
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Fixed a bug which caused WMFs placed on the clipboard to grow by one
// pixel on both axes. Also fixed a problem in ImgToClipboard() which
// caused a GPF when pasting a WMF image. 
// 
// *****************  Version 8  *****************
// User: Johnd        Date: 3/05/98    Time: 12:09p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// fixed minor warning
// 
// *****************  Version 7  *****************
// User: Johnd        Date: 2/24/98    Time: 2:43p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Added includes for 16 bit builds that were misplaced in previous
// checkins.
// 
// *****************  Version 6  *****************
// User: Ericw        Date: 2/03/98    Time: 12:13p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Moved all the common dialog stuff out of this module.
// 
// *****************  Version 5  *****************
// User: Ericw        Date: 1/26/98    Time: 11:45a
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Fixed double click on unselected item crash on NT.
// Change cursor to hourglass when opening the preview image.
// Initial preview had palette realization problem.  Preview image had
// aspect
// ratio problems.
// 
// *****************  Version 4  *****************
// User: Ericw        Date: 1/19/98    Time: 6:02p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Common dialog wasn't previewing vector images.
// 
// *****************  Version 3  *****************
// User: Ericw        Date: 1/19/98    Time: 11:16a
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Common dialog support - new function - ImgOpenEx.
// 
// *****************  Version 2  *****************
// User: Johnd        Date: 8/05/97    Time: 1:46p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Fixed B-483 (16 Bit ImgInit Memory Fault)
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 7/10/97    Time: 2:03p
// Created in $/imageman 16-32/dll-api level 2
// Initial Lvl 2 Checkin
// 
// *****************  Version 26  *****************
// User: Johnd        Date: 12/18/96   Time: 3:42p
// Updated in $/ImageMan 16-32/DLL
// Changed Expire to Mid 97
// 
// *****************  Version 25  *****************
// User: Johnd        Date: 9/25/96    Time: 1:39p
// Updated in $/ImageMan 16-32/DLL
// Added code for DEMO Build
// 
// *****************  Version 24  *****************
// User: Johnd        Date: 9/25/96    Time: 1:26p
// Updated in $/ImageMan 16-32/DLL
// Added Support for Demo Build
// 
// *****************  Version 23  *****************
// User: Johnd        Date: 9/19/96    Time: 1:59p
// Updated in $/ImageMan 16-32/DLL
// Enlarged COPYBUFSIZE to enhance ImgCopy performance
// 
// *****************  Version 22  *****************
// User: Johnd        Date: 7/30/96    Time: 2:11p
// Updated in $/ImageManVB/Image Control
// Changed buffer size in ImgCopy for COPY_DEL case to add 10 bytes of
// padding
// 
// *****************  Version 21  *****************
// User: Johnd        Date: 7/17/96    Time: 11:41a
// Updated in $/ImageMan 16-32/DLL
// Fixed Typo in last checkin
// 
// *****************  Version 21  *****************
// User: Johnd        Date: 7/17/96    Time: 11:39a
// Updated in $/ImageManVB/Image Control
// Chgd ImgCopy to fire status events when doing COPY_DEL scaling
// 
// *****************  Version 20  *****************
// User: Johnd        Date: 7/17/96    Time: 11:20a
// Updated in $/ImageMan 16-32/DLL
// Chgd ImgCopy so it fires status events when using COPY_DEL method
// 
// *****************  Version 19  *****************
// User: Timk         Date: 6/28/96    Time: 3:22p
// Updated in $/ImageMan 16-32/DLL
// Fixed bug when an app calls ImgInit/ImgShutdown multiple times in one
// session. We weren't properly marking the DLLs as released in the
// EXTList structure.
// 
// *****************  Version 18  *****************
// User: Johnd        Date: 5/09/96    Time: 10:07a
// Updated in $/ImageMan 16-32/DLL
// Fixed Bug #360 (16 MB Limit in 32 bit Code)
// 
// *****************  Version 17  *****************
// User: Johnd        Date: 4/11/96    Time: 3:24p
// Updated in $/ImageMan 16-32/DLL
// Moved OutputDebugString() defines into internal.h
// 
// *****************  Version 16  *****************
// User: Johnd        Date: 3/25/96    Time: 4:02p
// Updated in $/ImageMan 16-32/DLL
// Fixed bug which caused us to always get portions starting at row 0 even
// if we requested another value in ImgCopy.
// 
// *****************  Version 15  *****************
// User: Timk         Date: 2/01/96    Time: 11:02a
// Updated in $/ImageMan 16-32/DLL
// Added (or revived, rather) the Debug panel!
// 
// *****************  Version 14  *****************
// User: Johnd        Date: 1/30/96    Time: 5:22p
// Updated in $/ImageMan 16-32/DLL
// We now free the img->lpbi pointer when changing pages.
// 
// *****************  Version 13  *****************
// User: Johnd        Date: 1/29/96    Time: 10:34a
// Updated in $/ImageMan 16-32/DLL
// Fixed Bug #324 (Overflow in math in InternalLoad)
// 
// *****************  Version 12  *****************
// User: Timk         Date: 9/29/95    Time: 2:52p
// Updated in $/ImageMan 16-32/DLL
// Fixed scaling/loading of large images using ImgCopy. Also, added code
// to seek to the proper row when using ImgCopy and loading the image.
// 
// *****************  Version 11  *****************
// User: Johnd        Date: 9/22/95    Time: 9:27a
// Updated in $/ImageMan 16-32/DLL
// Added xtra bytes @ end of dst buffer in ImgCopy.
// 
// *****************  Version 10  *****************
// User: Johnd        Date: 9/21/95    Time: 3:26p
// Updated in $/ImageMan 16-32/DLL
// Chgs to ignore decode errors when loading.
// 
// *****************  Version 9  *****************
// User: Timk         Date: 9/12/95    Time: 3:32p
// Updated in $/ImageMan 16-32/DLL
// Fixed potential crash bug w/FillErrBuf function not initializing
// filename handle to NULL.
// 
// *****************  Version 8  *****************
// User: Timk         Date: 9/11/95    Time: 11:06p
// Updated in $/ImageMan 16-32/DLL
// Fixed problem w/copying MetaFiles under Win32.
// 
// *****************  Version 7  *****************
// User: Timk         Date: 7/25/95    Time: 11:05p
// Updated in $/ImageMan 16-32/DLL
// Added writing using IO function blocks.
// 
// *****************  Version 6  *****************
// User: Timk         Date: 6/30/95    Time: 12:08p
// Updated in $/ImageMan 16-32/DLL
// Part 1 of massive changes for read/write using function blocks.
// 
// *****************  Version 5  *****************
// User: Timk         Date: 5/22/95    Time: 1:10p
// Updated in $/ImageMan 16-32/DLL
// Fixed bug 166, problems with auto-detection and the TGA DIL. We now pay
// attention to the return value from the IMDLverify_img function call (1
// == absolutely verified, 2 == maybe).
// 
// *****************  Version 4  *****************
// User: Johnd        Date: 5/17/95    Time: 11:03a
// Updated in $/ImageManVB/Image Control
// Fixed bug in ImgCopy (B-196).
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 1/18/95    Time: 3:59p
// Created in $/ImageMan 16-32/DLL
// ImageMan 4.0 Beta 1
//
//

#include "internal.h"
#pragma hdrstop
#include <assert.h>
#include <string.h>
#include <stdarg.h>
#include <time.h>


#include "dibfuncs.h"
#include "imgio.h"
#include "nag.h"

BOOL DisplayEvalDlg(void);

#define MAXEXTS       30
#define MAXTASKS      30
#define COPYBUFSIZE   0x0016E360L
#define VERIFYBUFSIZE 100
#define MAXMEMSIZE    (16000L*32) // Max size of Global Alloc in Win

#ifdef DEMO
#pragma message("\r\n\r\n===========>Compiling A Demo Build\r\n\r\n")
#else
#pragma message("\r\n\r\n\r\n===========>Compiling A Release Build\r\n\r\n")
#endif

//
// the following #defines should correspond to the dilspec defined
// in the makefile(s)
//
#ifdef WIN32
#define DILFSPEC "im31*.dil" 
#else
#define DILFSPEC "im11*.dil" 
#include <dos.h>
#include "windos.h"
#endif

typedef struct tagEXTinfo {
	char      ext[4]; 						// ext. supported (w/ terminating null)
	HINSTANCE hLib;   						// handle to loaded DLL
	DILFUNCS  funcs;  						// exported function pointers within 'hLib'
	char      DLL[MAX_PATH]; 				// path & name of DLL for this ext
	char      ver[40];  					// Version string for this ext
} EXTinfo, *PEXTINFO;

#ifndef WIN32
static int NEAR PASCAL SetDrive(int);
static int NEAR PASCAL GetDrive(void);
#endif

// Thread-based (Win16) variables:
#ifdef WIN32
static DWORD TlsIndex;          		// Thread local storage index
#else
int img_status; 						// imgman2 status... available to all!
static THREADINFO tasks[MAXTASKS];    	// globals for the individual tasks
static int   nRefCount = -1;  			// connected tasks count
static HINSTANCE LoadedDLL[ MAXEXTS ];
#endif
#ifdef WINGIS
static	BOOL	ECWAllowed = false;
#endif

HINSTANCE hInst; 						// who we is in this life... available to all!

static LPSTR   lpExtString; 			// list of types and extensions gathered from DILs
static EXTinfo EXTList[MAXEXTS]; 		// list of loaded DILs w/ extra information...
static	HTASK inittask;

// standard IO function blocks
IMG_IO file_ioblk = {fileio_open, fileio_close, fileio_read, fileio_write, fileio_seek, fileio_getchar, fileio_error};
IMG_IO mem_ioblk = {memio_open, memio_close, memio_read, memio_write, memio_seek, memio_getchar, memio_error};

#if DEBUG
#include <commdlg.h>
#include "imgmanrc.h"

static HWND hDebugDlg;

int WINAPI GetLogFile(HANDLE,LPSTR);
BOOL __cdecl FAR DebugStringOut(UINT,LPSTR,...);
BOOL WINAPI DebugPanelProc(HWND,UINT,WPARAM,LPARAM);
void DebugInit(void);
int bLog, hLogFile, nStartLine, nEndLine, nCurLine;
char LogFile[255];
#endif

static HANDLE NEAR PASCAL OpenFileInternal(LPVOID, int nSize, long, long, LPCSTR, LPIMG_IO, LPCSTR);
static HBITMAP NEAR PASCAL load_image(INTERNALINFO FAR *, HDC, int, int, LPRECT);
static int NEAR PASCAL addExtension(LPSTR,HINSTANCE,LPSTR, LPSTR);
static void NEAR PASCAL get_ext(LPSTR, LPCSTR);
static int NEAR PASCAL find_ext(LPSTR, LPVOID, LPSTR, LPIMG_IO);
LPINTERNALINFO NEAR PASCAL GetImgPtr(HANDLE);
static HGLOBAL NEAR PASCAL CopyMemBlock(HGLOBAL);


static int NEAR PASCAL error_stringDLL(int,LPCSTR,HANDLE,LPSTR,int);
static int NEAR PASCAL printDLL(int,HANDLE,HDC,LPRECT,LPRECT,LPCSTR);
static int NEAR PASCAL renderDLL(int,HANDLE,HDC,LPRECT,LPRECT,LPCSTR);
static int NEAR PASCAL buildDIBheadDLL(int,HANDLE,INTERNALINFO FAR *,LPCSTR);
static int NEAR PASCAL set_rowDLL(int,HANDLE,UINT,LPCSTR);
static int NEAR PASCAL read_rowsDLL(int,HANDLE,LPSTR,UINT,UINT,UINT,LPSTATUS,LPCSTR);
static int NEAR PASCAL close_keepDLL(int,HANDLE,LPCSTR);
static int NEAR PASCAL openDLL(int,HANDLE,LPVOID,LPCSTR);
static int NEAR PASCAL close_imageDLL(int,HANDLE,LPCSTR);
static int NEAR PASCAL init_imageDLL(int,LPCSTR,LPHANDLE,LPVOID,long,long,LPIMG_IO,LPCSTR);
static int NEAR PASCAL load_wmfDLL(int,HANDLE,HMETAFILE FAR *,LPRECT, LPIMG_IO,LPCSTR);
static int NEAR PASCAL set_pageDLL(int,HANDLE,int,LPCSTR);
static BOOL NEAR PASCAL verify_imgDLL(int,LPSTR,LPVOID,LPIMG_IO);
static int NEAR PASCAL scale_imageDLL(int nExt, HANDLE hImage, UINT nWidth, UINT nHeight, LPRECT lpSrc); 


BOOL IsLicensed();

#ifdef WIN32
static int processDIL(LPCSTR path,LPSTR lpExtList)
{
	HINSTANCE hdill;
	HANDLE fhandle;
	WIN32_FIND_DATA finfo;
	char buf[MAX_PATH],*p1,*p2,*p3;
	DLLget_ver lpfver;
	int count,cukecount = 0;
#ifdef WINGIS
	char ECWExt[10];
	BOOL	ECW;
#endif

	if (!*path) return (0);

#ifdef WINGIS
	// prepare ECW check string
	strcpy(ECWExt, ".");
	strcat(ECWExt, "ec");
	strcat(ECWExt, "w");
#endif

	// create full search path
	strcpy(buf,path);
	if (buf[strlen(buf)-1] != '\\') strcat(buf,"\\");
	strcat(buf,DILFSPEC);

	if ((fhandle = FindFirstFile(buf,&finfo)) == INVALID_HANDLE_VALUE) return (0);
	do {
		// create the full path name of located DIL
		strcpy(buf,path);
		if (buf[strlen(buf)-1] != '\\') strcat(buf,"\\");
		strcat(buf,finfo.cFileName);
#ifndef NDEBUG
		{
			char buf2[MAX_PATH];
			wsprintf(buf2,"IM32: DIL (%s)\n",buf);
			OutputDebugString(buf2);
		}
#endif
		// Only attempt to load DIL if it's new
		if (!GetModuleHandle(buf) && (hdill = LoadLibrary(buf))) {
			count = 0;
			if (LoadString(hdill,1000,buf,sizeof(buf))) {
				if (lpfver = (DLLget_ver)GetProcAddress(hdill,GET_VER)) {
#ifdef WINGIS
					// check ECW allowance
					ECW = strstr(buf, ECWExt) != NULL;
					if ((!ECW) || ((ECWAllowed) && ECW))
#endif
						// Add extensions stored in buffer to our internal lists
						for (p1 = buf; p2 = strchr(p1,'~'); p1 = p3+1) {
							p3 = strchr(p2+1,'~'); // find end of extension list
							assert(p3);
							*p3 = '\0';
							// p2+3 to skip '~*.' part of string
							if (addExtension(p2+3, hdill, finfo.cFileName,(*lpfver)()) == IMG_OK) 
								count++;
							*p3 = '~';
						}
					}
			}
			if (count) {
				cukecount++;           // one more DIL loaded
				strcat(lpExtList,buf); // add type and extensions when successful
			}
			else FreeLibrary(hdill); // free only when no extensions in DIL added
		}
	} while (FindNextFile(fhandle,&finfo));
	FindClose(fhandle);
	return (cukecount);
}

static BOOL InitRect( LPRECT lpRect, int nWidth, int nHeight)
{
	return SetRect( lpRect, 0, 0, nWidth - 1, nHeight - 1);
}


///////////////////////////////////////////////////////////////////////////
//
// findDLL
//
// High-level directory searcher. Creates/parses the available search
// paths, and calls the given function (dodir) with that path. It's then
// up to that 'dodir' routine to search that particular directory.
// 'findDLL' also allocates the extension-list memory; however, it's also
// up to each 'dodir' function to fill this list with appropriate
// information.
// In the interest of supporting multiple applications, the search path
// order is:
//     1. calling process's home directory (most likely to have DILs)
//     2. ImageMan library's directory (if different from #1)
//     3. current directory (if different from #1 and #2)
//     4. Window's system directory
//     5. Window's directory
//     6. directories listed in the PATH environment variable
// 
// Entry:
//   dodir    - function which will process each directory
//   lpExtPtr - location of pointer which will hold the extension list
// Exit:
//   TRUE if everything successful, and at least one DIL was located,
//   FALSE if anything fails or no DILs found.
// Side:
//   This routine (or anyone below it) must not try to access the 
//   THREADINFO block, as it will not yet be initialized.
//
///////////////////////////////////////////////////////////////////////////

BOOL findDLL(LPFNDODLLDIR dodir,LPSTR FAR *lpExtPtr)
{
	LPSTR p,lpext;
	int size;
	char
#ifndef WINGIS // we need it only for full search algorithm
		srch1[MAX_PATH],
#endif
		srch2[MAX_PATH]
#ifndef WINGIS // we need it only for full search algorithm
		, srch3[MAX_PATH]
#endif
		;

	// make sure we haven't already setup this process
	assert(!*lpExtPtr);

	// allocate lots of space... we'll shrink it back later
	lpext = (LPSTR) GlobalAllocPtr(GHND,1024);
	assert(lpext);
	*lpext = '\0';

#ifndef WINGIS // Don't look in any directory except of imgman32.dll's
	// always search calling process' path
	GetModuleFileName(NULL,srch1,sizeof(srch1));
	if (p = strrchr(srch1,'\\')) *p = '\0'; // remove filename and trailing backslash
	  (*dodir)(srch1,lpext);
#endif
	// search ImageMan DLL's path only if different than 'srch1'
	GetModuleFileName(hInst,srch2,sizeof(srch2));
	if (p = strrchr(srch2,'\\')) *p = '\0'; // remove filename and trailing backslash
#ifndef WINGIS // Don't look in any directory except of where imgman32.dll's
	if (strcmp(srch1,srch2)) 
#endif
		(*dodir)(srch2,lpext);

#ifndef WINGIS // Don't look in any directory except of where imgman32.dll's
	// search current directory only if it's unique
	GetCurrentDirectory(sizeof(srch3),srch3);
	if (strcmp(srch1,srch3) && strcmp(srch2,srch3)) 
	  (*dodir)(srch3,lpext);

	// search Windows' system directory
	GetSystemDirectory(srch1,sizeof(srch1));
	(*dodir)(srch1,lpext);

	// search the Windows home directory
	GetWindowsDirectory(srch1,sizeof(srch1));
	(*dodir)(srch1,lpext);

	// now search directories specified in the PATH environment variable
	if (GetEnvironmentVariable("PATH",srch1,sizeof(srch1)) < sizeof(srch1)) {
		// < sizeof(srch1) is to ensure there's room for any appended backslash
		while (p = strrchr(srch1,';')) {
			(*dodir)(p+1,lpext); // process last path in list
			*p = '\0';     // remove last path
		}
		(*dodir)(srch1,lpext); // process remaining directory
	}
#endif

	if ((size = strlen(lpext)) > 0) {
		*lpExtPtr = (LPSTR) GlobalAllocPtr(GHND,size+2);
		strcpy(*lpExtPtr,lpext);
	} 
	GlobalFreePtr(lpext);
	return (size > 0);
}

#ifdef WINGIS
//++Sygsky: add WinGIS license check
void SimpleDecode(char * Encoded, char * Decoded)
{
	int i, j;
	// now ask atom table for license from WinGIS and ImageMan
	for( i = 0, Decoded[j = strlen(Encoded)] = (char)0; i <= j;)	{
			Decoded[i] = Encoded[--j];
			Decoded[j] = Encoded[i++];
		}
}
//////////////////////////////////////////////////////////////////////
BOOL CheckWinGISLicense(void)
{
	ATOM ECWAtom = 0;
	static	const char EncodedJHC[] = "CHJ&morf!olleH";
	static	char	KeyStr[32]; // "ECW allowed' should fit to this buffer

	SimpleDecode((char*)EncodedJHC, KeyStr);
	return 
		(GlobalFindAtom(KeyStr) !=0);
}
#endif

///////////////////////////////////////////////////////////////////////////
//
// DllMain
//
// This is the Win32 'DllEntryPoint'. It is similar in function to LibMain
// and the WEP in 16-bit land, but is much more thorough. LibMain is
// called whenever a process or thread attaches or detaches to the
// ImageMan library (static linkage = during process initialization,
// dynamic linkage = before LoadLibrary returns).
//
// Entry:
//   hinstDLL  - handle to the ImageMan DLL
//   fdwReason - notification code (attach/detach)
//     DLL_PROCESS_ATTACH
//     DLL_PROCESS_DETACH
//     DLL_THREAD_ATTACH
//     DLL_THREAD_DETACH
//   lpvReserved - NULL during dynamic process attach/detach, non-NULL
//                 during static attach/detach.
// Exit:
//   DLL_PROCESS_ATTACH - TRUE for success, FALSE for failure
//   all others         - return code ignored
///////////////////////////////////////////////////////////////////////////

BOOL APIENTRY DllMain(HINSTANCE hinstDLL,DWORD fdwReason,LPVOID lpvReserved)
{
	LPTHREADINFO lpInfo;
	UINT i;
	LPSTR p;
#ifdef WINGIS
	static	const char	EncodedECW[] = WINGISLICENSEATOM;
#endif

	
	switch (fdwReason) {
	// The DLL is attaching to a process, due to process
	// initialization or a call to LoadLibrary.
	case DLL_PROCESS_ATTACH:
		// store away this instance's handle
		hInst = hinstDLL;

		// Allocate a "Thread Local Storage" (TLS) index right away.
		if ((TlsIndex = TlsAlloc()) == 0xFFFFFFFF) return (FALSE);

#if DEBUG
		DebugInit();
#endif

		// Search for DILs, initializing the "process globals" with any supported
		// import formats.
		// Ignore any load error here when processing a static attach such that 
		// the calling App has a chance to call ImgInit() and get the error.
#ifdef WINGIS
		//++Sygsky: add WinGIS licensing code
		ECWAllowed = CheckWinGISLicense();
#endif
		if (findDLL(processDIL,&lpExtString)) {
			assert(lpExtString);
			// 'lpExtString' will contain separating '~'; replace with '\0'.
			for (p = lpExtString; *p; p++) {
				if (*p == '~') *p = '\0';
			}
		}
		else if (!lpvReserved) {
			return (FALSE); // Fail DLL dynamic load
		}
		// initialize the export module
		ImgXInit();

		// Fall thru: Initialize the index for first thread.
	case DLL_THREAD_ATTACH:
		// The attached process created a new thread.
		// Initialize the TLS index for this thread if an error handler hasn't
		// already set it up...
		if (!(lpInfo = (LPTHREADINFO)LocalAlloc(LPTR,sizeof(THREADINFO)))) {
			return (FALSE);
		}
		// these fields already zeroed as result of allocation
		//lpInfo->dwDefaultUserInfo = 0;
		//lpInfo->lpfnDefaultStat = NULL;
		//lpInfo->lDefaultInterval = 0;
		lpInfo->lpError = (LPSTR) LocalAlloc(LPTR, 200 );
		lpInfo->status = IMG_OK;
		if (!TlsSetValue(TlsIndex,lpInfo)) {
			LocalFree(lpInfo);
			return (FALSE);
		}
#ifdef _WIN32
		if ( lpInfo->hPlugInHost = LoadLibrary( PLUGINHOSTSPEC ) )
			{
				lpInfo->bPlugInAvailable = TRUE ;
			  FreeLibrary ( lpInfo->hPlugInHost ) ;
				lpInfo->hPlugInHost = 0 ;
			}
		else
			lpInfo->bPlugInAvailable = FALSE ;
#endif
		break;

	// The thread of the attached process terminates.
	case DLL_THREAD_DETACH:
		// Release the allocated memory for this thread.
		if (lpInfo = (LPTHREADINFO) TlsGetValue(TlsIndex)) {
#ifdef _WIN32
			if ( lpInfo->hPlugInHost ) {
				FreeLibrary( lpInfo->hPlugInHost ) ;
				lpInfo->hPlugInHost = 0 ;
			}
#endif
			LocalFree((HLOCAL)lpInfo->lpError);
			LocalFree((HLOCAL)lpInfo);
		}
		break;

	// The DLL is detaching from a process, due to
	// process termination or call to FreeLibrary.
	case DLL_PROCESS_DETACH:
		// Release the allocated memory for this thread.
		if (lpInfo = (LPTHREADINFO) TlsGetValue(TlsIndex)) {
#ifdef _WIN32
			if ( lpInfo->hPlugInHost ) {
				FreeLibrary( lpInfo->hPlugInHost ) ;
				lpInfo->hPlugInHost = 0 ;
			}
#endif
			LocalFree((HLOCAL)lpInfo->lpError);
			LocalFree((HLOCAL)lpInfo);
		}
		// Release the TLS index.
		TlsFree(TlsIndex);

#if DEBUG
		if (hDebugDlg) {
			DestroyWindow(hDebugDlg);
			if (hLogFile) _lclose(hLogFile);
		}
#endif

		// release libraries loaded by this process
		for (i = MAXEXTS-1; i--; ) {
			if (EXTList[i].hLib) FreeLibrary(EXTList[i].hLib);
		}
		break;
	}
//	OutputDebugString("IM32: OK\n");
	return (TRUE);

	UNREFERENCED_PARAMETER(lpvReserved);
}

LPTHREADINFO GetThreadInfo(void)
{
	LPTHREADINFO lpInfo;

	// Retrieve pointer stored for us by system.
	if (!(lpInfo = (LPTHREADINFO) TlsGetValue(TlsIndex))) {
		// Whoops, this thread doesn't have any memory yet. Try to get some.
		if (lpInfo = (LPTHREADINFO)LocalAlloc(LPTR,sizeof(THREADINFO))) {
			lpInfo->lpError = (LPSTR) LocalAlloc(LPTR, 200 );
			lpInfo->status = IMG_OK;
			TlsSetValue(TlsIndex,lpInfo);
			// We also need to Init this memory
		}
	}
	return (lpInfo);
}


// Stub for apps that still want to call ().
int IMAPI ImgInit(void)
{
	if (!lpExtString) {
		// we didn't get any valid import types (no valid DILs) during a
		// static DLL load, so report that error now.
		SetError(NULL, IMG_NO_DILS, 0, NULL, hInst);
		return (IMG_ERR);
	}
	return (IMG_OK);
}

// Stub for apps that still want to call ImgShutdown().
void IMAPI ImgShutdown(void)
{
	return;
}

#else // WIN32


////////////////////////////////////////////////////////////////////////////
//
// int FAR PASCAL ImgInit( HANDLE hInst )
//
// Initializes the ImageMan. Checks for usable DLL's to support images
// and initializes them. Searches for DILS in the DLL directory, current dir
// and then the path. Retrieves list of supported file extensions for
// images from the DIL's.
//
// Returns: IMG_OK on successful initialization, 0 on failure.
//
// Globals: lpExtString, img_status
//
////////////////////////////////////////////////////////////////////////////

int  IMAPI ImgInit(void)
{
  struct 	find_t buf;
  int      stat, oldDrive, curDrive, i;
  DLLget_ver    lpfunc2;
  HINSTANCE   hDLL;
  HANDLE hPath;
  char  srchSpec[100];
  LPSTR p, path, curDir;
	int   bDILFlag=0; 													// 1 if at least 1 DIL was found, 0 otherwise
	char 	szModPath[100];
	char	bLoadedFlag;
	char 	jbuf[255];

	nRefCount++;

#if DEBUG
		if (!nRefCount) DebugInit();
#endif


	if( inittask == GetCurrentTask() ) 
		return IMG_OK;
	else 
	if( !inittask ) 
		inittask = GetCurrentTask();


	// find first empty entry in tasks array 
	for (i=0; i<MAXTASKS && tasks[i].hTask; i++);

	// now fill it with our very own default stuff
	tasks[i].hTask = GetCurrentTask();
	tasks[i].lpfnDefaultStat = NULL;
	tasks[i].lDefaultInterval = 0;
	tasks[i].dwDefaultUserInfo = 0;
	tasks[i].lpError = (LPSTR)GlobalAllocPtr( GHND, 200);
	
	szModPath[0] = '\0';	
	GetModuleFileName( hInst, szModPath, sizeof(szModPath) );

	p = szModPath + _fstrlen(szModPath) - 1;							// Strip off the the filename of the DLL
	while( p > szModPath && *p != '\\' )
		p--;

	*++p = '\0';

  // Get current path for searching, but only if this is the first time
	// ImgInit's been called...

	if( !lpExtString ) {
	  p = GetDOSEnvironment();
	  while (*p) {
	
	    if (!_fstrncmp(p, "PATH=", 5)) {
	      hPath = GlobalAlloc(DLLHND, _fstrlen(p)+_fstrlen(szModPath) );  // +1 for '/0', -5 for PATH=
	      path = (LPSTR) GlobalLock(hPath);
			 _fstrcpy(path, szModPath );
	      _fstrcat(path, ";.;");
	      _fstrcat(path, p+5);
	      break;
	    }
	
	    p += _fstrlen(p) + 1;
	  }
	
	  if( !path ) {
	      hPath = GlobalAlloc(DLLHND, _fstrlen(szModPath) + 5 );  // +1 for '/0' +3 for ;.;
	      path = (LPSTR) GlobalLock(hPath);
          _fstrcpy(path, szModPath );
	      _fstrcat(path, ";.;");
	  }

	}
	else {
		hPath = GlobalAlloc(DLLHND, _fstrlen(szModPath) + 1 );  
  	path = (LPSTR) GlobalLock(hPath);
		_fstrcpy(path, szModPath);
	}

  // now we search each directory in path, keeping a list of DILs that
  // we've found along the way...any DIL that we find is compared to those
  // in the list -- if the dll's already in the list, it's discarded,
  // otherwise it's added to the list & initialized.

  _fstrupr(path);
  oldDrive= curDrive = GetDrive();
  
  curDir = _fstrtok(path, ";");

  while (curDir) {

    // strip out drive letter & change drives if needed
    if (_fstrchr(curDir, ':')) {
      if ( (int)(*curDir - 'A' + 1) != curDrive) {
        SetDrive(*curDir - 'A' + 1);
        curDrive = *curDir - 'A' + 1;
      }
      curDir += 2;
    }
    else if (curDrive != oldDrive) {
        SetDrive(oldDrive);
    }

    _fstrcpy(srchSpec, curDir);
    if (srchSpec[_fstrlen(srchSpec) - 1] != '\\')
      _fstrcat(srchSpec,"\\");
    _fstrcat( srchSpec, DILFSPEC);

    stat=_fdos_findfirst(srchSpec,_A_NORMAL,&buf);

    while (!stat) {

		// check to see if we've already loaded this DLL
		for (i=0, bLoadedFlag = 0; i<MAXEXTS; i++) {
			if (!_fstrcmp(buf.name, EXTList[i].DLL)) 
				bLoadedFlag = 1;
		}

		if ( (!bLoadedFlag) && ((hDLL = LoadLibrary(buf.name)) != NULL)) {

			//add to list of currently loaded DLLs
			for (i=0; i<MAXEXTS; i++) {
				if (!LoadedDLL[i]) {
					LoadedDLL[i] = hDLL;
					break;
				}
			}
						
			if (LoadString((HINSTANCE) hDLL, 1000, (LPSTR)(&jbuf), 255)) {
				if( lpExtString ) {
					lpExtString = (LPSTR) GlobalReAllocPtr( lpExtString, GlobalSize( GlobalPtrHandle(lpExtString) )+_fstrlen(jbuf)+2, GMEM_MOVEABLE );
					if (!_fstrstr(lpExtString, jbuf)) {
						_fstrcat( lpExtString, jbuf );
					}
				}	
				else {
					lpExtString = (LPSTR) GlobalAllocPtr( DLLHND, _fstrlen(jbuf)+1 );
					_fstrcpy( lpExtString, jbuf );
				}

				lpfunc2 = (DLLget_ver) GetProcAddress( hDLL, GET_VER );
				p = _fstrtok(jbuf, "~");
				while (p) {
					p = _fstrtok(NULL, "~");	// skip the description
					addExtension(p+2, hDLL, buf.name, (*lpfunc2)());
					p = _fstrtok(NULL, "~");
				}
			} 
			else {
				FreeLibrary(hDLL);
			}

	    bDILFlag = 1; // remember that we've seen a DIL
	  }
  stat=_fdos_findnext(&buf);
}
    // NOTE that _fstrtok(NULL, ";") WON'T WORK! We used _fstrtok in the
    // inner loop, which reset our original stuff. We can, fortunately,
    // get around this problem by beginning our search from beginning of
    // next string.
    //
    // I know that, upon first examination, this code looks like it could
    // go beyond end of path's global memory block. It does, indeed,
    // set curDir to a value greater than the end of the path string;
    // however, the global block was allocated with a few extra bytes,
    // which are set to 0 upon allocation (DLLHND), so IT WORKS FINE
    // EVERY TIME!
    //
    curDir = _fstrtok(curDir+_fstrlen(curDir)+1, ";");
  }

  SetDrive( oldDrive );   								// make sure we end up where we started!

  GlobalUnlock( hPath );
  GlobalFree( hPath );

  if( bDILFlag ) {
		// Now remove the '~' chars from the extension list string & replace them with 0's
		p = _fstrtok( lpExtString, "~" );
		while( _fstrtok(NULL, "~") )
			;

    img_status = IMG_OK;
	  ImgXInit();
    return IMG_OK;
  } else {  												// we never saw a DIL, and that's an ERROR!
    SetError(NULL, IMG_NO_DILS, NULL, NULL, hInst);
    return IMG_ERR;
  }

}

//////////////////////////////////////////////////////////////////////////
//
// This function shuts down ImageMan and gets rid of stuff
//
//////////////////////////////////////////////////////////////////////////
void IMAPI ImgShutdown(void)
{
	int 	i;
	HTASK	hTask = GetCurrentTask();

	img_status = IMG_OK;
#ifdef DEBUG
	DEBUGOUT(IMGMANAPI, "%s", (LPSTR)"ImgShutDown");
#endif

	nRefCount--;
	if( !nRefCount ) {
		for (i=0; i<MAXEXTS; i++) {
			if (LoadedDLL[i]) {
				FreeLibrary(LoadedDLL[i]);
				LoadedDLL[i] = 0;
			}        
			EXTList[i].DLL[0] = 0;
			EXTList[i].ext[0] = 0;
			EXTList[i].hLib = 0;
		}
		GlobalFreePtr( lpExtString );
		lpExtString = NULL;

	// torch any default task info we may be hording
	for (i=0; i<MAXTASKS; i++) {
		if (tasks[i].hTask == hTask) {
			tasks[i].hTask = 0;
			GlobalFreePtr( tasks[i].lpError );
		}
	}

	if (inittask == hTask) inittask = 0;	
	
#if DEBUG
		if (nRefCount < 0 && hDebugDlg) {
			DestroyWindow(hDebugDlg);

			if (hLogFile) _lclose(hLogFile);
		}
#endif
	}

}


#ifndef IMAGEMANVB
int FAR PASCAL LibMain(HANDLE hInstance,WORD wDataSeg,WORD wHeapSize,LPSTR lpCmdLine)
{
	if (!wHeapSize) return (0);  /* no heap indicates fatal error */

//	OutputDebugString("IM16: Process++\n");

	hInst = (HINSTANCE) hInstance;
	return (ImgInit() == IMG_OK);
}

#endif



// Thread is a task under Windows 3.xx
// Assume current task
LPTHREADINFO GetThreadInfo(void)
{
	int i;
	HTASK hTask = GetCurrentTask();

	for (i = MAXTASKS; i--; ) {
		if (tasks[i].hTask == hTask)
			return (LPTHREADINFO)(&tasks[i]);
	}
	return (NULL);
}
#endif // WIN32

int CALLBACK WEP(int bSystemExit)
{
	return (0);
	UNREFERENCED_PARAMETER(bSystemExit);
}

////////////////////////////////////////////////////////////////////////////
//
// ImgInvert
//
// Inverts the specified image (makes a negative).
//
////////////////////////////////////////////////////////////////////////////

int IMAPI ImgInvert(HANDLE hImage)
{
	LPINTERNALINFO img;
	LPBITMAPINFO lpbi;
	long lBlockLen;
	HPLONG lpPtr;

#ifdef DEBUG
	DEBUGOUT(IMGMANAPI, "%s", (LPSTR)"ImgInvert");
#endif

	if ((img = GetImgPtr( hImage )) == NULL) return (IMG_ERR);

	if (!img->pDIB) {
		InternalLoad(img, NULL, &img->pDIB, &img->hWMF );
	}
	// now just invert each byte in the bytes remaining in the global memory
	// block. Simple, right?
	// Wrong! If it's a palette-based image (16 or 256 colors), we've got to
	// invert the stupid palette instead. It never ends, does it?
	//
	if (img->lpbi->bmiHeader.biBitCount == 4 || img->lpbi->bmiHeader.biBitCount == 8) {

		lBlockLen = PALCOLORS((LPBITMAPINFOHEADER)img->lpbi);

		lpbi = img->pDIB->GetLPBI(); 

		while (lBlockLen--) {
			*(LPDWORD)(lpbi->bmiColors+lBlockLen) ^= ~0;
			*(LPDWORD)(img->lpbi->bmiColors+lBlockLen) = *(LPDWORD)(lpbi->bmiColors+lBlockLen);
		}

	}
	else {
		lpPtr = (HPLONG)img->pDIB->GetDataPtr();	   
		lBlockLen = img->pDIB->GetImageMemSize();    

		lBlockLen /= 4;
		while (lBlockLen--)
			*lpPtr++ ^= ~0;

	}

	GlobalUnlock( hImage );

	return IMG_OK;
}

////////////////////////////////////////////////////////////////////////////
//
// void IMAPI ImgSetDefaultStatusProc
//
// Sets up a default status proc for the calling task
//
////////////////////////////////////////////////////////////////////////////
void IMAPI ImgSetDefaultStatusProc(STATUSPROC lpfnStat,long lInterval,DWORD dwUserInfo)
{
	LPTHREADINFO lpInfo;

	if (lpInfo = GetThreadInfo()) {
		lpInfo->lpfnDefaultStat = lpfnStat;
		lpInfo->lDefaultInterval = lInterval;
		lpInfo->dwDefaultUserInfo = dwUserInfo;
	}
#ifndef WIN32
	// if not in array, add it. (will always be there under Win32)
	else {
		lpInfo = GetThreadInfo();
		
		lpInfo->lpfnDefaultStat = lpfnStat;
		lpInfo->lDefaultInterval = lInterval;
		lpInfo->dwDefaultUserInfo = dwUserInfo;
		
	}
#endif
}


////////////////////////////////////////////////////////////////////////////
//
// int IMAPI ImgGetStatus(void)
//
// Returns: status of last ImageMan function call
//
// Globals: none
//
////////////////////////////////////////////////////////////////////////////
int IMAPI ImgGetStatus(void)
{
#ifdef WIN32
	LPTHREADINFO lpInfo;

	return ((lpInfo = GetThreadInfo()) ? lpInfo->status : IMG_ERR);
#else
	return (img_status);
#endif
}

#ifdef WIN32
void IMAPI ImgSetStatus(int status)
{
	LPTHREADINFO lpInfo;

	if (lpInfo = GetThreadInfo()) lpInfo->status = status;
}
#endif

////////////////////////////////////////////////////////////////////////////
//
// int IMAPI ImgPageCount(HANDLE hImage, LPINT lpCnt)
//
// Places the # of pages the image contains in lpCnt;
//
// Returns: IMG_OK on success, error code otherwise
//
// Globals: none
//
////////////////////////////////////////////////////////////////////////////
int IMAPI ImgPageCount(HANDLE hImage, LPINT lpCnt)
{
  LPINTERNALINFO img;

#ifdef DEBUG
	DEBUGOUT(IMGMANAPI, "%s(%d, %d)", (LPSTR)"ImgPageCount", hImage, lpCnt);
#endif

	if (!(img = GetImgPtr(hImage))) return (IMG_ERR);

	*lpCnt = img->nPages;

	GlobalUnlock(hImage);

	SETSTATUS(IMG_OK);

	return (IMG_OK);
}

////////////////////////////////////////////////////////////////////////////
//
// int IMAPI ImgSetPage(HANDLE hImage, int nPage)
//
// Prepares the image for reading from nPage. nPage is a 0-based offset
// into the pages of the image (first page is page 0).
//
// Returns: IMG_OK on success, error code otherwise
//
// Globals: none
//
////////////////////////////////////////////////////////////////////////////
int IMAPI ImgSetPage(HANDLE hImage, int nPage)
{
	int mystatus;
	LPINTERNALINFO img;

#ifdef DEBUG
	DEBUGOUT(IMGMANAPI, "%s(%d, %d)", (LPSTR)"ImgSetPage", hImage, nPage);
#endif

	if (!(img = GetImgPtr(hImage))) return (IMG_ERR);

	if (nPage < 0 || nPage >= img->nPages) {
		SetError(NULL, IMG_INV_PAGE, img->nExt, img->hFile, hInst);
		mystatus = IMG_INV_PAGE;
	} 
	else if ((mystatus = openDLL(img->nExt,img->hFile,img->lpHandle, img->lpAlias)) == IMG_OK) {
		if ((mystatus = set_pageDLL(img->nExt, img->hFile, nPage, img->lpAlias)) == IMG_OK) {

			if (img->pDIB ) {
				if( img->lpbi != img->pDIB->GetLPBI() ) {
					GlobalFreePtr(img->lpbi);
					img->lpbi = NULL;
				}

			} else
			if ( img->lpbi ) {
				GlobalFreePtr(img->lpbi);
				img->lpbi = NULL;
			}

		 	ImgUnload(hImage, TRUE, TRUE, TRUE);	// get rid of everything!
		 	buildDIBheadDLL(img->nExt, img->hFile, img, img->lpAlias);
		}
		close_keepDLL(img->nExt, img->hFile, img->lpAlias);
		img->nCurPage = nPage;
	}

	GlobalUnlock(hImage);
	SETSTATUS(mystatus);

	return (mystatus);
}

////////////////////////////////////////////////////////////////////////////
//
// int IMAPI ImgGetPage(HANDLE hImage, LPINT lpPage)
//
//	Returns the current page for the given image in lpPage.
//
// Returns: IMG_OK on success, error code otherwise
//
// Globals: none
//
////////////////////////////////////////////////////////////////////////////
int IMAPI ImgGetPage(HANDLE hImage, LPINT lpPage)
{
	LPINTERNALINFO img;

#ifdef DEBUG
	DEBUGOUT(IMGMANAPI, "%s(%d, %x:%x)", (LPSTR)"ImgGetPage", hImage, HIWORD(lpPage), LOWORD(lpPage));
#endif

	if (!(img = GetImgPtr(hImage))) return (IMG_ERR);

	*lpPage = img->nCurPage;

	GlobalUnlock(hImage);

	SETSTATUS(IMG_OK);
	return (IMG_OK);
}

////////////////////////////////////////////////////////////////////////////
//
//  ImgSetStatusProc(hImage, lpfnStatus, lCallInterval, dwUserInfo)
//
//	This function sets a status function to be used when reading 
//	images. The status function will be called after lCallInterval
//	bytes are processed by the DIL, and will be given the percentage
//	of the image that's been read.
//
//	Returns: Nothing.
//
////////////////////////////////////////////////////////////////////////////
int IMAPI ImgSetStatusProc(HANDLE hImage, STATUSPROC lpfnStatus, LONG lCallInterval, DWORD dwUserInfo)
{
	LPINTERNALINFO img;

#ifdef DEBUG
	DEBUGOUT(IMGMANAPI, "%s(%d, %x:%x, %ld)", (LPSTR)"ImgStatusFunc", hImage, HIWORD(lpfnStatus), LOWORD(lpfnStatus), lCallInterval);
#endif

	if (!(img = GetImgPtr(hImage))) return (IMG_ERR);

	img->lpfnStat = lpfnStatus;
	img->lInterval = lCallInterval;
	img->dwUserInfo = dwUserInfo;

	GlobalUnlock(hImage);

	SETSTATUS(IMG_OK);
	return (IMG_OK);
}

////////////////////////////////////////////////////////////////////////////
//
// LPSTR IMAPI ImgGetExt(void)
//
// Returns: string containing list of supported file extensions. Strings are 
// in a form suitable for passing to the Windows Common Dialog functions.
//
// *** The caller is responsible for freeing this memory block!!!! ***
//
//        For example:
//
//          "Tiff V5.0\0*.tif\0Windows Bitmap\0*.bmp;*.dib\0\0"
//
// Globals: none
//
////////////////////////////////////////////////////////////////////////////
LPSTR IMAPI ImgGetExt(void)
{
	LPSTR lpStr,lpMem,lpTmp;

#ifdef DEBUG
	DEBUGOUT(IMGMANAPI,"%s",(LPSTR)"ImgGetExt()");
#endif

	SETSTATUS(IMG_OK);

	if (!lpExtString) {
		SETSTATUS(IMG_NO_DILS);
		return (NULL);
	}

	if (lpStr = (LPSTR) GlobalAllocPtr(GHND,GlobalSize( GlobalPtrHandle(lpExtString) )*2)) {
		_fstrcpy(lpStr,"All Image Files");
		lpMem = lpStr+_fstrlen(lpStr)+1; // preserve the '\0'
		// add all "*.???" blocks for the "All Image Files" selection
		lpTmp = lpExtString;
		while (*lpTmp) {
			lpTmp += _fstrlen(lpTmp)+1; // point past description at Extensions
			_fstrcat(lpMem,lpTmp);
			_fstrcat(lpMem,";");
			lpTmp += _fstrlen(lpTmp)+1; // point at next description
		}
		lpMem += _fstrlen(lpMem); // find end of list...
		*(lpMem-1) = '\0';        // ... and remove trailing semicolon
		// then append all types and extensions 
		_fmemcpy(lpMem,lpExtString,(size_t)GlobalSize(GlobalPtrHandle(lpExtString)));
		return (lpStr);
	} 

	return (lpExtString);	// at least return something...
}

////////////////////////////////////////////////////////////////////////////
//
//
////////////////////////////////////////////////////////////////////////////
HANDLE IMAPI ImgOpenSolo(LPCSTR file, LPCSTR ext)
{
#ifdef DEBUG
	DEBUGOUT(IMGMANAPI, "%s(%s, %s)", (LPSTR)"ImgOpenSolo", (LPSTR)file, (LPSTR)ext);
#endif
	if (!(IsLicensed()))
	{
		if( !DisplayEvalDlg() )
			return IMG_ERR;
	}

	return (OpenFileInternal((LPVOID)file, _fstrlen(file)+1, 0L, 0L, ext, &file_ioblk, file));
}

HANDLE IMAPI ImgOpenCustom(LPVOID lpOpenInfo, WORD wSize, LPIMG_IO lpIO, LPSTR lpszImgIDStr, LPCSTR lpExt)
{
	if (lpExt) return (OpenFileInternal(lpOpenInfo, wSize, 0L, 0L, lpExt, lpIO, lpszImgIDStr));
	else return (OpenFileInternal(lpOpenInfo, wSize, 0L, 0L, "NUL", lpIO, lpszImgIDStr));	// NUL is a non-valid extension, forcing auto-detect
}

HANDLE IMAPI ImgOpenMem(LPCSTR hpMem, DWORD dwSize, LPCSTR lpExt)
{
	MEM_IO	mem_io;

	//++Sygsky: I moved this check before initialize code for code bum purposes :)
	if (!(IsLicensed()))
	{
		if( !DisplayEvalDlg() )
			return IMG_ERR;
	}


	mem_io.hpMem = (HPSTR) hpMem;
	mem_io.dwSize = dwSize;
	mem_io.dwUsed = dwSize;
	mem_io.hMem = NULL;
	mem_io.wFlags = 0;


	if (lpExt) return (OpenFileInternal(&mem_io, sizeof(MEM_IO), 0L, 0L, lpExt, &mem_ioblk, "MEM_IMG"));
	else return (OpenFileInternal(&mem_io, sizeof(MEM_IO), 0L, 0L, "NUL", &mem_ioblk, "MEM_IMG"));	//NUL is a non-valid extension, forcing auto-detect
}

////////////////////////////////////////////////////////////////////////////
//
//
////////////////////////////////////////////////////////////////////////////
HANDLE IMAPI ImgOpenEmbedded(LPCSTR file, LONG lOffset, LONG lLen, LPCSTR ext)
{
#ifdef DEBUG
	DEBUGOUT(IMGMANAPI, "%s(%s, %lu, %lu, %s)", (LPSTR)"ImgOpenEmbedded", file, lOffset, lLen, (LPSTR)ext);
#endif
	return (OpenFileInternal((LPVOID)file, _fstrlen(file)+1, lOffset, lLen, ext, &file_ioblk, file));
}

////////////////////////////////////////////////////////////////////////////
//
// int IMAPI ImgClose(HANDLE hImage)
//
// Frees all memory associated with the image given by hImage.
//
// Returns: IMG_OK on success, IMG_ERR on failure.
//
// Globals: errBuf, img_status
//
////////////////////////////////////////////////////////////////////////////
int IMAPI ImgClose( HANDLE hImage )
{
	LPINTERNALINFO img;

#ifdef DEBUG
			DEBUGOUT(IMGMANAPI, "%s(%d)", (LPSTR)"ImgClose", hImage);
#endif

	if (!(img = GetImgPtr(hImage))) 
		return (IMG_ERR);

	if (img->nExt != -1) 
		close_imageDLL(img->nExt, img->hFile, img->lpAlias);

	if (img->hBitmap) 
		DeleteBitmap(img->hBitmap);


	// In the case of memory resident images the lpbi pointer points to the internal DIB structure we
	// we have so there is no need to free it. Otherwise it has been allocated by the DIL and we must free it.

	if (img->pDIB ) {
		if( img->lpbi != img->pDIB->GetLPBI() )
			GlobalFreePtr(img->lpbi);
	} else
		if ( img->lpbi ) {
			GlobalUnlockPtr(img->lpbi);
			//GlobalFree(img->lpbi);
			GlobalFreePtr(img->lpbi);
		}



	if (img->pDIB)
		delete img->pDIB;

	if (img->hWMF) 
		DeleteMetaFile(img->hWMF);
	if (img->lpAlias) 
		GlobalFreePtr(img->lpAlias);
	if (img->lpOpenInfo) 
		GlobalFreePtr(img->lpOpenInfo);
	    
	GlobalUnlock(hImage);
	GlobalFree(hImage);

	return (GETSTATUS == IMG_OK ? IMG_OK : IMG_ERR);
}

///////////////////////////////////////////////////////////////////////////
//
//  int IMAPI ImgSetROP(HANDLE hImage, DWORD dwRop)
//
//  This function sets the ROP code used to place a raster image on the
// output device. Note that this ROP code IS NOT USED with vector images
// unless the vector image has been rendered into a bitmap (using CreateDDB).
//
///////////////////////////////////////////////////////////////////////////
int  IMAPI ImgSetROP(HANDLE hImage, DWORD dwNewROP)
{
	LPINTERNALINFO img;

#ifdef DEBUG
	DEBUGOUT(IMGMANAPI, "%s(%d, %lx)", (LPSTR)"ImgSetROP", hImage, dwNewROP);
#endif

	if (!(img = GetImgPtr(hImage))) return (IMG_ERR);

	img->dwROP = dwNewROP;
	GlobalUnlock(hImage);

	return (IMG_OK);
}

///////////////////////////////////////////////////////////////////////////
//
//  DWORD IMAPI ImgGetROP(HANDLE hImage)
//
//  Returns the current ROP code used for placing raster images on the
// output device.
//
///////////////////////////////////////////////////////////////////////////
DWORD IMAPI ImgGetROP(HANDLE hImage)
{
	LPINTERNALINFO img;
	DWORD dwROP;

	SETSTATUS(IMG_OK);
#ifdef DEBUG
	DEBUGOUT( IMGMANAPI, "%s(%d)", (LPSTR)"ImgGetROP", hImage );
#endif

	if (!(img = GetImgPtr(hImage))) return (IMG_ERR);

	dwROP = img->dwROP;

	GlobalUnlock(hImage);

	return (dwROP);
}

///////////////////////////////////////////////////////////////////////////
//
//  ImgUnload(hImage, bDIB, bDDB, bWMF)
//
// Allows caller to unload one or all of a given image's image handles.
// Note that if an image is memory-based (i.e., came from ImgFromClipboard
// or ImgFromDIB or something along those lines), you can't delete the
// hDIB or hWMF.
//
///////////////////////////////////////////////////////////////////////////
int IMAPI ImgUnload(HANDLE hImage, BOOL bDIB, BOOL bDDB, BOOL bWMF)
{
	LPINTERNALINFO img;

#ifdef DEBUG
	DEBUGOUT(IMGMANAPI, "%s(%d, %x, %x)", (LPSTR)"ImgUnload", hImage, bDIB, bDDB);
#endif

	if (!(img = GetImgPtr(hImage))) return (IMG_ERR);

	if (!(img->bFlags & IMG_MEM_BASED)) {
		
		if (bDIB && img->pDIB) {
			delete img->pDIB;
			img->pDIB = NULL;
		}

		if (bWMF && img->hWMF) {
			GlobalFree(img->hWMF);
			img->hWMF = NULL;
		}
	}

	if (bDDB && img->hBitmap) {
		DeleteBitmap(img->hBitmap);
		img->hBitmap = NULL;
	}

	if( img->lpbi ) {
		img->lpbi->bmiHeader.biWidth = img->lOrgWid;
		img->lpbi->bmiHeader.biHeight = img->lOrgHi;
	}

	GlobalUnlock(hImage);

	return (IMG_OK);
}

///////////////////////////////////////////////////////////////////////////
//
//  int ImgLoad(HANDLE, LPRECT)
//
// Forces the image, or the portion of it specified by LPRECT, to be 
// brought into memory. 
//
///////////////////////////////////////////////////////////////////////////
int IMAPI ImgLoad(HANDLE hImage, LPRECT lpSrc)
{
	LPINTERNALINFO img;
	LPBITMAPINFOHEADER	lpbi;

	if (!(img = GetImgPtr(hImage))) return (IMG_ERR);

#ifdef DEBUG
	if (lpSrc) DEBUGOUT(IMGMANAPI, "%s(%d, (%d,%d,%d,%d) )", (LPSTR)"ImgLoad", hImage, lpSrc->left, lpSrc->top, lpSrc->right, lpSrc->bottom);
	else DEBUGOUT(IMGMANAPI, "%s(%d, NULL)", (LPSTR)"ImgLoad", hImage);
#endif

	if (img->bFlags & IMG_MEM_BASED) {
		// Can't load or unload a mem-based image, just return OK
		SETSTATUS(IMG_OK);
	}
	else {
		if (img->bFlags & IMG_DISP_VECTOR) {
			if  (!(img->bFlags & IMG_RENDER_SELF)) {
				if (openDLL(img->nExt, img->hFile, img->lpHandle, img->lpAlias) == IMG_OK) {
					SETSTATUS(load_wmfDLL(img->nExt,img->hFile,&(img->hWMF),&(img->wmfBBOX), img->lpIO, img->lpAlias));
					img->hBitmap = (HBITMAP) NULL;
					img->pDIB = NULL;
					close_keepDLL(img->nExt, img->hFile, img->lpAlias);
				}
			}
		} 
		else if (InternalLoad(img, lpSrc, &img->pDIB, &img->hWMF ) == IMG_OK) {
			if (lpSrc) {
				img->lpbi->bmiHeader.biWidth = RECTWID(lpSrc);
				img->lpbi->bmiHeader.biHeight = RECTHI(lpSrc);
				if (img->pDIB) {
					lpbi = (LPBITMAPINFOHEADER) img->pDIB->GetLPBI(); 

					lpbi->biWidth = img->lpbi->bmiHeader.biWidth;
					lpbi->biHeight = img->lpbi->bmiHeader.biHeight;

				}
			}
			img->hBitmap = NULL;
		}
	}
	GlobalUnlock(hImage);

	return (GETSTATUS);
}

///////////////////////////////////////////////////////////////////////////
//
//  int InternalLoad(HANDLE, LPRECT)
//
///////////////////////////////////////////////////////////////////////////
int InternalLoad(LPINTERNALINFO img, LPRECT lpSrc,PDIB FAR *lpDIB, HMETAFILE FAR *lphWMF )
{
	long   lBufSize, lRowSize;
	int    nStartByte, nEndByte, nShiftCnt, nInBytes;
	int		Res;
	RECT   rTemp;
	LPSTR  lpBits = NULL;
	STATUS stat;
	LPBITMAPINFO lpbi;
	RECT	ECWRect;


	if ((!*lpDIB) && (!*lphWMF)) {
		if (img->bFlags & IMG_DISP_VECTOR) {
			if (openDLL(img->nExt, img->hFile, img->lpHandle, img->lpAlias) == IMG_OK) {
				SETSTATUS(load_wmfDLL(img->nExt, img->hFile, lphWMF, &(img->wmfBBOX), img->lpIO, img->lpAlias));
				*lpDIB = NULL;
				close_keepDLL( img->nExt, img->hFile, img->lpAlias);
			}
			return (GETSTATUS);
		} 
		else {
			//++Sygsky
			// get full image rect
//			SetRect(&rTemp, 0, 0, (int)img->lOrgWid-1, (int)img->lOrgHi-1);
			InitRect(&rTemp, (int)img->lOrgWid, (int)img->lOrgHi);
			if (!lpSrc) // fill if not set to the full rect
				lpSrc = (LPRECT)&rTemp;
			if (img->bFlags & IMG_SCALE_SELF) { // do the trick with self scaling image

				HANDLE hOldImage = GlobalHandle(img);
				HANDLE hNewImage;
				LPINTERNALINFO img1;
				
				// first read original rectangle of ECW image itself to detect supersampling danger
				Res = scale_imageDLL(img->nExt, img->hFile, 0, 0, &ECWRect);
				OffsetRect(&ECWRect, -ECWRect.left, -ECWRect.top);
				// check if supersampling possible from the parameters settings
				if ((RECTWID(&ECWRect) < RECTWID(lpSrc)) || (RECTHI(&ECWRect) < RECTHI(lpSrc)))	{ // yes, supersampling!

					PDIB	pNewDIB;
					 
//***************** load original bitmap and stretch it to wanted one *********************
					hNewImage = ImgCopy( hOldImage, RECTWID(&ECWRect), RECTHI(&ECWRect), lpSrc, COPY_DEL);
					if (!hNewImage)
						return IMG_ERR;
					img1 = (LPINTERNALINFO)GlobalLock(hNewImage);
					pNewDIB =  new DIB( RECTWID(lpSrc), RECTHI(lpSrc), img->lpbi->bmiHeader.biBitCount, true /*, img->lpbi */);
					if (pNewDIB)	{
						*lpDIB = pNewDIB;	// store new image! ****************************
						Res = InternalLoad( img1, NULL, &(img1->pDIB), lphWMF);
						if (Res = IMG_OK)	{  

							HANDLE hScale;

							// we have to scale the one-to-one DIB from ECW into the DIB wanted by user 
							hScale = SetDIBScale((LPBITMAPINFOHEADER)(img1->lpbi), &ECWRect, 
												 (LPBITMAPINFOHEADER)(img->lpbi), NULL, COPY_DEL);
							ScaleDIB( (HPSTR) img1->pDIB->GetDataPtr(), (HPSTR) pNewDIB->GetDataPtr(),	
									   RECTHI(&ECWRect), RECTHI(lpSrc), 0, hScale);
							EndDIBScale(hScale);

							// finish himmmm :o)
							GlobalUnlock(hNewImage);
							ImgClose(hNewImage);
						}
					} else
						Res = IMG_NOMEM;
					return Res;
				} else if (!EqualRect( lpSrc, &rTemp)) { // check image to be self scaling
					hNewImage = ImgCopy( hOldImage, RECTWID(lpSrc), RECTHI(lpSrc), lpSrc, COPY_DEL);
					if (!hNewImage)
						return IMG_ERR;
					img1 = (LPINTERNALINFO)GlobalLock(hNewImage);
					Res = InternalLoad( img1, NULL, lpDIB, lphWMF);
					if (Res = IMG_OK)	{
						// finish him :o)
						GlobalUnlock(hNewImage);
						ImgClose(hNewImage);
						return Res;
					}
				}
			}
			//--Sygsky
			if (lpSrc != (LPRECT)&rTemp)	{
				//
				// check to make sure the requested portion is a valid one
				//
				if ( (lpSrc->left < 0) || (lpSrc->top < 0)
					|| (lpSrc->right >= img->lOrgWid) 
					|| (lpSrc->bottom >= img->lOrgHi) ) {
					SetError(NULL, IMG_BAD_SRC, img->nExt, img->hFile, hInst);
				}
			}


			if (GETSTATUS == IMG_OK) {
				//
				// remember that all lines must be on DWORD boundaries
				//
				nStartByte = ((long)img->lpbi->bmiHeader.biBitCount * lpSrc->left)/8;
				nShiftCnt = ((long)img->lpbi->bmiHeader.biBitCount * lpSrc->left) % 8;
				nEndByte = ((long)img->lpbi->bmiHeader.biBitCount * (lpSrc->right+1))/8;

				lRowSize = ROWSIZE(RECTWID(lpSrc),img->lpbi->bmiHeader.biBitCount);

				nInBytes = (nEndByte - nStartByte) + 1;

				//
				// we need 1 extra byte on the end of the buffer in case nInBytes
				// is greater than lRowSize.
				//
				lBufSize = lRowSize * RECTHI(lpSrc);

#ifndef WIN32
				// Make sure the buffer is less than 16MB since thats the limit on Windows
				// memory blocks.
				if (lBufSize > MAXMEMSIZE) {
					SetError(NULL, IMG_TOOBIG, img->nExt, img->hFile, hInst);
					return (IMG_ERR);
				}
#endif

				// when we allocate the new block, allocate enough space for the
				// bitmapinfo information also, so we can create a packed DIB.
				//if (*lphDIB = GlobalAlloc(GHND,lBufSize+1+BITSOFFSET((LPBITMAPINFOHEADER)img->lpbi))) {
				//	lpBits = (HPSTR)GlobalLock(*lphDIB);
				//}

				*lpDIB = new DIB ( RECTWID(lpSrc), RECTHI(lpSrc), img->lpbi->bmiHeader.biBitCount, 1, img->lpbi );

				if ((*lpDIB)->IsValid() ) {
					lpbi = (*lpDIB)->GetLPBI();

					// first copy the bitmapinfo header & palette stuff.
					_fmemcpy(lpbi, img->lpbi,(int)BITSOFFSET((LPBITMAPINFOHEADER)img->lpbi));

					// Set the size of the resulting DIB to the selected portion
					lpbi->bmiHeader.biWidth = lpSrc->right - lpSrc->left + 1;
					lpbi->bmiHeader.biHeight = lpSrc->bottom - lpSrc->top + 1;
					lpbi->bmiHeader.biSizeImage = 0;

					lpBits = (*lpDIB)->GetDataPtr();

					if (openDLL(img->nExt, img->hFile, img->lpHandle, img->lpAlias) == IMG_OK) {
						if (GETSTATUS == IMG_OK) {
//							set_rowDLL(img->nExt, img->hFile, 0, img->lpAlias); //++ Sygsky: to init buffers
							SETSTATUS(set_rowDLL(img->nExt, img->hFile, lpSrc->top, img->lpAlias));
						}
						if (GETSTATUS == IMG_OK) {
							stat.lInterval = img->lInterval;
							stat.lpfnStat = img->lpfnStat;
							stat.lDone = stat.lAcc = 0;
							stat.lTotal = lRowSize * RECTHI(lpSrc);
							stat.dwUserInfo = img->dwUserInfo;
							stat.hImage = GlobalPtrHandle(img);
							
							// read all the buffer
							Res = read_rowsDLL(img->nExt, img->hFile, lpBits, RECTHI(lpSrc), (int)lRowSize, nStartByte, &stat, img->lpAlias);
							if (Res != IMG_OK)  // error was detected
								SetError(NULL, Res, img->nExt, img->hFile, hInst);
							if( stat.lpfnStat )
								(*stat.lpfnStat)(stat.hImage, 100, img->dwUserInfo);
						}
					}
					close_keepDLL( img->nExt, img->hFile, img->lpAlias);
					if (GETSTATUS != IMG_OK) {
						delete *lpDIB;
						*lpDIB = NULL;
					}
				} else {
					SetError(NULL, IMG_NOMEM, img->nExt, img->hFile, hInst);
				}
			}
		}
	}

	return GETSTATUS;
}

///////////////////////////////////////////////////////////////////////////
//
//  HGLOBAL IMAPI ImgGetDIB(HANDLE hImage, BOOL bNewDIB, LPRECT lpSrc)
//
// Returns a handle to a global memory block containing the image in packed
// DIB format. The block contains a BITMAPINFOHEADER/Palette & Bits as in CF_DIB
// format. If bNewDIB is specified, the caller gets a handle of his very
// own which he them becomes responsible for.
//
///////////////////////////////////////////////////////////////////////////

HGLOBAL IMAPI ImgGetDIB(HANDLE hImage, BOOL bNewDIB, LPRECT lpSrc)
{
	LPINTERNALINFO img;
	LPBITMAPINFOHEADER lpbi, lpbiDst;
	RECT Src;
	HGLOBAL hDIB = NULL;

	SETSTATUS(IMG_OK);
#ifdef DEBUG
	DEBUGOUT(IMGMANAPI, "%s(%d, %d)", (LPSTR)"ImgGetDIB", hImage, bNewDIB);
#endif
  
	if (!(img = GetImgPtr(hImage))) return (IMG_ERR);

	if (bNewDIB) {
		if (lpSrc) Src = *lpSrc;
		else {
			Src.left = Src.top = 0;
			Src.right = (int)img->lOrgWid - 1;
			Src.bottom = (int)img->lOrgHi - 1;
		}

		if (img->pDIB) {         // If image is loaded
			HPSTR hpSrcDIB,hpDstDIB;
			long lDstRowSize,lSrcRowSize,lDstSize;

			if (!lpSrc) {         // Just copy existing DIB
				hDIB = img->pDIB->GethDIB(1);
			} 
			else {
				hpSrcDIB = (HPSTR) img->pDIB->GetLPBI();
				lpbi = (LPBITMAPINFOHEADER)hpSrcDIB;

				lDstRowSize = ROWSIZE(RECTWID(&Src),lpbi->biBitCount);
				lSrcRowSize = img->pDIB->GetRowSize();

				lDstSize = lDstRowSize*RECTHI(&Src);
				lDstSize += BITSOFFSET(lpbi) + 16;

				hDIB = GlobalAlloc(GHND, lDstSize);

				if (!hDIB) {
					SetError(NULL, IMG_NOMEM, img->nExt, img->hFile, hInst);
					return (NULL);
				}

				hpDstDIB = (HPSTR) GlobalLock(hDIB);
				lpbiDst = (LPBITMAPINFOHEADER)hpDstDIB;

				// Copy BITMAP header to Dst DIB
				_fmemcpy(hpDstDIB,hpSrcDIB,(int)BITSOFFSET(lpbi));

				// Adjust Header info for portion
				lpbiDst->biWidth = Src.right - Src.left + 1;
				lpbiDst->biHeight = Src.bottom - Src.top + 1;
				lpbiDst->biSizeImage = 0;

				// ExtractDIB requires Pointers to 1st scanline ie highest address
				hpSrcDIB = (HPSTR) img->pDIB->GetDataPtr() + (lSrcRowSize * (lpbi->biHeight-1));
				hpDstDIB += lDstRowSize * (Src.bottom - Src.top) + BITSOFFSET(lpbi);

				ExtractDIB(lpbi,hpSrcDIB,hpDstDIB,&Src);

				// call ExtractDIB() to get the bits
				GlobalUnlock(hDIB);
			}
		} 
		else { // Image is not loaded -> Use InternalLoad to get a new image
			HMETAFILE junk = NULL;
			PDIB pTmp = NULL;

			// Load into temp pDIB			
			InternalLoad(img,&Src,&pTmp,&junk);

			hDIB = pTmp->GethDIB(1);
			// free pDIB
			delete pTmp;
		}
	} 
	else {
		if (!img->pDIB) {
			InternalLoad(img, NULL, &img->pDIB, &img->hWMF );
		}
		hDIB = img->pDIB->GethDIB(0);
	}

	GlobalUnlock(hImage);

	return (hDIB);
}

///////////////////////////////////////////////////////////////////////////
//
//  HANDLE IMAPI ImgGetDDB(HANDLE hImage, HDC hDC)
//
// Returns a handle to a DDB for the given image. The DIB is converted
// to a DDB based on hDC. 
//
// NOTE: If this routine returns an error, it's *not* always something
//			the caller should notify the user of. Since the hDIB is always
//			there, and ImgDrawImage will use it if it needs to, the image
//			will still show up -- just not quite as fast as a DDB.
//
///////////////////////////////////////////////////////////////////////////
HANDLE IMAPI ImgGetDDB(HANDLE hImage, HDC hDC)
{
	LPINTERNALINFO img;
	HANDLE hRet = NULL;

#ifdef DEBUG
	DEBUGOUT(IMGMANAPI, "%s(%d, %x)", (LPSTR)"ImgGetDDB", hImage, hDC);
#endif

	if (!(img = GetImgPtr(hImage))) return (IMG_ERR);

	if (!img->pDIB) {
		InternalLoad(img, NULL, &img->pDIB, &img->hWMF );
	}

	if (img->pDIB->IsValid() ) {

		hRet = CreateDIBitmap(hDC, (LPBITMAPINFOHEADER)img->pDIB->GetLPBI(), CBM_INIT,img->pDIB->GetDataPtr(),
			img->pDIB->GetLPBI(),DIB_RGB_COLORS);

	}

	GlobalUnlock(hImage);

	if (!hRet)
		SetError(NULL, IMG_NOMEM, img->nExt, img->hFile, hInst);

	return (hRet);
}

///////////////////////////////////////////////////////////////////////////
//
//  HANDLE IMAPI ImgGetWMF(HANDLE hImage, LPRECT lpBBox)
//
// Returns the metafile handle for the given image.
//
///////////////////////////////////////////////////////////////////////////
HANDLE IMAPI ImgGetWMF(HANDLE hImage, LPRECT lpBBox)
{
	LPINTERNALINFO img;
	HANDLE htmp;

#ifdef DEBUG
	DEBUGOUT(IMGMANAPI, "%s(%d)", (LPSTR)"ImgGetWMF", hImage);
#endif

	if (!(img = GetImgPtr(hImage))) return (IMG_ERR);

	if (!img->hWMF) {
		InternalLoad(img, NULL, &img->pDIB, &img->hWMF );
	}

	*lpBBox = img->wmfBBOX;
	htmp = img->hWMF;

	GlobalUnlock(hImage);

	return (htmp);
}

///////////////////////////////////////////////////////////////////////////
//
//  HANDLE IMAPI ImgFromWMF(HANDLE hWMF, LPRECT lpBBox)
//
// Returns an image handle from a Windows metafile handle
//
///////////////////////////////////////////////////////////////////////////
HANDLE IMAPI ImgFromWMF(HANDLE hWMF, LPRECT lpBBox)
{
	LPBITMAPINFO lpbi;
	LPINTERNALINFO lpInfo;
	HANDLE hImage;

	SETSTATUS(IMG_OK);
#ifdef DEBUG
	DEBUGOUT(IMGMANAPI, "%s(%x)", (LPSTR)"ImgFromWMF", hWMF);
#endif
	hImage = GlobalAlloc(GHND, sizeof(INTERNALINFO));
	if (hImage) {
		lpbi = (LPBITMAPINFO)GlobalAllocPtr(DLLHND, sizeof(BITMAPINFO));
		lpbi->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
		lpbi->bmiHeader.biWidth = RECTWID(lpBBox);
		lpbi->bmiHeader.biHeight = RECTHI(lpBBox);
		lpbi->bmiHeader.biXPelsPerMeter = lpbi->bmiHeader.biYPelsPerMeter = 0;
		lpbi->bmiHeader.biBitCount = 24;

		lpInfo = (INTERNALINFO FAR *)GlobalLock(hImage);

		lpInfo->sig[0] = 'I'; lpInfo->sig[1] = 'M'; lpInfo->sig[2] = '2'; lpInfo->sig[3] = '0';
		lpInfo->lpbi = lpbi;
		lpInfo->bFlags = IMG_MEM_BASED | IMG_DISP_VECTOR | IMG_PRNT_VECTOR;
		lpInfo->lOrgWid = lpbi->bmiHeader.biWidth;
		lpInfo->lOrgHi = lpbi->bmiHeader.biHeight;
		lpInfo->hFile = NULL;
		lpInfo->nExt = -1;
		lpInfo->hWMF = (HMETAFILE)hWMF;
		lpInfo->hBitmap = NULL;
		lpInfo->dwROP = SRCCOPY;
		lpInfo->lpHandle = NULL;
		lpInfo->lpIO = NULL;
		lpInfo->wmfBBOX = *lpBBox;
		lpInfo->pDIB = NULL;
		GlobalUnlock(hImage);
	}

	return (hImage);
}

///////////////////////////////////////////////////////////////////////////
//
//  HANDLE IMAPI ImgFromClipboard(void)
//
// Returns a handle to an image retrieved from the clipboard (if one exists
// on the clipboard, that is...)
// 
// Notes: This works only with DIBs on the clipboard at the moment. Should
//        work for CF_TIFF, CF_BMP also eventually.
//
///////////////////////////////////////////////////////////////////////////
HANDLE IMAPI ImgFromClipboard(void)
{
	HANDLE hCF;
	HANDLE	hNewImage = NULL;

	SETSTATUS(IMG_OK);
#ifdef DEBUG
	DEBUGOUT(IMGMANAPI, "%s", (LPSTR)"ImgFromClipboard()");
#endif

	if (IsClipboardFormatAvailable(CF_DIB)) {
		PDIB pDIB;

		OpenClipboard(GetActiveWindow());
		hCF = GetClipboardData(CF_DIB);

		pDIB = new DIB( hCF, 1);
		
		CloseClipboard();
		
		if( pDIB->IsValid() )
			hNewImage = ImgFromPDIB( pDIB );
		else {
			SetError(NULL, IMG_NOMEM, 0, 0, hInst);
			delete pDIB;
			return NULL;
		}


		return hNewImage;
	} 
	else if (IsClipboardFormatAvailable(CF_METAFILEPICT)) {
		LPMETAFILEPICT lpmf;
		RECT r;
		HANDLE hWmf;

		OpenClipboard(GetActiveWindow());
		hCF = GetClipboardData(CF_METAFILEPICT);
		lpmf = (LPMETAFILEPICT)GlobalLock(hCF);

		r.left = r.top = 0;
		r.right = lpmf->xExt-1;
		r.bottom = lpmf->yExt-1;

#ifdef WIN32
		hWmf = CopyMetaFile(lpmf->hMF, NULL);
#else
		hWmf = CopyMemBlock(lpmf->hMF);
#endif
		CloseClipboard();
		if(!hWmf) {
			SetError(NULL, IMG_PASTECLIP, 0, 0, hInst);
			return NULL;
		}
		GlobalUnlock(hCF);

		return (ImgFromWMF(hWmf,&r));
	}
	return (NULL);
}


///////////////////////////////////////////////////////////////////////////
//
// int IMAPI ImgToClipboard(HANDLE hImage)
//
// This function places the given image on the clipboard. Depending
//    on the type of image (raster/vector) a DIB or a WMF will be
//    put on the clipboard.
// 
// Notes: This should be altered to do a CF_TIFF format someday.
//
///////////////////////////////////////////////////////////////////////////
int IMAPI ImgToClipboard(HANDLE hImage)
{
	LPINTERNALINFO img;
	HANDLE hDIB,hWMF;

#ifdef DEBUG
	DEBUGOUT(IMGMANAPI, "%s(%d)", (LPSTR)"ImgToClipboard", hImage);
#endif

	if (!(img = GetImgPtr(hImage))) return (IMG_ERR);

	if (!img->pDIB && !img->hWMF) {
		InternalLoad(img, NULL, &img->pDIB, &img->hWMF );
	}

	if( img->pDIB && img->pDIB->IsValid() ) {
		hDIB = img->pDIB->GethDIB( 1 ); 

		if (!hDIB) 
			return (IMG_ERR);

		OpenClipboard(GetActiveWindow());
		EmptyClipboard();

		if (!SetClipboardData(CF_DIB, hDIB))
			SetError(NULL, IMG_PASTECLIP, img->nExt, img->hFile, hInst);

		CloseClipboard();
	} 
	else if (img->hWMF) {
		HANDLE hMFP;
		LPMETAFILEPICT lpmf;
#ifdef WIN32
		hWMF = CopyMetaFile(img->hWMF, NULL);
#else
		hWMF = CopyMemBlock(img->hWMF);
#endif

		if (!hWMF) {
			SetError(NULL, IMG_PASTECLIP, img->nExt, img->hFile, hInst);
			return (IMG_ERR);
		}

		hMFP = GlobalAlloc(GHND, sizeof(METAFILEPICT));
		lpmf = (LPMETAFILEPICT)GlobalLock(hMFP);
		lpmf->mm = MM_ANISOTROPIC;
		lpmf->xExt = (int)img->lOrgWid-1;
		lpmf->yExt = (int)img->lOrgHi-1;
		lpmf->hMF = (HMETAFILE) hWMF;

		GlobalUnlock(hMFP);

		OpenClipboard(GetActiveWindow());

		EmptyClipboard();

		if (!SetClipboardData(CF_METAFILEPICT, hMFP))
			SetError(NULL, IMG_PASTECLIP, img->nExt, img->hFile, hInst);

		CloseClipboard();
	}
				
	GlobalUnlock(hImage);

	return (GETSTATUS);
}

////////////////////////////////////////////////////////////////////////////
//
// HANDLE ImgCopy(hImage,nWidth,nHeight,lpSrc)
//
// This functions returns a copy of the given hImage as a new hImage. 
// The new image will be nWidth x nHeight in size. If nWidth or nHeight
// are set to 0, they become the dimension of the passed hImage.
//
// Note that if the original hImage hasn't been loaded yet, this 
// function will attempt to minimize the amount of memory used in
// loading the original and resizing it. In addition, when the copy
// function is complete, the original will not have a loaded image. This
// makes this a real handy function for making a low-resolution image
// from a monster-sized image without having to have the monster image
// totally in memory.
//
////////////////////////////////////////////////////////////////////////////
HANDLE IMAPI ImgCopy( HANDLE hImage, int nWidth, int nHeight, LPRECT lpSrc,	DWORD lFlags)
{
	LPBITMAPINFOHEADER lpbi,lpbiDst;
	LPINTERNALINFO img, img1;
	DWORD lOutRowSize,lInRowSize;
	HANDLE hScale, hInfo;
	RECT rSrc, rTmp;
	HANDLE hRet = NULL;
	UINT graybits;
	int mystatus;
	STATUS stat;
	PDIB	pNewDIB;
	LPTHREADINFO lpInfo = GetThreadInfo();
	
	stat.lpfnStat = NULL;

	if (!(img = GetImgPtr(hImage))) 
		return IMG_ERR;

	lpbi = (LPBITMAPINFOHEADER)img->lpbi;
	if (!lpSrc) {	// Fill maximum bounding  rectangle
		rSrc.left = rSrc.top = 0;
		rSrc.right = (int)lpbi->biWidth-1;
		rSrc.bottom = (int)lpbi->biHeight-1;
		lpSrc = &rSrc;
#ifdef DEBUG
		DEBUGOUT(IMGMANAPI, "%s(%d, %d, %d, NULL), lFlags = %lx", (LPSTR)"ImgCopy", hImage, nWidth, nHeight, lFlags);
#endif
	} 
#ifdef DEBUG
	else DEBUGOUT(IMGMANAPI, "%s(%d, %d, %d, (%d,%d,%d,%d) ) lFlags = %lx", (LPSTR)"ImgCopy", hImage, nWidth, nHeight, lpSrc->left, lpSrc->top, lpSrc->right, lpSrc->bottom, lFlags);
#endif

//++Sygsky: check image to be self-scaling (like ECW)
	SETSTATUS(IMG_OK);
	if (img->bFlags & IMG_SCALE_SELF) {
		hRet = (HANDLE)scale_imageDLL(img->nExt, img->hFile, nWidth, nHeight, lpSrc);
		if (hRet) {	// if success then create new info and copy the internal DIL handle to it
					// now allocate new structure to hold info
			if ((hInfo = GlobalAlloc(GHND, sizeof(INTERNALINFO))) == NULL) {
				VSETSTATUS(lpInfo,IMG_NOMEM);
				SetError(img->lpAlias, IMG_NOMEM, 0, NULL, hInst);
				GlobalUnlock(hImage);
				return (NULL);
			}
			img1 = (INTERNALINFO FAR *)GlobalLock(hInfo);
			
			// copy all the data
			*img1 = *img;

			//++Sygsky: we don't need lower data it our case for self scaling images
			(LPVOID)img1->lpOpenInfo = NULL;
			img1->hBitmap = 0;
			img1->pDIB = 0;
			img1->hWMF = 0;
			
			// detail new info
			img1->lOrgHi  = nHeight;
			img1->lOrgWid = nWidth;

			// make a copy of the alias for this image so we can use it
			// later for things like error messages, etc...
			img1->lpAlias = (LPSTR)GlobalAllocPtr(GHND, _fstrlen(img->lpAlias)+1);
			_fstrcpy(img1->lpAlias, img->lpAlias);

			if (lpInfo) {
				img1->lpfnStat = lpInfo->lpfnDefaultStat;
				img1->dwUserInfo = lpInfo->dwDefaultUserInfo;
				img1->lInterval = lpInfo->lDefaultInterval;
			};
			img1->hFile = hRet; // !!!!! it is the main item for DIL/DEL

			// now create DIB header in our internal info	
			mystatus = buildDIBheadDLL(img1->nExt, hRet, img1, img1->lpAlias);
			SETSTATUS(mystatus);
			GlobalUnlock(hImage);
			GlobalUnlock( hInfo );
			if (mystatus != IMG_OK)	{
				GlobalFree(hInfo);
				hInfo = NULL;
			}
			return (hInfo);
		}
	}
//--Sygsky

	if (img->bFlags & IMG_DISP_VECTOR) {
		GlobalUnlock(hImage);
		SetError(NULL, IMG_PROC_ERR, img->nExt, img->hFile, hInst);
		return (NULL);
	}


	if (!nWidth) 
		nWidth = (int)lpSrc->right - lpSrc->left + 1;

	if (!nHeight) 
		nHeight = (int)lpSrc->bottom - lpSrc->top + 1;


	// Make a destination w/ gray levels if requested: ScaleDIB() says that
	//   we must be reducing its height and not increasing its width.
	graybits = 0;
	if( lFlags & (COPY_ANTIALIAS|COPY_ANTIALIAS256) ) {
		if (lpbi->biBitCount == 1 && nWidth <= RECTWID(lpSrc) && nHeight < RECTHI(lpSrc)) {
			graybits =  lFlags&COPY_ANTIALIAS256 ? 8 : 4;
		}
		else lFlags &= ~(COPY_ANTIALIAS|COPY_ANTIALIAS256); // turn it off so SetDIBScale won't be confused
	}

	lInRowSize = ROWSIZE(lpbi->biWidth,lpbi->biBitCount); 

	//
	// allocate space for the new DIB and copy over color info
	// (temporarily change bits/pixel if destination has more bits than source)
	//
	if (graybits) {
		lOutRowSize = ROWSIZE(nWidth,graybits);
		//hDest = GlobalAlloc(GHND,sizeof(BITMAPINFOHEADER)+(1U<<graybits)*sizeof(RGBQUAD)+lOutRowSize*nHeight);
		pNewDIB = new DIB( nWidth, nHeight, graybits );
	}
	else {
		lOutRowSize = ROWSIZE(nWidth,lpbi->biBitCount);
		//hDest = GlobalAlloc(GHND, BITSOFFSET(lpbi)+lOutRowSize*nHeight + 16);
		pNewDIB = new DIB( nWidth, nHeight, lpbi->biBitCount, 1, img->lpbi );
		if ( (1 == lpbi->biBitCount) || (4 == lpbi->biBitCount) )
		{
			// Need to zero out memory or else windows 98 machines introduce trash.
			LPSTR	hpMem = NULL;

			hpMem = pNewDIB->GetDataPtr();
			if (hpMem)
			{
				DWORD	dw = NULL;
				dw = pNewDIB->GetImageMemSize();
#ifdef WIN32
				memset(hpMem, NULL, dw);
#else
				_fmemset(hpMem, NULL, dw);
#endif
			}
		}
	}

	if ( pNewDIB->IsValid() ) {
		//lpbiDst = (LPBITMAPINFOHEADER)GlobalLock(hDest);
		lpbiDst = (LPBITMAPINFOHEADER)pNewDIB->GetLPBI();

		_fmemcpy(lpbiDst, lpbi, (int)BITSOFFSET(lpbi));
		// now reset header info for correct width & height
		lpbiDst->biWidth = nWidth;
		lpbiDst->biHeight = nHeight;
		lpbiDst->biSizeImage = 0;

		// make any adjustments for a grayscale
		if (graybits) {
			LPRGBQUAD rgb;
			int i,inc;

			lpbiDst->biBitCount = graybits;

			graybits = 1U<<graybits;
			lpbiDst->biClrUsed = graybits;
			lpbiDst->biClrImportant = graybits;
			inc = graybits == 256 ? 1 : 0x11;
			rgb = ((LPBITMAPINFO)lpbiDst)->bmiColors; // create a grayscale ramp
			for (i = 0; i <= 0x00ff; i += inc,rgb++) {
				rgb->rgbRed = rgb->rgbGreen = rgb->rgbBlue = i;
			}
		}
	
		if (img->pDIB) { // DIB already in memory
		    HPSTR lpSrcDIB = (HPSTR) img->pDIB->GetDIBPtr();		
//			HPSTR lpSrcDIB = (HPSTR) img->pDIB->GetDataPtr();		

			// Look for a 1:1 copy
			if (nWidth == RECTWID(lpSrc) && nHeight == RECTHI(lpSrc)) {
				if (nWidth == lpbi->biWidth && nHeight == lpbi->biHeight) {
					// we're simply making a copy of the DIB
					hmemcpy( pNewDIB->GetDataPtr(), img->pDIB->GetDataPtr(), pNewDIB->GetImageMemSize() );
				}
				else {
					// extract a full-size DIB portion; we need to point at our
					// DIBs' first row, which is at the end of the memory block...
//					ExtractDIB(lpbi, lpSrcDIB+BITSOFFSET((LPBITMAPINFOHEADER)(lpSrcDIB)) + (((LPBITMAPINFOHEADER)lpSrcDIB)->biHeight-1)*lInRowSize,
//					(HPSTR)pNewDIB->GetDataPtr()+lOutRowSize*(lpbiDst->biHeight-1),
//					lpSrc);
					ExtractDIB(lpbi,(HPSTR)img->pDIB->GetDataPtr()+ (((LPBITMAPINFOHEADER)lpSrcDIB)->biHeight-1)*lInRowSize,
						(HPSTR)pNewDIB->GetDataPtr()+lOutRowSize*(lpbiDst->biHeight-1),
						lpSrc);
				}
			}
			else {
				// we'll have to scale the DIB in one way or another
				hScale = SetDIBScale(lpbi,lpSrc,lpbiDst,NULL,lFlags);

				ScaleDIB( (HPSTR) img->pDIB->GetDataPtr(), (HPSTR) pNewDIB->GetDataPtr(),
					RECTHI(lpSrc), nHeight, (int)lpbi->biHeight - lpSrc->bottom - 1, hScale);
				EndDIBScale(hScale);
			}
		}
		else { // DIB not in memory
			HPSTR lpSrcDIB,hpDstDIB;
			int nDstRowsLeft,nSrcRows,nDstRows,i,nDstRowErr,nDstErrAcc;

			// first, see if the source image will fit entirely into a single
			// copybuffer. If it does, simply load it, copy it, and be done
			// with the whole thing...
			if (nWidth == lpbi->biWidth && nHeight == lpbi->biHeight 
				|| (RECTHI(lpSrc) * lInRowSize <= COPYBUFSIZE)) {
				RECT	rTmpSrc = *lpSrc;
				rTmpSrc.left = 0; rTmpSrc.right = (int)lpbi->biWidth-1;	//load full rows only

				if (InternalLoad(img, &rTmpSrc, &img->pDIB, &img->hWMF) != IMG_OK) { //Sygsky: was "== IMG_ERROR"
					delete pNewDIB;
					//GlobalUnlock( hDest );
					//GlobalFree( hDest );

					GlobalUnlock(hImage);
					return IMG_ERR;
				}
 
				lpSrcDIB = (HPSTR) img->pDIB->GetDIBPtr();

				// Look for a 1:1 copy
				if (nWidth == RECTWID(lpSrc) && nHeight == RECTHI(lpSrc)) {
					if (nWidth == lpbi->biWidth && nHeight == lpbi->biHeight) {
						// we're simply making a copy of the DIB
						hmemcpy( pNewDIB->GetDataPtr(), img->pDIB->GetDataPtr(), nHeight*lInRowSize);
					}
					else {
						// extract a full-size DIB portion
						rTmpSrc = *lpSrc;
						rTmpSrc.bottom -= rTmpSrc.top;
						rTmpSrc.top = 0;
						ExtractDIB(lpbi,(HPSTR)img->pDIB->GetDataPtr()+(RECTHI(lpSrc)-1)*lInRowSize,
							(HPSTR)pNewDIB->GetDataPtr()+lOutRowSize*(lpbiDst->biHeight-1),
							&rTmpSrc);
					}
				}
				else {
					// we'll have to scale the DIB in one way or another
					hScale = SetDIBScale(lpbi,lpSrc,lpbiDst,NULL,lFlags);

					ScaleDIB( (HPSTR) img->pDIB->GetDataPtr(), (HPSTR) pNewDIB->GetDataPtr(),
						RECTHI(lpSrc), nHeight, 0, hScale);
					EndDIBScale(hScale);
				}
	
				delete img->pDIB;
				img->pDIB = NULL;
			}
			// 
			// if we're using COPY_DEL scaling, we can just jump to the particular
			// line we want from the image and scale it horizontally to fit 
			// our output buffer. If we're doing a fancy-schmancy scaling we'll
			// have to load the entire image, which could take a while for a large
			// image.
			//
			else if (lFlags == COPY_DEL) { // use the incremental approach
				int	nSrcErr, nSrcErrAcc, nSrcCur;

				//nHeight--;
				//nWidth--;
			
				nSrcRows = RECTHI(lpSrc) / nHeight;	//# of rows to skip
				nSrcErr = RECTHI(lpSrc) % nHeight;	//# err for each row
				nSrcErrAcc = 0;
                nSrcCur = lpSrc->top;
				nDstRowsLeft = nHeight;

				// setup our scaling, if needed
				if (nWidth == lpbi->biWidth && nHeight == lpbi->biHeight) {
					// make a copy of hDIB
					hScale = NULL;
				} else {
					// we'll have to scale the DIB in one way or another
					hScale = SetDIBScale(lpbi,lpSrc,lpbiDst,NULL,lFlags);
				}

				// start at the bottom of our images (which is, paradoxically, the top)
				hpDstDIB = (HPSTR) pNewDIB->GetDataPtr() + lOutRowSize*(nHeight-1);
								
				if (openDLL(img->nExt, img->hFile, img->lpHandle, img->lpAlias) == IMG_OK) {
					LPSTR lpBits = (LPSTR) GlobalAllocPtr(GHND,lInRowSize + 10);

					stat.lInterval = img->lInterval;
					stat.lpfnStat = img->lpfnStat;
					stat.lDone = stat.lAcc = 0;
					stat.lTotal = nHeight * lInRowSize; 
					stat.dwUserInfo = img->dwUserInfo;
					stat.hImage = hImage;

					while (nDstRowsLeft--) {
						if (GETSTATUS == IMG_OK) {
							set_rowDLL(img->nExt, img->hFile, nSrcCur, img->lpAlias);
							read_rowsDLL(img->nExt, img->hFile, lpBits, 1, (int)lInRowSize, 0, &stat, img->lpAlias);
							if (GETSTATUS != IMG_OK) {
								delete pNewDIB;

								//GlobalUnlock( hDest );
								//GlobalFree( hDest );

								GlobalUnlock( hImage );	
								GlobalFreePtr( lpBits );
								return IMG_ERR;
							}
						}
        
						if (hScale) {
							ScaleDIB((HPSTR) lpBits,hpDstDIB,1,1,0,hScale);
						}
						
						hpDstDIB -= lOutRowSize;
						nSrcCur += nSrcRows;
						nSrcErrAcc += nSrcErr;
						if (nSrcErrAcc >= nHeight) {
							nSrcCur++;
							nSrcErrAcc -= nHeight;
						}
					}
					GlobalFreePtr(lpBits);
				}
				close_keepDLL(img->nExt, img->hFile, img->lpAlias);

				if (hScale) EndDIBScale(hScale);
			}
			else {
				// setup our scaling, if needed
				if (nWidth == lpbi->biWidth && nHeight == lpbi->biHeight) {
					// make a copy of hDIB
					hScale = NULL;
				} else {
					// we'll have to scale the DIB in one way or another
					hScale = SetDIBScale(lpbi,lpSrc,lpbiDst,NULL,lFlags);
				}

				nSrcRows = (int)(COPYBUFSIZE / lInRowSize);
				nDstRows = (int)((nHeight * (long)nSrcRows)/ RECTHI(lpSrc)); // # of dest rows per nSrcRows
				nDstRowErr = (int)((nHeight * (long)nSrcRows) % RECTHI(lpSrc));	// error accumulator
				nDstRowsLeft = nHeight;
				nDstErrAcc = 0;

				// start at the bottom of our images (which is, paradoxically, the top)
				if (nHeight >= nDstRows) hpDstDIB = (HPSTR) pNewDIB->GetDataPtr() + lOutRowSize*(nHeight-nDstRows);
				else hpDstDIB = (HPSTR) pNewDIB->GetDataPtr();

				rTmp.top = lpSrc->top;
				rTmp.left = 0;
				rTmp.bottom = min(rTmp.top+nSrcRows-1, lpSrc->bottom);
				rTmp.right = (int) lpbi->biWidth-1;

				if (openDLL(img->nExt, img->hFile, img->lpHandle, img->lpAlias) == IMG_OK) {
					LPSTR lpBits = (LPSTR) GlobalAllocPtr(GHND,lInRowSize*(nSrcRows+1));

					set_rowDLL(img->nExt, img->hFile, lpSrc->top, img->lpAlias);

					stat.lInterval = img->lInterval;
					stat.lpfnStat = img->lpfnStat;
					stat.lDone = stat.lAcc = 0;
					stat.lTotal = lInRowSize * RECTHI(lpSrc);
					stat.dwUserInfo = img->dwUserInfo;
					stat.hImage = hImage;

					DEBUGOUT(IMGMANINT, "\tBand Scaling, nDstRows = %d, nSrcRows = %d, nHeight = %d", nDstRows, nSrcRows, nHeight);

					for (i = 0; i < RECTHI(lpSrc); i += nSrcRows) {
						if (GETSTATUS == IMG_OK) {
							read_rowsDLL(img->nExt, img->hFile, lpBits, RECTHI((&rTmp)), (int)lInRowSize, 0, &stat, img->lpAlias);
							if (GETSTATUS != IMG_OK) {
								delete pNewDIB;
								//GlobalUnlock( hDest );
								//GlobalFree( hDest );

								GlobalUnlock( hImage );	
								GlobalFreePtr( lpBits );
								return IMG_ERR;
							}
						}
        
						if (hScale) {
							DEBUGOUT(IMGMANINT, "\tOutrows = %d, left=%d, srcRows = %d", nDstRows,nDstRowsLeft,RECTHI((&rTmp)));
							if (nDstErrAcc > RECTHI(lpSrc)) {
								nDstErrAcc -= RECTHI(lpSrc);
								ScaleDIB((HPSTR) lpBits,hpDstDIB,RECTHI(&rTmp),nDstRows+1,0,hScale);
							}
							else {
								ScaleDIB((HPSTR) lpBits,hpDstDIB,RECTHI(&rTmp),nDstRows,0,hScale);
							}
						}

						OffsetRect(&rTmp,0,nSrcRows);
						nDstErrAcc += nDstRowErr;
						if (nDstErrAcc > RECTHI(lpSrc)) {
							nDstRowsLeft -= (nDstRows+1);
							hpDstDIB -= (nDstRows+1)*lOutRowSize;
						}
						else {
							nDstRowsLeft -= nDstRows;
							hpDstDIB -= nDstRows*lOutRowSize;
						}

						if (rTmp.bottom > lpSrc->bottom) {
							rTmp.bottom = lpSrc->bottom;
							nDstRows = nDstRowsLeft;
							hpDstDIB = (HPSTR) pNewDIB->GetDataPtr();
						}
					}
					GlobalFreePtr(lpBits);
				}
				close_keepDLL(img->nExt, img->hFile, img->lpAlias);

				if (hScale) EndDIBScale(hScale);
			}
		}

		hRet = ImgFromPDIB(pNewDIB);
	} else {
		SETSTATUS(IMG_NOMEM);
		SetError(NULL, IMG_NOMEM, img->nExt, img->hFile, hInst);
		hRet = NULL;
	}

 	if( stat.lpfnStat )
 		(*stat.lpfnStat)(stat.hImage, 100, img->dwUserInfo);

	GlobalUnlock(hImage);
	return (hRet);
}

////////////////////////////////////////////////////////////////////////////
//
// HANDLE ImgFromDIB(HDIB)
//
// Returns an ImageMan image handle given a handle to a packed DIB.
// Control of the HDIB is given to ImageMan.
//
////////////////////////////////////////////////////////////////////////////
HANDLE IMAPI ImgFromDIB( HANDLE hDIB )
{
	LPBITMAPINFO   lpbi;
	LPINTERNALINFO lpInfo;
	HANDLE         hImage=NULL;

	SETSTATUS(IMG_OK);
#ifdef DEBUG
	DEBUGOUT(IMGMANAPI, "%s(%x)", (LPSTR)"ImgFromDIB", hDIB);
#endif

	hImage = GlobalAlloc(DLLHND, sizeof(INTERNALINFO));
	if (hImage) {
		lpInfo = (LPINTERNALINFO)GlobalLock(hImage);

		lpInfo->pDIB = new DIB( hDIB );

		lpbi = lpInfo->lpbi = lpInfo->pDIB->GetLPBI();

		lpInfo->sig[0] = 'I';
		lpInfo->sig[1] = 'M';
		lpInfo->sig[2] = '2';
		lpInfo->sig[3] = '0';

		lpInfo->bFlags = IMG_MEM_BASED;
		lpInfo->lOrgWid = lpbi->bmiHeader.biWidth;
		lpInfo->lOrgHi = lpbi->bmiHeader.biHeight;
		lpInfo->hFile = NULL;
		lpInfo->nExt = -1;
		lpInfo->hBitmap = NULL;
		lpInfo->hWMF = NULL;
		lpInfo->dwROP = SRCCOPY;
		lpInfo->lpHandle = NULL;
		lpInfo->lpIO = NULL;
		lpInfo->nPages = 1;


		GlobalUnlock(hImage);
	} 
	else {
		SETSTATUS(IMG_NOMEM);
		SetError(NULL, IMG_NOMEM, 0, NULL, hInst);
	}
	
	return hImage;
}

////////////////////////////////////////////////////////////////////////////
//
// int IMAPI ImgCreateDDB(hImage, hDC, nWid, nHi)
//
// Forces the creation of an internal DDB for ImageMan to use
// when drawing. If nWid && nHi are specified, a DDB of that
// size is created, otherwise the original image size is used.
//
// NOTE: When creating a DDB from a WMF, you *MUST* specify nWid
//       and nHi, otherwise an error will be returned.
//
// Returns: IMG_ERR on error, IMG_OK on success.
//
// Globals: img_status, errBuf
//
////////////////////////////////////////////////////////////////////////////
int IMAPI ImgCreateDDB(HANDLE hImage, HDC hDC, int nWid, int nHi)
{
	LPINTERNALINFO img;
	LPBITMAPINFOHEADER lpbi;
	HDC hMemDC;

	if (!(img = GetImgPtr(hImage))) return (IMG_ERR);

	if (img->hBitmap) {
		DeleteBitmap(img->hBitmap);
		img->hBitmap = NULL;
	}

	if (img->pDIB && !img->hBitmap) {
		lpbi = (LPBITMAPINFOHEADER) img->pDIB->GetLPBI();

		hMemDC = lpbi->biBitCount == 1 ? CreateCompatibleDC(hDC) : hDC;

		img->hBitmap = CreateDIBitmap(hMemDC, lpbi,CBM_INIT,img->pDIB->GetDataPtr(),
			(LPBITMAPINFO)lpbi,DIB_RGB_COLORS);

		if (lpbi->biBitCount == 1) DeleteDC(hMemDC);

		if (!img->hBitmap) {
			SETSTATUS(IMG_NOMEM);
			SetError(NULL, IMG_NOMEM, img->nExt, img->hFile, hInst);
		}
	}
	else if (img->hWMF && !img->hBitmap && nWid && nHi) {
		img->hBitmap = CreateCompatibleBitmap(hDC, nWid, nHi);
		if (img->hBitmap) {
			hMemDC = CreateCompatibleDC(hDC);
			SelectBitmap(hMemDC, img->hBitmap);
			PatBlt(hMemDC, 0, 0, nWid, nHi, WHITENESS);
			SetMapMode(hMemDC, MM_ANISOTROPIC);
			SetViewportExtEx(hMemDC, nWid, nHi,NULL);
			PlayMetaFile(hMemDC, img->hWMF);
			DeleteDC(hMemDC);
			img->lpbi->bmiHeader.biWidth = nWid;
			img->lpbi->bmiHeader.biHeight = nHi;
		}
		else {
			SETSTATUS(IMG_NOMEM);
			SetError(NULL, IMG_NOMEM, img->nExt, img->hFile, hInst);
		}
	}

	GlobalUnlock(hImage);

	return (GETSTATUS);
}

////////////////////////////////////////////////////////////////////////////
//
// int IMAPI ImgDrawImage(HANDLE hFile, HDC hDC, LPRECT lpDest,
//              LPRECT lpSrc)
//
// Displays the image referenced by hFile on the device context hDC. lpDest
// specifies the rectangle on hDC to place the image in, and is given
// in logical coordinates. lpSrc specifies the portion of the image to
// display in the output rectangle, and is given in image units.
//
// Returns: IMG_ERR on error, IMG_OK on success.
//
// Globals: img_status, errBuf
//
////////////////////////////////////////////////////////////////////////////
int IMAPI ImgDrawImage(HANDLE hImage, HDC hDC, LPRECT lpDest, LPRECT lpSrc)
{
	LPINTERNALINFO img;
	LPBITMAPINFOHEADER lpbi;
	HDC hMemDC;
	RECT rSrc;
	HANDLE hOldPal;

#ifdef DEBUG
	if (lpSrc) DEBUGOUT(IMGMANAPI, "%s(%d, %x, (%d,%d,%d,%d), (%d,%d,%d,%d) )", (LPSTR)"ImgDrawImage", hImage, hDC, lpDest->left, lpDest->top, lpDest->right, lpDest->bottom, lpSrc->left, lpSrc->top, lpSrc->right, lpSrc->bottom);
	else DEBUGOUT(IMGMANAPI, "%s(%d, %x, (%d,%d,%d,%d), NULL)", (LPSTR)"ImgDrawImage", hImage, hDC, lpDest->left, lpDest->top, lpDest->right, lpDest->bottom);
#endif

	if (!(img = GetImgPtr(hImage))) 
		return (IMG_ERR);
//		SetRect( &rSrc, 0, 0, (int)img->lpbi->bmiHeader.biWidth-1, (int)img->lpbi->bmiHeader.biHeight-1);
	InitRect( &rSrc, (int)img->lpbi->bmiHeader.biWidth, (int)img->lpbi->bmiHeader.biHeight);

	if (!lpSrc)
		lpSrc = &rSrc;

	// if there's an hBitmap, *always* use it first.
	if (img->hBitmap ) {
		hMemDC = CreateCompatibleDC(hDC);

		// make sure the mem DC has a palette identical to source DC
		hOldPal = SelectPalette(hDC,(HPALETTE)GetStockObject(DEFAULT_PALETTE),0);
		SelectPalette(hDC, (HPALETTE) hOldPal, 0);
		SelectPalette(hMemDC, (HPALETTE) hOldPal, 0);


		SelectObject(hMemDC, img->hBitmap );
		//SelectObject(hMemDC, img->hBitmap ? img->hBitmap : img->hDIBSection );

		if (!StretchBlt(hDC,lpDest->left,lpDest->top,RECTWID(lpDest),RECTHI(lpDest),
			hMemDC,lpSrc->left,lpSrc->top,RECTWID(lpSrc),RECTHI(lpSrc),img->dwROP)) {

			SetError(NULL, IMG_BLITERR, img->nExt, img->hFile, hInst);
			SETSTATUS(IMG_BLITERR);
		}

		DeleteDC(hMemDC);
	}
	else if (img->bFlags & IMG_RENDER_SELF) {
		SETSTATUS(renderDLL(img->nExt, img->hFile, hDC, lpDest, lpSrc, img->lpAlias));
	}
	else if (!EqualRect(&rSrc, lpSrc)) { // not fullimage will be drawn!
		if (img->bFlags & IMG_SCALE_SELF) {	// check image to be self scaled
			
			HANDLE hImage1;
			
			// create smallest possible copy of our image. 
			// And moreover it is so easy :)
			hImage1 = ImgCopy(hImage, RECTWID(&rSrc), RECTHI(&rSrc), lpSrc, COPY_DEL);
			if (hImage1)  { // success ! Use it now
				InitRect(&rSrc, RECTWID(&rSrc), RECTHI(&rSrc));
				// Draw this one!
				SETSTATUS(ImgDrawImage( hImage1, hDC, &rSrc, &rSrc));
				ImgClose(hImage1);
			} else
				SETSTATUS(IMG_ERR);
		}
	}
	else	{
		// make sure the image is loaded
		if (!img->pDIB && !img->hBitmap && !img->hWMF) {
			if (InternalLoad( img, NULL, &img->pDIB, &img->hWMF ) != IMG_OK) {
				GlobalUnlock(hImage);
				return (IMG_ERR);
			}
		}
		
		if ((img->bFlags & IMG_DISP_VECTOR) && img->hWMF) {
			RECT	rect, ClipRect;
			int	wOrgX, wOrgY;
			int	wExtentX, wExtentY;
			int	wBBdx, wBBdy;

			SaveDC( hDC );

			rect = *lpDest;
			LPtoDP( hDC, (LPPOINT) &rect, 2 );

			GetClipBox( hDC, &ClipRect );

			LPtoDP( hDC, (LPPOINT)&ClipRect, 2 );

			IntersectClipRect( hDC, lpDest->left, lpDest->top, lpDest->right, lpDest->bottom );

			SetMapMode(hDC, MM_ANISOTROPIC);

			wBBdx = img->wmfBBOX.right - img->wmfBBOX.left;
			wBBdy = img->wmfBBOX.bottom - img->wmfBBOX.top;

			wExtentX = (int)(((long)wBBdx * (rect.right - rect.left)) / (lpSrc->right - lpSrc->left));
			wExtentY = (int)(((long)wBBdy * (rect.bottom - rect.top)) / (lpSrc->bottom - lpSrc->top));

			wOrgX = rect.left - (int)(((long)lpSrc->left * wExtentX) / wBBdx);
			wOrgY = rect.top - (int)(((long)lpSrc->top * wExtentY) / wBBdy);

			SetViewportOrgEx(hDC,wOrgX,wOrgY,NULL);
			SetViewportExtEx(hDC,wExtentX,wExtentY,NULL);

			SetWindowOrgEx(hDC,img->wmfBBOX.left,img->wmfBBOX.top,NULL);
			SetWindowExtEx(hDC,wBBdx,wBBdy,NULL);

			if (!PlayMetaFile(hDC, img->hWMF)) {
				SetError(NULL, IMG_METAFILEERR, img->nExt, img->hFile, hInst);
				SETSTATUS(IMG_METAFILEERR);
			}

			RestoreDC(hDC,-1);
		}
		else if (img->pDIB) {
			RECT rDIBsrc;

			rDIBsrc = *lpSrc;
			rDIBsrc.top = (int)img->lpbi->bmiHeader.biHeight - lpSrc->bottom - 1;

			lpbi = (LPBITMAPINFOHEADER) img->pDIB->GetLPBI();

#ifdef WIN32
			GdiFlush();
#endif
			if (!StretchDIBits(hDC, lpDest->left, lpDest->top, RECTWID(lpDest), RECTHI(lpDest),
				rDIBsrc.left, rDIBsrc.top, RECTWID(lpSrc), RECTHI(lpSrc), img->pDIB->GetDataPtr(),
				(LPBITMAPINFO)lpbi, DIB_RGB_COLORS, img->dwROP)) {
				SetError(NULL, IMG_DIBERR, img->nExt, img->hFile, hInst);
				SETSTATUS(IMG_DIBERR);
			}
		}
		GlobalUnlock(hImage);
	}
	return (GETSTATUS);
}

////////////////////////////////////////////////////////////////////////////
//
// int IMAPI ImgPrintImage(HANDLE hFile, HDC hPrnDC, LPRECT lpDest,
//                      LPRECT lpSrc)
//
//  Prints the image associated with hFile on the printer Device Context given
// by hPrnDC. lpDest specifies the rectangle on hPrnDC to place the image in,
// and is given in logical coordinates. lpSrc specifies the portion of the
// image to display in the output rectangle, and is given in image units.
//
// Returns: IMG_OK on success, error value on error
//
// Globals: img_status, errBuf
//
////////////////////////////////////////////////////////////////////////////

int IMAPI ImgPrintImage(HANDLE hImage, HDC hPrnDC, LPRECT lpDest, LPRECT lpSrc)
{
	LPINTERNALINFO img;
	LPBITMAPINFOHEADER	lpbi;
	HDC				hMemDC;
	RECT				rSrc;
	HANDLE			hOldPal;

	if (!(img = GetImgPtr(hImage))) return (IMG_ERR);

	if (!lpSrc) {
		rSrc.left = rSrc.top = 0;
		rSrc.right = (int)img->lpbi->bmiHeader.biWidth-1;
		rSrc.bottom = (int)img->lpbi->bmiHeader.biHeight-1;
		lpSrc = &rSrc;
	}

	// make sure the image is loaded
	if (!img->pDIB && !img->hBitmap && !img->hWMF) {
		if( InternalLoad( img, NULL, &img->pDIB, &img->hWMF ) == IMG_ERR ) {
			GlobalUnlock(hImage);
			return (IMG_ERR);
		}
	}

	if (img->bFlags & IMG_PRINT_SELF) {
		SETSTATUS(printDLL(img->nExt, img->hFile, hPrnDC, lpDest, lpSrc, img->lpAlias));
	}
	else {
		if (img->hWMF && img->bFlags & IMG_PRNT_VECTOR) {
			SETSTATUS(ImgDrawImage(hImage, hPrnDC, lpDest, lpSrc));
		}
		else {
			if (!img->hBitmap && !img->pDIB) {
					if (InternalLoad( img, NULL, &img->pDIB, &img->hWMF  ) != IMG_OK) {
					return (IMG_ERR);
				}
			}

			//
			// When printing, we want to use the DIB if possible to retain better
			// color fidelity, so we try the StretchDIBits call first...
			//
			if (img->pDIB) {
				lpbi = (LPBITMAPINFOHEADER) img->pDIB->GetLPBI();
			
				StretchDIBits(hPrnDC, lpDest->left, lpDest->top, RECTWID(lpDest), RECTHI(lpDest),
					lpSrc->left, (int)lpbi->biHeight - (lpSrc->top + RECTHI(lpSrc)), 
					RECTWID(lpSrc),RECTHI(lpSrc),img->pDIB->GetDataPtr(),
					(LPBITMAPINFO)lpbi, DIB_RGB_COLORS, img->dwROP);
			}
			else if (img->hBitmap) {
				hMemDC = CreateCompatibleDC(hPrnDC);

				// make sure the mem DC has a palette identical to source DC
				hOldPal = SelectPalette( hPrnDC, (HPALETTE) GetStockObject(DEFAULT_PALETTE), 0);
				SelectPalette(hPrnDC, (HPALETTE) hOldPal, 0);
				SelectPalette(hMemDC, (HPALETTE) hOldPal, 0);
		
				SelectBitmap(hMemDC, img->hBitmap);
				StretchBlt(hPrnDC, lpDest->left, lpDest->top, RECTWID(lpDest), RECTHI(lpDest),
					hMemDC, lpSrc->left, lpSrc->top, RECTWID(lpSrc), RECTHI(lpSrc), img->dwROP);
				DeleteDC(hMemDC);
			}
			else {
				// image isn't loaded at all -- we'll have to band the sucker!
				assert(FALSE);
			}
		}
	}
	GlobalUnlock(hImage);

	return (IMG_OK);
}

////////////////////////////////////////////////////////////////////////////
//
// int IMAPI ImgErrBox(HWND parent);
//
//  Displays a message box containing a message detailing the last error
// encountered within the ImageMan.
//
// Returns: IMG_OK
//
////////////////////////////////////////////////////////////////////////////
int IMAPI ImgErrBox(HWND hParent)
{
	char buf[250];
	LPSTR p;

	LPTHREADINFO lpInfo = GetThreadInfo();

	if( lpInfo ) {
		if (!lpInfo->lpError[0] )
		  return (IMG_OK);

		if (lpInfo->lpError[0] == ';') {   // ImageMan level error
			_fstrcpy(buf, lpInfo->lpError);
		} else {
			if (p = _fstrchr(lpInfo->lpError, ';')) {
				*p = '\0';
				_fstrcpy(buf, p+1);
				_fstrcat(buf, "\015File: ");
				_fstrcat(buf, lpInfo->lpError);
				*p = ';';
			} 
		}
		MessageBox(hParent, buf, "ImageMan Error!", MB_OK | MB_ICONEXCLAMATION);
	}

	return (IMG_OK);
}

////////////////////////////////////////////////////////////////////////////
//
// int IMAPI ImgErrString(LPSTR lpszBuf, int nMaxLen);
//
//  Returns a string containing a message detailing the last error
// encountered within the ImageMan. The message has the following
// format:
//
//    file name;error message
//
// where file name specifies the full path of the file that caused the error.
//
// Returns: IMG_OK
//
////////////////////////////////////////////////////////////////////////////
int IMAPI ImgErrString(LPSTR lpszBuf, int nMaxLen)
{
	LPTHREADINFO lpInfo = GetThreadInfo();

	if( lpInfo )
		_fstrncpy(lpszBuf, lpInfo->lpError, nMaxLen);

	return (IMG_OK);
}

////////////////////////////////////////////////////////////////////////////
//
// int IMAPI ImgVerString(LPSTR lpszBuf, int nMaxLen);
//
// Returns a string which contains textual descriptions of the current
// versions of ImageMan and each DIL associated with ImageMan.
//
// Returns: IMG_OK
//
////////////////////////////////////////////////////////////////////////////

int IMAPI ImgVerString(LPSTR lpszBuf, int nMaxLen)
{
	int i;
	char  oldDLL[30];

	lpszBuf[0] = '\0';
	oldDLL[0] = '\0';

	for (i=1; i<10 && nMaxLen > 0; i++) {
		if (_fstrcmp(oldDLL, EXTList[i].DLL) != 0) {
			_fstrncat(lpszBuf, "\015", nMaxLen);
			_fstrncat(lpszBuf, EXTList[i].ver, nMaxLen-4);
			nMaxLen -= (_fstrlen(EXTList[i].ver)+4);
			_fstrcpy(oldDLL, EXTList[i].DLL);
		}
	}
	return (IMG_OK);
}

///////////////////////////////////////////////////////////////////////////
//
//  	LPBITMAPINFO IMAPI ImgGetInfo(HANDLE hFile, LPINT lpbFlags)
//
//  	Returns a handle to an IMAGEINFO struct, which contains information
//	about the image specified by hFile.
//
///////////////////////////////////////////////////////////////////////////

LPBITMAPINFO IMAPI ImgGetInfo(HANDLE hImage, LPINT lpbFlags)
{
	LPINTERNALINFO  img;
  
#ifdef DEBUG
	DEBUGOUT(IMGMANAPI, "%s(%d, %x:%x)", (LPSTR)"ImgGetInfo", hImage, HIWORD(lpbFlags), LOWORD(lpbFlags));
#endif

	if (!(img = GetImgPtr(hImage))) return (IMG_ERR);

	if (lpbFlags) *lpbFlags = img->bFlags;

	GlobalUnlock(hImage);

	return (img->lpbi);
}

///////////////////////////////////////////////////////////////////////////
//
// int IMAPI ImgGetInfoStruct(HANDLE,LPBITMAPINFOHEADER,LPINT)
//
//  	Returns a handle to an IMAGEINFO struct, which contains information
//	about the image specified by hFile.
//
///////////////////////////////////////////////////////////////////////////

int IMAPI ImgGetInfoStruct(HANDLE hImage, LPBITMAPINFOHEADER lpbi, LPINT lpbFlags)
{
	LPINTERNALINFO img;

#ifdef DEBUG
	DEBUGOUT(IMGMANAPI, "%s(%d, %x:%x)", (LPSTR)"ImgGetInfo", hImage, HIWORD(lpbFlags), LOWORD(lpbFlags));
#endif

	if (!(img = GetImgPtr(hImage))) return (IMG_ERR);
	if (lpbFlags) *lpbFlags = img->bFlags;

	*lpbi = *(LPBITMAPINFOHEADER)img->lpbi;

	GlobalUnlock( hImage );

	return (IMG_OK);
}

HANDLE IMAPI ImgGetPalette( HANDLE hImage )
{
	LPBITMAPINFOHEADER lpbi;
	LPINTERNALINFO img;
	HANDLE hMem, hPal;
	LPLOGPALETTE lpPal;
	int i;
  
#ifdef DEBUG
	DEBUGOUT(IMGMANAPI, "%s(%d)", (LPSTR)"ImgGetPalette", hImage);
#endif

	if (!(img = GetImgPtr(hImage))) return (IMG_ERR);

	if (img->pDIB && img->pDIB->IsValid() ) 
		lpbi = (LPBITMAPINFOHEADER) img->pDIB->GetLPBI();
	else 
		lpbi = (LPBITMAPINFOHEADER)img->lpbi;

	if (!lpbi) 
		return (NULL);

	hPal = NULL;
	// Convert 1,4,8 -> 2,16,256. If > 8, 'i' gets 0.
	i = (1<<lpbi->biBitCount)&0x01ff;
	if (i) {
		i = lpbi->biClrUsed ? (int)lpbi->biClrUsed : i ;

		hMem = GlobalAlloc(GHND, sizeof(LOGPALETTE) + i*sizeof(PALETTEENTRY));
		lpPal = (LPLOGPALETTE)GlobalLock(hMem);
		lpPal->palNumEntries = i;
		lpPal->palVersion = 0x300;
  
		while (i--) {
			lpPal->palPalEntry[i].peRed = ((LPBITMAPINFO)lpbi)->bmiColors[i].rgbRed;
			lpPal->palPalEntry[i].peGreen = ((LPBITMAPINFO)lpbi)->bmiColors[i].rgbGreen;
			lpPal->palPalEntry[i].peBlue = ((LPBITMAPINFO)lpbi)->bmiColors[i].rgbBlue;
			lpPal->palPalEntry[i].peFlags = 0;
		}
  
		hPal = CreatePalette(lpPal);
  
		GlobalUnlock(hMem);
		GlobalFree(hMem);
	}
  
	GlobalUnlock(hImage);

	return (hPal);
}

HANDLE IMAPI ImgLoadLibraryExt(LPSTR lpExt)
{
	int nExt;

	nExt = find_ext(lpExt, 0, NULL, NULL);
	return (nExt ? LoadLibraryPath(EXTList[nExt].DLL) : NULL);
}

// --------------------------------------------------------------------
//    Helper functions
// --------------------------------------------------------------------


////////////////////////////////////////////////////////////////////////////
//
// int addExtension(LPSTR ext, LPSTR DLL);
//
// Adds the passed extension and DLL filename to the list of associated
// extensions and DLLs kept internally to the ImageMan.
//
// NOTE: The passed extension could actually consist of 2 or more
//   extensions separated by semi-colons (bmp;dib;rle).
//
// Returns: 0
//
// Globals: EXTList[]
//
////////////////////////////////////////////////////////////////////////////
static int NEAR PASCAL addExtension(LPSTR ext, HINSTANCE hLib, LPSTR lpDLL, LPSTR ver)
{
	int i;
	LPSTR p;

	if (p = _fstrchr(ext,';')) {
		// p+3: skip ";*." part of string
		addExtension(p+3, hLib, lpDLL, ver);
		*p = '\0';
	}

	// first make sure the extension isn't a duplicate!
	for (i = 1; i < MAXEXTS; i++) {
		if (!_fstrcmp(ext, EXTList[i].ext)) return (IMG_ERR);
	}

	// find an open slot
	for (i = 1; EXTList[i].DLL[0]; i++) ;

	EXTList[i].hLib = hLib;
	_fstrcpy(EXTList[i].DLL, lpDLL);
	_fstrcpy(EXTList[i].ext, ext);
	_fstrcpy(EXTList[i].ver, ver);

	// restore semicolon if necessary
	if (p) *p = ';';

	// get proc addresses from the DIL
	EXTList[i].funcs.init_image = (DLLinit_image)GetProcAddress(hLib, INIT_IMAGE);
	EXTList[i].funcs.close_image = (DLLclose_image)GetProcAddress(hLib, CLOSE_IMAGE);
	EXTList[i].funcs.open = (DLLopen)GetProcAddress(hLib, OPEN);
	EXTList[i].funcs.close_keep = (DLLclose_keep)GetProcAddress(hLib, CLOSE_KEEP);
	EXTList[i].funcs.read_rows = (DLLread_rows)GetProcAddress(hLib, READ_ROWS);
	EXTList[i].funcs.set_row = (DLLset_row)GetProcAddress(hLib, SET_ROW);
	EXTList[i].funcs.buildDIBHead = (DLLbuildDIBhead)GetProcAddress(hLib, BUILDDIB);
	EXTList[i].funcs.print = (DLLprint)GetProcAddress(hLib, PRINT);
	EXTList[i].funcs.render = (DLLrender)GetProcAddress(hLib, RENDER);
	EXTList[i].funcs.error_string = (DLLerror_string)GetProcAddress(hLib, ERROR_STRING);
	EXTList[i].funcs.img_name= (DLLimg_name)GetProcAddress(hLib, IMG_NAME);
	EXTList[i].funcs.load_wmf = (DLLload_wmf)GetProcAddress(hLib, LOAD_WMF);
	EXTList[i].funcs.set_page = (DLLset_page)GetProcAddress(hLib, SET_PAGE);
	EXTList[i].funcs.get_page = (DLLget_page)GetProcAddress(hLib, GET_PAGE);
	EXTList[i].funcs.verify_img = (DLLverify_img)GetProcAddress(hLib, VERIFY_IMG);
	EXTList[i].funcs.scale_img = (DLLscale_img)GetProcAddress(hLib, SCALE_SELF);
	return (IMG_OK);
}

////////////////////////////////////////////////////////////////////////////
//
// void get_ext(LPSTR extBuf, LPSTR file);
//
// Retrives the file extension from a given filename and places it in extBuf.
//
// Returns: nothing
//
// Globals: none
//
////////////////////////////////////////////////////////////////////////////
static void NEAR PASCAL get_ext(LPSTR extBuf, LPCSTR file)
{
	LPSTR p;

	//
	// start at the end of the string & search backwards for the first '.' 
	//
	if (p = _fstrrchr(file,'.')) _fstrcpy(extBuf,p+1);
	else *extBuf = '\0';
}

////////////////////////////////////////////////////////////////////////////
//
// int find_ext(LPSTR ext, int fHand, LPSTR lpVerfBuf);
//
// Searches EXTList[] for extension - if found, returns pointer to name of
// DLL that supports the extension.
//
// Returns: LPSTR to DLL filename on success, NULL on failure
//
// Globals: none
//
////////////////////////////////////////////////////////////////////////////
static int NEAR PASCAL find_ext(LPSTR ext, LPVOID lpHandle, LPSTR lpVerfBuf, LPIMG_IO lpIO)
{
	int i;

	// first try the most likely, i.e., the one that matches the passed ext
	for (i = 1; i < MAXEXTS; i++) {
		if (!lstrcmpi(ext,(LPSTR)EXTList[i].ext)) {
			if (!lpVerfBuf) return (i);
			else if (verify_imgDLL(i, lpVerfBuf, lpHandle, lpIO)) return (i);
		}
	}

	// well, it wasn't what it was *supposed* to be, so we'll go through
	// the list until we verify the sucker...
	for (i = 1; i < MAXEXTS; i++) {
		if (EXTList[i].DLL[0] && lpVerfBuf && (verify_imgDLL(i, lpVerfBuf, lpHandle, lpIO) == 1) ) return (i);
	}

	// if we can't get a hard verification (i.e., verify_img returns 1) then
	// we'll take the first "probable" verification, (i.e. verify_img returns 2).
	for (i = 1; i < MAXEXTS; i++) {
		if (EXTList[i].DLL[0] && lpVerfBuf && (verify_imgDLL(i, lpVerfBuf, lpHandle, lpIO) == 2) ) return (i);
	}

	return (0);
}
// void IMAPI LOADDS ImgXSetError(HANDLE hInstance, int nIndex, LPCSTR lpModule, LPCSTR lpFileName)

void PASCAL SetError(LPCSTR lpName, int nID, int nExt, HANDLE hImage, HINSTANCE hInstance)
{
	DLLimg_name lpNameFunc;
	HANDLE      hFileName;
	char        buf[200];
	char		tBuf[255];
	LPTHREADINFO lpInfo;

	
	SETSTATUS( nID );

	// Build the error message and store it's * in the TLS data

	if (lpName) 
		_fstrcpy(tBuf,lpName);
	else 
	if (nExt > 0 && GETSTATUS != IMG_INV_HAND) {
		lpNameFunc = (DLLimg_name)EXTList[nExt].funcs.img_name;
		hFileName = NULL;
		(*lpNameFunc)( hImage, &hFileName );
		if (hFileName) {
			lpName = (LPCSTR) GlobalLock( hFileName );
			_fstrcpy( tBuf, lpName );
			GlobalUnlock( hFileName );
		} else 
			_fstrcpy( tBuf, "No File" );
	} else 
		_fstrcpy( tBuf, "Invalid File" );

	_fstrcat( tBuf, ";");

	if (LoadString( hInstance, nID, buf, 199 )) 
		_fstrcat( tBuf, buf );

	// Set the TLS * to our Error (Free pointer if it points to something else)
 	lpInfo = GetThreadInfo();

	if( lpInfo )
		_fstrcpy( lpInfo->lpError, tBuf );
}

// -----------------------------------------------------------------------
//
//  DLL interface functions - call w/ handle to DLL to perform function
// All functions return the return value from call to the DLL. If there
// is an error returned, the routines fill in errBuf, either by calling the
// DLL error routine or by manually copying an error string.
//
//------------------------------------------------------------------------

static int NEAR PASCAL init_imageDLL(int nExt, LPCSTR lpszFile,LPHANDLE hImage, 
	LPVOID lpHandle,long offset,long lLen, LPIMG_IO lpIO, LPCSTR lpAlias)
{
	int mystatus;
	DLLinit_image lpfunc;
	LPTHREADINFO lpInfo = GetThreadInfo();

	lpfunc = EXTList[nExt].funcs.init_image;
	if (!lpfunc) {
		SetError(NULL, IMG_NO_PROC, 0, NULL, hInst);
		mystatus = IMG_NO_PROC;
	}
	else {
		mystatus = (*lpfunc)(lpszFile, hImage, lpHandle, offset, lLen, lpIO);
		if (mystatus != IMG_OK) {
			error_stringDLL(nExt, lpAlias,*hImage, lpInfo->lpError, ERRBUFSIZE);
		}
	}
	SETSTATUS(mystatus);
	return (mystatus);
}

static int NEAR PASCAL close_imageDLL(int nExt, HANDLE hImage, LPCSTR lpAlias)
{
	int mystatus;
	DLLclose_image lpfunc;
	LPTHREADINFO lpInfo = GetThreadInfo();

	lpfunc = EXTList[nExt].funcs.close_image;
	if (!lpfunc) {
		SetError(NULL, IMG_NO_PROC, 0, NULL, hInst);
		mystatus = IMG_NO_PROC;
	}
	else {
		mystatus = (*lpfunc)(hImage);
		if (mystatus != IMG_OK) {
			error_stringDLL(nExt, lpAlias, hImage, lpInfo->lpError, ERRBUFSIZE);
		}
	}
	SETSTATUS(mystatus);
	return (mystatus);
}

static int NEAR PASCAL openDLL(int nExt, HANDLE hImage, LPVOID lpHandle, LPCSTR lpAlias)
{
	int mystatus;
	DLLopen lpfunc;
	LPTHREADINFO lpInfo = GetThreadInfo();

	lpfunc = EXTList[nExt].funcs.open;
	if (!lpfunc) {
		SetError(NULL, IMG_NO_PROC, 0, NULL, hInst);
		mystatus = IMG_NO_PROC;
	}
	else {
		mystatus = (*lpfunc)(hImage, lpHandle);
		if (mystatus != IMG_OK) {
			error_stringDLL(nExt, lpAlias, hImage, lpInfo->lpError, ERRBUFSIZE);
		}
	}
	SETSTATUS(mystatus);
	return (mystatus);
}

static int NEAR PASCAL close_keepDLL(int nExt, HANDLE hImage, LPCSTR lpAlias)
{
	int mystatus;
	DLLclose_keep lpfunc;
	LPTHREADINFO lpInfo = GetThreadInfo();

	lpfunc = EXTList[nExt].funcs.close_keep;
	if (!lpfunc) {
		SetError(NULL, IMG_NO_PROC, 0, NULL, hInst);
		mystatus = IMG_NO_PROC;
	}
	else {
		mystatus = (*lpfunc)(hImage);
		if (mystatus != IMG_OK) {
			error_stringDLL(nExt, lpAlias, hImage, lpInfo->lpError, ERRBUFSIZE);
		}
	}
	SETSTATUS(mystatus);
	return (mystatus);
}

static int NEAR PASCAL read_rowsDLL(int nExt, HANDLE hImage, LPSTR buf, 
	UINT rows,UINT rowSize,UINT rowStart,LPSTATUS lpStat, LPCSTR lpAlias)
{
	int mystatus;
	DLLread_rows lpfunc;
	LPTHREADINFO lpInfo = GetThreadInfo();

	lpfunc = EXTList[nExt].funcs.read_rows;
	if (!lpfunc) {
		SetError(NULL, IMG_NO_PROC, 0, NULL, hInst);
		mystatus = IMG_NO_PROC;
	}
	else {
		mystatus = (*lpfunc)(hImage,buf,rows,rowSize,rowStart,lpStat);
		if (mystatus != IMG_OK) {
			error_stringDLL(nExt, lpAlias, hImage, lpInfo->lpError, ERRBUFSIZE);
		}
	}
//	SETSTATUS(mystatus);
	return (mystatus);
}

static int NEAR PASCAL set_rowDLL(int nExt, HANDLE hImage,UINT row, LPCSTR lpAlias)
{
	int mystatus;
	DLLset_row  lpfunc;
	LPTHREADINFO lpInfo = GetThreadInfo();

	lpfunc = EXTList[nExt].funcs.set_row;
	if (!lpfunc) {
		SetError(NULL, IMG_NO_PROC, 0, NULL, hInst);
		mystatus = IMG_NO_PROC;
	}
	else {
		mystatus = (*lpfunc)(hImage,row);
		if (mystatus != IMG_OK) {
			error_stringDLL(nExt, lpAlias, hImage, lpInfo->lpError, ERRBUFSIZE);
		}
	}
	SETSTATUS(mystatus);
	return (mystatus);
}

static int NEAR PASCAL buildDIBheadDLL(int nExt, HANDLE hImage, LPINTERNALINFO lpii, LPCSTR lpAlias)
{
	int mystatus;
	DLLbuildDIBhead lpfunc;
	LPTHREADINFO lpInfo = GetThreadInfo();

	lpfunc = EXTList[nExt].funcs.buildDIBHead;
	if (!lpfunc) {
		SetError(NULL, IMG_NO_PROC, 0, NULL, hInst);
		mystatus = IMG_NO_PROC;
	}
	else {
		mystatus = (*lpfunc)(hImage,lpii);
		if (mystatus != IMG_OK) {
			error_stringDLL(nExt, lpAlias, hImage, lpInfo->lpError, ERRBUFSIZE);
		}
	}
	SETSTATUS(mystatus);
	return (mystatus);
}

static int NEAR PASCAL printDLL(int nExt, HANDLE hImage, HDC hDC, LPRECT lpDest, 
	LPRECT lpSrc, LPCSTR lpAlias)
{
	int mystatus;
	DLLprint lpfunc;
	LPTHREADINFO lpInfo = GetThreadInfo();

	lpfunc = EXTList[nExt].funcs.print;
	if (!lpfunc) {
		SetError(NULL, IMG_NO_PROC, 0, NULL, hInst);
		mystatus = IMG_NO_PROC;
	}
	else {
		mystatus = (*lpfunc)(hImage, hDC, lpDest, lpSrc);
		if (mystatus != IMG_OK) {
			error_stringDLL(nExt, lpAlias, hImage, lpInfo->lpError, ERRBUFSIZE);
		}
	}
	SETSTATUS(mystatus);
	return (mystatus);
}

static int NEAR PASCAL renderDLL(int nExt, HANDLE hImage, HDC hDC, LPRECT lpDest, 
	LPRECT lpSrc, LPCSTR lpAlias)
{
	int mystatus;
	DLLrender lpfunc;
	LPTHREADINFO lpInfo = GetThreadInfo();

	lpfunc = EXTList[nExt].funcs.render;
	if (!lpfunc) {
		SetError(NULL, IMG_NO_PROC, 0, NULL, hInst);
		mystatus = IMG_NO_PROC;
	}
	else {
		mystatus = (*lpfunc)(hImage, hDC, lpDest, lpSrc);
		if (mystatus != IMG_OK) {
			error_stringDLL(nExt, lpAlias, hImage, lpInfo->lpError, ERRBUFSIZE);
		}
	}
	SETSTATUS(mystatus);
	return (mystatus);
}

static int NEAR PASCAL load_wmfDLL(int nExt, HANDLE hImage, HMETAFILE FAR *lpHWMF, 
	LPRECT lpBBOX, LPIMG_IO lpIO, LPCSTR lpAlias)
{
	int mystatus;
	DLLload_wmf lpfunc;
	LPTHREADINFO lpInfo = GetThreadInfo();

	lpfunc = EXTList[nExt].funcs.load_wmf;
	if (!lpfunc) {
		SetError(NULL, IMG_NO_PROC, 0, NULL, hInst);
		mystatus = IMG_NO_PROC;
	}
	else {
		mystatus = (*lpfunc)(hImage, lpHWMF, lpBBOX, lpIO);
#ifdef DEBUG
		DEBUGOUT(IMGMANINT, "wmfBBOX = %d,%d,%d,%d", lpBBOX->left, lpBBOX->top, lpBBOX->right, lpBBOX->bottom);
#endif
		if (mystatus != IMG_OK) {
			error_stringDLL(nExt, lpAlias, hImage, lpInfo->lpError, ERRBUFSIZE);
		}
	}
	SETSTATUS(mystatus);
	return (mystatus);
}

static int NEAR PASCAL set_pageDLL(int nExt, HANDLE hImage, int nPage, LPCSTR lpAlias)
{
	int mystatus;
	DLLset_page lpfunc;
	LPTHREADINFO lpInfo = GetThreadInfo();

	lpfunc = EXTList[nExt].funcs.set_page;
	if (!lpfunc) {
		SetError(NULL, IMG_NO_PROC, 0, NULL, hInst);
		mystatus = IMG_NO_PROC;
	}
	else {
		mystatus = (*lpfunc)(hImage, nPage);
		if (mystatus != IMG_OK) {
			error_stringDLL(nExt, lpAlias, hImage, lpInfo->lpError, ERRBUFSIZE);
		}
	}
	SETSTATUS(mystatus);
	return (mystatus);
}

static BOOL NEAR PASCAL verify_imgDLL(int nExt, LPSTR lpVerfBuf, LPVOID lpHandle, LPIMG_IO lpIO)
{
	DLLverify_img lpfunc;
	LPTHREADINFO lpInfo = GetThreadInfo();

	lpfunc = EXTList[nExt].funcs.verify_img;
	if (!lpfunc) {
		SetError(NULL, IMG_NO_PROC, 0, NULL, hInst);
		SETSTATUS(IMG_NO_PROC);
		return (0);
	}
	return ((*lpfunc)(lpVerfBuf, lpHandle, lpIO));
}

static int NEAR PASCAL error_stringDLL(int nExt, LPCSTR lpName, HANDLE hImage,
	LPSTR buf, int bufSize)
{
	DLLerror_string lpErrFunc;
	int nNameLen;

	//
	// don't add the filename if error resulted from bad handle
	//
	if (GETSTATUS != IMG_INV_HAND) {
		_fstrncpy(buf, lpName, bufSize);
		nNameLen = _fstrlen(lpName);
	}
	else {
		nNameLen = 0;
	}

	buf += nNameLen;
	*buf++ = ';';

	lpErrFunc = EXTList[nExt].funcs.error_string;
	(*lpErrFunc)(buf, bufSize - nNameLen - 1);
#ifdef DEBUG
	MessageBox( 0, buf, "ECW report", MB_OK+MB_ICONEXCLAMATION);
#endif
	return (IMG_OK);
}

static int NEAR PASCAL scale_imageDLL(int nExt, HANDLE hImage, UINT nWidth, UINT nHeight, LPRECT lpSrc) 
{
	int mystatus;
	DLLscale_img lpfunc;
	LPTHREADINFO lpInfo = GetThreadInfo();

	lpfunc = EXTList[nExt].funcs.scale_img;
	if (!lpfunc) {
		SetError(NULL, IMG_NO_PROC, 0, NULL, hInst);
		mystatus = 0;
	}
	else
		mystatus = (int)((*lpfunc)(hImage, nWidth, nHeight, lpSrc));
	return mystatus;
}

static HANDLE NEAR PASCAL OpenFileInternal(LPVOID lpData, int nDataLen, long offset, long lLen, LPCSTR ext, LPIMG_IO lpIO, LPCSTR lpAlias)
{
	char extBuf[4];      // buffer for extension
	HANDLE  hTmp;
	INTERNALINFO  tmpInfo;
	LPINTERNALINFO img;
	char verifyBuf[VERIFYBUFSIZE];	// buf to hold header validation bytes
	LPTHREADINFO lpInfo = GetThreadInfo();
	LPVOID	lpHandle, lpTmpData;

	// we have to make a copy of this information so we can be sure it'll
	// be around when we need it.
	lpTmpData = GlobalAllocPtr(GHND, nDataLen);
	_fmemcpy(lpTmpData, lpData, nDataLen);

	// first make sure that there are some valid extensions. If not, return
	// error.

	if (EXTList[1].ext[0] == '\0') {   // didn't find any extensions
		VSETSTATUS(lpInfo,IMG_NO_DILS);
		SetError(lpAlias, IMG_NO_DILS, 0, NULL, hInst);
		SetError(NULL, IMG_NO_DILS, 0, NULL, hInst);
		GlobalFreePtr ( lpTmpData ) ;
		return (NULL);
	}

	VSETSTATUS(lpInfo,IMG_OK);

	//
	// next, open up the file and extract the 1st 100 bytes in order to
	// figger out what type of file it is.
	//
	if (lpIO->open((LPSTR)lpTmpData, OF_READ | OF_SHARE_COMPAT, &lpHandle) != IMG_OK) {
		VSETSTATUS(lpInfo,IMG_NOFILE);
		SetError(lpAlias, IMG_NOFILE, 0, NULL, hInst);
		GlobalFreePtr ( lpTmpData ) ;
		return (NULL);
	}

	lpIO->seek(lpHandle, offset, 0);

	if (lpIO->read(lpHandle, (LPVOID)verifyBuf, VERIFYBUFSIZE) != VERIFYBUFSIZE) {
		lpIO->close(lpHandle);
		VSETSTATUS(lpInfo,IMG_FILE_ERR);
		SetError(lpAlias, IMG_FILE_ERR, 0, NULL, hInst);
		GlobalFreePtr ( lpTmpData ) ;
		return (NULL);
	}

	// setup proper extension type for this file
	if (!ext) 
		get_ext(extBuf, (LPCSTR) lpTmpData);
	else 
		_fstrcpy(extBuf,ext);

	if ((tmpInfo.nExt = find_ext(extBuf, lpHandle, verifyBuf, lpIO)) == 0) {
		lpIO->close(lpHandle);
		VSETSTATUS(lpInfo,IMG_INV_FILE);
		SetError(lpAlias, IMG_INV_FILE, 0, NULL, hInst);
		GlobalFreePtr ( lpTmpData ) ;
		return (NULL);
	}

	tmpInfo.nPages = 1;		//initialize this puppy *before* calling buildDIBhead

	//
	// initialize the image with the DLL
	//
	// at this point, responsibility for the open file is handed over to the
	// DIL.
	//
	lpIO->seek(lpHandle, offset, 0);

	if (init_imageDLL(tmpInfo.nExt, (LPCSTR) lpTmpData,&tmpInfo.hFile,lpHandle,offset,lLen,lpIO,lpAlias) != IMG_OK) {
		lpIO->close( lpHandle ) ;
		GlobalFreePtr ( lpTmpData ) ;
		return (NULL);
	}

	// create the bitmap info header
	if (buildDIBheadDLL(tmpInfo.nExt, tmpInfo.hFile, &tmpInfo, lpAlias) != IMG_OK) {
		lpIO->close( lpHandle ) ;
		GlobalFreePtr ( lpTmpData ) ;
		return (NULL);
	}

	// protect ourselves from 0 height/width images - just in case
	if ( ( 0 == tmpInfo.lpbi->bmiHeader.biHeight ) ||
		   ( 0 == tmpInfo.lpbi->bmiHeader.biWidth  ) ) {
		lpIO->close( lpHandle ) ;
		VSETSTATUS(lpInfo,IMG_INV_FILE) ;
		SetError(lpAlias, IMG_INV_FILE, 0, NULL, hInst ) ;
		GlobalFreePtr ( lpTmpData ) ;
		return ( NULL ) ;
	}

	if (close_keepDLL(tmpInfo.nExt, tmpInfo.hFile, lpAlias) != IMG_OK) {
		GlobalFreePtr ( lpTmpData ) ;
		return (NULL);
	}

	// now allocate new structure to hold info
	if ((hTmp = GlobalAlloc(DLLHND,sizeof(INTERNALINFO))) == NULL) {
		VSETSTATUS(lpInfo,IMG_NOMEM);
		SetError(lpAlias, IMG_NOMEM, 0, NULL, hInst);
		GlobalFreePtr ( lpTmpData ) ;
		return (NULL);
	}

	img = (INTERNALINFO FAR *)GlobalLock(hTmp);
	tmpInfo.sig[0] = 'I';
	tmpInfo.sig[1] = 'M';
	tmpInfo.sig[2] = '2';
	tmpInfo.sig[3] = '0';

	tmpInfo.hBitmap = NULL;
	tmpInfo.lpHandle = lpHandle;
	tmpInfo.lpIO = lpIO;
	tmpInfo.hDIB = NULL;
	tmpInfo.hWMF = NULL;
	tmpInfo.dwROP = SRCCOPY;
	tmpInfo.lpfnStat = NULL;
	tmpInfo.nCurPage = 0;
	tmpInfo.lpOpenInfo = lpTmpData;

	tmpInfo.pDIB = NULL;

	// make a copy of the alias for this image so we can use it
	// later for things like error messages, etc...
	tmpInfo.lpAlias = (LPSTR)GlobalAllocPtr(GHND, _fstrlen(lpAlias)+1);
	_fstrcpy(tmpInfo.lpAlias, lpAlias);

	if (lpInfo) {
		tmpInfo.lpfnStat = lpInfo->lpfnDefaultStat;
		tmpInfo.dwUserInfo = lpInfo->dwDefaultUserInfo;
		tmpInfo.lInterval = lpInfo->lDefaultInterval;
	}
	else {
		tmpInfo.lpfnStat = NULL;
		tmpInfo.dwUserInfo = 0;
		tmpInfo.lInterval = 0;
	}

	*img = tmpInfo;
	GlobalUnlock( hTmp );
	SETSTATUS(IMG_OK);

	return (hTmp);
}

LPINTERNALINFO NEAR PASCAL GetImgPtr(HGLOBAL hImage)
{
	LPINTERNALINFO lpII;

	SETSTATUS(IMG_OK);

	if (!(lpII = (LPINTERNALINFO)GlobalLock(hImage))) {
		SetError(NULL, IMG_INV_HAND, 0, NULL, hInst);
		SETSTATUS(IMG_INV_HAND);
		return (NULL);
	}

	if (lpII->sig[0] != 'I' || lpII->sig[1] != 'M' || lpII->sig[2] != '2' || lpII->sig[3] != '0' ) {
		SetError(NULL, IMG_INV_HAND, 0, NULL, hInst);
		SETSTATUS(IMG_INV_HAND);
		GlobalUnlock( hImage );
		return (NULL);
	}

	return (lpII);
}

static HGLOBAL NEAR PASCAL CopyMemBlock( HGLOBAL hSrc )
{
	HGLOBAL hDst;
	HPSTR hpFrom, hpTo;
	DWORD dwCnt;

	dwCnt = GlobalSize(hSrc);
	if (!(hDst = GlobalAlloc(GHND,dwCnt))) {
		SetError(NULL, IMG_NOMEM, 0, NULL, hInst);
		SETSTATUS(IMG_NOMEM);
		return (NULL);
	}

	if (!(hpTo = (HPSTR)GlobalLock(hDst))) {
		GlobalFree(hDst);
		SetError(NULL, IMG_NOMEM, 0, NULL, hInst);
		SETSTATUS(IMG_NOMEM);
		return (NULL);
	}

	if (!(hpFrom = (HPSTR)GlobalLock(hSrc))) {
		GlobalUnlock(hDst);
		GlobalFree(hDst);
		SetError(NULL, IMG_NOMEM, 0, NULL, hInst);
		SETSTATUS(IMG_NOMEM);
		return (NULL);
	}

	hmemcpy(hpTo, hpFrom, dwCnt);

	GlobalUnlock(hSrc);
	GlobalUnlock(hDst);

	return (hDst);
}


#if DEBUG
BOOL FAR PASCAL _export DebugPanelProc(HWND hDlg,UINT message,WPARAM wParam,
	LPARAM lParam)
{
	int nTab;
	BOOL bTrans;

	switch (message) {
	case WM_INITDIALOG:            /* message: initialize dialog box */
		nTab = 2;
		SendMessage(GetDlgItem(hDlg,IDC_LIST1),LB_SETTABSTOPS,1,(LPARAM)(UINT)&nTab);
		bLog = FALSE;
		nCurLine = nEndLine = 0;
		nStartLine = 32767;
		SetDlgItemInt(hDlg, IDC_STARTLINE, nStartLine, 0);
		SetDlgItemInt(hDlg, IDC_ENDLINE, nEndLine, 0);

		ShowWindow( hDlg, SW_MINIMIZE ); 
		return (TRUE);

	case WM_PAINT:
		if (IsIconic( hDlg ) ) {
			PAINTSTRUCT ps;
			HANDLE hIcon;
			RECT rect;

			BeginPaint( hDlg, &ps );
			GetClientRect( hDlg, &rect );

                        FillRect( ps.hdc, &rect,  (HBRUSH)GetStockObject( LTGRAY_BRUSH) );
			hIcon = LoadIcon( hInst, MAKEINTRESOURCE(100) );
								
                        DrawIcon( ps.hdc, 2, 2, (HICON)hIcon );

			EndPaint( hDlg, &ps );
			return TRUE;
		}
		break;

	case WM_COMMAND:   
		switch (wParam) {
		case IDC_LOG:
			if (!bLog && GetLogFile(hDlg, LogFile)) {
				OFSTRUCT	of;
				if (hLogFile = OpenFile(LogFile,&of,OF_CREATE|OF_WRITE)) {
					bLog = TRUE;
					SetDlgItemText(hDlg, IDC_LOG, "Log Off");
				}
			}
			else {
				_lclose(hLogFile);
				hLogFile = NULL;
				SetDlgItemText(hDlg, IDC_LOG, "Log On");
				bLog = FALSE;
			}
			break;
		case IDC_CLEAROUTPUT:
			SendDlgItemMessage(hDlg, IDC_LIST1, LB_RESETCONTENT, 0, 0);
			break;

		case IDC_STARTLINE:
			if (HIWORD(lParam) == EN_CHANGE) {
				nStartLine = GetDlgItemInt(hDlg, IDC_STARTLINE, &bTrans, 0);
			}
			break;
		case IDC_ENDLINE:
			if (HIWORD(lParam) == EN_CHANGE) {
				nEndLine = GetDlgItemInt(hDlg, IDC_ENDLINE, &bTrans, 0);
			}
			break;
		}
	}
	return (FALSE);               /* Didn't process a message    */
}

int WINAPI GetLogFile(HANDLE hWnd, LPSTR lpBuf)
{
	OPENFILENAME ofn;
	LPSTR szFilter = "Debug Log Files\0*.log\0\0";

	ofn.lStructSize = sizeof(OPENFILENAME);
        ofn.hwndOwner = (HWND)hWnd;
	ofn.lpstrFilter = szFilter;
	ofn.lpstrCustomFilter = (LPSTR) NULL;
	ofn.nMaxCustFilter = 0;
	ofn.nFilterIndex = 1;
	ofn.lpstrFile = lpBuf;
	ofn.nMaxFile = 255;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = NULL;
	ofn.lpstrInitialDir = NULL;
	ofn.lpstrTitle = (LPSTR)"Select Log File";
	ofn.Flags = OFN_PATHMUSTEXIST; 
	ofn.nFileOffset = 0;
	ofn.nFileExtension = 0;
	ofn.lpstrDefExt = "*.log";

	return GetOpenFileName(&ofn);
}

void DebugInit(void)
{
	hDebugDlg = CreateDialog((HMODULE)ERRBUFSIZE, "DebugPanel", NULL, DebugPanelProc);
}

BOOL __cdecl FAR DebugStringOut(UINT wCategory, LPSTR lpFormat, LPVOID vargs)
{
	/* local variables */
	char 		szBuf[255];
	WORD		wFlags = 0;
	BOOL		bResult = FALSE;

	if (wCategory == DILSETLINE) {
		nCurLine = *((LPINT)lpFormat);
	}
	else {
		// build our test word
		wFlags |= IsDlgButtonChecked(hDebugDlg, IDC_DBGIMGMAN) ? IMGMANAPI : 0;
		wFlags |= IsDlgButtonChecked(hDebugDlg, IDC_DBGINTERNAL) ? IMGMANINT : 0;
		wFlags |= IsDlgButtonChecked(hDebugDlg, IDC_DBGDIL) ? DILAPI : 0;
		wFlags |= IsDlgButtonChecked(hDebugDlg, IDC_DBGDILINT) ? DILINT : 0;
		wFlags |= IsDlgButtonChecked(hDebugDlg, IDC_DBGIMAGE) ? IMGDETAIL : 0;
		wFlags |= IMGMANERR | DILERR;
	
		if ((wFlags & wCategory) && nCurLine <= nStartLine && nCurLine >= nEndLine) {
			wvsprintf(szBuf,lpFormat,(char *)vargs);
			SendMessage(GetDlgItem(hDebugDlg, IDC_LIST1), LB_ADDSTRING, 0, (LPARAM)(LPSTR)szBuf);
			UpdateWindow(GetDlgItem(hDebugDlg, IDC_LIST1));
			if (bLog && hLogFile) {
				_fstrcat(szBuf, "\r\n");
				_lwrite(hLogFile, szBuf, _fstrlen(szBuf));
			}
		}
	}
	/* return result */
	return (bResult);
}       
 

void __cdecl FAR Debug(UINT wCategory,LPSTR lpFormat,...)
{
	va_list vaArgs;

        //if ( DebugStringOut != NULL ) {
		va_start(vaArgs,lpFormat);
		DebugStringOut(wCategory, lpFormat, vaArgs);
		va_end(vaArgs);
	//}
}

#else

BOOL WINAPI DebugPanelProc(HWND hDlg,UINT message,WPARAM wParam,LPARAM lParam)
{
	return (FALSE);
	UNREFERENCED_PARAMETER(hDlg);
	UNREFERENCED_PARAMETER(message);
	UNREFERENCED_PARAMETER(wParam);
	UNREFERENCED_PARAMETER(lParam);
}

int WINAPI GetLogFile(HANDLE hWnd, LPSTR lpBuf)
{
	return (0);
	UNREFERENCED_PARAMETER(hWnd);
	UNREFERENCED_PARAMETER(lpBuf);
}

void __cdecl FAR Debug(UINT wCategory,LPSTR lpFormat,...)
{
	UNREFERENCED_PARAMETER(wCategory);
	UNREFERENCED_PARAMETER(lpFormat);
}

BOOL __cdecl FAR DebugStringOut(UINT wCategory,LPSTR lpFormat,...)
{
	return (FALSE);
	UNREFERENCED_PARAMETER(wCategory);
	UNREFERENCED_PARAMETER(lpFormat);
}       

#endif /* DEBUG */


BOOL DisplayEvalDlg(void)
{
	static int	nOpens;

	if( !(nOpens++ % 50) )
		return (DisplayBotherScreen((long)hInst));
	else
		return (TRUE);
}


HANDLE IMAPI ImgGetHBitmap( HANDLE hImage )
{
	LPINTERNALINFO img;
	
	if ((img = GetImgPtr( hImage )) == NULL) 
		return (IMG_ERR);

	if( img->pDIB )
		return img->pDIB->GethDIBSection();
	else {
		SetError(NULL, IMG_NOTAVAIL, 0, NULL, hInst);
		return NULL;
	}
}

LPVOID IMAPI ImgGetDIBPtr( HANDLE hImage )
{
	LPINTERNALINFO img;

	if ((img = GetImgPtr( hImage )) == NULL) 
		return (IMG_ERR);

	if( !img->pDIB )
		return NULL;
	else
		return (LPVOID) img->pDIB->GetDIBPtr();
}

LPVOID IMAPI ImgGetDataPtr( HANDLE hImage )
{
	LPINTERNALINFO img;

	if ((img = GetImgPtr( hImage )) == NULL) 
		return (IMG_ERR);

	if( !img->pDIB )
		return NULL;
	else
		return (LPVOID) img->pDIB->GetDataPtr();
}

HANDLE IMAPI ImgFromPDIB( PDIB pNewDIB )
{
	LPBITMAPINFO   lpbi;
	LPINTERNALINFO lpInfo;
	HANDLE         hImage=NULL;

	SETSTATUS(IMG_OK);
#ifdef DEBUG
	DEBUGOUT(IMGMANAPI, "%s(%x)", (LPSTR)"ImgFrompDIB", pNewDIB);
#endif

	hImage = GlobalAlloc(DLLHND, sizeof(INTERNALINFO));
	if (hImage) {
		lpbi = (LPBITMAPINFO) pNewDIB->GetLPBI();
		lpInfo = (LPINTERNALINFO)GlobalLock(hImage);

		lpInfo->sig[0] = 'I';
		lpInfo->sig[1] = 'M';
		lpInfo->sig[2] = '2';
		lpInfo->sig[3] = '0';

		lpInfo->lpbi = lpbi;
		lpInfo->bFlags = IMG_MEM_BASED;
		lpInfo->lOrgWid = lpbi->bmiHeader.biWidth;
		lpInfo->lOrgHi = lpbi->bmiHeader.biHeight;
		lpInfo->hFile = NULL;
		lpInfo->nExt = -1;
		lpInfo->hBitmap = NULL;
		lpInfo->hWMF = NULL;
		lpInfo->dwROP = SRCCOPY;
		lpInfo->lpHandle = NULL;
		lpInfo->lpIO = NULL;
		lpInfo->nPages = 1;

		lpInfo->pDIB = pNewDIB;

		GlobalUnlock(hImage);
	} 
	else {
		SETSTATUS(IMG_NOMEM);
		SetError(NULL, IMG_NOMEM, 0, NULL, hInst);
	}
	
	return hImage;
}

HANDLE IMAPI ImgFromDIBSection( HBITMAP hDIBSection )
{
	HANDLE         hImage=NULL;

#ifdef WIN32
	PDIB			pNewDIB;
	LPBITMAPINFO   lpbi;
	LPINTERNALINFO lpInfo;


	SETSTATUS(IMG_OK);
#ifdef DEBUG
	DEBUGOUT(IMGMANAPI, "%s(%x)", (LPSTR)"ImgFromDIBSection", hDIBSection);
#endif

	pNewDIB = new DIB( hDIBSection );

	if( !pNewDIB->IsValid() ) {
		SETSTATUS( IMG_BADBITMAP);
		delete pNewDIB;
		return NULL;
	}

	hImage = GlobalAlloc(DLLHND, sizeof(INTERNALINFO));

	if (hImage) {
		lpbi = (LPBITMAPINFO) pNewDIB->GetDIBPtr();
		lpInfo = (LPINTERNALINFO)GlobalLock(hImage);

		lpInfo->sig[0] = 'I';
		lpInfo->sig[1] = 'M';
		lpInfo->sig[2] = '2';
		lpInfo->sig[3] = '0';

		lpInfo->lpbi = lpbi;
		lpInfo->bFlags = IMG_MEM_BASED;
		lpInfo->lOrgWid = lpbi->bmiHeader.biWidth;
		lpInfo->lOrgHi = lpbi->bmiHeader.biHeight;
		lpInfo->hFile = NULL;
		lpInfo->nExt = -1;
		lpInfo->hBitmap = NULL;
		lpInfo->hWMF = NULL;
		lpInfo->dwROP = SRCCOPY;
		lpInfo->lpHandle = NULL;
		lpInfo->lpIO = NULL;
		lpInfo->nPages = 1;

		lpInfo->pDIB = pNewDIB;

		GlobalUnlock(hImage);
	} 
	else {
		SETSTATUS(IMG_NOMEM);
		SetError(NULL, IMG_NOMEM, 0, NULL, hInst);
	}

#endif
	return hImage;
}

#ifndef WIN32
#pragma warning(4:4035) // move "no return value" to level 4
int NEAR PASCAL GetDrive(void)
{
	int retval;

	_asm {
		mov ah, 19h
		int 21h
		cbw
		inc ax
		mov retval, ax
	}
	return retval;
}

int NEAR PASCAL SetDrive(int drive)
{
	int retval;

	_asm {
		mov dx, drive
		xor dh, dh
		dec dl
		mov ah, 0eh
		int 21h
		cbw
		mov retval, ax
	}
	return retval;
}
#endif // #ifndef WIN32

BOOL IsLicensed()
{
#ifdef DEMO
	return FALSE;
#else
	return TRUE;
#endif
}