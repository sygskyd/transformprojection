/////////////////////////////////////////////////////////////////////////////
//
// dxf.c
//
// AutoCad "Drawing Interchange File" DIL Module
//
// ImageMan Image Processing Library
//
// Developed for DTI by Chris Roueche, 1994.
// Copyright (c) 1994 Data Techniques, Inc.
// All Rights Reserved
//
/////////////////////////////////////////////////////////////////////////////
// $History: DXF.C $
// 
// *****************  Version 6  *****************
// User: Ericw        Date: 5/20/99    Time: 12:08p
// Updated in $/ImageMan 16-32/DILS
// Fixed bug #164.  We now check zero witdth/height images and will return
// "No height and/or width indicated" as an error string.
// 
// *****************  Version 5  *****************
// User: Timk         Date: 2/01/96    Time: 10:41a
// Updated in $/ImageMan 16-32/DILS
// Added some code to support debug dialog.
// 
// *****************  Version 4  *****************
// User: Timk         Date: 9/21/95    Time: 6:30p
// Updated in $/ImageMan 16-32/DILS
// Fixed to work with embedded images.
// 
// *****************  Version 3  *****************
// User: Timk         Date: 9/12/95    Time: 12:03p
// Updated in $/ImageMan 16-32/DILS
// Fixed lOrgWid and lOrgHi so they're set to proper values. This fixes
// problems w/cutting & pasting DXF's using the clipboard.
// 
// *****************  Version 2  *****************
// User: Timk         Date: 6/30/95    Time: 12:04p
// Updated in $/ImageMan 16-32/DILS
// Part 1 of massive changes for read/write using function pointer blocks.
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 1/19/95    Time: 9:46a
// Created in $/ImageMan 16-32/DILS
// ImageMan 4.0 Beta 1

#include "internal.h"
#pragma hdrstop
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "dxf2.h"
#include "dxf.h"


#define VERSION   ("AutoCAD\xAE DXF Loader v1.0" DXFBUILDSTR)
#define RBUFSIZE  1024

#define DPI2DPM(dpi)  ((ULONG)(dpi)*3937UL/100UL) // 100/2.54 = 39.37

#ifdef NDEBUG
#undef OutputDebugString
#define OutputDebugString(S) ((void)0)
#endif

typedef struct tagImageInfo {
	LPVOID		lpOpenInfo;			// info needed to open the image
	LPVOID		lpHandle;			// file handle needed for embedded images
	long        offset;			  // DXF starting location within image
	int         scale;			  // scaling factor from DXF to WMF space (also dpi)
	RECT        bbox;				  // WMF space bounding box
	LPIMG_IO		lpIO;
	LPDXFCONTEXT dxf;
}           IMAGEINFO, FAR * LPIMAGEINFO;

// I want to store both the error value and the line number it occured on.
// If I make the rash assumption that nobody's going to try to load a DXF
// with more than 2^24 (>16 million) lines, I can put the line number in
// the top 24 bits and the error in the bottom 8 bits, and fit it all into
// a DWORD. :-)
#ifdef WIN32

#ifdef WINGIS // so the file TLSByJHC.h was included
#define SETERROR(_E,_L) bJHCTLSSetValue( (DWORD)((DWORD)(_E)|((DWORD)(_L)<<8)) )
#define GETERROR    nJHCTLSGetValue()
#else
// Since we only need to store the error value on a per-thread basis, I'm
// just gonna place it in our DWORD TLS slot.
static DWORD TlsIndex;
#define SETERROR(_E,_L) TlsSetValue(TlsIndex,(LPVOID)((DWORD)(_E)|((DWORD)(_L)<<8)))
#define GETERROR      ((DWORD)TlsGetValue(TlsIndex))
#endif

#else
static DWORD nErrInfo;
#define SETERROR(_E,_L) nErrInfo = ((DWORD)(_E)|((DWORD)(_L)<<8))
#define GETERROR       (nErrInfo)
#endif

static HINSTANCE myInstance;
#if DEBUG
static DEBUGPROC DebugStringOut;
#endif


#ifdef WIN32
// /////////////////////////////////////////////////////////////////////////
//
// DllMain
//
// This is the Win32 'DllEntryPoint'. It is similar in function to LibMain
// and the WEP in 16-bit land, but is much more thorough. LibMain is
// called whenever a process or thread attaches or detaches to the
// BMP DIL
//
// Entry:
// hinstDLL  - handle to the ImageMan DLL
// fdwReason - notification code (attach/detach)
// DLL_PROCESS_ATTACH
// DLL_PROCESS_DETACH
// DLL_THREAD_ATTACH
// DLL_THREAD_DETACH
// lpvReserved - NULL during dynamic process attach/detach, non-NULL
// during static attach/detach.
// Exit:
// DLL_PROCESS_ATTACH - TRUE for success, FALSE for failure
// all others         - return code ignored
// /////////////////////////////////////////////////////////////////////////

BOOL APIENTRY DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
	switch (fdwReason) {
			// The DLL is attaching to a process, due to LoadLibrary.
			case DLL_PROCESS_ATTACH:
			assert(!lpvReserved);  // must always be loaded via LoadLibrary!

			OutputDebugString("IM2DXF: Process++\n");
			myInstance = hinstDLL;  // save our identity
#ifndef WINGIS
			// Allocate a "Thread Local Storage" (TLS) index right away.

			if ((TlsIndex = TlsAlloc()) == 0xFFFFFFFF)
				return (FALSE);
#else
			bJHCTLSClear();
#endif
			// Fall thru: Initialize the index for first thread.

			// The attached process created a new thread.
		case DLL_THREAD_ATTACH:
#ifndef WINGIS
			OutputDebugString("IM2DXF: Thread++\n");
			// Initialize the TLS index for this thread... it's actually 'nErrID'
			if (!TlsSetValue(TlsIndex, (LPVOID) 0))
				return (FALSE);
#else
			nJHCTLSAlloc(); // Stamp this thread to be existed
#endif
			break;

			// The thread of the attached process terminates.
		case DLL_THREAD_DETACH:
#ifdef WINGIS
			bJHCTLSFree(); // Free this thread entry
#else
			OutputDebugString("IM2DXF: Thread--\n");
#endif
			break;

			// The DLL is detaching from a process, due to a call to FreeLibrary.
		case DLL_PROCESS_DETACH:
#ifndef WINGIS
			OutputDebugString("IM2DXF: Process--\n");
			// Release the TLS index.
			TlsFree(TlsIndex);
#else
			bJHCTLSClear();  // Release all the threads error descriptors
#endif
			break;
	}
	OutputDebugString("IM2DXF: OK\n");
	return (TRUE);

	UNREFERENCED_PARAMETER(lpvReserved);
}

#else // WIN32

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int WINAPI  LibMain(HANDLE hInstance, WORD wDataSeg, WORD wHeapSize, LPSTR lpCmdLine)
{
	if (!wHeapSize)
		return (0);					  // no heap indicates fatal error

	myInstance = hInstance;

	return (1);
	UNREFERENCED_PARAMETER(wDataSeg);
	UNREFERENCED_PARAMETER(lpCmdLine);
}
#endif // WIN32

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
void WINAPI WEP(int bSystemExit)
{
	return;
	UNREFERENCED_PARAMETER(bSystemExit);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
LPSTR IMAPI IMDLget_ver(void)
{
	return ((LPSTR) (VERSION));
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLescape(HANDLE hImage, int nInCnt, LPVOID lpIn, LPVOID lpOut)
{
	return (IMG_NSUPPORT);
	UNREFERENCED_PARAMETER(hImage);
	UNREFERENCED_PARAMETER(nInCnt);
	UNREFERENCED_PARAMETER(lpIn);
	UNREFERENCED_PARAMETER(lpOut);
}

// /////////////////////////////////////////////////////////////////////////
//
// IMDLverify_img
//
// Examines the given buffer (100 bytes/VERIFYBUFSIZE) taken from front
// of 'fHand' for validity as the current decode type.
//
// Returns:
// TRUE if valid file type, otherwise FALSE.
//
// /////////////////////////////////////////////////////////////////////////

BOOL IMAPI  IMDLverify_img(LPSTR lpVerfBuf, LPVOID lpHandle, LPIMG_IO lpIO)
{
	return (dxf_recognize(lpVerfBuf));

	UNREFERENCED_PARAMETER(lpHandle);
	UNREFERENCED_PARAMETER(lpIO);
}

// /////////////////////////////////////////////////////////////////////////
//
// IMDLinit_image
//
// Initialize internal read-state for the new file.
//
// /////////////////////////////////////////////////////////////////////////

int IMAPI   IMDLinit_image(LPCSTR lpOpenInfo, LPHANDLE hFile, LPVOID lpHandle, long offset, long lLen, LPIMG_IO lpIO)
{
	LPIMAGEINFO ii;
	UCHAR       buf[32];

	// verify DXF format
	lpIO->seek(lpHandle, offset, SEEK_SET);
	if (lpIO->read(lpHandle, buf, sizeof(buf)) != sizeof(buf)) {
		SETERROR(IDS_BADREAD, 0);
		lpIO->close(lpHandle);
		return (IMG_FILE_ERR);
	}
	//lpIO->close(lpHandle);

	if (!dxf_recognize(buf)) {
		SETERROR(IDS_NOTADXF, 0);
		return (IMG_INV_FILE);
	}
	// create new internal structure
	if (!(*hFile = GlobalAlloc(DLLHND, sizeof(IMAGEINFO)))) {
		SETERROR(IDS_NOMEM, 0);
		return (IMG_NOMEM);
	}
	// fill in the structure
	if (ii = (LPIMAGEINFO) GlobalLock(*hFile)) {
		ii->lpOpenInfo = (LPVOID)lpOpenInfo;
		ii->offset = offset;
		ii->lpHandle = lpHandle;
		ii->dxf = NULL;
		ii->lpIO = lpIO;
		GlobalUnlock(*hFile);
		return (IMG_OK);
	}
	GlobalFree(*hFile);
	*hFile = NULL;
	SETERROR(IDS_NOMEM, 0);
	return (IMG_NOMEM);
}

// /////////////////////////////////////////////////////////////////////////
//
// IMDLset_ext
//
// Never called? What's this routine for?
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLset_ext(HGLOBAL hFile, LPSTR ext)
{
	return (IMG_NSUPPORT);
	UNREFERENCED_PARAMETER(hFile);
	UNREFERENCED_PARAMETER(ext);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLimg_name(HGLOBAL hInfo, HANDLE FAR * lpHand)
{
	// Since the file name is stored first in our info-handle, simply
	// return the info handle.
	*lpHand = hInfo;

	return (IMG_OK);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLopen(HGLOBAL hFile, LPVOID lpHandle)
{
	LPIMAGEINFO ii;

	if (!(ii = (LPIMAGEINFO) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND, 0);
		return (IMG_INV_HAND);
	}

	if (!ii->lpHandle) {
		if (ii->lpIO->open(ii->lpOpenInfo, OF_READ, &ii->lpHandle) != IMG_OK) {
			GlobalUnlock(hFile);
			SETERROR(IDS_BADREAD, 0);
			return IMG_NOFILE;
		}
	}

	ii->lpIO->seek(ii->lpHandle, ii->offset, SEEK_SET);	

	GlobalUnlock(hFile);

	return (IMG_OK);
	UNREFERENCED_PARAMETER(hFile);
	UNREFERENCED_PARAMETER(lpHandle);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLread_rows(HGLOBAL hInfo, LPSTR buf, UINT rows, UINT rowSize,
								              UINT start, LPSTATUS lpStat)
{
	return (IMG_NSUPPORT);
	UNREFERENCED_PARAMETER(hInfo);
	UNREFERENCED_PARAMETER(buf);
	UNREFERENCED_PARAMETER(rows);
	UNREFERENCED_PARAMETER(rowSize);
	UNREFERENCED_PARAMETER(start);
	UNREFERENCED_PARAMETER(lpStat);
}


// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLset_row(HGLOBAL hInfo, UINT row)
{
	return (IMG_NSUPPORT);
	UNREFERENCED_PARAMETER(hInfo);
	UNREFERENCED_PARAMETER(row);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLbuildDIBhead(HGLOBAL hFile, LPINTERNALINFO lpinfo)
{
	LPIMAGEINFO ii;
	LPBITMAPINFO lpbi;
	DXFRESULT   r;
	LPVOID		lpHandle;

	if (!(ii = (LPIMAGEINFO) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND, 0);
		return (IMG_INV_HAND);
	}

	// If we're an embedded image, we need to seek to the proper offset
	// in the file. Otherwise, we have to open the file ourselves.
	if (ii->offset) {
		ii->lpIO->seek(ii->lpHandle, ii->offset, SEEK_SET);
		lpHandle = ii->lpHandle;
	}
	else	if (ii->lpIO->open(ii->lpOpenInfo, OF_READ, &lpHandle) != IMG_OK) {
		SETERROR(IDS_BADREAD, 0);
		return (IMG_FILE_ERR);
	}
	if (!(ii->dxf = dxf_init(lpHandle, ii->lpIO))) {
		if (!ii->offset) ii->lpIO->close(lpHandle);
		SETERROR(IDS_NOMEM, 0);
		return (IMG_NOMEM);
	}
	r = dxf_read(ii->dxf);		  // only gather DXF information at this time

	if (!ii->offset) ii->lpIO->close(lpHandle);

	if (r != DXF_OK) {
		dxf_cleanup(ii->dxf);
		ii->dxf = NULL;
		GlobalUnlock(hFile);
		SETERROR(EDXF2IDS(r), dxf_lastline(ii->dxf));
#ifndef NDEBUG
		{
			char        mystr[64];
			IMDLerror_string(mystr, 64);
			OutputDebugString(mystr);
		}
#endif
		return (IMG_FILE_ERR);
	}
	ii->scale = dxf_getbbox(ii->dxf, &ii->bbox);

	// Now that i've read the DXF, I can generate information about
	// its size.
	if (!(lpbi = (LPBITMAPINFO) GlobalAllocPtr(DLLHND, sizeof(BITMAPINFOHEADER) + 256 * sizeof(RGBQUAD)))) {
		GlobalUnlock(hFile);
		SETERROR(IDS_NOMEM, 0);
		return (IMG_NOMEM);
	}
	// fill in the header with defaults for a metafile
	lpbi->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	lpbi->bmiHeader.biWidth = ii->bbox.right - ii->bbox.left;
	lpbi->bmiHeader.biHeight = ii->bbox.bottom - ii->bbox.top;
	if ( ( 0 == lpbi->bmiHeader.biWidth  ) ||
		   ( 0 == lpbi->bmiHeader.biHeight ) ) {
		GlobalFreePtr( lpbi ) ;
		lpbi = NULL ;
		GlobalUnlock( hFile ) ;
		SETERROR( IDS_BADHIWID, 0 ) ;
		return ( IMG_INV_FILE ) ;
	}
	lpbi->bmiHeader.biPlanes = 1;
	lpbi->bmiHeader.biBitCount = 8;	// DXF always 256 colors

	lpbi->bmiHeader.biCompression = BI_RGB;
	lpbi->bmiHeader.biSizeImage = 0;
	// I assume the DXF is in inches!
	lpbi->bmiHeader.biXPelsPerMeter
		= lpbi->bmiHeader.biYPelsPerMeter = (long) (ii->scale * 100.0 / 2.54);
	lpbi->bmiHeader.biClrUsed = 0;
	lpbi->bmiHeader.biClrImportant = 0;

	// provide the standard (and fixed) AutoCad color table
	dxf_fillrgb(lpbi->bmiColors);

	lpinfo->lpbi = lpbi;
	lpinfo->bFlags = IMG_PRNT_VECTOR | IMG_DISP_VECTOR;
	lpinfo->lOrgWid = lpbi->bmiHeader.biWidth;
	lpinfo->lOrgHi = lpbi->bmiHeader.biHeight;

	GlobalUnlock(hFile);
	return (IMG_OK);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLload_wmf(HGLOBAL hFile, HMETAFILE FAR * lphwmf, LPRECT lpbbox, LPIMG_IO lpIO)
{
	LPIMAGEINFO ii;

	if (!(ii = (LPIMAGEINFO) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND, 0);
		return (IMG_INV_HAND);
	}
	if (ii->dxf) {
		if (lpbbox)
			*lpbbox = ii->bbox;

		// DXF should have been read during the "buildDIB" stage. Now
		// convert the DXF drawing list into a WMF.
		if (lphwmf)
			*lphwmf = dxf_mkwmf(ii->dxf);
	}
	GlobalUnlock(hFile);
	return (IMG_OK);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLclose_keep(HANDLE hFile)
{
	LPIMAGEINFO ii;

	// remove internal structures
	if (!(ii = (LPIMAGEINFO) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND, 0);
		return (IMG_INV_HAND);
	}

	if (ii->lpHandle) {
		ii->lpIO->close(ii->lpHandle);
		ii->lpHandle = NULL;
	}

	GlobalUnlock(hFile);


	return (IMG_OK);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLclose_image(HGLOBAL hFile)
{
	LPIMAGEINFO ii;

	// remove internal structures
	if (!(ii = (LPIMAGEINFO) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND, 0);
		return (IMG_INV_HAND);
	}
	dxf_cleanup(ii->dxf);
	ii->dxf = NULL;

	if (ii->lpHandle) {
		ii->lpIO->close(ii->lpHandle);
		ii->lpHandle = NULL;
	}

	GlobalUnlock(hFile);
	GlobalFree(hFile);
	return (IMG_OK);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLprint(HGLOBAL hFile, HDC hPrnDC, LPRECT lpDest, LPRECT lpSrc)
{
	return (IMG_NOTAVAIL);
	UNREFERENCED_PARAMETER(hFile);
	UNREFERENCED_PARAMETER(hPrnDC);
	UNREFERENCED_PARAMETER(lpDest);
	UNREFERENCED_PARAMETER(lpSrc);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLrender(HGLOBAL hFile, HDC hDC, LPRECT lpDest, LPRECT lpSrc)
{
	return (IMG_ERR);
	UNREFERENCED_PARAMETER(hFile);
	UNREFERENCED_PARAMETER(hDC);
	UNREFERENCED_PARAMETER(lpDest);
	UNREFERENCED_PARAMETER(lpSrc);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLget_page(HANDLE hImage, LPINT nPage)
{
	return (IMG_ERR);
	UNREFERENCED_PARAMETER(hImage);
	UNREFERENCED_PARAMETER(nPage);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLset_page(HANDLE hImage, int nPage)
{
	return (IMG_ERR);
	UNREFERENCED_PARAMETER(hImage);
	UNREFERENCED_PARAMETER(nPage);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLerror_string(LPSTR lpszBuf, int nMaxLen)
{
	DWORD       err, line;
	char        str[64];

	err = GETERROR;
	line = err >> 8;
	err &= 0x00ff;
	LoadString(myInstance, (int) err, str, sizeof(str));
	if (line > 0)
		wsprintf(lpszBuf, "DXF: %lu: %s", line, str);
	else
		wsprintf(lpszBuf, "DXF: %s", str);

	return (IMG_OK);
}

#if DEBUG
void __cdecl FAR Debug(UINT wCategory, LPSTR lpFormat,...)
{
	va_list     vaArgs;

	if (DebugStringOut) {
		va_start(vaArgs, lpFormat);
		(*DebugStringOut) (wCategory, lpFormat, vaArgs);
		va_end(vaArgs);
	}
}
#else
void __cdecl FAR Debug(UINT wCategory, LPSTR lpFormat,...)
{
	UNREFERENCED_PARAMETER(wCategory);
	UNREFERENCED_PARAMETER(lpFormat);
}
#endif
