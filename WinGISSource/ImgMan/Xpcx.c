/////////////////////////////////////////////////////////////////////////////
//
// xpcx.c
//
// ImageMan PCX DEL
//
// Copyright (c) 1991-5 Data Techniques, Inc.
// All Rights Reserved
//
// ///////////////////////////////////////////////////////////////////////////
// $History: XPCX.C $
// 
// *****************  Version 4  *****************
// User: Erict        Date: 7/13/00    Time: 8:57a
// Updated in $/ImageMan 16-32/DELS
// Fixed problem caused by global OutputBuffer. If two apps were using the
// dll for writes at once, the output files would tromp on each other.
// Moved the buffer and the counter to the EXPORTINFO struct which is the
// same as it is in xdcx.c
// 
// *****************  Version 3  *****************
// User: Ericj        Date: 4/29/98    Time: 11:04a
// Updated in $/ImageMan 16-32/DELS
// Changed fname in LEXPORTINFO to MAX_PATH to export files
// longer than 100 characters.
// 
// *****************  Version 2  *****************
// User: Timk         Date: 7/25/95    Time: 10:55p
// Updated in $/ImageMan 16-32/DELS
// Added support for writing using IO pointer blocks.
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 1/18/95    Time: 5:11p
// Created in $/ImageMan 16-32/DELS
// ImageMan 4.0 Beta 1
//

#define WIN32_LEAN_AND_MEAN

#ifdef WIN32
#define IMAPI __cdecl
#else
#define IMAPI WINAPI
#endif

#define _WINDLL
#include <windows.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "internal.h"
#include "pcx.h"
#include "xpcx.h"


#ifndef WIN32

#include "windos.h"

#endif

#ifdef WIN32



#ifdef WINGIS // so the file TLSByJHC.h was included to "internal.h"

#define SETERROR(E) bJHCTLSSetValue((DWORD)(E))
#define GETERROR    nJHCTLSGetValue()

#else
// Since we only need to store the error value on a per-thread basis, I'm
// just gonna place it in our DWORD TLS slot.
static DWORD TlsIndex;
#define SETERROR(E) TlsSetValue(TlsIndex,(LPVOID)(DWORD)(E))
#define GETERROR    ((DWORD)TlsGetValue(TlsIndex))
#endif

#else

static int  nErrID;				  // id of latest error
#define SETERROR(E) nErrID = (E)
#define GETERROR    (nErrID)

#endif

#define OUTBUFSIZE 2048

typedef struct _EXPORTINFO {
	char        fName[MAX_PATH];		  // name of file to export
	LPVOID		lpHandle;
	LPIMG_IO		lpIO;
	int         nColors;			  // color info
	PCXHEADER   pcx;				  // pcx header info
	PCXPAL      pal[256];
	char        outBuf[OUTBUFSIZE];
	UINT        nOutCnt;
}           EXPORTINFO;

typedef EXPORTINFO FAR *LPEXPORTINFO;

int NEAR PASCAL round(double);

int         WriteRow(LPEXPORTINFO lpex, int nByteLine, HPSTR lpBits);
int         EncodePCXRun(LPEXPORTINFO lpex, char nData, char nRepeat);
//int         WriteRow(LPVOID lpHandle, LPIMG_IO lpIO, int nByteLine, HPSTR lpBits);
//int         EncodePCXRun(char nData, char nRepeat, LPVOID lpHandle, LPIMG_IO lpIO);
int         FillPCXHead(LPPCXHEADER pPCX, LPBITMAPINFO lpDIB, HANDLE hOptBlk);
void        set_error(int nIndex, LPCSTR lpFilename);
XERRORPROC  XSetError;

HANDLE      hInst;
//char        outBuf[OUTBUFSIZE]; // this causes problems when multiple 16 bit apps are writing 
//							at the same time, output buffers tromp on each other. moved to EXPORTINFO
//UINT        nOutCnt;


#ifdef WIN32
// /////////////////////////////////////////////////////////////////////////
//
// DllMain
//
// This is the Win32 'DllEntryPoint'. It is similar in function to LibMain
// and the WEP in 16-bit land, but is much more thorough. LibMain is
// called whenever a process or thread attaches or detaches to the
// BMP DIL
//
// Entry:
// hinstDLL  - handle to the ImageMan DLL
// fdwReason - notification code (attach/detach)
// DLL_PROCESS_ATTACH
// DLL_PROCESS_DETACH
// DLL_THREAD_ATTACH
// DLL_THREAD_DETACH
// lpvReserved - NULL during dynamic process attach/detach, non-NULL
// during static attach/detach.
// Exit:
// DLL_PROCESS_ATTACH - TRUE for success, FALSE for failure
// all others         - return code ignored
// /////////////////////////////////////////////////////////////////////////

BOOL APIENTRY DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
	switch (fdwReason) {

			// The DLL is attaching to a process, due to LoadLibrary.
			case DLL_PROCESS_ATTACH:
			assert(!lpvReserved);  // must always be loaded via LoadLibrary!

			hInst = hinstDLL;		  // save our identity
#ifndef WINGIS
			OutputDebugString("IM2XBMP: Process++\n");
			// Allocate a "Thread Local Storage" (TLS) index right away.

			if ((TlsIndex = TlsAlloc()) == 0xFFFFFFFF)
				return (FALSE);
#else
			bJHCTLSClear();
#endif
			// Fall thru: Initialize the index for first thread.
			// The attached process created a new thread.

		case DLL_THREAD_ATTACH:
#ifndef WINGIS
			OutputDebugString("IM2XBMP: Thread++\n");
			// Initialize the TLS index for this thread... it's actually 'nErrID'
			if (!TlsSetValue(TlsIndex, (LPVOID) 0))
				return (FALSE);
#else
			nJHCTLSAlloc(); // Stamp this thread to be existed
#endif
			break;

			// The thread of the attached process terminates.
		case DLL_THREAD_DETACH:
#ifdef WINGIS
			bJHCTLSFree(); // Free this thread entry
#endif
#ifdef DEBUG
			OutputDebugString("IM2XBMP: Thread--\n");
#endif			
			break;

			// The DLL is detaching from a process, due to a call to FreeLibrary.
		case DLL_PROCESS_DETACH:
#ifdef DEBUG
			OutputDebugString("IM2XBMP: Process--\n");
#endif			
#ifndef WINGIS
			// Release the TLS index.
			TlsFree(TlsIndex);
#else
			bJHCTLSClear();  // Release all the threads error descriptors
#endif
			break;
	}
#ifdef DEBUG
	OutputDebugString("IM2XBMP: OK\n");
#endif
	return (TRUE);

	UNREFERENCED_PARAMETER(lpvReserved);
}

#else // WIN32

int WINAPI  LibMain(hInstance, wDataSeg, wHeapSize, lpCmdLine)
	HANDLE      hInstance;
	WORD        wDataSeg, wHeapSize;
	LPSTR       lpCmdLine;
{
	if (wHeapSize == 0)			  /* no heap indicates fatal error */
		return 0;

	hInst = hInstance;			  // save instance handle

	return 1;
}

#endif

VOID WINAPI WEP(int bSystemExit)
{
	if (bSystemExit) {			  // if exiting Windows entirely

	}
}

LPSTR IMAPI IMXDLget_ext(LPXINITSTRUCT lpInit)
{
	lpInit->lFlags = XF_ALLCOLORS;
	XSetError = lpInit->SetError;

	return (LPSTR) "PCX;PC Paintbrush (*.pcx)";
}

LPSTR IMAPI IMXDLget_ver(void)
{
	return (LPSTR) "1.00";
}

HANDLE IMAPI IMXDLbegin_write(LPVOID lpOpenInfo, LPIMG_IO lpIO, LPCSTR lpszAlias, LPBITMAPINFO lpDIB, HANDLE hOptBlk, LPXINFO lpInfo)
{
	HANDLE      hImage = NULL;
	LPVOID		lpHandle;
	PCXHEADER   pcx;
	LPEXPORTINFO lpex;
	int         i, nColors;


	if (lpIO->open(lpOpenInfo, OF_CREATE, &lpHandle) == IMG_OK) {

		// fill in and write the pcx header info from the OptBlock
		FillPCXHead(&pcx, lpDIB, hOptBlk);

		// now create and initialize our lovely data structure
		hImage = GlobalAlloc(GHND, sizeof(EXPORTINFO));
		if (hImage) {
			lpex = (LPEXPORTINFO) GlobalLock(hImage);


			// now go after the palette info
			switch (lpDIB->bmiHeader.biBitCount) {
				case 4:
					nColors = 16;
					for (i = 0; i < 16; i++) {
						pcx.colmap[i][0] = lpDIB->bmiColors[i].rgbRed;
						pcx.colmap[i][1] = lpDIB->bmiColors[i].rgbGreen;
						pcx.colmap[i][2] = lpDIB->bmiColors[i].rgbBlue;
					}
					break;
				case 8:
					nColors = 256;
					for (i = 0; i < nColors; i++) {
						lpex->pal[i].red = lpDIB->bmiColors[i].rgbRed;
						lpex->pal[i].blue = lpDIB->bmiColors[i].rgbBlue;
						lpex->pal[i].green = lpDIB->bmiColors[i].rgbGreen;
					}
					break;
				default:
				case 1:
				case 24:
					nColors = 2;
					break;
			}

			lpex->nColors = nColors;
			lpex->pcx = pcx;
			lpex->lpHandle = lpHandle;
			lpex->lpIO = lpIO;
			_fstrcpy(lpex->fName, lpszAlias);

			if (lpIO->write(lpex->lpHandle, (LPSTR) & pcx, sizeof(PCXHEADER)) != sizeof(PCXHEADER))
				set_error(EXP_WRITE_HEAD, lpszAlias);

			GlobalUnlock(hImage);
		} else
			lpIO->close(lpHandle);

	} else {							  // unable to open the file

		hImage = NULL;
		set_error(EXP_NOOPEN, lpszAlias);
	}

	lpex->nOutCnt = 0;
	lpInfo->lFlags = XF_ALLCOLORS;
	return hImage;
}

int IMAPI   IMXDLwrite_block(HANDLE hImage, int nRows, int nRowSize, LPSTR lpBits, LPXINFO lpInfo)
{
	LPEXPORTINFO lpex;
	HPSTR       hpBits;
	HPSTR       hpMux;
	LPSTR       lpScratch;
	int         j, retval = IMGX_OK;
	char        r, g, b, i, c;
	HPSTR       lpR, lpG, lpB, lpI;
	char        val[256];

	val[0] = 0;
	val[8] = val[4] = val[2] = val[1] = 1;
	val[0x80] = val[0x40] = val[0x20] = val[0x10] = 2;
	val[0x88] = val[0x44] = val[0x22] = val[0x11] = 3;


	// DIBs are upside down, so we need to start from the end of the
	// lpBits block to output the first row first...

	hpBits = (HPSTR) lpBits + (long) (nRows - 1) * nRowSize;
	lpex = (LPEXPORTINFO) GlobalLock(hImage);

	lpScratch = GlobalAllocPtr(GHND, (long) (lpex->pcx.byteline * 4));

			while (nRows-- && (retval == IMGX_OK)) {

		lpInfo->lIncCnt += nRowSize;

		if (lpInfo->lIncCnt > lpInfo->lIncrement) {
			lpInfo->lProcessed += lpInfo->lIncCnt;
			if (lpInfo->lpfnStat)
				if( !((*lpInfo->lpfnStat) (0, (int) ((lpInfo->lProcessed * 100) / lpInfo->lTotalBytes), lpInfo->dwUserInfo) )) {
					GlobalFreePtr(lpScratch);
					GlobalUnlock(hImage);

					return IMGX_CANCELLED;
				}
			lpInfo->lIncCnt = 0;
		}
		switch (lpex->pcx.nplanes) {
			case 1:
				if (WriteRow(lpex, lpex->pcx.byteline, hpBits)) {
					set_error(EXP_WRITE, lpex->fName);
					retval = IMGX_FILE_ERR;
				}
				hpBits -= nRowSize;
				break;

			case 3:					  // 24-bit color only!!!
				//
				// here we have to break each DIB row into 3 separate rows of
				// red, green, and blue, then output each row separately.
				//

				lpR = lpScratch;
				lpG = lpR + lpex->pcx.byteline;
				lpB = lpG + lpex->pcx.byteline;
				hpMux = hpBits;

				while (hpMux < (hpBits + lpex->pcx.byteline * 3)) {
					*lpR++ = *hpMux++;
					*lpG++ = *hpMux++;
					*lpB++ = *hpMux++;
				}

				lpR = lpScratch;
				lpG = lpR + lpex->pcx.byteline;
				lpB = lpG + lpex->pcx.byteline;

				// now output each row separately...
				WriteRow(lpex, lpex->pcx.byteline, lpB);
				WriteRow(lpex,  lpex->pcx.byteline, lpG);
				if (WriteRow(lpex, lpex->pcx.byteline, lpR)) {
					set_error(EXP_WRITE, lpex->fName);
					retval = IMGX_FILE_ERR;
				}
				hpBits -= nRowSize;

				break;

			case 4:
				//
				// here we have to break each DIB row into 4 separate rows of
				// red, green, blue, and "intensity", then output each row.
				//
				lpR = lpScratch;
				lpG = lpR + lpex->pcx.byteline;
				lpB = lpG + lpex->pcx.byteline;
				lpI = lpB + lpex->pcx.byteline;
				hpMux = hpBits;

				while (hpMux < (hpBits + lpex->pcx.byteline * 4)) {
					r = g = b = i = 0;
					for (j = 0; j < 4; j++) {
						c = *hpMux++;
						r <<= 2;
						g <<= 2;
						b <<= 2;
						i <<= 2;
						r |= val[c & 0x11];
						g |= val[c & 0x22];
						b |= val[c & 0x44];
						i |= val[c & 0x88];
					}
					*lpR++ = r;
					*lpG++ = g;
					*lpB++ = b;
					*lpI++ = i;
				}

				lpR = lpScratch;
				lpG = lpR + lpex->pcx.byteline;
				lpB = lpG + lpex->pcx.byteline;
				lpI = lpB + lpex->pcx.byteline;

				//
				// now write each row separately...
				//
				WriteRow(lpex, lpex->pcx.byteline, lpR);
				WriteRow(lpex, lpex->pcx.byteline, lpG);
				WriteRow(lpex, lpex->pcx.byteline, lpB);
				if (WriteRow(lpex, lpex->pcx.byteline, lpI)) {
					retval = IMGX_FILE_ERR;
					set_error(EXP_WRITE, lpex->fName);
				}
				hpBits -= nRowSize;

				break;

			default:
				break;
		}
	}

	GlobalFreePtr(lpScratch);
	GlobalUnlock(hImage);

	return retval;
}

int IMAPI   IMXDLend_write(HANDLE hImage, LPXINFO lpInfo)
{
	LPEXPORTINFO lpex;
	char        i;
	int         retval = IMGX_OK;

	lpex = (LPEXPORTINFO) GlobalLock(hImage);

	// write any leftover stuff in outBuf
	if (lpex->nOutCnt) {
		if (lpex->lpIO->write(lpex->lpHandle, lpex->outBuf, lpex->nOutCnt) != lpex->nOutCnt) {
			set_error(EXP_WRITE, lpex->fName);
			retval = IMGX_FILE_ERR;
		}
	}
	// if we have a palette, we write it out now...
	if (lpex->nColors > 16) {
		i = 0x0c;
		lpex->lpIO->write(lpex->lpHandle, (LPSTR) & i, 1);
		if (lpex->lpIO->write(lpex->lpHandle, (LPSTR) lpex->pal, sizeof(PCXPAL) * 256) != sizeof(PCXPAL) * 256) {
			set_error(EXP_WRITE, lpex->fName);
			retval = IMGX_FILE_ERR;
		}
	}
	lpInfo->lWritten = lpex->lpIO->seek(lpex->lpHandle, 0, SEEK_END);
	lpex->lpIO->close(lpex->lpHandle);

	GlobalUnlock(hImage);
	GlobalFree(hImage);

	return retval;
}

// //////////////////////////////////////////////////////////////////////////
//
// Helper functions
//
// //////////////////////////////////////////////////////////////////////////

//int         WriteRow(LPVOID lpHandle, LPIMG_IO lpIO, int nByteLine, HPSTR lpBits)
int         WriteRow(LPEXPORTINFO lpex, int nByteLine, HPSTR lpBits)
{
	int         nCur;				  // current input byte
	int         nRepeat;			  // # of times current byte has repeated
	int         nPrev;			  // previous char
	int         nOutCount;		  // # of scan-line bytes output
	int         retval = 0;

	nPrev = *lpBits++;			  // initialize previous data

	nRepeat = 1;
	nOutCount = 1;

	while (nOutCount < nByteLine && !retval) {
		nCur = *lpBits++;			  // get next input char

		nOutCount++;

		if (nCur == nPrev) {
			nRepeat++;
			if (nRepeat == 0x3f) {  // max allowable repeat count?

				retval = EncodePCXRun(lpex, (char) nPrev, (char) nRepeat);
				nRepeat = 0;
			}
		} else {						  // end of repeating section

			if (nRepeat > 0)
				retval = EncodePCXRun(lpex, (char) nPrev, (char) nRepeat);

			nPrev = nCur;
			nRepeat = 1;
		}
	}

	// check for any remaining data
	if (nRepeat > 0 && !retval)
		retval = EncodePCXRun(lpex, (char) nPrev, (char) nRepeat);

	return retval;
}

//int         EncodePCXRun(char nData, char nRepeat, LPVOID lpHandle, LPIMG_IO lpex->lpIO)
int         EncodePCXRun(LPEXPORTINFO lpex, char nData, char nRepeat)
{
	int         retval = 0;

	if (((nData & 0xc0) == 0xc0) || nRepeat > 1) {
		nRepeat |= 0xc0;
		lpex->outBuf[lpex->nOutCnt++] = nRepeat;
	}
	lpex->outBuf[lpex->nOutCnt++] = nData;

	// now make sure we won't overflow buffer
	if (lpex->nOutCnt > (OUTBUFSIZE - 2)) {
		if (lpex->lpIO->write(lpex->lpHandle, lpex->outBuf, lpex->nOutCnt) != lpex->nOutCnt)
			retval = 1;
		lpex->nOutCnt = 0;
	}
	return retval;
}


int         FillPCXHead(LPPCXHEADER pPCX, LPBITMAPINFO lpDIB, HANDLE hOptBlk)
{

	pPCX->man = 0x0a;				  // always!!!

	pPCX->enc = 1;					  // PCX encoding scheme

	pPCX->win[0] = pPCX->win[1] = 0;
	pPCX->win[2] = (int) lpDIB->bmiHeader.biWidth - 1;
	pPCX->win[3] = (int) lpDIB->bmiHeader.biHeight - 1;
	pPCX->hrez = round(lpDIB->bmiHeader.biXPelsPerMeter / 39.37);
	pPCX->vrez = round(lpDIB->bmiHeader.biYPelsPerMeter / 39.37);
	pPCX->reserved = 0;
	pPCX->palinfo = 0x01;

	// now fill in the tricky ones...
	switch (lpDIB->bmiHeader.biBitCount) {
		case 1:
			pPCX->ver = 2;
			pPCX->bits = 1;
			pPCX->nplanes = 1;
			break;
		case 4:
			pPCX->ver = 2;
			pPCX->bits = 1;
			pPCX->nplanes = 4;
			break;
		case 8:
			pPCX->ver = 5;
			pPCX->bits = 8;
			pPCX->nplanes = 1;
			break;
		case 24:
			pPCX->ver = 5;
			pPCX->bits = 8;
			pPCX->nplanes = 3;
			break;
	}

	pPCX->byteline = (int) (((lpDIB->bmiHeader.biWidth * pPCX->bits) + 15) / 16 * 2);

	return 0;
}

void        set_error(int nIndex, LPCSTR lpFilename)
{
	XSETERROR(hInst, nIndex, "IM10XPCX.DEL", lpFilename);
}

int NEAR PASCAL  round(double val)
{

	if (val - (int) val < 0.5)
		return (int) val;
	else
		return (int) val + 1;
}
