/////////////////////////////////////////////////////////////////////////////
//
//    CDialogs.cpp
//
//    ImageMan Common Dialog Module
//
//    ImageMan Image Processing Library
//
//    Copyright (c) 1991-8 Data Techniques, Inc.
//    All Rights Reserved
//	 
/////////////////////////////////////////////////////////////////////////////
//$Header: /ImageMan 16-32/DLL - V 6.xx API/CDialogs.cpp 12    5/17/99 1:16p Ericw $History: cdialogs.cpp $

#include "internal.h"
#include "imgmanrc.h"
#include <windowsx.h>
#pragma hdrstop
#include <string.h>

#ifndef WIN32
extern int  img_status;
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <commdlg.h>
#include <cderr.h>				// dialog errs
#include <dlgs.h>               // more commdlg includes
#include <time.h>

#ifndef WIN32
#include <stdio.h>
#include <dos.h>
#include <direct.h>
#include "windos.h"
#else
#include <commctrl.h>
#endif

typedef struct tagOPENDIALOG {
	HANDLE   hImage ;
	int      iSaveWindowHeight ;
	int      iSaveWindowWidth  ;
	long	 lOptions ;
	int      iLoadPercent ;
	char	 szFile [ MAX_PATH ] ;
	BITMAPINFO bmiHeader ;
} OPENDIALOG, FAR * LPOPENDIALOG ;

#ifdef __cplusplus
extern "C" {
#endif
UINT CALLBACK LOADDS ImgOpenProc ( HWND hDlg, UINT message,
												 WPARAM wParam, LPARAM lParam ) ;

UINT CALLBACK LOADDS ImgSaveProc ( HWND hDlg, UINT message,
												 WPARAM wParam, LPARAM lParam ) ;
#ifdef __cplusplus
	}
#endif

extern HINSTANCE hInst ;


// /////////////////////////////////////////////////////////////////////
//
//  The first part of this module is solely devoted to the Open Dialog.
//
// /////////////////////////////////////////////////////////////////////

HANDLE IMAPI ImgOpenDialog ( LPOPENFILENAME ofnOpen, LONG lOptions )
{
	OPENDIALOG		OpenDialog ;
	HANDLE			hImage = NULL ;
	LPSTR 			lpFilters = NULL;

	_fmemset ( (LPOPENDIALOG)&OpenDialog, 0, sizeof(OPENDIALOG) ) ;
	ofnOpen->lStructSize = sizeof(OPENFILENAME);
	ofnOpen->hInstance = hInst ;
	ofnOpen->Flags |= OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST ;

	if( !ofnOpen->lpstrFilter ) {
		lpFilters = ImgGetExt();
		ofnOpen->lpstrFilter = lpFilters;
	}

	if ( lOptions | IMG_OFN_OPTIONS )			// any options
		{
			ofnOpen->Flags |= OFN_ENABLEHOOK | OFN_ENABLETEMPLATE ;
			OpenDialog.lOptions = lOptions ;
			ofnOpen->lCustData = (DWORD) (LPOPENDIALOG) & OpenDialog ;
#ifdef _WIN32
			ofnOpen->Flags |= OFN_EXPLORER ;
			ofnOpen->lpfnHook = (LPOFNHOOKPROC) ImgOpenProc ;
#else
#define OFN_NONETWORKBUTTON 0x00020000 
			ofnOpen->Flags |= OFN_NONETWORKBUTTON ;
			ofnOpen->lpfnHook = ImgOpenProc ;
#endif
			ofnOpen->lpTemplateName = "IMGFILEOPEN" ;
		}
	else
		{
			ofnOpen->lpTemplateName = NULL ;
			ofnOpen->lpfnHook = NULL ;
		}
	if ( GetOpenFileName( ofnOpen ) )
		{
			hImage = ImgOpenSolo( ofnOpen->lpstrFile, NULL ) ;
		}
	else
		{
			long cderr ;
			long err ;
			cderr = CommDlgExtendedError() ;
			switch ( cderr )
				{
					case FNERR_BUFFERTOOSMALL :
						err = IMG_BUFFTOOSMALL ;
						break;
					case 0 :
						err = IMG_DLGCANCEL ;
						break ;
					default :		// some other dlg error occurred
						err = IMG_DLGERROR ;
						break ;
				}
			 SetError( NULL, err, 0, NULL, hInst ) ;
			ofnOpen->lpTemplateName = NULL;

			if( lpFilters ) {
				GlobalFreePtr( lpFilters );
				ofnOpen->lpstrFilter = NULL;
			}
			return ( NULL ) ;
		}

	ofnOpen->lpTemplateName = NULL;

	if ( OpenDialog.hImage )		// possible preview image left open !
		ImgClose ( OpenDialog.hImage ) ;

	if( lpFilters ) {
		GlobalFreePtr( lpFilters );
		ofnOpen->lpstrFilter = NULL;
	}

	return ( hImage ) ;
}

void SetOpenWinSize ( HWND hDlgSave, LPOPENDIALOG lpOpen )
{
	BOOL bCheck ;
#ifdef _WIN32
	HWND hWnd = GetParent( hDlgSave ) ;
#else
	HWND hWnd = hDlgSave ;
#endif
	RECT clirect ;
	int  iHeight = 15, iWidth ;

  if ( ! lpOpen )
    return ;

	GetWindowRect ( hWnd, & clirect ) ;
	bCheck = IsDlgButtonChecked( hDlgSave, IDC_SHOWSTATS) ;
	if ( ( bCheck ) || ( lpOpen->lOptions & IMG_OFN_SHOWSTATS ) )
		iHeight = 145 ;
	else
		if ( (( lpOpen->lOptions & IMG_OFN_ALLOWSTATSOPT ) &&
			    ( ! ( bCheck ) )) ||
				 ( lpOpen->lOptions & IMG_OFN_ALLOWTHUMBOPT ) )
			iHeight =  35 ;
		else
			iHeight = 15 ;

	bCheck = IsDlgButtonChecked( hDlgSave, IDC_SHOWPREVIEW ) ;
	if ( ( bCheck ) || ( lpOpen->lOptions & IMG_OFN_SHOWTHUMB ) )
		iHeight = 145 ;

#ifdef _WIN32
	iHeight += lpOpen->iSaveWindowHeight ;
#else
	iHeight += 315 ;
#endif

	iWidth = lpOpen->iSaveWindowWidth ;

	MoveWindow ( hWnd, clirect.left, clirect.top, iWidth, iHeight, TRUE ) ;
	clirect.bottom = iHeight ;
	clirect.right = iWidth ;
// we only do a move on hDlgSave if it's 32 bit because in 16 bit
// hDlgSave IS our window and the previous MoveWindow already did it.
#ifdef _WIN32
	if ( ! lpOpen->lOptions )
		iHeight = 1 ;
	MoveWindow ( hDlgSave, 0, 0, iWidth, iHeight, TRUE ) ;
#endif
}

#ifndef _WIN32
char FAR * FormatDateTime( UINT uTime, UINT uDate, char FAR * OutBuf )
{
	char * mos[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" } ;

	wsprintf ( OutBuf, "%02d:%02d:%02d %s %02d, %4d",
				( uTime & 0xF800 ) >> 11, ( uTime & 0x07E0 ) >> 5, ( uTime & 0x001F ),  // HH:MM:SS
				(char FAR*)mos[((uDate&0x01F0)>>5)-1], ( uDate & 0x001F ), (( uDate & 0xFE00 ) >> 9 ) + 1980 ) ;
	return ( OutBuf ) ;
}
#endif

BOOL AllowOpen ( HWND hDlg, LPOPENDIALOG lpOpen ) 
{
	long lOptions = lpOpen->lOptions ;
	BOOL bAllow = FALSE ;
	BOOL bStatCheck ;
	BOOL bPrevCheck ;
#ifdef _WIN32
	HWND hWnd = GetParent( hDlg ) ;
#else
	HWND hWnd = hDlg ;
#endif

	bStatCheck = IsDlgButtonChecked ( hDlg, IDC_SHOWSTATS   ) ;
	bPrevCheck = IsDlgButtonChecked ( hDlg, IDC_SHOWPREVIEW ) ;
	if ( bStatCheck || bPrevCheck )
		return TRUE ;

	if ( ( !(lOptions & IMG_OFN_ALLOWSTATSOPT) ) &&
			 ( ( lOptions & IMG_OFN_SHOWSTATS )    ) )
		return TRUE ;

	if ( ( !(lOptions & IMG_OFN_ALLOWTHUMBOPT) ) &&
			 ( ( lOptions & IMG_OFN_SHOWTHUMB )    ) )
		return TRUE ;

	return FALSE ;
}

void CheckShowFileStatsPreview ( HWND hDlg, LPSTR szFile, LPOPENDIALOG lpOpen, BOOL bForceOpen )
{
	long   lImgColors, lDispColors ;
	int    iFlags ;
	int    iRes   ;
	HWND   hWnd   ;
	HDC		 hPreviewDC ;
	HPALETTE hPal ;
	RECT   rect, drawrect ;
	HANDLE hImage ;
	LPBITMAPINFO lpbi ;
	char szTemp[ MAX_PATH ] ;
	LPSTR szMsg ;
	float xDiv, yDiv;
	LONG  lHeight, lWidth ;
	HBRUSH hBrush ;
	HCURSOR hCursor ;
	struct stat filestats ;
	BOOL	bNewImg ;
	BOOL	bErrImg = FALSE ;
	BOOL  bIsDir = FALSE ;

	if ( ! lpOpen )
		return ;
	bNewImg = (_fstricmp( lpOpen->szFile, szFile )	!= 0 ) || bForceOpen ;

	if ( bNewImg )
		{
			if ( lpOpen->hImage )
				ImgClose ( lpOpen->hImage ) ;					// close the old
			_fstrcpy ( lpOpen->szFile, szFile ) ;		// copy the new
			if ( szFile[0] )
				{
					if ( ! ( hImage = ImgOpenSolo(szFile,NULL) ) )
						{
							ImgErrString( szTemp, sizeof(szTemp) ) ;
							if ( (szMsg=_fstrchr ( szTemp, ';' )) != NULL )
								_fstrcpy( szTemp, ++szMsg ) ;
							bErrImg = TRUE ;
							hImage = NULL ;
						}
					else
						szTemp[0] = '\0' ;
				}
			else
				{
					bErrImg = TRUE ;
					szTemp[0] = '\0' ;
				}
			SetDlgItemText ( hDlg, IDC_IMGERRSTRING, szTemp ) ;

			lpOpen->hImage = hImage ;								// save the new
			lpbi = bErrImg ? NULL : ImgGetInfo ( hImage, & iFlags ) ;
			if ( ! bErrImg )
				lpOpen->bmiHeader = * lpbi ;
		}
	else
		if ( !(hImage = lpOpen->hImage) )
			return ;

	if ( ! AllowOpen ( hDlg, lpOpen ) )
		return ;

	hWnd = GetDlgItem ( hDlg, IDC_PREVIEWIMAGE ) ;
	hPreviewDC = GetDC ( hWnd ) ;

	lpbi = & lpOpen->bmiHeader ;

	if ( ! bErrImg ) {
		lDispColors = 1L << (min(24,(GetDeviceCaps( hPreviewDC, BITSPIXEL ) * GetDeviceCaps( hPreviewDC, PLANES )) )) ;
		lImgColors = 1L << lpbi->bmiHeader.biBitCount ;
	}
	// first we'll do the file statistics
	SetDlgItemText ( hDlg, IDC_STATNAMEVALUE, szFile ) ;

	if ( ( ! bErrImg ) && ( szFile[0] ) )
		wsprintf ( szTemp, "%ld(w) x %ld(h)", 
		  	       lWidth = lpbi->bmiHeader.biWidth, lHeight = lpbi->bmiHeader.biHeight ) ;
	else
		szTemp[0] = '\0' ;
	SetDlgItemText ( hDlg, IDC_STATDIMENSIONSVALUE, szTemp ) ;

	if ( ( ! bErrImg ) && ( szFile[0] ) )
		{
			_fstrncpy ( szTemp, &szFile[_fstrlen(szFile)-3], 3 ) ;
			szTemp[3] = '\0' ;
		}
	SetDlgItemText ( hDlg, IDC_STATTYPEVALUE, szTemp ) ;

	if ( ( ! bErrImg ) && ( szFile[0] ) )
		wsprintf ( szTemp, "%d-bit", lpbi->bmiHeader.biBitCount ) ;
	SetDlgItemText ( hDlg, IDC_STATCOLORSVALUE, szTemp ) ;

	#ifdef _WIN32
		DWORD dAttr = GetFileAttributes ( szFile ) ;
		if ( ( dAttr & FILE_ATTRIBUTE_DIRECTORY ) == 0 )
			{
				stat ( szFile, &filestats ) ;
				wsprintf ( szTemp, "%s", ctime(&filestats.st_mtime) );
			}
		else
			{
				bIsDir = TRUE ;
				szTemp[0] = '\0' ;
			}
		SetDlgItemText ( hDlg, IDC_STATDATEVALUE, szTemp ) ;
	#else
			{
				char tmp[ 100 ] ;
				struct mystat {
					char res [21] ;
					char attrib ;
					unsigned int uTime ;
					unsigned int uDate ;
					long size ;
					char name[13];
				} mystat ;
				_fstrcpy ( szTemp, szFile ) ;
				_fdos_findfirst( szTemp, _A_NORMAL,(struct find_t *)&mystat ) ;
				if ( mystat.attrib & _A_SUBDIR )
					{
						szTemp[0] = '\0' ;
						bIsDir = TRUE ;
					}
				else
					{
						filestats.st_size = mystat.size ;
						wsprintf ( szTemp, "%s", FormatDateTime( mystat.uTime, mystat.uDate, tmp) );
					}
				SetDlgItemText ( hDlg, IDC_STATDATEVALUE, szTemp ) ;
			}
	#endif

	if ( ! bIsDir )
		wsprintf ( szTemp, "%ld (bytes)", filestats.st_size ) ;
	else
		szTemp[0] = '\0' ;
	SetDlgItemText ( hDlg, IDC_STATSIZEVALUE, szTemp ) ;

	if ( ! IsWindowVisible(hWnd) )
		return ;

	// erase what was already being thumb'ed
	GetClientRect ( hWnd, & rect );
	rect.right = rect.bottom = 110 ;

	// figure out the scaled sizes now
	drawrect.top = drawrect.left = 0 ;
	xDiv = (float) rect.right  / lWidth  ;
	yDiv = (float) rect.bottom / lHeight ;
	if( yDiv < xDiv ) {		// Fit Height 
 		drawrect.right = (int) (lWidth * yDiv) ;
		drawrect.bottom = rect.bottom ;
	} else {						// Fit Width
		drawrect.right = rect.right ;
		drawrect.bottom = (int)(lHeight * xDiv) ;
	}

	// If the image is smaller than the thumbnail then show it actual size
	if( drawrect.right > lWidth && drawrect.bottom > lHeight ) {
		drawrect.right = lWidth;
		drawrect.bottom = lHeight;
	}

	// Center the Image in the thumbnail Window
	int left, top;

	top = (rect.bottom - drawrect.bottom)  / 2;
	left = (rect.right - drawrect.right)  / 2;

	drawrect.left += left;
	drawrect.right += left;
	drawrect.top += top;
	drawrect.bottom += top;
	

	// first scale it down to the preview
	drawrect.right-- ;  drawrect.bottom -- ;
	hCursor = SetCursor( LoadCursor(NULL, IDC_WAIT) ) ;

	if ( ( ! ( iFlags & IMG_DISP_VECTOR ) ) && ( ! bErrImg ) )
		{
			if ( bNewImg )
				{
					lpOpen->hImage = ImgCopy ( hImage, drawrect.right-drawrect.left-1, drawrect.bottom-drawrect.top-1, NULL, COPY_INTERPOLATE|COPY_ANTIALIAS ) ;

					ImgClose ( hImage ) ;
					// reduce to display colors if need be
					if ( lpOpen->hImage && lImgColors > lDispColors )
						{
							if ( lDispColors <= 256 )
								{
									hImage = ImgReduceColors ( lpOpen->hImage, lDispColors, IMG_FIXEDPALETTE | IMG_FLOYD ) ;
									// close the old one and set the reduced one to the new one.
									ImgClose ( lpOpen->hImage ) ;
									lpOpen->hImage = hImage ;
								}
						}
				}
		}
	if ( hPal = (HPALETTE) ImgGetPalette( lpOpen->hImage ) )
		{
			SelectPalette ( hPreviewDC, hPal,  FALSE ) ;
			RealizePalette ( hPreviewDC ) ;
		}

	if ( bNewImg )
		{
			hBrush = (HBRUSH)GetStockObject( LTGRAY_BRUSH ) ;
			rect.right++ ;  rect.bottom++ ;
			FillRect ( hPreviewDC, &rect, hBrush ) ;
			DeleteObject ( hBrush ) ;
		}

	if ( lpOpen->hImage && !bErrImg ) 
		iRes = ImgDrawImage ( lpOpen->hImage, hPreviewDC, &drawrect, NULL ) ;

	ReleaseDC ( hWnd, hPreviewDC ) ;

	if( hPal )
		DeleteObject ( hPal ) ;

	SetCursor( hCursor ) ;
}
	
void SetControls ( HWND hDlg, long lOptions )
{
	HWND hWnd ;
	int  iShowHide ;
	BOOL bEnable ;
	int  nId = 0 ;

	if ( ! lOptions )
		return ;

	hWnd = GetDlgItem ( hDlg, IDC_SHOWSTATS ) ;
	if ( lOptions & IMG_OFN_ALLOWSTATSOPT )
		{
			ShowWindow ( hWnd, SW_SHOW ) ;
			EnableWindow ( hWnd, TRUE ) ;
		}
	else
		{
			ShowWindow ( hWnd, SW_HIDE ) ;
			EnableWindow ( hWnd, FALSE ) ;
		}
	if ( (( lOptions & IMG_OFN_ALLOWSTATSOPT ) &&
				( IsDlgButtonChecked( hDlg, IDC_SHOWSTATS) )) ||
			 ( lOptions & IMG_OFN_SHOWSTATS ) )
		iShowHide = SW_SHOW ;
	else
		if ( lOptions & IMG_OFN_SHOWSTATS )
			iShowHide = SW_SHOW ;
		else
			iShowHide = SW_HIDE ;
	bEnable = TRUE ;
	if ( ( lOptions & IMG_OFN_SHOWSTATS ) &&
			 (( lOptions & IMG_OFN_ALLOWSTATSOPT    ) &&
				( !(IsDlgButtonChecked( hDlg, IDC_SHOWSTATS)) ))  )
		bEnable = FALSE ;
	for ( nId = IDC_STATNAME ; nId <= IDC_STATDATEVALUE; nId++ )
		{
			ShowWindow( hWnd=GetDlgItem(hDlg, nId), iShowHide ) ;
			EnableWindow( hWnd, bEnable ) ;
		}

	hWnd = GetDlgItem ( hDlg, IDC_SHOWPREVIEW ) ;
	if ( lOptions & IMG_OFN_ALLOWTHUMBOPT )
		{
			ShowWindow ( hWnd, SW_SHOW ) ;
			EnableWindow ( hWnd, TRUE ) ;
		}
	else
		{
			ShowWindow ( hWnd, SW_HIDE ) ;
			EnableWindow ( hWnd, FALSE ) ;
		}
	if ( (( lOptions & IMG_OFN_ALLOWTHUMBOPT   ) &&
				( IsDlgButtonChecked( hDlg, IDC_SHOWPREVIEW) )) ||
			 ((( lOptions & IMG_OFN_ALLOWTHUMBOPT ) == 0 ) &&
				( ( lOptions & IMG_OFN_SHOWTHUMB    )      ))  )
		iShowHide = SW_SHOW ;
	else
		if ( lOptions & IMG_OFN_SHOWTHUMB )
			iShowHide = SW_SHOW ;
		else
			iShowHide = SW_HIDE ;

	hWnd = GetDlgItem ( hDlg, IDC_PREVIEWIMAGE ) ;
	if ( iShowHide == SW_SHOW )
		{
			ShowWindow ( hWnd, SW_SHOW ) ;
			EnableWindow ( hWnd, TRUE ) ;
		}
	else
		ShowWindow ( hWnd, SW_HIDE ) ;
}

BOOL CleanTheFileName ( LPSTR szFile )
{
	int ixx ;
	BOOL bDirCh = FALSE ;
#ifdef _WIN32
	DWORD dAttr = GetFileAttributes( szFile ) ;
#else
	unsigned dAttr ;
	struct mystat {
		char res [21] ;
		char attrib ;
		unsigned int uTime ;
		unsigned int uDate ;
		long size ;
		char name[13];
	} mystat ;
#endif
	
#ifdef _WIN32
	if ( dAttr != 0xFFFFFFFF )
		{
			if ( dAttr & FILE_ATTRIBUTE_DIRECTORY )
				{
					szFile[0] = '\0' ;
					return ( TRUE ) ;
				}
		}
	else
		szFile[0] = '\0' ;
#else
	if ( _fdos_findfirst( szFile, _A_NORMAL,(struct find_t *)&mystat ) == 0 ) // success
		{
			if ( dAttr & _A_SUBDIR )
				{
					szFile[0] = '\0' ;
					return ( TRUE ) ;
				}
		}
	else
		szFile[0] = '\0' ;
#endif
	ixx = _fstrlen(szFile) - 1 ;
	if ( ixx >= 0 )
		{
			bDirCh = ( ( szFile[ixx] == '\\' ) ||
								 ( szFile[ixx] == ':'  ) ||
								 ( szFile[ixx] == '?'  ) ||
								 ( szFile[ixx] == '*'  ) ) ;
			if ( bDirCh )
				szFile[0] = '\0' ;
		}
	if ( ( _fstrchr( (LPSTR)szFile, '*' ) ) ||  // i.e. there is one
	     ( _fstrchr( (LPSTR)szFile, '?' ) )	)   // i.e. there is one
		{
			szFile[0] = '\0' ;
			bDirCh = TRUE ;
		}
	return bDirCh ;
}

UINT CALLBACK LOADDS ImgOpenProc ( HWND hDlg, UINT message,
												 WPARAM wParam, LPARAM lParam )
{
	LPOPENDIALOG   lpOpen = NULL ;
        LPOPENFILENAME lpOFN  = NULL ;
#ifdef _WIN32
	LPOFNOTIFY     lpNotify ;
#endif
  char szFile[ 255 ] ;
	BOOL bSetOptions = FALSE ;
	BOOL bProcessed = FALSE ;
	BOOL bNoSetProcessed = FALSE ;
	BOOL bEnable = TRUE ;
	BOOL bForceOpen = FALSE ;
	BOOL bDirCh = FALSE ;
	long lOptions ;
	HWND hWnd ;

	if ( lpOFN = (LPOPENFILENAME)GetWindowLong( hDlg, DWL_USER) )
		if ( lpOpen = (LPOPENDIALOG)lpOFN->lCustData )
			lOptions = lpOpen->lOptions ;
	szFile[0] = '\0' ;
	#ifdef _WIN32
		CommDlg_OpenSave_GetSpec(GetParent(hDlg), szFile, sizeof(szFile) ) ;
	#else
		GetDlgItemText( hDlg, edt1, szFile, sizeof(szFile)-1 ) ;
	#endif
		bDirCh = CleanTheFileName ( szFile ) ;
	switch ( message )
		{
			case WM_SETFOCUS :
				if ( wParam != (WPARAM)hDlg )
					{
						if ( szFile[0] )
							CheckShowFileStatsPreview ( hDlg, szFile, lpOpen, bForceOpen ) ;
					}
				break;

			case WM_SIZE :
				SetOpenWinSize ( hDlg, lpOpen ) ;
				bSetOptions = TRUE ;
				bProcessed = TRUE ;
				break ;

			case WM_QUERYNEWPALETTE :
				bSetOptions = TRUE ;
				bProcessed = TRUE ;
				return 0 ;
				break ;

			case WM_PALETTECHANGED :
				if ( wParam != (WPARAM)hDlg )
					{
						if ( szFile[0] )
							CheckShowFileStatsPreview ( hDlg, szFile, lpOpen, bForceOpen ) ;
						bSetOptions = TRUE ;
						bProcessed = TRUE ;
					}
				return 0 ;
				break ;

			case WM_INITDIALOG :
				{
					RECT saverect ;
					SetWindowLong ( hDlg, DWL_USER, lParam ) ;
					if ( lpOFN = (LPOPENFILENAME) lParam )
						if ( lpOpen = (LPOPENDIALOG) lpOFN->lCustData )
							lOptions = lpOpen->lOptions ;
#ifdef _WIN32
					GetWindowRect ( GetParent(hDlg), & saverect ) ;
#else
          GetWindowRect ( hDlg, & saverect ) ;
					#define BST_CHECKED 1
#endif
          lpOpen->iSaveWindowHeight = (UINT)(saverect.bottom - saverect.top) ;
					lpOpen->iSaveWindowWidth  = (UINT)(saverect.right  - saverect.left) ;

					if ( ( lOptions & IMG_OFN_ALLOWSTATSOPT ) &&
						   ( lOptions & IMG_OFN_SHOWSTATS     ) )
						CheckDlgButton ( hDlg, IDC_SHOWSTATS, BST_CHECKED ) ;

					if ( ( lOptions & IMG_OFN_ALLOWTHUMBOPT ) &&
						   ( lOptions & IMG_OFN_SHOWTHUMB     ) )
						CheckDlgButton ( hDlg, IDC_SHOWPREVIEW, BST_CHECKED ) ;
					bSetOptions = TRUE ;
				}
				break ;

			case WM_DESTROY :
				if ( lpOpen->hImage )
					{
						ImgClose ( lpOpen->hImage ) ;
						lpOpen->hImage = NULL ;
					}
				break ;

#ifdef _WIN32
			case WM_NOTIFY :
				lpNotify = (LPOFNOTIFY) lParam ;
				lpOFN = lpNotify->lpOFN ;
				switch ( lpNotify->hdr.idFrom )
					{
						case lst1 :
							if ( lpNotify->hdr.code == NM_DBLCLK )
								CommDlg_OpenSave_GetSpec(GetParent(hDlg), szFile, sizeof(szFile) ) ;
							break ;
						default :
							break ;
					}
				switch ( lpNotify->hdr.code )
					{
						case CDN_FOLDERCHANGE :
							CommDlg_OpenSave_GetSpec(GetParent(hDlg), szFile, sizeof(szFile) ) ;
							bDirCh = TRUE ;
							szFile[0] = '\0' ;
							// follow through on purpose !!
						case CDN_SELCHANGE :
							CheckShowFileStatsPreview ( hDlg, szFile, lpOpen, bForceOpen ) ;
							if ( bDirCh )
								return 0 ;
							break ;

						case CDN_FILEOK :
//							_fstrcpy ( lpOFN->lpstrFile, lpNotify->pszFile ) ;
							return 0 ;
							break ;

						default :
							break ;
					}
				break ;
#endif
			case WM_DRAWITEM :
				{
					LPDRAWITEMSTRUCT pdis = (LPDRAWITEMSTRUCT) lParam ;
					if ( pdis->itemAction & ODA_DRAWENTIRE )
						{
							if ( pdis->CtlID == IDC_PREVIEWIMAGE )
								{
										szFile[0] = '\0' ;
									#ifdef _WIN32
										CommDlg_OpenSave_GetSpec(GetParent(hDlg), szFile, sizeof(szFile) ) ;
									#else
										GetDlgItemText( hDlg, edt1, szFile, sizeof(szFile)-1 ) ;
									#endif
									bDirCh = CleanTheFileName ( szFile ) ;
//									if ( szFile[0] )
										CheckShowFileStatsPreview ( hDlg, szFile, lpOpen, bForceOpen ) ;
								}
						}
					return TRUE ;
				}

			case WM_COMMAND :
				bSetOptions = TRUE ;
				szFile[0] = '\0' ;
				#ifdef _WIN32
					CommDlg_OpenSave_GetSpec(GetParent(hDlg), szFile, sizeof(szFile) ) ;
				#else
					GetDlgItemText( hDlg, edt1, szFile, sizeof(szFile)-1 ) ;
				#endif
				bDirCh = CleanTheFileName ( szFile ) ;

				switch ( LOWORD(wParam) )
					{
#ifndef _WIN32
						case IDOK :
							{
								FILE * fimg ;
								char *szValue = (char *) LocalAlloc ( LMEM_FIXED, MAX_PATH);
								
								GetDlgItemText( hDlg, stc1, szValue, sizeof(szValue)-1 ) ;
								_fstrcpy ( lpOFN->lpstrFile, (LPSTR)szValue ) ;
								GetDlgItemText( hDlg, edt1, szValue, sizeof(szValue)-1 ) ;
								_fstrcat ( lpOFN->lpstrFile, (LPSTR)szValue ) ;
								_fstrcpy ( szValue, lpOFN->lpstrFile ) ;
								if ( (fimg=fopen( szValue, "r+b")) )		// file exists
									{
										fclose ( fimg ) ;
										EndDialog( hDlg, TRUE );
									}
								else
									{
										bNoSetProcessed = TRUE ;
									}    
								LocalFree( (HANDLE)szValue);									
							}
							break ;
							//return ( TRUE ) ;

						case IDCANCEL :
							EndDialog( hDlg, FALSE );
							return ( TRUE ) ;
							break ;

						case edt1 :		//  filename edit box
//							if ( HIWORD(lParam) != EN_KILLFOCUS )
//								return 0 ;		
							GetDlgItemText( hDlg, edt1, szFile, sizeof(szFile)-1 ) ;
							bDirCh = CleanTheFileName ( szFile ) ;
							bSetOptions = TRUE ;
							break ;

						case lst1 :		//  file list box
							{
								int iIndex, iLength ;
								HWND hList ;
								if ( HIWORD(lParam) != LBN_SELCHANGE )
									return 0 ;
								szFile[0] = '\0' ;
//								GetDlgItemText( hDlg, edt1, szFile, sizeof(szFile)-1 ) ;
								hList = GetDlgItem( hDlg, lst1 ) ;
								iIndex = SendMessage ( hList, LB_GETCURSEL, 0, 0 );
								iLength = SendMessage( hList, LB_GETTEXT, iIndex, (LONG)szFile ) ;
								bNoSetProcessed = TRUE ;
								bSetOptions = TRUE ;
							}
							break ; 

						case cmb1 :		//  file types box
						case lst2 :		//  dir list box
						case cmb2 :		//  drive list drop down
							bNoSetProcessed = TRUE ;
							break ;
#endif
						case IDC_SHOWSTATS   :
							bSetOptions = TRUE ;
							break ;
						case IDC_SHOWPREVIEW :
							bForceOpen = IsDlgButtonChecked( hDlg, IDC_SHOWPREVIEW ) ;
							bSetOptions = TRUE ;
							break ;

						case IDC_PREVIEWIMAGE :
							switch ( HIWORD(lParam) )
								{
									default :
										break ;
									case BN_CLICKED :
										#ifdef _WIN32
											CommDlg_OpenSave_GetSpec(GetParent(hDlg), szFile, sizeof(szFile) ) ;
											hWnd = GetDlgItem( GetParent(hDlg), IDOK ) ;
										#else
											GetDlgItemText( hDlg, edt1, szFile, sizeof(szFile)-1 ) ;
											hWnd = GetDlgItem ( hDlg, IDOK ) ;
										#endif
										bDirCh = CleanTheFileName( szFile ) ;
										_fstrcpy( lpOFN->lpstrFile, szFile ) ;
										// fake out clicking on the OK button
										// SendMessage ( GetParent(hDlg), BN_CLICKED, IDOK, (LPARAM)hWnd ) ;
										SendMessage ( hWnd, WM_LBUTTONDOWN, 1, 0 ) ;
										SendMessage ( hWnd, WM_LBUTTONUP,   1, 0 ) ;
										break ;
								}
							return TRUE ;
					}
				if ( ! bNoSetProcessed )
					bProcessed = TRUE ;
				break ;
		}
	if ( bSetOptions )
		{
			if ( _fstrchr( (LPSTR)szFile, '*' ) )		// i.e. there is one
				szFile[0] = '\0' ;
			if ( message != WM_INITDIALOG )
				SetOpenWinSize ( hDlg, lpOpen ) ;
			SetControls ( hDlg, lOptions ) ;
			if ( szFile[0] )
				CheckShowFileStatsPreview ( hDlg, szFile, lpOpen, bForceOpen ) ;
		}
	return ( bProcessed ) ;
}

// /////////////////////////////////////////////////////////////////////
//
//  The remaining portion is solely devoted to the Save Dialog.
//
// /////////////////////////////////////////////////////////////////////

#define IMG_TIF 1
#define IMG_GIF 2
#define IMG_PNG 3
#define IMG_DCX 4
#define IMG_JPG 5
#define IMG_FMF 6

int LOADDS CheckSetFileType ( LPSTR szFileType )
{
	if ( ! szFileType )
		return 0 ;
	if ( ! szFileType[0] )
		return ( 0 ) ;

  if ( ! (_fstricmp(szFileType,(LPSTR)"tif")) )
		return IMG_TIF ;
  if ( ! (_fstricmp(szFileType,(LPSTR)"gif")) )
		return IMG_GIF ;
  if ( ! (_fstricmp(szFileType,(LPSTR)"fmf")) )
		return IMG_FMF ;
  if ( ! (_fstricmp(szFileType,(LPSTR)"png")) )
		return IMG_PNG ;
  if ( ! (_fstricmp(szFileType,(LPSTR)"dcx")) )
		return IMG_DCX ;
  if ( ! (_fstricmp(szFileType,(LPSTR)"jpg")) )
		return IMG_JPG ;

	return 0 ;
}

#define IDX_COMPRESS 0
#define IDX_APPEND   1
#define IDX_XRES     2
#define IDX_YRES     3
#define IDX_COLORMOD 4
#define IDX_CPU      5
#define IDX_FILTERS  6
#define IDX_FILLORD  6

void LOADDS SetOptBlkOptions ( HWND hDlg, int iImgType, HANDLE hOpt, LPLONG lOpts )
{
	char szValue[ 255 ] ;
	LPSTR lpMisc ;
	switch ( iImgType )
		{
			case IMG_TIF :
				switch ( lOpts[ IDX_COMPRESS ] )
					{
						default :
							lpMisc = NULL ;
							break ;
						case IDC_LZWCOMP  :
							lpMisc = "LZW" ;
							break ;
						case IDC_PACKBITS :
							lpMisc = "PACKBITS" ;
							break ;
						case IDC_MODCCITT :
							lpMisc = "MODCCITT" ;
							break ;
						case IDC_GROUP3   :
							lpMisc = "GROUP3" ;
							break ;
						case IDC_GROUP4   :
							lpMisc = "GROUP4" ;
							break ;
					}
				if ( lpMisc )
					ImgXOptBlkAdd( hOpt, "COMPRESS", "ON" ) ;		// go ahead and set this in case
				ImgXOptBlkAdd (hOpt, "TIFF_COMPRESS", lpMisc ) ;

				lpMisc = ( lOpts[ IDX_COLORMOD ] == IDC_CMYKCOLOR ) ? "CMYK" : (LPSTR)NULL ;
				ImgXOptBlkAdd ( hOpt, "TIFF_COLORMODEL", lpMisc ) ;

				lpMisc = ( lOpts[ IDX_CPU ] == IDC_TIFFMOTOROLA ) ? "MOTOROLA" : (LPSTR)NULL ;
				ImgXOptBlkAdd( hOpt, "TIFF_BYTEFORMAT", lpMisc ) ;

				lpMisc = ( lOpts[ IDX_FILLORD ] == IDC_TIFFFILLORDERMSB ) ? "2" : (LPSTR)NULL ;
				ImgXOptBlkAdd( hOpt, "TIFF_FILLORDER", lpMisc ) ;

				GetDlgItemText ( hDlg, IDC_XRES, (LPSTR)szValue, sizeof(szValue)-1 ) ;
				lpMisc = szValue[0] ? (LPSTR)szValue : (LPSTR)NULL ;
				ImgXOptBlkAdd( hOpt, "TIFF_XRES", lpMisc ) ;

				GetDlgItemText ( hDlg, IDC_YRES, (LPSTR)szValue, sizeof(szValue)-1 ) ;
				lpMisc = szValue[0] ? (LPSTR)szValue : (LPSTR)NULL ;
				ImgXOptBlkAdd( hOpt, "TIFF_YRES", lpMisc ) ;

				GetDlgItemText ( hDlg, IDC_TIFFROWSPERSTRIP, szValue, sizeof(szValue)-1 ) ;
				lpMisc = szValue[0] ? (LPSTR)szValue : (LPSTR)NULL ;
				ImgXOptBlkAdd( hOpt, "TIFF_ROWSPERSTRIP", lpMisc ) ;

				GetDlgItemText ( hDlg, IDC_TIFFARTIST, szValue, sizeof(szValue)-1 ) ;
				lpMisc = szValue[0] ? (LPSTR)szValue : (LPSTR)NULL ;
				ImgXOptBlkAdd( hOpt, "TIFF_ARTIST", lpMisc ) ;

				GetDlgItemText ( hDlg, IDC_TIFFCOPYRIGHT, szValue, sizeof(szValue)-1 ) ;
				lpMisc = szValue[0] ? (LPSTR)szValue : (LPSTR)NULL ;
				ImgXOptBlkAdd( hOpt, "TIFF_COPYRIGHT", lpMisc ) ;

				break ;		// end of the TIFF'ers

			case IMG_GIF :
				lpMisc = IsDlgButtonChecked(hDlg, IDC_GIFINTERLACED ) ? "ON" : (LPSTR)NULL ;
				ImgXOptBlkAdd(hOpt, "GIF_INTERLACE", lpMisc ) ;
				break ;

			case IMG_PNG :
				switch ( lOpts[ IDX_FILTERS ] )
					{
						default :
						case IDC_PNGFILTERNONE :
							lpMisc = (LPSTR)NULL ;
							break ;
						case IDC_PNGFILTERSUB :
							lpMisc = "SUB" ;
							break ;
						case IDC_PNGFILTERUP :
							lpMisc = "UP" ;
							break ;
						case IDC_PNGFILTERAVERAGE :
							lpMisc = "AVERAGE" ;
							break ;
						case IDC_PNGFILTERPAETH :
							lpMisc = "PAETH" ;
							break ;
					}
				ImgXOptBlkAdd( hOpt, "PNG_FILTER", lpMisc ) ;
				switch ( lOpts[ IDX_COMPRESS ] )
					{
						default :
						case IDC_PNGNOCOMPRESS :
							lpMisc = (LPSTR)NULL ;
							break ;
						case IDC_PNGAVERAGECOMPRESS :
							lpMisc = "AVERAGE" ;
							break ;
						case IDC_PNGMAXCOMPRESS :
							lpMisc = "MAXIMUM" ;
							break ;
					}
				if ( lpMisc )
					ImgXOptBlkAdd( hOpt, "COMPRESS", "ON" ) ;		// go ahead and set this in case
				ImgXOptBlkAdd( hOpt, "PNG_COMPRESS", lpMisc ) ;
				break ;
			case IMG_DCX :
				break ;

			case IMG_JPG :
				GetDlgItemText ( hDlg, IDC_LOSSYQUALITY, szValue, sizeof(szValue)-1 ) ;
				lpMisc = szValue[0] ? (LPSTR)szValue : (LPSTR)NULL ;
				ImgXOptBlkAdd( hOpt, "LOSSY_QUALITY", lpMisc ) ;
				break ;

			default :
				break ;
		}
	ImgXOptBlkAdd( hOpt, "APPEND", lOpts[IDX_APPEND] ? "ON" : (LPSTR)NULL ) ;
}

void SetDlgWinHeight ( HWND hDlgSave, int iImgType, int iSavedHeight )
{
#ifdef _WIN32
	HWND hWnd = GetParent( hDlgSave ) ;
#else
	HWND hWnd = hDlgSave ;
#endif
	RECT clirect ;
	int  iHeight, iWidth ;

	GetWindowRect ( hWnd, & clirect ) ;

	iHeight = ( iImgType == IMG_TIF ) ? 205 :
						( iImgType == IMG_GIF ) ? 20  :
						( iImgType == IMG_DCX ) ? 30  :
						( iImgType == IMG_FMF ) ? 30  :
						( iImgType == IMG_PNG ) ? 100 :
						( iImgType == IMG_JPG ) ? 40  : 0 ;
#ifdef _WIN32
	iHeight += iSavedHeight ;
#else
	iHeight += 300 ;
#endif
	iWidth = clirect.right - clirect.left ;
	MoveWindow ( hWnd,     clirect.left, clirect.top, iWidth, iHeight, TRUE ) ;
// we only do a move on hDlgSave if it's 32 bit because in 16 bit
// hDlgSave IS our window and the previous MoveWindow already did it.
#ifdef _WIN32
	if ( ! iImgType )
		iHeight = 1 ;
	MoveWindow ( hDlgSave, 0, 0, iWidth, iHeight, TRUE ) ;
//	MoveWindow ( hDlgSave, clirect.left, clirect.top, iWidth, iHeight, TRUE ) ;
#endif
}

#ifndef  _WIN32
BOOL AllowExit ( LPSTR szFile )
{
	if ( ! szFile )
		return ( FALSE ) ;
	if ( ! szFile[0] )
		return ( FALSE ) ;
	if ( _fstrchr(szFile, '*' ) )
		return ( FALSE ) ;
	// we'll assume it's ok at this point
	return ( TRUE ) ;
}
#endif

UINT CALLBACK LOADDS ImgSaveProc ( HWND hDlg, UINT message,
												 WPARAM wParam, LPARAM lParam )
{
	LPSAVEDIALOG   lpSave = NULL ;
	LPOPENFILENAME lpOFN  ;
	char szValue[10];
	HWND hWnd ;
#ifdef _WIN32
	LPOFNOTIFY     lpNotify ;
#endif
	long ixx ;
  char szFile[ 255 ] ;
	BOOL bProcessed = FALSE ;
	int  iShowHide = 0 ;
	BOOL bNoSetProcessed = FALSE ;
	BOOL bSetOpts = FALSE ;
	BOOL bShowGIF = FALSE ;
	BOOL bAllowCompress = FALSE ;
	BOOL bCompressOpts = FALSE ;
	BOOL bColorOpts = FALSE ;
	BOOL bShowAppend = FALSE ;
	BOOL bAppend = FALSE ;
	BOOL bAllowLossy = FALSE ;
	BOOL bEnableFillOrder = FALSE ;
	LPSTR lpFileType = NULL ;
	LPSTR lpMisc ;
	int  nId, iImgType = 0 ;

	if ( lpOFN = (LPOPENFILENAME)GetWindowLong( hDlg, DWL_USER) )
		if ( lpSave = (LPSAVEDIALOG)lpOFN->lCustData )
			iImgType = CheckSetFileType ( (LPSTR)lpSave->szFileType ) ;

	switch ( message )
		{
			case WM_INITDIALOG :
				{
					RECT saverect ;
					SetWindowLong ( hDlg, DWL_USER, lParam ) ;
					if ( lpOFN = (LPOPENFILENAME) lParam )
						lpSave = (LPSAVEDIALOG) lpOFN->lCustData ;
					CheckRadioButton ( hDlg, IDC_RGBCOLOR, IDC_CMYKCOLOR, IDC_RGBCOLOR ) ;
					CheckRadioButton ( hDlg, IDC_TIFFINTEL, IDC_TIFFMOTOROLA, IDC_TIFFINTEL ) ;
					CheckRadioButton ( hDlg, IDC_TIFFFILLORDERLSB, IDC_TIFFFILLORDERMSB, IDC_TIFFFILLORDERLSB ) ;
					CheckRadioButton ( hDlg, IDC_PNGNOCOMPRESS, IDC_PNGMAXCOMPRESS, IDC_PNGNOCOMPRESS ) ;
					CheckRadioButton ( hDlg, IDC_PNGFILTERNONE, IDC_PNGFILTERPAETH, IDC_PNGFILTERNONE ) ;

					SendMessage( GetDlgItem(hDlg,IDC_XRES), EM_LIMITTEXT, 4, 0 ) ;
					SendMessage( GetDlgItem(hDlg,IDC_YRES), EM_LIMITTEXT, 4, 0 ) ;
					szValue[0] = 0 ;
					ImgXOptBlkGet( lpSave->hOptBlk, "XRES", (LPSTR)&szValue ) ;
					SetDlgItemText ( hDlg, IDC_XRES, (LPSTR)&szValue ) ;
					szValue[0] = 0 ;
					lpMisc = (LPSTR)&"YRES" ;
					ImgXOptBlkGet( lpSave->hOptBlk, lpMisc, (LPSTR)szValue ) ;
					SetDlgItemText ( hDlg, IDC_YRES, (LPSTR)szValue ) ;
						
					SendMessage( GetDlgItem(hDlg,IDC_LOSSYQUALITY), EM_LIMITTEXT, 2, 0 ) ;
					SetDlgItemText ( hDlg, IDC_LOSSYQUALITY, "75" ) ;
#ifdef _WIN32
					GetWindowRect ( GetParent(hDlg), & saverect ) ;
          lpSave->iSaveWindowHeight = (UINT)(saverect.bottom - saverect.top) ;
#else
          GetWindowRect ( hDlg, & saverect ) ;
#endif
					SetDlgWinHeight ( hDlg, 0, lpSave->iSaveWindowHeight ) ;
				}
				bSetOpts = TRUE ;
				break ;
			case WM_DESTROY :
				break ;

#ifdef _WIN32
			case WM_NOTIFY :
				lpNotify = (LPOFNOTIFY) lParam ;
				lpOFN = lpNotify->lpOFN ;
				switch ( lpNotify->hdr.code )
					{
						case CDN_SELCHANGE :
							CommDlg_OpenSave_GetSpec(GetParent(hDlg), szFile, sizeof(szFile) ) ;
							if ( szFile[0] )
								{
									if ( ( strchr( szFile, '*' ) ) &&  // they may manually wild-card it
										   ( strlen( szFile ) >= 4 ) )
										{
											strcpy ( lpSave->szFileType, & szFile[ strlen(szFile)-3 ] ) ;
											iImgType = CheckSetFileType( lpSave->szFileType ) ;
											SetDlgWinHeight ( hDlg, iImgType, lpSave->iSaveWindowHeight ) ;
											bSetOpts = TRUE ;
										}
								}
							break ;
						case CDN_TYPECHANGE :		// nFilter index points to new value
							lpFileType = (LPSTR)lpOFN->lpstrFilter ;
							for ( ixx = 0  ; ixx != ( (lpOFN->nFilterIndex * 2 ) - 1 ) ; ixx++ )
								lpFileType += strlen( lpFileType ) + 1 ;
							lpFileType += 2 ;     // this is to skip the "*."
							strcpy ( lpSave->szFileType, lpFileType ) ;
							iImgType = CheckSetFileType( lpSave->szFileType ) ;
							SetDlgWinHeight ( hDlg, iImgType, lpSave->iSaveWindowHeight ) ;
							bSetOpts = TRUE ;
							break ;

						case CDN_FILEOK :
							SetOptBlkOptions ( hDlg, iImgType, lpSave->hOptBlk, lpSave->lOptions ) ;
							return 0 ;
							break ;

						default :
							break ;
					}
				break ;
#endif

			case WM_COMMAND :
				bSetOpts = TRUE ;
				switch ( LOWORD(wParam) )
					{
#ifndef _WIN32
						case IDOK :
							GetDlgItemText( hDlg, edt1, szFile, sizeof(szFile)-1 ) ;
							if ( AllowExit( (LPSTR)szFile ) )
								{
									_fstrcpy ( lpOFN->lpstrFile, (LPSTR)szFile ) ;
									SetOptBlkOptions ( hDlg, iImgType, lpSave->hOptBlk, (LPLONG)lpSave->lOptions ) ;
									EndDialog( hDlg, TRUE );
								}
							return ( TRUE ) ;

						case IDCANCEL :
							EndDialog( hDlg, FALSE );
							return ( TRUE ) ;
							break ;

						case edt1 :		//  filename edit box
							szFile[0] = 0 ;
							GetDlgItemText( hDlg, edt1, szFile, sizeof(szFile)-1 ) ;
							if ( szFile[0] )
								{
									if ( ( _fstrchr( (LPSTR)szFile, '*' ) ) &&  // they may manually wild-card it
										   ( _fstrlen( (LPSTR)szFile ) >= 4 ) )
										{
											if ( lpSave )
												{
													_fstrcpy ( (LPSTR)lpSave->szFileType, (LPSTR)& szFile[ _fstrlen((LPSTR)szFile)-3 ] ) ;
													iImgType = CheckSetFileType( (LPSTR)lpSave->szFileType ) ;
													SetDlgWinHeight ( hDlg, iImgType, lpSave->iSaveWindowHeight ) ;
												}
											bSetOpts = TRUE ;
										}
								}
							bNoSetProcessed = TRUE ;
							break ;
						case cmb1 :		//  file types box
							if ( lpOFN )
								{
									lpSave = (LPSAVEDIALOG) lpOFN->lCustData ;
									lpFileType = (LPSTR)lpOFN->lpstrFilter ;
									if ( lpFileType )
										{
											szFile[0] = 0 ;
											GetDlgItemText( hDlg, edt1, szFile, sizeof(szFile)-1 ) ;
											if ( szFile[0] )
												{
													if ( ( _fstrchr( (LPSTR)szFile, '*' ) ) &&  // they may manually wild-card it
															 ( _fstrlen( (LPSTR)szFile ) >= 4 ) )
														{
															_fstrcpy ( (LPSTR)lpSave->szFileType, (LPSTR)& szFile[ _fstrlen((LPSTR)szFile)-3 ] ) ;
															iImgType = CheckSetFileType( (LPSTR)lpSave->szFileType ) ;
															SetDlgWinHeight ( hDlg, iImgType, lpSave->iSaveWindowHeight ) ;
															bSetOpts = TRUE ;
														}
												}
										}
								}
							bNoSetProcessed = TRUE ;
							break ;

						case lst1 :		//  file list box
						case lst2 :		//  dir list box
						case cmb2 :		//  drive list drop down
							bNoSetProcessed = TRUE ;
							break ;
#endif
						case IDC_COMPRESS :
							if ( ( bCompressOpts=IsDlgButtonChecked( hDlg, IDC_COMPRESS )) )
								lpMisc = "ON" ;
							else
								lpMisc = NULL ;
							ImgXOptBlkAdd ( lpSave->hOptBlk, "COMPRESS", lpMisc ) ;
							break ;

						case IDC_LZWCOMP  :
						case IDC_PACKBITS :
						case IDC_MODCCITT :
						case IDC_GROUP3   :
						case IDC_GROUP4   :
							lpSave->lOptions[ IDX_COMPRESS ] = LOWORD( wParam ) ;
							bCompressOpts = TRUE ;
							break ;

						case IDC_RGBCOLOR  :
						case IDC_CMYKCOLOR :
							lpSave->lOptions[ IDX_COLORMOD ] = LOWORD( wParam ) ;
							break ;

						case IDC_TIFFFILLORDERLSB:
						case IDC_TIFFFILLORDERMSB :
							lpSave->lOptions[ IDX_FILLORD ] = LOWORD( wParam ) ;
							break ;

						case IDC_APPENDIMAGE :
							bAppend = IsDlgButtonChecked(hDlg, IDC_APPENDIMAGE) ;
							lpSave->lOptions[ IDX_APPEND ] = (long)bAppend ;
							break ;

						case IDC_PNGFILTERNONE :
						case IDC_PNGFILTERSUB :
						case IDC_PNGFILTERUP :
						case IDC_PNGFILTERAVERAGE :
						case IDC_PNGFILTERPAETH :
							lpSave->lOptions[ IDX_FILTERS ] = LOWORD( wParam ) ;
							break ;

						case IDC_PNGNOCOMPRESS :
						case IDC_PNGAVERAGECOMPRESS :
						case IDC_PNGMAXCOMPRESS :
							lpSave->lOptions[ IDX_COMPRESS ] = LOWORD( wParam ) ;
							break ;

						case IDC_TIFFINTEL    :
						case IDC_TIFFMOTOROLA :
							lpSave->lOptions[ IDX_CPU ] = LOWORD( wParam ) ;
							break;
					}
				if ( ! bNoSetProcessed )
					bProcessed = TRUE ;
				break ;
		}
	switch ( iImgType )
		{
			case IMG_TIF :
				bAllowCompress = bColorOpts = TRUE ;
				bCompressOpts = IsDlgButtonChecked( hDlg, IDC_COMPRESS ) ;
				break ;
			case IMG_PNG :
				bCompressOpts = TRUE ;
				break ;
			case IMG_GIF :
				bShowGIF = TRUE ;
				break ;
			case IMG_DCX :
				break ;
			case IMG_JPG :
				bAllowLossy = TRUE ;
				break ;
			default :
				break ;
		}
	if ( ( iImgType == IMG_TIF ) ||
		   ( iImgType == IMG_GIF ) ||
			 ( iImgType == IMG_FMF ) ||
			 ( iImgType == IMG_DCX ) )
		bShowAppend = TRUE ;
	if ( bSetOpts )
		{
			ShowWindow( hWnd=GetDlgItem(hDlg, IDC_COMPRESS), bAllowCompress ? SW_SHOW : SW_HIDE ) ;
			EnableWindow( hWnd, bAllowCompress ) ;

			iShowHide = bAllowLossy ? SW_SHOW : SW_HIDE ;
			ShowWindow( hWnd=GetDlgItem(hDlg, IDC_LOSSYQUALITY), iShowHide ) ;
			EnableWindow( hWnd, bAllowLossy ) ;
			ShowWindow( GetDlgItem(hDlg,IDC_STATICJPEGLOSSY), iShowHide ) ;

			iShowHide = bShowGIF ? SW_SHOW : SW_HIDE ;
			ShowWindow ( hWnd=GetDlgItem(hDlg,IDC_GIFINTERLACED), iShowHide ) ;
			EnableWindow( hWnd, bShowGIF ) ;

			iShowHide = bShowAppend ? SW_SHOW : SW_HIDE ;
			ShowWindow ( hWnd=GetDlgItem(hDlg,IDC_APPENDIMAGE), iShowHide ) ;
			EnableWindow( hWnd, bShowAppend ) ;

			iShowHide = bColorOpts ? SW_SHOW : SW_HIDE ;
			ShowWindow( GetDlgItem(hDlg, IDC_STATICCOLOR), iShowHide ) ;
			ShowWindow( GetDlgItem(hDlg,    IDC_RGBCOLOR), iShowHide ) ;
			ShowWindow( GetDlgItem(hDlg,   IDC_CMYKCOLOR), iShowHide ) ;

			bEnableFillOrder = (( iImgType == IMG_TIF ) && (lpSave->iColors == 1 )) ;
			EnableWindow ( GetDlgItem(hDlg,IDC_STATICTIFFFILLORDER), bEnableFillOrder ) ;
			EnableWindow ( GetDlgItem(hDlg,IDC_TIFFFILLORDERLSB), bEnableFillOrder ) ;
			EnableWindow ( GetDlgItem(hDlg,IDC_TIFFFILLORDERMSB), bEnableFillOrder ) ;

			iShowHide = ( iImgType == IMG_TIF ) ? SW_SHOW : SW_HIDE ;
			ShowWindow ( hWnd=GetDlgItem(hDlg, IDC_STATICCOMPRESS), iShowHide ) ;
			EnableWindow( hWnd, TRUE ) ;
			bCompressOpts = IsDlgButtonChecked( hDlg, IDC_COMPRESS ) ;
			for ( nId = IDC_LZWCOMP ; nId <= IDC_GROUP4 ; nId++ )
				{
					ShowWindow ( hWnd=GetDlgItem(hDlg,nId), iShowHide ) ;
					if ( iShowHide == SW_SHOW )
						{
							if ( (( nId == IDC_MODCCITT ) ||
								    ( nId == IDC_GROUP3   ) ||
										( nId == IDC_GROUP4   )) &&
									 ( lpSave->iColors != 1  ) )
								EnableWindow( hWnd, FALSE ) ;
							else
								EnableWindow( hWnd, bCompressOpts ) ;
						}
				}
			for ( nId = IDC_XRES ; nId <= IDC_STATICTIFFCOPYRIGHT ; nId ++ )
				ShowWindow ( GetDlgItem(hDlg,nId), iShowHide ) ;

			iShowHide = ( bCompressOpts=(iImgType == IMG_PNG) ) ? SW_SHOW : SW_HIDE ;
			for ( nId = IDC_STATICPNGFILTER ; nId <= IDC_PNGMAXCOMPRESS ; nId++ )
				{
					ShowWindow ( hWnd=GetDlgItem(hDlg,nId), iShowHide ) ;
					EnableWindow( hWnd, bCompressOpts ) ;
				}
		}
	return ( bProcessed ) ;
}

