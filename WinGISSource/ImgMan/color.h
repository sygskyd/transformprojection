/////////////////////////////////////////////////////////////////////////////
//
//    color.h
//
//    Color Reduction/Dithering Module Prototypes
//
//    ImageMan Image Processing Library
//
//    Copyright (c) 1991-5 Data Techniques, Inc.
//    All Rights Reserved
//
/////////////////////////////////////////////////////////////////////////////
// $History: color.h $
// 
// *****************  Version 2  *****************
// User: Ericw        Date: 9/23/97    Time: 5:14p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Implemented octree color reduction stuff for v6.
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 7/10/97    Time: 2:04p
// Created in $/imageman 16-32/dll-api level 2
// Initial Lvl 2 Checkin
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 1/18/95    Time: 3:59p
// Created in $/ImageMan 16-32/DLL
// ImageMan 4.0 Beta 1

extern HANDLE PASCAL DitherToBW(LPINTERNALINFO,DWORD);
extern HANDLE PASCAL ColorToGrayscale(LPINTERNALINFO);
extern HANDLE PASCAL ReduceColorToColor(LPINTERNALINFO,UINT,DWORD, HPALETTE );
extern HANDLE NEAR PASCAL AllocBitmap(long nColors,int nWidth,int nHeight);


