/////////////////////////////////////////////////////////////////////////////
//
//    pri.h
//
//    PRI Defines
//
//    ImageMan Image Processing Library
//
//    Copyright (c) 1991-1995 Data Techniques, Inc.
//    All Rights Reserved
//
/////////////////////////////////////////////////////////////////////////////
// $History: PRI.H $
// 
// *****************  Version 1  *****************
// User: Sygsky from JHC Date: 04-OCT-2000  Time: 15:47
// Updated in $/ImageMan 16-32/DILS
// Added version information
// 

/* defines for Image error strings */
#define IDS_NOOPEN      	1
#define IDS_READERR     	2
#define IDS_MEMERR      	3
#define IDS_INVHAND     	4
#define IDS_EOF       		5
#define IDS_NOPRINT     	6
#define IDS_BADMAN      	7
#define IDS_BADSWAP     	8
#define IDS_PROCESS     	9
#define IDS_BADBITS     	10
#define IDS_BADCOMPRESS 	11
#define IDS_BADPLANES   	12
#define IDS_BADCONFIG   	13
#define IDS_BADPREDICTOR  14
#define IDS_INVPAGE			15

#define WBUFSIZE       4096 // default buffer size

// Make sure everyone has access to this stuff.
typedef struct tagFileStruct {
	LPVOID		lpOpenInfo;	// info needed to reopen the image
	LPVOID		lpHandle;
	LPIMG_IO		lpIO;
	long        offset;		// offset of image in file
	TiffStruct  tiffHead;	// tiff tag info
	int			nPixBits;	// # of bits/pixel in output DIB
	long        row;			// next row for this image
	int         nCurPage;	// current page
	ULONG				lCurIFDOffset;	//offset in file of IFD for current page
	LPLZWC      lpLZW;		// LZW decompression struct (if needed)
	LONG			lRowSize;
	// CCITT state information:
	ULONG       lCodes;		// bit code accumulator; take bits off top
	int         nBits;		// valid bit count inside 'lCodes'
	UINT        bBuf2Offset;	// location of Group 4 bbuf2 after 'wbuf'
	UINT        bBufPrev;	// toggles between 0 and bBuf2Offset
	UINT			wbufpos;
	UINT			wbufsize;
	// Group 4 decode buffers will be after 'wbuf'
	char			wbuf[1];
}           FileStruct;

// TRUE if each data byte must have its bit order reversed
#define BYTEFLIP(F)    ((F)->tiffHead.FillOrder == 2)
// TRUE if photointerp says to change white->black and black->white
#define PHOTOFLIP(F)   ((F)->tiffHead.WhiteValue == 0)

#define BBUF2OFFSET    (1000*sizeof(int))

#define CCITT_BLACKMAX 13
#define CCITT_WHITEMAX 12
#ifdef WIN32
#define GETTOPBITS(V,N) ((V)>>(32-(N)))
#else
#define GETTOPBITS(V,N) (HIWORD(V)>>(16-(N)))
#endif

UINT        wgetword(FileStruct FAR *);
void        ccitt_blackrun(HPSTR, int, int);

#define DLL_FILEDESCRIPTION        "ImageMan PRI Reader\0"
#define DLL_INTERNALNAME           "IMAGEMAN\0"
#define DLL_ORIGINALFILENAME       "PRI.DIL\0"
#define DLL_PRODUCTNAME            "ImageMan Image Processing Toolkit\0"
#ifdef USELZW
#define DLL_VERSION                 5,00,1,00
#define DLL_VERSION_STR            "5.00.001\0"
#else
#define DLL_VERSION                 5,00,0,00
#define DLL_VERSION_STR            "5.00.000\0"
#endif
