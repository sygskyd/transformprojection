/*****************************************************************************
 *
 *    File Name:     $Workfile: Keylib.h $
 *
 *    Function:       
 *
 *    Description:    
 *
 *    Author:        $Author: Ericj $
 *
 *    Revision:      $Revision: 1 $
 *
 *    $History: Keylib.h $
// 
// *****************  Version 1  *****************
// User: Ericj        Date: 7/14/99    Time: 5:23p
// Created in $/ImageMan 16-32/DLL - V 6.xx API
 * 
 * *****************  Version 43  *****************
 * User: Wozniakm     Date: 6/08/99    Time: 10:11p
 * Updated in $/PPP/KeyLib/Source
 * Added prototype for pp_semused().
 * 
 * *****************  Version 42  *****************
 * User: Wozniakm     Date: 5/17/99    Time: 2:35p
 * Updated in $/PPP/KeyLib/Source
 * Added two more COMPNO defines.
 * 
 * *****************  Version 41  *****************
 * User: Wozniakm     Date: 4/06/99    Time: 6:10p
 * Updated in $/PPP/KeyLib/Source
 * Added another FAKE ERR line to make FoxProW work properly.
 * 
 * *****************  Version 40  *****************
 * User: Wozniakm     Date: 4/06/99    Time: 3:36p
 * Updated in $/PPP/KeyLib/Source
 * Added more EZTrial variable definitions.
 * 
 * *****************  Version 39  *****************
 * User: Wozniakm     Date: 3/30/99    Time: 5:56p
 * Updated in $/PPP/KeyLib/Source
 * Added fake error number to make KEYFOXW.FLL to generate the write BIOS
 * number.
 * 
 * *****************  Version 38  *****************
 * User: Wozniakm     Date: 3/26/99    Time: 5:47p
 * Updated in $/PPP/KeyLib/Source
 * Changed HWND parameters to LONG.
 * 
 * *****************  Version 37  *****************
 * User: Wozniakm     Date: 3/16/99    Time: 8:15p
 * Updated in $/PPP/KeyLib/Source
 * Added the flag LFOPEN_NOLOOP.
 * 
 * *****************  Version 36  *****************
 * User: Wozniakm     Date: 3/09/99    Time: 7:37p
 * Updated in $/PPP/KeyLib/Source
 * Added the lfhandleret parameter to pp_eztrial1().
 * 
 * *****************  Version 35  *****************
 * User: Wozniakm     Date: 3/08/99    Time: 10:15p
 * Updated in $/PPP/KeyLib/Source
 * Added the user-defined string labels parameter to pp_eztrig1dlg()
 * prototype.
 * 
 * *****************  Version 34  *****************
 * User: Wozniakm     Date: 3/06/99    Time: 1:13p
 * Updated in $/PPP/KeyLib/Source
 * Changed number for LF_INTERNET.  Added variables and errors for EZTrial
 * and EZTrigger.
 * 
 * *****************  Version 33  *****************
 * User: Wozniakm     Date: 2/13/99    Time: 3:14p
 * Updated in $/PPP/KeyLib/Source
 * Added definition for LF_INTERNET and error codes specific to WINSOCK
 * communication.
 * 
 * *****************  Version 32  *****************
 * User: Wozniakm     Date: 2/01/99    Time: 7:20p
 * Updated in $/PPP/KeyLib/Source
 * Added additional define for ERR_INVALID_LFHANDLE.
 * 
 * *****************  Version 31  *****************
 * User: Wozniakm     Date: 11/10/98   Time: 10:33a
 * Updated in $/PPP/KeyLib/Source
 * Added prototype for pp_daysleft().  Renamed ACTION_STRICT to
 * COPYCHK_STRICT.
 * 
 * *****************  Version 30  *****************
 * User: Wozniakm     Date: 11/08/98   Time: 4:05p
 * Updated in $/PPP/KeyLib/Source
 * Added the define ACTION_STRICT for pp_copycheck().
 * 
 * *****************  Version 29  *****************
 * User: Wozniakm     Date: 11/05/98   Time: 4:23p
 * Updated in $/PPP/KeyLib/Source
 * Added the two constants UPDDATE_NOTIME and UPDDATE_FORCE which are used
 * in pp_upddate().
 * 
 * *****************  Version 28  *****************
 * User: Wozniakm     Date: 4/05/98    Time: 12:13p
 * Updated in $/PPP/KeyLib/Source
 * Added prototype for pp_lfdelete().
 * 
 * *****************  Version 27  *****************
 * User: Wozniakm     Date: 1/07/98    Time: 2:55p
 * Updated in $/PPP/KeyLib/Source
 * Changed most pp_ function names to PP_ so the LIBRARY_CLIPPER library
 * can call the pp_ function names as documented in the manual.
 * Previously, we had to use p_ because there was a conflict in the main
 * library code and the CLIPPER wrapper functions.
 * 
 * *****************  Version 26  *****************
 * User: Wozniakm     Date: 12/29/97   Time: 6:19p
 * Updated in $/PPP/KeyLib/Source
 * Added definition for new error ERR_DEMO_HAS_EXPIRED.
 * 
 * *****************  Version 25  *****************
 * User: Wozniakm     Date: 8/13/97    Time: 12:45a
 * Updated in $/PPP/KeyLib/Source
 * Changed prototype for pp_nencryptx()/decyptx().  Changed processor
 * directive for DOS library definitions.
 * 
 * *****************  Version 24  *****************
 * User: Wozniakm     Date: 8/11/97    Time: 11:10p
 * Updated in $/PPP/KeyLib/Source
 * Added definitions for HINSTANCE and HWND so they can compile in DOS
 * libraries.
 * 
 * *****************  Version 23  *****************
 * User: Wozniakm     Date: 8/10/97    Time: 11:34a
 * Updated in $/PPP/KeyLib/Source
 * Removed the IFDEF that took out pp_getcode() and pp_initlib().  Change
 * the return type for pp_ndecryptx().
 * 
 * *****************  Version 22  *****************
 * User: Wozniakm     Date: 6/04/97    Time: 10:10p
 * Updated in $/PPP/KeyLib/Source
 * Added functions for pp_nencryptx() and pp_ndecryptx().
 * 
 * *****************  Version 21  *****************
 * User: Wozniakm     Date: 4/08/97    Time: 6:21p
 * Updated in $/PPP/KeyLib/Source
 * Defined needed data types for C/DOS library.
 * 
 * *****************  Version 20  *****************
 * User: Wozniakm     Date: 2/25/97    Time: 5:40p
 * Updated in $/PPP/KeyLib/Source
 * Initial changes to "C" library to get it to compile.
 * 
 * *****************  Version 19  *****************
 * User: Wozniakm     Date: 1/05/97    Time: 4:40p
 * Updated in $/PPP/KeyLib/Source
 * The function prototype for pp_password() was added since it was
 * missing.  The define for VAR_LF_CHECKSUM was added.
 * 
 * *****************  Version 18  *****************
 * User: Wozniakm     Date: 12/15/96   Time: 7:03p
 * Updated in $/PPP/KeyLib/Source
 * Changed prototype of pp_chkvarxxx() functions to reflect the first
 * parameter name change.
 * 
 * *****************  Version 17  *****************
 * User: Wozniakm     Date: 12/15/96   Time: 2:08p
 * Updated in $/PPP/KeyLib/Source
 * Added define for COMPNO_MACADDR.
 * 
 * *****************  Version 16  *****************
 * User: Wozniakm     Date: 12/12/96   Time: 11:09p
 * Updated in $/PPP/KeyLib/Source
 * Added an error code to list.  Added prototypes for pp_chkvarxxx()
 * functions.
 * 
 * *****************  Version 15  *****************
 * User: Wozniakm     Date: 11/05/96   Time: 9:43p
 * Updated in $/PPP/KeyLib/Source
 * Added the variable TCODE_MAX for Mark.
 * 
 * *****************  Version 14  *****************
 * User: Wozniakm     Date: 11/02/96   Time: 5:40p
 * Updated in $/PPP/KeyLib/Source
 * Added support for Crypto-Box Versa and 560.
 * 
 * *****************  Version 13  *****************
 * User: Wozniakm     Date: 10/25/96   Time: 12:13a
 * Updated in $/PPP/KeyLib/Source
 * Added definitions for SEM_ options.
 * 
 * *****************  Version 12  *****************
 * User: Wozniakm     Date: 10/07/96   Time: 9:30p
 * Updated in $/PPP/KeyLib/Source
 * Added #ifdef for C++ so the function prototypes are not mangled.
 * 
 * *****************  Version 11  *****************
 * User: Wozniakm     Date: 9/28/96    Time: 10:28p
 * Updated in $/PPP/KeyLib/Source
 * Added error codes for Registry functions.  Added prototype to
 * pp_initlib().
 * 
 * *****************  Version 10  *****************
 * User: Wozniakm     Date: 9/22/96    Time: 4:36p
 * Updated in $/PPP/KeyLib/Source
 * Added missing prototype for pp_npdate() and changed pp_lfopen() to
 * accept a flags parameter.
 * 
 * *****************  Version 9  *****************
 * User: Wozniakm     Date: 9/16/96    Time: 10:12a
 * Updated in $/PPP/KeyLib/Source
 * Changed function prototypes of pp_lfcreate() and pp_copyadd() - they
 * both got <flags> parameters.  Added #defines for the various flag bit
 * definitions.
 * 
 * *****************  Version 8  *****************
 * User: Wozniakm     Date: 9/01/96    Time: 2:58a
 * Updated in $/PPP/KeyLib/Source
 * Added LF checksum failure error flag.
 * 
 * *****************  Version 7  *****************
 * User: Wozniakm     Date: 8/31/96    Time: 5:14p
 * Updated in $/PPP/KeyLib/Source
 * Added one more global error code.
 * 
 * *****************  Version 6  *****************
 * User: Wozniakm     Date: 8/26/96    Time: 12:25p
 * Updated in $/PPP/KeyLib/Source
 * Reorganized error numbers again since when I implemented pp_errorstr()
 * realized they have to be sequential numbers.
 * 
 * *****************  Version 5  *****************
 * User: Wozniakm     Date: 8/31/96    Time: 11:41a
 * Updated in $/PPP/KeyLib/Source
 * Added a few result codes and re-numbered them all.  Added definitions
 * for more user-defined fields.  Made a few changes to the prototypes.  
 * 
 * *****************  Version 4  *****************
 * User: Wozniakm     Date: 8/25/96    Time: 11:06p
 * Updated in $/PPP/KeyLib/Source
 * 
 * *****************  Version 3  *****************
 * User: Wozniakm     Date: 8/25/96    Time: 10:18p
 * Updated in $/PPP/KeyLib/Source
 *
 *    Copyright (c) 1996 Concept Software, Inc.  All rights reserved.
 *
 ****************************************************************************/

#ifndef KEYLIB_H
#define KEYLIB_H

#ifdef __cplusplus     /* Assume C declarations for C++ */
extern "C" {
#endif  /* __cplusplus */

/* computer number defines */
#define COMPNO_BIOS              0x0001
#define COMPNO_HDSERIAL          0x0002
#define COMPNO_HDLOCK            0x0004
#define COMPNO_HDTYPE            0x0008
#define COMPNO_NETNAME           0x0010
#define COMPNO_MACADDR           0x0020
#define COMPNO_FILESTART         0x0040
#define COMPNO_WINPRODID         0x0080


/* pp_copycheck() action codes */
#define ACTION_MANUAL            0x0000
#define ACTION_AUTOADD           0x0001
#define COPYCHK_STRICT           0x0002


/* pp_copyadd() flags */
#define COPYADD_ADDDUPE          0x0001
#define COPYADD_ERASEALL         0x0002


/* license file type defines */
#define LF_FILE                  1
#define LF_REGISTRY              2
#define LF_CRYPTO_BOX_VERSA      3
#define LF_CRYPTO_BOX_560        4
#define LF_INTERNET              5


/* pp_lfopen() flag defines */
#define LF_CREATE_NORMAL         0x0000
#define LF_CREATE_RDONLY         0x0001
#define LF_CREATE_HIDDEN         0x0002
#define LF_CREATE_SYSTEM         0x0004
#define LF_CREATE_MISSING        0x0008
#define LFOPEN_NOLOOP            0x0010


/* pp_lfcreate() flags */
#define LFCREATE_OVERWRITE       0x0001


/* pp_semopen() flags */
#define SEM_FILE                    0
#define SEM_TCPIP                   1


/* pp_upddate() flags */
#define UPDDATE_FORCE               1
#define UPDDATE_NOTIME              2


/* pp_lfalias() flags */
#define LFALIAS_VERIFY_CHECKSUM     1
#define LFALIAS_FIND_RECENT         2
#define LFALIAS_OVERWRITE_OLDER     4
#define LFALIAS_CREATE_MISSING      8

/* pp_eztrig1dlg() flags */
#define EZTRIG1DLG_PROCESS          1
#define EZTRIG1DLG_DELAYED          2


/* miscellaneous defines */
#define TCODE_MAX                   50
#define PP_MAX_UDEF_STRINGS         10
#define PP_MAX_UDEF_NUMBERS         5
#define PP_MAX_UDEF_DATES           5


/* result codes */
#define PP_FAILURE                              0
#define PP_FALSE                                0
#define PP_SUCCESS                              1
#define PP_TRUE                                 1

#define ERR_INVALID_ATTRIBUTES                  2
#define ERR_CANNOT_CHANGE_ATTRIBS               3
#define ERR_HFILE_ERROR                         4
#define ERR_CANNOT_WRITE_FILE                   5
#define ERR_CANNOT_CLOSE_FILE                   6
#define ERR_CANNOT_OPEN_FILE                    7
#define ERR_CANNOT_READ_FILE                    8
#define ERR_CANNOT_CREATE_FILE                  9
#define ERR_CANNOT_DELETE_FILE                  10
#define ERR_FILE_WAS_CREATED                    11
#define ERR_INVALID_PASSWORD                    12
#define ERR_WRONG_PASSWORD                      13
#define ERR_INCORRECT_PARAMETERS                14
#define ERR_FILE_MISSING                        15
#define ERR_MEMORY_ALLOCATION                   16
#define ERR_MEMORY_FREE                         17
#define ERR_INVALID_LFHANDLE                    18
#define ERR_MEMORY_LOCK                         18
#define ERR_SLOT_NUM_INVALID                    19
#define ERR_SLOT_EMPTY                          20
#define ERR_SLOTS_FULL                          21
#define ERR_SLOT_ALREADY_ASSIGNED               22
#define ERR_NET_LIC_FULL                        23
#define ERR_COMPNO_NOT_FOUND                    24
#define ERR_VAR_NO_INVALID                      25
#define ERR_SOFT_EXPIRATION                     26
#define ERR_EXPTYPE_INVALID                     27
#define ERR_EXP_DATE_EMPTY                      28
#define ERR_STRING_TOO_LONG                     29
#define ERR_CURRENT_DATE_OLDER                  30
#define ERR_CANNOT_LOCK_FILE                    31
#define ERR_WRONG_LF_VERSION                    32
#define ERR_CORRUPT_LICENSE_FILE                33
#define ERR_SEM_FILE_LOCKED                     34
#define ERR_CORRUPT_CONTROL_FILE                35
#define ERR_WRONG_CF_SERIAL_NUM                 36
#define ERR_LF_LOCKED                           37
#define ERR_LF_CHECKSUM_INVALID                 38
#define ERR_NOT_APPLICABLE                      39
#define ERR_NOT_IMPLEMENTED_YET                 40
#define ERR_FILE_EXISTS                         41
#define ERR_REGISTRY_OPEN                       42
#define ERR_REGISTRY_QUERY                      43
#define ERR_REGISTRY_CLOSE                      44
#define ERR_REGISTRY_READ                       45
#define ERR_REGISTRY_SET                        46
#define ERR_CBOX_NOT_PRESENT                    47
#define ERR_CBOX_WRONG_TYPE                     48
#define ERR_CBOX_READ_RAM1_ERROR                49
#define ERR_CBOX_READ_RAM2_ERROR                50
#define ERR_CBOX_WRITE_RAM1_ERROR               51
#define ERR_CBOX_WRITE_RAM2_ERROR               52
#define ERR_CBOX_ID1_ERROR                      53
#define ERR_CBOX_ID2_ERROR                      54
#define ERR_CBOX_ID3_ERROR                      55
#define ERR_VAR_NOT_AVAILABLE                   56
#define ERR_DEMO_HAS_EXPIRED                    57
#define ERR_WINSOCK_STARTUP_ERROR               58
#define ERR_WINSOCK_CANNOT_RESOLVE_HOST         59
#define ERR_WINSOCK_CANNOT_CREATE_SOCKET        60
#define ERR_WINSOCK_CANNOT_CONNECT_TO_SERVER    61
#define ERR_WINSOCK_CANNOT_SEND_DATA            62
#define ERR_WINSOCK_CANNOT_READ_DATA            63
#define ERR_NO_MORE_SOFTWARE_KEYS_AVAILABLE     64
#define ERR_INVALID_SERVER_RESPONSE             65
#define ERR_CANNOT_ALLOCATE_MEMORY              66
#define ERR_WINSOCK_CANNOT_RESOLVE_PROXY        67
#define ERR_ALIAS_FILE_DOES_NOT_MATCH           68
#define ERR_INVALID_CODE_ENTERED                69
#define ERR_FAKE_FOR_FOXPRO

/* file attributes defines */
#define PP_NORMAL                0x0000
#define PP_RDONLY                0x0001
#define PP_HIDDEN                0x0002
#define PP_SYSTEM                0x0004


/* expiration types */
#define EXP_NONE                 "N"
#define EXP_EXE_LIMIT            "E"
#define EXP_SHAREWARE_VER        "S"
#define EXP_PAYMENT_LIMIT        "P"
#define EXP_DEMO_VERSION         "D"


/* getvar / setvar definitions - character fields */
#define VAR_COMPANY                 1
#define VAR_NAME                    2
#define VAR_ADDRESS1                3
#define VAR_ADDRESS2                4
#define VAR_ADDRESS3                5
#define VAR_PHONE1                  6
#define VAR_PHONE2                  7
#define VAR_SERIAL_TEXT             8
#define VAR_EXPIRE_TYPE             9
#define VAR_UDEF_CHAR_1             10
#define VAR_UDEF_CHAR_2             11
#define VAR_UDEF_CHAR_3             12
#define VAR_UDEF_CHAR_4             13
#define VAR_UDEF_CHAR_5             14
#define VAR_UDEF_CHAR_6             15
#define VAR_UDEF_CHAR_7             16
#define VAR_UDEF_CHAR_8             17
#define VAR_UDEF_CHAR_9             18
#define VAR_UDEF_CHAR_10            19
#define VAR_EZTRIAL_COMPNO_DRIVE    20
#define VAR_EZTRIAL_SYSDIR_FILENAME 21
#define VAR_EZTRIG_COMPNO_DRIVE     22
#define VAR_EZTRIAL_REG_ALIAS1      23
#define VAR_EZTRIAL_REG_ALIAS2      24
#define VAR_EZTRIG_DLG_LABELS       25


/* getvar / setvar definitions - numeric fields */
#define VAR_SERIAL_NUM                 1
#define VAR_EXP_COUNT                  2
#define VAR_EXP_LIMIT                  3
#define VAR_LAN_COUNT                  4
#define VAR_LAN_LIMIT                  5
#define VAR_INSTALL_COUNT              6
#define VAR_INSTALL_LIMIT              7
#define VAR_AUTHORIZED_COMPS           8
#define VAR_UDEF_NUM_1                 9
#define VAR_UDEF_NUM_2                 10
#define VAR_UDEF_NUM_3                 11
#define VAR_UDEF_NUM_4                 12
#define VAR_UDEF_NUM_5                 13
#define VAR_LF_CHECKSUM                14
#define VAR_EZTRIAL_SOFT_BINDING       15
#define VAR_EZTRIAL_HARD_BINDING       16
#define VAR_EZTRIAL_COMPNO_THRESHOLD   17
#define VAR_EZTRIAL_CONVERT_COPIES     18
#define VAR_EZTRIAL_UPDATE_LAST_TIME   19
#define VAR_EZTRIAL_COMPNO_ALGORITHMS  20
#define VAR_EZTRIAL_FILE_VERSION       21
#define VAR_EZTRIG_FLAGS               22
#define VAR_EZTRIG_COMPNO_ALGORITHMS   23
#define VAR_EZTRIG_SEED                24
#define VAR_EZTRIG_REGKEY2SEED         25
#define VAR_EZTRIAL_DAYS_TO_RUN        26
#define VAR_EZTRIAL_TIMES_TO_RUN       27


/* getvar / setvar definitions - date fields */
#define VAR_EXP_DATE_SOFT        1
#define VAR_EXP_DATE_HARD        2
#define VAR_LAST_DATE            3
#define VAR_LAST_TIME            4
#define VAR_UDEF_DATE_1          5
#define VAR_UDEF_DATE_2          6
#define VAR_UDEF_DATE_3          7
#define VAR_UDEF_DATE_4          8
#define VAR_UDEF_DATE_5          9


/* for compatibility with previous versions */
#define VAR_PRODUCT              VAR_UDEF_CHAR_2
#define VAR_DISTRIBUTOR          VAR_UDEF_CHAR_3
#define VAR_USER_DEF_1           VAR_UDEF_CHAR_1
#define VAR_USER_DEF_2           VAR_UDEF_NUM_1
#define VAR_USER_DEF_3           VAR_UDEF_NUM_2
#define VAR_USER_DEF_4           VAR_UDEF_DATE_1

#ifndef PPPEXPORT
#define PPPEXPORT
#endif


/* for the C/DOS library */
#ifndef _WINDOWS

#ifndef LONG
#define LONG long int
#endif
#ifndef LPLONG
#define LPLONG long int *
#endif
#ifndef WINAPI
#define WINAPI pascal
#endif
#ifndef VOID
#define VOID void
#endif
#ifndef LPSTR
#define LPSTR char *
#endif
#ifndef LPDWORD
#define LPDWORD long int *
#endif
#ifndef HINSTANCE
#define HINSTANCE long int
#endif
#ifndef HWND
#define HWND unsigned short
#endif

#endif

#ifndef LIBRARY_CLIPPER
#define PP_ADDDAYS pp_adddays
#define PP_BITCLEAR pp_bitclear
#define PP_BITSET pp_bitset
#define PP_BITTEST pp_bittest
#define PP_CEDATE pp_cedate
#define PP_CHECKSUM pp_checksum
#define PP_CENUM pp_cenum
#define PP_CHKVARCHAR pp_chkvarchar
#define PP_CHKVARDATE pp_chkvardate
#define PP_CHKVARNUM pp_chkvarnum
#define PP_COMPNO pp_compno
#define PP_CONVERTV3 pp_convertv3
#define PP_COPYADD pp_copyadd
#define PP_COPYCHECK pp_copycheck
#define PP_COPYCHECKTH pp_copycheckth
#define PP_COPYDELETE pp_copydelete
#define PP_COPYGET pp_copyget
#define PP_COUNTDEC pp_countdec
#define PP_COUNTINC pp_countinc
#define PP_CTCODES pp_ctcodes
#define PP_DAYSLEFT pp_daysleft
#define PP_DECRYPT pp_decrypt
#define PP_ENCRYPT pp_encrypt
#define PP_ERRORSTR pp_errorstr
#define PP_EXPIRED pp_expired
#define PP_EZTRIAL1 pp_eztrial1
#define PP_EZTRIG1 pp_eztrig1
#define PP_EZTRIG1DLG pp_eztrig1dlg
#define PP_EZTRIG1EX pp_eztrig1ex
#define PP_FILEDELETE pp_filedelete
#define PP_GETCODE pp_getcode
#define PP_GETDATE pp_getdate
#define PP_GETTIME pp_gettime
#define PP_GETVARCHAR pp_getvarchar
#define PP_GETVARDATE pp_getvardate
#define PP_GETVARNUM pp_getvarnum
#define PP_INITLIB pp_initlib
#define PP_LASTDAY pp_lastday
#define PP_LANACTIVE pp_lanactive
#define PP_LANCHECK pp_lancheck
#define PP_LIBTEST pp_libtest
#define PP_LFALIAS pp_lfalias
#define PP_LFCLOSE pp_lfclose
#define PP_LFCOPY pp_lfcopy
#define PP_LFCREATE pp_lfcreate
#define PP_LFDELETE pp_lfdelete
#define PP_LFLOCK pp_lflock
#define PP_LFOPEN pp_lfopen
#define PP_LFUNLOCK pp_lfunlock
#define PP_HDSERIAL pp_hdserial
#define PP_NDECRYPT pp_ndecrypt
#define PP_NDECRYPTX pp_ndecryptx
#define PP_NENCRYPT pp_nencrypt
#define PP_NENCRYPTX pp_nencryptx
#define PP_NPDATE pp_npdate
#define PP_PASSWORD pp_password
#define PP_REDIR pp_redir
#define PP_SEMCLOSE pp_semclose
#define PP_SEMCOUNT pp_semcount
#define PP_SEMOPEN pp_semopen
#define PP_SEMUSED pp_semused
#define PP_SETVARCHAR pp_setvarchar
#define PP_SETVARDATE pp_setvardate
#define PP_SETVARNUM pp_setvarnum
#define PP_TCODE pp_tcode
#define PP_TIMERCHECK pp_timercheck
#define PP_TIMERSTART pp_timerstart
#define PP_TRANSFER pp_transfer
#define PP_UPDDATE pp_upddate
#define PP_VALDATE pp_valdate
#endif

   
/* function prototypes follow */
VOID PPPEXPORT WINAPI PP_ADDDAYS( LPLONG month, LPLONG day, LPLONG year, LONG days );
LONG PPPEXPORT WINAPI PP_BITCLEAR( LPDWORD bit_field, LONG bit_number );
LONG PPPEXPORT WINAPI PP_BITSET( LPDWORD bit_field, LONG bit_number );
LONG PPPEXPORT WINAPI PP_BITTEST( LONG bit_field, LONG bit_number );
VOID PPPEXPORT WINAPI PP_CEDATE( LONG cenum, LPLONG month, LPLONG day, LPLONG year );
LONG PPPEXPORT WINAPI PP_CHECKSUM( LPSTR filename, LPLONG checksum );
LONG PPPEXPORT WINAPI PP_CENUM( VOID );
LONG PPPEXPORT WINAPI PP_CHKVARCHAR( LONG type, LONG var_no );
LONG PPPEXPORT WINAPI PP_CHKVARDATE( LONG type, LONG var_no );
LONG PPPEXPORT WINAPI PP_CHKVARNUM( LONG type, LONG var_no );
LONG PPPEXPORT WINAPI PP_COMPNO( LONG cnotype, LPSTR filename, LPSTR hard_drive );
LONG PPPEXPORT WINAPI PP_CONVERTV3( LONG handle, LPSTR v3_cf, LPSTR v3_cpf, LONG v3_sn );
LONG PPPEXPORT WINAPI PP_COPYADD( LONG handle, LONG flags, LONG value );
LONG PPPEXPORT WINAPI PP_COPYCHECK( LONG handle, LONG action, LONG comp_num );
LONG PPPEXPORT WINAPI PP_COPYCHECKTH( LONG handle, LONG action, LONG comp_num, LONG threshold );
LONG PPPEXPORT WINAPI PP_COPYDELETE( LONG handle, LONG comp_num );
LONG PPPEXPORT WINAPI PP_COPYGET( LONG handle, LONG slot, LPLONG comp_num );
LONG PPPEXPORT WINAPI PP_COUNTDEC( LONG handle, LONG var_no );
LONG PPPEXPORT WINAPI PP_COUNTINC( LONG handle, LONG var_no );
LONG PPPEXPORT WINAPI PP_CTCODES( LONG code, LONG cenum, LONG computer, LONG seed );
LONG PPPEXPORT WINAPI PP_DAYSLEFT( LONG handle, LPLONG daysleft );
VOID PPPEXPORT WINAPI PP_DECRYPT( LPSTR instr, LPSTR pwstr, LPSTR ret );
VOID PPPEXPORT WINAPI PP_ENCRYPT( LPSTR instr, LPSTR pwstr, LPSTR ret );
VOID PPPEXPORT WINAPI PP_ERRORSTR( LONG number, LPSTR buffer );
LONG PPPEXPORT WINAPI PP_EXPIRED( LONG handle );
LONG PPPEXPORT WINAPI PP_EZTRIAL1( LPSTR filename, LPSTR password, LPLONG errorcode, LPLONG lfhandle );
LONG PPPEXPORT WINAPI PP_EZTRIG1( LONG hwnd, LPSTR filename, LPSTR password, LPLONG errorcode );
LONG PPPEXPORT WINAPI PP_EZTRIG1DLG( LONG hwnd, LONG handle, LONG flags, LPSTR labels, LONG usercode1, LONG usercode2, LONG tcseed, LONG regkey2seed, LPLONG tcvalue, LPLONG tcdata );
LONG PPPEXPORT WINAPI PP_EZTRIG1EX( LONG handle, LONG regkey1, LONG regkey2, LONG flags, LONG usercode1, LONG usercode2, LONG tcseed, LONG regkey2seed, LPLONG tcvalue, LPLONG tcdata );
LONG PPPEXPORT WINAPI PP_FILEDELETE( LPSTR filename );
LONG PPPEXPORT WINAPI PP_GETCODE( HWND hwnd, LPSTR str_title, LPSTR str_cenum, LPSTR str_comp, LPSTR str_code );
VOID PPPEXPORT WINAPI PP_GETDATE( LPLONG month, LPLONG day, LPLONG year, LPLONG dayofweek );
VOID PPPEXPORT WINAPI PP_GETTIME( LPLONG hours, LPLONG minutes, LPLONG seconds, LPLONG hseconds );
LONG PPPEXPORT WINAPI PP_GETVARCHAR( LONG handle, LONG var_no, LPSTR buffer );
LONG PPPEXPORT WINAPI PP_GETVARDATE( LONG handle, LONG var_no, LPLONG month_hours, LPLONG day_minutes, LPLONG year_seconds );
LONG PPPEXPORT WINAPI PP_GETVARNUM( LONG handle, LONG var_no, LPLONG value );
VOID WINAPI PP_INITLIB( HINSTANCE hInstance );
VOID PPPEXPORT WINAPI PP_LASTDAY( LONG month, LPLONG day, LONG year );
LONG PPPEXPORT WINAPI PP_LANACTIVE( LONG handle );
LONG PPPEXPORT WINAPI PP_LANCHECK( LONG handle );
LONG PPPEXPORT WINAPI PP_LIBTEST( LONG testnum );
LONG PPPEXPORT WINAPI PP_LFALIAS( LONG handle, LPSTR filename, LONG flags, LONG type, LPSTR password, LPLONG recenthandle );
LONG PPPEXPORT WINAPI PP_LFCLOSE( LONG handle );
LONG PPPEXPORT WINAPI PP_LFCOPY( LONG handle, LPSTR filename, LONG lftype );
LONG PPPEXPORT WINAPI PP_LFCREATE( LPSTR file, LONG flags, LONG lftype, LPSTR password, LONG attrib );
LONG PPPEXPORT WINAPI PP_LFDELETE( LPSTR filename, LONG flags, LONG type, LPSTR password );
LONG PPPEXPORT WINAPI PP_LFLOCK( LONG mem_handle );
LONG PPPEXPORT WINAPI PP_LFOPEN( LPSTR filename, LONG flags, LONG type, LPSTR password, LPLONG ret_handle );
LONG PPPEXPORT WINAPI PP_LFUNLOCK( LONG mem_handle );
LONG PPPEXPORT WINAPI PP_HDSERIAL( LPSTR drive );
LONG PPPEXPORT WINAPI PP_NDECRYPT( LONG number, LONG seed );
LONG PPPEXPORT WINAPI PP_NDECRYPTX( LPSTR buffer, LPLONG value1, LPLONG value2, LPLONG value3, LPLONG value4, LONG seed );
LONG PPPEXPORT WINAPI PP_NENCRYPT( LONG number, LONG seed );
VOID PPPEXPORT WINAPI PP_NENCRYPTX( LPSTR buffer, LONG value1, LONG value2, LONG value3, LONG value4, LONG seed );
VOID PPPEXPORT WINAPI PP_NPDATE( LPLONG month, LPLONG day, LPLONG year, LONG dop );
LONG PPPEXPORT WINAPI PP_PASSWORD( LPSTR instr );
LONG PPPEXPORT WINAPI PP_REDIR( LPSTR drive );
LONG PPPEXPORT WINAPI PP_SEMCLOSE( LONG handle );
LONG PPPEXPORT WINAPI PP_SEMCOUNT( LONG handle, LONG semtype, LPSTR prefix_server, LPSTR name, LPLONG number );
LONG PPPEXPORT WINAPI PP_SEMOPEN( LONG handle, LONG semtype, LPSTR prefix_server, LPSTR name, LPLONG sem_handle );
LONG PPPEXPORT WINAPI PP_SEMUSED( LONG handle, LONG semtype, LPSTR prefix_server, LPSTR name, LPLONG number );
LONG PPPEXPORT WINAPI PP_SETVARCHAR( LONG handle, LONG var_no, LPSTR buffer );
LONG PPPEXPORT WINAPI PP_SETVARDATE( LONG handle, LONG var_no, LONG month_hours, LONG day_minutes, LONG year_seconds );
LONG PPPEXPORT WINAPI PP_SETVARNUM( LONG handle, LONG var_no, LONG value );
LONG PPPEXPORT WINAPI PP_TCODE( LONG number, LONG cenum, LONG computer, LONG seed );
LONG PPPEXPORT WINAPI PP_TIMERCHECK( LONG timestamp, LONG minutes );
LONG PPPEXPORT WINAPI PP_TIMERSTART( VOID );
LONG PPPEXPORT WINAPI PP_TRANSFER( LONG handle, LPSTR filename, LPSTR password, LONG comp_num );
LONG PPPEXPORT WINAPI PP_UPDDATE( LONG handle, LONG flag );
LONG PPPEXPORT WINAPI PP_VALDATE( LONG handle );

#ifdef __cplusplus     /* End of extern "C" { */
}
#endif  /* __cplusplus */

#endif
