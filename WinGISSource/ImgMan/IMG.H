// ///////////////////////////////////////////////////////////////////////////
//
// img.h
//
// GEM IMG file decoder
//
// ImageMan Image Processing Library
//
// Copyright (c) 1993-5 Data Techniques, Inc.
// All Rights Reserved
//
// ///////////////////////////////////////////////////////////////////////////
// $History: IMG.H $
// 
// *****************  Version 4  *****************
// User: Johnd        Date: 3/20/98    Time: 8:47a
// Updated in $/ImageMan 16-32/DILS
// Updated to new Version Info Scheme
// 
// *****************  Version 3  *****************
// User: Johnd        Date: 2/28/96    Time: 2:14p
// Updated in $/ImageMan 16-32/DILS
// Corrected Version info to 5.00
// 
// *****************  Version 2  *****************
// User: Timk         Date: 5/19/95    Time: 1:11a
// Updated in $/ImageMan 16-32/DILS
// Added version information resources
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 1/17/95    Time: 4:03p
// Created in $/ImageMan 16-32/DILS
// ImageMan 4.0 Beta 1

#define	IDS_NOOPEN			1
#define	IDS_READERR			2
#define	IDS_MEMERR			3
#define	IDS_INVHAND			4
#define	IDS_EOF				5
#define	IDS_NOPRINT			6
#define	IDS_BADMAN			7
#define	IDS_BADVER			8
#define	IDS_BADCOMPRESS	10

#define DLL_FILEDESCRIPTION        "ImageMan IMG File Reader\0"
#define DLL_ORIGINALFILENAME       "IMG.DIL\0"
