# Microsoft Developer Studio Generated NMAKE File, Based on ImageMan31.dsp
!IF "$(CFG)" == ""
CFG=ImageMan31 - Win32 Release
!MESSAGE No configuration specified. Defaulting to ImageMan31 - Win32 Release.
!ENDIF 

!IF "$(CFG)" != "ImageMan31 - Win32 Release" && "$(CFG)" != "ImageMan31 - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "ImageMan31.mak" CFG="ImageMan31 - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "ImageMan31 - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "ImageMan31 - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "ImageMan31 - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

ALL : "..\imgman32.dll" "$(OUTDIR)\ImageMan31.bsc"


CLEAN :
	-@erase "$(INTDIR)\CDialogs.obj"
	-@erase "$(INTDIR)\CDialogs.sbr"
	-@erase "$(INTDIR)\COLOR.obj"
	-@erase "$(INTDIR)\COLOR.sbr"
	-@erase "$(INTDIR)\COLOR2.obj"
	-@erase "$(INTDIR)\COLOR2.sbr"
	-@erase "$(INTDIR)\contrast.obj"
	-@erase "$(INTDIR)\contrast.sbr"
	-@erase "$(INTDIR)\Dib.obj"
	-@erase "$(INTDIR)\Dib.sbr"
	-@erase "$(INTDIR)\DIBFUNCS.obj"
	-@erase "$(INTDIR)\DIBFUNCS.sbr"
	-@erase "$(INTDIR)\EFFECTS.obj"
	-@erase "$(INTDIR)\EFFECTS.sbr"
	-@erase "$(INTDIR)\filter.obj"
	-@erase "$(INTDIR)\filter.sbr"
	-@erase "$(INTDIR)\fxblinds.obj"
	-@erase "$(INTDIR)\fxblinds.sbr"
	-@erase "$(INTDIR)\fxblur.obj"
	-@erase "$(INTDIR)\fxblur.sbr"
	-@erase "$(INTDIR)\fxcurtn.obj"
	-@erase "$(INTDIR)\fxcurtn.sbr"
	-@erase "$(INTDIR)\fxmosaic.obj"
	-@erase "$(INTDIR)\fxmosaic.sbr"
	-@erase "$(INTDIR)\fxshape.obj"
	-@erase "$(INTDIR)\fxshape.sbr"
	-@erase "$(INTDIR)\fxwipe.obj"
	-@erase "$(INTDIR)\fxwipe.sbr"
	-@erase "$(INTDIR)\HostInt.obj"
	-@erase "$(INTDIR)\HostInt.sbr"
	-@erase "$(INTDIR)\Imgfx.obj"
	-@erase "$(INTDIR)\Imgfx.sbr"
	-@erase "$(INTDIR)\IMGIO.OBJ"
	-@erase "$(INTDIR)\IMGIO.SBR"
	-@erase "$(INTDIR)\imgman.obj"
	-@erase "$(INTDIR)\imgman.sbr"
	-@erase "$(INTDIR)\Imgx.obj"
	-@erase "$(INTDIR)\Imgx.sbr"
	-@erase "$(INTDIR)\nag.obj"
	-@erase "$(INTDIR)\nag.sbr"
	-@erase "$(INTDIR)\OPTBLK.obj"
	-@erase "$(INTDIR)\OPTBLK.sbr"
	-@erase "$(INTDIR)\Twain.obj"
	-@erase "$(INTDIR)\Twain.sbr"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(OUTDIR)\ImageMan31.bsc"
	-@erase "$(OUTDIR)\imgman32.exp"
	-@erase "$(OUTDIR)\imgman32.lib"
	-@erase "..\imgman32.dll"
	-@erase ".\imgman.res"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /FR"$(INTDIR)\\" /Fp"$(INTDIR)\ImageMan31.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /J /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
RSC_PROJ=/l 0x409 /fo".\imgman.res" /d "NDEBUG" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\ImageMan31.bsc" 
BSC32_SBRS= \
	"$(INTDIR)\CDialogs.sbr" \
	"$(INTDIR)\COLOR.sbr" \
	"$(INTDIR)\COLOR2.sbr" \
	"$(INTDIR)\contrast.sbr" \
	"$(INTDIR)\Dib.sbr" \
	"$(INTDIR)\DIBFUNCS.sbr" \
	"$(INTDIR)\EFFECTS.sbr" \
	"$(INTDIR)\filter.sbr" \
	"$(INTDIR)\fxblinds.sbr" \
	"$(INTDIR)\fxblur.sbr" \
	"$(INTDIR)\fxcurtn.sbr" \
	"$(INTDIR)\fxmosaic.sbr" \
	"$(INTDIR)\fxshape.sbr" \
	"$(INTDIR)\fxwipe.sbr" \
	"$(INTDIR)\HostInt.sbr" \
	"$(INTDIR)\Imgfx.sbr" \
	"$(INTDIR)\IMGIO.SBR" \
	"$(INTDIR)\imgman.sbr" \
	"$(INTDIR)\Imgx.sbr" \
	"$(INTDIR)\nag.sbr" \
	"$(INTDIR)\OPTBLK.sbr" \
	"$(INTDIR)\Twain.sbr"

"$(OUTDIR)\ImageMan31.bsc" : "$(OUTDIR)" $(BSC32_SBRS)
    $(BSC32) @<<
  $(BSC32_FLAGS) $(BSC32_SBRS)
<<

LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /dll /incremental:no /pdb:"$(OUTDIR)\imgman32.pdb" /machine:I386 /def:".\Imgman32.def" /out:"..\imgman32.dll" /implib:"$(OUTDIR)\imgman32.lib" 
DEF_FILE= \
	".\Imgman32.def"
LINK32_OBJS= \
	"$(INTDIR)\CDialogs.obj" \
	"$(INTDIR)\COLOR.obj" \
	"$(INTDIR)\COLOR2.obj" \
	"$(INTDIR)\contrast.obj" \
	"$(INTDIR)\Dib.obj" \
	"$(INTDIR)\DIBFUNCS.obj" \
	"$(INTDIR)\EFFECTS.obj" \
	"$(INTDIR)\filter.obj" \
	"$(INTDIR)\fxblinds.obj" \
	"$(INTDIR)\fxblur.obj" \
	"$(INTDIR)\fxcurtn.obj" \
	"$(INTDIR)\fxmosaic.obj" \
	"$(INTDIR)\fxshape.obj" \
	"$(INTDIR)\fxwipe.obj" \
	"$(INTDIR)\HostInt.obj" \
	"$(INTDIR)\Imgfx.obj" \
	"$(INTDIR)\IMGIO.OBJ" \
	"$(INTDIR)\imgman.obj" \
	"$(INTDIR)\Imgx.obj" \
	"$(INTDIR)\nag.obj" \
	"$(INTDIR)\OPTBLK.obj" \
	"$(INTDIR)\Twain.obj" \
	".\imgman.res"

"..\imgman32.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "ImageMan31 - Win32 Debug"

OUTDIR=.\ImageMan
INTDIR=.\ImageMan
# Begin Custom Macros
OutDir=.\ImageMan
# End Custom Macros

ALL : "$(OUTDIR)\imgman32.dll" "$(OUTDIR)\ImageMan31.bsc"


CLEAN :
	-@erase "$(INTDIR)\CDialogs.obj"
	-@erase "$(INTDIR)\CDialogs.sbr"
	-@erase "$(INTDIR)\COLOR.obj"
	-@erase "$(INTDIR)\COLOR.sbr"
	-@erase "$(INTDIR)\COLOR2.obj"
	-@erase "$(INTDIR)\COLOR2.sbr"
	-@erase "$(INTDIR)\contrast.obj"
	-@erase "$(INTDIR)\contrast.sbr"
	-@erase "$(INTDIR)\Dib.obj"
	-@erase "$(INTDIR)\Dib.sbr"
	-@erase "$(INTDIR)\DIBFUNCS.obj"
	-@erase "$(INTDIR)\DIBFUNCS.sbr"
	-@erase "$(INTDIR)\EFFECTS.obj"
	-@erase "$(INTDIR)\EFFECTS.sbr"
	-@erase "$(INTDIR)\filter.obj"
	-@erase "$(INTDIR)\filter.sbr"
	-@erase "$(INTDIR)\fxblinds.obj"
	-@erase "$(INTDIR)\fxblinds.sbr"
	-@erase "$(INTDIR)\fxblur.obj"
	-@erase "$(INTDIR)\fxblur.sbr"
	-@erase "$(INTDIR)\fxcurtn.obj"
	-@erase "$(INTDIR)\fxcurtn.sbr"
	-@erase "$(INTDIR)\fxmosaic.obj"
	-@erase "$(INTDIR)\fxmosaic.sbr"
	-@erase "$(INTDIR)\fxshape.obj"
	-@erase "$(INTDIR)\fxshape.sbr"
	-@erase "$(INTDIR)\fxwipe.obj"
	-@erase "$(INTDIR)\fxwipe.sbr"
	-@erase "$(INTDIR)\HostInt.obj"
	-@erase "$(INTDIR)\HostInt.sbr"
	-@erase "$(INTDIR)\Imgfx.obj"
	-@erase "$(INTDIR)\Imgfx.sbr"
	-@erase "$(INTDIR)\IMGIO.OBJ"
	-@erase "$(INTDIR)\IMGIO.SBR"
	-@erase "$(INTDIR)\imgman.obj"
	-@erase "$(INTDIR)\imgman.sbr"
	-@erase "$(INTDIR)\Imgx.obj"
	-@erase "$(INTDIR)\Imgx.sbr"
	-@erase "$(INTDIR)\nag.obj"
	-@erase "$(INTDIR)\nag.sbr"
	-@erase "$(INTDIR)\OPTBLK.obj"
	-@erase "$(INTDIR)\OPTBLK.sbr"
	-@erase "$(INTDIR)\Twain.obj"
	-@erase "$(INTDIR)\Twain.sbr"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\ImageMan31.bsc"
	-@erase "$(OUTDIR)\imgman32.dll"
	-@erase "$(OUTDIR)\imgman32.exp"
	-@erase "$(OUTDIR)\imgman32.ilk"
	-@erase "$(OUTDIR)\imgman32.lib"
	-@erase "$(OUTDIR)\imgman32.pdb"
	-@erase ".\imgman.res"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "USELZW" /D "_WINDOWS" /FR"$(INTDIR)\\" /Fp"$(INTDIR)\ImageMan31.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /J /FD /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
RSC_PROJ=/l 0x409 /fo".\imgman.res" /d "_DEBUG" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\ImageMan31.bsc" 
BSC32_SBRS= \
	"$(INTDIR)\CDialogs.sbr" \
	"$(INTDIR)\COLOR.sbr" \
	"$(INTDIR)\COLOR2.sbr" \
	"$(INTDIR)\contrast.sbr" \
	"$(INTDIR)\Dib.sbr" \
	"$(INTDIR)\DIBFUNCS.sbr" \
	"$(INTDIR)\EFFECTS.sbr" \
	"$(INTDIR)\filter.sbr" \
	"$(INTDIR)\fxblinds.sbr" \
	"$(INTDIR)\fxblur.sbr" \
	"$(INTDIR)\fxcurtn.sbr" \
	"$(INTDIR)\fxmosaic.sbr" \
	"$(INTDIR)\fxshape.sbr" \
	"$(INTDIR)\fxwipe.sbr" \
	"$(INTDIR)\HostInt.sbr" \
	"$(INTDIR)\Imgfx.sbr" \
	"$(INTDIR)\IMGIO.SBR" \
	"$(INTDIR)\imgman.sbr" \
	"$(INTDIR)\Imgx.sbr" \
	"$(INTDIR)\nag.sbr" \
	"$(INTDIR)\OPTBLK.sbr" \
	"$(INTDIR)\Twain.sbr"

"$(OUTDIR)\ImageMan31.bsc" : "$(OUTDIR)" $(BSC32_SBRS)
    $(BSC32) @<<
  $(BSC32_FLAGS) $(BSC32_SBRS)
<<

LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /dll /incremental:yes /pdb:"$(OUTDIR)\imgman32.pdb" /debug /machine:I386 /def:".\Imgman32.def" /out:"$(OUTDIR)\imgman32.dll" /implib:"$(OUTDIR)\imgman32.lib" 
DEF_FILE= \
	".\Imgman32.def"
LINK32_OBJS= \
	"$(INTDIR)\CDialogs.obj" \
	"$(INTDIR)\COLOR.obj" \
	"$(INTDIR)\COLOR2.obj" \
	"$(INTDIR)\contrast.obj" \
	"$(INTDIR)\Dib.obj" \
	"$(INTDIR)\DIBFUNCS.obj" \
	"$(INTDIR)\EFFECTS.obj" \
	"$(INTDIR)\filter.obj" \
	"$(INTDIR)\fxblinds.obj" \
	"$(INTDIR)\fxblur.obj" \
	"$(INTDIR)\fxcurtn.obj" \
	"$(INTDIR)\fxmosaic.obj" \
	"$(INTDIR)\fxshape.obj" \
	"$(INTDIR)\fxwipe.obj" \
	"$(INTDIR)\HostInt.obj" \
	"$(INTDIR)\Imgfx.obj" \
	"$(INTDIR)\IMGIO.OBJ" \
	"$(INTDIR)\imgman.obj" \
	"$(INTDIR)\Imgx.obj" \
	"$(INTDIR)\nag.obj" \
	"$(INTDIR)\OPTBLK.obj" \
	"$(INTDIR)\Twain.obj" \
	".\imgman.res"

"$(OUTDIR)\imgman32.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("ImageMan31.dep")
!INCLUDE "ImageMan31.dep"
!ELSE 
!MESSAGE Warning: cannot find "ImageMan31.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "ImageMan31 - Win32 Release" || "$(CFG)" == "ImageMan31 - Win32 Debug"
SOURCE=.\CDialogs.cpp

"$(INTDIR)\CDialogs.obj"	"$(INTDIR)\CDialogs.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\COLOR.cpp

"$(INTDIR)\COLOR.obj"	"$(INTDIR)\COLOR.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\COLOR2.cpp

"$(INTDIR)\COLOR2.obj"	"$(INTDIR)\COLOR2.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\contrast.cpp

"$(INTDIR)\contrast.obj"	"$(INTDIR)\contrast.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\Dib.cpp

"$(INTDIR)\Dib.obj"	"$(INTDIR)\Dib.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\DIBFUNCS.cpp

"$(INTDIR)\DIBFUNCS.obj"	"$(INTDIR)\DIBFUNCS.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\EFFECTS.cpp

"$(INTDIR)\EFFECTS.obj"	"$(INTDIR)\EFFECTS.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\filter.cpp

"$(INTDIR)\filter.obj"	"$(INTDIR)\filter.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\fxblinds.cpp

"$(INTDIR)\fxblinds.obj"	"$(INTDIR)\fxblinds.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\fxblur.cpp

"$(INTDIR)\fxblur.obj"	"$(INTDIR)\fxblur.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\fxcurtn.cpp

"$(INTDIR)\fxcurtn.obj"	"$(INTDIR)\fxcurtn.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\fxmosaic.cpp

"$(INTDIR)\fxmosaic.obj"	"$(INTDIR)\fxmosaic.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\fxshape.cpp

"$(INTDIR)\fxshape.obj"	"$(INTDIR)\fxshape.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\fxwipe.cpp

"$(INTDIR)\fxwipe.obj"	"$(INTDIR)\fxwipe.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\HostInt.cpp

"$(INTDIR)\HostInt.obj"	"$(INTDIR)\HostInt.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\Imgfx.cpp

"$(INTDIR)\Imgfx.obj"	"$(INTDIR)\Imgfx.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\IMGIO.C

"$(INTDIR)\IMGIO.OBJ"	"$(INTDIR)\IMGIO.SBR" : $(SOURCE) "$(INTDIR)"


SOURCE=.\IMGIO.cpp
SOURCE=.\imgman.cpp

"$(INTDIR)\imgman.obj"	"$(INTDIR)\imgman.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\Imgman.rc

".\imgman.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) $(RSC_PROJ) $(SOURCE)


SOURCE=.\Imgx.cpp

"$(INTDIR)\Imgx.obj"	"$(INTDIR)\Imgx.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\nag.cpp

"$(INTDIR)\nag.obj"	"$(INTDIR)\nag.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\OPTBLK.cpp

"$(INTDIR)\OPTBLK.obj"	"$(INTDIR)\OPTBLK.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\Twain.cpp

"$(INTDIR)\Twain.obj"	"$(INTDIR)\Twain.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\Img2dat.rc

!ENDIF 

