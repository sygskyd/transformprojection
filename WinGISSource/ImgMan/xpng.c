/////////////////////////////////////////////////////////////////////////////
//
// xpng.c
//
// ImageMan/X PNG export library
//
// Copyright (c) 1991-5 Data Techniques, Inc.
// All Rights Reserved
//
// ///////////////////////////////////////////////////////////////////////////
// $History: xpng.c $
// 
// *****************  Version 2  *****************
// User: Ericj        Date: 4/29/98    Time: 11:04a
// Updated in $/ImageMan 16-32/DELS
// Changed fname in LEXPORTINFO to MAX_PATH to export files
// longer than 100 characters.
// 
// *****************  Version 1  *****************
// User: Timk         Date: 11/26/96   Time: 7:42a
// Created in $/imageman 16-32/dels
// Initial Checking of PNG Export Code
// 
// *****************  Version 1  *****************
// User: Tec        Date: 2/18/96    Time: 00:00
// Created in $/ImageMan 16-32/DELS
// ImageMan 4.0 Beta 1

#define WIN32_LEAN_AND_MEAN
#ifdef WIN32
#define IMAPI __cdecl
#else
#define IMAPI WINAPI
#endif
#include "internal.h"

#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "optblk.h"
#include "xpng.h"
#include "pngdefs.h"

#ifndef WIN32
#include "windos.h"
#endif

#define OUTBUFSIZE 2048

typedef struct _EXPORTINFO {
	char        fName[MAX_PATH];	// alias (usually filename) of file to export
	LPVOID		lpHandle;	// handle to IO job
	LPIMG_IO		lpIO;			// IO functions	
	png_struct  png;
	png_info   info;
    int (FAR *ImgPngWrite)(png_structp png, LPVOID datap, unsigned long numBytes);
    void (*PngError)(png_structp png, png_const_charp error_msg);
}           EXPORTINFO;

typedef EXPORTINFO FAR *LPEXPORTINFO;

int         FillPNGHead(png_infop info_ptr, png_structp png, LPBITMAPINFO lpDIB, HANDLE hOptBlk, int nColors);
void        set_error(int nIndex, LPCSTR lpFilename);
BOOL grayscale(LPBITMAPINFO lpDIB);

#ifdef WIN32

#ifdef WINGIS // so the file TLSByJHC.h was included to "internal.h"
#define SETERROR(E) bJHCTLSSetValue((DWORD)(E))
#define GETERROR    nJHCTLSGetValue()
#else

// Since we only need to store the error value on a per-thread basis, I'm
// just gonna place it in our DWORD TLS slot.
static DWORD TlsIndex;
#define SETERROR(E) TlsSetValue(TlsIndex,(LPVOID)(DWORD)(E))
#define GETERROR    ((DWORD)TlsGetValue(TlsIndex))
#endif // WINGIS

#else // WIN32
static int  nErrID;				  // id of latest error
#define SETERROR(E) nErrID = (E)        
#define GETERROR    (nErrID)
#endif

HANDLE      hInst;
jmp_buf jmpbuf;
XERRORPROC  XSetError;

#ifdef WIN32
// /////////////////////////////////////////////////////////////////////////
//
// DllMain
//
// This is the Win32 'DllEntryPoint'. It is similar in function to LibMain
// and the WEP in 16-bit land, but is much more thorough. LibMain is
// called whenever a process or thread attaches or detaches to the
// BMP DIL
//
// Entry:
// hinstDLL  - handle to the ImageMan DLL
// fdwReason - notification code (attach/detach)
// DLL_PROCESS_ATTACH
// DLL_PROCESS_DETACH
// DLL_THREAD_ATTACH
// DLL_THREAD_DETACH
// lpvReserved - NULL during dynamic process attach/detach, non-NULL
// during static attach/detach.
// Exit:
// DLL_PROCESS_ATTACH - TRUE for success, FALSE for failure
// all others         - return code ignored
// /////////////////////////////////////////////////////////////////////////

BOOL APIENTRY DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
	switch (fdwReason) {

		// The DLL is attaching to a process, due to LoadLibrary.
		case DLL_PROCESS_ATTACH:
			assert(!lpvReserved);  // must always be loaded via LoadLibrary!

			hInst = hinstDLL;		  // save our identity
#ifndef WINGIS
			// Allocate a "Thread Local Storage" (TLS) index right away.
			if ((TlsIndex = TlsAlloc()) == 0xFFFFFFFF)
				return (FALSE);
#else
			bJHCTLSClear();
#endif
			// Fall thru: Initialize the index for first thread.

		// The attached process created a new thread.
		case DLL_THREAD_ATTACH:
#ifndef WINGIS
			// Initialize the TLS index for this thread... it's actually 'nErrID'
			if (!TlsSetValue(TlsIndex, (LPVOID) 0))
				return (FALSE);
#else
			nJHCTLSAlloc(); // Stamp this thread to be existed
#endif
			break;

		// The thread of the attached process terminates.
		case DLL_THREAD_DETACH:
#ifdef WINGIS
			bJHCTLSFree(); // Free this thread entry
#endif
			break;

		// The DLL is detaching from a process, due to a call to FreeLibrary.
		case DLL_PROCESS_DETACH:
#ifndef WINGIS
			// Release the TLS index.
			TlsFree(TlsIndex);
#else
			bJHCTLSClear();  // Release all the threads error descriptors
#endif
			break;
	}

	return (TRUE);

	UNREFERENCED_PARAMETER(lpvReserved);
}

#else // WIN32

int FAR PASCAL LibMain(hInstance, wDataSeg, wHeapSize, lpCmdLine)
	HANDLE      hInstance;
	WORD        wDataSeg, wHeapSize;
	LPSTR       lpCmdLine;
{
	if (wHeapSize == 0)			  /* no heap indicates fatal error */
		return 0;

	hInst = hInstance;			  // save instance handle

	return 1;
}
#endif

VOID WINAPI WEP(int bSystemExit)
{
	if (bSystemExit) {			  // if exiting Windows entirely

	}
}

LPSTR IMAPI IMXDLget_ext(LPXINITSTRUCT lpInit)
{
	lpInit->lFlags = XF_ALLCOLORS;
	XSetError = lpInit->SetError;

	return (LPSTR) "PNG;PNG Images (*.png)";
}



LPSTR IMAPI IMXDLget_ver(void)
{
	return (LPSTR) "1.00";
}

HANDLE IMAPI IMXDLbegin_write(LPVOID lpOpenInfo, LPIMG_IO lpIO, LPCSTR lpszAlias, LPBITMAPINFO lpDIB, HANDLE hOptBlk, LPXINFO lpInfo)
{
    HANDLE      hImage = NULL;
    HANDLE      hPalette = NULL;
    png_colorp	palettep;
    LPEXPORTINFO lpex;
    unsigned short  nColors;
    long        i;
    LPVOID	lpHandle;
    int ImgPngWrite();
//    int (FAR *ImgPngWrite)(png_structp png, LPVOID datap, unsigned long numBytes);
	
    if (lpIO->open(lpOpenInfo, OF_CREATE, &lpHandle) == IMG_OK) 
    {
	switch (lpDIB->bmiHeader.biBitCount) 
	{
	    case 1:
		nColors = 2;
		break;
    	    case 4:
	    	nColors = 16;
		break;
	    case 8:
	        nColors = 256;
		break;
	    case 24:
	        nColors = 1;
		break;
	}
	// now create and initialize our data structure
	hImage = GlobalAlloc(GHND, sizeof(EXPORTINFO));
	if (hImage) 
	{
            lpex = (LPEXPORTINFO) GlobalLock(hImage);

	    lpex->lpHandle = lpHandle;
            lpex->lpIO = lpIO;

            lpex->ImgPngWrite = ImgPngWrite;

            if( setjmp( /*lpex->png.*/jmpbuf))
            {
                GlobalUnlock(hImage);
                GlobalFree(hImage);
	        set_error(EXP_WRITE_HEAD, lpszAlias);
                return NULL;
            }
            png_set_message_fn( &lpex->png, lpex, lpex->PngError, NULL);
            png_info_init( &lpex->info);
            png_write_init( &lpex->png);

	    // fill in the PNG header info from the OptBlock
	    FillPNGHead( &lpex->info, &lpex->png, lpDIB, hOptBlk, nColors);

            // set write functions
            png_set_write_fn( &lpex->png, lpex, lpex->ImgPngWrite, NULL);

            if(lpDIB->bmiHeader.biClrUsed || lpDIB->bmiHeader.biBitCount < 16)
            { 
              long NumColors;

              if( lpDIB->bmiHeader.biClrUsed != 0)
                NumColors = lpDIB->bmiHeader.biClrUsed;
              else
                NumColors = 1 << lpDIB->bmiHeader.biBitCount;

              lpex->info.valid |= PNG_INFO_PLTE;
              hPalette = GlobalAlloc(GHND, (NumColors * sizeof (png_color)));
  			  palettep = (png_colorp) GlobalLock(hPalette);
              lpex->info.num_palette = (unsigned short) NumColors;
              for( i = 0; i<NumColors; i++)
              {
                palettep[i].red = lpDIB->bmiColors[i].rgbRed;
                palettep[i].green = lpDIB->bmiColors[i].rgbGreen;
                palettep[i].blue = lpDIB->bmiColors[i].rgbBlue;
              }
  			  lpex->info.palette = palettep;
            }

            // write png header
            png_write_info( &lpex->png, &lpex->info);

            png_set_bgr( &lpex->png);
            png_set_swap( &lpex->png);

            _fstrcpy(lpex->fName, lpszAlias);


            png_write_flush( &lpex->png);
            GlobalUnlock(hImage);
            if(hPalette != NULL)
            {
              GlobalUnlock(hPalette);
              GlobalFree( hPalette);
            }
	} 
	else 
	{
		lpIO->close(lpHandle);
	}
    } 
    else 
    {	// unable to open the file
	hImage = NULL;
	set_error(EXP_NOOPEN, lpszAlias);
    }

    lpInfo->lFlags = XF_ALLCOLORS;
    return hImage;
}

int IMAPI   IMXDLwrite_block(HANDLE hImage, int nRows, int nRowSize, LPSTR lpBits, LPXINFO lpInfo)
{
    LPEXPORTINFO lpex;
    int         retval = IMGX_OK;
    long	nOutCnt;
    HPSTR       hpBits;
    long 	lBytes;
    long        i;
    long channels;
    png_bytep row_buf;
    long rowbytes;


    lpex = (LPEXPORTINFO) GlobalLock(hImage);

    lBytes = (LONG) nRowSize *nRows;
    hpBits = (HPSTR) lpBits;

    if ((lpex->info.color_type & 3) == 2)
      channels = 3;
    else
      channels = 1;
    if (lpex->info.color_type & 4)
      channels++;

    if( setjmp( jmpbuf))
    {
        GlobalUnlock(hImage);
        GlobalFree(hImage);
        set_error(EXP_WRITE, lpex->fName);
        return IMGX_ERR;
    }
    if(lpex->info.palette) 
      rowbytes = ROWSIZE(lpex->info.width, lpex->info.bit_depth);
    else
      rowbytes = ROWSIZE(lpex->info.width, (lpex->info.bit_depth * channels));

    //start = (nRows -1) + lpex->info.height - nRows;
    for(i = nRows-1; i > -1; i--)
    {
        nOutCnt = (int) min(0x7fffL, rowbytes);	// write 32K max

        row_buf = hpBits + (i * rowbytes);
        png_write_row(&lpex->png, row_buf);
	lpInfo->lIncCnt += nOutCnt;

        if (lpInfo->lIncCnt > lpInfo->lIncrement) {
            lpInfo->lProcessed += lpInfo->lIncCnt;
	    if (lpInfo->lpfnStat)
	       (*lpInfo->lpfnStat) (0, (int) ((lpInfo->lProcessed * 100) / lpInfo->lTotalBytes), lpInfo->dwUserInfo);
	    lpInfo->lIncCnt = 0;
	}
    }

    GlobalUnlock(hImage);

    return retval;
}

int IMAPI   IMXDLend_write(HANDLE hImage, LPXINFO lpInfo)
{
    LPEXPORTINFO lpex;
    png_text text_chunk;
    char text_key[128];
    char text_comment[255];
    int         retval = IMGX_OK;

    lpex = (LPEXPORTINFO) GlobalLock(hImage);
    // write text chunk
    text_chunk.compression = -1;
    _fstrcpy( text_key, TEXT_KEY);
    text_chunk.key = &text_key[0];
    _fstrcpy( text_comment, TEXT_COMMENT);
    text_chunk.text = &text_comment[0];
    lpex->info.num_text = 1;
    text_chunk.text_length = _fstrlen(text_chunk.text);
    lpex->info.text = &text_chunk;

    png_write_end( &lpex->png, &lpex->info);

    png_write_destroy( &lpex->png);
			lpInfo->lWritten = lpex->lpIO->seek(lpex->lpHandle, 0, SEEK_END);
			lpex->lpIO->close(lpex->lpHandle);

    GlobalUnlock(hImage);
    GlobalFree(hImage);

    return retval;
}

// //////////////////////////////////////////////////////////////////////////
//
// Helper functions
//
// //////////////////////////////////////////////////////////////////////////

int FillPNGHead(png_infop info_ptr, png_structp png_ptr, LPBITMAPINFO lpDIB, HANDLE hOptBlk, int nColors)
{
    char filter_value[255];
    char compress_value[255];
    info_ptr->width = lpDIB->bmiHeader.biWidth;
// TEC, check height for negative
    info_ptr->height = lpDIB->bmiHeader.biHeight;
    if( lpDIB->bmiHeader.biBitCount == 24 && nColors == 1)
        info_ptr->bit_depth = 8; 
    else
        info_ptr->bit_depth = (unsigned char) lpDIB->bmiHeader.biBitCount;

    if(nColors != 1)
    {  // palette present
        if(grayscale(lpDIB))
          info_ptr->color_type = 0;
        else
          info_ptr->color_type = 3;
    }
    else
        info_ptr->color_type = 2;

	// Now we can fill in special fields based on hOptBlk settings
    OptBlkGet( hOptBlk, "PNG_FILTER", filter_value);
    if( _fstrcmp(filter_value, "NONE") == 0 || info_ptr->color_type == 3 ||
        info_ptr->bit_depth < 8)
        png_set_filtering(png_ptr, 0);
    else if( _fstrcmp( filter_value, "SUB") == 0)
        png_set_filtering( png_ptr, 1);
    else if( _fstrcmp( filter_value, "UP") == 0)
        png_set_filtering( png_ptr, 2);
    else if( _fstrcmp( filter_value, "AVERAGE") == 0)
        png_set_filtering( png_ptr, 3);
    else if( _fstrcmp( filter_value, "PAETH") == 0)
        png_set_filtering( png_ptr, 4);
    
    OptBlkGet( hOptBlk, "PNG_COMPRESS", compress_value);
    if( _fstrcmp( compress_value, "NONE") == 0)
    {
        png_set_compression_level( png_ptr, 0);
        png_set_compression_mem_level( png_ptr, 0);
    }
    else if (_fstrcmp( compress_value, "MAXIMUM") == 0)
    {
        png_set_compression_level( png_ptr, 9);
        png_set_compression_level( png_ptr, 9);
    }
    else // set compression to default
    {
        png_set_compression_level( png_ptr, Z_DEFAULT_COMPRESSION);
        png_set_compression_mem_level( png_ptr, 8);
    }
	return 0;
}

BOOL grayscale(LPBITMAPINFO lpDIB)
{
  LPRGBQUAD pColor;
  ULONG i;

  pColor = (LPRGBQUAD) ((LPSTR) lpDIB + (WORD) (lpDIB->bmiHeader.biSize));
  if( lpDIB->bmiHeader.biClrUsed == 0)
    return FALSE;
  for(i=0; i<lpDIB->bmiHeader.biClrUsed; i++)
  {
     if((pColor[i].rgbRed != pColor[i].rgbGreen && pColor[i].rgbRed != pColor[i].rgbBlue))
       return FALSE;
  }
  return TRUE;
}

void        set_error(int nIndex, LPCSTR lpFilename)
{
	XSETERROR(hInst, nIndex, "IMGXBMP.DEL", lpFilename);
}

int ImgPngWrite(png_structp png, LPVOID datap, unsigned long numBytes)
{
    int nOutCnt;
    LPEXPORTINFO lpex;

    nOutCnt = (int) min(0x7fffL, numBytes); // write 32K max
    lpex = (LPEXPORTINFO) png->io_ptr;
    if( lpex->lpIO->write(lpex->lpHandle, (LPSTR) datap, numBytes) != (UINT) nOutCnt) {
       set_error( EXP_WRITE, lpex->fName);
       return IMGX_FILE_ERR;
    }
    return TRUE;
}

void PngError(png_structp png, png_const_charp error_msg)
{
    set_error( EXP_WRITE, "");
    longjmp( jmpbuf, 1);
}
