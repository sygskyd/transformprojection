/////////////////////////////////////////////////////////////////////////////
//
// xbmp.c
//
// ImageMan/X BMP export library
//
// Copyright (c) 1991-5 Data Techniques, Inc.
// All Rights Reserved
//
// ///////////////////////////////////////////////////////////////////////////
// $History: XBMP.C $
// 
// *****************  Version 5  *****************
// User: Erict        Date: 2/17/99    Time: 12:26p
// Updated in $/ImageMan 16-32/DELS
// Added Error handling in begin_write, end_write
// 
// *****************  Version 4  *****************
// User: Ericj        Date: 4/29/98    Time: 11:02a
// Updated in $/ImageMan 16-32/DELS
// Changed fName to MAX_PATH to export files
// longer than 100 characters.
// 
// *****************  Version 3  *****************
// User: Johnd        Date: 12/31/96   Time: 12:11p
// Updated in $/ImageMan 16-32/DELS
// Fixed 'Problem' with the # of bmBitsOffset field. We used to default to
// 256 pal entries instead of the actual #. Some readers didnt like this.
// 
// *****************  Version 2  *****************
// User: Timk         Date: 7/25/95    Time: 10:53p
// Updated in $/ImageMan 16-32/DELS
// Added support for writing using IO pointer blocks.
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 1/18/95    Time: 4:34p
// Created in $/ImageMan 16-32/DELS
// ImageMan 4.0 Beta 1

#define WIN32_LEAN_AND_MEAN
#ifdef WIN32
#define IMAPI __cdecl
#else
#define IMAPI WINAPI
#endif
#include "internal.h"

#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "xbmp.h"

#ifndef WIN32
#include "windos.h"
#endif

#define OUTBUFSIZE 2048

typedef struct _EXPORTINFO {
	char        fName[MAX_PATH];	// alias (usually filename) of file to export
	LPVOID		lpHandle;	// handle to IO job
	LPIMG_IO		lpIO;			// IO functions	
	BITMAPINFOHEADER bmh;
}           EXPORTINFO;

typedef EXPORTINFO FAR *LPEXPORTINFO;

int         FillBMPHead(BITMAPFILEHEADER FAR * pBMP, LPBITMAPINFO lpDIB, HANDLE hOptBlk);
void        set_error(int nIndex, LPCSTR lpFilename);

#ifdef WIN32

#ifdef WINGIS // so the file TLSByJHC.h was included
#define SETERROR(E) bJHCTLSSetValue((DWORD)(E))
#define GETERROR    nJHCTLSGetValue()
#else

#error !**************No WINGIS define*************!
// Since we only need to store the error value on a per-thread basis, I'm
// just gonna place it in our DWORD TLS slot.
static DWORD TlsIndex;
#define SETERROR(E) TlsSetValue(TlsIndex,(LPVOID)(DWORD)(E))
#define GETERROR    ((DWORD)TlsGetValue(TlsIndex))
#endif

#else
static int  nErrID;				  // id of latest error
#define SETERROR(E) nErrID = (E)
#define GETERROR    (nErrID)
#endif

HANDLE      hInst;

XERRORPROC  XSetError;

#ifdef WIN32
// /////////////////////////////////////////////////////////////////////////
//
// DllMain
//
// This is the Win32 'DllEntryPoint'. It is similar in function to LibMain
// and the WEP in 16-bit land, but is much more thorough. LibMain is
// called whenever a process or thread attaches or detaches to the
// BMP DIL
//
// Entry:
// hinstDLL  - handle to the ImageMan DLL
// fdwReason - notification code (attach/detach)
// DLL_PROCESS_ATTACH
// DLL_PROCESS_DETACH
// DLL_THREAD_ATTACH
// DLL_THREAD_DETACH
// lpvReserved - NULL during dynamic process attach/detach, non-NULL
// during static attach/detach.
// Exit:
// DLL_PROCESS_ATTACH - TRUE for success, FALSE for failure
// all others         - return code ignored
// /////////////////////////////////////////////////////////////////////////

BOOL APIENTRY DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
	switch (fdwReason) {

		// The DLL is attaching to a process, due to LoadLibrary.
		case DLL_PROCESS_ATTACH:
			assert(!lpvReserved);  // must always be loaded via LoadLibrary!

			hInst = hinstDLL;		  // save our identity
#ifndef WINGIS
			// Allocate a "Thread Local Storage" (TLS) index right away.

			if ((TlsIndex = TlsAlloc()) == 0xFFFFFFFF)
				return (FALSE);
#else
			bJHCTLSClear();
#endif
			// Fall thru: Initialize the index for first thread.

		// The attached process created a new thread.
		case DLL_THREAD_ATTACH:
#ifndef WINGIS
			// Initialize the TLS index for this thread... it's actually 'nErrID'
			if (!TlsSetValue(TlsIndex, (LPVOID) 0))
				return (FALSE);
#else
			nJHCTLSAlloc(); // Stamp this thread to be existed
#endif
			break;

		// The thread of the attached process terminates.
		case DLL_THREAD_DETACH:
#ifdef WINGIS
			bJHCTLSFree(); // Free this thread entry
#endif
			break;

		// The DLL is detaching from a process, due to a call to FreeLibrary.
		case DLL_PROCESS_DETACH:
#ifndef WINGIS
			// Release the TLS index.
			TlsFree(TlsIndex);
#else
			bJHCTLSClear();  // Release all the threads error descriptors
#endif
			break;
	}

	return (TRUE);

	UNREFERENCED_PARAMETER(lpvReserved);
}

#else // WIN32

int FAR PASCAL LibMain(hInstance, wDataSeg, wHeapSize, lpCmdLine)
	HANDLE      hInstance;
	WORD        wDataSeg, wHeapSize;
	LPSTR       lpCmdLine;
{
	if (wHeapSize == 0)			  /* no heap indicates fatal error */
		return 0;

	hInst = hInstance;			  // save instance handle

	return 1;
}
#endif

VOID WINAPI WEP(int bSystemExit)
{
	if (bSystemExit) {			  // if exiting Windows entirely

	}
}

LPSTR IMAPI IMXDLget_ext(LPXINITSTRUCT lpInit)
{
	lpInit->lFlags = XF_ALLCOLORS | XF_BACKWARD;
	XSetError = lpInit->SetError;

	return (LPSTR) "BMP;Windows Bitmap (*.bmp)";
}

LPSTR IMAPI IMXDLget_ver(void)
{
	return (LPSTR) "1.00";
}

HANDLE IMAPI IMXDLbegin_write(LPVOID lpOpenInfo, LPIMG_IO lpIO, LPCSTR lpszAlias, LPBITMAPINFO lpDIB, HANDLE hOptBlk, LPXINFO lpInfo)
{
	HANDLE      hImage = NULL;
	LPEXPORTINFO lpex;
	BITMAPFILEHEADER bmpHead;
	int         nColors;
	LPVOID		lpHandle;
	
	if (lpIO->open(lpOpenInfo, OF_CREATE, &lpHandle) == IMG_OK) {

		// fill in the BMP header info from the OptBlock
		FillBMPHead(&bmpHead, lpDIB, hOptBlk);

		// now create and initialize our data structure
		hImage = GlobalAlloc(GHND, sizeof(EXPORTINFO));
		if (hImage) {
			lpex = (LPEXPORTINFO) GlobalLock(hImage);

			lpex->lpHandle = lpHandle;
			lpex->lpIO = lpIO;
			lpex->bmh = *((LPBITMAPINFOHEADER) (lpDIB));
			_fstrcpy(lpex->fName, lpszAlias);

			// write out file header
			if (lpIO->write(lpHandle, &bmpHead, sizeof(bmpHead)) != sizeof(bmpHead)) {
				set_error(EXP_WRITE_HEAD, lpszAlias); 
				goto cleanup;
			}

			switch (lpDIB->bmiHeader.biBitCount) {
				case 1:
					nColors = 2;
					break;
				case 4:
					nColors = 16;
					break;
				case 8:
					nColors = 256;
					break;
				case 24:
					nColors = 1;
					break;
			}

			// write out BITMAPINFO struct
			if (lpIO->write(lpHandle, (LPSTR) lpDIB, sizeof(BITMAPINFOHEADER) + sizeof(RGBQUAD) * nColors) != sizeof(BITMAPINFOHEADER) + sizeof(RGBQUAD) * nColors) {
				set_error(EXP_WRITE_HEAD, lpszAlias);
				goto cleanup;
			}
			lpIO->seek(lpHandle, bmpHead.bfOffBits, 0);
			GlobalUnlock(hImage);
		} else {
			lpIO->close(lpHandle);
		}
	} else {							  // unable to open the file

		hImage = NULL;
		set_error(EXP_NOOPEN, lpszAlias);
	}

	lpInfo->lFlags = XF_ALLCOLORS | XF_BACKWARD;
	return hImage;

cleanup:
	lpIO->close(lpHandle);
	GlobalUnlock(hImage); 
	GlobalFree(hImage);
	hImage = NULL;
	
	return hImage;
}

int IMAPI   IMXDLwrite_block(HANDLE hImage, int nRows, int nRowSize, LPSTR lpBits, LPXINFO lpInfo)
{
	LPEXPORTINFO lpex;
	int         retval = IMGX_OK;
	HPSTR       hpBits;
	long        lBytes;
	int         nOutCnt;
 	//
	// since we're outputting a DIB, we don't have to output rows
	// in reverse order - they're pre-reversed...
	//
    if (! hImage)
    	return IMGX_INV_HAND;
    	
	lBytes = (LONG) nRowSize *nRows;
	hpBits = (HPSTR) lpBits;
	lpex = (LPEXPORTINFO) GlobalLock(hImage);
	while (lBytes && (retval == IMGX_OK)) {

		nOutCnt = (int) min(0x7fffL, lBytes);	// write 32K max

		if (lpInfo->lIncrement)
			nOutCnt = (int) min(lpInfo->lIncrement, nOutCnt);

		if (lpex->lpIO->write(lpex->lpHandle, (LPSTR) hpBits, nOutCnt) != (UINT) nOutCnt) {
			retval = IMGX_FILE_ERR;   
			set_error(EXP_WRITE, lpex->fName);
			goto cleanup;
		}
		lBytes -= nOutCnt;
		hpBits += nOutCnt;
		lpInfo->lIncCnt += nOutCnt;

		if (lpInfo->lIncCnt > lpInfo->lIncrement) {
			lpInfo->lProcessed += lpInfo->lIncCnt;
			if (lpInfo->lpfnStat)
				(*lpInfo->lpfnStat) (0, (int) ((lpInfo->lProcessed * 100) / lpInfo->lTotalBytes), lpInfo->dwUserInfo);
			lpInfo->lIncCnt = 0;
		}
	}
	GlobalUnlock(hImage);

	return retval;

cleanup:

	lpex->lpIO->close(lpex->lpHandle);
		
   	GlobalUnlock(hImage);
   	GlobalFree(hImage);
   	hImage = NULL;
   	
   	return retval;
}

int IMAPI   IMXDLend_write(HANDLE hImage, LPXINFO lpInfo)
{
	LPEXPORTINFO lpex;
	int         retval = IMGX_OK;
	long        lFileSize;
      
    if (! hImage)
    	return IMGX_INV_HAND;

      
	lpex = (LPEXPORTINFO) GlobalLock(hImage);

	lFileSize = lpex->lpIO->seek(lpex->lpHandle, 0, 2);
	lpex->lpIO->seek(lpex->lpHandle, 2, 0);
	if ( lpex->lpIO->write(lpex->lpHandle, (LPSTR) & lFileSize, sizeof(long)) != sizeof(long)) {
		retval = IMGX_FILE_ERR;   
		set_error(EXP_WRITE, lpex->fName);
		goto cleanup;
	}
	lpInfo->lWritten = lpex->lpIO->seek(lpex->lpHandle, 0, SEEK_END);
	lpex->lpIO->close(lpex->lpHandle);

	GlobalUnlock(hImage);
	GlobalFree(hImage);
    hImage = NULL;
    
	return retval;

cleanup:

	lpex->lpIO->close(lpex->lpHandle);
		
   	GlobalUnlock(hImage);
   	GlobalFree(hImage);
   	hImage = NULL;
   	
   	return retval;
}

// //////////////////////////////////////////////////////////////////////////
//
// Helper functions
//
// //////////////////////////////////////////////////////////////////////////

int         FillBMPHead(BITMAPFILEHEADER FAR * pBMP, LPBITMAPINFO lpDIB, HANDLE hOptBlk)
{
	int nColors;

	switch (lpDIB->bmiHeader.biBitCount) {
		case 1:
			nColors = 2;
			break;
		case 4:
			nColors = 16;
			break;
		case 8:
			nColors = 256;
			break;
		case 24:
			nColors = 0;
			break;
	}

	pBMP->bfType = 'M' * 256 + 'B';
	pBMP->bfSize = 0;				  // place holder -- filled in after file written

	pBMP->bfReserved1 = 0;
	pBMP->bfReserved2 = 0;
	pBMP->bfOffBits = (DWORD) (sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + (nColors * sizeof(RGBQUAD)));

	// Now we can fill in special fields based on hOptBlk settings

	return 0;
}

void        set_error(int nIndex, LPCSTR lpFilename)
{
	XSETERROR(hInst, nIndex, "IMGXBMP.DEL", lpFilename);
}
