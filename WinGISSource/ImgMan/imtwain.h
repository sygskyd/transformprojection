/////////////////////////////////////////////////////////////////////////////
//
//    IMTWAIN.H
//
//    ImageMan TWAIN Scanner Support Includes
//
//    ImageMan Image Processing Library
//
//    Copyright (c) 1996 Data Techniques, Inc.
//    All Rights Reserved
//
/////////////////////////////////////////////////////////////////////////////
// $History: imtwain.h $
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 7/10/97    Time: 2:04p
// Created in $/imageman 16-32/dll-api level 2
// Initial Lvl 2 Checkin
// 
// *****************  Version 2  *****************
// User: Johnd        Date: 4/09/96    Time: 3:57p
// Updated in $/ImageMan 16-32/DLL
// Added changes to support TWAIN OCX Control
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 2/19/96    Time: 10:26p
// Created in $/ImageMan 16-32/DLL
// Initial Release of New TWAIN Support

#ifndef IMTWAIN_H
#define IMTWAIN_H

#ifndef TWAIN
#include "twain.h"
#endif

#ifdef __cplusplus
extern "C" {            
#endif	

#define WM_IMSCAN		WM_USER+1001
#define WM_IMTWAIN		WM_USER+1002

// TWAIN Scanning API
typedef struct tagTWAINJOB {

	int			nState;					// TWAIN state (per the standard)
	DSMENTRYPROC	pDSM_Entry;			// entry point of Data Source Manager
	HINSTANCE		hDSMLib;			// handle of DSM
	TW_IDENTITY	AppId;
	TW_IDENTITY	SourceId;				// source identity structure
	TW_USERINTERFACE twUI;
	TW_PENDINGXFERS pendingXfers;
	HANDLE		hDib;					// bitmap returned by native transfer
	TW_INT16		rc;					// result code       
	HWND			hwnd;
	TW_IMAGELAYOUT dcLayout;			// Bounding Box Info
	TW_UINT16	nBrightness;
	TW_UINT16	nContrast;
	BOOL			bShowUI;
	BOOL			bUseADF;
	TW_UINT16	nPages;
	TW_UINT16	nPgCount;
	TW_UINT16	nResolution;
	char		SourceDevice[80];		// Name of TWAIN device to use
} TWAINJOB;

typedef TWAINJOB FAR * LPTWAINJOB;
typedef TWAINJOB FAR * HTWAINJOB;


// Low Level Scanning Functions

HTWAINJOB IMAPI ScanLowInitTwainJob( HWND hwnd );	// Returns a HANDLE to a TWAIN Job

void IMAPI ScanLowRegisterApp(	// record application information
	HTWAINJOB	lpJob,
	int		nMajorNum, int nMinorNum,	// major and incremental revision of application. E.g.
													// for version 2.1, nMajorNum == 2 and nMinorNum == 1
	int		nLanguage,						// language of this version (use TWLG_xxx from TWAIN.H)
	int		nCountry,						// country of this version (use TWCY_xxx from TWAIN.H)
	LPSTR	lpszVersion,						// version info string e.g. "1.0b3 Beta release"
	LPSTR	lpszMfg,								// name of manufacturer/developer e.g. "Crazbat Software"
	LPSTR	lpszFamily,							// product family e.g. "BitStomper"
	LPSTR	lpszProduct);						// specific product e.g. "BitStomper Deluxe Pro"


void IMAPI ScanLowSetScanArea( HTWAINJOB lpJob, float left, float top, float right, float bottom );
void IMAPI ScanLowSetBrightnessContrast( HTWAINJOB lpJob, WORD nBrightness, WORD nContrast );
void IMAPI ScanLowSetResolution( HTWAINJOB lpJob, WORD nResolution );

int IMAPI ScanLowCloseJob( HTWAINJOB lpJob );
int IMAPI ScanLowAcquirePages( HTWAINJOB lpJob, int nPages, WORD wPixTypes, int nFlags, HANDLE FAR *lphDIB );

void IMAPI ScanLowSetScanDevice( HTWAINJOB lpJob, LPCSTR lpDeviceName );

VOID BuildUpOneValue (pTW_CAPABILITY pData, TW_UINT16 ItemType, TW_UINT32 Item);


int FAR PASCAL TWAIN_State( HTWAINJOB );
// Returns the TWAIN Protocol State per the spec.

//--------- Lower-level functions for greater control of the TWAIN protocol --------
int FAR PASCAL TWAIN_SetCaps(HTWAINJOB lpJob);

unsigned FAR PASCAL TWAIN_GetResultCode(HTWAINJOB	lpJob);
// Return the result code (TWRC_xxx) from the last triplet sent to TWAIN

unsigned FAR PASCAL TWAIN_GetConditionCode(HTWAINJOB	lpJob);
// If a source is open, return the condition code from the last TWAIN operation (triplet)
// If a source is NOT open, return the condition code of the source manager.

int FAR PASCAL TWAIN_LoadSourceManager(HTWAINJOB	lpJob);
// Finds and loads the Data Source Manager, TWAIN.DLL.
// If Source Manager is already loaded, does nothing and returns TRUE.
// This can fail if TWAIN.DLL is not installed (in the right place), or
// if the library cannot load for some reason (insufficient memory?) or
// if TWAIN.DLL has been corrupted.

int FAR PASCAL TWAIN_OpenSourceManager(HTWAINJOB	lpJob);
// Opens the Data Source Manager, if not already open.
// If the Source Manager is already open, does nothing and returns TRUE.
// This call will fail if the Source Manager is not loaded.

int FAR PASCAL TWAIN_OpenDefaultSource(HTWAINJOB	lpJob);
// This opens the source selected in the Select Source dialog.
// If a source is already open, does nothing and returns TRUE.
// This will fail if the source manager is not loaded and open.

int FAR PASCAL TWAIN_EnableSource(HTWAINJOB	lpJob);
// Enables the open Data Source, if any - this posts the source's user interface
// and allows image acquisition to begin.
// If the source is already enabled, does nothing and returns TRUE.

int FAR PASCAL TWAIN_DisableSource(HTWAINJOB	lpJob);
// Disables the open Data Source, if any.
// This closes the source's user interface.
// If there is not an enabled source, does nothing and returns TRUE.

int FAR PASCAL TWAIN_CloseSource(HTWAINJOB	lpJob);
// Closes the open Data Source, if any.
// If the source is enabled, disables it first.
// If there is not an open source, does nothing and returns TRUE.

int FAR PASCAL TWAIN_CloseSourceManager(HTWAINJOB	lpJob);
// Closes the Data Source Manager, if it is open.
// If a source is open, disables and closes it as needed.
// If the Source Manager is not open, does nothing and returns TRUE.

int FAR PASCAL TWAIN_UnloadSourceManager(HTWAINJOB	lpJob);
// Unloads the Data Source Manager i.e. TWAIN.DLL - releasing
// any associated memory or resources.
// This call will fail if the Source Manager is open, otherwise
// it always succeeds and returns TRUE.


int FAR PASCAL TWAIN_MessageHook(HTWAINJOB	lpJob, LPMSG lpmsg);
// This function detects Windows messages that should be routed
// to an enabled Data Source, and picks them off.  In a full TWAIN
// app, TWAIN_MessageHook is called inside the main GetMessage loop.
// The skeleton code looks like this:  
//		MSG msg;
//  	while (GetMessage((LPMSG)&msg, NULL, 0, 0)) {
//			if (!TWAIN_MessageHook ((LPMSG)&msg)) {
//				TranslateMessage ((LPMSG)&msg);
//				DispatchMessage ((LPMSG)&msg);
//			}
//		} // while


void FAR PASCAL TWAIN_ModalEventLoop(HTWAINJOB	lpJob);
// Run a GetMessage loop until termination, source disable, or image transfer.
//
// Executes exactly the sample code given above for TWAIN_MessageHook, except
// that it terminates as soon as the source is disabled or a transfer completes.
// Used by TWAIN_AcquireNative.


int FAR PASCAL TWAIN_AbortAllPendingXfers(HTWAINJOB lpJob);

int FAR PASCAL TWAIN_NegotiateXferCount(HTWAINJOB lpJob);

int FAR PASCAL TWAIN_NegotiatePixelTypes(HTWAINJOB lpJob, unsigned wPixTypes);


//--------- Lowest-level functions for TWAIN protocol --------


int FAR PASCAL TWAIN_DS(HTWAINJOB lpJob, unsigned long DG, unsigned DAT, unsigned MSG, void FAR *pData);

int FAR PASCAL TWAIN_Mgr(HTWAINJOB lpJob, unsigned long DG, unsigned DAT, unsigned MSG, void FAR *pData);


#ifdef __cplusplus
}
#endif

#endif
