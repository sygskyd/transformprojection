// PPP.h   Class wrapper for Protection Plus
// Header file for Class CPPP
//

#include "windows.h"
#include "keylib.h"

#ifndef WIN32
#include <memory.h>
#include <string.h>
#endif

#ifndef MAX_PATH
#define MAX_PATH 260
#endif

/* getvar / setvar definitions - character fields */
typedef enum
{
    VAR_CHAR_COMPANY = 1,
    VAR_CHAR_NAME,
    VAR_CHAR_ADDRESS1,
    VAR_CHAR_ADDRESS2,
    VAR_CHAR_ADDRESS3,
    VAR_CHAR_PHONE1,
    VAR_CHAR_PHONE2,
    VAR_CHAR_SERIAL_TEXT,
    VAR_CHAR_EXPIRE_TYPE,
    VAR_CHAR_UDEF_CHAR_1,
    VAR_CHAR_UDEF_CHAR_2,
    VAR_CHAR_UDEF_CHAR_3,
    VAR_CHAR_UDEF_CHAR_4,
    VAR_CHAR_UDEF_CHAR_5,
    VAR_CHAR_UDEF_CHAR_6,
    VAR_CHAR_UDEF_CHAR_7,
    VAR_CHAR_UDEF_CHAR_8,
    VAR_CHAR_UDEF_CHAR_9,
    VAR_CHAR_UDEF_CHAR_10
}	enumVarChar;

/* getvar / setvar definitions - numeric fields */
typedef enum
{
    VAR_NUM_SERIAL_NUM = 1,
    VAR_NUM_EXP_COUNT,
    VAR_NUM_EXP_LIMIT,
    VAR_NUM_LAN_COUNT,
    VAR_NUM_LAN_LIMIT,
    VAR_NUM_INSTALL_COUNT,
    VAR_NUM_INSTALL_LIMIT,
    VAR_NUM_AUTHORIZED_COMPS,
	VAR_NUM_HARDRIVE_NO,		//VAR_NUM_UDEF_NUM_1,
    VAR_NUM_UDEF_NUM_2,
    VAR_NUM_UDEF_NUM_3,
    VAR_NUM_UDEF_NUM_4,
    VAR_NUM_UDEF_NUM_5,
    VAR_NUM_LF_CHECKSUM
}  enumVarNum;


/* getvar / setvar definitions - date fields */
typedef enum
{
    VAR_DATE_EXP_DATE_SOFT = 1,
    VAR_DATE_EXP_DATE_HARD,
    VAR_DATE_LAST_DATE,
    VAR_DATE_LAST_TIME,
    VAR_DATE_UDEF_DATE_1,
    VAR_DATE_UDEF_DATE_2,
    VAR_DATE_UDEF_DATE_3,
    VAR_DATE_UDEF_DATE_4,
    VAR_DATE_UDEF_DATE_5
}  enumVarDate;


class CPPP
{
public:

	CPPP();
	~CPPP();

	// Product Unique!! Filename. Must be in 8.3 format.
	void SetLicenseFileName(LPSTR lpName);

	// Either HINSTANCE, LicenseFilePath or IsLicenseFileInWindowsDir
	// must be called.
	// If a license file is not found the product 
	// will be unregistered and these funcitons will return false.
	BOOL SetHINSTANCE(long  hInst); 
	BOOL SetLicenseFilePath(LPSTR lpPath);
	BOOL IsLicenseFileInWindowsDir();
	
	// Product Unique!! Password. Limit of 124 characters
	void SetPassword(LPSTR lpPassword);

	// Product Unique!! Computer Number
	void SetCompNo(LONG lCompNo);
	// Returns NetName ComputerID for Avidium (only usable on Network Server).
	LONG GetCompNo(void);
	// Returns ComputerID for ActiveX controls.
	LONG GetHDNo(void);

	// If hInst is NULL lpLicFileName will be searched for in Windows dir
	BOOL SetAll(long hInst, LPSTR lpLicFileName, LPSTR lpPassword, LONG lpCompNo);

	// Returns License Status.  Will only load license file
	//  if it has not already been loaded once.
	BOOL IsLicensed();

	// Returns Last Error
	void GetErrString(LPSTR lpErr, int nSize);

	// Retreives String Passed in Buffer, Longest buffer is 60 chars.
	BOOL GetVarChar(enumVarChar var_no, LPSTR buffer);

	// Retrieves Long Value
	BOOL GetVarNum(enumVarNum var_no, LPLONG value);
		
	// Retrieves Date passed in 3 LONGS.
	BOOL GetVarDate(enumVarDate var_no, LPLONG month_hours, LPLONG day_minutes, LPLONG year_secs);

	void CheckDate(BOOL bCheck = TRUE);

private:

	char		m_LicenseFileName[MAX_PATH];
	char		m_LicenseFilePath[MAX_PATH];
	HINSTANCE	m_hInst;
	char		m_Password[124]; // PPP limit.
	LPSTR		m_Results;		// Contains the descriptive string
	LONG		m_compID;		// Computer ID from pp_compno() - represents this computer
	LONG		m_lfHandle;		// Memory handle to license file (sample.ini)
	BOOL		m_authorized;	// TRUE if app is authorized( used to enable/disable menu options)
	char		m_ErrString[500];
	BOOL		m_bCheckDate;

	BOOL		DoesFileExist();
	BOOL		LoadLicenseFile();
	
};

typedef CPPP * PCPPP;