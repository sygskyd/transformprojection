# Microsoft Developer Studio Generated NMAKE File, Based on IMPIHost.dsp
!IF "$(CFG)" == ""
CFG=IMPIHost - Win32 Debug
!MESSAGE No configuration specified. Defaulting to IMPIHost - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "IMPIHost - Win32 Release" && "$(CFG)" != "IMPIHost - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "IMPIHost.mak" CFG="IMPIHost - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "IMPIHost - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "IMPIHost - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "IMPIHost - Win32 Release"

OUTDIR=.\.
INTDIR=.\.
# Begin Custom Macros
OutDir=.\.
# End Custom Macros

ALL : "$(OUTDIR)\impihost.dll"


CLEAN :
	-@erase "$(INTDIR)\CPiPL.obj"
	-@erase "$(INTDIR)\dib.obj"
	-@erase "$(INTDIR)\HostMain.obj"
	-@erase "$(INTDIR)\impihost.res"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\impihost.dll"
	-@erase "$(OUTDIR)\impihost.exp"
	-@erase "$(OUTDIR)\impihost.lib"
	-@erase "$(OUTDIR)\impihost.pdb"
	-@erase "$(OUTDIR)\IMPlhost.map"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /MT /W3 /GX /Zi /Ox /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /Fp"$(INTDIR)\IMPIHost.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /J /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32 
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\impihost.res" /d "NDEBUG" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\IMPIHost.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib .\imgman32.lib /nologo /subsystem:windows /dll /incremental:no /pdb:"$(OUTDIR)\impihost.pdb" /map:"$(INTDIR)\IMPlhost.map" /debug /machine:I386 /def:".\IMPIHost.def" /out:"$(OUTDIR)\impihost.dll" /implib:"$(OUTDIR)\impihost.lib" 
DEF_FILE= \
	".\IMPIHost.def"
LINK32_OBJS= \
	"$(INTDIR)\CPiPL.obj" \
	"$(INTDIR)\dib.obj" \
	"$(INTDIR)\HostMain.obj" \
	"$(INTDIR)\impihost.res"

"$(OUTDIR)\impihost.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "IMPIHost - Win32 Debug"

OUTDIR=.\Debug\IMHOST
INTDIR=.\Debug\IMHOST
# Begin Custom Macros
OutDir=.\Debug\IMHOST
# End Custom Macros

ALL : ".\IMHost32.dll" "$(OUTDIR)\IMPIHost.bsc"


CLEAN :
	-@erase "$(INTDIR)\CPiPL.obj"
	-@erase "$(INTDIR)\CPiPL.sbr"
	-@erase "$(INTDIR)\dib.obj"
	-@erase "$(INTDIR)\Dib.sbr"
	-@erase "$(INTDIR)\HostMain.obj"
	-@erase "$(INTDIR)\HostMain.sbr"
	-@erase "$(INTDIR)\impihost.res"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\IMHost32.exp"
	-@erase "$(OUTDIR)\IMHost32.lib"
	-@erase "$(OUTDIR)\IMHost32.pdb"
	-@erase "$(OUTDIR)\IMPIHost.bsc"
	-@erase ".\IMHost32.dll"
	-@erase ".\IMHost32.ilk"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /FR"$(INTDIR)\\" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /J /FD /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32 
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\impihost.res" /d "_DEBUG" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\IMPIHost.bsc" 
BSC32_SBRS= \
	"$(INTDIR)\CPiPL.sbr" \
	"$(INTDIR)\Dib.sbr" \
	"$(INTDIR)\HostMain.sbr"

"$(OUTDIR)\IMPIHost.bsc" : "$(OUTDIR)" $(BSC32_SBRS)
    $(BSC32) @<<
  $(BSC32_FLAGS) $(BSC32_SBRS)
<<

LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib .\imgman32.lib /nologo /subsystem:windows /dll /incremental:yes /pdb:"$(OUTDIR)\IMHost32.pdb" /debug /machine:I386 /def:".\IMPIHost.def" /out:".\IMHost32.dll" /implib:"$(OUTDIR)\IMHost32.lib" 
DEF_FILE= \
	".\IMPIHost.def"
LINK32_OBJS= \
	"$(INTDIR)\CPiPL.obj" \
	"$(INTDIR)\dib.obj" \
	"$(INTDIR)\HostMain.obj" \
	"$(INTDIR)\impihost.res"

".\IMHost32.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("IMPIHost.dep")
!INCLUDE "IMPIHost.dep"
!ELSE 
!MESSAGE Warning: cannot find "IMPIHost.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "IMPIHost - Win32 Release" || "$(CFG)" == "IMPIHost - Win32 Debug"
SOURCE=.\CPiPL.cpp

!IF  "$(CFG)" == "IMPIHost - Win32 Release"


"$(INTDIR)\CPiPL.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "IMPIHost - Win32 Debug"


"$(INTDIR)\CPiPL.obj"	"$(INTDIR)\CPiPL.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\dib.cpp

!IF  "$(CFG)" == "IMPIHost - Win32 Release"


"$(INTDIR)\dib.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "IMPIHost - Win32 Debug"


"$(INTDIR)\dib.obj"	"$(INTDIR)\Dib.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\HostMain.cpp

!IF  "$(CFG)" == "IMPIHost - Win32 Release"


"$(INTDIR)\HostMain.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "IMPIHost - Win32 Debug"


"$(INTDIR)\HostMain.obj"	"$(INTDIR)\HostMain.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\impihost.rc

"$(INTDIR)\impihost.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) $(RSC_PROJ) $(SOURCE)



!ENDIF 

