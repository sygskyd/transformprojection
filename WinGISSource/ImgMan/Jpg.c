/////////////////////////////////////////////////////////////////////////////
//
// jpg.c
//
// JPEG file reader
//
// ImageMan Image Processing Library
//
// Copyright (c) 1991-5 Data Techniques, Inc.
// All Rights Reserved
//
// ///////////////////////////////////////////////////////////////////////////
// $History: JPG.C $
// 
// *****************  Version 9  *****************
// User: Ericj        Date: 11/03/98   Time: 10:30a
// Updated in $/ImageMan 16-32/DILS
// Opening corrupt images from a memory block got stuck
// in endless loop.
// 
// *****************  Version 8  *****************
// User: Johnd        Date: 4/10/98    Time: 5:56p
// Updated in $/ImageMan 16-32/DILS
// Fixed JPG xDens/yDens issue
// 
// *****************  Version 7  *****************
// User: Johnd        Date: 3/20/98    Time: 12:01p
// Updated in $/ImageMan 16-32/DILS
// Added support for getting the xDens/yDens fields
// 
// *****************  Version 6  *****************
// User: Johnd        Date: 8/01/96    Time: 9:49a
// Updated in $/ImageMan 16-32/DILS
// Fixed Bug #394
// 
// *****************  Version 5  *****************
// User: Timk         Date: 1/29/96    Time: 1:11a
// Updated in $/ImageMan 16-32/DILS
// Fixed bugs w/reading portions of an image (set_row problems).
// 
// *****************  Version 4  *****************
// User: Timk         Date: 9/11/95    Time: 11:47a
// Updated in $/ImageMan 16-32/DILS
// Fixed bug in initialization of clamping arrays.
// 
// *****************  Version 3  *****************
// User: Timk         Date: 6/30/95    Time: 12:04p
// Updated in $/ImageMan 16-32/DILS
// Part 1 of massive changes for read/write using function pointer blocks.
// 
// *****************  Version 2  *****************
// User: Timk         Date: 6/09/95    Time: 11:20a
// Updated in $/ImageMan 16-32/DILS
// Updated to use faster look-ahead table for Huffman code extraction. As
// a by-product of this change I had to alter the input stream to be wider
// (16/32 bits) instead of 8.
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 1/17/95    Time: 6:36p
// Created in $/ImageMan 16-32/DILS
// ImageMan 4.0 Beta 1

#include "internal.h"
#pragma hdrstop
#include <assert.h>
#include <stdarg.h>
#include <string.h>
#include <stdio.h>
#include "jpg.h"     // stringtable defines

#define VERSION "JPEG File Reader V1.0"

#ifdef NDEBUG
#undef OutputDebugString
#define OutputDebugString(S) ((void)0)
#endif

#ifdef WIN32

#ifdef WINGIS // so the file TLSByJHC.h was included

#define SETERROR(E) bJHCTLSSetValue((DWORD)(E))
#define GETERROR    nJHCTLSGetValue()

#else

DWORD       TlsIndex;			  // Thread local storage index: for error code
#define SETERROR(E) TlsSetValue(TlsIndex,(LPVOID)(DWORD)(E))
#define GETERROR    ((DWORD)TlsGetValue(TlsIndex))
#endif


#else
int         nErrID;				  // id of latest error
#define SETERROR(E) nErrID = (E)
#define GETERROR    (nErrID)
#endif

// RGB color conversion tables; shared with everyone!
int         rgb_r[256], rgb_b[256], rgb_gr[256], rgb_gb[256];
UCHAR       rgb_clamp[708];


static HINSTANCE jpegInstance;
#if DEBUG
static DEBUGPROC DebugStringOut;
#endif



#ifdef WIN32
// /////////////////////////////////////////////////////////////////////////
//
// DllMain
//
// This is the Win32 'DllEntryPoint'. It is similar in function to LibMain
// and the WEP in 16-bit land, but is much more thorough. LibMain is
// called whenever a process or thread attaches or detaches to the
// BMP DIL
//
// Entry:
// hinstDLL  - handle to the ImageMan DLL
// fdwReason - notification code (attach/detach)
// DLL_PROCESS_ATTACH
// DLL_PROCESS_DETACH
// DLL_THREAD_ATTACH
// DLL_THREAD_DETACH
// lpvReserved - NULL during dynamic process attach/detach, non-NULL
// during static attach/detach.
// Exit:
// DLL_PROCESS_ATTACH - TRUE for success, FALSE for failure
// all others         - return code ignored
// /////////////////////////////////////////////////////////////////////////

BOOL APIENTRY DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
	switch (fdwReason) {
			// The DLL is attaching to a process, due to LoadLibrary.
			case DLL_PROCESS_ATTACH:
			assert(!lpvReserved);  // must always be loaded via LoadLibrary!

//			OutputDebugString("IM2JPG: Process++\n");

			jpegInstance = hinstDLL;	// save our identity

			{
				int         i;

				// initialize the color conversion tables - there's 4 of 'em.
				for (i = 0; i<256; i++) {
					rgb_r[i] = (int) (1.40200 * (i - 128));
					rgb_b[i] = (int) (1.77200 * (i - 128));
					rgb_gr[i] = (int) (-0.71414 * (i - 128));
					rgb_gb[i] = (int) (-0.34414 * (i - 128));
				}
				// initialize the color clamping table. The maximum range is
				// produced by the blue pixel, where Y+rgb_b[Cb] is in [-226..480].
				memset(rgb_clamp, 255, sizeof(rgb_clamp));	// 256..480 is 255

				memset(rgb_clamp, 0, RGBCLAMP_OFFSET);	// -226..-1 is 0

				for (i = 256; i--;)
					rgb_clamp[i + RGBCLAMP_OFFSET] = i;	// 0..255 identity

			}

#ifndef WINGIS
			// Allocate a "Thread Local Storage" (TLS) index right away.
			if ((TlsIndex = TlsAlloc()) == 0xFFFFFFFF)
				return (FALSE);
#else
			bJHCTLSClear();
#endif
			// Fall thru: Initialize the index for first thread.

			// The attached process created a new thread.
		case DLL_THREAD_ATTACH:
#ifndef WINGIS
			// Initialize the TLS index for this thread... it's actually 'nErrID'
			OutputDebugString("IM2JPG: Thread++\n");
			if (!TlsSetValue(TlsIndex, (LPVOID) 0))
				return (FALSE);
#else
			nJHCTLSAlloc(); // Stamp this thread to be existed
#endif
			break;

			// The thread of the attached process terminates.
		case DLL_THREAD_DETACH:
//			OutputDebugString("IM2JPG: Thread--\n");
#ifdef WINGIS
			bJHCTLSFree(); // Free this thread entry
#endif
			break;

			// The DLL is detaching from a process, due to a call to FreeLibrary.
		case DLL_PROCESS_DETACH:
#ifndef WINGIS
			OutputDebugString("IM2JPG: Process--\n");
			// Release the TLS index.
			TlsFree(TlsIndex);
#else
			bJHCTLSClear();
#endif
			break;
	}
	OutputDebugString("IM2JPG: OK.\n");
	return (TRUE);

	UNREFERENCED_PARAMETER(lpvReserved);
}

#else // WIN32

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int WINAPI  LibMain(HINSTANCE hInstance, WORD wDataSeg, WORD wHeapSize, LPSTR lpCmdLine)
{
	int         i;

	if (!wHeapSize)
		return (0);					  // no heap indicates fatal error

#if DEBUG
	{
		HANDLE      hMod = GetModuleHandle("IMGMAN11.DLL");
		if (hMod)
			DebugStringOut = (DEBUGPROC) GetProcAddress(hMod, "DebugStringOut");
	}
#endif

	jpegInstance = hInstance;

	// initialize the color conversion tables - there's 4 of 'em.
	for (i = 0; i < 256; i++) {
		rgb_r[i] = (int) (1.40200 * (i - 128));
		rgb_b[i] = (int) (1.77200 * (i - 128));
		rgb_gr[i] = (int) (-0.71414 * (i - 128));
		rgb_gb[i] = (int) (-0.34414 * (i - 128));
	}
	// initialize the color clamping table. The maximum range is
	// produced by the blue pixel, where Y+rgb_b[Cb] is in [-226..480].
	memset(rgb_clamp, 255, sizeof(rgb_clamp));	// 256..480 is 255

	memset(rgb_clamp, 0, RGBCLAMP_OFFSET);	// -226..-1 is 0

	for (i = 256; i--;)
		rgb_clamp[i + RGBCLAMP_OFFSET] = i;	// 0..255 identity

	return (1);
}
#endif // WIN32

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
void WINAPI WEP(int bSystemExit)
{
	return;
	UNREFERENCED_PARAMETER(bSystemExit);
}

//------------------------------------------------------------------

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
BOOL IMAPI  IMDLverify_img(LPSTR lpVerfBuf, LPVOID lpHandle, LPIMG_IO lpIO)
{
	return (*((LPWORD) lpVerfBuf) == M_SOI);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
LPSTR IMAPI IMDLget_ver(void)
{
	return (LPSTR) (VERSION);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLescape(HANDLE hImage, int nInCnt, LPVOID lpIn, LPVOID lpOut)
{
	return IMG_NSUPPORT;
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLinit_image(LPCSTR lpOpenInfo, LPHANDLE hFile, LPVOID lpHandle, long offset, long lLen, LPIMG_IO lpIO)
{
	FileStruct FAR *fs;
	int         retval;
	JPEGINFO    jpeg;
	int			i;

	// Init table pointers to NULL
	for (i = 0; i < 4; i++) {
		jpeg.huff[i].huffval = NULL;
		jpeg.huff[i].huffcode = NULL;
	}

	// get the JPG header info, including all header & frame markers
	lpIO->seek(lpHandle, offset, SEEK_SET);	
	if ((retval = DecodeJPEGHeader(lpHandle, lpIO, &jpeg)) == IMG_OK) {
		if (!(*hFile = GlobalAlloc(DLLHND, sizeof(FileStruct)))) {
			lpIO->close(lpHandle);
			SETERROR(IDS_MEMERR);
			return IMG_NOMEM;
		}
		// fill in structure
		fs = (FileStruct FAR *) GlobalLock(*hFile);

		fs->jpeg = jpeg;
		fs->lpOpenInfo = lpOpenInfo;
		fs->lpHandle = lpHandle;
		fs->lpIO = lpIO;
		fs->offset = offset;
		fs->nCurRow = 0;
		fs->nCurBlk = -1;
		fs->wbufpos = WBUFSIZE;
		fs->nErr = 0;

		GlobalUnlock(*hFile);
	} else {
		lpIO->close(lpHandle);
	}

	return retval;
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLset_ext(HGLOBAL hFile, LPSTR ext)
{
	DEBUGOUT(DILAPI, "\t%s-->set_ext(%d,%s)", (LPSTR) __FILE__, hFile, (LPSTR) ext);
	// no extensions to differentiate between - just return OK
	return IMG_OK;
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLclose_image(HGLOBAL hFile)
{
	FileStruct FAR *fs;
	int         i;

	DEBUGOUT(DILAPI, "\t%s-->close_image(%d)", (LPSTR) __FILE__, hFile);
	// check to see if file is still open - if it is, close it
	if (!(fs = (FileStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}
	if (fs->lpHandle) {
		fs->lpIO->close(fs->lpHandle);
	}

	for (i = 0; i < fs->jpeg.nComponents; i++) {
		if (fs->jpeg.npComponents[i].lpSampsBase)
			GlobalFreePtr(fs->jpeg.npComponents[i].lpSampsBase);
		if (fs->jpeg.npComponents[i].lpUpSamps)
			GlobalFreePtr(fs->jpeg.npComponents[i].lpUpSamps);
	}
	if (fs->jpeg.npComponents)
		LocalFree((HLOCAL) fs->jpeg.npComponents);

	for (i = 0; i < 4; i++) {
		if( fs->jpeg.huff[i].huffval )
			LocalFree((HLOCAL) fs->jpeg.huff[i].huffval);

		if( fs->jpeg.huff[i].huffcode )
			LocalFree((HLOCAL) fs->jpeg.huff[i].huffcode);
	}

	GlobalUnlock(hFile);
	GlobalFree(hFile);
	return IMG_OK;
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLimg_name(HGLOBAL hFile, LPHANDLE lpHand)
{
	FileStruct FAR *fs;

	if (!(fs = (FileStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}
//	*lpHand = fs->fName;

	GlobalUnlock(hFile);
	return IMG_OK;
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLopen(HGLOBAL hFile, LPVOID lpHandle)
{
	FileStruct FAR *fs;
	int         j;

	if (!(fs = (FileStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}
	if (!fs->lpHandle) {
		if (fs->lpIO->open(fs->lpOpenInfo, OF_READ, &(fs->lpHandle)) != IMG_OK) {
			GlobalUnlock(hFile);
			SETERROR(IDS_NOOPEN);
			return (IMG_NOFILE);
		}
	}
	fs->lpIO->seek(fs->lpHandle, fs->jpeg.lOffset, SEEK_SET);
	fs->jpeg.nCodes = 0;
	fs->jpeg.nBit = 0;
	fs->nCurRow = 0;
	fs->nCurBlk = -1;
	fs->wbufpos = WBUFSIZE;

	for (j = fs->jpeg.nComponents; j--;) {
		fs->jpeg.npComponents[j].lastdc = 0;
	}

	GlobalUnlock(hFile);
	return IMG_OK;
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLclose_keep(HGLOBAL hFile)
{
	FileStruct FAR *fs;

	DEBUGOUT(DILAPI, "\t%s-->close_keep(%d)", (LPSTR) __FILE__, hFile);
	if (!(fs = (FileStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}
	if (fs->lpHandle) {
		fs->lpIO->close(fs->lpHandle);
		fs->lpHandle = NULL;
	}
	GlobalUnlock(hFile);
	return IMG_OK;
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLread_rows(HGLOBAL hFile, LPSTR buf, UINT rows, UINT rowSize,
								              UINT start, LPSTATUS lpStat)
{
	FileStruct FAR *fs;
	HPSTR       tmpBuf = (HPSTR) buf;
	UINT        nMCURows;		  // nMCURows is the # of rows in an MCU row

	int         i;

	DEBUGOUT(DILAPI, "\t%s-->%s(%d, %x:%x, %u, %u, %u, %x:%x)", (LPSTR) __FILE__, (LPSTR) "read_rows", hFile, HIWORD(buf), LOWORD(buf), rows, rowSize, start, HIWORD(lpStat), LOWORD(lpStat));
	if (!(fs = (FileStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}
	tmpBuf += (rows - 1) * (ULONG) rowSize;
	nMCURows = fs->jpeg.nMaxVert * DCTSIZE;

	// before we go off and read some new stuff, we may have old stuff that
	// we've already read but haven't used.
	if ((fs->nCurRow/nMCURows) == fs->nCurBlk) {
		// fs->rowSkip = nMCURows - (fs->nCurRow % nMCURows);
		i = (fs->nCurRow % nMCURows);	// # of rows to skip in current band

		UpSample(&fs->jpeg);		  // this will upsample all the components at once...
		i = OutputActualPixels(tmpBuf, &fs->jpeg, rowSize, rows, i, start, rowSize);

		tmpBuf -= (long) rowSize *i;
		fs->nCurRow += i;
		rows -= i;

		// update status of our read operation
		lpStat->lAcc += (long) rowSize * i;
		if (lpStat->lpfnStat && lpStat->lAcc > lpStat->lInterval) {
			lpStat->lDone += lpStat->lAcc;
			lpStat->lDone = min(lpStat->lDone, lpStat->lTotal);

			if( !((*lpStat->lpfnStat) (lpStat->hImage, (int) ((lpStat->lDone * 100) / lpStat->lTotal), lpStat->dwUserInfo) )) {
				GlobalUnlock( hFile );
				return IMG_CANCELLED;
			}
			lpStat->lAcc %= lpStat->lInterval;
		}
		// tmpBuf -= (long)rowSize * min(rows, nMCURows - fs->rowSkip);
		// fs->nCurRow += min(rows, nMCURows - fs->rowSkip);
		// rows -= min(rows, nMCURows - fs->rowSkip);
		// fs->rowSkip = 0;
	}
	while (rows) {
		for (i = fs->jpeg.nComponents; i--;) {
			fs->jpeg.npComponents[i].lpSamps = fs->jpeg.npComponents[i].lpSampsBase;
		}

		// get a row of MCUs - this should be enough data for nMCURows
		// (probably 16) rows of actual output data (yippee!)
		for (i = fs->jpeg.nMCUsPerRow; i--;) {
			GetAnMCU(fs);
		}
		fs->nCurBlk++;

		// now upsample the row we just got -- for now, we're just using the
		// current MCU row and doing linear interpolation within it, assuming
		// zero for the rows above & below -- later, we'll probably need to
		// fix this, but it looks like a real mess...
		UpSample(&fs->jpeg);		  // this will upsample all the components at once...

		// OK, now we're ready to do the output of the pixel values.
		// CAR: 'rowSkip' is always zero!
		i = OutputActualPixels(tmpBuf, &fs->jpeg, rowSize, rows, 0, start, rowSize);

		tmpBuf -= (long) rowSize *i;
		// tmpBuf -= (long)rowSize * (nMCURows - fs->rowSkip);
		fs->nCurRow += i;			  // min(nMCURows - fs->rowSkip, rows);

		rows -= i;					  // min(rows, (nMCURows - fs->rowSkip));
		// fs->rowSkip = 0;

		// update status of our read operation
		lpStat->lAcc += (long) rowSize * i;
		if (lpStat->lpfnStat && lpStat->lAcc > lpStat->lInterval) {
			lpStat->lDone += lpStat->lAcc;
			lpStat->lDone = min(lpStat->lDone, lpStat->lTotal);

			if( !((*lpStat->lpfnStat) (lpStat->hImage, (int) ((lpStat->lDone * 100) / lpStat->lTotal), lpStat->dwUserInfo) )) {
				GlobalUnlock( hFile );
				return IMG_CANCELLED;
			}
			lpStat->lAcc %= lpStat->lInterval;
		}
	}

	assert(tmpBuf + rowSize == buf);

	GlobalUnlock(hFile);
	return IMG_OK;
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLset_row(HGLOBAL hFile, UINT row)
{
	FileStruct FAR *fs;
	UINT        nMCURows;
	int         i, nNewBlk;

	DEBUGOUT(DILAPI, "\t%s-->set_row(%d,%d)", (LPSTR) __FILE__, hFile, row);
	if (!(fs = (FileStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}
	nMCURows = fs->jpeg.nMaxVert * DCTSIZE;

	// when we get called here, we're either at the start of the image
	// or we've read some part of the image (fs->nCurRow != 0)
	//
	// What we need to do is determine the # of rows *past* nCurRow we
	// need to read to get to the proper row.
	//
	nNewBlk = (row/nMCURows);
	if (nNewBlk < fs->nCurBlk) {
		fs->lpIO->seek(fs->lpHandle, fs->jpeg.lOffset, SEEK_SET);
		fs->nCurRow = 0;
		fs->nCurBlk = -1;
	}

	// OK, we need to be careful here. It's possible that row is within
	// the currently held buffers. In that case, just increment nCurRow to
	// row and we're all set. If row is past the current buffers, set nCurRow
	// to the start of the next MCU block and start reading MCU rows 'till
	// we've got row in the buffers.

	if (nNewBlk == fs->nCurBlk) {	//the requested row is already in memory
		fs->nCurRow = row;
	} 
	else {							  // we'll have to do some decoding to get there...
		while (fs->nCurBlk < nNewBlk) {
			for (i = 0; i < fs->jpeg.nComponents; i++) {
				fs->jpeg.npComponents[i].lpSamps = fs->jpeg.npComponents[i].lpSampsBase;
			}

			// get a row of MCUs - this should be enough data for lpjpeg->nMaxVert * DCTSIZE
			// (probably 16) rows of actual output data (yippee!)
			for (i = 0; i < fs->jpeg.nMCUsPerRow; i++) {
				GetAnMCU(fs);
			}
			fs->nCurBlk++;
		}
		fs->nCurRow = row;
	}

	GlobalUnlock(hFile);
	return IMG_OK;
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLbuildDIBhead(HGLOBAL hFile, INTERNALINFO FAR * lpii)
{
	LPBITMAPINFOHEADER lpbi;
	LPBITMAPINFO lpbi2;
	BITMAPINFOHEADER bi;
	int         nDIBits, i;
	FileStruct FAR *fs;
	long        wid, hi;
	HANDLE      hMem;
	int         nStructSize;

	DEBUGOUT(DILAPI, "\t%s-->buildDIBhead(%d)", (LPSTR) __FILE__, hFile);
	if (!(fs = (FileStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}
	wid = fs->jpeg.nWidth;
	hi = fs->jpeg.nHeight;
	if (fs->jpeg.nComponents == 1) {
		nDIBits = 8;
		nStructSize = sizeof(BITMAPINFOHEADER) + sizeof(RGBQUAD) * 256;
	} else {
		nDIBits = 24;
		nStructSize = sizeof(BITMAPINFOHEADER);
	}

	bi.biSize = sizeof(BITMAPINFOHEADER);
	bi.biWidth = wid;
	bi.biHeight = hi;
	bi.biPlanes = 1;				  // Always

	bi.biBitCount = nDIBits;
	bi.biCompression = BI_RGB;
	bi.biSizeImage = ((wid * nDIBits + 31) / 32 * 4) * hi;
	// DensUnit  = 1 - DPI, 2 - DPCM, 0 - Pixels ie no res
	if( fs->jpeg.DensUnit == 1 ) {
		bi.biXPelsPerMeter = (long)(fs->jpeg.xDens * 39.37);
		bi.biYPelsPerMeter = (long)(fs->jpeg.yDens * 39.37);
	} else {
		bi.biXPelsPerMeter = (long)(fs->jpeg.xDens * 100);
		bi.biYPelsPerMeter = (long)(fs->jpeg.yDens * 100);
	}
	bi.biClrUsed = 0;
	bi.biClrImportant = 0;

	lpii->lOrgWid = wid;
	lpii->lOrgHi = hi;
	lpii->bFlags = 0;

	if ((hMem = GlobalAlloc(GHND, nStructSize)) == NULL) {
		SETERROR(IDS_MEMERR);
		return IMG_NOMEM;
	}
	lpbi = (LPBITMAPINFOHEADER) GlobalLock(hMem);
	*(LPBITMAPINFOHEADER) lpbi = bi;
	// fill in the palette info if it's a grayscale image
	if (fs->jpeg.nComponents == 1) {
		lpbi2 = (LPBITMAPINFO) lpbi;
		for (i = 0; i < 256; i++) {
			lpbi2->bmiColors[i].rgbBlue = lpbi2->bmiColors[i].rgbRed = lpbi2->bmiColors[i].rgbGreen = i;
			lpbi2->bmiColors[i].rgbReserved = 0;
		}
	}
	lpii->lpbi = (LPBITMAPINFO) lpbi;

	GlobalUnlock(hFile);
	return IMG_OK;
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLprint(HGLOBAL hFile, HDC hPrnDC, LPRECT lpDest, LPRECT lpSrc)
{
	return (0);
	UNREFERENCED_PARAMETER(hFile);
	UNREFERENCED_PARAMETER(hPrnDC);
	UNREFERENCED_PARAMETER(lpDest);
	UNREFERENCED_PARAMETER(lpSrc);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLrender(HGLOBAL hFile, HDC hDC, LPRECT lpDest, LPRECT lpSrc)
{
	return (0);
	UNREFERENCED_PARAMETER(hFile);
	UNREFERENCED_PARAMETER(hDC);
	UNREFERENCED_PARAMETER(lpDest);
	UNREFERENCED_PARAMETER(lpSrc);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLerror_string(LPSTR lpszBuf, int nMaxLen)
{
	LoadString(jpegInstance, GETERROR, lpszBuf, nMaxLen);
	return IMG_OK;
}


// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
// wgetc
// acts like getc(), just uses your own buffer and counter
int         wgetc(FileStruct FAR * fs, int bSkipZero)
{
	int	retval;

	if (fs->wbufpos >= WBUFSIZE) {

		//fs->lpIO->read(fs->lpHandle, fs->wbuf, WBUFSIZE);
		//fs->wbufpos = 0;

		if ((fs->lpIO->read(fs->lpHandle, fs->wbuf, WBUFSIZE) == 0)) {
			fs->nErr = IMG_FILE_ERR; 
			return 0;
		} else {
			fs->wbufpos = 0;
		}
	} 
	retval = fs->wbuf[fs->wbufpos++];

	//
	// If bSkipZero is true, then skip the zero byte following the 0xff
	//
	if (bSkipZero && (retval == 0xff)) {
		if (fs->wbufpos >= WBUFSIZE) {

			//fs->lpIO->read(fs->lpHandle, fs->wbuf, WBUFSIZE);
			//fs->wbufpos = 0;

			if ((fs->lpIO->read(fs->lpHandle, fs->wbuf, WBUFSIZE) == 0)) {
				fs->nErr = IMG_FILE_ERR; 
				return 0;
			} else {
				fs->wbufpos = 0;
			}

		}
		if (fs->wbuf[fs->wbufpos] == 0) fs->wbufpos++;
	}
	return retval;
}

#if DEBUG
void __cdecl FAR Debug(UINT wCategory, LPSTR lpFormat,...)
{
	LPSTR       vaArgs;

	if (DebugStringOut) {
		va_start(vaArgs, lpFormat);
		(*DebugStringOut) (wCategory, lpFormat, vaArgs);
		va_end(vaArgs);
	}
}
#else
void __cdecl FAR Debug(UINT wCategory, LPSTR lpFormat,...)
{
	UNREFERENCED_PARAMETER(wCategory);
	UNREFERENCED_PARAMETER(lpFormat);
}
#endif

// Define our own assert 'cause MSVC's doesn't work correctly!
#ifndef NDEBUG
#include <stdlib.h>
void __cdecl _assert(void *e, void *f, unsigned l)
{
	char        mystr[256];

	wsprintf(mystr, "Assertion: (%s) failed at %s::%d.\n", e, f, l);
	OutputDebugString(mystr);
	abort();
}
#endif
