/////////////////////////////////////////////////////////////////////////////
//
// gif.cpp
//
// GIF File DIL
//
// ImageMan Image Processing Library
//
// Copyright (c) 1991-1997 Data Techniques, Inc.
// All Rights Reserved
//
/////////////////////////////////////////////////////////////////////////////
// $History: GIF.C $
// 
// *****************  Version 9  *****************
// User: Rusha        Date: 6/29/00    Time: 11:34a
// Updated in $/ImageMan 16-32/DILS
// Added support for  the Application Control block
// 
// *****************  Version 8  *****************
// User: Ericw        Date: 8/10/98    Time: 5:21p
// Updated in $/ImageMan 16-32/DILS
// Fixed bug #126.  GIFs would no longer load because the fileio_read was
// returning the count of bytes requested not the actual bytes read.
// 32bit only.  Attempting to open a GIF would hang the system.
// Also added super tight error checking in the frame loading section.
// 
// *****************  Version 7  *****************
// User: Ericw        Date: 3/19/98    Time: 10:33a
// Updated in $/ImageMan 16-32/DILS
// Now read each individual color table.  This would cause palette
// problems for subsequent images using different palettes.  Now you can
// intermix 16 & 256 color images interlace and non-interlaced as well.
// 
// *****************  Version 6  *****************
// User: Ericw        Date: 1/26/98    Time: 11:55a
// Updated in $/ImageMan 16-32/DILS
// Fixed bug loading an image if there was a comment block containing
// A comma before the first image descriptor was found.  Also has better
// IO error handling during the load of a multi-frame file.
// 
// *****************  Version 5  *****************
// User: Ericw        Date: 11/26/97   Time: 9:38a
// Updated in $/ImageMan 16-32/DILS
// Implemented animated GIF support as multi-page files just the
// multi-page tifs and set the version numbers to 6.0.
// 
// *****************  Version 4  *****************
// User: Timk         Date: 2/20/97    Time: 6:10p
// Updated in $/ImageMan 16-32/DILS
// Reset pass counter so a GIF image can be reloaded properly after an
// ImgUnload.
// 
// *****************  Version 3  *****************
// User: Timk         Date: 10/28/96   Time: 2:04p
// Updated in $/ImageMan 16-32/DILS
// Added support for interlaced images. Cool.
// 
// *****************  Version 2  *****************
// User: Timk         Date: 6/30/95    Time: 12:04p
// Updated in $/ImageMan 16-32/DILS
// Part 1 of massive changes for read/write using function pointer blocks.
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 1/17/95    Time: 4:03p
// Created in $/ImageMan 16-32/DILS
// ImageMan 4.0 Beta 1

#include "internal.h"
#pragma hdrstop
#include <assert.h>
#include <stdarg.h>
#include <string.h>
#include <stdio.h>

#include "gif.h"     // stringtable defines
#include "lzw.h"

#define USEGROOVYREAD 1

#define VERSION "GIF File Reader V1.0"

#if USEGROOVYREAD
#define WBUFSIZE 4096
#define WBUFXTRA 256
#else
#define WBUFSIZE 256   // maximum GIF data block size is 255
#define WBUFXTRA sizeof(UINT)
#endif

typedef struct tagImageStruct {
	LPVOID		lpOpenInfo;
	LPVOID		lpHandle;
	LPIMG_IO		lpIO;
	long        offset;			  // offset of image in file
	long        len;
	long        dataOffset;		  // Offset of raster data
	BOOL				bInterlaced;		//TRUE if this is an interlaced image
	WORD				wDelay ;
	WORD				wCurRow;				// next row to read in image
	WORD				wPass;					// current interlace pass
	WORD				wRowSkip;			//# of rows to skip before we get to the one we want -- interlaced only
	LPLZWC      lpLZW;
	PGIFFRAME   pTopFrame ;
	PGIFFRAME   pCurFrame ;
	PGIFFRAME   pLastFrame ;
	unsigned    nPageCount ;
	unsigned    nCurPage ;
	int         BytesLine;
	int         nCodeSize;		  // initial code size
	int         nBitSize;		  // bits/pixel (not necessarily same as nCodeSize)
	int         wbufpos;			  // byte position within read buffer
	int         wbufcount;		  // valid byte count within buffer
	UCHAR       wbuf[WBUFSIZE + WBUFXTRA];	// read buffer for this file
}           ImageStruct;

static WORD wStartRowOffset[4] = {0, 4, 2, 1};	//initial row offset for current pass on interlaced images
static WORD wRowSkip[4] = {8, 8, 4, 2};	//# of rows to skip for current pass on interlaced images

static int  fill_palette(LPBITMAPINFO, ImageStruct FAR *);
static int  read_row(ImageStruct FAR *, HPSTR, LPSTR, UINT, UINT);
static void Norm2DIB(HPSTR, LPSTR, int, int);

int  LoadFrames ( ImageStruct FAR * fs, GIFGCE FAR * lastgce ) ;
BOOL SkipToNext ( LPVOID lpHandle, LPIMG_IO lpio ) ;

#ifdef WIN32

#ifdef WINGIS // so the file TLSByJHC.h was included

#define SETERROR(E) bJHCTLSSetValue((DWORD)(E))
#define GETERROR    nJHCTLSGetValue()

#else

static DWORD TlsIndex;			  // Thread local storage index
#define SETERROR(E) TlsSetValue(TlsIndex,(LPVOID)(DWORD)(E))
#define GETERROR    ((DWORD)TlsGetValue(TlsIndex))

#endif

#else
static int  nErrID;				  // id of latest error
#define SETERROR(E) nErrID = (E)
#define GETERROR    (nErrID)
#endif

static HINSTANCE gifInstance;
#if DEBUG
static DEBUGPROC DebugStringOut;
#endif


#ifdef WIN32
// /////////////////////////////////////////////////////////////////////////
//
// DllMain
//
// This is the Win32 'DllEntryPoint'. It is similar in function to LibMain
// and the WEP in 16-bit land, but is much more thorough. LibMain is
// called whenever a process or thread attaches or detaches to the
// BMP DIL
//
// Entry:
// hinstDLL  - handle to the ImageMan DLL
// fdwReason - notification code (attach/detach)
// DLL_PROCESS_ATTACH
// DLL_PROCESS_DETACH
// DLL_THREAD_ATTACH
// DLL_THREAD_DETACH
// lpvReserved - NULL during dynamic process attach/detach, non-NULL
// during static attach/detach.
// Exit:
// DLL_PROCESS_ATTACH - TRUE for success, FALSE for failure
// all others         - return code ignored
// /////////////////////////////////////////////////////////////////////////

BOOL APIENTRY DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
	switch (fdwReason) {
		// The DLL is attaching to a process, due to LoadLibrary.
		case DLL_PROCESS_ATTACH:
			assert(!lpvReserved);  // must always be loaded via LoadLibrary!

			gifInstance = hinstDLL;// save our identity
#ifndef WINGIS
			// Allocate a "Thread Local Storage" (TLS) index right away.

			if ((TlsIndex = TlsAlloc()) == 0xFFFFFFFF)
				return (FALSE);
#else
			bJHCTLSClear();
#endif
		// Fall thru: Initialize the index for first thread.

		// The attached process created a new thread.
		case DLL_THREAD_ATTACH:
#ifndef WINGIS
			// Initialize the TLS index for this thread... it's actually 'nErrID'
			if (!TlsSetValue(TlsIndex, (LPVOID) 0))
				return (FALSE);
#else
			nJHCTLSAlloc(); // Stamp this thread to be existed
#endif
			break;

		// The thread of the attached process terminates.
		case DLL_THREAD_DETACH:
#ifdef WINGIS
			bJHCTLSFree(); // Free this thread entry
#endif
			break;

		// The DLL is detaching from a process, due to a call to FreeLibrary.
		case DLL_PROCESS_DETACH:
#ifndef WINGIS
			// Release the TLS index.
			TlsFree(TlsIndex);
#else
			bJHCTLSClear();
#endif
			break;
	}

	return (TRUE);

	UNREFERENCED_PARAMETER(lpvReserved);
}

#else // WIN32

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int WINAPI  LibMain(HINSTANCE hInstance, WORD wDataSeg, WORD wHeapSize, LPSTR lpCmdLine)
{
	if (!wHeapSize)
		return (0);					  // no heap indicates fatal error

#if DEBUG
	{
		HANDLE      hMod = GetModuleHandle("IMGMAN2.DLL");
		if (hMod)
			DebugStringOut = (DEBUGPROC) GetProcAddress(hMod, "DebugStringOut");
	}
#endif

	gifInstance = hInstance;
	return (1);
}
#endif // WIN32

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
void WINAPI WEP(int bSystemExit)
{
	return;
	UNREFERENCED_PARAMETER(bSystemExit);
}

//------------------------------------------------------------------

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
BOOL IMAPI  IMDLverify_img(LPSTR lpVerfBuf, LPVOID lpHandle, LPIMG_IO lpIO)
{
	return (lpVerfBuf[0] == 'G' && lpVerfBuf[1] == 'I' && lpVerfBuf[2] == 'F');
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLescape(HANDLE hImage, int nInCnt, LPVOID lpIn, LPVOID lpOut)
{
	return IMG_NSUPPORT;
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLinit_image(LPCSTR lpOpenInfo, LPHANDLE hFile, LPVOID lpHandle, long offset, long lLen, LPIMG_IO lpIO)
{
	BOOL bColorTable = FALSE ;
	GIFID       id;				  // Image Descriptor
	int nBits, nPalEntries ;
	ImageStruct FAR *fs;
	GIFSig      sig;
	GIFSD       sd;
	GIFGCE			gce ;
	long				poffset, lfagce = 0 ;
	int         len, retval;
	UCHAR       byte;

	// open the gif file
//	DEBUGOUT(DILAPI, "\t%s-->init_image(%s,%x:%x,%d,%ld,%ld)", (LPSTR) __FILE__, (LPSTR) file, HIWORD(hFile), LOWORD(hFile), offset, lLen);

	lpIO->seek(lpHandle, offset, SEEK_SET);

	if ((lpIO->read(lpHandle, (LPSTR) & sig, sizeof(sig)) != sizeof(sig))) {
		lpIO->close(lpHandle);

		SETERROR(IDS_READERR);
		return IMG_FILE_ERR;
	}
	// check the header info
	retval = IMG_OK;

	if (sig.Sig[0] != 'G' || sig.Sig[1] != 'I' || sig.Sig[2] != 'F') {
		SETERROR(IDS_BADSIG);
		lpIO->close(lpHandle);
		return IMG_FILE_ERR;
	}
	// create new internal structure
	if (!(*hFile = GlobalAlloc(DLLHND, sizeof(ImageStruct)))) {

		lpIO->close(lpHandle);

		SETERROR(IDS_MEMERR);
		return IMG_NOMEM;
	}
	fs = (ImageStruct FAR *) GlobalLock(*hFile);

	// Now read the Screen Descriptor Structure
	// If there is a Global Palette store its offset in poffset
	// then seek over it to the 1st Image Descriptor
	if (lpIO->read(lpHandle, (LPSTR) & sd, sizeof(sd)) != sizeof(sd)) {
		lpIO->close(lpHandle);

		GlobalUnlock(*hFile);
		GlobalFree(*hFile);
		*hFile = NULL;
		SETERROR(IDS_READERR);
		return IMG_FILE_ERR;
	}
	if (sd.Flags & 0x80) {		  // A Global Color Map Exists
		bColorTable = TRUE ;
		poffset = offset + sizeof(sig) + sizeof(sd);
		nBits = ( sd.Flags & 0x07 ) + 1 ;
		nPalEntries = ( 1U << nBits ) ;
		len = nPalEntries * sizeof(COLORMAP);
		lpIO->seek(lpHandle, len, SEEK_CUR);
	}
	// Read till find a 0x2c ',' which indicated the first Image Descriptor
	// We might possibly find an Extension here so lets code defensively
	if ( lpIO->read(lpHandle, &byte, sizeof(byte) ) == 0 ) {
		lpIO->close(lpHandle);

		GlobalUnlock(*hFile);
		GlobalFree(*hFile);
		*hFile = NULL;
		SETERROR(IDS_READERR);
		return IMG_FILE_ERR;
	}
  _fmemset ( (struct GIFGCE FAR *)&gce, 0, sizeof(GIFGCE) ) ;
	while (byte != 0x2c) {
		if (byte == '!') {		  // It's an extension block  i.e. x21
			// read the function code & length
			// skip over it
			// read the terminating 0
			if ( lpIO->read(lpHandle, &byte, sizeof(byte) ) == 0 ) {
				lpIO->close(lpHandle);
				GlobalUnlock(*hFile);
				GlobalFree(*hFile);
				*hFile = NULL;
				SETERROR(IDS_READERR);
				return IMG_FILE_ERR;
			}
			switch ( byte) {
				case 0xF9:  // graphic control extension
				{ 						
					lfagce = lpIO->seek ( lpHandle, 0, SEEK_CUR ) ;
					if ( lpIO->read( lpHandle, & gce, sizeof(GIFGCE)) == 0 ) {
						lpIO->close(lpHandle);
						GlobalUnlock(*hFile);
						GlobalFree(*hFile);
						*hFile = NULL;
						SETERROR(IDS_READERR);
						return IMG_FILE_ERR;
					}
					break;
				}
				case 0xFE:  	// comment block !!!
				//case 0x53:		//temp check
				{
					do
						{
							if ( !(lpIO->read(lpHandle, &byte, sizeof(byte))) ) {
								lpIO->close(lpHandle) ;
								GlobalUnlock(*hFile) ;
								GlobalFree(*hFile);
								*hFile = NULL;
								SETERROR(IDS_READERR);
								return IMG_FILE_ERR;
							}
						}
					while ( byte != '\0' ) ;		// read until the null char
					break;
				}
				case 0x3b: //trailer
					break;
					
				case 0xFF:  //Application Extension!!
				{
					UCHAR bBlockSize;
					int i;

					//read block size
					if ( !(lpIO->read(lpHandle, &bBlockSize, sizeof(bBlockSize))) ) {
						lpIO->close(lpHandle) ;
						GlobalUnlock(*hFile) ;
						GlobalFree(*hFile);
						*hFile = NULL;
						SETERROR(IDS_READERR);
						return IMG_FILE_ERR;
					}
				
					//flush data block
					for ( i=0; i<bBlockSize; i++)
					{
						if ( !(lpIO->read(lpHandle, &byte, sizeof(byte))) ) {
							lpIO->close(lpHandle) ;
							GlobalUnlock(*hFile) ;
							GlobalFree(*hFile);
							*hFile = NULL;
							SETERROR(IDS_READERR);
							return IMG_FILE_ERR;
						}

					}

					//read data sub block size
					if ( !(lpIO->read(lpHandle, &bBlockSize, sizeof(bBlockSize))) ) {
						lpIO->close(lpHandle) ;
						GlobalUnlock(*hFile) ;
						GlobalFree(*hFile);
						*hFile = NULL;
						SETERROR(IDS_READERR);
						return IMG_FILE_ERR;
					}
				
					//flush data sub block 
					for ( i=0; i<bBlockSize; i++)
					{
						if ( !(lpIO->read(lpHandle, &byte, sizeof(byte))) ) {
							lpIO->close(lpHandle) ;
							GlobalUnlock(*hFile) ;
							GlobalFree(*hFile);
							*hFile = NULL;
							SETERROR(IDS_READERR);
							return IMG_FILE_ERR;
						}

					}

					do
					{
						if ( !(lpIO->read(lpHandle, &byte, sizeof(byte))) ) {
							lpIO->close(lpHandle) ;
							GlobalUnlock(*hFile) ;
							GlobalFree(*hFile);
							*hFile = NULL;
							SETERROR(IDS_READERR);
							return IMG_FILE_ERR;
						}
					}
					while ( byte != '\0' ) ;		// read until the null char
				
					
					break;
				}
			}

		}
		if ( lpIO->read(lpHandle, &byte, sizeof(byte)) == 0 ) {
			lpIO->close(lpHandle);
			GlobalUnlock(*hFile);
			GlobalFree(*hFile);
			*hFile = NULL;
			SETERROR(IDS_READERR);
			return IMG_FILE_ERR;
		}
	}

	// The Image Descriptor is the next item to read
	if (lpIO->read(lpHandle, (LPSTR) & id, sizeof(GIFID)) != sizeof(GIFID)) {
		lpIO->close(lpHandle);

		GlobalUnlock(*hFile);
		GlobalFree(*hFile);
		*hFile = NULL;
		SETERROR(IDS_READERR);
		return IMG_FILE_ERR;
	}
	// check for interlaced/non-interlaced
	if ( id.Flags & INTERLACED) {
		fs->bInterlaced = TRUE;
	}
	// If there's a Local Color Map then store its offset and skip over it

	if ( id.Flags & 0x80) {	  // A Local Color Map Exists
		bColorTable = TRUE ;
		poffset = lpIO->seek(lpHandle, 0, SEEK_CUR);
		nBits = ( id.Flags & 0x07 ) + 1 ;
		len = (1U << nBits ) * sizeof(COLORMAP);
		lpIO->seek(lpHandle, len, SEEK_CUR);
	} else
		id.Flags = sd.Flags;

	// Read the Code Length Byte from the data stream & store it for later
	lpIO->read(lpHandle, &byte, sizeof(byte));
	fs->nCodeSize = byte;

	// first alloc the top level frame 
	fs->pTopFrame = (PGIFFRAME) GlobalAllocPtr ( GHND, sizeof(tagGIFFRAME) ) ;
	fs->pCurFrame = fs->pTopFrame ;
	fs->pTopFrame->lfaImgOffset = fs->dataOffset = lpIO->seek(lpHandle, 0, SEEK_CUR);
	fs->pTopFrame->bInterlaced  = fs->bInterlaced ;
	fs->pTopFrame->lfaGCExtension = lfagce ;
	fs->pTopFrame->lfaImgPalette  = poffset ;
	fs->pTopFrame->nCodeSize = fs->nCodeSize ;
	fs->pTopFrame->nLeft   = id.Left   ;
	fs->pTopFrame->nTop    = id.Top    ;
	fs->pTopFrame->nWidth  = id.Width  ;
	fs->pTopFrame->nHeight = id.Height ;
	if ( lfagce )
		fs->pTopFrame->nDelay = gce.Delay ;

	fs->lpOpenInfo = (LPVOID)lpOpenInfo;
	fs->lpHandle = lpHandle;
	fs->lpIO = lpIO;
	fs->wCurRow = 0;
	fs->wPass = 0;
	fs->wRowSkip = 0;
	fs->offset = offset;
	fs->len = lLen;
	fs->nBitSize = max(fs->nCodeSize, nBits );
	fs->pTopFrame->nBitDepth = fs->nBitSize ;
	fs->pTopFrame->nPageNumber = fs->nCurPage = 0 ;

	fs->BytesLine = (fs->pCurFrame->nWidth * ( (id.Flags & 0x07) + 1) + 7) / 8;

	fs->lpLZW = LZWOpenContext(byte + 1, 0, fs->pCurFrame->nWidth, fs->pCurFrame->nWidth, 0);

	fs->wbufpos = fs->wbufcount = 0;

	LoadFrames ( fs, (LPGIFGCE)&gce ) ;

	GlobalUnlock(*hFile);

	return IMG_OK;
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLbuildDIBhead(HGLOBAL hFile, INTERNALINFO FAR * lpii)
{
	LPBITMAPINFO lpbi;
	BITMAPINFOHEADER bi;
	int         nDIBits;
	ImageStruct FAR *fs;
	long        wid, hi;
	HGLOBAL     hMem;

	DEBUGOUT(DILAPI, "\t%s-->buildDIBhead(%d)", (LPSTR) __FILE__, hFile);
	if (!(fs = (ImageStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);
		return IMG_INV_HAND;
	}
	// nDIBits = (fs->nBitSize == 2) ? 1:fs->nBitSize;
	// no support for 24-bit GIFs
	if (fs->pCurFrame->nBitDepth == 1)
		nDIBits = 1;
	else if (fs->pCurFrame->nBitDepth < 5)
		nDIBits = 4;
	else
		nDIBits = 8;

	bi.biSize = sizeof(BITMAPINFOHEADER);
	bi.biWidth  = wid = fs->pCurFrame->nWidth  ;
	bi.biHeight = hi  = fs->pCurFrame->nHeight ;
	bi.biPlanes = 1;				  // Always
	// nDIBits already set to valid DIB value.

	bi.biBitCount = nDIBits;
	bi.biClrUsed = 1U << nDIBits ;
	bi.biCompression = BI_RGB;
	bi.biSizeImage = (wid * nDIBits) * hi;
	bi.biXPelsPerMeter = 0L;
	bi.biYPelsPerMeter = 0L;
	bi.biClrImportant = 0;

	lpii->lOrgWid = wid;
	lpii->lOrgHi = hi;
	lpii->bFlags = 0;

	lpii->nPages = fs->nPageCount ;

	if (!(hMem = GlobalAlloc(GHND, sizeof(BITMAPINFOHEADER) + bi.biClrUsed * sizeof(RGBQUAD)))) {
		SETERROR(IDS_MEMERR);
		return IMG_NOMEM;
	}
	lpbi = (LPBITMAPINFO) GlobalLock(hMem);
	*(LPBITMAPINFOHEADER) lpbi = bi;

	fs->nBitSize  = fs->pCurFrame->nBitDepth ;
	fs->nCodeSize = fs->pCurFrame->nCodeSize ;
	// now fill in the palette information
	fill_palette(lpbi, fs);
	lpii->lpbi = lpbi;

	GlobalUnlock(hFile);

	return IMG_OK;
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLread_rows(HGLOBAL hFile, LPSTR buf, UINT rows, UINT rowSize,
								              UINT start, LPSTATUS lpStat)
{
	ImageStruct FAR *fs;
	LPSTR       scratch;
	HPSTR       tmpBuf;
	int         tmp;
	
	DEBUGOUT(DILAPI, "\t%s-->%s(%d, %x:%x, %u, %u, %u, %x:%x)", (LPSTR) __FILE__, (LPSTR) "read_rows", hFile, HIWORD(buf), LOWORD(buf), rows, rowSize, start, HIWORD(lpStat), LOWORD(lpStat));
	if (!(fs = (ImageStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}
	// Only allocate scratch memory if we'll need it
	// We will always use it when reading interlaced images
	if ((fs->nBitSize <= 4) || (rowSize < fs->pCurFrame->nWidth) || fs->bInterlaced) {
		scratch = GlobalAllocPtr(GHND, fs->pCurFrame->nWidth);
	} else
		scratch = NULL;

	tmpBuf = (HPSTR) buf;
	tmpBuf += (rows - 1) * (long) rowSize;

	if (fs->bInterlaced) {	//interlaced read -- no fun :-(

		// well, we know that we're always starting from the beginning of the
		// image...basically, we just read all the rows in, adjusting for the
		// interlaced row skipping, and every row that we read that falls within
		// the requested image boundary (this is usually the entire image, but
		// not always) is output in its proper location. All other rows are
		// discarded.

		fs->wCurRow = 0;
		while (fs->wPass < 4) {

			// for interlaced images, we read into the scratch buffer first, then
			// copy it to the proper output location in the image buffer...			
			if ((tmp = read_row(fs, tmpBuf, scratch, start, rowSize)) != IMG_OK) {
				return tmp;
			}

			lpStat->lAcc += rowSize;
			if (lpStat->lpfnStat && lpStat->lAcc > lpStat->lInterval) {
				lpStat->lDone += lpStat->lAcc;
				lpStat->lDone = min(lpStat->lDone, lpStat->lTotal);
				if( !((*lpStat->lpfnStat) (lpStat->hImage, (int) ((lpStat->lDone * 100) / lpStat->lTotal), lpStat->dwUserInfo) )) {
					if (scratch)
						GlobalFreePtr(scratch);

					GlobalUnlock( hFile );
					return IMG_CANCELLED;
				}
				lpStat->lAcc %= lpStat->lInterval;
			}

			// make sure we skip any row that's not supposed to be part
			// of our output rectangle...
			if ((fs->wCurRow >= fs->wRowSkip) && (fs->wCurRow < (fs->wRowSkip+rows)) ) {
				switch (fs->wPass) {
					case 0:
					case 1:
						tmpBuf -= (rowSize * 8L);
						fs->wCurRow += 8;
						break;
					case 2:
						tmpBuf -= (rowSize * 4L);
						fs->wCurRow += 4;
						break;
					case 3:
						tmpBuf -= (rowSize * 2L);
						fs->wCurRow += 2;
						break;
				}
			}

			if (fs->wCurRow >= fs->pCurFrame->nHeight) {	//start a new pass!
				fs->wPass++;
				fs->wCurRow = wStartRowOffset[fs->wPass];
				tmpBuf = (HPSTR) buf;
				tmpBuf += ((rows-wStartRowOffset[fs->wPass]) - 1) * (long) rowSize;
			}
		}

		// once we're done, we have to go back to start of file, as we
		// just can't pick up where we've left off...
		IMDLset_row(hFile, 0);
	}
	else {	// non-interlaced read --- simple!

		while (rows--) {
			if ((tmp = read_row(fs, tmpBuf, scratch, start, rowSize)) != IMG_OK) {
				return tmp;
			}

			lpStat->lAcc += rowSize;
			if (lpStat->lpfnStat && lpStat->lAcc > lpStat->lInterval) {
				lpStat->lDone += lpStat->lAcc;
				lpStat->lDone = min(lpStat->lDone, lpStat->lTotal);
				if( !((*lpStat->lpfnStat) (lpStat->hImage, (int) ((lpStat->lDone * 100) / lpStat->lTotal), lpStat->dwUserInfo) )) {
					if (scratch)
						GlobalFreePtr(scratch);

					GlobalUnlock( hFile );
					return IMG_CANCELLED;
				}
				lpStat->lAcc %= lpStat->lInterval;
			}

			fs->wCurRow++;
			tmpBuf -= rowSize;
		}
	}

	if (scratch)
		GlobalFreePtr(scratch);

	GlobalUnlock(hFile);

	return IMG_OK;
}

// This routine assumes that file pointer is pointing to next line to
// read.
int         read_row(ImageStruct FAR * fs, HPSTR buf, LPSTR scratch, UINT start, UINT len)
{
	int         errVal;

	//
	// If we're reading in a partial row, or we're reading an image w/less
	// than 1 byte/pixel, we'll need to decode into the scratch buffer
	// and copy the portion we want. Otherwise, we can decode straight into
	// output buffer...
	//
	if (fs->nBitSize <= 4) {	  // use scratch buf to get info

		errVal = LZWDecodeRow(fs, scratch, fs->lpLZW);
		//
		// now we've got to convert from 1 byte/pixel to DIB format...
		//
		if (fs->nBitSize == 1)
			Norm2DIB(buf, scratch + start * 8, len * 8, 1);
		else
			Norm2DIB(buf, scratch + start * 2, len * 2, fs->nBitSize);
	} else if (len < fs->pCurFrame->nWidth) {
		errVal = LZWDecodeRow(fs, scratch, fs->lpLZW);
		_fmemmove(buf, scratch + start, len);
	} else
		errVal = LZWDecodeRow(fs, buf, fs->lpLZW);

	if (errVal != IMG_OK)
		SETERROR(IDS_PROCESS);

	return (errVal);
}


// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLset_row(HGLOBAL hFile, UINT row)
{
	ImageStruct FAR *fs;
	int         errVal;
	LPSTR       scratch;
	UINT				i;

	DEBUGOUT(DILAPI, "\t%s-->set_row(%d,%d)", (LPSTR) __FILE__, hFile, row);
	if (!(fs = (ImageStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}

	scratch = GlobalAllocPtr(GHND, fs->pCurFrame->nWidth);

	// read in the GIF format scan line
	// If it's an interlaced image, we will *always* just start
	// from the beginning of the file and read from there...
	if ((row < fs->wCurRow) || fs->bInterlaced) {
		fs->lpIO->seek(fs->lpHandle, fs->dataOffset, SEEK_SET);
		i = row;
		fs->wRowSkip = row;
		fs->wCurRow = 0;
		fs->wPass = 0;
	}
	else {
		i = row - fs->wCurRow;
		while (i--) {
			errVal = LZWDecodeRow(fs, scratch, fs->lpLZW);
		}
		fs->wCurRow = row;
	}

	GlobalFreePtr(scratch);
	GlobalUnlock(hFile);
	return IMG_OK;
}


// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLclose_image(HGLOBAL hFile)
{
  PGIFFRAME pNext ;
  PGIFFRAME pFrame ;
	ImageStruct FAR *fs;

	// check to see if file is still open - if it is, close it
	DEBUGOUT(DILAPI, "\t%s-->close_image(%d)", (LPSTR) __FILE__, hFile);
	if (!(fs = (ImageStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}
	if (fs->lpHandle) {
		fs->lpIO->close(fs->lpHandle);
	}

	LZWCloseContext(fs->lpLZW);

	pFrame = fs->pTopFrame ;
	while ( pFrame )		// kill the frame tree
		{
		  pNext = pFrame->pNextFrame ;
		  GlobalFreePtr ( pFrame ) ;
			pFrame = pNext ;
		}

	GlobalUnlock(hFile);
	GlobalFree(hFile);

	return IMG_OK;
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLopen(HGLOBAL hFile, LPVOID lpHandle)
{
	ImageStruct FAR *fs;

	if (!(fs = (ImageStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}
	if (!fs->lpHandle) {
		if (fs->lpIO->open(fs->lpOpenInfo, OF_READ, &(fs->lpHandle)) != IMG_OK) {
			GlobalUnlock(hFile);
			SETERROR(IDS_NOOPEN);
			return IMG_NOFILE;
		}
	}
	fs->lpIO->seek(fs->lpHandle, fs->dataOffset, SEEK_SET);	// seek to 1st row of Raster Data

	LZWResetContext(fs->lpLZW);

	fs->wbufpos = fs->wbufcount = 0;

	GlobalUnlock(hFile);

	return IMG_OK;
}


// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLclose_keep(HGLOBAL hFile)
{
	ImageStruct FAR *fs;

	DEBUGOUT(DILAPI, "\t%s-->close_keep(%d)", (LPSTR) __FILE__, hFile);
	if (!(fs = (ImageStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}
	if (fs->lpHandle) {
		fs->lpIO->close(fs->lpHandle);
		fs->lpHandle = NULL;
	}
	GlobalUnlock(hFile);

	return IMG_OK;
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLprint(HGLOBAL hFile, HDC hPrnDC, LPRECT lpDest, LPRECT lpSrc)
{
	SETERROR(IDS_NOPRINT);
	return IMG_NOTAVAIL;
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLrender(HGLOBAL hFile, HDC hDC, LPRECT lpDest, LPRECT lpSrc)
{
	return (IMG_ERR);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLerror_string(LPSTR lpszBuf, int nMaxLen)
{
	LoadString(gifInstance, GETERROR, lpszBuf, nMaxLen);
	return IMG_OK;
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
LPSTR IMAPI IMDLget_ver(void)
{
	return (LPSTR) (VERSION);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLset_ext(HGLOBAL hFile, LPSTR ext)
{
	DEBUGOUT(DILAPI, "\t%s-->set_ext(%d,%s)", (LPSTR) __FILE__, hFile, (LPSTR) ext);
	// no extensions to differentiate between - just return OK
	return IMG_OK;
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLimg_name(HGLOBAL hFile, LPHANDLE lpHand)
{
	ImageStruct FAR *fs;

	if (!(fs = (ImageStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}
//	*lpHand = fs->fName;

	GlobalUnlock(hFile);
	return IMG_OK;
}

// ======================================================================
// Helper Routines
// ======================================================================

UINT        wgetu(LPVOID cData)
{
#define lpIS ((ImageStruct FAR *)cData)

	// This hideousness which is "wgetu" attempts to fill the entire buffer
	// in one lread(). It then glues multiple GIF data blocks together to
	// form one giant data stream. Each UINT is then retrieved from this
	// contiguous stream until the next retrieval would go past the end.
	// While handling each GIF data block as a buffer-full isn't slow, this
	// monster-read provides a 14% speed boost in Windows 3.11, and a whopping
	// 30% increase in Windows NT. (Seems NT's I/O ain't so efficient)

	if (lpIS->wbufpos + (int) sizeof(UINT) > lpIS->wbufcount) {
#if USEGROOVYREAD
		UINT        i;
#ifdef WIN32
		LPSTR       src, dst, max;
#else
		UCHAR       __based((__segment) cData) * src;	// based pointers gain another 2%

		UCHAR       __based((__segment) cData) * dst;
		UCHAR       __based((__segment) cData) * max;
#endif

		if ((lpIS->wbufcount -= lpIS->wbufpos) > 0) {
			// preserve any orphaned bytes before the read
			*(UINT FAR *) lpIS->wbuf = *(UINT FAR *) (lpIS->wbuf + lpIS->wbufpos);
		}
		src = dst = (LPSTR)lpIS->wbuf + lpIS->wbufcount;
		max = src + lpIS->lpIO->read(lpIS->lpHandle, src, WBUFSIZE);	// read big chunk

		while (src < max && (i = *src++)) {	// parse a GIF data block

			if (src + i > max) {
				i -= max - src;	  // Incomplete data block;

				while (src < max)
					*dst++ = *src++;  // copy what we got,

				dst += lpIS->lpIO->read(lpIS->lpHandle, dst, i);	// then complete the block.

				break;
			}
			while (--i)
				*dst++ = *src++;	  // shift entire data block

			*dst++ = *src++;		  // down over block count byte

		}
		lpIS->wbufcount = (LPSTR) dst - lpIS->wbuf;	// how many bytes we gots?

		lpIS->wbufpos = sizeof(UINT);	// adjust for first get

		return (*(UINT FAR *) lpIS->wbuf);	// perform first get
#else
		UCHAR       count;

		if ((lpIS->wbufcount -= lpIS->wbufpos) > 0) {
			// preserve any orphaned bytes before the read
			*(UINT FAR *) lpIS->wbuf = *(UINT FAR *) (lpIS->wbuf + lpIS->wbufpos);
		}
		if (lpIS->lpIO->read(lpIS->lpHandle, &count, sizeof(count)) == sizeof(count)) {
			lpIS->wbufcount += lpIS->lpIO->read(lpIS->lpHandle, lpIS->wbuf + lpIS->wbufcount, count);
		}
		lpIS->wbufpos = 0;
#endif
	}
	// Pre-adjust the buffer position so we don't need a temporary for the
	// return value. The "-sizeof(UINT)" adds no additional time to the access.
	lpIS->wbufpos += sizeof(UINT);
	return (*(UINT FAR *) (lpIS->wbuf + lpIS->wbufpos - sizeof(UINT)));
}

int fill_palette ( LPBITMAPINFO lpbi, ImageStruct FAR * fs)
{
	long        curpos;
	COLORMAP    pal[256];
	RGBQUAD FAR *lprgb;
	int         i, nPalEntries ;

	curpos = fs->lpIO->seek(fs->lpHandle, 0, SEEK_CUR);

	fs->lpIO->seek(fs->lpHandle, fs->pCurFrame->lfaImgPalette, SEEK_SET);
	nPalEntries = ( 1 << fs->pCurFrame->nBitDepth ) ;

	// read palette info from map into pal
	fs->lpIO->read(fs->lpHandle, pal, (UINT) nPalEntries * sizeof(COLORMAP));

	//
	// now assign to lpbi->bmiColors
	//
	i = (int) nPalEntries ;
	for (lprgb = lpbi->bmiColors + i - 1; i--; lprgb--) {
		lprgb->rgbRed = pal[i].Red;
		lprgb->rgbGreen = pal[i].Green;
		lprgb->rgbBlue = pal[i].Blue;
		lprgb->rgbReserved = 0;
	}

	fs->lpIO->seek(fs->lpHandle, curpos, SEEK_SET);

	return 0;
}

//
// Norm2DIB converts a "normalized" (1 byte/pixel) image into a DIB format
// image.
//
static void Norm2DIB(HPSTR lpOut, LPSTR lpIn, int nPixels, int nBits)
{

	// switch (nBits) {
	// case 4:
	// case 3:
	// case 2:
	// Convert 2,3, or 4-bpp GIF into a 4-bpp DIB.
	while (nPixels) {
		*lpOut++ = (*lpIn << 4) | *(lpIn + 1);
		lpIn += 2;
		nPixels -= 2;
	}
	// break;
	// case 2:   // 1 bits/pixel is stored as 2 in GIF files
	// while (nPixels) {
	// *lpOut++ = (*lpIn << 7) | (*(lpIn+1) << 6) | (*(lpIn+2) << 5) |
	// (*(lpIn+3) << 4) | (*(lpIn+4) << 3) | (*(lpIn+5) << 2) |
	// (*(lpIn+6) << 1) | *(lpIn+7);
	// lpIn += 8;
	// nPixels -= 8;
	// }
	// break;
	// default:
	// break;
	// }
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLget_page(HANDLE hImage, LPINT nPage)
{
	ImageStruct FAR *fs;

	if (!(fs = (ImageStruct FAR *) GlobalLock(hImage))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}
	*nPage = fs->pCurFrame->nPageNumber ;

	GlobalUnlock(hImage);
	return IMG_OK;
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLset_page(HANDLE hImage, int nPage)
{
	ImageStruct FAR *fs;
	int retval = IMG_OK;

	if (!(fs = (ImageStruct FAR *) GlobalLock(hImage))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}
	if (nPage < 0 || (unsigned) nPage >= fs->nPageCount ) {
		SETERROR(IDS_INVPAGE);
		retval = IMG_INV_PAGE;
	} 
	else if (fs->nCurPage == (unsigned)nPage) {
		GlobalUnlock(hImage);
		return retval;
	}
	else if (fs->nCurPage > (unsigned)nPage) {
		// seek to first page
		fs->pCurFrame = fs->pTopFrame ;
		fs->nCurPage = 0;
	}
	
	while (fs->nCurPage < (unsigned)nPage) {
		fs->pCurFrame = fs->pCurFrame->pNextFrame ;
		fs->nCurPage++;
	}
	fs->dataOffset = fs->offset = fs->pCurFrame->lfaImgOffset ;
	fs->bInterlaced = fs->pCurFrame->bInterlaced ;
	fs->wDelay = fs->pCurFrame->nDelay ;
	fs->nCodeSize = fs->pCurFrame->nCodeSize ;
	fs->wCurRow = 0 ;						  // next row to read is 1st row in image
	fs->wPass = 0 ;
	fs->wRowSkip = 0 ;

	LZWCloseContext(fs->lpLZW);
	fs->lpLZW = LZWOpenContext(fs->nCodeSize+1, 0, fs->pCurFrame->nWidth, fs->pCurFrame->nWidth, 0);

	fs->wbufpos = 0 ;			// reset buffer

	GlobalUnlock(hImage);

	return retval;
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
BOOL SkipToNext ( LPVOID lpHandle, LPIMG_IO lpio )
{
	UCHAR uBlockLen ;
	int iRead ;
  long lfa = lpio->seek( lpHandle, 0, SEEK_CUR ) ;

	if ( !(iRead = lpio->read ( lpHandle, & uBlockLen, sizeof(uBlockLen))) )
		return FALSE ;
	while ( uBlockLen )
		{
			lfa += uBlockLen + 1 ;
			lpio->seek ( lpHandle, lfa, SEEK_SET ) ;
			if ( !(lpio->read ( lpHandle, & uBlockLen, sizeof(uBlockLen))) )
				return FALSE ;
		}
  return TRUE ;
}
// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int  LoadFrames ( ImageStruct FAR * fs, GIFGCE FAR * lastgce )
{
	GIFGCE lastGCE ;
	long   lfagce = 0 ;
	UCHAR ulabel ;
	UCHAR byte ;
	GIFID id ;
	LPIMG_IO lpio = fs->lpIO ;
	LPVOID   lpHandle = fs->lpHandle ;
	PGIFFRAME pCurFrame = fs->pTopFrame ;	// start at the top

	fs->nPageCount = 1 ;		// when we first come here we are at p1

	hmemcpy ( (LPGIFGCE)& lastGCE, (LPGIFGCE)lastgce, sizeof(GIFGCE) ) ;
	if ( ! (SkipToNext ( lpHandle, lpio )) )
		return 1 ;
	while ( lpio->read(lpHandle, &byte, sizeof(byte)) != 0 )
		{
			if ( byte == 0x21 )		// got ourselves an extension block, best skip it
				{										// we have to do this because 0x2c is valid data in these blocks
					if ( lpio->read ( lpHandle, & ulabel, sizeof(ulabel)) != sizeof(ulabel) ) 
						return 1;
					if ( ( ulabel == 0xFE ) ||		// it's a comment extenion
						   ( ulabel == 0xFF ) ||		// it's a application extension
							 ( ulabel == 0x01 ) )			// it's a plain text extension
						{
							do
								{
									long lfa = lpio->seek ( lpHandle, 0, SEEK_CUR ) ;
									if ( lpio->read ( lpHandle, & byte, sizeof(byte)) != sizeof(byte) )	// comment length
										return 1 ;
									lfa += ( byte + 1 ) ;
								}
							while ( byte != 0 ) ;		// while we still get some data
						}
					else
						if ( ulabel == 0xF9 )		// it's a graphic control extension
							{
								lfagce = lpio->seek ( lpHandle, 0, SEEK_CUR ) ;
								if ( lpio->read ( lpHandle, & lastGCE, sizeof(GIFGCE)) != sizeof(GIFGCE) )
									return 1 ;
							}
					byte = 0 ;  // set byte to zero since 0x2c may have been read in as a length
				}

		  if ( byte == 0x2c )	// its a new image descriptor
				{
				  if ( lpio->read( lpHandle, & id, sizeof(GIFID)) != sizeof(GIFID) )
						return 1 ;
					pCurFrame->pNextFrame = GlobalAllocPtr ( GHND, sizeof(tagGIFFRAME) ) ;
					pCurFrame->pNextFrame->pPrevFrame = pCurFrame ;		// set's the next prev to curr
					pCurFrame = pCurFrame->pNextFrame ;
					pCurFrame->nTop    = id.Top    ;
					pCurFrame->nLeft   = id.Left   ;
					pCurFrame->nWidth  = id.Width  ;
					pCurFrame->nHeight = id.Height ;
					if ( pCurFrame->bLocalColorTable = ( id.Flags & 0x80 ) ) // it does exist
						{
							pCurFrame->nBitDepth     = ( id.Flags & 0x07 ) + 1 ;
							pCurFrame->lfaImgPalette = lpio->seek(lpHandle, 0, SEEK_CUR ) ;
							// now jump the palette to the image data
							lpio->seek(lpHandle, (1U << pCurFrame->nBitDepth ) * sizeof(COLORMAP), SEEK_CUR);
						}
					else 
						{
							pCurFrame->nBitDepth = fs->pTopFrame->nBitDepth ;
							pCurFrame->lfaImgPalette = fs->pTopFrame->lfaImgPalette ;
						}
					if ( lpio->read(lpHandle, &pCurFrame->nCodeSize, sizeof(pCurFrame->nCodeSize)) != IMG_OK )
						return 1 ;
					// now set the image offset
					pCurFrame->lfaImgOffset = lpio->seek(lpHandle, 0, SEEK_CUR ) ;

					// check for interlaced/non-interlaced
					if ( id.Flags & INTERLACED)
						pCurFrame->bInterlaced = TRUE;
					pCurFrame->lfaGCExtension = lfagce ;
					pCurFrame->nDelay = lastGCE.Delay ;
					pCurFrame->nPageNumber = ++fs->nPageCount ;
					if ( ! (SkipToNext ( lpHandle, lpio )) )
						return 1 ;
				}
		}
	return 1 ;
}

#if DEBUG
void __cdecl FAR Debug(UINT wCategory, LPSTR lpFormat,...)
{
	LPSTR       vaArgs;

	if (DebugStringOut) {
		va_start(vaArgs, lpFormat);
		(*DebugStringOut) (wCategory, lpFormat, vaArgs);
		va_end(vaArgs);
	}
}
#else
void __cdecl FAR Debug(UINT wCategory, LPSTR lpFormat,...)
{
	UNREFERENCED_PARAMETER(wCategory);
	UNREFERENCED_PARAMETER(lpFormat);
}
#endif
