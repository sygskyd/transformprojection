/////////////////////////////////////////////////////////////////////////////
//
// ImgFX.h
//
// ImageMan Effects Module:
//   prototypes & user structures
//
// Copyright (c) 1996 Data Techniques, Inc.
// Copyright (c) 1996 Chris Roueche
// All Rights Reserved
//
/////////////////////////////////////////////////////////////////////////////
// $History: imgfx.h $
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 7/10/97    Time: 2:04p
// Created in $/imageman 16-32/dll-api level 2
// Initial Lvl 2 Checkin
//

#ifndef __IMGFX_H__

#ifdef __cplusplus
extern "C" {
#endif

#ifndef IMFXDECL
#ifdef WIN32
#define IMFXDECL __declspec(dllimport)
#else
#define IMFXDECL
#endif
#endif

#ifndef IMAPI
#ifdef WIN32
#define IMAPI __stdcall
#else
#define IMAPI WINAPI
#endif
#endif

// Here's the list of supported image effects. Immediately after each
// effect family is a list of effect modifiers (flags) appropriate for
// that family. Enjoy!
enum tag_ImageManDrawEffects {
	// NONE: Draw the image with no effects (like ImgDrawImage)
	IMFX_NONE,

#define IMDF_NONE           0 // generic flag for all effects

	// WIPE: Image wipes across DC
	IMFX_WIPELEFT,            // ...from right edge
	IMFX_WIPERIGHT,           // ...from left edge
	IMFX_WIPEUP,              // ...from bottom
	IMFX_WIPEDOWN,            // ...from top
#define IMDF_WIPE_IMGSLIDE  0     // image slides into view <default>
#define IMDF_WIPE_IMGEXPOSE 1     // image is exposed in-place
#define IMDF_WIPE_PUSHBKGND 0     // new image pushes background out <default>
#define IMDF_WIPE_OVERBKGND 2     // new image floats above background

	// CURTAIN: Image displayed about a central axis (like stage curtain)
	IMFX_VCURTAIN_OUT,        // vertical curtain, from middle
	IMFX_VCURTAIN_IN,         // vertical curtain, to middle
	IMFX_HCURTAIN_OUT,        // horizontal curtain, from middle
	IMFX_HCURTAIN_IN,         // horizontal curtain, to middle
#define IMDF_CURTAIN_IMGSLIDE  0  // image slides 
#define IMDF_CURTAIN_IMGEXPOSE 1  // image exposed in-place <default>
#define IMDF_CURTAIN_PUSHBKGND 0  // background pushed out
#define IMDF_CURTAIN_OVERBKGND 2  // image appears over background <default>

	// MOSAIC: Image displayed via random rectangles 
	IMFX_MOSAIC,              // ...spread over entire area
	IMFX_MOSAICLEFT,          // ...creeping from the right edge
	IMFX_MOSAICRIGHT,         // ...creeping from the left edge
	IMFX_MOSAICUP,            // ...creeping from the bottom
	IMFX_MOSAICDOWN,          // ...creeping from the top

	// BLUR: Zooms in and out of focus
	IMFX_BLUR_IN,             // blur image in (from bad to good)
	IMFX_BLUR_OUT,            // blur image out (from good to bad)

	// SHAPES: Image displayed using various shapes
	IMFX_SHAPE_EXPLODE,
	IMFX_SHAPE_IMPLODE,
#define IMDF_SHAPE_RECTANGLE      0
#define IMDF_SHAPE_ROUNDRECTANGLE 1
#define IMDF_SHAPE_ELLIPSE        2

	// BLIND: Venetian blind effect
	IMFX_VBLIND_LEFT,
	IMFX_VBLIND_RIGHT,
	IMFX_VBLIND_UP,
	IMFX_VBLIND_DOWN,
#define IMDF_VBLIND_IMGSLIDE  0  // image slides 
#define IMDF_VBLIND_IMGEXPOSE 1  // image exposed in-place <default>
#define IMDF_VBLIND_PUSHBKGND 0  // background pushed out
#define IMDF_VBLIND_OVERBKGND 2  // image appears over background <default>

	IMFX_ENUM                 // number of effects supported
};



#define ERROR_IMFX_SUCCESS        0   // seems odd, but follows 'winerror.h' format
#define ERROR_IMFX_INVALIDPARAM   100
#define ERROR_IMFX_INVALIDIMAGE   102
#define ERROR_IMFX_OUTOFMEMORY    103
#define ERROR_IMFX_OUTOFRESOURCES 104

// IMFXCALLBACK
//
// This user provided routine is called between every FX operation. It
// can be used for FX timing as well as tracking display progress.
// It currently shares the same format as ImageMan's STATUSPROC for user
// convenience.
//
// Parameters:
//   hImg       - handle of image being drawn
//   nPercent   - percent of process completed
//   dwUserInfo - user data passed into ImgDrawImageFX()
// Returns:
//   The user should return 0 (zero) to continue processing, or an error
//   code to stop. Any error returned from the callback will also be
//   returned by ImgDrawImageFX().
//
typedef int (WINAPI *IMFXCALLBACK)(HANDLE hImg,UINT nPercent,DWORD dwUserInfo);


// ImgDrawImageFX
//
// Draws an image to the given DC using the specified drawing effect.
// The image will be loaded if necessary. A vector image will draw
// with IMFX_NONE *unless* a rendered bitmap (DDB) is available (use
// ImgCreateDDB).
//
// Clip Regions:
//   Any active clip region in the given DC will be maintained. Note, however,
// that an internal *copy* is used in some effects. If you modify the DC's
// clip region in the callback, the region may or may not be restored to that
// copy at some point. We advise the clip region not be modified during
// an effect.
//
// Parameters:
//   hImg        - ImageMan image handle. If NULL, the specified effect
//                 is applied using the DC's background brush.
//   hDC         - destination DC
//   pDstBounds  - specifies destination location and size in logical 
//                 coordinates.
//   pSrcBounds  - specifies source pixels to display. If NULL, use entire
//                 source.
//   nEffect     - one of IMFX_* listed above
//   dwFlags     - one of IMDF_* listed above, a per-effect modifier
//   dwExtra     - per-effect extra data (low/high word) [range] <default>
//      Slide           : step increment <1>  / unused
//      Wipe            : step increment <1>  / unused
//      Mosaic          : cell width <1>      / cell height <cell width>
//      Creeping Mosaic : square cell size <1>/ fill percent [0..100]
//      Curtain         : step increment <1>  / unused
//      Venetian Blind  : step increment <1>  / slat size <10>
//   pfnCallback - address of a callback (status) function, to be called
//                 between every effect operation (may be NULL)
//   nInterval   - number of percentage points between each callback
//                 invocation (e.g. '5' will callback at 5%,10%,15%,...)
//                 Valid values are in the range [0..100], where zero will
//                 callback after every single atomic operation (lots!).
//   dwUserInfo  - data to be passed inn to your callback function
// Returns:
//   0 (zero) if successful, otherwise the error code. The error code may
//   have been supplied by your callback.
//
extern int IMFXDECL IMAPI ImgDrawImageFX(HANDLE hImg,
	HDC hDC,LPRECT pDstBounds,LPRECT pSrcBounds,
	UINT nEffect,DWORD dwFlags,DWORD dwExtra,
	IMFXCALLBACK pfnCallback,UINT nInterval,DWORD dwUserInfo);


#ifdef __cplusplus
}
#endif

#define __IMGFX_H__
#endif
