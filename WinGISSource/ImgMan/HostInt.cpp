/////////////////////////////////////////////////////////////////////////////
//
//    HostInt.cpp
//
//    ImageMan Plug-In Host Interface Module
//
//    ImageMan Image Processing Library
//
//    Copyright (c) 1998 Data Techniques, Inc.
//    All Rights Reserved
//   
/////////////////////////////////////////////////////////////////////////////
//$Header: /ImageMan 16-32/DLL - V 6.xx API/HostInt.cpp 11    11/09/98 2:52p Johnd $
//
//$History: HostInt.cpp $
// 
// *****************  Version 11  *****************
// User: Johnd        Date: 11/09/98   Time: 2:52p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Fixed a typo in the last checkin which prevented 16 bit code from
// compiling
// 
// *****************  Version 10  *****************
// User: Johnd        Date: 11/09/98   Time: 12:46p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Misc Optimizations and Fix in DIB.cpp for unitialized hDIB when using
// width/height ctor.
// 
// *****************  Version 9  *****************
// User: Johnd        Date: 11/03/98   Time: 11:02a
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Added ImgPlugInInvokeEx stub for 16 bit code
// 
// *****************  Version 8  *****************
// User: Ericw        Date: 4/24/98    Time: 11:37a
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Messed up error check in invoke functions when everything was OK.
// 
// *****************  Version 7  *****************
// User: Ericw        Date: 4/24/98    Time: 10:59a
// Updated in $/ImageMan 16-32/DLL-API Level 2
// New ImgPlugInInvokeEx function and now use LPRECTs.
// 
// *****************  Version 5  *****************
// User: Ericw        Date: 4/07/98    Time: 4:34p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// You can now call HostLoad with a new plug-in directory entry and it
// will use that one from then on.  Also changed the invoker to receive an
// LPRECT.
// 
// *****************  Version 4  *****************
// User: Ericw        Date: 4/07/98    Time: 11:51a
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Changed the PlugInLoadSpecific to just PlugInLoad - too wordy.
// 
// *****************  Version 3  *****************
// User: Ericw        Date: 4/06/98    Time: 11:56a
// Updated in $/ImageMan 16-32/DLL-API Level 2
// The plug-in enumerate function now protects itself from a NULL
// enumProc.
// 

#include "internal.h"

#ifdef _WIN32
	#include "PIHost.h"
#endif

extern HINSTANCE hInst ;

#ifndef _WIN32
BOOL   IMAPI ImgPlugInHostIsAvailable () {
	SetError(NULL, IMGPI_UNAVAIL, 0, NULL, hInst);
	return ( FALSE ) ;
}

BOOL   IMAPI ImgPlugInHostIsEnabled   () {
	SetError(NULL, IMGPI_UNAVAIL, 0, NULL, hInst);
	return ( FALSE ) ;
}

int IMAPI ImgPlugInHostLoad ( LPSTR lpPlugInDir ) {
	SetError(NULL, IMGPI_UNAVAIL, 0, NULL, hInst);
	return ( IMG_ERR ) ;
}

int IMAPI ImgPlugInHostUnload  () {
	SetError(NULL, IMGPI_UNAVAIL, 0, NULL, hInst);
	return ( IMG_ERR ) ;
}

int IMAPI ImgPlugInAbout ( HWND hOwner, LPSTR lpPlugIn ) {
	SetError(NULL, IMGPI_UNAVAIL, 0, NULL, hInst);
	return ( IMG_ERR ) ;
}

int IMAPI ImgPlugInLoad ( LPSTR lpPlugInName ) {
	SetError(NULL, IMGPI_UNAVAIL, 0, NULL, hInst);
	return ( IMG_ERR ) ;
}

int IMAPI ImgPlugInInvoke ( HANDLE hImage, HWND hOwner, LPRECT rect ) {
	SetError(NULL, IMGPI_UNAVAIL, 0, NULL, hInst);
	return ( IMG_ERR ) ;
}

int IMAPI ImgPlugInInvokeEx ( HANDLE hImage, HWND hOwner, LPRECT rectin, LPBYTE * lpData, BOOL bUI ) {
	SetError(NULL, IMGPI_UNAVAIL, 0, NULL, hInst);
	return ( IMG_ERR ) ;
}

int IMAPI ImgPlugInEnumerate ( PIIMENUMCALLBACKPROC enumProc, LONG lPIType, LONG lParam ) {
	SetError(NULL, IMGPI_UNAVAIL, 0, NULL, hInst);
	return ( IMG_ERR ) ;
}

int    IMAPI ImgPlugInSetStatusProc ( STATUSPROC lp, LONG lInt, DWORD dwUser ) {
	SetError(NULL, IMGPI_UNAVAIL, 0, NULL, hInst);
	return ( IMG_ERR ) ;
}



#else

BOOL IMAPI ImgPlugInHostIsAvailable ()
{
	LPTHREADINFO lpInfo = GetThreadInfo() ;
  return lpInfo->bPlugInAvailable ; 
}


BOOL IMAPI ImgPlugInHostIsEnabled ()
{
	LPTHREADINFO lpInfo = GetThreadInfo() ;
  return ( lpInfo->hPlugInHost ) ? TRUE : FALSE ; 
}

int IMAPI  ImgPlugInHostLoad ( LPSTR lpPlugInDir )
{
	LPTHREADINFO lpInfo = GetThreadInfo() ;
	HMODULE hPI = lpInfo->hPlugInHost ;
	PIIMINITPROC InitProc ;
	if ( ! hPI ) // it's not already loaded
		hPI = lpInfo->hPlugInHost = LoadLibrary( PLUGINHOSTSPEC ) ;

	if ( hPI ) {
		InitProc = (PIIMINITPROC)GetProcAddress( hPI, MAKEINTRESOURCE(101) ) ;
		if ( InitProc )
			return ( (*InitProc)( lpPlugInDir ) ) ;
	}

	SetError(NULL, IMGPI_NOTLOADED, 0, NULL, hInst);
	return ( IMG_ERR ) ;
}

int IMAPI ImgPlugInHostUnload ()
{
	LPTHREADINFO lpInfo = GetThreadInfo() ;
  if ( ! lpInfo->hPlugInHost ) {
		SetError(NULL, IMGPI_NOTLOADED, 0, NULL, hInst);
		return ( IMG_OK ) ;     // but doesn't return as an err return
	}
	if ( !FreeLibrary(lpInfo->hPlugInHost) ) {
		SetError(NULL, IMGPI_CANTUNLOAD, 0, NULL, hInst);
		return ( IMG_ERR ) ;
	}
	else
		lpInfo->hPlugInHost = 0 ;

	return ( IMG_OK ) ;
}

int IMAPI ImgPlugInAbout ( HWND hOwner, LPSTR lpPlugIn )
{
	LPTHREADINFO lpInfo = GetThreadInfo() ;
	HMODULE hPI = lpInfo->hPlugInHost ;
	PIIMABOUTPROC AboutProc ;

  if ( ! hPI ) {
		SetError(NULL, IMGPI_NOTLOADED, 0, NULL, hInst);
		return ( IMG_OK ) ;     // but doesn't return as an err return
	}
	if ( ! ImgPlugInHostIsAvailable() ) {
		SetError(NULL, IMGPI_UNAVAIL, 0, NULL, hInst);
		return ( IMG_ERR ) ;
	}
	AboutProc = (PIIMABOUTPROC)GetProcAddress( hPI, MAKEINTRESOURCE(103) ) ;
	if ( ! AboutProc )
		return IMG_ERR ;
	long lres = (*AboutProc)( hOwner, lpPlugIn ) ;

	return ( lres ) ;
}


int IMAPI ImgPlugInLoad ( LPSTR lpPlugInName )
{
	LPTHREADINFO lpInfo = GetThreadInfo() ;
	HMODULE hPI = lpInfo->hPlugInHost ;
	PIIMLOADPROC LoadProc ;

	if ( ! ImgPlugInHostIsAvailable() ) {
		SetError(NULL, IMGPI_UNAVAIL, 0, NULL, hInst);
		return ( IMG_ERR ) ;
	}
	LoadProc = (PIIMLOADPROC)GetProcAddress( hPI, MAKEINTRESOURCE(102) ) ;
	if ( ! LoadProc ) {
		SetError(NULL, IMGPI_UNAVAIL, 0, NULL, hInst);
		return IMG_ERR ;
	}
	if ( (*LoadProc)( lpPlugInName ) == IMG_OK )
		return ( IMG_OK ) ;
	else
		{
			SetError(NULL, IMGPI_ERRORLOADINGPLUGIN, 0, NULL, hInst);
			return ( IMG_ERR ) ;
		}
}

int IMAPI ImgPlugInEnumerate ( PIIMENUMCALLBACKPROC enumProc, LONG lPIType, LONG lParam )
{
	LPTHREADINFO lpInfo = GetThreadInfo() ;
	HMODULE hPI = lpInfo->hPlugInHost ;
	PIIMHOSTENUMPROC enumHostProc ;

	if ( ! enumProc ) {
		SetError( NULL, IMGPI_ERRORENUMERATING, 0, NULL, hInst );
		return IMG_ERR ;
	}
	if ( ! ImgPlugInHostIsAvailable() ) {
		SetError(NULL, IMGPI_UNAVAIL, 0, NULL, hInst);
		return IMG_ERR ;
	}
	enumHostProc = (PIIMHOSTENUMPROC)GetProcAddress( hPI, MAKEINTRESOURCE(105) ) ;
	if ( ! enumHostProc ) {
		SetError(NULL, IMGPI_UNAVAIL, 0, NULL, hInst);
		return IMG_ERR ;
	}
	if ( (*enumHostProc)( enumProc, lPIType, lParam ) == IMG_OK )
		return ( IMG_OK ) ;
	else
		{
			SetError(NULL, IMGPI_ERRORENUMERATING, 0, NULL, hInst);
			return ( IMG_ERR ) ;
		}
}

int IMAPI ImgPlugInSetStatusProc ( STATUSPROC lpStat, LONG lInterval, DWORD dwUser )
{
	PIIMSETSTATUSPROC pluginSetStatusProc ;
	LPTHREADINFO lpInfo = GetThreadInfo() ;
	HMODULE hPI = lpInfo->hPlugInHost ;

	if ( ! ImgPlugInHostIsAvailable() ) {
		SetError(NULL, IMGPI_UNAVAIL, 0, NULL, hInst);
		return IMG_ERR ;
	}
	pluginSetStatusProc = (PIIMSETSTATUSPROC)GetProcAddress( hPI, MAKEINTRESOURCE(106) ) ;
	if ( ! pluginSetStatusProc ) {
		SetError(NULL, IMGPI_UNAVAIL, 0, NULL, hInst);
		return IMG_ERR ;
	}
	(*pluginSetStatusProc)( lpStat, lInterval, dwUser ) ;

	SETSTATUS(IMG_OK);
	return (IMG_OK);
}

int IMAPI ImgPlugInInvokeEx ( HANDLE hImage, HWND hOwner, LPRECT rectin, LPBYTE * lpData, BOOL bUI )
{
	LPTHREADINFO lpInfo = GetThreadInfo() ;
	HMODULE hPI = lpInfo->hPlugInHost ;
	PIIMINVOKEPROC InvokeProc ;
	long lRes = IMG_OK ;
	LPINTERNALINFO lpII, lpFilterII = NULL ;
	HANDLE hFilterImage = NULL ;
	RECT rect ;
	PDIB pDIB ;


	if (!(lpII = (LPINTERNALINFO)GlobalLock( hImage ))) {
		SetError(NULL, IMG_INV_HAND, 0, NULL, hInst);
		return (IMG_ERR);
	}
	if (lpII->bFlags & IMG_DISP_VECTOR) {
		SetError(NULL, IMG_BAD_TYPE, 0, NULL, hInst);
		GlobalUnlock( hImage ) ;
		return (IMG_ERR);
	}
	if (!lpII->pDIB) {
		if (InternalLoad( lpII, NULL, &lpII->pDIB, &lpII->hWMF ) != IMG_OK) {
			GlobalUnlock( hImage ) ;
			return (IMG_ERR);
		}
	}
	if ( lpII->pDIB->GetBitCount() != 24 ) { // only do 24 bit images
		// for real, we'll eventually call ImgIncreaseColors
		SetError( NULL, IMG_BAD_TYPE, 0, NULL, hInst );
		GlobalUnlock( hImage ) ;
		return ( IMG_ERR ) ;
	}
	rect.left = rect.top = 0 ;
	rect.bottom = lpII->pDIB->GetHeight() - 1 ;
	rect.right  = lpII->pDIB->GetWidth () - 1 ;

	if ( ! rectin || !EqualRect( rectin, &rect ) ) {
		hFilterImage = hImage ;
		pDIB = lpII->pDIB ;
	} else {
		// validate the inbound rect bounds
		if ( ( rectin->top  >= rectin->bottom ) ||
			   ( rectin->left >= rectin->right  ) ) {  // baaad rect
			SetError( NULL, IMG_BAD_PARAM, 0, NULL, hInst );
			GlobalUnlock( hImage ) ;
			return ( IMG_ERR ) ;
		}
		rect = * rectin ;
		if ( !(hFilterImage = ImgCopy ( hImage, 
										  rectin->right - rectin->left + 1,
																		rectin->bottom - rectin->top + 1,
																		rectin, COPY_INTERPOLATE )) ) {
			SetError( NULL, IMG_PROC_ERR, 0, NULL, hInst ) ;
			GlobalUnlock( hImage ) ;
			return ( IMG_ERR ) ;
		}
		if (!(lpFilterII = (LPINTERNALINFO)GlobalLock( hFilterImage ))) {
			SetError(NULL, IMG_INV_HAND, 0, NULL, hInst);
			GlobalUnlock ( hImage ) ;
			return (IMG_ERR);
		}
		pDIB = lpFilterII->pDIB ;
	}

	InvokeProc = (PIIMINVOKEPROC)GetProcAddress( hPI, MAKEINTRESOURCE(104) ) ;
	
	if ( ! InvokeProc ) {
		SetError(NULL, IMGPI_PLUGINERROR, 0, NULL, hInst);
		if ( hFilterImage != hImage ) 
			ImgClose ( hFilterImage ) ;
		GlobalUnlock( hImage ) ;
		return IMG_ERR ;
	}

	lRes = (*InvokeProc)( hFilterImage, pDIB, hOwner, rect, lpData, bUI ) ;

	if ( lRes == IMG_OK ) {
		// Now we may have to paste back the filter data.
		// Also note if the following is true pDIB IS the filterDIB
		if ( hFilterImage != hImage ) {
			int iSrcRows = ( rectin->bottom - rectin->top ) + 1 ;
			int iRow ;
			int iBytes = ( pDIB->GetBitCount() / 8 ) * pDIB->GetWidth() ;
			LPSTR lpSrc = pDIB->GetDataPtr() ;
			LPSTR lpDst = lpII->pDIB->GetDataPtr() +
							( ((lpII->pDIB->GetHeight()-1 ) - rectin->bottom) * lpII->pDIB->GetRowSize() ) +
										( rectin->left * ( pDIB->GetBitCount() / 8 ) ) ;
			for ( iRow = 0 ; iRow < iSrcRows ; iRow ++ ) {
				memcpy ( lpDst, lpSrc, iBytes ) ;
				lpDst += lpII->pDIB->GetRowSize() ;
				lpSrc += pDIB->GetRowSize() ;
			}
		}
	}
	if ( hFilterImage != hImage )
		ImgClose ( hFilterImage ) ;

	SetError(NULL, lRes, 0, NULL, hInst);
	if ( lRes != IMG_OK )
		lRes = IMG_ERR ;

	GlobalUnlock( hImage ) ;
	return ( lRes ) ;
}

int IMAPI ImgPlugInInvoke  ( HANDLE hImage, HWND hOwner, LPRECT rectin )
{
  return(ImgPlugInInvokeEx( hImage, hOwner, rectin, NULL, TRUE ) ) ;
}
#endif
