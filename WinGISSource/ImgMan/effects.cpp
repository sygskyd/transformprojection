/////////////////////////////////////////////////////////////////////////////
//
//    effects.c
//
//
//    Effects Module
//
//    ImageMan Image Processing Library
//
//    Copyright (c) 1992-5 Data Techniques, Inc.
//    All Rights Reserved
//
/////////////////////////////////////////////////////////////////////////////
// $History: effects.cpp $
// 
// *****************  Version 4  *****************
// User: Johnd        Date: 11/11/98   Time: 1:10p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Chgd angle param in ImgRotate to UINT to allow 16 bit code to specify
// angles > 326
// 
// *****************  Version 3  *****************
// User: Johnd        Date: 3/20/98    Time: 9:05a
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Updated  internal error handling. Added error string info to TLS data
// instead of the global buffers where it had been stored. Also added
// SetError() func to set the internal error state.
// 
// *****************  Version 2  *****************
// User: Johnd        Date: 2/06/98    Time: 10:49a
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Chgd ImgRotate to allow rotation by .01 of a degree
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 7/10/97    Time: 2:02p
// Created in $/imageman 16-32/dll-api level 2
// Initial Lvl 2 Checkin
// 
// *****************  Version 5  *****************
// User: Johnd        Date: 6/19/96    Time: 10:16a
// Updated in $/ImageManVB/Image Control
// Fixed Bug #382 - ImgMirror() prob w/1 bit Horizontal mirroring & 8 Bit
// problem
// 
// *****************  Version 3  *****************
// User: Johnd        Date: 2/03/95    Time: 3:23p
// Updated in $/ImageMan 16-32/DLL
// Fixed B/W 90/270 rotation bugs (Again!)
// 
// *****************  Version 2  *****************
// User: Johnd        Date: 2/02/95    Time: 2:59p
// Updated in $/ImageMan 16-32/DLL
// Fixed bugs in Rotate8 & Rotate4 (180 degree Rotation)

#include "internal.h"
#pragma hdrstop
#include <assert.h>
#include <string.h>
#include <math.h>

#define PI           (3.14159265358979323846)
#define D2R(D)       ((D)/180.0*PI)
#define DBLROUND(V)  ((int)((V)+0.5))
#define DBL2FIX(V)   ((DTIFIXED)((V)*65536.0))
#define INT2FIX(V)   ((DTIFIXED)((long)(V)<<16))
#define FIX2INT(V)   ((int)((V)>>16))
#define FIXTRUNC(V)  ((V)&0xffff0000)

typedef struct tag_dblpoint {
	double x;
	double y;
} DBLPOINT,FAR *LPDBLPOINT;

typedef struct tag_dblrect {
	double left;
	double top;
	double right;
	double bottom;
} DBLRECT,FAR *LPDBLRECT;

typedef struct tag_matrix {
	double e[2][2];   // 2x2 matrix
} MATRIX,FAR *LPMATRIX;

typedef long DTIFIXED;

typedef struct tag_fixedpoint {
	DTIFIXED x;
	DTIFIXED y;
} FIXEDPOINT;

typedef struct tag_fixedrect {
	DTIFIXED left;
	DTIFIXED top;
	DTIFIXED right;
	DTIFIXED bottom;
} FIXEDRECT;

typedef struct tag_transinfo {
	HPSTR     dstbase;
	int       dstrowextra;
	int       dstwidth;
	int       dstheight;
	BYTE      dstbk[4];
	HPSTR      srctop;
	DWORD      srcrowbytes;
	FIXEDRECT  srect;
	FIXEDPOINT spos;
	FIXEDPOINT minor;
	FIXEDPOINT erradj;
	FIXEDPOINT major;
	DTIFIXED   stepval;
	long       bigstep;
	long       smallstep;
} TRANSINFO;


static DWORD const rottable[8][16] = { 
	0x00000000, 0x00000001, 0x00000100, 0x00000101, 
	0x00010000, 0x00010001, 0x00010100, 0x00010101, 
	0x01000000, 0x01000001, 0x01000100, 0x01000101, 
	0x01010000, 0x01010001, 0x01010100, 0x01010101,

	0x00000000, 0x00000002, 0x00000200, 0x00000202, 
	0x00020000, 0x00020002, 0x00020200, 0x00020202, 
	0x02000000, 0x02000002, 0x02000200, 0x02000202, 
	0x02020000, 0x02020002, 0x02020200, 0x02020202,

	0x00000000, 0x00000004, 0x00000400, 0x00000404, 
	0x00040000, 0x00040004, 0x00040400, 0x00040404, 
	0x04000000, 0x04000004, 0x04000400, 0x04000404, 
	0x04040000, 0x04040004, 0x04040400, 0x04040404,

	0x00000000, 0x00000008, 0x00000800, 0x00000808, 
	0x00080000, 0x00080008, 0x00080800, 0x00080808, 
	0x08000000, 0x08000008, 0x08000800, 0x08000808, 
	0x08080000, 0x08080008, 0x08080800, 0x08080808,

	0x00000000, 0x00000010, 0x00001000, 0x00001010, // 4
	0x00100000, 0x00100010, 0x00101000, 0x00101010, 
	0x10000000, 0x10000010, 0x10001000, 0x10001010, 
	0x10100000, 0x10100010, 0x10101000, 0x10101010,

	0x00000000, 0x00000020, 0x00002000, 0x00002020, // 5
	0x00200000, 0x00200020, 0x00202000, 0x00202020, 
	0x20000000, 0x20000020, 0x20002000, 0x20002020, 
	0x20200000, 0x20200020, 0x20202000, 0x20202020,

	0x00000000, 0x00000040, 0x00004000, 0x00004040, // 6
	0x00400000, 0x00400040, 0x00404000, 0x00404040, 
	0x40000000, 0x40000040, 0x40004000, 0x40004040, 
	0x40400000, 0x40400040, 0x40404000, 0x40404040,

	0x00000000, 0x00000080, 0x00008000, 0x00008080, // 7
	0x00800000, 0x00800080, 0x00808000, 0x00808080, 
	0x80000000, 0x80000080, 0x80008000, 0x80008080, 
	0x80800000, 0x80800080, 0x80808000, 0x80808080  
};

// Byte flip table for 1 bit rotation code
static UCHAR const fliptable[] = {
	  0, 128,  64, 192,  32, 160,  96, 224,  16, 
	144,  80, 208,  48, 176, 112, 240,   8, 
	136,  72, 200,  40, 168, 104, 232,  24, 
	152,  88, 216,  56, 184, 120, 248,   4, 
	132,  68, 196,  36, 164, 100, 228,  20, 
	148,  84, 212,  52, 180, 116, 244,  12, 
	140,  76, 204,  44, 172, 108, 236,  28, 
	156,  92, 220,  60, 188, 124, 252,   2, 
	130,  66, 194,  34, 162,  98, 226,  18, 
	146,  82, 210,  50, 178, 114, 242,  10, 
	138,  74, 202,  42, 170, 106, 234,  26, 
	154,  90, 218,  58, 186, 122, 250,   6, 
	134,  70, 198,  38, 166, 102, 230,  22, 
	150,  86, 214,  54, 182, 118, 246,  14, 
	142,  78, 206,  46, 174, 110, 238,  30, 
	158,  94, 222,  62, 190, 126, 254,   1, 
	129,  65, 193,  33, 161,  97, 225,  17, 
	145,  81, 209,  49, 177, 113, 241,   9, 
	137,  73, 201,  41, 169, 105, 233,  25, 
	153,  89, 217,  57, 185, 121, 249,   5, 
	133,  69, 197,  37, 165, 101, 229,  21, 
	149,  85, 213,  53, 181, 117, 245,  13, 
	141,  77, 205,  45, 173, 109, 237,  29, 
	157,  93, 221,  61, 189, 125, 253,   3, 
	131,  67, 195,  35, 163,  99, 227,  19, 
	147,  83, 211,  51, 179, 115, 243,  11, 
	139,  75, 203,  43, 171, 107, 235,  27, 
	155,  91, 219,  59, 187, 123, 251,   7, 
	135,  71, 199,  39, 167, 103, 231,  23, 
	151,  87, 215,  55, 183, 119, 247,  15, 
	143,  79, 207,  47, 175, 111, 239,  31, 
	159,  95, 223,  63, 191, 127, 255, 
};

#ifndef __NT__
extern int img_status;    // ImageMan status
#endif

extern HINSTANCE hInst;			  // who we is in this life... available to all!

static LPSTR NEAR PASCAL Rotate2(LPBITMAPINFOHEADER,HPSTR,HPSTR,UINT,UINT,int);
static LPSTR NEAR PASCAL Rotate4(LPBITMAPINFOHEADER,HPSTR,HPSTR,UINT,UINT,int);
static LPSTR NEAR PASCAL Rotate8(LPBITMAPINFOHEADER,HPSTR,HPSTR,UINT,UINT,int);
static LPSTR NEAR PASCAL Rotate24(LPBITMAPINFOHEADER,HPSTR,HPSTR,UINT,UINT,int);

static PDIB transformDIB(LPBITMAPINFO,HPSTR,LPRECT,LPMATRIX,LPMATRIX,COLORREF);


HGLOBAL IMAPI ImgRotate(HGLOBAL hImage, UINT nDegrees, COLORREF rgbBackColor)
{
	LPSTR lpOutBits;
	PDIB pDIB;
	UINT wHdrSize,dstWidth,dstHeight;
	LPINTERNALINFO lpII;
	HPSTR lpBits,lpInBits;

	// Make sure source DIB is available
	if (!(lpII = (LPINTERNALINFO)GlobalLock(hImage))) {
		SetError(NULL, IMG_INV_HAND, 0, NULL, hInst);
		return (NULL);
	}

	if (lpII->bFlags & IMG_DISP_VECTOR) {
		SetError(NULL, IMG_BAD_TYPE, 0, NULL, hInst);
		return (NULL);
	}
	//++Sygsky:ECW suport
	// as this kind of images can be HUGE, please don't rotate them
	if (lpII->bFlags & IMG_SCALE_SELF) {
		GlobalUnlock(hImage);
		SetError(NULL, IMG_BAD_TYPE, 0, NULL, hInst);
		return (NULL);
	}

	if (!lpII->pDIB) {
		if (InternalLoad(lpII, NULL, &lpII->pDIB, &lpII->hWMF) != IMG_OK) {
			return (NULL);
		}
	}

	if (lpII->hBitmap) {
		DeleteBitmap(lpII->hBitmap);
		lpII->hBitmap = NULL;
	}

	wHdrSize = (UINT)BITSOFFSET(&lpII->lpbi->bmiHeader);

	lpInBits = (HPSTR) lpII->pDIB->GetDataPtr();

	if (nDegrees % 9000) { // Handle general affine transformation
		MATRIX m,mi;
		double theta;

		// setup the transform matrix and its inverse
		theta = D2R( ((float)nDegrees / 100)); // math libraries want radians
		mi.e[0][0] = mi.e[1][1] = m.e[0][0] = m.e[1][1] = cos(theta);
		mi.e[1][0] = m.e[0][1] = -(mi.e[0][1] = m.e[1][0] = sin(theta));

		pDIB = transformDIB(lpII->lpbi,lpInBits,NULL,&m,&mi,rgbBackColor);
	}
	else {
		nDegrees /= 100;
		// Special case:
		//    Handle 90 degree increment rotations seperately, since they can
		//    be done much faster in specialized code.
		// (Image remains rectangular and no "filler" pixels are necessary.)
		if (nDegrees > 359) 
			nDegrees %= 360;

		// find size of resulting DIB
		if (nDegrees == 90 || nDegrees == 270) {
			dstWidth = (UINT)lpII->lpbi->bmiHeader.biHeight;
			dstHeight = (UINT)lpII->lpbi->bmiHeader.biWidth;
		} 
		else {
			dstWidth = (UINT)lpII->lpbi->bmiHeader.biWidth;
			dstHeight = (UINT)lpII->lpbi->bmiHeader.biHeight;
		}

		pDIB = new DIB( dstWidth, dstHeight, lpII->lpbi->bmiHeader.biBitCount );

		if( !pDIB->IsValid() ) {
			delete pDIB;
			SETSTATUS(IMG_NOMEM);
			return NULL;
		}

		lpOutBits = pDIB->GetDIBPtr();

		// lpBits points to raster data
		// lpOutBIts points to BITMAPINFO
		lpBits = (HPSTR) pDIB->GetDataPtr();

		// Copy the header into the new DIB
		_fmemcpy(lpOutBits,lpII->lpbi,wHdrSize);

		// call rotation function for this color format
		switch (lpII->lpbi->bmiHeader.biBitCount) {
		case 1:
			Rotate2((LPBITMAPINFOHEADER)lpOutBits,lpInBits,lpBits,dstWidth,dstHeight,nDegrees);
			break;

		case 4:
			Rotate4((LPBITMAPINFOHEADER)lpOutBits,lpInBits,lpBits,dstWidth,dstHeight,nDegrees);
			break;

		case 8:
			Rotate8((LPBITMAPINFOHEADER)lpOutBits,lpInBits,lpBits,dstWidth,dstHeight,nDegrees);
			break;

		case 24:
			Rotate24((LPBITMAPINFOHEADER)lpOutBits,lpInBits,lpBits,dstWidth,dstHeight,nDegrees);
			break;
		}
		((LPBITMAPINFOHEADER)lpOutBits)->biWidth = dstWidth;
		((LPBITMAPINFOHEADER)lpOutBits)->biHeight = dstHeight;
		((LPBITMAPINFOHEADER)lpOutBits)->biSizeImage = 0;
	}

	// Allocate an INTERNALINFO for the new image
	return (ImgFromPDIB(pDIB));    
}

static void NEAR rotate1x(int nSrcRows,HPSTR src,long srcstep,int nRows,
	HPSTR dst,long dststep)
{
	int i;
	DWORD low, hi;

	low = hi = 0;

	for (i = 8; nSrcRows && i--; nSrcRows--) {
		low |= rottable[i][*src&0x0f]; 
		hi |= rottable[i][*src>>4]; 
		src += srcstep;
	}

	// Now unpack the rotated bits
	for (i = 7; i >= 4; i--) { // 7,6,5,4
		if (nRows > i) {
			*dst = (UCHAR)(low & 0xff);
			dst += dststep;
		}
		low >>= 8;
	}

	do { // 3,2,1,0
		if (nRows > i) {
			*dst = (UCHAR)(hi & 0xff);
			dst += dststep;
		}
		hi >>= 8;
	} while (i--);
}


static LPSTR NEAR PASCAL Rotate2( LPBITMAPINFOHEADER lpbi, HPSTR lpInBits, HPSTR lpOutBits, UINT Width, UINT Height, int percent )
{
	long    lRowSize;
	long lInRowSize;
	long SizeBytes;
	long x, y; 

	HPSTR lpOut;
	HPSTR lpIn;

	// calculate the size of the new bitmap

	if( percent == 90 || percent == 270 ) {
		Width = lpbi->biHeight;
		Height = lpbi->biWidth;
	} else {
		Height = lpbi->biHeight;
		Width = lpbi->biWidth;
	}

	lRowSize = ((Width * lpbi->biBitCount) + 31 ) / 32 * 4;

	lInRowSize = ((lpbi->biWidth * lpbi->biBitCount) + 31 ) / 32 * 4;

	SizeBytes = lRowSize * (Height + 1);

	// Ready to rotate 1 bit stuff

	if( percent == 90 ) {                           // do 90' rotation
		int nRows, nCols;
		int row, col;
		HPSTR lpSrc;
		HPSTR lpDst;

		nCols = (int)lpbi->biWidth / 8;
		nRows = (int)lpbi->biHeight / 8;
		col = 0;


		lpSrc = lpInBits + ((lpbi->biHeight-1) * lInRowSize);

		for( row = 0; row < nRows; row++ ) {

			lpDst = lpOutBits + row + (lRowSize * 7); 

			for( col = 0; col < nCols; col++ ) {
				rotate1x( 8, lpSrc, (int)-lInRowSize, 8, lpDst, (int)-lRowSize );
				lpDst += lRowSize * 8;
				lpSrc++;
			}

			// Do a partial block, if any
			
			if( lpbi->biWidth % 8 ) {
			   lpDst -= (8 - (lpbi->biWidth % 8)) * lRowSize;
			  	rotate1x( 8, lpSrc, (int)-lInRowSize, (int)lpbi->biWidth % 8, lpDst, (int)-lRowSize );
			}

			lpSrc -= (lInRowSize * 8) + nCols;
		}


		// do the old bottom edge		
		if( lpbi->biHeight % 8 ) {

			// lpSrc should be OK from last loop
			lpDst = lpOutBits + row + (lRowSize * 7); 
			
			for( col = 0; col < nCols; col++ ) {
				rotate1x( (int)lpbi->biHeight % 8, lpSrc, (int)-lInRowSize, 8, lpDst, (int)-lRowSize );
				lpDst += lRowSize * 8;
				lpSrc++;
			}

			if( lpbi->biWidth % 8 ) {
			   lpDst -= (8 - (lpbi->biWidth % 8)) * lRowSize;
			  	rotate1x( (int)lpbi->biHeight % 8, lpSrc, (int)-lInRowSize, (int)lpbi->biWidth % 8, lpDst, (int)-lRowSize );
			}
		}


	} else
	if( percent == 180 ) {                          // do 180' rotation
		int nShift;
		int InPad, OutPad;
		int nInBytes;
		HPSTR lpTmp;

		InPad = (int) (lInRowSize - (lpbi->biWidth * lpbi->biBitCount + 7 ) /8);

		OutPad = (int) (lRowSize - (Width * lpbi->biBitCount + 7 ) /8);

		lpIn = lpInBits;
		lpOut = lpOutBits + (lRowSize * (Height-1) ) + ((Width-1) / 8);

		if( Width & 7 )
			nShift = (int) 8 - (Width % 8);
		else
			nShift = 0;

		for( y = Height;  y; y--) {
			for( x = (Width+7) / 8; x > 0; x-- )
				*lpOut-- = fliptable[ *lpIn++ ];
		
			if( nShift ) {	
				lpTmp = (HPSTR) lpOut;
		
				nInBytes = (int)lRowSize - OutPad;

				*lpTmp++ <<= nShift;

				while( nInBytes-- ) {
					int c;

					c = *lpTmp++;

					c <<= nShift;

					*(lpTmp-2) |= c >> 8;
					*(lpTmp-1) = c & 0xff;
				}
			}

			lpOut -= OutPad;
			lpIn += InPad;
		}

	} else 
	if( percent == 270 ) {                          // do 270' rotation
		int nRows, nCols;
		int row, col;
		HPSTR lpSrc;
		HPSTR lpDst;

		nCols = (int)lpbi->biWidth / 8;
		nRows = (int)lpbi->biHeight / 8;
		col = 0;

		for( row = 0; row < nRows; row++ ) {

			lpSrc = lpInBits + (row * lInRowSize * 8); 

			lpDst = lpOutBits + (lRowSize * (lpbi->biWidth)) + ( row );
			lpDst -= lRowSize * 8;

			for( col = 0; col < nCols; col++ ) {
				rotate1x( 8, lpSrc, (int)lInRowSize, 8, lpDst, (int)lRowSize );
				lpDst -= lRowSize * 8;
				lpSrc++;
			}

			// Do a partial block, if any
			if( lpbi->biWidth % 8 ) {
			   lpDst = lpOutBits + row;
			  	rotate1x( 8, lpSrc, (int)lInRowSize, (int)lpbi->biWidth % 8, lpDst, (int)lRowSize );
			}

		}

		// do the old bottom edge		
		if( lpbi->biHeight % 8 ) {

			lpSrc = lpInBits + (nRows * 8 * lInRowSize);

			lpDst = lpOutBits + (lRowSize * (lpbi->biWidth)) + ( row );
			lpDst -= lRowSize * 8;

			for( col = 0; col < nCols; col++ ) {
				rotate1x( (int)lpbi->biHeight % 8, lpSrc, (int)lInRowSize, 8, lpDst, (int)lRowSize );
				lpDst -= lRowSize * 8;
				lpSrc++;
			}

			if( lpbi->biWidth % 8 ) {
			   lpDst = lpOutBits + row;
			  	rotate1x( (int)lpbi->biHeight % 8, lpSrc, (int)lInRowSize, (int)lpbi->biWidth % 8, lpDst, (int)lRowSize );
			}

		}
	}

	if( lpOutBits ) {
		lpbi->biWidth = Width;
		lpbi->biHeight = Height;
	}
	
	return (LPSTR) lpOutBits;
}

//
//      Rotate4 - Rotation support for 4 Bit Images
//

static LPSTR NEAR PASCAL Rotate4(LPBITMAPINFOHEADER lpbi,HPSTR lpInBits,HPSTR lpOutBits,
	UINT Width,UINT Height,int percent)
{
	int x,y;
	long lRowSize;
	UINT InPad;
	HPSTR lpOut;

	lRowSize = ROWSIZE(Width,4);
	InPad = (UINT)(ROWSIZE(lpbi->biWidth,4) - (lpbi->biWidth*4 + 7 ) /8);

	// Ready to rotate 4 bits
	if (percent == 90 || percent == 270) {    // do 90' and 270' rotation
		register UCHAR ShiftCnt;
		UINT yidx;
		int yinc;

		if (Height&1) InPad++; // prevents an extra "huge" increment
		if (percent == 90) {   // 90: traverse destination bottom to top
			ShiftCnt = ((UCHAR)Width&1)<<2; // lpbi->biHeight&1 ? 4 : 0
			yidx = Width-1;                 // move right to left
			yinc = -1;
		}
		else {                 // 270: traverse destination top to bottom
			ShiftCnt = 4;       // start in high nibble
			lpOutBits += (Height-1)*lRowSize;
			lRowSize = -lRowSize;
			yidx = 0;          // move left to right
			yinc = +1;
		}

		for (y = (int)Width; --y >= 0; lpInBits += InPad, ShiftCnt ^= 4, yidx += yinc) {
			lpOut = lpOutBits+yidx/2;
			x = (int)(Height/2);
			if (ShiftCnt) {
				for ( ; --x >= 0; lpOut += lRowSize) { // copy into high nibble
					register UCHAR uc = *lpInBits++;
					*lpOut |= uc&0xF0;
					*(lpOut += lRowSize) |= uc<<4;
				}
				if (Height&1) *lpOut |= *lpInBits&0xF0; // InPad++ pays here
			}
			else {
				for ( ; --x >= 0; lpOut += lRowSize) { // copy into low nibble
					register UCHAR uc = *lpInBits++;
					*lpOut |= uc>>4;
					*(lpOut += lRowSize) |= uc&0x0F;
				}
				if (Height&1) *lpOut |= (UCHAR)(*lpInBits)>>4; // InPad++ pays here
			}
		}
	} 
	else if (percent == 180) {                          // do 180' rotation
		int OutPad;

		lpOut = lpOutBits + (lRowSize * (Height-1)) + (Width-1)/2;

		// adding negatives to perform subtraction is faster in MSVC 1.5
		OutPad = -(int)(lRowSize - (Width*4 + 7 ) /8);
		if (Width&1) OutPad--; // prevents an extra "huge" decrement

		// reverse nibbles within each byte, then output in reverse order
		for (y = Height; --y >= 0; lpOut += OutPad,lpInBits += InPad) {
			x = (int)(Width/2);
			if (Width&1) {
				while (--x >= 0) { // split source byte across destination bytes
					register UCHAR uc = *lpInBits++;
					*lpOut |= uc&0xF0;
					*--lpOut |= uc&0x0F;
				}
				*lpOut |= *lpInBits++&0xF0; // here's where OutPad-- pays
			}
			else {
				while (--x >= 0) { // copy source byte with nibble flippage
					register UCHAR uc = *lpInBits++;
					uc = uc<<4 | uc>>4;  // swap nibbles
					*lpOut-- = uc;
				}
			}
		}
	} 

	return (LPSTR)lpOutBits;
}

//
//      Rotate8 - Rotation support for 8 Bit Images
//
 
static LPSTR NEAR PASCAL Rotate8(LPBITMAPINFOHEADER lpbi,HPSTR lpInBits,HPSTR lpOutBits,
	UINT Width,UINT Height,int percent)
{                                                                                                                               
	int x,y;
	long lRowSize,lInRowSize;
	long SizeBytes;
	UINT InPad, OutPad;
	HPSTR lpOut,lpIn;

	lRowSize = ROWSIZE(Width,8);
	SizeBytes = lRowSize * Height;

	lInRowSize = ROWSIZE(lpbi->biWidth,8);


	InPad = (UINT)(lInRowSize - lpbi->biWidth);   // Bytes = Pixels in 8 bit
	OutPad = (UINT)(lRowSize - Width);

	// Ready to rotate bits
	if (percent == 90) {                           // do 90' rotation
		lpIn = lpInBits;

		for ( ; Width--; lpIn += InPad) {
			lpOut = lpOutBits + Width;
			for (x = (int)Height; x--; lpOut += lRowSize) {
				*lpOut = *lpIn++;
			}
		}
	} 
	else if (percent == 180) {                    // do 180' rotation
		lpIn = lpInBits;
		lpOut = lpOutBits + (lRowSize * (Height-1) ) + Width - 1;

		// simply reverse each scanline
		for ( ; Height--; lpOut -= OutPad,lpIn += InPad) {
			for (x = Width; x--; ) 
				*lpOut-- = *lpIn++;
		}
	} 
	else if (percent == 270) {                    // do 270' rotation
		HPSTR BotPtr;

		lpIn = lpInBits;
		BotPtr = lpOutBits + (Height-1) * lRowSize;

		for (y = 0; y < (int)Width;  y++, lpIn += InPad) {
			lpOut = BotPtr + y;
			for (x = (int)Height; x--; lpOut -= lRowSize) {
				*lpOut = *lpIn++;
			}
		}
	}

	return (LPSTR) lpOutBits;
}

//
//      Rotate24 - Rotation support for 24 Bit Images
//
 
static LPSTR NEAR PASCAL Rotate24(LPBITMAPINFOHEADER lpbi,HPSTR lpInBits,HPSTR lpOutBits,
	UINT Width,UINT Height,int percent)
{
	int x,y;
	long lRowSize,lInRowSize;
	long SizeBytes;
	UINT InPad, OutPad;
	HPSTR lpOut,lpIn;

	lRowSize = ROWSIZE(Width,24);
	SizeBytes = lRowSize * Height;

	lInRowSize = ROWSIZE(lpbi->biWidth,24);

	InPad = (UINT)(lInRowSize - (lpbi->biWidth * 24)/8);
	OutPad = (UINT)(lRowSize - (Width * 24)/8);

	// Ready to rotate bits
	if (percent == 90) {                           // do 90' rotation
		lpIn = lpInBits;
		lRowSize -= 2; // move back to same pixel on next scanline

		for (y = (int)Width*3; y > 0; lpIn += InPad) {

			lpOut = lpOutBits+(y -= 3);

			for (x = (int)Height; x--; lpOut += lRowSize) {
				*lpOut++ = *lpIn++;
				*lpOut++ = *lpIn++;
				*lpOut = *lpIn++;
			}
		}
	} 
	else if (percent == 180) {                     // do 180' rotation
		UCHAR r,g;

		lpIn = lpInBits;
		lpOut = lpOutBits + (lRowSize * (Height-1) ) + Width * 3 - 1;

		for (y = Height; y--; lpOut -= OutPad, lpIn += InPad) {
			for (x = Width; x > 0; x-- ) {
				r = *lpIn++;
				g = *lpIn++;
				*lpOut-- = *lpIn++; // B
				*lpOut-- = g;
				*lpOut-- = r;
			}
		}
	} 
	else if (percent == 270) {                     // do 270' rotation
		HPSTR BotPtr;

		lpIn = lpInBits;
		BotPtr = lpOutBits + (Height-1) * lRowSize;
		lRowSize += 2; // move back to same pixel on next scanline

		for (y = 0; y < (int)Width;  y++, lpIn += InPad) {
			lpOut = BotPtr + y * 3;
			for (x = 0; x < (int)Height; x++, lpOut -= lRowSize) {
				*lpOut++ = *lpIn++;
				*lpOut++ = *lpIn++;
				*lpOut = *lpIn++;
			}
		}
	}

	return (LPSTR) lpOutBits;
}


int IMAPI ImgMirror(HANDLE hImage,BOOL bVertical,BOOL bHorizontal)
{
	LPINTERNALINFO lpII;
	HPSTR lpSrc,lpDst;
	HPSTR lpBits;
	int nWidth,nHeight,x,y;
	long lRowSize;


	if (!(lpII = (LPINTERNALINFO)GlobalLock( hImage ))) {
		SetError( NULL, IMG_INV_HAND, 0, NULL, hInst );
		return (IMG_ERR);
	}

	if (lpII->bFlags & IMG_DISP_VECTOR) {
		SetError( NULL, IMG_BAD_TYPE, 0, NULL, hInst );
		return (IMG_ERR);
	}

	if (!bVertical && !bHorizontal) return (IMG_OK);

	if (!lpII->pDIB) {
		if (InternalLoad( lpII, NULL, &lpII->pDIB, &lpII->hWMF ) != IMG_OK) {
			return (IMG_ERR);
		}
	}

	if (lpII->hBitmap) {
		DeleteBitmap(lpII->hBitmap);
		lpII->hBitmap = NULL;
	}


	nWidth = (int)lpII->lpbi->bmiHeader.biWidth;
	nHeight = (int)lpII->lpbi->bmiHeader.biHeight;

	lpBits = (HPSTR) lpII->pDIB->GetDataPtr();
	lRowSize = lpII->pDIB->GetRowSize();

	if (bVertical) {
		UCHAR NEAR *linBuf;

		if (!(linBuf = (UCHAR NEAR *)LocalAlloc(LMEM_FIXED,(int)lRowSize))) {
			SetError( NULL, IMG_NOMEM, 0, NULL, hInst );
			return (IMG_ERR);
		}

		lpSrc = lpBits;
		lpDst = lpBits+(nHeight-1)*lRowSize;
		for( ; lpSrc < lpDst; lpSrc += lRowSize, lpDst -= lRowSize) {
			hmemcpy( linBuf, lpDst, lRowSize );
			hmemcpy( lpDst, lpSrc, lRowSize );
			hmemcpy( lpSrc, linBuf, lRowSize );
		}

		LocalFree((HLOCAL)linBuf);
	}

	if (bHorizontal) {
		if (lpII->lpbi->bmiHeader.biBitCount == 1) {
			if (!(nWidth & 7))  {            // If multiple of 8 then optimize
				UCHAR tmp;

				for (y = 0; y < nHeight; y++) {
					lpSrc = lpBits +  y * lRowSize;
					lpDst = lpBits +  y * lRowSize  + ((nWidth-7) / 8);

					for (x = (nWidth / 8) / 2; x--; ) {
						tmp = fliptable[*lpSrc];
						*lpSrc++ = fliptable[*lpDst];
						*lpDst-- = tmp;
					}
				}

				// Catch the last byte if 'nWidth/8' is odd
				if (lpDst != lpSrc) *lpSrc = fliptable[*lpSrc];

			} 
			else {
				UCHAR SrcMask,DstMask;

				for (y = 0; y < nHeight; y++) {
					lpSrc = lpBits +  y * lRowSize;
					lpDst = lpBits +  y * lRowSize + ((nWidth) / 8);
	
					SrcMask = 0x80;

					DstMask = 0x80 >> (((nWidth-1)&7) - 1);

					for (x = 0; x < nWidth / 2; x++) {
						// if bit set in dst, set same bit in src
						if (*lpDst & DstMask) 
							*lpSrc |= SrcMask;
						else 
							*lpSrc &= ~SrcMask;

						if (!(SrcMask >>=1)) {
							SrcMask = 0x80;
							lpSrc++;
						}

						// if bit set in src, set same bit in dst
						if (*lpSrc&SrcMask) 
							*lpDst |= DstMask;
						else 
							*lpDst &= ~DstMask;

						if (!(DstMask <<= 1)) {
							DstMask = 0x01;
							lpDst--;
						}
					}
				}
			}
		} 
		else if (lpII->lpbi->bmiHeader.biBitCount == 4) {
			UCHAR SrcPix,DstPix,SrcShift,DstShift;

			for (y = 0; y < nHeight; y++) {
				lpSrc = lpBits +  y * lRowSize;
				lpDst = lpBits +  y * lRowSize + ((nWidth-1) / 2);

				SrcShift = 4;
				DstShift = (nWidth&1)<<2;

				for (x = nWidth / 2; x--; ) {
					SrcPix = (*lpSrc >> SrcShift ) & 0x0f;
					DstPix = (*lpDst >> DstShift ) & 0x0f;

					*lpSrc &= 0xf0 >> SrcShift;
					*lpSrc |= DstPix << SrcShift;

					*lpDst &= 0xf0 >> DstShift;
					*lpDst |= SrcPix << DstShift;

					if (SrcShift ^= 4) lpSrc++; // move when shift toggles to 4.
					if (!(DstShift ^= 4)) lpDst--; // move when shift toggles to 0.
				}
			}
		} 
		else if (lpII->lpbi->bmiHeader.biBitCount == 8) {
			UCHAR tmp;

			for (y = 0; y < nHeight; y++) {
				lpSrc = lpBits +  y * lRowSize;
				lpDst = lpBits +  y * lRowSize + nWidth-1;

				for (x = nWidth / 2; x--; ) {
					tmp = *lpSrc;
					*lpSrc++ = *lpDst;
					*lpDst-- = tmp;
				}
			}
		} 
		else if (lpII->lpbi->bmiHeader.biBitCount == 24) {
			UCHAR tmp;

			for (y = 0; y < nHeight; y++) {
				lpSrc = lpBits +  y * lRowSize;
				lpDst = lpBits +  y * lRowSize + (nWidth - 1) * 3;

				for (x = nWidth / 2; x--; ) {
					tmp = *lpSrc;                           // B
					*lpSrc++ = *lpDst;
					*lpDst++ = tmp;

					tmp = *lpSrc;                           // G
					*lpSrc++ = *lpDst;
					*lpDst++ = tmp;

					tmp = *lpSrc;                           // R
					*lpSrc++ = *lpDst;
					*lpDst = tmp;

					lpDst -= 5;             // Skip back to previous triple
				}
			}
		}
	}

	return (IMG_OK);
}

/////////////////////////////////////////////////////////////////////////////
//
// Affine Transformation Routines
//
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// mkcolor
//
// Find 'best' color given an RGB based COLORREF. If DIB is 24-bit, I
// simply put the R and B elements in the correct position. For palette
// DIB's, I first look for an exact RGB match. If not found, I find the
// 'closest' color (using the same method Windows uses).
//
/////////////////////////////////////////////////////////////////////////////

static DWORD mkcolor(LPRGBQUAD lpQuad,UINT num,COLORREF color)
{
	BYTE cc[4];
	int tmp;
	UINT best;
	ULONG diff,mindiff;

	// simply return the palette index if so specified
	if (color&0x01000000) return (color&0x000000ff);

	// Force 'color' into RGBQUAD format (swap R and B)
	cc[3] = 0;
	cc[2] = (BYTE)color;       // R
	cc[1] = (BYTE)(color>>8);  // G
	cc[0] = (BYTE)(color>>16); // B

	// Simply return BGR value for 24-bit DIBs
	if (num > 8) return (*(DWORD FAR *)cc);

	// First, try to find an exact match
	num = 1<<num;
	for (best = 0; best < num; best++) {
		if (*(DWORD FAR *)cc == ((DWORD FAR *)lpQuad)[best]) return (best);
	}

	// No exact match, so I'll have to find the closest match. I use the
	// least squares difference method, just like Windows.
	for (mindiff = ~0UL; num--; lpQuad++) {
		if ((tmp = (cc[2]-lpQuad->rgbRed)) < 0) tmp = -tmp;
		diff = (UINT)tmp*tmp;
		if ((tmp = (cc[1]-lpQuad->rgbGreen)) < 0) tmp = -tmp;
		diff += (UINT)tmp*tmp;
		if ((tmp = (cc[0]-lpQuad->rgbBlue)) < 0) tmp = -tmp;
		diff += (UINT)tmp*tmp;
		if (diff < mindiff) {
			mindiff = diff;
			best = num;
		}
	}
	return (best);
}

/////////////////////////////////////////////////////////////////////////////
//
/////////////////////////////////////////////////////////////////////////////

static void transformpoint(LPDBLPOINT pt,LPMATRIX m)
{
	double oldx = pt->x;

	pt->x = oldx*m->e[0][0]+pt->y*m->e[1][0];
	pt->y = oldx*m->e[0][1]+pt->y*m->e[1][1];
}

/////////////////////////////////////////////////////////////////////////////
//
/////////////////////////////////////////////////////////////////////////////

static void transformbounds(LPDBLRECT brect,LPRECT srect,LPMATRIX m)
{
	register int i;
	DBLPOINT pt[4],min,max;  // [0] = tl, [1] = tr, [2] = bl, [3] = br

	// apply transform to all 4 corners
	pt[0].x = pt[2].x = srect->left; 
	pt[0].y = pt[1].y = srect->top;
	pt[1].x = pt[3].x = srect->right;
	pt[2].y = pt[3].y = srect->bottom;
	for (i = 4; i--; ) transformpoint(&pt[i],m);

	// find mins and maxs
	min = max = pt[3];
	for (i = 3; i--; ) {
		if (pt[i].x < min.x) min.x = pt[i].x;
		else if (pt[i].x > max.x) max.x = pt[i].x;
		if (pt[i].y < min.y) min.y = pt[i].y;
		else if (pt[i].y > max.y) max.y = pt[i].y;
	}
	brect->left = min.x;
	brect->top = min.y;
	brect->right = max.x;
	brect->bottom = max.y;
}


/////////////////////////////////////////////////////////////////////////////
//
// 1-bit DIB affine transformation engine.
//
/////////////////////////////////////////////////////////////////////////////

static void transform1(TRANSINFO ti)
{
	HPSTR sp,dp;
	register DTIFIXED sx,sy,i;
	int width,spos;
	BYTE dbyte,dmask,smask;

	// I need color index in every bit
	if (ti.dstbk[0]) ti.dstbk[0] = 0xFF;
	
	dp = ti.dstbase;

	while (ti.dstheight--) {
		width = ti.dstwidth;
		sx = ti.spos.x+ti.erradj.x;
		sy = ti.spos.y+ti.erradj.y;

		while (TRUE) {
			if (sx >= ti.srect.left && sx < ti.srect.right && sy >= ti.srect.top
				&& sy < ti.srect.bottom) {
				break;
			}
			if (!--width) break;
			sx += ti.minor.x;
			sy += ti.minor.y;
		}
		// Fill destination bits all at once.  Any partial-byte is used to
		// initialize the destination byte cache.
		for (spos = ti.dstwidth-width; spos >= 8; spos -= 8) *dp++ = ti.dstbk[0];
		if (!width) {
			if (spos > 0) *dp++ = ti.dstbk[0]; // fill whole byte w/ color
			goto rowdone;
		}
		dbyte = ti.dstbk[0]<<(8-spos);
		dmask = 0x0080>>spos;

		// Create the source pointer off our current source location.
		spos = FIX2INT(sx);
		sp = ti.srctop-(ti.srcrowbytes*FIX2INT(sy)-spos/8);
		smask = 0x0080>>(spos&7);

		while (TRUE) {
			if (*sp&smask) dbyte |= dmask;
			if (!(dmask >>= 1)) {
				*dp++ = dbyte;
				dbyte = 0;
				dmask = 0x80;
			}
			if (!--width) break;
			// Move to next source location; if new pixel, check for in-ness
			i = FIXTRUNC(sx);
			if (i -= FIXTRUNC(sx += ti.minor.x)) {
				if (sx < ti.srect.left || sx >= ti.srect.right) break;
				i = spos-FIX2INT(i);    // new position
				sp += (i>>3)-(spos>>3); // adjust byte position
				smask = 0x0080>>(i&7);  // update mask, too
				spos = (int)i;
			}
			i = FIXTRUNC(sy);
			if (i -= FIXTRUNC(sy += ti.minor.y)) {
				if (sy < ti.srect.top || sy >= ti.srect.bottom) break;
				if (i != ti.stepval) sp += ti.smallstep;
				else sp += ti.bigstep;
			}
		}
		// Empty the destination cache.
		if (dmask != 0x80) {
			// Fill in rest of bits with background pixel
			if (ti.dstbk[0]) dbyte |= dmask|(dmask-1);
			*dp++ = dbyte;
			for ( ; dmask; dmask >>= 1) width--; // need to update width
		}
		// May have some more background pixels to fill
		for ( ; width > 0; width -= 8) *dp++ = ti.dstbk[0];

rowdone:
		// Move to next destination row
		ti.spos.x += ti.major.x;
		ti.spos.y += ti.major.y;
		dp += ti.dstrowextra;
	}
}

/////////////////////////////////////////////////////////////////////////////
//
// 4-bit DIB affine transformation engine.
//
/////////////////////////////////////////////////////////////////////////////

static void transform4(TRANSINFO ti)
{
	HPSTR sp,dp;
	register DTIFIXED sx,sy,i;
	int width,j;
	UINT dcache;
	BYTE dpos,spos;
   
   // I need color index in both nibbles
   ti.dstbk[0] |= ti.dstbk[0]<<4;
   
	dp = ti.dstbase;

	while (ti.dstheight--) {
		width = ti.dstwidth;
		sx = ti.spos.x+ti.erradj.x;
		sy = ti.spos.y+ti.erradj.y;

		while (TRUE) {
			if (sx >= ti.srect.left && sx < ti.srect.right && sy >= ti.srect.top
				&& sy < ti.srect.bottom) {
				break;
			}
			if (!--width) break;
			sx += ti.minor.x;
			sy += ti.minor.y;
		}
		// Fill destination bits all at once.
		for (j = ti.dstwidth-width; j >= 2; j -= 2) *dp++ = ti.dstbk[0];
		if (!width) {
			if (j > 0) *dp++ = ti.dstbk[0]; // fill whole byte w/ color
			goto rowdone;
		}
		dpos = j&1;
		dcache = (UINT)ti.dstbk[0]<<8; // low byte must be empty!

		// Create the source pointer off our current source location.
		j = FIX2INT(sx);
		sp = ti.srctop-(ti.srcrowbytes*FIX2INT(sy)-j/2);
		spos = (j&1)<<2;

		// Copy pixels as long as we remain inside the source DIB.
		while (TRUE) {
			dcache = (dcache|((*sp<<spos)&0xF0))<<4;
			if (!(dpos ^= 1)) *dp++ = dcache>>8;

			if (!--width) break;

			i = FIXTRUNC(sx);
			if (i = FIXTRUNC(sx += ti.minor.x)-i) {
				if (sx < ti.srect.left || sx >= ti.srect.right) break;
				i = FIX2INT(i);
				sp += (i+(spos>>2))>>1;
				if (i&1) spos ^= 4;
			}
			i = FIXTRUNC(sy);
			if (i -= FIXTRUNC(sy += ti.minor.y)) {
				if (sy < ti.srect.top || sy >= ti.srect.bottom) break;
				if (i != ti.stepval) sp += ti.smallstep;
				else sp += ti.bigstep;
			}
		}
		// Empty the destination cache.
		if (dpos) {
			dcache |= ti.dstbk[0];
			*dp++ = dcache>>4;
			width--;
		}
		// May have some more background pixels to fill
		for ( ; width > 0; width -= 2) *dp++ = ti.dstbk[0];

rowdone:
		// Move to next destination row
		ti.spos.x += ti.major.x;
		ti.spos.y += ti.major.y;
		dp += ti.dstrowextra;
	}
}

/////////////////////////////////////////////////////////////////////////////
//
// 8-bit DIB affine transformation engine.
//
/////////////////////////////////////////////////////////////////////////////

static void transform8(TRANSINFO ti)
{
	HPSTR sp,dp;
	register DTIFIXED sx,sy,i;
	int width;

	dp = ti.dstbase;

	while (ti.dstheight--) {
		width = ti.dstwidth;

		// Set local copy of source position, and add in rounding error
		// adjustment and pre-rounding factor.
		sx = ti.spos.x+ti.erradj.x;
		sy = ti.spos.y+ti.erradj.y;

		// Move along source's 'minor' axis until I'm inside the source DIB.
		while (TRUE) {
			if (sx >= ti.srect.left && sx < ti.srect.right && sy >= ti.srect.top
				&& sy < ti.srect.bottom) {
				break;
			}
			*dp++ = ti.dstbk[0];
			if (!--width) goto rowdone; // all out of destination pixels
			sx += ti.minor.x;
			sy += ti.minor.y;
		}

		// Create the source pointer off our current source location.
		sp = ti.srctop-(ti.srcrowbytes*FIX2INT(sy)-FIX2INT(sx));

		// Copy pixels as long as we remain inside the source DIB.
		while (TRUE) {
			*dp++ = *sp;
			if (!--width) goto rowdone;
			// Move to next source location; if new pixel, check for in-ness
			i = FIXTRUNC(sx);
			if (i -= FIXTRUNC(sx += ti.minor.x)) {
				if (sx < ti.srect.left || sx >= ti.srect.right) break;
				sp -= FIX2INT(i);
			}
			i = FIXTRUNC(sy);
			if (i -= FIXTRUNC(sy += ti.minor.y)) {
				if (sy < ti.srect.top || sy >= ti.srect.bottom) break;
				if (i != ti.stepval) sp += ti.smallstep;
				else sp += ti.bigstep;
			}
		}

		// Must have at least one destination pixel left to fill with the
		// background color.
		while (width--) *dp++ = ti.dstbk[0];

rowdone:
		// Move to next destination row
		ti.spos.x += ti.major.x;
		ti.spos.y += ti.major.y;
		dp += ti.dstrowextra;
	}
}

/////////////////////////////////////////////////////////////////////////////
//
// 24-bit DIB affine transformation engine.
//
/////////////////////////////////////////////////////////////////////////////

static void transform24(TRANSINFO ti)
{
	HPSTR sp,dp;
	register DTIFIXED sx,sy,i;
	int width;

	dp = ti.dstbase;

	while (ti.dstheight--) {
		width = ti.dstwidth;
		sx = ti.spos.x+ti.erradj.x;
		sy = ti.spos.y+ti.erradj.y;

		while (TRUE) {
			if (sx >= ti.srect.left && sx < ti.srect.right && sy >= ti.srect.top
				&& sy < ti.srect.bottom) {
				break;
			}
			*dp++ = ti.dstbk[0];
			*dp++ = ti.dstbk[1];
			*dp++ = ti.dstbk[2];
			if (!--width) goto rowdone; // all out of destination pixels
			sx += ti.minor.x;
			sy += ti.minor.y;
		}

		sp = ti.srctop-(ti.srcrowbytes*FIX2INT(sy)-FIX2INT(sx)*3);

		while (TRUE) {
			*dp++ = *sp;
			*dp++ = *(sp+1);
			*dp++ = *(sp+2);
			if (!--width) goto rowdone;
			i = FIXTRUNC(sx);
			if (i -= FIXTRUNC(sx += ti.minor.x)) {
				if (sx < ti.srect.left || sx >= ti.srect.right) break;
				sp -= FIX2INT(i)*3;
			}
			i = FIXTRUNC(sy);
			if (i -= FIXTRUNC(sy += ti.minor.y)) {
				if (sy < ti.srect.top || sy >= ti.srect.bottom) break;
				if (i != ti.stepval) sp += ti.smallstep;
				else sp += ti.bigstep;
			}
		}

		while (width--) {
			*dp++ = ti.dstbk[0];
			*dp++ = ti.dstbk[1];
			*dp++ = ti.dstbk[2];
		}
rowdone:
		ti.spos.x += ti.major.x;
		ti.spos.y += ti.major.y;
		dp += ti.dstrowextra;
	}
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
//
// transformDIB
//
// DIB affine transformation entry point. This routine will pass any
// part of a source DIB through the given transformation, and return the
// resulting DIB in a new global handle. Any 'background' pixels in the
// new DIB (those not filled from the source DIB) are filled as directed
// by the specified COLORREF.
//
// Entry:
//    srcInfo  - source DIB header and color information
//    srcBits  - pointer to source DIB bits (bottom-most scanline)
//    srcRect  - may specify any portion of the source DIB, or NULL to
//               transform the entire source DIB
//    m        - a 2x2 matrix
//    mi       - the inverse matrix for 'm'
//    bkColor  - directions for 'background' pixels in the destination. May
//               be created via RGB() for closest color, or PALETTEINDEX()
//               to specify an exact palette index.
//
// Returns:
//    Unlocked global handle containing a Packed DIB just large enough to
//    fully contain the transformed source area.
//
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

static PDIB transformDIB(LPBITMAPINFO srcInfo,HPSTR srcBits,LPRECT srcRect,
	LPMATRIX m,LPMATRIX mi,COLORREF bkColor)
{
	UINT wHdrSize;
	PDIB pDIB;
	RECT srect,trect;
	DBLRECT brect;
	DBLPOINT c1,c2,c4,major,minor;
	TRANSINFO ti;

	// Find source area to transform
	SetRect(&srect,0,0,(int)srcInfo->bmiHeader.biWidth,(int)srcInfo->bmiHeader.biHeight);
	if (srcRect) IntersectRect(&srect,&srect,srcRect);

	// Find the transformed source area's bounding rectangle. This will tell me
	// how large the destination bitmap needs to be, and give me some corners
	// to work with later on.
	transformbounds(&brect,&srect,m);

	ti.dstwidth = DBLROUND(brect.right-brect.left);
	ti.dstheight = DBLROUND(brect.bottom-brect.top);

	// find size of resulting DIB
	wHdrSize = (UINT)BITSOFFSET(&srcInfo->bmiHeader);

	//if (!(hDIB = GlobalAlloc(GHND,lDIBSize))) return (NULL);
	//ti.dstbase = (LPSTR) GlobalLock(hDIB);

	pDIB = new DIB( ti.dstwidth, ti.dstheight, srcInfo->bmiHeader.biBitCount);

	if( !pDIB->IsValid() ) {
		delete pDIB;
		return NULL;
	}

	ti.dstbase = (HPSTR) pDIB->GetDIBPtr();

	// Copy the header into the new DIB
	_fmemcpy(ti.dstbase, srcInfo, wHdrSize);
	((LPBITMAPINFOHEADER)ti.dstbase)->biWidth = ti.dstwidth;
	((LPBITMAPINFOHEADER)ti.dstbase)->biHeight = ti.dstheight;

	// Move to data bits.
	ti.dstbase = (HPSTR) pDIB->GetDataPtr();

	ti.dstrowextra = pDIB->GetRowPadding();		//nRowSize-(ti.dstwidth*srcInfo->bmiHeader.biBitCount+7)/8;

	// Now I can do all the nasty transformation calculations that'll make
	// your head spin.
	// The first problem I run into is that the destination will most likely
	// not have the same pixel count as the source; it's not a 1-to-1 mapping.
	// This foils my first inclination to traverse the source along its
	// scanlines, placing pixels into the destination along 'Bresenham' lines:
	// this could leave unfilled or multiply filled pixels.  The secret, 
	// of course, is to fill the destination scanline by scanline, pulling 
	// pixels from the source.
	// To do this, I need to map the corners of my destination DIB back into
	// source space. These points (which will lie outside the source DIB) form
	// the endpoints of lines thru the source which I must traverse to fill
	// the destination.
	// Well, that's the basic idea here. There's extra goo to guarantee
	// everything lines up correctly (adjust for rounding errors) and to make
	// the process fast. So, hold on to your hat...
	//   -CAR
	//
	// In order to fill the destination DIB from the bottom up, I process
	// the bottom corners (1 & 2) and the top-left corner (4).
	c4.x = c1.x = brect.left;
	c1.y = c2.y = brect.bottom;
	c2.x = brect.right;
	c4.y = brect.top;
	transformpoint(&c1,mi);
	transformpoint(&c2,mi);
	transformpoint(&c4,mi);

	// Create the step values to traverse the source's 'minor' vector
	// (corresponding to the destination's X axis). The step size is based on
	// the number of pixels needed to fill a destination scanline.
	minor.x = (c2.x-c1.x)/(brect.right-brect.left);
	minor.y = (c2.y-c1.y)/(brect.right-brect.left);
	ti.minor.x = DBL2FIX(minor.x); // engines want 16:16 fixed-point values
	ti.minor.y = DBL2FIX(minor.y);

	// Now create the step values to traverse the source's 'major' vector
	// (corresponding to the destination's Y axis).
	major.x = (c4.x-c1.x)/(brect.bottom-brect.top);
	major.y = (c4.y-c1.y)/(brect.bottom-brect.top);
	ti.major.x = DBL2FIX(major.x);
	ti.major.y = DBL2FIX(major.y);

	// There may be inaccuracies in our starting point caused by rounding
	// the bounding rectangle. Adjust by moving the starting position in the
	// correct direction along the major vector.
	c4.y = DBLROUND(brect.bottom)-brect.bottom;

	// The rounding inaccuracies affect the X-axis as well, so adjust along
	// the minor vector. This adjustment is made inside the transformation
	// engine, so just create it here.
	// I also add in the rounding factor (+0.5) so that the engine may simply
	// (and much more quickly) truncate the X and Y positions.
	c4.x = DBLROUND(brect.left)-brect.left;
	ti.erradj.x = DBL2FIX(minor.x*c4.x+0.5);
	ti.erradj.y = DBL2FIX(minor.y*c4.x+0.5);

	// The source starting position (origin of major and minor vectors) is 
	// simply the destination's bottom-left corner's (c1) position in source
	// space. Make sure the starting point is a valid source pixel.
	SetRect(&trect,0,0,(int)srcInfo->bmiHeader.biWidth-1,(int)srcInfo->bmiHeader.biHeight-1);
	IntersectRect(&trect,&trect,&srect);
	transformbounds(&brect,&trect,m);
	c1.x = brect.left;
	c1.y = brect.bottom;
	transformpoint(&c1,mi);                 // get "real" starting point
	ti.spos.x = DBL2FIX(c1.x+major.x*c4.y); // add adjustment to "real" start
	ti.spos.y = DBL2FIX(c1.y+major.y*c4.y);

	// Create a fixed-point source rectangle to quickly detect whether any point
	// is a member of the source area.
	ti.srect.top = INT2FIX(srect.top);
	ti.srect.left = INT2FIX(srect.left);
	ti.srect.right = INT2FIX(srect.right);
	ti.srect.bottom = INT2FIX(srect.bottom);

	ti.srcrowbytes = ROWSIZE((int)srcInfo->bmiHeader.biWidth,srcInfo->bmiHeader.biBitCount);
	ti.srctop = srcBits+ti.srcrowbytes*(srcInfo->bmiHeader.biHeight-1);

	// With a little extra work, I can avoid a multiplication within the inner
	// translation engine loop.
	ti.smallstep = ti.minor.y < 0 ? ti.srcrowbytes : -(long)ti.srcrowbytes;
	if ((ti.minor.y < 0 ? -ti.minor.y : ti.minor.y) < 0x00010000) {
		ti.bigstep = ti.stepval = 0;
	}
	else {
		ti.bigstep = -FIX2INT(ti.minor.y)*ti.srcrowbytes;
		ti.stepval = -(DTIFIXED)FIXTRUNC(ti.minor.y);
	}
	ti.smallstep += ti.bigstep; // might as well combine them here

	// Create the background filler color
	*(DWORD FAR *)ti.dstbk = mkcolor(srcInfo->bmiColors,srcInfo->bmiHeader.biBitCount,bkColor);

	switch (srcInfo->bmiHeader.biBitCount) {
	case 1: transform1(ti); break;
	case 4: transform4(ti); break;
	case 8: transform8(ti); break;
	case 24: transform24(ti); break;
	}

	return pDIB;
}




