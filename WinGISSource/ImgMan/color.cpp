/////////////////////////////////////////////////////////////////////////////
//
// COLOR.C
//
// Color Reduction/Dithering Module
//
// ImageMan Image Processing Library
//
// Copyright (c) 1991-7 Data Techniques, Inc.
// All Rights Reserved
//
// ///////////////////////////////////////////////////////////////////////////
// $History: color.cpp $
// 
// *****************  Version 16  *****************
// User: Johnd        Date: 11/13/98   Time: 2:50p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Fixed a GPF bug when reducing to an image's pal in 16 bit compile
// 
// *****************  Version 15  *****************
// User: Johnd        Date: 11/02/98   Time: 4:42p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Misc 6.02 Updates
// 
// *****************  Version 14  *****************
// User: Johnd        Date: 10/13/98   Time: 4:58p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Fixed Bug 145 (ImgReduceToPal R/B swapped)
// 
// *****************  Version 13  *****************
// User: Ericj        Date: 6/30/98    Time: 2:59p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Needed to Weight Grey Map considerably more towards the end.  Or else
// the dotting problem shows up with slightly off white images.
// 
// *****************  Version 12  *****************
// User: Ericj        Date: 6/09/98    Time: 1:27p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Changed the Black and white dithering algorithms so they do not leave a
// pattern of black dots on white images.
// 
// *****************  Version 11  *****************
// User: Johnd        Date: 4/16/98    Time: 5:37p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Misc Updates
// 
// *****************  Version 10  *****************
// User: Johnd        Date: 4/16/98    Time: 4:28p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Removed over zealous param checking in InternalReduceColors
// 
// *****************  Version 9  *****************
// User: Johnd        Date: 4/16/98    Time: 4:25p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Removed nColors param from ImgReduceColorsPal
// 
// *****************  Version 8  *****************
// User: Johnd        Date: 4/15/98    Time: 2:09p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Chg ImgReduceColors(Pal) to make Octree Method the default
// 
// *****************  Version 7  *****************
// User: Ericj        Date: 4/01/98    Time: 4:27p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Added bad parameter error checking for some functions.
// 
// *****************  Version 6  *****************
// User: Johnd        Date: 3/20/98    Time: 9:05a
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Updated  internal error handling. Added error string info to TLS data
// instead of the global buffers where it had been stored. Also added
// SetError() func to set the internal error state.
// 
// *****************  Version 5  *****************
// User: Ericw        Date: 3/18/98    Time: 10:13a
// Updated in $/ImageMan 16-32/DLL-API Level 2
// If reducing to the same color depth and try to use either calculated
// palette, we now return "not supported" error.
// 
// *****************  Version 4  *****************
// User: Ericw        Date: 12/03/97   Time: 5:08p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Took out NEAR on InitTransPal - not good under 16 bit release mode.
// 
// *****************  Version 3  *****************
// User: Johnd        Date: 10/09/97   Time: 2:07p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Fixed Vector Test Bug in ImgReduceColorsPal
// 
// *****************  Version 2  *****************
// User: Ericw        Date: 9/23/97    Time: 5:14p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Implemented octree color reduction stuff for v6.
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 7/10/97    Time: 2:02p
// Created in $/imageman 16-32/dll-api level 2
// Initial Lvl 2 Checkin
// 
// *****************  Version 3  *****************
// User: Timk         Date: 8/30/95    Time: 5:09p
// Updated in $/ImageMan 16-32/DLL
// Fixed small bug in IncreaseColors when using mono-images - we weren't
// resetting the mask for each line.
// 
// *****************  Version 2  *****************
// User: Johnd        Date: 5/17/95    Time: 9:58a
// Updated in $/ImageManVB/Image Control
// Added additional error checking to ImgReduceColors.
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 1/18/95    Time: 3:59p
// Created in $/ImageMan 16-32/DLL
// ImageMan 4.0 Beta 1

#include "internal.h"
#pragma hdrstop
#include <assert.h>
#include <string.h>
#include "color.h"
#include "color2.h"

#ifndef WIN32
extern int  img_status;
#endif

extern HINSTANCE hInst;			  // who we is in this life... available to all!

// The following are used for GetTickCount performance timings.
//static long lStart, lElapsed ;

#define STRICT

// Local Tables
static RGBQUAD const Win16Palette[] = {
	0, 0, 0, 0,
	128, 0, 0, 0,
	0, 128, 0, 0,
	128, 128, 0, 0,
	0, 0, 128, 0,
	128, 0, 128, 0,
	0, 128, 128, 0,
	192, 192, 192, 0,
	128, 128, 128, 0,
	255, 0, 0, 0,
	0, 255, 0, 0,
	255, 255, 0, 0,
	0, 0, 255, 0,
	255, 0, 255, 0,
	0, 255, 255, 0,
	255, 255, 255, 0
};

static RGBQUAD const Std256Palette[] = {
	0, 0, 0, 0,
	0, 0, 192, 0,
	0, 192, 0, 0,
	0, 192, 192, 0,
	192, 0, 0, 0,
	192, 0, 192, 0,
	192, 192, 0, 0,
	192, 192, 192, 0,
	192, 220, 192, 0,
	240, 200, 164, 0,
	0, 0, 128, 0,
	0, 128, 0, 0,
	0, 128, 128, 0,
	128, 0, 0, 0,
	128, 0, 128, 0,
	128, 128, 0, 0,
	170, 189, 244, 0,
	170, 207, 244, 0,
	170, 226, 244, 0,
	170, 244, 244, 0,
	170, 244, 226, 0,
	170, 244, 207, 0,
	170, 244, 189, 0,
	170, 244, 170, 0,
	189, 244, 170, 0,
	207, 244, 170, 0,
	226, 244, 170, 0,
	244, 244, 170, 0,
	244, 226, 170, 0,
	244, 207, 170, 0,
	244, 189, 170, 0,
	244, 170, 170, 0,
	244, 170, 189, 0,
	244, 170, 207, 0,
	244, 170, 226, 0,
	244, 170, 244, 0,
	226, 170, 244, 0,
	207, 170, 244, 0,
	189, 170, 244, 0,
	173, 171, 243, 0,
	133, 159, 239, 0,
	133, 186, 239, 0,
	133, 213, 239, 0,
	133, 239, 239, 0,
	133, 239, 213, 0,
	133, 239, 186, 0,
	133, 239, 159, 0,
	133, 239, 133, 0,
	159, 239, 133, 0,
	186, 239, 133, 0,
	213, 239, 133, 0,
	239, 239, 133, 0,
	239, 213, 133, 0,
	239, 186, 133, 0,
	239, 159, 133, 0,
	239, 133, 133, 0,
	239, 133, 159, 0,
	239, 133, 186, 0,
	239, 133, 213, 0,
	239, 133, 239, 0,
	213, 133, 239, 0,
	186, 133, 239, 0,
	159, 133, 239, 0,
	136, 134, 238, 0,
	96, 131, 234, 0,
	96, 165, 234, 0,
	96, 200, 234, 0,
	96, 234, 234, 0,
	96, 234, 200, 0,
	96, 234, 165, 0,
	96, 234, 131, 0,
	96, 234, 96, 0,
	131, 234, 96, 0,
	165, 234, 96, 0,
	200, 234, 96, 0,
	234, 234, 96, 0,
	234, 200, 96, 0,
	234, 165, 96, 0,
	234, 131, 96, 0,
	234, 96, 96, 0,
	234, 96, 131, 0,
	234, 96, 165, 0,
	234, 96, 200, 0,
	234, 96, 234, 0,
	200, 96, 234, 0,
	165, 96, 234, 0,
	131, 96, 234, 0,
	100, 97, 233, 0,
	58, 101, 228, 0,
	58, 143, 228, 0,
	58, 186, 228, 0,
	58, 228, 228, 0,
	58, 228, 186, 0,
	58, 228, 143, 0,
	58, 228, 101, 0,
	58, 228, 58, 0,
	101, 228, 58, 0,
	143, 228, 58, 0,
	186, 228, 58, 0,
	228, 228, 58, 0,
	228, 186, 58, 0,
	228, 143, 58, 0,
	228, 101, 58, 0,
	228, 58, 58, 0,
	228, 58, 101, 0,
	228, 58, 143, 0,
	228, 58, 186, 0,
	228, 58, 228, 0,
	186, 58, 228, 0,
	143, 58, 228, 0,
	101, 58, 228, 0,
	64, 60, 227, 0,
	30, 77, 215, 0,
	30, 122, 215, 0,
	30, 169, 215, 0,
	30, 215, 215, 0,
	30, 215, 169, 0,
	30, 215, 122, 0,
	30, 215, 77, 0,
	77, 215, 30, 0,
	122, 215, 30, 0,
	169, 215, 30, 0,
	215, 215, 30, 0,
	215, 169, 30, 0,
	215, 122, 30, 0,
	215, 77, 30, 0,
	215, 30, 30, 0,
	215, 30, 77, 0,
	215, 30, 122, 0,
	215, 30, 169, 0,
	215, 30, 215, 0,
	169, 30, 215, 0,
	122, 30, 215, 0,
	77, 30, 215, 0,
	35, 31, 214, 0,
	26, 64, 176, 0,
	24, 101, 177, 0,
	24, 139, 177, 0,
	24, 177, 177, 0,
	24, 177, 139, 0,
	24, 177, 101, 0,
	24, 177, 63, 0,
	63, 177, 24, 0,
	101, 177, 24, 0,
	139, 177, 24, 0,
	177, 177, 24, 0,
	177, 139, 24, 0,
	177, 101, 24, 0,
	177, 63, 24, 0,
	177, 24, 24, 0,
	177, 24, 63, 0,
	177, 24, 101, 0,
	177, 24, 139, 0,
	177, 24, 177, 0,
	139, 24, 177, 0,
	101, 24, 177, 0,
	63, 24, 177, 0,
	30, 26, 176, 0,
	20, 50, 139, 0,
	19, 80, 140, 0,
	19, 111, 140, 0,
	19, 140, 140, 0,
	19, 140, 111, 0,
	19, 140, 80, 0,
	19, 140, 50, 0,
	19, 140, 19, 0,
	80, 140, 19, 0,
	111, 140, 19, 0,
	140, 140, 19, 0,
	140, 111, 19, 0,
	140, 80, 19, 0,
	140, 50, 19, 0,
	140, 19, 50, 0,
	140, 19, 80, 0,
	140, 19, 111, 0,
	140, 19, 140, 0,
	111, 19, 140, 0,
	80, 19, 140, 0,
	50, 19, 140, 0,
	23, 20, 139, 0,
	15, 37, 102, 0,
	14, 81, 103, 0,
	14, 103, 103, 0,
	14, 103, 81, 0,
	14, 103, 58, 0,
	14, 103, 36, 0,
	36, 103, 14, 0,
	81, 103, 14, 0,
	103, 103, 14, 0,
	103, 81, 14, 0,
	103, 58, 14, 0,
	103, 36, 14, 0,
	103, 14, 58, 0,
	103, 14, 81, 0,
	103, 14, 103, 0,
	81, 14, 103, 0,
	58, 14, 103, 0,
	48, 48, 48, 0,
	71, 71, 71, 0,
	95, 95, 95, 0,
	119, 119, 119, 0,
	143, 143, 143, 0,
	167, 167, 167, 0,
	190, 190, 190, 0,
	214, 214, 214, 0,
	238, 238, 238, 0,
	208, 219, 249, 0,
	208, 228, 249, 0,
	208, 239, 249, 0,
	208, 249, 249, 0,
	208, 249, 239, 0,
	208, 249, 228, 0,
	208, 249, 219, 0,
	208, 249, 208, 0,
	219, 249, 208, 0,
	228, 249, 208, 0,
	239, 249, 208, 0,
	249, 249, 208, 0,
	249, 239, 208, 0,
	249, 228, 208, 0,
	249, 219, 208, 0,
	249, 208, 208, 0,
	249, 208, 219, 0,
	249, 208, 228, 0,
	249, 208, 239, 0,
	249, 208, 249, 0,
	239, 208, 249, 0,
	228, 208, 249, 0,
	219, 208, 249, 0,
	209, 208, 249, 0,
	226, 234, 252, 0,
	226, 239, 252, 0,
	226, 245, 252, 0,
	226, 252, 252, 0,
	226, 252, 245, 0,
	226, 252, 239, 0,
	240, 251, 255, 0,
	164, 160, 160, 0,
	128, 128, 128, 0,
	0, 0, 255, 0,
	0, 255, 0, 0,
	0, 255, 255, 0,
	255, 0, 0, 0,
	255, 0, 255, 0,
	255, 255, 0, 0,
	255, 255, 255, 0,
	240, 251, 255, 0,
	164, 160, 160, 0,
	128, 128, 128, 0,
	0, 0, 255, 0,
	0, 255, 0, 0,
	0, 255, 255, 0,
	255, 0, 0, 0,
	255, 0, 255, 0,
	255, 255, 0, 0,
	255, 255, 255, 0
};


// Gamma Correction Tables

// gamma 1.5
static UCHAR const G15[256] = {
	0, 6, 10, 13, 16, 19, 21, 23, 25, 27, 29, 31, 33, 35, 37, 39,
	40, 42, 44, 45, 47, 48, 50, 51, 53, 54, 56, 57, 58, 60, 61, 63,
	64, 65, 67, 68, 69, 70, 72, 73, 74, 75, 77, 78, 79, 80, 81, 83,
	84, 85, 86, 87, 88, 89, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100,
	101, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117,
	118, 119, 120, 121, 122, 123, 124, 125, 125, 126, 127, 128, 129, 130, 131, 132,
	133, 134, 135, 136, 137, 138, 138, 139, 140, 141, 142, 143, 144, 145, 146, 146,
	147, 148, 149, 150, 151, 152, 153, 153, 154, 155, 156, 157, 158, 159, 159, 160,
	161, 162, 163, 164, 164, 165, 166, 167, 168, 169, 169, 170, 171, 172, 173, 173,
	174, 175, 176, 177, 177, 178, 179, 180, 181, 181, 182, 183, 184, 185, 185, 186,
	187, 188, 188, 189, 190, 191, 192, 192, 193, 194, 195, 195, 196, 197, 198, 198,
	199, 200, 201, 201, 202, 203, 204, 204, 205, 206, 207, 207, 208, 209, 210, 210,
	211, 212, 213, 213, 214, 215, 215, 216, 217, 218, 218, 219, 220, 220, 221, 222,
	223, 223, 224, 225, 225, 226, 227, 228, 228, 229, 230, 230, 231, 232, 232, 233,
	234, 235, 235, 236, 237, 237, 238, 239, 239, 240, 241, 241, 242, 243, 244, 244,
	245, 246, 246, 247, 248, 248, 249, 250, 250, 251, 252, 252, 253, 254, 254, 255
};

// gamma 2.0
static UCHAR const G20[256] = {
	0, 16, 23, 28, 32, 36, 39, 42, 45, 48, 50, 53, 55, 58, 60, 62,
	64, 66, 68, 70, 71, 73, 75, 77, 78, 80, 81, 83, 84, 86, 87, 89,
	90, 92, 93, 94, 96, 97, 98, 100, 101, 102, 103, 105, 106, 107, 108, 109,
	111, 112, 113, 114, 115, 116, 117, 118, 119, 121, 122, 123, 124, 125, 126, 127,
	128, 129, 130, 131, 132, 133, 134, 135, 135, 136, 137, 138, 139, 140, 141, 142,
	143, 144, 145, 145, 146, 147, 148, 149, 150, 151, 151, 152, 153, 154, 155, 156,
	156, 157, 158, 159, 160, 160, 161, 162, 163, 164, 164, 165, 166, 167, 167, 168,
	169, 170, 170, 171, 172, 173, 173, 174, 175, 176, 176, 177, 178, 179, 179, 180,
	181, 181, 182, 183, 183, 184, 185, 186, 186, 187, 188, 188, 189, 190, 190, 191,
	192, 192, 193, 194, 194, 195, 196, 196, 197, 198, 198, 199, 199, 200, 201, 201,
	202, 203, 203, 204, 204, 205, 206, 206, 207, 208, 208, 209, 209, 210, 211, 211,
	212, 212, 213, 214, 214, 215, 215, 216, 217, 217, 218, 218, 219, 220, 220, 221,
	221, 222, 222, 223, 224, 224, 225, 225, 226, 226, 227, 228, 228, 229, 229, 230,
	230, 231, 231, 232, 233, 233, 234, 234, 235, 235, 236, 236, 237, 237, 238, 238,
	239, 240, 240, 241, 241, 242, 242, 243, 243, 244, 244, 245, 245, 246, 246, 247,
	247, 248, 248, 249, 249, 250, 250, 251, 251, 252, 252, 253, 253, 254, 254, 255
};

// gamma 2.5
static UCHAR const G25[256] = {
	0, 28, 37, 43, 48, 53, 57, 61, 64, 67, 70, 73, 75, 78, 80, 82,
	84, 86, 88, 90, 92, 94, 96, 97, 99, 101, 102, 104, 105, 107, 108, 110,
	111, 113, 114, 115, 117, 118, 119, 120, 122, 123, 124, 125, 126, 127, 129, 130,
	131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146,
	147, 148, 149, 149, 150, 151, 152, 153, 154, 155, 155, 156, 157, 158, 159, 160,
	160, 161, 162, 163, 164, 164, 165, 166, 167, 167, 168, 169, 170, 170, 171, 172,
	173, 173, 174, 175, 175, 176, 177, 177, 178, 179, 179, 180, 181, 182, 182, 183,
	183, 184, 185, 185, 186, 187, 187, 188, 189, 189, 190, 190, 191, 192, 192, 193,
	194, 194, 195, 195, 196, 197, 197, 198, 198, 199, 199, 200, 201, 201, 202, 202,
	203, 203, 204, 205, 205, 206, 206, 207, 207, 208, 208, 209, 209, 210, 211, 211,
	212, 212, 213, 213, 214, 214, 215, 215, 216, 216, 217, 217, 218, 218, 219, 219,
	220, 220, 221, 221, 222, 222, 223, 223, 224, 224, 225, 225, 226, 226, 227, 227,
	228, 228, 229, 229, 230, 230, 230, 231, 231, 232, 232, 233, 233, 234, 234, 235,
	235, 235, 236, 236, 237, 237, 238, 238, 239, 239, 240, 240, 240, 241, 241, 242,
	242, 243, 243, 243, 244, 244, 245, 245, 246, 246, 246, 247, 247, 248, 248, 248,
	249, 249, 250, 250, 251, 251, 251, 252, 252, 253, 253, 253, 254, 254, 255, 255
};

// gamma 3.0
static UCHAR const G30[256] = {
	0, 40, 51, 58, 64, 69, 73, 77, 80, 84, 87, 89, 92, 95, 97, 99,
	101, 103, 105, 107, 109, 111, 113, 114, 116, 118, 119, 121, 122, 124, 125, 126,
	128, 129, 130, 132, 133, 134, 135, 136, 138, 139, 140, 141, 142, 143, 144, 145,
	146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 157, 158, 159, 160,
	161, 162, 163, 163, 164, 165, 166, 167, 167, 168, 169, 170, 170, 171, 172, 173,
	173, 174, 175, 175, 176, 177, 177, 178, 179, 180, 180, 181, 182, 182, 183, 183,
	184, 185, 185, 186, 187, 187, 188, 188, 189, 190, 190, 191, 191, 192, 193, 193,
	194, 194, 195, 196, 196, 197, 197, 198, 198, 199, 199, 200, 201, 201, 202, 202,
	203, 203, 204, 204, 205, 205, 206, 206, 207, 207, 208, 208, 209, 209, 210, 210,
	211, 211, 212, 212, 213, 213, 214, 214, 215, 215, 216, 216, 216, 217, 217, 218,
	218, 219, 219, 220, 220, 221, 221, 221, 222, 222, 223, 223, 224, 224, 224, 225,
	225, 226, 226, 227, 227, 227, 228, 228, 229, 229, 230, 230, 230, 231, 231, 232,
	232, 232, 233, 233, 234, 234, 234, 235, 235, 236, 236, 236, 237, 237, 237, 238,
	238, 239, 239, 239, 240, 240, 241, 241, 241, 242, 242, 242, 243, 243, 243, 244,
	244, 245, 245, 245, 246, 246, 246, 247, 247, 247, 248, 248, 249, 249, 249, 250,
	250, 250, 251, 251, 251, 252, 252, 252, 253, 253, 253, 254, 254, 254, 255, 255
};

// Bayer Dither
static UCHAR const bayer[8][8] = {
	0, 32, 8, 40, 2, 34, 10, 42,
	48, 16, 56, 24, 50, 18, 58, 26,
	12, 44, 4, 36, 14, 46, 6, 38,
	60, 28, 52, 20, 62, 30, 54, 22,
	3, 35, 11, 43, 1, 33, 9, 41,
	51, 19, 59, 27, 49, 17, 57, 25,
	15, 47, 7, 39, 13, 45, 5, 37,
	63, 31, 55, 23, 61, 29, 53, 21
};

static UCHAR const greymap[256] = {
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,

	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,

	0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x02,
	0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a,

	0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0x10, 0x11, 0x12,
	0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a,

	0x1b, 0x1c, 0x1d, 0x1e, 0x1f, 0x20, 0x20, 0x21,
	0x22, 0x23, 0x23, 0x24, 0x25, 0x27, 0x27, 0x28,

	0x29, 0x2a, 0x2b, 0x2c, 0x2d, 0x2e, 0x2f, 0x2f,
	0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37,

	0x38, 0x39, 0x3a, 0x3a, 0x3b, 0x3c, 0x3d, 0x3e,
	0x3f, 0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46,

	0x47, 0x48, 0x49, 0x4a, 0x4b, 0x4c, 0x4d, 0x4e,
	0x50, 0x51, 0x52, 0x53, 0x55, 0x56, 0x57, 0x58,

	0x59, 0x5a, 0x5b, 0x5d, 0x5e, 0x5f, 0x60, 0x61,
	0x63, 0x64, 0x65, 0x66, 0x67, 0x69, 0x6a, 0x6b,

	0x6c, 0x6e, 0x70, 0x72, 0x73, 0x74, 0x76, 0x78,
	0x7a, 0x7c, 0x7e, 0x80, 0x82, 0x84, 0x86, 0x88,

	0x8a, 0x8c, 0x8f, 0x91, 0x93, 0x95, 0x98, 0x9a,
	0x9c, 0x9f, 0xa1, 0xa4, 0xa6, 0xa9, 0xab, 0xae,

	0xb0, 0xb2, 0xb3, 0xb5, 0xb7, 0xb9, 0xba, 0xbc,
	0xbd, 0xbe, 0xc0, 0xc2, 0xc4, 0xc6, 0xc8, 0xca,

	0xcc, 0xce, 0xd0, 0xd2, 0xd4, 0xd6, 0xd9, 0xdb,
	0xdd, 0xe0, 0xe3, 0xe6, 0xe8, 0xeb, 0xed, 0xef,

	0xf2, 0xf5, 0xf8, 0xfb, 0xfe, 0xfe, 0xfe, 0xfe,
	0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe,

	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,

	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff
};

static USHORT const SqSum[256] = {
	0, 1, 4, 9, 16, 25, 36, 49, 64, 81,
	100, 121, 144, 169, 196, 225, 256, 289, 324, 361,
	400, 441, 484, 529, 576, 625, 676, 729, 784, 841,
	900, 961, 1024, 1089, 1156, 1225, 1296, 1369, 1444, 1521,
	1600, 1681, 1764, 1849, 1936, 2025, 2116, 2209, 2304, 2401,
	2500, 2601, 2704, 2809, 2916, 3025, 3136, 3249, 3364, 3481,
	3600, 3721, 3844, 3969, 4096, 4225, 4356, 4489, 4624, 4761,
	4900, 5041, 5184, 5329, 5476, 5625, 5776, 5929, 6084, 6241,
	6400, 6561, 6724, 6889, 7056, 7225, 7396, 7569, 7744, 7921,
	8100, 8281, 8464, 8649, 8836, 9025, 9216, 9409, 9604, 9801,
	10000, 10201, 10404, 10609, 10816, 11025, 11236, 11449, 11664, 11881,
	12100, 12321, 12544, 12769, 12996, 13225, 13456, 13689, 13924, 14161,
	14400, 14641, 14884, 15129, 15376, 15625, 15876, 16129, 16384, 16641,
	16900, 17161, 17424, 17689, 17956, 18225, 18496, 18769, 19044, 19321,
	19600, 19881, 20164, 20449, 20736, 21025, 21316, 21609, 21904, 22201,
	22500, 22801, 23104, 23409, 23716, 24025, 24336, 24649, 24964, 25281,
	25600, 25921, 26244, 26569, 26896, 27225, 27556, 27889, 28224, 28561,
	28900, 29241, 29584, 29929, 30276, 30625, 30976, 31329, 31684, 32041,
	32400, 32761, 33124, 33489, 33856, 34225, 34596, 34969, 35344, 35721,
	36100, 36481, 36864, 37249, 37636, 38025, 38416, 38809, 39204, 39601,
	40000, 40401, 40804, 41209, 41616, 42025, 42436, 42849, 43264, 43681,
	44100, 44521, 44944, 45369, 45796, 46225, 46656, 47089, 47524, 47961,
	48400, 48841, 49284, 49729, 50176, 50625, 51076, 51529, 51984, 52441,
	52900, 53361, 53824, 54289, 54756, 55225, 55696, 56169, 56644, 57121,
	57600, 58081, 58564, 59049, 59536, 60025, 60516, 61009, 61504, 62001,
	62500, 63001, 63504, 64009, 64516, 65025
};

#define PIN_OFFSET (256)  // offset to identity lookup
// static int const pin[] = {
int pin[] = {
// pin [-256..-1] to 0
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
// [0..255] is identity lookup
0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31,
32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47,
48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63,
64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79,
80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95,
96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111,
112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127,
128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143,
144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159,
160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175,
176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191,
192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207,
208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223,
224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239,
240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255,
// pin [256..512] to 255
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255
};

// Module prototypes:
static ULONG ColorDist(int, int, int, PALETTE FAR *);
static void NEAR PASCAL SetGamma(UCHAR FAR *, int);
static LPWORD NEAR PASCAL LoadFixedPalette(UINT);
static void NEAR PASCAL DiffuseErr(int, int, int);

// //////////////////////////////////////////////////////////////////////////
//
// int IMAPI ImgBrightness(HIMAGE hImage, int nBrightness)
//
// Adjusts the image's brightness by the specified amount. The range for
// nBrightness is -255 -> 255.
//
// Globals: img_status
//
// //////////////////////////////////////////////////////////////////////////

int IMAPI   ImgBrightness(HANDLE hImage, int nBrightness)
{
	LPINTERNALINFO lpII;
	LPBITMAPINFO lpbi;
	HPSTR       lpBits;
	int         Color;
	UINT        nColors, i;

	if (!(lpII = (LPINTERNALINFO) GlobalLock(hImage))) {
		SetError(NULL, IMG_INV_HAND, 0, NULL, hInst);
		return IMG_ERR;
	}
	if (lpII->bFlags & IMG_DISP_VECTOR) {
		SetError(NULL, IMG_BAD_TYPE, 0, NULL, hInst);
		GlobalUnlock(hImage);
		return IMG_ERR;
	}
	if (!lpII->pDIB) {
		if (InternalLoad(lpII, NULL, &lpII->pDIB, &lpII->hWMF) != IMG_OK) {
			GlobalUnlock(hImage);
			return IMG_ERR;
		}
	}
	if (lpII->hBitmap) {
		DeleteBitmap(lpII->hBitmap);
		lpII->hBitmap = NULL;
	}

	lpbi = lpII->pDIB->GetLPBI();


	if (lpII->lpbi->bmiHeader.biBitCount == 24) {
		UINT        nSrcPad, x, y;

		lpBits = (HPSTR) lpII->pDIB->GetDataPtr();

		nSrcPad = lpII->pDIB->GetRowPadding();

		// Need to adjust every pixel in the image.
		for (y = (UINT) lpII->lpbi->bmiHeader.biHeight; y--;) {
			for (x = (UINT) lpII->lpbi->bmiHeader.biWidth; x--;) {
				Color = *lpBits;	  // Do Red

				Color += nBrightness;

				*lpBits++ = pin[PIN_OFFSET + Color];

				Color = *lpBits;	  // Do Green

				Color += nBrightness;
				*lpBits++ = pin[PIN_OFFSET + Color];

				Color = *lpBits;	  // Do Blue

				Color += nBrightness;
				*lpBits++ = pin[PIN_OFFSET + Color];

			}
			lpBits += nSrcPad;
		}
	} else {
		if (lpbi->bmiHeader.biClrUsed)
			nColors = (UINT) lpbi->bmiHeader.biClrUsed;
		else
			nColors = 1 << lpbi->bmiHeader.biBitCount;

		// Simply adjust each palette entry.
		for (i = 0; i < nColors; i++) {
			Color = lpbi->bmiColors[i].rgbRed;
			Color += nBrightness;
			lpbi->bmiColors[i].rgbRed = pin[PIN_OFFSET + Color];

			Color = lpbi->bmiColors[i].rgbGreen;
			Color += nBrightness;
			lpbi->bmiColors[i].rgbGreen = pin[PIN_OFFSET + Color];

			Color = lpbi->bmiColors[i].rgbBlue;
			Color += nBrightness;
			lpbi->bmiColors[i].rgbBlue = pin[PIN_OFFSET + Color];
		}

		// Update the palette in the Image structure
		_fmemcpy(lpII->lpbi, lpbi, (int) BITSOFFSET((LPBITMAPINFOHEADER) lpbi));
	}

	GlobalUnlock(hImage);
	return (IMG_OK);
}

// //////////////////////////////////////////////////////////////////////////
//
// int IMAPI ImgGamma(HIMAGE hImage, int nGamma)
//
// Gamma corrects the image. The nGamma range is 10(1.0)  -> 50(5.0)
//
// Globals: img_status
//
// //////////////////////////////////////////////////////////////////////////

int IMAPI   ImgGamma(HANDLE hImage, int nGamma)
{
	LPINTERNALINFO lpII;
	LPBITMAPINFO lpbi;
	HPSTR       lpBits;
	UCHAR       GammaCorrection[256];

	if ((nGamma < 10) || (nGamma > 50)) {		
		SetError(NULL, IMG_BAD_PARAM, 0, NULL,hInst);  
		return (IMG_ERR);
	}
	if (!(lpII = (LPINTERNALINFO) GlobalLock(hImage))) {
		SetError(NULL, IMG_INV_HAND, 0, NULL, hInst);
		return (IMG_ERR);
	}
	if (lpII->bFlags & IMG_DISP_VECTOR) {
		SetError(NULL, IMG_BAD_TYPE, 0, NULL,hInst);
		GlobalUnlock(hImage);
		return (IMG_ERR);
	}
	if (!lpII->pDIB ) {
		if (InternalLoad(lpII, NULL, &lpII->pDIB, &lpII->hWMF) != IMG_OK) {
			GlobalUnlock(hImage);
			return (IMG_ERR);
		}
	}
	if (lpII->hBitmap) {
		DeleteObject(lpII->hBitmap);
		lpII->hBitmap = 0;
	}

	lpbi = lpII->pDIB->GetLPBI();
	 
	SetGamma(GammaCorrection, nGamma);		

	if (lpII->lpbi->bmiHeader.biBitCount == 24) {
		UINT        nSrcPad, x, y;

		lpBits = (HPSTR) lpII->pDIB->GetDataPtr();

		nSrcPad = lpII->pDIB->GetRowPadding();

		for (y = (UINT) lpII->lpbi->bmiHeader.biHeight; y--;) {
			for (x = (UINT) lpII->lpbi->bmiHeader.biWidth; x--;) {
				*lpBits = GammaCorrection[*lpBits];	// Blue

				lpBits++;

				*lpBits = GammaCorrection[*lpBits];	// Green

				lpBits++;

				*lpBits = GammaCorrection[*lpBits];	// Red

				lpBits++;
			}
			lpBits += nSrcPad;
		}
	} else {
		UCHAR       Color;
		int         nColors, i;

		nColors = (int) (lpbi->bmiHeader.biClrUsed ? lpbi->bmiHeader.biClrUsed : (1 << lpbi->bmiHeader.biBitCount));

		for (i = 0; i < nColors; i++) {
			Color = lpbi->bmiColors[i].rgbRed;
			lpbi->bmiColors[i].rgbRed = GammaCorrection[Color];

			Color = lpbi->bmiColors[i].rgbGreen;
			lpbi->bmiColors[i].rgbGreen = GammaCorrection[Color];

			Color = lpbi->bmiColors[i].rgbBlue;
			lpbi->bmiColors[i].rgbBlue = GammaCorrection[Color];
		}
	}
	GlobalUnlock(hImage);
	return (IMG_OK);
}


HANDLE IMAPI InternalReduceColors(HANDLE hImg, UINT nColors, DWORD lFlags, HPALETTE hPal )
{
	HANDLE      hNewImage = NULL;
	LPINTERNALINFO lpII;


	if (!(lpII = (LPINTERNALINFO) GlobalLock(hImg))) {
		SetError(NULL, IMG_INV_HAND, 0, NULL, hInst);
		return (NULL);
	}

	if( lpII->bFlags & IMG_DISP_VECTOR ) {					// Make sure its a raster image
		SetError(NULL, IMG_NSUPPORT, 0, NULL, hInst);
		GlobalUnlock(hImg);
		return (NULL);
	}

	// If we are given a palette we will reduce to that palettes # of colors.
	if ( hPal )
		GetObject ( hPal, sizeof(nColors), & nColors ) ;

	if( !nColors ) {
		SetError(NULL, IMG_BAD_PARAM, 0, NULL, hInst);
		GlobalUnlock(hImg);
		return (NULL);
	}

	if (!lpII->pDIB) {
		if (InternalLoad(lpII, NULL, &lpII->pDIB, &lpII->hWMF) != IMG_OK) {
			GlobalUnlock(hImg);
			return (NULL);
		}
	}
	if (lpII->hBitmap) {
		DeleteObject(lpII->hBitmap);
		lpII->hBitmap = 0;
	}


	if (nColors == 2) {
		if (lpII->lpbi->bmiHeader.biBitCount == 1) {
			SetError(NULL, IMG_NSUPPORT, 0, NULL, hInst);
			GlobalUnlock(hImg);
			return (NULL);
		}

		hNewImage = DitherToBW(lpII, lFlags);
	} else if (nColors <= 256) {
		hNewImage = ReduceColorToColor(lpII, nColors, lFlags, hPal );
	}

	GlobalUnlock(hImg);

	return hNewImage ;
}

////////////////////////////////////////////////////////////////////////////
//
// HANDLE IMAPI ImgReduceColors(HIMAGE hImage,UINT nColors,DWORD lFlags)
//
// Reduces the image to the a new image containing the specified number of
// Colors. lFlags can be:
//
// IMG_GRAYSCALE - Make a 256 level greyscale image
// IMG_BAYER - Use Bayer Dither
// IMG_BURKES - Use Burkes Dither
// IMG_FLOYD - Use Floyd-Steinberg Dither
//
// Globals: img_status
//
// //////////////////////////////////////////////////////////////////////////

HANDLE IMAPI ImgReduceColors(HANDLE hImg, UINT nColors, DWORD lFlags)
{
	return InternalReduceColors ( hImg, nColors, lFlags, 0 ) ;
}


HANDLE IMAPI ImgReduceColorsPal(HANDLE hImg, DWORD lFlags, HPALETTE hPal )
{
	return InternalReduceColors ( hImg, 0, lFlags, hPal ) ;
}

// //////////////////////////////////////////////////////////////////////////
//
// Internal Helper Functions
//
// //////////////////////////////////////////////////////////////////////////


HANDLE PASCAL DitherToBW(LPINTERNALINFO lpII, DWORD nFlags)
{
	UINT        x, y;
	int         Color;
	PDIB		pNewDIB;
	LPBITMAPINFO lpbi;
	LPBITMAPINFOHEADER lpbiHdr;
	LPRGBQUAD   pRGB;
	UCHAR       mask;
	DWORD       lInRowSize, lOutRowSize;
	UINT        nSrcPad, nDstPad;
	int         nBits, nIdx;
	HPSTR       lpSrcBits, lpDstBits;
	HPSTR       lpIn, lpOut;
	int         nErr;
	short NEAR *ErrLine1, NEAR * ErrLine2, NEAR * CurLine, NEAR * NxtLine;

	lpbi = lpII->lpbi;
	nBits = lpbi->bmiHeader.biBitCount;

	lpSrcBits = (HPSTR) lpII->pDIB->GetDIBPtr();

	pRGB = (LPRGBQUAD) (lpSrcBits + ((LPBITMAPINFOHEADER) lpSrcBits)->biSize);

	lpSrcBits = (HPSTR) lpII->pDIB->GetDataPtr();

	pNewDIB = new DIB( lpbi->bmiHeader.biWidth, lpbi->bmiHeader.biHeight, 1 );

	if( !pNewDIB->IsValid() ) {
		delete pNewDIB;
		SetError(NULL, IMG_NOMEM, 0, NULL, hInst);
		return NULL;
	}
	
	lInRowSize = lpII->pDIB->GetRowSize();
	lOutRowSize = pNewDIB->GetRowSize();
	
	lpbiHdr = (LPBITMAPINFOHEADER) pNewDIB->GetLPBI();
	lpDstBits = (HPSTR) pNewDIB->GetDataPtr();		//= sizeof(BITMAPINFOHEADER) + 2 * sizeof(RGBQUAD);

	// Set the  resolution fields of the new DIB
	lpbiHdr->biXPelsPerMeter = lpbi->bmiHeader.biXPelsPerMeter;
	lpbiHdr->biYPelsPerMeter = lpbi->bmiHeader.biYPelsPerMeter;

	// Source and destination are same widths
	nSrcPad = lpII->pDIB->GetRowPadding();	//l(UINT) (lInRowSize - (lpbiHdr->biWidth * nBits + 7) / 8);
	nDstPad = pNewDIB->GetRowPadding();		//(UINT) (lOutRowSize - (lpbiHdr->biWidth + 7) / 8);

	// Allocate the Error buffers
	// Add 6 ints at the end since we actually write width + 2 ints & we shift the pointers over so we
	// don't have to check the [x-2] & [x-1] accesses.

	x = (UINT) lpbiHdr->biWidth + 6;
	if (!(ErrLine1 = (short NEAR *) LocalAlloc(LPTR, x * sizeof(short) * 2))) {
		SetError(NULL, IMG_NOMEM, 0, NULL, hInst);
		return (NULL);
	}
	ErrLine2 = ErrLine1 + x;

	CurLine = ErrLine1 + 2;		  // Move the Pointer to we have two junk entries before it

	NxtLine = ErrLine2 + 4;		  // Same thing but we have to move it by 4 shorts

	// Now we can dither the image, finally!

	for (y = 0; y < (UINT) lpbi->bmiHeader.biHeight; y++) {
		mask = 0x80;

		lpIn = lpSrcBits + ((lpbi->bmiHeader.biHeight - 1) - y) * lInRowSize;
		lpOut = lpDstBits + ((lpbi->bmiHeader.biHeight - 1) - y) * lOutRowSize;


		for (x = 0; x < (UINT) lpbi->bmiHeader.biWidth; x++) {
			// CAR: Can we come pretty close with shifts instead of multiplies?
			if (nBits == 8) {
				nIdx = *lpIn++;
				Color = pRGB[nIdx].rgbRed * 30 + pRGB[nIdx].rgbGreen * 59 + pRGB[nIdx].rgbBlue * 11;
			} else if (nBits == 4) {
				if (x & 1)
					nIdx = *lpIn++ & 0x0f;
				else
					nIdx = *lpIn >> 4;
				Color = pRGB[nIdx].rgbRed * 30 + pRGB[nIdx].rgbGreen * 59 + pRGB[nIdx].rgbBlue * 11;
			} else {					  // Must be 24 Bit

				Color = *lpIn++ * 11;	// B

				Color += *lpIn++ * 59;	// G

				Color += *lpIn++ * 30;	// R

			}

			Color = greymap[Color / 100];

			if (!mask) {
				mask = 0x80;
				lpOut++;
			}
			if (nFlags & IMG_BAYER) {
				if (((Color + 1) >> 2) > bayer[x & 7][y & 7])
					*lpOut |= mask;
			} else if (nFlags & (IMG_BURKES | IMG_FLOYD)) {

				if (nFlags & IMG_BURKES) {
					Color += CurLine[x] / 32;	// Add Error to this pixel

					Color = pin[PIN_OFFSET + Color];

					if (Color > 127) {
						*lpOut |= mask;
						nErr = Color - 0xff;
					} else
						nErr = Color;

					CurLine[x + 1] += (8 * nErr);
					CurLine[x + 2] += (4 * nErr);

					NxtLine[x - 2] += (2 * nErr);

					NxtLine[x - 1] += (4 * nErr);

					NxtLine[x] += (8 * nErr);
					NxtLine[x + 1] += (4 * nErr);
					NxtLine[x + 2] += (2 * nErr);
				} else {				  // Must be Floyd-Steinberg

					Color += CurLine[x] / 16;	// Add Error to this pixel

					Color = pin[PIN_OFFSET + Color];

					if (Color > 127) {
						*lpOut |= mask;
						nErr = Color - 0xff;
					} else
						nErr = Color;

					CurLine[x + 1] += (7 * nErr);

					NxtLine[x - 1] += (3 * nErr);

					NxtLine[x] += (5 * nErr);

					NxtLine[x + 1] += nErr;
				}
			} else {					  // No Dithering

				if (Color > 127)
					*lpOut |= mask;
			}

			mask >>= 1;
		}

		// Exchange the error accum lines
		if (nFlags & (IMG_BURKES | IMG_FLOYD)) {
			short NEAR *tmp = NxtLine;
			NxtLine = CurLine;
			CurLine = tmp;
			// reset error entries
			_fmemset(NxtLine - 2, 0, ((size_t) lpbiHdr->biWidth + 2) * sizeof(short));
		}
	}

	LocalFree((HLOCAL) ErrLine1);

	return ImgFromPDIB(pNewDIB);
}

static void NEAR PASCAL QuickPalSort(PALETTE NEAR * lpPal, int first, int last)
{
	int         indx, jndx;
	PALETTE     temp;

	indx = first;

	if (indx < last) {
		// Check for already sorted list
		while ((lpPal[indx].Red < lpPal[indx + 1].Red) && indx < last)
			indx++;
	}
	if (indx == last)
		return;

	jndx = last;
	temp = lpPal[indx];

	while (indx != jndx) {
		while (indx != jndx && (temp.Red < lpPal[jndx].Red))
			jndx--;

		if (indx != jndx) {
			lpPal[indx] = lpPal[jndx];
			indx++;

			while (indx != jndx && (lpPal[indx].Red < temp.Red))
				indx++;

			if (indx != jndx) {
				lpPal[jndx] = lpPal[indx];
				jndx--;
			}
		}
	}
	lpPal[indx] = temp;
	if (first < jndx)
		QuickPalSort(lpPal, first, jndx - 1);
	if (indx < last)
		QuickPalSort(lpPal, indx + 1, last);
}

static ULONG ColorDist(int red, int green, int blue, PALETTE FAR * palt)
{
	ULONG       sum;
	int         dif;

	dif = red - palt->Red;
	if (dif < 0)
		dif = -dif;
	sum = SqSum[dif];

	dif = green - palt->Green;
	if (dif < 0)
		dif = -dif;
	sum += SqSum[dif];

	dif = blue - palt->Blue;
	if (dif < 0)
		dif = -dif;
	sum += SqSum[dif];

	return (sum);
}

PCDSTUFF PASCAL InitTransPal(UINT nColors, LPRGBQUAD lpPal)
{
	UINT        i, j;
	PCDSTUFF    pStuff;
	PALETTE NEAR *pal;

	if (!(pStuff = (PCDSTUFF) LocalAlloc(LPTR, sizeof(CDSTUFF)))) {
		return (NULL);
	}
	pStuff->nColors = nColors;
	pal = pStuff->Pals;

	// Make a copy of the palette
	for (i = 0; i < nColors; i++) {
		pal[i].num = i;
		pal[i].Red = lpPal[i].rgbRed;
		pal[i].Green = lpPal[i].rgbGreen;
		pal[i].Blue = lpPal[i].rgbBlue;
	}
	QuickPalSort(pal, 0, nColors - 1);

	// Build the quick index
	for (i = 0, j = 0; i < 256; i++) {
		while (pal[j].Red < i) {
			if (++j >= nColors) {
				j = nColors - 1;
				break;
			}
		}
		pStuff->redindex[i] = j;
	}
	return (pStuff);
}

int PASCAL Closest(int red, int green, int blue, PCDSTUFF pStuff)
{
	int         dif;
	int         left, right, index;
	ULONG       least, sum;
	int         nColors = (int) pStuff->nColors;
	PALETTE *pal = pStuff->Pals;

	left = right = index = pStuff->redindex[red];
	least = ColorDist(red, green, blue, &pal[index]);

	while ((left >= 0) || (right < nColors)) {
		if (--left >= 0) {
			dif = red - pal[left].Red;
			if (dif < 0)
				dif = -dif;

			if (SqSum[dif] > least)
				left = -1;
			else {
				sum = ColorDist(red, green, blue, &pal[left]);
				if (sum < least) {
					least = sum;
					index = left;
				}
			}
		}
		if (++right < nColors) {
			dif = red - pal[right].Red;
			if (dif < 0)
				dif = -dif;
			if (SqSum[dif] > least)
				right = nColors;
			else {
				sum = ColorDist(red, green, blue, &pal[right]);
				if (sum < least) {
					least = sum;
					index = right;
				}
			}
		}
	}

	return (pal[index].num);
}

static void NEAR PASCAL DitherToColor(LPBITMAPINFO lpInBMI, HPSTR lpInBits,
										            LPBITMAPINFO lpOutBMI, HPSTR lpOutBits, LPWORD lpLookup)
{
	int         x, y, width;
	LPRGBQUAD   srcRGB, dstRGB;
	DWORD       lInRowSize, lOutRowSize;
	UINT        nInBits, nOutBits;
	HPSTR       lpIn, lpOut;
	int         errr, errg, errb;
	int NEAR   *errorBase, NEAR * error;
	PCDSTUFF    pStuff;

	register int red, green, blue;
	int         nIdx, nColorIdx;

	nInBits = lpInBMI->bmiHeader.biBitCount;
	nOutBits = lpOutBMI->bmiHeader.biBitCount;
	width = (int) lpInBMI->bmiHeader.biWidth;

	lInRowSize = ROWSIZE(width, nInBits);
	lOutRowSize = ROWSIZE(width, nOutBits);

	srcRGB = lpInBMI->bmiColors;
	dstRGB = lpOutBMI->bmiColors;

	if (!(pStuff = InitTransPal((nOutBits == 4) ? 16 : 256, dstRGB))) {
		SetError(NULL, IMG_NOMEM, 0, NULL, hInst);
		return;
	}
	// Allocate the error buffer
	// Add 2 ints at the end since we actually write width + 2 ints
	if (!(errorBase = (int NEAR *) LocalAlloc(LPTR, (width + 2) * 3 * sizeof(int)))) {
		SetError(NULL, IMG_NOMEM, 0, NULL, hInst);
		return;
	}
	lpInBits += (lpInBMI->bmiHeader.biHeight - 1) * lInRowSize;
	lpOutBits += (lpOutBMI->bmiHeader.biHeight - 1) * lOutRowSize;

	// Now we can dither the image, finally!
	for (y = (int) lpInBMI->bmiHeader.biHeight; --y >= 0;) {

		lpIn = lpInBits;
		lpOut = lpOutBits;

		// reset error accumulator state
		error = errorBase + 3;
		errr = error[0];
		errg = error[1];
		errb = error[2];
		error[0] = error[1] = error[2] = 0;

		for (x = 0; x < width; x++) {
			if (nInBits == 4) {
				if (x & 1)
					nIdx = *lpIn++ & 0x0f;
				else
					nIdx = (*lpIn & 0xf0) >> 4;

				red = srcRGB[nIdx].rgbRed;
				green = srcRGB[nIdx].rgbGreen;
				blue = srcRGB[nIdx].rgbBlue;
			} else if (nInBits == 8) {
				nIdx = *lpIn++;
				red = srcRGB[nIdx].rgbRed;
				green = srcRGB[nIdx].rgbGreen;
				blue = srcRGB[nIdx].rgbBlue;
			} else {					  // Must be 24 Bit
#ifdef WIN32
				blue = lpIn[0];
				green = lpIn[1];
				red = lpIn[2];
				lpIn += 3;
#else
				blue = *lpIn++;	  // B

				green = *lpIn++;	  // G

				red = *lpIn++;		  // R
#endif
			}

			// Add the error to this pixel, and pin the value within [0..255]
			red = pin[PIN_OFFSET + red + (errr >> 4)];	// divide by 16 here (>>4)

			green = pin[PIN_OFFSET + green + (errg >> 4)];
			blue = pin[PIN_OFFSET + blue + (errb >> 4)];

			nColorIdx = ((red & 0x00F8) << 7) | ((green & 0x00F8) << 2) | (blue >> 3);

			if ((nIdx = lpLookup[nColorIdx]) == 0xffffU) {
				nIdx = Closest(red, green, blue, pStuff);
				lpLookup[nColorIdx] = nIdx;
			}
			if (nOutBits == 4) {
				if (x & 1)
					*lpOut++ |= nIdx;
				else
					*lpOut = nIdx << 4;
			} else
				*lpOut++ = nIdx;

			// Create error by diffing what we wanted with what we got
			red -= dstRGB[nIdx].rgbRed;
			green -= dstRGB[nIdx].rgbGreen;
			blue -= dstRGB[nIdx].rgbBlue;

			// Distribute the error. I delay the '/16' part until the actual
			// lookup... this reduces the number of divisions/shifts (by 9)
			// per pixel.
			error[-3] += red * 3;  // add 3/16 to pixel below and left

			error[-2] += green * 3;
			error[-1] += blue * 3;

			error[0] += red * 5;	  // add 5/16 to pixel below

			error[1] += green * 5;
			error[2] += blue * 5;

			errr = error[3] + red * 7;	// preserve next pixel's error

			errg = error[4] + green * 7;	// and add on 7/16 of this pixel's error

			errb = error[5] + blue * 7;

			error[3] = red;		  // send 1/16 to pixel below and right

			error[4] = green;
			error[5] = blue;

			error += 3;				  // then move to next pixel

		}
		lpInBits -= lInRowSize;
		lpOutBits -= lOutRowSize;
	}
	LocalFree((HLOCAL) errorBase);
	LocalFree((HLOCAL) pStuff);
}



/* returns the voltage value, given gamma and intensity I */
/* interpolates using the above tables */

static UCHAR NEAR PASCAL GammaFunc(int gamma, int I)
{
	int         k, dg, a, b;

	if (gamma >= 30)
		return (G30[I]);
	else if (gamma <= 10)
		return (I);

	k = gamma / 5;
	dg = gamma - 5 * k;

	a = 2 * dg;
	b = 10 - a;

	switch (k) {
		case 2:						  /* gamma 11 to 14 */
			return (a * G15[I] + b * I + 5) / 10;

		case 3:						  /* gamma 15 to 19 */
			return (a * G20[I] + b * G15[I] + 5) / 10;

		case 4:						  /* gamma 20 to 24 */
			return (a * G25[I] + b * G20[I] + 5) / 10;

		case 5:						  /* gamma 25 to 29 */
			return (a * G30[I] + b * G25[I] + 5) / 10;
	}
	return (I);
}

/* sets the GammaCorrection table */

static void NEAR PASCAL SetGamma(UCHAR FAR * table, int gamma)
{
	int         k = 256;

	while (k--)
		table[k] = GammaFunc(gamma, k);
}

HANDLE PASCAL ReduceColorToColor(LPINTERNALINFO lpII, UINT nColors, DWORD nFlags, HPALETTE hPal )
{
	PDIB		pNewDIB;
	LPBITMAPINFO lpInBMI, lpOutBMI;
	HPSTR       lpInBits, lpOutBits;
	LPWORD  lpLookup ;
	int			nBits;
	UINT    uPalCount ;

	if (nFlags & IMG_GRAYSCALE)
		return (ColorToGrayscale(lpII));

	// Get our input pointers

	lpInBMI = lpII->pDIB->GetLPBI();  

	// Don't try increasing the color depth
	if (nColors > 256 || (lpInBMI->bmiHeader.biBitCount < 8
								 && nColors > (1U << lpInBMI->bmiHeader.biBitCount))) {
		SetError(NULL, IMG_NSUPPORT, 0, NULL, hInst);
		return (NULL);
	}
	
	lpInBits = (HPSTR) lpII->pDIB->GetDataPtr();
  uPalCount = nColors <= 16 ? 16 : 256 ;
	// Get the destination palette and color mapping lookup table
	if ( ! hPal ) 
		{
			if (nFlags & IMG_FIXEDPALETTE) 
				{
					// load lookup table & palette from resources for destination colors
					lpLookup = LoadFixedPalette(nColors);
				}
			else 
				{
					if ( ( nColors == ( 1 << lpInBMI->bmiHeader.biBitCount )) ) {
						SetError(NULL, IMG_NSUPPORT, 0, NULL, hInst);
						return (NULL);
					}
					if ( nFlags & IMG_MEDIANCUT ) {
							// Generate the optimized palette and build lookup table
							lpLookup = GenOptimizedPalette(lpInBMI, lpInBits, nColors);
					} else {
							// Generate the octree palette 
							lpLookup = GenOctreePalette(lpInBMI, lpInBits, nColors);
					}
				}
		}
	else  // we have a user supplied palette
		{
			PALETTEENTRY palEntry [256] ;
			int nTmp, i;

			int iCols = GetPaletteEntries ( hPal, 0, nColors, palEntry ) ;
			
			// Swap R & B entries since we copy this and assum its an array of RGBQUADS which swap r/b.
			for( i = 0; i < iCols; i++ ) {
				nTmp = palEntry[i].peRed;
				palEntry[i].peRed = palEntry[i].peBlue;
				palEntry[i].peBlue = nTmp;
			}


			if (!(lpLookup = (LPWORD) GlobalAllocPtr(GMEM_FIXED, LOOKUPSIZE + NEWPALSIZE)))
				return ( NULL ) ;
#ifdef WIN32
			memset(lpLookup,0xff,LOOKUPSIZE);
#else
			// fill whole segment, assuming lpLookup's offset is 0.
			for (i = 32768; i--; ) lpLookup[i] = 0xffff;
#endif

			_fmemcpy( ((HPSTR) lpLookup) + LOOKUPSIZE, palEntry, uPalCount * sizeof(RGBQUAD) ) ;
		}

	if (!lpLookup)
		return NULL;

	if (nColors <= 16)
		nBits = 4;
	else if (nColors <= 256)
		nBits = 8;
	else
		nBits = 24;

	// allocate destination bitmap for specified number of colors
	pNewDIB = new DIB( lpInBMI->bmiHeader.biWidth, lpInBMI->bmiHeader.biHeight, nBits );

	if( !pNewDIB->IsValid() ) {
		delete pNewDIB;
		return NULL;
	}

	lpOutBMI = pNewDIB->GetLPBI();

	// Fill in new BMI header palette info
	_fmemcpy ( lpOutBMI->bmiColors, ((HPSTR) lpLookup) + LOOKUPSIZE, 
		         uPalCount * sizeof(RGBQUAD) ) ;

	// Set the  resolution fields of the new DIB
	lpOutBMI->bmiHeader.biXPelsPerMeter = lpInBMI->bmiHeader.biXPelsPerMeter;
	lpOutBMI->bmiHeader.biYPelsPerMeter = lpInBMI->bmiHeader.biYPelsPerMeter;

	lpOutBits =(HPSTR) pNewDIB->GetDataPtr();

	if (nFlags & (IMG_FLOYD | IMG_BURKES)) {
		// Dither Image to Palette
		DitherToColor(lpInBMI, lpInBits, lpOutBMI, lpOutBits, lpLookup);
	} else {
		// Remap image to new palette
		RemapPixels(lpInBMI, lpInBits, lpOutBMI, lpOutBits, lpLookup);
	}

	GlobalFreePtr(lpLookup);

	return (ImgFromPDIB(pNewDIB));
}

static LPWORD NEAR PASCAL LoadFixedPalette(UINT nColors)
{
	HRSRC       hRsc;
	HGLOBAL     hGlobal;
	HPSTR       lpRes;
	LPWORD      lpLookup;

	if (!(lpLookup = (LPWORD) GlobalAllocPtr(GMEM_FIXED, LOOKUPSIZE + NEWPALSIZE))) {
		return (NULL);
	}
	if (nColors <= 16) {
		_fmemcpy(((HPSTR) lpLookup) + LOOKUPSIZE, Win16Palette, 16 * sizeof(RGBQUAD));
		hRsc = FindResource(hInst, "LKUP16", "RAWDATA");
	} else {
		_fmemcpy(((HPSTR) lpLookup) + LOOKUPSIZE, Std256Palette, 256 * sizeof(RGBQUAD));
		hRsc = FindResource(hInst, "LKUP256", "RAWDATA");
	}

	assert(hRsc);
	// Load the lookup table from its resource and copy into new memory
	hGlobal = LoadResource(hInst, hRsc);
	lpRes = (HPSTR) LockResource(hGlobal);
	hmemcpy(lpLookup, lpRes, LOOKUPSIZE);
	FreeResource(hGlobal);

	return (lpLookup);
}

HANDLE PASCAL ColorToGrayscale(LPINTERNALINFO lpII)
{
	HANDLE      hNewImage;
	PDIB		pNewDIB;
	HPSTR       lpSrcDIB, lpDstDIB;
	HPSTR       lpSrc, lpDst;
	LPRGBQUAD   lpSrcPal, lpDstPal;
	LPBITMAPINFOHEADER lpbi;
	UINT        nGray, x, y, nSrcPad, nDstPad, i;

	// Can't convert B/W to Grayscale
	if (lpII->lpbi->bmiHeader.biBitCount == 1)
		return (NULL);

	// If the src is a palettized image then we can just copy the
	// old image and change its palette to gray
	if (lpII->lpbi->bmiHeader.biBitCount <= 8) {

		pNewDIB = new DIB( lpII->lpbi->bmiHeader.biWidth, lpII->lpbi->bmiHeader.biHeight, lpII->lpbi->bmiHeader.biBitCount, 1, lpII->lpbi );

		if ( pNewDIB->IsValid() ) {

			lpSrcDIB = (HPSTR) lpII->pDIB->GetDataPtr();
			lpDstDIB = (HPSTR) pNewDIB->GetDataPtr();

			// Make sure they both locked OK!
			hmemcpy(lpDstDIB, lpSrcDIB, pNewDIB->GetImageMemSize() );

			// Now remap the palette to gray
			lpSrcPal = ((LPBITMAPINFO) lpII->pDIB->GetDIBPtr())->bmiColors;
			lpDstPal = ((LPBITMAPINFO) pNewDIB->GetDIBPtr())->bmiColors;

			for (i = (UINT) PALCOLORS((LPBITMAPINFOHEADER) lpII->lpbi); i--; lpSrcPal++, lpDstPal++) {
				nGray = lpSrcPal->rgbRed * 30 + lpSrcPal->rgbGreen * 59 + lpSrcPal->rgbBlue * 11;
				nGray /= 100;
				if (nGray > 255)
					nGray = 255;
				lpDstPal->rgbRed = lpDstPal->rgbGreen = lpDstPal->rgbBlue = greymap[nGray];
			}

			if (!(hNewImage = ImgFromPDIB(pNewDIB))) {
				 delete pNewDIB;
			}
			return hNewImage;
		}
		return (NULL);
	}
	x = (UINT) lpII->lpbi->bmiHeader.biWidth;
	y = (UINT) lpII->lpbi->bmiHeader.biHeight;

	pNewDIB = new DIB(x, y, 8);

	if( !pNewDIB->IsValid() ) {
	 	delete pNewDIB;
		return NULL;
	}

	// Lock the DIBs
	lpSrcDIB = (HPSTR) lpII->pDIB->GetDIBPtr();
	lpDstDIB = (HPSTR) pNewDIB->GetDIBPtr();

	lpbi = (LPBITMAPINFOHEADER) lpDstDIB;

	// Set the  resolution fields of the new DIB
	lpbi->biXPelsPerMeter = lpII->lpbi->bmiHeader.biXPelsPerMeter;
	lpbi->biYPelsPerMeter = lpII->lpbi->bmiHeader.biYPelsPerMeter;

	// Fill in the Dst Palette
	lpDstPal = ((LPBITMAPINFO) lpDstDIB)->bmiColors;
	for (i = 0; i < 256; i++, lpDstPal++) {
		lpDstPal->rgbRed = lpDstPal->rgbGreen = lpDstPal->rgbBlue = i;
	}

	// Calc the Padding for the Src & Dst Bitmaps. We know source is 24-bit and
	// the destination is 8-bit.
	nSrcPad = x & 3;
	nDstPad = (4 - (x & 3)) & 3;  // 0 1 2 3 4 5 6... -> 0 3 2 1 0 3 2...

	lpSrc = (HPSTR) lpII->pDIB->GetDataPtr();

	lpDst = (HPSTR) pNewDIB->GetDataPtr();

	while (y--) {
		for (x = (UINT) lpbi->biWidth; x--;) {
			nGray = *lpSrc++ * 11;  // Blue

			nGray += *lpSrc++ * 59;// Green

			nGray += *lpSrc++ * 30;// Red

			nGray /= 100;

			*lpDst++ = nGray;
		}
		// Add line padding to Src & Dst pointers
		lpDst += nDstPad;
		lpSrc += nSrcPad;
	}

	if (!(hNewImage = ImgFromPDIB(pNewDIB))) {
		delete pNewDIB;
	}

	return (hNewImage);
}

// //////////////////////////////////////////////////////////////////////////
//
// HANDLE IMAPI ImgIncreaseColors( HIMAGE hImage, int nBitsPerPixel )
//
// Increases the image's color depth to the specified bit per pixel.
//
// //////////////////////////////////////////////////////////////////////////

HANDLE IMAPI ImgIncreaseColors(HANDLE hImage, WORD nBitsPerPixel)
{
	HPUSTR      lpInBits, lpCurInLine;
	HPUSTR      lpOutBits, lpCurOutLine;
	HPUSTR      lpInBuf;
	LPBITMAPINFO lpInBMI, lpOutBMI;
	LPSTR       lpBuf;
	LPINTERNALINFO lpII;
	int         x, y;
	int         nWidth, nHeight;
	unsigned int idx;
	int         nInBits, nOutBits;
	long        lInRowSize, lOutRowSize;
	unsigned char mask;
	DIB *		pNewDIB;

	if ((lpII = (LPINTERNALINFO) GlobalLock(hImage)) == NULL) {
		SetError(NULL, IMG_INV_HAND, lpII->nExt, NULL, hInst);
		return NULL;
	}
	if (lpII->bFlags & IMG_DISP_VECTOR) {
		SetError(NULL, IMG_NSUPPORT, lpII->nExt, NULL, hInst);
		return NULL;
	}
 	if (!lpII->pDIB)
		if (InternalLoad(lpII, NULL, &lpII->pDIB, &lpII->hWMF) != IMG_OK)
			return NULL;
	if ( (lpII->pDIB->GetBitCount() >= nBitsPerPixel ) ||
	     ( ( nBitsPerPixel != 24 ) &&
		   ( nBitsPerPixel != 8  ) &&
		   ( nBitsPerPixel != 4  ) )  ) {
		SetError(NULL, IMG_BAD_PARAM, lpII->nExt, NULL, hInst);
		return NULL;
	}
	if (lpII->hBitmap) {
		DeleteObject(lpII->hBitmap);
		lpII->hBitmap = 0;
	}
	// Lock input dib

	lpInBuf = (HPUSTR) lpII->pDIB->GetDIBPtr(); 

	lpInBMI = (LPBITMAPINFO) lpInBuf;

	lpInBits = (HPUSTR) lpII->pDIB->GetDataPtr();

	if (nBitsPerPixel <= lpInBMI->bmiHeader.biBitCount) {
		SetError(NULL, IMG_NSUPPORT, lpII->nExt, NULL, hInst);
		return NULL;
	}
	// Allocate the output image and fill in it's BITMAPINFO

	pNewDIB = new DIB( lpInBMI->bmiHeader.biWidth, lpInBMI->bmiHeader.biHeight, nBitsPerPixel );

	if( !pNewDIB->IsValid() ) {
		SetError(NULL, IMG_NOMEM, lpII->nExt, NULL, hInst);
		return NULL;
	}

	lpBuf = pNewDIB->GetDIBPtr();

	lpOutBMI = (LPBITMAPINFO) lpBuf;
	lpOutBits = (HPUSTR) pNewDIB->GetDataPtr();

	if (nBitsPerPixel < 24) {
		LPRGBQUAD   lpOutPal;

		// Initialize the new pallette
		lpOutPal = (LPRGBQUAD) (lpBuf + sizeof(BITMAPINFOHEADER));

		_fmemset(lpOutPal, 0, ((1 << nBitsPerPixel) * sizeof(RGBQUAD)));

		// copy palette from old image
		_fmemcpy(lpOutPal, lpInBuf + sizeof(BITMAPINFOHEADER), (int) PALETTESIZE((LPBITMAPINFOHEADER) lpInBMI));

	}
	nWidth = (int) lpInBMI->bmiHeader.biWidth;
	nHeight = (int) lpInBMI->bmiHeader.biHeight;
	nInBits = (int) lpInBMI->bmiHeader.biBitCount;
	nOutBits = (int) lpOutBMI->bmiHeader.biBitCount;

	lInRowSize = ROWSIZE(lpInBMI->bmiHeader.biWidth, lpInBMI->bmiHeader.biBitCount);
	lOutRowSize = ROWSIZE(lpOutBMI->bmiHeader.biWidth, lpOutBMI->bmiHeader.biBitCount);
	lpCurInLine = lpInBits;
	lpCurOutLine = lpOutBits;

	mask = 0x80;

	// Remap the pixels
	for (y = 0; y < nHeight; y++) {
		for (x = 0; x < nWidth; x++) {
			if (nInBits == 1) {
				idx = *lpInBits & mask ? 1 : 0;
				mask >>= 1;
				if (!mask) {
					mask = 0x80;
					lpInBits++;
				}
			} else if (nInBits == 4) {
				if (x & 1)
					idx = *lpInBits++ & 0x0f;
				else
					idx = *lpInBits >> 4;
			} else if (nInBits == 8) {
				idx = *lpInBits++;
			}
			if (nOutBits == 4) {
				if (x & 1) {
					*lpOutBits |= idx & 0x0f;
					lpOutBits++;
				} else {
					*lpOutBits = (idx & 0x0f) << 4;
				}
			} else if (nOutBits == 8)	// Must be 8 bit data

				*lpOutBits++ = idx;
			else {					  // Must be 24 bit output

				*lpOutBits++ = lpInBMI->bmiColors[idx].rgbBlue;
				*lpOutBits++ = lpInBMI->bmiColors[idx].rgbGreen;
				*lpOutBits++ = lpInBMI->bmiColors[idx].rgbRed;
			}

		}

		if (nOutBits == 4 && nWidth % 2)
			lpOutBits++;

		if (nInBits == 4 && nWidth % 2)
			lpInBits++;


		lpCurInLine += lInRowSize;
		lpInBits = lpCurInLine;
		lpCurOutLine += lOutRowSize;
		lpOutBits = lpCurOutLine;
		mask = 0x80;
	}

	GlobalUnlock(hImage);

	return ImgFromPDIB( pNewDIB);
}
