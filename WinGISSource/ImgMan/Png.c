////////////////////////////////////////////////////////////////////////////
//
// png.c
//
// Windows PNG Format DIL Module
//
// ImageMan Image Processing Library
//
// Copyright (c) 1991-5 Data Techniques, Inc.
// All Rights Reserved
//
/////////////////////////////////////////////////////////////////////////////
// $History: PNG.C $
// 
// *****************  Version 8  *****************
// User: Ericw        Date: 4/11/00    Time: 10:28a
// Updated in $/ImageMan 16-32/DILS
// Fixed bug #238 - unable to open embedded PNGs.
// 
// *****************  Version 7  *****************
// User: Timk         Date: 11/11/98   Time: 6:18p
// Updated in $/ImageMan 16-32/DILS
// Fixed 24-bit RGB + alpha channel images. Just ignore the alpha channel
// for now.
// 
// *****************  Version 6  *****************
// User: Timk         Date: 7/09/98    Time: 9:49p
// Updated in $/ImageMan 16-32/DILS
// Fixed bug when swapping RGB to BGR for 24-bit images.
// 
// *****************  Version 5  *****************
// User: Timk         Date: 7/09/98    Time: 12:54p
// Updated in $/ImageMan 16-32/DILS
// Fixed to handle two-color grayscale images (we had previously assumed,
// incorrectly, that 2-color images would be palette based).
// 
// *****************  Version 4  *****************
// User: Timk         Date: 7/23/97    Time: 11:09a
// Updated in $/ImageMan 16-32/DILS
// Fixed memory leak.
// 
// *****************  Version 3  *****************
// User: Timk         Date: 2/27/96    Time: 12:25p
// Updated in $/ImageMan 16-32/DILS
// Changes to support loading portions of a PNG image. Still can't load
// portion of an interlaced PNG.
// 
// *****************  Version 2  *****************
// User: Timk         Date: 2/01/96    Time: 10:40a
// Updated in $/ImageMan 16-32/DILS
// Removed all warnings in compiled code.
// 
// *****************  Version 1  *****************
// User: Timk         Date: 1/29/96    Time: 12:57a
// Created in $/ImageMan 16-32/DILS
// PNG viewer.
// 

#include "internal.h"
#pragma hdrstop
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "png.h"     
#include "zlib.h"

#define VERSION "PNG File Reader V1.0"

#ifdef NDEBUG
#undef OutputDebugString
#define OutputDebugString(S) ((void)0)
#endif

typedef struct tagFileStruct FileStruct;

typedef enum {
	PNGFILTER_NONE,
	PNGFILTER_SUB,
	PNGFILTER_UP,
	PNGFILTER_AVERAGE,
	PNGFILTER_PAETH,
} PNG_FILTER;

typedef int (FAR *PNG_OUT_FUNC)(HPSTR, HPSTR, DWORD, HPSTR, WORD);

typedef struct tagFileStruct {
	LPVOID		lpOpenInfo;		// info needed to reopen the image
	LPVOID		lpHandle;		// handle to file particulars for IMG_IO block functions
	LPIMG_IO		lpIO;				// IO block 
	long        offset;			// offset of image in file
	DWORD       rowSize;			// size of 1 row of the output image
										// Note that rowSize is the size of the *output* image,
										// not the whole image. If we're just extracting
										// a portion of a row, rowSize represents that portion.
	DWORD			dwFullRowSize;	// # of bytes in a row of input image (i.e., whole image)
	UINT        rows;				// # of rows in image
	UINT			nCurRow;			// current row
	IHDR_STRUCT	ihdr;				// IHDR information
	int			nPixSize;		// # of bytes/pixel, rounded up to 1 if <1
	int			nTotalBPP;		// size of complete pixels, in bits (not the same as ihdr.bpp!)
	UCHAR			pal[256*3];		// Palette information
	LONG			lIDATOffset;	// location of 1st IDAT chunk
	LONG			lNextIDAT;		// location of IDAT
	LONG			lCurIDATCnt;	// # of chars consumed from cur IDAT
	int			nPass;			// pass # (0-7) for interlaced images
	DWORD			dwImgColCnt;	// column counter
	HPSTR			hpOutBuf;		// pointer to start of output buffer
	HPSTR			hpOutRow;		// current output row
	HPSTR			hpDecodeBuf;	// line buffer for easy filtering
	HPSTR			hpOutByte;		// current output byte in hpDecodeBuf
	DWORD			dwIntRowSize[7]; // width of a interlaced row for each interlaced pass 
	LPSTR			lpIntBuf;			//interlace buf
	WORD			wStartRow;		// first row to output
	WORD			wEndRow;			// last row to output
	BOOL			bBuf;					//toggle - TRUE when otuput to 1st half of lpIntBuf, FALSE for 2nd
}           FileStruct;

//
// Define the values needed for calculating placement of pixels
// for interlaced images
//
// The Adam7 interlacing scheme is composed of 7 passes, defined as follows:
//
// 	1 6 4 6 2 6 4 6
// 	7 7 7 7 7 7 7 7
// 	5 6 5 6 5 6 5 6
// 	7 7 7 7 7 7 7 7
// 	3 6 4 6 3 6 4 6
// 	7 7 7 7 7 7 7 7
// 	5 6 5 6 5 6 5 6
// 	7 7 7 7 7 7 7 7

WORD wIntVOffset[7] = {0,0,4,0,2,0,1};	//# of rows to skip for start row
WORD wIntHOffset[7] = {0,4,0,2,0,1,0};	//# of cols to skip for each row
WORD wIntVSkip[7] = {8,8,8,4,4,2,2};		//# of rows to skip between each row
WORD wIntHSkip[7] = {8,8,4,4,2,2,1};		//# of cols to skip between pixels

#ifdef WIN32

#ifdef WINGIS // so the file TLSByJHC.h was included

#define SETERROR(E) bJHCTLSSetValue((DWORD)(E))
#define GETERROR    nJHCTLSGetValue()

#else
// Since we only need to store the error value on a per-thread basis, I'm
// just gonna place it in our DWORD TLS slot.
static DWORD TlsIndex;
#define SETERROR(E) TlsSetValue(TlsIndex,(LPVOID)(DWORD)(E))
#define GETERROR    ((DWORD)TlsGetValue(TlsIndex))
#endif

#else
static int  nErrID;				  // id of latest error
#define SETERROR(E) nErrID = (E)
#define GETERROR    (nErrID)
#endif

static HINSTANCE pngInstance;
#if DEBUG
static DEBUGPROC DebugStringOut;
#endif

#ifdef WIN32
// inline swaps for 32-bit compiles
#define BSWAP(S)  (S) = (WORD)(((UINT)(S)<<8)|(((UINT)(S))>>8))
#define BSWAPL(L) (L) = ((L)<<24)|((ULONG)(L)>>24)\
                          |(((L)>>8)&0x0000ff00)|(((L)<<8)&0x00ff0000)
#else
// swaps for 16-bit compiles, very fast if "inline intrinsics" optimization is on
#define BSWAP(S) (S) = _rotl((S),8)
#define BSWAPL(L) (L) = ((L)<<24)|((ULONG)(L)>>24)\
                          |(((L)>>8)&0x0000ff00)|(((L)<<8)&0x00ff0000)
//#define BSWAPL(L) (L) = (_rotl(LOWORD(L),8)<<16)|_rotl(HIWORD(L),8)
#endif

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

char			jbuf[100];

int Initialize(HANDLE hInstance)
{

#if DEBUG
	{
		HANDLE      hMod = GetModuleHandle("IMGMAN2.DLL");
		if (hMod)
			DebugStringOut = (DEBUGPROC) GetProcAddress(hMod, "DebugStringOut");
	}
#endif

	pngInstance = hInstance;

	return (1);
}

#ifdef WIN32
// /////////////////////////////////////////////////////////////////////////
//
// DllMain
//
// This is the Win32 'DllEntryPoint'. It is similar in function to LibMain
// and the WEP in 16-bit land, but is much more thorough. LibMain is
// called whenever a process or thread attaches or detaches to the
// BMP DIL
//
// Entry:
// hinstDLL  - handle to the ImageMan DLL
// fdwReason - notification code (attach/detach)
// DLL_PROCESS_ATTACH
// DLL_PROCESS_DETACH
// DLL_THREAD_ATTACH
// DLL_THREAD_DETACH
// lpvReserved - NULL during dynamic process attach/detach, non-NULL
// during static attach/detach.
// Exit:
// DLL_PROCESS_ATTACH - TRUE for success, FALSE for failure
// all others         - return code ignored
// /////////////////////////////////////////////////////////////////////////

BOOL APIENTRY DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
	switch (fdwReason) {
 		// The DLL is attaching to a process, due to LoadLibrary.
 		case DLL_PROCESS_ATTACH:
			assert(!lpvReserved);  // must always be loaded via LoadLibrary!

//			OutputDebugString("IM2PNG: Process++\n");
			Initialize(hinstDLL);
#ifndef WINGIS
			// Allocate a "Thread Local Storage" (TLS) index right away.

			if ((TlsIndex = TlsAlloc()) == 0xFFFFFFFF)
				return (FALSE);
#else
			bJHCTLSClear();
#endif
			// Fall thru: Initialize the index for first thread.

 		// The attached process created a new thread.
		case DLL_THREAD_ATTACH:
#ifndef WINGIS
			OutputDebugString("IM2PNG: Thread++\n");
			// Initialize the TLS index for this thread... it's actually 'nErrID'
			if (!TlsSetValue(TlsIndex, (LPVOID) 0))
				return (FALSE);
#else
			nJHCTLSAlloc(); // Stamp this thread to be existed
#endif
			break;

 		// The thread of the attached process terminates.
		case DLL_THREAD_DETACH:
#ifdef WINGIS
			bJHCTLSFree(); // Free this thread entry
#else
			OutputDebugString("IM2PNG: Thread--\n");
#endif
			break;

 		// The DLL is detaching from a process, due to a call to FreeLibrary.
		case DLL_PROCESS_DETACH:
#ifndef WINGIS
			OutputDebugString("IM2PNG: Process--\n");
			// Release the TLS index.
			TlsFree(TlsIndex);
#else
			bJHCTLSClear();
#endif
			break;
	}

	OutputDebugString("IM2PNG: OK\n");
	return (TRUE);

	UNREFERENCED_PARAMETER(lpvReserved);
}

#else // WIN32

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int WINAPI  LibMain(HANDLE hInstance, WORD wDataSeg, WORD wHeapSize, LPSTR lpCmdLine)
{
	if (!wHeapSize)
		return (0);					  // no heap indicates fatal error

	return Initialize(hInstance);

	UNREFERENCED_PARAMETER(wDataSeg);
	UNREFERENCED_PARAMETER(lpCmdLine);
}
#endif // WIN32

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
void WINAPI WEP(int bSystemExit)
{
	return;
	UNREFERENCED_PARAMETER(bSystemExit);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
LPSTR IMAPI IMDLget_ver(void)
{
	return ((LPSTR) (VERSION));
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLescape(HANDLE hImage, int nInCnt, LPVOID lpIn, LPVOID lpOut)
{
	return (IMG_NSUPPORT);
	UNREFERENCED_PARAMETER(hImage);
	UNREFERENCED_PARAMETER(nInCnt);
	UNREFERENCED_PARAMETER(lpIn);
	UNREFERENCED_PARAMETER(lpOut);
}

static _inline UCHAR PaethPredictor(int a, int b, int c)
{
	int	p, pa, pb, pc;

	p = a+b-c;
	pa = abs(p - a);
	pb = abs(p - b);
	pc = abs(p - c);
	if ((pa <= pb) && (pa <= pc)) return a;
	else if (pb <= pc) return b;
	else return c;
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
BOOL IMAPI  IMDLverify_img(LPSTR lpVerfBuf, LPVOID lpHandle, LPIMG_IO lpIO)
{
	return _fstrncmp(lpVerfBuf, PNGSIG, 8) ? FALSE:TRUE;
	UNREFERENCED_PARAMETER(lpHandle);
	UNREFERENCED_PARAMETER(lpIO);
}

int FilterRow(HPSTR pOut, HPSTR pIn, DWORD dwCnt, HPSTR hpPrev, WORD wPixSize)
{
	WORD	w;

	// the first byte is the filter byte
	dwCnt--;
	switch (*pIn++) {
		case PNGFILTER_NONE:
			_fmemcpy(pOut, pIn, (WORD)dwCnt);
			break;

		case PNGFILTER_SUB:
			// handle first pixel so we can look backwards
			for (w=0; w<wPixSize; w++) {
				*pOut++ = *pIn++;
				dwCnt--;
			}
			while (dwCnt--) {
				*pOut = *pIn++ + *(pOut - wPixSize);
				pOut++;
			}
			break;

		case PNGFILTER_UP:
			if (hpPrev) {
				while (dwCnt--) *pOut++ = *pIn++ + *hpPrev++;
			}
			else {
				_fmemcpy(pOut, pIn, (WORD)dwCnt);
			}
			break;

		case PNGFILTER_AVERAGE:
			if (hpPrev) {
				for (w=0; w<wPixSize; w++) {
					*pOut++ = *pIn++ + *hpPrev++/2;
					dwCnt--;
				}
				while (dwCnt--) {
					*pOut = *pIn++ + (*(pOut-wPixSize)+*hpPrev)/2;
					hpPrev++;
					pOut++;
				}
			}
			else {
				for (w=0; w<wPixSize; w++) {
					*pOut++ = *pIn++;
					dwCnt--;
				}
				while (dwCnt--) {
					*pOut = *pIn++ + *(pOut-wPixSize)/2;
					pOut++;
				}
			}
			break;

		case PNGFILTER_PAETH:
			if (hpPrev) {
				for (w=0; w<wPixSize; w++) {
					*pOut++ = (*pIn++ + PaethPredictor(0, *hpPrev++, 0)) & 0xff;
					dwCnt--;
				}
				while (dwCnt--) {
					*pOut = (*pIn++ + PaethPredictor(*(pOut-wPixSize), *hpPrev, *(hpPrev-wPixSize))) & 0xff;
					hpPrev++;
					pOut++;
				}
			}
			else {
				for (w=0; w<wPixSize; w++) {
					*pOut++ = (*pIn++ + PaethPredictor(0, 0, 0)) & 0xff;
					dwCnt--;
				}
				while (dwCnt--) {
					*pOut = (*pIn++ + PaethPredictor(*(pOut-wPixSize), 0, 0)) & 0xff;
					pOut++;
				}
			}
			break;
	}

	return 1;
}

//
// Reads all PNG chunks and extracts the information we
// need from it. 
//
// Before calling, set file pointer to first chunk in file.
//
// Returns IMG_OK on succes, error value otherwise
//
int ReadChunks(FileStruct FAR *fs)
{
	CHUNKHEAD	chunk;
	LONG			lCurPos;
	BOOL			bIDAT = FALSE;

	// read IHDR chunk first
	if (fs->lpIO->read(fs->lpHandle, &chunk, sizeof(CHUNKHEAD)) != sizeof(CHUNKHEAD)) {
		SETERROR(IDS_READERR);
		return IMG_FILE_ERR;
	}

	while (chunk.type != IEND) {

		BSWAPL(chunk.len);
		lCurPos = fs->lpIO->seek(fs->lpHandle, 0, SEEK_CUR);
		switch (chunk.type) {
			case IHDR:
				fs->lpIO->read(fs->lpHandle, &fs->ihdr, sizeof(IHDR_STRUCT));
				BSWAPL(fs->ihdr.width);
				BSWAPL(fs->ihdr.height);
				// now determine the size of each pixel...
				switch (fs->ihdr.colortype) {
					case 0:		//grayscale values
						fs->nPixSize = max(fs->ihdr.bpp/8, 1);
						fs->nTotalBPP = fs->ihdr.bpp;
						// if the pixels are less than a byte wide, we'll
						// need to adjust the full row size to be accurate...
						if (fs->ihdr.bpp < 8) {
							fs->dwFullRowSize = ((fs->ihdr.bpp * fs->ihdr.width)+7)/8 + 1;
						}
						else {
							fs->dwFullRowSize = fs->nPixSize * fs->ihdr.width + 1;
						}
						break;
					case 2:		//rgb values
						fs->nPixSize = (fs->ihdr.bpp/8) * 3;
						fs->nTotalBPP = fs->ihdr.bpp * 3;
						fs->dwFullRowSize = fs->nPixSize * fs->ihdr.width + 1;
						break;
					case 3:		//palette index values
						fs->nPixSize = 1;
						fs->dwFullRowSize = (fs->ihdr.bpp*fs->ihdr.width+7)/8 + 1;
						fs->nTotalBPP = fs->ihdr.bpp;
						break;
					case 4:		//grayscale-alpha
						fs->nPixSize = fs->ihdr.bpp/8;
						fs->nTotalBPP = fs->ihdr.bpp*2;
						fs->dwFullRowSize = fs->nPixSize * fs->ihdr.width + 1;
						break;
					case 6:		//rgb-alpha
						fs->nPixSize = (fs->ihdr.bpp/8)*4;
						fs->dwFullRowSize = fs->nPixSize * fs->ihdr.width + 1;
						fs->nTotalBPP = fs->ihdr.bpp*4;
						break;
				}

				fs->nPass = 0;

				if (fs->ihdr.interlace) {
					// calculate the row widths for each pass of the 
					// interlaced image
					//
					// start by calcing the # of pixels in each row, then
					//	multiply by # of bytes/pixel
					fs->dwIntRowSize[0] = ((fs->ihdr.width + 7)/8 * fs->nTotalBPP+7)/8 + 1;
					fs->dwIntRowSize[1] = ((fs->ihdr.width + 3)/8 * fs->nTotalBPP+7)/8 + 1;
					fs->dwIntRowSize[2] = ((fs->ihdr.width+3)/4 * fs->nTotalBPP+7)/8 + 1;
					fs->dwIntRowSize[3] = ((fs->ihdr.width+1)/4 * fs->nTotalBPP+7)/8 + 1;
					fs->dwIntRowSize[4] = ((fs->ihdr.width+1)/2 * fs->nTotalBPP+7)/8 + 1;
					fs->dwIntRowSize[5] = ((fs->ihdr.width)/2 * fs->nTotalBPP+7)/8 + 1;
					fs->dwIntRowSize[6] = (fs->ihdr.width * fs->nTotalBPP+7)/8 + 1; 

				}
				// if it's not interlaced, set dwIntRowSize[0] to be dwFullRowSize.
				// This way we can use dwIntRowSize[nPass] for all cases, since
				// nPass is always 0 for non-interlaced images.
				else {
					fs->dwIntRowSize[0] = fs->dwFullRowSize;	
				}

				// we'll need this buffer whether the image is interlaced or
				// not, as we may be reading some arbitrary portion of the image,
				// and we'll need a temporary buffer.
				fs->lpIntBuf = (LPSTR)GlobalAllocPtr(GHND, fs->dwFullRowSize*2);
				break;

			case PLTE:
				fs->lpIO->read(fs->lpHandle, fs->pal, chunk.len);
				break;

			case IDAT:
				if (!bIDAT) {
					bIDAT = TRUE;
					fs->lIDATOffset = (lCurPos - sizeof(CHUNKHEAD)) ;
				}
				break;

			case bKGD:
				break;

			case cHRM:
				break;

			case gAMA:
				break;

			case hIST:
				break;

			case pHYs:
				break;

			case sBIT:
				break;

			case tEXt:
				break;

			case tIME:
				break;

			case tRNS:
				break;

			case zTXt:
				break;

			default:
				break;
		}

		// now skip ahead by the size specified in the chunk.
		fs->lpIO->seek(fs->lpHandle, lCurPos, 0);
		fs->lpIO->seek(fs->lpHandle, chunk.len+4, SEEK_CUR); //skip CRC

		// now read next chunk
		if (fs->lpIO->read(fs->lpHandle, &chunk, sizeof(CHUNKHEAD)) != sizeof(CHUNKHEAD)) {
			SETERROR(IDS_READERR);
			return IMG_FILE_ERR;
		}
	}

	return IMG_OK;
}

LONG GetBytes(FileStruct FAR *fs, LPSTR lpBuf, LONG lBytes)
{
	CHUNKHEAD	chunk;
	LONG			lRead, lTotal=0;

	while (lBytes) {
		if (fs->lCurIDATCnt) {
			lRead = fs->lpIO->read(fs->lpHandle, lpBuf, min(lBytes, fs->lCurIDATCnt));
			if (!lRead) return (lTotal ? lTotal : -1);
			lBytes -= lRead;
			lpBuf += lRead;
			lTotal += lRead;
			fs->lCurIDATCnt -= lRead;
		}
		else {	//load up the next block
			fs->lpIO->seek(fs->lpHandle, fs->lNextIDAT, 0);
			fs->lpIO->read(fs->lpHandle, &chunk, sizeof(CHUNKHEAD));
			if (chunk.type != IDAT) return (lTotal ? lTotal : -1);
			BSWAPL(chunk.len);
			fs->lCurIDATCnt = chunk.len;
			fs->lNextIDAT = fs->lpIO->seek(fs->lpHandle, 0, SEEK_CUR) + chunk.len + 4 ;
		}
	}

	return lTotal;
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLinit_image(LPCSTR lpOpenInfo, LPHANDLE hFile, LPVOID lpHandle, long offset, long lLen, LPIMG_IO lpIO)
{
	FileStruct FAR *fs;
	int         retval;

//	DEBUGOUT(DILAPI, "\t%s-->init_image(%s,%x:%x,%d,%ld,%ld)", (LPSTR) __FILE__, (LPSTR) file, HIWORD(hFile), LOWORD(hFile), offset, lLen);

	// skip PNG signature bytes
	lpIO->seek(lpHandle, offset+8, 0);	

	// create new internal structure
	if (!(*hFile = GlobalAlloc(DLLHND, sizeof(FileStruct)))) {
		lpIO->close(lpHandle);
		SETERROR(IDS_MEMERR);
		return (IMG_NOMEM);
	}

	// fill in the structure
	fs = (FileStruct FAR *) GlobalLock(*hFile);
	fs->lpOpenInfo = (LPVOID)lpOpenInfo;
	fs->lpHandle = lpHandle;
	fs->lpIO = lpIO;
	fs->offset = offset;
	fs->dwImgColCnt = 0;
	fs->nCurRow = 0;
	fs->wStartRow = fs->wEndRow = 0;
	fs->bBuf = TRUE;

	if ((retval = ReadChunks(fs)) != IMG_OK) {
		GlobalUnlock(*hFile);
		GlobalFree(*hFile);
		*hFile = 0;
		return retval;
	}
	
	GlobalUnlock(*hFile);

	return (IMG_OK);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLset_ext(HGLOBAL hFile, LPSTR ext)
{
	DEBUGOUT(DILAPI, "\t%s-->set_ext(%d,%s)", (LPSTR) __FILE__, hFile, (LPSTR) ext);
	// no extensions to differentiate between - just return OK
	return (IMG_OK);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLclose_image(HGLOBAL hFile)
{
	FileStruct FAR *fs;

	DEBUGOUT(DILAPI, "\t%s-->close_image(%d)", (LPSTR) __FILE__, hFile);
	// check to see if file is still open - if it is, close it
	if ((fs = (FileStruct FAR *) GlobalLock(hFile)) == NULL) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}
	if (fs->lpHandle) {
		fs->lpIO->close(fs->lpHandle);
		fs->lpHandle = NULL;
	}

	if (fs->hpDecodeBuf) GlobalFreePtr(fs->hpDecodeBuf);
	if (fs->lpIntBuf) GlobalFreePtr(fs->lpIntBuf);

	GlobalUnlock(hFile);
	GlobalFree(hFile);
	return (IMG_OK);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLimg_name(HGLOBAL hFile, HANDLE FAR * lpHand)
{
	FileStruct FAR *fs;

	if (!(fs = (FileStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}

	GlobalUnlock(hFile);
	return (IMG_OK);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLopen(HGLOBAL hFile, LPVOID lpHandle)
{
	FileStruct FAR *fs;

	if (!(fs = (FileStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}
	if (!fs->lpHandle) {
		if (fs->lpIO->open(fs->lpOpenInfo, OF_READ, &(fs->lpHandle)) != IMG_OK) {
			GlobalUnlock(hFile);
			SETERROR(IDS_NOOPEN);
			return (IMG_NOFILE);
		}
  	fs->lpIO->seek(fs->lpHandle, fs->offset, 0);
    ReadChunks(fs) ;
	}
	fs->lpIO->seek(fs->lpHandle, fs->lIDATOffset, 0);
	fs->lNextIDAT = fs->lIDATOffset ;
	fs->lCurIDATCnt = 0;

	GlobalUnlock(hFile);
	return (IMG_OK);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLclose_keep(HANDLE hFile)
{
	FileStruct FAR *fs;

	DEBUGOUT(DILAPI, "\t%s-->close_keep(%d)", (LPSTR) __FILE__, hFile);
	if (!(fs = (FileStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}
	if (fs->lpHandle) {
		fs->lpIO->close(fs->lpHandle);
		fs->lpHandle = NULL;
	}

	GlobalUnlock(hFile);
	return (IMG_OK);
}

int FAR PASCAL OutputBytes(FileStruct FAR *fs, LPSTR lpChar, int nCnt, UINT uStartByte, UINT uWid)
{
	int nCopy;
	int i;
	LPSTR	lpOut, lpPrev, lpTmp;
	HPSTR hpTmp;

	// if nCnt will push over the edge, we need to be careful,
	// otherwise just memcpy
	do { 
		if (fs->dwImgColCnt+nCnt > fs->dwIntRowSize[fs->nPass]) {
			nCopy = (int)(fs->dwIntRowSize[fs->nPass] - fs->dwImgColCnt);
			if (nCopy) {
				_fmemmove(fs->hpOutByte, lpChar, nCopy); //output to temp buf
				lpChar += nCopy;
				nCnt -= nCopy;
				fs->dwImgColCnt += nCopy;
			}
		}
		else {
			_fmemmove(fs->hpOutByte, lpChar, nCnt);	//output to temp buf
			fs->hpOutByte += nCnt;
			fs->dwImgColCnt += nCnt;
			nCnt = 0;
		}

		// now output a row if we've got it done...
		if (fs->dwImgColCnt >= fs->dwIntRowSize[fs->nPass]) {
			fs->dwImgColCnt = 0;
			// we have reached a momentous point...we have a fully
			// decoded row ready for output! Now we just have to
			// filter it and output it to the DIB
			//

			if (fs->bBuf) { //odd rows
				lpOut = fs->lpIntBuf+fs->dwFullRowSize;
				lpPrev = fs->lpIntBuf;
				fs->bBuf = FALSE;
			}
			else {	//even rows
				lpPrev = fs->lpIntBuf+fs->dwFullRowSize;
				lpOut = fs->lpIntBuf;
				fs->bBuf = TRUE;
			}

				FilterRow(lpOut, 
					fs->hpDecodeBuf, 
					fs->dwIntRowSize[fs->nPass], 
					lpPrev, 
					fs->nPixSize);

			if (fs->ihdr.interlace) {
				HPSTR	hpOutTmp;
				WORD	wCurInPix, wCurOutPix;


				// now that we've got a row of interlaced pixels, output 
				// them to their proper place in the world...
				if ( (fs->nCurRow >= fs->wStartRow) && (fs->nCurRow <= fs->wEndRow) ) switch (fs->nTotalBPP) {
					// in the cases < 8, we've got to break the individual
					// pixels out and place them in the proper bit locations
					// in the output DIB, AND for 2-bit images we've got to
					// promote them to 4 bits. This is normally done after
					// the images have been loaded, but for interlaced images
					// it's got to be done here.
					case 1:
						if (fs->nPass < 6) {
							BYTE bInMask;
							BYTE bm1[1] = {0x80}, bm2[1] = {0x08}, bm3[2] = {0x80, 0x08};
							BYTE bm4[2] = {0x20, 0x02}, bm5[4] = {0x80, 0x20, 0x08, 0x02};
							BYTE bm6[4] = {0x40, 0x10, 0x04, 0x01};
							LPBYTE lpbm;
							WORD	wCnt, wCntMax[6] = {1,1,2,2,4,4};

							hpOutTmp = fs->hpOutRow;	//always start at first byte
							switch (fs->nPass) {
								case 0: lpbm = bm1; break;
								case 1: lpbm = bm2; break;
								case 2: lpbm = bm3; break;
								case 3: lpbm = bm4; break;
								case 4: lpbm = bm5; break;
								case 5: lpbm = bm6; break;
							}
							for (wCurOutPix=wIntHOffset[fs->nPass], bInMask = 0x80, wCnt = 0; 
									wCurOutPix < (WORD)(fs->ihdr.width); 
									wCurOutPix += wIntHSkip[fs->nPass]) {

								if (*lpOut & bInMask) *hpOutTmp |= lpbm[wCnt];
								wCnt++;
								if (wCnt >= wCntMax[fs->nPass]) {
									wCnt = 0;
									hpOutTmp++;
								}
								bInMask >>= 1;
								if (!bInMask) {
									bInMask = 0x80;
									lpOut++;
								}
							}
						}
						else _fmemcpy(fs->hpOutRow, lpOut+uStartByte, uWid);
						break;

					case 2:
						wCurInPix = 0;

						if (fs->nPass < 6) {
							BYTE	bPixVal[4];
							BYTE	bOutShift;

							// The difficulty in this loop appears to be calculating the 
							// output pixel's mask quickly. A look at the possible steps
							// allowed in the interlacing scheme will show, however, that
							// the output mask is identical for every pixel in a given 
							// interlaced row (except for pass 7). 
							// So we calculate it beforehand.
							bOutShift = (BYTE)(4 - ((wIntHOffset[fs->nPass] % 2)*4));
							hpOutTmp = fs->hpOutRow + wIntHOffset[fs->nPass]/2;
							for (wCurOutPix=wIntHOffset[fs->nPass]; 
									wCurOutPix < (WORD)(fs->ihdr.width); 
									wCurOutPix += wIntHSkip[fs->nPass], 
									hpOutTmp += wIntHSkip[fs->nPass]/2) {

								if (!wCurInPix) {
									bPixVal[0] = (*lpOut & 0xc0) >> 6;
									bPixVal[1] = (*lpOut & 0x30) >> 4;
									bPixVal[2] = (*lpOut & 0x0c) >> 2;
									bPixVal[3] = (*lpOut & 0x03);
									lpOut++;
								}
								*hpOutTmp |= (bPixVal[wCurInPix] << bOutShift);
								wCurInPix = (wCurInPix+1)&0x03;
							}
						}
						else {	// last row must be handled differently
							hpOutTmp = fs->hpOutRow;
							for (wCurOutPix=0; wCurOutPix < (WORD)(fs->ihdr.width); 
									wCurOutPix += 4, hpOutTmp+=2, lpOut++) {

								*hpOutTmp = (*lpOut & 0xc0) >> 2;
								*hpOutTmp |= (*lpOut & 0x30) >> 4;
								*(hpOutTmp+1) = (*lpOut & 0x0c) << 2;
								*(hpOutTmp+1) |= (*lpOut & 0x03);
							}
						}
						break;

					case 4:
						if (fs->nPass < 6) {
							BYTE bLoNybble = (BYTE)((wIntHOffset[fs->nPass] % 2));
							hpOutTmp = fs->hpOutRow + (wIntHOffset[fs->nPass])/2;
							for (wCurOutPix=wIntHOffset[fs->nPass]; wCurOutPix < (WORD)(fs->ihdr.width); wCurOutPix += wIntHSkip[fs->nPass]*2, lpOut++) {
								if (bLoNybble) {
									*hpOutTmp |= ((*lpOut >> 4) & 0x0f);
									hpOutTmp += wIntHSkip[fs->nPass]/2;
									*hpOutTmp |= (*lpOut & 0x0f);
									hpOutTmp += wIntHSkip[fs->nPass]/2;
								}
								else {
									*hpOutTmp |= (*lpOut & 0xf0);
									hpOutTmp += wIntHSkip[fs->nPass]/2;
									*hpOutTmp |= (*lpOut << 4);
									hpOutTmp += wIntHSkip[fs->nPass]/2;
								}
							}
						}
						else _fmemcpy(fs->hpOutRow, lpOut, (WORD)(fs->dwFullRowSize-1));
						break;

					case 8:
						hpOutTmp = fs->hpOutRow + wIntHOffset[fs->nPass]*fs->nPixSize;	//start on proper pixel
						for (wCurOutPix=wIntHOffset[fs->nPass]; wCurOutPix < (WORD)(fs->ihdr.width); wCurOutPix += wIntHSkip[fs->nPass], hpOutTmp += wIntHSkip[fs->nPass]) {
							*hpOutTmp = *lpOut++;
						}
						break;
					case 24:	//8-bit rgb output
						hpOutTmp = fs->hpOutRow + wIntHOffset[fs->nPass]*fs->nPixSize;	//start on proper pixel
						for (wCurOutPix=wIntHOffset[fs->nPass]; wCurOutPix < (WORD)(fs->ihdr.width); wCurOutPix += wIntHSkip[fs->nPass], hpOutTmp += wIntHSkip[fs->nPass]*3) {
							*hpOutTmp = *lpOut++;
							*(hpOutTmp+1) = *lpOut++;
							*(hpOutTmp+2) = *lpOut++;
						}
						break;

					case 32: //8-bit rgb+alpha
					case 48: //16-bit rgb
					case 64:		//16-bit rgb+alpha
						hpOutTmp = fs->hpOutRow + wIntHOffset[fs->nPass]*fs->nPixSize;	//start on proper pixel
						for (wCurOutPix=wIntHOffset[fs->nPass]; wCurOutPix < (WORD)(fs->ihdr.width); wCurOutPix += wIntHSkip[fs->nPass], hpOutTmp += wIntHSkip[fs->nPass]*(fs->nTotalBPP/8)) {
							_fmemmove(hpOutTmp, lpOut, (fs->nTotalBPP/8));
						}
						break;
				}

				fs->hpOutRow -= fs->rowSize*wIntVSkip[fs->nPass];

				// now update the row counter by jumping to the next
				// row for this pass's output pixels. When nCurRow is
				// greater than ihdr.height, it's time for the next
				// pass...
				fs->nCurRow += wIntVSkip[fs->nPass];
				if (fs->nCurRow >= (WORD)(fs->ihdr.height)) {
					fs->nPass++;
					// look for empty passes! Here, we just skip ahead to the
					// next non-empty pass
					while (!fs->dwIntRowSize[fs->nPass] && (fs->nPass <= 6)) fs->nPass++;
					if (fs->nPass > 6) return 0;

					// set proper output row...
					fs->nCurRow = wIntVOffset[fs->nPass];
					fs->hpOutRow = fs->hpOutBuf - fs->rowSize*wIntVOffset[fs->nPass];

					// clear interlaced buffer
					_fmemset(fs->lpIntBuf, 0, (WORD)(fs->dwFullRowSize*2));
				}
			}
			else {
				if (fs->nCurRow >= fs->wStartRow) {
					switch (fs->ihdr.colortype) {
						case 0:		//grayscale values
						case 2:		//rgb values
						case 3:		//palette index values
						case 4:		//grayscale-alpha
							_fmemcpy(fs->hpOutRow, lpOut+uStartByte, uWid);
							break;
						case 6:		//rgb-alpha
							hpTmp = fs->hpOutRow;
							lpTmp = lpOut+uStartByte;

							if (fs->ihdr.bpp == 8) { //8 and 16 are only allowed values here...
								// we've got to get rid of the alpha info, as it does no good currently...
								for (i=0; (i<fs->ihdr.width) && (hpTmp < (fs->hpOutRow+uWid)); i++) {	//for each pixel
									*hpTmp++ = *lpOut++; *hpTmp++ = *lpOut++; *hpTmp++ = *lpOut++;	//copy rgb
									lpOut++;
								}
							}
							else {	//it's got to be 16
							}
							break;
					}

					fs->hpOutRow -= uWid;
				}
				fs->nCurRow++;
			}

			fs->hpOutByte = fs->hpDecodeBuf;
		}
	} while (nCnt && (fs->nCurRow < fs->wEndRow));
	
	return 0;
}

voidpf  MyAlloc(voidpf opaque, uInt nItems, uInt nSize)
{
	return GlobalAllocPtr(GHND, nItems*nSize);
}

void  MyFree(voidpf opaque, voidpf address)
{
	GlobalFreePtr(address);
}


// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLread_rows(HGLOBAL hFile, LPSTR buf, UINT rows, UINT rowSize, UINT start, LPSTATUS lpStat)
{
	FileStruct FAR *fs;
	HPSTR       tmpBuf1, tmpBuf = (HPSTR) buf;
	LONG			lRetBytes;
	int			retval = IMG_OK;
	DWORD			dwCnt;
	UCHAR			uc;
  z_stream d_stream; /* decompression stream */
#define BUFSIZE 4096
	LPSTR			lpInBuf, lpOutBuf;

	DEBUGOUT(DILAPI, "\t%s-->%s(%d, %x:%x, %u, %u, %u, %x:%x)", (LPSTR) __FILE__, (LPSTR) "read_rows", hFile, HIWORD(buf), LOWORD(buf), rows, rowSize, start, HIWORD(lpStat), LOWORD(lpStat));
	if (!(fs = (FileStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}

	if (fs->ihdr.interlace && rows != fs->ihdr.height) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}

	fs->wEndRow = fs->wStartRow + rows;
	fs->hpOutRow = (HPSTR)buf + (LONG)rowSize*(rows-1);
	fs->hpOutBuf = fs->hpOutRow;
	fs->rowSize = rowSize;
	fs->hpOutByte = fs->hpDecodeBuf;

	// now decode the image
	lpInBuf = GlobalAllocPtr(GHND, BUFSIZE);
	lpOutBuf = GlobalAllocPtr(GHND, BUFSIZE);

	d_stream.zalloc = MyAlloc;
	d_stream.zfree = MyFree;
	d_stream.next_in = lpInBuf;
	d_stream.next_out = lpOutBuf;

	inflateInit(&d_stream);	// this resets avail_in and avail_out
	d_stream.avail_in = 0;
	d_stream.avail_out = BUFSIZE;
	dwCnt = 0;

	for (;fs->nCurRow < fs->wEndRow;) {
		if (!d_stream.avail_in) {
			lRetBytes = GetBytes(fs, lpInBuf, BUFSIZE);
			if (lRetBytes < 0) break;
			d_stream.avail_in = (WORD)lRetBytes;
			d_stream.next_in = lpInBuf;
		}

		if (inflate(&d_stream, Z_PARTIAL_FLUSH) != Z_OK) break;

		if (!d_stream.avail_out) {
			OutputBytes(fs, lpOutBuf, (int)(d_stream.total_out - dwCnt), start, rowSize);

			// update status counter
			if (lpStat) {
				lpStat->lAcc += d_stream.total_out - dwCnt;
				if (lpStat->lpfnStat && lpStat->lAcc > lpStat->lInterval) {
					lpStat->lDone += lpStat->lAcc;
					lpStat->lDone = min(lpStat->lDone, lpStat->lTotal);
					
					if( !((*lpStat->lpfnStat) (lpStat->hImage, (int) ((lpStat->lDone * 100) / lpStat->lTotal), lpStat->dwUserInfo) )) {
						retval = IMG_CANCELLED;
						break;
					}
										
					lpStat->lAcc %= lpStat->lInterval;
				}
			}

			dwCnt = d_stream.total_out;
			d_stream.next_out = lpOutBuf;
			d_stream.avail_out = BUFSIZE;
		}

	}

	// flush the rest of our data to output
	if (d_stream.total_out - dwCnt) {
		OutputBytes(fs, lpOutBuf, (int)(d_stream.total_out - dwCnt), start, rowSize);
		dwCnt = d_stream.total_out;
	}

	GlobalFreePtr(lpInBuf);
	GlobalFreePtr(lpOutBuf);

	// if we're outputting an RGB image, we need to adjust for the 
	// fact that RGB DIB images are actually in BGR order in memory.
	// The PNG format is actual RGB, so we've got to swap an awful
	// lot of R and B bytes. The reason we can't do this during
	// output has to do with the need to be able to accurately and
	// easily calculate filter functions on the previous output, which
	// is expected to be in RGB order.
	if ((fs->ihdr.colortype == 2) || (fs->ihdr.colortype == 6)) {
		tmpBuf1 = (HPSTR)buf;
		while (rows--) {
			tmpBuf = tmpBuf1;
			for (dwCnt = 0; dwCnt < (fs->ihdr.width * 3); dwCnt+=3, tmpBuf += 3) {
				uc = *tmpBuf;	//store red
				*tmpBuf = *(tmpBuf+2);	//move blue
				*(tmpBuf+2) = uc;	// move red
			}
			tmpBuf1 += rowSize;
		}
	}
	// if it's a 2-bit (4-color) image, we'll have to upgrade from 4 pixels/byte
	// to 2 pixels/byte
	else if (!(fs->ihdr.interlace) && (fs->ihdr.colortype == 3) && (fs->ihdr.bpp == 2)) {
		HPSTR pRow, p1, p2;
		int	nPixCnt;

		pRow = (HPSTR)buf;		
		while (rows--) {
			// first copy the undecoded row into hpDecodeBuf
			// then expand it
			_fmemcpy(fs->hpDecodeBuf, pRow, (WORD)(fs->dwFullRowSize));
			for (p1=fs->hpDecodeBuf, p2=pRow, nPixCnt=(int)(fs->ihdr.width); nPixCnt>0; p1++, nPixCnt-=4) {
				*p2 = (*p1 & 0xc0) >> 2;
				*p2++ |= (*p1 & 0x30) >> 4;
				*p2 = (*p1 & 0x0c) << 2;
				*p2++ |= *p1 & 0x03;
			}
			pRow += rowSize;
		}
	}

	inflateEnd(&d_stream);
	fs->wStartRow = fs->nCurRow;
	GlobalUnlock(hFile);
	return (retval);
}


// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLset_row(HGLOBAL hFile, UINT row)
{
	FileStruct FAR *fs;

	DEBUGOUT(DILAPI, "\t%s-->set_row(%d,%d)", (LPSTR) __FILE__, hFile, row);
	if (!(fs = (FileStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}

	if (fs->ihdr.interlace || !row || (row < fs->nCurRow) ) {
		fs->lpIO->seek(fs->lpHandle, fs->lIDATOffset, 0);
		fs->lNextIDAT = fs->lIDATOffset ;
		fs->lCurIDATCnt = 0;
		fs->nCurRow = 0;
	}

	fs->wStartRow = row;

	GlobalUnlock(hFile);
	return (IMG_OK);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLbuildDIBhead(HGLOBAL hFile, LPINTERNALINFO lpii)
{
	LPBITMAPINFO lpbi;
	BITMAPINFOHEADER	bi;
	int         nColors, i;
	FileStruct FAR *fs;
	UCHAR			FAR *lpPal;

	DEBUGOUT(DILAPI, "\t%s-->buildDIBhead(%d)", (LPSTR) __FILE__, hFile);
	if (!(fs = (FileStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}

	nColors = fs->ihdr.bpp;
	if (nColors == 2) nColors = 4;
	else if (nColors == 16) nColors = 8;

	// if the palette bit isn't set and it's color, it's RGB data
	// Otherwise, if the palette bit isn't set it must be Grayscale
	if (!(fs->ihdr.colortype & 0x01) && (fs->ihdr.colortype & 0x02)) nColors = 24;

	bi.biWidth = fs->ihdr.width;
	bi.biHeight = fs->ihdr.height;
	bi.biBitCount = nColors;
	bi.biPlanes = 1;
	bi.biSize = sizeof(BITMAPINFOHEADER);
	bi.biCompression = BI_RGB;
	bi.biSizeImage = 0;
	bi.biXPelsPerMeter = 0;
	bi.biYPelsPerMeter = 0;
	bi.biClrImportant = 0;
	bi.biClrUsed = (1 << nColors) & 0x01ff;

	lpii->nPages = 1;
	lpii->lOrgWid = bi.biWidth;
	lpii->lOrgHi = bi.biHeight;
	lpii->bFlags = 0;

	lpbi = (LPBITMAPINFO)GlobalAllocPtr(GHND, sizeof(BITMAPINFO)+PALETTESIZE(&bi));
	lpbi->bmiHeader = bi;

	// fill palette entries
	// they need to be in a range from 0 to 255
	if (fs->ihdr.colortype == 0) {
		switch (bi.biBitCount) {
			case 1:
				lpbi->bmiColors[0].rgbRed =	lpbi->bmiColors[0].rgbGreen = lpbi->bmiColors[0].rgbBlue = 0;
				lpbi->bmiColors[0].rgbReserved = 0;

				lpbi->bmiColors[1].rgbRed = lpbi->bmiColors[1].rgbGreen = lpbi->bmiColors[1].rgbBlue = 255;
				lpbi->bmiColors[1].rgbReserved = 0;
				break;
			case 2:
				lpbi->bmiColors[0].rgbRed = lpbi->bmiColors[0].rgbGreen = lpbi->bmiColors[0].rgbBlue = 0;
				lpbi->bmiColors[0].rgbReserved = 0;

				lpbi->bmiColors[1].rgbRed = lpbi->bmiColors[1].rgbGreen = lpbi->bmiColors[1].rgbBlue = 86;
				lpbi->bmiColors[1].rgbReserved = 0;

				lpbi->bmiColors[2].rgbRed = lpbi->bmiColors[2].rgbGreen = lpbi->bmiColors[2].rgbBlue = 172;
				lpbi->bmiColors[2].rgbReserved = 0;

				lpbi->bmiColors[3].rgbRed = lpbi->bmiColors[3].rgbGreen = lpbi->bmiColors[3].rgbBlue = 255;
				lpbi->bmiColors[3].rgbReserved = 0;
				break;
			case 4:
				for (i=0, lpPal = fs->pal; (WORD)i<15; i++) {
					lpbi->bmiColors[i].rgbRed = lpbi->bmiColors[i].rgbGreen = lpbi->bmiColors[i].rgbBlue = i * (256/15);
					lpbi->bmiColors[i].rgbReserved = 0;
				}
				// make sure that the highest value is pure white...
				lpbi->bmiColors[15].rgbRed = lpbi->bmiColors[15].rgbGreen = lpbi->bmiColors[15].rgbBlue = 255;
				lpbi->bmiColors[15].rgbReserved = 0;
				break;
			default:
				for (i=0, lpPal = fs->pal; (WORD)i<PALCOLORS(&bi); i++) {
					lpbi->bmiColors[i].rgbRed = i;
					lpbi->bmiColors[i].rgbGreen = i;
					lpbi->bmiColors[i].rgbBlue = i;
					lpbi->bmiColors[i].rgbReserved = 0;
				}
				break;
		}
	}
	else {
		for (i=0, lpPal = fs->pal; (WORD)i<PALCOLORS(&bi); i++) {
			lpbi->bmiColors[i].rgbRed = *lpPal++;
			lpbi->bmiColors[i].rgbGreen = *lpPal++;
			lpbi->bmiColors[i].rgbBlue = *lpPal++;
			lpbi->bmiColors[i].rgbReserved = 0;
		}
	}

	fs->hpDecodeBuf = (HPSTR)GlobalAllocPtr(GHND, fs->dwFullRowSize);

	lpii->lpbi = (LPBITMAPINFO) lpbi;

	GlobalUnlock(hFile);
	return (fs->hpDecodeBuf ? IMG_OK : IMG_NOMEM);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLprint(HGLOBAL hFile, HDC hPrnDC, LPRECT lpDest, LPRECT lpSrc)
{
	return (IMG_NOTAVAIL);
	UNREFERENCED_PARAMETER(hFile);
	UNREFERENCED_PARAMETER(hPrnDC);
	UNREFERENCED_PARAMETER(lpDest);
	UNREFERENCED_PARAMETER(lpSrc);
}


// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLrender(HGLOBAL hFile, HDC hDC, LPRECT lpDest, LPRECT lpSrc)
{
	return (IMG_ERR);
	UNREFERENCED_PARAMETER(hFile);
	UNREFERENCED_PARAMETER(hDC);
	UNREFERENCED_PARAMETER(lpDest);
	UNREFERENCED_PARAMETER(lpSrc);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLload_wmf(HANDLE hImage, HMETAFILE FAR * lpmeta, LPRECT lpbbox, LPIMG_IO lpIO)
{
	return (IMG_ERR);
	UNREFERENCED_PARAMETER(hImage);
	UNREFERENCED_PARAMETER(lpmeta);
	UNREFERENCED_PARAMETER(lpbbox);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLget_page(HANDLE hImage, LPINT nPage)
{
	return (IMG_ERR);
	UNREFERENCED_PARAMETER(hImage);
	UNREFERENCED_PARAMETER(nPage);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLset_page(HANDLE hImage, int nPage)
{
	return (IMG_ERR);
	UNREFERENCED_PARAMETER(hImage);
	UNREFERENCED_PARAMETER(nPage);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLerror_string(LPSTR lpszBuf, int nMaxLen)
{
	LoadString(pngInstance, GETERROR, lpszBuf, nMaxLen);
	return (IMG_OK);
}

#if DEBUG
void __cdecl FAR Debug(UINT wCategory, LPSTR lpFormat,...)
{
	va_list     vaArgs;

	if (DebugStringOut) {
		va_start(vaArgs, lpFormat);
		(*DebugStringOut) (wCategory, lpFormat, vaArgs);
		va_end(vaArgs);
	}
}
#else
void __cdecl FAR Debug(UINT wCategory, LPSTR lpFormat,...)
{
	UNREFERENCED_PARAMETER(wCategory);
	UNREFERENCED_PARAMETER(lpFormat);
}
#endif
