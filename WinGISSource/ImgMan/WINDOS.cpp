/////////////////////////////////////////////////////////////////////////////
//
//    windos.c
//
//    ImageMan DOS common support code
//
//    Copyright (c) 1992 Data Techniques, Inc.
//    All Rights Reserved
//
/////////////////////////////////////////////////////////////////////////////
// $History: WINDOS.cpp $
// 
// *****************  Version 3  *****************
// User: Johnd        Date: 11/05/98   Time: 5:06p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// #ifdefed code in LoadLibraryPath so its not used in Win32
// 
// *****************  Version 2  *****************
// User: Ericw        Date: 1/20/98    Time: 2:57p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Needed to inlucde windos.h to get the extern C prototypes.
// 
// *****************  Version 1  *****************
// User: Ericw        Date: 7/28/97    Time: 3:31p
// Created in $/ImageMan 16-32/DLL-API Level 2
// For 16 bit builds this file was missing.  It's also been renamed with
// cpp.
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 1/18/95    Time: 3:59p
// Created in $/ImageMan 16-32/DLL
// ImageMan 4.0 Beta 1

#include "internal.h"
#pragma hdrstop
#include <direct.h>
#include <dos.h>
#include <string.h>
#include "modtable.h"	// module-table definition
#include "windos.h"

//
// Peforms a dos find-first function -- compatible with MSC 6.0
//
// Returns DOS error code
//
int FAR _fdos_findfirst(char _far * lpszMask, unsigned nAttribs, struct find_t _far *buf)
{
  int retval;

  _asm {

    push ds

    mov ah, 2fh   ;// first get the current DTA & stash it on stack
    int 21h
    push  es
    push  bx

;// next set DTA to point to buf

    lds dx, buf     ;// DS:DX points to our buffer
    mov ah, 1ah
    int 21h

    ;// now find the first file...

    lds dx, lpszMask  ;// DS:DX points to mask for search
    mov cx, nAttribs  ;// CX = attributes to search for
    mov ah, 4eh
    int 21h       ;// ax contains error code when done
    mov retval, ax

    ;// now restore old DTA

    pop dx          ;// get old DTA offset
    pop ds          ;// get old DTA segment
    mov ah,1ah
    int 21h

    pop ds
  }

  return retval;
}

//
// Performs a dos-findnext function - compatible w/MSC 6.0
//
// returns DOS error code
//
int FAR _fdos_findnext(struct find_t _far *buf)
{
  int retval;

  _asm {

    push ds

    ;// first stash old DTA address on stack

    mov ah, 2fh
    int 21h
    push  es
    push  bx

    ; //now set DTA address to point to buf
    lds dx, buf     ;// DS:DX points to our buffer
    mov ah, 1ah
    int 21h       ;// make sure we're set to correct DTA

    mov ah, 4fh
    int 21h
    mov retval, ax

    ;// now restore old DTA

    pop dx          ;// get old DTA offset
    pop ds          ;// get old DTA segment
    mov ah,1ah
    int 21h

    pop ds
  }

  return retval;
}

//
// Puts the current working directory in buf (max of nMax chars)
//
void FAR _fgetcwd(char _far *buf, int nMax)
{
  char  localBuf[65];
  LPSTR p;

  p = localBuf;
  _asm {

    push  ds

    mov ah,47h
    xor dl,dl     ;// use 0 for current drive
    lds si,p      ;// DS:SI points at our buffer, which has to
              ;// be 64 bytes long!
    int 21h

    pop ds
  }

  _fstrncpy(buf, (char far *)localBuf, nMax);
}

//
// changes to a new directory
//
int FAR _fchdir(char _far *lpDir)
{
  int i;

  _asm {
    push  ds

    lds dx,lpDir
    mov ah,3bh
    int 21h
    mov i,ax

    pop ds
  }

  return i;
}

//
// Given a full path specification for a library, loads the lib correctly
//
HANDLE FAR LoadLibraryPath(LPSTR lpszPath)
{
#ifndef WIN32
	UCHAR nSaveDrive;
	int nLen;
	char  saveDir[100], toDir[100];
	HANDLE hRet;

  nSaveDrive = _getdrive();

  if ((*lpszPath-'A'+1) != nSaveDrive) {
    _chdrive(*lpszPath - 'A' + 1);
  }

  nLen = _fstrlen(lpszPath);
  while (nLen && lpszPath[nLen] != '\\') {
    nLen--;
  }

  saveDir[0] = '\\';
  _fgetcwd((LPSTR)(&saveDir[1]), 90);

  _fstrcpy(toDir, lpszPath);
  toDir[nLen] = '\0';
  _fchdir(toDir);
#endif

  hRet = LoadLibrary((LPSTR)(&lpszPath[nLen+1]));

#ifndef WIN32
  //
  // revert to original directory on this drive before returning to old drive
  //
  _fchdir((LPSTR)saveDir);
  _chdrive(nSaveDrive);
#endif
  return hRet;
}

//
// Given a far pointer to a string which represents an int, convert from
// string to int...
//
int FAR _fatoi(char _far *lpInt)
{
	int	i = 0;

	while (*lpInt) {
		i *= 10;
		i += (*lpInt++ - '0');
	}

	return i;
}

int FAR _funlink(char _far *lpBye)
{
	int	retval;

	_asm {
		push	ds
		lds	dx,lpBye		;//get address of file to remove
		mov	ah,41h		;//DOS delete file function
		int	21h
		jb		dl_err		;//if jump, couldn't delete
		xor	ax,ax			;//if ok, return 0
    }

	dl_err:

    _asm {
		mov	retval,ax	;//store return value
		pop	ds
	}

	return retval;
}


FARPROC FAR GetAnyProcAddress(HANDLE hInst, LPSTR lpProc)
{
	MODULE_TABLE FAR *lpMod;
	HANDLE	hMod;
	WORD		wOldFlags;
	FARPROC	fp = NULL;
	
	hMod = GetInstanceModule(hInst);
	if (hMod) {
		lpMod = (MODULE_TABLE FAR *)GlobalLock(hMod);
		wOldFlags = lpMod->ne_flags;
		lpMod->ne_flags |= 0x8000;	// hi-bit is 1 for DLL
                fp = GetProcAddress((HINSTANCE)hInst, lpProc);
		lpMod->ne_flags = wOldFlags;	// restore flags
		GlobalUnlock(hMod);
	}

	return fp;
}

#define WRITEBLKSIZE	8096L	//I've heard that 8K blocks are optimal
unsigned long FAR PASCAL HugeWrite(int fHand, char huge *hpBuf, unsigned long lBytes)
{
	unsigned long lCnt = 0;
	unsigned long tmp;
	unsigned retval;

	while (lBytes) {
		tmp = min(lBytes, WRITEBLKSIZE);

		if ( (retval = _lwrite(fHand, hpBuf, (unsigned)tmp)) != (unsigned)tmp) {
			return lCnt + retval;
		}

		hpBuf += tmp;
		lCnt += tmp;
		lBytes -= tmp;
	}

	return lCnt;
}
