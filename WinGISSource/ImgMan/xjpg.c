// ///////////////////////////////////////////////////////////////////////////
//
// xjpg.c
//
// ImageMan JPEG/JFIF DEL Code
//
// Copyright (c) 1991-5 Data Techniques, Inc.
// All Rights Reserved
//
// This particular implementation of a JPEG file writer does one
// thing and one thing only - a baseline JPEG file that is JFIF
// compliant, using standard 2:1:1 subsampling ratios. Most of
// the other JPEG stuff seems almost redundant, in that it doesn't
// provide sufficient payback, in terms of compression ratios,
// to warrant the effort at this time (arithmetic compression, for instance
// provides about 10% better compression - worthwhile, but not mandatory).
//
// ///////////////////////////////////////////////////////////////////////////
// $History: XJPG.C $
// 
// *****************  Version 12  *****************
// User: Johnd        Date: 6/29/00    Time: 4:58p
// Updated in $/ImageMan 16-32/DELS
// Fixed issue 256 (Incorrect resolution being written)
// 
// *****************  Version 11  *****************
// User: Erict        Date: 6/28/00    Time: 9:57a
// Updated in $/ImageMan 16-32/DELS
// added link to optblk.obj to mak file so that there wouldn't have to be
// a 32 bit call to a lpOptBlkGet that was causing problems in the 16 bit
// minidrv.
// 
// *****************  Version 10  *****************
// User: Timk         Date: 11/11/98   Time: 4:31p
// Updated in $/ImageMan 16-32/DELS
// Fixed some hideous output bugs by making sure that we didn't overwrite
// the output buffer. We also now use a fixed-size buffer, which we can do
// because we're checking the fullness each time we output a byte.
// 
// Also fixed a small bug when up-sampling 16-color images.
// 
// *****************  Version 9  *****************
// User: Ericj        Date: 4/29/98    Time: 11:04a
// Updated in $/ImageMan 16-32/DELS
// Changed fname in LEXPORTINFO to MAX_PATH to export files
// longer than 100 characters.
// 
// *****************  Version 8  *****************
// User: Johnd        Date: 4/10/98    Time: 5:57p
// Updated in $/ImageMan 16-32/DELS
// Fixed xDens/yDens fields
// 
// *****************  Version 7  *****************
// User: Johnd        Date: 3/20/98    Time: 12:02p
// Updated in $/ImageMan 16-32/DELS
// Added support for setting the sDens/yDens fields
// 
// *****************  Version 6  *****************
// User: Timk         Date: 7/07/97    Time: 4:20p
// Updated in $/ImageMan 16-32/DELS
// Fixed very wide image saves. We were using a fixed compression buffer
// size, which I have changed to a dynamic size, so we should not have a
// problem.
// 
// *****************  Version 5  *****************
// User: Timk         Date: 2/11/97    Time: 11:36p
// Updated in $/ImageMan 16-32/DELS
// Added support for writing grayscale images as 8-bit images instead of
// 24-bit images.
// 
// *****************  Version 4  *****************
// User: Johnd        Date: 1/10/97    Time: 11:20a
// Updated in $/ImageMan 16-32/DELS
// Fixed GPF on wide image saves & check GlobalAllocPtr calls better
// 
// *****************  Version 3  *****************
// User: Johnd        Date: 9/11/96    Time: 8:20a
// Updated in $/ImageMan 16-32/DELS
// Fixed Bug 418 (Missing EOI)
// 
// *****************  Version 2  *****************
// User: Timk         Date: 7/25/95    Time: 10:55p
// Updated in $/ImageMan 16-32/DELS
// Added support for writing using IO pointer blocks.
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 1/18/95    Time: 6:06p
// Created in $/ImageMan 16-32/DELS
// ImageMan 4.0 Beta 1
//

#define _WINDLL
#include <windows.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <stdlib.h>
#include <assert.h>
#include "internal.h"
#include "xjpg.h"
#include "optblk.h"

#ifndef WIN32
#include "windos.h"
#endif

#pragma pack(4)

#define COLORSHIFT                      7
#define DEFAULT_Q                       75

#ifdef WIN32
#define SELECTOROF(x) (x)
#define HugeWrite _lwrite
#endif


#ifdef WIN32

#ifdef WINGIS // so the file TLSByJHC.h was included

#ifdef NDEBUG
#define SETERROR(E) bJHCTLSSetValue((DWORD)(E))
#else
static void IM2PCXerr(int);
#define SETERROR(E) { \
	SETERROR(E) bJHCTLSSetValue((DWORD)(E)); \
	if ((E) != IMG_OK) IM2PCXerr(E); \
	}
#endif

#define GETERROR    nJHCTLSGetValue()

#else

static DWORD TlsIndex;			  // Thread local storage index
#ifdef NDEBUG
#define SETERROR(E) TlsSetValue(TlsIndex,(LPVOID)(DWORD)(E))
#else
static void IM2PCXerr(int);
#define SETERROR(E) { \
	TlsSetValue(TlsIndex,(LPVOID)(DWORD)(E)); \
	if ((E) != IMG_OK) IM2PCXerr(E); \
	}
#endif
#define GETERROR    ((DWORD)TlsGetValue(TlsIndex))
#endif // WINGIS


#else
static int  nErrID;				  // id of latest error
#define SETERROR(E) nErrID = (E)
#define GETERROR    (nErrID)
#endif

#define COMPRESSBUFSIZE 65536

typedef struct _EXPORTINFO {
	char        fName[MAX_PATH];		  // name of file to export
	LPVOID		lpHandle;
	LPIMG_IO		lpIO;
	long        nImgWidth;
	long        nImgHeight;
	LPSTR       lpDecodeBuf;
	unsigned long nCompUsed;
	int         nOutRowSize;
	int         nMCUsPerRow;
	HPSTR       lpY;
	HPSTR       lpCb;
	HPSTR       lpCr;
	int         nLastYdc;
	int         nLastCbdc;
	int         nLastCrdc;
	int         nLineCnt;		  // when ==16, we've got an MCU row
	LPSTR       lpSubY;			  // subsampled Y component
	LPSTR       lpSubCb;			  // subsampled Cb component
	LPSTR       lpSubCr;			  // subsampled Cr component
	WORD        nBits;			  // output bit buffer
	int         nAvail;			  // # of bits available in nBits
	HUFF_TBL    dchuff_tbl[2];
	HUFF_TBL    achuff_tbl[2];
	UCHAR       luminance_q[DCTSIZE2];
	UCHAR       chrominance_q[DCTSIZE2];
	DCTBLOCK    dctblock;		  // block to use when DCT'ing some data
	RGBQUAD     pal[256];
	int         nColors;
	WORD				wComponents;	//# of components -- 3 for RGB, 1 for Grayscale
	WORD				wRowsPerMCU;	//# of image rows covered by an MCU row
}           EXPORTINFO, FAR * LPEXPORTINFO;


void        subsample(UCHAR FAR * lpInBuf, UCHAR FAR * lpOutBuf, long nImgWidth, int nSubRowSize);
void        copyDCTblock(unsigned char FAR * lpBlkStart, LPINT lpDest, int nRowSize);
int         processDCTblock(DCTBLOCK dct, UCHAR FAR * qtbl, LPHUFF_TBL dctbl, LPHUFF_TBL actbl, LPINT lpLastDC, LPEXPORTINFO lpex);
int         emit_bits(LPEXPORTINFO lpex, int nVal, int nLen);
void        init_huff(UCHAR * pBits, UCHAR * pVal, LPHUFF_TBL lpHuff);
void        setupQuantTables(LPEXPORTINFO lpex, unsigned qf);
int         processRows(LPEXPORTINFO lpex);
void        set_error(int, LPCSTR);
void        swap(LPSTR a);
void        swapl(LPSTR a);

HANDLE      hInst;
HANDLE      hScratch;			  // global scratch buffer...

DEBUGPROC   DebugStringOut;

UCHAR       bitmask[8] = {0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02, 0x01};

int         Yr[256], Yg[256], Yb[256], Cbr[256], Cbg[256], Cbb[256], Crr[256], Crg[256], Crb[256];
char        optbuf[100];
XERRORPROC  XSetError;

//LPFNOPTBLKGET lpfnOptBlkGet;

int NEAR PASCAL round(double val);


/* Set up the standard Huffman tables (cf. JPEG standard section K.3) */
UCHAR       dc_luminance_bits[17] =
{ /* 0-base */ 0, 0, 1, 5, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0};
UCHAR       dc_luminance_val[] =
{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};

UCHAR       dc_chrominance_bits[17] =
{ /* 0-base */ 0, 0, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0};
UCHAR       dc_chrominance_val[] =
{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};

UCHAR       ac_luminance_bits[17] =
{ /* 0-base */ 0, 0, 2, 1, 3, 3, 2, 4, 3, 5, 5, 4, 4, 0, 0, 1, 0x7d};
UCHAR       ac_luminance_val[] =
{0x01, 0x02, 0x03, 0x00, 0x04, 0x11, 0x05, 0x12,
	0x21, 0x31, 0x41, 0x06, 0x13, 0x51, 0x61, 0x07,
	0x22, 0x71, 0x14, 0x32, 0x81, 0x91, 0xa1, 0x08,
	0x23, 0x42, 0xb1, 0xc1, 0x15, 0x52, 0xd1, 0xf0,
	0x24, 0x33, 0x62, 0x72, 0x82, 0x09, 0x0a, 0x16,
	0x17, 0x18, 0x19, 0x1a, 0x25, 0x26, 0x27, 0x28,
	0x29, 0x2a, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39,
	0x3a, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49,
	0x4a, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x59,
	0x5a, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69,
	0x6a, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78, 0x79,
	0x7a, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89,
	0x8a, 0x92, 0x93, 0x94, 0x95, 0x96, 0x97, 0x98,
	0x99, 0x9a, 0xa2, 0xa3, 0xa4, 0xa5, 0xa6, 0xa7,
	0xa8, 0xa9, 0xaa, 0xb2, 0xb3, 0xb4, 0xb5, 0xb6,
	0xb7, 0xb8, 0xb9, 0xba, 0xc2, 0xc3, 0xc4, 0xc5,
	0xc6, 0xc7, 0xc8, 0xc9, 0xca, 0xd2, 0xd3, 0xd4,
	0xd5, 0xd6, 0xd7, 0xd8, 0xd9, 0xda, 0xe1, 0xe2,
	0xe3, 0xe4, 0xe5, 0xe6, 0xe7, 0xe8, 0xe9, 0xea,
	0xf1, 0xf2, 0xf3, 0xf4, 0xf5, 0xf6, 0xf7, 0xf8,
0xf9, 0xfa};

UCHAR       ac_chrominance_bits[17] =
{ /* 0-base */ 0, 0, 2, 1, 2, 4, 4, 3, 4, 7, 5, 4, 4, 0, 1, 2, 0x77};
UCHAR       ac_chrominance_val[] =
{0x00, 0x01, 0x02, 0x03, 0x11, 0x04, 0x05, 0x21,
	0x31, 0x06, 0x12, 0x41, 0x51, 0x07, 0x61, 0x71,
	0x13, 0x22, 0x32, 0x81, 0x08, 0x14, 0x42, 0x91,
	0xa1, 0xb1, 0xc1, 0x09, 0x23, 0x33, 0x52, 0xf0,
	0x15, 0x62, 0x72, 0xd1, 0x0a, 0x16, 0x24, 0x34,
	0xe1, 0x25, 0xf1, 0x17, 0x18, 0x19, 0x1a, 0x26,
	0x27, 0x28, 0x29, 0x2a, 0x35, 0x36, 0x37, 0x38,
	0x39, 0x3a, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48,
	0x49, 0x4a, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58,
	0x59, 0x5a, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68,
	0x69, 0x6a, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78,
	0x79, 0x7a, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87,
	0x88, 0x89, 0x8a, 0x92, 0x93, 0x94, 0x95, 0x96,
	0x97, 0x98, 0x99, 0x9a, 0xa2, 0xa3, 0xa4, 0xa5,
	0xa6, 0xa7, 0xa8, 0xa9, 0xaa, 0xb2, 0xb3, 0xb4,
	0xb5, 0xb6, 0xb7, 0xb8, 0xb9, 0xba, 0xc2, 0xc3,
	0xc4, 0xc5, 0xc6, 0xc7, 0xc8, 0xc9, 0xca, 0xd2,
	0xd3, 0xd4, 0xd5, 0xd6, 0xd7, 0xd8, 0xd9, 0xda,
	0xe2, 0xe3, 0xe4, 0xe5, 0xe6, 0xe7, 0xe8, 0xe9,
	0xea, 0xf2, 0xf3, 0xf4, 0xf5, 0xf6, 0xf7, 0xf8,
0xf9, 0xfa};

/* This is the sample quantization table given in the JPEG spec section K.1, but expressed in
 * zigzag order (as are all of our quant. tables). The spec says that the values given produce
 * "good" quality, and when divided by 2, "very good" quality.  (These two settings are selected by
 * quality=50 and quality=75 respectively.) */
UCHAR       std_luminance_quant_tbl[DCTSIZE2] = {
	16, 11, 12, 14, 12, 10, 16, 14,
	13, 14, 18, 17, 16, 19, 24, 40,
	26, 24, 22, 22, 24, 49, 35, 37,
	29, 40, 58, 51, 61, 60, 57, 51,
	56, 55, 64, 72, 92, 78, 64, 68,
	87, 69, 55, 56, 80, 109, 81, 87,
	95, 98, 103, 104, 103, 62, 77, 113,
	121, 112, 100, 120, 92, 101, 103, 99
};
UCHAR       std_chrominance_quant_tbl[DCTSIZE2] = {
	17, 18, 18, 24, 21, 24, 47, 26,
	26, 47, 99, 66, 56, 66, 99, 99,
	99, 99, 99, 99, 99, 99, 99, 99,
	99, 99, 99, 99, 99, 99, 99, 99,
	99, 99, 99, 99, 99, 99, 99, 99,
	99, 99, 99, 99, 99, 99, 99, 99,
	99, 99, 99, 99, 99, 99, 99, 99,
	99, 99, 99, 99, 99, 99, 99, 99
};

// ZAG[i] is the natural-order position of the i'th element of zigzag order.
short       ZAG[64] = {
	0, 1, 8, 16, 9, 2, 3, 10,
	17, 24, 32, 25, 18, 11, 4, 5,
	12, 19, 26, 33, 40, 48, 41, 34,
	27, 20, 13, 6, 7, 14, 21, 28,
	35, 42, 49, 56, 57, 50, 43, 36,
	29, 22, 15, 23, 30, 37, 44, 51,
	58, 59, 52, 45, 38, 31, 39, 46,
	53, 60, 61, 54, 47, 55, 62, 63
};

#ifdef WIN32
// /////////////////////////////////////////////////////////////////////////
//
// DllMain
//
// This is the Win32 'DllEntryPoint'. It is similar in function to LibMain
// and the WEP in 16-bit land, but is much more thorough. LibMain is
// called whenever a process or thread attaches or detaches to the
// BMP DIL
//
// Entry:
// hinstDLL  - handle to the ImageMan DLL
// fdwReason - notification code (attach/detach)
// DLL_PROCESS_ATTACH
// DLL_PROCESS_DETACH
// DLL_THREAD_ATTACH
// DLL_THREAD_DETACH
// lpvReserved - NULL during dynamic process attach/detach, non-NULL
// during static attach/detach.
// Exit:
// DLL_PROCESS_ATTACH - TRUE for success, FALSE for failure
// all others         - return code ignored
// /////////////////////////////////////////////////////////////////////////

BOOL APIENTRY DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
	int         i;

	switch (fdwReason) {
		// The DLL is attaching to a process, due to LoadLibrary.
		case DLL_PROCESS_ATTACH:
			assert(!lpvReserved);  // must always be loaded via LoadLibrary!

			hInst = hinstDLL;		  // save our identity
#ifndef WINGIS
			// Allocate a "Thread Local Storage" (TLS) index right away.

			if ((TlsIndex = TlsAlloc()) == 0xFFFFFFFF)
				return (FALSE);
#else
			bJHCTLSClear();
#endif
			// Fall thru: Initialize the index for first thread.

			for (i = 0; i < 256; i++) {
				// lpY[i] = (UCHAR)(0.299*r + 0.587*g + 0.114*b);
				// lpCb[i] = (UCHAR)(-0.1687*r - 0.3313*g + 0.5*b + CENTERJSAMPLE);
				// lpCr[i] = (UCHAR)(0.5*r - 0.4187*g - 0.0813*b + CENTERJSAMPLE);
				Yr[i] = (int) (0.299 * (i << COLORSHIFT));
				Yg[i] = (int) (0.587 * (i << COLORSHIFT));
				Yb[i] = (int) (0.114 * (i << COLORSHIFT));
				Cbr[i] = (int) (-0.1687 * (i << COLORSHIFT));
				Cbg[i] = (int) (-0.3313 * (i << COLORSHIFT));
				Cbb[i] = (int) (0.5 * (i << COLORSHIFT));
				Crr[i] = (int) (0.5 * (i << COLORSHIFT));
				Crg[i] = (int) (-0.4187 * (i << COLORSHIFT));
				Crb[i] = (int) (-0.0813 * (i << COLORSHIFT));
			}

		// The attached process created a new thread.
		case DLL_THREAD_ATTACH:
#ifndef WINGIS
			// Initialize the TLS index for this thread... it's actually 'nErrID'
			if (!TlsSetValue(TlsIndex, (LPVOID) 0))
				return (FALSE);
#else
			nJHCTLSAlloc(); // Stamp this thread to be existed
#endif
			break;

		// The thread of the attached process terminates.
		case DLL_THREAD_DETACH:
#ifdef WINGIS
			bJHCTLSFree(); // Free this thread entry
#endif
			break;

		// The DLL is detaching from a process, due to a call to FreeLibrary.
		case DLL_PROCESS_DETACH:
#ifndef WINGIS
			// Release the TLS index.
			TlsFree(TlsIndex);
#else
			bJHCTLSClear();  // Release all the threads error descriptors
#endif
			break;
	}
	return (TRUE);

	UNREFERENCED_PARAMETER(lpvReserved);
}

#else // WIN32

int FAR PASCAL LibMain(hInstance, wDataSeg, wHeapSize, lpCmdLine)
	HANDLE      hInstance;
	WORD        wDataSeg, wHeapSize;
	LPSTR       lpCmdLine;
{
	long        i;

	if (wHeapSize == 0)			  /* no heap indicates fatal error */
		return 0;

	hInst = hInstance;			  // save instance handle

	hScratch = GlobalAlloc(GHND, 10L);

#if DEBUG
	{
		HANDLE      hMod = GetModuleHandle("IMGMAN2.DLL");
		if (hMod)
			DebugStringOut = (DEBUGPROC) GetProcAddress(hMod, "DebugStringOut");
	}
#endif

	// pre-calculate RGB=>YCbCr conversion tables
	for (i = 0; i < 256; i++) {
		// lpY[i] = (UCHAR)(0.299*r + 0.587*g + 0.114*b);
		// lpCb[i] = (UCHAR)(-0.1687*r - 0.3313*g + 0.5*b + CENTERJSAMPLE);
		// lpCr[i] = (UCHAR)(0.5*r - 0.4187*g - 0.0813*b + CENTERJSAMPLE);
		Yr[i] = (int) (0.299 * (i << COLORSHIFT));
		Yg[i] = (int) (0.587 * (i << COLORSHIFT));
		Yb[i] = (int) (0.114 * (i << COLORSHIFT));
		Cbr[i] = (int) (-0.1687 * (i << COLORSHIFT));
		Cbg[i] = (int) (-0.3313 * (i << COLORSHIFT));
		Cbb[i] = (int) (0.5 * (i << COLORSHIFT));
		Crr[i] = (int) (0.5 * (i << COLORSHIFT));
		Crg[i] = (int) (-0.4187 * (i << COLORSHIFT));
		Crb[i] = (int) (-0.0813 * (i << COLORSHIFT));
	}

	return 1;
}
#endif

VOID WINAPI WEP(int bSystemExit)
{
	if (bSystemExit) {			  // if exiting Windows entirely

	}
}

LPSTR IMAPI IMXDLget_ext(LPXINITSTRUCT lpInit)
{
	lpInit->lFlags = XF_8BIT | XF_ALLCOLORS;

	XSetError = lpInit->SetError;

//	if (lpInit->hImgManInst)
//		lpfnOptBlkGet = (LPFNOPTBLKGET) GetProcAddress(lpInit->hImgManInst, "ImgXOptBlkGet");

	return (LPSTR) "JPG;JPEG (*.JPG)";
}

LPSTR IMAPI IMXDLget_ver(void)
{
	return (LPSTR) "1.00";
}

HANDLE IMAPI IMXDLbegin_write(LPVOID lpOpenInfo, LPIMG_IO lpIO, LPCSTR lpszAlias, LPBITMAPINFO lpDIB, HANDLE hOptBlk, LPXINFO lpInfo)
{
	LPVOID		lpHandle;
	HANDLE      hImage = NULL;
	int         nColors = (1 << lpDIB->bmiHeader.biBitCount);
	int         i;
	LPEXPORTINFO lpex;
	JFIFHEAD    jHead;			  /* JFIF extensions, go in APP0 */
	QUANTTABLE  qtbl;
	HUFFTABLE   htbl;
	unsigned    quality;			  // 1(worst) to 100(best)


	// verify that we're tryin' to write a 24-bit image
	// if (lpDIB->bmiHeader.biBitCount != 24) {
	// set_error(EXP_BAD_COLORS, lpszAlias);
	// return NULL;
	// }
	if (lpIO->open(lpOpenInfo, OF_CREATE, &lpHandle) == IMG_OK) {

		if (!(OptBlkGet(hOptBlk, "LOSSY_QUALITY", (LPSTR) optbuf))) {
			quality = atoi(optbuf);

		} else
			quality = DEFAULT_Q;	  // default


		// now create and initialize our data structure
		hImage = GlobalAlloc(GHND, sizeof(EXPORTINFO));

		if (hImage) {
			lpex = (LPEXPORTINFO) GlobalLock(hImage);
			lpex->lpIO = lpIO;
			lpex->lpHandle = lpHandle;
			_fstrcpy(lpex->fName, lpszAlias);

			// Check bit-depth & palette to determine if we have a 
			// grayscale image. If so, we'll output it as a single
			// component. Otherwise, it's 3 components (Luminance, Cr, Cb)
			lpex->wComponents = 3;	//this is the default
			if (lpDIB->bmiHeader.biBitCount <= 8) {
				// only if *all* the palette entries are grayscale can we assume grayscale.
				lpex->wComponents = 1;
				for (i=0; i<nColors; i++) {
					if ( (lpDIB->bmiColors[i].rgbRed != lpDIB->bmiColors[i].rgbGreen) ||
								(lpDIB->bmiColors[i].rgbGreen != lpDIB->bmiColors[i].rgbBlue) ) {
								lpex->wComponents = 3;
					}
				}
			}


			// The file header looks like:
			// SOI
			// APP0    (this has to go right after SOI according to JFIF)
			// Frame Header
			jHead.soi = M_SOI;

			// APP0 Stuff
			jHead.jfif.app0 = M_APP0;
			jHead.jfif.app0len = sizeof(JFIFINFO) - 2;
			swap((LPSTR) & jHead.jfif.app0len);
			_fstrcpy(jHead.jfif.id, "JFIF");
			jHead.jfif.version = 0x0201;	// currently v1.02 (swapped)

			jHead.jfif.units = 1;  // pixels are my units

			jHead.jfif.xdens = round(lpDIB->bmiHeader.biXPelsPerMeter / 39.37);
			jHead.jfif.ydens = round(lpDIB->bmiHeader.biYPelsPerMeter / 39.37);

			swap((LPSTR) & jHead.jfif.xdens);
			swap((LPSTR) & jHead.jfif.ydens);

			jHead.jfif.xthumb = jHead.jfif.ythumb = 0;

			// frame header stuff
			jHead.fHead.sof = M_SOF0;
			jHead.fHead.fLen = sizeof(FRAMEHEAD) -(sizeof(COMPONENT)*(3-lpex->wComponents)) - 2;
			swap((LPSTR) & jHead.fHead.fLen);
			jHead.fHead.precision = 8;	// 8-bit precision is standard

			jHead.fHead.y = (int) (lpDIB->bmiHeader.biHeight);
			swap((LPSTR) & jHead.fHead.y);
			jHead.fHead.x = (int) (lpDIB->bmiHeader.biWidth);
			swap((LPSTR) & jHead.fHead.x);
			jHead.fHead.components = lpex->wComponents;

			if (lpex->wComponents == 1) {
				jHead.fHead.comp[0].id = 1;
				jHead.fHead.comp[0].sampfactor = 0x11;
				jHead.fHead.comp[0].qtbl = 0;	
			}
			else for (i = 0; i < lpex->wComponents; i++) {
				jHead.fHead.comp[i].id = i + 1;
				jHead.fHead.comp[i].sampfactor = (i == 0 ? 0x22 : 0x11);
				jHead.fHead.comp[i].qtbl = (i == 0 ? 0 : 1);	// use 2 quant tables

			}

			// For convenience, we've combined the SOI, APP0 & Frame Header
			// into one mondo structure, which we'll just write once.
			if (lpIO->write(lpHandle, &jHead, sizeof(jHead) ) 
				!= sizeof(jHead)) {
				set_error(EXP_WRITE_HEAD, lpszAlias);
			}
			// setup the quantization tables...
			setupQuantTables(lpex, quality);

			// now output our Quantization tables
			qtbl.dqt = M_DQT;
			qtbl.dqtlen = sizeof(QUANTTABLE) - 2;
			swap((LPSTR) & qtbl.dqtlen);

			// output luminance quantization table (table 0)
			qtbl.prec_id = 0;
			_fmemcpy(qtbl.table, lpex->luminance_q, DCTSIZE2);
			lpIO->write(lpHandle, &qtbl, sizeof(QUANTTABLE));

			if (lpex->wComponents == 3) {
				// output chrominance quantization table (table 1)
				qtbl.prec_id = 1;
				_fmemcpy(qtbl.table, lpex->chrominance_q, DCTSIZE2);
				lpIO->write(lpHandle, &qtbl, sizeof(QUANTTABLE));
			}

			// Huffman tables
			//
			// Since we're using static Huffman tables, I've pre-calculated
			// the lengths to save some small amount of time.
			//
			htbl.dht = M_DHT;
			htbl.dhtlen = sizeof(HUFFTABLE) - 2 + 12;	// DC table sizes

			swap((LPSTR) & htbl.dhtlen);

			htbl.acdc_id = 0x00;
			_fmemcpy(htbl.lens, &(dc_luminance_bits[1]), 16);
			lpIO->write(lpHandle, &htbl, sizeof(HUFFTABLE));
			lpIO->write(lpHandle, &dc_luminance_val, 12);

			if (lpex->wComponents == 3) {
				_fmemcpy(htbl.lens, &(dc_chrominance_bits[1]), 16);
				htbl.acdc_id = 0x01;
				lpIO->write(lpHandle, &htbl, sizeof(HUFFTABLE));
				lpIO->write(lpHandle, &dc_chrominance_val, 12);
			}

			htbl.dhtlen = sizeof(HUFFTABLE) - 2 + 162;	// AC table sizes

			swap((LPSTR) & htbl.dhtlen);
			_fmemcpy(htbl.lens, &(ac_luminance_bits[1]), 16);
			htbl.acdc_id = 0x10;
			lpIO->write(lpHandle, &htbl, sizeof(HUFFTABLE));
			lpIO->write(lpHandle, &ac_luminance_val, 162);

			if (lpex->wComponents == 3) {
				_fmemcpy(htbl.lens, &(ac_chrominance_bits[1]), 16);
				htbl.acdc_id = 0x11;
				lpIO->write(lpHandle, &htbl, sizeof(HUFFTABLE));
				lpIO->write(lpHandle, &ac_chrominance_val, 162);
			}

			// -------------------------------------------------------------
			// Now we're finally ready for the SOS marker
			if (lpex->wComponents == 3) {
				SCANHEAD3    scan;
				scan.sos = M_SOS;
				scan.soslen = sizeof(SCANHEAD3) - 2;
				swap((LPSTR) & scan.soslen);
				scan.nComp = 3;
				scan.comp1_id = 1;	  // luminance
				scan.comp1_tables = 0;  // DC=0, AC=0
				scan.comp2_id = 2;	  // Cb
				scan.comp2_tables = 0x11;	// DC=1, AC=1
				scan.comp3_id = 3;	  // Cr
				scan.comp3_tables = 0x11;	// DC=1, AC=1
				scan.spectral_start = 0;
				scan.spectral_end = 63;
				scan.approx = 0;
				lpIO->write(lpHandle, &scan, sizeof(SCANHEAD3));
			}
			else {
				SCANHEAD1    scan;
				scan.sos = M_SOS;
				scan.soslen = sizeof(SCANHEAD1) - 2;
				swap((LPSTR) & scan.soslen);
				scan.nComp = 1;
				scan.comp1_id = 1;	  // luminance
				scan.comp1_tables = 0;  // DC=0, AC=0
				scan.spectral_start = 0;
				scan.spectral_end = 63;
				scan.approx = 0;
				lpIO->write(lpHandle, &scan, sizeof(SCANHEAD1));
			}

			// -------------------------------------------------------------
			// now setup our Exportinfo struct
			lpex->nImgWidth = lpDIB->bmiHeader.biWidth;
			lpex->nImgHeight = lpDIB->bmiHeader.biHeight;
			lpex->nCompUsed = 0;
			lpex->wRowsPerMCU = (lpex->wComponents == 1 ? 8 : 16);

			lpex->lpDecodeBuf = GlobalAllocPtr(GHND, COMPRESSBUFSIZE);
			lpex->lpY = GlobalAllocPtr(GHND, lpDIB->bmiHeader.biWidth * lpex->wRowsPerMCU * lpex->wComponents);	

			if (lpex->wComponents == 3) {
				lpex->lpCb = lpex->lpY + lpDIB->bmiHeader.biWidth * lpex->wRowsPerMCU;
				lpex->lpCr = lpex->lpCb + lpDIB->bmiHeader.biWidth * lpex->wRowsPerMCU;
			}

			lpex->nLineCnt = 0;
			lpex->nLastYdc = lpex->nLastCrdc = lpex->nLastCbdc = 0;
			lpex->nBits = 0;
			lpex->nAvail = 8;

			lpex->nColors = nColors;

			if (nColors <= 256)
				for (i = 0; i < nColors; i++)
					lpex->pal[i] = lpDIB->bmiColors[i];	// store palette for later

			init_huff((UCHAR *) dc_luminance_bits, (UCHAR *) dc_luminance_val, &(lpex->dchuff_tbl[0]));
			init_huff((UCHAR *) dc_chrominance_bits, (UCHAR *) dc_chrominance_val, &(lpex->dchuff_tbl[1]));
			init_huff((UCHAR *) ac_luminance_bits, (UCHAR *) ac_luminance_val, &(lpex->achuff_tbl[0]));
			init_huff((UCHAR *) ac_chrominance_bits, (UCHAR *) ac_chrominance_val, &(lpex->achuff_tbl[1]));

			// the Luminance sub-sampling buffer is used only to
			// align the input on MCU boundaries
			lpex->nMCUsPerRow = (int) ((lpDIB->bmiHeader.biWidth + (lpex->wRowsPerMCU-1)) / lpex->wRowsPerMCU);
			lpex->lpSubY = GlobalAllocPtr(GHND, (DWORD)lpex->nMCUsPerRow * lpex->wRowsPerMCU * lpex->wRowsPerMCU);

			if( !lpex->lpSubY ) {
				lpIO->close(lpHandle);
				GlobalUnlock( hImage );
				GlobalFree( hImage );
				set_error(EXP_NOMEM, lpszAlias);
				return NULL;
			}


			// the CbCr sub-sampling buffer should be 1/4 the size of the regular-size
			// buffer and should be a multiple of DCTSIZE (8 in our case)
			lpex->lpSubCb = GlobalAllocPtr(GHND, (DWORD)lpex->nMCUsPerRow * 8 * 8 * 2);

			if( !lpex->lpSubCb ) {
				lpIO->close(lpHandle);
				GlobalUnlock( hImage );
				GlobalFree( hImage );
				set_error(EXP_NOMEM, lpszAlias);
				return NULL;
			}

			lpex->lpSubCr = lpex->lpSubCb + lpex->nMCUsPerRow * 8 * 8;
			GlobalUnlock(hImage);
		} else {
			lpIO->close(lpHandle);
		}
	} else {							  // unable to open the file

		hImage = NULL;
		set_error(EXP_NOOPEN, lpszAlias);
	}

	lpInfo->lFlags = XF_8BIT | XF_24BIT;	// NOTE no XF_PALETTE - can only do 8-bit grayscale

	return hImage;
}

int IMAPI   IMXDLwrite_block(HANDLE hImage, int nRows, int nRowSize, LPSTR lpBits, LPXINFO lpInfo)
{
	LPEXPORTINFO lpex;
	int         retval = IMGX_OK;
	int         i, j, k;
	HPUSTR      hpBits;
	HPSTR       lpY, lpCb, lpCr, hpJunk;
	unsigned    r, g, b;

	lpex = (LPEXPORTINFO) GlobalLock(hImage);
	lpY = (HPSTR) (lpex->lpY + lpex->nLineCnt * lpex->nImgWidth);
	if (lpex->wComponents == 3) {
		lpCb = (HPSTR) (lpex->lpCb + lpex->nLineCnt * lpex->nImgWidth);
		lpCr = (HPSTR) (lpex->lpCr + lpex->nLineCnt * lpex->nImgWidth);
	}

	//
	// Since we're receiving a DIB block, we need to start at the last
	// row in the block and work our way backwards...
	hpBits = (HPUSTR) lpBits + (long) (nRows - 1) * nRowSize;

	while (nRows--) {
		//
		// deal with our progress indicator...
		//
		lpInfo->lIncCnt += nRowSize;
		if (lpInfo->lIncCnt > lpInfo->lIncrement) {
			lpInfo->lProcessed += lpInfo->lIncCnt;
			if (lpInfo->lpfnStat)
				if( !((*lpInfo->lpfnStat) (0, (int) ((lpInfo->lProcessed * 100) / lpInfo->lTotalBytes), lpInfo->dwUserInfo) )) {
					GlobalUnlock(hImage);
					return IMGX_CANCELLED;
				}
			lpInfo->lIncCnt = 0;
		}
		switch (lpex->nColors) {
			case 2:
				for (i = 0, hpJunk = hpBits; i < lpex->nImgWidth; i += 8, hpJunk++) {
					for (j = 0; j < 8; j++) {
						if ((i + j) >= lpex->nImgWidth)
							break;

						k = (*hpJunk & bitmask[j]) ? 1 : 0;

						b = lpex->pal[k].rgbBlue;
						g = lpex->pal[k].rgbGreen;
						r = lpex->pal[k].rgbRed;

						if (lpex->wComponents == 3) {
							lpY[i + j] = (UCHAR) ((Yr[r] + Yg[g] + Yb[b]) >> COLORSHIFT);
							lpCb[i + j] = (UCHAR) (((Cbr[r] + Cbg[g] + Cbb[b]) >> COLORSHIFT) + CENTERJSAMPLE);
							lpCr[i + j] = (UCHAR) (((Crr[r] + Crg[g] + Crb[b]) >> COLORSHIFT) + CENTERJSAMPLE);
						}
						else lpY[i + j] = (UCHAR) ((Yr[r] + Yg[g] + Yb[b]) >> COLORSHIFT);
					}
				}
				break;

			case 16:
				// if it's an odd-width image, we dont' want to process an extra
				// pixel on the end of the row, so and off the low bit and 
				// we'll do the last pixel, if needed, after the loop...
				for (i = 0, hpJunk = hpBits; i < (lpex->nImgWidth & 0xfffffffe); i += 2, hpJunk++) {
					b = lpex->pal[((unsigned char) (*hpJunk)) >> 4].rgbBlue;
					g = lpex->pal[((unsigned char) (*hpJunk)) >> 4].rgbGreen;
					r = lpex->pal[((unsigned char) (*hpJunk)) >> 4].rgbRed;

					lpY[i] = (UCHAR) ((Yr[r] + Yg[g] + Yb[b]) >> COLORSHIFT);
					if (lpex->wComponents == 3) {
						lpCb[i] = (UCHAR) (((Cbr[r] + Cbg[g] + Cbb[b]) >> COLORSHIFT) + CENTERJSAMPLE);
						lpCr[i] = (UCHAR) (((Crr[r] + Crg[g] + Crb[b]) >> COLORSHIFT) + CENTERJSAMPLE);
					}

					b = lpex->pal[(*hpJunk) & 0x0f].rgbBlue;
					g = lpex->pal[(*hpJunk) & 0x0f].rgbGreen;
					r = lpex->pal[(*hpJunk) & 0x0f].rgbRed;

					lpY[i + 1] = (UCHAR) ((Yr[r] + Yg[g] + Yb[b]) >> COLORSHIFT);
					if (lpex->wComponents == 3) {
						lpCb[i + 1] = (UCHAR) (((Cbr[r] + Cbg[g] + Cbb[b]) >> COLORSHIFT) + CENTERJSAMPLE);
						lpCr[i + 1] = (UCHAR) (((Crr[r] + Crg[g] + Crb[b]) >> COLORSHIFT) + CENTERJSAMPLE);
					}
				}
				// now do that pesky last pixel, if it's there...
				if (lpex->nImgWidth & 0x01) {
					b = lpex->pal[((unsigned char) (*hpJunk)) >> 4].rgbBlue;
					g = lpex->pal[((unsigned char) (*hpJunk)) >> 4].rgbGreen;
					r = lpex->pal[((unsigned char) (*hpJunk)) >> 4].rgbRed;

					lpY[i] = (UCHAR) ((Yr[r] + Yg[g] + Yb[b]) >> COLORSHIFT);
					if (lpex->wComponents == 3) {
						lpCb[i] = (UCHAR) (((Cbr[r] + Cbg[g] + Cbb[b]) >> COLORSHIFT) + CENTERJSAMPLE);
						lpCr[i] = (UCHAR) (((Crr[r] + Crg[g] + Crb[b]) >> COLORSHIFT) + CENTERJSAMPLE);
					}
				}
				break;

			case 256:
				for (i = 0, hpJunk = hpBits; i < lpex->nImgWidth; i++, hpJunk++) {
					b = lpex->pal[*hpJunk].rgbBlue;
					g = lpex->pal[*hpJunk].rgbGreen;
					r = lpex->pal[*hpJunk].rgbRed;

					if (lpex->wComponents == 3) {
						lpY[i] = (UCHAR) ((Yr[r] + Yg[g] + Yb[b]) >> COLORSHIFT);
						lpCb[i] = (UCHAR) (((Cbr[r] + Cbg[g] + Cbb[b]) >> COLORSHIFT) + CENTERJSAMPLE);
						lpCr[i] = (UCHAR) (((Crr[r] + Crg[g] + Crb[b]) >> COLORSHIFT) + CENTERJSAMPLE);
					}
					else 
						lpY[i] = (UCHAR) ((Yr[r] + Yg[g] + Yb[b]) >> COLORSHIFT);
				}
				break;

			default:				  // 24-bit

				for (i = 0, hpJunk = hpBits; i < lpex->nImgWidth; i++) {
					b = *hpJunk++;
					g = *hpJunk++;
					r = *hpJunk++;

					lpY[i] = (UCHAR) ((Yr[r] + Yg[g] + Yb[b]) >> COLORSHIFT);
					lpCb[i] = (UCHAR) (((Cbr[r] + Cbg[g] + Cbb[b]) >> COLORSHIFT) + CENTERJSAMPLE);
					lpCr[i] = (UCHAR) (((Crr[r] + Crg[g] + Crb[b]) >> COLORSHIFT) + CENTERJSAMPLE);
				}
				break;
		}

		lpex->nLineCnt++;
		lpY += lpex->nImgWidth;
		if (lpex->wComponents == 3) {
			lpCb += lpex->nImgWidth;
			lpCr += lpex->nImgWidth;
		}

		if (lpex->nLineCnt == lpex->wRowsPerMCU) {	// we've got an MCU-row's worth of data

			processRows(lpex);

			lpY = lpex->lpY;
			if (lpex->wComponents == 3) {
				lpCb = lpex->lpCb;
				lpCr = lpex->lpCr;
			}
			lpex->nLineCnt = 0;
			// _fmemset(lpY, 0, (int)(16*lpex->nImgWidth*3));  // zero all components
		}
		hpBits -= (long) nRowSize;
	}

	GlobalUnlock(hImage);
	return retval;
}

int IMAPI   IMXDLend_write(HANDLE hImage, LPXINFO lpInfo)
{
	LPEXPORTINFO lpex;
	int         retval = IMGX_OK;
	USHORT	nMarker;
	USHORT	nMarkerLen;

	lpex = (LPEXPORTINFO) GlobalLock(hImage);

	// process remainder of the last MCU-row
	if (lpex->nLineCnt)
		processRows(lpex);

	lpex->lpIO->write(lpex->lpHandle, lpex->lpDecodeBuf, lpex->nCompUsed + 1);
	
	if (lpex->lpDecodeBuf) GlobalFreePtr(lpex->lpDecodeBuf);

	if (lpex->lpY) GlobalFreePtr(lpex->lpY);

	if (lpex->lpSubY)	GlobalFreePtr(lpex->lpSubY);

	if (lpex->lpSubCb) GlobalFreePtr(lpex->lpSubCb);

	lpex->lpIO->seek(lpex->lpHandle, 0, SEEK_END);

	// Write our EOI marker to signal the end of the image!

	nMarker = M_EOI;
	nMarkerLen = sizeof(USHORT);

	lpex->lpIO->write(lpex->lpHandle, &nMarker, sizeof(nMarker));
	lpex->lpIO->write(lpex->lpHandle, &nMarkerLen, sizeof(nMarkerLen));

	lpInfo->lWritten = lpex->lpIO->seek(lpex->lpHandle, 0, SEEK_END);


	lpex->lpIO->close(lpex->lpHandle);

	GlobalUnlock(hImage);
	GlobalFree(hImage);

	return retval;
}

void        set_error(int nIndex, LPCSTR lpFilename)
{
	XSETERROR(hInst, nIndex, "IM10XJPG.DEL", lpFilename);
}

void        subsample(UCHAR FAR * lpInBuf, UCHAR FAR * lpOutBuf, long nImgWidth, int nSubRowSize)
{
	int         i, j;
	HPUSTR      lpSubIn1;
	HPUSTR      lpSubIn2;
	HPUSTR      lpSubOut;

	lpSubIn1 = lpInBuf;
	lpSubIn2 = lpSubIn1 + nImgWidth;
	for (i = 0; i < 8; i++) {
		lpSubOut = lpOutBuf + i * nSubRowSize;
		for (j = 0; j < nImgWidth; j += 2) {
			if (j == nImgWidth - 1) {	// last row of odd image

				*lpSubOut++ = (lpSubIn1[j] + lpSubIn2[j]) / 2;
			} else {
				*lpSubOut++ = (lpSubIn1[j] + lpSubIn1[j + 1] + lpSubIn2[j] + lpSubIn2[j + 1]) / 4;
			}
		}
		lpSubIn1 += nImgWidth * 2;
		lpSubIn2 = lpSubIn1 + nImgWidth;
	}
}

void        copyDCTblock(unsigned char FAR * lpBlkStart, LPINT lpDest, int nRowSize)
{
	int         i, j;

	for (i = 0; i < DCTSIZE; i++, lpDest += DCTSIZE, lpBlkStart += nRowSize) {
		for (j = 0; j < DCTSIZE; j++) {
			lpDest[j] = (int) lpBlkStart[j] - CENTERJSAMPLE;
		}
	}
}

//
// fwd_dct, Quantize, convert to zig-zag order, and huffmanize
// the dctblock to the output stream...
//
int         processDCTblock(DCTBLOCK dct, UCHAR FAR * qtbl, LPHUFF_TBL dctbl, LPHUFF_TBL actbl, LPINT lpLastDC, LPEXPORTINFO lpex)
{
	int         i, temp, temp2, nBits, k, r;

	j_fwd_dct(dct);

	if (dct[0] < 0) {
		dct[0] = -dct[0];
		dct[0] += (*qtbl / 2);
		dct[0] /= *qtbl++;
		dct[0] = -dct[0];
	} else {
		dct[0] += (*qtbl / 2);
		dct[0] /= *qtbl++;
	}

	// convert the DC coefficient into a difference
	// DEBUGOUT(IMGDETAIL, "DC: orig = %d, q'd: %d, Q:%d", temp, dct[0], *(qtbl-1));
	temp = dct[0];
	dct[0] = dct[0] - *lpLastDC;
	*lpLastDC = temp;

	// OK, first do the DC coefficient...
	if (dct[0] < 0) {
		temp = -dct[0];
		temp2 = dct[0] - 1;
	} else {
		temp = temp2 = dct[0];
	}

	// get # of bits needed to output this sucker
	nBits = 0;
	while (temp) {
		nBits++;
		temp >>= 1;
	}

	// output the huffman code symbol for # of bits
	emit_bits(lpex, dctbl->ehufco[nBits], dctbl->ehufsi[nBits]);

	// emit the value
	if (nBits)
		emit_bits(lpex, temp2, nBits);

	// now do the ac coefficients...
	r = 0;

	// DEBUGOUT(IMGDETAIL, "%s","-----------------------------");

	for (k = 1; k < DCTSIZE2; k++) {
		temp = dct[ZAG[k]];
		if (temp < 0) {
			temp = -temp;
			temp += (*qtbl / 2);
			temp /= *qtbl++;
			temp = -temp;
		} else {
			temp += (*qtbl / 2);
			temp /= *qtbl++;
		}
		dct[ZAG[k]] = temp;

		if ((temp = dct[ZAG[k]]) == 0) {
			r++;
		} else {
			while (r > 15) {
				emit_bits(lpex, actbl->ehufco[0xf0], actbl->ehufsi[0xf0]);
				r -= 16;
			}

			temp2 = temp;
			if (temp < 0) {
				temp = -temp;
				temp2--;
			}
			nBits = 1;
			while (temp >>= 1)
				nBits++;

			i = (r << 4) + nBits;
			emit_bits(lpex, actbl->ehufco[i], actbl->ehufsi[i]);
			emit_bits(lpex, (unsigned) temp2, nBits);

			r = 0;
		}
	}

	if (r > 0)
		emit_bits(lpex, actbl->ehufco[0], actbl->ehufsi[0]);

	return 0;
}

//
// outputs rightmost nLen bits of nVal to output buffer
//
int         emit_bits(LPEXPORTINFO lpex, int nVal, int nLen)
{
	HPSTR       hpOut = (HPSTR) (lpex->lpDecodeBuf);
	static UCHAR bMask[9] = {0x00, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80};
	static unsigned valMask[17] = {0x00,
		0x0001, 0x0002, 0x0004, 0x0008,
		0x0010, 0x0020, 0x0040, 0x0080,
		0x0100, 0x0200, 0x0400, 0x0800,
	0x1000, 0x2000, 0x4000, 0x8000};

	// DEBUGOUT(IMGDETAIL, "emit_bits: code=%6d, size=%3d", nVal, nLen);

	while (nLen) {
		*(hpOut + lpex->nCompUsed) |= (nVal & valMask[nLen--] ? bMask[lpex->nAvail] : 0);
		lpex->nAvail--;

		if (!lpex->nAvail) {
			// DEBUGOUT(IMGDETAIL, "%x", (unsigned)((unsigned char)(lpex->lpDecodeBuf[lpex->nCompUsed])));
			if (*(hpOut + lpex->nCompUsed) == 0xff) {
				lpex->nCompUsed++;
				if (lpex->nCompUsed >= COMPRESSBUFSIZE) {
					lpex->lpIO->write(lpex->lpHandle, lpex->lpDecodeBuf, lpex->nCompUsed);
					lpex->lpDecodeBuf[0] = *((HPSTR) (lpex->lpDecodeBuf) + lpex->nCompUsed);
					lpex->nCompUsed = 0;	
				}
				*(hpOut + lpex->nCompUsed) = 0x00;
			}
			lpex->nAvail = 8;
			lpex->nCompUsed++;
			if (lpex->nCompUsed >= COMPRESSBUFSIZE) {
				lpex->lpIO->write(lpex->lpHandle, lpex->lpDecodeBuf, lpex->nCompUsed);
				lpex->lpDecodeBuf[0] = *((HPSTR) (lpex->lpDecodeBuf) + lpex->nCompUsed);
				lpex->nCompUsed = 0;	
			}
			*(hpOut + lpex->nCompUsed) = 0x00;
		}
	}

	return 0;
}

void        init_huff(UCHAR * pBits, UCHAR * pVal, LPHUFF_TBL lpHuff)
{
	int         p, i, l, lastp, si;
	char        huffsize[257];
	unsigned    huffcode[257];
	unsigned    code;

	lpHuff->bits = pBits;
	lpHuff->val = pVal;

	// create table of Huffman code lengths for each symbol
	p = 0;
	for (l = 1; l <= 16; l++) {
		for (i = 1; i <= (int) pBits[l]; i++) {
			huffsize[p++] = (char) l;
		}
	}
	huffsize[p] = 0;
	lastp = p;

	// generate the codes themselves
	code = 0;
	si = huffsize[0];
	p = 0;
	while (huffsize[p]) {
		while (((int) huffsize[p]) == si) {
			huffcode[p++] = code++;
		}
		code <<= 1;
		si++;
	}

	// generate encoding tables
	_fmemset(lpHuff->ehufsi, sizeof(lpHuff->ehufsi), 0x00);
	for (p = 0; p < lastp; p++) {
		lpHuff->ehufco[lpHuff->val[p]] = huffcode[p];
		lpHuff->ehufsi[lpHuff->val[p]] = huffsize[p];
	}
}

int         processRows(LPEXPORTINFO lpex)
{
	int         retval = IMGX_OK;
	int         i;
	HPSTR       lpY, lpCb, lpCr, lpSubOut, lpSubIn1;
	int         nSubRowSize;

	// don't need to subsample the Y component, but we do
	// want to copy it to our DCT-block sized buffer
	lpSubIn1 = lpex->lpY;
	lpSubOut = lpex->lpSubY;
	nSubRowSize = lpex->nMCUsPerRow * lpex->wRowsPerMCU;
	for (i = 0; i < lpex->wRowsPerMCU; i++) {
		_fmemcpy(lpSubOut, lpSubIn1, (int) (lpex->nImgWidth));
		lpSubIn1 += lpex->nImgWidth;
		lpSubOut += nSubRowSize;
	}

	if (lpex->wComponents == 3) {
		// do some subsampling on Cb & Cr components
		subsample(lpex->lpCb, lpex->lpSubCb, lpex->nImgWidth, lpex->nMCUsPerRow * 8);
		subsample(lpex->lpCr, lpex->lpSubCr, lpex->nImgWidth, lpex->nMCUsPerRow * 8);
	}

	// DCT and output the sub-sampled blocks
	//
	lpY = lpex->lpSubY;
	if (lpex->wComponents == 3) {
		lpCb = lpex->lpSubCb;
		lpCr = lpex->lpSubCr;
	}

	for (i = 0; i < lpex->nMCUsPerRow; i++) {

		if (lpex->wComponents == 1) {
			copyDCTblock(lpY, (LPINT) lpex->dctblock, lpex->nMCUsPerRow * 8);
			processDCTblock(lpex->dctblock, lpex->luminance_q, &(lpex->dchuff_tbl[0]), &(lpex->achuff_tbl[0]), &lpex->nLastYdc, lpex);

			lpY += DCTSIZE;
		}
		else {
			// first the Y component (2 horz, 2 vertical)
			copyDCTblock(lpY, (LPINT) lpex->dctblock, lpex->nMCUsPerRow * 16);
			processDCTblock(lpex->dctblock, lpex->luminance_q, &(lpex->dchuff_tbl[0]), &(lpex->achuff_tbl[0]), &lpex->nLastYdc, lpex);

			copyDCTblock(lpY + 8, (LPINT) lpex->dctblock, lpex->nMCUsPerRow * 16);
			processDCTblock(lpex->dctblock, lpex->luminance_q, &(lpex->dchuff_tbl[0]), &(lpex->achuff_tbl[0]), &lpex->nLastYdc, lpex);

			copyDCTblock(lpY + lpex->nMCUsPerRow * 16 * 8, (LPINT) lpex->dctblock, lpex->nMCUsPerRow * 16);
			processDCTblock(lpex->dctblock, lpex->luminance_q, &(lpex->dchuff_tbl[0]), &(lpex->achuff_tbl[0]), &lpex->nLastYdc, lpex);

			copyDCTblock(lpY + 8 + lpex->nMCUsPerRow * 16 * 8, (LPINT) lpex->dctblock, lpex->nMCUsPerRow * 16);
			processDCTblock(lpex->dctblock, lpex->luminance_q, &(lpex->dchuff_tbl[0]), &(lpex->achuff_tbl[0]), &lpex->nLastYdc, lpex);

			lpY += DCTSIZE * 2;

			// now the Cb component (just 1)
			copyDCTblock(lpCb, (LPINT) lpex->dctblock, lpex->nMCUsPerRow * 8);
			processDCTblock(lpex->dctblock, lpex->chrominance_q, &(lpex->dchuff_tbl[1]), &(lpex->achuff_tbl[1]), &lpex->nLastCbdc, lpex);
			lpCb += DCTSIZE;

			// and finally the Cr component
			copyDCTblock(lpCr, (LPINT) lpex->dctblock, lpex->nMCUsPerRow * 8);
			processDCTblock(lpex->dctblock, lpex->chrominance_q, &(lpex->dchuff_tbl[1]), &(lpex->achuff_tbl[1]), &lpex->nLastCrdc, lpex);
			lpCr += DCTSIZE;
		}
	}

	return 0;
}

void        setupQuantTables(LPEXPORTINFO lpex, unsigned qf)
{
	unsigned    i;
	long        temp;
	UCHAR FAR  *lum_q, FAR * chrom_q;

	if (qf <= 0)
		qf = 1;
	if (qf > 100)
		qf = 100;

	/* The basic table is used as-is (scaling 100) for a quality of 50. Qualities 50..100 are
	 * converted to scaling percentage 200 - 2*Q; note that at Q=100 the scaling is 0, which will
	 * cause j_add_quant_table to make all the table entries 1 (hence, no quantization loss).
	 * Qualities 1..50 are converted to scaling percentage 5000/Q. */
	if (qf < 50)
		qf = 5000 / qf;
	else
		qf = 200 - qf * 2;

	lum_q = lpex->luminance_q;
	chrom_q = lpex->chrominance_q;

	for (i = 0; i < DCTSIZE2; i++) {
		temp = ((long) std_luminance_quant_tbl[i] * qf + 50L) / 100L;
		if (temp > 255)
			temp = 255;
		if (temp < 1)
			temp = 1;
		lum_q[i] = (UCHAR) temp;

		temp = ((long) std_chrominance_quant_tbl[i] * qf + 50L) / 100L;
		if (temp > 255)
			temp = 255;
		if (temp < 1)
			temp = 1;
		chrom_q[i] = (UCHAR) temp;
	}
}

void        swap(LPSTR a)
{
	register char tmp;

	tmp = a[1];
	a[1] = a[0];
	a[0] = tmp;
}

void        swapl(LPSTR a)
{
	register char tmp;

	tmp = a[0];
	a[0] = a[3];
	a[3] = tmp;

	tmp = a[1];
	a[1] = a[2];
	a[2] = tmp;
}

#if DEBUG
void _cdecl FAR Debug(UINT wCategory, LPSTR lpFormat,...)
{
//	LPSTR       vaArgs;
//	BOOL        bResult;

//	va_start(vaArgs, lpFormat);
//	bResult = (*DebugStringOut) (wCategory, lpFormat, vaArgs);
//	va_end(vaArgs);
}
#else
void _cdecl FAR Debug(UINT wCategory, LPSTR lpFormat,...)
{
}
#endif

int NEAR PASCAL round(double val)
{

	if (val - (int) val < 0.5)
		return (int) val;
	else
		return (int) val + 1;
}
