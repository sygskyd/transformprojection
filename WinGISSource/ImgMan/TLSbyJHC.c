//////////////////////////////////////////////////
//
// TLSbyJHC.c
//
// created by Sygsky from JHC 03-OCT-2000 at 14:12
// to handle thread's local storage without calls to TLSAlloc() etc.
//
// mailto:sygsky@poboxes.com
//
//////////////////////////////////////////////////

#include "TLSByJHC.h"

typedef struct tagJHC_SINGLE_TLS {
	DWORD Id;
	DWORD Value;
} JHC_SINGLE_TLS;

// Personal storage for local thread data! One for any module
// Note: last entry is reserved to accumulate garbage from  all the attachments
// exceeded (MAXTHREADNUM-1) index

static JHC_SINGLE_TLS LocalTls[MAXTHREADNUM+1]; 

// Call it to store the value

BOOL bJHCTLSSetValue( DWORD value)
{
	DWORD Id = GetCurrentThreadId();
	DWORD i = 0;
	// Find Id already in list
	for(; i < MAXTHREADNUM; i++) 
		if (LocalTls[i].Id == Id) // Found, store here
		{ 
			LocalTls[i].Value = value;
			return TRUE;
		};
	
	// ID was not found, try to allocate it
	i= nJHCTLSAlloc(); // Allocate first free or last common 
	LocalTls[Id].Value = value;
	return TRUE; // Always true! At least common garbage accumulator will be set
};

// Call it at start in DllMain with reason DLL_THREAD_ATTACH to allocate the thread storage

DWORD nJHCTLSAlloc( VOID)
{
	DWORD Id = GetCurrentThreadId();
	DWORD i = 0;
	for(; i < MAXTHREADNUM; i++) 
		if (LocalTls[i].Id == Id)
		{
			LocalTls[i].Value = 0;	// Clear value in found slot
			return i;
		};
	// id was not found, try to allocate it
	for(i = 0; i < MAXTHREADNUM; i++) 
		if (LocalTls[i].Id == 0)
			break;
	LocalTls[i].Id = Id;	// Allocate new thread by set stamp.
	LocalTls[i].Value = 0;	// Clear value
	return i; // return first free or last default common index
};

// Call it to extract the loaded value (if it was not set before, zero will be returned)

DWORD nJHCTLSGetValue( VOID )
{
	DWORD Id = GetCurrentThreadId();
	DWORD i = 0;
	for(; i < MAXTHREADNUM; i++)  // Find  the entry for the caller's thread
		if (LocalTls[i].Id == Id) // Done, break with i set to found index
			break;
	return LocalTls[i].Value; // Always return something, garbage at least
};

// Call it at end in DllMain with reason DLL_THREAD_DETACH to allocate the thread storage

BOOL bJHCTLSFree( VOID )
{
	DWORD Id = GetCurrentThreadId();
	DWORD i = 0;
	BOOL res = FALSE;
	for(; i < MAXTHREADNUM; i++)  
		if (LocalTls[i].Id == Id) // Allocated space for the caller's thread is found
		{
			LocalTls[i].Id = 0;	// And marked now as free space
			LocalTls[i].Value = 0; // Just in case
			res = TRUE;			// Continue to find next duplicated entries
		};
	return res;
};

// Call it at end in DllMain with reason DLL_PROCESS_ATTACH/DETACH to allocate the thread storage

BOOL bJHCTLSClear( VOID )
{
	ZeroMemory(&LocalTls, sizeof(LocalTls));
	return TRUE;
};
